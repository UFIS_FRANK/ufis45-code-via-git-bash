// FidasDateTimeFieldDialog.cpp : implementation file
//

#include <stdafx.h>
#include <fidas.h>
#include <FidasDateTimeFieldDialog.h>
#include <FidasUtilities.h>
#include <CFDDoc.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

typedef struct	DateTimeFormat	{
	const char *omFormat;
	UINT		imName;
	BOOL		bmDefault;
}	DateTimeFormat;

static DateTimeFormat omHourFormat [] =	{
										"hh",	IDS_HOUR_FORMAT_12_WITH_LEADING_ZEROS,FALSE,
										"h",	IDS_HOUR_FORMAT_12_WITHOUT_LEADING_ZEROS,FALSE,
										"HH",	IDS_HOUR_FORMAT_24_WITH_LEADING_ZEROS,TRUE,
										"H",	IDS_HOUR_FORMAT_24_WITHOUT_LEADING_ZEROS,FALSE,
										NULL,	IDS_HOUR_FORMAT_NONE,FALSE
									};
		
static DateTimeFormat omMinuteFormat [] =	{
										"mm",	IDS_MINUTE_FORMAT_WITH_LEADING_ZEROS,TRUE,
										"m",	IDS_MINUTE_FORMAT_WITHOUT_LEADING_ZEROS,FALSE,
										NULL,	IDS_MINUTE_FORMAT_NONE,FALSE
									};
		
static DateTimeFormat omSecondFormat [] =	{
										"ss",	IDS_SECOND_FORMAT_WITH_LEADING_ZEROS,FALSE,
										"s",	IDS_SECOND_FORMAT_WITHOUT_LEADING_ZEROS,FALSE,
										NULL,	IDS_SECOND_FORMAT_NONE,TRUE
									};
		
static DateTimeFormat omIndicatorFormat [] =	{
										"am",	IDS_IND_FORMAT_AM_LOWERCASE,FALSE,
										"AM",	IDS_IND_FORMAT_AM_UPPERCASE,FALSE,
										"a",	IDS_IND_FORMAT_A_LOWERCASE,FALSE,
										"A",	IDS_IND_FORMAT_A_UPPERCASE,FALSE,
										NULL,	IDS_IND_FORMAT_NONE,TRUE
									};

static DateTimeFormat omYearFormat [] =	{
										"yyyy",	IDS_YEAR_FORMAT_4DIGITS,TRUE,
										"yy",	IDS_YEAR_FORMAT_2DIGITS,FALSE,
										NULL,	IDS_YEAR_FORMAT_NONE,FALSE
									};
		
static DateTimeFormat omMonthFormat [] =	{
										"mmmm",	IDS_MONTH_FORMAT_FULL_NAME_LOWERCASE,FALSE,
										"mmm",	IDS_MONTH_FORMAT_3LETTER_ABBREVIATION_LOWERCASE,FALSE,
										"mm",	IDS_MONTH_FORMAT_DIGITS_WITH_LEADING_ZEROS,TRUE,
										"m",	IDS_MONTH_FORMAT_DIGITS_WITHOUT_LEADING_ZEROS,FALSE,
										"MMMM",	IDS_MONTH_FORMAT_FULL_NAME_UPPERCASE,FALSE,
										"MMM",	IDS_MONTH_FORMAT_3LETTER_ABBREVIATION_UPPERCASE,FALSE,
										NULL,	IDS_MONTH_FORMAT_NONE,FALSE
									};

static DateTimeFormat omDayFormat [] =	{
										"d",	IDS_DAY_FORMAT_DIGITS_WITHOUT_LEADING_ZEROS,FALSE,
										"dd",	IDS_DAY_FORMAT_DIGITS_WITH_LEADING_ZEROS,TRUE,
										NULL,	IDS_DAY_FORMAT_NONE,FALSE
									};

static DateTimeFormat omDowFormat [] =	{
										"ww",	IDS_DOW_FORMAT_2LETTER_ABBREVIATION_LOWERCASE,FALSE,
										"www",	IDS_DOW_FORMAT_3LETTER_ABBREVIATION_LOWERCASE,FALSE,
										"wwww",	IDS_DOW_FORMAT_FULL_NAME_LOWERCASE,FALSE,
										"WW",	IDS_DOW_FORMAT_2LETTER_ABBREVIATION_UPPERCASE,FALSE,
										"WWW",	IDS_DOW_FORMAT_3LETTER_ABBREVIATION_UPPERCASE,FALSE,
										"WWWW",	IDS_DOW_FORMAT_FULL_NAME_UPPERCASE,FALSE,
										NULL,	IDS_DOW_FORMAT_NONE,TRUE
									};

/////////////////////////////////////////////////////////////////////////////
// FidasDateTimeFieldDialog dialog


FidasDateTimeFieldDialog::FidasDateTimeFieldDialog(CWnd* pParent /*=NULL*/)
	: CDialog(FidasDateTimeFieldDialog::IDD, pParent)
{
	EnableAutomation();

	//{{AFX_DATA_INIT(FidasDateTimeFieldDialog)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	pomFieldDesc = NULL;
	bmShowTime = TRUE;
	bmShowDate = TRUE;
	bmShowExtendedTime = TRUE;
}


void FidasDateTimeFieldDialog::OnFinalRelease()
{
	// When the last reference for an automation object is released
	// OnFinalRelease is called.  The base class will automatically
	// deletes the object.  Add additional cleanup required for your
	// object before calling the base class.

	CDialog::OnFinalRelease();
}

void FidasDateTimeFieldDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(FidasDateTimeFieldDialog)
	DDX_Control(pDX, IDC_UFIS_TIMEDATENEW_ADDITIONALINDICATOR_STATIC, m_Static_AdditionalIndicator);
	DDX_Control(pDX, IDC_UFIS_TIMEDATENEW_CANCEL_BUTTON, m_Cancel_Button);
	DDX_Control(pDX, IDC_UFIS_TIMEDATENEW_DATEHOUROFFSET_STATIC, m_Static_DateHourOffset);
	DDX_Control(pDX, IDC_UFIS_TIMEDATENEW_DATEPROPERTIES_GROUP, m_Group_DateProperties);
	DDX_Control(pDX, IDC_UFIS_TIMEDATENEW_DAYFORMAT_STATIC, m_Static_DayFormat);
	DDX_Control(pDX, IDC_UFIS_TIMEDATENEW_DOWFORMAT_STATIC, m_Static_DowFormat);
	DDX_Control(pDX, IDC_UFIS_TIMEDATENEW_HOURFORMAT_STATIC, m_Static_HourFormat);
	DDX_Control(pDX, IDC_UFIS_TIMEDATENEW_MINUTEFORMAT_STATIC, m_Static_MinuteFormat);
	DDX_Control(pDX, IDC_UFIS_TIMEDATENEW_MONTHFORMAT_STATIC, m_Static_MonthFormat);
	DDX_Control(pDX, IDC_UFIS_TIMEDATENEW_OK_BUTTON, m_Ok_Button);
	DDX_Control(pDX, IDC_UFIS_TIMEDATENEW_SECONDFORMAT_COMBO, m_Combo_SecondFormat);
	DDX_Control(pDX, IDC_UFIS_TIMEDATENEW_SECONDFORMAT_STATIC, m_Static_SecondFormat);
	DDX_Control(pDX, IDC_UFIS_TIMEDATENEW_TIMEHOUROFFSET_STATIC, m_Static_TimeHourOffset);
	DDX_Control(pDX, IDC_UFIS_TIMEDATENEW_TIMEPROPERTIES_GROUP, m_Group_TimeProperties);
	DDX_Control(pDX, IDC_UFIS_TIMEDATENEW_DATEMASKEDIT_GROUP, m_Group_DateMask);
	DDX_Control(pDX, IDC_UFIS_TIMEDATENEW_TIMEMASKEDIT_GROUP, m_Group_TimeMask);
	DDX_Control(pDX, IDC_UFIS_TIMEDATENEW_YEARFORMAT_STATIC, m_Static_YearFormat);
	DDX_Control(pDX, IDC_UFIS_TIMEDATENEW_SEPARATOR_STATIC, m_Static_Separator);
	DDX_Control(pDX, IDC_UFIS_TIMEDATENEW_DISPLAYORDER_GROUP, m_Group_DisplayOrder);
	DDX_Control(pDX, IDC_UFIS_TIMEDATENEW_EXTENDEDPROPERTIES_GROUP, m_Group_ExtendedProperties);
	DDX_Control(pDX, IDC_UFIS_TIMEDATENEW_DATEHOUROFFSET_EDIT, m_Edit_DateHourOffset);
	DDX_Control(pDX, IDC_UFIS_TIMEDATENEW_DATEHOUROFFSET_SPIN, m_Spin_DateHourOffset);
	DDX_Control(pDX, IDC_UFIS_TIMEDATENEW_DAYFORMAT_COMBO, m_Combo_DayFormat);
	DDX_Control(pDX, IDC_UFIS_TIMEDATENEW_DOWFORMAT_COMBO, m_Combo_DowFormat);
	DDX_Control(pDX, IDC_UFIS_TIMEDATENEW_HOURFORMAT_COMBO, m_Combo_HourFormat);
	DDX_Control(pDX, IDC_UFIS_TIMEDATENEW_MINUTEFORMAT_COMBO, m_Combo_MinuteFormat);
	DDX_Control(pDX, IDC_UFIS_TIMEDATENEW_MONTHFORMAT_COMBO, m_Combo_MonthFormat);
	DDX_Control(pDX, IDC_UFIS_TIMEDATENEW_TIMEHOUROFFSET_EDIT, m_Edit_TimeHourOffset);
	DDX_Control(pDX, IDC_UFIS_TIMEDATENEW_TIMEHOUROFFSET_SPIN, m_Spin_TimeHourOffset);
	DDX_Control(pDX, IDC_UFIS_TIMEDATENEW_YEARFORMAT_COMBO, m_Combo_YearFormat);
	DDX_Control(pDX, IDC_UFIS_TIMEDATENEW_DATEMASK_EDIT, m_Edit_DateMask);
	DDX_Control(pDX, IDC_UFIS_TIMEDATENEW_TIMEMASK_EDIT, m_Edit_TimeMask);
	DDX_Control(pDX, IDC_UFIS_TIMEDATENEW_SEPARATOR_EDIT, m_Edit_Separator);
	DDX_Control(pDX, IDC_UFIS_TIMEDATENEW_ADDITIONALINDICATOR_COMBO, m_Combo_AdditionalIndicator);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(FidasDateTimeFieldDialog, CDialog)
	//{{AFX_MSG_MAP(FidasDateTimeFieldDialog)
	ON_BN_CLICKED(IDC_UFIS_TIMEDATENEW_CANCEL_BUTTON, OnUfisTimedatenewCancelButton)
	ON_BN_CLICKED(IDC_UFIS_TIMEDATENEW_OK_BUTTON, OnUfisTimedatenewOkButton)
	ON_CBN_SELCHANGE(IDC_UFIS_TIMEDATENEW_MONTHFORMAT_COMBO, OnSelchangeUfisTimedatenewMonthformatCombo)
	ON_CBN_SELCHANGE(IDC_UFIS_TIMEDATENEW_DOWFORMAT_COMBO, OnSelchangeUfisTimedatenewDowformatCombo)
	ON_NOTIFY(UDN_DELTAPOS, IDC_UFIS_TIMEDATENEW_DATEHOUROFFSET_SPIN, OnDeltaposUfisTimedatenewDatehouroffsetSpin)
	ON_CBN_SELCHANGE(IDC_UFIS_TIMEDATENEW_ADDITIONALINDICATOR_COMBO, OnSelchangeUfisTimedatenewAdditionalindicatorCombo)
	ON_CBN_SELCHANGE(IDC_UFIS_TIMEDATENEW_YEARFORMAT_COMBO, OnSelchangeUfisTimedatenewYearformatCombo)
	ON_CBN_SELCHANGE(IDC_UFIS_TIMEDATENEW_DAYFORMAT_COMBO, OnSelchangeUfisTimedatenewDayformatCombo)
	ON_EN_CHANGE(IDC_UFIS_TIMEDATENEW_DATEHOUROFFSET_EDIT, OnChangeUfisTimedatenewDatehouroffsetEdit)
	ON_CBN_SELCHANGE(IDC_UFIS_TIMEDATENEW_HOURFORMAT_COMBO, OnSelchangeUfisTimedatenewHourformatCombo)
	ON_CBN_SELCHANGE(IDC_UFIS_TIMEDATENEW_MINUTEFORMAT_COMBO, OnSelchangeUfisTimedatenewMinuteformatCombo)
	ON_CBN_SELCHANGE(IDC_UFIS_TIMEDATENEW_SECONDFORMAT_COMBO, OnSelchangeUfisTimedatenewSecondformatCombo)
	ON_EN_CHANGE(IDC_UFIS_TIMEDATENEW_TIMEHOUROFFSET_EDIT, OnChangeUfisTimedatenewTimehouroffsetEdit)
	ON_EN_CHANGE(IDC_UFIS_TIMEDATENEW_SEPARATOR_EDIT, OnChangeUfisTimedatenewSeparatorEdit)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

BEGIN_DISPATCH_MAP(FidasDateTimeFieldDialog, CDialog)
	//{{AFX_DISPATCH_MAP(FidasDateTimeFieldDialog)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_DISPATCH_MAP
END_DISPATCH_MAP()

// Note: we add support for IID_IFidasDateTimeFieldDialog to support typesafe binding
//  from VBA.  This IID must match the GUID that is attached to the 
//  dispinterface in the .ODL file.

// {50809609-5E26-11D5-812B-00010215BFDE}
static const IID IID_IFidasDateTimeFieldDialog =
{ 0x50809609, 0x5e26, 0x11d5, { 0x81, 0x2b, 0x0, 0x1, 0x2, 0x15, 0xbf, 0xde } };

BEGIN_INTERFACE_MAP(FidasDateTimeFieldDialog, CDialog)
	INTERFACE_PART(FidasDateTimeFieldDialog, IID_IFidasDateTimeFieldDialog, Dispatch)
END_INTERFACE_MAP()

void FidasDateTimeFieldDialog::SetFieldDescription(CFieldDesc *prpFieldDesc)
{
	ASSERT(prpFieldDesc);
	pomFieldDesc = prpFieldDesc;
}

CString	FidasDateTimeFieldDialog::MakeDateFormat() const
{
	CString olFormat;
	// day format
	bool blUseDayFormat = false;
	int ind = m_Combo_DayFormat.GetCurSel();
	if (ind >= 0)
	{
		const char *polFormat = omDayFormat[m_Combo_DayFormat.GetItemData(ind)].omFormat;
		if (polFormat != NULL)
		{
			blUseDayFormat = true;
			olFormat += polFormat;
		}
			
	}

	// month format
	bool blUseMonthFormat = false;
	ind = m_Combo_MonthFormat.GetCurSel();
	if (ind >= 0)
	{
		const char *polFormat = omMonthFormat[m_Combo_MonthFormat.GetItemData(ind)].omFormat;
		if (polFormat != NULL)
		{
			if (blUseDayFormat)
				olFormat += '.';
			blUseMonthFormat = true;
			olFormat += polFormat;
		}
			
	}

	// year format
	bool blUseYearFormat = false;
	ind = m_Combo_YearFormat.GetCurSel();
	if (ind >= 0)
	{
		const char *polFormat = omYearFormat[m_Combo_YearFormat.GetItemData(ind)].omFormat;
		if (polFormat != NULL)
		{
			if (blUseDayFormat || blUseMonthFormat)
				olFormat += '.';
			blUseYearFormat = true;
			olFormat += polFormat;
		}
			
	}

	// dow format
	bool blUseDowFormat = false;
	ind = m_Combo_DowFormat.GetCurSel();
	if (ind >= 0)
	{
		const char *polFormat = omDowFormat[m_Combo_DowFormat.GetItemData(ind)].omFormat;
		if (polFormat != NULL)
		{
			if (blUseDayFormat || blUseMonthFormat || blUseYearFormat)
				olFormat += '.';
			blUseDowFormat = true;
			olFormat += polFormat;
		}
			
	}

	int mydatehouroffset = m_Spin_DateHourOffset.GetPos();
	if (mydatehouroffset != 0) 
	{
		CString mydatehouroffsetstr;
		if (mydatehouroffset > 32776) 
			mydatehouroffsetstr.Format("%i",mydatehouroffset-65536);
		else 
			mydatehouroffsetstr.Format("+%i",mydatehouroffset);

		if (blUseYearFormat || blUseMonthFormat || blUseDayFormat || blUseDowFormat) 
			olFormat +=" ";
		olFormat += mydatehouroffsetstr;
	}

	return olFormat;
}

BOOL FidasDateTimeFieldDialog::ParseDateFormat(const CString& Dateformatstr)
{
		CString myDateformatstr = Dateformatstr;

		int ilFound = -1;
		int ilNotFound = -1;
		for (int i = 0; i < m_Combo_YearFormat.GetCount(); i++)
		{
			const char *polFormat = omYearFormat[m_Combo_YearFormat.GetItemData(i)].omFormat;
			if (polFormat == NULL)
				ilNotFound = i;
			else if (myDateformatstr.Find(polFormat) != -1)
			{
				ilFound = i;
				break;
			}
		}

		if (ilFound >= 0)
			m_Combo_YearFormat.SetCurSel(ilFound);					
		else 
			m_Combo_YearFormat.SetCurSel(ilNotFound);

		ilFound = -1;
		ilNotFound = -1;
		for (i = 0; i < m_Combo_MonthFormat.GetCount(); i++)
		{
			const char *polFormat = omMonthFormat[m_Combo_MonthFormat.GetItemData(i)].omFormat;
			if (polFormat == NULL)
				ilNotFound = i;
			else if (myDateformatstr.Find(polFormat) != -1)
			{
				ilFound = i;
				break;
			}
		}

		if (ilFound >= 0)
			m_Combo_MonthFormat.SetCurSel(ilFound);					
		else 
			m_Combo_MonthFormat.SetCurSel(ilNotFound);

		ilFound = -1;
		ilNotFound = -1;
		for (i = 0; i < m_Combo_DayFormat.GetCount(); i++)
		{
			const char *polFormat = omDayFormat[m_Combo_DayFormat.GetItemData(i)].omFormat;
			if (polFormat == NULL)
				ilNotFound = i;
			else if (myDateformatstr.Find(polFormat) != -1)
			{
				ilFound = i;
				break;
			}
		}

		if (ilFound >= 0)
			m_Combo_DayFormat.SetCurSel(ilFound);					
		else 
			m_Combo_DayFormat.SetCurSel(ilNotFound);

		ilFound = -1;
		ilNotFound = -1;
		for (i = 0; i < m_Combo_DowFormat.GetCount(); i++)
		{
			const char *polFormat = omDowFormat[m_Combo_DowFormat.GetItemData(i)].omFormat;
			if (polFormat == NULL)
				ilNotFound = i;
			else if (myDateformatstr.Find(polFormat) != -1)
			{
				ilFound = i;
				break;
			}
		}

		if (ilFound >= 0)
			m_Combo_DowFormat.SetCurSel(ilFound);					
		else 
			m_Combo_DowFormat.SetCurSel(ilNotFound);


		// +/- hour offset
		int mypos = myDateformatstr.ReverseFind('+');
		if ((mypos != -1) && ((myDateformatstr[mypos+1] >= '0') && (myDateformatstr[mypos+1] <= '9'))) 
		{
			CString myoffset = myDateformatstr.Right(myDateformatstr.GetLength() - mypos);
			m_Edit_DateHourOffset.SetWindowText(myoffset);
			int myoffsetint = atoi(myoffset);
			m_Spin_DateHourOffset.SetPos(myoffsetint);
		}
		else
		{
			mypos = myDateformatstr.ReverseFind('-');
			if ((mypos != -1) && ((myDateformatstr[mypos+1] >= '0') && (myDateformatstr[mypos+1] <= '9'))) 
			{
				CString myoffset = myDateformatstr.Right(myDateformatstr.GetLength() - mypos);
				m_Edit_DateHourOffset.SetWindowText(myoffset);
				int myoffsetint=atoi(myoffset);
				m_Spin_DateHourOffset.SetPos(myoffsetint);

			}
			else 
				TRACE ("time hour offset is in incorrect format !!! (%s)\n",myDateformatstr);
		}
	return TRUE;
}

CString	FidasDateTimeFieldDialog::MakeTimeFormat() const
{
	CString olFormat;
	// Hour format
	bool blUseHourFormat = false;
	int ind = m_Combo_HourFormat.GetCurSel();
	if (ind >= 0)
	{
		const char *polFormat = omHourFormat[m_Combo_HourFormat.GetItemData(ind)].omFormat;
		if (polFormat != NULL)
		{
			blUseHourFormat = true;
			olFormat += polFormat;
		}
			
	}

	// Minute format
	bool blUseMinuteFormat = false;
	ind = m_Combo_MinuteFormat.GetCurSel();
	if (ind >= 0)
	{
		const char *polFormat = omMinuteFormat[m_Combo_MinuteFormat.GetItemData(ind)].omFormat;
		if (polFormat != NULL)
		{
			if (blUseHourFormat)
				olFormat += ':';
			blUseMinuteFormat = true;
			olFormat += polFormat;
		}
			
	}

	// Second format
	bool blUseSecondFormat = false;
	ind = m_Combo_SecondFormat.GetCurSel();
	if (ind >= 0)
	{
		const char *polFormat = omSecondFormat[m_Combo_SecondFormat.GetItemData(ind)].omFormat;
		if (polFormat != NULL)
		{
			if (blUseMinuteFormat || blUseHourFormat)
				olFormat += ':';
			blUseSecondFormat = true;
			olFormat += polFormat;
		}
			
	}

	// Indicator format
	bool blUseIndicatorFormat = false;
	ind = m_Combo_AdditionalIndicator.GetCurSel();
	if (ind >= 0)
	{
		const char *polFormat = omIndicatorFormat[m_Combo_AdditionalIndicator.GetItemData(ind)].omFormat;
		if (polFormat != NULL)
		{
			if (blUseSecondFormat || blUseMinuteFormat || blUseHourFormat)
				olFormat += ' ';
			blUseIndicatorFormat = true;
			olFormat += polFormat;
		}
			
	}

	int mytimehouroffset = m_Spin_TimeHourOffset.GetPos();
	if (mytimehouroffset != 0) 
	{
		CString mytimehouroffsetstr;
		if (mytimehouroffset > 32776) 
			mytimehouroffsetstr.Format("%i",mytimehouroffset-65536);
		else 
			mytimehouroffsetstr.Format("+%i",mytimehouroffset);
		
		if (blUseIndicatorFormat ||blUseSecondFormat || blUseMinuteFormat || blUseHourFormat) 
			olFormat +=' ';
		olFormat += mytimehouroffsetstr;
	}

	return olFormat;
}

BOOL FidasDateTimeFieldDialog::ParseTimeFormat(const CString& Timeformatstr)
{
		CString myTimeformatstr;

		// first, separate possible AM/PM part
		CString ampmstr= "";
		int ampmpos = Timeformatstr.Find(" ");
		if (ampmpos != -1) 
		{
			ampmstr = Timeformatstr.Right(Timeformatstr.GetLength() - ampmpos - 1);
			myTimeformatstr = Timeformatstr.Left(ampmpos);

			int ilFound = -1;
			int ilNotFound = -1;
			for (int i = 0; i < m_Combo_AdditionalIndicator.GetCount(); i++)
			{
				const char *polFormat = omIndicatorFormat[m_Combo_AdditionalIndicator.GetItemData(i)].omFormat;
				if (polFormat == NULL)
					ilNotFound = i;
				else if (ampmstr.Find(polFormat) != -1)
				{
					ilFound = i;
					break;
				}
			}

			if (ilFound >= 0)
				m_Combo_AdditionalIndicator.SetCurSel(ilFound);					
			else 
				m_Combo_AdditionalIndicator.SetCurSel(ilNotFound);
		}
		else 
		{
			ampmstr = Timeformatstr;
			for (int i = 0; i < m_Combo_AdditionalIndicator.GetCount(); i++)
			{
				const char *polFormat = omIndicatorFormat[m_Combo_AdditionalIndicator.GetItemData(i)].omFormat;
				if (polFormat == NULL)
				{
					m_Combo_AdditionalIndicator.SetCurSel(i);
					break;
				}
			}

			myTimeformatstr = Timeformatstr;
		}


		int ilFound = -1;
		int ilNotFound = -1;
		for (int i = 0; i < m_Combo_HourFormat.GetCount(); i++)
		{
			const char *polFormat = omHourFormat[m_Combo_HourFormat.GetItemData(i)].omFormat;
			if (polFormat == NULL)
				ilNotFound = i;
			else if (myTimeformatstr.Find(polFormat) != -1)
			{
				ilFound = i;
				break;
			}
		}

		if (ilFound >= 0)
			m_Combo_HourFormat.SetCurSel(ilFound);					
		else 
			m_Combo_HourFormat.SetCurSel(ilNotFound);

		ilFound = -1;
		ilNotFound = -1;
		for (i = 0; i < m_Combo_MinuteFormat.GetCount(); i++)
		{
			const char *polFormat = omMinuteFormat[m_Combo_MinuteFormat.GetItemData(i)].omFormat;
			if (polFormat == NULL)
				ilNotFound = i;
			else if (myTimeformatstr.Find(polFormat) != -1)
			{
				ilFound = i;
				break;
			}
		}

		if (ilFound >= 0)
			m_Combo_MinuteFormat.SetCurSel(ilFound);					
		else 
			m_Combo_MinuteFormat.SetCurSel(ilNotFound);

		ilFound = -1;
		ilNotFound = -1;
		for (i = 0; i < m_Combo_SecondFormat.GetCount(); i++)
		{
			const char *polFormat = omSecondFormat[m_Combo_SecondFormat.GetItemData(i)].omFormat;
			if (polFormat == NULL)
				ilNotFound = i;
			else if (myTimeformatstr.Find(polFormat) != -1)
			{
				ilFound = i;
				break;
			}
		}

		if (ilFound >= 0)
			m_Combo_SecondFormat.SetCurSel(ilFound);					
		else 
			m_Combo_SecondFormat.SetCurSel(ilNotFound);

		// +/- hour offset
		int mypos=ampmstr.ReverseFind('+');
		if ((mypos != -1) && ((ampmstr[mypos+1] >= '0') && (ampmstr[mypos+1] <= '9'))) 
		{
			CString myoffset=ampmstr.Right(ampmstr.GetLength()-mypos);
			m_Edit_TimeHourOffset.SetWindowText(myoffset);
			int myoffsetint=atoi(myoffset);
			m_Spin_TimeHourOffset.SetPos(myoffsetint);
		}
		else
		{
			mypos=ampmstr.ReverseFind('-');
			if ((mypos != -1) && ((ampmstr[mypos+1]>='0') && (ampmstr[mypos+1]<='9'))) 
			{
				CString myoffset=ampmstr.Right(ampmstr.GetLength()-mypos);
				m_Edit_TimeHourOffset.SetWindowText(myoffset);
				int myoffsetint=atoi(myoffset);
				m_Spin_TimeHourOffset.SetPos(myoffsetint);
			}
			else TRACE ("time hour offset is in incorrect format !!! (%s)\n",ampmstr);
		}
	return TRUE;
}

void FidasDateTimeFieldDialog::UpdateDateMask()
{
	if (m_Edit_DateMask.m_hWnd)
	{
		CString olFormat = MakeDateFormat();
		m_Edit_DateMask.SetWindowText(olFormat);
	}
}

void FidasDateTimeFieldDialog::UpdateTimeMask()
{
	if (m_Edit_TimeMask.m_hWnd)
	{
		CString olFormat = MakeTimeFormat();
		m_Edit_TimeMask.SetWindowText(olFormat);
	}
}

/////////////////////////////////////////////////////////////////////////////
// FidasDateTimeFieldDialog message handlers

BOOL FidasDateTimeFieldDialog::OnInitDialog() 
{
	ASSERT(pomFieldDesc && pomFieldDesc->DpyType() && pomFieldDesc->FieldRef());

	CString olTimeFormat;
	CString olDateFormat;

	const CDpyType_TimeDate *polType = static_cast<const CDpyType_TimeDate *>(pomFieldDesc->DpyType());

	switch(pomFieldDesc->DpyType()->GetType())
	{
	case CDpyType::T:
		bmShowDate = FALSE;
		olTimeFormat = polType->GetFormat();
	break;
	case CDpyType::D:
		bmShowTime = FALSE;
		olDateFormat = polType->GetFormat();
	break;
	case CDpyType::DT:
		{
			CString olFormat = polType->GetFormat();
			if (olFormat.GetLength())
			{
			}
			else
			{
				bmDateFirst = TRUE;
				omTimeDateSeparator="";
			}
		}
	break;
	case CDpyType::CT:
		bmShowDate = FALSE;
		bmShowExtendedTime = FALSE;
		olTimeFormat = polType->GetFormat();
	break;
	case CDpyType::CD:
		bmShowTime = FALSE;
		bmShowExtendedTime = FALSE;
		olDateFormat = polType->GetFormat();
	break;
	case CDpyType::CDT:
		bmShowExtendedTime = FALSE;
	break;
	case CDpyType::TT:
		bmShowDate = FALSE;
		olTimeFormat = polType->GetFormat();
	break;
	case CDpyType::TD:
		bmShowTime = FALSE;
		olDateFormat = polType->GetFormat();
	break;
	case CDpyType::TDT:
	break;
	default:
		ASSERT(FALSE);
	}
	
	CDialog::OnInitDialog();
	// localization
	SetWindowText(CFidasUtilities::GetString(IDS_UFIS_TIMEDATE_NEW));

	CWnd *pWnd = GetDlgItem(IDC_UFIS_TIMEDATENEW_DATEPROPERTIES_GROUP);
	if (pWnd)
		pWnd->SetWindowText(CFidasUtilities::GetString(IDS_UFIS_TIMEDATENEW_DATEPROPERTIES_GROUP));

	pWnd = GetDlgItem(IDC_UFIS_TIMEDATENEW_DAYFORMAT_STATIC);
	if (pWnd)
		pWnd->SetWindowText(CFidasUtilities::GetString(IDS_UFIS_TIMEDATENEW_DAYFORMAT_STATIC));

	pWnd = GetDlgItem(IDC_UFIS_TIMEDATENEW_MONTHFORMAT_STATIC);
	if (pWnd)
		pWnd->SetWindowText(CFidasUtilities::GetString(IDS_UFIS_TIMEDATENEW_MONTHFORMAT_STATIC));

	pWnd = GetDlgItem(IDC_UFIS_TIMEDATENEW_YEARFORMAT_STATIC);
	if (pWnd)
		pWnd->SetWindowText(CFidasUtilities::GetString(IDS_UFIS_TIMEDATENEW_YEARFORMAT_STATIC));

	pWnd = GetDlgItem(IDC_UFIS_TIMEDATENEW_DOWFORMAT_STATIC);
	if (pWnd)
		pWnd->SetWindowText(CFidasUtilities::GetString(IDS_UFIS_TIMEDATENEW_DOWFORMAT_STATIC));

	pWnd = GetDlgItem(IDC_UFIS_TIMEDATENEW_DATEHOUROFFSET_STATIC);
	if (pWnd)
		pWnd->SetWindowText(CFidasUtilities::GetString(IDS_UFIS_TIMEDATENEW_DATEHOUROFFSET_STATIC));

	pWnd = GetDlgItem(IDC_UFIS_TIMEDATENEW_DATEMASKEDIT_GROUP);
	if (pWnd)
		pWnd->SetWindowText(CFidasUtilities::GetString(IDS_UFIS_TIMEDATENEW_DATEMASKEDIT_GROUP));

	pWnd = GetDlgItem(IDC_UFIS_TIMEDATENEW_EXTENDEDPROPERTIES_GROUP);
	if (pWnd)
		pWnd->SetWindowText(CFidasUtilities::GetString(IDS_UFIS_TIMEDATENEW_EXTENDEDPROPERTIES_GROUP));

	pWnd = GetDlgItem(IDC_UFIS_TIMEDATENEW_SEPARATOR_STATIC);
	if (pWnd)
		pWnd->SetWindowText(CFidasUtilities::GetString(IDS_UFIS_TIMEDATENEW_SEPARATOR_STATIC));

	pWnd = GetDlgItem(IDC_UFIS_TIMEDATENEW_DISPLAYORDER_GROUP);
	if (pWnd)
		pWnd->SetWindowText(CFidasUtilities::GetString(IDS_UFIS_TIMEDATENEW_DISPLAYORDER_GROUP));

	pWnd = GetDlgItem(IDC_UFIS_TIMEDATENEW_DATEFIRST_RADIO);
	if (pWnd)
		pWnd->SetWindowText(CFidasUtilities::GetString(IDS_UFIS_TIMEDATENEW_DATEFIRST_RADIO));

	pWnd = GetDlgItem(IDC_UFIS_TIMEDATENEW_TIMEFIRST_RADIO);
	if (pWnd)
		pWnd->SetWindowText(CFidasUtilities::GetString(IDS_UFIS_TIMEDATENEW_TIMEFIRST_RADIO));

	pWnd = GetDlgItem(IDC_UFIS_TIMEDATENEW_OK_BUTTON);
	if (pWnd)
		pWnd->SetWindowText(CFidasUtilities::GetString(IDS_OK));
	
	pWnd = GetDlgItem(IDC_UFIS_TIMEDATENEW_CANCEL_BUTTON);
	if (pWnd)
		pWnd->SetWindowText(CFidasUtilities::GetString(IDS_CANCEL));

	pWnd = GetDlgItem(IDC_UFIS_TIMEDATENEW_TIMEPROPERTIES_GROUP);
	if (pWnd)
		pWnd->SetWindowText(CFidasUtilities::GetString(IDS_UFIS_TIMEDATENEW_TIMEPROPERTIES_GROUP));

	pWnd = GetDlgItem(IDC_UFIS_TIMEDATENEW_HOURFORMAT_STATIC);
	if (pWnd)
		pWnd->SetWindowText(CFidasUtilities::GetString(IDS_UFIS_TIMEDATENEW_HOURFORMAT_STATIC));

	pWnd = GetDlgItem(IDC_UFIS_TIMEDATENEW_MINUTEFORMAT_STATIC);
	if (pWnd)
		pWnd->SetWindowText(CFidasUtilities::GetString(IDS_UFIS_TIMEDATENEW_MINUTEFORMAT_STATIC));

	pWnd = GetDlgItem(IDC_UFIS_TIMEDATENEW_SECONDFORMAT_STATIC);
	if (pWnd)
		pWnd->SetWindowText(CFidasUtilities::GetString(IDS_UFIS_TIMEDATENEW_SECONDFORMAT_STATIC));

	pWnd = GetDlgItem(IDC_UFIS_TIMEDATENEW_ADDITIONALINDICATOR_STATIC);
	if (pWnd)
		pWnd->SetWindowText(CFidasUtilities::GetString(IDS_UFIS_TIMEDATENEW_ADDITIONALINDICATOR_STATIC));

	pWnd = GetDlgItem(IDC_UFIS_TIMEDATENEW_TIMEHOUROFFSET_STATIC);
	if (pWnd)
		pWnd->SetWindowText(CFidasUtilities::GetString(IDS_UFIS_TIMEDATENEW_TIMEHOUROFFSET_STATIC));

	pWnd = GetDlgItem(IDC_UFIS_TIMEDATENEW_TIMEMASKEDIT_GROUP);
	if (pWnd)
		pWnd->SetWindowText(CFidasUtilities::GetString(IDS_UFIS_TIMEDATENEW_TIMEMASKEDIT_GROUP));


	// TODO: Add extra initialization here
	if (bmShowDate)
	{
		m_Combo_YearFormat.ResetContent();
		for (int i = 0; ;i++)
		{
			int ind = m_Combo_YearFormat.AddString(CFidasUtilities::GetString(omYearFormat[i].imName));
			m_Combo_YearFormat.SetItemData(ind,i);

			if (omYearFormat[i].bmDefault)
				m_Combo_YearFormat.SetCurSel(ind);

			if (omYearFormat[i].omFormat == NULL)
				break;
		}

		m_Combo_MonthFormat.ResetContent();
		for (i = 0; ;i++)
		{
			int ind = m_Combo_MonthFormat.AddString(CFidasUtilities::GetString(omMonthFormat[i].imName));
			m_Combo_MonthFormat.SetItemData(ind,i);

			if (omMonthFormat[i].omFormat == NULL)
				break;
		}

		m_Combo_DayFormat.ResetContent();
		for (i = 0; ;i++)
		{
			int ind = m_Combo_DayFormat.AddString(CFidasUtilities::GetString(omDayFormat[i].imName));
			m_Combo_DayFormat.SetItemData(ind,i);

			if (omDayFormat[i].omFormat == NULL)
				break;
		}

		m_Combo_DowFormat.ResetContent();
		for (i = 0; ;i++)
		{
			int ind = m_Combo_DowFormat.AddString(CFidasUtilities::GetString(omDowFormat[i].imName));
			m_Combo_DowFormat.SetItemData(ind,i);

			if (omDowFormat[i].omFormat == NULL)
				break;
		}

		// set current selections
		for (i = 0; ;i++)
		{
			if (omYearFormat[i].bmDefault)
			{
				int ind = m_Combo_YearFormat.FindStringExact(-1,CFidasUtilities::GetString(omYearFormat[i].imName));
				m_Combo_YearFormat.SetCurSel(ind);
			}

			if (omYearFormat[i].omFormat == NULL)
				break;
		}

		// set current selections
		for (i = 0; ;i++)
		{
			if (omMonthFormat[i].bmDefault)
			{
				int ind = m_Combo_MonthFormat.FindStringExact(-1,CFidasUtilities::GetString(omMonthFormat[i].imName));
				m_Combo_MonthFormat.SetCurSel(ind);
			}

			if (omMonthFormat[i].omFormat == NULL)
				break;
		}

		// set current selections
		for (i = 0; ;i++)
		{
			if (omDayFormat[i].bmDefault)
			{
				int ind = m_Combo_DayFormat.FindStringExact(-1,CFidasUtilities::GetString(omDayFormat[i].imName));
				m_Combo_DayFormat.SetCurSel(ind);
			}

			if (omDayFormat[i].omFormat == NULL)
				break;
		}

		// set current selections
		for (i = 0; ;i++)
		{
			if (omDowFormat[i].bmDefault)
			{
				int ind = m_Combo_DowFormat.FindStringExact(-1,CFidasUtilities::GetString(omDowFormat[i].imName));
				m_Combo_DowFormat.SetCurSel(ind);
			}

			if (omDowFormat[i].omFormat == NULL)
				break;
		}

		m_Spin_DateHourOffset.SetRange(-12, 12);	// wes 67

		if (olDateFormat.GetLength())
			ParseDateFormat(olDateFormat);
		UpdateDateMask();

	}

	// initialize time
	if (bmShowTime)
	{
		m_Combo_HourFormat.ResetContent();
		for (int i = 0; ;i++)
		{
			int ind = m_Combo_HourFormat.AddString(CFidasUtilities::GetString(omHourFormat[i].imName));
			m_Combo_HourFormat.SetItemData(ind,i);

			if (omHourFormat[i].bmDefault)
				m_Combo_HourFormat.SetCurSel(ind);

			if (omHourFormat[i].omFormat == NULL)
				break;
		}

		m_Combo_MinuteFormat.ResetContent();
		for (i = 0; ;i++)
		{
			int ind = m_Combo_MinuteFormat.AddString(CFidasUtilities::GetString(omMinuteFormat[i].imName));
			m_Combo_MinuteFormat.SetItemData(ind,i);

			if (omMinuteFormat[i].omFormat == NULL)
				break;
		}

		m_Combo_SecondFormat.ResetContent();
		for (i = 0; ;i++)
		{
			int ind = m_Combo_SecondFormat.AddString(CFidasUtilities::GetString(omSecondFormat[i].imName));
			m_Combo_SecondFormat.SetItemData(ind,i);

			if (omSecondFormat[i].omFormat == NULL)
				break;
		}

		m_Combo_AdditionalIndicator.ResetContent();
		for (i = 0; ;i++)
		{
			int ind = m_Combo_AdditionalIndicator.AddString(CFidasUtilities::GetString(omIndicatorFormat[i].imName));
			m_Combo_AdditionalIndicator.SetItemData(ind,i);

			if (omIndicatorFormat[i].omFormat == NULL)
				break;
		}

		// set current selections
		for (i = 0; ;i++)
		{
			if (omHourFormat[i].bmDefault)
			{
				int ind = m_Combo_HourFormat.FindStringExact(-1,CFidasUtilities::GetString(omHourFormat[i].imName));
				m_Combo_HourFormat.SetCurSel(ind);
			}

			if (omHourFormat[i].omFormat == NULL)
				break;
		}

		// set current selections
		for (i = 0; ;i++)
		{
			if (omMinuteFormat[i].bmDefault)
			{
				int ind = m_Combo_MinuteFormat.FindStringExact(-1,CFidasUtilities::GetString(omMinuteFormat[i].imName));
				m_Combo_MinuteFormat.SetCurSel(ind);
			}

			if (omMinuteFormat[i].omFormat == NULL)
				break;
		}

		// set current selections
		for (i = 0; ;i++)
		{
			if (omSecondFormat[i].bmDefault)
			{
				int ind = m_Combo_SecondFormat.FindStringExact(-1,CFidasUtilities::GetString(omSecondFormat[i].imName));
				m_Combo_SecondFormat.SetCurSel(ind);
			}

			if (omSecondFormat[i].omFormat == NULL)
				break;
		}

		for (i = 0; ;i++)
		{
			if (omIndicatorFormat[i].bmDefault)
			{
				int ind = m_Combo_AdditionalIndicator.FindStringExact(-1,CFidasUtilities::GetString(omIndicatorFormat[i].imName));
				m_Combo_AdditionalIndicator.SetCurSel(ind);
			}

			if (omIndicatorFormat[i].omFormat == NULL)
				break;
		}

		m_Spin_TimeHourOffset.SetRange(-12, 12);	// wes 67

		if (olTimeFormat.GetLength())
			ParseTimeFormat(olTimeFormat);
		UpdateTimeMask();

	}

	// enable date settings
	m_Static_YearFormat.EnableWindow(bmShowDate);
	m_Combo_YearFormat.EnableWindow(bmShowDate);
	m_Static_MonthFormat.EnableWindow(bmShowDate);
	m_Combo_MonthFormat.EnableWindow(bmShowDate);
	m_Static_DowFormat.EnableWindow(bmShowDate);
	m_Combo_DowFormat.EnableWindow(bmShowDate);
	m_Static_DayFormat.EnableWindow(bmShowDate);
	m_Combo_DayFormat.EnableWindow(bmShowDate);
	m_Group_DateProperties.EnableWindow(bmShowDate);
	m_Static_DateHourOffset.EnableWindow(bmShowDate && bmShowExtendedTime);
	m_Spin_DateHourOffset.EnableWindow(bmShowDate && bmShowExtendedTime);
	m_Edit_DateHourOffset.EnableWindow(bmShowDate && bmShowExtendedTime);
	m_Group_DateMask.EnableWindow(bmShowDate);
	m_Edit_DateMask.EnableWindow(bmShowDate);

	// enable time settings	
	m_Group_TimeProperties.EnableWindow(bmShowTime);
	m_Static_TimeHourOffset.EnableWindow(bmShowTime && bmShowExtendedTime);
	m_Spin_TimeHourOffset.EnableWindow(bmShowTime && bmShowExtendedTime);
	m_Edit_TimeHourOffset.EnableWindow(bmShowTime && bmShowExtendedTime);
	m_Static_SecondFormat.EnableWindow(bmShowTime);
	m_Combo_SecondFormat.EnableWindow(bmShowTime);
	m_Static_AdditionalIndicator.EnableWindow(bmShowTime);
	m_Combo_AdditionalIndicator.EnableWindow(bmShowTime);
	m_Static_MinuteFormat.EnableWindow(bmShowTime);
	m_Combo_MinuteFormat.EnableWindow(bmShowTime);
	m_Static_HourFormat.EnableWindow(bmShowTime);
	m_Combo_HourFormat.EnableWindow(bmShowTime);
	m_Group_TimeMask.EnableWindow(bmShowTime);
	m_Edit_TimeMask.EnableWindow(bmShowTime);

	// enable exteneded properties
	bool blShowExtendedProperties = (bmShowTime && bmShowDate);
	m_Edit_Separator.ShowWindow(blShowExtendedProperties);
	m_Static_Separator.ShowWindow(blShowExtendedProperties);
	m_Group_ExtendedProperties.ShowWindow(blShowExtendedProperties);
	m_Group_DisplayOrder.ShowWindow(blShowExtendedProperties);
	((CButton*) GetDlgItem(IDC_UFIS_TIMEDATENEW_DATEFIRST_RADIO))->ShowWindow(blShowExtendedProperties);
	((CButton*) GetDlgItem(IDC_UFIS_TIMEDATENEW_TIMEFIRST_RADIO))->ShowWindow(blShowExtendedProperties);

	if (blShowExtendedProperties)
	{
		if (!bmDateFirst)
		{
			((CButton*) GetDlgItem(IDC_UFIS_TIMEDATENEW_DATEFIRST_RADIO))->SetCheck(0);
			((CButton*) GetDlgItem(IDC_UFIS_TIMEDATENEW_TIMEFIRST_RADIO))->SetCheck(1);
		}
		else
		{
			((CButton*) GetDlgItem(IDC_UFIS_TIMEDATENEW_DATEFIRST_RADIO))->SetCheck(1);
			((CButton*) GetDlgItem(IDC_UFIS_TIMEDATENEW_TIMEFIRST_RADIO))->SetCheck(0);
		}

/*
		myUFISPageEditorExtention.MapUFIS2Separator(TimeDateSeparator);

		if (myUFISPageEditorExtention.IsValidSeparatorString(TimeDateSeparator)) 
			goodtext = TimeDateSeparator;
		else
		{
			TRACE ("this separator is not valid and was ignored: %s\n",TimeDateSeparator);
			TimeDateSeparator="";
			goodtext="";
		}
		m_Edit_Separator.SetWindowText(TimeDateSeparator);
*/
	}
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void FidasDateTimeFieldDialog::OnUfisTimedatenewCancelButton() 
{
	// TODO: Add your control notification handler code here
	EndDialog(IDCANCEL);
}

void FidasDateTimeFieldDialog::OnUfisTimedatenewOkButton() 
{
	// TODO: Add your control notification handler code here
	CString olTimeFormat;
	CString olDateFormat;

	m_Edit_TimeMask.GetWindowText(olTimeFormat);
	m_Edit_DateMask.GetWindowText(olDateFormat);

//	m_Edit_Separator.GetWindowText(TimeDateSeparator);
//	myUFISPageEditorExtention.MapSeparator2UFIS(TimeDateSeparator);

	CDpyType_TimeDate *polType = static_cast<CDpyType_TimeDate *>(pomFieldDesc->GetDpyType());
	ASSERT(polType);

	BOOL blShowExtendedProperties = bmShowDate && bmShowTime;

	if (blShowExtendedProperties)
	{
		CString olFormat;
		if (bmDateFirst)
			olFormat.Format("%d,%s,%s,%s",bmDateFirst,omTimeDateSeparator,olDateFormat,olTimeFormat);
		else
			olFormat.Format("%d,%s,%s,%s",bmDateFirst,omTimeDateSeparator,olTimeFormat,olDateFormat);
		polType->SetFormat(olFormat);
	}
	else if (bmShowTime)
	{
		polType->SetFormat(olTimeFormat);
	}
	else if (bmShowDate)
	{
		polType->SetFormat(olDateFormat);
	}

	pomFieldDesc->SetDpyType(polType);

	if (blShowExtendedProperties)	
		bmDateFirst = (((CButton*) GetDlgItem(IDC_UFIS_TIMEDATENEW_DATEFIRST_RADIO))->GetCheck() == 1);
	else 
		bmDateFirst = true;

	EndDialog(IDOK);
}

void FidasDateTimeFieldDialog::OnSelchangeUfisTimedatenewMonthformatCombo() 
{
	UpdateDateMask();
}

void FidasDateTimeFieldDialog::OnSelchangeUfisTimedatenewDowformatCombo() 
{
	UpdateDateMask();
}

void FidasDateTimeFieldDialog::OnDeltaposUfisTimedatenewDatehouroffsetSpin(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	// TODO: Add your control notification handler code here
	
	*pResult = 0;
}

void FidasDateTimeFieldDialog::OnSelchangeUfisTimedatenewAdditionalindicatorCombo() 
{
	UpdateTimeMask();
}

void FidasDateTimeFieldDialog::OnSelchangeUfisTimedatenewYearformatCombo() 
{
	UpdateDateMask();
}

void FidasDateTimeFieldDialog::OnSelchangeUfisTimedatenewDayformatCombo() 
{
	UpdateDateMask();
}

void FidasDateTimeFieldDialog::OnChangeUfisTimedatenewDatehouroffsetEdit() 
{
	UpdateDateMask();
}

void FidasDateTimeFieldDialog::OnSelchangeUfisTimedatenewHourformatCombo() 
{
	UpdateTimeMask();
}

void FidasDateTimeFieldDialog::OnSelchangeUfisTimedatenewMinuteformatCombo() 
{
	UpdateTimeMask();
}

void FidasDateTimeFieldDialog::OnSelchangeUfisTimedatenewSecondformatCombo() 
{
	UpdateTimeMask();
}

void FidasDateTimeFieldDialog::OnChangeUfisTimedatenewTimehouroffsetEdit() 
{
	UpdateTimeMask();	
}

void FidasDateTimeFieldDialog::OnChangeUfisTimedatenewSeparatorEdit() 
{
/**
	CString mysep;
	m_Edit_Separator.GetWindowText(mysep);
	if (!myUFISPageEditorExtention.IsValidSeparatorString(mysep,3))		// wes 73
	{
		TRACE ("due to internal processing, this character is forbidden!\n");
		if (mysep.GetLength()>3) MessageBox(UFIS_TXT_ErrorTooManyChars,UFIS_TXT_ErrorCaption,MB_ICONHAND + MB_OK);
		else MessageBox(UFIS_TXT_ErrorCharNotAllowed,UFIS_TXT_ErrorCaption,MB_ICONHAND + MB_OK);
		mysep=goodtext;		// wes 73
		m_Edit_Separator.SetWindowText(mysep);
	}
	else goodtext=mysep;			// wes 73
*/
}
