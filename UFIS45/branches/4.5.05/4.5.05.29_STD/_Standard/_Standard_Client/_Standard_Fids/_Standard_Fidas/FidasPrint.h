#ifndef	_FidasPrint_h
#define	_FidasPrint_h

#include <vector>

class CFidasPrint : public CObject
{
public : // Attribute types

	enum egPrintInfo {	PRINT_LANDSCAPE,PRINT_PORTRAET,PRINT_LEFT,PRINT_RIGHT,PRINT_CENTER,
						PRINT_NOFRAME,PRINT_FRAMETHIN,PRINT_FRAMEMEDIUM,PRINT_FRAMETHICK,
						PRINT_SMALL,PRINT_MEDIUM,PRINT_LARGE,
						PRINT_SMALLBOLD,PRINT_MEDIUMBOLD,PRINT_LARGEBOLD
					};

	struct PRINTELEDATA
	{
		int Alignment;
		int Length;
		int FrameLeft;
		int FrameRight;
		int FrameTop;
		int FrameBottom;
		CFont *pFont;
		CString Text;
	};
public:
	CFidasPrint(CWnd *opParent = NULL);
	CFidasPrint(CWnd *opParent,int ipOrientation,int ipLineHeight,int ipFirstLine,int ipLeftOffset,
		CString opHeader1 = "",CString opHeader2 = "",CString opHeader3 = "",
		CString opHeader4 = "",
		CString opFooter1 = "",CString opFooter2 = "",CString opFooter3 = "");

	~CFidasPrint();

	// Implementation
	InitializePrinter(int ipOrientation = PRINT_PORTRAET);


	BOOL OnPreparePrinting(CPrintInfo* pInfo,int ipOrientation,int ipLineHeight,int ipFirstLine,int ipLeftOffset);
	void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);

	BOOL PrintHeader(void);
	BOOL PrintHeader(CString opHeader1,CString opHeader2 = "",CString opHeader3 = "",CString opHeader4 = "");
	BOOL PrintFooter();
	BOOL PrintFooter(CString opFooter1,CString opFooter2 = "");
	BOOL PrintLine(std::vector<PRINTELEDATA> &rlPrintLine);

	BOOL PrintText(int ipStartX,int ipStartY,int ipEndX,int ipEndY,int ipType,CString opText,BOOL ipUseOffset);
	void SetBitmaps(CBitmap *popBitmap, CBitmap *popCcsBitmap);

private:
	BOOL SelectFramePen(int ipFrameType);
	void PrintLeft(CRect opRect,CString opText);
	void PrintRight(CRect opRect,CString opText);
	void PrintCenter(CRect opRect,CString opText);
	int	 MMX(int);
	int	 MMY(int);

	// Attributes
public:
	CDC	 omCdc;
	int imLineNo;
	int imPageNo;
	int imMaxLinesPerPage;
	
	int imLineHeight;
	int imGanttLineHeight;
	int imBarHeight;
	int imBarVerticalTextOffset;
	int imFirstLine;
	int imLeftOffset;
	int imYOffset;

	CFont omSmallFont_Regular;
	CFont omMediumFont_Regular;
	CFont omLargeFont_Regular;
	CFont omSmallFont_Bold;
	CFont omMediumFont_Bold;
	CFont omLargeFont_Bold;
	CFont omCCSFont;
	CFont omSmallFont_RegularForGantt;

    CPen ThinPen;
    CPen MediumPen;
    CPen ThickPen;
	CPen DottedPen;

private:
	CRgn			omRgn;
	int				imOrientation;
	CStringArray	omHeader;
	CStringArray	omFooter;
	CString			omLogoPath;
	CWnd*			pomParent;
	CBitmap*		pomBitmap;
	CDC				omMemDc;
	CBitmap*		pomCcsBitmap;
	CDC				omCcsMemDc;
	int				imLogPixelsY;
	int				imLogPixelsX;
	BOOL			bmIsInitialized;
	double			dmXFactor;

	int				imGanttStartX;
	int				imGanttEndX;
	int				imGanttStartY;
	int				imGanttEndY;

	CTime			omStartTime;
	CTime			omEndTime;

};

#endif