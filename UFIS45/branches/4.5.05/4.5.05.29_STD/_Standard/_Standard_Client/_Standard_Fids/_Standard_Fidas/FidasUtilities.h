// FidasUtilities.h : utilities header file for the Fidas application
//

#if !defined(AFX_FidasUtilities_H__7B1DCED8_54EB_11D5_811D_00010215BFDE__INCLUDED_)
#define AFX_FidasUtilities_H__7B1DCED8_54EB_11D5_811D_00010215BFDE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include <resource.h>       // main symbols
#include <CDpyType.h>

class CFieldInfo;
typedef std::vector<CFieldInfo> CFieldInfoVector;

class CFidasUtilities
{

public :
	static bool		Initialize();
	static CString	ConfigurationPath();
	static CString	ApplicationName();
	static CString	TableExtension();
	static CString	HomeAirport();
	static CString	HostName();
	static CString	Language();
	static BOOL		DisplayDebugInfo();

	static CString	GetString(UINT upId);
	static int		ExtractTextLineFast(CStringArray& ropItems,const char *pcpLineText,const char *pcpSepa);
	static CString	GetString(const CDpyType::Type& repType);
	static BOOL		IsValidString(const CString& ropString,int ipMaxChars = 10200);
	static BOOL		IsValidSeparatorString(const CString& ropString,int ipMaxChars = 10200);

	static BOOL		GetTableList(const CDpyType::Type& repType,CStringVector& ropTableNames);
	static BOOL		GetFieldList(const CDpyType::Type& repType,const CString& ropTable,CFieldInfoVector& ropFieldInfos);

	static void		LogDebugInfo(const char *popFormat,...);	
	static void		GetDebugInfo(CListBox& ropDebugInfo);
private:
	static CStringVector omDebugInfo;
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_FidasUtilities_H__7B1DCED8_54EB_11D5_811D_00010215BFDE__INCLUDED_)
