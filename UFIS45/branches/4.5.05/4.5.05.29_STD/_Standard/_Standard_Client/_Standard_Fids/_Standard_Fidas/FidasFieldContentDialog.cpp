// FidasFieldContentDialog.cpp : implementation file
//

#include <stdafx.h>
#include <fidas.h>
#include <FidasFieldContentDialog.h>
#include <FidasUtilities.h>
#include <FidasSelectTableFieldDialog.h>
#include <CFDDoc.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CFidasFieldContentDialog dialog


CFidasFieldContentDialog::CFidasFieldContentDialog(CWnd* pParent /*=NULL*/)
	: CDialog(CFidasFieldContentDialog::IDD, pParent)
{
	EnableAutomation();

	//{{AFX_DATA_INIT(CFidasFieldContentDialog)
	bmRange = FALSE;
	omSeparator = _T(".");
	imRangeStart = 0;
	imRangeEnd = 0;
	omSecondField = _T("[second field not defined]");
	//}}AFX_DATA_INIT
	pomFieldDesc = NULL;
	imRangeMin = 0;
	imRangeMax = 0;
}


void CFidasFieldContentDialog::OnFinalRelease()
{
	// When the last reference for an automation object is released
	// OnFinalRelease is called.  The base class will automatically
	// deletes the object.  Add additional cleanup required for your
	// object before calling the base class.

	CDialog::OnFinalRelease();
}

void CFidasFieldContentDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CFidasFieldContentDialog)
	DDX_Check(pDX, IDC_UFIS_FIELDDEF_RANGE_CHECK, bmRange);
	DDX_Text(pDX, IDC_UFIS_FIELDDEF_SEPARATEDBY_EDIT, omSeparator);
	DDV_MaxChars(pDX, omSeparator, 127);
	DDX_Text(pDX, IDC_UFIS_FIELDDEF_EDIT_START, imRangeStart);
	DDV_MinMaxInt(pDX, imRangeStart, imRangeMin, imRangeMax);
	DDX_Text(pDX, IDC_UFIS_FIELDDEF_EDIT_END, imRangeEnd);
	DDV_MinMaxInt(pDX, imRangeEnd, imRangeMin, imRangeMax);
	DDX_Text(pDX, IDC_UFIS_FIELDDEF_STATIC_SECONDFIELD, omSecondField);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CFidasFieldContentDialog, CDialog)
	//{{AFX_MSG_MAP(CFidasFieldContentDialog)
	ON_BN_CLICKED(IDC_UFIS_FIELDDEF_CANCEL_BUTTON, OnUfisFielddefCancelButton)
	ON_BN_CLICKED(IDC_UFIS_FIELDDEF_OK_BUTTON, OnUfisFielddefOkButton)
	ON_BN_CLICKED(IDC_UFIS_FIELDDEF_SELECTSECONDFIELD_BUTTON, OnUfisFielddefSelectsecondfieldButton)
	ON_EN_CHANGE(IDC_UFIS_FIELDDEF_SEPARATEDBY_EDIT, OnChangeUfisFielddefSeparatedbyEdit)
	ON_BN_CLICKED(IDC_UFIS_FIELDDEF_RANGE_CHECK, OnUfisFielddefRangeCheck)
	ON_EN_CHANGE(IDC_UFIS_FIELDDEF_EDIT_END, OnChangeUfisFielddefEditEnd)
	ON_EN_CHANGE(IDC_UFIS_FIELDDEF_EDIT_START, OnChangeUfisFielddefEditStart)
	ON_EN_KILLFOCUS(IDC_UFIS_FIELDDEF_EDIT_END, OnKillfocusUfisFielddefEditEnd)
	ON_EN_KILLFOCUS(IDC_UFIS_FIELDDEF_EDIT_START, OnKillfocusUfisFielddefEditStart)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

BEGIN_DISPATCH_MAP(CFidasFieldContentDialog, CDialog)
	//{{AFX_DISPATCH_MAP(CFidasFieldContentDialog)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_DISPATCH_MAP
END_DISPATCH_MAP()

// Note: we add support for IID_IFidasFieldContentDialog to support typesafe binding
//  from VBA.  This IID must match the GUID that is attached to the 
//  dispinterface in the .ODL file.

// {564A0CC8-65FF-11D5-8133-00010215BFDE}
static const IID IID_IFidasFieldContentDialog =
{ 0x564a0cc8, 0x65ff, 0x11d5, { 0x81, 0x33, 0x0, 0x1, 0x2, 0x15, 0xbf, 0xde } };

BEGIN_INTERFACE_MAP(CFidasFieldContentDialog, CDialog)
	INTERFACE_PART(CFidasFieldContentDialog, IID_IFidasFieldContentDialog, Dispatch)
END_INTERFACE_MAP()

void CFidasFieldContentDialog::SetFieldDescription(CFieldDesc *prpFieldDesc)
{
	ASSERT(prpFieldDesc);
	pomFieldDesc = prpFieldDesc;
}

/////////////////////////////////////////////////////////////////////////////
// CFidasFieldContentDialog message handlers

BOOL CFidasFieldContentDialog::OnInitDialog() 
{
	ASSERT(pomFieldDesc && pomFieldDesc->DpyType() && pomFieldDesc->DpyType()->GetType() == CDpyType::R);

	CDialog::OnInitDialog();
	
	// Localization
	SetWindowText(CFidasUtilities::GetString(IDS_UFIS_FIELDCONTENT));

	CWnd *pWnd = GetDlgItem(IDC_UFIS_FIELDDEF_SECONDFIELD_FRAME);
	if (pWnd)
		pWnd->SetWindowText(CFidasUtilities::GetString(IDS_UFIS_FIELDDEF_SECONDFIELD_FRAME));

	pWnd = GetDlgItem(IDC_UFIS_FIELDDEF_STATIC_SECONDFIELD);
	if (pWnd)
		pWnd->SetWindowText(CFidasUtilities::GetString(IDS_UFIS_FIELDDEF_STATIC_SECONDFIELD));

	pWnd = GetDlgItem(IDC_UFIS_FIELDDEF_SELECTSECONDFIELD_BUTTON);
	if (pWnd)
		pWnd->SetWindowText(CFidasUtilities::GetString(IDS_UFIS_FIELDDEF_SELECTSECONDFIELD_BUTTON));

	pWnd = GetDlgItem(IDC_UFIS_FIELDDEF_RANGE_CHECK);
	if (pWnd)
		pWnd->SetWindowText(CFidasUtilities::GetString(IDS_UFIS_FIELDDEF_RANGE_CHECK));

	pWnd = GetDlgItem(IDC_UFIS_FIELDDEF_STATIC_START);
	if (pWnd)
		pWnd->SetWindowText(CFidasUtilities::GetString(IDS_UFIS_FIELDDEF_STATIC_START));

	pWnd = GetDlgItem(IDC_UFIS_FIELDDEF_STATIC_END);
	if (pWnd)
		pWnd->SetWindowText(CFidasUtilities::GetString(IDS_UFIS_FIELDDEF_STATIC_END));

	pWnd = GetDlgItem(IDC_UFIS_FIELDDEF_SEPARATED_STATIC);
	if (pWnd)
		pWnd->SetWindowText(CFidasUtilities::GetString(IDS_UFIS_FIELDDEF_SEPARATED_STATIC));

	pWnd = GetDlgItem(IDC_UFIS_FIELDDEF_OK_BUTTON);
	if (pWnd)
		pWnd->SetWindowText(CFidasUtilities::GetString(IDS_OK));
	
	pWnd = GetDlgItem(IDC_UFIS_FIELDDEF_CANCEL_BUTTON);
	if (pWnd)
		pWnd->SetWindowText(CFidasUtilities::GetString(IDS_CANCEL));


	// TODO: Add extra initialization here
	const CDpyType_R *polType = static_cast<const CDpyType_R *>(pomFieldDesc->DpyType());
	ASSERT(polType);
	
	const CFieldRef *polRef = pomFieldDesc->FieldRef();
	ASSERT(polRef);

	CFidasUtilities::GetFieldList(CDpyType::R,polRef->GetTableName(),omFieldInfos);
	if (polType->GetFieldName().GetLength())
	{
		for (int i = 0; i < omFieldInfos.size(); i++)
		{
			const CFieldInfo& rolInfo = omFieldInfos.at(i);
			if ((polRef->GetTableName() + "." + rolInfo.csName) == polType->GetFieldName())
			{
				omSecondField = rolInfo.csTranslation;
				imRangeMin	  = 1;
				imRangeMax	  = rolInfo.ciLength;

				if (polType->GetStart() == 0 || polType->GetEnd() == 0)
					bmRange = FALSE;
				else
					bmRange = TRUE;

				if (polType->GetStart() > 0)
					imRangeStart = polType->GetStart();
				else
					imRangeStart = imRangeMin;

				if (polType->GetEnd() > 0)
					imRangeEnd = polType->GetEnd();
				else
					imRangeEnd = imRangeMax;
				
				UpdateData(FALSE);
				CWnd *polWnd = GetDlgItem(IDC_UFIS_FIELDDEF_RANGE_CHECK);
				if (polWnd)
					polWnd->EnableWindow(TRUE);
				OnUfisFielddefRangeCheck();
				break;
			}
		}
				
	}
	else
	{
		CWnd *polWnd = GetDlgItem(IDC_UFIS_FIELDDEF_RANGE_CHECK);
		if (polWnd)
		{
			polWnd->EnableWindow(FALSE);
		}
	}

	if (polType->GetSeparator().GetLength())
		omSeparator = polType->GetSeparator();


	UpdateData(FALSE);

	OnUfisFielddefRangeCheck();


	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CFidasFieldContentDialog::OnUfisFielddefCancelButton() 
{
	// TODO: Add your control notification handler code here
	EndDialog(IDCANCEL);
	
}

void CFidasFieldContentDialog::OnUfisFielddefOkButton() 
{
	// TODO: Add your control notification handler code here
	if (!UpdateData(TRUE))
		return;

	CDpyType_R *polType = static_cast<CDpyType_R *>(pomFieldDesc->GetDpyType());
	ASSERT(polType);

	// add the table name
	const CFieldRef *polRef = pomFieldDesc->FieldRef();
	ASSERT(polRef);
	polType->SetTableName(polRef->GetTableName());

	polType->SetSeparator(omSeparator);

	if (bmRange)
	{
		polType->SetStart(imRangeStart);
		polType->SetEnd(imRangeEnd);
	}
	else
	{
		polType->SetStart(0);
		polType->SetEnd(0);
	}

	pomFieldDesc->SetDpyType(polType);
	EndDialog(IDOK);
}

void CFidasFieldContentDialog::OnUfisFielddefSelectsecondfieldButton() 
{
	// TODO: Add your control notification handler code here
	CFidasSelectTableFieldDialog dlg(this);
	dlg.SetFieldDescription(pomFieldDesc);
	dlg.SetSecondField(TRUE);
	if (dlg.DoModal() != IDOK)
		return;

	const CDpyType_R *polType = static_cast<const CDpyType_R *>(pomFieldDesc->DpyType());
	ASSERT(polType);

	CString olSecondField = polType->GetFieldName();
	
	if (olSecondField.GetLength())
	{
		for (int i = 0; i < omFieldInfos.size(); i++)
		{
			const CFieldInfo& rolInfo = omFieldInfos.at(i);
			if (rolInfo.csName == olSecondField)
			{
				omSecondField = rolInfo.csTranslation;
				imRangeStart  = 1;
				imRangeEnd	  = rolInfo.ciLength;
				imRangeMin	  = 1;
				imRangeMax	  = rolInfo.ciLength;
				
				UpdateData(FALSE);
				CWnd *polWnd = GetDlgItem(IDC_UFIS_FIELDDEF_RANGE_CHECK);
				if (polWnd)
					polWnd->EnableWindow(TRUE);
				OnUfisFielddefRangeCheck();
				break;
			}
		}
	}
	else
	{
		omSecondField = _T("[second field not defined]");
		imRangeStart = 0;
		imRangeEnd   = 0;
		imRangeMin	 = 0;
		imRangeMax	 = 0;
		bmRange = FALSE;
		UpdateData(FALSE);
		
		CWnd *polWnd = GetDlgItem(IDC_UFIS_FIELDDEF_RANGE_CHECK);
		if (polWnd)
			polWnd->EnableWindow(TRUE);
		OnUfisFielddefRangeCheck();
	}
}

void CFidasFieldContentDialog::OnChangeUfisFielddefSeparatedbyEdit() 
{
	// TODO: If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialog::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.
	
	// TODO: Add your control notification handler code here
	CString olCurrent = omSeparator;
	UpdateData(TRUE);

	if (!CFidasUtilities::IsValidSeparatorString(olCurrent,127))
	{
		TRACE ("due to internal processing, this character is forbidden!\n");
		MessageBox(CFidasUtilities::GetString(IDS_ERROR_INVALID_CHARACTER),CFidasUtilities::GetString(IDS_ERROR_TITLE),MB_ICONHAND + MB_OK);
		omSeparator = olCurrent;
		UpdateData(FALSE);
	}
	
}

void CFidasFieldContentDialog::OnUfisFielddefRangeCheck() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);

	CWnd *polWnd = GetDlgItem(IDC_UFIS_FIELDDEF_EDIT_START);
	if (polWnd)
		polWnd->EnableWindow(bmRange);

	polWnd = GetDlgItem(IDC_UFIS_FIELDDEF_EDIT_END);
	if (polWnd)
		polWnd->EnableWindow(bmRange);
}

void CFidasFieldContentDialog::OnChangeUfisFielddefEditEnd() 
{
	// TODO: If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialog::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.
	
	// TODO: Add your control notification handler code here
}

void CFidasFieldContentDialog::OnChangeUfisFielddefEditStart() 
{
	// TODO: If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialog::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.
	
	// TODO: Add your control notification handler code here
	
}

void CFidasFieldContentDialog::OnKillfocusUfisFielddefEditEnd() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
}

void CFidasFieldContentDialog::OnKillfocusUfisFielddefEditStart() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);	
}
