// MainFrm.cpp : implementation of the CMainFrame class
//

#include <stdafx.h>
#include <Fidas.h>
#include <VersionInfo.h>
#include <FidasUtilities.h>
#include <FidasNewDocumentDialog.h>
#include <FidasDoc.h>
#include <MainFrm.h>
#include <FidasSelectGroupDialog.h>
#include <CFDDoc.h>
#include <CUfisControlDB.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMainFrame

IMPLEMENT_DYNAMIC(CMainFrame, CMDIFrameWnd)

BEGIN_MESSAGE_MAP(CMainFrame, CMDIFrameWnd)
	//{{AFX_MSG_MAP(CMainFrame)
	ON_WM_CREATE()
	ON_WM_ACTIVATE()
	ON_WM_DESTROY()
	ON_COMMAND(ID_FILE_NEW, OnFileNew)
	ON_COMMAND(ID_FILE_OPEN, OnFileOpen)
	ON_COMMAND(ID_FILE_SAVE, OnFileSave)
	ON_COMMAND(ID_FILE_SAVE_AS, OnFileSaveAs)
	ON_UPDATE_COMMAND_UI(ID_INSERT_FIELDDEF, OnUpdateInsertFielddef)
	ON_UPDATE_COMMAND_UI(ID_PROPERTIES, OnUpdateProperties)
	ON_COMMAND(ID_EXPORT_TABLES, OnExportTables)
	ON_UPDATE_COMMAND_UI(ID_EXPORT_TABLES, OnUpdateExportTables)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

BEGIN_EVENTSINK_MAP(CMainFrame, CMDIFrameWnd)
	ON_EVENT(CMainFrame,ID_LOGINCONTROL,1,OnLoginResult,VTS_BSTR)
END_EVENTSINK_MAP()

static UINT indicators[] =
{
	ID_SEPARATOR,           // status line indicator
	ID_INDICATOR_CAPS,
	ID_INDICATOR_NUM,
	ID_INDICATOR_SCRL,
};

/////////////////////////////////////////////////////////////////////////////
// CMainFrame construction/destruction

CMainFrame::CMainFrame()
{
	// TODO: add member initialization code here
	bmLoggedIn = FALSE;
	pomUfisDatabase = NULL;
}

CMainFrame::~CMainFrame()
{
	delete pomUfisDatabase;
}

int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CMDIFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	if (!m_wndToolBar.CreateEx(this, TBSTYLE_FLAT, WS_CHILD | WS_VISIBLE | CBRS_TOP
		| CBRS_GRIPPER | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC) ||
		!m_wndToolBar.LoadToolBar(IDR_MAINFRAME))
	{
		TRACE0("Failed to create toolbar\n");
		return -1;      // fail to create
	}

	if (!m_wndStatusBar.Create(this) ||
		!m_wndStatusBar.SetIndicators(indicators,
		  sizeof(indicators)/sizeof(UINT)))
	{
		TRACE0("Failed to create status bar\n");
		return -1;      // fail to create
	}

	// TODO: Delete these three lines if you don't want the toolbar to
	//  be dockable
	m_wndToolBar.EnableDocking(CBRS_ALIGN_ANY);
	EnableDocking(CBRS_ALIGN_ANY);
	DockControlBar(&m_wndToolBar);

	// create UFIS COM control window
	BOOL blCreated = omUfisCom.Create(NULL,WS_DISABLED,CRect(0,0,100,100),this,ID_UFISCOM);
	ASSERT(blCreated);

	// create UFIS Login control window
	blCreated = omLoginCtrl.Create(NULL,WS_VISIBLE,CRect(0,0,100,100),this,ID_LOGINCONTROL);
	ASSERT(blCreated);


	omLoginCtrl.SetApplicationName(CFidasUtilities::ApplicationName());
	omLoginCtrl.SetRegisterApplicationString("FIDAS,InitModu,InitModu,InitModu");

	VersionInfo rlInfo;
	VersionInfo::GetVersionInfo(AfxGetInstanceHandle(),rlInfo);
	CString olVersionString;
	olVersionString.Format("Version: %s %s  Compiled: %s",rlInfo.omFileVersion,rlInfo.omSpecialBuild,__DATE__);
	omLoginCtrl.SetVersionString(olVersionString);

//	IDispatch *pIDispatch = omUfisCom.GetIDispatch(TRUE);
	IUnknown *pIUnk = omUfisCom.GetControlUnknown();
	IDispatch *pIDispatch;
	HRESULT res = pIUnk->QueryInterface(IID_IDispatch,(void **)&pIDispatch);
	omLoginCtrl.SetUfisComCtrl(pIDispatch);

	CString olRetStr = omLoginCtrl.ShowLoginDialog();

	// check, if we are logged in
	if (olRetStr.Find("OK") == -1)
		return -1;

	BOOL blOK = omUfisCom.SetCedaPerameters(omLoginCtrl.GetUserName_(),CFidasUtilities::HomeAirport(),CFidasUtilities::TableExtension());
	VERIFY(blOK);

	blOK = omUfisCom.InitCom(CFidasUtilities::HostName(),"CEDA");
	VERIFY(blOK);

	// create ufis database access
	pomUfisDatabase = new CUfisControlDB(&omUfisCom);
	ASSERT(pomUfisDatabase);

	CFieldDefDoc::SetDatabase(*pomUfisDatabase);

	return 0;
}

BOOL CMainFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	if( !CMDIFrameWnd::PreCreateWindow(cs) )
		return FALSE;
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////
// CMainFrame diagnostics

#ifdef _DEBUG
void CMainFrame::AssertValid() const
{
	CMDIFrameWnd::AssertValid();
}

void CMainFrame::Dump(CDumpContext& dc) const
{
	CMDIFrameWnd::Dump(dc);
}

#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CMainFrame message handlers


void CMainFrame::OnActivate(UINT nState, CWnd* pWndOther, BOOL bMinimized) 
{
	CMDIFrameWnd::OnActivate(nState, pWndOther, bMinimized);
	
	// TODO: Add your message handler code here
}

void CMainFrame::OnDestroy() 
{
	if (bmLoggedIn)
	{
		BOOL blOK = omUfisCom.CleanupCom();	
		VERIFY(blOK);
	}

	CMDIFrameWnd::OnDestroy();
}

BOOL CMainFrame::OnLoginResult(const char *pszResult)
{
	ASSERT(pszResult);
	CString olResult(pszResult);
	if (olResult.CompareNoCase("CANCEL") == 0)
		bmLoggedIn = FALSE;
	else if (olResult.CompareNoCase("OK") == 0)
		bmLoggedIn = TRUE;
	else
		bmLoggedIn = FALSE;
	return TRUE;		
}

void CMainFrame::OnFileNew() 
{
	// TODO: Add your command handler code here
	CWinApp *pApp = AfxGetApp();
	ASSERT(pApp);

	POSITION pos = pApp->GetFirstDocTemplatePosition();
	while(pos)
	{
		CDocTemplate *pDocTemplate =  pApp->GetNextDocTemplate(pos);
		if (pDocTemplate)
		{
			CDocument *pDoc = pDocTemplate->CreateNewDocument();
			ASSERT(pDoc);
			CFrameWnd *pFrameWnd = pDocTemplate->CreateNewFrame(pDoc,NULL);
			ASSERT(pFrameWnd);
			if (pDoc->GetDocTemplate() == NULL)
				pDocTemplate->AddDocument(pDoc);
			pDocTemplate->InitialUpdateFrame(pFrameWnd,pDoc,TRUE);
			return;
		}
	}
}

void CMainFrame::OnFileOpen() 
{
	// TODO: Add your command handler code here
	CFidasSelectGroupDialog	dlg(this);
	CStringVector olGroupList;
	CFieldDefDoc::GetDocList(olGroupList);
	dlg.SetGroupList(olGroupList);
	if (dlg.DoModal() != IDOK)
		return;

	CWinApp *pApp = AfxGetApp();
	ASSERT(pApp);

	POSITION pos = pApp->GetFirstDocTemplatePosition();
	while(pos)
	{
		CDocTemplate *pDocTemplate =  pApp->GetNextDocTemplate(pos);
		if (pDocTemplate)
		{
			CDocument *pDoc = new CFidasDoc(dlg.GetSelectedGroup());
			ASSERT(pDoc);
			CFrameWnd *pFrameWnd = pDocTemplate->CreateNewFrame(pDoc,NULL);
			ASSERT(pFrameWnd);
			if (pDoc->GetDocTemplate() == NULL)
				pDocTemplate->AddDocument(pDoc);
			pDocTemplate->InitialUpdateFrame(pFrameWnd,pDoc,TRUE);
			return;
		}
	}
}

void CMainFrame::OnFileSave() 
{
	// TODO: Add your command handler code here
	
}

void CMainFrame::OnFileSaveAs() 
{
	// TODO: Add your command handler code here
	
}

void CMainFrame::OnUpdateInsertFielddef(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	ASSERT(pCmdUI);	
	pCmdUI->Enable(this->MDIGetActive(FALSE) != NULL);
}

void CMainFrame::OnUpdateProperties(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	ASSERT(pCmdUI);	
	pCmdUI->Enable(this->MDIGetActive(FALSE) != NULL);
	
}

void CMainFrame::OnExportTables() 
{
	// TODO: Add your command handler code here
	ASSERT(pomUfisDatabase);
	pomUfisDatabase->PEDTableProc();
}

void CMainFrame::OnUpdateExportTables(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	ASSERT(pCmdUI);
	pCmdUI->Enable(bmLoggedIn);
}
