// FidasFieldTypesDialog.cpp : implementation file
//

#include <stdafx.h>
#include <fidas.h>
#include <FidasFieldTypesDialog.h>
#include <FidasUtilities.h>
#include <CDpyType.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CFidasFieldTypesDialog dialog


CFidasFieldTypesDialog::CFidasFieldTypesDialog(CWnd* pParent /*=NULL*/)
	: CDialog(CFidasFieldTypesDialog::IDD, pParent)
{
	EnableAutomation();

	//{{AFX_DATA_INIT(CFidasFieldTypesDialog)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CFidasFieldTypesDialog::OnFinalRelease()
{
	// When the last reference for an automation object is released
	// OnFinalRelease is called.  The base class will automatically
	// deletes the object.  Add additional cleanup required for your
	// object before calling the base class.

	CDialog::OnFinalRelease();
}

void CFidasFieldTypesDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CFidasFieldTypesDialog)
	DDX_Control(pDX, IDC_LIST_FIELD_TYPES, m_ListFieldTypes);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CFidasFieldTypesDialog, CDialog)
	//{{AFX_MSG_MAP(CFidasFieldTypesDialog)
	ON_LBN_SELCHANGE(IDC_LIST_FIELD_TYPES, OnSelchangeListFieldTypes)
	ON_LBN_DBLCLK(IDC_LIST_FIELD_TYPES, OnDblclkListFieldTypes)
	ON_LBN_SELCANCEL(IDC_LIST_FIELD_TYPES, OnSelcancelListFieldTypes)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

BEGIN_DISPATCH_MAP(CFidasFieldTypesDialog, CDialog)
	//{{AFX_DISPATCH_MAP(CFidasFieldTypesDialog)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_DISPATCH_MAP
END_DISPATCH_MAP()

// Note: we add support for IID_IFidasFieldTypesDialog to support typesafe binding
//  from VBA.  This IID must match the GUID that is attached to the 
//  dispinterface in the .ODL file.

// {50809606-5E26-11D5-812B-00010215BFDE}
static const IID IID_IFidasFieldTypesDialog =
{ 0x50809606, 0x5e26, 0x11d5, { 0x81, 0x2b, 0x0, 0x1, 0x2, 0x15, 0xbf, 0xde } };

BEGIN_INTERFACE_MAP(CFidasFieldTypesDialog, CDialog)
	INTERFACE_PART(CFidasFieldTypesDialog, IID_IFidasFieldTypesDialog, Dispatch)
END_INTERFACE_MAP()

CDpyType::Type CFidasFieldTypesDialog::GetSelectedType() const
{
	return emSelectedType;
}

void CFidasFieldTypesDialog::SetSelectedType(const CDpyType::Type& repType)
{
	emSelectedType = repType;
}

/////////////////////////////////////////////////////////////////////////////
// CFidasFieldTypesDialog message handlers

BOOL CFidasFieldTypesDialog::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here

	// localization
	SetWindowText(CFidasUtilities::GetString(IDS_FIELD_TYPES));

	CWnd *pWnd = GetDlgItem(IDC_FIELD_TYPES_STATIC);
	if (pWnd)
		pWnd->SetWindowText(CFidasUtilities::GetString(IDS_FIELD_TYPES_STATIC));

	pWnd = GetDlgItem(IDOK);
	if (pWnd)
		pWnd->SetWindowText(CFidasUtilities::GetString(IDS_OK));
	
	pWnd = GetDlgItem(IDCANCEL);
	if (pWnd)
		pWnd->SetWindowText(CFidasUtilities::GetString(IDS_CANCEL));

	// initialize members
	int ind = m_ListFieldTypes.AddString(CFidasUtilities::GetString(CDpyType::N));
	m_ListFieldTypes.SetItemData(ind,CDpyType::N);
	ind = m_ListFieldTypes.AddString(CFidasUtilities::GetString(CDpyType::M));
	m_ListFieldTypes.SetItemData(ind,CDpyType::M);
	ind = m_ListFieldTypes.AddString(CFidasUtilities::GetString(CDpyType::J));
	m_ListFieldTypes.SetItemData(ind,CDpyType::J);

//	ind = m_ListFieldTypes.AddString(CFidasUtilities::GetString(CDpyType::S));
//	m_ListFieldTypes.SetItemData(ind,CDpyType::S);

	ind = m_ListFieldTypes.AddString(CFidasUtilities::GetString(CDpyType::L));
	m_ListFieldTypes.SetItemData(ind,CDpyType::L);
	ind = m_ListFieldTypes.AddString(CFidasUtilities::GetString(CDpyType::LJ));
	m_ListFieldTypes.SetItemData(ind,CDpyType::LJ);

	ind = m_ListFieldTypes.AddString(CFidasUtilities::GetString(CDpyType::T));
	m_ListFieldTypes.SetItemData(ind,CDpyType::T);
//	ind = m_ListFieldTypes.AddString(CFidasUtilities::GetString(CDpyType::D));
//	m_ListFieldTypes.SetItemData(ind,CDpyType::D);
//	ind = m_ListFieldTypes.AddString(CFidasUtilities::GetString(CDpyType::DT));
//	m_ListFieldTypes.SetItemData(ind,CDpyType::DT);
//	ind = m_ListFieldTypes.AddString(CFidasUtilities::GetString(CDpyType::CT));
//	m_ListFieldTypes.SetItemData(ind,CDpyType::CT);
//	ind = m_ListFieldTypes.AddString(CFidasUtilities::GetString(CDpyType::CD));
//	m_ListFieldTypes.SetItemData(ind,CDpyType::CD);
//	ind = m_ListFieldTypes.AddString(CFidasUtilities::GetString(CDpyType::CDT));
//	m_ListFieldTypes.SetItemData(ind,CDpyType::CDT);
	ind = m_ListFieldTypes.AddString(CFidasUtilities::GetString(CDpyType::TT));
	m_ListFieldTypes.SetItemData(ind,CDpyType::TT);
//	ind = m_ListFieldTypes.AddString(CFidasUtilities::GetString(CDpyType::TD));
//	m_ListFieldTypes.SetItemData(ind,CDpyType::TD);
//	ind = m_ListFieldTypes.AddString(CFidasUtilities::GetString(CDpyType::TDT));
//	m_ListFieldTypes.SetItemData(ind,CDpyType::TDT);
	ind = m_ListFieldTypes.AddString(CFidasUtilities::GetString(CDpyType::R));
	m_ListFieldTypes.SetItemData(ind,CDpyType::R);
	ind = m_ListFieldTypes.AddString(CFidasUtilities::GetString(CDpyType::B));
	m_ListFieldTypes.SetItemData(ind,CDpyType::B);
	ind = m_ListFieldTypes.AddString(CFidasUtilities::GetString(CDpyType::A));
	m_ListFieldTypes.SetItemData(ind,CDpyType::A);
	ind = m_ListFieldTypes.AddString(CFidasUtilities::GetString(CDpyType::AL));
	m_ListFieldTypes.SetItemData(ind,CDpyType::AL);
//	ind = m_ListFieldTypes.AddString(CFidasUtilities::GetString(CDpyType::M));
//	m_ListFieldTypes.SetItemData(ind,CDpyType::M);

	m_ListFieldTypes.SetCurSel(0);
	for (int i = 0; i < m_ListFieldTypes.GetCount(); i++)
	{
		if (m_ListFieldTypes.GetItemData(i) == emSelectedType)
		{
			m_ListFieldTypes.SetCurSel(i);
			break;
		}
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CFidasFieldTypesDialog::OnSelchangeListFieldTypes() 
{
	// TODO: Add your control notification handler code here
	
}

void CFidasFieldTypesDialog::OnDblclkListFieldTypes() 
{
	// TODO: Add your control notification handler code here
	int ind = m_ListFieldTypes.GetCurSel();
	if (ind < 0)
		return;
	emSelectedType = (CDpyType::Type)m_ListFieldTypes.GetItemData(ind);
	CDialog::EndDialog(IDOK);
}

void CFidasFieldTypesDialog::OnSelcancelListFieldTypes() 
{
	// TODO: Add your control notification handler code here
	
}

void CFidasFieldTypesDialog::OnOK() 
{
	// TODO: Add extra validation here
	int ind = m_ListFieldTypes.GetCurSel();
	if (ind < 0)
		return;
	emSelectedType = (CDpyType::Type)m_ListFieldTypes.GetItemData(ind);
	
	CDialog::OnOK();
}

void CFidasFieldTypesDialog::OnCancel() 
{
	// TODO: Add extra cleanup here
	
	CDialog::OnCancel();
}
