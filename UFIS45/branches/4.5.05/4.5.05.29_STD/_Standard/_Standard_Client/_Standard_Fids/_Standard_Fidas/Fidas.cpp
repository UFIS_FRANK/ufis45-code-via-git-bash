// Fidas.cpp : Defines the class behaviors for the application.
//

#include <stdafx.h>
#include <Fidas.h>

#include <MainFrm.h>
#include <ChildFrm.h>
#include <FidasDoc.h>
#include <FidasView.h>
#include <FidasTabView.h>
#include <FidasAirlineDialog.h>
#include <FidasUtilities.h>
#include <VersionInfo.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CFidasApp

BEGIN_MESSAGE_MAP(CFidasApp, CWinApp)
	//{{AFX_MSG_MAP(CFidasApp)
	ON_COMMAND(ID_APP_ABOUT, OnAppAbout)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
	// Standard file based document commands
//	ON_COMMAND(ID_FILE_NEW, CWinApp::OnFileNew)
//	ON_COMMAND(ID_FILE_OPEN, CWinApp::OnFileOpen)
	// Standard print setup command
	ON_COMMAND(ID_FILE_PRINT_SETUP, CWinApp::OnFilePrintSetup)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CFidasApp construction

CFidasApp::CFidasApp()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}

/////////////////////////////////////////////////////////////////////////////
// The one and only CFidasApp object

CFidasApp theApp;

// This identifier was generated to be statistically unique for your app.
// You may change it if you prefer to choose a specific identifier.

// {7B1DCED3-54EB-11D5-811D-00010215BFDE}
static const CLSID clsid =
{ 0x7b1dced3, 0x54eb, 0x11d5, { 0x81, 0x1d, 0x0, 0x1, 0x2, 0x15, 0xbf, 0xde } };

/////////////////////////////////////////////////////////////////////////////
// CFidasApp initialization

BOOL CFidasApp::InitInstance()
{
	// Initialize OLE libraries
	if (!AfxOleInit())
	{
		AfxMessageBox(IDP_OLE_INIT_FAILED);
		return FALSE;
	}

	AfxEnableControlContainer();

	// Standard initialization
	// If you are not using these features and wish to reduce the size
	//  of your final executable, you should remove from the following
	//  the specific initialization routines you do not need.

#ifdef _AFXDLL
	Enable3dControls();			// Call this when using MFC in a shared DLL
#else
	Enable3dControlsStatic();	// Call this when linking to MFC statically
#endif

	// Change the registry key under which our settings are stored.
	// TODO: You should modify this string to be something appropriate
	// such as the name of your company or organization.
	SetRegistryKey(_T("Local AppWizard-Generated Applications"));

	LoadStdProfileSettings();  // Load standard INI file options (including MRU)

	// Register the application's document templates.  Document templates
	//  serve as the connection between documents, frame windows and views.

	CMultiDocTemplate* pDocTemplate;
	pDocTemplate = new CMultiDocTemplate(
		IDR_FidasTYPE,
		RUNTIME_CLASS(CFidasDoc),
		RUNTIME_CLASS(CChildFrame), // custom MDI child frame
		RUNTIME_CLASS(CFidasView));
	AddDocTemplate(pDocTemplate);

	// Connect the COleTemplateServer to the document template.
	//  The COleTemplateServer creates new documents on behalf
	//  of requesting OLE containers by using information
	//  specified in the document template.
	m_server.ConnectTemplate(clsid, pDocTemplate, FALSE);

	// Register all OLE server factories as running.  This enables the
	//  OLE libraries to create objects from other applications.
	COleTemplateServer::RegisterAll();
		// Note: MDI applications register all server objects without regard
		//  to the /Embedding or /Automation on the command line.

	// create main MDI Frame window
	CMainFrame* pMainFrame = new CMainFrame;
	if (!pMainFrame->LoadFrame(IDR_MAINFRAME))
		return FALSE;
	m_pMainWnd = pMainFrame;

	// Parse command line for standard shell commands, DDE, file open
	CCommandLineInfo cmdInfo;
	ParseCommandLine(cmdInfo);

	// Check to see if launched as OLE server
	if (cmdInfo.m_bRunEmbedded || cmdInfo.m_bRunAutomated)
	{
		// Application was run with /Embedding or /Automation.  Don't show the
		//  main window in this case.
		return TRUE;
	}

	// When a server application is launched stand-alone, it is a good idea
	//  to update the system registry in case it has been damaged.
	m_server.UpdateRegistry(OAT_DISPATCH_OBJECT);
	COleObjectFactory::UpdateRegistryAll();

	// Dispatch commands specified on the command line
	if (!ProcessShellCommand(cmdInfo))
		return FALSE;

	// The main window has been initialized, so show and update it.
	pMainFrame->ShowWindow(m_nCmdShow);
	pMainFrame->UpdateWindow();

	return TRUE;
}


/////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	CListBox	m_InfoList;
	CString	m_Version;
	CString	m_Server;
	CString	m_UfisVersion;
	CString	m_User;
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAboutDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	//{{AFX_MSG(CAboutDlg)
		// No message handlers
	virtual BOOL OnInitDialog();
	afx_msg void OnDebugInfoLog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	CStringArray *pomInfoList;
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	m_Version = _T("");
	m_Server = _T("");
	m_UfisVersion = _T("");
	m_User = _T("");
	//}}AFX_DATA_INIT

	pomInfoList = NULL;
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	DDX_Control(pDX, IDC_INFOLIST, m_InfoList);
	DDX_Text(pDX, IDC_VERSION, m_Version);
	DDX_Text(pDX, IDC_SERVER, m_Server);
	DDX_Text(pDX, IDC_UFISVERSION, m_UfisVersion);
	DDX_Text(pDX, IDC_USER, m_User);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
		// No message handlers
	ON_BN_CLICKED(ID_DEBUG_INFO_LOG, OnDebugInfoLog)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


BOOL CAboutDlg::OnInitDialog() 
{
	SetWindowText(CFidasUtilities::GetString(IDS_ABOUTBOX)); 

	CWnd *polWnd = GetDlgItem(IDC_INFO); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(CFidasUtilities::GetString(IDS_ABOUTBOX_INFO));
	}
	polWnd = GetDlgItem(IDC_UFISVERSION);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(CFidasUtilities::GetString(IDS_ABOUTBOX_UFISVERSION));
	}
	polWnd = GetDlgItem(IDC_VERSION);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(CFidasUtilities::GetString(IDS_ABOUTBOX_VERSION));
	}
	polWnd = GetDlgItem(IDC_COPYRIGHT);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(CFidasUtilities::GetString(IDS_ABOUTBOX_COPYRIGHT));
	}
	polWnd = GetDlgItem(IDC_COPYRIGHT2);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(CFidasUtilities::GetString(IDS_ABOUTBOX_COPYRIGHT2));
	}
	polWnd = GetDlgItem(IDOK);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(CFidasUtilities::GetString(IDS_ABOUTBOX_OK));
	}
	polWnd = GetDlgItem(IDC_SERVERTITLE);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(CFidasUtilities::GetString(IDS_ABOUTBOX_SERVERTITLE));
	}
	polWnd = GetDlgItem(IDC_SERVER);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(CFidasUtilities::GetString(IDS_ABOUTBOX_SERVER));
	}
	polWnd = GetDlgItem(IDC_USERTITLE);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(CFidasUtilities::GetString(IDS_ABOUTBOX_USERTITLE));
	}
	polWnd = GetDlgItem(IDC_USER);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(CFidasUtilities::GetString(IDS_ABOUTBOX_USER));
	}

	if(CFidasUtilities::DisplayDebugInfo())
	{
		CListBox *polListBox = static_cast<CListBox *>(GetDlgItem(IDC_INFOLIST));
		if (polListBox)
		{
			polListBox->SetHorizontalExtent(5000);
			CFidasUtilities::GetDebugInfo(*polListBox);
		}

		polWnd = GetDlgItem(ID_DEBUG_INFO_LOG);
		if(polWnd != NULL)
		{
			polWnd->ShowWindow(SW_HIDE);
		}

	}
	else
	{
		polWnd = GetDlgItem(ID_DEBUG_INFO_LOG);
		if(polWnd != NULL)
		{
			polWnd->ShowWindow(SW_HIDE);
		}

		polWnd = GetDlgItem(IDC_INFOLIST);
		if (polWnd != NULL)
		{
			CRect olLowerRect;
			polWnd->GetWindowRect(olLowerRect);
			polWnd->ShowWindow(SW_HIDE);

			CRect olRect;
			this->GetWindowRect(olRect);
			CPoint& olPoint = olRect.BottomRight();
			olPoint.y = olLowerRect.TopLeft().y;

			this->SetWindowPos(NULL,olRect.TopLeft().x,olRect.TopLeft().y,olRect.Width(),olRect.Height(),SWP_NOMOVE|SWP_NOOWNERZORDER|SWP_NOZORDER);
		}
	}

	VersionInfo rlInfo;
	VersionInfo::GetVersionInfo(AfxGetInstanceHandle(),rlInfo);

	m_UfisVersion   = rlInfo.omProductVersion;

	m_Version.Format("Version: %s %s  Compiled: %s",rlInfo.omFileVersion,rlInfo.omSpecialBuild,__DATE__);

	CMainFrame *pMainFrm = (CMainFrame *)AfxGetMainWnd();
	ASSERT(pMainFrm);
	m_User = pMainFrm->omUfisCom.GetUserName() + '/' + pMainFrm->omUfisCom.GetWorkstationName();
	m_Server = pMainFrm->omUfisCom.GetServerName();

	CDialog::OnInitDialog();
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CAboutDlg::OnDebugInfoLog() 
{
}

// App command to run the dialog
void CFidasApp::OnAppAbout()
{
	CAboutDlg aboutDlg;
	aboutDlg.DoModal();
}

/////////////////////////////////////////////////////////////////////////////
// CFidasApp message handlers

