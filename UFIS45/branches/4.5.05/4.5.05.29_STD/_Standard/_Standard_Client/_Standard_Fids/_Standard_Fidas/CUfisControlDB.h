
/*
 * starter version FIDAS UFIS DB access class declarations
 * j heilig June 7 01
 */

#ifndef CUfisControlDB_included
#define CUfisControlDB_included

/*
 * To encapsulate the database access details, the class CUfisDB is defined
 * to provide the required methods. An instance of it must be created and
 * passed to the document manager before starting the work with FIDAS
 * documents. 
 *
 * The class CPEDRec is defined to pass record data.
 */

#include <CUfisDB.h> 
#include <FidasTableFields.h>

class CUfisCom;
class CUfisControlDB : public CUfisDB
{
public:
	CUfisControlDB(CUfisCom *popUfisCom);
	virtual	~	CUfisControlDB();
	virtual int GetColumns(const char *table, CStringVector &fields,CStringVector& translated) const;
	virtual int GetPEDTable(std::vector<CPEDRec> &dst);
	virtual int UpdatePEDRec(const CPEDRec &); 
	virtual int InsertPEDRec(const CPEDRec &); 
	virtual int DeletePEDRec(const CPEDRec &); 
	virtual CString InFilterSep(const char *);
	virtual CString OutFilterSep(const char *);
	virtual	int	GetFieldInfos(const char *table,std::vector<CFieldInfo>& dst);
	virtual	int PEDTableProc(int ind);
protected:
	CUfisControlDB();
private:
	// DB access method specific parts
	CUfisCom*			pomUfisCom;
	CStringVector		omUrnos;
	CFidasTableFields	omFDDFields;
	CFidasTableFields	omFLVFields;
	CFidasTableFields	omFIDFields;
	CFidasTableFields	omAPTFields;
	CFidasTableFields	omDEVFields;/*added by JHI 05062002*/
	CFidasTableFields	omFDVFields;/*added by JHI 27072002*/
};


#endif // CUfisDB_included

