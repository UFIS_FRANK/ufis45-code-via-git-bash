/*
 * AAB/AAT UFIS Display Handler
 *
 * FIDAS display type definitions -- j. heilig June 14 2001
 *
 * Jun 22 01 released to test (jhe) 
 *
 */
  
#include <CDpyType.h>

static const char *szIDEL= " \t,";  // std delimiters
static const char *szLDEL= " \t";   // std delimiters at beginning/end of line
static const char *szNDEL = "0123456789"; // std delimiters expecting digits

CDpyType *CDpyType::Create(char *p)
{
CDpyType *res = NULL;
istrstream is(p);

	CTokenZ tzDpyType(is, szIDEL, szLDEL);
	if(tzDpyType.Length() == 0) // could set good flag here 
		return NULL;

	if(tzDpyType == "N")	/* normal text -- data */
	{
		res = new CDpyType_N();
	}
	else if(tzDpyType == "M")	/* normal text without BLANK -- data	*/
	{
		res = new CDpyType_M();
	}
	else if(tzDpyType == "J")	/* flight number with code shares -- data	*/
	{
		res = new CDpyType_J();
	}
	else if(tzDpyType == "S")	/* static text (not yet supported?) */
	{
		CDpyType_S *s = new CDpyType_S();
		res = s;
		CTokenZ tzT(is, szLDEL, szIDEL);
		s->SetText(CString(tzT.String()));
	}
	else if(tzDpyType == "L")
	{
		CDpyType_L *l = new CDpyType_L();
		res = l;
		CTokenZ tz(is, szLDEL, szIDEL);
		l->SetExtension(CString(tz.String()));
	}
	else if(tzDpyType == "LJ")
	{
		CDpyType_LJ *l = new CDpyType_LJ();
		res = l;
		CTokenZ tz(is, szLDEL, szIDEL);
		l->SetExtension(CString(tz.String()));
	}
	else if(tzDpyType == "T")
	{
		CDpyType_TimeDate *t = new CDpyType_T();
		res = t;
		CTokenZ tz(is, szLDEL, szIDEL);
		t->SetFormat(CString(tz.String()));
	}
	else if(tzDpyType == "D")
	{
		CDpyType_TimeDate *t = new CDpyType_D();
		res = t;
		CTokenZ tz(is, szLDEL, szIDEL);
		t->SetFormat(CString(tz.String()));
	}
	else if(tzDpyType == "DT")
	{
		CDpyType_TimeDate *t = new CDpyType_DT();
		res = t;
		CTokenZ tz(is, szLDEL, szIDEL);
		t->SetFormat(CString(tz.String()));
	}
	else if(tzDpyType == "CT")
	{
		CDpyType_TimeDate *t = new CDpyType_CT();
		res = t;
		CTokenZ tz(is, szLDEL, szIDEL);
		t->SetFormat(CString(tz.String()));
	}
	else if(tzDpyType == "CD")
	{
		CDpyType_TimeDate *t = new CDpyType_CD();
		res = t;
		CTokenZ tz(is, szLDEL, szIDEL);
		t->SetFormat(CString(tz.String()));
	}
	else if(tzDpyType == "CDT")
	{
		CDpyType_TimeDate *t = new CDpyType_CDT();
		res = t;
		CTokenZ tz(is, szLDEL, szIDEL);
		t->SetFormat(CString(tz.String()));
	}
	else if(tzDpyType == "TT")
	{
		CDpyType_TimeDate *t = new CDpyType_TT();
		res = t;
		CTokenZ tz(is, szLDEL, szIDEL);
		t->SetFormat(CString(tz.String()));
	}
	else if(tzDpyType == "TD")
	{
		CDpyType_TimeDate *t = new CDpyType_TD();
		res = t;
		CTokenZ tz(is, szLDEL, szIDEL);
		t->SetFormat(CString(tz.String()));
	}
	else if(tzDpyType == "TDT")
	{
		CDpyType_TimeDate *t = new CDpyType_TDT();
		res = t;
		CTokenZ tz(is, szLDEL, szIDEL);
		t->SetFormat(CString(tz.String()));
	}
	else if(tzDpyType == "R")
	{
		CDpyType_R *r = new CDpyType_R();
		res = r;
		CTokenZ tzS(is, ",", szIDEL);
		r->SetSeparator(CString(tzS.String()));
		CTokenZ tzF(is, "(", szIDEL);
		r->SetFieldName(CString(tzF.String()));
		if(is.peek() == '(')
		{
			int s, e;
			char c;
			is >> c >> s >> c >> e;
			if (!is.fail())
			{
				r->SetStart(s);
				r->SetEnd(e);
			}
		}
	}
	else if(tzDpyType == "B")
	{
		CDpyType_B *b = new CDpyType_B();
		res = b;
		CTokenZ tzT(is, szIDEL, szIDEL);
		b->SetRemarkType(CString(tzT.String()));
		CTokenZ tzB(is, szNDEL,  szIDEL);
		int i;
		is >> i;
		if(!is.fail())
			b->SetBlinkC(i);
		CTokenZ tzC(is, szIDEL, szIDEL);
		if(tzC.Length())
			b->SetTimeConversion(*(tzC.String()));
		b->SetFieldNames(CTokenZ::GetStrings(is, ","));
	}
	else if(tzDpyType == "A")
	{
		CDpyType_A *a = new CDpyType_A();
		res = a;
		CTokenZ tzNA(is, szNDEL);
		int i;
		is >> i;
		if(!is.fail())
			a->SetNAirports(i);
		CTokenZ tzSQ(is, szIDEL, szIDEL);
		if(tzSQ.Length())
			a->SetSequence( *(tzSQ.String()));
		CTokenZ tzSP(is, ",", szIDEL);
		a->SetSeparator(tzSP.String());
		a->SetFieldNames(CTokenZ::GetStrings(is, ","));
	}
	else if(tzDpyType == "AL")
	{
		CDpyType_AL *al = new CDpyType_AL();
		res = al;
		CTokenZ tzAL(is, szLDEL, szIDEL);
		al->SetFieldName(CString(tzAL.String()));
	}
	else
	{
			aatError("Bad field display type '" << tzDpyType.String() << "'");
	}
	return res;
}

/*
 * most trivial types implemented inline in header file)
 * some more complex cases implemented here
 */

CDpyType *CDpyType_N::Clone(const CDpyType *p)
{
	switch(GetType())
	{
	case N :
		return new CDpyType_N(*(CDpyType_N *)p);
	break;
	case M :
		return new CDpyType_M(*(CDpyType_M *)p);
	break;
	case J :
		return new CDpyType_J(*(CDpyType_J *)p);
	break;
	default :
		assert(0);
		return NULL;
	}
}

CString CDpyType_N::ToString() const
{
	switch(GetType())
	{
	case N :
		return CString("N");
	break;
	case M :
		return CString("M");
	break;
	case J :
		return CString("J");
	break;
	default :
		assert(0);
		return CString("?");
	}
}

int CDpyType_N::operator == (const CDpyType &a) const
{
	return a.GetType() == GetType();
}


CDpyType *CDpyType_S::Clone(const CDpyType *p)
{
	return new CDpyType_S(*(CDpyType_S *)p);
}

CString CDpyType_S::ToString() const
{
CString ret("S,");
	ret += csText;
	return ret;
}

int CDpyType_S::operator == (const CDpyType &b) const
{

	if(b.GetType() != GetType())
		return 0;
const CDpyType_S *a = (CDpyType_S *)&b;
	if(a->GetText() != GetText())
		return 0;
	return 1;
}

CDpyType *CDpyType_L::Clone(const CDpyType *p)
{
	if (GetType() == L)
		return new CDpyType_L(*(CDpyType_L *)p);
	else
		return new CDpyType_LJ(*(CDpyType_LJ *)p);
}

CString CDpyType_L::ToString() const
{
	CString ret;
	if (GetType() == L)
		ret += "L,";
	else
		ret += "LJ,";

	ret += csExt;
	return ret;
}

int CDpyType_L::operator == (const CDpyType & b) const
{
	if(b.GetType() != GetType())
		return 0;
const CDpyType_L *a = (CDpyType_L *) &b;
	if(a->GetExtension() != GetExtension())
		return 0;
	return 1;
}

CDpyType *CDpyType_TimeDate::Clone(const CDpyType *p)
{
	return new CDpyType_TimeDate(*(CDpyType_TimeDate *)p);
}

CDpyType_TimeDate::CDpyType_TimeDate(const CDpyType_TimeDate &c)
: CDpyType(c.GetType()), csFmt(c.csFmt)
{
}
  
CString CDpyType_TimeDate::ToString() const
{
ostrstream s;
char *p;
	switch(GetType())
	{
		case T:
			p = "T";
			break;
		case D:
			p = "D";
			break;
		case DT:
			p = "DT";
			break;
		case CT:
			p = "CT";
			break;
		case CD:
			p = "CD";
			break;
		case CDT:
			p = "CDT";
			break;
		case TT:
			p = "TT";
			break;
		case TD:
			p = "TD";
			break;
		case TDT:
			p = "TDT";
			break;
		default:
			Assert(0);
	}
	s << p << "," << csFmt << '\0';
	p = s.str();
	CString ret(p);
	delete p;
	return ret;
}

int CDpyType_TimeDate::operator == (const CDpyType & b) const
{

	if(b.GetType() != GetType())
		return 0;
const CDpyType_TimeDate *a = (CDpyType_TimeDate *) &b;
	if(a->GetFormat() != GetFormat())
		return 0;
	return 1;
}

CDpyType_R::CDpyType_R(const CDpyType_R &c)
: CDpyType(R)
, csSep(c.csSep)
, csTableName(c.csTableName)
, csFieldName(c.csFieldName)
, start(c.start)
, end(c.end)
{
}

CDpyType *CDpyType_R::Clone(const CDpyType *p)
{
	return new CDpyType_R(*(CDpyType_R *)p);
}

CString CDpyType_R::ToString() const
{
ostrstream s;
char *p;
	if (csTableName.GetLength() > 0 && strstr(csFieldName, csTableName + ".") == NULL)
		s << "R," << csSep << "," << csTableName << "." << csFieldName;
	else
		s << "R," << csSep << "," << csFieldName;
	if (start > 0 && end >= start)
		s << "(" << start << "," << end << ")";
	s << '\0';
	p = s.str();
	CString ret(p);
	delete p;
	return ret;
}

	
int CDpyType_R::operator == (const CDpyType &b) const
{
	if(b.GetType() != GetType())
		return 0;
const CDpyType_R *a = (CDpyType_R *)&b;
	if(a->GetSeparator() != GetSeparator())
		return 0;
	if(a->GetTableName() != GetTableName())
		return 0;
	if(a->GetFieldName() != GetFieldName())
		return 0;
	if(a->GetStart() != GetStart() || a->GetEnd() != GetEnd())
		return 0;
	return 1;
}

CDpyType_B::CDpyType_B()
: CDpyType(B), iBlinkC(0), cTimeConversion(0), pFNVector(NULL)
{
}

CDpyType *CDpyType_B::Clone(const CDpyType *p)
{
	return new CDpyType_B(*(CDpyType_B *)p);
}

CDpyType_B::CDpyType_B(const CDpyType_B &c)
: CDpyType(B), csType(c.csType), iBlinkC(c.iBlinkC),
  cTimeConversion(c.cTimeConversion),
  pFNVector(NULL)
{
	if(c.pFNVector)
	{
/*****
		std::vector<CString>::iterator it;
		for(it=c.pFNVector->begin(); it!=c.pFNVector->end(); ++it)
		{
			pFNVector->push_back(*it);
		}
*****/
		pFNVector = new CStringVector(* c.pFNVector);
	}
}

CDpyType_B::~CDpyType_B()
{
	delete pFNVector;
}

// caller becomes owner of list
CStringVector *CDpyType_B::GetFieldNames()
{
CStringVector *p;
	p = pFNVector;
	pFNVector = NULL;
	return p;
}

// instance becomes owner of list
void CDpyType_B::SetFieldNames(CStringVector *p)
{
	delete pFNVector;
	pFNVector = p;
}
		
CString CDpyType_B::ToString() const
{
ostrstream s;
char *p;
std::vector<CString>::iterator it;
	s << "B," << csType << "," << iBlinkC << "," << cTimeConversion;
	if(pFNVector)
	{
		for(it=pFNVector->begin(); it!=pFNVector->end(); ++it)
			s << "," << *(it);
	}
	s << '\0';
	p = s.str();
	CString ret(p);
	delete p;
	return ret;
}
	
int CDpyType_B::operator == (const CDpyType &b) const
{
	if(b.GetType() != GetType())
		return 0;
const CDpyType_B *a = (CDpyType_B *) &b;
	if(a->GetRemarkType() != GetRemarkType())
		return 0;
	if(a->GetBlinkC() != GetBlinkC())
		return 0;
	if(a->GetTimeConversion() != GetTimeConversion())
		return 0;
	return StringVecEqual(a->FieldNames(), FieldNames());
}


CDpyType_A::CDpyType_A()
: CDpyType(A), iNAirp(0), cSeq(0), pFNVector(NULL)
{
}

CDpyType *CDpyType_A::Clone(const CDpyType *p)
{
	return new CDpyType_A(*(CDpyType_A *)p);
}

CDpyType_A::CDpyType_A(const CDpyType_A &c)
: CDpyType(A), iNAirp(c.iNAirp), cSeq(c.cSeq), csSep(c.csSep), pFNVector(NULL)
{
	if (c.pFNVector)
	{
/*****
		std::vector<CString>::iterator it;
		for(it=c.pFNVector->begin(); it!=c.pFNVector->end(); ++it)
		{
			pFNVector->push_back(*it);
		}
*****/
		pFNVector = new CStringVector(*c.pFNVector);
	}
}

CDpyType_A::~CDpyType_A()
{
	delete pFNVector;
}

// caller becomes owner of list
CStringVector *CDpyType_A::GetFieldNames()
{
CStringVector *p;
	p = pFNVector;
	pFNVector = NULL;
	return p;
}

// instance becomes owner of list
void CDpyType_A::SetFieldNames(CStringVector *p)
{
	delete pFNVector;
	pFNVector = p;
}
		
CString CDpyType_A::ToString() const
{
ostrstream s;
char *p;
std::vector<CString>::iterator it;
	s << "A," << iNAirp << "," << cSeq << "," << csSep;
	if (pFNVector)
	{
		for(it=pFNVector->begin(); it!=pFNVector->end(); ++it)
			s << "," << *(it);
	}
	s << '\0';
	
	p = s.str();
	CString ret(p);
	delete p;
	return ret;
}

int CDpyType_A::operator == (const CDpyType &b) const
{
	if(b.GetType() != GetType())
		return 0;
const CDpyType_A *a = (CDpyType_A *)&b;
	if(a->GetNAirports() != GetNAirports())
		return 0;
	if(a->GetSequence() != GetSequence())
		return 0;
	if(a->GetSeparator() != GetSeparator())
		return 0;
	return StringVecEqual(a->FieldNames(), FieldNames());
}


CDpyType_AL::CDpyType_AL(const CDpyType_AL &c) 
: CDpyType(AL), csFieldName(c.csFieldName)
{
}
  
CDpyType *CDpyType_AL::Clone(const CDpyType *p)
{
	return new CDpyType_AL(*(CDpyType_AL *)p);
}

CString CDpyType_AL::ToString() const
{
CString ret("AL,");
	ret += csFieldName;
	return ret;
}

int CDpyType_AL::operator == (const CDpyType &b) const
{

	if(b.GetType() != GetType())
		return 0;
const CDpyType_AL *a = (CDpyType_AL *) &b;
	if(a->GetFieldName() != GetFieldName())
		return 0;
	return 1;
}
