/*
 * ABB/AAT tools lib starter version Jun 14 2001 
 * j.heilig
 *
 * Jun 22 01 released to test (jhe)
 *
 * $Log: Ufis/_Standard/_Standard_Client/_Standard_Fids/_Standard_Fidas/aatlib.cpp  $
 * Revision 1.1 2003/03/18 20:59:11SGT fei 
 * Initial revision
 * Member added to project /usr1/projects/UFIS4.5/_Standard_Fidas.pj
 * Revision 1.3 2002/08/14 09:40:49CEST fei 
 * Set additional include path to .\
 * Revision 1.2 2002/07/24 10:33:23GMT+02:00 tho 
 * Revision 1.1 2002/07/05 12:45:49GMT+02:00 fei 
 * Initial revision
 * Revision 1.1 2001/06/22 11:00:08GMT+02:00 tho 
 * Initial revision
 * -----
 * $Id: Ufis/_Standard/_Standard_Client/_Standard_Fids/_Standard_Fidas/aatlib.cpp 1.1 2003/03/18 20:59:11SGT fei Exp  $
 */

#include <aatlib.h>

#ifdef own_string_class

CString::CString() : length(0), buffer(NULL) { }

CString::CString(const char *str) : length(0), buffer(NULL) { set(str); }

CString::CString(const CString &r) : length(0), buffer(NULL) { set(r.buffer); }

CString::~CString() { if (buffer) delete buffer; }

void CString::set(const char * str)
{
	if (buffer)
	{
		if (str && strcmp(str,buffer) == 0)
			return;
		delete buffer;
	}

	if (str)
	{
		length = strlen(str);
		buffer = new char[length + 1];
	}
	else
	{
		buffer = NULL;
	}
	if (buffer)
	{
		memcpy(buffer,str,length + 1);
	}
	else
	{
		length = 0;
	}
	return;
}

void CString::append(const char * str)
{
	int l;
	char * pb;
	if (str == NULL || strlen(str) == 0)
		return;
	l = strlen(str);
	pb = new char[length + l + 1];
	if (pb == NULL)
		return;
	memcpy(pb,buffer,length);
	memcpy(pb+length,str,l+1);
	set(pb);
	delete pb;
}

/*>>>> MSC VS CString problems
ostream& operator << (ostream & os, const CString & rs)
{
	if (rs.buffer)
		os << rs.buffer;
	else
		os << "*null*";
	return os;
}
<<<<<<*/

CString& CString::operator = (const char * str)
{
	set(str);
	return *this;
}

CString& CString::operator = (const CString &rs)
{
	set(rs.buffer);
	return *this;
}

CString CString::operator + (const char * ts) const
{
	CString s(*this);
	s.append(ts);
	return s;
}

CString &CString::operator += (const char * ts) 
{
	append(ts);
	return *this;
}

CString CString::operator + (const CString& rs) const
{
	return *this + rs.buffer;
}

int CString::operator == (const CString& rs) const
{
	return strcmp(buffer,rs.buffer) == 0;
}

int CString::operator == (const char *rs) const
{
	return strcmp(buffer,rs) == 0;
}

int CString::operator != (const CString& rs) const
{
	return strcmp(buffer,rs.buffer) != 0;
}

int CString::operator != (const char *rs) const
{
	return strcmp(buffer,rs) != 0;
}

CString::operator const char* () const
{
	return buffer;
}

#endif // own-string_class

/**********************************************************************
 The CTokenZ class can be used to extract a token from an input stream
 and to access it as a null terminated character string. A set of delimiters
 can be specified. Extraction ends if either one of the delimiter chars or
 EOF is seen on the input stream. The delimiter char is NOT extracted.
**********************************************************************/

/*
 * This contructor is used to extract the token from the input stream
 * specified by <is> and delimited by one of the characters specified 
 * by <delim>.
 */

CTokenZ::CTokenZ(istream &is, const char *delim, const char *skip, int trim)
: buff(NULL), ncont(0), nmax(0)
{
char c;
int dlen, slen, ix, done, ci;
	buff = new char [QUANTUM];
	Assert(buff);
	nmax = QUANTUM;
	buff[0] = '\0';

	dlen = strlen(delim);
	slen = skip ? strlen(skip) : 0;

	for(done=0; !done;)
	{
		if ( (ci = is.peek()) == EOF)
			break;
		c = (char) ci;
// cout << ci << "=" << c << endl;
		for(ix=0; ix < slen; ++ix)
		{
			if (c == skip[ix])
				break;
		}
		if (slen && ix < slen)
		{
			is.ignore(1);
			continue;
		}
      slen = 0;
		for(ix=0; ix < dlen; ++ix)
		{
			if (c == delim[ix])
			{
				done = 1;
				break;
			}	
		}
		if (!done)
		{
			is.ignore(1);
			add_char(c);
		}
	}
	if(trim)
		trim_buff();
}

/*
 * The descructor simply frees memory used by the instance
 */

CTokenZ::~CTokenZ()
{
	delete buff;
}

/*
 * Return the number of characters in the token without the terminating null.
 */

int CTokenZ::Length() const
{
	return ncont;
}

/*
 * Return a pointer (readonly) to the token string.
 */

const char *CTokenZ::String() const
{
	return buff;
}

/*
 * Return a pointer (readonly) to the token string.
 */

CTokenZ::operator const char *() const
{
	return buff;
}
/*
 * Case sensitive compare op to compare token with a string (strcmp logic)
 */

int CTokenZ::operator == (const char *p) const
{
	return strcmp(buff,p) == 0;
}

/*
 * Case sensitive unequal op to compare token with a string (strcmp logic)
 */

int CTokenZ::operator != (const char *p) const
{
	return strcmp(buff,p);
}

CStringVector *CTokenZ::GetStrings(istream &is, const char *pcDel)
{
const char *p;
CStringVector *res;

	res = NULL;
	for(;;)
	{
		CTokenZ tz(is, pcDel, pcDel);
		if(tz.Length() == 0)
			break;
		p = tz.String();
		Assert(p);
		if(res == NULL)
			res = new CStringVector;
		Assert(res);
		res->push_back(CString(p));
	}
	return res;
}

char *CTokenZ::DupZ() 
{
char *p;
	p = new char [ncont+1];
	Assert(p);
	strcpy(p, buff);
	return p;
}

/*
 * private methods
 */

void CTokenZ::add_char(char c)
{
char *np;
	if (ncont >= nmax-1)
	{
		nmax += QUANTUM;
		np = new char [nmax];
		Assert(np);
		memcpy(np,buff,ncont);
		delete buff;
		buff = np;
	}

	buff[ncont++] = c;
	buff[ncont] = '\0';
}

int CTokenZ::trim_buff()
{
char *p;
int ix, nzc;
	if(buff==NULL)
		return 0;
	p = buff;
	while(*p == ' ' || *p == '\t')
		++p;
	for(ix=nzc=0; *p; ++p,++ix)
	{
		buff[ix] = *p;
		if(*p != ' ' && *p != '\t')
			nzc = ix+1;
	}
	buff[nzc] = '\0';
	return nzc;
}

int StringVecEqual(const CStringVector *p1, const CStringVector *p2)
{
// GNU int ix;
unsigned int ix, n;
	if(!p1 && !p2)
		return 1;
	if(p1 && p2)
	{
		n = p1->size();
		if(n != p2->size())
			return 0;
		for(ix=0; ix<n; ++ix)
#ifndef LINUX
			if(p1->at(ix) != p2->at(ix))
#else
			if(p1[ix] != p2[ix])
#endif
				return 0;
	}
	return 1;
}
