#if !defined(AFX_FIDASSELECTTABLEFIELDDIALOG_H__4FE89CE7_6154_11D5_812F_00010215BFDE__INCLUDED_)
#define AFX_FIDASSELECTTABLEFIELDDIALOG_H__4FE89CE7_6154_11D5_812F_00010215BFDE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// FidasSelectTableFieldDialog.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CFidasSelectTableFieldDialog dialog
#include <vector>
class CFieldDesc;
class CFieldInfo;

typedef std::vector<CString> CStringVector;
typedef std::vector<CFieldInfo> CFieldInfoVector;

class CFidasSelectTableFieldDialog : public CDialog
{
// Construction
public:
	CFidasSelectTableFieldDialog(CWnd* pParent = NULL);   // standard constructor
	void SetFieldDescription(CFieldDesc *prpFieldDesc);
	void ShowFieldRange(BOOL bpShow);
	void ShowFieldSize(BOOL bpShow);
	void SetMaxFieldSize(int ipSize,BOOL bpShow = TRUE);
	void SetFieldRange(int ipMin,int ipMax,BOOL bpShow = TRUE);	
	void SetSecondField(BOOL bpSecond = TRUE);	
private:
// Dialog Data
	//{{AFX_DATA(CFidasSelectTableFieldDialog)
	enum { IDD = IDD_UFIS_SELECTTABLEFIELD };
	CButton	m_OK_Button;
	CStatic	m_Table_Static;
	CComboBox	m_Table_Combo;
	CStatic	m_RangeStart_Static;
	CEdit	m_RangeStart_Edit;
	CStatic	m_RangeEnd_Static;
	CEdit	m_RangeEnd_Edit;
	CButton	m_Range_Check;
	CEdit	m_MaxSize_Edit;
	CButton	m_MaxSize_Check;
	CStatic	m_FieldList_Static;
	CComboBox	m_FieldList_Combo;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CFidasSelectTableFieldDialog)
	public:
	virtual void OnFinalRelease();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CFidasSelectTableFieldDialog)
	virtual BOOL OnInitDialog();
	afx_msg void OnUfisSelecttablefieldCancelButton();
	afx_msg void OnUfisSelecttablefieldSelectButton();
	afx_msg void OnUfisSelecttablefieldRangeCheck();
	afx_msg void OnUfisSelecttablefieldMaxsizeCheck();
	afx_msg void OnSelchangeUfisSelecttablefieldTableCombo();
	afx_msg void OnSelchangeUfisSelecttablefieldFieldCombo();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
	// Generated OLE dispatch map functions
	//{{AFX_DISPATCH(CFidasSelectTableFieldDialog)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_DISPATCH
	DECLARE_DISPATCH_MAP()
	DECLARE_INTERFACE_MAP()
private:
	void	UpdateFieldList();	
	friend class CFidasFieldContentDialog;
private:
	CFieldDesc*			pomFieldDesc;
	BOOL				bmSecondField;
	BOOL				bmShowFieldRange;
	BOOL				bmShowFieldSize;
	int					imMaxFieldSize;
	int					imMinFieldRange;
	int					imMaxFieldRange;
	CStringVector		omTableNames;
	CFieldInfoVector	omFieldInfos;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_FIDASSELECTTABLEFIELDDIALOG_H__4FE89CE7_6154_11D5_812F_00010215BFDE__INCLUDED_)
