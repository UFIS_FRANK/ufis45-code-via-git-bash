/*
 * AAB/AAT UFIS Display Handler
 *
 * Declaration of FIDAS document constituting classes -- j. heilig June 15 2001
 *
 * Jun 22 01 released to test (jhe)
 *
 */
  
#ifndef CFDDoc_included
#define CFDDoc_included 

#include <aatlib.h>
#include <CDpyType.h>
#include <CUfisDB.h>
#include <list>
#include <map>

class CFieldRef
{
public:
	CFieldRef();
	CFieldRef(const CFieldRef &);
	~CFieldRef();
	CFieldRef(char *);
	CString ToString() const;
	const CString &GetTableName() const;
	void SetTableName(const CString &);
	const CString &GetFieldName() const;
	void SetFieldName(const CString &);
	int GetStart() const;
	void SetStart(int);
	int GetEnd() const;
	void SetEnd(int);
	int operator == (const CFieldRef &) const;
	int operator != (const CFieldRef &) const;
	CFieldRef &operator=(const CFieldRef &);
private:
	CString csTableName, csFieldName;
	int start, end;
// blocker
};

/*
 * The class CFieldDesc contains one field description consisting of field
 * reference, display type and size. These members are public and can be
 * accessed directly.
*/

class CFieldDesc
{
public:
	CFieldDesc();
	CFieldDesc(const CFieldDesc &);
	~CFieldDesc();
	CFieldDesc(char *);
	CString ToString() const;
	int GetSize() const;
	void SetSize(int);
	const CFieldRef *FieldRef() const; // readonly access
	CFieldRef *GetFieldRef();	 // passes ownership
	void SetFieldRef(CFieldRef *); // takes ownership

	const CDpyType *DpyType() const;	// readonly access
	CDpyType *GetDpyType();	// passes ownership
	void SetDpyType(CDpyType *);	// takes ownership
	int operator == (const CFieldDesc &) const;
	int operator != (const CFieldDesc &) const;	
	CFieldDesc &operator=(const CFieldDesc &);
private:
	CFieldRef *pFieldRef;
	CDpyType *pDpyType;
	int iSize;
};


/*
 * The class CFieldDef implements the field definition object. It contains
 * the user defined name, a textual description, language value and a list
 * of CFieldDesc objects. Access methods are provided for read/write access
 * of the properties.
 */

class CFieldDef
{
public:
	CFieldDef();
	CFieldDef(const CString &, const CString &, const CString &, int);
	~CFieldDef();
	CFieldDef(const CFieldDef &);
	void CFieldDef::DescFromString(const CString &);
	int GetLang()const;
	void SetLang(int);
	const CString &GetName() const;
	void SetName(const CString &);
	const CString &GetDescription() const;
	void SetDescription(const CString &);

	const std::vector<CFieldDesc> *FieldDesc() const; // readonly access
	std::vector<CFieldDesc> *GetFieldDesc();	// passes ownership
	void SetFieldDesc(std::vector<CFieldDesc> *); // takes ownership
	CString CFieldDef::DescToString() const;
	static int Language(const CString &);
	static CString Language(int);
	int operator == (const CFieldDef &) const;
	int operator != (const CFieldDef &) const;	
	CFieldDef &operator=(const CFieldDef &);
private:
	int imLanguage;
	CString omName;
	CString omDesc;
	std::vector<CFieldDesc> *omDescList;
// blocker
};


/*
 *
 * The class CFieldDefDoc implements the document objects displayed in one
 * document window. Actually , it consists of the field definition records
 * with the same group (GNAM in PEDTAB) name which is also the document name.
 * 
 * The number of contained CFieldDesc objects can be inquired by calling
 * GetNFieldDefs. Read access to a specific field definition is given
 * by FieldDef. The methods GetFieldDef and SetFieldDef can be used to 
 * get ownership of the list, manipulate it and pass it back.
 */

/*
--- ? problems with map ?
typedef std::map<CString, CFieldDef> CFieldDefTable;
typedef std::map<CString, CFieldDefTable> CPEDTable;
*/
typedef std::vector<CPEDRec> CPEDTable;
typedef std::vector<CPEDRec>::iterator CPEDTable_it;


class CFieldDefDoc
{
public:
	static int SetDatabase(CUfisDB &);
	static int GetDocList(CStringVector&);

	CFieldDefDoc(const char *szDocName);
	~CFieldDefDoc();
	int SaveDoc(const char *szName);
	const CString &GetDocName()const;

	const std::vector<CFieldDef> *FieldDef() const;
	std::vector<CFieldDef> *GetFieldDef();
	void SetFieldDef(std::vector<CFieldDef> *);

private:
	CString	omName;
	std::vector<CFieldDef> *omFieldList;
	static CUfisDB *pDB;
	static CStringVector GroupList;
	static CPEDTable PEDTable;
	static void build_doclist();
// blocker
	CFieldDefDoc(const CFieldDefDoc &);
	CFieldDefDoc &operator=(const CFieldDefDoc &);
};

 
#endif CFDDoc_included 
