Attribute VB_Name = "StartModule"
Option Explicit

Public strRetLogin As String

Sub Main()
        
    On Error GoTo ErrHdl
    'ogCommandLine = Command()

    Set frmSplash.AATLoginControl1.UfisComCtrl = frmSplash.UfisCom1
    frmSplash.AATLoginControl1.VersionString = "Version 4.4.1.1"
    frmSplash.AATLoginControl1.InfoCaption = "Info about GATESTATE"
    frmSplash.AATLoginControl1.InfoButtonVisible = True
    frmSplash.AATLoginControl1.InfoUfisVersion = "UFIS Version 4.4"
    frmSplash.AATLoginControl1.InfoAppVersion = CStr("GATESTATE 4.4.1.1 / 23.01.2002")
    frmSplash.AATLoginControl1.InfoCopyright = "Copyright 2002 AAT/I"
    frmSplash.AATLoginControl1.InfoAAT = "ABB Airport Technologies GmbH / Information Systems"
    frmSplash.AATLoginControl1.UserNameLCase = True

    frmSplash.AATLoginControl1.ApplicationName = "GATESTATE"
    frmSplash.AATLoginControl1.LoginAttempts = 3
    frmSplash.AATLoginControl1.RegisterApplicationString = "GATESTATE,InitModu,InitModu,Initialisieren (InitModu),B,-" + _
                                                             ",Scheduled Begin,m_GD1B,Action,A,0" + _
                                                             ",Scheduled End,m_GD1E,Action,A,0" + _
                                                             ",Actual Begin,m_GD1X,Action,A,0" + _
                                                             ",Actual End,m_GD1Y,Action,A,0" + _
                                                             ",Remark,m_REMP,Action,A,1" + _
                                                             ",Submit,m_Submit,Action,A,1"

'    frmSplash.AATLoginControl1.RegisterApplicationString = "GATESTATE,InitModu,InitModu,Initialisieren (InitModu),B,-" + _
'                                                           ",Submit,m_Submit,Action,A,1"
'

    'do the login
     strRetLogin = frmSplash.AATLoginControl1.ShowLoginDialog

    'look after the login result
    If strRetLogin = "CANCEL" Or strRetLogin = "ERROR" Then
        End
    ElseIf strRetLogin = "OK" Then
        ogActualUser = frmSplash.AATLoginControl1.GetUserName
    End If
   
    Globals.ogStartTime = Now()

    DoEvents
    ogSystemMetrics.GetMetrics

    frmSplash.Show
    frmSplash.Refresh
    
    ApplicationIsStarted = True
    
    Load Flights
    
    frmSplash.Visible = False
    frmSplash.Hide

    Flights.Show
    
    Exit Sub
ErrHdl:
    'WriteErrToLog "c:\tmp\GATESTATE.txt", "Module1.Main", Err
    Err.Clear
    Resume Next
End Sub
  
Public Sub SetAllFormsOnTop(SetValue As Boolean)
'we do nothing here
End Sub

Public Sub HandleBroadCast(ReqId As String, DestName As String, RecvName As String, CedaCmd As String, ObjName As String, Seq As String, Tws As String, Twe As String, CedaSqlKey As String, Fields As String, Data As String, BcNum As String)
    Dim s As String
    Dim clDest As String
    Dim clRecv As String
    Dim clCmd As String
    Dim clTable As String
    Dim clSelKey As String
    Dim clFields As String
    Dim clData As String
    'clTable = Left(ObjName, 3)
    clTable = ObjName
    If InStr("AFTTAB", clTable) > 0 Then
        clCmd = CedaCmd
        clSelKey = CedaSqlKey
        clFields = Fields
        clData = Data
        'MsgBox "BC Received: " & clTable & vbNewLine & clCmd & vbNewLine & clSelKey & vbNewLine & clFields & vbNewLine & clData
        'TelexPoolHead.EvaluateBc BcNum, DestName, RecvName, clTable, clCmd, clSelKey, clFields, clData
    End If
End Sub

Public Sub MsgFromUFISAppManager(Orig As Long, Data As String)
Dim tmpFldLst As String
Dim tmpDatLst As String
Dim nlPos As Integer
    If ApplicationIsStarted = True Then
        'TelexPoolHead.Show
        'TelexPoolHead.Refresh
        nlPos = InStr(Data, vbLf)
        If nlPos > 1 Then
            tmpFldLst = Left(Data, nlPos - 1)
            tmpDatLst = Mid(Data, nlPos + 1)
            'TelexPoolHead.SearchFlightTelexes tmpFldLst, tmpDatLst
        Else
            'If CallAskUser(0, 0, 0, "Communication Control", "Undefined message received from FIPS", "hand", "", UserAnswer) > 0 Then DoNothing
        End If
    End If
End Sub


Public Sub HandleDisplayChanged()
    
End Sub

Public Sub HandleSysColorsChanged()
    
End Sub

Public Sub HandleTimeChanged()
    
End Sub

Public Sub ShutDownApplication()
Dim i As Integer
Dim cnt As Integer
    On Error GoTo ErrorHandler
'    MyMsgBox.InfoApi 0, "The 'On Line' functions will be disconnected from the server" & vbNewLine & "after leaving the application.", "Tip"
'    If CallAskUser(0, 0, 0, "Application Control", "Do you want to exit ?", "ask", "Yes,No;F", UserAnswer) = 1 Then
        ShutDownRequested = True
        frmSplash.UfisCom1.CleanupCom
        cnt = Forms.Count - 1
        For i = cnt To 0 Step -1
            Unload Forms(i)
        Next
        End
'    End If
    Exit Sub
ErrorHandler:
    Resume Next
End Sub


