<?php
/*********************************************************************************
 *       Filename: S_FLDSRecord.php
 *       Generated with CodeCharge 1.1.16
 *       PHP & Templates build 03/28/2001
 *********************************************************************************/

include ("./common.php");
include ("./Admin.php");

session_start();

$filename = "S_FLDSRecord.php";



check_security(3);

$tpl = new Template($app_path);
$tpl->load_file("S_FLDSRecord.html", "main");
$tpl->load_file($header_filename, "Header");

$tpl->set_var("FileName", $filename);


$sS_FLDS1Err = "";

$sAction = get_param("FormAction");
$sForm = get_param("FormName");
switch ($sForm) {
  case "S_FLDS1":
    S_FLDS1_action($sAction);
  break;
}Administration_show();
S_FLDS_show();
S_FLDS1_show();

$tpl->parse("Header", false);

$tpl->pparse("main", false);

//********************************************************************************



function S_FLDS_show()
{

  
  global $tpl;
  global $db;
  global $sS_FLDSErr;
  $sWhere = "";
  $sOrder = "";
  $sSQL = "";
  $HasParam = false;

  
  $tpl->set_var("TransitParams", "");
  $tpl->set_var("FormParams", "");
  // Build WHERE statement
  

  $sDirection = "";
  $sSortParams = "";
  
  // Build ORDER statement for "Table fields"
  //$sOrder = " order by S.FLDTABLE, S.CSS_CLASSNAME, S.ROWNUMBER ASC";
  //$sOrder = " order by S.FLDTABLE, S.ROWNUMBER ASC";
  $sOrder = " order by S.FLDTABLE ASC";
  $iSort = get_param("FormS_FLDS_Sorting");
  $iSorted = get_param("FormS_FLDS_Sorted");
  if(!$iSort)
  {
    $tpl->set_var("Form_Sorting", "");
  }
  else
  {
    if($iSort == $iSorted)
    {
      $tpl->set_var("Form_Sorting", "");
      $sDirection = " DESC";
      $sSortParams = "FormS_FLDS_Sorting=" . $iSort . "&FormS_FLDS_Sorted=" . $iSort . "&";
    }
    else
    {
      $tpl->set_var("Form_Sorting", $iSort);
      $sDirection = " ASC";
      $sSortParams = "FormS_FLDS_Sorting=" . $iSort . "&FormS_FLDS_Sorted=" . "&";
    }
    
    //if ($iSort == 1) $sOrder = " order by S.ROWNUMBER" . $sDirection;
    if ($iSort == 1) $sOrder = " order by S.FLDNAME" . $sDirection;
    if ($iSort == 2) $sOrder = " order by S.FLDTABLE" . $sDirection;
    if ($iSort == 3) $sOrder = " order by S.FLDDISPLAYNAME" . $sDirection;
    //if ($iSort == 5) $sOrder = " order by S.CSS_CLASSNAME" . $sDirection;
    if ($iSort == 4) $sOrder = " order by S.FLDID" . $sDirection;
  }

  // Build full SQL statement
  
  $sSQL = "select S.FLDDISPLAYNAME as S_FLDDISPLAYNAME, " . 
    "S.FLDID as S_FLDID, " . 
    "S.FLDNAME as S_FLDNAME, " . 
    "S.FLDTABLE as S_FLDTABLE " . 
    //"S.FLDTABLE as S_FLDTABLE, " . 
    //"S.ROWNUMBER as S_ROWNUMBER " . 
    //"S.ROWNUMBER as S_ROWNUMBER, " . 
    //"S.CSS_CLASSNAME as S_CSS_CLASSNAME " . 
    " from S_FLDS S ";
  
  $sSQL .= $sWhere . $sOrder;
  $tpl->set_var("FormAction", "S_FLDSRecord.php");
  $tpl->set_var("SortParams", $sSortParams);

  // Execute SQL statement
  $db->query($sSQL);
  
  // Select current page
  $iPage = get_param("FormS_FLDS_Page");
  //echo "Page:($iPage)<br>";
  if(!strlen($iPage)) $iPage = 1;
  //$RecordsPerPage = 10;
  $RecordsPerPage = 25;
  if(($iPage - 1) * $RecordsPerPage != 0)
    $db->seek(($iPage - 1) * $RecordsPerPage);
  $iCounter = 0;

  if($db->next_record())
  {  
    // Show main table based on SQL query
    do
    {
      $fldFLDID = $db->f("S_FLDID");
      $fldFLDNAME = $db->f("S_FLDNAME");
      $fldFLDTABLE = $db->f("S_FLDTABLE");
      $fldFLDDISPLAYNAME = $db->f("S_FLDDISPLAYNAME");

	  
      //$fldROWNUMBER = $db->f("S_ROWNUMBER");
      //$fldCSS_CLASSNAME = $db->f("S_CSS_CLASSNAME");
      $tpl->set_var("FLDID", tohtml($fldFLDID));
      $tpl->set_var("FLDID_URLLink", "S_FLDSRecord.php");
      $tpl->set_var("Prm_FLDID", tourl($db->f("S_FLDID"))); 
      $tpl->set_var("FormS_FLDS_Page", tourl($iPage)); 
      $tpl->set_var("FLDNAME", tohtml($fldFLDNAME));
      $tpl->set_var("FLDTABLE", tohtml($fldFLDTABLE));
      $tpl->set_var("FLDDISPLAYNAME", tohtml($fldFLDDISPLAYNAME));
      //$tpl->set_var("ROWNUMBER", tohtml($fldROWNUMBER));
      //$tpl->set_var("CSS_CLASSNAME", tohtml($fldCSS_CLASSNAME));
      $tpl->parse("DListS_FLDS", true);
      $iCounter++;
    } while($iCounter < $RecordsPerPage &&$db->next_record());
  }
  else
  {
    // No Records in DB
    $tpl->set_var("DListS_FLDS", "");
    $tpl->parse("S_FLDSNoRecords", false);
    $tpl->set_var("S_FLDSScroller", "");
    $tpl->parse("FormS_FLDS", false);
    return;
  }
  
  // Parse scroller
  if(@$db->next_record())
  {
    if ($iPage == 1)
    {
      $tpl->set_var("S_FLDSScrollerPrevSwitch", "_");
    }
    else
    {
      $tpl->set_var("PrevPage", ($iPage - 1));
      $tpl->set_var("S_FLDSScrollerPrevSwitch", "");
    }
    $tpl->set_var("NextPage", ($iPage + 1));
    $tpl->set_var("S_FLDSScrollerNextSwitch", "");
    $tpl->set_var("S_FLDSCurrentPage", $iPage);
    $tpl->parse("S_FLDSScroller", false);
  }
  else
  {
    if ($iPage == 1)
    {
      $tpl->set_var("S_FLDSScroller", "");
    }
    else
    {
      $tpl->set_var("S_FLDSScrollerNextSwitch", "_");
      $tpl->set_var("PrevPage", ($iPage - 1));
      $tpl->set_var("S_FLDSScrollerPrevSwitch", "");
      $tpl->set_var("S_FLDSCurrentPage", $iPage);
      $tpl->parse("S_FLDSScroller", false);
    }
  }
  $tpl->set_var("S_FLDSNoRecords", "");
  $tpl->parse("FormS_FLDS", false);
  
}



function S_FLDS1_action($sAction)
{
  global $db;
  global $tpl;
  global $sForm;
  global $sS_FLDS1Err;
  
  $sParams = "";
  $sActionFileName = "S_FLDSRecord.php";

  

  $sWhere = "";
  $bErr = false;

  if($sAction == "cancel")
    header("Location: " . $sActionFileName); 

  
  // Create WHERE statement
  if($sAction == "update" || $sAction == "delete") 
  {
    $pPKFLDID = get_param("PK_FLDID");
    if( !strlen($pPKFLDID)) return;
    $sWhere = "FLDID=" . tosql($pPKFLDID, "Text");
  }

  // Load all form fields into variables
  
  $fldFLDNAME = get_param("FLDNAME");
  $fldFLDTABLE = get_param("FLDTABLE");
  $fldFLDDISPLAYNAME = get_param("FLDDISPLAYNAME");
  $fldFLDEXPTEXT = get_param("FLDEXPTEXT");
  
  //20070221 GFO: WAW Project: Second Language Support
  $fldFLDDISPLAYNAME_TR = get_param("FLDDISPLAYNAME_TR");
  $fldFLDEXPTEXT_TR = get_param("FLDEXPTEXT_TR");
  
  //$fldFLDFORMAT = get_param("FLDFORMAT");
  $fldTMPL_MENU_ID = get_param("TMPL_MENU_ID");
  $fldTMPLID = get_param("TMPLID");
  $fldQUERY = get_param("QUERY");
  //$fldCSS_CLASSNAME = get_param("CSS_CLASSNAME");
  //$fldROWNUMBER = get_param("ROWNUMBER");
  $fldFUNCTION = get_param("FUNCTION");
  $fldDISPLAYFIELD = get_checkbox_value(get_param("DISPLAYFIELD"), "1", "0", "Number");
  // Validate fields
  if($sAction == "insert" || $sAction == "update") 
  {
    //if(!is_number($fldROWNUMBER))
      //$sS_FLDS1Err .= "The value in field Column Number is incorrect.<br>";
    if(!strlen($fldFLDNAME))
      $sS_FLDS1Err .= "No Field Name set! Please correct.<br>";
    if(!strlen($fldFLDTABLE))
      $sS_FLDS1Err .= "No Field Table set! Please correct.<br>";
    

    if(strlen($sS_FLDS1Err)) return;
  }
  

  $sSQL = "";
  // Create SQL statement
  
  switch(strtolower($sAction)) 
  {
    case "insert":
	
        //20070221 GFO: WAW Project: Second Language Support
        $sSQL = "insert into S_FLDS (" . 
          "FLDNAME," . 
          "FLDTABLE," . 
          "FLDDISPLAYNAME," . 
          "FLDEXPTEXT," . 
          //"FLDFORMAT," . 
          "TMPL_MENU_ID," . 
          "TMPLID," . 
          "QUERY," . 
          //"CSS_CLASSNAME," . 
          //"ROWNUMBER," . 
          "FUNCTION," . 
          "DISPLAYFIELD,
		  FLDDISPLAYNAME_TR,
		  FLDEXPTEXT_TR)" . 
          " values (" . 
          tosql($fldFLDNAME, "Text") . "," .
          tosql($fldFLDTABLE, "Text") . "," .
          tosql($fldFLDDISPLAYNAME, "Text") . "," .
          tosql($fldFLDEXPTEXT, "Text") . "," .
          //tosql($fldFLDFORMAT, "Text") . "," .
          tosql($fldTMPL_MENU_ID, "Text") . "," .
          tosql($fldTMPLID, "Text") . "," .
          tosql($fldQUERY, "Text") . "," .
          //tosql($fldCSS_CLASSNAME, "Text") . "," .
          //tosql($fldROWNUMBER, "Number") . "," .
          tosql($fldFUNCTION, "Text") . "," .
          $fldDISPLAYFIELD . ",".
		  tosql($fldFLDDISPLAYNAME_TR, "Text") . "," .
          tosql($fldFLDEXPTEXT_TR, "Text") . ")";    
    break;
    case "update":
       
	   //20070221 GFO: WAW Project: Second Language Support
	   
        $sSQL = "update S_FLDS set " .
          "FLDNAME=" . tosql($fldFLDNAME, "Text") .
          ",FLDTABLE=" . tosql($fldFLDTABLE, "Text") .
          ",FLDDISPLAYNAME=" . tosql($fldFLDDISPLAYNAME, "Text") .
          ",FLDEXPTEXT=" . tosql($fldFLDEXPTEXT, "Text") .
          ",FLDDISPLAYNAME_TR=" . tosql($fldFLDDISPLAYNAME_TR, "Text") .
          ",FLDEXPTEXT_TR=" . tosql($fldFLDEXPTEXT_TR, "Text") .

          //",FLDFORMAT=" . tosql($fldFLDFORMAT, "Text") .
          ",TMPL_MENU_ID=" . tosql($fldTMPL_MENU_ID, "Text") .
          ",TMPLID=" . tosql($fldTMPLID, "Text") .
          ",QUERY=" . tosql($fldQUERY, "Text") .
          //",CSS_CLASSNAME=" . tosql($fldCSS_CLASSNAME, "Text") .
          //",ROWNUMBER=" . tosql($fldROWNUMBER, "Number") .
          ",FUNCTION=" . tosql($fldFUNCTION, "Text") .
          ",DISPLAYFIELD=" . $fldDISPLAYFIELD;
        $sSQL .= " where " . $sWhere;
    break;
    case "delete":
      
        $sSQL = "delete from S_FLDS where " . $sWhere;
    break;
  }
  // Execute SQL statement
  if(strlen($sS_FLDS1Err)) return;
  $db->query($sSQL);
  
  header("Location: " . $sActionFileName);
  
}

function S_FLDS1_show()
{
  global $db;
  global $tpl;
  global $sAction;
  global $sForm;
  global $sS_FLDS1Err;

  $sWhere = "";
  
  $bPK = true;
  $fldFLDID = "";
  $fldFLDNAME = "";
  $fldFLDTABLE = "";
  $fldFLDDISPLAYNAME = "";
  $fldFLDEXPTEXT = "";
  //$fldFLDFORMAT = "";
  $fldTMPL_MENU_ID = "";
  $fldTMPLID = "";
  $fldQUERY = "";
  //$fldCSS_CLASSNAME = "";
  //$fldROWNUMBER = "";
  $fldFUNCTION = "";
  $fldDISPLAYFIELD = "";
 
  //20070221 GFO: WAW Project: Second Language Support 
  $fldFLDDISPLAYNAME_TR = "";
  $fldFLDEXPTEXT_TR = "";
  
  if($sS_FLDS1Err == "")
  {
    // Load primary key and form parameters
    $fldFLDID = get_param("FLDID");
    $pFLDID = get_param("FLDID");
    $tpl->set_var("S_FLDS1Error", "");
  }
  else
  {
    // Load primary key, form parameters and form fields
    $fldFLDID = strip(get_param("FLDID"));
    $fldFLDNAME = strip(get_param("FLDNAME"));
    $fldFLDTABLE = strip(get_param("FLDTABLE"));
    $fldFLDDISPLAYNAME = strip(get_param("FLDDISPLAYNAME"));
    $fldFLDEXPTEXT = strip(get_param("FLDEXPTEXT"));
	
	//20070221 GFO: WAW Project: Second Language Support
    $fldFLDDISPLAYNAME_TR = strip(get_param("FLDDISPLAYNAME_TR"));
    $fldFLDEXPTEXT_TR = strip(get_param("FLDEXPTEXT_TR"));
	
    //$fldFLDFORMAT = strip(get_param("FLDFORMAT"));
    $fldTMPL_MENU_ID = strip(get_param("TMPL_MENU_ID"));
    $fldTMPLID = strip(get_param("TMPLID"));
    $fldQUERY = strip(get_param("QUERY"));
    //$fldCSS_CLASSNAME = strip(get_param("CSS_CLASSNAME"));
    //$fldROWNUMBER = strip(get_param("ROWNUMBER"));
    $fldFUNCTION = strip(get_param("FUNCTION"));
    $fldDISPLAYFIELD = strip(get_param("DISPLAYFIELD"));
    $pFLDID = get_param("PK_FLDID");
    $tpl->set_var("sS_FLDS1Err", $sS_FLDS1Err);
    $tpl->parse("S_FLDS1Error", false);
  }

  
  if( !strlen($pFLDID)) $bPK = false;
  
  $sWhere .= "FLDID=" . tosql($pFLDID, "Text");
  $tpl->set_var("PK_FLDID", $pFLDID);

  $sSQL = "select * from S_FLDS where " . $sWhere;

  

  if($bPK && !($sAction == "insert" && $sForm == "S_FLDS1"))
  {
    // Execute SQL statement
    $db->query($sSQL);
    $db->next_record();
    
    $fldFLDID = $db->f("FLDID");
    if($sS_FLDS1Err == "") 
    {
      // Load data from recordset when form displayed first time
      $fldFLDNAME = $db->f("FLDNAME");
      $fldFLDTABLE = $db->f("FLDTABLE");
      $fldFLDDISPLAYNAME = $db->f("FLDDISPLAYNAME");
      $fldFLDEXPTEXT = $db->f("FLDEXPTEXT");

 	  //20070221 GFO: WAW Project: Second Language Support 
	  $fldFLDDISPLAYNAME_TR = $db->f("FLDDISPLAYNAME_TR");
      $fldFLDEXPTEXT_TR = $db->f("FLDEXPTEXT_TR");
	
      //$fldFLDFORMAT = $db->f("FLDFORMAT");
      $fldTMPL_MENU_ID = $db->f("TMPL_MENU_ID");
      $fldTMPLID = $db->f("TMPLID");
      $fldQUERY = $db->f("QUERY");
      //$fldCSS_CLASSNAME = $db->f("CSS_CLASSNAME");
      //$fldROWNUMBER = $db->f("ROWNUMBER");
      $fldFUNCTION = $db->f("FUNCTION");
      $fldDISPLAYFIELD = $db->f("DISPLAYFIELD");
    }
    $tpl->set_var("S_FLDS1Insert", "");
    $tpl->parse("S_FLDS1Edit", false);
  }
  else
  {
    if($sS_FLDS1Err == "")
    {
      $fldFLDID = tohtml(get_param("FLDID"));
      //$fldCSS_CLASSNAME= "TDDeparture";
    }
    $tpl->set_var("S_FLDS1Edit", "");
    $tpl->parse("S_FLDS1Insert", false);
  }
  $tpl->parse("S_FLDS1Cancel", false);

  // Show form field
  
    $tpl->set_var("FLDID", tohtml($fldFLDID));
    $tpl->set_var("FLDNAME", tohtml($fldFLDNAME));
    $tpl->set_var("FLDTABLE", tohtml($fldFLDTABLE));
    $tpl->set_var("FLDDISPLAYNAME", tohtml($fldFLDDISPLAYNAME));
    $tpl->set_var("FLDEXPTEXT", tohtml($fldFLDEXPTEXT));

    //20070221 GFO: WAW Project: Second Language Support
    $tpl->set_var("FLDDISPLAYNAME_TR", tohtml($fldFLDDISPLAYNAME_TR));
    $tpl->set_var("FLDEXPTEXT_TR", tohtml($fldFLDEXPTEXT_TR));
	
    //$tpl->set_var("FLDFORMAT", tohtml($fldFLDFORMAT));
		//switch ($fldFLDFORMAT)
		//{
		//	case "0":
    //		$tpl->set_var("FLDFORMATCHECKED_0", "CHECKED");
		//		break;
		//	case "1":
    //		$tpl->set_var("FLDFORMATCHECKED_1", "CHECKED");
		//		break;
		//	case "2":
    //		$tpl->set_var("FLDFORMATCHECKED_2", "CHECKED");
		//		break;
		//	default:
		//		break;
		//}
    $tpl->set_var("LBTMPL_MENU_ID", "");
    $tpl->set_var("ID", "");
    $tpl->set_var("Value", "");
    $tpl->parse("LBTMPL_MENU_ID", true);
    $dbTMPL_MENU_ID = new DB_Sql();
    $dbTMPL_MENU_ID->Database = DATABASE_NAME;
    $dbTMPL_MENU_ID->User     = DATABASE_USER;
    $dbTMPL_MENU_ID->Password = DATABASE_PASSWORD;
    $dbTMPL_MENU_ID->Host     = DATABASE_HOST;
  
    
    $dbTMPL_MENU_ID->query("select TMPL_MENUID, MENUNAME from S_TMPL_MENUS order by 2");
    while($dbTMPL_MENU_ID->next_record())
    {
      $tpl->set_var("ID", $dbTMPL_MENU_ID->f(0));
      $tpl->set_var("Value", $dbTMPL_MENU_ID->f(1));
      if($dbTMPL_MENU_ID->f(0) == $fldTMPL_MENU_ID)
        $tpl->set_var("Selected", "SELECTED" );
      else 
        $tpl->set_var("Selected", "");
      $tpl->parse("LBTMPL_MENU_ID", true);
    }
    
    $tpl->set_var("LBTMPLID", "");
    $tpl->set_var("ID", "");
    $tpl->set_var("Value", "");
    $tpl->parse("LBTMPLID", true);
    $dbTMPLID = new DB_Sql();
    $dbTMPLID->Database = DATABASE_NAME;
    $dbTMPLID->User     = DATABASE_USER;
    $dbTMPLID->Password = DATABASE_PASSWORD;
    $dbTMPLID->Host     = DATABASE_HOST;
  
    
    $dbTMPLID->query("select TMPLID, TMPLNAME from S_TMPL order by 2");
    while($dbTMPLID->next_record())
    {
      $tpl->set_var("ID", $dbTMPLID->f(0));
      $tpl->set_var("Value", $dbTMPLID->f(1));
      if($dbTMPLID->f(0) == $fldTMPLID)
        $tpl->set_var("Selected", "SELECTED" );
      else 
        $tpl->set_var("Selected", "");
      $tpl->parse("LBTMPLID", true);
    }
    
    $tpl->set_var("QUERY", tohtml($fldQUERY));
    //$tpl->set_var("CSS_CLASSNAME", tohtml($fldCSS_CLASSNAME));
    //$tpl->set_var("ROWNUMBER", tohtml($fldROWNUMBER));
    $tpl->set_var("LBFUNCTION", "");
    $tpl->set_var("ID", "");
    $tpl->set_var("Value", "");
    $tpl->parse("LBFUNCTION", true);
    $LOV = split(";", "AND;AND;OR;OR");
  
    if(sizeof($LOV)%2 != 0) 
      $array_length = sizeof($LOV) - 1;
    else
      $array_length = sizeof($LOV);
    reset($LOV);
    for($i = 0; $i < $array_length; $i = $i + 2)
    {
      $tpl->set_var("ID", $LOV[$i]);
      $tpl->set_var("Value", $LOV[$i + 1]);
      if($LOV[$i] == $fldFUNCTION) 
        $tpl->set_var("Selected", "SELECTED");
      else
        $tpl->set_var("Selected", "");
      $tpl->parse("LBFUNCTION", true);
    }
      if(strtolower($fldDISPLAYFIELD) == strtolower("1")) 
        $tpl->set_var("DISPLAYFIELD_CHECKED", "CHECKED");
      else
        $tpl->set_var("DISPLAYFIELD_CHECKED", "");

  $tpl->parse("FormS_FLDS1", false);
  

}

?>
