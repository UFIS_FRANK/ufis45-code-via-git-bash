<?php
/*********************************************************************************
 *       Filename: S_ADMINRecord.php
 *       Generated with CodeCharge 1.1.16
 *       PHP & Templates build 03/28/2001
 *********************************************************************************/

include ("./common.php");
include ("./Admin.php");

session_start();

$filename = "S_ADMINRecord.php";



check_security(3);

$tpl = new Template($app_path);
$tpl->load_file("S_ADMINRecord.html", "main");
$tpl->load_file($header_filename, "Header");

$tpl->set_var("FileName", $filename);


$sS_ADMIN1Err = "";

$sAction = get_param("FormAction");
$sForm = get_param("FormName");
switch ($sForm) {
  case "S_ADMIN1":
    S_ADMIN1_action($sAction);
  break;
}Administration_show();
S_ADMIN_show();
S_ADMIN1_show();

$tpl->parse("Header", false);

$tpl->pparse("main", false);

//********************************************************************************



function S_ADMIN_show()
{

  
  global $tpl;
  global $db;
  global $sS_ADMINErr;
  $sWhere = "";
  $sOrder = "";
  $sSQL = "";
  $HasParam = false;

  
  $tpl->set_var("TransitParams", "");
  $tpl->set_var("FormParams", "");
  // Build WHERE statement
  

  $sDirection = "";
  $sSortParams = "";
  
  // Build ORDER statement
  $sOrder = " order by S.USERNAME Asc";
  $iSort = get_param("FormS_ADMIN_Sorting");
  $iSorted = get_param("FormS_ADMIN_Sorted");
  if(!$iSort)
  {
    $tpl->set_var("Form_Sorting", "");
  }
  else
  {
    if($iSort == $iSorted)
    {
      $tpl->set_var("Form_Sorting", "");
      $sDirection = " DESC";
      $sSortParams = "FormS_ADMIN_Sorting=" . $iSort . "&FormS_ADMIN_Sorted=" . $iSort . "&";
    }
    else
    {
      $tpl->set_var("Form_Sorting", $iSort);
      $sDirection = " ASC";
      $sSortParams = "FormS_ADMIN_Sorting=" . $iSort . "&FormS_ADMIN_Sorted=" . "&";
    }
    
    if ($iSort == 1) $sOrder = " order by S.ADMINID" . $sDirection;
    if ($iSort == 2) $sOrder = " order by S.USERNAME" . $sDirection;
    if ($iSort == 3) $sOrder = " order by S.PASSWORD" . $sDirection;
  }

  // Build full SQL statement
  
  $sSQL = "select S.ADMINID as S_ADMINID, " . 
    "S.PASSWORD as S_PASSWORD, " . 
    "S.USERNAME as S_USERNAME " . 
    " from S_ADMIN S ";
  
  $sSQL .= $sWhere . $sOrder;
  $tpl->set_var("SortParams", $sSortParams);

  // Execute SQL statement
  $db->query($sSQL);
  
  // Select current page
  $iPage = get_param("FormS_ADMIN_Page");
  if(!strlen($iPage)) $iPage = 1;
  $RecordsPerPage = 10;
  if(($iPage - 1) * $RecordsPerPage != 0)
    $db->seek(($iPage - 1) * $RecordsPerPage);
  $iCounter = 0;

  if($db->next_record())
  {  
    // Show main table based on SQL query
    do
    {
      $fldADMINID = $db->f("S_ADMINID");
      $fldUSERNAME = $db->f("S_USERNAME");
      $fldPASSWORD = $db->f("S_PASSWORD");
      $tpl->set_var("ADMINID", tohtml($fldADMINID));
      $tpl->set_var("ADMINID_URLLink", "S_ADMINRecord.php");
      $tpl->set_var("Prm_ADMINID", tourl($db->f("S_ADMINID"))); 
      $tpl->set_var("USERNAME", tohtml($fldUSERNAME));
      $tpl->set_var("PASSWORD", tohtml($fldPASSWORD));
      $tpl->parse("DListS_ADMIN", true);
      $iCounter++;
    } while($iCounter < $RecordsPerPage &&$db->next_record());
  }
  else
  {
    // No Records in DB
    $tpl->set_var("DListS_ADMIN", "");
    $tpl->parse("S_ADMINNoRecords", false);
    $tpl->set_var("S_ADMINScroller", "");
    $tpl->parse("FormS_ADMIN", false);
    return;
  }
  
  // Parse scroller
  if(@$db->next_record())
  {
    if ($iPage == 1)
    {
      $tpl->set_var("S_ADMINScrollerPrevSwitch", "_");
    }
    else
    {
      $tpl->set_var("PrevPage", ($iPage - 1));
      $tpl->set_var("S_ADMINScrollerPrevSwitch", "");
    }
    $tpl->set_var("NextPage", ($iPage + 1));
    $tpl->set_var("S_ADMINScrollerNextSwitch", "");
    $tpl->set_var("S_ADMINCurrentPage", $iPage);
    $tpl->parse("S_ADMINScroller", false);
  }
  else
  {
    if ($iPage == 1)
    {
      $tpl->set_var("S_ADMINScroller", "");
    }
    else
    {
      $tpl->set_var("S_ADMINScrollerNextSwitch", "_");
      $tpl->set_var("PrevPage", ($iPage - 1));
      $tpl->set_var("S_ADMINScrollerPrevSwitch", "");
      $tpl->set_var("S_ADMINCurrentPage", $iPage);
      $tpl->parse("S_ADMINScroller", false);
    }
  }
  $tpl->set_var("S_ADMINNoRecords", "");
  $tpl->parse("FormS_ADMIN", false);
  
}



function S_ADMIN1_action($sAction)
{
  global $db;
  global $tpl;
  global $sForm;
  global $sS_ADMIN1Err;
  
  $sParams = "";
  $sActionFileName = "S_ADMINRecord.php";

  

  $sWhere = "";
  $bErr = false;

  if($sAction == "cancel")
    header("Location: " . $sActionFileName); 

  
  // Create WHERE statement
  if($sAction == "update" || $sAction == "delete") 
  {
    $pPKADMINID = get_param("PK_ADMINID");
    if( !strlen($pPKADMINID)) return;
    $sWhere = "ADMINID=" . tosql($pPKADMINID, "Text");
  }

  // Load all form fields into variables
  
  $fldUSERNAME = get_param("USERNAME");
  $fldPASSWORD = get_param("PASSWORD");
  // Validate fields
  if($sAction == "insert" || $sAction == "update") 
  {

    if(strlen($sS_ADMIN1Err)) return;
  }
  

  $sSQL = "";
  // Create SQL statement
  
  switch(strtolower($sAction)) 
  {
    case "update":
      
        $sSQL = "update S_ADMIN set " .
          "USERNAME=" . tosql($fldUSERNAME, "Text") .
          ",PASSWORD=" . tosql($fldPASSWORD, "Text");
        $sSQL .= " where " . $sWhere;
    break;
  }
  // Execute SQL statement
  if(strlen($sS_ADMIN1Err)) return;
  $db->query($sSQL);
  
  header("Location: " . $sActionFileName);
  
}

function S_ADMIN1_show()
{
  global $db;
  global $tpl;
  global $sAction;
  global $sForm;
  global $sS_ADMIN1Err;

  $sWhere = "";
  
  $bPK = true;
  $fldADMINID = "";
  $fldUSERNAME = "";
  $fldPASSWORD = "";
  

  if($sS_ADMIN1Err == "")
  {
    // Load primary key and form parameters
    $fldADMINID = get_param("ADMINID");
    $pADMINID = get_param("ADMINID");
    $tpl->set_var("S_ADMIN1Error", "");
  }
  else
  {
    // Load primary key, form parameters and form fields
    $fldADMINID = strip(get_param("ADMINID"));
    $fldUSERNAME = strip(get_param("USERNAME"));
    $fldPASSWORD = strip(get_param("PASSWORD"));
    $pADMINID = get_param("PK_ADMINID");
    $tpl->set_var("sS_ADMIN1Err", $sS_ADMIN1Err);
    $tpl->parse("S_ADMIN1Error", false);
  }

  
  if( !strlen($pADMINID)) $bPK = false;
  
  $sWhere .= "ADMINID=" . tosql($pADMINID, "Text");
  $tpl->set_var("PK_ADMINID", $pADMINID);

  $sSQL = "select * from S_ADMIN where " . $sWhere;

  

  if($bPK && !($sAction == "insert" && $sForm == "S_ADMIN1"))
  {
    // Execute SQL statement
    $db->query($sSQL);
    $db->next_record();
    
    $fldADMINID = $db->f("ADMINID");
    if($sS_ADMIN1Err == "") 
    {
      // Load data from recordset when form displayed first time
      $fldUSERNAME = $db->f("USERNAME");
      $fldPASSWORD = $db->f("PASSWORD");
    }
    $tpl->set_var("S_ADMIN1Delete", "");
    $tpl->set_var("S_ADMIN1Insert", "");
    $tpl->parse("S_ADMIN1Edit", false);
  }
  else
  {
    if($sS_ADMIN1Err == "")
    {
      $fldADMINID = tohtml(get_param("ADMINID"));
    }
    $tpl->set_var("S_ADMIN1Edit", "");
    $tpl->set_var("S_ADMIN1Insert", "");
  }
  $tpl->parse("S_ADMIN1Cancel", false);

  // Show form field
  
    $tpl->set_var("ADMINID", tohtml($fldADMINID));
    $tpl->set_var("USERNAME", tohtml($fldUSERNAME));
    $tpl->set_var("PASSWORD", tohtml($fldPASSWORD));
  $tpl->parse("FormS_ADMIN1", false);
  

}

?>