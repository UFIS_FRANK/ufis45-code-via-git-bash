// basicdat.cpp CBasicData class for providing general used methods

#include "stdafx.h"
#include "BasicData.h"
#include "CedaCfgData.h"

int GetItemCount(CString olList, char cpTrenner  )
{
	CStringArray olStrArray;
	return ExtractItemList(olList,&olStrArray,cpTrenner);

}

CString LoadStg(UINT nID)
{
	CString olString = "";
	olString.LoadString(nID);
	return olString;
}

int ExtractItemList(CString opSubString, CStringArray *popStrArray, char cpTrenner)
{

	CString olText;

	popStrArray->RemoveAll();

	BOOL blEnd = FALSE;
	
	if(!opSubString.IsEmpty())
	{
		int pos;
		int olPos = 0;
		while(blEnd == FALSE)
		{
			pos = opSubString.Find(cpTrenner);
			if(pos == -1)
			{
				blEnd = TRUE;
				olText = opSubString;
			}
			else
			{
				olText = opSubString.Mid(0, opSubString.Find(cpTrenner));
				opSubString = opSubString.Mid(opSubString.Find(cpTrenner)+1, opSubString.GetLength( )-opSubString.Find(cpTrenner)+1);
			}
			popStrArray->Add(olText);
		}
	}
	return	popStrArray->GetSize();

}


CString GetListItem(CString &opList, int ipPos, bool bpCut, char cpTrenner )
{
	CStringArray olStrArray;
	int ilAnz = ExtractItemList(opList, &olStrArray, cpTrenner);
	CString olReturn;

	if(ipPos == -1)
		ipPos = ilAnz;

	if((ipPos <= ilAnz) && (ipPos > 0))
		olReturn = olStrArray[ipPos - 1];
	if(bpCut)
	{
		opList = "";
		for(int ilLc = 0; ilLc < ilAnz; ilLc++)
		{
			if(ilLc != (ipPos - 1))
			{
				opList = opList + olStrArray[ilLc] + cpTrenner;
			}
		}
	}
	if(bpCut)
		opList = opList.Left(opList.GetLength() - 1);
	return olReturn;
}



CString DeleteListItem(CString &opList, CString olItem)
{
	CStringArray olStrArray;
	int ilAnz = ExtractItemList(opList, &olStrArray);
	opList = "";
	for(int ilLc = 0; ilLc < ilAnz; ilLc++)
	{
			if(olStrArray[ilLc] != olItem)
				opList = opList + olStrArray[ilLc] + ",";
	}
	opList = opList.Left(opList.GetLength() - 1);
	return opList;
}


CBasicData::CBasicData(void)
{
	char pclTmpText[512];
	char pclConfigPath[512];
    if (getenv("CEDA") == NULL)
        strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
    else
        strcpy(pclConfigPath, getenv("CEDA"));

	imNextOrder = 4711;
	*pcmCedaCmd = '\0';


	omDiaStartTime = CTime::GetCurrentTime();
	omDiaStartTime -= CTimeSpan(0, 1, 0, 0);
	omDiaEndTime = omDiaStartTime + CTimeSpan(0, 6, 0, 0);
	

	// now reading the ceda commands
	char pclComandBuf[24];
	omDefaultComands.Empty();
	omActualComands.Empty();
 
    GetPrivateProfileString(pcgAppName, "EIOHDL", "LLF",
		pclTmpText, sizeof pclTmpText, pclConfigPath);
	sprintf(pclComandBuf," %3s ","LLF");
	omDefaultComands += pclComandBuf;
	sprintf(pclComandBuf," %3s ",pclTmpText);
	omActualComands += pclComandBuf;
}



CBasicData::~CBasicData(void)
{
}

long CBasicData::GetNextUrno(void)
{
	bool	olRc = true;
	long				llNextUrno = 0L;
	

	if (omUrnos.GetSize() == 0)
	{
		olRc = GetNurnos(100);
	}

	if (omUrnos.GetSize() > 0)
	{
		llNextUrno = omUrnos[0];
		omUrnos.RemoveAt(0);

		if ( (llNextUrno != 0L) && (olRc == true) )
		{
			return(llNextUrno);
		}
	}
	//TO DO ==> Meldung und Programmabbruch
	//::MessageBox(NULL,LoadStg(IDS_STRING444),LoadStg(IDS_STRING445),MB_OK);
	return -1;	
}


bool CBasicData::GetNurnos(int ipNrOfUrnos)
{
	bool	ilRc = false;
	char 	pclTmpDataBuf[12*N_URNOS_AT_ONCE];

	sprintf(pclTmpDataBuf, "%d", ipNrOfUrnos);

	ilRc = CedaAction("GMU", "", "", pclTmpDataBuf);

	if (ilRc == true)
	{
		for ( int ilItemNo=1; (ilItemNo <= N_URNOS_AT_ONCE) ; ilItemNo++ )
		{
			char pclTmpBuf[64];

			GetItem(ilItemNo, pclTmpDataBuf, pclTmpBuf);

			long llNewUrno = atol(pclTmpBuf);

			omUrnos.Add(llNewUrno);
		}
	}

	return ilRc;
}


/*long CBasicData::GetNextUrno(void)
{
	if (CedaAction("GNU") == true)
	{
		return(atol(omDataBuf[0]));
	}
	MessageBox(NULL,"Keine Verbindung zum Server oder Timeout,\nProgramm wird beendet",
			"Systemfehler",MB_OK);
	ExitProcess(1);
	return -1;	
}
*/

int CBasicData::GetNextOrderNo()
{
	imNextOrder++;

	return (imNextOrder);
}


char *CBasicData::GetCedaCommand(CString opCmdType)
{ 

	int ilIndex;

	if ((ilIndex = omDefaultComands.Find(opCmdType)) != -1)
	{
		strcpy(pcmCedaComand,omActualComands.Mid(ilIndex,3));
	}
	else
	{
		strcpy(pcmCedaComand,opCmdType);
	}
	return pcmCedaComand;
}


void CBasicData::GetDiagramStartTime(CTime &opStart, CTime &opEnd)
{
	opStart = omDiaStartTime;
	opEnd = omDiaEndTime;
	return;
}


void CBasicData::SetDiagramStartTime(CTime opDiagramStartTime, CTime opDiagramEndTime)
{
	omDiaStartTime = opDiagramStartTime;
	omDiaEndTime = opDiagramEndTime;
}


void CBasicData::SetWorkstationName(CString opWsName)
{
	omWorkstationName = CString(opWsName);
}


CString CBasicData::GetWorkstationName()
{
	return omWorkstationName;
}


