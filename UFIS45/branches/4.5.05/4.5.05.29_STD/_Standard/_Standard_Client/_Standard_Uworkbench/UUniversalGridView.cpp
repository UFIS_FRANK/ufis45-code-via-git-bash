// UUniversalGridView.cpp : implementation file
//

#include "stdafx.h"
#include "UWorkBench.h"
#include "UUniversalGridView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// UUniversalGridView

IMPLEMENT_DYNCREATE(UUniversalGridView, CGXGridView)

UUniversalGridView::UUniversalGridView()
{
}

UUniversalGridView::~UUniversalGridView()
{
}


BEGIN_MESSAGE_MAP(UUniversalGridView, CView)
	//{{AFX_MSG_MAP(UUniversalGridView)
	ON_WM_CREATE()
	//}}AFX_MSG_MAP
	ON_COMMAND(ID_FILE_PRINT, CGXGridView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, CGXGridView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, CGXGridView::OnFilePrintPreview)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// UUniversalGridView drawing

BOOL UUniversalGridView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CGXGridView::PreCreateWindow(cs);
}

int UUniversalGridView::OnCreate( LPCREATESTRUCT lpCreateStruct )
{
#if _MFC_VER >= 0x0400
	// Enable grid to be used as data source
	EnableOleDataSource();

	// Register the grid as drop target
	VERIFY(m_objDndDropTarget.Register(this));
#endif
	return CGXGridView::OnCreate(lpCreateStruct);
}
/////////////////////////////////////////////////////////////////////////////
// CGridView drawing

void UUniversalGridView::OnDraw(CDC* pDC)
{
	UUniversalGridDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	CGXGridView::OnDraw(pDC);
	// TODO: add draw code for native data here
}

void UUniversalGridView::OnInitialUpdate()
{


	BOOL bFirstView = FALSE;	

//	if (GetDocument()->m_pParam == NULL)
	{
		bFirstView = TRUE;       
			// bFirstView = TRUE indicates that this is 
			// the first view connected to the document
			// and therefore the data need to be intialized.
		// construct parameter object
		GetDocument()->m_pParam = new CGXGridParam;
	}
	// else
		// bFirstView = FALSE indicates that this is 
		// only another view connected to the document
		// No data need to be initialized. They are all 
		// available in the document already.


	// pass the pointer to the grid view
	SetParam(GetDocument()->m_pParam, FALSE);
	                           //     ^-- indicates that document is 
	                        // responsible for deleting the object.

#if _MFC_VER >= 0x0400
EnableGridToolTips();
#endif


	// Lock any drawing
	BOOL bOld = LockUpdate();

	// Custom initialization of styles map
/*	if (GetParam()->GetStylesMap() == NULL)
	{
		// create a stylesmap and connect it with the parameter-object
		CGXStylesMap* pStyMap = new CGXStylesMap;
		
		GetParam()->SetStylesMap(pStyMap);

		// add standard styles
		pStyMap->CreateStandardStyles();
		


		SetupUserAttributes();

		// If you want to specify a specific profile section
		// for load and storing the base styles in the registry,
		// simply uncomment the next line
		// pStyMap->SetSection(_T("My Base Styles"));  // extra profile section

		//pStyMap->ReadProfile();
	}

	// Custom initialization of property object
	if (GetParam()->GetProperties() == NULL)
	{
		// create a stylesmap and connect it with the parameter-object
		CGXProperties* pProperties = new CGXProperties;

		pProperties->AddDefaultUserProperties();

		// TODO: Add here your own preferred setup of properties

		// SAMPLE CODE
		//uncomment as required
		//pProperties->SetCenterHorizontal(TRUE);
		//pProperties->SetDisplayHorzLines(FALSE);
		//pProperties->SetPrintHorzLines(FALSE);
		//pProperties->SetPrintVertLines(FALSE);
		//pProperties->SetBlackWhite(TRUE);
		//pProperties->SetPrintFrame(TRUE);
		// END OF SAMPLE CODE

		// If you want to specify a specific profile section
		// for load and storing the properties in the registry,
		// simply uncomment the next line
		// pProperties->SetSection(_T("My Properties"));  // extra profile section

		pProperties->ReadProfile();

		GetParam()->SetProperties(pProperties);
	}

	// Custom initialization of printer settings
	if (GetParam()->GetPrintDevice() == NULL)
	{
		// Initialize printer object if you want to have
		// some default settings for the grid printing, e.g. Print Landscape
	}
*/
	CGXGridView::OnInitialUpdate();	



	// check if data need to be initialized
	if (bFirstView)
	{
		EnableHints(FALSE);

		// initialize the grid data

		// disable undo mechanism for the following commands
		GetParam()->EnableUndo(FALSE);

		// TODO: Remove the sample code and place here your initialization code for
		// setting up the initial grid appearance. 

		// START OF SAMPLE CODE TO BE REMOVED
		
		// Example: Initialize grid with 30 rows and 10 columns

		
		SetRowCount(30);    // 30 rows
		SetColCount(10);    // 10 columns

		// Display Hello world in the first two cells.

		SetValueRange(CGXRange(1, 1), "Hello");
		SetValueRange(CGXRange(2, 1), "world");

		// world should be displayed right aligned

		SetStyleRange(CGXRange(2, 1),
			CGXStyle().SetHorizontalAlignment(DT_RIGHT));

		// make a big headline from cell (4,2) to cell (6,5)

		SetCoveredCellsRowCol(4, 2, 6, 5);

		// display "Welcome" in it with some formattings.
		ChangeStandardStyle(
			CGXStyle()
				.SetFont(CGXFont().SetSize(10)) // bigger font
				.SetAutoSize(TRUE)    // cells will grow automatically when 
                                      // user enters large text
				.SetWrapText(TRUE)    // wrap text when it does not fit 
                                      // into a single line
				.SetAllowEnter(TRUE)  // when user presses <Enter> a new line will 
                                      // be inserted into the text of the cell
			, gxOverride );

		// You can enable sorting by inserting the following calls
	    // in your InitialUpdate routine:

		GetParam()->SetSortRowsOnDblClk(TRUE);
		
		// END OF SAMPLE CODE TO BE REMOVED

			// Floating cells mode
		SetFloatCellsMode(gxnFloatDelayEval);
		
		// The other possible mode for floatcells is 
		// gxnFloatEvalOnDisplay 

		// For merge cells
		SetMergeCellsMode(gxnMergeDelayEval);

		// where other modes can be one of:

		// gxnMergeDisable       - Disable merge cells
		// gxnMergeDelayEval     - Delay evaluation of merge cells
		// gxnMergeEvalOnDisplay - Always reevaluate merge cells before they are displayed
		// combined with

		// gxnMergeHorzOnly      - Cells can only be merged horizontally
		// gxnMergeVertOnly      - Cells can only be merged vertically


		// reenable undo mechanism
		GetParam()->EnableUndo(TRUE);

	}
	// Register all controls for the grid

	// Unlock drawing
	LockUpdate(bOld);

	// Just to be sure that everything is redrawn
	Invalidate();

	// Enable Objective Grid internal update-hint mechanism

	// You should put this line as last command into OnInitialUpdate,
	// becaus as long as EnableHints is not called, the modified flag
	// of the document will not be changed.

	EnableHints();
}


/////////////////////////////////////////////////////////////////////////////
// CGridView printing

BOOL UUniversalGridView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// default preparation
	return DoPreparePrinting(pInfo);
}

void UUniversalGridView::OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo)
{
	CGXGridView::OnBeginPrinting(pDC, pInfo);
	// TODO: add extra initialization before printing
}

void UUniversalGridView::OnEndPrinting(CDC* pDC, CPrintInfo* pInfo)
{
	CGXGridView::OnEndPrinting(pDC, pInfo);

	// TODO: add cleanup after printing
}


/////////////////////////////////////////////////////////////////////////////
// UUniversalGridView diagnostics

#ifdef _DEBUG
void UUniversalGridView::AssertValid() const
{
	CView::AssertValid();
}

void UUniversalGridView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}
#endif //_DEBUG
UUniversalGridDoc* UUniversalGridView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(UUniversalGridDoc)));
	return (UUniversalGridDoc*)m_pDocument;
}

/////////////////////////////////////////////////////////////////////////////
// UUniversalGridView message handlers
