// ObjectCopyDlg.cpp : implementation file
//

#include "stdafx.h"
#include "uworkbench.h"
#include "ObjectCopyDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// ObjectCopyDlg dialog


ObjectCopyDlg::ObjectCopyDlg(CWnd* pParent /*=NULL*/)
	: CDialog(ObjectCopyDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(ObjectCopyDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT


	int i = 0; 
	for( i = 0; i < ogLoadSets.GetSize(); i++)
	{
		UDBLoadSet *polLoadSet = new UDBLoadSet();
		*polLoadSet = ogLoadSets[i];
		omSourceLoadSets.Add(polLoadSet);
	}
	for( i = 0; i < ogGridDefs.GetSize(); i++)
	{
		UGridDefinition *polGridDef = new UGridDefinition();
		*polGridDef = ogGridDefs[i];
		omSourceGridDefs.Add(polGridDef);
	}
	for( i = 0; i < ogForms.GetSize(); i++)
	{
		CString olFormName = ogForms[i].omName;
		for( int j = 0; j < ogForms[i].omProperties.GetSize(); j++)
		{
			UCtrlProperties olProp;
			olProp = ogForms[i].omProperties[j];
			if(olProp.omCtrlType == "grid")
			{
				int o;
				o = 0;
			}
		}
		omSourceForms.NewAt(omSourceForms.GetSize(), ogForms[i]);
	}
	for( i = 0; i < omSourceQueries.GetSize(); i++)
	{
		UQuery *polQuery = new UQuery();
		*polQuery = omSourceQueries[i];
		omSourceQueries.Add(polQuery);
	}

/*	UFormProperties olForm;
	UFormProperties olNewForm;
	UGridCtrlProperties *polGrid = new UGridCtrlProperties();
	olForm.omProperties.Add(polGrid);
	olNewForm = olForm;
	i = 1;
*/
}

//--------------------------------------------------------------------------
// Destructur:
// Delete all allocated Objects
//--------------------------------------------------------------------------
ObjectCopyDlg::~ObjectCopyDlg()
{
	ClearSourceArrays();
	ClearDestinationArrays();
}

//---------------------------------------------------------------------------
// Clear the Source part of allocated Objects ==> for Array reuse
//---------------------------------------------------------------------------
void ObjectCopyDlg::ClearSourceArrays()
{
	int i=0;
	for( i = 0; i < omSourceLoadSets.GetSize(); i++)
	{
		omSourceLoadSets[i].TableArray.DeleteAll();
	}
	omSourceLoadSets.DeleteAll();
	for(i = 0; i < omSourceQueries.GetSize(); i++)
	{
		omSourceQueries[i].omQueryDetails.DeleteAll();
	}
	omSourceQueries.DeleteAll();

	for( i = 0; i < omSourceGridDefs.GetSize(); i++)
	{
		omSourceGridDefs[i].omGridFields.DeleteAll();
	}
	omSourceGridDefs.DeleteAll();

	
	 for( i = 0; i < omSourceForms.GetSize(); i++)
	 {
		for(int j = omSourceForms[i].omProperties.GetSize()-1; j >= 0; j--)
		{
			CString olCtrlType = omSourceForms[i].omProperties[j].omCtrlType;
			if(olCtrlType == "grid")
			{
				UGridCtrlProperties *polCtrl = (UGridCtrlProperties *)&(omSourceForms[i].omProperties[j]);
				delete polCtrl;
			}
			else if(olCtrlType == "edit")
			{
				UEditCtrlProperties *polCtrl = (UEditCtrlProperties *)&(omSourceForms[i].omProperties[j]);
				delete polCtrl;
			}
			else if(olCtrlType == "static")
			{
				UStaticCtrlProperties *polCtrl = (UStaticCtrlProperties *)&(omSourceForms[i].omProperties[j]);
				delete polCtrl;
			}
			else if(olCtrlType == "button")
			{
				UButtonCtrlProperties *polCtrl = (UButtonCtrlProperties *)&(omSourceForms[i].omProperties[j]);
				delete polCtrl;
			}
			else if(olCtrlType == "checkbox")
			{
				UCheckboxCtrlProperties *polCtrl = (UCheckboxCtrlProperties *)&(omSourceForms[i].omProperties[j]);
				delete polCtrl;
			}
			else if(olCtrlType == "combobox")
			{
				UComboboxCtrlProperties *polCtrl = (UComboboxCtrlProperties *)&(omSourceForms[i].omProperties[j]);
				delete polCtrl;
			}
			else if(olCtrlType == "line")
			{
				ULineCtrlProperties *polCtrl = (ULineCtrlProperties *)&(omSourceForms[i].omProperties[j]);
				delete polCtrl;
			}
			else if(olCtrlType == "rectangle")
			{
				URectCtrlProperties *polCtrl = (URectCtrlProperties *)&(omSourceForms[i].omProperties[j]);
				delete polCtrl;
			}
			else if(olCtrlType == "radio")
			{
				URadioCtrlProperties *polCtrl = (URadioCtrlProperties *)&(omSourceForms[i].omProperties[j]);
				delete polCtrl;
			}
			else
			{//
				omSourceForms[i].omProperties.DeleteAt(j);
			}
			//omSourceForms[i].omProperties.DeleteAll();
		}
		omSourceForms[i].omProperties.RemoveAll();
		omSourceForms[i].omDBFields.DeleteAll();
	 }
	 omSourceForms.DeleteAll();
	

}

//---------------------------------------------------------------------------
// Clear the Destination part of allocated Objects ==> for Array reuse
//---------------------------------------------------------------------------
void ObjectCopyDlg::ClearDestinationArrays()
{
	int i;	
	for( i = 0; i < omDestinationLoadSets.GetSize(); i++)
	{
		omDestinationLoadSets[i].TableArray.DeleteAll();
	}
	omDestinationLoadSets.DeleteAll();
	for(i = 0; i < omDestinationQueries.GetSize(); i++)
	{
		omDestinationQueries[i].omQueryDetails.DeleteAll();
	}
	omDestinationQueries.DeleteAll();

	for( i = 0; i < omDestinationGridDefs.GetSize(); i++)
	{
		omDestinationGridDefs[i].omGridFields.DeleteAll();
	}
	omDestinationGridDefs.DeleteAll();

	
	 for( i = 0; i < omDestinationForms.GetSize(); i++)
	 {
		for(int j = omDestinationForms[i].omProperties.GetSize()-1; j >= 0; j--)
		{
			CString olCtrlType = omDestinationForms[i].omProperties[j].omCtrlType;
			if(olCtrlType == "grid")
			{
				UGridCtrlProperties *polCtrl = (UGridCtrlProperties *)&(omDestinationForms[i].omProperties[j]);
				delete polCtrl;
			}
			else if(olCtrlType == "edit")
			{
				UEditCtrlProperties *polCtrl = (UEditCtrlProperties *)&(omDestinationForms[i].omProperties[j]);
				delete polCtrl;
			}
			else if(olCtrlType == "static")
			{
				UStaticCtrlProperties *polCtrl = (UStaticCtrlProperties *)&(omDestinationForms[i].omProperties[j]);
				delete polCtrl;
			}
			else if(olCtrlType == "button")
			{
				UButtonCtrlProperties *polCtrl = (UButtonCtrlProperties *)&(omDestinationForms[i].omProperties[j]);
				delete polCtrl;
			}
			else if(olCtrlType == "checkbox")
			{
				UCheckboxCtrlProperties *polCtrl = (UCheckboxCtrlProperties *)&(omDestinationForms[i].omProperties[j]);
				delete polCtrl;
			}
			else if(olCtrlType == "combobox")
			{
				UComboboxCtrlProperties *polCtrl = (UComboboxCtrlProperties *)&(omDestinationForms[i].omProperties[j]);
				delete polCtrl;
			}
			else if(olCtrlType == "line")
			{
				ULineCtrlProperties *polCtrl = (ULineCtrlProperties *)&(omDestinationForms[i].omProperties[j]);
				delete polCtrl;
			}
			else if(olCtrlType == "rectangle")
			{
				URectCtrlProperties *polCtrl = (URectCtrlProperties *)&(omDestinationForms[i].omProperties[j]);
				delete polCtrl;
			}
			else if(olCtrlType == "radio")
			{
				URadioCtrlProperties *polCtrl = (URadioCtrlProperties *)&(omDestinationForms[i].omProperties[j]);
				delete polCtrl;
			}
			else
			{//
				omDestinationForms[i].omProperties.DeleteAt(j);
			}
			//omDestinationForms[i].omProperties.DeleteAll();
		}
		omDestinationForms[i].omProperties.RemoveAll();
		omDestinationForms[i].omDBFields.DeleteAll();
	 }
	 omDestinationForms.DeleteAll();
}

void ObjectCopyDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(ObjectCopyDlg)
	DDX_Control(pDX, IDC_NEW_GRIDS, m_NewGrids);
	DDX_Control(pDX, IDC_NEW_FORMS, m_NewForms);
	DDX_Control(pDX, IDC_GRIDS, m_Grids);
	DDX_Control(pDX, IDC_FORMS, m_Forms);
	DDX_Control(pDX, IDC_FILENAME, m_FileName);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(ObjectCopyDlg, CDialog)
	//{{AFX_MSG_MAP(ObjectCopyDlg)
	ON_BN_CLICKED(IDC_LOAD_DESTINATION, OnLoadDestination)
	ON_BN_CLICKED(IDC_LOAD_SOURCE, OnLoadSource)
	ON_BN_CLICKED(IDC_GRID_DELETE, OnGridDelete)
	ON_BN_CLICKED(IDC_GRID_TO_RIGHT, OnGridToRight)
	ON_BN_CLICKED(IDC_FORM_DELETE, OnFormDelete)
	ON_BN_CLICKED(IDC_FORM_TO_RIGHT, OnFormToRight)
	ON_BN_CLICKED(IDC_BROWSE, OnBrowse)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// ObjectCopyDlg message handlers

void ObjectCopyDlg::OnLoadDestination() 
{
	CFileDialog olFileDlg(TRUE, "cfg", "*.cfg", OFN_HIDEREADONLY|OFN_OVERWRITEPROMPT, "*.cfg");
	if(olFileDlg.DoModal() == IDOK)
	{
		SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
		omDestinationFile = olFileDlg.GetPathName();
		m_FileName.SetWindowText(omDestinationFile);
		ClearDestinationArrays();
		((CUWorkBenchApp*)AfxGetApp())->ReadAnyCfgFile(omDestinationFile,
													  omDestinationLoadSets,
													  omDestinationGridDefs,
													  omDestinationForms,
													  omDestinationQueries);
		InitialFillListBoxes();
		SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	}
}

void ObjectCopyDlg::OnLoadSource() 
{
	CFileDialog olFileDlg(TRUE, "cfg", "*.cfg", OFN_HIDEREADONLY|OFN_OVERWRITEPROMPT, "*.cfg");
	if(olFileDlg.DoModal() == IDOK)
	{
		SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
		omSourceFile = olFileDlg.GetPathName();
		ClearSourceArrays();
		((CUWorkBenchApp*)AfxGetApp())->ReadAnyCfgFile(omSourceFile,
													  omSourceLoadSets,
													  omSourceGridDefs,
													  omSourceForms,
													  omSourceQueries);
		InitialFillListBoxes();
		SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	}
}

void ObjectCopyDlg::OnGridDelete() 
{
	int ilSelCount = m_NewGrids.GetSelCount();
	int *ipSels;
	bool blHasChanges = false;
	bool blDuplicates = false;
	ipSels = (int*)malloc(ilSelCount*sizeof(int));
	m_NewGrids.GetSelItems(ilSelCount, ipSels);
	for (int ili = 0; ili < ilSelCount; ili++)
	{
		CString olItem;
		m_NewGrids.GetText(ipSels[ili], olItem);
		DeleteDestinationGridByName(olItem);
		blHasChanges = true;
	}
	if(blHasChanges == true)
	{
		InitialFillListBoxes();
	}
	free(ipSels);
}

void ObjectCopyDlg::OnGridToRight() 
{
	int ilSelCount = m_Grids.GetSelCount();
	int *ipSels;
	bool blHasChanges = false;
	bool blDuplicates = false;
	ipSels = (int*)malloc(ilSelCount*sizeof(int));
	m_Grids.GetSelItems(ilSelCount, ipSels);
	for (int ili = 0; ili < ilSelCount; ili++)
	{
		CString olItem;
		m_Grids.GetText(ipSels[ili], olItem);
		if(m_NewGrids.FindStringExact(-1, olItem) == LB_ERR)
		{
			UGridDefinition *polGridDef = GetSourceGridName(olItem);
			if(polGridDef != NULL)
			{
				blHasChanges = true;
				UGridDefinition *polNewGridDef = new UGridDefinition();
				*polNewGridDef = *polGridDef;
				omDestinationGridDefs.Add(polNewGridDef);
			}
		}
		else
		{
			blDuplicates = true;
		}
	}
	if(blHasChanges == true)
	{
		InitialFillListBoxes();
	}
	if(blDuplicates == true)
	{
		AfxMessageBox(LoadStg(IDS_DUPPLICATE_ITEMS));
	}
	free(ipSels);
}

void ObjectCopyDlg::OnFormDelete() 
{
	int ilSelCount = m_NewForms.GetSelCount();
	int *ipSels;
	bool blHasChanges = false;
	bool blDuplicates = false;
	ipSels = (int*)malloc(ilSelCount*sizeof(int));
	m_NewForms.GetSelItems(ilSelCount, ipSels);
	for (int ili = 0; ili < ilSelCount; ili++)
	{
		CString olItem;
		m_NewForms.GetText(ipSels[ili], olItem);
		DeleteDestinationFormByName(olItem);
		blHasChanges = true;
	}
	if(blHasChanges == true)
	{
		InitialFillListBoxes();
	}
	free(ipSels);
}

void ObjectCopyDlg::OnFormToRight() 
{
	int ilSelCount = m_Forms.GetSelCount();
	int *ipSels;
	bool blHasChanges = false;
	bool blDuplicates = false;
	ipSels = (int*)malloc(ilSelCount*sizeof(int));
	m_Forms.GetSelItems(ilSelCount, ipSels);
	for (int ili = 0; ili < ilSelCount; ili++)
	{
		CString olItem;
		m_Forms.GetText(ipSels[ili], olItem);
		if(m_NewForms.FindStringExact(-1, olItem) == LB_ERR)
		{
			UFormProperties *polForm = GetSourceFormName(olItem);
			if(polForm != NULL)
			{
				blHasChanges = true;
				UFormProperties *polNewForm = new UFormProperties();
				*polNewForm = *polForm;
				omDestinationForms.Add(polNewForm);
			}
		}
		else
		{
			blDuplicates = true;
		}
	}
	if(blHasChanges == true)
	{
		InitialFillListBoxes();
	}
	if(blDuplicates == true)
	{
		AfxMessageBox(LoadStg(IDS_DUPPLICATE_ITEMS));
	}
	free(ipSels);
}

void ObjectCopyDlg::OnOK() 
{
	CString olFileName;
	m_FileName.GetWindowText(olFileName);
	if(!olFileName.IsEmpty())
	{
		//SaveConfigAs(olFileName.GetBuffer(0), ogLoadSets, ogGridDefs, ogForms, ogQueries);
		((CUWorkBenchApp*)AfxGetApp())->SaveConfigAs(olFileName.GetBuffer(0),
													  omDestinationLoadSets,
													  omDestinationGridDefs,
													  omDestinationForms,
													  omDestinationQueries);

		CDialog::OnOK();
	}
	else
	{
		AfxMessageBox("Missing filename");
		m_FileName.SetFocus();
	}
}


BOOL ObjectCopyDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	InitialFillListBoxes();
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void ObjectCopyDlg::InitialFillListBoxes()
{
	int i = 0;

	m_Grids.ResetContent();
	m_Forms.ResetContent();
	m_NewGrids.ResetContent();
	m_NewForms.ResetContent();

	for( i = 0; i < omSourceGridDefs.GetSize(); i++)
	{
		m_Grids.AddString(omSourceGridDefs[i].GridName);
	}
	for( i = 0; i < omSourceForms.GetSize(); i++)
	{
		m_Forms.AddString(omSourceForms[i].omName);
	}
	for( i = 0; i < omDestinationGridDefs.GetSize(); i++)
	{
		m_NewGrids.AddString(omDestinationGridDefs[i].GridName);
	}
	for( i = 0; i < omDestinationForms.GetSize(); i++)
	{
		m_NewForms.AddString(omDestinationForms[i].omName);
	}
}

UGridDefinition * ObjectCopyDlg::GetSourceGridName(CString opName)
{
	for(int i = 0; i < omSourceGridDefs.GetSize(); i++)
	{
		if(opName == omSourceGridDefs[i].GridName)
		{
			return &(omSourceGridDefs[i]);
		}
	}
	return NULL;
}

UFormProperties * ObjectCopyDlg::GetSourceFormName(CString opName)
{
	for(int i = 0; i < omSourceForms.GetSize(); i++)
	{
		if(opName == omSourceForms[i].omName)
		{
			return &(omSourceForms[i]);
		}
	}
	return NULL;
}

UGridDefinition * ObjectCopyDlg::GetDestinationGridName(CString opName)
{
	for(int i = 0; i < omDestinationGridDefs.GetSize(); i++)
	{
		if(opName == omDestinationGridDefs[i].GridName)
		{
			return &(omDestinationGridDefs[i]);
		}
	}
	return NULL;
}

UFormProperties * ObjectCopyDlg::GetDestinationFromName(CString opName)
{
	for(int i = 0; i < omDestinationForms.GetSize(); i++)
	{
		if(opName == omDestinationForms[i].omName)
		{
			return &(omDestinationForms[i]);
		}
	}
	return NULL;
}


void ObjectCopyDlg::DeleteDestinationGridByName(CString opName)
{
	for(int i = omDestinationGridDefs.GetSize()-1; i >= 0; i--)
	{
		if(omDestinationGridDefs[i].GridName == opName)
		{
			omDestinationGridDefs.DeleteAt(i);
			return;
		}
	}
}

void ObjectCopyDlg::DeleteDestinationFormByName(CString opName)
{
	for(int i = omDestinationForms.GetSize()-1; i >= 0; i--)
	{
		if(omDestinationForms[i].omName == opName)
		{
			omDestinationForms.DeleteAt(i);
			return;
		}
	}
}

void ObjectCopyDlg::OnBrowse() 
{
	CFileDialog olFileDlg(TRUE, "cfg", "*.cfg", OFN_HIDEREADONLY|OFN_OVERWRITEPROMPT, "*.cfg");
	if(olFileDlg.DoModal() == IDOK)
	{
		SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
		omDestinationFile = olFileDlg.GetPathName();
		m_FileName.SetWindowText(omDestinationFile);
		SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	}
}
