#if !defined(AFX_ULISTVIEW_H__CDDEF225_31E7_11D3_B081_00001C019205__INCLUDED_)
#define AFX_ULISTVIEW_H__CDDEF225_31E7_11D3_B081_00001C019205__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// UListView.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// UListView view
//#ifndef __AFXEXT_H__
//#include <afxext.h>
//#endif

#include <afx.h>
class CListView;

class UListView : public CListView
{
protected:
	UListView();           // protected constructor used by dynamic creation
	DECLARE_DYNCREATE(UListView)

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(UListView)
	public:
	virtual void OnInitialUpdate();
	protected:
	virtual void OnDraw(CDC* pDC);      // overridden to draw this view
	virtual void OnUpdate(CView* pSender, LPARAM lHint, CObject* pHint);
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual ~UListView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	// Generated message map functions
protected:
	//{{AFX_MSG(UListView)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ULISTVIEW_H__CDDEF225_31E7_11D3_B081_00001C019205__INCLUDED_)
