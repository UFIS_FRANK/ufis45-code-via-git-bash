// UFISGridViewerDoc.h : interface of the CUFISGridViewerDoc class
//
/////////////////////////////////////////////////////////////////////////////
#ifndef __GRID_LINE_DATA_H__
#define __GRID_LINE_DATA_H__

#include "CCSGlobl.h"

class GRID_LINE_DATA
{
public:
	const GRID_LINE_DATA &operator =(const GRID_LINE_DATA &opO) 
	{
		this->MasterKeyField = opO.MasterKeyField;
		this->MasterKeyValue = opO.MasterKeyValue;
		this->Values.RemoveAll();
		for(int i = 0; i < opO.Values.GetSize(); i++)
		{
			this->Values.Add(opO.Values[i]);
		}
		return *this;
	}
	CString MasterKeyField; //e.g. URNO
	CString MasterKeyValue;
	CStringArray Values;
};

struct GRID_FIELD_DETAIL_INFO
{
	CString Table;
	CString Field;
	CStringArray Relations;		// 1. DEM.URUD=RUD.URNO 2.RUD.URUE=RUE.URNO
	CStringArray FieldsToShow;	// 1. RUE.RUSN (= Rule Shortname) 2. RUE.RUNA (= Rulename) 3.RUD.RETY (= Rule demand resource type)
	CStringArray HeaderToShow;  // Read from systab RUE.RUSN ==> Addi
	CStringArray FieldTypes;	//TRIM,URNO,DATE,DTIM,TIME,LONG
	CString		 Reference;
};

struct GRID_FIELD_INFO
{
	CString Table;
	//CStringArray Fields;
	CCSPtrArray<GRID_FIELD_DETAIL_INFO> Fields;
	CStringArray Show;		// "Y" => Show, "N" ==> Don not show
	CString		 Type;		// Type of the field
	CStringArray Query;		// If empty ==> a Query is used for the given field 
							// ==> Show is default "N"
	bool IsMasterTable;
	GRID_FIELD_INFO(void)
	{
		IsMasterTable = false;
	}
};

class CUFISGridViewerDoc : public CDocument
{
protected: // create from serialization only
	CUFISGridViewerDoc();
	DECLARE_DYNCREATE(CUFISGridViewerDoc)
// Attributes
public:

// Operations
public:

	UGridDefinition *pomGridDef;

	int imColumnCount;				//How many columns does the grid need

	CString omTable;
	CStringArray						omFieldsList;
	CStringArray						omHeaderList;
	CStringArray						omVisibleList; //0 ==> Don't show it ;  1 ==> Show it

	CStringArray						Relations;

	CCSPtrArray<GRID_LINE_DATA>			omData;
	CCSPtrArray<GRID_FIELD_INFO>		omFieldInfo;
	CMapStringToPtr						omFieldMap;
	CStringArray						omFieldsInVisibleOrder;
	CStringArray						omColumnTypeList; //TRIM,URNO,DATE,DTIM,TIME,LONG

	static CUIntArray							omSelectedColumns;

	GRID_FIELD_INFO *GetFieldInfo(CString opTable);

	void MakeFieldInfo();

	void MakeReferenceTabInfo(CString opRef, CString opVisible);

	void ReadAllData();

	void MakeLines();

	int MakeLines(CStringArray opTables);

	int GetMakeLines(GRID_FIELD_INFO *popTab, 
					 CStringArray &ropFieldList, 
					 CCSPtrArray<GRID_LINE_DATA> &opData, 
					 CString opRefField, 
					 CString opRefValue,
					 GRID_LINE_DATA *popLine,
					 bool bpWasLastField);

	void AppendLineArray(int ipAddLineCount, 
						 GRID_LINE_DATA *polCurrentLine, 
						 CCSPtrArray<GRID_LINE_DATA> &ropCurrentGridLines);


	void MakeLine();

	void SortImmediate(CString opSortOrder);

	void SetGridDef(UGridDefinition *popGridDef);//OLD

	void SetDefinitionInfo(UGridDefinition *popGridDef, bool bpWithRead = true);

	void MakeTabAndFieldFromQuery(UQuery *popQuery);

	void CreateFieldDetailByQuery(GRID_FIELD_DETAIL_INFO* polFieldDetail, CString opQueryName);

	CString GetLeftRelationPart(CString opRelation);

	CString GetRightRelationPart(CString opRelation);

	BOOL CanCloseFrame(CFrameWnd* pFrame);

	CString GetMainTableUrnoFromLineNo(int ipLineNo);

	void UpdateLine(int ipLineNo, CString opUrno);

	void DeleteLine(int ipLineNo, CString opUrno);

	void InsertLine(CString opUrno);

	void  ProcessBroacast(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

	long GetLineNoByUrno(CString opUrno);
//Helper funtions

	//Parameter ==> TAB.FIELD  e.g. AFT.STOA ==> returns AFT
	CString GetTableName(CString opObject);
	//Parameter ==> TAB.FIELD  e.g. AFT.STOA ==> returns STOA
	CString GetFieldName(CString opObject);

	int GetFieldIndex(CString opField);
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CUFISGridViewerDoc)
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
	virtual BOOL OnOpenDocument(LPCTSTR lpszPathName);
	//}}AFX_VIRTUAL


// Implementation
public:
	virtual ~CUFISGridViewerDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CUFISGridViewerDoc)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////
#endif //__GRID_LINE_DATA_H__