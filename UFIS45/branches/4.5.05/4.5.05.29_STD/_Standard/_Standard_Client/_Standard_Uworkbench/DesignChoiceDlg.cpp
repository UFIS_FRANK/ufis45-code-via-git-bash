// DesignChoiceDlg.cpp : implementation file
//

#include "stdafx.h"
#include "UWorkBench.h"
#include "DesignChoiceDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// DesignChoiceDlg dialog


DesignChoiceDlg::DesignChoiceDlg(CWnd* pParent /*=NULL*/)
	: CDialog(DesignChoiceDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(DesignChoiceDlg)
	m_Choice = 0;
	//}}AFX_DATA_INIT
}


void DesignChoiceDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(DesignChoiceDlg)
	DDX_Radio(pDX, IDC_RADIO1, m_Choice);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(DesignChoiceDlg, CDialog)
	//{{AFX_MSG_MAP(DesignChoiceDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// DesignChoiceDlg message handlers

BOOL DesignChoiceDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void DesignChoiceDlg::OnOK() 
{
	// TODO: Add extra validation here
	
	CDialog::OnOK();
}
