// UFISGridViewerView.cpp : implementation of the CUFISGridViewerView class
//

#include "stdafx.h"
#include "UWorkBench.h"

#include "UFISGridViewerDoc.h"
#include "UFISGridViewerView.h"
#include "FormDesignerDoc.h"
#include "FormDesignerView.h"
#include "FormDesignerMDI.h"
#include "FormExecuteDoc.h"
#include "FormDialogDlg.h"
#include "UserViewDefinitionDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
/////////////////// Stingray Software Objective Grid ////////////////////////
/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
// CUFISGridViewerView

IMPLEMENT_DYNCREATE(CUFISGridViewerView, CGXGridView)

BEGIN_MESSAGE_MAP(CUFISGridViewerView, CGXGridView)
	//{{AFX_MSG_MAP(CUFISGridViewerView)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	ON_WM_CREATE()
	ON_COMMAND(31, SortAscending)
	ON_COMMAND(32, SortDescending)
	ON_COMMAND(ID_GRIDSEARCH, SearchInGrid)
	ON_COMMAND(ID_NEW_RECORD, OnNew)
	ON_COMMAND(ID_UPDATE, OnUpdate)
	ON_COMMAND(ID_DELETE, OnDelete)
	ON_COMMAND(ID_COPY, OnCopyRecord)
	ON_COMMAND(ID_LOAD_CONDITION_DLG, OnViewFilter)
	//}}AFX_MSG_MAP
	// Standard printing commands
	ON_COMMAND(ID_FILE_PRINT, CGXGridView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, CGXGridView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, CGXGridView::OnFilePrintPreview)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CUFISGridViewerView construction/destruction

CUFISGridViewerView::CUFISGridViewerView()
{
	// TODO: add construction code here
	imCurrentSelected = -1;
}

CUFISGridViewerView::~CUFISGridViewerView()
{
}

BOOL CUFISGridViewerView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CGXGridView::PreCreateWindow(cs);
}

int CUFISGridViewerView::OnCreate( LPCREATESTRUCT lpCreateStruct )
{
#if _MFC_VER >= 0x0400
	// Enable grid to be used as data source
	EnableOleDataSource();

	// Register the grid as drop target
	VERIFY(m_objDndDropTarget.Register(this));
#endif
	return CGXGridView::OnCreate(lpCreateStruct);
}
/////////////////////////////////////////////////////////////////////////////
// CUFISGridViewerView drawing

void CUFISGridViewerView::OnDraw(CDC* pDC)
{
//	CUFISGridViewerDoc* pDoc = GetDocument();
//	ASSERT_VALID(pDoc);
	CGXGridView::OnDraw(pDC);
	// TODO: add draw code for native data here
}

void CUFISGridViewerView::OnInitialUpdate()
{



#if _MFC_VER >= 0x0400
EnableGridToolTips();
#endif

	SetParam(new CGXGridParam);

	// Lock any drawing
	BOOL bOld = LockUpdate();

	// Custom initialization of styles map
	if (GetParam()->GetStylesMap() == NULL)
	{
		// create a stylesmap and connect it with the parameter-object
		CGXStylesMap* pStyMap = new CGXStylesMap;
		
		GetParam()->SetStylesMap(pStyMap);

		// add standard styles
		pStyMap->CreateStandardStyles();
		


		SetupUserAttributes();

		// If you want to specify a specific profile section
		// for load and storing the base styles in the registry,
		// simply uncomment the next line
		// pStyMap->SetSection(_T("My Base Styles"));  // extra profile section

		pStyMap->ReadProfile();
	}

	// Custom initialization of property object
	if (GetParam()->GetProperties() == NULL)
	{
		// create a stylesmap and connect it with the parameter-object
		CGXProperties* pProperties = new CGXProperties;

		pProperties->AddDefaultUserProperties();


		pProperties->ReadProfile();

		GetParam()->SetProperties(pProperties);
	}

	// Custom initialization of printer settings
	if (GetParam()->GetPrintDevice() == NULL)
	{
		// Initialize printer object if you want to have
		// some default settings for the grid printing, e.g. Print Landscape
	}

	CGXGridView::OnInitialUpdate();	
	GetParam()->SetSortColsOnDblClk(TRUE) ;

	GetParam()->SetSpecialMode(GX_MODELBOX_SS);
	//SetReadOnly(TRUE);
	//GetParam()->SetLockReadOnly(TRUE);
	LockUpdate(bOld);

	// Just to be sure that everything is redrawn
	Invalidate();

	// Enable Objective Grid internal update-hint mechanism

	// You should put this line as last command into OnInitialUpdate,
	// becaus as long as EnableHints is not called, the modified flag
	// of the document will not be changed.

	EnableHints();
}


void CUFISGridViewerView::OnUpdate( CView* pSender, LPARAM lHint, CObject* pHint )
{
	CUFISGridViewerDoc* polDoc = GetDocument();
//	int i,j;
	int ilRowCount, ilColCount;
	BOOL bOld = LockUpdate();

	SetReadOnly(FALSE);
	if(polDoc != NULL)
	{
		CGXStyle olStyle;

		ilRowCount = polDoc->omData.GetSize();
		ilColCount = polDoc->omFieldsList.GetSize(); //imColumnCount;
		SetRowCount(ilRowCount);
		SetRowHeight(0, 0, 40);
		SetColCount(ilColCount);
		if(polDoc->pomGridDef != NULL)
		{
			int ilColCount = GetColCount();
			for(int i = 1; i <= polDoc->pomGridDef->omColWidths.GetSize(); i++)
			{
				int ilW = polDoc->pomGridDef->omColWidths[i-1];
				SetColWidth(i,i,polDoc->pomGridDef->omColWidths[i-1]);
			}
			//SetEnabled(BOOL b);
			for(i = 0; i < polDoc->omVisibleList.GetSize(); i++)
			{
				if(polDoc->omVisibleList[i] == CString("0"))
				{
					//SetStyleRange(CGXRange(0, i+1),
					//	CGXStyle().SetEnabled(FALSE));
					//SetColWidth(i+1,i+1, 0);
					HideCols(i+1,i+1);
				}
			}

		}
	}
	LockUpdate(bOld);
	ilRowCount = GetRowCount();
	ilColCount = GetColCount();
	Invalidate();

	SetReadOnly(TRUE);

}

void CUFISGridViewerView::OnGridClick(WPARAM wParam, LPARAM lParam)
{
}

void CUFISGridViewerView::SetupUserAttributes()
{
		CGXStylesMap* stylesmap = GetParam()->GetStylesMap();
	
		// Register user attributes for the CGXStyleSheet-User Attributes page.
		// (control objects will be created on demand in CGXControlFactory).
		stylesmap->RegisterDefaultUserAttributes();
}




/////////////////////////////////////////////////////////////////////////////
// CUFISGridViewerView printing

BOOL CUFISGridViewerView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// default preparation
	return DoPreparePrinting(pInfo);
}

void CUFISGridViewerView::OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo)
{
	CGXGridView::OnBeginPrinting(pDC, pInfo);
	// TODO: add extra initialization before printing
}

void CUFISGridViewerView::OnEndPrinting(CDC* pDC, CPrintInfo* pInfo)
{
	CGXGridView::OnEndPrinting(pDC, pInfo);

	// TODO: add cleanup after printing
}

BOOL CUFISGridViewerView::OnRButtonDblClkRowCol(ROWCOL nRow, ROWCOL nCol, UINT nFlags, CPoint pt)
{
	return TRUE;
}

BOOL CUFISGridViewerView::GetStyleRowCol(ROWCOL nRow, ROWCOL nCol, CGXStyle& style, GXModifyType mt, int nType)
{   
	if (mt == gxRemove) // not supported      
		return FALSE;
   // Note: I do not distinct between gxApplyNew, gxCopy and gxOverride
   ASSERT(nRow <= LONG_MAX);   
   //GetParam()->SetReadOnly(FALSE);
   long nRecord = (long) nRow;   
   if (nType == -1)
   {      
	   // here you can return the style for a complete row, column or table
      return FALSE;   
   }   
   else if (nRow == 0 && nCol == 0)   
   {
      // style for the top-left button      
	   return FALSE;   
   }
//   else if (nRow == 0)   
//   {      // Column headers      
//	   style.SetValue(nCol);   
//   }
   else    
   {      // Cell in the grid
/*		if(GetRowCount() > 1 && GetColCount() > 1)
		{
		SetStyleRange(CGXRange(1, 1, GetRowCount(), GetColCount()), 
						CGXStyle().SetEnabled(TRUE)
								  .SetReadOnly(FALSE));
		}
*/
	  if(nRow == 0)
	  {
		  if(nCol != 0)
		  {
			 //omFieldsList
			//style.SetValue(GetDocument()->omFieldsList[nCol-1]);
			style.SetValue(GetDocument()->omHeaderList[nCol-1]);
		  }
	  }
	  else if(nCol == 0)
	  {
		  if(nRow != 0)
		  {
 			 CString olText;
			 olText.Format("%d", nRow);
			 style.SetValue(olText);
		  }
	  }
	  else 
	  {
		if((nCol-1) < (UINT)GetDocument()->omData[nRow-1].Values.GetSize())
		{
//			style.SetValue(GetDocument()->omData[nRow-1].Values[nCol-1]);
			if(GetDocument()->omColumnTypeList[nCol-1] == CString("TRIM"))
			{
				style.SetValue(GetDocument()->omData[nRow-1].Values[nCol-1]);
			}
			else if(GetDocument()->omColumnTypeList[nCol-1] == CString("DATE"))
			{
				CString olValue = GetDocument()->omData[nRow-1].Values[nCol-1];
				CTime olTime;
				olTime = DBStringToDateTime(olValue);
				if(olTime != TIMENULL)
				{
					olValue = olTime.Format("%d.%m.%Y");
					style.SetValue(olValue);
				}
			}
			else if(GetDocument()->omColumnTypeList[nCol-1] == CString("TIME"))
			{
				CString olValue = GetDocument()->omData[nRow-1].Values[nCol-1];
				CTime olTime;
				olTime = DBStringToDateTime(olValue);
				if(olTime != TIMENULL)
				{
					olValue = olTime.Format("%H:%M");
					style.SetValue(olValue);
				}
			}
			else if(GetDocument()->omColumnTypeList[nCol-1] == CString("DTIM"))
			{
				CString olValue = GetDocument()->omData[nRow-1].Values[nCol-1];
				CTime olTime;
				olTime = DBStringToDateTime(olValue);
				if(olTime != TIMENULL)
				{
					olValue = olTime.Format("%d.%m.%Y - %H:%M");
					style.SetValue(olValue);
				}
			}
			else
			{
				style.SetValue(GetDocument()->omData[nRow-1].Values[nCol-1]);
			}
		}
		else
		{
			style.SetValue("");
		}
	  }
      //style.SetValue(myData[nRow][nCol].text);      
	  // - Or -
	  //GetParam()->SetReadOnly(TRUE);
/*		if(GetRowCount() > 1 && GetColCount() > 1)
		{
			SetStyleRange(CGXRange(1, 1, GetRowCount(), GetColCount()), 
							CGXStyle().SetEnabled(FALSE)
									  .SetReadOnly(TRUE));
		}
*/
	  return TRUE;   
   }   
   //GetParam()->SetReadOnly(TRUE);
   return FALSE;
}
BOOL CUFISGridViewerView::StoreStyleRowCol(ROWCOL nRow, ROWCOL nCol, const CGXStyle* pStyle, GXModifyType mt, int nType)
{   
   SetReadOnly(FALSE);
   GetParam()->SetLockReadOnly(FALSE);
   if (mt == gxRemove) // not supported      
		return FALSE;
   // Note: I do not distinct between gxApplyNew, gxCopy and gxOverride
   if (nType == -1)   
   {
      // here you could store the style for a complete row, column or table
      return FALSE;   
   }   
   else if (nRow == 0 && nCol == 0)   
   {
      // style for the top-left button      
	   return FALSE;   
   }
   else if (nRow == 0)   
   {      // Column headers      
	   return FALSE;   
   }   
   else 
   {      // cell      
	  GetDocument()->omData[nRow-1].Values[nCol-1] = pStyle->GetValue();
	   //myData[nRow][nCol].text = style.GetValue();
      return TRUE;   
   }   // unreferenced:   
   mt;   
   SetReadOnly(TRUE);
   GetParam()->SetLockReadOnly(TRUE);
   return FALSE;
}
ROWCOL CUFISGridViewerView::GetRowCount()
{
	return GetDocument()->omData.GetSize();
}

BOOL CUFISGridViewerView::OnTrackColWidth(ROWCOL nCol)
{
	if(IsColHidden(nCol) == TRUE)
	{
		return FALSE;
	}

	return TRUE;
}

BOOL CUFISGridViewerView::OnTrackColWidthEnd(ROWCOL nCol, int nWidth)
{
	BOOL blRet = CGXGridView::OnTrackColWidthEnd(nCol, nWidth);
	if(GetDocument()->pomGridDef != NULL)
	{
		GetDocument()->pomGridDef->omColWidths.RemoveAll();
		int ilColCount = GetColCount();
		for(int i = 1; i <= ilColCount; i++)
		{
			if((UINT)i == nCol)
			{
				GetDocument()->pomGridDef->omColWidths.Add(nWidth);
			}
			else
			{
				int ilW = GetColWidth(i);
				GetDocument()->pomGridDef->omColWidths.Add(ilW);
			}
		}
	}
	return blRet;
}

BOOL CUFISGridViewerView::OnLButtonClickedRowCol(ROWCOL nRow, ROWCOL nCol, UINT nFlags, CPoint pt)
{
	BOOL blRet;
	if(nRow == 0 && nCol == 0)
	{
		return FALSE;
	}
	if(nFlags & MK_CONTROL )
	{
		blRet = CGXGridView::OnLButtonClickedRowCol(nRow, nCol, nFlags, pt);
		bool blFound = false;
		if(nRow == 0)
		{
			for(int i = 0; (blFound == false) && (i < GetDocument()->omSelectedColumns.GetSize()); i++)
			{
				if(GetDocument()->omSelectedColumns[i] == (nCol-1))
				{
					blFound = true;
				}
			}

			if(blFound == false)
			{
				GetDocument()->omSelectedColumns.Add(nCol-1);
			}
		}
		else
		{
			//GetDocument()->omSelectedColumns.RemoveAll();
//			ROWCOL ilRow = ((CELLPOS*)lParam)->Row;
			//SelectRange(CGXRange(nRow, 0, nRow, GetColCount()), TRUE);
		}
		imCurrentSelected = -1;
	}
	else if(!(nFlags & MK_SHIFT))
	{
		blRet = CGXGridView::OnLButtonClickedRowCol(nRow, nCol, nFlags, pt);
		GetDocument()->omSelectedColumns.RemoveAll();
		bool blFound = false;
		if(nRow == 0)
		{
			for(int i = 0; (blFound == false) && (i < GetDocument()->omSelectedColumns.GetSize()); i++)
			{
				if(GetDocument()->omSelectedColumns[i] == (nCol-1))
				{
					blFound = true;
				}
			}

			if(blFound == false)
			{
				GetDocument()->omSelectedColumns.Add(nCol-1);
			}
			imCurrentSelected = -1;
		}
		else // Selection of line/row for update/delet
		{
			if(nCol == 0)
			{
				imCurrentSelected = nRow - 1;
			}
			else
			{
				imCurrentSelected = -1;
//				ROWCOL ilRow = ((CELLPOS*)lParam)->Row;
			//	SelectRange(CGXRange(nRow, 0, nRow, GetColCount()), TRUE);
			}
		}
		
	}
	return blRet;
}

BOOL CUFISGridViewerView::OnRButtonClickedRowCol(ROWCOL nRow, ROWCOL nCol, UINT nFlags, CPoint pt)
{
	CMenu menu;
	menu.CreatePopupMenu();
	for (int i = menu.GetMenuItemCount(); i > 0; i--)
	     menu.RemoveMenu(i, MF_BYPOSITION);
    menu.AppendMenu(MF_STRING,31, "Sort Ascending");	// Confirm
    menu.AppendMenu(MF_STRING,32, "Sort Descending");	// Confirm

//	CPoint olPoint(LOWORD(lParam),HIWORD(lParam));
	ClientToScreen(&pt);
    menu.TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, pt.x, pt.y, this, NULL);
	return TRUE;
}
void CUFISGridViewerView::SortAscending()
{
	GetDocument()->SortImmediate(CString("ASC"));
	GetDocument()->pomGridDef->omSortColumns.RemoveAll();
	for(int i = 0; i < GetDocument()->omSelectedColumns.GetSize(); i++)
	{
		GetDocument()->pomGridDef->omSortColumns.Add(GetDocument()->omSelectedColumns[i]);
	}
	GetDocument()->pomGridDef->omSortDirection = CString("ASC");
}

void CUFISGridViewerView::SortDescending()
{
	GetDocument()->SortImmediate(CString("DESC"));
	GetDocument()->pomGridDef->omSortColumns.RemoveAll();
	for(int i = 0; i < GetDocument()->omSelectedColumns.GetSize(); i++)
	{
		GetDocument()->pomGridDef->omSortColumns.Add(GetDocument()->omSelectedColumns[i]);
	}
	GetDocument()->pomGridDef->omSortDirection = CString("DESC");
}

BOOL CUFISGridViewerView::OnLButtonDblClkRowCol(ROWCOL nRow, ROWCOL nCol, UINT nFlags, CPoint pt)
{

	BOOL blRet = CGXGridView::OnLButtonDblClkRowCol(nRow, nCol, nFlags, pt);
	CUFISGridViewerDoc* polDoc = GetDocument();
	if(polDoc != NULL)
	{
		if(polDoc->pomGridDef->AsBasicData == TRUE)
		{
			if(!polDoc->pomGridDef->FormName.IsEmpty())
			{
				UFormProperties *polForm = NULL;
				for(int i = 0; i < ogForms.GetSize(); i++)
				{
					if(ogForms[i].omName == polDoc->pomGridDef->FormName)
					{
						polForm = &ogForms[i];
						i = ogForms.GetSize();
					}
				}
				if(polForm != NULL)
				{
					if(polForm->omTable != polDoc->pomGridDef->omTables[0])
					{
						//Tables zwischen Grid und Form passen nicht zusammen
						AfxMessageBox("Tables of Grid and Form do not fit!!");
					}
					else
					{
						if(polForm->bmModal == TRUE)
						{
							CString olUrno;
							olUrno = polDoc->GetMainTableUrnoFromLineNo(nRow-1);
							if(!olUrno.IsEmpty())
							{
								FormDialogDlg *polDlg = new FormDialogDlg(this, "UPDATE", polForm, olUrno, polDoc->pomGridDef);
								if(polDlg->DoModal() == IDOK)
								{
									polDoc->UpdateLine(nRow-1, olUrno);
								}
								delete polDlg;
							}
						}
						else
						{
							FormExecuteDoc *polDoc;
							polDoc = (FormExecuteDoc*)((CMultiDocTemplate*)((CUWorkBenchApp*)AfxGetApp())->pFormExecuteTpl->OpenDocumentFile((LPCTSTR )polForm));
						}
					}
				}

			}
		}
	}
	SelectRange(CGXRange(nRow, 0, nRow, GetColCount()), TRUE);
	return blRet;

}

void CUFISGridViewerView::OnNew()
{
	CUFISGridViewerDoc* polDoc = GetDocument();
	if(polDoc != NULL)
	{
		if(polDoc->pomGridDef->AsBasicData == TRUE)
		{
			if(!polDoc->pomGridDef->FormName.IsEmpty())
			{
				UFormProperties *polForm = NULL;
				for(int i = 0; i < ogForms.GetSize(); i++)
				{
					if(ogForms[i].omName == polDoc->pomGridDef->FormName)
					{
						polForm = &ogForms[i];
						i = ogForms.GetSize();
					}
				}
				if(polForm != NULL)
				{
					if(polForm->omTable != polDoc->pomGridDef->omTables[0])
					{
						//Tables zwischen Grid und Form passen nicht zusammen
						AfxMessageBox("Tables of Grid and Form do not fit!!");
					}
					else
					{
						if(polForm->bmModal == TRUE)
						{
							CString olUrno;
							olUrno = CString("");
 							FormDialogDlg *polDlg = new FormDialogDlg(this, "INSERT", polForm, olUrno, polDoc->pomGridDef);
							if(polDlg->DoModal() == IDOK)
							{
 								polDoc->InsertLine(polDlg->omKey);
							}
							delete polDlg;
						}
						else
						{
							FormExecuteDoc *polDoc;
							polDoc = (FormExecuteDoc*)((CMultiDocTemplate*)((CUWorkBenchApp*)AfxGetApp())->pFormExecuteTpl->OpenDocumentFile((LPCTSTR )polForm));
						}
					}
				}

			}
		}
	}
}

void CUFISGridViewerView::OnCopyRecord()
{
	if(imCurrentSelected == -1)
	{
		AfxMessageBox("Please select a line/record!!");
	}
	else
	{
		CUFISGridViewerDoc* polDoc = GetDocument();
		if(polDoc != NULL)
		{
			if(polDoc->pomGridDef->AsBasicData == TRUE)
			{
				if(!polDoc->pomGridDef->FormName.IsEmpty())
				{
					UFormProperties *polForm = NULL;
					for(int i = 0; i < ogForms.GetSize(); i++)
					{
						if(ogForms[i].omName == polDoc->pomGridDef->FormName)
						{
							polForm = &ogForms[i];
							i = ogForms.GetSize();
						}
					}
					if(polForm != NULL)
					{
						if(polForm->omTable != polDoc->pomGridDef->omTables[0])
						{
							//Tables zwischen Grid und Form passen nicht zusammen
							AfxMessageBox("Tables of Grid and Form do not fit!!");
						}
						else
						{
							if(polForm->bmModal == TRUE)
							{
								CString olUrno;
								olUrno = polDoc->GetMainTableUrnoFromLineNo(imCurrentSelected);
								if(!olUrno.IsEmpty())
								{
									FormDialogDlg *polDlg = new FormDialogDlg(this, "COPY", polForm, olUrno, polDoc->pomGridDef);
									if(polDlg->DoModal() == IDOK)
									{
										polDoc->UpdateLine(imCurrentSelected, olUrno);
									}
									delete polDlg;
								}
							}
							else
							{
								FormExecuteDoc *polDoc;
								polDoc = (FormExecuteDoc*)((CMultiDocTemplate*)((CUWorkBenchApp*)AfxGetApp())->pFormExecuteTpl->OpenDocumentFile((LPCTSTR )polForm));
							}
						}
					}

				}
			}// As Basic Data
			else
			{
				//TO DO
			}// Not as Basic Data
		}
	}
}

void CUFISGridViewerView::OnUpdate()
{
	if(imCurrentSelected == -1)
	{
		AfxMessageBox("Please select a line/record!!");
	}
	else
	{
		CUFISGridViewerDoc* polDoc = GetDocument();
		if(polDoc != NULL)
		{
			if(polDoc->pomGridDef->AsBasicData == TRUE)
			{
				if(!polDoc->pomGridDef->FormName.IsEmpty())
				{
					UFormProperties *polForm = NULL;
					for(int i = 0; i < ogForms.GetSize(); i++)
					{
						if(ogForms[i].omName == polDoc->pomGridDef->FormName)
						{
							polForm = &ogForms[i];
							i = ogForms.GetSize();
						}
					}
					if(polForm != NULL)
					{
						if(polForm->omTable != polDoc->pomGridDef->omTables[0])
						{
							//Tables zwischen Grid und Form passen nicht zusammen
							AfxMessageBox("Tables of Grid and Form do not fit!!");
						}
						else
						{
							if(polForm->bmModal == TRUE)
							{
								CString olUrno;
								olUrno = polDoc->GetMainTableUrnoFromLineNo(imCurrentSelected);
								if(!olUrno.IsEmpty())
								{
									FormDialogDlg *polDlg = new FormDialogDlg(this, "UPDATE", polForm, olUrno, polDoc->pomGridDef);
									if(polDlg->DoModal() == IDOK)
									{
										polDoc->UpdateLine(imCurrentSelected, olUrno);
									}
									delete polDlg;
								}
							}
							else
							{
								FormExecuteDoc *polDoc;
								polDoc = (FormExecuteDoc*)((CMultiDocTemplate*)((CUWorkBenchApp*)AfxGetApp())->pFormExecuteTpl->OpenDocumentFile((LPCTSTR )polForm));
							}
						}
					}

				}
			}// As Basic Data
			else
			{
				//TO DO
			}// Not as Basic Data
		}
	}
}
void CUFISGridViewerView::OnDelete()
{
	if(imCurrentSelected > -1)
	{
		CUFISGridViewerDoc* polDoc = GetDocument();
		if(polDoc != NULL)
		{
			CString olUrno;
			olUrno = polDoc->GetMainTableUrnoFromLineNo(imCurrentSelected);
			if(!olUrno.IsEmpty())
			{
				polDoc->DeleteLine(imCurrentSelected, olUrno);
				//ogBCD.DeleteRecord(CString opObject, CString opRefField, CString opRefValue, bool bpDbSave = false);
			}
		}//if(polDoc != NULL)
	}
	else
	{
		//TO DO
		AfxMessageBox("No line selected!\n\n Please select a line/record!!");
	}
}

void CUFISGridViewerView::OnViewFilter()
{
	CUFISGridViewerDoc* polDoc = GetDocument();
	if(polDoc != NULL)
	{
		UserViewDefinitionDlg *polDlg = new UserViewDefinitionDlg(this, polDoc->pomGridDef);
		if(polDlg->DoModal() == IDOK)
		{
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			ogBCD.Read(polDoc->pomGridDef->omTables[0], polDoc->pomGridDef->Where);
			//ogTools.SetTablesForGridAndForm(polDoc->pomGridDef, true);
			polDoc->SetDefinitionInfo(polDoc->pomGridDef, false);
			//((CUWorkBenchApp*)AfxGetApp())->pMainFrame->OnShowUFISGrid(polGridDef);
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
		}
		delete polDlg;
	}
}

//-------------------------------------
// Suchfunktionalit�t im Grid benutzen
//-------------------------------------
void CUFISGridViewerView::SearchInGrid()
{
	OnShowFindReplaceDialog(TRUE);
}
//------------------------------------------------------
// Suchfunktionalit�t:
// Diese Methode mu� �berschrieben werden,
// da die Suche �ber alle Spalten und nicht nur
// �ber die aktuelle gehen soll
//------------------------------------------------------
void CUFISGridViewerView::OnTextNotFound(LPCTSTR)
{
	ROWCOL		ilRow, ilCol, ilColAnz;
	BOOL		blFound = FALSE;
	ilColAnz = GetColCount();
	GX_FR_STATE	*pslState = GXGetLastFRState();

	if ( GetCurrentCell( &ilRow, &ilCol ) && pslState )
	{
		if ( pslState->bNext )		//  d.h Abw�rts suchen
		{
			
			while ( !blFound && ( ilCol < ilColAnz ) ) //  und noch nicht in letzter Spalte
			{
				ilCol ++;
				ilRow = GetHeaderRows ()+1;
				blFound = FindText( ilRow, ilCol, true ) ;
			}
			if (!blFound) 
			{ 
				ROWCOL i=1;
				blFound = FindText(i,ilCol,true);
			}

		}
		else 
		{	//  d.h Aufw�rts suchen
			
			while ( !blFound &&  ( ilCol > GetHeaderCols () + 1 ) )	
			{
				ilCol --;
				ilRow = GetRowCount ();
				blFound = FindText( ilRow, ilCol, true );
			}
			if (!blFound)
			{
				ROWCOL i =GetRowCount();
				blFound = FindText(i,ilCol,true);
			}
		}
		if ( blFound )
			return;

	}
	MessageBeep(0);
}

/////////////////////////////////////////////////////////////////////////////
// CUFISGridViewerView diagnostics

#ifdef _DEBUG
void CUFISGridViewerView::AssertValid() const
{
	CGXGridView::AssertValid();
}

void CUFISGridViewerView::Dump(CDumpContext& dc) const
{
	CGXGridView::Dump(dc);
}

CUFISGridViewerDoc* CUFISGridViewerView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CUFISGridViewerDoc)));
	return (CUFISGridViewerDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CUFISGridViewerView message handlers
