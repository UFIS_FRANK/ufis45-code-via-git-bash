// GridView.h : interface of the CGridView class
//
/////////////////////////////////////////////////////////////////////////////
#ifndef __CGridView__
#define __CGridView__


#include "UWorkBenchDoc.h"

const int gxnFirstCtrl = 101;

class CGridView : public CGXGridView
{
protected: // create from serialization only
	CGridView();
	DECLARE_DYNCREATE(CGridView)

// Attributes
public:
	CUWorkBenchDoc* GetDocument();

// Operations
public:
//added for initialization 
	void SetupUserAttributes();
#if _MFC_VER >= 0x0400
	CGXGridDropTarget m_objDndDropTarget;
#endif


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CGridView)
	public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	protected:
	virtual void OnInitialUpdate(); // called first time after construct
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);


	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CGridView();
//#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
//#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CGridView)
	afx_msg int OnCreate( LPCREATESTRUCT lpCreateStruct );
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//#ifndef _DEBUG  // debug version in GridView.cpp
//inline CGridDoc* CGridView::GetDocument()
   //{ return (CGridDoc*)m_pDocument; }
//#endif

/////////////////////////////////////////////////////////////////////////////
#endif //__CGridView__