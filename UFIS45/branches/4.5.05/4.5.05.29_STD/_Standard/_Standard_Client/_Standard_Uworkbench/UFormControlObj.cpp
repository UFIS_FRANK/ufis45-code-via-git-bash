#include "stdafx.h"

#include "CCSGlobl.h"
#include "FormDesignerView.h"
#include "FormDesignerDoc.h"

#include "UFormControlObj.h"
#include "PropertiesDlg.h"
#include "Tools.h"

UFormControlObj::UFormControlObj()
{
	pomProperties = NULL;
}

UFormControlObj::~UFormControlObj()
{
	if(	pomProperties != NULL)
	{
//		delete pomProperties;
	}
}

UFormControlObj::UFormControlObj(const CRect& ropPosition)
{
	omPosition = ropPosition;
	pomDocument = NULL;

	omPen = TRUE;
	omLogPen.lopnStyle = PS_INSIDEFRAME;
	omLogPen.lopnWidth.x = 1;
	omLogPen.lopnWidth.y = 1;
	omLogPen.lopnColor = RGB(0, 0, 0);

	omBrush = TRUE;
	omLogBrush.lbStyle = BS_SOLID;
	omLogBrush.lbColor = RGB(192, 192, 192);
	omLogBrush.lbHatch = HS_HORIZONTAL;
}

void UFormControlObj::Remove()
{
	delete this;
}

void UFormControlObj::Draw(CDC*, FormDesignerView* popView)
{
}

void UFormControlObj::DrawTracker(CDC* popDC, TrackerState state)
{
	ASSERT_VALID(this);

	switch (state)
	{
	case normal:
		break;

	case selected:
	case active:
		{
			int nHandleCount = GetHandleCount();
			for (int nHandle = 1; nHandle <= nHandleCount; nHandle += 1)
			{
				CPoint handle = GetHandle(nHandle);
				popDC->PatBlt(handle.x - 3, handle.y - 3, 7, 7, DSTINVERT);
			}
		}
		break;
	}
}

// position is in logical
void UFormControlObj::MoveTo(const CRect& ropPosition, FormDesignerView* popView)
{
	ASSERT_VALID(this);

	if (ropPosition == omPosition) 
		return;

	if (popView == NULL)
	{
		Invalidate();
		omPosition = ropPosition;
//		omPosition.NormalizeRect();
		pomProperties->omRect = omPosition;
		pomProperties->omRect.NormalizeRect();
		Invalidate();
	}
	else
	{
		popView->InvalObj(this);
		omPosition = ropPosition;
//		omPosition.NormalizeRect();
		pomProperties->omRect = omPosition;
		pomProperties->omRect.NormalizeRect();
		popView->InvalObj(this);
	}
	pomDocument->SetModifiedFlag();
}
 
// Note: if bSelected, hit-codes start at one for the top-left
// and increment clockwise, 0 means no hit.
// If !bSelected, 0 = no hit, 1 = hit (anywhere)

// point is in logical coordinates
int UFormControlObj::HitTest(CPoint opPoint, FormDesignerView* popView, BOOL bpSelected)
{
	ASSERT_VALID(this);
	ASSERT(popView != NULL);

	CPoint olPoint = opPoint;
	//popView->ClientToDoc(olPoint);
	if (bpSelected)
	{
		int ilHandleCount = GetHandleCount();
		for (int ilHandle = 1; ilHandle <= ilHandleCount; ilHandle += 1)
		{
			// GetHandleRect returns in logical coords
			CRect olRect = GetHandleRect(ilHandle,popView);
			if (olPoint.x >= olRect.left && olPoint.x < olRect.right &&
				 olPoint.y >= olRect.top && olPoint.y < olRect.bottom)
				return ilHandle;
//			if (opPoint.x >= olRect.left && opPoint.x < olRect.right &&
//				 opPoint.y <= olRect.top && opPoint.y > olRect.bottom)
//				return ilHandle;
		}
	}
	else
	{
		if (opPoint.x >= omPosition.left && opPoint.x < omPosition.right &&
			opPoint.y >= omPosition.top && opPoint.y < omPosition.bottom)
//		if (opPoint.x >= omPosition.left && opPoint.x < omPosition.right &&
//			opPoint.y <= omPosition.top && opPoint.y > omPosition.bottom)

			return 1;
	}
	return 0;
}

// rect must be in logical coordinates
BOOL UFormControlObj::Intersects(const CRect& ropRect)
{
	ASSERT_VALID(this);

	CRect olFixedRect = omPosition;
	olFixedRect.NormalizeRect();
	CRect olRectT = ropRect;
	olRectT.NormalizeRect();
	return !(olRectT & olFixedRect).IsRectEmpty();
}

int UFormControlObj::GetHandleCount()
{
	ASSERT_VALID(this);
	return 8;
}

// returns logical coords of center of handle
CPoint UFormControlObj::GetHandle(int ipHandle)
{
	ASSERT_VALID(this);
	int x, y, xCenter, yCenter;

	// this gets the center regardless of left/right and top/bottom ordering
	xCenter = omPosition.left + omPosition.Width() / 2;
	yCenter = omPosition.top + omPosition.Height() / 2;

	switch (ipHandle)
	{
	default:
		ASSERT(FALSE);

	case 1:
		x = omPosition.left;
		y = omPosition.top;
		break;

	case 2:
		x = xCenter;
		y = omPosition.top;
		break;

	case 3:
		x = omPosition.right;
		y = omPosition.top;
		break;

	case 4:
		x = omPosition.right;
		y = yCenter;
		break;

	case 5:
		x = omPosition.right;
		y = omPosition.bottom;
		break;

	case 6:
		x = xCenter;
		y = omPosition.bottom;
		break;

	case 7:
		x = omPosition.left;
		y = omPosition.bottom;
		break;

	case 8:
		x = omPosition.left;
		y = yCenter;
		break;
	}

	return CPoint(x, y);
}

// return rectange of handle in logical coords
CRect UFormControlObj::GetHandleRect(int ipHandleID, FormDesignerView* popView)
{
	ASSERT_VALID(this);
	ASSERT(popView != NULL);

	CRect olRect;
	// get the center of the handle in logical coords
	CPoint olPoint = GetHandle(ipHandleID);
	// convert to client/device coords
	popView->DocToClient(olPoint);
	// return CRect of handle in device coords
	olRect.SetRect(olPoint.x-3, olPoint.y-3, olPoint.x+3, olPoint.y+3);
	popView->ClientToDoc(olRect);

	return olRect;
}

HCURSOR UFormControlObj::GetHandleCursor(int ipHandle)
{
	ASSERT_VALID(this);

	LPCTSTR id;
	switch (ipHandle)
	{
	default:
		ASSERT(FALSE);

	case 1:
	case 5:
		id = IDC_SIZENWSE;
		break;

	case 2:
	case 6:
		id = IDC_SIZENS;
		break;

	case 3:
	case 7:
		id = IDC_SIZENESW;
		break;

	case 4:
	case 8:
		id = IDC_SIZEWE;
		break;
	}

	return AfxGetApp()->LoadStandardCursor(id);
}

// point must be in logical
void UFormControlObj::MoveHandleTo(int ipHandle, CPoint opPoint, FormDesignerView* popView)
{
	ASSERT_VALID(this);

	CRect olPosition = omPosition;
	switch (ipHandle)
	{
	default:
		ASSERT(FALSE);

	case 1:
		olPosition.left = opPoint.x;
		olPosition.top = opPoint.y;
		break;

	case 2:
		olPosition.top = opPoint.y;
		break;

	case 3:
		olPosition.right = opPoint.x;
		olPosition.top = opPoint.y;
		break;

	case 4:
		olPosition.right = opPoint.x;
		break;

	case 5:
		olPosition.right = opPoint.x;
		olPosition.bottom = opPoint.y;
		break;

	case 6:
		olPosition.bottom = opPoint.y;
		break;

	case 7:
		olPosition.left = opPoint.x;
		olPosition.bottom = opPoint.y;
		break;

	case 8:
		olPosition.left = opPoint.x;
		break;
	}

	MoveTo(olPosition, popView);
}

void UFormControlObj::Invalidate()
{
	ASSERT_VALID(this);
	pomDocument->UpdateAllViews(NULL, HINT_UPDATE_DRAWOBJ, this);
}


void UFormControlObj::OnEditProperties(FormDesignerView* popView)
{
}

void UFormControlObj::OnOpen(FormDesignerView* pView )
{
	OnEditProperties(pView);
}

void UFormControlObj::SetLineColor(COLORREF color)
{
	ASSERT_VALID(this);

	omLogPen.lopnColor = color;
	Invalidate();
	pomDocument->SetModifiedFlag();
}

void UFormControlObj::SetFillColor(COLORREF color)
{
	ASSERT_VALID(this);

	omLogBrush.lbColor = color;
	Invalidate();
	pomDocument->SetModifiedFlag();
}

UFormControlObj* UFormControlObj::Clone(FormDesignerDoc* pDoc, UFormControlObj* pObj)
{
	ASSERT_VALID(this);

	UFormControlObj* pClone = new UFormControlObj(omPosition);
	pClone->omPen = omPen;
	pClone->omLogPen = omLogPen;
	pClone->omBrush = omBrush;
	pClone->omLogBrush = omLogBrush;
	
	ASSERT_VALID(pClone);

	if (pDoc != NULL)
		pDoc->Add(pClone);

	return pClone;
}

void UFormControlObj::ShowPropertyDlg(UCtrlProperties* popProperty, UFormControlObj *popCtrlObj, FormDesignerView* popView)
{
	if(pogPropertiesDlg == NULL)
	{
		pogPropertiesDlg = new PropertiesDlg(NULL, popProperty, popCtrlObj, popView->GetDocument()->pomForm->omTable, popView);
	}
	else
	{
		pogPropertiesDlg->SetObjectProperties(popProperty, popCtrlObj, popView->GetDocument()->pomForm->omTable, popView);
	}
}

////Control Edit

UFormEditControl::UFormEditControl()
{
}

UFormEditControl::UFormEditControl(const CRect& ropPosition)
	: UFormControlObj(ropPosition)
{
	ASSERT_VALID(this);

}

void UFormEditControl::OnEditProperties(FormDesignerView* popView)
{
	ShowPropertyDlg(pomProperties, (UFormControlObj *)this, popView);
}

void UFormEditControl::Draw(CDC* popDC, FormDesignerView* popView)
{
	ASSERT_VALID(this);

//	CBrush brush;
	CBrush blWhiteBrush( COLORREF(RGB(255,255,255)) );
	CBrush blBackBrush ( pomProperties->omBackColor );

//	if (!brush.CreateBrushIndirect(&blWhiteBrush))
//		return;
	CPen pen;
	CPen olSilverPen( PS_SOLID, 1, COLORREF(SILVER));
	CPen olGrayPen  ( PS_SOLID, 1, COLORREF(GRAY));
	CPen olBlackPen ( PS_SOLID, 1, COLORREF(BLACK));


	CBrush* pOldBrush;
	CPen* pOldPen;

	pOldBrush = popDC->SelectObject(&blBackBrush);
	pOldPen = popDC->SelectObject(&olSilverPen);

	CRect olRect = omPosition;
//	popView->DocToClient(olRect);
		popDC->Rectangle(olRect);
	popDC->SetTextColor(pomProperties->omFontColor);
	popDC->SetBkColor(pomProperties->omBackColor);

	popDC->SelectObject(&olBlackPen);
	popDC->SelectObject(&ogMSSansSerif_Regular_8/*ogMSSansSerif_Bold_8*/);
	CRect olCheckRect = olRect;
	//olCheckRect.OffsetRect(-3,3);
	olCheckRect.InflateRect(2,2);
	popDC->DrawText(pomProperties->omName, &olCheckRect, (DT_VCENTER|DT_CENTER|DT_SINGLELINE) );

	olCheckRect = olRect;
	CPoint olPtFrom, olPtTo;
	popView->DocToClient(olCheckRect);
	popDC->SelectObject(&olSilverPen);
	olPtFrom = CPoint(olCheckRect.left+1, olCheckRect.bottom-3);
	olPtTo   = CPoint(olCheckRect.right-3, olCheckRect.bottom-3);
	popView->ClientToDoc(olPtFrom); 
	popView->ClientToDoc(olPtTo);
	popDC->MoveTo(olPtFrom);
	popDC->LineTo(olPtTo);

	olPtTo   = CPoint(olCheckRect.right-3, olCheckRect.top+1);
	popView->ClientToDoc(olPtTo);
	popDC->LineTo(olPtTo);
	//----
	popDC->SelectObject(&olGrayPen);
	olPtFrom = CPoint(olCheckRect.left+1, olCheckRect.bottom-4);
	olPtTo   = CPoint(olCheckRect.left+1, olCheckRect.top+1);
	popView->ClientToDoc(olPtFrom); 
	popView->ClientToDoc(olPtTo);
	popDC->MoveTo(olPtFrom);
	popDC->LineTo(olPtTo);

	olPtTo   = CPoint(olCheckRect.right-1, olCheckRect.top+1);
	popView->ClientToDoc(olPtTo);
	popDC->LineTo(olPtTo);

	//----
	popDC->SelectObject(&olBlackPen);
	olPtFrom = CPoint(olCheckRect.left+2, olCheckRect.bottom-4);
	olPtTo   = CPoint(olCheckRect.left+2, olCheckRect.top+2);
	popView->ClientToDoc(olPtFrom); 
	popView->ClientToDoc(olPtTo);
	popDC->MoveTo(olPtFrom);
	popDC->LineTo(olPtTo);

	olPtTo   = CPoint(olCheckRect.right-2, olCheckRect.top+2);
	popView->ClientToDoc(olPtTo);
	popDC->LineTo(olPtTo);

	popDC->SelectObject(pOldBrush);
	popDC->SelectObject(pOldPen);
}

int UFormEditControl::GetHandleCount()
{
	ASSERT_VALID(this);
	return UFormControlObj::GetHandleCount();
}

// returns center of handle in logical coordinates
CPoint UFormEditControl::GetHandle(int nHandle)
{
	ASSERT_VALID(this);
	return UFormControlObj::GetHandle(nHandle);
}

HCURSOR UFormEditControl::GetHandleCursor(int nHandle)
{
	ASSERT_VALID(this);
	return UFormControlObj::GetHandleCursor(nHandle);
}

// point is in logical coordinates
void UFormEditControl::MoveHandleTo(int nHandle, CPoint point, FormDesignerView* pView)
{
	ASSERT_VALID(this);
	UFormControlObj::MoveHandleTo(nHandle, point, pView);
}

// rect must be in logical coordinates
BOOL UFormEditControl::Intersects(const CRect& rect)
{
	ASSERT_VALID(this);

	CRect rectT = rect;
	rectT.NormalizeRect();

	CRect fixed = omPosition;
	fixed.NormalizeRect();
	if ((rectT & fixed).IsRectEmpty())
		return FALSE;

	CRgn rgn;
		return TRUE;

	return rgn.RectInRegion(fixed);
}

UFormControlObj* UFormEditControl::Clone(FormDesignerDoc* popDoc, UFormControlObj* pObj)
{
	ASSERT_VALID(this);

	UFormEditControl* polClone = new UFormEditControl(omPosition);
	polClone->pomProperties = (UCtrlProperties*) new UEditCtrlProperties();
	*(polClone->pomProperties) = *(pObj->pomProperties);
	int ilC = popDoc->IncEditCounter();
	CString olC; olC.Format("%d", ilC);
	CString olName = polClone->pomProperties->omName;
	polClone->pomProperties->omName = olName + olC;
	popDoc->AddCtrl((UCtrlProperties*)polClone->pomProperties);
	ASSERT_VALID(polClone);

	if (popDoc != NULL)
		popDoc->Add(polClone);

	ASSERT_VALID(polClone);
	return polClone;
}

//XXX

////Control Edit

UFormStaticControl::UFormStaticControl()
{
}

UFormStaticControl::UFormStaticControl(const CRect& ropPosition)
	: UFormControlObj(ropPosition)
{
	ASSERT_VALID(this);

}

void UFormStaticControl::OnEditProperties(FormDesignerView* popView)
{
	/*PropertiesDlg *polDlg = */
		//new PropertiesDlg((CWnd*)popView);
	ShowPropertyDlg(pomProperties, (UFormControlObj *)this, popView);

}

void UFormStaticControl::Draw(CDC* popDC, FormDesignerView* popView)
{
	ASSERT_VALID(this);

//	CBrush blWhiteBrush( COLORREF(RGB(255,255,255)) );
	CBrush blWhiteBrush( pomProperties->omBackColor);
	
//	CBrush blSilverBrush( COLORREF(SILVER) );
	CBrush blSilverBrush( pomProperties->omBackColor );

//	if (!brush.CreateBrushIndirect(&blWhiteBrush))
//		return;
	CPen pen;
	CPen olSilverPen( PS_SOLID, 1, COLORREF(SILVER));
	CPen olGrayPen  ( PS_SOLID, pomProperties->imBorderWidth, pomProperties->omBorderColor);//COLORREF(GRAY));
	CPen olBlackPen ( PS_SOLID, 1, COLORREF(BLACK));


	CBrush* pOldBrush;
	CPen* pOldPen;

	pOldBrush = popDC->SelectObject(&blSilverBrush);
	pOldPen = popDC->SelectObject(&olGrayPen);

	CRect olRect = pomProperties->omRect;//omPosition;
	CRect olCheckRect = olRect;
	popDC->Rectangle(olRect);
	olCheckRect.NormalizeRect();
	popDC->SetTextColor(pomProperties->omFontColor);
	popDC->SetBkColor(pomProperties->omBackColor);

	CPoint olPtFrom, olPtTo;
	popDC->SelectObject(&olBlackPen);
	popDC->SelectObject(&ogMSSansSerif_Regular_8/*ogMSSansSerif_Bold_8*/);
	olCheckRect.OffsetRect(2,2);
	CString olOrie = pomProperties->omOrientation;
	olOrie.MakeUpper();
	if(olOrie == "LEFT")
		popDC->DrawText(pomProperties->omLabel, &olCheckRect, DT_LEFT  );
	if(olOrie == "CENTER")
		popDC->DrawText(pomProperties->omLabel, &olCheckRect, DT_CENTER  );
	if(olOrie == "RIGHT")
		popDC->DrawText(pomProperties->omLabel, &olCheckRect, DT_RIGHT  );

	popDC->SelectObject(pOldBrush);
	popDC->SelectObject(pOldPen);
}

int UFormStaticControl::GetHandleCount()
{
	ASSERT_VALID(this);
	return UFormControlObj::GetHandleCount();
}

// returns center of handle in logical coordinates
CPoint UFormStaticControl::GetHandle(int nHandle)
{
	ASSERT_VALID(this);
	return UFormControlObj::GetHandle(nHandle);
}

HCURSOR UFormStaticControl::GetHandleCursor(int nHandle)
{
	ASSERT_VALID(this);
	return UFormControlObj::GetHandleCursor(nHandle);
}

// point is in logical coordinates
void UFormStaticControl::MoveHandleTo(int nHandle, CPoint point, FormDesignerView* pView)
{
	ASSERT_VALID(this);
	UFormControlObj::MoveHandleTo(nHandle, point, pView);
}

// rect must be in logical coordinates
BOOL UFormStaticControl::Intersects(const CRect& rect)
{
	ASSERT_VALID(this);

	CRect rectT = rect;
	rectT.NormalizeRect();

	CRect fixed = omPosition;
	fixed.NormalizeRect();
	if ((rectT & fixed).IsRectEmpty())
		return FALSE;

	CRgn rgn;
		return TRUE;

	return rgn.RectInRegion(fixed);
}

UFormControlObj* UFormStaticControl::Clone(FormDesignerDoc* popDoc, UFormControlObj* pObj)
{
	ASSERT_VALID(this);

	UFormStaticControl* polClone = new UFormStaticControl(omPosition);
	polClone->pomProperties = (UCtrlProperties*) new UStaticCtrlProperties();
	*(polClone->pomProperties) = *(pObj->pomProperties);
	int ilC = popDoc->IncStaticCounter();
	CString olC; olC.Format("%d", ilC);
	CString olName = polClone->pomProperties->omName;
	polClone->pomProperties->omName = olName + olC;
	popDoc->AddCtrl((UCtrlProperties*)polClone->pomProperties);

	ASSERT_VALID(polClone);

	if (popDoc != NULL)
		popDoc->Add(polClone);

	ASSERT_VALID(polClone);
	return polClone;
}

//XXX
////Control Checkbox
UFormCheckboxControl::UFormCheckboxControl()
{
}

UFormCheckboxControl::UFormCheckboxControl(const CRect& ropPosition)
	: UFormControlObj(ropPosition)
{
	ASSERT_VALID(this);

}

void UFormCheckboxControl::OnEditProperties(FormDesignerView* popView)
{
	ShowPropertyDlg(pomProperties, (UFormControlObj *)this, popView);
}

void UFormCheckboxControl::Draw(CDC* popDC, FormDesignerView* popView)
{
	ASSERT_VALID(this);

	CBrush blSilverBrush( COLORREF(RGB(192,192,192)) );
	CBrush blWhiteBrush( COLORREF(RGB(255,255,255)) );
	CBrush blBackBrush ( pomProperties->omBackColor );

	CPen pen;
	CPen olSilverPen( PS_SOLID, 1, COLORREF(SILVER));
	CPen olGrayPen  ( PS_SOLID, 1, COLORREF(GRAY));
	CPen olBlackPen ( PS_SOLID, 1, COLORREF(BLACK));


	CBrush* pOldBrush;
	CPen* pOldPen;

	pOldBrush = popDC->SelectObject(&blBackBrush);
	pOldPen = popDC->SelectObject(&olSilverPen);
	popDC->SetTextColor(pomProperties->omFontColor);
	popDC->SetBkColor(pomProperties->omBackColor);

	CRect olRect = omPosition;
	popDC->Rectangle(olRect);


	popView->DocToClient(olRect);
	popDC->SelectObject(&olBlackPen); 
	popDC->SelectObject(&ogMSSansSerif_Regular_8);
	CRect olCheckRect = CRect(olRect.left+22, olRect.top, olRect.right, olRect.bottom);
	olCheckRect.NormalizeRect();
	olCheckRect.top = olRect.top + ( ( (int)((olRect.bottom - olRect.top)/2) ) - ( (int)((olCheckRect.bottom - olCheckRect.top)/2)  ));
	popView->ClientToDoc(olCheckRect);
	olCheckRect.InflateRect(2,2);
	popDC->DrawText(pomProperties->omLabel, &olCheckRect, (DT_VCENTER|DT_LEFT|DT_SINGLELINE) );


	popDC->SelectObject(&blSilverBrush);
	popDC->SelectObject(&olSilverPen);
	int ilHight=0,
		ilWidth=0;
		ilHight = olRect.bottom - olRect.top;
		ilWidth = olRect.left - olRect.right;

	olCheckRect;
	olCheckRect = CRect(olRect.left, olRect.top, olRect.left+15, olRect.top+15);
	olCheckRect.top = olRect.top + ( ( (int)((olRect.bottom - olRect.top)/2) ) - ( (int)((olCheckRect.bottom - olCheckRect.top)/2)  ));
	olCheckRect.bottom = olCheckRect.top + 15;
	popDC->SelectObject(&blWhiteBrush);
	popDC->SelectObject(&olSilverPen);
	popView->ClientToDoc(olCheckRect);
	popDC->Rectangle(olCheckRect);
	popView->DocToClient(olCheckRect);
	popDC->SelectObject(&olSilverPen);
	CPoint olPtFrom, olPtTo;
	olPtFrom = CPoint(olCheckRect.left+1, olCheckRect.bottom-3);
	olPtTo   = CPoint(olCheckRect.right-3, olCheckRect.bottom-3);
	popView->ClientToDoc(olPtFrom); 
	popView->ClientToDoc(olPtTo);
	popDC->MoveTo(olPtFrom);
	popDC->LineTo(olPtTo);
	//--
	olPtTo = CPoint(olCheckRect.right-3, olCheckRect.top+2);
	popView->ClientToDoc(olPtTo); 
	popDC->LineTo(olPtTo);
	//---
	popDC->SelectObject(&olGrayPen);
	olPtFrom = CPoint(olCheckRect.left+1, olCheckRect.bottom-3);
	olPtTo   = CPoint(olCheckRect.left+1, olCheckRect.top+1);
	popView->ClientToDoc(olPtFrom); 
	popView->ClientToDoc(olPtTo);
	popDC->MoveTo(olPtFrom);
	popDC->LineTo(olPtTo);
	//--
	olPtTo   = CPoint(olCheckRect.right-1, olCheckRect.top+1);
	popView->ClientToDoc(olPtTo);
	popDC->LineTo(olPtTo);
	//---
	popDC->SelectObject(&olBlackPen);
	olPtFrom = CPoint(olCheckRect.left+2, olCheckRect.bottom-4);
	olPtTo   = CPoint(olCheckRect.left+2, olCheckRect.top+2);
	popView->ClientToDoc(olPtFrom); 
	popView->ClientToDoc(olPtTo);
	popDC->MoveTo(olPtFrom);
	popDC->LineTo(olPtTo);
	//--
	olPtTo   = CPoint(olCheckRect.right-3, olCheckRect.top+2);
	popView->ClientToDoc(olPtTo);
	popDC->LineTo(olPtTo);

	popDC->SelectObject(pOldBrush);
	popDC->SelectObject(pOldPen);

}

int UFormCheckboxControl::GetHandleCount()
{
	ASSERT_VALID(this);
	return UFormControlObj::GetHandleCount();
}

// returns center of handle in logical coordinates
CPoint UFormCheckboxControl::GetHandle(int nHandle)
{
	ASSERT_VALID(this);
	return UFormControlObj::GetHandle(nHandle);
}

HCURSOR UFormCheckboxControl::GetHandleCursor(int nHandle)
{
	ASSERT_VALID(this);
	return UFormControlObj::GetHandleCursor(nHandle);
}

// point is in logical coordinates
void UFormCheckboxControl::MoveHandleTo(int nHandle, CPoint point, FormDesignerView* pView)
{
	ASSERT_VALID(this);
	UFormControlObj::MoveHandleTo(nHandle, point, pView);
}

// rect must be in logical coordinates
BOOL UFormCheckboxControl::Intersects(const CRect& rect)
{
	ASSERT_VALID(this);

	CRect rectT = rect;
	rectT.NormalizeRect();

	CRect fixed = omPosition;
	fixed.NormalizeRect();
	if ((rectT & fixed).IsRectEmpty())
		return FALSE;

	CRgn rgn;
		return TRUE;

	return rgn.RectInRegion(fixed);
}

UFormControlObj* UFormCheckboxControl::Clone(FormDesignerDoc* popDoc, UFormControlObj* pObj)
{
	ASSERT_VALID(this);

	UFormCheckboxControl* polClone = new UFormCheckboxControl(omPosition);
	polClone->pomProperties = (UCtrlProperties*) new UCheckboxCtrlProperties();
	*(polClone->pomProperties) = *(pObj->pomProperties);

	int ilC = popDoc->IncCheckboxCounter();
	CString olC; olC.Format("%d", ilC);
	CString olName = polClone->pomProperties->omName;
	polClone->pomProperties->omName = olName + olC;
	popDoc->AddCtrl((UCtrlProperties*)polClone->pomProperties);
	ASSERT_VALID(polClone);

	if (popDoc != NULL)
		popDoc->Add(polClone);

	ASSERT_VALID(polClone);
	return polClone;
}
//Checkbox Control
//XXX
UFormComboboxControl::UFormComboboxControl()
{
}
UFormComboboxControl::UFormComboboxControl(const CRect& ropPosition)
	: UFormControlObj(ropPosition)
{
}
void UFormComboboxControl::OnEditProperties(FormDesignerView* popView)
{
	ShowPropertyDlg(pomProperties, (UFormControlObj *)this, popView);
}
void UFormComboboxControl::Draw(CDC* popDC, FormDesignerView* popView)
{
	ASSERT_VALID(this);

	CBrush blWhiteBrush( COLORREF(RGB(255,255,255)) );
	CBrush blSilverBrush(COLORREF(RGB(192,192,192)));
	CPen pen;
	CPen olWhitePen ( PS_SOLID, 1, COLORREF(RGB(255,255,255)));
	CPen olSilverPen( PS_SOLID, 1, COLORREF(SILVER));
	CPen olGrayPen  ( PS_SOLID, 1, COLORREF(GRAY));
	CPen olBlackPen ( PS_SOLID, 1, COLORREF(BLACK));

	CBrush blBackBrush ( pomProperties->omBackColor );

	CBrush* pOldBrush;
	CPen* pOldPen;

	pOldBrush = popDC->SelectObject(&blBackBrush);
	pOldPen = popDC->SelectObject(&olSilverPen);
	CRect olRect = omPosition;



//	popDC->SelectObject(&blWhiteBrush);
//	popDC->SelectObject(&olSilverPen);

	popView->DocToClient(olRect);
	if(olRect.bottom - olRect.top != 25)
	{
		olRect.bottom = olRect.top + 25;
	}
	if(olRect.right - olRect.left < 25)
	{
		olRect.right = olRect.left + 25;
	}
	popView->ClientToDoc(olRect); 
		popDC->Rectangle(olRect);

	popDC->SetTextColor(pomProperties->omFontColor);
	popDC->SetBkColor(pomProperties->omBackColor);
	popDC->SelectObject(&olBlackPen);
	popDC->SelectObject(&ogMSSansSerif_Regular_8/*ogMSSansSerif_Bold_8*/);
	CRect olCheckRect = olRect;
	olCheckRect.left += 7;
	olCheckRect.InflateRect(2,2);
	popDC->DrawText(pomProperties->omName, &olCheckRect, (DT_VCENTER|DT_LEFT|DT_SINGLELINE) );


	olCheckRect = olRect;
	CPoint olPtFrom, olPtTo;
	popView->DocToClient(olCheckRect);
	popDC->SelectObject(&olSilverPen);
	olPtFrom = CPoint(olCheckRect.left+1, olCheckRect.bottom-3);
	olPtTo   = CPoint(olCheckRect.right-3, olCheckRect.bottom-3);
	popView->ClientToDoc(olPtFrom); 
	popView->ClientToDoc(olPtTo);
	popDC->MoveTo(olPtFrom);
	popDC->LineTo(olPtTo);

	olPtTo   = CPoint(olCheckRect.right-3, olCheckRect.top+1);
	popView->ClientToDoc(olPtTo);
	popDC->LineTo(olPtTo);
	//----
	popDC->SelectObject(&olGrayPen);
	olPtFrom = CPoint(olCheckRect.left+1, olCheckRect.bottom-4);
	olPtTo   = CPoint(olCheckRect.left+1, olCheckRect.top+1);
	popView->ClientToDoc(olPtFrom); 
	popView->ClientToDoc(olPtTo);
	popDC->MoveTo(olPtFrom);
	popDC->LineTo(olPtTo);

	olPtTo   = CPoint(olCheckRect.right-1, olCheckRect.top+1);
	popView->ClientToDoc(olPtTo);
	popDC->LineTo(olPtTo);

	//----
	popDC->SelectObject(&olBlackPen);
	olPtFrom = CPoint(olCheckRect.left+2, olCheckRect.bottom-4);
	olPtTo   = CPoint(olCheckRect.left+2, olCheckRect.top+2);
	popView->ClientToDoc(olPtFrom); 
	popView->ClientToDoc(olPtTo);
	popDC->MoveTo(olPtFrom);
	popDC->LineTo(olPtTo);

	olPtTo   = CPoint(olCheckRect.right-4, olCheckRect.top+2);
	popView->ClientToDoc(olPtTo);
	popDC->LineTo(olPtTo);

	olPtTo   = CPoint(olCheckRect.right-4, olCheckRect.bottom-3);
	popView->ClientToDoc(olPtTo);
	popDC->LineTo(olPtTo);

	olPtTo   = CPoint(olCheckRect.right-19, olCheckRect.bottom-3);
	popView->ClientToDoc(olPtTo);
	popDC->LineTo(olPtTo);
	//----
	popDC->SelectObject(&blSilverBrush);
	popDC->SelectObject(&olGrayPen);
	CRect olFillRect = CRect(olCheckRect.right-19, olCheckRect.top+3, olCheckRect.right-4, olCheckRect.bottom-4);
	popView->ClientToDoc(olFillRect); 
	popDC->Rectangle(olFillRect);
	//----
	popDC->SelectObject(&olGrayPen);
	olPtFrom = CPoint(olCheckRect.right-18, olCheckRect.bottom-5);
	olPtTo   = CPoint(olCheckRect.right-5, olCheckRect.bottom-5);
	popView->ClientToDoc(olPtFrom); 
	popView->ClientToDoc(olPtTo);
	popDC->MoveTo(olPtFrom);
	popDC->LineTo(olPtTo);
	olPtTo   = CPoint(olCheckRect.right-5, olCheckRect.top+4);
	popView->ClientToDoc(olPtTo);
	popDC->LineTo(olPtTo);
	//----
	popDC->SelectObject(&olWhitePen);
	olPtFrom = CPoint(olCheckRect.right-18, olCheckRect.bottom-5);
	olPtTo   = CPoint(olCheckRect.right-18, olCheckRect.top+4);
	popView->ClientToDoc(olPtFrom); 
	popView->ClientToDoc(olPtTo);
	popDC->MoveTo(olPtFrom);
	popDC->LineTo(olPtTo);
	olPtTo   = CPoint(olCheckRect.right-5, olCheckRect.top+4);
	popView->ClientToDoc(olPtTo);
	popDC->LineTo(olPtTo);
	//----
	popDC->SelectObject(&olBlackPen);
	olPtFrom = CPoint(olCheckRect.right-14, olCheckRect.top+9);
	olPtTo   = CPoint(olCheckRect.right-8 , olCheckRect.top+9);
	popView->ClientToDoc(olPtFrom); 
	popView->ClientToDoc(olPtTo);
	popDC->MoveTo(olPtFrom);
	popDC->LineTo(olPtTo);
	//----
	olPtFrom = CPoint(olCheckRect.right-13, olCheckRect.top+10);
	olPtTo   = CPoint(olCheckRect.right-9 , olCheckRect.top+10);
	popView->ClientToDoc(olPtFrom); 
	popView->ClientToDoc(olPtTo);
	popDC->MoveTo(olPtFrom);
	popDC->LineTo(olPtTo);
	//----
	olPtFrom = CPoint(olCheckRect.right-12, olCheckRect.top+11);
	olPtTo   = CPoint(olCheckRect.right-10, olCheckRect.top+11);
	popView->ClientToDoc(olPtFrom); 
	popView->ClientToDoc(olPtTo);
	popDC->MoveTo(olPtFrom);
	popDC->LineTo(olPtTo);
	//----
	olPtFrom = CPoint(olCheckRect.right-11, olCheckRect.top+10);
	olPtTo   = CPoint(olCheckRect.right-11, olCheckRect.top+12);
	popView->ClientToDoc(olPtFrom); 
	popView->ClientToDoc(olPtTo);
	popDC->MoveTo(olPtFrom);
	popDC->LineTo(olPtTo);

	popDC->SelectObject(pOldBrush);
	popDC->SelectObject(pOldPen);
}

int UFormComboboxControl::GetHandleCount()
{
	ASSERT_VALID(this);
	return UFormControlObj::GetHandleCount();
}
BOOL UFormComboboxControl::Intersects(const CRect& ropRect)
{
	ASSERT_VALID(this);

	CRect rectT = ropRect;
	rectT.NormalizeRect();

	CRect fixed = omPosition;
	fixed.NormalizeRect();
	if ((rectT & fixed).IsRectEmpty())
		return FALSE;

	CRgn rgn;
		return TRUE;

	return rgn.RectInRegion(fixed);
}
// end UFormControlObj
//Buttom Control
UFormButtonControl::UFormButtonControl()
{
}
UFormButtonControl::UFormButtonControl(const CRect& ropPosition)
	: UFormControlObj(ropPosition)
{
}
void UFormButtonControl::OnEditProperties(FormDesignerView* popView)
{
	ShowPropertyDlg(pomProperties, (UFormControlObj *)this, popView);

}
void UFormButtonControl::Draw(CDC* popDC, FormDesignerView* popView)
{
	ASSERT_VALID(this);

	CBrush blBackBrush( pomProperties->omBackColor );
	CBrush blWhiteBrush( COLORREF(RGB(255,255,255)) );
	CBrush blSilverBrush(COLORREF(RGB(192,192,192)));
	CPen pen;
	CPen olWhitePen ( PS_SOLID, 1, COLORREF(RGB(255,255,255)));
	CPen olSilverPen( PS_SOLID, 1, COLORREF(SILVER));
	CPen olGrayPen  ( PS_SOLID, 1, COLORREF(GRAY));
	CPen olBlackPen ( PS_SOLID, 1, COLORREF(BLACK));


	CBrush* pOldBrush;
	CPen* pOldPen;

	pOldBrush = popDC->SelectObject(&blBackBrush);
	pOldPen = popDC->SelectObject(&olSilverPen);

	CRect olRect = pomProperties->omRect;//omPosition;
	popDC->Rectangle(olRect);

	popDC->SetTextColor(pomProperties->omFontColor);
	popDC->SetBkColor(pomProperties->omBackColor);

	popDC->SelectObject(&olBlackPen);
	popDC->SelectObject(&ogMSSansSerif_Regular_8/*ogMSSansSerif_Bold_8*/);
	CRect olCheckRect = olRect;
	//olCheckRect.OffsetRect(-3,3);
	olCheckRect.InflateRect(2,2);
	popDC->DrawText(pomProperties->omLabel, &olCheckRect, (DT_VCENTER|DT_CENTER|DT_SINGLELINE) );

	CPoint olPtFrom, olPtTo;
	popView->DocToClient(olCheckRect);
	popDC->SelectObject(&olWhitePen);
	olPtFrom = CPoint(olCheckRect.left+1, olCheckRect.bottom-1);
	olPtTo   = CPoint(olCheckRect.left+1, olCheckRect.top +1);
	popView->ClientToDoc(olPtFrom); 
	popView->ClientToDoc(olPtTo);
	popDC->MoveTo(olPtFrom);
	popDC->LineTo(olPtTo);

	olPtTo   = CPoint(olCheckRect.right-1, olCheckRect.top +1);
	popView->ClientToDoc(olPtTo);
	popDC->LineTo(olPtTo);
	//----
	popDC->SelectObject(&olBlackPen);
	olPtFrom = CPoint(olCheckRect.left+1, olCheckRect.bottom-1);
	olPtTo   = CPoint(olCheckRect.right-1, olCheckRect.bottom-1);
	popView->ClientToDoc(olPtFrom); 
	popView->ClientToDoc(olPtTo);
	popDC->MoveTo(olPtFrom);
	popDC->LineTo(olPtTo);

	olPtTo   = CPoint(olCheckRect.right-1, olCheckRect.top);
	popView->ClientToDoc(olPtTo);
	popDC->LineTo(olPtTo);
	//----
	popDC->SelectObject(&olGrayPen);
	olPtFrom = CPoint(olCheckRect.left+2, olCheckRect.bottom-2);
	olPtTo   = CPoint(olCheckRect.right-2, olCheckRect.bottom-2);
	popView->ClientToDoc(olPtFrom); 
	popView->ClientToDoc(olPtTo);
	popDC->MoveTo(olPtFrom);
	popDC->LineTo(olPtTo);

	olPtTo   = CPoint(olCheckRect.right-2, olCheckRect.top +1);
	popView->ClientToDoc(olPtTo);
	popDC->LineTo(olPtTo);

	popDC->SelectObject(pOldBrush);
	popDC->SelectObject(pOldPen);
}

int UFormButtonControl::GetHandleCount()
{
	ASSERT_VALID(this);
	return UFormControlObj::GetHandleCount();
}
BOOL UFormButtonControl::Intersects(const CRect& ropRect)
{
	ASSERT_VALID(this);

	CRect rectT = ropRect;
	rectT.NormalizeRect();

	CRect fixed = omPosition;
	fixed.NormalizeRect();
	if ((rectT & fixed).IsRectEmpty())
		return FALSE;

	CRgn rgn;
		return TRUE;

	return rgn.RectInRegion(fixed);
}
UFormControlObj* UFormButtonControl::Clone(FormDesignerDoc* popDoc, UFormControlObj* pObj)
{
	ASSERT_VALID(this);

	UFormButtonControl* polClone = new UFormButtonControl(omPosition);
	polClone->pomProperties = (UCtrlProperties*) new UButtonCtrlProperties();
	*(polClone->pomProperties) = *(pObj->pomProperties);
	int ilC = popDoc->IncButtonCounter();
	CString olC; olC.Format("%d", ilC);
	CString olName = polClone->pomProperties->omName;
	polClone->pomProperties->omName = olName + olC;
	popDoc->AddCtrl((UCtrlProperties*)polClone->pomProperties);
	ASSERT_VALID(polClone);

	if (popDoc != NULL)
		popDoc->Add(polClone);

	ASSERT_VALID(polClone);
	return polClone;
}

//Group Control
UFormGroupControl::UFormGroupControl()
{
}
UFormGroupControl::UFormGroupControl(const CRect& ropPosition)
	: UFormControlObj(ropPosition)
{
}
void UFormGroupControl::OnEditProperties(FormDesignerView* popView)
{
	ShowPropertyDlg(pomProperties, (UFormControlObj *)this, popView);

}
void UFormGroupControl::Draw(CDC* popDC, FormDesignerView* popView)
{
	ASSERT_VALID(this);

	CBrush blBackBrush( pomProperties->omBackColor );
	CBrush blWhiteBrush( COLORREF(RGB(255,255,255)) );
	CBrush blSilverBrush(COLORREF(RGB(192,192,192)));
	CPen pen;
	CPen olWhitePen ( PS_SOLID, 1, COLORREF(RGB(255,255,255)));
	CPen olSilverPen( PS_SOLID, 1, COLORREF(SILVER));
	CPen olGrayPen  ( PS_SOLID, 1, COLORREF(GRAY));
	CPen olBlackPen ( PS_SOLID, 1, COLORREF(BLACK));


	CBrush* pOldBrush;
	CPen* pOldPen;

	pOldBrush = popDC->SelectObject(&blBackBrush);
	pOldPen = popDC->SelectObject(&olSilverPen);

	CRect olRect = pomProperties->omRect;//omPosition;
	CRect olTextRect = olRect;
	int ilTextOffest = 23;
	olTextRect.left = olTextRect.left + ilTextOffest;
	olTextRect.bottom = olTextRect.top + ilTextOffest;
	int ilTextHeight = olTextRect.bottom - olTextRect.top;
	int ilTextWidth = 0;
	popDC->Rectangle(olRect);

	popDC->SetTextColor(pomProperties->omFontColor);
	popDC->SetBkColor(pomProperties->omBackColor);

	popDC->SelectObject(&olBlackPen);
	popDC->SelectObject(&ogMSSansSerif_Regular_8/*ogMSSansSerif_Bold_8*/);
	CSize olTextSize;
	olTextSize = popDC->GetTextExtent(pomProperties->omLabel);
	if(ilTextHeight < olTextSize.cy)
	{
		ilTextHeight = olTextSize.cy;
		olTextRect.bottom = olTextRect.top + ilTextHeight + 2;
	}
	if(ilTextWidth < olTextSize.cx)
	{
		ilTextWidth = olTextSize.cx;
		olTextRect.right = olTextRect.left + ilTextWidth + 2;
	}


	CRect olCheckRect = olRect;
	olTextRect.InflateRect(2,2);
	popDC->DrawText(pomProperties->omLabel, &olTextRect, (DT_LEFT|DT_SINGLELINE) );

	CPoint olPtFrom, olPtTo;
	olCheckRect.InflateRect(2,2);
	CRect olTmpRect2 = olCheckRect;
	popView->DocToClient(olCheckRect);
	popDC->SelectObject(&olGrayPen);
	olPtFrom = CPoint(olCheckRect.left+18, olCheckRect.top + (int)(ilTextHeight/4));
	olPtTo   = CPoint(olCheckRect.left+1, olCheckRect.top + (int)(ilTextHeight/4));
	popView->ClientToDoc(olPtFrom); 
	popView->ClientToDoc(olPtTo);
	popDC->MoveTo(olPtFrom);
	popDC->LineTo(olPtTo);

	olPtTo   = CPoint(olCheckRect.left+1, olCheckRect.bottom - 1);
	popView->ClientToDoc(olPtTo);
	popDC->LineTo(olPtTo);
	//----
	olPtTo   = CPoint(olCheckRect.right-2, olCheckRect.bottom - 1);
	popView->ClientToDoc(olPtTo);
	popDC->LineTo(olPtTo);
	//----
	olPtTo   = CPoint(olCheckRect.right-2, olCheckRect.top + (int)(ilTextHeight/4));
	popView->ClientToDoc(olPtTo);
	popDC->LineTo(olPtTo);
	//----
	olPtTo   = CPoint(olCheckRect.left + ilTextOffest + ilTextWidth + 1, olCheckRect.top + (int)(ilTextHeight/4));
	popView->ClientToDoc(olPtTo);
	popDC->LineTo(olPtTo);

//----- White pen
	popDC->SelectObject(&olWhitePen);
	olPtFrom = CPoint(olCheckRect.left+18, olCheckRect.top + (int)(ilTextHeight/4) + 1);
	olPtTo   = CPoint(olCheckRect.left+2, olCheckRect.top + (int)(ilTextHeight/4) + 1);
	popView->ClientToDoc(olPtFrom); 
	popView->ClientToDoc(olPtTo);
	popDC->MoveTo(olPtFrom);
	popDC->LineTo(olPtTo);

	olPtTo   = CPoint(olCheckRect.left+2, olCheckRect.bottom - 2);
	popView->ClientToDoc(olPtTo);
	popDC->LineTo(olPtTo);

	//----
	olPtFrom = CPoint(olCheckRect.left, olCheckRect.bottom);
	olPtTo   = CPoint(olCheckRect.right-1, olCheckRect.bottom);
	popView->ClientToDoc(olPtFrom);
	popView->ClientToDoc(olPtTo);
	popDC->MoveTo(olPtFrom);
	popDC->LineTo(olPtTo);
	//----
	olPtTo   = CPoint(olCheckRect.right-1, olCheckRect.top + (int)(ilTextHeight/4));
	popView->ClientToDoc(olPtTo);
	popDC->LineTo(olPtTo);
	//----
	//----
	olPtFrom = CPoint(olCheckRect.right-3, olCheckRect.top + (int)(ilTextHeight/4) + 1);
	olPtTo   = CPoint(olCheckRect.left + ilTextOffest + ilTextWidth + 1, olCheckRect.top + (int)(ilTextHeight/4) + 1);
	popView->ClientToDoc(olPtFrom);
	popView->ClientToDoc(olPtTo);
	popDC->MoveTo(olPtFrom);
	popDC->LineTo(olPtTo);

	popDC->SelectObject(pOldBrush);
	popDC->SelectObject(pOldPen);
}

int UFormGroupControl::GetHandleCount()
{
	ASSERT_VALID(this);
	return UFormControlObj::GetHandleCount();
}
BOOL UFormGroupControl::Intersects(const CRect& ropRect)
{
	ASSERT_VALID(this);

	CRect rectT = ropRect;
	rectT.NormalizeRect();

	CRect fixed = omPosition;
	fixed.NormalizeRect();
	if ((rectT & fixed).IsRectEmpty())
		return FALSE;

	CRgn rgn;
		return TRUE;

	return rgn.RectInRegion(fixed);
}
UFormControlObj* UFormGroupControl::Clone(FormDesignerDoc* popDoc, UFormControlObj* pObj)
{
	ASSERT_VALID(this);

	UFormGroupControl* polClone = new UFormGroupControl(omPosition);
	polClone->pomProperties = (UCtrlProperties*) new UButtonCtrlProperties();
	*(polClone->pomProperties) = *(pObj->pomProperties);
	int ilC = popDoc->IncButtonCounter();
	CString olC; olC.Format("%d", ilC);
	CString olName = polClone->pomProperties->omName;
	polClone->pomProperties->omName = olName + olC;
	popDoc->AddCtrl((UCtrlProperties*)polClone->pomProperties);
	ASSERT_VALID(polClone);

	if (popDoc != NULL)
		popDoc->Add(polClone);

	ASSERT_VALID(polClone);
	return polClone;
}

//MWO

UFormControlObj* UFormComboboxControl::Clone(FormDesignerDoc* popDoc, UFormControlObj* pObj)
{
	ASSERT_VALID(this);

	UFormComboboxControl* polClone = new UFormComboboxControl(omPosition);
	polClone->pomProperties = (UCtrlProperties*) new UComboboxCtrlProperties();
	*(polClone->pomProperties) = *(pObj->pomProperties);
	int ilC = popDoc->IncComboboxCounter();
	CString olC; olC.Format("%d", ilC);
	CString olName = polClone->pomProperties->omName;
	polClone->pomProperties->omName = olName + olC;
	popDoc->AddCtrl((UCtrlProperties*)polClone->pomProperties);
	ASSERT_VALID(polClone);

	if (popDoc != NULL)
		popDoc->Add(polClone);

	ASSERT_VALID(polClone);
	return polClone;
}





////Control Rectangle
UFormRectControl::UFormRectControl()
{
}

UFormRectControl::UFormRectControl(const CRect& ropPosition)
	: UFormControlObj(ropPosition)
{
	ASSERT_VALID(this);

	omShape = rectangle;
	m_roundness.x = 16;
	m_roundness.y = 16;
}

void UFormRectControl::OnEditProperties(FormDesignerView* popView)
{
	ShowPropertyDlg(pomProperties, (UFormControlObj *)this, popView);
}

void UFormRectControl::Draw(CDC* popDC, FormDesignerView* popView)
{
	ASSERT_VALID(this);

	CBrush brush(pomProperties->omBackColor);
//	if (!brush.CreateBrushIndirect(&omLogBrush))
//		return;
	CPen pen(PS_SOLID, pomProperties->imBorderWidth, pomProperties->omBorderColor);
//	if (!pen.CreatePenIndirect(&omLogPen))
//		return;

	CBrush* pOldBrush;
	CPen* pOldPen;

	if (omBrush)
		pOldBrush = popDC->SelectObject(&brush);
	else
		pOldBrush = (CBrush*)popDC->SelectStockObject(NULL_BRUSH);

	if (omPen)
		pOldPen = popDC->SelectObject(&pen);
	else
		pOldPen = (CPen*)popDC->SelectStockObject(NULL_PEN);

	CRect olRect = pomProperties->omRect;//omPosition;
	switch (omShape)
	{
	case rectangle:
		popDC->Rectangle(olRect);
		break;
	case line:
		if (olRect.top > olRect.bottom)
		{
			olRect.top -= omLogPen.lopnWidth.y / 2;
			olRect.bottom += (omLogPen.lopnWidth.y + 1) / 2;
		}
		else
		{
			olRect.top += (omLogPen.lopnWidth.y + 1) / 2;
			olRect.bottom -= omLogPen.lopnWidth.y / 2;
		}

		if (olRect.left > olRect.right)
		{
			olRect.left -= omLogPen.lopnWidth.x / 2;
			olRect.right += (omLogPen.lopnWidth.x + 1) / 2;
		}
		else
		{
			olRect.left += (omLogPen.lopnWidth.x + 1) / 2;
			olRect.right -= omLogPen.lopnWidth.x / 2;
		}

		popDC->MoveTo(olRect.TopLeft());
		popDC->LineTo(olRect.BottomRight());
		break;
	}

	popDC->SelectObject(pOldBrush);
	popDC->SelectObject(pOldPen);
}


int UFormRectControl::GetHandleCount()
{
	ASSERT_VALID(this);

	return omShape == line ? 2 :
		UFormControlObj::GetHandleCount();
}

// returns center of handle in logical coordinates
CPoint UFormRectControl::GetHandle(int nHandle)
{
	ASSERT_VALID(this);

	if (omShape == line && nHandle == 2)
		nHandle = 5;

	return UFormControlObj::GetHandle(nHandle);
}

HCURSOR UFormRectControl::GetHandleCursor(int nHandle)
{
	ASSERT_VALID(this);

	if (omShape == line && nHandle == 2)
		nHandle = 5;
	return UFormControlObj::GetHandleCursor(nHandle);
}

// point is in logical coordinates
void UFormRectControl::MoveHandleTo(int nHandle, CPoint point, FormDesignerView* pView)
{
	ASSERT_VALID(this);

	if (omShape == line && nHandle == 2)
		nHandle = 5;

	UFormControlObj::MoveHandleTo(nHandle, point, pView);
}

// rect must be in logical coordinates
BOOL UFormRectControl::Intersects(const CRect& rect)
{
	ASSERT_VALID(this);

	CRect rectT = rect;
	rectT.NormalizeRect();

	CRect fixed = omPosition;
	fixed.NormalizeRect();
	if ((rectT & fixed).IsRectEmpty())
		return FALSE;

	CRgn rgn;
	switch (omShape)
	{
	case rectangle:
		return TRUE;

	case line:
		{
			int x = (omLogPen.lopnWidth.x + 5) / 2;
			int y = (omLogPen.lopnWidth.y + 5) / 2;
			POINT points[4];
			points[0].x = fixed.left;
			points[0].y = fixed.top;
			points[1].x = fixed.left;
			points[1].y = fixed.top;
			points[2].x = fixed.right;
			points[2].y = fixed.bottom;
			points[3].x = fixed.right;
			points[3].y = fixed.bottom;

			if (fixed.left < fixed.right)
			{
				points[0].x -= x;
				points[1].x += x;
				points[2].x += x;
				points[3].x -= x;
			}
			else
			{
				points[0].x += x;
				points[1].x -= x;
				points[2].x -= x;
				points[3].x += x;
			}

			if (fixed.top < fixed.bottom)
			{
				points[0].y -= y;
				points[1].y += y;
				points[2].y += y;
				points[3].y -= y;
			}
			else
			{
				points[0].y += y;
				points[1].y -= y;
				points[2].y -= y;
				points[3].y += y;
			}
			rgn.CreatePolygonRgn(points, 4, ALTERNATE);
		}
		break;
	}
	return rgn.RectInRegion(fixed);
}

UFormControlObj* UFormRectControl::Clone(FormDesignerDoc* popDoc, UFormControlObj* pObj)
{
	ASSERT_VALID(this);

	UFormRectControl* polClone = new UFormRectControl(omPosition);
	if(omShape == line)
		polClone->pomProperties = (UCtrlProperties*) new ULineCtrlProperties();
	else
		polClone->pomProperties = (UCtrlProperties*) new URectCtrlProperties();

	*(polClone->pomProperties) = *(pObj->pomProperties);
	int ilC = popDoc->IncRectCounter();
	CString olC; olC.Format("%d", ilC);
	CString olName = polClone->pomProperties->omName;
	polClone->pomProperties->omName = olName + olC;
	popDoc->AddCtrl((UCtrlProperties*)polClone->pomProperties);

	polClone->omPen = omPen;
	polClone->omLogPen = omLogPen;
	polClone->omBrush = omBrush;
	polClone->omLogBrush = omLogBrush;
	polClone->omShape = omShape;
	ASSERT_VALID(polClone);

	if (popDoc != NULL)
		popDoc->Add(polClone);

	ASSERT_VALID(polClone);
	return polClone;
}


//XXX Grid control

UFormGridControl::UFormGridControl()
{
}
UFormGridControl::UFormGridControl(const CRect& ropPosition)
	: UFormControlObj(ropPosition)
{
}
void UFormGridControl::OnEditProperties(FormDesignerView* popView)
{
	ShowPropertyDlg(pomProperties, (UFormControlObj *)this, popView);
}
void UFormGridControl::Draw(CDC* popDC, FormDesignerView* popView)
{
	ASSERT_VALID(this);

	CBrush blWhiteBrush( COLORREF(RGB(255,255,255)) );
	CBrush blSilverBrush(COLORREF(RGB(192,192,192)));
	CBrush blGrayBrush(COLORREF(RGB(128,128,128)));
	CPen pen;
	CPen olWhitePen ( PS_SOLID, 1, COLORREF(RGB(255,255,255)));
	CPen olSilverPen( PS_SOLID, 1, COLORREF(SILVER));
	CPen olGrayPen  ( PS_SOLID, 1, COLORREF(GRAY));
	CPen olBlackPen ( PS_SOLID, 1, COLORREF(BLACK));

	CBrush blBackBrush ( pomProperties->omBackColor );

	CBrush* pOldBrush;
	CPen* pOldPen;

	pOldBrush = popDC->SelectObject(&blBackBrush);
	pOldPen = popDC->SelectObject(&olBlackPen);
	CRect olRect = omPosition;

	popView->DocToClient(olRect);
	popView->ClientToDoc(olRect); 
		popDC->Rectangle(olRect);

//	popView->ClientToDoc(olPtFrom); 
//	popView->ClientToDoc(olPtTo);


	popDC->SetTextColor(pomProperties->omFontColor);
	popDC->SetBkColor(pomProperties->omBackColor);
	popDC->SelectObject(&olBlackPen);
	popDC->SelectObject(&ogMSSansSerif_Regular_8/*ogMSSansSerif_Bold_8*/);
	CRect olCheckRect = olRect;
	int ilColWidth = 60;
	int ilHeaderCount = (int)((olCheckRect.right - olCheckRect.left)/ilColWidth);
	int ilTopOffset = 20;
	if(ilTopOffset > (olCheckRect.bottom - olCheckRect.top))
		ilTopOffset = (olCheckRect.bottom - olCheckRect.top);
	CRect olTmpRect = CRect(olCheckRect.left, olCheckRect.top, olCheckRect.right, olCheckRect.top+ilTopOffset); 
//	olTmpRect.InflateRect(-1, -1);
	popDC->SelectObject(&blSilverBrush);
//	popDC->SelectObject(&olGrayPen);
	popDC->Rectangle(olTmpRect);
	int ilLeftOffset = ilColWidth;
	if(ilLeftOffset > (olCheckRect.right - olCheckRect.left))
		ilLeftOffset = (olCheckRect.right - olCheckRect.left);
	olTmpRect = CRect(olCheckRect.left, olCheckRect.top, olCheckRect.left + ilLeftOffset+1, olCheckRect.bottom);
	popDC->Rectangle(olTmpRect);

	popDC->SelectObject(&olWhitePen);
	popDC->MoveTo(olCheckRect.left+1, olCheckRect.top+1);
	popDC->LineTo(olCheckRect.left+1, olCheckRect.bottom-1);
	popDC->MoveTo(olCheckRect.left+1, olCheckRect.top+1);
	popDC->LineTo(olCheckRect.right-1, olCheckRect.top+1);

	int ilLX = ilColWidth;
	for(int i = 0; i < ilHeaderCount; i++)
	{
		if(i == 0)
		{
			popDC->SelectObject(&olBlackPen);
			popDC->MoveTo(olCheckRect.left+(ilLX), olCheckRect.top+1);
			popDC->LineTo(olCheckRect.left+(ilLX), olCheckRect.bottom-1);
		}
		else
		{
			popDC->SelectObject(&olSilverPen);
			popDC->MoveTo(olCheckRect.left+(ilLX), olCheckRect.top+1);
			popDC->LineTo(olCheckRect.left+(ilLX), olCheckRect.bottom-1);
		}

		popDC->SelectObject(&olBlackPen);
		popDC->MoveTo(olCheckRect.left+(ilLX), olCheckRect.top+1);
		popDC->LineTo(olCheckRect.left+(ilLX), olCheckRect.top+ilTopOffset);

		popDC->SelectObject(&olWhitePen);
		popDC->MoveTo(olCheckRect.left+(ilLX+1), olCheckRect.top+1);
		popDC->LineTo(olCheckRect.left+(ilLX+1), olCheckRect.top+ilTopOffset);
		ilLX+=ilColWidth;
	}

	int ilRowHeight = 20;
	int ilLY = ilRowHeight;
	int ilLineCount = (int)((olCheckRect.bottom - olCheckRect.top)/ilRowHeight);
	for(i = 0; i < ilLineCount; i++)
	{
		if( i == 0)
		{
			popDC->SelectObject(&olBlackPen);
			popDC->MoveTo(olCheckRect.left+1, olCheckRect.top+/*ilTopOffset*/+ilLY-1);
			popDC->LineTo(olCheckRect.right-1, olCheckRect.top+ilLY-1);
		}
		else
		{
			popDC->SelectObject(&olSilverPen);
			popDC->MoveTo(olCheckRect.left+1, olCheckRect.top+/*ilTopOffset*/+ilLY-1);
			popDC->LineTo(olCheckRect.right-1, olCheckRect.top+ilLY-1);
		}
		popDC->SelectObject(&olBlackPen);
		popDC->MoveTo(olCheckRect.left+1, olCheckRect.top+/*ilTopOffset*/+ilLY-1);
		popDC->LineTo(olCheckRect.left+ilColWidth, olCheckRect.top+ilLY-1);

		popDC->SelectObject(&olWhitePen);
		popDC->MoveTo(olCheckRect.left+1, olCheckRect.top+ilLY);
		popDC->LineTo(olCheckRect.left+ilColWidth, olCheckRect.top+ilLY);

		ilLY+=ilRowHeight;
	}

	olCheckRect.left += 5;
	olCheckRect.top += 25;
	olCheckRect.bottom += 7;
	olCheckRect.InflateRect(2,2);
	popDC->SetBkMode(TRANSPARENT);
	popDC->DrawText(pomProperties->omName, &olCheckRect, (DT_LEFT/*DT_VCENTER|DT_LEFT|DT_SINGLELINE*/) );

	popDC->SelectObject(pOldBrush);
	popDC->SelectObject(pOldPen);
}

int UFormGridControl::GetHandleCount()
{
	ASSERT_VALID(this);
	return UFormControlObj::GetHandleCount();
}
BOOL UFormGridControl::Intersects(const CRect& ropRect)
{
	ASSERT_VALID(this);

	CRect rectT = ropRect;
	rectT.NormalizeRect();

	CRect fixed = omPosition;
	fixed.NormalizeRect();
	if ((rectT & fixed).IsRectEmpty())
		return FALSE;

	CRgn rgn;
		return TRUE;

	return rgn.RectInRegion(fixed);
}
UFormControlObj* UFormGridControl::Clone(FormDesignerDoc* popDoc, UFormControlObj* pObj)
{
	ASSERT_VALID(this);

	UFormGridControl* polClone = new UFormGridControl(omPosition);
	polClone->pomProperties = (UCtrlProperties*) new UGridCtrlProperties();
	*(polClone->pomProperties) = *(pObj->pomProperties);
	int ilC = popDoc->IncGridCounter();
	CString olC; olC.Format("%d", ilC);
	CString olName = polClone->pomProperties->omName;
	polClone->pomProperties->omName = olName + olC;
	popDoc->AddCtrl((UCtrlProperties*)polClone->pomProperties);
	ASSERT_VALID(polClone);

	if (popDoc != NULL)
		popDoc->Add(polClone);

	ASSERT_VALID(polClone);
	return polClone;
}

//XXX End Grid Control


//XXX
////Control Radiobutton
UFormRadioButtonControl::UFormRadioButtonControl()
{
}

UFormRadioButtonControl::UFormRadioButtonControl(const CRect& ropPosition)
	: UFormControlObj(ropPosition)
{
	ASSERT_VALID(this);

}

void UFormRadioButtonControl::OnEditProperties(FormDesignerView* popView)
{
	ShowPropertyDlg(pomProperties, (UFormControlObj *)this, popView);
}

void UFormRadioButtonControl::Draw(CDC* popDC, FormDesignerView* popView)
{
	ASSERT_VALID(this);

	CBrush blSilverBrush( COLORREF(RGB(192,192,192)) );
	CBrush blWhiteBrush( COLORREF(RGB(255,255,255)) );
	CBrush blGrayBrush( COLORREF(RGB(128,128,128)) );
	CBrush blBlackBrush( COLORREF(RGB(0,0,0)) );
	CBrush blBackBrush ( pomProperties->omBackColor );

	CPen pen;
	CPen olSilverPen( PS_SOLID, 1, COLORREF(SILVER));
	CPen olGrayPen  ( PS_SOLID, 1, COLORREF(GRAY));
	CPen olBlackPen ( PS_SOLID, 1, COLORREF(BLACK));
	CPen olWhitePen ( PS_SOLID, 1, COLORREF(WHITE));

	CBrush* pOldBrush;
	CPen* pOldPen;

	pOldBrush = popDC->SelectObject(&blBackBrush);
	pOldPen = popDC->SelectObject(&olSilverPen);
	popDC->SetTextColor(pomProperties->omFontColor);
	popDC->SetBkColor(pomProperties->omBackColor);

	CRect olRect = omPosition;
	popDC->Rectangle(olRect);


	popView->DocToClient(olRect);
	popDC->SelectObject(&olBlackPen); 
	popDC->SelectObject(&ogMSSansSerif_Regular_8);
	CRect olCheckRect = CRect(olRect.left+22, olRect.top, olRect.right, olRect.bottom);
	olCheckRect.NormalizeRect();
	olCheckRect.top = olRect.top + ( ( (int)((olRect.bottom - olRect.top)/2) ) - ( (int)((olCheckRect.bottom - olCheckRect.top)/2)  ));
	popView->ClientToDoc(olCheckRect);
	olCheckRect.InflateRect(2,2);
	popDC->DrawText(pomProperties->omLabel, &olCheckRect, (DT_VCENTER|DT_LEFT|DT_SINGLELINE) );

	olCheckRect;
	olCheckRect = CRect(olRect.left, olRect.top, olRect.left+15, olRect.top+15);
	olCheckRect.top = olRect.top + ( ( (int)((olRect.bottom - olRect.top)/2) ) - ( (int)((olCheckRect.bottom - olCheckRect.top)/2)  ));
	olCheckRect.bottom = olCheckRect.top + 15;

	olCheckRect.InflateRect(-1,-1);
	CRect olTmpRect, olTmpRect2;
	olTmpRect = olCheckRect;
	olTmpRect2 = olTmpRect;
	popDC->SelectObject(&blGrayBrush);
	popDC->SelectObject(&olGrayPen);
	popDC->SetBkMode(TRANSPARENT);
	popView->ClientToDoc(olTmpRect); 
	popDC->Ellipse(olTmpRect);

	olTmpRect2.top+=1;
	olTmpRect2.bottom+=1;
	olTmpRect2.right+=1;
	olTmpRect2.left+=1;
	olTmpRect2.InflateRect(-1,-1);
	popDC->SelectObject(&blWhiteBrush);
	popDC->SelectObject(&olWhitePen);
	popDC->SetBkMode(TRANSPARENT);
	popView->ClientToDoc(olTmpRect2); 
	popDC->Ellipse(olTmpRect2);

	olCheckRect.InflateRect(-1,-1);
	olTmpRect = olCheckRect;
	popDC->SelectObject(&blBlackBrush);
	popDC->SelectObject(&olBlackPen);
	popDC->SetBkMode(TRANSPARENT);
	popView->ClientToDoc(olTmpRect); 
	popDC->Ellipse(olTmpRect);

	olCheckRect.InflateRect(-1,-1);
	olTmpRect = olCheckRect;
	popDC->SelectObject(&blWhiteBrush);
	popDC->SelectObject(&olWhitePen);
	popDC->SetBkMode(TRANSPARENT);
	popView->ClientToDoc(olTmpRect); 
	popDC->Ellipse(olTmpRect);

	popDC->SelectObject(pOldBrush);
	popDC->SelectObject(pOldPen);

}

int UFormRadioButtonControl::GetHandleCount()
{
	ASSERT_VALID(this);
	return UFormControlObj::GetHandleCount();
}

// returns center of handle in logical coordinates
CPoint UFormRadioButtonControl::GetHandle(int nHandle)
{
	ASSERT_VALID(this);
	return UFormControlObj::GetHandle(nHandle);
}

HCURSOR UFormRadioButtonControl::GetHandleCursor(int nHandle)
{
	ASSERT_VALID(this);
	return UFormControlObj::GetHandleCursor(nHandle);
}

// point is in logical coordinates
void UFormRadioButtonControl::MoveHandleTo(int nHandle, CPoint point, FormDesignerView* pView)
{
	ASSERT_VALID(this);
	UFormControlObj::MoveHandleTo(nHandle, point, pView);
}

// rect must be in logical coordinates
BOOL UFormRadioButtonControl::Intersects(const CRect& rect)
{
	ASSERT_VALID(this);

	CRect rectT = rect;
	rectT.NormalizeRect();

	CRect fixed = omPosition;
	fixed.NormalizeRect();
	if ((rectT & fixed).IsRectEmpty())
		return FALSE;

	CRgn rgn;
		return TRUE;

	return rgn.RectInRegion(fixed);
}

UFormControlObj* UFormRadioButtonControl::Clone(FormDesignerDoc* popDoc, UFormControlObj* pObj)
{
	ASSERT_VALID(this);

	UFormRadioButtonControl* polClone = new UFormRadioButtonControl(omPosition);
	polClone->pomProperties = (UCtrlProperties*) new URadioCtrlProperties();
	*(polClone->pomProperties) = *(pObj->pomProperties);

	int ilC = popDoc->IncRadioCounter();
	CString olC; olC.Format("%d", ilC);
	CString olName = polClone->pomProperties->omName;
	polClone->pomProperties->omName = olName + olC;
	popDoc->AddCtrl((UCtrlProperties*)polClone->pomProperties);
	ASSERT_VALID(polClone);

	if (popDoc != NULL)
		popDoc->Add(polClone);

	ASSERT_VALID(polClone);
	return polClone;
}
//Checkbox Control
//XXX


