#if !defined(AFX_UQUERYDESIGNERTABLEVIEW_H__B2E12D10_2DDF_11D3_B07C_00001C019205__INCLUDED_)
#define AFX_UQUERYDESIGNERTABLEVIEW_H__B2E12D10_2DDF_11D3_B07C_00001C019205__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// UQueryDesignerTableView.h : header file
//

#include "resource.h"
#include "CCSGlobl.h"
#include "TableListFrameWnd.h"
#include "UDragDropListBox.h"


struct FLYING_WINDOWS
{
	TableListFrameWnd *Window;
	CRect      Rect;
};

/////////////////////////////////////////////////////////////////////////////
// UQueryDesignerTableView view

class UQueryDesignerTableView : public CScrollView
{
protected:
	UQueryDesignerTableView();           // protected constructor used by dynamic creation
	DECLARE_DYNCREATE(UQueryDesignerTableView)

// Attributes
public:
	CCSPtrArray<FLYING_WINDOWS> omTabWnds;
	CStringArray omTables;

	CString omDropSourceTable;
	CString omDropDestinationTable;
	CString omDropSourceField;
	CString omDropDestinationField;

// Operations
public:
	CRect CalcNextFreeRect();
	void OnTablesChanged();
	void OnAddTables();

	void SetRelSourceTable(CString opTable, CString opField);
	void SetRelDestinationTable(CString opTable, CString opField);
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(UQueryDesignerTableView)
	protected:
	virtual void OnDraw(CDC* pDC);      // overridden to draw this view
	virtual void OnInitialUpdate();     // first time after construct
	virtual void OnUpdate(CView* pSender, LPARAM lHint, CObject* pHint);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual ~UQueryDesignerTableView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	// Generated message map functions
	//{{AFX_MSG(UQueryDesignerTableView)
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
	afx_msg LONG OnDestroyFlyWnd(UINT wParam, LONG lParam);
	afx_msg LONG OnBeginDrag(UINT wParam, LONG lParam);
	afx_msg LONG OnDragOver(UINT wParam, LONG lParam);
	afx_msg LONG OnDrop(UINT wParam, LONG lParam);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_UQUERYDESIGNERTABLEVIEW_H__B2E12D10_2DDF_11D3_B07C_00001C019205__INCLUDED_)
