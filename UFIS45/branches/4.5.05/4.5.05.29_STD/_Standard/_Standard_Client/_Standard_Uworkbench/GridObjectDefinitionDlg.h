#if !defined(AFX_GRIDOBJECTDEFINITIONDLG_H__C1DCAC04_3A88_11D3_B085_00001C019205__INCLUDED_)
#define AFX_GRIDOBJECTDEFINITIONDLG_H__C1DCAC04_3A88_11D3_B085_00001C019205__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// GridObjectDefinitionDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// GridObjectDefinitionDlg dialog

class GridObjectDefinitionDlg : public CDialog
{
// Construction
public:
	GridObjectDefinitionDlg(CWnd* pParent, UGridDefinition *popGridDef);   // standard constructor

	UGridDefinition *pomGridDef;
// Dialog Data
	//{{AFX_DATA(GridObjectDefinitionDlg)
	enum { IDD = IDD_GRIDDEFINITION };
	CStatic	m_ReturnLabel;
	CButton	m_UrnoVisible;
	CEdit	m_TomorrowTo;
	CEdit	m_TomorrowFrom;
	CEdit	m_TodayTo;
	CEdit	m_TodayFrom;
	CButton	m_Today;
	CButton	m_ShowTimeFrameDialog;
	CButton	m_SetDefaultTimeFrame;
	CStatic	m_SepaBmp2;
	CStatic	m_SepaBmp;
	CButton	m_SensitiveForChanges;
	CButton	m_SaveUserLayout;
	CComboBox	m_ReturnField;
	CButton	m_IsPrintable;
	CButton	m_MultiSelect;
	CButton	m_IsEditable;
	CButton	m_HeaderSize;
	CButton	m_HasViewButton;
	CComboBox	m_GroupBy;
	CEdit	m_GridName;
	CComboBox	m_Filter;
	CComboBox	m_DataSource;
	CButton	m_AsChoiceList;
	CButton	m_AsFrameWindow;
	BOOL	v_AsFrameWindow;
	BOOL	v_AsChoiceList;
	CString	v_DataSource;
	CString	v_Filter;
	CString	v_GridName;
	CString	v_GroupBy;
	BOOL	v_HasViewButton;
	BOOL	v_HeaderSize;
	BOOL	v_IsEditable;
	BOOL	v_MultiSelect;
	BOOL	v_Printable;
	CString	v_ReturnField;
	BOOL	v_SaveUserLayout;
	BOOL	v_SensitiveForChanges;
	BOOL	v_SetDefaultTimeFrame;
	BOOL	v_ShowTimeFrameDialog;
	int		v_Today;
	CString	v_TodayFrom;
	CString	v_TodayTo;
	CString	v_TomorrowFrom;
	CString	v_TomorrowTo;
	BOOL	v_UrnoVisible;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(GridObjectDefinitionDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(GridObjectDefinitionDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnAschoicelist();
	afx_msg void OnSetDefaultTimefram();
	afx_msg void OnShowTimeframeDialog();
	afx_msg void OnToday();
	afx_msg void OnTomorrow();
	virtual void OnOK();
	afx_msg void OnSelchangeDatasourceCb();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_GRIDOBJECTDEFINITIONDLG_H__C1DCAC04_3A88_11D3_B085_00001C019205__INCLUDED_)
