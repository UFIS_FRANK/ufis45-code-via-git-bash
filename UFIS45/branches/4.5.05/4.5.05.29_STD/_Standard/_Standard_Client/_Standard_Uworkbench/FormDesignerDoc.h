#if !defined(AFX_FORMDESIGNERDOC_H__99507045_6CE0_11D3_A1EA_0000B4984BBE__INCLUDED_)
#define AFX_FORMDESIGNERDOC_H__99507045_6CE0_11D3_A1EA_0000B4984BBE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// FormDesignerDoc.h : header file
//
#include "UFormControlObj.h"
#include "CtrlProperties.h"

/////////////////////////////////////////////////////////////////////////////
// FormDesignerDoc document

class FormDesignerDoc : public CDocument
{
protected:
	FormDesignerDoc();           // protected constructor used by dynamic creation
	DECLARE_DYNCREATE(FormDesignerDoc)

// Attributes
public:
	UFormControlObjList* GetObjects() { return &omObjects; }
	const CSize& GetSize() const { return omSize; }
	void ComputePageSize();
	int GetMapMode() const { return imMapMode; }
	COLORREF GetPaperColor() const { return omPaperColor; }

	UFormProperties *pomForm;
	void AddCtrl(UCtrlProperties *popCtrl);
	void AddDBField(UFormField* popField);
	void RemoveCtrl(UCtrlProperties *popCtrl);
	void RemoveDBField(UFormField* popField);
	int  IncStaticCounter();
	int  IncEditCounter();
	int  IncComboboxCounter();
	int  IncCheckboxCounter();
	int  IncRadioCounter();
	int  IncListBoxCounter();
	int  IncButtonCounter();
	int  IncRectCounter();
	int  IncLineCounter();
	int	 IncGridCounter();
// Operations
public:
	UFormControlObj* ObjectAt(const CPoint& ropPoint);
	void Draw(CDC* popDC, FormDesignerView* pView);
	void Add(UFormControlObj* popObj);
	void Remove(UFormControlObj* popObj);

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(FormDesignerDoc)
	public:
	virtual void Serialize(CArchive& ar);   // overridden for document i/o
	virtual BOOL OnOpenDocument(LPCTSTR lpszPathName);
	virtual BOOL CanCloseFrame(CFrameWnd* pFrame);
	protected:
	virtual BOOL OnNewDocument();
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~FormDesignerDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	// Generated message map functions
protected:
	UFormControlObjList omObjects;
	CSize				omSize;
	int					imMapMode;
	COLORREF			omPaperColor;

	//{{AFX_MSG(FormDesignerDoc)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_FORMDESIGNERDOC_H__99507045_6CE0_11D3_A1EA_0000B4984BBE__INCLUDED_)
