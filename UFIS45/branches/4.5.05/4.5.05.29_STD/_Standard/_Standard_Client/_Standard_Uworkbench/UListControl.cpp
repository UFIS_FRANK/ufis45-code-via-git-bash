// UListControl.cpp : implementation file
//

#include "stdafx.h"
#include "UWorkBench.h"
#include "UListControl.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// UListControl

UListControl::UListControl()
{
}

UListControl::~UListControl()
{
}


BEGIN_MESSAGE_MAP(UListControl, CListCtrl)
	//{{AFX_MSG_MAP(UListControl)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// UListControl message handlers
