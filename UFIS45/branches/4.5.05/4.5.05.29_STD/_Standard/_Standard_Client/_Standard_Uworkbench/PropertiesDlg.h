#if !defined(AFX_PROPERTIESDLG_H__3FB57D63_70E5_11D3_A1F1_0000B4984BBE__INCLUDED_)
#define AFX_PROPERTIESDLG_H__3FB57D63_70E5_11D3_A1F1_0000B4984BBE__INCLUDED_
 
#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// PropertiesDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// PropertiesDlg dialog
#define PROP_PROPERTIES 0
#define PROP_EVENTS		1

#include "resource.h"
#include "GxGridTab.h"
#include "CtrlProperties.h"
#include "UFormControlObj.h"
#include "FormDesignerView.h"

#include "VBScriptEditorDoc.h"
#include "VBScriptEditorMDI.h"
#include "VBScriptEditorView.h"


class PropertiesDlg : public CDialog
{
// Construction
public:
	PropertiesDlg(CWnd* pParent, 
				  UCtrlProperties *popProperties, 
				  UFormControlObj *popCtrlObj = NULL, 
				  CString opTable = CString(), 
				  FormDesignerView* popView = NULL);   // standard constructor
	~PropertiesDlg();
	UCtrlProperties *pomProperties;
	UFormControlObj *pomCtrlObj;
	FormDesignerView* pomView;
// Dialog Data
	//{{AFX_DATA(PropertiesDlg)
	enum { IDD = IDD_DETAIL_PROPERTIES };
	CButton	omOnOkButton;
	CTabCtrl	m_Tab;
	//}}AFX_DATA

	GxGridTab *pomPropTab;

	// Current selected tab page
	// 0 = PROP_PROPERTIES
	// 1 = PROP_EVENTS
	int imCurrentPage;

	CString omCurrentTable;
	CString omCurrentControl;

	// Common for events and properties
	void SetObjectProperties(UCtrlProperties *popProperties, 
							 UFormControlObj *popCtrlObj, 
							 CString opTable = CString(), 
							 FormDesignerView* popView = NULL);
	void SetProperties(UCtrlProperties *popProperties, 
							 UFormControlObj *popCtrlObj, 
							 CString opTable = CString(), 
							 FormDesignerView* popView = NULL);
	void SetEvents(UCtrlProperties *popProperties, 
							 UFormControlObj *popCtrlObj, 
							 CString opTable = CString(), 
							 FormDesignerView* popView = NULL);

	// Cell of the Grid changed
	void RowColChanged_Properties(UINT wParam, LONG lParam);
	void RowColChanged_Event(UINT wParam, LONG lParam);
	
	//Button of the grid was clicked
	void GridButtonClicked_Properties(UINT wParam, LONG lParam);
	void GridButtonClicked_Event(UINT wParam, LONG lParam);

	//Open the VB-script MDIs
	void OpenVBScriptForFunction(CString opFileName, CString opFunction);
	// Create and Select function
	void JumpToFunction(CVBScriptEditorView* popView, CString opFunction, CString opParameters = CString(""));
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(PropertiesDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void PostNcDestroy();
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(PropertiesDlg)
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	afx_msg void OnDestroy();
	afx_msg void OnClose();
	afx_msg LONG OnRowColChanged(UINT wParam, LONG lParam);
	afx_msg LONG OnGridButtonClicked(UINT wParam, LONG lParam);
	afx_msg LONG OnPropertyUpdate(UINT wParam, LONG lParam);
	afx_msg void OnSelchangeTab1(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnButton1();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PROPERTIESDLG_H__3FB57D63_70E5_11D3_A1F1_0000B4984BBE__INCLUDED_)
