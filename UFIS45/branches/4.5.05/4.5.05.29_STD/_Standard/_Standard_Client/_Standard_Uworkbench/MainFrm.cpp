// MainFrm.cpp : implementation of the CMainFrame class
//
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 *	UWorkBench - Project
 *	
 *		Mainframe Dialog class
 *		
 *
 *	ABB Airport Technologies GmbH 2000
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 *
 *		Date		Author			Comment
 *		-------------------------------------------------------------
 *
 *		??/??/??	mwo AAT/ID		Initial version
 *		23/11/00	cla AAT/ID		Adaptions for the Workbench Step 2 made
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
#include "stdafx.h"
#include "UWorkBench.h"
#include "UQueryDesingerDoc.h"
#include "UUFisObjectsMDI.h"
#include "MainFrm.h"
#include "QueryChildFrame.h"
#include "UUniversalGridMDI.h"
#include "UUniversalGridDoc.h"
#include "UUniversalGridView.h"


//Previews
#include "UFISGridViewerDoc.h"
#include "UFISGridMDIChild.h"
#include "UFISGridViewerView.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMainFrame

IMPLEMENT_DYNAMIC(CMainFrame, CMDIFrameWnd)

BEGIN_MESSAGE_MAP(CMainFrame, CMDIFrameWnd)
	//{{AFX_MSG_MAP(CMainFrame)
	ON_WM_CREATE()
	ON_WM_NCACTIVATE()
	ON_MESSAGE(WM_BCADD,OnBcAdd)
	//}}AFX_MSG_MAP
	// Global help commands
	ON_COMMAND_EX(ID_SHOW_WORKSPACE, OnBarCheck)
	ON_UPDATE_COMMAND_UI(ID_SHOW_WORKSPACE, CMDIFrameWnd::OnUpdateControlBarMenu)
	ON_COMMAND(ID_HELP_FINDER, CMDIFrameWnd::OnHelpFinder)
	ON_COMMAND(ID_HELP, CMDIFrameWnd::OnHelp)
	ON_COMMAND(ID_CONTEXT_HELP, CMDIFrameWnd::OnContextHelp)
	ON_COMMAND(ID_DEFAULT_HELP, CMDIFrameWnd::OnHelpFinder)
END_MESSAGE_MAP()

static UINT indicators[] =
{
	ID_SEPARATOR,           // status line indicator
	ID_INDICATOR_CAPS,
	ID_INDICATOR_NUM,
	ID_INDICATOR_SCRL,
};

/////////////////////////////////////////////////////////////////////////////
// CMainFrame construction/destruction

CMainFrame::CMainFrame()
{
	// TODO: add member initialization code here
	
}

CMainFrame::~CMainFrame()
{

	omActiveGrids.DeleteAll();
}


int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CMDIFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	if(ogMode == "DESIGN")
	{
		if (!m_wndToolBar.CreateEx(this) ||
			!m_wndToolBar.LoadToolBar(IDR_MAINFRAME))
		{
			TRACE0("Failed to create toolbar\n");
			return -1;      // fail to create
		}
		if (!m_wndDlgBar.Create(this, IDR_MAINFRAME, 
			CBRS_ALIGN_TOP, AFX_IDW_DIALOGBAR))
		{
			TRACE0("Failed to create dialogbar\n");
			return -1;		// fail to create
		}

		if (!m_wndReBar.Create(this) ||
			!m_wndReBar.AddBar(&m_wndToolBar) ||
			!m_wndReBar.AddBar(&m_wndDlgBar))
		{
			TRACE0("Failed to create rebar\n");
			return -1;      // fail to create
		}
	}
	else
	{
		if (!m_wndToolBar.CreateEx(this) ||
			!m_wndToolBar.LoadToolBar(IDR_MAINFRAME_RUNTIME))
		{
			TRACE0("Failed to create toolbar\n");
			return -1;      // fail to create
		}
/*		if (!m_wndDlgBar.Create(this, IDR_MAINFRAME, 
			CBRS_ALIGN_TOP, AFX_IDW_DIALOGBAR))
		{
			TRACE0("Failed to create dialogbar\n");
			return -1;		// fail to create
		}

		if (!m_wndReBar.Create(this) ||
			!m_wndReBar.AddBar(&m_wndToolBar) ||
			!m_wndReBar.AddBar(&m_wndDlgBar))
		{
			TRACE0("Failed to create rebar\n");
			return -1;      // fail to create
		}
*/
	}
	if (!m_wndStatusBar.Create(this) ||
		!m_wndStatusBar.SetIndicators(indicators,
		  sizeof(indicators)/sizeof(UINT)))
	{
		TRACE0("Failed to create status bar\n");
		return -1;      // fail to create
	}

	// TODO: Remove this if you don't want tool tips
	m_wndToolBar.SetBarStyle(m_wndToolBar.GetBarStyle() |
		CBRS_TOOLTIPS | CBRS_FLYBY);

	EnableDocking(CBRS_ALIGN_ANY);
//Workspace
	if( !m_wndWorkspace.Create(this, ID_SHOW_WORKSPACE,
		_T("WKS"), CSize(100,200), CBRS_LEFT ))
	{
		TRACE0("Failed to create dialog bar m_wndWorkspace\n");
		return -1;		// fail to create
	}
	m_wndWorkspace.ShowFrameControls(TRUE, TRUE);// 1. set to FALSE to hide the gripper. 2.set to FALSE to hide the frame buttons.
		
	m_wndWorkspace.SetNormalSize(CSize(200,400));

	m_pObjectDoc = new TabObjectRuntimeDoc;
	ASSERT_KINDOF( CDocument, m_pObjectDoc );
	m_wndWorkspace.AddView( _T( "Runtime" ), RUNTIME_CLASS( TabObjectRuntimeView ), m_pObjectDoc );
	m_wndWorkspace.SetActiveView(RUNTIME_CLASS( TabObjectRuntimeView ));
	// let the document update the tab view.
	m_pObjectDoc->UpdateTabView();


	m_wndWorkspace.EnableDockingOnSizeBar( CBRS_ALIGN_ANY );
	EnableDockingSizeBar( CBRS_ALIGN_ANY );
	DockSizeBar( &m_wndWorkspace );
 
	// set the popoup menu id.
	m_wndWorkspace.SetMenuID( IDR_MAINFRAME, NULL, 2 );
//	m_wndWorkspace.MoveWindow(CRect(100, 100, 300, 300));

//Properties
/*	if( !m_wndProperties.Create(this, 104,
		_T("Properties"), CSize(130,100), CBRS_RIGHT ))
	{
		TRACE0("Failed to create dialog bar m_wndWorkspace\n");
		return -1;		// fail to create
	}
	m_wndProperties.ShowFrameControls(TRUE, TRUE);// 1. set to FALSE to hide the gripper. 2.set to FALSE to hide the frame buttons.
		
	//m_pObjectDoc = new TabObjectRuntimeDoc;
	//ASSERT_KINDOF( CDocument, m_pObjectDoc );
	m_wndProperties.AddView( _T( "Properties" ), RUNTIME_CLASS( TabObjectRuntimeView ), m_pObjectDoc );
	// let the document update the tab view.
	//m_pObjectDoc->UpdateTabView();


	m_wndProperties.EnableDockingOnSizeBar( CBRS_ALIGN_ANY );
	EnableDockingSizeBar( CBRS_ALIGN_ANY );
	DockSizeBar( &m_wndProperties );
 
	// set the popoup menu id.
	m_wndProperties.SetMenuID( IDR_MAINFRAME, NULL, 2 );
*/
	ogCommHandler.RegisterBcWindow(this);
	ogBcHandle.GetBc();

	return 0;
}


LONG CMainFrame::OnBcAdd(UINT wParam, LONG /*lParam*/)
{

	ogBcHandle.GetBc(wParam);
	//ogBcHandle.GetBc();
	return TRUE;
}


void CMainFrame::UpdateTabView()
{
	m_pObjectDoc->UpdateTabView();
}

BOOL CMainFrame::OnNcActivate(BOOL bActive)
{
	if (GXDiscardNcActivate())
		return TRUE;

	return CMDIFrameWnd::OnNcActivate(bActive);
}

BOOL CMainFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	if( !CMDIFrameWnd::PreCreateWindow(cs) )
		return FALSE;
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return TRUE;
}


void CMainFrame::OnNewQueryDesingerWindow()
{
	CQueryChildFrame *polActiveChild = (CQueryChildFrame *)MDIGetActive();
	//CDocument *polDoc;
	UQueryDesingerDoc *polDoc;
	if(polActiveChild == NULL ||
		(polDoc = (UQueryDesingerDoc *)polActiveChild->GetActiveDocument()) == NULL)
	{
		AfxMessageBox(AFX_IDP_COMMAND_FAILURE);
	}
	CMultiDocTemplate* polDocTemplate = (CMultiDocTemplate*)((CUWorkBenchApp*)AfxGetApp())->pDesignerDocTemplate;
	polDoc->SetCurrentQuery(new UQuery());
	//polDoc->SetNewQuery();
	CFrameWnd *polFrameWnd = polDocTemplate->CreateNewFrame(polDoc, polActiveChild);
	polDocTemplate->InitialUpdateFrame(polFrameWnd, polDoc);
	polDocTemplate->CreateNewDocument();


/*===>evtl. wieder rein
	CQueryChildFrame *polActiveChild = (CQueryChildFrame *)MDIGetActive();
	CMultiDocTemplate* polDocTemplate = (CMultiDocTemplate*)((CUWorkBenchApp*)AfxGetApp())->pDesignerDocTemplate;
	UQueryDesingerDoc *polDoc = (UQueryDesingerDoc *)polDocTemplate->CreateNewDocument();
	polDoc->SetTitle("New Query");
	CQueryChildFrame *polFrame = (CQueryChildFrame *)polDocTemplate->CreateNewFrame(polDoc, polActiveChild);
	polDocTemplate->InitialUpdateFrame(polFrame, polDoc);
	polDoc->SetNewQuery();
*/
//	(CMultiDocTemplate*)((CUWorkBenchApp*)AfxGetApp())->pDesignerDocTemplate->OpenDocumentFile(NULL);
}

void CMainFrame::OnNewQueryDesingerWindow(UQuery *popQuery)
{
	CQueryChildFrame *polActiveChild = (CQueryChildFrame *)MDIGetActive();
	//CDocument *polDoc;
	UQueryDesingerDoc *polDoc;
	if(polActiveChild == NULL ||
		(polDoc = (UQueryDesingerDoc *)polActiveChild->GetActiveDocument()) == NULL)
	{
		AfxMessageBox(AFX_IDP_COMMAND_FAILURE);
	}
	CMultiDocTemplate* polDocTemplate = (CMultiDocTemplate*)((CUWorkBenchApp*)AfxGetApp())->pDesignerDocTemplate;
	polDoc->SetCurrentQuery(popQuery);
	CFrameWnd *polFrameWnd = polDocTemplate->CreateNewFrame(polDoc, polActiveChild);
	polDocTemplate->InitialUpdateFrame(polFrameWnd, polDoc);
	UQueryDesingerDoc *polNewDoc = (UQueryDesingerDoc *)polDocTemplate->CreateNewDocument();
	CView *polV = polActiveChild->GetActiveView();

	polDoc->SetCurrentQuery(popQuery);
	//polNewDoc->SetCurrentQuery(popQuery);
   
//	polDoc->UpdateAllViews(NULL);

//	UQueryDesingerDoc *polDoc = (UQueryDesingerDoc *)(CMultiDocTemplate*)((CUWorkBenchApp*)AfxGetApp())->pDesignerDocTemplate->OpenDocumentFile(NULL);
//	polDoc->SetCurrentQuery(popQuery);
}

void CMainFrame::OnShowUFISGrid(UGridDefinition *popGridDef)
{
//	CUFISGridViewerDoc  *polDoc;
//	polDoc = (CUFISGridViewerDoc *)((CMultiDocTemplate*)((CUWorkBenchApp*)AfxGetApp())->pUfisGridMDI->OpenDocumentFile((LPCTSTR )popGridDef));
	CUFISGridViewerDoc *polDoc;
	polDoc = (CUFISGridViewerDoc*)((CMultiDocTemplate*)((CUWorkBenchApp*)AfxGetApp())->pUfisGridMDI->OpenDocumentFile(NULL));
	if(polDoc )
	{
		//polDoc->SetGridDef(popGridDef);
		UFISGridMDIChild* polMDI = (UFISGridMDIChild*)GetActiveFrame( );
		if(polMDI != NULL)
		{
			if(polMDI->IsKindOf( RUNTIME_CLASS(UFISGridMDIChild)))
			{
				CRect olClientRect;
				GetClientRect(&olClientRect);
				ClientToScreen(&olClientRect);
				polMDI->SetDefinition(popGridDef, olClientRect.top);
				polMDI->SetSecurity(popGridDef);
			}
		}
		polDoc->SetDefinitionInfo(popGridDef);
		AddGridMDI(popGridDef->GridName, polMDI, polDoc);

	}

}

void CMainFrame::RemoveGridMDI(CString GridName)
{
	for(int i = omActiveGrids.GetSize()-1; i >= 0; i--)
	{
		if(GridName == omActiveGrids[i].Name)
		{
			omActiveGrids.DeleteAt(i);
		}
	}
}
void CMainFrame::AddGridMDI(CString GridName, UFISGridMDIChild* popMDI, CUFISGridViewerDoc* popDoc)
{
	ActiveMDI *polActiveGrid = new ActiveMDI;
	polActiveGrid->Name = GridName;
	polActiveGrid->pomDoc   = popDoc;
	polActiveGrid->pomMDI	= popMDI;
	omActiveGrids.Add(polActiveGrid);
}

void CMainFrame::ActivateGridMDI(CString GridName)
{
	for(int i = 0; i < omActiveGrids.GetSize(); i++)
	{
		if(omActiveGrids[i].Name == GridName)
		{
			omActiveGrids[i].pomMDI->MDIActivate();
			return;
		}
	}
}

//Activation for VB-Script-Editors
void CMainFrame::RemoveScriptMDI(CString opName)
{
	for(int i = omActiveScripts.GetSize()-1; i >= 0; i--)
	{
		if(opName == omActiveScripts[i].Name)
		{
			omActiveScripts.DeleteAt(i);
		}
	}
}

void CMainFrame::AddScriptMDI(CString opName, CMDIChildWnd* popMDI)
{
	ActiveMDI *polActive = new ActiveMDI;
	polActive->Name = opName;
	polActive->pomMDI	= popMDI;
	omActiveScripts.Add(polActive);
}

void CMainFrame::ActivateScriptMDI(CString opName)
{
	for(int i = 0; i < omActiveScripts.GetSize(); i++)
	{
		if(omActiveScripts[i].Name == opName)
		{
			omActiveScripts[i].pomMDI->MDIActivate();
			return;
		}
	}
}





void CMainFrame::OnNewUniversalGrid(UGridDefinition *popGridDef)
{
	CQueryChildFrame *polActiveChild = (CQueryChildFrame *)MDIGetActive();
	//CDocument *polDoc;
	UUniversalGridDoc *polDoc;
	if(polActiveChild == NULL ||
		(polDoc = (UUniversalGridDoc *)polActiveChild->GetActiveDocument()) == NULL)
	{
		AfxMessageBox(AFX_IDP_COMMAND_FAILURE);
	}
	CMultiDocTemplate* polDocTemplate = (CMultiDocTemplate*)((CUWorkBenchApp*)AfxGetApp())->pUniversalGridTpl;
	polDoc->SetCurrentGridDef(popGridDef);
	CFrameWnd *polFrameWnd = polDocTemplate->CreateNewFrame(polDoc, polActiveChild);
	polDocTemplate->InitialUpdateFrame(polFrameWnd, polDoc);
	UUniversalGridDoc *polNewDoc = (UUniversalGridDoc *)polDocTemplate->CreateNewDocument();
	CView *polV = polActiveChild->GetActiveView();

	polDoc->SetCurrentGridDef(popGridDef);
}
/////////////////////////////////////////////////////////////////////////////
// CMainFrame diagnostics

#ifdef _DEBUG
void CMainFrame::AssertValid() const
{
	CMDIFrameWnd::AssertValid();
}

void CMainFrame::Dump(CDumpContext& dc) const
{
	CMDIFrameWnd::Dump(dc);
}

#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CMainFrame message handlers


// ==================================================================
// 
// FUNCTION :  CMainFrame::OnSaveVbw()
// 
// * Description : Save the Visual Basic Code
// 
// 
// * Author : [cla AAT/ID], Created : [23.11.00 14:51:36]
// 
// * Returns : [void] -
// 
// * Function parameters : 
// 
// ==================================================================
void CMainFrame::OnSaveVbw() 
{
	// TODO: Add your command handler code here
	
}
