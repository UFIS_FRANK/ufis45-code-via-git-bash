#include "stdafx.h"
#include "CCSCedadata.h"
#include "CCSCedacom.h"
#include "LoginDialog.h"
#include "PrivList.h"
//#include "BasicData.h"

#ifdef _DEBUG
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// LoginDialog dialog

//extern CCSBasicData ogBasicData;


LoginDialog::LoginDialog(const char *pcpHomeAirport, CStringArray &opApps, CWnd* pParent /*=NULL*/)
    : CDialog(LoginDialog::IDD, pParent)
{
    //{{AFX_DATA_INIT(LoginDialog)
	//}}AFX_DATA_INIT

	for(int i = 0; i < opApps.GetSize(); i++)
	{
		omApps.Add(opApps[i]);
	}
	strcpy(pcmHomeAirport,pcpHomeAirport);
	//strcpy(pcmAppl,pcpAppl);

	// default text
	INVALID_USERNAME = LoadStg(IDS_STRING985);
	INVALID_APPLICATION = LoadStg(IDS_STRING986);
	INVALID_PASSWORD = LoadStg(IDS_STRING987);
	EXPIRED_USERNAME = LoadStg(IDS_STRING988);
	EXPIRED_APPLICATION = LoadStg(IDS_STRING989);
	EXPIRED_WORKSTATION = LoadStg(IDS_STRING990);
	DISABLED_USERNAME = LoadStg(IDS_STRING991);
	DISABLED_APPLICATION = LoadStg(IDS_STRING992);
	DISABLED_WORKSTATION = LoadStg(IDS_STRING993);
	UNDEFINED_PROFILE = LoadStg(IDS_STRING994);
	MESSAGE_BOX_CAPTION = LoadStg(IDS_STRING995);
	USERNAME_CAPTION = LoadStg(IDS_STRING996);
	PASSWORD_CAPTION = LoadStg(IDS_STRING997);
	OK_CAPTION = LoadStg(IDS_STRING998);
	CANCEL_CAPTION = LoadStg(IDS_STRING999);
	WINDOW_CAPTION = LoadStg(IDS_STRING1000);
}

void LoginDialog::DoDataExchange(CDataExchange* pDX)
{
    CDialog::DoDataExchange(pDX);
    //{{AFX_DATA_MAP(LoginDialog)
	DDX_Control(pDX, IDC_USERNAME, m_UsernameCtrl);
	DDX_Control(pDX, IDC_PASSWORD, m_PasswordCtrl);
	DDX_Control(pDX, IDC_USIDCAPTION, m_UsidCaption);
	DDX_Control(pDX, IDC_PASSCAPTION, m_PassCaption);
	DDX_Control(pDX, IDOK, m_OkCaption);
	DDX_Control(pDX, IDCANCEL, m_CancelCaption);
	DDX_Control(pDX, IDC_APPLICATION, m_AppsCombo);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(LoginDialog, CDialog)
    //{{AFX_MSG_MAP(LoginDialog)
    ON_WM_PAINT()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// LoginDialog message handlers

void LoginDialog::OnPaint() 
{
    CPaintDC dc(this); // device context for painting
    
    // TODO: Add your message handler code here
    CBrush olBrush( RGB( 192, 192, 192 ) );
    CBrush *polOldBrush = dc.SelectObject( &olBrush );
    CRect olRect;
    GetClientRect( &olRect );
    dc.FillRect( &olRect, &olBrush );
    dc.SelectObject( polOldBrush );
    
/*    CDC olDC;
    olDC.CreateCompatibleDC( &dc );
    CBitmap olBitmap;
    olBitmap.LoadBitmap( IDB_LOGIN );
    olDC.SelectObject( &olBitmap );
    dc.BitBlt( 0, 140, 409, 52, &olDC, 0, 0, SRCCOPY );
    olDC.DeleteDC( );
 */  
    // Do not call CDialog::OnPaint() for painting messages
}

void LoginDialog::OnOK() 
{
	bool blRc = true;
	CWnd *polFocusCtrl = &m_UsernameCtrl;
	char pclErrTxt[500];

	// get the username
	m_UsernameCtrl.GetWindowText(omUsername);
	m_PasswordCtrl.GetWindowText(omPassword);


	// check the username + password
	AfxGetApp()->DoWaitCursor(1);
	char pclUserName[100]="";
	char pclPassword[100]="";

	strcpy(pclUserName, omUsername.GetBuffer(0));
	strcpy(pclPassword, omPassword);

	strcpy(pcgUser,omUsername.GetBuffer(0));
	strcpy(pcgPasswd,omPassword);
	ogBasicData.omUserID = omUsername.GetBuffer(0);

	ogCommHandler.SetUser(omUsername.GetBuffer(0));
	strcpy(CCSCedaData::pcmUser, omUsername.GetBuffer(0));
	strcpy(CCSCedaData::pcmReqId, ogCommHandler.pcmReqId);

	CString olApp;
	m_AppsCombo.GetWindowText(olApp);

	ogPseudoAppName = olApp;
	strcpy(pcmAppl, olApp.GetBuffer(0));

	blRc = ogPrivList.Login(pcgTableExt,pclUserName,pclPassword,pcmAppl);
	// blRc = ogPrivList.Login(pcmHomeAirport,omUsername,omPassword,pcmAppl);
	AfxGetApp()->DoWaitCursor(-1);

	if( blRc )
	{
		CDialog::OnOK();
	}
	else
	{
		if( !strcmp(ogPrivList.omErrorMessage,"LOGINERROR INVALID_USER") ) { 
			strcpy(pclErrTxt,INVALID_USERNAME);
		}
		else if( !strcmp(ogPrivList.omErrorMessage,"LOGINERROR INVALID_APPLICATION") ) {
			strcpy(pclErrTxt,INVALID_APPLICATION);
		}
		else if( !strcmp(ogPrivList.omErrorMessage,"LOGINERROR INVALID_PASSWORD") ) {
			strcpy(pclErrTxt,INVALID_PASSWORD);
			polFocusCtrl = &m_PasswordCtrl;
		}
		else if( !strcmp(ogPrivList.omErrorMessage,"LOGINERROR EXPIRED_USER") ) {
			strcpy(pclErrTxt,EXPIRED_USERNAME);
		}
		else if( !strcmp(ogPrivList.omErrorMessage,"LOGINERROR EXPIRED_APPLICATION") ) {
			strcpy(pclErrTxt,EXPIRED_APPLICATION);
		}
		else if( !strcmp(ogPrivList.omErrorMessage,"LOGINERROR EXPIRED_WORKSTATION") ) {
			strcpy(pclErrTxt,EXPIRED_WORKSTATION);
		}
		else if( !strcmp(ogPrivList.omErrorMessage,"LOGINERROR DISABLED_USER") ) {
			strcpy(pclErrTxt,DISABLED_USERNAME);
		}
		else if( !strcmp(ogPrivList.omErrorMessage,"LOGINERROR DISABLED_APPLICATION") ) {
			strcpy(pclErrTxt,DISABLED_APPLICATION);
		}
		else if( !strcmp(ogPrivList.omErrorMessage,"LOGINERROR DISABLED_WORKSTATION") ) {
			strcpy(pclErrTxt,DISABLED_WORKSTATION);
		}
		else if( !strcmp(ogPrivList.omErrorMessage,"LOGINERROR UNDEFINED_PROFILE") ) {
			strcpy(pclErrTxt,UNDEFINED_PROFILE);
		}
		else {
			strcpy(pclErrTxt,ogPrivList.omErrorMessage);
		}

		MessageBox(pclErrTxt,MESSAGE_BOX_CAPTION,MB_ICONINFORMATION);
	
		imLoginCount++;

		if( imLoginCount >= MAX_LOGIN )
			OnCancel();
		else
		{
			if( polFocusCtrl != NULL )
				polFocusCtrl->SetFocus(); // set the focus to the offending control
		}
	}
}


void LoginDialog::OnCancel() 
{
	// TODO: Add extra cleanup here
	
	CDialog::OnCancel();
}




BOOL LoginDialog::OnInitDialog() 
{
	CDialog::OnInitDialog();

	m_AppsCombo.ResetContent();
	for(int i = 0; i < omApps.GetSize(); i++)
	{
		m_AppsCombo.AddString(omApps[i]);
	}
	m_AppsCombo.SetCurSel(0);
	imLoginCount = 0;
	SetWindowText(WINDOW_CAPTION);
	m_UsidCaption.SetWindowText(USERNAME_CAPTION);
	m_PassCaption.SetWindowText(PASSWORD_CAPTION);
	m_OkCaption.SetWindowText(OK_CAPTION);
	m_CancelCaption.SetWindowText(CANCEL_CAPTION);

	m_UsernameCtrl.SetTypeToString("X(32)",32,1);
	m_UsernameCtrl.SetBKColor(YELLOW);
	m_UsernameCtrl.SetTextErrColor(RED);

	m_PasswordCtrl.SetTypeToString("X(32)",32,1);
	m_PasswordCtrl.SetBKColor(YELLOW);
	m_PasswordCtrl.SetTextErrColor(RED);


	m_UsernameCtrl.SetFocus();
//	CDialog::OnInitDialog();

	CString olCaption = LoadStg(IDS_STRING1001);
	olCaption  += ogCommHandler.pcmRealHostName;
	olCaption  += " / ";
	olCaption  += ogCommHandler.pcmRealHostType;
	SetWindowText(olCaption  );

	int ilY = ::GetSystemMetrics(SM_CYSCREEN);
	CRect olRect, olNewRect;
	GetWindowRect(&olRect);
	int ilHeight = olRect.bottom - olRect.top;
	int ilWidth = olRect.right - olRect.left;
	olNewRect.top = (int)((int)(ilY/2) - (int)(ilHeight/2));
	olNewRect.left = 300;
	olNewRect.bottom = olNewRect.top + ilHeight;
	olNewRect.right = olNewRect.left + ilWidth;

	MoveWindow(&olNewRect);

	return FALSE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

