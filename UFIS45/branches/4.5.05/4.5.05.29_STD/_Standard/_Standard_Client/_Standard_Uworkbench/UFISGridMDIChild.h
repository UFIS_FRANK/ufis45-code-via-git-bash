#if !defined(AFX_UFISGRIDMDICHILD_H__4E471453_5EAC_11D3_A1E1_0000B4984BBE__INCLUDED_)
#define AFX_UFISGRIDMDICHILD_H__4E471453_5EAC_11D3_A1E1_0000B4984BBE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// UFISGridMDIChild.h : header file
//

#include "UQueryDesingerDoc.h"

/////////////////////////////////////////////////////////////////////////////
// UFISGridMDIChild frame

class UFISGridMDIChild : public CMDIChildWnd
{
	DECLARE_DYNCREATE(UFISGridMDIChild)
protected:
	UFISGridMDIChild();           // protected constructor used by dynamic creation

// Attributes
public:

// Operations
public:


	CToolBar    m_wndToolBar; //IDR_UNIVERSAL_GRID_TOOLBAR
	CToolBarCtrl *pom_ToolBarCtrl;

	void OnView();
	void OnFilter();
	void OnNew();
	void OnUpdate();
	void OnDelete();
	bool blIsInit;
	UGridDefinition *pomGridDef;

	void SetDefinition(UGridDefinition *popGridDef, int ipOffset);

	//Depending on SEC Enable or Disable the toolbar buttons
	void SetSecurity(UGridDefinition *popGridDef);
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(UFISGridMDIChild)
	public:
	virtual void ActivateFrame(int nCmdShow = -1);
	protected:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual BOOL OnCreateClient(LPCREATESTRUCT lpcs, CCreateContext* pContext);
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual ~UFISGridMDIChild();

	// Generated message map functions
	//{{AFX_MSG(UFISGridMDIChild)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnMove(int x, int y);
	afx_msg void OnClose();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_UFISGRIDMDICHILD_H__4E471453_5EAC_11D3_A1E1_0000B4984BBE__INCLUDED_)
