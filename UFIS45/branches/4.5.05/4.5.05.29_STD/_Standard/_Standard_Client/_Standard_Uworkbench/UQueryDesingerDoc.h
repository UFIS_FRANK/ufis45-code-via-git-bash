#if !defined(AFX_UQUERYDESINGERDOC_H__250AD386_2EDA_11D3_B07E_00001C019205__INCLUDED_)
#define AFX_UQUERYDESINGERDOC_H__250AD386_2EDA_11D3_B07E_00001C019205__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// UQueryDesingerDoc.h : header file
//
#include "CCSGlobl.h"
//#include "QueryDesignerGridView.h"
/////////////////////////////////////////////////////////////////////////////
// UQueryDesingerDoc document

class CQueryDesignerGridView;
class CView;

class UQueryDesingerDoc : public CDocument
{
protected:
	UQueryDesingerDoc();           // protected constructor used by dynamic creation
	DECLARE_DYNCREATE(UQueryDesingerDoc)

// Attributes
public:
	friend CQueryDesignerGridView;
	friend CView;
	UQuery *pomCurrentQuery;
	CStringArray omCurrentQueryDesingerTables;
	CString omCurrentQueryName;

	CGXGridParam* m_pParam;

// Operations
public:
	void SetCurrentQueryDesignerTables(CStringArray &opTabs);
	void SetCurrentQuery(UQuery *popQuery);
	void DeleteRelation(CString opRelation);
	CString GetQueryName();
	void SetNewQuery();
	UQuery *GetQueryByName(CString opName);
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(UQueryDesingerDoc)
	public:
	virtual void Serialize(CArchive& ar);   // overridden for document i/o
	virtual BOOL CanCloseFrame(CFrameWnd* pFrame);
	virtual void OnCloseDocument();
	virtual BOOL OnSaveDocument(LPCTSTR lpszPathName);
	virtual void SetTitle(LPCTSTR lpszTitle);
	virtual void DeleteContents();
	virtual BOOL OnOpenDocument(LPCTSTR lpszPathName);
	protected:
	virtual BOOL OnNewDocument();
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~UQueryDesingerDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	// Generated message map functions
protected:
	//{{AFX_MSG(UQueryDesingerDoc)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_UQUERYDESINGERDOC_H__250AD386_2EDA_11D3_B07E_00001C019205__INCLUDED_)
