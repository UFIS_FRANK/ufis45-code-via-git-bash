#if !defined(AFX_UCHECKBOX_H__66893423_91DE_11D3_A213_00500437F607__INCLUDED_)
#define AFX_UCHECKBOX_H__66893423_91DE_11D3_A213_00500437F607__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// UCheckBox.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// UCheckBox window
#define WM_UCHECKBOX_KILLFOCUS		(WM_USER+9600)
#define WM_UCHECKBOX_CLICKED		(WM_USER+9601)
#define WM_URADIOBUTTON_KILLFOCUS	(WM_USER+9602)
#define WM_URADIOBUTTON_CLICKED		(WM_USER+9603)

class UCheckBox : public CButton
{
// Construction
public:
	UCheckBox(CWnd *popParent, 
			  COLORREF opBkColor = COLORREF(RGB(192,192,192)), 
			  COLORREF opTextColor = COLORREF(RGB(0,0,0)));
	CWnd *pomParent;
	COLORREF omBKColor;
	COLORREF omTextColor;
	CBrush omBrush;


	void SetBackColor(COLORREF opColor);
	void SetTxtColor(COLORREF opColor);

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(UCheckBox)
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~UCheckBox();

	// Generated message map functions
protected:
	//{{AFX_MSG(UCheckBox)
	afx_msg void OnClicked();
	afx_msg HBRUSH CtlColor(CDC* pDC, UINT nCtlColor);
	afx_msg void OnKillFocus(CWnd* pNewWnd);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

class URadioButton : public CButton
{
// Construction
public:
	URadioButton(CWnd *popParent, 
			  COLORREF opBkColor = COLORREF(RGB(192,192,192)), 
			  COLORREF opTextColor = COLORREF(RGB(0,0,0)));
	CWnd *pomParent;
	COLORREF omBKColor;
	COLORREF omTextColor;
	CBrush omBrush;


	void SetBackColor(COLORREF opColor);
	void SetTxtColor(COLORREF opColor);

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(UCheckBox)
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~URadioButton();

	// Generated message map functions
protected:
	//{{AFX_MSG(UCheckBox)
	afx_msg void OnClicked();
	afx_msg HBRUSH CtlColor(CDC* pDC, UINT nCtlColor);
	afx_msg void OnKillFocus(CWnd* pNewWnd);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

class UStatic : public CStatic
{
// Construction
public:
	UStatic(CWnd *popParent, 
			  COLORREF opBkColor = COLORREF(RGB(192,192,192)), 
			  COLORREF opTextColor = COLORREF(RGB(0,0,0)));
	CWnd *pomParent;
	COLORREF omBKColor;
	COLORREF omTextColor;
	CBrush omBrush;


	void SetBackColor(COLORREF opColor);
	void SetTxtColor(COLORREF opColor);

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(UCheckBox)
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~UStatic();

	// Generated message map functions
protected:
	//{{AFX_MSG(UCheckBox)
	afx_msg HBRUSH CtlColor(CDC* pDC, UINT nCtlColor);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_UCHECKBOX_H__66893423_91DE_11D3_A213_00500437F607__INCLUDED_)
