#if !defined(AFX_TABOBJECTRUNTIMEDOC_H__2C2E1894_0540_11D4_A28D_00500437F607__INCLUDED_)
#define AFX_TABOBJECTRUNTIMEDOC_H__2C2E1894_0540_11D4_A28D_00500437F607__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// TabObjectRuntimeDoc.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// TabObjectRuntimeDoc document

class TabObjectRuntimeDoc : public CDocument
{
protected:
	//TabObjectRuntimeDoc();           // protected constructor used by dynamic creation
	DECLARE_DYNCREATE(TabObjectRuntimeDoc)

// Attributes
public:
	TabObjectRuntimeDoc();
// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(TabObjectRuntimeDoc)
	public:
	virtual void Serialize(CArchive& ar);   // overridden for document i/o
	protected:
	virtual BOOL OnNewDocument();
	//}}AFX_VIRTUAL

// Implementation
public:
	void UpdateTabView();
public:
	virtual ~TabObjectRuntimeDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	// Generated message map functions
protected:
	//{{AFX_MSG(TabObjectRuntimeDoc)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TABOBJECTRUNTIMEDOC_H__2C2E1894_0540_11D4_A28D_00500437F607__INCLUDED_)
