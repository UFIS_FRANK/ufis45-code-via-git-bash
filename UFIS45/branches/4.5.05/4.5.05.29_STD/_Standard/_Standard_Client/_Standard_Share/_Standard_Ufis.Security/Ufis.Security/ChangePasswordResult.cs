﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Ufis.Security
{
    /// <summary>
    /// Enumeration for password modification result.
    /// </summary>
    public enum ChangePasswordResult
    {
        Succeeded,
        InvalidUser,
        InvalidPassword,
        MismatchedNewPassword,
        InvalidNewPassword,
        FailedUnknownReason
    }
}
