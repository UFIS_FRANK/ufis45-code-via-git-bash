﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ufis.Data;

namespace Ufis.Security
{
    public interface IUfisUserAuthentication
    {
        /// <summary>
        /// Gets or sets the EntityDataContext used by this instance of UserAuthentication
        /// </summary>
        EntityDataContextBase DataContext { get; set; }        

        /// <summary>
        /// Authenticate the user to ceda system.
        /// </summary>
        /// <param name="userName">The user's name.</param>
        /// <param name="password">The user's password.</param>
        /// <param name="privileges">The user privileges.</param>
        /// <param name="info">The additional information returned from server.</param>
        /// <returns></returns>
        AuthenticationResult AuthenticateUser(string userName, string password, 
            UserPrivileges privileges, SecurityInfo info);

        /// <summary>
        /// Change user's password.
        /// </summary>
        /// <param name="userName">The user's name.</param>
        /// <param name="oldPassword">The user's old password.</param>
        /// <param name="newPassword">The user's new password.</param>
        /// <returns>Change password result.</returns>
        ChangePasswordResult ChangePassword(string userName, string oldPassword, string newPassword);

        /// <summary>
        /// Get password rule.
        /// </summary>
        /// <returns>Password rule.</returns>
        PasswordRule GetPasswordRule();

        /// <summary>
        /// Register the module.
        /// </summary>
        /// <param name="registrationString">A registration string to register.</param>
        /// <returns>Error message (if any).</returns>
        string RegisterModule(string registrationString);
    }
}