/************************************
  REVISION LOG ENTRY
  Revision By: Mihai Filimon
  Revised on 12/11/98 3:19:40 PM
  Comments: NComboCtl.h : Declaration of the CNComboCtrl ActiveX Control class.
			CNComboCtrl : See NComboCtl.cpp for implementation.
 ************************************/


#if !defined(AFX_NCOMBOCTL_H__DE201796_9000_11D2_8726_0040055C08D9__INCLUDED_)
#define AFX_NCOMBOCTL_H__DE201796_9000_11D2_8726_0040055C08D9__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "NWindow.h"

class CNComboCtrl : public COleControl
{
	friend class CNWindow;
	friend class CNListCtrl;
	friend class CNHeader;
	DECLARE_DYNCREATE(CNComboCtrl)

// Constructor
public:
	CNComboCtrl();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CNComboCtrl)
	public:
	virtual void OnDraw(CDC* pdc, const CRect& rcBounds, const CRect& rcInvalid);
	virtual void DoPropExchange(CPropExchange* pPX);
	virtual void OnResetState();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	//}}AFX_VIRTUAL

// Implementation
protected:
	short m_ResultColumn;
	CString m_ColumnWidths;
	CString m_ValueSeparator;
	int m_Mandatory;
	CString m_Label;
	CString m_ReferenceField;
	BOOL m_bCreated;
	CNWindow m_nWindow;
	CString m_DBFields;

	~CNComboCtrl();

	BEGIN_OLEFACTORY(CNComboCtrl)        // Class factory and guid
		virtual BOOL VerifyUserLicense();
		virtual BOOL GetLicenseKey(DWORD, BSTR FAR*);
	END_OLEFACTORY(CNComboCtrl)

	DECLARE_OLETYPELIB(CNComboCtrl)      // GetTypeInfo
	DECLARE_PROPPAGEIDS(CNComboCtrl)     // Property page IDs
	DECLARE_OLECTLTYPE(CNComboCtrl)		// Type name and misc status

// Message maps
	//{{AFX_MSG(CNComboCtrl)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

// Dispatch maps
	//{{AFX_DISPATCH(CNComboCtrl)
	long m_nDropDownHeight;
	afx_msg void OnDropDownHeightChanged();
	afx_msg long GetColumnKey();
	afx_msg void SetColumnKey(long nNewValue);
	afx_msg BOOL GetResizing();
	afx_msg void SetResizing(BOOL bNewValue);
	afx_msg long GetSelectItem();
	afx_msg void SetSelectItem(long nNewValue);
	afx_msg BSTR GetDBFields();
	afx_msg void SetDBFields(LPCTSTR lpszNewValue);
	afx_msg BSTR GetReferenceField();
	afx_msg void SetReferenceField(LPCTSTR lpszNewValue);
	afx_msg BSTR GetLabel();
	afx_msg void SetLabel(LPCTSTR lpszNewValue);
	afx_msg BOOL GetMandatory();
	afx_msg void SetMandatory(BOOL bNewValue);
	afx_msg BSTR GetValueSeparator();
	afx_msg void SetValueSeparator(LPCTSTR lpszNewValue);
	afx_msg BSTR GetColumnWidths();
	afx_msg void SetColumnWidths(LPCTSTR lpszNewValue);
	afx_msg short GetResultColumn();
	afx_msg void SetResultColumn(short nNewValue);
	afx_msg long InsertColumn(long nColumn, LPCTSTR sColumnName, long nWidth, long nJustify, BOOL bEnable, BOOL bVisible);
	afx_msg BOOL DeleteColumn(long nColumn);
	afx_msg long GetColumnCount();
	afx_msg long InsertItem(long nItem, LPCTSTR sItem);
	afx_msg BOOL SetItemText(long nItem, long nSubItem, LPCTSTR sItem);
	afx_msg BOOL SetItemData(long nItem, long wData);
	afx_msg long GetItemCount();
	afx_msg BSTR GetItemText(long nItem, long nSubItem);
	afx_msg long GetItemData(long nItem);
	afx_msg BSTR GetColumnName(long nColumn);
	afx_msg short FindItemText(short nColumn, LPCTSTR lpszColumnText);
	afx_msg short FindItemData(long nItemData);
	afx_msg long GetColumnWidth(long nColumn);
	afx_msg void SetColumnWidth(long nColumn, long nNewValue);
	afx_msg BOOL GetColumnVisible(long nColumn);
	afx_msg void SetColumnVisible(long nColumn, BOOL bNewValue);
	afx_msg BOOL GetColumnEnable(long nColumn);
	afx_msg void SetColumnEnable(long nColumn, BOOL bNewValue);
	//}}AFX_DISPATCH
	DECLARE_DISPATCH_MAP()

	afx_msg void AboutBox();

// Event maps
	//{{AFX_EVENT(CNComboCtrl)
	void FireChangingItem(long nItem)
		{FireEvent(eventidChangingItem,EVENT_PARAM(VTS_I4), nItem);}
	void FireChangedItem(long nItem)
		{FireEvent(eventidChangedItem,EVENT_PARAM(VTS_I4), nItem);}
	void FireChangeColumnKey(long nColumn)
		{FireEvent(eventidChangeColumnKey,EVENT_PARAM(VTS_I4), nColumn);}
	void FireChangeColumnContent(short nColumn, LPCTSTR sColumnContent, long nType)
		{FireEvent(eventidChangeColumnContent,EVENT_PARAM(VTS_I2  VTS_BSTR  VTS_I4), nColumn, sColumnContent, nType);}
	//}}AFX_EVENT
	DECLARE_EVENT_MAP()

// Dispatch and event IDs
public:
	enum {
	//{{AFX_DISP_ID(CNComboCtrl)
	dispidDropDownHeight = 1L,
	dispidColumnKey = 2L,
	dispidResizing = 3L,
	dispidSelectItem = 4L,
	dispidDBFields = 5L,
	dispidReferenceField = 6L,
	dispidLabel = 7L,
	dispidMandatory = 8L,
	dispidValueSeparator = 9L,
	dispidColumnWidths = 10L,
	dispidResultColumn = 11L,
	dispidInsertColumn = 12L,
	dispidDeleteColumn = 13L,
	dispidGetColumnCount = 14L,
	dispidInsertItem = 15L,
	dispidSetItemText = 16L,
	dispidSetItemData = 17L,
	dispidGetItemCount = 18L,
	dispidGetItemText = 19L,
	dispidGetItemData = 20L,
	dispidGetColumnName = 21L,
	dispidColumnWidth = 24L,
	dispidColumnVisible = 25L,
	dispidColumnEnable = 26L,
	dispidFindItemText = 22L,
	dispidFindItemData = 23L,
	eventidChangingItem = 1L,
	eventidChangedItem = 2L,
	eventidChangeColumnKey = 3L,
	eventidChangeColumnContent = 4L,
	//}}AFX_DISP_ID
	};
private:
	
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_NCOMBOCTL_H__DE201796_9000_11D2_8726_0040055C08D9__INCLUDED)
