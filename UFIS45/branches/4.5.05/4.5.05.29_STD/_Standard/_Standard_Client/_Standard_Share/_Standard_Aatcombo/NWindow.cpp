/************************************
  REVISION LOG ENTRY
  Revision By: Mihai Filimon
  Revised on 12/10/98 1:34:39 PM
  Comments: NWindow.cpp : implementation file
 ************************************/

#include "stdafx.h"
#include "NCombo.h"
#include "NWindow.h"
#include "NComboCtl.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CNWindow

#define ID_HEADER	0
#define ID_LISTCTRL 1

// Function name	: CNWindow::CNWindow
// Description	    : 
// Return type		: 
CNWindow::CNWindow()
{
	RegisterNWindow();
	m_pControl = NULL;
	m_nDefaultHeight = 0;
	m_nColumnKey = 0;
}

CNWindow::~CNWindow()
{
}


BEGIN_MESSAGE_MAP(CNWindow, CWnd)
	//{{AFX_MSG_MAP(CNWindow)
	ON_WM_SIZE()
	ON_WM_CREATE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// CNWindow message handlers

#define NWINDOW _T("NWindow")

void CNWindow::RegisterNWindow()
{
	VERIFY(RegisterWindowClass(NWINDOW));
}

// Function name	: CNWindow::PreCreateWindow
// Description	    : 
// Return type		: BOOL 
// Argument         : CREATESTRUCT& cs
BOOL CNWindow::PreCreateWindow(CREATESTRUCT& cs) 
{
	cs.lpszClass = NWINDOW;
	
	return CWnd::PreCreateWindow(cs);
}

// Function name	: CNWindow::OnCreate
// Description	    : 
// Return type		: int 
// Argument         : LPCREATESTRUCT lpCreateStruct
int CNWindow::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	return OnInit();
}

// Function name	: CNWindow::OnInit
// Description	    : Called when this is created. Return 0 if succefull
// Return type		: int 
int CNWindow::OnInit()
{
	CreateFont();
	if (m_nHeader.Create(NULL, NULL, WS_CHILD | WS_VISIBLE, CRect(0,0,0,0), this, ID_HEADER))
	{
		m_nListCtrl.SetParent(this);
		if (m_nListCtrl.CreateEx(WS_EX_TOOLWINDOW | WS_EX_TOPMOST, WC_LISTVIEW, NULL, LVS_SINGLESEL | WS_BORDER | WS_CHILD | LVS_NOCOLUMNHEADER | LVS_REPORT | LVS_SHOWSELALWAYS , CRect(0,0,0,0), GetDesktopWindow(), NULL))
		{
			SetFont(&m_font);
			m_nListCtrl.SetOwner(this);
			m_nHeader.SetParent(this);
			m_nHeader.SetFont(&m_font);
			return 0;
		}
	}
	TRACE(_T("One of the childs creation failed!"));
	return -1;
}

// Function name	: CNWindow::CreateFont
// Description	    : 
// Return type		: void 
void CNWindow::CreateFont()
{
	m_font.DeleteObject();
	VERIFY(CreateFontDisp(m_pControl->GetFont(), m_font));
}
// Function name	: CNWindow::OnSize
// Description	    : On size will resize all childs...
// Return type		: void 
// Argument         : UINT nType
// Argument         : int cx
// Argument         : int cy
void CNWindow::OnSize(UINT nType, int cx, int cy) 
{
	CWnd::OnSize(nType, cx, m_nHeader.GetDefaultHeight());
	Resize(cx, cy);
}

// Function name	: CNWindow::Create
// Description	    : Create the NWindow wiht specified parameters
// Return type		: BOOL 
// Argument         : DWORD wStyle
// Argument         : CRect rect
// Argument         : CWnd *pParent
// Argument         : UINT nID
BOOL CNWindow::Create(DWORD wStyle, CRect rect, CWnd *pParent, UINT nID)
{
	return CWnd::Create(NWINDOW, NULL, wStyle, rect, pParent, nID);
}

// Function name	: CNWindow::Resize
// Description	    : 
// Return type		: void 
void CNWindow::Resize(int cx, int cy)
{
	int nDHeight = m_nHeader.GetDefaultHeight();
	ASSERT (::IsWindow(m_nHeader.m_hWnd));
	m_nHeader.MoveWindow(0,0,cx, nDHeight );
	CRect rect; m_nHeader.GetWindowRect(rect);
	CSize s = rect.Size();
	ASSERT (::IsWindow(m_nListCtrl.m_hWnd));
	CRect r(CPoint(0, s.cy), CSize(cx, max(0, cy - s.cy)));
	ClientToScreen(r);
	m_nListCtrl.MoveWindow(r);
	m_lastSizeListCtrl = r.Size();
}

// Function name	: CNWindow::Resize
// Description	    : 
// Return type		: void 
void CNWindow::Resize()
{
	CRect rC; GetClientRect(rC);
	CRect rL; m_nListCtrl.GetWindowRect(rL);
	Resize(rC.Width(), rL.Height() + m_nHeader.GetDefaultHeight());
}

// Function name	: CNColumn::RefreshEditIDs
// Description	    : After a new column occurs or column deleted, the rest of edit's columns will be refreshe on ID
// Return type		: void 
// Argument         : int nCol
void CNWindow::RefreshEditIDs()
{
	if (CNHeaderCtrl* pNHeader = GetHeader())
	{
		CNEdit* pNEdit = NULL;
		for (int i = 0; pNEdit = pNHeader->GetNEdit(i); i++)
			if (::IsWindow(pNEdit->m_hWnd))
				pNEdit->SetColumn(i);
	}
}

// Function name	: CNWindow::InsertColumn
// Description	    : Insert a new column into NCombo.
// Return type		: int 
// Argument         : int nCol
// Argument         : LPCTSTR lpszColumnName
// Argument         : DWORD nWidth
// Argument         : UINT nJustify
// Argument         : BOOL bEnable
// Argument         : BOOL bVisible
int CNWindow::InsertColumn(int nCol, LPCTSTR lpszColumnName, DWORD nWidth, UINT nJustify, BOOL bEnable, BOOL bVisible)
{
	if (nCol >= 0)
		if (nCol <= GetColumnCount())
		{
			int nItem = GetListCtrl()->InsertColumn(nCol, lpszColumnName, nJustify, nWidth);
			CNColumn* pColumn = new CNColumn(this, nItem, lpszColumnName);
			GetHeader()->m_arColumns.InsertAt(nCol, pColumn);
			RefreshEditIDs();
			EnableColumn(nItem, bEnable);
			VisibleColumn(nItem, bVisible);
			GetHeader()->ScrollToPos(GetListCtrl()->GetHScroll());
			return nItem;
		}
	return -1;
}

// Function name	: CNWindow::DeleteColumn
// Description	    : 
// Return type		: BOOL 
// Argument         : in nCol
BOOL CNWindow::DeleteColumn(int nCol)
{
	if (nCol < GetHeader()->m_arColumns.GetSize())
	{
		CNColumn* pColumn = GetHeader()->m_arColumns[nCol];
		GetHeader()->m_arColumns.RemoveAt(nCol);
		delete pColumn;
		RefreshEditIDs();
	}
	return GetListCtrl()->DeleteColumn(nCol);
}

// Function name	: CNWindow::InsertItem
// Description	    : Insert a new item into NWindow
// Return type		: int 
// Argument         : int nItem
// Argument         : LPCTSTR lpszItem
int CNWindow::InsertItem(int nItem, LPCTSTR lpszItem)
{
	return m_nListCtrl.InsertItem(nItem, lpszItem);
}

// Function name	: CNWindow::GetItemData
// Description	    : return the Item data of nItem
// Return type		: DWORD 
// Argument         : int nItem
DWORD CNWindow::GetItemData(int nItem)
{
	return m_nListCtrl.GetItemData(nItem);
}

// Function name	: CNWindow::SetItemText
// Description	    : Set the subitem text
// Return type		: BOOL 
// Argument         : int nItem
// Argument         : int nSubItem
// Argument         : LPTSTR lpszText
BOOL CNWindow::SetItemText(int nItem, int nSubItem, LPCTSTR lpszText)
{
	return m_nListCtrl.SetItemText(nItem, nSubItem, lpszText);
}

// Function name	: CNWindow::GetItemText
// Description	    : Return the text of element (nItem, nSubItem)
// Return type		: CString 
// Argument         : int nItem
// Argument         : int nSubItem
CString CNWindow::GetItemText(int nItem, int nSubItem)
{
	return m_nListCtrl.GetItemText(nItem, nSubItem);
}

// Function name	: CNWindow::SetItemData
// Description	    : Set the item data of one item
// Return type		: BOOL 
// Argument         : int nItem
// Argument         : DWORD dwData
BOOL CNWindow::SetItemData(int nItem, DWORD dwData)
{
	return m_nListCtrl.SetItemData(nItem, dwData);
}

// Function name	: CNWindow::SetColumnWidth
// Description	    : 
// Return type		: BOOL 
// Argument         : in iColumn
// Argument         : int nNewValue
BOOL CNWindow::SetColumnWidth(int iColumn, int nNewValue)
{
	m_nListCtrl.SetColumnWidth2(iColumn, nNewValue);
	return TRUE;
}


// Function name	: CNWindow::GetLocalFont
// Description	    : 
// Return type		: CFont* 
CFont* CNWindow::GetLocalFont()
{
	return &m_font;
}

// Function name	: CNWindow::SetColumnKey
// Description	    : 
// Return type		: void 
// Argument         : int nColumnKey
void CNWindow::SetColumnKey(int nColumnKey)
{
	m_nColumnKey = nColumnKey;
	NotifyChangeColumnKey(nColumnKey);
}

// Function name	: CNWindow::GetColumnKey
// Description	    : 
// Return type		: int 
int CNWindow::GetColumnKey()
{
	return m_nColumnKey;
}

// Function name	: CNWindow::GetListCtrl
// Description	    : 
// Return type		: CNListCtrl* 
CNListCtrl* CNWindow::GetListCtrl()
{
	if (::IsWindow(m_nListCtrl.m_hWnd))
		return &m_nListCtrl;
	return NULL;
}

// Function name	: CNWindow::GetHeader
// Description	    : 
// Return type		: CNHeaderCtrl* 
CNHeaderCtrl* CNWindow::GetHeader()
{
	if (::IsWindow(m_nHeader.m_hWnd))
		return &m_nHeader;
	return NULL;
}

// Function name	: CNWindow::EnableColumn
// Description	    : Enable or disable one edit column...
// Return type		: BOOL 
// Argument         : int nColumn
// Argument         : BOOL bEnable
BOOL CNWindow::EnableColumn(int nColumn, BOOL bEnable)
{
	if (CNHeaderCtrl* pNHeader = GetHeader())
		if (CWnd* pWnd = pNHeader->GetDlgItem(nColumn))
		{
			pWnd->EnableWindow(bEnable);
			return TRUE;
		}
	return FALSE;
}

// Function name	: CNWindow::VisibleColumn
// Description	    : 
// Return type		: BOOL 
// Argument         : int nColumn
// Argument         : BOOL bVisible
BOOL CNWindow::VisibleColumn(int nColumn, BOOL bVisible)
{
	if (CNHeaderCtrl* pNHeader = GetHeader())
		if (CWnd* pWnd = pNHeader->GetDlgItem(nColumn))
		{
			pWnd->ShowWindow(bVisible ? SW_SHOW : SW_HIDE);
			return TRUE;
		}
	return FALSE;
}

// Function name	: CNWindow::SetResizing
// Description	    : 
// Return type		: void 
// Argument         : BOOL bSizing
void CNWindow::SetResizing(BOOL bSizing)
{
	m_nListCtrl.m_bSizing = bSizing;
}

// Function name	: CNWindow::SetSelectItem
// Description	    : 
// Return type		: void 
// Argument         : int nItem
void CNWindow::SetSelectItem(int nItem)
{
	m_nListCtrl.SetSelect(nItem, TRUE);
}

// Function name	: CNWindow::GetColumnCount
// Description	    : Get count of columns
// Return type		: long 
long CNWindow::GetColumnCount()
{
	return m_nHeader.m_arColumns.GetSize();
}

// Function name	: CNWindow::GetItemCount
// Description	    : 
// Return type		: long 
long CNWindow::GetItemCount()
{
	return m_nListCtrl.GetItemCount();
}

BOOL CNWindow::GetResizing()
{
	return m_nListCtrl.m_bSizing;
}

// Function name	: CNWindow::GetColumnName
// Description	    : Return the column name
// Return type		: CString 
// Argument         : int nColumn
CString CNWindow::GetColumnName(int nColumn)
{
	if (nColumn >= 0)
		if (nColumn < GetColumnCount())
			return GetHeader()->m_arColumns[nColumn]->GetName();
	return _T("");
}

// Function name	: CNWindow::GetSelectItem
// Description	    : return the index of selected item
// Return type		: long 
long CNWindow::GetSelectItem()
{
	return m_nListCtrl.GetSelect();
}


// Function name	: CNWindow::GetVisibleColumn
// Description	    : return TRUE if nColumn is visible
// Return type		: BOOL 
// Argument         : int nColumn
BOOL CNWindow::GetVisibleColumn(int nColumn)
{
	if (CNHeaderCtrl* pNHeader = GetHeader())
		if (CWnd* pWnd = pNHeader->GetDlgItem(nColumn))
			return pWnd->GetStyle() & WS_VISIBLE;
	return FALSE;
}

// Function name	: CNWindow::GetEnableColumn
// Description	    : return TRUE if nColumn is enable
// Return type		: BOOL 
// Argument         : int nColumn
BOOL CNWindow::GetEnableColumn(int nColumn)
{
	if (CNHeaderCtrl* pNHeader = GetHeader())
		if (CWnd* pWnd = pNHeader->GetDlgItem(nColumn))
			return (!(pWnd->GetStyle() & WS_DISABLED));
	return FALSE;
}

// Function name	: CNWindow::NotifyChangeColumnKey
// Description	    : 
// Return type		: void 
// Argument         : long nColumn
void CNWindow::NotifyChangeColumnKey(long nColumn)
{
	if (m_pControl)
		m_pControl->FireChangeColumnKey(nColumn);
}


short CNWindow::FindItemText(short nColumn, LPCTSTR lpszColumnText)
{
	return m_nListCtrl.FindItemText(nColumn, lpszColumnText);
}

short CNWindow::FindItemData(long nItemData)
{
	return m_nListCtrl.FindItemData(nItemData);
}
