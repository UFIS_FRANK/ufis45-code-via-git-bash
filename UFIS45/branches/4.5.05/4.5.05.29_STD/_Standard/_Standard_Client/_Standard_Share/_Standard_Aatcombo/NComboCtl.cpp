// NComboCtl.cpp : Implementation of the CNComboCtrl ActiveX Control class.

#include "stdafx.h"
#include "NCombo.h"
#include "NComboCtl.h"
#include "NComboPpg.h"
#include "AboutDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


IMPLEMENT_DYNCREATE(CNComboCtrl, COleControl)


/////////////////////////////////////////////////////////////////////////////
// Message map

BEGIN_MESSAGE_MAP(CNComboCtrl, COleControl)
	//{{AFX_MSG_MAP(CNComboCtrl)
	ON_WM_CREATE()
	ON_WM_SIZE()
	//}}AFX_MSG_MAP
	ON_OLEVERB(AFX_IDS_VERB_PROPERTIES, OnProperties)
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// Dispatch map

BEGIN_DISPATCH_MAP(CNComboCtrl, COleControl)
	//{{AFX_DISPATCH_MAP(CNComboCtrl)
	DISP_PROPERTY_NOTIFY(CNComboCtrl, "DropDownHeight", m_nDropDownHeight, OnDropDownHeightChanged, VT_I4)
	DISP_PROPERTY_EX(CNComboCtrl, "ColumnKey", GetColumnKey, SetColumnKey, VT_I4)
	DISP_PROPERTY_EX(CNComboCtrl, "Resizing", GetResizing, SetResizing, VT_BOOL)
	DISP_PROPERTY_EX(CNComboCtrl, "SelectItem", GetSelectItem, SetSelectItem, VT_I4)
	DISP_PROPERTY_EX(CNComboCtrl, "DBFields", GetDBFields, SetDBFields, VT_BSTR)
	DISP_PROPERTY_EX(CNComboCtrl, "ReferenceField", GetReferenceField, SetReferenceField, VT_BSTR)
	DISP_PROPERTY_EX(CNComboCtrl, "Label", GetLabel, SetLabel, VT_BSTR)
	DISP_PROPERTY_EX(CNComboCtrl, "Mandatory", GetMandatory, SetMandatory, VT_BOOL)
	DISP_PROPERTY_EX(CNComboCtrl, "ValueSeparator", GetValueSeparator, SetValueSeparator, VT_BSTR)
	DISP_PROPERTY_EX(CNComboCtrl, "ColumnWidths", GetColumnWidths, SetColumnWidths, VT_BSTR)
	DISP_PROPERTY_EX(CNComboCtrl, "ResultColumn", GetResultColumn, SetResultColumn, VT_I2)
	DISP_FUNCTION(CNComboCtrl, "InsertColumn", InsertColumn, VT_I4, VTS_I4 VTS_BSTR VTS_I4 VTS_I4 VTS_BOOL VTS_BOOL)
	DISP_FUNCTION(CNComboCtrl, "DeleteColumn", DeleteColumn, VT_BOOL, VTS_I4)
	DISP_FUNCTION(CNComboCtrl, "GetColumnCount", GetColumnCount, VT_I4, VTS_NONE)
	DISP_FUNCTION(CNComboCtrl, "InsertItem", InsertItem, VT_I4, VTS_I4 VTS_BSTR)
	DISP_FUNCTION(CNComboCtrl, "SetItemText", SetItemText, VT_BOOL, VTS_I4 VTS_I4 VTS_BSTR)
	DISP_FUNCTION(CNComboCtrl, "SetItemData", SetItemData, VT_BOOL, VTS_I4 VTS_I4)
	DISP_FUNCTION(CNComboCtrl, "GetItemCount", GetItemCount, VT_I4, VTS_NONE)
	DISP_FUNCTION(CNComboCtrl, "GetItemText", GetItemText, VT_BSTR, VTS_I4 VTS_I4)
	DISP_FUNCTION(CNComboCtrl, "GetItemData", GetItemData, VT_I4, VTS_I4)
	DISP_FUNCTION(CNComboCtrl, "GetColumnName", GetColumnName, VT_BSTR, VTS_I4)
	DISP_FUNCTION(CNComboCtrl, "FindItemText", FindItemText, VT_I2, VTS_I2 VTS_BSTR)
	DISP_FUNCTION(CNComboCtrl, "FindItemData", FindItemData, VT_I2, VTS_I4)
	DISP_PROPERTY_PARAM(CNComboCtrl, "ColumnWidth", GetColumnWidth, SetColumnWidth, VT_I4, VTS_I4)
	DISP_PROPERTY_PARAM(CNComboCtrl, "ColumnVisible", GetColumnVisible, SetColumnVisible, VT_BOOL, VTS_I4)
	DISP_PROPERTY_PARAM(CNComboCtrl, "ColumnEnable", GetColumnEnable, SetColumnEnable, VT_BOOL, VTS_I4)
	DISP_STOCKPROP_FONT()
	DISP_STOCKPROP_BACKCOLOR()
	DISP_STOCKPROP_FORECOLOR()
	DISP_STOCKPROP_ENABLED()
	//}}AFX_DISPATCH_MAP
	DISP_FUNCTION_ID(CNComboCtrl, "AboutBox", DISPID_ABOUTBOX, AboutBox, VT_EMPTY, VTS_NONE)
END_DISPATCH_MAP()


/////////////////////////////////////////////////////////////////////////////
// Event map

BEGIN_EVENT_MAP(CNComboCtrl, COleControl)
	//{{AFX_EVENT_MAP(CNComboCtrl)
	EVENT_CUSTOM("ChangingItem", FireChangingItem, VTS_I4)
	EVENT_CUSTOM("ChangedItem", FireChangedItem, VTS_I4)
	EVENT_CUSTOM("ChangeColumnKey", FireChangeColumnKey, VTS_I4)
	EVENT_CUSTOM("ChangeColumnContent", FireChangeColumnContent, VTS_I2  VTS_BSTR VTS_I4)
	//}}AFX_EVENT_MAP
END_EVENT_MAP()


/////////////////////////////////////////////////////////////////////////////
// Property pages

// TODO: Add more property pages as needed.  Remember to increase the count!
BEGIN_PROPPAGEIDS(CNComboCtrl, 2)
	PROPPAGEID(CNComboPropPage::guid)
	PROPPAGEID(CLSID_CFontPropPage )
END_PROPPAGEIDS(CNComboCtrl)


/////////////////////////////////////////////////////////////////////////////
// Initialize class factory and guid

IMPLEMENT_OLECREATE_EX(CNComboCtrl, "NCOMBO.NComboCtrl.1",
	0xde201788, 0x9000, 0x11d2, 0x87, 0x26, 0, 0x40, 0x5, 0x5c, 0x8, 0xd9)


/////////////////////////////////////////////////////////////////////////////
// Type library ID and version

IMPLEMENT_OLETYPELIB(CNComboCtrl, _tlid, _wVerMajor, _wVerMinor)


/////////////////////////////////////////////////////////////////////////////
// Interface IDs

const IID BASED_CODE IID_DNCombo =
		{ 0xde201786, 0x9000, 0x11d2, { 0x87, 0x26, 0, 0x40, 0x5, 0x5c, 0x8, 0xd9 } };
const IID BASED_CODE IID_DNComboEvents =
		{ 0xde201787, 0x9000, 0x11d2, { 0x87, 0x26, 0, 0x40, 0x5, 0x5c, 0x8, 0xd9 } };


/////////////////////////////////////////////////////////////////////////////
// Control type information

static const DWORD BASED_CODE _dwNComboOleMisc =
	OLEMISC_ACTIVATEWHENVISIBLE |
	OLEMISC_SETCLIENTSITEFIRST |
	OLEMISC_INSIDEOUT |
	OLEMISC_CANTLINKINSIDE |
	OLEMISC_RECOMPOSEONRESIZE;

IMPLEMENT_OLECTLTYPE(CNComboCtrl, IDS_NCOMBO, _dwNComboOleMisc)


/////////////////////////////////////////////////////////////////////////////
// CNComboCtrl::CNComboCtrlFactory::UpdateRegistry -
// Adds or removes system registry entries for CNComboCtrl

BOOL CNComboCtrl::CNComboCtrlFactory::UpdateRegistry(BOOL bRegister)
{
	// TODO: Verify that your control follows apartment-model threading rules.
	// Refer to MFC TechNote 64 for more information.
	// If your control does not conform to the apartment-model rules, then
	// you must modify the code below, changing the 6th parameter from
	// afxRegApartmentThreading to 0.

	if (bRegister)
		return AfxOleRegisterControlClass(
			AfxGetInstanceHandle(),
			m_clsid,
			m_lpszProgID,
			IDS_NCOMBO,
			IDB_NCOMBO,
			afxRegApartmentThreading,
			_dwNComboOleMisc,
			_tlid,
			_wVerMajor,
			_wVerMinor);
	else
		return AfxOleUnregisterClass(m_clsid, m_lpszProgID);
}


/////////////////////////////////////////////////////////////////////////////
// Licensing strings

static const TCHAR BASED_CODE _szLicFileName[] = _T("NCombo.lic");

static const WCHAR BASED_CODE _szLicString[] =
	L"Copyright (c) 1998 Filimon";


/////////////////////////////////////////////////////////////////////////////
// CNComboCtrl::CNComboCtrlFactory::VerifyUserLicense -
// Checks for existence of a user license

BOOL CNComboCtrl::CNComboCtrlFactory::VerifyUserLicense()
{
	return AfxVerifyLicFile(AfxGetInstanceHandle(), _szLicFileName,
		_szLicString);
}


/////////////////////////////////////////////////////////////////////////////
// CNComboCtrl::CNComboCtrlFactory::GetLicenseKey -
// Returns a runtime licensing key

BOOL CNComboCtrl::CNComboCtrlFactory::GetLicenseKey(DWORD dwReserved,
	BSTR FAR* pbstrKey)
{
	if (pbstrKey == NULL)
		return FALSE;

	*pbstrKey = SysAllocString(_szLicString);
	return (*pbstrKey != NULL);
}


/////////////////////////////////////////////////////////////////////////////
// CNComboCtrl::CNComboCtrl - Constructor

// Function name	: CNComboCtrl::CNComboCtrl
// Description	    : Default constructor
// Return type		: 
CNComboCtrl::CNComboCtrl()
{
	InitializeIIDs(&IID_DNCombo, &IID_DNComboEvents);

	m_bCreated = FALSE;
}


/////////////////////////////////////////////////////////////////////////////
// CNComboCtrl::~CNComboCtrl - Destructor

CNComboCtrl::~CNComboCtrl()
{
	// TODO: Cleanup your control's instance data here.
}


/////////////////////////////////////////////////////////////////////////////
// CNComboCtrl::OnDraw - Drawing function

void CNComboCtrl::OnDraw(
			CDC* pdc, const CRect& rcBounds, const CRect& rcInvalid)
{
	 CBrush bkBrush(TranslateColor(GetBackColor()));
	 pdc->FillRect(rcBounds, &bkBrush);
	// TODO: Replace the following code with your own drawing code.
	//pdc->SetBkColor(RGB(128,0,0));
}


/////////////////////////////////////////////////////////////////////////////
// CNComboCtrl::DoPropExchange - Persistence support

void CNComboCtrl::DoPropExchange(CPropExchange* pPX)
{
	ExchangeVersion(pPX, MAKELONG(_wVerMinor, _wVerMajor));
	COleControl::DoPropExchange(pPX);

	PX_Long(pPX, _T("DropDownHeight"), m_nDropDownHeight, 256);

	PX_String(pPX, _T("DBFields"), m_DBFields, "");
	PX_String(pPX, _T("ReferenceField"), m_ReferenceField, "");
	PX_String(pPX, _T("Label"), m_Label, "");
	PX_String(pPX, _T("ValueSeparator"), m_ValueSeparator, ";");
	PX_String(pPX, _T("ColumnWidths"), m_ColumnWidths, ";");

	PX_Bool(pPX, _T("Mandatory"), m_Mandatory, FALSE);

	PX_Short(pPX, _T("ResultColumn"), m_ResultColumn, 0);
}


/////////////////////////////////////////////////////////////////////////////
// CNComboCtrl::OnResetState - Reset control to default state

void CNComboCtrl::OnResetState()
{
	COleControl::OnResetState();  // Resets defaults found in DoPropExchange

	SetControlSize(100, m_nDropDownHeight);
}

/////////////////////////////////////////////////////////////////////////////
// CNComboCtrl::AboutBox - Display an "About" box to the user

void CNComboCtrl::AboutBox()
{
	CAboutDlg dlgAbout;
	dlgAbout.DoModal();
}

/////////////////////////////////////////////////////////////////////////////
// CNComboCtrl message handlers

typedef TCHAR (*RNDSTR) (CString* pString);

TCHAR RandomLimited(CString* pString)
{
        return (TCHAR)pString->GetAt(((BYTE)rand()) %
pString->GetLength());
}

TCHAR RandomUnLimited(CString* pString)
{
        return (TCHAR)(BYTE)rand();
}

CString RandomString(int nLength = 16, CString sWhat = _T("0123456789"))
{
        RNDSTR f = sWhat.IsEmpty() ? RandomUnLimited : RandomLimited;
        CString result;
        for (int i = 0; i < nLength; i++)
                result += f(&sWhat);
        return result;
}

// Function name	: CNComboCtrl::OnCreate
// Description	    : On create the control, we will create too a NWindow control
// Return type		: int 
// Argument         : LPCREATESTRUCT lpCreateStruct
int CNComboCtrl::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (COleControl::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	m_nWindow.m_pControl = this;
	if (m_bCreated = m_nWindow.Create(WS_CHILD | WS_VISIBLE, CRect(0,0,0,0), this, 0))
	{
/*		m_nWindow.SetResizing();
		m_nWindow.InsertColumn(0, _T("Column 2"), 256, LVCFMT_CENTER, FALSE);
		m_nWindow.InsertColumn(1, _T("Column 2"), 64, LVCFMT_RIGHT);
		m_nWindow.InsertColumn(2, _T("Column 2"), 164);
		m_nWindow.InsertColumn(0, _T("Column 2"), 87);

		for (int i = 0; i < 102; i++)
		{
			CString text;
			text.Format(_T("Item %i"), i);
			int ij = m_nWindow.InsertItem(0, text);
			for (int j = 1; j < 6; j++)
				m_nWindow.SetItemText(ij,j, RandomString());
		}	

		m_nWindow.SetSelectItem(20);*/
	}
	
	return 0;
}

// Function name	: CNComboCtrl::OnSize
// Description	    : 
// Return type		: void 
// Argument         : UINT nType
// Argument         : int cx
// Argument         : int cy
void CNComboCtrl::OnSize(UINT nType, int cx, int cy) 
{
	BOOL bVisible = IsWindowVisible();
	if (!bVisible)
		cy = m_nDropDownHeight;
	COleControl::OnSize(nType, cx, m_nWindow.GetHeader()->GetDefaultHeight());
	SetControlSize(cx, m_nWindow.GetHeader()->GetDefaultHeight());
	if (bVisible)
	{
		m_nDropDownHeight = cy;
		OnDropDownHeightChanged();
	}
	if (m_bCreated)
		m_nWindow.MoveWindow(0,0, cx, cy);
}

// Function name	: CNComboCtrl::PreCreateWindow
// Description	    : 
// Return type		: BOOL 
// Argument         : CREATESTRUCT& cs
BOOL CNComboCtrl::PreCreateWindow(CREATESTRUCT& cs) 
{
	cs.style |= WS_CLIPCHILDREN | WS_CLIPSIBLINGS;
	
	return COleControl::PreCreateWindow(cs);
}

// Function name	: CNComboCtrl::OnDropDownHeightChanged
// Description	    : 
// Return type		: void 
void CNComboCtrl::OnDropDownHeightChanged() 
{
	m_nWindow.m_nDefaultHeight = m_nDropDownHeight;
	SetModifiedFlag();
}

// Function name	: CNComboCtrl::PreTranslateMessage
// Description	    : Dispatch the VK_TAB key to focused window
// Return type		: BOOL 
// Argument         : MSG* pMsg
BOOL CNComboCtrl::PreTranslateMessage(MSG* pMsg) 
{
	if (pMsg->message == WM_KEYDOWN)
		switch (pMsg->wParam)
		{
			case VK_TAB:
			case VK_DOWN:
			case VK_UP:
			case VK_LEFT:
			case VK_RIGHT:
			case VK_NEXT:
			case VK_PRIOR:
			case VK_ESCAPE:
			{
				if (CWnd* pFocus = GetFocus())
				{
					pFocus->SendMessage(pMsg->message, pMsg->wParam, pMsg->lParam);
					return TRUE;
				}
			break;
			}
		}
				
	
	return COleControl::PreTranslateMessage(pMsg);
}

// Function name	: CNComboCtrl::InsertColumn
// Description	    : Call this function when you want to insert a new column into NCombo control
// Return type		: long 
// Argument         : long nColumn
// Argument         : LPCTSTR sColumnName
// Argument         : long nWidth
// Argument         : long nJustify
// Argument         : BOOL bEnable
// Argument         : BOOL bVisible
long CNComboCtrl::InsertColumn(long nColumn, LPCTSTR sColumnName, long nWidth, long nJustify, BOOL bEnable, BOOL bVisible) 
{
	return m_nWindow.InsertColumn(nColumn, sColumnName, nWidth, nJustify, bEnable, bVisible);
}

// Function name	: CNComboCtrl::DeleteColumn
// Description	    : Call this function when you wnt to delete one existing column. TRUE if succefull
// Return type		: BOOL 
// Argument         : long nColumn
BOOL CNComboCtrl::DeleteColumn(long nColumn) 
{
	return m_nWindow.DeleteColumn(nColumn);
}

// Function name	: CNComboCtrl::GetColumnCount
// Description	    : Call this when you want to found the count of column into NCombo
// Return type		: long 
long CNComboCtrl::GetColumnCount() 
{
	return m_nWindow.GetColumnCount();
}

// Function name	: CNComboCtrl::InsertItem
// Description	    : Call to insert a new item. Retunr the index of this
// Return type		: long 
// Argument         : long nItem
// Argument         : LPCTSTR sItem
long CNComboCtrl::InsertItem(long nItem, LPCTSTR sItem) 
{
	return m_nWindow.InsertItem(nItem, sItem);
}

// Function name	: CNComboCtrl::SetItemText
// Description	    : Call this function to set up a text for one subitem
// Return type		: BOOL 
// Argument         : long nItem
// Argument         : long nSubItem
// Argument         : LPCTSTR sItem
BOOL CNComboCtrl::SetItemText(long nItem, long nSubItem, LPCTSTR sItem) 
{
	return m_nWindow.SetItemText(nItem, nSubItem, sItem);
}

// Function name	: CNComboCtrl::SetItemData
// Description	    : Set the user data for one item
// Return type		: BOOL 
// Argument         : long nItem
// Argument         : long wData
BOOL CNComboCtrl::SetItemData(long nItem, long wData) 
{
	return m_nWindow.SetItemData(nItem, wData);
}

// Function name	: CNComboCtrl::GetItemCount
// Description	    : 
// Return type		: long 
long CNComboCtrl::GetItemCount() 
{
	return m_nWindow.GetItemCount();
}

// Function name	: CNComboCtrl::GetColumnKey
// Description	    : 
// Return type		: long 
long CNComboCtrl::GetColumnKey() 
{
	return m_nWindow.GetColumnKey();
}

// Function name	: CNComboCtrl::SetColumnKey
// Description	    : 
// Return type		: void 
// Argument         : long nNewValue
void CNComboCtrl::SetColumnKey(long nNewValue) 
{
	m_nWindow.SetColumnKey(nNewValue);

	SetModifiedFlag();
}

// Function name	: CNComboCtrl::GetResizing
// Description	    : 
// Return type		: BOOL 
BOOL CNComboCtrl::GetResizing() 
{
	return m_nWindow.GetResizing();
}

// Function name	: CNComboCtrl::SetResizing
// Description	    : 
// Return type		: void 
// Argument         : BOOL bNewValue
void CNComboCtrl::SetResizing(BOOL bNewValue) 
{
	m_nWindow.SetResizing(bNewValue);

	SetModifiedFlag();
}

// Function name	: CNComboCtrl::GetItemText
// Description	    : Return the item text.
// Return type		: BSTR 
// Argument         : long nItem
// Argument         : long nSubItem
BSTR CNComboCtrl::GetItemText(long nItem, long nSubItem) 
{
	CString strResult = m_nWindow.GetItemText(nItem, nSubItem);

	return strResult.AllocSysString();
}

// Function name	: CNComboCtrl::GetItemData
// Description	    : Return the associated item user data
// Return type		: long 
// Argument         : long nItem
long CNComboCtrl::GetItemData(long nItem) 
{
	return m_nWindow.GetItemData(nItem);
}

// Function name	: CNComboCtrl::GetColumnName
// Description	    : Get the column name
// Return type		: BSTR 
// Argument         : long nColumn
BSTR CNComboCtrl::GetColumnName(long nColumn) 
{
	CString strResult = m_nWindow.GetColumnName(nColumn);

	return strResult.AllocSysString();
}

// Function name	: CNComboCtrl::GetSelectItem
// Description	    : return the index of selected item
// Return type		: long 
long CNComboCtrl::GetSelectItem() 
{
	return m_nWindow.GetSelectItem();
}

// Function name	: CNComboCtrl::SetSelectItem
// Description	    : Set the new selected item
// Return type		: void 
// Argument         : long nNewValue
void CNComboCtrl::SetSelectItem(long nNewValue) 
{
	m_nWindow.SetSelectItem(nNewValue);

	SetModifiedFlag();
}

// Function name	: CNComboCtrl::GetColumnWidth
// Description	    : return the column width in pixels
// Return type		: long 
// Argument         : long nColumn
long CNComboCtrl::GetColumnWidth(long nColumn) 
{
	return m_nWindow.GetListCtrl()->GetColumnWidth(nColumn);
}

// Function name	: CNComboCtrl::SetColumnWidth
// Description	    : Set the new column width for nColumn to nNewValue
// Return type		: void 
// Argument         : long nColumn
// Argument         : long nNewValue
void CNComboCtrl::SetColumnWidth(long nColumn, long nNewValue) 
{
	m_nWindow.SetColumnWidth(nColumn, nNewValue);

	SetModifiedFlag();
}

// Function name	: CNComboCtrl::GetColumnVisible
// Description	    : Return TRUE if column nColumn is visible or not
// Return type		: BOOL 
// Argument         : long nColumn
BOOL CNComboCtrl::GetColumnVisible(long nColumn) 
{
	return m_nWindow.GetVisibleColumn(nColumn);
}

// Function name	: CNComboCtrl::SetColumnVisible
// Description	    : Show or hide the nColumn column
// Return type		: void 
// Argument         : long nColumn
// Argument         : BOOL bNewValue
void CNComboCtrl::SetColumnVisible(long nColumn, BOOL bNewValue) 
{
	m_nWindow.VisibleColumn(nColumn, bNewValue);

	SetModifiedFlag();
}

// Function name	: CNComboCtrl::GetColumnEnable
// Description	    : Return TRUE if column nColumn is enable.
// Return type		: BOOL 
// Argument         : long nColumn
BOOL CNComboCtrl::GetColumnEnable(long nColumn) 
{
	return m_nWindow.GetEnableColumn(nColumn);
}

// Function name	: CNComboCtrl::SetColumnEnable
// Description	    : Enable or disable the column nColumn
// Return type		: void 
// Argument         : long nColumn
// Argument         : BOOL bNewValue
void CNComboCtrl::SetColumnEnable(long nColumn, BOOL bNewValue) 
{
	m_nWindow.EnableColumn(nColumn, bNewValue);

	SetModifiedFlag();
}


short CNComboCtrl::FindItemText(short nColumn, LPCTSTR lpszColumnText) 
{
	return m_nWindow.FindItemText(nColumn, lpszColumnText);
}

short CNComboCtrl::FindItemData(long nItemData) 
{
	return m_nWindow.FindItemData(nItemData);
}

BSTR CNComboCtrl::GetDBFields() 
{
	return m_DBFields.AllocSysString();
}

void CNComboCtrl::SetDBFields(LPCTSTR lpszNewValue) 
{
	if (m_DBFields != lpszNewValue){
        m_DBFields = lpszNewValue;
        SetModifiedFlag();
        InvalidateControl();
	}
}

BSTR CNComboCtrl::GetReferenceField() 
{
	return m_ReferenceField.AllocSysString();
}

void CNComboCtrl::SetReferenceField(LPCTSTR lpszNewValue) 
{
	if (m_ReferenceField != lpszNewValue){
        m_ReferenceField = lpszNewValue;
        SetModifiedFlag();
        InvalidateControl();
	}
}

BSTR CNComboCtrl::GetLabel() 
{
	return m_Label.AllocSysString();
}

void CNComboCtrl::SetLabel(LPCTSTR lpszNewValue) 
{
	if (m_Label != lpszNewValue){
        m_Label = lpszNewValue;
        SetModifiedFlag();
        InvalidateControl();
	}
}

BOOL CNComboCtrl::GetMandatory() 
{
	return m_Mandatory;
}

void CNComboCtrl::SetMandatory(BOOL bNewValue) 
{
	if (m_Mandatory != bNewValue){
        m_Mandatory = bNewValue;
        SetModifiedFlag();
        InvalidateControl();
	}
}


BSTR CNComboCtrl::GetValueSeparator() 
{
	return m_ValueSeparator.AllocSysString();
}

void CNComboCtrl::SetValueSeparator(LPCTSTR lpszNewValue) 
{
	if (m_ValueSeparator != lpszNewValue){
        m_ValueSeparator = lpszNewValue;
        SetModifiedFlag();
        InvalidateControl();
	}
}

BSTR CNComboCtrl::GetColumnWidths() 
{
	return m_ColumnWidths.AllocSysString();
}

void CNComboCtrl::SetColumnWidths(LPCTSTR lpszNewValue) 
{
	if (m_ColumnWidths != lpszNewValue){
        m_ColumnWidths = lpszNewValue;
        SetModifiedFlag();
        InvalidateControl();
	}
}

short CNComboCtrl::GetResultColumn() 
{
	return m_ResultColumn;
}

void CNComboCtrl::SetResultColumn(short nNewValue) 
{
	if (m_ResultColumn != nNewValue){
        m_ResultColumn = nNewValue;
        SetModifiedFlag();
        InvalidateControl();
	}
}
