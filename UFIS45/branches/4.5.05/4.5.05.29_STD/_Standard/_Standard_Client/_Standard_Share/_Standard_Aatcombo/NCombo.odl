// NCombo.odl : type library source for ActiveX Control project.

// This file will be processed by the Make Type Library (mktyplib) tool to
// produce the type library (NCombo.tlb) that will become a resource in
// NCombo.ocx.

#include <olectl.h>
#include <idispids.h>

[ uuid(DE201785-9000-11D2-8726-0040055C08D9), version(1.0),
  helpfile("NCombo.hlp"),
  helpstring("NCombo ActiveX Control module"),
  control ]
library NCOMBOLib
{
	importlib(STDOLE_TLB);
	importlib(STDTYPE_TLB);

	//  Primary dispatch interface for CNComboCtrl

	[ uuid(DE201786-9000-11D2-8726-0040055C08D9),
	  helpstring("Dispatch interface for NCombo Control"), hidden ]
	dispinterface _DNCombo
	{
		properties:
			// NOTE - ClassWizard will maintain property information here.
			//    Use extreme caution when editing this section.
			//{{AFX_ODL_PROP(CNComboCtrl)
			[id(1)] long DropDownHeight;
			[id(DISPID_FONT), bindable] IFontDisp* Font;
			[id(2)] long ColumnKey;
			[id(3)] boolean Resizing;
			[id(4)] long SelectItem;
			[id(DISPID_BACKCOLOR), bindable, requestedit] OLE_COLOR BackColor;
			[id(5)] BSTR DBFields;
			[id(6)] BSTR ReferenceField;
			[id(7)] BSTR Label;
			[id(DISPID_FORECOLOR), bindable, requestedit] OLE_COLOR ForeColor;
			[id(DISPID_ENABLED), bindable, requestedit] boolean Enabled;
			[id(8)] boolean Mandatory;
			[id(9)] BSTR ValueSeparator;
			[id(10)] BSTR ColumnWidths;
			[id(11)] short ResultColumn;
			//}}AFX_ODL_PROP

		methods:
			// NOTE - ClassWizard will maintain method information here.
			//    Use extreme caution when editing this section.
			//{{AFX_ODL_METHOD(CNComboCtrl)
			[id(12)] long InsertColumn(long nColumn, BSTR sColumnName, long nWidth, long nJustify, boolean bEnable, boolean bVisible);
			[id(13)] boolean DeleteColumn(long nColumn);
			[id(14)] long GetColumnCount();
			[id(15)] long InsertItem(long nItem, BSTR sItem);
			[id(16)] boolean SetItemText(long nItem, long nSubItem, BSTR sItem);
			[id(17)] boolean SetItemData(long nItem, long wData);
			[id(18)] long GetItemCount();
			[id(19)] BSTR GetItemText(long nItem, long nSubItem);
			[id(20)] long GetItemData(long nItem);
			[id(21)] BSTR GetColumnName(long nColumn);
			[id(24), propget] long ColumnWidth(long nColumn);
			[id(24), propput] void ColumnWidth(long nColumn, long nNewValue);
			[id(25), propget] boolean ColumnVisible(long nColumn);
			[id(25), propput] void ColumnVisible(long nColumn, boolean bNewValue);
			[id(26), propget] boolean ColumnEnable(long nColumn);
			[id(26), propput] void ColumnEnable(long nColumn, boolean bNewValue);
			[id(22)] short FindItemText(short nColumn, BSTR lpszColumnText);
			[id(23)] short FindItemData(long nItemData);
			//}}AFX_ODL_METHOD

			[id(DISPID_ABOUTBOX)] void AboutBox();
	};

	//  Event dispatch interface for CNComboCtrl

	[ uuid(DE201787-9000-11D2-8726-0040055C08D9),
	  helpstring("Event interface for NCombo Control") ]
	dispinterface _DNComboEvents
	{
		properties:
			//  Event interface has no properties

		methods:
			// NOTE - ClassWizard will maintain event information here.
			//    Use extreme caution when editing this section.
			//{{AFX_ODL_EVENT(CNComboCtrl)
			[id(1)] void ChangingItem(long nItem);
			[id(2)] void ChangedItem(long nItem);
			[id(3)] void ChangeColumnKey(long nColumn);
			[id(4)] void ChangeColumnContent(short nColumn, BSTR sColumnContent, long nType);
			//}}AFX_ODL_EVENT
	};

	//  Class information for CNComboCtrl

	[ uuid(DE201788-9000-11D2-8726-0040055C08D9), licensed,
	  helpstring("NCombo Control"), control ]
	coclass NCombo
	{
		[default] dispinterface _DNCombo;
		[default, source] dispinterface _DNComboEvents;
	};


	//{{AFX_APPEND_ODL}}
	//}}AFX_APPEND_ODL}}
};
