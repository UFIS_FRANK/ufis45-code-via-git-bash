// NCombo.cpp : Implementation of CNComboApp and DLL registration.

#include "stdafx.h"
#include "NCombo.h"
#include "gdi.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


CNComboApp NEAR theApp;

const GUID CDECL BASED_CODE _tlid =
		{ 0xde201785, 0x9000, 0x11d2, { 0x87, 0x26, 0, 0x40, 0x5, 0x5c, 0x8, 0xd9 } };
const WORD _wVerMajor = 1;
const WORD _wVerMinor = 0;


////////////////////////////////////////////////////////////////////////////
// CNComboApp::InitInstance - DLL initialization

BOOL CNComboApp::InitInstance()
{
	BOOL bInit = COleControlModule::InitInstance();

	if (bInit)
	{
		// TODO: Add your own module initialization code here.
	}

	return bInit;
}


////////////////////////////////////////////////////////////////////////////
// CNComboApp::ExitInstance - DLL termination

int CNComboApp::ExitInstance()
{
	// TODO: Add your own module termination code here.

	return COleControlModule::ExitInstance();
}

/////////////////////////////////////////////////////////////////////////////
// DllRegisterServer - Adds entries to the system registry

STDAPI DllRegisterServer(void)
{
	AFX_MANAGE_STATE(_afxModuleAddrThis);

	if (!AfxOleRegisterTypeLib(AfxGetInstanceHandle(), _tlid))
		return ResultFromScode(SELFREG_E_TYPELIB);

	if (!COleObjectFactoryEx::UpdateRegistryAll(TRUE))
		return ResultFromScode(SELFREG_E_CLASS);

	return NOERROR;
}


/////////////////////////////////////////////////////////////////////////////
// DllUnregisterServer - Removes entries from the system registry

STDAPI DllUnregisterServer(void)
{
	AFX_MANAGE_STATE(_afxModuleAddrThis);

	if (!AfxOleUnregisterTypeLib(_tlid, _wVerMajor, _wVerMinor))
		return ResultFromScode(SELFREG_E_TYPELIB);

	if (!COleObjectFactoryEx::UpdateRegistryAll(FALSE))
		return ResultFromScode(SELFREG_E_CLASS);

	return NOERROR;
}

static const CBrush m_brBkGnd(RGB(128,128,128));

BOOL RegisterWindowClass(LPCTSTR lpszClassName)
{
	WNDCLASS wndClass;
		wndClass.style = CS_DBLCLKS | CS_HREDRAW | CS_VREDRAW;
		wndClass.lpfnWndProc = DefWindowProc;
		wndClass.cbClsExtra = NULL;
		wndClass.cbWndExtra = NULL;
		wndClass.hInstance = AfxGetInstanceHandle();
		wndClass.hIcon = NULL;
		wndClass.hCursor = AfxGetApp()->LoadStandardCursor(IDC_ARROW);
		wndClass.hbrBackground = (HBRUSH)m_brBkGnd;
		wndClass.lpszMenuName = NULL;
		wndClass.lpszClassName = lpszClassName;
	return AfxRegisterClass(&wndClass);
}

static const TCHAR* pszL = NULL;
CString _S(int i)
{
	CString sTemp; AfxExtractSubString(sTemp, pszL, i, TCHAR(','));
	return sTemp;
}
#define _L(i) atol(_S(i))
#define _B(i) (BYTE)_L(i)
#define StartConversion(s) pszL = s
void FillLogFont(LOGFONT& logFont, LPCTSTR lpszFontString )
{
	StartConversion(lpszFontString);
	logFont.lfHeight = _L(0);
	logFont.lfWidth = _L(1);
	logFont.lfEscapement = _L(2);
	logFont.lfOrientation = _L(3);
	logFont.lfWeight = _L(4);
	logFont.lfItalic = _B(5);
	logFont.lfUnderline = _B(6);
	logFont.lfStrikeOut = _B(7);
	logFont.lfCharSet = _B(8);
	logFont.lfOutPrecision = _B(9);
	logFont.lfClipPrecision = _B(10);
	logFont.lfQuality = _B(11);
	logFont.lfPitchAndFamily = _B(12);
	memcpy(logFont.lfFaceName, _S(13), LF_FACESIZE);
}

// Function name	: CloneFont
// Description	    : 
// Return type		: void 
// Argument         : CFont* pFontSource
// Argument         : CFont* pFontDestination
// Argument         : LPCTSTR lpszDefaultFont
void CloneFont(CFont* pFontSource, CFont* pFontDestination, LPCTSTR lpszDefaultFont)
{
	ASSERT(pFontDestination);
	LOGFONT logFont;
	FillLogFont(logFont, lpszDefaultFont);
	if (pFontSource)
		if (pFontSource->GetSafeHandle())
			pFontSource->GetLogFont(&logFont);
	pFontDestination->DeleteObject();
	VERIFY(pFontDestination->CreateFontIndirect((const LOGFONT*)&logFont));
}

// Function name	: CreateFontDisp
// Description	    : Create a font from a IFont interface
// Return type		: BOOL 
// Argument         : IDispatch* pDispatchFont
// Argument         : CFont& font
BOOL CreateFontDisp(IDispatch* pDispatchFont, CFont& font)
{
	HFONT hFont = NULL;
	TRY
	{
		IFont* pIFont = NULL;
		if (!FAILED(pDispatchFont->QueryInterface(IID_IFont, (void**)&pIFont)))
			if (!FAILED(pIFont->get_hFont(&hFont)))
			{
				font.DeleteObject();
				LOGFONT logFont;
				CFont::FromHandle(hFont)->GetLogFont(&logFont);
				pIFont->Release();
				return font.CreateFontIndirect(&logFont);
			}
	}
	CATCH_ALL(e)
	{
		e->Delete();
	}
	END_CATCH_ALL;
	return FALSE;
}
