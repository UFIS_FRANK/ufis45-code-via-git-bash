#if !defined(AFX_NHEADERCTRL_H__DE20179F_9000_11D2_8726_0040055C08D9__INCLUDED_)
#define AFX_NHEADERCTRL_H__DE20179F_9000_11D2_8726_0040055C08D9__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// NHeaderCtrl.h : header file
//
#include "NColumn.h"
class CNWindow;
/////////////////////////////////////////////////////////////////////////////
// CNHeaderCtrl window
#include <afxtempl.h>

class CNHeaderCtrl : public CWnd
{
	friend class CNEdit;
	friend class CNWindow;
// Construction
public:
	CNHeaderCtrl();

// Attributes
public:

// Operations
public:

	CNHeaderCtrl* operator = (int nItem);

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CNHeaderCtrl)
	protected:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	//}}AFX_VIRTUAL

// Implementation
public:
	void DecUserReason();
	virtual void IncUserReason();
	virtual BOOL IsUserReason();
	virtual CNEdit* GetNEdit(int nIndex);
	virtual BOOL IsScrolling();
	virtual int Lookup(CWnd* pEdit);
	virtual void Next(CNEdit* pEdit, BOOL bNext = TRUE);
	virtual void ScrollToPos(int nPos);
	virtual void SetParent(CNWindow* pParent);
	int GetDefaultHeight();
	virtual ~CNHeaderCtrl();
	virtual void DrawButton(CDC* pDC, BOOL bUp = TRUE);

	// Generated message map functions
protected:
	int m_nSetText;
	int m_nSelected;
	virtual CRect GetButtonRect();
	virtual int GetTotalWidth2(CNEdit* pEdit, int nMaxScroll);
	CArray<CNColumn*, CNColumn*> m_arColumns;
	virtual int OnInit();
	CBrush m_brBkGnd;
	CNWindow* m_pParent;
	//{{AFX_MSG(CNHeaderCtrl)
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnPaint();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnSetFocus(CWnd* pOldWnd);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	BOOL m_bCaptured;
	virtual void ResizeLastEdit();
	int m_nScrolling;
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_NHEADERCTRL_H__DE20179F_9000_11D2_8726_0040055C08D9__INCLUDED_)
