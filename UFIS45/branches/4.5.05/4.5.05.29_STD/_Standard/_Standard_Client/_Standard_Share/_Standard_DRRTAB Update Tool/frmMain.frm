VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.ocx"
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "TAB.ocx"
Object = "{A2F31E92-C74F-11D3-A251-00500437F607}#1.0#0"; "UfisCom.ocx"
Object = "{7BDE1363-DF9A-4D96-A2A0-4E85E0191F0F}#1.0#0"; "AatLogin.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form frmMain 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "DRRTAB Update Tool"
   ClientHeight    =   9840
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   12705
   Icon            =   "frmMain.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   ScaleHeight     =   9840
   ScaleWidth      =   12705
   StartUpPosition =   2  'CenterScreen
   Begin VB.TextBox txtPENO 
      Height          =   375
      Left            =   3120
      TabIndex        =   1
      Top             =   2040
      Width           =   2895
   End
   Begin VB.TextBox txtHopo 
      Enabled         =   0   'False
      Height          =   330
      Left            =   3000
      TabIndex        =   5
      Text            =   "SIN"
      Top             =   9000
      Width           =   945
   End
   Begin VB.TextBox txtServer 
      Enabled         =   0   'False
      Height          =   330
      Left            =   1080
      TabIndex        =   4
      Top             =   9000
      Width           =   945
   End
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   330
      Left            =   0
      TabIndex        =   0
      Top             =   9510
      Width           =   12705
      _ExtentX        =   22410
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   3
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   19120
            MinWidth        =   15522
            Text            =   "Ready."
            TextSave        =   "Ready."
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   6
            Object.Width           =   1764
            MinWidth        =   1764
            TextSave        =   "16-Mar-09"
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   5
            Object.Width           =   1411
            MinWidth        =   1411
            TextSave        =   "4:17 PM"
         EndProperty
      EndProperty
   End
   Begin UFISCOMLib.UfisCom UfisCom 
      Left            =   5040
      Top             =   8880
      _Version        =   65536
      _ExtentX        =   1164
      _ExtentY        =   1032
      _StockProps     =   0
   End
   Begin VB.Frame Frame1 
      Height          =   735
      Left            =   120
      TabIndex        =   6
      Top             =   8760
      Width           =   4335
      Begin VB.Label lblHOPO 
         Caption         =   "HOPO"
         Height          =   255
         Left            =   2160
         TabIndex        =   8
         Top             =   240
         Width           =   855
      End
      Begin VB.Label lblServer 
         Caption         =   "Server"
         Height          =   255
         Left            =   240
         TabIndex        =   7
         Top             =   240
         Width           =   975
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "DRRTAB UPDATE TOOL"
      Height          =   8535
      Left            =   120
      TabIndex        =   9
      Top             =   120
      Width           =   12495
      Begin VB.CommandButton cmdUpdate 
         Caption         =   "Update"
         Height          =   375
         Left            =   480
         TabIndex        =   22
         Top             =   7680
         Width           =   1095
      End
      Begin TABLib.TAB TABDRR 
         Height          =   3375
         Left            =   480
         TabIndex        =   19
         Tag             =   "{=TABLE=}DRRTAB{=FIELDS=}URNO,ROSL,ROSS,SCOO,SCOD,SDAY,USEC,USEU,FCTC,CDAT,LSTU"
         Top             =   3960
         Width           =   11415
         _Version        =   65536
         _ExtentX        =   20135
         _ExtentY        =   5953
         _StockProps     =   64
      End
      Begin VB.CommandButton cmdExit 
         Caption         =   "Exit"
         Height          =   375
         Left            =   10800
         TabIndex        =   17
         Top             =   7800
         Width           =   1095
      End
      Begin VB.Frame Frame3 
         Height          =   2535
         Left            =   480
         TabIndex        =   11
         Top             =   960
         Width           =   11535
         Begin MSComCtl2.DTPicker DTPicker1 
            Height          =   375
            Left            =   2520
            TabIndex        =   21
            Top             =   480
            Width           =   2895
            _ExtentX        =   5106
            _ExtentY        =   661
            _Version        =   393216
            Format          =   20709377
            CurrentDate     =   39884
         End
         Begin VB.TextBox txtNickname 
            Height          =   375
            Left            =   2520
            Locked          =   -1  'True
            TabIndex        =   3
            Top             =   1920
            Width           =   2895
         End
         Begin VB.TextBox txtName 
            Height          =   375
            Left            =   2520
            Locked          =   -1  'True
            TabIndex        =   2
            Top             =   1440
            Width           =   2895
         End
         Begin VB.CommandButton cmdSearch 
            Caption         =   "Search"
            Height          =   375
            Left            =   9840
            TabIndex        =   15
            Top             =   480
            Width           =   1095
         End
         Begin VB.CommandButton cmdReset 
            Caption         =   "Reset"
            Height          =   375
            Left            =   9840
            TabIndex        =   14
            Top             =   1080
            Width           =   1095
         End
         Begin VB.Label lblDate 
            Caption         =   "Date (DRRTAB.SDAY) :"
            Height          =   255
            Left            =   360
            TabIndex        =   20
            Top             =   480
            Width           =   1935
         End
         Begin VB.Label lblRVAL 
            Caption         =   "Nick Name :"
            Height          =   375
            Left            =   360
            TabIndex        =   16
            Top             =   1920
            Width           =   1695
         End
         Begin VB.Label lblName 
            Caption         =   "Name :"
            Height          =   375
            Left            =   360
            TabIndex        =   13
            Top             =   1440
            Width           =   855
         End
         Begin VB.Label lblPeno 
            Caption         =   "Staff Personnal No :"
            Height          =   375
            Left            =   360
            TabIndex        =   12
            Top             =   960
            Width           =   1815
         End
      End
      Begin VB.Label Label1 
         Caption         =   "Update will RE-activate the status of Level 2 (DRRTAB.ROSS from ""L"" to ""A"")"
         Height          =   255
         Left            =   1800
         TabIndex        =   23
         Top             =   7800
         Width           =   5775
      End
      Begin VB.Label lblDesc 
         Caption         =   "This tool will re-activate staff on Level 2. So that he/she can be release to OPSS-PM afterward"
         Height          =   375
         Left            =   480
         TabIndex        =   10
         Top             =   480
         Width           =   9375
      End
   End
   Begin AATLOGINLib.AatLogin AATLoginControl1 
      Height          =   495
      Left            =   3120
      TabIndex        =   18
      Top             =   9000
      Width           =   1335
      _Version        =   65536
      _ExtentX        =   2355
      _ExtentY        =   873
      _StockProps     =   0
   End
End
Attribute VB_Name = "frmMain"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim strHOPO As String
Dim strServer As String
Dim globURNOlist As String

Dim globTestOnly As Boolean
Dim insertAction As Boolean
Private strTableExt As String
Public frmConnTimesDirty As Boolean
Public lmCurrCol As Long
Private gNewRecord As Boolean
Private gMAPLKEY As String
Private gMAPTYPE As String
Private gMAPRVAL As String
Private gMAPLVAL As String
Private gMAPURNO As String
Private gSaved As Boolean
Private strSTFU As String
Private strDate As String


Public Sub LoadData(ByRef rTab As TABLib.Tab, ByRef pWhere As String)
    Dim strCommand As String
    Dim strTable As String
    Dim strFieldList As String
    Dim strData As String
    Dim strWhere As String

    MousePointer = vbHourglass
    strCommand = "RT"
    rTab.ResetContent

    GetKeyItem strTable, rTab.Tag, "{=TABLE=}", "{="
    GetKeyItem strFieldList, rTab.Tag, "{=FIELDS=}", "{="
    strData = ""
    strWhere = pWhere
    If Len(strWhere) = 0 Then
        GetKeyItem strWhere, rTab.Tag, "{=WHERE=}", "{="
    End If
    If rTab.CedaAction(strCommand, strTable, strFieldList, strData, strWhere) = False Then
        MsgBox "Error loading " & strTable & "!" & vbCrLf & _
            "Field list: " & strFieldList & vbCrLf & _
            "Where statement: " & strWhere, vbCritical, "Error reading data (RT)"
            rTab.GetLastCedaError
    End If
    MousePointer = vbDefault
End Sub
Private Sub cmdExit_Click()
End
    Dim ilRet
    ilRet = MsgBox("Do you want to exit ?", vbYesNo, "EXIT")
    If ilRet = vbYes Then
        Unload frmData
        End
    End If
End Sub
Private Sub cmdReset_Click()
    DTPicker1.Value = Now
    txtPENO.Text = ""
    txtName.Text = ""
    txtNickname = ""
    TABDRR.ResetContent
    TABDRR.Refresh
End Sub

Private Sub cmdSearch_Click()
    Dim strWhere As String
    
    Dim i As Integer
    
    
    If txtPENO.Text <> "" Then
        strWhere = "PENO = '" & txtPENO.Text & "'"
        For i = 0 To frmData.tab_STFTAB.GetLineCount - 1 Step 1
            If txtPENO.Text = frmData.tab_STFTAB.GetColumnValue(i, 1) Then
                strSTFU = frmData.tab_STFTAB.GetColumnValue(i, 0)
                txtName = frmData.tab_STFTAB.GetColumnValue(i, 3)
                txtNickname = frmData.tab_STFTAB.GetColumnValue(i, 2)
            End If
        Next i
    Else
        MsgBox "Please enter Staff Personnel Number."
        txtPENO.SetFocus
        Exit Sub
    End If
    
    strDate = Format(DTPicker1.Value, "YYYYMMDD")
    strWhere = "WHERE STFU = '" + strSTFU + "' AND SDAY = '" + strDate + "' AND URNO > 0 order by sday,rosl"
    frmData.InitTabGeneral TABDRR
    frmData.InitTabForCedaConnection TABDRR
    LoadData TABDRR, strWhere
    TABDRR.AutoSizeColumns
    TABDRR.Sort 0, True, False
    TABDRR.Refresh
    
End Sub

Private Sub cmdUpdate_Click()
    Dim strWhere As String
    Dim strFields As String
    Dim strData As String
    Dim strURNO As String
    Dim RetVal As String
    Dim i As Integer
    
    If TABDRR.GetLineCount = "2" Then
        For i = 0 To TABDRR.GetLineCount - 1
            If TABDRR.GetColumnValue(i, "1") = "2" Then
                If TABDRR.GetColumnValue(i, "2") = "L" Then
                    RetVal = MyMsgBox.CallAskUser(0, 0, 0, "Application Control", "Record found that Level Status is - L, update require, proceed?", "ask", "Yes,No;F", UserAnswer)
                    If RetVal = 1 Then
                        'strURNO = TABDRR.GetFieldValue(i, "URNO")
                        strURNO = TABDRR.GetColumnValue(i, 0)
                        strWhere = "WHERE URNO = '" + strURNO + "'"
                        strFields = "ROSS,USEU,LSTU"
                        strData = "A" & "," & gsUserName & "," & GetTimeStamp(-480)
                        'Updating record
                        If UfisCom.CallServer("URT", "DRRTAB", strFields, strData, strWhere, "360") <> 0 Then
                            MsgBox frmMain.UfisCom.LastErrorMessage, vbCritical
                            Exit Sub
                        End If
                        MsgBox "You may be now release this staff from Level 2 to 3 using Rostering module."
                        cmdSearch_Click
                    End If
                Else
                    MsgBox "The level 2 status is - A, no update require."
                    Exit Sub
                End If
            End If
        Next i
    Else
        MsgBox "No record to update. Please search for employee."
    End If
End Sub

Private Sub Form_Load()
    InitTabs
End Sub
Sub InitTabs()
    Dim cnt As Long
    Dim i As Long
    'Initialise STFTAB
    frmData.InitTabGeneral frmData.tab_STFTAB
    frmData.InitTabForCedaConnection frmData.tab_STFTAB
    'frmData.LoadData frmData.tab_STFTAB, "DODM = ' '"
    frmData.LoadData frmData.tab_STFTAB, "WHERE DODM = ' '"
    DTPicker1.Value = Now
    txtServer.Text = frmData.strServer
    txtHopo.Text = frmData.strHOPO
End Sub

'Public Sub RefreshView()
'    'Initialise MAPTAB
'    frmData.InitTabGeneral tab_MAPTAB
'    frmData.InitTabForCedaConnection tab_MAPTAB
'    frmData.LoadData tab_MAPTAB, "order by URNO"
'    tab_MAPTAB.AutoSizeColumns
'
'    tab_MAPTAB.HeaderLengthString = "60,60,126,126,105,120,84,84"
'    tab_MAPTAB.HeaderString = "LKEY,TYPE,RVAL(UFIS Ref),LVAL(Ext Info),URNO,LSTU,USEC,USEU"
'    tab_MAPTAB.HeaderFontSize = 17
'    gMAPLKEY = ""
'    gMAPTYPE = ""
'    gMAPRVAL = ""
'    gMAPLVAL = ""
'    gMAPURNO = ""
'
'    cmdCancel.Refresh
'    cmdSave.Refresh
'    cmdDelete.Refresh
'    cmdNew.Refresh
'
'    StatusBar1.Panels(1).Text = "Ready."
'
'    gSaved = True
'    gNewRecord = True
'End Sub

Private Sub Form_Unload(Cancel As Integer)
    cmdExit_Click
End Sub

