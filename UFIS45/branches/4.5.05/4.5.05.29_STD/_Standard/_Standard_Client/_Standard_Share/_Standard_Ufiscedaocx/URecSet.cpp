// URecSet.cpp : implementation file
//

#include <stdafx.h>
#include <UfisCom.h>
#include <URecSet.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// URecSet

IMPLEMENT_DYNAMIC(URecSet, CRecordset)

URecSet::URecSet(CDatabase* pdb)
	: CRecordset(pdb)
{
	//{{AFX_FIELD_INIT(URecSet)
	m_ADDI = _T("");
	m_CONT = _T("");
	m_DATR = _T("");
	m_DEFA = _T("");
	m_DEPE = _T("");
	m_FELE = _T("");
	m_FINA = _T("");
	m_FITY = _T("");
	m_FMTS = _T("");
	m_GCFL = _T("");
	m_GDSR = _T("");
	m_GLFL = _T("");
	m_GSTY = _T("");
	m_GTYP = _T("");
	m_GUID = _T("");
	m_HOPO = _T("");
	m_INDX = _T("");
	m_KONT = _T("");
	m_LABL = _T("");
	m_LOGD = _T("");
	m_MSGT = _T("");
	m_ORIE = _T("");
	m_PROJ = _T("");
	m_REFE = _T("");
	m_REQF = _T("");
	m_SORT = _T("");
	m_STAT = _T("");
	m_SYST = _T("");
	m_TANA = _T("");
	m_TATY = _T("");
	m_TYPE = _T("");
	m_URNO = _T("");
	m_CLSR = _T("");
	m_DEFV = _T("");
	m_DSCR = _T("");
	m_FTAN = _T("");
	m_HEDR = _T("");
	m_TOLT = _T("");
	m_TTYP = _T("");
	m_TZON = _T("");
	m_nFields = 40;
	//}}AFX_FIELD_INIT
	m_nDefaultType = dynaset;
}


CString URecSet::GetDefaultConnect()
{
	return _T("ODBC;DSN=Peking-ODBC");
}

CString URecSet::GetDefaultSQL()
{
	return _T("[CEDA].[SYSTAB]");
}

void URecSet::DoFieldExchange(CFieldExchange* pFX)
{
	//{{AFX_FIELD_MAP(URecSet)
	pFX->SetFieldType(CFieldExchange::outputColumn);
	RFX_Text(pFX, _T("[ADDI]"), m_ADDI);
	RFX_Text(pFX, _T("[CONT]"), m_CONT);
	RFX_Text(pFX, _T("[DATR]"), m_DATR);
	RFX_Text(pFX, _T("[DEFA]"), m_DEFA);
	RFX_Text(pFX, _T("[DEPE]"), m_DEPE);
	RFX_Text(pFX, _T("[FELE]"), m_FELE);
	RFX_Text(pFX, _T("[FINA]"), m_FINA);
	RFX_Text(pFX, _T("[FITY]"), m_FITY);
	RFX_Text(pFX, _T("[FMTS]"), m_FMTS);
	RFX_Text(pFX, _T("[GCFL]"), m_GCFL);
	RFX_Text(pFX, _T("[GDSR]"), m_GDSR);
	RFX_Text(pFX, _T("[GLFL]"), m_GLFL);
	RFX_Text(pFX, _T("[GSTY]"), m_GSTY);
	RFX_Text(pFX, _T("[GTYP]"), m_GTYP);
	RFX_Text(pFX, _T("[GUID]"), m_GUID);
	RFX_Text(pFX, _T("[HOPO]"), m_HOPO);
	RFX_Text(pFX, _T("[INDX]"), m_INDX);
	RFX_Text(pFX, _T("[KONT]"), m_KONT);
	RFX_Text(pFX, _T("[LABL]"), m_LABL);
	RFX_Text(pFX, _T("[LOGD]"), m_LOGD);
	RFX_Text(pFX, _T("[MSGT]"), m_MSGT);
	RFX_Text(pFX, _T("[ORIE]"), m_ORIE);
	RFX_Text(pFX, _T("[PROJ]"), m_PROJ);
	RFX_Text(pFX, _T("[REFE]"), m_REFE);
	RFX_Text(pFX, _T("[REQF]"), m_REQF);
	RFX_Text(pFX, _T("[SORT]"), m_SORT);
	RFX_Text(pFX, _T("[STAT]"), m_STAT);
	RFX_Text(pFX, _T("[SYST]"), m_SYST);
	RFX_Text(pFX, _T("[TANA]"), m_TANA);
	RFX_Text(pFX, _T("[TATY]"), m_TATY);
	RFX_Text(pFX, _T("[TYPE]"), m_TYPE);
	RFX_Text(pFX, _T("[URNO]"), m_URNO);
	RFX_Text(pFX, _T("[CLSR]"), m_CLSR);
	RFX_Text(pFX, _T("[DEFV]"), m_DEFV);
	RFX_Text(pFX, _T("[DSCR]"), m_DSCR);
	RFX_Text(pFX, _T("[FTAN]"), m_FTAN);
	RFX_Text(pFX, _T("[HEDR]"), m_HEDR);
	RFX_Text(pFX, _T("[TOLT]"), m_TOLT);
	RFX_Text(pFX, _T("[TTYP]"), m_TTYP);
	RFX_Text(pFX, _T("[TZON]"), m_TZON);
	//}}AFX_FIELD_MAP
}

/////////////////////////////////////////////////////////////////////////////
// URecSet diagnostics

#ifdef _DEBUG
void URecSet::AssertValid() const
{
	CRecordset::AssertValid();
}

void URecSet::Dump(CDumpContext& dc) const
{
	CRecordset::Dump(dc);
}
#endif //_DEBUG
