// UfisComPpg.cpp : Implementation of the CUfisComPropPage property page class.

#include <stdafx.h>
#include <UfisCom.h>
#include <UfisComPpg.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


IMPLEMENT_DYNCREATE(CUfisComPropPage, COlePropertyPage)


/////////////////////////////////////////////////////////////////////////////
// Message map

BEGIN_MESSAGE_MAP(CUfisComPropPage, COlePropertyPage)
	//{{AFX_MSG_MAP(CUfisComPropPage)
	// NOTE - ClassWizard will add and remove message map entries
	//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// Initialize class factory and guid

IMPLEMENT_OLECREATE_EX(CUfisComPropPage, "UFISCOM.UfisComPropPage.1",
	0xa2f31e96, 0xc74f, 0x11d3, 0xa2, 0x51, 0, 0x50, 0x4, 0x37, 0xf6, 0x7)


/////////////////////////////////////////////////////////////////////////////
// CUfisComPropPage::CUfisComPropPageFactory::UpdateRegistry -
// Adds or removes system registry entries for CUfisComPropPage

BOOL CUfisComPropPage::CUfisComPropPageFactory::UpdateRegistry(BOOL bRegister)
{
	if (bRegister)
		return AfxOleRegisterPropertyPageClass(AfxGetInstanceHandle(),
			m_clsid, IDS_UFISCOM_PPG);
	else
		return AfxOleUnregisterClass(m_clsid, NULL);
}


/////////////////////////////////////////////////////////////////////////////
// CUfisComPropPage::CUfisComPropPage - Constructor

CUfisComPropPage::CUfisComPropPage() :
	COlePropertyPage(IDD, IDS_UFISCOM_PPG_CAPTION)
{
	//{{AFX_DATA_INIT(CUfisComPropPage)
	// NOTE: ClassWizard will add member initialization here
	//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_DATA_INIT
}


/////////////////////////////////////////////////////////////////////////////
// CUfisComPropPage::DoDataExchange - Moves data between page and properties

void CUfisComPropPage::DoDataExchange(CDataExchange* pDX)
{
	//{{AFX_DATA_MAP(CUfisComPropPage)
	// NOTE: ClassWizard will add DDP, DDX, and DDV calls here
	//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_DATA_MAP
	DDP_PostProcessing(pDX);
}


/////////////////////////////////////////////////////////////////////////////
// CUfisComPropPage message handlers
