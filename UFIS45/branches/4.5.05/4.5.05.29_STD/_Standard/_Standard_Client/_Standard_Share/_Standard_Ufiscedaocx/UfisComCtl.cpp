// UfisComCtl.cpp : Implementation of the CUfisComCtrl ActiveX Control class.

#include <stdafx.h>
#include <UfisCom.h>
#include <UfisComCtl.h>
#include <UfisComPpg.h>
#include <URecSet.h>
#include <_Recordset.h>
#include <fields.h>
#include <comdef.h>
#include <atlbase.h>
#include <resource.h>
#include <versioninfo.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

void CCSMemCpy  (char  *Pdest, char  *Psrc, DWORD cnt);
IMPLEMENT_DYNCREATE(CUfisComCtrl, COleControl)

int ExtractItemList(CString opSubString, CStringArray *popStrArray, char cpTrenner)
{

	CString olText;

	popStrArray->RemoveAll();

	BOOL blEnd = FALSE;
	
	if(!opSubString.IsEmpty())
	{
		int pos;
		int olPos = 0;
		while(blEnd == FALSE)
		{
			pos = opSubString.Find(cpTrenner);
			if(pos == -1)
			{
				blEnd = TRUE;
				olText = opSubString;
			}
			else
			{
				olText = opSubString.Mid(0, opSubString.Find(cpTrenner));
				opSubString = opSubString.Mid(opSubString.Find(cpTrenner)+1, opSubString.GetLength( )-opSubString.Find(cpTrenner)+1);
			}
			popStrArray->Add(olText);
		}
	}
	return	popStrArray->GetSize();
}


/////////////////////////////////////////////////////////////////////////////
// Message map

BEGIN_MESSAGE_MAP(CUfisComCtrl, COleControl)
	//{{AFX_MSG_MAP(CUfisComCtrl)
	ON_WM_SIZE()
	ON_WM_MOVE()
	//}}AFX_MSG_MAP
	ON_OLEVERB(AFX_IDS_VERB_EDIT, OnEdit)
	ON_OLEVERB(AFX_IDS_VERB_PROPERTIES, OnProperties)
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// Dispatch map

BEGIN_DISPATCH_MAP(CUfisComCtrl, COleControl)
	//{{AFX_DISPATCH_MAP(CUfisComCtrl)
	DISP_PROPERTY_NOTIFY(CUfisComCtrl, "ServerName", omServerName, OnServerNameChanged, VT_BSTR)
	DISP_PROPERTY_NOTIFY(CUfisComCtrl, "AccessMethod", omAccessMethod, OnAccessMethodChanged, VT_BSTR)
	DISP_PROPERTY_NOTIFY(CUfisComCtrl, "WorkStation", omWorkStation, OnWorkStationChanged, VT_BSTR)
	DISP_PROPERTY_NOTIFY(CUfisComCtrl, "UserName", omUserName, OnUserNameChanged, VT_BSTR)
	DISP_PROPERTY_NOTIFY(CUfisComCtrl, "TableExt", omTableExt, OnTableExtChanged, VT_BSTR)
	DISP_PROPERTY_NOTIFY(CUfisComCtrl, "HomeAirport", omHomeAirport, OnHomeAirportChanged, VT_BSTR)
	DISP_PROPERTY_NOTIFY(CUfisComCtrl, "Reqid", m_reqid, OnReqidChanged, VT_BSTR)
	DISP_PROPERTY_NOTIFY(CUfisComCtrl, "Dest1", m_dest1, OnDest1Changed, VT_BSTR)
	DISP_PROPERTY_NOTIFY(CUfisComCtrl, "Dest2", m_dest2, OnDest2Changed, VT_BSTR)
	DISP_PROPERTY_NOTIFY(CUfisComCtrl, "Cmd", m_cmd, OnCmdChanged, VT_BSTR)
	DISP_PROPERTY_NOTIFY(CUfisComCtrl, "Object", m_object, OnObjectChanged, VT_BSTR)
	DISP_PROPERTY_NOTIFY(CUfisComCtrl, "Seq", m_seq, OnSeqChanged, VT_BSTR)
	DISP_PROPERTY_NOTIFY(CUfisComCtrl, "Tws", m_tws, OnTwsChanged, VT_BSTR)
	DISP_PROPERTY_NOTIFY(CUfisComCtrl, "Twe", m_twe, OnTweChanged, VT_BSTR)
	DISP_PROPERTY_NOTIFY(CUfisComCtrl, "OrigName", m_origName, OnOrigNameChanged, VT_BSTR)
	DISP_PROPERTY_NOTIFY(CUfisComCtrl, "Selection", m_selection, OnSelectionChanged, VT_BSTR)
	DISP_PROPERTY_NOTIFY(CUfisComCtrl, "Fields", m_fields, OnFieldsChanged, VT_BSTR)
	DISP_PROPERTY_NOTIFY(CUfisComCtrl, "Version", m_version, OnVersionChanged, VT_BSTR)
	DISP_PROPERTY_NOTIFY(CUfisComCtrl, "Module", m_module, OnModuleChanged, VT_BSTR)
	DISP_PROPERTY_NOTIFY(CUfisComCtrl, "LastErrorMessage", m_lastErrorMessage, OnLastErrorMessageChanged, VT_BSTR)
	DISP_PROPERTY_NOTIFY(CUfisComCtrl, "BuildDate", m_buildDate, OnBuildDateChanged, VT_BSTR)
	DISP_PROPERTY_EX(CUfisComCtrl, "XUser", GetXUser, SetXUser, VT_BSTR)
	DISP_PROPERTY_EX(CUfisComCtrl, "StayConnected", GetStayConnected, SetStayConnected, VT_BOOL)
	DISP_FUNCTION(CUfisComCtrl, "InitCom", InitCom, VT_BOOL, VTS_BSTR VTS_BSTR)
	DISP_FUNCTION(CUfisComCtrl, "CleanupCom", CleanupCom, VT_BOOL, VTS_NONE)
	DISP_FUNCTION(CUfisComCtrl, "GetWorkstationName", GetWorkstationName, VT_BSTR, VTS_NONE)
	DISP_FUNCTION(CUfisComCtrl, "SetCedaPerameters", SetCedaPerameters, VT_BOOL, VTS_BSTR VTS_BSTR VTS_BSTR)
	DISP_FUNCTION(CUfisComCtrl, "Ufis", Ufis, VT_BSTR, VTS_BSTR VTS_BSTR VTS_BSTR VTS_BSTR VTS_BSTR)
	DISP_FUNCTION(CUfisComCtrl, "GetBufferCount", GetBufferCount, VT_I4, VTS_NONE)
	DISP_FUNCTION(CUfisComCtrl, "GetBufferLine", GetBufferLine, VT_BSTR, VTS_I4)
	DISP_FUNCTION(CUfisComCtrl, "GetDataBuffer", GetDataBuffer, VT_BSTR, VTS_BOOL)
	DISP_FUNCTION(CUfisComCtrl, "ClearDataBuffer", ClearDataBuffer, VT_EMPTY, VTS_NONE)
	DISP_FUNCTION(CUfisComCtrl, "CallServer", CallServer, VT_I2, VTS_BSTR VTS_BSTR VTS_BSTR VTS_BSTR VTS_BSTR VTS_BSTR)
	//}}AFX_DISPATCH_MAP
	DISP_FUNCTION_ID(CUfisComCtrl, "AboutBox", DISPID_ABOUTBOX, AboutBox, VT_EMPTY, VTS_NONE)
END_DISPATCH_MAP()


/////////////////////////////////////////////////////////////////////////////
// Event map

BEGIN_EVENT_MAP(CUfisComCtrl, COleControl)
	//{{AFX_EVENT_MAP(CUfisComCtrl)
	// NOTE - ClassWizard will add and remove event map entries
	//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_EVENT_MAP
END_EVENT_MAP()


/////////////////////////////////////////////////////////////////////////////
// Property pages

// TODO: Add more property pages as needed.  Remember to increase the count!
BEGIN_PROPPAGEIDS(CUfisComCtrl, 1)
	PROPPAGEID(CUfisComPropPage::guid)
END_PROPPAGEIDS(CUfisComCtrl)


/////////////////////////////////////////////////////////////////////////////
// Initialize class factory and guid

IMPLEMENT_OLECREATE_EX(CUfisComCtrl, "UFISCOM.UfisComCtrl.1",
	0xa2f31e95, 0xc74f, 0x11d3, 0xa2, 0x51, 0, 0x50, 0x4, 0x37, 0xf6, 0x7)


/////////////////////////////////////////////////////////////////////////////
// Type library ID and version

IMPLEMENT_OLETYPELIB(CUfisComCtrl, _tlid, _wVerMajor, _wVerMinor)


/////////////////////////////////////////////////////////////////////////////
// Interface IDs

const IID BASED_CODE IID_DUfisCom =
		{ 0xa2f31e93, 0xc74f, 0x11d3, { 0xa2, 0x51, 0, 0x50, 0x4, 0x37, 0xf6, 0x7 } };
const IID BASED_CODE IID_DUfisComEvents =
		{ 0xa2f31e94, 0xc74f, 0x11d3, { 0xa2, 0x51, 0, 0x50, 0x4, 0x37, 0xf6, 0x7 } };


/////////////////////////////////////////////////////////////////////////////
// Control type information

static const DWORD BASED_CODE _dwUfisComOleMisc =
	OLEMISC_INVISIBLEATRUNTIME |
	OLEMISC_ACTIVATEWHENVISIBLE |
	OLEMISC_SETCLIENTSITEFIRST |
	OLEMISC_INSIDEOUT |
	OLEMISC_CANTLINKINSIDE |
	OLEMISC_RECOMPOSEONRESIZE;

IMPLEMENT_OLECTLTYPE(CUfisComCtrl, IDS_UFISCOM, _dwUfisComOleMisc)


/////////////////////////////////////////////////////////////////////////////
// CUfisComCtrl::CUfisComCtrlFactory::UpdateRegistry -
// Adds or removes system registry entries for CUfisComCtrl

BOOL CUfisComCtrl::CUfisComCtrlFactory::UpdateRegistry(BOOL bRegister)
{
	// TODO: Verify that your control follows apartment-model threading rules.
	// Refer to MFC TechNote 64 for more information.
	// If your control does not conform to the apartment-model rules, then
	// you must modify the code below, changing the 6th parameter from
	// afxRegInsertable | afxRegApartmentThreading to afxRegInsertable.

	if (bRegister)
		return AfxOleRegisterControlClass(
			AfxGetInstanceHandle(),
			m_clsid,
			m_lpszProgID,
			IDS_UFISCOM,
			IDB_UFISCOM,
			afxRegInsertable | afxRegApartmentThreading,
			_dwUfisComOleMisc,
			_tlid,
			_wVerMajor,
			_wVerMinor);
	else
		return AfxOleUnregisterClass(m_clsid, m_lpszProgID);
}


/////////////////////////////////////////////////////////////////////////////
// CUfisComCtrl::CUfisComCtrl - Constructor

CUfisComCtrl::CUfisComCtrl()
{
	InitializeIIDs (&IID_DUfisCom, &IID_DUfisComEvents);

	GetVersionInfo();

	pomCedaSock	= NULL;
	pomAdoCtrl  = NULL;
	isOdbcInit  = false;
	bmIsFromCallServer	= false;
	bmUseIpAddress		= false;
	bmStayConnected		= false;
	bmIsConnected		= false;

	char pclTmpText[512];
	char pclConfigPath[512];
    if (getenv("CEDA") == NULL)
        strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
    else
        strcpy(pclConfigPath, getenv("CEDA"));


    GetPrivateProfileString("GLOBAL", "USEWORKSTATIONNAME","YES", pclTmpText,sizeof pclTmpText, pclConfigPath);
	if (stricmp(pclTmpText,"NO") == 0)
	{
		bmUseIpAddress = true;
	}
	
}


/////////////////////////////////////////////////////////////////////////////
// CUfisComCtrl::~CUfisComCtrl - Destructor

CUfisComCtrl::~CUfisComCtrl()
{
	if(pomCedaSock != NULL)
	{
		delete pomCedaSock;
	}

	if(pomAdoCtrl != NULL)
	{
		delete pomAdoCtrl;
		isOdbcInit = false;
	}

	if(isOdbcInit == true)
	{
		::SQLDisconnect(hDbConn);
		::SQLFreeConnect(hDbConn);
		::SQLFreeEnv(hEnv);
		isOdbcInit = false;
	}
}


/////////////////////////////////////////////////////////////////////////////
// CUfisComCtrl::OnDraw - Drawing function

void CUfisComCtrl::OnDraw(CDC* pdc, const CRect& rcBounds, const CRect& rcInvalid)
{
	CBrush olBlackBrush(COLORREF(RGB(0,0,0)));
	int ilH = rcBounds.bottom - rcBounds.top;
	CSize ilTH = pdc->GetTextExtent("Ceda");
	ilH = (int)(ilH/2);
	ilH -= (int)(ilTH.cy/2);
	pdc->FillRect(rcBounds, CBrush::FromHandle((HBRUSH)GetStockObject(LTGRAY_BRUSH)));
	pdc->FrameRect(rcBounds, &olBlackBrush);
	pdc->SetTextColor(COLORREF(RGB(255,128,0)));
	pdc->SetBkMode(TRANSPARENT);
	pdc->TextOut(0, ilH, "Ceda");
}


/////////////////////////////////////////////////////////////////////////////
// CUfisComCtrl::DoPropExchange - Persistence support

void CUfisComCtrl::DoPropExchange(CPropExchange* pPX)
{
	ExchangeVersion(pPX, MAKELONG(_wVerMinor, _wVerMajor));
	COleControl::DoPropExchange(pPX);

	// TODO: Call PX_ functions for each persistent custom property.

}


/////////////////////////////////////////////////////////////////////////////
// CUfisComCtrl::OnResetState - Reset control to default state

void CUfisComCtrl::OnResetState()
{
	COleControl::OnResetState();  // Resets defaults found in DoPropExchange

	// TODO: Reset any other control state here.
}


/////////////////////////////////////////////////////////////////////////////
// CUfisComCtrl::AboutBox - Display an "About" box to the user

void CUfisComCtrl::AboutBox()
{
	CDialog dlgAbout(IDD_ABOUTBOX_UFISCOM);
	dlgAbout.DoModal();
}


/////////////////////////////////////////////////////////////////////////////
// CUfisComCtrl message handlers

BOOL CUfisComCtrl::InitCom(LPCTSTR ServerName, LPCTSTR AccessMethod) 
{
	if (this->bmIsConnected)
	{
		return FALSE;
	}

	omAccessMethod = AccessMethod;
	omServerName = ServerName;
	char *pclTmpPtr;
	char OrigServer[512]="",
		 Svr1[512]="",
		 Svr2[512]="";
	bool blHas2Servers = false;

	strcpy(OrigServer, ServerName);
	pclTmpPtr = strchr(OrigServer, '/');
	if(pclTmpPtr != NULL)
	{
		*pclTmpPtr = '\0';
		strcpy(Svr1, OrigServer);
		pclTmpPtr++;
		strcpy(Svr2, pclTmpPtr);
		blHas2Servers = true;
	}
	else
	{
		strcpy(Svr1, ServerName);
	}

	if(omAccessMethod == "")
	{
		omAccessMethod = CString("CEDA");
	}

	pomCedaSock = new UfisSocket();

	if (!pomCedaSock->Create())
	{
		int ilErr = pomCedaSock->GetLastError();
		CString olTxt;
		olTxt.Format("Creation failed: Error Number ==> %d",ilErr);
		delete pomCedaSock;
		pomCedaSock = NULL;
		AfxMessageBox(olTxt);
		return FALSE;
	}

	if(blHas2Servers == true)
	{
		if (!pomCedaSock->Connect(Svr1/*omServerName*/, 3350))
		{
			if (!pomCedaSock->Connect(Svr2/*omServerName*/, 3350))
			{
				//if (AfxMessageBox("Retry Connect??",MB_YESNO) == IDNO)
				//{
				//  No Server Found
					delete pomCedaSock;
					pomCedaSock = NULL;
					return FALSE;
				//}
			}
		}
	}
	else
	{
		if (!pomCedaSock->Connect(Svr1/*omServerName*/, 3350))
		{
		//  No Server Found
			delete pomCedaSock;
			pomCedaSock = NULL;
			return FALSE;
		}
	}

	this->omWorkStation = this->GetWorkstationName();

	if (this->bmIsFromCallServer == false && this->bmStayConnected == false)
	{
		CleanupCom();
	}
	else
	{
		this->bmIsConnected	  = true;
	}

	return TRUE;
}




BOOL CUfisComCtrl::CleanupCom() 
{
	if (pomCedaSock != NULL)
	{
		BYTE Buffer[50];
		pomCedaSock->ShutDown();

		while(pomCedaSock->Receive(Buffer,50) > 0);
	}

	
	delete pomCedaSock;
	pomCedaSock = NULL;
	bmIsConnected = false;

	return TRUE;
}

void CUfisComCtrl::OnServerNameChanged() 
{
	// TODO: Add notification handler code

	SetModifiedFlag();
}

void CUfisComCtrl::OnAccessMethodChanged() 
{
	// TODO: Add notification handler code

	SetModifiedFlag();
}

void CUfisComCtrl::OnSize(UINT nType, int cx, int cy) 
{
	COleControl::OnSize(nType, cx, cy);
}

void CUfisComCtrl::OnMove(int x, int y) 
{
	COleControl::OnMove(x, y);
}

void CUfisComCtrl::OnWorkStationChanged() 
{
	// TODO: Add notification handler code

	SetModifiedFlag();
}

BSTR CUfisComCtrl::GetWorkstationName() 
{
	CString strResult;
	
	char  pclWksName[133];
	if (gethostname( pclWksName,130) == 0) 
	{
		if (this->bmUseIpAddress)
		{
			struct hostent *polHostEnt = gethostbyname(pclWksName);
			if (polHostEnt != NULL)
			{
				IN_ADDR olIpAddress = (*(IN_ADDR *)polHostEnt->h_addr_list[0]);
				strResult.Format("%X",olIpAddress.S_un.S_addr);
			}
			else
			{
				strResult = pclWksName;
			}
		}
		else
		{
			strResult = pclWksName;
		}
	}
	else
	{
		strResult = "";
	}

	return strResult.AllocSysString();

}


void CUfisComCtrl::OnUserNameChanged() 
{
	// TODO: Add notification handler code

	SetModifiedFlag();
}

void CUfisComCtrl::OnTableExtChanged() 
{
	// TODO: Add notification handler code

	SetModifiedFlag();
}

void CUfisComCtrl::OnHomeAirportChanged() 
{
	// TODO: Add notification handler code

	SetModifiedFlag();
}

BOOL CUfisComCtrl::SetCedaPerameters(LPCTSTR UserName, LPCTSTR HomeAirport, LPCTSTR TableExt) 
{
	omUserName = UserName;
	omXUser = omUserName;
	omHomeAirport = HomeAirport;
	omTableExt = TableExt;
	return TRUE;
}

BSTR CUfisComCtrl::GetXUser() 
{
	CString strResult;
		strResult = omXUser;
	return strResult.AllocSysString();
}

void CUfisComCtrl::SetXUser(LPCTSTR lpszNewValue) 
{
	omXUser = lpszNewValue;

	SetModifiedFlag();
}

static int SendBuffer (SOCKET socket, BC_HEAD  *Pbc_head, long buflen)  
{             
	int rc = RC_SUCCESS;
	long	remain;
	int	send_len;
	COMMIF	 *ldata;
	COMMIF	ack;
	char   * offs;
	int serr = 0;

	ldata 	= (COMMIF  *) malloc((long) (DATAPACKETLEN + sizeof(COMMIF)));
	offs	= (char  *) Pbc_head;
	remain	= buflen;                             
	
	do {
		if ( remain > DATAPACKETLEN ) {
			ldata->command = htons((u_short)PACKET_DATA);
			ldata->length = htonl((long) DATAPACKETLEN);
			memcpy((char *) &(ldata->data),offs,DATAPACKETLEN);
			remain -= DATAPACKETLEN;
			offs+=DATAPACKETLEN;
		} /* end if */
		else {
			ldata->length = htonl(remain);
			ldata->command = htons((u_short)PACKET_END);
			memcpy((char *) &(ldata->data),offs,(int)remain);
			remain = 0;
		} /* end else */                   
		send_len = (int) (htonl(ldata->length) + sizeof(COMMIF));
//		rc = send (socket, (char *)ldata, PACKET_LEN, 0);
		rc = send (socket, (char *)ldata, send_len, 0);
		if ( rc == SOCKET_ERROR ) {
			serr = WSAGetLastError();
			TRACE("send failed ERROR %d",serr);
			rc = RC_COMM_FAIL;
		} /* end if */
		else {
			do {
				rc = recv(socket,(char *) &ack,sizeof(COMMIF), 0);
				if ( rc == SOCKET_ERROR) {
					serr = WSAGetLastError();
					rc = RC_COMM_FAIL;
					TRACE("receive ACK failed");
					Sleep(1);
				} /* end if */
			} while ( rc <= 0 );
			//while ( rc == 0 && rc != RC_COMM_FAIL );
		} /* end else */
	} while (remain >0 && rc != RC_COMM_FAIL);
	
	free(ldata);
    
    if (rc != RC_COMM_FAIL)
    	rc = RC_SUCCESS;
 	return rc;
} /* end SendBuffer */
   

int CUfisComCtrl::CallCeda(char * req_id,		/*  Wird von Toolbook vergeben  */
						    char * dest1,     	/*  Im Regelfall CEDA			*/
						    char * dest2,     	/*  Ergebniss Empfaenger		*/
						    char * cmd,       	/*  Kommando (RT,....)         	*/
						    char * object,   	/*  Object fuer Kommando (bei RT: TableName)*/
						    char * seq,       	/*  Sortierkriterium			*/
						    char * tws,       	/*  Time Window start   		*/
						    char * twe,       	/*  Time Window end				*/
						    char * selection, 	/*  (Bsp: WHERE CARR = 'LH')	*/
						    char * fields,    	/*  (Bsp: AC2,AC3,CARR  )		*/
						    char * data,
							char * timeout)     /*  ( )                			*/
{
	int ilRet = 0;
	BC_HEAD  *polBc_Head = NULL;
	DWORD buflen = 0;
	int rc = 0; //RC_SUCCESS         
	CMDBLK  *polCmdBlk;
	char * pclActData = NULL;
 
	buflen = CalcBufLen (selection, fields, data);
	polBc_Head = (BC_HEAD *)malloc(buflen);
	if(polBc_Head == NULL)
		return -1;

	// copy all data to the structure
    polBc_Head->rc = 0; // = RC_SUCCESS
	strncpy ((char *) polBc_Head->dest_name, dest1, __min (strlen(dest1) +1, 10)); 
	polBc_Head->orig_name[0] = '\0';   	
	strncpy ((char *) polBc_Head->recv_name, dest2, __min (strlen(dest2) +1,10));    	

	//Berni: Hier wird der Timeout gesetzt!!!!
	strncpy ((char *) polBc_Head->orig_name, timeout, __min (strlen(timeout) +1,10));    	
    //End Berni

    polBc_Head->bc_num = 0;  /* Init */
	strncpy ((char *) polBc_Head->seq_id, req_id, __min (strlen(req_id) +1,10));    	
    polBc_Head->act_buf = 0; /* Init */
	polBc_Head->ref_seq_id[0] = '\0';   	
                  
	polCmdBlk = (CMDBLK  *) &(polBc_Head->data);
	
	strncpy ((char *) &(polCmdBlk->command), cmd, __min (strlen(cmd) +1, 6));
	strncpy ((char *) &(polCmdBlk->obj_name), object, __min (strlen(object) +1, 33));
	strncpy ((char *) &(polCmdBlk->order), seq, __min (strlen(seq) +1, 2));
	strncpy ((char *) &(polCmdBlk->tw_start), tws, __min (strlen(tws) +1, 33));
	strncpy ((char *) &(polCmdBlk->tw_end), twe, __min (strlen(twe) +1, 33)); 
	                                      
    polBc_Head->cmd_size = sizeof (polCmdBlk) - 1 + strlen (selection) +
    					strlen (fields) ;
	
    polBc_Head->data_size = strlen (data) ;

    polBc_Head->tot_size = sizeof (BC_HEAD) - 1 +
    					polBc_Head->cmd_size  + polBc_Head->data_size;

	pclActData = (char *) &(polBc_Head->data) + sizeof (CMDBLK) - 1;				

	strcpy (pclActData,	selection);
	pclActData += strlen (selection) + 1;
	
	strcpy (pclActData,	fields);
	pclActData +=	strlen (fields) + 1;

	strcpy (pclActData,	data);

 SendBuffer(pomCedaSock->m_hSocket,polBc_Head,buflen);

//-------------------------------------------------------------------
// Und schale drum rum, um den mist in die structur COOIF zu kriegen 
// und dann endlich zu senden
	COMMIF	 *polData;
//	COMMIF	olAck;
	char   * pclOffs;
	int ilSendLen = 0;

    int x = sizeof(COMMIF);
	polData 	= (COMMIF  *) malloc(buflen + sizeof(COMMIF));
	pclOffs	= (char  *) polBc_Head;
	polData->length = htonl(buflen);
	polData->command = htons((u_short)PACKET_END);
	memcpy((char *) &(polData->data),pclOffs,buflen);
	ilSendLen = (int) (htonl(polData->length) + sizeof(COMMIF));

//		rc = send (socket, (char *)polDdata, send_len, 0);


//	if(pomCedaSock->Send((char *)polData, ilSendLen) == SOCKET_ERROR )
//	{
//		int ilLastError = pomCedaSock->GetLastError();
//	}


	/************
	if(pomCedaSock->Receive((char *) &olAck,sizeof(COMMIF), 0) == SOCKET_ERROR)
	{
		AfxMessageBox("Receive ACK: Socket Error");
	}
	***********************/
	free (polData);	


//&Pbc_head, &buflen
	int ilTotalReturn;
	ilTotalReturn = ReceiveUfisData(&polBc_Head, &buflen);
	free(polBc_Head);
	return ilTotalReturn;

}


DWORD CUfisComCtrl::CalcBufLen (char * selection, char * fields, char * data)
{               
	DWORD cmd_size, data_size, tot_size;
	
    cmd_size	= sizeof (CMDBLK) - 1 + strlen (selection) + strlen (fields);

    data_size	= strlen (data) ;

    tot_size	= sizeof (BC_HEAD) - 1 + cmd_size + data_size + 4;
	
	return tot_size;
}

BSTR CUfisComCtrl::Ufis(LPCTSTR Command, LPCTSTR Object, LPCTSTR Fields, LPCTSTR Where, LPCTSTR Data) 
{
	CString strResult;
	CString olTwe;
	CString olCommand = Command;
	CString olAction;
	CString olObject = Object;
	CString olTimeout = Data;

	if(olCommand == "RT" || olCommand == "GFR")
		olAction = "Select";
	if(olCommand == "URT")
		olAction = "Update";
	if(olCommand == "IRT")
		olAction = "Insert";
	if(olCommand == "DRT")
		olAction = "Delete";

	CString olFields = Fields;
	omCurrFields.RemoveAll();
	ExtractItemList(Fields, &omCurrFields, ',');
	CString olWhere = Where;
	CString olData = Data;
	if(m_twe.IsEmpty())
	{
		m_twe = this->omHomeAirport + "," + this->omTableExt + "," + m_module;
	}
	if(olObject.GetLength() == 3)
	{
		if(omTableExt.GetLength() == 3)
		{
			olObject += omTableExt;
		}
		else
		{
			AfxMessageBox("Tablename invalid!!");
		}
	}


	if(omAccessMethod == "CEDA" )
	{
		CallCeda(omWorkStation.GetBuffer(0),omUserName.GetBuffer(0),omWorkStation.GetBuffer(0),     	
					olCommand.GetBuffer(0), olObject.GetBuffer(0), m_seq.GetBuffer(0), m_tws.GetBuffer(0), 
					m_twe.GetBuffer(0), olWhere.GetBuffer(0),        	
					olFields.GetBuffer(0),  olData.GetBuffer(0), olTimeout.GetBuffer(0));
	}
	else    //if(omAccessMethod == "ODBC")
	{
		CallRecordSet(olAction, olObject, olFields, olWhere, olData);
	}
	CString olTmp;
	olTmp.Format("%ld Records", omDataArray.GetSize());
	return strResult.AllocSysString();
}


// -----------------------------------------------------------------
//
// Function: snap
//
// Purpose : Hex Dump tool
//
// Params  : Buffer, no of bytes
//
// Returns : none
//
// Comments: 
//           
// -----------------------------------------------------------------
void snap (char * buf, int buflen)
{
 	int i; 
 	char * Pact=buf;
	int t_i=-1;
	char trace_buffer[4096];

 	*trace_buffer = '\0';
   	  for (i=0; i<buflen; i++)
 	  {
 		if (buflen - i >= 16)
 		{
 			sprintf (trace_buffer, 
 			"%02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x\n",
            (unsigned char) *(Pact+0),(unsigned char) *(Pact+1),
            (unsigned char) *(Pact+2),(unsigned char) *(Pact+3),
            (unsigned char) *(Pact+4),(unsigned char) *(Pact+5),
            (unsigned char) *(Pact+6),(unsigned char) *(Pact+7),
            (unsigned char) *(Pact+8),(unsigned char) *(Pact+9),
            (unsigned char) *(Pact+10),(unsigned char) *(Pact+11),
            (unsigned char) *(Pact+12),(unsigned char) *(Pact+13),
            (unsigned char) *(Pact+14),(unsigned char) *(Pact+15));
			TRACE("%s\n",trace_buffer);
			trace_buffer[0] = 0;
            Pact += 16;
            i += 15;
        }
        else
        {
 			sprintf (trace_buffer + strlen (trace_buffer), "%02x ", (unsigned char) *(Pact+0));
 			Pact++;         	
        } 
	}
  TRACE("%s\n",trace_buffer);
} /* end snap */


#define SELECTION 0
#define FIELDS    1
#define DATA      2

bool CUfisComCtrl::AddToList(CString& olList,char *pcpActData,int *pipLen)
{
	bool blFound = false;
	
	if(strchr(pcpActData,'\0') != NULL)
	{
		int ilLen = strlen(pcpActData);

		if (ilLen < *pipLen)
		{
			blFound = true;
			olList += pcpActData;
			*pipLen -= (strlen(pcpActData) + 1);
		}
	}

	if ( ! blFound)
	{
		char clTmp[2048];
		
		strncpy(clTmp,pcpActData,*pipLen);
		olList += clTmp;
	}

	return blFound;
}

int CUfisComCtrl::ReceiveUfisData(BC_HEAD  **Pbc_head, DWORD *buflen)
{
	int	rc			= RC_SUCCESS;	/* Return code				*/
    int count		= 0;
    int cur_len		= 0;
    int p_len		= 0;
	short slStatus  = SELECTION;
	bool blDelFound = false;

	BC_HEAD  *BcHead;

	CMDBLK	 *CmdBlk;

	COMMIF	 *packet;				/* transport data			*/
	COMMIF	 *pdata;				/* transport data			*/
	COMMIF	 ack;

	char *cur_pdata = NULL;			/* transport data pointer	*/
	char *pclSelKey = NULL;
	char *pclFldLst = NULL;
	char *pclDatLst = NULL;
//	char *cur_adr;
	char pclDataBuf[10000]="";

	DWORD packet_len= 0;
	DWORD tmp		= 0;

	short ready		= FALSE;
	short firstLine = TRUE;
	BOOL blFirstData = TRUE;

	m_lastErrorMessage = "";
	m_selection.Empty();
	m_fields.Empty();

	memset((void*)pclDataBuf, '\0', size_t(10000));
	omDataArray.RemoveAll();
	packet = (COMMIF *) malloc((size_t)(long)NETREAD_SIZE); //DoMalloc((long)NETREAD_SIZE, &packet_hndl);
	pdata  = (COMMIF *) malloc((size_t)(long)PACKET_LEN);   //DoMalloc((long)PACKET_LEN, &data_hndl);

	if (packet == NULL || pdata == NULL)
	{
		return RC_FAIL;
	}

	packet_len = NETREAD_SIZE;

	if (rc == RC_SUCCESS)
	{
//		cur_adr = (char *)(packet->data) ;
		packet->length = 0;
		ack.command    = (short)htons((u_short)PACKET_DATA);
		ack.length     = htonl(sizeof(COMMIF));

		while (!ready)
		{
			cur_pdata = (char *) pdata;
            cur_len	  = 0;
            p_len	  = PACKET_LEN;

			while ((rc == RC_SUCCESS) && (cur_len < PACKET_LEN) && (cur_len < p_len))
			{
				rc = pomCedaSock->Receive((char *) cur_pdata, PACKET_LEN - cur_len, 0);
//snap((char *)cur_pdata,rc);
				if (rc == SOCKET_ERROR )
				{
				//	AfxMessageBox( "Daten holen: Receive fehlgeschlagen");
					free(packet);
					ready = TRUE;
					rc = RC_FAIL;
					TRACE("receive Data failed");
				} 
				else
				{
					if (rc == 0)
					{
						// MCU 96.10.11 we should return RC_FAIL if the connection has been closed
						rc = RC_FAIL;
					}
					else
					{
						cur_len += rc;
						cur_pdata += rc;
						if (cur_len >= sizeof (COMMIF) )
						{                      /* How many bytes will come ? */
							p_len = (int) ntohl(pdata->length) + sizeof(COMMIF);
						} /* fi */
						rc = RC_SUCCESS;
					}
				} /* fi */
			}


			int ilDataLen;


			//Berni: Ab hier wird die Kiste zusammengeschn�rt

			char *pclActData;
			BOOL blReady = TRUE;


			if (rc == RC_SUCCESS)
			{
				pomCedaSock->Send((char *) &ack, sizeof(COMMIF), 0);//rc = send (socket, (char *) &ack, sizeof(COMMIF), 0);
				pdata->command = ntohs(pdata->command);
				pdata->length =  ntohl(pdata->length);
				if (firstLine == TRUE)
				{

	//				int ilGarbageLen = sizeof(BC_HEAD) -1 + sizeof(CMDBLK) - 1;
					int ilGarbageLen = 64 + 107; //sizeof(BC_HEAD) -2 + sizeof(CMDBLK);
					ilDataLen = pdata->length - ilGarbageLen;

					// Schweinetest
					//ilDataLen -= 6;

					char *pclTest = (char *)pdata + ilGarbageLen + 8;
					BcHead		= (BC_HEAD *) ((char *)pdata->data);
					if( BcHead->rc < 0 )
					{
						slStatus = 14; //to prevent that switch/case of status will be entered
						m_lastErrorMessage = CString(BcHead->data);
						rc = RC_FAIL;
					}
					else
					{
						CmdBlk		= (CMDBLK  *) BcHead->data;
						pclActData  = (char *) CmdBlk->data;
					   // ilDataLen   = pdata->length - ((char *)pclActData - (char *)pdata);
						pclSelKey	= (char *) CmdBlk->data;
						pclFldLst	= pclSelKey + strlen (pclSelKey) + 1;
						pclDatLst	= pclFldLst + strlen (pclFldLst) + 1;
					//	strcpy (pclDataBuf, pclDatLst);
						firstLine	= FALSE;
						m_reqid		= CString (BcHead->seq_id);
						m_dest1		= CString (BcHead->dest_name);
						m_dest2		= CString (BcHead->recv_name);
						m_cmd		= CString (CmdBlk->command);
						m_object	= CString (CmdBlk->obj_name);
						m_seq		= CString (BcHead->seq_id);
						m_tws		= CString (CmdBlk->tw_start);
						m_twe		= CString (CmdBlk->tw_end);
						m_origName	= CString (BcHead->orig_name);
					//	m_selection = CString (pclSelKey);
					//	m_fields	= CString (pclFldLst);
					}
				}
				else
				{
					pclActData = pdata->data;
					ilDataLen  = pdata->length;
				}
			do
			{
					switch(slStatus)
					{
					case SELECTION: blDelFound = AddToList(m_selection,pclActData,&ilDataLen);
						break;
					case FIELDS:  blDelFound = AddToList(m_fields,pclActData,&ilDataLen);
						break;
					case DATA: MakeRow (pclActData,pdata->command,ilDataLen,blFirstData);
						blFirstData = FALSE;
						break;

					};
					blReady = TRUE;

					if (slStatus != DATA)
					{
						if (blDelFound)
						{
							char *pclDelimiter = strchr(pclActData,'\0');
							if (pclDelimiter != NULL)
							{
								slStatus++;
						//		ilDataLen -= strlen(pclActData) - 1;
								pclActData = pclDelimiter + 1;
								blReady = FALSE;
								blDelFound = false;
							}
						}
					}
				} while (! blReady );
					/***
					
				else
				{
					strncat (pclDataBuf, pdata->data, pdata->length);
				}
			
				MakeRow (pclDataBuf,pdata->command,pdata->length);
				**************/
				if (pdata->command == PACKET_END) 
				{
					//omDataArray.Add (CString (pclDataBuf));
					ready = TRUE;
				}
				
			} /* fi */
		} /* end while */
		free(pdata);//MWO DoFree(data_hndl); 
	} /* end if hndl == NULL */
	free(packet);
	return rc;
}

void CCSMemCpy  (char  *Pdest, char  *Psrc, DWORD cnt)
{            
	DWORD i=0;
	              
//	sprintf(trace_buffer,"CCSMemCpy:cnt=%ld dest=%p src=%p",cnt, Pdest, Psrc);
//	WriteTrace(trace_buffer, WT_DEBUG);   

	if (Pdest > Psrc)
	{   
		Pdest += (cnt - 1);
		Psrc  += (cnt - 1);
		for (i=0; i < cnt; i++, Pdest--, Psrc--)
			*Pdest = *Psrc;
	}
	else
	{
		for (i=0; i < cnt; i++, Pdest++, Psrc++)
			*Pdest = *Psrc;
	}
} /* CCSMemCpy */

bool CUfisComCtrl::MakeRow(char *pclDataBuf,short spCommand, int ipLength,BOOL bpFirstLine)
{
	CString olDat;
	CString olNewStr;
	CString olRest;
	static  CString olStaticRest;

	
	// Berni: Hier werden die Datens�tze auseinandergenommen
	// Diese Stelle w�rde mit strstr-Funktionalit�t sicher
	// um etliches schneller funktionieren
	// Du must lediglich daf�r sorgen, da� omDataArray gef�llt wird
	// darin werden die Datens�tze tempor�r gehalten ( das ist also der Buffer)

	// Wie kann man den mit char* f�llen?
	// char *pcpX;
	// omDataArray.Add(CString(pcpX));

	if (bpFirstLine)
	{
		olStaticRest.Empty();
	}
	if (!olStaticRest.IsEmpty())
	{
		int ilolDatLen;
		CString TmpBuf;
		TmpBuf = pclDataBuf;
		TmpBuf = TmpBuf.Left(ipLength);
		olDat = olStaticRest + TmpBuf;//pclDataBuf;
		ilolDatLen = olDat.GetLength();
		ipLength += olStaticRest.GetLength();
	}
	else
	{
		olDat = pclDataBuf;
		olDat = olDat.Left(ipLength);
	}

	int ilIdx = olDat.Find("\n");
	int ilCrIdx = olDat.Find("\r");
	if (ilIdx == -1)
	{
		olRest = olDat.Left(ipLength);
	}
	while (ilIdx != -1)
	{
		if(ilIdx != 0)
		{
			if( ilCrIdx != -1)
			{
				olNewStr = olDat.Left  (ilIdx-1);
				olRest	 = olDat.Right (olDat.GetLength() - ilIdx - 1);
				ipLength -= (ilIdx + 1);   /// + 1 wg CR
			}
			else
			{
				olNewStr = olDat.Left  (ilIdx);
				olRest	 = olDat.Right (olDat.GetLength() - ilIdx - 1);
				ipLength -= (ilIdx );   
			}
		//	ipLength = olRest.GetLength();
			omDataArray.Add (olNewStr);
		//	strcpy (pclDataBuf, olRest);
			int ilTest = olNewStr.GetLength();
			olDat = olRest;
			ilIdx = olDat.Find ("\n");
			ilCrIdx = olDat.Find("\r");
		}
		else
		{
			if( ilCrIdx != -1)
			{
				olNewStr = olDat.Left  (ilIdx-1);
				olRest	 = olDat.Right (olDat.GetLength() - ilIdx - 1);
				ipLength -= (ilIdx + 1);   /// + 1 wg CR
			}
			else
			{
				olNewStr = olDat.Left  (ilIdx);
				olRest	 = olDat.Right (olDat.GetLength() - ilIdx - 1);
				ipLength -= (ilIdx );   
			}
		//	ipLength = olRest.GetLength();
		//	omDataArray.Add (olNewStr);
		//	strcpy (pclDataBuf, olRest);
			int ilTest = olNewStr.GetLength();
			olDat = olRest;
			ilIdx = olDat.Find ("\n");
			ilCrIdx = olDat.Find("\r");
		}
	}
	if (spCommand == PACKET_DATA)
	{
		olStaticRest = olRest.Left(ipLength);
	} 
	else if (spCommand == PACKET_END)
	{
		if (ipLength > 1)
		{
			omDataArray.Add (olDat.Left(ipLength));
			}
	}
	return true;	
}

long CUfisComCtrl::GetBufferCount() 
{
	return omDataArray.GetSize();
}

BSTR CUfisComCtrl::GetBufferLine(long BufferIndex) 
{
	CString strResult;

	int ilCount = omDataArray.GetSize();
	if(BufferIndex >= ilCount)
	{
		strResult = CString(" ");
	}
	else
	{
		strResult = omDataArray[BufferIndex];
	}
	return strResult.AllocSysString();
}

int CUfisComCtrl::CallRecordSet(CString opAction, CString opObject, CString opFields, CString opWhere, CString opData)
{
	CDatabase olDB;
	int sqlRet = 0;
//	HENV	hEnv;
//	HDBC	hDbConn;
//	HSTMT	hStmt;
	char	pclValue[2000]="";
//	SDWORD	OutputDataLen;
//	short  nCols;
	unsigned char pclConnStrOut[256];
	int ilFieldCount=0;
	char  sDataArray[100][255];
	char pclDataBuffer[25600];

	SDWORD dwDataLen[250];
	CStringArray olFields;
	ilFieldCount = ExtractItemList(opFields, &olFields, ',');
	omDataArray.RemoveAll();


	if(isOdbcInit == false)
	{
		sqlRet = ::SQLAllocEnv(&hEnv);
		if(sqlRet == SQL_SUCCESS)
		{
			sqlRet = ::SQLAllocConnect(hEnv, &hDbConn);
			if(sqlRet == SQL_SUCCESS)
			{
				sqlRet = ::SQLDriverConnect(hDbConn,0,(unsigned char*)"DSN=Peking-ODBC;UID=ceda;PWD=ceda", SQL_NTS, pclConnStrOut, 256, NULL, SQL_DRIVER_NOPROMPT);
				if(sqlRet == SQL_SUCCESS)
				{
					isOdbcInit = true;				
				}
				else
				{
					AfxMessageBox("Could not connect ODBC-Database!!");
					return 0;
				}
			}
		}
	} 

	CString olSql;
	if(opAction == "Select")
	{
		if(::SQLAllocStmt(hDbConn, &hStmt) == SQL_SUCCESS)
		{

			CString olSql = opAction + " " + opFields + " FROM " + "CEDA." + opObject + " " + opWhere;
			if(::SQLExecDirect(hStmt, (unsigned char*)olSql.GetBuffer(0), SQL_NTS) == SQL_SUCCESS) //ilFieldCount
			{
			   //SQLNumResultCols(hStmt, &nCols);
			   for(int ilC =0; ilC < ilFieldCount; ilC++)
				  SQLBindCol(hStmt, (UWORD)(ilC+1), SQL_C_CHAR, (unsigned char*)sDataArray[ilC], 250, &dwDataLen[ilC]);

				for(sqlRet = ::SQLFetch(hStmt); sqlRet == SQL_SUCCESS; sqlRet = ::SQLFetch(hStmt))
				{
					pclDataBuffer[0]='\0';
				  for(int i=0;  i<ilFieldCount; i++) 
				  {
					 // check if the column is a null value?
					 strcat(pclDataBuffer, (dwDataLen[i]==SQL_NULL_DATA)?"":sDataArray[i]);
					 int dwText = strlen(pclDataBuffer);
					 pclDataBuffer[dwText++] = ',';
					 pclDataBuffer[dwText] = '\0';
				  }
				  if (*pclDataBuffer)
					 pclDataBuffer[strlen(pclDataBuffer)-1]='\0';
				  else
					 break;
				  omDataArray.Add(pclDataBuffer);
				  pclDataBuffer[0]='\0';

				}
/*				for(sqlRet = ::SQLFetch(hStmt); sqlRet == SQL_SUCCESS; sqlRet = ::SQLFetch(hStmt))
				{
					CString olPart;
					for(int i = 0; i < ilFieldCount; i++)
					{
						SQLGetData(hStmt, i+1, SQL_C_CHAR, pclValue, 2000, &OutputDataLen);
						if((i+1) == ilFieldCount)
						{
							olPart += CString(pclValue);
						}
						else
						{
							olPart += CString(pclValue) + ",";
						}
					}

					omDataArray.Add(olPart);
				}
*/
			}
			::SQLFreeStmt(hStmt, SQL_DROP);
		}

	}
	if(opAction == "Update")
	{
	}
	if(opAction == "Insert")
	{
	}
	if(opAction == "Delete")
	{
	}

	return 0;
}

bool CUfisComCtrl::InitAdo()
{
	bool blRet = true;
	pomAdoCtrl = new CAdodc();
	isOdbcInit = true;
	//IOleClientSite *polSite = GetClientSite();
	//IOleContainer *polCont=NULL;
	//polSite->GetContainer(&polCont);

	//pomAdoCtrl->Create("", WS_CHILD, CRect(0, 0, 0, 0), GetParent(), 1);
	pomAdoCtrl->SetCommandTimeout(120); 
	pomAdoCtrl->SetCommandType(1); // 1 = Textcommand ==> "Select ... from ... where ..."
	pomAdoCtrl->SetConnectionString("Provider=MSDASQL.1;Password=ceda;Persist Security Info=True;User ID=ceda;Data Source=Peking-ODBC");
	pomAdoCtrl->SetConnectionTimeout(15);
	pomAdoCtrl->SetCursorLocation(3); // 3 = UseClient 2 = UseServer
	pomAdoCtrl->SetCursorType(3);	// 1 = OpenKeyset, 2 = OpenDynamic, 3 = OpenStatic
	pomAdoCtrl->SetEnabled(FALSE);
	pomAdoCtrl->SetEOFAction(0); // 0 = MoveLast, 1 = StayEOF, 2 = AddNew
	pomAdoCtrl->SetLockType(3);  // -1 = Undefined, 1 = LockReadOnly, 2 = LockPessimistic, 3 = LockOptomistic, 4 = LockBatchOptimistic
	pomAdoCtrl->SetMode(3); // Read/Write
	pomAdoCtrl->SetOrientation(0); // 0 = Horizontal, 1 = Vertical
	//RecordSource ==> has later to ve removed
	pomAdoCtrl->SetRecordSource("select count(*) from alttab where add4='USA'");
	pomAdoCtrl->ShowWindow(SW_HIDE);
	return blRet;
}

/*	if(!olDB.IsOpen())
	{
		//if(olDB.OpenEx(NULL, CDatabase::forceOdbcDialog) == FALSE)
		//"ODBC;DSN=Peking-ODBC;UID=ceda;PWD=ceda;DBQ=Peking;DBA=W;APA=T;PFC=1;TLO=0;"
		//if(olDB.Open( "DSN=Peking-ODBC;UID=ceda;PWD=ceda;DBQ=Peking;DBA=W;APA=T;PFC=1;TLO=0;", FALSE, FALSE, "ODBC;") == FALSE)
		if(olDB.Open( "ODBC;DSN=Peking-ODBC;UID=ceda;PWD=ceda;DBQ=Peking;DBA=W;APA=T;PFC=1;TLO=0;", FALSE, FALSE, "ODBC;") == FALSE)
		{
			AfxMessageBox("Unable to Connect to ODBC-Database!!");
			return -1;
		}
	}
		if(isOdbcInit == false)
		{
			InitAdo();
		}
		olRS = pomAdoCtrl->GetRecordset();
		olRS.MoveFirst();
		while(!olRS.GetEof()) 
		{
			CFields olFields;
			VARIANT olStart;
			//VT_I4 l = 0;
			olFields = olRS.GetFields();
			//VARIANT olV = olRS.GetRows(1, 0, olFields);
			
		}

		omDataArray.RemoveAll();
		olSql = opAction + " " + opFields + " FROM " + "CEDA." + opObject + " " + opWhere;
		olRS.Open(AFX_DB_USE_DEFAULT_TYPE, olSql);
		olRS.MoveFirst();
		while (!olRS.IsEOF())
		{
			CString olCurrRow;
			CString olTmp;
			for(int i = 0; i < omCurrFields.GetSize(); i++)
			{
				olRS.GetFieldValue(omCurrFields[i], olTmp);
				olTmp.TrimRight();
				olCurrRow += olTmp;
				if((i+1) < omCurrFields.GetSize())
				{
					olCurrRow += ",";
				}
			}
			if(olCurrRow.GetLength() > 0)
			{
				omDataArray.Add(olCurrRow);
			}
			olRS.MoveNext();
		}

		olSql.Format("Datens�tze: %d", omDataArray.GetSize());
		AfxMessageBox(olSql);
*/



void CUfisComCtrl::OnReqidChanged() 
{
	// TODO: Add notification handler code

	SetModifiedFlag();
}

void CUfisComCtrl::OnDest1Changed() 
{
	// TODO: Add notification handler code

	SetModifiedFlag();
}

void CUfisComCtrl::OnDest2Changed() 
{
	// TODO: Add notification handler code

	SetModifiedFlag();
}

void CUfisComCtrl::OnCmdChanged() 
{
	// TODO: Add notification handler code

	SetModifiedFlag();
}

void CUfisComCtrl::OnObjectChanged() 
{
	// TODO: Add notification handler code

	SetModifiedFlag();
}

void CUfisComCtrl::OnSeqChanged() 
{
	// TODO: Add notification handler code

	SetModifiedFlag();
}

void CUfisComCtrl::OnTwsChanged() 
{
	// TODO: Add notification handler code

	SetModifiedFlag();
}

void CUfisComCtrl::OnTweChanged() 
{
	// TODO: Add notification handler code

	SetModifiedFlag();
}

void CUfisComCtrl::OnOrigNameChanged() 
{
	// TODO: Add notification handler code

	SetModifiedFlag();
}

void CUfisComCtrl::OnSelectionChanged() 
{
	// TODO: Add notification handler code

	SetModifiedFlag();
}

void CUfisComCtrl::OnFieldsChanged() 
{
	// TODO: Add notification handler code

	SetModifiedFlag();
}

BSTR CUfisComCtrl::GetDataBuffer(BOOL WithClear) 
{
	CString strResult;
	if(WithClear == TRUE)
	{
		while(omDataArray.GetSize() > 0)
		{
			strResult+=omDataArray[0] + CString("\n");
			omDataArray.RemoveAt(0);
		}
	}
	else
	{
		for(int i = 0; i < omDataArray.GetSize(); i++)
		{
			strResult+=omDataArray[i] + CString("\n");
		}
	}
	return strResult.AllocSysString();
}

void CUfisComCtrl::ClearDataBuffer() 
{
	omDataArray.RemoveAll();
}

short CUfisComCtrl::CallServer(LPCTSTR Command, LPCTSTR Object, LPCTSTR Fields, LPCTSTR Data, LPCTSTR Where, LPCTSTR Timout) 
{
	short ilReturn = 0;
	CString strResult;
	CString olCommand = Command;
	CString olAction;
	CString olObject = Object;
	CString olWorkstation = omWorkStation;
	CString olUserName = omUserName;
	CString olSeq = m_seq;
	CString olTws = m_tws;
	CString olTwe = m_twe;

	if (this->bmIsConnected == false)
	{
		this->bmIsFromCallServer = true;
		InitCom(omServerName, omAccessMethod);
		this->bmIsFromCallServer = false;
	}

	if(olCommand == "RT" || olCommand == "GFR")
		olAction = "Select";
	if(olCommand == "URT")
		olAction = "Update";
	if(olCommand == "IRT")
		olAction = "Insert";
	if(olCommand == "DRT")
		olAction = "Delete";

	CString olFields = Fields;
	omCurrFields.RemoveAll();
	ExtractItemList(Fields, &omCurrFields, ',');
	CString olWhere = Where;
	CString olData = Data;
	CString olTimeout = Timout;
	if(olTwe.IsEmpty())
	{
		olTwe = this->omHomeAirport + "," + this->omTableExt + "," + m_module;
	}

	if(omAccessMethod == "CEDA" )
	{
		ilReturn = CallCeda(olWorkstation.GetBuffer(0),olUserName.GetBuffer(0),olWorkstation.GetBuffer(0),     	
					olCommand.GetBuffer(0), olObject.GetBuffer(0), olSeq.GetBuffer(0), olTws.GetBuffer(0), 
					olTwe.GetBuffer(0), olWhere.GetBuffer(0),        	
					olFields.GetBuffer(0), olData.GetBuffer(0), olTimeout.GetBuffer(0));
	}
	else    //if(omAccessMethod == "ODBC")
	{
		CallRecordSet(olAction, olObject, olFields, olWhere, olData);
	}
	CString olTmp;
	olTmp.Format("%ld Records", omDataArray.GetSize());

	if (this->bmStayConnected == false)
	{
		if (this->bmIsConnected == true)
		{
			CleanupCom();
		}
	}

	return ilReturn;
}

void CUfisComCtrl::OnVersionChanged() 
{
	// TODO: Add notification handler code

	SetModifiedFlag();
}

void CUfisComCtrl::OnModuleChanged() 
{
	// TODO: Add notification handler code

	SetModifiedFlag();
}

void CUfisComCtrl::OnLastErrorMessageChanged() 
{
	// TODO: Add notification handler code

	SetModifiedFlag();
}

void CUfisComCtrl::OnBuildDateChanged() 
{
	// TODO: Add notification handler code

	SetModifiedFlag();
}

void CUfisComCtrl::GetVersionInfo()
{
	HMODULE hModule = AfxGetInstanceHandle();
	VersionInfo olVersionInfo;
	if (VersionInfo::GetVersionInfo(hModule,olVersionInfo))
	{
		m_version = olVersionInfo.omFileVersion;
	}

	m_buildDate = CString(__DATE__) + " - " + CString(__TIME__);

}

BOOL CUfisComCtrl::GetStayConnected() 
{
	// TODO: Add your property handler here

	return this->bmStayConnected;
}

void CUfisComCtrl::SetStayConnected(BOOL bNewValue) 
{
	// TODO: Add your property handler here
	if ((BOOL)bmStayConnected != bNewValue)
	{
		bmStayConnected = bNewValue;
		SetModifiedFlag();
	}
}
