﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Controls;
using System.Windows;
using Ufis.Utilities;

namespace Ufis.Resources
{
    /// <summary>
    /// Provides the functionality to select data template for DevExpress.Xpf.Grid.GridColumn
    /// based on Ufis.Utilities.UfisGridColumn.
    /// </summary>
    public class GridColumnTemplateSelector : DataTemplateSelector
    {
        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            DataTemplate dataTemplate = null;
            
            UfisGridColumn column = item as UfisGridColumn;
            Control control = container as Control;
            if (column != null && control != null)
            {
                object objResource = control.TryFindResource(column.Settings + "ColumnTemplate");
                if (objResource != null && objResource is DataTemplate)
                    dataTemplate = (DataTemplate)objResource;
            }

            if (dataTemplate != null)
                return dataTemplate;
            else
                return base.SelectTemplate(item, container);
        }
    }
}
