using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Data;

namespace Ufis.Resources
{
    /// <summary>
    /// Converts AutoFilterRow to AutoFilterRow Text
    /// </summary>
    [ValueConversion(typeof(bool), typeof(string))]
    public class AutoFilterRowTextConverter : IValueConverter
    {
        const string showAutoFilterRowText = "Show Auto Filter Row";
        const string hideAutoFilterRowText = "Hide Auto Filter Row";

        #region IValueConverter Members

        public object Convert(object value, Type targetType,
            object parameter, System.Globalization.CultureInfo culture)
        {
            bool isShown = (bool)value;

            if (isShown)
            {
                return hideAutoFilterRowText;
            }
            else
            {
                return showAutoFilterRowText;
            }
        }

        public object ConvertBack(object value, Type targetType,
            object parameter, System.Globalization.CultureInfo culture)
        {
            return (value.ToString() == hideAutoFilterRowText);
        }

        #endregion
    }
}
