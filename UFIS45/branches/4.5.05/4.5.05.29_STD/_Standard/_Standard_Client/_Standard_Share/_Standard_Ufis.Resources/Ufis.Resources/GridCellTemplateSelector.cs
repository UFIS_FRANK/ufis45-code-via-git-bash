﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Controls;
using System.Windows;
using DevExpress.Xpf.Grid;

namespace Ufis.Resources
{
    /// <summary>
    /// Provides the functionality to select data template for DevExpress.Xpf.Grid.GridCell
    /// based on Ufis.Utilities.UfisGridColumn.
    /// </summary>
    public class GridCellTemplateSelector : DataTemplateSelector
    {
        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            DataTemplate dataTemplate = null;
            
            EditGridCellData cell = item as EditGridCellData;
            if (cell != null)
            {
                ColumnBase column = cell.Column;
                if (column != null)
                {
                    object objResource = column.TryFindResource(column.Name + "CellTemplate");
                    if (objResource != null && objResource is DataTemplate)
                        dataTemplate = (DataTemplate)objResource;
                }
            }

            if (dataTemplate != null)
                return dataTemplate;
            else
                return base.SelectTemplate(item, container);
        }
    }
}
