﻿using System;
using System.Collections.Generic;
using System.Linq;
using DevExpress.Xpf.Docking;

namespace Ufis.MVVM.ViewModel
{
    /// <summary>
    /// Represents the workspace that that contains multiple child-workspaces 
    /// and multiple commands and can be render in DevExpress Xpf Docking suite.
    /// This class is abstract.
    /// </summary>
    public abstract class PanelWorkspaceViewModel : WorkspaceViewModel
    {
        /// <summary>
        /// Gets the name of DevExpress docking layout group object which will host this workspace.
        /// </summary>
        abstract protected string TargetName { get; }

        /// <summary>
        /// Initializes the new PanelWorkspaceViewModel object.
        /// </summary>
        public PanelWorkspaceViewModel()
        {
            MVVMHelper.SetTargetName(this, TargetName);
        }
        
        /// <summary>
        /// Initializes the new PanelWorkspaceViewModel object.
        /// </summary>
        /// <param name="targetName">The name of DevExpress docking layout group object which will host this workspace.</param>
        public PanelWorkspaceViewModel(string targetName)
        {
            MVVMHelper.SetTargetName(this, targetName);
        }
    }
}
