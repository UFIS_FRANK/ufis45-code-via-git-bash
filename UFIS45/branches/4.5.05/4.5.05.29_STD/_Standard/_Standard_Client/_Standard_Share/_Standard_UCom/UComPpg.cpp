// UComPpg.cpp : Implementation of the CUComPropPage property page class.

#include "stdafx.h"
#include "UCom.h"
#include "UComPpg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


IMPLEMENT_DYNCREATE(CUComPropPage, COlePropertyPage)


/////////////////////////////////////////////////////////////////////////////
// Message map

BEGIN_MESSAGE_MAP(CUComPropPage, COlePropertyPage)
	//{{AFX_MSG_MAP(CUComPropPage)
	// NOTE - ClassWizard will add and remove message map entries
	//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// Initialize class factory and guid

IMPLEMENT_OLECREATE_EX(CUComPropPage, "UCOM.UComPropPage.1",
	0xf9478337, 0x76ba, 0x11d6, 0x80, 0x67, 0, 0x1, 0x2, 0x22, 0x5, 0xe4)


/////////////////////////////////////////////////////////////////////////////
// CUComPropPage::CUComPropPageFactory::UpdateRegistry -
// Adds or removes system registry entries for CUComPropPage

BOOL CUComPropPage::CUComPropPageFactory::UpdateRegistry(BOOL bRegister)
{
	if (bRegister)
		return AfxOleRegisterPropertyPageClass(AfxGetInstanceHandle(),
			m_clsid, IDS_UCOM_PPG);
	else
		return AfxOleUnregisterClass(m_clsid, NULL);
}


/////////////////////////////////////////////////////////////////////////////
// CUComPropPage::CUComPropPage - Constructor

CUComPropPage::CUComPropPage() :
	COlePropertyPage(IDD, IDS_UCOM_PPG_CAPTION)
{
	//{{AFX_DATA_INIT(CUComPropPage)
	// NOTE: ClassWizard will add member initialization here
	//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_DATA_INIT
}


/////////////////////////////////////////////////////////////////////////////
// CUComPropPage::DoDataExchange - Moves data between page and properties

void CUComPropPage::DoDataExchange(CDataExchange* pDX)
{
	//{{AFX_DATA_MAP(CUComPropPage)
	// NOTE: ClassWizard will add DDP, DDX, and DDV calls here
	//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_DATA_MAP
	DDP_PostProcessing(pDX);
}


/////////////////////////////////////////////////////////////////////////////
// CUComPropPage message handlers
