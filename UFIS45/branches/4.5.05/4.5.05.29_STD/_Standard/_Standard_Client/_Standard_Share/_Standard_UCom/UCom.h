#if !defined(AFX_UCOM_H__F947833C_76BA_11D6_8067_0001022205E4__INCLUDED_)
#define AFX_UCOM_H__F947833C_76BA_11D6_8067_0001022205E4__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

// UCom.h : main header file for UCOM.DLL

#if !defined( __AFXCTL_H__ )
	#error include 'afxctl.h' before including this file
#endif

#include "resource.h"       // main symbols

/////////////////////////////////////////////////////////////////////////////
// CUComApp : See UCom.cpp for implementation.

class CUComApp : public COleControlModule
{
public:
	BOOL InitInstance();
	int ExitInstance();
};

extern const GUID CDECL _tlid;
extern const WORD _wVerMajor;
extern const WORD _wVerMinor;

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_UCOM_H__F947833C_76BA_11D6_8067_0001022205E4__INCLUDED)
