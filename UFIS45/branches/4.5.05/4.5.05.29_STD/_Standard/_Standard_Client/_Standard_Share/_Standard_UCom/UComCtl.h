#if !defined(AFX_UCOMCTL_H__F9478343_76BA_11D6_8067_0001022205E4__INCLUDED_)
#define AFX_UCOMCTL_H__F9478343_76BA_11D6_8067_0001022205E4__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "FastCedaConnection.h"
#include ".\UfisOdbc.h"
// UComCtl.h : Declaration of the CUComCtrl ActiveX Control class.

/////////////////////////////////////////////////////////////////////////////
// CUComCtrl : See UComCtl.cpp for implementation.

class CUComCtrl : public COleControl
{
	DECLARE_DYNCREATE(CUComCtrl)

// Constructor
public:
	CUComCtrl();

	UfisOdbc omUfisOdbc;
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CUComCtrl)
	public:
	virtual void OnDraw(CDC* pdc, const CRect& rcBounds, const CRect& rcInvalid);
	virtual void DoPropExchange(CPropExchange* pPX);
	virtual void OnResetState();
	//}}AFX_VIRTUAL

// Implementation
protected:
	~CUComCtrl();

	DECLARE_OLECREATE_EX(CUComCtrl)    // Class factory and guid
	DECLARE_OLETYPELIB(CUComCtrl)      // GetTypeInfo
	DECLARE_PROPPAGEIDS(CUComCtrl)     // Property page IDs
	DECLARE_OLECTLTYPE(CUComCtrl)		// Type name and misc status

// Message maps
	//{{AFX_MSG(CUComCtrl)
		// NOTE - ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

// Dispatch maps
	//{{AFX_DISPATCH(CUComCtrl)
	CString m_version;
	afx_msg void OnVersionChanged();
	CString m_buildDate;
	afx_msg void OnBuildDateChanged();
	CString m_SQLAccessMethod;
	afx_msg void OnSQLAccessMethodChanged();
	CString m_SQLConnectionString;
	afx_msg void OnSQLConnectionStringChanged();
	afx_msg BSTR GetApplicationName();
	afx_msg void SetApplicationName(LPCTSTR lpszNewValue);
	afx_msg BSTR GetPort();
	afx_msg void SetPort(LPCTSTR lpszNewValue);
	afx_msg BSTR GetHopo();
	afx_msg void SetHopo(LPCTSTR lpszNewValue);
	afx_msg BSTR GetServerName();
	afx_msg void SetServerName(LPCTSTR lpszNewValue);
	afx_msg BSTR GetTableExtension();
	afx_msg void SetTableExtension(LPCTSTR lpszNewValue);
	afx_msg BSTR GetUserName();
	afx_msg void SetUserName(LPCTSTR lpszNewValue);
	afx_msg BSTR GetWorkstationName();
	afx_msg void SetWorkstationName(LPCTSTR lpszNewValue);
	afx_msg BSTR GetSendTimeoutSeconds();
	afx_msg void SetSendTimeoutSeconds(LPCTSTR lpszNewValue);
	afx_msg BSTR GetReceiveTimeoutSeconds();
	afx_msg void SetReceiveTimeoutSeconds(LPCTSTR lpszNewValue);
	afx_msg BSTR GetRecordSeparator();
	afx_msg void SetRecordSeparator(LPCTSTR lpszNewValue);
	afx_msg BSTR GetCedaIdentifier();
	afx_msg void SetCedaIdentifier(LPCTSTR lpszNewValue);
	afx_msg long GetPacketSize();
	afx_msg void SetPacketSize(long nNewValue);
	afx_msg BSTR GetErrorSimulation();
	afx_msg void SetErrorSimulation(LPCTSTR lpszNewValue);
	afx_msg BSTR GetLastError();
	afx_msg BOOL CedaAction(LPCTSTR Command, LPCTSTR DBTable, LPCTSTR DBFields, LPCTSTR DBData, LPCTSTR DBWhere);
	afx_msg long GetBufferCount();
	afx_msg BSTR GetRecord(long RecordNo);
	//}}AFX_DISPATCH
	DECLARE_DISPATCH_MAP()

// Event maps
	//{{AFX_EVENT(CUComCtrl)
	void FirePackageReceived(long ipPackage)
		{FireEvent(eventidPackageReceived,EVENT_PARAM(VTS_I4), ipPackage);}
	//}}AFX_EVENT
	DECLARE_EVENT_MAP()

// Dispatch and event IDs
public:
	FastCedaConnection omFastCeda;
	enum {
	//{{AFX_DISP_ID(CUComCtrl)
	dispidApplicationName = 5L,
	dispidPort = 6L,
	dispidHopo = 7L,
	dispidServerName = 8L,
	dispidTableExtension = 9L,
	dispidUserName = 10L,
	dispidWorkstationName = 11L,
	dispidSendTimeoutSeconds = 12L,
	dispidReceiveTimeoutSeconds = 13L,
	dispidRecordSeparator = 14L,
	dispidCedaIdentifier = 15L,
	dispidVersion = 1L,
	dispidBuildDate = 2L,
	dispidSQLAccessMethod = 3L,
	dispidSQLConnectionString = 4L,
	dispidPacketSize = 16L,
	dispidErrorSimulation = 17L,
	dispidGetLastError = 18L,
	dispidCedaAction = 19L,
	dispidGetBufferCount = 20L,
	dispidGetRecord = 21L,
	eventidPackageReceived = 1L,
	//}}AFX_DISP_ID
	};

	void GetVersionInfo();

	static void EventCallback(CCmdTarget *popControl,long ipPackage);
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_UCOMCTL_H__F9478343_76BA_11D6_8067_0001022205E4__INCLUDED)
