// ErrorPage.cpp : implementation file
//

#include <stdafx.h>
#include <Alerter.h>
#include <ErrorPage.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CErrorPage dialog


CErrorPage::CErrorPage(CWnd* pParent /*=NULL*/)
	: CDialog(CErrorPage::IDD, pParent)
{
	//{{AFX_DATA_INIT(CErrorPage)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CErrorPage::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CErrorPage)
	DDX_Control(pDX, IDC_TABCTRL, m_TabControl);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CErrorPage, CDialog)
	//{{AFX_MSG_MAP(CErrorPage)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CErrorPage message handlers

BOOL CErrorPage::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}
