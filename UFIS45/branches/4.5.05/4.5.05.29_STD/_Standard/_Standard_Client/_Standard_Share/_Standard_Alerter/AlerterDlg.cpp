// AlerterDlg.cpp : implementation file
//

#include <stdafx.h>
#include <Alerter.h>
#include <AlerterDlg.h>
#include <DlgProxy.h>
#include <VersionInfo.h>
#include <AatHelp.h>
#include <CcsTime.h>
#include <LibGlobl.h>
#include "resource.h"
#include <process.h>
 
#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

//IID of interface BcPrxyEvents
static const IID IID_BcPrxyEvents = {0x68e25ff2, 0x6790, 0x11d4, {0x90, 0x3b, 0x0, 0x01, 0x02, 0x04, 0xaa, 0x51}};  

int ExtractItemList(CString opSubString, CStringArray *popStrArray, char cpTrenner = ',');

/////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg(const CString& ropHostName,const CString& ropHostType,const CString& ropUserName,const CTime& ropLoginTime);

// Dialog Data
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	CStatic	m_ClassLibVersion;
	CStatic	m_StcUser;
	CStatic	m_StcServer;
	CStatic	m_StcLogintime;
	CStatic	m_Copyright4;
	CStatic	m_Copyright3;
	CStatic	m_Copyright2;
	CStatic	m_Copyright1;
	CStatic	m_Server;
	CStatic	m_User;
	CStatic	m_Logintime;
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAboutDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	//{{AFX_MSG(CAboutDlg)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	CString omHostName;
	CString omHostType;
	CString omUserName;
	CTime	omLoginTime;

};

CAboutDlg::CAboutDlg(const CString& ropHostName,const CString& ropHostType,const CString& ropUserName,const CTime& ropLoginTime) 
: CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT
	omHostName = ropHostName;
	omHostType = ropHostType;
	omUserName = ropUserName;
	omLoginTime= ropLoginTime;
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	DDX_Control(pDX, IDC_CLASSLIBVERSION, m_ClassLibVersion);
	DDX_Control(pDX, IDC_STATIC_USER, m_StcUser);
	DDX_Control(pDX, IDC_STATIC_SERVER, m_StcServer);
	DDX_Control(pDX, IDC_STATIC_LOGINTIME, m_StcLogintime);
	DDX_Control(pDX, IDC_COPYRIGHT4, m_Copyright4);
	DDX_Control(pDX, IDC_COPYRIGHT3, m_Copyright3);
	DDX_Control(pDX, IDC_COPYRIGHT2, m_Copyright2);
	DDX_Control(pDX, IDC_COPYRIGHT1, m_Copyright1);
	DDX_Control(pDX, IDC_SERVER,	 m_Server);
	DDX_Control(pDX, IDC_USER,		 m_User);
	DDX_Control(pDX, IDC_LOGINTIME,  m_Logintime);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
		// No message handlers
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

BOOL CAboutDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// Versionsinformation ermitteln
	VersionInfo rlInfo;
//	VersionInfo::GetVersionInfo(AfxGetInstanceHandle(),rlInfo);
	VersionInfo::GetVersionInfo(NULL,rlInfo);	// must use NULL (and it works), because AFX is not initialized yet and will cause an ASSERT

	CString olVersionString;
	olVersionString.Format("%s %s %s  Compiled: %s",rlInfo.omProductName,rlInfo.omFileVersion,rlInfo.omSpecialBuild,__DATE__);

	// Statics einstellen
	m_Copyright1.SetWindowText(rlInfo.omProductVersion);
	m_Copyright2.SetWindowText(olVersionString);
	m_Copyright3.SetWindowText(rlInfo.omLegalCopyright);
	m_Copyright4.SetWindowText(rlInfo.omCompanyName);

	CString olText;
	olText.LoadString(IDS_STATIC_SERVER);
	m_StcServer.SetWindowText(olText);

	olText.LoadString(IDS_STATIC_USER);
	m_StcUser.SetWindowText(olText);


	olText.LoadString(IDS_STATIC_LOGINTIME);
	m_StcLogintime.SetWindowText(olText);

	CString olServer = omHostName;
	olServer  += " / ";
	olServer  += omHostType;
	m_Server.SetWindowText(olServer);

	m_User.SetWindowText(omUserName);
	m_Logintime.SetWindowText(omLoginTime.Format("%d.%m.%Y  %H:%M"));

	olText.LoadString(IDS_ALERTER_ABOUT);
	SetWindowText(olText);

	m_ClassLibVersion.SetWindowText(CString(pcgClassLibVersion));
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

/////////////////////////////////////////////////////////////////////////////
// CAlerterDlg dialog

IMPLEMENT_DYNAMIC(CAlerterDlg, CDialog);

CAlerterDlg::CAlerterDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CAlerterDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CAlerterDlg)
	m_TimeFormat = 0;
	//}}AFX_DATA_INIT
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	m_pAutoProxy			= NULL;
	this->pDispBcPrxyEvents	= NULL;
	this->pDispUfisCom		= NULL;
	this->pIUnknown_Auto	= NULL;
	this->m_dwCookie		= 0;
	this->bmLoggedIn		= FALSE;
	this->omHomeAirport		= "SIN";
	this->omHostName		= "SIN1";
	this->omTableExtension	= "TAB";
	this->imTimer			= -1;
	this->omTich			= CTime::GetCurrentTime();
	this->omLocalDiff1		= CTimeSpan(0,0,0,0);
	this->omLocalDiff2		= CTimeSpan(0,0,0,0);
	this->bmDetailListIndexesOK = FALSE;

	EnableAutomation();
}

CAlerterDlg::~CAlerterDlg()
{
	// If there is an automation proxy for this dialog, set
	//  its back pointer to this dialog to NULL, so it knows
	//  the dialog has been deleted.
	if (m_pAutoProxy != NULL)
		m_pAutoProxy->m_pDialog = NULL;
}

void CAlerterDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAlerterDlg)
	DDX_Control(pDX, IDC_DETAIL_HEADER, m_DetailHeader);
	DDX_Control(pDX, IDC_AATLOGINCONTROL1, m_LoginControl);
	DDX_Control(pDX, IDC_TABCTRL, m_DetailList);
	DDX_Control(pDX, IDC_TABCTRL1, m_OverviewList);
	DDX_Control(pDX, IDC_UFISCOMCTRL1, m_UfisComControl);
	DDX_Control(pDX, IDC_TAB_PAGES,m_Pages);
	DDX_Radio(pDX, IDC_RADIO_UCS, m_TimeFormat);
	DDX_Control(pDX, IDC_AMPTESTCTRL1, m_TrafficLightControl);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAlerterDlg, CDialog)
	//{{AFX_MSG_MAP(CAlerterDlg)
	ON_WM_SYSCOMMAND()
	ON_WM_DESTROY()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_WM_CLOSE()
	ON_BN_CLICKED(ID_HELP, OnHelp)
	ON_BN_CLICKED(IDC_CONFIRM, OnConfirm)
	ON_BN_CLICKED(IDC_REOPEN, OnReopen)
	ON_BN_CLICKED(IDC_RADIO_LOCAL, OnRadioLocal)
	ON_BN_CLICKED(IDC_RADIO_UCS, OnRadioUcs)
	ON_BN_CLICKED(IDC_CLOSE, OnCloseStatus)
	ON_COMMAND(IDS_STRING140, OnMenuConfirm)
	ON_COMMAND(IDS_STRING141, OnMenuClose)
	ON_COMMAND(IDS_STRING142, OnMenuReopen)
	ON_WM_TIMER()
	ON_WM_SHOWWINDOW()
	ON_WM_SIZE()
	ON_WM_ACTIVATE()
	ON_NOTIFY(TCN_SELCHANGING, IDC_TAB_PAGES, OnSelchangingTabPages)
	ON_NOTIFY(TCN_SELCHANGE, IDC_TAB_PAGES, OnSelchangeTabPages)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

BEGIN_EVENTSINK_MAP(CAlerterDlg, CDialog)
	ON_EVENT(CAlerterDlg,IDC_AATLOGINCONTROL1,1,OnLoginResult,VTS_BSTR)
	ON_EVENT(CAlerterDlg,IDC_TABCTRL1 , 9 /* RowSelectionChanged */, OnRowSelectionChangedTabctrl1, VTS_I4 VTS_BOOL)
	ON_EVENT_RANGE(CAlerterDlg,IDS_STRING102,IDS_STRING110, 9 /* RowSelectionChanged */, OnRowSelectionChanged, VTS_I4 VTS_I4 VTS_BOOL)
	ON_EVENT(CAlerterDlg, IDC_TABCTRL1, 3 /* SendRButtonClick */, OnSendRButtonClickTabctrl1, VTS_I4 VTS_I4)
	ON_EVENT_RANGE(CAlerterDlg,IDS_STRING102,IDS_STRING110, 3 /* SendRButtonClick */, OnSendRButtonClick, VTS_I4 VTS_I4 VTS_I4)
	ON_EVENT_RANGE(CAlerterDlg,IDS_STRING102,IDS_STRING110, 8 /* OnHScroll        */, OnOnHScroll, VTS_I4 VTS_I4)
END_EVENTSINK_MAP()


BEGIN_DISPATCH_MAP(CAlerterDlg, CDialog)
	//{{AFX_DISPATCH_MAP(CAlerterDlg)
		DISP_FUNCTION_ID(CAlerterDlg,"OnBcReceive",1,OnBroadcast, VT_BOOL, VTS_BSTR VTS_BSTR VTS_BSTR VTS_BSTR VTS_BSTR VTS_BSTR VTS_BSTR VTS_BSTR VTS_BSTR VTS_BSTR VTS_BSTR VTS_BSTR)
	//}}AFX_DISPATCH_MAP
END_DISPATCH_MAP()

BEGIN_INTERFACE_MAP(CAlerterDlg, CDialog)
	INTERFACE_PART(CAlerterDlg, IID_BcPrxyEvents, Dispatch)
END_INTERFACE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CAlerterDlg message handlers

BOOL CAlerterDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	m_resizeHelper.Init(this->m_hWnd);

	// default behaviour is to resize sub windows proportional to the 
	// resize of the parent you can fix horizontal and/or vertical 
	// dimensions for some sub windows by either specifying the 
	// sub window by its hwnd:
	m_resizeHelper.Fix(IDC_CONFIRM,DlgResizeHelper::kWidth,DlgResizeHelper::kHeight);
	m_resizeHelper.Fix(IDC_CLOSE,DlgResizeHelper::kWidth,DlgResizeHelper::kHeight);
	m_resizeHelper.Fix(IDC_REOPEN,DlgResizeHelper::kWidth,DlgResizeHelper::kHeight);
	m_resizeHelper.Fix(IDC_RADIO_UCS,DlgResizeHelper::kWidth,DlgResizeHelper::kHeight);
	m_resizeHelper.Fix(IDC_RADIO_LOCAL,DlgResizeHelper::kWidth,DlgResizeHelper::kHeight);
	m_resizeHelper.Fix(ID_HELP,DlgResizeHelper::kWidth,DlgResizeHelper::kHeight);
	m_resizeHelper.Fix(IDOK,DlgResizeHelper::kWidth,DlgResizeHelper::kHeight);

	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	
	// TODO: Add extra initialization here
	m_LoginControl.SetApplicationName("ALERTER");
	m_LoginControl.SetRegisterApplicationString("ALERTER,InitModu,InitModu,InitModu");
	m_LoginControl.SetInfoButtonVisible(TRUE);

	IUnknown *pIUnk = m_UfisComControl.GetControlUnknown();
	IDispatch *pIDispatch = NULL;
	HRESULT res = pIUnk->QueryInterface(IID_IDispatch,(void **)&pIDispatch);
	VERIFY (res == S_OK);

	m_LoginControl.SetUfisComCtrl(pIDispatch);

	CString olRetStr = m_LoginControl.ShowLoginDialog();

	// check, if we are logged in
	if (olRetStr.Find("OK") == -1)
	{
		PostQuitMessage(0);			
		return FALSE;
	}

	this->omLoginTime = CTime::GetCurrentTime();

	char pclUseLocal[256];
	char pclHopo[20];
	char pclLoadTime[256];	
	char pclConfigPath[256];
	char pclInterfaces[256];

    if (getenv("CEDA") == NULL)
        strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
    else
        strcpy(pclConfigPath, getenv("CEDA"));



    GetPrivateProfileString("GLOBAL", "HOMEAIRPORT", "SIN", pclHopo, sizeof pclHopo, pclConfigPath );


	this->omHomeAirport		= CString(pclHopo);


	GetPrivateProfileString("Alerter","USELOCAL","TRUE",pclUseLocal,sizeof(pclUseLocal),pclConfigPath);
	if (stricmp(pclUseLocal,"TRUE") == 0)
	{
		this->m_TimeFormat = 1;
		UpdateData(FALSE);
	}

	GetPrivateProfileString("Alerter","LOADTIME","24",pclLoadTime,sizeof(pclLoadTime),pclConfigPath);
	imLoadTime = atoi(pclLoadTime);

	BOOL blOK = this->SetLocalDiff();
	VERIFY(blOK);

	CString olTabNameList;
	//Retrieve interfaces from Ceda.ini
	GetPrivateProfileString("Alerter","INTERFACES","NONE",pclInterfaces,sizeof(pclInterfaces),pclConfigPath);
	
	//olTabNameList.LoadString(IDS_STRING143);
	//::ExtractItemList(olTabNameList,&this->omTabNames,',');
	::ExtractItemList(pclInterfaces,&this->omTabNames,',');
	

	blOK = this->StartBc();
	VERIFY(blOK);

	DWORD	llNow,llLast;
	double	flDiff;

	llLast = GetTickCount();

	
	blOK = this->InitTabs();
	VERIFY(blOK);

	llNow  = GetTickCount();
	flDiff = llNow-llLast;
	llLast = llNow;
	TRACE("Alerter::InitTabs() %.3lf sec\n", flDiff/1000.0 );

	blOK = this->FillTabs();
	VERIFY(blOK);

	llNow  = GetTickCount();
	flDiff = llNow-llLast;
	llLast = llNow;
	TRACE("Alerter::FillTabs() %.3lf sec\n", flDiff/1000.0 );

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CAlerterDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout(m_UfisComControl.GetServerName(),this->omHomeAirport,m_LoginControl.GetUserName_(),this->omLoginTime);
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

void CAlerterDlg::OnDestroy()
{
/***
	for (int i = 0; i < this->m_Pages.GetItemCount(); i++)
	{
		TCITEM olItem;
		olItem.mask	   = TCIF_PARAM;
		olItem.lParam  = 0;	
		if (this->m_Pages.GetItem(i,&olItem))
		{
			ERROR_PAGE *polPage = (ERROR_PAGE *)olItem.lParam;
			ASSERT(polPage != NULL);
			delete polPage;
		}
	}
***/
	WinHelp(0L, HELP_QUIT);
	CDialog::OnDestroy();
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CAlerterDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CAlerterDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

// Automation servers should not exit when a user closes the UI
//  if a controller still holds on to one of its objects.  These
//  message handlers make sure that if the proxy is still in use,
//  then the UI is hidden but the dialog remains around if it
//  is dismissed.

void CAlerterDlg::OnClose() 
{
	if (CanExit())
		CDialog::OnClose();
}

void CAlerterDlg::OnOK() 
{
	if (CanExit())
		CDialog::OnOK();
}

void CAlerterDlg::OnCancel() 
{
	if (CanExit())
		CDialog::OnCancel();
}

BOOL CAlerterDlg::CanExit()
{
	// If the proxy object is still around, then the automation
	//  controller is still holding on to this application.  Leave
	//  the dialog around, but hide its UI.
	if (m_dwCookie != 0)
	{
		AfxConnectionUnadvise(pDispBcPrxyEvents,IID_BcPrxyEvents,pIUnknown_Auto,TRUE,m_dwCookie);
		pIUnknown_Auto->Release();
		m_dwCookie = 0;
	}

	if (omBcPrxyEvents.m_lpDispatch != NULL)
	{
		omBcPrxyEvents.ReleaseDispatch();
	}

#if	0
	for (int i = 0; i < this->m_Pages.GetItemCount(); i++)
	{
		TCITEM olItem;
		olItem.mask	   = TCIF_PARAM;
		olItem.lParam  = 0;	
		if (this->m_Pages.GetItem(i,&olItem))
		{
			ERROR_PAGE *polPage = (ERROR_PAGE *)olItem.lParam;
			ASSERT(polPage != NULL);
			polPage->m_DetailList.Detach();
			polPage->m_DetailHeader.Detach();

			delete polPage;
		}

	}
	this->m_Pages.DeleteAllItems();
#endif

	if (m_pAutoProxy != NULL)
	{
		ShowWindow(SW_HIDE);
		return FALSE;
	}

	return TRUE;
}

BOOL CAlerterDlg::OnLoginResult(const char *pszResult)
{
	ASSERT(pszResult);
	CString olResult(pszResult);
	if (olResult.CompareNoCase("CANCEL") == 0)
		bmLoggedIn = FALSE;
	else if (olResult.CompareNoCase("OK") == 0)
		bmLoggedIn = TRUE;
	else
		bmLoggedIn = FALSE;
	return TRUE;		
}

bool CAlerterDlg::StartBc()
{
	CLSID clsid;
	HRESULT res;

	res = CLSIDFromProgID(L"BcProxy.BcPrxy.1",&clsid);
	if (res != S_OK)
	{
		MessageBox (_T("couldn�t get clsid of BcProxy !"));
		return false;
	}

	if (!omBcPrxyEvents.CreateDispatch (clsid))
	{
		MessageBox (_T("unable to create BcPrxy instance !"));
		return false;
	}

	pDispBcPrxyEvents = omBcPrxyEvents.m_lpDispatch;
	// ^^^^^ pointer to Disp. Interface of BcPrxyEvents
	
	IUnknown* lpIUnknown = m_UfisComControl.GetControlUnknown ();
	lpIUnknown->QueryInterface (IID_IDispatch,(void**)&pDispUfisCom);

	m_TrafficLightControl.SetDispBc(pDispBcPrxyEvents);
	m_TrafficLightControl.SetDispatch(pDispUfisCom);
	m_TrafficLightControl.Init();

	//connect server to automation class
	pIUnknown_Auto = this->GetIDispatch(TRUE);
	int erg = AfxConnectionAdvise(pDispBcPrxyEvents,IID_BcPrxyEvents,pIUnknown_Auto,TRUE,&m_dwCookie);

	return true;

}

bool CAlerterDlg::InitTabs()
{
	CString olString;
	for (int i = 0; i < this->omTabNames.GetSize(); i++)
	{
		// load page header string from resource
		olString = this->omTabNames[i];

		// create page
		ERROR_PAGE	*polPage = new ERROR_PAGE;
		polPage->m_HeaderId  = i;

		DWORD dwStyle = WS_CHILD|WS_VISIBLE;
		CRect olRect;

		this->m_DetailHeader.GetWindowRect(olRect);
		this->ScreenToClient(olRect);
		polPage->m_DetailHeader.Create(olString,dwStyle,olRect,this);

		dwStyle = WS_VISIBLE;
		this->m_DetailList.GetWindowRect(olRect);
		this->ScreenToClient(olRect);
		if (!polPage->m_DetailList.Create(_T("Banane"),dwStyle,olRect,this,IDS_STRING102+i))
		{
			delete polPage;
			return false;
		}

		this->m_resizeHelper.Add(polPage->m_DetailHeader.m_hWnd,false);
		this->m_resizeHelper.Add(polPage->m_DetailList.m_hWnd,true);

		polPage->m_DetailList.SetWindowPos(&wndTop,0,0,0,0,SWP_NOMOVE|SWP_NOSIZE);

		CString olHeaderString;
		olHeaderString.LoadString(IDS_STRING114);
		polPage->m_DetailList.SetHeaderText(olHeaderString);

		olHeaderString.LoadString(IDS_STRING115);
		polPage->m_DetailList.SetHeaderLengthString(olHeaderString);

		olHeaderString.LoadString(IDS_STRING118);
		polPage->m_DetailList.SetLogicalFieldList(olHeaderString);

		olHeaderString.LoadString(IDS_STRING137);
		polPage->m_DetailList.SetColumnWidthString(olHeaderString);

		olHeaderString.LoadString(IDS_STRING138);
		polPage->m_DetailList.SetColumnAlignmentString(olHeaderString);

		polPage->m_DetailList.DateTimeSetColumnFormat(1,"YYYYMMDDhhmmss","DD'.'MM'.'YYYY' 'hh':'mm");
		polPage->m_DetailList.DateTimeSetColumnFormat(3,"YYYYMMDDhhmmss","DD'.'MM'.'YYYY' 'hh':'mm");

		polPage->m_DetailList.SetMyName(olString);

		polPage->m_DetailList.ShowHorzScroller(true);
		polPage->m_DetailList.EnableHeaderSizing(true);
		polPage->m_DetailList.ResetContent();

		TCITEM olItem;
		olItem.mask	   = TCIF_TEXT|TCIF_PARAM;
		olItem.dwState = 0;
		olItem.dwStateMask = 0;
		olItem.pszText = olString.GetBuffer(0);
		olItem.cchTextMax = olString.GetLength();
		olItem.iImage  = -1;
		olItem.lParam  = (long)polPage;
		int ilTabno = this->m_Pages.InsertItem(i,&olItem);
		if (ilTabno == -1)
		{
			return false;
		}
		else if (ilTabno == i)
		{
			if (!this->m_Pages.AttachControlToTab(&polPage->m_DetailHeader,ilTabno))
				return false;
			if (!this->m_Pages.AttachControlToTab(&polPage->m_DetailList,ilTabno))
				return false;
			polPage->m_ItemId = ilTabno;
		}

	}
	
	if (this->m_Pages.GetItemCount() > 0)
	{
		this->m_Pages.SetCurSel(0);
	}

	// initialize overview grid
	CString olHeaderString;
	olHeaderString.LoadString(IDS_STRING120);
	this->m_OverviewList.SetHeaderText(olHeaderString);

	olHeaderString.LoadString(IDS_STRING121);
	this->m_OverviewList.SetHeaderLengthString(olHeaderString);

	olHeaderString.LoadString(IDS_STRING120);
	this->m_OverviewList.SetLogicalFieldList(olHeaderString);

	olHeaderString.LoadString(IDS_STRING135);
	this->m_OverviewList.SetColumnWidthString(olHeaderString);

	olHeaderString.LoadString(IDS_STRING136);
	this->m_OverviewList.SetColumnAlignmentString(olHeaderString);

    this->m_OverviewList.SetMainHeaderOnly(true);
    this->m_OverviewList.SetMainHeader(false);
	
	this->m_OverviewList.DateTimeSetColumnFormat(2,"YYYYMMDDhhmmss","hh':'mm");


	this->m_OverviewList.ResetContent();

	// initialize hidden data grid		
	olHeaderString.LoadString(IDS_STRING116);
	this->m_DetailList.SetHeaderText(olHeaderString);

	olHeaderString.LoadString(IDS_STRING117);
	this->m_DetailList.SetHeaderLengthString(olHeaderString);

	olHeaderString.LoadString(IDS_STRING116);
	this->m_DetailList.SetLogicalFieldList(olHeaderString);

	olHeaderString.LoadString(IDS_STRING133);
	this->m_DetailList.SetColumnWidthString(olHeaderString);

	olHeaderString.LoadString(IDS_STRING134);
	this->m_DetailList.SetColumnAlignmentString(olHeaderString);

	
	this->m_DetailList.ResetContent();
	this->m_DetailList.SetCedaServerName(this->m_UfisComControl.GetServerName());
	this->m_DetailList.SetCedaPort("3357");
	this->m_DetailList.SetCedaHopo(this->m_UfisComControl.GetHomeAirport());
    this->m_DetailList.SetCedaCurrentApplication("Alerter");
    this->m_DetailList.SetCedaTabext("TAB");
    this->m_DetailList.SetCedaUser(this->m_LoginControl.GetUserName_());
    this->m_DetailList.SetCedaWorkstation(this->m_UfisComControl.GetWorkstationName());
    this->m_DetailList.SetCedaSendTimeout(3);
    this->m_DetailList.SetCedaReceiveTimeout(240);

	CString olSeperator((char)10);
    this->m_DetailList.SetCedaRecordSeparator(olSeperator);
    this->m_DetailList.SetCedaIdentifier("Alerter");
    this->m_DetailList.ShowHorzScroller(true);
    this->m_DetailList.EnableHeaderSizing(true);
	CString olFields;
	olFields.LoadString(IDS_STRING116);

	CTime olStartTime = CTime::GetCurrentTime();
	olStartTime -= CTimeSpan(0,this->imLoadTime,0,0);
	this->LocalToUtc(olStartTime);

	CString olWhere;
	olWhere.Format("WHERE CDAT >= '%s' ",olStartTime.Format("%Y%m%d%H%M"));
	olWhere += "ORDER BY CDAT";

	this->m_DetailList.CedaAction("RT","ICSTAB",olFields,"",olWhere);

	this->m_DetailList.IndexCreate("IINTR",5);
	this->m_DetailList.IndexCreate("IURNO",8);
	this->bmDetailListIndexesOK = TRUE;

	return true;
}

bool CAlerterDlg::FillTabs()
{
	CWaitCursor olWait;

	DWORD	llNow,llLast;
	double	flDiff;

	llLast = GetTickCount();

	for (int i = 0; i < this->m_Pages.GetItemCount(); i++)
	{
		TCITEM olItem;
		olItem.mask	   = TCIF_PARAM;
		olItem.lParam  = 0;	
		if (this->m_Pages.GetItem(i,&olItem))
		{
			ERROR_PAGE *polPage = (ERROR_PAGE *)olItem.lParam;
			ASSERT(polPage != NULL);
			polPage->m_DetailList.ResetContent();

			llNow  = GetTickCount();
			flDiff = llNow-llLast;
			llLast = llNow;
			TRACE("Alerter::FillTabs().ResetContent %.3lf sec\n", flDiff/1000.0 );
			
			if (!this->bmDetailListIndexesOK)
			{
				this->m_DetailList.IndexCreate("IINTR",5);
				this->m_DetailList.IndexCreate("IURNO",8);
				this->bmDetailListIndexesOK = TRUE;
			}

			this->m_DetailList.SetInternalLineBuffer(true);
			this->m_DetailList.GetLinesByIndexValue("IINTR",polPage->m_DetailList.GetMyName(),0);			

			llNow  = GetTickCount();
			flDiff = llNow-llLast;
			llLast = llNow;
			TRACE("Alerter::FillTabs().GetLinesByIndexValue %.3lf sec\n", flDiff/1000.0 );

			int ilLines = 0;
			long llLine = this->m_DetailList.GetNextResultLine();
			while (llLine >= 0)
			{
				this->CreateOrUpdateDetailLine(llLine,polPage,false,true);
				llLine = this->m_DetailList.GetNextResultLine();
				++ilLines;
			}
			
			llNow  = GetTickCount();
			flDiff = llNow-llLast;
			llLast = llNow;
			TRACE("Alerter::FillTabs().CreateOrUpdateDetailLine(%d) %.3lf sec\n", ilLines,flDiff/1000.0 );

			polPage->m_DetailList.Refresh();

			llNow  = GetTickCount();
			flDiff = llNow-llLast;
			llLast = llNow;
			TRACE("Alerter::FillTabs().Refresh %.3lf sec\n", flDiff/1000.0 );
			
			CString olHeader;
			olHeader = this->omTabNames[polPage->m_HeaderId];
			this->GetItemInfo(olHeader,polPage->m_OpenItems,polPage->m_ConfirmedItems,polPage->m_ClosedItems);

			llNow  = GetTickCount();
			flDiff = llNow-llLast;
			llLast = llNow;
			TRACE("Alerter::FillTabs().GetItemInfo %.3lf sec\n", flDiff/1000.0 );

			CString olText;
			olText.Format("%s (%ld)",olHeader,polPage->m_OpenItems);
			
			olItem.mask	   = TCIF_TEXT;
			olItem.dwState = 0;
			olItem.dwStateMask = 0;
			olItem.pszText = olText.GetBuffer(0);
			olItem.cchTextMax = olText.GetLength();
			olItem.iImage  = -1;
			olItem.lParam  = NULL;
			this->m_Pages.SetItem(polPage->m_ItemId,&olItem);

			olText.Format("%s (%ld open,%ld confimed,%ld closed)",olHeader,polPage->m_OpenItems,polPage->m_ConfirmedItems,polPage->m_ClosedItems);
			polPage->m_DetailHeader.SetWindowText(olText);	

			llNow  = GetTickCount();
			flDiff = llNow-llLast;
			llLast = llNow;
			TRACE("Alerter::FillTabs().SetWindowText %.3lf sec\n", flDiff/1000.0 );


		}
		this->m_DetailList.SetInternalLineBuffer(false);
	}


	llLast = GetTickCount();

	// fill overview grid
	char pclTimeSpan[256];
	char pclConfigPath[256];

    if (getenv("CEDA") == NULL)
        strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
    else
        strcpy(pclConfigPath, getenv("CEDA"));

	GetPrivateProfileString("Alerter","TimeSpan","5",pclTimeSpan,sizeof(pclTimeSpan),pclConfigPath);
	imTimeSpan = atoi(pclTimeSpan);
	omCurrTime = CTime::GetCurrentTime();
	
	this->m_OverviewList.ResetContent();
	for (long llLine = 0; llLine < this->m_DetailList.GetLineCount(); llLine++)
	{
		this->CreateOrUpdateOverviewLine(llLine,false,true);
	}

	this->m_OverviewList.Refresh();

	llNow  = GetTickCount();
	flDiff = llNow-llLast;
	llLast = llNow;
	TRACE("Alerter::FillTabs().m_OverviewList %.3lf sec\n", flDiff/1000.0 );

	return true;
}

BOOL CAlerterDlg::OnBroadcast(LPCTSTR ReqId, 
							 LPCTSTR Dest1, 
							 LPCTSTR Dest2, 
							 LPCTSTR Cmd, 
							 LPCTSTR Object,
							 LPCTSTR Seq, 
							 LPCTSTR Tws, 
							 LPCTSTR Twe, 
							 LPCTSTR Selection, 
							 LPCTSTR Fields, 
							 LPCTSTR Data, 
							 LPCTSTR BcNum)
{
	if (strcmp(Object,"ICSTAB") == 0)
	{
		omCurrTime = CTime::GetCurrentTime();
		// UTC to be displayed ?
		if (this->m_TimeFormat == 0)
		{
			this->LocalToUtc(omCurrTime);
		}

		if (strcmp(Cmd,"IRT") == 0)
		{
			if (imTimer == -1 && this->GetForegroundWindow() != this)
			{
				imTimer = this->SetTimer(1, (UINT)250, NULL);
			}
			
			if (this->bmDetailListIndexesOK)
			{
				this->m_DetailList.IndexDestroy("IINTR");
				this->m_DetailList.IndexDestroy("IURNO");
				this->bmDetailListIndexesOK = FALSE;
			}

			CString olEmptyLine;
			olEmptyLine.LoadString(IDS_STRING122);
			this->m_DetailList.InsertTextLine(olEmptyLine,false);
			long llLine = this->m_DetailList.GetLineCount() - 1;
			this->m_DetailList.SetFieldValues(llLine,Fields,Data);

			ERROR_PAGE *polPage = this->FindErrorPage(this->m_DetailList.GetFieldValue(llLine,"INTR"));
			if (polPage != NULL)
			{
				CString olStat = this->m_DetailList.GetFieldValue(llLine,"STAT");
				if (olStat == "O")
					++polPage->m_OpenItems;
				else if (olStat == "A")
					++polPage->m_ConfirmedItems;
				else
					++polPage->m_ClosedItems;
			}

			this->CreateOrUpdateDetailLine(llLine,NULL,true);

			this->CreateOrUpdateOverviewLine(llLine,true);


		}
		else if (strcmp(Cmd,"URT") == 0)
		{
			CString olUrno = this->GetUrnoFromSelection(Selection);

			if (!this->bmDetailListIndexesOK)
			{
				this->m_DetailList.IndexCreate("IINTR",5);
				this->m_DetailList.IndexCreate("IURNO",8);
				this->bmDetailListIndexesOK = TRUE;
			}

			this->m_DetailList.SetInternalLineBuffer(true);
			this->m_DetailList.GetLinesByIndexValue("IURNO",olUrno,0);
			long llLine = this->m_DetailList.GetNextResultLine();
			if (llLine > -1)
			{
				ERROR_PAGE *polPage = this->FindErrorPage(this->m_DetailList.GetFieldValue(llLine,"INTR"));
				if (polPage != NULL)
				{
					CString olStat = this->m_DetailList.GetFieldValue(llLine,"STAT");
					if (olStat == "O")
						--polPage->m_OpenItems;
					else if (olStat == "A")
						--polPage->m_ConfirmedItems;
					else
						--polPage->m_ClosedItems;
				}
							

				this->m_DetailList.SetFieldValues(llLine,Fields,Data);

				if (polPage != NULL)
				{
					CString olStat = this->m_DetailList.GetFieldValue(llLine,"STAT");
					if (olStat == "O")
						++polPage->m_OpenItems;
					else if (olStat == "A")
						++polPage->m_ConfirmedItems;
					else
						++polPage->m_ClosedItems;
				}


				this->CreateOrUpdateDetailLine(llLine,NULL,true);

				this->CreateOrUpdateOverviewLine(llLine,true);
			}
		}
	}
	else if (strcmp(Cmd,"CLO") == 0)
	{
		CString olMsg;
		olMsg.LoadString(IDS_STRING123);
		AfxMessageBox(olMsg);
		::PostQuitMessage(0);
	}
	return TRUE;
}

void CAlerterDlg::OnSelchangeTabPages(NMHDR* pNMHDR, LRESULT* pResult) 
{
	// TODO: Add your control notification handler code here
	int ilSel = this->m_Pages.GetCurSel();
	if (ilSel > -1)
	{
		TCITEM olItem;
		olItem.mask	   = TCIF_PARAM;
		olItem.lParam  = 0;	
		if (this->m_Pages.GetItem(ilSel,&olItem))
		{
			ERROR_PAGE *polPage = (ERROR_PAGE *)olItem.lParam;
			ASSERT(polPage != NULL);
			this->m_resizeHelper.OnSize();			
		}
	}

	*pResult = 0;
}

void CAlerterDlg::OnHelp() 
{

	char File[64];


	char pclConfigPath[256];

    if (getenv("CEDA") == NULL)
        strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
    else
        strcpy(pclConfigPath, getenv("CEDA"));




	CString olAppName = "ALERTER";


	strcpy(File, olAppName);
	strcat(File, ".HLP");

	CWnd *pwnd = AfxGetMainWnd();
	
	ASSERT(pwnd);

	CString omFileName;
	CString omPath;
	char pclTmpText[512];

	GetPrivateProfileString("GLOBAL", "HELPDIRECTORY", "c:\\Ufis\\Help",pclTmpText,sizeof pclTmpText, pclConfigPath );
	omFileName = pclTmpText;												
	if(!omFileName.IsEmpty())
	{
		GetPrivateProfileString(olAppName, "HELPFILE", olAppName + ".chm",pclTmpText,sizeof pclTmpText, pclConfigPath );
		if (!omFileName.IsEmpty())
		{
			if (omFileName[omFileName.GetLength() - 1] != '\\')
				omFileName += '\\';
			omPath = omFileName;												
			omPath += "HH.exe";												
			omFileName += pclTmpText;
			char *args[4];
			args[0] = "child";
			args[1] = omFileName.GetBuffer(0);
			args[2] = NULL;
			args[3] = NULL;
//			int ilReturn = _spawnv(_P_NOWAIT,"C:\\Winnt\\HH.exe",args);
			int ilReturn = _spawnv(_P_NOWAIT,omPath,args); 
		}
	}



	// TODO: Add your control notification handler code here
	//WinHelp(0);
}

void CAlerterDlg::OnConfirm() 
{
	// TODO: Add your control notification handler code here
	ChangeStatus("O","","A");
}

void CAlerterDlg::OnReopen() 
{
	// TODO: Add your control notification handler code here
	ChangeStatus("C","A","O");
}

void CAlerterDlg::OnSelchangingTabPages(NMHDR* pNMHDR, LRESULT* pResult) 
{
	// TODO: Add your control notification handler code here
	int ilSel = this->m_Pages.GetCurSel();
	if (ilSel > -1)
	{
	}
	*pResult = 0;
}


CAlerterDlg::ERROR_PAGE	*CAlerterDlg::FindErrorPage(const CString& ropIntr)
{
	for (int i = 0; i < this->m_Pages.GetItemCount(); i++)
	{
		TCITEM olItem;
		olItem.mask	   = TCIF_PARAM;
		olItem.lParam  = 0;	
		if (this->m_Pages.GetItem(i,&olItem))
		{
			ERROR_PAGE *polPage = (ERROR_PAGE *)olItem.lParam;
			if (polPage->m_DetailList.GetMyName() == ropIntr)
				return polPage;
		}
	}

	return NULL;
}

long CAlerterDlg::FindLine(CTAB& ropTab,const CString& ropUrno)
{
	for (long llLine = 0; llLine < ropTab.GetLineCount(); llLine++)
	{
		if (ropTab.GetLineTag(llLine) == ropUrno)
			return llLine;
	}

	return -1;
}


CString	CAlerterDlg::GetConnectionState(const CString& ropCons)
{
	static const  int	sclText[]	= { IDS_STRING126,IDS_STRING127,IDS_STRING128,0 };
	static const  char *sclState[]  = { "D","U","E",NULL };

	CString olText;
	const char **pilState = sclState;
	const int *pilText   = sclText;
	while(*pilState != NULL)
	{
		if (*pilState++ == ropCons)
		{
			olText.LoadString(*pilText);
			return olText;
		}
		++pilText;
	}

	return olText;
}

CString	CAlerterDlg::GetErrorState(const CString& ropCons)
{
	static const  int	sclText[]	= { IDS_STRING129,IDS_STRING130,IDS_STRING131,0 };
	static const  char *sclState[]  = { "O","A","C",NULL };

	CString olText;
	const char **pilState = sclState;
	const int *pilText   = sclText;
	while(*pilState != NULL)
	{
		if (*pilState++ == ropCons)
		{
			olText.LoadString(*pilText);
			return olText;
		}
		++pilText;
	}

	return olText;
}

bool CAlerterDlg::CreateOrUpdateDetailLine(long lpLine,ERROR_PAGE *popPage,bool bpRefresh,bool bpCreate)
{
#ifdef	PERF_CHECK
	DWORD	llNow,llLast;
	double	flDiff;

	llLast = GetTickCount();
#endif
	if (popPage == NULL)
	{
		popPage = FindErrorPage(this->m_DetailList.GetFieldValue(lpLine,"INTR"));
		if (popPage == NULL)
			return false;
	}

#ifdef	PERF_CHECK
	llNow  = GetTickCount();
	flDiff = llNow-llLast;
	llLast = llNow;
	TRACE("Alerter::CreateOrUpdateDetailLine().FindErrorPage %.3lf sec\n", flDiff/1000.0 );
#endif

	CString olUrno = this->m_DetailList.GetFieldValue(lpLine,"URNO");

#ifdef	PERF_CHECK
	llNow  = GetTickCount();
	flDiff = llNow-llLast;
	llLast = llNow;
	TRACE("Alerter::CreateOrUpdateDetailLine().GetFieldValue %.3lf sec\n", flDiff/1000.0 );
#endif

	long llLine = -1;
	if (!bpCreate)
		llLine = FindLine(popPage->m_DetailList,olUrno);

#ifdef	PERF_CHECK
	llNow  = GetTickCount();
	flDiff = llNow-llLast;
	llLast = llNow;
	TRACE("Alerter::CreateOrUpdateDetailLine().FindLine %.3lf sec\n", flDiff/1000.0 );
#endif

	if (llLine < 0)	// IRT
	{
		CString olEmptyLine;
		olEmptyLine.LoadString(IDS_STRING124);
		popPage->m_DetailList.InsertTextLineAt(0,olEmptyLine,false);

		llLine = 0;
		popPage->m_DetailList.SetLineTag(llLine,olUrno);
	}

#ifdef	PERF_CHECK
	llNow  = GetTickCount();
	flDiff = llNow-llLast;
	llLast = llNow;
	TRACE("Alerter::CreateOrUpdateDetailLine().InsertTextLineAt %.3lf sec\n", flDiff/1000.0 );
#endif

	popPage->m_DetailList.SetFieldValues(llLine,"CONS",GetConnectionState(this->m_DetailList.GetFieldValue(lpLine,"CONS")));

#ifdef	PERF_CHECK
	llNow  = GetTickCount();
	flDiff = llNow-llLast;
	llLast = llNow;
	TRACE("Alerter::CreateOrUpdateDetailLine().SetFieldValues %.3lf sec\n", flDiff/1000.0 );
#endif

	if (this->m_TimeFormat == 0)	// UCS
	{
		CString olCont = this->m_DetailList.GetFieldValue(lpLine,"CONT");
		if (olCont.GetLength() > 1) 
			popPage->m_DetailList.SetFieldValues(llLine,"CONT",olCont);
		popPage->m_DetailList.SetFieldValues(llLine,"CDAT",this->m_DetailList.GetFieldValue(lpLine,"CDAT"));
	}
	else
	{
		// get connection time in UCS
		CString olCont = this->m_DetailList.GetFieldValue(lpLine,"CONT");
		if (olCont.GetLength() > 1) 
		{
			CTime olContt = ::DBStringToDateTime(olCont);
			this->UtcToLocal(olContt);
			popPage->m_DetailList.SetFieldValues(llLine,"CONT",::CTimeToDBString(olContt,olContt));
		}

		// get error time in UCS
		CTime olCdat = ::DBStringToDateTime(this->m_DetailList.GetFieldValue(lpLine,"CDAT"));
		this->UtcToLocal(olCdat);
		popPage->m_DetailList.SetFieldValues(llLine,"CDAT",::CTimeToDBString(olCdat,olCdat));
	}

#ifdef	PERF_CHECK
	llNow  = GetTickCount();
	flDiff = llNow-llLast;
	llLast = llNow;
	TRACE("Alerter::CreateOrUpdateDetailLine().SetFieldValues2 %.3lf sec\n", flDiff/1000.0 );
#endif

	popPage->m_DetailList.SetFieldValues(llLine,"ERRM",this->m_DetailList.GetFieldValue(lpLine,"ERRM"));
	popPage->m_DetailList.SetFieldValues(llLine,"STAT",GetErrorState(this->m_DetailList.GetFieldValue(lpLine,"STAT")));
	popPage->m_DetailList.SetFieldValues(llLine,"DATA",this->m_DetailList.GetFieldValue(lpLine,"DATA"));

#ifdef	PERF_CHECK
	llNow  = GetTickCount();
	flDiff = llNow-llLast;
	llLast = llNow;
	TRACE("Alerter::CreateOrUpdateDetailLine().SetFieldValues3 %.3lf sec\n", flDiff/1000.0 );
#endif

	long llTxtColor;
	long llBkColor;

	popPage->m_DetailList.GetLineColor(llLine,&llTxtColor,&llBkColor);
	CString olStatus = this->m_DetailList.GetFieldValue(lpLine,"STAT");

	if (olStatus == "O")	// open alert
	{
		llBkColor = RGB(255,136,136);	// red
		popPage->m_DetailList.SetLineColor(llLine,llTxtColor,llBkColor);
	}
	else if (olStatus == "A")	// accepted (confirmed)
	{
		llBkColor = RGB(255,255,0);	// yellow
		popPage->m_DetailList.SetLineColor(llLine,llTxtColor,llBkColor);
	}
	else if (olStatus == "C")	// closed
	{
		llBkColor = RGB(128,255,0);	// green
		popPage->m_DetailList.SetLineColor(llLine,llTxtColor,llBkColor);
	}

#ifdef	PERF_CHECK
	llNow  = GetTickCount();
	flDiff = llNow-llLast;
	llLast = llNow;
	TRACE("Alerter::CreateOrUpdateDetailLine().SetFieldValues4 %.3lf sec\n", flDiff/1000.0 );
#endif

	if (bpRefresh)
	{
		popPage->m_DetailList.Refresh();

		CString olHeader;
		olHeader = this->omTabNames[popPage->m_HeaderId];

		CString olText;
		olText.Format("%s (%ld)",olHeader,popPage->m_OpenItems);
		
		TCITEM olItem;

		olItem.mask	   = TCIF_TEXT;
		olItem.dwState = 0;
		olItem.dwStateMask = 0;
		olItem.pszText = olText.GetBuffer(0);
		olItem.cchTextMax = olText.GetLength();
		olItem.iImage  = -1;
		olItem.lParam  = NULL;
		this->m_Pages.SetItem(popPage->m_ItemId,&olItem);

		olText.Format("%s (%ld open,%ld confimed,%ld closed)",olHeader,popPage->m_OpenItems,popPage->m_ConfirmedItems,popPage->m_ClosedItems);
		popPage->m_DetailHeader.SetWindowText(olText);	

	}

#ifdef	PERF_CHECK
	llNow  = GetTickCount();
	flDiff = llNow-llLast;
	llLast = llNow;
	TRACE("Alerter::CreateOrUpdateDetailLine().Refresh %.3lf sec\n", flDiff/1000.0 );
#endif

	return true;													
}

bool CAlerterDlg::CreateOrUpdateOverviewLine(long lpLine, bool bpRefresh,bool bpCreate)
{
	// get error time in UCS
	CTime olCdat = ::DBStringToDateTime(this->m_DetailList.GetFieldValue(lpLine,"CDAT"));

	CTime olPrevTime = omCurrTime - CTimeSpan(0,0,imTimeSpan,0);
	
	bool blInsideFrame = olCdat >= olPrevTime && olCdat <= omCurrTime;

	CString olUrno = this->m_DetailList.GetFieldValue(lpLine,"URNO");

	long llLine = -1;
	if (!bpCreate)
		llLine = FindLine(this->m_OverviewList,olUrno);

	if (llLine >= 0 || (blInsideFrame == true || bpRefresh == true))
	{
		if (llLine < 0)	// IRT
		{
			CString olEmptyLine;
			olEmptyLine.LoadString(IDS_STRING124);
			this->m_OverviewList.InsertTextLineAt(0,olEmptyLine,false);
			llLine = 0;
			this->m_OverviewList.SetLineTag(llLine,olUrno);

		}

		this->m_OverviewList.SetFieldValues(llLine,"INTR",this->m_DetailList.GetFieldValue(lpLine,"INTR"));
		this->m_OverviewList.SetFieldValues(llLine,"ERRM",this->m_DetailList.GetFieldValue(lpLine,"ERRM"));
		this->m_OverviewList.SetFieldValues(llLine,"DATA",this->m_DetailList.GetFieldValue(lpLine,"DATA"));

		if (this->m_TimeFormat == 0)	// UCS
		{
			this->m_OverviewList.SetFieldValues(llLine,"CDAT",this->m_DetailList.GetFieldValue(lpLine,"CDAT"));
		}
		else
		{
			this->UtcToLocal(olCdat);
			this->m_OverviewList.SetFieldValues(llLine,"CDAT",::CTimeToDBString(olCdat,olCdat));
		}

		this->m_OverviewList.SetFieldValues(llLine,"STAT",GetErrorState(this->m_DetailList.GetFieldValue(lpLine,"STAT")));
		
		long llTxtColor;
		long llBkColor;

		this->m_OverviewList.GetLineColor(llLine,&llTxtColor,&llBkColor);
		CString olStatus = this->m_DetailList.GetFieldValue(lpLine,"STAT");

		if (olStatus == "O")	// open alert
		{
			llBkColor = RGB(255,136,136);	// red
			this->m_OverviewList.SetLineColor(llLine,llTxtColor,llBkColor);
		}
		else if (olStatus == "A")	// accepted (confirmed)
		{
			llBkColor = RGB(255,255,0);	// yellow
			this->m_OverviewList.SetLineColor(llLine,llTxtColor,llBkColor);
		}
		else if (olStatus == "C")	// closed
		{
			llBkColor = RGB(128,255,0);	// green
			this->m_OverviewList.SetLineColor(llLine,llTxtColor,llBkColor);
		}

		if (bpRefresh)
		{
			this->m_OverviewList.Refresh();
		}
		return true;													
	}
	else
		return false;
}

CString CAlerterDlg::GetUrnoFromSelection(CString opSelection)
{
	// bch 18.03.2003 PRF 3991 -> replaced the code commented out below because
	// it does not work when the selection does not contain speechmarks
	// for example: "WHERE URNO = 119675853"
	CString olUrno;
	for(int i = 0; i < opSelection.GetLength(); i++)
	{
		// remove all non-numeric characters
		if(opSelection[i] >= '0' && opSelection[i] <= '9')
		{
			olUrno += opSelection[i];
		}
	}
	return olUrno;
}


void CAlerterDlg::OnRowSelectionChangedTabctrl1(long lpLineNo, BOOL bpSelected) 
{
	// TODO: Add your control notification handler code here
}

BOOL CAlerterDlg::OnRowSelectionChanged(UINT nID,long lpLineNo, BOOL bpSelected) 
{
	// TODO: Add your control notification handler code here
	
	return TRUE;
}


void CAlerterDlg::OnRadioLocal() 
{
	// TODO: Add your control notification handler code here
	DWORD	llNow,llLast;
	double	flDiff;

	llLast = GetTickCount();

	UpdateData(true);
	FillTabs();

	llNow  = GetTickCount();
	flDiff = llNow-llLast;
	llLast = llNow;
	TRACE("Alerter::OnRadioLocal() %.3lf sec\n", flDiff/1000.0 );

}

void CAlerterDlg::OnRadioUcs() 
{
	// TODO: Add your control notification handler code here
	DWORD	llNow,llLast;
	double	flDiff;

	llLast = GetTickCount();

	UpdateData(true);
	FillTabs();
	
	llNow  = GetTickCount();
	flDiff = llNow-llLast;
	llLast = llNow;
	TRACE("Alerter::OnRadioUcs() %.3lf sec\n", flDiff/1000.0 );
}

void CAlerterDlg::LocalToUtc(CTime& ropTime)
{
	if(ropTime != -1)
	{
		if(ropTime < omTich)
		{
			ropTime -= omLocalDiff1;
		}
		else
		{
			ropTime -= omLocalDiff2;
		}
	}
				
}

void CAlerterDlg::UtcToLocal(CTime& ropTime)
{
	CTime olTichUtc;
	olTichUtc = omTich - CTimeSpan(omLocalDiff2);

	if(ropTime != -1)
	{
		if(ropTime < olTichUtc)
		{
			ropTime += omLocalDiff1;
		}
		else
		{
			CString olFromDate;
			olFromDate.Format("%04d%02d%02d%02d%02d", omTich.GetYear(), omTich.GetMonth(), omTich.GetDay(), omTich.GetHour()-2, omTich.GetMinute());
			CString olToDate;
			olToDate.Format("%04d%02d%02d%02d%02d", omTich.GetYear(), omTich.GetMonth(), omTich.GetDay(), omTich.GetHour()-1, omTich.GetMinute());
			CString olOpDate = ropTime.Format("%Y%m%d%H%M");
			if(olOpDate >= olFromDate && olOpDate <= olToDate)
			{
				ropTime += omLocalDiff1;
			}
			else
			{
				ropTime += omLocalDiff2;
			}
		}
	}

}

BOOL CAlerterDlg::SetLocalDiff()
{
	BOOL	blResult = TRUE;
	int		ilTdi1;
	int		ilTdi2; 
	int		ilMin;
	int		ilHour;
	CTime	olTich ;	
	CTime	olCurr;

	// especially for working with server SIN1 in HEU
	char pclUseMesz[256];
	char pclConfigPath[256];

    if (getenv("CEDA") == NULL)
        strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
    else
        strcpy(pclConfigPath, getenv("CEDA"));

	GetPrivateProfileString("Alerter","USEMESZ","FALSE",pclUseMesz,sizeof(pclUseMesz),pclConfigPath);
	if (stricmp(pclUseMesz,"TRUE") == 0)
	{
		omTich = CTime(2003,03,30,01,00,00);
		omLocalDiff1 = CTimeSpan(0,0, 60,0);
		omLocalDiff2 = CTimeSpan(0,0,120,0);
		return TRUE;
	}

	CString olWhere = CString("WHERE APC3 = '") + this->omHomeAirport + CString("'");

	this->m_UfisComControl.CallServer("RT","APTTAB","TICH,TDI1,TDI2","",olWhere,"240");
	for (int i = 0; i < this->m_UfisComControl.GetBufferCount(); i++)
	{
		CString olData = this->m_UfisComControl.GetBufferLine(i);
		CStringArray olDataList;
		::ExtractItemList(olData,&olDataList,',');

		CString olTime = olDataList[0] +"00";
		olTich = DBStringToDateTime(olTime);
		if (olTich == -1)
		{
			blResult = FALSE;
		}
		else
		{
			omTich = olTich;
		}

		if (strlen(olDataList[1]) > 0)
		{
			ilTdi1 = atoi(olDataList[1]);
			ilHour = ilTdi1 / 60;
			ilMin  = ilTdi1 % 60;
			omLocalDiff1 = CTimeSpan(0,ilHour ,ilMin ,0);
		}
		else
		{
			blResult = FALSE;
		}

		if (strlen(olDataList[2]) > 0)
		{
			ilTdi2 = atoi(olDataList[2]);
			ilHour = ilTdi2 / 60;
			ilMin  = ilTdi2 % 60;
			omLocalDiff2 = CTimeSpan(0,ilHour ,ilMin ,0);
		}
		else
		{
			blResult = FALSE;
		}
	}


	return blResult;
}

bool CAlerterDlg::ChangeStatus(const CString& ropOldStatus,const CString& ropOldStatus2,const CString& ropNewStatus)
{
	long llLine = this->m_OverviewList.GetCurrentSelected();
	if (llLine >= 0)
	{
		CString olUrno = this->m_OverviewList.GetLineTag(llLine);

		if (!this->bmDetailListIndexesOK)
		{
			this->m_DetailList.IndexCreate("IINTR",5);
			this->m_DetailList.IndexCreate("IURNO",8);
			this->bmDetailListIndexesOK = TRUE;
		}

		this->m_DetailList.SetInternalLineBuffer(true);
		this->m_DetailList.GetLinesByIndexValue("IURNO",olUrno,0);
		llLine = this->m_DetailList.GetNextResultLine();
		if (llLine >= 0)
		{
			CString olStat = this->m_DetailList.GetFieldValue(llLine,"STAT");
			if (olStat == ropOldStatus || olStat == ropOldStatus2)	// open ?
			{
				if (!WriteStatusChanges(llLine,ropNewStatus))
				{
					AfxMessageBox(IDS_STRING132,MB_ICONSTOP|MB_OK);
					return false;
				}
			}
		}
	}

	int ilSel = this->m_Pages.GetCurSel();
	if (ilSel > -1)
	{
		TCITEM olItem;
		olItem.mask	   = TCIF_PARAM;
		olItem.lParam  = 0;	
		if (this->m_Pages.GetItem(ilSel,&olItem))
		{
			ERROR_PAGE *polPage = (ERROR_PAGE *)olItem.lParam;
			ASSERT(polPage != NULL);
			llLine = polPage->m_DetailList.GetCurrentSelected();
			if (llLine >= 0)
			{
				CString olUrno = polPage->m_DetailList.GetLineTag(llLine);

				if (!this->bmDetailListIndexesOK)
				{
					this->m_DetailList.IndexCreate("IINTR",5);
					this->m_DetailList.IndexCreate("IURNO",8);
					this->bmDetailListIndexesOK = TRUE;
				}

				this->m_DetailList.SetInternalLineBuffer(true);
				this->m_DetailList.GetLinesByIndexValue("IURNO",olUrno,0);
				llLine = this->m_DetailList.GetNextResultLine();
				if (llLine >= 0)
				{
					CString olStat = this->m_DetailList.GetFieldValue(llLine,"STAT");
					if (olStat == ropOldStatus || olStat == ropOldStatus2)	// open ?
					{
						if (!WriteStatusChanges(llLine,ropNewStatus))
						{
							AfxMessageBox(IDS_STRING132,MB_ICONSTOP|MB_OK);
							return false;
						}
					}
				}
			}
		}
	}

	return true;
}

bool CAlerterDlg::ChangeStatus(CTAB *popList,long lpLine,long lpColumn,const CString& ropOldStatus,const CString& ropOldStatus2,const CString& ropNewStatus)
{
	CString olUrno = popList->GetLineTag(lpLine);

	if (!this->bmDetailListIndexesOK)
	{
		this->m_DetailList.IndexCreate("IINTR",5);
		this->m_DetailList.IndexCreate("IURNO",8);
		this->bmDetailListIndexesOK = TRUE;
	}

	this->m_DetailList.SetInternalLineBuffer(true);
	this->m_DetailList.GetLinesByIndexValue("IURNO",olUrno,0);
	long llLine = this->m_DetailList.GetNextResultLine();
	if (llLine >= 0)
	{
		CString olStat = this->m_DetailList.GetFieldValue(llLine,"STAT");
		if (olStat == ropOldStatus || olStat == ropOldStatus2)	// open ?
		{
			if (!WriteStatusChanges(llLine,ropNewStatus))
			{
				AfxMessageBox(IDS_STRING132,MB_ICONSTOP|MB_OK);
				return false;
			}
		}
	}

	return true;
}

bool CAlerterDlg::WriteStatusChanges(long lpLine,const CString& ropStatus)
{
	CString olUrno  = this->m_DetailList.GetFieldValue(lpLine,"URNO");

	CString olWhere = CString("WHERE URNO = '") + olUrno + CString("'");

	CTime olTime = CTime::GetCurrentTime();
	this->LocalToUtc(olTime);

	CString olData = olUrno;
	olData += ',';
	olData += ropStatus;
	olData += ',';
	olData += ::CTimeToDBString(olTime,olTime);
	olData += ',';
	olData += m_LoginControl.GetUserName_();

	return this->m_UfisComControl.CallServer("URT","ICSTAB","URNO,STAT,LSTU,USEU",olData,olWhere,"240") == 0;
}


void CAlerterDlg::OnCloseStatus() 
{
	// TODO: Add your control notification handler code here
	ChangeStatus("O","A","C");
	
}

void CAlerterDlg::OnSendRButtonClickTabctrl1(long LineNo, long ColNo) 
{
	// TODO: Add your control notification handler code here
	CPoint olPoint;
	::GetCursorPos(&olPoint);

	this->pomSelTab = &this->m_OverviewList;
	this->lmSelLine = LineNo;
	this->lmSelColumn = ColNo;

	CString olUrno = this->pomSelTab->GetLineTag(LineNo);

	if (!this->bmDetailListIndexesOK)
	{
		this->m_DetailList.IndexCreate("IINTR",5);
		this->m_DetailList.IndexCreate("IURNO",8);
		this->bmDetailListIndexesOK = TRUE;
	}

	this->m_DetailList.SetInternalLineBuffer(true);
	this->m_DetailList.GetLinesByIndexValue("IURNO",olUrno,0);
	long llLine = this->m_DetailList.GetNextResultLine();
	if (llLine >= 0)
	{
		CString olStat = this->m_DetailList.GetFieldValue(llLine,"STAT");

		CMenu olMenu;
		olMenu.CreatePopupMenu();
		CString olText;

		olText.LoadString(IDS_STRING140);	// Confirm
		olMenu.AppendMenu(MF_STRING,IDS_STRING140, olText);

		olText.LoadString(IDS_STRING141);	// Close
		olMenu.AppendMenu(MF_STRING,IDS_STRING141, olText);

		olText.LoadString(IDS_STRING142);	// Reopen
		olMenu.AppendMenu(MF_STRING,IDS_STRING142, olText);

		if (olStat == "O")
			olMenu.EnableMenuItem(IDS_STRING142,MF_BYCOMMAND|MF_GRAYED);
		else if (olStat == "A")
			olMenu.EnableMenuItem(IDS_STRING140,MF_BYCOMMAND|MF_GRAYED);
		else
		{
			olMenu.EnableMenuItem(IDS_STRING140,MF_BYCOMMAND|MF_GRAYED);
			olMenu.EnableMenuItem(IDS_STRING141,MF_BYCOMMAND|MF_GRAYED);
		}
			
		olMenu.TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, olPoint.x, olPoint.y, this, NULL);
	}	
}

BOOL CAlerterDlg::OnSendRButtonClick(UINT nID ,long LineNo, long ColNo) 
{
	// TODO: Add your control notification handler code here
	CPoint olPoint;
	::GetCursorPos(&olPoint);

	int ilSel = this->m_Pages.GetCurSel();
	if (ilSel > -1)
	{
		TCITEM olItem;
		olItem.mask	   = TCIF_PARAM;
		olItem.lParam  = 0;	
		if (this->m_Pages.GetItem(ilSel,&olItem))
		{
			ERROR_PAGE *polPage = (ERROR_PAGE *)olItem.lParam;
			ASSERT(polPage != NULL);

			this->pomSelTab = &polPage->m_DetailList;
			this->lmSelLine = LineNo;
			this->lmSelColumn = ColNo;

			CString olUrno = this->pomSelTab->GetLineTag(LineNo);
			if (!this->bmDetailListIndexesOK)
			{
				this->m_DetailList.IndexCreate("IINTR",5);
				this->m_DetailList.IndexCreate("IURNO",8);
				this->bmDetailListIndexesOK = TRUE;
			}

			this->m_DetailList.SetInternalLineBuffer(true);
			this->m_DetailList.GetLinesByIndexValue("IURNO",olUrno,0);
			long llLine = this->m_DetailList.GetNextResultLine();
			if (llLine >= 0)
			{
				CString olStat = this->m_DetailList.GetFieldValue(llLine,"STAT");

				CMenu olMenu;
				olMenu.CreatePopupMenu();
				CString olText;

				olText.LoadString(IDS_STRING140);	// Confirm
				olMenu.AppendMenu(MF_STRING,IDS_STRING140, olText);

				olText.LoadString(IDS_STRING141);	// Close
				olMenu.AppendMenu(MF_STRING,IDS_STRING141, olText);

				olText.LoadString(IDS_STRING142);	// Reopen
				olMenu.AppendMenu(MF_STRING,IDS_STRING142, olText);

				if (olStat == "O")
					olMenu.EnableMenuItem(IDS_STRING142,MF_BYCOMMAND|MF_GRAYED);
				else if (olStat == "A")
					olMenu.EnableMenuItem(IDS_STRING140,MF_BYCOMMAND|MF_GRAYED);
				else
				{
					olMenu.EnableMenuItem(IDS_STRING140,MF_BYCOMMAND|MF_GRAYED);
					olMenu.EnableMenuItem(IDS_STRING141,MF_BYCOMMAND|MF_GRAYED);
				}

				olMenu.TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, olPoint.x, olPoint.y, this, NULL);
			}
		}
	}

	
	return FALSE;
}

void CAlerterDlg::OnMenuConfirm()
{
	ChangeStatus(this->pomSelTab,this->lmSelLine,this->lmSelColumn,"O","","A");		
}

void CAlerterDlg::OnMenuClose()
{
	ChangeStatus(this->pomSelTab,this->lmSelLine,this->lmSelColumn,"O","A","C");		
}

void CAlerterDlg::OnMenuReopen()
{
	ChangeStatus(this->pomSelTab,this->lmSelLine,this->lmSelColumn,"C","A","O");		
}

void CAlerterDlg::PlaySound()
{
	static bool blFirst = true;
	static CString olSoundName;

	if (blFirst)
	{
		char pclSoundFile[256];
		char pclConfigPath[256];

		if (getenv("CEDA") == NULL)
			strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
		else
			strcpy(pclConfigPath, getenv("CEDA"));

		GetPrivateProfileString("Alerter","SOUNDFILE","NONE",pclSoundFile,sizeof(pclSoundFile),pclConfigPath);
		if (stricmp(pclSoundFile,"NONE") != 0)
		{
			olSoundName = pclSoundFile;			
		}
		
	}
	
	if (olSoundName.GetLength() > 0)
		::PlaySound(olSoundName,NULL,SND_FILENAME|SND_NOSTOP|SND_NOWAIT);	
}

void CAlerterDlg::OnTimer(UINT nIDEvent) 
{
	// TODO: Add your message handler code here and/or call default
	if (nIDEvent == this->imTimer)
	{
		this->FlashWindow(TRUE);
		this->PlaySound();
	}
	
	CDialog::OnTimer(nIDEvent);
}

void CAlerterDlg::OnShowWindow(BOOL bShow, UINT nStatus) 
{
	CDialog::OnShowWindow(bShow, nStatus);
	
	// TODO: Add your message handler code here
	
}

void CAlerterDlg::OnSize(UINT nType, int cx, int cy) 
{
	CDialog::OnSize(nType, cx, cy);
	
	// TODO: Add your message handler code here
	m_resizeHelper.OnSize();
}

void CAlerterDlg::OnActivate( UINT nState, CWnd* pWndOther, BOOL bMinimized )
{
	CDialog::OnActivate(nState, pWndOther, bMinimized);

	if ((nState == WA_ACTIVE || nState == WA_CLICKACTIVE) && imTimer != -1)
	{

		if (KillTimer(imTimer))
		{
			this->FlashWindow(FALSE);
//			this->SetActiveWindow();
			imTimer = -1;

		}
	}

}

void CAlerterDlg::GetItemInfo(const CString& ropIntr,long& rlpOpenItems,long& rlpConfirmedItems,long& rlpClosedItems)
{
	rlpOpenItems	  = 0;
	rlpConfirmedItems = 0;
	rlpClosedItems	  = 0;

	if (!this->bmDetailListIndexesOK)
	{
		this->m_DetailList.IndexCreate("IINTR",5);
		this->m_DetailList.IndexCreate("IURNO",8);
		this->bmDetailListIndexesOK = TRUE;
	}

	this->m_DetailList.SetInternalLineBuffer(true);
	this->m_DetailList.GetLinesByIndexValue("IINTR",ropIntr,0);
	long llLine = this->m_DetailList.GetNextResultLine();
	while (llLine >= 0)
	{
		CString olStat = this->m_DetailList.GetFieldValue(llLine,"STAT");
		if (olStat == "O")
			++rlpOpenItems;
		else if (olStat == "A")
			++rlpConfirmedItems;
		else 
			++rlpClosedItems;

		llLine = this->m_DetailList.GetNextResultLine();
	}
}

void CAlerterDlg::OnOnHScroll(UINT nID,long ColNo) 
{
	// TODO: Add your control notification handler code here
	int ilSel = this->m_Pages.GetCurSel();
	if (ilSel > -1)
	{
		TCITEM olItem;
		olItem.mask	   = TCIF_PARAM;
		olItem.lParam  = 0;	
		if (this->m_Pages.GetItem(ilSel,&olItem))
		{
			ERROR_PAGE *polPage = (ERROR_PAGE *)olItem.lParam;
			ASSERT(polPage != NULL);
		}
	}
}
