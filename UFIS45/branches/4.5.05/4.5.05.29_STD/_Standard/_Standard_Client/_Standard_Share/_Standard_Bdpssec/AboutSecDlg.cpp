#include <stdafx.h>
#include <AboutSecDlg.h>
#include <VersionInfo.h>
#include <resrc1.h>
#include <LibGlobl.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


/////////////////////////////////////////////////////////////////////////////
// CAboutSecDlg dialog used for App About


CAboutSecDlg::CAboutSecDlg() : CDialog(CAboutSecDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutSecDlg)
	//}}AFX_DATA_INIT
}

void CAboutSecDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutSecDlg)
	DDX_Control(pDX, IDC_CLASSLIBVERSION, m_ClassLibVersion);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutSecDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutSecDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

BOOL CAboutSecDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	VersionInfo rlInfo;
	VersionInfo::GetVersionInfo(AfxGetInstanceHandle(),rlInfo);
	
	CWnd *pWnd = GetDlgItem(IDC_UFIS_VERSION);
	if (pWnd)
	{
		pWnd->SetWindowText(rlInfo.omProductVersion);
	}

	pWnd = GetDlgItem(IDC_BDPSSEC_VERSION);
	if (pWnd)
	{
		CString olVersionString;
		olVersionString.Format("Version: %s %s  Compiled: %s",rlInfo.omFileVersion,rlInfo.omSpecialBuild,__DATE__);

		pWnd->SetWindowText(olVersionString);
	}

	pWnd = GetDlgItem(IDC_COPYRIGHT);
	if (pWnd)
	{
		pWnd->SetWindowText(rlInfo.omLegalCopyright);
	}

	m_ClassLibVersion.SetWindowText(CString(pcgClassLibVersion));
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}
