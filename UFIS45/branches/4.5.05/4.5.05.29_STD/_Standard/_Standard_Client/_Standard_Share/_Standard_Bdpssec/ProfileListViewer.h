// FuncTableViewer.h
//
#ifndef __PROFILELISTVIEWER_H__
#define __PROFILELISTVIEWER_H__

#include <CCSPtrArray.h>
#include <CCSTable.h>
#include <CedaSecData.h>
#include <CedaPrvData.h>
#include <CedaFktData.h>


typedef enum 
{ 
	VALIDFINALPROFILE = 0,
	INVALIDFINALPROFILE,
	VALIDGROUP, 
	INVALIDGROUP, 
	VALIDPROFILE, 
	INVALIDPROFILE, 
	ERRORRECURSION 
} ImageType;

struct PROFILELISTOBJECT
{
	SECDATA *Sec; // SEC rec of Application in Prv
	PRVDATA *Prv; // PRV rec and - 
	FKTDATA *Fkt; // - corresponding FKT rec
};
typedef struct PROFILELISTOBJECT ProfileListObject;


struct PROFILELISTITEM
{
	char	FREL[URNOLEN];
	char	TYPE[TYPELEN];  // P = Profile (single profile) G = Group (Mixture of profiles)
	char	NAME[100];		// Profile or Group Name
	char	DESC[200];		// Validity
	int		Level;			// indentation in the profile list
	ImageType ImageType;	// Icon displayed
	bool	IsPersonalProfile;
	bool	IsValid;
	CMapStringToPtr FrelList; // list of all Frels below this one (in Children)
	CCSPtrArray <ProfileListObject> Objects; // PRV recs for this profile
	CMapStringToPtr FfktMap; // Map into Objects ordered by Ffkt
	CCSPtrArray <PROFILELISTITEM> Children; // Any profiles below this one (ie groups or profiles)

	PROFILELISTITEM(void)
	{
		memset(TYPE,0,TYPELEN);
		memset(NAME,0,100);
		memset(DESC,0,200);
		Level = 0;
		ImageType = VALIDFINALPROFILE;
		IsPersonalProfile = false;
		IsValid = false;
	}
};
typedef struct PROFILELISTITEM ProfileListItem;


class ProfileListViewer
{
// Constructions
public:
    ProfileListViewer();
    ~ProfileListViewer();

	CComboBoxEx *pomProfileListCtrl;
	ProfileListItem omProfileList;
	COMBOBOXEXITEM	omProfileListItem;
	int imProfileListItem;
	void CreateFrelList(ProfileListItem &ropParent, CCSPtrArray <ProfileListItem> &ropChildren);
	bool NoProfilesDefined();
	void DeleteProfileListItem(ProfileListItem &rrpProfileListItem);
	void CreateProfileList(const char *pcpFsec);
	void CreateProfileListItems(const char *pcpFsec, CCSPtrArray <ProfileListItem> &ropChildren, int ipLevel);
	void Attach(CComboBoxEx *popProfileListCtrl);
	void ChangeViewTo(const char *pcpFsec);
	void UpdateDisplay();
	void DrawProfileListItems(CCSPtrArray <ProfileListItem> &ropChildren);
	bool DrawProfileListItem(ImageType epImageType, int ipIndent, const char *pcpString, LPARAM lpUserData=0L);
	void CreateProfile(CMapStringToPtr &ropFrelList, CCSPtrArray <PRVDATA> &ropProfile, CMapStringToPtr &ropFfktMap);
	void CreateObjects(ProfileListItem &ropProfileListItem);
	void CreateObjectList(ProfileListItem &ropParent);

	CMapStringToPtr omUrnoList;
	bool UrnoLoaded(const char *pcpUrno);
};

#endif __PROFILELISTVIEWER_H__
