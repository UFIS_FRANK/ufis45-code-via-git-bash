//
// CedaFktData.cpp - Read/update FKTTAB
//
#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#include <stdafx.h>
#include <CCSGlobl.h>
#include <CedaFktData.h>
#include <MessList.h>
#include <resrc1.h>

//
// CedaFktData()
//
// Constructor
//
CedaFktData::CedaFktData()
{
    // Create an array of CEDARECINFO for FKTDATA
    BEGIN_CEDARECINFO(FKTDATA, CedaFktData)
        FIELD_CHAR_TRIM(URNO,"URNO")
        FIELD_CHAR_TRIM(FAPP,"FAPP")
        FIELD_CHAR_TRIM(SUBD,"SUBD")
        FIELD_CHAR_TRIM(SDAL,"SDAL")
        FIELD_CHAR_TRIM(FUNC,"FUNC")
        FIELD_CHAR_TRIM(FUAL,"FUAL")
        FIELD_CHAR_TRIM(TYPE,"TYPE")
        FIELD_CHAR_TRIM(STAT,"STAT")
    END_CEDARECINFO

    // Copy the record structure
    for (int i = 0; i < sizeof(CedaFktData)/sizeof(CedaFktData[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&CedaFktData[i],sizeof(CEDARECINFO));
        omRecInfo.Add(prpCedaRecInfo);
	}


    // Initialize table name and field names
    sprintf(pcmTableName,"FKT%s",pcgTableExt);
    pcmFieldList = "URNO,FAPP,SUBD,SDAL,FUNC,FUAL,TYPE,STAT";


} // end CedaFktData()


//
// ~CedaFktData()
//
// Destructor
//
CedaFktData::~CedaFktData(void)
{
	//ogCCSDdx.UnRegister(this,NOTUSED);

	omRecInfo.DeleteAll();
	ClearAll();

} // end ~CedaFktData()


//
// ClearAll()
//
// Delete omData
//
void CedaFktData::ClearAll(void)
{
    omData.DeleteAll();
    omApplMap.RemoveAll();
	omUrnoMap.RemoveAll();
	ClearFappMap();

} // end ClearAll()


//
// Read()
//
// Read the entire FKTTAB and load into omData and omApplList
//
bool CedaFktData::Read()
{
    sprintf(pcmTableName,"FKT%s",pcgTableExt);

	CString olContext = "CedaFktData";
	ogDdx.Register((void *)this, BC_INSERT_FKT, olContext, CString("GRPTAB IRT"), CedaFktDataCf);
	ogDdx.Register((void *)this, BC_UPDATE_FKT, olContext, CString("GRPTAB URT"), CedaFktDataCf);
	ogDdx.Register((void *)this, BC_DELETE_FKT, olContext, CString("GRPTAB DRT"), CedaFktDataCf);
	ogDdx.Register((void *)this, FKT_INSERT, olContext, CString("FKTTAB INSERT"), CedaFktDataCf);
	ogDdx.Register((void *)this, SEC_DELAPP, olContext, CString("SECTAB DELAPP"), CedaFktDataCf);

	if (pogInitialLoad != NULL)
	{
		char pclTmp[200];
		sprintf(pclTmp,GetString(IDS_LOADING_TABLE),pcmTableName);
		pogInitialLoad->SetMessage(pclTmp);
	}


    // Select data from the database
	char pclWhere[512];
	bool blRc = true;
	char pclCom[50] = "RT";
	char pclFields[512];
	strcpy(pclFields,pcmFieldList);
	char pclSort[512] = "";
	char pclData[1024] = "";
	char pclTable[20];
	memset(pclTable,0,20);
	strncpy(pclTable,pcmTableName,6);

	sprintf(pclWhere,"ORDER BY FAPP");
//	blRc = CedaAction(pclCom, pclWhere);
	blRc = CedaAction(pclCom,pclTable,pclFields,pclWhere,pclSort,pclData);
	if ( ! blRc )
	{
		ogLog.Trace("LOADFKT","Ceda-Error %s (%d) \n",omLastErrorMessage,blRc);

		// it's not an error if no data were found
		if(omLastErrorMessage.Find(ORA_NOT_FOUND) != -1 || omLastErrorMessage == "No Data Found")
			blRc = true;
	}
	else
	{
		// Load data from CedaData into the dynamic array of records
		for (int ilLc = 0; blRc; ilLc++)
		{

			FKTDATA *prpFkt = new FKTDATA;
			if ((blRc = GetBufferRecord(ilLc,prpFkt)) == true)
				Add(prpFkt);
			else
				delete prpFkt;
		}

		int ilSize = omData.GetSize();
		ogLog.Trace("LOADFKT"," %d records loaded from %s",ilSize,pcmTableName);
		if (pogInitialLoad != NULL)
		{
			char pclBuf[128];
			pogInitialLoad->SetProgress(20);
			sprintf(pclBuf,GetString(IDS_NUM_DB_RECS_READ),ilSize,pcmTableName);
			pogInitialLoad->SetMessage(CString(pclBuf));
			pogInitialLoad->UpdateWindow();
		}

		blRc = true;
	}

	return blRc;

} // end Read()


// ----------------------------------------------------------------------------------------------
// SetFkt()
//
// Update STAT for records in FKTTAB
//
// pcpDataList		- one or many URNO,STAT
// pcpErrorMessage	- on RCFail this buffer will contain an error message else blank
//
bool CedaFktData::SetFkt( const char *pcpDataField, char *pcpErrorMessage)
{
	bool blRc = true;
	CString olListOfData;
	char pclFields[BIGBUF];
	char pclWhere[50];
	int ilDataFieldLen = strlen(pcpDataField);

	char *pclData = new char[max(BIGBUF,ilDataFieldLen)];

	memset(pclData,0,ilDataFieldLen);
	strncpy(pclData,pcpDataField,ilDataFieldLen-1); // copy except final comma


	strcpy(pcpErrorMessage,""); // no errors yet


	strcpy(pclFields,"URNO,STAT");
	strcpy(pclWhere,"WHERE SETFKT");

	// Action,TableName,FieldList,Selection,Sort,Data,Dest (eg "RETURN")
	char pclSort[50] = "";
	blRc = CedaAction(pcgSecCommand,pcgTableExt,pclFields,pclWhere,pclSort,pclData);


	if( ! blRc )
	{
		if( omLastErrorMessage != CString(pclData) )
			strcpy(pcpErrorMessage,omLastErrorMessage);
		else
			sprintf(pcpErrorMessage,"Ceda error <%d> !",blRc);
	}
	else
	{
		// loop through the URNO/STAT list updating local PRV records
		char *pclCopy = new char[ilDataFieldLen+1];
		strcpy(pclCopy,pcpDataField);
		char *pclComma, *pclItem = pclCopy, pclUrno[URNOLEN], pclStat[STATLEN];
		
		while( (pclComma = strchr(pclItem,',')) != NULL )
		{
			memset(pclUrno,0,sizeof(pclUrno));
			memset(pclStat,0,sizeof(pclStat));

			strncpy(pclUrno,pclItem,strlen(pclItem)-strlen(pclComma));
			pclItem = pclComma + sizeof(char);

			pclComma = strchr(pclItem,',');
			strncpy(pclStat,pclItem,strlen(pclItem)-strlen(pclComma));
			pclItem = pclComma + sizeof(char);

			Update(pclUrno,pclStat);
		}


		delete [] pclCopy;
		blRc = true;
	}

	delete [] pclData;

	return blRc;

} // end SetFkt()


static void CedaFktDataCf(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
    CedaFktData *polThis = (CedaFktData *)popInstance;

	switch(ipDDXType) {

	case FKT_INSERT:
		polThis->Add((FKTDATA *)vpDataPointer);
		break;
	case SEC_DELAPP:
		// application deleted so delete it from FKTTAB
		polThis->DeleteByFapp((char *)vpDataPointer);
		break;
	case BC_INSERT_FKT:
	case BC_UPDATE_FKT:
	case BC_DELETE_FKT:
		polThis->ProcessFktBc(ipDDXType, vpDataPointer, ropInstanceName);
		break;
	default:
		break;
	}
}


void CedaFktData::ProcessFktBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlFktData = (struct BcStruct *) vpDataPointer;
	FKTDATA rlFkt, *prlFkt = NULL;
	char	clUrno[URNOLEN];
	long llUrno = GetUrnoFromSelection(prlFktData->Selection);
	GetRecordFromItemList(&rlFkt,prlFktData->Fields,prlFktData->Data);
	if(llUrno != 0L)
		sprintf(clUrno, "%ld", llUrno);
	else
		strcpy(clUrno, rlFkt.URNO);

	switch(ipDDXType)
	{
		case BC_INSERT_FKT:
		{
			if((prlFkt = GetFktByUrno(clUrno)) != NULL)
			{
				GetRecordFromItemList(prlFkt,prlFktData->Fields,prlFktData->Data);
				ogDdx.DataChanged((void *)this, UPDATE_FKT, (void *)prlFkt);
			}
			else if((prlFkt = AddFktInternal(rlFkt)) != NULL)
			{
				ogDdx.DataChanged((void *)this, INSERT_FKT, (void *)prlFkt);
			}
			break;
		}
		case BC_UPDATE_FKT:
		{
			if((prlFkt = GetFktByUrno(clUrno)) != NULL)
			{
				GetRecordFromItemList(prlFkt,prlFktData->Fields,prlFktData->Data);
				ogDdx.DataChanged((void *)this, UPDATE_FKT, (void *)prlFkt);
			}
			break;
		}
		case BC_DELETE_FKT:
		{
			if((prlFkt = GetFktByUrno(clUrno)) != NULL)
			{
				FKTDATA rlOldFkt = *prlFkt;
				DeleteFktInternal(clUrno);
				ogDdx.DataChanged((void *)this, DELETE_FKT, (void *)&rlOldFkt);
			}
			break;
		}
	}
}

FKTDATA *CedaFktData::AddFktInternal(FKTDATA &rrpFkt)
{
	FKTDATA *prlFkt = new FKTDATA;
	*prlFkt = rrpFkt;
	Add(prlFkt);

	return prlFkt;
}

void CedaFktData::DeleteFktInternal(char *pcpUrno)
{
	int ilNumFkts = omData.GetSize();
	for(int ilFkt = (ilNumFkts-1); ilFkt >= 0; ilFkt--)
	{
		if(!strcmp(omData[ilFkt].URNO,pcpUrno))
		{
			DeleteFromFappMapByUrno(omData[ilFkt].FAPP, omData[ilFkt].URNO);
			omData.DeleteAt(ilFkt);
		}
	}
	omUrnoMap.RemoveKey(pcpUrno);
}


//
// Add()
//
// Add a FKTTAB record to omData
//
// prpFkt - record read from FKTTAB
//
bool CedaFktData::Add(FKTDATA *prpFkt)
{

	omData.Add(prpFkt);
	omUrnoMap.SetAt(prpFkt->URNO,prpFkt);
	AddToFappMap(prpFkt);

    return true;

} // end Add()



// Given the URNO return a pointer to a FKT rec.
FKTDATA* CedaFktData::GetFktByUrno(const char *pcpUrno)
{
	FKTDATA *prlFktRec = NULL;
	
	omUrnoMap.Lookup(pcpUrno,(void *&) prlFktRec);

	return prlFktRec;
}


//
// Update()
//
// Update a record in omData
//
bool CedaFktData::Update(const char *pcpUrno, const char *pcpStat)
{
	FKTDATA *prlFkt = GetFktByUrno(pcpUrno);

	if( prlFkt != NULL )
		strcpy(prlFkt->STAT,pcpStat);

    return prlFkt != NULL ? true : false;

} // end Update()


//
// DeleteByFapp()
//
// delete a record from local data using pcpFapp as the key
// deletes all records for the specified application
//
bool CedaFktData::DeleteByFapp( const char  *pcpFapp )
{
	bool blNotFound = true;
	int ilCount = omData.GetSize();

	for(int ilLineNo = ilCount-1; ilLineNo >= 0; ilLineNo--)
	{
		if(strcmp(omData[ilLineNo].FAPP, pcpFapp) == 0)
		{
			omUrnoMap.RemoveKey(omData[ilLineNo].URNO);
			omData.DeleteAt(ilLineNo);
			blNotFound = false;
		}
	}
	DeleteFromFappMapByFapp(pcpFapp);

	return !blNotFound;
}


// create a map of URNOs/flag for the application
// this is used to check if an application is completely installed
int CedaFktData::CreateApplMap( const char *pcpFapp )
{
    omApplMap.RemoveAll();

	int ilCount = omData.GetSize();

	for( int ilLc = 0; ilLc < ilCount; ilLc++)
		if(strcmp(omData[ilLc].FAPP, pcpFapp) == 0)
			omApplMap.SetAt(omData[ilLc].URNO,&omData[ilLc]);

	return omApplMap.GetCount();
}

// check if FFKT is in the application map
// return true if FFKT is in the map else false
bool CedaFktData::IsInApplMap( const char *pcpFfkt )
{
	FKTDATA *prlFkt;	
	
	return omApplMap.Lookup((LPCSTR) pcpFfkt, (void *&) prlFkt) ? true : false;
}

// FAPP -> URNO
//		-> URNO
//		-> URNO
//		-> URNO
//		-> URNO
// FAPP -> URNO
//		-> URNO
// etc
void CedaFktData::AddToFappMap(FKTDATA *prpFkt)
{
	CMapStringToPtr *polUrnoMap;
	if(omFappMap.Lookup(prpFkt->FAPP,(void *&)polUrnoMap))
	{
		polUrnoMap->SetAt(prpFkt->URNO,prpFkt);
	}
	else
	{
		polUrnoMap = new CMapStringToPtr;
		polUrnoMap->SetAt(prpFkt->URNO,prpFkt);
		omFappMap.SetAt(prpFkt->FAPP,polUrnoMap);
	}
}


void CedaFktData::GetFktListByFapp(const char *pcpFapp, CCSPtrArray <FKTDATA> &ropFktList, bool bpReset /*true*/)
{
	if(bpReset)
	{
		ropFktList.RemoveAll();
	}

	CMapStringToPtr *polUrnoMap;
	CString olUrno;
	FKTDATA *prlFkt;

	if(omFappMap.Lookup(pcpFapp,(void *& )polUrnoMap))
	{
		for(POSITION rlPos = polUrnoMap->GetStartPosition(); rlPos != NULL; )
		{
			polUrnoMap->GetNextAssoc(rlPos,olUrno,(void *& )prlFkt);
			// SDAL set to 1 to indicate that the record is old
			// FKT rec is only kept until no profiles reference it
			if(strcmp(prlFkt->SDAL,"1")) 
				ropFktList.Add(prlFkt);
		}
	}
}

void CedaFktData::ClearFappMap()
{
	CMapStringToPtr *polUrnoMap;
	CString olUrno;
	for(POSITION rlPos = omFappMap.GetStartPosition(); rlPos != NULL; )
	{
		omFappMap.GetNextAssoc(rlPos,olUrno,(void *& )polUrnoMap);
		polUrnoMap->RemoveAll();
		delete polUrnoMap;
	}
	omFappMap.RemoveAll();
}

// delete all records for an application from fapp map
bool CedaFktData::DeleteFromFappMapByFapp(const char *pcpFapp)
{
	bool blDeleted = false;
	CMapStringToPtr *polUrnoMap;
	CString olUrno;

	if(omFappMap.Lookup(pcpFapp,(void *& )polUrnoMap))
	{
		polUrnoMap->RemoveAll();
		delete polUrnoMap;
		omFappMap.RemoveKey(pcpFapp);
		blDeleted = true;
	}

	return blDeleted;
}

// delete single record from fapp map
void CedaFktData::DeleteFromFappMapByUrno(const char *pcpFapp, const char *pcpUrno)
{
	CMapStringToPtr *polUrnoMap;
	CString olUrno;

	if(omFappMap.Lookup(pcpFapp,(void *& )polUrnoMap))
	{
		polUrnoMap->RemoveKey(pcpUrno);
		if(polUrnoMap->GetCount() <= 0)
		{
			delete polUrnoMap;
			omFappMap.RemoveKey(pcpFapp);
		}
	}
}