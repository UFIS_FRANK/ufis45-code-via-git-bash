# Microsoft Developer Studio Project File - Name="BDPS_SEC" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Application" 0x0101

CFG=BDPS_SEC - Win32 bchDebug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "BDPS_SEC.MAK".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "BDPS_SEC.MAK" CFG="BDPS_SEC - Win32 bchDebug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "BDPS_SEC - Win32 Release" (based on "Win32 (x86) Application")
!MESSAGE "BDPS_SEC - Win32 Debug" (based on "Win32 (x86) Application")
!MESSAGE "BDPS_SEC - Win32 bchDebug" (based on "Win32 (x86) Application")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "BDPS_SEC - Win32 Release"

# PROP BASE Use_MFC 5
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 5
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "c:\Ufis_Bin\Release"
# PROP Intermediate_Dir "c:\Ufis_Intermediate\Bdpssec\Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MT /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /MT /W3 /GX /O2 /I ".\\" /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /Yu"stdafx.h" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /o "NUL" /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /o "NUL" /win32
# ADD BASE RSC /l 0x407 /d "NDEBUG"
# ADD RSC /l 0x407 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /machine:I386
# ADD LINK32 c:\Ufis_Bin\release\ufis32.lib c:\Ufis_Bin\ClassLib\release\ccsclass.lib Wsock32.lib /nologo /subsystem:windows /machine:I386

!ELSEIF  "$(CFG)" == "BDPS_SEC - Win32 Debug"

# PROP BASE Use_MFC 5
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 5
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "c:\Ufis_Bin\debug"
# PROP Intermediate_Dir "c:\Ufis_Intermediate\Bdpssec\debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MTd /W3 /Gm /GX /Zi /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /MTd /W3 /Gm /GX /ZI /Od /I ".\\" /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /FR /Yu"stdafx.h" /FD /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /o "NUL" /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /o "NUL" /win32
# ADD BASE RSC /l 0x407 /d "_DEBUG"
# ADD RSC /l 0x809 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept
# ADD LINK32 c:\Ufis_Bin\debug\ufis32.lib c:\Ufis_Bin\ClassLib\debug\ccsclass.lib Wsock32.lib C:\Ufis_Bin\ForaignLibs\htmlhelp.lib /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept

!ELSEIF  "$(CFG)" == "BDPS_SEC - Win32 bchDebug"

# PROP BASE Use_MFC 5
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "BDPS_SEC___Win32_bchDebug"
# PROP BASE Intermediate_Dir "BDPS_SEC___Win32_bchDebug"
# PROP BASE Ignore_Export_Lib 0
# PROP BASE Target_Dir ""
# PROP Use_MFC 5
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MTd /W3 /Gm /GX /ZI /Od /I ".\\" /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /FR /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /MTd /W3 /Gm /GX /ZI /Od /I ".\\" /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /FR /Yu"stdafx.h" /FD /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /o "NUL" /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /o "NUL" /win32
# ADD BASE RSC /l 0x809 /d "_DEBUG"
# ADD RSC /l 0x809 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 c:\Ufis_Bin\debug\ufis32.lib c:\Ufis_Bin\ClassLib\debug\ccsclass.lib Wsock32.lib /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept
# ADD LINK32 c:\Ufis_Bin\debug\ufis32.lib c:\Ufis_Bin\ClassLib\debug\ccsclass.lib Wsock32.lib C:\Ufis_Bin\ForaignLibs\htmlhelp.lib /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept

!ENDIF 

# Begin Target

# Name "BDPS_SEC - Win32 Release"
# Name "BDPS_SEC - Win32 Debug"
# Name "BDPS_SEC - Win32 bchDebug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=..\_Standard_Wrapper\aatlogin.cpp
# End Source File
# Begin Source File

SOURCE=.\AboutSecDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\AllocatedDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\ApplTableViewer.cpp
# End Source File
# Begin Source File

SOURCE=.\BASICDATA.CPP
# End Source File
# Begin Source File

SOURCE=.\BDPS_SEC.cpp
# End Source File
# Begin Source File

SOURCE=.\hlp\BDPS_SEC.hpj
# End Source File
# Begin Source File

SOURCE=.\BDPS_SEC.rc
# End Source File
# Begin Source File

SOURCE=.\ButtonListDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\CalTableViewer.cpp
# End Source File
# Begin Source File

SOURCE=.\CCSGlobl.cpp
# End Source File
# Begin Source File

SOURCE=.\CCSPrint.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaCalData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaFktData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaGrpData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaInitModuData.Cpp
# End Source File
# Begin Source File

SOURCE=.\CedaPrvData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaSecData.cpp
# End Source File
# Begin Source File

SOURCE=.\EditUserDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\FuncTableViewer.cpp
# End Source File
# Begin Source File

SOURCE=.\GrpTableViewer.cpp
# End Source File
# Begin Source File

SOURCE=.\GUILng.cpp
# End Source File
# Begin Source File

SOURCE=.\InitialLoadDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\LoginDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\MessList.cpp
# End Source File
# Begin Source File

SOURCE=.\ObjDialog.cpp
# End Source File
# Begin Source File

SOURCE=.\ObjectListBox.cpp
# End Source File
# Begin Source File

SOURCE=.\ObjList.cpp
# End Source File
# Begin Source File

SOURCE=.\PrintOption.cpp
# End Source File
# Begin Source File

SOURCE=.\PrintRightsDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\PrivList.cpp
# End Source File
# Begin Source File

SOURCE=.\ProfileListViewer.cpp
# End Source File
# Begin Source File

SOURCE=.\RegisterDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\resrc1.h
# End Source File
# Begin Source File

SOURCE=.\SecTableViewer.cpp
# End Source File
# Begin Source File

SOURCE=.\StdAfx.cpp
# ADD CPP /Yc"stdafx.h"
# End Source File
# Begin Source File

SOURCE=.\SubdTableViewer.cpp
# End Source File
# Begin Source File

SOURCE=.\VersionInfo.cpp
# End Source File
# Begin Source File

SOURCE=.\WaitDlg.cpp
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=..\_Standard_Wrapper\aatlogin.h
# End Source File
# Begin Source File

SOURCE=.\AboutSecDlg.h
# End Source File
# Begin Source File

SOURCE=.\AllocatedDlg.h
# End Source File
# Begin Source File

SOURCE=.\ApplTableViewer.h
# End Source File
# Begin Source File

SOURCE=.\BASICDATA.H
# End Source File
# Begin Source File

SOURCE=.\BDPS_SEC.h
# End Source File
# Begin Source File

SOURCE=.\ButtonListDlg.h
# End Source File
# Begin Source File

SOURCE=.\CalTableViewer.h
# End Source File
# Begin Source File

SOURCE=.\CCSGlobl.h
# End Source File
# Begin Source File

SOURCE=.\CCSPrint.h
# End Source File
# Begin Source File

SOURCE=.\CedaCalData.H
# End Source File
# Begin Source File

SOURCE=.\CedaFktData.H
# End Source File
# Begin Source File

SOURCE=.\CedaGrpData.h
# End Source File
# Begin Source File

SOURCE=.\CedaInitModuData.h
# End Source File
# Begin Source File

SOURCE=.\CedaPrvData.H
# End Source File
# Begin Source File

SOURCE=.\CedaSecData.H
# End Source File
# Begin Source File

SOURCE=.\EditUserDlg.h
# End Source File
# Begin Source File

SOURCE=.\FilterData.h
# End Source File
# Begin Source File

SOURCE=.\FuncTableViewer.h
# End Source File
# Begin Source File

SOURCE=.\GrpTableViewer.h
# End Source File
# Begin Source File

SOURCE=.\GUILng.h
# End Source File
# Begin Source File

SOURCE=.\InitialLoadDlg.h
# End Source File
# Begin Source File

SOURCE=.\LoginDlg.h
# End Source File
# Begin Source File

SOURCE=.\MessList.h
# End Source File
# Begin Source File

SOURCE=.\ObjDialog.h
# End Source File
# Begin Source File

SOURCE=.\ObjectListBox.h
# End Source File
# Begin Source File

SOURCE=.\ObjList.h
# End Source File
# Begin Source File

SOURCE=.\PrintOption.h
# End Source File
# Begin Source File

SOURCE=.\PrintRightsDlg.h
# End Source File
# Begin Source File

SOURCE=.\PrivList.h
# End Source File
# Begin Source File

SOURCE=.\ProfileListViewer.h
# End Source File
# Begin Source File

SOURCE=.\RegisterDlg.h
# End Source File
# Begin Source File

SOURCE=.\Resource.h
# End Source File
# Begin Source File

SOURCE=.\SecTableViewer.h
# End Source File
# Begin Source File

SOURCE=.\StdAfx.h
# End Source File
# Begin Source File

SOURCE=.\SubdTableViewer.h
# End Source File
# Begin Source File

SOURCE=.\VersionInfo.h
# End Source File
# Begin Source File

SOURCE=.\WaitDlg.h
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;cnt;rtf;gif;jpg;jpeg;jpe"
# Begin Source File

SOURCE=.\res\attentio.ico
# End Source File
# Begin Source File

SOURCE=.\res\BDPS_SEC.ico
# End Source File
# Begin Source File

SOURCE=.\res\BDPS_SEC.rc2
# End Source File
# Begin Source File

SOURCE=.\res\bitmap1.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00001.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00002.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00003.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00004.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00005.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00006.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00007.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00008.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00009.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00010.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00011.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00012.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00013.bmp
# End Source File
# Begin Source File

SOURCE=.\res\break.bmp
# End Source File
# Begin Source File

SOURCE=.\res\button.ico
# End Source File
# Begin Source File

SOURCE=.\res\buttonic.ico
# End Source File
# Begin Source File

SOURCE=.\res\checkbox.ico
# End Source File
# Begin Source File

SOURCE=.\res\combobox.ico
# End Source File
# Begin Source File

SOURCE=.\res\edit.ico
# End Source File
# Begin Source File

SOURCE=.\res\errorrec.ico
# End Source File
# Begin Source File

SOURCE=.\res\exclamat.bmp
# End Source File
# Begin Source File

SOURCE=.\res\finalpro.ico
# End Source File
# Begin Source File

SOURCE=.\res\ico00001.ico
# End Source File
# Begin Source File

SOURCE=.\res\ico00002.ico
# End Source File
# Begin Source File

SOURCE=.\res\ico00003.ico
# End Source File
# Begin Source File

SOURCE=.\res\ico00004.ico
# End Source File
# Begin Source File

SOURCE=.\res\ico00005.ico
# End Source File
# Begin Source File

SOURCE=.\res\ico00006.ico
# End Source File
# Begin Source File

SOURCE=.\res\ico00007.ico
# End Source File
# Begin Source File

SOURCE=.\res\ico00008.ico
# End Source File
# Begin Source File

SOURCE=.\res\ico00009.ico
# End Source File
# Begin Source File

SOURCE=.\res\ico00010.ico
# End Source File
# Begin Source File

SOURCE=.\res\ico00011.ico
# End Source File
# Begin Source File

SOURCE=.\res\ico00012.ico
# End Source File
# Begin Source File

SOURCE=.\res\ico00013.ico
# End Source File
# Begin Source File

SOURCE=.\res\ico00014.ico
# End Source File
# Begin Source File

SOURCE=.\res\ico00015.ico
# End Source File
# Begin Source File

SOURCE=.\res\ico00016.ico
# End Source File
# Begin Source File

SOURCE=.\res\ico00017.ico
# End Source File
# Begin Source File

SOURCE=.\res\ico00018.ico
# End Source File
# Begin Source File

SOURCE=.\res\ico00019.ico
# End Source File
# Begin Source File

SOURCE=.\res\ico00020.ico
# End Source File
# Begin Source File

SOURCE=.\res\ico00021.ico
# End Source File
# Begin Source File

SOURCE=.\res\ico00022.ico
# End Source File
# Begin Source File

SOURCE=.\res\ico00023.ico
# End Source File
# Begin Source File

SOURCE=.\res\ico00024.ico
# End Source File
# Begin Source File

SOURCE=.\res\icon1.ico
# End Source File
# Begin Source File

SOURCE=.\res\invalidf.ico
# End Source File
# Begin Source File

SOURCE=.\res\invalidg.ico
# End Source File
# Begin Source File

SOURCE=.\res\invalidp.ico
# End Source File
# Begin Source File

SOURCE=.\res\listbox.ico
# End Source File
# Begin Source File

SOURCE=.\res\listctrl.ico
# End Source File
# Begin Source File

SOURCE=.\res\LOGIN.BMP
# End Source File
# Begin Source File

SOURCE=.\res\login1.bmp
# End Source File
# Begin Source File

SOURCE=.\res\menuitem.ico
# End Source File
# Begin Source File

SOURCE=.\res\radiobut.ico
# End Source File
# Begin Source File

SOURCE=.\res\slider.ico
# End Source File
# Begin Source File

SOURCE=.\res\tabctrl.ico
# End Source File
# Begin Source File

SOURCE=.\res\tl_amber.bmp
# End Source File
# Begin Source File

SOURCE=.\res\tl_green.bmp
# End Source File
# Begin Source File

SOURCE=.\res\tl_red.bmp
# End Source File
# Begin Source File

SOURCE=.\res\tl_red1.bmp
# End Source File
# Begin Source File

SOURCE=.\Trafred.ico
# End Source File
# Begin Source File

SOURCE=.\res\treectrl.ico
# End Source File
# Begin Source File

SOURCE=.\res\ufis.ico
# End Source File
# Begin Source File

SOURCE=.\res\Ufisblau.ico
# End Source File
# Begin Source File

SOURCE=.\res\validfin.ico
# End Source File
# Begin Source File

SOURCE=.\res\validgro.ico
# End Source File
# Begin Source File

SOURCE=.\res\validpro.ico
# End Source File
# End Group
# Begin Group "Help Files"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\hlp\AfxDlg.rtf
# End Source File
# Begin Source File

SOURCE=.\hlp\BDPS_SEC.cnt
# End Source File
# Begin Source File

SOURCE=.\MakeHelp.bat
# End Source File
# End Group
# Begin Source File

SOURCE=.\Bdps_Sec.cfg
# End Source File
# Begin Source File

SOURCE=.\ReadMe.txt
# End Source File
# End Target
# End Project
