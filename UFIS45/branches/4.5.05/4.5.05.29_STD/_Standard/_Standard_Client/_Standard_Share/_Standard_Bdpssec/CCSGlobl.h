// CCSGlobl.h: interface for the CCSGlobl class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_CCSGLOBL_H__55D90F34_191F_11D1_82BE_0080AD1DC701__INCLUDED_)
#define AFX_CCSGLOBL_H__55D90F34_191F_11D1_82BE_0080AD1DC701__INCLUDED_

#ifndef __CCSGLOBL_H__
#define __CCSGLOBL_H__

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000


#include <CCSDefines.h>
#include <CCSTime.h>
#include <CCSCedaData.h>
#include <CCSCedaCom.h>
#include <CCSDdx.h>
#include <CCSLog.h>
#include <CCSBcHandle.h>
#include <BasicData.h>
#include <InitialLoadDlg.h>
#include <PrivList.h>


/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
// Global Variable Section


class CedaBasicData;
class CCSBcHandle;
class CCSCedaCom;
class CCSDdx;
class CCSLog;
class CCSBcHandle;
class CBasicData;

class CedaSecData;
class CedaCalData;
class CedaGrpData;
class CedaPrvData;
class CedaFktData;

class ObjList;
class PrivList;


extern CedaBasicData ogBCD;
extern CCSBcHandle ogBcHandle;

extern CCSCedaCom ogCommHandler;  // The one and only CedaCom object
extern CCSDdx ogDdx;
extern CCSLog ogLog;
extern CBasicData ogBasicData;

extern CedaSecData ogCedaSecData;
extern CedaCalData ogCedaCalData;
extern CedaGrpData ogCedaGrpData;
extern CedaPrvData ogCedaPrvData;
extern CedaFktData ogCedaFktData;

extern const char *pcgAppName;
extern CString ogUsername;


enum enumColorIndexes
{
	IDX_GRAY=2,IDX_GREEN,IDX_RED,IDX_BLUE,IDX_SILVER,IDX_MAROON,
	IDX_OLIVE,IDX_NAVY,IDX_PURPLE,IDX_TEAL,IDX_LIME,
	IDX_YELLOW,IDX_FUCHSIA,IDX_AQUA, IDX_WHITE,IDX_BLACK,IDX_ORANGE
};

/*
enum //enumRecordState 
{ 
    DATA_UNCHANGED, DATA_NEW, DATA_CHANGED, DATA_DELETED
};
*/

enum DLG_ACTION 
{ 
    DLG_INSERT,DLG_UPDATE
   
};  

//enum enumBarType{BAR_FLIGHT,BAR_SPECIAL,BKBAR,GEDBAR,BAR_BREAK,BAR_ABSENT,BAR_SHADOW};





#define MAXCOLORS 64
#define FIRSTCONFLICTCOLOR 31

// Symbolic colors (helper constants for CGateDiagramDialog -- testing purpose only)
#define BLACK   RGB(  0,   0,   0)
#define MAROON  RGB(128,   0,   0)          // dark red
#define GREEN   RGB(  0, 128,   0)          // dark green
#define OLIVE   RGB(128, 128,   0)          // dark yellow
#define NAVY    RGB(  0,   0, 128)          // dark blue
#define PURPLE  RGB(128,   0, 128)          // dark magenta
#define TEAL    RGB(  0, 128, 128)          // dark cyan
#define GRAY    RGB(128, 128, 128)          // dark gray
//PRF6932 #define SILVER		RGB(192, 192, 192)  // light gray
#define SILVER				 ::GetSysColor(COLOR_BTNFACE)
#define RED     RGB(255,   0,   0)
#define ORANGE  RGB(255, 132,   0)
#define LIME    RGB(  0, 255,   0)          // green
#define YELLOW  RGB(255, 255,   0)
#define BLUE    RGB(  0,   0, 255)
#define FUCHSIA RGB(255,   0, 255)          // magenta
#define AQUA    RGB(100, 255, 255)          // cyan
#define WHITE   RGB(255, 255, 255)

extern COLORREF ogColors[];
extern CBrush *ogBrushs[];
extern CString ogAppName;

extern COLORREF lgBkColor;
extern COLORREF lgTextColor;
extern COLORREF lgHilightColor;

extern enum enumRecordState egRecordState;

extern BOOL bgIsInitialized;

// CFont : translate from logical point to physical pixel
#define PT_TO_PIXELS(dc, pt)    (- MulDiv( pt, (dc).GetDeviceCaps( LOGPIXELSY ), 72 ))


/////////////////////////////////////////////////////////////////////////////
// IDs


#define IDC_PREV            0x4008
#define IDC_NEXT            0x4009
#define IDC_INPLACEEDIT		0x400c


/////////////////////////////////////////////////////////////////////////////
// Messages


// Message and constants which are used to handshake viewer and the attached diagram
#define WM_POSITIONCHILD				(WM_USER + 201)
#define WM_UPDATEDIAGRAM				(WM_USER + 202)
#define UD_UPDATEGROUP                  (WM_USER + 203)
#define UD_DELETELINE                   (WM_USER + 204)
#define UD_DELETEGROUP                  (WM_USER + 205)
#define UD_INSERTLINE                   (WM_USER + 206)
#define UD_INSERTGROUP                  (WM_USER + 207)
#define UD_UPDATELINEHEIGHT             (WM_USER + 208)
#define UD_UPDATELINE					(WM_USER + 209)
#define UD_UPDATEDLG					(WM_USER + 210)
#define WM_REASSIGNFINISHED				(WM_USER + 211)
#define WM_ATTENTIONSETCOLOR			(WM_USER + 212)
#define UD_REASSIGNFINISHED				(WM_USER + 212)



/////////////////////////////////////////////////////////////////////////////
// Font variable

extern CFont ogSmallFonts_Regular_6;
extern CFont ogSmallFonts_Regular_7;
extern CFont ogMSSansSerif_Regular_8;
extern CFont ogMSSansSerif_Bold_8;
extern CFont ogCourier_Bold_10;
extern CFont ogCourier_Regular_10;
extern CFont ogCourier_Regular_8;
extern CFont ogCourier_Regular_9;

extern CFont ogScalingFonts[4];
extern int igFontIndex1;
extern int igFontIndex2;


void InitFont();
void DeleteBrushes();
void CreateBrushes();
CString GetString(UINT ipStringId);


enum{MS_SANS6, MS_SANS8, MS_SANS12, MS_SANS16, 
     MS_SANS6BOLD, MS_SANS8BOLD, MS_SANS12BOLD, MS_SANS16BOLD};

struct FONT_INDEXES
{
	int VerticalScale;
	int Chart;
	FONT_INDEXES(void)
	{VerticalScale=0;Chart=0;}
};


/////////////////////////////////////////////////////////////////////////////
// Color and brush variables

extern COLORREF ogColors[];
extern CBrush *ogBrushs[];

/////////////////////////////////////////////////////////////////////////////



struct TIMEFRAMEDATA
{
	CTime StartTime;
	CTime EndTime;
	TIMEFRAMEDATA(void)
	{StartTime=TIMENULL;EndTime=TIMENULL;}
};


/////////////////////////////////////////////////////////////////////////////
// application globals 
extern char pcgHome[50];
extern char pcgOldHome[50]; // table name - only for backwards compatibility
extern char pcgTableExt[50];
extern char pcgWks[40];
extern int  igValidToDays;
extern char pcgSecCommand[4];
extern bool bgNoScroll;
extern bool bgOnline;
extern bool bgIsButtonListMovable;
extern bool bgIsSuperUser;
extern CString ogSuperUser;

class InitialLoadDlg;
extern InitialLoadDlg *pogInitialLoad;

/////////////////////////////////////////////////////////////////////////////
// IDs

//***** moved from "ccsglobl.h" by Damkerng 04/30/96 18:30
//***** moved from "tscall.cpp" by Pichate May 08,96 18:15
#define IDC_TIMESCALE       0x4001

//***** moved from "ccsglobl.h" by Damkerng 04/30/96 18:30
//***** moved from "vscale.cpp" by Pichate May 08,96 18:15
#define IDC_VERTICALSCALE   0x4002

//***** moved from "ccsglobl.h" by Damkerng 04/30/96 18:30
//***** moved from "gbar.cpp" by Pichate May 08,96 18:15
#define IDC_GANTTBAR        0x4003

//#define IDD_DIAGRAM         0x4004
#define IDD_CHART           0x4005
#define IDC_CHARTBUTTON     0x4006
#define IDD_GANTT           0x4007

#define IDC_PREV            0x4008
#define IDC_NEXT            0x4009
#define IDC_ARRIVAL         0x400a
#define IDC_DEPARTURE       0x400b
#define IDC_INPLACEEDIT		0x400c

#define IDD_GANTTCHART      0x4101


/////////////////////////////////////////////////////////////////////////////
// Messages

// Message sent from PrePlanTable to the parent window when closed
// Id 24-Sep-96
// Fix some bug here, since CCI diagram and Gate diagram shares the same message number.
// This surely will crash the machine or destroy our memory blocks if the user terminate
// the diagram with Alt-F4.
#define WM_STAFFTABLE_EXIT          (WM_APP + 20)	/* tables */
#define WM_GEQTABLE_EXIT            (WM_APP + 21)	/* tables */
#define WM_INFTABLE_EXIT            (WM_APP + 22)	/* tables */
#define WM_HWDIA_EXIT				(WM_APP + 23)	/* tables */
#define WM_FLIGHTDIA_EXIT           (WM_APP + 24)	/* tables */
#define WM_STAFFDIA_EXIT            (WM_APP + 25)	/* tables */
#define WM_CONFTABLE_EXIT           (WM_APP + 26)	/* tables */
#define WM_FLIGHTTABLE_EXIT         (WM_APP + 27)	/* tables */
#define WM_MAGNETICTABLE_EXIT       (WM_APP + 28)	/* tables */
#define WM_ATTENTIONTABLE_EXIT      (WM_APP + 29)	/* tables */
#define WM_CONFLICTTABLE_EXIT       (WM_APP + 30)	/* tables */
#define WM_LOCKWINDOWUPDATE         (WM_APP + 31)	/* tables */
#define WM_ZOLLTABLE_EXIT			(WM_APP + 32)	/* tables */





/////////////////////////////////////////////////////////////////////////////
// Drag Information Type

enum 
{
	DIT_AFLIGHT,	// source: Flugdatenansicht
	DIT_DFLIGHT		// source: Flugdatenansicht
};

// DIT 0 - 49



enum
{
	DLG_NEW,
	DLG_COPY,
	DLG_CHANGE
};


enum enumDDXTypes
{
    UNDO_CHANGE, 
    BC_CFG_INSERT,BC_CFG_CHANGE,CFG_CHANGE,CFG_INSERT,CFG_DELETE,
    // from here, there are defines for your project
	SEC_INSERT,CAL_INSERT,GRP_INSERT,PRV_INSERT,FKT_INSERT,
	SEC_UPDATE,CAL_UPDATE,GRP_UPDATE,PRV_UPDATE,FKT_UPDATE,
	SEC_DELETE,CAL_DELETE,GRP_DELETE,PRV_DELETE,FKT_DELETE,SEC_DELAPP,
	SEC_VIEWER_INSERT,SEC_VIEWER_UPDATE,SEC_VIEWER_DELETE,
	OBJ_DLG_CHANGE
};
/*
struct FKTDATA
{
	char Subdivision[34];	//Gruppierung max 32.
	char Name[82];			//Name max.80
	char Alias[34];			//Aliasname max.32
	char Type;				//Typ (I,B,E,C,...)
	char Status;			//Status ('-' = hidden,'1' = enabled, '0' = disabled)
};
*/

// end globals
/////////////////////////////////////////////////////////////////////////////

extern char cgYes;
extern char cgNo;


// STAT values
#define HIDDEN_STAT "-"
#define DISABLED_STAT "0"
#define ENABLED_STAT "1"

// database field lengths
#define URNOLEN 11
#define DATELEN 15
#define USIDLEN 33
#define NAMELEN 33
#define TYPELEN 2
#define STATLEN 2
#define LANGLEN 5
#define REMALEN 257
#define FREQLEN 8
#define SUBDLEN 81
#define SDALLEN 81
#define FUNCLEN 81
#define FUALLEN 81
#define TYPELEN 2
#define PASSLEN 33
#define BIGBUF 750

#define TABLENAMELEN 25
#define FILENAMELEN 250
#define ERRMESSLEN 500	// buffer big enough to hold error messages from CedaAction()

typedef char SEC_REC_TYPE;
#define SEC_USER 'U'
#define SEC_PROFILE 'P'
#define SEC_APPL 'A'
#define SEC_GROUP 'G'
#define SEC_WKS 'W'
#define SEC_WKSGROUP 'S'



// struct to hold return values from CRU
// these values can be:
//
//type fld1 fld2 fld3 fld4 fld5 fld6 fld7 fld8 
// SEC,URNO											--> from create user in SECTAB
// CAL,URNO,FSEC,VAFR,VATO,FREQ						--> from CALTAB (create user (SECTAB) and create personal profile (GRPTAB))
// GRP,URNO,FREL,FSEC,TYPE,STAT						--> from GRPTAB create personal profile
// PRV,URNO,FSEC,FFKT,FAPP,STAT						--> from PRVTAB create personal profile
// FKT,URNO,FAPP,SUBD,SDAL,FUNC,FUAL,TYPE,STAT		--> from FKTTAB create ModuInit button
//
#define FLDLEN 100
struct RETDATA {

	char	TYPE[4];
	char	FLD1[URNOLEN];
	char	FLD2[FLDLEN];
	char	FLD3[FLDLEN];
	char	FLD4[FLDLEN];
	char	FLD5[FLDLEN];
	char	FLD6[FLDLEN];
	char	FLD7[FLDLEN];
	char	FLD8[FLDLEN];

	RETDATA(void)
	{
		strcpy(TYPE, "");
		strcpy(FLD1, "");
		strcpy(FLD2, "");
		strcpy(FLD3, "");
		strcpy(FLD4, "");
		strcpy(FLD5, "");
		strcpy(FLD6, "");
		strcpy(FLD7, "");
		strcpy(FLD8, "");
	}
};

typedef enum
{
	IS_INSTALLED,
	ISNOT_INSTALLED,
	PARTIALLY_INSTALLED
} INSTALLED_STAT;

#endif //__CCSGLOBL_H__
#endif // !defined(AFX_CCSGLOBL_H__55D90F34_191F_11D1_82BE_0080AD1DC701__INCLUDED_)
