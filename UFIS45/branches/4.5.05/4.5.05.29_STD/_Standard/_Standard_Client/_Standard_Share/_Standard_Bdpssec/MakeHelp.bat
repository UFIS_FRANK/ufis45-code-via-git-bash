@echo off
REM -- First make map file from Microsoft Visual C++ generated resource.h
echo // MAKEHELP.BAT generated Help Map file.  Used by BDPS_SEC.HPJ. >"hlp\BDPS_SEC.hm"
echo. >>"hlp\BDPS_SEC.hm"
echo // Commands (ID_* and IDM_*) >>"hlp\BDPS_SEC.hm"
makehm ID_,HID_,0x10000 IDM_,HIDM_,0x10000 resource.h >>"hlp\BDPS_SEC.hm"
echo. >>"hlp\BDPS_SEC.hm"
echo // Prompts (IDP_*) >>"hlp\BDPS_SEC.hm"
makehm IDP_,HIDP_,0x30000 resource.h >>"hlp\BDPS_SEC.hm"
echo. >>"hlp\BDPS_SEC.hm"
echo // Resources (IDR_*) >>"hlp\BDPS_SEC.hm"
makehm IDR_,HIDR_,0x20000 resource.h >>"hlp\BDPS_SEC.hm"
echo. >>"hlp\BDPS_SEC.hm"
echo // Dialogs (IDD_*) >>"hlp\BDPS_SEC.hm"
makehm IDD_,HIDD_,0x20000 resource.h >>"hlp\BDPS_SEC.hm"
echo. >>"hlp\BDPS_SEC.hm"
echo // Frame Controls (IDW_*) >>"hlp\BDPS_SEC.hm"
makehm IDW_,HIDW_,0x50000 resource.h >>"hlp\BDPS_SEC.hm"
REM -- Make help for Project BDPS_SEC


echo Building Win32 Help files
start /wait hcw /C /E /M "hlp\BDPS_SEC.hpj"
if errorlevel 1 goto :Error
if not exist "hlp\BDPS_SEC.hlp" goto :Error
if not exist "hlp\BDPS_SEC.cnt" goto :Error
echo.
if exist Debug\nul copy "hlp\BDPS_SEC.hlp" Debug
if exist Debug\nul copy "hlp\BDPS_SEC.cnt" Debug
if exist Release\nul copy "hlp\BDPS_SEC.hlp" Release
if exist Release\nul copy "hlp\BDPS_SEC.cnt" Release
echo.
goto :done

:Error
echo hlp\BDPS_SEC.hpj(1) : error: Problem encountered creating help file

:done
echo.
