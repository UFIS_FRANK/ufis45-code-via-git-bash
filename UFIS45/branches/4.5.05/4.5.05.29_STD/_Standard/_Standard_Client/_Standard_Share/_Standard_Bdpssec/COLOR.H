// color.h : header file
//

#ifndef _COLOR_
#define _COLOR_

#define MAXCOLORS 64
#define FIRSTCONFLICTCOLOR 31
        
/////////////////////////////////////////////////////////////////////////////
//

#define BLACK   RGB(  0,   0,   0)
#define MAROON  RGB(128,   0,   0)          // dark red
#define GREEN   RGB(  0, 128,   0)          // dark green
#define OLIVE   RGB(128, 128,   0)          // dark yellow
#define NAVY    RGB(  0,   0, 128)          // dark blue
#define PURPLE  RGB(128,   0, 128)          // dark magenta
#define TEAL    RGB(  0, 128, 128)          // dark cyan
#define GRAY    RGB(128, 128, 128)          // dark gray
//PRF6932 #define SILVER		RGB(192, 192, 192)  // light gray
#define SILVER				 ::GetSysColor(COLOR_BTNFACE)
#define RED     RGB(255,   0,   0)
#define LIME    RGB(  0, 255,   0)          // green
#define YELLOW  RGB(255, 255,   0)
#define BLUE    RGB(  0,   0, 255)
#define FUCHSIA RGB(255,   0, 255)          // magenta
#define AQUA    RGB(  0, 255, 255)          // cyan
#define WHITE   RGB(255, 255, 255)


/////////////////////////////////////////////////////////////////////////////
// Color variable

extern COLORREF lgBkColor;
extern COLORREF lgTextColor;
extern COLORREF lgHilightColor;

/////////////////////////////////////////////////////////////////////////////


#endif // _COLOR_