// SecTableViewer.cpp : implementation file
//

#include <stdafx.h>
#include <CCSGlobl.h>
#include <BDPS_SEC.h>
#include <SecTableViewer.h>
#include <AllocatedDlg.h>
#include <MessList.h>
#include <CcsPrint.h>
#include <PrintOption.h>
#include <resrc1.h>
#include <fstream.h>
#include <iomanip.h>
#include <process.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

const CString CONFIG_PATH = "C:\\UFIS\\SYSTEM\\CEDA.INI";
const CString CEDA = "CEDA";
const CString EXCEL = "EXCEL";
const CString DEFAULT = "DEFAULT";
const CString EXCELSEPARATOR = "EXCELSEPARATOR";
const CString FILENAME = "SecTable";
const CString EXCEL_PATH_NOT_FOUND = "Excel path not found in CEDA.INI file.";
const CString EXCEL_PATH_NOT_CORRECT = "Excel Path In CEDA.INI Is Not Valid.";
const int OPTIONPRINT = 0;
const int OPTIONEXCEL = 1;
char SEPARATOR = ',';

// Local function prototype
static void SecTableViewerCf(void *popInstance, int ipDDXType,
					   void *vpDataPointer, CString &ropInstanceName);




//*************************************************************************************
//
// SecTableViewer() - constructor
//
//*************************************************************************************

SecTableViewer::SecTableViewer()
{
    pomTable = NULL;

	ogDdx.Register((void *) this, SEC_VIEWER_INSERT, CString("SECTABLE"),CString("SECTAB INSERT"), SecTableViewerCf);
	ogDdx.Register((void *) this, SEC_VIEWER_UPDATE, CString("SECTABLE"),CString("SECTAB UPDATE"), SecTableViewerCf);
	ogDdx.Register((void *) this, SEC_VIEWER_DELETE, CString("SECTABLE"),CString("SECTAB DELETE"), SecTableViewerCf);

	ogDdx.Register((void *) this, INSERT_SEC, CString("SECTABLE"),CString("SECTAB INSERT"), SecTableViewerCf);
	ogDdx.Register((void *) this, UPDATE_SEC, CString("SECTABLE"),CString("SECTAB UPDATE"), SecTableViewerCf);
	ogDdx.Register((void *) this, BC_SBC_DELSEC, CString("SECTABLE"),CString("SECTAB DELETE"), SecTableViewerCf);

	ogDdx.Register((void *) this, INSERT_CAL, CString("SECTABLE"),CString("CALTAB INSERT"), SecTableViewerCf);
	ogDdx.Register((void *) this, UPDATE_CAL, CString("SECTABLE"),CString("CALTAB UPDATE"), SecTableViewerCf);
	ogDdx.Register((void *) this, DELETE_CAL, CString("SECTABLE"),CString("CALTAB DELETE"), SecTableViewerCf);

	ogDdx.Register((void *) this, INSERT_GRP, CString("SECTABLE"),CString("GRPTAB INSERT"), SecTableViewerCf);
	ogDdx.Register((void *) this, UPDATE_GRP, CString("SECTABLE"),CString("GRPTAB UPDATE"), SecTableViewerCf);
	ogDdx.Register((void *) this, DELETE_GRP, CString("SECTABLE"),CString("GRPTAB DELETE"), SecTableViewerCf);

	ogDdx.Register((void *) this, BC_SBC_NEWREL, CString("SECTABLE"),CString("SECTAB UPDAPP"), SecTableViewerCf);
	ogDdx.Register((void *) this, BC_SBC_DELREL, CString("SECTABLE"),CString("SECTAB UPDAPP"), SecTableViewerCf);

	// set some text to be displayed
	omEnabledMess = GetString(IDS_SECTABLEVIEWER_VALID);
	omDisabledMess = GetString(IDS_SECTABLEVIEWER_INVALID);
	omUnknownMess = GetString(IDS_SECTABLEVIEWER_UNKNOWN);

	omColumnData.VerticalSeparator = SEPA_NONE;
	omColumnData.SeparatorType = SEPA_NONE;
	omColumnData.Font = &ogCourier_Regular_10;//&ogMSSansSerif_Bold_8;//&ogCourier_Regular_10;
	omColumnData.Alignment = COLALIGN_LEFT;

	bmOtherSuperusersFound = false;
}


//*************************************************************************************
//
// SecTableViewer() - destructor
//
//*************************************************************************************

SecTableViewer::~SecTableViewer()
{
    DeleteAll();
}


//*************************************************************************************
//
// DeleteAll() - delete all lines (records) read from SECTAB
//
//*************************************************************************************

void SecTableViewer::DeleteAll()
{
	while(omLines.GetSize() > 0)
        DeleteLine(0);
}

//*************************************************************************************
//
// GetSize() - return the number of lines in omLines (and thus number of lines in CCSTable)
//
//*************************************************************************************

int SecTableViewer::GetSize()
{
	return omLines.GetSize();
}

//*************************************************************************************
//
// DeleteLine() - delete a line (record copied from SECTAB) at ipLineno
//
//*************************************************************************************

void SecTableViewer::DeleteLine(int ipLineno)
{
	omLines.DeleteAt(ipLineno);
}

//*************************************************************************************
//
// Attach() - receives a pointer to the CCSTable
//
//*************************************************************************************

void SecTableViewer::Attach(CCSTable *popTable)
{
    pomTable = popTable;
}


//*************************************************************************************
//
// ChangeViewTo() - function responsible for updating the display
//
//*************************************************************************************

void SecTableViewer::ChangeViewTo(void)
{
	if( rgCurrType != SEC_APPL && rgCurrType != SEC_PROFILE )
		bmDisplayProfile = true;
	else
		bmDisplayProfile = false;

	
	// delete all lines defined
	DeleteAll();

	// get lines specified in piViewType from CedaSecData into local memory
	MakeLines();

	// draw the lines loaded in CCSTable
	UpdateDisplay();
}




//*************************************************************************************
//
// IsPassFilter() - rgCurrType='U','P' etc --> used to select either Users,Profiles etc
//
//*************************************************************************************

BOOL SecTableViewer::IsPassFilter(SECDATA  *prpSec)
{
	BOOL blRc = TRUE;

	if(*prpSec->TYPE != rgCurrType)
	{
		blRc = FALSE;
	}
	else if(!bgIsSuperUser && (ogCedaSecData.IsSuperuser(prpSec) || (*prpSec->TYPE == SEC_USER && !strcmp(prpSec->USID,ogSuperUser))))
	{
		// only the superuser (UFIS$ADMIN) can display other system administrators and itself
		bmOtherSuperusersFound = true;
		blRc = FALSE;
	}

	return blRc;
}


//*************************************************************************************
//
// CompareLine() - 
//
//*************************************************************************************

int SecTableViewer::CompareLine(SECTAB_LINEDATA *prpLine1, SECTAB_LINEDATA *prpLine2)
{
	int  ilCompareResult = 0;

	// Implementation of sorting
	return (prpLine1->USID == prpLine2->USID)? 0: (prpLine1->USID >  prpLine2->USID)? 1: -1;

}


/////////////////////////////////////////////////////////////////////////////
// SecTableViewer -- code specific to this class





//*************************************************************************************
//
// MakeLines() - loop through the data held in ogCedaSecData copying to local memory
//
//*************************************************************************************

void SecTableViewer::MakeLines()
{
	int ilCount;
	
	ilCount = ogCedaSecData.omData.GetSize();
	for(int i = 0; i < ilCount; i++)
		MakeLine(&ogCedaSecData.omData[i]);
}

//*************************************************************************************
//
// MakeLine() - copy and process a line of data, create its field and display it
//
//*************************************************************************************

void SecTableViewer::MakeLine(SECDATA  *prpSec)
{

	// filter out either profiles or users or modul
	if( !IsPassFilter(prpSec)) 
		return;

	
	SECTAB_LINEDATA rlLine;

	strcpy(rlLine.URNO,prpSec->URNO);
	rlLine.USID = prpSec->USID;
	rlLine.NAME = prpSec->NAME;
	rlLine.REMA = prpSec->REMA;

	ogCedaCalData.GetDatesByFsec(rlLine.URNO,rlLine.VAFR,rlLine.VATO,rlLine.FREQ);	

	if( strcmp(prpSec->STAT,"1") == 0 )
		rlLine.STAT = omEnabledMess; // if STAT is '1' set text displayed to "Enabled"
	else if( strcmp(prpSec->STAT,"0") == 0 )
		rlLine.STAT = omDisabledMess; // if STAT is '0' set text displayed to "Disabled"
	else
		rlLine.STAT = omUnknownMess; // else set text displayed to "Unknown"

	// for users get any relational profiles
	if( bmDisplayProfile )
	{
		// get the relational record from GRPTAB
		GRPDATA *prlGrpRec = ogCedaGrpData.GetGrpByFsecType(rlLine.URNO,SEC_PROFILE);

		// if not personal profile...
		if( prlGrpRec != NULL && strcmp(prlGrpRec->FREL,prlGrpRec->FSEC) )
		{
			// get the profile name from SECTAB
			SECDATA *prlPrfRec = ogCedaSecData.GetSecByUrno(prlGrpRec->FREL);
			if( prlPrfRec != NULL )
			{
				rlLine.PROF = prlPrfRec->USID;
				rlLine.Fgrp = prlPrfRec->URNO;
			}
		}
	}

	if(ogCedaSecData.IsSuperuser(prpSec) || !strcmp(prpSec->USID,ogSuperUser))
	{
		rlLine.IsSysAdmin = true;
	}
	else
	{
		rlLine.IsSysAdmin = false;
	}

	CreateLine(&rlLine);
}



//*************************************************************************************
//
// CreateLine() - add the line to omLines (array of pointers to each line)
//
//*************************************************************************************

int SecTableViewer::CreateLine(SECTAB_LINEDATA *prpSec)
{
    int ilLineCount = omLines.GetSize();

    for (int ilLineno = 0; ilLineno < ilLineCount; ilLineno++)
        if (CompareLine(prpSec, &omLines[ilLineno]) <= 0)
            break;  

	SECTAB_LINEDATA  rlLine;
    rlLine = *prpSec;
	omLines.NewAt(ilLineno, rlLine);

	return ilLineCount;
}




/////////////////////////////////////////////////////////////////////////////
// SecTableViewer - display drawing routines



//*************************************************************************************
//
// UpdateDisplay() - write local SECTAB data to the CCSTable
// returns false if there are no lines to display, else true
//
//*************************************************************************************

void SecTableViewer::UpdateDisplay()
{
	CString		olTitle1;
	CString		olNoDataMsg;

	// set the header and dialog caption
	switch(rgCurrType)
	{
		case SEC_PROFILE:
			olTitle1 = GetString(IDS_SECTABLEVIEWER_PROFILENAME);
			olNoDataMsg = GetString(IDS_SECTABLEVIEWER_NOPROFILES);
			break;
		case SEC_APPL:
			olTitle1 = GetString(IDS_SECTABLEVIEWER_MODULENAME);
			olNoDataMsg = GetString(IDS_SECTABLEVIEWER_NOMODULES);
			break;
		case SEC_GROUP:
			olTitle1 = GetString(IDS_SECTABLEVIEWER_GROUPNAME);
			olNoDataMsg = GetString(IDS_SECTABLEVIEWER_NOGROUPS);
			break;
		case SEC_WKS:
			olTitle1 = GetString(IDS_SECTABLEVIEWER_WKSNAME);
			olNoDataMsg = GetString(IDS_SECTABLEVIEWER_NOWKS);
			break;
		case SEC_WKSGROUP:
			olTitle1 = GetString(IDS_SECTABLEVIEWER_WKSGROUPNAME);
			olNoDataMsg = GetString(IDS_SECTABLEVIEWER_NOWKSGROUPS);
			break;
		case SEC_USER:
		default:
			olTitle1 = GetString(IDS_SECTABLEVIEWER_USERNAME);
			olNoDataMsg = GetString(IDS_SECTABLEVIEWER_NOUSERS);
	}
	// step 1 set table header
	pomTable->ResetContent();
	CCSPtrArray <TABLE_HEADER_COLUMN> omHeaderDataArray;

	pomTable->SetShowSelection(TRUE);
	TABLE_HEADER_COLUMN rlHeader;
	rlHeader.Alignment = COLALIGN_LEFT;
	rlHeader.Font = &ogCourier_Bold_10;

	// USID header
	rlHeader.Length = USIDCOLUMNWIDTH; 
	rlHeader.Text = olTitle1;
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);

	// NAME header
	rlHeader.Length = FULLNAMECOLUMNWIDTH; 
	rlHeader.Text = GetString(IDS_SECTABLEVIEWER_NAME);
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);

	// STAT header
	rlHeader.Length = STATCOLUMNWIDTH; 
	rlHeader.Text = GetString(IDS_SECTABLEVIEWER_STATUS);
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);

	// VAFR header
	rlHeader.Length = VAFRCOLUMNWIDTH; 
	rlHeader.Text = GetString(IDS_SECTABLEVIEWER_VALIDFROM);
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);

	// VATO header
	rlHeader.Length = VATOCOLUMNWIDTH; 
	rlHeader.Text = GetString(IDS_SECTABLEVIEWER_VALIDTO);
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);

	// VATO header
	rlHeader.Length = FREQCOLUMNWIDTH; 
	rlHeader.Text = GetString(IDS_SECTABLEVIEWER_FREQ);
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);


	if( bmDisplayProfile )
	{
		// PROF header
		rlHeader.Length = PROFCOLUMNWIDTH; 
		rlHeader.Text = GetString(IDS_SECTABLEVIEWER_PROFILEHEADER);
		omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	}

	// REMA header
	rlHeader.Length = REMACOLUMNWIDTH; 
	rlHeader.Text = GetString(IDS_SECTABLEVIEWER_REMARK);
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);

	pomTable->SetHeaderFields(omHeaderDataArray);
	omHeaderDataArray.DeleteAll();
	
	pomTable->SetDefaultSeparator();
	
   // step 2 set linedata
	CCSPtrArray<TABLE_COLUMN> olColList;

	int ilNumLines = omLines.GetSize();
	for (int ilLc = 0; ilLc < ilNumLines; ilLc++)
	{
		SECTAB_LINEDATA *prlLine = &omLines[ilLc];

		if(prlLine->IsSysAdmin)
		{
			omColumnData.BkColor = AQUA;
		}
		else
		{
			omColumnData.BkColor = WHITE;
		}

		omColumnData.Text = prlLine->USID;
		olColList.NewAt(olColList.GetSize(), omColumnData);

		omColumnData.Text = prlLine->NAME;
		olColList.NewAt(olColList.GetSize(), omColumnData);

		omColumnData.Text = prlLine->STAT;;
		olColList.NewAt(olColList.GetSize(), omColumnData);

		omColumnData.Text = prlLine->VAFR;
		olColList.NewAt(olColList.GetSize(), omColumnData);

		omColumnData.Text = prlLine->VATO;
		olColList.NewAt(olColList.GetSize(), omColumnData);

		omColumnData.Text = prlLine->FREQ;
		olColList.NewAt(olColList.GetSize(), omColumnData);

		if( bmDisplayProfile ) {
			omColumnData.Text = prlLine->PROF;
			olColList.NewAt(olColList.GetSize(), omColumnData);
		}

		omColumnData.Text = prlLine->REMA;
		olColList.NewAt(olColList.GetSize(), omColumnData);

		pomTable->AddTextLine(olColList, (void*)prlLine);

		// disable drag&drop for each line
		pomTable->SetTextLineDragEnable(ilLc,FALSE);

		olColList.DeleteAll();
	}

	pomTable->DisplayTable();


	// if there are data then select the first line, else display a message
	if( ilNumLines > 0 )
		pomTable->SelectLine(0);
	else
	{
		// if the alloc dialog is open and there are no more records then close it
		if( pogAllocDlg != NULL )
			pogAllocDlg->SendCancel();

		if(bmOtherSuperusersFound)
		{
			CString olErr;
			olErr.Format(GetString(IDS_CANONLYVIEWNONSUPERUSERS),ogUsername);
			pomTable->MessageBox(olErr,"",MB_ICONINFORMATION);
		}
		else
		{
			pomTable->MessageBox(olNoDataMsg,"",MB_ICONINFORMATION);
		}
	}

}

int SecTableViewer::FindLine(const char *pcpUrno)
{
	int ilLine = -1;

	int ilCount = omLines.GetSize();
	for(int ilLc = 0; ilLine == -1 && ilLc < ilCount; ilLc++)
	{
		if(!strcmp(omLines[ilLc].URNO,pcpUrno))
		{
			ilLine = ilLc;
		}
	}
	return ilLine;
}


static void SecTableViewerCf(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
    SecTableViewer *polThis = (SecTableViewer *)popInstance;

	switch(ipDDXType) {

	case SEC_VIEWER_INSERT:
		polThis->Insert((char *)vpDataPointer);
		break;
	case SEC_VIEWER_UPDATE:
        polThis->Update((char *)vpDataPointer);
		break;
	case SEC_VIEWER_DELETE:
        polThis->Delete((char *)vpDataPointer);
		break;
	case INSERT_SEC:
		polThis->HandleSecInsert((SECDATA *)vpDataPointer);
		break;
	case UPDATE_SEC:
		polThis->HandleSecUpdate((SECDATA *)vpDataPointer);
		break;
	case BC_SBC_DELSEC:
		// SBC DELSEC is handled here instead of SEC DRT because
		// CAllocatedDlg needs to close itself before a new line
		// is selected on the main screen. Therefore CAllocatedDlg
		// responds to SEC DRT which is sent before SBC DELSEC
		polThis->HandleDelSec((struct BcStruct *) vpDataPointer);
		break;
	case BC_SBC_NEWREL:
		polThis->HandleNewRel((struct BcStruct *) vpDataPointer);
		break;
	case BC_SBC_DELREL:
		polThis->HandleDelRel((struct BcStruct *) vpDataPointer);
		break;
	case INSERT_CAL:
	case UPDATE_CAL:
	case DELETE_CAL:
		polThis->HandleCal((CALDATA *)vpDataPointer);
		break;
	case INSERT_GRP:
	case UPDATE_GRP:
	case DELETE_GRP:
		polThis->HandleGrp((GRPDATA *)vpDataPointer);
		break;
	default:
		break;
	}
}

void SecTableViewer::HandleSecInsert(SECDATA *prpSec)
{
//	MakeLine(prpSec);
//	int ilLine = FindLine(prpSec->URNO);
//	if(ilLine != -1)
//	{
//		pomTable->InsertTextLine(ilLine,Format(&omLines[ilItem]), &omLines[ilItem]);
//		pomTable->DisplayTable();
//	}
	Insert(prpSec->URNO, false);
}

void SecTableViewer::HandleSecUpdate(SECDATA *prpSec)
{
	Update(prpSec->URNO);
}

void SecTableViewer::HandleDelSec(struct BcStruct *prpBcStruct)
{
	Delete(prpBcStruct->Data); // contains the SECTAB.URNO
}

void SecTableViewer::HandleNewRel(struct BcStruct *prpBcStruct)
{
	CStringArray olFsecs;
	ExtractItemList(prpBcStruct->Data, &olFsecs);
	for(int i = 0; i < olFsecs.GetSize(); i++)
	{
		Update(olFsecs[i]);
	}
}

void SecTableViewer::HandleDelRel(struct BcStruct *prpBcStruct)
{
	SECTAB_LINEDATA *prlLine = NULL;
	CStringArray olUrnos;
	ExtractItemList(prpBcStruct->Data, &olUrnos);
	for(int i = 0; i < olUrnos.GetSize(); i++)
	{
		for(int l = 0; l < omLines.GetSize(); l++)
		{
			prlLine = &omLines[l];
			if(prlLine->Fgrp == olUrnos[i])
			{
				Update(prlLine->URNO);
				break;
			}
		}
	}
}

void SecTableViewer::HandleSecDelete(SECDATA *prpSec)
{
	Delete(prpSec->URNO);
}

void SecTableViewer::HandleCal(CALDATA *prpCal)
{
	SECDATA *prlSec = ogCedaSecData.GetSecByUrno(prpCal->FSEC);
	if(prlSec != NULL)
	{
		Update(prlSec->URNO);
	}
}

void SecTableViewer::HandleGrp(GRPDATA *prpGrp)
{
	SECDATA *prlSec = NULL;
	if((prlSec = ogCedaSecData.GetSecByUrno(prpGrp->FREL)) != NULL)
	{
		Update(prlSec->URNO);
	}
	if((prlSec = ogCedaSecData.GetSecByUrno(prpGrp->FSEC)) != NULL)
	{
		Update(prlSec->URNO);
	}
}

void SecTableViewer::SelectLine(const char *pcpUrno)
{
	int ilLine = FindLine(pcpUrno);

	// select the line indexed by pcpUsid
	if(ilLine != -1)
	{
		pomTable->SelectLine(ilLine);
	}
}

CString SecTableViewer::GetSelectedUrno()
{
	CString olSelectedUrno;
	int ilSelLine = pomTable->GetCurrentLine();
	if(ilSelLine != -1)
	{
		SECTAB_LINEDATA *prlSelLine = (SECTAB_LINEDATA *) pomTable->GetTextLineData(ilSelLine);
		olSelectedUrno = prlSelLine->URNO;
	}

	return olSelectedUrno;
}

void SecTableViewer::Update(const char *pcpUrno)
{
	int ilLine = FindLine(pcpUrno);
	bool blRc = true;

	if(ilLine != -1)
	{
		CString olSelectedUrno = GetSelectedUrno();

		CListBox *prlListBox = pomTable->GetCTableListBox();
		if(prlListBox == NULL)
			return;

		int ilTopIndex = prlListBox->GetTopIndex();

		// get the SEC rec
		SECDATA *prlSecRec;
		prlSecRec = ogCedaSecData.GetSecByUrno(pcpUrno);

		if( prlSecRec != NULL )
		{
			SECTAB_LINEDATA *prlLine = &omLines[ilLine];

			// read the current line characteristics
			CCSPtrArray <TABLE_COLUMN> rlLine;
			pomTable->GetTextLineColumns(&rlLine, ilLine);

			// update columns 0 to 4
			rlLine[0].Text = prlSecRec->USID;
			prlLine->USID = prlSecRec->USID;
			rlLine[1].Text = prlSecRec->NAME;
			prlLine->NAME = prlSecRec->NAME;


			// get the celendar dates
			ogCedaCalData.GetDatesByFsec(pcpUrno,rlLine[3].Text,rlLine[4].Text,rlLine[5].Text);	
			prlLine->VAFR = rlLine[3].Text;
			prlLine->VATO = rlLine[4].Text;
			prlLine->FREQ = rlLine[5].Text;

			COLORREF rlBkColor;
			if(ogCedaSecData.IsSuperuser(prlSecRec) || !strcmp(prlSecRec->USID,ogSuperUser))
			{
				rlBkColor = AQUA;
				prlLine->IsSysAdmin = true;
			}
			else
			{
				rlBkColor = WHITE;
				prlLine->IsSysAdmin = false;
			}
			int ilNumCols = rlLine.GetSize();
			for(int ilCol = 0; ilCol < ilNumCols; ilCol++)
			{
				rlLine[ilCol].BkColor = rlBkColor;
			}

			// update column 3 (STAT)
			if( strcmp(prlSecRec->STAT,"1") == 0 )
				rlLine[2].Text = omEnabledMess; // if STAT is '1' set text displayed to "Enabled"
			else if( strcmp(prlSecRec->STAT,"0") == 0 )
				rlLine[2].Text = omDisabledMess; // if STAT is '0' set text displayed to "Disabled"
			else
				rlLine[2].Text = omUnknownMess; // else set text displayed to "Unknown"

			prlLine->STAT = rlLine[2].Text;

			if( bmDisplayProfile )
			{
				rlLine[6].Text = "";
				GRPDATA *prlGrpRec = ogCedaGrpData.GetGrpByFsecType(pcpUrno,SEC_PROFILE);

				// if a user has a non-personal profile, update the profile field
				if( prlGrpRec != NULL && strcmp(prlGrpRec->FREL,prlGrpRec->FSEC))
				{
					SECDATA *prlPrfRec = ogCedaSecData.GetSecByUrno(prlGrpRec->FREL);
					if( prlPrfRec != NULL )
					{
						rlLine[6].Text = prlPrfRec->USID;
						prlLine->Fgrp = prlPrfRec->URNO;
					}
				}
				rlLine[7].Text = prlSecRec->REMA;

				prlLine->PROF = rlLine[6].Text;
				prlLine->REMA = rlLine[7].Text;
			}
			else
			{
				rlLine[6].Text = prlSecRec->REMA;
				prlLine->REMA = rlLine[6].Text;
			}


			// refresh the table
			pomTable->DisplayTable();
		}
		// reselect the originally selected line
		int ilNumLines = omLines.GetSize();
		if(ilNumLines > 0)
		{
			if(!olSelectedUrno.IsEmpty())
			{
				SelectLine(olSelectedUrno);
			}
			if(ilTopIndex >= ilNumLines)
				ilTopIndex = ilNumLines;
			prlListBox->SetTopIndex(ilTopIndex);
		}
	}
}

void SecTableViewer::Insert(char *pcpUrno, bool bpSelectInsertedLine /* = true */)
{
	CListBox *prlListBox = pomTable->GetCTableListBox();
	if(prlListBox == NULL)
		return;

	int ilTopIndex = prlListBox->GetTopIndex();

	CString olSelectedUrno = GetSelectedUrno();

	// update the table
	ChangeViewTo();

	int ilNumLines = omLines.GetSize();

	// select the line of the newly inserted USID
	if(ilNumLines > 0)
	{
		if(bpSelectInsertedLine)
		{
			SelectLine(pcpUrno);
		}
		else if(!olSelectedUrno.IsEmpty())
		{
			SelectLine(olSelectedUrno);
		}
		if(ilTopIndex >= ilNumLines)
			ilTopIndex = ilNumLines;
		prlListBox->SetTopIndex(ilTopIndex);
	}
}

void SecTableViewer::Delete(char *pcpUrno)
{
	CString olSelectedUrno = GetSelectedUrno();
	int ilLine = FindLine(pcpUrno);

	CListBox *prlListBox = pomTable->GetCTableListBox();
	if(prlListBox == NULL)
		return;

	int ilTopIndex = prlListBox->GetTopIndex();

	// delete the line from local mem
	if(ilLine != -1)
	{
		DeleteLine(ilLine);
	}

	// update the table
	ChangeViewTo();

	int ilNumLines = omLines.GetSize();

	// reselect the line
	if( ilNumLines > 0 )
	{
		if(!olSelectedUrno.IsEmpty())
		{
			SelectLine(olSelectedUrno);
		}
		else if(ilLine < ilNumLines)
		{
			pomTable->SelectLine(ilLine);
		}
		else
		{
			pomTable->SelectLine(ilLine-1);
		}
		if(ilTopIndex >= ilNumLines)
			ilTopIndex = ilNumLines;
		prlListBox->SetTopIndex(ilTopIndex);
	}
}

void SecTableViewer::Print(CString opCaption, CWnd *popParent)
{
	int ilSelection = -1;
	CPrintOption printOption(CWnd::FromHandle(AfxGetApp()->m_pMainWnd->m_hWnd));	
	if(IsExcelConfigured() == TRUE)
	{
		if((printOption.DoModal() == IDOK))	
			ilSelection = printOption.GetSelection();
	}
	else
	{
		ilSelection = OPTIONPRINT;
	}

	if(ilSelection == OPTIONPRINT)
	{
	CString olFooter1,olFooter2;
	int ilOrientation = PRINT_LANDSCAPE;//  PRINT_PORTRAET  PRINT_LANDSCAPE

	pomPrint = NULL;
	pomPrint = new CCSPrint(popParent,ilOrientation,45);// FirstLine(P=200,L=200),LeftOffset(P=200,L=50)

	if (pomPrint != NULL)
	{
		if (pomPrint->InitializePrinter(&ilOrientation) == TRUE)
		{
			int ilLinesPerPage = ilOrientation == PRINT_PORTRAET ? 57 : 38;

			pomPrint->imLineNo = ilLinesPerPage + 1;
			DOCINFO	rlDocInfo;
			memset(&rlDocInfo, 0, sizeof(DOCINFO));
			rlDocInfo.cbSize = sizeof( DOCINFO );
			rlDocInfo.lpszDocName = opCaption;	
			pomPrint->omCdc.StartDoc( &rlDocInfo );
			pomPrint->imPageNo = 0;

			int ilLines = 	omLines.GetSize();
			int ilTotalPages = (((int) ilLines-1) / (int) ilLinesPerPage) + 1;
			int ilPageCount = 1;

			pomPrint->imLineNo = 0;
			olFooter1 = CTime::GetCurrentTime().Format("%d.%m.%Y %H:%M");
			for(int ilCurrLine = 0; ilCurrLine < ilLines; ilCurrLine++ ) 
			{
				if(pomPrint->imLineNo >= ilLinesPerPage)
				{
					// end of page reached
					olFooter2.Format("%d/%d",ilPageCount++,ilTotalPages);
					pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
					pomPrint->omCdc.EndPage();
					pomPrint->imLineNo = 0;
				}

				if( pomPrint->imLineNo == 0 )
				{
					// print the header at the start and when the page has changed
					pomPrint->PrintUIFHeader(opCaption,"",pomPrint->imFirstLine-10);
				}

				// print a line
				PrintLine(&omLines[ilCurrLine], ilOrientation);
			}

			olFooter2.Format("%d/%d",ilPageCount++,ilTotalPages);
			pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
			pomPrint->omCdc.EndPage();
			pomPrint->omCdc.EndDoc();
		}

		delete pomPrint;
		pomPrint = NULL;
	}
	}

	else if(ilSelection == OPTIONEXCEL)
	{		
		try
		{
			GenerateExcelSheet();
		}
		catch(CString& ropMessage)
		{
			AfxMessageBox(ropMessage);
		}
		catch(...)
		{
			AfxMessageBox("Unknown error occurred in generating excel sheet");			
		}			
	}
	AfxGetApp()->DoWaitCursor(1);
}

void SecTableViewer::GenerateExcelSheet()
{
	CTime olCurrentTime = CTime::GetCurrentTime();
	CString olFileName;
	CString olTitle;
	AfxGetApp()->m_pMainWnd->GetWindowText(olTitle);
	olTitle.Remove(' ');
	olFileName.Format("%s\\%s_%s.csv", CCSLog::GetTmpPath(),olTitle,olCurrentTime.Format("%Y%m%d_%H%M%S"));

	CreateExcelSheet(olFileName);

	char pclTmp[256];
	strcpy(pclTmp, olFileName); 

   /* Set up parameters to be sent: */
	char *args[4];
	args[0] = "child";
	args[1] = pclTmp;
	args[2] = NULL;
	args[3] = NULL;

	int ilReturn = 0;
	
	if(!m_ExcelPath.IsEmpty())
		ilReturn = _spawnv( _P_NOWAIT , m_ExcelPath, args );	
	else
		throw EXCEL_PATH_NOT_FOUND;

	if(ilReturn == -1)
		throw EXCEL_PATH_NOT_CORRECT;	
}

BOOL SecTableViewer::IsExcelConfigured()
{
	char pclConfigPath[256];
	char pclSeparator[64];
	char pclExcelPath[256];

	CString olTempPath = CCSLog::GetTmpPath();

	if (getenv(CEDA) == NULL)
		strcpy(pclConfigPath,CONFIG_PATH ); 
	else
		strcpy(pclConfigPath, getenv(CEDA));

	GetPrivateProfileString(pcgAppName, EXCEL, DEFAULT,pclExcelPath, sizeof pclExcelPath, pclConfigPath);
	GetPrivateProfileString(pcgAppName, EXCELSEPARATOR,CString(SEPARATOR),pclSeparator, sizeof pclSeparator, pclConfigPath);
	if (strcmp(pclExcelPath, "DEFAULT") == 0)
	{
		return FALSE;
	}
	m_Separator = pclSeparator;
	m_Separator.TrimLeft();
	m_Separator.TrimRight();	
	m_ExcelPath= pclExcelPath;
	return TRUE;
}

void SecTableViewer::CreateExcelSheet(const CString& rolFileName)
{
	CString olLine;
	CString olTitle;
	CString olHeader;
	SECTAB_LINEDATA* prlLine = NULL;
	pomTable->GetWindowText(olTitle);

	ofstream of;
	of.open(rolFileName, ios::out);

	//Title
	if(!olTitle.IsEmpty())
		of << setw(0) << olTitle << endl;

	olHeader.Format("%s%s%s%s%s%s%s%s%s%s%s%s%s",
					GetString(IDS_SECTABLEVIEWER_USERNAME),m_Separator,
					GetString(IDS_SECTABLEVIEWER_NAME),m_Separator,
					GetString(IDS_SECTABLEVIEWER_STATUS),m_Separator,
					GetString(IDS_SECTABLEVIEWER_VALIDFROM),m_Separator,
					GetString(IDS_SECTABLEVIEWER_VALIDTO),m_Separator,
					GetString(IDS_SECTABLEVIEWER_FREQ),m_Separator,
					(bmDisplayProfile ? GetString(IDS_SECTABLEVIEWER_PROFILEHEADER) + m_Separator : ""));
	olHeader += GetString(IDS_SECTABLEVIEWER_REMARK);
	
	//Name of the fields
	of  << setw(0) << olHeader << endl;
	
	for(int iLine = 0 ; iLine < omLines.GetSize(); iLine++)
	{
		prlLine = &omLines[iLine];
		olLine.Format("%s%s%s%s%s%s%s%s%s%s%s%s%s",
		              ReplaceSeparator(prlLine->USID),m_Separator,ReplaceSeparator(prlLine->NAME),m_Separator,
					  ReplaceSeparator(prlLine->STAT),m_Separator,ReplaceSeparator(prlLine->VAFR),m_Separator,
					  ReplaceSeparator(prlLine->VATO),m_Separator,ReplaceSeparator(prlLine->FREQ),m_Separator,
					  (bmDisplayProfile ? prlLine->PROF + m_Separator : ""));
		olLine += prlLine->REMA;
		of << setw(0) << olLine << endl;
	}

	of.close();
}

inline CString SecTableViewer::ReplaceSeparator(CString opField)
{
	opField.Replace(m_Separator," ");	
	return opField;
}

bool SecTableViewer::PrintLine(SECTAB_LINEDATA *prpLine, int ipOrientation)
{
	bool blRc = false;

	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;
	int ilNumChars;

	rlElement.pFont      = &pomPrint->ogCourierNew_Regular_8;
	rlElement.Alignment  =  PRINT_LEFT;
	if(prpLine->IsSysAdmin)
	{
		rlElement.FrameTop   =  PRINT_FRAMETHICK;
		rlElement.FrameBottom = PRINT_FRAMETHICK;
		rlElement.FrameLeft  =  PRINT_FRAMETHICK;
		rlElement.FrameRight =  PRINT_FRAMETHICK;
	}
	else
	{
		rlElement.FrameTop   =  PRINT_FRAMETHIN;
		rlElement.FrameBottom = PRINT_FRAMETHIN;
		rlElement.FrameLeft  =  PRINT_FRAMETHIN;
		rlElement.FrameRight =  PRINT_FRAMETHIN;
	}

	ilNumChars = ipOrientation == PRINT_LANDSCAPE ? 40 : 35;
	rlElement.Length = (int)((ilNumChars*5)*dgCCSPrintFactor);
	rlElement.Text = CString(prpLine->USID);
	rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

	if(ipOrientation == PRINT_LANDSCAPE)
	{
		ilNumChars = 65;
		rlElement.Length = (int)((ilNumChars*5)*dgCCSPrintFactor);
		rlElement.Text = CString(prpLine->NAME);
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
	}

	ilNumChars = 20;
	rlElement.Length = (int)((ilNumChars*5)*dgCCSPrintFactor);
	rlElement.Text = CString(prpLine->STAT);
	rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

	ilNumChars = 30;
	rlElement.Length = (int)((ilNumChars*5)*dgCCSPrintFactor);
	rlElement.Text = CString(prpLine->VAFR);
	rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

	ilNumChars = 30;
	rlElement.Length = (int)((ilNumChars*5)*dgCCSPrintFactor);
	rlElement.Text = CString(prpLine->VATO);
	rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

	ilNumChars = 16;
	rlElement.Length = (int)((ilNumChars*5)*dgCCSPrintFactor);
	rlElement.Text = CString(prpLine->FREQ);
	rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

	ilNumChars = ipOrientation == PRINT_LANDSCAPE ? 40 : 35;
	rlElement.Length = (int)((ilNumChars*5)*dgCCSPrintFactor);
	rlElement.Text = CString(prpLine->PROF);
	rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);


	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();

	blRc = true;

	return blRc;
}
