// dbtnlist.cpp : implementation file
//
#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#include <stdafx.h>
#include <CCSGlobl.h>
#include <resource.h>
#include <BDPS_SEC.h>
#include <CCSCedacom.h>
#include <CCSCedadata.h>
#include <CCSButtonCtrl.h>
#include <Ufis.h>
#include <CCSBCHandle.h>
#include <ButtonListDlg.h>
#include <CCSDdx.h>
#include <BasicData.h>
#include <EditUserDlg.h>
#include <AllocatedDlg.h>
#include <ObjDialog.h>
#include <AboutSecDlg.h>
#include <resrc1.h>


#include <CedaSecData.h>
#include <CedaCalData.h>
#include <CedaGrpData.h>
#include <CedaPrvData.h>
#include <CedaFktData.h>

#ifdef _DEBUG
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif

CTime ogPrePlanTime;

extern void DeleteBrushes(void);
extern CBDPS_SECApp theApp;

// the broadcast CallBack function, has to be outside the ButtonList class
void ProcessCf(void *vpInstance,enum enumDDXTypes ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//MWO: make it flexible, because when BCCOM is connected the timer value is 5 minutes
//otherwise 1 minute.
long imTimerValue = 60000;

/////////////////////////////////////////////////////////////////////////////
// CButtonListDialog dialog

CButtonListDialog::CButtonListDialog(CWnd* pParent /*=NULL*/)
    : CDialog(CButtonListDialog::IDD, pParent)
{
    //{{AFX_DATA_INIT(CButtonListDialog)
	//}}AFX_DATA_INIT


	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	m_hIcon = AfxGetApp()->LoadIcon(IDI_UFIS);
	pomTable = new CCSTable;
	pomTable->SetSelectMode(0);
	pogAllocDlg = NULL; // pointer to modeless alloc dialog
	prmPrevSelLine = NULL;

	imBroadcastState = -1;
	bmBroadcastReceived = false;
	bmBroadcastCheckMsg = true;
}

CButtonListDialog::~CButtonListDialog(void)
{
	delete pomTable;
	if (::IsWindow(m_hWnd))
	{
		if(imBroadcastState != -1) // used
			KillTimer(3);
	}
	OnDestroy();
}

void CButtonListDialog::RegisterTimer(CWnd *popWnd)
{
	if (popWnd != pomWndRequireTimer)
	{
		if (::IsWindow(pomWndRequireTimer->GetSafeHwnd()))
			pomWndRequireTimer->PostMessage(WM_TIMER, 0, NULL);	
	}
	pomWndRequireTimer = popWnd;
}

void CButtonListDialog::UnRegisterTimer(CWnd *popWnd)
{
	if (popWnd == pomWndRequireTimer)
	{
		pomWndRequireTimer = NULL;
	}
}


void CButtonListDialog::DoDataExchange(CDataExchange* pDX)
{
    CDialog::DoDataExchange(pDX);
    //{{AFX_DATA_MAP(CButtonListDialog)
	DDX_Control(pDX, IDC_STATUSBAR, m_StatusBar);
	DDX_Control(pDX, ID_InsertButton, m_InsertButton);
	DDX_Control(pDX, ID_UpdateButton, m_UpdateButton);
	DDX_Control(pDX, ID_DeleteButton, m_DeleteButton);
	DDX_Control(pDX, ID_PasswordButton, m_PasswordButton);
	DDX_Control(pDX, ID_RightsButton, m_RightsButton);
	DDX_Control(pDX, ID_AllocateButton, m_AllocateButton);
	DDX_Control(pDX, IDC_BC_STATUS, m_CB_BCStatus);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CButtonListDialog, CDialog)
    //{{AFX_MSG_MAP(CButtonListDialog)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_WM_CREATE()
	ON_WM_DESTROY()
    ON_WM_ERASEBKGND()
	ON_WM_SIZE()
	ON_WM_CLOSE()
	ON_BN_CLICKED(ID_InsertButton, OnInsertButton)
	ON_BN_CLICKED(ID_UpdateButton, OnUpdateButton)
	ON_BN_CLICKED(ID_DeleteButton, OnDeleteButton)
	ON_BN_CLICKED(ID_PasswordButton, OnPasswordButton)
	ON_BN_CLICKED(ID_RightsButton, OnRightsButton)
	ON_BN_CLICKED(ID_AllocateButton, OnAllocateButton)
	ON_COMMAND(ID_ExitBDPSSEC, OnExitBDPSSEC)
	ON_COMMAND(ID_User, OnUser)
	ON_COMMAND(ID_Profile, OnProfile)
	ON_COMMAND(ID_Application, OnApplication)
	ON_COMMAND(ID_Group, OnGroup)
	ON_COMMAND(ID_Wks, OnWks)
	ON_COMMAND(ID_WksGroup, OnWksGroup)
	ON_MESSAGE(WM_TABLE_LBUTTONDBLCLK,OnTableLButtonDblClk)
	ON_MESSAGE(WM_TABLE_LBUTTONDOWN,OnTableLButtonDown)
	ON_MESSAGE(WM_TABLE_RBUTTONDOWN,OnTableRButtonDown)
	ON_MESSAGE(WM_TABLE_SELCHANGE,OnTableSelChange)
	ON_BN_CLICKED(ID_TestButton, OnTestButton)
	ON_BN_CLICKED(ID_ExitButton, OnExitButton)
	ON_COMMAND(ID_InsertButton, OnInsertButton)
	ON_COMMAND(ID_UpdateButton, OnUpdateButton)
	ON_COMMAND(ID_DeleteButton, OnDeleteButton)
	ON_COMMAND(ID_PasswordButton, OnPasswordButton)
	ON_COMMAND(ID_RightsButton, OnRightsButton)
	ON_COMMAND(ID_AllocateButton, OnAllocateButton)
	ON_COMMAND(IDD_ABOUTBOX, OnAppAbout)
	ON_WM_TIMER()
    ON_WM_CTLCOLOR()
	ON_BN_CLICKED(ID_ProfileButton, OnProfile)
	ON_BN_CLICKED(ID_ModuleButton, OnApplication)
	ON_BN_CLICKED(ID_UserButton, OnUser)
	ON_BN_CLICKED(ID_GroupButton, OnGroup)
	ON_BN_CLICKED(ID_WksButton, OnWks)
	ON_BN_CLICKED(ID_WksGroupButton, OnWksGroup)
	ON_BN_CLICKED(ID_PrintButton, OnPrintButton)
	ON_MESSAGE(WM_BCADD,OnBcAdd)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// CButtonListDialog message handlers

BOOL CButtonListDialog::OnInitDialog()
{
	CDialog::OnInitDialog();

	// load the menu for this dialogue
	omMenu.LoadMenu(IDR_BDPSSEC_MENU);
	SetMenu(&omMenu);
	CRect olInitRect;

	CWnd *polInsertButton = this->GetDlgItem(ID_InsertButton);
	if (polInsertButton != NULL)
	{
		polInsertButton->GetWindowRect(olInitRect);
		this->ScreenToClient(olInitRect);
		m_nDialogBarHeight = olInitRect.bottom;
	}
	else
	{
		GetClientRect(&olInitRect);
		m_nDialogBarHeight = olInitRect.bottom - olInitRect.top + 30;
	}

	// changed bch 10.03.98 to set the dialog size to a single screen (for mu�ltiple screens)
    // calculate the window height
    //CRect rectScreen(0, 0, ::GetSystemMetrics(SM_CXSCREEN), ::GetSystemMetrics(SM_CYSCREEN)); //  ::GetSystemMetrics(SM_CYSCREEN));
    //MoveWindow(&rectScreen, true);


    // calculate the window height
    CRect olScreenRect;
    GetClientRect(&olScreenRect);

    // extend dialog window to current screen width
    olScreenRect.left = 0;
    olScreenRect.top = 0;
    olScreenRect.right = 1024;
    olScreenRect.bottom = 768;
    MoveWindow(&olScreenRect);
	ShowWindow(SW_SHOW);
	// end changed bch 10.03.98

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, true);			// Set big icon
	SetIcon(m_hIcon, false);		// Set small icon

	char pclConfigPath[142];
    if (getenv("CEDA") == NULL)
        strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
    else
        strcpy(pclConfigPath, getenv("CEDA"));

/*
	char pclUseBcProxy[256];
	GetPrivateProfileString("GLOBAL", "BCEVENTTYPE", "BCSERV",pclUseBcProxy, sizeof pclUseBcProxy, pclConfigPath);

	//MWO/RRO 25.03.03
	if (stricmp(pclUseBcProxy,"BCPROXY") == 0 || stricmp(pclUseBcProxy,"BCCOM") == 0)
	{
		//MWO/RRO
		//ogCommHandler.RegisterBcWindow(this);
		ogBcHandle.StartBc(); 
		//END MWO/RRO
	}
	else
	{
		ogCommHandler.RegisterBcWindow(this);
	}
*/
	char pclUseBcProxy[256];
	GetPrivateProfileString("GLOBAL", "BCEVENTTYPE", "BCSERV",pclUseBcProxy, sizeof pclUseBcProxy, pclConfigPath);

	if (stricmp(pclUseBcProxy,"BCSERV") == 0)
	{
		ogCommHandler.RegisterBcWindow(this);
	}
	else
	{
		ogBcHandle.StartBc(); 
	}

	if (stricmp(pclUseBcProxy,"BCCOM") == 0)
	{
		imTimerValue = 60 * 5000;
	}

	ogBcHandle.GetBc();


	// printer style ???
	theApp.SetLandscape();


	// set the size of the CCSTable and thus ultimately the size of the whole screen
	CRect rect;
	GetClientRect(&rect);
	rect.OffsetRect(0, m_nDialogBarHeight);
	rect.InflateRect(1, 1);     // hiding the CTable window border
	pomTable->SetTableData(this, rect.left, rect.right, rect.top, (rect.bottom-STATBARWIDTH));
	m_StatusBar.SetWindowPos((CWnd*)pomTable,rect.left,(rect.bottom-STATBARWIDTH),(rect.right-rect.left),STATBARWIDTH,0);


	// attach the CCSTable to the user list
	omSecTableViewer.Attach(pomTable);

	GetDlgItem(ID_ModuleButton)->SetWindowText(GetString(IDS_BL_MODULE));
	GetDlgItem(ID_ProfileButton)->SetWindowText(GetString(IDS_BL_PROFILE));
	GetDlgItem(ID_UserButton)->SetWindowText(GetString(IDS_BL_USER));
	GetDlgItem(ID_GroupButton)->SetWindowText(GetString(IDS_BL_GROUP));
	GetDlgItem(ID_WksButton)->SetWindowText(GetString(IDS_BL_WKS));
	GetDlgItem(ID_WksGroupButton)->SetWindowText(GetString(IDS_BL_WKSGROUP));
	GetDlgItem(ID_InsertButton)->SetWindowText(GetString(IDS_BL_INSERT));
	GetDlgItem(ID_UpdateButton)->SetWindowText(GetString(IDS_BL_UPDATE));
	GetDlgItem(ID_DeleteButton)->SetWindowText(GetString(IDS_BL_DELETE));
	GetDlgItem(ID_RightsButton)->SetWindowText(GetString(IDS_BL_RIGHTS));
	GetDlgItem(ID_AllocateButton)->SetWindowText(GetString(IDS_BL_ALLOCATE));
	GetDlgItem(ID_PasswordButton)->SetWindowText(GetString(IDS_BL_PASSWORD));
	GetDlgItem(ID_PrintButton)->SetWindowText(GetString(IDS_BL_PRINT));
	GetDlgItem(ID_ExitButton)->SetWindowText(GetString(IDS_BL_EXIT));

	omMenu.ModifyMenu(0,MF_STRING|MF_BYPOSITION,MF_STRING,GetString(IDS_FUNCTIONS));
	omMenu.ModifyMenu(1,MF_STRING|MF_BYPOSITION,MF_STRING,GetString(IDS_HELP));

	CMenu *prlSubMenu = omMenu.GetSubMenu(0);
	if(prlSubMenu != NULL)
	{
		prlSubMenu->ModifyMenu(ID_Application,MF_STRING|MF_BYCOMMAND,ID_Application,GetString(IDS_BL_MODULE));
		prlSubMenu->ModifyMenu(ID_Profile,MF_STRING|MF_BYCOMMAND,ID_Profile,GetString(IDS_BL_PROFILE));
		prlSubMenu->ModifyMenu(ID_User,MF_STRING|MF_BYCOMMAND,ID_User,GetString(IDS_BL_USER));
		prlSubMenu->ModifyMenu(ID_Group,MF_STRING|MF_BYCOMMAND,ID_Group,GetString(IDS_BL_GROUP));
		prlSubMenu->ModifyMenu(ID_Wks,MF_STRING|MF_BYCOMMAND,ID_Wks,GetString(IDS_BL_WKS));
		prlSubMenu->ModifyMenu(ID_WksGroup,MF_STRING|MF_BYCOMMAND,ID_WksGroup,GetString(IDS_BL_WKSGROUP));
		prlSubMenu->ModifyMenu(ID_ExitBDPSSEC,MF_STRING|MF_BYCOMMAND,ID_ExitBDPSSEC,GetString(IDS_BL_EXIT));
	}

	prlSubMenu = omMenu.GetSubMenu(1);
	if(prlSubMenu != NULL)
	{
		prlSubMenu->ModifyMenu(IDD_HELP_CONTENTS,MF_STRING|MF_BYCOMMAND,IDD_HELP_CONTENTS,GetString(IDS_HELPCONTENTS));
		prlSubMenu->ModifyMenu(IDD_HELP_USING,MF_STRING|MF_BYCOMMAND,IDD_HELP_USING,GetString(IDS_GENERAL));
		prlSubMenu->ModifyMenu(IDD_ABOUTBOX,MF_STRING|MF_BYCOMMAND,IDD_ABOUTBOX,GetString(IDS_ABOUT));
	}

	// display initially the list of users
	rgCurrType = SEC_USER;
	UpdateView();


	// enable/disable objects for this user
/*
	m_InsertButton.EnableWindow(ogPrivList.IsEnabled("Main Screen","Insert"));;
	m_UpdateButton.EnableWindow(ogPrivList.IsEnabled("Main Screen","Update"));;
	m_DeleteButton.EnableWindow(ogPrivList.IsEnabled("Main Screen","Delete"));;
	m_RightsButton.EnableWindow(ogPrivList.IsEnabled("Main Screen","Rights"));;
	m_AllocateButton.EnableWindow(ogPrivList.IsEnabled("Main Screen","Allocate"));;
*/

	
	// get a pointer to this dialogue's menu
	CMenu *polMenu = this->GetMenu();

	if (polMenu != NULL )
	{
/*
		// "User" menu item
		if(ogPrivList.IsEnabled("Main Screen","User"))
			polMenu->EnableMenuItem(ID_User,MF_ENABLED);
		else
			polMenu->EnableMenuItem(ID_User,MF_GRAYED);

		// "Profile" menu item
		if(ogPrivList.IsEnabled("Main Screen","Profile"))
			polMenu->EnableMenuItem(ID_Profile,MF_ENABLED);
		else
			polMenu->EnableMenuItem(ID_Profile,MF_GRAYED);

		// "Exit" menu item
		if(ogPrivList.IsEnabled("Main Screen","Exit"))
			polMenu->EnableMenuItem(ID_ExitBDPSSEC,MF_ENABLED);
		else
			polMenu->EnableMenuItem(ID_ExitBDPSSEC,MF_GRAYED);

		// "About BDPS_SEC" menu item
		if(ogPrivList.IsEnabled("Main Screen","About BDPS_SEC"))
			polMenu->EnableMenuItem(IDD_ABOUTBOX,MF_ENABLED);
		else
			polMenu->EnableMenuItem(IDD_ABOUTBOX,MF_GRAYED);
*/
	}

	SetTimer(3, imTimerValue,NULL);
	imBroadcastState = 0;
	m_CB_BCStatus.SetColors(TRAFFIC_LIGHT_GREEN, ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
		
    return true;  // return TRUE  unless you set the focus to a control

}

void CButtonListDialog::OnTimer(UINT nIDEvent) 
{
	if (nIDEvent == 3)	// watch dog event
	{
		if(bmBroadcastReceived)
		{
			bmBroadcastReceived = false;
		}
		else
		{
			switch(imBroadcastState)
			{
				case 0 :	// initialized
					imBroadcastState = 1;	// data sent !
					break;
				case 1 :	// data sent -> no broadcast message received
//					KillTimer(3);	// don't get recursive !
//					m_CB_BCStatus.SetColors(TRAFFIC_LIGHT_RED, ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
//					AfxMessageBox(GetString(IDS_BUTTONLIST_NOBC),MB_ICONSTOP);
//					ExitBDPSSEC();
					{
						KillTimer(3);	// don't get recursive !
						m_CB_BCStatus.SetColors(TRAFFIC_LIGHT_RED, ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
						if (bmBroadcastCheckMsg)
						{
							CString olMessage(GetString(IDS_NOBROADCASTS)); 
							int ilState = AfxMessageBox(olMessage,MB_YESNO|MB_ICONSTOP);
							SetTimer(3, imTimerValue,NULL);
							if (ilState == IDYES)	// we try it again
							{
								imBroadcastState = 1;
								//MWO unnecessary ogBasicData.CedaAction("SBC","SBCXXX","","","","");
							}
							else
							{
								imBroadcastState = 1;
								//MWO unnecessary ogBasicData.CedaAction("SBC","SBCXXX","","","","");
								bmBroadcastCheckMsg = false;
							}
						}
						else	// retry !
						{
							SetTimer(3, imTimerValue,NULL);
							imBroadcastState = 1;
							//MWO unnecessary ogBasicData.CedaAction("SBC","SBCXXX","","","","");
						}
					}
					break;
				case 2 :	// data received
					imBroadcastState = 1;	// data sent !
					m_CB_BCStatus.SetColors(TRAFFIC_LIGHT_GREEN, ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
					break;
				case 3 :	// broadcast not received -> ignore further events
					break;
			}
		}
	}

	CDialog::OnTimer(nIDEvent);
}

//
// OnTableLButtonDown()
//
// receives a WM_TABLE_SELCHANGE message from CCSTable
// when the function SelectLine() is called
// if the left mouse button was pressed, calls OnTableSelChange()
//
// ipParam contains flags indicating which button was pressed
// lpParam contains x,y co-ord where the mouse button was clicked
//
LONG CButtonListDialog::OnTableLButtonDown(UINT ipParam, LONG lpParam)
{
	// if the left mouse button was pressed, calls OnTableSelChange()
	if( ipParam >= 0 && MK_LBUTTON )	
		OnTableSelChange(pomTable->GetCurSel(),0);


	return 0L;
}


//
// OnTableSelChange()
//
// receives a WM_TABLE_SELCHANGE message from CCSTable
//
// ipParam contains flags indicating which button was pressed
// lpParam contains x,y co-ord where the mouse button was clicked
//
LONG CButtonListDialog::OnTableSelChange(UINT ipParam, LONG lpParam)
{
	// update the alloc dialog if it is open
	if( pogAllocDlg != NULL )
		OnAllocateButton();

	return 0L;
}



void CButtonListDialog::UpdateView()
{
	AfxGetApp()->DoWaitCursor(1);

	m_PasswordButton.EnableWindow(TRUE);
	m_AllocateButton.EnableWindow(TRUE);

	switch(rgCurrType)
	{
	case SEC_PROFILE:
		SetWindowText(GetString(IDS_BUTTONLISTDLG_PROFILECAPTION));
		m_PasswordButton.EnableWindow(FALSE);
		break;
	case SEC_APPL:
		SetWindowText(GetString(IDS_BUTTONLISTDLG_MODULECAPTION));
		m_PasswordButton.EnableWindow(FALSE);
		m_AllocateButton.EnableWindow(FALSE);
		break;
	case SEC_GROUP:
		SetWindowText(GetString(IDS_BUTTONLISTDLG_GROUPCAPTION));
		m_PasswordButton.EnableWindow(FALSE);
		break;
	case SEC_WKS:
		SetWindowText(GetString(IDS_BUTTONLISTDLG_WKSCAPTION));
		m_PasswordButton.EnableWindow(FALSE);
		break;
	case SEC_WKSGROUP:
		SetWindowText(GetString(IDS_BUTTONLISTDLG_WKSGROUPCAPTION));
		m_PasswordButton.EnableWindow(FALSE);
		break;
	case SEC_USER:
	default:
		SetWindowText(GetString(IDS_BUTTONLISTDLG_USERCAPTION));
	}

	omSecTableViewer.ChangeViewTo();
	
	AfxGetApp()->DoWaitCursor(-1);
}

void CButtonListDialog::OnCancel()
{
    // Ignore cancel -- This makes Alt-F4 and Escape keys no effect
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CButtonListDialog::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CButtonListDialog::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

int CButtonListDialog::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
    CRect rectScreen(0, 0, ::GetSystemMetrics(SM_CXSCREEN), 56);
    MoveWindow(&rectScreen, TRUE);

	if (CDialog::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	return 0;
}

void CButtonListDialog::OnDestroy() 
{
//	if (bgModal == TRUE)
//		return;

	CDialog::OnDestroy();
}

BOOL CButtonListDialog::OnEraseBkgnd(CDC* pDC)
{
    // determine size of the screen
    CRect rectScreen(0, 0, ::GetSystemMetrics(SM_CXSCREEN), ::GetSystemMetrics(SM_CYSCREEN));

    // determine rectangle of the updated region
    CRect rectUpdate;
    pDC->GetClipBox(&rectUpdate);

    // paint background of button-list menu area in the top of the screen
    CRect rectMenu = rectScreen;
    rectMenu.bottom = m_nDialogBarHeight;
    if (rectMenu.IntersectRect(&rectMenu, &rectUpdate))
    {
        CBrush brush;
        brush.CreateSolidBrush(::GetSysColor(COLOR_BTNFACE));// .CreateStockObject(LTGRAY_BRUSH);
        CBrush *pOldBrush = pDC->SelectObject(&brush);
        pDC->PatBlt(rectMenu.left, rectMenu.top, rectMenu.Width(), rectMenu.Height(), PATCOPY);
        pDC->SelectObject(pOldBrush);
    }

    // paint background of remaining area in the lower part of the screen
    CRect rectBody = rectScreen;
    rectBody.top = m_nDialogBarHeight;
    if (rectBody.IntersectRect(&rectBody, &rectUpdate))
    {
        CBrush brush;
        brush.CreateSolidBrush(::GetSysColor(COLOR_BTNFACE));// .CreateStockObject(LTGRAY_BRUSH);
        CBrush *pOldBrush = pDC->SelectObject(&brush);
        pDC->PatBlt(rectBody.left, rectBody.top, rectBody.Width(), rectBody.Height(), PATCOPY);
        pDC->SelectObject(pOldBrush);
    }

    return TRUE;
}



/////////////////////////////////////////////////////////////////////////////
// CButtonListDialog message handlers for the rightmost group buttons





/////////////////////////////////////////////////////////////////////////////
// CButtonListDialog helper routines

//LONG CButtonListDialog::OnBcAdd(UINT /*wParam*/, LONG /*lParam*/)
//{
//	ogBcHandle.GetBc();
//	return TRUE;
//}
LONG CButtonListDialog::OnBcAdd(UINT wParam, LONG lParam)
{
	TRACE("Broadcast num %d\n",wParam);
	bmBroadcastReceived = true;
	
	m_CB_BCStatus.SetColors(TRAFFIC_LIGHT_YELLOW, ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
	m_CB_BCStatus.UpdateWindow();

	bool blStatus = ogBcHandle.GetBc(wParam);

	imBroadcastState = 2;
	m_CB_BCStatus.SetColors(TRAFFIC_LIGHT_GREEN, ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
	m_CB_BCStatus.UpdateWindow();
			
	return TRUE;
}



void  ProcessCf(void *vpInstance,enum enumDDXTypes ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
//	 ogCfgData.ProcessCfgBc(ipDDXType,vpDataPointer,ropInstanceName);

//		CButtonListDialog *polButtonList = (CButtonListDialog *)vpInstance;
}


void CButtonListDialog::OnSize(UINT nType, int cx, int cy) 
{
	CDialog::OnSize(nType, cx, cy);
	
	pomTable->SetPosition(-1, cx+1, m_nDialogBarHeight-1, cy+1);
//	pomTable->SetPosition(-1, cx+1, m_nDialogBarHeight-1, cy-STATBARWIDTH);
//	m_StatusBar.SetWindowPos((CWnd*)pomTable,-1,(cy-STATBARWIDTH),cx+1,STATBARWIDTH,0);

	
}

void CButtonListDialog::OnClose() 
{
	OnExitBDPSSEC();
}



//
// OnTableLButtonDblClk()
//
// receives a WM_TABLE_LBUTTONDBLCLK message from CCSTable
//
LONG CButtonListDialog::OnTableLButtonDblClk(UINT ipParam, LONG lpParam)
{
	OnUpdateButton();

	return 0L;
}

//
// OnTableLButtonDown()
//
// receives a WM_TABLE_RBUTTONDOWN message from CCSTable
// when the function SelectLine() is called
// if the right mouse button was pressed, displays context sensetive menus
//
// ipParam contains flags indicating which button was pressed
// lpParam contains x,y co-ord where the mouse button was clicked
//
LONG CButtonListDialog::OnTableRButtonDown(UINT ipParam, LONG lpParam)
{
	CCSTABLENOTIFY *polNotify = (CCSTABLENOTIFY *) lpParam;
	int ilLine = polNotify->Line;

	if( polNotify->SourceTable == pomTable )
	{
		CMenu olMenu;
		olMenu.CreatePopupMenu();
		for (int i = olMenu.GetMenuItemCount(); --i >= 0;)
  			olMenu.RemoveMenu(i, MF_BYPOSITION);
  		olMenu.AppendMenu(MF_STRING|MF_ENABLED,ID_InsertButton, GetString(IDS_BUTTONLISTDLG_MENUINSERT));
  		olMenu.AppendMenu(MF_STRING|MF_ENABLED,ID_UpdateButton, GetString(IDS_BUTTONLISTDLG_MENUUPDATE));
  		olMenu.AppendMenu(MF_STRING|MF_ENABLED,ID_DeleteButton, GetString(IDS_BUTTONLISTDLG_MENUDELETE));
  		olMenu.AppendMenu(MF_SEPARATOR);
  		olMenu.AppendMenu(MF_STRING|MF_ENABLED,ID_RightsButton, GetString(IDS_BUTTONLISTDLG_MENURIGHTS));
		if( rgCurrType == SEC_APPL )
	  		olMenu.AppendMenu(MF_STRING|MF_GRAYED,ID_AllocateButton, GetString(IDS_BUTTONLISTDLG_MENUALLOCATE));
		else
	  		olMenu.AppendMenu(MF_STRING|MF_ENABLED,ID_AllocateButton, GetString(IDS_BUTTONLISTDLG_MENUALLOCATE));
  		olMenu.AppendMenu(MF_SEPARATOR);
		if( rgCurrType == SEC_USER )
			olMenu.AppendMenu(MF_STRING|MF_ENABLED,ID_PasswordButton, GetString(IDS_BUTTONLISTDLG_MENUPASSWORD));
		else
			olMenu.AppendMenu(MF_STRING|MF_GRAYED,ID_PasswordButton, GetString(IDS_BUTTONLISTDLG_MENUPASSWORD));

		pomTable->ClientToScreen(&polNotify->Point);
		olMenu.TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, polNotify->Point.x, polNotify->Point.y, this, NULL);
	}
	
	return 0L;
}

// insert a user or profile
void CButtonListDialog::OnInsertButton() 
{
	CEditUserDlg	olDlg;

	olDlg.omUrno = "";
	olDlg.omUsid = "";
	olDlg.omName = "";
	olDlg.omRema = "";
	olDlg.m_STAT = TRUE;
	olDlg.m_ModuleType = 0; // no inital profile
	olDlg.bmIsUpdate = false;
	olDlg.m_CanUseSec = false;


	// call the dialog
	if (olDlg.DoModal() != IDCANCEL)
	{

		AfxGetApp()->DoWaitCursor(1); // hourGlass

		SECDATA rlSecData;
		strcpy(rlSecData.USID,olDlg.m_USID);
		strcpy(rlSecData.NAME,olDlg.m_NAME);
		strcpy(rlSecData.STAT,olDlg.m_STAT ? "1" : "0");
		ogCedaSecData.SetIsSuperuser(&rlSecData, olDlg.m_CanUseSec);
		sprintf(rlSecData.TYPE,"%c\0",rgCurrType);
		strcpy(rlSecData.REMA,olDlg.m_REMA);

		CALDATA rlCalData;
		rlCalData.VAFR = olDlg.omVAFR;
		rlCalData.VATO = olDlg.omVATO;
		strcpy(rlCalData.FREQ," ");


		if( rgCurrType == SEC_APPL )
		{
			if(ogCedaSecData.NewApp( &rlSecData, olDlg.omCalTableViewer.omInsertedList, mCedaErrorMessage ) != true )
				MessageBox(GetString(IDS_DATABASE_ERROR),"",MB_ICONEXCLAMATION);
		}
		else
		{
			if(ogCedaSecData.NewSec( &rlSecData, olDlg.omCalTableViewer.omInsertedList, olDlg.imSelectedModu, mCedaErrorMessage ) != true )
				MessageBox(GetString(IDS_DATABASE_ERROR),"",MB_ICONEXCLAMATION);
			else
			{
				if( rgCurrType == SEC_USER )
				{
					// tell the user the new password
					char clBuf1[100];
					sprintf(clBuf1,"%s \"%s\"",GetString(IDS_BUTTONLISTDLG_NEWPASSWORD),GetString(IDS_DEFAULT_PASSWORD));
					char clBuf2[100];
					sprintf(clBuf2,"%s %s",GetString(IDS_BUTTONLISTDLG_NEWPASSWORDCAPTION),rlSecData.USID);
					MessageBox(clBuf1,clBuf2,MB_ICONINFORMATION);
				}

			}
		}

		AfxGetApp()->DoWaitCursor(-1); // hourGlass

	}

}


void CButtonListDialog::OnUpdateButton() 
{
	bool blRc = true;

	// get the line selected
	SECTAB_LINEDATA *prlSelLine = (SECTAB_LINEDATA *) pomTable->GetTextLineData(pomTable->GetCurrentLine());

	if( prlSelLine == NULL )
	{
		// no line selected
		MessageBox(GetString(IDS_NO_LINE_SELECTED),"",MB_ICONINFORMATION);
	}
	else
	{
		CEditUserDlg	olDlg;

		// get the data from the line selected
		SECDATA *prlSecData;

		prlSecData = ogCedaSecData.GetSecByUrno(prlSelLine->URNO);

		if( prlSecData == NULL )
		{
			AfxMessageBox(GetString(IDS_BUTTONLISTDLG_GETSECBYURNOERROR));
			blRc = false;
		}

		if( blRc )
		{
			olDlg.omUrno = prlSecData->URNO;
			olDlg.omUsid = prlSecData->USID;
			olDlg.omName = prlSecData->NAME;
			olDlg.omRema = prlSecData->REMA;
			olDlg.omOldUsid = prlSecData->USID;
			olDlg.m_STAT = (strcmp(prlSecData->STAT,"1")) ? FALSE : TRUE ;
			olDlg.bmIsUpdate = true;
			olDlg.m_CanUseSec = ogCedaSecData.IsSuperuser(prlSecData);


			// call the edit mask
			if (olDlg.DoModal() != IDCANCEL)
			{

				AfxGetApp()->DoWaitCursor(1); // hourGlass

				strcpy(prlSecData->USID,olDlg.m_USID);
				strcpy(prlSecData->STAT,olDlg.m_STAT ? "1" : "0");
				strcpy(prlSecData->NAME,olDlg.m_NAME);
				strcpy(prlSecData->REMA,olDlg.m_REMA);
				ogCedaSecData.SetIsSuperuser(prlSecData, olDlg.m_CanUseSec);
				
				// update any changed sec data
				if(ogCedaSecData.UpdSec( prlSecData, mCedaErrorMessage ) != true )
				{
					MessageBox(GetString(IDS_DATABASE_ERROR),"",MB_ICONEXCLAMATION);
					blRc = false;
				}

				// insert new CAL recs
				if( blRc && olDlg.omCalTableViewer.omInsertedList.GetSize() > 0 )
				{
					if(ogCedaCalData.NewCal( olDlg.omCalTableViewer.omInsertedList, mCedaErrorMessage ) != true )
					{
						MessageBox(GetString(IDS_DATABASE_ERROR),"",MB_ICONEXCLAMATION);
						blRc = false;
					}
				}

				// update existing CAL recs
				if( blRc && olDlg.omCalTableViewer.omUpdatedList.GetSize() > 0 )
				{
					if(ogCedaCalData.UpdCal( olDlg.omCalTableViewer.omUpdatedList, mCedaErrorMessage ) != true )
					{
						MessageBox(GetString(IDS_DATABASE_ERROR),"",MB_ICONEXCLAMATION);
						blRc = false;
					}
				}

				// remove deleted CAL recs
				if( blRc && olDlg.omCalTableViewer.omDeletedList.GetSize() > 0 )
				{
					if(ogCedaCalData.DelCal( olDlg.omCalTableViewer.omDeletedList, mCedaErrorMessage ) != true )
					{
						MessageBox(GetString(IDS_DATABASE_ERROR),"",MB_ICONEXCLAMATION);
						blRc = false;
					}
				}

				AfxGetApp()->DoWaitCursor(-1); // hourGlass
			}			
		
		}
		
	}	
}

void CButtonListDialog::OnDeleteButton() 
{

	// get the line selected
	SECTAB_LINEDATA *prlSelLine = (SECTAB_LINEDATA *) pomTable->GetTextLineData(pomTable->GetCurrentLine());

	if( prlSelLine == NULL )
	{
		// no line selected
		MessageBox(GetString(IDS_NO_LINE_SELECTED),"",MB_ICONINFORMATION);
	}
	else
	{
//		if(rgCurrType == SEC_USER && prlSelLine->USID == omAdminUsid)
//		{
//			// cannot delete UFIS$ADMIN (omAdminUsid read from Ceda.Ini)
//			MessageBox(GetString(IDS_CANNOT_DELETE ADMIN),GetString(IDS_DELETE_USER CAPTION),MB_ICONINFORMATION);
//		}
//		else
//		{
		char pclCaption[100];

		// confirm the delete
		switch(rgCurrType)
		{
			case SEC_PROFILE:
				strcpy(pclCaption,GetString(IDS_BUTTONLISTDLG_DELETEPROFILE));
				break;
			case SEC_GROUP:
				strcpy(pclCaption,GetString(IDS_BUTTONLISTDLG_DELETEGROUP));
				break;
			case SEC_WKS:
				strcpy(pclCaption,GetString(IDS_BUTTONLISTDLG_DELETEWKS));
				break;
			case SEC_WKSGROUP:
				strcpy(pclCaption,GetString(IDS_BUTTONLISTDLG_DELETEWKSGROUP));
				break;
			case SEC_APPL:
				strcpy(pclCaption,GetString(IDS_BUTTONLISTDLG_DELETEMODULE));
				break;
			case SEC_USER:
			default:
				strcpy(pclCaption,GetString(IDS_BUTTONLISTDLG_DELETEUSER));
				break;
		}
				

		if(MessageBox(GetString(IDS_BUTTONLISTDLG_DELETELINE),pclCaption,MB_YESNO|MB_ICONQUESTION) == IDYES )
		{
			// YES button pressed
			AfxGetApp()->DoWaitCursor(1); // hourGlass

			if(rgCurrType == SEC_APPL )
			{
				// delete the appplication from the database			
				if( ogCedaSecData.DelApp( prlSelLine->URNO, mCedaErrorMessage ) != true )
					MessageBox(GetString(IDS_DATABASE_ERROR),"",MB_ICONEXCLAMATION);
			}
			else
			{
				// delete the record from the database			
				if( ogCedaSecData.DelSec( prlSelLine->URNO, mCedaErrorMessage ) != true )
					MessageBox(GetString(IDS_DATABASE_ERROR),"",MB_ICONEXCLAMATION);
			}
			AfxGetApp()->DoWaitCursor(-1); // hourGlass
		}
	}

}


void CButtonListDialog::OnRightsButton() 
{
	int ilLine = pomTable->GetCurrentLine();
	SECTAB_LINEDATA *prlSelLine = (SECTAB_LINEDATA *) pomTable->GetTextLineData(ilLine);

	if( prlSelLine == NULL )
	{
		// no line selected
		MessageBox(GetString(IDS_NO_LINE_SELECTED),"",MB_ICONINFORMATION);
	}
	else
	{
		ObjDialog olDlg;
		strcpy(olDlg.pcmFsec,prlSelLine->URNO);
		if( rgCurrType == SEC_APPL )
			olDlg.imLine = ilLine; // MODUL -> select the module selected on the main screen
		else
			olDlg.imLine = 0; // select the first application

		if (olDlg.DoModal() != IDCANCEL && !olDlg.omStatList.IsEmpty())
		{
			AfxGetApp()->DoWaitCursor(1); // hourGlass
			if( rgCurrType == SEC_APPL )
			{
				if( ogCedaFktData.SetFkt( olDlg.omStatList, mCedaErrorMessage ) != true )
					MessageBox(mCedaErrorMessage, GetString(IDS_DATABASE_ERROR), MB_ICONEXCLAMATION);
			}
			else
			{
				if( ogCedaPrvData.SetPrv( olDlg.omStatList, mCedaErrorMessage ) != true )
					MessageBox(mCedaErrorMessage, GetString(IDS_DATABASE_ERROR), MB_ICONEXCLAMATION);
			}
			AfxGetApp()->DoWaitCursor(-1); // hourGlass
		}
	}
}



// allocates PROFILE:	one or more users to a profile
//			 USER:		a profile to a user
void CButtonListDialog::OnAllocateButton() 
{
	// get the line selected
	SECTAB_LINEDATA *prlSelLine = (SECTAB_LINEDATA *) pomTable->GetTextLineData(pomTable->GetCurSel()/*GetCurrentLine()*/);
	if(pogAllocDlg == NULL || prlSelLine != prmPrevSelLine)
	{
		prmPrevSelLine = prlSelLine;
		if( prlSelLine == NULL )
		{
			// close the alloc dialog if it is open
			if( pogAllocDlg != NULL )
				pogAllocDlg->SendCancel();
			else
				MessageBox(GetString(IDS_NO_LINE_SELECTED),"",MB_ICONINFORMATION); // no line selected
		}
		else
		{
			if( pogAllocDlg == NULL )
			{
				pogAllocDlg = new CAllocatedDlg(this);
				if( pogAllocDlg->Create() != TRUE )
					AfxMessageBox("Error creating alloc dialog");
			}
			else
				pogAllocDlg->SetActiveWindow();
			
			pogAllocDlg->OnInitDialog(prlSelLine->URNO);
		}
	}
}

void CButtonListDialog::OnExitBDPSSEC() 
{
	if(MessageBox(GetString(IDS_BUTTONLISTDLG_CONFIRMEXIT),"",MB_ICONQUESTION|MB_YESNO) == IDYES)
	{
		ExitBDPSSEC();
	}	
}

void CButtonListDialog::ExitBDPSSEC()
{
	AfxGetApp()->DoWaitCursor(1);

	CDialog::OnCancel();
	ogDdx.UnRegisterAll();

	EndDialog(IDCANCEL);
	PostQuitMessage(0);
}

void CButtonListDialog::OnPasswordButton() 
{
	// get the line selected
	SECTAB_LINEDATA *prlSelLine = (SECTAB_LINEDATA *) pomTable->GetTextLineData(pomTable->GetCurrentLine());

	if( prlSelLine == NULL )
	{
		// no line selected
		MessageBox(GetString(IDS_NO_LINE_SELECTED),"",MB_ICONINFORMATION);
	}
	else
	{
		// confirm the delete
		if(MessageBox(GetString(IDS_BUTTONLISTDLG_CONFIRMSETPASS),GetString(IDS_BUTTONLISTDLG_PASSWORDCAPTION),MB_YESNO|MB_ICONQUESTION) == IDYES )
		{
			// YES button pressed
			AfxGetApp()->DoWaitCursor(1); // hourGlass

			char clDefaultPassword[PASSLEN];
			strcpy(clDefaultPassword,GetString(IDS_DEFAULT_PASSWORD));
			if( ogCedaSecData.SetPwd( prlSelLine->URNO, clDefaultPassword, mCedaErrorMessage ) != true )
			{
				MessageBox(GetString(IDS_DATABASE_ERROR),"",MB_ICONEXCLAMATION);
			}
			else
			{
				// tell the user the new password
				char clBuf1[100];
				sprintf(clBuf1,"%s \"%s\"",GetString(IDS_BUTTONLISTDLG_NEWPASSWORD),clDefaultPassword);
				MessageBox(clBuf1,GetString(IDS_BUTTONLISTDLG_PASSWORDCAPTION),MB_ICONINFORMATION);
			}
		}
	}
	
}


////////////////////////////////////////////////////////////////////////////////////////
// Menu Items

void CButtonListDialog::OnUser() 
{
	rgCurrType = SEC_USER;
	UpdateView();
}

void CButtonListDialog::OnProfile() 
{
	rgCurrType = SEC_PROFILE;
	UpdateView();
}

void CButtonListDialog::OnApplication() 
{
	rgCurrType = SEC_APPL;

	// close the alloc dialog if it is open
	if( pogAllocDlg != NULL )
		pogAllocDlg->SendCancel();

	
	UpdateView();
}

void CButtonListDialog::OnGroup() 
{
	rgCurrType = SEC_GROUP;

	UpdateView();
}

void CButtonListDialog::OnWks() 
{
	rgCurrType = SEC_WKS;

	UpdateView();
}

void CButtonListDialog::OnWksGroup() 
{
	rgCurrType = SEC_WKSGROUP;

	UpdateView();
}



void CButtonListDialog::OnTestButton() 
{
	CString olText;
	char clBuf[100];

	sprintf(clBuf,"Count SECTAB = %d\n",ogCedaSecData.omData.GetSize());
	olText += CString(clBuf);
	sprintf(clBuf,"Count CALTAB = %d\n",ogCedaCalData.omData.GetSize());
	olText += CString(clBuf);
	sprintf(clBuf,"Count GRPTAB = %d\n",ogCedaGrpData.omData.GetSize());
	olText += CString(clBuf);
	sprintf(clBuf,"Count PRVTAB = %d\n",ogCedaPrvData.omData.GetSize());
	olText += CString(clBuf);
	sprintf(clBuf,"Count FKTTAB = %d",ogCedaFktData.omData.GetSize());
	olText += CString(clBuf);

	AfxMessageBox(olText);

}

void CButtonListDialog::OnExitButton() 
{
	OnExitBDPSSEC();
}

void CButtonListDialog::OnAppAbout()
{
	CAboutSecDlg olAboutDlg;
	olAboutDlg.DoModal();
}

void CButtonListDialog::OnPrintButton() 
{
	AfxGetApp()->DoWaitCursor(1);
	CString olTitle;
	GetWindowText(olTitle);
	omSecTableViewer.Print(olTitle,this);
	AfxGetApp()->DoWaitCursor(-1);
}

void CButtonListDialog::UpdateTrafficLight(int ipState)
{
	KillTimer(3);
	if(ipState == 0)
	{
		bmBroadcastReceived = true;

		m_CB_BCStatus.SetColors(TRAFFIC_LIGHT_YELLOW, ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
		m_CB_BCStatus.UpdateWindow();
	}
	else if(ipState == 1)
	{
		imBroadcastState = 2;
		m_CB_BCStatus.SetColors(TRAFFIC_LIGHT_GREEN, ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
		m_CB_BCStatus.UpdateWindow();
	}

	SetTimer(3, imTimerValue,NULL);
}
