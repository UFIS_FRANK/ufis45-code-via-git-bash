﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Timers;

using Ufis.Utils;
using Ufis.Data;
using _Standard_Bdpsuif_Addon.UI;
using _Standard_Bdpsuif_Addon.Resources.UI;
using System.Threading;
using System.Drawing;

namespace _Standard_Bdpsuif_Addon
{
    static class Program
    {
        [DllImport("user32.dll")]
        private static extern
            bool SetForegroundWindow(IntPtr hWnd);
        [DllImport("user32.dll")]
        private static extern
            bool ShowWindowAsync(IntPtr hWnd, int nCmdShow);
        [DllImport("user32.dll")]
        private static extern
            bool IsIconic(IntPtr hWnd);

        private const int SW_HIDE = 0;
        private const int SW_SHOWNORMAL = 1;
        private const int SW_SHOWMINIMIZED = 2;
        private const int SW_SHOWMAXIMIZED = 3;
        private const int SW_SHOWNOACTIVATE = 4;
        private const int SW_RESTORE = 9;
        private const int SW_SHOWDEFAULT = 10;


        internal static string UserName { get; set; }
        internal static string Password { get; set; }
        internal static string FormID { get; set; }
        internal static string URNO1 { get; set; }
        internal static string URNO2 { get; set; }
        internal static string Permit { get; set; }
        internal static string Time { get; set; }
        internal static bool HasUserIdNPwd
        {
            get
            {
                return (!string.IsNullOrEmpty(UserName) && !string.IsNullOrEmpty(Password));
            }
        }

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            Application.ThreadException += new ThreadExceptionEventHandler(Application_ThreadException);
            AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);

           //args = new string[] { "BARCOLOR,URNO1,URNO2|UFIS$ADMIN,1,L|100,100|Password" };
            //MessageBox.Show(args[0]);

            if (args.Length > 0)
            {
                ParseArgs(args);

                bool multipleInstancesAllowed = (FormID == "ACR_INS" || FormID == "ALT_INS");

                if ((!multipleInstancesAllowed) && IsApplicationAlreadyRunning())
                {

                    return;
                }

                if (HasUserIdNPwd)
                {
                    frmMain mainForm = new frmMain();
                    bool loginSucceeded = mainForm.IsValidLogin(UserName, Password);
                    mainForm.Close();

                    if (!loginSucceeded)
                    {
                        return;
                    }

                    //FormID = "ACTIVITIES";

                    //URNO1 = "1001795";

                    // URNO1= "1025579";


                    //FormID = "BWA_ALT";

                    //URNO1 = "134652236";

                    ////FormID = "BWA_ACR";
                    ////URNO1 = "21827420";

                    ////FormID = "BWA_ACT";
                    ////URNO1 = "132072371"; 

                    if (IsDBConnected())
                    {
                        if (!multipleInstancesAllowed)
                        {
                            StartTimer();
                        }


                        switch (FormID)
                        {
                            case "Multiple-Blocking":
                                Application.Run(new frmBlockParkingStand());
                                break;

                            case "UIF_CON":
                                Application.Run(new frmContinentList());
                                break;

                            case "ACR_INS":
                            case "ALT_INS":
                                if (!InsertBasicData(FormID, URNO1, URNO2))
                                {
                                    MessageBox.Show("Failed to save to validity table!");
                                }
                                break;
                            case "ACTIVITIES":
                                if (URNO1.Length > 1)
                                {
                                    Form frmTask = new frmTaskActivity(FormID, URNO1, Permit);
                                    frmTask.BringToFront();
                                    frmTask.TopMost = true;
                                    Application.Run(frmTask);
                                }
                                else if (URNO2.Length > 1)
                                {

                                    Form frmTask = new frmTaskActivity(FormID, URNO2, Permit);
                                    frmTask.BringToFront();
                                    frmTask.TopMost = true;
                                    Application.Run(frmTask);
                                }
                                else
                                {
                                    MessageBox.Show("Failed to Retrive Flight Information!");
                                }
                                break;

                            case "BARCOLOR":
                                Form frmBarColor = new frmBarColor(FormID, Permit);
                                frmBarColor.BringToFront();
                                frmBarColor.TopMost = true;
                                Application.Run(frmBarColor);
                                break;

                            default:
                                if (FormID.StartsWith("BDV_"))
                                {
                                    FormID = FormID.Substring(4);
                                    Form frmBD = new frmBDValidity(FormID, URNO1, Permit);
                                    frmBD.BringToFront();
                                    frmBD.TopMost = true;
                                    Application.Run(frmBD);
                                }
                                else if (FormID.StartsWith("BWA_"))
                                {
                                    Form frmBW = new frmBWValidity(FormID, URNO1, Permit);
                                    frmBW.BringToFront();
                                    frmBW.TopMost = true;
                                    Application.Run(frmBW);
                                }
                                break;
                        }
                    }
                    else
                    {
                        MessageBox.Show("Failed to connect to ufis database server!");
                    }
                }
                else
                {
                    MessageBox.Show("Invalid credential!");
                }
            }
            else
            {
                //The application is not supposed to run directly at this moment                

            }

        }

        private static bool IsApplicationAlreadyRunning()
        {
            bool isRunning = false;
            // get the name of our process

            string proc = Process.GetCurrentProcess().ProcessName;
            // get the list of all processes by that name

            Process[] processes = Process.GetProcessesByName(proc);
            // if there is more than one process...

            if (processes.Length > 1)
            {
                // Assume there is our process, which we will terminate, 
                // and the other process, which we want to bring to the 
                // foreground. This assumes there are only two processes
                // in the processes array, and we need to find out which
                // one is NOT us.

                // get our process
                Process p = Process.GetCurrentProcess();
                int n = 0;        // assume the other process is at index 0

                // if this process id is OUR process ID...

                if (processes[0].Id == p.Id)
                {
                    // then the other process is at index 1
                    n = 1;
                }
                isRunning = true;
                // get the window handle

                IntPtr hWnd = processes[n].MainWindowHandle;
                // if iconic, we need to restore the window
                if (IsIconic(hWnd))
                {
                    ShowWindowAsync(hWnd, SW_RESTORE);
                }
                // bring it to the foreground

                SetForegroundWindow(hWnd);

                // exit our process
            }
            // ... continue with your application initialization here.
            return isRunning;
        }


        private static void ParseArgs(string[] args)
        {
            //Being called from other program by passing args.
            //"frmBlockParkingStand,222,555|UFIS$ADMIN,"",L|100,100|Password";

            // "ACTIVITIES,URNO1,URNO2|UFIS$ADMIN,"",L|100,100|Password";


            args[0] = args[0].Replace((char)176, ' ');

            string[] stParams = args[0].Split('|');
            if (stParams.Length >= 4)
            {
                //Parsing id block
                string[] idTemps = stParams[0].Split(',');
                if (idTemps.Length >= 2)
                {
                    FormID = idTemps[0];
                    URNO1 = idTemps[1];

                    if (idTemps.Length > 2)
                    {
                        URNO2 = idTemps[2];
                    }
                }

                //Parsing username block
                string[] userTemp = stParams[1].Split(',');
                UserName = userTemp[0];
                if (userTemp.Length > 1)
                {
                    Permit = userTemp[1];
                    if (userTemp.Length >= 2)
                    {
                        Time = userTemp[2];
                    }
                }

                //Parsing Points


                //Parsing Password
                Password = stParams[3];

            }
            else if (args.Length == 2)
            {
                Password = args[1];
            }
        }


        private static bool IsDBConnected()
        {
            return Ctrl.CtrlData.GetInstance().MyDB.Connected;
        }


        static System.Timers.Timer aTimer = new System.Timers.Timer();
        public static void StartTimer()
        {
            aTimer.Elapsed += new ElapsedEventHandler(OnTimedEvent);
            aTimer.Interval = 2000;
            aTimer.Enabled = true;
        }
        private static void OnTimedEvent(object source, ElapsedEventArgs e)
        {
            Process current = Process.GetCurrentProcess();
            SetForegroundWindow(current.MainWindowHandle);
            aTimer.Stop();

        }

        private static bool InsertBasicData(string appType, string urno, string fieldAndValueList)
        {
            bool ret = false;

            Ctrl.CtrlBDIns ctrlBDIns = Ctrl.CtrlConfig.GetBDIns(appType);
            if (ctrlBDIns != null)
            {
                ret = ctrlBDIns.InsertToValidityTable(urno, fieldAndValueList);
            }

            return ret;
        }



        static void Application_ThreadException(object sender, ThreadExceptionEventArgs e)
        {
            MessageBox.Show(e.Exception.Message, "Unhandled Thread Exception");
            // here you can log the exception ...
        }

        static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            MessageBox.Show((e.ExceptionObject as Exception).Message, "Unhandled UI Exception");
            // here you can log the exception ...
        }

    }
}


