﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Ufis.Data;
using Ufis.Utils;

using _Standard_Bdpsuif_Addon.Ctrl;
using _Standard_Bdpsuif_Addon.Helper;
using _Standard_Bdpsuif_Addon.UserControl;

namespace _Standard_Bdpsuif_Addon.UI
{
        

    public partial class frmContinentList : Form
    {
        public const string CNTTAB_DB_FIELDS =
            //"URNO,CODE,DESP";
            "URNO,CODE,DESP,CDAT,USEC,LSTU,USEU,VAFR,VATO,HOPO,STAT";
        public const string CNTTAB_FIELD_LENGTHS =
            //"14,14,200";
            "14,14,200,14,32,14,32,14,14,3,1";
        public const string CNTTAB_LOGICAL_FIELDS =
            //"URNO,Code,Description";
            //"URNO,Code,Description,CDAT,USEC,LSTU,USEU,VAFR,VATO,HOPO,STAT";
            "URNO,CODE,DESP,CDAT,USEC,LSTU,USEU,VAFR,VATO,HOPO,STAT";

        public const string TAB_DB_FIELDS =
           "URNO,CODE,DESP";
        public const string TAB_FIELD_LENGTHS =
            "14,14,200";
        public const string TAB_LOGICAL_FIELDS =
            "URNO,Code,Description";

        //private IniFile myIni = null;
        private IDatabase myDB = null;
        private ITable ctnTab = null;
        public string UserName { get; set; }
        public string _urno=string.Empty;
        public int _line=0;
        private void LoadData()
        {
            
            ctnTab = myDB.Bind("CNT", "CNT", CNTTAB_LOGICAL_FIELDS, CNTTAB_FIELD_LENGTHS, CNTTAB_DB_FIELDS);
            ctnTab.Clear();
            ctnTab.Load("WHERE STAT<> 'D' ORDER BY URNO");
            ctnTab.CreateIndex("URNO", "URNO");

            PrepareTab();
            FillTabWithData(ctnTab);

            _line = 0;
            //tabCnt.SetLineColor(_line, -1, UT.colBlue);
        }
        private void FillTabWithData(ITable myTable)
        {
           
            tabCnt.ResetContent();
            string strLine = "";
            for (int j = 0; j < myTable.Count; j++)
            {
                strLine = myTable[j][0] + ","
                     + myTable[j][1] + ","
                     + myTable[j][2];
                tabCnt.InsertTextLine(strLine, false);
                strLine = "";
            }
        }
        private void PrepareTab()
        {

            tabCnt.HeaderString = TAB_LOGICAL_FIELDS;
            tabCnt.LogicalFieldList = TAB_LOGICAL_FIELDS;
            tabCnt.HeaderLengthString = "0,100,300";
            tabCnt.ColumnAlignmentString = "L,L,L";
            

            //int idx = 0;
            //idx = UT.GetItemNo(tabLOG.LogicalFieldList, "TIME");
            //tabLOG.DateTimeSetColumnFormat(idx, "YYYYMMDDhhmmss", "DD'.'MM'.'YYYY' / 'hh':'mm':'ss");

            //idx = UT.GetItemNo(tabLOG.LogicalFieldList, "CKBS");
            //tabLOG.DateTimeSetColumnFormat(idx, "YYYYMMDDhhmmss", "DD'.'MM'.'YYYY' / 'hh':'mm");

            //idx = UT.GetItemNo(tabLOG.LogicalFieldList, "CKES");
            //tabLOG.DateTimeSetColumnFormat(idx, "YYYYMMDDhhmmss", "DD'.'MM'.'YYYY' / 'hh':'mm");

            //idx = UT.GetItemNo(tabLOG.LogicalFieldList, "VALU");
            //tabLOG.DateTimeSetColumnFormat(idx, "YYYYMMDDhhmmss", "DD'.'MM'.'YYYY' / 'hh':'mm");

            //idx = UT.GetItemNo(tabLOG.LogicalFieldList, "OVAL");
            // tabLOG.DateTimeSetColumnFormat(idx, "YYYYMMDDhhmmss", "DD'.'MM'.'YYYY' / 'hh':'mm");



            //string strColor = UT.colBlue + "," + UT.colBlue;
            //tabCnt.CursorDecoration(tabCnt.LogicalFieldList, "B,T", "2,2", strColor);
            //tabCnt.SetInternalLineBuffer(true);
            //tabCnt.Refresh();


        }
        public frmContinentList()
        {
            InitializeComponent();

            myDB = CtrlData.GetInstance().MyDB;

            tabCnt.LifeStyle = true;
            tabCnt.LineHeight = 20;
            tabCnt.FontName = "Courier New"; //"Arial"
            tabCnt.FontSize = 16;
            tabCnt.HeaderFontSize = 16;
            tabCnt.DefaultCursor = false;
            tabCnt.InplaceEditUpperCase = false;
            tabCnt.EnableHeaderSizing(true);
            tabCnt.ShowHorzScroller(true);
            tabCnt.AutoSizeByHeader = true;
          
            
            // show tab
            tabCnt.SetInternalLineBuffer(false);
            tabCnt.Refresh();

            LoadData();

            //this.ShowDialog();
        }

        private void frmContinentList_Load(object sender, EventArgs e)
        {

        }

        private void tabCnt_SendLButtonDblClick(object sender, AxTABLib._DTABEvents_SendLButtonDblClickEvent e)
        {
            /*
            int iLineNo = e.lineNo;
            string Urno = tabCnt.GetFieldValue(iLineNo,"URNO");

            if (!string.IsNullOrEmpty(Urno))
            {

                frmContinent dlgContinent = new frmContinent(Urno, _Standard_Bdpsuif_Addon.UI.frmContinent.Modes.CONTINENT_UPDATE);

                if (dlgContinent.ShowDialog() == DialogResult.OK)
                {
                    //reload the continents
                    LoadData();
                }
            }
             * */
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            frmContinent dlgContinent = new frmContinent("", _Standard_Bdpsuif_Addon.UI.frmContinent.Modes.CONTINENT_NEW);
            if (dlgContinent.ShowDialog() == DialogResult.OK)
            {
                LoadData();
            }

        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            //to return the selected urno to the BDPSUIF
            if (!string.IsNullOrEmpty(_urno))
            {
                SendMsgToBDPSApp(_urno);
            }

            //this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void SendMsgToBDPSApp(string data)
        {
            CtrlUtil ctl = new CtrlUtil();
            ctl.SendMsgToBDPSApp(data);
        }

        private void tabCnt_SendLButtonClick(object sender, AxTABLib._DTABEvents_SendLButtonClickEvent e)
        {
            int iLineNo = e.lineNo;
            tabCnt.SetLineColor(_line, -1, UT.colWhite);
            _line = iLineNo;
            tabCnt.SetLineColor(iLineNo, -1, UT.colBlue);
            string Urno = tabCnt.GetFieldValue(iLineNo, "URNO");
            _urno = Urno;
            
        }

        private void btnUpd_Click(object sender, EventArgs e)
        {
            //int iLineNo = e.lineNo;
            //string Urno = tabCnt.GetFieldValue(iLineNo, "URNO");

            if (!string.IsNullOrEmpty(_urno))
            {
                frmContinent dlgContinent = new frmContinent(_urno, _Standard_Bdpsuif_Addon.UI.frmContinent.Modes.CONTINENT_UPDATE);

                if (dlgContinent.ShowDialog() == DialogResult.OK)
                {
                    //reload the continents
                    LoadData();

                    CtrlUtil ctl = new CtrlUtil();
                    ctl.SendDelMsgToBDPSApp(_urno);
                }
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(_urno))
            {
                DialogResult result = MessageBox.Show(
                        "Are you sure to delete this record?", this.Text,
                        MessageBoxButtons.YesNo, MessageBoxIcon.Question,
                        MessageBoxDefaultButton.Button2);
                if (result == DialogResult.Yes)
                {
                    if (DeleteAContinent(_urno))
                    {
                        LoadData();
                        CtrlUtil ctl = new CtrlUtil();
                        ctl.SendDelMsgToBDPSApp(_urno);
                        _urno = string.Empty;
                    }
                }
            }
        }

        private bool  DeleteAContinent(string urno)
        {
            bool result = false;
            IRow row = null;
            for (int i = 0; i < ctnTab.Count; i++)
            {
                row = ctnTab[i];
                string rowUro = row["URNO"];
                if (rowUro == urno)
                {
                    break;
                }
            }
            if (row != null)
            {
                row["URNO"] = urno;
                row["LSTU"] = UT.DateTimeToCeda(UT.LocalToUtc(DateTime.Now));
                row["USEU"] = Program.UserName;
                row["HOPO"] = UT.Hopo;
                row["STAT"] = "D";                
                row.Status = State.Modified;
                ctnTab.Save(row);
                result = true;
            }
            return result;
        }
    }
}
