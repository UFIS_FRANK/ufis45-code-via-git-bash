﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UFISAPPMNGLib;

namespace _Standard_Bdpsuif_Addon.Ctrl
{
    public class CtrlUtil
    {
        public void SendMsgToBDPSApp(string data)
        {
            UFISAmClass objAmCls = new UFISAmClass();
            objAmCls.TransferData(6, 6, data);
        }

        public void SendDelMsgToBDPSApp(string data)
        {
            UFISAmClass objAmCls = new UFISAmClass();
            objAmCls.TransferData(6, 7, data);
        }

        public void SendReloadMsgToFipsApp()
        {
            UFISAmClass objAmCls = new UFISAmClass();
            objAmCls.TransferData(2, 2, "RELOAD_NATURE");
        }
    }
}
