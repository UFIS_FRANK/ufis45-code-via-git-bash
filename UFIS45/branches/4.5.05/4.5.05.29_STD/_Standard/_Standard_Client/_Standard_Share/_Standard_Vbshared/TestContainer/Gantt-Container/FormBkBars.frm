VERSION 5.00
Begin VB.Form FormBkBars 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "FormBkBars"
   ClientHeight    =   2670
   ClientLeft      =   45
   ClientTop       =   285
   ClientWidth     =   6630
   LinkTopic       =   "Form2"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2670
   ScaleWidth      =   6630
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton SetBkBarColorByKey 
      Caption         =   "SetBkBarColorByKey"
      Height          =   285
      Left            =   105
      TabIndex        =   15
      Top             =   1260
      Width           =   1695
   End
   Begin VB.TextBox txtSetBkBarColorByKey 
      Height          =   285
      Left            =   1905
      TabIndex        =   14
      Text            =   "KeyTest"
      ToolTipText     =   "BkBarKey"
      Top             =   1260
      Width           =   1215
   End
   Begin VB.TextBox txtBkBarColor 
      Height          =   285
      Left            =   3225
      TabIndex        =   13
      Text            =   "255"
      ToolTipText     =   "BkBarColor"
      Top             =   1260
      Width           =   855
   End
   Begin VB.CommandButton SetBkBarTimeByKey 
      Caption         =   "SetBkBarTimeByKey"
      Height          =   285
      Left            =   105
      TabIndex        =   12
      Top             =   1575
      Width           =   1695
   End
   Begin VB.TextBox txtSetBkBarTimeByKey_Key 
      Height          =   285
      Left            =   1890
      TabIndex        =   11
      Text            =   "KeyTest"
      Top             =   1575
      Width           =   1215
   End
   Begin VB.TextBox txtSetBkBarTimeByKeyBegin 
      Height          =   285
      Left            =   3225
      TabIndex        =   10
      Text            =   "29.09.00 20:00:00"
      Top             =   1575
      Width           =   1440
   End
   Begin VB.TextBox txtSetBkBarTimeByKeyEnd 
      Height          =   285
      Left            =   4725
      TabIndex        =   9
      Text            =   "30.09.00 3:00:00"
      Top             =   1575
      Width           =   1440
   End
   Begin VB.CommandButton GetBkBarsByLine 
      Caption         =   "GetBkBarsByLine"
      Height          =   285
      Left            =   105
      TabIndex        =   8
      Top             =   2275
      Width           =   1665
   End
   Begin VB.TextBox txtGetBkBarsByLine 
      Height          =   285
      Left            =   1890
      TabIndex        =   7
      Text            =   "0"
      ToolTipText     =   "Insert LineNo"
      Top             =   2275
      Width           =   615
   End
   Begin VB.CommandButton btnDeleteBkBar 
      Caption         =   "DeleteBkBarByKey"
      Height          =   285
      Left            =   105
      TabIndex        =   6
      Top             =   525
      Width           =   1530
   End
   Begin VB.TextBox txtDeleteBkBar 
      Height          =   285
      Left            =   1785
      TabIndex        =   5
      Text            =   "KeyTest"
      Top             =   525
      Width           =   975
   End
   Begin VB.CommandButton btnInsertBkBar 
      Caption         =   "AddBkBarToLine"
      Height          =   285
      Left            =   105
      TabIndex        =   4
      ToolTipText     =   "Insert background barwith 1: start, 2: end, 3: LineNo"
      Top             =   210
      Width           =   1530
   End
   Begin VB.TextBox btnBkBarLineNo 
      Height          =   285
      Left            =   1785
      TabIndex        =   3
      Text            =   "4"
      ToolTipText     =   "Lineno where Backgroundbar shall be inserted"
      Top             =   210
      Width           =   720
   End
   Begin VB.TextBox txtBkBarKey 
      Height          =   285
      Left            =   2520
      TabIndex        =   2
      Text            =   "KeyTest"
      ToolTipText     =   "Key for the BkBar"
      Top             =   210
      Width           =   1020
   End
   Begin VB.TextBox txtBkBarStartTime 
      Height          =   285
      Left            =   3570
      TabIndex        =   1
      Text            =   "29.09.00 20:00:00"
      ToolTipText     =   "Start of backgroundbar"
      Top             =   210
      Width           =   1440
   End
   Begin VB.TextBox txtBkBarEndTime 
      Height          =   285
      Left            =   5040
      TabIndex        =   0
      Text            =   "30.09.00 4:00:00"
      ToolTipText     =   "Endtime of BkBar, e.g. 01.01.2000 10:15"
      Top             =   210
      Width           =   1485
   End
   Begin VB.Frame Frame1 
      Height          =   960
      Left            =   0
      TabIndex        =   16
      Top             =   0
      Width           =   6630
   End
   Begin VB.Frame Frame2 
      Height          =   960
      Left            =   0
      TabIndex        =   17
      Top             =   1050
      Width           =   6630
   End
   Begin VB.Frame Frame3 
      Height          =   540
      Left            =   0
      TabIndex        =   18
      Top             =   2100
      Width           =   6630
   End
End
Attribute VB_Name = "FormBkBars"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub btnDeleteBkBar_Click()
    Dim HelpStr As String
    If (IsNumeric(txtDeleteBkBar.Text) = True) Then
        HelpStr = CStr(txtDeleteBkBar.Text)
    Else
        HelpStr = txtDeleteBkBar.Text
    End If
    Form1.Gantt.DeleteBkBarByKey (HelpStr)
    Form1.Gantt.RefreshArea "GANTT"
End Sub

Private Sub btnInsertBkBar_Click()
    Dim Start As Date, Ende As Date
    Dim Color As Long

    Form1.CommonDialog1.ShowColor
    
    Color = Form1.CommonDialog1.Color
    Start = txtBkBarStartTime.Text
    Ende = txtBkBarEndTime.Text
    
    Form1.Gantt.AddBkBarToLine btnBkBarLineNo.Text, _
                                txtBkBarKey.Text, _
                                Start, _
                                Ende, _
                                Color, _
                                ""
    Form1.Gantt.RefreshArea "GANTT"
End Sub

Private Sub GetBkBarsByLine_Click()
    Dim LineNr As String
    LineNr = txtGetBkBarsByLine.Text

    If (IsNumeric(LineNr) = True) Then
        If LineNr > -1 Then
            MsgBox Form1.Gantt.GetBkBarsByLine(LineNr), vbOKOnly, "GetBkBarsByLine"
        End If
    End If
End Sub

Private Sub SetBkBarColorByKey_Click()
    Form1.Gantt.SetBkBarColor txtBkBarKey, txtBkBarColor
    Form1.Gantt.RefreshArea "GANTT"
End Sub

Private Sub SetBkBarTimeByKey_Click()
    Form1.Gantt.SetBkBarDate txtSetBkBarTimeByKey_Key, txtSetBkBarTimeByKeyBegin, txtSetBkBarTimeByKeyEnd
    Form1.Gantt.RefreshArea "GANTT"
End Sub

