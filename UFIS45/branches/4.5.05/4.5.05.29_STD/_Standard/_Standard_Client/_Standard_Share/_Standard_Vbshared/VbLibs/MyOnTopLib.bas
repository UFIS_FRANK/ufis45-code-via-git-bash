Attribute VB_Name = "MyOnTopLib"
Option Explicit

Public Sub ShowChildForm(Caller As Form, CallButton As Control, ChildForm As Form, TaskValue As String)
    ChildForm.RegisterMyParent Caller, CallButton, TaskValue
    CheckFormOnTop Caller, False, False
    ChildForm.Show , Caller
    CheckFormOnTop Caller, True, True
    Caller.SetFocus
End Sub

