// CedaDEVData.cpp
 
#include <stdafx.h>
#include <CedaDEVData.h>
#include <resource.h>
#include <CedaBasicData.h>


// Local function prototype
static void ProcessDEVCf(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//--CEDADATA-----------------------------------------------------------------------------------------------

CedaDEVData::CedaDEVData() : CCSCedaData(&ogCommHandler)
{
    // Create an array of CEDARECINFO for DEVDATA
	BEGIN_CEDARECINFO(DEVDATA,DEVDataRecInfo)
		FIELD_LONG		(Urno,"URNO")
		FIELD_DATE		(Cdat,"CDAT")
		FIELD_CHAR_TRIM	(Usec,"USEC")
		FIELD_DATE		(Lstu,"LSTU")
		FIELD_CHAR_TRIM	(Useu,"USEU")
		FIELD_CHAR_TRIM	(Devn,"DEVN")
		FIELD_CHAR_TRIM	(Grpn,"GRPN")
		FIELD_CHAR_TRIM	(Dadr,"DADR")
		FIELD_CHAR_TRIM	(Dprt,"DPRT")
		FIELD_CHAR_TRIM	(Stat,"STAT")
		FIELD_CHAR_TRIM	(Dpid,"DPID")
		FIELD_CHAR_TRIM	(Adid,"ADID")
		FIELD_CHAR_TRIM	(Drnp,"DRNP")
		FIELD_CHAR_TRIM	(Dffd,"DFFD")
		FIELD_CHAR_TRIM	(Dfco,"DFCO")
		FIELD_CHAR_TRIM	(Dter,"DTER")
		FIELD_CHAR_TRIM	(Dare,"DARE")
		FIELD_CHAR_TRIM	(Algc,"ALGC")
		FIELD_CHAR_TRIM	(Dalz,"DALZ")
		FIELD_CHAR_TRIM	(Ddgi,"DDGI")
		FIELD_CHAR_TRIM	(Loca,"LOCA")
		FIELD_CHAR_TRIM	(Rema,"REMA")
		FIELD_CHAR_TRIM	(Divn,"DIVN")
		FIELD_CHAR_TRIM	(Drna,"DRNA")
		FIELD_DATE		(Dlct,"DLCT")
		FIELD_DATE		(Dldc,"DLDC")
		FIELD_CHAR_TRIM	(Dlrd,"DLRD")
		FIELD_DATE		(Dldt,"DLDT")
		FIELD_DATE	    (Vafr,"VAFR")
		FIELD_DATE	    (Vato,"VATO")
		FIELD_CHAR_TRIM	(Drgi,"DRGI")
		FIELD_CHAR_TRIM	(Dbrf,"DBRF")
		FIELD_CHAR_TRIM	(Dcof,"DCOF")
		FIELD_CHAR_TRIM	(Prfl,"PRFL")
	END_CEDARECINFO //(DEVDATA)


	// Copy the record structure
	for (int i=0; i< sizeof(DEVDataRecInfo)/sizeof(DEVDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&DEVDataRecInfo[i],sizeof(CEDARECINFO));
		omRecInfo.Add(prpCedaRecInfo);
	} // end for

	// initialize field names
	strcpy(pcmTableName,"DEV");
	strcpy(pcmDEVFieldList,"URNO,CDAT,USEC,LSTU,USEU,DEVN,GRPN,DADR,DPRT,STAT,DPID,ADID,DRNP,DFFD,DFCO,DTER,DARE,ALGC,DALZ,DDGI,LOCA,REMA,DIVN,DRNA,DLCT,DLDC,DLRD,DLDT,VAFR,VATO,DRGI,DBRF,DCOF,PRFL");
	pcmFieldList = pcmDEVFieldList;
	omData.SetSize(0,1000);
	//omUrnoMap.InitHashTable(2001);
}

//---------------------------------------------------------------------------------------------------

void CedaDEVData::GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType)
{
	ropFields.Add("DEVN"); ropDesription.Add(LoadStg(IDS_STRING935)); ropType.Add("String");
	ropFields.Add("GRPN"); ropDesription.Add(LoadStg(IDS_STRING936)); ropType.Add("String");
	ropFields.Add("DADR"); ropDesription.Add(LoadStg(IDS_STRING937)); ropType.Add("String");
	ropFields.Add("DPRT"); ropDesription.Add(LoadStg(IDS_STRING938)); ropType.Add("String");
	ropFields.Add("STAT"); ropDesription.Add(LoadStg(IDS_STRING939)); ropType.Add("String");
	ropFields.Add("DPID"); ropDesription.Add(LoadStg(IDS_STRING940)); ropType.Add("String");
	ropFields.Add("ADID"); ropDesription.Add(LoadStg(IDS_STRING941)); ropType.Add("String");
	ropFields.Add("DFFD"); ropDesription.Add(LoadStg(IDS_STRING943)); ropType.Add("String");
	ropFields.Add("DFCO"); ropDesription.Add(LoadStg(IDS_STRING944)); ropType.Add("String");
	ropFields.Add("DTER"); ropDesription.Add(LoadStg(IDS_STRING945)); ropType.Add("String");
	ropFields.Add("DARE"); ropDesription.Add(LoadStg(IDS_STRING946)); ropType.Add("String");
	ropFields.Add("ALGC"); ropDesription.Add(LoadStg(IDS_STRING947)); ropType.Add("String");
	ropFields.Add("DALZ"); ropDesription.Add(LoadStg(IDS_STRING948)); ropType.Add("String");
	ropFields.Add("DRGI"); ropDesription.Add(LoadStg(IDS_STRING1015)); ropType.Add("String");
	ropFields.Add("DBRF"); ropDesription.Add(LoadStg(IDS_STRING1016)); ropType.Add("String");
	ropFields.Add("DCOF"); ropDesription.Add(LoadStg(IDS_STRING1017)); ropType.Add("String");

	ropFields.Add("URNO"); ropDesription.Add(LoadStg(IDS_STRING346)); ropType.Add("String");
	ropFields.Add("CDAT"); ropDesription.Add(LoadStg(IDS_STRING343)); ropType.Add("Date");
	ropFields.Add("USEC"); ropDesription.Add(LoadStg(IDS_STRING347)); ropType.Add("String");
	ropFields.Add("LSTU"); ropDesription.Add(LoadStg(IDS_STRING344)); ropType.Add("Date");
	ropFields.Add("USEU"); ropDesription.Add(LoadStg(IDS_STRING348)); ropType.Add("String");

	ropFields.Add("DRNP"); ropDesription.Add(LoadStg(IDS_STRING1059)); ropType.Add("String");
	ropFields.Add("DDGI"); ropDesription.Add(LoadStg(IDS_STRING1060)); ropType.Add("String");
	ropFields.Add("LOCA"); ropDesription.Add(LoadStg(IDS_STRING1061)); ropType.Add("String");
	ropFields.Add("REMA"); ropDesription.Add(LoadStg(IDS_STRING1062)); ropType.Add("String");
	ropFields.Add("DIVN"); ropDesription.Add(LoadStg(IDS_STRING1063)); ropType.Add("String");
	ropFields.Add("DRNA"); ropDesription.Add(LoadStg(IDS_STRING1064)); ropType.Add("String");
	ropFields.Add("DLCT"); ropDesription.Add(LoadStg(IDS_STRING1065)); ropType.Add("Date");
	ropFields.Add("DLDC"); ropDesription.Add(LoadStg(IDS_STRING1066)); ropType.Add("Date");
	ropFields.Add("DLRD"); ropDesription.Add(LoadStg(IDS_STRING1067)); ropType.Add("String");
	ropFields.Add("DLDT"); ropDesription.Add(LoadStg(IDS_STRING1068)); ropType.Add("String");
	ropFields.Add("VAFR"); ropDesription.Add(LoadStg(IDS_STRING230)); ropType.Add("Date");
	ropFields.Add("VATO"); ropDesription.Add(LoadStg(IDS_STRING231)); ropType.Add("Date");
	ropFields.Add("PRFL"); ropDesription.Add(LoadStg(IDS_STRING345)); ropType.Add("String");

}

//-----------------------------------------------------------------------------------------------

void CedaDEVData::Register(void)
{
	ogDdx.Register((void *)this,BC_DEV_CHANGE,CString("DEVDATA"), CString("DEV-changed"),ProcessDEVCf);
	ogDdx.Register((void *)this,BC_DEV_DELETE,CString("DEVDATA"), CString("DEV-deleted"),ProcessDEVCf);
}

//--~CEDADATA----------------------------------------------------------------------------------------------

CedaDEVData::~CedaDEVData(void)
{
	omRecInfo.DeleteAll();
	ClearAll();
}

//--CLEAR-ALL----------------------------------------------------------------------------------------------

void CedaDEVData::ClearAll(void)
{
    omUrnoMap.RemoveAll();
	omDadrMap.RemoveAll();
    omData.DeleteAll();
	ogDdx.UnRegister(this,NOTUSED);
}

//--READ-ALL-----------------------------------------------------------------------------------------------

bool CedaDEVData::Read(char *pcpWhere)
{
    // Select data from the database
	bool ilRc = true;
    omUrnoMap.RemoveAll();
	omDadrMap.RemoveAll();
    omData.DeleteAll();
	if(pcpWhere != NULL)
		ilRc = CedaAction(ogBasicData.GetCedaCommand("RT"), pcpWhere);
	else
		ilRc = CedaAction(ogBasicData.GetCedaCommand("RT"));
	if (ilRc != true)
	{
		return false;
	}
	// Load data from CedaData into the dynamic array of record
	for (int ilLc = 0; ilRc == true; ilLc++)
	{
		DEVDATA *prpDEV = new DEVDATA;
		if ((ilRc = GetFirstBufferRecord(prpDEV)) == true)
		{
			prpDEV->IsChanged = DATA_UNCHANGED;
			omData.Add(prpDEV);//Update omData
			omUrnoMap.SetAt((void *)prpDEV->Urno,prpDEV);
			omDadrMap.SetAt(prpDEV->Dadr,prpDEV);
		}
		else
		{
			delete prpDEV;
		}
	}
    return true;
}

//--INSERT-------------------------------------------------------------------------------------------------

bool CedaDEVData::InsertDEV(DEVDATA *prpDEV,BOOL bpSendDdx)
{
	prpDEV->IsChanged = DATA_NEW;
	if (SaveDEV(prpDEV) == false) 
		return false; //Update Database
	InsertDEVInternal(prpDEV);
    return true;
}

//--INSERT-INTERNAL-------------------------------------------------------------------------------------------

bool CedaDEVData::InsertDEVInternal(DEVDATA *prpDEV)
{
	//PrepareDEVData(prpDEV);
	ogDdx.DataChanged((void *)this, DEV_CHANGE,(void *)prpDEV ); //Update Viewer
	omData.Add(prpDEV);//Update omData
	omUrnoMap.SetAt((void *)prpDEV->Urno,prpDEV);
	omDadrMap.SetAt(prpDEV->Dadr,prpDEV);
    return true;
}

//--DELETE-------------------------------------------------------------------------------------------------

bool CedaDEVData::DeleteDEV(long lpUrno)
{
	DEVDATA *prlDEV = GetDEVByUrno(lpUrno);
	if (prlDEV != NULL)
	{
		prlDEV->IsChanged = DATA_DELETED;
		if(SaveDEV(prlDEV) == false) return false; //Update Database
		DeleteDEVInternal(prlDEV);
	}
    return true;
}

//--DELETE-INTERNAL----------------------------------------------------------------------------------------

bool CedaDEVData::DeleteDEVInternal(DEVDATA *prpDEV)
{
	ogDdx.DataChanged((void *)this,DEV_DELETE,(void *)prpDEV); //Update Viewer
	omUrnoMap.RemoveKey((void *)prpDEV->Urno);
	omDadrMap.RemoveKey(prpDEV->Dadr);
	int ilDEVCount = omData.GetSize();
	for (int ilLc = 0; ilLc < ilDEVCount; ilLc++)
	{
		if (omData[ilLc].Urno == prpDEV->Urno)
		{
			omData.DeleteAt(ilLc);//Update omData
			break;
		}
	}
    return true;
}

//--PREPARE-DATA-------------------------------------------------------------------------------------------

void CedaDEVData::PrepareDEVData(DEVDATA *prpDEV)
{
	// TODO: add code here
}

//--UPDATE-------------------------------------------------------------------------------------------------

bool CedaDEVData::UpdateDEV(DEVDATA *prpDEV,BOOL bpSendDdx)
{
	if (GetDEVByUrno(prpDEV->Urno) != NULL)
	{
		if (prpDEV->IsChanged == DATA_UNCHANGED)
		{
			prpDEV->IsChanged = DATA_CHANGED;
		}
		if(SaveDEV(prpDEV) == false) return false; //Update Database
		UpdateDEVInternal(prpDEV);
	}
    return true;
}

//--UPDATE-INTERNAL--------------------------------------------------------------------------------------------

bool CedaDEVData::UpdateDEVInternal(DEVDATA *prpDEV)
{
	DEVDATA *prlDEV = GetDEVByUrno(prpDEV->Urno);
	if (prlDEV != NULL)
	{
		omDadrMap.RemoveKey(prlDEV->Dadr);
		*prlDEV = *prpDEV; //Update omData
		omDadrMap.SetAt(prlDEV->Dadr,prlDEV);
		ogDdx.DataChanged((void *)this,DEV_CHANGE,(void *)prlDEV); //Update Viewer
	}
    return true;
}

//--GET-BY-URNO--------------------------------------------------------------------------------------------

DEVDATA *CedaDEVData::GetDEVByUrno(long lpUrno)
{
	DEVDATA  *prlDEV;
	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlDEV) == TRUE)
	{
		return prlDEV;
	}
	return NULL;
	
}
//--GET-BY-DADR--------------------------------------------------------------------------------------------

DEVDATA *CedaDEVData::GetDEVByDadr(const CString& ropDadr)
{
	DEVDATA  *prlDEV;
	if (omDadrMap.Lookup(ropDadr,(void *& )prlDEV) == TRUE)
	{
		return prlDEV;
	}
	return NULL;
	
}

//--READSPECIALDATA-------------------------------------------------------------------------------------

bool CedaDEVData::ReadSpecialData(CCSPtrArray<DEVDATA> *popAlt,char *pspWhere,char *pspFieldList,bool ipSYS/*=true*/)
{
	bool ilRc = true;
	char pclFieldList[256] = " ";

	if(strlen(pspFieldList) > 0) 
	{
		strcpy(pclFieldList, pspFieldList);
	}
	if(ipSYS == true) 
	{
		if (CedaAction("SYS","DEV",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	else
	{
		if (CedaAction("RT","DEV",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	if(popAlt != NULL)
	{
		for (int ilLc = 0; ilRc == true; ilLc++)
		{
			DEVDATA *prpAlt = new DEVDATA;
			if ((ilRc = GetBufferRecord(ilLc,prpAlt,CString(pclFieldList))) == true)
			{
				popAlt->Add(prpAlt);
			}
			else
			{
				delete prpAlt;
			}
		}
		if(popAlt->GetSize() == 0) return false;
	}
    return true;
}

//--SAVE---------------------------------------------------------------------------------------------------

bool CedaDEVData::SaveDEV(DEVDATA *prpDEV)
{
	bool ilRc = true;
	CString olListOfData;
	char pclSelection[124];
	char pclData[2048];

	if (prpDEV->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}

	switch(prpDEV->IsChanged)
	{
	case DATA_NEW:
		MakeCedaData(&omRecInfo,olListOfData,prpDEV);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("IRT","","",pclData);
		prpDEV->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpDEV->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpDEV);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("URT",pclSelection,"",pclData);
		prpDEV->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_DELETED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpDEV->Urno);
		ilRc = CedaAction("DRT",pclSelection);
		break;
	}

    return ilRc;
}

//--PROCESS-CF---------------------------------------------------------------------------------------------

void  ProcessDEVCf(void * popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	switch(ipDDXType)
	{
	case BC_DEV_CHANGE :
	case BC_DEV_DELETE :
		((CedaDEVData *)popInstance)->ProcessDEVBc(ipDDXType,vpDataPointer,ropInstanceName);
		break;
	}
}

//--PROCESS-BC---------------------------------------------------------------------------------------------

void  CedaDEVData::ProcessDEVBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlDEVData = (struct BcStruct *) vpDataPointer;
	if (strstr(prlDEVData->Selection,"URNO") == NULL)
	{
		CString olDadr = GetDadrFromSelection(prlDEVData->Selection);
		DEVDATA *prlDEV = GetDEVByDadr(olDadr);
		if(ipDDXType == BC_DEV_CHANGE)
		{
			if(prlDEV != NULL)
			{
				GetRecordFromItemList(prlDEV,prlDEVData->Fields,prlDEVData->Data);
			    if(ValidateDEVBcData(prlDEV->Dadr)) //Prf: 8795
				{
					UpdateDEVInternal(prlDEV);
				}
				else
				{
					DeleteDEVInternal(prlDEV);
				}
			}
			else
			{
				prlDEV = new DEVDATA;
				GetRecordFromItemList(prlDEV,prlDEVData->Fields,prlDEVData->Data);
			    if(ValidateDEVBcData(prlDEV->Dadr)) //Prf: 8795
				{
					InsertDEVInternal(prlDEV);
				}
				else
				{
					delete prlDEV;
				}
			}
		}
		if(ipDDXType == BC_DEV_DELETE)
		{
			if (prlDEV != NULL)
			{
				DeleteDEVInternal(prlDEV);
			}
		}
		
	}
	else
	{
		long llUrno = GetUrnoFromSelection(prlDEVData->Selection);

		DEVDATA *prlDEV = GetDEVByUrno(llUrno);
		if(ipDDXType == BC_DEV_CHANGE)
		{
			if(prlDEV != NULL)
			{
				GetRecordFromItemList(prlDEV,prlDEVData->Fields,prlDEVData->Data);
			    if(ValidateDEVBcData(prlDEV->Urno)) //Prf: 8795
				{
					UpdateDEVInternal(prlDEV);
				}
				else
				{
					DeleteDEVInternal(prlDEV);
				}
			}
			else
			{
				prlDEV = new DEVDATA;
				GetRecordFromItemList(prlDEV,prlDEVData->Fields,prlDEVData->Data);
			    if(ValidateDEVBcData(prlDEV->Urno)) //Prf: 8795
				{
					InsertDEVInternal(prlDEV);
				}
				else
				{
					delete prlDEV;
				}
			}
		}
		if(ipDDXType == BC_DEV_DELETE)
		{
			if (prlDEV != NULL)
			{
				DeleteDEVInternal(prlDEV);
			}
		}
	}
}

//Prf: 8795
//--ValidateDEVBcData--------------------------------------------------------------------------------------

bool CedaDEVData::ValidateDEVBcData(const long& lrpUrno)
{
	bool blValidateDEVBcData = true;
	if(!omWhere.IsEmpty()) //It means it is not the default view and we need to check in the database, since we don't need to check for the default view
	{
		char chWhere[20];
		sprintf(chWhere, "URNO='%ld'",lrpUrno);
		CString olWhere = omWhere + CString(" AND ") + CString(chWhere);
		CCSPtrArray<DEVDATA> olDevs;
		if(!ReadSpecialData(&olDevs,olWhere.GetBuffer(0),"URNO",false))
		{		
			blValidateDEVBcData = false;
		}
	}
	return blValidateDEVBcData;
}

//Prf: 8795
//--ValidateDEVBcData--------------------------------------------------------------------------------------

bool CedaDEVData::ValidateDEVBcData(const CString& ropDadr)
{
	bool blValidateDEVBcData = true;
	if(!omWhere.IsEmpty()) //It means it is not the default view and we need to check in the database, since we don't need to check for the default view
	{
		char chWhere[20];
		sprintf(chWhere, "DADR='%s'",ropDadr);
		CString olWhere = omWhere + CString(" AND ") + CString(chWhere);
		CCSPtrArray<DEVDATA> olDevs;
		if(!ReadSpecialData(&olDevs,olWhere.GetBuffer(0),"DADR",false))
		{		
			blValidateDEVBcData = false;
		}
	}
	return blValidateDEVBcData;
}

CString CedaDEVData::GetDadrFromSelection(CString opSelection)
{
	int ilFirst = opSelection.Find('\'');
	int ilLast  = opSelection.ReverseFind('\'');
	if ((ilFirst == -1) || (ilLast == -1) || ((ilLast-ilFirst) < 3))
	{
		return "";
	}

	ilFirst++;
	return(opSelection.Mid(ilFirst,ilLast-ilFirst));
}
//---------------------------------------------------------------------------------------------------------


int CedaDEVData::GetALGCLength()
{
	CCSPtrArray<RecordSet> olBuffer;
	RecordSet olRec;
	CString olTmp;
	int ilRet = 4;

	ogBCD.SetObject("SYS","FELE");
	
	int ilCount = ogBCD.ReadSpecial("SYS", CString("FELE"), CString("WHERE TANA = 'DEV' AND FINA = 'ALGC'"), olBuffer);

	if(olBuffer.GetSize() == 1)
	{
		olRec = olBuffer[0];
		olTmp = olRec.Values[0];	
		ilRet = atoi(olTmp);

	}

	olBuffer.DeleteAll();

	return ilRet;

}
