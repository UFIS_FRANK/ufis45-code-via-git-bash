// AirportDlg.cpp : implementation file
//

#include <stdafx.h>
#include <BDPSUIF.h>
#include <CedaBasicData.h>
#include <AirportDlg.h>
#include <PrivList.h>
#include <process.h>
#include <util.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// AirportDlg dialog


AirportDlg::AirportDlg(APTDATA *popAPT,int iDlgTyp,CWnd* pParent /*=NULL*/) : CDialog(AirportDlg::IDD, pParent)
{
	pomAPT = popAPT;
	m_iDlgTyp = iDlgTyp;
	m_bReceiveData = false;
	m_StrReceiveData = "";
	
	//{{AFX_DATA_INIT(AirportDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void AirportDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(AirportDlg)
	DDX_Control(pDX, IDC_BLACK_LIST, m_BLACK_LIST);
	DDX_Control(pDX, IDC_ST_CON, m_ST_CON);
	DDX_Control(pDX, IDC_CON, m_CON);
	DDX_Control(pDX, IDC_CONTINENT, m_CONTINENT);
	DDX_Control(pDX, IDOK, m_OK);
	DDX_Control(pDX, IDC_CDAT_D, m_CDATD);
	DDX_Control(pDX, IDC_CDAT_T, m_CDATT);
	DDX_Control(pDX, IDC_LSTU_D, m_LSTUD);
	DDX_Control(pDX, IDC_LSTU_T, m_LSTUT);
	DDX_Control(pDX, IDC_USEU,	 m_USEU);
	DDX_Control(pDX, IDC_USEC,	 m_USEC);
	DDX_Control(pDX, IDC_APC3,	 m_APC3);
	DDX_Control(pDX, IDC_APC4,	 m_APC4);
	DDX_Control(pDX, IDC_APFN,	 m_APFN);
	DDX_Control(pDX, IDC_APSN,	 m_APSN);
	DDX_Control(pDX, IDC_APN2,	 m_APN2);
	DDX_Control(pDX, IDC_APN3,	 m_APN3);
	DDX_Control(pDX, IDC_APN4,	 m_APN4);
	DDX_Control(pDX, IDC_ETOF,	 m_ETOF);
	DDX_Control(pDX, IDC_GRUP,	 m_GRUP);
	DDX_Control(pDX, IDC_LAND,	 m_LAND);
	DDX_Control(pDX, IDC_TDI1,	 m_TDI1);
	DDX_Control(pDX, IDC_TDI2,	 m_TDI2);
	DDX_Control(pDX, IDC_TICH_D, m_TICHD);
	DDX_Control(pDX, IDC_TICH_T, m_TICHT);
	DDX_Control(pDX, IDC_VAFR_D, m_VAFRD);
	DDX_Control(pDX, IDC_VAFR_T, m_VAFRT);
	DDX_Control(pDX, IDC_VATO_D1, m_VATOD);
	DDX_Control(pDX, IDC_VATO_T1, m_VATOT);
	DDX_Control(pDX, IDC_APTT,	 m_APTT);
	DDX_Control(pDX, IDC_HOME,	 m_HOME);
	DDX_Control(pDX, IDC_TDIS,	 m_TDIS);
	DDX_Control(pDX, IDC_TDIW,	 m_TDIW);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(AirportDlg, CDialog)
	//{{AFX_MSG_MAP(AirportDlg)
	ON_BN_CLICKED(IDC_BUTTON1, OnForeignLangRemarks)
	ON_MESSAGE(WM_DATA_RECEIVED,OnReceiveMsg)
	ON_BN_CLICKED(IDC_CON, OnContinent)
	ON_BN_CLICKED(IDC_BLACK_LIST, OnBlackList)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// AirportDlg message handlers
//----------------------------------------------------------------------------------------

BOOL AirportDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	//Read the continents
	if(ogBasicData.IsContinentAvailable())
	{
		CString clWhere;
		clWhere.Format("WHERE STAT<> '%s'","D");

		ogBCD.Read("CNT",clWhere);
	}

	m_Caption = LoadStg(IDS_STRING174) + " <" + m_Caption + ">";
	SetWindowText(m_Caption);
	char clStat = ogPrivList.GetStat("AIRPORTDLG.m_OK");
	SetWndStatAll(clStat,m_OK);

	//------------------------------------
	m_CDATD.SetBKColor(SILVER);
	m_CDATD.SetInitText(pomAPT->Cdat.Format("%d.%m.%Y"));
	m_CDATD.SetSecState(ogPrivList.GetStat("AIRPORTDLG.m_CDAT"));
	m_CDATT.SetBKColor(SILVER);
	m_CDATT.SetInitText(pomAPT->Cdat.Format("%H:%M"));
	m_CDATT.SetSecState(ogPrivList.GetStat("AIRPORTDLG.m_CDAT"));
	//------------------------------------
	m_LSTUD.SetBKColor(SILVER);
	m_LSTUD.SetInitText(pomAPT->Lstu.Format("%d.%m.%Y"));
	m_LSTUD.SetSecState(ogPrivList.GetStat("AIRPORTDLG.m_LSTU"));
	m_LSTUT.SetBKColor(SILVER);
	m_LSTUT.SetInitText(pomAPT->Lstu.Format("%H:%M"));
	m_LSTUT.SetSecState(ogPrivList.GetStat("AIRPORTDLG.m_LSTU"));
	//------------------------------------
	m_USEC.SetBKColor(SILVER);
	m_USEC.SetInitText(pomAPT->Usec);
	m_USEC.SetSecState(ogPrivList.GetStat("AIRPORTDLG.m_USEC"));
	//------------------------------------
	m_USEU.SetBKColor(SILVER);
	m_USEU.SetInitText(pomAPT->Useu);
	m_USEU.SetSecState(ogPrivList.GetStat("AIRPORTDLG.m_USEU"));
	//------------------------------------
	m_GRUP.SetBKColor(SILVER);
	//m_GRUP.SetInitText(pomAPT->);
	m_GRUP.SetSecState(ogPrivList.GetStat("AIRPORTDLG.m_GRUP"));
	//------------------------------------//WHITE,YELLOW,SILVER,RED,LYELLOW
	m_APC3.SetFormat("x|#x|#x|#");
	//m_APC3.SetBKColor(YELLOW);  
	//m_APC3.SetTextLimit(-1,-1,true);
	m_APC3.SetTextErrColor(RED);
	m_APC3.SetInitText(pomAPT->Apc3);
	m_APC3.SetSecState(ogPrivList.GetStat("AIRPORTDLG.m_APC3"));
	//------------------------------------
	m_APC4.SetFormat("x|#x|#x|#x|#");
	m_APC4.SetBKColor(YELLOW);  
	m_APC4.SetTextErrColor(RED);
	m_APC4.SetInitText(pomAPT->Apc4);
	m_APC4.SetSecState(ogPrivList.GetStat("AIRPORTDLG.m_APC4"));
	//------------------------------------
	m_APFN.SetTypeToString("X(32)",32,1);
	m_APFN.SetBKColor(YELLOW);
	m_APFN.SetTextErrColor(RED);
	m_APFN.SetInitText(pomAPT->Apfn);
	m_APFN.SetSecState(ogPrivList.GetStat("AIRPORTDLG.m_APFN"));
	//------------------------------------
	m_APSN.SetTypeToString("X(20)",20,0);
//	m_APSN.SetBKColor(YELLOW);
	m_APSN.SetTextErrColor(RED);
	m_APSN.SetInitText(pomAPT->Apsn);
	m_APSN.SetSecState(ogPrivList.GetStat("AIRPORTDLG.m_APSN"));
	//------------------------------------
	m_APN2.SetTypeToString("X(20)",20,0);
	m_APN2.SetTextErrColor(RED);
	m_APN2.SetInitText(pomAPT->Apn2);
	m_APN2.SetSecState(ogPrivList.GetStat("AIRPORTDLG.m_APN2"));
	//------------------------------------
	m_APN3.SetTypeToString("X(20)",20,0);
	m_APN3.SetTextErrColor(RED);
	m_APN3.SetInitText(pomAPT->Apn3);
	m_APN3.SetSecState(ogPrivList.GetStat("AIRPORTDLG.m_APN3"));
	//------------------------------------
	m_APN4.SetTypeToString("X(20)",20,0);
	m_APN4.SetTextErrColor(RED);
	m_APN4.SetInitText(pomAPT->Apn4);
	m_APN4.SetSecState(ogPrivList.GetStat("AIRPORTDLG.m_APN4"));
	//------------------------------------
	if ( bgCcode == false)
		m_LAND.SetFormat("XX"); //
	else
		m_LAND.SetTypeToString("X(2)",2,1); // To accept two or one character
	m_LAND.SetBKColor(YELLOW);  
	m_LAND.SetTextErrColor(RED);
	m_LAND.SetInitText(pomAPT->Land);
	m_LAND.SetSecState(ogPrivList.GetStat("AIRPORTDLG.m_LAND"));
	//------------------------------------
	m_APTT.SetFormat("X");
//	m_APTT.SetBKColor(YELLOW);  
	m_APTT.SetTextErrColor(RED);
	m_APTT.SetInitText(pomAPT->Aptt);
	m_APTT.SetSecState(ogPrivList.GetStat("AIRPORTDLG.m_APTT"));
	//------------------------------------
	m_ETOF.SetTypeToInt(0,9999);
	m_ETOF.SetTextErrColor(RED);
	m_ETOF.SetInitText(pomAPT->Etof);
	m_ETOF.SetSecState(ogPrivList.GetStat("AIRPORTDLG.m_ETOF"));
	//------------------------------------
	m_TICHD.SetTypeToDate();
	m_TICHD.SetTextErrColor(RED);
	m_TICHD.SetInitText(pomAPT->Tich.Format("%d.%m.%Y"));
	m_TICHD.SetSecState(ogPrivList.GetStat("AIRPORTDLG.m_TICH"));
	// - - - - - - - - - - - - - - - - - - 
	m_TICHT.SetTypeToTime();
	m_TICHT.SetTextErrColor(RED);
	m_TICHT.SetInitText(pomAPT->Tich.Format("%H:%M"));
	m_TICHT.SetSecState(ogPrivList.GetStat("AIRPORTDLG.m_TICH"));
	//------------------------------------
	m_TDI1.SetTypeToInt(-999,999);
	m_TDI1.SetTextErrColor(RED);
	m_TDI1.SetInitText(pomAPT->Tdi1);
	m_TDI1.SetSecState(ogPrivList.GetStat("AIRPORTDLG.m_TDI1"));
	//------------------------------------
	m_TDI2.SetTypeToInt(-999,999);
	m_TDI2.SetTextErrColor(RED);
	m_TDI2.SetInitText(pomAPT->Tdi2);
	m_TDI2.SetSecState(ogPrivList.GetStat("AIRPORTDLG.m_TDI2"));
	//------------------------------------
	m_VAFRD.SetTypeToDate(true);
	m_VAFRD.SetTextErrColor(RED);
	m_VAFRD.SetBKColor(YELLOW);
	m_VAFRD.SetInitText(pomAPT->Vafr.Format("%d.%m.%Y"));
	m_VAFRD.SetSecState(ogPrivList.GetStat("AIRPORTDLG.m_VAFR"));
	// - - - - - - - - - - - - - - - - - -
	m_VAFRT.SetTypeToTime(true);
	m_VAFRT.SetTextErrColor(RED);
	m_VAFRT.SetBKColor(YELLOW);
	m_VAFRT.SetInitText(pomAPT->Vafr.Format("%H:%M"));
	m_VAFRT.SetSecState(ogPrivList.GetStat("AIRPORTDLG.m_VAFR"));
	//------------------------------------
	m_VATOD.SetTypeToDate();
	m_VATOD.SetTextErrColor(RED);
	m_VATOD.SetInitText(pomAPT->Vato.Format("%d.%m.%Y"));
	m_VATOD.SetSecState(ogPrivList.GetStat("AIRPORTDLG.m_VATO"));
	// - - - - - - - - - - - - - - - - - -
	m_VATOT.SetTypeToTime();
	m_VATOT.SetTextErrColor(RED);
	m_VATOT.SetInitText(pomAPT->Vato.Format("%H:%M"));
	m_VATOT.SetSecState(ogPrivList.GetStat("AIRPORTDLG.m_VATO"));
	//------------------------------------
	m_HOME.SetTypeToString("X(3)",3,1);
	m_HOME.SetTextErrColor(RED);
	m_HOME.SetInitText(pomAPT->Home);
	m_HOME.SetSecState(ogPrivList.GetStat("AIRPORTDLG.m_HOME"));
	//------------------------------------
	m_TDIS.SetTypeToInt(-999,999);
	m_TDIS.SetTextErrColor(RED);
	m_TDIS.SetInitText(pomAPT->Tdis);
	m_TDIS.SetSecState(ogPrivList.GetStat("AIRPORTDLG.m_TDIS"));
	//------------------------------------
	m_TDIW.SetTypeToInt(-999,999);
	m_TDIW.SetTextErrColor(RED);
	m_TDIW.SetInitText(pomAPT->Tdiw);
	m_TDIW.SetSecState(ogPrivList.GetStat("AIRPORTDLG.m_TDIW"));
	//------------------------------------
	m_BLACK_LIST.SetSecState(ogPrivList.GetStat("AIRPORTDLG.m_BLACK_LIST"));
	//------------------------------------
	//Added by Christine
	if(ogBasicData.IsContinentAvailable())
	{
		m_CONTINENT.SetTypeToString("X(3)",3,1);
		m_CONTINENT.SetTextErrColor(RED);
		CString tmp=pomAPT->Cont;
		//strcpy(tmp,pomAPT->Cont);
		CString strContCode = ogBCD.GetField("CNT", "URNO", tmp, "CODE");
		m_CONTINENT.SetInitText(strContCode);
		m_CONTINENT.SetSecState(ogPrivList.GetStat("AIRPORTDLG.m_CONTINENT"));

		char clStat = ogPrivList.GetStat("AIRPORTDLG.m_CONTINENT");
		switch (clStat)
		{
			case '1': 
				m_CONTINENT.EnableWindow(TRUE);
				m_CON.EnableWindow(TRUE);
				m_ST_CON.ShowWindow(SW_SHOW); 
				break;
			case '0': 
				m_CONTINENT.EnableWindow(FALSE);
				m_CON.EnableWindow(FALSE);
				m_ST_CON.ShowWindow(SW_SHOW); 
				break;
			case '-': 
				m_CONTINENT.ShowWindow(SW_HIDE);
				m_CON.ShowWindow(SW_HIDE);
				m_ST_CON.ShowWindow(SW_HIDE); 
				break;
		}

	}
	else
	{
		//m_CONTINENT.SetSecState(ogPrivList.GetStat("AIRPORTDLG.m_CONTINENT"));
		m_CONTINENT.ShowWindow(SW_HIDE);
		m_CON.ShowWindow(FALSE);
		m_ST_CON.ShowWindow(FALSE);
	}
	//------------------------------------

	if(m_iDlgTyp)
	{
		CWnd* pWnd ;
		//Abbreviated Name Text Box1
		pWnd = GetDlgItem(IDC_APSN);
		pWnd->ShowWindow(0);

		//Abbreviated Name Text Box2
		pWnd = GetDlgItem(IDC_APN2);
		pWnd->ShowWindow(0);

		//Abbreviated Name Text Box3
		pWnd = GetDlgItem(IDC_APN3);
		pWnd->ShowWindow(0);

		//Abbreviated Name Text Box4
		pWnd = GetDlgItem(IDC_APN4);
		pWnd->ShowWindow(0);

		//Static Text Abbreviated Name 1
		pWnd = GetDlgItem(IDC_STATIC1);
		pWnd->ShowWindow(0);
		
		//Static Text Abbreviated Name 2
		pWnd = GetDlgItem(IDC_STATIC2);
		pWnd->ShowWindow(0);

		//Static Text Abbreviated Name 3
		pWnd = GetDlgItem(IDC_STATIC3);
		pWnd->ShowWindow(0);

		//Static Text Abbreviated Name 4
		pWnd = GetDlgItem(IDC_STATIC4);
		pWnd->ShowWindow(0);

		CWnd* pWndBut = GetDlgItem(IDC_BUTTON1);
		pWndBut->ShowWindow(1);

		//delete pWnd;
		//delete pWndBut;

		ResizeWindow();

	}

	mdBlackListProcId=NULL;
	
	return TRUE;  
}

//----------------------------------------------------------------------------------------

void AirportDlg::OnOK() 
{
	AfxGetApp()->DoWaitCursor(1);
	CString olErrorText;
	bool ilStatus = true;
	if(m_APC3.GetStatus() == false)
	{
		if(m_APC3.GetWindowTextLength() > 0)
		/*{
			olErrorText += LoadStg(IDS_STRING290) +  ogNoData;
		}
		else*/
		{
			ilStatus = false;
			olErrorText += LoadStg(IDS_STRING290) + ogNotFormat;
		}
	}
	if(m_APC4.GetStatus() == false)
	{
		ilStatus = false;
		if(m_APC4.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING300) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING300) + ogNotFormat;
		}
	}
	if(m_APFN.GetStatus() == false)
	{
		ilStatus = false;
		if(m_APFN.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING288) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING288) + ogNotFormat;
		}
	}

	if(!bgUnicode)
	{
	if(m_APSN.GetStatus() == false)
	{
		ilStatus = false;
//		if(m_APSN.GetWindowTextLength() == 0)
//		{
//			olErrorText += LoadStg(IDS_STRING301) + CString(" 1") +  ogNoData;
//		}
//		else
//		{
			olErrorText += LoadStg(IDS_STRING301) + CString(" 1") + ogNotFormat;
//		}
	}
	if(m_APN2.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING301) + CString(" 2") + ogNotFormat;
	}
	if(m_APN3.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING301) + CString(" 3") + ogNotFormat;
	}
	if(m_APN4.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING301) + CString(" 4") + ogNotFormat;
	}
	}

	if(m_LAND.GetStatus() == false) // veda
	{
		ilStatus = false;
		if(m_LAND.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING302) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING302) + ogNotFormat;
		}
	}
	if(m_APTT.GetStatus() == false)
	{
		if(!m_APTT.GetWindowTextLength() == 0)
		{
			ilStatus = false;
			olErrorText += LoadStg(IDS_STRING303) + ogNotFormat;
		}

//		if(m_APTT.GetWindowTextLength() == 0)
//		{
//			olErrorText += LoadStg(IDS_STRING303) +  ogNoData;
//		}
//		else
//		{
//		}
	}
	if(m_ETOF.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING304) + ogNotFormat;
	}
	if(m_TICHD.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING305) + LoadStg(IDS_STRING232) + ogNotFormat;
	}
	if(m_TICHT.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING305) + LoadStg(IDS_STRING233) + ogNotFormat;
	}
	if(m_TDI1.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING306) + ogNotFormat;
	}
	if(m_TDI2.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING307) + ogNotFormat;
	}
	if(m_VAFRD.GetStatus() == false)
	{
		ilStatus = false;
		if(m_VAFRD.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING232) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING232) + ogNotFormat;
		}
	}
	if(m_VAFRT.GetStatus() == false)
	{
		ilStatus = false;
		if(m_VAFRT.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING233) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING233) + ogNotFormat;
		}
	}
	if(m_VATOD.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING231) + LoadStg(IDS_STRING232) + ogNotFormat;
	}
	if(m_VATOT.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING231) + LoadStg(IDS_STRING233) + ogNotFormat;
	}

	if(m_TDIS.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING990) + ogNotFormat;
	}
	if(m_TDIW.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING991) + ogNotFormat;
	}
	if(ogBasicData.IsContinentAvailable())
	{
		/*
		if(m_CONTINENT.GetStatus() == false)
		{
			ilStatus = false;
			olErrorText += LoadStg(IDS_STRING32827) + ogNotFormat;
		}
		*/
	}
	///////////////////////////////////////////////////////////////////////////
	CString olVafrd,olVafrt,olVatod,olVatot;
	CTime olTmpTimeFr,olTmpTimeTo;
	m_VAFRD.GetWindowText(olVafrd);
	m_VAFRT.GetWindowText(olVafrt);
	m_VATOD.GetWindowText(olVatod);
	m_VATOT.GetWindowText(olVatot);

	if((m_VAFRD.GetWindowTextLength() != 0 && m_VAFRT.GetWindowTextLength() == 0) || (m_VAFRD.GetWindowTextLength() == 0 && m_VAFRT.GetWindowTextLength() != 0))
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING234);
	}
	if((m_VATOD.GetWindowTextLength() != 0 && m_VATOT.GetWindowTextLength() == 0) || (m_VATOD.GetWindowTextLength() == 0 && m_VATOT.GetWindowTextLength() != 0))
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING235);
	}
	if(m_VAFRD.GetStatus() == true && m_VAFRT.GetStatus() == true && m_VATOD.GetStatus() == true && m_VATOT.GetStatus() == true)
	{
		olTmpTimeFr = DateHourStringToDate(olVafrd,olVafrt);
		olTmpTimeTo = DateHourStringToDate(olVatod,olVatot);
		if(olTmpTimeTo != -1 && (olTmpTimeTo < olTmpTimeFr || olTmpTimeFr == -1))
		{
			ilStatus = false;
			olErrorText += LoadStg(IDS_STRING236);
		}
	}

	if((m_TICHD.GetWindowTextLength() != 0 && m_TICHT.GetWindowTextLength() == 0) || (m_TICHD.GetWindowTextLength() == 0 && m_TICHT.GetWindowTextLength() != 0))
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING308);
	}
	//Konsistenz-Check//
	if(ilStatus == true)
	{
		CString olApc3,olApc4, olAptt;
		char clWhere[100];
		CCSPtrArray<APTDATA> olAptCPA;

		m_APC3.GetWindowText(olApc3);
		if(olApc3.GetLength() == 3)
		{
			sprintf(clWhere,"WHERE APC3='%s'",olApc3);
			if(ogAPTData.ReadSpecialData(&olAptCPA,clWhere,"URNO,APC3",false) == true)
			{
				if(olAptCPA[0].Urno != pomAPT->Urno)
				{
					ilStatus = false;
					olErrorText += LoadStg(IDS_STRING290) + LoadStg(IDS_EXIST);
				}
			}
			olAptCPA.DeleteAll();
		}

		m_APC4.GetWindowText(olApc4);
		sprintf(clWhere,"WHERE APC4='%s'",olApc4);
		if(ogAPTData.ReadSpecialData(&olAptCPA,clWhere,"URNO,APC4",false) == true)
		{
			if(olAptCPA[0].Urno != pomAPT->Urno)
			{
				ilStatus = false;
				olErrorText += LoadStg(IDS_STRING300) + LoadStg(IDS_EXIST);
			}
		}
		olAptCPA.DeleteAll();

		m_APTT.GetWindowText(olAptt);
		if (!olAptt.IsEmpty())
		{
/*****
			if ( (olAptt.Find('S') != 0) && (olAptt.Find('D') != 0) && (olAptt.Find('I') != 0) )
			{
				ilStatus = false;
				olErrorText += LoadStg(IDS_STRING710) +  "> invalid Data!";//LoadStg(IDS_EXIST);
			}
*****/
		}

	}
	///////////////////////////////////////////////////////////////////////////
	AfxGetApp()->DoWaitCursor(-1);
	if (ilStatus == true)
	{
		m_APC3.GetWindowText(pomAPT->Apc3,4);
		m_APC4.GetWindowText(pomAPT->Apc4,5);
		m_APFN.GetWindowText(pomAPT->Apfn,33);
		
		if(bgUnicode)
		{
			if(!m_StrReceiveData.IsEmpty() && m_bReceiveData)
			{
				
				CString str = m_StrReceiveData;
				CStringArray olStrArray;
							
				int k = 0;
				//Abbrev1
				for(int j = 0 ; j <4;j++)
				{
					k = str.Find(",");
					CString olStr = str.Left(k);
					olStrArray.Add(str.Left(k));
					str =	str.Mid(k+1);
				}

				olStrArray.Add(str);

				sprintf(pomAPT->Apsn,"%s",olStrArray.GetAt(0)); 
				sprintf(pomAPT->Apn2,"%s",olStrArray.GetAt(1)); 
				sprintf(pomAPT->Apn3,"%s",olStrArray.GetAt(2));
				sprintf(pomAPT->Apn4,"%s",olStrArray.GetAt(3)); 
			}
		}
		else
		{
		m_APSN.GetWindowText(pomAPT->Apsn,21);
		m_APN2.GetWindowText(pomAPT->Apn2,21);
		m_APN3.GetWindowText(pomAPT->Apn3,21);
		m_APN4.GetWindowText(pomAPT->Apn4,21);
		}
		m_LAND.GetWindowText(pomAPT->Land,3);
		m_APTT.GetWindowText(pomAPT->Aptt,2);
		m_ETOF.GetWindowText(pomAPT->Etof,5);
		m_TDI1.GetWindowText(pomAPT->Tdi1,5);
		m_TDI2.GetWindowText(pomAPT->Tdi2,5);
		m_HOME.GetWindowText(pomAPT->Home,4);
		CString olTichd,olTicht;
		m_TICHD.GetWindowText(olTichd);
		m_TICHT.GetWindowText(olTicht);
		pomAPT->Tich = DateHourStringToDate(olTichd,olTicht);
		pomAPT->Vafr = DateHourStringToDate(olVafrd,olVafrt);
		pomAPT->Vato = DateHourStringToDate(olVatod,olVatot);
		m_TDIS.GetWindowText(pomAPT->Tdis,5);
		m_TDIW.GetWindowText(pomAPT->Tdiw,5);

		//Added by Christine
		if(ogBasicData.IsContinentAvailable())
		{
			CString tmpCon;	
			m_CONTINENT.GetWindowText(tmpCon);

			//m_CONTINENT.GetWindowText(pomAPT->Cont,14);
			//URNO will be return from C#dialog
			if(m_strCONUrno.GetLength() >0)
			{
				
				if(tmpCon.GetLength() > 0)
				{
					strcpy(pomAPT->Cont,m_strCONUrno);
				}
				else
				{
					strcpy(pomAPT->Cont,"");
				}
			}
			else
			{
				if(tmpCon.GetLength() > 0)
				{
					
				}
				else
				{
					strcpy(pomAPT->Cont,"");
				}
				
			}
		}
		else
		{
			strcpy(pomAPT->Cont,"");
		}

		//Kill the process
		if (mdBlackListProcId != NULL)			
		{		
			killProcess( mdBlackListProcId );	
		}	


		CDialog::OnOK();
	}
	else
	{
		Beep(440,70);
		MessageBox(olErrorText,LoadStg(IDS_STRING145),MB_ICONERROR);
		m_APC3.SetFocus();
		m_APC3.SetSel(0,-1);
	}
}

//----------------------------------------------------------------------------------------


void AirportDlg::ResizeWindow()
{
	/*CRect olWindowRectAbove,olWindowRectBelow;
	CRect lDefault(0,0,15,15);
	((CWnd*)GetDlgItem(IDC_APC3))->GetWindowRect(olWindowRectAbove);
	ScreenToClient(olWindowRectAbove);
	((CWnd*)GetDlgItem(IDC_LAND))->GetWindowRect(olWindowRectBelow);
	ScreenToClient(olWindowRectBelow);
	
	olWindowRectBelow.top = olWindowRectAbove.bottom + lDefault.Height();
	olWindowRectBelow.bottom = olWindowRectBelow.top + olWindowRectBelow.Height();
	((CWnd*)GetDlgItem(IDC_LAND))->MoveWindow(olWindowRectBelow);*/



	CRect olWindowRect;
	CRect olNameWindowRect;
	int olOffset = 0;

	((CEdit*)GetDlgItem(IDC_BUTTON1))->GetWindowRect(olNameWindowRect);
	ScreenToClient(olNameWindowRect);

	((CStatic*)GetDlgItem(IDC_APN4))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);

	olOffset = olWindowRect.bottom - olNameWindowRect.bottom;

	CRect olDeflateRect(0,-(olOffset),0,olOffset);
	this->GetWindowRect(&olWindowRect);
	olWindowRect.bottom -= olOffset;
	this->MoveWindow(olWindowRect);	
	
	
	//Country Code Edit Box
	((CEdit*)GetDlgItem(IDC_LAND))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);
	olWindowRect.DeflateRect(olDeflateRect);
	((CEdit*)GetDlgItem(IDC_LAND))->MoveWindow(olWindowRect);

	((CStatic*)GetDlgItem(IDC_STATIC_CONTRYCODE))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);
	olWindowRect.DeflateRect(olDeflateRect);
	((CStatic*)GetDlgItem(IDC_STATIC_CONTRYCODE))->MoveWindow(olWindowRect);

	//Airport Type
	((CEdit*)GetDlgItem(IDC_APTT))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);
	olWindowRect.DeflateRect(olDeflateRect);
	((CEdit*)GetDlgItem(IDC_APTT))->MoveWindow(olWindowRect);

	((CStatic*)GetDlgItem(IDC_STATIC_APTTYP))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);
	olWindowRect.DeflateRect(olDeflateRect);
	((CStatic*)GetDlgItem(IDC_STATIC_APTTYP))->MoveWindow(olWindowRect);	
	
	((CStatic*)GetDlgItem(IDC_STATIC_IDS))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);
	olWindowRect.DeflateRect(olDeflateRect);
	((CStatic*)GetDlgItem(IDC_STATIC_IDS))->MoveWindow(olWindowRect);	

	//Time Change 
	((CEdit*)GetDlgItem(IDC_TICH_D))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);
	olWindowRect.DeflateRect(olDeflateRect);
	((CEdit*)GetDlgItem(IDC_TICH_D))->MoveWindow(olWindowRect);

	((CEdit*)GetDlgItem(IDC_TICH_T))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);
	olWindowRect.DeflateRect(olDeflateRect);
	((CEdit*)GetDlgItem(IDC_TICH_T))->MoveWindow(olWindowRect);

	((CStatic*)GetDlgItem(IDC_STATIC_TIMCHAN))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);
	olWindowRect.DeflateRect(olDeflateRect);
	((CStatic*)GetDlgItem(IDC_STATIC_TIMCHAN))->MoveWindow(olWindowRect);
	
	//Standard Flight time
	((CEdit*)GetDlgItem(IDC_ETOF))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);
	olWindowRect.DeflateRect(olDeflateRect);
	((CEdit*)GetDlgItem(IDC_ETOF))->MoveWindow(olWindowRect);

	((CStatic*)GetDlgItem(IDC_STATIC_STDFLTTIM))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);
	olWindowRect.DeflateRect(olDeflateRect);
	((CStatic*)GetDlgItem(IDC_STATIC_STDFLTTIM))->MoveWindow(olWindowRect);

	((CStatic*)GetDlgItem(IDC_STATIC_MMM))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);
	olWindowRect.DeflateRect(olDeflateRect);
	((CStatic*)GetDlgItem(IDC_STATIC_MMM))->MoveWindow(olWindowRect);

	//Time Difference Before
	((CEdit*)GetDlgItem(IDC_TDI1))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);
	olWindowRect.DeflateRect(olDeflateRect);
	((CEdit*)GetDlgItem(IDC_TDI1))->MoveWindow(olWindowRect);

	((CStatic*)GetDlgItem(IDC_STATIC_TIMBFR))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);
	olWindowRect.DeflateRect(olDeflateRect);
	((CStatic*)GetDlgItem(IDC_STATIC_TIMBFR))->MoveWindow(olWindowRect);

	((CStatic*)GetDlgItem(IDC_STATIC_MM1))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);
	olWindowRect.DeflateRect(olDeflateRect);
	((CStatic*)GetDlgItem(IDC_STATIC_MM1))->MoveWindow(olWindowRect);

	//Time Difference After
	((CEdit*)GetDlgItem(IDC_TDI2))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);
	olWindowRect.DeflateRect(olDeflateRect);
	((CEdit*)GetDlgItem(IDC_TDI2))->MoveWindow(olWindowRect);

	((CStatic*)GetDlgItem(IDC_STATIC_TIMAFTR))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);
	olWindowRect.DeflateRect(olDeflateRect);
	((CStatic*)GetDlgItem(IDC_STATIC_TIMAFTR))->MoveWindow(olWindowRect);

	((CStatic*)GetDlgItem(IDC_STATIC_MM2))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);
	olWindowRect.DeflateRect(olDeflateRect);
	((CStatic*)GetDlgItem(IDC_STATIC_MM2))->MoveWindow(olWindowRect);
	
	//Static Group
	((CStatic*)GetDlgItem(IDC_STATIC_TIMGRP))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);
	olWindowRect.DeflateRect(olDeflateRect);
	((CStatic*)GetDlgItem(IDC_STATIC_TIMGRP))->MoveWindow(olWindowRect);
	
	//Time Difference in Summer
	((CEdit*)GetDlgItem(IDC_TDIS))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);
	olWindowRect.DeflateRect(olDeflateRect);
	((CEdit*)GetDlgItem(IDC_TDIS))->MoveWindow(olWindowRect);

	((CStatic*)GetDlgItem(IDC_STATIC_TIMSUM))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);
	olWindowRect.DeflateRect(olDeflateRect);
	((CStatic*)GetDlgItem(IDC_STATIC_TIMSUM))->MoveWindow(olWindowRect);

	//Time Difference in Winter
	((CEdit*)GetDlgItem(IDC_TDIW))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);
	olWindowRect.DeflateRect(olDeflateRect);
	((CEdit*)GetDlgItem(IDC_TDIW))->MoveWindow(olWindowRect);

	((CStatic*)GetDlgItem(IDC_STATIC_TIMWIN))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);
	olWindowRect.DeflateRect(olDeflateRect);
	((CStatic*)GetDlgItem(IDC_STATIC_TIMWIN))->MoveWindow(olWindowRect);

	//Static Group
	((CStatic*)GetDlgItem(IDC_STATIC_VALGRP))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);
	olWindowRect.DeflateRect(olDeflateRect);
	((CStatic*)GetDlgItem(IDC_STATIC_VALGRP))->MoveWindow(olWindowRect);

	//Valid From
	((CEdit*)GetDlgItem(IDC_VAFR_D))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);
	olWindowRect.DeflateRect(olDeflateRect);
	((CEdit*)GetDlgItem(IDC_VAFR_D))->MoveWindow(olWindowRect);

	((CEdit*)GetDlgItem(IDC_VAFR_T))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);
	olWindowRect.DeflateRect(olDeflateRect);
	((CEdit*)GetDlgItem(IDC_VAFR_T))->MoveWindow(olWindowRect);

	((CStatic*)GetDlgItem(IDC_STATIC_VALFRM))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);
	olWindowRect.DeflateRect(olDeflateRect);
	((CStatic*)GetDlgItem(IDC_STATIC_VALFRM))->MoveWindow(olWindowRect);

	//Valid To
	((CEdit*)GetDlgItem(IDC_VATO_D1))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);
	olWindowRect.DeflateRect(olDeflateRect);
	((CEdit*)GetDlgItem(IDC_VATO_D1))->MoveWindow(olWindowRect);

	((CEdit*)GetDlgItem(IDC_VATO_T1))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);
	olWindowRect.DeflateRect(olDeflateRect);
	((CEdit*)GetDlgItem(IDC_VATO_T1))->MoveWindow(olWindowRect);

	((CStatic*)GetDlgItem(IDC_STATIC_VALTO))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);
	olWindowRect.DeflateRect(olDeflateRect);
	((CStatic*)GetDlgItem(IDC_STATIC_VALTO))->MoveWindow(olWindowRect);	

	//Static Group
	((CStatic*)GetDlgItem(IDC_STATIC_CREATE))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);
	olWindowRect.DeflateRect(olDeflateRect);
	((CStatic*)GetDlgItem(IDC_STATIC_CREATE))->MoveWindow(olWindowRect);

	//Create On
	((CEdit*)GetDlgItem(IDC_CDAT_D))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);
	olWindowRect.DeflateRect(olDeflateRect);
	((CEdit*)GetDlgItem(IDC_CDAT_D))->MoveWindow(olWindowRect);

	((CEdit*)GetDlgItem(IDC_CDAT_T))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);
	olWindowRect.DeflateRect(olDeflateRect);
	((CEdit*)GetDlgItem(IDC_CDAT_T))->MoveWindow(olWindowRect);

	((CStatic*)GetDlgItem(IDC_STATIC_CREON))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);
	olWindowRect.DeflateRect(olDeflateRect);
	((CStatic*)GetDlgItem(IDC_STATIC_CREON))->MoveWindow(olWindowRect);	

	//Changed on
	((CEdit*)GetDlgItem(IDC_LSTU_D))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);
	olWindowRect.DeflateRect(olDeflateRect);
	((CEdit*)GetDlgItem(IDC_LSTU_D))->MoveWindow(olWindowRect);

	((CEdit*)GetDlgItem(IDC_LSTU_T))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);
	olWindowRect.DeflateRect(olDeflateRect);
	((CEdit*)GetDlgItem(IDC_LSTU_T))->MoveWindow(olWindowRect);

	((CStatic*)GetDlgItem(IDC_STATIC_CHANON))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);
	olWindowRect.DeflateRect(olDeflateRect);
	((CStatic*)GetDlgItem(IDC_STATIC_CHANON))->MoveWindow(olWindowRect);
	
	//Created By
	((CEdit*)GetDlgItem(IDC_USEC))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);
	olWindowRect.DeflateRect(olDeflateRect);
	((CEdit*)GetDlgItem(IDC_LSTU_T))->MoveWindow(olWindowRect);

	((CStatic*)GetDlgItem(IDC_STATIC_CREBY))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);
	olWindowRect.DeflateRect(olDeflateRect);
	((CStatic*)GetDlgItem(IDC_STATIC_CREBY))->MoveWindow(olWindowRect);

	//Changed By
	((CEdit*)GetDlgItem(IDC_USEU))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);
	olWindowRect.DeflateRect(olDeflateRect);
	((CEdit*)GetDlgItem(IDC_USEU))->MoveWindow(olWindowRect);

	((CStatic*)GetDlgItem(IDC_STATIC_CHABY))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);
	olWindowRect.DeflateRect(olDeflateRect);
	((CStatic*)GetDlgItem(IDC_STATIC_CHABY))->MoveWindow(olWindowRect);

	//OK button
	((CButton*)GetDlgItem(IDOK))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);
	olWindowRect.DeflateRect(olDeflateRect);
	((CButton*)GetDlgItem(IDOK))->MoveWindow(olWindowRect);

	//Cancel Button
	((CStatic*)GetDlgItem(IDCANCEL))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);
	olWindowRect.DeflateRect(olDeflateRect);
	((CStatic*)GetDlgItem(IDCANCEL))->MoveWindow(olWindowRect);

}

void AirportDlg::OnForeignLangRemarks() 
{
	CString olStr1,olStr2,olStr3,olStr4;
	olStr1 =  pomAPT->Apsn ;
	olStr2 =  pomAPT->Apn2;
	olStr3 =  pomAPT->Apn3;
	olStr4 =  pomAPT->Apn4;
	CString olStrDlg = "Airport";
	olStr1 = olStrDlg + "," + olStr1 + ","+ olStr2 +","+ olStr3 +","+ olStr4;

	char pclConfigPath[256];
	char pclTeluguDlgPath[256];

	if (getenv("CEDA") == NULL)
		strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
	else
		strcpy(pclConfigPath, getenv("CEDA"));

	GetPrivateProfileString(ogAppName, "UCDialog", "DEFAULT",
		pclTeluguDlgPath, sizeof pclTeluguDlgPath, pclConfigPath);


	if(!strcmp(pclTeluguDlgPath, "DEFAULT"))
		MessageBox(LoadStg(IDS_STRING32802), "Error:", MB_ICONERROR);

	char *args[4];
	char slRunTxt[1024];

	args[0] = "child";
	sprintf(slRunTxt,"%s",olStr1);
	args[1] = slRunTxt;
	args[2] = NULL;
	args[3] = NULL;

	int ilReturn = _spawnv(_P_NOWAIT,pclTeluguDlgPath,args);
}


void AirportDlg::OnReceiveMsg(WPARAM wParam, LPARAM lParam)
{
	

	RECEIVE_DATA* polReceiveData = (RECEIVE_DATA*)lParam;
	if(polReceiveData->m_Data.CompareNoCase("6B4CCF9C-5AA9-4ede-B298-08BAAAC9FB88") == 0)
	{
		m_bReceiveData = false;
		EnableWindow();
	}
	else if(polReceiveData->m_Data.CompareNoCase("71425678-6CF3-438f-AFA7-946DF4D5674F") == 0)
	{
		EnableWindow(FALSE);
	}
	else
	{
	m_bReceiveData = true;
	m_StrReceiveData = polReceiveData->m_Data;
		EnableWindow();

		
	}

	
}


void AirportDlg::OnContinent() 
{
	// TODO: Add your control notification handler code here
	char pclCheck[256];
	char pclConfigPath[256];
	
	if (getenv("CEDA") == NULL)
		strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
	else
		strcpy(pclConfigPath, getenv("CEDA"));

	
	
	GetPrivateProfileString(ogAppName, "BDPSUIF_ADDON" , "BDPS-UIF", pclCheck, sizeof pclCheck, pclConfigPath);	
	omModeWindowName = pclCheck ;	

	

	char clStat = ogPrivList.GetStat("AIRPORTDLG.m_CONTINENT");
	char buf[33];
	

	mdContProcId = LaunchTool( "UIF_CON", CString(itoa(pomAPT->Urno, buf, 10)), "", clStat, "BDPSUIF_ADDON",pclCheck , mdContProcId );

	


	
}




void AirportDlg::LoadCont(CString urno)
{
	CString strTmp = urno;
	//CString strTmp ="1033819847";
	m_strCONUrno = strTmp;

	if(strTmp.GetLength() > 0)
	{
		//Added by Christine
		if(ogBasicData.IsContinentAvailable())
		{
			CString clWhere;
			clWhere.Format("WHERE STAT<> '%s'","D");

			ogBCD.Read("CNT",clWhere);
		}

		
		//CString strContCode= ogBasicData.GetFieldByUrno("CTN", strTmp, "CODE");
		CString strContCode = ogBCD.GetField("CNT", "URNO", strTmp, "CODE");
		
		if(strContCode.GetLength() > 0)
		{
			m_CONTINENT.SetWindowText(strContCode);
		}
	}
}

void AirportDlg::UpdateCont(CString urno)
{
	
	//Added by Christine
	if(ogBasicData.IsContinentAvailable())
	{
		CString clWhere;
		clWhere.Format("WHERE STAT<> '%s'","D");
		ogBCD.Read("CNT",clWhere);

		CString tmp=pomAPT->Cont;
		CString strContCode = ogBCD.GetField("CNT", "URNO", tmp, "CODE");
		m_CONTINENT.SetInitText(strContCode);
	}
	
}
void AirportDlg::ResetContBlank()
{
	m_strCONUrno ="";
}


void AirportDlg::OnBlackList() 
{
	
	char pclCheck[256];
	char pclConfigPath[256];
	
	if (getenv("CEDA") == NULL)
		strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
	else
		strcpy(pclConfigPath, getenv("CEDA"));

	
	
	GetPrivateProfileString(ogAppName, "BDPSUIF_ADDON" , "BDPS-UIF", pclCheck, sizeof pclCheck, pclConfigPath);	
	omModeWindowName = pclCheck ;	

	
	//BWA_APT ,URNO1,URNO2|UserName,Permit,Time|100,100|Password
	char clStat = ogPrivList.GetStat("AIRPORTDLG.m_BLACK_LIST");
	if(pomAPT != NULL)
	{
		char buf[33];
		mdBlackListProcId = LaunchTool( "BWA_APT", CString(itoa(pomAPT->Urno, buf, 10)), "", clStat, "BDPSUIF_ADDON",pclCheck , mdBlackListProcId );
	}	
}
