// ArbeitsvertragsartenDlg.cpp : implementation file
//

#include <stdafx.h>
#include <BDPSUIF.h>
#include <ArbeitsvertragsartenDlg.h>
#include <CedaOrgData.h>
#include <PrivList.h>
#include <AwDlg.h>
#include <CheckReferenz.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// ArbeitsvertragsartenDlg dialog

ArbeitsvertragsartenDlg::ArbeitsvertragsartenDlg(COTDATA *popCot, CWnd* pParent /*=NULL*/)
	: CDialog(ArbeitsvertragsartenDlg::IDD, pParent)
{
	pomCot = popCot;
	//{{AFX_DATA_INIT(ArbeitsvertragsartenDlg)
	//}}AFX_DATA_INIT
	pomStatus = NULL;
}

void ArbeitsvertragsartenDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(ArbeitsvertragsartenDlg)
	DDX_Control(pDX, IDC_INBU,   m_Inbu);
	DDX_Control(pDX, IDOK,       m_OK);
	DDX_Control(pDX, IDC_CDAT_D, m_CDATD);
	DDX_Control(pDX, IDC_CDAT_T, m_CDATT);
	DDX_Control(pDX, IDC_CTRC,   m_CTRC);
	DDX_Control(pDX, IDC_CTRN,   m_CTRN);
	DDX_Control(pDX, IDC_DPTC,   m_DPTC);
	DDX_Control(pDX, IDC_LSTU_D, m_LSTUD);
	DDX_Control(pDX, IDC_LSTU_T, m_LSTUT);
	DDX_Control(pDX, IDC_REMA,   m_REMA);
	DDX_Control(pDX, IDC_USEC,   m_USEC);
	DDX_Control(pDX, IDC_USEU,   m_USEU);
	DDX_Control(pDX, IDC_WHPW,   m_WHPW);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(ArbeitsvertragsartenDlg, CDialog)
	//{{AFX_MSG_MAP(ArbeitsvertragsartenDlg)
	ON_BN_CLICKED(IDC_B_ORG_AW, OnBOrgAw)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// ArbeitsvertragsartenDlg message handlers
//----------------------------------------------------------------------------------------

BOOL ArbeitsvertragsartenDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	if(m_Caption == LoadStg(IDS_STRING150))
		bmChangeAction = true;
	else
		bmChangeAction = false;

	m_Caption = LoadStg(IDS_STRING162) + " <" + m_Caption + ">";
	SetWindowText(m_Caption);
	
	char clStat = ogPrivList.GetStat("ARBEITSVERTRAGSARTENDLG.m_OK");
	SetWndStatAll(clStat,m_OK);

	//------------------------------------
	m_CDATD.SetBKColor(SILVER);
	m_CDATD.SetInitText(pomCot->Cdat.Format("%d.%m.%Y"));
	m_CDATD.SetSecState(ogPrivList.GetStat("ARBEITSVERTRAGSARTENDLG.m_CDAT"));
	// - - - - - - - - - - - - - - - - - -
	m_CDATT.SetBKColor(SILVER);
	m_CDATT.SetInitText(pomCot->Cdat.Format("%H:%M"));
	m_CDATT.SetSecState(ogPrivList.GetStat("ARBEITSVERTRAGSARTENDLG.m_CDAT"));
	//------------------------------------
	m_LSTUD.SetBKColor(SILVER);
	m_LSTUD.SetInitText(pomCot->Lstu.Format("%d.%m.%Y"));
	m_LSTUD.SetSecState(ogPrivList.GetStat("ARBEITSVERTRAGSARTENDLG.m_LSTU"));
	// - - - - - - - - - - - - - - - - - -
	m_LSTUT.SetBKColor(SILVER);
	m_LSTUT.SetInitText(pomCot->Lstu.Format("%H:%M"));
	m_LSTUT.SetSecState(ogPrivList.GetStat("ARBEITSVERTRAGSARTENDLG.m_LSTU"));
	//------------------------------------
	m_USEC.SetBKColor(SILVER);
	m_USEC.SetInitText(pomCot->Usec);
	m_USEC.SetSecState(ogPrivList.GetStat("ARBEITSVERTRAGSARTENDLG.m_USEC"));
	//------------------------------------
	m_USEU.SetBKColor(SILVER);
	m_USEU.SetInitText(pomCot->Useu);
	m_USEU.SetSecState(ogPrivList.GetStat("ARBEITSVERTRAGSARTENDLG.m_USEU"));
	//------------------------------------//WHITE,YELLOW,SILVER,RED,LYELLOW
	m_CTRC.SetFormat("x|#x|#x|#x|#x|#");
	m_CTRC.SetTextLimit(1,5);
	m_CTRC.SetBKColor(YELLOW);
	m_CTRC.SetTextErrColor(RED);
	m_CTRC.SetInitText(pomCot->Ctrc);
	this->omOldCode = pomCot->Ctrc;
	m_CTRC.SetSecState(ogPrivList.GetStat("ARBEITSVERTRAGSARTENDLG.m_CTRC"));
	//------------------------------------
	m_CTRN.SetTypeToString("X(40)",40,1);
	m_CTRN.SetBKColor(YELLOW);
	m_CTRN.SetTextErrColor(RED);
	m_CTRN.SetInitText(pomCot->Ctrn);
	m_CTRN.SetSecState(ogPrivList.GetStat("ARBEITSVERTRAGSARTENDLG.m_CTRN"));
	//------------------------------------
	//m_DPTC.SetFormat("x|#x|#x|#x|#x|#x|#x|#x|#");
	m_DPTC.SetTextLimit(0,8);
	m_DPTC.SetTextErrColor(RED);
	m_DPTC.SetInitText(pomCot->Dptc);
	m_DPTC.SetSecState(ogPrivList.GetStat("ARBEITSVERTRAGSARTENDLG.m_DPTC"));
	//------------------------------------
	m_REMA.SetTypeToString("X(60)",60,0);
	m_REMA.SetTextErrColor(RED);
	m_REMA.SetInitText(pomCot->Rema);
	m_REMA.SetSecState(ogPrivList.GetStat("ARBEITSVERTRAGSARTENDLG.m_REMA"));
	//------------------------------------
//	m_WHPW.SetTypeToDouble(2,1);
	m_WHPW.SetTextLimit(1,5);
	m_WHPW.SetBKColor(YELLOW);
	m_WHPW.SetTextErrColor(RED);

	/*float flTmp = atof(pomCot->Whpw);
	double dlTmp = flTmp * 60;
	CString slTmp = "";
	slTmp.Format("%f",dlTmp);

	CString out;
	out = DBmin2Gui(slTmp);

	m_WHPW.SetInitText(out);*/
	
	CString olTmp = pomCot->Whpw;
	olTmp.Insert(2,":");
	m_WHPW.SetInitText(olTmp);
	m_WHPW.SetSecState(ogPrivList.GetStat("ARBEITSVERTRAGSARTENDLG.m_WHPW"));
	//------------------------------------
	
	if (pomCot->Inbu[0] =='x')
		m_Inbu.SetCheck(1);
	else
		m_Inbu.SetCheck(0);
	//------------------------------------

	return TRUE;  
}


//----------------------------------------------------------------------------------------

void ArbeitsvertragsartenDlg::OnOK() 
{
	AfxGetApp()->DoWaitCursor(1);
	CString olErrorText,olNewCode;
	bool ilStatus = true;

	if(m_CTRC.GetStatus() == false)
	{
		ilStatus = false;
		if(m_CTRC.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING237) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING237) + ogNotFormat;
		}
	}
	if(m_CTRN.GetStatus() == false)
	{
		ilStatus = false;
		if(m_CTRN.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING229) + ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING229) + ogNotFormat;
		}
	}
	if(m_DPTC.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING41) + ogNotFormat;
	}
	if(m_WHPW.GetStatus() == false)
	{
		ilStatus = false;
		if(m_WHPW.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING318) + ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING318) + ogNotFormat;
		}
	}
	if(m_REMA.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING238) + ogNotFormat;
	}
	///////////////////////////////////////////////////////////////////
	//Konsistenz-Check//
	if(ilStatus == true)
	{
		CString olDptc,olCtrc;
		char clWhere[100];
	
		if(m_DPTC.GetWindowTextLength() != 0)
		{
			CCSPtrArray<ORGDATA> olOrgCPA;
			m_DPTC.GetWindowText(olDptc);
			sprintf(clWhere,"WHERE DPT1='%s'",olDptc);
			if(ogOrgData.ReadSpecialData(&olOrgCPA,clWhere,"URNO",false) == false)
			{
				ilStatus = false;
				olErrorText += LoadStg(IDS_STRING320);
			}
			olOrgCPA.DeleteAll();
		}
		else
		{
			olDptc = " ";
		}

/*		//Gültigkeit Code + Org.Code
		if(m_CTRC.GetWindowTextLength() != 0)
		{
			CCSPtrArray<COTDATA> olData;

			CString olText;
			m_CTRC.GetWindowText(olCtrc);
			sprintf(clWhere,"WHERE CTRC='%s' AND DPTC = '%s'",olCtrc, olDptc);
			if(ogCotData.ReadSpecialData(&olData,clWhere,"URNO",false) == true)
			{
				if(pomCot->Urno != olData[0].Urno)
				{
					ilStatus = false;
					olErrorText += LoadStg(IDS_STRING616);
				}
			}
			olData.DeleteAll();
		}
*/
		//Gültigkeit nur Code
		if(m_CTRC.GetWindowTextLength() != 0)
		{
			CCSPtrArray<COTDATA> olData;

			CString olText;
			m_CTRC.GetWindowText(olCtrc);
			olNewCode = olCtrc;
			sprintf(clWhere,"WHERE CTRC='%s'",olCtrc);
			if(ogCotData.ReadSpecialData(&olData,clWhere,"URNO",false) == true)
			{
				if(pomCot->Urno != olData[0].Urno)
				{
					ilStatus = false;
					olErrorText += LoadStg(IDS_STRING237) + LoadStg(IDS_EXIST);
				}
			}
			olData.DeleteAll();
		}
		CString olText = "";
		m_WHPW.GetWindowText(olText);
		if ( atoi(olText.Right(2)) > 59 )
		{
			ilStatus = false;
			olErrorText += LoadStg(IDS_STRING318) + "> Wrong value for minute";
		}


	}
	///////////////////////////////////////////////////////////////////
	AfxGetApp()->DoWaitCursor(-1);
	if (ilStatus == true)
	{
		bool blChangeCode = true;
		if(olNewCode != omOldCode && bmChangeAction)
		{
			// Referenz Check
			CheckReferenz olCheckReferenz;
			if(pomStatus != NULL) pomStatus->SetWindowText(LoadStg(IDS_STRING120));
			int ilCount = olCheckReferenz.Check(_COT, pomCot->Urno, omOldCode);
			if(pomStatus != NULL) pomStatus->SetWindowText(LoadStg(ST_OK));
			CString olTxt;
			if(ilCount>0)
			{
				blChangeCode = false;
				if (!ogBasicData.BackDoorEnabled())
				{
					olTxt.Format("%d %s",ilCount, LoadStg(IDS_STRING119));
					MessageBox(olTxt,LoadStg(IDS_STRING145),MB_ICONERROR);
				}
				else
				{
					olTxt.Format("%d %s",ilCount, LoadStg(IDS_STRING983));
					if(IDYES == MessageBox(olTxt,LoadStg(IDS_STRING149),(MB_ICONSTOP | MB_YESNOCANCEL | MB_DEFBUTTON2)))
					{
						blChangeCode = true;
					}
				}
			}
			// END Referenz Check
		}

		if(blChangeCode)
		{
			CString olText = "";
			m_CTRC.GetWindowText(pomCot->Ctrc,6);
			m_CTRN.GetWindowText(pomCot->Ctrn,41);
			m_DPTC.GetWindowText(pomCot->Dptc,9);

			m_WHPW.GetWindowText(olText);
			olText = Gui2DBmin(olText);

			float flTmp = atof(olText);
			flTmp = flTmp / 60;
			olText.Format("%04.2f",flTmp);
			olText.Replace(".","");

			if (strlen(olText) == 3)
				olText = "0" + olText;

			strcpy(pomCot->Whpw, olText);

			if (m_Inbu.GetCheck() == 1)
				strcpy(pomCot->Inbu,"x");
			else
				strcpy(pomCot->Inbu," ");

			//wandelt Return in Blank//
			CString olTemp;
			m_REMA.GetWindowText(olTemp);
			for(int i=0;i>-1;)
			{
				i = olTemp.Find("\r\n");
				if(i != -1)
				{
					if(olTemp.GetLength() > (i+2))
						olTemp = olTemp.Left(i) + " " + olTemp.Mid(i+2);
					else
						olTemp = olTemp.Left(i);
				}
			}
			strcpy(pomCot->Rema,olTemp);
			////////////////////////////
			CDialog::OnOK();
		}
	}
	else
	{
		Beep(440,70);
		MessageBox(olErrorText,LoadStg(IDS_STRING145),MB_ICONERROR);
		m_CTRC.SetFocus();
	}	
}

//----------------------------------------------------------------------------------------

void ArbeitsvertragsartenDlg::OnBOrgAw() 
{
	CStringArray olCol1, olCol2, olCol3;
	CCSPtrArray<ORGDATA> olList;
	if(ogOrgData.ReadSpecialData(&olList, "order by DPT1", "URNO,DPT1,DPTN", false) == true)
	{
		for(int i = 0; i < olList.GetSize(); i++)
		{
			char pclUrno[20]="";
			sprintf(pclUrno, "%ld", olList[i].Urno);
			olCol3.Add(CString(pclUrno));
			olCol1.Add(CString(olList[i].Dpt1));
			olCol2.Add(CString(olList[i].Dptn));
		}
		AwDlg *pomDlg = new AwDlg(&olCol1, &olCol2, &olCol3, LoadStg(IDS_STRING237), LoadStg(IDS_STRING229), this);
		if(pomDlg->DoModal() == IDOK)
		{
			m_DPTC.SetInitText(pomDlg->omReturnString);
			m_DPTC.SetFocus();
		}
		delete pomDlg;
	}
	else
	{
		MessageBox(LoadStg(IDS_STRING321), LoadStg(IDS_STRING149), (MB_ICONSTOP|MB_OK));
	}
	olList.DeleteAll();
}

//----------------------------------------------------------------------------------------


// Umwandlung von 180 min aus DB 
// immer ins Format 03:00 (hh:mm) der Gui
CString ArbeitsvertragsartenDlg::DBmin2Gui(CString dbmin)
{
	int		ilTmp = 0;
	div_t	divTmp;   
	CString slOut = "";
	

	if (!dbmin.IsEmpty())
	{
		ilTmp = atoi(dbmin);
		divTmp = div( ilTmp, 60 );
		if (divTmp.quot<10)
		{
			if (divTmp.rem<10)
			{
				slOut.Format("0%d:0%d",divTmp.quot,divTmp.rem);
			}
			else
			{
				slOut.Format("0%d:%d",divTmp.quot,divTmp.rem);
			}
		}
		else
		{
			if (divTmp.rem<10)
			{
				slOut.Format("%d:0%d",divTmp.quot,divTmp.rem);
			}
			else
			{
				slOut.Format("%d:%d",divTmp.quot,divTmp.rem);
			}
		}
	}
	else
	{
		slOut ="00:00";
	}

	return slOut;
}

// Umwandlung von 03:00 / 3:00 / 0300 aus der Gui 
// immer ins Format 180 min der DB
CString ArbeitsvertragsartenDlg::Gui2DBmin(CString guimin)
{
	CString slOut	= "";
	int ilPos		= 0;
	int ilHour		= 0;
	int ilMin		= 0;
	
	ilPos = guimin.Find(":");
	if (ilPos > 0)
	{	// Format hh:mm oder h:mm
		slOut  = guimin.Left(ilPos);	
		ilHour = atoi(slOut);
		ilHour = ilHour * 60;
		slOut  = guimin.Right(guimin.GetLength()-(ilPos+1));
		ilMin  = atoi(slOut);
		ilMin  = ilHour + ilMin;
	}
	else
	{	// Format hhmm
		slOut  = guimin.Left(2);	
		ilHour = atoi(slOut);
		ilHour = ilHour * 60;
		slOut  = guimin.Right(2);	
		ilMin  = ilHour + ilMin;	
	}
	slOut.Format("%d",ilMin);

	return slOut;
}
