// CedaCHUData.cpp
 
#include <stdafx.h>
#include <CedaCHUData.h>
#include <resource.h>
#include <CedaBasicData.h>


// Local function prototype
static void ProcessCHUCf(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//--CEDADATA-----------------------------------------------------------------------------------------------

CedaCHUData::CedaCHUData() : CCSCedaData(&ogCommHandler)
{
    // Create an array of CEDARECINFO for CHUDATA
	BEGIN_CEDARECINFO(CHUDATA,CHUDATARecInfo)
		FIELD_LONG		(Urno,"URNO")
		FIELD_CHAR_TRIM (Cnam,"CNAM")
		FIELD_CHAR_TRIM (Term,"TERM")
		FIELD_CHAR_TRIM (Sort,"SORT")
		FIELD_CHAR_TRIM (Maxf,"MAXF")
		FIELD_CHAR_TRIM	(Hopo,"HOPO")
		FIELD_CHAR_TRIM	(Usec,"USEC")
		FIELD_CHAR_TRIM	(Useu,"USEU")
		FIELD_DATE		(Cdat,"CDAT")
		FIELD_DATE		(Lstu,"LSTU")
		FIELD_DATE	    (Vafr,"VAFR")
		FIELD_DATE	    (Vato,"VATO")
	END_CEDARECINFO //(CHUDATA)


	// Copy the record structure
	for (int i=0; i< sizeof(CHUDATARecInfo)/sizeof(CHUDATARecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&CHUDATARecInfo[i],sizeof(CEDARECINFO));
		omRecInfo.Add(prpCedaRecInfo);
	} // end for

	// initialize field names
	strcpy(pcmTableName,"CHU");	
	strcpy(pcmCHUFieldList,"URNO,CNAM,TERM,SORT,MAXF,HOPO,USEC,USEU,CDAT,LSTU,VAFR,VATO");
	pcmFieldList = pcmCHUFieldList;
	omData.SetSize(0,1000);
}

//---------------------------------------------------------------------------------------------------

void CedaCHUData::GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType)
{	
	ropFields.Add("URNO"); ropDesription.Add(LoadStg(IDS_STRING346));  ropType.Add("String");
	ropFields.Add("CNAM"); ropDesription.Add(LoadStg(IDS_STRING1153)); ropType.Add("String");
	ropFields.Add("TERM"); ropDesription.Add(LoadStg(IDS_STRING1154)); ropType.Add("String");
	ropFields.Add("SORT"); ropDesription.Add(LoadStg(IDS_STRING1155)); ropType.Add("String");
	ropFields.Add("MAXF"); ropDesription.Add(LoadStg(IDS_STRING1156)); ropType.Add("String");
	ropFields.Add("HOPO"); ropDesription.Add(LoadStg(IDS_STRING711));  ropType.Add("String");	
	ropFields.Add("USEC"); ropDesription.Add(LoadStg(IDS_STRING347));  ropType.Add("String");
	ropFields.Add("USEU"); ropDesription.Add(LoadStg(IDS_STRING348));  ropType.Add("String");	
	ropFields.Add("CDAT"); ropDesription.Add(LoadStg(IDS_STRING343));  ropType.Add("Date");
	ropFields.Add("LSTU"); ropDesription.Add(LoadStg(IDS_STRING344));  ropType.Add("Date");
	ropFields.Add("VAFR"); ropDesription.Add(LoadStg(IDS_STRING230));  ropType.Add("Date");
	ropFields.Add("VATO"); ropDesription.Add(LoadStg(IDS_STRING231));  ropType.Add("Date");
}

//-----------------------------------------------------------------------------------------------

void CedaCHUData::Register(void)
{
	ogDdx.Register((void *)this,BC_CHU_CHANGE,CString("CHUDATA"), CString("CHU-changed"),ProcessCHUCf);
	ogDdx.Register((void *)this,BC_CHU_DELETE,CString("CHUDATA"), CString("CHU-deleted"),ProcessCHUCf);	
}

//--~CEDADATA----------------------------------------------------------------------------------------------

CedaCHUData::~CedaCHUData(void)
{
	omRecInfo.DeleteAll();
	ClearAll();
}

//--CLEAR-ALL----------------------------------------------------------------------------------------------

void CedaCHUData::ClearAll(void)
{
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	ogDdx.UnRegister(this,NOTUSED);
}

//--READ-ALL-----------------------------------------------------------------------------------------------

bool CedaCHUData::Read(char *pcpWhere)
{
    // Select data from the database
	bool ilRc = true;
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(pcpWhere != NULL)
		ilRc = CedaAction(ogBasicData.GetCedaCommand("RT"), pcpWhere);
	else
		ilRc = CedaAction(ogBasicData.GetCedaCommand("RT"));
	if (ilRc != true)
	{
		return false;
	}
	// Load data from CedaData into the dynamic array of record
	for (int ilLc = 0; ilRc == true; ilLc++)
	{
		CHUDATA *prpCHU = new CHUDATA;
		if ((ilRc = GetFirstBufferRecord(prpCHU)) == true)
		{
			prpCHU->IsChanged = DATA_UNCHANGED;
			omData.Add(prpCHU);//Update omData
			omUrnoMap.SetAt((void *)prpCHU->Urno,prpCHU);
		}
		else
		{
			delete prpCHU;
		}
	}
    return true;
}

//--INSERT-------------------------------------------------------------------------------------------------

bool CedaCHUData::InsertCHU(CHUDATA *prpCHU,BOOL bpSendDdx)
{
	prpCHU->IsChanged = DATA_NEW;
	if (SaveCHU(prpCHU) == false) 
		return false; //Update Database
	InsertCHUInternal(prpCHU);
    return true;
}

//--INSERT-INTERNAL-------------------------------------------------------------------------------------------

bool CedaCHUData::InsertCHUInternal(CHUDATA *prpCHU)
{
	//PrepareCHUDATA(prpCHU);
	ogDdx.DataChanged((void *)this, CHU_CHANGE,(void *)prpCHU ); //Update Viewer
	omData.Add(prpCHU);//Update omData
	omUrnoMap.SetAt((void *)prpCHU->Urno,prpCHU);
    return true;
}

//--DELETE-------------------------------------------------------------------------------------------------

bool CedaCHUData::DeleteCHU(long lpUrno)
{
	CHUDATA *prlCHU = GetCHUByUrno(lpUrno);
	if (prlCHU != NULL)
	{
		prlCHU->IsChanged = DATA_DELETED;
		if(SaveCHU(prlCHU) == false) return false; //Update Database
		DeleteCHUInternal(prlCHU);
	}
    return true;
}

//--DELETE-INTERNAL----------------------------------------------------------------------------------------

bool CedaCHUData::DeleteCHUInternal(CHUDATA *prpCHU)
{
	ogDdx.DataChanged((void *)this,CHU_DELETE,(void *)prpCHU); //Update Viewer
	omUrnoMap.RemoveKey((void *)prpCHU->Urno);
	int ilCHUCount = omData.GetSize();
	for (int ilLc = 0; ilLc < ilCHUCount; ilLc++)
	{
		if (omData[ilLc].Urno == prpCHU->Urno)
		{
			omData.DeleteAt(ilLc);//Update omData
			break;
		}
	}
    return true;
}

//--PREPARE-DATA-------------------------------------------------------------------------------------------

void CedaCHUData::PrepareCHUData(CHUDATA *prpCHU)
{
	// TODO: add code here
}

//--UPDATE-------------------------------------------------------------------------------------------------

bool CedaCHUData::UpdateCHU(CHUDATA *prpCHU,BOOL bpSendDdx)
{
	if (GetCHUByUrno(prpCHU->Urno) != NULL)
	{
		if (prpCHU->IsChanged == DATA_UNCHANGED)
		{
			prpCHU->IsChanged = DATA_CHANGED;
		}
		if(SaveCHU(prpCHU) == false) return false; //Update Database
		UpdateCHUInternal(prpCHU);
	}
    return true;
}

//--UPDATE-INTERNAL--------------------------------------------------------------------------------------------

bool CedaCHUData::UpdateCHUInternal(CHUDATA *prpCHU)
{
	CHUDATA *prlCHU = GetCHUByUrno(prpCHU->Urno);
	if (prlCHU != NULL)
	{
		*prlCHU = *prpCHU; //Update omData
		ogDdx.DataChanged((void *)this,CHU_CHANGE,(void *)prlCHU); //Update Viewer
	}
    return true;
}

//--GET-BY-URNO--------------------------------------------------------------------------------------------

CHUDATA *CedaCHUData::GetCHUByUrno(long lpUrno)
{
	CHUDATA  *prlCHU;
	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlCHU) == TRUE)
	{
		return prlCHU;
	}
	return NULL;
	
}

//--READSPECIALDATA-------------------------------------------------------------------------------------

bool CedaCHUData::ReadSpecialData(CCSPtrArray<CHUDATA> *popCHUArray,char *pspWhere,char *pspFieldList,bool ipSYS/*=true*/)
{
	bool ilRc = true;
	char pclFieldList[256] = " ";

	if(strlen(pspFieldList) > 0) 
	{
		strcpy(pclFieldList, pspFieldList);
	}
	if(ipSYS == true) 
	{
		if (CedaAction("SYS","CHU",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	else
	{
		if (CedaAction("RT","CHU",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	if(popCHUArray != NULL)
	{
		for (int ilLc = 0; ilRc == true; ilLc++)
		{
			CHUDATA *prpCHU = new CHUDATA;
			if ((ilRc = GetBufferRecord(ilLc,prpCHU,CString(pclFieldList))) == true)
			{
				popCHUArray->Add(prpCHU);
			}
			else
			{
				delete prpCHU;
			}
		}
		if(popCHUArray->GetSize() == 0) return false;
	}
    return true;
}

//--SAVE---------------------------------------------------------------------------------------------------

bool CedaCHUData::SaveCHU(CHUDATA *prpCHU)
{
	bool ilRc = true;
	CString olListOfData;
	char pclSelection[124];
	char pclData[2048];

	if (prpCHU->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}

	switch(prpCHU->IsChanged)
	{
	case DATA_NEW:
		MakeCedaData(&omRecInfo,olListOfData,prpCHU);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("IRT","","",pclData);
		prpCHU->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpCHU->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpCHU);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("URT",pclSelection,"",pclData);
		prpCHU->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_DELETED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpCHU->Urno);
		ilRc = CedaAction("DRT",pclSelection);
		break;
	}

    return ilRc;
}

//--PROCESS-CF---------------------------------------------------------------------------------------------

void  ProcessCHUCf(void * popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	switch(ipDDXType)
	{
	case BC_CHU_CHANGE :
	case BC_CHU_DELETE :
		((CedaCHUData *)popInstance)->ProcessCHUBc(ipDDXType,vpDataPointer,ropInstanceName);
		break;
	}
}

//--PROCESS-BC---------------------------------------------------------------------------------------------

void  CedaCHUData::ProcessCHUBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlCHUData = (struct BcStruct *) vpDataPointer;
	long llUrno = GetUrnoFromSelection(prlCHUData->Selection);

	CHUDATA *prlCHU = GetCHUByUrno(llUrno);
	if(ipDDXType == BC_CHU_CHANGE)
	{
		if(prlCHU != NULL)
		{
			GetRecordFromItemList(prlCHU,prlCHUData->Fields,prlCHUData->Data);
			if(ValidateCHUBcData(prlCHU->Urno))
			{
				UpdateCHUInternal(prlCHU);
			}
			else
			{
				DeleteCHUInternal(prlCHU);
			}
		}
		else
		{
			prlCHU = new CHUDATA;
			GetRecordFromItemList(prlCHU,prlCHUData->Fields,prlCHUData->Data);
			if(ValidateCHUBcData(prlCHU->Urno))
			{
				InsertCHUInternal(prlCHU);
			}
			else
			{
				delete prlCHU;
			}
		}
	}
	if(ipDDXType == BC_CHU_DELETE)
	{
		if (prlCHU != NULL)
		{
			DeleteCHUInternal(prlCHU);
		}
	}
}

//--ValidateCHUBcData--------------------------------------------------------------------------------------

bool CedaCHUData::ValidateCHUBcData(const long& lrpUrno)
{
	bool blValidateCHUBcData = true;
	if(!omWhere.IsEmpty()) //It means it is not the default view and we need to check in the database, since we don't need to check for the default view
	{
		char chWhere[20];
		sprintf(chWhere, "URNO='%ld'",lrpUrno);
		CString olWhere = omWhere + CString(" AND ") + CString(chWhere);
		CCSPtrArray<CHUDATA> olCHUs;
		if(!ReadSpecialData(&olCHUs,olWhere.GetBuffer(0),"URNO",false))
		{		
			blValidateCHUBcData = false;
		}
	}
	return blValidateCHUBcData;
}
//---------------------------------------------------------------------------------------------------------
