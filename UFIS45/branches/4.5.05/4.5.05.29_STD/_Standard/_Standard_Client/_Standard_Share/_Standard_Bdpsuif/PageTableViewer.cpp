// PageTableViewer.cpp 
//

#include <stdafx.h>
#include <PageTableViewer.h>
#include <CcsDdx.h>
#include <resource.h>
#include <fstream.h>
#include <iomanip.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif


// Local function prototype
static void PageTableCf(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//-----------------------------------------------------------------------------------------------
// PageTableViewer
//

PageTableViewer::PageTableViewer(CCSPtrArray<PAGDATA> *popData)
{
	pomData = popData;

	bmIsFromSearch = FALSE;
    pomPageTable = NULL;
    ogDdx.Register(this, PAG_CHANGE, CString("PAGETABLEVIEWER"), CString("Page Update/new"), PageTableCf);
    ogDdx.Register(this, PAG_DELETE, CString("PAGETABLEVIEWER"), CString("Page Delete"), PageTableCf);
}

//-----------------------------------------------------------------------------------------------

PageTableViewer::~PageTableViewer()
{
	ogDdx.UnRegister(this, NOTUSED);
    DeleteAll();
	omHeaderDataArray.DeleteAll();
}

//-----------------------------------------------------------------------------------------------

void PageTableViewer::Attach(CCSTable *popTable)
{
    pomPageTable = popTable;
}

//-----------------------------------------------------------------------------------------------

void PageTableViewer::ChangeViewTo(const char *pcpViewName)
{
    DeleteAll();    
    MakeLines();
	UpdateDisplay();
}

//-----------------------------------------------------------------------------------------------

void PageTableViewer::MakeLines()
{
	int ilPageCount = pomData->GetSize();
	for (int ilLc = 0; ilLc < ilPageCount; ilLc++)
	{
		PAGDATA *prlPageData = &pomData->GetAt(ilLc);
		MakeLine(prlPageData);
	}
}

//-----------------------------------------------------------------------------------------------

void PageTableViewer::MakeLine(PAGDATA *prpPage)
{

    // Update viewer data for this shift record
    PAGETABLE_LINEDATA rlPage;

	rlPage.Urno = prpPage->Urno; 
	rlPage.Pagi = prpPage->Pagi; 
	rlPage.Pcfn = prpPage->Pcfn; 
	rlPage.Pdpt = prpPage->Pdpt; 
	rlPage.Pdti = prpPage->Pdti; 
	rlPage.Pnof = prpPage->Pnof; 
	rlPage.Pfnt = prpPage->Pfnt; 
	rlPage.Pdss = prpPage->Pdss; 
	rlPage.Pcti = prpPage->Pcti; 
	rlPage.Ptfb = prpPage->Ptfb; 
	rlPage.Ptfe = prpPage->Ptfe;
	rlPage.Ptd1 = prpPage->Ptd1;
	rlPage.Ptd2 = prpPage->Ptd2;
	rlPage.Prfn = prpPage->Prfn;
	rlPage.Ptdc = prpPage->Ptdc;
	rlPage.Cotb = prpPage->Cotb;
	
	if(ogPAGData.DoesRemarkCodeExist() == true)
	{
		rlPage.Rema = prpPage->Rema;
	}
	
	CreateLine(&rlPage);
}

//-----------------------------------------------------------------------------------------------

void PageTableViewer::CreateLine(PAGETABLE_LINEDATA *prpPage)
{
    int ilLineCount = omLines.GetSize();
    for (int ilLineno = ilLineCount; ilLineno > 0; ilLineno--)
        if (ComparePage(prpPage, &omLines[ilLineno-1]) >= 0)
	        break;							 // should be inserted after Lines[ilLineno-1]
	PAGETABLE_LINEDATA rlPage;
	rlPage = *prpPage;
    omLines.NewAt(ilLineno, rlPage);
}

//-----------------------------------------------------------------------------------------------
// UpdateDisplay: Load data selected by filter conditions to the display by using "omTable"

void PageTableViewer::UpdateDisplay()
{
	int ilLines = omLines.GetSize();
	const int ilColumns = 14;	//<===== Spaltenanzahl setzen (von 0 ab)
	omHeaderDataArray.DeleteAll();

	pomPageTable->SetShowSelection(TRUE);
	pomPageTable->ResetContent();

	TABLE_HEADER_COLUMN rlHeader;
	rlHeader.Alignment = COLALIGN_LEFT;
	rlHeader.Font = &ogCourier_Regular_10;

	int ilPos = 1;
	rlHeader.Length = 100; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING934),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);	// page name
	rlHeader.Length = 100; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING934),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);	// config file name
	rlHeader.Length = 10; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING934),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);	// Display type
	rlHeader.Length = 30; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING934),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);	// Internal display type
	rlHeader.Length = 60; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING934),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);	// # of flights / field
	rlHeader.Length = 30; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING934),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);	// flight nature
	rlHeader.Length = 30; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING934),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);	// display sequence
	rlHeader.Length = 50; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING934),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);	// carousel time
	rlHeader.Length = 50; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING934),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);	// time frame begin
	rlHeader.Length = 50; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING934),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);	// time frame end
	rlHeader.Length = 50; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING934),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);	// delete from display after TIFA/D
	rlHeader.Length = 50; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING934),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);	// delete from display after PRFN
	rlHeader.Length = 50; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING934),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);	// field name for PTD2
	rlHeader.Length = 50; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING934),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);	// delete cancelled flight after TIFA/D
	rlHeader.Length = 50; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING934),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);	// color table
	
	if(ogPAGData.DoesRemarkCodeExist() == true)
	{
		rlHeader.Length = 300; 
		rlHeader.Text = GetListItem(LoadStg(IDS_STRING934),ilPos++,true,'|');
		omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);	// Remarks
	}
	
	pomPageTable->SetHeaderFields(omHeaderDataArray);

	pomPageTable->SetDefaultSeparator();
	pomPageTable->SetTableEditable(FALSE);

	TABLE_COLUMN rlColumnData;
	rlColumnData.Font = &ogCourier_Regular_10;
	rlColumnData.BkColor = WHITE;
	rlColumnData.Alignment = COLALIGN_LEFT;
	
    for (int ilLineNo = 0; ilLineNo < ilLines; ilLineNo++)
	{
		CCSPtrArray<TABLE_COLUMN> olColList;

		rlColumnData.Text = omLines[ilLineNo].Pagi;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Pcfn;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Pdpt;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Pdti;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Pnof;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Pfnt;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Pdss;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Pcti;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Ptfb;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Ptfe;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Ptd1;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Ptd2;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Prfn;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Ptdc;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Cotb;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		
		if(ogPAGData.DoesRemarkCodeExist() == true)
		{
			rlColumnData.Text = omLines[ilLineNo].Rema;
			olColList.NewAt(olColList.GetSize(), rlColumnData);
		}
		
		pomPageTable->AddTextLine(olColList,(void*)NULL);
		olColList.DeleteAll();
	}
	pomPageTable->DisplayTable();
}

//-----------------------------------------------------------------------------------------------

CString PageTableViewer::Format(PAGETABLE_LINEDATA *prpLine)
{
    CString s;
	// ???
    return s;
}

//-----------------------------------------------------------------------------------------------

bool PageTableViewer::FindPage(char *pcpPage, int& ilItem)
{
	int ilCount = omLines.GetSize();
    for (ilItem = 0; ilItem < ilCount; ilItem++)
    {
/*		if ( (omLines[ilItem].Keya == pcpPageKeya) &&
			 (omLines[ilItem].Keyd == pcpPageKeyd)    )
			return TRUE;*/
	}
	return false;
}

//-----------------------------------------------------------------------------------------------

static void PageTableCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName)
{
    PageTableViewer *polViewer = (PageTableViewer *)popInstance;
    if (ipDDXType == PAG_CHANGE) 
		polViewer->ProcessPageChange((PAGDATA *)vpDataPointer);
    else if (ipDDXType == PAG_DELETE) 
		polViewer->ProcessPageDelete((PAGDATA *)vpDataPointer);
} 


//-----------------------------------------------------------------------------------------------

void PageTableViewer::ProcessPageChange(PAGDATA *prpPage)
{
	int ilItem = 0;
	int ilColumnNo = 0;
	TABLE_COLUMN rlColumn;
	CCSPtrArray <TABLE_COLUMN> olLine;

	rlColumn.Lineno = ilItem;
	rlColumn.VerticalSeparator = SEPA_NONE;
	rlColumn.SeparatorType = SEPA_NONE;
	rlColumn.BkColor = WHITE;
	rlColumn.Font = &ogCourier_Regular_10;
	rlColumn.Alignment = COLALIGN_LEFT;

	rlColumn.Text = prpPage->Pagi;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpPage->Pcfn;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpPage->Pdpt;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpPage->Pdti;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpPage->Pnof;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpPage->Pfnt;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpPage->Pdss;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpPage->Pcti;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpPage->Ptfb; 
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpPage->Ptfe; 
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);

	rlColumn.Text = prpPage->Ptd1;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);

	rlColumn.Text = prpPage->Ptd2;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpPage->Prfn;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpPage->Ptdc;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpPage->Cotb;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);

	if(ogPAGData.DoesRemarkCodeExist() == true)
	{
		rlColumn.Text = prpPage->Rema;
		rlColumn.Columnno = ilColumnNo++;
		olLine.NewAt(olLine.GetSize(), rlColumn);
	}

	if (FindLine(prpPage->Urno, ilItem))
	{
		DeleteLine(ilItem);
		pomPageTable->DeleteTextLine(ilItem);

		MakeLine(prpPage);
		if (FindLine(prpPage->Urno, ilItem))
		{
	        PAGETABLE_LINEDATA *prlLine = &omLines[ilItem];
			if(prlLine != NULL)
			{
				pomPageTable->AddTextLine(olLine, (void *)prlLine);
				pomPageTable->DisplayTable();
			}
		}
		else
		{
			pomPageTable->DisplayTable();
		}
	}
	else
	{
		MakeLine(prpPage);
		if (FindLine(prpPage->Urno, ilItem))
		{
	        PAGETABLE_LINEDATA *prlLine = &omLines[ilItem];
			if(prlLine != NULL)
			{
				pomPageTable->AddTextLine(olLine, (void *)prlLine);
				pomPageTable->DisplayTable();
			}
		}
	}
	olLine.DeleteAll();
}

//-----------------------------------------------------------------------------------------------

void PageTableViewer::ProcessPageDelete(PAGDATA *prpPage)
{
	int ilItem;
	if (FindLine(prpPage->Urno, ilItem))
	{
		DeleteLine(ilItem);
		pomPageTable->DeleteTextLine(ilItem);
		pomPageTable->DisplayTable();
	}
}

//-----------------------------------------------------------------------------------------------

bool PageTableViewer::IsPassFilter(PAGDATA *prpPage)
{

	return true;
}

//-----------------------------------------------------------------------------------------------

int PageTableViewer::ComparePage(PAGETABLE_LINEDATA *prpPage1, PAGETABLE_LINEDATA *prpPage2)
{
	return 0;
}

//-----------------------------------------------------------------------------------------------

void PageTableViewer::DeleteLine(int ipLineno)
{
	omLines.DeleteAt(ipLineno);
}

//-----------------------------------------------------------------------------------------------

bool PageTableViewer::FindLine(long lpUrno, int &rilLineno)
{
    for (rilLineno = 0; rilLineno < omLines.GetSize(); rilLineno++)
	{
        if (omLines[rilLineno].Urno == lpUrno)
            return true;
	}
    return false;
}
//-----------------------------------------------------------------------------------------------

void PageTableViewer::DeleteAll()
{
    while (omLines.GetSize() > 0)
        DeleteLine(0);
}

//-----------------------------------------------------------------------------------------------
// Printing Routines
//-----------------------------------------------------------------------------------------------

void PageTableViewer::PrintTableView()
{
	CString olFooter1,olFooter2;
	omTableName = LoadStg(IDS_STRING931);
	int ilOrientation = PRINT_LANDSCAPE;//  PRINT_PORTRAET  PRINT_LANDSCAPE
	pomPrint = new CCSPrint((CWnd*)this,ilOrientation,45);// FirstLine(P=200,L=200),LeftOffset(P=200,L=50)
	//pomPrint->imMaxLines = 38;  // (P=57,L=38)
	//omBitmap.LoadBitmap(IDB_HAJLOGO);
	//pomPrint->SetBitmaps(&omBitmap,&omBitmap);
	if (pomPrint != NULL)
	{
		if (pomPrint->InitializePrinter(ilOrientation) == TRUE)
		{
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			DOCINFO	rlDocInfo;
			memset(&rlDocInfo, 0, sizeof(DOCINFO));
			rlDocInfo.cbSize = sizeof( DOCINFO );
			rlDocInfo.lpszDocName = omTableName;	

//			calculate count of pages
			CUIntArray olColsPerPage;
			int ilPages = pomPrint->CalculateCountOfPages(this->omHeaderDataArray,olColsPerPage);

			pomPrint->omCdc.StartDoc( &rlDocInfo );
			for (int ilPage = 0; ilPage < ilPages; ilPage++)
			{
				pomPrint->imPageNo = 0;
				int ilLines = omLines.GetSize();
				pomPrint->imLineNo = pomPrint->imMaxLines + 1;
				olFooter1.Format("%d %s",ilLines,omTableName);
				for(int i = 0; i < ilLines; i++ ) 
				{
					if(pomPrint->imLineNo >= pomPrint->imMaxLines)
					{
						if(pomPrint->imPageNo > 0)
						{
							olFooter2.Format("%s: %d",LoadStg(IDS_STRING222),pomPrint->imPageNo);
							pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
							pomPrint->omCdc.EndPage();
						}
						PrintTableHeader(ilPage,olColsPerPage);
					}
					if(pomPrint->imLineNo == (pomPrint->imMaxLines-1) || i == (ilLines-1))
					{
						PrintTableLine(ilPage,olColsPerPage,&omLines[i],true);
					}
					else
					{
						PrintTableLine(ilPage,olColsPerPage,&omLines[i],false);
					}
				}
				olFooter2.Format("%s: %d",LoadStg(IDS_STRING222),pomPrint->imPageNo);
				pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
				pomPrint->omCdc.EndPage();
			}
			pomPrint->omCdc.EndDoc();
		}
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	delete pomPrint;
	//omBitmap.DeleteObject();
	pomPrint = NULL;
	}
}

//-----------------------------------------------------------------------------------------------

bool PageTableViewer::PrintTableHeader(int ilPage,CUIntArray& ropColsPerPage)
{
	pomPrint->omCdc.StartPage();
	pomPrint->imPageNo++;
	pomPrint->imLineNo = 0;

	pomPrint->PrintUIFHeader(omTableName,CString(CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")),pomPrint->imFirstLine-10);

	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;

	rlElement.Alignment   = PRINT_LEFT;
	rlElement.FrameLeft   = PRINT_NOFRAME;
	rlElement.FrameRight  = PRINT_NOFRAME;
	rlElement.FrameTop    = PRINT_NOFRAME;
	rlElement.FrameBottom = PRINT_FRAMEMEDIUM;
	rlElement.pFont       = &pomPrint->ogCourierNew_Bold_8;

	int ilSize = omHeaderDataArray.GetSize();
	for(int i=0;i<ilSize;i++)
	{
		bool blColumn = false;
		if (ilPage == 0)
		{
			if (i < ropColsPerPage[ilPage])
			{
				blColumn = true;
			}
		}
		else
		{
			if (i >= ropColsPerPage[ilPage-1] && i < ropColsPerPage[ilPage])
			{
				blColumn = true;
			}
		}

		if (blColumn)
		{
			rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
			if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
			rlElement.Text   = omHeaderDataArray[i].Text;
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		}
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();


	return true;
}

//-----------------------------------------------------------------------------------------------

bool PageTableViewer::PrintTableLine(int ilPage,CUIntArray& ropColsPerPage,PAGETABLE_LINEDATA *prpLine,bool bpLastLine)
{
	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;
	rlElement.pFont      = &pomPrint->ogCourierNew_Regular_8;
	int ilSize = omHeaderDataArray.GetSize();
	for(int i=0;i<ilSize;i++)
	{
		bool blColumn = false;
		if (ilPage == 0)
		{
			if (i < ropColsPerPage[ilPage])
			{
				blColumn = true;
			}
		}
		else
		{
			if (i >= ropColsPerPage[ilPage-1] && i < ropColsPerPage[ilPage])
			{
				blColumn = true;
			}
		}

		if(blColumn)// <= setzen, wenn Spalten nicht angezeigt werden sollen 
		{
			rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
			if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
			rlElement.Alignment  = PRINT_LEFT;
			rlElement.FrameTop   = PRINT_FRAMETHIN;
			if(bpLastLine == true)
			{
				rlElement.FrameBottom = PRINT_FRAMETHIN;
			}
			else
			{
				rlElement.FrameBottom = PRINT_NOFRAME;
			}
			rlElement.FrameLeft  = PRINT_NOFRAME;
			rlElement.FrameRight = PRINT_NOFRAME;
			switch(i)
			{
			case 0:
				{
					rlElement.FrameLeft  = PRINT_FRAMETHIN;
					rlElement.Text		= prpLine->Pagi;
				}
				break;
			case 1:
				{
					rlElement.Text		= prpLine->Pcfn;
				}
				break;
			case 2:
				{
					rlElement.Text		= prpLine->Pdpt;
				}
				break;
			case 3:
				{
					rlElement.Text		= prpLine->Pdti;
				}
				break;
			case 4:
				{
					rlElement.Text		= prpLine->Pnof;
				}
				break;
			case 5:
				{
					rlElement.Text		= prpLine->Pfnt;
				}
				break;
			case 6:
				{
					rlElement.Text		= prpLine->Pdss;
				}
				break;
			case 7:
				{
					rlElement.Text		= prpLine->Pcti;
				}
				break;
			case 8:
				{
					rlElement.Text		= prpLine->Ptfb;
				}
				break;
			case 9:
				{
					rlElement.Text		= prpLine->Ptfe;
				}
				break;
			case 10:
				{
					rlElement.Text		= prpLine->Ptd1;
				}
				break;
			case 11:
				{
					rlElement.Text		= prpLine->Ptd2;
				}
				break;
			case 12:
				{
					rlElement.Text		= prpLine->Prfn;
				}
				break;
			case 13:
				{
					rlElement.Text		= prpLine->Ptdc;
				}
				break;
			case 14:
				{
					if(ogPAGData.DoesRemarkCodeExist() == false)
					{
					rlElement.FrameRight= PRINT_FRAMETHIN;
					}
					rlElement.Text		= prpLine->Cotb;
				}
				break;
			case 15:
				{
					if(ogPAGData.DoesRemarkCodeExist() == true)
					{
						rlElement.FrameRight = PRINT_FRAMETHIN;
						rlElement.Text = prpLine->Rema;
					}
				}
				break;
			}
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		}
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();
	return true;
}

//-----------------------------------------------------------------------------------------------
bool PageTableViewer::CreateExcelFile(const CString& opTrenner,const CString& ropListSeparator)
{
	ofstream of;

	CString olTmpPath = CCSLog::GetTmpPath();

	CString olTableName;
	olTableName.Format("%s -   %s", "PAGTAB", (CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")) );


	CString olFileName = olTableName;
	olFileName.Remove('*');
	olFileName.Remove('.');
	olFileName.Remove(':');
	olFileName.Remove('/');
	olFileName.Replace(" ", "_");
	
	char pHeader[256];
	strcpy (pHeader, olTmpPath);
	CString path = pHeader;
	omFileName =  path + "\\" + olFileName + ".csv";

	of.open( omFileName, ios::out);


	of  << setw(0) << "PAGTAB" << "     "  << CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")  << endl;

	of	<< setw(0) << LoadStg(IDS_STRING964) //CString("Page name");
		<< setw(0) << opTrenner
		<< setw(0) << LoadStg(IDS_STRING965) //CString("Configuration");
		<< setw(0) << opTrenner
		<< setw(0) << LoadStg(IDS_STRING966) //CString("Display type");
		<< setw(0) << opTrenner
		<< setw(0) << LoadStg(IDS_STRING967) //CString("Display type internal");
		<< setw(0) << opTrenner
		<< setw(0) << LoadStg(IDS_STRING968) //CString("No of flights / field");
		<< setw(0) << opTrenner
		<< setw(0) << LoadStg(IDS_STRING969) //CString("Flight nature");
		<< setw(0) << opTrenner
		<< setw(0) << LoadStg(IDS_STRING970) //CString("Display sequence");
		<< setw(0) << opTrenner
		<< setw(0) << LoadStg(IDS_STRING971) //CString("Page caroousel time");
		<< setw(0) << opTrenner
		<< setw(0) << LoadStg(IDS_STRING972) //CString("Time frame begin");
		<< setw(0) << opTrenner
		<< setw(0) << LoadStg(IDS_STRING973) //CString("Time frame end");
		<< setw(0) << opTrenner
		<< setw(0) << LoadStg(IDS_STRING974) //CString("Delete from display after TIFA/D");
		<< setw(0) << opTrenner
		<< setw(0) << LoadStg(IDS_STRING975) //CString("Delete from display after PRFN");
		<< setw(0) << opTrenner
		<< setw(0) << LoadStg(IDS_STRING976) //CString("Field name for PTD2");
		<< setw(0) << opTrenner
		<< setw(0) << LoadStg(IDS_STRING977) //CString("Delete cancelled flight after TIFA/D");
		<< setw(0) << opTrenner
		<< setw(0) << LoadStg(IDS_STRING1020)//CString("Color table");
		<< endl;


	int ilCount = omLines.GetSize();
	COleDateTime olCurrTime = COleDateTime::GetCurrentTime();

	for(int i = 0; i < ilCount; i++)
	{
		PAGETABLE_LINEDATA rlLine = omLines[i];
		of.setf(ios::left, ios::adjustfield);

		of   << setw(0) << rlLine.Pagi							// Name
		     << setw(0) << opTrenner 
		     << setw(0) << rlLine.Pcfn							// Vorname
		     << setw(0) << opTrenner 
		     << setw(0) << rlLine.Pdpt							// Personal Nummer
		     << setw(0) << opTrenner 
			 << setw(0) << rlLine.Pdti							// Organisationseinheit
		     << setw(0) << opTrenner 
			 << setw(0) << rlLine.Pnof							// Funktionen
		     << setw(0) << opTrenner 
			 << setw(0) << rlLine.Pfnt							// Qualifikationen
		     << setw(0) << opTrenner 
			 << setw(0) << rlLine.Pdss							// Abk�rzung
		     << setw(0) << opTrenner 
			 << setw(0) << rlLine.Pcti							// Initialien
		     << setw(0) << opTrenner 
			 << setw(0) << rlLine.Ptfb							// Telephon B�ro
		     << setw(0) << opTrenner 
			 << setw(0) << rlLine.Ptfe							// Telephon mobil
		     << setw(0) << opTrenner 
			 << setw(0) << rlLine.Ptd1							// Telephon privat
		     << setw(0) << opTrenner 
			 << setw(0) << rlLine.Ptd2							// Gender Ind
		     << setw(0) << opTrenner 
			 << setw(0) << rlLine.Prfn							// Pdgl
		     << setw(0) << opTrenner 
			 << setw(0) << rlLine.Ptdc							// Pdkl
		     << setw(0) << opTrenner 
			 << setw(0) << rlLine.Cotb;							// Cotb

		if(ogPAGData.DoesRemarkCodeExist() == true)
		{
			of << setw(0) << opTrenner
			   << setw(0) << rlLine.Rema;
		}
		 
		of << endl; 
		
	}

	of.close();
	return true;
}

//-----------------------------------------------------------------------------------------------
