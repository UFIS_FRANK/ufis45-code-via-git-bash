// CedaScoData.cpp
 
#include <stdafx.h>
#include <CedaScoData.h>
#include <resource.h>


void ProcessScoCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName);


//--CEDADATA-----------------------------------------------------------------------------------------------

CedaScoData::CedaScoData() : CCSCedaData(&ogCommHandler)
{
	// Create an array of CEDARECINFO for SCODataStruct
	BEGIN_CEDARECINFO(SCODATA,SCODataRecInfo)
		FIELD_LONG		(Urno,"URNO")
		FIELD_LONG		(Surn,"SURN")
		FIELD_CHAR_TRIM	(Code,"CODE")
		FIELD_OLEDATE	(Vpfr,"VPFR")
		FIELD_OLEDATE	(Vpto,"VPTO")
		FIELD_CHAR_TRIM	(Kost,"KOST")
		FIELD_CHAR_TRIM	(Cweh,"CWEH")
		FIELD_CHAR_TRIM	(Kstn,"KSTN")
	END_CEDARECINFO //(SCODataStruct)

	// FIELD_LONG, FIELD_DATE 
	// Copy the record structure
	for (int i=0; i< sizeof(SCODataRecInfo)/sizeof(SCODataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&SCODataRecInfo[i],sizeof(CEDARECINFO));
		omRecInfo.Add(prpCedaRecInfo);
	} // end for

	// initialize field names
	strcpy(pcmTableName,"SCO");
	strcpy(pcmListOfFields,"URNO,SURN,CODE,VPFR,VPTO,KOST,CWEH,KSTN");
	pcmFieldList = pcmListOfFields; //because in CedaData pcmFieldList is just a pointer

	omData.RemoveAll();
}

//---------------------------------------------------------------------------------------------------------

void CedaScoData::GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType)
{
	ropFields.Add("URNO");
	ropFields.Add("SURN");
	ropFields.Add("CODE");
	ropFields.Add("VPFR");
	ropFields.Add("VPTO");
	ropFields.Add("KOST");
	ropFields.Add("CWEH");

	ropDesription.Add(LoadStg(IDS_STRING346));
	ropDesription.Add(LoadStg(IDS_STRING464));
	ropDesription.Add(LoadStg(IDS_STRING465));
	ropDesription.Add(LoadStg(IDS_STRING230));
	ropDesription.Add(LoadStg(IDS_STRING231));
	ropDesription.Add(LoadStg(IDS_STRING397));
	ropDesription.Add(LoadStg(IDS_STRING318));

	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("Date");
	ropType.Add("Date");
	ropType.Add("String");
	ropType.Add("String");
}

//--REGISTER----------------------------------------------------------------------------------------------

void CedaScoData::Register(void)
{
	ogDdx.Register((void *)this,BC_SCO_CHANGE,	CString("SCODATA"), CString("Sco-changed"),	ProcessScoCf);
	ogDdx.Register((void *)this,BC_SCO_NEW,		CString("SCODATA"), CString("Sco-new"),		ProcessScoCf);
	ogDdx.Register((void *)this,BC_SCO_DELETE,	CString("SCODATA"), CString("Sco-deleted"),	ProcessScoCf);
}

//--~CEDADATA----------------------------------------------------------------------------------------------

CedaScoData::~CedaScoData(void)
{
	omRecInfo.DeleteAll();
	ClearAll();
}

//--CLEAR-ALL----------------------------------------------------------------------------------------------

void CedaScoData::ClearAll(bool bpWithRegistration)
{
    omUrnoMap.RemoveAll();
    omStaffUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(bpWithRegistration)
	{
		ogDdx.UnRegister(this,NOTUSED);
	}
}

//--READ-ALL-----------------------------------------------------------------------------------------------

bool CedaScoData::Read(char *pspWhere /*NULL*/)
{
    // Select data from the database
	COleDateTime  olCurrTime = COleDateTime ::GetCurrentTime();
	bool ilRc = true;
    omUrnoMap.RemoveAll();
    omStaffUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(pspWhere == NULL)
	{	
		ilRc = CedaAction("RT");
	}
	else
	{
		ilRc = CedaAction("RT", pspWhere);
	}
	if (ilRc != true)
	{
		return ilRc;
	}
	// Load data from CedaData into the dynamic array of record
	for (int ilLc = 0; ilRc == true; ilLc++)
	{
		SCODATA *prlSco = new SCODATA;
		if ((ilRc = GetFirstBufferRecord(prlSco)) == true)
		{
			omData.Add(prlSco);//Update omData
			omUrnoMap.SetAt((void *)prlSco->Urno,prlSco);
			bool blVptoIsOk = true;
			if(prlSco->Vpto.GetStatus() == COleDateTime::valid)
			{
				if(prlSco->Vpto < olCurrTime)
				{
					blVptoIsOk = false;
				}
			}

			if((prlSco->Vpfr < olCurrTime ) && blVptoIsOk)
			{
				omStaffUrnoMap.SetAt((void *)prlSco->Surn,prlSco);
			}

		}
		else
		{
			delete prlSco;
		}
	}
    return true;
}

//--INSERT-------------------------------------------------------------------------------------------------

bool CedaScoData::Insert(SCODATA *prpSco)
{
	prpSco->IsChanged = DATA_NEW;
	if(Save(prpSco) == false) return false; //Update Database
	InsertInternal(prpSco);
    return true;
}

//--INSERT-INTERNAL--------------------------------------------------------------------------------------

bool CedaScoData::InsertInternal(SCODATA *prpSco)
{
	COleDateTime  olCurrTime = COleDateTime ::GetCurrentTime();
	ogDdx.DataChanged((void *)this, SCO_NEW,(void *)prpSco ); //Update Viewer
	omData.Add(prpSco);//Update omData
	omUrnoMap.SetAt((void *)prpSco->Urno,prpSco);
	bool blVptoIsOk = true;
	if(prpSco->Vpto.GetStatus() == COleDateTime::valid)
	{
		if(prpSco->Vpto < olCurrTime)
		{
			blVptoIsOk = false;
		}
	}

	if((prpSco->Vpfr < olCurrTime ) && blVptoIsOk)
	{
		omStaffUrnoMap.SetAt((void *)prpSco->Surn,prpSco);
	}

    return true;
}

//--DELETE-------------------------------------------------------------------------------------------------

bool CedaScoData::Delete(long lpUrno)
{
	SCODATA *prlSco = GetScoByUrno(lpUrno);
	if (prlSco != NULL)
	{
		prlSco->IsChanged = DATA_DELETED;
		if(Save(prlSco) == false) return false; //Update Database
		DeleteInternal(prlSco);
	}
    return true;
}

//--DELETE-INTERNAL----------------------------------------------------------------------------------------

bool CedaScoData::DeleteInternal(SCODATA *prpSco)
{
	COleDateTime  olCurrTime = COleDateTime ::GetCurrentTime();

	bool blVptoIsOk = true;
	if(prpSco->Vpto.GetStatus() == COleDateTime::valid)
	{
		if(prpSco->Vpto < olCurrTime)
		{
			blVptoIsOk = false;
		}
	}

	if((prpSco->Vpfr < olCurrTime ) && blVptoIsOk)
	{
		omStaffUrnoMap.RemoveKey((void *)prpSco->Surn);
		int ilScoCount = omData.GetSize();
		for (int ilLc = 0; ilLc < ilScoCount; ilLc++)
		{
			if (omData[ilLc].Urno == prpSco->Urno)
			{
				continue;
			}
			else
			{
				bool blVptoIsOk = true;
				if (omData[ilLc].Vpto.GetStatus() == COleDateTime::valid)
				{
					if(omData[ilLc].Vpto < olCurrTime)
					{
						blVptoIsOk = false;
					}
				}

				if ((omData[ilLc].Vpfr < olCurrTime ) && blVptoIsOk)
				{
					omStaffUrnoMap.SetAt((void *)omData[ilLc].Surn,&omData[ilLc]);
				}

			}
		}
	}

	ogDdx.DataChanged((void *)this,SCO_DELETE,(void *)prpSco); //Update Viewer
	omUrnoMap.RemoveKey((void *)prpSco->Urno);

	int ilScoCount = omData.GetSize();
	for (int ilLc = 0; ilLc < ilScoCount; ilLc++)
	{
		if (omData[ilLc].Urno == prpSco->Urno)
		{
			omData.DeleteAt(ilLc);//Update omData
			break;
		}
	}
    return true;
}

//--UPDATE-------------------------------------------------------------------------------------------------

bool CedaScoData::Update(SCODATA *prpSco)
{
	if (GetScoByUrno(prpSco->Urno) != NULL)
	{
		if (prpSco->IsChanged == DATA_UNCHANGED)
		{
			prpSco->IsChanged = DATA_CHANGED;
		}
		if(Save(prpSco) == false) return false; //Update Database
		UpdateInternal(prpSco);
	}
    return true;
}

//--UPDATE-INTERNAL--------------------------------------------------------------------------------------------

bool CedaScoData::UpdateInternal(SCODATA *prpSco)
{
	COleDateTime  olCurrTime = COleDateTime ::GetCurrentTime();

	SCODATA *prlSco = GetScoByUrno(prpSco->Urno);
	if (prlSco != NULL)
	{
		*prlSco = *prpSco; //Update omData

		bool blVptoIsOk = true;
		if(prlSco->Vpto.GetStatus() == COleDateTime::valid)
		{
			if(prlSco->Vpto < olCurrTime)
			{
				blVptoIsOk = false;
			}
		}

		if((prlSco->Vpfr < olCurrTime ) && blVptoIsOk)
		{
			omStaffUrnoMap.SetAt((void *)prlSco->Surn,prlSco);
		}


		ogDdx.DataChanged((void *)this,SCO_CHANGE,(void *)prlSco); //Update Viewer
	}
    return true;
}

//--GET-BY-URNO--------------------------------------------------------------------------------------------

SCODATA *CedaScoData::GetScoByUrno(long lpUrno)
{
	SCODATA  *prlSco;
	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlSco) == TRUE)
	{
		return prlSco;
	}
	return NULL;
}

//--GET-BY-SURN--------------------------------------------------------------------------------------------

SCODATA *CedaScoData::GetValidScoBySurn(long lpSurn)
{
	SCODATA  *prlSco;
	if (omStaffUrnoMap.Lookup((void *)lpSurn,(void *& )prlSco) == TRUE)
	{
		return prlSco;
	}
	return NULL;
}

//--GET-ALL BY-SURN--------------------------------------------------------------------------------------------

void CedaScoData::GetAllScoBySurn(long lpSurn,CCSPtrArray<SCODATA> &ropList)
{
	int ilScoCount = omData.GetSize();
	ropList.RemoveAll();
	for (int ilLc = 0; ilLc < ilScoCount; ilLc++)
	{
		if (omData[ilLc].Surn == lpSurn)
		{
			ropList.Add(&omData[ilLc]);
		}
	}
}


//--READSCOCIALDATA-------------------------------------------------------------------------------------

bool CedaScoData::ReadScocialData(CCSPtrArray<SCODATA> *popSco,char *pspWhere,char *pspFieldList,bool ipSYS/*=true*/)
{
	bool ilRc = true;
	char pclFieldList[256] = " ";

	if(strlen(pspFieldList) > 0) 
	{
		strcpy(pclFieldList, pspFieldList);
	}
	if(ipSYS == true) 
	{
		if (CedaAction("SYS","SCO",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	else
	{
		if (CedaAction("RT","SCO",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	if(popSco != NULL)
	{
		for (int ilLc = 0; ilRc == true; ilLc++)
		{
			SCODATA *prpSco = new SCODATA;
			if ((ilRc = GetBufferRecord(ilLc,prpSco,CString(pclFieldList))) == true)
			{
				popSco->Add(prpSco);
			}
			else
			{
				delete prpSco;
			}
		}
		if(popSco->GetSize() == 0) return false;
	}
    return true;
}

//--SAVE---------------------------------------------------------------------------------------------------

bool CedaScoData::Save(SCODATA *prpSco)
{
	bool ilRc = true;
	CString olListOfData;
	char pclSelection[124];
	char pclData[524];

	if (prpSco->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}
	switch(prpSco->IsChanged)
	{
	case DATA_NEW:
		MakeCedaData(&omRecInfo,olListOfData,prpSco);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("IRT","","",pclData);
		prpSco->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpSco->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpSco);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("URT",pclSelection,"",pclData);
		prpSco->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_DELETED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpSco->Urno);
		ilRc = CedaAction("DRT",pclSelection);
		break;
	}
    return ilRc;
}

//--PROCESS-CF---------------------------------------------------------------------------------------------

void  ProcessScoCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	ogScoData.ProcessBc(ipDDXType,vpDataPointer,ropInstanceName);
}

//--PROCESS-BC---------------------------------------------------------------------------------------------

void  CedaScoData::ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlScoData;
	prlScoData = (struct BcStruct *) vpDataPointer;
	SCODATA *prlSco;
	if(ipDDXType == BC_SCO_NEW)
	{
		prlSco = new SCODATA;
		GetRecordFromItemList(prlSco,prlScoData->Fields,prlScoData->Data);
		InsertInternal(prlSco);
	}
	if(ipDDXType == BC_SCO_CHANGE)
	{
		long llUrno = GetUrnoFromSelection(prlScoData->Selection);
		prlSco = GetScoByUrno(llUrno);
		if(prlSco != NULL)
		{
			GetRecordFromItemList(prlSco,prlScoData->Fields,prlScoData->Data);
			UpdateInternal(prlSco);
		}
	}
	if(ipDDXType == BC_SCO_DELETE)
	{
		long llUrno = GetUrnoFromSelection(prlScoData->Selection);
		prlSco = GetScoByUrno(llUrno);
		if (prlSco != NULL)
		{
			DeleteInternal(prlSco);
		}
	}
}

//---------------------------------------------------------------------------------------------------------
