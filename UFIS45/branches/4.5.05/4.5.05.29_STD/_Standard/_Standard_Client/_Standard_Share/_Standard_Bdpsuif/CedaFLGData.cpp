// CedaFLGData.cpp
 
#include <stdafx.h>
#include <CedaFLGData.h>
#include <CedaDPTData.h>
#include <resource.h>
#include <CedaBasicData.h>


// Local function prototype
static void ProcessFLGCf(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//--CEDADATA-----------------------------------------------------------------------------------------------

CedaFLGData::CedaFLGData() : CCSCedaData(&ogCommHandler)
{
    // Create an array of CEDARECINFO for FLGDATA
	BEGIN_CEDARECINFO(FLGDATA,FLGDATARecInfo)
		FIELD_LONG		(Urno,"URNO")		
		FIELD_CHAR_TRIM	(Hopo,"HOPO")		
		FIELD_CHAR_TRIM	(Usec,"USEC")
		FIELD_DATE		(Cdat,"CDAT")
		FIELD_CHAR_TRIM	(Useu,"USEU")
		FIELD_DATE		(Lstu,"LSTU")
		FIELD_CHAR_TRIM	(Lgfn,"LGFN")
		FIELD_CHAR_TRIM (Lgsn,"LGSN")
		FIELD_CHAR_TRIM (Type,"TYPE")
		FIELD_CHAR_TRIM (Alc2,"ALC2")
		FIELD_CHAR_TRIM (Alc3,"ALC3")
	END_CEDARECINFO //(FLGDATA)


	// Copy the record structure
	for (int i=0; i< sizeof(FLGDATARecInfo)/sizeof(FLGDATARecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&FLGDATARecInfo[i],sizeof(CEDARECINFO));
		omRecInfo.Add(prpCedaRecInfo);
	} // end for

	// initialize field names
	strcpy(pcmTableName,"FLG");	
	strcpy(pcmFLGFieldList,"URNO,HOPO,USEC,CDAT,USEU,LSTU,LGFN,LGSN,TYPE,ALC2,ALC3");
	pcmFieldList = pcmFLGFieldList;
	omData.SetSize(0,1000);	
}

//---------------------------------------------------------------------------------------------------

void CedaFLGData::GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType)
{	
	ropFields.Add("LGSN"); ropDesription.Add(LoadStg(IDS_STRING1142)); ropType.Add("String");
	ropFields.Add("LGFN"); ropDesription.Add(LoadStg(IDS_STRING1141)); ropType.Add("String");
	ropFields.Add("TYPE"); ropDesription.Add(LoadStg(IDS_STRING1143)); ropType.Add("String");
	ropFields.Add("ALC2"); ropDesription.Add(LoadStg(IDS_STRING1144)); ropType.Add("String");
	ropFields.Add("ALC3"); ropDesription.Add(LoadStg(IDS_STRING1145)); ropType.Add("String");
	ropFields.Add("HOPO"); ropDesription.Add(LoadStg(IDS_STRING711)); ropType.Add("String");
	ropFields.Add("USEC"); ropDesription.Add(LoadStg(IDS_STRING347)); ropType.Add("String");
	ropFields.Add("CDAT"); ropDesription.Add(LoadStg(IDS_STRING343)); ropType.Add("Date");
	ropFields.Add("USEU"); ropDesription.Add(LoadStg(IDS_STRING348)); ropType.Add("String");
	ropFields.Add("LSTU"); ropDesription.Add(LoadStg(IDS_STRING344)); ropType.Add("Date");
	ropFields.Add("URNO"); ropDesription.Add(LoadStg(IDS_STRING346)); ropType.Add("String");
}

//-----------------------------------------------------------------------------------------------

void CedaFLGData::Register(void)
{
	ogDdx.Register((void *)this,BC_FLG_CHANGE,CString("FLGDATA"), CString("FLG-changed"),ProcessFLGCf);
	ogDdx.Register((void *)this,BC_FLG_DELETE,CString("FLGDATA"), CString("FLG-deleted"),ProcessFLGCf);	
}

//--~CEDADATA----------------------------------------------------------------------------------------------

CedaFLGData::~CedaFLGData(void)
{
	omRecInfo.DeleteAll();
	ClearAll();
}

//--CLEAR-ALL----------------------------------------------------------------------------------------------

void CedaFLGData::ClearAll(void)
{
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	ogDdx.UnRegister(this,NOTUSED);
}

//--READ-ALL-----------------------------------------------------------------------------------------------

bool CedaFLGData::Read(char *pcpWhere)
{
    // Select data from the database
	bool ilRc = true;
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(pcpWhere != NULL)
		ilRc = CedaAction(ogBasicData.GetCedaCommand("RT"), pcpWhere);
	else
		ilRc = CedaAction(ogBasicData.GetCedaCommand("RT"));
	if (ilRc != true)
	{
		return false;
	}
	// Load data from CedaData into the dynamic array of record
	for (int ilLc = 0; ilRc == true; ilLc++)
	{
		FLGDATA *prpFLG = new FLGDATA;
		if ((ilRc = GetFirstBufferRecord(prpFLG)) == true)
		{
			prpFLG->IsChanged = DATA_UNCHANGED;
			omData.Add(prpFLG);//Update omData
			omUrnoMap.SetAt((void *)prpFLG->Urno,prpFLG);
		}
		else
		{
			delete prpFLG;
		}
	}
    return true;
}

//--INSERT-------------------------------------------------------------------------------------------------

bool CedaFLGData::InsertFLG(FLGDATA *prpFLG,BOOL bpSendDdx)
{
	prpFLG->IsChanged = DATA_NEW;
	if (SaveFLG(prpFLG) == false) 
		return false; //Update Database
	InsertFLGInternal(prpFLG);
    return true;
}

//--INSERT-INTERNAL-------------------------------------------------------------------------------------------

bool CedaFLGData::InsertFLGInternal(FLGDATA *prpFLG)
{	
	ogDdx.DataChanged((void *)this, FLG_CHANGE,(void *)prpFLG ); //Update Viewer
	omData.Add(prpFLG);//Update omData
	omUrnoMap.SetAt((void *)prpFLG->Urno,prpFLG);
    return true;
}

//--DELETE-------------------------------------------------------------------------------------------------

bool CedaFLGData::DeleteFLG(long lpUrno)
{
	FLGDATA *prlFLG = GetFLGByUrno(lpUrno);
	if (prlFLG != NULL)
	{
		prlFLG->IsChanged = DATA_DELETED;
		if(SaveFLG(prlFLG) == false) return false; //Update Database
		DeleteFLGInternal(prlFLG);

		char pclWhere[100];
		sprintf(pclWhere,"WHERE RURN = '%ld'",lpUrno);
		DPTDATA* polDPTData;
		CCSPtrArray<DPTDATA> olDPTDataArray;
		ogDPTData.ReadSpecialData(&olDPTDataArray,pclWhere,"URNO",false);
		for(int i = 0 ; i < olDPTDataArray.GetSize(); i++)
		{
			polDPTData = &olDPTDataArray[i];
			ogDPTData.DeleteDPT(polDPTData->Urno);
		}
	}
    return true;
}

//--DELETE-INTERNAL----------------------------------------------------------------------------------------

bool CedaFLGData::DeleteFLGInternal(FLGDATA *prpFLG)
{
	ogDdx.DataChanged((void *)this,FLG_DELETE,(void *)prpFLG); //Update Viewer
	omUrnoMap.RemoveKey((void *)prpFLG->Urno);
	int ilFLGCount = omData.GetSize();
	for (int ilLc = 0; ilLc < ilFLGCount; ilLc++)
	{
		if (omData[ilLc].Urno == prpFLG->Urno)
		{
			omData.DeleteAt(ilLc);//Update omData
			break;
		}
	}
    return true;
}

//--PREPARE-DATA-------------------------------------------------------------------------------------------

void CedaFLGData::PrepareFLGData(FLGDATA *prpFLG)
{
	// TODO: add code here
}

//--UPDATE-------------------------------------------------------------------------------------------------

bool CedaFLGData::UpdateFLG(FLGDATA *prpFLG,BOOL bpSendDdx)
{
	if (GetFLGByUrno(prpFLG->Urno) != NULL)
	{
		if (prpFLG->IsChanged == DATA_UNCHANGED)
		{
			prpFLG->IsChanged = DATA_CHANGED;
		}
		if(SaveFLG(prpFLG) == false) return false; //Update Database
		UpdateFLGInternal(prpFLG);
	}
    return true;
}

//--UPDATE-INTERNAL--------------------------------------------------------------------------------------------

bool CedaFLGData::UpdateFLGInternal(FLGDATA *prpFLG)
{
	FLGDATA *prlFLG = GetFLGByUrno(prpFLG->Urno);
	if (prlFLG != NULL)
	{
		*prlFLG = *prpFLG; //Update omData
		ogDdx.DataChanged((void *)this,FLG_CHANGE,(void *)prlFLG); //Update Viewer
	}
    return true;
}

//--GET-BY-URNO--------------------------------------------------------------------------------------------

FLGDATA *CedaFLGData::GetFLGByUrno(long lpUrno)
{
	FLGDATA  *prlFLG;
	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlFLG) == TRUE)
	{
		return prlFLG;
	}
	return NULL;
	
}

//--READSPECIALDATA-------------------------------------------------------------------------------------

bool CedaFLGData::ReadSpecialData(CCSPtrArray<FLGDATA> *popFLGDataArray,char *pspWhere,char *pspFieldList,bool ipSYS/*=true*/)
{
	bool ilRc = true;
	char pclFieldList[256] = " ";

	if(strlen(pspFieldList) > 0) 
	{
		strcpy(pclFieldList, pspFieldList);
	}
	if(ipSYS == true) 
	{
		if (CedaAction("SYS","FLG",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	else
	{
		if (CedaAction("RT","FLG",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	if(popFLGDataArray != NULL)
	{
		for (int ilLc = 0; ilRc == true; ilLc++)
		{
			FLGDATA *prpFLG = new FLGDATA;
			if ((ilRc = GetBufferRecord(ilLc,prpFLG,CString(pclFieldList))) == true)
			{
				popFLGDataArray->Add(prpFLG);
			}
			else
			{
				delete prpFLG;
			}
		}
		if(popFLGDataArray->GetSize() == 0) return false;
	}
    return true;
}

//--SAVE---------------------------------------------------------------------------------------------------

bool CedaFLGData::SaveFLG(FLGDATA *prpFLG)
{
	bool ilRc = true;
	CString olListOfData;
	char pclSelection[124];
	char pclData[2048];

	if (prpFLG->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}

	switch(prpFLG->IsChanged)
	{
	case DATA_NEW:
		MakeCedaData(&omRecInfo,olListOfData,prpFLG);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("IRT","","",pclData);
		prpFLG->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpFLG->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpFLG);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("URT",pclSelection,"",pclData);
		prpFLG->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_DELETED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpFLG->Urno);
		ilRc = CedaAction("DRT",pclSelection);
		break;
	}

    return ilRc;
}

//--PROCESS-CF---------------------------------------------------------------------------------------------

void  ProcessFLGCf(void * popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	switch(ipDDXType)
	{
	case BC_FLG_CHANGE :
	case BC_FLG_DELETE :
		((CedaFLGData *)popInstance)->ProcessFLGBc(ipDDXType,vpDataPointer,ropInstanceName);
		break;
	}
}

//--PROCESS-BC---------------------------------------------------------------------------------------------

void  CedaFLGData::ProcessFLGBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlFLGData = (struct BcStruct *) vpDataPointer;
	long llUrno = GetUrnoFromSelection(prlFLGData->Selection);

	FLGDATA *prlFLG = GetFLGByUrno(llUrno);
	if(ipDDXType == BC_FLG_CHANGE)
	{
		if(prlFLG != NULL)
		{
			GetRecordFromItemList(prlFLG,prlFLGData->Fields,prlFLGData->Data);
			if(ValidateFLGBcData(prlFLG->Urno))
			{
				UpdateFLGInternal(prlFLG);
			}
			else
			{
				DeleteFLGInternal(prlFLG);
			}
		}
		else
		{
			prlFLG = new FLGDATA;
			GetRecordFromItemList(prlFLG,prlFLGData->Fields,prlFLGData->Data);
			if(ValidateFLGBcData(prlFLG->Urno))
			{
				InsertFLGInternal(prlFLG);
			}
			else
			{
				delete prlFLG;
			}
		}
	}
	if(ipDDXType == BC_FLG_DELETE)
	{
		if (prlFLG != NULL)
		{
			DeleteFLGInternal(prlFLG);
		}
	}
}

//--ValidateFLGBcData--------------------------------------------------------------------------------------

bool CedaFLGData::ValidateFLGBcData(const long& lrpUrno)
{
	bool blValidateFLGBcData = true;
	if(!omWhere.IsEmpty()) //It means it is not the default view and we need to check in the database, since we don't need to check for the default view
	{
		char chWhere[20];
		sprintf(chWhere, "URNO='%ld'",lrpUrno);
		CString olWhere = omWhere + CString(" AND ") + CString(chWhere);
		CCSPtrArray<FLGDATA> olFLGs;
		if(!ReadSpecialData(&olFLGs,olWhere.GetBuffer(0),"URNO",false))
		{		
			blValidateFLGBcData = false;
		}
	}
	return blValidateFLGBcData;
}
//---------------------------------------------------------------------------------------------------------
