// CedaDSPData.h

#ifndef __CEDADSPDATA__
#define __CEDADSPDATA__
 
#include <stdafx.h>
#include <basicdata.h>

//---------------------------------------------------------------------------------------------------------
// Record structure declaration

struct DSPDATA 
{
	long 	 Urno; 		// Eindeutige Datensatz-Nr.
	CTime	 Cdat;		// Erstellungsdatum
	char 	 Usec[34]; 	// Anwender (Ersteller)
	CTime	 Lstu;	 	// Datum letzte �nderung
	char 	 Useu[34]; 	// Anwender (letzte �nderung)
	char 	 Dpid[14]; 	// Display name
	char 	 Dnop[4]; 	// Anzahl Seiten
	char 	 Dfpn[4]; 	// Nummer der ersten Seite
	char 	 Demt[4]; 	// Nummer der Standardseite
	char 	 Dapn[4]; 	// Nummer der Alarmseite
	char 	 Dicf[14]; 	// Init. config file

	char 	 Pi01[14]; 	// Referenz auf PAGTAB.PAGI
	char 	 Pn01[4]; 	// Seitennummer der Seite
	char 	 Pi02[14]; 	// Referenz auf PAGTAB.PAGI
	char 	 Pn02[4]; 	// Seitennummer der Seite
	char 	 Pi03[14]; 	// Referenz auf PAGTAB.PAGI
	char 	 Pn03[4]; 	// Seitennummer der Seite
	char 	 Pi04[14]; 	// Referenz auf PAGTAB.PAGI
	char 	 Pn04[4]; 	// Seitennummer der Seite
	char 	 Pi05[14]; 	// Referenz auf PAGTAB.PAGI
	char 	 Pn05[4]; 	// Seitennummer der Seite
	char 	 Pi06[14]; 	// Referenz auf PAGTAB.PAGI
	char 	 Pn06[4]; 	// Seitennummer der Seite
	char 	 Pi07[14]; 	// Referenz auf PAGTAB.PAGI
	char 	 Pn07[4]; 	// Seitennummer der Seite
	char 	 Pi08[14]; 	// Referenz auf PAGTAB.PAGI
	char 	 Pn08[4]; 	// Seitennummer der Seite
	char 	 Pi09[14]; 	// Referenz auf PAGTAB.PAGI
	char 	 Pn09[4]; 	// Seitennummer der Seite
	char 	 Pi10[14]; 	// Referenz auf PAGTAB.PAGI
	char 	 Pn10[4]; 	// Seitennummer der Seite

	char 	 Pi11[14]; 	// Referenz auf PAGTAB.PAGI
	char 	 Pn11[4]; 	// Seitennummer der Seite
	char 	 Pi12[14]; 	// Referenz auf PAGTAB.PAGI
	char 	 Pn12[4]; 	// Seitennummer der Seite
	char 	 Pi13[14]; 	// Referenz auf PAGTAB.PAGI
	char 	 Pn13[4]; 	// Seitennummer der Seite
	char 	 Pi14[14]; 	// Referenz auf PAGTAB.PAGI
	char 	 Pn14[4]; 	// Seitennummer der Seite
	char 	 Pi15[14]; 	// Referenz auf PAGTAB.PAGI
	char 	 Pn15[4]; 	// Seitennummer der Seite
	char 	 Pi16[14]; 	// Referenz auf PAGTAB.PAGI
	char 	 Pn16[4]; 	// Seitennummer der Seite
	char 	 Pi17[14]; 	// Referenz auf PAGTAB.PAGI
	char 	 Pn17[4]; 	// Seitennummer der Seite
	char 	 Pi18[14]; 	// Referenz auf PAGTAB.PAGI
	char 	 Pn18[4]; 	// Seitennummer der Seite
	char 	 Pi19[14]; 	// Referenz auf PAGTAB.PAGI
	char 	 Pn19[4]; 	// Seitennummer der Seite
	char 	 Pi20[14]; 	// Referenz auf PAGTAB.PAGI
	char 	 Pn20[4]; 	// Seitennummer der Seite

	char 	 Pi21[14]; 	// Referenz auf PAGTAB.PAGI
	char 	 Pn21[4]; 	// Seitennummer der Seite
	char 	 Pi22[14]; 	// Referenz auf PAGTAB.PAGI
	char 	 Pn22[4]; 	// Seitennummer der Seite
	char 	 Pi23[14]; 	// Referenz auf PAGTAB.PAGI
	char 	 Pn23[4]; 	// Seitennummer der Seite
	char 	 Pi24[14]; 	// Referenz auf PAGTAB.PAGI
	char 	 Pn24[4]; 	// Seitennummer der Seite
	char 	 Pi25[14]; 	// Referenz auf PAGTAB.PAGI
	char 	 Pn25[4]; 	// Seitennummer der Seite
	char 	 Pi26[14]; 	// Referenz auf PAGTAB.PAGI
	char 	 Pn26[4]; 	// Seitennummer der Seite
	char 	 Pi27[14]; 	// Referenz auf PAGTAB.PAGI
	char 	 Pn27[4]; 	// Seitennummer der Seite
	char 	 Pi28[14]; 	// Referenz auf PAGTAB.PAGI
	char 	 Pn28[4]; 	// Seitennummer der Seite
	char 	 Pi29[14]; 	// Referenz auf PAGTAB.PAGI
	char 	 Pn29[4]; 	// Seitennummer der Seite
	char 	 Pi30[14]; 	// Referenz auf PAGTAB.PAGI
	char 	 Pn30[4]; 	// Seitennummer der Seite

	char 	 Pi31[14]; 	// Referenz auf PAGTAB.PAGI
	char 	 Pn31[4]; 	// Seitennummer der Seite
	char 	 Pi32[14]; 	// Referenz auf PAGTAB.PAGI
	char 	 Pn32[4]; 	// Seitennummer der Seite
	char 	 Pi33[14]; 	// Referenz auf PAGTAB.PAGI
	char 	 Pn33[4]; 	// Seitennummer der Seite
	char 	 Pi34[14]; 	// Referenz auf PAGTAB.PAGI
	char 	 Pn34[4]; 	// Seitennummer der Seite
	char 	 Pi35[14]; 	// Referenz auf PAGTAB.PAGI
	char 	 Pn35[4]; 	// Seitennummer der Seite
	char 	 Pi36[14]; 	// Referenz auf PAGTAB.PAGI
	char 	 Pn36[4]; 	// Seitennummer der Seite
	char 	 Pi37[14]; 	// Referenz auf PAGTAB.PAGI
	char 	 Pn37[4]; 	// Seitennummer der Seite
	char 	 Pi38[14]; 	// Referenz auf PAGTAB.PAGI
	char 	 Pn38[4]; 	// Seitennummer der Seite
	char 	 Pi39[14]; 	// Referenz auf PAGTAB.PAGI
	char 	 Pn39[4]; 	// Seitennummer der Seite
	char 	 Pi40[14]; 	// Referenz auf PAGTAB.PAGI
	char 	 Pn40[4]; 	// Seitennummer der Seite

	char 	 Pi41[14]; 	// Referenz auf PAGTAB.PAGI
	char 	 Pn41[4]; 	// Seitennummer der Seite
	char 	 Pi42[14]; 	// Referenz auf PAGTAB.PAGI
	char 	 Pn42[4]; 	// Seitennummer der Seite
	char 	 Pi43[14]; 	// Referenz auf PAGTAB.PAGI
	char 	 Pn43[4]; 	// Seitennummer der Seite
	char 	 Pi44[14]; 	// Referenz auf PAGTAB.PAGI
	char 	 Pn44[4]; 	// Seitennummer der Seite
	char 	 Pi45[14]; 	// Referenz auf PAGTAB.PAGI
	char 	 Pn45[4]; 	// Seitennummer der Seite
	char 	 Pi46[14]; 	// Referenz auf PAGTAB.PAGI
	char 	 Pn46[4]; 	// Seitennummer der Seite
	char 	 Pi47[14]; 	// Referenz auf PAGTAB.PAGI
	char 	 Pn47[4]; 	// Seitennummer der Seite
	char 	 Pi48[14]; 	// Referenz auf PAGTAB.PAGI
	char 	 Pn48[4]; 	// Seitennummer der Seite
	char 	 Pi49[14]; 	// Referenz auf PAGTAB.PAGI
	char 	 Pn49[4]; 	// Seitennummer der Seite
	char 	 Pi50[14]; 	// Referenz auf PAGTAB.PAGI
	char 	 Pn50[4]; 	// Seitennummer der Seite
	char     Rema[513];  // Remarks

	CTime	 Vafr;	 	// G�ltig von
	CTime	 Vato;	 	// G�ltig bis

	//DataCreated by this class
	int		 IsChanged;

	DSPDATA(void)
	{ 
		memset(this,'\0',sizeof(*this));
		Cdat=-1;
		Lstu=-1;
		Vafr=-1;
		Vato=-1;
	}

}; // end DSPDataStrukt


//---------------------------------------------------------------------------------------------------------
// Class declaratino

class CedaDSPData: public CCSCedaData
{
// Attributes
public:
    CMapPtrToPtr omUrnoMap;

    CCSPtrArray<DSPDATA> omData;

// Operations
public:
    CedaDSPData();
	~CedaDSPData();

	virtual void GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType);
	void Register(void);
	void ClearAll(void);

    bool Read(char *pcpWhere = NULL);
	bool ReadSpecialData(CCSPtrArray<DSPDATA> *popAlt,char *pspWhere,char *pspFieldList,bool ipSYS = true);
	bool InsertDSP(DSPDATA *prpDSP,BOOL bpSendDdx = TRUE);
	bool InsertDSPInternal(DSPDATA *prpDSP);
	bool UpdateDSP(DSPDATA *prpDSP,BOOL bpSendDdx = TRUE);
	bool UpdateDSPInternal(DSPDATA *prpDSP);
	bool DeleteDSP(long lpUrno);
	bool DeleteDSPInternal(DSPDATA *prpDSP);
	DSPDATA  *GetDSPByUrno(long lpUrno);
	bool SaveDSP(DSPDATA *prpDSP);
	char pcmDSPFieldList[2048];
	void ProcessDSPBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
	inline bool DoesRemarkCodeExist(){return bmRemarkExists;}
	void SetWhere(const CString& ropWhere){omWhere = ropWhere;} //Prf: 8795
	void GetWhere(CString& ropWhere) const{ropWhere = omWhere;} //Prf: 8795

	// Private methods
private:
    void PrepareDSPData(DSPDATA *prpDSPData);
	void AppendFields();
	bool bmRemarkExists;
	CString omWhere; //Prf: 8795
	bool ValidateDSPBcData(const long& lrpUnro); //Prf: 8795
};

//---------------------------------------------------------------------------------------------------------

#endif //__CEDADSPDATA__
