// ErgAbfertigungsartenTableViewer.cpp 
//

#include <stdafx.h>
#include <ErgAbfertigungsartenTableViewer.h>
#include <CcsDdx.h>
#include <CedaGrnData.h>
#include <resource.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif


// Local function prototype
static void ErgAbfertigungsartenTableCf(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//-----------------------------------------------------------------------------------------------
// ErgAbfertigungsartenTableViewer
//

ErgAbfertigungsartenTableViewer::ErgAbfertigungsartenTableViewer(CCSPtrArray<SPHDATA> *popData)
{
	pomData = popData;

	bmIsFromSearch = FALSE;
    pomErgAbfertigungsartenTable = NULL;
    ogDdx.Register(this, SPH_NEW,	 CString("ERGABFERTIGUNGSARTENTABLEVIEWER"), CString("ErgAbfertigungsarten New"),	 ErgAbfertigungsartenTableCf);
    ogDdx.Register(this, SPH_CHANGE, CString("ERGABFERTIGUNGSARTENTABLEVIEWER"), CString("ErgAbfertigungsarten Update"), ErgAbfertigungsartenTableCf);
    ogDdx.Register(this, SPH_DELETE, CString("ERGABFERTIGUNGSARTENTABLEVIEWER"), CString("ErgAbfertigungsarten Delete"), ErgAbfertigungsartenTableCf);
}

//-----------------------------------------------------------------------------------------------

ErgAbfertigungsartenTableViewer::~ErgAbfertigungsartenTableViewer()
{
	ogDdx.UnRegister(this, NOTUSED);
    DeleteAll();
	omHeaderDataArray.DeleteAll();
}

//-----------------------------------------------------------------------------------------------

void ErgAbfertigungsartenTableViewer::Attach(CCSTable *popTable)
{
    pomErgAbfertigungsartenTable = popTable;
}

//-----------------------------------------------------------------------------------------------

void ErgAbfertigungsartenTableViewer::ChangeViewTo(const char *pcpViewName)
{
    DeleteAll();    
    MakeLines();
	UpdateDisplay();
}

//-----------------------------------------------------------------------------------------------

void ErgAbfertigungsartenTableViewer::MakeLines()
{
	int ilErgAbfertigungsartenCount = pomData->GetSize();
	for (int ilLc = 0; ilLc < ilErgAbfertigungsartenCount; ilLc++)
	{
		SPHDATA *prlErgAbfertigungsartenData = &pomData->GetAt(ilLc);
		MakeLine(prlErgAbfertigungsartenData);
	}
}

//-----------------------------------------------------------------------------------------------

void ErgAbfertigungsartenTableViewer::MakeLine(SPHDATA *prpErgAbfertigungsarten)
{

    //if( !IsPassFilter(prpErgAbfertigungsarten)) return;

    // Update viewer data for this shift record
    ERGABFERTIGUNGSARTENTABLE_LINEDATA rlErgAbfertigungsarten;

	rlErgAbfertigungsarten.Act3 = prpErgAbfertigungsarten->Act3; 
	rlErgAbfertigungsarten.Apc3 = prpErgAbfertigungsarten->Apc3; 
	rlErgAbfertigungsarten.Arde = prpErgAbfertigungsarten->Arde; 
 	rlErgAbfertigungsarten.Days = prpErgAbfertigungsarten->Days; 
 	rlErgAbfertigungsarten.Flnc = prpErgAbfertigungsarten->Flnc; 
	rlErgAbfertigungsarten.Flnn = prpErgAbfertigungsarten->Flnn; 
	rlErgAbfertigungsarten.Flns = prpErgAbfertigungsarten->Flns; 
 	rlErgAbfertigungsarten.Regn = prpErgAbfertigungsarten->Regn; 
 	rlErgAbfertigungsarten.Styp = prpErgAbfertigungsarten->Styp; 
	rlErgAbfertigungsarten.Ttyp = prpErgAbfertigungsarten->Ttyp; 
	rlErgAbfertigungsarten.Urno = prpErgAbfertigungsarten->Urno;
	
	GRNDATA *prlGrn;
	prlGrn = ogGrnData.GetGrnByUrno(prpErgAbfertigungsarten->Actm);
	if(prlGrn != NULL)
	{
		rlErgAbfertigungsarten.Actm = prlGrn->Grsn; 
	}
	else
	{
		rlErgAbfertigungsarten.Actm = " "; 
	}
	prlGrn = ogGrnData.GetGrnByUrno(prpErgAbfertigungsarten->Apcm);
	if(prlGrn != NULL)
	{
		rlErgAbfertigungsarten.Apcm = prlGrn->Grsn; 
	}
	else
	{
		rlErgAbfertigungsarten.Apcm = " "; 
	}
	prlGrn = ogGrnData.GetGrnByUrno(prpErgAbfertigungsarten->Alcm);
	if(prlGrn != NULL)
	{
		rlErgAbfertigungsarten.Alcm = prlGrn->Grsn; 
	}
	else
	{
		rlErgAbfertigungsarten.Alcm = " "; 
	}


	CreateLine(&rlErgAbfertigungsarten);
}

//-----------------------------------------------------------------------------------------------

void ErgAbfertigungsartenTableViewer::CreateLine(ERGABFERTIGUNGSARTENTABLE_LINEDATA *prpErgAbfertigungsarten)
{
    int ilLineCount = omLines.GetSize();
    for (int ilLineno = ilLineCount; ilLineno > 0; ilLineno--)
        if (CompareErgAbfertigungsarten(prpErgAbfertigungsarten, &omLines[ilLineno-1]) >= 0)
	        break;							 // should be inserted after Lines[ilLineno-1]
	ERGABFERTIGUNGSARTENTABLE_LINEDATA rlErgAbfertigungsarten;
	rlErgAbfertigungsarten = *prpErgAbfertigungsarten;
    omLines.NewAt(ilLineno, rlErgAbfertigungsarten);
}

//-----------------------------------------------------------------------------------------------
// UpdateDisplay: Load data selected by filter conditions to the display by using "omTable"

void ErgAbfertigungsartenTableViewer::UpdateDisplay()
{
	int ilLines = omLines.GetSize();
	const int ilColumns = 10;	//<===== Spaltenanzahl setzen (von 0 ab)
	omHeaderDataArray.DeleteAll();

	pomErgAbfertigungsartenTable->SetShowSelection(TRUE);
	pomErgAbfertigungsartenTable->ResetContent();

	TABLE_HEADER_COLUMN rlHeader;
	rlHeader.Alignment = COLALIGN_LEFT;
	rlHeader.Font = &ogCourier_Regular_10;

	int ilPos = 1;
	rlHeader.Length = 42; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING262),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 75; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING262),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 25; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING262),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 100; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING262),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 25; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING262),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 25; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING262),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 25; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING262),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 25; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING262),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 58; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING262),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 17; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING262),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 8; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING262),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);

	pomErgAbfertigungsartenTable->SetHeaderFields(omHeaderDataArray);
	pomErgAbfertigungsartenTable->SetDefaultSeparator();
	pomErgAbfertigungsartenTable->SetTableEditable(FALSE);

	TABLE_COLUMN rlColumnData;
	rlColumnData.Font = &ogCourier_Regular_10;
	rlColumnData.BkColor = WHITE;
	rlColumnData.Alignment = COLALIGN_LEFT;
	
    for (int ilLineNo = 0; ilLineNo < ilLines; ilLineNo++)
	{
		CCSPtrArray<TABLE_COLUMN> olColList;
		rlColumnData.Text = omLines[ilLineNo].Ttyp;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = CString(omLines[ilLineNo].Flnc) + CString(omLines[ilLineNo].Flnn) + CString(omLines[ilLineNo].Flns);
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Alcm;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Regn;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Act3;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Actm;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Apc3;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Apcm;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Days;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Styp;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Arde;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		pomErgAbfertigungsartenTable->AddTextLine(olColList,(void*)NULL);
		olColList.DeleteAll();
	}
	pomErgAbfertigungsartenTable->DisplayTable();
}

//-----------------------------------------------------------------------------------------------

CString ErgAbfertigungsartenTableViewer::Format(ERGABFERTIGUNGSARTENTABLE_LINEDATA *prpLine)
{
    CString s;
	// ???
    return s;
}

//-----------------------------------------------------------------------------------------------

bool ErgAbfertigungsartenTableViewer::FindErgAbfertigungsarten(char *pcpErgAbfertigungsartenKeya, char *pcpErgAbfertigungsartenKeyd, int& ilItem)
{
	int ilCount = omLines.GetSize();
    for (ilItem = 0; ilItem < ilCount; ilItem++)
    {
/*		if ( (omLines[ilItem].Keya == pcpErgAbfertigungsartenKeya) &&
			 (omLines[ilItem].Keyd == pcpErgAbfertigungsartenKeyd)    )
			return true;*/
	}
	return false;
}

//-----------------------------------------------------------------------------------------------

static void ErgAbfertigungsartenTableCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName)
{
    ErgAbfertigungsartenTableViewer *polViewer = (ErgAbfertigungsartenTableViewer *)popInstance;
	SPHDATA* polData =(SPHDATA *)vpDataPointer;
	if(*polData->Eart == 'A')
	{
		if (ipDDXType == SPH_CHANGE || ipDDXType == SPH_NEW) polViewer->ProcessErgAbfertigungsartenChange((SPHDATA *)vpDataPointer);
		if (ipDDXType == SPH_DELETE) polViewer->ProcessErgAbfertigungsartenDelete((SPHDATA *)vpDataPointer);
	} 
}

//-----------------------------------------------------------------------------------------------

void ErgAbfertigungsartenTableViewer::ProcessErgAbfertigungsartenChange(SPHDATA *prpErgAbfertigungsarten)
{
	int ilItem = 0;
	int ilColumnNo = 0;
	TABLE_COLUMN rlColumn;
	CCSPtrArray <TABLE_COLUMN> olLine;
	GRNDATA *prlGrn;

	rlColumn.Lineno = ilItem;
	rlColumn.VerticalSeparator = SEPA_NONE;
	rlColumn.SeparatorType = SEPA_NONE;
	rlColumn.BkColor = WHITE;
	rlColumn.Font = &ogCourier_Regular_10;
	rlColumn.Alignment = COLALIGN_LEFT;

	rlColumn.Text = prpErgAbfertigungsarten->Ttyp;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);

	rlColumn.Text = CString(prpErgAbfertigungsarten->Flnc) + CString(prpErgAbfertigungsarten->Flnn) + CString(prpErgAbfertigungsarten->Flns);
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);

	prlGrn = ogGrnData.GetGrnByUrno(prpErgAbfertigungsarten->Alcm);
	if(prlGrn != NULL)
	{
		rlColumn.Text = prlGrn->Grsn;
	}
	else
	{
		rlColumn.Text = " ";
	}

	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);

	rlColumn.Text = prpErgAbfertigungsarten->Regn;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);

	rlColumn.Text = prpErgAbfertigungsarten->Act3;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);

	prlGrn = ogGrnData.GetGrnByUrno(prpErgAbfertigungsarten->Actm);
	if(prlGrn != NULL)
	{
		rlColumn.Text = prlGrn->Grsn;
	}
	else
	{
		rlColumn.Text = " ";
	}
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);

	rlColumn.Text = prpErgAbfertigungsarten->Apc3;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);

	prlGrn = ogGrnData.GetGrnByUrno(prpErgAbfertigungsarten->Apcm);
	if(prlGrn != NULL)
	{
		rlColumn.Text = prlGrn->Grsn;
	}
	else
	{
		rlColumn.Text = " ";
	}
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);

	rlColumn.Text = prpErgAbfertigungsarten->Days;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);

	rlColumn.Text = prpErgAbfertigungsarten->Styp;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);

	rlColumn.Text = prpErgAbfertigungsarten->Arde;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);

	if (FindLine(prpErgAbfertigungsarten->Urno, ilItem))
	{
        ERGABFERTIGUNGSARTENTABLE_LINEDATA *prlLine = &omLines[ilItem];

		prlLine->Act3 = prpErgAbfertigungsarten->Act3;
		prlLine->Apc3 = prpErgAbfertigungsarten->Apc3;
		prlLine->Arde = prpErgAbfertigungsarten->Arde;
		prlLine->Days = prpErgAbfertigungsarten->Days;
		prlLine->Flnc = prpErgAbfertigungsarten->Flnc;
		prlLine->Flnn = prpErgAbfertigungsarten->Flnn;
		prlLine->Flns = prpErgAbfertigungsarten->Flns;
		prlLine->Regn = prpErgAbfertigungsarten->Regn;
		prlLine->Styp = prpErgAbfertigungsarten->Styp;
		prlLine->Ttyp = prpErgAbfertigungsarten->Ttyp;
		prlLine->Urno = prpErgAbfertigungsarten->Urno;

		prlGrn = ogGrnData.GetGrnByUrno(prpErgAbfertigungsarten->Actm);
		if(prlGrn != NULL)
		{
			prlLine->Actm = prlGrn->Grsn; 
		}
		else
		{
			prlLine->Actm = " "; 
		}
		prlGrn = ogGrnData.GetGrnByUrno(prpErgAbfertigungsarten->Apcm);
		if(prlGrn != NULL)
		{
			prlLine->Apcm = prlGrn->Grsn;
		}
		else
		{
			prlLine->Apcm = " "; 
		}
		prlGrn = ogGrnData.GetGrnByUrno(prpErgAbfertigungsarten->Alcm);
		
		if(prlGrn != NULL)
		{
			prlLine->Alcm = prlGrn->Grsn; 
		}
		else
		{
			prlLine->Alcm = " "; 
		}


		pomErgAbfertigungsartenTable->ChangeTextLine(ilItem, &olLine, (void *)prlLine);
		pomErgAbfertigungsartenTable->DisplayTable();
	}
	else
	{
		MakeLine(prpErgAbfertigungsarten);
		if (FindLine(prpErgAbfertigungsarten->Urno, ilItem))
		{
	        ERGABFERTIGUNGSARTENTABLE_LINEDATA *prlLine = &omLines[ilItem];
			if(prlLine != NULL)
			{
				pomErgAbfertigungsartenTable->AddTextLine(olLine, (void *)prlLine);
				pomErgAbfertigungsartenTable->DisplayTable();
			}
		}
	}
	olLine.DeleteAll();
}

//-----------------------------------------------------------------------------------------------

void ErgAbfertigungsartenTableViewer::ProcessErgAbfertigungsartenDelete(SPHDATA *prpErgAbfertigungsarten)
{
	int ilItem;
	if (FindLine(prpErgAbfertigungsarten->Urno, ilItem))
	{
		DeleteLine(ilItem);
		pomErgAbfertigungsartenTable->DeleteTextLine(ilItem);
		pomErgAbfertigungsartenTable->DisplayTable();
	}
}

//-----------------------------------------------------------------------------------------------

bool ErgAbfertigungsartenTableViewer::IsPassFilter(SPHDATA *prpErgAbfertigungsarten)
{

	return true;
}

//-----------------------------------------------------------------------------------------------

int ErgAbfertigungsartenTableViewer::CompareErgAbfertigungsarten(ERGABFERTIGUNGSARTENTABLE_LINEDATA *prpErgAbfertigungsarten1, ERGABFERTIGUNGSARTENTABLE_LINEDATA *prpErgAbfertigungsarten2)
{
/*	int ilCase=0;
	int	ilCompareResult;
	if(prpErgAbfertigungsarten1->Tifd == prpErgAbfertigungsarten2->Tifd)
	{
		ilCompareResult= 0;
	}
	else
	{
		if(prpErgAbfertigungsarten1->Tifd > prpErgAbfertigungsarten2->Tifd)
		{
			ilCompareResult = 1;
		}
		else
		{
			ilCompareResult = -1;
		}
	}
	return ilCompareResult;
*/  
	return 0;
}

//-----------------------------------------------------------------------------------------------

void ErgAbfertigungsartenTableViewer::DeleteLine(int ipLineno)
{
	omLines.DeleteAt(ipLineno);
}

//-----------------------------------------------------------------------------------------------

bool ErgAbfertigungsartenTableViewer::FindLine(long lpUrno, int &rilLineno)
{
    for (rilLineno = 0; rilLineno < omLines.GetSize(); rilLineno++)
	{
        if (omLines[rilLineno].Urno == lpUrno)
            return true;
	}
    return false;
}
//-----------------------------------------------------------------------------------------------

void ErgAbfertigungsartenTableViewer::DeleteAll()
{
    while (omLines.GetSize() > 0)
        DeleteLine(0);
}

//-----------------------------------------------------------------------------------------------
// Printing Routines
//-----------------------------------------------------------------------------------------------

void ErgAbfertigungsartenTableViewer::PrintTableView()
{
	CString olFooter1,olFooter2;
	omTableName = LoadStg(IDS_STRING169);
	int ilOrientation = PRINT_PORTRAET;//  PRINT_PORTRAET  PRINT_LANDSCAPE
	pomPrint = new CCSPrint((CWnd*)this,ilOrientation,45);// FirstLine(P=200,L=200),LeftOffset(P=200,L=50)
	//pomPrint->imMaxLines = 38;  // (P=57,L=38)
	//omBitmap.LoadBitmap(IDB_HAJLOGO);
	//pomPrint->SetBitmaps(&omBitmap,&omBitmap);
	if (pomPrint != NULL)
	{
		if (pomPrint->InitializePrinter(ilOrientation) == TRUE)
		{
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			pomPrint->imLineNo = pomPrint->imMaxLines + 1;
			DOCINFO	rlDocInfo;
			memset(&rlDocInfo, 0, sizeof(DOCINFO));
			rlDocInfo.cbSize = sizeof( DOCINFO );
			rlDocInfo.lpszDocName = omTableName;	
			pomPrint->omCdc.StartDoc( &rlDocInfo );
			pomPrint->imPageNo = 0;
			int ilLines = omLines.GetSize();
			olFooter1.Format("%d %s",ilLines,omTableName);
			for(int i = 0; i < ilLines; i++ ) 
			{
				if(pomPrint->imLineNo >= pomPrint->imMaxLines)
				{
					if(pomPrint->imPageNo > 0)
					{
						olFooter2.Format("%s: %d",LoadStg(IDS_STRING222),pomPrint->imPageNo);
						pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
						pomPrint->omCdc.EndPage();
					}
					PrintTableHeader();
				}
				if(pomPrint->imLineNo == (pomPrint->imMaxLines-1) || i == (ilLines-1))
				{
					PrintTableLine(&omLines[i],true);
				}
				else
				{
					PrintTableLine(&omLines[i],false);
				}
			}
			olFooter2.Format("%s: %d",LoadStg(IDS_STRING222),pomPrint->imPageNo);
			pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
			pomPrint->omCdc.EndPage();
			pomPrint->omCdc.EndDoc();
		}
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	delete pomPrint;
	//omBitmap.DeleteObject();
	pomPrint = NULL;
	}
}

//-----------------------------------------------------------------------------------------------

bool ErgAbfertigungsartenTableViewer::PrintTableHeader()
{
	pomPrint->omCdc.StartPage();
	pomPrint->imPageNo++;
	pomPrint->imLineNo = 0;

	pomPrint->PrintUIFHeader(omTableName,CString(CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")),pomPrint->imFirstLine-10);

	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;

	rlElement.Alignment   = PRINT_LEFT;
	rlElement.FrameLeft   = PRINT_NOFRAME;
	rlElement.FrameRight  = PRINT_NOFRAME;
	rlElement.FrameTop    = PRINT_NOFRAME;
	rlElement.FrameBottom = PRINT_FRAMEMEDIUM;
	rlElement.pFont       = &pomPrint->ogCourierNew_Bold_8;

	int ilSize = omHeaderDataArray.GetSize();
	for(int i=0;i<ilSize;i++)
	{
		//if(i!=1&&i!=2)// <= setzen, wenn Spalten nicht angezeigt werden sollen 
		{
			rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
			if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
			rlElement.Text   = omHeaderDataArray[i].Text;
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		}
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();


	return true;
}

//-----------------------------------------------------------------------------------------------

bool ErgAbfertigungsartenTableViewer::PrintTableLine(ERGABFERTIGUNGSARTENTABLE_LINEDATA *prpLine,bool bpLastLine)
{
	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;
	rlElement.pFont      = &pomPrint->ogCourierNew_Regular_8;
	int ilSize = omHeaderDataArray.GetSize();
	for(int i=0;i<ilSize;i++)
	{
		//if(i!=1&&i!=1)// <= setzen, wenn Spalten nicht angezeigt werden sollen 
		{
			rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
			if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
			rlElement.Alignment  = PRINT_LEFT;
			rlElement.FrameTop   = PRINT_FRAMETHIN;
			if(bpLastLine == true)
			{
				rlElement.FrameBottom = PRINT_FRAMETHIN;
			}
			else
			{
				rlElement.FrameBottom = PRINT_NOFRAME;
			}
			rlElement.FrameLeft  = PRINT_NOFRAME;
			rlElement.FrameRight = PRINT_NOFRAME;
			switch(i)
			{
			case 0:
				{
					rlElement.FrameLeft  = PRINT_FRAMETHIN;
					rlElement.Text		= prpLine->Ttyp;
				}
				break;
			case 1:
				{
					rlElement.Text		= CString(prpLine->Flnc) + CString(prpLine->Flnn) + CString(prpLine->Flns);
				}
				break;
			case 2:
				{
					rlElement.Text		= prpLine->Alcm;
				}
				break;
			case 3:
				{
					rlElement.Text		= prpLine->Regn;
				}
				break;
			case 4:
				{
					rlElement.Text		= prpLine->Act3;
				}
				break;
			case 5:
				{
					rlElement.Text		= prpLine->Actm;
				}
				break;
			case 6:
				{
					rlElement.Text		= prpLine->Apc3;
				}
				break;
			case 7:
				{
					rlElement.Text		= prpLine->Apcm;
				}
				break;
			case 8:
				{
					rlElement.Text		= prpLine->Days;
				}
				break;
			case 9:
				{
					rlElement.Text		= prpLine->Styp;
				}
				break;
			case 10:
				{
					rlElement.FrameRight = PRINT_FRAMETHIN;
					rlElement.Text		= prpLine->Arde;
				}
				break;
			}
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		}
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();
	return true;
}

//-----------------------------------------------------------------------------------------------
