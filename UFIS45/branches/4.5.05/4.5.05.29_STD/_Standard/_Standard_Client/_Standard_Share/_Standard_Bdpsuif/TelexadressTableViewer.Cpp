// TelexadressTableViewer.cpp 
//

#include <stdafx.h>
#include <TelexadressTableViewer.h>
#include <CcsDdx.h>
#include <resource.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif


// Local function prototype
static void TelexadressTableCf(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//-----------------------------------------------------------------------------------------------
// TelexadressTableViewer
//

TelexadressTableViewer::TelexadressTableViewer(CCSPtrArray<MVTDATA> *popData)
{
	pomData = popData;

	bmIsFromSearch = FALSE;
    pomTelexadressTable = NULL;
    ogDdx.Register(this, MVT_CHANGE, CString("TELEXADRESSTABLEVIEWER"), CString("Telexadress Update/new"), TelexadressTableCf);
    ogDdx.Register(this, MVT_DELETE, CString("TELEXADRESSTABLEVIEWER"), CString("Telexadress Delete"), TelexadressTableCf);
}

//-----------------------------------------------------------------------------------------------

TelexadressTableViewer::~TelexadressTableViewer()
{
	ogDdx.UnRegister(this, NOTUSED);
    DeleteAll();
	omHeaderDataArray.DeleteAll();
}

//-----------------------------------------------------------------------------------------------

void TelexadressTableViewer::Attach(CCSTable *popTable)
{
    pomTelexadressTable = popTable;
}

//-----------------------------------------------------------------------------------------------

void TelexadressTableViewer::ChangeViewTo(const char *pcpViewName)
{
    DeleteAll();    
    MakeLines();
	UpdateDisplay();
}

//-----------------------------------------------------------------------------------------------

void TelexadressTableViewer::MakeLines()
{
	int ilTelexadressCount = pomData->GetSize();
	for (int ilLc = 0; ilLc < ilTelexadressCount; ilLc++)
	{
		MVTDATA *prlTelexadressData = &pomData->GetAt(ilLc);
		MakeLine(prlTelexadressData);
	}
}

//-----------------------------------------------------------------------------------------------

void TelexadressTableViewer::MakeLine(MVTDATA *prpTelexadress)
{

    //if( !IsPassFilter(prpTelexadress)) return;

    // Update viewer data for this shift record
    TELEXADRESSTABLE_LINEDATA rlTelexadress;
	rlTelexadress.Urno = prpTelexadress->Urno; 
	rlTelexadress.Tead = prpTelexadress->Tead; 
	rlTelexadress.Alc3 = prpTelexadress->Alc3; 
	rlTelexadress.Apc3 = prpTelexadress->Apc3; 
	rlTelexadress.Tema = prpTelexadress->Tema; 
	rlTelexadress.Paye = prpTelexadress->Paye;
	rlTelexadress.Smvt = prpTelexadress->Smvt; 
	rlTelexadress.Beme = prpTelexadress->Beme;   
	rlTelexadress.Vafr = prpTelexadress->Vafr.Format("%d.%m.%Y %H:%M");   
	rlTelexadress.Vato = prpTelexadress->Vato.Format("%d.%m.%Y %H:%M"); 
	CreateLine(&rlTelexadress);
}

//-----------------------------------------------------------------------------------------------

void TelexadressTableViewer::CreateLine(TELEXADRESSTABLE_LINEDATA *prpTelexadress)
{
    int ilLineCount = omLines.GetSize();
    for (int ilLineno = ilLineCount; ilLineno > 0; ilLineno--)
        if (CompareTelexadress(prpTelexadress, &omLines[ilLineno-1]) >= 0)
	        break;							 // should be inserted after Lines[ilLineno-1]
	TELEXADRESSTABLE_LINEDATA rlTelexadress;
	rlTelexadress = *prpTelexadress;
    omLines.NewAt(ilLineno, rlTelexadress);
}

//-----------------------------------------------------------------------------------------------
// UpdateDisplay: Load data selected by filter conditions to the display by using "omTable"

void TelexadressTableViewer::UpdateDisplay()
{
	int ilLines = omLines.GetSize();
	const int ilColumns = 8;	//<===== Spaltenanzahl setzen (von 0 ab)
	omHeaderDataArray.DeleteAll();

	pomTelexadressTable->SetShowSelection(TRUE);
	pomTelexadressTable->ResetContent();

	TABLE_HEADER_COLUMN rlHeader;
	rlHeader.Alignment = COLALIGN_LEFT;
	rlHeader.Font = &ogCourier_Regular_10;

	int ilPos = 1;
	rlHeader.Length = 58; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING285),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 25; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING285),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 25; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING285),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 25; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING285),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 8; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING285),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 8; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING285),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 664; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING285),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 133; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING285),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 133; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING285),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);

	pomTelexadressTable->SetHeaderFields(omHeaderDataArray);
	pomTelexadressTable->SetDefaultSeparator();
	pomTelexadressTable->SetTableEditable(FALSE);

	TABLE_COLUMN rlColumnData;
	rlColumnData.Font = &ogCourier_Regular_10;
	rlColumnData.BkColor = WHITE;
	rlColumnData.Alignment = COLALIGN_LEFT;
	
    for (int ilLineNo = 0; ilLineNo < ilLines; ilLineNo++)
	{
		CCSPtrArray<TABLE_COLUMN> olColList;

		rlColumnData.Text = omLines[ilLineNo].Tead;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Alc3;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Apc3;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Paye;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		(omLines[ilLineNo].Tema == 'X') ? rlColumnData.Text = cgYes : rlColumnData.Text = cgNo;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		(omLines[ilLineNo].Smvt == 'X') ? rlColumnData.Text = cgYes : rlColumnData.Text = cgNo;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Beme;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Vafr;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Vato;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		pomTelexadressTable->AddTextLine(olColList,(void*)NULL);
		olColList.DeleteAll();
	}
	pomTelexadressTable->DisplayTable();
}

//-----------------------------------------------------------------------------------------------

CString TelexadressTableViewer::Format(TELEXADRESSTABLE_LINEDATA *prpLine)
{
    CString s;
	// ???
    return s;
}

//-----------------------------------------------------------------------------------------------

BOOL TelexadressTableViewer::FindTelexadress(char *pcpTelexadressKeya, char *pcpTelexadressKeyd, int& ilItem)
{
	int ilCount = omLines.GetSize();
    for (ilItem = 0; ilItem < ilCount; ilItem++)
    {
/*		if ( (omLines[ilItem].Keya == pcpTelexadressKeya) &&
			 (omLines[ilItem].Keyd == pcpTelexadressKeyd)    )
			return TRUE;*/
	}
	return FALSE;
}

//-----------------------------------------------------------------------------------------------

static void TelexadressTableCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName)
{
    TelexadressTableViewer *polViewer = (TelexadressTableViewer *)popInstance;
    if (ipDDXType == MVT_CHANGE) polViewer->ProcessTelexadressChange((MVTDATA *)vpDataPointer);
    if (ipDDXType == MVT_DELETE) polViewer->ProcessTelexadressDelete((MVTDATA *)vpDataPointer);
} 


//-----------------------------------------------------------------------------------------------

void TelexadressTableViewer::ProcessTelexadressChange(MVTDATA *prpTelexadress)
{
	int ilItem = 0;
	int ilColumnNo = 0;
	TABLE_COLUMN rlColumn;
	CCSPtrArray <TABLE_COLUMN> olLine;

	rlColumn.Lineno = ilItem;
	rlColumn.VerticalSeparator = SEPA_NONE;
	rlColumn.SeparatorType = SEPA_NONE;
	rlColumn.BkColor = WHITE;
	rlColumn.Font = &ogCourier_Regular_10;
	rlColumn.Alignment = COLALIGN_LEFT;

	rlColumn.Text = prpTelexadress->Tead;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpTelexadress->Alc3;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpTelexadress->Apc3;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpTelexadress->Paye;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	(*prpTelexadress->Tema == 'X') ? rlColumn.Text = cgYes : rlColumn.Text = cgNo;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	(*prpTelexadress->Smvt == 'X') ? rlColumn.Text = cgYes : rlColumn.Text = cgNo;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpTelexadress->Beme;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpTelexadress->Vafr.Format("%d.%m.%Y %H:%M");
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpTelexadress->Vato.Format("%d.%m.%Y %H:%M");
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);

	if (FindLine(prpTelexadress->Urno, ilItem))
	{
        TELEXADRESSTABLE_LINEDATA *prlLine = &omLines[ilItem];

		prlLine->Urno = prpTelexadress->Urno; 
		prlLine->Tead = prpTelexadress->Tead; 
		prlLine->Alc3 = prpTelexadress->Alc3; 
		prlLine->Apc3 = prpTelexadress->Apc3; 
		prlLine->Paye = prpTelexadress->Paye; 
		prlLine->Tema = prpTelexadress->Tema; 
		prlLine->Smvt = prpTelexadress->Smvt; 
		prlLine->Beme = prpTelexadress->Beme;   
		prlLine->Vafr = prpTelexadress->Vafr.Format("%d.%m.%Y %H:%M"); 
		prlLine->Vato = prpTelexadress->Vato.Format("%d.%m.%Y %H:%M"); 
	
		pomTelexadressTable->ChangeTextLine(ilItem, &olLine, (void *)prlLine);
		pomTelexadressTable->DisplayTable();
	}
	else
	{
		MakeLine(prpTelexadress);
		if (FindLine(prpTelexadress->Urno, ilItem))
		{
	        TELEXADRESSTABLE_LINEDATA *prlLine = &omLines[ilItem];
			if(prlLine != NULL)
			{
				pomTelexadressTable->AddTextLine(olLine, (void *)prlLine);
				pomTelexadressTable->DisplayTable();
			}
		}
	}
	olLine.DeleteAll();
}

//-----------------------------------------------------------------------------------------------

void TelexadressTableViewer::ProcessTelexadressDelete(MVTDATA *prpTelexadress)
{
	int ilItem;
	if (FindLine(prpTelexadress->Urno, ilItem))
	{
		DeleteLine(ilItem);
		pomTelexadressTable->DeleteTextLine(ilItem);
		pomTelexadressTable->DisplayTable();
	}
}

//-----------------------------------------------------------------------------------------------

BOOL TelexadressTableViewer::IsPassFilter(MVTDATA *prpTelexadress)
{

	return TRUE;
}

//-----------------------------------------------------------------------------------------------

int TelexadressTableViewer::CompareTelexadress(TELEXADRESSTABLE_LINEDATA *prpTelexadress1, TELEXADRESSTABLE_LINEDATA *prpTelexadress2)
{
/*	int ilCase=0;
	int	ilCompareResult;
	if(prpTelexadress1->Tifd == prpTelexadress2->Tifd)
	{
		ilCompareResult= 0;
	}
	else
	{
		if(prpTelexadress1->Tifd > prpTelexadress2->Tifd)
		{
			ilCompareResult = 1;
		}
		else
		{
			ilCompareResult = -1;
		}
	}
	return ilCompareResult;
*/  
	return 0;
}

//-----------------------------------------------------------------------------------------------

void TelexadressTableViewer::DeleteLine(int ipLineno)
{
	omLines.DeleteAt(ipLineno);
}

//-----------------------------------------------------------------------------------------------

BOOL TelexadressTableViewer::FindLine(long lpUrno, int &rilLineno)
{
    for (rilLineno = 0; rilLineno < omLines.GetSize(); rilLineno++)
	{
        if (omLines[rilLineno].Urno == lpUrno)
            return TRUE;
	}
    return FALSE;
}
//-----------------------------------------------------------------------------------------------

void TelexadressTableViewer::DeleteAll()
{
    while (omLines.GetSize() > 0)
        DeleteLine(0);
}

//-----------------------------------------------------------------------------------------------
// Printing Routines
//-----------------------------------------------------------------------------------------------

void TelexadressTableViewer::PrintTableView()
{
	CString olFooter1,olFooter2;
	omTableName = LoadStg(IDS_STRING192);
	int ilOrientation = PRINT_LANDSCAPE;//  PRINT_PORTRAET  PRINT_LANDSCAPE
	pomPrint = new CCSPrint((CWnd*)this,ilOrientation,45);// FirstLine(P=200,L=200),LeftOffset(P=200,L=50)
	//pomPrint->imMaxLines = 38;  // (P=57,L=38)
	//omBitmap.LoadBitmap(IDB_HAJLOGO);
	//pomPrint->SetBitmaps(&omBitmap,&omBitmap);
	if (pomPrint != NULL)
	{
		if (pomPrint->InitializePrinter(ilOrientation) == TRUE)
		{
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			pomPrint->imLineNo = pomPrint->imMaxLines + 1;
			DOCINFO	rlDocInfo;
			memset(&rlDocInfo, 0, sizeof(DOCINFO));
			rlDocInfo.cbSize = sizeof( DOCINFO );
			rlDocInfo.lpszDocName = omTableName;	
			pomPrint->omCdc.StartDoc( &rlDocInfo );
			pomPrint->imPageNo = 0;
			int ilLines = omLines.GetSize();
			olFooter1.Format("%d %s",ilLines,omTableName);
			for(int i = 0; i < ilLines; i++ ) 
			{
				if(pomPrint->imLineNo >= pomPrint->imMaxLines)
				{
					if(pomPrint->imPageNo > 0)
					{
						olFooter2.Format("%s: %d",LoadStg(IDS_STRING222),pomPrint->imPageNo);
						pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
						pomPrint->omCdc.EndPage();
					}
					PrintTableHeader();
				}
				if(pomPrint->imLineNo == (pomPrint->imMaxLines-1) || i == (ilLines-1))
				{
					PrintTableLine(&omLines[i],true);
				}
				else
				{
					PrintTableLine(&omLines[i],false);
				}
			}
			olFooter2.Format("%s: %d",LoadStg(IDS_STRING222),pomPrint->imPageNo);
			pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
			pomPrint->omCdc.EndPage();
			pomPrint->omCdc.EndDoc();
		}
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	delete pomPrint;
	//omBitmap.DeleteObject();
	pomPrint = NULL;
	}
}

//-----------------------------------------------------------------------------------------------

bool TelexadressTableViewer::PrintTableHeader()
{
	pomPrint->omCdc.StartPage();
	pomPrint->imPageNo++;
	pomPrint->imLineNo = 0;

	pomPrint->PrintUIFHeader(omTableName,CString(CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")),pomPrint->imFirstLine-10);

	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;

	rlElement.Alignment   = PRINT_LEFT;
	rlElement.FrameLeft   = PRINT_NOFRAME;
	rlElement.FrameRight  = PRINT_NOFRAME;
	rlElement.FrameTop    = PRINT_NOFRAME;
	rlElement.FrameBottom = PRINT_FRAMEMEDIUM;
	rlElement.pFont       = &pomPrint->ogCourierNew_Bold_8;

	int ilSize = omHeaderDataArray.GetSize();
	for(int i=0;i<ilSize;i++)
	{
		//if(i!=1&&i!=2)// <= setzen, wenn Spalten nicht angezeigt werden sollen 
		{
			rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
			if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
			rlElement.Text   = omHeaderDataArray[i].Text;
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		}
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();


	return true;
}

//-----------------------------------------------------------------------------------------------

bool TelexadressTableViewer::PrintTableLine(TELEXADRESSTABLE_LINEDATA *prpLine,bool bpLastLine)
{
	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;
	rlElement.pFont      = &pomPrint->ogCourierNew_Regular_8;
	int ilSize = omHeaderDataArray.GetSize();
	for(int i=0;i<ilSize;i++)
	{
		//if(i!=1&&i!=1)// <= setzen, wenn Spalten nicht angezeigt werden sollen 
		{
			rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
			if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
			rlElement.Alignment  = PRINT_LEFT;
			rlElement.FrameTop   = PRINT_FRAMETHIN;
			if(bpLastLine == true)
			{
				rlElement.FrameBottom = PRINT_FRAMETHIN;
			}
			else
			{
				rlElement.FrameBottom = PRINT_NOFRAME;
			}
			rlElement.FrameLeft  = PRINT_NOFRAME;
			rlElement.FrameRight = PRINT_NOFRAME;
			switch(i)
			{
			case 0:
				{
					rlElement.FrameLeft  = PRINT_FRAMETHIN;
					rlElement.Text		= prpLine->Tead;
				}
				break;
			case 1:
				{
					rlElement.Text		= prpLine->Alc3;
				}
				break;
			case 2:
				{
					rlElement.Text		= prpLine->Apc3;
				}
				break;
			case 3:
				{
					rlElement.Text		= prpLine->Paye;
				}
				break;
			case 4:
				{
					rlElement.Text		= prpLine->Tema;
				}
				break;
			case 5:
				{
					rlElement.Text		= prpLine->Smvt;
				}
				break;
			case 6:
				{
					rlElement.Text		= prpLine->Beme;
				}
				break;
			case 7:
				{
					rlElement.Text		= prpLine->Vafr;
				}
				break;
			case 8:
				{
				rlElement.FrameRight = PRINT_FRAMETHIN;
					rlElement.Text		= prpLine->Vato;
				}
				break;
			}
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		}
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();
	return true;
}

//-----------------------------------------------------------------------------------------------
