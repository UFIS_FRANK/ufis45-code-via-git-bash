#if !defined(AFX_SCO_DLG_H__67302835_F8CF_11D3_8FFC_0050DA1CAD13__INCLUDED_)
#define AFX_SCO_DLG_H__67302835_F8CF_11D3_8FFC_0050DA1CAD13__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Sco_Dlg.h : Header-Datei
//

#include <resrc1.h>		// main symbols#
#include <basicdata.h>
#include <ccsedit.h>


/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CSco_Dlg 

class CSco_Dlg : public CDialog
{
// Konstruktion
public:
	CSco_Dlg(CWnd* pParent = NULL);   // Standardkonstruktor

	CSco_Dlg(CString Code, CString Kost, CString Cweh, CWnd* pParent = NULL);

	CString omCode;
	CString omKost;
	CString omCweh;

// Dialogfelddaten
	//{{AFX_DATA(CSco_Dlg)
	enum { IDD = IDD_SCO_DLG };
	CCSEdit	m_ctrl_Cweh;
	CEdit	m_ctrl_Kost;
	CComboBox	m_CB_SCO;
	CString	m_CWEH;
	CString	m_KOST;
	//}}AFX_DATA
	CString m_COT_Code;


// Überschreibungen
	// Vom Klassen-Assistenten generierte virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(CSco_Dlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	//}}AFX_VIRTUAL

// Implementierung
protected:

	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(CSco_Dlg)
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	bool SaveSCOData();
	void ReadCOTData();
	void ReadSCOData();
	void SetSCOData();
	void InitCombo();
	CStringArray	*pomColumn2;
	CStringArray	*pomColumn1;
	CStringArray	*pomUrnos;
	CString			pomSCOUrno;
	CString			omHeader1;
	CString			omHeader2;
	int				im_UrnoPos;
	int				im_SurnPos;
	int				im_CodePos;
	int				im_VpfrPos;
	int				im_VptoPos;
	int				im_KostPos;
	int				im_CewhPos;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ fügt unmittelbar vor der vorhergehenden Zeile zusätzliche Deklarationen ein.

#endif // AFX_SCO_DLG_H__67302835_F8CF_11D3_8FFC_0050DA1CAD13__INCLUDED_
