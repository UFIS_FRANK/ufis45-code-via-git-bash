#if !defined(AFX_ERGVERKEHRSARTENDLG_H__9D480051_5507_11D1_B3C1_0000C016B067__INCLUDED_)
#define AFX_ERGVERKEHRSARTENDLG_H__9D480051_5507_11D1_B3C1_0000C016B067__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// ErgVerkehrsartenDlg.h : header file
//
#include <CedaSphData.h>
#include <CCSEdit.h>
#include <AatColorCheckBox.h>

/////////////////////////////////////////////////////////////////////////////
// ErgVerkehrsartenDlg dialog

class ErgVerkehrsartenDlg : public CDialog
{
// Construction
public:
	ErgVerkehrsartenDlg(SPHDATA *popSph,CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(ErgVerkehrsartenDlg)
	enum { IDD = IDD_ERGVERKEHRSARTENDLG };
	CStatic	m_ART;
	CCSEdit	m_REGN;
	CButton	m_OK;
	CString	m_Caption;
	AatColorCheckBox	m_DAYS1;
	AatColorCheckBox	m_DAYS2;
	AatColorCheckBox	m_DAYS3;
	AatColorCheckBox	m_DAYS4;
	AatColorCheckBox	m_DAYS5;
	AatColorCheckBox	m_DAYS6;
	AatColorCheckBox	m_DAYS7;
	AatColorCheckBox	m_DAYST;
	CButton	m_ARDEA;
	CButton	m_ARDED;
	CCSEdit	m_ACT3;
	CCSEdit	m_ACTM;
	CCSEdit	m_ALCM;
	CCSEdit	m_APC3;
	CCSEdit	m_APCM;
	CCSEdit	m_CDATD;
	CCSEdit	m_CDATT;
	CCSEdit	m_FLNC;
	CCSEdit	m_FLNN;
	CCSEdit	m_FLNS;
	CCSEdit	m_LSTUD;
	CCSEdit	m_LSTUT;
	CCSEdit	m_STYP;
	CCSEdit	m_TTYP;
	CCSEdit	m_USEC;
	CCSEdit	m_USEU;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(ErgVerkehrsartenDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(ErgVerkehrsartenDlg)
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	afx_msg void OnDays17();
	afx_msg void OnDaysT();
    afx_msg LONG OnKillfocus(UINT wParam, LONG lParam);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:
	void	SetCheckBoxState();
private:

	SPHDATA *pomSph;

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ERGVERKEHRSARTENDLG_H__9D480051_5507_11D1_B3C1_0000C016B067__INCLUDED_)
