// ParameterDlg.cpp : implementation file
//

#include <stdafx.h>
#include <BDPSUIF.h>
#include <ParameterDlg.h>
#include <PrivList.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// ParameterDlg dialog


ParameterDlg::ParameterDlg(PARDATA *popPar, VALDATA *popVal, CWnd* pParent /*=NULL*/) : CDialog(ParameterDlg::IDD, pParent)
{
	pomPar = popPar;
	pomVal = popVal;

	
	//{{AFX_DATA_INIT(ParameterDlg)
	//}}AFX_DATA_INIT
}


void ParameterDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(ParameterDlg)
	DDX_Control(pDX, IDOK,			 m_OK);
	DDX_Control(pDX, IDC_CDAT_D,	 m_CDATD);
	DDX_Control(pDX, IDC_CDAT_T,	 m_CDATT);
	DDX_Control(pDX, IDC_LSTU_D,	 m_LSTUD);
	DDX_Control(pDX, IDC_LSTU_T,	 m_LSTUT);
	DDX_Control(pDX, IDC_USEU,		 m_USEU);
	DDX_Control(pDX, IDC_USEC,		 m_USEC);
	DDX_Control(pDX, IDC_NAME,		 m_NAME);
	DDX_Control(pDX, IDC_APPL,		 m_APPL);
	DDX_Control(pDX, IDC_PTYP,		 m_PTYP);
	DDX_Control(pDX, IDC_VALU_TIME,	 m_VALU_TIME);
	DDX_Control(pDX, IDC_VALU_DATE,	 m_VALU_DATE);
	DDX_Control(pDX, IDC_VALU_TEXT,	 m_VALU_TEXT);
	DDX_Control(pDX, IDC_PAID,		 m_PAID);
	DDX_Control(pDX, IDC_VAFR_D,	 m_VAFRD);
	DDX_Control(pDX, IDC_VAFR_T,	 m_VAFRT);
	DDX_Control(pDX, IDC_VATO_D,	 m_VATOD);
	DDX_Control(pDX, IDC_VATO_T,	 m_VATOT);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(ParameterDlg, CDialog)
	//{{AFX_MSG_MAP(ParameterDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// ParameterDlg message handlers
//----------------------------------------------------------------------------------------

BOOL ParameterDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	m_Caption = LoadStg(IDS_STRING730) + " <" + m_Caption + ">";
	SetWindowText(m_Caption);

	char clStat = ogPrivList.GetStat("PARAMETERDLG.m_OK");
	SetWndStatAll(clStat,m_OK);

	//------------------------------------
	m_CDATD.SetBKColor(SILVER);
	m_CDATD.SetInitText(pomPar->Cdat.Format("%d.%m.%Y"));
	m_CDATT.SetBKColor(SILVER);
	m_CDATT.SetInitText(pomPar->Cdat.Format("%H:%M"));
	//------------------------------------
	m_LSTUD.SetBKColor(SILVER);
	m_LSTUD.SetInitText(pomPar->Lstu.Format("%d.%m.%Y"));
	m_LSTUT.SetBKColor(SILVER);
	m_LSTUT.SetInitText(pomPar->Lstu.Format("%H:%M"));
	//------------------------------------
	m_USEC.SetBKColor(SILVER);
	m_USEC.SetInitText(pomPar->Usec);
	//------------------------------------
	m_USEU.SetBKColor(SILVER);
	m_USEU.SetInitText(pomPar->Useu);
	//------------------------------------
	m_NAME.SetBKColor(SILVER);
	m_NAME.SetInitText(pomPar->Name);
	//------------------------------------
	m_PAID.SetBKColor(SILVER);
	m_PAID.SetInitText(pomPar->Paid);
	//------------------------------------
	m_APPL.SetBKColor(SILVER);
	m_APPL.SetInitText(pomPar->Appl);
	//------------------------------------
	m_PTYP.SetBKColor(SILVER);
	m_PTYP.SetInitText(pomPar->Ptyp);
	//------------------------------------

	omPType = pomPar->Type;
	CString olValu = pomPar->Valu;
	CTime  olTime;
	if(omPType == "DATE")
	{
		//Date
		m_VALU_DATE.SetTypeToDate(true);
		m_VALU_DATE.SetTextErrColor(RED);
		m_VALU_DATE.SetBKColor(YELLOW);

		m_VALU_TIME.SetBKColor(SILVER);
		m_VALU_TEXT.SetBKColor(SILVER);
		m_VALU_TIME.EnableWindow(FALSE);
		m_VALU_TEXT.EnableWindow(FALSE);

		olTime = DateStringToDate(olValu);
		m_VALU_DATE.SetInitText(olTime.Format("%d.%m.%Y"));
	}
	else if(omPType == "TIME")
	{
		//Time
		m_VALU_TIME.SetTypeToTime(true);
		m_VALU_TIME.SetTextErrColor(RED);
		m_VALU_TIME.SetBKColor(YELLOW);

		m_VALU_DATE.SetBKColor(SILVER);
		m_VALU_TEXT.SetBKColor(SILVER);
		m_VALU_DATE.EnableWindow(FALSE);
		m_VALU_TEXT.EnableWindow(FALSE);

		olTime = HourStringToDate(olValu);
		m_VALU_TIME.SetInitText(olTime.Format("%H:%M"));
	}
	else if(omPType == "DATI")
	{
		//Date and Time
		m_VALU_DATE.SetTypeToDate(true);
		m_VALU_DATE.SetTextErrColor(RED);
		m_VALU_DATE.SetBKColor(YELLOW);

		m_VALU_TIME.SetTypeToTime(true);
		m_VALU_TIME.SetTextErrColor(RED);
		m_VALU_TIME.SetBKColor(YELLOW);

		m_VALU_TEXT.SetBKColor(SILVER);
		m_VALU_TEXT.EnableWindow(FALSE);

		olTime = DBStringToDateTime(olValu);
		m_VALU_DATE.SetInitText(olTime.Format("%d.%m.%Y"));
		m_VALU_TIME.SetInitText(olTime.Format("%H:%M"));
	}
	else if(omPType == "LONG")
	{
		//Integer
		m_VALU_TEXT.SetTypeToInt(-9999999,9999999);

		m_VALU_TEXT.SetTextErrColor(RED);
		m_VALU_TEXT.SetBKColor(YELLOW);
		m_VALU_TEXT.SetInitText(pomPar->Valu);

		m_VALU_DATE.SetBKColor(SILVER);
		m_VALU_TIME.SetBKColor(SILVER);
		m_VALU_DATE.EnableWindow(FALSE);
		m_VALU_TIME.EnableWindow(FALSE);

	}
	else if(omPType == "FLOA")
	{
		//Float e.g. 
		m_VALU_TEXT.SetTypeToDouble(7,2,000.01,9999999.99);
		m_VALU_TEXT.SetTextErrColor(RED);
		m_VALU_TEXT.SetBKColor(YELLOW);
		m_VALU_TEXT.SetInitText(pomPar->Valu);

		m_VALU_DATE.SetBKColor(SILVER);
		m_VALU_TIME.SetBKColor(SILVER);
		m_VALU_DATE.EnableWindow(FALSE);
		m_VALU_TIME.EnableWindow(FALSE);

	}
	else //omPType = "TRIM"
	{
		m_VALU_TEXT.SetTypeToString("X(64)",64,1);
		m_VALU_TEXT.SetTextErrColor(RED);
		m_VALU_TEXT.SetBKColor(YELLOW);
		m_VALU_TEXT.SetInitText(pomPar->Valu);

		m_VALU_DATE.SetBKColor(SILVER);
		m_VALU_TIME.SetBKColor(SILVER);
		m_VALU_DATE.EnableWindow(FALSE);
		m_VALU_TIME.EnableWindow(FALSE);
	}

	//------------------------------------
	m_VAFRD.SetTypeToDate(true);
	m_VAFRD.SetTextErrColor(RED);
	m_VAFRD.SetBKColor(YELLOW);
	if(pomVal->Vafr.GetStatus() == COleDateTime::valid)
		m_VAFRD.SetInitText(pomVal->Vafr.Format("%d.%m.%Y"));
	else
		m_VAFRD.SetInitText("");
	// - - - - - - - - - - - - - - - - - -
	m_VAFRT.SetTypeToTime();
	m_VAFRT.SetTextErrColor(RED);
	//m_VAFRT.SetBKColor(YELLOW);
	if(pomVal->Vafr.GetStatus() == COleDateTime::valid)
		m_VAFRT.SetInitText(pomVal->Vafr.Format("%H:%M"));
	else
		m_VAFRT.SetInitText("");
	//------------------------------------
	m_VATOD.SetTypeToDate();
	m_VATOD.SetTextErrColor(RED);
	m_VATOD.SetInitText(pomVal->Vato.Format("%d.%m.%Y"));
	if(pomVal->Vato.GetStatus() == COleDateTime::valid)
		m_VATOD.SetInitText(pomVal->Vato.Format("%d.%m.%Y"));
	else
		m_VATOD.SetInitText("");
	// - - - - - - - - - - - - - - - - - -
	m_VATOT.SetTypeToTime();
	m_VATOT.SetTextErrColor(RED);
	m_VATOT.SetInitText(pomVal->Vato.Format("%H:%M"));
	if(pomVal->Vato.GetStatus() == COleDateTime::valid)
		m_VATOT.SetInitText(pomVal->Vato.Format("%H:%M"));
	else
		m_VATOT.SetInitText("");
	//------------------------------------
	return TRUE;
}

//----------------------------------------------------------------------------------------

void ParameterDlg::OnOK() 
{
	AfxGetApp()->DoWaitCursor(1);
	CString olErrorText;
	bool ilStatus = true;

	
	// Check different values types ////////////////////
	CString olValu, olValuText, olValuDate, olValuTime;
	CTime  olTime;
	if(omPType == "DATE")
	{
		if(m_VALU_DATE.GetStatus() == false)
		{
			ilStatus = false;
			if(m_VALU_DATE.GetWindowTextLength() == 0)
			{
				olErrorText += LoadStg(IDS_STRING107) +  ogNoData;
			}
			else
			{
				olErrorText += LoadStg(IDS_STRING107) + ogNotFormat;
			}
		}
		else
		{
			m_VALU_DATE.GetWindowText(olValuDate);
			olTime = DateStringToDate(olValuDate);
			olValu = olTime.Format("%Y%m%d");
		}
	}
	else if(omPType == "TIME")
	{
		if(m_VALU_TIME.GetStatus() == false)
		{
			ilStatus = false;
			if(m_VALU_TIME.GetWindowTextLength() == 0)
			{
				olErrorText += LoadStg(IDS_STRING767) +  ogNoData;
			}
			else
			{
				olErrorText += LoadStg(IDS_STRING767) + ogNotFormat;
			}
		}
		else
		{
			m_VALU_TIME.GetWindowText(olValuTime);
			olTime = DateStringToDate(olValuTime);
			olValu = olTime.Format("%H%M");
		}
	}
	else if(omPType == "DATI")
	{
		if(m_VALU_DATE.GetStatus() == false)
		{
			ilStatus = false;
			if(m_VALU_DATE.GetWindowTextLength() == 0)
			{
				olErrorText += LoadStg(IDS_STRING107) +  ogNoData;
			}
			else
			{
				olErrorText += LoadStg(IDS_STRING107) + ogNotFormat;
			}
		}
		else
		{
			if(m_VALU_TIME.GetStatus() == false)
			{
				ilStatus = false;
				if(m_VALU_TIME.GetWindowTextLength() == 0)
				{
					olErrorText += LoadStg(IDS_STRING767) +  ogNoData;
				}
				else
				{
					olErrorText += LoadStg(IDS_STRING767) + ogNotFormat;
				}
			}
			else
			{
				m_VALU_TIME.GetWindowText(olValuTime);
				m_VALU_DATE.GetWindowText(olValuDate);
				olTime = DateHourStringToDate(olValuDate, olValuTime);
				olValu = olTime.Format("%Y%m%d%H%M00");
			}
		}
	}
	else if(omPType == "LONG")
	{
		if(m_VALU_TEXT.GetStatus() == false)
		{
			ilStatus = false;
			if(m_VALU_TEXT.GetWindowTextLength() == 0)
			{
				olErrorText += LoadStg(IDS_STRING769) +  ogNoData;
			}
			else
			{
				olErrorText += LoadStg(IDS_STRING768);
			}
		}
		else
		{
			m_VALU_TEXT.GetWindowText(olValuText);
			olValu = olValuText;
		}
	}
	else if(omPType == "FLOA")
	{
		if(m_VALU_TEXT.GetStatus() == false)
		{
			ilStatus = false;
			if(m_VALU_TEXT.GetWindowTextLength() == 0)
			{
				olErrorText += LoadStg(IDS_STRING769) +  ogNoData;
			}
			else
			{
				olErrorText += LoadStg(IDS_STRING770);
			}
		}
		else
		{
			m_VALU_TEXT.GetWindowText(olValuText);
			olValu = olValuText;
		}
	}
	else //omPType = "TRIM"
	{
		if(m_VALU_TEXT.GetStatus() == false)
		{
			ilStatus = false;
			if(m_VALU_TEXT.GetWindowTextLength() == 0)
			{
				olErrorText += LoadStg(IDS_STRING769) +  ogNoData;
			}
			else
			{
				olErrorText += LoadStg(IDS_STRING769) + ogNotFormat;
			}
		}
		else
		{
			m_VALU_TEXT.GetWindowText(olValuText);
			olValu = olValuText;
		}
	}

	if(m_VAFRD.GetStatus() == false)
	{
		ilStatus = false;
		if(m_VAFRD.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING232) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING232) + ogNotFormat;
		}
	}
	if(m_VAFRT.GetStatus() == false)
	{
		
		/*if(m_VAFRT.GetWindowTextLength() > 0)
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING233) +  ogNoData;
		}
		else
		{*/
			ilStatus = false;
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING233) + ogNotFormat;
		//}
	}
	if(m_VATOD.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING231) + LoadStg(IDS_STRING232) + ogNotFormat;
	}
	if(m_VATOT.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING231) + LoadStg(IDS_STRING233) + ogNotFormat;
	}

	///////////////////////////////////////////////////////////////////////////
	CString olNafrd,olNafrt,olNatod,olNatot,olVafrd,olVafrt,olVatod,olVatot;
	COleDateTime olTmpTimeFr,olTmpTimeTo;

	m_VAFRD.GetWindowText(olVafrd);
	m_VAFRT.GetWindowText(olVafrt);
	m_VATOD.GetWindowText(olVatod);
	m_VATOT.GetWindowText(olVatot);

	if(olVafrd.GetLength() > 0 && olVafrt.GetLength() == 0)
	{
		olVafrt = CString("0000");
	}
	if(olVatod.GetLength() > 0 && olVatot.GetLength() == 0)
	{
		olVatot = CString("2359");
	}
	/*if((m_VAFRD.GetWindowTextLength() != 0 && m_VAFRT.GetWindowTextLength() == 0) || (m_VAFRD.GetWindowTextLength() == 0 && m_VAFRT.GetWindowTextLength() != 0))
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING234);
	}
	if((m_VATOD.GetWindowTextLength() != 0 && m_VATOT.GetWindowTextLength() == 0) || (m_VATOD.GetWindowTextLength() == 0 && m_VATOT.GetWindowTextLength() != 0))
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING235);
	}*/
	if(ilStatus == true) //m_VAFRD.GetStatus() == true && m_VAFRT.GetStatus() == true && m_VATOD.GetStatus() == true && m_VATOT.GetStatus() == true)
	{
		//CString olText;
		COleDateTime olDate; 
		CTime        olTime;
		//m_VAFRD.GetWindowText(olText); 
		olDate = OleDateStringToDate(olVafrd);
		//m_VAFRT.GetWindowText(olText); 
		olTime = HourStringToDate(olVafrt);
		if(olDate.GetStatus() == COleDateTime::valid && olTime != TIMENULL)
		{
			olTmpTimeFr = COleDateTime(olDate.GetYear(),olDate.GetMonth(),olDate.GetDay(),olTime.GetHour(), olTime.GetMinute(), 0);
		}
		else
		{
			olTmpTimeFr.SetStatus(COleDateTime::null);
		}
		//m_VATOD.GetWindowText(olText); 
		olDate = OleDateStringToDate(olVatod);
		//m_VATOT.GetWindowText(olText); 
		olTime = HourStringToDate(olVatot);
		if(olDate.GetStatus() == COleDateTime::valid && olTime != TIMENULL)
		{
			olTmpTimeTo = COleDateTime(olDate.GetYear(),olDate.GetMonth(),olDate.GetDay(),olTime.GetHour(), olTime.GetMinute(), 0);
		}
		else
		{
			olTmpTimeTo.SetStatus(COleDateTime::null);
		}

		if(olTmpTimeTo.GetStatus() == COleDateTime::valid && (olTmpTimeTo < olTmpTimeFr || olTmpTimeFr.GetStatus() == COleDateTime::null))
		{
			ilStatus = false;
			olErrorText += LoadStg(IDS_STRING236);
		}
	}

/*
	//Konsistenz-Check//
	if(ilStatus == true)
	{
		CString olEnam;
		char clWhere[100];
		CCSPtrArray<PARDATA> olParCPA;

		m_ENAM.GetWindowText(olEnam);
		sprintf(clWhere,"WHERE ENAM='%s'",olEnam);
		if(ogParData.ReadSpecialData(&olParCPA,clWhere,"URNO,ENAM",false) == true)
		{
			if(olParCPA[0].Urno != pomPar->Urno)
			{
				ilStatus = false;
				olErrorText += LoadStg(IDS_STRING288) + LoadStg(IDS_EXIST);
			}
		}
		olParCPA.DeleteAll();
	}
*/
	///////////////////////////////////////////////////////////////////////////
	AfxGetApp()->DoWaitCursor(-1);
	if (ilStatus == true)
	{
		strcpy(pomPar->Valu, olValu.GetBuffer(0));
		pomVal->Vafr = olTmpTimeFr;
		pomVal->Vato = olTmpTimeTo;
		CDialog::OnOK();
	}
	else
	{
		Beep(440,70);
		MessageBox(olErrorText,LoadStg(IDS_STRING145),MB_ICONERROR);
		if(omPType == "DATE" || omPType == "DATI")
		{
			m_VALU_DATE.SetFocus();
			m_VALU_DATE.SetSel(0,-1);
		}
		else if(omPType == "TIME")
		{
			m_VALU_TIME.SetFocus();
			m_VALU_TIME.SetSel(0,-1);
		}
		else
		{
			m_VALU_TEXT.SetFocus();
			m_VALU_TEXT.SetSel(0,-1);
		}
	}
}

//----------------------------------------------------------------------------------------

