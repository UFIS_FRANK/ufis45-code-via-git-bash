#ifndef __ARBEITSVERTRAGSARTENTABLEVIEWER_H__
#define __ARBEITSVERTRAGSARTENTABLEVIEWER_H__

#include <stdafx.h>
#include <CedaCotData.h>
#include <CCSTable.h>
#include <CViewer.h>
#include <CCSPrint.h>


struct ARBEITSVERTRAGSARTENTABLE_LINEDATA
{

	long 	 Urno; 	// Eindeutige Datensatz-Nr.
	CString  Ctrc; 	// Code
	CString  Ctrn; 	// Bezeichnung
	CString  Dptc; 	// Organisationseinheit Code
	CString  Rema; 	// Bemerkung
	CString  Whpw; 	// Arbeitstunden pro Wochen
	CString  Wrkd; 	// Wochenarbeitstage
	CString  Inbu; 	// Arbeitstunden pro Wochen
	CString  Regi; 	// Wochenarbeitstage


};

/////////////////////////////////////////////////////////////////////////////
// ArbeitsvertragsartenTableViewer

	  
class ArbeitsvertragsartenTableViewer : public CViewer
{
// Constructions
public:
    ArbeitsvertragsartenTableViewer(CCSPtrArray<COTDATA> *popData);
    ~ArbeitsvertragsartenTableViewer();

    void Attach(CCSTable *popAttachWnd);
    virtual void ChangeViewTo(const char *pcpViewName);
// Internal data processing routines
private:
	bool IsPassFilter(COTDATA *prpArbeitsvertragsarten);
	int CompareArbeitsvertragsarten(ARBEITSVERTRAGSARTENTABLE_LINEDATA *prpArbeitsvertragsarten1, ARBEITSVERTRAGSARTENTABLE_LINEDATA *prpArbeitsvertragsarten2);
    void MakeLines();
	void MakeLine(COTDATA *prpArbeitsvertragsarten);
	bool FindLine(long lpUrno, int &rilLineno);
// Operations
public:
	void DeleteAll();
	void CreateLine(ARBEITSVERTRAGSARTENTABLE_LINEDATA *prpArbeitsvertragsarten);
	void DeleteLine(int ipLineno);
// Window refreshing routines
public:
	void UpdateDisplay();
	CString Format(ARBEITSVERTRAGSARTENTABLE_LINEDATA *prpLine);
	void ProcessArbeitsvertragsartenChange(COTDATA *prpArbeitsvertragsarten);
	void ProcessArbeitsvertragsartenDelete(COTDATA *prpArbeitsvertragsarten);
	bool FindArbeitsvertragsarten(char *prpArbeitsvertragsartenKeya, char *prpArbeitsvertragsartenKeyd, int& ilItem);
// Attributes used for filtering condition
private:
	bool bmIsFromSearch;
// Attributes
private:
    CCSTable *pomArbeitsvertragsartenTable;
	CCSPtrArray<COTDATA> *pomData;
// Methods which handle changes (from Data Distributor)
public:
    CCSPtrArray<ARBEITSVERTRAGSARTENTABLE_LINEDATA> omLines;
///////
	CCSPtrArray<TABLE_HEADER_COLUMN> omHeaderDataArray;
//Print 
	void PrintTableView();
	bool PrintTableLine(ARBEITSVERTRAGSARTENTABLE_LINEDATA *prpLine,bool bpLastLine);
	bool PrintTableHeader();
	CCSPrint *pomPrint;
	//CBitmap omBitmap;
	CString omTableName;

};

#endif //__ARBEITSVERTRAGSARTENTABLEVIEWER_H__
