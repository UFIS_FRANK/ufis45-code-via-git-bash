// CedaEntData.cpp
 
#include <stdafx.h>
#include <CedaEntData.h>
#include <resource.h>


void ProcessEntCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//--CEDADATA-----------------------------------------------------------------------------------------------

CedaEntData::CedaEntData() : CCSCedaData(&ogCommHandler)
{
	// Create an array of CEDARECINFO for ENTDataStruct
	BEGIN_CEDARECINFO(ENTDATA,ENTDataRecInfo)
		FIELD_DATE		(Cdat,"CDAT")
		FIELD_CHAR_TRIM	(Entc,"ENTC")
		FIELD_CHAR_TRIM	(Enam,"ENAM")
		FIELD_DATE		(Lstu,"LSTU")
		FIELD_LONG		(Urno,"URNO")
		FIELD_CHAR_TRIM	(Usec,"USEC")
		FIELD_CHAR_TRIM	(Useu,"USEU")

	END_CEDARECINFO //(ENTDataStruct)

	// FIELD_LONG, FIELD_DATE 
	// Copy the record structure
	for (int i=0; i< sizeof(ENTDataRecInfo)/sizeof(ENTDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&ENTDataRecInfo[i],sizeof(CEDARECINFO));
		omRecInfo.Add(prpCedaRecInfo);
	} // end for

	// initialize field names
	strcpy(pcmTableName,"ENT");
	strcpy(pcmListOfFields,"CDAT,ENTC,ENAM,LSTU,URNO,USEC,USEU");
	pcmFieldList = pcmListOfFields; //because in CedaData pcmFieldList is just a pointer

	omData.RemoveAll();
}

//---------------------------------------------------------------------------------------------------------

void CedaEntData::GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType)
{
	ropFields.Add("CDAT");
	ropFields.Add("ENTC");
	ropFields.Add("ENAM");
	ropFields.Add("LSTU");
	ropFields.Add("URNO");
	ropFields.Add("USEC");
	ropFields.Add("USEU");

	ropDesription.Add(LoadStg(IDS_STRING343));
	ropDesription.Add(LoadStg(IDS_STRING632));
	ropDesription.Add(LoadStg(IDS_STRING633));
	ropDesription.Add(LoadStg(IDS_STRING344));
	ropDesription.Add(LoadStg(IDS_STRING346));
	ropDesription.Add(LoadStg(IDS_STRING347));
	ropDesription.Add(LoadStg(IDS_STRING348));

	ropType.Add("Date");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("Date");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");

}

//--REGISTER----------------------------------------------------------------------------------------------

void CedaEntData::Register(void)
{
	ogDdx.Register((void *)this,BC_ENT_CHANGE,	CString("ENTDATA"), CString("Ent-changed"),	ProcessEntCf);
	ogDdx.Register((void *)this,BC_ENT_NEW,		CString("ENTDATA"), CString("Ent-new"),		ProcessEntCf);
	ogDdx.Register((void *)this,BC_ENT_DELETE,	CString("ENTDATA"), CString("Ent-deleted"),	ProcessEntCf);
}

//--~CEDADATA----------------------------------------------------------------------------------------------

CedaEntData::~CedaEntData(void)
{
	omRecInfo.DeleteAll();
	ClearAll();
}

//--CLEAR-ALL----------------------------------------------------------------------------------------------

void CedaEntData::ClearAll(bool bpWithRegistration)
{
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(bpWithRegistration)
	{
		ogDdx.UnRegister(this,NOTUSED);
	}
}

//--READ-ALL-----------------------------------------------------------------------------------------------

bool CedaEntData::Read(char *pspWhere /*NULL*/)
{
    // Select data from the database
	bool ilRc = true;
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(pspWhere == NULL)
	{	
		ilRc = CedaAction("RT");
	}
	else
	{
		ilRc = CedaAction("RT", pspWhere);
	}
	if (ilRc != true)
	{
		return ilRc;
	}
	// Load data from CedaData into the dynamic array of record
	for (int ilLc = 0; ilRc == true; ilLc++)
	{
		ENTDATA *prlEnt = new ENTDATA;
		if ((ilRc = GetFirstBufferRecord(prlEnt)) == true)
		{
			omData.Add(prlEnt);//Update omData
			omUrnoMap.SetAt((void *)prlEnt->Urno,prlEnt);
		}
		else
		{
			delete prlEnt;
		}
	}
    return true;
}

//--INSERT-------------------------------------------------------------------------------------------------

bool CedaEntData::Insert(ENTDATA *prpEnt)
{
	prpEnt->IsChanged = DATA_NEW;
	if(Save(prpEnt) == false) return false; //Update Database
	InsertInternal(prpEnt);
    return true;
}

//--INSERT-INTERNAL--------------------------------------------------------------------------------------

bool CedaEntData::InsertInternal(ENTDATA *prpEnt)
{
	ogDdx.DataChanged((void *)this, ENT_NEW,(void *)prpEnt ); //Update Viewer
	omData.Add(prpEnt);//Update omData
	omUrnoMap.SetAt((void *)prpEnt->Urno,prpEnt);
    return true;
}

//--DELETE-------------------------------------------------------------------------------------------------

bool CedaEntData::Delete(long lpUrno)
{
	ENTDATA *prlEnt = GetEntByUrno(lpUrno);
	if (prlEnt != NULL)
	{
		prlEnt->IsChanged = DATA_DELETED;
		if(Save(prlEnt) == false) return false; //Update Database
		DeleteInternal(prlEnt);
	}
    return true;
}

//--DELETE-INTERNAL----------------------------------------------------------------------------------------

bool CedaEntData::DeleteInternal(ENTDATA *prpEnt)
{
	ogDdx.DataChanged((void *)this,ENT_DELETE,(void *)prpEnt); //Update Viewer
	omUrnoMap.RemoveKey((void *)prpEnt->Urno);
	int ilEntCount = omData.GetSize();
	for (int ilLc = 0; ilLc < ilEntCount; ilLc++)
	{
		if (omData[ilLc].Urno == prpEnt->Urno)
		{
			omData.DeleteAt(ilLc);//Update omData
			break;
		}
	}
    return true;
}

//--UPDATE-------------------------------------------------------------------------------------------------

bool CedaEntData::Update(ENTDATA *prpEnt)
{
	if (GetEntByUrno(prpEnt->Urno) != NULL)
	{
		if (prpEnt->IsChanged == DATA_UNCHANGED)
		{
			prpEnt->IsChanged = DATA_CHANGED;
		}
		if(Save(prpEnt) == false) return false; //Update Database
		UpdateInternal(prpEnt);
	}
    return true;
}

//--UPDATE-INTERNAL--------------------------------------------------------------------------------------------

bool CedaEntData::UpdateInternal(ENTDATA *prpEnt)
{
	ENTDATA *prlEnt = GetEntByUrno(prpEnt->Urno);
	if (prlEnt != NULL)
	{
		*prlEnt = *prpEnt; //Update omData
		ogDdx.DataChanged((void *)this,ENT_CHANGE,(void *)prlEnt); //Update Viewer
	}
    return true;
}

//--GET-BY-URNO--------------------------------------------------------------------------------------------

ENTDATA *CedaEntData::GetEntByUrno(long lpUrno)
{
	ENTDATA  *prlEnt;
	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlEnt) == TRUE)
	{
		return prlEnt;
	}
	return NULL;
}

//--READSPECIALDATA-------------------------------------------------------------------------------------

bool CedaEntData::ReadSpecialData(CCSPtrArray<ENTDATA> *popEnt,char *pspWhere,char *pspFieldList,bool ipSYS/*=true*/)
{
	bool ilRc = true;
	char pclFieldList[256] = " ";

	if(strlen(pspFieldList) > 0) 
	{
		strcpy(pclFieldList, pspFieldList);
	}
	if(ipSYS == true) 
	{
		if (CedaAction("SYS","ENT",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	else
	{
		if (CedaAction("RT","ENT",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	if(popEnt != NULL)
	{
		for (int ilLc = 0; ilRc == true; ilLc++)
		{
			ENTDATA *prpEnt = new ENTDATA;
			if ((ilRc = GetBufferRecord(ilLc,prpEnt,CString(pclFieldList))) == true)
			{
				popEnt->Add(prpEnt);
			}
			else
			{
				delete prpEnt;
			}
		}
		if(popEnt->GetSize() == 0) return false;
	}
    return true;
}

//--SAVE---------------------------------------------------------------------------------------------------

bool CedaEntData::Save(ENTDATA *prpEnt)
{
	bool ilRc = true;
	CString olListOfData;
	char pclSelection[124];
	char pclData[524];

	if (prpEnt->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}
	switch(prpEnt->IsChanged)
	{
	case DATA_NEW:
		MakeCedaData(&omRecInfo,olListOfData,prpEnt);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("IRT","","",pclData);
		prpEnt->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpEnt->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpEnt);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("URT",pclSelection,"",pclData);
		prpEnt->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_DELETED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpEnt->Urno);
		ilRc = CedaAction("DRT",pclSelection);
		break;
	}
    return ilRc;
}

//--PROCESS-CF---------------------------------------------------------------------------------------------

void  ProcessEntCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	ogENTData.ProcessBc(ipDDXType,vpDataPointer,ropInstanceName);
}

//--PROCESS-BC---------------------------------------------------------------------------------------------

void  CedaEntData::ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlEntData;
	prlEntData = (struct BcStruct *) vpDataPointer;
	ENTDATA *prlEnt;
	if(ipDDXType == BC_ENT_NEW)
	{
		prlEnt = new ENTDATA;
		GetRecordFromItemList(prlEnt,prlEntData->Fields,prlEntData->Data);
		if(ValidateEntBcData(prlEnt->Urno)) //Prf: 8795
		{
			InsertInternal(prlEnt);
		}
		else
		{
			delete prlEnt;
		}
	}
	if(ipDDXType == BC_ENT_CHANGE)
	{
		long llUrno = GetUrnoFromSelection(prlEntData->Selection);
		prlEnt = GetEntByUrno(llUrno);
		if(prlEnt != NULL)
		{
			GetRecordFromItemList(prlEnt,prlEntData->Fields,prlEntData->Data);
			if(ValidateEntBcData(prlEnt->Urno)) //Prf: 8795
			{
				UpdateInternal(prlEnt);
			}
			else
			{
				DeleteInternal(prlEnt);
			}
		}
	}
	if(ipDDXType == BC_ENT_DELETE)
	{
		long llUrno = GetUrnoFromSelection(prlEntData->Selection);

		prlEnt = GetEntByUrno(llUrno);
		if (prlEnt != NULL)
		{
			DeleteInternal(prlEnt);
		}
	}
}

//Prf: 8795
//--ValidateEntBcData--------------------------------------------------------------------------------------

bool CedaEntData::ValidateEntBcData(const long& lrpUrno)
{
	bool blValidateEntBcData = true;
	if(!omWhere.IsEmpty()) //It means it is not the default view and we need to check in the database, since we don't need to check for the default view
	{
		char chWhere[20];
		sprintf(chWhere, "URNO='%ld'",lrpUrno);
		CString olWhere = omWhere + CString(" AND ") + CString(chWhere);
		CCSPtrArray<ENTDATA> olEnts;
		if(!ReadSpecialData(&olEnts,olWhere.GetBuffer(0),"URNO",false))
		{		
			blValidateEntBcData = false;
		}
	}
	return blValidateEntBcData;
}

//---------------------------------------------------------------------------------------------------------
