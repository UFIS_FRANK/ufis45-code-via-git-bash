#ifndef __LOGONAMESTABLEVIEWER_H__
#define __LOGONAMESTABLEVIEWER_H__

#include <stdafx.h>
#include <CedaFLGData.h>
#include <CCSTable.h>
#include <CViewer.h>
#include <CCSPrint.h>

struct LOGONAMESTABLE_LINEDATA
{
	long	Urno;	//Unique Record Number
	CString	Lgfn;   //Logo File Description
	CString	Lgsn;   //Logo File Name
	CString	Type;   //Logo Type (G=Gate, C=Counter)
	CString Alc2;   //Airline 2-Letter Code
	CString Alc3;   //Airline 3-Letter Code
};

/////////////////////////////////////////////////////////////////////////////
// LogoNamesTableViewer

class LogoNamesTableViewer : public CViewer
{
	// Constructions
public:
    LogoNamesTableViewer(CCSPtrArray<FLGDATA> *popData);
    ~LogoNamesTableViewer();
    void Attach(CCSTable *popAttachWnd);
    virtual void ChangeViewTo(const char *pcpViewName);

	// Internal data processing routines
private:
	bool IsPassFilter(FLGDATA *prpAirline);
	int  CompareLogoNames(LOGONAMESTABLE_LINEDATA *prpAirline1, LOGONAMESTABLE_LINEDATA *prpAirline2);
    void MakeLines();
	void MakeLine(FLGDATA *prpAirline);
	bool FindLine(long lpUrno, int &rilLineno);
	CString PrepareDisplayDateFormat(const CString& ropDate,bool blSetDefaultIncaseEmptyDate = false) const;

	// Operations
public:
	void DeleteAll();
	void CreateLine(LOGONAMESTABLE_LINEDATA *prpAirline);
	void DeleteLine(int ipLineno);

	// Window refreshing routines
public:
	void UpdateLogoNames();
	void ProcessLogoNamesChange(FLGDATA *prpAirline);
	void ProcessLogoNamesDelete(FLGDATA *prpAirline);
	bool FindLogoNames(char *prpLogoNamesName, int& ilItem);
	//bool CreateExcelFile(const CString& ropSeparator,const CString& ropListSeparator);

	// Attributes used for filtering condition
private:
	bool bmIsFromSearch;
	
	// Attributes
private:
    CCSTable			*pomLogoNamesTable;
	CCSPtrArray<FLGDATA>*pomData;
	
	// Methods which handle changes (from Data Distributor)
public:
    CCSPtrArray<LOGONAMESTABLE_LINEDATA> omLines;
	CCSPtrArray<TABLE_HEADER_COLUMN> omHeaderDataArray;

	// Print 
	void PrintTableView();
	bool PrintTableLine(int ilPage,CUIntArray& ropColsPerPage,LOGONAMESTABLE_LINEDATA *prpLine,bool bpLastLine);
	bool PrintTableHeader(int ilPage,CUIntArray& ropColsPerPage);
	CCSPrint *pomPrint;
	
	// CBitmap omBitmap;
	CString omTableName;
	CString	omFileName;
};

#endif //__LOGONAMESTABLEVIEWER_H__
