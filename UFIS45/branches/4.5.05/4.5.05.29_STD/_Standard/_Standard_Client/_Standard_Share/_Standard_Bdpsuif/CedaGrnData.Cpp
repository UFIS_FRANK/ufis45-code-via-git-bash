// CedaGrnData.cpp
 
#include <stdafx.h>
#include <CedaGrnData.h>
#include <resource.h>

 
void ProcessGrnCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//--CEDADATA-----------------------------------------------------------------------------------------------

CedaGrnData::CedaGrnData() : CCSCedaData(&ogCommHandler)
{
    BEGIN_CEDARECINFO(GRNDATA, GrnDataRecInfo)
		FIELD_CHAR_TRIM	(Appl,"APPL")
		FIELD_DATE		(Cdat,"CDAT")
		FIELD_CHAR_TRIM	(Fldn,"FLDN")
		FIELD_CHAR_TRIM	(Grpn,"GRPN")
		FIELD_DATE		(Lstu,"LSTU")
		FIELD_CHAR_TRIM	(Prfl,"PRFL")
		FIELD_CHAR_TRIM	(Tabn,"TABN")
		FIELD_LONG		(Urno,"URNO")
		FIELD_CHAR_TRIM	(Usec,"USEC")
		FIELD_CHAR_TRIM	(Useu,"USEU")
		FIELD_CHAR_TRIM	(Grds,"GRDS")
		FIELD_CHAR_TRIM	(Grsn,"GRSN")
	END_CEDARECINFO

    // Copy the record structure
    for (int i = 0; i < sizeof(GrnDataRecInfo)/sizeof(GrnDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&GrnDataRecInfo[i],sizeof(CEDARECINFO));
        omRecInfo.Add(prpCedaRecInfo);
	}
    // Initialize table names and field names
    strcpy(pcmTableName,"GRN");
    sprintf(pcmListOfFields,"APPL,CDAT,FLDN,GRPN,LSTU,PRFL,TABN,URNO,USEC,USEU,GRDS,GRSN");

	pcmFieldList = pcmListOfFields; //because in CedaData pcmFieldList is just a pointer

	omData.RemoveAll();
}

//----------------------------------------------------------------------------------------------------

void CedaGrnData::GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType)
{
	ropFields.Add("APPL");
	ropFields.Add("CDAT");
	ropFields.Add("FLDN");
	ropFields.Add("GRDS");
	ropFields.Add("GRPN");
	ropFields.Add("GRSN");
	ropFields.Add("LSTU");
	ropFields.Add("PRFL");
	ropFields.Add("TABN");
	ropFields.Add("URNO");
	ropFields.Add("USEC");
	ropFields.Add("USEU");

	ropDesription.Add(LoadStg(IDS_STRING499));
	ropDesription.Add(LoadStg(IDS_STRING343));
	ropDesription.Add(LoadStg(IDS_STRING500));
	ropDesription.Add(LoadStg(IDS_STRING501));
	ropDesription.Add(LoadStg(IDS_STRING502));
	ropDesription.Add(LoadStg(IDS_STRING503));
	ropDesription.Add(LoadStg(IDS_STRING344));
	ropDesription.Add(LoadStg(IDS_STRING345));
	ropDesription.Add(LoadStg(IDS_STRING504));
	ropDesription.Add(LoadStg(IDS_STRING346));
	ropDesription.Add(LoadStg(IDS_STRING347));
	ropDesription.Add(LoadStg(IDS_STRING348));

	ropType.Add("String");
	ropType.Add("Date");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("Date");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");

}

//--REGISTER----------------------------------------------------------------------------------------------

void CedaGrnData::Register(void)
{
	ogDdx.Register((void *)this,BC_GRN_CHANGE,	CString("GRNDATA"), CString("Grn-changed"),	ProcessGrnCf);
	ogDdx.Register((void *)this,BC_GRN_NEW,		CString("GRNDATA"), CString("Grn-new"),		ProcessGrnCf);
	ogDdx.Register((void *)this,BC_GRN_DELETE,	CString("GRNDATA"), CString("Grn-deleted"),	ProcessGrnCf);
}

//--~CEDADATA----------------------------------------------------------------------------------------------

CedaGrnData::~CedaGrnData(void)
{
	omRecInfo.DeleteAll();
	ClearAll();
}

//--CLEAR-ALL----------------------------------------------------------------------------------------------

void CedaGrnData::ClearAll(bool bpWithRegistration)
{
    omUrnoMap.RemoveAll();

	CString olKey;
	CMapStringToPtr *polMap = NULL;

	for (POSITION pos = omGrpnMap.GetStartPosition(); pos != NULL;)
	{
		omGrpnMap.GetNextAssoc(pos,olKey,(void *&)polMap);
		if (polMap != NULL)
		{
			polMap->RemoveAll();
			delete polMap;
		}
	}
	omGrpnMap.RemoveAll();

    omData.DeleteAll();
	if(bpWithRegistration)
	{
		ogDdx.UnRegister(this,NOTUSED);
	}
}

//--READ-ALL-----------------------------------------------------------------------------------------------

bool CedaGrnData::Read(char *pspWhere /*NULL*/)
{
    // Select data from the database
	bool ilRc = true;
    omUrnoMap.RemoveAll();
	omGrpnMap.RemoveAll();
    omData.DeleteAll();
	if(pspWhere == NULL)
	{	
		ilRc = CedaAction("RT");
	}
	else
	{
		ilRc = CedaAction("RT", pspWhere);
	}
	if (ilRc != true)
	{
		return ilRc;
	}
	// Load data from CedaData into the dynamic array of record
	for (int ilLc = 0; ilRc == true; ilLc++)
	{
		GRNDATA *prlGrn = new GRNDATA;
		if ((ilRc = GetFirstBufferRecord(prlGrn)) == true)
		{
			omData.Add(prlGrn);//Update omData
			omUrnoMap.SetAt((void *)prlGrn->Urno,prlGrn);

			CMapStringToPtr *polMap;
			if (omGrpnMap.Lookup(prlGrn->Tabn,(void *&)polMap) == FALSE)
			{
				polMap = new CMapStringToPtr();
				omGrpnMap.SetAt(prlGrn->Tabn,polMap);
			}

			polMap->SetAt(prlGrn->Grpn,prlGrn);
		}
		else
		{
			delete prlGrn;
		}
	}
    return true;
}

//--INSERT-------------------------------------------------------------------------------------------------

bool CedaGrnData::Insert(GRNDATA *prpGrn)
{
	prpGrn->IsChanged = DATA_NEW;
	if(Save(prpGrn) == false) return false; //Update Database
	InsertInternal(prpGrn);
    return true;
}

//--INSERT-INTERNAL--------------------------------------------------------------------------------------

bool CedaGrnData::InsertInternal(GRNDATA *prpGrn)
{
	ogDdx.DataChanged((void *)this, GRN_NEW,(void *)prpGrn ); //Update Viewer
	omData.Add(prpGrn);//Update omData
	omUrnoMap.SetAt((void *)prpGrn->Urno,prpGrn);

	CMapStringToPtr *polMap;
	if (omGrpnMap.Lookup(prpGrn->Tabn,(void *&)polMap) == FALSE)
	{
		polMap = new CMapStringToPtr();
		omGrpnMap.SetAt(prpGrn->Tabn,polMap);
	}

	polMap->SetAt(prpGrn->Grpn,prpGrn);
    return true;
}

//--DELETE-------------------------------------------------------------------------------------------------

bool CedaGrnData::Delete(long lpUrno)
{
	GRNDATA *prlGrn = GetGrnByUrno(lpUrno);
	if (prlGrn != NULL)
	{
		prlGrn->IsChanged = DATA_DELETED;
		if(Save(prlGrn) == false) return false; //Update Database
		DeleteInternal(prlGrn);
	}
    return true;
}

//--DELETE-INTERNAL----------------------------------------------------------------------------------------

bool CedaGrnData::DeleteInternal(GRNDATA *prpGrn)
{
	ogDdx.DataChanged((void *)this,GRN_DELETE,(void *)prpGrn); //Update Viewer
	omUrnoMap.RemoveKey((void *)prpGrn->Urno);

	CMapStringToPtr *polMap = NULL;
	if (this->omGrpnMap.Lookup(prpGrn->Tabn,(void *&)polMap) == TRUE)
	{
		polMap->RemoveKey(prpGrn->Grpn);
		if (polMap->GetCount() == 0)
		{
			this->omGrpnMap.RemoveKey(prpGrn->Tabn);
			delete polMap;
		}
	}

	int ilGrnCount = omData.GetSize();
	for (int ilLc = 0; ilLc < ilGrnCount; ilLc++)
	{
		if (omData[ilLc].Urno == prpGrn->Urno)
		{
			omData.DeleteAt(ilLc);//Update omData
			break;
		}
	}
    return true;
}

//--UPDATE-------------------------------------------------------------------------------------------------

bool CedaGrnData::Update(GRNDATA *prpGrn)
{
	if (GetGrnByUrno(prpGrn->Urno) != NULL)
	{
		if (prpGrn->IsChanged == DATA_UNCHANGED)
		{
			prpGrn->IsChanged = DATA_CHANGED;
		}
		if(Save(prpGrn) == false) return false; //Update Database
		UpdateInternal(prpGrn);
	}
    return true;
}

//--UPDATE-INTERNAL--------------------------------------------------------------------------------------------

bool CedaGrnData::UpdateInternal(GRNDATA *prpGrn)
{
	GRNDATA *prlGrn = GetGrnByUrno(prpGrn->Urno);
	if (prlGrn != NULL)
	{

		CMapStringToPtr *polMap = NULL;
		if (this->omGrpnMap.Lookup(prlGrn->Tabn,(void *&)polMap) == TRUE)
		{	
			polMap->RemoveKey(prlGrn->Grpn);
			if (polMap->GetCount() == 0)
			{
				this->omGrpnMap.RemoveKey(prlGrn->Tabn);
				delete polMap;
			}
		}

		*prlGrn = *prpGrn; //Update omData

		if (omGrpnMap.Lookup(prlGrn->Tabn,(void *&)polMap) == FALSE)
		{
			polMap = new CMapStringToPtr();
			omGrpnMap.SetAt(prlGrn->Tabn,polMap);
		}

		polMap->SetAt(prlGrn->Grpn,prlGrn);

		ogDdx.DataChanged((void *)this,GRN_CHANGE,(void *)prlGrn); //Update Viewer
	}
    return true;
}

//--GET-BY-URNO--------------------------------------------------------------------------------------------

GRNDATA *CedaGrnData::GetGrnByUrno(long lpUrno)
{
	GRNDATA  *prlGrn;
	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlGrn) == TRUE)
	{
		return prlGrn;
	}
	return NULL;
}

//--GET-BY-GRPN--------------------------------------------------------------------------------------------

GRNDATA *CedaGrnData::GetGrnByGrpn(const CString& ropTabn,const CString& ropGrpn)
{
	CMapStringToPtr *polMap = NULL;
	if (this->omGrpnMap.Lookup(ropTabn,(void *&)polMap) == TRUE)
	{
		GRNDATA  *prlGrn;
		if (polMap->Lookup(ropGrpn,(void *& )prlGrn) == TRUE)
		{
			return prlGrn;
		}
	}
	return NULL;
}

//--READSPECIALDATA-------------------------------------------------------------------------------------

bool CedaGrnData::ReadSpecialData(CCSPtrArray<GRNDATA> *popGrn,char *pspWhere,char *pspFieldList,bool ipSYS/*=true*/)
{
	bool ilRc = true;
	char pclFieldList[256] = " ";

	if(strlen(pspFieldList) > 0) 
	{
		strcpy(pclFieldList, pspFieldList);
	}
	if(ipSYS == true) 
	{
		if (CedaAction("SYS","GRN",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	else
	{
		if (CedaAction("RT","GRN",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	if(popGrn != NULL)
	{
		for (int ilLc = 0; ilRc == true; ilLc++)
		{
			GRNDATA *prpGrn = new GRNDATA;
			if ((ilRc = GetBufferRecord(ilLc,prpGrn,CString(pclFieldList))) == true)
			{
				popGrn->Add(prpGrn);
			}
			else
			{
				delete prpGrn;
			}
		}
		if(popGrn->GetSize() == 0) return false;
	}
    return true;
}

//--SAVE---------------------------------------------------------------------------------------------------

bool CedaGrnData::Save(GRNDATA *prpGrn)
{
	bool ilRc = true;
	CString olListOfData;
	char pclSelection[124];
	char pclData[524];

	if (prpGrn->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}
	switch(prpGrn->IsChanged)
	{
	case DATA_NEW:
		MakeCedaData(&omRecInfo,olListOfData,prpGrn);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("IRT","","",pclData);
		prpGrn->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpGrn->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpGrn);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("URT",pclSelection,"",pclData);
		prpGrn->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_DELETED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpGrn->Urno);
		ilRc = CedaAction("DRT",pclSelection);
		break;
	}
    return ilRc;
}

//--Get sorted GRPN list-----------------------------------------------------------------------------------

CString CedaGrnData::GetSortedGrpnList(const CString& ropDelimiter)
{
	CString olResult;
	for (int i = 0; i < this->omData.GetSize(); i++)
	{
		olResult += CString(this->omData[i].Grpn) + ropDelimiter;		
	}

	return SortItemList(olResult,ropDelimiter[0]);
}

//--Get sorted GRPN list-----------------------------------------------------------------------------------
CString  CedaGrnData::GetSortedGrpnList(const CString& ropTable,const CString& ropDelimiter)
{
	CString olResult;
	for (int i = 0; i < this->omData.GetSize(); i++)
	{
		if (this->omData[i].Tabn == ropTable)
		{
			olResult += CString(this->omData[i].Grpn) + ropDelimiter;		
		}
	}

	return SortItemList(olResult,ropDelimiter[0]);
}

//--PROCESS-CF---------------------------------------------------------------------------------------------

void  ProcessGrnCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	ogGrnData.ProcessBc(ipDDXType,vpDataPointer,ropInstanceName);
}

//--PROCESS-BC---------------------------------------------------------------------------------------------

void  CedaGrnData::ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlGrnData;
	prlGrnData = (struct BcStruct *) vpDataPointer;
	GRNDATA *prlGrn;
	if(ipDDXType == BC_GRN_NEW)
	{
		prlGrn = new GRNDATA;
		GetRecordFromItemList(prlGrn,prlGrnData->Fields,prlGrnData->Data);
		InsertInternal(prlGrn);
	}
	if(ipDDXType == BC_GRN_CHANGE)
	{
		long llUrno = GetUrnoFromSelection(prlGrnData->Selection);
		prlGrn = GetGrnByUrno(llUrno);
		if(prlGrn != NULL)
		{
			GetRecordFromItemList(prlGrn,prlGrnData->Fields,prlGrnData->Data);
			UpdateInternal(prlGrn);
		}
	}
	if(ipDDXType == BC_GRN_DELETE)
	{
		long llUrno;
		CString olSelection = (CString)prlGrnData->Selection;
		if (olSelection.Find('\'') != -1)
		{
			llUrno = GetUrnoFromSelection(prlGrnData->Selection);
		}
		else
		{
			int ilFirst = olSelection.Find("=")+2;
			int ilLast  = olSelection.GetLength();
			llUrno = atol(olSelection.Mid(ilFirst,ilLast-ilFirst));
		}
		prlGrn = GetGrnByUrno(llUrno);
		if (prlGrn != NULL)
		{
			DeleteInternal(prlGrn);
		}
	}
}

//---------------------------------------------------------------------------------------------------------
