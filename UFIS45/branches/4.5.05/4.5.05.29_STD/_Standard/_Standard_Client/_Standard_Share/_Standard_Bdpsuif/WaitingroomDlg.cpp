// WaitingroomDlg.cpp : implementation file
//

#include <stdafx.h>
#include <BDPSUIF.h>
#include <WaitingroomDlg.h>
#include <CedaGATData.h>
#include <NotAvailableDlg.h>
#include <PrivList.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// WaitingroomDlg dialog


WaitingroomDlg::WaitingroomDlg(WRODATA *popWRO,CWnd* pParent /*=NULL*/) 
: CDialog(WaitingroomDlg::IDD, pParent)
{
	pomWRO = popWRO;
	pomTable = NULL;
	pomTable = new CCSTable;
	//{{AFX_DATA_INIT(WaitingroomDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}

WaitingroomDlg::WaitingroomDlg(WRODATA *popWRO,int ipDlg,CWnd* pParent /*=NULL*/) 
: CDialog(ipDlg, pParent)
{
	pomWRO = popWRO;
	pomTable = NULL;
	pomTable = new CCSTable;
	//{{AFX_DATA_INIT(WaitingroomDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}

WaitingroomDlg::~WaitingroomDlg()
{
	omBlkPtrA.DeleteAll();
	omDeleteBlkPtrA.DeleteAll();
	delete pomTable;

}

void WaitingroomDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(WaitingroomDlg)
	DDX_Control(pDX, IDC_DEFD, m_DEFD);
	DDX_Control(pDX, IDOK, m_OK);
	DDX_Control(pDX, IDC_NOAVNEW, m_NOAVNEW);
	DDX_Control(pDX, IDC_NOAVDEL, m_NOAVDEL);
	DDX_Control(pDX, IDC_NOAV,	 m_NOAVFRAME);
	DDX_Control(pDX, IDC_CDAT_D, m_CDATD);
	DDX_Control(pDX, IDC_CDAT_T, m_CDATT);
	DDX_Control(pDX, IDC_LSTU_D, m_LSTUD);
	DDX_Control(pDX, IDC_LSTU_T, m_LSTUT);
	DDX_Control(pDX, IDC_USEU,	 m_USEU);
	DDX_Control(pDX, IDC_USEC,	 m_USEC);
	DDX_Control(pDX, IDC_VAFR_D, m_VAFRD);
	DDX_Control(pDX, IDC_VAFR_T, m_VAFRT);
	DDX_Control(pDX, IDC_VATO_D, m_VATOD);
	DDX_Control(pDX, IDC_VATO_T, m_VATOT);
	DDX_Control(pDX, IDC_WNAM,	 m_WNAM);
	DDX_Control(pDX, IDC_GRUP,	 m_GRUP);
	DDX_Control(pDX, IDC_BRCA,	 m_BRCA);
	DDX_Control(pDX, IDC_GTE1,	 m_GTE1);
	DDX_Control(pDX, IDC_GTE2,	 m_GTE2);
	DDX_Control(pDX, IDC_TELE,	 m_TELE);
	DDX_Control(pDX, IDC_TERM,	 m_TERM);
	DDX_Control(pDX, IDC_HOME,	 m_HOME);
	DDX_Control(pDX, IDC_WROT,	 m_WROT);
	DDX_Control(pDX, IDC_MAXF,	 m_MAXF);
	DDX_Control(pDX, IDC_RADIO_UTC,	 m_UTC);
	DDX_Control(pDX, IDC_RADIO_LOCAL,m_LOCAL);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(WaitingroomDlg, CDialog)
	//{{AFX_MSG_MAP(WaitingroomDlg)
	ON_MESSAGE(WM_TABLE_LBUTTONDBLCLK,	OnTableLButtonDblclk)
	ON_BN_CLICKED(IDC_NOAVDEL, OnNoavDel)
	ON_BN_CLICKED(IDC_NOAVNEW, OnNoavNew)
	ON_BN_CLICKED(IDC_RADIO_UTC,	OnUTC)
	ON_BN_CLICKED(IDC_RADIO_LOCAL,	OnLocal)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// WaitingroomDlg message handlers
//----------------------------------------------------------------------------------------

BOOL WaitingroomDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	m_Caption = LoadStg(IDS_STRING195) + " <" + m_Caption + ">";
	SetWindowText(m_Caption);

	char clStat = ogPrivList.GetStat("WAITINGROOMDLG.m_OK");
	SetWndStatAll(clStat,m_OK);

	//------------------------------------
	m_CDATD.SetBKColor(SILVER);
	m_CDATD.SetInitText(pomWRO->Cdat.Format("%d.%m.%Y"));
	m_CDATD.SetSecState(ogPrivList.GetStat("WAITINGROOMDLG.m_CDAT"));
	m_CDATT.SetBKColor(SILVER);
	m_CDATT.SetInitText(pomWRO->Cdat.Format("%H:%M"));
	m_CDATT.SetSecState(ogPrivList.GetStat("WAITINGROOMDLG.m_CDAT"));
	//------------------------------------
	m_LSTUD.SetBKColor(SILVER);
	m_LSTUD.SetInitText(pomWRO->Lstu.Format("%d.%m.%Y"));
	m_LSTUD.SetSecState(ogPrivList.GetStat("WAITINGROOMDLG.m_LSTU"));
	m_LSTUT.SetBKColor(SILVER);
	m_LSTUT.SetInitText(pomWRO->Lstu.Format("%H:%M"));
	m_LSTUT.SetSecState(ogPrivList.GetStat("WAITINGROOMDLG.m_LSTU"));
	//------------------------------------
	m_USEC.SetBKColor(SILVER);
	m_USEC.SetInitText(pomWRO->Usec);
	m_USEC.SetSecState(ogPrivList.GetStat("WAITINGROOMDLG.m_USEC"));
	//------------------------------------
	m_USEU.SetBKColor(SILVER);
	m_USEU.SetInitText(pomWRO->Useu);
	m_USEU.SetSecState(ogPrivList.GetStat("WAITINGROOMDLG.m_USEU"));
	//------------------------------------
	m_GRUP.SetBKColor(SILVER);
	//m_GRUP.SetInitText(pomWRO->);
	m_GRUP.SetSecState(ogPrivList.GetStat("WAITINGROOMDLG.m_GRUP"));
	//------------------------------------//WHITE,YELLOW,SILVER,RED,LYELLOW
	m_WNAM.SetFormat("x|#x|#x|#x|#x|#");
	m_WNAM.SetTextLimit(1,5);
	m_WNAM.SetBKColor(YELLOW);
	m_WNAM.SetTextErrColor(RED);
	m_WNAM.SetInitText(pomWRO->Wnam);
	m_WNAM.SetSecState(ogPrivList.GetStat("WAITINGROOMDLG.m_WNAM"));
	//------------------------------------
	m_GTE1.SetFormat("x|#x|#x|#x|#x|#");
	m_GTE1.SetTextLimit(0,5);
	m_GTE1.SetTextErrColor(RED);
	m_GTE1.SetInitText(pomWRO->Gte1);
	m_GTE1.SetSecState(ogPrivList.GetStat("WAITINGROOMDLG.m_GTE1"));
	//------------------------------------
	m_GTE2.SetFormat("x|#x|#x|#x|#x|#");
	m_GTE2.SetTextLimit(0,5);
	m_GTE2.SetTextErrColor(RED);
	m_GTE2.SetInitText(pomWRO->Gte2);
	m_GTE2.SetSecState(ogPrivList.GetStat("WAITINGROOMDLG.m_GTE2"));
	//------------------------------------
	m_BRCA.SetTypeToInt(0,99999);
	m_BRCA.SetTextErrColor(RED);
	m_BRCA.SetInitText(pomWRO->Brca);
	m_BRCA.SetSecState(ogPrivList.GetStat("WAITINGROOMDLG.m_BRCA"));
	//------------------------------------
	m_TERM.SetFormat("x|#");
	m_TERM.SetTextLimit(0,1);
	m_TERM.SetTextErrColor(RED);
	m_TERM.SetInitText(pomWRO->Term);
	m_TERM.SetSecState(ogPrivList.GetStat("WAITINGROOMDLG.m_TERM"));
	//------------------------------------
	m_TELE.SetTypeToString("X(10)",10,0);
	m_TELE.SetTextErrColor(RED);
	m_TELE.SetInitText(pomWRO->Tele);
	m_TELE.SetSecState(ogPrivList.GetStat("WAITINGROOMDLG.m_TELE"));
	//------------------------------------
	m_HOME.SetTypeToString("X(4)",4,1);
	m_HOME.SetTextErrColor(RED);
	m_HOME.SetInitText(pomWRO->Home);
	m_HOME.SetSecState(ogPrivList.GetStat("WAITINGROOMDLG.m_HOME"));
	//------------------------------------
	m_WROT.SetTypeToString("X(2)",2,1);
	m_WROT.SetTextErrColor(RED);
	m_WROT.SetInitText(pomWRO->Wrot);
	m_WROT.SetSecState(ogPrivList.GetStat("WAITINGROOMDLG.m_WROT"));
	//------------------------------------
	m_VAFRD.SetTypeToDate(true);
	m_VAFRD.SetTextErrColor(RED);
	m_VAFRD.SetBKColor(YELLOW);
	m_VAFRD.SetInitText(pomWRO->Vafr.Format("%d.%m.%Y"));
	m_VAFRD.SetSecState(ogPrivList.GetStat("WAITINGROOMDLG.m_VAFR"));
	// - - - - - - - - - - - - - - - - - -
	m_VAFRT.SetTypeToTime(true);
	m_VAFRT.SetTextErrColor(RED);
	m_VAFRT.SetBKColor(YELLOW);
	m_VAFRT.SetInitText(pomWRO->Vafr.Format("%H:%M"));
	m_VAFRT.SetSecState(ogPrivList.GetStat("WAITINGROOMDLG.m_VAFR"));
	//------------------------------------
	m_VATOD.SetTypeToDate();
	m_VATOD.SetTextErrColor(RED);
	m_VATOD.SetInitText(pomWRO->Vato.Format("%d.%m.%Y"));
	m_VATOD.SetSecState(ogPrivList.GetStat("WAITINGROOMDLG.m_VATO"));
	// - - - - - - - - - - - - - - - - - -
	m_VATOT.SetTypeToTime();
	m_VATOT.SetTextErrColor(RED);
	m_VATOT.SetInitText(pomWRO->Vato.Format("%H:%M"));
	m_VATOT.SetSecState(ogPrivList.GetStat("WAITINGROOMDLG.m_VATO"));
	//------------------------------------
	m_DEFD.SetFormat("###");
	m_DEFD.SetTextLimit(0,3);
	m_DEFD.SetTextErrColor(RED);
	m_DEFD.SetInitText(pomWRO->Defd);
	m_DEFD.SetSecState(ogPrivList.GetStat("WAITINGROOMDLG.m_DEFD"));
	//------------------------------------
	m_MAXF.SetFormat("##");
	m_MAXF.SetTextLimit(0,2);
	m_MAXF.SetTextErrColor(RED);
	m_MAXF.SetInitText(pomWRO->Maxf);
	m_MAXF.SetSecState(ogPrivList.GetStat("WAITINGROOMDLG.m_MAXF"));
	
	if (ogBasicData.UseLocalTimeAsDefault())
	{
		this->m_UTC.SetCheck(0);
		this->m_LOCAL.SetCheck(1);
		imLastSelection = 2;
	}
	else
	{
		this->m_UTC.SetCheck(1);
		this->m_LOCAL.SetCheck(0);
		imLastSelection = 1;
	}

	//------------------------------------
	CString olUrno;
	olUrno.Format("%d",pomWRO->Urno);
	MakeNoavTable("WRO", olUrno);

	return TRUE;
}

//----------------------------------------------------------------------------------------

void WaitingroomDlg::OnOK() 
{
	AfxGetApp()->DoWaitCursor(1);
	CString olErrorText;
	bool ilStatus = true;
	if(m_WNAM.GetStatus() == false)
	{
		ilStatus = false;
		if(m_WNAM.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING288) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING288) + ogNotFormat;
		}
	}
	if(m_GTE1.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING377) + ogNotFormat;
	}
	if(m_GTE2.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING378) + ogNotFormat;
	}
	if(m_BRCA.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING239) + ogNotFormat;
	}
	if(m_TERM.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING296) + ogNotFormat;
	}
	if(m_TELE.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING294) + ogNotFormat;
	}
	if(m_VAFRD.GetStatus() == false)
	{
		ilStatus = false;
		if(m_VAFRD.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING232) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING232) + ogNotFormat;
		}
	}
	if(m_VAFRT.GetStatus() == false)
	{
		ilStatus = false;
		if(m_VAFRT.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING233) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING233) + ogNotFormat;
		}
	}
	if(m_VATOD.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING231) + LoadStg(IDS_STRING232) + ogNotFormat;
	}
	if(m_VATOT.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING231) + LoadStg(IDS_STRING233) + ogNotFormat;
	}

	if(m_DEFD.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING856) + ogNotFormat;
	}

	if(m_MAXF.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING855) + ogNotFormat;
	}


	///////////////////////////////////////////////////////////////////////////
	CString olVafrd,olVafrt,olVatod,olVatot;
	CTime olTmpTimeFr,olTmpTimeTo;
	m_VAFRD.GetWindowText(olVafrd);
	m_VAFRT.GetWindowText(olVafrt);
	m_VATOD.GetWindowText(olVatod);
	m_VATOT.GetWindowText(olVatot);

	if((m_VAFRD.GetWindowTextLength() != 0 && m_VAFRT.GetWindowTextLength() == 0) || (m_VAFRD.GetWindowTextLength() == 0 && m_VAFRT.GetWindowTextLength() != 0))
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING234);
	}
	if((m_VATOD.GetWindowTextLength() != 0 && m_VATOT.GetWindowTextLength() == 0) || (m_VATOD.GetWindowTextLength() == 0 && m_VATOT.GetWindowTextLength() != 0))
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING235);
	}
	if(m_VAFRD.GetStatus() == true && m_VAFRT.GetStatus() == true && m_VATOD.GetStatus() == true && m_VATOT.GetStatus() == true)
	{
		olTmpTimeFr = DateHourStringToDate(olVafrd,olVafrt);
		olTmpTimeTo = DateHourStringToDate(olVatod,olVatot);
		if(olTmpTimeTo != -1 && (olTmpTimeTo < olTmpTimeFr || olTmpTimeFr == -1))
		{
			ilStatus = false;
			olErrorText += LoadStg(IDS_STRING236);
		}
	}

	//Konsistenz-Check//
	if(ilStatus == true)
	{
		CString olGte1,olGte2,olWnam;
		char clWhere[100];
		CCSPtrArray<WRODATA> olWroCPA;

		m_WNAM.GetWindowText(olWnam);
		sprintf(clWhere,"WHERE WNAM='%s'",olWnam);
		if(ogWROData.ReadSpecialData(&olWroCPA,clWhere,"URNO,WNAM",false) == true)
		{
			if(olWroCPA[0].Urno != pomWRO->Urno)
			{
				ilStatus = false;
				olErrorText += LoadStg(IDS_STRING288) + LoadStg(IDS_EXIST);
			}
		}
		olWroCPA.DeleteAll();

		if(m_GTE1.GetWindowTextLength() != 0)
		{
			m_GTE1.GetWindowText(olGte1);
			sprintf(clWhere,"WHERE GNAM='%s'",olGte1);
			if(ogGATData.ReadSpecialData(NULL,clWhere,"GNAM",false) == false)
			{
				ilStatus = false;
				olErrorText += LoadStg(IDS_STRING379);
			}
		}

		if(m_GTE2.GetWindowTextLength() != 0)
		{
			m_GTE2.GetWindowText(olGte2);
			sprintf(clWhere,"WHERE GNAM='%s'",olGte2);
			if(ogGATData.ReadSpecialData(NULL,clWhere,"GNAM",false) == false)
			{
				ilStatus = false;
				olErrorText += LoadStg(IDS_STRING380);
			}
		}
	}

	///////////////////////////////////////////////////////////////////////////
	AfxGetApp()->DoWaitCursor(-1);
	if (ilStatus == true)
	{
		m_WNAM.GetWindowText(pomWRO->Wnam,6);
		m_GTE1.GetWindowText(pomWRO->Gte1,6);
		m_GTE2.GetWindowText(pomWRO->Gte2,6);
		m_BRCA.GetWindowText(pomWRO->Brca,6);
		m_TERM.GetWindowText(pomWRO->Term,2);
		m_TELE.GetWindowText(pomWRO->Tele,11);
		m_HOME.GetWindowText(pomWRO->Home,3);
		m_WROT.GetWindowText(pomWRO->Wrot,2);
		pomWRO->Vafr = DateHourStringToDate(olVafrd,olVafrt);
		pomWRO->Vato = DateHourStringToDate(olVatod,olVatot);
		m_DEFD.GetWindowText(pomWRO->Defd,4);
		m_MAXF.GetWindowText(pomWRO->Maxf,3);
		CDialog::OnOK();
	}
	else
	{
		Beep(440,70);
		MessageBox(olErrorText,LoadStg(IDS_STRING145),MB_ICONERROR);
		m_WNAM.SetFocus();
		m_WNAM.SetSel(0,-1);
	}
}

//----------------------------------------------------------------------------------------
void WaitingroomDlg::MakeNoavTable(CString opTabn, CString opBurn)
{
	CRect olRectBorder;
	m_NOAVFRAME.GetWindowRect( olRectBorder );
	ScreenToClient(olRectBorder);
	olRectBorder.DeflateRect(2,2);

    pomTable->SetTableData(this, olRectBorder.left, olRectBorder.right, olRectBorder.top, olRectBorder.bottom);
	pomTable->SetSelectMode(0);
	pomTable->SetShowSelection(TRUE);
	pomTable->ResetContent();

	CCSPtrArray <TABLE_HEADER_COLUMN> olHeaderDataArray;
	TABLE_HEADER_COLUMN rlHeader;
	rlHeader.Alignment = COLALIGN_LEFT;
	rlHeader.Font = &ogCourier_Regular_10;
	int ilPos = 1;
	rlHeader.Length = 133; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING101),ilPos++,true,'|');
	olHeaderDataArray.NewAt(olHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 133; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING101),ilPos++,true,'|');
	olHeaderDataArray.NewAt(olHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 58; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING101),ilPos++,true,'|');
	olHeaderDataArray.NewAt(olHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 42; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING101),ilPos++,true,'|');
	olHeaderDataArray.NewAt(olHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 42; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING101),ilPos++,true,'|');
	olHeaderDataArray.NewAt(olHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 8; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING101),ilPos++,true,'|');
	olHeaderDataArray.NewAt(olHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 332; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING101),ilPos++,true,'|');
	olHeaderDataArray.NewAt(olHeaderDataArray.GetSize(), rlHeader);

	pomTable->SetHeaderFields(olHeaderDataArray);
	pomTable->SetDefaultSeparator();
	olHeaderDataArray.DeleteAll();

	char clWhere[100];
	CCSPtrArray<BLKDATA> olBlkCPA;
	sprintf(clWhere,"WHERE TABN='%s' AND BURN='%s'",opTabn,opBurn);
	ogBlkData.ReadSpecialData(&omBlkPtrA,clWhere,"",false);
	UpdateNoavTable();
}

//----------------------------------------------------------------------------------------
void WaitingroomDlg::UpdateNoavTable()
{
	int ilLines = omBlkPtrA.GetSize();

	pomTable->ResetContent();
	pomTable->SetDefaultSeparator();
	pomTable->SetTableEditable(FALSE);

	TABLE_COLUMN rlColumnData;
	rlColumnData.Font = &ogCourier_Regular_10;
	rlColumnData.BkColor = WHITE;
	rlColumnData.Alignment = COLALIGN_LEFT;
	
    for (int ilLineNo = 0; ilLineNo < ilLines; ilLineNo++)
	{
		CCSPtrArray<TABLE_COLUMN> olColList;

		CTime olNafr = omBlkPtrA[ilLineNo].Nafr;
		CTime olNato = omBlkPtrA[ilLineNo].Nato;
		if (this->imLastSelection == 2)	// local
		{
			ogBasicData.UtcToLocal(olNafr);
			ogBasicData.UtcToLocal(olNato);
		}
		rlColumnData.Text = olNafr.Format("%d.%m.%Y %H:%M");
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = olNato.Format("%d.%m.%Y %H:%M");
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omBlkPtrA[ilLineNo].Days;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		char pclTmp[10]="";
		rlColumnData.Text = "";
		if(strlen(omBlkPtrA[ilLineNo].Tifr) > 0)
		{
			if (this->imLastSelection == 2)	// local
			{
				CString olText = omBlkPtrA[ilLineNo].Tifr;
				CTime olCurrent = CTime::GetCurrentTime();
				CTime olTifr(olCurrent.GetYear(),olCurrent.GetMonth(),olCurrent.GetDay(),atoi(olText.Left(2)),atoi(olText.Right(2)),0);

				ogBasicData.UtcToLocal(olTifr);
				rlColumnData.Text = olTifr.Format("%H:%M");
			}
			else
			{
				sprintf(pclTmp, "%s:%s", CString(omBlkPtrA[ilLineNo].Tifr).Left(2), CString(omBlkPtrA[ilLineNo].Tifr).Right(2));
				rlColumnData.Text = pclTmp;
			}
		}
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.Text = "";
		if(strlen(omBlkPtrA[ilLineNo].Tito) > 0)
		{
			if (this->imLastSelection == 2)	// local
			{
				CString olText = omBlkPtrA[ilLineNo].Tito;
				CTime olCurrent = CTime::GetCurrentTime();
				CTime olTito(olCurrent.GetYear(),olCurrent.GetMonth(),olCurrent.GetDay(),atoi(olText.Left(2)),atoi(olText.Right(2)),0);

				ogBasicData.UtcToLocal(olTito);
				rlColumnData.Text = olTito.Format("%H:%M");
			}
			else
			{
				sprintf(pclTmp, "%s:%s", CString(omBlkPtrA[ilLineNo].Tito).Left(2), CString(omBlkPtrA[ilLineNo].Tito).Right(2));
				rlColumnData.Text = pclTmp;
			}
		}
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		(*omBlkPtrA[ilLineNo].Type == 'X') ? rlColumnData.Text = cgYes : rlColumnData.Text = cgNo;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omBlkPtrA[ilLineNo].Resn;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		pomTable->AddTextLine(olColList,(void*)NULL);
		olColList.DeleteAll();
	}
	pomTable->DisplayTable();
}

//----------------------------------------------------------------------------------------

void WaitingroomDlg::ChangeNoavTable(BLKDATA *prpBlk, int ipLineNo)
{
	if(prpBlk != NULL)
	{
		TABLE_COLUMN rlColumn;
		CCSPtrArray <TABLE_COLUMN> olLine;

		rlColumn.Lineno = 0;
		rlColumn.VerticalSeparator = SEPA_NONE;
		rlColumn.SeparatorType = SEPA_NONE;
		rlColumn.BkColor = WHITE;
		rlColumn.Font = &ogCourier_Regular_10;
		rlColumn.Alignment = COLALIGN_LEFT;

		CTime olNafr = prpBlk->Nafr;
		CTime olNato = prpBlk->Nato;
		if (this->imLastSelection == 2)	// local
		{
			ogBasicData.UtcToLocal(olNafr);
			ogBasicData.UtcToLocal(olNato);
		}

		int ilColumnNo = 0;
		rlColumn.Text = olNafr.Format("%d.%m.%Y %H:%M");
		rlColumn.Columnno = ilColumnNo++;
		olLine.NewAt(olLine.GetSize(), rlColumn);
		rlColumn.Text = olNato.Format("%d.%m.%Y %H:%M"); 
		rlColumn.Columnno = ilColumnNo++;
		olLine.NewAt(olLine.GetSize(), rlColumn);
		rlColumn.Text = prpBlk->Days;
		rlColumn.Columnno = ilColumnNo++;
		olLine.NewAt(olLine.GetSize(), rlColumn);

		char pclTmp[10]="";
		rlColumn.Text = "";
		if(strlen(prpBlk->Tifr) > 0)
		{
			if (this->imLastSelection == 2)	// local
			{
				CString olText = prpBlk->Tifr;
				CTime olCurrent = CTime::GetCurrentTime();
				CTime olTifr(olCurrent.GetYear(),olCurrent.GetMonth(),olCurrent.GetDay(),atoi(olText.Left(2)),atoi(olText.Right(2)),0);

				ogBasicData.UtcToLocal(olTifr);
				rlColumn.Text = olTifr.Format("%H:%M");
			}
			else
			{
				sprintf(pclTmp, "%s:%s", CString(prpBlk->Tifr).Left(2), CString(prpBlk->Tifr).Right(2));
				rlColumn.Text = pclTmp;
			}
		}
		rlColumn.Columnno = ilColumnNo++;
		olLine.NewAt(olLine.GetSize(), rlColumn);

		rlColumn.Text = "";
		if(strlen(prpBlk->Tito) > 0)
		{
			if (this->imLastSelection == 2)	// local
			{
				CString olText = prpBlk->Tito;
				CTime olCurrent = CTime::GetCurrentTime();
				CTime olTito(olCurrent.GetYear(),olCurrent.GetMonth(),olCurrent.GetDay(),atoi(olText.Left(2)),atoi(olText.Right(2)),0);

				ogBasicData.UtcToLocal(olTito);
				rlColumn.Text = olTito.Format("%H:%M");
			}
			else
			{
				sprintf(pclTmp, "%s:%s", CString(prpBlk->Tito).Left(2), CString(prpBlk->Tito).Right(2));
				rlColumn.Text = pclTmp;
			}
		}
		rlColumn.Columnno = ilColumnNo++;
		olLine.NewAt(olLine.GetSize(), rlColumn);

		(*prpBlk->Type == 'X') ? rlColumn.Text = cgYes : rlColumn.Text = cgNo;
		rlColumn.Columnno = ilColumnNo++;
		olLine.NewAt(olLine.GetSize(), rlColumn);
		rlColumn.Text = prpBlk->Resn;
		rlColumn.Columnno = ilColumnNo++;
		olLine.NewAt(olLine.GetSize(), rlColumn);

		if(ipLineNo>-1)
		{
			pomTable->ChangeTextLine(ipLineNo, &olLine, (void *)prpBlk);
			pomTable->DisplayTable();
		}
		else
		{
			if(prpBlk != NULL)
			{
				pomTable->AddTextLine(olLine, (void *)prpBlk);
				pomTable->DisplayTable();
			}
		}
		olLine.DeleteAll();
	}
	else
	{
		if(ipLineNo>-1)
		{
			pomTable->DeleteTextLine(ipLineNo);
			pomTable->DisplayTable();
		}
	}
}

//----------------------------------------------------------------------------------------

LONG WaitingroomDlg::OnTableLButtonDblclk(UINT wParam, LONG lParam) 
{
	if(ogPrivList.GetStat("WAITINGROOMDLG.m_NOAVCHA") == '1')
	{
		int ilLineNo = pomTable->GetCurSel();
		if (ilLineNo == -1)
		{
			MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONSTOP);
		}
		else
		{
			BLKDATA *prlBlk = &omBlkPtrA[ilLineNo];
			if (prlBlk != NULL)
			{
				BLKDATA rlBlk = *prlBlk;
				NotAvailableDlg *polDlg = new NotAvailableDlg(&rlBlk,this);
				m_WNAM.GetWindowText(polDlg->omName);
				if(polDlg->DoModal() == IDOK)
				{
					*prlBlk = rlBlk;
					if(prlBlk->IsChanged != DATA_NEW) prlBlk->IsChanged = DATA_CHANGED;
					ChangeNoavTable(prlBlk, ilLineNo);
				}
				delete polDlg;
			}
		}
	}
	return 0L;
}

//----------------------------------------------------------------------------------------

void WaitingroomDlg::OnNoavNew() 
{
	BLKDATA rlBlk;

	if(ogPrivList.GetStat("WAITINGROOMDLG.m_NOAVNEW") == '1')
	{
		NotAvailableDlg *polDlg = new NotAvailableDlg(&rlBlk,this);
		m_WNAM.GetWindowText(polDlg->omName);
		if(polDlg->DoModal() == IDOK)
		{
			BLKDATA *prlBlk = new BLKDATA;
			*prlBlk = rlBlk;
			omBlkPtrA.Add(prlBlk);
			prlBlk->IsChanged = DATA_NEW;
			ChangeNoavTable(prlBlk, -1);
		}
		delete polDlg;
	}
}

//----------------------------------------------------------------------------------------

void WaitingroomDlg::OnNoavDel() 
{
	int ilLineNo = pomTable->GetCurSel();
	if (ilLineNo == -1)
	{
		MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONSTOP);
	}
	else
	{
		if(ogPrivList.GetStat("WAITINGROOMDLG.m_NOAVDEL") == '1')
		{
			if (&omBlkPtrA[ilLineNo] != NULL)
			{
				BLKDATA *prlBlk = new BLKDATA;
				*prlBlk = omBlkPtrA[ilLineNo];
				prlBlk->IsChanged = DATA_DELETED;
				omDeleteBlkPtrA.Add(prlBlk);
				omBlkPtrA.DeleteAt(ilLineNo);
				ChangeNoavTable(NULL, ilLineNo);
			}
		}
	}
}

//----------------------------------------------------------------------------------------
void WaitingroomDlg::OnUTC() 
{
	if (imLastSelection == 1)
		return;

	imLastSelection = 1;

	UpdateNoavTable();
}

//----------------------------------------------------------------------------------------
void WaitingroomDlg::OnLocal() 
{
	if (imLastSelection == 2)
		return;

	imLastSelection = 2;

	UpdateNoavTable();
}
