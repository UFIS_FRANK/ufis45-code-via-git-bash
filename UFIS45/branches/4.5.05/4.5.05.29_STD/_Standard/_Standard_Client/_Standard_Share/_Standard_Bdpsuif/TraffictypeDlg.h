#if !defined(AFX_TRAFFICTYPEDLG_H__B35519F2_3320_11D1_B39D_0000C016B067__INCLUDED_)
#define AFX_TRAFFICTYPEDLG_H__B35519F2_3320_11D1_B39D_0000C016B067__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// TraffictypeDlg.h : header file
//
#include <CedaNATData.h>
#include <CCSEdit.h>

/////////////////////////////////////////////////////////////////////////////
// TraffictypeDlg dialog

class TraffictypeDlg : public CDialog
{
// Construction
public:
	TraffictypeDlg(NATDATA *popNAT,CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(TraffictypeDlg)
	enum { IDD = IDD_TRAFFICTYPEDLG };
	CButton	m_OK;
	CString	m_Caption;
	CCSEdit	m_CDATD;
	CCSEdit	m_CDATT;
	CCSEdit	m_LSTUD;
	CCSEdit	m_LSTUT;
	CCSEdit	m_USEU;
	CCSEdit	m_USEC;
	CCSEdit	m_TTYP;
	CCSEdit	m_TNAM;
	CCSEdit	m_VAFRD;
	CCSEdit	m_VAFRT;
	CCSEdit	m_VATOD;
	CCSEdit	m_VATOT;
	CCSEdit m_STYP;
	CCSEdit m_FLTI;
	CCSEdit m_APC4;
	CCSEdit m_ALC3;

	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(TraffictypeDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(TraffictypeDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:

	NATDATA *pomNAT;

private:
	void ResizeWindow();
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TRAFFICTYPEDLG_H__B35519F2_3320_11D1_B39D_0000C016B067__INCLUDED_)
