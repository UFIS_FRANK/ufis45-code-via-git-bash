// CedaAfmData.cpp
 
#include <stdafx.h>
#include <CedaAfmData.h>
#include <resource.h>


void ProcessAfmCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//--CEDADATA-----------------------------------------------------------------------------------------------

CedaAfmData::CedaAfmData() : CCSCedaData(&ogCommHandler)
{
	// Create an array of CEDARECINFO for AFMDataStruct
	BEGIN_CEDARECINFO(AFMDATA,AFMDataRecInfo)
		FIELD_DATE		(Cdat,"CDAT")
		FIELD_CHAR_TRIM	(Afmc,"AFMC")
		FIELD_CHAR_TRIM	(Anam,"ANAM")
		FIELD_DATE		(Lstu,"LSTU")
		FIELD_LONG		(Urno,"URNO")
		FIELD_CHAR_TRIM	(Usec,"USEC")
		FIELD_CHAR_TRIM	(Useu,"USEU")

	END_CEDARECINFO //(AFMDataStruct)

	// FIELD_LONG, FIELD_DATE 
	// Copy the record structure
	for (int i=0; i< sizeof(AFMDataRecInfo)/sizeof(AFMDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&AFMDataRecInfo[i],sizeof(CEDARECINFO));
		omRecInfo.Add(prpCedaRecInfo);
	} // end for

	// initialize field names
	strcpy(pcmTableName,"AFM");
	strcpy(pcmListOfFields,"CDAT,AFMC,ANAM,LSTU,URNO,USEC,USEU");
	pcmFieldList = pcmListOfFields; //because in CedaData pcmFieldList is just a pointer

	omData.RemoveAll();
}

//---------------------------------------------------------------------------------------------------------

void CedaAfmData::GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType)
{
	ropFields.Add("CDAT");
	ropFields.Add("AFMC");
	ropFields.Add("ANAM");
	ropFields.Add("LSTU");
	ropFields.Add("URNO");
	ropFields.Add("USEC");
	ropFields.Add("USEU");

	ropDesription.Add(LoadStg(IDS_STRING343));
	ropDesription.Add(LoadStg(IDS_STRING630));
	ropDesription.Add(LoadStg(IDS_STRING631));
	ropDesription.Add(LoadStg(IDS_STRING344));
	ropDesription.Add(LoadStg(IDS_STRING346));
	ropDesription.Add(LoadStg(IDS_STRING347));
	ropDesription.Add(LoadStg(IDS_STRING348));

	ropType.Add("Date");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("Date");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");

}

//--REGISTER----------------------------------------------------------------------------------------------

void CedaAfmData::Register(void)
{
	ogDdx.Register((void *)this,BC_AFM_CHANGE,	CString("AFMDATA"), CString("Afm-changed"),	ProcessAfmCf);
	ogDdx.Register((void *)this,BC_AFM_NEW,		CString("AFMDATA"), CString("Afm-new"),		ProcessAfmCf);
	ogDdx.Register((void *)this,BC_AFM_DELETE,	CString("AFMDATA"), CString("Afm-deleted"),	ProcessAfmCf);
}

//--~CEDADATA----------------------------------------------------------------------------------------------

CedaAfmData::~CedaAfmData(void)
{
	omRecInfo.DeleteAll();
	ClearAll();
}

//--CLEAR-ALL----------------------------------------------------------------------------------------------

void CedaAfmData::ClearAll(bool bpWithRegistration)
{
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(bpWithRegistration)
	{
		ogDdx.UnRegister(this,NOTUSED);
	}
}

//--READ-ALL-----------------------------------------------------------------------------------------------

bool CedaAfmData::Read(char *pspWhere /*NULL*/)
{
    // Select data from the database
	bool ilRc = true;
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(pspWhere == NULL)
	{	
		ilRc = CedaAction("RT");
	}
	else
	{
		ilRc = CedaAction("RT", pspWhere);
	}
	if (ilRc != true)
	{
		return ilRc;
	}
	// Load data from CedaData into the dynamic array of record
	for (int ilLc = 0; ilRc == true; ilLc++)
	{
		AFMDATA *prlAfm = new AFMDATA;
		if ((ilRc = GetFirstBufferRecord(prlAfm)) == true)
		{
			omData.Add(prlAfm);//Update omData
			omUrnoMap.SetAt((void *)prlAfm->Urno,prlAfm);
		}
		else
		{
			delete prlAfm;
		}
	}
    return true;
}

//--INSERT-------------------------------------------------------------------------------------------------

bool CedaAfmData::Insert(AFMDATA *prpAfm)
{
	prpAfm->IsChanged = DATA_NEW;
	if(Save(prpAfm) == false) return false; //Update Database
	InsertInternal(prpAfm);
    return true;
}

//--INSERT-INTERNAL--------------------------------------------------------------------------------------

bool CedaAfmData::InsertInternal(AFMDATA *prpAfm)
{
	ogDdx.DataChanged((void *)this, AFM_NEW,(void *)prpAfm ); //Update Viewer
	omData.Add(prpAfm);//Update omData
	omUrnoMap.SetAt((void *)prpAfm->Urno,prpAfm);
    return true;
}

//--DELETE-------------------------------------------------------------------------------------------------

bool CedaAfmData::Delete(long lpUrno)
{
	AFMDATA *prlAfm = GetAfmByUrno(lpUrno);
	if (prlAfm != NULL)
	{
		prlAfm->IsChanged = DATA_DELETED;
		if(Save(prlAfm) == false) return false; //Update Database
		DeleteInternal(prlAfm);
	}
    return true;
}

//--DELETE-INTERNAL----------------------------------------------------------------------------------------

bool CedaAfmData::DeleteInternal(AFMDATA *prpAfm)
{
	ogDdx.DataChanged((void *)this,AFM_DELETE,(void *)prpAfm); //Update Viewer
	omUrnoMap.RemoveKey((void *)prpAfm->Urno);
	int ilAfmCount = omData.GetSize();
	for (int ilLc = 0; ilLc < ilAfmCount; ilLc++)
	{
		if (omData[ilLc].Urno == prpAfm->Urno)
		{
			omData.DeleteAt(ilLc);//Update omData
			break;
		}
	}
    return true;
}

//--UPDATE-------------------------------------------------------------------------------------------------

bool CedaAfmData::Update(AFMDATA *prpAfm)
{
	if (GetAfmByUrno(prpAfm->Urno) != NULL)
	{
		if (prpAfm->IsChanged == DATA_UNCHANGED)
		{
			prpAfm->IsChanged = DATA_CHANGED;
		}
		if(Save(prpAfm) == false) return false; //Update Database
		UpdateInternal(prpAfm);
	}
    return true;
}

//--UPDATE-INTERNAL--------------------------------------------------------------------------------------------

bool CedaAfmData::UpdateInternal(AFMDATA *prpAfm)
{
	AFMDATA *prlAfm = GetAfmByUrno(prpAfm->Urno);
	if (prlAfm != NULL)
	{
		*prlAfm = *prpAfm; //Update omData
		ogDdx.DataChanged((void *)this,AFM_CHANGE,(void *)prlAfm); //Update Viewer
	}
    return true;
}

//--GET-BY-URNO--------------------------------------------------------------------------------------------

AFMDATA *CedaAfmData::GetAfmByUrno(long lpUrno)
{
	AFMDATA  *prlAfm;
	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlAfm) == TRUE)
	{
		return prlAfm;
	}
	return NULL;
}

//--READSPECIALDATA-------------------------------------------------------------------------------------

bool CedaAfmData::ReadSpecialData(CCSPtrArray<AFMDATA> *popAfm,char *pspWhere,char *pspFieldList,bool ipSYS/*=true*/)
{
	bool ilRc = true;
	char pclFieldList[256] = " ";

	if(strlen(pspFieldList) > 0) 
	{
		strcpy(pclFieldList, pspFieldList);
	}
	if(ipSYS == true) 
	{
		if (CedaAction("SYS","AFM",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	else
	{
		if (CedaAction("RT","AFM",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	if(popAfm != NULL)
	{
		for (int ilLc = 0; ilRc == true; ilLc++)
		{
			AFMDATA *prpAfm = new AFMDATA;
			if ((ilRc = GetBufferRecord(ilLc,prpAfm,CString(pclFieldList))) == true)
			{
				popAfm->Add(prpAfm);
			}
			else
			{
				delete prpAfm;
			}
		}
		if(popAfm->GetSize() == 0) return false;
	}
    return true;
}

//--SAVE---------------------------------------------------------------------------------------------------

bool CedaAfmData::Save(AFMDATA *prpAfm)
{
	bool ilRc = true;
	CString olListOfData;
	char pclSelection[124];
	char pclData[524];

	if (prpAfm->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}
	switch(prpAfm->IsChanged)
	{
	case DATA_NEW:
		MakeCedaData(&omRecInfo,olListOfData,prpAfm);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("IRT","","",pclData);
		prpAfm->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpAfm->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpAfm);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("URT",pclSelection,"",pclData);
		prpAfm->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_DELETED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpAfm->Urno);
		ilRc = CedaAction("DRT",pclSelection);
		break;
	}
    return ilRc;
}

//--PROCESS-CF---------------------------------------------------------------------------------------------

void  ProcessAfmCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	ogAFMData.ProcessBc(ipDDXType,vpDataPointer,ropInstanceName);
}

//--PROCESS-BC---------------------------------------------------------------------------------------------

void  CedaAfmData::ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlAfmData;
	prlAfmData = (struct BcStruct *) vpDataPointer;
	AFMDATA *prlAfm;
	if(ipDDXType == BC_AFM_NEW)
	{
		prlAfm = new AFMDATA;
		GetRecordFromItemList(prlAfm,prlAfmData->Fields,prlAfmData->Data);
		if(ValidateAfmBcData(prlAfm->Urno)) //Prf: 8795
		{
			InsertInternal(prlAfm);
		}
		else
		{
			delete prlAfm;
		}
	}
	if(ipDDXType == BC_AFM_CHANGE)
	{
		long llUrno = GetUrnoFromSelection(prlAfmData->Selection);
		prlAfm = GetAfmByUrno(llUrno);
		if(prlAfm != NULL)
		{
			GetRecordFromItemList(prlAfm,prlAfmData->Fields,prlAfmData->Data);
			if(ValidateAfmBcData(prlAfm->Urno)) //Prf: 8795
			{
				UpdateInternal(prlAfm);
			}
			else
			{
				DeleteInternal(prlAfm);
			}
		}
	}
	if(ipDDXType == BC_AFM_DELETE)
	{
		long llUrno = GetUrnoFromSelection(prlAfmData->Selection);

		prlAfm = GetAfmByUrno(llUrno);
		if (prlAfm != NULL)
		{
			DeleteInternal(prlAfm);
		}
	}
}

//Prf: 8795
//--ValidateAfmBcData--------------------------------------------------------------------------------------

bool CedaAfmData::ValidateAfmBcData(const long& lrpUrno)
{
	bool blValidateAfmBcData = true;
	if(!omWhere.IsEmpty()) //It means it is not the default view and we need to check in the database, since we don't need to check for the default view
	{
		char chWhere[20];
		sprintf(chWhere, "URNO='%ld'",lrpUrno);
		CString olWhere = omWhere + CString(" AND ") + CString(chWhere);
		CCSPtrArray<AFMDATA> olAfms;
		if(!ReadSpecialData(&olAfms,olWhere.GetBuffer(0),"URNO",false))
		{		
			blValidateAfmBcData = false;
		}
	}
	return blValidateAfmBcData;
}

//---------------------------------------------------------------------------------------------------------
