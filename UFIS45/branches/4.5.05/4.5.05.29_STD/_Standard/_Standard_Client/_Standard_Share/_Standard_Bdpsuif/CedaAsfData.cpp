// CedaAsfData.cpp
 
#include <stdafx.h>
#include <CedaAsfData.h>
#include <resource.h>


void ProcessAsfCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//--CEDADATA-----------------------------------------------------------------------------------------------

CedaAsfData::CedaAsfData() : CCSCedaData(&ogCommHandler)
{
	// Create an array of CEDARECINFO for ASFDATA
	BEGIN_CEDARECINFO(ASFDATA,AsfDataRecInfo)
		FIELD_CHAR_TRIM	(Bewc,"BEWC")
		FIELD_CHAR_TRIM	(Bewf,"BEWF")
		FIELD_CHAR_TRIM	(Bewn,"BEWN")
		FIELD_DATE		(Cdat,"CDAT")
		FIELD_DATE		(Lstu,"LSTU")
		FIELD_CHAR_TRIM	(Prfl,"PRFL")
		FIELD_CHAR_TRIM	(Rema,"REMA")
		FIELD_LONG		(Urno,"URNO")
		FIELD_CHAR_TRIM	(Usec,"USEC")
		FIELD_CHAR_TRIM	(Useu,"USEU")

	END_CEDARECINFO //(ASFDATA)

	// Copy the record structure
	for (int i=0; i< sizeof(AsfDataRecInfo)/sizeof(AsfDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&AsfDataRecInfo[i],sizeof(CEDARECINFO));
		omRecInfo.Add(prpCedaRecInfo);
	} // end for

	// initialize field names
	strcpy(pcmTableName,"ASF");
	strcpy(pcmListOfFields,"BEWC,BEWF,BEWN,CDAT,LSTU,PRFL,REMA,URNO,USEC,USEU");
	pcmFieldList = pcmListOfFields; //because in CedaData pcmFieldList is just a pointer

	omData.RemoveAll();
}

//---------------------------------------------------------------------------------------------------------

void CedaAsfData::GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType)
{
	ropFields.Add("BEWC");
	ropFields.Add("BEWF");
	ropFields.Add("BEWN");
	ropFields.Add("CDAT");
	ropFields.Add("LSTU");
	ropFields.Add("PRFL");
	ropFields.Add("REMA");
	ropFields.Add("URNO");
	ropFields.Add("USEC");
	ropFields.Add("USEU");

	ropDesription.Add(LoadStg(IDS_STRING237));
	ropDesription.Add(LoadStg(IDS_STRING313));
	ropDesription.Add(LoadStg(IDS_STRING229));
	ropDesription.Add(LoadStg(IDS_STRING343));
	ropDesription.Add(LoadStg(IDS_STRING344));
	ropDesription.Add(LoadStg(IDS_STRING345));
	ropDesription.Add(LoadStg(IDS_STRING238));
	ropDesription.Add(LoadStg(IDS_STRING346));
	ropDesription.Add(LoadStg(IDS_STRING347));
	ropDesription.Add(LoadStg(IDS_STRING348));

	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("Date");
	ropType.Add("Date");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");

}

//--REGISTER----------------------------------------------------------------------------------------------

void CedaAsfData::Register(void)
{
	ogDdx.Register((void *)this,BC_ASF_CHANGE,	CString("ASFDATA"), CString("Asf-changed"),	ProcessAsfCf);
	ogDdx.Register((void *)this,BC_ASF_NEW,		CString("ASFDATA"), CString("Asf-new"),		ProcessAsfCf);
	ogDdx.Register((void *)this,BC_ASF_DELETE,	CString("ASFDATA"), CString("Asf-deleted"),	ProcessAsfCf);
}

//--~CEDADATA----------------------------------------------------------------------------------------------

CedaAsfData::~CedaAsfData(void)
{
	omRecInfo.DeleteAll();
	ClearAll();
}

//--CLEAR-ALL----------------------------------------------------------------------------------------------

void CedaAsfData::ClearAll(bool bpWithRegistration)
{
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(bpWithRegistration)
	{
		ogDdx.UnRegister(this,NOTUSED);
	}
}

//--READ-ALL-----------------------------------------------------------------------------------------------

bool CedaAsfData::Read(char *pspWhere /*NULL*/)
{
    // Select data from the database
	bool ilRc = true;
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(pspWhere == NULL)
	{	
		ilRc = CedaAction("RT");
	}
	else
	{
		ilRc = CedaAction("RT", pspWhere);
	}
	if (ilRc != true)
	{
		return ilRc;
	}
	// Load data from CedaData into the dynamic array of record
	for (int ilLc = 0; ilRc == true; ilLc++)
	{
		ASFDATA *prlAsf = new ASFDATA;
		if ((ilRc = GetFirstBufferRecord(prlAsf)) == true)
		{
			omData.Add(prlAsf);//Update omData
			omUrnoMap.SetAt((void *)prlAsf->Urno,prlAsf);
		}
		else
		{
			delete prlAsf;
		}
	}
    return true;
}

//--INSERT-------------------------------------------------------------------------------------------------

bool CedaAsfData::Insert(ASFDATA *prpAsf)
{
	prpAsf->IsChanged = DATA_NEW;
	if(Save(prpAsf) == false) return false; //Update Database
	InsertInternal(prpAsf);
    return true;
}

//--INSERT-INTERNAL--------------------------------------------------------------------------------------

bool CedaAsfData::InsertInternal(ASFDATA *prpAsf)
{
	ogDdx.DataChanged((void *)this, ASF_NEW,(void *)prpAsf ); //Update Viewer
	omData.Add(prpAsf);//Update omData
	omUrnoMap.SetAt((void *)prpAsf->Urno,prpAsf);
    return true;
}

//--DELETE-------------------------------------------------------------------------------------------------

bool CedaAsfData::Delete(long lpUrno)
{
	ASFDATA *prlAsf = GetAsfByUrno(lpUrno);
	if (prlAsf != NULL)
	{
		prlAsf->IsChanged = DATA_DELETED;
		if(Save(prlAsf) == false) return false; //Update Database
		DeleteInternal(prlAsf);
	}
    return true;
}

//--DELETE-INTERNAL----------------------------------------------------------------------------------------

bool CedaAsfData::DeleteInternal(ASFDATA *prpAsf)
{
	ogDdx.DataChanged((void *)this,ASF_DELETE,(void *)prpAsf); //Update Viewer
	omUrnoMap.RemoveKey((void *)prpAsf->Urno);
	int ilAsfCount = omData.GetSize();
	for (int ilLc = 0; ilLc < ilAsfCount; ilLc++)
	{
		if (omData[ilLc].Urno == prpAsf->Urno)
		{
			omData.DeleteAt(ilLc);//Update omData
			break;
		}
	}
    return true;
}

//--UPDATE-------------------------------------------------------------------------------------------------

bool CedaAsfData::Update(ASFDATA *prpAsf)
{
	if (GetAsfByUrno(prpAsf->Urno) != NULL)
	{
		if (prpAsf->IsChanged == DATA_UNCHANGED)
		{
			prpAsf->IsChanged = DATA_CHANGED;
		}
		if(Save(prpAsf) == false) return false; //Update Database
		UpdateInternal(prpAsf);
	}
    return true;
}

//--UPDATE-INTERNAL--------------------------------------------------------------------------------------------

bool CedaAsfData::UpdateInternal(ASFDATA *prpAsf)
{
	ASFDATA *prlAsf = GetAsfByUrno(prpAsf->Urno);
	if (prlAsf != NULL)
	{
		*prlAsf = *prpAsf; //Update omData
		ogDdx.DataChanged((void *)this,ASF_CHANGE,(void *)prlAsf); //Update Viewer
	}
    return true;
}

//--GET-BY-URNO--------------------------------------------------------------------------------------------

ASFDATA *CedaAsfData::GetAsfByUrno(long lpUrno)
{
	ASFDATA  *prlAsf;
	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlAsf) == TRUE)
	{
		return prlAsf;
	}
	return NULL;
}

//--READSPECIALDATA-------------------------------------------------------------------------------------

bool CedaAsfData::ReadSpecialData(CCSPtrArray<ASFDATA> *popAsf,char *pspWhere,char *pspFieldList,bool ipSYS/*=true*/)
{
	bool ilRc = true;
	char pclFieldList[256] = " ";

	if(strlen(pspFieldList) > 0) 
	{
		strcpy(pclFieldList, pspFieldList);
	}
	if(ipSYS == true) 
	{
		if (CedaAction("SYS","ASF",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	else
	{
		if (CedaAction("RT","ASF",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	if(popAsf != NULL)
	{
		for (int ilLc = 0; ilRc == true; ilLc++)
		{
			ASFDATA *prpAsf = new ASFDATA;
			if ((ilRc = GetBufferRecord(ilLc,prpAsf,CString(pclFieldList))) == true)
			{
				popAsf->Add(prpAsf);
			}
			else
			{
				delete prpAsf;
			}
		}
		if(popAsf->GetSize() == 0) return false;
	}
    return true;
}

//--SAVE---------------------------------------------------------------------------------------------------

bool CedaAsfData::Save(ASFDATA *prpAsf)
{
	bool ilRc = true;
	CString olListOfData;
	char pclSelection[124];
	char pclData[524];

	if (prpAsf->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}
	switch(prpAsf->IsChanged)
	{
	case DATA_NEW:
		MakeCedaData(&omRecInfo,olListOfData,prpAsf);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("IRT","","",pclData);
		prpAsf->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpAsf->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpAsf);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("URT",pclSelection,"",pclData);
		prpAsf->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_DELETED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpAsf->Urno);
		ilRc = CedaAction("DRT",pclSelection);
		break;
	}
    return ilRc;
}

//--PROCESS-CF---------------------------------------------------------------------------------------------

void  ProcessAsfCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	ogAsfData.ProcessBc(ipDDXType,vpDataPointer,ropInstanceName);
}

//--PROCESS-BC---------------------------------------------------------------------------------------------

void  CedaAsfData::ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlAsfData;
	prlAsfData = (struct BcStruct *) vpDataPointer;
	ASFDATA *prlAsf;
	if(ipDDXType == BC_ASF_NEW)
	{
		prlAsf = new ASFDATA;
		GetRecordFromItemList(prlAsf,prlAsfData->Fields,prlAsfData->Data);
		if(ValidateAsfBcData(prlAsf->Urno)) //Prf: 8795
		{
			InsertInternal(prlAsf);
		}
		else
		{
			delete prlAsf;
		}
	}
	if(ipDDXType == BC_ASF_CHANGE)
	{
		long llUrno = GetUrnoFromSelection(prlAsfData->Selection);
		prlAsf = GetAsfByUrno(llUrno);
		if(prlAsf != NULL)
		{
			GetRecordFromItemList(prlAsf,prlAsfData->Fields,prlAsfData->Data);
			if(ValidateAsfBcData(prlAsf->Urno)) //Prf: 8795
			{
				UpdateInternal(prlAsf);
			}
			else
			{
				DeleteInternal(prlAsf);
			}
		}
	}
	if(ipDDXType == BC_ASF_DELETE)
	{
		long llUrno = GetUrnoFromSelection(prlAsfData->Selection);

		prlAsf = GetAsfByUrno(llUrno);
		if (prlAsf != NULL)
		{
			DeleteInternal(prlAsf);
		}
	}
}

//Prf: 8795
//--ValidateAsfBcData--------------------------------------------------------------------------------------

bool CedaAsfData::ValidateAsfBcData(const long& lrpUrno)
{
	bool blValidateAsfBcData = true;
	if(!omWhere.IsEmpty()) //It means it is not the default view and we need to check in the database, since we don't need to check for the default view
	{
		char chWhere[20];
		sprintf(chWhere, "URNO='%ld'",lrpUrno);
		CString olWhere = omWhere + CString(" AND ") + CString(chWhere);
		CCSPtrArray<ASFDATA> olAsfs;
		if(!ReadSpecialData(&olAsfs,olWhere.GetBuffer(0),"URNO",false))
		{		
			blValidateAsfBcData = false;
		}
	}
	return blValidateAsfBcData;
}

//---------------------------------------------------------------------------------------------------------
