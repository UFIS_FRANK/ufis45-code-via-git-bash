// STSDlg.cpp : implementation file
//

#include <stdafx.h>
#include <bdpsuif.h>
#include <STSDlg.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSTSDlg dialog


CSTSDlg::CSTSDlg(STSDATA *popSts, CWnd* pParent /*=NULL*/) : CDialog(CSTSDlg::IDD, pParent)
{
	pomSts = popSts;
	//{{AFX_DATA_INIT(CSTSDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CSTSDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSTSDlg)
	DDX_Control(pDX, IDOK, m_OK);
	DDX_Control(pDX, IDC_CDAT_D, m_CDATD);
	DDX_Control(pDX, IDC_CDAT_T, m_CDATT);
	DDX_Control(pDX, IDC_STSC, m_STSC);
	DDX_Control(pDX, IDC_STSD, m_STSD);
	DDX_Control(pDX, IDC_REMA, m_REMA);
	DDX_Control(pDX, IDC_LSTU_D, m_LSTUD);
	DDX_Control(pDX, IDC_LSTU_T, m_LSTUT);
	DDX_Control(pDX, IDC_USEC, m_USEC);
	DDX_Control(pDX, IDC_USEU, m_USEU);
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CSTSDlg, CDialog)
	//{{AFX_MSG_MAP(CSTSDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSTSDlg message handlers

BOOL CSTSDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	m_Caption = LoadStg(IDS_STRING765) + " <" + m_Caption + ">";
	SetWindowText(m_Caption);
	char clStat = ogPrivList.GetStat("STSDLG.m_OK");
	SetWndStatAll(clStat,m_OK);

	//------------------------------------
	m_CDATD.SetBKColor(SILVER);
	m_CDATD.SetInitText(pomSts->Cdat.Format("%d.%m.%Y"));
	// - - - - - - - - - - - - - - - - - -
	m_CDATT.SetBKColor(SILVER);
	m_CDATT.SetInitText(pomSts->Cdat.Format("%H:%M"));
	//------------------------------------
	m_LSTUD.SetBKColor(SILVER);
	m_LSTUD.SetInitText(pomSts->Lstu.Format("%d.%m.%Y"));
	// - - - - - - - - - - - - - - - - - -
	m_LSTUT.SetBKColor(SILVER);
	m_LSTUT.SetInitText(pomSts->Lstu.Format("%H:%M"));
	//------------------------------------
	m_USEC.SetBKColor(SILVER);
	m_USEC.SetInitText(pomSts->Usec);
	//------------------------------------
	m_USEU.SetBKColor(SILVER);
	m_USEU.SetInitText(pomSts->Useu);
	//------------------------------------//WHITE,YELLOW,SILVER,RED,LYELLOW
	m_STSD.SetTypeToString("X(40)",40,0);
	m_STSD.SetTextErrColor(RED);
	m_STSD.SetInitText(pomSts->Stsd);
	//m_Temp.SetSecState(ogPrivList.GetStat("AIRPORTWETDLG.m_TEMP"));
	//------------------------------------
	m_STSC.SetFormat("x|#x|#");
	m_STSC.SetTextLimit(1,2);
	m_STSC.SetBKColor(YELLOW);  
	m_STSC.SetTextErrColor(RED);
	m_STSC.SetInitText(pomSts->Stsc);
	//------------------------------------
	m_REMA.SetTypeToString("X(60)",60,0);
	m_REMA.SetTextErrColor(RED);
	m_REMA.SetInitText(pomSts->Rema);
	//m_Rema.SetSecState(ogPrivList.GetStat("AIRPORTWETDLG.m_WDIR"));
	//------------------------------------

	return TRUE;  
}

void CSTSDlg::OnOK() 
{
	AfxGetApp()->DoWaitCursor(1);
	CString olErrorText;
	bool ilStatus = true;
	CString olTmp;
	if(m_STSC.GetStatus() == false)
	{
		ilStatus = false;
		if(m_STSC.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING237) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING237) + ogNotFormat;
		}
	}
	if(m_STSD.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING501) + ogNotFormat;//+ CString(" 2") 
	}
	if(m_REMA.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING805) + ogNotFormat;//+ CString(" 2") 
	}
	///////////////////////////////////////////////////////////////////////////

	//Konsistenz-Check//
	if(ilStatus == true)
	{
		CString olText;//
		if(m_STSC.GetWindowTextLength() != 0)
		{
			CCSPtrArray<STSDATA> olStsCPA;
			char clWhere[100];
			m_STSC.GetWindowText(olText);
			sprintf(clWhere,"WHERE STSC='%s'",olText);
			if(ogStsData.ReadSpecialData(&olStsCPA,clWhere,"URNO",false) == true)
			{
				if(olStsCPA[0].Urno != pomSts->Urno)
				{
					ilStatus = false;
					olErrorText += LoadStg(IDS_STRING237) + LoadStg(IDS_EXIST);
				}
			}
			olStsCPA.DeleteAll();
		}
	}
	///////////////////////////////////////////////////////////////////////////
	AfxGetApp()->DoWaitCursor(-1);
	if(ilStatus == true)
	{
		m_STSC.GetWindowText(pomSts->Stsc,2+2);
		//wandelt Return in Blank//
		CString olTemp;
		m_STSD.GetWindowText(olTemp);
		for(int i=0;i>-1;)
		{
			i = olTemp.Find("\r\n");
			if(i != -1)
			{
				if(olTemp.GetLength() > (i+2))
					olTemp = olTemp.Left(i) + " " + olTemp.Mid(i+2);
				else
					olTemp = olTemp.Left(i);
			}
		}
		strcpy(pomSts->Stsd,olTemp);

		//wandelt Return in Blank//
		m_REMA.GetWindowText(olTemp);
		for(i=0;i>-1;)
		{
			i = olTemp.Find("\r\n");
			if(i != -1)
			{
				if(olTemp.GetLength() > (i+2))
					olTemp = olTemp.Left(i) + " " + olTemp.Mid(i+2);
				else
					olTemp = olTemp.Left(i);
			}
		}
		strcpy(pomSts->Rema,olTemp);

		CDialog::OnOK();
	}
	else
	{
		Beep(440,70);
		MessageBox(olErrorText,LoadStg(IDS_STRING145),MB_ICONERROR);
		m_STSC.SetFocus();
		m_STSC.SetSel(0,-1);
	}
}
	

