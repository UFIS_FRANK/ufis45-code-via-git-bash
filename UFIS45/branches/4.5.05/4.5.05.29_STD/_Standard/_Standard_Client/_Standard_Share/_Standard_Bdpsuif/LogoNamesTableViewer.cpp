// LogoNamesTableViewer.cpp
//

#include <stdafx.h>
#include <LogoNamesTableViewer.h>
#include <CcsDdx.h>
#include <resource.h>
#include <fstream.h>
#include <iomanip.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif


// Local function prototype
static void LogoNamesTableCf(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//-----------------------------------------------------------------------------------------------
// LogoNamesTableViewer
//

LogoNamesTableViewer::LogoNamesTableViewer(CCSPtrArray<FLGDATA> *popData)
{
	pomData = popData;
	bmIsFromSearch = FALSE;
    pomLogoNamesTable = NULL;
    ogDdx.Register(this, FLG_CHANGE, CString("LOGONAMESTABLEVIEWER"), CString("LogoNames Update/new"), LogoNamesTableCf);
    ogDdx.Register(this, FLG_DELETE, CString("LOGONAMESTABLEVIEWER"), CString("LogoNames Delete"), LogoNamesTableCf);
}

//-----------------------------------------------------------------------------------------------

LogoNamesTableViewer::~LogoNamesTableViewer()
{
	ogDdx.UnRegister(this, NOTUSED);
    DeleteAll();
	omHeaderDataArray.DeleteAll();
}

//-----------------------------------------------------------------------------------------------

void LogoNamesTableViewer::Attach(CCSTable *popTable)
{
    pomLogoNamesTable = popTable;
}

//-----------------------------------------------------------------------------------------------

void LogoNamesTableViewer::ChangeViewTo(const char *pcpViewName)
{
    DeleteAll();
    MakeLines();
	UpdateLogoNames();
}

//-----------------------------------------------------------------------------------------------

void LogoNamesTableViewer::MakeLines()
{
	int ilLogoNamesCount = pomData->GetSize();
	for (int ilLc = 0; ilLc < ilLogoNamesCount; ilLc++)
	{
		FLGDATA *prlLogoNamesData = &pomData->GetAt(ilLc);
		MakeLine(prlLogoNamesData);
	}
}

//-----------------------------------------------------------------------------------------------

void LogoNamesTableViewer::MakeLine(FLGDATA *prpLogoNames)
{

    // Update viewer data for this shift record
    LOGONAMESTABLE_LINEDATA rlLogoNames;

	rlLogoNames.Urno = prpLogoNames->Urno;
	rlLogoNames.Lgfn = prpLogoNames->Lgfn;
	rlLogoNames.Lgsn = prpLogoNames->Lgsn;
	rlLogoNames.Type = prpLogoNames->Type;
	rlLogoNames.Alc2 = prpLogoNames->Alc2;
	rlLogoNames.Alc3 = prpLogoNames->Alc3;
	CreateLine(&rlLogoNames);
}

//-----------------------------------------------------------------------------------------------

void LogoNamesTableViewer::CreateLine(LOGONAMESTABLE_LINEDATA *prpLogoNames)
{
    int ilLineCount = omLines.GetSize();
    for (int ilLineno = ilLineCount; ilLineno > 0; ilLineno--)
        if (CompareLogoNames(prpLogoNames, &omLines[ilLineno-1]) >= 0)
	        break;							 // should be inserted after Lines[ilLineno-1]
	LOGONAMESTABLE_LINEDATA rlLogoNames;
	rlLogoNames = *prpLogoNames;
    omLines.NewAt(ilLineno, rlLogoNames);
}

//-----------------------------------------------------------------------------------------------
// UpdateLogoNames: Load data selected by filter conditions to the LogoNames by using "omTable"

void LogoNamesTableViewer::UpdateLogoNames()
{
	int ilLines = omLines.GetSize();
	const int ilColumns = 3;
	omHeaderDataArray.DeleteAll();

	pomLogoNamesTable->SetShowSelection(TRUE);
	pomLogoNamesTable->ResetContent();

	TABLE_HEADER_COLUMN rlHeader;
	rlHeader.Alignment = COLALIGN_LEFT;
	rlHeader.Font = &ogCourier_Regular_10;

	CString olHeader(LoadStg(IDS_STRING1146));

	int ilPos = 1;
	rlHeader.Length = 100;
	rlHeader.Text = GetListItem(olHeader,ilPos++,false,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader); //Logo Name

	rlHeader.Length = 400;
	rlHeader.Text = GetListItem(olHeader,ilPos++,false,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);	// Logo Description

	rlHeader.Length = 50;
	rlHeader.Text = GetListItem(olHeader,ilPos++,false,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);	// Type

	rlHeader.Length = 90;
	rlHeader.Text = GetListItem(olHeader,ilPos++,false,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);	// Alc2

	rlHeader.Length = 90;
	rlHeader.Text = GetListItem(olHeader,ilPos++,false,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);	// Alc3

	pomLogoNamesTable->SetHeaderFields(omHeaderDataArray);
	pomLogoNamesTable->SetDefaultSeparator();
	pomLogoNamesTable->SetTableEditable(FALSE);

	TABLE_COLUMN rlColumnData;
	rlColumnData.Font = &ogCourier_Regular_10;
	rlColumnData.BkColor = WHITE;
	rlColumnData.Alignment = COLALIGN_LEFT;

    for (int ilLineNo = 0; ilLineNo < ilLines; ilLineNo++)
	{
		CCSPtrArray<TABLE_COLUMN> olColList;

		rlColumnData.Text = omLines[ilLineNo].Lgsn;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Lgfn;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Type;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Alc2;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Alc3;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		pomLogoNamesTable->AddTextLine(olColList,(void*)NULL);
		olColList.DeleteAll();
	}
	pomLogoNamesTable->DisplayTable();
}

//-----------------------------------------------------------------------------------------------

static void LogoNamesTableCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName)
{
    LogoNamesTableViewer *polViewer = (LogoNamesTableViewer *)popInstance;
    if (ipDDXType == FLG_CHANGE)
		polViewer->ProcessLogoNamesChange((FLGDATA *)vpDataPointer);
    else if (ipDDXType == FLG_DELETE)
		polViewer->ProcessLogoNamesDelete((FLGDATA *)vpDataPointer);
}


//-----------------------------------------------------------------------------------------------

void LogoNamesTableViewer::ProcessLogoNamesChange(FLGDATA *prpLogoNames)
{
	int ilItem = 0;
	int ilColumnNo = 0;
	TABLE_COLUMN rlColumn;
	CCSPtrArray <TABLE_COLUMN> olLine;

	rlColumn.Lineno = ilItem;
	rlColumn.VerticalSeparator = SEPA_NONE;
	rlColumn.SeparatorType = SEPA_NONE;
	rlColumn.BkColor = WHITE;
	rlColumn.Font = &ogCourier_Regular_10;
	rlColumn.Alignment = COLALIGN_LEFT;

	rlColumn.Text = prpLogoNames->Lgsn;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpLogoNames->Lgfn;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpLogoNames->Type;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpLogoNames->Alc2;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpLogoNames->Alc3;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);

	if (FindLine(prpLogoNames->Urno, ilItem))
	{
        LOGONAMESTABLE_LINEDATA *prlLine = &omLines[ilItem];
		prlLine->Urno = prpLogoNames->Urno;
		prlLine->Lgsn = prpLogoNames->Lgsn;
		prlLine->Lgfn = prpLogoNames->Lgfn;
		prlLine->Type = prpLogoNames->Type;
		prlLine->Alc2 = prpLogoNames->Alc2;
		prlLine->Alc3 = prpLogoNames->Alc3;
		pomLogoNamesTable->ChangeTextLine(ilItem, &olLine, (void *)prlLine);
		pomLogoNamesTable->DisplayTable();
	}
	else
	{
		MakeLine(prpLogoNames);
		if (FindLine(prpLogoNames->Urno, ilItem))
		{
	        LOGONAMESTABLE_LINEDATA *prlLine = &omLines[ilItem];
			if(prlLine != NULL)
			{
				pomLogoNamesTable->AddTextLine(olLine, (void *)prlLine);
				pomLogoNamesTable->DisplayTable();
			}
		}
	}
	olLine.DeleteAll();
}

//-----------------------------------------------------------------------------------------------

void LogoNamesTableViewer::ProcessLogoNamesDelete(FLGDATA *prpLogoNames)
{
	int ilItem;
	if (FindLine(prpLogoNames->Urno, ilItem))
	{
		DeleteLine(ilItem);
		pomLogoNamesTable->DeleteTextLine(ilItem);
		pomLogoNamesTable->DisplayTable();
	}
}

//-----------------------------------------------------------------------------------------------

int LogoNamesTableViewer::CompareLogoNames(LOGONAMESTABLE_LINEDATA *prpLogoNames1, LOGONAMESTABLE_LINEDATA *prpLogoNames2)
{
	return 0;
}

//-----------------------------------------------------------------------------------------------

void LogoNamesTableViewer::DeleteLine(int ipLineno)
{
	omLines.DeleteAt(ipLineno);
}

//-----------------------------------------------------------------------------------------------

bool LogoNamesTableViewer::FindLine(long lpUrno, int &rilLineno)
{
    for (rilLineno = 0; rilLineno < omLines.GetSize(); rilLineno++)
	{
        if (omLines[rilLineno].Urno == lpUrno)
            return true;
	}
    return false;
}
//-----------------------------------------------------------------------------------------------

CString LogoNamesTableViewer::PrepareDisplayDateFormat(const CString& ropDate
															 ,bool blSetDefaultIncaseEmptyDate) const
{
	CString olReturnDateString;
	if(ropDate.GetLength() == 0 && blSetDefaultIncaseEmptyDate == true)
	{
		return CTime::GetCurrentTime().Format("%d.%m.%Y");
	}
	else if(ropDate.GetLength() > 0)
	{
		olReturnDateString = ropDate.Mid(6,2) + "." + ropDate.Mid(4,2) + "." + ropDate.Mid(0,4);
	}
	return olReturnDateString;
}
//-----------------------------------------------------------------------------------------------

void LogoNamesTableViewer::DeleteAll()
{
    while (omLines.GetSize() > 0)
        DeleteLine(0);
}

//-----------------------------------------------------------------------------------------------
// Printing Routines
//-----------------------------------------------------------------------------------------------

void LogoNamesTableViewer::PrintTableView()
{
	CString olFooter1,olFooter2;
	omTableName = LoadStg(IDS_STRING1147);
	int ilOrientation = PRINT_LANDSCAPE;
	pomPrint = new CCSPrint((CWnd*)this,ilOrientation,45);
	if (pomPrint != NULL)
	{
		if (pomPrint->InitializePrinter(ilOrientation) == TRUE)
		{
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			DOCINFO	rlDocInfo;
			memset(&rlDocInfo, 0, sizeof(DOCINFO));
			rlDocInfo.cbSize = sizeof( DOCINFO );
			rlDocInfo.lpszDocName = omTableName;

//			calculate count of pages
			CUIntArray olColsPerPage;
			int ilPages = pomPrint->CalculateCountOfPages(this->omHeaderDataArray,olColsPerPage);

			pomPrint->omCdc.StartDoc( &rlDocInfo );
			for (int ilPage = 0; ilPage < ilPages; ilPage++)
			{
				pomPrint->imPageNo = 0;
				int ilLines = omLines.GetSize();
				pomPrint->imLineNo = pomPrint->imMaxLines + 1;
				olFooter1.Format("%d %s",ilLines,omTableName);
				for(int i = 0; i < ilLines; i++ )
				{
					if(pomPrint->imLineNo >= pomPrint->imMaxLines)
					{
						if(pomPrint->imPageNo > 0)
						{
							olFooter2.Format("%s: %d",LoadStg(IDS_STRING222),pomPrint->imPageNo);
							pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
							pomPrint->omCdc.EndPage();
						}
						PrintTableHeader(ilPage,olColsPerPage);
					}
					if(pomPrint->imLineNo == (pomPrint->imMaxLines-1) || i == (ilLines-1))
					{
						PrintTableLine(ilPage,olColsPerPage,&omLines[i],true);
					}
					else
					{
						PrintTableLine(ilPage,olColsPerPage,&omLines[i],false);
					}
				}
				olFooter2.Format("%s: %d",LoadStg(IDS_STRING222),pomPrint->imPageNo);
				pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
				pomPrint->omCdc.EndPage();
			}
			pomPrint->omCdc.EndDoc();
		}
		SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
		delete pomPrint;
		pomPrint = NULL;
	}
}

//-----------------------------------------------------------------------------------------------

bool LogoNamesTableViewer::PrintTableHeader(int ilPage,CUIntArray& ropColsPerPage)
{
	pomPrint->omCdc.StartPage();
	pomPrint->imPageNo++;
	pomPrint->imLineNo = 0;

	pomPrint->PrintUIFHeader(omTableName,CString(CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")),pomPrint->imFirstLine-10);

	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;

	rlElement.Alignment   = PRINT_LEFT;
	rlElement.FrameLeft   = PRINT_NOFRAME;
	rlElement.FrameRight  = PRINT_NOFRAME;
	rlElement.FrameTop    = PRINT_NOFRAME;
	rlElement.FrameBottom = PRINT_FRAMEMEDIUM;
	rlElement.pFont       = &pomPrint->ogCourierNew_Bold_8;

	int ilSize = omHeaderDataArray.GetSize();
	for(int i=0;i<ilSize;i++)
	{

		rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
		if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength;
		rlElement.Text   = omHeaderDataArray[i].Text;
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();


	return true;
}

//-----------------------------------------------------------------------------------------------

bool LogoNamesTableViewer::PrintTableLine(int ilPage,CUIntArray& ropColsPerPage,LOGONAMESTABLE_LINEDATA *prpLine,bool bpLastLine)
{
	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;
	rlElement.pFont      = &pomPrint->ogCourierNew_Regular_8;
	int ilSize = omHeaderDataArray.GetSize();
	for(int i=0;i<ilSize;i++)
	{

		rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
		if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength;
		rlElement.Alignment  = PRINT_LEFT;
		rlElement.FrameTop   = PRINT_FRAMETHIN;
		if(bpLastLine == true)
		{
			rlElement.FrameBottom = PRINT_FRAMETHIN;
		}
		else
		{
			rlElement.FrameBottom = PRINT_NOFRAME;
		}
		rlElement.FrameLeft  = PRINT_NOFRAME;
		rlElement.FrameRight = PRINT_NOFRAME;
		switch(i)
		{
		case 0:
			{
				rlElement.FrameLeft  = PRINT_FRAMETHIN;
				rlElement.Text		= prpLine->Lgsn;
			}
			break;
		case 1:
			{
				rlElement.Text		= prpLine->Lgfn;
			}
			break;
		case 2:
			{
				rlElement.Text		= prpLine->Type;
			}
			break;
		case 3:
			{
				rlElement.Text		= prpLine->Alc2;
			}
			break;
		case 4:
			{
				rlElement.Text		= prpLine->Alc3;
			}
			break;
		default :
			break;
		}
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();
	return true;
}

//-----------------------------------------------------------------------------------------------
/*bool LogoNamesTableViewer::CreateExcelFile(const CString& opTrenner,const CString& ropListSeparator)
{
	ofstream of;

	CString olTmpPath = CCSLog::GetTmpPath();

	CString olTableName;
	olTableName.Format("%s -   %s", "FLGTAB", (CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")) );


	CString olFileName = olTableName;
	olFileName.Remove('*');
	olFileName.Remove('.');
	olFileName.Remove(':');
	olFileName.Remove('/');
	olFileName.Replace(" ", "_");

	char pHeader[256];
	strcpy (pHeader, olTmpPath);
	CString path = pHeader;
	omFileName =  path + "\\" + olFileName + ".csv";

	of.open( omFileName, ios::out);


	of  << setw(0) << "FLGTAB" << "     "  << CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")  << endl;

	of	<< setw(0) << LoadStg(IDS_STRING1151) //CString("Logo Name");
		<< setw(0) << opTrenner
		<< setw(0) << LoadStg(IDS_STRING1141) //CString("Logo File Description");
		<< setw(0) << opTrenner
		<< setw(0) << LoadStg(IDS_STRING1143) //CString("Logo Type");
		<< setw(0) << opTrenner
		<< setw(0) << LoadStg(IDS_STRING1144) //CString("Airline 2-Letter Code");
		<< setw(0) << opTrenner
		<< setw(0) << LoadStg(IDS_STRING1145) //CString("Airline 3-Letter Code");
		;

	int ilCount = omLines.GetSize();
	COleDateTime olCurrTime = COleDateTime::GetCurrentTime();

	for(int i = 0; i < ilCount; i++)
	{
		LOGONAMESTABLE_LINEDATA rlLine = omLines[i];
		of.setf(ios::left, ios::adjustfield);

		of   << setw(0) << rlLine.Lgsn						    // Logo Name
		     << setw(0) << opTrenner
		     << setw(0) << rlLine.Lgfn							// Logo Description
		     << setw(0) << opTrenner
		     << setw(0) << rlLine.Type							// Logo Type
		     << setw(0) << opTrenner
		     << setw(0) << rlLine.Alc2							// Airline - 2 Letter code
		     << setw(0) << opTrenner
			 << setw(0) << rlLine.Alc3							// Airline - 3 Letter code
			 ;
	    of	<< endl;

	}

	of.close();
	return true;
}*/
//-----------------------------------------------------------------------------------------------