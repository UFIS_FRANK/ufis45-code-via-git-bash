// CedaOrgData.h

#ifndef __CEDAORGDATA__
#define __CEDAORGDATA__
 
#include <stdafx.h>
#include <basicdata.h>
#include <resrc1.h>		// main symbols#

//---------------------------------------------------------------------------------------------------------

struct ORGDATA 
{
	CTime 	 Cdat; 		// Erstellungsdatum
	char 	 Dpt1[8+2]; 	// Code
	char 	 Dpt2[8+2]; 	// Übergeordnete Einheit. Code
	char 	 Dptn[42]; 	// Bezeichnung
	CTime 	 Lstu;		// Datum letzte Änderung
	char 	 Prfl[3]; 	// Protokollierungskennzeichen
	char 	 Rema[62]; 	// Bemerkung
	long 	 Urno;		// Eindeutige Datensatz-Nr.
	char 	 Usec[34]; 	// Anwender (Ersteller)
	char 	 Useu[34]; 	// Anwender (letzte Änderung)

	char 	 Odgl[5]; 	// durchschnittliche Ganztourenlänge
	char 	 Odkl[5]; 	// durchschnittliche Kurztourenlänge
	char 	 Odsl[5]; 	// durchschnittliche Kurslänge
	//DataCreated by this class
	int		 IsChanged;	// Check whether Data has Changed für Relaese

	ORGDATA(void)
	{ 
		memset(this,'\0',sizeof(*this));
		IsChanged	=	DATA_UNCHANGED;
		Cdat		=	TIMENULL;
		Lstu		=	TIMENULL;
	}

}; // end OrgDataStrukt

//---------------------------------------------------------------------------------------------------------
// Class declaratino

class CedaOrgData: public CCSCedaData
{
// Attributes
public:
    CMapPtrToPtr omUrnoMap;

    CCSPtrArray<ORGDATA> omData;

	char pcmListOfFields[2048];

// Operations
public:
    CedaOrgData();
	~CedaOrgData();
	virtual void GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType);
	void Register(void);
	void ClearAll(bool bpWithRegistration = true);
    bool Read(char *pspWhere = NULL);
	bool Insert(ORGDATA *prpOrg);
	bool InsertInternal(ORGDATA *prpOrg);
	bool Update(ORGDATA *prpOrg);
	bool UpdateInternal(ORGDATA *prpOrg);
	bool Delete(long lpUrno);
	bool DeleteInternal(ORGDATA *prpOrg);
	bool ReadSpecialData(CCSPtrArray<ORGDATA> *popOrg,char *pspWhere,char *pspFieldList,bool ipSYS=true);
	bool Save(ORGDATA *prpOrg);
	void ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
	ORGDATA  *GetOrgByUrno(long lpUrno);

	void GetAllCodes(CStringArray &olList);
	void SetWhere(const CString& ropWhere){omWhere = ropWhere;} //Prf: 8795
	void GetWhere(CString& ropWhere) const{ropWhere = omWhere;} //Prf: 8795

	// Private methods
private:
	bool SaveODGData();
	int im_UrnoPos;
	int	im_HopoPos;
	int	im_OdgcPos;
	int	im_OdgnPos;
	int	im_OrguPos;
	int	im_RemaPos;
	int ReadODG();
    void PrepareOrgData(ORGDATA *prpOrgData);
	CString omWhere; //Prf: 8795
	bool ValidateOrgBcData(const long& lrpUnro); //Prf: 8795

};

//---------------------------------------------------------------------------------------------------------


#endif //__CEDAORGDATA__
