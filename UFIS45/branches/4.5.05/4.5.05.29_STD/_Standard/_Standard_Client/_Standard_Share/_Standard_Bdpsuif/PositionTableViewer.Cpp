// PositionTableViewer.cpp 
//

#include <stdafx.h>
#include <PositionTableViewer.h>
#include <CcsDdx.h>
#include <resource.h>
#include <fstream.h>
#include <iomanip.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif 


// Local function prototype
static void PositionTableCf(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//-----------------------------------------------------------------------------------------------
// PositionTableViewer
//

PositionTableViewer::PositionTableViewer(CCSPtrArray<PSTDATA> *popData)
{
	pomData = popData;

	bmIsFromSearch = FALSE;
    pomPositionTable = NULL;
    ogDdx.Register(this, PST_CHANGE, CString("POSITIONTABLEVIEWER"), CString("Position Update/new"), PositionTableCf);
    ogDdx.Register(this, PST_DELETE, CString("POSITIONTABLEVIEWER"), CString("Position Delete"), PositionTableCf);
	if (ogBasicData.IsGatPosEnabled())
	{
		ogDdx.Register(this, SGM_NEW,	 CString("POSITIONTABLEVIEWER"), CString("SGM New"   ), PositionTableCf);
		ogDdx.Register(this, SGM_CHANGE, CString("POSITIONTABLEVIEWER"), CString("SGM Update"), PositionTableCf);
		ogDdx.Register(this, SGM_DELETE, CString("POSITIONTABLEVIEWER"), CString("SGM Delete"), PositionTableCf);
	}
}

//-----------------------------------------------------------------------------------------------

PositionTableViewer::~PositionTableViewer()
{
	ogDdx.UnRegister(this, NOTUSED);
    DeleteAll();
	omHeaderDataArray.DeleteAll();
}

//-----------------------------------------------------------------------------------------------

void PositionTableViewer::Attach(CCSTable *popTable)
{
    pomPositionTable = popTable;
}

//-----------------------------------------------------------------------------------------------

void PositionTableViewer::ChangeViewTo(const char *pcpViewName)
{
    DeleteAll();    
    MakeLines();
	UpdateDisplay();
}

//-----------------------------------------------------------------------------------------------

void PositionTableViewer::MakeLines()
{
	int ilPositionCount = pomData->GetSize();
	for (int ilLc = 0; ilLc < ilPositionCount; ilLc++)
	{
		PSTDATA *prlPositionData = &pomData->GetAt(ilLc);
		MakeLine(prlPositionData);
	}
}

//-----------------------------------------------------------------------------------------------

void PositionTableViewer::MakeLine(PSTDATA *prpPosition)
{

    //if( !IsPassFilter(prpPosition)) return;

    // Update viewer data for this shift record
    POSITIONTABLE_LINEDATA rlPosition;
	rlPosition.Urno = prpPosition->Urno; 
	rlPosition.Pnam = prpPosition->Pnam; 
	rlPosition.Gpus = prpPosition->Gpus; 
	rlPosition.Fuls = prpPosition->Fuls; 
	rlPosition.Pubk = prpPosition->Pubk; 
	rlPosition.Brgs = prpPosition->Brgs; 
	rlPosition.Acus = prpPosition->Acus; 
	rlPosition.Pcas = prpPosition->Pcas; 
	rlPosition.Dgss = prpPosition->Dgss; 
	rlPosition.Tele = prpPosition->Tele; 
	rlPosition.Taxi = prpPosition->Taxi; 
	rlPosition.Vafr = prpPosition->Vafr.Format("%d.%m.%Y %H:%M"); 
	rlPosition.Vato = prpPosition->Vato.Format("%d.%m.%Y %H:%M"); 
	rlPosition.Mxac = prpPosition->Mxac; 

	if (ogBasicData.IsGatPosEnabled())
	{
		CStringArray olGates;
		ogBasicData.GetGatesForPosition(prpPosition->Urno,olGates);
		for (int i = 0; i < olGates.GetSize(); i++)
		{
			if (!rlPosition.Gates.IsEmpty())	
				rlPosition.Gates +=',';
			rlPosition.Gates += olGates[i];
		}
	}

	CreateLine(&rlPosition);
}

//-----------------------------------------------------------------------------------------------

void PositionTableViewer::CreateLine(POSITIONTABLE_LINEDATA *prpPosition)
{
    int ilLineCount = omLines.GetSize();
    for (int ilLineno = ilLineCount; ilLineno > 0; ilLineno--)
        if (ComparePosition(prpPosition, &omLines[ilLineno-1]) >= 0)
	        break;							 // should be inserted after Lines[ilLineno-1]
	POSITIONTABLE_LINEDATA rlPosition;
	rlPosition = *prpPosition;
    omLines.NewAt(ilLineno, rlPosition);
}

//-----------------------------------------------------------------------------------------------
// UpdateDisplay: Load data selected by filter conditions to the display by using "omTable"

void PositionTableViewer::UpdateDisplay()
{
	int ilLines = omLines.GetSize();
	omHeaderDataArray.DeleteAll();

	pomPositionTable->SetShowSelection(TRUE);
	pomPositionTable->ResetContent();

	TABLE_HEADER_COLUMN rlHeader;
	rlHeader.Alignment = COLALIGN_LEFT;
	rlHeader.Font = &ogCourier_Regular_10;

	CString olString;
	if (ogBasicData.IsGatPosEnabled())
	{
		olString = LoadStg(IDS_STRING891);
	}
	else
	{
		olString = LoadStg(IDS_STRING277);
	}

	int ilPos = 1;
	rlHeader.Length = 42;											// Name 
	rlHeader.Text = GetListItem(olString,ilPos++,false,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 8;											// G
	rlHeader.Text = GetListItem(olString,ilPos++,false,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 8;											// F
	rlHeader.Text = GetListItem(olString,ilPos++,false,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 8;											// P
	rlHeader.Text = GetListItem(olString,ilPos++,false,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 8;											// B
	rlHeader.Text = GetListItem(olString,ilPos++,false,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 8;											// A
	rlHeader.Text = GetListItem(olString,ilPos++,false,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 8;											// C
	rlHeader.Text = GetListItem(olString,ilPos++,false,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 8;											// D
	rlHeader.Text = GetListItem(olString,ilPos++,false,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 83;											// Phone no.
	rlHeader.Text = GetListItem(olString,ilPos++,false,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	if (!ogBasicData.IsGatPosEnabled())
	{
		rlHeader.Length = 42;										// Taxiway
		rlHeader.Text = GetListItem(olString,ilPos++,false,'|');
		omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	}
	else
	{
		++ilPos;
	}

	rlHeader.Length = 53;											// A/C type
	rlHeader.Text = GetListItem(olString,ilPos++,false,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 133;											// Valid from
	rlHeader.Text = GetListItem(olString,ilPos++,false,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 133;											// Valid to
	rlHeader.Text = GetListItem(olString,ilPos++,false,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);

	if (ogBasicData.IsGatPosEnabled())
	{
		rlHeader.Length = 133;										// Gates
		rlHeader.Text = GetListItem(olString,ilPos++,false,'|');
		omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	}

	pomPositionTable->SetHeaderFields(omHeaderDataArray);
	pomPositionTable->SetDefaultSeparator();
	pomPositionTable->SetTableEditable(FALSE);

	TABLE_COLUMN rlColumnData;
	rlColumnData.Font = &ogCourier_Regular_10;
	rlColumnData.BkColor = WHITE;
	rlColumnData.Alignment = COLALIGN_LEFT;
	
    for (int ilLineNo = 0; ilLineNo < ilLines; ilLineNo++)
	{
		CCSPtrArray<TABLE_COLUMN> olColList;

		rlColumnData.Text = omLines[ilLineNo].Pnam;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		(omLines[ilLineNo].Gpus == 'X') ? rlColumnData.Text = cgYes : rlColumnData.Text = cgNo;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		(omLines[ilLineNo].Fuls == 'X') ? rlColumnData.Text = cgYes : rlColumnData.Text = cgNo;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		(omLines[ilLineNo].Pubk == 'X') ? rlColumnData.Text = cgYes : rlColumnData.Text = cgNo;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		(omLines[ilLineNo].Brgs == 'X') ? rlColumnData.Text = cgYes : rlColumnData.Text = cgNo;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		(omLines[ilLineNo].Acus == 'X') ? rlColumnData.Text = cgYes : rlColumnData.Text = cgNo;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		(omLines[ilLineNo].Pcas == 'X') ? rlColumnData.Text = cgYes : rlColumnData.Text = cgNo;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		(omLines[ilLineNo].Dgss == 'X') ? rlColumnData.Text = cgYes : rlColumnData.Text = cgNo;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Tele;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		if (!ogBasicData.IsGatPosEnabled())
		{
			rlColumnData.Text = omLines[ilLineNo].Taxi;
			olColList.NewAt(olColList.GetSize(), rlColumnData);
		}
		rlColumnData.Text = omLines[ilLineNo].Mxac;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Vafr;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Vato;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		if (ogBasicData.IsGatPosEnabled())
		{
			rlColumnData.Text = omLines[ilLineNo].Gates;
			olColList.NewAt(olColList.GetSize(), rlColumnData);
		}

		pomPositionTable->AddTextLine(olColList,(void*)NULL);
		olColList.DeleteAll();
	}
	pomPositionTable->DisplayTable();
}

//-----------------------------------------------------------------------------------------------

CString PositionTableViewer::Format(POSITIONTABLE_LINEDATA *prpLine)
{
    CString s;
	// ???
    return s;
}

//-----------------------------------------------------------------------------------------------

BOOL PositionTableViewer::FindPosition(char *pcpPositionKeya, char *pcpPositionKeyd, int& ilItem)
{
	int ilCount = omLines.GetSize();
    for (ilItem = 0; ilItem < ilCount; ilItem++)
    {
/*		if ( (omLines[ilItem].Keya == pcpPositionKeya) &&
			 (omLines[ilItem].Keyd == pcpPositionKeyd)    )
			return TRUE;*/
	}
	return FALSE;
}

//-----------------------------------------------------------------------------------------------

static void PositionTableCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName)
{
    PositionTableViewer *polViewer = (PositionTableViewer *)popInstance;
	switch(ipDDXType)
	{
	case PST_CHANGE:
		polViewer->ProcessPositionChange((PSTDATA *)vpDataPointer);
	break;
    case PST_DELETE:
		polViewer->ProcessPositionDelete((PSTDATA *)vpDataPointer);
	break;
	case SGM_NEW:
	case SGM_CHANGE:
	case SGM_DELETE:
		polViewer->ProcessPositionChange((SGMDATA *)vpDataPointer);
	break;
	}

} 

//-----------------------------------------------------------------------------------------------
void PositionTableViewer::ProcessPositionChange(SGMDATA *prpSgm)
{
	if (ogBasicData.IsGatPosEnabled())
	{
		int ilItem = 0;

		if (FindLine(prpSgm->Valu, ilItem))
		{
			PSTDATA *prpPst = ogPSTData.GetPSTByUrno(prpSgm->Valu);
			if (prpPst)
			{
				ProcessPositionChange(prpPst);
			}
		}
	}
}



//-----------------------------------------------------------------------------------------------
void PositionTableViewer::ProcessPositionChange(PSTDATA *prpPosition)
{
	int ilItem = 0;
	int ilColumnNo = 0;
	TABLE_COLUMN rlColumn;
	CCSPtrArray <TABLE_COLUMN> olLine;

	rlColumn.Lineno = ilItem;
	rlColumn.VerticalSeparator = SEPA_NONE;
	rlColumn.SeparatorType = SEPA_NONE;
	rlColumn.BkColor = WHITE;
	rlColumn.Font = &ogCourier_Regular_10;
	rlColumn.Alignment = COLALIGN_LEFT;

	rlColumn.Text = prpPosition->Pnam;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	(*prpPosition->Gpus == 'X') ? rlColumn.Text = cgYes : rlColumn.Text = cgNo;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	(*prpPosition->Fuls == 'X') ? rlColumn.Text = cgYes : rlColumn.Text = cgNo;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	(*prpPosition->Pubk == 'X') ? rlColumn.Text = cgYes : rlColumn.Text = cgNo;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	(*prpPosition->Brgs == 'X') ? rlColumn.Text = cgYes : rlColumn.Text = cgNo;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	(*prpPosition->Acus == 'X') ? rlColumn.Text = cgYes : rlColumn.Text = cgNo;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	(*prpPosition->Pcas == 'X') ? rlColumn.Text = cgYes : rlColumn.Text = cgNo;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	(*prpPosition->Dgss == 'X') ? rlColumn.Text = cgYes : rlColumn.Text = cgNo;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpPosition->Tele;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	if (!ogBasicData.IsGatPosEnabled())
	{
		rlColumn.Text = prpPosition->Taxi;
		rlColumn.Columnno = ilColumnNo++;
		olLine.NewAt(olLine.GetSize(), rlColumn);
	}

	rlColumn.Text = prpPosition->Mxac;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpPosition->Vafr.Format("%d.%m.%Y %H:%M"); 
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpPosition->Vato.Format("%d.%m.%Y %H:%M"); 
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);

	CStringArray olGates;
	if (ogBasicData.IsGatPosEnabled())
	{
		rlColumn.Text = "";
		ogBasicData.GetGatesForPosition(prpPosition->Urno,olGates);
		for (int i = 0; i < olGates.GetSize(); i++)
		{
			if (!rlColumn.Text.IsEmpty())	
				rlColumn.Text +=',';
			rlColumn.Text += olGates[i];
		}

		rlColumn.Columnno = ilColumnNo++;
		olLine.NewAt(olLine.GetSize(), rlColumn);
	}


	if (FindLine(prpPosition->Urno, ilItem))
	{
        POSITIONTABLE_LINEDATA *prlLine = &omLines[ilItem];

		prlLine->Urno = prpPosition->Urno;
		prlLine->Pnam = prpPosition->Pnam;
		prlLine->Gpus = prpPosition->Gpus;
		prlLine->Fuls = prpPosition->Fuls;
		prlLine->Pubk = prpPosition->Pubk;
		prlLine->Brgs = prpPosition->Brgs;
		prlLine->Acus = prpPosition->Acus;
		prlLine->Pcas = prpPosition->Pcas;
		prlLine->Dgss = prpPosition->Dgss;
		prlLine->Tele = prpPosition->Tele;
		prlLine->Taxi = prpPosition->Taxi;
		prlLine->Vafr = prpPosition->Vafr.Format("%d.%m.%Y %H:%M");
		prlLine->Vato = prpPosition->Vato.Format("%d.%m.%Y %H:%M");
		prlLine->Mxac = prpPosition->Mxac;

		if (ogBasicData.IsGatPosEnabled())
		{
			prlLine->Gates = "";
			for (int i = 0; i < olGates.GetSize(); i++)
			{
				if (!prlLine->Gates.IsEmpty())	
					prlLine->Gates +=',';
				prlLine->Gates += olGates[i];
			}
		}

		pomPositionTable->ChangeTextLine(ilItem, &olLine, (void *)prlLine);
		pomPositionTable->DisplayTable();
	}
	else
	{
		MakeLine(prpPosition);
		if (FindLine(prpPosition->Urno, ilItem))
		{
	        POSITIONTABLE_LINEDATA *prlLine = &omLines[ilItem];
			if(prlLine != NULL)
			{
				pomPositionTable->AddTextLine(olLine, (void *)prlLine);
				pomPositionTable->DisplayTable();
			}
		}
	}
	olLine.DeleteAll();
}

//-----------------------------------------------------------------------------------------------

void PositionTableViewer::ProcessPositionDelete(PSTDATA *prpPosition)
{
	int ilItem;
	if (FindLine(prpPosition->Urno, ilItem))
	{
		DeleteLine(ilItem);
		pomPositionTable->DeleteTextLine(ilItem);
		pomPositionTable->DisplayTable();
	}
}

//-----------------------------------------------------------------------------------------------

BOOL PositionTableViewer::IsPassFilter(PSTDATA *prpPosition)
{

	return TRUE;
}

//-----------------------------------------------------------------------------------------------

int PositionTableViewer::ComparePosition(POSITIONTABLE_LINEDATA *prpPosition1, POSITIONTABLE_LINEDATA *prpPosition2)
{
/*	int ilCase=0;
	int	ilCompareResult;
	if(prpPosition1->Tifd == prpPosition2->Tifd)
	{
		ilCompareResult= 0;
	}
	else
	{
		if(prpPosition1->Tifd > prpPosition2->Tifd)
		{
			ilCompareResult = 1;
		}
		else
		{
			ilCompareResult = -1;
		}
	}
	return ilCompareResult;
*/  
	return 0;
}

//-----------------------------------------------------------------------------------------------

void PositionTableViewer::DeleteLine(int ipLineno)
{
	omLines.DeleteAt(ipLineno);
}

//-----------------------------------------------------------------------------------------------

BOOL PositionTableViewer::FindLine(long lpUrno, int &rilLineno)
{
    for (rilLineno = 0; rilLineno < omLines.GetSize(); rilLineno++)
	{
        if (omLines[rilLineno].Urno == lpUrno)
            return TRUE;
	}
    return FALSE;
}
//-----------------------------------------------------------------------------------------------

void PositionTableViewer::DeleteAll()
{
    while (omLines.GetSize() > 0)
        DeleteLine(0);
}

//-----------------------------------------------------------------------------------------------
// Printing Routines
//-----------------------------------------------------------------------------------------------

void PositionTableViewer::PrintTableView()
{
	CString olFooter1,olFooter2;
	omTableName = LoadStg(IDS_STRING185);
	int ilOrientation = PRINT_LANDSCAPE;//  PRINT_PORTRAET  PRINT_LANDSCAPE
	pomPrint = new CCSPrint((CWnd*)this,ilOrientation,45);// FirstLine(P=200,L=200),LeftOffset(P=200,L=50)
	//pomPrint->imMaxLines = 38;  // (P=57,L=38)
	//omBitmap.LoadBitmap(IDB_HAJLOGO);
	//pomPrint->SetBitmaps(&omBitmap,&omBitmap);
	if (pomPrint != NULL)
	{
		if (pomPrint->InitializePrinter(ilOrientation) == TRUE)
		{
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			pomPrint->imLineNo = pomPrint->imMaxLines + 1;
			DOCINFO	rlDocInfo;
			memset(&rlDocInfo, 0, sizeof(DOCINFO));
			rlDocInfo.cbSize = sizeof( DOCINFO );
			rlDocInfo.lpszDocName = omTableName;	
			pomPrint->omCdc.StartDoc( &rlDocInfo );
			pomPrint->imPageNo = 0;
			int ilLines = omLines.GetSize();
			olFooter1.Format("%d %s",ilLines,omTableName);
			for(int i = 0; i < ilLines; i++ ) 
			{
				if(pomPrint->imLineNo >= pomPrint->imMaxLines)
				{
					if(pomPrint->imPageNo > 0)
					{
						olFooter2.Format("%s: %d",LoadStg(IDS_STRING222),pomPrint->imPageNo);
						pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
						pomPrint->omCdc.EndPage();
					}
					PrintTableHeader();
				}
				if(pomPrint->imLineNo == (pomPrint->imMaxLines-1) || i == (ilLines-1))
				{
					PrintTableLine(&omLines[i],true);
				}
				else
				{
					PrintTableLine(&omLines[i],false);
				}
			}
			olFooter2.Format("%s: %d",LoadStg(IDS_STRING222),pomPrint->imPageNo);
			pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
			pomPrint->omCdc.EndPage();
			pomPrint->omCdc.EndDoc();
		}
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	delete pomPrint;
	//omBitmap.DeleteObject();
	pomPrint = NULL;
	}
}

//-----------------------------------------------------------------------------------------------

bool PositionTableViewer::PrintTableHeader()
{
	pomPrint->omCdc.StartPage();
	pomPrint->imPageNo++;
	pomPrint->imLineNo = 0;

	pomPrint->PrintUIFHeader(omTableName,CString(CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")),pomPrint->imFirstLine-10);

	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;

	rlElement.Alignment   = PRINT_LEFT;
	rlElement.FrameLeft   = PRINT_NOFRAME;
	rlElement.FrameRight  = PRINT_NOFRAME;
	rlElement.FrameTop    = PRINT_NOFRAME;
	rlElement.FrameBottom = PRINT_FRAMEMEDIUM;
	rlElement.pFont       = &pomPrint->ogCourierNew_Bold_8;

	int ilSize = omHeaderDataArray.GetSize();
	for(int i=0;i<ilSize;i++)
	{
		//if(i!=1&&i!=2)// <= setzen, wenn Spalten nicht angezeigt werden sollen 
		{
			rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
			if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
			rlElement.Text   = omHeaderDataArray[i].Text;
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		}
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();


	return true;
}

//-----------------------------------------------------------------------------------------------

bool PositionTableViewer::PrintTableLine(POSITIONTABLE_LINEDATA *prpLine,bool bpLastLine)
{
	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;
	rlElement.pFont      = &pomPrint->ogCourierNew_Regular_8;
	int ilSize = omHeaderDataArray.GetSize();
	for(int i=0;i<ilSize;i++)
	{
		//if(i!=1&&i!=1)// <= setzen, wenn Spalten nicht angezeigt werden sollen 
		{
			rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
			if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
			rlElement.Alignment  = PRINT_LEFT;
			rlElement.FrameTop   = PRINT_FRAMETHIN;
			if(bpLastLine == true)
			{
				rlElement.FrameBottom = PRINT_FRAMETHIN;
			}
			else
			{
				rlElement.FrameBottom = PRINT_NOFRAME;
			}
			rlElement.FrameLeft  = PRINT_NOFRAME;
			rlElement.FrameRight = PRINT_NOFRAME;
			if (!ogBasicData.IsGatPosEnabled())
			{
				switch(i)
				{
				case 0:
					{
						rlElement.FrameLeft  = PRINT_FRAMETHIN;
						rlElement.Text		= prpLine->Pnam;
					}
					break;
				case 1:
					{
						rlElement.Text		= prpLine->Gpus;
					}
					break;
				case 2:
					{
						rlElement.Text		= prpLine->Fuls;
					}
					break;
				case 3:
					{
						rlElement.Text		= prpLine->Pubk;
					}
					break;
				case 4:
					{
						rlElement.Text		= prpLine->Brgs;
					}
					break;
				case 5:
					{
						rlElement.Text		= prpLine->Acus;
					}
					break;
				case 6:
					{
						rlElement.Text		= prpLine->Pcas;
					}
					break;
				case 7:
					{
						rlElement.Text		= prpLine->Dgss;
					}
					break;
				case 8:
					{
						rlElement.Text		= prpLine->Tele;
					}
					break;
				case 9:
					{
						rlElement.Text		= prpLine->Taxi;
					}
					break;
				case 10:
					{
						rlElement.Text		= prpLine->Mxac;
					}
					break;
				case 11:
					{
						rlElement.Text		= prpLine->Vafr;
					}
					break;
				case 12:
					{
						rlElement.FrameRight = PRINT_FRAMETHIN;
						rlElement.Text		= prpLine->Vato;
					}
					break;
				}
			}
			else
			{
				switch(i)
				{
				case 0:
					{
						rlElement.FrameLeft  = PRINT_FRAMETHIN;
						rlElement.Text		= prpLine->Pnam;
					}
					break;
				case 1:
					{
						rlElement.Text		= prpLine->Gpus;
					}
					break;
				case 2:
					{
						rlElement.Text		= prpLine->Fuls;
					}
					break;
				case 3:
					{
						rlElement.Text		= prpLine->Pubk;
					}
					break;
				case 4:
					{
						rlElement.Text		= prpLine->Brgs;
					}
					break;
				case 5:
					{
						rlElement.Text		= prpLine->Acus;
					}
					break;
				case 6:
					{
						rlElement.Text		= prpLine->Pcas;
					}
					break;
				case 7:
					{
						rlElement.Text		= prpLine->Dgss;
					}
					break;
				case 8:
					{
						rlElement.Text		= prpLine->Tele;
					}
					break;
				case 9:
					{
						rlElement.Text		= prpLine->Mxac;
					}
					break;
				case 10:
					{
						rlElement.Text		= prpLine->Vafr;
					}
					break;
				case 11:
					{
						rlElement.Text		= prpLine->Vato;
					}
					break;
				case 12:
					{
						rlElement.FrameRight = PRINT_FRAMETHIN;
						rlElement.Text		= prpLine->Gates;
					}
					break;
				}
			}
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		}
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();
	return true;
}

//-----------------------------------------------------------------------------------------------
bool PositionTableViewer::CreateExcelFile(const CString& opTrenner,const CString& ropListSeparator)
{
	ofstream of;

	CString olTmpPath = CCSLog::GetTmpPath();

	CString olTableName;
	olTableName.Format("%s -   %s", "PSTTAB", (CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")) );


	CString olFileName = olTableName;
	olFileName.Remove('*');
	olFileName.Remove('.');
	olFileName.Remove(':');
	olFileName.Remove('/');
	olFileName.Replace(" ", "_");
	
	char pHeader[256];
	strcpy (pHeader, olTmpPath);
	CString path = pHeader;
	omFileName =  path + "\\" + olFileName + ".csv";

	of.open( omFileName, ios::out);


	of  << setw(0) << "PSTTAB" << "     "  << CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")  << endl;

	CString olString;
	if (ogBasicData.IsGatPosEnabled())
	{
		olString = LoadStg(IDS_STRING891);
	}
	else
	{
		olString = LoadStg(IDS_STRING277);
	}

	CStringArray olStrArray;
	ExtractItemList(olString,&olStrArray,'|');

	of	<< setw(0) << olStrArray[0]	//CString("Name");
		<< setw(0) << opTrenner
		<< setw(0) << olStrArray[1]	//CString("G");
		<< setw(0) << opTrenner
		<< setw(0) << olStrArray[2]	//CString("F");
		<< setw(0) << opTrenner
		<< setw(0) << olStrArray[3]	//CString("P");
		<< setw(0) << opTrenner
		<< setw(0) << olStrArray[4]	//CString("B");
		<< setw(0) << opTrenner
		<< setw(0) << olStrArray[5]	//CString("A");
		<< setw(0) << opTrenner
		<< setw(0) << olStrArray[6]	//CString("C");
		<< setw(0) << opTrenner
		<< setw(0) << olStrArray[7]	//CString("D");
		<< setw(0) << opTrenner
		<< setw(0) << olStrArray[8]	//CString("Phone number");
		<< setw(0) << opTrenner
		<< setw(0) << olStrArray[9]	//CString("Taxiway");
		<< setw(0) << opTrenner
		<< setw(0) << olStrArray[10]//CString("A/C type");
		<< setw(0) << opTrenner
		<< setw(0) << olStrArray[11]//CString("Valid from");
		<< setw(0) << opTrenner
		<< setw(0) << olStrArray[12]//CString("Valid to");
		;
	if (ogBasicData.IsGatPosEnabled())
	{
		of	<< setw(0) << opTrenner
			<< setw(0) << olStrArray[13]	//CString("Gates");
			<< endl;
	}
	else
	{
		of << endl;
	}


	
	int ilCount = omLines.GetSize();
	COleDateTime olCurrTime = COleDateTime::GetCurrentTime();

	for(int i = 0; i < ilCount; i++)
	{
		POSITIONTABLE_LINEDATA rlLine = omLines[i];
		CString olGates;
		CStringArray olGatesList;
		ogBasicData.GetGatesForPosition(rlLine.Urno,olGatesList);			
		for (int j = 0; j < olGatesList.GetSize(); j++)
		{
			if (!olGates.IsEmpty())			
				olGates += ropListSeparator;
			olGates += olGatesList[j];
		}

		of.setf(ios::left, ios::adjustfield);

		of   << setw(0) << rlLine.Pnam							// Name
		     << setw(0) << opTrenner 
		     << setw(0) << rlLine.Gpus							// G
		     << setw(0) << opTrenner 
		     << setw(0) << rlLine.Fuls							// F
		     << setw(0) << opTrenner 
		     << setw(0) << rlLine.Pubk							// P
		     << setw(0) << opTrenner 
		     << setw(0) << rlLine.Brgs							// B
		     << setw(0) << opTrenner 
		     << setw(0) << rlLine.Acus							// A
		     << setw(0) << opTrenner 
		     << setw(0) << rlLine.Pcas							// C
		     << setw(0) << opTrenner 
		     << setw(0) << rlLine.Dgss							// D
		     << setw(0) << opTrenner 
		     << setw(0) << rlLine.Tele							// Phone number
		     << setw(0) << opTrenner 
			 << setw(0) << rlLine.Taxi							// Taxi
		     << setw(0) << opTrenner 
			 << setw(0) << rlLine.Mxac							// A/C Type
		     << setw(0) << opTrenner 
			 << setw(0) << rlLine.Vafr							// Valid from
		     << setw(0) << opTrenner 
			 << setw(0) << rlLine.Vato							// Valid to
			 ;
		if (ogBasicData.IsGatPosEnabled())
		{
			of	<< setw(0) << opTrenner 
				<< setw(0) << olGates							// Gates
				<< endl; 
		}
		else
		{
			of	<< endl;
		}
		
	}

	of.close();
	return true;
}
//-----------------------------------------------------------------------------------------------
