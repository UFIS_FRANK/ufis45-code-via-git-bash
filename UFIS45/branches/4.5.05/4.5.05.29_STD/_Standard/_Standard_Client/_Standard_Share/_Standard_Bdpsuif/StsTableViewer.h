#ifndef __STSTABLEVIEWER_H__
#define __STSTABLEVIEWER_H__

#include <stdafx.h>
#include <CedaStsData.h>
#include <CCSTable.h>
#include <CViewer.h>
#include <CCSPrint.h>


struct STSTABLE_LINEDATA
{
	long 	Urno;
	CString	Stsc;
	CString Stsd;
	CString	Rema;
	CTime	Cdat;
	CTime	Lstu;
	CString	Usec;
	CString	Useu;
};

/////////////////////////////////////////////////////////////////////////////
// StsTableViewer

class StsTableViewer : public CViewer
{
// Constructions
public:
    StsTableViewer(CCSPtrArray<STSDATA> *popData);
    ~StsTableViewer();

    void Attach(CCSTable *popAttachWnd);
    virtual void ChangeViewTo(const char *pcpViewName);
// Internal data processing routines
private:
	bool IsPassFilter(STSDATA *prpSts);
	int CompareSts(STSTABLE_LINEDATA *prpSts1, STSTABLE_LINEDATA *prpSts2);
    void MakeLines();
	void MakeLine(STSDATA *prpSts);
	bool FindLine(long lpUrno, int &rilLineno);
// Operations
public:
	void DeleteAll();
	void CreateLine(STSTABLE_LINEDATA *prpSts);
	void DeleteLine(int ipLineno);
// Window refreshing routines
public:
	void UpdateDisplay();
	CString Format(STSTABLE_LINEDATA *prpLine);
	void ProcessStsChange(STSDATA *prpSts);
	void ProcessStsDelete(STSDATA *prpSts);
	bool FindSts(char *prpStsKeya, char *prpStsKeyd, int& ilItem);
// Attributes used for filtering condition
private:
	bool bmIsFromSearch;
// Attributes
private:
    CCSTable *pomStsTable;
	CCSPtrArray<STSDATA> *pomData;
// Methods which handle changes (from Data Distributor)
public:
    CCSPtrArray<STSTABLE_LINEDATA> omLines;
///////
	CCSPtrArray<TABLE_HEADER_COLUMN> omHeaderDataArray;
//Print 
	void PrintTableView();
	bool PrintTableLine(STSTABLE_LINEDATA *prpLine,bool bpLastLine);
	bool PrintTableHeader();
	CCSPrint *pomPrint;
	//CBitmap omBitmap;
	CString omTableName;

};

#endif //__STSTABLEVIEWER_H__
