#ifndef __AIRCRAFTTABLEVIEWER_H__
#define __AIRCRAFTTABLEVIEWER_H__

#include <stdafx.h>
#include <CedaACTData.h>
#include <CCSTable.h>
#include <CCSPrint.h>
#include <CViewer.h>


struct AIRCRAFTTABLE_LINEDATA
{
	long	Urno;
	CString	Act3;
	CString	Act5;
	CString	Acti;
	//uhi 26.3.01
	CString	Acbt;
	
	CString	Acfn;
	CString	Acws;
	CString	Acle;
	CString	Ache;
	CString	Seat;
	CString	Seaf;
	CString	Seab;
	CString	Seae;
	CString	Enty;
	CString	Enno;
	CString	Vafr;
	CString	Vato;
	CString	Nads;
	CString	Afmc;
	CString	Altc;
	CString	Modc;
};

/////////////////////////////////////////////////////////////////////////////
// AircraftTableViewer

class AircraftTableViewer : public CViewer
{
// Constructions
public:
    AircraftTableViewer(CCSPtrArray<ACTDATA> *popData);
    ~AircraftTableViewer();

    void Attach(CCSTable *popAttachWnd);
    virtual void ChangeViewTo(const char *pcpViewName);
// Internal data processing routines
private:
	bool IsPassFilter(ACTDATA *prpAircraft);
	int CompareAircraft(AIRCRAFTTABLE_LINEDATA *prpAircraft1, AIRCRAFTTABLE_LINEDATA *prpAircraft2);
    void MakeLines();
	void MakeLine(ACTDATA *prpAircraft);
	bool FindLine(long lpUrno, int &rilLineno);
// Operations
public:
	void DeleteAll();
	void CreateLine(AIRCRAFTTABLE_LINEDATA *prpAircraft);
	void DeleteLine(int ipLineno);
// Window refreshing routines
public:
	void UpdateDisplay();
	CString Format(AIRCRAFTTABLE_LINEDATA *prpLine);
	void ProcessAircraftChange(ACTDATA *prpAircraft);
	void ProcessAircraftDelete(ACTDATA *prpAircraft);
	bool FindAircraft(char *prpAircraftKeya, char *prpAircraftKeyd, int& ilItem);
// Attributes used for filtering condition
private:
	bool bmIsFromSearch;
// Attributes
private:
    CCSTable *pomAircraftTable;
	CCSPtrArray<ACTDATA> *pomData;
// Methods which handle changes (from Data Distributor)
public:
    CCSPtrArray<AIRCRAFTTABLE_LINEDATA> omLines;
///////
	CCSPtrArray<TABLE_HEADER_COLUMN> omHeaderDataArray;
//Print 
	void PrintTableView();
	bool PrintTableLine(AIRCRAFTTABLE_LINEDATA *prpLine,bool bpLastLine);
	bool PrintTableHeader();
	CCSPrint *pomPrint;
	//CBitmap omBitmap;
	CString omTableName;

};

#endif //__AIRCRAFTTABLEVIEWER_H__
