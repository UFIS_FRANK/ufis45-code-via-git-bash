#ifndef __POLTABLEVIEWER_H__
#define __POLTABLEVIEWER_H__

#include <stdafx.h>
#include <CedaPolData.h>
#include <CCSTable.h>
#include <CViewer.h>
#include <CCSPrint.h>

struct POLTABLE_LINEDATA
{
	long	Urno;
	CString	Name;
	CString	Pool;
	CString	Dtel;
};

/////////////////////////////////////////////////////////////////////////////
// PolTableViewer

class PolTableViewer : public CViewer
{
// Constructions
public:
    PolTableViewer(CCSPtrArray<POLDATA> *popData);
    ~PolTableViewer();

    void Attach(CCSTable *popAttachWnd);
    virtual void ChangeViewTo(const char *pcpViewName);
// Internal data processing routines
private:
	BOOL IsPassFilter(POLDATA *prpPol);
	int ComparePol(POLTABLE_LINEDATA *prpPol1, POLTABLE_LINEDATA *prpPol2);
    void MakeLines();
	void MakeLine(POLDATA *prpPol);
	BOOL FindLine(long lpUrno, int &rilLineno);
// Operations
public:
	void DeleteAll();
	void CreateLine(POLTABLE_LINEDATA *prpPol);
	void DeleteLine(int ipLineno);
// Window refreshing routines
public:
	void UpdateDisplay();
	CString Format(POLTABLE_LINEDATA *prpLine);
	void ProcessPolChange(POLDATA *prpPol);
	void ProcessPolDelete(POLDATA *prpPol);
	BOOL FindPol(char *prpPolKeya, char *prpPolKeyd, int& ilItem);
// Attributes used for filtering condition
private:
	BOOL bmIsFromSearch;
// Attributes
private:
    CCSTable *pomPolTable;
	CCSPtrArray<POLDATA> *pomData;
// Methods which handle changes (from Data Distributor)
public:
    CCSPtrArray<POLTABLE_LINEDATA> omLines;
///////
	CCSPtrArray<TABLE_HEADER_COLUMN> omHeaderDataArray;
//Print 
	void PrintTableView();
	bool PrintTableLine(POLTABLE_LINEDATA *prpLine,bool bpLastLine);
	bool PrintTableHeader();
	CCSPrint *pomPrint;
	//CBitmap omBitmap;
	CString omTableName;

};

#endif //__POLTABLEVIEWER_H__
