#if !defined(AFX_SELECTIONDLG_H__5076FC67_B8AD_11D6_8214_00010215BFDE__INCLUDED_)
#define AFX_SELECTIONDLG_H__5076FC67_B8AD_11D6_8214_00010215BFDE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SelectionDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// SelectionDlg dialog

class SelectionDlg : public CDialog
{
// Construction
public:
	SelectionDlg(CWnd* pParent = NULL);   // standard constructor
	~SelectionDlg();

	void SetCaption(const CString& ropCaption);
	void SetPossibleList(const CString& ropCaption,const CStringArray& ropList);
	void SetSelectionList(const CString& ropCaption,const CStringArray& ropList);
	void GetSelectionList(CStringArray& ropList);
	void UseAllItem(bool bpUseIt);
	
// Dialog Data
	//{{AFX_DATA(SelectionDlg)
	enum { IDD = IDD_SELECTION };
	CSpinButtonCtrl	m_SpinSelectedItems;
	CButton		m_RemoveButton;
	CButton		m_AddButton;
	CButton		m_RemoveAllButton;
	CButton		m_AddAllButton;
	CListBox	m_ContentList;
	CListBox	m_InsertList;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(SelectionDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(SelectionDlg)
	afx_msg void OnButtonRemoveAll();
	afx_msg void OnButtonAdd();
	afx_msg void OnButtonAddAll();
	afx_msg void OnButtonRemove();
	virtual BOOL OnInitDialog();
	afx_msg void OnDeltaposSpinSelectedItems(NMHDR* pNMHDR, LRESULT* pResult);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	CString		 omCaption;
	CString		 omPossibleCaption;
	CString		 omSelectedCaption;
	CStringArray omPossibleItems;
	CStringArray omSelectedItems;
	bool		 bmUseAllItem;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SELECTIONDLG_H__5076FC67_B8AD_11D6_8214_00010215BFDE__INCLUDED_)
