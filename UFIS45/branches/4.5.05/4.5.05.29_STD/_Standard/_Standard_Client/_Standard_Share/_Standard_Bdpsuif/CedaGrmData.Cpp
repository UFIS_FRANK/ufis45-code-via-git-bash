// CedaGrmData.cpp
 
#include <stdafx.h>
#include <CedaGrmData.h>
#include <resource.h>


void ProcessGrmCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//--CEDADATA-----------------------------------------------------------------------------------------------

CedaGrmData::CedaGrmData() : CCSCedaData(&ogCommHandler)
{
    BEGIN_CEDARECINFO(GRMDATA, GrmDataRecInfo)
		FIELD_DATE		(Cdat,"CDAT")
		FIELD_LONG		(Gurn,"GURN")
		FIELD_DATE		(Lstu,"LSTU")
		FIELD_CHAR_TRIM	(Prfl,"PRFL")
		FIELD_LONG		(Urno,"URNO")
		FIELD_CHAR_TRIM	(Usec,"USEC")
		FIELD_CHAR_TRIM	(Useu,"USEU")
		FIELD_CHAR_TRIM	(Valu,"VALU")
	END_CEDARECINFO

    // Copy the record structure
    for (int i = 0; i < sizeof(GrmDataRecInfo)/sizeof(GrmDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&GrmDataRecInfo[i],sizeof(CEDARECINFO));
        omRecInfo.Add(prpCedaRecInfo);
	}
    // Initialize table names and field names
    strcpy(pcmTableName,"GRM");
    sprintf(pcmListOfFields,"CDAT,GURN,LSTU,PRFL,URNO,USEC,USEU,VALU");

	pcmFieldList = pcmListOfFields; //because in CedaData pcmFieldList is just a pointer

	omData.RemoveAll();
}

//---------------------------------------------------------------------------------------------------------

void CedaGrmData::GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType)
{
	ropFields.Add("CDAT");
	ropFields.Add("GURN");
	ropFields.Add("LSTU");
	ropFields.Add("PRFL");
	ropFields.Add("URNO");
	ropFields.Add("USEC");
	ropFields.Add("USEU");
	ropFields.Add("VALU");

	ropDesription.Add(LoadStg(IDS_STRING343));
	ropDesription.Add(LoadStg(IDS_STRING461));
	ropDesription.Add(LoadStg(IDS_STRING344));
	ropDesription.Add(LoadStg(IDS_STRING345));
	ropDesription.Add(LoadStg(IDS_STRING346));
	ropDesription.Add(LoadStg(IDS_STRING347));
	ropDesription.Add(LoadStg(IDS_STRING348));
	ropDesription.Add(LoadStg(IDS_STRING462));

	ropType.Add("Date");
	ropType.Add("String");
	ropType.Add("Date");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");

}

//--REGISTER----------------------------------------------------------------------------------------------

void CedaGrmData::Register(void)
{
	ogDdx.Register((void *)this,BC_GRM_CHANGE,	CString("GRMDATA"), CString("Grm-changed"),	ProcessGrmCf);
	ogDdx.Register((void *)this,BC_GRM_NEW,		CString("GRMDATA"), CString("Grm-new"),		ProcessGrmCf);
	ogDdx.Register((void *)this,BC_GRM_DELETE,	CString("GRMDATA"), CString("Grm-deleted"),	ProcessGrmCf);
}

//--~CEDADATA----------------------------------------------------------------------------------------------

CedaGrmData::~CedaGrmData(void)
{
	omRecInfo.DeleteAll();
	ClearAll();
}

//--CLEAR-ALL----------------------------------------------------------------------------------------------

void CedaGrmData::ClearAll(bool bpWithRegistration)
{
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(bpWithRegistration)
	{
		ogDdx.UnRegister(this,NOTUSED);
	}
}

//--READ-ALL-----------------------------------------------------------------------------------------------

bool CedaGrmData::Read(char *pspWhere /*NULL*/)
{
    // Select data from the database
	bool ilRc = true;
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(pspWhere == NULL)
	{	
		ilRc = CedaAction("RT");
	}
	else
	{
		ilRc = CedaAction("RT", pspWhere);
	}
	if (ilRc != true)
	{
		return ilRc;
	}
	// Load data from CedaData into the dynamic array of record
	for (int ilLc = 0; ilRc == true; ilLc++)
	{
		GRMDATA *prlGrm = new GRMDATA;
		if ((ilRc = GetFirstBufferRecord(prlGrm)) == true)
		{
			omData.Add(prlGrm);//Update omData
			omUrnoMap.SetAt((void *)prlGrm->Urno,prlGrm);
		}
		else
		{
			delete prlGrm;
		}
	}
    return true;
}

//--INSERT-------------------------------------------------------------------------------------------------

bool CedaGrmData::Insert(GRMDATA *prpGrm)
{
	prpGrm->IsChanged = DATA_NEW;
	if(Save(prpGrm) == false) return false; //Update Database
	InsertInternal(prpGrm);
    return true;
}

//--INSERT-INTERNAL--------------------------------------------------------------------------------------

bool CedaGrmData::InsertInternal(GRMDATA *prpGrm)
{
	ogDdx.DataChanged((void *)this, GRM_NEW,(void *)prpGrm ); //Update Viewer
	omData.Add(prpGrm);//Update omData
	omUrnoMap.SetAt((void *)prpGrm->Urno,prpGrm);
    return true;
}

//--DELETE-------------------------------------------------------------------------------------------------

bool CedaGrmData::Delete(long lpUrno)
{
	GRMDATA *prlGrm = GetGrmByUrno(lpUrno);
	if (prlGrm != NULL)
	{
		prlGrm->IsChanged = DATA_DELETED;
		if(Save(prlGrm) == false) return false; //Update Database
		DeleteInternal(prlGrm);
	}
    return true;
}

//--DELETE-INTERNAL----------------------------------------------------------------------------------------

bool CedaGrmData::DeleteInternal(GRMDATA *prpGrm)
{
	ogDdx.DataChanged((void *)this,GRM_DELETE,(void *)prpGrm); //Update Viewer
	omUrnoMap.RemoveKey((void *)prpGrm->Urno);
	int ilGrmCount = omData.GetSize();
	for (int ilLc = 0; ilLc < ilGrmCount; ilLc++)
	{
		if (omData[ilLc].Urno == prpGrm->Urno)
		{
			omData.DeleteAt(ilLc);//Update omData
			break;
		}
	}
    return true;
}

//--UPDATE-------------------------------------------------------------------------------------------------

bool CedaGrmData::Update(GRMDATA *prpGrm)
{
	if (GetGrmByUrno(prpGrm->Urno) != NULL)
	{
		if (prpGrm->IsChanged == DATA_UNCHANGED)
		{
			prpGrm->IsChanged = DATA_CHANGED;
		}
		if(Save(prpGrm) == false) return false; //Update Database
		UpdateInternal(prpGrm);
	}
    return true;
}

//--UPDATE-INTERNAL--------------------------------------------------------------------------------------------

bool CedaGrmData::UpdateInternal(GRMDATA *prpGrm)
{
	GRMDATA *prlGrm = GetGrmByUrno(prpGrm->Urno);
	if (prlGrm != NULL)
	{
		*prlGrm = *prpGrm; //Update omData
		ogDdx.DataChanged((void *)this,GRM_CHANGE,(void *)prlGrm); //Update Viewer
	}
    return true;
}

//--GET-BY-URNO--------------------------------------------------------------------------------------------

GRMDATA *CedaGrmData::GetGrmByUrno(long lpUrno)
{
	GRMDATA  *prlGrm;
	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlGrm) == TRUE)
	{
		return prlGrm;
	}
	return NULL;
}

//--READSPECIALDATA-------------------------------------------------------------------------------------

bool CedaGrmData::ReadSpecialData(CCSPtrArray<GRMDATA> *popGrm,char *pspWhere,char *pspFieldList,bool ipSYS/*=true*/)
{
	bool ilRc = true;
	char pclFieldList[256] = " ";

	if(strlen(pspFieldList) > 0) 
	{
		strcpy(pclFieldList, pspFieldList);
	}
	if(ipSYS == true) 
	{
		if (CedaAction("SYS","GRM",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	else
	{
		if (CedaAction("RT","GRM",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	if(popGrm != NULL)
	{
		for (int ilLc = 0; ilRc == true; ilLc++)
		{
			GRMDATA *prpGrm = new GRMDATA;
			if ((ilRc = GetBufferRecord(ilLc,prpGrm,CString(pclFieldList))) == true)
			{
				popGrm->Add(prpGrm);
			}
			else
			{
				delete prpGrm;
			}
		}
		if(popGrm->GetSize() == 0) return false;
	}
    return true;
}

//--SAVE---------------------------------------------------------------------------------------------------

bool CedaGrmData::Save(GRMDATA *prpGrm)
{
	bool ilRc = true;
	CString olListOfData;
	char pclSelection[124];
	char pclData[524];

	if (prpGrm->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}
	switch(prpGrm->IsChanged)
	{
	case DATA_NEW:
		MakeCedaData(&omRecInfo,olListOfData,prpGrm);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("IRT","","",pclData);
		prpGrm->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpGrm->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpGrm);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("URT",pclSelection,"",pclData);
		prpGrm->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_DELETED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpGrm->Urno);
		ilRc = CedaAction("DRT",pclSelection);
		break;
	}
    return ilRc;
}

//--PROCESS-CF---------------------------------------------------------------------------------------------

void  ProcessGrmCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	ogGrmData.ProcessBc(ipDDXType,vpDataPointer,ropInstanceName);
}

//--PROCESS-BC---------------------------------------------------------------------------------------------

void  CedaGrmData::ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlGrmData;
	prlGrmData = (struct BcStruct *) vpDataPointer;
	GRMDATA *prlGrm;
	if(ipDDXType == BC_GRM_NEW)
	{
		prlGrm = new GRMDATA;
		GetRecordFromItemList(prlGrm,prlGrmData->Fields,prlGrmData->Data);
		InsertInternal(prlGrm);
	}
	if(ipDDXType == BC_GRM_CHANGE)
	{
		long llUrno = GetUrnoFromSelection(prlGrmData->Selection);
		prlGrm = GetGrmByUrno(llUrno);
		if(prlGrm != NULL)
		{
			GetRecordFromItemList(prlGrm,prlGrmData->Fields,prlGrmData->Data);
			UpdateInternal(prlGrm);
		}
	}
	if(ipDDXType == BC_GRM_DELETE)
	{
		long llUrno = GetUrnoFromSelection(prlGrmData->Selection);

		prlGrm = GetGrmByUrno(llUrno);
		if (prlGrm != NULL)
		{
			DeleteInternal(prlGrm);
		}
	}
}

//---------------------------------------------------------------------------------------------------------
