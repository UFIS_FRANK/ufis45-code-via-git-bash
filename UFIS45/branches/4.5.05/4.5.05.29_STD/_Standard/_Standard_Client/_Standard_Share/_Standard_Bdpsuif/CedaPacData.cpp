// CedaPacData.cpp
 
#include <stdafx.h>
#include <CedaPacData.h>
#include <resource.h>


void ProcessPacCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//--CEDADATA-----------------------------------------------------------------------------------------------

CedaPacData::CedaPacData() : CCSCedaData(&ogCommHandler)
{
	// Create an array of CEDARECINFO for PACDataStruct
	BEGIN_CEDARECINFO(PACDATA,PACDataRecInfo)
		FIELD_LONG		(Urno,"URNO")
		FIELD_CHAR_TRIM	(Ctrc,"CTRC")
		FIELD_CHAR_TRIM	(Tifr,"TIFR")
		FIELD_LONG		(Pmxu,"PMXU")

	END_CEDARECINFO //(PACDataStruct)

	// FIELD_LONG, FIELD_DATE 
	// Copy the record structure
	for (int i=0; i< sizeof(PACDataRecInfo)/sizeof(PACDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&PACDataRecInfo[i],sizeof(CEDARECINFO));
		omRecInfo.Add(prpCedaRecInfo);
	} // end for

	// initialize field names
	strcpy(pcmTableName,"PAC");
	strcpy(pcmListOfFields,"URNO,CTRC,TIFR,PMXU");
	pcmFieldList = pcmListOfFields; //because in CedaData pcmFieldList is just a pointer

	omData.RemoveAll();
}

//---------------------------------------------------------------------------------------------------------

void CedaPacData::GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType)
{
	ropFields.Add("URNO");
	ropFields.Add("CTRC");
	ropFields.Add("TIFR");
	ropFields.Add("PMXU");

	ropDesription.Add(LoadStg(IDS_STRING345));
	ropDesription.Add(LoadStg(IDS_STRING237));
	ropDesription.Add(LoadStg(IDS_STRING229));
	ropDesription.Add(LoadStg(IDS_STRING200));

	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");

}

//--REGISTER----------------------------------------------------------------------------------------------

void CedaPacData::Register(void)
{
	ogDdx.Register((void *)this,BC_PAC_CHANGE,	CString("PACDATA"), CString("Pac-changed"),	ProcessPacCf);
	ogDdx.Register((void *)this,BC_PAC_NEW,		CString("PACDATA"), CString("Pac-new"),		ProcessPacCf);
	ogDdx.Register((void *)this,BC_PAC_DELETE,	CString("PACDATA"), CString("Pac-deleted"),	ProcessPacCf);
}

//--~CEDADATA----------------------------------------------------------------------------------------------

CedaPacData::~CedaPacData(void)
{
	omRecInfo.DeleteAll();
	ClearAll();
}

//--CLEAR-ALL----------------------------------------------------------------------------------------------

void CedaPacData::ClearAll(bool bpWithRegistration)
{
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(bpWithRegistration)
	{
		ogDdx.UnRegister(this,NOTUSED);
	}
}

//--READ-ALL-----------------------------------------------------------------------------------------------

bool CedaPacData::Read(char *pspWhere /*NULL*/)
{
    // Select data from the database
	bool ilRc = true;
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(pspWhere == NULL)
	{	
		ilRc = CedaAction("RT");
	}
	else
	{
		ilRc = CedaAction("RT", pspWhere);
	}
	if (ilRc != true)
	{
		return ilRc;
	}
	// Load data from CedaData into the dynamic array of record
	for (int ilLc = 0; ilRc == true; ilLc++)
	{
		PACDATA *prlPac = new PACDATA;
		if ((ilRc = GetFirstBufferRecord(prlPac)) == true)
		{
			omData.Add(prlPac);//Update omData
			omUrnoMap.SetAt((void *)prlPac->Urno,prlPac);
		}
		else
		{
			delete prlPac;
		}
	}
    return true;
}

//--INSERT-------------------------------------------------------------------------------------------------

bool CedaPacData::Insert(PACDATA *prpPac)
{
	prpPac->IsChanged = DATA_NEW;
	if(Save(prpPac) == false) return false; //Update Database
	InsertInternal(prpPac);
    return true;
}

//--INSERT-INTERNAL--------------------------------------------------------------------------------------

bool CedaPacData::InsertInternal(PACDATA *prpPac)
{
	ogDdx.DataChanged((void *)this, PAC_NEW,(void *)prpPac ); //Update Viewer
	omData.Add(prpPac);//Update omData
	omUrnoMap.SetAt((void *)prpPac->Urno,prpPac);
    return true;
}

//--DELETE-------------------------------------------------------------------------------------------------

bool CedaPacData::Delete(long lpUrno)
{
	PACDATA *prlPac = GetPacByUrno(lpUrno);
	if (prlPac != NULL)
	{
		prlPac->IsChanged = DATA_DELETED;
		if(Save(prlPac) == false) return false; //Update Database
		DeleteInternal(prlPac);
	}
    return true;
}

//--DELETE-INTERNAL----------------------------------------------------------------------------------------

bool CedaPacData::DeleteInternal(PACDATA *prpPac)
{
	ogDdx.DataChanged((void *)this,PAC_DELETE,(void *)prpPac); //Update Viewer
	omUrnoMap.RemoveKey((void *)prpPac->Urno);
	int ilPacCount = omData.GetSize();
	for (int ilLc = 0; ilLc < ilPacCount; ilLc++)
	{
		if (omData[ilLc].Urno == prpPac->Urno)
		{
			omData.DeleteAt(ilLc);//Update omData
			break;
		}
	}
    return true;
}

//--UPDATE-------------------------------------------------------------------------------------------------

bool CedaPacData::Update(PACDATA *prpPac)
{
	if (GetPacByUrno(prpPac->Urno) != NULL)
	{
		if (prpPac->IsChanged == DATA_UNCHANGED)
		{
			prpPac->IsChanged = DATA_CHANGED;
		}
		if(Save(prpPac) == false) return false; //Update Database
		UpdateInternal(prpPac);
	}
    return true;
}

//--UPDATE-INTERNAL--------------------------------------------------------------------------------------------

bool CedaPacData::UpdateInternal(PACDATA *prpPac)
{
	PACDATA *prlPac = GetPacByUrno(prpPac->Urno);
	if (prlPac != NULL)
	{
		*prlPac = *prpPac; //Update omData
		ogDdx.DataChanged((void *)this,PAC_CHANGE,(void *)prlPac); //Update Viewer
	}
    return true;
}

//--GET-BY-URNO--------------------------------------------------------------------------------------------

PACDATA *CedaPacData::GetPacByUrno(long lpUrno)
{
	PACDATA  *prlPac;
	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlPac) == TRUE)
	{
		return prlPac;
	}
	return NULL;
}

//--READSPECIALDATA-------------------------------------------------------------------------------------

bool CedaPacData::ReadSpecialData(CCSPtrArray<PACDATA> *popPac,char *pspWhere,char *pspFieldList,bool ipSYS/*=true*/)
{
	bool ilRc = true;
	char pclFieldList[256] = " ";

	if(strlen(pspFieldList) > 0) 
	{
		strcpy(pclFieldList, pspFieldList);
	}
	if(ipSYS == true) 
	{
		if (CedaAction("SYS","PAC",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	else
	{
		if (CedaAction("RT","PAC",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	if(popPac != NULL)
	{
		for (int ilLc = 0; ilRc == true; ilLc++)
		{
			PACDATA *prpPac = new PACDATA;
			if ((ilRc = GetBufferRecord(ilLc,prpPac,CString(pclFieldList))) == true)
			{
				popPac->Add(prpPac);
			}
			else
			{
				delete prpPac;
			}
		}
		if(popPac->GetSize() == 0) return false;
	}
    return true;
}

//--SAVE---------------------------------------------------------------------------------------------------

bool CedaPacData::Save(PACDATA *prpPac)
{
	bool ilRc = true;
	CString olListOfData;
	char pclSelection[124];
	char pclData[524];

	if (prpPac->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}
	switch(prpPac->IsChanged)
	{
	case DATA_NEW:
		MakeCedaData(&omRecInfo,olListOfData,prpPac);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("IRT","","",pclData);
		prpPac->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpPac->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpPac);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("URT",pclSelection,"",pclData);
		prpPac->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_DELETED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpPac->Urno);
		ilRc = CedaAction("DRT",pclSelection);
		break;
	}
    return ilRc;
}

//--PROCESS-CF---------------------------------------------------------------------------------------------

void  ProcessPacCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	ogPacData.ProcessBc(ipDDXType,vpDataPointer,ropInstanceName);
}

//--PROCESS-BC---------------------------------------------------------------------------------------------

void  CedaPacData::ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlPacData;
	prlPacData = (struct BcStruct *) vpDataPointer;
	PACDATA *prlPac;
	if(ipDDXType == BC_PAC_NEW)
	{
		prlPac = new PACDATA;
		GetRecordFromItemList(prlPac,prlPacData->Fields,prlPacData->Data);
		InsertInternal(prlPac);
	}
	if(ipDDXType == BC_PAC_CHANGE)
	{
		long llUrno = GetUrnoFromSelection(prlPacData->Selection);
		prlPac = GetPacByUrno(llUrno);
		if(prlPac != NULL)
		{
			GetRecordFromItemList(prlPac,prlPacData->Fields,prlPacData->Data);
			UpdateInternal(prlPac);
		}
	}
	if(ipDDXType == BC_PAC_DELETE)
	{
		long llUrno = GetUrnoFromSelection(prlPacData->Selection);

		prlPac = GetPacByUrno(llUrno);
		if (prlPac != NULL)
		{
			DeleteInternal(prlPac);
		}
	}
}

//---------------------------------------------------------------------------------------------------------
