#if !defined(AFX_GATPOSBAGGAGEBELTDLG_H__452453F7_BB0D_11D6_8214_00010215BFDE__INCLUDED_)
#define AFX_GATPOSBAGGAGEBELTDLG_H__452453F7_BB0D_11D6_8214_00010215BFDE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// GatPosBaggageBeltDlg.h : header file
//
#include <BeggagebeltDlg.h>
#include <AatBitmapComboBox.h>

/////////////////////////////////////////////////////////////////////////////
// GatPosBaggageBeltDlg dialog

class GatPosBaggageBeltDlg : public BeggagebeltDlg
{
// Construction
public:
	GatPosBaggageBeltDlg(BLTDATA *popBLT,CWnd* pParent = NULL);   // standard constructor
	int GetConnectedExits(CStringArray& ropExits);	
// Dialog Data
	//{{AFX_DATA(GatPosBaggageBeltDlg)
	enum { IDD = IDD_GATPOS_BAGGAGEBELTDLG };
	AatBitmapComboBox	m_BlockingBitmap;
	CListBox		m_ConnectedExits;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(GatPosBaggageBeltDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(GatPosBaggageBeltDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnExits();
	afx_msg void OnSelendokBlockingBitmap();
	afx_msg void OnSelchangeBlockingBitmap();
	virtual void OnOK();
	afx_msg LONG OnTableLButtonDblclk(UINT wParam, LONG lParam);
	afx_msg void OnNoavNew();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
protected:
	CStringArray	omAvailableExits;
	CStringArray	omConnectedExits;
	CImageList		omBlockingImages;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_GATPOSBAGGAGEBELTDLG_H__452453F7_BB0D_11D6_8214_00010215BFDE__INCLUDED_)
