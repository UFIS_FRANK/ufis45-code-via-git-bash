// DiensteUndAbwesenheitenDlg.cpp : implementation file
//

#include <stdafx.h>
#include <bdpsuif.h>
#include <DiensteUndAbwesenheitenDlg.h>
#include <PrivList.h>
#include <CedaBsdData.h>
#include <CedaParData.h>
#include <AwDlg.h>
#include <CheckReferenz.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// DiensteUndAbwesenheitenDlg dialog


DiensteUndAbwesenheitenDlg::DiensteUndAbwesenheitenDlg(ODADATA *popOda, CCSPtrArray<OACDATA> *popOacData,CWnd* pParent /*=NULL*/, bool opUpdate /*=false*/) : CDialog(DiensteUndAbwesenheitenDlg::IDD, pParent)
{
	pomOda = popOda;
	bmUpdate = opUpdate;
	//{{AFX_DATA_INIT(DiensteUndAbwesenheitenDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	pomOacData = popOacData;
	pomTable = new CCSTable;
	pomStatus = NULL;
}

DiensteUndAbwesenheitenDlg::~DiensteUndAbwesenheitenDlg()
{
	delete pomTable;
	pomTable = NULL;
}

void DiensteUndAbwesenheitenDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(DiensteUndAbwesenheitenDlg)
	DDX_Control(pDX, IDC_ABTO, m_Abto);
	DDX_Control(pDX, IDC_ABFR, m_Abfr);
	DDX_Control(pDX, IDC_BLEN, m_Blen);
	DDX_Control(pDX, IDC_SDAA, m_SDAA);
	DDX_Control(pDX, IDC_C_TBSD, m_TBSD);
	DDX_Control(pDX, IDC_C_WORK, m_WORK);
	DDX_Control(pDX, IDC_R_REGULAR_FREE, m_R_Free);
	DDX_Control(pDX, IDC_R_COMPENSATION, m_R_Compensation);
	DDX_Control(pDX, IDC_R_COMPENSATION_NIGHT, m_R_Compensation_Night);
	DDX_Control(pDX, IDC_R_ILLNESS, m_R_Illness);
	DDX_Control(pDX, IDC_HELP_OAC, m_HELP_OAC);
	DDX_Control(pDX, IDC_C_UPLN, m_C_Upln);
	DDX_Control(pDX, IDC_C_TSAP, m_C_Tsap);
	DDX_Control(pDX, IDOK,       m_OK);
	DDX_Control(pDX, IDC_CDAT_D, m_CDATD);
	DDX_Control(pDX, IDC_CDAT_T, m_CDATT);
	DDX_Control(pDX, IDC_LSTU_D, m_LSTUD);
	DDX_Control(pDX, IDC_LSTU_T, m_LSTUT);
	DDX_Control(pDX, IDC_REMA,	 m_REMA);
	DDX_Control(pDX, IDC_SDAC,	 m_SDAC);
	DDX_Control(pDX, IDC_SDAE,	 m_SDAE);
	DDX_Control(pDX, IDC_SDAK,	 m_SDAK);
	DDX_Control(pDX, IDC_SDAN,	 m_SDAN);
	DDX_Control(pDX, IDC_SDAS,	 m_SDAS);
	DDX_Control(pDX, IDC_USEC,	 m_USEC);
	DDX_Control(pDX, IDC_USEU,	 m_USEU);
	DDX_Control(pDX, IDC_R_TRAINING,m_R_Training);
	DDX_Control(pDX, IDC_R_REGULAR_ABSENCE,m_R_Absence);
	DDX_Control(pDX, IDC_R_SHIFT_CHANGEOVER_DAY,m_R_Changeover);
	DDX_Control(pDX, IDC_RGBC,	 m_RGBC);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(DiensteUndAbwesenheitenDlg, CDialog)
	//{{AFX_MSG_MAP(DiensteUndAbwesenheitenDlg)
	ON_BN_CLICKED(IDC_B_AW_COT, OnBAwCot)
    ON_MESSAGE(WM_EDIT_KILLFOCUS, OnKillfocus)
	ON_BN_CLICKED(IDC_B1, OnB1)
	ON_BN_CLICKED(IDC_B2, OnB2)
	ON_BN_CLICKED(IDC_B3, OnB3)
	ON_BN_CLICKED(IDC_B4, OnB4)
	ON_BN_CLICKED(IDC_R_REGULAR_ABSENCE,		OnRegularAbsence)
	ON_BN_CLICKED(IDC_R_REGULAR_FREE,			OnRegularFree)
	ON_BN_CLICKED(IDC_R_SHIFT_CHANGEOVER_DAY,	OnShiftChangeoverDay)
	ON_BN_CLICKED(IDC_R_TRAINING,				OnTraining)
	ON_BN_CLICKED(IDC_R_COMPENSATION,			OnCompensation)
	ON_BN_CLICKED(IDC_R_COMPENSATION_NIGHT,		OnCompensation_Night)
	ON_BN_CLICKED(IDC_R_ILLNESS,				OnIllness)
	ON_BN_CLICKED(IDC_RGBC, OnRgbc)
	ON_WM_CTLCOLOR()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// DiensteUndAbwesenheitenDlg message handlers
//----------------------------------------------------------------------------------------

BOOL DiensteUndAbwesenheitenDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	if(m_Caption == LoadStg(IDS_STRING150))
		bmChangeAction = true;
	else
		bmChangeAction = false;

	m_Caption = LoadStg(IDS_STRING168) + " <" + m_Caption + ">";
	SetWindowText(m_Caption);
	
	char clStat = ogPrivList.GetStat("DIENSTEUNDABWESENHEITENDLG.m_OK");
	SetWndStatAll(clStat,m_OK);

	//------------------------------------
	m_CDATD.SetBKColor(SILVER);
	m_CDATD.SetInitText(pomOda->Cdat.Format("%d.%m.%Y"));
	// - - - - - - - - - - - - - - - - - -
	m_CDATT.SetBKColor(SILVER);
	m_CDATT.SetInitText(pomOda->Cdat.Format("%H:%M"));
	//------------------------------------
	m_LSTUD.SetBKColor(SILVER);
	m_LSTUD.SetInitText(pomOda->Lstu.Format("%d.%m.%Y"));
	// - - - - - - - - - - - - - - - - - -
	m_LSTUT.SetBKColor(SILVER);
	m_LSTUT.SetInitText(pomOda->Lstu.Format("%H:%M"));
	//------------------------------------
	m_USEC.SetBKColor(SILVER);
	m_USEC.SetInitText(pomOda->Usec);
	//------------------------------------
	m_USEU.SetBKColor(SILVER);
	m_USEU.SetInitText(pomOda->Useu);
	//------------------------------------
	m_REMA.SetTypeToString("X(60)", 60, 0);
	m_REMA.SetTextErrColor(RED);
	m_REMA.SetInitText(pomOda->Rema);
	//------------------------------------
	m_SDAC.SetFormat("x|#x|#x|#x|#x|#x|#x|#x|#");
	m_SDAC.SetTextLimit(1,8);
	m_SDAC.SetBKColor(YELLOW);  
	m_SDAC.SetTextErrColor(RED);
	m_SDAC.SetInitText(pomOda->Sdac);

	this->omOldCode = pomOda->Sdac;

	if(bmUpdate == true)
		m_SDAC.EnableWindow(false);
	else
		m_SDAC.EnableWindow(true);

	//------------------------------------
	m_SDAE.SetFormat("x|#x|#x|#x|#x|#x|#x|#x|#");
	m_SDAE.SetTextLimit(0,8);
	m_SDAE.SetTextErrColor(RED);
	m_SDAE.SetInitText(pomOda->Sdae);
	//------------------------------------
	m_SDAK.SetTypeToString("X(12)", 12, 1);
	m_SDAK.SetBKColor(YELLOW);  
	m_SDAK.SetTextErrColor(RED);
	m_SDAK.SetInitText(pomOda->Sdak);
	//------------------------------------
	
	m_SDAA.SetFormat("a");
	m_SDAA.SetTextLimit(0,1);
	m_SDAA.SetTextErrColor(RED);
	m_SDAA.SetInitText(pomOda->Sdaa);

	m_Abfr.SetTextErrColor(RED);
	m_Abfr.SetFormat("[##':'##]");
	m_Abfr.SetTypeToTime();
	if (strlen(pomOda->Abfr) > 0)
	{
		char pclTmp[10]="";
		sprintf(pclTmp, "%s:%s", CString(pomOda->Abfr).Left(2), CString(pomOda->Abfr).Right(2));
		m_Abfr.SetInitText(pclTmp);
	}
	
	m_Abto.SetTextErrColor(RED);
	m_Abto.SetFormat("[##':'##]");
	m_Abto.SetTypeToTime();
	if(strlen(pomOda->Abto) > 0)
	{
		char pclTmp[10]="";
		sprintf(pclTmp, "%s:%s", CString(pomOda->Abto).Left(2), CString(pomOda->Abto).Right(2));
		m_Abto.SetInitText(pclTmp);
	}
	
	m_Blen.SetTextErrColor(RED);
	m_Blen.SetFormat("[##':'##]");
	m_Blen.SetTypeToTime();
	if(strlen(pomOda->Blen) > 0)
	{
		char pclTmp[10]="";
		sprintf(pclTmp, "%s:%s", CString(pomOda->Blen).Left(2), CString(pomOda->Blen).Right(2));
		m_Blen.SetInitText(pclTmp);
	}

	m_SDAN.SetTypeToString("X(40)", 40, 1);
	m_SDAN.SetBKColor(YELLOW);  
	m_SDAN.SetTextErrColor(RED);
	m_SDAN.SetInitText(pomOda->Sdan);
	//------------------------------------
	m_SDAS.SetTypeToString("X(8)", 8, 0);
	m_SDAS.SetTextErrColor(RED);
	m_SDAS.SetInitText(pomOda->Sdas);
	//------------------------------------
	m_C_Upln.SetCheck(0);
	if(pomOda->Upln[0] == 'Y') m_C_Upln.SetCheck(1);
	//------------------------------------
	m_C_Tsap.SetCheck(0);
	if(pomOda->Tsap[0] == 'Y') m_C_Tsap.SetCheck(1);
	//------------------------------------
	m_WORK.SetCheck(0);
	if(pomOda->Work[0] == '1') m_WORK.SetCheck(1);
	//------------------------------------
	m_TBSD.SetCheck(0);
	if(pomOda->Tbsd[0] == '1') m_TBSD.SetCheck(1);
	//------------------------------------
	if (pomOda->Free[0] == 'x')
	{
		this->CheckRadioButton(IDC_R_REGULAR_ABSENCE,IDC_R_COMPENSATION_NIGHT,IDC_R_REGULAR_FREE);

		m_C_Upln.EnableWindow(FALSE);
		m_WORK.EnableWindow(FALSE);
		m_TBSD.EnableWindow(FALSE);
	}
	else if (pomOda->Type[0] == 'T')
	{
		this->CheckRadioButton(IDC_R_REGULAR_ABSENCE,IDC_R_COMPENSATION_NIGHT,IDC_R_TRAINING);
		m_C_Upln.EnableWindow(FALSE);
	}
	else if (pomOda->Type[0] == 'S')
	{
		this->CheckRadioButton(IDC_R_REGULAR_ABSENCE,IDC_R_COMPENSATION_NIGHT,IDC_R_SHIFT_CHANGEOVER_DAY);	

		m_C_Upln.EnableWindow(FALSE);
		m_WORK.EnableWindow(FALSE);
		m_TBSD.EnableWindow(FALSE);
	}
	else if (pomOda->Type[0] == 'C')
	{
		this->CheckRadioButton(IDC_R_REGULAR_ABSENCE,IDC_R_COMPENSATION_NIGHT,IDC_R_COMPENSATION);
		m_C_Upln.EnableWindow(FALSE);
	}
	else if (pomOda->Type[0] == 'I')
	{
		this->CheckRadioButton(IDC_R_REGULAR_ABSENCE,IDC_R_COMPENSATION_NIGHT,IDC_R_ILLNESS);
		m_C_Upln.EnableWindow(FALSE);
	}
	else if (pomOda->Type[0] == 'N')
	{
		this->CheckRadioButton(IDC_R_REGULAR_ABSENCE,IDC_R_COMPENSATION_NIGHT,IDC_R_COMPENSATION_NIGHT);
		m_C_Upln.EnableWindow(FALSE);
	}
	else
	{
		this->CheckRadioButton(IDC_R_REGULAR_ABSENCE,IDC_R_COMPENSATION_NIGHT,IDC_R_REGULAR_ABSENCE);	
	}

	if (ogBasicData.IsColorCodeAvailable() && "ATH" == ogParData.GetParValue("ROSTER","ID_PROD_CUSTOMER"))
	{
		if (strlen(pomOda->Rgbc) > 0)
		{
			sscanf(pomOda->Rgbc,"%x",&this->omRgbc);
		}
		else
		{
			this->omRgbc = WHITE;
		}

		ogBasicData.SetWindowStat("DIENSTEUNDABWESENHEITENDLG.m_RGBC",&this->m_RGBC);

	}
	else
	{
		this->omRgbc = WHITE;

		m_RGBC.ShowWindow(SW_HIDE);
		m_RGBC.EnableWindow(FALSE);
		CWnd *polWnd = GetDlgItem(IDC_EDIT_RGBC);
		if (polWnd)
		{
			polWnd->ShowWindow(SW_HIDE);
			polWnd->EnableWindow(FALSE);
		}
	}

	//------------------------------------
		
	InitTables();
	return TRUE;
}

//----------------------------------------------------------------------------------------

void DiensteUndAbwesenheitenDlg::OnOK() 
{
	AfxGetApp()->DoWaitCursor(1);
	CString olErrorText,olNewCode;
	bool ilStatus = true;

	if(m_SDAC.GetStatus() == false) 
	{
		ilStatus = false;
		if(m_SDAC.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING237) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING237) + ogNotFormat;
		}
	}
	if(m_SDAK.GetStatus() == false) 
	{
		ilStatus = false;
		if(m_SDAK.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING301) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING301) + ogNotFormat;
		}
	}
	if(m_SDAN.GetStatus() == false) 
	{
		ilStatus = false;
		if(m_SDAN.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING229) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING229) + ogNotFormat;
		}
	}
	if(m_SDAS.GetStatus() == false) 
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING331) + ogNotFormat;
	}

	if(m_SDAA.GetStatus() == false) 
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING841) + ogNotFormat;
	}

	if(m_REMA.GetStatus() == false) 
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING238) + ogNotFormat;
	}
	
	if(m_Abfr.GetStatus() == false) 
	{
		ilStatus = false;
		if(m_Abfr.GetWindowTextLength() > 0)
			olErrorText += LoadStg(IDS_STRING850) + ogNotFormat;
	}

	if(m_Abto.GetStatus() == false) 
	{
		ilStatus = false;
		if(m_Abto.GetWindowTextLength() > 0)
			olErrorText += LoadStg(IDS_STRING851) + ogNotFormat;
	}

	if(m_Blen.GetStatus() == false) 
	{
		ilStatus = false;
		if(m_Blen.GetWindowTextLength() > 0)
			olErrorText += LoadStg(IDS_STRING852) + ogNotFormat;
	}

	///////////////////////////////////////////////////////////////////
	//Konsistenz-Check//
	if(ilStatus == true)
	{
		CString olText;
		char clWhere[100];


		CCSPtrArray<BSDDATA> olBsdList;
		sprintf(clWhere,"WHERE BSDC='%s'",olText);
		if(ogBsdData.ReadSpecialData(&olBsdList,clWhere,"URNO,BSDC",false) == true)
		{
			ilStatus = false;
			olErrorText += LoadStg(IDS_STRING237) + LoadStg(IDS_EXISTINBSD);
		}
		olBsdList.DeleteAll();

		//G�ltigkeit Code
		CCSPtrArray<ODADATA> olOdaCPA;
		m_SDAC.GetWindowText(olText);
		olNewCode = olText;
		sprintf(clWhere,"WHERE SDAC='%s'",olText);
		if(ogOdaData.ReadSpecialData(&olOdaCPA,clWhere,"URNO,SDAC",false) == true)
		{
			if(olOdaCPA[0].Urno != pomOda->Urno)
			{
				ilStatus = false;
				olErrorText += LoadStg(IDS_STRING237) + LoadStg(IDS_EXIST);
			}
		}
		olOdaCPA.DeleteAll();

		CString olTableErrorText;
		int i;

		for(i = 0; i < 50; i++)
		{
			int ilFilledCells = 0;
			bool blLineOK = true;
			char pclZeile[10], pclSpalte[10];
			CCSPtrArray<TABLE_COLUMN> olLine;
			//////////////////////////////////////////////////////////////////////////////////////////
			pomTable->GetTextLineColumns(&olLine, i);
			for(int k = 0; k < olLine.GetSize()-1; k++)
			{
				if(olLine[k].Text != "")
				{
					ilFilledCells++;
				}
				sprintf(pclZeile, "%d", i+1);
				blLineOK = pomTable->GetCellStatus(i, k);
				if(blLineOK == false)
				{
					ilStatus = false;
					sprintf(pclSpalte, "%d", k+1);
					if(k == 0 || k == 1)
					{
						olTableErrorText += LoadStg(IDS_STRING80) + LoadStg(IDS_STRING85) + CString(pclZeile) + LoadStg(IDS_STRING86) + CString(pclSpalte) + LoadStg(IDS_STRING87);
					}
				}
			}

			olLine.RemoveAll();
		}
	}
	///////////////////////////////////////////////////////////////////
	AfxGetApp()->DoWaitCursor(-1);

	if(ilStatus && m_Abfr.GetWindowTextLength() > 0 && m_Abto.GetWindowTextLength() == 0)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING854);
	}

	if(ilStatus && m_Abfr.GetWindowTextLength() == 0 && m_Abto.GetWindowTextLength() > 0)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING853);
	}

	if (ilStatus == true)
	{
		bool blChangeCode = true;
		if(olNewCode != omOldCode && bmChangeAction)
		{
			// Referenz Check
			CheckReferenz olCheckReferenz;
			if(pomStatus != NULL) pomStatus->SetWindowText(LoadStg(IDS_STRING120));
			int ilCount = olCheckReferenz.Check(_ODA, pomOda->Urno, omOldCode);
			if(pomStatus != NULL) pomStatus->SetWindowText(LoadStg(ST_OK));
			CString olTxt;
			if(ilCount>0)
			{
				blChangeCode = false;
				if (!ogBasicData.BackDoorEnabled())
				{
					olTxt.Format("%d %s",ilCount, LoadStg(IDS_STRING119));
					MessageBox(olTxt,LoadStg(IDS_STRING145),MB_ICONERROR);
				}
				else
				{
					olTxt.Format("%d %s",ilCount, LoadStg(IDS_STRING983));
					if(IDYES == MessageBox(olTxt,LoadStg(IDS_STRING149),(MB_ICONSTOP | MB_YESNOCANCEL | MB_DEFBUTTON2)))
					{
						blChangeCode = true;
					}
				}
			}
			// END Referenz Check
		}

		if(blChangeCode)
		{
			m_SDAC.GetWindowText(pomOda->Sdac,8+1);
			m_SDAE.GetWindowText(pomOda->Sdae,8+1);
			m_SDAK.GetWindowText(pomOda->Sdak,12+1);
			m_SDAN.GetWindowText(pomOda->Sdan,40+1);
			m_SDAS.GetWindowText(pomOda->Sdas,8+1);
			//uhi 19.6.01
			m_SDAA.GetWindowText(pomOda->Sdaa, 3+1);

			//uhi 20.9.01
			CString olText;
			
			if (m_Abfr.GetWindowTextLength() > 0 &&m_Abto.GetWindowTextLength() > 0){
				m_Abfr.GetWindowText(olText); 
				olText = olText.Left(2) + olText.Right(2);
				strcpy(pomOda->Abfr, olText);

				m_Abto.GetWindowText(olText); 
				olText = olText.Left(2) + olText.Right(2);
				strcpy(pomOda->Abto, olText);
			}
			else{
				olText = CString("");
				strcpy(pomOda->Abfr, olText);
				strcpy(pomOda->Abto, olText);
			}

			if(m_Blen.GetWindowTextLength() > 0){
				m_Blen.GetWindowText(olText); 
				olText = olText.Left(2) + olText.Right(2);
				strcpy(pomOda->Blen, olText);
			}
			else{
				olText = CString("");
				strcpy(pomOda->Blen, olText);
			}


			if (m_R_Free.GetCheck() == 1)
			{
				pomOda->Free[0]='x';
				pomOda->Type[0]=' ';
			}
			else if (m_R_Training.GetCheck() == 1)
			{
				pomOda->Free[0]=' ';
				pomOda->Type[0]='T';
			}
			else if (m_R_Changeover.GetCheck() == 1)
			{
				pomOda->Free[0]=' ';
				pomOda->Type[0]='S';
			}
			else if (m_R_Compensation.GetCheck() == 1)
			{
				pomOda->Free[0]=' ';
				pomOda->Type[0]='C';
			}
			else if (m_R_Illness.GetCheck() == 1)
			{
				pomOda->Free[0]=' ';
				pomOda->Type[0]='I';
			}
			else if (m_R_Compensation_Night.GetCheck() == 1)
			{
				pomOda->Free[0]=' ';
				pomOda->Type[0]='N';
			}
			else
			{
				pomOda->Free[0]=' ';
				pomOda->Type[0]=' ';
			}
			
  			if(m_C_Tsap.GetCheck() == 1) pomOda->Tsap[0] = 'Y';
			else						 pomOda->Tsap[0] = ' ';
			

  			if(m_WORK.GetCheck() == 1) pomOda->Work[0] = '1';
			else					   pomOda->Work[0] = '0';

  			if(m_TBSD.GetCheck() == 1) pomOda->Tbsd[0] = '1';
			else					   pomOda->Tbsd[0] = '0';

			if(m_C_Upln.GetCheck() == 1) pomOda->Upln[0] = 'Y';
			else			 		     pomOda->Upln[0] = ' ';

			//wandelt Return in Blank//
			CString olTemp;
			m_REMA.GetWindowText(olTemp);
			for(int i=0;i>-1;)
			{
				i = olTemp.Find("\r\n");
				if(i != -1)
				{
					if(olTemp.GetLength() > (i+2))
						olTemp = olTemp.Left(i) + " " + olTemp.Mid(i+2);
					else
						olTemp = olTemp.Left(i);
				}
			}
			strcpy(pomOda->Rema,olTemp);

			sprintf(pomOda->Rgbc,"%6x",this->omRgbc);

			////////////////////////////
			int ilOacCount = 0;
			for(i = 0; i < 50; i++)
			{
				bool blLineOK = true;
				CCSPtrArray<TABLE_COLUMN> olLine;
				/////////////////////////////////////////////////////////////////////////////
				// Schreibe Daten aus pomOrgTable in OACTAB
				/////////////////////////////////////////////////////////////////////////////
				pomTable->GetTextLineColumns(&olLine, i);
				int test = olLine.GetSize();
				for(int k = 0; k < olLine.GetSize(); k++)
				{
					blLineOK = pomTable->GetCellStatus(i, k);
				}
				if(blLineOK == true)
				{
					if(olLine.GetSize() > 1)
					{
						if(olLine[0].Text != CString(""))
						{
							if(olLine[1].Text == CString(""))
							{
								OACDATA *prlOac = new OACDATA;
								strcpy(prlOac->Ctrc, olLine[0].Text);
								strcpy(prlOac->Sdac, pomOda->Sdac);//Odgc 4

								prlOac->IsChanged = DATA_NEW;
								pomOacData->Add(prlOac);
							}
							else
							{
								int ilTmpUrno = atoi(olLine[1].Text);
								//Nach �nderungen Checken
								bool blFound = false;
								for(int j = 0; j < pomOacData->GetSize(); j ++)
								{
									if(ilTmpUrno == pomOacData->GetAt(j).Urno)
									{
										OACDATA *prlOac = ogOacData.GetOacByUrno(pomOacData->GetAt(j).Urno);
										if(olLine[0].Text != CString(pomOacData->GetAt(j).Ctrc))
										{
											if(prlOac != NULL)
											{
												strcpy(prlOac->Ctrc, olLine[0].Text);
												strcpy(prlOac->Sdac,pomOda->Sdac);//Odgc 4

												prlOac->IsChanged = DATA_CHANGED;
												ogOacData.UpdateInternal(prlOac);
												blFound = true;
												break;
											}
										}
										else
										{
											prlOac->IsChanged = DATA_UNCHANGED;
											blFound = true;
											break;
										}
									}
								}
							}
						}//Alle Columns sind gef�llt
						else
						{
							if (olLine[0].Text == CString(""))
							{
								if(olLine[1].Text != CString(""))
								{
									int ilTmpUrno = atoi(olLine[1].Text);
									bool blFound = false;
									for(int j = 0; j < pomOacData->GetSize(); j ++)
									{
										if(ilTmpUrno == pomOacData->GetAt(j).Urno)
										{
											OACDATA *prlOac = ogOacData.GetOacByUrno(pomOacData->GetAt(j).Urno);
											if(prlOac != NULL)
											{
												prlOac->IsChanged = DATA_DELETED;
											}
											break;
										}
									}
								}
							}
						}
					}
				}
				else
				{
					ilOacCount++;
				}
				olLine.RemoveAll();
			}
			CDialog::OnOK();
		}
	}	
	else
	{
		Beep(440,70);
		MessageBox(olErrorText,LoadStg(IDS_STRING145),MB_ICONERROR);
		m_SDAC.SetFocus();
		m_SDAC.SetSel(0,-1);
	}
}

//----------------------------------------------------------------------------------------

void DiensteUndAbwesenheitenDlg::OnBAwCot() 
{
	CStringArray olCol1, olCol2, olCol3;
	CCSPtrArray<COTDATA> olList;
	if(ogCotData.ReadSpecialData(&olList, "", "URNO,CTRC,CTRN", false) == true)
	{
		for(int i = 0; i < olList.GetSize(); i++)
		{
			COTDATA rlCot = olList[i];
			char pclUrno[20]="";
			sprintf(pclUrno, "%ld", olList[i].Urno);
			olCol3.Add(CString(pclUrno));
			olCol1.Add(CString(olList[i].Ctrc));
			olCol2.Add(CString(olList[i].Ctrn));
		}
		AwDlg *pomDlg = new AwDlg(&olCol1, &olCol2, &olCol3, LoadStg(IDS_STRING237), LoadStg(IDS_STRING229), this);
		if(pomDlg->DoModal() == IDOK)
		{
			m_CTRC.SetInitText(pomDlg->omReturnString, false);
			m_CTRC.SetFocus();
		}
		delete pomDlg;
	}
	else
	{
		MessageBox(LoadStg(IDS_STRING321), LoadStg(IDS_STRING149), (MB_ICONSTOP|MB_OK));
	}
	olList.DeleteAll();
}

//----------------------------------------------------------------------------------------

LONG DiensteUndAbwesenheitenDlg::OnKillfocus(UINT lParam, LONG wParam) 
{
	if(lParam == (UINT)m_CTRC.imID) 
	{
		if(m_CTRC.GetWindowTextLength() != 0) 
		{
			char clWhere[100];
			CCSPtrArray<COTDATA> olCotList;
			CString olTmp;m_CTRC.GetWindowText(olTmp);
			sprintf(clWhere,"WHERE CTRC='%s'",olTmp);
			if(ogCotData.ReadSpecialData(&olCotList, clWhere, "CTRC,CTRN", false) == true)
			{
				m_CTRC2.SetInitText(CString(olCotList[0].Ctrn));
			}
			else
			{
				m_CTRC2.SetInitText("");
			}
			olCotList.DeleteAll();
		}
		else
		{
			m_CTRC2.SetInitText("");
		}
	}
	return 0L;
}

//------------------------------------------------------------------------------------------------------------------------

LONG DiensteUndAbwesenheitenDlg::OnTableIPEditKillfocus( UINT wParam, LPARAM lParam)
{
	CCSIPEDITNOTIFY *prlNotify = (CCSIPEDITNOTIFY*)lParam;

	if(prlNotify->Text.IsEmpty())
		return 0L;
	
	if(prlNotify->SourceTable == pomTable) 
	{
		if(prlNotify->Column == 0)
		{
			CedaCotData olData;
			CCSPtrArray <COTDATA> omData;
			char pclWhere[128];
			sprintf(pclWhere,"WHERE CTRC='%s'", prlNotify->Text);
			prlNotify->UserStatus = true;
			prlNotify->Status = olData.ReadSpecialData(&omData, pclWhere, "CTRC", false);
			omData.DeleteAll();
			return 0L;
		}
	}
	return 0L;
}

//------------------------------------------------------------------------------------------------------------------------
// Implementierung
//------------------------------------------------------------------------------------------------------------------------
void DiensteUndAbwesenheitenDlg::InitTables()
{
	CRect olRect;
	m_HELP_OAC.GetWindowRect(olRect);
	ScreenToClient(olRect);
	olRect.DeflateRect(2,2);

	int ilTab = olRect.right - olRect.left - 15; 
	pomTable->SetHeaderSpacing(0);
	pomTable->SetMiniTable();
	pomTable->SetTableEditable(true);
    pomTable->SetTableData(this, olRect.left, olRect.right, olRect.top, olRect.bottom);
	pomTable->SetSelectMode(0);

	CCSPtrArray <TABLE_HEADER_COLUMN> omHeaderDataArray;
	TABLE_HEADER_COLUMN *prlHeader[2];
	pomTable->SetShowSelection(false);
	pomTable->ResetContent();

	prlHeader[0] = new TABLE_HEADER_COLUMN;
	prlHeader[0]->Alignment = COLALIGN_CENTER;
	prlHeader[0]->Length = (int)(ilTab - 7); 
	prlHeader[0]->Font = &ogCourier_Regular_8;
	prlHeader[0]->Text = CString(""); 

	prlHeader[1] = new TABLE_HEADER_COLUMN;
	prlHeader[1]->Alignment = COLALIGN_CENTER;
	prlHeader[1]->Length = 0; 
	prlHeader[1]->Font = &ogCourier_Regular_10;
	prlHeader[1]->Text = CString("");

	for(int ili = 0; ili < 2; ili++)
	{
		omHeaderDataArray.Add(prlHeader[ili]);
	}

	pomTable->SetHeaderFields(omHeaderDataArray);
	pomTable->SetDefaultSeparator();
	pomTable->SetTableEditable(true);
	pomTable->DisplayTable();
	omHeaderDataArray.DeleteAll();


// Set attributes for table
	CCSEDIT_ATTRIB rlAttribC1;

	rlAttribC1.Format = "X(8)";
	rlAttribC1.TextMinLenght = 0;
	rlAttribC1.TextMaxLenght = 8;
	rlAttribC1.Style = ES_UPPERCASE;
	
	CCSPtrArray<TABLE_COLUMN> olColList;
	TABLE_COLUMN rlColumnData;
	rlColumnData.Font = &ogCourier_Regular_8;//ogMS_Sans_Serif_8;
	

		for(int i = 0; i < pomOacData->GetSize(); i++)
		{
			rlColumnData.SeparatorType = SEPA_NONE;
			rlColumnData.VerticalSeparator = SEPA_LIKEVERTICAL;

			rlColumnData.Text = CString(pomOacData->GetAt(i).Ctrc);
			rlColumnData.EditAttrib = rlAttribC1;

			rlColumnData.SeparatorType = SEPA_NONE;
			rlColumnData.VerticalSeparator = SEPA_NONE;
			olColList.NewAt(olColList.GetSize(), rlColumnData);
			
			if(pomOacData->GetAt(i).Urno != 0)
			{
				char pclText[20];
				sprintf(pclText, "%ld", pomOacData->GetAt(i).Urno);
				rlColumnData.Text = CString(pclText);
			}
			else
			{
				rlColumnData.Text = CString("");
			}
			rlColumnData.SeparatorType = SEPA_NONE;
			rlColumnData.VerticalSeparator = SEPA_NONE;
			rlColumnData.HorizontalSeparator = SEPA_NONE;
			olColList.NewAt(olColList.GetSize(), rlColumnData);

			pomTable->AddTextLine(olColList, (void*)(NULL));
			olColList.DeleteAll();
		}
		for(i = pomOacData->GetSize(); i < 50; i++)
		{
			rlColumnData.SeparatorType = SEPA_NONE;
			rlColumnData.VerticalSeparator = SEPA_LIKEVERTICAL;
			rlColumnData.Text = CString("");
			rlColumnData.EditAttrib = rlAttribC1;
			rlColumnData.SeparatorType = SEPA_NONE;
			rlColumnData.VerticalSeparator = SEPA_NONE;
			olColList.NewAt(olColList.GetSize(), rlColumnData);

			rlColumnData.Text = CString("");
			rlColumnData.SeparatorType = SEPA_NONE;
			rlColumnData.VerticalSeparator = SEPA_NONE;
			olColList.NewAt(olColList.GetSize(), rlColumnData);

			pomTable->AddTextLine(olColList, (void*)(NULL));
			olColList.DeleteAll();
		}
//	}
	pomTable->DisplayTable();
	pomTable->SetColumnEditable(0, true);
	pomTable->SetColumnEditable(1, false);
}
//------------------------------------------------------------------------------------------------------------------------

void DiensteUndAbwesenheitenDlg::OnB1() 
{
	OnBf_Button(0);
	return;
}

void DiensteUndAbwesenheitenDlg::OnB2() 
{
	OnBf_Button(1);
	return;
}

void DiensteUndAbwesenheitenDlg::OnB3() 
{
	OnBf_Button(2);
	return;
}

void DiensteUndAbwesenheitenDlg::OnB4() 
{
	OnBf_Button(3);
	return;
}

//------------------------------------------------------------------------------------------------------------------------

void DiensteUndAbwesenheitenDlg::OnBf_Button(int ipColY)
{
	CStringArray olCol1, olCol2, olCol3;
	CCSPtrArray<COTDATA> olList;
	AfxGetApp()->DoWaitCursor(1);
	if(ogCotData.ReadSpecialData(&olList, "", "URNO,CTRC,CTRN", false) == true)
	{
		AfxGetApp()->DoWaitCursor(-1);
		for(int i = 0; i < olList.GetSize(); i++)
		{
			char pclUrno[20]="";
			sprintf(pclUrno, "%ld", olList[i].Urno);
			olCol3.Add(CString(pclUrno));
			olCol1.Add(CString(olList[i].Ctrc));
			olCol2.Add(CString(olList[i].Ctrn));
		}
		AwDlg *pomDlg = new AwDlg(&olCol1, &olCol2, &olCol3, LoadStg(IDS_STRING237), LoadStg(IDS_STRING229), this);
		if(pomDlg->DoModal() == IDOK)
		{
			int ilColX = -1, ilColY = -1;
			CCSTableListBox *pomListBox = pomTable->pomListBox;

			if(pomListBox->GetVisibleLeftTopColumn(ilColX, ilColY) == true)
			{
				ilColY += ipColY;
				pomTable->SetIPValue(ilColY, 0, pomDlg->omReturnString);
				pomTable->imCurrentColumn = 0;
				pomTable->MakeInplaceEdit(ilColY);
			}
		}
		delete pomDlg;
	}
	else
	{
		MessageBox(LoadStg(IDS_STRING321), LoadStg(IDS_STRING149), (MB_ICONSTOP|MB_OK));
	}
	olList.DeleteAll();

	AfxGetApp()->DoWaitCursor(-1);
}

void DiensteUndAbwesenheitenDlg::OnRegularAbsence()
{
	this->CheckRadioButton(IDC_R_REGULAR_ABSENCE,IDC_R_COMPENSATION_NIGHT,IDC_R_REGULAR_ABSENCE);	
	this->m_C_Upln.SetCheck(0);
	this->m_WORK.SetCheck(0);
	this->m_TBSD.SetCheck(0);

	this->m_C_Upln.EnableWindow(TRUE);
	this->m_WORK.EnableWindow(TRUE);
	this->m_TBSD.EnableWindow(TRUE);

}

void DiensteUndAbwesenheitenDlg::OnRegularFree()
{
	this->CheckRadioButton(IDC_R_REGULAR_ABSENCE,IDC_R_COMPENSATION_NIGHT,IDC_R_REGULAR_FREE);	
	this->m_C_Upln.SetCheck(0);
	this->m_WORK.SetCheck(0);
	this->m_TBSD.SetCheck(0);

	this->m_C_Upln.EnableWindow(FALSE);
	this->m_WORK.EnableWindow(FALSE);
	this->m_TBSD.EnableWindow(FALSE);
}

void DiensteUndAbwesenheitenDlg::OnShiftChangeoverDay()
{
	this->CheckRadioButton(IDC_R_REGULAR_ABSENCE,IDC_R_COMPENSATION_NIGHT,IDC_R_SHIFT_CHANGEOVER_DAY);	
	this->m_C_Upln.SetCheck(0);
	this->m_WORK.SetCheck(0);
	this->m_TBSD.SetCheck(0);

	this->m_C_Upln.EnableWindow(FALSE);
	this->m_WORK.EnableWindow(FALSE);
	this->m_TBSD.EnableWindow(FALSE);

}

void DiensteUndAbwesenheitenDlg::OnTraining()
{
	this->CheckRadioButton(IDC_R_REGULAR_ABSENCE,IDC_R_COMPENSATION_NIGHT,IDC_R_TRAINING);	
	this->m_C_Upln.SetCheck(0);
	this->m_WORK.SetCheck(1);
	this->m_TBSD.SetCheck(0);

	this->m_C_Upln.EnableWindow(FALSE);
	this->m_WORK.EnableWindow(TRUE);
	this->m_TBSD.EnableWindow(TRUE);

}

void DiensteUndAbwesenheitenDlg::OnCompensation()
{
	this->CheckRadioButton(IDC_R_REGULAR_ABSENCE,IDC_R_COMPENSATION_NIGHT,IDC_R_COMPENSATION);	
	this->m_C_Upln.SetCheck(0);
	this->m_WORK.SetCheck(0);
	this->m_TBSD.SetCheck(0);

	this->m_C_Upln.EnableWindow(FALSE);
	this->m_WORK.EnableWindow(TRUE);
	this->m_TBSD.EnableWindow(TRUE);
}

void DiensteUndAbwesenheitenDlg::OnIllness()
{
	this->CheckRadioButton(IDC_R_REGULAR_ABSENCE,IDC_R_COMPENSATION_NIGHT,IDC_R_ILLNESS);	
	this->m_C_Upln.SetCheck(0);
	this->m_WORK.SetCheck(0);
	this->m_TBSD.SetCheck(1);

	this->m_C_Upln.EnableWindow(FALSE);
	this->m_WORK.EnableWindow(TRUE);
	this->m_TBSD.EnableWindow(TRUE);
}

void DiensteUndAbwesenheitenDlg::OnCompensation_Night()
{
	this->CheckRadioButton(IDC_R_REGULAR_ABSENCE,IDC_R_COMPENSATION_NIGHT,IDC_R_COMPENSATION_NIGHT);	
	this->m_C_Upln.SetCheck(0);
	this->m_WORK.SetCheck(0);
	this->m_TBSD.SetCheck(0);

	this->m_C_Upln.EnableWindow(FALSE);
	this->m_WORK.EnableWindow(TRUE);
	this->m_TBSD.EnableWindow(TRUE);
}


//---------------------------------------------------------------------------
void DiensteUndAbwesenheitenDlg::OnRgbc() 
{
	CColorDialog olDlg(this->omRgbc,0,this);
	if (olDlg.DoModal() == IDOK)
	{
		this->omRgbc = olDlg.GetColor();
		this->Invalidate();
	}
}

//---------------------------------------------------------------------------
HBRUSH DiensteUndAbwesenheitenDlg::OnCtlColor( CDC* pDC, CWnd* pWnd, UINT nCtlColor )
{
	CWnd *polWnd;// = GetDlgItem(IDC_ADD_STANDARD);
	polWnd = GetDlgItem(IDC_EDIT_RGBC);
	if (polWnd && (polWnd->m_hWnd == pWnd->m_hWnd))
	{
		pDC->SetBkColor(this->omRgbc);		
		pDC->SetTextColor(this->omRgbc);

		omBrush.DeleteObject();
		omBrush.CreateSolidBrush(this->omRgbc);

		HBRUSH hr = omBrush;	
		return hr;

	}

	return CDialog::OnCtlColor(pDC,pWnd,nCtlColor);
}

