// RCTableViewer.cpp: implementation of the CRCTableViewer class.  //PRF 8378
//
//////////////////////////////////////////////////////////////////////

#include <stdafx.h>
#include <CRCTableViewer.h>
#include <CcsDdx.h>
#include <resource.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif


// Local function prototype
static void ChgReasonCodeTableCf(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CRCTableViewer::CRCTableViewer(CCSPtrArray<CRCDATA> *popData)
{
	pomData = popData;

	bmIsFromSearch = FALSE;
    pomCRCTable = NULL;
    ogDdx.Register(this, CRC_CHANGE, CString("CRCTableViewer"), CString("ChgReasonCode Update"), ChgReasonCodeTableCf);
    ogDdx.Register(this, CRC_NEW,    CString("CRCTableViewer"), CString("ChgReasonCode New"),    ChgReasonCodeTableCf);
    ogDdx.Register(this, CRC_DELETE, CString("CRCTableViewer"), CString("ChgReasonCode Delete"), ChgReasonCodeTableCf);

}

CRCTableViewer::~CRCTableViewer()
{
	ogDdx.UnRegister(this, NOTUSED);
    DeleteAll();
	omHeaderDataArray.DeleteAll();
}

//-----------------------------------------------------------------------------------------------

void CRCTableViewer::Attach(CCSTable *popTable)
{
    pomCRCTable = popTable;
}

//-----------------------------------------------------------------------------------------------

void CRCTableViewer::ChangeViewTo(const char *pcpViewName)
{
    DeleteAll();    
    MakeLines();
	UpdateDisplay();
}

//-----------------------------------------------------------------------------------------------

void CRCTableViewer::MakeLines()
{
	int ilChgReasonCodeCount = pomData->GetSize();
	for (int ilLc = 0; ilLc < ilChgReasonCodeCount; ilLc++)
	{
		CRCDATA *prlChgReasonCodeData = &pomData->GetAt(ilLc);
		MakeLine(prlChgReasonCodeData);
	}
}

//-----------------------------------------------------------------------------------------------

void CRCTableViewer::MakeLine(CRCDATA *prpChgReasonCode)
{

    //if( !IsPassFilter(prpChgReasonCode)) return;

    // Update viewer data for this shift record
    CRCTABLE_LINEDATA rlChgReasonCode;

	rlChgReasonCode.Urno = prpChgReasonCode->Urno;	
	rlChgReasonCode.Code = prpChgReasonCode->Code;
	rlChgReasonCode.Rema = prpChgReasonCode->Rema;
	rlChgReasonCode.Gatf = prpChgReasonCode->Gatf;
	rlChgReasonCode.Posf = prpChgReasonCode->Posf;
	rlChgReasonCode.Cxxf = prpChgReasonCode->Cxxf;
	rlChgReasonCode.Belt = prpChgReasonCode->Belt;
	rlChgReasonCode.Vafr = prpChgReasonCode->Vafr.Format("%d.%m.%Y %H:%M");;
	rlChgReasonCode.Vato = prpChgReasonCode->Vato.Format("%d.%m.%Y %H:%M");;

	CreateLine(&rlChgReasonCode);
}

//-----------------------------------------------------------------------------------------------

void CRCTableViewer::CreateLine(CRCTABLE_LINEDATA *prpChgReasonCode)
{
    int ilLineCount = omLines.GetSize();
    for (int ilLineno = ilLineCount; ilLineno > 0; ilLineno--)
        if (CompareChgReasonCode(prpChgReasonCode, &omLines[ilLineno-1]) >= 0)
	        break;							 // should be inserted after Lines[ilLineno-1]
	CRCTABLE_LINEDATA rlChgReasonCode;
	rlChgReasonCode = *prpChgReasonCode;
    omLines.NewAt(ilLineno, rlChgReasonCode);
}

//-----------------------------------------------------------------------------------------------
// UpdateDisplay: Load data selected by filter conditions to the display by using "omTable"

void CRCTableViewer::UpdateDisplay()
{
	int ilLines = omLines.GetSize();
	const int ilColumns = 1;	//<===== Spaltenanzahl setzen (von 0 ab)
	omHeaderDataArray.DeleteAll();

	pomCRCTable->SetShowSelection(TRUE);
	pomCRCTable->ResetContent();

	TABLE_HEADER_COLUMN rlHeader;
	rlHeader.Alignment = COLALIGN_LEFT;
	rlHeader.Font = &ogCourier_Regular_10;

	int ilPos = 1;
	/*rlHeader.Length = 42; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING32796),ilPos++,true,'|');// Home Airport
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);*/
	rlHeader.Length = 42; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING32796),ilPos++,true,'|');// Reason Code
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 498; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING32796),ilPos++,true,'|');// Reason Description
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 8; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING32796),ilPos++,true,'|');// Flag for Gate Changes
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	
	rlHeader.Length = 8; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING32796),ilPos++,true,'|');// Flag for Position Changes
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);

	if (bgCxxfDisp)
	{
		rlHeader.Length = 8; 
		rlHeader.Text = GetListItem(LoadStg(IDS_STRING32796),ilPos++,true,'|');// Flag for CXX Changes
		omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	}
	else{
		ilPos++;
	}
	
	rlHeader.Length = 8; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING32796),ilPos++,true,'|');// Flag for Belt Changes
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);

	rlHeader.Length = 133; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING32796),ilPos++,true,'|');// Valid from
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 133; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING32796),ilPos++,true,'|');// Valid To
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	
	pomCRCTable->SetHeaderFields(omHeaderDataArray);

	pomCRCTable->SetDefaultSeparator();
	pomCRCTable->SetTableEditable(FALSE);

	TABLE_COLUMN rlColumnData;
	rlColumnData.Font = &ogCourier_Regular_10;
	rlColumnData.BkColor = WHITE;
	rlColumnData.Alignment = COLALIGN_LEFT;
	
    for (int ilLineNo = 0; ilLineNo < ilLines; ilLineNo++)
	{
		CCSPtrArray<TABLE_COLUMN> olColList;
		
		rlColumnData.Text = omLines[ilLineNo].Code;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		
		rlColumnData.Text = omLines[ilLineNo].Rema;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		
		(omLines[ilLineNo].Gatf == 'X') ? rlColumnData.Text = cgYes : rlColumnData.Text = cgNo;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		
		(omLines[ilLineNo].Posf == 'X') ? rlColumnData.Text = cgYes : rlColumnData.Text = cgNo;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		if (bgCxxfDisp)
		{
			(omLines[ilLineNo].Cxxf == 'X') ? rlColumnData.Text = cgYes : rlColumnData.Text = cgNo;
			olColList.NewAt(olColList.GetSize(), rlColumnData);
		}

		(omLines[ilLineNo].Belt == 'X') ? rlColumnData.Text = cgYes : rlColumnData.Text = cgNo;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		
		rlColumnData.Text = omLines[ilLineNo].Vafr;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Vato;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		pomCRCTable->AddTextLine(olColList,(void*)NULL);
		olColList.DeleteAll();
	}
	pomCRCTable->DisplayTable();
}

//-----------------------------------------------------------------------------------------------

CString CRCTableViewer::Format(CRCTABLE_LINEDATA *prpLine)
{
    CString s;
	// ???
    return s;
}

//-----------------------------------------------------------------------------------------------

bool CRCTableViewer::FindChgReasonCode(char *pcpChgReasonCodeKeya, char *pcpChgReasonCodeKeyd, int& ilItem)
{
	int ilCount = omLines.GetSize();
    for (ilItem = 0; ilItem < ilCount; ilItem++)
    {
/*		if ( (omLines[ilItem].Keya == pcpChgReasonCodeKeya) &&
			 (omLines[ilItem].Keyd == pcpChgReasonCodeKeyd)    )
			return TRUE;*/
	}
	return false;
}

//-----------------------------------------------------------------------------------------------

static void ChgReasonCodeTableCf(void *popInstance, int ipDDXType,
								void *vpDataPointer, CString &ropInstanceName)
{
    CRCTableViewer *polViewer = (CRCTableViewer *)popInstance;
    if (ipDDXType == CRC_CHANGE || ipDDXType == CRC_NEW) polViewer->ProcessChgReasonCodeChange((CRCDATA *)vpDataPointer);
    if (ipDDXType == CRC_DELETE) polViewer->ProcessChgReasonCodeDelete((CRCDATA *)vpDataPointer);
} 


//-----------------------------------------------------------------------------------------------

void CRCTableViewer::ProcessChgReasonCodeChange(CRCDATA *prpChgReasonCode)
{
	int ilItem = 0;
	int ilColumnNo = 0;
	TABLE_COLUMN rlColumn;
	CCSPtrArray <TABLE_COLUMN> olLine;

	rlColumn.Lineno = ilItem;
	rlColumn.VerticalSeparator = SEPA_NONE;
	rlColumn.SeparatorType = SEPA_NONE;
	rlColumn.BkColor = WHITE;
	rlColumn.Font = &ogCourier_Regular_10;
	rlColumn.Alignment = COLALIGN_LEFT;

	rlColumn.Text = prpChgReasonCode->Code;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpChgReasonCode->Rema;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	(*prpChgReasonCode->Gatf == 'X') ? rlColumn.Text = cgYes : rlColumn.Text = cgNo;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	(*prpChgReasonCode->Posf == 'X') ? rlColumn.Text = cgYes : rlColumn.Text = cgNo;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	if (bgCxxfDisp)
	{
		(*prpChgReasonCode->Cxxf == 'X') ? rlColumn.Text = cgYes : rlColumn.Text = cgNo;
		olLine.NewAt(olLine.GetSize(), rlColumn);
	}
	(*prpChgReasonCode->Belt == 'X') ? rlColumn.Text = cgYes : rlColumn.Text = cgNo;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpChgReasonCode->Vafr.Format("%d.%m.%Y %H:%M");;
	olLine.NewAt(olLine.GetSize(), rlColumn);

	rlColumn.Text = prpChgReasonCode->Vato.Format("%d.%m.%Y %H:%M");;
	olLine.NewAt(olLine.GetSize(), rlColumn);

	
	if (FindLine(prpChgReasonCode->Urno, ilItem))
	{
        CRCTABLE_LINEDATA *prlLine = &omLines[ilItem];

		prlLine->Urno = prpChgReasonCode->Urno;		
		prlLine->Code = prpChgReasonCode->Code;

		prlLine->Rema = prpChgReasonCode->Rema;
		prlLine->Gatf = prpChgReasonCode->Gatf;
		prlLine->Posf = prpChgReasonCode->Posf;
		if (bgCxxfDisp)
		{
			prlLine->Cxxf = prpChgReasonCode->Cxxf;
		}
		prlLine->Belt = prpChgReasonCode->Belt;

		prlLine->Vafr = prpChgReasonCode->Vafr.Format("%d.%m.%Y %H:%M");
		prlLine->Vato = prpChgReasonCode->Vato.Format("%d.%m.%Y %H:%M");

		pomCRCTable->ChangeTextLine(ilItem, &olLine, (void *)prlLine);
		pomCRCTable->DisplayTable();
	}
	else
	{
		MakeLine(prpChgReasonCode);
		if (FindLine(prpChgReasonCode->Urno, ilItem))
		{
	        CRCTABLE_LINEDATA *prlLine = &omLines[ilItem];
			if(prlLine != NULL)
			{
				pomCRCTable->AddTextLine(olLine, (void *)prlLine);
				pomCRCTable->DisplayTable();
			}
		}
	}
	olLine.DeleteAll();
}

//-----------------------------------------------------------------------------------------------

void CRCTableViewer::ProcessChgReasonCodeDelete(CRCDATA *prpChgReasonCode)
{
	int ilItem;
	if (FindLine(prpChgReasonCode->Urno, ilItem))
	{
		DeleteLine(ilItem);
		pomCRCTable->DeleteTextLine(ilItem);
		pomCRCTable->DisplayTable();
	}
}

//-----------------------------------------------------------------------------------------------

bool CRCTableViewer::IsPassFilter(CRCDATA *prpChgReasonCode)
{

	return true;
}

//-----------------------------------------------------------------------------------------------

int CRCTableViewer::CompareChgReasonCode(CRCTABLE_LINEDATA *prpChgReasonCode1, CRCTABLE_LINEDATA *prpChgReasonCode2)
{
/*	int ilCase=0;
	int	ilCompareResult;
	if(prpChgReasonCode1->Tifd == prpChgReasonCode2->Tifd)
	{
		ilCompareResult= 0;
	}
	else
	{
		if(prpChgReasonCode1->Tifd > prpChgReasonCode2->Tifd)
		{
			ilCompareResult = 1;
		}
		else
		{
			ilCompareResult = -1;
		}
	}
	return ilCompareResult;
*/  
	return 0;
}

//-----------------------------------------------------------------------------------------------

void CRCTableViewer::DeleteLine(int ipLineno)
{
	omLines.DeleteAt(ipLineno);
}

//-----------------------------------------------------------------------------------------------

bool CRCTableViewer::FindLine(long lpUrno, int &rilLineno)
{
    for (rilLineno = 0; rilLineno < omLines.GetSize(); rilLineno++)
	{
        if (omLines[rilLineno].Urno == lpUrno)
            return true;
	}
    return false;
}
//-----------------------------------------------------------------------------------------------

void CRCTableViewer::DeleteAll()
{
    while (omLines.GetSize() > 0)
		DeleteLine(0);
}

//-----------------------------------------------------------------------------------------------
// Printing Routines
//-----------------------------------------------------------------------------------------------

void CRCTableViewer::PrintTableView()
{
	CString olFooter1,olFooter2;
	omTableName = LoadStg(IDS_STRING32795);
	int ilOrientation = PRINT_LANDSCAPE;//  PRINT_PORTRAET  PRINT_LANDSCAPE
	pomPrint = new CCSPrint((CWnd*)this,ilOrientation,45);// FirstLine(P=200,L=200),LeftOffset(P=200,L=50)
	
	if (pomPrint != NULL)
	{
		if (pomPrint->InitializePrinter(ilOrientation) == TRUE)
		{
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			pomPrint->imLineNo = pomPrint->imMaxLines + 1;
			DOCINFO	rlDocInfo;
			memset(&rlDocInfo, 0, sizeof(DOCINFO));
			rlDocInfo.cbSize = sizeof( DOCINFO );
			rlDocInfo.lpszDocName = omTableName;	
			pomPrint->omCdc.StartDoc( &rlDocInfo );
			pomPrint->imPageNo = 0;
			int ilLines = omLines.GetSize();
			olFooter1.Format("%d %s",ilLines,omTableName);
			for(int i = 0; i < ilLines; i++ ) 
			{
				if(pomPrint->imLineNo >= pomPrint->imMaxLines)
				{
					if(pomPrint->imPageNo > 0)
					{
						olFooter2.Format("%s: %d",LoadStg(IDS_STRING222),pomPrint->imPageNo);
						pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
						pomPrint->omCdc.EndPage();
					}
					PrintTableHeader();
				}
				if(pomPrint->imLineNo == (pomPrint->imMaxLines-1) || i == (ilLines-1))
				{
					PrintTableLine(&omLines[i],true);
				}
				else
				{
					PrintTableLine(&omLines[i],false);
				}
			}
			olFooter2.Format("%s: %d",LoadStg(IDS_STRING222),pomPrint->imPageNo);
			pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
			pomPrint->omCdc.EndPage();
			pomPrint->omCdc.EndDoc();
		}
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	delete pomPrint;
	//omBitmap.DeleteObject();
	pomPrint = NULL;
	}
}

//-----------------------------------------------------------------------------------------------

bool CRCTableViewer::PrintTableHeader()
{
	pomPrint->omCdc.StartPage();
	pomPrint->imPageNo++;
	pomPrint->imLineNo = 0;

	pomPrint->PrintUIFHeader(omTableName,CString(CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")),pomPrint->imFirstLine-10);

	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;

	rlElement.Alignment   = PRINT_LEFT;
	rlElement.FrameLeft   = PRINT_NOFRAME;
	rlElement.FrameRight  = PRINT_NOFRAME;
	rlElement.FrameTop    = PRINT_NOFRAME;
	rlElement.FrameBottom = PRINT_FRAMEMEDIUM;
	rlElement.pFont       = &pomPrint->ogCourierNew_Bold_8;

	int ilSize = omHeaderDataArray.GetSize();
	for(int i=0;i<ilSize;i++)
	{
		//if(i!=1&&i!=2)// <= setzen, wenn Spalten nicht angezeigt werden sollen 
		{
			rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
			if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
			rlElement.Text   = omHeaderDataArray[i].Text;
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		}
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();


	return true;
}

//-----------------------------------------------------------------------------------------------

bool CRCTableViewer::PrintTableLine(CRCTABLE_LINEDATA *prpLine,bool bpLastLine)
{
	bool blPrint = false;
	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;
	rlElement.pFont      = &pomPrint->ogCourierNew_Regular_8;
	int ilSize = omHeaderDataArray.GetSize();
	for(int i=0;i<ilSize;i++)
	{
		blPrint = true;
		//if(i!=1&&i!=1)// <= setzen, wenn Spalten nicht angezeigt werden sollen 
		{
			rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
			if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
			rlElement.Alignment  = PRINT_LEFT;
			rlElement.FrameTop   = PRINT_FRAMETHIN;
			if(bpLastLine == true)
			{
				rlElement.FrameBottom = PRINT_FRAMETHIN;
			}
			else
			{
				rlElement.FrameBottom = PRINT_NOFRAME;
			}
			rlElement.FrameLeft  = PRINT_NOFRAME;
			rlElement.FrameRight = PRINT_NOFRAME;

			switch(i)
			{
			case 0:
				rlElement.FrameLeft  = PRINT_FRAMETHIN;
				rlElement.Text		 = prpLine->Code;
				break;
			case 1:
				rlElement.Text		 = prpLine->Rema;
				break;
			case 2:
				rlElement.Text		 = prpLine->Gatf;
				break;
			case 3:
				rlElement.Text		 = prpLine->Posf;
				break;
			case 4:
				if(bgCxxfDisp){
					rlElement.Text		 = prpLine->Cxxf;					
				}
				else{
					blPrint = false;
				}
				break;
			case 5:
				rlElement.Text		 = prpLine->Belt;
				break;
			case 6:
				rlElement.Text		= prpLine->Vafr;
				break;
			case 7:
				rlElement.FrameRight = PRINT_FRAMETHIN;
				rlElement.Text		= prpLine->Vato;
				break;

			}
			if (blPrint)
			{
				rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
			}
		}
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();
	return true;
}

//-----------------------------------------------------------------------------------------------