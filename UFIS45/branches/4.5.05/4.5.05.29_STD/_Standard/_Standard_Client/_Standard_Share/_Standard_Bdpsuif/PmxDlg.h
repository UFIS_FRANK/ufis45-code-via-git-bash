#if !defined(AFX_PMXDLG_H__D6712674_E86B_11D4_BFFA_00D0B7E2A4B5__INCLUDED_)
#define AFX_PMXDLG_H__D6712674_E86B_11D4_BFFA_00D0B7E2A4B5__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// PmxDlg.h : header file
//
#include <PrivList.h>
#include <CCSEdit.h>
#include <AwDlg.h>
#include <CCSGlobl.h>
#include <CCSTable.h>
#include <CedaPmxData.h>
#include <CedaPacData.h>
#include <CedaCotData.h>


/////////////////////////////////////////////////////////////////////////////
// CPmxDlg dialog

class CPmxDlg : public CDialog
{
// Construction
public:
	CPmxDlg(PMXDATA *popPmx, CCSPtrArray<PACDATA> *popPacData, CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CPmxDlg)
	enum { IDD = IDD_PMXDLG };
	CButton	m_OK;
	CCSEdit	m_CDATD;
	CCSEdit	m_CDATT;
	CStatic	m_HELP_PAC;
	CCSEdit	m_POMO;
	CCSEdit	m_POTU;
	CCSEdit	m_POWE;
	CCSEdit	m_POTH;
	CCSEdit	m_POFR;
	CCSEdit	m_POSA;
	CCSEdit	m_POSU;
	CCSEdit	m_TIFR;
	CCSEdit	m_LSTUD;
	CCSEdit	m_LSTUT;
	CCSEdit	m_USEC;
	CCSEdit	m_USEU;
	//uhi 7.5.01
	CCSEdit	m_VAFR;
	CCSEdit	m_VATO;

	CString m_Caption;
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPmxDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

private:
	PMXDATA *pomPmx;
	void OnBf_Button(int ipColY);
	CString GetCodeShort(CString opCode);
	LONG OnTableIPEditKillfocus( UINT wParam, LPARAM lParam);
	void InitTables();

	// Implementation
protected:

	CCSTable *pomTable;
	PACDATA *pomPac;
	
	CCSPtrArray <COTDATA> *pomCotData;
	CCSPtrArray <PACDATA> *pomPacData;

	// Generated message map functions
	//{{AFX_MSG(CPmxDlg)
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	afx_msg void OnB1();
	afx_msg void OnB2();
	afx_msg void OnB3();
	afx_msg void OnB4();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PMXDLG_H__D6712674_E86B_11D4_BFFA_00D0B7E2A4B5__INCLUDED_)
