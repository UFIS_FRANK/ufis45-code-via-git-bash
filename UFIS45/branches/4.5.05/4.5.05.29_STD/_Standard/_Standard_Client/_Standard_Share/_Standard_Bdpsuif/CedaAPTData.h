// CedaAPTData.h

#ifndef __CEDAAPTDATA__
#define __CEDAAPTDATA__
 
#include <stdafx.h>
#include <basicdata.h>

//---------------------------------------------------------------------------------------------------------
// Record structure declaration

struct APTDATA 
{
	long 	 Urno;		// Eindeutige Datensatz-Nr.
	CTime	 Cdat;		// Erstellungsdatum
	char 	 Usec[34]; 	// Anwender (Ersteller)
	CTime	 Lstu;	 	// Datum letzte �nderung
	char 	 Prfl[3]; 	// Protokollierungskennzeichen
	char 	 Useu[34]; 	// Anwender (letzte �nderung)
	char 	 Apc3[5]; 	// Flughafen 3-Letter Code
	char 	 Apc4[6]; 	// Flughafen 4-Letter Code
	char 	 Apfn[34]; 	// Flughafen Name
	char 	 Land[4]; 	// Landeskennung
	char 	 Aptt[3]; 	// Landeskennung
	char 	 Etof[6]; 	// Standardflugzeit
	CTime	 Tich;	 	// Uhrzeitwechsel in GMT
	char 	 Tdi1[6]; 	// Zeitdifferenz 1 bis TICH
	char 	 Tdi2[6]; 	// Zeitdifferenz 2 bis TICH
	char 	 Apsn[256]; 	// Flughafen Kurzname 1
	char 	 Apn2[256]; 	// Flughafen Kurzname 2
	char 	 Apn3[256]; 	// Flughafen Kurzname 3
	char 	 Apn4[256]; 	// Flughafen Kurzname 4
	CTime	 Vafr;	 	// G�ltig von
	CTime	 Vato;	 	// G�ltig bis
	char 	 Home[4]; 	// Flughafen Kurzname 4
	char	 Tdis[6];	// Zeitdifferenz Sommer
	char	 Tdiw[6];	// Zeitdifferenz Winter

	//DataCreated by this class
	int      IsChanged;
	char	 Cont[14];//continent Urno

	APTDATA(void)
	{	memset(this,'\0',sizeof(*this));
		Cdat=-1;
		Lstu=-1;
		Tich=-1;
		Vafr=-1;
		Vato=-1;
	}


}; // end APTDataStrukt


//---------------------------------------------------------------------------------------------------------
// Class declaratino


class CedaAPTData: public CCSCedaData
{
// Attributes
public:
    CMapPtrToPtr omUrnoMap;

    CCSPtrArray<APTDATA> omData;
	CString m_strSelConUrno;

// Operations
public:
    CedaAPTData();
	~CedaAPTData();
	virtual void GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType);
	void Register(void);
	void ClearAll(void);

    bool Read(char *pcpWhere = NULL);
	bool ReadSpecialData(CCSPtrArray<APTDATA> *popApt,char *pspWhere,char *pspFieldList,bool ipSYS = true);
	bool InsertAPT(APTDATA *prpAPT,BOOL bpSendDdx = TRUE);
	bool InsertAPTInternal(APTDATA *prpAPT);
	bool UpdateAPT(APTDATA *prpAPT,BOOL bpSendDdx = TRUE);
	bool UpdateAPTInternal(APTDATA *prpAPT);
	bool DeleteAPT(long lpUrno);
	bool DeleteAPTInternal(APTDATA *prpAPT);
	APTDATA  *GetAPTByUrno(long lpUrno);
	bool SaveAPT(APTDATA *prpAPT);
	char pcmAPTFieldList[2048];
	void ProcessAPTBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
	void SetWhere(const CString& ropWhere){omWhere = ropWhere;} //Prf: 8795
	void GetWhere(CString& ropWhere) const{ropWhere = omWhere;} //Prf: 8795
	void SetTheSelectedContinent(CString strContinentUrno);
	CString GetTheSelectedContinent();

	// Private methods
private:
    void PrepareAPTData(APTDATA *prpAPTData);
	CString omWhere; //Prf: 8795
	bool ValidateAPTBcData(const long& lrpUnro); //Prf: 8795

};

//---------------------------------------------------------------------------------------------------------

#endif //__CEDAAPTDATA__
