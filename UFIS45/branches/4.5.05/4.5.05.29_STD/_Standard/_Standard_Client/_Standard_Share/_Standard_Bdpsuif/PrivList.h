#ifndef __PRIVLIST__
#define __PRIVLIST__

#include <CCSCedaData.h>



#define PL_FLDLEN 100
#define PL_TABLENAMELEN 25
#define PL_ERRORMESSLEN 500


struct PRIVLIST {

	char	FLD1[PL_FLDLEN];
	char	FLD2[PL_FLDLEN];
	char	FLD3[PL_FLDLEN];

	PRIVLIST(void)
	{
		strcpy(FLD1, "");
		strcpy(FLD2, "");
		strcpy(FLD3, "");
	}
};




class PrivList: public CCSCedaData
{
private:

	// Clear the privilege list, called in Init().
	void ClearAll(void);

	// Add a record to the privilege list, called in Login().
	bool Add(PRIVLIST *prpPrv);

    CMapStringToPtr omFLD1Map; //Urno
	CMapStringToPtr omFLD2Map; //Func

public:

	CCSPtrArray <PRIVLIST> omData;
	
	//Last error message returned from Login().
	CString omErrorMessage;

	//Constructor.
	PrivList();

	//Destructor.
	~PrivList();

	bool Login(const char *pcpHomeAirport, const char *pcpUsid, const char *pcpPass, const char *pcpAppl, const char *pcpWks);

	char GetStat( const char *pcpFunc);
	
	int GetSize();

	bool Add(const CString& prpPrivilegList);
};

extern PrivList ogPrivList;
extern PrivList ogPrmPrivList;

#endif //__PRIVLIST__
