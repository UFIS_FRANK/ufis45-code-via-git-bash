// CedaPgpData.cpp
 
#include <stdafx.h>
#include <CedaPgpData.h>
#include <resource.h>


void ProcessPgpCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//--CEDADATA-----------------------------------------------------------------------------------------------

CedaPgpData::CedaPgpData() : CCSCedaData(&ogCommHandler)
{
	// Create an array of CEDARECINFO for PGPDATA
	BEGIN_CEDARECINFO(PGPDATA,PgpDataRecInfo)
		FIELD_LONG		(Urno,"URNO")
		FIELD_DATE		(Cdat,"CDAT")
		FIELD_DATE		(Lstu,"LSTU")
		FIELD_CHAR_TRIM	(Usec,"USEC")
		FIELD_CHAR_TRIM	(Useu,"USEU")
		FIELD_CHAR_TRIM	(Prfl,"PRFL")
		FIELD_CHAR_TRIM	(Pgpc,"PGPC")
		FIELD_CHAR_TRIM	(Type,"TYPE")
		FIELD_CHAR_TRIM	(Pgpn,"PGPN")
		FIELD_CHAR_TRIM	(Rema,"REMA")
		FIELD_CHAR_TRIM	(Pgpm,"PGPM")
		FIELD_CHAR_TRIM	(Minm,"MINM")
		FIELD_CHAR_TRIM	(Maxm,"MAXM")
	END_CEDARECINFO //(PGPDATA)

	// Copy the record structure
	for (int i=0; i< sizeof(PgpDataRecInfo)/sizeof(PgpDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&PgpDataRecInfo[i],sizeof(CEDARECINFO));
		omRecInfo.Add(prpCedaRecInfo);
	} // end for

	// initialize field names
	strcpy(pcmTableName,"PGP");
	strcpy(pcmListOfFields,"URNO,CDAT,LSTU,USEC,USEU,PRFL,PGPC,TYPE,PGPN,REMA,PGPM,MINM,MAXM");
	pcmFieldList = pcmListOfFields; //because in CedaData pcmFieldList is just a pointer

	omData.RemoveAll();
}

//---------------------------------------------------------------------------------------------------------

void CedaPgpData::GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType)
{
	ropFields.Add("URNO");			ropDesription.Add(LoadStg(IDS_STRING343));		ropType.Add("String");
	ropFields.Add("CDAT");			ropDesription.Add(LoadStg(IDS_STRING344));		ropType.Add("Date");
	ropFields.Add("LSTU");			ropDesription.Add(LoadStg(IDS_STRING345));		ropType.Add("Date");
	ropFields.Add("USEC");			ropDesription.Add(LoadStg(IDS_STRING347));		ropType.Add("String");
	ropFields.Add("USEU");			ropDesription.Add(LoadStg(IDS_STRING348));		ropType.Add("String");
	ropFields.Add("PGPC");			ropDesription.Add(LoadStg(IDS_STRING237));		ropType.Add("String");
	ropFields.Add("PGPN");			ropDesription.Add(LoadStg(IDS_STRING229));		ropType.Add("String");
	ropFields.Add("REMA");			ropDesription.Add(LoadStg(IDS_STRING342));		ropType.Add("String");

	ropFields.Add("PRFL");			ropDesription.Add(LoadStg(IDS_STRING1053));		ropType.Add("String");
	ropFields.Add("TYPE");			ropDesription.Add(LoadStg(IDS_STRING1054));		ropType.Add("String");
	ropFields.Add("PGPM");			ropDesription.Add(LoadStg(IDS_STRING1055));		ropType.Add("String");
	ropFields.Add("MINM");			ropDesription.Add(LoadStg(IDS_STRING1056));		ropType.Add("String");
	ropFields.Add("MAXM");			ropDesription.Add(LoadStg(IDS_STRING1057));		ropType.Add("String");
	
	
	
	
	
	
	
	

	
	
	
	
	
	
	
	
}

//--REGISTER----------------------------------------------------------------------------------------------

void CedaPgpData::Register(void)
{
	ogDdx.Register((void *)this,BC_PGP_CHANGE,	CString("PGPDATA"), CString("Pgp-changed"),	ProcessPgpCf);
	ogDdx.Register((void *)this,BC_PGP_NEW,		CString("PGPDATA"), CString("Pgp-new"),		ProcessPgpCf);
	ogDdx.Register((void *)this,BC_PGP_DELETE,	CString("PGPDATA"), CString("Pgp-deleted"),	ProcessPgpCf);
}

//--~CEDADATA----------------------------------------------------------------------------------------------

CedaPgpData::~CedaPgpData(void)
{
	omRecInfo.DeleteAll();
	ClearAll();
}

//--CLEAR-ALL----------------------------------------------------------------------------------------------

void CedaPgpData::ClearAll(bool bpWithRegistration)
{
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(bpWithRegistration)
	{
		ogDdx.UnRegister(this,NOTUSED);
	}
}

//--READ-ALL-----------------------------------------------------------------------------------------------

bool CedaPgpData::Read(char *pspWhere /*NULL*/)
{
    // Select data from the database
	bool ilRc = true;
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(pspWhere == NULL)
	{	
		ilRc = CedaAction("RT");
	}
	else
	{
		ilRc = CedaAction("RT", pspWhere);
	}
	if (ilRc != true)
	{
		return ilRc;
	}
	// Load data from CedaData into the dynamic array of record
	for (int ilLc = 0; ilRc == true; ilLc++)
	{
		PGPDATA *prlPgp = new PGPDATA;
		if ((ilRc = GetFirstBufferRecord(prlPgp)) == true)
		{
			omData.Add(prlPgp);//Update omData
			omUrnoMap.SetAt((void *)prlPgp->Urno,prlPgp);
		}
		else
		{
			delete prlPgp;
		}
	}
    return true;
}

//--INSERT-------------------------------------------------------------------------------------------------

bool CedaPgpData::Insert(PGPDATA *prpPgp)
{
	prpPgp->IsChanged = DATA_NEW;
	if(Save(prpPgp) == false) return false; //Update Database
	InsertInternal(prpPgp);
    return true;
}

//--INSERT-INTERNAL--------------------------------------------------------------------------------------

bool CedaPgpData::InsertInternal(PGPDATA *prpPgp)
{
	ogDdx.DataChanged((void *)this, PGP_NEW,(void *)prpPgp ); //Update Viewer
	omData.Add(prpPgp);//Update omData
	omUrnoMap.SetAt((void *)prpPgp->Urno,prpPgp);
    return true;
}

//--DELETE-------------------------------------------------------------------------------------------------

bool CedaPgpData::Delete(long lpUrno)
{
	PGPDATA *prlPgp = GetPgpByUrno(lpUrno);
	if (prlPgp != NULL)
	{
		prlPgp->IsChanged = DATA_DELETED;
		if(Save(prlPgp) == false) return false; //Update Database
		DeleteInternal(prlPgp);
	}
    return true;
}

//--DELETE-INTERNAL----------------------------------------------------------------------------------------

bool CedaPgpData::DeleteInternal(PGPDATA *prpPgp)
{
	ogDdx.DataChanged((void *)this,PGP_DELETE,(void *)prpPgp); //Update Viewer
	omUrnoMap.RemoveKey((void *)prpPgp->Urno);
	int ilPgpCount = omData.GetSize();
	for (int ilLc = 0; ilLc < ilPgpCount; ilLc++)
	{
		if (omData[ilLc].Urno == prpPgp->Urno)
		{
			omData.DeleteAt(ilLc);//Update omData
			break;
		}
	}
    return true;
}

//--UPDATE-------------------------------------------------------------------------------------------------

bool CedaPgpData::Update(PGPDATA *prpPgp)
{
	if (GetPgpByUrno(prpPgp->Urno) != NULL)
	{
		if (prpPgp->IsChanged == DATA_UNCHANGED)
		{
			prpPgp->IsChanged = DATA_CHANGED;
		}
		if(Save(prpPgp) == false) return false; //Update Database
		UpdateInternal(prpPgp);
	}
    return true;
}

//--UPDATE-INTERNAL--------------------------------------------------------------------------------------------

bool CedaPgpData::UpdateInternal(PGPDATA *prpPgp)
{
	PGPDATA *prlPgp = GetPgpByUrno(prpPgp->Urno);
	if (prlPgp != NULL)
	{
		*prlPgp = *prpPgp; //Update omData
		ogDdx.DataChanged((void *)this,PGP_CHANGE,(void *)prlPgp); //Update Viewer
	}
    return true;
}

//--GET-BY-URNO--------------------------------------------------------------------------------------------

PGPDATA *CedaPgpData::GetPgpByUrno(long lpUrno)
{
	PGPDATA  *prlPgp;
	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlPgp) == TRUE)
	{
		return prlPgp;
	}
	return NULL;
}

//--READSPECIALDATA-------------------------------------------------------------------------------------

bool CedaPgpData::ReadSpecialData(CCSPtrArray<PGPDATA> *popPgp,char *pspWhere,char *pspFieldList,bool ipSYS/*=true*/)
{
	bool ilRc = true;
	char pclFieldList[256] = " ";

	if(strlen(pspFieldList) > 0) 
	{
		strcpy(pclFieldList, pspFieldList);
	}
	if(ipSYS == true) 
	{
		if (CedaAction("SYS","PGP",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	else
	{
		if (CedaAction("RT","PGP",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	if(popPgp != NULL)
	{
		for (int ilLc = 0; ilRc == true; ilLc++)
		{
			PGPDATA *prpPgp = new PGPDATA;
			if ((ilRc = GetBufferRecord(ilLc,prpPgp,CString(pclFieldList))) == true)
			{
				popPgp->Add(prpPgp);
			}
			else
			{
				delete prpPgp;
			}
		}
		if(popPgp->GetSize() == 0) return false;
	}
    return true;
}

//--SAVE---------------------------------------------------------------------------------------------------

bool CedaPgpData::Save(PGPDATA *prpPgp)
{
	bool ilRc = true;
	CString olListOfData;
	char pclSelection[124];
	char pclData[524];

	if (prpPgp->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}
	switch(prpPgp->IsChanged)
	{
	case DATA_NEW:
		MakeCedaData(&omRecInfo,olListOfData,prpPgp);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("IRT","","",pclData);
		prpPgp->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpPgp->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpPgp);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("URT",pclSelection,"",pclData);
		prpPgp->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_DELETED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpPgp->Urno);
		ilRc = CedaAction("DRT",pclSelection);
		break;
	}
    return ilRc;
}

//--PROCESS-CF---------------------------------------------------------------------------------------------

void  ProcessPgpCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	ogPgpData.ProcessBc(ipDDXType,vpDataPointer,ropInstanceName);
}

//--PROCESS-BC---------------------------------------------------------------------------------------------

void  CedaPgpData::ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlPgpData;
	prlPgpData = (struct BcStruct *) vpDataPointer;
	PGPDATA *prlPgp;
	if(ipDDXType == BC_PGP_NEW)
	{
		prlPgp = new PGPDATA;
		GetRecordFromItemList(prlPgp,prlPgpData->Fields,prlPgpData->Data);
		if(ValidatePgpBcData(prlPgp->Urno)) //Prf: 8795
		{
			InsertInternal(prlPgp);
		}
		else
		{
			delete prlPgp;
		}
	}
	if(ipDDXType == BC_PGP_CHANGE)
	{
		long llUrno = GetUrnoFromSelection(prlPgpData->Selection);
		prlPgp = GetPgpByUrno(llUrno);
		if(prlPgp != NULL)
		{
			GetRecordFromItemList(prlPgp,prlPgpData->Fields,prlPgpData->Data);
			if(ValidatePgpBcData(prlPgp->Urno)) //Prf: 8795
			{
				UpdateInternal(prlPgp);
			}
			else
			{
				DeleteInternal(prlPgp);
			}
		}
	}
	if(ipDDXType == BC_PGP_DELETE)
	{
		long llUrno = GetUrnoFromSelection(prlPgpData->Selection);

		prlPgp = GetPgpByUrno(llUrno);
		if (prlPgp != NULL)
		{
			DeleteInternal(prlPgp);
		}
	}
}

//Prf: 8795
//--ValidatePgpBcData--------------------------------------------------------------------------------------

bool CedaPgpData::ValidatePgpBcData(const long& lrpUrno)
{
	bool blValidatePgpBcData = true;
	if(!omWhere.IsEmpty()) //It means it is not the default view and we need to check in the database, since we don't need to check for the default view
	{
		char chWhere[20];
		sprintf(chWhere, "URNO='%ld'",lrpUrno);
		CString olWhere = omWhere + CString(" AND ") + CString(chWhere);
		CCSPtrArray<PGPDATA> olPgps;
		if(!ReadSpecialData(&olPgps,olWhere.GetBuffer(0),"URNO",false))
		{		
			blValidatePgpBcData = false;
		}
	}
	return blValidatePgpBcData;
}

//---------------------------------------------------------------------------------------------------------
