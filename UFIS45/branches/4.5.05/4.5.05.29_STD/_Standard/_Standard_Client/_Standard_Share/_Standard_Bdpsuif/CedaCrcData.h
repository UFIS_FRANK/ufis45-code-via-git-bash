// CedaCrcData.h: interface for the CedaCrcData class.   //PRF 8378
//
//////////////////////////////////////////////////////////////////////
#ifndef __CEDACRCDATA__
#define __CEDACRCDATA__

#include <stdafx.h>
#include <basicdata.h>

struct CRCDATA
{
	long	Urno;
	CTime	Cdat;		// Erstellungsdatum
	char 	Usec[34]; 	// Anwender (Ersteller)
	CTime	Lstu;	 	// Datum letzte �nderung
	char 	Useu[34]; 	// Anwender (letzte �nderung)
	char	Hopo[5];	// Home Airport
	char	Code[7];	// Reason Code
	char	Rema[130];	// Reason Description
	char	Gatf[3];	// Flag for Gate Changes
	char	Posf[3];	// Flag for Position Changes
	char	Cxxf[3];	// Flag for Flight Cancellation
	char	Belt[3];	// Flag for Belt Changes
	CTime	Vafr;		// G�ltig von
	CTime	Vato;		// G�ltig bis

	//DataCreated by this class
	int		 IsChanged;

	CRCDATA(void)
	{ 
		memset(this,'\0',sizeof(*this));
		Cdat=-1;
		Lstu=-1;
		Vafr=-1;
		Vato=-1;
	}
};


//---------------------------------------------------------------------------------------------------------
// Class declaratino

class CedaCrcData: public CCSCedaData
{
// Attributes
public:
    CMapPtrToPtr omUrnoMap;

    CCSPtrArray<CRCDATA> omData;

	char pcmListOfFields[2048];

// OAfmations
public:
    CedaCrcData();
	~CedaCrcData();
	virtual void GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType);
	void Register(void);
	void ClearAll(bool bpWithRegistration = true);
    bool Read(char *pspWhere = NULL);
	bool InsertCRC(CRCDATA *prpCrc);
	bool InsertInternal(CRCDATA *prpCrc);
	bool UpdateCRC(CRCDATA *prpCrc);
	bool UpdateInternal(CRCDATA *prpCrc);
	bool DeleteCRC(long lpUrno);
	bool DeleteInternal(CRCDATA *prpCrc);
	bool ReadSpecialData(CCSPtrArray<CRCDATA> *popCrc,char *pspWhere,char *pspFieldList,bool ipSYS=true);
	bool Save(CRCDATA *prpCrc);
	void ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
	CRCDATA  *GetCrcByUrno(long lpUrno);
	void SetWhere(const CString& ropWhere){omWhere = ropWhere;} //Prf: 8795
	void GetWhere(CString& ropWhere) const{ropWhere = omWhere;} //Prf: 8795


	// Private methods
private:
    void PrepareCrcData(CRCDATA *prpCrcData);
	CString omWhere; //Prf: 8795
	bool ValidateCrcBcData(const long& lrpUnro); //Prf: 8795

};

//---------------------------------------------------------------------------------------------------------
#endif // __CEDACRCDATA__