// CedaBLTData.h

#ifndef __CEDABLTDATA__
#define __CEDABLTDATA__
 
#include <stdafx.h>
#include <basicdata.h>

//---------------------------------------------------------------------------------------------------------
// Record structure declaration

struct BLTDATA 
{
	char 	 Bnam[7]; 	// Gep�ckband Name
	CTime	 Cdat; 		// Erstellungsdatum
	CTime	 Lstu; 		// Datum letzte �nderung
//	CTime	 Nafr; 		// Nicht verf�gbar von
//	CTime	 Nato; 		// Nicht verf�gbar bis
//	char 	 Resn[42];	// Grund der Sperrung
	char 	 Prfl[3]; 	// Protokollierungskennzeichen
	char 	 Tele[12]; 	// Telefonnummer
	long 	 Urno; 		// Eindeutige Datensatz-Nr.
	char 	 Usec[34]; 	// Anwender (Ersteller)
	char 	 Useu[34]; 	// Anwender (letzte �nderung)
	CTime	 Vafr; 		// G�ltig von
	CTime	 Vato; 		// G�ltig bis
	char 	 Term[3]; 	// Terminal
	char 	 Home[4]; 	// Terminal
	char 	 Bltt[2]; 	// Terminal
	char 	 Stat[4]; 	// Terminal
	char	 Maxf[3];	// Maximum no. of flights
	char	 Defd[5];   // Default allocation duration
	long	 Ibit;		// Index of bitmap for blocked times

	//DataCreated by this class
	int      IsChanged;

	BLTDATA(void)
	{ 
		memset(this,'\0',sizeof(*this));
		Cdat=-1;
		Lstu=-1;
//		Nafr=-1;
//		Nato=-1;
		Vafr=-1;
		Vato=-1;
	}

}; // end BLTDataStrukt


//---------------------------------------------------------------------------------------------------------
// Class declaratino

class CedaBLTData: public CCSCedaData
{
// Attributes
public:
    CMapPtrToPtr		omUrnoMap;
	CMapStringToPtr		omNameMap;

    CCSPtrArray<BLTDATA>omData;

// Operations
public:
    CedaBLTData();
	~CedaBLTData();
	virtual void GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType);
	void Register(void);
	void ClearAll(void);

    bool Read(char *pcpWhere = NULL);
	bool ReadSpecialData(CCSPtrArray<BLTDATA> *popBlt,char *pspWhere,char *pspFieldList,bool ipSYS = true);
	bool InsertBLT(BLTDATA *prpBLT,BOOL bpSendDdx = TRUE);
	bool InsertBLTInternal(BLTDATA *prpBLT);
	bool UpdateBLT(BLTDATA *prpBLT,BOOL bpSendDdx = TRUE);
	bool UpdateBLTInternal(BLTDATA *prpBLT);
	bool DeleteBLT(long lpUrno);
	bool DeleteBLTInternal(BLTDATA *prpBLT);
	BLTDATA  *GetBLTByUrno(long lpUrno);
	BLTDATA	 *GetBLTByName(const CString& ropName);
	bool SaveBLT(BLTDATA *prpBLT);
	char pcmBLTFieldList[2048];
	void ProcessBLTBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
	int	 GetBLTList(CStringArray& ropList);
	void SetWhere(const CString& ropWhere){omWhere = ropWhere;} //Prf: 8795
	void GetWhere(CString& ropWhere) const{ropWhere = omWhere;} //Prf: 8795
	// Private methods
private:
    void PrepareBLTData(BLTDATA *prpBLTData);
    void DeleteFromSgrSgm(const BLTDATA &ropBLT);
    bool MakeCedaData(CCSPtrArray<CEDARECINFO> *pomRecInfo,CString &pcpListOfData, void *pvpDataStruct);
	CString omWhere; //Prf: 8795
	bool ValidateBLTBcData(const long& lrpUnro); //Prf: 8795
};

//---------------------------------------------------------------------------------------------------------

#endif //__CEDABLTDATA__
