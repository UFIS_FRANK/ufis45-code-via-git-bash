// CedaPSTData.h

#ifndef __CEDAPSTDATA__
#define __CEDAPSTDATA__
 
#include <stdafx.h>
#include <basicdata.h>

//---------------------------------------------------------------------------------------------------------
// Record structure declaration

struct PSTDATA 
{
	char 	 Acus[3]; 	// ACU Status
	CTime	 Cdat; 		// Erstellungsdatum
	char 	 Fuls[3]; 	// Fuel Pit Status
	char 	 Gpus[3]; 	// GPU Status
	CTime	 Lstu; 		// Datum letzte �nderung
//	CTime	 Nafr; 		// Nicht verf�gbar vom
//	CTime	 Nato; 		// Nicht verf�gbar bis
	char 	 Pcas[3]; 	// PCA Status
	char 	 Pnam[7]; 	// Parking stands
	char 	 Prfl[3]; 	// Protokollierungskennzeichen
	char 	 Pubk[3]; 	// Pushback Position
	char 	 Taxi[7]; 	// Angeschlossener Taxiway
	char 	 Tele[12]; 	// Telefonnummer
	long 	 Urno; 		// Eindeutige Datensatz-Nr.
	char 	 Usec[34]; 	// Anwender (Ersteller)
	char 	 Useu[34]; 	// Anwender (letzte �nderung)
	CTime	 Vafr; 		// G�ltig von
	CTime	 Vato;		// G�ltig bis
//	char 	 Resn[42]; 	// Grund f�r die Sperrung
	char 	 Dgss[3]; 	// Andocksystem verf�gbar
	char 	 Brgs[3]; 	// Aussteigehilfe
	char 	 Home[5]; 	// Aussteigehilfe
	char 	 Mxac[6]; 	// Aussteigehilfe
	char	 Defd[5];	// Default allocation duration
	char	 Posr[2002];	// Rules
	long	 Ibit;		// Index of bitmap for blocked times	
	//DataCreated by this class
	int      IsChanged;

	PSTDATA(void)
	{ memset(this,'\0',sizeof(*this));
	  Cdat=-1;
	  Lstu=-1;
//	  Nafr=-1;
//	  Nato=-1;
	  Vafr=-1;
	  Vato=-1;
	}

}; // end PSTDataStrukt

//---------------------------------------------------------------------------------------------------------
// Class declaratino

class CedaPSTData: public CCSCedaData
{
// Attributes
public:
    CMapPtrToPtr omUrnoMap;

    CCSPtrArray<PSTDATA> omData;

	char pcmPSTFieldList[2048];

// Operations
public:
    CedaPSTData();
	~CedaPSTData();
	virtual void GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType);
	void Register(void);
	
	void ClearAll(void);

    bool Read(char *pcpWhere = NULL);
	bool ReadSpecialData(CCSPtrArray<PSTDATA> *popPst,char *pspWhere,char *pspFieldList,bool ipSYS = true);
	bool InsertPST(PSTDATA *prpPST,BOOL bpSendDdx = TRUE);
	bool InsertPSTInternal(PSTDATA *prpPST);
	bool UpdatePST(PSTDATA *prpPST,BOOL bpSendDdx = TRUE);
	bool UpdatePSTInternal(PSTDATA *prpPST);
	bool DeletePST(long lpUrno);
	bool DeletePSTInternal(PSTDATA *prpPST);
	PSTDATA  *GetPSTByUrno(long lpUrno);
	bool SavePST(PSTDATA *prpPST);
	void ProcessPSTBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
	void SetWhere(const CString& ropWhere){omWhere = ropWhere;} //Prf: 8795
	void GetWhere(CString& ropWhere) const{ropWhere = omWhere;} //Prf: 8795

	// Private methods
private:
    void PreparePSTData(PSTDATA *prpPSTData);
	void DeleteFromSgrSgm(const PSTDATA &ropPST);
    bool MakeCedaData(CCSPtrArray<CEDARECINFO> *pomRecInfo,CString &pcpListOfData, void *pvpDataStruct);
	CString omWhere; //Prf: 8795
	bool ValidatePSTBcData(const long& lrpUnro); //Prf: 8795

};

//---------------------------------------------------------------------------------------------------------

#endif //__CEDAPSTDATA__
