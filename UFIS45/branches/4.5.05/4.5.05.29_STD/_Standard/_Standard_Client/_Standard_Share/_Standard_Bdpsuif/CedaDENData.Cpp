// CedaDENData.cpp
 
#include <stdafx.h>
#include <CedaDENData.h>
#include <resource.h>
#include <process.h>

#define OPTION_SAVE 1
#define OPTION_READ 0

#define FIELD_LIST "CDAT,DECA,DECN,DENA,LSTU,PRFL,URNO,USEC,USEU,ALC3,VAFR,VATO"
// Local function prototype
static void ProcessDENCf(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//--CEDADATA-----------------------------------------------------------------------------------------------

CedaDENData::CedaDENData() : CCSCedaData(&ogCommHandler)
{
    // Create an array of CEDARECINFO for DENDATA
	BEGIN_CEDARECINFO(DENDATA,DENDataRecInfo)
		FIELD_DATE		(Cdat,"CDAT")
		FIELD_CHAR_TRIM	(Deca,"DECA")
		FIELD_CHAR_TRIM	(Decn,"DECN")
		FIELD_CHAR_TRIM	(Dena,"DENA")
		FIELD_DATE		(Lstu,"LSTU")
		FIELD_CHAR_TRIM	(Prfl,"PRFL")
		FIELD_LONG		(Urno,"URNO")
		FIELD_CHAR_TRIM	(Usec,"USEC")
		FIELD_CHAR_TRIM	(Useu,"USEU")
		FIELD_CHAR_TRIM	(Alc3,"ALC3")
		FIELD_DATE		(Vafr,"VAFR")
		FIELD_DATE		(Vato,"VATO")
		FIELD_CHAR_TRIM	(RCol,"RCOL")
		FIELD_CHAR_TRIM (Decs,"DECS")
	END_CEDARECINFO
	//(DENDATA)
	// Copy the record structure
	for (int i=0; i< sizeof(DENDataRecInfo)/sizeof(DENDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&DENDataRecInfo[i],sizeof(CEDARECINFO));
		omRecInfo.Add(prpCedaRecInfo);
	} // end for

	// initialize field names
	strcpy(pcmTableName,"DEN");
	strcpy(pcmDENFieldList,"CDAT,DECA,DECN,DENA,LSTU,PRFL,URNO,USEC,USEU,ALC3,VAFR,VATO");

	if (bgDEN_DECS_Exist) 
	{
		strcat(pcmDENFieldList,",DECS");
	}
	pcmFieldList = pcmDENFieldList;


	omData.SetSize(0,1000);
	//omUrnoMap.InitHashTable(2001);
}

//---------------------------------------------------------------------------------------------------------

void CedaDENData::GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType)
{
	ropFields.RemoveAll();ropDesription.RemoveAll();ropType.RemoveAll();

	ropFields.Add("ALC3");  ropDesription.Add(LoadStg(IDS_STRING436));  ropType.Add("String");
	ropFields.Add("CDAT");  ropDesription.Add(LoadStg(IDS_STRING343));  ropType.Add("Date");
	ropFields.Add("DECA");  ropDesription.Add(LoadStg(IDS_STRING449));  ropType.Add("String");
	ropFields.Add("DECN");  ropDesription.Add(LoadStg(IDS_STRING450));  ropType.Add("String");
	ropFields.Add("DENA");  ropDesription.Add(LoadStg(IDS_STRING451));  ropType.Add("String");
	ropFields.Add("LSTU");  ropDesription.Add(LoadStg(IDS_STRING344));  ropType.Add("Date");
	ropFields.Add("PRFL");  ropDesription.Add(LoadStg(IDS_STRING345));  ropType.Add("String");
	ropFields.Add("URNO");  ropDesription.Add(LoadStg(IDS_STRING346));  ropType.Add("String");
	ropFields.Add("USEC");  ropDesription.Add(LoadStg(IDS_STRING347));  ropType.Add("String");
	ropFields.Add("USEU");  ropDesription.Add(LoadStg(IDS_STRING348));  ropType.Add("String");
	ropFields.Add("VAFR");  ropDesription.Add(LoadStg(IDS_STRING230));  ropType.Add("Date");
	ropFields.Add("VATO");  ropDesription.Add(LoadStg(IDS_STRING231));  ropType.Add("Date");
	


	if (ogBasicData.IsColumnForAbnormalFlightsReportAvailable())
	{
		ropFields.Add("RCOL");  ropDesription.Add(LoadStg(IDS_STRING1109));  ropType.Add("String");
	}

	if (bgDEN_DECS_Exist)
	{
		ropFields.Add("DECS");  ropDesription.Add(LoadStg(IDS_STRING1161));  ropType.Add("String");
	}

}

//--------------------------------------------------------------------------------------------------------

void CedaDENData::Register(void)
{
	ogDdx.Register((void *)this,BC_DEN_CHANGE,CString("DENDATA"), CString("DEN-changed"),ProcessDENCf);
	ogDdx.Register((void *)this,BC_DEN_DELETE,CString("DENDATA"), CString("DEN-deleted"),ProcessDENCf);
}

//--~CEDADATA----------------------------------------------------------------------------------------------

CedaDENData::~CedaDENData(void)
{
	omRecInfo.DeleteAll();
	ClearAll();
}

//--CLEAR-ALL----------------------------------------------------------------------------------------------

void CedaDENData::ClearAll(void)
{
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	ogDdx.UnRegister(this,NOTUSED);
}

//--READ-ALL-----------------------------------------------------------------------------------------------

bool CedaDENData::Read(char *pcpWhere)
{

	/*AM:20100913 Commented out
	if (ogBasicData.IsColumnForAbnormalFlightsReportAvailable())
	{
		strcpy(pcmDENFieldList,"CDAT,DECA,DECN,DENA,LSTU,PRFL,URNO,USEC,USEU,ALC3,VAFR,VATO,RCOL");
		pcmFieldList = pcmDENFieldList;
	}
	*/

    PrepareFieldList(OPTION_READ);
	

    // Select data from the database
	bool ilRc = true;
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(pcpWhere != NULL)
		ilRc = CedaAction(ogBasicData.GetCedaCommand("RT"), pcpWhere);
	else
		ilRc = CedaAction(ogBasicData.GetCedaCommand("RT"));
	if (ilRc != true)
	{
		return false;
	}
	// Load data from CedaData into the dynamic array of record
	for (int ilLc = 0; ilRc == true; ilLc++)
	{
		DENDATA *prpDEN = new DENDATA;
		if ((ilRc = GetFirstBufferRecord(prpDEN)) == true)
		{
			prpDEN->IsChanged = DATA_UNCHANGED;
			omData.Add(prpDEN);//Update omData
			omUrnoMap.SetAt((void *)prpDEN->Urno,prpDEN);
		}
		else
		{
			delete prpDEN;
		}
	}
    return true;
}

//--INSERT-------------------------------------------------------------------------------------------------

bool CedaDENData::InsertDEN(DENDATA *prpDEN,BOOL bpSendDdx)
{
	prpDEN->IsChanged = DATA_NEW;
	if(SaveDEN(prpDEN) == false) return false; //Update Database
	InsertDENInternal(prpDEN);
    return true;
}

//--INSERT-INTERNAL--------------------------------------------------------------------------------------

bool CedaDENData::InsertDENInternal(DENDATA *prpDEN)
{
	//PrepareDENData(prpDEN);
	ogDdx.DataChanged((void *)this, DEN_CHANGE,(void *)prpDEN ); //Update Viewer
	omData.Add(prpDEN);//Update omData
	omUrnoMap.SetAt((void *)prpDEN->Urno,prpDEN);
    return true;
}

//--DELETE-------------------------------------------------------------------------------------------------

bool CedaDENData::DeleteDEN(long lpUrno)
{
	DENDATA *prlDEN = GetDENByUrno(lpUrno);
	if (prlDEN != NULL)
	{
		prlDEN->IsChanged = DATA_DELETED;
		if(SaveDEN(prlDEN) == false) return false; //Update Database
		DeleteDENInternal(prlDEN);
	}
    return true;
}

//--DELETE-INTERNAL----------------------------------------------------------------------------------------

bool CedaDENData::DeleteDENInternal(DENDATA *prpDEN)
{
	ogDdx.DataChanged((void *)this,DEN_DELETE,(void *)prpDEN); //Update Viewer
	omUrnoMap.RemoveKey((void *)prpDEN->Urno);
	int ilDENCount = omData.GetSize();
	for (int ilLc = 0; ilLc < ilDENCount; ilLc++)
	{
		if (omData[ilLc].Urno == prpDEN->Urno)
		{
			omData.DeleteAt(ilLc);//Update omData
			break;
		}
	}
    return true;
}

//--PREPARE-DATA-------------------------------------------------------------------------------------------

void CedaDENData::PrepareDENData(DENDATA *prpDEN)
{
	// TODO: add code here
}

//--UPDATE-------------------------------------------------------------------------------------------------

bool CedaDENData::UpdateDEN(DENDATA *prpDEN,BOOL bpSendDdx)
{
	if (GetDENByUrno(prpDEN->Urno) != NULL)
	{
		if (prpDEN->IsChanged == DATA_UNCHANGED)
		{
			prpDEN->IsChanged = DATA_CHANGED;
		}
		if(SaveDEN(prpDEN) == false) return false; //Update Database
		UpdateDENInternal(prpDEN);
	}
    return true;
}

//--UPDATE-INTERNAL--------------------------------------------------------------------------------------------

bool CedaDENData::UpdateDENInternal(DENDATA *prpDEN)
{
	DENDATA *prlDEN = GetDENByUrno(prpDEN->Urno);
	if (prlDEN != NULL)
	{
		*prlDEN = *prpDEN; //Update omData
		ogDdx.DataChanged((void *)this,DEN_CHANGE,(void *)prlDEN); //Update Viewer
	}
    return true;
}

//--GET-BY-URNO--------------------------------------------------------------------------------------------

DENDATA *CedaDENData::GetDENByUrno(long lpUrno)
{
	DENDATA  *prlDEN;
	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlDEN) == TRUE)
	{
		return prlDEN;
	}
	return NULL;
}

//--READSPECIALDATA-----------------------------------------------------------------------------------------

bool CedaDENData::ReadSpecialData(CCSPtrArray<DENDATA> *popDen,char *pspWhere,char *pspFieldList,bool ipSYS/*=true*/)
{
	bool ilRc = true;
	char pclFieldList[256] = " ";

	//PrepareFieldList(OPTION_READ);	 

	if(strlen(pspFieldList) > 0) 
	{
		strcpy(pclFieldList, pspFieldList);
	}
	if(ipSYS == true) 
	{
		if (CedaAction("SYS","DEN",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	else
	{
		if (CedaAction("RT","DEN",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	if(popDen != NULL)
	{
		for (int ilLc = 0; ilRc == true; ilLc++)
		{
			DENDATA *prpDen = new DENDATA;
			if ((ilRc = GetBufferRecord(ilLc,prpDen,CString(pclFieldList))) == true)
			{
				popDen->Add(prpDen);
			}
			else
			{
				delete prpDen;
			}
		}
		if(popDen->GetSize() == 0) return false;
	}
    return true;
}

//--SAVE---------------------------------------------------------------------------------------------------

void CedaDENData::PrepareFieldList( int option )
{
	CString olFields = FIELD_LIST;

	if (ogBasicData.IsColumnForAbnormalFlightsReportAvailable())
	{
		olFields += ",RCOL";

	}
	else
	{
		//if (option==OPTION_READ) olFields += ",''";
	}

	if (bgDEN_DECS_Exist) 
	{
		olFields += ",DECS";
	}
	else
	{
	//if (option==OPTION_READ) olFields += ",''";
	}


	strcpy(pcmDENFieldList, olFields );
	pcmFieldList = pcmDENFieldList;

}

bool CedaDENData::SaveDEN(DENDATA *prpDEN)
{
	bool ilRc = true;
	CString olListOfData;
	char pclSelection[124];
	char pclData[524];

	if (prpDEN->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}

	switch(prpDEN->IsChanged)
	{
	case DATA_NEW:
		PrepareFieldList(OPTION_SAVE);
		MakeCedaData(&omRecInfo,olListOfData,prpDEN);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("IRT","","",pclData);
		prpDEN->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_CHANGED:
		PrepareFieldList(OPTION_SAVE);
		sprintf(pclSelection, "WHERE URNO = %ld", prpDEN->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpDEN);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("URT",pclSelection,"",pclData);
		prpDEN->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_DELETED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpDEN->Urno);
		ilRc = CedaAction("DRT",pclSelection);
		break;
	}
	PrepareFieldList(OPTION_READ);

    return ilRc;
}

//--PROCESS-CF---------------------------------------------------------------------------------------------

void  ProcessDENCf(void * popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	switch(ipDDXType)
	{
	case BC_DEN_CHANGE :
	case BC_DEN_DELETE :
		((CedaDENData *)popInstance)->ProcessDENBc(ipDDXType,vpDataPointer,ropInstanceName);
		break;
	}
}

//--PROCESS-BC---------------------------------------------------------------------------------------------

void  CedaDENData::ProcessDENBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlDENData = (struct BcStruct *) vpDataPointer;
	long llUrno = GetUrnoFromSelection(prlDENData->Selection);

	DENDATA *prlDEN = GetDENByUrno(llUrno);
	if(ipDDXType == BC_DEN_CHANGE)
	{
		if (prlDEN != NULL)
		{
			GetRecordFromItemList(prlDEN,prlDENData->Fields,prlDENData->Data);
			if(ValidateDENBcData(prlDEN->Urno)) //Prf: 8795
			{
				UpdateDENInternal(prlDEN);
			}
			else
			{
				DeleteDENInternal(prlDEN);
			}
		}
		else
		{
			prlDEN = new DENDATA;
			GetRecordFromItemList(prlDEN,prlDENData->Fields,prlDENData->Data);
			if(ValidateDENBcData(prlDEN->Urno)) //Prf: 8795
			{
				InsertDENInternal(prlDEN);
			}
			else
			{
				delete prlDEN;
			}
		}
	}
	if(ipDDXType == BC_DEN_DELETE)
	{
		if (prlDEN != NULL)
		{
			DeleteDENInternal(prlDEN);
		}
	}
}

//Prf: 8795
//--ValidateDENBcData--------------------------------------------------------------------------------------

bool CedaDENData::ValidateDENBcData(const long& lrpUrno)
{
	bool blValidateDENBcData = true;
	if(!omWhere.IsEmpty()) //It means it is not the default view and we need to check in the database, since we don't need to check for the default view
	{
		char chWhere[20];
		sprintf(chWhere, "URNO='%ld'",lrpUrno);
		CString olWhere = omWhere + CString(" AND ") + CString(chWhere);
		CCSPtrArray<DENDATA> olDens;
		if(!ReadSpecialData(&olDens,olWhere.GetBuffer(0),"URNO",false))
		{		
			blValidateDENBcData = false;
		}
	}
	return blValidateDENBcData;
}

//---------------------------------------------------------------------------------------------------------
bool CedaDENData::MakeCedaData(CString &pcpListOfData, CString &pcpFieldList, void *pvpSaveDataStruct, void *pvpChangedDataStruct)
{
	return MakeCedaData(&omRecInfo,pcpListOfData, pcpFieldList, pvpSaveDataStruct, pvpChangedDataStruct);
}

//---------------------------------------------------------------------------------------------------------
bool CedaDENData::MakeCedaData(CCSPtrArray<CEDARECINFO> *pomRecInfo,CString &pcpListOfData, CString &pcpFieldList, void *pvpSaveDataStruct, void *pvpChangedDataStruct)
{
    pcpListOfData = "";
	char pclBuf[128];
    char buf[33];   // ltoa() need buffer 33 bytes
    CString sSave;
    CString sChanged;
    CString sOrg;
    CString pclFieldList;
	int ilFieldListPos = 0;
	int ilFieldListLength = 0;
	int ilTmpPos = 0;
	int ilLc = 0;

    // Start conversion, field-by-field
    int nfields = pomRecInfo->GetSize();

    
	for (int fieldno = 0; fieldno < nfields; fieldno++)
    {
        int type = (*pomRecInfo)[fieldno].Type;

        void *pSave    = (void *)((char *)pvpSaveDataStruct + (*pomRecInfo)[fieldno].Offset);
        void *pChanged = (void *)((char *)pvpChangedDataStruct + (*pomRecInfo)[fieldno].Offset);

        int length = (*pomRecInfo)[fieldno].Length;

		if (strstr(this->pcmFieldList,(*pomRecInfo)[fieldno].Name) == NULL)
			continue;

        switch (type) 
		{
        case CEDACHAR:
			{
				sSave = (length == 0)? CString(" "): CString((char *)pSave).Left(length);
				MakeCedaString(sSave);
				sChanged = (length == 0)? CString(" "): CString((char *)pChanged).Left(length);
				MakeCedaString(sChanged);
			}
            break;
        case CEDAINT:
			{
				sSave	 = CString(itoa(*(int *)pSave, buf, 10));
				sChanged = CString(itoa(*(int *)pChanged, buf, 10));
			}
            break;
        case CEDALONG:
			{
				sSave	 = CString(ltoa(*(long *)pSave, buf, 10));
				sChanged = CString(ltoa(*(long *)pChanged, buf, 10));
			}
            break;
        case CEDADATE:  // format: YYYYMMDDHHMMSS
			{
				sSave = ((CTime *)pSave)->Format("%Y%m%d%H%M00");
				sChanged = ((CTime *)pChanged)->Format("%Y%m%d%H%M00");
				sOrg = ((CTime *)pChanged)->Format("%Y%m%d%H%M%S");
			}
            break;
        case CEDAOLEDATE:  // format: YYYYMMDDHHMMSS
			{
				sSave = ((COleDateTime *)pSave)->Format("%Y%m%d%H%M%S");
				sChanged = ((COleDateTime *)pChanged)->Format("%Y%m%d%H%M%S");
			}
            break;
		case CEDADOUBLE:
			{
				sprintf(pclBuf,"%.2f",(double *)pSave);
				sSave = CString(pclBuf);
				sprintf(pclBuf,"%.2f",(double *)pChanged);
				sChanged = CString(pclBuf);
			}
			break;
        default:    // invalid field type
            return false;
        }

		ilTmpPos = ilFieldListPos;
		ilFieldListLength = pcpFieldList.GetLength();

		for( ilFieldListPos++ ; ((ilFieldListPos < ilFieldListLength)  && (pcpFieldList[ilFieldListPos] != ',')); ilFieldListPos++);

		if(sChanged != sSave)
		{
			if(type == CEDADATE)
				pcpListOfData += sOrg + ",";
			else
				pcpListOfData += sChanged + ",";

// TVO 02.02.2000
			if( (ilFieldListPos - ilTmpPos > 0) && (pcpFieldList.GetLength() > ilTmpPos + ilFieldListPos - ilTmpPos - 1) )
//			if( (ilFieldListPos - ilTmpPos > 0) && (pcpFieldList.GetLength() > ilTmpPos + ilFieldListPos - ilTmpPos ) )
				pclFieldList += pcpFieldList.Mid(ilTmpPos, ilFieldListPos - ilTmpPos);
		}

    }
	if(!pclFieldList.IsEmpty())
	{
		if(pclFieldList[0] == ',')
			pclFieldList.SetAt(0,' ');
		if(pclFieldList[pclFieldList.GetLength()-1] == ',')
			pclFieldList.SetAt(pclFieldList.GetLength()-1,' ');
		pclFieldList.TrimRight();
		pclFieldList.TrimLeft();
	}
	pcpFieldList = pclFieldList;
	if(!pcpListOfData.IsEmpty())
	{
		pcpListOfData = pcpListOfData.Left(pcpListOfData.GetLength()-1);
	}
    return true;
}

//---------------------------------------------------------------------------------------------------------
bool CedaDENData::MakeCedaData(CCSPtrArray<CEDARECINFO> *pomRecInfo,CString &pcpListOfData, void *pvpDataStruct)
{
    pcpListOfData = "";

	CString olFieldData;

    // Start conversion, field-by-field
    int nfields = pomRecInfo->GetSize();
    for (int fieldno = 0; fieldno < nfields; fieldno++)
    {

        CString s;
        char buf[33];   // ltoa() need buffer 33 bytes
        int type = (*pomRecInfo)[fieldno].Type;
        void *p = (void *)((char *)pvpDataStruct + (*pomRecInfo)[fieldno].Offset);
        int length = (*pomRecInfo)[fieldno].Length;

		if (strstr(this->pcmFieldList,(*pomRecInfo)[fieldno].Name) == NULL)
			continue;

        switch (type) {
        case CEDACHAR:
            s = (length == 0)? CString(" "): CString((char *)p).Left(length);
			MakeCedaString(s);
            olFieldData = s + ",";
            break;
        case CEDAINT:
            olFieldData = CString(itoa(*(int *)p, buf, 10)) + ",";
            break;
        case CEDALONG:
            olFieldData = CString(ltoa(*(long *)p, buf, 10)) + ",";
            break;
        case CEDADATE:  // format: YYYYMMDDHHMMSS
			olFieldData = ((CTime *)p)->Format("%Y%m%d%H%M%S") + ",";
            break;
        case CEDAOLEDATE:  // format: YYYYMMDDHHMMSS
			if(((COleDateTime *)p)->GetStatus() == COleDateTime::valid)
			{
				olFieldData = ((COleDateTime *)p)->Format("%Y%m%d%H%M%S") + ",";
			}
			else
			{
				olFieldData = " ,"; 
			}
            break;
		case CEDADOUBLE:
			char pclBuf[128];
//MWO 18.12.98
			sprintf(pclBuf,"%lf",(double *)p);
//			sprintf(pclBuf,"%.2f",(double *)p);
            olFieldData = CString(pclBuf) + ",";
			break;
        default:    // invalid field type
            return false;
        }
		pcpListOfData += olFieldData;
    }

	if(pcpListOfData.GetLength() > 0)
		pcpListOfData = pcpListOfData.Left(pcpListOfData.GetLength()-1);

	//pcpListOfData.SetAt(pcpListOfData.GetLength()-1,' ');
	//pcpListOfData.TrimRight( );

    return true;
}

//---------------------------------------------------------------------------------------------------------

bool CedaDENData::PrintExcelHeader(ofstream &of, CString opTrenner)
{
		of   << setw(1) << LoadStg(IDS_STRING450)
			 << setw(1) << opTrenner
			 << setw(1) << LoadStg(IDS_STRING449)   
			 
				; 
		if (bgDEN_DECS_Exist)
		{
			of << setw(1) << opTrenner
				<< setw(1) << LoadStg(IDS_STRING1161) ;
		}
		of	<< setw(1) << opTrenner
			<< setw(1) << LoadStg(IDS_STRING436) 
			<< setw(1) << opTrenner
			<< setw(1) << LoadStg(IDS_STRING451)  	 
		;
		if (ogBasicData.IsColumnForAbnormalFlightsReportAvailable())
		{
			of << setw(1) << opTrenner
				<< setw(1) << LoadStg(IDS_STRING1109) ;
		}

		of	 << setw(1) << opTrenner
			 << setw(1) << LoadStg(IDS_STRING347) 
			 << setw(1) << opTrenner
			 << setw(1) << LoadStg(IDS_STRING348)  
			 << setw(1) << LoadStg(IDS_STRING343)  
			 << setw(1) << opTrenner
			 << setw(1) << LoadStg(IDS_STRING344)  
			 << setw(1) << opTrenner
			 << setw(1) << opTrenner
			 << setw(1) << LoadStg(IDS_STRING230)  
			 << setw(1) << opTrenner
			 << setw(1) << LoadStg(IDS_STRING231)  
		;
		of << endl;

		return true;

}

void CedaDENData::PrintExcelLine(ofstream &of, DENDATA *prpLine, CString opTrenner)
{
		of.setf(ios::left, ios::adjustfield);

		of  << setw(1) << prpLine->Decn
		    << setw(1) << opTrenner 
		    << setw(1) << prpLine->Deca
		   ;

		if(bgDEN_DECS_Exist)
		{
			of << setw(1) << opTrenner 		    
			<< setw(1) << prpLine->Decs;
		}

		of	<< setw(1) << opTrenner
			<< setw(1) << prpLine->Alc3
			<< setw(1) << opTrenner 
		    << setw(1) << prpLine->Dena
			;

		if (ogBasicData.IsColumnForAbnormalFlightsReportAvailable())
		{
			
			of << setw(1) << opTrenner
			   << setw(1) << prpLine->RCol;
		}

		of  << setw(1) << opTrenner 
			<< setw(1) << prpLine->Usec
		    << setw(1) << opTrenner 
		    << setw(1) << prpLine->Useu
			<< setw(1) << opTrenner 
			<< setw(1) << prpLine->Vafr.Format(" %d.%m.%y")
			<< setw(1) << opTrenner 
			<< setw(1) << prpLine->Vato.Format(" %d.%m.%y")
			<< setw(1) << opTrenner 
		    << setw(1) << prpLine->Cdat.Format(" %d.%m.%y")
			<< setw(1) << opTrenner 
		    << setw(1) << prpLine->Lstu.Format(" %d.%m.%y")
		;
		
	
		
		of << endl;

}

bool CedaDENData::CreateExcelFile( CString opTrenner )
{
	ofstream of;
	CString olTableName;
	olTableName.Format("%s -   %s", "DEN", (CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")) );


	CString olFileName = olTableName;
	olFileName.Remove('*');
	olFileName.Remove('.');
	olFileName.Remove(':');
	olFileName.Remove('/');
	olFileName.Replace(" ", "_");
	
	char pHeader[256];
	strcpy (pHeader, CCSLog::GetTmpPath());
	CString path = pHeader;
	CString omFileName =  path + "\\" + olFileName + ".csv";

	of.open(omFileName, ios::out);
	of  << setw(0) << "DEN"<< "     "  << CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")  << endl;

	PrintExcelHeader(of, opTrenner); 

	int ilDENCount = omData.GetSize();
	DENDATA *rlLine;
	for (int ilLc = 0; ilLc < ilDENCount; ilLc++)
	{
		rlLine = &omData[ilLc];
		PrintExcelLine(of, rlLine, opTrenner);
	}

	of.close();

	char pclConfigPath[256];
	char pclExcelPath[256];
	
	

    if (getenv("CEDA") == NULL)
        strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
    else
        strcpy(pclConfigPath, getenv("CEDA"));

    GetPrivateProfileString(ogAppName, "Excel", "DEFAULT",
        pclExcelPath, sizeof pclExcelPath, pclConfigPath);



	char pclTmp[256];
	strcpy(pclTmp, omFileName); 

   /* Set up parameters to be sent: */
	char *args[4];
	args[0] = "child";
	args[1] = pclTmp;
	args[2] = NULL;
	args[3] = NULL;

	_spawnv( _P_NOWAIT , pclExcelPath, args );
	
	return true;

}