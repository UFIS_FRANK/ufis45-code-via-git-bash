// EquipmentTypeDlg.cpp : implementation file
//

#include <stdafx.h>
#include <bdpsuif.h>
#include <EquipmentTypeDlg.h>
#include <PrivList.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// EquipmentTypeDlg dialog


EquipmentTypeDlg::EquipmentTypeDlg(EQTDATA *popEqt, CWnd* pParent /*=NULL*/) : CDialog(EquipmentTypeDlg::IDD, pParent)
{
	pomEqt = popEqt;
	//{{AFX_DATA_INIT(EquipmentTypeDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void EquipmentTypeDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(EquipmentTypeDlg)
	DDX_Control(pDX, IDC_EQUIPMENTTYPE_CODE, m_CODE);
	DDX_Control(pDX, IDC_EQUIPMENTTYPE_NAME, m_NAME);
	DDX_Control(pDX, IDOK,       m_OK);
	DDX_Control(pDX, IDC_CDAT_D, m_CDATD);
	DDX_Control(pDX, IDC_CDAT_T, m_CDATT);
	DDX_Control(pDX, IDC_LSTU_D, m_LSTUD);
	DDX_Control(pDX, IDC_LSTU_T, m_LSTUT);
	DDX_Control(pDX, IDC_USEC,	 m_USEC);
	DDX_Control(pDX, IDC_USEU,	 m_USEU);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(EquipmentTypeDlg, CDialog)
	//{{AFX_MSG_MAP(EquipmentTypeDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// EquipmentTypeDlg message handlers
//----------------------------------------------------------------------------------------

BOOL EquipmentTypeDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	m_Caption = LoadStg(IDS_STRING862) + " <" + m_Caption + ">";
	SetWindowText(m_Caption);
	
	char clStat = ogPrivList.GetStat("EQUIPMENTTYPEDLG.m_OK");
	SetWndStatAll(clStat,m_OK);

	//------------------------------------
	m_CDATD.SetBKColor(SILVER);
	m_CDATD.SetInitText(pomEqt->Cdat.Format("%d.%m.%Y"));
	//m_CDATD.SetSecState(ogPrivList.GetStat("EQUIPMENTTYPEDLG.m_CDAT"));
	// - - - - - - - - - - - - - - - - - -
	m_CDATT.SetBKColor(SILVER);
	m_CDATT.SetInitText(pomEqt->Cdat.Format("%H:%M"));
	//m_CDATT.SetSecState(ogPrivList.GetStat("EQUIPMENTTYPEDLG.m_CDAT"));
	//------------------------------------
	m_LSTUD.SetBKColor(SILVER);
	m_LSTUD.SetInitText(pomEqt->Lstu.Format("%d.%m.%Y"));
	//m_LSTUD.SetSecState(ogPrivList.GetStat("EQUIPMENTTYPEDLG.m_LSTU"));
	// - - - - - - - - - - - - - - - - - -
	m_LSTUT.SetBKColor(SILVER);
	m_LSTUT.SetInitText(pomEqt->Lstu.Format("%H:%M"));
	//m_LSTUT.SetSecState(ogPrivList.GetStat("EQUIPMENTTYPEDLG.m_LSTU"));
	//------------------------------------
	m_USEC.SetBKColor(SILVER);
	m_USEC.SetInitText(pomEqt->Usec);
	//m_USEC.SetSecState(ogPrivList.GetStat("EQUIPMENTTYPEDLG.m_USEC"));
	//------------------------------------
	m_USEU.SetBKColor(SILVER);
	m_USEU.SetInitText(pomEqt->Useu);
	//m_USEU.SetSecState(ogPrivList.GetStat("EQUIPMENTTYPEDLG.m_USEU"));
	
	m_CODE.SetTextLimit(1,5);
	m_CODE.SetBKColor(YELLOW);  
	m_CODE.SetTextErrColor(RED);
	m_CODE.SetInitText(pomEqt->Code);
	//m_CODE.SetSecState(ogPrivList.GetStat("EQUIPMENTTYPEDLG.m_CODE"));
	//------------------------------------
	m_NAME.SetTextLimit(1,20);
	m_NAME.SetBKColor(YELLOW);  
	m_NAME.SetTextErrColor(RED);
	m_NAME.SetInitText(pomEqt->Name);
	//m_NAME.SetSecState(ogPrivList.GetStat("EQUIPMENTTYPEDLG.m_NAME"));
	
	return TRUE;
}

//----------------------------------------------------------------------------------------

void EquipmentTypeDlg::OnOK() 
{
	AfxGetApp()->DoWaitCursor(1);
	CString olErrorText;
	bool ilStatus = true;

	if(m_CODE.GetStatus() == false) 
	{
		ilStatus = false;
		if(m_CODE.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING237) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING237) + ogNotFormat;
		}
	}
	if(m_NAME.GetStatus() == false) 
	{
		ilStatus = false;
		if(m_NAME.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING288) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING288) + ogNotFormat;
		}
	}
	
	if (ilStatus == true)
	{
		//Gültigkeit Code
		CString olText;
		char clWhere[100];
		CCSPtrArray<EQTDATA> olEqtCPA;
		m_CODE.GetWindowText(olText);
		sprintf(clWhere,"WHERE CODE='%s'",olText);
		if(ogEqtData.ReadSpecialData(&olEqtCPA,clWhere,"URNO,CODE",false) == true)
		{
			if(olEqtCPA[0].Urno != pomEqt->Urno)
			{
				ilStatus = false;
				olErrorText += LoadStg(IDS_STRING237) + LoadStg(IDS_EXIST);
			}
		}
		olEqtCPA.DeleteAll();
	}

	if (ilStatus == true)
	{
		//Gültigkeit Name
		CString olText;
		char clWhere[100];
		CCSPtrArray<EQTDATA> olEqtCPA;
		m_NAME.GetWindowText(olText);
		sprintf(clWhere,"WHERE NAME='%s'",olText);
		if(ogEqtData.ReadSpecialData(&olEqtCPA,clWhere,"URNO,NAME",false) == true)
		{
			if(olEqtCPA[0].Urno != pomEqt->Urno)
			{
				ilStatus = false;
				olErrorText += LoadStg(IDS_STRING288) + LoadStg(IDS_EXIST);
			}
		}
		olEqtCPA.DeleteAll();
	}

	if (ilStatus == true)
	{
		m_CODE.GetWindowText(pomEqt->Code,6);
		m_NAME.GetWindowText(pomEqt->Name,21);
		CDialog::OnOK();
	}	
	else
	{
		Beep(440,70);
		MessageBox(olErrorText,LoadStg(IDS_STRING145),MB_ICONERROR);
		m_CODE.SetFocus();
		m_CODE.SetSel(0,-1);
	}
}

//----------------------------------------------------------------------------------------

