#if !defined(AFX_ADR_DLG_H__FC432005_0EB5_11D4_900B_0050DA1CAD13__INCLUDED_)
#define AFX_ADR_DLG_H__FC432005_0EB5_11D4_900B_0050DA1CAD13__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ADR_Dlg.h : Header-Datei
//
#include <gridcontrol.h>
#include <resrc1.h>		// main symbols#


CTime COleDateTimeToCTime(COleDateTime opTime);
COleDateTime CTimeToCOleDateTime(CTime opTime);

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CADR_Dlg 

class CADR_Dlg : public CDialog
{
// Konstruktion
public:
	CADR_Dlg(CWnd* pParent = NULL,CString SFTU = "");   // Standardkonstruktor
	~CADR_Dlg();

// Dialogfelddaten
	//{{AFX_DATA(CADR_Dlg)
	enum { IDD = IDD_ADR_Dlg };
		// HINWEIS: Der Klassen-Assistent fügt hier Datenelemente ein
	//}}AFX_DATA


// Überschreibungen
	// Vom Klassen-Assistenten generierte virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(CADR_Dlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	//}}AFX_VIRTUAL

// Implementierung
protected:

//	CGXGridWnd * pomGrid;
	CGridControl		*	pomGrid;
	void InitGrid();
	void ReadADR_Data();
	bool SaveADR_Data();
	void FillGrid();
	void ReadSTF_Data();
	void Test();
	void FillMember(UINT row,UINT col);
	void InitMemberValue();

	
	CString m_Stfu;
	CString m_Lanm;
	CString m_Finm;
	CString m_Peno;
	CStringArray m_Vafr;
	CStringArray m_Vato;	
	CStringArray m_Zipa;
	CStringArray m_City;
	CStringArray m_Stra;
	CStringArray m_Adrc;
	CStringArray m_Lana;
	CStringArray m_Vwt1;
	CStringArray m_Tel1;
	CStringArray m_Vwt2;
	CStringArray m_Tel2;
	
	int im_UrnoPos;
	int im_StfuPos;
	int im_PenoPos;
	int im_StraPos;
	int im_AdrcPos;
	int im_ZipaPos;
	int im_CityPos;
	int im_LanaPos;
	int im_Vwt1Pos;
	int im_Tel1Pos;
	int im_Vwt2Pos;
	int im_Tel2Pos;
	int im_VafrPos;
	int im_VatoPos;
	int im_CdatPos;
	int im_UsecPos;
	int im_UseuPos;
	int im_LstuPos;
	int im_HopoPos;
	int im_ADRCount;


	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(CADR_Dlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	afx_msg void OnDel();
	//}}AFX_MSG
	afx_msg void OnGridEndEditing( WPARAM wparam, LPARAM lparam );
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ fügt unmittelbar vor der vorhergehenden Zeile zusätzliche Deklarationen ein.

#endif // AFX_ADR_DLG_H__FC432005_0EB5_11D4_900B_0050DA1CAD13__INCLUDED_
