// CedaAadData.h

#ifndef __CEDAAADDATA__
#define __CEDAAADDATA__
 
#include <stdafx.h>
#include <basicdata.h>

//---------------------------------------------------------------------------------------------------------

struct AADDATA 
{
	CTime 	 Cdat; 		// Erstellungsdatum
	CTime 	 Lstu; 		// Datum letzte �nderung
	char 	 Deid[6]; 	// Desk ID
	char 	 Name[32]; 	// Desk ID
	char 	 Moti[83]; 	// Name
	long 	 Urno; 		// Eindeutige Datensatz-Nr.
	char 	 Usec[34]; 	// Anwender (Ersteller)
	char 	 Useu[34]; 	// Anwender (letzte �nderung)
	CTime 	 Vafr; 		// G�ltig von
	CTime 	 Vato; 		// G�ltig bis
	CTime 	 Nato; 		// 
	CTime 	 Nafr; 		// 

	//DataCreated by this class
	int		 IsChanged;	// Check whether Data has Changed f�r Relaese

	AADDATA(void)
	{ 
		memset(this,'\0',sizeof(*this));
		IsChanged	=	DATA_UNCHANGED;
		Cdat		=	TIMENULL;
		Lstu		=	TIMENULL;
		Vafr		=	TIMENULL;
		Vato		=	TIMENULL;
		Nafr		=	TIMENULL;
		Nato		=	TIMENULL;
	}

}; // end SeaDataStrukt

//---------------------------------------------------------------------------------------------------------
// Class declaratino

class CedaAadData: public CCSCedaData
{
// Attributes
public:
    CMapPtrToPtr omUrnoMap;

    CCSPtrArray<AADDATA> omData;

	char pcmListOfFields[2048];

// Operations
public:
    CedaAadData();
	~CedaAadData();
	virtual void GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType);
	void Register(void);
	void ClearAll(bool bpWithRegistration = true);
    bool Read(char *pspWhere = NULL);
	bool Insert(AADDATA *prpAad);
	bool InsertInternal(AADDATA *prpAad);
	bool Update(AADDATA *prpAad);
	bool UpdateInternal(AADDATA *prpAad);
	bool Delete(long lpUrno);
	bool DeleteInternal(AADDATA *prpAad);
	bool ReadSpecialData(CCSPtrArray<AADDATA> *popAad,char *pspWhere,char *pspFieldList,bool ipSYS = true);
	bool Save(AADDATA *prpAad);
	void ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
	AADDATA  *GetAadByUrno(long lpUrno);
	void SetWhere(const CString& ropWhere){omWhere = ropWhere;} //Prf: 8795
	void GetWhere(CString& ropWhere) const{ropWhere = omWhere;} //Prf: 8795

	// Private methods
private:
    void PrepareAadData(AADDATA *prpAadData);
	CString omWhere; //Prf: 8795
	bool ValidateAadBcData(const long& lrpUnro); //Prf: 8795

};

//---------------------------------------------------------------------------------------------------------

#endif //__CEDAAADDATA__
