// CedaPacData.h

#ifndef __CEDAPPACDATA__
#define __CEDAPPACDATA__
 
#include <stdafx.h>
#include <basicdata.h>

//---------------------------------------------------------------------------------------------------------

struct PACDATA 
{
	long 	 Urno;	 	// Eindeutige Datensatz-Nr.
	char 	 Ctrc[8+2]; 	// Code
	char 	 Tifr[4+2]; 	// Bezeichnung
	long 	 Pmxu;	 	// Urno der Planungsgruppe

	//DataCreated by this class
	int      IsChanged;

	//long, CTime
	PACDATA(void)
	{ 
		memset(this,'\0',sizeof(*this));
	}

}; // end PACDATA

//---------------------------------------------------------------------------------------------------------
// Class declaratino

class CedaPacData: public CCSCedaData
{
// Attributes
public:
    CMapPtrToPtr omUrnoMap;

    CCSPtrArray<PACDATA> omData;

	char pcmListOfFields[2048];

// OPacations
public:
    CedaPacData();
	~CedaPacData();
	virtual void GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType);
	void Register(void);
	void ClearAll(bool bpWithRegistration = true);
    bool Read(char *pspWhere = NULL);
	bool Insert(PACDATA *prpPac);
	bool InsertInternal(PACDATA *prpPac);
	bool Update(PACDATA *prpPac);
	bool UpdateInternal(PACDATA *prpPac);
	bool Delete(long lpUrno);
	bool DeleteInternal(PACDATA *prpPac);
	bool ReadSpecialData(CCSPtrArray<PACDATA> *popPac,char *pspWhere,char *pspFieldList,bool ipSYS=true);
	bool Save(PACDATA *prpPac);
	void ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
	PACDATA  *GetPacByUrno(long lpUrno);


	// Private methods
private:
    void PreparePacData(PACDATA *prpPacData);

};

//---------------------------------------------------------------------------------------------------------


#endif //__CEDAPPACDATA__
