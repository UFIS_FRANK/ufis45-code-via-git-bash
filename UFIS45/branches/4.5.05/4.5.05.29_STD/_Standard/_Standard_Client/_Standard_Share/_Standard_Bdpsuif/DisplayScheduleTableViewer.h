#ifndef __DISPLAYSCHEDULETABLEVIEWER_H__
#define __DISPLAYSCHEDULETABLEVIEWER_H__

#include <stdafx.h>
#include <CedaDSCData.h>
#include <CCSTable.h>
#include <CViewer.h>
#include <CCSPrint.h>

struct DISPLAYSCHEDULETABLE_LINEDATA
{
	long	Urno;
	CString	Devn;
	CString	Vafr;
	CString	Vato;
};

/////////////////////////////////////////////////////////////////////////////
// DisplayScheduleTableViewer

class DisplayScheduleTableViewer : public CViewer
{
	// Constructions
public:
    DisplayScheduleTableViewer(CCSPtrArray<DSCDATA> *popData);
    ~DisplayScheduleTableViewer();
    void Attach(CCSTable *popAttachWnd);
    virtual void ChangeViewTo(const char *pcpViewName);

	// Internal data processing routines
private:
	bool IsPassFilter(DSCDATA *prpAirline);
	int  CompareDisplaySchedule(DISPLAYSCHEDULETABLE_LINEDATA *prpAirline1, DISPLAYSCHEDULETABLE_LINEDATA *prpAirline2);
    void MakeLines();
	void MakeLine(DSCDATA *prpAirline);
	bool FindLine(long lpUrno, int &rilLineno);
	CString PrepareDisplayDateFormat(const CString& ropDate,bool blSetDefaultIncaseEmptyDate = false) const;

	// Operations
public:
	void DeleteAll();
	void CreateLine(DISPLAYSCHEDULETABLE_LINEDATA *prpAirline);
	void DeleteLine(int ipLineno);

	// Window refreshing routines
public:
	void UpdateDisplaySchedule();
	void ProcessDisplayScheduleChange(DSCDATA *prpAirline);
	void ProcessDisplayScheduleDelete(DSCDATA *prpAirline);
	bool FindDisplaySchedule(char *prpDisplayScheduleName, int& ilItem);
	bool CreateExcelFile(const CString& ropSeparator,const CString& ropListSeparator);

	// Attributes used for filtering condition
private:
	bool bmIsFromSearch;
	
	// Attributes
private:
    CCSTable			*pomDisplayScheduleTable;
	CCSPtrArray<DSCDATA>*pomData;
	
	// Methods which handle changes (from Data Distributor)
public:
    CCSPtrArray<DISPLAYSCHEDULETABLE_LINEDATA> omLines;
	CCSPtrArray<TABLE_HEADER_COLUMN> omHeaderDataArray;

	// Print 
	void PrintTableView();
	bool PrintTableLine(int ilPage,CUIntArray& ropColsPerPage,DISPLAYSCHEDULETABLE_LINEDATA *prpLine,bool bpLastLine);
	bool PrintTableHeader(int ilPage,CUIntArray& ropColsPerPage);
	CCSPrint *pomPrint;
	
	// CBitmap omBitmap;
	CString omTableName;
	CString	omFileName;
};

#endif //__DISPLAYSCHEDULETABLEVIEWER_H__
