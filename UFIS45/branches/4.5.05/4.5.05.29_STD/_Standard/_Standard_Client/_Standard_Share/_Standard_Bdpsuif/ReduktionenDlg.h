#if !defined(AFX_REDUKTIONENDLG_H__AB36A833_7540_11D1_B430_0000B45A33F5__INCLUDED_)
#define AFX_REDUKTIONENDLG_H__AB36A833_7540_11D1_B430_0000B45A33F5__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// ReduktionenDlg.h : header file
//
#include <CedaSphData.h>
#include <CCSEdit.h>
#include <CedaPrcData.h>

/////////////////////////////////////////////////////////////////////////////
// ReduktionenDlg dialog

class ReduktionenDlg : public CDialog
{
// Construction
public:
	ReduktionenDlg(PRCDATA *popPrc, CWnd* pParent = NULL);   // standard constructor

	PRCDATA *pomPrc;
// Dialog Data
	//{{AFX_DATA(ReduktionenDlg)
	enum { IDD = IDD_REDUKTIONSSTUFENDLG };
	CButton	m_OK;
	CCSEdit	m_CDATD;
	CCSEdit	m_CDATT;
	CCSEdit	m_LSTUD;
	CCSEdit	m_LSTUT;
	CCSEdit	m_REDC;
	CCSEdit	m_REDN;
	CCSEdit	m_USEC;
	CCSEdit	m_USEU;
	CString m_Caption;
	CCSEdit	m_REDL;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(ReduktionenDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(ReduktionenDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_REDUKTIONENDLG_H__AB36A833_7540_11D1_B430_0000B45A33F5__INCLUDED_)
