#ifndef __CTHTABLEVIEWER_H__
#define __CTHTABLEVIEWER_H__

#include <stdafx.h>
#include <CedaCthData.h>
#include <CCSTable.h>
#include <CViewer.h>
#include <CCSPrint.h>


struct CTHTABLE_LINEDATA
{
	long 	Urno;
	CString	Abst;
	CString	Absf;
	CString	Cthg;
	CString	Redu;
	CString	Year;
};

/////////////////////////////////////////////////////////////////////////////
// CthTableViewer

class CthTableViewer : public CViewer
{
// Constructions
public:
    CthTableViewer(CCSPtrArray<CTHDATA> *popData);
    ~CthTableViewer();

    void Attach(CCSTable *popAttachWnd);
    virtual void ChangeViewTo(const char *pcpViewName);
// Internal data processing routines
private:
	bool IsPassFilter(CTHDATA *prpCth);
	int CompareCth(CTHTABLE_LINEDATA *prpCth1, CTHTABLE_LINEDATA *prpCth2);
    void MakeLines();
	void MakeLine(CTHDATA *prpCth);
	bool FindLine(long lpUrno, int &rilLineno);
// Operations
public:
	void DeleteAll();
	void CreateLine(CTHTABLE_LINEDATA *prpCth);
	void DeleteLine(int ipLineno);
// Window refreshing routines
public:
	void UpdateDisplay();
	CString Format(CTHTABLE_LINEDATA *prpLine);
	void ProcessCthChange(CTHDATA *prpCth);
	void ProcessCthDelete(CTHDATA *prpCth);
	bool FindCth(char *prpCthKeya, char *prpCthKeyd, int& ilItem);
// Attributes used for filtering condition
private:
	bool bmIsFromSearch;
// Attributes
private:
    CCSTable *pomCthTable;
	CCSPtrArray<CTHDATA> *pomData;
// Methods which handle changes (from Data Distributor)
public:
    CCSPtrArray<CTHTABLE_LINEDATA> omLines;
///////
	CCSPtrArray<TABLE_HEADER_COLUMN> omHeaderDataArray;
//Print 
	void PrintTableView();
	bool PrintTableLine(CTHTABLE_LINEDATA *prpLine,bool bpLastLine);
	bool PrintTableHeader();
	CCSPrint *pomPrint;
	//CBitmap omBitmap;
	CString omTableName;

};

#endif //__CTHTABLEVIEWER_H__
