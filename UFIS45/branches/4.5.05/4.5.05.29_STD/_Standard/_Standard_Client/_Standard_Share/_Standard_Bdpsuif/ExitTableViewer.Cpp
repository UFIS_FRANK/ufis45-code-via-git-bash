// ExitTableViewer.cpp 
//

#include <stdafx.h>
#include <ExitTableViewer.h>
#include <CcsDdx.h>
#include <resource.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif


// Local function prototype
static void ExitTableCf(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//-----------------------------------------------------------------------------------------------
// ExitTableViewer
//

ExitTableViewer::ExitTableViewer(CCSPtrArray<EXTDATA> *popData)
{
	pomData = popData;

	bmIsFromSearch = FALSE;
    pomExitTable = NULL;
    ogDdx.Register(this, EXT_CHANGE, CString("EXITTABLEVIEWER"), CString("Exit Update/new"), ExitTableCf);
    ogDdx.Register(this, EXT_DELETE, CString("EXITTABLEVIEWER"), CString("Exit Delete"), ExitTableCf);
}

//-----------------------------------------------------------------------------------------------

ExitTableViewer::~ExitTableViewer()
{
	ogDdx.UnRegister(this, NOTUSED);
    DeleteAll();
	omHeaderDataArray.DeleteAll();
}

//-----------------------------------------------------------------------------------------------

void ExitTableViewer::Attach(CCSTable *popTable)
{
    pomExitTable = popTable;
}

//-----------------------------------------------------------------------------------------------

void ExitTableViewer::ChangeViewTo(const char *pcpViewName)
{
    DeleteAll();    
    MakeLines();
	UpdateDisplay();
}

//-----------------------------------------------------------------------------------------------

void ExitTableViewer::MakeLines()
{
	int ilExitCount = pomData->GetSize();
	for (int ilLc = 0; ilLc < ilExitCount; ilLc++)
	{
		EXTDATA *prlExitData = &pomData->GetAt(ilLc);
		MakeLine(prlExitData);
	}
}

//-----------------------------------------------------------------------------------------------

void ExitTableViewer::MakeLine(EXTDATA *prpExit)
{
    //if( !IsPassFilter(prpExit)) return;

    // Update viewer data for this shift record
    EXITTABLE_LINEDATA rlExit;
	rlExit.Urno = prpExit->Urno; 
	rlExit.Enam = prpExit->Enam; 
	rlExit.Nafr = prpExit->Nafr.Format("%d.%m.%Y %H:%M"); 
	rlExit.Nato = prpExit->Nato.Format("%d.%m.%Y %H:%M"); 
	rlExit.Resn = prpExit->Resn;  
	rlExit.Term = prpExit->Term;
	rlExit.Tele = prpExit->Tele;
	rlExit.Home = prpExit->Home;
	rlExit.Vafr = prpExit->Vafr.Format("%d.%m.%Y %H:%M"); 
	rlExit.Vato = prpExit->Vato.Format("%d.%m.%Y %H:%M"); 
	CreateLine(&rlExit);
}

//-----------------------------------------------------------------------------------------------

void ExitTableViewer::CreateLine(EXITTABLE_LINEDATA *prpExit)
{
    int ilLineCount = omLines.GetSize();
    for (int ilLineno = ilLineCount; ilLineno > 0; ilLineno--)
        if (CompareExit(prpExit, &omLines[ilLineno-1]) >= 0)
	        break;							 // should be inserted after Lines[ilLineno-1]
	EXITTABLE_LINEDATA rlExit;
	rlExit = *prpExit;
    omLines.NewAt(ilLineno, rlExit);
}

//-----------------------------------------------------------------------------------------------
// UpdateDisplay: Load data selected by filter conditions to the display by using "omTable"

void ExitTableViewer::UpdateDisplay()
{
	int ilLines = omLines.GetSize();
	const int ilColumns = 8;	//<===== Spaltenanzahl setzen (von 0 ab)
	omHeaderDataArray.DeleteAll();

	pomExitTable->SetShowSelection(TRUE);
	pomExitTable->ResetContent();

	TABLE_HEADER_COLUMN rlHeader;
	rlHeader.Alignment = COLALIGN_LEFT;
	rlHeader.Font = &ogCourier_Regular_10;

	int ilPos = 1;
	rlHeader.Length = 42; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING265),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 133; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING265),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 133; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING265),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 332; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING265),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 8; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING265),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 83; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING265),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 133; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING265),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 133; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING265),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);

	rlHeader.Length = 33; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING265),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);

	pomExitTable->SetHeaderFields(omHeaderDataArray);

	pomExitTable->SetDefaultSeparator();
	pomExitTable->SetTableEditable(FALSE);

	TABLE_COLUMN rlColumnData;
	rlColumnData.Font = &ogCourier_Regular_10;
	rlColumnData.BkColor = WHITE;
	rlColumnData.Alignment = COLALIGN_LEFT;
	
    for (int ilLineNo = 0; ilLineNo < ilLines; ilLineNo++)
	{
		CCSPtrArray<TABLE_COLUMN> olColList;

		rlColumnData.Text = omLines[ilLineNo].Enam;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Nafr;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Nato;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Resn;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Term;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Tele;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Vafr;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Vato;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.Text = omLines[ilLineNo].Home;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		pomExitTable->AddTextLine(olColList,(void*)NULL);
		olColList.DeleteAll();
	}
	pomExitTable->DisplayTable();
}

//-----------------------------------------------------------------------------------------------

CString ExitTableViewer::Format(EXITTABLE_LINEDATA *prpLine)
{
    CString s;
	// ???
    return s;
}

//-----------------------------------------------------------------------------------------------

BOOL ExitTableViewer::FindExit(char *pcpExitKeya, char *pcpExitKeyd, int& ilItem)
{
	int ilCount = omLines.GetSize();
    for (ilItem = 0; ilItem < ilCount; ilItem++)
    {
/*		if ( (omLines[ilItem].Keya == pcpExitKeya) &&
			 (omLines[ilItem].Keyd == pcpExitKeyd)    )
			return TRUE;*/
	}
	return FALSE;
}

//-----------------------------------------------------------------------------------------------

static void ExitTableCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName)
{
    ExitTableViewer *polViewer = (ExitTableViewer *)popInstance;
    if (ipDDXType == EXT_CHANGE) polViewer->ProcessExitChange((EXTDATA *)vpDataPointer);
    if (ipDDXType == EXT_DELETE) polViewer->ProcessExitDelete((EXTDATA *)vpDataPointer);
} 


//-----------------------------------------------------------------------------------------------
void ExitTableViewer::ProcessExitChange(EXTDATA *prpExit)
{
	int ilItem = 0;
	int ilColumnNo = 0;
	TABLE_COLUMN rlColumn;
	CCSPtrArray <TABLE_COLUMN> olLine;

	rlColumn.Lineno = ilItem;
	rlColumn.VerticalSeparator = SEPA_NONE;
	rlColumn.SeparatorType = SEPA_NONE;
	rlColumn.BkColor = WHITE;
	rlColumn.Font = &ogCourier_Regular_10;
	rlColumn.Alignment = COLALIGN_LEFT;

	rlColumn.Text = prpExit->Enam;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpExit->Nafr.Format("%d.%m.%Y %H:%M"); 
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpExit->Nato.Format("%d.%m.%Y %H:%M"); 
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpExit->Resn;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpExit->Term;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpExit->Tele;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpExit->Vafr.Format("%d.%m.%Y %H:%M"); 
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpExit->Vato.Format("%d.%m.%Y %H:%M"); 
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);

	rlColumn.Text = prpExit->Home;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);

	if (FindLine(prpExit->Urno, ilItem))
	{
        EXITTABLE_LINEDATA *prlLine = &omLines[ilItem];

		prlLine->Urno = prpExit->Urno;
		prlLine->Enam = prpExit->Enam;
		prlLine->Nafr = prpExit->Nafr.Format("%d.%m.%Y %H:%M");
		prlLine->Nato = prpExit->Nato.Format("%d.%m.%Y %H:%M");
		prlLine->Resn = prpExit->Resn;
		prlLine->Term = prpExit->Term;
		prlLine->Tele = prpExit->Tele;
		prlLine->Home = prpExit->Home;
		prlLine->Vafr = prpExit->Vafr.Format("%d.%m.%Y %H:%M");
		prlLine->Vato = prpExit->Vato.Format("%d.%m.%Y %H:%M");
	
		pomExitTable->ChangeTextLine(ilItem, &olLine, (void *)prlLine);
		pomExitTable->DisplayTable();
	}
	else
	{
		MakeLine(prpExit);
		if (FindLine(prpExit->Urno, ilItem))
		{
	        EXITTABLE_LINEDATA *prlLine = &omLines[ilItem];
			if(prlLine != NULL)
			{
				pomExitTable->AddTextLine(olLine, (void *)prlLine);
				pomExitTable->DisplayTable();
			}
		}
	}
	olLine.DeleteAll();
}

//-----------------------------------------------------------------------------------------------

void ExitTableViewer::ProcessExitDelete(EXTDATA *prpExit)
{
	int ilItem;
	if (FindLine(prpExit->Urno, ilItem))
	{
		DeleteLine(ilItem);
		pomExitTable->DeleteTextLine(ilItem);
		pomExitTable->DisplayTable();
	}
}

//-----------------------------------------------------------------------------------------------

BOOL ExitTableViewer::IsPassFilter(EXTDATA *prpExit)
{

	return TRUE;
}

//-----------------------------------------------------------------------------------------------

int ExitTableViewer::CompareExit(EXITTABLE_LINEDATA *prpExit1, EXITTABLE_LINEDATA *prpExit2)
{
/*	int ilCase=0;
	int	ilCompareResult;
	if(prpExit1->Tifd == prpExit2->Tifd)
	{
		ilCompareResult= 0;
	}
	else
	{
		if(prpExit1->Tifd > prpExit2->Tifd)
		{
			ilCompareResult = 1;
		}
		else
		{
			ilCompareResult = -1;
		}
	}
	return ilCompareResult;
*/  
	return 0;
}

//-----------------------------------------------------------------------------------------------

void ExitTableViewer::DeleteLine(int ipLineno)
{
	omLines.DeleteAt(ipLineno);
}

//-----------------------------------------------------------------------------------------------

BOOL ExitTableViewer::FindLine(long lpUrno, int &rilLineno)
{
    for (rilLineno = 0; rilLineno < omLines.GetSize(); rilLineno++)
	{
        if (omLines[rilLineno].Urno == lpUrno)
            return TRUE;
	}
    return FALSE;
}
//-----------------------------------------------------------------------------------------------

void ExitTableViewer::DeleteAll()
{
    while (omLines.GetSize() > 0)
        DeleteLine(0);
}

//-----------------------------------------------------------------------------------------------
// Printing Routines
//-----------------------------------------------------------------------------------------------

void ExitTableViewer::PrintTableView()
{
	CString olFooter1,olFooter2;
	omTableName = LoadStg(IDS_STRING163);
	int ilOrientation = PRINT_LANDSCAPE;//  PRINT_PORTRAET  PRINT_LANDSCAPE
	pomPrint = new CCSPrint((CWnd*)this,ilOrientation,45);// FirstLine(P=200,L=200),LeftOffset(P=200,L=50)
	//pomPrint->imMaxLines = 38;  // (P=57,L=38)
	//omBitmap.LoadBitmap(IDB_HAJLOGO);
	//pomPrint->SetBitmaps(&omBitmap,&omBitmap);
	if (pomPrint != NULL)
	{
		if (pomPrint->InitializePrinter(ilOrientation) == TRUE)
		{
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			pomPrint->imLineNo = pomPrint->imMaxLines + 1;
			DOCINFO	rlDocInfo;
			memset(&rlDocInfo, 0, sizeof(DOCINFO));
			rlDocInfo.cbSize = sizeof( DOCINFO );
			rlDocInfo.lpszDocName = omTableName;	
			pomPrint->omCdc.StartDoc( &rlDocInfo );
			pomPrint->imPageNo = 0;
			int ilLines = omLines.GetSize();
			olFooter1.Format("%d %s",ilLines,omTableName);
			for(int i = 0; i < ilLines; i++ ) 
			{
				if(pomPrint->imLineNo >= pomPrint->imMaxLines)
				{
					if(pomPrint->imPageNo > 0)
					{
						olFooter2.Format("%s: %d",LoadStg(IDS_STRING222),pomPrint->imPageNo);
						pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
						pomPrint->omCdc.EndPage();
					}
					PrintTableHeader();
				}
				if(pomPrint->imLineNo == (pomPrint->imMaxLines-1) || i == (ilLines-1))
				{
					PrintTableLine(&omLines[i],true);
				}
				else
				{
					PrintTableLine(&omLines[i],false);
				}
			}
			olFooter2.Format("%s: %d",LoadStg(IDS_STRING222),pomPrint->imPageNo);
			pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
			pomPrint->omCdc.EndPage();
			pomPrint->omCdc.EndDoc();
		}
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	delete pomPrint;
	//omBitmap.DeleteObject();
	pomPrint = NULL;
	}
}

//-----------------------------------------------------------------------------------------------

bool ExitTableViewer::PrintTableHeader()
{
	pomPrint->omCdc.StartPage();
	pomPrint->imPageNo++;
	pomPrint->imLineNo = 0;

	pomPrint->PrintUIFHeader(omTableName,CString(CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")),pomPrint->imFirstLine-10);

	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;

	rlElement.Alignment   = PRINT_LEFT;
	rlElement.FrameLeft   = PRINT_NOFRAME;
	rlElement.FrameRight  = PRINT_NOFRAME;
	rlElement.FrameTop    = PRINT_NOFRAME;
	rlElement.FrameBottom = PRINT_FRAMEMEDIUM;
	rlElement.pFont       = &pomPrint->ogCourierNew_Bold_8;

	int ilSize = omHeaderDataArray.GetSize();
	for(int i=0;i<ilSize;i++)
	{
		//if(i!=1&&i!=2)// <= setzen, wenn Spalten nicht angezeigt werden sollen 
		{
			rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
			if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
			rlElement.Text   = omHeaderDataArray[i].Text;
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		}
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();


	return true;
}

//-----------------------------------------------------------------------------------------------

bool ExitTableViewer::PrintTableLine(EXITTABLE_LINEDATA *prpLine,bool bpLastLine)
{
	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;
	rlElement.pFont = &pomPrint->ogCourierNew_Regular_8;
	int ilSize = omHeaderDataArray.GetSize();
	for(int i=0;i<ilSize;i++)
	{
		//if(i!=1&&i!=1)// <= setzen, wenn Spalten nicht angezeigt werden sollen 
		{
			rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
			if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
			rlElement.Alignment  = PRINT_LEFT;
			rlElement.FrameTop   = PRINT_FRAMETHIN;
			if(bpLastLine == true)
			{
				rlElement.FrameBottom = PRINT_FRAMETHIN;
			}
			else
			{
				rlElement.FrameBottom = PRINT_NOFRAME;
			}
			rlElement.FrameLeft  = PRINT_NOFRAME;
			rlElement.FrameRight = PRINT_NOFRAME;
			switch(i)
			{
			case 0:
				{
					rlElement.FrameLeft  = PRINT_FRAMETHIN;
					rlElement.Text		= prpLine->Enam;
				}
				break;
			case 1:
				{
					rlElement.Text		= prpLine->Nafr;
				}
				break;
			case 2:
				{
					rlElement.Text		= prpLine->Nato;
				}
				break;
			case 3:
				{
					rlElement.Text		= prpLine->Resn;
				}
				break;
			case 4:
				{
					rlElement.Text		= prpLine->Term;
				}
				break;
			case 5:
				{
					rlElement.Text		= prpLine->Tele;
				}
				break;
			case 6:
				{
					rlElement.Text		= prpLine->Vafr;
				}
				break;
			case 7:
				{
					rlElement.Text		= prpLine->Vato;
				}
			case 8:
				{
					rlElement.FrameRight = PRINT_FRAMETHIN;
					rlElement.Text		= prpLine->Home;
				}
				break;
			}
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		}
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();
	return true;
}

//-----------------------------------------------------------------------------------------------
