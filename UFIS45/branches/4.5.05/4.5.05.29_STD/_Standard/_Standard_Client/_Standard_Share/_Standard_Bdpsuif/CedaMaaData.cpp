// CedaMaaData.cpp
 
#include "stdafx.h"
#include "CedaMaaData.h"
#include "resource.h"


void ProcessMaaCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//--CEDADATA-----------------------------------------------------------------------------------------------

CedaMaaData::CedaMaaData() : CCSCedaData(&ogCommHandler)
{
	// Create an array of CEDARECINFO for BSDDataStruct
	BEGIN_CEDARECINFO(MAADATA,BSDDataRecInfo)
		FIELD_CHAR_TRIM	(Hopo,"HOPO")
		FIELD_LONG  	(Mawu,"MAWU")
		FIELD_LONG  	(Urno,"URNO")
		FIELD_CHAR_TRIM	(Valu,"VALU")
		FIELD_CHAR_TRIM	(Week,"WEEK")
		FIELD_CHAR_TRIM	(Year,"YEAR")
	END_CEDARECINFO //(BSDDataStruct)

	// FIELD_LONG, FIELD_DATE 
	// Copy the record structure
	for (int i=0; i< sizeof(BSDDataRecInfo)/sizeof(BSDDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&BSDDataRecInfo[i],sizeof(CEDARECINFO));
		omRecInfo.Add(prpCedaRecInfo);
	} // end for

	// initialize field names
	strcpy(pcmTableName,"MAA");
	strcpy(pcmListOfFields,"HOPO,MAWU,URNO,VALU,WEEK,YEAR");
	pcmFieldList = pcmListOfFields; //because in CedaData pcmFieldList is just a pointer

	omData.RemoveAll();
}

//---------------------------------------------------------------------------------------------------------

void CedaMaaData::GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType)
{
	ropFields.Add("HOPO");
	ropFields.Add("MAWU");
	ropFields.Add("URNO");
	ropFields.Add("VALU");
	ropFields.Add("WEEK");
	ropFields.Add("YEAR");

	ropDesription.Add(LoadStg(IDS_STRING341));
	ropDesription.Add(LoadStg(IDS_STRING340));
	ropDesription.Add(LoadStg(IDS_STRING338));
	ropDesription.Add(LoadStg(IDS_STRING446));
	ropDesription.Add(LoadStg(IDS_STRING339));
	ropDesription.Add(LoadStg(IDS_STRING339));
	
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	
}

//--REGISTER----------------------------------------------------------------------------------------------

void CedaMaaData::Register(void)
{
	ogDdx.Register((void *)this,BC_MAA_CHANGE,	CString("MAADATA"), CString("Maa-changed"),	ProcessMaaCf);
	ogDdx.Register((void *)this,BC_MAA_NEW,		CString("MAADATA"), CString("Maa-new"),		ProcessMaaCf);
	ogDdx.Register((void *)this,BC_MAA_DELETE,	CString("MAADATA"), CString("Maa-deleted"),	ProcessMaaCf);
}

//--~CEDADATA----------------------------------------------------------------------------------------------

CedaMaaData::~CedaMaaData(void)
{
	omRecInfo.DeleteAll();
	ClearAll();
}

//--CLEAR-ALL----------------------------------------------------------------------------------------------

void CedaMaaData::ClearAll(bool bpWithRegistration)
{
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(bpWithRegistration)
	{
		ogDdx.UnRegister(this,NOTUSED);
	}
}

//--READ-ALL-----------------------------------------------------------------------------------------------

bool CedaMaaData::Read(char *pspWhere /*NULL*/)
{
    // Select data from the database
	bool ilRc = true;
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(pspWhere == NULL)
	{	
		ilRc = CedaAction("RT");
	}
	else
	{
		ilRc = CedaAction("RT", pspWhere);
	}
	if (ilRc != true)
	{
		return ilRc;
	}
	// Load data from CedaData into the dynamic array of record
	for (int ilLc = 0; ilRc == true; ilLc++)
	{
		MAADATA *prlMaa = new MAADATA;
		if ((ilRc = GetFirstBufferRecord(prlMaa)) == true)
		{
			omData.Add(prlMaa);//Update omData
			omUrnoMap.SetAt((void *)prlMaa->Urno,prlMaa);
		}
		else
		{
			delete prlMaa;
		}
	}
    return true;
}

//--INSERT-------------------------------------------------------------------------------------------------

bool CedaMaaData::Insert(MAADATA *prpMaa)
{
	prpMaa->IsChanged = DATA_NEW;
	if(Save(prpMaa) == false) return false; //Update Database
	InsertInternal(prpMaa);
    return true;
}

//--INSERT-INTERNAL--------------------------------------------------------------------------------------

bool CedaMaaData::InsertInternal(MAADATA *prpMaa)
{
	ogDdx.DataChanged((void *)this, MAA_NEW,(void *)prpMaa ); //Update Viewer
	omData.Add(prpMaa);//Update omData
	omUrnoMap.SetAt((void *)prpMaa->Urno,prpMaa);
    return true;
}

//--DELETE-------------------------------------------------------------------------------------------------

bool CedaMaaData::Delete(long lpUrno)
{
	MAADATA *prlMaa = GetMaaByUrno(lpUrno);
	if (prlMaa != NULL)
	{
		prlMaa->IsChanged = DATA_DELETED;
		if(Save(prlMaa) == false) return false; //Update Database
		DeleteInternal(prlMaa);
	}
    return true;
}

//--DELETE-INTERNAL----------------------------------------------------------------------------------------

bool CedaMaaData::DeleteInternal(MAADATA *prpMaa)
{
	ogDdx.DataChanged((void *)this,MAA_DELETE,(void *)prpMaa); //Update Viewer
	omUrnoMap.RemoveKey((void *)prpMaa->Urno);
	int ilMawCount = omData.GetSize();
	for (int ilLc = 0; ilLc < ilMawCount; ilLc++)
	{
		if (omData[ilLc].Urno == prpMaa->Urno)
		{
			omData.DeleteAt(ilLc);//Update omData
			break;
		}
	}
    return true;
}

//--UPDATE-------------------------------------------------------------------------------------------------

bool CedaMaaData::Update(MAADATA *prpMaa)
{
	if (GetMaaByUrno(prpMaa->Urno) != NULL)
	{
		if (prpMaa->IsChanged == DATA_UNCHANGED)
		{
			prpMaa->IsChanged = DATA_CHANGED;
		}
		if(Save(prpMaa) == false) return false; //Update Database
		UpdateInternal(prpMaa);
	}
    return true;
}

//--UPDATE-INTERNAL--------------------------------------------------------------------------------------------

bool CedaMaaData::UpdateInternal(MAADATA *prpMaa)
{
	MAADATA *prlMaa = GetMaaByUrno(prpMaa->Urno);
	if (prlMaa != NULL)
	{
		*prlMaa = *prpMaa; //Update omData
		ogDdx.DataChanged((void *)this,MAA_CHANGE,(void *)prlMaa); //Update Viewer
	}
    return true;
}

//--GET-BY-URNO--------------------------------------------------------------------------------------------

MAADATA *CedaMaaData::GetMaaByUrno(long lpUrno)
{
	MAADATA  *prlMaa;
	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlMaa) == TRUE)
	{
		return prlMaa;
	}
	return NULL;
}

//--READSPECIALDATA-------------------------------------------------------------------------------------

bool CedaMaaData::ReadSpecialData(CCSPtrArray<MAADATA> *popMaa,char *pspWhere,char *pspFieldList,bool ipSYS/*=true*/)
{
	bool ilRc = true;
	char pclFieldList[256] = " ";

	if(strlen(pspFieldList) > 0) 
	{
		strcpy(pclFieldList, pspFieldList);
	}
	if(ipSYS == true) 
	{
		if (CedaAction("SYS","MAA",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	else
	{
		if (CedaAction("RT","MAA",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	if(popMaa != NULL)
	{
		for (int ilLc = 0; ilRc == true; ilLc++)
		{
			MAADATA *prpMaa = new MAADATA;
			if ((ilRc = GetBufferRecord(ilLc,prpMaa,CString(pclFieldList))) == true)
			{
				popMaa->Add(prpMaa);
			}
			else
			{
				delete prpMaa;
			}
		}
		if(popMaa->GetSize() == 0) return false;
	}
    return true;
}

//--SAVE---------------------------------------------------------------------------------------------------

bool CedaMaaData::Save(MAADATA *prpMaa)
{
	bool ilRc = true;
	CString olListOfData;
	char pclSelection[124];
	char pclData[524];

	if (prpMaa->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}
	switch(prpMaa->IsChanged)
	{
	case DATA_NEW:
		MakeCedaData(&omRecInfo,olListOfData,prpMaa);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("IRT","","",pclData);
		prpMaa->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpMaa->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpMaa);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("URT",pclSelection,"",pclData);
		prpMaa->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_DELETED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpMaa->Urno);
		ilRc = CedaAction("DRT",pclSelection);
		break;
	}
    return ilRc;
}

//--PROCESS-CF---------------------------------------------------------------------------------------------

void  ProcessMaaCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	ogMaaData.ProcessBc(ipDDXType,vpDataPointer,ropInstanceName);
}

//--PROCESS-BC---------------------------------------------------------------------------------------------

void  CedaMaaData::ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlMaaData;
	prlMaaData = (struct BcStruct *) vpDataPointer;
	MAADATA *prlMaa;
	if(ipDDXType == BC_MAA_NEW)
	{
		prlMaa = new MAADATA;
		GetRecordFromItemList(prlMaa,prlMaaData->Fields,prlMaaData->Data);
		InsertInternal(prlMaa);
	}
	if(ipDDXType == BC_MAA_CHANGE)
	{
		long llUrno = GetUrnoFromSelection(prlMaaData->Selection);
		prlMaa = GetMaaByUrno(llUrno);
		if(prlMaa != NULL)
		{
			GetRecordFromItemList(prlMaa,prlMaaData->Fields,prlMaaData->Data);
			UpdateInternal(prlMaa);
		}
	}
	if(ipDDXType == BC_MAA_DELETE)
	{
		long llUrno = GetUrnoFromSelection(prlMaaData->Selection);

		prlMaa = GetMaaByUrno(llUrno);
		if (prlMaa != NULL)
		{
			DeleteInternal(prlMaa);
		}
	}
}

//---------------------------------------------------------------------------------------------------------
