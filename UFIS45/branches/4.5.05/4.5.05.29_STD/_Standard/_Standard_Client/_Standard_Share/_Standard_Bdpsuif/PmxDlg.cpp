// PmxDlg.cpp : implementation file
//

#include <stdafx.h>
#include <bdpsuif.h>
#include <PmxDlg.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CPmxDlg dialog


CPmxDlg::CPmxDlg(PMXDATA *popPmx, CCSPtrArray<PACDATA> *popPacData, CWnd* pParent /*=NULL*/) : CDialog(CPmxDlg::IDD, pParent)
{
	pomPmx = popPmx;
	pomPacData = popPacData;

	pomTable = new CCSTable;
	//{{AFX_DATA_INIT(CPmxDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CPmxDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CPmxDlg)
	DDX_Control(pDX, IDOK, m_OK);
	DDX_Control(pDX, IDC_CDAT_D, m_CDATD);
	DDX_Control(pDX, IDC_CDAT_T, m_CDATT);
	DDX_Control(pDX, IDC_HELP_PAC, m_HELP_PAC);
	DDX_Control(pDX, IDC_POFR, m_POFR);
	DDX_Control(pDX, IDC_POMO, m_POMO);
	DDX_Control(pDX, IDC_POTU, m_POTU);
	DDX_Control(pDX, IDC_POWE, m_POWE);
	DDX_Control(pDX, IDC_POTH, m_POTH);
	DDX_Control(pDX, IDC_POSA, m_POSA);
	DDX_Control(pDX, IDC_POSU, m_POSU);
	DDX_Control(pDX, IDC_TIFR, m_TIFR);
	DDX_Control(pDX, IDC_LSTU_D, m_LSTUD);
	DDX_Control(pDX, IDC_LSTU_T, m_LSTUT);
	DDX_Control(pDX, IDC_USEC, m_USEC);
	DDX_Control(pDX, IDC_USEU, m_USEU);
	//uhi 7.5.01
	DDX_Control(pDX, IDC_VAFR, m_VAFR);
	DDX_Control(pDX, IDC_VATO, m_VATO);

		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CPmxDlg, CDialog)
	//{{AFX_MSG_MAP(CPmxDlg)
	ON_BN_CLICKED(IDC_B1, OnB1)
	ON_BN_CLICKED(IDC_B2, OnB2)
	ON_BN_CLICKED(IDC_B3, OnB3)
	ON_BN_CLICKED(IDC_B4, OnB4)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CPmxDlg message handlers

void CPmxDlg::OnOK() 
{
	AfxGetApp()->DoWaitCursor(1);
	CString olErrorText;
	bool ilStatus = true;
	CString olTmp;
	if(m_POMO.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING809) + ogNotFormat;
	}
	else if(m_POMO.GetWindowTextLength() == 0)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING809) +  ogNoData;
	}
	else 
	{
		m_POMO.GetWindowText(olTmp);
		if (olTmp.Find(",") >= 0)
		{
			ilStatus = false;
			olErrorText += LoadStg(IDS_STRING809) +  " Use '.' instead of ','!" ;
		}
	}
	if(m_POTU.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING810) + ogNotFormat;
	}
	else if(m_POTU.GetWindowTextLength() == 0)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING810) +  ogNoData;
	}
	else 
	{
		m_POTU.GetWindowText(olTmp);
		if (olTmp.Find(",") >= 0)
		{
			ilStatus = false;
			olErrorText += LoadStg(IDS_STRING810) +  " Use '.' instead of ','!" ;
		}
	}
	if(m_POWE.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING811) + ogNotFormat;
	}
	else if(m_POWE.GetWindowTextLength() == 0)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING811) +  ogNoData;
	}
	else 
	{
		m_POWE.GetWindowText(olTmp);
		if (olTmp.Find(",") >= 0)
		{
			ilStatus = false;
			olErrorText += LoadStg(IDS_STRING811) +  " Use '.' instead of ','!" ;
		}
	}
	if(m_POTH.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING812) + ogNotFormat;
	}
	else if(m_POTH.GetWindowTextLength() == 0)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING812) +  ogNoData;
	}
	else 
	{
		m_POTH.GetWindowText(olTmp);
		if (olTmp.Find(",") >= 0)
		{
			ilStatus = false;
			olErrorText += LoadStg(IDS_STRING812) +  " Use '.' instead of ','!" ;
		}
	}
	if(m_POFR.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING813) + ogNotFormat;
	}
	else if(m_POFR.GetWindowTextLength() == 0)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING813) +  ogNoData;
	}
	else 
	{
		m_POFR.GetWindowText(olTmp);
		if (olTmp.Find(",") >= 0)
		{
			ilStatus = false;
			olErrorText += LoadStg(IDS_STRING813) +  " Use '.' instead of ','!" ;
		}
	}
	if(m_POSA.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING814) + ogNotFormat;
	}
	else if(m_POSA.GetWindowTextLength() == 0)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING814) +  ogNoData;
	}
	else 
	{
		m_POSA.GetWindowText(olTmp);
		if (olTmp.Find(",") >= 0)
		{
			ilStatus = false;
			olErrorText += LoadStg(IDS_STRING814) +  " Use '.' instead of ','!" ;
		}
	}
	if(m_POSU.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING815) + ogNotFormat;
	}
	else if(m_POSU.GetWindowTextLength() == 0)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING815) +  ogNoData;
	}
	else 
	{
		m_POSU.GetWindowText(olTmp);
		if (olTmp.Find(",") >= 0)
		{
			ilStatus = false;
			olErrorText += LoadStg(IDS_STRING815) +  " Use '.' instead of ','!" ;
		}
	}
	
	if(m_TIFR.GetStatus() == false)
	{
		ilStatus = false;
		if(m_TIFR.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING816) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING816) + ogNotFormat;
		}
	}

	//uhi 7.5.01
	if(m_VAFR.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING230) + ogNotFormat;
	}
	else
		if(m_VAFR.GetWindowTextLength() == 0){
			ilStatus = false;
			olErrorText += LoadStg(IDS_STRING230) +  ogNoData;
		}
	
	if(m_VATO.GetStatus() == false){
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING231) + ogNotFormat;
	}
	else
		if(m_VATO.GetWindowTextLength() == 0){
			ilStatus = false;
			olErrorText += LoadStg(IDS_STRING231) +  ogNoData;
		}
	

	///////////////////////////////////////////////////////////////////////////
	//Konsistenz-Check//
 	if(ilStatus == true)
	{
		CString olTifr;
		CCSPtrArray<PMXDATA> olPmxCPA;
		//char clWhere[100];

		m_TIFR.GetWindowText(olTifr);
		if ( olTifr.GetLength() <= 3 )
//		{
//			sprintf(clWhere,"WHERE TIFR='%s'",olTifr);
//			if(ogPmxData.ReadSpecialData(&olPmxCPA,clWhere,"URNO",false) == true)
//			{
//				if(olPmxCPA[0].Urno != pomPmx->Urno)
//				{
//					ilStatus = false;
//					olErrorText += LoadStg(IDS_STRING816) + LoadStg(IDS_EXIST);
//				}
//			}
//		}
//		else
		{
			ilStatus = false;
			olErrorText += LoadStg(IDS_STRING816);
		}

		CString olTableErrorText;
		int i;

		for(i = 0; i < 50; i++)
		{
			int ilFilledCells = 0;
			bool blLineOK = true;
			char pclZeile[10], pclSpalte[10];
			CCSPtrArray<TABLE_COLUMN> olLine;
			//////////////////////////////////////////////////////////////////////////////////////////
			pomTable->GetTextLineColumns(&olLine, i);
			for(int k = 0; k < olLine.GetSize()-1; k++)
			{
				if(olLine[k].Text != "")
				{
					ilFilledCells++;
				}
				sprintf(pclZeile, "%d", i+1);
				blLineOK = pomTable->GetCellStatus(i, k);
				if(blLineOK == false)
				{
					ilStatus = false;
					sprintf(pclSpalte, "%d", k+1);
					if(k == 0 || k == 1)
					{
						olTableErrorText += LoadStg(IDS_STRING80) + LoadStg(IDS_STRING85) + CString(pclZeile) + LoadStg(IDS_STRING86) + CString(pclSpalte) + LoadStg(IDS_STRING87);
					}
				}
			}

			olLine.RemoveAll();
		}
	}
	///////////////////////////////////////////////////////////////////////////
	AfxGetApp()->DoWaitCursor(-1);
	if(ilStatus == true)
	{
		CString olText;
		m_POMO.GetWindowText(olText);
		if (olText.GetLength() < 3)
			olText += ".0";
		strcpy(pomPmx->Pomo,olText);
		m_POTU.GetWindowText(olText);
		if (olText.GetLength() < 3)
			olText += ".0";
		strcpy(pomPmx->Potu,olText);
		m_POWE.GetWindowText(olText);
		if (olText.GetLength() < 3)
			olText += ".0";
		strcpy(pomPmx->Powe,olText);
		m_POTH.GetWindowText(olText);
		if (olText.GetLength() < 3)
			olText += ".0";
		strcpy(pomPmx->Poth,olText);
		m_POFR.GetWindowText(olText);
		if (olText.GetLength() < 3)
			olText += ".0";
		strcpy(pomPmx->Pofr,olText);
		m_POSA.GetWindowText(olText);
		if (olText.GetLength() < 3)
			olText += ".0";
		strcpy(pomPmx->Posa,olText);
		m_POSU.GetWindowText(olText);
		if (olText.GetLength() < 3)
			olText += ".0";
		strcpy(pomPmx->Posu,olText);
		m_TIFR.GetWindowText(olText);
		olText = olText.Left(2) + olText.Right(2);
		strcpy(pomPmx->Tifr, olText);

		COleDateTime olTempDateFrom, olTempDateTo;
	
		//uhi 7.5.01
		m_VAFR.GetWindowText(olText);
		olTempDateFrom = OleDateStringToDate(olText);
		pomPmx->Vafr = olTempDateFrom;
		
		m_VATO.GetWindowText(olText);
		olTempDateTo = OleDateStringToDate(olText);
		pomPmx->Vato = olTempDateTo;
		
		int ilPacCount = 0;
		for(int  i = 0; i < 50; i++)
		{
			bool blLineOK = true;
			CCSPtrArray<TABLE_COLUMN> olLine;
			/////////////////////////////////////////////////////////////////////////////
			// Schreibe Daten aus pomOrgTable in PACTAB
			/////////////////////////////////////////////////////////////////////////////
			pomTable->GetTextLineColumns(&olLine, i);
			int test = olLine.GetSize();
			for(int k = 0; k < olLine.GetSize(); k++)
			{
				blLineOK = pomTable->GetCellStatus(i, k);
			}
			if(blLineOK == true)
			{
				if(olLine.GetSize() > 1)
				{
					if(olLine[0].Text != CString(""))
					{
						if(olLine[1].Text == CString(""))
						{
							PACDATA *prlPac = new PACDATA;
							strcpy(prlPac->Ctrc, olLine[0].Text);
							strcpy(prlPac->Tifr, pomPmx->Tifr);//Odgc 4
							prlPac->Pmxu = pomPmx->Urno;//Lead 5

							prlPac->IsChanged = DATA_NEW;
							pomPacData->Add(prlPac);
						}
						else
						{
							int ilTmpUrno = atoi(olLine[1].Text);
							//Nach �nderungen Checken
							bool blFound = false;
							for(int j = 0; j < pomPacData->GetSize(); j ++)
							{
								if(ilTmpUrno == pomPacData->GetAt(j).Urno)
								{
									PACDATA *prlPac = ogPacData.GetPacByUrno(pomPacData->GetAt(j).Urno);
									if(olLine[0].Text != CString(pomPacData->GetAt(j).Ctrc))
									{
										if(prlPac != NULL)
										{
											strcpy(prlPac->Ctrc, olLine[0].Text);
											strcpy(prlPac->Tifr,pomPmx->Tifr);//Odgc 4

											prlPac->IsChanged = DATA_CHANGED;
											ogPacData.UpdateInternal(prlPac);
											blFound = true;
											break;
										}
									}
									else
									{
										prlPac->IsChanged = DATA_UNCHANGED;
										blFound = true;
										break;
									}
								}
							}
						}
					}//Alle Columns sind gef�llt
					else
					{
						if (olLine[0].Text == CString(""))
						{
							if(olLine[1].Text != CString(""))
							{
								int ilTmpUrno = atoi(olLine[1].Text);
								bool blFound = false;
								for(int j = 0; j < pomPacData->GetSize(); j ++)
								{
									if(ilTmpUrno == pomPacData->GetAt(j).Urno)
									{
										PACDATA *prlPac = ogPacData.GetPacByUrno(pomPacData->GetAt(j).Urno);
										if(prlPac != NULL)
										{
											prlPac->IsChanged = DATA_DELETED;
										}
										break;
									}
								}
							}
						}
					}
				}
			}
			else
			{
				ilPacCount++;
			}
			olLine.RemoveAll();
		}
	
		CDialog::OnOK();
	}
	else
	{
		Beep(440,70);
		MessageBox(olErrorText,LoadStg(IDS_STRING145),MB_ICONERROR);
		m_TIFR.SetFocus();
		m_TIFR.SetSel(0,-1);
	}
}

BOOL CPmxDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	m_Caption = LoadStg(IDS_STRING758) + " <" + m_Caption + ">";
	SetWindowText(m_Caption);
	char clStat = ogPrivList.GetStat("PMXDLG.m_OK");
	SetWndStatAll(clStat,m_OK);

	//------------------------------------
	m_CDATD.SetBKColor(SILVER);
	m_CDATD.SetInitText(pomPmx->Cdat.Format("%d.%m.%Y"));
	// - - - - - - - - - - - - - - - - - -
	m_CDATT.SetBKColor(SILVER);
	m_CDATT.SetInitText(pomPmx->Cdat.Format("%H:%M"));
	//------------------------------------
	m_LSTUD.SetBKColor(SILVER);
	m_LSTUD.SetInitText(pomPmx->Lstu.Format("%d.%m.%Y"));
	// - - - - - - - - - - - - - - - - - -
	m_LSTUT.SetBKColor(SILVER);
	m_LSTUT.SetInitText(pomPmx->Lstu.Format("%H:%M"));
	//------------------------------------
	m_USEC.SetBKColor(SILVER);
	m_USEC.SetInitText(pomPmx->Usec);
	//------------------------------------
	m_USEU.SetBKColor(SILVER);
	m_USEU.SetInitText(pomPmx->Useu);
	//------------------------------------//WHITE,YELLOW,SILVER,RED,LYELLOW
	m_POMO.SetTypeToDouble(2, 1, 00.0,99.9);
	m_POMO.SetBKColor(YELLOW);  
	m_POMO.SetTextErrColor(RED);
	m_POMO.SetInitText(pomPmx->Pomo);
	//------------------------------------
	m_POTU.SetTypeToDouble(2, 1, 00.0,99.9);
	m_POTU.SetBKColor(YELLOW);  
	m_POTU.SetTextErrColor(RED);
	m_POTU.SetInitText(pomPmx->Potu);
	//------------------------------------
	m_POWE.SetTypeToDouble(2, 1, 00.0,99.9);
	m_POWE.SetBKColor(YELLOW);  
	m_POWE.SetTextErrColor(RED);
	m_POWE.SetInitText(pomPmx->Powe);
	//------------------------------------
	m_POTH.SetTypeToDouble(2, 1, 00.0,99.9);
	m_POTH.SetBKColor(YELLOW);  
	m_POTH.SetTextErrColor(RED);
	m_POTH.SetInitText(pomPmx->Poth);
	//------------------------------------
	m_POFR.SetTypeToDouble(2, 1, 00.0,99.9);
	m_POFR.SetBKColor(YELLOW);  
	m_POFR.SetTextErrColor(RED);
	m_POFR.SetInitText(pomPmx->Pofr);
	//------------------------------------
	m_POSA.SetTypeToDouble(2, 1, 00.0,99.9);
	m_POSA.SetBKColor(YELLOW);  
	m_POSA.SetTextErrColor(RED);
	m_POSA.SetInitText(pomPmx->Posa);
	//------------------------------------
	m_POSU.SetTypeToDouble(2, 1, 00.0,99.9);
	m_POSU.SetBKColor(YELLOW);  
	m_POSU.SetTextErrColor(RED);
	m_POSU.SetInitText(pomPmx->Posu);
	//------------------------------------
	m_TIFR.SetFormat("[##':'##]");
	m_TIFR.SetBKColor(YELLOW);  
	m_TIFR.SetTextErrColor(RED);
	if(strlen(pomPmx->Tifr) > 0)
	{
		char pclTmp[10]="";
		sprintf(pclTmp, "%s:%s", CString(pomPmx->Tifr).Left(2), CString(pomPmx->Tifr).Right(2));
		m_TIFR.SetInitText(pclTmp);
	}
	//------------------------------------

	//uhi 7.5.01
	m_VAFR.SetTypeToDate();
	m_VAFR.SetTextErrColor(RED);
	m_VAFR.SetBKColor(YELLOW);
	if(pomPmx->Vafr.GetStatus() == COleDateTime::valid)
		m_VAFR.SetInitText(pomPmx->Vafr.Format("%d.%m.%Y"));
	
	m_VATO.SetTypeToDate();
	m_VATO.SetTextErrColor(RED);
	m_VATO.SetBKColor(YELLOW);
	if(pomPmx->Vato.GetStatus() == COleDateTime::valid)
		m_VATO.SetInitText(pomPmx->Vato.Format("%d.%m.%Y"));

	InitTables();
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CPmxDlg::OnB1() 
{
	OnBf_Button(0);
	return;
}

void CPmxDlg::OnB2() 
{
	OnBf_Button(1);
	return;
}

void CPmxDlg::OnB3() 
{
	OnBf_Button(2);
	return;
}

void CPmxDlg::OnB4() 
{
	OnBf_Button(3);
	return;
}


//------------------------------------------------------------------------------------------------------------------------

void CPmxDlg::OnBf_Button(int ipColY)
{
	CStringArray olCol1, olCol2, olCol3;
	CCSPtrArray<COTDATA> olList;
	AfxGetApp()->DoWaitCursor(1);
	if(ogCotData.ReadSpecialData(&olList, "", "URNO,CTRC,CTRN", false) == true)
	{
		AfxGetApp()->DoWaitCursor(-1);
		for(int i = 0; i < olList.GetSize(); i++)
		{
			char pclUrno[20]="";
			sprintf(pclUrno, "%ld", olList[i].Urno);
			olCol3.Add(CString(pclUrno));
			olCol1.Add(CString(olList[i].Ctrc));
			olCol2.Add(CString(olList[i].Ctrn));
		}
		AwDlg *pomDlg = new AwDlg(&olCol1, &olCol2, &olCol3, LoadStg(IDS_STRING237), LoadStg(IDS_STRING229), this);
		if(pomDlg->DoModal() == IDOK)
		{
			int ilColX = -1, ilColY = -1;
			CCSTableListBox *pomListBox = pomTable->pomListBox;

			if(pomListBox->GetVisibleLeftTopColumn(ilColX, ilColY) == true)
			{
				ilColY += ipColY;
				pomTable->SetIPValue(ilColY, 0, pomDlg->omReturnString);
				pomTable->imCurrentColumn = 0;
				pomTable->MakeInplaceEdit(ilColY);
			}
		}
		delete pomDlg;
	}
	else
	{
		MessageBox(LoadStg(IDS_STRING321), LoadStg(IDS_STRING149), (MB_ICONSTOP|MB_OK));
	}
	olList.DeleteAll();

	AfxGetApp()->DoWaitCursor(-1);
}
//------------------------------------------------------------------------------------------------------------------------

CString CPmxDlg::GetCodeShort(CString opCode)
{
	CString olReturnString;
	olReturnString = "";

	if(opCode == LoadStg(IDS_STRING177)) //Funktionen 
	{
		olReturnString = "PAC";
	}
/*	else if(opCode == LoadStg())
	{
		olReturnString = "???";
	}*/

	return olReturnString;
}
//------------------------------------------------------------------------------------------------------------------------

LONG CPmxDlg::OnTableIPEditKillfocus( UINT wParam, LPARAM lParam)
{
	CCSIPEDITNOTIFY *prlNotify = (CCSIPEDITNOTIFY*)lParam;

	if(prlNotify->Text.IsEmpty())
		return 0L;
	
	if(prlNotify->SourceTable == pomTable) 
	{
		if(prlNotify->Column == 0)
		{
			CedaCotData olData;
			CCSPtrArray <COTDATA> omData;
			char pclWhere[128];
			sprintf(pclWhere,"WHERE CTRC='%s'", prlNotify->Text);
			prlNotify->UserStatus = true;
			prlNotify->Status = olData.ReadSpecialData(&omData, pclWhere, "CTRC", false);
			omData.DeleteAll();
			return 0L;
		}
	}
	return 0L;
}

//------------------------------------------------------------------------------------------------------------------------
// Implementierung
//------------------------------------------------------------------------------------------------------------------------
void CPmxDlg::InitTables()
{
	CRect olRect;
	m_HELP_PAC.GetWindowRect(olRect);
	ScreenToClient(olRect);
	olRect.DeflateRect(2,2);

	int ilTab = olRect.right - olRect.left - 15; 
	pomTable->SetHeaderSpacing(0);
	pomTable->SetMiniTable();
	pomTable->SetTableEditable(true);
    pomTable->SetTableData(this, olRect.left, olRect.right, olRect.top, olRect.bottom);
	pomTable->SetSelectMode(0);

	CCSPtrArray <TABLE_HEADER_COLUMN> omHeaderDataArray;
	TABLE_HEADER_COLUMN *prlHeader[2];
	pomTable->SetShowSelection(false);
	pomTable->ResetContent();

	prlHeader[0] = new TABLE_HEADER_COLUMN;
	prlHeader[0]->Alignment = COLALIGN_CENTER;
	prlHeader[0]->Length = (int)(ilTab - 5); 
	prlHeader[0]->Font = &ogCourier_Regular_8;
	prlHeader[0]->Text = CString(""); 

	prlHeader[1] = new TABLE_HEADER_COLUMN;
	prlHeader[1]->Alignment = COLALIGN_CENTER;
	prlHeader[1]->Length = 0; 
	prlHeader[1]->Font = &ogCourier_Regular_10;
	prlHeader[1]->Text = CString("");

	for(int ili = 0; ili < 2; ili++)
	{
		omHeaderDataArray.Add(prlHeader[ili]);
	}

	pomTable->SetHeaderFields(omHeaderDataArray);
	pomTable->SetDefaultSeparator();
	pomTable->SetTableEditable(true);
	pomTable->DisplayTable();
	omHeaderDataArray.DeleteAll();


// Set attributes for table
	CCSEDIT_ATTRIB rlAttribC1;

	rlAttribC1.Format = "X(8)";
	rlAttribC1.TextMinLenght = 0;
	rlAttribC1.TextMaxLenght = 8;
	rlAttribC1.Style = ES_UPPERCASE;
	
	CCSPtrArray<TABLE_COLUMN> olColList;
	TABLE_COLUMN rlColumnData;
	rlColumnData.Font = &ogCourier_Regular_8;//ogMS_Sans_Serif_8;
	

		for(int i = 0; i < pomPacData->GetSize(); i++)
		{
			rlColumnData.SeparatorType = SEPA_NONE;
			rlColumnData.VerticalSeparator = SEPA_LIKEVERTICAL;

			rlColumnData.Text = CString(pomPacData->GetAt(i).Ctrc);
			rlColumnData.EditAttrib = rlAttribC1;

			rlColumnData.SeparatorType = SEPA_NONE;
			rlColumnData.VerticalSeparator = SEPA_NONE;
			olColList.NewAt(olColList.GetSize(), rlColumnData);
			
			if(pomPacData->GetAt(i).Urno != 0)
			{
				char pclText[20];
				sprintf(pclText, "%ld", pomPacData->GetAt(i).Urno);
				rlColumnData.Text = CString(pclText);
			}
			else
			{
				rlColumnData.Text = CString("");
			}
			rlColumnData.SeparatorType = SEPA_NONE;
			rlColumnData.VerticalSeparator = SEPA_NONE;
			rlColumnData.HorizontalSeparator = SEPA_NONE;
			olColList.NewAt(olColList.GetSize(), rlColumnData);

			pomTable->AddTextLine(olColList, (void*)(NULL));
			olColList.DeleteAll();
		}
		for(i = pomPacData->GetSize(); i < 50; i++)
		{
			rlColumnData.SeparatorType = SEPA_NONE;
			rlColumnData.VerticalSeparator = SEPA_LIKEVERTICAL;
			rlColumnData.Text = CString("");
			rlColumnData.EditAttrib = rlAttribC1;
			rlColumnData.SeparatorType = SEPA_NONE;
			rlColumnData.VerticalSeparator = SEPA_NONE;
			olColList.NewAt(olColList.GetSize(), rlColumnData);

			rlColumnData.Text = CString("");
			rlColumnData.SeparatorType = SEPA_NONE;
			rlColumnData.VerticalSeparator = SEPA_NONE;
			olColList.NewAt(olColList.GetSize(), rlColumnData);

			pomTable->AddTextLine(olColList, (void*)(NULL));
			olColList.DeleteAll();
		}
//	}
	pomTable->DisplayTable();
	pomTable->SetColumnEditable(0, true);
	pomTable->SetColumnEditable(1, false);
}
//------------------------------------------------------------------------------------------------------------------------

