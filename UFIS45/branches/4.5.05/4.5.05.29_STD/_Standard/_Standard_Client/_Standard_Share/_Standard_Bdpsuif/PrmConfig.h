// PrmConfig Configuration and priviledges for PRM System

#ifndef _PRMCONFIG
#define _PRMCONFIG

#include <CCSCedaData.h>
#include <CedaHagData.h>

class PrmConfig: public CCSCedaData
{

// Construction && destruction
public:
		PrmConfig();

		bool UseHag;
		
		
		void ReadHandlingAgentsList();
		bool CheckHandlingAgent(CString opHandlingAgent);
		char *getHandlingAgentShortName();
		HAGDATA *getHandlingAgent();
		void Initialize();

private:
		char cmHandlingAgentsList[512];
		char cmAirlineList[512];
		HAGDATA *prmHag;
		CedaHAGData *prmHagData;
	
public:
		char *getAirlineList();

};

extern PrmConfig ogPrmConfig;

#endif
