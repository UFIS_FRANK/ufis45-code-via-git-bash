// CedaBlkData.cpp
 
#include <stdafx.h>
#include <CedaBlkData.h>
#include <resource.h>


void ProcessBlkCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//--CEDADATA-----------------------------------------------------------------------------------------------

CedaBlkData::CedaBlkData() : CCSCedaData(&ogCommHandler)
{
    BEGIN_CEDARECINFO(BLKDATA, BlkDataRecInfo)
		FIELD_LONG			(Urno,"URNO")
		FIELD_DATE			(Nafr,"NAFR")
		FIELD_DATE			(Nato,"NATO")
		FIELD_CHAR_TRIM		(Days,"DAYS")
		FIELD_CHAR_TRIM		(Type,"TYPE")
		FIELD_CHAR_TRIM		(Tabn,"TABN")
		FIELD_CHAR_TRIM		(Resn,"RESN")
		FIELD_LONG			(Burn,"BURN")
		FIELD_CHAR_TRIM		(Tifr,"TIFR")
		FIELD_CHAR_TRIM		(Tito,"TITO")
		FIELD_LONG			(Ibit,"IBIT")
	END_CEDARECINFO

    // Copy the record structure
    for (int i = 0; i < sizeof(BlkDataRecInfo)/sizeof(BlkDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&BlkDataRecInfo[i],sizeof(CEDARECINFO));
        omRecInfo.Add(prpCedaRecInfo);
	}
    // Initialize table names and field names
    strcpy(pcmTableName,"BLK");



    sprintf(pcmListOfFields,"URNO,NAFR,NATO,DAYS,TYPE,TABN,RESN,BURN,TIFR,TITO,IBIT");

	pcmFieldList = pcmListOfFields; //because in CedaData pcmFieldList is just a pointer

	omData.RemoveAll();
}

//---------------------------------------------------------------------------------------------------------

void CedaBlkData::GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType)
{
	//ropFields.Add("");
	//ropDesription.Add(LoadStg());
	//ropType.Add("");

}

//--REGISTER----------------------------------------------------------------------------------------------

void CedaBlkData::Register(void)
{
	ogDdx.Register((void *)this,BC_BLK_CHANGE,	CString("BLKDATA"), CString("Blk-changed"),	ProcessBlkCf);
	ogDdx.Register((void *)this,BC_BLK_NEW,		CString("BLKDATA"), CString("Blk-new"),		ProcessBlkCf);
	ogDdx.Register((void *)this,BC_BLK_DELETE,	CString("BLKDATA"), CString("Blk-deleted"),	ProcessBlkCf);
}

//--~CEDADATA----------------------------------------------------------------------------------------------

CedaBlkData::~CedaBlkData(void)
{
	omRecInfo.DeleteAll();
	ClearAll();
}

//--CLEAR-ALL----------------------------------------------------------------------------------------------

void CedaBlkData::ClearAll(bool bpWithRegistration)
{
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(bpWithRegistration)
	{
		ogDdx.UnRegister(this,NOTUSED);
	}
}

//--READ-ALL-----------------------------------------------------------------------------------------------

bool CedaBlkData::Read(char *pspWhere /*NULL*/)
{
    // Select data from the database
	bool ilRc = true;
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(pspWhere == NULL)
	{	
		ilRc = CedaAction("RT");
	}
	else
	{
		ilRc = CedaAction("RT", pspWhere);
	}
	if (ilRc != true)
	{
		return false;
	}
	// Load data from CedaData into the dynamic array of record
	for (int ilLc = 0; ilRc == true; ilLc++)
	{
		BLKDATA *prlBlk = new BLKDATA;
		if ((ilRc = GetFirstBufferRecord(prlBlk)) == true)
		{
			omData.Add(prlBlk);//Update omData
			omUrnoMap.SetAt((void *)prlBlk->Urno,prlBlk);
		}
		else
		{
			delete prlBlk;
		}
	}
    return true;

}

//--INSERT-------------------------------------------------------------------------------------------------

bool CedaBlkData::Insert(BLKDATA *prpBlk)
{
	prpBlk->IsChanged = DATA_NEW;
	if(Save(prpBlk) == false) return false; //Update Database
	InsertInternal(prpBlk);
    return true;
}

//--INSERT-INTERNAL--------------------------------------------------------------------------------------

bool CedaBlkData::InsertInternal(BLKDATA *prpBlk)
{
	ogDdx.DataChanged((void *)this, BLK_NEW,(void *)prpBlk ); //Update Viewer
	omData.Add(prpBlk);//Update omData
	omUrnoMap.SetAt((void *)prpBlk->Urno,prpBlk);
    return true;
}

//--DELETE-------------------------------------------------------------------------------------------------

bool CedaBlkData::Delete(long lpUrno)
{
	BLKDATA *prlBlk = GetBlkByUrno(lpUrno);
	if (prlBlk != NULL)
	{
		prlBlk->IsChanged = DATA_DELETED;
		if(Save(prlBlk) == false) return false; //Update Database
		DeleteInternal(prlBlk);
	}
    return true;
}

//--DELETE-INTERNAL----------------------------------------------------------------------------------------

bool CedaBlkData::DeleteInternal(BLKDATA *prpBlk)
{
	ogDdx.DataChanged((void *)this,BLK_DELETE,(void *)prpBlk); //Update Viewer
	omUrnoMap.RemoveKey((void *)prpBlk->Urno);
	int ilBlkCount = omData.GetSize();
	for (int ilLc = 0; ilLc < ilBlkCount; ilLc++)
	{
		if (omData[ilLc].Urno == prpBlk->Urno)
		{
			omData.DeleteAt(ilLc);//Update omData
			break;
		}
	}
    return true;
}

//--UPDATE-------------------------------------------------------------------------------------------------

bool CedaBlkData::Update(BLKDATA *prpBlk)
{
	if (GetBlkByUrno(prpBlk->Urno) != NULL)
	{
		if (prpBlk->IsChanged == DATA_UNCHANGED)
		{
			prpBlk->IsChanged = DATA_CHANGED;
		}
		if(Save(prpBlk) == false) return false; //Update Database
		UpdateInternal(prpBlk);
	}
    return true;
}

//--UPDATE-INTERNAL--------------------------------------------------------------------------------------------

bool CedaBlkData::UpdateInternal(BLKDATA *prpBlk)
{
	BLKDATA *prlBlk = GetBlkByUrno(prpBlk->Urno);
	if (prlBlk != NULL)
	{
		*prlBlk = *prpBlk; //Update omData
		ogDdx.DataChanged((void *)this,BLK_CHANGE,(void *)prlBlk); //Update Viewer
	}
    return true;
}

//--GET-BY-URNO--------------------------------------------------------------------------------------------

BLKDATA *CedaBlkData::GetBlkByUrno(long lpUrno)
{
	BLKDATA  *prlBlk;
	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlBlk) == TRUE)
	{
		return prlBlk;
	}
	return NULL;
}

//--READSPECIALDATA-------------------------------------------------------------------------------------

bool CedaBlkData::ReadSpecialData(CCSPtrArray<BLKDATA> *popBlk,char *pspWhere,char *pspFieldList,bool ipSYS/*=true*/)
{
	bool ilRc = true;
	char pclFieldList[256] = " ";

	if(strlen(pspFieldList) > 0) 
	{
		strcpy(pclFieldList, pspFieldList);
	}
	else
	{
		strcpy(pclFieldList, pcmListOfFields);
	}
	if(ipSYS == true) 
	{
		if (CedaAction("SYS","BLK",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	else
	{
		if (CedaAction("RT","BLK",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	if(popBlk != NULL)
	{
		for (int ilLc = 0; ilRc == true; ilLc++)
		{
			BLKDATA *prpBlk = new BLKDATA;
			if ((ilRc = GetBufferRecord(ilLc,prpBlk,CString(pclFieldList))) == true)
			{
				popBlk->Add(prpBlk);
			}
			else
			{
				delete prpBlk;
			}
		}
		if(popBlk->GetSize() == 0) return false;
	}
    return true;
}

//--SAVE---------------------------------------------------------------------------------------------------

bool CedaBlkData::Save(BLKDATA *prpBlk)
{
	bool ilRc = true;
	CString olListOfData;
	char pclSelection[124];
	char pclData[524];

	if (prpBlk->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}
	switch(prpBlk->IsChanged)
	{
	case DATA_NEW:
		MakeCedaData(&omRecInfo,olListOfData,prpBlk);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("IRT","","",pclData);
		prpBlk->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpBlk->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpBlk);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("URT",pclSelection,"",pclData);
		prpBlk->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_DELETED:
		if(prpBlk->Urno == 0)
		{
			sprintf(pclSelection, "WHERE BURN = %ld AND TABN = '%s'", prpBlk->Burn, prpBlk->Tabn);
		}
		else
		{
			sprintf(pclSelection, "WHERE URNO = %ld", prpBlk->Urno);
		}
		ilRc = CedaAction("DRT",pclSelection);
		break;
	}
    return ilRc;
}

//--PROCESS-CF---------------------------------------------------------------------------------------------

void  ProcessBlkCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	ogBlkData.ProcessBc(ipDDXType,vpDataPointer,ropInstanceName);
}

//--PROCESS-BC---------------------------------------------------------------------------------------------

void  CedaBlkData::ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlBlkData;
	prlBlkData = (struct BcStruct *) vpDataPointer;
	BLKDATA *prlBlk;
	if(ipDDXType == BC_BLK_NEW)
	{
		prlBlk = new BLKDATA;
		GetRecordFromItemList(prlBlk,prlBlkData->Fields,prlBlkData->Data);
		InsertInternal(prlBlk);
	}
	if(ipDDXType == BC_BLK_CHANGE)
	{
		long llUrno = GetUrnoFromSelection(prlBlkData->Selection);
		prlBlk = GetBlkByUrno(llUrno);
		if(prlBlk != NULL)
		{
			GetRecordFromItemList(prlBlk,prlBlkData->Fields,prlBlkData->Data);
			UpdateInternal(prlBlk);
		}
	}
	if(ipDDXType == BC_BLK_DELETE)
	{
		long llUrno = GetUrnoFromSelection(prlBlkData->Selection);

		prlBlk = GetBlkByUrno(llUrno);
		if (prlBlk != NULL)
		{
			DeleteInternal(prlBlk);
		}
	}
}

//---------------------------------------------------------------------------------------------------------
