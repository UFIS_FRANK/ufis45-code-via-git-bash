// FidsLogoNamesDlg.cpp : implementation file
//

#include <stdafx.h>
#include <BDPSUIF.h>
#include <FidsLogoNamesDlg.h>
#include <PrivList.h>
#include <AwDlg.h>
#include <CedaFLGData.h>
#include <CedaALTData.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// FidsLogoNamesDlg dialog


FidsLogoNamesDlg::FidsLogoNamesDlg(FLGDATA *popFLG,const CString& ropCaption,CWnd* pParent /*=NULL*/)
	: CDialog(FidsLogoNamesDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(FidsLogoNamesDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	pomFLG	  = popFLG;
	m_Caption = ropCaption;
	pomTable = new CCSTable;
}

FidsLogoNamesDlg::~FidsLogoNamesDlg()
{
	if(omMapAlc2ToAlc3.GetCount() > 0)
	{
		omMapAlc2ToAlc3.RemoveAll();	
	}
	if(omMapAlc3ToAlc2.GetCount() > 0)
	{
		omMapAlc3ToAlc2.RemoveAll();
	}
	if(omListAlc2CombineAlc3.GetCount() > 0)
	{
		omListAlc2CombineAlc3.RemoveAll();
	}
	delete pomTable;
}

void FidsLogoNamesDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(FidsLogoNamesDlg)
	DDX_Control(pDX, IDC_EDIT_NAME, m_LGSN);
	DDX_Control(pDX, IDC_EDIT_FULLNAME, m_LGFN);
	DDX_Control(pDX, IDC_EDIT_TYPE, m_TYPE);
	DDX_Control(pDX, IDC_EDIT_ALC2, m_ALC2);
	DDX_Control(pDX, IDC_EDIT_ALC3, m_ALC3);
	DDX_Control(pDX, IDC_CDAT_D, m_CDATD);
	DDX_Control(pDX, IDC_CDAT_T, m_CDATT);
	DDX_Control(pDX, IDC_LSTU_D, m_LSTUD);
	DDX_Control(pDX, IDC_LSTU_T, m_LSTUT);
	DDX_Control(pDX, IDC_USEU, m_USEU);
	DDX_Control(pDX, IDC_USEC, m_USEC);		
	DDX_Control(pDX, IDC_BUTTON_TYPE, m_BTN_TYPE);	
	DDX_Control(pDX, IDC_BUTTON_ALC2, m_BTN_ALC2);	
	DDX_Control(pDX, IDC_BUTTON_ALC3, m_BTN_ALC3);
	DDX_Control(pDX, IDCANCEL, m_CANCEL);
	DDX_Control(pDX, IDOK, m_OK);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(FidsLogoNamesDlg, CDialog)
	//{{AFX_MSG_MAP(FidsLogoNamesDlg)	
	ON_BN_CLICKED(IDC_BUTTON_TYPE, OnButtonTypeClicked)
	ON_BN_CLICKED(IDC_BUTTON_ALC2, OnButtonAlc2Clicked)
	ON_BN_CLICKED(IDC_BUTTON_ALC3, OnButtonAlc3Clicked)	
	ON_MESSAGE(WM_EDIT_KILLFOCUS, OnKillfocus)	
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// FidsLogoNamesDlg message handlers

BOOL FidsLogoNamesDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	m_Caption = LoadStg(IDS_STRING1147) + " <" + m_Caption + ">";
	SetWindowText(m_Caption);
	char clStat = ogPrivList.GetStat("DISPLAYDLG.m_OK");
	SetWndStatAll(clStat,m_OK);
	
	//------------------------------------		
	m_LGSN.SetFormat("X(30)");
	m_LGSN.SetTextLimit(1,30);	
	m_LGSN.SetBKColor(YELLOW);		
	m_LGSN.SetTextErrColor(RED);
	m_LGSN.SetInitText(pomFLG->Lgsn);
	m_LGSN.SetSecState(ogPrivList.GetStat("LOGONAMESDLG.m_LGSN"));
	//------------------------------------	
	m_LGFN.SetFormat("X(128)");
	m_LGFN.SetTextLimit(1,128);
	m_LGFN.SetBKColor(YELLOW);		
	m_LGFN.SetTextErrColor(RED);
	m_LGFN.SetInitText(pomFLG->Lgfn);
	m_LGFN.SetSecState(ogPrivList.GetStat("LOGONAMESDLG.m_LGFN"));
	//------------------------------------	
	//m_TYPE.SetTypeToString("X(1)", 1, 1);
	m_TYPE.SetFormat("'C'|'G'");
	m_TYPE.SetTextLimit(1,1);
	m_TYPE.SetBKColor(YELLOW);	
	m_TYPE.SetInitText(pomFLG->Type);
	m_TYPE.SetSecState(ogPrivList.GetStat("LOGONAMESDLG.m_TYPE"));
	//------------------------------------	
	m_ALC2.SetTypeToString("x|#x|#", 2, 0);
	m_ALC2.SetTextErrColor(RED);
	m_ALC2.SetInitText(pomFLG->Alc2);
	m_ALC2.SetSecState(ogPrivList.GetStat("LOGONAMESDLG.m_ALC2"));
	//------------------------------------	
	m_ALC3.SetTypeToString("x|#x|#x|#", 3, 0);
	m_ALC3.SetTextErrColor(RED);
	m_ALC3.SetInitText(pomFLG->Alc3);
	m_ALC3.SetSecState(ogPrivList.GetStat("LOGONAMESDLG.m_ALC3"));
	//------------------------------------
	m_CDATD.SetBKColor(SILVER);			
	m_CDATD.SetInitText(pomFLG->Cdat.Format("%d.%m.%Y"));
	m_CDATD.SetSecState(ogPrivList.GetStat("LOGONAMESDLG.m_CDAT"));
	m_CDATT.SetBKColor(SILVER);
	m_CDATT.SetInitText(pomFLG->Cdat.Format("%H:%M"));
	m_CDATT.SetSecState(ogPrivList.GetStat("LOGONAMESDLG.m_CDAT"));
	//------------------------------------
	m_LSTUD.SetBKColor(SILVER);
	m_LSTUD.SetInitText(pomFLG->Lstu.Format("%d.%m.%Y"));
	m_LSTUD.SetSecState(ogPrivList.GetStat("LOGONAMESDLG.m_LSTU"));
	m_LSTUT.SetBKColor(SILVER);
	m_LSTUT.SetInitText(pomFLG->Lstu.Format("%H:%M"));
	m_LSTUT.SetSecState(ogPrivList.GetStat("LOGONAMESDLG.m_LSTU"));
	//------------------------------------
	m_USEC.SetBKColor(SILVER);
	m_USEC.SetInitText(pomFLG->Usec);
	m_USEC.SetSecState(ogPrivList.GetStat("LOGONAMESDLG.m_USEC"));
	//------------------------------------
	m_USEU.SetBKColor(SILVER);
	m_USEU.SetInitText(pomFLG->Useu);
	m_USEU.SetSecState(ogPrivList.GetStat("LOGONAMESDLG.m_USEU"));

	SetWndStatAll(ogPrivList.GetStat("LOGONAMESDLG.m_BTN_TYPE"),m_BTN_TYPE);
	SetWndStatAll(ogPrivList.GetStat("LOGONAMESDLG.m_BTN_ALC2"),m_BTN_ALC2);
	SetWndStatAll(ogPrivList.GetStat("LOGONAMESDLG.m_BTN_ALC3"),m_BTN_ALC3);
	
	if(omMapAlc2ToAlc3.GetCount() > 0)
	{
		omMapAlc2ToAlc3.RemoveAll();	
	}
	if(omMapAlc3ToAlc2.GetCount() > 0)
	{
		omMapAlc3ToAlc2.RemoveAll();
	}
	if(omListAlc2CombineAlc3.GetCount() > 0)
	{
		omListAlc2CombineAlc3.RemoveAll();
	}

	ALTDATA* polALTData = NULL;
	for(int i = 0; i < ogALTData.omData.GetSize(); i++)
	{
		polALTData = &ogALTData.omData[i];
		if(strlen(polALTData->Alc2) > 0)
		{
			omMapAlc2ToAlc3.SetAt((CString)polALTData->Alc2,(CString)polALTData->Alc3);
		}
		if(strlen(polALTData->Alc3) > 0)
		{
			omMapAlc3ToAlc2.SetAt((CString)polALTData->Alc3,(CString)polALTData->Alc2);
		}
		if((strlen(polALTData->Alc2) > 0) && (strlen(polALTData->Alc3) > 0))
		{
			omListAlc2CombineAlc3.AddTail((CString)polALTData->Alc2 + (CString)polALTData->Alc3);
		}
	}
	//------------------------------------
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void FidsLogoNamesDlg::OnOK() 
{
	// TODO: Add extra validation here
	CWaitCursor olWait;
	CString olErrorText;
	bool ilStatus = true;
	
	if(m_LGSN.GetStatus() == false)
	{
		ilStatus = false;
		if(m_LGSN.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING1142) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING1142) + ogNotFormat;
		} 
	}
	if(m_LGFN.GetStatus() == false)
	{
		ilStatus = false;
		if(m_LGFN.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING1141) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING1141) + ogNotFormat;
		} 
	}
	if(m_TYPE.GetStatus() == false)
	{
		ilStatus = false;
		if(m_TYPE.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING1143) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING1143) + ogNotFormat;
		} 
	}

	CString olAirline2Code;
	CString olAirline3Code;	
	CString olAirlineEditCode;

	m_ALC2.GetWindowText(olAirline2Code);	

	if(olAirline2Code.GetLength() > 0)
	{
		olAirline2Code.TrimLeft();
		olAirline2Code.TrimRight();
		if(omMapAlc2ToAlc3.Lookup(olAirline2Code,olAirline3Code) == FALSE)
		{
			m_ALC3.GetWindowText(olAirlineEditCode);
			if(omMapAlc3ToAlc2.Lookup(olAirlineEditCode,olAirline2Code) == FALSE)
			{
				ilStatus = false;
			    m_ALC2.SetStatus(false);
			}
			else
			{
				olAirline2Code.TrimLeft();
				olAirline2Code.TrimRight();
				m_ALC2.SetWindowText(olAirline2Code);				
				m_ALC2.SetStatus(true);
			}
		}
		else
		{
			m_ALC2.SetWindowText(olAirline2Code);
			m_ALC3.GetWindowText(olAirlineEditCode);
			if(omMapAlc3ToAlc2.Lookup(olAirlineEditCode,olAirline2Code) == FALSE)
			{
				olAirline3Code.TrimLeft();
				olAirline3Code.TrimRight();
				m_ALC3.SetWindowText(olAirline3Code);
			}
			else
			{
				m_ALC2.SetWindowText(olAirline2Code);				
				m_ALC2.SetStatus(true);
			}

		}
	}

	m_ALC3.GetWindowText(olAirline3Code);
	if(ilStatus == true && olAirline3Code.GetLength() > 0)
	{
		olAirline3Code.TrimLeft();
		olAirline3Code.TrimRight();
		if(omMapAlc3ToAlc2.Lookup(olAirline3Code,olAirline2Code) == FALSE)
		{
			ilStatus = false;
			m_ALC3.SetStatus(false);
		}
		else
		{
			m_ALC3.SetWindowText(olAirline3Code);
			olAirline2Code.TrimLeft();
			olAirline2Code.TrimRight();
			m_ALC2.SetWindowText(olAirline2Code);			
			m_ALC2.SetStatus(true);
			m_ALC3.SetStatus(true);
		}
	}

	if(m_ALC2.GetStatus() == false || m_ALC3.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING327);
	}

	/////////////////////////////////////////////////////////////////////////
	//Konsistenz-Check//
	if(ilStatus == true)
	{
		CString olLgsn;
		CString olType;
		char clWhere[100];		
		CCSPtrArray<FLGDATA> olFLGCPA;
		m_LGSN.GetWindowText(olLgsn);
		m_TYPE.GetWindowText(olType);
		if (olLgsn.GetLength() > 0)
		{
			sprintf(clWhere,"WHERE LGSN='%s' AND TYPE='%s'",olLgsn, olType);
			if(ogFLGData.ReadSpecialData(&olFLGCPA,clWhere,"URNO,LGSN",false) == true)
			{
				if(olFLGCPA[0].Urno != pomFLG->Urno)
				{
					ilStatus = false;
					olErrorText += LoadStg(IDS_STRING32816) + LoadStg(IDS_EXIST);
				}
			}
			olFLGCPA.DeleteAll();
		}
	}

	AfxGetApp()->DoWaitCursor(-1);
	if (ilStatus == true)
	{
		m_LGSN.GetWindowText(pomFLG->Lgsn,sizeof(pomFLG->Lgsn));
		m_LGFN.GetWindowText(pomFLG->Lgfn,sizeof(pomFLG->Lgfn));
		m_TYPE.GetWindowText(pomFLG->Type,sizeof(pomFLG->Type));
		m_ALC2.GetWindowText(pomFLG->Alc2,sizeof(pomFLG->Alc2));
		m_ALC3.GetWindowText(pomFLG->Alc3,sizeof(pomFLG->Alc3));
		CDialog::OnOK();
	}
	else
	{
		Beep(440,70);
		MessageBox(olErrorText,LoadStg(IDS_STRING145),MB_ICONERROR);
		m_LGSN.SetFocus();
		m_LGSN.SetSel(0,-1);
	}

}

//----------------------------------------------------------------------------------------

void FidsLogoNamesDlg::OnButtonAlc2Clicked() 
{
	CStringArray olCol1, olCol2,olCol3;
	CCSPtrArray<ALTDATA> olList;

	AfxGetApp()->DoWaitCursor(1);
	if(ogALTData.ReadSpecialData(&olList, "  ORDER BY ALC2", "URNO,ALC2,ALFN", false) == true)
	{
		AfxGetApp()->DoWaitCursor(-1);
		for(int i = 0; i < olList.GetSize(); i++)
		{
			if(strlen(olList[i].Alc2) == 0)
			{
				continue;
			}
			char pclUrno[20]="";
			sprintf(pclUrno, "%ld", olList[i].Urno);
			olCol3.Add(CString(pclUrno));			
			olCol1.Add(CString(olList[i].Alc2));
			olCol2.Add(CString(olList[i].Alfn));
		}

		AwDlg *pomDlg = new AwDlg(&olCol1,&olCol2,&olCol3,LoadStg(IDS_STRING289),LoadStg(IDS_STRING326),this);
		pomDlg->bmMultipleSelection = false;
		if(pomDlg->DoModal() == IDOK)
		{
			m_ALC2.SetWindowText(pomDlg->omReturnString);			
			m_ALC2.SetFocus();
			ALTDATA* polAltData = ogALTData.GetALTByUrno(pomDlg->lmReturnUrno);
			if(polAltData != NULL)
			{
				m_ALC3.SetWindowText(polAltData->Alc3);
			}			
		}
		delete pomDlg;
	}
	else
	{
		MessageBox(LoadStg(IDS_STRING321), LoadStg(IDS_STRING149), (MB_ICONSTOP|MB_OK));
	}
	olList.DeleteAll();

	AfxGetApp()->DoWaitCursor(-1);
}

//----------------------------------------------------------------------------------------

void FidsLogoNamesDlg::OnButtonAlc3Clicked() 
{
	CStringArray olCol1, olCol2,olCol3;
	CCSPtrArray<ALTDATA> olList;

	AfxGetApp()->DoWaitCursor(1);
	if(ogALTData.ReadSpecialData(&olList, " ORDER BY ALC3", "URNO,ALC3,ALFN", false) == true)
	{
		AfxGetApp()->DoWaitCursor(-1);
		for(int i = 0; i < olList.GetSize(); i++)
		{
			if(strlen(olList[i].Alc3) == 0)
			{
				continue;
			}
			char pclUrno[20]="";
			sprintf(pclUrno, "%ld", olList[i].Urno);
			olCol3.Add(CString(pclUrno));			
			olCol1.Add(CString(olList[i].Alc3));
			olCol2.Add(CString(olList[i].Alfn));
		}

		AwDlg *pomDlg = new AwDlg(&olCol1,&olCol2,&olCol3,LoadStg(IDS_STRING290),LoadStg(IDS_STRING326),this);
		pomDlg->bmMultipleSelection = false;
		if(pomDlg->DoModal() == IDOK)
		{
			m_ALC3.SetWindowText(pomDlg->omReturnString);			
			m_ALC3.SetFocus();
			ALTDATA* polAltData = ogALTData.GetALTByUrno(pomDlg->lmReturnUrno);
			if(polAltData != NULL)
			{
				m_ALC2.SetWindowText(polAltData->Alc2);
			}			
		}
		delete pomDlg;
	}
	else
	{
		MessageBox(LoadStg(IDS_STRING321), LoadStg(IDS_STRING149), (MB_ICONSTOP|MB_OK));
	}
	olList.DeleteAll();

	AfxGetApp()->DoWaitCursor(-1);
}

void FidsLogoNamesDlg::OnButtonTypeClicked()
{
	CStringArray olCol1, olCol2,olCol3;
	CCSPtrArray<ALTDATA> olList;

	AfxGetApp()->DoWaitCursor(1);	
	for(int i = 0; i < 2; i++)
	{	
		olCol3.Add(CString("0"));
		olCol1.Add(i ==0 ? "C" : "G");
		olCol2.Add(i ==0 ? "Counters" : "Gates");
	}

	AwDlg *pomDlg = new AwDlg(&olCol1,&olCol2,&olCol3,"",LoadStg(IDS_STRING935),this);
	if(pomDlg->DoModal() == IDOK)
	{
		m_TYPE.SetWindowText(pomDlg->omReturnString);
		m_TYPE.SetFocus();
	}
	delete pomDlg;
	olList.DeleteAll();
	AfxGetApp()->DoWaitCursor(-1);
}

LONG FidsLogoNamesDlg::OnKillfocus(UINT lParam, LONG wParam) 
{
	CString olAirline2Code;
	CString olAirline3Code;
	CString olAirlineEditCode;
	CString olAirlineAlc2CombineAlc3;
	if(lParam == (UINT)m_ALC2.imID && m_ALC2.GetWindowTextLength() > 0)
	{
		m_ALC2.GetWindowText(olAirline2Code);
		
		if(omMapAlc2ToAlc3.Lookup(olAirline2Code,olAirline3Code) == TRUE)
		{			
			m_ALC3.GetWindowText(olAirlineEditCode);
			olAirlineAlc2CombineAlc3 = olAirline2Code + olAirlineEditCode;

			//if((omMapAlc3ToAlc2.Lookup(olAirlineEditCode,olAirline2Code) == FALSE) &&
			 //  (olAirlineEditCode.CompareNoCase(olAirline3Code) != 0))
			if(omListAlc2CombineAlc3.Find(olAirlineAlc2CombineAlc3) == NULL)
			{
				m_ALC3.SetWindowText(olAirline3Code);
				m_ALC3.SetTextChangedColor(GREEN);
			}
			m_ALC3.SetStatus(true);
			m_ALC2.SetStatus(true);
			m_ALC2.SetTextChangedColor(GREEN);
		}
		else
		{
			m_ALC2.SetStatus(false);	        
			m_ALC2.SetTextChangedColor(RED);
		}
	}
	if(lParam == (UINT)m_ALC3.imID && m_ALC3.GetWindowTextLength() > 0)
	{
		m_ALC3.GetWindowText(olAirline3Code);
		if(omMapAlc3ToAlc2.Lookup(olAirline3Code,olAirline2Code) == TRUE)
		{
			m_ALC2.GetWindowText(olAirlineEditCode);
			if(olAirlineEditCode.CompareNoCase(olAirline2Code) != 0)
			{
				m_ALC2.SetWindowText(olAirline2Code);
				m_ALC2.SetTextChangedColor(GREEN);
			}			
			m_ALC2.SetStatus(true);			
			m_ALC3.SetStatus(true);
			m_ALC3.SetTextChangedColor(GREEN);
		}
		else
		{
			m_ALC3.SetStatus(false);			
			m_ALC3.SetTextChangedColor(RED);
		}
	}
	return 0L;	
}

