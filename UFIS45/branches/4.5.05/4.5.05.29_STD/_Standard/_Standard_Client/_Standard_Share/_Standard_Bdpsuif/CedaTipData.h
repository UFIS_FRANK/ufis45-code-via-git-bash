// CedaTipData.h

#ifndef __CEDATIPDATA__
#define __CEDATIPDATA__
 
#include <stdafx.h>
#include <basicdata.h>

//---------------------------------------------------------------------------------------------------------

struct TIPDATA 
{
	char 	 Tipc[6];
	char 	 Tipt[130];
	char 	 Tipu[3];
	char 	 Tipv[6];
	CTime 	 Vafr; 		// G�ltig von
	CTime 	 Vato; 		// G�ltig bis
	long 	 Urno; 		// Eindeutige Datensatz-Nr.
	CTime 	 Cdat; 		// Erstellungsdatum
	CTime 	 Lstu; 		// Datum letzte �nderung
	char 	 Prfl[3]; 	// Protokollierungskennzeichen
	char 	 Usec[34]; 	// Anwender (Ersteller)
	char 	 Useu[34]; 	// Anwender (letzte �nderung)

	int		 IsChanged;	// Check whether Data has Changed f�r Relaese

	TIPDATA(void)
	{ 
		memset(this,'\0',sizeof(*this));
		IsChanged	=	DATA_UNCHANGED;
		Cdat		=	TIMENULL;
		Lstu		=	TIMENULL;
		Vafr		=	TIMENULL;
		Vato		=	TIMENULL;
	}

}; // end TipDataStrukt

//---------------------------------------------------------------------------------------------------------
// Class declaratino

class CedaTipData: public CCSCedaData
{
// Attributes
public:
    CMapPtrToPtr omUrnoMap;

    CCSPtrArray<TIPDATA> omData;

	char pcmListOfFields[2048];

// Operations
public:
    CedaTipData();
	~CedaTipData();
	virtual void GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType);
	void Register(void);
	void ClearAll(bool bpWithRegistration = true);
    bool Read(char *pspWhere = NULL);
	bool Insert(TIPDATA *prpTip);
	bool InsertInternal(TIPDATA *prpTip);
	bool Update(TIPDATA *prpTip);
	bool UpdateInternal(TIPDATA *prpTip);
	bool Delete(long lpUrno);
	bool DeleteInternal(TIPDATA *prpTip);
	bool ReadSpecialData(CCSPtrArray<TIPDATA> *popTip,char *pspWhere,char *pspFieldList,bool ipSYS=true);
	bool Save(TIPDATA *prpTip);
	void ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
	TIPDATA  *GetTipByUrno(long lpUrno);
	void SetWhere(const CString& ropWhere){omWhere = ropWhere;} //Prf: 8795
	void GetWhere(CString& ropWhere) const{ropWhere = omWhere;} //Prf: 8795


	// Private methods
private:
    void PrepareTipData(TIPDATA *prpTipData);
	CString omWhere; //Prf: 8795
	bool ValidateTipBcData(const long& lrpUnro); //Prf: 8795

};

//---------------------------------------------------------------------------------------------------------

#endif //__CEDATIPDATA__
