// CedaDSPData.cpp
 
#include <stdafx.h>
#include <CedaDSPData.h>
#include <resource.h>


// Local function prototype
static void ProcessDSPCf(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//--CEDADATA-----------------------------------------------------------------------------------------------

CedaDSPData::CedaDSPData() : CCSCedaData(&ogCommHandler)
{
    // Create an array of CEDARECINFO for DSPDATA
	BEGIN_CEDARECINFO(DSPDATA,DSPDataRecInfo)
		FIELD_LONG		(Urno,"URNO")
		FIELD_DATE		(Cdat,"CDAT")
		FIELD_CHAR_TRIM	(Usec,"USEC")
		FIELD_DATE		(Lstu,"LSTU")
		FIELD_CHAR_TRIM	(Useu,"USEU")
		FIELD_CHAR_TRIM	(Dpid,"DPID")
		FIELD_CHAR_TRIM	(Dnop,"DNOP")
		FIELD_CHAR_TRIM	(Dfpn,"DFPN")
		FIELD_CHAR_TRIM	(Demt,"DEMT")
		FIELD_CHAR_TRIM	(Dapn,"DAPN")
		FIELD_CHAR_TRIM	(Dicf,"DICF")
		FIELD_CHAR_TRIM	(Pi01,"PI01")
		FIELD_CHAR_TRIM	(Pn01,"PN01")
		FIELD_CHAR_TRIM	(Pi02,"PI02")
		FIELD_CHAR_TRIM	(Pn02,"PN02")
		FIELD_CHAR_TRIM	(Pi03,"PI03")
		FIELD_CHAR_TRIM	(Pn03,"PN03")
		FIELD_CHAR_TRIM	(Pi04,"PI04")
		FIELD_CHAR_TRIM	(Pn04,"PN04")
		FIELD_CHAR_TRIM	(Pi05,"PI05")
		FIELD_CHAR_TRIM	(Pn05,"PN05")
		FIELD_CHAR_TRIM	(Pi06,"PI06")
		FIELD_CHAR_TRIM	(Pn06,"PN06")
		FIELD_CHAR_TRIM	(Pi07,"PI07")
		FIELD_CHAR_TRIM	(Pn07,"PN07")
		FIELD_CHAR_TRIM	(Pi08,"PI08")
		FIELD_CHAR_TRIM	(Pn08,"PN08")
		FIELD_CHAR_TRIM	(Pi09,"PI09")
		FIELD_CHAR_TRIM	(Pn09,"PN09")
		FIELD_CHAR_TRIM	(Pi10,"PI10")
		FIELD_CHAR_TRIM	(Pn10,"PN10")
		FIELD_CHAR_TRIM	(Pi11,"PI11")
		FIELD_CHAR_TRIM	(Pn11,"PN11")
		FIELD_CHAR_TRIM	(Pi12,"PI12")
		FIELD_CHAR_TRIM	(Pn12,"PN12")
		FIELD_CHAR_TRIM	(Pi13,"PI13")
		FIELD_CHAR_TRIM	(Pn13,"PN13")
		FIELD_CHAR_TRIM	(Pi14,"PI14")
		FIELD_CHAR_TRIM	(Pn14,"PN14")
		FIELD_CHAR_TRIM	(Pi15,"PI15")
		FIELD_CHAR_TRIM	(Pn15,"PN15")
		FIELD_CHAR_TRIM	(Pi16,"PI16")
		FIELD_CHAR_TRIM	(Pn16,"PN16")
		FIELD_CHAR_TRIM	(Pi17,"PI17")
		FIELD_CHAR_TRIM	(Pn17,"PN17")
		FIELD_CHAR_TRIM	(Pi18,"PI18")
		FIELD_CHAR_TRIM	(Pn18,"PN18")
		FIELD_CHAR_TRIM	(Pi19,"PI19")
		FIELD_CHAR_TRIM	(Pn19,"PN19")
		FIELD_CHAR_TRIM	(Pi20,"PI20")
		FIELD_CHAR_TRIM	(Pn20,"PN20")
		FIELD_CHAR_TRIM	(Pi21,"PI21")
		FIELD_CHAR_TRIM	(Pn21,"PN21")
		FIELD_CHAR_TRIM	(Pi22,"PI22")
		FIELD_CHAR_TRIM	(Pn22,"PN22")
		FIELD_CHAR_TRIM	(Pi23,"PI23")
		FIELD_CHAR_TRIM	(Pn23,"PN23")
		FIELD_CHAR_TRIM	(Pi24,"PI24")
		FIELD_CHAR_TRIM	(Pn24,"PN24")
		FIELD_CHAR_TRIM	(Pi25,"PI25")
		FIELD_CHAR_TRIM	(Pn25,"PN25")
		FIELD_CHAR_TRIM	(Pi26,"PI26")
		FIELD_CHAR_TRIM	(Pn26,"PN26")
		FIELD_CHAR_TRIM	(Pi27,"PI27")
		FIELD_CHAR_TRIM	(Pn27,"PN27")
		FIELD_CHAR_TRIM	(Pi28,"PI28")
		FIELD_CHAR_TRIM	(Pn28,"PN28")
		FIELD_CHAR_TRIM	(Pi29,"PI29")
		FIELD_CHAR_TRIM	(Pn29,"PN29")
		FIELD_CHAR_TRIM	(Pi30,"PI30")
		FIELD_CHAR_TRIM	(Pn30,"PN30")
		FIELD_CHAR_TRIM	(Pi31,"PI31")
		FIELD_CHAR_TRIM	(Pn31,"PN31")
		FIELD_CHAR_TRIM	(Pi32,"PI32")
		FIELD_CHAR_TRIM	(Pn32,"PN32")
		FIELD_CHAR_TRIM	(Pi33,"PI33")
		FIELD_CHAR_TRIM	(Pn33,"PN33")
		FIELD_CHAR_TRIM	(Pi34,"PI34")
		FIELD_CHAR_TRIM	(Pn34,"PN34")
		FIELD_CHAR_TRIM	(Pi35,"PI35")
		FIELD_CHAR_TRIM	(Pn35,"PN35")
		FIELD_CHAR_TRIM	(Pi36,"PI36")
		FIELD_CHAR_TRIM	(Pn36,"PN36")
		FIELD_CHAR_TRIM	(Pi37,"PI37")
		FIELD_CHAR_TRIM	(Pn37,"PN37")
		FIELD_CHAR_TRIM	(Pi38,"PI38")
		FIELD_CHAR_TRIM	(Pn38,"PN38")
		FIELD_CHAR_TRIM	(Pi39,"PI39")
		FIELD_CHAR_TRIM	(Pn39,"PN39")
		FIELD_CHAR_TRIM	(Pi40,"PI40")
		FIELD_CHAR_TRIM	(Pn40,"PN40")
		FIELD_CHAR_TRIM	(Pi41,"PI41")
		FIELD_CHAR_TRIM	(Pn41,"PN41")
		FIELD_CHAR_TRIM	(Pi42,"PI42")
		FIELD_CHAR_TRIM	(Pn42,"PN42")
		FIELD_CHAR_TRIM	(Pi43,"PI43")
		FIELD_CHAR_TRIM	(Pn43,"PN43")
		FIELD_CHAR_TRIM	(Pi44,"PI44")
		FIELD_CHAR_TRIM	(Pn44,"PN44")
		FIELD_CHAR_TRIM	(Pi45,"PI45")
		FIELD_CHAR_TRIM	(Pn45,"PN45")
		FIELD_CHAR_TRIM	(Pi46,"PI46")
		FIELD_CHAR_TRIM	(Pn46,"PN46")
		FIELD_CHAR_TRIM	(Pi47,"PI47")
		FIELD_CHAR_TRIM	(Pn47,"PN47")
		FIELD_CHAR_TRIM	(Pi48,"PI48")
		FIELD_CHAR_TRIM	(Pn48,"PN48")
		FIELD_CHAR_TRIM	(Pi49,"PI49")
		FIELD_CHAR_TRIM	(Pn49,"PN49")
		FIELD_CHAR_TRIM	(Pi50,"PI50")
		FIELD_CHAR_TRIM	(Pn50,"PN50")
		FIELD_DATE	    (Vafr,"VAFR")
		FIELD_DATE	    (Vato,"VATO")
	END_CEDARECINFO //(DSPDATA)


	bmRemarkExists = false;	

	// Copy the record structure
	for (int i=0; i< sizeof(DSPDataRecInfo)/sizeof(DSPDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&DSPDataRecInfo[i],sizeof(CEDARECINFO));
		omRecInfo.Add(prpCedaRecInfo);
	} // end for

	// initialize field names
	CString olNames;
	strcpy(pcmTableName,"DSP");
	strcpy(pcmDSPFieldList,"URNO,CDAT,USEC,LSTU,USEU,DPID,DNOP,DFPN,DEMT,DAPN,DICF,");
	for (i = 1; i <= 50; i++)
	{
		olNames.Format("PI%02d,PN%02d,",i,i);
		strcat(pcmDSPFieldList,olNames);
	}
	strcat(pcmDSPFieldList,"VAFR,VATO");
	pcmFieldList = pcmDSPFieldList;
	omData.SetSize(0,1000);
}

void CedaDSPData::AppendFields()
{	
	if(ogBasicData.DoesFieldExist(pcmTableName,"REMA") == true)
	{
		if(strstr(pcmFieldList,"REMA") != NULL)
			return;

		bmRemarkExists = true;

		BEGIN_CEDARECINFO(DSPDATA,DSPDataRecInfo)
			FIELD_CHAR_TRIM (Rema, "REMA")
		END_CEDARECINFO	
		for (int i=0; i< sizeof(DSPDataRecInfo)/sizeof(DSPDataRecInfo[0]) ; i++)
		{
			CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
			memcpy(prpCedaRecInfo,&DSPDataRecInfo[0],sizeof(CEDARECINFO));
			omRecInfo.Add(prpCedaRecInfo);
		}
			
		strcat(pcmDSPFieldList,",REMA");		
		pcmFieldList = pcmDSPFieldList;
	}
}
//---------------------------------------------------------------------------------------------------

void CedaDSPData::GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType)
{
	ropFields.Add("URNO"); ropDesription.Add(LoadStg(IDS_STRING346)); ropType.Add("String");
	ropFields.Add("CDAT"); ropDesription.Add(LoadStg(IDS_STRING343)); ropType.Add("Date");
	ropFields.Add("USEC"); ropDesription.Add(LoadStg(IDS_STRING347)); ropType.Add("String");
	ropFields.Add("LSTU"); ropDesription.Add(LoadStg(IDS_STRING344)); ropType.Add("Date");
	ropFields.Add("USEU"); ropDesription.Add(LoadStg(IDS_STRING348)); ropType.Add("String");
	ropFields.Add("VAFR"); ropDesription.Add(LoadStg(IDS_STRING230)); ropType.Add("Date");
	ropFields.Add("VATO"); ropDesription.Add(LoadStg(IDS_STRING231)); ropType.Add("Date");

	ropFields.Add("DPID"); ropDesription.Add(LoadStg(IDS_STRING940)); ropType.Add("String");
	ropFields.Add("DNOP"); ropDesription.Add(LoadStg(IDS_STRING1071)); ropType.Add("String");
	ropFields.Add("DFPN"); ropDesription.Add(LoadStg(IDS_STRING1072)); ropType.Add("String");
	ropFields.Add("DEMT"); ropDesription.Add(LoadStg(IDS_STRING1073)); ropType.Add("String");
	ropFields.Add("DAPN"); ropDesription.Add(LoadStg(IDS_STRING1074)); ropType.Add("String");
	ropFields.Add("DICF"); ropDesription.Add(LoadStg(IDS_STRING1075)); ropType.Add("String");

	CString olIndex;
	CString olName;
	CString olIndexText;
	CString olNameText;

	for (int i = 1; i <= 50; i++)
	{
		olIndex.Format("PI%02d",i);
		olName.Format("PN%02d",i);

		olIndexText.Format("%s %02d",LoadStg(IDS_STRING1069),i);
		olNameText.Format("%s %02d",LoadStg(IDS_STRING1070),i);

		ropFields.Add(olIndex); ropDesription.Add(olIndexText); ropType.Add("String");
		ropFields.Add(olName); ropDesription.Add(olNameText); ropType.Add("String");
	}

	if(bmRemarkExists == true)
	{
		ropFields.Add("REMA"); ropDesription.Add(LoadStg(IDS_STRING1129)); ropType.Add("String");
	}

}

//-----------------------------------------------------------------------------------------------

void CedaDSPData::Register(void)
{
	ogDdx.Register((void *)this,BC_DSP_CHANGE,CString("DSPDATA"), CString("DSP-changed"),ProcessDSPCf);
	ogDdx.Register((void *)this,BC_DSP_DELETE,CString("DSPDATA"), CString("DSP-deleted"),ProcessDSPCf);
}

//--~CEDADATA----------------------------------------------------------------------------------------------

CedaDSPData::~CedaDSPData(void)
{
	omRecInfo.DeleteAll();
	ClearAll();
}

//--CLEAR-ALL----------------------------------------------------------------------------------------------

void CedaDSPData::ClearAll(void)
{
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	ogDdx.UnRegister(this,NOTUSED);
}

//--READ-ALL-----------------------------------------------------------------------------------------------

bool CedaDSPData::Read(char *pcpWhere)
{
    // Select data from the database
	AppendFields();
	bool ilRc = true;
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(pcpWhere != NULL)
		ilRc = CedaAction(ogBasicData.GetCedaCommand("RT"), pcpWhere);
	else
		ilRc = CedaAction(ogBasicData.GetCedaCommand("RT"));
	if (ilRc != true)
	{
		return false;
	}
	// Load data from CedaData into the dynamic array of record
	for (int ilLc = 0; ilRc == true; ilLc++)
	{
		DSPDATA *prpDSP = new DSPDATA;
		if ((ilRc = GetFirstBufferRecord(prpDSP)) == true)
		{
			prpDSP->IsChanged = DATA_UNCHANGED;
			omData.Add(prpDSP);//Update omData
			omUrnoMap.SetAt((void *)prpDSP->Urno,prpDSP);
		}
		else
		{
			delete prpDSP;
		}
	}
    return true;
}

//--INSERT-------------------------------------------------------------------------------------------------

bool CedaDSPData::InsertDSP(DSPDATA *prpDSP,BOOL bpSendDdx)
{
	prpDSP->IsChanged = DATA_NEW;
	if (SaveDSP(prpDSP) == false) 
		return false; //Update Database
	InsertDSPInternal(prpDSP);
    return true;
}

//--INSERT-INTERNAL-------------------------------------------------------------------------------------------

bool CedaDSPData::InsertDSPInternal(DSPDATA *prpDSP)
{
	//PrepareDSPData(prpDSP);
	ogDdx.DataChanged((void *)this, DSP_CHANGE,(void *)prpDSP ); //Update Viewer
	omData.Add(prpDSP);//Update omData
	omUrnoMap.SetAt((void *)prpDSP->Urno,prpDSP);
    return true;
}

//--DELETE-------------------------------------------------------------------------------------------------

bool CedaDSPData::DeleteDSP(long lpUrno)
{
	DSPDATA *prlDSP = GetDSPByUrno(lpUrno);
	if (prlDSP != NULL)
	{
		prlDSP->IsChanged = DATA_DELETED;
		if(SaveDSP(prlDSP) == false) return false; //Update Database
		DeleteDSPInternal(prlDSP);
	}
    return true;
}

//--DELETE-INTERNAL----------------------------------------------------------------------------------------

bool CedaDSPData::DeleteDSPInternal(DSPDATA *prpDSP)
{
	ogDdx.DataChanged((void *)this,DSP_DELETE,(void *)prpDSP); //Update Viewer
	omUrnoMap.RemoveKey((void *)prpDSP->Urno);
	int ilDSPCount = omData.GetSize();
	for (int ilLc = 0; ilLc < ilDSPCount; ilLc++)
	{
		if (omData[ilLc].Urno == prpDSP->Urno)
		{
			omData.DeleteAt(ilLc);//Update omData
			break;
		}
	}
    return true;
}

//--PREPARE-DATA-------------------------------------------------------------------------------------------

void CedaDSPData::PrepareDSPData(DSPDATA *prpDSP)
{
	// TODO: add code here
}

//--UPDATE-------------------------------------------------------------------------------------------------

bool CedaDSPData::UpdateDSP(DSPDATA *prpDSP,BOOL bpSendDdx)
{
	if (GetDSPByUrno(prpDSP->Urno) != NULL)
	{
		if (prpDSP->IsChanged == DATA_UNCHANGED)
		{
			prpDSP->IsChanged = DATA_CHANGED;
		}
		if(SaveDSP(prpDSP) == false) return false; //Update Database
		UpdateDSPInternal(prpDSP);
	}
    return true;
}

//--UPDATE-INTERNAL--------------------------------------------------------------------------------------------

bool CedaDSPData::UpdateDSPInternal(DSPDATA *prpDSP)
{
	DSPDATA *prlDSP = GetDSPByUrno(prpDSP->Urno);
	if (prlDSP != NULL)
	{
		*prlDSP = *prpDSP; //Update omData
		ogDdx.DataChanged((void *)this,DSP_CHANGE,(void *)prlDSP); //Update Viewer
	}
    return true;
}

//--GET-BY-URNO--------------------------------------------------------------------------------------------

DSPDATA *CedaDSPData::GetDSPByUrno(long lpUrno)
{
	DSPDATA  *prlDSP;
	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlDSP) == TRUE)
	{
		return prlDSP;
	}
	return NULL;
	
}

//--READSPECIALDATA-------------------------------------------------------------------------------------

bool CedaDSPData::ReadSpecialData(CCSPtrArray<DSPDATA> *popAlt,char *pspWhere,char *pspFieldList,bool ipSYS/*=true*/)
{
	bool ilRc = true;
	char pclFieldList[256] = " ";

	if(strlen(pspFieldList) > 0) 
	{
		strcpy(pclFieldList, pspFieldList);
	}
	if(ipSYS == true) 
	{
		if (CedaAction("SYS","DSP",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	else
	{
		if (CedaAction("RT","DSP",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	if(popAlt != NULL)
	{
		for (int ilLc = 0; ilRc == true; ilLc++)
		{
			DSPDATA *prpAlt = new DSPDATA;
			if ((ilRc = GetBufferRecord(ilLc,prpAlt,CString(pclFieldList))) == true)
			{
				popAlt->Add(prpAlt);
			}
			else
			{
				delete prpAlt;
			}
		}
		if(popAlt->GetSize() == 0) return false;
	}
    return true;
}

//--SAVE---------------------------------------------------------------------------------------------------

bool CedaDSPData::SaveDSP(DSPDATA *prpDSP)
{
	bool ilRc = true;
	CString olListOfData;
	char pclSelection[124];
	char pclData[2048];

	if (prpDSP->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}

	switch(prpDSP->IsChanged)
	{
	case DATA_NEW:
		MakeCedaData(&omRecInfo,olListOfData,prpDSP);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("IRT","","",pclData);
		prpDSP->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpDSP->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpDSP);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("URT",pclSelection,"",pclData);
		prpDSP->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_DELETED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpDSP->Urno);
		ilRc = CedaAction("DRT",pclSelection);
		break;
	}

    return ilRc;
}

//--PROCESS-CF---------------------------------------------------------------------------------------------

void  ProcessDSPCf(void * popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	switch(ipDDXType)
	{
	case BC_DSP_CHANGE :
	case BC_DSP_DELETE :
		((CedaDSPData *)popInstance)->ProcessDSPBc(ipDDXType,vpDataPointer,ropInstanceName);
		break;
	}
}

//--PROCESS-BC---------------------------------------------------------------------------------------------

void  CedaDSPData::ProcessDSPBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlDSPData = (struct BcStruct *) vpDataPointer;
	long llUrno = GetUrnoFromSelection(prlDSPData->Selection);

	DSPDATA *prlDSP = GetDSPByUrno(llUrno);
	if(ipDDXType == BC_DSP_CHANGE)
	{
		if(prlDSP != NULL)
		{
			GetRecordFromItemList(prlDSP,prlDSPData->Fields,prlDSPData->Data);
			if(ValidateDSPBcData(prlDSP->Urno)) //Prf: 8795
			{
				UpdateDSPInternal(prlDSP);
			}
			else
			{
				DeleteDSPInternal(prlDSP);
			}
		}
		else
		{
			prlDSP = new DSPDATA;
			GetRecordFromItemList(prlDSP,prlDSPData->Fields,prlDSPData->Data);
			if(ValidateDSPBcData(prlDSP->Urno)) //Prf: 8795
			{
				InsertDSPInternal(prlDSP);
			}
			else
			{
				delete prlDSP;
			}
		}
	}
	if(ipDDXType == BC_DSP_DELETE)
	{
		if (prlDSP != NULL)
		{
			DeleteDSPInternal(prlDSP);
		}
	}
}

//Prf: 8795
//--ValidateDSPBcData--------------------------------------------------------------------------------------

bool CedaDSPData::ValidateDSPBcData(const long& lrpUrno)
{
	bool blValidateDSPBcData = true;
	if(!omWhere.IsEmpty()) //It means it is not the default view and we need to check in the database, since we don't need to check for the default view
	{
		char chWhere[20];
		sprintf(chWhere, "URNO='%ld'",lrpUrno);
		CString olWhere = omWhere + CString(" AND ") + CString(chWhere);
		CCSPtrArray<DSPDATA> olDsps;
		if(!ReadSpecialData(&olDsps,olWhere.GetBuffer(0),"URNO",false))
		{		
			blValidateDSPBcData = false;
		}
	}
	return blValidateDSPBcData;
}

//---------------------------------------------------------------------------------------------------------
