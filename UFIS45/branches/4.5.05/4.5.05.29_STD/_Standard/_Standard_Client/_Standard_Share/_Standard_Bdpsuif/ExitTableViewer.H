#ifndef __EXITTABLEVIEWER_H__
#define __EXITTABLEVIEWER_H__

#include <stdafx.h>
#include <CedaEXTData.h>
#include <CCSTable.h>
#include <CViewer.h>
#include <CCSPrint.h>

struct EXITTABLE_LINEDATA
{
	long	Urno;
	CString	Enam;
	CString	Nafr;
	CString	Nato;
	CString	Resn;
	CString	Term;
	CString	Tele;
	CString	Vafr;
	CString	Vato;
	CString	Home;
};

/////////////////////////////////////////////////////////////////////////////
// ExitTableViewer

class ExitTableViewer : public CViewer
{
// Constructions
public:
    ExitTableViewer(CCSPtrArray<EXTDATA> *popData);
    ~ExitTableViewer();

    void Attach(CCSTable *popAttachWnd);
    virtual void ChangeViewTo(const char *pcpViewName);
// Internal data processing routines
private:
	BOOL IsPassFilter(EXTDATA *prpExit);
	int CompareExit(EXITTABLE_LINEDATA *prpExit1, EXITTABLE_LINEDATA *prpExit2);
    void MakeLines();
	void MakeLine(EXTDATA *prpExit);
	BOOL FindLine(long lpUrno, int &rilLineno);
// Operations
public:
	void DeleteAll();
	void CreateLine(EXITTABLE_LINEDATA *prpExit);
	void DeleteLine(int ipLineno);
// Window refreshing routines
public:
	void UpdateDisplay();
	CString Format(EXITTABLE_LINEDATA *prpLine);
	void ProcessExitChange(EXTDATA *prpExit);
	void ProcessExitDelete(EXTDATA *prpExit);
	BOOL FindExit(char *prpExitKeya, char *prpExitKeyd, int& ilItem);
// Attributes used for filtering condition
private:
	BOOL bmIsFromSearch;
// Attributes
private:
    CCSTable *pomExitTable;
	CCSPtrArray<EXTDATA> *pomData;
// Methods which handle changes (from Data Distributor)
public:
    CCSPtrArray<EXITTABLE_LINEDATA> omLines;
///////
	CCSPtrArray<TABLE_HEADER_COLUMN> omHeaderDataArray;
//Print 
	void PrintTableView();
	bool PrintTableLine(EXITTABLE_LINEDATA *prpLine,bool bpLastLine);
	bool PrintTableHeader();
	CCSPrint *pomPrint;
	//CBitmap omBitmap;
	CString omTableName;

};

#endif //__EXITTABLEVIEWER_H__
