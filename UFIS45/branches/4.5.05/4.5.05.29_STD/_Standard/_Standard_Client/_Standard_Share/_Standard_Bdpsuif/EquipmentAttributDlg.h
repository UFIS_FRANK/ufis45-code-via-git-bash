#if !defined(AFX_EQUIPMENTATTRIBUTDLG_H__3D72D837_098D_11D6_9163_0050DADD7302__INCLUDED_)
#define AFX_EQUIPMENTATTRIBUTDLG_H__3D72D837_098D_11D6_9163_0050DADD7302__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// EquipmentAttributDlg.h : header file
//

#include <CedaEqaData.h>
#include <CCSEdit.h>

/////////////////////////////////////////////////////////////////////////////
// EquipmentAttributDlg dialog

class EquipmentAttributDlg : public CDialog
{
// Construction
public:
	EquipmentAttributDlg(EQADATA *popBlk, CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(EquipmentAttributDlg)
	enum { IDD = IDD_EQUIPMENTATTRIBUTDLG };
	CCSEdit	m_VALU;
	CCSEdit	m_NAME;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(EquipmentAttributDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(EquipmentAttributDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:
	EQADATA *pomEqa;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_EQUIPMENTATTRIBUTDLG_H__3D72D837_098D_11D6_9163_0050DADD7302__INCLUDED_)
