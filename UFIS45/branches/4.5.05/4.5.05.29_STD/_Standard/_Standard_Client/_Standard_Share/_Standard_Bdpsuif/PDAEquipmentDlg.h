#if !defined(AFX_PDAEQUIPMENTDLG_H__32F75442_55BF_11D1_B3C2_0000C016B067__INCLUDED_)
#define AFX_PDAEQUIPMENTDLG_H__32F75442_55BF_11D1_B3C2_0000C016B067__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// PDAEquipmentDlg.h : header file
//
#include <CedaPdaData.h>
#include <CCSEdit.h>
#include <CCSIPEdit.h>

/////////////////////////////////////////////////////////////////////////////
// PDAEquipmentDlg dialog

class PDAEquipmentDlg : public CDialog
{
// Construction
public:
	PDAEquipmentDlg(PDADATA *popPda,CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(PDAEquipmentDlg)
	enum { IDD = IDD_PDA_EQUIPMENT };
	CButton	m_OK;
	CString	m_Caption;
	CCSEdit	m_CDATD;
	CCSEdit	m_CDATT;
	CCSEdit	m_LSTUD;
	CCSEdit	m_LSTUT;
	CCSEdit	m_USEC;
	CCSEdit	m_USEU;
	CCSEdit m_PDID;
	CIPAddressCtrl	m_IPAD;
	CCSEdit	m_VPFRD;
	CCSEdit	m_VPFRT;
	CCSEdit	m_VPTOD;
	CCSEdit	m_VPTOT;
	CCSEdit	m_NAFRT;
	CCSEdit	m_NAFRD;
	CCSEdit	m_NATOT;
	CCSEdit	m_NATOD;
	CComboBox	m_USTF;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(PDAEquipmentDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(PDAEquipmentDlg)
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:

	PDADATA *pomPda;

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PDAEQIUPMENTNDLG_H__32F75442_55BF_11D1_B3C2_0000C016B067__INCLUDED_)
