#if !defined(AFX_AIRPORTARRIVALDESK_H__32F75442_55BF_11D1_B3C2_0000C016B067__INCLUDED_)
#define AFX_AIRPORTARRIVALDESK_H__32F75442_55BF_11D1_B3C2_0000C016B067__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// AirportArrivalDeskDlg.h : header file
//
#include <CedaAadData.h>
#include <CCSEdit.h>

/////////////////////////////////////////////////////////////////////////////
// AirportArrivalDeskDlg dialog

class AirportArrivalDeskDlg : public CDialog
{
// Construction
public:
	AirportArrivalDeskDlg(AADDATA *popAad,CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(AirportArrivalDeskDlg)
	enum { IDD = IDD_AIRPORT_ARRIVAL_DESK };
	CButton	m_OK;
	CString	m_Caption;
	CCSEdit	m_CDATD;
	CCSEdit	m_CDATT;
	CCSEdit	m_LSTUD;
	CCSEdit	m_LSTUT;
	CCSEdit	m_USEC;
	CCSEdit	m_USEU;
	CCSEdit	m_VPFRD;
	CCSEdit	m_VPFRT;
	CCSEdit	m_VPTOD;
	CCSEdit	m_VPTOT;
	CCSEdit	m_NAFRT;
	CCSEdit	m_NAFRD;
	CCSEdit	m_NATOT;
	CCSEdit	m_NATOD;
	CCSEdit	m_DEID;
	CCSEdit	m_MOTI;
	CCSEdit	m_NAME;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(AirportArrivalDeskDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(AirportArrivalDeskDlg)
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:

	AADDATA *pomAad;

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_AIRPORTARRIVALDESK_H__32F75442_55BF_11D1_B3C2_0000C016B067__INCLUDED_)
