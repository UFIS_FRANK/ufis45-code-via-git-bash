// CedaWgnData.h

#ifndef __CEDAWGNDATA__
#define __CEDAWGNDATA__
 
#include <stdafx.h>
#include <basicdata.h>

//---------------------------------------------------------------------------------------------------------

struct WGNDATA 
{
	long 	 Urno;	 	// Eindeutige Datensatz-Nr.
	char	 Algr[20+2];
	//DataCreated by this class
	int      IsChanged;

	//long, CTime
	WGNDATA(void)
	{ 
		memset(this,'\0',sizeof(*this));
	}

}; // end WGNDATA

//---------------------------------------------------------------------------------------------------------
// Class declaratino

class CedaWgnData: public CCSCedaData
{
// Attributes
public:
    CMapPtrToPtr omUrnoMap;

    CCSPtrArray<WGNDATA> omData;

	char pcmListOfFields[2048];

// OWgnations
public:
    CedaWgnData();
	~CedaWgnData();
	virtual void GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType);
	void Register(void);
	void ClearAll(bool bpWithRegistration = true);
    bool Read(char *pspWhere = NULL);
	bool Insert(WGNDATA *prpWgn);
	bool InsertInternal(WGNDATA *prpWgn);
	bool Update(WGNDATA *prpWgn);
	bool UpdateInternal(WGNDATA *prpWgn);
	bool Delete(long lpUrno);
	bool DeleteInternal(WGNDATA *prpWgn);
	bool ReadSpecialData(CCSPtrArray<WGNDATA> *popWgn,char *pspWhere,char *pspFieldList,bool ipSYS=true);
	bool Save(WGNDATA *prpWgn);
	void ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
	WGNDATA  *GetWgnByUrno(long lpUrno);


	// Private methods
private:
    void PrepareWgnData(WGNDATA *prpWgnData);

};

//---------------------------------------------------------------------------------------------------------


#endif //__CEDAWGNDATA__
