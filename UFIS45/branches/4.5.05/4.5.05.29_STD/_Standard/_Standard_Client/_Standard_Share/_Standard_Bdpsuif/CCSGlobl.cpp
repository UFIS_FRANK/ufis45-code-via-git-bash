// CCSGlobl.cpp: implementation of the CCSGlobl .
//
//////////////////////////////////////////////////////////////////////

#include <stdafx.h>
#include <CCSGlobl.h>
#include <CCSCedaData.h>
#include <CCSCedaCom.h>
#include <CCSDdx.h>
#include <CCSLog.h>
#include <CCSBcHandle.h>
#include <BasicData.h>
#include <CCSBasic.h>
#include <CedaBasicData.h>

#include <PrivList.h>

#include <CedaCfgData.h>
#include <CedaWROData.h>
#include <CedaTWYData.h>
#include <CedaSTYData.h>
#include <CedaRWYData.h>
#include <CedaPSTData.h>
#include <CedaNATData.h>
#include <CedaMVTData.h>
#include <CedaHTYData.h>
#include <CedaHaiData.h>
#include <CedaHAGData.h>
#include <CedaGATData.h>
#include <CedaFIDData.h>
#include <CedaEXTData.h>
#include <CedaParData.h>
#include <CedaPolData.h>
#include <CedaValData.h>
#include <CedaDENData.h>
#include <CedaCICData.h>
#include <CedaBLTData.h>
#include <CedaALTData.h>
#include <CedaAPTData.h>
#include <CedaACTData.h>
#include <CedaACRData.h>
#include <CedaSeaData.h>
#include <CedaSphData.h>
#include <CedaStrData.h>
#include <CedaGhsData.h>
#include <CedaGegData.h>
#include <CedaPerData.h>
#include <CedaGrmData.h>
#include <CedaGrnData.h>
#include <CedaOrgData.h>
#include <CedaPfcData.h>
#include <CedaMfmData.h>
#include <CedaCotData.h>
#include <CedaAsfData.h>
#include <CedaBsdData.h>
#include <CedaOdaData.h>
#include <CedaEqtData.h>
#include <CedaEquData.h>
#include <CedaEqaData.h>
#include <CedaTeaData.h>
#include <CedaStfData.h>
#include <CedaWayData.h>
#include <CedaPrcData.h>
#include <CedaSorData.h>
#include <CedaSpfData.h>
#include <CedaSpeData.h>
#include <CedaScoData.h>
#include <CedaSteData.h>
#include <CedaChtData.h>
#include <CedaTipData.h>
#include <CedaBlkData.h>
#include <CedaHolData.h>
#include <CedaWgpData.h>
#include <CedaSwgData.h>
#include <CedaPgpData.h>
#include <CedaPacData.h>
#include <CedaOacData.h>
#include <CedaAwiData.h>
#include <CedaCccData.h>
#include <CedaVipData.h>
#include <CedaAFMData.h>
#include <CedaENTData.h>
#include <CedaSrcData.h>
#include <CedaSgrData.h>
#include <CedaSgmData.h>
#include <CedaStsData.h>
#include <CedaNwhData.h>
#include <CedaPmxData.h>
#include <CedaCohData.h>
#include <CedaWisData.h>
#include <CedaWgnData.h>
#include <CedaMaaData.h>
#include <CedaMawData.h>
#include <CedaSdaData.h>
#include <CedaDEVData.h>
#include <CedaDSPData.h>
#include <CedaPAGData.h>
#include <CedaSreData.h>
#include <CedaDatData.h>
#include <CedaOccData.h>
#include <CedaCrcData.h>  

#include <CedaAadData.h>  
#include <CedaPdaData.h>  
#include <CedaDSCData.h>
#include <CedaDPTData.h>
#include <CedaFLGData.h>
#include <CedaCHUData.h>

/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
// Global Variable Section

const char *pcgAppName = "BDPS-UIF";

char pcgHome[4] = "   "; 
char pcgTableExt[4]= "   "; 
char pcgUMode[6] = "     "; 
CString ogHome;
CString ogTableExt;
CString ogUIFModus;

CString ogAppName = "BDPS-UIF"; // Name of *.exe file of this


CCSLog          ogLog(pcgAppName);
CCSDdx			ogDdx;
CCSCedaCom      ogCommHandler(&ogLog, pcgAppName);  
CCSBcHandle     ogBcHandle(&ogDdx, &ogCommHandler, &ogLog);
CBasicData      ogBasicData;
CCSBasic		ogCCSBasic;

CedaBasicData	ogBCD("", &ogDdx, &ogBcHandle, &ogCommHandler, &ogCCSBasic);


CedaCfgData		ogCfgData;
CedaParData		ogParData;
CedaValData		ogValData;
CedaWROData		ogWROData;
CedaTWYData		ogTWYData;
CedaSTYData		ogSTYData;
CedaRWYData		ogRWYData;
CedaPSTData		ogPSTData;
CedaNATData		ogNATData;
CedaMVTData		ogMVTData;
CedaHTYData		ogHTYData;

CedaHaiData		ogHaiData;

CedaHAGData		ogHAGData;
CedaGATData		ogGATData;
CedaFIDData		ogFIDData;
CedaEXTData		ogEXTData;
CedaPolData		ogPolData;
CedaDENData		ogDENData;
CedaCICData		ogCICData;
CedaBLTData		ogBLTData;
CedaALTData		ogALTData;
CedaAPTData		ogAPTData;
CedaACTData		ogACTData;
CedaACRData		ogACRData;
CedaSeaData		ogSeaData;
CedaAadData		ogAadData;
CedaPdaData		ogPdaData;
CedaSphData		ogSphData;
CedaStrData		ogStrData;
CedaGhsData		ogGhsData;
CedaGegData		ogGegData;
CedaPerData		ogPerData;
CedaGrmData		ogGrmData;
CedaGrnData		ogGrnData;
CedaOrgData		ogOrgData;
CedaPfcData		ogPfcData;
CedaMfmData		ogMfmData;
CedaCotData		ogCotData;
CedaAsfData		ogAsfData;
CedaBsdData		ogBsdData;
CedaOdaData		ogOdaData;
CedaEqtData		ogEqtData;
CedaEquData		ogEquData;
CedaEqaData		ogEqaData;
CedaTeaData		ogTeaData;
CedaStfData		ogStfData;
CedaWayData		ogWayData;
CedaPrcData		ogPrcData;
CedaSorData		ogSorData;
CedaSpfData		ogSpfData;
CedaSpeData		ogSpeData;
CedaScoData		ogScoData;
CedaSteData		ogSteData;
CedaSwgData		ogSwgData;
CedaChtData		ogChtData;
CedaTipData		ogTipData;
CedaBlkData		ogBlkData;
CedaHolData		ogHolData;
CedaWgpData		ogWgpData;
CedaPgpData		ogPgpData;
CedaPacData		ogPacData;
CedaOacData		ogOacData;
CedaAwiData		ogAwiData;
CedaCccData		ogCccData;
CedaVipData		ogVipData;
CedaEntData		ogENTData;
CedaAfmData		ogAFMData;
CedaSgmData		ogSgmData;
CedaSgrData		ogSgrData;
CedaStsData		ogStsData;
CedaNwhData		ogNwhData;
CedaPmxData		ogPmxData;
CedaWisData		ogWisData;
CedaWgnData		ogWgnData;
CedaCohData		ogCohData;
CedaSrcData		ogSrcData;
CedaMaaData		ogMaaData;
CedaMawData		ogMawData;
CedaSdaData		ogSdaData;
CedaDEVData		ogDEVData;
CedaDSPData		ogDSPData;
CedaPAGData		ogPAGData;
CedaSreData		ogSreData;
CedaDatData		ogDatData;
CedaOccData		ogOccData;
CedaCrcData		ogCrcData;			
CedaDSCData     ogDSCData;
CedaDPTData     ogDPTData;
CedaFLGData     ogFLGData;
CedaCHUData     ogCHUData;
				
PrivList		ogPrivList;
PrivList		ogPrmPrivList;
CString			ogListSeparator;



long  igWhatIfUrno;
CString ogWhatIfKey;

bool bgNoScroll;
bool bgOnline = true;
bool bgIsButtonListMovable = false;

bool bgViewEditFilter = false;
bool bgViewEditSort = false;
bool bgUnicode = false;
bool bgUseOffsetForCCA = false;
bool bgModeOfPay=false;
bool bgShowTaxi=false;
bool bgCommonGate=false;

IUFISAmPtr pConnect; // for com-interface

CInitialLoadDlg *pogInitialLoad;

CFont ogSmallFonts_Regular_6;
CFont ogSmallFonts_Regular_7;
CFont ogMSSansSerif_Regular_6;
CFont ogMSSansSerif_Regular_8;
CFont ogMSSansSerif_Regular_12;
CFont ogMSSansSerif_Regular_16;
CFont ogMSSansSerif_Bold_8;
CFont ogMSSansSerif_Bold_12;
CFont ogCourier_Bold_10;
CFont ogCourier_Regular_10;
CFont ogCourier_Regular_8;
CFont ogCourier_Regular_9;

CFont ogScalingFonts[4];
FONT_INDEXES ogStaffIndex;
FONT_INDEXES ogFlightIndex;
FONT_INDEXES ogHwIndex;
FONT_INDEXES ogRotIndex;

BOOL bgIsInitialized = FALSE;

CBrush *ogBrushs[MAXCOLORS+1];
COLORREF ogColors[MAXCOLORS+1];

COLORREF lgBkColor = SILVER;
COLORREF lgTextColor = BLACK;
COLORREF lgHilightColor = WHITE;

CPoint ogMaxTrackSize = CPoint(1024, 768);
CPoint ogMinTrackSize = CPoint(1024 / 4, 768 / 4);

char cgYes = '+';
char cgNo = ' ';
CString ogNoData;
CString ogNotFormat;

CString ogAnsicht;

bool bgDEN_DECS_Exist = false;//AM:20100913 - For MultiDelayCode

CMapStringToString ogConfigMap;
/////////////////////////////////////////////////////////////////////////////

void CreateBrushes()
{
	 int ilLc;

	for( ilLc = 0; ilLc < FIRSTCONFLICTCOLOR; ilLc++)
		ogColors[ilLc] = RED;
	ogColors[2] = GRAY;
	ogColors[3] = GREEN;
	ogColors[4] = RED;
	ogColors[5] = BLUE;
	ogColors[6] = SILVER;
	ogColors[7] = MAROON;
	ogColors[8] = OLIVE;
	ogColors[9] = NAVY;
	ogColors[10] = PURPLE;
	ogColors[11] = TEAL;
	ogColors[12] = LIME;
	ogColors[13] = YELLOW;
	ogColors[14] = FUCHSIA;
	ogColors[15] = AQUA;
	ogColors[16] = WHITE;
	ogColors[17] = BLACK;
	ogColors[18] = ORANGE;


// don't use index 21 it's used for Break jobs already

	for(ilLc = 0; ilLc < MAXCOLORS; ilLc++)
	{
		ogBrushs[ilLc] = new CBrush(ogColors[ilLc]);
	}

	// create a break job brush pattern
	/*
    delete ogBrushs[21];
	CBitmap omBitmap;
	omBitmap.LoadBitmap(IDB_BREAK);
	ogBrushs[21] = new CBrush(&omBitmap);
    */
}

void DeleteBrushes()
{
	for( int ilLc = 0; ilLc < MAXCOLORS; ilLc++)
	{
		CBrush *olBrush = ogBrushs[ilLc];
		delete olBrush;
	}
}


void InitFont() 
{
    CDC dc;
    dc.CreateCompatibleDC(NULL);

    LOGFONT logFont;
    memset(&logFont, 0, sizeof(LOGFONT));

	logFont.lfCharSet= DEFAULT_CHARSET;
    logFont.lfHeight = - MulDiv(6, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_NORMAL;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_SWISS;
    lstrcpy(logFont.lfFaceName, "Small Fonts");
    ogSmallFonts_Regular_6.CreateFontIndirect(&logFont);
        
	logFont.lfCharSet= DEFAULT_CHARSET;
    logFont.lfHeight = - MulDiv(7, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_NORMAL;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_SWISS;
    lstrcpy(logFont.lfFaceName, "Small Fonts");
    ogSmallFonts_Regular_7.CreateFontIndirect(&logFont);

	logFont.lfCharSet= DEFAULT_CHARSET;
    logFont.lfHeight = - MulDiv(10, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_BOLD;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_MODERN;
    lstrcpy(logFont.lfFaceName, "Courier New");
    ogCourier_Bold_10.CreateFontIndirect(&logFont);

	logFont.lfCharSet= DEFAULT_CHARSET;
    logFont.lfHeight = - MulDiv(10, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_NORMAL;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_MODERN;
    lstrcpy(logFont.lfFaceName, "Courier New");
    ogCourier_Regular_10.CreateFontIndirect(&logFont);

	logFont.lfCharSet= DEFAULT_CHARSET;
    logFont.lfHeight = - MulDiv(8, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_NORMAL;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_MODERN;
    lstrcpy(logFont.lfFaceName, "Courier New");
    ogCourier_Regular_8.CreateFontIndirect(&logFont);

	logFont.lfCharSet= DEFAULT_CHARSET;
    logFont.lfHeight = - MulDiv(9, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_NORMAL;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_MODERN;
    lstrcpy(logFont.lfFaceName, "Courier New");
    ogCourier_Regular_9.CreateFontIndirect(&logFont);

	logFont.lfCharSet= DEFAULT_CHARSET;
    logFont.lfHeight = - MulDiv(6, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_NORMAL;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_SWISS;
    lstrcpy(logFont.lfFaceName, "Small Fonts"); /*"MS Sans Serif""Arial"*/
    ogScalingFonts[MS_SANS6].CreateFontIndirect(&logFont);

	logFont.lfCharSet= DEFAULT_CHARSET;
    logFont.lfHeight = - MulDiv(8, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_NORMAL;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_SWISS;
    lstrcpy(logFont.lfFaceName, "MS Sans Serif");
    ogScalingFonts[MS_SANS8].CreateFontIndirect(&logFont);

	logFont.lfCharSet= DEFAULT_CHARSET;
    logFont.lfHeight = - MulDiv(12, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_BOLD;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_SWISS;
    lstrcpy(logFont.lfFaceName, "MS Sans Serif");
    ogScalingFonts[MS_SANS12].CreateFontIndirect(&logFont);

	// logFont.lfHeight = - MulDiv(16, dc.GetDeviceCaps(LOGPIXELSY), 72);
	logFont.lfCharSet= DEFAULT_CHARSET;
	logFont.lfHeight = - MulDiv(8, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_BOLD;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_SWISS;
    lstrcpy(logFont.lfFaceName, "MS Sans Serif");
    ogScalingFonts[MS_SANS16].CreateFontIndirect(&logFont);
 
	ogStaffIndex.VerticalScale = MS_SANS8;
	ogStaffIndex.Chart = MS_SANS6;
	ogFlightIndex.VerticalScale = MS_SANS8;
	ogFlightIndex.Chart = MS_SANS6;
	ogHwIndex.VerticalScale = MS_SANS8;
	ogHwIndex.Chart = MS_SANS6;

	logFont.lfCharSet= DEFAULT_CHARSET;
    logFont.lfHeight = - MulDiv(8, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_BOLD;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_SWISS;
    lstrcpy(logFont.lfFaceName, "MS Sans Serif");
    ogMSSansSerif_Bold_8.CreateFontIndirect(&logFont);

	logFont.lfCharSet= DEFAULT_CHARSET;
    logFont.lfHeight = - MulDiv(12, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_BOLD;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_SWISS;
    lstrcpy(logFont.lfFaceName, "MS Sans Serif");
    ogMSSansSerif_Bold_12.CreateFontIndirect(&logFont);

    dc.DeleteDC();
}

