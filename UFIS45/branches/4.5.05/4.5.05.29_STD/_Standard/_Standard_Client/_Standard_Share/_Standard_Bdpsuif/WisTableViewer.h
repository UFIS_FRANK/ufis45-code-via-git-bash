#ifndef __WISTABLEVIEWER_H__
#define __WISTABLEVIEWER_H__

#include <stdafx.h>
#include <CedaWisData.h>
#include <CCSTable.h>
#include <CViewer.h>
#include <CCSPrint.h>


struct WISTABLE_LINEDATA
{
	long 	Urno;
	CString	Wisc;
	CString Wisd;
	CString	Rema;
	CString Shir;
	CString	Orgc;
	CTime	Cdat;
	CTime	Lstu;
	CString	Usec;
	CString	Useu;
};

/////////////////////////////////////////////////////////////////////////////
// WisTableViewer

class WisTableViewer : public CViewer
{
// Constructions
public:
    WisTableViewer(CCSPtrArray<WISDATA> *popData);
    ~WisTableViewer();

    void Attach(CCSTable *popAttachWnd);
    virtual void ChangeViewTo(const char *pcpViewName);
// Internal data processing routines
private:
	bool IsPassFilter(WISDATA *prpWis);
	int CompareWis(WISTABLE_LINEDATA *prpWis1, WISTABLE_LINEDATA *prpWis2);
    void MakeLines();
	void MakeLine(WISDATA *prpWis);
	bool FindLine(long lpUrno, int &rilLineno);
// Operations
public:
	void DeleteAll();
	void CreateLine(WISTABLE_LINEDATA *prpWis);
	void DeleteLine(int ipLineno);
// Window refreshing routines
public:
	void UpdateDisplay();
	CString Format(WISTABLE_LINEDATA *prpLine);
	void ProcessWisChange(WISDATA *prpWis);
	void ProcessWisDelete(WISDATA *prpWis);
	bool FindWis(char *prpWisKeya, char *prpWisKeyd, int& ilItem);
// Attributes used for filtering condition
private:
	bool bmIsFromSearch;
// Attributes
private:
    CCSTable *pomWisTable;
	CCSPtrArray<WISDATA> *pomData;
// Methods which handle changes (from Data Distributor)
public:
    CCSPtrArray<WISTABLE_LINEDATA> omLines;
///////
	CCSPtrArray<TABLE_HEADER_COLUMN> omHeaderDataArray;
//Print 
	void PrintTableView();
	bool PrintTableLine(WISTABLE_LINEDATA *prpLine,bool bpLastLine);
	bool PrintTableHeader();
	CCSPrint *pomPrint;
	//CBitmap omBitmap;
	CString omTableName;

};

#endif //__WISTABLEVIEWER_H__
