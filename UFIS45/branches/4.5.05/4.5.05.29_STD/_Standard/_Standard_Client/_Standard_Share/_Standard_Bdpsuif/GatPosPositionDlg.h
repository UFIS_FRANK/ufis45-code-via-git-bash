#if !defined(AFX_GATPOSPOSITIONDLG_H__5076FC66_B8AD_11D6_8214_00010215BFDE__INCLUDED_)
#define AFX_GATPOSPOSITIONDLG_H__5076FC66_B8AD_11D6_8214_00010215BFDE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// GatPosPositionDlg.h : header file
//
#include <PositionDlg.h>
#include <AatBitmapComboBox.h>

/////////////////////////////////////////////////////////////////////////////
// GatPosPositionDlg dialog

class GatPosPositionDlg : public PositionDlg
{
// Construction
public:
	GatPosPositionDlg(PSTDATA *popPST,CWnd* pParent = NULL);   // standard constructor
	GatPosPositionDlg(PSTDATA *popPST,int ipDlg,CWnd* pParent = NULL);   // standard constructor
	int GetConnectedGates(CStringArray& ropGates);

// Dialog Data
	//{{AFX_DATA(GatPosPositionDlg)
	enum { IDD = IDD_GATPOS_POSITIONDLG };
	AatBitmapComboBox	m_BlockingBitmap;
	CListBox		m_ConnectedGates;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(GatPosPositionDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(GatPosPositionDlg)
	afx_msg void OnGates();
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	afx_msg LONG OnTableLButtonDblclk(UINT wParam, LONG lParam);
	afx_msg void OnNoavNew();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	CStringArray	omAvailableGates;
	CStringArray	omConnectedGates;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_GATPOSPOSITIONDLG_H__5076FC66_B8AD_11D6_8214_00010215BFDE__INCLUDED_)
