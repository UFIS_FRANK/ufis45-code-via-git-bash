// FidsDisplayScheduleDlg.cpp : implementation file
//

#include <stdafx.h>
#include <BDPSUIF.h>
#include <FidsDisplayScheduleDlg.h>
#include <PrivList.h>
#include <AwDlg.h>
#include <CedaPAGData.h>
#include <CedaDEVData.h>
#include <CedaDSPData.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

const int DISPLAY_PROTOTYPE_COLUMNS = 10;

/////////////////////////////////////////////////////////////////////////////
// FidsDisplayScheduleDlg dialog


FidsDisplayScheduleDlg::FidsDisplayScheduleDlg(DSCDATA *popDSC,const CString& ropCaption,CWnd* pParent /*=NULL*/)
	: CDialog(FidsDisplayScheduleDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(FidsDisplayScheduleDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	pomDSC	  = popDSC;
	m_Caption = ropCaption;
	pomTable = new CCSTable;
	omTobeDeletedDptUrnosArray.RemoveAll();
	omDisplayPrototypeLines.RemoveAll();
}

FidsDisplayScheduleDlg::~FidsDisplayScheduleDlg()
{
	omTobeDeletedDptUrnosArray.RemoveAll();
	omDisplayPrototypeLines.RemoveAll();
	ogPAGData.ClearAll();
	delete pomTable;
}

void FidsDisplayScheduleDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(FidsDisplayScheduleDlg)
	DDX_Control(pDX, IDC_EDIT_NAME, m_DEVN);
	DDX_Control(pDX, IDC_INSERT_DPT, m_DPTNEW);
	DDX_Control(pDX, IDC_DELETE_DPT, m_DPTDEL);
	DDX_Control(pDX, IDC_DPTTABLE,	  m_DPTTABLEFRAME);
	DDX_Control(pDX, IDC_VAFRD, m_VAFRD);
	DDX_Control(pDX, IDC_VATOD, m_VATOD);
	DDX_Control(pDX, IDC_CDAT_D, m_CDATD);
	DDX_Control(pDX, IDC_CDAT_T, m_CDATT);
	DDX_Control(pDX, IDC_LSTU_D, m_LSTUD);
	DDX_Control(pDX, IDC_LSTU_T, m_LSTUT);
	DDX_Control(pDX, IDC_USEU,	 m_USEU);
	DDX_Control(pDX, IDC_USEC,	 m_USEC);	
	DDX_Control(pDX, IDCANCEL, m_CANCEL);
	DDX_Control(pDX, IDOK, m_OK);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(FidsDisplayScheduleDlg, CDialog)
	//{{AFX_MSG_MAP(FidsDisplayScheduleDlg)
	ON_MESSAGE(WM_TABLE_LBUTTONDBLCLK,	OnTableLButtonDblclk)
	ON_BN_CLICKED(IDC_INSERT_DPT, OnDPTNew)
	ON_BN_CLICKED(IDC_DELETE_DPT, OnDPTDel)
	ON_BN_CLICKED(IDC_BUTTON_DEVICE, OnButtonDeviceClicked)	
	ON_MESSAGE(WM_TABLE_IPEDIT, OnTableIPEdit)
	ON_MESSAGE(WM_EDIT_TAB_BUTTONDOWN, OnTableIPEditButtonDown)
	ON_MESSAGE(WM_EDIT_SHIFTTAB_BUTTONDOWN, OnTableIPEditShiftButtonDown)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// FidsDisplayScheduleDlg message handlers

BOOL FidsDisplayScheduleDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	m_Caption = LoadStg(IDS_STRING930) + " <" + m_Caption + ">";
	SetWindowText(m_Caption);
	char clStat = ogPrivList.GetStat("DISPLAYDLG.m_OK");
	SetWndStatAll(clStat,m_OK);
	
	//------------------------------------	
	m_DEVN.SetFormat("X(20)");
	m_DEVN.SetTextLimit(1,20);
	m_DEVN.SetBKColor(YELLOW);		
	m_DEVN.SetTextErrColor(RED);
	m_DEVN.SetInitText(pomDSC->Devn);
	m_DEVN.SetSecState(ogPrivList.GetStat("DISPLAYSCHDLG.m_DEVN"));
	//------------------------------------
	m_CDATD.SetBKColor(SILVER);			
	m_CDATD.SetInitText(pomDSC->Cdat.Format("%d.%m.%Y"));
	m_CDATD.SetSecState(ogPrivList.GetStat("DISPLAYSCHDLG.m_CDAT"));
	m_CDATT.SetBKColor(SILVER);
	m_CDATT.SetInitText(pomDSC->Cdat.Format("%H:%M"));
	m_CDATT.SetSecState(ogPrivList.GetStat("DISPLAYSCHDLG.m_CDAT"));
	//------------------------------------
	m_LSTUD.SetBKColor(SILVER);
	m_LSTUD.SetInitText(pomDSC->Lstu.Format("%d.%m.%Y"));
	m_LSTUD.SetSecState(ogPrivList.GetStat("DISPLAYSCHDLG.m_LSTU"));
	m_LSTUT.SetBKColor(SILVER);
	m_LSTUT.SetInitText(pomDSC->Lstu.Format("%H:%M"));
	m_LSTUT.SetSecState(ogPrivList.GetStat("DISPLAYSCHDLG.m_LSTU"));
	//------------------------------------
	m_USEC.SetBKColor(SILVER);
	m_USEC.SetInitText(pomDSC->Usec);
	m_USEC.SetSecState(ogPrivList.GetStat("DISPLAYSCHDLG.m_USEC"));
	//------------------------------------
	m_USEU.SetBKColor(SILVER);
	m_USEU.SetInitText(pomDSC->Useu);
	m_USEU.SetSecState(ogPrivList.GetStat("DISPLAYSCHDLG.m_USEU"));
	//------------------------------------
	m_VAFRD.SetTypeToDate(true);
	m_VAFRD.SetTextErrColor(RED);
	m_VAFRD.SetBKColor(YELLOW);
	m_VAFRD.SetInitText(PrepareDisplayDateFormat(pomDSC->Vafr,true));
	m_VAFRD.SetSecState(ogPrivList.GetStat("DISPLAYSCHDLG.m_VAFR"));
	//------------------------------------
	m_VATOD.SetTypeToDate(true);
	m_VATOD.SetTextErrColor(RED);	
	m_VATOD.SetBKColor(YELLOW);
	m_VATOD.SetInitText(PrepareDisplayDateFormat(pomDSC->Vato));
	m_VATOD.SetSecState(ogPrivList.GetStat("DISPLAYSCHDLG.m_VATO"));
	//------------------------------------	


	MakeDisplayPrototypeTable();
	PopulateDisplayPrototypeTable();	

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void FidsDisplayScheduleDlg::OnOK() 
{
	// TODO: Add extra validation here
	CWaitCursor olWait;
	CString olErrorText;
	bool ilStatus = true;
	
	if(m_DEVN.GetStatus() == false)
	{
		ilStatus = false;
		if(m_DEVN.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING935) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING935) + ogNotFormat;
		} 
	}

	if(m_VAFRD.GetStatus() == false)
	{
		ilStatus = false;
		if(m_VAFRD.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING232) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING232) + ogNotFormat;
		}
	}
	if(m_VATOD.GetStatus() == false)
	{
		ilStatus = false;
		if(m_VATOD.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING231) + LoadStg(IDS_STRING232) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING231) + LoadStg(IDS_STRING232) + ogNotFormat;
		}
	}

	/////////////////////////////////////////////////////////////////////////
	CString olVafrd,olVafrt,olVatod,olVatot;
	CTime olTmpTimeFr,olTmpTimeTo;
	m_VAFRD.GetWindowText(olVafrd);	
	m_VATOD.GetWindowText(olVatod);

	/*if(m_VAFRD.GetWindowTextLength() == 0)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING1009);
	}
	if(m_VATOD.GetWindowTextLength() == 0)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING1010);
	}*/
	if(m_VAFRD.GetStatus() == true && m_VATOD.GetStatus() == true)
	{		
		olTmpTimeFr = DateHourStringToDate(olVafrd,CString("00:00"));
		olTmpTimeTo = DateHourStringToDate(olVatod,CString("00:00"));
		if(olTmpTimeTo != -1 && (olTmpTimeTo < olTmpTimeFr))
		{
			ilStatus = false;
			olErrorText += LoadStg(IDS_STRING236);
		}
		if(olTmpTimeFr == -1)
		{
			ilStatus = false;
			olErrorText += LoadStg(IDS_STRING1008);			
		}
		if(olTmpTimeTo == -1)
		{			
			ilStatus = false;
			olErrorText += LoadStg(IDS_STRING1009);
		}
	}

	//Konsistenz-Check//
	if(ilStatus == true)
	{
		CString olDevn;
		char clWhere[100];
		CCSPtrArray<DEVDATA> olDEVCPA;
		CCSPtrArray<DSCDATA> olDSCCPA;
		m_DEVN.GetWindowText(olDevn);
		if (olDevn.GetLength() > 0)
		{
			sprintf(clWhere,"WHERE DEVN='%s'",olDevn);
			if(ogDSCData.ReadSpecialData(&olDSCCPA,clWhere,"URNO,DEVN",false) == true)
			{
				if(olDSCCPA[0].Urno != pomDSC->Urno)
				{
					ilStatus = false;
					olErrorText += LoadStg(IDS_STRING935) + LoadStg(IDS_EXIST);
				}
			}
			olDSCCPA.DeleteAll();
			if(ogDEVData.ReadSpecialData(&olDEVCPA,clWhere,"URNO,DEVN",false) == false)
			{
				ilStatus = false;
				olErrorText += LoadStg(IDS_STRING1004);
			}
			olDEVCPA.DeleteAll();
		}
	}

	AfxGetApp()->DoWaitCursor(-1);
	CString olErrorMessage;
	if(ValidateDPTData(olErrorMessage) == false)
	{
		olErrorText += olErrorMessage;
		ilStatus = false;
	}

	if (ilStatus == true)
	{
		m_DEVN.GetWindowText(pomDSC->Devn,sizeof(pomDSC->Devn));
		strcpy(pomDSC->Vafr,DateHourStringToDate(olVafrd,CString("00:00")).Format("%Y%m%d")); 
		strcpy(pomDSC->Vato,DateHourStringToDate(olVatod,CString("00:00")).Format("%Y%m%d"));
		CDialog::OnOK();
	}
	else
	{
		Beep(440,70);
		MessageBox(olErrorText,LoadStg(IDS_STRING145),MB_ICONERROR);
		m_DEVN.SetFocus();
		m_DEVN.SetSel(0,-1);
	}

}

void FidsDisplayScheduleDlg::MakeDisplayPrototypeTable()
{
	CRect olRectBorder;
	m_DPTTABLEFRAME.GetWindowRect( olRectBorder );
	ScreenToClient(olRectBorder);
	olRectBorder.DeflateRect(2,2);

	pomTable->SetTableEditable(true);
    pomTable->SetTableData(this, olRectBorder.left, olRectBorder.right, olRectBorder.top, olRectBorder.bottom);
	pomTable->SetSelectMode(0);
	pomTable->SetShowSelection(TRUE);
	pomTable->SetIPEditModus(TRUE);
	pomTable->ResetContent();

	CCSPtrArray <TABLE_HEADER_COLUMN> olHeaderDataArray;
	TABLE_HEADER_COLUMN rlHeader;
	rlHeader.Alignment = COLALIGN_LEFT;
	rlHeader.Font = &ogCourier_Regular_10;
	int ilPos = 1;
	rlHeader.Length = 8; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING1003),ilPos++,true,'|');
	olHeaderDataArray.NewAt(olHeaderDataArray.GetSize(), rlHeader);

	rlHeader.Length = 58; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING1003),ilPos++,true,'|');
	olHeaderDataArray.NewAt(olHeaderDataArray.GetSize(), rlHeader);

	rlHeader.Length = 40; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING1003),ilPos++,true,'|');
	olHeaderDataArray.NewAt(olHeaderDataArray.GetSize(), rlHeader);

	rlHeader.Length = 40; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING1003),ilPos++,true,'|');
	olHeaderDataArray.NewAt(olHeaderDataArray.GetSize(), rlHeader);

	rlHeader.Length = 100; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING1003),ilPos++,true,'|');
	olHeaderDataArray.NewAt(olHeaderDataArray.GetSize(), rlHeader);

	rlHeader.Length = 50; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING1003),ilPos++,true,'|');
	olHeaderDataArray.NewAt(olHeaderDataArray.GetSize(), rlHeader);

	rlHeader.Length = 100; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING1003),ilPos++,true,'|');
	olHeaderDataArray.NewAt(olHeaderDataArray.GetSize(), rlHeader);

	rlHeader.Length = 50; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING1003),ilPos++,true,'|');
	olHeaderDataArray.NewAt(olHeaderDataArray.GetSize(), rlHeader);

	rlHeader.Length = 80; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING1003),ilPos++,true,'|');
	olHeaderDataArray.NewAt(olHeaderDataArray.GetSize(), rlHeader);

	rlHeader.Length = 100; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING1003),ilPos++,true,'|');
	olHeaderDataArray.NewAt(olHeaderDataArray.GetSize(), rlHeader);

	pomTable->SetHeaderFields(olHeaderDataArray);
	pomTable->SetDefaultSeparator();
	olHeaderDataArray.DeleteAll();

	int ilLines = 2;
	if(pomDSC->Urno != 0)
	{		
		char pclWhere[100];
		sprintf(pclWhere, "WHERE RURN = '%ld' ORDER BY DEFL DESC", pomDSC->Urno);
		if(ogDPTData.ReadSpecialData(&omDPTDataArray,pclWhere,ogDPTData.pcmDPTFieldList,false) == true)
		{
			ilLines = omDPTDataArray.GetSize();
		}
	}

	pomTable->SetDefaultSeparator();
	pomTable->SetTableEditable(TRUE);

	TABLE_COLUMN rlColumnData;
	rlColumnData.Font = &ogCourier_Regular_10;
	rlColumnData.BkColor = WHITE;
	rlColumnData.Alignment = COLALIGN_LEFT;

	CCSEDIT_ATTRIB rlAttrib[DISPLAY_PROTOTYPE_COLUMNS];
	CreateEditAttributes(rlAttrib);
		
    for (int ilLineNo = 0; ilLineNo < ilLines; ilLineNo++)
	{
		CCSPtrArray<TABLE_COLUMN> olColList;

		for(int jlColumnNo = 0; jlColumnNo < DISPLAY_PROTOTYPE_COLUMNS; jlColumnNo++)
		{
			rlColumnData.EditAttrib = rlAttrib[jlColumnNo];
			rlColumnData.BkColor = RGB(255,255,255);
			if((jlColumnNo == 1 || jlColumnNo == 2 || jlColumnNo == 3) && ilLineNo != 0)
			{
				rlColumnData.BkColor = YELLOW;
			}
			rlColumnData.Columnno = jlColumnNo;
			pomTable->GetTextFieldValue(ilLineNo, jlColumnNo, rlColumnData.Text);
			olColList.NewAt(olColList.GetSize(), rlColumnData);			
		}
		pomTable->AddTextLine(olColList,(void*)NULL);
		olColList.DeleteAll();
		DisplayPrototypeLineData olDisplayPrototypeLineData;
		omDisplayPrototypeLines.NewAt(omDisplayPrototypeLines.GetSize(),olDisplayPrototypeLineData);
	}

	for(int ilColumnNo = 0; ilColumnNo < DISPLAY_PROTOTYPE_COLUMNS; ilColumnNo++)
	{
		pomTable->SetColumnEditable(ilColumnNo,true);
	}
	pomTable->DisplayTable();
	
	for (ilLineNo = 0; ilLineNo < ilLines; ilLineNo++)
	{
		if(ilLineNo == 0)
		{
			pomTable->omLines[0].Columns[0].Text = "D";
			for(ilColumnNo = 0; ilColumnNo <= 3; ilColumnNo++)
			{			
				pomTable->SetColumnEditable(0,ilColumnNo,false);
			}
		}
		else
		{
			pomTable->omLines[ilLineNo].Columns[0].Text = "";
			pomTable->SetColumnEditable(ilLineNo,0,false);
		}
	}
}

void FidsDisplayScheduleDlg::PopulateDisplayPrototypeTable()
{
	TABLE_COLUMN rlColumnData;
	rlColumnData.Font = &ogCourier_Regular_10;
	rlColumnData.BkColor = WHITE;
	rlColumnData.Alignment = COLALIGN_LEFT;
	
	if(omDPTDataArray.GetSize() > 0)
	{
		omDisplayPrototypeLines.RemoveAll();
	}
	DPTDATA* polDptData;
    for (int ilLineNo = 0; ilLineNo < omDPTDataArray.GetSize(); ilLineNo++)
	{	
		polDptData = &omDPTDataArray.GetAt(ilLineNo);

		for(int jlColumnNo = 0; jlColumnNo < DISPLAY_PROTOTYPE_COLUMNS; jlColumnNo++)
		{
			switch(jlColumnNo)
			{
			case 0:
				pomTable->omLines[ilLineNo].Columns[0].Text = polDptData->Defl;
				break;
			case 1:
				pomTable->omLines[ilLineNo].Columns[1].Text = polDptData->Doop;
				break;
			case 2:
				if(strlen(polDptData->Dbgn) > 0)
				{
					pomTable->omLines[ilLineNo].Columns[2].Text = CString(polDptData->Dbgn).Mid(0,2) + ":" + CString(polDptData->Dbgn).Mid(2,2);
				}
				else
				{
					pomTable->omLines[ilLineNo].Columns[2].Text = polDptData->Dbgn;
				}
				break;
			case 3:
				if(strlen(polDptData->Dend) > 0)
				{
					pomTable->omLines[ilLineNo].Columns[3].Text = CString(polDptData->Dend).Mid(0,2) + ":" + CString(polDptData->Dend).Mid(2,2);
				}
				else
				{
					pomTable->omLines[ilLineNo].Columns[3].Text = polDptData->Dend;
				}
				break;
			case 4:
				pomTable->omLines[ilLineNo].Columns[4].Text = polDptData->Dpid;
				break;
			case 5:
				pomTable->omLines[ilLineNo].Columns[5].Text = polDptData->Dffd;
				break;
			case 6:
				pomTable->omLines[ilLineNo].Columns[6].Text = polDptData->Dfco;
				break;
			case 7:
				pomTable->omLines[ilLineNo].Columns[7].Text = polDptData->Dter;
				break;
			case 8:
				pomTable->omLines[ilLineNo].Columns[8].Text = polDptData->Dare;
				break;
			case 9:
				pomTable->omLines[ilLineNo].Columns[9].Text = polDptData->Algc;
				break;
			default:
				break;
				
			}
		}	
		DisplayPrototypeLineData olDisplayPrototypeLineData;
		PopulateDisplayPrototypeLineData(olDisplayPrototypeLineData,*polDptData);
		omDisplayPrototypeLines.NewAt(omDisplayPrototypeLines.GetSize(),olDisplayPrototypeLineData);		
	}
	pomTable->DisplayTable();
}

//----------------------------------------------------------------------------------------

void FidsDisplayScheduleDlg::InsertOrDelete(enumOperation InsertOrDel,int ipLineNo)
{
	if(InsertOrDel == OP_INSERT)
	{
		int ilLineCount = pomTable->GetLinesCount();
		TABLE_COLUMN rlColumn;
		CCSPtrArray <TABLE_COLUMN> olLine;

		rlColumn.Lineno = 0;
		rlColumn.VerticalSeparator = SEPA_NONE;
		rlColumn.SeparatorType = SEPA_NONE;
		rlColumn.BkColor = WHITE;
		rlColumn.Font = &ogCourier_Regular_10;
		rlColumn.Alignment = COLALIGN_LEFT;

		CCSEDIT_ATTRIB rlAttrib[DISPLAY_PROTOTYPE_COLUMNS];
		CreateEditAttributes(rlAttrib);
		
		CCSPtrArray<TABLE_COLUMN> olColList;

		for(int ilColumnNo = 0; ilColumnNo < DISPLAY_PROTOTYPE_COLUMNS; ilColumnNo++)
		{
			rlColumn.BkColor = WHITE;
			if((ilColumnNo == 1 || ilColumnNo == 2 || ilColumnNo == 3) && ilLineCount != 0)
			{
				rlColumn.BkColor = YELLOW;
			}
			rlColumn.Columnno = ilColumnNo;
			rlColumn.EditAttrib = rlAttrib[ilColumnNo];
			olColList.NewAt(olColList.GetSize(), rlColumn);			
		}
		pomTable->AddTextLine(olColList,(void*)NULL);
		olColList.DeleteAll();
		pomTable->DisplayTable();
		DisplayPrototypeLineData olDisplayPrototypeLineData;		
		omDisplayPrototypeLines.NewAt(omDisplayPrototypeLines.GetSize(),olDisplayPrototypeLineData);
		if(ilLineCount == 0)
		{
			for(ilColumnNo = 0; ilColumnNo <= 3; ilColumnNo++)
			{		
				pomTable->SetColumnEditable(0,ilColumnNo,false);
			}
			pomTable->omLines[0].Columns[0].Text = "D";
		}
		else
		{
			pomTable->SetColumnEditable(pomTable->GetLinesCount() -1,0,false);
			pomTable->omLines[pomTable->GetLinesCount()-1].Columns[0].Text = "";
		}		
	}
	else if(InsertOrDel == OP_DELETE && ipLineNo > 0)
	{
		if(pomTable->GetLinesCount() == 2)
		{
			MessageBox(LoadStg(IDS_STRING1007),LoadStg(IDS_STRING149),MB_ICONSTOP);
		}
		else
		{
			if(omDisplayPrototypeLines[ipLineNo].Urno != 0)
			{			
				omTobeDeletedDptUrnosArray.AddHead(omDisplayPrototypeLines[ipLineNo].Urno);
			}
			omDisplayPrototypeLines.DeleteAt(ipLineNo);		
			pomTable->DeleteTextLine(ipLineNo);
			pomTable->DisplayTable();		
		}
	}
}

//----------------------------------------------------------------------------------------

LONG FidsDisplayScheduleDlg::OnTableLButtonDblclk(UINT wParam, LONG lParam) 
{	
	int ilLineNo = pomTable->GetCurSel();
	
	CPoint olPoint;
	GetCursorPos(&olPoint);
	pomTable->pomListBox->ScreenToClient(&olPoint);

	if (ilLineNo == -1)
	{
		MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONSTOP);
	}
	else if (ilLineNo >= pomTable->omLines.GetSize())
	{
		MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONSTOP);
	}
	else if(pomTable->pomListBox->GetColumnFromPoint(olPoint) == 4)
	{
		CStringArray olCol1, olCol2,olCol3;
		CCSPtrArray<DSPDATA> olList;

		AfxGetApp()->DoWaitCursor(1);
		if(ogDSPData.ReadSpecialData(&olList, "", "URNO,DPID", false) == true)
		{
			AfxGetApp()->DoWaitCursor(-1);
			for(int i = 0; i < olList.GetSize(); i++)
			{
				char pclUrno[20]="";
				sprintf(pclUrno, "%ld", olList[i].Urno);
				olCol3.Add(CString(pclUrno));
				olCol2.Add(CString(olList[i].Dpid));
			}

			AwDlg *pomDlg = new AwDlg(NULL,&olCol2,&olCol3,"",LoadStg(IDS_STRING1135),this);
			if(pomDlg->DoModal() == IDOK)
			{					
				int ilCurrentLineNo = pomTable->GetCurrentLine();					
				TABLE_COLUMN *prlColumn = pomTable->FindColumn(ilCurrentLineNo, 4);
				prlColumn->Text = pomDlg->omReturnString;
				pomTable->ChangeColumnData(ilCurrentLineNo,4,prlColumn);
				pomTable->omLines[pomTable->GetCurrentLine()].Columns[4].Text = pomDlg->omReturnString;
			}
			delete pomDlg;
		}
		else
		{
			MessageBox(LoadStg(IDS_STRING321), LoadStg(IDS_STRING149), (MB_ICONSTOP|MB_OK));
		}
		olList.DeleteAll();

		AfxGetApp()->DoWaitCursor(-1);
	}
	return 0L;
}

//----------------------------------------------------------------------------------------

void FidsDisplayScheduleDlg::OnDPTNew() 
{
	if (ogPrivList.GetStat("DISPLAYSCHDLG.m_DPTNEW") == '1')
	{
		InsertOrDelete(OP_INSERT);
	}
}

//----------------------------------------------------------------------------------------

void FidsDisplayScheduleDlg::OnDPTDel() 
{
	int ilLineNo = pomTable->GetCurSel();
	if (ilLineNo == -1)
	{
		MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONSTOP);
	}
	else
	{
		if(ogPrivList.GetStat("DISPLAYSCHDLG.m_DPTDEL") == '1')
		{
			InsertOrDelete(OP_DELETE,ilLineNo);
		}
	}
}

void FidsDisplayScheduleDlg::OnButtonDeviceClicked()
{
	CStringArray olCol1, olCol2,olCol3;
	CCSPtrArray<DEVDATA> olList;

	AfxGetApp()->DoWaitCursor(1);
	if(ogDEVData.ReadSpecialData(&olList, "", "URNO,DEVN", false) == true)
	{
		AfxGetApp()->DoWaitCursor(-1);
		for(int i = 0; i < olList.GetSize(); i++)
		{
			char pclUrno[20]="";
			sprintf(pclUrno, "%ld", olList[i].Urno);
			olCol3.Add(CString(pclUrno));
			olCol2.Add(CString(olList[i].Devn));
		}

		AwDlg *pomDlg = new AwDlg(NULL,&olCol2,&olCol3,"",LoadStg(IDS_STRING935),this);
		if(pomDlg->DoModal() == IDOK)
		{
			m_DEVN.SetWindowText(pomDlg->omReturnString);			
			m_DEVN.SetFocus();
			m_DEVN.SetTextColor(GREEN);
		}
		delete pomDlg;
	}
	else
	{
		MessageBox(LoadStg(IDS_STRING321), LoadStg(IDS_STRING149), (MB_ICONSTOP|MB_OK));
	}
	olList.DeleteAll();

	AfxGetApp()->DoWaitCursor(-1);
}

void FidsDisplayScheduleDlg::OnTableIPEdit()
{
	if(pomTable->GetCurrentLine() != 0 && (pomTable->imCurrentColumn == 1 || pomTable->imCurrentColumn == 2 || pomTable->imCurrentColumn == 3))
	{
		pomTable->pomIPEdit->SetBackGrColor(YELLOW);
	}
}

void FidsDisplayScheduleDlg::OnTableIPEditButtonDown(UINT wParam, LONG lParam)
{
	int ilCurrentColumn = lParam;
	if(pomTable->GetCurrentLine() != 0 && (ilCurrentColumn == 1 || ilCurrentColumn == 2 || ilCurrentColumn == 3))
	{
		pomTable->pomIPEdit->SetBackGrColor(YELLOW);
	}
}

void FidsDisplayScheduleDlg::OnTableIPEditShiftButtonDown(UINT wParam, LONG lParam)
{
	int ilCurrentColumn = lParam;
	if(pomTable->GetCurrentLine() != 0 && (ilCurrentColumn == 1 || ilCurrentColumn == 2 || ilCurrentColumn == 3))
	{
		pomTable->pomIPEdit->SetBackGrColor(YELLOW);
	}
}

void FidsDisplayScheduleDlg::CreateEditAttributes(CCSEDIT_ATTRIB* prpAttrib)
{
	prpAttrib[0].Format = "X(1)";
	prpAttrib[0].TextMinLenght = 0;
	prpAttrib[0].TextMaxLenght = 1;
	prpAttrib[0].Style = ES_UPPERCASE;

	prpAttrib[1].Format = "#(7)";
	prpAttrib[1].TextMinLenght = 0;
	prpAttrib[1].TextMaxLenght = 7;
	prpAttrib[1].Style = ES_UPPERCASE;

	prpAttrib[2].Format = "##':'##";
	prpAttrib[2].Type = KT_TIME;
	prpAttrib[2].TextMinLenght = 4;
	prpAttrib[2].TextMaxLenght =5;
	prpAttrib[2].Style = ES_UPPERCASE;
	
	prpAttrib[3].Format = "##':'##";
	prpAttrib[3].Type = KT_TIME;
	prpAttrib[3].TextMinLenght = 4;
	prpAttrib[3].TextMaxLenght =5;
	prpAttrib[3].Style = ES_UPPERCASE;

	prpAttrib[4].Format = "X(32)";
	prpAttrib[4].TextMinLenght = 0;
	prpAttrib[4].TextMaxLenght =32;
	prpAttrib[4].Style = ES_UPPERCASE;
	
	prpAttrib[5].Format = "X(8)";
	prpAttrib[5].TextMinLenght = 0;
	prpAttrib[5].TextMaxLenght =8;
	prpAttrib[5].Style = ES_UPPERCASE;

	prpAttrib[6].Format = "X(128)";
	prpAttrib[6].TextMinLenght = 0;
	prpAttrib[6].TextMaxLenght =128;
	prpAttrib[6].Style = ES_UPPERCASE;

	prpAttrib[7].Format = "X(5)";
	prpAttrib[7].TextMinLenght = 0;
	prpAttrib[7].TextMaxLenght =5;
	prpAttrib[7].Style = ES_UPPERCASE;

	prpAttrib[8].Format = "X(10)";
	prpAttrib[8].TextMinLenght = 0;
	prpAttrib[8].TextMaxLenght =10;
	prpAttrib[8].Style = ES_UPPERCASE;

	prpAttrib[9].Format = "X(40)";
	prpAttrib[9].TextMinLenght = 0;
	prpAttrib[9].TextMaxLenght =40;
	prpAttrib[9].Style = ES_UPPERCASE;
}


void FidsDisplayScheduleDlg::PrepareDPTDataArray()
{
	omDPTSaveDataArray.RemoveAll();
	for(int i = 0 ; i < pomTable->omLines.GetSize() ; i++)
	{
		DPTDATA olDPTData;		
		PopulateDPTData(olDPTData,i);
		omDPTSaveDataArray.NewAt(omDPTSaveDataArray.GetSize(),olDPTData);
	}
}

bool FidsDisplayScheduleDlg::ValidateDPTData(CString& ropErrorMessage) const
{
	for(int i = 0 ; i < pomTable->GetLinesCount() ; i++)
	{
		if(i == 0)
		{
		}
		else
		{
			if(pomTable->omLines[i].Columns[1].Text.IsEmpty() || pomTable->omLines[i].Columns[2].Text.IsEmpty() || pomTable->omLines[i].Columns[3].Text.IsEmpty())
			{
				ropErrorMessage.LoadString(IDS_STRING1006);
				return false;
			}
			else if(pomTable->omLines[i].Columns[1].Text.IsEmpty() && pomTable->omLines[i].Columns[2].Text.IsEmpty() && pomTable->omLines[i].Columns[3].Text.IsEmpty())
			{
				ropErrorMessage.LoadString(IDS_STRING1005);
				return false;
			}
		}		
	}
	return true;
}

void FidsDisplayScheduleDlg::PopulateDisplayPrototypeLineData(DisplayPrototypeLineData& ropDisplayPrototype,const DPTDATA& ropDptData)
{
	strcpy(ropDisplayPrototype.Algc,ropDptData.Algc);
	ropDisplayPrototype.Cdat = ropDptData.Cdat;
	strcpy(ropDisplayPrototype.Dare,ropDptData.Dare);
	strcpy(ropDisplayPrototype.Dbgn,ropDptData.Dbgn);
	strcpy(ropDisplayPrototype.Defl,ropDptData.Defl);
	strcpy(ropDisplayPrototype.Dend,ropDptData.Dend);
	strcpy(ropDisplayPrototype.Dfco,ropDptData.Dfco);
	strcpy(ropDisplayPrototype.Dffd,ropDptData.Dffd);
	strcpy(ropDisplayPrototype.Doop,ropDptData.Doop);
	strcpy(ropDisplayPrototype.Dpid,ropDptData.Dpid);
	strcpy(ropDisplayPrototype.Dter,ropDptData.Dter);
	strcpy(ropDisplayPrototype.Hopo,ropDptData.Hopo);
	ropDisplayPrototype.Lstu = ropDptData.Lstu;
	ropDisplayPrototype.Rurn = ropDptData.Rurn;
	ropDisplayPrototype.Urno = ropDptData.Urno;
	strcpy(ropDisplayPrototype.Usec,ropDptData.Usec);
	strcpy(ropDisplayPrototype.Useu,ropDptData.Useu);
}

void FidsDisplayScheduleDlg::PopulateDisplayPrototypeLineData()
{
	CString olData;
	for(int ilLineNo = 0 ; ilLineNo < pomTable->GetLinesCount(); ilLineNo++)
	{
		for(int jlColumnNo = 0; jlColumnNo < DISPLAY_PROTOTYPE_COLUMNS; jlColumnNo++)
		{
			switch(jlColumnNo)
			{
			case 0:
				strcpy(omDisplayPrototypeLines[ilLineNo].Defl,pomTable->omLines[ilLineNo].Columns[0].Text);
				break;
			case 1:
				strcpy(omDisplayPrototypeLines[ilLineNo].Doop,pomTable->omLines[ilLineNo].Columns[1].Text);
				break;
			case 2:
				olData = pomTable->omLines[ilLineNo].Columns[2].Text.Remove(':');
				strcpy(omDisplayPrototypeLines[ilLineNo].Dbgn,olData);
				break;
			case 3:
				olData = pomTable->omLines[ilLineNo].Columns[3].Text.Remove(':');
				strcpy(omDisplayPrototypeLines[ilLineNo].Dend,olData);
				break;
			case 4:
				strcpy(omDisplayPrototypeLines[ilLineNo].Dpid,pomTable->omLines[ilLineNo].Columns[4].Text);
				break;
			case 5:
				strcpy(omDisplayPrototypeLines[ilLineNo].Dffd,pomTable->omLines[ilLineNo].Columns[5].Text);
				break;
			case 6:
				strcpy(omDisplayPrototypeLines[ilLineNo].Dfco,pomTable->omLines[ilLineNo].Columns[6].Text);
				break;
			case 7:
				strcpy(omDisplayPrototypeLines[ilLineNo].Dter,pomTable->omLines[ilLineNo].Columns[7].Text);
				break;
			case 8:
				strcpy(omDisplayPrototypeLines[ilLineNo].Dare,pomTable->omLines[ilLineNo].Columns[8].Text);
				break;
			case 9:
				strcpy(omDisplayPrototypeLines[ilLineNo].Algc,pomTable->omLines[ilLineNo].Columns[9].Text);
				break;
			default:
				break;
				
			}
		}
	}
}

void FidsDisplayScheduleDlg::PopulateDPTData(DPTDATA& ropDptData,int ilLineNo)
{
	strcpy(ropDptData.Defl,pomTable->omLines[ilLineNo].Columns[0].Text);
	strcpy(ropDptData.Doop,pomTable->omLines[ilLineNo].Columns[1].Text);
	strcpy(ropDptData.Dbgn,pomTable->omLines[ilLineNo].Columns[2].Text);
	strcpy(ropDptData.Dend,pomTable->omLines[ilLineNo].Columns[3].Text);
	strcpy(ropDptData.Dpid,pomTable->omLines[ilLineNo].Columns[4].Text);
	strcpy(ropDptData.Dffd,pomTable->omLines[ilLineNo].Columns[5].Text);
	strcpy(ropDptData.Dfco,pomTable->omLines[ilLineNo].Columns[6].Text);
	strcpy(ropDptData.Dter,pomTable->omLines[ilLineNo].Columns[7].Text);
	strcpy(ropDptData.Dare,pomTable->omLines[ilLineNo].Columns[8].Text);
	strcpy(ropDptData.Algc,pomTable->omLines[ilLineNo].Columns[9].Text);

	if(omDisplayPrototypeLines[ilLineNo].Urno != 0)
	{
		ropDptData.Rurn = pomDSC->Urno;
		strcpy(ropDptData.Hopo ,ogHome);
		strcpy(ropDptData.Usec,omDisplayPrototypeLines[ilLineNo].Usec);
		ropDptData.Urno = omDisplayPrototypeLines[ilLineNo].Urno;
		ropDptData.Lstu = CTime::GetCurrentTime();
		strcpy(ropDptData.Useu,cgUserName); 
	}
	else
	{
		ropDptData.Rurn = pomDSC->Urno;
		ropDptData.Cdat = CTime::GetCurrentTime();
		ropDptData.Urno = 0;
		strcpy(ropDptData.Hopo ,ogHome);
		strcpy(ropDptData.Usec,cgUserName);
	}
}

bool FidsDisplayScheduleDlg::SaveDPTData()
{	
	CString olInsertErrTxt(LoadStg(ST_INSERTERR));
	CString olUpdateErrTxt(LoadStg(ST_UPDATEERR));
	CString olDeleteErrTxt(LoadStg(ST_DELETEERR));
	CString olErrorText;
	bool blSaveUpdateDeleteDPTData = true;

	CString olErrorMessage;
	if(ValidateDPTData(olErrorMessage) == true)
	{
		PopulateDisplayPrototypeLineData();
		PrepareDPTDataArray();
	}
	else
	{		
		return false;
	}

	DPTDATA* polDptData;
	for(int i = 0; i < omDPTSaveDataArray.GetSize(); i++)
	{
		polDptData = &omDPTSaveDataArray[i];
		if(polDptData->Urno == 0)
		{
			polDptData->Urno = ogBasicData.GetNextUrno();
			if(ogDPTData.InsertDPT(polDptData) == false)
			{			
				olErrorText.Format("%s %d\n%s",olInsertErrTxt, ogDPTData.imLastReturnCode, ogDPTData.omLastErrorMessage);
				MessageBox(olErrorText,LoadStg(IDS_STRING145),MB_ICONERROR);
				blSaveUpdateDeleteDPTData = false;
				break;
			}
		}
		else
		{
			if(ogDPTData.UpdateDPT(polDptData) == false)
			{
				olErrorText.Format("%s %d\n%s",olUpdateErrTxt,ogDPTData.imLastReturnCode, ogDPTData.omLastErrorMessage);
				MessageBox(olErrorText,LoadStg(IDS_STRING145),MB_ICONERROR);
				blSaveUpdateDeleteDPTData = false;
				break;
			}
		}
	}
	if(omTobeDeletedDptUrnosArray.GetCount() > 0)
	{		
		POSITION olPosition = NULL;
		olPosition = omTobeDeletedDptUrnosArray.GetHeadPosition();
		while(olPosition != NULL)
		{
			long llUrno = omTobeDeletedDptUrnosArray.GetNext(olPosition);
			if(ogDPTData.DeleteDPT(llUrno) == false)
			{
				CString olErrorText;				
				olErrorText.Format("%s %d\n%s",olDeleteErrTxt, ogDPTData.imLastReturnCode, ogDPTData.omLastErrorMessage);
				MessageBox(olErrorText,LoadStg(IDS_STRING145),MB_ICONERROR);
				blSaveUpdateDeleteDPTData = false;
				break;
			}
		}
	}
	return blSaveUpdateDeleteDPTData;
}

CString FidsDisplayScheduleDlg::PrepareDisplayDateFormat(const CString& ropDate, 
														 bool blSetDefaultIncaseEmptyDate) const
{
	CString olReturnDateString;
	if(ropDate.GetLength() == 0 && blSetDefaultIncaseEmptyDate == true)
	{
		return CTime::GetCurrentTime().Format("%d.%m.%Y");
	}
	else if(ropDate.GetLength() > 0)
	{
		olReturnDateString = ropDate.Mid(6,2) + "." + ropDate.Mid(4,2) + "." + ropDate.Mid(0,4);
	}
	return olReturnDateString;
}