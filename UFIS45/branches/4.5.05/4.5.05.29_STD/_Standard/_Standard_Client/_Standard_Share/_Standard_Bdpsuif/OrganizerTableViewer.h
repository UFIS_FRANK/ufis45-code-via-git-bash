#ifndef __ORGANIZERTABLEVIEWER_H__
#define __ORGANIZERTABLEVIEWER_H__

#include <stdafx.h>
#include <CedaChtData.h>
#include <CCSTable.h>
#include <CViewer.h>
#include <CCSPrint.h>


struct ORGANIZERTABLE_LINEDATA
{
	long	Urno;
	CString	Chtn;
	CString	Chtc;
	CString	Vafr;
	CString	Vato;
};

/////////////////////////////////////////////////////////////////////////////
// OrganizerTableViewer

class OrganizerTableViewer : public CViewer
{
// Constructions
public:
    OrganizerTableViewer(CCSPtrArray<CHTDATA> *popData);
    ~OrganizerTableViewer();

    void Attach(CCSTable *popAttachWnd);
    virtual void ChangeViewTo(const char *pcpViewName);
// Internal data processing routines
private:
	bool IsPassFilter(CHTDATA *prpOrganizer);
	int CompareOrganizer(ORGANIZERTABLE_LINEDATA *prpOrganizer1, ORGANIZERTABLE_LINEDATA *prpOrganizer2);
    void MakeLines();
	void MakeLine(CHTDATA *prpOrganizer);
	bool FindLine(long lpUrno, int &rilLineno);
// Operations
public:
	void DeleteAll();
	void CreateLine(ORGANIZERTABLE_LINEDATA *prpOrganizer);
	void DeleteLine(int ipLineno);
// Window refreshing routines
public:
	void UpdateDisplay();
	CString Format(ORGANIZERTABLE_LINEDATA *prpLine);
	void ProcessOrganizerChange(CHTDATA *prpOrganizer);
	void ProcessOrganizerDelete(CHTDATA *prpOrganizer);
	bool FindOrganizer(char *prpOrganizerKeya, char *prpOrganizerKeyd, int& ilItem);
// Attributes used for filtering condition
private:
	bool bmIsFromSearch;
// Attributes
private:
    CCSTable *pomOrganizerTable;
	CCSPtrArray<CHTDATA> *pomData;
// Methods which handle changes (from Data Distributor)
public:
    CCSPtrArray<ORGANIZERTABLE_LINEDATA> omLines;
///////
	CCSPtrArray<TABLE_HEADER_COLUMN> omHeaderDataArray;
//Print 
	void PrintTableView();
	bool PrintTableLine(ORGANIZERTABLE_LINEDATA *prpLine,bool bpLastLine);
	bool PrintTableHeader();
	CCSPrint *pomPrint;
	//CBitmap omBitmap;
	CString omTableName;

};

#endif //__ORGANIZERTABLEVIEWER_H__
