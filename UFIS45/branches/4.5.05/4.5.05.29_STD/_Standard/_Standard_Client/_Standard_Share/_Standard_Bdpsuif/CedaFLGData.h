// CedaFLGData.h

#ifndef __CEDAFLGDATA__
#define __CEDAFLGDATA__

#include <basicdata.h>

//---------------------------------------------------------------------------------------------------------
// Record structure declaration

struct FLGDATA 
{
	long 	 Urno;      //Unique Record Number
	char     Hopo[4];	//Home Airport
	char 	 Usec[33];  //User Creation
	CTime	 Cdat;	    //Creation Date / Time
	char 	 Useu[33];  //User Update
	CTime	 Lstu;      //Last Update Date / Time
	char     Lgfn[129]; //Logo File Description
	char     Lgsn[31];  //Logo File Name
	char     Type[2];   //Logo Type (G=Gate, C=Counter)
	char     Alc2[3];   //Airline 2-Letter Code
	char     Alc3[4];   //Airline 3-Letter Code

	//DataCreated by this class
	int		 IsChanged;

	FLGDATA(void)
	{ 
		memset(this,'\0',sizeof(*this));
		Cdat=-1;
		Lstu=-1;
	}
};


//---------------------------------------------------------------------------------------------------------
// Class declaration

class CedaFLGData: public CCSCedaData
{
// Attributes
public:
    CMapPtrToPtr		 omUrnoMap;	
    CCSPtrArray<FLGDATA> omData;

// Operations
public:
    CedaFLGData();
	~CedaFLGData();

	virtual void GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType);
	void Register(void);
	void ClearAll(void);

    bool Read(char *pcpWhere = NULL);
	bool ReadSpecialData(CCSPtrArray<FLGDATA> *popFLGDataArray,char *pspWhere,char *pspFieldList,bool ipSYS = true);
	bool InsertFLG(FLGDATA *prpFLG,BOOL bpSendDdx = TRUE);
	bool InsertFLGInternal(FLGDATA *prpFLG);
	bool UpdateFLG(FLGDATA *prpFLG,BOOL bpSendDdx = TRUE);
	bool UpdateFLGInternal(FLGDATA *prpFLG);
	bool DeleteFLG(long lpUrno);
	bool DeleteFLGInternal(FLGDATA *prpFLG);
	FLGDATA  *GetFLGByUrno(long lpUrno);	
	bool SaveFLG(FLGDATA *prpFLG);
	char pcmFLGFieldList[2048];
	void ProcessFLGBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);	
	void SetWhere(const CString& ropWhere){omWhere = ropWhere;}
	void GetWhere(CString& ropWhere) const{ropWhere = omWhere;}
	
private:
    void PrepareFLGData(FLGDATA *prpFLGData);	
	CString omWhere;
	bool ValidateFLGBcData(const long& lrpUnro);
};


//---------------------------------------------------------------------------------------------------------

#endif //__CedaFLGData__
