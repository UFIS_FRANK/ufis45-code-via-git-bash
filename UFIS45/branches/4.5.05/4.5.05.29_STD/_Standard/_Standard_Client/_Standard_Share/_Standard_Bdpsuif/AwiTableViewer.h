#ifndef __AirportwetTableViewer_H__
#define __AirportwetTableViewer_H__

#include <stdafx.h>
#include <CedaAwiData.h>
#include <CCSTable.h>
#include <CViewer.h>
#include <CCSPrint.h>


struct AIRPORTWETTABLE_LINEDATA
{
	long 	 Urno; 	// Eindeutige Datensatz-Nr.
	CString  Apc3; 	// Code
	CString  Apc4; 	
	CString  Humi; 	
	CString  Temp; 	
	CString  Visi; 	
	CString  Wcod; 	
	CString  Wdir; 	
	CString  Wind; 	
	CString  Msgt; 	
	CString	 Vafr; 	
	CString  Vato; 	

};

/////////////////////////////////////////////////////////////////////////////
// AirportwetTableViewer

	  
class AirportwetTableViewer : public CViewer
{
// Constructions
public:
    AirportwetTableViewer(CCSPtrArray<AWIDATA> *popData);
    ~AirportwetTableViewer();

    void Attach(CCSTable *popAttachWnd);
    virtual void ChangeViewTo(const char *pcpViewName);
// Internal data processing routines
private:
	bool IsPassFilter(AWIDATA *prpAirportwet);
	int CompareAirportwet(AIRPORTWETTABLE_LINEDATA *prpAirportwet1, AIRPORTWETTABLE_LINEDATA *prpAirportwet2);
    void MakeLines();
	void MakeLine(AWIDATA *prpAirportwet);
	bool FindLine(long lpUrno, int &rilLineno);
// Operations
public:
	void DeleteAll();
	void CreateLine(AIRPORTWETTABLE_LINEDATA *prpAirportwet);
	void DeleteLine(int ipLineno);
// Window refreshing routines
public:
	void UpdateDisplay();
	CString Format(AIRPORTWETTABLE_LINEDATA *prpLine);
	void ProcessAirportwetChange(AWIDATA *prpAirportwet);
	void ProcessAirportwetDelete(AWIDATA *prpAirportwet);
	bool FindAirportwet(char *prpAirportwetKeya, char *prpAirportwetKeyd, int& ilItem);
// Attributes used for filtering condition
private:
	bool bmIsFromSearch;
// Attributes
private:
    CCSTable *pomAirPortWetTable;
	CCSPtrArray<AWIDATA> *pomData;
// Methods which handle changes (from Data Distributor)
public:
    CCSPtrArray<AIRPORTWETTABLE_LINEDATA> omLines;
///////
	CCSPtrArray<TABLE_HEADER_COLUMN> omHeaderDataArray;
//Print 
	void PrintTableView();
	bool PrintTableLine(AIRPORTWETTABLE_LINEDATA *prpLine,bool bpLastLine);
	bool PrintTableHeader();
	CCSPrint *pomPrint;
	//CBitmap omBitmap;
	CString omTableName;

};

#endif //__AirportwetTableViewer_H__
