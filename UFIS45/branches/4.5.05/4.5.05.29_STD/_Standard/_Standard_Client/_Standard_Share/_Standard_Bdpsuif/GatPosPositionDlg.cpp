// GatPosPositionDlg.cpp : implementation file
//

#include <stdafx.h>
#include <bdpsuif.h>
#include <GatPosPositionDlg.h>
#include <SelectionDlg.h>
#include <CedaGatData.h>
#include <BasicData.h>
#include <PrivList.h>
#include <GatPosNotAvailableDlg.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// GatPosPositionDlg dialog


GatPosPositionDlg::GatPosPositionDlg(PSTDATA *popPST,CWnd* pParent /*=NULL*/)
	: PositionDlg(popPST,GatPosPositionDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(GatPosPositionDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}

GatPosPositionDlg::GatPosPositionDlg(PSTDATA *popPST,int ipDlg,CWnd* pParent /*=NULL*/)
	: PositionDlg(popPST,ipDlg, pParent)
{
	//{{AFX_DATA_INIT(GatPosPositionDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void GatPosPositionDlg::DoDataExchange(CDataExchange* pDX)
{
	PositionDlg::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(GatPosPositionDlg)
	DDX_Control(pDX, IDC_BLOCKING_BITMAP, m_BlockingBitmap);
	DDX_Control(pDX, IDC_GATES_LIST, m_ConnectedGates);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(GatPosPositionDlg, PositionDlg)
	//{{AFX_MSG_MAP(GatPosPositionDlg)
	ON_BN_CLICKED(IDC_GATES, OnGates)
	ON_MESSAGE(WM_TABLE_LBUTTONDBLCLK,	OnTableLButtonDblclk)
	ON_BN_CLICKED(IDC_NOAVNEW, OnNoavNew)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// GatPosPositionDlg message handlers

void GatPosPositionDlg::OnGates() 
{
	// TODO: Add your control notification handler code here
	SelectionDlg olDlg(this);
	CString olCaption;
	olCaption.Format(LoadStg(IDS_STRING906),this->pomPST->Pnam);
	olDlg.SetCaption(olCaption);
	olDlg.SetPossibleList(LoadStg(IDS_STRING907),omAvailableGates);
	olDlg.SetSelectionList(LoadStg(IDS_STRING908),omConnectedGates);
	if (olDlg.DoModal() == IDOK)
	{
		omConnectedGates.RemoveAll();
		olDlg.GetSelectionList(omConnectedGates);
		m_ConnectedGates.ResetContent();
		for (int i = 0; i < omConnectedGates.GetSize(); i++)
		{
			m_ConnectedGates.AddString(omConnectedGates[i]);
		}
	}
}

BOOL GatPosPositionDlg::OnInitDialog() 
{
	if (!PositionDlg::OnInitDialog())
		return FALSE;
	
	// TODO: Add extra initialization here
	ogGATData.GetGATList(omAvailableGates);
	ogBasicData.GetGatesForPosition(this->pomPST->Urno,omConnectedGates);
	for (int i = 0; i < omConnectedGates.GetSize(); i++)
	{
		m_ConnectedGates.AddString(omConnectedGates[i]);
	}

	CWnd *polWnd = GetDlgItem(IDC_GATES);
	if (polWnd)
		ogBasicData.SetWindowStat("POSITIONDLG.m_GATE",polWnd);

	ogBasicData.AddBlockingImages(m_BlockingBitmap);
	m_BlockingBitmap.SetCurSel(this->pomPST->Ibit);
	ogBasicData.SetWindowStat("POSITIONDLG.m_IBIT",&m_BlockingBitmap);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void GatPosPositionDlg::OnOK() 
{
	// TODO: Add extra validation here
	this->pomPST->Ibit = m_BlockingBitmap.GetCurSel();

	PositionDlg::OnOK();
}

int GatPosPositionDlg::GetConnectedGates(CStringArray& ropGates)
{
	ropGates.Append(this->omConnectedGates);
	return ropGates.GetSize();
}

//----------------------------------------------------------------------------------------

LONG GatPosPositionDlg::OnTableLButtonDblclk(UINT wParam, LONG lParam) 
{
	if(ogPrivList.GetStat("POSITIONDLG.m_NOAVCHA") == '1')
	{
		int ilLineNo = pomTable->GetCurSel();
		if (ilLineNo == -1)
		{
			MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONSTOP);
		}
		else
		{
			BLKDATA *prlBlk = &omBlkPtrA[ilLineNo];
			if (prlBlk != NULL)
			{
				BLKDATA rlBlk = *prlBlk;
				GatPosNotAvailableDlg *polDlg = new GatPosNotAvailableDlg(&rlBlk,this);
				m_PNAM.GetWindowText(polDlg->omName);
				if(polDlg->DoModal() == IDOK)
				{
					*prlBlk = rlBlk;
					if(prlBlk->IsChanged != DATA_NEW) prlBlk->IsChanged = DATA_CHANGED;
					ChangeNoavTable(prlBlk, ilLineNo);
				}
				delete polDlg;
			}
		}
	}
	return 0L;
}

//----------------------------------------------------------------------------------------

void GatPosPositionDlg::OnNoavNew() 
{
	BLKDATA rlBlk;

	if(ogPrivList.GetStat("POSITIONDLG.m_NOAVNEW") == '1')
	{
		GatPosNotAvailableDlg *polDlg = new GatPosNotAvailableDlg(&rlBlk,this);
		m_PNAM.GetWindowText(polDlg->omName);
		if(polDlg->DoModal() == IDOK)
		{
			BLKDATA *prlBlk = new BLKDATA;
			*prlBlk = rlBlk;
			omBlkPtrA.Add(prlBlk);
			prlBlk->IsChanged = DATA_NEW;
			ChangeNoavTable(prlBlk, -1);
		}
		delete polDlg;
	}
}
