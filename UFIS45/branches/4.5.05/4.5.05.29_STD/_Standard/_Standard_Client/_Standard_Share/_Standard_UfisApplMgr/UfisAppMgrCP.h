#ifndef _UFISAPPMGRCP_H_
#define _UFISAPPMGRCP_H_

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * 
 *	UFISAppMang:	UFIS Client Application Manager Project
 *
 *	Proxy Communicator for Client Applications
 *	
 *	ABB Airport Technologies GmbH 2000
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 *		Author			Date			Comment
 *	------------------------------------------------------------------
 *
 *		cla	AAT/ID		02/09/2000		Initial version
 *		cla AAT/ID		17/03/2001		Multithreaded version
 *										to avoid deadlocks
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

 /*
  * Be careful here, store a backup of this file in SCM system!
  * The App wizard will recreate this stuff! So you will loose all the changes
  * Remark: To understand what is going on here, see either Don Box -> COM ...
  * or Guy Eddon, Henry Eddon -> Inside DCOM
  */

template <class T>
class CProxy_IUfisAppMgrEvents : public IConnectionPointImpl<T, &DIID__IUfisAppMgrEvents, CComDynamicUnkArray>
{
	//Warning this class may be recreated by the wizard.

public:
	HRESULT Fire_OnDataReceive(BSTR Data)
	{

		T* pT = static_cast<T*>(this);

		int nConnectionIndex;
		int nConnections = m_vec.GetSize();
		CComVariant olVars[1];
		CComVariant olResult;

		for (nConnectionIndex = 0; nConnectionIndex < nConnections; nConnectionIndex++)
		{
			pT->Lock();
			CComPtr<IUnknown> sp = m_vec.GetAt(nConnectionIndex);
			pT->Unlock();
			IDispatch* pDispatch = reinterpret_cast<IDispatch*>(sp.p);

			if (pDispatch != NULL)
			{

				olVars[0] = Data;

				DISPPARAMS disp = {olVars, NULL, 1, 0 };

				pDispatch->Invoke(0x1, IID_NULL, LOCALE_USER_DEFAULT, DISPATCH_METHOD, &disp, &olResult, NULL, NULL);

			}
		}

		return olResult.scode;
	}

};
#endif