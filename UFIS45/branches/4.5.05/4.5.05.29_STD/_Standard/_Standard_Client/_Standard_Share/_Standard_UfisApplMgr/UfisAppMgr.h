// UfisAppMgr.h : Declaration of the CUfisAppMgr

#ifndef __UFISAPPMGR_H_
#define __UFISAPPMGR_H_

#include <resource.h>       // main symbols
#include <UfisAppMgrCP.h>
#include <UfisApplMgr.h>
#include <list>
#include <iterator>

/////////////////////////////////////////////////////////////////////////////
// CUfisAppMgr

#define GEN_PATH_LEN 512

class ATL_NO_VTABLE CUfisAppMgr : 
	public CComObjectRootEx<CComSingleThreadModel>,
	public CComCoClass<CUfisAppMgr, &CLSID_UfisAppMgr>,
	public ISupportErrorInfo,
	public IConnectionPointContainerImpl<CUfisAppMgr>,
	public IDispatchImpl<IUfisAppMgr, &IID_IUfisAppMgr, &LIBID_UFISAPPLMGRLib>,
	public CProxy_IUfisAppMgrEvents< CUfisAppMgr >
{
public:
	CUfisAppMgr()
	{
	}

DECLARE_REGISTRY_RESOURCEID(IDR_UFISAPPMGR)
DECLARE_GET_CONTROLLING_UNKNOWN()
DECLARE_PROTECT_FINAL_CONSTRUCT()

BEGIN_COM_MAP(CUfisAppMgr)
	COM_INTERFACE_ENTRY(IUfisAppMgr)
	COM_INTERFACE_ENTRY(IDispatch)
	COM_INTERFACE_ENTRY(IConnectionPointContainer)
	COM_INTERFACE_ENTRY_IMPL(IConnectionPointContainer)
END_COM_MAP()
BEGIN_CONNECTION_POINT_MAP(CUfisAppMgr)
CONNECTION_POINT_ENTRY(DIID__IUfisAppMgrEvents)
END_CONNECTION_POINT_MAP()


// ISupportsErrorInfo
	STDMETHOD(InterfaceSupportsErrorInfo)(REFIID riid);

// IUfisAppMgr
public:
	STDMETHOD(get_Version)(/*[out, retval]*/ BSTR *pVal);
	STDMETHOD(get_BuildDate)(/*[out, retval]*/ BSTR *pVal);
	STDMETHOD(get_GetNameByID)(long lID, /*[out, retval]*/ BSTR *pVal);
	STDMETHOD(get_GetVersion)(/*[out, retval]*/ BSTR *pVal);
	STDMETHOD(get_GetMyID)(/*[out, retval]*/ long *pVal);
	STDMETHOD(get_GetIDsByAppName)(BSTR AppPathName, /*[out, retval]*/ BSTR *pVal);
	STDMETHOD(get_IsAppPending)(BSTR AppPathName, /*[out, retval]*/ BOOL *pVal);
	STDMETHOD(get_IsAppStarted)(BSTR AppPathName, /*[out, retval]*/ BOOL *pVal);
	STDMETHOD(SendDataToAll)(BSTR Data);
	STDMETHOD(SendDataByID)(long lID, BSTR Data);
	STDMETHOD(SendDataByName)(BSTR AppPathName, BSTR Data);
	STDMETHOD(Register)(BSTR AppPathName);
	STDMETHOD(StartApp)(BSTR AppPathName, BSTR CommandLine);

	//
	// Trace of the advise and unadvise call
	//
	STDMETHOD(Advise)(IUnknown* pUnkSink,DWORD* pdwCookie);
	STDMETHOD(Unadvise)(DWORD pdwCookie);

	// holding important information about 'this'
	//
	IUnknown* m_pActUnk;
	DWORD m_cookie;
};

#endif //__UFISAPPMGR_H_
