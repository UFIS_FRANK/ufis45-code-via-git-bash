// BcCCmdTarget.h: interface for the BcCCmdTarget class.
//
//////////////////////////////////////////////////////////////////////

#include <CCSBcHandle.h>

#if !defined(AFX_BCCCMDTARGET_H__30E3FB73_5EE7_11D7_80C6_0001022205E4__INCLUDED_)
#define AFX_BCCCMDTARGET_H__30E3FB73_5EE7_11D7_80C6_0001022205E4__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CCSBcHandle;
class BcCCmdTarget : public CCmdTarget  
{
public:
	BcCCmdTarget(CCSBcHandle* prpBcHandle);
	virtual ~BcCCmdTarget();
	void SetSpoolOn();
	void SetSpoolOff();
	void AddTableCommand(CString opTable, CString opCommand,bool bpOwnBc,CString opAppl);
	void RemoveTableCommand(CString opTable, CString opCommand);
	void ExitApp();
	void SetFilterRange(CString spTable, CString spFromField, CString spToField, CString spFromValue, CString spToValue);

	//{{AFX_DISPATCH(BcCCmdTarget)
		afx_msg BOOL OnBroadcast(LPCTSTR ReqId, LPCTSTR Dest1, LPCTSTR Dest2, LPCTSTR Cmd, LPCTSTR Object, LPCTSTR Seq, LPCTSTR Tws, LPCTSTR Twe, LPCTSTR Selection, LPCTSTR Fields, LPCTSTR Data, LPCTSTR BcNum);
		afx_msg long OnLostBc(long ilErr);
		afx_msg long OnLostBcEx(LPCTSTR strException);
		afx_msg long OnBc(long size,long currPackage,long totalPackages, LPCTSTR strData);
	//}}AFX_DISPATCH

	DECLARE_DISPATCH_MAP()
	DECLARE_MESSAGE_MAP()
	DECLARE_INTERFACE_MAP()
	DECLARE_OLECREATE(BcCCmdTarget)

private:
	CCSBcHandle *prmBcHandle;
	_IBcPrxyEvents *pomBcPrxyEvents;
	IBcPrxy *pomBcPrxyInstance;
	IDispatch* pDispBcPrxyEvents;
	DWORD m_dwCookie;// used for AfxConnectionAdvise(..)	BcProxy
	IUnknown* pIUnknown_Auto;
	BOOL	m_bUseBcProxy;

	_IBcComAtlEvents omAtlComEvents;
	IBcComAtl *pomAtlComInstance;
	IDispatch* pDispAtlComEvents;

	DWORD m_dwAtlComCookie;// used for AfxConnectionAdvise(..) BCComServer
	CString	omData;
	BOOL	m_bUseBcCom;
	BOOL	m_bTransmitFilter;

};

#endif // !defined(AFX_BCCCMDTARGET_H__30E3FB73_5EE7_11D7_80C6_0001022205E4__INCLUDED_)
