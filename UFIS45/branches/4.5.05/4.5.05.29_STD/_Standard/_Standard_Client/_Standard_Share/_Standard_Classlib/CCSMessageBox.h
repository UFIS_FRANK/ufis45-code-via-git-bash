// CCSMessageBox.h: interface for the CCSMessageBox class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_CCSMESSAGEBOX_H__451F6262_1DDD_11D1_82C6_0080AD1DC701__INCLUDED_)
#define AFX_CCSMESSAGEBOX_H__451F6262_1DDD_11D1_82C6_0080AD1DC701__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

class CCSMessageBox  
{
public:
	CCSMessageBox();
	virtual ~CCSMessageBox();
    int Show(CWnd *popWnd,LPCTSTR lpszText,LPCTSTR lpszCaption,UINT nType);
    bool IsShown();

private:
    static int imAnzahl;

};

extern CCSMessageBox ogMessageBox;

#endif // !defined(AFX_CCSMESSAGEBOX_H__451F6262_1DDD_11D1_82C6_0080AD1DC701__INCLUDED_)
