// CCSBasic.cpp: Implementierung der Klasse CCSBasic.
//
//////////////////////////////////////////////////////////////////////

#include <stdafx.h>
#include <CCSBasic.h> 

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Konstruktion/Destruktion
//////////////////////////////////////////////////////////////////////

CCSBasic::CCSBasic()
{
	omReplaceMap.SetAt("\0xe4", "ae"); // ein �
	omReplaceMap.SetAt("\0xf6", "oe"); // ein �
	omReplaceMap.SetAt("\0xfc", "ue"); // ein �
	omReplaceMap.SetAt("\0xc4", "Ae"); // ein �
	omReplaceMap.SetAt("\0xd6", "Oe"); // ein �
	omReplaceMap.SetAt("\0xdc", "Ue"); // ein �
	omReplaceMap.SetAt("\0xdf", "ss"); // ein �
}

CCSBasic::~CCSBasic()
{
	omReplaceMap.RemoveAll();
}


CString CCSBasic::GetSortValue(CString opValue)
{

	CString olNew;	
	opValue.MakeLower();
	
	for(int i = opValue.GetLength() - 1; i >= 0; i--)
	{
		if(omReplaceMap.Lookup((LPCSTR)opValue.Mid(i,1), olNew) == TRUE)
		{
			opValue = opValue.Left(i) + olNew + opValue.Right(opValue.GetLength() - i - 1);
		}
	}

	return  opValue;
}
