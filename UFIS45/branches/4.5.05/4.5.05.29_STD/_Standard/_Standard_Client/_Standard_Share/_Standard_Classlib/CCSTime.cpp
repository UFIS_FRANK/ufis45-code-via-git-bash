//---------------------------------------------------------------------------------------
// Module:					CCSTime.cpp
// 
// Implementiation:			CCSTime-class
//	
// Modification History:	23.04.97  MWO / 24.10.97 ARE 
//							19.04.01  TVO / DateToHourDivString() changed
//---------------------------------------------------------------------------------------

#include <stdlib.h>
#include <stdafx.h>
#include <ctype.h>
#include <CCSTime.h>
#include <CCSDefines.h>

//---------------------------------------------------------------------------------------



void UtcToLocal(CTime &opUtc)
{
	if(opUtc != TIMENULL)
	{
		CTimeSpan olSpan(0,1,0,0);
		opUtc += olSpan;
	}
}

//---------------------------------------------------------------------------------------

void LocalToUtc(CTime &opLocal)
{
	if(opLocal != TIMENULL)
	{
		CTimeSpan olSpan(0,1, 0, 0);
		opLocal -= olSpan;
	}
}




bool CheckUtcPast(CTime &opTime, int ipDays)
{
	if(opTime == TIMENULL)
		return true;

	CTime olCurrTime = CTime::GetCurrentTime();

	LocalToUtc(olCurrTime);

	CTime olRefTime(olCurrTime.GetYear(), olCurrTime.GetMonth(), olCurrTime.GetDay(), 0, 0, 0);

	CTimeSpan olSpan(ipDays - 1);

	olRefTime -= olSpan;

	if(opTime >= olRefTime)
		return true;
	else
		return false;
}



bool DaysinPeriod(CTime olMinDate, CTime olMaxDate, CString &olDays)
{
	// mehr als ein durch eine Zahl repr�sentierter Wochentag: Tage sortieren
	// eine Wochenspanne oder mehr: Funktion liefert true zur�ck
	// Zeitspanne < eine Woche: alle Wochentage in der Spanne = true, ansonsten false;
	// keine Ziffer soll in CString doppelt vorkommen
	bool blRet = true;
	//char clSysnumber;
	//CString olDaysok;

	char buffer[64];

	CString olDaysIn;

	CTimeSpan olOneDay(1,0,0,0);

	CString olValid = CString("1234567");

	if ((olMaxDate < olMinDate) || (olDays.IsEmpty()))
			blRet = false;


	int i = 0;

	if (blRet == true)
	{

		while(olMinDate < olMaxDate && i < 7)
		{
			int iDaynumber = GetDayOfWeek(olMinDate); 

			sprintf(buffer, "%d", iDaynumber);

			olDaysIn += CString(buffer);

			olMinDate += olOneDay;
			i++;
		}


		for( i = 0; i < olDays.GetLength(); i++)
		{
			if(olValid.Find(olDays[i]) < 0)
			{
				blRet = false;
				break;
			}

			
			if(olDaysIn.Find(olDays[i]) < 0)
			{
				blRet = false;
				break;
			}

		}

	}

	olDays = olDaysIn;



		/*

		CTimeSpan olDiv  =  olMaxDate - olMinDate;

		int iDays = olDiv.GetDays()+1;	// Min bis einschl. Max

		if (iDays < 7)	
		{
			int iDaynumber = GetDayOfWeek(olMinDate); 
			 
			// pr�fen, ob Tage in Periode
			for(int Daycnt = 0; Daycnt < iDays; Daycnt++)
			{
				if (iDaynumber == 8)
					iDaynumber = 1;
				itoa(iDaynumber, &clSysnumber, 10);	
				if (olDays.Find(clSysnumber) == -1)	// not found
					blRet = false;
				else
					olDaysok += clSysnumber;

				iDaynumber++;
			}
		
		}

		// mehr als einer: Ziffern f�r Wochentage sortieren
		int iPos = 0;
		//if ((olDays->GetLength()) > 1 && (blRet == true))
		if (olDaysok.GetLength() > 0)
		{
			CString olCompare;
			for (int i = 1; i < 8; i++)
			{
				itoa(i, &clSysnumber, 10);	
				if (olDaysok.Find(clSysnumber) >= 0)
				{
					//olCompare.SetAt(iPos, clSysnumber);
					olCompare += clSysnumber;
					iPos++;
				}
			}
			if (olDays.GetLength() > olCompare.GetLength())
				blRet = false;		// es waren ung�ltige Eintr�ge dabei

			olDays = olCompare;

		}
	}
	*/
	return blRet;
}



int GetDayOfWeek(CTime &opDate)
{
	if(opDate == TIMENULL)
		return -1;
	int ilDay = opDate.GetDayOfWeek() + 6;
	if(ilDay > 7) ilDay -= 7;
	return ilDay;
}


bool GetDayOfWeek(CTime &opDate, char *pcpDest)
{
	if(pcpDest == NULL)
		return false;

	if(opDate == TIMENULL)
	{
		strcpy(pcpDest, "");
		return false;
	}
	int ilDay = opDate.GetDayOfWeek() + 6;
	if(ilDay > 7) ilDay -= 7;
	itoa(ilDay, pcpDest, 10);
	return true;
}



CString CTimeToDBString(CTime &opDateTime,CTime &opDate)
{
	if(opDateTime != TIMENULL)
		return opDateTime.Format("%Y%m%d%H%M%S");
	else
		return "              ";
}



CTime DBStringToDateTime(CString &opString)
{
	int ilYear, ilMonth, ilDay, ilHour, ilMin, ilSec;

	if(opString.GetLength() < 14)
		return TIMENULL;


	ilYear = atoi(opString.Left(4));
	ilMonth = atoi(opString.Mid(4,2));
	ilDay = atoi(opString.Mid(6,2));
	ilHour = atoi(opString.Mid(8,2));
	ilMin =  atoi(opString.Mid(10,2));
	ilSec =  atoi(opString.Mid(12,2));

	if(!((ilYear >= 1970) && (ilYear <= 2037)))
		return TIMENULL;

	if(!((ilMonth >= 1) && (ilMonth <= 12)))
		return TIMENULL;
	
	if(!((ilDay >= 1) && (ilDay <= 31)))
		return TIMENULL;
	
	if(!((ilHour >= 0) && (ilHour <= 23)))
		return TIMENULL;
	
	if(!((ilMin >= 0) && (ilMin <= 60)))
		return TIMENULL;
	
	if(!((ilSec >= 0) && (ilSec <= 60)))
		return TIMENULL;

	return  CTime::CTime(ilYear, ilMonth, ilDay, ilHour, ilMin, ilSec);
}

//---------------------------------------------------------------------------------------


//---------------------------------------------------------------------------------------
COleDateTime OleDateStringToDate(CString &opString)
{
	COleDateTime olOleTime;
	if(opString.IsEmpty())
	{
		olOleTime.SetStatus(COleDateTime::invalid);
		return olOleTime;
	}
	if ( !CheckOleDDMMYYValid( opString ) )
    {
		olOleTime.SetStatus(COleDateTime::invalid);
		return olOleTime;
	}
	int year = atoi( opString.Right(4) );
    int month = atoi( opString.Mid(3, 2) );
    int day = atoi( opString.Left(2) );
	
	return COleDateTime(year, month, day, 0, 0, 0);
}

CTime DateStringToDate(CString &opString)
{
	if(opString.IsEmpty())
	{
		return TIMENULL;
	}
	if ( !CheckDDMMYYValid( opString ) )
    {
		return TIMENULL;
	}
	int year = atoi( opString.Right(4) );
	if(year < 1970 || year > 2037)	return TIMENULL;
    int month = atoi( opString.Mid(3, 2) );
    int day = atoi( opString.Left(2) );
	
	if (year == 1970) day++;

	return CTime(year, month, day, 0, 0, 0);
}

//---------------------------------------------------------------------------------------

CTime HourStringToDate(CString &opString,bool blSet)
{
	return (HourStringToDate(opString,TIMENULL,blSet));
}

//---------------------------------------------------------------------------------------

int GetOffset(CString &opString)
{
	int ilOffset = 0;
	char clOperator = ' ';
	int ilDays = 0;

	int ilLength = opString.GetLength();
	if(opString[ilLength-2] == '-' || opString[ilLength-2] == '+')
	{
		if(isdigit(opString[ilLength-1]))  
		{
			clOperator = opString[ilLength-2];
			ilDays = atoi(opString.Right(1));
		}
		else
		{
			ilOffset = 0;
		}
	}
	else
		ilOffset = 0;

	return ilOffset;
}

//---------------------------------------------------------------------------------------

CTime HourStringToDate(CString &opString,CTime olDate,bool blSet)
{
	int year,month,day,hour,minute;
	int ilDays = 0;
	char clOperator = ' ';
	CString olString = opString;
	if(olString.IsEmpty() || olString.GetLength() < 4)
    {
		return TIMENULL;
    }

	int ilLength = olString.GetLength();
	int ilOpPos = -1;
	ilOpPos = olString.Find('-');
	if (ilOpPos !=-1)
		clOperator = '-';
	else
	{
		ilOpPos = olString.Find('+');
		if (ilOpPos !=-1)
			clOperator = '+';
	}

	if(ilOpPos != -1)
	{
		CString olOffset = olString.Right(ilLength-1 - ilOpPos);
		bool blDigit = true;
		for (int k = 0; k < olOffset.GetLength(); k++)
		{
			if( !isdigit(olOffset[k]) )  
				blDigit = false;

		}
		if(blDigit && blSet)  
		{
			ilDays = atoi(olOffset);
			olString = olString.Left(ilOpPos);
		}
		else
		{
			return TIMENULL;
		}
	}


	if(olString.Find('.') != -1)
	{
		olString.SetAt(olString.Find('.'),':');
		if(olString.Find('.') != -1) return TIMENULL;
	}

    if (!CheckHHMMValid(olString)) return TIMENULL;

	if(olDate == TIMENULL)
	{
		CTime olCurrentTime = CTime::GetCurrentTime();
		// Create a time object which is not bordering about the summer and winter time,
		//	because olTime + CTimeSpan(1,0,0,0) can result in the same day!
		CTime olWoDate(olCurrentTime.GetYear(), olCurrentTime.GetMonth(), olCurrentTime.GetDay(), 0, 0, 0, 0); //olCurrentTime.GetHour(), olCurrentTime.GetMinute(), olCurrentTime.GetSecond(), 0);
		if(clOperator == '+') olWoDate += CTimeSpan(ilDays*86400);
		if(clOperator == '-') olWoDate -= CTimeSpan(ilDays*86400);
		year = olWoDate.GetYear();
		month = olWoDate.GetMonth();
		day = olWoDate.GetDay();
	}
	else
	{
		// Create a time object which is not bordering about the summer and winter time,
		//	because olTime + CTimeSpan(1,0,0,0) can result in the same day!
		CTime olWoDate(olDate.GetYear(), olDate.GetMonth(), olDate.GetDay(), 0, 0, 0, 0); //olDate.GetHour(), olDate.GetMinute(), olDate.GetSecond(), 0);
		if(clOperator == '+') olWoDate += CTimeSpan(ilDays*86400);
		if(clOperator == '-') olWoDate -= CTimeSpan(ilDays*86400);
		year = olWoDate.GetYear();
		month = olWoDate.GetMonth();
		day = olWoDate.GetDay();
	}
    hour =	atoi(olString.Left(2));
    minute = atoi(olString.Right(2));
	return CTime(year, month, day, hour, minute, 00);


/*	int year,month,day,hour,minute;
	int ilDays = 0;
	char clOperator = ' ';
	CString olString = opString;
	if(olString.IsEmpty() || olString.GetLength() < 4)
    {
		return TIMENULL;
    }

	int ilLength = olString.GetLength();
	if(olString[ilLength-2] == '-' || olString[ilLength-2] == '+')
	{
		if(isdigit(olString[ilLength-1]) && blSet)  
		{
			clOperator = olString[ilLength-2];
			ilDays = atoi(olString.Right(1));
			olString = olString.Left(ilLength-2);
		}
		else
		{
			return TIMENULL;
		}
	}
	if(olString.Find('.') != -1)
	{
		olString.SetAt(olString.Find('.'),':');
		if(olString.Find('.') != -1) return TIMENULL;
	}

    if (!CheckHHMMValid(olString)) return TIMENULL;

	if(olDate == TIMENULL)
	{
		CTime olCurrentTime = CTime::GetCurrentTime();
		// Create a time object which is not bordering about the summer and winter time,
		//	because olTime + CTimeSpan(1,0,0,0) can result in the same day!
		CTime olWoDate(olCurrentTime.GetYear(), olCurrentTime.GetMonth(), olCurrentTime.GetDay(), 0, 0, 0, 0); //olCurrentTime.GetHour(), olCurrentTime.GetMinute(), olCurrentTime.GetSecond(), 0);
		if(clOperator == '+') olWoDate += CTimeSpan(ilDays*86400);
		if(clOperator == '-') olWoDate -= CTimeSpan(ilDays*86400);
		year = olWoDate.GetYear();
		month = olWoDate.GetMonth();
		day = olWoDate.GetDay();
	}
	else
	{
		// Create a time object which is not bordering about the summer and winter time,
		//	because olTime + CTimeSpan(1,0,0,0) can result in the same day!
		CTime olWoDate(olDate.GetYear(), olDate.GetMonth(), olDate.GetDay(), 0, 0, 0, 0); //olDate.GetHour(), olDate.GetMinute(), olDate.GetSecond(), 0);
		if(clOperator == '+') olWoDate += CTimeSpan(ilDays*86400);
		if(clOperator == '-') olWoDate -= CTimeSpan(ilDays*86400);
		year = olWoDate.GetYear();
		month = olWoDate.GetMonth();
		day = olWoDate.GetDay();
	}
    hour =	atoi(olString.Left(2));
    minute = atoi(olString.Right(2));
	return CTime(year, month, day, hour, minute, 00);
*/
}

//---------------------------------------------------------------------------------------

CTime DateHourStringToDate(CString &opDateString, CString &opHourString)
{
	/*RST
	CTime olDate,olHour;
	olDate = DateStringToDate(opDateString);
	olHour = HourStringToDate(opHourString);
	if(olDate == TIMENULL || olHour == TIMENULL) return TIMENULL;
	CTime olTime(CTime(olDate.GetYear(),olDate.GetMonth(),olDate.GetDay(),olHour.GetHour(),olHour.GetMinute(),00));
	*/
	
	CString olDateStr = 	opDateString;
	CString olHourStr = 	opHourString;

	CTime olDate,olHour;
	olDate = DateStringToDate(opDateString);
	olHour = HourStringToDate(opHourString);

	opDateString = olDateStr;
	opHourString = olHourStr;
	
	if(olDate == TIMENULL || olHour == TIMENULL) return TIMENULL;
	

	CTime olRet = HourStringToDate(opHourString, olDate , true);

	opDateString = olDateStr;
	opHourString = olHourStr;

	return olRet;
}

//---------------------------------------------------------------------------------------

CString DateToHourDivString(CTime &opDateTime,CTime &opDate)
{
	CString olHourDiv;
	if(opDateTime == TIMENULL || opDate == TIMENULL) return olHourDiv;


	//TVO CTime olDateTime = CTime(opDateTime.GetYear(),opDateTime.GetMonth(),opDateTime.GetDay(),0,0,0);
	//TVO CTime olDate	 = CTime(opDate.GetYear(),opDate.GetMonth(),opDate.GetDay(),0,0,0);


	// TVO 26.03.2001 START
	// because (26.03.2001 00:00 - 25.03.2001 00:00) == 23 Std !!!!! due to summer- / wintertime change
	CTime olDateTime = -1;
	CTime olDate = -1;

	if (opDateTime > opDate)
	{
		olDateTime = CTime(opDateTime.GetYear(),opDateTime.GetMonth(),opDateTime.GetDay(),1,0,0);

		if (opDate.GetYear() == 1970 && opDate.GetMonth() == 1 && opDate.GetDay() == 1)
			olDate = -1;
		else
			olDate	 = CTime(opDate.GetYear(),opDate.GetMonth(),opDate.GetDay(),0,0,0);
	}
	else
	{
		if (opDateTime.GetYear() == 1970 && opDateTime.GetMonth() == 1 && opDateTime.GetDay() == 1)
			olDateTime = -1;
		else
			olDateTime = CTime(opDateTime.GetYear(),opDateTime.GetMonth(),opDateTime.GetDay(),0,0,0);

		olDate	 = CTime(opDate.GetYear(),opDate.GetMonth(),opDate.GetDay(),1,0,0);
	}
	// TVO END

	CTimeSpan olDiv  =  olDateTime - olDate;

	if(olDiv.GetDays() > 0) 
		olHourDiv = opDateTime.Format("%H:%M") + "+" + olDiv.Format("%D");
 	else
	{
		if(olDiv.GetDays() == 0) 
			olHourDiv = opDateTime.Format("%H:%M"); 
 		else 
			olHourDiv = opDateTime.Format("%H:%M") + olDiv.Format("%D"); 
	}
	return olHourDiv;

/*

	CString olHourDiv;
	if(opDateTime == TIMENULL || opDate == TIMENULL) return olHourDiv;
	CTime olDateTime = CTime(opDateTime.GetYear(),opDateTime.GetMonth(),opDateTime.GetDay(),0,0,0);
	CTime olDate	 = CTime(opDate.GetYear(),opDate.GetMonth(),opDate.GetDay(),0,0,0);
	CTimeSpan olDiv  =  olDateTime - olDate;

	if(olDiv.GetDays() > 0) 
		olHourDiv = opDateTime.Format("%H:%M") + "+" + olDiv.Format("%D");
 	else
	{
		if(olDiv.GetDays() == 0) 
			olHourDiv = opDateTime.Format("%H:%M"); 
 		else 
			olHourDiv = opDateTime.Format("%H:%M") + olDiv.Format("%D"); 
	}
	return olHourDiv;
*/

}

/////////////////////////////////////////////////////////////////////////////
// DDX/DDV of CCS type 
//
// Check function. 
//---------------------------------------------------------------------------------------

BOOL CheckDateValid( CString str )
{          
    if( str.IsEmpty() ) return FALSE;
    if( str.GetLength() > 2 ) return FALSE;
    if( !isdigit(str.GetAt(0)) || !isdigit(str.GetAt(1)) ) return FALSE;
    int i = atoi( str );
    if( (i < 1) || (i > 31) ) return FALSE;
    return TRUE;
}

//---------------------------------------------------------------------------------------

BOOL CheckMonthValid( CString str )
{
    if( str.IsEmpty() ) return FALSE;
    if( str.GetLength() > 2 ) return FALSE;
    if( !isdigit(str.GetAt(0)) || !isdigit(str.GetAt(1)) ) return FALSE;
    int i = atoi( str );
    if( (i < 1) || (i > 12) ) return FALSE;
    return TRUE;
}

//---------------------------------------------------------------------------------------

BOOL CheckYearValid( CString str )
{       
    int i;
    
    if( str.IsEmpty() ) return FALSE;
    if( str.GetLength() > 4 ) return FALSE;
    for( i = 0; i < str.GetLength(); i++ ) 
        if( !isdigit(str.GetAt(i)) ) return FALSE;
    i = atoi( str );        
    if( str.GetLength() == 2 ) {
        i = ( i < 70 ) ? 2000 + i : 1900 + i;                
    }
    if( (i < 1970) || (i > 2070) ) return FALSE;
    return TRUE;
}

//---------------------------------------------------------------------------------------
    
BOOL CheckDDMMYYValid( CString &str )
{               
    int day = 0;
    int month = 0;
	int year  = 0;
	if( str.GetLength()  < 6) return FALSE;
    for(int i = 0; i < str.GetLength(); i++ ) 
        if((!isdigit(str.GetAt(i)) ) && (str.GetAt(i) != '.') )  return FALSE;
     
	// get the day
	int ilSeparatorPos1 = str.Find('.');
	if (ilSeparatorPos1 == -1)
	{
		// no Separator, format is DDMM or DDMMYY or DDMMYYYY
		day   = atoi( str.Left(2) );
		month = atoi( str.Mid(2, 2) );
		if(str.GetLength() == 8) year  = atoi( str.Right(4));
		else if(str.GetLength() == 6) year  = atoi( str.Right(2));
		else if(str.GetLength() == 4)
			{
				CTime olYear = CTime::GetCurrentTime();
				year = atoi(olYear.Format("%Y"));
			}
		else return FALSE;
	}
	else
	{
		// Separator, format is DD.MM.YY or DD.MM.YYYY
		int ilSeparatorPos2 = str.ReverseFind('.');
		if (ilSeparatorPos2 < 3) return FALSE;
		int ilMonthLen = ilSeparatorPos2-ilSeparatorPos1-1;
		if (ilMonthLen < 1)	return FALSE;
		day = atoi(str.Left(ilSeparatorPos1));
		month = atoi(str.Mid(ilSeparatorPos1+1,ilMonthLen));
		/********************+
		Diese Pruefung verursacht ein Y2K Problem,
		Jahr "00" wird zum aktuellem Jahr
		if((str.GetLength()-(ilSeparatorPos2+1))== 0)
		{
			CTime olYear = CTime::GetCurrentTime();
			year = atoi(olYear.Format("%Y"));

		}
		else
		{
		***********************/
			year = atoi(str.Right(str.GetLength()-(ilSeparatorPos2+1)));
//		}
	}

	if (year < 100)
		year = ( year < 70 ) ? 2000 + year : 1900 + year;

	if (year < 1970 || year > 2037)
		return FALSE;

    if( (year % 4) == 0 ) 
		g_dayInmonth[1] = 29;

	if(month < 1 || month > 12) 
		return FALSE;
    if ((day < 1) || ( day > g_dayInmonth[month - 1 ] )) 
		return FALSE;

	str.Format("%02d.%02d.%04d",day,month,year);

    return TRUE;
}    
BOOL CheckOleDDMMYYValid( CString &str )
{               
    int day = 0;
    int month = 0;
	int year  = 0;
	if( str.IsEmpty() ) return FALSE;
    for(int i = 0; i < str.GetLength(); i++ ) 
        if((!isdigit(str.GetAt(i)) ) && (str.GetAt(i) != '.') )  return FALSE;
     
	// get the day
	int ilSeparatorPos1 = str.Find('.');
	if (ilSeparatorPos1 == -1)
	{
		// no Separator, format is DDMM or DDMMYY or DDMMYYYY
		if( str.GetLength() >= 2 )		// 050406 MVy: entering just one char in some date edit field function CString::Left() crashes
			day = atoi( str.Left(2) );
		if( str.GetLength() > 4 )		// 050406 MVy: entering just one char in some date edit field function CString::Mid() crashes
			month = atoi( str.Mid(2, 2) );
		if(str.GetLength() == 8) year  = atoi( str.Right(4));
		else if(str.GetLength() == 6) year  = atoi( str.Right(2));
		else if(str.GetLength() == 4)
			{
				COleDateTime olYear = COleDateTime::GetCurrentTime();
				year = atoi(olYear.Format("%Y"));
			}
		else return FALSE;
	}
	else
	{
		// Separator, format is DD.MM.YY or DD.MM.YYYY
		int ilSeparatorPos2 = str.ReverseFind('.');
		if (ilSeparatorPos2 < 3) return FALSE;
		int ilMonthLen = ilSeparatorPos2-ilSeparatorPos1-1;
		if (ilMonthLen < 1)	return FALSE;
		day = atoi(str.Left(ilSeparatorPos1));
		month = atoi(str.Mid(ilSeparatorPos1+1,ilMonthLen));
		/***************************
		Diese Pruefung verursacht ein Y2K Problem,
		Jahr "00" wird zum aktuellem Jahr
		if((str.GetLength()-(ilSeparatorPos2+1))== 0)
		{
			COleDateTime olYear = COleDateTime::GetCurrentTime();
			year = atoi(olYear.Format("%Y"));

		}
		else
		{
		************************/
			year = atoi(str.Right(str.GetLength()-(ilSeparatorPos2+1)));
		//}
	}

	if (year < 100)
		year = ( year < 20 ) ? 2000 + year : 1900 + year;
    if( (year % 4) == 0 ) g_dayInmonth[1] = 29;
	if(month < 1 || month > 12) return FALSE;
    if((day < 1) || ( day > g_dayInmonth[month - 1 ] )) return FALSE;
	str.Format("%02d.%02d.%04d",day,month,year);
    return TRUE;
}    

//---------------------------------------------------------------------------------------

BOOL CheckDDMMValid( CString &str )
{               
    if( str.IsEmpty() ) return FALSE;
    if( str.GetLength() > 5 ) return FALSE;
    for(int i = 0; i < str.GetLength(); i++ ) 
        if( (!isdigit(str.GetAt(i))) && (str.GetAt(i) != '.') ) return FALSE;
                                  
  //  if( !CheckDateValid( str.Left(2) ) )    return FALSE; 
  //  if( !CheckMonthValid( str.Right(2) ) ) return FALSE; 
    
	int ilSeparatorPos = str.Find('.');
    int day = 0;
    int month = 0;
	if (ilSeparatorPos != -1)
	{
		if (ilSeparatorPos > 1)
		{
			day = atoi(str.Left(ilSeparatorPos-1));
		}
		month = atoi(str.Mid(ilSeparatorPos+1));
	}
	else
	{
		day   = atoi( str.Left(2) );
		month = atoi( str.Right(2) );
	}
    int year  = CTime::GetCurrentTime().GetYear();
    year = ( year < 70 ) ? 2000 + year : 1900 + year;
    if( (year % 4) == 0 ) g_dayInmonth[1] = 29;
	if(month < 1 || month > 12) return FALSE;
    if((day < 1) || ( day > g_dayInmonth[month - 1 ] )) return FALSE;
	str.Format("%02d.%02d",day,month);
    return TRUE;
}    

//---------------------------------------------------------------------------------------

BOOL CheckHHMMValid( CString &str )
{
    int hour = 0;
    int min = 0;
	if( str.IsEmpty() ) return FALSE;
    for( int i = 0; i < str.GetLength(); i++ ) 
        if( (!isdigit(str.GetAt(i))) && (str.GetAt(i) != ':') ) return FALSE;
	int ilSeparatorPos = str.Find(':');
	if (ilSeparatorPos != -1 && (ilSeparatorPos != 2 || str.GetLength() != 5)) return FALSE;
	if (ilSeparatorPos == -1 &&  str.GetLength() != 4) return FALSE;
	hour  = atoi( str.Left(2) );
	min   = atoi( str.Right(2) );
	if( (hour < 0) || (hour > 23 ) ) return FALSE; 
    if( (min < 0) || (min > 59 ) )   return FALSE;
	char pclBuf[82];
	sprintf(pclBuf,"%02d:%02d",hour,min);
	str = pclBuf;
    return TRUE;
}
    
/////////////////////////////////////////////////////////////////////////////
// DDX/DDV function.
//---------------------------------------------------------------------------------------
  
void WINAPI DDX_CCSDate( CDataExchange *pDX, int nIDC, CTime& tm )
{
    CString val;

    pDX->PrepareEditCtrl(nIDC);    
    if (pDX->m_bSaveAndValidate)
    {
        pDX->m_pDlgWnd->GetDlgItem(nIDC)->GetWindowText(val);
        if( !CheckDateValid( val ) ) {
            pDX->m_pDlgWnd->MessageBox("Please enter a valid Date.\n01...31");
            pDX->Fail();
        }
        tm = CTime( tm.GetYear(), 
                    tm.GetMonth(), 
                    atoi(val), 
                    tm.GetHour(),
                    tm.GetMinute(), 
                    tm.GetSecond());
    }
    else
    {
        val = tm.Format( "%d" );
        pDX->m_pDlgWnd->GetDlgItem( nIDC )->SetWindowText( val );
    }
}  

//---------------------------------------------------------------------------------------

void WINAPI DDX_CCSMonth( CDataExchange *pDX, int nIDC, CTime& tm )
{
    CString val;

    pDX->PrepareEditCtrl(nIDC);    
    if (pDX->m_bSaveAndValidate)
    {
        pDX->m_pDlgWnd->GetDlgItem(nIDC)->GetWindowText(val);
        if( !CheckMonthValid( val ) ) {
            pDX->m_pDlgWnd->MessageBox("Please enter a valid Month.\n01...12");
            pDX->Fail();
        }
        tm = CTime( tm.GetYear(), 
                    atoi(val), 
                    tm.GetDay(), 
                    tm.GetHour(),
                    tm.GetMinute(), 
                    tm.GetSecond());

        
    }
    else
    {
        val = tm.Format( "%m" );
        pDX->m_pDlgWnd->GetDlgItem( nIDC )->SetWindowText( val );
    }
}

//---------------------------------------------------------------------------------------

void WINAPI DDX_CCSYear( CDataExchange *pDX, int nIDC, CTime& tm )
{
    CString val;

    pDX->PrepareEditCtrl(nIDC);    
    if (pDX->m_bSaveAndValidate)
    {
        pDX->m_pDlgWnd->GetDlgItem(nIDC)->GetWindowText(val);
        if( !CheckYearValid( val ) ) {
            pDX->m_pDlgWnd->MessageBox("Please enter a valid Year.");
            pDX->Fail();
        }
		int ilYearOfset = atoi(val) < 1900 ? 1900 : 0;
        tm = CTime( atoi(val) + ilYearOfset, 
                    tm.GetMonth(),
                    tm.GetDay(), 
                    tm.GetHour(),
                    tm.GetMinute(), 
                    tm.GetSecond());

    }
    else
    {
        val = tm.Format( "%Y" );
        pDX->m_pDlgWnd->GetDlgItem( nIDC )->SetWindowText( val );
    }
}

//---------------------------------------------------------------------------------------

void WINAPI DDX_CCSddmmyy( CDataExchange *pDX, int nIDC, CTime& tm )
{ 
    CString val;

    pDX->PrepareEditCtrl(nIDC);
    if (pDX->m_bSaveAndValidate)
    {
        pDX->m_pDlgWnd->GetDlgItem(nIDC)->GetWindowText(val);
        if ( !CheckDDMMYYValid( val ) )
        {
            pDX->m_pDlgWnd->MessageBox("Please enter a valid value.\nDDMMYY (ex. 01.02.96)");
            pDX->Fail();
			tm = CTime( -1 );
			return;
        }

		
        int year = atoi( val.Right(4) );
        int month = atoi( val.Mid(3, 2) );
        int day = atoi( val.Left(2) );
		
		if (year == 1970)
			day++;

        tm = CTime(  year, 
                    month, 
                    day, 0, 0, 0);
                    //tm.GetHour(),
                    //tm.GetMinute(), 
                    //tm.GetSecond());      

    }
    else
    {
        val = tm.Format( "%d.%m.%Y" );
        pDX->m_pDlgWnd->GetDlgItem( nIDC )->SetWindowText( val );
    }
}  

//---------------------------------------------------------------------------------------

void WINAPI DDX_CCSddmm( CDataExchange *pDX, int nIDC, CTime& tm )
{ 
    CString val;

    pDX->PrepareEditCtrl(nIDC);
    if (pDX->m_bSaveAndValidate)
    {
        pDX->m_pDlgWnd->GetDlgItem(nIDC)->GetWindowText(val);
        if ( !CheckDDMMValid( val ) )
        {
            pDX->m_pDlgWnd->MessageBox("Please enter a valid value.\nDDMM (ex. 21.04)");
            pDX->Fail();
        }

        int month = atoi( val.Right(2) );
        int day = atoi( val.Left(2) );

        tm = CTime( CTime::GetCurrentTime().GetYear(),
                    month, 
                    day, 0, 0, 0);
                    //tm.GetHour(),
                    //tm.GetMinute(), 
                    //tm.GetSecond());      

    }
    else
    {
        val = tm.Format( "%d.%m" );
        pDX->m_pDlgWnd->GetDlgItem( nIDC )->SetWindowText( val );
    }
}  

//---------------------------------------------------------------------------------------

void WINAPI DDX_CCSTime( CDataExchange *pDX, int nIDC, CTime& tm )
{
    CString val;

    pDX->PrepareEditCtrl(nIDC);
    if (pDX->m_bSaveAndValidate)
    {
        pDX->m_pDlgWnd->GetDlgItem(nIDC)->GetWindowText(val);
        if ( !CheckHHMMValid( val ) )
        {
            pDX->m_pDlgWnd->MessageBox("Please enter a valid Time.\nHHMM (ex. 23:59)");
            pDX->Fail();
        }

        int hour = atoi( val.Left(2) );
        int minute = atoi( val.Right(2) );

        tm = CTime( CTime::GetCurrentTime().GetYear(),
                    CTime::GetCurrentTime().GetMonth(),
                    CTime::GetCurrentTime().GetDay(),
                    hour, minute, 0);
    }
    else
    {
        val = tm.Format( "%H:%M" );
        pDX->m_pDlgWnd->GetDlgItem( nIDC )->SetWindowText( val );
    }
}

//---------------------------------------------------------------------------------------

void AFXAPI DDX_CCSFString(CDataExchange* pDX, int nIDC, CString& value)
{
	pDX;
	value;
	/*HWND hWndCtrl = */pDX->PrepareEditCtrl(nIDC);
	/*if (pDX->m_bSaveAndValidate)
	{
		int nLen = ::GetWindowTextLength(hWndCtrl);
		//::GetWindowText(hWndCtrl, value.GetBufferSetLength(nLen), nLen+1);
		//value.ReleaseBuffer();
	}
	else
	{
        //pDX->m_pDlgWnd->GetDlgItem( nIDC )->SetWindowText( value );
	}*/
}

//---------------------------------------------------------------------------------------

