#ifndef _CEDASYSTABDATA_H_
#define _CEDASYSTABDATA_H_
 
#include <afxtempl.h>
#include <CCSCedaData.h>


struct SYSTABDATA
{
	char	Urno[12];		// Internal not used! Only generated for the systab-table
	char	Tana[34];		// Tablename
	char	Fina[18];		// Fieldname
	char	Fity[6];		// Fieldtyp
	int		Fele;			// Tabellenname
	char	Msgt[82];		// Messagenumber
	char	Addi[1026];		// Additional Information
	char	Type[6];		// Datum,Flightnumber
    char	Refe[50];		// Querbeziehungen AFT<>Basisdaten
	char	Datr[50];		// Querbeziehungen SDB<>Basisdaten
	char	Indx[2];		// Art des Index N(keiner) I(Index) U(Unique)
	char	Defa[66];		// Initialwerte
	char	Taty[12];		// Tabletyp
	char	Syst[2];		// Ins Shared Memory
	char	Proj[130];		// Projectlist
	char	Stat[34];		// AFT Standartfeld?
	char	Depe[66];		// Abh�ngige Felder
	char	Guid[22];		// GUI Eingenschaften
	char	Sort[6];		// Pro Tabelle aufsteigende Nummer
	char	Cont[34];		// Feld enth�lt welche Felder
	char	Labl[66];		// GUI:Label vor dem Feld
	char	Fmts[22];		// Formatstring
	char	Gtyp[34];		// GUI Controltyp
	char	Gsty[258];		// GUI Window Style
	char	Glfl[66];		// GUI List Filter
	char	Gdsr[34];		// GUI Data Source
	char	Gcfl[34];		// GUI Control Constraint Fields
	char	Orie[4];		// Orientation
	char	Reqf[2];		// Required Field
	char	Kont[130];		// Kontextsensitive Hilfe
	char	Logd[34];		// Log-Destination, Tabellenname


	SYSTABDATA(void) 
	{ memset(this,'\0',sizeof(*this));	strcpy(Syst, "N"); strcpy(Reqf, "N");
										strcpy(Indx, "N"); strcpy(Fity, "C");
										Fele = 0;          strcpy(Sort, "0"); }

};


/////////////////////////////////////////////////////////////////////////////
// Class declaration


class CedaSysTabData: public CCSCedaData
{
// Attributes
public:

    CCSPtrArray<SYSTABDATA> omData;
	CMapStringToPtr			omTables;

// Operations
public:
    CedaSysTabData();
 	~CedaSysTabData();

	void SetTableExtension(CString opExtension);

	char pcmFlist[1000];
	
	void ClearAll(void);
	
	bool Read(char *pspWhere, bool bpSM = true, char *pcpFieldList = NULL);

	//bool AddDef(SYSTABDATA *prpSysTab, CString opToProj = "", CString opToTana = "", CString opToFina = "");
	
	bool AddDef(SYSTABDATA *prpSysTab);
	bool Read(SYSTABDATA *prpSysTab);
	void GetAllTableNames(CStringArray &ropTables);
	int GetAllFieldsForTable(CCSPtrArray<SYSTABDATA> &ropArray, CString opTabName);
};


#endif
