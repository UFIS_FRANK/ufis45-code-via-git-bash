#ifndef __UFIS_ODBC__
#define __UFIS_ODBC__

#include <SQL.h>
#include <RecordSet.h>
#include <CCSPtrArray.h>

class UfisOdbc
{
public:
	UfisOdbc(CString opConnectionString);
	~UfisOdbc();

	HENV	hEnv;
	HDBC	hDbConn;
	HSTMT	hStmt;

	CString omConnectionString;
	bool isOdbcInit;
	CString omXUser;

	CStringArray omDataArray;

	CString omLastErrorMessage;

	void SetConnect(CString opConnect)
	{
		omConnectionString = opConnect;
	}
	int CallDB(CString opAction, 
			   CString opTable, 
		       CString opFields, 
		       CString opWhere, 
		       CString opValues,
			   CCSPtrArray<RecordSet> *popData = NULL);

	void RemoveInternalData(){omDataArray.RemoveAll();}

	bool GetManyUrnos(int ipCount, CUIntArray &opUrnos);

	int GetLineCount();

	int GetLine(int ipLineNo, CStringArray &ropValues);

	void MakeClientString(CString &ropText);

};

#endif //__UFIS_ODBC__