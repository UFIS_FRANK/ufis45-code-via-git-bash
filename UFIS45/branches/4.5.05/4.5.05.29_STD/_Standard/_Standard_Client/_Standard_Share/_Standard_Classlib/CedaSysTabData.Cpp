// csystabd.cpp - Read systab data
// 

#include <stdafx.h>
#include <CedaSysTabData.h>
#include <ccsddx.h>
#include <CcsDefines.h>



CedaSysTabData::CedaSysTabData()
{
    /* Create an array of CEDARECINFO for SYSTABDATA*/
    BEGIN_CEDARECINFO(SYSTABDATA, SysTabDataRecInfo)
		FIELD_CHAR_TRIM(Urno,"URNO")
		FIELD_CHAR_TRIM(Tana,"TANA")
		FIELD_CHAR_TRIM(Fina,"FINA")
		FIELD_CHAR_TRIM(Fity,"FITY")
		FIELD_INT(Fele,"FELE")
		FIELD_CHAR_TRIM(Msgt,"MSGT")
		FIELD_CHAR_TRIM(Addi,"ADDI")
		FIELD_CHAR_TRIM(Type,"TYPE")
		FIELD_CHAR_TRIM(Refe,"REFE")
		FIELD_CHAR_TRIM(Datr,"DATR")
		FIELD_CHAR_TRIM(Indx,"INDX")
		FIELD_CHAR_TRIM(Defa,"DEFA")
		FIELD_CHAR_TRIM(Taty,"TATY")
		FIELD_CHAR_TRIM(Syst,"SYST")
		FIELD_CHAR_TRIM(Proj,"PROJ")
		FIELD_CHAR_TRIM(Stat,"STAT")
		FIELD_CHAR_TRIM(Depe,"DEPE")
		FIELD_CHAR_TRIM(Guid,"GUID")
		FIELD_CHAR_TRIM(Sort,"SORT")
		FIELD_CHAR_TRIM(Cont,"CONT")
		FIELD_CHAR_TRIM(Labl,"LABL")
		FIELD_CHAR_TRIM(Fmts,"FMTS")
		FIELD_CHAR_TRIM(Gtyp,"GTYP")
		FIELD_CHAR_TRIM(Gsty,"GSTY")
		FIELD_CHAR_TRIM(Glfl,"GLFL")
		FIELD_CHAR_TRIM(Gdsr,"GDSR")
		FIELD_CHAR_TRIM(Gcfl,"GCFL")
		FIELD_CHAR_TRIM(Orie,"ORIE")
		FIELD_CHAR_TRIM(Reqf,"REQF")
		FIELD_CHAR_TRIM(Kont,"KONT")
		FIELD_CHAR_TRIM(Logd,"LOGD")
	END_CEDARECINFO

	// Copy the record structure
    for (int i = 0; i < sizeof(SysTabDataRecInfo)/sizeof(SysTabDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&SysTabDataRecInfo[i],sizeof(CEDARECINFO));
        omRecInfo.Add(prpCedaRecInfo);
	}


    // Initialize table names and field names
    strcpy(pcmTableName,"SYSHAJ");
	
	strcpy(pcmFlist,"URNO,TANA,FINA,FITY,FELE,MSGT,ADDI,TYPE,REFE,DATR,"
						   "INDX,DEFA,TATY,SYST,PROJ,STAT,DEPE,GUID,SORT,"
						   "CONT,LABL,FMTS,GTYP,GSTY,GLFL,GDSR,GCFL,ORIE,"
						   "REQF,KONT,LOGD");

	pcmFieldList = pcmFlist;
	//omData.SetSize(0,1000);
}



CedaSysTabData::~CedaSysTabData(void)
{
	ClearAll();
}


void CedaSysTabData::SetTableExtension(CString opExtension)
{
	strcpy(pcmTableName,"SYS");
	strcat(pcmTableName,opExtension);
}



void CedaSysTabData::ClearAll(void)
{
	for (POSITION pos = omTables.GetStartPosition(); pos != NULL; )
	{
		CString olTabName;
		CCSPtrArray<SYSTABDATA>	*polValue = NULL;
		omTables.GetNextAssoc(pos,olTabName,(void *&)polValue);
		delete polValue;			
	}

	omTables.RemoveAll();
	omData.DeleteAll();
}



bool CedaSysTabData::Read(char *pspWhere, bool bpSM, char *pcpFieldList)
{
	bool ilRc = true;
	char pclOldPCMFieldList[512]="";

	ClearAll();
	
	bool blRet = true;

	char pclFieldList[2000];

	strcpy(pclOldPCMFieldList, pcmFieldList);

	if(pcpFieldList != NULL)
	{
		strcpy(pclFieldList, pcpFieldList);
		strcpy(pcmFieldList, pcpFieldList);
	}
	else
	{
		strcpy(pclFieldList, pcmFieldList);
	}


	//int ilL = imL
	if(bpSM)
	{
		if (CedaAction2("RT", pspWhere, "", "")  == false)
		{
			blRet = false;
		}
	}
	else
	{
		if (CedaAction2("RTA", pspWhere, "", "")  == false)
		//if (CedaAction2("RTA","SYS",pclFieldList,pspWhere,"",pcgDataBuf) == false)
		{
			blRet = false;
		}
	}

	if(blRet)
	{
		for (int ilLc = 0; ilRc == true; ilLc++)
		{
			SYSTABDATA *prlSys = new SYSTABDATA;
			if ((ilRc = GetBufferRecord2(ilLc,prlSys, CString(pclFieldList))) == true)
			{
				omData.Add(prlSys);
				CCSPtrArray<SYSTABDATA>	*polValue = NULL;
				if (omTables.Lookup(prlSys->Tana, (void *&)polValue) == 0)
				{
					polValue = new CCSPtrArray<SYSTABDATA>;
					omTables.SetAt(prlSys->Tana,polValue);
				}

				polValue->Add(prlSys);
			}
			else
				delete prlSys;
		}


		if(omData.GetSize() == 0)
			blRet = false;
		else
			blRet = true;
	}

	strcpy(pcmFieldList, pclOldPCMFieldList);

	return blRet;

}


void CedaSysTabData::GetAllTableNames(CStringArray &ropTables)
{
	POSITION rlPos;
	for ( rlPos = omTables.GetStartPosition(); rlPos != NULL; )
	{
		CCSPtrArray<SYSTABDATA>	*polValue = NULL;
		CString olTableName;
		omTables.GetNextAssoc(rlPos, olTableName, (void *&)polValue);
		ropTables.Add(olTableName);
	}
}

int CedaSysTabData::GetAllFieldsForTable(CCSPtrArray<SYSTABDATA> &ropArray, CString opTabName)
{
	CCSPtrArray<SYSTABDATA>	*polValue = NULL;
	if (omTables.Lookup(opTabName,(void *&)polValue))
	{
		ropArray.Append(*polValue);
	}

	return ropArray.GetSize();
}





