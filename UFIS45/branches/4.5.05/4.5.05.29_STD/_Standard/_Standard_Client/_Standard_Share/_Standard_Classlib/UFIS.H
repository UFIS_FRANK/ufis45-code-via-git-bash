#ifndef _UFIS_H_
#define _UFIS_H_

// ufis.h - DLL functions in "UFIS.DLL"
//
// Description:
// This file just simply tells us the function prototypes contained in the
// external library UFIS.DLL which must be linked to the application by
// UFIS.LIB (which generated from the IMPLIB program).
//
//
// Written by:
// Damkerng Thammathakerngkit   April 5, 1995

#include <afxwin.h>

extern "C" int WINAPI InitCom(LPSTR HostType, LPSTR CedaHost);
extern "C" int WINAPI CallCeda(const LPSTR req_id, const LPSTR dest1, const LPSTR dest2,
      const LPSTR cmd, const LPSTR object, const LPSTR seq, const LPSTR tws, const LPSTR twe,
      const LPSTR selection, const LPSTR fields, const LPSTR data, const LPSTR data_dest);
extern "C" int WINAPI CleanupCom(void);

extern "C" int WINAPI UfisDllAdmin(const LPSTR parm1, const LPSTR parm2, const LPSTR parm3);

extern "C" int WINAPI GetBufferHandle(LPSTR buf_slot, HGLOBAL *pHandle);

// GetResultBuffer(..) is here for temporary using and testing purpose (because it is
// very SLOW!! -- according to Manfred). It should be removed when the actual buffer
// accessing routine in CEDADATA.CPP is completed.
//
extern "C" int WINAPI GetResultBuffer(LPSTR pResult, int buflen, LPSTR buf_slot,
      int line_no, int line_cnt, LPSTR item_list);
extern "C" int WINAPI GetBc (char * req_id,char * dest1, char * dest2, char * cmd, char * object,
	          char * seq, char * tws, char * twe, char * selection, char * fields,
	          char * data, char * bc_num) ;
extern "C" int WINAPI RegisterBcWindow(HWND hlBcWindow);
extern "C" int WINAPI UnRegisterBcWindow(HWND hlBcWindow);
extern "C" int WINAPI GetWorkstationName (LPSTR ws_name);
extern "C" int WINAPI GetNumberOfLines (char  *buf_slot);
extern "C" int WINAPI GetActualBcNum ();
extern "C" int WINAPI GetFirstBcNum ();
extern "C" int WINAPI GetLostBcCount();
extern "C" int WINAPI GetRealWorkstationName (LPSTR ws_name);

#endif
