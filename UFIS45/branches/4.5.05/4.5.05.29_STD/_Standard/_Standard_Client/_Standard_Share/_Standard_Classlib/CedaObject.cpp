// CedaObject.cpp: implementation of the CedaObject class.
//
//////////////////////////////////////////////////////////////////////

#include <stdafx.h>
#include <CedaObject.h>
#include <CedaSysTabData.h>
#include <CCSCedaData.h>
#include <CCSDefines.h>
#include <CCSBasicFunc.h>

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif


#define FINA_IDX 0
#define TYPE_IDX 1
#define FELE_IDX 2
#define FITY_IDX 3
#define SYST_IDX 4
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

#define MAX_BC_BUFFER_SIZE  1000


//hag20030422 CStringArray CedaObject::smOrder;
//hag20030422 CUIntArray CedaObject::imIndex;
CStringArray* CedaObject::psmOrder=0;
CUIntArray* CedaObject::pimIndex=0;
bool* CedaObject::pbmNumericSort=false;


CCSBasic *CedaObject::pomBasic = NULL;
CedaSysTabData *CedaObject::pomSystab = NULL;	

char CedaObject::SystabErrorMsg[256] = "Unable to read Systab (%s). \nPlease consult your System Administrator.";
char CedaObject::SystabErrorCaptionMsg[64] = "Fatal error";


CedaObject::CedaObject(CString opAlias, CString opObject, CString opTableExtension, CCSBasic *popBasic)
{
	Alias = opAlias;
	omTableExtension = opTableExtension;
	pomBasic = popBasic;
	Object = opObject;
//	bmFieldsInSM = true;
	bmFieldsInSM = false;
	FieldCount = 0;
	imLogicalFieldCount = 0;
	bmIsDataRead = false;
	bmIsReadAll = false;
	BcBufferSize = 0;
	pomBcFunc	 = NULL;	
	bmCdat = false;
	bmLstu = false;
	bmUsec = false;
	bmUseu = false;
	bmNumericSort = false;
}


CedaObject::~CedaObject()
{
	Data.DeleteAll();
	Desc.DeleteAll();

	CMapStringToPtr *prlKeyMap;
	POSITION pos, pos2;
	CString olTmp;
	CCSPtrArray<RecordSet> *prlRecords;
	
	for( pos = omKeyMaps.GetStartPosition(); pos != NULL; )
	{
		omKeyMaps.GetNextAssoc( pos, olTmp, (void *&)prlKeyMap );

		for( pos2 = prlKeyMap->GetStartPosition(); pos2 != NULL; )
		{
			prlKeyMap->GetNextAssoc( pos2, olTmp, (void *&)prlRecords );
			prlRecords->RemoveAll();
			delete prlRecords;
		}
		prlKeyMap->RemoveAll();
		delete prlKeyMap;
	}
	omKeyMaps.RemoveAll();
	HeaderMap.RemoveAll();
	omFieldSetMap.RemoveAll();
	omIndexMap.RemoveAll();
}


CString CedaObject::GetTableName()
{
	return Object;
}


CedaObject::CedaObject(const CedaObject& s)
{
	Object = s.Object;

/*	
	for(int i = 0; i < s.Values.GetSize(); i++)
	{
		Values.Add(s.Values[i]);
	}
*/
}


const CedaObject& CedaObject::operator= ( const CedaObject& s)
{
	if(this == &s)
	{
		return *this;
	}
	Object = s.Object;

/*
	for(int i = 0; i < s.Values.GetSize(); i++)
	{
		Values.Add(s.Values[i]);
	}
*/
	return *this;

}


void CedaObject::SetBroadcastCallback(CALLBACKFUNC pfpFunc)
{
	pomBcFunc = pfpFunc;
}

CALLBACKFUNC CedaObject::GetBroadcastCallback()
{
	return pomBcFunc;
}

bool CedaObject::SetLogicalFields(CCSPtrArray<FieldSet> opFieldSet)
{
	FieldSet *prlFieldSet;
	for(int i = 0; i < opFieldSet.GetSize(); i++)
	{
		prlFieldSet = new FieldSet;
		prlFieldSet->Field  = opFieldSet[i].Field;
		prlFieldSet->Type   = opFieldSet[i].Type;
		prlFieldSet->DBType = opFieldSet[i].DBType;
		prlFieldSet->Length = opFieldSet[i].Length;

		Desc.Add(prlFieldSet);
		omFieldSetMap.SetAt((LPCSTR)prlFieldSet->Field, (void *)prlFieldSet);
		omIndexMap.SetAt((LPCSTR)prlFieldSet->Field, (void *)i/*FieldCount*/);
		FieldCount++;
		imLogicalFieldCount++;
	}
	return true;
}





bool CedaObject::Init(CString opFieldList, UfisOdbc *popOdbc /*= NULL*/)
{
	bmIsLogicObject = false;

	// check, if we must create an Systab object and load the SYSTAB
	if (pomSystab == NULL)
	{
		pomSystab = new CedaSysTabData();
		pomSystab->SetTableExtension(omTableExtension);

		pomSystab->Read("ORDER BY TANA",true,"TANA,FINA,TYPE,FELE,FITY,SYST");
	};	

	CCSPtrArray<SYSTABDATA> olData;
	pomSystab->GetAllFieldsForTable(olData,Object);

	bool blAllFields = false;	

	if(opFieldList.IsEmpty())
	{
		blAllFields = true;
		bmFieldsInSM = false;
	}

	if( popOdbc == NULL) // normal CEDA connection
	{
		FieldSet  *prlFieldSet;

		for(int i = olData.GetSize() - 1; i >= 0; i--)
		{

			if(strcmp(olData[i].Fina, "CDAT") == 0 )
				bmCdat = true;
			if(strcmp(olData[i].Fina, "LSTU") == 0 )
				bmLstu = true;
			if(strcmp(olData[i].Fina, "USEC") == 0 )
				bmUsec = true;
			if(strcmp(olData[i].Fina, "USEU") == 0 )
				bmUseu = true;


			if(blAllFields || opFieldList.Find(olData[i].Fina) >= 0)
			{
					if(bmFieldsInSM && (strcmp(olData[i].Syst, "N" ) == 0))
						bmFieldsInSM = false;

					prlFieldSet = new FieldSet;
					prlFieldSet->Field = CString(olData[i].Fina);
					prlFieldSet->Type = CString(olData[i].Type);
					prlFieldSet->DBType = CString(olData[i].Fity);
					prlFieldSet->Length = olData[i].Fele;

					Desc.Add(prlFieldSet);
					omFieldSetMap.SetAt((LPCSTR)prlFieldSet->Field, (void *)prlFieldSet);
					omIndexMap.SetAt((LPCSTR)prlFieldSet->Field, (void *)FieldCount);
					FieldCount++;
			}
		}

		int ilUrnoI;
		if(omIndexMap.Lookup("URNO", (void *&)ilUrnoI) == TRUE)
		{
			AddKeyMap("URNO");
		}

	/*
		olTime = CTime::GetCurrentTime();
		TRACE("\nend    ReadSystab  <%s>  %s\n\n", Object,olTime.Format("%H:%M:%S"));
	*/	
		
		return true;
	}
	else // connection is ODBC
	{
		CString olTab;
		olTab = "SYS" + omTableExtension;
		CString olWhere;
		olWhere.Format("WHERE TANA='%s'", Object);
		if(popOdbc->CallDB("Select", olTab, "FINA,TYPE,FELE,FITY,SYST", olWhere, "") == 0)
		{
			FieldSet  *prlFieldSet;

			for(int i = popOdbc->GetLineCount() - 1; i >= 0; i--)
			{
				CStringArray olValues;
				CString olFina;
				popOdbc->GetLine(i, olValues);
				olFina = olValues[FINA_IDX];

				if(olValues[FINA_IDX] == "CDAT") 
					bmCdat = true;
				if(olValues[FINA_IDX] == "LSTU")
					bmLstu = true;
				if(olValues[FINA_IDX] == "USEC")
					bmUsec = true;
				if(olValues[FINA_IDX] == "USEU")
					bmUseu = true;

	
				if(blAllFields || opFieldList.Find(olValues[FINA_IDX]) >= 0)
				{
					prlFieldSet = new FieldSet;
					prlFieldSet->Field = CString(olValues[FINA_IDX]);
					prlFieldSet->Type = CString(olValues[TYPE_IDX]);
					prlFieldSet->DBType = CString(olValues[FITY_IDX]);
					prlFieldSet->Length = atoi(olValues[FELE_IDX]);

					Desc.Add(prlFieldSet);
					omFieldSetMap.SetAt((LPCSTR)prlFieldSet->Field, (void *)prlFieldSet);
					omIndexMap.SetAt((LPCSTR)prlFieldSet->Field, (void *)FieldCount);
					FieldCount++;
				}
			}

			int ilUrnoI;
			if(omIndexMap.Lookup("URNO", (void *&)ilUrnoI) == TRUE)
			{
				AddKeyMap("URNO");
			}
		}
		popOdbc->RemoveInternalData();
	}
	return true;
}

bool CedaObject::Init(CCSPtrArray<FieldSet> opFieldSet)
{
	bmIsLogicObject = true;
//	for(int i = opFieldSet.GetSize() - 1; i >= 0; i--)
	//  hag000706: Vorgehen f�hrte dazu, da� beim Clonen von Tabellen in Desc die Reihenfolge
	//  der FieldSets unterschiedlich war -> Fehler bei SetField
	for(int i = 0; i<opFieldSet.GetSize() ; i++)
	{
		FieldSet *prlFieldSet = new FieldSet;
		prlFieldSet->Field = opFieldSet[i].Field;
		prlFieldSet->Type = opFieldSet[i].Type;
		prlFieldSet->DBType = opFieldSet[i].DBType;
		prlFieldSet->Length = opFieldSet[i].Length;

		prlFieldSet->omPossibleValues = opFieldSet[i].omPossibleValues;
		Desc.Add(prlFieldSet);
		omFieldSetMap.SetAt((LPCSTR)prlFieldSet->Field, (void *)prlFieldSet);
		omIndexMap.SetAt((LPCSTR)prlFieldSet->Field, (void *&)i);
		FieldCount++;
	}

	//May be there is a logical URNO
	int ilUrnoI;
	if(omIndexMap.Lookup("URNO", (void *&)ilUrnoI) == TRUE)
	{
		AddKeyMap("URNO");
	}

	return true;
}


int CedaObject::GetMaxFieldLength(CString opField)
{
	int ilIndex;
	int ilRet = -1;
	if(omIndexMap.Lookup(opField, (void *&)ilIndex) == TRUE)
	{
		ilRet = Desc[ilIndex].Length;
	}
	return ilRet;
}

//MWO 13.03.200 
void CedaObject::GetFieldSetInfo(CStringArray &ropDBTypes, CStringArray &ropFields)
{
	int ilCount = Desc.GetSize();
	CString olTmp = "CDAT,LSTU,USEC,USEU";

	for(int i = 0; i < FieldCount - imLogicalFieldCount; i++)
	{
		if(olTmp.Find(Desc[i].Field) < 0)
		{
			ropDBTypes.Add(Desc[i].DBType);
			ropFields.Add(Desc[i].Field);
		}
	}
}

CString CedaObject::MakeODBCString(CString opData)
{
	CStringArray olDataArr;
	CStringArray olDBTypes;
	CStringArray olFields;
	CString olRealDataList;
	int ilC = ExtractItemList(opData, &olDataArr, ',');
	GetFieldSetInfo(olDBTypes, olFields);
	if(ilC == olDBTypes.GetSize() && ilC == olFields.GetSize())
	{
		for(int i = 0; i < ilC; i++)
		{
			if(olDBTypes[i] == "C" || olDBTypes[i] == "VC2")
				olRealDataList += CString("'") + olDataArr[i] + CString("'");
			if(olDBTypes[i] == "N")//Number
				olRealDataList += olDataArr[i];
			if((i+1) < ilC)
			{
				olRealDataList += CString(",");
			}
		}
	}
	return olRealDataList;
}

CString CedaObject::GetDBFieldType(CString opField)
{
	int ilIndex;
	CString olRet;
	if(omIndexMap.Lookup(opField, (void *&)ilIndex) == TRUE)
	{
		olRet = Desc[ilIndex].DBType;
	}
	return olRet;
}

//END MWO
CString CedaObject::GetFieldType(CString opField)
{
	int ilIndex;
	CString olRet;
	if(omIndexMap.Lookup(opField, (void *&)ilIndex) == TRUE)
	{
		olRet = Desc[ilIndex].Type;
	}
	return olRet;
}


CString CedaObject::GetFieldList()
{
	CString olFieldList;
	int ilCount = Desc.GetSize();

	for(int i = 0; i < ilCount; i++)
	{
		olFieldList += Desc[i].Field + CString(",");
	}
	if(!olFieldList.IsEmpty())
		olFieldList = olFieldList.Left(olFieldList.GetLength() - 1);
	return olFieldList ;
}

CString CedaObject::GetDBFieldList()
{
	CString olFieldList;
	int ilCount = Desc.GetSize() - imLogicalFieldCount;

	for(int i = 0; i < ilCount; i++)
	{
		olFieldList += Desc[i].Field + CString(",");
	}
	if(!olFieldList.IsEmpty())
		olFieldList = olFieldList.Left(olFieldList.GetLength() - 1);
	return olFieldList ;
}



CString CedaObject::GetInternalFieldList()
{
	CString olFieldList;
	int ilCount = Desc.GetSize();
	CString olTmp = "CDAT,LSTU,USEC,USEU";

	for(int i = 0; i < ilCount; i++)
	{
		if(olTmp.Find(Desc[i].Field) < 0)
			olFieldList += Desc[i].Field + CString(",");
	}
	if(!olFieldList.IsEmpty())
		olFieldList = olFieldList.Left(olFieldList.GetLength() - 1);
	return olFieldList ;
}

CString CedaObject::GetInternalDBFieldList()
{
	CString olFieldList;
	int ilCount = Desc.GetSize();
	CString olTmp = "CDAT,LSTU,USEC,USEU";

	for(int i = 0; i < ilCount - imLogicalFieldCount; i++)
	{
		if(olTmp.Find(Desc[i].Field) < 0)
			olFieldList += Desc[i].Field + CString(",");
	}
	if(!olFieldList.IsEmpty())
		olFieldList = olFieldList.Left(olFieldList.GetLength() - 1);
	return olFieldList ;
}


CString CedaObject::GetInternalDBDataList(RecordSet *prpRecord, CString opAccessMethod /*= CString("")*/)
{	
	if(prpRecord == NULL)
		return "";

	CString olListOfData;
	CString olTmp = "CDAT,LSTU,USEC,USEU";

	for(int i = 0; i < FieldCount - imLogicalFieldCount; i++)
	{
		if(olTmp.Find(Desc[i].Field) < 0)
		{
			if(opAccessMethod == "ODBC")
			{
				CString olStr = prpRecord->Values[i];
				if(Desc[i].DBType == "C" || Desc[i].DBType == "VC2")
				{
					olStr = CString("'") + olStr + CString("'");
					if(olStr == "''")
						olStr = "' '";
				}
				//CCSCedaData::MakeCedaString(olStr);
				olListOfData += olStr + CString(",");
			}
			else
			{
				CString olStr = prpRecord->Values[i];
				CCSCedaData::MakeCedaString(olStr);
				olListOfData += olStr + CString(",");
			}
		}
	}
	if(!olListOfData.IsEmpty())
	{
		olListOfData = olListOfData.Left(olListOfData.GetLength() - 1);
	}

	return olListOfData;
}

// MNE 27.01.00
CString CedaObject::GetDBDataList(RecordSet *prpRecord)
{	
	if(prpRecord == NULL)
		return "";

	CString olListOfData, olStr;

	for(int i = 0; i < FieldCount - imLogicalFieldCount; i++)
	{
		olStr = prpRecord->Values[i];
		CCSCedaData::MakeCedaString(olStr);
		olListOfData += olStr + CString(",");
	}

	if(!olListOfData.IsEmpty())
	{
		olListOfData = olListOfData.Left(olListOfData.GetLength() - 1);
	}

	return olListOfData;
}
// END MNE


CString CedaObject::GetInternalDataList(RecordSet *prpRecord)
{	
	if(prpRecord == NULL)
		return "";

	CString olListOfData;
	CString olTmp = "CDAT,LSTU,USEC,USEU";

	for(int i = 0; i < FieldCount; i++)
	{
		if(olTmp.Find(Desc[i].Field) < 0)
			olListOfData += prpRecord->Values[i] + CString(",");
	}
	if(!olListOfData.IsEmpty())
		olListOfData = olListOfData.Left(olListOfData.GetLength() - 1);

	return olListOfData;
}

void CedaObject::AddRecord(CString &opDataList)
{
	bmIsDataRead = true;
	RecordSet *prlRecordSet = new RecordSet(opDataList, FieldCount);
	Data.Add(prlRecordSet);
	AddToKeyMap(prlRecordSet);
}


void CedaObject::AddRecord(CCSPtrArray<RecordSet> *popBuffer, CString &opDataList)
{
	RecordSet *prlRecordSet = new RecordSet(opDataList, FieldCount);
	popBuffer->Add(prlRecordSet);
}



void CedaObject::CreateKeyMap(int ipStartPos)
{
	bmIsDataRead = true;
	for(int i = Data.GetSize() - 1; i >= ipStartPos; i--)
	{
		AddToKeyMap(&Data[i]);
	}
}

bool CedaObject::AddKeyMap(CString opField)
{
	if(bmIsDataRead)
		return false;
	CMapStringToPtr *prlKeyMap;
	//  wenn schon vorhanden fertig  hag990903
	if ( omKeyMaps.Lookup( (LPCSTR)opField, (void *&)prlKeyMap ) )
		return true;
	prlKeyMap = new CMapStringToPtr;
	omKeyMaps.SetAt((LPCSTR)opField, (void *&) prlKeyMap);
	return true;
}


CMapStringToPtr * CedaObject::GetMapPointer(CString opField)
{

	CMapStringToPtr *prlKeyMap = NULL;
	CMapStringToPtr *prlRetMap = NULL;
	POSITION pos;
	CString olField;


	for( pos = omKeyMaps.GetStartPosition(); pos != NULL; )
	{
		omKeyMaps.GetNextAssoc( pos, olField, (void *&)prlKeyMap );
		if(olField == opField)
		{
			prlRetMap = prlKeyMap;
		}
	}
	return prlRetMap;
}

void CedaObject::AddToKeyMap( RecordSet *prlRecordSet )
{
	int ilIndex;

	CMapStringToPtr *prlKeyMap;
	POSITION pos;
	CString olField;
	CString olValue;

	CCSPtrArray<RecordSet> *prlRecords;

	for( pos = omKeyMaps.GetStartPosition(); pos != NULL; )
	{
		omKeyMaps.GetNextAssoc( pos, olField, (void *&)prlKeyMap );

		if(omIndexMap.Lookup(olField, (void *&)ilIndex) == TRUE)
		{
			if(prlKeyMap->Lookup((LPCSTR)prlRecordSet->Values[ilIndex], (void *&)prlRecords) == TRUE)
			{
				bool blTreffer = false;
				for(int i = prlRecords->GetSize() -1 ; i >= 0; i--)
				{
					if( &((*prlRecords)[i]) == prlRecordSet)
					{
						blTreffer = true;
						break;
					}
				}
				if(!blTreffer)
					prlRecords->Add(prlRecordSet);
			}
			else
			{
				prlRecords = new CCSPtrArray<RecordSet>;
				prlRecords->Add(prlRecordSet);
				prlKeyMap->SetAt((LPCSTR)prlRecordSet->Values[ilIndex],(void *)prlRecords);
			}
		}
	}
}




void CedaObject::DelFromKeyMap( RecordSet *prlRecordSet)
{
	int ilIndex;

	CMapStringToPtr *prlKeyMap;
	POSITION pos;
	CString olField;
	CString olValue;
	CCSPtrArray<RecordSet> *prlRecords;

	for( pos = omKeyMaps.GetStartPosition(); pos != NULL; )
	{
		omKeyMaps.GetNextAssoc( pos, olField, (void *&)prlKeyMap );

		if(omIndexMap.Lookup(olField, (void *&)ilIndex) == TRUE)
		{
			if(prlKeyMap->Lookup((LPCSTR)prlRecordSet->Values[ilIndex], (void *&)prlRecords) == TRUE)
			{
				for(int i = prlRecords->GetSize() -1 ; i >= 0; i--)
				{
					if( &((*prlRecords)[i]) == prlRecordSet)
					{
						prlRecords->RemoveAt(i);
						break;
					}
				}
				if(prlRecords->GetSize() == 0)
				{
					delete prlRecords;
					prlKeyMap->RemoveKey((LPCSTR)prlRecordSet->Values[ilIndex]);
				}
			}
		}
	}
}


bool CedaObject::GetWhere(CString &opRefField, CString &opRefValue, CString &opWhere)
{
	opWhere = "";
	int ilIndex;
	if(omIndexMap.Lookup(opRefField, (void *&)ilIndex) == FALSE)
		return false;

	if(Desc[ilIndex].DBType == "N")
	{
		if(bmFieldsInSM)
		{
			CString olTmp = CString("                    ") + opRefValue;
			olTmp = olTmp.Right(Desc[ilIndex].Length);
			TRACE("\n Length: %d  Value: %s  Name: %s", olTmp.GetLength(), olTmp, opRefField);
			opWhere = CString("WHERE ") + opRefField + CString("='") + olTmp + CString("'");
		}
		else
			opWhere = CString("WHERE ") + opRefField + CString("=") + opRefValue;
	}
	else
		opWhere = CString("WHERE ") + opRefField + CString("='") + opRefValue + CString("'");

	return true;
}


//MWO 25.08.1999 
//if the fieldindex is not valid ==> function returns -1
bool CedaObject::GetFieldIndex(CString &opField, int &ilIndex)
{
	if(omIndexMap.Lookup(opField, (void *&)ilIndex) == TRUE)
	{
		return true;
	}
	else
	{
		ilIndex = -1;
		return false;
	}
}


bool CedaObject::IsFieldAvailabe(CString &opField)
{
	int ilIndex;
	if(omIndexMap.Lookup(opField, (void *&)ilIndex) == TRUE)
		return true;
	else
		return false;
}


CString CedaObject::GetFields(int ipIndex, CString opFields, CString opTrenner, int ipFlag)
{
	CString olRet;
	CStringArray olFields;
	int ilIndex;
	CString olTmp;
	CString olBlanks = "                                                                                    ";

	if(!(ipIndex >= 0 && ipIndex < Data.GetSize()))
		return olRet;
		
	int ilCount = ExtractItemList(opFields, &olFields);

	for( int i = 0; i < ilCount; i++)
	{
		if(omIndexMap.Lookup(olFields[i], (void *&)ilIndex) == TRUE)
		{
			olTmp = Data[ipIndex].Values[ilIndex];
			switch (ipFlag)
			{
			case FILLBLANK:
				{
					if(olTmp.GetLength() < Desc[ilIndex].Length)
					{
						olTmp += olBlanks;
						olTmp = olTmp.Left(Desc[ilIndex].Length);
					}
				}
				break;
			case TOTRIMLEFT:
				{
					olTmp.TrimLeft();
				}
				break;
			case TOTRIMRIGHT:
				{
					olTmp.TrimRight();
				}
				break;
			default: 
				break;
			}
			//CCSCedaData::MakeClientString(olTmp);
			olRet += olTmp + opTrenner;
		}
	}
	//olRet.TrimRight();
	return olRet;
}




void CedaObject::GetAllFields(CString opFields, CString opTrenner, int ipFlag, CStringArray &olData)
{
	CString olRet;
	CStringArray olFields;
	int ilIndex;
	CString olTmp;
	CString olBlanks = "                                                                                    ";

	int ilLen;


	int ilCount = ExtractItemList(opFields, &olFields);

	int ilEnd = ilCount - 1;
	int ilDataCount = Data.GetSize();

	olData.SetSize(ilDataCount);

	for( int j = 0; j < ilDataCount; j++)
	{
		olRet = "";
		olTmp = "";

		for( int i = 0; i < ilCount; i++)
		{
			if(omIndexMap.Lookup(olFields[i], (void *&)ilIndex) == TRUE)
			{
				olTmp = Data[j].Values[ilIndex];
				switch (ipFlag)
				{
				case FILLBLANK:
					{
						ilLen = olTmp.GetLength() ;
						if((ilLen < Desc[ilIndex].Length) && (i != ilEnd))
						{
							olTmp += olBlanks.Left(Desc[ilIndex].Length - ilLen);
						}
					}
					break;
				case TOTRIMLEFT:
					{
						olTmp.TrimLeft();
					}
					break;
				case TOTRIMRIGHT:
					{
						olTmp.TrimRight();
					}
					break;
				default: 
					break;
				}
				//CCSCedaData::MakeClientString(olTmp);
				olRet += olTmp + opTrenner;
			}
		}
		olData[j] = olRet;
	}
}



int CedaObject::ShowAllFields(CString opFields, CListBox *polList, CString opSelString, bool bpTrimRight/*false*/, bool bpAddBlankLine /*false*/)
{
	CString olRet;
	CStringArray olFields;
	int ilIndex;
	CString olTmp;
	CString olBlanks = "                                                                                    ";
	CString olTrenner = "  ";
	int ilLen;
	int i , j;
	int ilRet = 0;

	CCSPtrArray<int> olIndex; 

	int ilSelIndex = -1;
	//int ilFieldSelIndex = -1;

	//int ilSelStringLen = opSelString.GetLength();

	int ilFieldCount = ExtractItemList(opFields, &olFields) - 1;

	for( i = 0; i <= ilFieldCount; i++)
	{
		if(omIndexMap.Lookup(olFields[i], (void *&)ilIndex) == TRUE)
		{
			olIndex.New(ilIndex);

			//if( (ilSelStringLen > 0) &&  (GetMaxFieldLength(olFields[i]) == ilSelStringLen) ) 
			//	ilFieldSelIndex = ilIndex;

		}
	}

	if(bpAddBlankLine)
		polList->AddString("     ");

	ilFieldCount = olIndex.GetSize() - 1;

	int ilDataCount = Data.GetSize();

	for( j = 0; j < ilDataCount; j++)
	{
		olRet = "";
		for(  i = 0; i <= ilFieldCount; i++)
		{
			olTmp = Data[j].Values[olIndex[i]];

			//if( (olIndex[i] == ilFieldSelIndex) && (olTmp == opSelString) )
			if (ilSelIndex == -1 && olTmp == opSelString)
				ilSelIndex = j;


			ilLen = olTmp.GetLength();
			if((ilLen < Desc[olIndex[i]].Length) && (i != ilFieldCount))
			{
				olTmp += olBlanks.Left(Desc[olIndex[i]].Length - ilLen);
			}
			//CCSCedaData::MakeClientString(olTmp);
			olRet += olTmp + olTrenner;
		}

		if (bpTrimRight)
			olRet.TrimRight();

		if(ilRet < olRet.GetLength())
			ilRet = olRet.GetLength();

		polList->AddString(olRet);
	}

	olIndex.DeleteAll();

	if(ilSelIndex >= 0)
	{
		if(bpAddBlankLine)
			ilSelIndex = ilSelIndex + 1;

		polList->SetCurSel(ilSelIndex);

	}

	return ilRet;
}


CString CedaObject::GetField(int ipIndex, CString opField)
{
	CString olRet;
//MWO 25.08.1999 
//if the field is not valid ==> function returns "NULL"
	olRet = CString("NULL");
	int ilValIndex;
	if(omIndexMap.Lookup(opField, (void *&)ilValIndex) == TRUE)
	{
		if(ipIndex >= 0 && ipIndex < Data.GetSize())
			olRet = Data[ipIndex].Values[ilValIndex];
	}
	//CCSCedaData::MakeClientString(olRet);
	return olRet;
}


CString  CedaObject::GetTableHeader(CString opFields, CString opTrenner)
{
	CString olRet;
	CStringArray olFields;
	CString olTmp;
	CString olBlanks = "                                                                                    ";
	int ilIndex;
		
	int ilCount = ExtractItemList(opFields, &olFields);

	for( int i = 0; i < ilCount; i++)
	{
		if(omIndexMap.Lookup(olFields[i], (void *&)ilIndex) == TRUE)
		{
			if(HeaderMap.Lookup(olFields[i], olTmp) == TRUE)
			{
				olTmp += olBlanks;
				olTmp = olTmp.Left(Desc[ilIndex].Length);
				olRet += olTmp + opTrenner;
			}
		}
	}
	olRet.TrimRight();
	return olRet;
}



void  CedaObject::SetTableHeader(CString opField, CString opHeader)
{
	HeaderMap.SetAt((LPCSTR) opField , (LPCSTR) opHeader);
}



bool CedaObject::GetFieldBetween(CString opRefFieldFrom, CString opRefFieldTo, CString opRefValue, CString opField, CString &opReturn)
{
	int ilValIndex;
	int ilRefFromIndex;
	int ilRefToIndex;
	bool blRet = false;
	RecordSet *prlRecord;

	if(omIndexMap.Lookup(opField, (void *&)ilValIndex) == FALSE)
		return false;

	if(omIndexMap.Lookup(opRefFieldFrom, (void *&)ilRefFromIndex) == FALSE)
		return false;
	
	if(omIndexMap.Lookup(opRefFieldTo, (void *&)ilRefToIndex) == FALSE)
		return false;



	for(int i = Data.GetSize() - 1; i >= 0; i--)
	{
		prlRecord = &Data[i];
		if((prlRecord->Values[ilRefFromIndex] <= opRefValue) && (prlRecord->Values[ilRefToIndex] >= opRefValue))
		{
			opReturn = prlRecord->Values[ilValIndex];
			//CCSCedaData::MakeClientString(opReturn);
			blRet = true;
			break;
		}
	}

	return blRet;
}



bool CedaObject::GetField(CString opRefField, CString opRefField2, CString opRefValue, CString &opReturn1, CString &opReturn2, CString opValDate)
{
	int ilRefIndex;
	int ilRefIndex2;
	bool blRet = false;

	if(omIndexMap.Lookup(opRefField, (void *&)ilRefIndex) == FALSE)
		return false;
	
	if(omIndexMap.Lookup(opRefField2, (void *&)ilRefIndex2) == FALSE)
		return false;


	if(opValDate.IsEmpty())
	{
		if(Desc[ilRefIndex2].Length == opRefValue.GetLength())
		{
			if(GetField(opRefField2, opRefValue, opRefField2, opReturn2))
			{
				blRet = GetField(opRefField2, opRefValue, opRefField, opReturn1);
			}
		}
		else
		{
			if(Desc[ilRefIndex].Length == opRefValue.GetLength())
			{
			
				if(GetField(opRefField, opRefValue, opRefField, opReturn1))
				{
					blRet = GetField(opRefField, opRefValue, opRefField2, opReturn2);
				}
			}
		}
	}
	else
	{
		if(Desc[ilRefIndex2].Length == opRefValue.GetLength())
		{
			if(GetFieldVal(opRefField2, opRefValue, opRefField2, opReturn2, opValDate))
			{
				blRet = GetFieldVal(opRefField2, opRefValue, opRefField, opReturn1, opValDate);
			}
		}
		else
		{
			if(Desc[ilRefIndex].Length == opRefValue.GetLength())
			{
			
				if(GetFieldVal(opRefField, opRefValue, opRefField, opReturn1, opValDate))
				{
					blRet = GetFieldVal(opRefField, opRefValue, opRefField2, opReturn2, opValDate);
				}
			}
		}
	}


	return blRet;
}


bool CedaObject::GetFields(CString opRefField, CString opRefValue, CString opField1, CString opField2, CString &opReturn1, CString &opReturn2)
{
	bool blRet = false;

	if(GetField(opRefField, opRefValue, opField1, opReturn1))
	{
		blRet = GetField(opRefField, opRefValue, opField2, opReturn2);
	}
	return blRet;
}


bool CedaObject::GetField(CString opRefField, CString opRefValue, CString opField, CString &opReturn)
{

	int ilValIndex;
	int ilRefIndex;
	bool blRet = false;

	CMapStringToPtr *prlKeyMap;
	RecordSet *prlRecord;
	CCSPtrArray<RecordSet> *prlRecords;

	if(omIndexMap.Lookup(opField, (void *&)ilValIndex) == FALSE)
		return false;

	if(omIndexMap.Lookup(opRefField, (void *&)ilRefIndex) == FALSE)
		return false;


	if(omKeyMaps.Lookup((LPCSTR)opRefField, (void *&)prlKeyMap) == TRUE)
	{
		if(prlKeyMap->Lookup((LPCSTR)opRefValue, (void *&)prlRecords) == TRUE)
		{
			opReturn = (*prlRecords)[0].Values[ilValIndex];
			//CCSCedaData::MakeClientString(opReturn);
			blRet = true;
		}		
	}
	else
	{
		for(int i = Data.GetSize() - 1; i >= 0; i--)
		{
			prlRecord = &Data[i];
			CString wer = prlRecord->Values[ilRefIndex];
			if(prlRecord->Values[ilRefIndex] == opRefValue)
			{
				opReturn = prlRecord->Values[ilValIndex];
				//CCSCedaData::MakeClientString(opReturn);
				blRet = true;
				break;
			}
		}
	}
	return blRet;
}

bool CedaObject::GetFieldVal(CString opRefField, CString opRefValue, CString opField, CString &opReturn, CString opValDate)
{

	int ilVafrIndex;
	int ilVatoIndex;
	
	int ilValIndex;
	int ilRefIndex;
	bool blRet = false;

	CString olVafr;
	CString olVato;


	RecordSet *prlRecord;

	if(omIndexMap.Lookup(opField, (void *&)ilValIndex) == FALSE)
		return false;

	if(omIndexMap.Lookup(opRefField, (void *&)ilRefIndex) == FALSE)
		return false;


	if(omIndexMap.Lookup("VAFR", (void *&)ilVafrIndex) == FALSE)
		return false;


	if(omIndexMap.Lookup("VATO", (void *&)ilVatoIndex) == FALSE)
		return false;


	for(int i = Data.GetSize() - 1; i >= 0; i--)
	{
		prlRecord = &Data[i];
		CString wer = prlRecord->Values[ilRefIndex];
		if(prlRecord->Values[ilRefIndex] == opRefValue)
		{
			olVafr = prlRecord->Values[ilVafrIndex];
			olVato = prlRecord->Values[ilVatoIndex];

			if( olVafr <= opValDate && (olVato.IsEmpty() || olVato >= opValDate))
			{
				opReturn = prlRecord->Values[ilValIndex];
				//CCSCedaData::MakeClientString(opReturn);
				blRet = true;
				break;
			}
		}
	}

	return blRet;
}




bool CedaObject::DeleteRecord(CString opRefField, CString opRefValue)
{
	int ilRefIndex;
	bool blRet = false;

	CMapStringToPtr *prlKeyMap;
	RecordSet *prlRecord;
	CCSPtrArray<RecordSet> *prlRecords;

	if ( bmIsLogicObject )
		return RemoveRecord ( opRefField, opRefValue );

	if(omIndexMap.Lookup(opRefField, (void *&)ilRefIndex) == FALSE)
		return false;


	if(omKeyMaps.Lookup((LPCSTR)opRefField, (void *&)prlKeyMap) == TRUE)
	{
		
		if(prlKeyMap->Lookup((LPCSTR)opRefValue, (void *&)prlRecords) == TRUE)
		{
			for(int i = prlRecords->GetSize() - 1; i >= 0; i--)
			{
				(*prlRecords)[i].Status = DATA_DELETED;
				omChangedMap.SetAt((void *)&((*prlRecords)[i]),(void *)&((*prlRecords)[i]));
				DelFromKeyMap(&((*prlRecords)[i]));
				blRet = true;
			}
		}		
		
		/*
		if(prlKeyMap->Lookup((LPCSTR)opRefValue, (void *&)prlRecord) == TRUE)
		{
			prlRecord->Status = DATA_DELETED;
			DelFromKeyMap(prlRecord);
			omChangedMap.SetAt((void *)prlRecord,(void *)prlRecord);
			blRet = true;
		}
		*/
	}
	else
	{
		for(int i = Data.GetSize() - 1; i >= 0; i--)
		{
			prlRecord = &Data[i];
			if(prlRecord->Values[ilRefIndex] == opRefValue)
			{
				prlRecord->Status = DATA_DELETED;
				omChangedMap.SetAt((void *)prlRecord,(void *)prlRecord);
				DelFromKeyMap(prlRecord);
				blRet = true;
				break;
			}
		}
	}

	// MNE 990806
	/*if(!blRet && opRefField == "URNO")
	{
		int ilIndex;
		if(omIndexMap.Lookup("URNO", (void *&)ilIndex) == TRUE)
		{
			blRet = true;
			RecordSet *prlRecordSet = new RecordSet(FieldCount);
			prlRecordSet->Values[ilIndex] = opRefValue;
			Data.Add(prlRecordSet);
			prlRecordSet->Status = DATA_DELETED;
			omChangedMap.SetAt((void *)prlRecordSet,(void *)prlRecordSet);
		}
	}*/
	// END MNE 990806


	return blRet;
}




CString CedaObject::GetField(CString opRefField, CString opRefValue, CString opField)
{
	CString olValue;
	GetField(opRefField, opRefValue, opField, olValue);
	return olValue;
}

int CedaObject::GetAllRecords(CCSPtrArray <RecordSet> &ropRecords)
{
	int ilCount = Data.GetSize();

	for (int i = 0; i < ilCount; i++)
	{
		ropRecords.New(Data[i]);
	}

	return ropRecords.GetSize(); 	
}



bool CedaObject::GetRecord(int ilIndex, RecordSet &rpRecord)
{
	bool blRet = false;
	if(ilIndex >= 0 && ilIndex < Data.GetSize())
	{
		rpRecord = Data[ilIndex];
		blRet  = true;
	}
	return blRet; 
}


void CedaObject::GetRecords(CString opRefField, CString opRefValue, CCSPtrArray<RecordSet> *prpRecords)
{
	int ilRefIndex;

	CMapStringToPtr *prlKeyMap;
	RecordSet *prlRecord;
	bool blRet = false;
	CCSPtrArray<RecordSet> *prlRecords;

	prpRecords->DeleteAll();

	if(omIndexMap.Lookup(opRefField, (void *&)ilRefIndex) == TRUE)
	{
		if(omKeyMaps.Lookup((LPCSTR)opRefField, (void *&)prlKeyMap) == TRUE)
		{
			if(prlKeyMap->Lookup((LPCSTR)opRefValue, (void *&)prlRecords) == TRUE)
			{
				for(int i = prlRecords->GetSize() - 1; i >= 0; i--)
				{
					prpRecords->New((*prlRecords)[i]);
				}
			}		
		}
		else
		{
			for(int i = Data.GetSize() - 1; i >= 0; i--)
			{
				prlRecord = &Data[i];
				if(prlRecord->Values[ilRefIndex] == opRefValue)
				{
					prpRecords->New(*prlRecord);
				}
			}
		}
	}
}


bool CedaObject::GetRecord(CString opRefField, CString opRefValue, RecordSet &rpRecord)
{
	int ilRefIndex;

	CMapStringToPtr *prlKeyMap;
	RecordSet *prlRecord;
	bool blRet = false;
	CCSPtrArray<RecordSet> *prlRecords;


	if(omIndexMap.Lookup(opRefField, (void *&)ilRefIndex) == TRUE)
	{
		if(omKeyMaps.Lookup((LPCSTR)opRefField, (void *&)prlKeyMap) == TRUE)
		{

			
			if(prlKeyMap->Lookup((LPCSTR)opRefValue, (void *&)prlRecords) == TRUE)
			{
				rpRecord = (*prlRecords)[0];
				//rpRecord = *prlRecord;
				blRet = true;
			}		
			/*
			if(prlKeyMap->Lookup((LPCSTR)opRefValue, (void *&)prlRecord) == TRUE)
			{
				rpRecord = *prlRecord;
				blRet  = true;
			}
			*/
		}
		else
		{
			for(int i = Data.GetSize() - 1; i >= 0; i--)
			{
				prlRecord = &Data[i];
				if(prlRecord->Values[ilRefIndex] == opRefValue)
				{
					rpRecord = *prlRecord;
					blRet  = true;
					break;
				}
			}
		}
	}
	return blRet;
}




void CedaObject::ClearAll()
{
	Data.DeleteAll();

	CMapStringToPtr *prlKeyMap;
	POSITION pos;
	POSITION pos2;
	CString olTmp;
	CCSPtrArray<RecordSet> *prlRecords;

	for( pos = omKeyMaps.GetStartPosition(); pos != NULL; )
	{
		omKeyMaps.GetNextAssoc( pos, olTmp, (void *&)prlKeyMap );
		for( pos2 = prlKeyMap->GetStartPosition(); pos2 != NULL; )
		{
			prlKeyMap->GetNextAssoc( pos2, olTmp, (void *&)prlRecords );
			prlRecords->RemoveAll();
			delete prlRecords;
		}

		prlKeyMap->RemoveAll();
	}
}


void CedaObject::TraceData()
{
	RecordSet *prlRecord;

	CString olDataList;
	int ilCount = Data.GetSize();

	for(int i = 0; i < ilCount; i++)
	{
		prlRecord = &Data[i];

		olDataList = "";

		for(int j = 0; j < prlRecord->Values.GetSize(); j++)
		{
			olDataList += CString("<") + prlRecord->Values[j] + CString(">");
		}
		TRACE("\n%d %s", i, olDataList);
	}

}



bool CedaObject::DeleteInternal(CString &opUrno, RecordSet *prpRecord)
{
	int ilIndex;
	RecordSet *prlRecord;
	bool blRet = false;


	if(omIndexMap.Lookup("URNO", (void *&)ilIndex) == FALSE)
		return false;

	for(int i = Data.GetSize() - 1; i >= 0; i--)
	{
		prlRecord = &Data[i];
		if(prlRecord->Values[ilIndex] == opUrno)
		{
			if(prpRecord != NULL)
				*prpRecord = *prlRecord;
			DelFromKeyMap(prlRecord);
			Data.DeleteAt(i);
			blRet = true;
			break;
		}
	}
	return blRet;
}



bool CedaObject::UpdateInternal(CString &opUrno, CString opFieldList, CString opDataList, RecordSet *prpRecord)
{
	int ilIndex;
	CMapStringToPtr *prlKeyMap;
	RecordSet *prlRecord;
	bool blRet = false;
	CCSPtrArray<RecordSet> *prlRecords;
	
	if(omIndexMap.Lookup("URNO", (void *&)ilIndex) == FALSE)
		return false;


	if(omKeyMaps.Lookup("URNO", (void *&)prlKeyMap) == TRUE)
	{

		
		if(prlKeyMap->Lookup((LPCSTR)opUrno, (void *&)prlRecords) == TRUE)
		{
			prlRecord = &((*prlRecords)[0]);
			DelFromKeyMap(prlRecord);
			UpdateRecord(prlRecord, opFieldList, opDataList);
			AddToKeyMap(prlRecord);
			if(prpRecord != NULL)
				*prpRecord = *prlRecord;

			//opReturn = (*prlRecords)[0].Values[ilValIndex];
			blRet = true;
		}
		
		
		/*		
		if(prlKeyMap->Lookup((LPCSTR)opUrno, (void *&)prlRecord) == TRUE)
		{
			DelFromKeyMap(prlRecord);
			UpdateRecord(prlRecord, opFieldList, opDataList);
			AddToKeyMap(prlRecord);
			if(prpRecord != NULL)
				*prpRecord = *prlRecord;
			blRet = true;
		}
		*/
	}
	return blRet;
}


bool CedaObject::InsertInternal(CString opFieldList, CString opDataList, RecordSet *prpRecord)
{
	int ilIndex;
	bool blRet = false;

	if(omIndexMap.Lookup("URNO", (void *&)ilIndex) == FALSE)
		return false;

	bmIsDataRead = true;

	RecordSet *prlRecord = new RecordSet(CString(""), FieldCount);
	UpdateRecord(prlRecord, opFieldList, opDataList);
	DeleteInternal(prlRecord->Values[ilIndex]);
	Data.Add(prlRecord);
	if(prpRecord != NULL)
		*prpRecord = *prlRecord;
	blRet = true;
	AddToKeyMap(prlRecord);
	return blRet;
}








/////////////////////////////////////////////



void CedaObject::UpdateRecord(RecordSet *prlRecordSet, CString opFieldList, CString opDataList)
{
	if(prlRecordSet == NULL)
		return;

	CStringArray olData;
	CStringArray olFields;

	int ilData	 = ExtractItemList(opDataList,  &olData);
	int ilFields = ExtractItemList(opFieldList, &olFields);
	int ilIndex;
	CString olTmp;

	for(int i = 0; (i < ilData) && (i < ilFields); i++)
	{
		olTmp = olFields[i];
		olTmp.TrimLeft();
		olTmp.TrimRight();
		if(omIndexMap.Lookup(olTmp, (void *&)ilIndex) == TRUE)
		{
			CCSCedaData::MakeClientString(olData[i]);
			prlRecordSet->Values[ilIndex] = olData[i];
			prlRecordSet->Values[ilIndex].TrimRight();
		}
	}
}



bool CedaObject::SetRecord(CString opRefField, CString opRefValue, CStringArray &opRecord)
{
	int ilRefIndex;
	CMapStringToPtr *prlKeyMap;
	RecordSet *prlRecord;
	bool blRet = false;
	CCSPtrArray<RecordSet> *prlRecords;

	if(omIndexMap.Lookup(opRefField, (void *&)ilRefIndex) == TRUE)
	{
		if(omKeyMaps.Lookup((LPCSTR)opRefField, (void *&)prlKeyMap) == TRUE)
		{

			if(prlKeyMap->Lookup((LPCSTR)opRefValue, (void *&)prlRecords) == TRUE)
			{
				prlRecord = &((*prlRecords)[0]);
				blRet = true;
			}		
			
			/*
			if(prlKeyMap->Lookup((LPCSTR)opRefValue, (void *&)prlRecord) == TRUE)
			{
				blRet  = true;
			}
			*/
		}
		else
		{
			for(int i = Data.GetSize() - 1; i >= 0; i--)
			{
				prlRecord = &Data[i];
				if(prlRecord->Values[ilRefIndex] == opRefValue)
				{
					blRet  = true;
					break;
				}
			}
		}
	}

	if(blRet)
	{
		for(int i = 0; i < opRecord.GetSize(); i++)
			prlRecord->Values[i] = opRecord[i];
		
		if(prlRecord->Status != DATA_NEW)	
			prlRecord->Status = DATA_CHANGED;
		omChangedMap.SetAt((void *)prlRecord,(void *)prlRecord);
	}
	return blRet;
}


bool CedaObject::SetField(CString opRefField, CString opRefValue, CString opField, CString opValue)
{
	int ilValIndex;
	int ilRefIndex;
	bool blRet = false;

	CMapStringToPtr *prlKeyMap;
	RecordSet *prlRecord;
	CCSPtrArray<RecordSet> *prlRecords;

	if(opField == "URNO")
		return false;

	if(omIndexMap.Lookup(opField, (void *&)ilValIndex) == FALSE)
		return false;

	if(omIndexMap.Lookup(opRefField, (void *&)ilRefIndex) == FALSE)
		return false;


	if(omKeyMaps.Lookup((LPCSTR)opRefField, (void *&)prlKeyMap) == TRUE)
	{
		if(prlKeyMap->Lookup((LPCSTR)opRefValue, (void *&)prlRecords) == TRUE)
		{
			prlRecord = &((*prlRecords)[0]);
			blRet = true;
		}		

		/*		
		if(prlKeyMap->Lookup((LPCSTR)opRefValue, (void *&)prlRecord) == TRUE)
		{
			blRet = true;
		}
		*/
	}
	else
	{
		for(int i = Data.GetSize() - 1; i >= 0; i--)
		{
			prlRecord = &Data[i];
			if(prlRecord->Values[ilRefIndex] == opRefValue)
			{
				blRet = true;
				break;
			}
		}
	}
	if(blRet)
	{
		if(prlRecord->Status != DATA_NEW)	
			prlRecord->Status = DATA_CHANGED;
		omChangedMap.SetAt((void *)prlRecord,(void *)prlRecord);
		opValue = opValue.Left(Desc[ilValIndex].Length);
		CCSCedaData::MakeCedaString(opValue);
		prlRecord->Values[ilValIndex] = opValue;
	}
	
	return blRet;
}


bool CedaObject::InsertRecord(RecordSet &rpRecord, CString opUrno)
{
	bool blRet = false;
	int ilIndex;

	if(omIndexMap.Lookup("URNO", (void *&)ilIndex) == FALSE)
		return blRet;

	blRet = true;

	if( rpRecord[ilIndex].IsEmpty())
			rpRecord[ilIndex] = opUrno;

	//for(int i = opRecord.GetSize() - 1; i >= 0 ; i--)
	//	opRecord[i] = opRecord[i].Left(Desc[i].Length);;
	
	RecordSet *prlRecordSet = new RecordSet(FieldCount);
	*prlRecordSet = rpRecord;
	Data.Add(prlRecordSet);
	prlRecordSet->Status = DATA_NEW;
	AddToKeyMap(prlRecordSet);
	omChangedMap.SetAt((void *)prlRecordSet,(void *)prlRecordSet);
	return blRet;
}



bool CedaObject::RemoveRecord(CString opRefField, CString opRefValue)
{
	int ilRefIndex;
	bool blRet = false;
	RecordSet *prlRecord;

	if(omIndexMap.Lookup(opRefField, (void *&)ilRefIndex) == FALSE)
		return false;

	for(int i = Data.GetSize() - 1; i >= 0; i--)
	{
		prlRecord = &Data[i];
		if(prlRecord->Values[ilRefIndex] == opRefValue)
		{
			DelFromKeyMap(prlRecord);
			omChangedMap.RemoveKey((void *)prlRecord);
			Data.DeleteAt(i);
			blRet = true;
			break;
		}
	}
	return blRet;
}

bool CedaObject::RemoveRecord(RecordSet *prpRecord)
{
	bool blRet = false;
	RecordSet *prlRecord;

	for(int i = Data.GetSize() - 1; i >= 0; i--)
	{
		prlRecord = &Data[i];
		if(prlRecord == prpRecord)
		{
			DelFromKeyMap(prlRecord);
			omChangedMap.RemoveKey((void *)prlRecord);
			Data.DeleteAt(i);
			blRet = true;
			break;
		}
	}
	return blRet;
}


void CedaObject::SetSort(CString opSortList, bool bpDo, bool bpNumeric)
{
	smOrder.RemoveAll();
	imIndex.RemoveAll();

	bmNumericSort = bpNumeric;

	CStringArray olSort;

	int ilIndex;

	int ilCount = ExtractItemList(opSortList, &olSort);

	for(int i = 0; i < ilCount; i++)
	{
		if(GetFieldIndex(olSort[i].Left(4), ilIndex))
		{
			imIndex.Add(ilIndex);	
		
			if(olSort[i].GetLength() == 5)
				smOrder.Add(olSort[i].Right(1));
			else
				smOrder.Add("+");
		}
	}
	if(bpDo)
		Sort();

	bmNumericSort = false;
}


void CedaObject::Sort()
{
	pimIndex = &imIndex;
	psmOrder = &smOrder;
	pbmNumericSort = &bmNumericSort;

	Data.Sort(CompareRecord);
}



int CedaObject::CompareRecord(const RecordSet **e1, const RecordSet **e2)
{
	int	ilCompareResult = 0;

	if ( !psmOrder || !pimIndex )
		return 0;

	int ilCount = psmOrder->GetSize();

	for (int i = 0; i < ilCount; i++)
	{
		if((*psmOrder)[i] == "+")
		{
			if(*pbmNumericSort)
			{
				ilCompareResult = ( atoi((*e1)->Values[(*pimIndex)[i]]) == atoi((*e2)->Values[(*pimIndex)[i]]) )? 0: ( atoi((*e1)->Values[(*pimIndex)[i]]) < atoi((*e2)->Values[(*pimIndex)[i]]) )? -1: 1;
			}
			else
			    ilCompareResult = (*e1)->Values[(*pimIndex)[i]].Collate((*e2)->Values[(*pimIndex)[i]]);
		}
		else
		{
			if(*pbmNumericSort)
				ilCompareResult = ( atoi((*e1)->Values[(*pimIndex)[i]]) == atoi((*e2)->Values[(*pimIndex)[i]]) )? 0: ( atoi((*e1)->Values[(*pimIndex)[i]]) > atoi((*e2)->Values[(*pimIndex)[i]]) )? -1: 1;
			else
			    ilCompareResult = (*e2)->Values[(*pimIndex)[i]].Collate((*e1)->Values[(*pimIndex)[i]]);
		}

		if (ilCompareResult != 0)
			return ilCompareResult;
	}
	return 0;
}



void CedaObject::SetDdxType(CString opCmd, int ipDdxType)
{
	DdxMap.SetAt((LPCSTR)opCmd, (void *) ipDdxType);
}


void CedaObject::SetSentBcBufferOverflow(int ipDdxType)
{
	DdxMap.SetAt("BBO", (void *) ipDdxType);
}


bool CedaObject::SentBcBufferOverflow(int &ipDdxType)
{
	bool blRet = false;
	CString olCmd = CString("BBO");
	
	if(DdxMap.Lookup(olCmd ,(void * &)ipDdxType) == TRUE)
	{
		if(BcBuffer.GetSize() > BcBufferSize)
		{
			blRet = true;
		}
	}
	return blRet;
}


bool CedaObject::GetDdxType(CString opCmd, int &ipDdxType)
{
	if(DdxMap.Lookup(opCmd ,(void * &)ipDdxType) == TRUE)
	{
		return true;
	}
	return false;
}


bool CedaObject::BufferBc(BcStruct *prpBC)
{
	bool blRet = false;
	
	if(BcBufferSize > 0)
	{
		BcBuffer.New(*prpBC);
		if(BcBuffer.GetSize() > MAX_BC_BUFFER_SIZE)
			BcBuffer.DeleteAt(0);
		blRet = true;
	}
	return blRet;
}


bool CedaObject::GetAllValuesForField(CString opField, CStringArray &ropValues)
{
	int ilValIndex = -1;
	if(omIndexMap.Lookup(opField, (void *&)ilValIndex) == FALSE)
	{
		return false;
	}
	for(int i = Data.GetSize() - 1; i >= 0; i--)
	{
		RecordSet *prlRecord = NULL;
		CString olTmp;
		prlRecord = &Data[i];
		olTmp = prlRecord->Values[ilValIndex];
		olTmp.TrimLeft();
		ropValues.Add(olTmp);
	}
	
	return true;
}

CString CedaObject::GetValueList(CString opRefField, CString opRefValue, CString opField, CString opTrenner)
{

	int ilValIndex;
	int ilRefIndex;
	CString olRet;

	RecordSet *prlRecord;
	CMapStringToPtr *prlKeyMap;
	CCSPtrArray<RecordSet> *prlRecords;

	if(omIndexMap.Lookup(opField, (void *&)ilValIndex) == FALSE)
		return "";

	if(omIndexMap.Lookup(opRefField, (void *&)ilRefIndex) == FALSE)
		return "";

	CString olTmp;

	if(omKeyMaps.Lookup((LPCSTR)opRefField, (void *&)prlKeyMap) == TRUE)
	{
		if(prlKeyMap->Lookup((LPCSTR)opRefValue, (void *&)prlRecords) == TRUE)
		{
			for(int i = prlRecords->GetSize() - 1; i >= 0; i--)
			{
				prlRecord = &((*prlRecords)[i]);
				olTmp = prlRecord->Values[ilRefIndex];
				olTmp.TrimLeft();
				if(olTmp == opRefValue)
				{
					olRet += prlRecord->Values[ilValIndex] + opTrenner;
				}
			}
		}		
	}
	else
	{
		for(int i = Data.GetSize() - 1; i >= 0; i--)
		{
			prlRecord = &Data[i];
			olTmp = prlRecord->Values[ilRefIndex];
			olTmp.TrimLeft();
			if(olTmp == opRefValue)
			{
				olRet += prlRecord->Values[ilValIndex] + opTrenner;
			}
		}
	}
	
	if(!olRet.IsEmpty())
		olRet = olRet.Left(olRet.GetLength() - 1);

	return olRet;

}


CString CedaObject::GetField2(CString opRefField1, CString opRefField2, CString opRefValue, CString opField)
{
	int ilRefIndex1;
	int ilRefIndex2;
	bool blRet = false;

	CString olRet;

	if(omIndexMap.Lookup(opRefField1, (void *&)ilRefIndex1) == FALSE)
		return olRet;
	
	if(omIndexMap.Lookup(opRefField2, (void *&)ilRefIndex2) == FALSE)
		return olRet;

	if(Desc[ilRefIndex2].Length == opRefValue.GetLength())
	{
			blRet = GetField(opRefField2, opRefValue, opField, olRet);
	}
	else
	{
		if(Desc[ilRefIndex1].Length == opRefValue.GetLength())
		{
			blRet = GetField(opRefField1, opRefValue, opField, olRet);
		}
	}
	return olRet;

}





CString CedaObject::GetFieldExt(CString opRefField1, CString opRefField2, CString opRefValue1, CString opRefValue2, CString opField)
{
	int ilRefIndex1;
	int ilRefIndex2;
	int ilValIndex;
	CString olRet = "";
	CMapStringToPtr *prlKeyMap;
	RecordSet *prlRecord;
	CString olTmp;

	if(omIndexMap.Lookup(opRefField1, (void *&)ilRefIndex1) == FALSE)
		return olRet;
	
	if(omIndexMap.Lookup(opRefField2, (void *&)ilRefIndex2) == FALSE)
		return olRet;

	if(omIndexMap.Lookup(opField, (void *&)ilValIndex) == FALSE)
		return olRet;

	CCSPtrArray<RecordSet> *prlRecords;

	if(omKeyMaps.Lookup((LPCSTR)opRefField1, (void *&)prlKeyMap) == TRUE)
	{
		if(prlKeyMap->Lookup((LPCSTR)opRefValue1, (void *&)prlRecords) == TRUE)
		{
			for(int i = prlRecords->GetSize() - 1; i >= 0; i--)
			{
				prlRecord = &((*prlRecords)[i]);
				olTmp = prlRecord->Values[ilRefIndex2];
				olTmp.TrimLeft();
				if(olTmp == opRefValue2)
				{
					return prlRecord->Values[ilValIndex] ;
				}
			}
		}		
	}
	else
	{
		for(int i = Data.GetSize() - 1; i >= 0; i--)
		{
			if((Data[i].Values[ilRefIndex1] == 	opRefValue1) && (Data[i].Values[ilRefIndex2] == opRefValue2))	
			{
				olRet = Data[i].Values[ilValIndex];
				//CCSCedaData::MakeClientString(olRet);
				break;
			}
		}
	}
	return olRet;

}



int CedaObject::GetRecordsExt(CString opRefField1, CString opRefField2, CString opRefValue1, CString opRefValue2, CCSPtrArray<RecordSet> *prpRecords)
{

	int ilRefIndex1;
	int ilRefIndex2;
	CString olTmp;
	CMapStringToPtr *prlKeyMap;
	RecordSet *prlRecord;
	RecordSet *prlRecord2;
	CCSPtrArray<RecordSet> *prlRecords;

	if(omIndexMap.Lookup(opRefField1, (void *&)ilRefIndex1) == FALSE)
		return -1;
	
	if(omIndexMap.Lookup(opRefField2, (void *&)ilRefIndex2) == FALSE)
		return -1;


	if(omKeyMaps.Lookup((LPCSTR)opRefField1, (void *&)prlKeyMap) == TRUE)
	{
		if(prlKeyMap->Lookup((LPCSTR)opRefValue1, (void *&)prlRecords) == TRUE)
		{
			for(int i = prlRecords->GetSize() - 1; i >= 0; i--)
			{
				prlRecord = &((*prlRecords)[i]);
				olTmp = prlRecord->Values[ilRefIndex2];
				olTmp.TrimLeft();
				if(olTmp == opRefValue2)
				{
					prlRecord2 = new RecordSet(FieldCount);
					*prlRecord2 = (*prlRecords)[i];
					prpRecords->Add(prlRecord2);
				}
			}
		}		
	}
	else
	{
		for(int i = Data.GetSize() - 1; i >= 0; i--)
		{
			if((Data[i].Values[ilRefIndex1] == 	opRefValue1) && (Data[i].Values[ilRefIndex2] == opRefValue2))	
			{
				prlRecord = new RecordSet(FieldCount);
				*prlRecord = Data[i];
				prpRecords->Add(prlRecord);
			}
		}
	}

	return prpRecords->GetSize();

}

bool CedaObject::IsFieldAvailable(CString olTana, CString olFina)
{
	if (pomSystab != NULL)
	{
		CCSPtrArray<SYSTABDATA>	olArr;
		pomSystab->GetAllFieldsForTable(olArr, olTana);

		for (int i = 0; i < olArr.GetSize(); i++)
		{
			if (!strcmp(olArr[i].Fina, olFina.GetBuffer(0)))
			{
				return true;
			}
		}
	}

	return false;
}

int CedaObject::GetMaxFieldLength(CString olTana, CString olFina)
{
	if (pomSystab != NULL)
	{
		CCSPtrArray<SYSTABDATA>	olArr;
		pomSystab->GetAllFieldsForTable(olArr, olTana);

		for (int i = 0; i < olArr.GetSize(); i++)
		{
			if (!strcmp(olArr[i].Fina, olFina.GetBuffer(0)))
			{
				return olArr[i].Fele;
			}
		}
	}

	return -1;
}