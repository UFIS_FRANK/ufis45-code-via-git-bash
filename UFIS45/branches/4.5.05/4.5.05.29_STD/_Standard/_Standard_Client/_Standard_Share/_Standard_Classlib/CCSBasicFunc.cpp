// CCSBasicFunc.cpp: implementation of the CCSBasicFunc class.
//
//////////////////////////////////////////////////////////////////////

#include <stdafx.h>
#include <CCSBasicFunc.h>

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif




int ExtractItemList(CString opSubString, CStringArray *popStrArray, char cpTrenner)
{
	int ilSepaLen = 1;//
	int ilCount = 0;
	char *currPtr;
	char *ptrOrig;
	char *pclTextLine;
	bool blLastCharIsSepa = false;
	
	if (opSubString.GetLength() > 0)
	{
		blLastCharIsSepa = opSubString[opSubString.GetLength()-1] == cpTrenner;
	}

	pclTextLine = (char*)malloc((size_t)opSubString.GetLength()+1);
	memset((void*)pclTextLine, 0x00, opSubString.GetLength()+1);
	strcpy(pclTextLine, opSubString.GetBuffer(0));

	ptrOrig = pclTextLine;
	currPtr = strchr(pclTextLine ,cpTrenner);
	while (currPtr != NULL)
	{
		*currPtr = '\0';
		popStrArray->Add(pclTextLine );
		ilCount++;
		pclTextLine  = currPtr + ilSepaLen;
		currPtr		= strchr(pclTextLine ,cpTrenner);
	}

	// not empty ?
	if (strcmp(pclTextLine , "") != 0)
	{
		popStrArray->Add(pclTextLine );
		ilCount++;
	}
	else if (blLastCharIsSepa)
	{
		popStrArray->Add(pclTextLine);
		ilCount++;
	}

	free(ptrOrig);
	return ilCount;
}
