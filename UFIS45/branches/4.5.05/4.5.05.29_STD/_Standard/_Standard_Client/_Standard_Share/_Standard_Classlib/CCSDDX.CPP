// CCSDdx.cpp - Data distribution class
//  
//  

#include <ccsptrarray.h>
#include <CCSDdx.H>



CCSDdx::CCSDdx(void)
{
}

CCSDdx::~CCSDdx(void)
{
	CCSPtrArray<DDXCallBackEntry> *polDDXCallBackTable;
	POSITION pos;
	void *pVoid;
	for( pos = KeyMap.GetStartPosition(); pos != NULL; )
	{
		KeyMap.GetNextAssoc( pos, pVoid , (void *&)polDDXCallBackTable );
		polDDXCallBackTable->RemoveAll();
		delete polDDXCallBackTable;
	}
	KeyMap.RemoveAll();
	DDXCallBackTable.DeleteAll();
}

void CCSDdx::Register(void *vpInstance, int ipDDXType, CString &DDXName, CString &InstanceName, DDXCALLBACK pfpCallBack)
{
	// TVO (13.02.2001): doppelte Eintraege verhindern!
	UnRegister(vpInstance, ipDDXType);


	DDXCallBackEntry *polDDXRegistration = new DDXCallBackEntry;

	polDDXRegistration->Instance = vpInstance;
	polDDXRegistration->DDXType = ipDDXType;
	polDDXRegistration->DDXName = DDXName;
	polDDXRegistration->InstanceName = InstanceName;
	polDDXRegistration->CallBack = pfpCallBack;
	DDXCallBackTable.Add(polDDXRegistration);

	AddToKeyMap(polDDXRegistration);
	
	//TRACE("DDX-Register: (%ld)  %s %s\n", DDXCallBackTable.GetSize(), (LPCSTR)DDXName,(LPCSTR)InstanceName);
}

void CCSDdx::UnRegister(void *vpInstance,int ipDDXType)
{

	for (int ilLc = DDXCallBackTable.GetSize()-1; ilLc >= 0; ilLc--)
	{
		if (DDXCallBackTable[ilLc].Instance == vpInstance)
		{
			if ((ipDDXType == NOTUSED) || (DDXCallBackTable[ilLc].DDXType == ipDDXType))
			{
				DelFromKeyMap(&DDXCallBackTable[ilLc]);
				DDXCallBackTable.DeleteAt(ilLc);
			}
		}
	}
}

void CCSDdx::AddToKeyMap(DDXCallBackEntry *polDDXRegistration)
{
	CCSPtrArray<DDXCallBackEntry> *polDDXCallBackTable;


	if(KeyMap.Lookup((void *)polDDXRegistration->DDXType,(void *& )polDDXCallBackTable) == TRUE)
	{
		for(int i = polDDXCallBackTable->GetSize() - 1; i >= 0; i--)
		{
			if ((*polDDXCallBackTable)[i].Instance == polDDXRegistration->Instance)
			{
				polDDXCallBackTable->RemoveAt(i);
				break;
			}
		}
		polDDXCallBackTable->Add(polDDXRegistration);
	}
	else
	{
		polDDXCallBackTable = new CCSPtrArray<DDXCallBackEntry>;
		polDDXCallBackTable->Add(polDDXRegistration);
		KeyMap.SetAt((void *)polDDXRegistration->DDXType,polDDXCallBackTable);
	}

}

void CCSDdx::DelFromKeyMap(DDXCallBackEntry *polDDXRegistration)
{
	CCSPtrArray<DDXCallBackEntry> *polDDXCallBackTable;

	if(KeyMap.Lookup((void *)polDDXRegistration->DDXType,(void *& )polDDXCallBackTable) == TRUE)
	{
		for(int i = polDDXCallBackTable->GetSize() - 1; i >= 0; i--)
		{
			if ((*polDDXCallBackTable)[i].Instance == polDDXRegistration->Instance)
			{
				polDDXCallBackTable->RemoveAt(i);
				break;
			}
		}
		if(polDDXCallBackTable->GetSize() == 0)
		{
			KeyMap.RemoveKey((void *)polDDXRegistration->DDXType);
			delete polDDXCallBackTable;
		}
	}
}







void CCSDdx::UnRegisterAll()
{
	CCSPtrArray<DDXCallBackEntry> *polDDXCallBackTable;
	POSITION pos;
	void *pVoid;
	for( pos = KeyMap.GetStartPosition(); pos != NULL; )
	{
		KeyMap.GetNextAssoc( pos, pVoid , (void *&)polDDXCallBackTable );
		polDDXCallBackTable->RemoveAll();
		delete polDDXCallBackTable;
	}
	KeyMap.RemoveAll();
	DDXCallBackTable.DeleteAll();
}


void CCSDdx::DataChanged(void *vpInstance, int ipDDXType, void *vpDataPointer )
{
	CCSPtrArray<DDXCallBackEntry> *polDDXCallBackTable;
	DDXCallBackEntry *polDDXRegistration;


	if(KeyMap.Lookup((void *)ipDDXType,(void *& )polDDXCallBackTable) == TRUE)
	{
		for(int i = polDDXCallBackTable->GetSize() - 1; i >= 0; i--)
		{
			polDDXRegistration = &((*polDDXCallBackTable)[i]);
			polDDXRegistration->CallBack(polDDXRegistration->Instance,polDDXRegistration->DDXType,vpDataPointer, polDDXRegistration->InstanceName);
		}
	}

/*
	for (int ilLc = 0; ilLc < DDXCallBackTable.GetSize(); ilLc++)
	{
		polDDXRegistration = &DDXCallBackTable[ilLc];

		if (polDDXRegistration->DDXType == ipDDXType)
		{
			polDDXRegistration->CallBack(polDDXRegistration->Instance,
					polDDXRegistration->DDXType,
				vpDataPointer, polDDXRegistration->InstanceName);
		}
	}
*/
}
