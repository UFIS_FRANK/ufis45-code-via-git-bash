// CCSMessageBox.cpp: implementation of the CCSMessageBox class.
//
//////////////////////////////////////////////////////////////////////

#include <stdafx.h>
#include <CCSMessageBox.h>

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CCSMessageBox   ogMessageBox;


int CCSMessageBox::imAnzahl = 0;


CCSMessageBox::CCSMessageBox()
{

}

CCSMessageBox::~CCSMessageBox()
{

}

bool CCSMessageBox::IsShown()
{
    if(imAnzahl <= 0)
        return false;
    else
        return true;
}

int CCSMessageBox::Show(CWnd *popWnd,LPCTSTR lpszText,LPCTSTR lpszCaption,UINT nType)
{

	if (popWnd != NULL)
	{
		imAnzahl++;
		int ilRc = popWnd->MessageBox(lpszText,lpszCaption,nType);
		imAnzahl--;
		return ilRc;

	}
	else
	{
		imAnzahl++;
		int ilRc = ::MessageBox(NULL,lpszText,lpszCaption,nType);
		imAnzahl--;
		return ilRc;

	}
	return IDCANCEL;
}
