// ccsbuttonctrl.h : header file
//
#ifndef _CCSBUTTONCTRL_H_
#define _CCSBUTTONCTRL_H_

/////////////////////////////////////////////////////////////////////////////
// CCSButtonCtrl frame

#include <afxwin.h>
#include <ccsdragdropctrl.h>


class CCSButtonCtrl : public CButton
{
    DECLARE_DYNCREATE(CCSButtonCtrl)
public:
    CCSButtonCtrl(bool bpWithRecess = false);            // protected constructor used by dynamic creation
    virtual ~CCSButtonCtrl();

	CCSButtonCtrl(const CCSButtonCtrl& s);
	const CCSButtonCtrl& operator= ( const CCSButtonCtrl& s);
    
	void SetSecState(char cpKey);

	void EnableWindow(BOOL bpEnable);
	BOOL ShowWindow( int nCmdShow );

//MWO 06.02.202
	void DragDropRegister();	

	CCSDragDropCtrl omDragDrop;
	CWnd  *pomParent;

//END MWO
// Attributes
protected:
	char cmKey;

	bool bmWithRecess;
// Attributes
    // This member makes a C2StateButton different from a plain CButton.
    // Its value controls the appearance of the push button (recessed or protuded).
    //
    bool bmIsRecessed;

    COLORREF    m_BtnFace;
    COLORREF    m_BtnShadow;
    COLORREF    m_BtnHilite;
    COLORREF    m_BtnFaceDisabled;

// Operations
public:
	void SetInflateTextRectHight(int ipPixel);
	void SetInflateTextRectWidth(int ipPixel);
	void SetColors(COLORREF lpBtnFace,COLORREF lpBtnShadow,COLORREF BtnHilite);
// Operations
    // Recess(true) makes the button pressed (let's say recessed). Recess(false)
    // makes a pressed button released (let's say protuded). Calling Recess() with
    // no parameters returns current state of the 2-state push button.
    //
    void Recess(bool bpIsRecessed);
    bool Recess();



// Implementation
protected:
    virtual void DrawItem(LPDRAWITEMSTRUCT lpDIS);
    void DrawOuterFrame(HDC dc, CRect& rect, bool bThick);

    // Generated message map functions
    //{{AFX_MSG(CCSButtonCtrl)
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
    afx_msg UINT OnGetDlgCode();
    afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
    afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	//MWO 06.02.2002
    afx_msg LONG OnDragOver(UINT, LONG); 
    afx_msg LONG OnDrop(UINT, LONG); 
    afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	//END MWO
	afx_msg void OnKillFocus(CWnd* pNewWnd);
	//afx_msg void OnEnable(BOOL bEnable);
	//}}AFX_MSG
    DECLARE_MESSAGE_MAP()
private:
	int imInflateRectPixelHight;
	int imInflateRectPixelWidth;
};

/////////////////////////////////////////////////////////////////////////////


class CCSTableButton : public CCSButtonCtrl
{
public:
    //DECLARE_DYNCREATE(CTableButton)
public:
    CCSTableButton(int ipCell_X, int ipCell_Y, bool bpWithRecess = false);            // protected constructor used by dynamic creation
    virtual ~CCSTableButton();

	int imTablePos_X;
	int imTablePos_Y;
    // Generated message map functions
    //{{AFX_MSG(CCSButtonCtrl)
    afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	//}}AFX_MSG
    DECLARE_MESSAGE_MAP()
};

#endif //_BUTTONCT_H_
