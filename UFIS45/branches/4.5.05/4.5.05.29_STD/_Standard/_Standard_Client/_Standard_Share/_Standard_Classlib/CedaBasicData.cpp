// CedaBasicData.cpp: implementation of the CedaBasicData class.
//
//////////////////////////////////////////////////////////////////////

#include <stdafx.h>
#include <CedaBasicData.h>
#include <CCSDefines.h>
#include <CCSBasicFunc.h>



#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif



static int bgUseCedaAction2 = -1;

// Local function prototype
static void ProcessUniDataCf(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);



////////////////////////////////////////////////////////////////////////////
// DDX
//
void  ProcessUniDataCf(void * popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	switch(ipDDXType)
	{
	case BC_UNI_CHANGE :
		((CedaBasicData *)popInstance)->ProcessUniDataBc(ipDDXType, vpDataPointer,ropInstanceName);
		break;
	}
}



//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////


CedaBasicData::CedaBasicData(CString opTableExt, CCSDdx *popDdx, CCSBcHandle *popBcHandle, CCSCedaCom *popCommHandler, CCSBasic *popBasic)
{
	pomBasic = popBasic;
	pomDdx = popDdx;
	pomBcHandle = popBcHandle;
	pomCommHandler  = popCommHandler;
	omTableExt = opTableExt;
	pomDdx->Register((void *)this,BC_UNI_CHANGE, CString("UNIDATA"), CString("UniData changed"),ProcessUniDataCf);
	pomOdbc = new UfisOdbc(CString(""));

	if (bgUseCedaAction2 == -1)
	{
		char pclTmpText[512];
		char pclConfigPath[512];
		if (getenv("CEDA") == NULL)
			strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
		else
			strcpy(pclConfigPath, getenv("CEDA"));

		GetPrivateProfileString("GLOBAL", "USECEDAACTION2", "YES",pclTmpText, sizeof pclTmpText, pclConfigPath);
		bgUseCedaAction2 = (strcmp(pclTmpText,"YES") == 0) ? true : false;
	}
}

CedaBasicData::~CedaBasicData()
{
	CedaObjects.DeleteAll();
	pomDdx->UnRegister(this,NOTUSED);
	delete pomOdbc;
}

//MWO 23.12.98
bool CedaBasicData::InitReferences()
{
	return true;
}


void CedaBasicData::SetSystabErrorMsg(CString opSystabErrorMsg, CString opSystabErrorCaptionMsg)
{
	strcpy( CedaObject::SystabErrorCaptionMsg , opSystabErrorCaptionMsg);
	strcpy( CedaObject::SystabErrorMsg , opSystabErrorMsg);
}



bool CedaBasicData::GetFieldSet(CString opObject, CCSPtrArray<FieldSet> &ropFieldSet) 
{
	bool blRet = false;
	CedaObject *prlObject; 
	if(omObjectMap.Lookup((LPCSTR)opObject,(void *& )prlObject) == TRUE)
	{
		blRet = true;
		for(int i = 0; i < prlObject->Desc.GetSize(); i++)
		{
			ropFieldSet.Add(&prlObject->Desc[i]);
		}
	}
	return blRet;
}

bool CedaBasicData::GetPossibleLookupValues(CString opObject, CString opField, CStringArray &ropValues)
{
	bool blRet = false;
	CedaObject *prlObject; 
	if(omObjectMap.Lookup((LPCSTR)opObject,(void *& )prlObject) == TRUE)
	{
		FieldSet *prlFielSet = NULL;
		if(prlObject->omFieldSetMap.Lookup((LPCSTR)opField,(void *& )prlFielSet) == TRUE)
		{
			blRet = true;
			if(prlFielSet->pomRefObject != NULL)
			{
				prlFielSet->omPossibleValues.pomRefObject->GetAllValuesForField(opField, ropValues);
			}
			else
			{
				for(int i = 0; i < prlFielSet->omPossibleValues.DBValues.GetSize(); i++)
				{
					ropValues.Add(prlFielSet->omPossibleValues.DBValues[i]);
				}
			}
		}
	}
	return blRet;
}

//END MWO

bool CedaBasicData::CloneFieldSet(CString opObject, CCSPtrArray<FieldSet> &ropFields)
{
	CedaObject *prlObject; 
	bool blRet = false;
	if(omObjectMap.Lookup((LPCSTR)opObject,(void *& )prlObject) == TRUE)
	{
		blRet = true;
		for(int i = 0; i < prlObject->Desc.GetSize(); i++)
		{
			ropFields.NewAt(ropFields.GetSize(), prlObject->Desc[i]);
		}
	}
	return blRet;
}

bool CedaBasicData::SetLogicalFields(CString opObject, CCSPtrArray<FieldSet> opFields)
{
	CedaObject *prlObject; 
	bool blRet = false;
	if(omObjectMap.Lookup((LPCSTR)opObject,(void *& )prlObject) == TRUE)
	{
		blRet = prlObject->SetLogicalFields(opFields);
	}
	return blRet;
}

bool CedaBasicData::SetObject(CString opObject, CString opFieldList,bool bpUseOwnBc,CALLBACKFUNC pfpFunc)
{
	return SetObject(opObject, opObject, opFieldList,bpUseOwnBc,pfpFunc);
}


bool CedaBasicData::SetObject(CString opObject, CCSPtrArray<FieldSet> opFieldSet,bool bpUseOwnBc,CALLBACKFUNC pfpFunc)
{
	CedaObject *prlObject = new CedaObject(opObject, opObject, omTableExt, pomBasic); 

	CedaObjects.InsertAt(0,prlObject);
	
	bool blRet;
	blRet = prlObject->Init(opFieldSet);
	
	if(!blRet)
	{
		CedaObjects.DeleteAt(0);
	}
	else
	{
		prlObject->SetBroadcastCallback(pfpFunc);
		omObjectMap.SetAt((LPCSTR) opObject, (void *) prlObject );	
		pomBcHandle->AddTableCommand(opObject + omTableExt, CString("UBT"), BC_UNI_CHANGE, bpUseOwnBc,pcmApplName);
		pomBcHandle->AddTableCommand(opObject + omTableExt, CString("IBT"), BC_UNI_CHANGE, bpUseOwnBc,pcmApplName);
		pomBcHandle->AddTableCommand(opObject + omTableExt, CString("IRT"), BC_UNI_CHANGE, bpUseOwnBc,pcmApplName);
		pomBcHandle->AddTableCommand(opObject + omTableExt, CString("URT"), BC_UNI_CHANGE, bpUseOwnBc,pcmApplName);
		pomBcHandle->AddTableCommand(opObject + omTableExt, CString("DRT"), BC_UNI_CHANGE, bpUseOwnBc,pcmApplName);
	}

	return blRet;
}


bool CedaBasicData::RemoveObject( CString opObject)
{
	bool blRet = false;
	CedaObject *prlObject; 
	if(omObjectMap.Lookup((LPCSTR)opObject,(void *& )prlObject) == TRUE)
	{

		for( int i = 0; i < CedaObjects.GetSize(); i++)
		{
			if( &CedaObjects[i] == prlObject)
			{
				pomBcHandle->RemoveTableCommand(opObject + omTableExt, CString("UBT"), BC_UNI_CHANGE);
				pomBcHandle->RemoveTableCommand(opObject + omTableExt, CString("IBT"), BC_UNI_CHANGE);
				pomBcHandle->RemoveTableCommand(opObject + omTableExt, CString("IRT"), BC_UNI_CHANGE);
				pomBcHandle->RemoveTableCommand(opObject + omTableExt, CString("URT"), BC_UNI_CHANGE);
				pomBcHandle->RemoveTableCommand(opObject + omTableExt, CString("DRT"), BC_UNI_CHANGE);

				omObjectMap.RemoveKey((LPCSTR) opObject);	

				CedaObjects.DeleteAt(i);
				i = CedaObjects.GetSize();
			}
		}

		blRet = true;
	}
	return blRet;
}




bool CedaBasicData::SetObject(CString opAlias, CString opObject, CString opFieldList,bool bpUseOwnBc,CALLBACKFUNC pfpFunc)
{
	CedaObject *prlObject = new CedaObject(opAlias, opObject, omTableExt, pomBasic); 

	CedaObjects.InsertAt(0,prlObject);
	
	bool blRet;
	if(omAccessMethod == "ODBC")
		blRet = prlObject->Init(opFieldList, pomOdbc);
	else
		blRet = prlObject->Init(opFieldList);
	
	if(!blRet)
	{
		CedaObjects.DeleteAt(0);
	}
	else
	{
		prlObject->SetBroadcastCallback(pfpFunc);
		omObjectMap.SetAt((LPCSTR) opAlias, (void *) prlObject );	
		pomBcHandle->AddTableCommand(opObject + omTableExt, CString("UBT"), BC_UNI_CHANGE, bpUseOwnBc,pcmApplName);
		pomBcHandle->AddTableCommand(opObject + omTableExt, CString("IBT"), BC_UNI_CHANGE, bpUseOwnBc,pcmApplName);
		pomBcHandle->AddTableCommand(opObject + omTableExt, CString("IRT"), BC_UNI_CHANGE, bpUseOwnBc,pcmApplName);
		pomBcHandle->AddTableCommand(opObject + omTableExt, CString("URT"), BC_UNI_CHANGE, bpUseOwnBc,pcmApplName);
		pomBcHandle->AddTableCommand(opObject + omTableExt, CString("DRT"), BC_UNI_CHANGE, bpUseOwnBc,pcmApplName);
	}

	return blRet;
}


void CedaBasicData::SetObjectDesc(CString opObject, CString opDesc)
{
	CedaObject *prlObject; 
	if(omObjectMap.Lookup((LPCSTR)opObject,(void *& )prlObject) == TRUE)
	{
		prlObject->SetObjectDesc(opDesc);
	}
}


CString CedaBasicData::GetObjectDesc(CString opObject)
{
	CString olRet;
	CedaObject *prlObject; 
	if(omObjectMap.Lookup((LPCSTR)opObject,(void *& )prlObject) == TRUE)
	{
		olRet = prlObject->GetObjectDesc();
	}
	return olRet;
}


int CedaBasicData::GetMaxFieldLength(CString opObject, CString opField)
{
	int ilRet = -1;
	CedaObject *prlObject; 
	if(omObjectMap.Lookup((LPCSTR)opObject,(void *& )prlObject) == TRUE)
	{
		ilRet = prlObject->GetMaxFieldLength(opField);
	}
	return ilRet;
}



bool CedaBasicData::AddKeyMap(CString opObject, CString opField)
{
	bool blRet = false;
	CedaObject *prlObject; 
	if(omObjectMap.Lookup((LPCSTR)opObject,(void *& )prlObject) == TRUE)
	{
		blRet = prlObject->AddKeyMap(opField);
	}
	return blRet;
}

CMapStringToPtr * CedaBasicData::GetMapPointer(CString opObject, CString opField)
{
	CMapStringToPtr *prlMapPointer = NULL;
	CedaObject *prlObject; 
	if(omObjectMap.Lookup((LPCSTR)opObject,(void *& )prlObject) == TRUE)
	{
		prlMapPointer = prlObject->GetMapPointer(opField);
	}
	return prlMapPointer;

}


bool CedaBasicData::CheckSMWhere(CString opWhere)
{
	if(opWhere.GetLength() == 0)
		return true;

	opWhere.MakeUpper();

	if((opWhere.Find("AND") >= 0) && (opWhere.Find("OR") >= 0))
		return false;

	if(opWhere.Find("(") >= 0)
		return false;

	if(opWhere.Find("LIKE") >= 0)
		return false;

	return true;
}



int CedaBasicData::Read(CString opObject, CString opWhere, CCSPtrArray<RecordSet> *pomBuffer, bool bpDeleteBuffer /*= true*/)
{
	return ReadInternal(opObject, opWhere, pomBuffer, true, bpDeleteBuffer);
}


int CedaBasicData::ReadInternal(CString opObject, CString opWhere, CCSPtrArray<RecordSet> *pomBuffer, bool bpSetSel, bool bpDeleteBuffer /*= true*/)
{
	CedaObject *prlObject; 
	char* pclSelection = 0;
	char pclTable[64];
	char pclFieldList[2048];
	char pclData[256] = "";
	char pclCmd[16] = "";
	CString olDataList;
	int ilCount = 0;
	bool blRet; 
	bool blUseCedaAction2 = bgUseCedaAction2 != 0;
	
	pclSelection = (char*)malloc(opWhere.GetLength()+1);
	if(!pclSelection) 
		return 0;

	strcpy(pclSelection, opWhere);

	//strcpy(pclTable, opObject);
	//strcat(pclTable, omTableExt);


	CString olTable;


	if (omObjectMap.Lookup((LPCSTR)opObject,(void *& )prlObject) == TRUE)
	{
		strcpy(pclFieldList, prlObject->GetDBFieldList());
		
		olTable = prlObject->GetTableName();

		strcpy(pclTable, olTable);
		strcat(pclTable, omTableExt);


		if(pomBuffer == NULL)
		{
			if (bpDeleteBuffer)
			{
				prlObject->ClearAll();
			}
		}
		else
		{
			if (bpDeleteBuffer)
			{
				pomBuffer->DeleteAll();
			}
		}

		if(opWhere.IsEmpty())
			prlObject->SetReadAll(true);
		else
			prlObject->SetReadAll(false);
		
		if(bpSetSel)
			prlObject->SetSelection(opWhere);


/*
		CTime olTime = CTime::GetCurrentTime();
		TRACE("\nbefore ReadAll  <%s>  %s", opObject,olTime.Format("%H:%M:%S"));

*/
		
		strcpy(pclCmd, "RT");
		
		if(prlObject->SharedMem())
		{
			if(CheckSMWhere(opWhere))
			{
				strcpy(pclCmd, "SYS");
				blUseCedaAction2 = false;
			}
		}

	    CStringArray *polDataBuf = GetDataBuff();
		polDataBuf->RemoveAll();
		int ilLineNo;

		if (pomBuffer == NULL)
		{
			if(omAccessMethod == "ODBC" || omAccessMethod == "READ_ODBC")
			{
				pomOdbc->CallDB("Select", CString(pclTable), CString(pclFieldList), CString(pclSelection), "",  prlObject->GetDataBuffer());
				pomOdbc->RemoveInternalData();

			}
			else if (blUseCedaAction2)
			{
				blRet = CedaAction2(pclCmd, pclTable, pclFieldList, pclSelection, "", pclData, "BUF1", false, 0, prlObject->GetDataBuffer(), prlObject->GetFieldCount());
			}
			else
			{
				blRet = CedaAction(pclCmd, pclTable, pclFieldList, pclSelection, "", pclData, "BUF1", false, 0, prlObject->GetDataBuffer(), prlObject->GetFieldCount());
			}
			prlObject->CreateKeyMap();
		}
		else
		{
			if(omAccessMethod == "ODBC" || omAccessMethod == "READ_ODBC")
				;
			else if (blUseCedaAction2)
				blRet = CedaAction2(pclCmd, pclTable, pclFieldList, pclSelection, "", pclData, "BUF1");
			else
				blRet = CedaAction(pclCmd, pclTable, pclFieldList, pclSelection, "", pclData, "BUF1");
		}
/*
		olTime = CTime::GetCurrentTime();
		TRACE("\nafter Cedaact   <%s>  %s", opObject,olTime.Format("%H:%M:%S"));
*/

		if(omAccessMethod == "ODBC" || omAccessMethod == "READ_ODBC")
		{
			ilCount = pomOdbc->GetLineCount();
		}
		else if (blUseCedaAction2)
		{
			if (pomBuffer != NULL)
			{
				bool blOk = true;
				ilCount = 0;
				for (ilLineNo = 0; blOk == true; ilLineNo++)
				{
					CString olLine;
					blOk = GetBufferLine2(ilLineNo,olLine);
					if (blOk)
					{
						prlObject->AddRecord(pomBuffer,olLine);
						++ilCount;
					}
				}
			}
		}
		else
		{
			if (pomBuffer != NULL)
			{
				bool blOk = true;
				ilCount = 0;
				for (ilLineNo = 0; blOk == true; ilLineNo++)
				{
					CString olLine;
					blOk = GetBufferLine(ilLineNo,olLine);
					if (blOk)
					{
						prlObject->AddRecord(pomBuffer,olLine);
						++ilCount;
					}
				}
			}
		}
/*
		olTime = CTime::GetCurrentTime();
		TRACE("\nafter fillstruct<%s>  %s", opObject,olTime.Format("%H:%M:%S"));
		TRACE("\n<%s>  %d Datens�tze gelesen \n\n", opObject,ilCount);
*/
	}

	if (pclSelection != 0)
		free((void*)pclSelection);

	return ilCount;
}
CString CedaBasicData::GetLastError()
{
	CString olError;
	if(omAccessMethod == "ODBC")
	{
		olError = pomOdbc->omLastErrorMessage;
	}
	else
	{
		if(omLastErrorMessage.Find("ORA-") != -1)
		{
			olError = omLastErrorMessage;
		}
	}
	return olError;
}

int CedaBasicData::SelectCount(CString opObject, CString opWhere /*= ""*/)
{
	char* pclSelection = NULL;
	char pclTable[64];
	char pclFieldList[2048];
	char pclData[256] = "";
	char pclCmd[16] = "";

	int ilCount=0;

	CCSPtrArray<RecordSet> olData;

	pclSelection = (char*)malloc(opWhere.GetLength()+1);
	if(!pclSelection) 
		return 0;
	strcpy(pclSelection, opWhere.GetBuffer(0));
	strcpy(pclTable, opObject.GetBuffer(0));
	strcat(pclTable, omTableExt);
	strcpy(pclFieldList, "count(*)");
	strcpy(pclCmd, "RT");

	if(omAccessMethod == "ODBC" || omAccessMethod == "READ_ODBC")
	{
		pomOdbc->CallDB("Select", CString(pclTable), CString(pclFieldList), CString(pclSelection), "",  &olData);
		pomOdbc->RemoveInternalData();

	}
	else
	{
		/*blRet = */CedaAction(pclCmd, pclTable, pclFieldList, pclSelection, "", pclData, "BUF1", false, 0, &olData, 1);
	}
	if(olData.GetSize() > 0)
	{
		ilCount = atoi(olData[0].Values[0]);
	}
	olData.DeleteAll();
	free(pclSelection);
	return ilCount;
}




bool CedaBasicData::UpdateSpecial(CString opObject, CString opFields, CString opWhere, CString opData)
{
	char pclSelection[512] = "";
	char pclData[2048] = "";
	char pclFieldList[2048] = "";
	char pclTable[16];
	char pclCmd[4] = "URT";	

	strcpy(pclTable, opObject);
	strcat(pclTable, omTableExt);

	strcpy(pclFieldList, opFields);
	strcpy(pclData, opData);
	strcpy(pclSelection, opWhere);

	if(omAccessMethod == "ODBC")
	{
		if(pomOdbc->CallDB(CString(pclCmd), CString(pclTable), CString(pclFieldList), CString(pclSelection), CString(pclData)) == 0)
			return true;
		else
			return false;
	}
	else
	{
		return CedaAction(pclCmd, pclTable, pclFieldList, pclSelection, "", pclData, "BUF1");
	}
}


int CedaBasicData::ReadSpecial(CString opObject, CString opFields, CString opWhere, CCSPtrArray<RecordSet> &opBuffer)
{
	CedaObject *prlObject; 
	char pclSelection[1024] = "";
	char pclTable[64];
	char pclFieldList[2048];
	char pclData[256] = "";
	char pclCmd[16] = "RT";
	CString olDataList;
	int ilCount;
	bool blRet = false; 

	if((opFields.IsEmpty()) || (opObject.IsEmpty()))
		return -1;

	opBuffer.DeleteAll();

	strcpy(pclSelection, opWhere);
	strcpy(pclTable, opObject);
	strcat(pclTable, omTableExt);


	if(omObjectMap.Lookup((LPCSTR)opObject,(void *& )prlObject) == TRUE)
	{
		strcpy(pclFieldList, opFields);
		
		CStringArray *polDataBuf = GetDataBuff();
		polDataBuf->RemoveAll();

		if(omAccessMethod == "ODBC")
		{
			if(pomOdbc->CallDB("Select", pclTable, pclFieldList, pclSelection, pclData, &opBuffer) == 0)
			{
				pomOdbc->RemoveInternalData();

				return pomOdbc->GetLineCount();
			}
			else
			{
				return -1;
			}
		}
		else
		{
			blRet = CedaAction(pclCmd, pclTable, pclFieldList, pclSelection, "", pclData, "BUF1");


			ilCount = GetBufferSize();

			for (int ilLineNo = 0;ilLineNo < ilCount; ilLineNo++)
			{
				if(GetBufferLine(ilLineNo, olDataList))
				{
					MakeClientString(olDataList);
					prlObject->AddRecord(&opBuffer, olDataList);
				}
			}
		}
	}
	if(blRet)
		return ilCount;
	else
		return -1;
}


CString  CedaBasicData::GetTableHeader(CString opObject, CString opFields, CString opTrenner)
{
	CString olRet;
	CedaObject *prlObject;
	if(omObjectMap.Lookup((LPCSTR)opObject,(void *& )prlObject) == TRUE)
	{
		olRet = prlObject->GetTableHeader(opFields, opTrenner);
	}
	return olRet;
}


void  CedaBasicData::SetTableHeader(CString opObject, CString opField, CString opHeader)
{
	CedaObject *prlObject;
	if(omObjectMap.Lookup((LPCSTR)opObject,(void *& )prlObject) == TRUE)
	{
		prlObject->SetTableHeader(opField, opHeader);
	}
}



CString  CedaBasicData::GetFields(CString opObject, int ipIndex, CString opFields, CString opTrenner, int ipFlags)
{
	CString olRet;
	CedaObject *prlObject;
	if(omObjectMap.Lookup((LPCSTR)opObject,(void *& )prlObject) == TRUE)
	{
		olRet = prlObject->GetFields(ipIndex, opFields, opTrenner, ipFlags);
	}
	return olRet;
}


void CedaBasicData::GetAllFields(CString opObject, CString opFields, CString opTrenner, int ipFlag, CStringArray &olData)
{
	CString olRet;
	CedaObject *prlObject;
	if(omObjectMap.Lookup((LPCSTR)opObject,(void *& )prlObject) == TRUE)
	{
		prlObject->GetAllFields(opFields, opTrenner, ipFlag, olData);
	}
}

int CedaBasicData::ShowAllFields(CString opObject, CString opFields, CListBox *polList, CString opSelString, bool bpTrimRight /*false*/, bool bpAddBlankLine /*false*/)
{
	int ilRet = -1;
	CedaObject *prlObject;
	if(omObjectMap.Lookup((LPCSTR)opObject,(void *& )prlObject) == TRUE)
	{
		ilRet = prlObject->ShowAllFields(opFields, polList, opSelString, bpTrimRight, bpAddBlankLine);
	}
	return ilRet;
}




CString CedaBasicData::GetField(CString opObject, int ipIndex, CString opField)
{
	CString olRet;
//MWO 25.08.1999 
//if the field is not valid ==> function returns "NULL"
	olRet = CString("NULL");
	CedaObject *prlObject;
	if(omObjectMap.Lookup((LPCSTR)opObject,(void *& )prlObject) == TRUE)
	{
		olRet = prlObject->GetField(ipIndex, opField);
	}
	return olRet;
}


CString CedaBasicData::GetField(CString opObject, CString opRefField, CString opRefValue, CString opField)
{
	CString olRet;
	GetField(opObject, opRefField, opRefValue, opField, olRet);
	return olRet;
}



bool CedaBasicData::GetFieldBetween(CString opObject, CString opRefFieldFrom, CString opRefFieldTo, CString opRefValue, CString opField, CString &opReturn)
{
	bool blRet = false;
	CedaObject *prlObject;
	if(omObjectMap.Lookup((LPCSTR)opObject,(void *& )prlObject) == TRUE)
	{
		blRet = prlObject->GetFieldBetween(opRefFieldFrom, opRefFieldTo, opRefValue, opField, opReturn);
	}
	return blRet;
}



bool  CedaBasicData::GetFields(CString opObject, CString opRefField, CString opRefValue, CString opField1, CString opField2, CString &opReturn1, CString &opReturn2)
{
	bool blRet = false;
	CedaObject *prlObject;
	if(omObjectMap.Lookup((LPCSTR)opObject,(void *& )prlObject) == TRUE)
	{
		blRet = prlObject->GetFields(opRefField, opRefValue, opField1, opField2, opReturn1, opReturn2);
	}
	return blRet;
}


bool CedaBasicData::GetField(CString opObject, CString opRefField1, CString opRefField2, CString opRefValue, CString &opReturn1, CString &opReturn2, CString opValDate)
{
	bool blRet = false;
	CedaObject *prlObject;
	if(omObjectMap.Lookup((LPCSTR)opObject,(void *& )prlObject) == TRUE)
	{
		blRet = prlObject->GetField( opRefField1, opRefField2, opRefValue, opReturn1, opReturn2, opValDate);
	}
	return blRet;
}


bool CedaBasicData::GetField(CString opObject, CString opRefField, CString opRefValue, CString opField, CString &opReturn)
{
	bool blRet = false;
	CedaObject *prlObject;
	
	if(omObjectMap.Lookup((LPCSTR)opObject,(void *& )prlObject) == TRUE)
	{

		if((prlObject->IsFieldAvailabe(opRefField)) && (prlObject->IsFieldAvailabe(opField)))
		{
			blRet = prlObject->GetField(opRefField, opRefValue, opField, opReturn);
		
		}
	}
	return blRet;
}


int CedaBasicData::GetDataCount(CString opObject)
{
	int ilRet = 0;
	CedaObject *prlObject; 
	if(omObjectMap.Lookup((LPCSTR)opObject,(void *& )prlObject) == TRUE)
	{
		ilRet = prlObject->GetDataCount();
	}
	return ilRet;
}



bool CedaBasicData::GetRecord(CString opObject, int ilIndex,  RecordSet &rpRecord)
{
	bool blRet = false;
	CedaObject *prlObject; 

	if(omObjectMap.Lookup((LPCSTR)opObject,(void *& )prlObject) == TRUE)
	{
		blRet = prlObject->GetRecord(ilIndex,  rpRecord);
	}
	return blRet;
}


void CedaBasicData::GetRecords(CString opObject, CString opRefField, CString opRefValue, CCSPtrArray<RecordSet> *prpRecords)
{
	CedaObject *prlObject; 

	if(omObjectMap.Lookup((LPCSTR)opObject,(void *& )prlObject) == TRUE)
	{
		prlObject->GetRecords(opRefField, opRefValue, prpRecords);
	}
}

int CedaBasicData::GetAllRecords(CString opObject, CCSPtrArray <RecordSet> &ropRecords)
{
	CedaObject *prlObject;
	
	if(omObjectMap.Lookup((LPCSTR)opObject,(void *& )prlObject) == TRUE)
	{
		return prlObject->GetAllRecords(ropRecords);
	}
	
	return -1;
}

bool CedaBasicData::GetRecord(CString opObject, CString opRefField, CString opRefValue, RecordSet &rpRecord)
{
	bool blRet = false;
	CedaObject *prlObject; 

	if(omObjectMap.Lookup((LPCSTR)opObject,(void *& )prlObject) == TRUE)
	{
		blRet = prlObject->GetRecord(opRefField, opRefValue, rpRecord);

	}
	return blRet;
}


////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////
// Data manipulation and save

bool CedaBasicData::InsertRecord(CString opObject, RecordSet &rpRecord, bool bpDbSave)
{
	bool blRet = false;
	CedaObject *prlObject;
	char pclUrno[65];
	if(omObjectMap.Lookup((LPCSTR)opObject,(void *& )prlObject) == TRUE)
	{
		ltoa(GetNextUrno(), pclUrno, 10);
		blRet = prlObject->InsertRecord( rpRecord, CString(pclUrno));
	}
	if(blRet && bpDbSave)
		blRet = Save(opObject);
	return blRet;
}


bool CedaBasicData::SetRecord(CString opObject, CString opRefField, CString opRefValue, CStringArray &opRecord, bool bpDbSave)
{
	bool blRet = false;
	CedaObject *prlObject;
	
	if(omObjectMap.Lookup((LPCSTR)opObject,(void *& )prlObject) == TRUE)
	{
		blRet = prlObject->SetRecord( opRefField, opRefValue, opRecord);
	}
	if(blRet && bpDbSave)
		blRet = Save(opObject);
	return blRet;
}


bool CedaBasicData::SetField(CString opObject, CString opRefField, CString opRefValue, CString opField, CString opValue, bool bpDbSave)
{
	bool blRet = false;
	CedaObject *prlObject;
	
	if(omObjectMap.Lookup((LPCSTR)opObject,(void *& )prlObject) == TRUE)
	{
		blRet = prlObject->SetField(opRefField, opRefValue, opField, opValue);
	}
	if(blRet && bpDbSave)
		blRet = Save(opObject);
	return blRet;
}



bool CedaBasicData::DeleteRecord(CString opObject, CString opRefField, CString opRefValue, bool bpDbSave)
{
	bool blRet = false;
	CedaObject *prlObject;
	
	if(omObjectMap.Lookup((LPCSTR)opObject,(void *& )prlObject) == TRUE)
	{
		blRet = prlObject->DeleteRecord(opRefField, opRefValue);
	}
	if(blRet && bpDbSave)
		blRet = Save(opObject);
	return blRet;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////



void CedaBasicData::TraceData(CString opObject)
{
	CedaObject *prlObject; 
	if(omObjectMap.Lookup((LPCSTR)opObject,(void *& )prlObject) == TRUE)
	{
		prlObject->TraceData();
	}

}


void CedaBasicData::SetSort(CString opObject, CString opSortList, bool bpDo, bool bpNumeric)
{
	CedaObject *prlObject; 
	if(omObjectMap.Lookup((LPCSTR)opObject,(void *& )prlObject) == TRUE)
	{
		prlObject->SetSort(opSortList, bpDo, bpNumeric);
	}
}

void CedaBasicData::Sort(CString opObject)
{
	CedaObject *prlObject; 
	if(omObjectMap.Lookup((LPCSTR)opObject,(void *& )prlObject) == TRUE)
	{
		prlObject->Sort();
	}
}




int CedaBasicData::GetFieldCount(CString opObject)
{
	int ilRet = 0;
	CedaObject *prlObject; 
	if(omObjectMap.Lookup((LPCSTR)opObject,(void *& )prlObject) == TRUE)
	{
		ilRet = prlObject->GetFieldCount();
	}
	return ilRet;
}

//MWO 25.08.1999 
//if the fieldindex is not valid ==> function returns -1
int CedaBasicData::GetFieldIndex(CString opObject, CString opField)
{
	int ilRet = -1;	
	CedaObject *prlObject; 
	if(omObjectMap.Lookup((LPCSTR)opObject,(void *& )prlObject) == TRUE)
	{
		prlObject->GetFieldIndex(opField, ilRet);
	}
	return ilRet;
}



void  CedaBasicData::ProcessUniDataBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	if(ipDDXType == BC_UNI_CHANGE)
		ProcessUniDataBcInternal( vpDataPointer, ropInstanceName);
}

void  CedaBasicData::ProcessUniDataBcInternal(void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlBC;
	long llUrno = 0;;
	prlBC = (struct BcStruct *) vpDataPointer;
	CedaObject *prlObject; 
	CString olSelection = (CString)prlBC->Selection;
	CString olObject = (CString)prlBC->Object;
	CString olProject = olObject.Right(3);
	olObject = olObject.Left(3);
	CString olUrno;
	char buffer[64];
	CString olCmd; 
	int ilDdxType; 
	RecordSet *prlRecord;

	CCSPtrArray<RecordSet> olRecords;

	if(olProject != omTableExt)
		return;

	if(omObjectMap.Lookup((LPCSTR)olObject,(void *& )prlObject) == FALSE)
		return;

	if(!prlObject->IsDataLoad())
		return;


	if(prlObject->SentBcBufferOverflow( ilDdxType ))
	{
		pomDdx->DataChanged((void *)this, ilDdxType, NULL );
		return;
	}

	if(prlObject->BufferBc( prlBC ))
	{
		return;
	}

	olSelection.TrimRight();

	if(!olSelection.IsEmpty())
	{
		if (olSelection.Find('\'') != -1)
		{
			llUrno = GetUrnoFromSelection(prlBC->Selection);
		}
		else
		{
			int ilFirst = olSelection.Find("=")+1;
			int ilLast  = olSelection.GetLength();
			llUrno = atol(olSelection.Mid(ilFirst,ilLast-ilFirst));
		}
	}

	if(llUrno <= 0)
	{
		CStringArray olTmp;
		int ilIndex = -1;

		ExtractItemList( CString(prlBC->Fields), &olTmp);
//MWO 25.08.1999
		int ilFieldCount = olTmp.GetSize();

		for ( int i = 0; i < ilFieldCount; i++ )
		{
			if ( olTmp[i] == "URNO" )
			{
				ilIndex = i;
				break;
			}
		}

		if ( ilIndex >= 0 )
		{
			ExtractItemList ( CString ( prlBC->Data ), &olTmp );
			int ilDataCount = olTmp.GetSize();	
			
			if (ilDataCount >= ilFieldCount)
			{
				llUrno = atoi ( olTmp[ilIndex] );
			}
			else
			{
				llUrno = -1;
			}
		}
/*
		for(int i = 0; i < olTmp.GetSize(); i++)
		{
			if(olTmp[i] == "URNO")
			{
				ilIndex = i;
				break;
			}
		}

		if(ilIndex >= 0)
		{
			ExtractItemList( CString(prlBC->Data), &olTmp);

			llUrno = atoi(olTmp[ilIndex]);
		}
*/
//MWO END
	}


	if(llUrno <= 0)
		return;

	ltoa(llUrno, buffer,  10);
	olUrno = CString(buffer);


	CString olFieldList(prlBC->Fields);

	prlRecord = new RecordSet();


	CString olSel = prlObject->GetSelection();


	if((!olSel.IsEmpty()) && (strcmp(prlBC->Cmd, "DRT") != 0))
	{
		// check, if we have a user defined broadcast callback function
		int ilResult = -1;
		CALLBACKFUNC pflFunc = prlObject->GetBroadcastCallback();
		if (pflFunc != NULL)
		{
			ilResult = pflFunc(prlBC);
			if (ilResult == TRUE)
			{
				if(prlObject->UpdateInternal(olUrno,prlBC->Fields,prlBC->Data, prlRecord))
				{
					if(prlObject->GetDdxType(CString(prlBC->Cmd), ilDdxType))
						pomDdx->DataChanged((void *)this, ilDdxType, (void *) prlRecord );
				}
				else
				{
					if(prlObject->InsertInternal(olFieldList,prlBC->Data, prlRecord))
					{
						if(prlObject->GetDdxType("IRT", ilDdxType))
							pomDdx->DataChanged((void *)this, ilDdxType, (void *) prlRecord );
					}
				}

				return;
			}
		}

		if (ilResult == -1)	// use old method
		{
			CString olDBType;
			olDBType = prlObject->GetDBFieldType("URNO");
			if (olDBType == "N")
				olSel.Format("WHERE URNO = %ld  AND (%s)", llUrno , olSel.Right(olSel.GetLength() - 6));
			else
				olSel.Format("WHERE URNO = '%ld' AND (%s)", llUrno , olSel.Right(olSel.GetLength() - 6));

			// check whether we must regard this BC or not
			// Note : if this internal read is an performance issue, we can use an free-form SQL statement instead
			// i.e. CedaAction("SQL",",,READ,1","","SELECT COUNT(*) FROM xxx WHERE yyy","","","BUF1")
			ReadInternal(olObject, olSel, &olRecords, false);


			if (olRecords.GetSize() != 1)
			{
				olRecords.DeleteAll();
				delete prlRecord;
				return;
			}
			else
			{
				*prlRecord =  olRecords[0];

				if(prlObject->UpdateInternal(olUrno,prlBC->Fields,prlBC->Data, prlRecord))
				{
					if(prlObject->GetDdxType(CString(prlBC->Cmd), ilDdxType))
						pomDdx->DataChanged((void *)this, ilDdxType, (void *) prlRecord );
				}
				else
				{
					if(prlObject->InsertInternal(olFieldList,prlBC->Data, prlRecord))
					{
						if(prlObject->GetDdxType("IRT", ilDdxType))
							pomDdx->DataChanged((void *)this, ilDdxType, (void *) prlRecord );
					}
				}
				olRecords.DeleteAll();
				delete prlRecord;
				return;
			}
		}

	}

	if((strcmp(prlBC->Cmd, "IRT") == 0) || (strcmp(prlBC->Cmd, "IBT") == 0))
	{
		if(prlObject->InsertInternal(olFieldList,prlBC->Data, prlRecord))
		{
			if(prlObject->GetDdxType("IRT", ilDdxType))
				pomDdx->DataChanged((void *)this, ilDdxType, (void *) prlRecord );
		}
	}
	else
	{
		if((strcmp(prlBC->Cmd, "URT") == 0) || (strcmp(prlBC->Cmd, "UBT") == 0))
		{
			if(prlObject->UpdateInternal(olUrno,prlBC->Fields,prlBC->Data, prlRecord))
			{
				if(prlObject->GetDdxType("URT", ilDdxType))
					pomDdx->DataChanged((void *)this, ilDdxType, (void *) prlRecord );
			}
		}

		if(strcmp(prlBC->Cmd, "DRT") == 0)
		{
			if(prlObject->DeleteInternal(olUrno, prlRecord))
			{
				if( prlObject->GetDdxType("DRT", ilDdxType))
					pomDdx->DataChanged((void *)this, ilDdxType, (void *) prlRecord );
			}
		}
	}
	delete prlRecord;
}

//---------------------------------------------------------------------------------------------------------


bool CedaBasicData::DeleteByWhere(CString opObject, CString opWhere)
{
	bool blRet = false;
	CedaObject *prlObject; 
	char pclTable[16];
	char pclCmd[4];	
	char pclSelection[512];
	char pclData[2048] = "";
	char pclFieldList[2048] = "";

	
	if(omObjectMap.Lookup((LPCSTR)opObject,(void *& )prlObject) == TRUE)
	{
		strcpy(pclTable, opObject);
		strcat(pclTable, omTableExt);
		strcpy(pclCmd, "DRT");
		if(!opWhere.IsEmpty())
		{
			strcpy(pclSelection, opWhere);
		}
		if(omAccessMethod == "ODBC")
		{
			if(pomOdbc->CallDB("Delete", pclTable, pclFieldList, pclSelection, "") == 0)
				blRet = true;
			else
				blRet = false;
		}
		else
		{
			blRet = CedaAction(pclCmd, pclTable, pclFieldList, pclSelection, "", pclData, "BUF1");
		}
	}
	return blRet;
}



bool CedaBasicData::Save(CString opObject)
{
	bool blRet = false;
	CedaObject *prlObject; 
	CString olListOfData;
	char pclData[2048] = "";
	char pclFieldList[2048] = "";
	RecordSet *prlRecord;
	char pclTable[16];
	char pclCmd[4] = "";	
	int ilFieldCount;
	CString olUrno;
	CString olTmp;
	int ilUrnoIndex;	
	CString olFieldList;
	CString olUser = CString(",") + CString(pomCommHandler->pcmUser); 
	CTime olTime = CTime::GetCurrentTime();
	CString olCurrTime = CString(",") + CTimeToDBString(olTime, TIMENULL);
	int ilCdatIdx, ilUsecIdx; 
	bool blCdatFilled, blUsecFilled;
			

	if(omObjectMap.Lookup((LPCSTR)opObject,(void *& )prlObject) == TRUE)
	{
		if(prlObject->bmIsLogicObject == false)
		{	
			ilFieldCount = prlObject->GetDBFieldCount();//MWO 21.05.99: ilFieldCount = prlObject->GetFieldCount();
			olFieldList = prlObject->GetInternalDBFieldList();

			strcpy(pclTable, opObject);
			strcat(pclTable, omTableExt);

			if(!prlObject->GetFieldIndex(CString("URNO"), ilUrnoIndex))
				return false;
			
			ilCdatIdx = GetFieldIndex(opObject, "CDAT");
			ilUsecIdx = GetFieldIndex(opObject, "USEC");

			CMapPtrToPtr *pomSave = prlObject->GetSaveRecords();


			POSITION pos;
			void *pVoid;
			for( pos = pomSave->GetStartPosition(); pos != NULL; )
			{
				char pclSelection[512] = "";
	
				pomSave->GetNextAssoc( pos, pVoid, (void *&)prlRecord);

				olListOfData = "";

				//MWO 21.05.99
				//olListOfData  = prlObject->GetInternalDataList(prlRecord);
				olListOfData  = prlObject->GetInternalDBDataList(prlRecord, omAccessMethod);
				//END MWO

				strcpy(pclFieldList, olFieldList);
				blCdatFilled = false;
				blUsecFilled = false;

				if(!olListOfData.IsEmpty())
				{
					strcpy(pclData, olListOfData);

					olUrno = prlRecord->Values[ilUrnoIndex];

					if ( prlRecord->GetStatus() == DATA_NEW )
					{
						if ( ilCdatIdx >= 0 )	// if CDAT in prlRecord and set -> use it
						{
							olTmp = prlRecord->Values[ilCdatIdx];
							olTmp.TrimRight();
							if( !olTmp.IsEmpty() )
								blCdatFilled = true;
						}
						if ( ilUsecIdx >= 0 )
						{
							olTmp = prlRecord->Values[ilUsecIdx];
							olTmp.TrimRight();
							if( !olTmp.IsEmpty() )
								blUsecFilled = true;
						}
					}

					switch(prlRecord->GetStatus())
					{
					case DATA_NEW:
						{
							if(omAccessMethod == "ODBC")
							{
								CString olData;
								CString olTmpUser; 
								olTmpUser = CString(pomCommHandler->pcmUser);
								if(olTmpUser.IsEmpty())
								{
									olTmpUser = "UNKNOWN";
								}
								olData = CString(pclData);
								if(prlObject->bmUsec)
								{
									strcat(pclFieldList, ",USEC");
									if ( blUsecFilled )
										olData += CString(",'") + prlRecord->Values[ilUsecIdx] + CString("'");
									else
										olData += CString(",'") + olTmpUser + CString("'");
								}
								if(prlObject->bmCdat)
								{
									strcat(pclFieldList, ",CDAT");
									if ( blCdatFilled )
										olData += CString(",'") + CTimeToDBString(olTime, TIMENULL) + CString("'");
									else
										olData += CString(",'") + prlRecord->Values[ilCdatIdx] + CString("'");
								}
								if(prlObject->bmUseu)
								{
									strcat(pclFieldList, ",USEU");
									olData += CString(",'") + olTmpUser + CString("'");
								}
								if(prlObject->bmLstu)
								{
									strcat(pclFieldList, ",LSTU");
									olData += CString(",'") + CTimeToDBString(olTime, TIMENULL) + CString("'");
								}
								//olData = prlObject->MakeODBCString(CString(pclData));
								if(pomOdbc->CallDB("Insert", CString(pclTable), CString(pclFieldList), CString(pclSelection), olData) == 0)
								{
									prlRecord->Status = DATA_UNCHANGED;
								}
							}
							else
							{
								if(prlObject->bmUsec )
								{
									strcat(pclFieldList, ",USEC");
									if ( blUsecFilled )
									{
										strcat(pclData, "," );
										strcat(pclData, prlRecord->Values[ilUsecIdx] );
									}
									else
										strcat(pclData, olUser);
								}
								if(prlObject->bmCdat)
								{
									strcat(pclFieldList, ",CDAT");
									if ( blCdatFilled )
									{
										strcat(pclData, "," );
										strcat(pclData, prlRecord->Values[ilCdatIdx] );
									}
									else
										strcat(pclData, olCurrTime);
								}
								if(prlObject->bmUseu)
								{
									strcat(pclFieldList, ",USEU");
									strcat(pclData, olUser);
								}
								if(prlObject->bmLstu)
								{
									strcat(pclFieldList, ",LSTU");
									strcat(pclData, olCurrTime);
								}
								strcpy(pclCmd, "IRT");
								blRet = CedaAction(pclCmd, pclTable, pclFieldList, pclSelection, "", pclData, "BUF1");
								if(blRet)
									prlRecord->Status = DATA_UNCHANGED;
							}
						}
						break;
					case DATA_CHANGED:
						{
							if(omAccessMethod == "ODBC")
							{
								CString olData;
								CString olTmpUser; 
								olTmpUser = CString(pomCommHandler->pcmUser);
								if(olTmpUser.IsEmpty())
								{
									olTmpUser = "UNKNOWN";
								}
								olData = CString(pclData);
								if(prlObject->bmUseu)
								{
									strcat(pclFieldList, ",USEU");
									olData += CString(",'") + olTmpUser + CString("'");
								}
								if(prlObject->bmLstu)
								{
									strcat(pclFieldList, ",LSTU");
									olData += CString(",'") + CTimeToDBString(olTime, TIMENULL) + CString("'");
								}
								CString olDBType;
								olDBType = prlObject->GetDBFieldType("URNO");
								if(olDBType == "N")
									sprintf(pclSelection, "WHERE URNO = %s", olUrno);
								else
									sprintf(pclSelection, "WHERE URNO = '%s'", olUrno);
								//olData = prlObject->MakeODBCString(CString(pclData));
								if(pomOdbc->CallDB("Update", CString(pclTable), CString(pclFieldList), CString(pclSelection), olData) == 0)
								{
									prlRecord->Status = DATA_UNCHANGED;
								}
							}
							else
							{
								if(prlObject->bmUseu)
								{
									strcat(pclFieldList, ",USEU");
									strcat(pclData, olUser);
								}
								if(prlObject->bmLstu)
								{
									strcat(pclFieldList, ",LSTU");
									strcat(pclData, olCurrTime);
								}

								CString olDBType;
								olDBType = prlObject->GetDBFieldType("URNO");
								if(olDBType == "N")
									sprintf(pclSelection, "WHERE URNO = %s", olUrno);
								else
									sprintf(pclSelection, "WHERE URNO = '%s'", olUrno);

								strcpy(pclCmd, "URT");
								blRet = CedaAction(pclCmd, pclTable, pclFieldList, pclSelection, "", pclData, "BUF1");
								if(blRet)
									prlRecord->Status = DATA_UNCHANGED;
							}
						}
						break;
					case DATA_DELETED:
						{
							if(omAccessMethod == "ODBC")
							{
								CString olDBType;
								olDBType = prlObject->GetDBFieldType("URNO");
								if(olDBType == "N")
									sprintf(pclSelection, "WHERE URNO = %s", olUrno);
								else
									sprintf(pclSelection, "WHERE URNO = '%s'", olUrno);
								if(pomOdbc->CallDB("Delete", CString(pclTable), CString(pclFieldList), CString(pclSelection), CString(pclData)) == 0)
								{
									prlRecord->Status = DATA_UNCHANGED;
								}
							}
							else
							{
								CString olDBType;
								olDBType = prlObject->GetDBFieldType("URNO");
								if(olDBType == "N")
									sprintf(pclSelection, "WHERE URNO = %s", olUrno);
								else
									sprintf(pclSelection, "WHERE URNO = '%s'", olUrno);

								strcpy(pclCmd, "DRT");
								blRet = CedaAction(pclCmd, pclTable, pclFieldList, pclSelection, "", pclData, "BUF1");
								if(blRet)
								{
									int ilDdxType;
									//  hag20030205 Send pointer to local copy instead of temporary pointer to
									//				orig. data, because this data may be deleted by internal data 
									//				storage, while main program is handling the event (PRF3730)
									RecordSet rlLocalCopy;
									rlLocalCopy = *prlRecord;
									prlObject->RemoveRecord(prlRecord);
									if(prlObject->GetDdxType("DRT", ilDdxType))
									{	
										//pomDdx->DataChanged((void *)this, ilDdxType, (void *) prlRecord );
										pomDdx->DataChanged((void *)this, ilDdxType, (void *) &rlLocalCopy );
									}
								}
							}
						}
						break;
					}
				}
			}
			pomSave->RemoveAll();
		}
	}
	return blRet;
}


void CedaBasicData::SetDdxType(CString opObject, CString opCmd, int ipDdxType)
{
	CedaObject *prlObject; 
	if(omObjectMap.Lookup((LPCSTR)opObject,(void *& )prlObject) == TRUE)
	{
		prlObject->SetDdxType(opCmd, ipDdxType);
	}
}



void CedaBasicData::ReleaseBcBuffer(CString opObject)
{
	CString opInstanceName;
	CedaObject *prlObject; 
	if(omObjectMap.Lookup((LPCSTR)opObject,(void *& )prlObject) == TRUE)
	{

		CCSPtrArray<BcStruct> olBcBuffer;
		
		olBcBuffer = prlObject->GetBcBuffer();

		int ilCount = olBcBuffer.GetSize();
		
		for(int i = 0; i < ilCount; i++)
		{
			ProcessUniDataBcInternal((void *)&olBcBuffer[i], opInstanceName);
		}
		olBcBuffer.DeleteAll();
	}
}


void CedaBasicData::SetBcMode(CString opObject, int ipBufferSize)
{
	CedaObject *prlObject; 
	if(omObjectMap.Lookup((LPCSTR)opObject,(void *& )prlObject) == TRUE)
	{
		prlObject->SetBcMode(ipBufferSize);
	}
}


void CedaBasicData::SetSentBcBufferOverflow(CString opObject, int ipDdxType)
{
	CedaObject *prlObject; 
	if(omObjectMap.Lookup((LPCSTR)opObject,(void *& )prlObject) == TRUE)
	{
		prlObject->SetSentBcBufferOverflow(ipDdxType);
	}
}


CString  CedaBasicData::GetValueList(CString opObject, CString opRefField, CString opRefValue, CString opField, CString opTrenner)
{
	CString olRet;
	CedaObject *prlObject;
	if(omObjectMap.Lookup((LPCSTR)opObject,(void *& )prlObject) == TRUE)
	{
		olRet = prlObject->GetValueList(opRefField, opRefValue, opField, opTrenner);
	}
	return olRet;
}


CString CedaBasicData::GetField2(CString opObject, CString opRefField1, CString opRefField2, CString opRefValue, CString opField)
{
	CString olRet;

	CedaObject *prlObject;
	if(omObjectMap.Lookup((LPCSTR)opObject,(void *& )prlObject) == TRUE)
	{
		olRet = prlObject->GetField2( opRefField1, opRefField2, opRefValue, opField);
	}
	return olRet;
}



CString CedaBasicData::GetFieldExt(CString opObject, CString opRefField1, CString opRefField2, CString opRefValue1, CString opRefValue2, CString opField)
{
	CString olRet;

	CedaObject *prlObject;
	if(omObjectMap.Lookup((LPCSTR)opObject,(void *& )prlObject) == TRUE)
	{
		olRet = prlObject->GetFieldExt( opRefField1, opRefField2, opRefValue1, opRefValue2, opField);
	}
	return olRet;
}



CString CedaBasicData::GetFieldType(CString opObject, CString opField)
{
	CString olRet;
	CedaObject *prlObject;
	if(omObjectMap.Lookup((LPCSTR)opObject,(void *& )prlObject) == TRUE)
	{
		olRet = prlObject->GetFieldType(opField);
	}
	return olRet;
}





long CedaBasicData::GetNextUrno(void)
{
	if(omNewUrnos.GetSize() == 0)
	{
		GetManyUrnos(500, omNewUrnos);
	}

	long llUrno;

	llUrno = omNewUrnos[0];

	omNewUrnos.RemoveAt(0);

	return llUrno;
}



bool CedaBasicData::GetManyUrnos(int ipAnz, CUIntArray &opUrnos)
{
	bool	ilRc = false;

	if(omAccessMethod == "ODBC")
	{
		pomOdbc->GetManyUrnos(ipAnz, opUrnos);
	}
	else
	{
		char 	pclTmpDataBuf[10000];

		sprintf(pclTmpDataBuf, "%d", ipAnz);

		ilRc = CedaAction("GMU", "", "", pclTmpDataBuf);

		if (ilRc == true)
		{
			for ( int ilItemNo=1; (ilItemNo <= ipAnz) ; ilItemNo++ )
			{
				char pclTmpBuf[64];

				GetItem(ilItemNo, pclTmpDataBuf, pclTmpBuf);

				long llNewUrno = atol(pclTmpBuf);

				opUrnos.Add(llNewUrno);
				ilRc = true;
			}
		}
	}

	return ilRc;
}




int CedaBasicData::GetRecordsExt(CString opObject, CString opRefField1, CString opRefField2, CString opRefValue1, CString opRefValue2, CCSPtrArray<RecordSet> *prpRecords)
{

	CedaObject *prlObject;
	if(omObjectMap.Lookup((LPCSTR)opObject,(void *& )prlObject) == TRUE)
	{
		return prlObject->GetRecordsExt(opRefField1, opRefField2, opRefValue1, opRefValue2, prpRecords);
	}
	return 0;

}

bool CedaBasicData::CedaAction2(char *pcpAction,
    char *pcpTableName, char *pcpFieldList,
    char *pcpSelection, char *pcpSort,
    char *pcpData,
	char *pcpDest,
	bool bpIsWriteAction /* false */,
	long lpID /* = 0*/,
	CCSPtrArray<RecordSet> *pomData,
	int ipFieldCount,
	bool bpDelDataArray /* true */)
{
	char *pclOldFieldList = pcmFieldList;
	strcpy(pcmTableName,pcpTableName);
	omTableExt = "TAB";
	if (strlen(pcmTableName) == 3)
		strcat(pcmTableName,omTableExt);
	pcmFieldList = pcpFieldList;
	//omServerName = pomCommHandler->pcmRealHostName;
	if (CCSCedaData::CedaAction2(pcpAction,pcpSelection,pcpSort,pcpData,pcpDest,bpIsWriteAction,lpID,bpDelDataArray) == true)
	{
		if (pomData != NULL)
		{
			for (int i = 0; i < this->pomFastCeda->omData.GetSize(); i++)
			{
				RecordSet *pRecord = new RecordSet(this->pomFastCeda->omData[i],ipFieldCount);
				pomData->Add(pRecord);
			}
		}

		pcmFieldList = pclOldFieldList;
		return true;
	}
	else
	{
		pcmFieldList = pclOldFieldList;
		return false;
	}
}

// Released die gesammelten �nderungen 	
bool CedaBasicData::Release(const CString& opObject,int ipPackages,char *ropRelCmd,char *pcpSbc)
{
	CedaObject *prlObject = NULL; 
	
	if (this->omObjectMap.Lookup(opObject,(void *&)prlObject) == FALSE || prlObject == NULL)
		return false;

	if (prlObject->bmIsLogicObject)
		return false;

	if (this->omAccessMethod == "ODBC")
		return false;

	CString olUser = CString(",") + CString(pomCommHandler->pcmUser); 
	CTime olTime = CTime::GetCurrentTime();
	CString olCurrTime = CString(",") + CTimeToDBString(olTime, TIMENULL);

	// Kommandostrings f�r CEDA: INSERT-, UPDATE- und DELETE-Kommando
	CString olIRT, olURT, olDRT;
	// Feldliste: beschreibt die zu speichernden Felder der Tabelle
	CString olFieldList;
	// vollst�ndige Kommandos (Update, Insert und Delete) f�r CedaData::CedaAction()
	// Format:	1.Zeile:		Kommando, Feldliste
	//			1.-n. Zeile:	relevante Felder des Datensatzes (zum L�schen reicht die Urno, sonst alle Felder)
	CString olUpdateString, olInsertString, olDeleteString;
	// Zwischenspeicher
	CString olTmpText;
	// Z�hler f�r Anzahlen der ge�nderten/gel�schten/neuen Datens�tze
	int ilNoOfUpdates = 0, ilNoOfDeletes = 0, ilNoOfInserts = 0;
	// Anzahl der Felder pro Datensatz ermitteln
	CStringArray olFields;	// nur Hilfsarray, wird nicht weiter benutzt
	int ilFields;

	char pclTable[16];
	strcpy(pclTable,opObject);
	strcat(pclTable,omTableExt);

	int ilUrnoIndex = -1;
	if (!prlObject->GetFieldIndex(CString("URNO"),ilUrnoIndex))
		return false;

	int ilCdatIdx = GetFieldIndex(opObject,"CDAT");
	int ilUsecIdx = GetFieldIndex(opObject,"USEC");

	char pclCmd[4] = "";	

	CString olOrigFieldList;
	// Paketz�hler: alle hundert Datens�tze einen Schreibvorgang anstossen
	int ilCurrentCount = 0;
	// Gesamtz�hler, wird am Ende benutzt, um zu pr�fen, ob mind. 1 DS geschrieben wurde
	int ilTotalCount = 0;

	// alte Feldliste speichern
	olOrigFieldList = prlObject->GetInternalDBFieldList();

	// Strings f�r CedaAction initialisieren
	olFieldList = prlObject->GetInternalDBFieldList();
	ilFields = prlObject->GetDBFieldCount();

	if (prlObject->bmUsec)
	{
		olFieldList += ",USEC";
		++ilFields;
	}
	if (prlObject->bmCdat)
	{
		olFieldList += ",CDAT";
		++ilFields;
	}

	if (prlObject->bmUseu)
	{
		olFieldList += ",USEU";
		++ilFields;
	}

	if (prlObject->bmLstu)
	{
		olFieldList += ",LSTU";
		++ilFields;
	}

	olIRT.Format("*CMD*,%s,IRT,%d,", pclTable, ilFields);
	olInsertString = olIRT + olFieldList + CString("\n");

	// Update
	olFieldList = prlObject->GetInternalDBFieldList();
	ilFields = prlObject->GetDBFieldCount();

	if (prlObject->bmUseu)
	{
		olFieldList += ",USEU";
		++ilFields;
	}

	if (prlObject->bmLstu)
	{
		olFieldList += ",LSTU";
		++ilFields;
	}

	olURT.Format("*CMD*,%s,URT,%d,", pclTable, ilFields);
	olUpdateString = olURT + olFieldList + CString(",[URNO=:VURNO]")+ CString("\n");

	// delete
	olDRT.Format("*CMD*,%s,DRT,-1,", pclTable);
	olDeleteString = olDRT + CString(",[URNO=:VURNO]")+ CString("\n");;


	CMapPtrToPtr	*pomSave = prlObject->GetSaveRecords();
	void *pVoid;
	RecordSet *prlRecord;
	for (POSITION rlPos = pomSave->GetStartPosition(); rlPos != NULL; )
	{
		////////////////////////////////////////////////////////////////////////
		//// �nderung pr�fen und Pakete mit Datens�tzen schn�ren
		////////////////////////////////////////////////////////////////////////

		pomSave->GetNextAssoc(rlPos,pVoid, (void *&)prlRecord);

		CString olListOfData;
		olListOfData = prlObject->GetInternalDBDataList(prlRecord,omAccessMethod);
		if (olListOfData.IsEmpty())
			continue;

		bool blCdatFilled = false;
		bool blUsecFilled = false;

		if (prlRecord->GetStatus() != DATA_UNCHANGED)
		{
			CString olCurrentUrno = prlRecord->Values[ilUrnoIndex];

			if (prlRecord->GetStatus() == DATA_NEW)
			{
				if (ilCdatIdx >= 0)	// CDAT is already part of record -> Use it
				{
					CString olTmp = prlRecord->Values[ilCdatIdx];
					olTmp.TrimRight();
					if (!olTmp.IsEmpty())
						blCdatFilled = true;
				}

				if (ilUsecIdx >= 0)	// USEC is already part of record -> Use it
				{
					CString olTmp = prlRecord->Values[ilUsecIdx];
					olTmp.TrimRight();
					if (!olTmp.IsEmpty())
						blUsecFilled = true;
				}
			}

			// was soll mit dem Datensatz geschehen
			switch(prlRecord->GetStatus())
			{
			case DATA_NEW:	// neuer DS
				{
					if (prlObject->bmUsec)
					{
						if (blUsecFilled)
						{
							olListOfData += ',';
							olListOfData += prlRecord->Values[ilUsecIdx];
						}
						else
						{
							olListOfData += ',';
							olListOfData += olUser;
						}
					}

					if (prlObject->bmCdat)
					{
						if (blCdatFilled)
						{
							olListOfData += ',';
							olListOfData += prlRecord->Values[ilCdatIdx];
						}
						else
						{
							olListOfData += ',';
							olListOfData += olCurrTime;
						}
					}

					if (prlObject->bmUseu)
					{
						olListOfData += ',';
						olListOfData += olUser;
					}

					if (prlObject->bmLstu)
					{
						olListOfData += ',';
						olListOfData += olCurrTime;
					}

					olInsertString += olListOfData + CString("\n");
					ilNoOfInserts++;
					break;
				}
			case DATA_CHANGED:	// ge�nderter DS
				{
					if (prlObject->bmUseu)
					{
						olListOfData += ',';
						olListOfData += olUser;
					}

					if (prlObject->bmLstu)
					{
						olListOfData += ',';
						olListOfData += olCurrTime;
					}

					olUpdateString += olListOfData + CString(",") + olCurrentUrno + CString("\n");
					ilNoOfUpdates++;
					break;
				}
			case DATA_DELETED:	// DS l�schen
				{
					olDeleteString += olCurrentUrno + CString("\n");
					ilNoOfDeletes++;
					break;
				}
			}
			// Z�hler inkrementieren
			ilCurrentCount++;
			ilTotalCount++;
			// �nderungsflag zur�cksetzen
			prlRecord->Status = DATA_UNCHANGED;
		}
		////////////////////////////////////////////////////////////////////////
		//// Ende �nderung pr�fen und Pakete mit Datens�tzen schn�ren
		////////////////////////////////////////////////////////////////////////
			
		////////////////////////////////////////////////////////////////////////
		//// 500er Paket in Datenbank speichern
		////////////////////////////////////////////////////////////////////////
		// 500 Datens�tze im Paket?
		if(ilCurrentCount == ipPackages)
		{
			// ja -> Paket speichern
			// neue Datens�tze erzeugt?
			if(ilNoOfInserts > 0)
			{
				// ja -> speichern
				CedaAction("REL",ropRelCmd,"",olInsertString.GetBuffer(0));
			}
			// Datens�tze ge�ndert?
			if(ilNoOfUpdates > 0)
			{
				// ja -> speichern
				CedaAction("REL",ropRelCmd,"",olUpdateString.GetBuffer(0));
			}
			// Datens�tze gel�scht?
			if(ilNoOfDeletes > 0)
			{
				// ja -> aus Datenbank l�schen
				CedaAction("REL",ropRelCmd,"",olDeleteString.GetBuffer(0));
			}

			
			// Strings f�r CedaAction initialisieren
			olFieldList = prlObject->GetInternalDBFieldList();
			ilFields = prlObject->GetDBFieldCount();

			if (prlObject->bmUsec)
			{
				olFieldList += ",USEC";
				++ilFields;
			}
			if (prlObject->bmCdat)
			{
				olFieldList += ",CDAT";
				++ilFields;
			}

			if (prlObject->bmUseu)
			{
				olFieldList += ",USEU";
				++ilFields;
			}

			if (prlObject->bmLstu)
			{
				olFieldList += ",LSTU";
				++ilFields;
			}

			olIRT.Format("*CMD*,%s,IRT,%d,", pclTable, ilFields);
			olInsertString = olIRT + olFieldList + CString("\n");

			olFieldList = prlObject->GetInternalDBFieldList();
			ilFields = prlObject->GetDBFieldCount();

			if (prlObject->bmUseu)
			{
				olFieldList += ",USEU";
				++ilFields;
			}

			if (prlObject->bmLstu)
			{
				olFieldList += ",LSTU";
				++ilFields;
			}

			// update
			olURT.Format("*CMD*,%s,URT,%d,", pclTable, ilFields);
			olUpdateString = olURT + olFieldList + CString(",[URNO=:VURNO]")+ CString("\n");

			// delete
			olDRT.Format("*CMD*,%s,DRT,-1,", pclTable);
			olDeleteString = olDRT + CString(",[URNO=:VURNO]")+ CString("\n");;

			// Z�hler und Strings wieder auf Initialwerte setzen
			ilCurrentCount = 0;
			ilNoOfInserts = 0;
			ilNoOfUpdates = 0;
			ilNoOfDeletes = 0;
		}
		////////////////////////////////////////////////////////////////////////
		//// Ende 500er Paket in Datenbank speichern
		////////////////////////////////////////////////////////////////////////
	}//for(rlPos = ......
		
		
	////////////////////////////////////////////////////////////////////////
	//// Rest(von unter Fuenfhundert)-Paket in Datenbank speichern
	////////////////////////////////////////////////////////////////////////
	// neue Datens�tze erzeugt?
	if(ilNoOfInserts > 0)
	{
		// ja -> speichern
		CedaAction("REL",ropRelCmd,"",olInsertString.GetBuffer(0));
	}
	// Datens�tze ge�ndert?
	if(ilNoOfUpdates > 0)
	{
		// ja -> speichern
		CedaAction("REL",ropRelCmd,"",olUpdateString.GetBuffer(0));
	}
	// Datens�tze gel�scht?
	if(ilNoOfDeletes > 0)
	{
		// ja -> aus Datenbank l�schen
		CedaAction("REL",ropRelCmd,"",olDeleteString.GetBuffer(0));
	}
	////////////////////////////////////////////////////////////////////////
	//// Ende Rest(von unter Fuenfhundert)-Paket in Datenbank speichern
	////////////////////////////////////////////////////////////////////////
		
	////////////////////////////////////////////////////////////////////////
	//// Ende Broadcast erzeugen, wenn erforderlich
	////////////////////////////////////////////////////////////////////////
	if (pcpSbc != NULL)
	{
		CedaAction("SBC",pcpSbc,"",CCSCedaData::pcmReqId,"","");
	}

	return true;

}
