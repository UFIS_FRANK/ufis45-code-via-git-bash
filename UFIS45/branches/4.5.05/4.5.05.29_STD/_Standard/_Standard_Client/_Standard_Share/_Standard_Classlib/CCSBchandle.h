// BcHandle.h : header file
//

/////////////////////////////////////////////////////////////////////////////
#ifndef _CCSBCHANDLE
#define _CCSBCHANDLE

#include <stdafx.h>
#include <CCSCedaData.h>
#include <CCSDdx.h>
#include <CCSLog.h>
#include <CCSCedacom.h>
#include <BcCCmdTarget.h>
#include <bccomclient.h>

#define BC_TO_APPLICATION (WM_USER+888)

/*
extern enum enumDDXTypes {NOTFOUND,BC_FLIGHT_CHANGE,
	BC_DEMAND_CHANGE,BC_DEMAND_DELETE,
	BC_JOB_CHANGE,BC_JOB_DELETE,
	BC_JOD_CHANGE,BC_JOD_DELETE} egDDXTypes;
*/

/* 
 *
 * Following the Broadcast Header  BC_HEAD 
 *
 */
typedef struct {
	char		dest_name[10];		// user login name
	char		orig_name[10];		// filter (BC) or timeout for netin
	char		recv_name[10];		// WKS-Name
	short		bc_num;				// serial number of broadcast 1-30000 
	char		seq_id[10];			// WKS-Name (obsolete)
	short		tot_buf;			// Count BC-Parts
	short		act_buf;			// actual BC PartNo
	char		ref_seq_id[10];		// not used
	short		rc;					// return code, mostly useless
	short		tot_size;			// total bytes of BC (data only)
	short		cmd_size;			// bytes of this part
	short		data_size;			// not used
	char		data[1];			// data, cmdblk
}BC_HEAD;

/* 
 *
 * Following the Broadcast Buffer from dZine Controller
 *
 */
/* 
 *
 * Following the Command Block  CMDBLK
 *
 */
typedef struct {
	char		command[6];			// router command
	char		obj_name[33];		// object name (table name)
	char		order[2];			// not used
	char		tw_start[33];		// used for application data
	char		tw_end[33];			// hopo,tabend,modul name,modul version
 	char		data[1];			// data (selection,fields,data)
}CMDBLK;
          
typedef	struct {
		short	command;
		long	length;
		char	data[1];
}COMMIF;


typedef struct {
	BC_HEAD		BcHead;	
	CMDBLK		CmdBlk;
	char		*DataBuffer;
	int			DataBufferSize;
}REC_BC;



extern struct BcStruct
	{
		char ReqId[12];
		char Dest1[12];
		char Dest2[12];
		char Cmd[8];
		char Object[34];
		char Seq[4];
		char Tws[34];
		char Twe[34];
		int  DdxType;
		char Selection[2048];
		char Fields[2048];
		char Data[8196];
		char BcNum[24];
		CString Attachment;

		BcStruct(void)
		{
			//memset(this,'\0',sizeof(*this));
			ReqId[0]=0x00;
			Dest1[0]=0x00;
			Dest2[0]=0x00;
			Cmd[8]=0x00;
			Object[0]=0x00;
			Seq[0]=0x00;
			Tws[0]=0x00;
			Twe[0]=0x00;
			DdxType=0;
			Selection[0]=0x00;
			Fields[0]=0x00;
			Data[0]=0x00;
			BcNum[0]=0x00;
			Attachment = "";
		}


	} rlBcData;

typedef struct BcStruct BCDATA;

// MWO/RRO 26.03.03
// - BcNum is the number of the BC
// - ipState indicates if the BC is going to be processed (ipState==0)
//   or that it is ready processed (ipState==1)
typedef void (*TRAFFIC_LIGHT_CALLBACK)(long BcNum,int ipState);
// MWO/RRO 26.03.03

struct BcTableHandler
{
	CString	Applname;
	CString Tablename;
	CString CedaCommand;
	int		DdxType;
	bool	UseOwnBc;
	bool	OnlyOwnBc;
};

typedef void (*BCFILTERFUNC)(char *,char *,long,char *);

class CCSBcHandle: public CCSCedaData
{
public:
	CCSBcHandle(CCSDdx *popDdx = NULL, CCSCedaCom *popCommHandler = NULL, CCSLog *popLog = NULL);           
	~CCSBcHandle();

	void StartBc(void);
	void StopBc(void);

	void BufferBc(void);
	void ReleaseBuffer(void);

	void DataChangeBc();
	bool GetBc(int ipBcNum = -1);
	bool ReReadBc(void);

	void UseBcProxy();
	void UseBcCom();
	bool DistributeBc(BcStruct &rlBcData);

	CWnd *pomMsgDlg;

	void SetTrafficLightCallBack(TRAFFIC_LIGHT_CALLBACK pTrafficLightCallBack)
	{
		TrafficLightCallBack = pTrafficLightCallBack;
	}
	void ExitApp();
	bool SetFilterRange(CString spTable, CString spFromField, CString spToField, CString spFromValue, CString spToValue);

public:
	void AddTableCommand(CString opTable, CString opCommand, int ipDDXType, bool bpUseOwnBc = false,CString opAppl = "");
	void RemoveTableCommand(CString opTable, CString opCommand, int ipDDXType);
	void TableCommandSetOnlyOwn(CString opTable, CString opCommand, int ipDDXType, bool bpSet);

	void SetCloMessage(CString opText);

	void SetHomeAirPort(char *pcpHomeAirport){ strcpy(pcmHomeAirport, pcpHomeAirport)   ;};

	bool GetAtlBc(CCSPtrArray<BcStruct> &ropBcs, int ipBcNum = -1);

	//MWO 02.07.04
	bool ProcessBroadcast(LPCTSTR ReqId, LPCTSTR Dest1, LPCTSTR Dest2, LPCTSTR Cmd, LPCTSTR Object, LPCTSTR Seq, LPCTSTR Tws, LPCTSTR Twe, LPCTSTR Selection, LPCTSTR Fields, LPCTSTR Data, LPCTSTR BcNum, LPCTSTR Attachment, LPCTSTR Additional);
	//END MWO 02.07.04

	//MWO: 15.07.2004 attach bc state of the application
	void AttachApplicationBCState(int *pipBCState);
	//END MWO: 15.07.2004 attach bc state of the application

protected:
	CCSPtrArray<BcTableHandler> omTableHandlers;

	bool bmBcActiv;

	bool bmInit;
	long lmLastBcNum; 

	char pcmHomeAirport[4];

	CCSDdx *pomDdx;	
	CCSCedaCom *pomCommHandler;
	CCSLog *pomLog;

	CString omCloMessage;
	
	CMapStringToPtr TableKeyMap;
	
	void DelFromKeyMap(BcTableHandler *prlTableHandler);
	void AddToKeyMap(BcTableHandler *prlTableHandler);
	void CreateCLOBroadcast(struct BcStruct *prpBcData,short lpNextBcNum);	

private:
	CCSPtrArray <BcStruct> omData;

	FILE *prmBcFile;
	REC_BC rmBcData;
	//aiarkr
	char pcmBcLogfile[120];
	BcCCmdTarget* prlBcCCmdTarget;
	bool	bmApplCheck;
	CTime omLastBcEnd;

	//MWO: 15.07.2004 the broadcast state in the application
	int *pimBCState;
	//END MWO: 15.07.2004 the broadcast state in the application

public:
	TRAFFIC_LIGHT_CALLBACK TrafficLightCallBack;
};

class CAttachment
{
public:
	CAttachment(CString &ropAttachment);   // standard constructor
	//aiarkr
	//char pcmBcLogfile[120];

	bool bmAttachmentValid;
	CString omTableName;
	CString omFieldList;
	CStringArray omInsertDataList;
	CStringArray omUpdateDataList;
	CStringArray omDeleteDataList;

private:
	char *GetAttachmentData(char *pcpData, char *pcpToken, CStringArray &ropDataList, char *pcpRecSeparator);
	void Trace(char *pcpFormatList, ...);
};


/////////////////////////////////////////////////////////////////////////////
#endif
