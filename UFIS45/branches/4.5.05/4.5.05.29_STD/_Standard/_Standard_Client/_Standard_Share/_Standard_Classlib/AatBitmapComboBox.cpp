// AatBitmapComboBox.cpp : implementation file
//

#include <stdafx.h>
#include <AatBitmapComboBox.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// AatBitmapComboBox

AatBitmapComboBox::AatBitmapComboBox()
{
	blDrawStrings = false;
}

AatBitmapComboBox::~AatBitmapComboBox()
{
	for (int i = omBitmaps.GetSize()-1; i >= 0; i--)
	{
		CBitmap *polBitmap = (CBitmap *)omBitmaps[i];
		polBitmap->DeleteObject();
		delete polBitmap;
	}

	omBitmaps.RemoveAll();
}


BEGIN_MESSAGE_MAP(AatBitmapComboBox, CComboBox)
	//{{AFX_MSG_MAP(AatBitmapComboBox)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	ON_WM_CREATE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

BOOL AatBitmapComboBox::LoadBitmapFromBMPFile( LPTSTR szFileName, HBITMAP *phBitmap,HPALETTE *phPalette )
{
     BITMAP  bm;

     *phBitmap = NULL;
     *phPalette = NULL;

     // Use LoadImage() to get the image loaded into a DIBSection
     *phBitmap = (HBITMAP)LoadImage( NULL, szFileName, IMAGE_BITMAP, 0, 0,
                 LR_CREATEDIBSECTION | LR_LOADFROMFILE );
     if( *phBitmap == NULL )
       return FALSE;

     // Get the color depth of the DIBSection
     GetObject(*phBitmap, sizeof(BITMAP), &bm );
     // If the DIBSection is 256 color or less, it has a color table
     if( ( bm.bmBitsPixel * bm.bmPlanes ) <= 8 )
     {
       HDC           hMemDC;
       HBITMAP       hOldBitmap;
       RGBQUAD       rgb[256];
       LPLOGPALETTE  pLogPal;
       WORD          i;

       // Create a memory DC and select the DIBSection into it
       hMemDC = CreateCompatibleDC( NULL );
       hOldBitmap = (HBITMAP)SelectObject( hMemDC, *phBitmap );
       // Get the DIBSection's color table
       GetDIBColorTable( hMemDC, 0, 256, rgb );
       // Create a palette from the color table
       pLogPal = (LOGPALETTE*)malloc( sizeof(LOGPALETTE) + (256*sizeof(PALETTEENTRY)) );
       pLogPal->palVersion = 0x300;
       pLogPal->palNumEntries = 256;
       for(i=0;i<256;i++)
       {
         pLogPal->palPalEntry[i].peRed = rgb[i].rgbRed;
         pLogPal->palPalEntry[i].peGreen = rgb[i].rgbGreen;
         pLogPal->palPalEntry[i].peBlue = rgb[i].rgbBlue;
         pLogPal->palPalEntry[i].peFlags = 0;
       }
       *phPalette = CreatePalette( pLogPal );
       // Clean up
       free( pLogPal );
       SelectObject( hMemDC, hOldBitmap );
       DeleteDC( hMemDC );
     }
     else   // It has no color table, so use a halftone palette
     {
       HDC    hRefDC;

       hRefDC = ::GetDC(NULL);
       *phPalette = CreateHalftonePalette( hRefDC );
       ::ReleaseDC(NULL,hRefDC );
     }
     return TRUE;
}

void AatBitmapComboBox::Initialize()
{
}

int AatBitmapComboBox::AddBitmap(const CString& ropFileName,const CString& ropString)
{
	CString olBuffer(ropFileName);
	HBITMAP hBitmap;
	HPALETTE hPalette;
	if (!this->LoadBitmapFromBMPFile(olBuffer.GetBuffer(0),&hBitmap,&hPalette))
		return -1;

	::DeleteObject(hPalette);

	CBitmap *polBitmap = new CBitmap;	
	if (!polBitmap->Attach(hBitmap))
		return -1;
	int ind = omBitmaps.Add(polBitmap);

	if (!blDrawStrings && !ropString.IsEmpty())
		blDrawStrings = true;

	return this->InsertString(ind,ropString);
}

int AatBitmapComboBox::AddBitmap(const CBitmap& ropBitmap,const CString& ropString)
{
	CBitmap *polBitmap = new CBitmap;	
	if (!polBitmap->Attach(ropBitmap))
		return -1;
	int ind = omBitmaps.Add(polBitmap);

	if (!blDrawStrings && !ropString.IsEmpty())
		blDrawStrings = true;

	return this->InsertString(ind,ropString);
}

int AatBitmapComboBox::InsertBitmap(int ipIndex,const CString& ropFileName,const CString& ropString)
{
	CString olBuffer(ropFileName);
	HBITMAP hBitmap;
	HPALETTE hPalette;
	if (!this->LoadBitmapFromBMPFile(olBuffer.GetBuffer(0),&hBitmap,&hPalette))
		return -1;

	::DeleteObject(hPalette);

	CBitmap *polBitmap = new CBitmap;	
	if (!polBitmap->Attach(hBitmap))
		return -1;

	if (!blDrawStrings && !ropString.IsEmpty())
		blDrawStrings = true;

	if (ipIndex < 0)
		ipIndex = omBitmaps.Add(polBitmap);

	return this->InsertString(ipIndex,ropString);
}

int AatBitmapComboBox::InsertBitmap(int ipIndex,const CBitmap& ropBitmap,const CString& ropString)
{
	CBitmap *polBitmap = new CBitmap;	
	if (!polBitmap->Attach(ropBitmap))
		return -1;

	if (ipIndex < 0)
		ipIndex = omBitmaps.Add(polBitmap);

	if (!blDrawStrings && !ropString.IsEmpty())
		blDrawStrings = true;

	return this->InsertString(ipIndex,ropString);
}

int AatBitmapComboBox::DeleteBitmap(int ipIndex)
{
	if (ipIndex < 0 || ipIndex > omBitmaps.GetSize() - 1)
		return CB_ERR;

	CBitmap *polBitmap = (CBitmap *)omBitmaps[ipIndex];
	if (polBitmap)
		delete polBitmap;
	omBitmaps[ipIndex] = NULL;

	return ipIndex;
}

void AatBitmapComboBox::ResetContent()
{
	for (int i = omBitmaps.GetSize()-1; i >= 0; i--)
	{
		CBitmap *polBitmap = (CBitmap *)omBitmaps[i];
		polBitmap->DeleteObject();
		delete polBitmap;
	}

	omBitmaps.RemoveAll();
	blDrawStrings = false;

	CComboBox::ResetContent();
}
/////////////////////////////////////////////////////////////////////////////
// AatBitmapComboBox message handlers

int AatBitmapComboBox::OnCreate( LPCREATESTRUCT pCStruct ) 
{
	if( CComboBox::OnCreate( pCStruct ) == -1 )				// If Create Failed
		return( -1 );										// Return Failure
	
	Initialize();											// Initialize Contents
	SetCurSel( 0 );											// Select First Item By Default

	return( 0 );											// Done!
}


void AatBitmapComboBox::PreSubclassWindow() 
{
	Initialize();											// Initialize Contents
	
	CComboBox::PreSubclassWindow();							// Subclass Control

	SetCurSel( 0 );											// Select First Item By Default

	return;													// Done!
}

void AatBitmapComboBox::DrawItem( LPDRAWITEMSTRUCT pDIStruct )
{
	static		CString	sColor;								// No Need To Reallocate Each Time

	CDC			dcContext;
	CRect		rItemRect( pDIStruct -> rcItem );
	CRect		rBlockRect( rItemRect );
	CRect		rTextRect( rBlockRect );
	CBrush		brFrameBrush;
	CBrush		brInnerBrush;
	int			iFourthWidth = 0;
	int			iItem = pDIStruct -> itemID;
	int			iAction = pDIStruct -> itemAction;
	int			iState = pDIStruct -> itemState;
	COLORREF	crColor = 0;
	COLORREF	crNormal = GetSysColor( COLOR_WINDOW );
	COLORREF	crSelected = GetSysColor( COLOR_HIGHLIGHT );
	COLORREF	crText = GetSysColor( COLOR_WINDOWTEXT );

	if ( !dcContext.Attach( pDIStruct -> hDC ) )			// Attach CDC Object
		return;												// Stop If Attach Failed
	if (blDrawStrings)
		iFourthWidth = ( rBlockRect.Width() / 4);			// Get 1/4 Of Item Area
	else
		iFourthWidth = ( rBlockRect.Width());				// Get 4/4 Of Item Area

	brFrameBrush.CreateStockObject( BLACK_BRUSH );			// Create Black Brush

	if ( iState & ODS_SELECTED )								// If Selected
	{														// Set Selected Attributes
		dcContext.SetTextColor(
				( 0x00FFFFFF & ~( crText ) ) );				// Set Inverted Text Color (With Mask)
		dcContext.SetBkColor( crSelected );					// Set BG To Highlight Color
		dcContext.FillSolidRect( &rBlockRect, crSelected );	// Erase Item
	}
	else													// If Not Selected
	{														// Set Standard Attributes
		dcContext.SetTextColor( crText );					// Set Text Color
		dcContext.SetBkColor( crNormal );					// Set BG Color
		dcContext.FillSolidRect( &rBlockRect, crNormal );	// Erase Item
	}

	if ( iState & ODS_FOCUS )								// If Item Has The Focus
		dcContext.DrawFocusRect( &rItemRect );				// Draw Focus Rect

	//
	//	Calculate Text Area
	//
	rTextRect.left += ( iFourthWidth + 2 );					// Set Start Of Text
	rTextRect.top += 2;										// Offset A Bit

	//
	//	Calculate Color Block Area
	//
	rBlockRect.DeflateRect( CSize( 2, 2 ) );				// Reduce Color Block Size
	rBlockRect.right = iFourthWidth;						// Set Width Of Color Block

	//
	//	Draw Color Text And Block
	//
	if( iItem != -1 )										// If Not An Empty Item
	{
		GetLBText( iItem, sColor );							// Get Color
		if( iState & ODS_DISABLED )							// If Disabled
		{
			// no change for color, because can't be seen
//			crColor = GetSysColor( COLOR_INACTIVECAPTIONTEXT ); 
			crColor = GetItemData( iItem );					// Get Color Value
//			dcContext.SetTextColor( crColor );				// Set Text Color
		}
		else												// If Normal
			crColor = GetItemData( iItem );					// Get Color Value

		dcContext.SetBkMode( TRANSPARENT );					// Transparent Background
		if (blDrawStrings)
			dcContext.TextOut(rTextRect.left,rTextRect.top,sColor );	// Draw Text

		if (iItem < omBitmaps.GetSize())
		{
			brInnerBrush.CreatePatternBrush((CBitmap *)omBitmaps[iItem]);
			dcContext.FillRect(&rBlockRect,&brInnerBrush);
		}
		dcContext.FrameRect( &rBlockRect, &brFrameBrush );	// Draw Frame
	}
	dcContext.Detach();										// Detach DC From Object
}
