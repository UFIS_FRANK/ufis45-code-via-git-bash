// CCSIPEdit.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CCSIPEdit window

#ifndef __CCSIPEDIT_CTRL__
#define __CCSIPEDIT_CTRL__

#include <stdafx.h>


class CCSIPEdit : public CEdit
{
// Construction
public:
	CCSIPEdit(CRect &popRect, CWnd *popParent = NULL, 
				 int ipType = 1, 
				 CString opFormat = CString(""), 
				 CFont *opFont = NULL,
				 int ipTextLimit = 100,
				 int ipRangeFrom = 0, 
				 int ipRageTo = 99999999);

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCSIPEdit)
	public:
	virtual BOOL Create(LPCTSTR lpszClassName, LPCTSTR lpszWindowName, DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID, CCreateContext* pContext = NULL);
	virtual BOOL OnChildNotify(UINT message, WPARAM wParam, LPARAM lParam, LRESULT* pLResult);
	virtual BOOL OnCmdMsg(UINT nID, int nCode, void* pExtra, AFX_CMDHANDLERINFO* pHandlerInfo);
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual LRESULT DefWindowProc(UINT message, WPARAM wParam, LPARAM lParam);
	virtual BOOL OnNotify(WPARAM wParam, LPARAM lParam, LRESULT* pResult);
	virtual LRESULT WindowProc(UINT message, WPARAM wParam, LPARAM lParam);
	//}}AFX_VIRTUAL

// Implementation
	//Sets the current type of the field. Possible values are:
	// KT_STRING, KT_DATE, KT_HOUR, KT_INT, KT_FLOAT, KT_MONEY
	void SetEditType(int ipType = KT_STRING);

	// set a textlimit of the field
	void SetTextLimit(int ipCount);

	// sets a formatstring. this method should be used for the type KT_STRING
	// otherwise it will make no sence
	// "": Every character is allowed
	void SetFormat(CString opFormat = CString(""));

	// for the fieldtyp KT_FLOAT and KT_INT we want to set a rage between that values
	// are allowed
	void SetRange(int ipFrom, int ipTo);

	// the type KT_FLOAT needs - perhaps - a precision 
	// e.g. 5, 3 meens totally 5 digits and 3 digits after the decimal point
	void SetPrecision(int ipPrePoint, int ipAfterPoint);

	// perhaps we need a special font for the time of inplace-editing
	void SetEditFont(CFont *popFont);

	BOOL CheckField(CString &opMessage);

protected:
	BOOL InterpretFormat(int ipPosition, UINT ipChar);
public:
	virtual ~CCSIPEdit();
	BOOL Create( DWORD dwStyle, const RECT& rect, CWnd* pParentWnd = NULL, UINT nID = 0);
	LONG OnNextCtrl(UINT wParam, LONG lParam);

	CWnd *pomTable;
	int imType;
	int imRangeFrom;
	int imRangeTo;
	int imCurrentLenght;
	int imTextLimit;
	int imFloatIntPart;
	int imFloatFloatPart;
	CString omFormat;
	CWnd  *pomParent;
	CFont *pomTextFont;
	CRect omRect;

	// Generated message map functions
protected:
	//{{AFX_MSG(CCSIPEdit)
	afx_msg void OnKillfocus();
	afx_msg void ParentNotify(UINT message, LPARAM lParam);
	afx_msg void OnChar(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg int OnCharToItem(UINT nChar, CListBox* pListBox, UINT nIndex);
	afx_msg void OnChildActivate();
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnKillFocus(CWnd* pNewWnd);
	afx_msg UINT OnNcHitTest(CPoint point);
	afx_msg void OnParentNotify(UINT message, LPARAM lParam);
	afx_msg void OnSysChar(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnSysKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnSysDeadChar(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg int OnVKeyToItem(UINT nKey, CListBox* pListBox, UINT nIndex);
	afx_msg void OnShowWindow( BOOL bShow, UINT nStatus );
	afx_msg void OnPaint( );
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

#endif //__CSCIPEDIT_CTRL__
