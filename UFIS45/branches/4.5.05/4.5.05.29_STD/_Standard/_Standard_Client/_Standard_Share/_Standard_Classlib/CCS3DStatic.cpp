// ccs3dstatic.cpp : implementation file
//
// Written By:
// Pichet on some day in the beginning time of the project -- Damkerng
//
// Modification History:
// Damkerng Thammathakerngkit	Sep 29th, 1996
//	Fix the painting routine which paints outside the window.
//	The fixed places will be commented as "-- this will paint outside the window".

#include <ccs3dstatic.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#define new DEBUG_NEW
#endif

/////////////////////////////////////////////////////////////////////////////
// CCS3DStatic
 



CCS3DStatic::CCS3DStatic(bool bpReverse)
{
    bmReverse = bpReverse;


    CDC dc;
    dc.CreateCompatibleDC(NULL);

    LOGFONT lolFont;
    memset(&lolFont, 0, sizeof(LOGFONT));

	pomFont = new CFont;
	lolFont.lfCharSet= DEFAULT_CHARSET;
    lolFont.lfHeight = - MulDiv(8, dc.GetDeviceCaps(LOGPIXELSY), 72);
    lolFont.lfWeight = FW_BOLD;
    lolFont.lfPitchAndFamily = VARIABLE_PITCH | FF_SWISS;
    lstrcpy(lolFont.lfFaceName, "MS Sans Serif");
    pomFont->CreateFontIndirect(&lolFont);

//	lmBkColor = RGB(192, 192, 192);
	lmBkColor = ::GetSysColor(COLOR_BTNFACE);
	lmTextColor = RGB(0,0,0);
	lmHilightColor = RGB(255,0,0);

}

CCS3DStatic::~CCS3DStatic()
{
	pomFont->DeleteObject();
	delete pomFont;
}


BEGIN_MESSAGE_MAP(CCS3DStatic, CStatic)
    //{{AFX_MSG_MAP(CCS3DStatic)
    ON_WM_PAINT()
    ON_WM_ERASEBKGND()
    ON_MESSAGE(WM_GETTEXT, OnGetText)
    ON_MESSAGE(WM_SETTEXT, OnSetText)
    ON_WM_CREATE()
    //}}AFX_MSG_MAP
END_MESSAGE_MAP()



void CCS3DStatic::SetTextColor(COLORREF opColor)
{ 
	lmTextColor = opColor;
}


void CCS3DStatic::SetAfterSlashColor(COLORREF opColor)
{ 
	lmHilightColor = opColor;
}


/////////////////////////////////////////////////////////////////////////////
// CCS3DStatic message handlers

void CCS3DStatic::OnPaint() 
{
    CPaintDC dc(this); // device context for painting
    
    // TODO: Add your message handler code here
    // Do not call CStatic::OnPaint() for painting messages

    CFont *polOldFont = (CFont *) dc.SelectObject(pomFont);
    CRect olClientRect; GetClientRect(&olClientRect);

    CPen ol1Pen;
    CPen ol2Pen;
    CPen ol3Pen;
    CPen ol4Pen;

    if (!bmReverse)
    {
        ol1Pen.CreatePen(PS_SOLID, 1, GetSysColor(COLOR_BTNFACE));
        ol2Pen.CreatePen(PS_SOLID, 1, GetSysColor(COLOR_WINDOWFRAME));
        ol3Pen.CreatePen(PS_SOLID, 1, GetSysColor(COLOR_WINDOW));
        ol4Pen.CreatePen(PS_SOLID, 1, GetSysColor(COLOR_BTNSHADOW));
    }
    else
    {
        ol4Pen.CreatePen(PS_SOLID, 1, GetSysColor(COLOR_BTNFACE));
        ol3Pen.CreatePen(PS_SOLID, 1, GetSysColor(COLOR_WINDOWFRAME));
        ol2Pen.CreatePen(PS_SOLID, 1, GetSysColor(COLOR_WINDOW));
        ol1Pen.CreatePen(PS_SOLID, 1, GetSysColor(COLOR_BTNSHADOW));
    }

    // draw top & left light-gray line
    CPen *polOldPen = dc.SelectObject(&ol1Pen);
    dc.MoveTo(olClientRect.right - 1, olClientRect.top);
    dc.LineTo(olClientRect.TopLeft());
    //dc.LineTo(olClientRect.left, olClientRect.bottom); -- this will paint outside the window
	dc.LineTo(olClientRect.left, olClientRect.bottom-1);

    // draw bottom & right black line
    dc.SelectObject(&ol2Pen);
    //dc.LineTo(olInnerRect.BottomRight());	-- this will paint outside the window
    //dc.LineTo(olClientRect.right, olClientRect.top - 1);
    dc.LineTo(olClientRect.right-1, olClientRect.bottom-1);
    dc.LineTo(olClientRect.right-1, olClientRect.top - 1);
    
    // draw top & left white line
    CRect olInnerRect(olClientRect.left+1, olClientRect.top+1, olClientRect.right-1, olClientRect.bottom-1);
    dc.SelectObject(&ol3Pen);
    dc.MoveTo(olInnerRect.right - 1, olInnerRect.top);
    dc.LineTo(olInnerRect.TopLeft());
    //dc.LineTo(olInnerRect.left, olInnerRect.bottom); -- this will paint outside the window
	dc.LineTo(olInnerRect.left, olInnerRect.bottom-1);

    // draw bottom & right dark-gray line
    dc.SelectObject(&ol4Pen);
    //dc.LineTo(olInnerRect.BottomRight()); -- this will paint outside the window
    //dc.LineTo(olInnerRect.right, olInnerRect.top - 1);
    dc.LineTo(olInnerRect.right-1, olInnerRect.bottom-1);
    dc.LineTo(olInnerRect.right-1, olInnerRect.top - 1);
    
    CString olWindowText;
    char clBuf[255]; 
	GetWindowText(clBuf, sizeof (clBuf)); 
	olWindowText = clBuf;
    
    dc.SetBkColor(lmBkColor);
    dc.SetTextColor(lmTextColor);
    
    int ilLeftPos;
    DWORD llStyle = GetStyle();
    
    // all static have SS_LEFT so we check SS_LEFT last
    if ((llStyle & SS_RIGHT) == SS_RIGHT)
    {
        ilLeftPos = olClientRect.right - dc.GetTextExtent(olWindowText, olWindowText.GetLength()).cx - 5;
        dc.TextOut(ilLeftPos, (olClientRect.Height() - dc.GetTextExtent(olWindowText, olWindowText.GetLength()).cy) / 2, olWindowText);
    }
    else if ((llStyle & SS_CENTER) == SS_CENTER)
    {
        ilLeftPos = (olClientRect.Width() - dc.GetTextExtent(olWindowText, olWindowText.GetLength()).cx) / 2;
        
        int ilSlashPosition = olWindowText.Find('/');
        if (ilSlashPosition != -1)
        {                                          
            ilSlashPosition++;
        
            CString olLeftText = olWindowText.Left(ilSlashPosition);
            CString olRightText = olWindowText.Mid(ilSlashPosition);
        
            dc.TextOut(ilLeftPos, (olClientRect.Height() - dc.GetTextExtent(olLeftText, olLeftText.GetLength()).cy) / 2, olLeftText);

            dc.SetTextColor(lmHilightColor);
        
            ilLeftPos = ((olClientRect.Width() - dc.GetTextExtent(olWindowText, olWindowText.GetLength()).cx) / 2) +
                dc.GetTextExtent(olLeftText, olLeftText.GetLength()).cx;
            dc.TextOut(ilLeftPos, (olClientRect.Height() - dc.GetTextExtent(olWindowText, olWindowText.GetLength()).cy) / 2, olRightText);
        }
        else
        {
            dc.TextOut(ilLeftPos, (olClientRect.Height() - dc.GetTextExtent(olWindowText, olWindowText.GetLength()).cy) / 2, olWindowText);
        }
    }
    else if ((llStyle & SS_LEFT) == SS_LEFT)
    {
        ilLeftPos = 5;
        dc.TextOut(ilLeftPos, (olClientRect.Height() - dc.GetTextExtent(olWindowText, olWindowText.GetLength()).cy) / 2, olWindowText);
    }

    dc.SelectObject(polOldPen);
    dc.SelectObject(polOldFont);
}

BOOL CCS3DStatic::OnEraseBkgnd(CDC* pDC) 
{
    CRect olClipRect;
    //pDC->GetClipBox(&olClipBox);     // can't be used
    GetClientRect(&olClipRect);

    CBrush olBrush(lmBkColor);
    CBrush *polOldBrush = pDC->SelectObject(&olBrush);
    pDC->PatBlt(olClipRect.left, olClipRect.top,
        olClipRect.Width(), olClipRect.Height(),
        PATCOPY);
    pDC->SelectObject(polOldBrush);
    
    return TRUE;
}

LONG CCS3DStatic::OnGetText(WPARAM wParam, LPARAM lParam)
{
    strncpy((LPSTR) lParam, omText, wParam);
    return omText.GetLength();
}

LONG CCS3DStatic::OnSetText(WPARAM wParam, LPARAM lParam)
{
    omText = (LPSTR)lParam;
    return 0L;
}

int CCS3DStatic::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
    if (CStatic::OnCreate(lpCreateStruct) == -1)
        return -1;
    
    SetWindowText(lpCreateStruct->lpszName);
    return 0;
}
