// ccs3dstatic.h : header file
//

#ifndef CCS3DSTATIC_H
#define CCS3DSTATIC_H

#include <afxwin.h>



/////////////////////////////////////////////////////////////////////////////
// CCS3DStatic window


/////////////////////////////////////////////////////////////////////////////
// Class declaration

//@Man:
//@Memo: Basic class {\bf CCS3DStatic}
/*@Doc:
*/
class CCS3DStatic : public CStatic
{
// Construction
public:
    //@ManMemo: Default constructor
    /*@Doc:
    */
    CCS3DStatic(bool bpReverse = false);

// Attributes
public:

// Operations
public:


void SetTextColor(COLORREF opColor);

void SetAfterSlashColor(COLORREF opColor);


// Overrides

// Implementation
public:
    //@ManMemo: Default destructor
    virtual ~CCS3DStatic();

    // Generated message map functions
public:
    //{{AFX_MSG(CCS3DStatic)
    afx_msg void OnPaint();
    afx_msg BOOL OnEraseBkgnd(CDC* pDC);
    afx_msg LONG OnGetText(WPARAM wParam, LPARAM lParam);
    afx_msg LONG OnSetText(WPARAM wParam, LPARAM lParam);
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	//}}AFX_MSG
    DECLARE_MESSAGE_MAP()

private:
    BOOL bmReverse;
    CString omText;
    
    CFont *pomFont;
    COLORREF lmBkColor;
    COLORREF lmTextColor;
    COLORREF lmHilightColor;
};

/////////////////////////////////////////////////////////////////////////////

#endif
