// CCSGlobl.cpp: implementation of the CCSGlobl .
//
//////////////////////////////////////////////////////////////////////
//
// CCSGlobl is part of the CoCo project
//
// It is in general the generic CCSGlobl-file but additionally holds some extra variables
//	marked by the author.
//
//	Version 1.0 on 04.11.1999 by WES
//
//

#include <stdafx.h>
#include <resource.h>
#include <CCSGlobl.h>
#include <CCSCedaData.h>
#include <CCSCedaCom.h>
#include <CCSDdx.h>
#include <CCSLog.h>
#include <CCSBcHandle.h>
//#include "BasicData.h"
//#include "InitialLoadDlg.h"
//#include "PrivList.h"
#include <CedaBasicData.h>
#include <CCSBasic.h>
//#include "DataSet.h"




// grid
#define GRID_MESSAGE_CELLCLICK			(WM_USER + 1)

//
// start of Walters part
//

int POS_URNO = 3;		// default values to be reset during initialization of app
int POS_APPL = 2;		// defaults are taken from NIZZAs database
int POS_COCO = 6;
int POS_STAT = 5;
int POS_STRN = 1;
int POS_TXID = 4;
int POS_STID = 0;

CString globfilterapp;
CString globfiltercoco;
CString globUrno;
CString globStatus;
CString globString;
CString globStringID;
CString globTextID;
CStringArray globAppList;
CStringArray globCoCoList;
//CStringArray globStrList;
CStringArray globUrnoList;

bool AutoCommitChanges = false;		// do not write back changes before closing program
bool isModified = false;			// should I ask for "save changes" when closing dialog ?
int showRCButton;					// initialized on startup from CEDA.INI
int showUrnoCol;					// --"--
int igAutoTranslationMode = NO_AUTO_TRANS;

GxGridTab *myglobGrid;				// used to mark duplicate entries while adding a new entry

//
// this two variables are defined globally to be set by the master dialog (WES)
//
CString refApp;
CString refCoCo = "US";		// as default, is overwritten by coco-selection in main dialog
CString defaultTitle = "UFIS language integration toolkit";

CString AllApplicationStr = "<all applications>";
CString AllLanguagesStr = "<all languages>";
int allcocos;
int allapplications;
int StdColWidthArray[] =	// default width for all grids
{ 50,100,100,25,100,300,150,100 };


COLORREF GetAppBkColor(CString appname)
{
	// use default CCS colors, defined in ogColors
	int i=globAppList.GetSize()-1;
	while ((i>=0) && (globAppList[i]!=appname) ) 
		i--; 

	if (i>=0) // application name already exists !
	{
		return MakeNumLtColor(i);
	}
	else
	{	//  sollte nicht vorkommen, ansonsten ist etwas faul !!!
		//globAppList.Add(appname);			// add at end of array
		//return MakeNumLtColor(globAppList.GetSize()-1);
		TRACE ("New Application %s found in function <GetAppBkColor> !!\n", appname );
		return RGB(255,255,255);
	}
}

//
// this function creates a unique color for numbers in interval [0..135]. Colors can be used as
//  background of a grid because they are very soft-toned ;-)
//
COLORREF MakeNumLtColor(int nr)
{

	nr++;		// dont's start with gray

	int r=0;
	int g=0;
	int b=0;


	if ((nr & 1)) r=25;
	if ((nr & 2)) g=25;
	if ((nr & 4)) b=25;
	if (nr >=128)     { r+=120; g+=120; b+=120; }
	else if (nr >=64) { r+=140; g+=140; b+=140; }
	else if (nr >=32) { r+=160; g+=160; b+=160; }
	else if (nr >=16) { r+=180; g+=180; b+=180; }
	else if (nr >=8)  { r+=210; g+=210; b+=210; }
	else			  { r+=230; g+=230; b+=230; }

	return (COLORREF)RGB(r,g,b);
}


void Grid2Internal(CString &s)
{
	// substitute some strings (decimal values)
	//	\n	->	#10
	//	\r	->	#13
	//	\t	->	#9
	int found;
	found = s.Replace ( "\\n", "\n" );
	found += s.Replace ( "\\t", "\t" );
	found += s.Replace ( "\\r", "\r" );
	/*
	CString l,r;
	int i;
	do
	{
		found=0;

		i=s.Find("\\n");
		if (i>=0) {
			l=s.Left(i);
			r=s.Right(s.GetLength()-i-2);
			s=l+"\012"+r;
			found=1; }

		i=s.Find("\\r");
		if (i>=0) {
			l=s.Left(i);
			r=s.Right(s.GetLength()-i-2);
			s=l+"\015"+r;
			found=1; }

		i=s.Find("\\t");
		if (i>=0) {
			l=s.Left(i);
			r=s.Right(s.GetLength()-i-2);
			s=l+"\011"+r;
			found=1; }
	}
	while (found);
	*/
}

void Internal2Grid(CString &s)
{
	// substitute some strings (decimal values)
	//	\n	<-	#13#10
	//	\r	<-	<space>#13
	//	\t	<-	<space>#7
	int found;
	CCSCedaData::MakeClientString(s);
	found = s.Replace ( "\n", "\\n" );
	found += s.Replace ( "\t", "\\t" );
	found += s.Replace ( "\r", "\\r" );
	/*
	CString l,r;

	do
	{
		found=0;

		i=s.Find("Flug&");
		if (i>=0)
		{
			found=0;
			int mynum=s[15];
		}

		i=s.Find('\012');
		if (i>=0) 
		{
			l=s.Left(i);
			r=s.Right(s.GetLength()-i-1);
			s=l+"\\n"+r;
			found=1; }

		i=s.Find('\015');
		if (i>=0) 
		{
			l=s.Left(i);
			r=s.Right(s.GetLength()-i-1);
			s=l+"\\r"+r;
			found=1; }

		i=s.Find('\011');
		if (i>=0) 
		{ 
			l=s.Left(i);
			r=s.Right(s.GetLength()-i-1);
			s=l+"\\t"+r;
			found=1; }
	}	
	while (found);
	*/
}

// PRF6106: two functions now using classlib character mapping
//			no longer copied from other source_
//

void MakeCedaString(CString &ropText)
{
	CCSCedaData::MakeCedaString ( ropText );
}

void MakeClientString(CString &ropText)
{
	CCSCedaData::MakeClientString ( ropText );
}

// end of Walters part
//



CString GetString(UINT ID)
{
	CString olRet;
	olRet.LoadString(ID);
	return olRet;
}


/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
// Global Variable Section

// �previous initialization

CString ogAppName = "LANGUAGETOOL"; 
CString ogCustomer = "WES"; 

char pcgUser[33];
char pcgPasswd[33];

char pcgHome[4] = "";
//char pcgHome4[5] = "EDDV";
char pcgTableExt[10] = "";
//char pcgVersion[12] = "4.3.5.01" ;
//char pcgInternalBuild[12] = "001";



CTimeSpan ogLocalDiff;
CTimeSpan ogUtcDiff;
CTime ogUtcStartTime;

//ofstream of_catch;
//ofstream of_debug;

bool bgDebug;
CCSBasic		ogCCSBasic;
CCSLog          ogLog(NULL);
CCSDdx			ogDdx;
CCSCedaCom      ogCommHandler(&ogLog, NULL);  
CCSBcHandle     ogBcHandle(&ogDdx, &ogCommHandler, &ogLog);
CedaBasicData	ogBCD(pcgHome, &ogDdx, &ogBcHandle, &ogCommHandler, &ogCCSBasic);



//BasicData      ogBasicData;
//DataSet		ogDataSet;
//CedaCfgData		ogCfgData;
//PrivList		ogPrivList;

//CInitialLoadDlg *pogInitialLoad;




CFont ogSmallFonts_Regular_6;
CFont ogSmallFonts_Regular_7;
CFont ogMSSansSerif_Regular_6;
CFont ogMSSansSerif_Regular_8;
CFont ogMSSansSerif_Regular_12;
CFont ogMSSansSerif_Regular_16;
CFont ogMSSansSerif_Bold_8;
CFont ogMSSansSerif_Bold_7;
CFont ogCourier_Bold_10;
CFont ogCourier_Bold_8;
CFont ogCourier_Regular_10;
CFont ogCourier_Regular_8;
CFont ogCourier_Regular_9;
CFont ogMS_Sans_Serif_8;

CFont ogTimesNewRoman_9;
CFont ogTimesNewRoman_12;
CFont ogTimesNewRoman_16;
CFont ogTimesNewRoman_30;

CFont ogSetupFont;


CFont ogScalingFonts[30];
FONT_INDEXES ogStaffIndex;
FONT_INDEXES ogFlightIndex;
FONT_INDEXES ogHwIndex;
FONT_INDEXES ogRotIndex;

BOOL bgIsInitialized = FALSE;

CBrush *ogBrushs[MAXCOLORS+1];
COLORREF ogColors[MAXCOLORS+1];
CTxtGrid *pogTxtGrid=0;

COLORREF lgBkColor = SILVER;
COLORREF lgTextColor = BLACK;
COLORREF lgHilightColor = WHITE;


CPoint ogMaxTrackSize = CPoint(1024, 768);
CPoint ogMinTrackSize = CPoint(1024 / 4, 768 / 4);

int   igDaysToRead;
int igFontIndex1;
int igFontIndex2;

CedaObject *prgTxtObject=0;

/////////////////////////////////////////////////////////////////////////////

void CreateBrushes()
{
	 int ilLc;

	for( ilLc = 0; ilLc < FIRSTCONFLICTCOLOR; ilLc++)
		ogColors[ilLc] = RED;
	ogColors[2] = GRAY;
	ogColors[3] = GREEN;
	ogColors[4] = RED;
	ogColors[5] = BLUE;
	ogColors[6] = SILVER;
	ogColors[7] = MAROON;
	ogColors[8] = OLIVE;
	ogColors[9] = NAVY;
	ogColors[10] = PURPLE;
	ogColors[11] = TEAL;
	ogColors[12] = LIME;
	ogColors[13] = YELLOW;
	ogColors[14] = FUCHSIA;
	ogColors[15] = AQUA;
	ogColors[16] = WHITE;
	ogColors[17] = BLACK;
	ogColors[18] = ORANGE;
	ogColors[19] = LGREEN;


// don't use index 21 it's used for Break jobs already

	for(ilLc = 0; ilLc < MAXCOLORS; ilLc++)
	{
		ogBrushs[ilLc] = new CBrush(ogColors[ilLc]);
	}

	// create a break job brush pattern
	/*
    delete ogBrushs[21];
	CBitmap omBitmap;
	omBitmap.LoadBitmap(IDB_BREAK);
	ogBrushs[21] = new CBrush(&omBitmap);
    */



}

void DeleteBrushes()
{
	for( int ilLc = 0; ilLc < MAXCOLORS; ilLc++)
	{
		CBrush *olBrush = ogBrushs[ilLc];
		delete olBrush;
	}


}


void InitFont() 
{
    CDC dc;
    dc.CreateCompatibleDC(NULL);

    LOGFONT logFont;
    memset(&logFont, 0, sizeof(LOGFONT));


////////////////////

	for(int i = 0; i < 8; i++)
	{
		logFont.lfCharSet= DEFAULT_CHARSET;
		logFont.lfHeight = - MulDiv(i, dc.GetDeviceCaps(LOGPIXELSY), 72);
		logFont.lfWeight = FW_NORMAL;
		logFont.lfQuality = PROOF_QUALITY;
		logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_SWISS;
		lstrcpy(logFont.lfFaceName, "Small Fonts"); 
		ogScalingFonts[i].CreateFontIndirect(&logFont);
	}
	for( i = 8; i < 20; i++)
	{ 
		logFont.lfCharSet= DEFAULT_CHARSET;
		logFont.lfHeight = - MulDiv(i, dc.GetDeviceCaps(LOGPIXELSY), 72);
		logFont.lfWeight = FW_NORMAL;
		logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_SWISS;
		logFont.lfQuality = PROOF_QUALITY;

		lstrcpy(logFont.lfFaceName, "MS Sans Serif"); /*"MS Sans Serif""Arial"*/
		ogScalingFonts[i/*GANTT_S_FONT*/].CreateFontIndirect(&logFont);
	}


    // ogMS_Sans_Serif_8.CreateFont(15, 0, 0, 0, 400, FALSE, FALSE, 0, ANSI_CHARSET, 
	//							 OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_SWISS, "MS Sans Serif");



	logFont.lfCharSet= DEFAULT_CHARSET;
    logFont.lfHeight = - MulDiv(8, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_NORMAL;
    logFont.lfPitchAndFamily = DEFAULT_PITCH | FF_SWISS;
    lstrcpy(logFont.lfFaceName, "MS Sans Serif");
    ogMS_Sans_Serif_8.CreateFontIndirect(&logFont);


////////////////////


	logFont.lfCharSet= DEFAULT_CHARSET;
    logFont.lfHeight = - MulDiv(6, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_NORMAL;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_SWISS;
    lstrcpy(logFont.lfFaceName, "Small Fonts");
    ogSmallFonts_Regular_6.CreateFontIndirect(&logFont);
        
	logFont.lfCharSet= DEFAULT_CHARSET;
    logFont.lfHeight = - MulDiv(7, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_NORMAL;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_SWISS;
    lstrcpy(logFont.lfFaceName, "Small Fonts");
    ogSmallFonts_Regular_7.CreateFontIndirect(&logFont);

	logFont.lfCharSet= DEFAULT_CHARSET;
    logFont.lfHeight = - MulDiv(10, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_BOLD;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_MODERN;
    lstrcpy(logFont.lfFaceName, "Courier New");
    ogCourier_Bold_10.CreateFontIndirect(&logFont);

	logFont.lfCharSet= DEFAULT_CHARSET;
	 logFont.lfHeight = - MulDiv(8, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_BOLD;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_MODERN;
    lstrcpy(logFont.lfFaceName, "Courier New");
    ogCourier_Bold_8.CreateFontIndirect(&logFont);

	logFont.lfCharSet= DEFAULT_CHARSET;
    logFont.lfHeight = - MulDiv(10, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_NORMAL;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_MODERN;
    lstrcpy(logFont.lfFaceName, "Courier New");
    ogCourier_Regular_10.CreateFontIndirect(&logFont);

	logFont.lfCharSet= DEFAULT_CHARSET;
    logFont.lfHeight = - MulDiv(8, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_NORMAL;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_MODERN;
    lstrcpy(logFont.lfFaceName, "Courier New");
    ogCourier_Regular_8.CreateFontIndirect(&logFont);


	logFont.lfCharSet= DEFAULT_CHARSET;
	logFont.lfHeight = - MulDiv(9, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_NORMAL;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_MODERN;
    lstrcpy(logFont.lfFaceName, "Courier New");
    ogCourier_Regular_9.CreateFontIndirect(&logFont);


/*
	logFont.lfHeight = - MulDiv(9, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = 200;
    logFont.lfQuality = PROOF_QUALITY;
    logFont.lfOutPrecision = OUT_CHARACTER_PRECIS;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_MODERN;
    lstrcpy(logFont.lfFaceName, "Fixedsys");
    ASSERT(ogCourier_Regular_9.CreateFontIndirect(&logFont));
*/
/*
    logFont.lfHeight = - MulDiv(6, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_NORMAL;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_SWISS;
    lstrcpy(logFont.lfFaceName, "Small Fonts"); /
    ASSERT(ogScalingFonts[MS_SANS6].CreateFontIndirect(&logFont));

    logFont.lfHeight = - MulDiv(8, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_NORMAL;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_SWISS;
    lstrcpy(logFont.lfFaceName, "MS Sans Serif");
    ASSERT(ogScalingFonts[MS_SANS8].CreateFontIndirect(&logFont));

    logFont.lfHeight = - MulDiv(12, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_BOLD;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_SWISS;
    lstrcpy(logFont.lfFaceName, "MS Sans Serif");
    ASSERT(ogScalingFonts[MS_SANS12].CreateFontIndirect(&logFont));

	// logFont.lfHeight = - MulDiv(16, dc.GetDeviceCaps(LOGPIXELSY), 72);
	logFont.lfHeight = - MulDiv(8, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_BOLD;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_SWISS;
    lstrcpy(logFont.lfFaceName, "MS Sans Serif");
    ASSERT(ogScalingFonts[MS_SANS16].CreateFontIndirect(&logFont));
*/ 

	ogStaffIndex.VerticalScale = MS_SANS8;
	ogStaffIndex.Chart = MS_SANS6;
	ogFlightIndex.VerticalScale = MS_SANS8;
	ogFlightIndex.Chart = MS_SANS6;
	ogHwIndex.VerticalScale = MS_SANS8;
	ogHwIndex.Chart = MS_SANS6;

	logFont.lfCharSet= DEFAULT_CHARSET;
    logFont.lfHeight = - MulDiv(8, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_BOLD;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_SWISS;
    lstrcpy(logFont.lfFaceName, "MS Sans Serif");
    ogMSSansSerif_Bold_8.CreateFontIndirect(&logFont);



	logFont.lfCharSet= DEFAULT_CHARSET;
    logFont.lfHeight = - MulDiv(7, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_BOLD;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_SWISS;
    lstrcpy(logFont.lfFaceName, "Verdana");
    ogMSSansSerif_Bold_7.CreateFontIndirect(&logFont);


	// 9 - 16 - 12 - 30

	logFont.lfCharSet= DEFAULT_CHARSET;
    logFont.lfHeight = - MulDiv(30, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_BOLD;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_ROMAN;
	//logFont.lfItalic = TRUE;
	logFont.lfQuality = PROOF_QUALITY;
    lstrcpy(logFont.lfFaceName, "Times New Roman");
    ogTimesNewRoman_30.CreateFontIndirect(&logFont);

	logFont.lfCharSet= DEFAULT_CHARSET;
    logFont.lfHeight = - MulDiv(16, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_NORMAL;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_ROMAN;
	//logFont.lfItalic = TRUE;
	logFont.lfQuality = PROOF_QUALITY;
    lstrcpy(logFont.lfFaceName, "Times New Roman");
    ogTimesNewRoman_16.CreateFontIndirect(&logFont);

	logFont.lfCharSet= DEFAULT_CHARSET;
    logFont.lfHeight = - MulDiv(12, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_NORMAL;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_ROMAN;
	//logFont.lfItalic = TRUE;
	logFont.lfQuality = PROOF_QUALITY;
    lstrcpy(logFont.lfFaceName, "Times New Roman");
    ogTimesNewRoman_12.CreateFontIndirect(&logFont);

	logFont.lfCharSet= DEFAULT_CHARSET;
    logFont.lfHeight = - MulDiv(9, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_NORMAL;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_ROMAN;
	//logFont.lfItalic = TRUE;
	logFont.lfQuality = PROOF_QUALITY;
    lstrcpy(logFont.lfFaceName, "Times New Roman");
    ogTimesNewRoman_9.CreateFontIndirect(&logFont);


	logFont.lfCharSet= DEFAULT_CHARSET;
	logFont.lfHeight = - MulDiv(9, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_NORMAL;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_MODERN;
    lstrcpy(logFont.lfFaceName, "Courier New");
    ogSetupFont.CreateFontIndirect(&logFont);


    dc.DeleteDC();
}

bool IsNewRecordAllowed ( CString &ropAppl, CString &ropCoco, CString &ropStid, 
						  CString &ropTxid, bool bpDispErr/*=true*/  )
{
	return IsChangedRecordAllowed ( ropAppl, ropCoco, ropStid, ropTxid, 
									CString("-1"), bpDispErr );
}


bool IsChangedRecordAllowed ( CString &ropAppl, CString &ropCoco, CString &ropStid,
							  CString &ropTxid, CString &ropUrno, 
							  bool bpDispErr/*=true*/ )
{
	CCSPtrArray<RecordSet>	olRecords;
	int						i;
	bool					blRet = true;
	CString					olMsg, olTitle;
	bool					blNew = ( ropUrno=="-1" );

	olTitle = blNew ? "Add entry failed" : "Change entry failed";
	if ( ropStid.IsEmpty () && ropTxid.IsEmpty () )
	{
		olMsg.Format ( "Error: StringID and TextId of %s record are empty!", 
					   blNew ? "new" : "changed" );
		if ( bpDispErr )
			MessageBox ( 0, olMsg, olTitle, MB_OK );
		return false;
	}
	if ( !ropStid.IsEmpty () )
	{
		ogBCD.GetRecordsExt("TXT", "APPL", "STID", ropAppl, ropStid, &olRecords );
		for ( i=0; blRet && (i<olRecords.GetSize()); i++ )
		{
			if ( ( olRecords[i].Values[POS_COCO] == ropCoco ) && 
				 ( olRecords[i].Values[POS_URNO] != ropUrno ) )
			{
				if ( bpDispErr )
					MessageBox( 0, "Error: Another record with this StringID already exists!", 
								olTitle, MB_OK );
				blRet = false;
			}
		}
		olRecords.DeleteAll();
	}
	if ( blRet && !ropTxid.IsEmpty () )
	{
		ogBCD.GetRecordsExt("TXT", "APPL", "TXID", ropAppl, ropTxid, &olRecords );
		for ( i=0; blRet && (i<olRecords.GetSize()); i++ )
		{
			if ( ( olRecords[i].Values[POS_COCO] == ropCoco ) && 
				 ( olRecords[i].Values[POS_URNO] != ropUrno ) )
			{
				if ( bpDispErr )
					MessageBox( 0, "Error: Another record with this TextID already exists!", 
								olTitle, MB_OK );
				blRet = false;
				int ilStatus = olRecords[i].GetStatus();
			}
		}
		olRecords.DeleteAll();
	}
	return blRet;
}



