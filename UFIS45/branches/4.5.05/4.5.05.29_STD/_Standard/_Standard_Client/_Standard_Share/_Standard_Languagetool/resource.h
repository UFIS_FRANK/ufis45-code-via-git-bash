//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by CoCo3.rc
//
#define IDC_SAVETODB                    3
#define IDD_COCO3_DIALOG                102
#define IDR_MAINFRAME                   128
#define IDD_ADDENTRY_DLG                129
#define IDD_COPYCOCO_DIALOG             130
#define IDD_CLONE_DLG                   131
#define IDD_COMPARE_DLG                 132
#define IDR_ACCELERATOR1                134
#define IDD_IMPORT_DLG                  135
#define IDR_MENU1                       136
#define IDD_NEW_COMPARE                 136
#define IDI_ICON1                       137
#define IDD_ADD_INTO_GRID               137
#define IDI_ICON2                       138
#define IDI_ICON3                       139
#define IDI_ICON4                       140
#define IDI_ICON5                       141
#define IDI_ICON6                       142
#define IDC_GRID1                       1000
#define IDC_EDIT2                       1002
#define IDC_EDIT3                       1003
#define IDC_EDIT4                       1004
#define IDC_COMBO1                      1004
#define IDC_EDIT5                       1005
#define IDC_COMBO2                      1005
#define IDC_EDIT6                       1006
#define IDC_BUTTON1                     1006
#define IDC_EDIT7                       1007
#define IDC_BUTTON2                     1007
#define IDC_BUTTON3                     1008
#define IDC_ADD_IN_GRID                 1009
#define IDC_BUTTON5                     1010
#define IDC_EDIT1                       1011
#define IDC_SRCGRID                     1015
#define IDC_DSTGRID                     1016
#define IDC_BUTTON6                     1017
#define IDC_CHECK1                      1018
#define IDC_DATETIMEPICKER1             1019
#define IDC_BUTTON40                    1020
#define IDC_COMBO3                      1022
#define IDC_COMBO4                      1023
#define IDC_COMBO5                      1024
#define IDC_COMBO6                      1025
#define IDC_BUTTON4                     1027
#define IDC_LIST1                       1028
#define IDC_LIST2                       1029
#define IDC_LIST3                       1030
#define IDC_PROGRESS1                   1031
#define IDC_BUTTON7                     1032
#define IDC_BUTTON8                     1033
#define IDC_COCO_LIST                   1034
#define IDC_NEW_LANG_CHK                1035
#define IDC_OLD_LANG_CHK                1036
#define IDC_COMPARE_GRID                1036
#define IDC_COPY_BTN                    1038
#define IDC_RIGHT_CB                    1039
#define IDC_LEFT_CB                     1040
#define IDC_APPL_CB                     1041
#define IDC_SHOW_DIFF_CHK               1042
#define IDC_ADD_GRID                    1043
#define IDC_AUTO_TRANSLATE_CHK          1044
#define IDC_WANT_CONFIRMATION_CHK       1045
#define ID_PROJECT_IMPORTRC             32772
#define ID_EDIT_ADDENTRY                32773
#define ID_EDIT_DELETEENTRY             32774
#define ID_EDIT_SEARCH                  32776
#define ID_EDIT_COMPARE                 32777
#define ID_EDIT_CLONEAPP                32779
#define ID_EDIT_CLONESELECTION          32780

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        143
#define _APS_NEXT_COMMAND_VALUE         32782
#define _APS_NEXT_CONTROL_VALUE         1046
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
