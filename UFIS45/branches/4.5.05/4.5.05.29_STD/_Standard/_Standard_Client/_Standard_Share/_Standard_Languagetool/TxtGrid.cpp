// TxtGrid.cpp: implementation of the CTxtGrid class.
//
//////////////////////////////////////////////////////////////////////

#include <stdafx.h>

#include <CedaBasicData.h>
#include <ccsglobl.h>
#include <CoCo3.h>
#include <TxtGrid.h>

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//char *omTxtFields[] = { "URNO", "APPL", "COCO", "STAT", "STRG", "TXID", "STID" };

extern int bmSortAscend;

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CTxtGrid::CTxtGrid()
{
	omGridFields.Add ("URNO"); 
	omGridFields.Add ("APPL"); 
	omGridFields.Add ("COCO"); 
	omGridFields.Add ("STAT"); 
	omGridFields.Add ("STRG"); 
	omGridFields.Add ("TXID"); 
	omGridFields.Add ("STID"); 
	imCols = omGridFields.GetSize();
	//imCols = 7;	
	imCompCol = 1;	//  Default-M��ig nach appl sortieren
	imDataIdx = new int[imCols];

	for ( int i=0; i<imCols; i++ )
		imDataIdx[i] = ogBCD.GetFieldIndex("TXT",omGridFields[i]);
	omSortOrder.AddTail ( (CObject*)1 ); 
	omSortOrder.AddTail ( (CObject*)2 ); 
	omSortOrder.AddTail ( (CObject*)5 ); 
}

CTxtGrid::~CTxtGrid()
{
	delete imDataIdx;
	omGridData.DeleteAll();
}

BOOL CTxtGrid::GetStyleRowCol(ROWCOL nRow, ROWCOL nCol, CGXStyle& style, GXModifyType mt, int nType)
{   
	if (mt == gxRemove) // not supported      
		return FALSE;
   // Note: I do not distinct between gxApplyNew, gxCopy and gxOverride
   ASSERT(nRow <= LONG_MAX);   
   long nRecord = (long) nRow;   
   if (nType == -1)
   {      
	   // here you can return the style for a complete row, column or table
      return FALSE;   
   }   
   else if (nRow == 0 && nCol == 0)   
   {
      // style for the top-left button      
	   return FALSE;   
   }
//   else if (nRow == 0)   
//   {      // Column headers      
//	   style.SetValue(nCol);   
//   }
   else    
   {      // Cell in the grid
	  if(nRow == 0)
	  {
		  if(nCol != 0)
		  {
			 //omFieldsList
			//style.SetValue(GetDocument()->omFieldsList[nCol-1]);
			style.SetValue(omGridFields[nCol-1]);
		  }
	  }
	  else if(nCol == 0)
	  {
		  if(nRow != 0)
		  {
 			 CString olText;
			 olText.Format("%d", nRow);
			 style.SetValue(olText);
		  }
	  }
	  else 
	  {
		if( nCol <= (UINT)imCols )
		{
			CString cStr, olAppl, *polData=0, olUrno;
			RecordSet *polRec=0;
			
			if ( !omGridData[nRow-1].ptr )
			{
				olUrno = omGridData[nRow-1].urno;
				if ( !olUrno.IsEmpty() )
					polRec = GetAdressOfTxtRec ( olUrno );
			}
			else
				polRec = omGridData[nRow-1].ptr;
			if( _CrtIsValidHeapPointer(polRec) )
					polData = polRec->Values.GetData();
			
			if ( polData && _CrtIsValidHeapPointer(polData) )	//hag000809
			{
				cStr = polRec->Values[imDataIdx[nCol-1]];
				Internal2Grid ( cStr );
				style.SetValue( cStr );
				olAppl = polRec->Values[ogBCD.GetFieldIndex("TXT","APPL")];
				style.SetInterior(GetAppBkColor(olAppl)); 
			}
			else 
			{
				TRACE ( "Invalid Heappointer found for row %d\n", nRow );
				return FALSE;
			}

		}
		else
		{
			style.SetValue("");
		}
	  }
      //style.SetValue(myData[nRow][nCol].text);      
	  // - Or -
	  return TRUE;   
   }   
   return FALSE;
}

BOOL CTxtGrid::StoreStyleRowCol(ROWCOL nRow, ROWCOL nCol, const CGXStyle* pStyle, GXModifyType mt, int nType)
{   
	if (mt == gxRemove) // not supported      
		return FALSE;
	// Note: I do not distinct between gxApplyNew, gxCopy and gxOverride
	if (nType == -1)   
	{
		// here you could store the style for a complete row, column or table
		return FALSE;   
	}   
	else if (nRow == 0 && nCol == 0)   
	{
		// style for the top-left button      
		return FALSE;   
	}
	else if (nRow == 0)   
	{   // Column headers      
		return FALSE;   
	}   
	else 
	{  
		// cell      
		bool blOk = false;
		CString olOldValue;
		CString olUrno = omGridData[nRow-1].ptr->Values[ogBCD.GetFieldIndex("TXT","URNO")] ;
		if ( !olUrno.IsEmpty () )
		{
			CString olStr = pStyle->GetValue() ;
			Grid2Internal ( olStr );
			olOldValue = ogBCD.GetField( "TXT", "URNO", olUrno, omGridFields[nCol-1] );
			if ( (omGridFields[nCol-1]=="STRG") && (olOldValue!=olStr) )
			{
				blOk = ogBCD.SetField( "TXT", "URNO", olUrno, omGridFields[nCol-1], olStr );
				theApp.TextChanged ( omGridData[nRow-1].ptr->Values[ogBCD.GetFieldIndex("TXT","COCO")], 
									 olOldValue, olStr );
			}	
		}
		return blOk;
   }   // unreferenced:   
   //mt;   
   return FALSE;
}

ROWCOL CTxtGrid::GetRowCount()
{
	return omGridData.GetSize();
}


void CTxtGrid::LoadData ( char *pcpCocoFilter, char *pcpApplFilter, bool bpGeneral )
{
	BeginWaitCursor();
	omGridData.DeleteAll();
	CString olAppl, olCoco, olUrno;
	if ( !prgTxtObject && 
		 !ogBCD.omObjectMap.Lookup("TXT",(void*&)prgTxtObject) )
		return;
	CCSPtrArray<RecordSet>  *polObjectData = prgTxtObject->GetDataBuffer();

	for ( int i=0; i<ogBCD.GetDataCount( "TXT" ); i++ )
	{
		if ( (*polObjectData)[i].GetStatus() == DATA_DELETED )
			continue;
		if ( pcpCocoFilter )	//  Filter f�r Sprache gesetzt
		{
			olCoco = ogBCD.GetField ( "TXT", i, "COCO" );
			if ( pcpCocoFilter != olCoco )
				continue;	//  Sprache des i-ten Records != filter-> auslassen
		}
		olAppl = ogBCD.GetField ( "TXT", i, "APPL" );

		if ( ( ( olAppl != "GENERAL" )  || !bpGeneral ) && 
			 pcpApplFilter && ( olAppl != pcpApplFilter ) ) 
			continue;
		GRIDDATA rlData;
		rlData.urno = ogBCD.GetField ( "TXT", i, "URNO" );
		rlData.ptr = &((*polObjectData)[i]) ;
		//  omGridData.New( &((*polObjectData)[i]) );
		omGridData.New(rlData);
	}
	GetParam()->EnableUndo(FALSE);
	SetRowCount(omGridData.GetSize() );
	GetParam()->EnableUndo(TRUE);

	omGridData.Sort ( CompareData );
	EndWaitCursor();
}

void CTxtGrid::SortTable ( ROWCOL ipCol )
{
	DWORD llStart = GetTickCount();
	if ( ( ipCol == 0 ) || ( (int)ipCol > imCols ) )
		return;
	imCompCol = ipCol-1;
	bmSortAscend = !bmSortAscend;
	POSITION pos;
	if ( pos = omSortOrder.Find ( (CObject*) imCompCol ) )
		omSortOrder.RemoveAt ( pos ); 
	omSortOrder.AddHead ( (CObject*)imCompCol ); 

	BeginWaitCursor();
	omGridData.Sort ( CompareData );
	Redraw ();
	EndWaitCursor();

	DWORD llEnd = GetTickCount();
	llEnd -= llStart;
	CString olText;
	double dlSec = (double)llEnd  / 1000.0;
	olText.Format ( "Funktion dauerte mit %d records %.3lf sec", 
					omGridData.GetSize(), dlSec );
	TRACE ( "%s   %s\n", "<CTxtGrid::SortTable>", olText );   
//	MessageBox ( olText, "<CTxtGrid::SortTable>" );
}

static int CompareData(GRIDDATA const **popItem1, GRIDDATA const  **popItem2)
{
	//CString olStr1, olStr2;
	int ilIdx;
	int ilRet=0;
	POSITION pos;

	if ( pogTxtGrid && (pogTxtGrid->imCompCol>=0) )
	{	
		pos  = pogTxtGrid->omSortOrder.GetHeadPosition();
		while ( !ilRet && pos )
		{
			ilIdx = pogTxtGrid->imDataIdx[(int)pogTxtGrid->omSortOrder.GetNext(pos)];
			//hag000809 ilRet = stricmp ( (**popItem1)->Values[ilIdx], (**popItem2)->Values[ilIdx] );
			ilRet = stricmp ( (**popItem1).ptr->Values[ilIdx], (**popItem2).ptr->Values[ilIdx] );
		}
		if ( !bmSortAscend )
			ilRet *= -1;
		return ilRet;
	}	
	else
		return 0;
}

static int CompareUrnos(CString const **popItem1, CString const  **popItem2)
{
	//CString olStr1, olStr2;
	int ilIdx;
	int ilRet=0;
	POSITION pos;
	RecordSet olRec1, olRec2;

	if ( pogTxtGrid && (pogTxtGrid->imCompCol>=0) )
	{	
		if ( !ogBCD.GetRecord ( "TXT", "URNO", (**popItem1), olRec1 ) || 
			 !ogBCD.GetRecord ( "TXT", "URNO", (**popItem2), olRec2 ) )
			 return 0;
		pos  = pogTxtGrid->omSortOrder.GetHeadPosition();
		while ( !ilRet && pos )
		{
			ilIdx = pogTxtGrid->imDataIdx[(int)pogTxtGrid->omSortOrder.GetNext(pos)];
			//olStr1 = (**popItem1)->Values[ilIdx];
			//olStr2 = (**popItem2)->Values[ilIdx];
			//ilRet = olStr1.CompareNoCase ( olStr2 );
			ilRet = stricmp ( olRec1.Values[ilIdx], olRec2.Values[ilIdx] );
		}
		if ( !bmSortAscend )
			ilRet *= -1;
		return ilRet;
	}	
	else
		return 0;
}


RecordSet *GetAdressOfTxtRec ( CString& ropUrno )
{
	RecordSet *polRec=0;
	if ( !prgTxtObject 
		 && !ogBCD.omObjectMap.Lookup("TXT",(void *& )prgTxtObject) )
		return 0;
	if ( ropUrno.IsEmpty() )
		return 0;
	CMapStringToPtr *polTxtUrnoMap;
	CCSPtrArray<RecordSet> *prlRecords;
	
	polTxtUrnoMap = prgTxtObject->GetMapPointer("URNO");
	if ( polTxtUrnoMap &&
		polTxtUrnoMap->Lookup ( ropUrno, (void *& )prlRecords ) )
		polRec = &((*prlRecords)[0]);
			
	if(  !polRec || !_CrtIsValidHeapPointer(polRec) || 
		 !_CrtIsValidHeapPointer(polRec->Values.GetData() ) )
	{
		CCSPtrArray<RecordSet>  *polObjectData = prgTxtObject->GetDataBuffer();
		CString olUrno;
		for ( int i=0; i<prgTxtObject->GetDataCount(); i++ )
		{
			if ( (*polObjectData)[i].GetStatus() == DATA_DELETED )
				continue;
			olUrno = (*polObjectData)[i].Values[POS_URNO];
			if ( olUrno == ropUrno )
			{
				polRec = &((*polObjectData)[i]) ;
				break;
			}
		}
	}
	return polRec;
}