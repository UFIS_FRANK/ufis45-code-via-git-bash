// CompareDlg.cpp : implementation file
//

#include <stdafx.h>

#include <CCSglobl.h>
#include <CoCo3.h>
#include <CompareDlg.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


IMPLEMENT_DYNCREATE(CCompareDlg, CDialog)

/////////////////////////////////////////////////////////////////////////////
// CCompareDlg dialog


CCompareDlg::CCompareDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CCompareDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CCompareDlg)
	imSelApplIdx = -1;
	imSelLeftCoco = -1;
	imSelRightCoco = -1;
	bmShowDifferences = FALSE;
	//}}AFX_DATA_INIT
	pbmHideArray = 0;
	imDataStatus = DATA_UNCHANGED;
}

CCompareDlg::~CCompareDlg()
{
	if ( pbmHideArray )
		delete pbmHideArray ;
}

void CCompareDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCompareDlg)
	DDX_Control(pDX, IDC_RIGHT_CB, omRightCocoCb);
	DDX_Control(pDX, IDC_LEFT_CB, omLeftCocoCb);
	DDX_Control(pDX, IDC_APPL_CB, omApplCb);
	DDX_CBIndex(pDX, IDC_APPL_CB, imSelApplIdx);
	DDX_CBIndex(pDX, IDC_LEFT_CB, imSelLeftCoco);
	DDX_CBIndex(pDX, IDC_RIGHT_CB, imSelRightCoco);
	DDX_Check(pDX, IDC_SHOW_DIFF_CHK, bmShowDifferences);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CCompareDlg, CDialog)
	//{{AFX_MSG_MAP(CCompareDlg)
	ON_CBN_SELENDOK(IDC_LEFT_CB, OnSelendokLeftCb)
	ON_CBN_SELENDOK(IDC_RIGHT_CB, OnSelendokRightCb)
	ON_CBN_SELENDOK(IDC_APPL_CB, OnSelendokApplCb)
	ON_WM_SIZE()
	ON_BN_CLICKED(IDC_SHOW_DIFF_CHK, OnShowDiffChk)
	ON_BN_CLICKED(IDC_COPY_BTN, OnCopy)
	ON_WM_CLOSE()
	//}}AFX_MSG_MAP
	ON_MESSAGE(GX_ROWCOL_CHANGED, OnGridChangeText)
	ON_MESSAGE(GX_GRID_SORTED,OnGridSorted)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCompareDlg message handlers

void CCompareDlg::OnSelendokLeftCb() 
{
	// TODO: Add your control notification handler code here
	if ( omLeftCocoCb.GetCurSel() != imSelLeftCoco )
		FillGrid();		//  nur wenn sich Auswahl ge�ndert Grid neu f�llen
}

void CCompareDlg::OnSelendokRightCb() 
{
	// TODO: Add your control notification handler code here
	if ( omRightCocoCb.GetCurSel() != imSelRightCoco )
		FillGrid();		//  nur wenn sich Auswahl ge�ndert Grid neu f�llen
}

BOOL CCompareDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	IniGrid ();
	IniComboBoxes ();
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}


void CCompareDlg::IniComboBoxes ()
{
	int i;
	for ( i=0; i<globAppList.GetSize(); i++ )
		omApplCb.AddString ( globAppList[i] );
	if ( !refApp.IsEmpty() && 
		 ( (i=omApplCb.FindStringExact(-1, refApp ) ) >= 0 ) 
		)
		omApplCb.SetCurSel ( i );
	for ( i=0; i<globCoCoList.GetSize(); i++ )
	{
		omLeftCocoCb.AddString ( globCoCoList[i] );
		omRightCocoCb.AddString ( globCoCoList[i] );
	}
	if ( !refCoCo.IsEmpty() && 
		 ( (i=omLeftCocoCb.FindStringExact(-1, refCoCo ) ) >= 0 ) 
		)
		omLeftCocoCb.SetCurSel ( i );
	//  wenn "engl." nicht schon links ausgew�hlt, recht ausw�hlen
	if ( ( refCoCo != "US" ) &&			
		 ( (i=omRightCocoCb.FindStringExact(-1, "US" ) ) >= 0 ) 
		)
		omRightCocoCb.SetCurSel ( i );
	FillGrid();		
}

void CCompareDlg::IniGrid ()
{
	omGrid.SetParent(this);
	omGrid.SubclassDlgItem(IDC_COMPARE_GRID, this);
	omGrid.Initialize();
	omGrid.GetParam()->EnableUndo(FALSE);

	// set headers

	omGrid.SetColCount(8);
	omGrid.SetValueRange(CGXRange(0, 1), "TextID");		//  10%
	omGrid.SetValueRange(CGXRange(0, 2), "StringID");	//  10%	
	omGrid.SetValueRange(CGXRange(0, 3), "Urno1");		//  hidden
	omGrid.SetValueRange(CGXRange(0, 4), "Status1");	//  10%
	omGrid.SetValueRange(CGXRange(0, 5), "String1");	//  30%	
	omGrid.SetValueRange(CGXRange(0, 6), "Urno2");		//  hidden
	omGrid.SetValueRange(CGXRange(0, 7), "Status2");	//  10%	
	omGrid.SetValueRange(CGXRange(0, 8), "String2");	//  30%	
	if (!showUrnoCol) 
	{
		omGrid.HideCols ( 3, 3 );
		omGrid.HideCols ( 6, 6 );
	}
	SetColWidths ();
	omGrid.GetParam()->EnableUndo(TRUE);
}


void CCompareDlg::OnSelendokApplCb() 
{
	// TODO: Add your control notification handler code here
	if ( omApplCb.GetCurSel() != imSelApplIdx )
		FillGrid();		//  nur wenn sich Auswahl ge�ndert Grid neu f�llen
}


void CCompareDlg::FillGrid ()
{
	CString					olRefAppl, olLeftCoco, olRightCoco;
	CCSPtrArray<RecordSet>	olLeftRecs, olRightRecs;
	int						i, ilRow=0, ilActRow;
	CString					olKey, olStrg, olTxid, olStid, olUrno, olStat;
	CMapStringToOb			olLang1Map;
	BOOL					blFound;

	UpdateData ();
	omGrid.GetParam()->EnableUndo(FALSE);
	i = omGrid.GetRowCount ();
	if ( i >= 1 )
		omGrid.RemoveRows( 1, i );
	if ( pbmHideArray )
	{
		delete pbmHideArray ;
		pbmHideArray = 0;	
	}

	if ( ( imSelApplIdx<0 ) || ( imSelLeftCoco<0 ) || ( imSelRightCoco<0 ) )
		return ;		//  in allen drei combo-boxen mu� etwas ausgew�hlt sein

	BeginWaitCursor();
	omGrid.LockUpdate(TRUE);
	omApplCb.GetLBText ( imSelApplIdx, olRefAppl );
	omLeftCocoCb.GetLBText ( imSelLeftCoco, olLeftCoco );
	omRightCocoCb.GetLBText ( imSelRightCoco, olRightCoco );

	ogBCD.GetRecordsExt ( "TXT", "APPL", "COCO", olRefAppl, olLeftCoco, 
						  &olLeftRecs );
	ogBCD.GetRecordsExt ( "TXT", "APPL", "COCO", olRefAppl, olRightCoco, 
						  &olRightRecs );
	omGrid.SetRowCount ( olLeftRecs.GetSize() +  olRightRecs.GetSize() );	//  obergrenze

	//  fill Grid with language1 ( col 1..5 )
	for ( i=0; i<olLeftRecs.GetSize(); i++ )
	{
		ilRow++;
		omGrid.SetValueRange(CGXRange(ilRow,0), (WORD)ilRow );	// lfd. Nr

		olTxid = olLeftRecs[i].Values[POS_TXID]; 
		omGrid.SetValueRange(CGXRange(ilRow,1), olTxid);		// textID
 		
		olStid = olLeftRecs[i].Values[POS_STID]; 
		omGrid.SetValueRange(CGXRange(ilRow,2), olStid);		// stringID

		olUrno = olLeftRecs[i].Values[POS_URNO]; 
		omGrid.SetValueRange(CGXRange(ilRow,3), olUrno);		// urno

		olStat = olLeftRecs[i].Values[POS_STAT];  
		omGrid.SetValueRange(CGXRange(ilRow,4), olStat);		// status
		
		olStrg = olLeftRecs[i].Values[POS_STRN]; 
		MakeClientString(olStrg); 
		Internal2Grid(olStrg); 
		omGrid.SetValueRange(CGXRange(ilRow,5), olStrg);		// string
		
		//olKey.Format ( "%s|%s", olTxid, olStid );
		if ( olStid.IsEmpty() )
			olKey.Format ( "%s|", olTxid );
		else
			olKey.Format ( "|%s", olStid );

		olLang1Map.SetAt ( olKey, (CObject*) ilRow );
	}

	

	//  fill Grid with language2 ( col 7..8 )
	//  Add Rows, if necessary 
	for ( i=0; i<olRightRecs.GetSize(); i++ )
	{
		olTxid = olRightRecs[i].Values[POS_TXID]; 
		olStid = olRightRecs[i].Values[POS_STID]; 
		olUrno = olRightRecs[i].Values[POS_URNO]; 
		olStat = olRightRecs[i].Values[POS_STAT];  
		olStrg = olRightRecs[i].Values[POS_STRN]; 
		MakeClientString(olStrg); 
		Internal2Grid(olStrg); 
		
		//olKey.Format ( "%s|%s", olTxid, olStid );
		if ( olStid.IsEmpty() )
			olKey.Format ( "%s|", olTxid );
		else
			olKey.Format ( "|%s", olStid );

		//  Gibt es Kombination TXID, STID auch in Language 1 ?
		blFound = olLang1Map.Lookup ( olKey, (CObject*&)ilActRow ) ;
		if ( !blFound ) 
			ilActRow = ++ilRow;

		omGrid.SetValueRange(CGXRange(ilActRow,6), olUrno);		// urno
		omGrid.SetValueRange(CGXRange(ilActRow,7), olStat);		// status
		omGrid.SetValueRange(CGXRange(ilActRow,8), olStrg);		// string

		if ( !blFound )
		{
			omGrid.SetValueRange(CGXRange(ilActRow,1), olTxid);		// textID
			omGrid.SetValueRange(CGXRange(ilActRow,2), olStid);		// stringID
		}
	}
	if ( (int)omGrid.GetRowCount() >  ilRow )
		omGrid.RemoveRows ( ilRow+1, omGrid.GetRowCount() );

	pbmHideArray = new BOOL [ilRow];
	if ( pbmHideArray )
	{
		for ( i=1; i<= ilRow; i++ )
			if ( omGrid.GetValueRowCol(i,3).IsEmpty()  ||
				 omGrid.GetValueRowCol(i,6).IsEmpty() )
				 pbmHideArray[i-1] = FALSE;
			else
				 pbmHideArray[i-1] = TRUE;
		if ( bmShowDifferences )
			omGrid.HideRows ( 1, omGrid.GetRowCount(), TRUE, pbmHideArray );
	}

	olLeftRecs.DeleteAll ();
	olRightRecs.DeleteAll ();
	omGrid.LockUpdate(FALSE);
	omGrid.Redraw();
	omGrid.GetParam()->EnableUndo(TRUE);
	SetColWidths ();
	EndWaitCursor();
}

void CCompareDlg::OnSize(UINT nType, int cx, int cy) 
{
	CDialog::OnSize(nType, cx, cy);
	
	// TODO: Add your message handler code here
	CRect olRect;
	if ( omGrid.m_hWnd )
	{
		GetClientRect(&olRect);
		olRect.top=50;
		omGrid.MoveWindow(&olRect); 
		SetColWidths ();
	}
	
}

void CCompareDlg::SetColWidths ()
{
	CRect olRect = omGrid.GetGridRect();
	int ilAll = olRect.Width() - omGrid.GetColWidth( 0 );
	
	int ilWidths[9];
	for ( ROWCOL i=0; i<omGrid.GetColCount(); i ++ )
		ilWidths[i] = omGrid.GetColWidth(i);
	if ( showUrnoCol )
	{
		omGrid.SetColWidth ( 3, 3, (int)((float)ilAll*0.06) );
		omGrid.SetColWidth ( 6, 6, (int)((float)ilAll*0.06) );
		ilAll -= 2*omGrid.GetColWidth ( 3 );
	}
	omGrid.SetColWidth ( 1, 1, (int)((float)ilAll*0.15) );
	omGrid.SetColWidth ( 2, 2, (int)((float)ilAll*0.05) );
	omGrid.SetColWidth ( 4, 4, (int)((float)ilAll*0.08) );
	omGrid.SetColWidth ( 5, 5, (int)((float)ilAll*0.32) );
	omGrid.SetColWidth ( 7, 7, (int)((float)ilAll*0.08) );
	omGrid.SetColWidth ( 8, 8, (int)((float)ilAll*0.32) );
}		 

void CCompareDlg::OnShowDiffChk() 
{
	// TODO: Add your control notification handler code here
	BOOL blOldShowDiff = bmShowDifferences ;
	UpdateData ();
	if ( blOldShowDiff != bmShowDifferences )
	{
		if ( bmShowDifferences && pbmHideArray )
			omGrid.HideRows ( 1, omGrid.GetRowCount(), TRUE, pbmHideArray );
		else
			omGrid.HideRows ( 1, omGrid.GetRowCount(), FALSE );
		
	}

}

void CCompareDlg::OnCopy() 
{
	// TODO: Add your control notification handler code here
	CString olLeftUrno, olRightUrno, olNewUrno, olNewCoco;
	CRowColArray olSelRows;
	BOOL blOk, blLeft2Right ;
	RecordSet olRecord;
	ROWCOL  i, nRow, ilSelCount = omGrid.GetSelectedRows( olSelRows );

	for ( i=0; i<ilSelCount; i++ )
	{
		nRow = olSelRows[i];
		olLeftUrno = omGrid.GetValueRowCol(nRow,3);
		olRightUrno = omGrid.GetValueRowCol(nRow,6);
		if ( olLeftUrno.IsEmpty() == olRightUrno.IsEmpty() )
			continue;		//  beide Seiten leer oder gef�llt -> nichts zu tun

		blLeft2Right = olRightUrno.IsEmpty();
		if ( blLeft2Right )
		{
			omRightCocoCb.GetLBText ( imSelRightCoco, olNewCoco );
			blOk = ogBCD.GetRecord ( "TXT", "URNO", olLeftUrno, olRecord );
		}
		else
		{
			omLeftCocoCb.GetLBText ( imSelLeftCoco, olNewCoco );
			blOk = ogBCD.GetRecord ( "TXT", "URNO", olRightUrno, olRecord );
		}
		if ( blOk )
		{
			olRecord.Values[POS_COCO] = olNewCoco;
			olRecord.Values[POS_STAT] = "NEW" ;
			olNewUrno.Format ( "%lu", ogBCD.GetNextUrno() );
			olRecord.Values[POS_URNO] = olNewUrno ;
			olRecord.Values[POS_STAT] = "NEW" ;
			blOk = ogBCD.InsertRecord("TXT",olRecord, AutoCommitChanges) ;
		}
		if ( blOk )
		{
			//globStrList.Add(olRecord.Values[POS_STRN]);
			globUrnoList.Add(olNewUrno);			// and also remember urnos
			omGrid.SetValueRange ( CGXRange(nRow,  blLeft2Right ? 6 : 3),
								   olNewUrno );
			omGrid.SetValueRange ( CGXRange(nRow, blLeft2Right ? 7 : 4), "NEW");
			omGrid.SetValueRange ( CGXRange(nRow, blLeft2Right ? 8 : 5), 
								   olRecord.Values[POS_STRN] );
			imDataStatus = DATA_NEW;
		}
		
	}
}

void CCompareDlg::OnClose() 
{
	// TODO: Add your message handler code here and/or call default
	
	CDialog::OnClose();
}

void CCompareDlg::OnGridChangeText( WPARAM nRow, LPARAM nCol )
{
	BOOL blOldHideStatus; 
	if ( pbmHideArray && ( nRow>0 ) )
	{
		blOldHideStatus = pbmHideArray[nRow-1]; 
		if ( omGrid.GetValueRowCol(nRow,3).IsEmpty()  ||
			 omGrid.GetValueRowCol(nRow,6).IsEmpty() )
			 pbmHideArray[nRow-1] = FALSE;
		else
			 pbmHideArray[nRow-1] = TRUE;
		if ( blOldHideStatus != pbmHideArray[nRow-1] )   //  d.h. Text neu 
		{
			imDataStatus = DATA_NEW;
			if ( bmShowDifferences )
				omGrid.HideRows ( 1, omGrid.GetRowCount(), TRUE, pbmHideArray );
		}
		else
			if (imDataStatus == DATA_UNCHANGED)
				imDataStatus = DATA_CHANGED;
			
	}
}

void CCompareDlg::OnGridSorted( WPARAM nRow, LPARAM nCol )
{
	int ilRow, i;
	
	omGrid.GetParam()->EnableUndo(FALSE);
	omGrid.LockUpdate(TRUE);
		
	if ( pbmHideArray )
	{
		delete pbmHideArray ;
		pbmHideArray = 0;	
	}
	ilRow = omGrid.GetRowCount() ;
	if ( ilRow < 1 )
		return;
	pbmHideArray = new BOOL [ilRow];
	if ( pbmHideArray )
	{
		for ( i=1; i<= ilRow; i++ )
			if ( omGrid.GetValueRowCol(i,3).IsEmpty()  ||
				 omGrid.GetValueRowCol(i,6).IsEmpty() )
				 pbmHideArray[i-1] = FALSE;
			else
				 pbmHideArray[i-1] = TRUE;
		if ( bmShowDifferences )
			omGrid.HideRows ( 1, omGrid.GetRowCount(), TRUE, pbmHideArray );
	}

	omGrid.LockUpdate(FALSE);
	omGrid.Redraw(); 
	omGrid.GetParam()->EnableUndo(TRUE);
	
}