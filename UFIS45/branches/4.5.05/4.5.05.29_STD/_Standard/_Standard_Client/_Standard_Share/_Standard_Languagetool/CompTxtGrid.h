// CompTxtGrid.h: interface for the CCompTxtGrid class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_COMPTXTGRID_H__54410F71_EB5D_11D3_93EB_00001C033B5D__INCLUDED_)
#define AFX_COMPTXTGRID_H__54410F71_EB5D_11D3_93EB_00001C033B5D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <GxGridTab.h>

class CCompTxtGrid : public GxGridTab  
{
public:
	CCompTxtGrid();
	virtual ~CCompTxtGrid();
protected:
	BOOL OnEndEditing(ROWCOL nRow, ROWCOL nCol);
	BOOL OnStartEditing(ROWCOL nRow, ROWCOL nCol);
	BOOL OnLButtonDblClkRowCol(ROWCOL nRow, ROWCOL nCol, UINT nFlags, CPoint pt);

	CString GetActualAppl ();
	CString GetActualCoco ( ROWCOL ipColumn );

};

#endif // !defined(AFX_COMPTXTGRID_H__54410F71_EB5D_11D3_93EB_00001C033B5D__INCLUDED_)
