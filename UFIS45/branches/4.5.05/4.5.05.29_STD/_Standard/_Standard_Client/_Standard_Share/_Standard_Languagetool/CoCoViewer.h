#if !defined(AFX_COCOVIEWER_H__441B9495_91D2_11D3_8FB2_0050DA1CAD13__INCLUDED_)
#define AFX_COCOVIEWER_H__441B9495_91D2_11D3_8FB2_0050DA1CAD13__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// CoCoViewer.h : header file
//

#include <GxGridTab.h>

/////////////////////////////////////////////////////////////////////////////
// CCoCoViewer dialog

class CCoCoViewer : public CDialog
{
// Construction
public:
	CCoCoViewer(CWnd* pParent = NULL);   // standard constructor

	GxGridTab	omSrcGrid, omDstGrid;

// Dialog Data
	//{{AFX_DATA(CCoCoViewer)
	enum { IDD = IDD_COMPARE_DLG };
	CComboBox	m_CmpCoCo;
	CString	m_RefCoCo;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCoCoViewer)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	void UpdateLangDisplay();

	// Generated message map functions
	//{{AFX_MSG(CCoCoViewer)
	afx_msg void OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct);
	virtual BOOL OnInitDialog();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnSelchangeCombo1();
	afx_msg void OnButton1();
	afx_msg void OnSelchangeCombo2();
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};



//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_COCOVIEWER_H__441B9495_91D2_11D3_8FB2_0050DA1CAD13__INCLUDED_)
