//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by TAB.rc
//
#define IDS_TAB                         1
#define IDD_ABOUTBOX_TAB                1
#define IDB_TAB                         1
#define IDI_ABOUTDLL                    1
#define IDS_TAB_PPG                     2
#define IDS_TAB_PPG_CAPTION             200
#define IDD_PROPPAGE_TAB                200
#define IDC_H1                          201
#define IDD_HEADER                      201
#define IDP_SOCKETS_INIT_FAILED         201
#define IDC_H2                          202
#define IDC_ARROWDOWN                   202
#define IDC_H3                          203
#define IDC_COLSIZE                     203
#define IDC_EDIT4                       204
#define IDC_EDIT5                       205
#define IDC_EDIT6                       206
#define IDC_SCROLLBAR1                  208

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        207
#define _APS_NEXT_COMMAND_VALUE         32768
#define _APS_NEXT_CONTROL_VALUE         209
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
