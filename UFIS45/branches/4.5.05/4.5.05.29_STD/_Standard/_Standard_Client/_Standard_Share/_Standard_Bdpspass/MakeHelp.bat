@echo off
REM -- First make map file from Microsoft Visual C++ generated resource.h
echo // MAKEHELP.BAT generated Help Map file.  Used by BDPSPASS.HPJ. >"hlp\BDPSPASS.hm"
echo. >>"hlp\BDPSPASS.hm"
echo // Commands (ID_* and IDM_*) >>"hlp\BDPSPASS.hm"
makehm ID_,HID_,0x10000 IDM_,HIDM_,0x10000 resource.h >>"hlp\BDPSPASS.hm"
echo. >>"hlp\BDPSPASS.hm"
echo // Prompts (IDP_*) >>"hlp\BDPSPASS.hm"
makehm IDP_,HIDP_,0x30000 resource.h >>"hlp\BDPSPASS.hm"
echo. >>"hlp\BDPSPASS.hm"
echo // Resources (IDR_*) >>"hlp\BDPSPASS.hm"
makehm IDR_,HIDR_,0x20000 resource.h >>"hlp\BDPSPASS.hm"
echo. >>"hlp\BDPSPASS.hm"
echo // Dialogs (IDD_*) >>"hlp\BDPSPASS.hm"
makehm IDD_,HIDD_,0x20000 resource.h >>"hlp\BDPSPASS.hm"
echo. >>"hlp\BDPSPASS.hm"
echo // Frame Controls (IDW_*) >>"hlp\BDPSPASS.hm"
makehm IDW_,HIDW_,0x50000 resource.h >>"hlp\BDPSPASS.hm"
REM -- Make help for Project BDPSPASS


echo Building Win32 Help files
start /wait hcw /C /E /M "hlp\BDPSPASS.hpj"
if errorlevel 1 goto :Error
if not exist "hlp\BDPSPASS.hlp" goto :Error
if not exist "hlp\BDPSPASS.cnt" goto :Error
echo.
if exist Debug\nul copy "hlp\BDPSPASS.hlp" Debug
if exist Debug\nul copy "hlp\BDPSPASS.cnt" Debug
if exist Release\nul copy "hlp\BDPSPASS.hlp" Release
if exist Release\nul copy "hlp\BDPSPASS.cnt" Release
echo.
goto :done

:Error
echo hlp\BDPSPASS.hpj(1) : error: Problem encountered creating help file

:done
echo.
