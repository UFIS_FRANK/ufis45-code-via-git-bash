﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ufis.Entities;
using System.ComponentModel;
using System.Windows.Data;
using System.Diagnostics;

namespace Ufis.Flight.Filter
{
    /// <summary>
    /// Represents the class for filtering flight records.
    /// </summary>
    public class FlightFilter: INotifyPropertyChanged
    {
        private readonly IList<FlightDateFilterType> _flightDateFilterTypes;
        private readonly ICollectionView _flightDateFilterTypeCollection;
        private DateTime _absoluteStartDate = DateTime.Today;
        private DateTime _absoluteEndDate = DateTime.Today.AddDays(1).AddSeconds(-1);
        private int _relativeStartRange = -2;
        private int _relativeEndRange = 2;
        private string _registrationNumber;
        private IList<EntCodeName> _flightTypes;

        #region Public properties

        /// <summary>
        /// Gets the collection of available Ufis.Flight.Filter.FlightDateFilterType objects.
        /// </summary>
        public ICollectionView DateFilterTypeCollection
        {
            get
            {
                return _flightDateFilterTypeCollection;
            }
        }

        /// <summary>
        /// Gets or sets the Ufis.Flight.Filter.FlightDateFilterType for this filter.
        /// </summary>
        public FlightDateFilterType? DateFilterType
        {
            get
            {
                return _flightDateFilterTypeCollection.CurrentItem as FlightDateFilterType?;
            }
            set
            {
                _flightDateFilterTypeCollection.MoveCurrentTo(value);
            }
        }

        /// <summary>
        /// Gets or sets the start flight date for this filter.
        /// </summary>
        public DateTime AbsoluteStartDate
        {
            get 
            {
                return _absoluteStartDate;
            }
            set
            {
                if (_absoluteStartDate == value)
                    return;

                _absoluteStartDate = value;
                OnPropertyChanged("AbsoluteStartDate");
            }
        }

        /// <summary>
        /// Gets or sets the end flight date for this filter.
        /// </summary>
        public DateTime AbsoluteEndDate
        {
            get
            {
                return _absoluteEndDate;
            }
            set
            {
                if (_absoluteEndDate == value)
                    return;

                _absoluteEndDate = value;
                OnPropertyChanged("AbsoluteEndDate");
            }
        }

        /// <summary>
        /// Gets or sets the start range of flight date relative to DateTime.Now for this filter.
        /// </summary>
        public int RelativeStartRange
        {
            get
            {
                return _relativeStartRange;
            }
            set
            {
                if (_relativeStartRange == value)
                    return;

                _relativeStartRange = value;
                OnPropertyChanged("RelativeStartRange");
            }
        }

        /// <summary>
        /// Gets or sets the end range of flight date relative to DateTime.Now for this filter.
        /// </summary>
        public int RelativeEndRange
        {
            get
            {
                return _relativeEndRange;
            }
            set
            {
                if (_relativeEndRange == value)
                    return;

                _relativeEndRange = value;
                OnPropertyChanged("RelativeEndRange");
            }
        }

        /// <summary>
        /// Gets or sets the registration number for this filter.
        /// </summary>
        public string RegistrationNumber
        {
            get
            {
                return _registrationNumber;
            }
            set
            {
                if (_registrationNumber == value)
                    return;

                _registrationNumber = value;
                OnPropertyChanged("RegistrationNumber");
            }
        }

        /// <summary>
        /// Gets or sets the list of flight operational types for this filter.
        /// </summary>
        public IList<EntCodeName> FlightTypes
        {
            get
            {
                return _flightTypes;
            }
            set
            {
                if (_flightTypes == value)
                    return;

                _flightTypes = value;
                OnPropertyChanged("FlightTypes");
            }
        }

        #endregion

        #region constructor

        /// <summary>
        /// Initializes the new instance of Ufis.Flight.Filter.FlightFilter object.
        /// </summary>
        public FlightFilter()
        {
            _flightDateFilterTypes = CreateDefaultDateFilterTypeList();
            _flightDateFilterTypeCollection = new CollectionViewSource() { Source = _flightDateFilterTypes }.View;
        }

        private IList<FlightDateFilterType> CreateDefaultDateFilterTypeList()
        {
            IList<FlightDateFilterType> defaultDateFilterTypeList = new List<FlightDateFilterType>();
            foreach (FlightDateFilterType flightDateFilterType in Enum.GetValues(typeof(FlightDateFilterType)))
                defaultDateFilterTypeList.Add(flightDateFilterType);

            return defaultDateFilterTypeList;
        }

        #endregion

        #region DoFilter

        /// <summary>
        /// Filters the flights based on filter condition.
        /// </summary>
        /// <param name="item">The Ufis.Entities.EntDbFlight object.</param>
        /// <returns>true if it meets the condition or false if otherwise.</returns>
        public bool FilterItem(object item)
        {
            bool bReturn = false;

            EntDbFlight flight = item as EntDbFlight;
            if (flight != null)
            {
                //filter by date
                if (flight.ArrivalDepartureId == "A")
                    bReturn = ((flight.StandardTimeOfArrival >= AbsoluteStartDate &&
                                flight.StandardTimeOfArrival <= AbsoluteEndDate) ||
                                (flight.TimeframeOfArrival >= AbsoluteStartDate &&
                                flight.TimeframeOfArrival <= AbsoluteEndDate));
                else
                    bReturn = ((flight.StandardTimeOfDeparture >= AbsoluteStartDate &&
                                flight.StandardTimeOfDeparture <= AbsoluteEndDate) ||
                                (flight.TimeframeOfDeparture >= AbsoluteStartDate &&
                                flight.TimeframeOfDeparture <= AbsoluteEndDate));

                if (bReturn)
                {
                    //filter by registration number
                    if (!string.IsNullOrEmpty(RegistrationNumber))
                        bReturn = (flight.RegistrationNumber == RegistrationNumber);
                }

                if (bReturn)
                {
                    //filter by flight types
                    if (FlightTypes != null)
                        bReturn = FlightTypes.Contains(new EntCodeName() { Code = flight.OperationalType} );
                }
            }

            return bReturn;
        }

        #endregion


        #region Debugging Aides

        /// <summary>
        /// Warns the developer if this object does not have
        /// a public property with the specified name. This 
        /// method does not exist in a Release build.
        /// </summary>
        [Conditional("DEBUG")]
        [DebuggerStepThrough]
        public void VerifyPropertyName(string propertyName)
        {
            // Verify that the property name matches a real,  
            // public, instance property on this object.
            if (TypeDescriptor.GetProperties(this)[propertyName] == null)
            {
                string msg = "Invalid property name: " + propertyName;

                if (this.ThrowOnInvalidPropertyName)
                    throw new Exception(msg);
                else
                    Debug.Fail(msg);
            }
        }

        /// <summary>
        /// Returns whether an exception is thrown, or if a Debug.Fail() is used
        /// when an invalid property name is passed to the VerifyPropertyName method.
        /// The default value is false, but subclasses used by unit tests might 
        /// override this property's getter to return true.
        /// </summary>
        protected virtual bool ThrowOnInvalidPropertyName { get; private set; }

        #endregion // Debugging Aides

        #region INotifyPropertyChanged Members

        /// <summary>
        /// Raised when a property on this object has a new value.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Raises this object's PropertyChanged event.
        /// </summary>
        /// <param name="propertyName">The property that has a new value.</param>
        protected virtual void OnPropertyChanged(string propertyName)
        {
            this.VerifyPropertyName(propertyName);

            PropertyChangedEventHandler handler = this.PropertyChanged;
            if (handler != null)
            {
                var e = new PropertyChangedEventArgs(propertyName);

                handler(this, e);
            }
        }

        #endregion // INotifyPropertyChanged Members

    }
}
