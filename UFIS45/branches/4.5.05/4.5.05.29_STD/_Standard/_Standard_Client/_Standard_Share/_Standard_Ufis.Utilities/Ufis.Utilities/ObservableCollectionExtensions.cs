using System;
using System.Collections.Generic;
using System.Linq;
using System.Collections.ObjectModel;

namespace Ufis.Utilities
{
    public static class ObservableCollectionExtensions
    {
        public static ObservableCollection<T> AddRange<T>(this ObservableCollection<T> source, ObservableCollection<T> items)
        {
            foreach (T item in items)
                source.Add(item);

            return source;
        }
    }
}
