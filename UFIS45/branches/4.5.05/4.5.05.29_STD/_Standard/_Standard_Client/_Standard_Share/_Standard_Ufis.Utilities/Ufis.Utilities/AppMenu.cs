﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media.Imaging;

namespace Ufis.Utilities
{
    /// <summary>
    /// Represents the class for application's menu.
    /// </summary>
    public class AppMenu
    {
        /// <summary>
        /// Gets or sets the menu name.
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Gets or sets the menu caption.
        /// </summary>
        public string Caption { get; set; }
        /// <summary>
        /// Gets or sets the menu description.
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// Gets or sets the menu large icon (32x32).
        /// </summary>
        public BitmapImage ImageLarge { get; set; }
        /// <summary>
        /// Gets or sets the menu large icon (16x16).
        /// </summary>
        public BitmapImage ImageSmall { get; set; }

        public override string ToString()
        {
            if (!string.IsNullOrEmpty(Caption))
                return Caption;

            if (!string.IsNullOrEmpty(Name))
                return Name;
            
            return base.ToString();
        }
    }
}
