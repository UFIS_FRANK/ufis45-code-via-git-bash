﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using DevExpress.Xpf.Bars;
using Ufis.MVVM.Helpers;

namespace Ufis.Utilities
{
    public class DxPopupHelper
    {
        public static void ClosePopup(DependencyObject depObject)
        {
            PopupControlContainer pcc =
                depObject.TryFindParent<PopupControlContainer>();
            if (depObject != null)
                pcc.ClosePopup();
        }
    }
}
