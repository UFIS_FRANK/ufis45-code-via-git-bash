#ifndef _BCPROXYCP_H_
#define _BCPROXYCP_H_
#include "ATLCPImplMT.h"
#include <BcProxy_i.h>

template <class T>
class CProxy_IBcPrxyEvents : public IConnectionPointImplMT<T, &DIID__IBcPrxyEvents, CComDynamicUnkArray>
{
	//Warning this class may be recreated by the wizard.
public:
	HRESULT Fire_OnBcReceive(BSTR ReqId, BSTR Dest1, BSTR Dest2, BSTR Cmd, BSTR Object, BSTR Seq, BSTR Tws, BSTR Twe, BSTR Selection, BSTR Fields, BSTR Data, BSTR BcNum)
	{
		CComVariant varResult;
		T* pT = static_cast<T*>(this);
		int nConnectionIndex;
		CComVariant* pvars = new CComVariant[12];
		int nConnections = m_vec.GetSize();
		
		for (nConnectionIndex = 0; nConnectionIndex < nConnections; nConnectionIndex++)
		{
//			pT->Lock();
//			CComPtr<IUnknown> sp = m_vec.GetAt(nConnectionIndex);
//			pT->Unlock();

			CComPtr<IUnknown> sp;
			sp.Attach(GetInterfaceAt(nConnectionIndex));

			IDispatch* pDispatch = reinterpret_cast<IDispatch*>(sp.p);
			if (pDispatch != NULL)
			{
				VariantClear(&varResult);
				pvars[11] = ReqId;
				pvars[10] = Dest1;
				pvars[9] = Dest2;
				pvars[8] = Cmd;
				pvars[7] = Object;
				pvars[6] = Seq;
				pvars[5] = Tws;
				pvars[4] = Twe;
				pvars[3] = Selection;
				pvars[2] = Fields;
				pvars[1] = Data;
				pvars[0] = BcNum;
				DISPPARAMS disp = { pvars, NULL, 12, 0 };
				pDispatch->Invoke(0x1, IID_NULL, LOCALE_USER_DEFAULT, DISPATCH_METHOD, &disp, &varResult, NULL, NULL);
			}
		}
		delete[] pvars;
		return varResult.scode;
	}
};

#endif