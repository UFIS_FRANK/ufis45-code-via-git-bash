// BcProxyDlg.h : header file
//

#if !defined(AFX_BCPROXYDLG_H__68E25FE6_6790_11D4_903B_00010204AA51__INCLUDED_)
#define AFX_BCPROXYDLG_H__68E25FE6_6790_11D4_903B_00010204AA51__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

/////////////////////////////////////////////////////////////////////////////
// CBcProxyDlg dialog
class CBcPrxy;			//forward declaration
class CBcProxyDlg : public CDialog
{
// Construction
public:
	CBcProxyDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	//{{AFX_DATA(CBcProxyDlg)
	enum { IDD = IDD_BCPROXY_DIALOG };
	CEdit	m_EditCommand;
	CEdit	m_EditTable;
	CListBox	m_BcList;
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CBcProxyDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL
	afx_msg LONG OnBcAdd(UINT wParam, LONG lParam);
// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(CBcProxyDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnDestroy();
	afx_msg void OnButtonFire();
	afx_msg void OnButtonTestfire();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	bool CheckFilter(CBcPrxy* prpClient, CString opTable, CString opFields, CString opData);
	CString	omWksName;
	bool	bmCheckApplication;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_BCPROXYDLG_H__68E25FE6_6790_11D4_903B_00010204AA51__INCLUDED_)
