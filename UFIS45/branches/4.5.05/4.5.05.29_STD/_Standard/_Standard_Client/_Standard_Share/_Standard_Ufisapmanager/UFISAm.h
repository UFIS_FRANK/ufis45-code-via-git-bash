/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * 
 *	UFISAppMang:	UFIS Client Application Manager Project
 *
 *	Proxy Communicator for Client Applications
 *	
 *	ABB Airport Technologies GmbH 2000
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 *		Author			Date			Comment
 *	------------------------------------------------------------------
 *
 *		cla				02/09/2000		Initial version
 *		cla				11/09/2000		isTlxPending added to avoid 
 *										multiple startup
 *		cla				18/10/2000		Support for FipsCUTE
 *		cla				24/10/2000		StartFipsCute corrected and holdData
 *										changed to avoid multiple detach ops
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
// UFISAm.h : Deklaration von CUFISAm

#ifndef __UFISAM_H_
#define __UFISAM_H_

#include <resource.h>       // Hauptsymbole
#include <UFISAppMngCP.h>
#include <ClntTypes.h>
#include <list>
#include <iterator>
#include <ClntCon.h>

/////////////////////////////////////////////////////////////////////////////
// CUFISAm

/*
 *  D E F I N E S
 */

#define GEN_PATH_LEN 512

/*
 * List container class
 */

typedef std::list<CClntCon> ClntList;
/*
 * Message filter example code, I will keep it
 * for later studies
 *

class MyMessageFilter : public IMessageFilter
{
	int omCount;
public:

	MyMessageFilter() {}
	STDMETHODIMP QueryInterface(REFIID riid,void **ppv)
	{
		if(ppv==NULL) {return E_INVALIDARG;}
		if(riid==IID_IUnknown || riid == IID_IMessageFilter)
		{
			*ppv = static_cast<IMessageFilter*>(this);
		}
		else
		{
			*ppv=NULL;
			return E_NOINTERFACE;
		}
		reinterpret_cast<IUnknown *>(*ppv)->AddRef();
		return S_OK;
	}
	STDMETHODIMP_(ULONG) AddRef(void)
	{ return 1;}
	STDMETHODIMP_(ULONG) Release(void)
	{ return 1;}
    STDMETHODIMP_(DWORD) 	HandleInComingCall(DWORD dwCallType, 
							HTASK htaskCaller, 							
							DWORD dwTickCount, 
							LPINTERFACEINFO lpInterfaceInfo)
	{
		//::MessageBox(NULL,"Hallo","Hallo",MB_OK);
		
		return SERVERCALL_ISHANDLED;
	}
	    STDMETHODIMP_(DWORD) RetryRejectedCall(HTASK htaskCallee, 
							DWORD dwTickCount, DWORD dwRejectType)	
		{ return -1; }
    STDMETHODIMP_(DWORD) MessagePending(HTASK htaskCallee, 
						DWORD dwTickCount,DWORD dwPendingType)
	{ return PENDINGMSG_WAITNOPROCESS; }    
};
*/
class ATL_NO_VTABLE CUFISAm : 
	public CComObjectRootEx<CComSingleThreadModel>,
	public CComCoClass<CUFISAm, &CLSID_UFISAm>,
	public ISupportErrorInfo,
	public IConnectionPointContainerImpl<CUFISAm>,
	public IDispatchImpl<IUFISAm, &IID_IUFISAm, &LIBID_UFISAPPMNGLib>,
	public CProxy_IUFISAmEvents< CUFISAm >
{
public:
	CUFISAm()
	{
		m_pUnkMarshaler = NULL;
		isTlxStarted = false;
		isTlxPending = false;
		isfCUTEStarted = false;
		isfCUTEPending = false;
		ReadProfileInfo();
	}

DECLARE_REGISTRY_RESOURCEID(IDR_UFISAM)
DECLARE_GET_CONTROLLING_UNKNOWN()

DECLARE_PROTECT_FINAL_CONSTRUCT()

/*
 * We are going to have one instance only
 * up to now shall be removed later
 */

DECLARE_CLASSFACTORY_SINGLETON(CUFISAm)
BEGIN_COM_MAP(CUFISAm)
	COM_INTERFACE_ENTRY(IUFISAm)
	COM_INTERFACE_ENTRY(IDispatch)
	COM_INTERFACE_ENTRY(ISupportErrorInfo)
	COM_INTERFACE_ENTRY(IConnectionPointContainer)
	COM_INTERFACE_ENTRY_AGGREGATE(IID_IMarshal, m_pUnkMarshaler.p)
	COM_INTERFACE_ENTRY_IMPL(IConnectionPointContainer)
END_COM_MAP()
BEGIN_CONNECTION_POINT_MAP(CUFISAm)
CONNECTION_POINT_ENTRY(DIID__IUFISAmEvents)
END_CONNECTION_POINT_MAP()


	HRESULT FinalConstruct()
	{
		/*
		 * IMessageFilter stuff:
		 *
		pomMsgFilter = new MyMessageFilter;

		IMessageFilter* polMf;
		HRESULT hr = CoRegisterMessageFilter(pomMsgFilter,&polMf);
		if(hr == S_FALSE)
			::MessageBox(NULL,"Bla bla","Bla bla",MB_OK);
        */
		return CoCreateFreeThreadedMarshaler(
			GetControllingUnknown(), &m_pUnkMarshaler.p);
	}

	void FinalRelease()
	{
		m_pUnkMarshaler.Release();
	}

	CComPtr<IUnknown> m_pUnkMarshaler;

// ISupportsErrorInfo
	STDMETHOD(InterfaceSupportsErrorInfo)(REFIID riid);

// IUFISAm
public:
	STDMETHOD(get_BuildDate)(/*[out, retval]*/ BSTR *pVal);
	STDMETHOD(get_Version)(/*[out, retval]*/ BSTR *pVal);
	STDMETHOD(DetachApp)(/*[in]*/ long AppID);
	STDMETHOD(AssignAppTag)(/*[in]*/ long AppID);
	STDMETHOD(TransferData)(/*[in]*/ long Dest,/*[in]*/ long Orig,/*[in]*/ BSTR Data);

	/*
	 * Trace of the Advise Call
	 */
	STDMETHOD(Advise)(IUnknown* pUnkSink,DWORD* pdwCookie)
	{
		//::MessageBox(NULL,"In Advise","",MB_OK);
		/*
		 * Store the pointer to the Sink object
		 * for later use
		 */
		m_pActUnk = pUnkSink;
		CProxy_IUFISAmEvents< CUFISAm >::Advise(pUnkSink,pdwCookie);
		m_cookie = *pdwCookie;
		return S_OK;
	}

	STDMETHOD(Unadvise)(DWORD pdwCookie);

	ClntList m_clntCon;
	IUnknown* m_pActUnk;
	DWORD m_cookie;
	
	bool isTlxStarted;
	bool isTlxPending;
	bool isfCUTEStarted;
	bool isfCUTEPending;

	CComBSTR m_holdTlxData;
	CComBSTR m_holdfCUTEData;

	TCHAR m_tlxLaunchPath[GEN_PATH_LEN];
	TCHAR m_tlxLaunchStr[GEN_PATH_LEN];

	TCHAR m_fCUTELaunchPath[GEN_PATH_LEN];
	TCHAR m_fCUTELaunchStr[GEN_PATH_LEN];
//	MyMessageFilter *pomMsgFilter;
	void ReadProfileInfo();
	void StartTlxPool();
	void StartFipsCute();
};

#endif //__UFISAM_H_
