VERSION 5.00
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "TAB.ocx"
Begin VB.Form frmDebug 
   Caption         =   "Form1"
   ClientHeight    =   6225
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   8430
   LinkTopic       =   "Form1"
   ScaleHeight     =   6225
   ScaleWidth      =   8430
   StartUpPosition =   3  'Windows Default
   Visible         =   0   'False
   Begin TABLib.TAB tabData 
      Height          =   2175
      Index           =   0
      Left            =   2835
      TabIndex        =   0
      Tag             =   "SYSTAB"
      Top             =   510
      Width           =   5310
      _Version        =   65536
      _ExtentX        =   9366
      _ExtentY        =   3836
      _StockProps     =   0
   End
   Begin TABLib.TAB tabData 
      Height          =   2325
      Index           =   1
      Left            =   2970
      TabIndex        =   1
      Tag             =   "TABTAB"
      Top             =   3645
      Width           =   5205
      _Version        =   65536
      _ExtentX        =   9181
      _ExtentY        =   4101
      _StockProps     =   0
   End
   Begin TABLib.TAB TabHistory 
      Height          =   735
      Index           =   2
      Left            =   150
      TabIndex        =   2
      Top             =   2385
      Width           =   1680
      _Version        =   65536
      _ExtentX        =   2963
      _ExtentY        =   1296
      _StockProps     =   0
   End
   Begin TABLib.TAB TabHistory 
      Height          =   735
      Index           =   1
      Left            =   105
      TabIndex        =   3
      Top             =   1350
      Width           =   1680
      _Version        =   65536
      _ExtentX        =   2963
      _ExtentY        =   1296
      _StockProps     =   0
   End
   Begin TABLib.TAB TabHistory 
      Height          =   735
      Index           =   0
      Left            =   105
      TabIndex        =   4
      Top             =   405
      Width           =   1680
      _Version        =   65536
      _ExtentX        =   2963
      _ExtentY        =   1296
      _StockProps     =   0
   End
   Begin VB.Label lblInfo 
      Caption         =   "SYSTAB"
      Height          =   255
      Index           =   0
      Left            =   2925
      TabIndex        =   7
      Top             =   180
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.Label lblInfo 
      Caption         =   "TABTAB"
      Height          =   255
      Index           =   1
      Left            =   2970
      TabIndex        =   6
      Top             =   3240
      Visible         =   0   'False
      Width           =   735
   End
   Begin VB.Label Label7 
      Caption         =   "HistoryTabs:"
      Height          =   285
      Left            =   60
      TabIndex        =   5
      Top             =   0
      Visible         =   0   'False
      Width           =   1005
   End
End
Attribute VB_Name = "frmDebug"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

