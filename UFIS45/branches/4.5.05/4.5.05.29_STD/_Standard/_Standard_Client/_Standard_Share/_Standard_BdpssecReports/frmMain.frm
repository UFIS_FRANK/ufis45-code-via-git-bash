VERSION 5.00
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "TAB.ocx"
Object = "{A2F31E92-C74F-11D3-A251-00500437F607}#1.0#0"; "UfisCom.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{7BDE1363-DF9A-4D96-A2A0-4E85E0191F0F}#1.0#0"; "AatLogin.ocx"
Object = "{8E27C92E-1264-101C-8A2F-040224009C02}#7.0#0"; "MSCAL.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "Comdlg32.ocx"
Begin VB.Form frmMain 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "BDPSSEC Reports"
   ClientHeight    =   12435
   ClientLeft      =   150
   ClientTop       =   435
   ClientWidth     =   14655
   Icon            =   "frmMain.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   ScaleHeight     =   12435
   ScaleWidth      =   14655
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdDEBUG 
      Cancel          =   -1  'True
      Caption         =   "!"
      Height          =   195
      Left            =   360
      TabIndex        =   3
      ToolTipText     =   "Debug Information"
      Top             =   10920
      Width           =   135
   End
   Begin VB.CommandButton cmdExit 
      Caption         =   "&Exit"
      Height          =   375
      Index           =   2
      Left            =   11985
      TabIndex        =   2
      Top             =   11160
      Width           =   1095
   End
   Begin VB.CommandButton cmdTest2 
      Caption         =   "Command1"
      Height          =   495
      Left            =   1560
      TabIndex        =   1
      Top             =   11160
      Visible         =   0   'False
      Width           =   1215
   End
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   660
      Left            =   0
      TabIndex        =   0
      Top             =   11775
      Width           =   14655
      _ExtentX        =   25850
      _ExtentY        =   1164
      SimpleText      =   "Ready."
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   3
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   20664
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   6
            TextSave        =   "22-Nov-07"
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   5
            Alignment       =   2
            TextSave        =   "10:20 AM"
         EndProperty
      EndProperty
   End
   Begin UFISCOMLib.UfisCom UfisCom1 
      Left            =   0
      Top             =   0
      _Version        =   65536
      _ExtentX        =   1111
      _ExtentY        =   1164
      _StockProps     =   0
   End
   Begin TabDlg.SSTab SSTAB1 
      Height          =   12495
      Left            =   120
      TabIndex        =   4
      Tag             =   "{=TABLE=}ALTTAB{=FIELDS=}ALC2,ALC3,ALFN"
      Top             =   120
      Width           =   14655
      _ExtentX        =   25850
      _ExtentY        =   22040
      _Version        =   393216
      Tabs            =   8
      Tab             =   7
      TabsPerRow      =   4
      TabHeight       =   520
      BackColor       =   0
      TabCaption(0)   =   "Configuration"
      TabPicture(0)   =   "frmMain.frx":030A
      Tab(0).ControlEnabled=   0   'False
      Tab(0).Control(0)=   "AATLoginControl1"
      Tab(0).Control(1)=   "Frame1"
      Tab(0).Control(2)=   "Frame2"
      Tab(0).Control(3)=   "cmdExit(0)"
      Tab(0).Control(4)=   "cmdSave"
      Tab(0).ControlCount=   5
      TabCaption(1)   =   "Manual Input"
      TabPicture(1)   =   "frmMain.frx":0326
      Tab(1).ControlEnabled=   0   'False
      Tab(1).ControlCount=   0
      TabCaption(2)   =   "Dutyroster Transactions"
      TabPicture(2)   =   "frmMain.frx":0342
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "cmdFields(3)"
      Tab(2).Control(1)=   "Frame6"
      Tab(2).Control(2)=   "Frame7"
      Tab(2).Control(3)=   "Frame11"
      Tab(2).Control(4)=   "Frame10"
      Tab(2).Control(5)=   "Frame9"
      Tab(2).Control(6)=   "Frame8"
      Tab(2).Control(7)=   "cmdTest"
      Tab(2).Control(8)=   "Frame5"
      Tab(2).Control(9)=   "tab_SYSDRR"
      Tab(2).ControlCount=   10
      TabCaption(3)   =   "Job Transactions Sven"
      TabPicture(3)   =   "frmMain.frx":035E
      Tab(3).ControlEnabled=   0   'False
      Tab(3).Control(0)=   "tab_AFTTABall"
      Tab(3).Control(1)=   "tab_JTYTAB"
      Tab(3).Control(2)=   "frmAllJobs"
      Tab(3).Control(3)=   "Frame12"
      Tab(3).Control(4)=   "Frame14"
      Tab(3).Control(5)=   "frmFlights"
      Tab(3).Control(6)=   "frmJobs"
      Tab(3).Control(7)=   "Frame17"
      Tab(3).Control(8)=   "Frame20"
      Tab(3).ControlCount=   9
      TabCaption(4)   =   "User Transactions Sven"
      TabPicture(4)   =   "frmMain.frx":037A
      Tab(4).ControlEnabled=   0   'False
      Tab(4).Control(0)=   "Frame22"
      Tab(4).Control(1)=   "Frame13"
      Tab(4).Control(2)=   "Frame3"
      Tab(4).Control(3)=   "Frame4"
      Tab(4).Control(4)=   "tab_STFCOMPLETE"
      Tab(4).Control(5)=   "tab_R02TABUSER"
      Tab(4).Control(6)=   "tab_R01TABUSER"
      Tab(4).ControlCount=   7
      TabCaption(5)   =   "User Transactions"
      TabPicture(5)   =   "frmMain.frx":0396
      Tab(5).ControlEnabled=   0   'False
      Tab(5).Control(0)=   "Frame15"
      Tab(5).Control(1)=   "Frame16"
      Tab(5).Control(2)=   "Frame18"
      Tab(5).ControlCount=   3
      TabCaption(6)   =   "Job Transactions"
      TabPicture(6)   =   "frmMain.frx":03B2
      Tab(6).ControlEnabled=   0   'False
      Tab(6).Control(0)=   "resetAll"
      Tab(6).Control(1)=   "Frame19"
      Tab(6).Control(2)=   "Frame21"
      Tab(6).Control(3)=   "Frame24"
      Tab(6).Control(4)=   "Frame25"
      Tab(6).Control(5)=   "Frame26"
      Tab(6).Control(6)=   "loadJobs"
      Tab(6).Control(7)=   "Frame27"
      Tab(6).Control(8)=   "Frame28"
      Tab(6).ControlCount=   9
      TabCaption(7)   =   "Org Units - BDPSSEC"
      TabPicture(7)   =   "frmMain.frx":03CE
      Tab(7).ControlEnabled=   -1  'True
      Tab(7).Control(0)=   "tab_SECTABAPP"
      Tab(7).Control(0).Enabled=   0   'False
      Tab(7).Control(1)=   "tab_FKTTAB"
      Tab(7).Control(1).Enabled=   0   'False
      Tab(7).Control(2)=   "tab_SECTABPRF"
      Tab(7).Control(2).Enabled=   0   'False
      Tab(7).Control(3)=   "tab_PRVTAB"
      Tab(7).Control(3).Enabled=   0   'False
      Tab(7).Control(4)=   "Frame23"
      Tab(7).Control(4).Enabled=   0   'False
      Tab(7).Control(5)=   "Frame29"
      Tab(7).Control(5).Enabled=   0   'False
      Tab(7).Control(6)=   "cmdExpStf"
      Tab(7).Control(6).Enabled=   0   'False
      Tab(7).Control(7)=   "dlgSave"
      Tab(7).Control(7).Enabled=   0   'False
      Tab(7).Control(8)=   "cmdExpProfile"
      Tab(7).Control(8).Enabled=   0   'False
      Tab(7).Control(9)=   "dlgSave1"
      Tab(7).Control(9).Enabled=   0   'False
      Tab(7).Control(10)=   "cmdExpProfile1"
      Tab(7).Control(10).Enabled=   0   'False
      Tab(7).Control(11)=   "dlgSave2"
      Tab(7).Control(11).Enabled=   0   'False
      Tab(7).ControlCount=   12
      Begin MSComDlg.CommonDialog dlgSave2 
         Left            =   7320
         Top             =   9360
         _ExtentX        =   847
         _ExtentY        =   847
         _Version        =   393216
      End
      Begin VB.CommandButton cmdExpProfile1 
         Caption         =   "Export Profiles and Groups"
         Height          =   735
         Left            =   4320
         TabIndex        =   244
         Top             =   9240
         Width           =   2775
      End
      Begin MSComDlg.CommonDialog dlgSave1 
         Left            =   7320
         Top             =   8400
         _ExtentX        =   847
         _ExtentY        =   847
         _Version        =   393216
      End
      Begin VB.CommandButton cmdExpProfile 
         Caption         =   "Export Profiles and Groups with Functions"
         Height          =   735
         Left            =   4320
         TabIndex        =   239
         Top             =   8280
         Width           =   2775
      End
      Begin MSComDlg.CommonDialog dlgSave 
         Left            =   7320
         Top             =   7440
         _ExtentX        =   847
         _ExtentY        =   847
         _Version        =   393216
      End
      Begin VB.CommandButton cmdExpStf 
         Caption         =   "Export To Excel"
         Height          =   735
         Left            =   4320
         TabIndex        =   234
         Top             =   7320
         Width           =   2775
      End
      Begin VB.Frame Frame29 
         Caption         =   "Staff"
         Height          =   5415
         Left            =   4920
         TabIndex        =   231
         Top             =   1080
         Width           =   8655
         Begin TABLib.TAB tab_STFLOG 
            Height          =   3975
            Left            =   240
            TabIndex        =   236
            Top             =   360
            Width           =   8175
            _Version        =   65536
            _ExtentX        =   14420
            _ExtentY        =   7011
            _StockProps     =   64
         End
         Begin TABLib.TAB tab_SECSTFTAB 
            Height          =   3975
            Left            =   240
            TabIndex        =   232
            Tag             =   "{=TABLE=}STFTAB{=FIELDS=}PENO,URNO,LANM,FINM"
            Top             =   360
            Width           =   8175
            _Version        =   65536
            _ExtentX        =   14420
            _ExtentY        =   7011
            _StockProps     =   64
         End
         Begin TABLib.TAB tab_SECTAB 
            Height          =   2895
            Left            =   4560
            TabIndex        =   235
            Tag             =   "{=TABLE=}SECTAB{=FIELDS=}URNO,USID"
            Top             =   720
            Width           =   2535
            _Version        =   65536
            _ExtentX        =   4471
            _ExtentY        =   5106
            _StockProps     =   64
         End
         Begin TABLib.TAB tab_SECTABGRP 
            Height          =   1095
            Left            =   480
            TabIndex        =   238
            Tag             =   "{=TABLE=}SECTAB{=FIELDS=}URNO,USID,NAME,TYPE"
            Top             =   2880
            Width           =   855
            _Version        =   65536
            _ExtentX        =   1508
            _ExtentY        =   1931
            _StockProps     =   64
         End
         Begin TABLib.TAB tab_GRPTAB 
            Height          =   1215
            Left            =   1560
            TabIndex        =   237
            Tag             =   "{=TABLE=}GRPTAB{=FIELDS=}FSEC,FREL"
            Top             =   2880
            Width           =   1335
            _Version        =   65536
            _ExtentX        =   2355
            _ExtentY        =   2143
            _StockProps     =   64
         End
         Begin VB.Label lblSecStfNo 
            Caption         =   "No Org Unit Selected."
            Height          =   495
            Left            =   360
            TabIndex        =   233
            Top             =   4680
            Width           =   4215
         End
      End
      Begin VB.Frame Frame23 
         Caption         =   "Org Units"
         Height          =   5415
         Left            =   480
         TabIndex        =   228
         Top             =   1080
         Width           =   4095
         Begin TABLib.TAB tab_SECORGTAB 
            Height          =   3975
            Left            =   360
            TabIndex        =   229
            Tag             =   "{=TABLE=}ORGTAB{=FIELDS=}DPT1"
            Top             =   360
            Width           =   3375
            _Version        =   65536
            _ExtentX        =   5953
            _ExtentY        =   7011
            _StockProps     =   64
         End
         Begin VB.Label lblSecOrgUnits 
            Caption         =   "No Org Units Available."
            Height          =   495
            Left            =   360
            TabIndex        =   230
            Top             =   4680
            Width           =   3375
         End
      End
      Begin VB.CommandButton resetAll 
         Caption         =   "Reset All"
         Height          =   495
         Left            =   -70560
         TabIndex        =   227
         Top             =   6480
         Width           =   1335
      End
      Begin VB.Frame Frame22 
         Caption         =   "Userlogins with transactions"
         Height          =   3015
         Left            =   -68280
         TabIndex        =   216
         Top             =   720
         Width           =   6135
         Begin VB.TextBox txtSearchUser 
            Enabled         =   0   'False
            Height          =   285
            Left            =   4560
            TabIndex        =   220
            Top             =   2160
            Width           =   1335
         End
         Begin VB.CommandButton cmdSearchUser 
            Caption         =   "&Search User!"
            Enabled         =   0   'False
            Height          =   375
            Left            =   4560
            TabIndex        =   219
            Top             =   2520
            Width           =   1335
         End
         Begin VB.CommandButton Command1 
            Caption         =   "Filter shift transactions"
            Enabled         =   0   'False
            Height          =   375
            Left            =   120
            TabIndex        =   218
            Top             =   2160
            Width           =   1695
         End
         Begin VB.CommandButton Command2 
            Caption         =   "Filter job transactions"
            Enabled         =   0   'False
            Height          =   375
            Left            =   120
            TabIndex        =   217
            Top             =   2520
            Width           =   1695
         End
         Begin TABLib.TAB tab_ALLUSERS 
            Height          =   1755
            Left            =   120
            TabIndex        =   221
            Top             =   240
            Width           =   5805
            _Version        =   65536
            _ExtentX        =   10239
            _ExtentY        =   3096
            _StockProps     =   64
         End
         Begin VB.Label Label34 
            Caption         =   "&Staff Number"
            Height          =   255
            Left            =   3360
            TabIndex        =   222
            Top             =   2160
            Width           =   1095
         End
      End
      Begin VB.Frame Frame13 
         Caption         =   "Login Date"
         Height          =   3015
         Left            =   -74640
         TabIndex        =   212
         Top             =   720
         Width           =   6135
         Begin VB.CommandButton cmdAllUsers 
            Caption         =   "Load all transactions"
            Height          =   375
            Left            =   3720
            TabIndex        =   214
            Top             =   2400
            Width           =   1935
         End
         Begin VB.CommandButton cmdResetUserTrans 
            Caption         =   "&Reset"
            Height          =   375
            Left            =   3720
            TabIndex        =   213
            Top             =   1920
            Width           =   1935
         End
         Begin MSACAL.Calendar calUser 
            Height          =   2655
            Left            =   120
            TabIndex        =   215
            Top             =   240
            Width           =   2895
            _Version        =   524288
            _ExtentX        =   5106
            _ExtentY        =   4683
            _StockProps     =   1
            BackColor       =   -2147483633
            Year            =   2005
            Month           =   8
            Day             =   26
            DayLength       =   1
            MonthLength     =   2
            DayFontColor    =   0
            FirstDay        =   1
            GridCellEffect  =   1
            GridFontColor   =   10485760
            GridLinesColor  =   -2147483632
            ShowDateSelectors=   -1  'True
            ShowDays        =   -1  'True
            ShowHorizontalGrid=   -1  'True
            ShowTitle       =   0   'False
            ShowVerticalGrid=   -1  'True
            TitleFontColor  =   10485760
            ValueIsNull     =   0   'False
            BeginProperty DayFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty GridFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty TitleFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
      End
      Begin VB.CommandButton cmdFields 
         Caption         =   "&Field Description"
         Height          =   375
         Index           =   3
         Left            =   -64680
         TabIndex        =   211
         Top             =   10680
         Width           =   1455
      End
      Begin VB.Frame Frame20 
         Caption         =   "Template"
         Height          =   1935
         Left            =   -67560
         TabIndex        =   209
         Top             =   10560
         Visible         =   0   'False
         Width           =   2535
         Begin VB.ComboBox cmbTemplate 
            Enabled         =   0   'False
            Height          =   315
            ItemData        =   "frmMain.frx":03EA
            Left            =   120
            List            =   "frmMain.frx":03EC
            Style           =   2  'Dropdown List
            TabIndex        =   210
            Top             =   360
            Width           =   2295
         End
      End
      Begin VB.Frame Frame17 
         Caption         =   "Transactions for the selected record"
         Height          =   3015
         Left            =   -74760
         TabIndex        =   206
         Top             =   7620
         Width           =   12855
         Begin TABLib.TAB tab_R02TAB 
            Height          =   1035
            Left            =   0
            TabIndex        =   207
            Tag             =   "{=TABLE=}R02TAB{=FIELDS=}ORNO,FLST,FVAL,USEC,TIME,SFKT,URNO"
            Top             =   1920
            Visible         =   0   'False
            Width           =   5355
            _Version        =   65536
            _ExtentX        =   9446
            _ExtentY        =   1826
            _StockProps     =   64
         End
         Begin TABLib.TAB tab_LOGJOB 
            Height          =   2595
            Left            =   120
            TabIndex        =   208
            Top             =   240
            Width           =   12645
            _Version        =   65536
            _ExtentX        =   22304
            _ExtentY        =   4577
            _StockProps     =   64
         End
      End
      Begin VB.Frame frmJobs 
         Caption         =   "Flight Jobs"
         Height          =   3675
         Left            =   -74760
         TabIndex        =   196
         Top             =   3840
         Width           =   7335
         Begin VB.TextBox lblJobUrno 
            BackColor       =   &H00C0C0C0&
            BorderStyle     =   0  'None
            Height          =   255
            Left            =   1560
            TabIndex        =   202
            Top             =   2760
            Width           =   1335
         End
         Begin VB.TextBox txtStfName 
            BackColor       =   &H00C0FFFF&
            BorderStyle     =   0  'None
            Height          =   255
            Left            =   1560
            TabIndex        =   201
            Top             =   2400
            Width           =   4215
         End
         Begin VB.TextBox txtStfPeno 
            BackColor       =   &H00C0FFFF&
            BorderStyle     =   0  'None
            Height          =   255
            Left            =   5880
            TabIndex        =   200
            Top             =   2400
            Width           =   1095
         End
         Begin VB.CommandButton cmdAllJobs 
            Caption         =   "Show all Jobs for staff"
            Height          =   375
            Left            =   5520
            TabIndex        =   199
            Top             =   3120
            Width           =   1695
         End
         Begin VB.CommandButton cmdShowRoster 
            Caption         =   "Show duty roster"
            Height          =   375
            Left            =   3720
            TabIndex        =   198
            Top             =   3120
            Width           =   1695
         End
         Begin VB.CommandButton cmdTransactionsJob 
            Caption         =   "View Transactions"
            Height          =   375
            Left            =   1920
            TabIndex        =   197
            Top             =   3120
            Width           =   1695
         End
         Begin TABLib.TAB tab_JOBTAB 
            Height          =   1995
            Left            =   120
            TabIndex        =   203
            Tag             =   "{=TABLE=}JOBTAB{=FIELDS=}URNO,ACFR,ACTO,PLFR,PLTO,UAFT,UJTY,UDSR,USTF"
            Top             =   240
            Width           =   7005
            _Version        =   65536
            _ExtentX        =   12356
            _ExtentY        =   3519
            _StockProps     =   64
            FontSize        =   12
         End
         Begin VB.Label Label33 
            Caption         =   "Urno (internal)"
            Height          =   255
            Left            =   480
            TabIndex        =   205
            Top             =   2760
            Width           =   1095
         End
         Begin VB.Label Label39 
            Caption         =   "Staff (name,peno)"
            Height          =   255
            Left            =   240
            TabIndex        =   204
            Top             =   2400
            Width           =   1335
         End
      End
      Begin VB.Frame frmFlights 
         Caption         =   "Flights"
         Height          =   3675
         Left            =   -67320
         TabIndex        =   183
         Top             =   720
         Width           =   5535
         Begin VB.TextBox txtSTO 
            BackColor       =   &H00C0FFFF&
            BorderStyle     =   0  'None
            Height          =   255
            Left            =   3600
            TabIndex        =   188
            Top             =   2520
            Width           =   1695
         End
         Begin VB.TextBox txtBEST 
            BackColor       =   &H00C0FFFF&
            BorderStyle     =   0  'None
            Height          =   255
            Left            =   3600
            TabIndex        =   187
            Top             =   2880
            Width           =   1695
         End
         Begin VB.TextBox txtORGDES 
            Appearance      =   0  'Flat
            BackColor       =   &H00C0FFFF&
            BorderStyle     =   0  'None
            Height          =   255
            Left            =   1200
            TabIndex        =   186
            Top             =   3240
            Width           =   1095
         End
         Begin VB.TextBox txtFlight 
            BackColor       =   &H00C0FFFF&
            BorderStyle     =   0  'None
            Height          =   255
            Left            =   1200
            TabIndex        =   185
            Top             =   2520
            Width           =   1095
         End
         Begin VB.TextBox txtUrno 
            BackColor       =   &H00C0C0C0&
            BorderStyle     =   0  'None
            Height          =   255
            Left            =   3600
            TabIndex        =   184
            Top             =   3240
            Width           =   1695
         End
         Begin TABLib.TAB tab_AFTTAB 
            Height          =   2115
            Left            =   120
            TabIndex        =   189
            Tag             =   "{=TABLE=}AFTTAB{=FIELDS=}URNO,FLNO,ADID,STOA,STOD,TIFD,TIFA,ORG3,DES3,ALC2"
            Top             =   240
            Width           =   5325
            _Version        =   65536
            _ExtentX        =   9393
            _ExtentY        =   3731
            _StockProps     =   64
         End
         Begin VB.Label Label31 
            Caption         =   "Flight Number"
            Height          =   255
            Left            =   120
            TabIndex        =   195
            Top             =   2520
            Width           =   1095
         End
         Begin VB.Label Label14 
            Caption         =   "Scheduled"
            Height          =   255
            Left            =   2520
            TabIndex        =   194
            Top             =   2520
            Width           =   1095
         End
         Begin VB.Label Label15 
            Caption         =   "Best Time"
            Height          =   255
            Left            =   2520
            TabIndex        =   193
            Top             =   2880
            Width           =   1095
         End
         Begin VB.Label lblAdid 
            BackColor       =   &H00C0FFFF&
            Height          =   255
            Left            =   1200
            TabIndex        =   192
            Top             =   2880
            Width           =   1095
         End
         Begin VB.Label Label32 
            Caption         =   "Urno (internal)"
            Height          =   255
            Left            =   2520
            TabIndex        =   191
            Top             =   3240
            Width           =   1095
         End
         Begin VB.Label Label36 
            BackStyle       =   0  'Transparent
            Caption         =   "(local times)"
            Height          =   255
            Left            =   120
            TabIndex        =   190
            Top             =   2760
            Width           =   1095
         End
      End
      Begin VB.Frame Frame14 
         Caption         =   "Filter"
         Height          =   3015
         Left            =   -71520
         TabIndex        =   172
         Top             =   720
         Width           =   4095
         Begin VB.CommandButton cmResetFLIGHT 
            Caption         =   "&Reset"
            Height          =   375
            Left            =   1440
            TabIndex        =   179
            Top             =   2520
            Width           =   1095
         End
         Begin VB.TextBox findALC2 
            BackColor       =   &H00C0FFC0&
            Height          =   285
            Left            =   1320
            TabIndex        =   178
            Top             =   360
            Width           =   1095
         End
         Begin VB.TextBox findFLNO 
            BackColor       =   &H00C0FFC0&
            Height          =   285
            Left            =   1320
            TabIndex        =   177
            Top             =   720
            Width           =   1095
         End
         Begin VB.CommandButton cmdLoadFlights 
            Caption         =   "&Load flights"
            Height          =   375
            Left            =   240
            TabIndex        =   176
            Top             =   2520
            Width           =   1095
         End
         Begin VB.CheckBox chkDeparture 
            Caption         =   "Departure"
            Height          =   375
            Left            =   2520
            TabIndex        =   175
            Top             =   600
            Value           =   1  'Checked
            Width           =   1095
         End
         Begin VB.CheckBox chkArrival 
            Caption         =   "Arrival"
            Height          =   255
            Left            =   2520
            TabIndex        =   174
            Top             =   360
            Value           =   1  'Checked
            Width           =   1095
         End
         Begin VB.TextBox txtSearchFlightUrno 
            BackColor       =   &H00C0C0C0&
            BorderStyle     =   0  'None
            Height          =   255
            Left            =   2760
            TabIndex        =   173
            Top             =   2640
            Width           =   1095
         End
         Begin TABLib.TAB tab_ALTTAB 
            Height          =   1395
            Left            =   240
            TabIndex        =   180
            Tag             =   "{=TABLE=}ALTTAB{=FIELDS=}ALC2,ALC3,ALFN"
            Top             =   1080
            Width           =   3645
            _Version        =   65536
            _ExtentX        =   6429
            _ExtentY        =   2461
            _StockProps     =   64
         End
         Begin VB.Label Label29 
            Caption         =   "2LC Airline"
            Height          =   255
            Left            =   240
            TabIndex        =   182
            Top             =   360
            Width           =   855
         End
         Begin VB.Label Label30 
            Caption         =   "Flight Number"
            Height          =   255
            Left            =   240
            TabIndex        =   181
            Top             =   720
            Width           =   1095
         End
      End
      Begin VB.Frame Frame12 
         Caption         =   "Flight Date"
         Height          =   3015
         Left            =   -74760
         TabIndex        =   170
         Top             =   720
         Width           =   3135
         Begin MSACAL.Calendar calFLDA 
            Height          =   2655
            Left            =   120
            TabIndex        =   171
            Top             =   240
            Width           =   2895
            _Version        =   524288
            _ExtentX        =   5106
            _ExtentY        =   4683
            _StockProps     =   1
            BackColor       =   -2147483633
            Year            =   2005
            Month           =   8
            Day             =   26
            DayLength       =   1
            MonthLength     =   2
            DayFontColor    =   0
            FirstDay        =   1
            GridCellEffect  =   1
            GridFontColor   =   10485760
            GridLinesColor  =   -2147483632
            ShowDateSelectors=   -1  'True
            ShowDays        =   -1  'True
            ShowHorizontalGrid=   -1  'True
            ShowTitle       =   0   'False
            ShowVerticalGrid=   -1  'True
            TitleFontColor  =   10485760
            ValueIsNull     =   0   'False
            BeginProperty DayFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty GridFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty TitleFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
      End
      Begin VB.Frame Frame6 
         Caption         =   "Date of shift start"
         Height          =   3015
         Left            =   -65160
         TabIndex        =   168
         Top             =   780
         Width           =   3495
         Begin MSACAL.Calendar calSDAY 
            Height          =   2655
            Left            =   120
            TabIndex        =   169
            Top             =   240
            Width           =   3255
            _Version        =   524288
            _ExtentX        =   5741
            _ExtentY        =   4683
            _StockProps     =   1
            BackColor       =   -2147483633
            Year            =   2005
            Month           =   8
            Day             =   26
            DayLength       =   1
            MonthLength     =   2
            DayFontColor    =   0
            FirstDay        =   1
            GridCellEffect  =   1
            GridFontColor   =   10485760
            GridLinesColor  =   -2147483632
            ShowDateSelectors=   -1  'True
            ShowDays        =   -1  'True
            ShowHorizontalGrid=   -1  'True
            ShowTitle       =   0   'False
            ShowVerticalGrid=   -1  'True
            TitleFontColor  =   10485760
            ValueIsNull     =   0   'False
            BeginProperty DayFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   9
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty GridFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty TitleFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   1.5
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
      End
      Begin VB.Frame Frame7 
         Caption         =   "Staff"
         Height          =   3015
         Left            =   -73080
         TabIndex        =   165
         Top             =   780
         Width           =   4935
         Begin TABLib.TAB tab_STFTAB 
            Height          =   2295
            Left            =   120
            TabIndex        =   166
            Tag             =   "{=TABLE=}STFTAB{=FIELDS=}PENO,URNO,LANM,FINM,SHNM"
            Top             =   240
            Width           =   4695
            _Version        =   65536
            _ExtentX        =   8281
            _ExtentY        =   4048
            _StockProps     =   64
         End
         Begin VB.Label lblNoStaff 
            BorderStyle     =   1  'Fixed Single
            Caption         =   "No org units selected."
            Height          =   255
            Left            =   120
            TabIndex        =   167
            Top             =   2640
            Width           =   4695
         End
      End
      Begin VB.Frame Frame11 
         Caption         =   "Search Staff"
         Height          =   3015
         Left            =   -68040
         TabIndex        =   155
         Top             =   780
         Width           =   2775
         Begin VB.TextBox nickName 
            BackColor       =   &H00C0FFC0&
            Height          =   285
            Left            =   1320
            TabIndex        =   247
            Top             =   1080
            Width           =   1335
         End
         Begin VB.CommandButton cmdReset 
            Caption         =   "&Reset"
            Height          =   375
            Left            =   240
            TabIndex        =   161
            Top             =   2400
            Width           =   1095
         End
         Begin VB.CommandButton cmdSearch 
            Caption         =   "&Search !"
            Height          =   375
            Left            =   1440
            TabIndex        =   160
            Top             =   2400
            Width           =   1095
         End
         Begin VB.TextBox findPENO 
            BackColor       =   &H00C0FFC0&
            Height          =   285
            Left            =   1320
            TabIndex        =   159
            Top             =   360
            Width           =   1335
         End
         Begin VB.TextBox findLANM 
            BackColor       =   &H00C0FFC0&
            Height          =   285
            Left            =   1320
            TabIndex        =   158
            Top             =   840
            Visible         =   0   'False
            Width           =   1335
         End
         Begin VB.TextBox findFINM 
            BackColor       =   &H00C0FFC0&
            Height          =   285
            Left            =   1320
            TabIndex        =   157
            Top             =   1320
            Visible         =   0   'False
            Width           =   1335
         End
         Begin VB.TextBox txtfindSTFUrno 
            BackColor       =   &H00C0C0C0&
            BorderStyle     =   0  'None
            Height          =   255
            Left            =   1320
            TabIndex        =   156
            Top             =   1920
            Width           =   1215
         End
         Begin VB.Label Label53 
            Caption         =   "Nick Name"
            Height          =   255
            Left            =   240
            TabIndex        =   246
            Top             =   1080
            Width           =   855
         End
         Begin VB.Label Label28 
            Caption         =   "&Staff Number"
            Height          =   255
            Left            =   240
            TabIndex        =   164
            Top             =   360
            Width           =   1095
         End
         Begin VB.Label Label27 
            Caption         =   "Last Name"
            Height          =   255
            Left            =   240
            TabIndex        =   163
            Top             =   840
            Visible         =   0   'False
            Width           =   1095
         End
         Begin VB.Label Label26 
            Caption         =   "First Name"
            Height          =   255
            Left            =   240
            TabIndex        =   162
            Top             =   1320
            Visible         =   0   'False
            Width           =   1095
         End
      End
      Begin VB.Frame Frame10 
         Caption         =   "Transactions for the selected record (local times)"
         Height          =   4395
         Left            =   -74880
         TabIndex        =   151
         Top             =   6240
         Width           =   14055
         Begin TABLib.TAB tab_R01TAB 
            Height          =   2715
            Left            =   120
            TabIndex        =   152
            Tag             =   "{=TABLE=}R01TAB{=FIELDS=}ORNO,FLST,FVAL,USEC,TIME,SFKT,URNO"
            Top             =   1560
            Visible         =   0   'False
            Width           =   12765
            _Version        =   65536
            _ExtentX        =   22516
            _ExtentY        =   4789
            _StockProps     =   64
         End
         Begin TABLib.TAB tab_LOG 
            Height          =   3795
            Left            =   120
            TabIndex        =   153
            Top             =   240
            Width           =   13725
            _Version        =   65536
            _ExtentX        =   24209
            _ExtentY        =   6694
            _StockProps     =   64
            FontSize        =   12
         End
         Begin VB.Label lblNoLog 
            BorderStyle     =   1  'Fixed Single
            Height          =   255
            Left            =   120
            TabIndex        =   154
            Top             =   4080
            Width           =   4695
         End
      End
      Begin VB.Frame Frame9 
         Caption         =   "Actual Shift Data"
         Height          =   2175
         Left            =   -70440
         TabIndex        =   149
         Top             =   3960
         Width           =   9615
         Begin TABLib.TAB tab_DRRTAB 
            Height          =   1755
            Left            =   120
            TabIndex        =   150
            Tag             =   "{=TABLE=}DRRTAB{=FIELDS=}URNO,SDAY,ROSL,ROSS,SCOD,SCOO,PRFL"
            Top             =   240
            Width           =   9405
            _Version        =   65536
            _ExtentX        =   16589
            _ExtentY        =   3096
            _StockProps     =   64
         End
      End
      Begin VB.Frame Frame8 
         Caption         =   "Selected Staff Data"
         Height          =   2175
         Left            =   -74880
         TabIndex        =   140
         Top             =   3960
         Width           =   4335
         Begin VB.TextBox txtFINM 
            BackColor       =   &H00C0FFFF&
            BorderStyle     =   0  'None
            Height          =   285
            Left            =   1560
            TabIndex        =   145
            TabStop         =   0   'False
            Top             =   1080
            Width           =   2175
         End
         Begin VB.TextBox txtLANM 
            BackColor       =   &H00C0FFFF&
            BorderStyle     =   0  'None
            Height          =   285
            Left            =   1560
            TabIndex        =   144
            TabStop         =   0   'False
            Top             =   720
            Width           =   2175
         End
         Begin VB.TextBox txtPENO 
            BackColor       =   &H00C0FFFF&
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   1560
            TabIndex        =   143
            TabStop         =   0   'False
            Top             =   360
            Width           =   2175
         End
         Begin VB.CommandButton cmdLoadShift 
            Caption         =   "&Load shifts"
            Height          =   375
            Left            =   2640
            TabIndex        =   142
            Top             =   1560
            Width           =   1095
         End
         Begin VB.TextBox txtStfUrno 
            BackColor       =   &H00C0C0C0&
            BorderStyle     =   0  'None
            Height          =   255
            Left            =   1560
            TabIndex        =   141
            Top             =   1560
            Width           =   975
         End
         Begin VB.Label Label25 
            Caption         =   "First Name"
            Height          =   255
            Left            =   240
            TabIndex        =   148
            Top             =   1080
            Width           =   1095
         End
         Begin VB.Label Label24 
            Caption         =   "Last Name"
            Height          =   255
            Left            =   240
            TabIndex        =   147
            Top             =   720
            Width           =   1095
         End
         Begin VB.Label Label20 
            Caption         =   "Staff Number"
            Height          =   255
            Left            =   240
            TabIndex        =   146
            Top             =   360
            Width           =   1095
         End
      End
      Begin VB.CommandButton cmdTest 
         Caption         =   "Command1"
         Height          =   495
         Left            =   -74880
         TabIndex        =   139
         Top             =   10680
         Visible         =   0   'False
         Width           =   1095
      End
      Begin VB.Frame Frame5 
         Caption         =   "Org Unit"
         Height          =   3015
         Left            =   -74880
         TabIndex        =   136
         Top             =   780
         Width           =   1695
         Begin TABLib.TAB tab_ORGTAB 
            Height          =   2235
            Left            =   120
            TabIndex        =   137
            Tag             =   "{=TABLE=}ORGTAB{=FIELDS=}DPT1"
            Top             =   240
            Width           =   1485
            _Version        =   65536
            _ExtentX        =   2619
            _ExtentY        =   3942
            _StockProps     =   64
         End
         Begin VB.Label lblOrgUnits 
            BorderStyle     =   1  'Fixed Single
            Caption         =   "No org units avail."
            Height          =   255
            Left            =   120
            TabIndex        =   138
            Top             =   2640
            Width           =   1455
         End
      End
      Begin VB.Frame Frame1 
         Caption         =   "Authorisation settings"
         Height          =   4185
         Left            =   -74775
         TabIndex        =   121
         Top             =   1125
         Width           =   9630
         Begin VB.TextBox txtPASW 
            Height          =   285
            Left            =   255
            TabIndex        =   130
            Top             =   3720
            Width           =   1755
         End
         Begin VB.ComboBox cmbWildcard 
            Height          =   315
            Left            =   7935
            TabIndex        =   129
            Text            =   "Combo1"
            Top             =   3495
            Width           =   1455
         End
         Begin VB.CommandButton cmdHATTAB 
            Caption         =   "Insert Wildcard:"
            Height          =   405
            Index           =   3
            Left            =   7965
            TabIndex        =   128
            Top             =   2955
            Width           =   1440
         End
         Begin VB.TextBox txtLGOU 
            Height          =   1035
            Left            =   5145
            MultiLine       =   -1  'True
            ScrollBars      =   3  'Both
            TabIndex        =   127
            Text            =   "frmMain.frx":03EE
            Top             =   2970
            Width           =   2520
         End
         Begin VB.TextBox txtNAME 
            Height          =   285
            Left            =   255
            TabIndex        =   126
            Top             =   2985
            Width           =   1755
         End
         Begin VB.CommandButton cmdHATTAB 
            Caption         =   "Delete"
            Height          =   405
            Index           =   2
            Left            =   7950
            TabIndex        =   125
            Top             =   1290
            Width           =   1440
         End
         Begin VB.CommandButton cmdHATTAB 
            Caption         =   "Update"
            Height          =   405
            Index           =   1
            Left            =   7950
            TabIndex        =   124
            Top             =   810
            Width           =   1440
         End
         Begin VB.CommandButton cmdHATTAB 
            Caption         =   "Insert"
            Height          =   405
            Index           =   0
            Left            =   7980
            TabIndex        =   123
            Top             =   345
            Width           =   1440
         End
         Begin VB.TextBox txtLGIN 
            Height          =   1035
            Left            =   2310
            MultiLine       =   -1  'True
            ScrollBars      =   3  'Both
            TabIndex        =   122
            Text            =   "frmMain.frx":03F8
            Top             =   2970
            Width           =   2520
         End
         Begin TABLib.TAB TAB_AUTHORISATION 
            Height          =   2265
            Left            =   240
            TabIndex        =   131
            TabStop         =   0   'False
            Tag             =   "{=FIELDS=}Name,Password,Login,Logout"
            Top             =   375
            Width           =   7425
            _Version        =   65536
            _ExtentX        =   13097
            _ExtentY        =   3995
            _StockProps     =   64
         End
         Begin VB.Label Label4 
            Caption         =   "Password:"
            Height          =   255
            Left            =   255
            TabIndex        =   135
            Top             =   3420
            Width           =   1830
         End
         Begin VB.Label Label3 
            Caption         =   "Logout:"
            Height          =   255
            Left            =   5145
            TabIndex        =   134
            Top             =   2730
            Width           =   1830
         End
         Begin VB.Label Label2 
            Caption         =   "Login:"
            Height          =   255
            Left            =   2310
            TabIndex        =   133
            Top             =   2730
            Width           =   1830
         End
         Begin VB.Label Label1 
            Caption         =   "Name:"
            Height          =   255
            Left            =   255
            TabIndex        =   132
            Top             =   2730
            Width           =   1830
         End
      End
      Begin VB.Frame Frame2 
         Caption         =   "Configuration settings"
         Height          =   5580
         Left            =   -74775
         TabIndex        =   84
         Top             =   5460
         Width           =   9600
         Begin VB.TextBox txtLPGC 
            Height          =   315
            Left            =   2760
            TabIndex        =   103
            ToolTipText     =   "Column Number for ""Keywords Last Page"""
            Top             =   5115
            Width           =   600
         End
         Begin VB.TextBox txtLPGL 
            Height          =   315
            Left            =   1995
            TabIndex        =   102
            ToolTipText     =   "Line Number for ""Keywords Last Page"""
            Top             =   5115
            Width           =   600
         End
         Begin VB.TextBox txtLPGK 
            Height          =   315
            Left            =   3525
            TabIndex        =   101
            ToolTipText     =   "Keywords to identify a ""Last Page"""
            Top             =   5115
            Width           =   5865
         End
         Begin VB.TextBox txtNPGC 
            Height          =   315
            Left            =   2760
            TabIndex        =   100
            ToolTipText     =   "Column Number for ""Keywords Next Page"""
            Top             =   4500
            Width           =   600
         End
         Begin VB.TextBox txtNPGL 
            Height          =   315
            Left            =   1995
            TabIndex        =   99
            ToolTipText     =   "Line Number for ""Keywords Next Page"""
            Top             =   4500
            Width           =   600
         End
         Begin VB.TextBox txtNPGK 
            Height          =   315
            Left            =   3525
            TabIndex        =   98
            ToolTipText     =   "Keywords to identify a ""Next Page"""
            Top             =   4500
            Width           =   5865
         End
         Begin VB.TextBox txtCMDN 
            Height          =   315
            Left            =   240
            TabIndex        =   97
            ToolTipText     =   "Command for ""Next Page"""
            Top             =   4500
            Width           =   1665
         End
         Begin VB.ComboBox cmbACTN 
            Height          =   315
            Left            =   7515
            TabIndex        =   96
            Text            =   "Combo1"
            ToolTipText     =   "Select the ACTN of the command: 'START' or 'REMV'."
            Top             =   3105
            Width           =   1305
         End
         Begin VB.TextBox txtEVNT 
            Height          =   315
            Left            =   6690
            TabIndex        =   95
            Text            =   "LPF"
            ToolTipText     =   "Enter the event for this command, e.g.'FC','LPF'."
            Top             =   3120
            Width           =   690
         End
         Begin VB.TextBox txtSERL 
            Height          =   315
            Left            =   6060
            TabIndex        =   94
            Text            =   "1"
            ToolTipText     =   "Enter the sequence number of this comand to order them."
            Top             =   3120
            Width           =   480
         End
         Begin VB.ComboBox cmbSTAT 
            Height          =   315
            Left            =   4605
            TabIndex        =   93
            Text            =   "Combo1"
            ToolTipText     =   "Select the status of the command: 'ACTIVE' or 'CANCELLED'."
            Top             =   3120
            Width           =   1305
         End
         Begin VB.TextBox txtTIHR 
            Height          =   315
            Left            =   4620
            TabIndex        =   92
            Text            =   "72,48,24,12,6,3,2"
            ToolTipText     =   "Enter the hours the timer event should occour, e.g. '72,48,24,12,6,3,2'."
            Top             =   3735
            Width           =   4200
         End
         Begin VB.TextBox txtTIMI 
            Height          =   315
            Left            =   240
            TabIndex        =   91
            Text            =   "70,60,50,40,30,25,20,15,10,5,ATD"
            ToolTipText     =   "Enter the minutes the timer event should occour, e.g. '70,60,50,40,30,25,20,15,10,5,ATD'."
            Top             =   3735
            Width           =   4200
         End
         Begin VB.TextBox txtCOMD 
            Height          =   315
            Left            =   2400
            TabIndex        =   90
            Text            =   "KWDSQ--/date/SIN"
            ToolTipText     =   "Enter a command, e.g. '@P/IN'."
            Top             =   3120
            Width           =   2025
         End
         Begin VB.ComboBox cmbGRPT 
            Height          =   315
            Left            =   885
            TabIndex        =   89
            Text            =   "Combo1"
            ToolTipText     =   "Select a group the airline belongs to."
            Top             =   3120
            Width           =   1305
         End
         Begin VB.TextBox txtALC2 
            Height          =   315
            Left            =   240
            TabIndex        =   88
            Text            =   "SQ"
            ToolTipText     =   "Enter 2-letter airline-code, e.g. 'SQ'"
            Top             =   3120
            Width           =   420
         End
         Begin VB.CommandButton cmdHCFTAB 
            Caption         =   "Insert"
            Height          =   405
            Index           =   0
            Left            =   7950
            TabIndex        =   87
            Top             =   315
            Width           =   1440
         End
         Begin VB.CommandButton cmdHCFTAB 
            Caption         =   "Update"
            Height          =   405
            Index           =   1
            Left            =   7935
            TabIndex        =   86
            Top             =   780
            Width           =   1440
         End
         Begin VB.CommandButton cmdHCFTAB 
            Caption         =   "Delete"
            Height          =   405
            Index           =   2
            Left            =   7935
            TabIndex        =   85
            Top             =   1260
            Width           =   1440
         End
         Begin TABLib.TAB TAB_CONFIGURATION 
            Height          =   2400
            Left            =   255
            TabIndex        =   104
            TabStop         =   0   'False
            Tag             =   $"frmMain.frx":0426
            Top             =   360
            Width           =   7425
            _Version        =   65536
            _ExtentX        =   13097
            _ExtentY        =   4233
            _StockProps     =   64
         End
         Begin VB.Label Label23 
            Caption         =   "ColNo:"
            Height          =   240
            Left            =   2760
            TabIndex        =   120
            Top             =   4875
            Width           =   615
         End
         Begin VB.Label Label22 
            Caption         =   "LineNo:"
            Height          =   240
            Left            =   1995
            TabIndex        =   119
            Top             =   4875
            Width           =   825
         End
         Begin VB.Label Label21 
            Caption         =   "Keywords to identify last page:"
            Height          =   240
            Left            =   3525
            TabIndex        =   118
            Top             =   4875
            Width           =   2910
         End
         Begin VB.Label Label19 
            Caption         =   "ColNo:"
            Height          =   240
            Left            =   2760
            TabIndex        =   117
            Top             =   4260
            Width           =   615
         End
         Begin VB.Label Label18 
            Caption         =   "LineNo:"
            Height          =   240
            Left            =   1995
            TabIndex        =   116
            Top             =   4260
            Width           =   825
         End
         Begin VB.Label Label17 
            Caption         =   "Keywords to identify next page:"
            Height          =   240
            Left            =   3525
            TabIndex        =   115
            Top             =   4260
            Width           =   2910
         End
         Begin VB.Label Label16 
            Caption         =   "Cmd next page:"
            Height          =   240
            Left            =   240
            TabIndex        =   114
            Top             =   4260
            Width           =   1560
         End
         Begin VB.Line Line1 
            X1              =   105
            X2              =   9495
            Y1              =   4155
            Y2              =   4155
         End
         Begin VB.Label Label13 
            Caption         =   "ACTN:"
            Height          =   330
            Left            =   7515
            TabIndex        =   113
            Top             =   2820
            Width           =   1275
         End
         Begin VB.Label Label12 
            Caption         =   "Event:"
            Height          =   180
            Left            =   6720
            TabIndex        =   112
            Top             =   2835
            Width           =   555
         End
         Begin VB.Label Label11 
            Caption         =   "Seq.:"
            Height          =   240
            Left            =   6060
            TabIndex        =   111
            Top             =   2835
            Width           =   465
         End
         Begin VB.Label Label10 
            Caption         =   "Status:"
            Height          =   330
            Left            =   4605
            TabIndex        =   110
            Top             =   2835
            Width           =   1275
         End
         Begin VB.Label Label9 
            Caption         =   "Timer hours:"
            Height          =   225
            Left            =   4605
            TabIndex        =   109
            Top             =   3495
            Width           =   1470
         End
         Begin VB.Label Label8 
            Caption         =   "Timer minutes:"
            Height          =   225
            Left            =   240
            TabIndex        =   108
            Top             =   3495
            Width           =   1470
         End
         Begin VB.Label Label7 
            Caption         =   "Command:"
            Height          =   225
            Left            =   2385
            TabIndex        =   107
            Top             =   2835
            Width           =   990
         End
         Begin VB.Label Label6 
            Caption         =   "Group type:"
            Height          =   240
            Left            =   885
            TabIndex        =   106
            Top             =   2835
            Width           =   1350
         End
         Begin VB.Label Label5 
            Caption         =   "ALC2:"
            Height          =   240
            Left            =   240
            TabIndex        =   105
            Top             =   2835
            Width           =   825
         End
      End
      Begin VB.CommandButton cmdExit 
         Caption         =   "Exit"
         Height          =   405
         Index           =   0
         Left            =   -69795
         TabIndex        =   83
         Top             =   11145
         Width           =   1440
      End
      Begin VB.CommandButton cmdSave 
         Caption         =   "Save"
         Height          =   405
         Left            =   -71415
         Style           =   1  'Graphical
         TabIndex        =   82
         Top             =   11145
         Width           =   1440
      End
      Begin VB.Frame frmAllJobs 
         Height          =   3135
         Left            =   -67320
         TabIndex        =   72
         Top             =   4440
         Width           =   5535
         Begin VB.CommandButton cmdShowLog 
            Caption         =   "View Transactions"
            Height          =   375
            Left            =   3600
            TabIndex        =   76
            Top             =   2520
            Width           =   1695
         End
         Begin VB.TextBox txtJobType 
            BackColor       =   &H00C0FFFF&
            BorderStyle     =   0  'None
            Height          =   255
            Left            =   1080
            TabIndex        =   75
            Top             =   2040
            Width           =   4215
         End
         Begin VB.TextBox txtJobFlight 
            BackColor       =   &H00C0FFFF&
            BorderStyle     =   0  'None
            Height          =   255
            Left            =   1080
            TabIndex        =   74
            Top             =   2400
            Width           =   1335
         End
         Begin VB.TextBox txtJobUrno 
            BackColor       =   &H00C0C0C0&
            BorderStyle     =   0  'None
            Height          =   255
            Left            =   1080
            TabIndex        =   73
            Top             =   2760
            Width           =   1335
         End
         Begin TABLib.TAB tab_STFJOBS 
            Height          =   1635
            Left            =   120
            TabIndex        =   77
            Tag             =   "{=TABLE=}JOBTAB{=FIELDS=}URNO,ACFR,ACTO,PLFR,PLTO,UAFT,UJTY,UDSR,USTF"
            Top             =   120
            Width           =   5205
            _Version        =   65536
            _ExtentX        =   9181
            _ExtentY        =   2884
            _StockProps     =   64
         End
         Begin VB.Label Label37 
            Caption         =   "Job Type"
            Height          =   255
            Left            =   240
            TabIndex        =   80
            Top             =   2040
            Width           =   1095
         End
         Begin VB.Label Label38 
            Caption         =   "Flight"
            Height          =   255
            Left            =   240
            TabIndex        =   79
            Top             =   2400
            Width           =   1095
         End
         Begin VB.Label Label40 
            Caption         =   "Urno (internal)"
            Height          =   375
            Left            =   240
            TabIndex        =   78
            Top             =   2640
            Width           =   855
         End
      End
      Begin VB.Frame Frame3 
         Caption         =   "All Transactions"
         Height          =   4215
         Left            =   -74640
         TabIndex        =   65
         Top             =   3840
         Width           =   12495
         Begin TABLib.TAB tab_LOGUSER 
            Height          =   1635
            Left            =   120
            TabIndex        =   66
            Top             =   480
            Width           =   12165
            _Version        =   65536
            _ExtentX        =   21458
            _ExtentY        =   2884
            _StockProps     =   64
            Columns         =   30
         End
         Begin TABLib.TAB tab_LOGUSER2 
            Height          =   1635
            Left            =   120
            TabIndex        =   67
            Top             =   2400
            Width           =   12165
            _Version        =   65536
            _ExtentX        =   21458
            _ExtentY        =   2884
            _StockProps     =   64
            Columns         =   30
         End
         Begin VB.Label lblSHIFT 
            Caption         =   "Shift record transactions"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Left            =   120
            TabIndex        =   69
            Top             =   240
            Width           =   4935
         End
         Begin VB.Label lblJOB 
            Caption         =   "Job record transactions"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Left            =   120
            TabIndex        =   68
            Top             =   2160
            Width           =   3975
         End
      End
      Begin VB.Frame Frame4 
         Caption         =   "Transactions by selected login"
         Height          =   2295
         Left            =   -74640
         TabIndex        =   62
         Top             =   8160
         Visible         =   0   'False
         Width           =   12495
         Begin TABLib.TAB TAB2 
            Height          =   1875
            Left            =   120
            TabIndex        =   63
            Top             =   240
            Width           =   12165
            _Version        =   65536
            _ExtentX        =   21458
            _ExtentY        =   3307
            _StockProps     =   64
            Columns         =   30
         End
      End
      Begin VB.Frame Frame15 
         Caption         =   "Login Date"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   3615
         Left            =   -74640
         TabIndex        =   58
         Top             =   1080
         Width           =   5055
         Begin MSACAL.Calendar userCal1 
            Height          =   3015
            Left            =   360
            TabIndex        =   59
            Top             =   360
            Width           =   4215
            _Version        =   524288
            _ExtentX        =   7435
            _ExtentY        =   5318
            _StockProps     =   1
            BackColor       =   -2147483633
            Year            =   2006
            Month           =   4
            Day             =   12
            DayLength       =   1
            MonthLength     =   2
            DayFontColor    =   0
            FirstDay        =   7
            GridCellEffect  =   1
            GridFontColor   =   10485760
            GridLinesColor  =   -2147483632
            ShowDateSelectors=   -1  'True
            ShowDays        =   -1  'True
            ShowHorizontalGrid=   -1  'True
            ShowTitle       =   0   'False
            ShowVerticalGrid=   -1  'True
            TitleFontColor  =   10485760
            ValueIsNull     =   0   'False
            BeginProperty DayFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty GridFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty TitleFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
      End
      Begin VB.Frame Frame16 
         Caption         =   "Staff Information"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   3615
         Left            =   -69120
         TabIndex        =   49
         Top             =   1080
         Width           =   6015
         Begin VB.CommandButton userLoadTrans1 
            Caption         =   "Load Job Transactions"
            Height          =   495
            Left            =   3720
            TabIndex        =   245
            Top             =   2880
            Width           =   1935
         End
         Begin VB.TextBox userStaffNumber 
            Height          =   405
            Left            =   1800
            TabIndex        =   54
            Top             =   480
            Width           =   1575
         End
         Begin VB.TextBox userLastName 
            Height          =   405
            Left            =   1800
            TabIndex        =   53
            Top             =   1150
            Width           =   1575
         End
         Begin VB.TextBox userFirstName 
            Height          =   405
            Left            =   1800
            TabIndex        =   52
            Top             =   1800
            Width           =   1575
         End
         Begin VB.CommandButton userReset 
            Caption         =   "Reset"
            Height          =   495
            Left            =   240
            TabIndex        =   51
            Top             =   2880
            Width           =   1095
         End
         Begin VB.CommandButton userLoadTrans 
            Caption         =   "Load Shift Transactions"
            Height          =   495
            Left            =   1560
            TabIndex        =   50
            Top             =   2880
            Width           =   1935
         End
         Begin VB.Label Label35 
            Caption         =   "Staff Number"
            Height          =   495
            Left            =   240
            TabIndex        =   57
            Top             =   600
            Width           =   1335
         End
         Begin VB.Label Label41 
            Caption         =   "Last Name"
            Height          =   495
            Left            =   240
            TabIndex        =   56
            Top             =   1200
            Width           =   975
         End
         Begin VB.Label Label42 
            Caption         =   "First Name"
            Height          =   495
            Left            =   240
            TabIndex        =   55
            Top             =   1920
            Width           =   1095
         End
      End
      Begin VB.Frame Frame18 
         Caption         =   "All Transactions"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   5415
         Left            =   -74640
         TabIndex        =   45
         Top             =   5040
         Width           =   13815
         Begin TABLib.TAB tab_LOGUSERR02 
            Height          =   2055
            Left            =   120
            TabIndex        =   248
            Top             =   3120
            Width           =   13455
            _Version        =   65536
            _ExtentX        =   23733
            _ExtentY        =   3625
            _StockProps     =   64
         End
         Begin TABLib.TAB tab_LOGUSERR01 
            Height          =   1935
            Left            =   120
            TabIndex        =   46
            Top             =   600
            Width           =   13455
            _Version        =   65536
            _ExtentX        =   23733
            _ExtentY        =   3413
            _StockProps     =   64
         End
         Begin TABLib.TAB tab_SERTAB 
            Height          =   495
            Left            =   11520
            TabIndex        =   224
            Tag             =   "{=TABLE=}SERTAB{=FIELDS=}URNO,SNAM,SECO"
            Top             =   1320
            Width           =   1455
            _Version        =   65536
            _ExtentX        =   2566
            _ExtentY        =   873
            _StockProps     =   64
         End
         Begin VB.Label Label43 
            Caption         =   " Shift Transactions"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Left            =   120
            TabIndex        =   48
            Top             =   360
            Width           =   2535
         End
         Begin VB.Label Label44 
            Caption         =   " Job Transactions"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Left            =   120
            TabIndex        =   47
            Top             =   2760
            Width           =   2895
         End
      End
      Begin VB.Frame Frame19 
         Caption         =   "Flight Date"
         Height          =   3375
         Left            =   -74880
         TabIndex        =   43
         Top             =   840
         Width           =   3015
         Begin MSACAL.Calendar flightCalendar 
            Height          =   2775
            Left            =   120
            TabIndex        =   44
            Top             =   360
            Width           =   2775
            _Version        =   524288
            _ExtentX        =   4895
            _ExtentY        =   4895
            _StockProps     =   1
            BackColor       =   -2147483633
            Year            =   2006
            Month           =   5
            Day             =   9
            DayLength       =   1
            MonthLength     =   2
            DayFontColor    =   0
            FirstDay        =   1
            GridCellEffect  =   1
            GridFontColor   =   10485760
            GridLinesColor  =   -2147483632
            ShowDateSelectors=   -1  'True
            ShowDays        =   -1  'True
            ShowHorizontalGrid=   -1  'True
            ShowTitle       =   0   'False
            ShowVerticalGrid=   -1  'True
            TitleFontColor  =   10485760
            ValueIsNull     =   0   'False
            BeginProperty DayFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty GridFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty TitleFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
      End
      Begin VB.Frame Frame21 
         Caption         =   "Flight Filter"
         Height          =   3375
         Left            =   -71760
         TabIndex        =   25
         Top             =   840
         Width           =   4935
         Begin VB.OptionButton checkDeparture 
            Caption         =   "Departure"
            Height          =   255
            Left            =   2760
            TabIndex        =   226
            Top             =   600
            Width           =   1455
         End
         Begin VB.OptionButton checkArrival 
            Caption         =   "Arrival"
            Height          =   255
            Left            =   2760
            TabIndex        =   225
            Top             =   240
            Width           =   975
         End
         Begin VB.TextBox textAirline 
            Height          =   285
            Left            =   1200
            TabIndex        =   29
            Top             =   240
            Width           =   1215
         End
         Begin VB.TextBox textFlight 
            Height          =   285
            Left            =   1200
            TabIndex        =   28
            Top             =   600
            Width           =   1215
         End
         Begin VB.CommandButton loadFlight 
            Caption         =   "Load Flight"
            Height          =   375
            Left            =   1200
            TabIndex        =   27
            Top             =   2760
            Width           =   1095
         End
         Begin VB.CommandButton resetFlight 
            Caption         =   "Reset"
            Height          =   375
            Left            =   2760
            TabIndex        =   26
            Top             =   2760
            Width           =   1095
         End
         Begin VB.Label Airline 
            Caption         =   "Airline"
            Height          =   255
            Left            =   120
            TabIndex        =   42
            Top             =   240
            Width           =   975
         End
         Begin VB.Label flightNo 
            Caption         =   "Flight No"
            Height          =   375
            Left            =   120
            TabIndex        =   41
            Top             =   600
            Width           =   855
         End
         Begin VB.Label labelFlightNo 
            BackColor       =   &H0080FFFF&
            Height          =   255
            Left            =   1200
            TabIndex        =   40
            Top             =   1200
            Width           =   855
         End
         Begin VB.Label Label46 
            Caption         =   "(Local Times)"
            Height          =   255
            Left            =   120
            TabIndex        =   39
            Top             =   1320
            Width           =   975
         End
         Begin VB.Label labelAdid 
            BackColor       =   &H0080FFFF&
            Height          =   255
            Left            =   360
            TabIndex        =   38
            Top             =   1680
            Width           =   1695
         End
         Begin VB.Label labelDest 
            BackColor       =   &H0080FFFF&
            ForeColor       =   &H00404040&
            Height          =   255
            Left            =   360
            TabIndex        =   37
            Top             =   2160
            Width           =   1695
         End
         Begin VB.Label Label47 
            Caption         =   "Sheduled"
            Height          =   255
            Left            =   2400
            TabIndex        =   36
            Top             =   1200
            Width           =   855
         End
         Begin VB.Label labelScheduled 
            BackColor       =   &H0080FFFF&
            Height          =   375
            Left            =   3360
            TabIndex        =   35
            Top             =   1080
            Width           =   1335
         End
         Begin VB.Label Label48 
            Caption         =   "Best Time"
            Height          =   255
            Left            =   2400
            TabIndex        =   34
            Top             =   1680
            Width           =   855
         End
         Begin VB.Label labelBest 
            BackColor       =   &H0080FFFF&
            Height          =   375
            Left            =   3360
            TabIndex        =   33
            Top             =   1560
            Width           =   1335
         End
         Begin VB.Label Label49 
            Caption         =   "Urno(Internal)"
            Height          =   255
            Left            =   2280
            TabIndex        =   32
            Top             =   2160
            Width           =   975
         End
         Begin VB.Label labelUrno 
            BackColor       =   &H80000010&
            Height          =   255
            Left            =   3360
            TabIndex        =   31
            Top             =   2160
            Width           =   1215
         End
         Begin VB.Label Label45 
            Caption         =   "Flight"
            Height          =   255
            Left            =   240
            TabIndex        =   30
            Top             =   1080
            Width           =   735
         End
      End
      Begin VB.Frame Frame24 
         Caption         =   "Employee"
         Height          =   3375
         Left            =   -66720
         TabIndex        =   16
         Top             =   840
         Width           =   2295
         Begin VB.TextBox textPeno 
            Height          =   285
            Left            =   120
            TabIndex        =   18
            Top             =   720
            Width           =   1935
         End
         Begin VB.CommandButton resetEmployee 
            Caption         =   "Reset"
            Height          =   375
            Left            =   480
            TabIndex        =   17
            Top             =   2760
            Width           =   1095
         End
         Begin VB.Label Label50 
            Caption         =   "Employee PENO"
            Height          =   375
            Left            =   120
            TabIndex        =   24
            Top             =   360
            Width           =   1335
         End
         Begin VB.Label Label51 
            Caption         =   "First Name"
            Height          =   255
            Left            =   120
            TabIndex        =   23
            Top             =   1080
            Width           =   1335
         End
         Begin VB.Label firstName 
            BackColor       =   &H0080FFFF&
            Height          =   375
            Left            =   120
            TabIndex        =   22
            Top             =   1320
            Width           =   1935
         End
         Begin VB.Label Label52 
            Caption         =   "Last Name"
            Height          =   255
            Left            =   120
            TabIndex        =   21
            Top             =   1800
            Width           =   1335
         End
         Begin VB.Label lastName 
            BackColor       =   &H0080FFFF&
            Height          =   375
            Left            =   120
            TabIndex        =   20
            Top             =   2160
            Width           =   1935
         End
         Begin VB.Label urnoSTF 
            Enabled         =   0   'False
            Height          =   15
            Left            =   120
            TabIndex        =   19
            Top             =   2640
            Width           =   255
         End
      End
      Begin VB.Frame Frame25 
         Caption         =   "Templates"
         Height          =   3375
         Left            =   -64320
         TabIndex        =   13
         Top             =   840
         Width           =   3615
         Begin VB.CommandButton resetTemplate 
            Caption         =   "Reset"
            Height          =   375
            Left            =   1320
            TabIndex        =   14
            Top             =   2760
            Width           =   1095
         End
         Begin TABLib.TAB tab_TPLTAB 
            Height          =   2295
            Left            =   120
            TabIndex        =   15
            Tag             =   "{=TABLE=}TPLTAB{=FIELDS=}URNO,TNAM,APPL,TPST"
            Top             =   360
            Width           =   3375
            _Version        =   65536
            _ExtentX        =   5953
            _ExtentY        =   4048
            _StockProps     =   64
         End
      End
      Begin VB.Frame Frame26 
         Caption         =   "Job Types"
         Height          =   3015
         Left            =   -74880
         TabIndex        =   10
         Top             =   4440
         Width           =   4095
         Begin VB.CommandButton resetJobtype 
            Caption         =   "Reset"
            Height          =   375
            Left            =   1440
            TabIndex        =   11
            Top             =   2520
            Width           =   975
         End
         Begin TABLib.TAB tab_JTYTAB1 
            Height          =   2055
            Left            =   120
            TabIndex        =   12
            Tag             =   "{=TABLE=}JTYTAB{=FIELDS=}URNO,DSCR"
            Top             =   360
            Width           =   3735
            _Version        =   65536
            _ExtentX        =   6588
            _ExtentY        =   3625
            _StockProps     =   64
         End
      End
      Begin VB.CommandButton loadJobs 
         Caption         =   "Load Jobs"
         Height          =   495
         Left            =   -70560
         TabIndex        =   9
         Top             =   5160
         Width           =   1335
      End
      Begin VB.Frame Frame27 
         Caption         =   "Jobs"
         Height          =   3015
         Left            =   -69000
         TabIndex        =   7
         Top             =   4440
         Width           =   8295
         Begin TABLib.TAB tab_JOBTAB1 
            Height          =   2175
            Left            =   120
            TabIndex        =   8
            Top             =   360
            Width           =   7935
            _Version        =   65536
            _ExtentX        =   13996
            _ExtentY        =   3836
            _StockProps     =   64
         End
      End
      Begin VB.Frame Frame28 
         Caption         =   "Transactions for Job"
         Height          =   3255
         Left            =   -74880
         TabIndex        =   5
         Top             =   7560
         Width           =   14175
         Begin TABLib.TAB tab_JOBLOG 
            Height          =   2655
            Left            =   240
            TabIndex        =   6
            Top             =   360
            Width           =   13695
            _Version        =   65536
            _ExtentX        =   24156
            _ExtentY        =   4683
            _StockProps     =   64
         End
      End
      Begin TABLib.TAB tab_STFCOMPLETE 
         Height          =   1635
         Left            =   -65160
         TabIndex        =   60
         Tag             =   "{=TABLE=}STFTAB{=FIELDS=}PENO,URNO,LANM,FINM"
         Top             =   8880
         Visible         =   0   'False
         Width           =   3285
         _Version        =   65536
         _ExtentX        =   5794
         _ExtentY        =   2884
         _StockProps     =   64
      End
      Begin TABLib.TAB tab_R02TABUSER 
         Height          =   2115
         Left            =   -65160
         TabIndex        =   61
         Tag             =   "{=TABLE=}R02TAB{=FIELDS=}ORNO,FLST,FVAL,USEC,TIME,SFKT,URNO,STAB"
         Top             =   6600
         Visible         =   0   'False
         Width           =   5085
         _Version        =   65536
         _ExtentX        =   8969
         _ExtentY        =   3731
         _StockProps     =   64
      End
      Begin TABLib.TAB tab_R01TABUSER 
         Height          =   2115
         Left            =   -65160
         TabIndex        =   64
         Tag             =   "{=TABLE=}R01TAB{=FIELDS=}ORNO,FLST,FVAL,USEC,TIME,SFKT,URNO,STAB"
         Top             =   4200
         Visible         =   0   'False
         Width           =   5085
         _Version        =   65536
         _ExtentX        =   8969
         _ExtentY        =   3731
         _StockProps     =   64
      End
      Begin TABLib.TAB tab_JTYTAB 
         Height          =   2115
         Left            =   -70680
         TabIndex        =   70
         Tag             =   "{=TABLE=}JTYTAB{=FIELDS=}URNO,DSCR"
         Top             =   8160
         Visible         =   0   'False
         Width           =   3405
         _Version        =   65536
         _ExtentX        =   6006
         _ExtentY        =   3731
         _StockProps     =   64
      End
      Begin TABLib.TAB tab_AFTTABall 
         Height          =   2115
         Left            =   -67080
         TabIndex        =   71
         Tag             =   "{=TABLE=}AFTTAB{=FIELDS=}URNO,FLNO,ADID,STOA,STOD,TIFD,TIFA,ORG3,DES3,ALC2"
         Top             =   8160
         Visible         =   0   'False
         Width           =   4605
         _Version        =   65536
         _ExtentX        =   8123
         _ExtentY        =   3731
         _StockProps     =   64
      End
      Begin TABLib.TAB tab_SYSDRR 
         Height          =   1095
         Left            =   -68040
         TabIndex        =   81
         Tag             =   "{=TABLE=}SYSTAB{=FIELDS=}FINA,ADDI"
         Top             =   720
         Visible         =   0   'False
         Width           =   6045
         _Version        =   65536
         _ExtentX        =   10663
         _ExtentY        =   1931
         _StockProps     =   64
      End
      Begin AATLOGINLib.AatLogin AATLoginControl1 
         Height          =   1095
         Left            =   -74400
         TabIndex        =   223
         Top             =   1080
         Visible         =   0   'False
         Width           =   1575
         _Version        =   65536
         _ExtentX        =   2778
         _ExtentY        =   1931
         _StockProps     =   0
      End
      Begin TABLib.TAB tab_PRVTAB 
         Height          =   375
         Left            =   7080
         TabIndex        =   242
         Tag             =   "{=TABLE=}PRVTAB{=FIELDS=}FAPP,FSEC,FFKT,STAT"
         Top             =   4800
         Width           =   375
         _Version        =   65536
         _ExtentX        =   661
         _ExtentY        =   661
         _StockProps     =   64
      End
      Begin TABLib.TAB tab_SECTABPRF 
         Height          =   495
         Left            =   7680
         TabIndex        =   240
         Tag             =   "{=TABLE=}SECTAB{=FIELDS=}URNO,USID,NAME,TYPE"
         Top             =   4560
         Width           =   135
         _Version        =   65536
         _ExtentX        =   238
         _ExtentY        =   873
         _StockProps     =   64
      End
      Begin TABLib.TAB tab_FKTTAB 
         Height          =   375
         Left            =   6840
         TabIndex        =   243
         Tag             =   "{=TABLE=}FKTTAB{=FIELDS=}URNO,SDAL,FUAL,FUNC"
         Top             =   4320
         Width           =   375
         _Version        =   65536
         _ExtentX        =   661
         _ExtentY        =   661
         _StockProps     =   64
      End
      Begin TABLib.TAB tab_SECTABAPP 
         Height          =   375
         Left            =   6120
         TabIndex        =   241
         Tag             =   "{=TABLE=}SECTAB{=FIELDS=}URNO,USID,NAME,TYPE"
         Top             =   4800
         Width           =   375
         _Version        =   65536
         _ExtentX        =   661
         _ExtentY        =   661
         _StockProps     =   64
      End
   End
End
Attribute VB_Name = "frmMain"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'#######################################################################
'# Main form
'# RMSLOGVIEWER
'#  based on HASConfigTool Sources
'#
'#  adapted by SHA 20050826
'#
'#  20050909 SHA  DRRTAB changed timefield R01TAb to reable time format
'#                STFTAB readable Headers, hide URNO
'#                Added User Transactions
'#  20050915 SHA  Reset for DRR transaction completed
'#  20050916 SHA  GUI functionality DRR finalized
'#  20050919 SHA
'#  20050920 SHA
'#######################################################################


'#######################################################################
'### UTC DIFF IN HOURS
'#######################################################################
Private Function GetUTCOffset() As Single
    Dim strUtcArr() As String
    Dim strArr() As String
    Dim CurrStr As String
    Dim tmpStr As String

    Dim istrRet As Integer
    Dim count As Integer
    Dim i As Integer

    GetUTCOffset = 0

    If UfisCom1.CallServer("GFR", "", "", "", "[CONFIG]", "360") = 0 Then
        tmpStr = UfisCom1.GetDataBuffer(True)
    End If
    strArr = Split(tmpStr, Chr(10))
    count = UBound(strArr)
    For i = 0 To count
        CurrStr = strArr(i)
        istrRet = InStr(1, CurrStr, "UTCD", 0)
        If istrRet <> 0 Then
            strUtcArr = Split(strArr(i), ",")
            GetUTCOffset = CSng(strUtcArr(1) / 60)
            Exit For
        End If
    Next i

End Function


'#######################################################################
'### LOAD ORG UNITS PLUS BASIC DATA
'#######################################################################
Sub LoadOrgUnits()
    
    Dim strQuery As String
    
    
    '### ORG UNITS
    frmData.LoadData tab_ORGTAB, ""
    tab_ORGTAB.AutoSizeColumns
    tab_ORGTAB.HeaderString = "Code"
    tab_ORGTAB.Sort 0, True, False
    
    lblOrgUnits.Caption = tab_ORGTAB.GetLineCount & " ORG Units."
    
    ' ORG UNITS to show in BDPSCSEC page
    frmData.LoadData tab_SECORGTAB, ""
    tab_SECORGTAB.AutoSizeColumns
    tab_SECORGTAB.HeaderString = "Org Unit Code"
    tab_SECORGTAB.HeaderLengthString = "210"
    tab_SECORGTAB.Sort 0, True, False
    ' kkh on 20/11/2007 to cater for USER do not assign to any org unit
    tab_SECORGTAB.InsertTextLine "NO ORG UNIT", False
    lblSecOrgUnits.Caption = tab_SECORGTAB.GetLineCount & " ORG Units."
        
    '### AIRLINE CODES
    frmData.LoadData tab_ALTTAB, ""
    tab_ALTTAB.AutoSizeColumns
    tab_ALTTAB.Sort 0, True, False
    
    '### AIRLINE CODES
    'frmData.LoadData tab_ALTTABJOB, ""
    'tab_ALTTABJOB.AutoSizeColumns
    'tab_ALTTABJOB.Sort 0, True, False
    
    '### JOBTYPES
    frmData.LoadData tab_JTYTAB1, ""
    tab_JTYTAB1.ShowHorzScroller True
    'tab_JTYTAB1.AutoSizeColumns
    'tab_JTYTAB1.Sort 0, True, False
    
    '### TEMPLATES
    strQuery = "where tpst = '1'"
    frmData.LoadData tab_TPLTAB, strQuery
    prepareTpltab
    tab_TPLTAB.ShowHorzScroller True
    'tab_TPLTAB.AutoSizeColumns
    'tab_TPLTAB.Sort 0, True, False
    
    '### Complete Staff table for reference
    frmData.LoadData tab_STFCOMPLETE, ""
    tab_STFCOMPLETE.AutoSizeColumns
    tab_STFCOMPLETE.Refresh
    
    '### JOB TYPES
    frmData.LoadData tab_JTYTAB, ""
    'tab_JTYTAB.AutoSizeColumns
    tab_JTYTAB.Refresh
    
    frmData.LoadData tab_SERTAB, ""
    'tab_JTYTAB.AutoSizeColumns
    tab_SERTAB.Refresh
       
End Sub


'#######################################################################
'### LOAD SHIFT DATA FOR SPECIFIC DAY
'#######################################################################
Private Sub calSDAY_DblClick()
    
    Dim strkey1 As String
    Dim strWhere As String
    Dim l As Long
    Dim findDate As String
    
    findDate = Format(calSDAY.Year, "0000") & Format(calSDAY.Month, "00") & Format(calSDAY.Day, "00")
    
    l = tab_STFTAB.GetCurrentSelected
    
    If l <> -1 Then
        strkey1 = tab_STFTAB.GetColumnValues(l, 1)
        
        tab_DRRTAB.ResetContent
                
        strWhere = "where stfu='" & strkey1 & "' and sday='" & findDate & "'"
        frmData.LoadData tab_DRRTAB, strWhere
        tab_DRRTAB.Sort 2, True, False
        tab_DRRTAB.Refresh
        
        If tab_DRRTAB.GetLineCount = 0 Then
            tab_LOG.ResetContent
            tab_LOG.Refresh
            MsgBox "No shift data available for this date."
        Else
            tab_DRRTAB.SetFocus
        End If
    Else
        MsgBox "No staff id selected."
    End If

End Sub

'#######################################################################
'###
'#######################################################################
Private Sub cmdAllJobs_Click()
    
    Dim l As Long
    Dim strkey1 As String
    
    l = tab_JOBTAB.GetCurrentSelected
    
    If l <> -1 Then
      
        frmAllJobs = "All jobs for " & txtStfPeno & " on " & calFLDA
          
        MousePointer = vbHourglass
        StatusBar1.Panels(1).Text = "Loading job records for selected staff ..."
        '### GET UDSR
        strkey1 = tab_JOBTAB.GetColumnValues(l, 7)
        strkey1 = "where udsr='" & strkey1 & "'"
        
        tab_STFJOBS.ResetContent
        frmData.LoadData tab_STFJOBS, strkey1
        tab_STFJOBS.Refresh
        
        TranslateStaffJobs
        
        MousePointer = vbDefault
        StatusBar1.Panels(1).Text = "Ready."
    Else
        MsgBox "No job selected"
    End If
  
End Sub

'#######################################################################
'###
'#######################################################################
Private Sub cmdAllUsers_Click()
    Dim tmpDay As String
    Dim tmpStr As String
    
    Dim strkey1 As String
    'Dim strWhere As String
    Dim strTemp As String
    Dim strFields As String
    Dim strValues As String
    Dim l As Long
    'Dim llLineCount As Long
    'Dim INlist As String

    tmpDay = Format(calUser.Year, "0000") & Format(calUser.Month, "00") & Format(calUser.Day, "00")

    cmdAllUsers.Enabled = False
    tab_R01TABUSER.ResetContent

    StatusBar1.Panels(1).Text = "Loading shift transactions..."
    MousePointer = vbHourglass

    tmpStr = "where time like '" & tmpDay & "%'"
    frmData.LoadData tab_R01TABUSER, tmpStr
    tab_R01TABUSER.AutoSizeColumns
    tab_R01TABUSER.Refresh
    
    lblSHIFT = "Shift record transactions (" & tab_R01TABUSER.GetLineCount & " records)"
    
    If tab_R01TABUSER.GetLineCount > 0 Then
        tab_LOGUSER.ResetContent
        
        '### CONTENT
        For l = 0 To tab_R01TABUSER.GetLineCount - 1 Step 1
            
            '### LOG COLUMNS
            strFields = tab_R01TABUSER.GetColumnValues(l, 1)
            strFields = Replace(strFields, Chr(30), ",")
            strValues = tab_R01TABUSER.GetColumnValues(l, 2)
            strValues = Replace(strValues, Chr(30), ",")
            
            '### USEC
            strkey1 = tab_R01TABUSER.GetColumnValues(l, 3)
            
            '### No user in R01TAB FOUND?
            If strkey1 = "" Or UCase(strkey1) = "EVENTSPOOL" Then
                '### TAKE USEU
                strkey1 = GetFieldValue("USEU", strValues, strFields)
                '### STILL EMPTY TAKE USEC
                If strkey1 = "" Then
                    strkey1 = GetFieldValue("USEC", strValues, strFields)
                End If
                '### STILL EMPTY PUT DEFAULT
                If strkey1 = "" Then
                    strkey1 = "User not available"
                End If
            End If
            If strkey1 = "SAP-HR" Then strkey1 = "SAP interface"
            
            
            '### TIME
            strkey1 = strkey1 + "," + CedaFullDateToVb(tab_R01TABUSER.GetColumnValues(l, 4))
            '### FUNCTION
            strkey1 = strkey1 + "," + tab_R01TABUSER.GetColumnValues(l, 5)
            
            strTemp = SortedR01Fields(strValues, strFields)
            
            strkey1 = strkey1 + "," + strTemp
            
            tab_LOGUSER.InsertTextLine strkey1, True
            
        Next l
            
        tab_LOGUSER.ShowHorzScroller True
        tab_LOGUSER.AutoSizeColumns
        tab_LOGUSER.EnableHeaderSizing True
        tab_LOGUSER.Sort 1, True, False
        tab_LOGUSER.Refresh
    
        '### LIST UP ALL DIFFERENT USER LOGINS FOUND IN R01TAB
        Dim tmpAllUSers As String
        Dim tmpSTAFF As String
        tab_ALLUSERS.ResetContent
        If tab_LOGUSER.GetLineCount > 0 Then
            For l = 0 To tab_LOGUSER.GetLineCount Step 1
                If InStr(1, tmpAllUSers, tab_LOGUSER.GetColumnValues(l, 0)) = 0 Then
                    tmpAllUSers = tmpAllUSers + tab_LOGUSER.GetColumnValues(l, 0) + ","
                    strTemp = GetStaffbyPENO(tab_LOGUSER.GetColumnValues(l, 0), tmpSTAFF)
                    
                    strTemp = tab_LOGUSER.GetColumnValues(l, 0) & "," & tmpSTAFF
                    If tab_LOGUSER.GetColumnValues(l, 0) <> "SAP interface" And _
                    tab_LOGUSER.GetColumnValues(l, 0) <> "User not available" _
                    Then
                        tab_ALLUSERS.InsertTextLine strTemp, True
                    End If
                End If
            Next l
        End If
        tab_ALLUSERS.Refresh
        
    Else
        'MsgBox "No transaction logs available"
        tab_LOGUSER.ResetContent
        tab_LOGUSER.Refresh
    End If
     
    StatusBar1.Panels(1).Text = "Loading shift transactions..."
    
    tab_R02TABUSER.ResetContent
    tmpStr = "where time like '" & tmpDay & "%'"
    frmData.LoadData tab_R02TABUSER, tmpStr
    tab_R02TABUSER.AutoSizeColumns
    tab_R02TABUSER.Refresh
    
    lblJOB = "Job record transactions (" & tab_R02TABUSER.GetLineCount & " records)"
    
        '### IF AVAILABLE TRANSLATE INTO READABLE FORMAT
        If tab_R02TABUSER.GetLineCount > 0 Then
            tab_LOGUSER2.ResetContent
            
            '### CONTENT
            For l = 0 To tab_R02TABUSER.GetLineCount - 1 Step 1
                '### USEC
                strkey1 = tab_R02TABUSER.GetColumnValues(l, 3)
                
                '### No user in R01TAB FOUND?
                If strkey1 = "" Or UCase(strkey1) = "EVENTSPOOL" Then
                    '### TAKE USEU
                    strkey1 = GetFieldValue("USEU", strValues, strFields)
                    '### STILL EMPTY TAKE USEC
                    If strkey1 = "" Then
                        strkey1 = GetFieldValue("USEC", strValues, strFields)
                    End If
                    '### STILL EMPTY PUT DEFAULT
                    If strkey1 = "" Then
                        strkey1 = "User not available"
                    End If
                End If
                If strkey1 = "polhdlr" Then strkey1 = "System"
                
                '### TIME
                'strkey1 = strkey1 + "," + tab_R02TAB.GetColumnValues(l, 4)
                strkey1 = strkey1 + "," & DateAdd("h", gUTCOffset, CedaFullDateToVb(tab_R02TABUSER.GetColumnValues(l, 4)))
                '### FUNCTION
                strkey1 = strkey1 + "," + tab_R02TABUSER.GetColumnValues(l, 5)
                
                '### LOG COLUMNS
                strFields = tab_R02TABUSER.GetColumnValues(l, 1)
                strFields = Replace(strFields, Chr(30), ",")
                strValues = tab_R02TABUSER.GetColumnValues(l, 2)
                strValues = Replace(strValues, Chr(30), ",")
                
                strTemp = SortedR02Fields(strValues, strFields)
                
                strkey1 = strkey1 + "," + strTemp
                
                tab_LOGUSER2.InsertTextLine strkey1, False
                
            Next l
                
            tab_LOGUSER2.ShowHorzScroller True
            tab_LOGUSER2.AutoSizeColumns
            tab_LOGUSER2.EnableHeaderSizing True
            tab_LOGUSER2.Refresh
        Else
            'MsgBox "No transaction logs available"
            tab_LOGUSER2.ResetContent
            tab_LOGUSER2.Refresh
        End If
    
        '### LIST UP ALL DIFFERENT USER LOGINS FOUND IN R02TAB
        If tab_LOGUSER2.GetLineCount > 0 Then
            For l = 0 To tab_LOGUSER2.GetLineCount Step 1
                If InStr(1, tmpAllUSers, tab_LOGUSER2.GetColumnValues(l, 0)) = 0 Then
                    tmpAllUSers = tmpAllUSers + tab_LOGUSER2.GetColumnValues(l, 0) + ","
                    strTemp = GetStaffbyPENO(tab_LOGUSER2.GetColumnValues(l, 0), tmpSTAFF)
                    
                    strTemp = tab_LOGUSER2.GetColumnValues(l, 0) & "," & tmpSTAFF
                    If tab_LOGUSER2.GetColumnValues(l, 0) <> "SAP interface" And _
                    tab_LOGUSER2.GetColumnValues(l, 0) <> "User not available" _
                    Then
                        tab_ALLUSERS.InsertTextLine strTemp, True
                    End If
                End If
            Next l
        End If
        tab_ALLUSERS.Refresh
    
    StatusBar1.Panels(1).Text = "Ready."
    MousePointer = vbDefault
    cmdAllUsers.Enabled = True
     
End Sub

Private Sub cmdDEBUG_Click()
    If tab_R01TABUSER.Visible = False Then
        tab_R01TABUSER.Visible = True
        tab_R02TABUSER.Visible = True
        tab_STFCOMPLETE.Visible = True
        tab_R01TAB.Visible = True
        tab_AFTTABall.Visible = True
        tab_JTYTAB.Visible = True
        tab_R02TAB.Visible = True
    Else
        tab_R01TABUSER.Visible = False
        tab_R02TABUSER.Visible = False
        tab_STFCOMPLETE.Visible = False
        tab_R01TAB.Visible = False
        tab_AFTTABall.Visible = False
        tab_JTYTAB.Visible = False
        tab_R02TAB.Visible = False
    End If
End Sub

'#######################################################################
'### Leave application
'#######################################################################

Private Sub cmdExit_Click(Index As Integer)
    
'!!!!!!!!!!!!!!
End
    Dim ilRet
    ilRet = MsgBox("Do you want to exit ?", vbYesNo, "EXIT")
    If ilRet = vbYes Then
        Unload frmData
        End
    End If

End Sub

'#######################################################################
'### INIT DB TABLES
'#######################################################################
Sub InitTabs()

    frmData.InitTabGeneral tab_R01TAB
    frmData.InitTabForCedaConnection tab_R01TAB
    tab_R01TAB.AutoSizeColumns

    frmData.InitTabGeneral tab_R02TAB
    frmData.InitTabForCedaConnection tab_R02TAB
    tab_R02TAB.AutoSizeColumns

    frmData.InitTabGeneral tab_ORGTAB
    frmData.InitTabForCedaConnection tab_ORGTAB
    tab_ORGTAB.AutoSizeColumns
    
    frmData.InitTabGeneral tab_SECORGTAB
    frmData.InitTabForCedaConnection tab_SECORGTAB
    tab_SECORGTAB.AutoSizeColumns
    
    frmData.InitTabGeneral tab_SECTAB
    frmData.InitTabForCedaConnection tab_SECTAB
    tab_SECTAB.AutoSizeColumns
    
    frmData.InitTabGeneral tab_SECTABGRP
    frmData.InitTabForCedaConnection tab_SECTABGRP
    tab_SECTABGRP.AutoSizeColumns
    
    frmData.InitTabGeneral tab_GRPTAB
    frmData.InitTabForCedaConnection tab_GRPTAB
    tab_GRPTAB.AutoSizeColumns
    
    frmData.InitTabGeneral tab_SECTABPRF
    frmData.InitTabForCedaConnection tab_SECTABPRF
    tab_SECTABPRF.AutoSizeColumns
    
    frmData.InitTabGeneral tab_SECTABAPP
    frmData.InitTabForCedaConnection tab_SECTABAPP
    tab_SECTABAPP.AutoSizeColumns
    
    frmData.InitTabGeneral tab_PRVTAB
    frmData.InitTabForCedaConnection tab_PRVTAB
    tab_PRVTAB.AutoSizeColumns
    
    frmData.InitTabGeneral tab_FKTTAB
    frmData.InitTabForCedaConnection tab_FKTTAB
    tab_FKTTAB.AutoSizeColumns

    frmData.InitTabGeneral tab_STFTAB
    frmData.InitTabForCedaConnection tab_STFTAB
    tab_STFTAB.HeaderString = "Staff No.,URNO,Last,First,Nick Name"
    tab_STFTAB.HeaderLengthString = "80,0,100,100,100"
    tab_STFTAB.Refresh
    
    frmData.InitTabGeneral tab_SECSTFTAB
    frmData.InitTabForCedaConnection tab_SECSTFTAB
    tab_SECSTFTAB.HeaderString = "PENO.,URNO,Last,First"
    tab_SECSTFTAB.HeaderLengthString = "80,0,100,100"
    tab_SECSTFTAB.Refresh


    frmData.InitTabGeneral tab_STFCOMPLETE
    frmData.InitTabForCedaConnection tab_STFCOMPLETE
    tab_STFCOMPLETE.AutoSizeColumns

    frmData.InitTabGeneral tab_DRRTAB
    frmData.InitTabForCedaConnection tab_DRRTAB
    tab_DRRTAB.HeaderString = "URNO,Day,Level,Status,Shift,Shift(old),Send-Flag"
    tab_DRRTAB.HeaderLengthString = "0,80,60,60,100,100,100"
    
    frmData.InitTabGeneral tab_ALTTAB
    frmData.InitTabForCedaConnection tab_ALTTAB
    tab_ALTTAB.AutoSizeColumns
    
    'frmData.InitTabGeneral tab_ALTTABJOB
    'frmData.InitTabForCedaConnection tab_ALTTABJOB
    'tab_ALTTABJOB.AutoSizeColumns

    frmData.InitTabGeneral tab_AFTTAB
    frmData.InitTabForCedaConnection tab_AFTTAB
    tab_AFTTAB.HeaderString = "URNO,Flight,A/D,STOA,STOD,Best TD,Best TA,Gate,ORG,DES"
    tab_AFTTAB.HeaderLengthString = "0,60,25,140,140,140,140,25,25,25"
    tab_AFTTAB.HeaderFontSize = 14
    tab_AFTTAB.FontSize = 14
    tab_AFTTAB.Refresh
    
    frmData.InitTabGeneral tab_AFTTABall
    frmData.InitTabForCedaConnection tab_AFTTABall
    'tab_AFTTAB.HeaderString = "URNO,Flight,A/D,STOA,STOD,Best TD,Best TA,ORG,DES,ALC2"
    'tab_AFTTAB.HeaderLengthString = "0,70,35,200,200,100,100,100,100,100"
    tab_AFTTABall.Refresh
    
    frmData.InitTabGeneral tab_JOBTAB
    frmData.InitTabForCedaConnection tab_JOBTAB
    tab_JOBTAB.HeaderString = "URNO,Actual from,Actual to,Planned from,Planned to,UAFT,UJTY,UDSR,USTF"
    tab_JOBTAB.HeaderLengthString = "0,140,140,140,140,0,0,0,0"
    tab_JOBTAB.HeaderFontSize = 14
    tab_JOBTAB.FontSize = 14
    tab_JOBTAB.Refresh
    'tab_JOBTAB.AutoSizeColumns
    
    frmData.InitTabGeneral tab_JTYTAB
    frmData.InitTabForCedaConnection tab_JTYTAB
    tab_JTYTAB.AutoSizeColumns
    
    frmData.InitTabGeneral tab_JTYTAB1
    frmData.InitTabForCedaConnection tab_JTYTAB1
    tab_JTYTAB1.HeaderString = "URNO,JOB TYPES"
    tab_JTYTAB1.HeaderLengthString = "0,500"
    tab_JTYTAB1.Refresh
    'tab_JTYTAB1.AutoSizeColumns
    
    frmData.InitTabGeneral tab_TPLTAB
    frmData.InitTabForCedaConnection tab_TPLTAB
    tab_TPLTAB.HeaderString = "URNO,TEMPLATES,FLIGHT RULE"
    tab_TPLTAB.HeaderLengthString = "0,100,200"
    'tab_TPLTAB.HeaderFontSize = 14
    'tab_TPLTAB.FontSize = 14
    tab_TPLTAB.Refresh
    'tab_TPLTAB.AutoSizeColumns
    
    frmData.InitTabGeneral tab_STFJOBS
    frmData.InitTabForCedaConnection tab_STFJOBS
    tab_STFJOBS.HeaderFontSize = 14
    tab_STFJOBS.FontSize = 14
    tab_STFJOBS.HeaderString = "URNO,Actual from,Actual to,Planned from,Planned to,UAFT,UJTY,UDSR,USTF"
    tab_STFJOBS.HeaderLengthString = "0,140,140,140,140,0,0,0,0"
    'tab_STFJOBS.AutoSizeColumns
    
    frmData.InitTabGeneral tab_SYSDRR
    frmData.InitTabForCedaConnection tab_SYSDRR
    frmData.LoadData tab_SYSDRR, "where tana='DRR' and logd='R01TAB'"
    tab_SYSDRR.HeaderString = "Field,Description"
    tab_SYSDRR.Sort 0, True, False
    tab_SYSDRR.AutoSizeColumns
    
    
    frmData.InitTabGeneral tab_R01TABUSER
    frmData.InitTabForCedaConnection tab_R01TABUSER
    tab_R01TABUSER.AutoSizeColumns
    
    frmData.InitTabGeneral tab_R02TABUSER
    frmData.InitTabForCedaConnection tab_R02TABUSER
    tab_R02TABUSER.AutoSizeColumns
    
    frmData.InitTabGeneral tab_SERTAB
    frmData.InitTabForCedaConnection tab_SERTAB
    tab_SERTAB.AutoSizeColumns
    
    tab_LOGJOB.FontName = "Courier New"
    tab_LOGJOB.HeaderFontSize = 14
    tab_LOGJOB.FontSize = 14
    
    'tab_LOG.FontName = "Courier New"
    'tab_LOG.HeaderFontSize = 14
    'tab_LOG.FontSize = 14
    
    
    tab_ALLUSERS.FontName = "Courier New"
    tab_ALLUSERS.HeaderFontSize = 16
    tab_ALLUSERS.FontSize = 14
    tab_ALLUSERS.HeaderString = "Login,Name"
    tab_ALLUSERS.HeaderLengthString = "100,250"
    tab_ALLUSERS.ResetContent
    tab_ALLUSERS.Refresh
    
End Sub

Private Sub cmdExpProfile_Click()
    
    Dim l As Integer
    Dim i As Long
    Dim j As Integer
    Dim llLineCount As Integer
    Dim strTemp As String
    Dim FileNumber
    Dim secUrno As String
    Dim secUrno1 As String
    Dim fktUrno As String
    Dim prvUrno As String
    Dim lineNum As String
    Dim functionStr As String
    Dim functionVal As String
    Dim strWhere As String
    Dim secStrWhere As String
    Dim prvtabList As String
    Dim prevApp As String
    Dim nextApp As String
    Dim i1Cnt As Long
    
    Dim grpKey As String
    Dim profileStr As String
    Dim grpLine As String
    Dim grpUrno As String
    Dim grpType As String
        
    
    MousePointer = vbHourglass
    ClearDetails
        
    
    'Get all the required information from DB.
    'Below piece of code to retrieve valid groups & profiles from SECTAB
    secStrWhere = "where stat='1' and type='G' or type='P'"
    tab_SECTABGRP.ResetContent
    tab_SECTABGRP.Refresh
    frmData.LoadData tab_SECTABGRP, secStrWhere
    
    'Below piece of code to retrieve valid groups.
    secStrWhere = ""
    tab_GRPTAB.ResetContent
    tab_GRPTAB.Refresh
    frmData.LoadData tab_GRPTAB, secStrWhere
    
    'Below piece of code to retrieve valid groups.
    secStrWhere = ""
    tab_GRPTAB.ResetContent
    tab_GRPTAB.Refresh
    frmData.LoadData tab_GRPTAB, secStrWhere
    
    'Below piece of code to retrieve valid Profiles
    secStrWhere = "where type='P' and stat='1'"
    tab_SECTABPRF.ResetContent
    tab_SECTABPRF.Refresh
    frmData.LoadData tab_SECTABPRF, secStrWhere
    
    'Below piece of code to retrieve valid Application Names
    secStrWhere = "where type='A' and stat='1'"
    tab_SECTABAPP.ResetContent
    tab_SECTABAPP.Refresh
    frmData.LoadData tab_SECTABAPP, secStrWhere
    
    'Below piece of code to retrieve valid Profile functions
    'secStrWhere = "order by fsec,fapp"
    'tab_PRVTAB.ResetContent
    'tab_PRVTAB.Refresh
    'frmData.LoadData tab_PRVTAB, secStrWhere
    'MsgBox (tab_PRVTAB.GetLineCount)
    
    
    'Below piece of code to retrieve all valid functions
    secStrWhere = ""
    tab_FKTTAB.ResetContent
    tab_FKTTAB.Refresh
    frmData.LoadData tab_FKTTAB, secStrWhere
    'MsgBox (tab_FKTTAB.GetLineCount)
    
    
    
    'Initialize export to excel requirements.
    dlgSave1.DialogTitle = "Save as Excel"
    dlgSave1.Filter = "Excel (CSV)|*.csv"
    dlgSave1.FileName = "Profiles-Groups in BDPSSEC"
    dlgSave1.ShowSave
    FileNumber = FreeFile   ' Get unused file number.
    Open dlgSave1.FileName For Output As #FileNumber    ' Create file name.
    Print #FileNumber, "Existing Profiles & Groups in BDPSSEC"
    Print #FileNumber, " , "
    Print #FileNumber, " Profiles  ,  , , , , , Functions Status: 1 = Activated; - = Hidden; 0  = Deactivated"
    Print #FileNumber, "   ,  "
    
    
    
    'Prepare for every profile, all the valid applications & functions
    llLineCount = tab_SECTABPRF.GetLineCount
    For l = 0 To llLineCount Step 1
        secUrno = tab_SECTABPRF.GetColumnValues(l, 0)
        'prvtabList = tab_PRVTAB.GetLinesByColumnValue(0, secUrno, 0)
        'ilCnt = ItemCount(prvtabList, ",")
        
        
        Print #FileNumber, "" + tab_SECTABPRF.GetColumnValues(l, 1) + "  ,  "
        
        functionsStr = ",Function Name:,"
        functionsVal = ",Status:,"
        prevApp = ""
        nextApp = ""
        
        secStrWhere = "where fsec=" + secUrno + " order by fapp"
        tab_PRVTAB.ResetContent
        tab_PRVTAB.Refresh
        frmData.LoadData tab_PRVTAB, secStrWhere
        
        
        i1Cnt = tab_PRVTAB.GetLineCount
        prevApp = tab_PRVTAB.GetColumnValues(0, 0)
        For i = 0 To i1Cnt Step 1
            ' kkh fixed on 20/11/2007
            'fktUrno = tab_PRVTAB.GetColumnValues(i, 1)
            fktUrno = tab_PRVTAB.GetColumnValues(i, 2)
            nextApp = tab_PRVTAB.GetColumnValues(i, 0)
            
            
            If prevApp <> nextApp Then
                secUrno1 = tab_SECTABAPP.GetLinesByColumnValue(0, prevApp, 0)
                If IsNumeric(secUrno1) Then Print #FileNumber, "," & tab_SECTABAPP.GetColumnValues(secUrno1, 1) & ",  "
                Print #FileNumber, functionsStr
                Print #FileNumber, functionsVal
                functionsStr = ",Function Name:,"
                functionsVal = ",Status:,"
                Print #FileNumber, ", , "
            End If
                        
            fktUrno = tab_FKTTAB.GetLinesByColumnValue(0, fktUrno, 0)
            
            If IsNumeric(fktUrno) Then functionsStr = functionsStr + tab_FKTTAB.GetColumnValues(fktUrno, 2) + ","
            ' kkh fixed on 20/11/2007
            'functionsVal = functionsVal + tab_PRVTAB.GetColumnValues(i, 2) + ","
            functionsVal = functionsVal + tab_PRVTAB.GetColumnValues(i, 3) + ","
            
            
            prevApp = nextApp
        Next i
        
        
        'For i = 1 To ilCnt Step 1
        '    prvLine = GetItem(prvtabList, i, ",")
        '    fktUrno = tab_PRVTAB.GetColumnValues(prvLine, 2)
        '    nextApp = tab_PRVTAB.GetColumnValues(prvLine, 1)
            
        '    If prevApp <> nextApp Then
        '        secUrno1 = tab_SECTABAPP.GetLinesByColumnValue(0, nextApp, 0)
        '        Print #FileNumber, "," + tab_SECTABAPP.GetColumnValues(secUrno1, 1) + "  ,  "
        '        Print #FileNumber, ",  ,  "
        '    Else
        '        fktUrno = tab_FKTTAB.GetLinesByColumnValue(0, fktUrno, 0)
        '        functionsStr = functionsStr + tab_FKTTAB.GetColumnValues(fktUrno, 1) + ","
        '        functionsVal = functionsVal + tab_PRVTAB.GetColumnValues(prvLine, 3) + ","
        '    End If
        '    prevApp = nextApp
        'Next i
          
    Next l
    
    'Groups
    'Below piece of code to retrieve valid Profiles
    secStrWhere = "where type='G' and stat='1'"
    tab_SECTABPRF.ResetContent
    tab_SECTABPRF.Refresh
    frmData.LoadData tab_SECTABPRF, secStrWhere
    
    Print #FileNumber, " Groups  ,  , , , , , Functions Status: 1 = Activated; - = Hidden; 0  = Deactivated"
    Print #FileNumber, "   ,  "
    
    'Prepare for every profile, all the valid applications & functions
    llLineCount = tab_SECTABPRF.GetLineCount
    For l = 0 To llLineCount Step 1
        secUrno = tab_SECTABPRF.GetColumnValues(l, 0)
        'prvtabList = tab_PRVTAB.GetLinesByColumnValue(0, secUrno, 0)
        'ilCnt = ItemCount(prvtabList, ",")
        
        
        Print #FileNumber, "" + tab_SECTABPRF.GetColumnValues(l, 1) + "  ,  "
        
        secStrWhere = "where fsec=" + secUrno + " order by fapp"
        tab_PRVTAB.ResetContent
        tab_PRVTAB.Refresh
        frmData.LoadData tab_PRVTAB, secStrWhere
        
        If tab_PRVTAB.GetLineCount > 0 Then
        
            functionsStr = ",Function Name:,"
            functionsVal = ",Status:,"
            prevApp = ""
            nextApp = ""
            
            i1Cnt = tab_PRVTAB.GetLineCount
            prevApp = tab_PRVTAB.GetColumnValues(0, 0)
            For i = 0 To i1Cnt Step 1
                ' kkh fixed on 20/11/2007
                'fktUrno = tab_PRVTAB.GetColumnValues(i, 1)
                fktUrno = tab_PRVTAB.GetColumnValues(i, 2)
                nextApp = tab_PRVTAB.GetColumnValues(i, 0)
                
                If prevApp <> nextApp Then
                    secUrno1 = tab_SECTABAPP.GetLinesByColumnValue(0, prevApp, 0)
                    If IsNumeric(secUrno1) Then Print #FileNumber, "," & tab_SECTABAPP.GetColumnValues(secUrno1, 1) & ",  "
                    Print #FileNumber, functionsStr
                    Print #FileNumber, functionsVal
                    functionsStr = ",Function Name:,"
                    functionsVal = ",Status:,"
                    Print #FileNumber, ", , "
                End If
                
                
                fktUrno = tab_FKTTAB.GetLinesByColumnValue(0, fktUrno, 0)
                If IsNumeric(fktUrno) Then functionsStr = functionsStr + tab_FKTTAB.GetColumnValues(fktUrno, 2) + ","
                ' kkh fixed on 20/11/2007
                'functionsVal = functionsVal + tab_PRVTAB.GetColumnValues(i, 2) + ","
                functionsVal = functionsVal + tab_PRVTAB.GetColumnValues(i, 3) + ","
                    
                prevApp = nextApp
            Next i
        
        Else
        
            'To get all the groups & profiles
            grpKey = tab_GRPTAB.GetLinesByColumnValue(0, secUrno, 0)
            profileStr = ""
            
            ilCnt = ItemCount(grpKey, ",")
            For j = 1 To ilCnt Step 1
                grpLine = GetItem(grpKey, j, ",")
                grpUrno = tab_GRPTAB.GetColumnValues(grpLine, 1)
                
                If IsNumeric(tab_SECTABGRP.GetLinesByColumnValue(0, grpUrno, 0)) Then
                    
                    grpUrno = tab_SECTABGRP.GetLinesByColumnValue(0, grpUrno, 0)
                    grpType = tab_SECTABGRP.GetColumnValue(grpUrno, 3)
                    
                    If grpType = "P" Then
                        profileStr = profileStr + tab_SECTABGRP.GetColumnValue(grpUrno, 1) + ";"
                    End If
                    
                End If
                
            Next j
            
            Print #FileNumber, "" + tab_SECTABPRF.GetColumnValues(l, 1) + "  Group: Belongs to Profile :" + profileStr
            
        End If
    
    Next l
        
    Close #FileNumber
    MousePointer = vbDefault
    
End Sub
Private Sub cmdExpProfile1_Click()
    
    Dim l As Integer
    Dim i As Long
    Dim j As Integer
    Dim llLineCount As Integer
    Dim strTemp As String
    Dim FileNumber
    Dim secUrno As String
    Dim secUrno1 As String
    Dim fktUrno As String
    Dim prvUrno As String
    Dim lineNum As String
    Dim functionStr As String
    Dim functionVal As String
    Dim strWhere As String
    Dim secStrWhere As String
    Dim prvtabList As String
    Dim prevApp As String
    Dim nextApp As String
    Dim i1Cnt As Long
    
    Dim grpKey As String
    Dim profileStr As String
    Dim grpLine As String
    Dim grpUrno As String
    Dim grpType As String
        
    
    MousePointer = vbHourglass
    ClearDetails
        
    
    'Get all the required information from DB.
    'Below piece of code to retrieve valid groups & profiles from SECTAB
    secStrWhere = "where stat='1' and type='G' or type='P'"
    tab_SECTABGRP.ResetContent
    tab_SECTABGRP.Refresh
    frmData.LoadData tab_SECTABGRP, secStrWhere
    
    'Below piece of code to retrieve valid groups.
    secStrWhere = ""
    tab_GRPTAB.ResetContent
    tab_GRPTAB.Refresh
    frmData.LoadData tab_GRPTAB, secStrWhere
    
    'Below piece of code to retrieve valid groups.
    secStrWhere = ""
    tab_GRPTAB.ResetContent
    tab_GRPTAB.Refresh
    frmData.LoadData tab_GRPTAB, secStrWhere
    
    'Below piece of code to retrieve valid Profiles
    secStrWhere = "where type='P' and stat='1'"
    tab_SECTABPRF.ResetContent
    tab_SECTABPRF.Refresh
    frmData.LoadData tab_SECTABPRF, secStrWhere
    
    'Below piece of code to retrieve valid Application Names
    secStrWhere = "where type='A' and stat='1'"
    tab_SECTABAPP.ResetContent
    tab_SECTABAPP.Refresh
    frmData.LoadData tab_SECTABAPP, secStrWhere
    
    'Below piece of code to retrieve valid Profile functions
    'secStrWhere = "order by fsec,fapp"
    'tab_PRVTAB.ResetContent
    'tab_PRVTAB.Refresh
    'frmData.LoadData tab_PRVTAB, secStrWhere
    'MsgBox (tab_PRVTAB.GetLineCount)
    
    
    'Below piece of code to retrieve all valid functions
    secStrWhere = ""
    tab_FKTTAB.ResetContent
    tab_FKTTAB.Refresh
    frmData.LoadData tab_FKTTAB, secStrWhere
    'MsgBox (tab_FKTTAB.GetLineCount)
    
    
    
    'Initialize export to excel requirements.
    dlgSave2.DialogTitle = "Save as Excel"
    dlgSave2.Filter = "Excel (CSV)|*.csv"
    dlgSave2.FileName = "Profiles-Groups in BDPSSEC"
    dlgSave2.ShowSave
    FileNumber = FreeFile   ' Get unused file number.
    Open dlgSave2.FileName For Output As #FileNumber    ' Create file name.
    Print #FileNumber, "Existing Profiles & Groups in BDPSSEC"
    Print #FileNumber, " , "
    Print #FileNumber, " Profiles  ,  , Applications, , , , "
    Print #FileNumber, "   ,  , "
    
    
    
    'Prepare for every profile, all the valid applications & functions
    llLineCount = tab_SECTABPRF.GetLineCount
    For l = 0 To llLineCount Step 1
        secUrno = tab_SECTABPRF.GetColumnValues(l, 0)
        
        Print #FileNumber, "" + tab_SECTABPRF.GetColumnValues(l, 1) + "  ,  "
        
        prevApp = ""
        nextApp = ""
        
        secStrWhere = "where fsec=" + secUrno + " order by fapp"
        tab_PRVTAB.ResetContent
        tab_PRVTAB.Refresh
        frmData.LoadData tab_PRVTAB, secStrWhere
        
        
        i1Cnt = tab_PRVTAB.GetLineCount
        prevApp = tab_PRVTAB.GetColumnValues(0, 0)
        For i = 0 To i1Cnt Step 1
            fktUrno = tab_PRVTAB.GetColumnValues(i, 1)
            nextApp = tab_PRVTAB.GetColumnValues(i, 0)
            
            
            If prevApp <> nextApp Then
                secUrno1 = tab_SECTABAPP.GetLinesByColumnValue(0, prevApp, 0)
                If IsNumeric(secUrno1) Then Print #FileNumber, ",," & tab_SECTABAPP.GetColumnValues(secUrno1, 1) & ",  "
            End If
            
            prevApp = nextApp
        Next i
    Next l
    
    'Groups
    'Below piece of code to retrieve valid Profiles
    secStrWhere = "where type='G' and stat='1'"
    tab_SECTABPRF.ResetContent
    tab_SECTABPRF.Refresh
    frmData.LoadData tab_SECTABPRF, secStrWhere
    
    Print #FileNumber, " Groups  ,  , , , , , "
    Print #FileNumber, "   ,  "
    
    'Prepare for every profile, all the valid applications & functions
    llLineCount = tab_SECTABPRF.GetLineCount
    For l = 0 To llLineCount Step 1
        secUrno = tab_SECTABPRF.GetColumnValues(l, 0)
    
        Print #FileNumber, "" + tab_SECTABPRF.GetColumnValues(l, 1) + "  ,  "
        
        secStrWhere = "where fsec=" + secUrno + " order by fapp"
        tab_PRVTAB.ResetContent
        tab_PRVTAB.Refresh
        frmData.LoadData tab_PRVTAB, secStrWhere
        
        If tab_PRVTAB.GetLineCount > 0 Then
            prevApp = ""
            nextApp = ""
            
            i1Cnt = tab_PRVTAB.GetLineCount
            prevApp = tab_PRVTAB.GetColumnValues(0, 0)
            For i = 0 To i1Cnt Step 1
                nextApp = tab_PRVTAB.GetColumnValues(i, 0)
                
                If prevApp <> nextApp Then
                    secUrno1 = tab_SECTABAPP.GetLinesByColumnValue(0, prevApp, 0)
                    If IsNumeric(secUrno1) Then Print #FileNumber, ",," & tab_SECTABAPP.GetColumnValues(secUrno1, 1) & ",  "
                End If
                    
                prevApp = nextApp
            Next i
        
        Else
        
            'To get all the groups & profiles
            grpKey = tab_GRPTAB.GetLinesByColumnValue(0, secUrno, 0)
            profileStr = ""
            
            ilCnt = ItemCount(grpKey, ",")
            For j = 1 To ilCnt Step 1
                grpLine = GetItem(grpKey, j, ",")
                grpUrno = tab_GRPTAB.GetColumnValues(grpLine, 1)
                
                If IsNumeric(tab_SECTABGRP.GetLinesByColumnValue(0, grpUrno, 0)) Then
                    
                    grpUrno = tab_SECTABGRP.GetLinesByColumnValue(0, grpUrno, 0)
                    grpType = tab_SECTABGRP.GetColumnValue(grpUrno, 3)
                    
                    If grpType = "P" Then
                        profileStr = profileStr + tab_SECTABGRP.GetColumnValue(grpUrno, 1) + ";"
                    End If
                    
                End If
                
            Next j
            
            Print #FileNumber, "" + tab_SECTABPRF.GetColumnValues(l, 1) + "  Group: Belongs to Profile :" + profileStr
            
        End If
    
    Next l
        
    Close #FileNumber
    MousePointer = vbDefault
    
End Sub

Private Sub cmdExpStf_Click()
    
    Dim l As Integer
    Dim i As Integer
    Dim llLineCount As Integer
    Dim strTemp As String
    Dim FileNumber
    Dim k As Integer
    Dim columnCount As Integer
    Dim appStr As String
    Dim appCount As Integer
    
    
    l = tab_SECORGTAB.GetCurrentSelected
    
    If l <> -1 Then
    
        MousePointer = vbHourglass
        ClearDetails
        
        dlgSave.DialogTitle = "Save as Excel"
        dlgSave.Filter = "Excel (CSV)|*.csv"
        dlgSave.FileName = "OrgUnit-" & tab_SECORGTAB.GetColumnValues(l, 0) & " Staff"
        dlgSave.ShowSave
    
        If dlgSave.FileName <> "" Then
            
            llLineCount = tab_STFLOG.GetLineCount
            
            If llLineCount < 1 Then
                MsgBox "No Data to export.", vbExclamation
                MousePointer = vbDefault
                Exit Sub
            End If
            
            FileNumber = FreeFile   ' Get unused file number.
            Open dlgSave.FileName For Output As #FileNumber    ' Create file name.
            
            
            appStr = "PENO,Last,First,Groups,Profiles,"
            appCount = tab_SECTABAPP.GetLineCount
            For k = 0 To appCount - 1 Step 1
                    appName = tab_SECTABAPP.GetColumnValue(k, 1)
                    appStr = appStr + appName + ","
            Next k
            
            Print #FileNumber, appStr
            Print #FileNumber, " ,  , , , , ,  "
            
        
            columnCount = tab_STFLOG.GetColumnCount
            
            For l = 0 To llLineCount - 1 Step 1
                strTemp = ""
                For k = 0 To columnCount - 1 Step 1
                    If k <> 1 Then strTemp = strTemp + tab_STFLOG.GetColumnValues(l, k) + ","
                Next k
                Print #FileNumber, strTemp
            Next l
            
            Print #FileNumber, " ,  ,  "
            Print #FileNumber, " Total Staff :  " & llLineCount
            
            Close #FileNumber
            
        Else
            MsgBox ("Please enter filename to export")
        End If
    Else
        MsgBox ("Please select org unit")
    End If
    
    MousePointer = vbDefault
    
End Sub

'#######################################################################
'### Sitch Field definition help on/off
'#######################################################################
Private Sub cmdFields_Click(Index As Integer)
    If tab_SYSDRR.Visible = True Then tab_SYSDRR.Visible = False Else tab_SYSDRR.Visible = True
End Sub

'#######################################################################
'###
'#######################################################################
Sub TranslateFlights()
    
    Dim tmpStr As String
    Dim Adid As String
    Dim l As Long
    
    If tab_AFTTAB.GetLineCount > 0 Then
        For l = 0 To tab_AFTTAB.GetLineCount - 1 Step 1
            Adid = tab_AFTTAB.GetColumnValues(l, 2)
            If Adid = "A" Then
                tab_AFTTAB.SetColumnValue l, 4, ""
                tab_AFTTAB.SetColumnValue l, 5, ""
                tab_AFTTAB.SetColumnValue l, 3, DateAdd("h", gUTCOffset, CedaFullDateToVb(tab_AFTTAB.GetColumnValues(l, 3)))
                tab_AFTTAB.SetColumnValue l, 6, DateAdd("h", gUTCOffset, CedaFullDateToVb(tab_AFTTAB.GetColumnValues(l, 6)))
            Else
                tab_AFTTAB.SetColumnValue l, 3, ""
                tab_AFTTAB.SetColumnValue l, 6, ""
                tab_AFTTAB.SetColumnValue l, 4, DateAdd("h", gUTCOffset, CedaFullDateToVb(tab_AFTTAB.GetColumnValues(l, 4)))
                tab_AFTTAB.SetColumnValue l, 5, DateAdd("h", gUTCOffset, CedaFullDateToVb(tab_AFTTAB.GetColumnValues(l, 5)))
            End If
        Next l
    End If
End Sub

'#######################################################################
'###
'#######################################################################
Private Sub cmdLoadFlights_Click()
   
    Dim strkey1 As String
    Dim strWhere As String
    Dim l As Long
    Dim findDate As String
    
    cmResetFLIGHT_Click
    
    If txtSearchFlightUrno <> "" Then
    
        tab_AFTTAB.ResetContent
        tab_AFTTAB.Refresh
        frmData.LoadData tab_AFTTAB, "where urno=" & txtSearchFlightUrno
        tab_AFTTAB.Refresh
        
        If tab_AFTTAB.GetLineCount = 0 Then MsgBox "No flights found."
        frmFlights = tab_AFTTAB.GetLineCount & " flight(s)"

        Exit Sub
    End If
    
    findDate = Format(calFLDA.Year, "0000") & Format(calFLDA.Month, "00") & Format(calFLDA.Day, "00")
    
    If chkArrival.Value = 0 And chkDeparture.Value = 0 Then
        MsgBox "Both Arrival and Departure not selected."
        Exit Sub
    End If
    
    If findALC2 = "" And findFLNO = "" Then
        MsgBox "No airline or flightnumber specified."
        Exit Sub
    End If
    
    StatusBar1.Panels(1).Text = "Loading flights..."
    MousePointer = vbHourglass
    
    findALC2 = UCase(findALC2)
        
    tab_AFTTAB.ResetContent
    tab_AFTTAB.Refresh
    
    tab_JOBTAB.ResetContent
    tab_JOBTAB.Refresh
    
    tab_LOGJOB.ResetContent
    tab_LOGJOB.Refresh
    
    
    If chkArrival.Value = 1 Then
        If findALC2 <> "" Then strWhere = "where STOA like '" & findDate & "%' and alc2 like '" & findALC2 & "%' and adid='A'"
        If findFLNO <> "" Then strWhere = "where STOA like '" & findDate & "%' and alc2 like '" & findALC2 & "%' and adid='A'"
        If findALC2 <> "" And findFLNO <> "" Then strWhere = "where STOA like '" & findDate & "%' and flno like '" & findALC2 & " " & findFLNO & "%' and adid='A'"
        If findALC2 = "" And findFLNO <> "" Then strWhere = "where STOA like '" & findDate & "%' and flno like '%" & findFLNO & "%' and adid='A'"
        frmData.LoadData tab_AFTTAB, strWhere
    End If
    
    If chkDeparture.Value = 1 Then
        If findALC2 <> "" Then strWhere = "where STOD like '" & findDate & "%' and alc2 like '" & findALC2 & "%' and adid='D'"
        If findFLNO <> "" Then strWhere = "where STOD like '" & findDate & "%' and alc2 like '" & findALC2 & "%' and adid='D'"
        If findALC2 <> "" And findFLNO <> "" Then strWhere = "where STOD like '" & findDate & "%' and flno like '" & findALC2 & " " & findFLNO & "%' and adid='D'"
        If findALC2 = "" And findFLNO <> "" Then strWhere = "where STOD like '" & findDate & "%' and flno like '%" & findFLNO & "%' and adid='D'"
        frmData.LoadData tab_AFTTAB, strWhere
    End If
     
    tab_AFTTABall.ResetContent
    tab_AFTTABall.Refresh
    frmData.LoadData tab_AFTTABall, "where STOD like '" & findDate & "%' or STOA like '" & findDate & "%'"
    tab_AFTTABall.Refresh
     
    If tab_AFTTAB.GetLineCount = 0 Then MsgBox "No flights found."
    frmFlights = tab_AFTTAB.GetLineCount & " flight(s)"
    
    TranslateFlights
    
    tab_AFTTAB.Sort 1, True, False
    tab_AFTTAB.Refresh
    
    StatusBar1.Panels(1).Text = "Ready."
    MousePointer = vbDefault
    
End Sub

'#######################################################################
'### When user clicks "Load shift' button, after selecting staff data
'#######################################################################
Private Sub cmdLoadShift_Click()
    calSDAY_DblClick
End Sub

'#######################################################################
'### RESET FOR NEW SEARCH
'#######################################################################
Private Sub cmdReset_Click()
    ClearDetails
    findPENO = ""
    findLANM = ""
    findFINM = ""
    
    lblNoLog.Caption = ""
    
    tab_STFTAB.ResetContent
    tab_STFTAB.Refresh
    
    tab_LOG.ResetContent
    tab_LOG.Refresh
    
    tab_DRRTAB.ResetContent
    tab_DRRTAB.Refresh
    
    
End Sub

Private Sub cmdResetUserTrans_Click()
    
    calUser.toDay
    
    tab_ALLUSERS.ResetContent
    tab_ALLUSERS.Refresh
    
    tab_LOGUSER.ResetContent
    tab_LOGUSER.Refresh
    
    tab_LOGUSER2.ResetContent
    tab_LOGUSER2.Refresh
    
    lblSHIFT = "Shift record transactions"
    lblJOB = "Job record transactions"
    
End Sub

'#######################################################################
'### FIND SPECIFIC STAFF
'#######################################################################
Private Sub cmdSearch_Click()
    
    Dim strWhere  As String

    If txtfindSTFUrno <> "" Then
    MsgBox (txtfindSTFUrno)
        tab_STFTAB.ResetContent
        '
        frmData.LoadData tab_STFTAB, "where urno=" & txtfindSTFUrno
        tab_STFTAB.Refresh
        If tab_STFTAB.GetLineCount = 0 Then MsgBox "No staff found with this urno."
        Exit Sub
    End If
    
    'If findPENO <> "" Or findLANM <> "" Or findFINM <> "" Or nickName <> "" Then
    '    strWhere = "where "
    '    If findPENO <> "" Then strWhere = strWhere & "peno like '" & findPENO & "%'"
    '    If findLANM <> "" Then strWhere = strWhere & " and lanm like '" & findLANM & "%'"
    '    If findFINM <> "" Then strWhere = strWhere & " and finm like '" & findFINM & "%'"
    '    If nickName <> "" Then strWhere = strWhere & " and shnm like '" & nickName & "%'"
    'Else
    '    MsgBox "No search criteria entered."
    'End If
    
    If findPENO <> "" Or nickName <> "" Then
    
        If findPENO <> "" Then
            strWhere = "where "
            strWhere = strWhere & "peno like '" & findPENO & "%'"
        End If
        
        If nickName <> "" Then
            strWhere = "where "
            strWhere = strWhere & " shnm like '" & nickName & "%'"
        End If
        
        If findPENO <> "" And nickName <> "" Then
            strWhere = "where "
            strWhere = strWhere & "peno like '" & findPENO & "%'"
            strWhere = strWhere & " and shnm like '" & nickName & "%'"
        End If
        
        
        MousePointer = vbHourglass
        tab_STFTAB.ResetContent
        cmdSearch.Enabled = False
                
        frmData.LoadData tab_STFTAB, strWhere
        tab_STFTAB.Refresh
        
        If tab_STFTAB.GetLineCount > 0 Then
            lblNoStaff.Caption = "Number of Staff: " & tab_STFTAB.GetLineCount
            tab_STFTAB.SetFocus
        Else
            lblNoStaff.Caption = "No staff records found."
        End If
        
        cmdSearch.Enabled = True
        MousePointer = vbDefault
    Else
        MsgBox "No search criteria entered."
    End If
End Sub


'#######################################################################
'###
'#######################################################################
Private Sub cmdSearchUser_Click()
    
    Dim tmpDay As String
    Dim tmpStr As String

    tmpDay = Format(calUser.Year, "0000") & Format(calUser.Month, "00") & Format(calUser.Day, "00")

    If txtSearchUser <> "" Then
    
        tab_R01TABUSER.ResetContent
    
        tmpStr = "where usec like '" & txtSearchUser & "%' and time like '" & tmpDay & "%'"
        frmData.LoadData tab_R01TABUSER, tmpStr
        tab_R01TABUSER.AutoSizeColumns
        tab_R01TABUSER.Refresh
    Else
        MsgBox "No staff login specified."
    End If

End Sub

Private Sub cmdShowLog_Click()

    If txtJobUrno <> "" Then
        showTransactionJOBTAB txtJobUrno
    End If
    
End Sub

'#######################################################################
'### SHOW LINKED SHIFT AFTER SELECTING JOBS
'#######################################################################
Private Sub cmdShowRoster_Click()
    
    cmdReset_Click
    SSTAB1.TAB = 2
    calSDAY = calFLDA
    findPENO = txtStfPeno
    cmdSearch_Click
    tab_STFTAB.SetCurrentSelection (0)
    tab_STFTAB_SendLButtonClick 0, 0
    calSDAY_DblClick
    
End Sub

'#######################################################################
'### TEST FUNCTION TO TRIGGER TESTS DURING DEVELOPMENT
'#######################################################################
Private Sub cmdTest_Click()
    Dim X As Integer
    
    Dim c  As String
    c = CedaFullDateToVb("20050911122500")
    
    
    X = InStr(1, "ABCD", "CDD", vbTextCompare)
    MsgBox X
End Sub


'#######################################################################
'### TEST FUNCTION TO TRIGGER TESTS DURING DEVELOPMENT
'#######################################################################
Private Sub cmdTest2_Click()


End Sub


'#######################################################################
'### DISPLAY TRANSACTIONS FOR SELECTED JOB UNDER FLIGHT JOBS
'#######################################################################
Private Sub cmdTransactionsJob_Click()
    
    If tab_JOBTAB.GetCurrentSelected <> -1 Then
        showTransactionJOBTAB tab_JOBTAB.GetColumnValues(tab_JOBTAB.GetCurrentSelected, 0)
    End If

End Sub

'#######################################################################
'###
'#######################################################################
Private Sub cmResetFLIGHT_Click()
       
    tab_AFTTAB.ResetContent
    tab_AFTTAB.Refresh
    
    tab_JOBTAB.ResetContent
    tab_JOBTAB.Refresh
    
    tab_STFJOBS.ResetContent
    tab_STFJOBS.Refresh
    
    
    tab_LOGJOB.ResetContent
    tab_LOGJOB.Refresh
    
    frmFlights = "0 flights"
    
    txtFlight = ""
    lblAdid = ""
    txtORGDES = ""
    txtUrno = ""
    txtBEST = ""
    txtSTO = ""
    txtStfName = ""
    txtStfPeno = ""
    lblJobUrno = ""
    txtJobUrno = ""
    txtJobType = ""
    txtJobFlight = ""
    
    frmJobs = "Flight Jobs"
    frmAllJobs = ""
    
End Sub



'#######################################################################
'###
'#######################################################################
Private Sub findALC2_Change()
    txtSearchFlightUrno = ""
End Sub

'#######################################################################
'###
'#######################################################################
Private Sub findFLNO_Change()
    txtSearchFlightUrno = ""
End Sub

'#######################################################################
'###
'#######################################################################
Private Sub findPENO_Change()
    txtfindSTFUrno = ""
End Sub

'#######################################################################
'###
'#######################################################################
Private Sub findPENO_KeyUp(KeyCode As Integer, Shift As Integer)
    If KeyCode = 13 Then cmdSearch_Click
End Sub
'#######################################################################
'### This function loads the flight data as soon as user clicks calendar button in JOB Transactions
'#######################################################################
Private Sub flightCalendar_Click()

    Dim aftStrWhere As String
    Dim fromDt1 As String
    Dim toDt1 As String
    
    'Preparing date, according to current date&time-8 & current date&time+12
    fromDt1 = DateAdd("h", -8, flightCalendar)
    fromDt1 = Format(fromDt1, "yyyymmddhh")
    fromDt1 = fromDt1 & "0000"
    
    
    toDt1 = DateAdd("h", 15, flightCalendar)
    toDt1 = Format(toDt1, "yyyymmddhh")
    toDt1 = toDt1 & "5959"
    
    
    fromDt1 = Format(DateAdd("d", -2, CedaFullDateToVb(fromDt1)), "yyyymmddhhmmss")
    toDt1 = Format(DateAdd("d", 2, CedaFullDateToVb(toDt1)), "yyyymmddhhmmss")
    
    aftStrWhere = "where STOD between '" + fromDt1 + "' and '" + toDt1 + "' or STOA between '" + fromDt1 + "' and '" + toDt1 + "'"
    tab_AFTTABall.ResetContent
    tab_AFTTABall.Refresh
    frmData.LoadData tab_AFTTABall, aftStrWhere
    'MsgBox tab_AFTTAB.GetLineCount
            
End Sub

'#######################################################################
'### FORM LOAD
'#######################################################################
Private Sub Form_Load()
    
    '#Switch invisible HAS STUFF
    SSTAB1.TabVisible(1) = False
    SSTAB1.TabVisible(0) = False
    SSTAB1.TabVisible(2) = False
    SSTAB1.TabVisible(3) = False
    SSTAB1.TabVisible(4) = False
    SSTAB1.TabVisible(5) = False
    SSTAB1.TabVisible(6) = False
    SSTAB1.TabVisible(7) = True
    
End Sub

'#######################################################################
'###
'#######################################################################
Public Sub RefreshView()
    
    InitTabs
    LoadOrgUnits
    
    '### INIT CAL CONTROL WITH TODAY
    calFLDA.toDay
    calSDAY.toDay
    calUser.toDay
    flightCalendar.toDay
    userCal1.toDay
    
    '### CHECK UTC DIFF
    gUTCOffset = GetUTCOffset
    
    '### ADD TEMPLATES TO COMBOBOX
    Dim l As Long
    Dim strkey1 As String
    cmbTemplate.AddItem "ALL"
    cmbTemplate = "ALL"
    '### CONTENT
    For l = 0 To frmData.tab_TPLTAB.GetLineCount - 1 Step 1
        strkey1 = frmData.tab_TPLTAB.GetColumnValues(l, 0)
        cmbTemplate.AddItem strkey1
    Next l
    
    ' Below sectabFlag variable set to True, this variable was used for ORGUNITS-BDPSSEC page.
    sectabFlag = True
    
    
    '### R01TAB / DRRTAB
    'Changed script, to get the field descriptions for all fields .. thanooj
    gR01FieldList = "USEC,TIME,SFKT,"
    gR01FieldDescList = "User,Time,Action,"

    gR01TAB(0) = "USEC"
    gR01TAB(1) = "TIME"
    gR01TAB(2) = "SFKT"
    frmData.tab_SYSTAB.ResetContent
    frmData.LoadData frmData.tab_SYSTAB, "where tana='DRR' and logd='R01TAB'"
    If frmData.tab_SYSTAB.GetLineCount > 0 Then
        For l = 0 To frmData.tab_SYSTAB.GetLineCount - 1 Step 1
            If (frmData.tab_SYSTAB.GetColumnValue(l, 1) <> "") Then
                gR01FieldList = gR01FieldList + frmData.tab_SYSTAB.GetColumnValue(l, 1)
                gR01FieldDescList = gR01FieldDescList + frmData.tab_SYSTAB.GetColumnValues(l, 3)
                If l < frmData.tab_SYSTAB.GetLineCount - 1 Then
                    gR01FieldList = gR01FieldList + ","
                    gR01FieldDescList = gR01FieldDescList + ","
                End If
                gR01TAB(l + 3) = frmData.tab_SYSTAB.GetColumnValue(l, 1)
            End If
        Next l
    End If
    
    'MsgBox (gR01FieldList)
    
    'gR01FieldDescList = "User,Time,Action,Present of,Present to,1. Break pays (�x�) or not (� �),break optimization value,URNO of BSD,Radio number,Creation date,Layer number (layer 1-n per day),Additional Info for shift after shift,Percentage of the inability to work (format 050Z),Additional Info for shift before shift,Additional Info for shift independent to begin or end,Flag for auxiliary information (DRS record),Export file (�A� = archives � �_� = NO of archives,,Home Airport,Last update time,Old A,,Protocol flag,Remark,Planning stage,Status of the planning stage (�L� = load � �A� = Active � �N� = NEXT one),Tracing situation of,Tracing length pays,Tracing length unpaid,Tracing situation to,Layer code,Shift-Code Old,Layer day (YYYYMMDD,SHIFT Day offset,Data record number of the coworker,Unique Record number,User (creator),User (last Anderung one)"
    gR01FieldDescList = "User,Time,Action,Present From,Present To,First Break paid,Break optimization value,URNO of BSD,WalkieTalkie No,Creation date,No. of shift,Additional Info for shift after shift,Percentage of inability,Additional Info for shift before shift,Additional Info for shift independent to begin or end,Internal Flag,Export File,Function Code,Home Airport,Last update time,Old absence code,Protocol flag,Remarks,Planning level,State of planning level (L=Last A=Active N=Next),Breakcorridor from,Paid break length,Unpaid break length,Breakcorridor to,Shift code,Previous shift code,Day of shift,Shift Day Offset,Staff PENO,Unique Record number,User (creator),User (update)"

    gR01items = ItemCount(gR01FieldList, ",")
    'MsgBox (gR01items)
    '### SETUP GRID
    tab_LOG.ResetContent
    'tab_LOG.HeaderString = gR01FieldList
    'tab_LOG.HeaderString = gR01FieldDescList
    tab_LOG.HeaderString = "User,Time,Action,Shift Code,Function Code,Remarks"
    tab_LOG.HeaderLengthString = "100,100,100,100,100"
    tab_LOG.AutoSizeColumns
    tab_LOG.Refresh
    
    tab_LOGUSERR01.ResetContent
    'tab_LOGUSERR01.HeaderString = gR01FieldDescList
    tab_LOGUSERR01.HeaderString = "ORNO,Staff,Time,Action,Remarks,Shift Code,Function Code"
    tab_LOGUSERR01.HeaderLengthString = "0,40,100,30,100,100,100"
    tab_LOGUSERR01.AutoSizeColumns
    tab_LOGUSERR01.Refresh
    
    
    tab_LOGUSER.ResetContent
    tab_LOGUSER.HeaderString = gR01FieldList
    tab_LOGUSER.AutoSizeColumns
    tab_LOGUSER.Refresh
  
  
    '### R02TAB / JOBTAB
    gR02FieldList = "USEC,TIME,SFKT,"
    'gR02FieldDescList = "User,Time,Action,"
    gR02TAB(0) = "USEC"
    gR02TAB(1) = "TIME"
    gR02TAB(2) = "SFKT"
    frmData.tab_SYSTAB.ResetContent
    frmData.LoadData frmData.tab_SYSTAB, "where tana='JOB' and logd='R02TAB'"
    If frmData.tab_SYSTAB.GetLineCount > 0 Then
        For l = 0 To frmData.tab_SYSTAB.GetLineCount Step 1
            gR02FieldList = gR02FieldList + frmData.tab_SYSTAB.GetColumnValues(l, 1)
            'gR02FieldDescList = gR02FieldDescList + frmData.tab_SYSTAB.GetColumnValues(l, 3)
            If l < frmData.tab_SYSTAB.GetLineCount - 1 Then
                gR02FieldList = gR02FieldList + ","
                'gR02FieldDescList = gR02FieldDescList + ","
            End If
            gR02TAB(l + 3) = frmData.tab_SYSTAB.GetColumnValues(l, 1)
        Next l
    End If
    gR02items = ItemCount(gR02FieldList, ",")
    
    gR02FieldDescList = "User,Time,Action,Actual Job Start,Aircraft Type,Actual Job End,Allocation ID,Code of allocation unit,Date of creation,Demand type 0(T)/1(A)/2(D),Function code,Gate,Homeport,URNO of master pool job,Last update,Planned Job Start,Planned Job End,Parking stand,Aircraft registration,Duty status,Comments,Time to go from[seconds],Time to go in[seconds],Flight Number,URNO of allocation ID,URNO of allocation unit,URNO of deligation,URNO of demand,URNO of deviation,URNO of shift(DRRTAB),URNO of equipment,Service Name,Jobtype,URNO,User Creation,User  last change,Staff PENO,Template,Work group code"
    '### SETUP GRID
    tab_LOGJOB.ResetContent
    tab_LOGJOB.HeaderString = gR02FieldList
    tab_LOGJOB.AutoSizeColumns
    tab_LOGJOB.Refresh
   
    tab_LOGUSER2.ResetContent
    tab_LOGUSER2.HeaderString = gR02FieldList
    tab_LOGUSER2.AutoSizeColumns
    tab_LOGUSER2.Refresh
    
    tab_LOGUSERR02.ResetContent
    'tab_LOGUSERR02.HeaderString = gR02FieldDescList
    tab_LOGUSERR02.HeaderString = "ORNO,Staff,Time,Action,Service Name,Actual Job Start,Actual Job End"
    tab_LOGUSERR02.HeaderLengthString = "0,40,100,30,200,140,140"
    tab_LOGUSERR02.AutoSizeColumns
    tab_LOGUSERR02.Refresh
    
    
    tab_JOBLOG.ResetContent
    'tab_JOBLOG.HeaderString = gR02FieldDescList
    tab_JOBLOG.HeaderString = "User,Time,Action,Flight Number,Function Code,Service Code,Staff Number,Remarks"
    tab_LOGUSERR02.HeaderLengthString = "15,40,10,10,15,15,15,100"
    tab_JOBLOG.AutoSizeColumns
    tab_JOBLOG.Refresh
    
    tab_JOBTAB1.ResetContent
    tab_JOBTAB1.HeaderFontSize = 14
    tab_JOBTAB1.FontSize = 14
    tab_JOBTAB1.HeaderString = "URNO,USER,Action Time,Actual from,Actual to,Planned from,Planned to,Flight,Template,Job Type"
    tab_JOBTAB1.HeaderLengthString = "0,10,140,140,140,140,0,0,0,0"
    
    
    tab_STFLOG.ResetContent
    'tab_STFLOG.HeaderString = "PENO,URNO,Last,First,Groups,Profiles"
    'tab_STFLOG.HeaderLengthString = "100,100,100,100,100,100"
    'tab_STFLOG.AutoSizeColumns
    tab_STFLOG.Refresh
    
    'Initialize FormEventDetails
    frmJobDetails.InitEventTabs
    
    'SSTAB1.TabVisible(0) = False
    'SSTAB1.TabVisible(1) = False
    'SSTAB1.TabVisible(3) = False
    'SSTAB1.TabVisible(4) = False
   
End Sub

'#######################################################################
'###
'#######################################################################
Public Sub SetPrivStat()
    Dim strPrivRet As String
    strPrivRet = AATLoginControl1.GetPrivileges("m_TAB_Configuration")

    If strPrivRet = "0" Or strPrivRet = "-" Then
        SSTAB1.TabVisible(0) = False
    End If
End Sub

'#######################################################################
'###
'#######################################################################
Private Sub Form_Unload(Cancel As Integer)
    cmdExit_Click (0)
End Sub

'#######################################################################
'###
'#######################################################################
Sub TranslateFlightJobs()
   Dim tmpStr As String
    Dim Adid As String
    Dim l As Long
    
    If tab_JOBTAB.GetLineCount > 0 Then
        For l = 0 To tab_JOBTAB.GetLineCount - 1 Step 1
            tab_JOBTAB.SetColumnValue l, 1, DateAdd("h", gUTCOffset, CedaFullDateToVb(tab_JOBTAB.GetColumnValues(l, 1)))
            tab_JOBTAB.SetColumnValue l, 2, DateAdd("h", gUTCOffset, CedaFullDateToVb(tab_JOBTAB.GetColumnValues(l, 2)))
            tab_JOBTAB.SetColumnValue l, 3, DateAdd("h", gUTCOffset, CedaFullDateToVb(tab_JOBTAB.GetColumnValues(l, 3)))
            tab_JOBTAB.SetColumnValue l, 4, DateAdd("h", gUTCOffset, CedaFullDateToVb(tab_JOBTAB.GetColumnValues(l, 4)))
        Next l
    End If
    
    tab_JOBTAB.Refresh
    
End Sub

'#######################################################################
'###
'#######################################################################
Sub TranslateStaffJobs()
   Dim tmpStr As String
    Dim Adid As String
    Dim l As Long
    
    If tab_STFJOBS.GetLineCount > 0 Then
        For l = 0 To tab_STFJOBS.GetLineCount - 1 Step 1
            tab_STFJOBS.SetColumnValue l, 1, DateAdd("h", gUTCOffset, CedaFullDateToVb(tab_STFJOBS.GetColumnValues(l, 1)))
            tab_STFJOBS.SetColumnValue l, 2, DateAdd("h", gUTCOffset, CedaFullDateToVb(tab_STFJOBS.GetColumnValues(l, 2)))
            tab_STFJOBS.SetColumnValue l, 3, DateAdd("h", gUTCOffset, CedaFullDateToVb(tab_STFJOBS.GetColumnValues(l, 3)))
            tab_STFJOBS.SetColumnValue l, 4, DateAdd("h", gUTCOffset, CedaFullDateToVb(tab_STFJOBS.GetColumnValues(l, 4)))
        Next l
    End If
    
    tab_STFJOBS.Refresh
    
End Sub



Private Sub loadFlight_Click()

    Dim strkey1 As String
    Dim strWhere As String
    Dim l As Long
    'Dim findDate As String
    
    'findDate = Format(flightCalendar.Year, "0000") & Format(flightCalendar.Month, "00") & Format(flightCalendar.Day, "00")
    
    
    Dim fromDate As String
    Dim toDate As String
    
    'Preparing date, according to current date&time-8 & current date&time+12
    fromDate = DateAdd("h", -8, flightCalendar)
    fromDate = Format(fromDate, "yyyymmddhh")
    fromDate = fromDate & "0000"
    'MsgBox ("from Date: " & fromDate)
    
    toDate = DateAdd("h", 15, flightCalendar)
    toDate = Format(toDate, "yyyymmddhh")
    toDate = toDate & "5959"
    
    
    If checkArrival.Value = False And checkDeparture.Value = False Then
        MsgBox "Both Arrival and Departure not selected."
        Exit Sub
    End If
    
    If textAirline = "" Then
        MsgBox "Please Enter Airline"
        Exit Sub
    End If
    
    If textFlight = "" Then
        MsgBox "Please Enter Flight Number"
        Exit Sub
    End If
    
    If textAirline = "" And textFlight = "" Then
        MsgBox "Please Enter Airline & Flight Number."
        Exit Sub
    End If
    
    StatusBar1.Panels(1).Text = "Loading flight..."
    MousePointer = vbHourglass
    
    textAirline = UCase(textAirline)
    tab_AFTTAB.ResetContent
    tab_AFTTAB.Refresh
    
    'STOD Between '" & fromDate & "' and '" & toDate & "'"
    
    If checkArrival.Value = True Then
        If textAirline <> "" Then strWhere = "where STOA between '" & fromDate & "' and '" & toDate & "' and alc2 like '" & textAirline & "%' and adid='A'"
        If textAirline <> "" And textFlight <> "" Then strWhere = "where STOA between '" & fromDate & "' and '" & toDate & "' and flno like '" & textAirline & " " & textFlight & "%' and adid='A'"
    End If
    
    If checkDeparture.Value = True Then
        If textAirline <> "" Then strWhere = "where STOD between '" & fromDate & "' and '" & toDate & "' and alc2 like '" & textAirline & "%' and adid='D'"
        If textAirline <> "" And textFlight <> "" Then strWhere = "where STOD between '" & fromDate & "' and '" & toDate & "' and flno like '" & textAirline & " " & textFlight & "%' and adid='D'"
    End If
    
    'If checkArrival.Value = 1 Then
    '    If textAirline <> "" Then strWhere = "where STOA like '" & findDate & "%' and alc2 like '" & textAirline & "%' and adid='A'"
    '    If textAirline <> "" And textFlight <> "" Then strWhere = "where STOA like '" & findDate & "%' and flno like '" & textAirline & " " & textFlight & "%' and adid='A'"
    'End If
    
    'If checkDeparture.Value = 1 Then
    '    If textAirline <> "" Then strWhere = "where STOD like '" & findDate & "%' and alc2 like '" & textAirline & "%' and adid='D'"
    '    If textAirline <> "" And textFlight <> "" Then strWhere = "where STOD like '" & findDate & "%' and flno like '" & textAirline & " " & textFlight & "%' and adid='D'"
    'End If
    
    'If (chkArrival.Value = 1 And chkDeparture.Value = 1) Then
    '    If textAirline <> "" Then strWhere = "where STOD like '" & findDate & "%' and alc2 like '" & textAirline & "%'"
    '    If textAirline <> "" And textFlight <> "" Then strWhere = "where STOD like '" & findDate & "%' and flno like '" & textAirline & " " & textFlight & "%'"
    'End If
    
    labelUrno = ""
    labelFlightNo = ""
    labelAdid = ""
    labelScheduled = ""
    labelBest = ""
    labelDest = ""
    
    frmData.LoadData tab_AFTTAB, strWhere
    'MsgBox (strWhere)
    'tab_AFTTAB.Sort 1, True, False
    'tab_AFTTAB.Refresh
    
    'MsgBox (tab_AFTTAB.GetLineCount)
    
    If tab_AFTTAB.GetLineCount <> 0 Then
    
        TranslateFlights
        MousePointer = vbHourglass
        
        strkey1 = tab_AFTTAB.GetColumnValues(0, 0)
        labelUrno = strkey1
        strkey1 = tab_AFTTAB.GetColumnValues(0, 1)
        labelFlightNo = strkey1
        labelDest = tab_AFTTAB.GetColumnValues(0, 7) & "-" & tab_AFTTAB.GetColumnValues(l, 8)
        strkey1 = tab_AFTTAB.GetColumnValues(0, 2)
        If strkey1 = "A" Then
            labelAdid = "ARRIVAL"
            labelScheduled = tab_AFTTAB.GetColumnValues(0, 3)
            labelBest = tab_AFTTAB.GetColumnValues(0, 6)
        Else
            labelAdid = "DEPARTURE"
            labelScheduled = tab_AFTTAB.GetColumnValues(0, 4)
            labelBest = tab_AFTTAB.GetColumnValues(0, 5)
        End If
    
      Else
        MsgBox ("No flights found for selected flight search.")
      End If
        
    
    tab_AFTTAB.ResetContent
    tab_AFTTAB.Refresh
    
    StatusBar1.Panels(1).Text = "Ready."
    MousePointer = vbDefault

End Sub

'#######################################################################
'### LOAD JOBS FOR SPECIFIED TEMPLATE, PENO, JOBTYPE & FLIGHT ETC
'#######################################################################

Private Sub loadJobs_Click()

    'This function written for taking care of all options selected, to load jobs.
    
    Dim strQuery As String
    Dim strTemp As String
    Dim sday As String
    Dim urnoFlight As String
    Dim urnoTemplate As String
    Dim urnoJobtype As String
    Dim urnoStftab As String
    Dim peno As String
    'Dim strFields As String
    'Dim strValues As String
    'Dim strLine As String
    Dim i As Integer
    Dim l As Integer
    
    
    'Get the selected values
    urnoFlight = labelUrno
    sday = Format(flightCalendar.Year, "0000") & Format(flightCalendar.Month, "00") & Format(flightCalendar.Day, "00")
    peno = textPeno
    
    l = tab_TPLTAB.GetCurrentSelected
    
    If l <> -1 Then urnoTemplate = tab_TPLTAB.GetColumnValues(l, 0)
    'MsgBox (urnoTemplate + "   " + tab_TPLTAB.GetColumnValues(l, 1))
    
    l = tab_JTYTAB1.GetCurrentSelected
    If l <> -1 Then urnoJobtype = tab_JTYTAB1.GetColumnValues(l, 0)
    'MsgBox (urnoJobtype + "   " + tab_JTYTAB1.GetColumnValues(l, 1))
    
    'Prepare query to load the jobs
    strQuery = prepareJobQuery(peno, sday, urnoTemplate, urnoJobtype, labelUrno)
    globalR02Query = strQuery
    
    If (strQuery <> "") Then
    
        'strQuery = "where usec like 'JFK%' and time like '" & sday & "%' order by orno"
        'MsgBox ("Query : " + strQuery)
        
        MousePointer = vbHourglass
        
        'This piece of code written To Display the Job Transaction records
        tab_R02TAB.ResetContent
        frmData.LoadData tab_R02TAB, strQuery
        tab_R02TAB.AutoSizeColumns
        tab_R02TAB.Refresh
        tab_R02TAB.Sort 0, True, True
        
        tab_JOBTAB1.ResetContent
        tab_JOBTAB1.Refresh
        tab_JOBLOG.ResetContent
        tab_JOBLOG.Refresh
        
        'Prepare JOBTAB1 with JOBS
        prepareJobtab1
        
        'Prepare JOBLOG with all JOBS
        'showJoblog
        
        'prepare JOBLOG for selected JOB
        'showJoblogForSelected
        
        MousePointer = vbDefault
        
    End If
End Sub

Private Sub resetAll_Click()
    
    labelUrno = ""
    labelFlightNo = ""
    labelAdid = ""
    labelScheduled = ""
    labelBest = ""
    labelDest = ""
    textAirline = ""
    textFlight = ""
    urnoSTF = ""
    firstName = ""
    lastName = ""
    textPeno = ""
    tab_JTYTAB1.SetCurrentSelection (-1)
    tab_TPLTAB.SetCurrentSelection (-1)
    
    globalR02Query = ""
    tab_JOBTAB1.ResetContent
    tab_JOBTAB1.Refresh
    tab_JOBLOG.ResetContent
    tab_JOBLOG.Refresh
    
End Sub

Private Sub resetEmployee_Click()
    urnoSTF = ""
    firstName = ""
    lastName = ""
    textPeno = ""
End Sub

Private Sub resetFlight_Click()
    
    labelUrno = ""
    labelFlightNo = ""
    labelAdid = ""
    labelScheduled = ""
    labelBest = ""
    labelDest = ""
    textAirline = ""
    textFlight = ""
    
    tab_JOBTAB1.ResetContent
    tab_JOBTAB1.Refresh
    tab_JOBLOG.ResetContent
    tab_JOBLOG.Refresh
    
End Sub

Private Sub resetJobtype_Click()
    Dim i As Integer
    i = tab_JTYTAB1.GetCurrentSelected
    If (i <> -1) Then
        tab_JTYTAB1.SetCurrentSelection (-1)
    End If
End Sub

Private Sub resetTemplate_Click()
    Dim i As Integer
    
    i = tab_TPLTAB.GetCurrentSelected
    If (i <> -1) Then
        tab_TPLTAB.SetCurrentSelection (-1)
    End If
    
End Sub





'#######################################################################
'### LOAD JOBS FOR CLICKED AFTTAB FLIGHT RECORD
'#######################################################################
Private Sub tab_AFTTAB_SendLButtonClick(ByVal LineNo As Long, ByVal ColNo As Long)
    
    Dim strkey1 As String
    Dim strWhere As String
    Dim strTemp As String
    Dim strFields As String
    Dim strValues As String
    Dim l As Long
    Dim llLineCount As Long
    Dim tmpVal As String
    
    l = tab_AFTTAB.GetCurrentSelected
    
    If l <> -1 Then
    
        MousePointer = vbHourglass
        
        strkey1 = tab_AFTTAB.GetColumnValues(l, 0)
        txtUrno = strkey1
        strkey1 = tab_AFTTAB.GetColumnValues(l, 1)
        txtFlight = strkey1
        txtORGDES = tab_AFTTAB.GetColumnValues(l, 7) & "-" & tab_AFTTAB.GetColumnValues(l, 8)
        strkey1 = tab_AFTTAB.GetColumnValues(l, 2)
        If strkey1 = "A" Then
            lblAdid = "ARRIVAL"
            'tmpVal = DateAdd("h", gUTCOffset, CedaFullDateToVb(tab_AFTTAB.GetColumnValues(l, 3)))
            'txtSTO = tmpVal
            txtSTO = tab_AFTTAB.GetColumnValues(l, 3)
            'tmpVal = DateAdd("h", gUTCOffset, CedaFullDateToVb(tab_AFTTAB.GetColumnValues(l, 6)))
            'txtBEST = tmpVal
            txtBEST = tab_AFTTAB.GetColumnValues(l, 6)
        Else
            lblAdid = "DEPARTURE"
            'tmpVal = DateAdd("h", gUTCOffset, CedaFullDateToVb(tab_AFTTAB.GetColumnValues(l, 4)))
            'txtSTO = tmpVal
            txtSTO = tab_AFTTAB.GetColumnValues(l, 4)
            'tmpVal = DateAdd("h", gUTCOffset, CedaFullDateToVb(tab_AFTTAB.GetColumnValues(l, 5)))
            'txtBEST = tmpVal
            txtBEST = tab_AFTTAB.GetColumnValues(l, 5)
        End If
        
        
        StatusBar1.Panels(1).Text = "Loading job records for selected flight record ..."
        '### GET AFTTAB URNO
        strkey1 = tab_AFTTAB.GetColumnValues(l, 0)
        strkey1 = "where uaft='" & strkey1 & "'"
        
        tab_JOBTAB.ResetContent
        frmData.LoadData tab_JOBTAB, strkey1
        'tab_R01TAB.Sort 6, True, False
        'tab_JOBTAB.AutoSizeColumns
        tab_JOBTAB.Refresh
        
        frmJobs.Caption = "Flight jobs (" & tab_JOBTAB.GetLineCount & ") for " & txtFlight

        TranslateFlightJobs

        StatusBar1.Panels(1).Text = "Ready."
        MousePointer = vbDefault
        
    End If
    
    If LineNo = -1 Then
        tab_AFTTAB.Sort ColNo, True, False
        tab_AFTTAB.Refresh
    End If

End Sub

'#######################################################################
'###
'#######################################################################
Private Sub tab_ALTTAB_SendLButtonClick(ByVal LineNo As Long, ByVal ColNo As Long)
    If LineNo <> -1 Then
       findALC2 = tab_ALTTAB.GetColumnValues(LineNo, 0)
    End If
End Sub

'#######################################################################
'### To fillup airline field with selected airline.
'#######################################################################

Private Sub tab_ALTTABJOB_SendLButtonClick(ByVal LineNo As Long, ByVal ColNo As Long)
    If LineNo <> -1 Then
       textAirline = tab_ALTTABJOB.GetColumnValues(LineNo, 0)
    End If
End Sub

'#######################################################################
'###
'#######################################################################
Private Sub tab_DRRTAB_RowSelectionChanged(ByVal LineNo As Long, ByVal Selected As Boolean)
    tab_DRRTAB_SendLButtonClick LineNo, 0
End Sub

'#######################################################################
'### LOAD TRANSACTION RECORDS FOR CLICKED DRR RECORD(Shift record)
'#######################################################################
'Below piece of code changed .. Thanooj
Private Sub tab_DRRTAB_SendLButtonClick(ByVal LineNo As Long, ByVal ColNo As Long)
    Dim strkey1 As String
    Dim strWhere As String
    Dim strTemp As String
    Dim strFields As String
    Dim strValues As String
    Dim l As Long
    Dim llLineCount As Long
    Dim INlist As String
    Dim strLine As String
    
    
    ' Get the selected DRRTAB record.
    l = tab_DRRTAB.GetCurrentSelected
 
    If l <> -1 Then
    
        MousePointer = vbHourglass
        
        StatusBar1.Panels(1).Text = "Loading transaction records for selected shift record ..."
        '### GET DRRTAB URNO
        strkey1 = tab_DRRTAB.GetColumnValues(l, 0)
        strkey1 = "where orno='" & strkey1 & "'"
        
        '### LOAD ALL TRANS RECORDS FOR THIS DRRTAB RECORD
        tab_R01TAB.ResetContent
        frmData.LoadData tab_R01TAB, strkey1
        tab_R01TAB.Sort 6, True, False
        tab_R01TAB.AutoSizeColumns
        tab_R01TAB.Refresh
        
        '### IF AVAILABLE TRANSLATE INTO READABLE FORMAT
        If tab_R01TAB.GetLineCount > 0 Then
            
            lblNoLog.Caption = tab_R01TAB.GetLineCount & " transaction records for selected shift record."
            tab_LOG.ResetContent
            
            '### CONTENT
            For l = 0 To tab_R01TAB.GetLineCount - 1 Step 1
                
                '### LOG COLUMNS
                strFields = tab_R01TAB.GetColumnValues(l, 1)
                strFields = Replace(strFields, Chr(30), ",")
                strValues = tab_R01TAB.GetColumnValues(l, 2)
                strValues = Replace(strValues, Chr(30), ",")
                              
                '### USEC
                strLine = tab_R01TAB.GetColumnValues(l, 3)
                                
                '### No user in R01TAB FOUND?
                If strLine = "" Or UCase(strLine) = "EVENTSPOOL" Then
                    '### TAKE USEU
                    strLine = GetFieldValue("USEU", strValues, strFields)
                    '### STILL EMPTY TAKE USEC
                    If strLine = "" Then
                        strLine = GetFieldValue("USEC", strValues, strFields)
                    End If
                    '### STILL EMPTY PUT DEFAULT
                    If strLine = "" Then
                        strLine = "User not available"
                    End If
                End If
                If strLine = "SAP-HR" Then strLine = "SAP interface"
                
                '### TIME
                'strLine = strLine + "," & CedaFullDateToVb(tab_R01TAB.GetColumnValues(l, 4))
                strLine = strLine + "," & DateAdd("h", gUTCOffset, CedaFullDateToVb(tab_R01TAB.GetColumnValues(l, 4)))
                
                '#Action
                strLine = strLine + "," + tab_R01TAB.GetColumnValues(l, 5)
                
                'Shift Code
                strkey1 = GetFieldValue("SCOD", strValues, strFields)
                strLine = strLine + "," & strkey1
                
                'Function Code
                strkey1 = GetFieldValue("FCTC", strValues, strFields)
                strLine = strLine + "," & strkey1
                
                'Remarks
                strkey1 = GetFieldValue("REMA", strValues, strFields)
                strLine = strLine + "," & strkey1
                
                'strTemp = SortedR01Fields(strValues, strFields)
                
                'strLine = strLine + "," + strTemp
                
                'Add the record to Log tab
                tab_LOG.InsertTextLine strLine, False
                
            Next l
                
            tab_LOG.ShowHorzScroller True
            tab_LOG.AutoSizeColumns
            tab_LOG.Sort 1, True, True
            tab_LOG.EnableHeaderSizing True
            tab_LOG.Refresh
        Else
            MsgBox "No transaction logs available"
        End If
     
        StatusBar1.Panels(1).Text = "Ready."
        MousePointer = vbDefault
        
    End If
End Sub

'#######################################################################
'### LOAD TRANSACTION RECORDS FOR CLICKED JOBTAB RECORD
'#######################################################################
Function GetStaffbyUrno(tmpURNO As String, ByRef stfName As String, ByRef stfPeno As String)

    Dim strkey1 As String
    Dim strTemp As String
    Dim l As Long
    
    '### CONTENT
    For l = 0 To tab_STFCOMPLETE.GetLineCount Step 1
        
        strkey1 = tab_STFCOMPLETE.GetColumnValues(l, 1)
        
        If strkey1 = tmpURNO Then
            stfName = tab_STFCOMPLETE.GetColumnValues(l, 2) & "," & tab_STFCOMPLETE.GetColumnValues(l, 3)
            stfPeno = tab_STFCOMPLETE.GetColumnValues(l, 0)
            GetStaffbyUrno = 0
            Exit Function
        End If
    Next l
    
    GetStaffbyUrno = -1
    
End Function

Function GetStaffbyPENO(tmpPENO As String, ByRef stfName As String)

    Dim strkey1 As String
    Dim strTemp As String
    Dim l As Long
    
    '### CONTENT
    For l = 0 To tab_STFCOMPLETE.GetLineCount Step 1
        
        strkey1 = tab_STFCOMPLETE.GetColumnValues(l, 0)
        
        If strkey1 = tmpPENO Then
            stfName = tab_STFCOMPLETE.GetColumnValues(l, 2) & ";" & tab_STFCOMPLETE.GetColumnValues(l, 3)
            'stfPeno = tab_STFCOMPLETE.GetColumnValues(l, 0)
            GetStaffbyPENO = 0
            Exit Function
        End If
    Next l
    
    GetStaffbyPENO = -1
    stfName = "No name available in basic data"
    
End Function

'#######################################################################
'###
'#######################################################################
Function GetFlightByUrno(tmpURNO As String, ByRef flightNumber As String)

    Dim strkey1 As String
    Dim strTemp As String
    Dim l As Long
    
    '### CONTENT
    For l = 0 To tab_AFTTABall.GetLineCount Step 1
        
        strkey1 = tab_AFTTABall.GetColumnValues(l, 0)
        
        If strkey1 = tmpURNO Then
            flightNumber = tab_AFTTABall.GetColumnValues(l, 1)
            'stfPeno = tab_STFCOMPLETE.GetColumnValues(l, 0)
            GetFlightByUrno = 0
            Exit Function
        End If
    Next l
    
    GetFlightByUrno = -1


End Function


'#######################################################################
'###
'#######################################################################
Function GetJobTypeByUrno(tmpURNO As String, ByRef JobDSCR As String)

    Dim strkey1 As String
    Dim strTemp As String
    Dim l As Long
    
    '### RUN THROUGH HOB TYPES AND FIND URNO
    For l = 0 To tab_JTYTAB.GetLineCount Step 1
        
        strkey1 = tab_JTYTAB.GetColumnValues(l, 0)
        
        If strkey1 = tmpURNO Then
            JobDSCR = tab_JTYTAB.GetColumnValues(l, 1)
            GetJobTypeByUrno = 0
            Exit Function
        End If
    Next l
    
    GetJobTypeByUrno = -1


End Function
Sub showTransactionJOBTAB(tmpURNO As String)

    Dim strkey1 As String
    Dim strWhere As String
    Dim strTemp As String
    Dim strFields As String
    Dim strValues As String
    Dim l As Long
    Dim llLineCount As Long
    Dim INlist As String
    
    Dim strName As String
    Dim strPeno As String

        StatusBar1.Panels(1).Text = "Loading transaction records for selected job ..."
        '### GET JOBTAB URNO
        'strkey1 = tab_JOBTAB.GetColumnValues(l, 0)
        
        strkey1 = "where orno='" & tmpURNO & "'"
        
        '### LOAD ALL TRANS RECORDS FOR THIS JOBTAB RECORD
        tab_R02TAB.ResetContent
        frmData.LoadData tab_R02TAB, strkey1
        tab_R02TAB.Sort 6, True, False
        tab_R02TAB.AutoSizeColumns
        tab_R02TAB.Refresh
        
        '### IF AVAILABLE TRANSLATE INTO READABLE FORMAT
        If tab_R02TAB.GetLineCount > 0 Then
            tab_LOGJOB.ResetContent
            
            '### CONTENT
            For l = 0 To tab_R02TAB.GetLineCount - 1 Step 1
                '### USEC
                strkey1 = tab_R02TAB.GetColumnValues(l, 3)
                
                '### No user in R01TAB FOUND?
                If strkey1 = "" Or UCase(strkey1) = "EVENTSPOOL" Then
                    '### TAKE USEU
                    strkey1 = GetFieldValue("USEU", strValues, strFields)
                    '### STILL EMPTY TAKE USEC
                    If strkey1 = "" Then
                        strkey1 = GetFieldValue("USEC", strValues, strFields)
                    End If
                    '### STILL EMPTY PUT DEFAULT
                    If strkey1 = "" Then
                        strkey1 = "User not available"
                    End If
                End If
                If strkey1 = "polhdlr" Then strkey1 = "System"
                
                '### TIME
                'strkey1 = strkey1 + "," + tab_R02TAB.GetColumnValues(l, 4)
                strkey1 = strkey1 + "," & DateAdd("h", gUTCOffset, CedaFullDateToVb(tab_R02TAB.GetColumnValues(l, 4)))
                '### FUNCTION
                strkey1 = strkey1 + "," + tab_R02TAB.GetColumnValues(l, 5)
                
                '### LOG COLUMNS
                strFields = tab_R02TAB.GetColumnValues(l, 1)
                strFields = Replace(strFields, Chr(30), ",")
                strValues = tab_R02TAB.GetColumnValues(l, 2)
                strValues = Replace(strValues, Chr(30), ",")
                
                strTemp = SortedR02Fields(strValues, strFields)
                
                strkey1 = strkey1 + "," + strTemp
                
                tab_LOGJOB.InsertTextLine strkey1, False
                
            Next l
                
            tab_LOGJOB.ShowHorzScroller True
            tab_LOGJOB.AutoSizeColumns
            tab_LOGJOB.EnableHeaderSizing True
            tab_LOGJOB.Refresh
        Else
            MsgBox "No transaction logs available"
        End If
     
        StatusBar1.Panels(1).Text = "Ready."
        MousePointer = vbDefault

End Sub

Private Sub tab_DRRTAB_SendLButtonDblClick(ByVal LineNo As Long, ByVal ColNo As Long)
    
    Dim i As Integer
    i = tab_DRRTAB.GetCurrentSelected
    
    If i <> -1 Then
        
        MousePointer = vbHourglass
        frmShiftDetails.Show
        frmShiftDetails.showShiftEventDetails tab_DRRTAB.GetColumnValues(i, 0)
        MousePointer = vbDefault
    End If
        
End Sub



'#######################################################################
'### LOAD TRANSACTION RECORDS FOR CLICKED JOBTAB RECORD
'#######################################################################
Private Sub tab_JOBTAB_SendLButtonClick(ByVal LineNo As Long, ByVal ColNo As Long)

    'Dim strkey1 As String
    'Dim strWhere As String
    Dim strTemp As String
    'Dim strFields As String
    'Dim strValues As String
    Dim l As Long
    'Dim llLineCount As Long
    'Dim INlist As String
    
    Dim strName As String
    Dim strPeno As String
    
    l = tab_JOBTAB.GetCurrentSelected
    
    If l <> -1 Then
        '### Detail Jobs
        lblJobUrno = tab_JOBTAB.GetColumnValues(l, 0)
        strTemp = tab_JOBTAB.GetColumnValues(l, 8)
        l = GetStaffbyUrno(strTemp, strName, strPeno)
        txtStfPeno = strPeno
        txtStfName = strName
    End If
    
End Sub
'#######################################################################
'### LOAD JOBLOG FOR SELECTED JOB
'#######################################################################
Private Sub tab_JOBTAB1_SendLButtonClick(ByVal LineNo As Long, ByVal ColNo As Long)
    
    Dim strFields As String
    Dim strValues As String
    Dim strLine As String
    Dim strTemp As String
    Dim ornoSelected As String
    Dim ornoTemp As String
    Dim strQuery As String
    Dim strkey1 As String
    Dim l As Integer
    Dim i As Integer
    
    
    'Show the jobs for selected ORNO of JOBTAB1
    i = tab_JOBTAB1.GetCurrentSelected
    
    If (i <> -1) Then
        
        MousePointer = vbHourglass
        
        ornoSelected = tab_JOBTAB1.GetColumnValues(i, 0)
        
        'If ORNO is zero then, so many records contains ORNO as ZERO
        If (ornoSelected = "0") Then
            strQuery = globalR02Query & " and orno = '0'"
        Else
            strQuery = "where orno = '" & ornoSelected & "'"
        End If
        
        tab_R02TAB.ResetContent
        frmData.LoadData tab_R02TAB, strQuery
        tab_R02TAB.AutoSizeColumns
        tab_R02TAB.Refresh
        tab_R02TAB.Sort 0, True, True
        
        tab_JOBLOG.ResetContent
        
        For l = 0 To tab_R02TAB.GetLineCount - 1 Step 1
            
            ornoTemp = tab_R02TAB.GetColumnValues(l, 0)
        
            '### LOG COLUMNS
            strFields = tab_R02TAB.GetColumnValues(l, 1)
            strFields = Replace(strFields, Chr(30), ",")
            strValues = tab_R02TAB.GetColumnValues(l, 2)
            strValues = Replace(strValues, Chr(30), ",")
                       
            'strLine = User value
            'strLine = tab_R02TAB.GetColumnValues(l, 3)
            
            'code change start
            strkey1 = tab_R02TAB.GetColumnValues(l, 3)
            If strkey1 = "" Or UCase(strkey1) = "EVENTSPOOL" Then
                '### TAKE USEU
                strkey1 = GetFieldValue("USEU", strValues, strFields)
                '### STILL EMPTY TAKE USEC
                If strkey1 = "" Then
                    strkey1 = GetFieldValue("USEC", strValues, strFields)
                End If
                '### STILL EMPTY PUT DEFAULT
                If strkey1 = "" Then
                    strkey1 = "User not available"
                End If
            End If
            If strkey1 = "polhdlr" Then strkey1 = "System"
            strLine = strkey1
            
            'code change end
            
            '### TIME
            strLine = strLine + "," & DateAdd("h", gUTCOffset, CedaFullDateToVb(tab_R02TAB.GetColumnValues(l, 4)))
            strLine = strLine + "," + tab_R02TAB.GetColumnValues(l, 5)
            
            'UAFT - Flight Number
            strkey1 = GetFieldValue("UAFT", strValues, strFields)
            strkey1 = tab_AFTTABall.GetLinesByColumnValue(0, strkey1, 0)
            If strkey1 <> "" Then strkey1 = tab_AFTTABall.GetColumnValue(CLng(strkey1), 1)
            strLine = strLine + "," & strkey1
            
            'FCCO - Function Code
            strkey1 = GetFieldValue("FCCO", strValues, strFields)
            strLine = strLine + "," & strkey1
            
            'UGHS - Service
            strkey1 = GetFieldValue("UGHS", strValues, strFields)
            If strkey1 <> "" Then strkey1 = tab_SERTAB.GetLinesByColumnValue(0, strkey1, 0)
            If strkey1 <> "" Then strkey1 = tab_SERTAB.GetColumnValue(CLng(strkey1), 1)
            strLine = strLine + "," & strkey1
            
            'USTF - PENO
            strkey1 = GetFieldValue("USTF", strValues, strFields)
            If strkey1 <> "" Then strkey1 = tab_STFCOMPLETE.GetLinesByColumnValue(1, strkey1, 0)
            If strkey1 <> "" Then strkey1 = tab_STFCOMPLETE.GetColumnValue(CLng(strkey1), 0)
            strLine = strLine + "," & strkey1
            
            'LSTU - Last updated time
            'strkey1 = GetFieldValue("LSTU", strValues, strFields)
            'If strkey1 <> "" Then strkey1 = DateAdd("h", gUTCOffset, CedaFullDateToVb(strkey1))
            'strLine = strLine + "," & strkey1
            
            'TEXT - Comments
            strkey1 = GetFieldValue("TEXT", strValues, strFields)
            strLine = strLine + "," & strkey1
            
            
            'ACFR - Actual From
            'strkey1 = GetFieldValue("ACFR", strValues, strFields)
            'If strkey1 <> "" Then strkey1 = DateAdd("h", gUTCOffset, CedaFullDateToVb(strkey1))
            'strLine = strLine + "," & strkey1
            
            'ACTO - Actual To
            'strkey1 = GetFieldValue("ACTO", strValues, strFields)
            'If strkey1 <> "" Then strkey1 = DateAdd("h", gUTCOffset, CedaFullDateToVb(strkey1))
            'strLine = strLine + "," & strkey1
            
            
            'strTemp = SortedR02Fields(strValues, strFields)
            'strLine = strLine + "," + strTemp
            
            'Added below information according to Client's request.
            
            
            
            
            'Add the record to Log tab
            tab_JOBLOG.InsertTextLine strLine, False
               
        Next l
            
        tab_JOBLOG.ShowHorzScroller True
        tab_JOBLOG.AutoSizeColumns
        'tab_JOBLOG.Sort 1, True, True
        tab_JOBLOG.EnableHeaderSizing True
        tab_JOBLOG.Refresh
        
        MousePointer = vbDefault
        
    End If
    
End Sub

Private Sub tab_JOBTAB1_SendLButtonDblClick(ByVal LineNo As Long, ByVal ColNo As Long)
        
    Dim i As Integer
    Dim l As Integer
    Dim strKey As String

    i = tab_JOBTAB1.GetCurrentSelected
    
    If i <> -1 Then
        
        MousePointer = vbHourglass
        
        frmJobDetails.Show
        frmJobDetails.showJobEventDetails tab_JOBTAB1.GetColumnValues(i, 0)
        'frmJobDetails.tab_EVENT.ResetContent
        'frmJobDetails.tab_EVENT.HeaderString = "Field Description, Value"
        'frmJobDetails.tab_EVENT.HeaderLengthString = "100,250"
        'frmJobDetails.tab_EVENT.HeaderFontSize = 14
        'frmJobDetails.tab_EVENT.FontSize = 10
        'frmJobDetails.tab_EVENT.Refresh
        
        'For l = 0 To gR02items - 1 Step 1
         '   strkey = tab_JOBLOG.GetColumnValue(i, l)
         '   frmJobDetails.tab_EVENT.SetColumnValue l, 1, strkey
        'Next l
        
        MousePointer = vbDefault
    
    End If
        'gR02FieldDescList
    
    
End Sub

Private Sub tab_LOGUSER_SendLButtonClick(ByVal LineNo As Long, ByVal ColNo As Long)
    If LineNo = -1 Then
        tab_LOGUSER.Sort ColNo, True, False
        tab_LOGUSER.Refresh
    End If
End Sub

Private Sub tab_LOGUSER2_SendLButtonClick(ByVal LineNo As Long, ByVal ColNo As Long)
    If LineNo = -1 Then
        tab_LOGUSER2.Sort ColNo, True, False
        tab_LOGUSER2.Refresh
    End If
End Sub


Private Sub tab_LOGUSERR01_SendLButtonDblClick(ByVal LineNo As Long, ByVal ColNo As Long)
    
    Dim i As Integer
    i = tab_LOGUSERR01.GetCurrentSelected
    
    If i <> -1 Then
        
        MousePointer = vbHourglass
        frmShiftDetails.Show
        frmShiftDetails.showShiftEventDetails tab_LOGUSERR01.GetColumnValues(i, 0)
        MousePointer = vbDefault
    End If
    
End Sub

Private Sub tab_LOGUSERR02_SendLButtonDblClick(ByVal LineNo As Long, ByVal ColNo As Long)
    
    Dim i As Integer
    Dim l As Integer
    Dim strKey As String

    i = tab_LOGUSERR02.GetCurrentSelected
    
    If i <> -1 Then
        
        MousePointer = vbHourglass
        frmJobDetails.Show
        frmJobDetails.showJobEventDetails tab_LOGUSERR02.GetColumnValues(i, 0)
        MousePointer = vbDefault
    
    End If
End Sub

'#######################################################################
'### LOAD SORTAB FOR ORGUNIT
'#######################################################################
'This piece of code written to retrieve the staff for selected Org Unit.

Private Sub tab_ORGTAB_SendLButtonDblClick(ByVal LineNo As Long, ByVal ColNo As Long)
    
    Dim strkey1 As String
    Dim strWhere As String
    Dim l As Long
    Dim llLineCount As Long
    Dim INlist As String
    
    'Getting the selected org unit index
    l = tab_ORGTAB.GetCurrentSelected
    
    If l <> -1 Then
    
        
        MousePointer = vbHourglass
        ClearDetails
        
        'Getting the org unit value
        strkey1 = tab_ORGTAB.GetColumnValues(l, 0)
        'MsgBox CStr(strkey1)
        
        'Prepare the query to retrieve staff information from Staff table SORTAB
        strWhere = "where code='" & strkey1 & "'"

        'To show the message in status bar, until it loads the staff information from DB
        StatusBar1.Panels(1).Text = "Loading staff data ..."
        
        
        'Clearing the SORTAB buffer, if any information is already available
        frmData.tab_SORTAB.ResetContent
        frmData.tab_SORTAB.Refresh
        'Load data from SORTAB
        frmData.LoadData frmData.tab_SORTAB, strWhere
        
        'Get the number of staff
        llLineCount = frmData.tab_SORTAB.GetLineCount
        'MsgBox CStr(llLineCount)

        For l = 0 To llLineCount Step 1
            strkey1 = frmData.tab_SORTAB.GetColumnValues(l, 1)
            INlist = INlist & strkey1
            If l < llLineCount - 1 Then
                INlist = INlist & ","
            End If
        Next l
        
        strWhere = "where urno in (" & INlist & ") order by peno"
        
        'Clearing the STFTAB buffer, if any information is already available
        tab_STFTAB.ResetContent
        tab_STFTAB.Refresh
        
        'To retrieve the information from STFTAB
        frmData.LoadData tab_STFTAB, strWhere
    
        StatusBar1.Panels(1).Text = "Ready."

        tab_STFTAB.AutoSizeColumns
        'tab_STFTAB.Refresh
    
        lblNoStaff.Caption = "Number of Staff: " & tab_STFTAB.GetLineCount
    
        MousePointer = vbDefault
        
    End If

End Sub

Sub ClearDetails()
'#######################################################################
'### CLEAR FIELDS AFTER SELECTING NEW ORG UNIT
'#######################################################################
    txtPENO = ""
    txtLANM = ""
    txtFINM = ""
End Sub

Private Sub tab_SECORGTAB_SendLButtonDblClick(ByVal LineNo As Long, ByVal ColNo As Long)
    
    Dim strkey1 As String
    Dim strWhere As String
    Dim l As Long
    Dim llLineCount As Long
    Dim INlist As String
    Dim secStrWhere As String
    Dim secCount As Long
    Dim toDay As String
    Dim grpKey As String
    Dim secUrno As String
    Dim grpUrno As String
    Dim groupStr As String
    Dim profileStr As String
    Dim ilCnt As Integer
    Dim i As Integer
    Dim strLineNo As String
    Dim grpLine As String
    Dim grpType As String
    
    Dim fappUrno As String
    Dim applicationStr As String
    Dim appCount As Long
    Dim appCount1 As Long
    Dim k As Long
    Dim grpCount As String
    Dim appStr As String
    Dim appHeader As String
    Dim appKey As String
    Dim j As Integer
    Dim secUrno1 As String
    Dim grpSecUrno As String
    
    Dim grpKey1 As String
    Dim i1Cnt1 As Integer
    Dim grpLine1 As String
    Dim grpSecUrno1 As String
    Dim grpSecLine1 As String
    Dim grpType1 As String

    ' kkh on 20/11/2007 to cater for USER do not assign to any org unit
    Dim m As Integer
    Dim strPeno As String
    Dim stfLine As String
    Dim stfUrno As String
    
    'Getting the selected org unit index
    l = tab_SECORGTAB.GetCurrentSelected
    
    If l <> -1 Then
    
        MousePointer = vbHourglass
        ClearDetails
        
        'Getting the org unit value
        strkey1 = tab_SECORGTAB.GetColumnValues(l, 0)
        
        
        'Below query holds good to retrieve the required information, but as dont know how to use joining 3 tables to retrieve info through TAB.
        'strWhere = "where stftab.urno = sortab.surn and to_char(trim(sectab.usid)) = to_char(trim(stftab.peno)) and sectab.type='U' and sectab.stat='1' and sortab.code = '" & strkey1 & "' and sortab.vpfr <= '20060705000000' and (sortab.vpto >= '20060705000000' or sortab.vpto=' ')"
        
        'So getting the info of USID from SECTAB where TYPE='U' and STAT='1'
        'Below check is to retrive USID from SECTAB only once & when user comes to this page and double clicks org unit.
        'Below code will execute only once to avoid network traffic, if user wants to test instant changes ,then remove the flag if check.
        If sectabFlag Then
            'Below piece of code to retrieve valid users.
            secStrWhere = "where type='U' and stat='1'"
            tab_SECTAB.ResetContent
            tab_SECTAB.Refresh
            frmData.LoadData tab_SECTAB, secStrWhere
            
            'Below piece of code to retrieve valid groups & profiles from SECTAB
            secStrWhere = "where stat='1' and type='G' or type='P'"
            tab_SECTABGRP.ResetContent
            tab_SECTABGRP.Refresh
            frmData.LoadData tab_SECTABGRP, secStrWhere
            
'            'Below piece of code to retrieve valid profiles from SECTAB
'            secStrWhere = "where stat='1' and type='P'"
'            tab_SECTABPRF.ResetContent
'            tab_SECTABPRF.Refresh
'            frmData.LoadData tab_SECTABPRF, secStrWhere
            
            'Below piece of code to retrieve valid groups.
            secStrWhere = ""
            tab_GRPTAB.ResetContent
            tab_GRPTAB.Refresh
            frmData.LoadData tab_GRPTAB, secStrWhere
            
            secStrWhere = "order by fsec,fapp"
            tab_PRVTAB.ResetContent
            tab_PRVTAB.Refresh
            frmData.LoadData tab_PRVTAB, secStrWhere
            'MsgBox (tab_PRVTAB.GetLineCount)
            
            
            'Below piece of code to retrieve valid Application Names
            secStrWhere = "where type='A' and stat='1'"
            tab_SECTABAPP.ResetContent
            tab_SECTABAPP.Refresh
            frmData.LoadData tab_SECTABAPP, secStrWhere
            'MsgBox (tab_SECTABAPP.GetLineCount)
            sectabFlag = False
        End If
        
        ' kkh on 20/11/2007 to cater for USER do not assign to any org unit
        If strkey1 = "NO ORG UNIT" Then
            StatusBar1.Panels(1).Text = "Loading staff data ..."
            
            'Clearing the SECSTFTAB buffer, if any information is already available
            tab_SECSTFTAB.ResetContent
            tab_SECSTFTAB.Refresh
            'To retrieve the information from STFTAB
            frmData.LoadData tab_SECSTFTAB, strWhere
            
            'below pice of code to add application names to the tab_STFLOG table header
            appStr = "PENO,URNO,Last,First,Groups,Profiles,"
            appHeader = "100,100,100,100,100,100,"
            
            appCount = tab_SECTABAPP.GetLineCount
            For k = 0 To appCount - 1 Step 1
                appName = tab_SECTABAPP.GetColumnValue(k, 1)
                appStr = appStr + appName + ","
                appHeader = appHeader + "100,"
            Next k
            
            tab_STFLOG.ResetContent
            tab_STFLOG.HeaderString = appStr
            tab_STFLOG.HeaderLengthString = appHeader
            tab_STFLOG.Refresh
            
            'Prepare the list of valid employees from SECTAB, & generate tab_STFLOG tab
            secCount = 0
            llLineCount = tab_SECTAB.GetLineCount
            For m = 0 To llLineCount - 1 Step 1
                'strkey1 = tab_SECSTFTAB.GetColumnValues(l, 0)
                strkey1 = tab_SECTAB.GetColumnValues(m, 1)
                secUrno = tab_SECTAB.GetColumnValues(m, 0)
                stfLine = tab_SECSTFTAB.GetLinesByColumnValue(0, strkey1, 0)
                If stfLine <> "" Then
                    stfUrno = tab_SECSTFTAB.GetColumnValue(stfLine, 1)
                    strWhere = "where SURN = " & stfUrno
                    frmData.tab_SORTAB.ResetContent
                    frmData.tab_SORTAB.Refresh
                    'Load data from SORTAB
                    frmData.LoadData frmData.tab_SORTAB, strWhere
                    
                    llLineCount = frmData.tab_SORTAB.GetLineCount
                    'For l = 0 To llLineCount Step 1
                    If llLineCount = 0 Then
                        
                        'Once valid User id found in SECTAB, Need to get the profile & group user belongs to.
                        'Need to get the URNO of SECTAB & based on that retrieve group,profile from GRPTAB
                                        
                        'To get all the groups & profiles
                        grpKey = tab_GRPTAB.GetLinesByColumnValue(0, secUrno, 0)
                        groupStr = ""
                        profileStr = ""
                        appStr = ""
                        applicationStr = ""
                    
                        ilCnt = ItemCount(grpKey, ",")
                        
                        For i = 1 To ilCnt Step 1
                            grpLine = GetItem(grpKey, i, ",")
                            grpSecUrno = tab_GRPTAB.GetColumnValues(grpLine, 1)
                        
                            If IsNumeric(tab_SECTABGRP.GetLinesByColumnValue(0, grpSecUrno, 0)) Then
                            
                                grpSecLine = ""
                                grpSecLine = tab_SECTABGRP.GetLinesByColumnValue(0, grpSecUrno, 0)
                                grpType = tab_SECTABGRP.GetColumnValue(grpSecLine, 3)
                            
                                'Below piece of code to get all the valid applications, user can access.
                                If grpType = "G" Then
                                    groupStr = groupStr + tab_SECTABGRP.GetColumnValue(grpSecLine, 1) + ";"
                                
                                    'Need to check whether is there any record available in PRVTAB, if not available then it belongs to Profile.
                                    grpCount = tab_PRVTAB.GetLinesByColumnValue(1, grpSecUrno, 0)
                                
                                    If ItemCount(grpCount, ",") > 0 Then
                                        'Group is having own profile.
                            
                                        For k = 0 To appCount - 1 Step 1
                                            appUrno = tab_SECTABAPP.GetColumnValue(k, 0)
                                            appKey = tab_PRVTAB.GetLinesByMultipleColumnValue("0,1", appUrno + "," + grpSecUrno, 0)
                                            appCount1 = ItemCount(appKey, ",")
                                            If appCount1 > 0 Then applicationStr = applicationStr + tab_SECTABAPP.GetColumnValue(k, 1)
                                        Next k
                                    Else
                                        'Group belongs to another profile ... Need to test, need to change the variable names
                                        grpKey1 = tab_GRPTAB.GetLinesByColumnValue(0, grpSecUrno, 0)
                
                                        ilCnt1 = ItemCount(grpKey1, ",")
                                        For j = 1 To ilCnt1 Step 1
                                            grpLine1 = GetItem(grpKey1, j, ",")
                                            grpSecUrno1 = tab_GRPTAB.GetColumnValues(grpLine1, 1)
                                        
                                            If IsNumeric(tab_SECTABGRP.GetLinesByColumnValue(0, grpSecUrno1, 0)) Then
                                            
                                                grpSecLine1 = tab_SECTABGRP.GetLinesByColumnValue(0, grpSecUrno1, 0)
                                                grpType1 = tab_SECTABGRP.GetColumnValue(grpSecLine1, 3)
                                            
                                                If grpType1 = "P" Then
                                                    'secUrno1 = tab_SECTABGRP.GetColumnValue(grpSecLine1, 0)
                                                    For k = 0 To appCount - 1 Step 1
                                                        appUrno = tab_SECTABAPP.GetColumnValue(k, 0)
                                                        appKey = tab_PRVTAB.GetLinesByMultipleColumnValue("0,1", appUrno + "," + grpSecUrno1, 0)
                                                        appCount1 = ItemCount(appKey, ",")
                                                        If appCount1 > 0 Then applicationStr = applicationStr + tab_SECTABAPP.GetColumnValue(k, 1)
                                                    Next k
                                                End If
                                            End If
                                        Next j
                                    End If
                                Else
                                    'kkh
                                    grpKey1 = tab_SECTABGRP.GetLinesByColumnValue(0, grpSecUrno, 0)
                                    'profileStr = profileStr + tab_SECTABGRP.GetColumnValue(grpSecUrno, 1) + ";"
                                    profileStr = profileStr + tab_SECTABGRP.GetColumnValue(grpKey1, 1) + ";"
                                    'Take secUrno & fappUrno(for all applications) to check in PRVTAB, If its available create a applicationStr
                                    For k = 0 To appCount - 1 Step 1
                                        appUrno = tab_SECTABAPP.GetColumnValue(k, 0)
                                        appKey = tab_PRVTAB.GetLinesByMultipleColumnValue("0,1", appUrno + "," + grpSecUrno, 0)
                                        appCount1 = ItemCount(appKey, ",")
                                        If appCount1 > 0 Then applicationStr = applicationStr + tab_SECTABAPP.GetColumnValue(k, 1)
                                    Next k
                                End If
                            End If
                        Next i
                        
                        appStr = ""
                        'To print the applications access. 1 for application access and 0 for no access
                        For k = 0 To appCount - 1 Step 1
                            appName = tab_SECTABAPP.GetColumnValue(k, 1)
                            
                            If InStr(applicationStr, appName) > 0 Then
                                appStr = appStr + "1,"
                            Else
                                appStr = appStr + "0,"
                            End If
                        Next k
                        'strLine = tab_SECSTFTAB.GetColumnValues(l, 0) + "," + tab_SECSTFTAB.GetColumnValues(l, 1) + "," + tab_SECSTFTAB.GetColumnValues(l, 2) + "," + tab_SECSTFTAB.GetColumnValues(l, 3) + "," + groupStr + "," + profileStr + "," + appStr
                        strLine = tab_SECSTFTAB.GetColumnValues(stfLine, 0) + "," + tab_SECSTFTAB.GetColumnValues(stfLine, 1) + "," + tab_SECSTFTAB.GetColumnValues(stfLine, 2) + "," + tab_SECSTFTAB.GetColumnValues(stfLine, 3) + "," + groupStr + "," + profileStr + "," + appStr
                                                            
                        tab_STFLOG.InsertTextLine strLine, False
                        'Debug.Print strLine
                        secCount = secCount + 1
                    End If
                End If
            Next m
        Else
            'Prepare the query to retrieve staff information from Staff table SORTAB
            toDay = Format(Now, "yyyymmddhhmmss")
            strWhere = "where code='" & strkey1 & "' and vpfr <= '" & toDay & "' and (vpto >= '" & toDay & "' or vpto=' ')"
            'MsgBox (strWhere)
            'strWhere = "where code='" & strkey1 & "' and vpfr <= '20060705000000' and (vpto >= '20060705000000' or vpto=' ')"
            
            'Clearing the SORTAB buffer, if any information is already available
            frmData.tab_SORTAB.ResetContent
            frmData.tab_SORTAB.Refresh
            'Load data from SORTAB
            frmData.LoadData frmData.tab_SORTAB, strWhere
            
            'Get the number of staff
            llLineCount = frmData.tab_SORTAB.GetLineCount
            For l = 0 To llLineCount Step 1
                strkey1 = frmData.tab_SORTAB.GetColumnValues(l, 1)
                INlist = INlist & strkey1
                If l < llLineCount - 1 Then
                    INlist = INlist & ","
                End If
            Next l
            strWhere = "where urno in (" & INlist & ") order by peno"
            
            'To show the message in status bar, until it loads the staff information from DB
            StatusBar1.Panels(1).Text = "Loading staff data ..."
            
            'Clearing the SECSTFTAB buffer, if any information is already available
            tab_SECSTFTAB.ResetContent
            tab_SECSTFTAB.Refresh
            'To retrieve the information from STFTAB
            frmData.LoadData tab_SECSTFTAB, strWhere
            
            'below pice of code to add application names to the tab_STFLOG table header
            appStr = "PENO,URNO,Last,First,Groups,Profiles,"
            appHeader = "100,100,100,100,100,100,"
            
            appCount = tab_SECTABAPP.GetLineCount
            For k = 0 To appCount - 1 Step 1
                appName = tab_SECTABAPP.GetColumnValue(k, 1)
                appStr = appStr + appName + ","
                appHeader = appHeader + "100,"
            Next k
            
            tab_STFLOG.ResetContent
            tab_STFLOG.HeaderString = appStr
            tab_STFLOG.HeaderLengthString = appHeader
            tab_STFLOG.Refresh
            
            'Prepare the list of valid employees from SECTAB, & generate tab_STFLOG tab
            secCount = 0
            llLineCount = tab_SECSTFTAB.GetLineCount
            For l = 0 To llLineCount - 1 Step 1
                strkey1 = tab_SECSTFTAB.GetColumnValues(l, 0)
                'Check whether he is a valid employee or not
                If IsNumeric(tab_SECTAB.GetLinesByColumnValue(1, strkey1, 0)) Then
                
                    'Need to write code for GRPTAB as well as SECTAB
                    'Once valid User id found in SECTAB, Need to get the profile & group user belongs to.
                    'Need to get the URNO of SECTAB & based on that retrieve group,profile from GRPTAB
                
                    secUrno = tab_SECTAB.GetLinesByColumnValue(1, strkey1, 0)
                    secUrno = tab_SECTAB.GetColumnValues(secUrno, 0)
                
                    'To get all the groups & profiles
                    grpKey = tab_GRPTAB.GetLinesByColumnValue(0, secUrno, 0)
                    groupStr = ""
                    profileStr = ""
                    appStr = ""
                    applicationStr = ""
                
                    ilCnt = ItemCount(grpKey, ",")
                    For i = 1 To ilCnt Step 1
                        grpLine = GetItem(grpKey, i, ",")
                        grpSecUrno = tab_GRPTAB.GetColumnValues(grpLine, 1)
                    
                        If IsNumeric(tab_SECTABGRP.GetLinesByColumnValue(0, grpSecUrno, 0)) Then
                        
                            grpSecLine = ""
                            grpSecLine = tab_SECTABGRP.GetLinesByColumnValue(0, grpSecUrno, 0)
                            grpType = tab_SECTABGRP.GetColumnValue(grpSecLine, 3)
                        
                            'Below piece of code to get all the valid applications, user can access.
                            If grpType = "G" Then
                                groupStr = groupStr + tab_SECTABGRP.GetColumnValue(grpSecLine, 1) + ";"
                            
                                'Need to check whether is there any record available in PRVTAB, if not available then it belongs to Profile.
                                grpCount = tab_PRVTAB.GetLinesByColumnValue(1, grpSecUrno, 0)
                            
                                If ItemCount(grpCount, ",") > 0 Then
                                    'Group is having own profile.
                        
                                    For k = 0 To appCount - 1 Step 1
                                        appUrno = tab_SECTABAPP.GetColumnValue(k, 0)
                                        appKey = tab_PRVTAB.GetLinesByMultipleColumnValue("0,1", appUrno + "," + grpSecUrno, 0)
                                        appCount1 = ItemCount(appKey, ",")
                                        If appCount1 > 0 Then applicationStr = applicationStr + tab_SECTABAPP.GetColumnValue(k, 1)
                                    Next k
                                Else
                                    'Group belongs to another profile ... Need to test, need to change the variable names
                                    grpKey1 = tab_GRPTAB.GetLinesByColumnValue(0, grpSecUrno, 0)
            
                                    ilCnt1 = ItemCount(grpKey1, ",")
                                    For j = 1 To ilCnt1 Step 1
                                        grpLine1 = GetItem(grpKey1, j, ",")
                                        grpSecUrno1 = tab_GRPTAB.GetColumnValues(grpLine1, 1)
                                    
                                        If IsNumeric(tab_SECTABGRP.GetLinesByColumnValue(0, grpSecUrno1, 0)) Then
                                        
                                            grpSecLine1 = tab_SECTABGRP.GetLinesByColumnValue(0, grpSecUrno1, 0)
                                            grpType1 = tab_SECTABGRP.GetColumnValue(grpSecLine1, 3)
                                        
                                            If grpType1 = "P" Then
                                                'secUrno1 = tab_SECTABGRP.GetColumnValue(grpSecLine1, 0)
                                                For k = 0 To appCount - 1 Step 1
                                                    appUrno = tab_SECTABAPP.GetColumnValue(k, 0)
                                                    appKey = tab_PRVTAB.GetLinesByMultipleColumnValue("0,1", appUrno + "," + grpSecUrno1, 0)
                                                    appCount1 = ItemCount(appKey, ",")
                                                    
                                                    If appCount1 > 0 Then applicationStr = applicationStr + tab_SECTABAPP.GetColumnValue(k, 1)
                                                    
                                                Next k
                                            End If
                                        End If
                                    Next j
                                End If
                            Else
                                'kkh
                                grpKey1 = tab_SECTABGRP.GetLinesByColumnValue(0, grpSecUrno, 0)
                                'profileStr = profileStr + tab_SECTABGRP.GetColumnValue(grpSecUrno, 1) + ";"
                                profileStr = profileStr + tab_SECTABGRP.GetColumnValue(grpKey1, 1) + ";"
                                'Take secUrno & fappUrno(for all applications) to check in PRVTAB, If its available create a applicationStr
                                For k = 0 To appCount - 1 Step 1
                                    appUrno = tab_SECTABAPP.GetColumnValue(k, 0)
                                    appKey = tab_PRVTAB.GetLinesByMultipleColumnValue("0,1", appUrno + "," + grpSecUrno, 0)
                                    appCount1 = ItemCount(appKey, ",")
                                    If appCount1 > 0 Then applicationStr = applicationStr + tab_SECTABAPP.GetColumnValue(k, 1)
                                Next k
                            End If
                        End If
                    Next i
                    
                    appStr = ""
                    'To print the applications access. 1 for application access and 0 for no access
                    For k = 0 To appCount - 1 Step 1
                        appName = tab_SECTABAPP.GetColumnValue(k, 1)
                        
                        If InStr(applicationStr, appName) > 0 Then
                            appStr = appStr + "1,"
                        Else
                            appStr = appStr + "0,"
                        End If
                    Next k
                    strLine = tab_SECSTFTAB.GetColumnValues(l, 0) + "," + tab_SECSTFTAB.GetColumnValues(l, 1) + "," + tab_SECSTFTAB.GetColumnValues(l, 2) + "," + tab_SECSTFTAB.GetColumnValues(l, 3) + "," + groupStr + "," + profileStr + "," + appStr
                                
                    tab_STFLOG.InsertTextLine strLine, False
                    secCount = secCount + 1
                Else
                    'MsgBox (tab_SECTAB.GetLinesByColumnValue(0, strkey1, 0))
                End If
            Next l
        End If
        'MsgBox ("" & tab_SECTAB.GetLinesByColumnValue(0, tab_SECSTFTAB.GetColumnValue(0, 0), 0))
        
        tab_STFLOG.ShowHorzScroller True
        'tab_STFLOG.AutoSizeColumns
        'tab_STFLOG.Sort 1, True, True
        tab_STFLOG.EnableHeaderSizing True
        tab_STFLOG.Refresh
        
        StatusBar1.Panels(1).Text = "Ready."
        lblSecStfNo.Caption = "Number of Staff: " & secCount
    
        MousePointer = vbDefault
        
    End If

End Sub

'#######################################################################
'###
'#######################################################################
Private Sub tab_STFJOBS_SendLButtonClick(ByVal LineNo As Long, ByVal ColNo As Long)
    
    Dim strkey1 As String
    Dim strWhere As String
    Dim l As Long
    Dim ret As Long
    Dim flightNumber As String
    Dim JobType As String
    
    l = tab_STFJOBS.GetCurrentSelected
        
    If l <> -1 Then
        ret = GetFlightByUrno(tab_STFJOBS.GetColumnValues(l, 5), flightNumber)
        txtJobFlight = flightNumber
        
        ret = GetJobTypeByUrno(tab_STFJOBS.GetColumnValues(l, 6), JobType)
        txtJobType = JobType
        
        txtJobUrno = tab_STFJOBS.GetColumnValues(l, 0)
        
    End If
    
End Sub



'#######################################################################
'###
'#######################################################################
Private Sub tab_STFTAB_HitKeyOnLine(ByVal Key As Integer, ByVal LineNo As Long)
    If Key = 13 Then calSDAY_DblClick
End Sub

'#######################################################################
'###
'#######################################################################
Private Sub tab_STFTAB_RowSelectionChanged(ByVal LineNo As Long, ByVal Selected As Boolean)
    tab_STFTAB_SendLButtonClick LineNo, 0
End Sub

'#######################################################################
'### DISPLAY SELECTED STAFF DATA
'#######################################################################
Private Sub tab_STFTAB_SendLButtonClick(ByVal LineNo As Long, ByVal ColNo As Long)
    Dim strkey1 As String
    Dim strWhere As String
    Dim l As Long
    Dim llLineCount As Long
    
    l = tab_STFTAB.GetCurrentSelected
        
    If l <> -1 Then
        txtPENO = tab_STFTAB.GetColumnValues(l, 0)
        txtLANM = tab_STFTAB.GetColumnValues(l, 2)
        txtFINM = tab_STFTAB.GetColumnValues(l, 3)
        txtStfUrno = tab_STFTAB.GetColumnValues(l, 1)
                
        tab_DRRTAB.ResetContent
        tab_DRRTAB.Refresh
        tab_LOG.ResetContent
        tab_LOG.Refresh
        
        lblNoLog.Caption = ""
    End If
    
    If LineNo = -1 Then
        tab_STFTAB.Sort ColNo, True, False
        tab_STFTAB.Refresh
    End If
        
End Sub

'#######################################################################
'### LOAD FIRST NAME, LAST NAME & URNO OF THE SELECTED STAFF
'#######################################################################

Private Sub textPeno_LostFocus()

 Dim strQuery As String
'Get the URNO of the staff(peno), if peno is not empty
 If (textPeno <> "") Then
    tab_STFTAB.ResetContent
    strQuery = "where " & "peno = '" & textPeno & "'"
    frmData.LoadData tab_STFTAB, strQuery
    tab_STFTAB.Refresh
    
    'MsgBox (tab_STFTAB.GetLineCount)
    If tab_STFTAB.GetLineCount > 0 Then
        urnoSTF = tab_STFTAB.GetColumnValues(0, 1)
        lastName = tab_STFTAB.GetColumnValues(0, 2)
        firstName = tab_STFTAB.GetColumnValues(0, 3)
        'MsgBox (urnoSTF)
    Else
        MsgBox ("Staff Not Exists, Enter proper PENO")
        urnoSTF = ""
        lastName = ""
        firstName = ""
        textPeno = ""
    End If
 End If
End Sub

'#######################################################################
'### LOAD SHIFT TRANSACTIONS FOR SELECTED STAFF
'#######################################################################
'This code written to display shift transactions in "User Transactions Test Tab"
'This code will execute, When user selects the date, enters personal number & clicks the "Load Transactions" command button

Private Sub userLoadTrans_Click()

    Dim strWhere As String
    Dim l As Long
    Dim findDate As String
    Dim strLine As String
    Dim strFields As String
    Dim strValues As String
    Dim strTemp As String
    Dim strkey1 As String
    Dim fromDt As String
    Dim toDt As String
    
    'Preparing date, according to current date&time-8 & current date&time+12
    fromDt = DateAdd("h", -8, userCal1)
    fromDt = Format(fromDt, "yyyymmddhh")
    fromDt = fromDt & "0000"
    
    toDt = DateAdd("h", 15, userCal1)
    toDt = Format(toDt, "yyyymmddhh")
    toDt = toDt & "5959"
    
    
    'Get the date & username & prepare queries for both R01TAB & R02TAB
    'Need to write queries for both R01TAB & R02TAB
    
    tab_STFTAB.ResetContent
    
    If userStaffNumber <> "" Then

        strWhere = "where "
        If userStaffNumber <> "" Then strWhere = strWhere & "peno = '" & userStaffNumber & "'"
        
        frmData.LoadData tab_STFTAB, strWhere
        tab_STFTAB.Refresh
        
        If tab_STFTAB.GetLineCount > 0 Then
            userStaffNumber = tab_STFTAB.GetColumnValues(l, 0)
            userLastName = tab_STFTAB.GetColumnValues(l, 2)
            userFirstName = tab_STFTAB.GetColumnValues(l, 3)
        Else
            MsgBox ("Staff Not Exists, Enter correct values")
            Exit Sub
        End If
    
    Else
        MsgBox "Please enter PENO of Staff"
        Exit Sub
    End If
    
    
    If userStaffNumber <> "" Then
    
        'This piece of code written To Display the Shift records - Start
        
        MousePointer = vbHourglass
        
        'strWhere = "where usec like '" & userStaffNumber & "%' and time like '" & findDate & "%'"
        strWhere = "where usec = '" & userStaffNumber & "' and time between '" & fromDt & "%' and '" & toDt & "' "
        tab_R01TAB.ResetContent
        frmData.LoadData tab_R01TAB, strWhere
        tab_R01TAB.Sort 6, True, False
        tab_R01TAB.AutoSizeColumns
        tab_R01TAB.Refresh
        
        '### IF AVAILABLE TRANSLATE INTO READABLE FORMAT
        If tab_R01TAB.GetLineCount > 0 Then
        
            tab_LOGUSERR01.ResetContent
            '### CONTENT
            For l = 0 To tab_R01TAB.GetLineCount - 1 Step 1
                
                '### LOG COLUMNS
                strFields = tab_R01TAB.GetColumnValues(l, 1)
                strFields = Replace(strFields, Chr(30), ",")
                strValues = tab_R01TAB.GetColumnValues(l, 2)
                strValues = Replace(strValues, Chr(30), ",")
                
                strLine = tab_R01TAB.GetColumnValues(l, 0)
                strLine = strLine + "," & tab_R01TAB.GetColumnValues(l, 3)
                
                'USTF - Staff PENO
                'strkey1 = GetFieldValue("USTF", strValues, strFields)
                'If strkey1 <> "" Then strkey1 = tab_STFCOMPLETE.GetLinesByColumnValue(1, strkey1, 0)
                'If strkey1 <> "" Then strkey1 = tab_STFCOMPLETE.GetColumnValue(CLng(strkey1), 0)
                'strLine = strLine + "," & strkey1
                
                strLine = strLine + "," & DateAdd("h", gUTCOffset, CedaFullDateToVb(tab_R01TAB.GetColumnValues(l, 4)))
                strLine = strLine + "," + tab_R01TAB.GetColumnValues(l, 5)
                
                'Remarks
                strkey1 = GetFieldValue("REMA", strValues, strFields)
                strLine = strLine + "," & strkey1
                
                'Shift Code
                strkey1 = GetFieldValue("SCOD", strValues, strFields)
                strLine = strLine + "," & strkey1
                
                'Function Code
                strkey1 = GetFieldValue("FCTC", strValues, strFields)
                strLine = strLine + "," & strkey1
                
                'Add the record to Log tab
                tab_LOGUSERR01.InsertTextLine strLine, False
                
            Next l
                
            tab_LOGUSERR01.ShowHorzScroller True
            tab_LOGUSERR01.AutoSizeColumns
            'tab_LOGUSERR01.Sort 2, True, True
            tab_LOGUSERR01.EnableHeaderSizing True
            tab_LOGUSERR01.Refresh
        Else
            MsgBox ("No Shift Transactions made by user" + userStaffNumber)
        End If
        ' Display of shift records - End
        
        MousePointer = vbDefault
         
    Else
        MsgBox "No staff login specified."
    End If

End Sub



'#######################################################################
'### LOAD JOB DATA FOR SELECTED CRITERIA
'#######################################################################
Sub prepareJobtab1()

Dim prevOrno As String
Dim nextOrno As String
Dim strLine As String
Dim strFields As String
Dim strValues As String
Dim strkey1 As String
Dim strkey2 As String
Dim l As Integer

'### IF AVAILABLE TRANSLATE JOBS INTO READABLE FORMAT .. Prepare JOBTAB1
If tab_R02TAB.GetLineCount > 0 Then
    '### CONTENT
    tab_JOBTAB1.ResetContent
    'tab_JOBTAB1.Refresh
    tab_JOBLOG.ResetContent
    'tab_JOBLOG.Refresh
    
    For l = 0 To tab_R02TAB.GetLineCount - 1 Step 1
        '### LOG COLUMNS
        nextOrno = tab_R02TAB.GetColumnValues(l, 0)
        
        If (nextOrno <> prevOrno) Then
            
            strLine = nextOrno
            
            ' Here need to get ACFR & ACTO values for the job.
            
            strFields = tab_R02TAB.GetColumnValues(l, 1)
            strFields = Replace(strFields, Chr(30), ",")
            strValues = tab_R02TAB.GetColumnValues(l, 2)
            strValues = Replace(strValues, Chr(30), ",")
            
            'USTF - Urno Staff - get Staff info & show
            'strkey1 = GetFieldValue("USTF", strValues, strFields)
            'strkey2 = strkey1
            'strkey1 = tab_STFCOMPLETE.GetLinesByColumnValue(1, strkey1, 0)
            'If strkey1 <> "" Then
             '   strkey1 = tab_STFCOMPLETE.GetColumnValue(CLng(strkey1), 0)
            'Else
             '   strkey1 = strkey2
            'End If
            'strLine = strLine + "," & strkey1
            
            'Get USER, who created the job
            'strLine = strLine + "," & tab_R02TAB.GetColumnValues(l, 3)
            
            'code change start
            strkey1 = tab_R02TAB.GetColumnValues(l, 3)
            If strkey1 = "" Or UCase(strkey1) = "EVENTSPOOL" Then
                '### TAKE USEU
                strkey1 = GetFieldValue("USEU", strValues, strFields)
                '### STILL EMPTY TAKE USEC
                If strkey1 = "" Then
                    strkey1 = GetFieldValue("USEC", strValues, strFields)
                End If
                '### STILL EMPTY PUT DEFAULT
                If strkey1 = "" Then
                    strkey1 = "User not available"
                End If
            End If
            If strkey1 = "polhdlr" Then strkey1 = "System"
            
            strLine = strLine + "," & strkey1
            
            'code change end
            
            
            'strLine = tab_R02TAB.GetColumnValues(l, 4)
            'strLine = strLine + "," & CedaFullDateToVb(tab_R02TAB.GetColumnValues(l, 4))
            strLine = strLine + "," & DateAdd("h", gUTCOffset, CedaFullDateToVb(tab_R02TAB.GetColumnValues(l, 4)))
            
            'TEXT
            'strkey1 = GetFieldValue("TEXT", strValues, strFields)
            'strkey1 = DateAdd("h", gUTCOffset, CedaFullDateToVb(strkey1))
            'strkey1 = "test test"
            'strLine = strLine + "," & strkey1
            
            'ACFR - Actual From
            strkey1 = GetFieldValue("ACFR", strValues, strFields)
            If strkey1 <> "" Then strkey1 = DateAdd("h", gUTCOffset, CedaFullDateToVb(strkey1))
            strLine = strLine + "," & strkey1
            
            'ACTO - Actual To
            strkey1 = GetFieldValue("ACTO", strValues, strFields)
            If strkey1 <> "" Then strkey1 = DateAdd("h", gUTCOffset, CedaFullDateToVb(strkey1))
            strLine = strLine + "," & strkey1
            
            'PLFR -  Planned From
            strkey1 = GetFieldValue("PLFR", strValues, strFields)
            If strkey1 <> "" Then strkey1 = DateAdd("h", gUTCOffset, CedaFullDateToVb(strkey1))
            strLine = strLine + "," & strkey1
            
            'PLTO - Planned To
            strkey1 = GetFieldValue("PLTO", strValues, strFields)
            If strkey1 <> "" Then strkey1 = DateAdd("h", gUTCOffset, CedaFullDateToVb(strkey1))
            strLine = strLine + "," & strkey1
            
            'UAFT - Urno Flight
            strkey1 = GetFieldValue("UAFT", strValues, strFields)
            strkey1 = tab_AFTTAB.GetLinesByColumnValue(0, strkey1, 0)
            If strkey1 <> "" Then strkey1 = tab_AFTTAB.GetColumnValue(CLng(strkey1), 1)
            strLine = strLine + "," & strkey1
            
            'UTPL - URNO Template
            strkey1 = GetFieldValue("UTPL", strValues, strFields)
            strkey1 = tab_TPLTAB.GetLinesByColumnValue(0, strkey1, 0)
            If strkey1 <> "" Then strkey1 = tab_TPLTAB.GetColumnValue(CLng(strkey1), 1)
            strLine = strLine + "," & strkey1
            
            'UJTY - URNO JOBTYPE
            strkey1 = GetFieldValue("UJTY", strValues, strFields)
            strkey1 = tab_JTYTAB1.GetLinesByColumnValue(0, strkey1, 0)
            If strkey1 <> "" Then strkey1 = tab_JTYTAB1.GetColumnValue(CLng(strkey1), 1)
            strLine = strLine + "," & strkey1
            
                        
            tab_JOBTAB1.InsertTextLine strLine, False
        End If
        
        prevOrno = nextOrno
        
    Next l

    tab_JOBTAB1.ShowHorzScroller True
    tab_JOBTAB1.AutoSizeColumns
    'tab_JOBTAB1.Sort 1, True, True
    tab_JOBTAB1.EnableHeaderSizing True
    tab_JOBTAB1.Refresh
           
Else
    tab_JOBTAB1.ResetContent
    tab_JOBTAB1.Refresh
    tab_JOBLOG.ResetContent
    tab_JOBLOG.Refresh
    MsgBox ("No Jobs available for search criteria")
End If
        
End Sub


'#######################################################################
'### LOAD JOB DATA FOR SELECTED CRITERIA
'#######################################################################
Sub showJoblog()
    
    Dim l As Integer
    Dim strFields As String
    Dim strValues As String
    Dim strLine As String
    Dim strTemp As String
    

    '### IF AVAILABLE TRANSLATE INTO READABLE FORMAT
    If tab_R02TAB.GetLineCount > 0 Then
    
        tab_JOBLOG.ResetContent
        '### CONTENT
        For l = 0 To tab_R02TAB.GetLineCount - 1 Step 1
            
            '### LOG COLUMNS
            strFields = tab_R02TAB.GetColumnValues(l, 1)
            strFields = Replace(strFields, Chr(30), ",")
            strValues = tab_R02TAB.GetColumnValues(l, 2)
            strValues = Replace(strValues, Chr(30), ",")
                          
            strLine = tab_R02TAB.GetColumnValues(l, 0)
            'strLine = tab_R02TAB.GetColumnValues(l, 3)
            
            '### TIME
            strLine = strLine + "," & CedaFullDateToVb(tab_R02TAB.GetColumnValues(l, 4))
            strLine = strLine + "," + tab_R02TAB.GetColumnValues(l, 5)
            strTemp = SortedR02Fields(strValues, strFields)
            strLine = strLine + "," + strTemp
            
            'Add the record to Log tab
            tab_JOBLOG.InsertTextLine strLine, False
            
        Next l
            
        tab_JOBLOG.ShowHorzScroller True
        tab_JOBLOG.AutoSizeColumns
        'tab_JOBLOG.Sort 1, True, False
        tab_JOBLOG.EnableHeaderSizing True
        tab_JOBLOG.Refresh
            
    Else
        MsgBox ("No job transactions found for selected search criteria")
    End If
    
End Sub

'#######################################################################
'### TO PREPARE QUERY TO LOAD JOBS
'#######################################################################
Function prepareJobQuery(peno As String, selectedDay As String, urnoTemplate As String, urnoJobtype As String, urnoFlight As String)
    Dim RetVal As String
    Dim tmpVal As String
    Dim strQuery As String
    
    Dim fromDate As String
    Dim toDate As String
    
    
    'Preparing date, according to current date&time-8 & current date&time+12
    fromDate = DateAdd("h", -8, flightCalendar)
    fromDate = Format(fromDate, "yyyymmddhh")
    fromDate = fromDate & "0000"
    'MsgBox ("from Date: " & fromDate)
    
    toDate = DateAdd("h", 15, flightCalendar)
    toDate = Format(toDate, "yyyymmddhh")
    toDate = toDate & "5959"
    'MsgBox ("To Date: " & toDate)
    
    
    If (peno = "" And urnoFlight = "" And urnoTemplate = "" And urnoJobtype = "") Then
        MsgBox ("Please Enter Search Criteria")
        tab_JOBTAB1.ResetContent
        tab_JOBTAB1.Refresh
        tab_JOBLOG.ResetContent
        tab_JOBLOG.Refresh
        Exit Function
    Else
        'If peno is not empty
        If (peno <> "") Then strQuery = "where key2 like '" & urnoSTF & "|%' and sday between '" & fromDate & "' and '" & toDate & "'"
        
        'If flight urno is not empty
        If (urnoFlight <> "") Then strQuery = "where key1 like '" & urnoFlight & "|%' and sday between '" & fromDate & "' and '" & toDate & "'"
            
        'If urnoTemplate is not empty
        If (urnoTemplate <> "") Then strQuery = "where sday between '" & fromDate & "' and '" & toDate & "' and key1 like '%" & "|" & urnoTemplate & "%'"
        
        'If urnoJobtype is not empty
        If (urnoJobtype <> "") Then strQuery = "where key3 like '" & urnoJobtype & "|%' and sday between '" & fromDate & "' and '" & toDate & "'"
        
        'If urnoJobtype and urnoTemplate are not empty
        If (urnoJobtype <> "" And urnoTemplate <> "") Then strQuery = "where key3 like '" & urnoJobtype & "|" & urnoTemplate & "|%' and sday between '" & fromDate & "' and '" & toDate & "'"
            
        'If both flightUrno and urnoTemplate not empty
        If (urnoFlight <> "" And urnoTemplate <> "") Then strQuery = "where key1 like '" & urnoFlight & "|" & urnoTemplate & "%' and sday between '" & fromDate & "' and '" & toDate & "'"
        
        'If both flightUrno and urnoJobtype not empty
        If (urnoFlight <> "" And urnoJobtype <> "") Then strQuery = "where key1 like '" & urnoFlight & "|%' and key3 like '" & urnoJobtype & "|%' and sday between '" & fromDate & "' and '" & toDate & "'"
        
        'If flightUrno, urnoJobtype and urnoTemplate not empty
        If (urnoFlight <> "" And urnoTemplate <> "" And urnoJobtype <> "") Then strQuery = "where key1 like '" & urnoFlight & "|" & urnoTemplate & "%' and key3 like '" & urnoJobtype & "|" & urnoTemplate & "|%' and sday between '" & fromDate & "' and '" & toDate & "'"
            
        'If peno, urnoTemplate are not empty
        If (peno <> "" And urnoTemplate <> "") Then strQuery = "where key2 like '" & urnoSTF & "|" & "%' and key1 like '%" & "|" & urnoTemplate & "%' and sday between '" & fromDate & "' and '" & toDate & "'"
        
        'If peno and Flight urno are not empty
        If (peno <> "" And urnoFlight <> "") Then strQuery = "where key2 like '" & urnoSTF & "|" & "%' and key1 like '" & urnoFlight & "|%' and sday between '" & fromDate & "' and '" & toDate & "'"
        
        'If peno and jobtype are not empty
        If (peno <> "" And urnoJobtype <> "") Then strQuery = "where key2 like '" & urnoSTF & "|" & "%' and key3 like '" & urnoJobtype & "|%' and sday between '" & fromDate & "' and '" & toDate & "'"
        
        'If peno, urnoTemplate and urnoJobtype are not empty
        If (peno <> "" And urnoTemplate <> "" And urnoJobtype <> "") Then strQuery = "where key2 like '" & urnoSTF & "|" & "%' and key3 like '" & urnoJobtype & "|" & urnoTemplate & "|%' and sday between '" & fromDate & "' and '" & toDate & "'"
        
        'If peno, flight urno and urnoTemplate are not empty
        If (peno <> "" And urnoFlight <> "" And urnoTemplate <> "") Then strQuery = "where key2 like '" & urnoSTF & "|" & "%' and key1 like '" & urnoFlight & "|" & urnoTemplate & "%' and sday between '" & fromDate & "' and '" & toDate & "'"
            
        'If peno, flight urno and urnoJobtype are not empty
        If (peno <> "" And urnoFlight <> "" And urnoJobtype <> "") Then strQuery = "where key2 like '" & urnoSTF & "|" & "%' and key1 like '" & urnoFlight & "|%' and key3 like '" & urnoJobtype & "|%' and sday between '" & fromDate & "' and '" & toDate & "'"
        
        
        'If peno, flight urno, urnoTemplate and urnoJobtype are not empty
        If (peno <> "" And urnoFlight <> "" And urnoTemplate <> "" And urnoJobtype <> "") Then strQuery = "where key2 like '" & urnoSTF & "|" & "%' and key1 like '" & urnoFlight & "|" & urnoTemplate & "%' and key3 like '" & urnoJobtype & "|" & urnoTemplate & "|%' and sday between '" & fromDate & "' and '" & toDate & "'"
        
        'MsgBox (strQuery)
        
        prepareJobQuery = strQuery
    End If
    
End Function


'#######################################################################
'### PREPARE TEMPLATE TABLE - LOOK & FEEL
'#######################################################################
Sub prepareTpltab()
    
    Dim l As Integer
    Dim i As Integer
    Dim temp As String
    Dim strKey As String
    
    
    If tab_TPLTAB.GetLineCount > 0 Then
        
        i = tab_TPLTAB.GetLineCount
        
        For l = 0 To i - 1 Step 1
            
            strKey = tab_TPLTAB.GetColumnValues(l, 2)
            
            If strKey = "RULE_AFT" Then
                temp = "Flight Related"
            Else
                temp = "Flight Independent"
            End If
            
            tab_TPLTAB.SetColumnValue l, 2, temp
        Next l
    End If
End Sub
    
'#######################################################################
'### LOAD JOB TRANSACTIONS FOR SELECTED STAFF
'#######################################################################
'This code written to display shift transactions in "User Transactions Test Tab"
'This code will execute, When user selects the date, enters personal number & clicks the "Load Transactions" command button

Private Sub userLoadTrans1_Click()
    Dim strWhere As String
    Dim l As Long
    Dim findDate As String
    Dim strLine As String
    Dim strFields As String
    Dim strValues As String
    Dim strTemp As String
    Dim strkey1 As String
    Dim fromDt As String
    Dim toDt As String
    
    'Preparing date, according to current date&time-8 & current date&time+12
    fromDt = DateAdd("h", -8, userCal1)
    fromDt = Format(fromDt, "yyyymmddhh")
    fromDt = fromDt & "0000"
    
    toDt = DateAdd("h", 15, userCal1)
    toDt = Format(toDt, "yyyymmddhh")
    toDt = toDt & "5959"
    
    
    'Get the date & username & prepare queries for both R01TAB & R02TAB
    'Need to write queries for both R01TAB & R02TAB
    
    tab_STFTAB.ResetContent
    
    If userStaffNumber <> "" Then

        strWhere = "where "
        If userStaffNumber <> "" Then strWhere = strWhere & "peno = '" & userStaffNumber & "'"
        
        frmData.LoadData tab_STFTAB, strWhere
        tab_STFTAB.Refresh
        
        If tab_STFTAB.GetLineCount > 0 Then
            userStaffNumber = tab_STFTAB.GetColumnValues(l, 0)
            userLastName = tab_STFTAB.GetColumnValues(l, 2)
            userFirstName = tab_STFTAB.GetColumnValues(l, 3)
        Else
            MsgBox ("Staff Not Exists, Enter correct values")
            Exit Sub
        End If
    
    Else
        MsgBox "Please enter PENO of Staff"
        Exit Sub
    End If
    
    
    If userStaffNumber <> "" Then
    
        'This piece of code written To Display the Shift records - Start
        
        MousePointer = vbHourglass
        
        'strWhere = "where usec like '" & userStaffNumber & "%' and time like '" & findDate & "%'"
        strWhere = "where usec = '" & userStaffNumber & "' and time between '" & fromDt & "%' and '" & toDt & "' "
        'This piece of code written To Display the Job Transaction records
        tab_R02TAB.ResetContent
        frmData.LoadData tab_R02TAB, strWhere
        tab_R02TAB.Sort 6, True, False
        tab_R02TAB.AutoSizeColumns
        tab_R02TAB.Refresh
        
        
        '### IF AVAILABLE TRANSLATE INTO READABLE FORMAT
        If tab_R02TAB.GetLineCount > 0 Then
        
            tab_LOGUSERR02.ResetContent
            '### CONTENT
            For l = 0 To tab_R02TAB.GetLineCount - 1 Step 1
                
                '### LOG COLUMNS
                strFields = tab_R02TAB.GetColumnValues(l, 1)
                strFields = Replace(strFields, Chr(30), ",")
                strValues = tab_R02TAB.GetColumnValues(l, 2)
                strValues = Replace(strValues, Chr(30), ",")
                              
                'strLine = userStaffNumber
                
                '### TIME
                'strLine = strLine + "," & CedaFullDateToVb(tab_R02TAB.GetColumnValues(l, 4))
                'strLine = strLine + "," + tab_R02TAB.GetColumnValues(l, 5)
                'strTemp = SortedR02Fields(strValues, strFields)
                'strLine = strLine + "," + strTemp
                
                strLine = tab_R02TAB.GetColumnValues(l, 0)
                
                strkey1 = GetFieldValue("USTF", strValues, strFields)
                If strkey1 <> "" Then strkey1 = tab_STFCOMPLETE.GetLinesByColumnValue(1, strkey1, 0)
                If strkey1 <> "" Then strkey1 = tab_STFCOMPLETE.GetColumnValue(CLng(strkey1), 0)
                strLine = strLine + "," & strkey1
                
                'strLine = strLine + "," & CedaFullDateToVb(tab_R02TAB.GetColumnValues(l, 4))
                strLine = strLine + "," & DateAdd("h", gUTCOffset, CedaFullDateToVb(tab_R02TAB.GetColumnValues(l, 4)))
                strLine = strLine + "," + tab_R02TAB.GetColumnValues(l, 5)
                
                'UGHS - Service
                strkey1 = GetFieldValue("UGHS", strValues, strFields)
                If strkey1 <> "" Then strkey1 = tab_SERTAB.GetLinesByColumnValue(0, strkey1, 0)
                If strkey1 <> "" Then strkey1 = tab_SERTAB.GetColumnValue(CLng(strkey1), 1)
                strLine = strLine + "," & strkey1
                
                
                'ACFR - Actual From
                strkey1 = GetFieldValue("ACFR", strValues, strFields)
                If strkey1 <> "" Then strkey1 = DateAdd("h", gUTCOffset, CedaFullDateToVb(strkey1))
                strLine = strLine + "," & strkey1
                
                'ACTO - Actual To
                strkey1 = GetFieldValue("ACTO", strValues, strFields)
                If strkey1 <> "" Then strkey1 = DateAdd("h", gUTCOffset, CedaFullDateToVb(strkey1))
                strLine = strLine + "," & strkey1
                
                'Add the record to Log tab
                tab_LOGUSERR02.InsertTextLine strLine, False
                
            Next l
            
            tab_LOGUSERR02.ShowHorzScroller True
            tab_LOGUSERR02.AutoSizeColumns
            'tab_LOGUSERR02.Sort 2, True, True
            tab_LOGUSERR02.EnableHeaderSizing True
            tab_LOGUSERR02.Refresh
        Else
            MsgBox ("No Job Transactions made by user" + userStaffNumber)
        End If
        'Display of  records - End
        MousePointer = vbDefault
    Else
        MsgBox "No staff login specified."
    End If
End Sub

Private Sub userReset_Click()
    userStaffNumber = ""
    userLastName = ""
    userFirstName = ""
    tab_R01TAB.ResetContent
    tab_R01TAB.Refresh
    tab_R02TAB.ResetContent
    tab_R02TAB.Refresh
    tab_LOGUSERR01.ResetContent
    tab_LOGUSERR01.Refresh
    tab_LOGUSERR02.ResetContent
    tab_LOGUSERR02.Refresh
End Sub
