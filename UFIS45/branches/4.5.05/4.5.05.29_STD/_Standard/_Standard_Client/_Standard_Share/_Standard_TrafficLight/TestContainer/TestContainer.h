// lb_container.h : main header file for the LB_CONTAINER application
//

#if !defined(AFX_LB_CONTAINER_H__8942EEDE_9796_11D5_9970_0000865098D4__INCLUDED_)
#define AFX_LB_CONTAINER_H__8942EEDE_9796_11D5_9970_0000865098D4__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include <resource.h>		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CLb_containerApp:
// See lb_container.cpp for the implementation of this class
//

class CLb_containerApp : public CWinApp
{
public:
	CLb_containerApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CLb_containerApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CLb_containerApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_LB_CONTAINER_H__8942EEDE_9796_11D5_9970_0000865098D4__INCLUDED_)
