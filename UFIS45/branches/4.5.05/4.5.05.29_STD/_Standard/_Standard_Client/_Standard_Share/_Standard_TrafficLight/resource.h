//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by TrafficLight.rc
//
#define IDS_AMP_TEST                    1
#define IDD_ABOUTBOX_AMP_TEST           1
#define IDB_AMP_TEST                    1
#define IDI_ABOUTDLL                    1
#define IDS_AMP_TEST_PPG                2
#define IDR_AMP_TEST                    102
#define IDT_TIMER1                      103
#define IDT_TIMER2                      105
#define IDS_AMP_TEST_PPG_CAPTION        200
#define IDD_PROPPAGE_AMP_TEST           200
#define IDB_AMP_RO                      201
#define IDC_CHECK_MESSAGE               201
#define IDS_AMP_PPG_GRAPHICS            201
#define IDB_AMP_GR                      202
#define IDC_SLIDER1                     202
#define IDB_AMP_GE                      203
#define IDC_PERIOD                      203
#define IDB_AMP_RO_HI                   204
#define IDC_TIME_RED                    204
#define IDB_AMP_GR_HI                   205
#define IDC_TIME_YELLOW                 205
#define IDB_AMP_GE_HI                   206
#define IDC_TIME_GREEN                  206
#define IDC_RESTORE                     207
#define IDB_AMP_ALL                     207
#define IDB_AMP_KL_RO                   208
#define IDC_PIC                         209
#define IDB_AMP_KL_GR                   209
#define IDB_AMP_KL_GE                   210
#define IDC_INTERVAL                    210
#define IDD_PROPPAGE_2                  211
#define IDC_RADIO1                      211
#define IDB_AMP_BUT_GR                  212
#define IDC_RADIO2                      212
#define IDB_AMP_BUT_GE                  213
#define IDB_AMP_BUT_RO                  214

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        215
#define _APS_NEXT_COMMAND_VALUE         32768
#define _APS_NEXT_CONTROL_VALUE         213
#define _APS_NEXT_SYMED_VALUE           106
#endif
#endif
