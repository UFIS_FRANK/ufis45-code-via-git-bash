#if !defined(AFX_AUTO_TEST_H__482A4E87_92ED_11D5_996A_0000865098D4__INCLUDED_)
#define AFX_AUTO_TEST_H__482A4E87_92ED_11D5_996A_0000865098D4__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Auto_test.h : header file
//



/////////////////////////////////////////////////////////////////////////////
// Auto_test command target

class Auto_test : public CCmdTarget
{
	DECLARE_DYNCREATE(Auto_test)

	Auto_test();           // protected constructor used by dynamic creation

// Attributes
public:
	void SetControl(CAmp_testCtrl *pCtrl);
	
	
// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(Auto_test)
	public:
	virtual void OnFinalRelease();
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual ~Auto_test();
	CAmp_testCtrl* m_pCtrl;
	// Generated message map functions
	//{{AFX_MSG(Auto_test)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
	// Generated OLE dispatch map functions
	//{{AFX_DISPATCH(Auto_test)
		afx_msg BOOL OnBroadcast(	LPCTSTR ReqId, LPCTSTR Dest1, LPCTSTR Dest2, LPCTSTR Cmd, LPCTSTR Object, LPCTSTR Seq, LPCTSTR Tws, LPCTSTR Twe, LPCTSTR Selection, LPCTSTR Fields, LPCTSTR Data, LPCTSTR BcNum);
	//}}AFX_DISPATCH
	DECLARE_DISPATCH_MAP()
	DECLARE_INTERFACE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_AUTO_TEST_H__482A4E87_92ED_11D5_996A_0000865098D4__INCLUDED_)
