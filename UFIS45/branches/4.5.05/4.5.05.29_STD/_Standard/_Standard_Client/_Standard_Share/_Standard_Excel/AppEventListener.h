#if !defined(AFX_APPEVENTLISTENER_H__5B7F9BC0_624E_494D_A7EA_C8DC9C271A1B__INCLUDED_)
#define AFX_APPEVENTLISTENER_H__5B7F9BC0_624E_494D_A7EA_C8DC9C271A1B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// AppEventListener.h : header file
//



/////////////////////////////////////////////////////////////////////////////
// CAppEventListener command target

class CAppEventListener : public CCmdTarget
{
	DECLARE_DYNCREATE(CAppEventListener)

	CAppEventListener();           // protected constructor used by dynamic creation

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAppEventListener)
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual ~CAppEventListener();

	// Generated message map functions
	//{{AFX_MSG(CAppEventListener)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_APPEVENTLISTENER_H__5B7F9BC0_624E_494D_A7EA_C8DC9C271A1B__INCLUDED_)
