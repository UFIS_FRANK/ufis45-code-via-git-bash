﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Ufis.Broadcast
{
    /// <summary>
    /// Event arguments for ufis broadcast.
    /// </summary>
    public class BroadcastEventArgs: EventArgs
    {
        public BroadcastMessageBase BroadcastMessage;
        public bool Handled;
    }
}
