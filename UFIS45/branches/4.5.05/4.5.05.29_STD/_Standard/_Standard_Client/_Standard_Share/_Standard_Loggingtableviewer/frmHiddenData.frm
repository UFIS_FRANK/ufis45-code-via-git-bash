VERSION 5.00
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "TAB.ocx"
Object = "{6FBA474E-43AC-11CE-9A0E-00AA0062BB4C}#1.0#0"; "SYSINFO.OCX"
Begin VB.Form frmHiddenData 
   Caption         =   "Hidden Data ..."
   ClientHeight    =   9555
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   8925
   Icon            =   "frmHiddenData.frx":0000
   LinkTopic       =   "Form2"
   ScaleHeight     =   9555
   ScaleWidth      =   8925
   StartUpPosition =   3  'Windows Default
   Begin SysInfoLib.SysInfo SysInfo 
      Left            =   3360
      Top             =   5850
      _ExtentX        =   1005
      _ExtentY        =   1005
      _Version        =   393216
   End
   Begin TABLib.TAB tabData 
      Height          =   1095
      Index           =   0
      Left            =   60
      TabIndex        =   0
      Top             =   300
      Width           =   8535
      _Version        =   65536
      _ExtentX        =   15055
      _ExtentY        =   1931
      _StockProps     =   64
   End
   Begin TABLib.TAB tabData 
      Height          =   1995
      Index           =   1
      Left            =   60
      TabIndex        =   2
      Top             =   1680
      Width           =   8535
      _Version        =   65536
      _ExtentX        =   15055
      _ExtentY        =   3519
      _StockProps     =   64
   End
   Begin TABLib.TAB tabData 
      Height          =   1275
      Index           =   2
      Left            =   60
      TabIndex        =   4
      Top             =   3960
      Width           =   8535
      _Version        =   65536
      _ExtentX        =   15055
      _ExtentY        =   2249
      _StockProps     =   64
   End
   Begin TABLib.TAB tabData 
      Height          =   1395
      Index           =   3
      Left            =   60
      TabIndex        =   6
      Top             =   5520
      Width           =   2415
      _Version        =   65536
      _ExtentX        =   4260
      _ExtentY        =   2461
      _StockProps     =   64
   End
   Begin VB.Label lblTab 
      Caption         =   "SECTAB: Recordcount = 0"
      Height          =   315
      Index           =   3
      Left            =   60
      TabIndex        =   7
      Top             =   5280
      Width           =   2355
   End
   Begin VB.Label lblTab 
      Caption         =   "L0<X>TAB: Recordcount = 0"
      Height          =   255
      Index           =   2
      Left            =   60
      TabIndex        =   5
      Top             =   3720
      Width           =   8535
   End
   Begin VB.Label lblTab 
      Caption         =   "SYSTAB: Recordcount = 0"
      Height          =   255
      Index           =   1
      Left            =   60
      TabIndex        =   3
      Top             =   1440
      Width           =   2835
   End
   Begin VB.Label lblTab 
      Caption         =   "TABTAB: Recordcount = 0"
      Height          =   255
      Index           =   0
      Left            =   60
      TabIndex        =   1
      Top             =   60
      Width           =   8535
   End
End
Attribute VB_Name = "frmHiddenData"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
'============================================
' Loads for AFT special for URNO the records
' into tabData(2) and extracts the fields
Public Sub LoadForAft(strUrno As String)
    Dim strWhere As String
'***Load the SYSTAB but only with AFTTAB fields
    tabData(1).ResetContent
    tabData(1).HeaderString = "TANA,FINA,ADDI,LOGD"
    tabData(1).HeaderLengthString = "100,100,100,100"
    tabData(1).LogicalFieldList = "TANA,FINA,ADDI,LOGD"
    tabData(1).CedaServerName = strServer
    tabData(1).CedaPort = "3357"
    tabData(1).CedaHopo = strHOPO
    tabData(1).CedaCurrentApplication = "LogTabViewer" & "," & GetApplVersion(True)
    tabData(1).CedaTabext = "TAB"
    tabData(1).CedaUser = GetWindowsUserName()
    tabData(1).CedaWorkstation = GetWorkStationName()
    tabData(1).CedaSendTimeout = "3"
    tabData(1).CedaReceiveTimeout = "240"
    tabData(1).CedaRecordSeparator = Chr(10)
    tabData(1).CedaIdentifier = "LogTab"
    tabData(1).ShowHorzScroller True
    tabData(1).EnableHeaderSizing True
    strWhere = " WHERE TANA='AFT' ORDER BY FINA"
    tabData(1).CedaAction "RT", "SYSTAB", tabData(1).HeaderString, "", strWhere
    tabData(1).RedrawTab
    
'***Load the L01TAB
    tabData(2).ResetContent
    tabData(2).HeaderString = "URNO,ORNO,SFKT,STAB,TIME,USEC,FLST,FVAL"
    tabData(2).HeaderLengthString = "80,80,80,80,100,80,200,200"
    tabData(2).LogicalFieldList = "URNO,ORNO,SFKT,STAB,TIME,USEC,FLST,FVAL"
    tabData(2).CedaServerName = strServer
    tabData(2).CedaPort = "3357"
    tabData(2).CedaHopo = strHOPO
    tabData(2).CedaCurrentApplication = "LogTabViewer" & "," & GetApplVersion(True)
    tabData(2).CedaTabext = "TAB"
    tabData(2).CedaUser = GetWindowsUserName()
    tabData(2).CedaWorkstation = GetWorkStationName()
    tabData(2).CedaSendTimeout = "3"
    tabData(2).CedaReceiveTimeout = "240"
    tabData(2).CedaRecordSeparator = Chr(10)
    tabData(2).CedaIdentifier = "LogTab"
    tabData(2).ShowHorzScroller True
    tabData(2).EnableHeaderSizing True
    strWhere = " WHERE ORNO='" + strUrno + "' ORDER BY TIME"
    tabData(2).CedaAction "RT", "L01TAB", tabData(2).HeaderString, "", strWhere
    tabData(2).RedrawTab
End Sub
'=================================================
' Loads systtab, tabtab and extracts the logging
' relations for each table to the tab in the
' frmLoggingTables as unique relations
'=================================================
Public Sub PrepareLoggingTableView()
    Dim myTab As TABLib.TAB
    Dim i As Long
    Dim cnt As Long
    Dim strWhere As String
    Dim strBuffer As String
    Dim strTana As String
    Dim strLogd As String
    Dim strDesc As String
    Dim strLine As String
    Dim ilItem As Long
    
    For i = 0 To tabData(1).GetLineCount - 1
        strTana = tabData(1).GetFieldValue(i, "TANA")
        strLogd = tabData(1).GetFieldValue(i, "LOGD")
        If strTana <> "" And strLogd <> "" And InStr(strLogd, "L0") > 0 Then
            strBuffer = strTana + "," + strLogd + ","
            frmLoggingTables.tabData.InsertTextLine strBuffer, False
        End If
    Next i
    frmLoggingTables.tabData.Refresh
    Set myTab = frmLoggingTables.tabData
    For i = 0 To myTab.GetLineCount - 1
        strTana = myTab.GetColumnValue(i, 0)
        strLine = tabData(0).GetLinesByColumnValue(0, strTana, 0)
        If Len(strLine) > 0 Then
            ilItem = GetRealItem(strLine, 0, ",")
            strDesc = tabData(0).GetFieldValue(ilItem, "LNAM")
            myTab.SetColumnValue i, 2, strDesc
        End If
    Next i
    myTab.Refresh
End Sub


Private Sub Form_Load()
    Dim strWhere As String
    Dim i As Long
    Dim strTmp As String
    
    If strUseAppFor = "COMMON" Then
    '***Load SYSTAB
        tabData(1).ResetContent
        tabData(1).HeaderString = "TANA,FINA,ADDI,LOGD"
        tabData(1).HeaderLengthString = "100,100,100,100"
        tabData(1).LogicalFieldList = "TANA,FINA,ADDI,LOGD"
        tabData(1).CedaServerName = strServer
        tabData(1).CedaPort = "3357"
        tabData(1).CedaHopo = strHOPO
        tabData(1).CedaCurrentApplication = "LogTabViewer" & "," & GetApplVersion(True)
        tabData(1).CedaTabext = "TAB"
        tabData(1).CedaUser = GetWindowsUserName()
        tabData(1).CedaWorkstation = GetWorkStationName()
        tabData(1).CedaSendTimeout = "3"
        tabData(1).CedaReceiveTimeout = "240"
        tabData(1).CedaRecordSeparator = Chr(10)
        tabData(1).CedaIdentifier = "LogTab"
        tabData(1).ShowHorzScroller True
        tabData(1).EnableHeaderSizing True
        strWhere = " ORDER BY TANA,FINA"
        tabData(1).CedaAction "RT", "SYSTAB", tabData(1).HeaderString, "", strWhere
        tabData(1).RedrawTab
        lblTab(1).Caption = "SYSTAB: Records = " & CStr(tabData(1).GetLineCount())
        
        'Replace the H00,H01,H02,H03 Table names for loggin to L00,L01,L02,L03
        For i = 0 To tabData(1).GetLineCount
            strTmp = tabData(1).GetFieldValue(i, "LOGD")
            If InStr(1, strTmp, "H") > 0 Then
                strTmp = Replace(strTmp, "H00TAB", "L00TAB")
                strTmp = Replace(strTmp, "H01TAB", "L01TAB")
                strTmp = Replace(strTmp, "H02TAB", "L02TAB")
                tabData(1).SetFieldValues i, "LOGD", strTmp
            End If
        Next i
        
    '***Init L0X but load nothing
        tabData(2).ResetContent
        tabData(2).HeaderString = "URNO,ORNO,SFKT,STAB,TIME,USEC,FLST,FVAL"
        tabData(2).HeaderLengthString = "80,80,80,80,100,80,200,200"
        tabData(2).LogicalFieldList = "URNO,ORNO,SFKT,STAB,TIME,USEC,FLST,FVAL"
        
    '***Load TABTAB
        tabData(0).ResetContent
        tabData(0).HeaderString = "LTNA,LNAM"
        tabData(0).HeaderLengthString = "100,200"
        tabData(0).LogicalFieldList = "LTNA,LNAM"
        tabData(0).CedaServerName = strServer
        tabData(0).CedaPort = "3357"
        tabData(0).CedaHopo = strHOPO
        tabData(0).CedaCurrentApplication = "LogTabViewer" & "," & GetApplVersion(True)
        tabData(0).CedaTabext = "TAB"
        tabData(0).CedaUser = GetWindowsUserName()
        tabData(0).CedaWorkstation = GetWorkStationName()
        tabData(0).CedaSendTimeout = "3"
        tabData(0).CedaReceiveTimeout = "240"
        tabData(0).CedaRecordSeparator = Chr(10)
        tabData(0).CedaIdentifier = "LogTab"
        tabData(0).ShowHorzScroller True
        tabData(0).EnableHeaderSizing True
        strWhere = ""
        tabData(0).CedaAction "RT", "TABTAB", tabData(0).HeaderString, "", strWhere
        tabData(0).RedrawTab
        lblTab(0).Caption = "TABTAB: Records = " & CStr(tabData(0).GetLineCount())
    '***Load the SECTAB
        tabData(3).ResetContent
        tabData(3).HeaderString = "USID"
        tabData(3).HeaderLengthString = "100"
        tabData(3).LogicalFieldList = "USID"
        tabData(3).CedaServerName = strServer
        tabData(3).CedaPort = "3357"
        tabData(3).CedaHopo = strHOPO
        tabData(3).CedaCurrentApplication = "LogTabViewer" & "," & GetApplVersion(True)
        tabData(3).CedaTabext = "TAB"
        tabData(3).CedaUser = GetWindowsUserName()
        tabData(3).CedaWorkstation = GetWorkStationName()
        tabData(3).CedaSendTimeout = "3"
        tabData(3).CedaReceiveTimeout = "240"
        tabData(3).CedaRecordSeparator = Chr(10)
        tabData(3).CedaIdentifier = "LogTab"
        tabData(3).ShowHorzScroller True
        tabData(3).EnableHeaderSizing True
        strWhere = " WHERE TYPE='U'"
        tabData(3).CedaAction "RT", "SECTAB", tabData(3).HeaderString, "", strWhere
        tabData(3).RedrawTab
    End If
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    If UnloadMode = 0 Then
        Cancel = True
    End If
End Sub

Public Function GetApplVersion(ShowExtended As Boolean) As String
    Dim tmpTxt As String
    Dim tmpMajor As String
    Dim tmpMinor As String
    Dim tmpRevis As String
    tmpMajor = App.Major
    tmpMinor = App.Minor
    tmpRevis = App.Revision
    tmpTxt = ""
    Select Case SysInfo.OSPlatform
        Case 0  'Win32s
        Case 1  'Win 95/98
        Case 2  'Win NT
            If SysInfo.OSVersion > 4 Then
                'Win 2000
                If ShowExtended Then tmpRevis = "0." & tmpRevis
            Else
                'Win NT 4
                If ShowExtended Then
                    tmpMinor = right("0000" & tmpMinor, 2)
                    tmpRevis = right("0000" & tmpRevis, 4)
                End If
            End If
        Case Else
    End Select
    tmpTxt = tmpTxt & tmpMajor & "." & tmpMinor & "." & tmpRevis
    GetApplVersion = tmpTxt
End Function


