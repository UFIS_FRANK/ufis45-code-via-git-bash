VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmLoadDefinition 
   Caption         =   "Security Logging Analyser ..."
   ClientHeight    =   3195
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   4185
   Icon            =   "frmLoadDefinition.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   3195
   ScaleWidth      =   4185
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton cmdScanCurrFilter 
      Caption         =   "Scan Curr. Filter"
      Height          =   315
      Left            =   900
      TabIndex        =   18
      Top             =   0
      Width           =   1575
   End
   Begin MSComctlLib.ProgressBar progress 
      Height          =   255
      Left            =   60
      TabIndex        =   17
      Top             =   2880
      Width           =   4035
      _ExtentX        =   7117
      _ExtentY        =   450
      _Version        =   393216
      Appearance      =   1
   End
   Begin VB.TextBox txtLogTable 
      Height          =   285
      Left            =   3060
      TabIndex        =   15
      Top             =   2520
      Width           =   1035
   End
   Begin VB.TextBox txtTable 
      Height          =   285
      Left            =   600
      TabIndex        =   13
      Top             =   2520
      Width           =   1035
   End
   Begin VB.ComboBox cbField 
      Height          =   315
      Left            =   600
      TabIndex        =   12
      Top             =   1920
      Width           =   3495
   End
   Begin VB.ComboBox cbUsers 
      Height          =   315
      Left            =   600
      TabIndex        =   10
      Top             =   1560
      Width           =   3495
   End
   Begin VB.ComboBox cbType 
      Height          =   315
      Left            =   600
      TabIndex        =   8
      Top             =   900
      Width           =   3495
   End
   Begin VB.TextBox txtToTime 
      Height          =   285
      Left            =   3480
      TabIndex        =   5
      Text            =   "0000"
      Top             =   360
      Width           =   615
   End
   Begin VB.TextBox txtToDate 
      Height          =   285
      Left            =   2520
      TabIndex        =   4
      Text            =   "20.03.2003"
      Top             =   360
      Width           =   975
   End
   Begin VB.TextBox txtFromTime 
      Height          =   285
      Left            =   1560
      TabIndex        =   3
      Text            =   "0000"
      Top             =   360
      Width           =   615
   End
   Begin VB.TextBox txtFromDate 
      Height          =   285
      Left            =   600
      TabIndex        =   2
      Text            =   "20.03.2003"
      Top             =   360
      Width           =   975
   End
   Begin VB.CommandButton cmdLoad 
      Caption         =   "&Load"
      Height          =   315
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   855
   End
   Begin VB.Label Label3 
      Caption         =   "Log-Table:"
      Height          =   195
      Left            =   2220
      TabIndex        =   16
      Top             =   2580
      Width           =   795
   End
   Begin VB.Line Line3 
      BorderWidth     =   2
      X1              =   0
      X2              =   4200
      Y1              =   780
      Y2              =   780
   End
   Begin VB.Label Label7 
      Caption         =   "Table:"
      Height          =   195
      Left            =   60
      TabIndex        =   14
      Top             =   2580
      Width           =   495
   End
   Begin VB.Line Line2 
      BorderWidth     =   2
      X1              =   0
      X2              =   4200
      Y1              =   2400
      Y2              =   2400
   End
   Begin VB.Label Label6 
      Caption         =   "Field:"
      Height          =   195
      Left            =   60
      TabIndex        =   11
      Top             =   1980
      Width           =   495
   End
   Begin VB.Label Label5 
      Caption         =   "User:"
      Height          =   195
      Left            =   60
      TabIndex        =   9
      Top             =   1620
      Width           =   495
   End
   Begin VB.Line Line1 
      BorderWidth     =   2
      X1              =   0
      X2              =   4200
      Y1              =   1380
      Y2              =   1380
   End
   Begin VB.Label Label4 
      Caption         =   "Type:"
      Height          =   195
      Left            =   60
      TabIndex        =   7
      Top             =   960
      Width           =   495
   End
   Begin VB.Label Label2 
      Caption         =   "to"
      Height          =   195
      Left            =   2220
      TabIndex        =   6
      Top             =   420
      Width           =   255
   End
   Begin VB.Label Label1 
      Caption         =   "Period:"
      Height          =   195
      Left            =   60
      TabIndex        =   1
      Top             =   420
      Width           =   495
   End
End
Attribute VB_Name = "frmLoadDefinition"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public bgReload As Boolean
Private Sub cbField_Change()
'    If bgReload = True Then frmMain.SetGridFromLogTable
End Sub

Private Sub cbField_Click()
    If bgReload = True Then frmMain.SetGridFromLogTable
End Sub

Private Sub cbType_Change()
'    If bgReload = True Then frmMain.SetGridFromLogTable
End Sub

Private Sub cbType_Click()
    If bgReload = True Then frmMain.SetGridFromLogTable
End Sub

Private Sub cbUsers_Change()
'    If bgReload = True Then frmMain.SetGridFromLogTable
End Sub

Private Sub cbUsers_Click()
    If bgReload = True Then frmMain.SetGridFromLogTable
End Sub

Private Sub cmdLoad_Click()
    Dim strWhere As String
    LoadMyData
End Sub


Private Sub cmdScanCurrFilter_Click()
    frmMain.SetGridFromLogTable
End Sub

Private Sub Form_Load()
    Dim ilLeft As Integer
    Dim ilTop As Integer
    Dim i As Long
    Dim cnt As Long
    Dim today As Date
    
    today = Now
'Read position from registry and move the window
    ilLeft = GetSetting(AppName:="LogTabViewer_LoadDefs", _
                        section:="Startup", _
                        Key:="frmMainLeft", _
                        Default:=-1)
    ilTop = GetSetting(AppName:="LogTabViewer_LoadDefs", _
                        section:="Startup", _
                        Key:="frmMainTop", _
                        Default:=-1)
    If ilTop = -1 Or ilLeft = -1 Then
        Me.Move 0, 0
    Else
        Me.Move ilLeft, ilTop
    End If
    
    cbUsers.AddItem "**ALL USERS**"
    For i = 0 To frmHiddenData.tabData(3).GetLineCount
        cbUsers.AddItem frmHiddenData.tabData(3).GetFieldValue(i, "USID")
    Next i
    
    cbUsers.Text = "**ALL USERS**"
    cbField.AddItem "**ALL FIELDS**"
    cbField.Text = "**ALL FIELDS**"
    cbType.AddItem "**ALL COMMANDS**"
    cbType.AddItem "IRT"
    cbType.AddItem "URT"
    cbType.AddItem "DRT"
    cbType.AddItem "ISF"
    cbType.AddItem "UFR"
'    cbType.AddItem ""
    
    cbType.Text = "**ALL COMMANDS**"
    txtFromDate = Format(today, "DD.MM.YYYY")
    txtFromTime = "0000"
    txtToDate = Format(today, "DD.MM.YYYY")
    txtToTime = "2359"
'END: Read position from registry and move the window
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    If UnloadMode = 0 Then
        Cancel = True
    End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Dim ilLeft As Integer
    Dim ilTop As Integer

    ilLeft = Me.left
    ilTop = Me.top
    SaveSetting "LogTabViewer_LoadDefs", _
                "Startup", _
                "frmMainLeft", _
                ilLeft
    SaveSetting "LogTabViewer_LoadDefs", _
                "Startup", _
                "frmMainTop", ilTop
End Sub
'===============================================
' generates the where string from the field
' settings
'===============================================
Public Sub LoadMyData()
    Dim strFromDate As String
    Dim strToDate As String
    Dim strCedaFrom As String
    Dim strCedaTo As String
    Dim datFrom As Date
    Dim datTo As Date
    Dim strTable As String
    Dim strLogTab As String
    Dim strUser As String
    Dim strType As String
    Dim strWhere As String
    Dim datReadTo As Date
    Dim tmpFr As String
    Dim tmpTo As String
    Dim diffValue As Integer
    
    frmHiddenData.tabData(2).ResetContent
    frmHiddenData.tabData(2).HeaderString = "URNO,ORNO,SFKT,STAB,TIME,USEC,FLST,FVAL"
    frmHiddenData.tabData(2).HeaderLengthString = "80,80,80,80,100,80,200,200"
    frmHiddenData.tabData(2).LogicalFieldList = "URNO,ORNO,SFKT,STAB,TIME,USEC,FLST,FVAL"
    frmHiddenData.tabData(2).CedaServerName = strServer
    frmHiddenData.tabData(2).CedaPort = "3357"
    frmHiddenData.tabData(2).CedaHopo = strHOPO
    frmHiddenData.tabData(2).CedaCurrentApplication = "LogTabViewer" & "," & frmHiddenData.GetApplVersion(True)
    frmHiddenData.tabData(2).CedaTabext = "TAB"
    frmHiddenData.tabData(2).CedaUser = GetWindowsUserName()
    frmHiddenData.tabData(2).CedaWorkstation = GetWorkStationName()
    frmHiddenData.tabData(2).CedaSendTimeout = "3"
    frmHiddenData.tabData(2).CedaReceiveTimeout = "240"
    frmHiddenData.tabData(2).CedaRecordSeparator = Chr(10)
    frmHiddenData.tabData(2).CedaIdentifier = "LogTab"
    frmHiddenData.tabData(2).ShowHorzScroller True
    frmHiddenData.tabData(2).EnableHeaderSizing True
    
    strUser = cbUsers.Text
    strType = cbType.Text
    strTable = txtTable
    strLogTab = txtLogTable
    If strTable = "" Or strLogTab = "" Then
        MsgBox "Please specify a table!!"
        Exit Sub
    End If
    
'---***Read the data
    strCedaFrom = txtFromDate.Tag + txtFromTime + "00"
    strCedaTo = txtToDate.Tag + txtToTime + "00"
    datFrom = CedaFullDateToVb(strCedaFrom)
    datTo = CedaFullDateToVb(strCedaTo)
    progress.Min = 0
    progress.Value = 0
    diffValue = DateDiff("d", datFrom, datTo)
    If diffValue < 1 Then
        progress.Max = 1
    Else
        progress.Max = diffValue + 2
    End If
    If progress.Max = 1 Then progress.Max = 3
    Do
        datReadTo = DateAdd("d", 1, datFrom)
        If datReadTo > datTo Then datReadTo = datTo
        tmpFr = Format(datFrom, "yyyymmdd")
        tmpTo = Format(datReadTo, "yyyymmdd")
        strWhere = " WHERE TIME BETWEEN '" & tmpFr & "000000' AND '" & tmpTo & "235959' AND STAB='" & strTable & "TAB" & "'"
        If strUser <> "**ALL USERS**" Then
            strWhere = strWhere + " AND USEC='" + strUser + "'"
        End If
        If strType <> "**ALL COMMANDS**" Then
            strWhere = strWhere + " AND SFKT='" + strType + "'"
        End If
        strWhere = strWhere + " ORDER BY TIME"
        frmHiddenData.tabData(2).CedaAction "RT", strLogTab, frmHiddenData.tabData(2).HeaderString, "", strWhere
        frmHiddenData.tabData(2).Refresh
        frmHiddenData.lblTab(2).Caption = strLogTab & ": Records = " & CStr(frmHiddenData.tabData(2).GetLineCount())
        frmHiddenData.lblTab(2).Refresh
        datFrom = DateAdd("d", 1, datReadTo)
        progress.Value = (progress.Value + 2)
    Loop While (datFrom <= datTo)
'---***Prepare the viewer for the lines
    frmMain.SetGridFromLogTable
    progress.Value = 0
End Sub

Private Sub txtFromDate_Change()
    CheckDateField txtFromDate, "dd.mm.yyyy", False
End Sub

Private Sub txtToDate_Change()
    CheckDateField txtToDate, "dd.mm.yyyy", False
End Sub
