﻿using System;
using System.Collections.Generic;
using Ufis.Entities;
using System.Reflection;

namespace Ufis.Data.Ceda
{
    /// <summary>
    /// Represents an SQL specific command class. This class is inherited from Ufis.Data.Ceda.CedaSqlCommand class.
    /// </summary>
    public class CedaEntityCommand : CedaSqlCommand
    {
        private Type _entityType;
        private string _attributeList;
        private string _whereClause;
        private Dictionary<string, string> _columnMap = new Dictionary<string, string>();

        /// <summary>
        /// Gets or sets the entity type.
        /// </summary>
        public Type EntityType
        {            
            get { return _entityType; }
            set 
            { 
                if (!typeof(Entity).IsAssignableFrom(value))                
                    throw new System.ArgumentException("Type provided is not Entity type.");

                object[] arrEntityAttributes = value.GetCustomAttributes(typeof(EntityAttribute), false);
                if (arrEntityAttributes.Length == 0)
                    throw new System.ArgumentException("Type provided does not have SerializedName attribute.");                
                
                base.TableName = ((EntityAttribute)arrEntityAttributes[0]).SerializedName;
                _entityType = value;

                _columnMap.Clear();
                PropertyInfo[] arrInfos = value.GetProperties();
                foreach (PropertyInfo info in arrInfos)
                {
                    arrEntityAttributes = info.GetCustomAttributes(typeof(EntityAttribute), false);
                    if (arrEntityAttributes.Length > 0)
                    {
                        _columnMap.Add(string.Format("[{0}]", info.Name), 
                            ((EntityAttribute)arrEntityAttributes[0]).SerializedName);
                    }
                }

                if ((!string.IsNullOrEmpty(AttributeList)) && string.IsNullOrEmpty(base.ColumnList))
                    AttributeList = AttributeList;

                if ((!string.IsNullOrEmpty(EntityWhereClause)) && string.IsNullOrEmpty(base.WhereClause))
                    EntityWhereClause = EntityWhereClause;
            }
        }

        /// <summary>
        /// Gets the database table name.
        /// </summary>
        public new string TableName
        {
            get { return base.TableName; }
        }

        /// <summary>
        /// Gets or sets the list of attributte to be fetched.
        /// </summary>
        public string AttributeList
        {
            get { return _attributeList; }
            set 
            { 
                if (_attributeList != value || string.IsNullOrEmpty(base.ColumnList))
                {
                    if (_attributeList != value) _attributeList = value;

                    if (_columnMap.Count > 0)
                    {
                        string strColumnList = string.Empty;
                        string[] arrAttr = _attributeList.Split(',');
                        foreach (string strAttr in arrAttr)
                        {
                            string strColumnName;
                            if (!_columnMap.TryGetValue(strAttr, out strColumnName))
                                strColumnName = strAttr;

                            strColumnList += string.Format(",{0}", strColumnName);
                        }

                        if (!string.IsNullOrEmpty(strColumnList))
                            strColumnList = strColumnList.Substring(1);
                        base.ColumnList = strColumnList;
                    }                    
                }
            }
        }

        /// <summary>
        /// Gets the list of columns to be fetched.
        /// </summary>
        public new string ColumnList
        {
            get { return base.ColumnList; }
        }

        /// <summary>
        /// Gets or sets the entity where clause.
        /// </summary>
        public string EntityWhereClause
        {
            get { return _whereClause; }
            set 
            { 
                if (_whereClause != value || string.IsNullOrEmpty(base.WhereClause))
                {
                    if (_whereClause != value) _whereClause = value;

                    if (_columnMap.Count > 0)
                    {
                        string strWhereClause = value;
                        foreach (string strAttr in _columnMap.Keys)
                        {
                            string strColumnName;
                            if (!_columnMap.TryGetValue(strAttr, out strColumnName))
                                strColumnName = strAttr;

                            strWhereClause = strWhereClause.Replace(strAttr, strColumnName);
                        }
                        base.WhereClause = strWhereClause;
                    }
                }
            }
        }

        /// <summary>
        /// Gets the SQL where clause.
        /// </summary>
        public new string WhereClause
        {
            get { return base.WhereClause; }
        }

        /// <summary>
        /// Initializes the new instance of Ufis.Data.Ceda.CedaEntityCommand class.
        /// </summary>
        public CedaEntityCommand() : base() { }
    }

    /// <summary>
    /// Represents an SQL Select specific command class. This class is inherited from Ufis.Data.Ceda.CedaEntityCommand class.
    /// </summary>
    public class CedaEntitySelectCommand : CedaEntityCommand
    {
        /// <summary>
        /// Gets the command text for this instance of Ufis.Data.Ceda.CedaEntitySelectCommand
        /// </summary>
        public new string CommandText
        {
            get { return base.CommandText; }
        }

        /// <summary>
        /// Gets the list of columns' values.
        /// </summary>
        public new string ValueList
        {
            get { return base.ValueList; }
        }

        /// <summary>
        /// Initializes the new instance of Ufis.Data.Ceda.CedaEntitySelectCommand class.
        /// </summary>
        public CedaEntitySelectCommand()
        {
            base.CommandText = "RT";
        }
    }

    /// <summary>
    /// Represents an SQL Insert specific command class. This class is inherited from Ufis.Data.Ceda.CedaEntityCommand class.
    /// </summary>
    public class CedaEntityInsertCommand : CedaEntityCommand
    {
        /// <summary>
        /// Gets the command text for this instance of Ufis.Data.Ceda.CedaEntityInsertCommand
        /// </summary>
        public new string CommandText
        {
            get { return base.CommandText; }
        }

        /// <summary>
        /// Gets the entity where clause.
        /// </summary>
        public new string EntityWhereClause
        {
            get { return base.EntityWhereClause; }
        }

        /// <summary>
        /// Initializes the new instance of Ufis.Data.Ceda.CedaEntityInsertCommand class.
        /// </summary>
        public CedaEntityInsertCommand()
        {
            base.CommandText = "IRT";
        }
    }

    /// <summary>
    /// Represents an SQL Update specific command class. This class is inherited from Ufis.Data.Ceda.CedaEntityCommand class.
    /// </summary>
    public class CedaEntityUpdateCommand : CedaEntityCommand
    {
        /// <summary>
        /// Gets the command text for this instance of Ufis.Data.Ceda.CedaEntityUpdateCommand
        /// </summary>
        public new string CommandText
        {
            get { return base.CommandText; }
        }

        /// <summary>
        /// Initializes the new instance of Ufis.Data.Ceda.CedaEntityUpdateCommand class.
        /// </summary>
        public CedaEntityUpdateCommand()
        {
            base.CommandText = "URT";
        }
    }

    /// <summary>
    /// Represents an SQL Delete specific command class. This class is inherited from Ufis.Data.Ceda.CedaEntityCommand class.
    /// </summary>
    public class CedaEntityDeleteCommand : CedaEntityCommand
    {
        /// <summary>
        /// Gets the command text for this instance of Ufis.Data.Ceda.CedaEntityDeleteCommand
        /// </summary>
        public new string CommandText
        {
            get { return base.CommandText; }
        }

        /// <summary>
        /// Gets the list of attributes to be fetched.
        /// </summary>
        public new string AttributeList
        {
            get { return base.AttributeList; }
        }

        /// <summary>
        /// Gets the list of columns' values.
        /// </summary>
        public new string ValueList
        {
            get { return base.ValueList; }
        }

        /// <summary>
        /// Initializes the new instance of Ufis.Data.Ceda.CedaEntityDeleteCommand class.
        /// </summary>
        public CedaEntityDeleteCommand()
        {
            base.CommandText = "DRT";
        }
    }

    /// <summary>
    /// Represents an SQL Select Flight specific command class. This class is inherited from Ufis.Data.Ceda.CedaEntityCommand class.
    /// </summary>
    public class CedaEntitySelectFlightCommand : CedaEntityCommand
    {
        /// <summary>
        /// Gets the entity type.
        /// </summary>
        public new Type EntityType
        {
            get { return base.EntityType; }
        }

        /// <summary>
        /// Gets the command text for this instance of Ufis.Data.Ceda.CedaEntitySelectFlightCommand
        /// </summary>
        public new string CommandText
        {
            get { return base.CommandText; }
        }

        /// <summary>
        /// Gets the list of columns' values.
        /// </summary>
        public new string ValueList
        {
            get { return base.ValueList; }
        }

        /// <summary>
        /// Initializes the new instance of Ufis.Data.Ceda.CedaEntitySelectFlightCommand class.
        /// </summary>
        public CedaEntitySelectFlightCommand()
        {
            base.EntityType = typeof(EntDbFlight);
            base.CommandText = "GFR";
        }
    }

    /// <summary>
    /// Represents an SQL Insert Flight specific command class. This class is inherited from Ufis.Data.Ceda.CedaEntityCommand class.
    /// </summary>
    public class CedaEntityInsertFlightCommand : CedaEntityCommand
    {
        /// <summary>
        /// Gets the entity type.
        /// </summary>
        public new Type EntityType
        {
            get { return base.EntityType; }
        }

        /// <summary>
        /// Gets the command text for this instance of Ufis.Data.Ceda.CedaEntityInsertFlightCommand
        /// </summary>
        public new string CommandText
        {
            get { return base.CommandText; }
        }

        /// <summary>
        /// Gets the entity where clause.
        /// </summary>
        public new string EntityWhereClause
        {
            get { return base.EntityWhereClause; }
        }

        /// <summary>
        /// Initializes the new instance of Ufis.Data.Ceda.CedaEntityInsertFlightCommand class.
        /// </summary>
        public CedaEntityInsertFlightCommand()
        {
            base.EntityType = typeof(EntDbFlight);
            base.CommandText = "IFR";
        }
    }

    /// <summary>
    /// Represents an SQL Update Flight specific command class. This class is inherited from Ufis.Data.Ceda.CedaEntityCommand class.
    /// </summary>
    public class CedaEntityUpdateFlightCommand : CedaEntityCommand
    {
        /// <summary>
        /// Gets the entity type.
        /// </summary>
        public new Type EntityType
        {
            get { return base.EntityType; }
        }

        /// <summary>
        /// Gets the command text for this instance of Ufis.Data.Ceda.CedaEntityUpdateFlightCommand
        /// </summary>
        public new string CommandText
        {
            get { return base.CommandText; }
        }

        /// <summary>
        /// Initializes the new instance of Ufis.Data.Ceda.CedaEntityUpdateFlightCommand class.
        /// </summary>
        public CedaEntityUpdateFlightCommand()
        {
            base.EntityType = typeof(EntDbFlight);
            base.CommandText = "UFR";
        }
    }

    /// <summary>
    /// Represents an SQL Delete Flight specific command class. This class is inherited from Ufis.Data.Ceda.CedaEntityCommand class.
    /// </summary>
    public class CedaEntityDeleteFlightCommand : CedaEntityCommand
    {
        /// <summary>
        /// Gets the entity type.
        /// </summary>
        public new Type EntityType
        {
            get { return base.EntityType; }
        }

        /// <summary>
        /// Gets the command text for this instance of Ufis.Data.Ceda.CedaEntityDeleteFlightCommand
        /// </summary>
        public new string CommandText
        {
            get { return base.CommandText; }
        }

        /// <summary>
        /// Gets the list of attributes to be fetched.
        /// </summary>
        public new string AttributeList
        {
            get { return base.AttributeList; }
        }

        /// <summary>
        /// Gets the list of columns' values.
        /// </summary>
        public new string ValueList
        {
            get { return base.ValueList; }
        }

        /// <summary>
        /// Initializes the new instance of Ufis.Data.Ceda.CedaEntityDeleteFlightCommand class.
        /// </summary>
        public CedaEntityDeleteFlightCommand()
        {
            base.EntityType = typeof(EntDbFlight);
            base.CommandText = "DFR";
        }
    }
}
