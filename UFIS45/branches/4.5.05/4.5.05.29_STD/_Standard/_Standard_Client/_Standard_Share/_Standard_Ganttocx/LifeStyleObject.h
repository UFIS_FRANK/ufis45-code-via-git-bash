#ifndef __LifeStyle_H__
#define __LifeStyle_H__

	struct _LIFE_STYLE
	{
		BOOL Tab_StyleUp;
		int  Tab_Type;
		int  Tab_Percent;
		int	 Tab_ColorIdx;

		BOOL Body_StyleUp;
		int  Body_Type;
		int  Body_Percent;
		int	 Body_ColorIdx;

		BOOL TimeScale_StyleUp;
		int  TimeScale_Type;
		int  TimeScale_Percent;
		int	 TimeScale_ColorIdx;

	} LIFE_STYLE;
#endif