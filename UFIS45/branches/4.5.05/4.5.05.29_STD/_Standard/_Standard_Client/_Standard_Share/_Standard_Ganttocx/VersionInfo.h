#ifndef _VERSIONINFO_H_
#define _VERSIONINFO_H_


class VersionInfo
{
public:	
	CString	omComments;
	CString omCompanyName;
	CString omFileDescription;
	CString omInternalName;
	CString omLegalCopyright;
	CString omLegalTrademarks;
	CString omOriginalFilename;
	CString omPrivateBuild;
	CString omProductName;
	CString omProductVersion;
	CString omSpecialBuild;
	CString omFileVersion;
	static bool GetVersionInfo(HMODULE hModule,VersionInfo& ropVersionInfo);	 
};

#endif	// VersionInfo