// UGantt.odl : type library source for ActiveX Control project.

// This file will be processed by the Make Type Library (mktyplib) tool to
// produce the type library (UGantt.tlb) that will become a resource in
// UGantt.ocx.

#include <olectl.h>
#include <idispids.h>

[ uuid(6782E133-3223-11D4-996A-0000863DE95C), version(1.1),
  helpfile("UGantt.hlp"),
  helpstring("UGantt ActiveX Control module"),
  control ]
library UGANTTLib
{
	importlib(STDOLE_TLB);
	importlib(STDTYPE_TLB);

	//  Primary dispatch interface for CUGanttCtrl

	[ uuid(6782E136-3223-11D4-996A-0000863DE95C),
	  helpstring("Dispatch interface for UGantt Control"), hidden ]
	dispinterface _DUGantt
	{
		properties:
			// NOTE - ClassWizard will maintain property information here.
			//    Use extreme caution when editing this section.
			//{{AFX_ODL_PROP(CUGanttCtrl)
			[id(1)] boolean EnableDragDropBar;
			[id(2)] boolean EnableDragDropBkBar;
			[id(3)] boolean EnableDragDrop;
			[id(4)] boolean AutoScroll;
			[id(5)] long ScrollDelay;
			[id(6)] long ScrollInset;
			[id(7)] long ScrollInterval;
			[id(8)] BSTR BarFontName;
			[id(9)] long BarFontSize;
			[id(10)] long TabBodyFontSize;
			[id(11)] boolean TabHeaderFontBold;
			[id(12)] boolean TabBodyFontBold;
			[id(13)] BSTR TabFontName;
			[id(14)] boolean BarFontBold;
			[id(15)] long SubbarStackCount;
			[id(16)] short UTCOffsetInHours;
			[id(17)] boolean AutoMoveSubBarsWithBar;
			[id(18)] boolean OverlappingRightBarTop;
			[id(24)] long TabColumns;
			[id(25)] long TabLines;
			[id(26)] BSTR TabHeaderString;
			[id(27)] BSTR TabHeaderLengthString;
			[id(28)] short TabLineHeight;
			[id(29)] short TabFontSize;
			[id(30)] short TabHeaderFontSize;
			[id(31)] BSTR TabHeaderFontName;
			[id(32)] BSTR TabBodyFontName;
			[id(33)] DATE TimeFrameFrom;
			[id(34)] DATE TimeFrameTo;
			[id(35)] long TimeScaleHeaderHeight;
			[id(36)] long SplitterPosition;
			[id(37)] boolean With2ndScroller;
			[id(38)] long TimeScaleDuration;
			[id(39)] long TabHighlightColor;
			[id(40)] long ResizeAreaWidth;
			[id(41)] long ResizeMinimalWidth;
			[id(42)] short BarOverlapOffset;
			[id(43)] short BarNumberExpandLine;
			[id(44)] boolean WithHorizontalScroller;
			[id(45)] short ArrowWidth;
			[id(46)] short ArrowHead;
			[id(47)] long ArrowColorNormal;
			[id(48)] long ArrowColorSelected;
			[id(19)] boolean AutoSizeLineHeightToFit;
			[id(DISPID_ENABLED), bindable, requestedit] boolean Enabled;
			[id(20)] boolean LifeStyle;
			[id(21)] BSTR Version;
			[id(22)] BSTR BuildDate;
			[id(23)] boolean SplitterMovable;
			//}}AFX_ODL_PROP

		methods:
			// NOTE - ClassWizard will maintain method information here.
			//    Use extreme caution when editing this section.
			//{{AFX_ODL_METHOD(CUGanttCtrl)
			[id(49)] void TabSetHeaderText(BSTR HeaderText);
			[id(50)] void ResetContent();
			[id(51)] void MakeNewLine(long LineNo, BSTR TabValues);
			[id(52)] void TabSetLineColors(long LineNo, long TextColor, long BackColor);
			[id(53)] void GanttSetBackColor(long BackColor);
			[id(54)] void DeleteLine(long LineNo);
			[id(55)] long TabGetCurrentSelected();
			[id(56)] BSTR TabGetLinesByTextColor(long Color);
			[id(57)] BSTR TabGetLinesByBackColor(long Color);
			[id(58)] void TabUpdateValues(long LineNo, BSTR Values);
			[id(59)] void AppendLine(BSTR Values);
			[id(60)] boolean AddBkBarToLine(long LineNo, BSTR BkBarKey, DATE StartTime, DATE EndTime, long BarColor, BSTR BarText);
			[id(61)] void RefreshArea(BSTR Hint);
			[id(62)] boolean AddBarToLine(long LineNo, BSTR Key, DATE StartTime, DATE EndTime, BSTR BarText, long Shape, long BackColor, long TextColor, BOOL SplitColor, long LeftBackColor, long RightBackColor, BSTR LeftOutsideText, BSTR RightOutsideText, long LeftTextColor, long RightTextColor);
			[id(63)] boolean DeleteBarByKey(BSTR Key);
			[id(64)] boolean DeleteBkBarByKey(BSTR Key);
			[id(65)] long GetBkBarLineByKey(BSTR Key);
			[id(66)] long GetBkBarPosInLine(BSTR olKey, long llLineNo);
			[id(67)] BSTR GetBkBarsByLine(long LineNo);
			[id(68)] BSTR GetBarsByLine(long LineNo);
			[id(69)] BSTR GetBarsByBkBar(BSTR olBkBarKey);
			[id(70)] void SetBarColor(BSTR Key, long Color);
			[id(71)] void SetBarDate(BSTR Key, DATE Begin, DATE Ende, BOOL HandleOverlap);
			[id(72)] void SetBkBarColor(BSTR Key, long Color);
			[id(73)] void SetBkBarDate(BSTR Key, DATE Begin, DATE Ende);
			[id(74)] void SetBarTextColor(BSTR Key, long Color);
			[id(75)] void SetBkBarTextColor(BSTR Key, long Color);
			[id(76)] void RefreshGanttLine(long LineNo);
			[id(77)] void ScrollTo(DATE FromDate);
			[id(78)] boolean AddLineAt(long LineNo, BSTR KeyValue, BSTR TabString);
			[id(79)] void SetLineSeparator(long LineNo, short LSHeight, long LSColor, short LSPen, short TotalHeight, short LSBegin);
			[id(80)] void DeleteLineSeparator(long LineNo);
			[id(81)] void SetStart2Start(BSTR BarKeySrc, BSTR BarKeyDest);
			[id(82)] void SetStart2Finish(BSTR BarKeySrc, BSTR BarKeyDest);
			[id(83)] void SetFinish2Start(BSTR BarKeySrc, BSTR BarKeyDest);
			[id(84)] void SetFinish2Finish(BSTR BarKeySrc, BSTR BarKeyDest);
			[id(85)] void DeleteX2X(BSTR BarKeySrc, BSTR BarKeyDest);
			[id(86)] void SetBarLeftOutsideText(BSTR Key, BSTR Text);
			[id(87)] void SetBarRightOutsideText(BSTR Key, BSTR Text);
			[id(88)] void SetBarLeftOutsideTextColor(BSTR Key, long Color);
			[id(89)] void SetBarRightOutsideTextColor(BSTR Key, long Color);
			[id(90)] void SetBarSplitColor(BSTR Key, long LeftColor, long RightColor);
			[id(91)] void DeleteSplitColor(BSTR Key);
			[id(92)] void SetBarStyle(BSTR Key, short BarStyle);
			[id(93)] boolean SetTimeMarker(DATE Time, long Color);
			[id(94)] boolean DeleteTimeMarker(DATE Time);
			[id(95)] long GetTimeMarker(DATE Time);
			[id(96)] void DeleteAllTimeMarkers();
			[id(97)] void DecorationObjectCreate(BSTR ID, BSTR LocationList, BSTR PixelList, BSTR ColorList);
			[id(98)] void DecorationObjectSetToBar(BSTR ID, BSTR BarKey);
			[id(99)] void DecorationObjectSetToBkBar(BSTR ID, BSTR BkBarKey);
			[id(100)] void DecorationObjectResetBar(BSTR ID, BSTR BarKey);
			[id(101)] void DecorationObjectResetBkBar(BSTR ID, BSTR BkBarKey);
			[id(102)] BSTR DecorationObjectsGetByBarKey(BSTR BarKey);
			[id(103)] BSTR DecorationObjectsGetByBkBarKey(BSTR BkBarKey);
			[id(104)] void AddSubBar(BSTR MainBarKey, BSTR Key, DATE StartTime, DATE EndTime, BSTR BarText, long Shape, long BackColor, long TextColor, long SplitColor, long LeftColor, long RightColor, BSTR LeftOutsideText, BSTR RightOutsideText, long LeftTextColor, long RightTextColor);
			[id(105)] void RemoveSubBar(BSTR MainKey, BSTR Key);
			[id(106)] long GetLineNoByKey(BSTR Key);
			[id(107)] void TabSetGroupColumn(short ColNo, long AlternateColor);
			[id(108)] void TabResetAllGroups();
			[id(109)] void TabResetGroupsColumn(short ColNo);
			[id(110)] void SortTab(BSTR ColNo, boolean blAsc, boolean blCaseSensitive);
			[id(111)] BSTR TabGetColumnValue(long LineNo, long ColNo);
			[id(112)] BSTR TabGetLineKey(long LineNo);
			[id(113)] BSTR TabGetLineValues(long LineNo);
			[id(DISPID_REFRESH)] void Refresh();
			[id(114)] void SetBarTriangles(BSTR BarKey, BSTR TriangleID, long LeftColor, long RightColor, boolean FullTriangle);
			[id(115)] DATE GetBarStartTime(BSTR BarKey);
			[id(116)] DATE GetBarEndTime(BSTR BarKey);
			[id(117)] DATE GetBkBarStartTime(BSTR BarKey);
			[id(118)] DATE GetBkBarEndTime(BSTR BarKey);
			[id(119)] VARIANT GetBarRecord(BSTR BarKey);
			[id(120)] VARIANT GetBarRecordsByLine(long LineNo);
			[id(121)] VARIANT GetGaps(long LineNo);
			[id(122)] VARIANT GetGapsForTimeframe(long LineNo, DATE FromDateTime, DATE ToDateTime);
			[id(123)] long GetTotalBarCount();
			[id(124)] void AddBarRecord(VARIANT pBar);
			[id(125)] void DeleteAllBars(boolean withBackgroundBars);
			[id(126)] long GetLineNoByBarKey(BSTR Key);
			[id(127)] BSTR GetGapsForTimeFrame2(long LineNo, DATE FromDateTime, DATE ToDateTime);
			[id(128)] void ScrollToLine(long LineNo);
			[id(129)] BOOL SaveToFile(BSTR fileName);
			[id(130)] BOOL SaveToFileWithTime(BSTR fileName, DATE fromDateTime, int toMinutes, int overlapMinutes);
			//}}AFX_ODL_METHOD

			[id(DISPID_ABOUTBOX)] void AboutBox();
	};

	//  Event dispatch interface for CUGanttCtrl

	[ uuid(6782E137-3223-11D4-996A-0000863DE95C),
	  helpstring("Event interface for UGantt Control") ]
	dispinterface _DUGanttEvents
	{
		properties:
			//  Event interface has no properties

		methods:
			// NOTE - ClassWizard will maintain event information here.
			//    Use extreme caution when editing this section.
			//{{AFX_ODL_EVENT(CUGanttCtrl)
			[id(1)] void ActualLineNo(long ActualLineNo);
			[id(2)] void TimeFromX(DATE Time);
			[id(3)] void ChangeBarDate(BSTR Key, DATE Begin, DATE Ende);
			[id(4)] void ChangeBkBarDate(BSTR Key, DATE Begin, DATE Ende);
			[id(5)] void ActualTimescaleDate(DATE TimescaleDate);
			[id(6)] void OnDragEnter(BSTR Key);
			[id(7)] void OnDragLeave(BSTR Key);
			[id(8)] void OnDragOver(BSTR Key, long LineNo);
			[id(9)] void OnDrop(BSTR Key, long LineNo);
			[id(10)] void EndOfSizeOrMove(BSTR Key, long LineNo);
			[id(11)] void OnLButtonDblClkBar(BSTR Key, short nFlags);
			[id(12)] void OnLButtonDblClkBkBar(BSTR Key, short nFlags);
			[id(13)] void OnLButtonDownBar(BSTR Key, short nFlags);
			[id(14)] void OnLButtonDownBkBar(BSTR Key, short nFlags);
			[id(15)] void OnRButtonDownBar(BSTR Key, short nFlags);
			[id(16)] void OnRButtonDownBkBar(BSTR Key, short nFlags);
			[id(17)] void OnRButtonDownBkGantt();
			[id(18)] void OnLButtonDownBkGantt();
			[id(19)] void OnLButtonDblClkBkGantt();
			[id(20)] void OnLButtonDownTab(long Row, short Column, short nFlags);
			[id(21)] void OnLButtonDblClkTab(long Row, short Column, short nFlags);
			[id(22)] void OnRButtonDownTab(long Row, short Column, short nFlags);
			[id(23)] void OnDebug(BSTR Output);
			[id(24)] void OnLButtonDblClkPercentBar(BSTR Key, short Percent, short nFlags);
			[id(25)] void OnLButtonDblClkPercentBkBar(BSTR Key, short Percent, short nFlags);
			[id(26)] void OnMouseMoveBar(BSTR Key);
			[id(27)] void OnMouseMoveBkBar(BSTR Key);
			[id(28)] void OnLButtonDownSubBar(BSTR MainBarKey, BSTR SubBarKey, short nFlags);
			[id(29)] void OnLButtonDblClkSubBar(BSTR MainBarKey, BSTR SubBarKey, short nFlags);
			[id(30)] void OnRButtonDownSubBar(BSTR MainBarKey, BSTR SubBarKey, short nFlags);
			[id(31)] void OnHScroll(long ScrollPos);
			//}}AFX_ODL_EVENT
	};

	//  Class information for CUGanttCtrl

	[ uuid(6782E138-3223-11D4-996A-0000863DE95C),
	  helpstring("UGantt Control"), control ]
	coclass UGantt
	{
		[default] dispinterface _DUGantt;
		[default, source] dispinterface _DUGanttEvents;
	};


	typedef 
	[ 
		uuid(909E6310-8157-11d7-80DC-00D0B7E2A467),
		version(1.0),
		helpstring("BAR_RECORD")
	]
	#include "GanttUDT.h"

	typedef 
	[ 
		uuid(DDFA5D40-8207-11d7-80DC-00D0B7E2A467),
		version(1.0),
		helpstring("UTIME_FRAME")
	]
	#include "UTimeFrame.h"

//-----------------------------------------------
	// {097DE6C1-A6F3-11d7-80E9-00D0B7E2A467}
	//static const GUID <<name>> = 
	//{ 0x97de6c1, 0xa6f3, 0x11d7, { 0x80, 0xe9, 0x0, 0xd0, 0xb7, 0xe2, 0xa4, 0x67 } };
	typedef
	[
		uuid(097DE6C1-A6F3-11d7-80E9-00D0B7E2A467),
		version(1.0),
		helpstring("LIFE_STYLE")
	]
	#include "LifeStyleObject.h"

//-----------------------------------------------
	// {097DE6C2-A6F3-11d7-80E9-00D0B7E2A467}
	//static const GUID <<name>> = 
	//{ 0x97de6c2, 0xa6f3, 0x11d7, { 0x80, 0xe9, 0x0, 0xd0, 0xb7, 0xe2, 0xa4, 0x67 } };
	typedef 
	[ 
		uuid(097DE6C2-A6F3-11d7-80E9-00D0B7E2A467),
		version(1.0),
		helpstring("UBAR_SHAPE")
	]
	enum {
		ugHorizontal = 0,
		ugVertical = 1,
		ugConvex = 2,
		ugConcave = 3
	} LIFESTYLE_TYPE;



	typedef 
	[ 
		uuid(48F04441-85F6-11d7-80DE-00D0B7E2A467),
		version(1.0),
		helpstring("UBAR_SHAPE")
	]
	enum {
		ugNormal = 0,
		ugRaised = 1,
		ugInset = 2
	} UBAR_SHAPE;

	//{{AFX_APPEND_ODL}}
	//}}AFX_APPEND_ODL}}
};
