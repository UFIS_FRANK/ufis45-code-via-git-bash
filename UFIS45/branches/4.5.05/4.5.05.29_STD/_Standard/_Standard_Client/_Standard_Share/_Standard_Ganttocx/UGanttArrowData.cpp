// UGanttArrowData.cpp: implementation of the CUGanttArrowData class.
//
//////////////////////////////////////////////////////////////////////

#include <stdafx.h>
#include <UGantt.h>
#include <UGanttArrowData.h>

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CUGanttArrowData::CUGanttArrowData()
{
	IsSelected		= FALSE;
	Color			= RGB(0,0,16);
	ArrowHead		= 0;
}

CUGanttArrowData::~CUGanttArrowData()
{

}