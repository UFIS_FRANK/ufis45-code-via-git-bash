3/// UGanttCtl.cpp : Implementation of the CUGanttCtrl ActiveX Control class.

#include <stdafx.h>
#include <UGantt.h>
#include <UGanttCtl.h>
#include <UGanttPpg.h>
#include <CCSTimeScale.h>
#include <afxcoll.h>
#include <atlbase.h>
#include <comdef.h>
#include <Windows.h>
#include <locale.h>
#include <VersionInfo.h>
//#include <cimage.h>
#include <atlimage.h>
#include <fstream>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

IMPLEMENT_DYNCREATE(CUGanttCtrl, COleControl)

// General macros for testing a point against an interval
#define IsReallyOverlapped(start1, end1, start2, end2)	((start1) < (end2) && (start2) < (end1))
#define IsWithIn(start1, end1, start2, end2)	((start1) >= (start2) && (end1) <= (end2))
#define IsBetween(val, start, end) (start <= val && val <= end)
#define IsOverlapped(start1, end1, start2, end2) (start1 <= end1 && start2 <= end1)

bool bmActivScrolling;
bool bmFireHScrollEvent;

int ExtractTextLineFast(CStringArray &opItems, char *pcpLineText, char *pcpSepa)
{
	int ilSepaLen = strlen(pcpSepa);
	int ilCount = 0;
	char *currPtr;
	currPtr = strstr(pcpLineText, pcpSepa);
	while (currPtr != NULL)
	{
		*currPtr = '\0';
		opItems.Add(pcpLineText);
		ilCount++;
		pcpLineText = currPtr + ilSepaLen;
		currPtr		= strstr(pcpLineText, pcpSepa);
	}
	if(strcmp(pcpLineText, "") != 0)
	{
		opItems.Add(pcpLineText);
		ilCount++;
	}

	return ilCount;
}


//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//- - methods concerning the sorting of the tab
//- -
//- - MWO	12.02.03
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
// - compare methods
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
int CompareAscendingCase(const LineData **a, const LineData **b)
{
	return ((**a).SortString.Compare((**b).SortString));
}
int CompareAscendingNoCase(const LineData **a, const LineData **b)
{
	return ((**a).SortString.CompareNoCase((**b).SortString));
}
int CompareDescendingCase(const LineData **a, const LineData **b)
{
	return (-1 * ((**a).SortString.Compare((**b).SortString)));
}
int CompareDescendingNoCase(const LineData **a, const LineData **b)
{
	return (-1 * ((**a).SortString.CompareNoCase((**b).SortString)));
}



static int CompareBkBarsByBegin(const BarData **b1, const BarData **b2)
{
	return ((**b1).Begin == (**b2).Begin) ? 0 : ((**b1).Begin > (**b2).Begin) ? 1 : -1;
}

static int CompareBkBarsByEnd(const BarData **b1, const BarData **b2)
{
	return ((**b1).End == (**b2).End) ? 0 : ((**b1).End > (**b2).End) ? 1 : -1;
}

static int CompareBarsByBegin(const BarData **b1, const BarData **b2)
{
	return ((**b1).Begin == (**b2).Begin) ? 0 : ((**b1).Begin > (**b2).Begin) ? 1 : -1;
}

static int CompareBarsByEnd(const BarData **b1, const BarData **b2)
{
	return ((**b1).End == (**b2).End) ? 0 : ((**b1).End > (**b2).End) ? 1 : -1;
}
/******************************************************
 * Function:   GetDataItem
 * Parameter:  OUT     char *pcpResult
 *             IN/OUT  char *pcpInput
 *             IN      int   ipNum     pos. of item in list
 *             IN      char  cpDel     Delimiter
 *             IN      char *pcpDef    Default Result if token is empty
 *             IN      char *pcpTrim   " \0" = trim left with space
 *                                     "\0 " = trim right with space
 *                                     "::"  = trim left and right colon
 *                                     "\0\0" = no trim
 *                                
 * Return Code: >=0          log. number of char. in result string
 *                           (blanks does not count as character )
 *                           pcpinput points behind hit 
 *              -1..-5       <number> argument is NULL
 *                           pcpinput not changed
 *                           empty pcpResult string
 *
 * Result:      *prpResult contains found string
 *           
 * Description: Get <ipNum>th item in <cpDel> seperated list
 *              and return the number of chars as return value
 *              and the item in <pcpResult>. 
 *              If the '\0' is used
 *              as delimiter the number of the requested item
 *              must not exceed the total number of items, to
 *              avoid a coredump. Every other delimiter is not 
 *              not critical, because end of string is check.
 *
 *******************************************************/

int GetDataItem(char *pcpResult, char *pcpInput, int ipNum, char cpDel,char *pcpDef,char *pcpTrim)
{
	int ilRC = 0;	//RC_SUCCESS;
	int ilLastChar=0;
	int ilPos=0;

	if ( pcpResult == NULL )
	{
		ilRC = -1;
	}
	else if ( pcpInput == NULL )
	{
		ilRC = -2;
	}
	else if (ipNum == 0)
	{
		ilRC = -3;
	}
	else if (pcpTrim == NULL)
	{
		ilRC = -4;
	}
	else
    {
		while(ipNum > 1  && (*pcpInput != '\0' || cpDel == '\0'))
		{
			if (*pcpInput == cpDel )
			{
				if ( cpDel != pcpTrim[0] || pcpInput[1] != cpDel)
				{
					ipNum--;
				}
			}
			pcpInput++;
		}/* end while*/

		pcpResult[0] = '\0';
		ilPos=ilLastChar=0;

		if (pcpTrim[0] != '\0')
		{
			while (*pcpInput == pcpTrim[0])
			{
				pcpInput++;
			}
		}
		/* Search first Delimiter character */
		while((*pcpInput != cpDel) && (*pcpInput != '\0') )
		{
			pcpResult[ilPos] = *pcpInput;

			if((pcpTrim != '\0' ) && (*pcpInput != pcpTrim[1]))
			{
				ilLastChar = ilPos+1 ;
			}
			pcpInput++;
			ilPos++;
 		}
		if (*pcpInput == cpDel)
		{
			pcpInput++;
		}

		if ( pcpTrim[1] != '\0')
		{
			ilPos = ilLastChar;
		}
		pcpResult[ilPos]='\0';
		ilRC = ilPos;

		if((ilPos == 0)  && (*pcpDef != '\0'))
		{
			strcpy(pcpResult,pcpDef);  
			ilRC = strlen(pcpDef)-1;
			while((ilRC >= 0 ) && (pcpResult[ilRC] == ' '))
			{
				ilRC--;
			}
		}
    } /* else end */

	return ilRC;
} /* end GetDataItem */



CString NumberToCString(int ipNum)
{
	CString olStr;
	olStr.Format("%d", ipNum);
	return olStr;
}

CString NumberToCString(long lpNum)
{
	CString olStr;
	olStr.Format("%ld", lpNum);
	return olStr;
}
/////////////////////////////////////////////////////////////////////////////
// Message map

BEGIN_MESSAGE_MAP(CUGanttCtrl, COleControl)
	//{{AFX_MSG_MAP(CUGanttCtrl)
	ON_WM_CREATE()
	ON_WM_MOUSEMOVE()
	ON_WM_SETCURSOR()
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_WM_ERASEBKGND()
	ON_WM_VSCROLL()
	ON_WM_SIZE()
	ON_WM_HSCROLL()
	ON_WM_TIMER()
	ON_WM_LBUTTONDBLCLK()
	ON_WM_RBUTTONDOWN()
	//}}AFX_MSG_MAP
	ON_OLEVERB(AFX_IDS_VERB_EDIT, OnEdit)
	ON_OLEVERB(AFX_IDS_VERB_PROPERTIES, OnProperties)
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// Dispatch map

BEGIN_DISPATCH_MAP(CUGanttCtrl, COleControl)
	//{{AFX_DISPATCH_MAP(CUGanttCtrl)
	DISP_PROPERTY_NOTIFY(CUGanttCtrl, "EnableDragDropBar", m_enableDragDropBar, OnEnableDragDropBarChanged, VT_BOOL)
	DISP_PROPERTY_NOTIFY(CUGanttCtrl, "EnableDragDropBkBar", m_enableDragDropBkBar, OnEnableDragDropBkBarChanged, VT_BOOL)
	DISP_PROPERTY_NOTIFY(CUGanttCtrl, "EnableDragDrop", m_enableDragDrop, OnEnableDragDropChanged, VT_BOOL)
	DISP_PROPERTY_NOTIFY(CUGanttCtrl, "AutoScroll", m_autoScroll, OnAutoScrollChanged, VT_BOOL)
	DISP_PROPERTY_NOTIFY(CUGanttCtrl, "ScrollDelay", m_nScrollDelay, OnScrollDelayChanged, VT_I4)
	DISP_PROPERTY_NOTIFY(CUGanttCtrl, "ScrollInset", m_nScrollInset, OnScrollInsetChanged, VT_I4)
	DISP_PROPERTY_NOTIFY(CUGanttCtrl, "ScrollInterval", m_nScrollInterval, OnScrollIntervalChanged, VT_I4)
	DISP_PROPERTY_NOTIFY(CUGanttCtrl, "BarFontName", m_barFontName, OnBarFontNameChanged, VT_BSTR)
	DISP_PROPERTY_NOTIFY(CUGanttCtrl, "BarFontSize", m_barFontSize, OnBarFontSizeChanged, VT_I4)
	DISP_PROPERTY_NOTIFY(CUGanttCtrl, "TabBodyFontSize", m_tabBodyFontSize, OnTabBodyFontSizeChanged, VT_I4)
	DISP_PROPERTY_NOTIFY(CUGanttCtrl, "TabHeaderFontBold", m_tabHeaderFontBold, OnTabHeaderFontBoldChanged, VT_BOOL)
	DISP_PROPERTY_NOTIFY(CUGanttCtrl, "TabBodyFontBold", m_tabBodyFontBold, OnTabBodyFontBoldChanged, VT_BOOL)
	DISP_PROPERTY_NOTIFY(CUGanttCtrl, "TabFontName", m_tabFontName, OnTabFontNameChanged, VT_BSTR)
	DISP_PROPERTY_NOTIFY(CUGanttCtrl, "BarFontBold", m_barFontBold, OnBarFontBoldChanged, VT_BOOL)
	DISP_PROPERTY_NOTIFY(CUGanttCtrl, "SubbarStackCount", m_subbarStackCount, OnSubbarStackCountChanged, VT_I4)
	DISP_PROPERTY_NOTIFY(CUGanttCtrl, "UTCOffsetInHours", m_uTCOffsetInHours, OnUTCOffsetInHoursChanged, VT_I2)
	DISP_PROPERTY_NOTIFY(CUGanttCtrl, "AutoMoveSubBarsWithBar", m_autoMoveSubBarsWithBar, OnAutoMoveSubBarsWithBarChanged, VT_BOOL)
	DISP_PROPERTY_NOTIFY(CUGanttCtrl, "OverlappingRightBarTop", m_overlappingRightBarTop, OnOverlappingRightBarTopChanged, VT_BOOL)
	DISP_PROPERTY_NOTIFY(CUGanttCtrl, "AutoSizeLineHeightToFit", m_autoSizeLineHeightToFit, OnAutoSizeLineHeightToFitChanged, VT_BOOL)
	DISP_PROPERTY_NOTIFY(CUGanttCtrl, "LifeStyle", m_lifeStyle, OnLifeStyleChanged, VT_BOOL)
	DISP_PROPERTY_NOTIFY(CUGanttCtrl, "Version", m_version, OnVersionChanged, VT_BSTR)
	DISP_PROPERTY_NOTIFY(CUGanttCtrl, "BuildDate", m_buildDate, OnBuildDateChanged, VT_BSTR)
	DISP_PROPERTY_NOTIFY(CUGanttCtrl, "SplitterMovable", m_splitterMovable, OnSplitterMovableChanged, VT_BOOL)
	DISP_PROPERTY_EX(CUGanttCtrl, "TabColumns", GetTabColumns, SetTabColumns, VT_I4)
	DISP_PROPERTY_EX(CUGanttCtrl, "TabLines", GetTabLines, SetTabLines, VT_I4)
	DISP_PROPERTY_EX(CUGanttCtrl, "TabHeaderString", GetTabHeaderString, SetTabHeaderString, VT_BSTR)
	DISP_PROPERTY_EX(CUGanttCtrl, "TabHeaderLengthString", GetTabHeaderLengthString, SetTabHeaderLengthString, VT_BSTR)
	DISP_PROPERTY_EX(CUGanttCtrl, "TabLineHeight", GetTabLineHeight, SetTabLineHeight, VT_I2)
	DISP_PROPERTY_EX(CUGanttCtrl, "TabFontSize", GetTabFontSize, SetTabFontSize, VT_I2)
	DISP_PROPERTY_EX(CUGanttCtrl, "TabHeaderFontSize", GetTabHeaderFontSize, SetTabHeaderFontSize, VT_I2)
	DISP_PROPERTY_EX(CUGanttCtrl, "TabHeaderFontName", GetTabHeaderFontName, SetTabHeaderFontName, VT_BSTR)
	DISP_PROPERTY_EX(CUGanttCtrl, "TabBodyFontName", GetTabBodyFontName, SetTabBodyFontName, VT_BSTR)
	DISP_PROPERTY_EX(CUGanttCtrl, "TimeFrameFrom", GetTimeFrameFrom, SetTimeFrameFrom, VT_DATE)
	DISP_PROPERTY_EX(CUGanttCtrl, "TimeFrameTo", GetTimeFrameTo, SetTimeFrameTo, VT_DATE)
	DISP_PROPERTY_EX(CUGanttCtrl, "TimeScaleHeaderHeight", GetTimeScaleHeaderHeight, SetTimeScaleHeaderHeight, VT_I4)
	DISP_PROPERTY_EX(CUGanttCtrl, "SplitterPosition", GetSplitterPosition, SetSplitterPosition, VT_I4)
	DISP_PROPERTY_EX(CUGanttCtrl, "With2ndScroller", GetWith2ndScroller, SetWith2ndScroller, VT_BOOL)
	DISP_PROPERTY_EX(CUGanttCtrl, "TimeScaleDuration", GetTimeScaleDuration, SetTimeScaleDuration, VT_I4)
	DISP_PROPERTY_EX(CUGanttCtrl, "TabHighlightColor", GetTabHighlightColor, SetTabHighlightColor, VT_I4)
	DISP_PROPERTY_EX(CUGanttCtrl, "ResizeAreaWidth", GetResizeAreaWidth, SetResizeAreaWidth, VT_I4)
	DISP_PROPERTY_EX(CUGanttCtrl, "ResizeMinimalWidth", GetResizeMinimalWidth, SetResizeMinimalWidth, VT_I4)
	DISP_PROPERTY_EX(CUGanttCtrl, "BarOverlapOffset", GetBarOverlapOffset, SetBarOverlapOffset, VT_I2)
	DISP_PROPERTY_EX(CUGanttCtrl, "BarNumberExpandLine", GetBarNumberExpandLine, SetBarNumberExpandLine, VT_I2)
	DISP_PROPERTY_EX(CUGanttCtrl, "WithHorizontalScroller", GetWithHorizontalScroller, SetWithHorizontalScroller, VT_BOOL)
	DISP_PROPERTY_EX(CUGanttCtrl, "ArrowWidth", GetArrowWidth, SetArrowWidth, VT_I2)
	DISP_PROPERTY_EX(CUGanttCtrl, "ArrowHead", GetArrowHead, SetArrowHead, VT_I2)
	DISP_PROPERTY_EX(CUGanttCtrl, "ArrowColorNormal", GetArrowColorNormal, SetArrowColorNormal, VT_I4)
	DISP_PROPERTY_EX(CUGanttCtrl, "ArrowColorSelected", GetArrowColorSelected, SetArrowColorSelected, VT_I4)
	DISP_FUNCTION(CUGanttCtrl, "TabSetHeaderText", TabSetHeaderText, VT_EMPTY, VTS_BSTR)
	DISP_FUNCTION(CUGanttCtrl, "ResetContent", ResetContent, VT_EMPTY, VTS_NONE)
	DISP_FUNCTION(CUGanttCtrl, "MakeNewLine", MakeNewLine, VT_EMPTY, VTS_I4 VTS_BSTR)
	DISP_FUNCTION(CUGanttCtrl, "TabSetLineColors", TabSetLineColors, VT_EMPTY, VTS_I4 VTS_I4 VTS_I4)
	DISP_FUNCTION(CUGanttCtrl, "GanttSetBackColor", GanttSetBackColor, VT_EMPTY, VTS_I4)
	DISP_FUNCTION(CUGanttCtrl, "DeleteLine", DeleteLine, VT_EMPTY, VTS_I4)
	DISP_FUNCTION(CUGanttCtrl, "TabGetCurrentSelected", TabGetCurrentSelected, VT_I4, VTS_NONE)
	DISP_FUNCTION(CUGanttCtrl, "TabGetLinesByTextColor", TabGetLinesByTextColor, VT_BSTR, VTS_I4)
	DISP_FUNCTION(CUGanttCtrl, "TabGetLinesByBackColor", TabGetLinesByBackColor, VT_BSTR, VTS_I4)
	DISP_FUNCTION(CUGanttCtrl, "TabUpdateValues", TabUpdateValues, VT_EMPTY, VTS_I4 VTS_BSTR)
	DISP_FUNCTION(CUGanttCtrl, "AppendLine", AppendLine, VT_EMPTY, VTS_BSTR)
	DISP_FUNCTION(CUGanttCtrl, "AddBkBarToLine", AddBkBarToLine, VT_BOOL, VTS_I4 VTS_BSTR VTS_DATE VTS_DATE VTS_I4 VTS_BSTR)
	DISP_FUNCTION(CUGanttCtrl, "RefreshArea", RefreshArea, VT_EMPTY, VTS_BSTR)
	DISP_FUNCTION(CUGanttCtrl, "AddBarToLine", AddBarToLine, VT_BOOL, VTS_I4 VTS_BSTR VTS_DATE VTS_DATE VTS_BSTR VTS_I4 VTS_I4 VTS_I4 VTS_BOOL VTS_I4 VTS_I4 VTS_BSTR VTS_BSTR VTS_I4 VTS_I4)
	DISP_FUNCTION(CUGanttCtrl, "DeleteBarByKey", DeleteBarByKey, VT_BOOL, VTS_BSTR)
	DISP_FUNCTION(CUGanttCtrl, "DeleteBkBarByKey", DeleteBkBarByKey, VT_BOOL, VTS_BSTR)
	DISP_FUNCTION(CUGanttCtrl, "GetBkBarLineByKey", GetBkBarLineByKey, VT_I4, VTS_BSTR)
	DISP_FUNCTION(CUGanttCtrl, "GetBkBarPosInLine", GetBkBarPosInLine, VT_I4, VTS_BSTR VTS_I4)
	DISP_FUNCTION(CUGanttCtrl, "GetBkBarsByLine", GetBkBarsByLine, VT_BSTR, VTS_I4)
	DISP_FUNCTION(CUGanttCtrl, "GetBarsByLine", GetBarsByLine, VT_BSTR, VTS_I4)
	DISP_FUNCTION(CUGanttCtrl, "GetBarsByBkBar", GetBarsByBkBar, VT_BSTR, VTS_BSTR)
	DISP_FUNCTION(CUGanttCtrl, "SetBarColor", SetBarColor, VT_EMPTY, VTS_BSTR VTS_I4)
	DISP_FUNCTION(CUGanttCtrl, "SetBarDate", SetBarDate, VT_EMPTY, VTS_BSTR VTS_DATE VTS_DATE VTS_BOOL)
	DISP_FUNCTION(CUGanttCtrl, "SetBkBarColor", SetBkBarColor, VT_EMPTY, VTS_BSTR VTS_I4)
	DISP_FUNCTION(CUGanttCtrl, "SetBkBarDate", SetBkBarDate, VT_EMPTY, VTS_BSTR VTS_DATE VTS_DATE)
	DISP_FUNCTION(CUGanttCtrl, "SetBarTextColor", SetBarTextColor, VT_EMPTY, VTS_BSTR VTS_I4)
	DISP_FUNCTION(CUGanttCtrl, "SetBkBarTextColor", SetBkBarTextColor, VT_EMPTY, VTS_BSTR VTS_I4)
	DISP_FUNCTION(CUGanttCtrl, "RefreshGanttLine", RefreshGanttLine, VT_EMPTY, VTS_I4)
	DISP_FUNCTION(CUGanttCtrl, "ScrollTo", ScrollTo, VT_EMPTY, VTS_DATE)
	DISP_FUNCTION(CUGanttCtrl, "AddLineAt", AddLineAt, VT_BOOL, VTS_I4 VTS_BSTR VTS_BSTR)
	DISP_FUNCTION(CUGanttCtrl, "SetLineSeparator", SetLineSeparator, VT_EMPTY, VTS_I4 VTS_I2 VTS_I4 VTS_I2 VTS_I2 VTS_I2)
	DISP_FUNCTION(CUGanttCtrl, "DeleteLineSeparator", DeleteLineSeparator, VT_EMPTY, VTS_I4)
	DISP_FUNCTION(CUGanttCtrl, "SetStart2Start", SetStart2Start, VT_EMPTY, VTS_BSTR VTS_BSTR)
	DISP_FUNCTION(CUGanttCtrl, "SetStart2Finish", SetStart2Finish, VT_EMPTY, VTS_BSTR VTS_BSTR)
	DISP_FUNCTION(CUGanttCtrl, "SetFinish2Start", SetFinish2Start, VT_EMPTY, VTS_BSTR VTS_BSTR)
	DISP_FUNCTION(CUGanttCtrl, "SetFinish2Finish", SetFinish2Finish, VT_EMPTY, VTS_BSTR VTS_BSTR)
	DISP_FUNCTION(CUGanttCtrl, "DeleteX2X", DeleteX2X, VT_EMPTY, VTS_BSTR VTS_BSTR)
	DISP_FUNCTION(CUGanttCtrl, "SetBarLeftOutsideText", SetBarLeftOutsideText, VT_EMPTY, VTS_BSTR VTS_BSTR)
	DISP_FUNCTION(CUGanttCtrl, "SetBarRightOutsideText", SetBarRightOutsideText, VT_EMPTY, VTS_BSTR VTS_BSTR)
	DISP_FUNCTION(CUGanttCtrl, "SetBarLeftOutsideTextColor", SetBarLeftOutsideTextColor, VT_EMPTY, VTS_BSTR VTS_I4)
	DISP_FUNCTION(CUGanttCtrl, "SetBarRightOutsideTextColor", SetBarRightOutsideTextColor, VT_EMPTY, VTS_BSTR VTS_I4)
	DISP_FUNCTION(CUGanttCtrl, "SetBarSplitColor", SetBarSplitColor, VT_EMPTY, VTS_BSTR VTS_I4 VTS_I4)
	DISP_FUNCTION(CUGanttCtrl, "DeleteSplitColor", DeleteSplitColor, VT_EMPTY, VTS_BSTR)
	DISP_FUNCTION(CUGanttCtrl, "SetBarStyle", SetBarStyle, VT_EMPTY, VTS_BSTR VTS_I2)
	DISP_FUNCTION(CUGanttCtrl, "SetTimeMarker", SetTimeMarker, VT_BOOL, VTS_DATE VTS_I4)
	DISP_FUNCTION(CUGanttCtrl, "DeleteTimeMarker", DeleteTimeMarker, VT_BOOL, VTS_DATE)
	DISP_FUNCTION(CUGanttCtrl, "GetTimeMarker", GetTimeMarker, VT_I4, VTS_DATE)
	DISP_FUNCTION(CUGanttCtrl, "DeleteAllTimeMarkers", DeleteAllTimeMarkers, VT_EMPTY, VTS_NONE)
	DISP_FUNCTION(CUGanttCtrl, "DecorationObjectCreate", DecorationObjectCreate, VT_EMPTY, VTS_BSTR VTS_BSTR VTS_BSTR VTS_BSTR)
	DISP_FUNCTION(CUGanttCtrl, "DecorationObjectSetToBar", DecorationObjectSetToBar, VT_EMPTY, VTS_BSTR VTS_BSTR)
	DISP_FUNCTION(CUGanttCtrl, "DecorationObjectSetToBkBar", DecorationObjectSetToBkBar, VT_EMPTY, VTS_BSTR VTS_BSTR)
	DISP_FUNCTION(CUGanttCtrl, "DecorationObjectResetBar", DecorationObjectResetBar, VT_EMPTY, VTS_BSTR VTS_BSTR)
	DISP_FUNCTION(CUGanttCtrl, "DecorationObjectResetBkBar", DecorationObjectResetBkBar, VT_EMPTY, VTS_BSTR VTS_BSTR)
	DISP_FUNCTION(CUGanttCtrl, "DecorationObjectsGetByBarKey", DecorationObjectsGetByBarKey, VT_BSTR, VTS_BSTR)
	DISP_FUNCTION(CUGanttCtrl, "DecorationObjectsGetByBkBarKey", DecorationObjectsGetByBkBarKey, VT_BSTR, VTS_BSTR)
	DISP_FUNCTION(CUGanttCtrl, "AddSubBar", AddSubBar, VT_EMPTY, VTS_BSTR VTS_BSTR VTS_DATE VTS_DATE VTS_BSTR VTS_I4 VTS_I4 VTS_I4 VTS_I4 VTS_I4 VTS_I4 VTS_BSTR VTS_BSTR VTS_I4 VTS_I4)
	DISP_FUNCTION(CUGanttCtrl, "RemoveSubBar", RemoveSubBar, VT_EMPTY, VTS_BSTR VTS_BSTR)
	DISP_FUNCTION(CUGanttCtrl, "GetLineNoByKey", GetLineNoByKey, VT_I4, VTS_BSTR)
	DISP_FUNCTION(CUGanttCtrl, "TabSetGroupColumn", TabSetGroupColumn, VT_EMPTY, VTS_I2 VTS_I4)
	DISP_FUNCTION(CUGanttCtrl, "TabResetAllGroups", TabResetAllGroups, VT_EMPTY, VTS_NONE)
	DISP_FUNCTION(CUGanttCtrl, "TabResetGroupsColumn", TabResetGroupsColumn, VT_EMPTY, VTS_I2)
	DISP_FUNCTION(CUGanttCtrl, "SortTab", SortTab, VT_EMPTY, VTS_BSTR VTS_BOOL VTS_BOOL)
	DISP_FUNCTION(CUGanttCtrl, "TabGetColumnValue", TabGetColumnValue, VT_BSTR, VTS_I4 VTS_I4)
	DISP_FUNCTION(CUGanttCtrl, "TabGetLineKey", TabGetLineKey, VT_BSTR, VTS_I4)
	DISP_FUNCTION(CUGanttCtrl, "TabGetLineValues", TabGetLineValues, VT_BSTR, VTS_I4)
	DISP_FUNCTION(CUGanttCtrl, "SetBarTriangles", SetBarTriangles, VT_EMPTY, VTS_BSTR VTS_BSTR VTS_I4 VTS_I4 VTS_BOOL)
	DISP_FUNCTION(CUGanttCtrl, "GetBarStartTime", GetBarStartTime, VT_DATE, VTS_BSTR)
	DISP_FUNCTION(CUGanttCtrl, "GetBarEndTime", GetBarEndTime, VT_DATE, VTS_BSTR)
	DISP_FUNCTION(CUGanttCtrl, "GetBkBarStartTime", GetBkBarStartTime, VT_DATE, VTS_BSTR)
	DISP_FUNCTION(CUGanttCtrl, "GetBkBarEndTime", GetBkBarEndTime, VT_DATE, VTS_BSTR)
	DISP_FUNCTION(CUGanttCtrl, "GetBarRecord", GetBarRecord, VT_VARIANT, VTS_BSTR)
	DISP_FUNCTION(CUGanttCtrl, "GetBarRecordsByLine", GetBarRecordsByLine, VT_VARIANT, VTS_I4)
	DISP_FUNCTION(CUGanttCtrl, "GetGaps", GetGaps, VT_VARIANT, VTS_I4)
	DISP_FUNCTION(CUGanttCtrl, "GetGapsForTimeframe", GetGapsForTimeframe, VT_VARIANT, VTS_I4 VTS_DATE VTS_DATE)
	DISP_FUNCTION(CUGanttCtrl, "GetTotalBarCount", GetTotalBarCount, VT_I4, VTS_NONE)
	DISP_FUNCTION(CUGanttCtrl, "AddBarRecord", AddBarRecord, VT_EMPTY, VTS_VARIANT)
	DISP_FUNCTION(CUGanttCtrl, "DeleteAllBars", DeleteAllBars, VT_EMPTY, VTS_BOOL)
	DISP_FUNCTION(CUGanttCtrl, "GetLineNoByBarKey", GetLineNoByBarKey, VT_I4, VTS_BSTR)
	DISP_FUNCTION(CUGanttCtrl, "GetGapsForTimeFrame2", GetGapsForTimeFrame2, VT_BSTR, VTS_I4 VTS_DATE VTS_DATE)
	DISP_FUNCTION(CUGanttCtrl, "ScrollToLine", ScrollToLine, VT_EMPTY, VTS_I4)
	DISP_FUNCTION(CUGanttCtrl, "SaveToFile", SaveToFile, VT_BOOL, VTS_BSTR)
	DISP_FUNCTION(CUGanttCtrl, "SaveToFileWithTime", SaveToFileWithTime, VT_BOOL, VTS_BSTR  VTS_DATE  VTS_I4 VTS_I4)
	DISP_STOCKFUNC_REFRESH()
	DISP_STOCKPROP_ENABLED()
	//}}AFX_DISPATCH_MAP
	DISP_FUNCTION_ID(CUGanttCtrl, "AboutBox", DISPID_ABOUTBOX, AboutBox, VT_EMPTY, VTS_NONE)
END_DISPATCH_MAP()


/////////////////////////////////////////////////////////////////////////////
// Event map

BEGIN_EVENT_MAP(CUGanttCtrl, COleControl)
	//{{AFX_EVENT_MAP(CUGanttCtrl)
	EVENT_CUSTOM("ActualLineNo", FireActualLineNo, VTS_I4)
	EVENT_CUSTOM("TimeFromX", FireTimeFromX, VTS_DATE)
	EVENT_CUSTOM("ChangeBarDate", FireChangeBarDate, VTS_BSTR  VTS_DATE  VTS_DATE)
	EVENT_CUSTOM("ChangeBkBarDate", FireChangeBkBarDate, VTS_BSTR  VTS_DATE  VTS_DATE)
	EVENT_CUSTOM("ActualTimescaleDate", FireActualTimescaleDate, VTS_DATE)
	EVENT_CUSTOM("OnDragEnter", FireOnDragEnter, VTS_BSTR)
	EVENT_CUSTOM("OnDragLeave", FireOnDragLeave, VTS_BSTR)
	EVENT_CUSTOM("OnDragOver", FireOnDragOver, VTS_BSTR  VTS_I4)
	EVENT_CUSTOM("OnDrop", FireOnDrop, VTS_BSTR  VTS_I4)
	EVENT_CUSTOM("EndOfSizeOrMove", FireEndOfSizeOrMove, VTS_BSTR  VTS_I4)
	EVENT_CUSTOM("OnLButtonDblClkBar", FireOnLButtonDblClkBar, VTS_BSTR  VTS_I2)
	EVENT_CUSTOM("OnLButtonDblClkBkBar", FireOnLButtonDblClkBkBar, VTS_BSTR  VTS_I2)
	EVENT_CUSTOM("OnLButtonDownBar", FireOnLButtonDownBar, VTS_BSTR  VTS_I2)
	EVENT_CUSTOM("OnLButtonDownBkBar", FireOnLButtonDownBkBar, VTS_BSTR  VTS_I2)
	EVENT_CUSTOM("OnRButtonDownBar", FireOnRButtonDownBar, VTS_BSTR  VTS_I2)
	EVENT_CUSTOM("OnRButtonDownBkBar", FireOnRButtonDownBkBar, VTS_BSTR  VTS_I2)
	EVENT_CUSTOM("OnRButtonDownBkGantt", FireOnRButtonDownBkGantt, VTS_NONE)
	EVENT_CUSTOM("OnLButtonDownBkGantt", FireOnLButtonDownBkGantt, VTS_NONE)
	EVENT_CUSTOM("OnLButtonDblClkBkGantt", FireOnLButtonDblClkBkGantt, VTS_NONE)
	EVENT_CUSTOM("OnLButtonDownTab", FireOnLButtonDownTab, VTS_I4  VTS_I2  VTS_I2)
	EVENT_CUSTOM("OnLButtonDblClkTab", FireOnLButtonDblClkTab, VTS_I4  VTS_I2  VTS_I2)
	EVENT_CUSTOM("OnRButtonDownTab", FireOnRButtonDownTab, VTS_I4  VTS_I2  VTS_I2)
	EVENT_CUSTOM("OnDebug", FireOnDebug, VTS_BSTR)
	EVENT_CUSTOM("OnLButtonDblClkPercentBar", FireOnLButtonDblClkPercentBar, VTS_BSTR  VTS_I2  VTS_I2)
	EVENT_CUSTOM("OnLButtonDblClkPercentBkBar", FireOnLButtonDblClkPercentBkBar, VTS_BSTR  VTS_I2  VTS_I2)
	EVENT_CUSTOM("OnMouseMoveBar", FireOnMouseMoveBar, VTS_BSTR)
	EVENT_CUSTOM("OnMouseMoveBkBar", FireOnMouseMoveBkBar, VTS_BSTR)
	EVENT_CUSTOM("OnLButtonDownSubBar", FireOnLButtonDownSubBar, VTS_BSTR  VTS_BSTR  VTS_I2)
	EVENT_CUSTOM("OnLButtonDblClkSubBar", FireOnLButtonDblClkSubBar, VTS_BSTR  VTS_BSTR  VTS_I2)
	EVENT_CUSTOM("OnRButtonDownSubBar", FireOnRButtonDownSubBar, VTS_BSTR  VTS_BSTR  VTS_I2)
	EVENT_CUSTOM("OnHScroll", FireOnHScroll, VTS_I4)
	//}}AFX_EVENT_MAP
END_EVENT_MAP()


/////////////////////////////////////////////////////////////////////////////
// Property pages

// TODO: Add more property pages as needed.  Remember to increase the count!
BEGIN_PROPPAGEIDS(CUGanttCtrl, 1)
	PROPPAGEID(CUGanttPropPage::guid)
END_PROPPAGEIDS(CUGanttCtrl)


/////////////////////////////////////////////////////////////////////////////
// Initialize class factory and guid

IMPLEMENT_OLECREATE_EX(CUGanttCtrl, "UGANTT.UGanttCtrl.1",
	0x6782e138, 0x3223, 0x11d4, 0x99, 0x6a, 0, 0, 0x86, 0x3d, 0xe9, 0x5c)


/////////////////////////////////////////////////////////////////////////////
// Type library ID and version

IMPLEMENT_OLETYPELIB(CUGanttCtrl, _tlid, _wVerMajor, _wVerMinor)


/////////////////////////////////////////////////////////////////////////////
// Interface IDs

const IID BASED_CODE IID_DUGantt =
		{ 0x6782e136, 0x3223, 0x11d4, { 0x99, 0x6a, 0, 0, 0x86, 0x3d, 0xe9, 0x5c } };
const IID BASED_CODE IID_DUGanttEvents =
		{ 0x6782e137, 0x3223, 0x11d4, { 0x99, 0x6a, 0, 0, 0x86, 0x3d, 0xe9, 0x5c } };

// {909E6310-8157-11d7-80DC-00D0B7E2A467}
static const GUID BAR_RECORD_GUID = 
{ 0x909e6310, 0x8157, 0x11d7, { 0x80, 0xdc, 0x0, 0xd0, 0xb7, 0xe2, 0xa4, 0x67 } };

// {DDFA5D40-8207-11d7-80DC-00D0B7E2A467}
static const GUID MY_TF_GUID = 
{ 0xddfa5d40, 0x8207, 0x11d7, { 0x80, 0xdc, 0x0, 0xd0, 0xb7, 0xe2, 0xa4, 0x67 } };

// {48F04441-85F6-11d7-80DE-00D0B7E2A467}
static const GUID ENUM_SHAPE_GUID = 
{ 0x48f04441, 0x85f6, 0x11d7, { 0x80, 0xde, 0x0, 0xd0, 0xb7, 0xe2, 0xa4, 0x67 } };


// {097DE6C1-A6F3-11d7-80E9-00D0B7E2A467}
static const GUID LIFE_STYLE_GUID = 
{ 0x97de6c1, 0xa6f3, 0x11d7, { 0x80, 0xe9, 0x0, 0xd0, 0xb7, 0xe2, 0xa4, 0x67 } };


// {097DE6C2-A6F3-11d7-80E9-00D0B7E2A467}
static const GUID LIFESTYLE_TYPE_GUID = 
{ 0x97de6c2, 0xa6f3, 0x11d7, { 0x80, 0xe9, 0x0, 0xd0, 0xb7, 0xe2, 0xa4, 0x67 } };

/////////////////////////////////////////////////////////////////////////////
// Control type information

static const DWORD BASED_CODE _dwUGanttOleMisc =
	OLEMISC_ACTIVATEWHENVISIBLE |
	OLEMISC_SETCLIENTSITEFIRST |
	OLEMISC_INSIDEOUT |
	OLEMISC_CANTLINKINSIDE |
	OLEMISC_RECOMPOSEONRESIZE;

IMPLEMENT_OLECTLTYPE(CUGanttCtrl, IDS_UGANTT, _dwUGanttOleMisc)


/////////////////////////////////////////////////////////////////////////////
// CUGanttCtrl::CUGanttCtrlFactory::UpdateRegistry -
// Adds or removes system registry entries for CUGanttCtrl

BOOL CUGanttCtrl::CUGanttCtrlFactory::UpdateRegistry(BOOL bRegister)
{
	// TODO: Verify that your control follows apartment-model threading rules.
	// Refer to MFC TechNote 64 for more information.
	// If your control does not conform to the apartment-model rules, then
	// you must modify the code below, changing the 6th parameter from
	// afxRegInsertable | afxRegApartmentThreading to afxRegInsertable.

	if (bRegister)
		return AfxOleRegisterControlClass(
			AfxGetInstanceHandle(),
			m_clsid,
			m_lpszProgID,
			IDS_UGANTT,
			IDB_UGANTT,
			afxRegInsertable | afxRegApartmentThreading,
			_dwUGanttOleMisc,
			_tlid,
			_wVerMajor,
			_wVerMinor);
	else
		return AfxOleUnregisterClass(m_clsid, m_lpszProgID);
}


/////////////////////////////////////////////////////////////////////////////
// CUGanttCtrl::CUGanttCtrl - Constructor

CUGanttCtrl::CUGanttCtrl()
: m_SelfIsDragging(false),
  m_CanAcceptDrop(false)
{
	InitializeIIDs(&IID_DUGantt, &IID_DUGanttEvents);

	omTabHeaderFont.CreateFont( 12, 7, 0, 0, FW_BOLD, 0, 0, 0, DEFAULT_CHARSET, OUT_CHARACTER_PRECIS, CLIP_DEFAULT_PRECIS, 
		                     PROOF_QUALITY, DEFAULT_PITCH, "Courier" );
	omTabFont.CreateFont( 12, 5, 0, 0, FW_NORMAL, 0, 0, 0, DEFAULT_CHARSET, OUT_CHARACTER_PRECIS, CLIP_DEFAULT_PRECIS, 
		                     PROOF_QUALITY, DEFAULT_PITCH, "Small Fonts" );

	GetVersionInfo();

	imTabLogicalWidth=0;
	SetTabHeaderString	("Name,Surname");
	omGroupCols.Add(-1);
	omGroupCols.Add(-1);
	omGroupAlternateColor.Add(-1);
	omGroupAlternateColor.Add(-1);
	omTabHeader.Add		("Name");
	omTabHeader.Add		("Surname");
	omTabHeaderLen.Add	(50);
	omTabHeaderLen.Add	(80);

	SplitterPosition = 130;

	With2ndScroller			= FALSE;
	WithHorizontalScroller  = TRUE;
	WithTimeScale			= TRUE;

	for (int i = 0; i < omTabHeaderLen.GetSize(); i++)
	{
		imTabLogicalWidth += omTabHeaderLen[i];
	}

  // On start we don't want to scroll to any direction:
	m_AutoScrollDirection = SCROLL_NOSCROLL;
	m_autoSizeLineHeightToFit = FALSE;

	m_enableDragDrop		= TRUE;
	m_enableDragDropBar		= TRUE;
	m_enableDragDropBkBar	= FALSE;
	m_overlappingRightBarTop= FALSE;
	m_nScrollDelay = 200;
	m_nScrollInterval = 100;
	m_nScrollInset = 20;
	m_autoScroll = FALSE;
	m_autoMoveSubBarsWithBar = FALSE;
	m_nCursor = LoadCursor(NULL, IDC_ARROW);
	m_dMoveBarOldBegin = 0;
	m_dMoveBarOldEnd   = 0;
	m_dMoveBkBarOldBegin = 0;
	m_dMoveBkBarOldEnd   = 0;
	m_uTCOffsetInHours	 = 0;
	pomTabVScroll			= NULL;
	pomHScroll				= NULL;
	pomVScroll				= NULL;
	pomTimeScale			= NULL;
	pomBar					= NULL;
	TabLineHeight			= 20;
	TabFontSize				= 7;
	TabHeaderFontSize		= 8;
	imTabSelectedLine		= -1;
	ResizeAreaWidth			= 5;
	ResizeMinimalWidth		= 30;
	BarOverlapOffset		= 4;
	BarNumberExpandLine		= 3;
	bmAllSelected			= false;
	TabBodyFontName			= "Small Fonts";
	TabHeaderFontName		= "Courier";

	bmWithArrows			= FALSE;
	smArrowWidth			= 2;
	smArrowHead				= 0;
	omArrowColorNormal		= RGB (  0,  0,  0);
	omArrowColorSelected	= RGB (255,  0,  0);
	omBackgroundColor		= ::GetSysColor(COLOR_BTNFACE);//RGB (192,192,192);

	bmDrawTAB				= FALSE;
	bmDrawGANTT				= FALSE;
	bmDrawSPLITTER			= FALSE;
	bmDrawTIMESCALE			= FALSE;
	bmDrawGLINE				= FALSE;

	COleDateTime olT;
	olT = COleDateTime::GetCurrentTime();
	TimeFrameFrom = COleDateTime(olT.GetYear(), olT.GetMonth(), olT.GetDay(), 0, 0, 0);
	TimeFrameTo   = COleDateTime(olT.GetYear(), olT.GetMonth(), olT.GetDay(), 23, 59, 0);
	omStartTime = TimeFrameFrom; 
	omCurrentTime = this->GetCurrentTime();

	imTimeScaleDuration = 10; //6 hours as default
	MakeSampleData();
	TabLines = omLines.GetSize();

	TabColumns = 2;
	for(int i = 0; i < TabLines; i++)
	{
		omTabTextColors.Add(0);
		omTabBackColors.Add(::GetSysColor(COLOR_BTNFACE));//COLORREF(RGB(192,192,192))/*16777214*/);
	}
	TabHeaderLengthString	= "50,80";
	TabHeaderFontSize		= 20;
	TimeScaleHeaderHeight	= 40;
	
	imCurrSplitterOffset	= -1;
	imCurrBarLine			= -1;
	isSplitterUpdating		= false;
	imCurrentHighlightLine	= -1;
	TabHighlightColor		=  COLORREF(RGB(150,255,255)); 
	bmFirstDrawing			= true;
	bmFoundBar = FALSE;
	bmFoundBkBar = FALSE;

  //MWO: 19.08.2002
	m_subbarStackCount		= 0;
	m_uTCOffsetInHours		= 0;
	InitOleBar(&omOleBar);

  //MWO: 25.06.2003
	m_lifeStyle = FALSE; 
	
	omLifeStyle.Tab_StyleUp = TRUE;
	omLifeStyle.Tab_Type = 1;
	omLifeStyle.Tab_Percent = 40;
	omLifeStyle.Tab_ColorIdx = 7;

	omLifeStyle.Body_StyleUp = FALSE;
	omLifeStyle.Body_Type = 0;
	omLifeStyle.Body_Percent = 50;
	omLifeStyle.Body_ColorIdx = 7;

	omLifeStyle.TimeScale_StyleUp = FALSE;
	omLifeStyle.TimeScale_Type = 2;
	omLifeStyle.TimeScale_Percent = 50;
	omLifeStyle.TimeScale_ColorIdx = 7;

	bmActivScrolling = false;
	m_splitterMovable = TRUE;
	bmFireHScrollEvent = true;
}


/////////////////////////////////////////////////////////////////////////////
// CUGanttCtrl::~CUGanttCtrl - Destructor

CUGanttCtrl::~CUGanttCtrl()
{
	omLines.DeleteAll();
	omTimeMarkers.DeleteAll();
	omArrows.DeleteAll();
	omTabHeader.RemoveAll();
	omTabTextColors.RemoveAll();
	omTabBackColors.RemoveAll();

	if (pomTabVScroll != NULL)
	{
		delete pomTabVScroll;
		pomTabVScroll = NULL;
	}
	if (pomVScroll    != NULL)
	{
		delete pomVScroll;
		pomVScroll = NULL;
	}
	if (pomHScroll	  != NULL)
	{
		delete pomHScroll;
		pomHScroll = NULL;
	}
	if (pomTimeScale  != NULL)
	{
		delete pomTimeScale;
		pomTimeScale = NULL;
	}

	//delete the map
	CUGanttRect *polGanttRect;
	CString		olRetKey;
	POSITION	pos = mapBarRect.GetStartPosition();
	while (pos != NULL)
	{
		mapBarRect.GetNextAssoc (pos, olRetKey, (CObject*&)polGanttRect);
		delete polGanttRect;
	}

	// cleaning CCSPtrArrays
	omDecoObjects.DeleteAll();
	m_MoveSubBarsOldBegin.DeleteAll();
	m_MoveSubBarsOldEnd.DeleteAll();

	// cleaning maps
	mapBarRect.RemoveAll();
	mapBarDecoObjects.RemoveAll();
	mapBkBarDecoObjects.RemoveAll();
	mapBars.RemoveAll();
	mapBkBars.RemoveAll();

}

void CUGanttCtrl::CalcAreaRects()
{
	CRect olRect;
	GetClientRect(&olRect);

	imTabCurrentWidth = SplitterPosition;// - 1;
	omCurrentTabRect.left = 0;
	omCurrentTabRect.bottom = olRect.bottom;
	omCurrentTabRect.top = olRect.top;
	omCurrentTabRect.right = imTabCurrentWidth;


	omSplitterRect	= CRect(SplitterPosition, olRect.top, SplitterPosition+6,olRect.right);

	int ilGLeft		= omCurrentTabRect.right + imTab2ndVScrollWidth + (omSplitterRect.right - omSplitterRect.left);
	int ilGTop		= olRect.top   + TimeScaleHeaderHeight;
	int ilGRight	= olRect.right - imTabVScrollWidth;
	int ilGBottom	= olRect.bottom - imTabHScrollHeight;
	omGanttRect		= CRect (ilGLeft, ilGTop, ilGRight, ilGBottom);

    if ((pomHScroll != NULL) && (WithHorizontalScroller != FALSE))

	//is there a VISIBLE timescale?
	if (WithTimeScale != FALSE)
	{
		omTimeScaleRect = CRect (ilGLeft - 1, olRect.top, ilGRight, ilGTop);
	}
	else
	{
		omTimeScaleRect = CRect (ilGLeft - 1, olRect.top, ilGRight, olRect.top);
	}

	pomVScroll->SetScrollRange (0, omLines.GetSize());
//Move scroller
	CRect olSRect;
	if(pomVScroll != NULL)
	{
		olSRect = olRect;
		olSRect.left = olSRect.right-15;
		pomVScroll->MoveWindow(&olSRect);
	}
// move 2nd Scrollbar
	if(With2ndScroller == TRUE)
	{
		if (pomTabVScroll != NULL)
		{
			CRect olScrollerRect;
			CRect olHScrollRect;
			pomTabVScroll->GetClientRect(&olScrollerRect);
			imTab2ndVScrollWidth = olScrollerRect.right - olScrollerRect.left;
			int ilW = olScrollerRect.right - olScrollerRect.left;
			olScrollerRect.left = SplitterPosition+6;
			olScrollerRect.right = olScrollerRect.left + ilW;
			pomVScroll->SetScrollRange( 0, omLines.GetSize());

			pomTabVScroll->MoveWindow(&olScrollerRect);
			pomTabVScroll->ShowWindow(SW_SHOWNORMAL);
			if ((pomHScroll != NULL) && (WithHorizontalScroller != FALSE))
			{
				olHScrollRect.left = olScrollerRect.right+1;
				olHScrollRect.right = olSRect.left;
				olHScrollRect.top = olRect.bottom - 15;
				olHScrollRect.bottom = olRect.bottom;
				pomHScroll->MoveWindow(&olHScrollRect);
				pomHScroll->ShowWindow(SW_SHOWNORMAL);
				pomHScroll->SetScrollRange(0,1000/*imTimeScaleDuration*60*/);
			}
		}
	}
	else
	{
	//Move horizontal scroller
		if ((pomHScroll != NULL) && (WithHorizontalScroller != FALSE))
		{
			CRect olHSRect = olRect;
			olHSRect.left = SplitterPosition+6;
			olHSRect.right = olRect.right - 15;
			olHSRect.top = olRect.bottom - 15;
			olHSRect.bottom = olRect.bottom;
			pomHScroll->MoveWindow(&olHSRect);
			pomHScroll->ShowWindow(SW_SHOWNORMAL);
			pomHScroll->SetScrollRange(0,1000/*imTimeScaleDuration*60*/);
		}
	}
//Move Timescale
	if ((pomTimeScale != NULL) && (WithTimeScale != FALSE))
	{
		pomTimeScale->MoveWindow(&omTimeScaleRect);
		pomTimeScale->SetDisplayTimeFrame(omStartTime/*TimeFrameFrom*/, 
										  COleDateTimeSpan(0,imTimeScaleDuration,0,0), 
										  COleDateTimeSpan(0,0,10,0));
		pomTimeScale->Invalidate(TRUE);
	}

}


/////////////////////////////////////////////////////////////////////////////
// CUGanttCtrl::OnDraw - Drawing function


void CUGanttCtrl::OnDraw(CDC* pdc, const CRect& rcBounds, const CRect& rcInvalid)
{
	OnMyDraw( pdc, rcBounds, rcInvalid, false );
}

void CUGanttCtrl::OnMyDraw(CDC* pdc, const CRect& rcBounds, const CRect& rcInvalid, bool bSaveOption)
{
	if (m_hWnd != NULL)
	{
		CRect olRect;
		GetClientRect(&olRect);
		CalcAreaRects();
		int ilPos;
		int ilWidth;
		CUGanttStatic *polTimeMarker;
		CRect olTMRect;

		bmDrawALL = TRUE;
		CUGanttStatic* ps1 = NULL;

	
		omTotalTabRect = CRect(olRect.left, olRect.top, olRect.left + imTabLogicalWidth, olRect.bottom);

		if (bSaveOption==true)
		{
			DrawTab (pdc);
			DrawSplitter (pdc);
			//DrawTimeScale(pdc);
			
			DrawGanttBmp (pdc, bSaveOption );
			DrawTimeScaleMark(pdc);
			DrawTimeMarker(pdc, bSaveOption );

			return;
		}

		DrawTimeMarker(pdc, bSaveOption );

		if (bmFirstDrawing == true)
		{
			DrawTab (pdc);
			DrawSplitter (pdc);
			DrawGanttBmp ();
			//DrawTimeMarker(pdc, bSaveOption );
			bmFirstDrawing = false;
			return;
		}

		

		if (bmDrawALL != FALSE)
		{
			DrawTab (pdc);
			DrawSplitter (pdc);
			//DrawTimeScale(pdc);
			DrawGanttBmp ();
		}
		else
		{
			if (bmDrawTAB != FALSE)
			{
				DrawTab (pdc);
				bmDrawTAB = FALSE;
			}
			if (bmDrawGANTT != FALSE)
			{
				DrawGanttBmp ();
				//AfxMessageBox("GANTT");
				bmDrawGANTT = FALSE;
			}
			if (bmDrawSPLITTER != FALSE)
			{
				DrawSplitter (pdc);
				bmDrawSPLITTER = FALSE;
			}
			if (bmDrawTIMESCALE != FALSE)
			{
				//DrawTimeScale (pdc);
				bmDrawTIMESCALE = FALSE;
			}
			if (bmDrawGLINE != FALSE)
			{
				if ((bmWithArrows != FALSE) && (omBarMode == "NORMAL"))
				{
					DrawGanttBmp ();		//there are arrows! so draw the gantt completely!
				}
				else
				{
					DrawGanttLineBmp ();	//there are no arrows! you can allow to redraw only the line
				}
				bmDrawGLINE = FALSE;
			}
		}
		bmDrawALL = TRUE;
	}
}


/////////////////////////////////////////////////////////////////////////////
// CUGanttCtrl::DoPropExchange - Persistence support

void CUGanttCtrl::DoPropExchange(CPropExchange* pPX)
{
	ExchangeVersion(pPX, MAKELONG(_wVerMinor, _wVerMajor));
	COleControl::DoPropExchange(pPX);

	// TODO: Call PX_ functions for each persistent custom property.

}


/////////////////////////////////////////////////////////////////////////////
// CUGanttCtrl::OnResetState - Reset control to default state

void CUGanttCtrl::OnResetState()
{
	COleControl::OnResetState();  // Resets defaults found in DoPropExchange

	// TODO: Reset any other control state here.
}


/////////////////////////////////////////////////////////////////////////////
// CUGanttCtrl::AboutBox - Display an "About" box to the user

void CUGanttCtrl::AboutBox()
{
	CDialog dlgAbout(IDD_ABOUTBOX_UGANTT);
	dlgAbout.DoModal();
}


/////////////////////////////////////////////////////////////////////////////
// CUGanttCtrl message handlers

long CUGanttCtrl::GetTabColumns() 
{
	return TabColumns;
}

void CUGanttCtrl::SetTabColumns(long nNewValue) 
{
	// TODO: Add your property handler here
	TabColumns = nNewValue;
	while(nNewValue >= omTabHeader.GetSize())
	{
		omTabHeader.RemoveAt(omTabHeader.GetSize()-1);
	}
	while(nNewValue >= omTabHeaderLen.GetSize())
	{
		omTabHeaderLen.RemoveAt(omTabHeaderLen.GetSize()-1);
	}
	while(nNewValue >= omTabTextColors.GetSize())
	{
		omTabTextColors.RemoveAt(omTabTextColors.GetSize()-1);
	}
	while(nNewValue >= omTabBackColors.GetSize())
	{
		omTabBackColors.RemoveAt(omTabBackColors.GetSize()-1);
	}
	SetModifiedFlag();
}

long CUGanttCtrl::GetTabLines() 
{
	return TabLines;
}

void CUGanttCtrl::SetTabLines(long nNewValue) 
{
	TabLines = nNewValue;
	SetModifiedFlag();
}

BSTR CUGanttCtrl::GetTabHeaderString() 
{
	CString strResult;
	strResult = TabHeaderString;

	return strResult.AllocSysString();
}

void CUGanttCtrl::SetTabHeaderString(LPCTSTR lpszNewValue) 
{
	TabHeaderString = CString(lpszNewValue);
	SetModifiedFlag();
}

BSTR CUGanttCtrl::GetTabHeaderLengthString() 
{
	CString strResult;
	strResult = TabHeaderLengthString;
	return strResult.AllocSysString();
}

void CUGanttCtrl::SetTabHeaderLengthString(LPCTSTR lpszNewValue) 
{
	omTabHeaderLen.RemoveAll();
	TabHeaderLengthString = CString(lpszNewValue);
	CStringArray olItems;
	CString olTmp = TabHeaderLengthString;
	ExtractTextLineFast (olItems, olTmp.GetBuffer(0), ",");
	imTabLogicalWidth = 0;
	TabColumns = olItems.GetSize();
	for(int i = 0; i < olItems.GetSize(); i++)
	{
		omTabHeaderLen.Add(atoi(olItems[i]));
		imTabLogicalWidth += atoi(olItems[i]);
	}
	while(olItems.GetSize() < omTabHeader.GetSize())
	{
		omTabHeader.RemoveAt(omTabHeader.GetSize()-1);
	}
	while(olItems.GetSize() > omTabHeader.GetSize())
	{
		omTabHeader.Add("xyz");
	}
	SetModifiedFlag();
}

short CUGanttCtrl::GetTabLineHeight() 
{
	return TabLineHeight;
}

void CUGanttCtrl::SetTabLineHeight(short nNewValue) 
{
	TabLineHeight = nNewValue;
	for (int i = 0; i < omLines.GetSize(); i++)
	{
		HandleLineHight (i);
	}
	SetModifiedFlag();
}

short CUGanttCtrl::GetTabFontSize() 
{
	return TabFontSize;
}

void CUGanttCtrl::SetTabFontSize(short nNewValue) 
{
	TabFontSize = nNewValue;
	CString olS;
	olS.Format("%d", nNewValue);
	m_tabFontName = olS;
	SetModifiedFlag();
	omTabFont.DeleteObject();
	if(m_tabBodyFontBold == TRUE)
	{
		omTabFont.CreateFont( TabFontSize, 0, 0, 0, FW_BOLD, 0, 0, 0, DEFAULT_CHARSET, OUT_CHARACTER_PRECIS, CLIP_DEFAULT_PRECIS, 
								 PROOF_QUALITY, DEFAULT_PITCH, m_tabFontName  );
	}
	else
	{
		omTabFont.CreateFont( TabFontSize, 0, 0, 0, FW_NORMAL, 0, 0, 0, DEFAULT_CHARSET, OUT_CHARACTER_PRECIS, CLIP_DEFAULT_PRECIS, 
								 PROOF_QUALITY, DEFAULT_PITCH, m_tabFontName );
	}
	SetModifiedFlag();
	InvalidateControl();
}

short CUGanttCtrl::GetTabHeaderFontSize() 
{
	return TabHeaderFontSize;
}

void CUGanttCtrl::SetTabHeaderFontSize(short nNewValue) 
{
	TabHeaderFontSize = nNewValue;
	SetModifiedFlag();
}

BSTR CUGanttCtrl::GetTabHeaderFontName() 
{
	CString strResult;
	strResult = TabHeaderFontName;
	return strResult.AllocSysString();
}

void CUGanttCtrl::SetTabHeaderFontName(LPCTSTR lpszNewValue) 
{
	TabHeaderFontName = CString(lpszNewValue);
	SetModifiedFlag();
	omTabHeaderFont.DeleteObject();
	if(m_tabHeaderFontBold == TRUE)
	{
		omTabHeaderFont.CreateFont( TabHeaderFontSize, 0, 0, 0, FW_BOLD, 0, 0, 0, DEFAULT_CHARSET, OUT_CHARACTER_PRECIS, CLIP_DEFAULT_PRECIS, 
								 PROOF_QUALITY, DEFAULT_PITCH, TabHeaderFontName  );
	}
	else
	{
		omTabHeaderFont.CreateFont( TabHeaderFontSize, 0, 0, 0, FW_NORMAL, 0, 0, 0, DEFAULT_CHARSET, OUT_CHARACTER_PRECIS, CLIP_DEFAULT_PRECIS, 
								 PROOF_QUALITY, DEFAULT_PITCH, TabHeaderFontName );
	}
	SetModifiedFlag();
	InvalidateControl();

}

BSTR CUGanttCtrl::GetTabBodyFontName() 
{
	CString strResult;
	strResult = TabBodyFontName;
	return strResult.AllocSysString();
}

void CUGanttCtrl::SetTabBodyFontName(LPCTSTR lpszNewValue) 
{
	m_tabFontName = CString(lpszNewValue);
	SetModifiedFlag();
	omTabFont.DeleteObject();
	if(m_tabBodyFontBold == TRUE)
	{
		omTabFont.CreateFont( TabFontSize, 0, 0, 0, FW_BOLD, 0, 0, 0, DEFAULT_CHARSET, OUT_CHARACTER_PRECIS, CLIP_DEFAULT_PRECIS, 
								 PROOF_QUALITY, DEFAULT_PITCH, m_tabFontName  );
	}
	else
	{
		omTabFont.CreateFont( TabFontSize, 0, 0, 0, FW_NORMAL, 0, 0, 0, DEFAULT_CHARSET, OUT_CHARACTER_PRECIS, CLIP_DEFAULT_PRECIS, 
								 PROOF_QUALITY, DEFAULT_PITCH, m_tabFontName );
	}
	SetModifiedFlag();
	InvalidateControl();
}

void CUGanttCtrl::TabSetHeaderText(LPCTSTR HeaderText) 
{
	CStringArray olItems;
	CString olTmp = CString(HeaderText);
	ExtractTextLineFast (olItems, olTmp.GetBuffer(0), ",");
	omTabHeader.RemoveAll();
	int ilHeaderCount = omTabHeader.GetSize();

	omGroupCols.RemoveAll();
	omGroupAlternateColor.RemoveAll();
	for(int i = 0; (i < TabColumns && i < olItems.GetSize()); i++)
	{
		omTabHeader.Add(olItems[i]);
		omGroupCols.Add(-1);
		omGroupAlternateColor.Add(-1);
	}
}

void CUGanttCtrl::AppendLine(LPCTSTR HeaderText) 
{
	// TODO: Add your dispatch handler code here

}

//**********************************************************
//*** reset the the relevant data of the Gantt
//***
//*** created from: MWO				date: ?
//***
//*** changes:
//*** vers.		date		who		what
//***	0.1		11.10.00	RRO		reset pomHScroll
//***
//**********************************************************
void CUGanttCtrl::ResetContent() 
{
	omLines.DeleteAll();
	omTabTextColors.RemoveAll();
	omTabBackColors.RemoveAll();
	omTabLineHeight.RemoveAll();
	omGroupCols.RemoveAll();
	omGroupAlternateColor.RemoveAll();
	TabLines = 0;
	TabColumns = 0;
	if(pomTabVScroll != NULL)
	{
		pomTabVScroll->SetScrollPos(0);
		pomTabVScroll->SetScrollRange(0,0);
	}
	if(pomVScroll != NULL)
	{
		pomVScroll->SetScrollPos(0);
		pomVScroll->SetScrollRange(0,0);
	}
	if(pomHScroll != NULL)
	{
		pomHScroll->SetScrollPos(0);
		pomHScroll->SetScrollRange(0,0);
	}
	if(TabLines != omLines.GetSize())
	{
		TabLines = omLines.GetSize();
	}

	imTabSelectedLine		= -1;
	bmAllSelected			= false;
	omTabHeader.RemoveAll();
	omTabHeaderLen.RemoveAll();
	TabHeaderString			= "";
	TabHeaderLengthString	= "";
}

void CUGanttCtrl::MakeNewLine(long LineNo, LPCTSTR TabValues) 
{
	// TODO: Add your dispatch handler code here

}

void CUGanttCtrl::TabSetLineColors(long LineNo, long TextColor, long BackColor) 
{
	// TODO: Add your dispatch handler code here

}

void CUGanttCtrl::GanttSetBackColor(long BackColor) 
{
	omBackgroundColor = BackColor;
}

void CUGanttCtrl::DeleteLine(long LineNo) 
{
	if(LineNo < omLines.GetSize() && LineNo != -1)
	{
		omLines.DeleteAt(LineNo);
	}
	TabLines	= omLines.GetSize();
}

long CUGanttCtrl::TabGetCurrentSelected() 
{
	// TODO: Add your dispatch handler code here

	return 0;
}

BSTR CUGanttCtrl::TabGetLinesByTextColor(long Color) 
{
	CString strResult;
	// TODO: Add your dispatch handler code here
	return strResult.AllocSysString();
}

BSTR CUGanttCtrl::TabGetLinesByBackColor(long Color) 
{
	CString strResult;
	// TODO: Add your dispatch handler code here
	return strResult.AllocSysString();
}

void CUGanttCtrl::TabUpdateValues(long LineNo, LPCTSTR Values) 
{
	// TODO: Add your dispatch handler code here
}

//**********************************************************
//*** the FROM-time of the timescale; it's the earliest time
//*** you can scroll to
//***
//*** created from: MWO				date: ???
//***
//*** changes:
//*** vers.		date		who		what
//***
//**********************************************************
DATE CUGanttCtrl::GetTimeFrameFrom() 
{
	return (DATE)TimeFrameFrom.m_dt;
}
void CUGanttCtrl::SetTimeFrameFrom(DATE newValue) 
{
	TimeFrameFrom = COleDateTime (newValue);
	omStartTime   = TimeFrameFrom; 
	SetModifiedFlag();
}

//**********************************************************
//*** the TO-time of the timescale; it's the latest time
//*** you can scroll to
//***
//*** created from: MWO				date: ???
//***
//*** changes:
//*** vers.		date		who		what
//***
//**********************************************************
DATE CUGanttCtrl::GetTimeFrameTo() 
{
	return (DATE) TimeFrameTo.m_dt;
}
void CUGanttCtrl::SetTimeFrameTo (DATE newValue) 
{
	TimeFrameTo = COleDateTime (newValue);
	SetModifiedFlag();
}

//**********************************************************
//*** control the height of the timescale. Be carefull to
//*** update the number of visible lines - it changes!
//***
//*** created from: RRO				date: 20.11.00
//***
//*** changes:
//*** vers.		date		who		what
//***
//**********************************************************
long CUGanttCtrl::GetTimeScaleHeaderHeight() 
{
	return TimeScaleHeaderHeight;
}
void CUGanttCtrl::SetTimeScaleHeaderHeight(long nNewValue) 
{
	TimeScaleHeaderHeight = nNewValue;
	CalcAreaRects();
	imMaxVisibleLines	  = GetNumberOfVisibleLines ();
	SetModifiedFlag();
}

int CUGanttCtrl::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	lpCreateStruct->style |= WS_TABSTOP;
	if (COleControl::OnCreate(lpCreateStruct) == -1)
		return -1;

	BOOL blTest = m_DropTarget.Register(this);

	CRect olRect, olScrollerRect;
	GetClientRect(&olRect);
	olRect.left = olRect.right - 15;

	SetTimer(0, (UINT)60*1000, NULL);
	SetTimer(1, (UINT)10, NULL);

	pomVScroll = new CScrollBar();
	pomHScroll = new CScrollBar();
	Handle2ndScroller();

	pomVScroll->Create (SBS_VERT, olRect, this, 50);
	pomVScroll->SetScrollRange (0, 10);
	pomVScroll->ShowWindow (SW_SHOWNORMAL);

	pomVScroll->GetClientRect (&olScrollerRect);
	imTabVScrollWidth = olScrollerRect.right - olScrollerRect.left;

	GetClientRect (&olRect);

	olRect.top = olRect.bottom - 15;
	pomHScroll->Create (SBS_HORZ, olRect, this, 50);
	pomHScroll->SetScrollRange (0, 1000/*imTimeScaleDuration*60*/);
	pomHScroll->ShowWindow (SW_SHOWNORMAL);

	pomHScroll->GetClientRect (&olScrollerRect);
	imTabHScrollHeight = olScrollerRect.bottom - olScrollerRect.top;

	CalcAreaRects();

	pomTimeScale = new CCSTimeScale (this);
	pomTimeScale->m_lifeStyle = FALSE;
	pomTimeScale->Create (NULL, "TimeScale", WS_CHILD|WS_VISIBLE|WS_BORDER, omTimeScaleRect, this, 0, NULL);
	pomTimeScale->SetDisplayTimeFrame (TimeFrameFrom/*olTimeFrameFrom*/, COleDateTimeSpan(0,imTimeScaleDuration,0,0), COleDateTimeSpan(0,0,10,0));

	for (int i = 0; i < omLines.GetSize(); i++)
	{
		omTabLineHeight.Add(20);
		omLines[i].MaxOverlapLevel = 0;
		for (int j = 0; j < omLines[i].omBars.GetSize(); j++)
		{
			omLines[i].omBars[j].OverlapLevel = 0;
		}
	}

	//- - - - - - - - - - - - - - - -
	//- - bitmap section
	//- - - - - - - - - - - - - - - -
	// get rid of old bitmaps
	if (bmpGantt.m_hObject != NULL)
		bmpGantt.DeleteObject();

	CClientDC dc (this);
	CDC dcMem;
	dcMem.CreateCompatibleDC (&dc);

	//now create the bitmaps - without the moving bitmap, because its size changes!
	bmpGantt.CreateCompatibleBitmap	(&dc, omGanttRect.right, omGanttRect.bottom);

	ASSERT (bmpGantt.m_hObject != NULL);

	dcMem.DeleteDC();

	FireActualTimescaleDate (pomTimeScale->GetDisplayStartTime());

	return 0;
}

//**********************************************************
//*** handles the second scroller
//***
//*** created from: MWO				date: ???
//***
//*** changes:
//*** vers.		date		who		what
//***	RRO		28.11.00	RRO		set the scroll pos on init
//***
//**********************************************************
void CUGanttCtrl::Handle2ndScroller()
{
	if(With2ndScroller == TRUE)
	{
		CRect olRect;
		GetClientRect (&olRect);

		olRect.left = olRect.right - 15;
		if (pomTabVScroll == NULL)
		{
			pomTabVScroll = new CScrollBar();
			pomTabVScroll->Create (SBS_VERT, olRect, this, 50);
		}
		int ilScrollMin =  0;
		int ilScrollMax = 10;
		int ilScrollPos =  0;

		if(pomVScroll != NULL)
		{
			pomVScroll->GetScrollRange(&ilScrollMin, &ilScrollMax);
			ilScrollPos = pomVScroll->GetScrollPos ();
		}
		pomTabVScroll->SetScrollRange (ilScrollMin, ilScrollMax);
		pomTabVScroll->SetScrollPos   (ilScrollPos);

		CRect olScrollerRect;
		pomTabVScroll->GetClientRect (&olScrollerRect);
		imTab2ndVScrollWidth = olScrollerRect.right - olScrollerRect.left;

		int ilW = olScrollerRect.right - olScrollerRect.left;
		olScrollerRect.left = SplitterPosition+6;
		olScrollerRect.right = olScrollerRect.left + ilW;
		
		pomTabVScroll->MoveWindow (&olScrollerRect);
		pomTabVScroll->ShowWindow (SW_SHOWNORMAL);
	}
	else
	{
		imTab2ndVScrollWidth = 0;
		if (pomTabVScroll != NULL)
		{
			delete pomTabVScroll;
			pomTabVScroll = NULL;
		}
	}
}

//**********************************************************
//*** handles the horizontal scrollbar
//*** calculations are necessary,
//*** so that it is in the right position
//***
//*** created from: RRO				date: 06.11.00
//***
//*** changes:
//*** vers.		date		who		what
//***
//**********************************************************
void CUGanttCtrl::HandleHorizontalScroller()
{
	if (WithHorizontalScroller != FALSE)
	{
		//calc the rect of the scrollbar
		CRect olRect;
		GetClientRect (&olRect);
		olRect.top = olRect.bottom - 15;

		//create the scrollbar
		pomHScroll = new CScrollBar ();
		pomHScroll->Create (SBS_HORZ, olRect, this, 50);
		pomHScroll->SetScrollRange (0, 1000);

		//calc the actual position of the scrollbar
		COleDateTimeSpan olTSTimeFrame  = TimeFrameTo - TimeFrameFrom;			//duration of the Timeframe
		COleDateTimeSpan olTSDisplay	= pomTimeScale->GetDisplayDuration();	//duration of what you can see
		double dlRelation = (pomTimeScale->GetDisplayStartTime() - TimeFrameFrom) /	(olTSTimeFrame - olTSDisplay);
		int ilScrollRange, ilMinPos, ilMaxPos;
		pomHScroll->GetScrollRange (&ilMinPos, &ilMaxPos);
		ilScrollRange = ilMaxPos - ilMinPos; //... I know, it is 1000 ... (see 8 lines above...)
		pomHScroll->SetScrollPos ((int)(ilScrollRange * dlRelation));

		//show the scrollbar
		pomHScroll->ShowWindow (SW_SHOWNORMAL);
		CalcAreaRects ();
	}
	else
	{
		if (pomHScroll != NULL)
		{
			delete pomHScroll;
			pomHScroll = NULL;
			CalcAreaRects();
		}
	}
}

//**********************************************************
//*** you can set the begin date of the visible timespan.
//*** it also effects the horizontal scrollbar, if there's one.
//***
//*** created from: RRO				date: 06.11.00
//***
//*** changes:
//*** vers.		date		who		what
//***
//**********************************************************
void CUGanttCtrl::ScrollTo(DATE FromDate) 
{
	//set the new starttime of the display
	omStartTime = FromDate;
	CalcAreaRects();

	//calc the actual position of the horizontal scrollbar (if there is one)
	if (pomHScroll != NULL)
	{
		COleDateTimeSpan olTSTimeFrame  = TimeFrameTo - TimeFrameFrom;			//duration of the Timeframe
		COleDateTimeSpan olTSDisplay	= pomTimeScale->GetDisplayDuration();	//duration of what you can see
		double dlRelation = (pomTimeScale->GetDisplayStartTime() - TimeFrameFrom) /	(olTSTimeFrame - olTSDisplay);
		int ilScrollRange, ilMinPos, ilMaxPos;
		pomHScroll->GetScrollRange (&ilMinPos, &ilMaxPos);
		ilScrollRange = ilMaxPos - ilMinPos;
		pomHScroll->SetScrollPos ((int)(ilScrollRange * dlRelation));
	}
}

//**********************************************************
//*** delivers the time of the specified x-coordinate
//***
//***
//*** created from: RRO				date: 25.08.00
//***
//*** changes:
//*** vers.		date		who		what
//***
//**********************************************************
COleDateTime CUGanttCtrl::GetTimeFromX(int ipX)
{
	COleDateTime	olTime;
	int 			ilRelativePoint;

	ilRelativePoint = (ipX - omGanttRect.left);
	olTime			= pomTimeScale->GetTimeFromX (ilRelativePoint);
	return olTime;
}

//**********************************************************
//*** delivers the x-coordinate of the specified time
//***
//***
//*** created from: RRO				date: 25.08.00
//***
//*** changes:
//*** vers.		date		who		what
//***
//**********************************************************
int CUGanttCtrl::GetXFromTime(COleDateTime opTime)
{
	int ilX = pomTimeScale->GetXFromTime (opTime) + omGanttRect.left;
	return ilX;
}


void CUGanttCtrl::DrawTimeScale(CDC* pdc)
{
	CBrush olBlackBrush(COLORREF(RGB(0,0,0)));
	CPen olWhitePen(PS_SOLID,1,COLORREF(RGB(255,255,255))),
		 *pOldPen;
	CBrush olSilverBrush(::GetSysColor(COLOR_BTNFACE));//(COLORREF(RGB(192,192,192)));
	pdc->FillRect(&omTimeScaleRect, &olSilverBrush);
	pdc->FrameRect(&omTimeScaleRect, &olBlackBrush);
	pOldPen = pdc->SelectObject(&olWhitePen);
	pdc->MoveTo(omTimeScaleRect.left+1, omTimeScaleRect.bottom-2);
	pdc->LineTo(omTimeScaleRect.left+1, omTimeScaleRect.top+1);
	pdc->LineTo(omTimeScaleRect.right-1, omTimeScaleRect.top+1);
	pdc->SelectObject(pOldPen);
}

//Table methods
void CUGanttCtrl::DrawTab(CDC *pdc)
{
	CRect olRect, olOrigClientRect;
	//GetWindowRect(&olRect);
	olRect = omCurrentTabRect;
	olOrigClientRect = olRect;
	int ilYExtent = olRect.bottom;

	//MWO 11.02.2003
	CStringArray olOldTabValues;
	CStringArray olCurrTabValues; // END MWO 11.02.2003
	CCSPtrArray<CBrush>  olAltBrushes;
	CCSPtrArray<CBrush>  olCurrBrushes;
	CCSPtrArray<bool>	 olEvenOdd;
//---------
//Calculate the number of possible lines
	if (imMaxVisibleLines < 0)
		imMaxVisibleLines = GetNumberOfVisibleLines();
	int ilMaxVisibleLines = imMaxVisibleLines;//GetNumberOfVisibleLines();//(int)(((olOrigClientRect.bottom - olOrigClientRect.top)/TabLineHeight));
	CRect olScRect;
	int ilDiff = 0;
	if(pomVScroll != NULL)
	{
		pomVScroll->GetWindowRect(&olScRect);
		ilDiff = olScRect.right - olScRect.left;
	}
	int ilWidth = olRect.right - olRect.left - ilDiff;
	int ilSingleWidth = 0;
	if(TabColumns > 0)
	{
		ilSingleWidth = (int)(ilWidth/TabColumns);
	}
	int ilTop = olRect.top;
	CBrush olBrush(COLORREF(RGB(0,0,0)));
	CBrush olGrayBrush(::GetSysColor(COLOR_BTNFACE));//(COLORREF(RGB(192,192,192)));
	CBrush olWhiteBrush(COLORREF(RGB(255,255,255)));
	CBrush olBackColorBrush;//(COLORREF(RGB(128,128,128)));
	CBrush olBackground(COLORREF(RGB(128,128,128)));
	CBrush olBlueBrush(COLORREF(RGB(0,0,255)));
	CRect olTmpRect = CRect(0, 0, olOrigClientRect.right - olOrigClientRect.left, olOrigClientRect.bottom-olOrigClientRect.top);

	CPen olGrayPen(PS_SOLID, 1, COLORREF(RGB(128,128,128)));
	int ilGridWidth=0;

	int i;
	for( i = 0; i < omTabHeaderLen.GetSize(); i++)
	{
		ilGridWidth += (int)omTabHeaderLen[i];
	}
	if(ilGridWidth > SplitterPosition)
	{
		ilGridWidth = SplitterPosition;
	}
	int ilHeaderCount = omTabHeader.GetSize();
	if(TabColumns < ilHeaderCount)
	{
		TabColumns = ilHeaderCount;
	}

	int ilMyTabWidth = 0;
	for( i = 0; (i < TabColumns && i < omTabHeaderLen.GetSize()); i++)
	{
		ilMyTabWidth += omTabHeaderLen[i];
	}


	//MWO: 25.06.03
	CDC myMemDC;
	CBitmap	bmpTab;
	CRect myRightOutSideRect; 

	myMemDC.CreateCompatibleDC (pdc);
	bmpTab.CreateCompatibleBitmap	(pdc, omCurrentTabRect.right, omCurrentTabRect.bottom);
	myMemDC.SelectObject (&bmpTab);

	CRect myTabLifeStyleRect = omCurrentTabRect;
	myTabLifeStyleRect.right = ilMyTabWidth;
	myRightOutSideRect = omCurrentTabRect;
	myRightOutSideRect.left = myTabLifeStyleRect.right;
	//MWO: 25.06.03
	if (m_lifeStyle == TRUE)
	{
		DrawLifeStyle(&myMemDC, myTabLifeStyleRect, omLifeStyle.Tab_ColorIdx, omLifeStyle.Tab_Type, omLifeStyle.Tab_StyleUp, omLifeStyle.Tab_Percent);
	}
	myMemDC.FillRect(&myRightOutSideRect, &olBackground);

	CFont *polOldFont;
	polOldFont = myMemDC.SelectObject(&omTabHeaderFont);	
	CPen olWhitePen(PS_SOLID, 1, COLORREF(RGB(255,255,255)));
	CPen olBluePen(PS_SOLID, 1, COLORREF(RGB(0,0,255)));
	CPen *pOldPen;
	pOldPen = myMemDC.SelectObject(&olWhitePen);
	int ilCurrX = 0;
	if (m_lifeStyle == TRUE)
	{
		int ilLFWidth = 0;
		for( i = 0; (i < TabColumns && i < omTabHeaderLen.GetSize()); i++)
		{
			ilLFWidth += omTabHeaderLen[i];
		}
		if(ilLFWidth >= SplitterPosition)
		{
			ilLFWidth = SplitterPosition;
		}
		CRect olLFRect;
		olLFRect = CRect(ilCurrX, 0, ilLFWidth, TimeScaleHeaderHeight);
		olLFRect.bottom = olLFRect.bottom-2;
		DrawLifeStyle(&myMemDC, olLFRect, omLifeStyle.TimeScale_ColorIdx, omLifeStyle.TimeScale_Type, omLifeStyle.TimeScale_StyleUp, omLifeStyle.TimeScale_Percent);
	}


	for(  i = 0; (i < TabColumns && i < omTabHeaderLen.GetSize()); i++)
	{
		CRect olHRect;
		olHRect = CRect(ilCurrX, 0, ilCurrX + omTabHeaderLen[i]+1, TimeScaleHeaderHeight);//TabLineHeight);
		if(olHRect.right >= SplitterPosition)
		{
			olHRect.right = SplitterPosition;
		}

		if(olHRect.right < olHRect.left)
		{
			break;
		}
		if (m_lifeStyle == FALSE)
		{
			myMemDC.FillRect(&olHRect, &olGrayBrush);
		}
		if(i < omTabHeader.GetSize())
		{
			myMemDC.SetBkMode(TRANSPARENT);
			CRect olHTextRect;
			olHTextRect = olHRect;
			//olHTextRect.top +=2;
			olHTextRect.top = olHRect.bottom - TabHeaderFontSize;
			olHTextRect.left += 2;
			if (m_lifeStyle == TRUE)
			{
				myMemDC.SetTextColor(RGB(0,0,255));
			}
			myMemDC.DrawText( omTabHeader[i], omTabHeader[i].GetLength(), olHTextRect, DT_LEFT );
		}
		myMemDC.SetTextColor(RGB(0,0,0));

		myMemDC.FrameRect( &olHRect, &olBrush );
		ilCurrX += omTabHeaderLen[i];
		
		myMemDC.MoveTo(olHRect.left+1, olHRect.bottom-1);
		myMemDC.LineTo(olHRect.left+1, olHRect.top+1);
		myMemDC.LineTo(olHRect.right-1, olHRect.top+1);
	}
	myMemDC.SelectObject(polOldFont);
//	myMemDC.SelectObject(polOldPen);
	int ilCurrY = TimeScaleHeaderHeight;//TabLineHeight;
	ilCurrX = 0;
	polOldFont = myMemDC.SelectObject(&omTabFont);	
	int ilStartIdx = 0;
	if(pomVScroll != NULL)
	{
		ilStartIdx = pomVScroll->GetScrollPos();
	}

	CString olTmp;
	for( i = ilStartIdx; (i < TabLines && i < ilStartIdx + ilMaxVisibleLines); i++)
	{
		CStringArray olLineItems;
		COLORREF olTextColor = COLORREF(RGB(0,0,0));
		COLORREF olBackColor = COLORREF(RGB(255,255,255));
		if (i < omLines.GetSize())
		{
			
			olTmp = omLines[i].TabValues;
			ExtractTextLineFast (olLineItems, olTmp.GetBuffer(0), ",");
			//MWO 11.02.03
			if(olOldTabValues.GetSize() < olLineItems.GetSize())
			{
				olOldTabValues.RemoveAll();
				for(int ilC = 0; (ilC < olLineItems.GetSize()) && (ilC<omGroupAlternateColor.GetSize()); ilC++)
				{
					CBrush *polB;
					CBrush *pN;
					bool bl = true;
					//COLORREF colRef = COLORREF((const)omGroupAlternateColor[ilC]);
					COLORREF colRef = COLORREF(omGroupAlternateColor[ilC]);
					polB = new CBrush(colRef);
					pN = new CBrush(::GetSysColor(COLOR_BTNFACE));//(COLORREF(RGB(192,192,192)));
					olOldTabValues.Add("");
					olAltBrushes.Add(polB);
					olEvenOdd.NewAt(olEvenOdd.GetSize(), bl);
					olCurrBrushes.Add(pN);
				}
			}//END MWO 11.02.03
			olTextColor = COLORREF(omLines[i].TabTextColor);
			olBackColor = COLORREF(omLines[i].TabBackColor);
		}
		olBackColorBrush.CreateSolidBrush(olBackColor);
		ilCurrX = 0;
		bool blBefore = true;
		for(int j = 0; (j < TabColumns && j < omTabHeaderLen.GetSize()); j++)
		{
			CRect olHRect;
			olHRect = CRect(ilCurrX, ilCurrY, ilCurrX + omTabHeaderLen[j]+1, ilCurrY + omTabLineHeight.GetAt(i));//TabLineHeight);
			if(olHRect.right >= SplitterPosition)
			{
				olHRect.right = SplitterPosition;
			}
			if(olHRect.right < olHRect.left)
			{
				break;
			}
			if(i == imTabSelectedLine)
			{
				myMemDC.FillRect(&olHRect, &olBlueBrush);
				myMemDC.SetTextColor(COLORREF(RGB(255,255,255)));
			}
			else
			{
				if(i == imCurrentHighlightLine)
				{
					CBrush olCurrLB(TabHighlightColor);
					myMemDC.FillRect(&olHRect, &olCurrLB);
					myMemDC.SetTextColor(olTextColor);
					if (bmDrawGLINE != FALSE)//(omDrawHint.Find("G_LINE") != -1)
					{
						DrawBarsOfLine (bmpGantt, i, ilCurrY); //DrawGanttLine(pdc, i, ilCurrY);
					}
				}
				else
				{
					if (m_lifeStyle == FALSE) //MWO: 25.06.03
					{
						myMemDC.FillRect(&olHRect, &olBackColorBrush);
						myMemDC.SetTextColor(olTextColor);
					}
				}
			}
			olHRect.top -= 1;
			//MWO 12.02.03
			if(omGroupCols[j] == -1)
			{
				myMemDC.FrameRect( &olHRect, &olBackground);
			}
			myMemDC.SetBkMode(TRANSPARENT);
			olHRect.left += 2;
			olHRect.top += 1;
			if(j < olLineItems.GetSize())
			{
				CRect olTR;
				olTR = olHRect;
				olTR.left++;
				olTR.right++;
				CString olOld = olOldTabValues[j];
				CString olNew = olLineItems[j];
				if(omGroupCols[j] == -1)
				{
					myMemDC.DrawText( olLineItems[j], olLineItems[j].GetLength(), olTR, DT_LEFT );
				}
				else
				{
					if(olLineItems[j] == "319")
					{
						int o;
						o=0;
					}
					if(j>0)
					{
						if(blBefore == true)
						{
							olEvenOdd[j] = !olEvenOdd[j];
/*							if(olEvenOdd[j] == true)
							{
								myMemDC.FillRect(&olHRect, &olAltBrushes[j]);
							}
*/
							myMemDC.SelectObject(olGrayPen);
							myMemDC.MoveTo(olHRect.right-2, olHRect.top-1);
							myMemDC.LineTo(olHRect.left-2, olHRect.top-1);
							myMemDC.LineTo(olHRect.left-2, olHRect.bottom);
							myMemDC.DrawText( olLineItems[j], olLineItems[j].GetLength(), olHRect, DT_LEFT );
							blBefore = true;
						}
						else if(olOldTabValues[j] != olLineItems[j])
						{
							olEvenOdd[j] = !olEvenOdd[j];
/*							if(olEvenOdd[j] == true)
							{
								myMemDC.FillRect(&olHRect, &olAltBrushes[j]);
							}
*/
							myMemDC.SelectObject(olGrayPen);
							myMemDC.MoveTo(olHRect.right-2, olHRect.top-1);
							myMemDC.LineTo(olHRect.left-2, olHRect.top-1);
							myMemDC.LineTo(olHRect.left-2, olHRect.bottom);
							myMemDC.DrawText( olLineItems[j], olLineItems[j].GetLength(), olHRect, DT_LEFT );
							blBefore = true;
						}
						else
						{
							blBefore = false;
/*							if(olEvenOdd[j] == true)
							{
								myMemDC.FillRect(&olHRect, &olAltBrushes[j]);
							}
*/
						}
					}
					else
					{
						if(olOldTabValues[j] != olLineItems[j])
						{
							olEvenOdd[j] = !olEvenOdd[j];
/*							if(olEvenOdd[j] == true)
							{
								myMemDC.FillRect(&olHRect, &olAltBrushes[j]);
							}
*/
							myMemDC.SelectObject(olGrayPen);
							myMemDC.MoveTo(olHRect.right-2, olHRect.top-1);
							myMemDC.LineTo(olHRect.left-2, olHRect.top-1);
							myMemDC.LineTo(olHRect.left-2, olHRect.bottom);
							myMemDC.DrawText( olLineItems[j], olLineItems[j].GetLength(), olHRect, DT_LEFT );
							blBefore = true;
						}
						else
						{
							blBefore = false;
/*							if(olEvenOdd[j] == true)
							{
								myMemDC.FillRect(&olHRect, &olAltBrushes[j]);
							}
*/
						}
					}
				}
				myMemDC.SelectObject(olGrayPen);
				myMemDC.MoveTo(olHRect.left-2, olHRect.top-1);
				myMemDC.LineTo(olHRect.left-2, olHRect.bottom);
			}
			ilCurrX += omTabHeaderLen[j];
		}
		//Black header frame
		if(i == ilStartIdx)
		{
			CRect olCurrRect;
			olCurrRect = omCurrentTabRect;
			CPen olPen(PS_SOLID, 1, COLORREF(RGB(0,0,0)));
			myMemDC.SelectObject(&olPen);
			myMemDC.MoveTo(olCurrRect.left, ilCurrY-1);
			myMemDC.LineTo(ilGridWidth, ilCurrY-1);
		}
		olBackColorBrush.DeleteObject();
		ilCurrY += omTabLineHeight.GetAt(i);//TabLineHeight;

		//Store old values to ensure the alternate color grouping
		olOldTabValues.RemoveAll();
		for(int ilC = 0; ilC < olLineItems.GetSize(); ilC++)
		{
			olOldTabValues.Add(olLineItems[ilC]);
		}
	}

	CRect olRestRect;
	GetClientRect(&olRestRect);

	if (m_lifeStyle == FALSE) //MWO: 25.06.03
	{
		if(ilCurrY < olOrigClientRect.bottom)
		{
			//olRestRect = olOrigClientRect;
			int ilGridWidth=0;
			for(int i = 0; i < omTabHeaderLen.GetSize(); i++)
			{
				ilGridWidth += (int)omTabHeaderLen[i];
			}
			olRestRect.top = ilCurrY;
			if(ilGridWidth >= SplitterPosition)
			{
				ilGridWidth = SplitterPosition;
			}
			else
			{
			;//	ilGridWidth = omCurrentTabRect.right;
			}
			olRestRect.right = ilGridWidth;
			myMemDC.FillRect(&olRestRect, &olBackground/*olWhiteBrush*/);
		}
	}
//MWO: 25.06.03 
	//Flush it to the screen
	pdc->BitBlt( omCurrentTabRect.left, omCurrentTabRect.top, omCurrentTabRect.right, omCurrentTabRect.bottom, &myMemDC, omCurrentTabRect.left, omCurrentTabRect.top, SRCCOPY);

	olAltBrushes.DeleteAll();
	olCurrBrushes.DeleteAll();
	olEvenOdd.DeleteAll();
}

//Splitter methods
void CUGanttCtrl::DrawSplitter(CDC *pdc)
{
	CPen olWhiteP  (PS_SOLID, 1, COLORREF(RGB(255,255,255))), 
		 olSilverP (PS_SOLID, 1, ::GetSysColor(COLOR_BTNFACE)),// COLORREF(RGB(192,192,192))),
		 olGrayP   (PS_SOLID, 1, COLORREF(RGB(128,128,128))),
		 olBlackP  (PS_SOLID, 1, COLORREF(RGB(  0,  0,  0))),
		 *pOldPen;
	CRect olRect;
	GetClientRect (&olRect);

	pOldPen = pdc->SelectObject (&olWhiteP);
	pdc->MoveTo (SplitterPosition,     olRect.top);
	pdc->LineTo (SplitterPosition,     olRect.bottom);
	pdc->SelectObject (&olSilverP);
	pdc->MoveTo (SplitterPosition + 1, olRect.top);
	pdc->LineTo (SplitterPosition + 1, olRect.bottom);
	pdc->MoveTo (SplitterPosition + 2, olRect.top);
	pdc->LineTo (SplitterPosition + 2, olRect.bottom);
	pdc->MoveTo (SplitterPosition + 3, olRect.top);
	pdc->LineTo (SplitterPosition + 3, olRect.bottom);
	pdc->SelectObject (&olGrayP);
	pdc->MoveTo (SplitterPosition + 4, olRect.top);
	pdc->LineTo (SplitterPosition + 4, olRect.bottom);
	pdc->SelectObject (&olBlackP);
	pdc->MoveTo (SplitterPosition + 5, olRect.top);
	pdc->LineTo (SplitterPosition + 5, olRect.bottom);
	pdc->SelectObject (&olBlackP);
	pdc->MoveTo (SplitterPosition,	   olRect.top);
	pdc->LineTo (SplitterPosition + 5, olRect.top);
	pdc->MoveTo (SplitterPosition,	   olRect.bottom - 1);
	pdc->LineTo (SplitterPosition + 5, olRect.bottom - 1);
	pdc->SelectObject (pOldPen);
}

void CUGanttCtrl::DrawGanttBmp()
{
	CRect olRect;
	GetClientRect(&olRect);

	CClientDC dc (this);
	CDC dcMem;

	dcMem.CreateCompatibleDC(&dc);

	CBitmap lBmp2;
	olRect.right -= imTabVScrollWidth;
	olRect.bottom -= imTabHScrollHeight;
	//now create the bitmaps - without the moving bitmap, because its size changes!
	lBmp2.CreateCompatibleBitmap(&dc, olRect.right , olRect.bottom);


	dcMem.SelectObject(&lBmp2);

	DrawGanttBmp(&dcMem, false);

	int x	= omGanttRect.left;
	int y	= omGanttRect.top;
	int dx	= omGanttRect.right  - x;
	int dy	= omGanttRect.bottom - y;

	dc.BitBlt ( x, y, dx, dy, &dcMem, x, y, SRCCOPY);

	dcMem.DeleteDC();

}

//**********************************************************
//*** draws the whole memory bitmap of the Gantt by doing
//*** some calculations and calling the functions to draw
//*** the bkbars, the arrows and the bars
//***
//*** created from: RRO				date: 25.08.00
//***
//*** changes:
//*** vers.		date		who		what
//***	0.1		29.11.00	RRO		optimizations
//***   0.2		07.12.00	RRO		now you've got only one bmp
//***
//**********************************************************
void CUGanttCtrl::DrawGanttBmp(CDC *pdc, bool bSaveFileOption)
{
	if (bmpGantt.m_hObject == NULL)
		return;

	UpdateAllRects ();

	int i			= 0;
	int ilCurrY		= TimeScaleHeaderHeight;
	int ilStartIdx	= 0;
	CRect olFocusRect;
	CRect olRect;

	//how many lines can be displayed in the screen
	if (imMaxVisibleLines < 0)
	{
		imMaxVisibleLines = GetNumberOfVisibleLines ();
	}

	//look for the position of the vertical scrollbar
	if(pomVScroll != NULL)
	{
		ilStartIdx = pomVScroll->GetScrollPos ();
	}

	//MWO: 25.06.03
	DrawBodyBackGround(bmpGantt);
	//END MWO: 25.06.03

	if (bmWithArrows != TRUE)
	{
		//draw line for line the Gantt; 
		//take care: if there are no arrows, you can draw line for line first the background bars, then
		//			 the bars. else you have to draw first ALL background bars, then ALL arrows and at
		//			 least ALL bars
		for (i = ilStartIdx; ((i < TabLines) && (i < ilStartIdx + imMaxVisibleLines)); i++)
		{
			DrawBkBarsOfLine (bmpGantt, i, ilCurrY);
			DrawBarsOfLine	 (bmpGantt, i, ilCurrY);
			ilCurrY += omTabLineHeight.GetAt (i);
		}
	}
	else
	{
		//draw line for line the background bars on the bmp
		for (i = ilStartIdx; ((i < TabLines) && (i < ilStartIdx + imMaxVisibleLines)); i++)
		{
			DrawBkBarsOfLine (bmpGantt, i, ilCurrY);
			ilCurrY += omTabLineHeight.GetAt (i);
		}

		//draw the arrows on the bmp; so they will be under the bars and over the bkbars
		DrawArrows (bmpGantt);

		//reinit
		ilCurrY	= TimeScaleHeaderHeight;
		if(pomVScroll != NULL)
		{
			ilStartIdx = pomVScroll->GetScrollPos();
		}

		//draw line for line the bars on the bmp
		for (i = ilStartIdx; ((i < TabLines) && (i < ilStartIdx + imMaxVisibleLines)); i++)
		{
			DrawBarsOfLine	 (bmpGantt, i, ilCurrY);
			ilCurrY += omTabLineHeight.GetAt (i);
		}
	}

	//calculate the bitmap dimensions
	int x	= omGanttRect.left;
	int y	= omGanttRect.top;
	int dx	= omGanttRect.right  - x;
	int dy	= omGanttRect.bottom - y;

	if (pdc==NULL)
	{
		CClientDC dc (this);
		pdc = &dc;
	}
	CDC memDC;

	memDC.CreateCompatibleDC (pdc);
	memDC.SelectObject (&bmpGantt);

	// Draw rest background between last row and horizontal scroller in dark gray
	CRect olRestRect = omGanttRect;
	if(m_lifeStyle == FALSE) //MWO: 25.06.03
	{
		if (ilCurrY < olRestRect.bottom)
		{
			CBrush olBackgroundBrush (omBackgroundColor);
			olRestRect.top = ilCurrY;
			memDC.FillRect (&olRestRect, &olBackgroundBrush);
		}
	}
	if (ilCurrY < olRestRect.bottom)
	{
		//look if you have to draw the red marker of the actual time
		// rro: we needn't that, it's ok to update the time-line every minute with the timer-event
		// omCurrentTime = GetCurrentTime();
		// pomTimeScale->UpdateCurrentTimeLine (omCurrentTime);
		int ilCurrTimeX = omGanttRect.left + pomTimeScale->GetXFromTime(omCurrentTime);
		if (IsBetween (ilCurrTimeX, omGanttRect.left, omGanttRect.right))
		{
			CPen *pOldPen,
				  olRedP (PS_SOLID, 1, COLORREF (RGB (255,0,0)));
			pOldPen = memDC.SelectObject (&olRedP);
			memDC.MoveTo (ilCurrTimeX, olRestRect.top);
			memDC.LineTo (ilCurrTimeX, olRestRect.bottom);
			memDC.SelectObject (pOldPen);
		}
	}
	//you have finished the bitmap ;o))
	//now move it to the real screen
	pdc->BitBlt ( x, y, dx, dy, &memDC, x, y, SRCCOPY);

	if (bSaveFileOption!=true)
	{
		// Draw a focus rect for the Found Bar (if there is one)
		if ((omBarMode != "NORMAL") && (bmFoundBar != FALSE))
		{
			//copy the bmpBackgroundAndBkBarsAndBars-bitmap on the REAL screen
			//memDC.SelectObject (&bmpGantt);
			//dc.BitBlt ( x, y, dx, dy, &memDC, x, y, SRCCOPY);
			olFocusRect	= omCurrBarRect;
			if (olFocusRect.left < omSplitterRect.right)	//don't draw over the splitter and the tab...!
			{
				olFocusRect.left = omSplitterRect.right;
			}
			olFocusRect.top		 = omGanttRect.top - 1;
			olFocusRect.bottom	 = omGanttRect.bottom;

			//draw the big FocusRect 
			pdc->DrawFocusRect (olFocusRect);

			//draw the small FocusRect
			olFocusRect.left  += 1;
			olFocusRect.right -= 1;
			olFocusRect.top	   = omCurrBarRect.top - 1;
			olFocusRect.bottom = omCurrBarRect.bottom;
			pdc->DrawFocusRect (olFocusRect);
		}

		// Draw a focus rect for the Found Background Bar (if there is one)
		if ((omBarMode != "NORMAL") && (bmFoundBkBar != FALSE))
		{
			//copy the bmpBackgroundAndBkBarsAndBars-bitmap on the REAL screen
			//memDC.SelectObject (&bmpGantt);
			//dc.BitBlt ( x, y, dx, dy, &memDC, x, y, SRCCOPY);
			olFocusRect.left   = omGanttRect.left + pomTimeScale->GetXFromTime (omLines[imCurrBarLine].omBackBars[imCurrBarPos].Begin);
			olFocusRect.right  = omGanttRect.left + pomTimeScale->GetXFromTime (omLines[imCurrBarLine].omBackBars[imCurrBarPos].End);
			olFocusRect.top		 = omGanttRect.top - 1;
			olFocusRect.bottom = omGanttRect.bottom;
			if (olFocusRect.left < omSplitterRect.right)	//don't draw over the splitter and the tab...!
			{
				olFocusRect.left = omSplitterRect.right;
			}

			//draw the big FocusRect 
			pdc->DrawFocusRect (olFocusRect);

			//draw the small FocusRect
			olFocusRect.left  += 1;
			olFocusRect.right -= 1;
			olFocusRect.top	   = imRefreshY;
			olFocusRect.bottom = olFocusRect.top + omTabLineHeight.GetAt (imCurrBarLine);
			pdc->DrawFocusRect (olFocusRect);
		}
	}
}

//**********************************************************
//*** 
//***
//*** created from: RRO				date: 04.12.00
//***
//*** changes:
//*** vers.		date		who		what
//***	0.1		07.12.00	RRO		forbid drawing the focus rects
//***								left of the gantt area
//***
//**********************************************************
void CUGanttCtrl::DrawGanttLineBmp()
{
	CClientDC dc (this);
	CDC memDC;
	memDC.CreateCompatibleDC (&dc);

	int x	= omGanttRect.left;
	int y	= omGanttRect.top;
	int dx	= omGanttRect.right  - x;
	int dy	= omGanttRect.bottom - y;
	CRect olFocusRect;

	CRect olRect;
	GetClientRect(&olRect);

	long llHeight	  = omTabLineHeight.GetAt (imCurrBarLine);
	long llWidth	  = olRect.Width ();

	//draw the bitmaps
	DrawBkBarsOfLine (bmpGantt, imCurrBarLine, imRefreshY);
	DrawBarsOfLine	 (bmpGantt,	imCurrBarLine, imRefreshY);

	//now bring the small bmpBackgroundAndBkBarAndBarLine on the big bmpBackgroundAndBkBarsAndBars
	memDC.SelectObject (&bmpGantt);
	dc.BitBlt (x, y, llWidth, dy, &memDC, x, y, SRCCOPY);

// Draw a focus rect for the Found Bar (if there is one)
	if ((omBarMode != "NORMAL") && (bmFoundBar != FALSE))
	{
		//copy the bmpBackgroundAndBkBarsAndBars-bitmap on the REAL screen
		//memDC.SelectObject (&bmpGantt);
		//dc.BitBlt ( x, y, dx, dy, &memDC, x, y, SRCCOPY);
		olFocusRect	= omCurrBarRect;
		if (olFocusRect.left < omSplitterRect.right)	//don't draw over the splitter and the tab...!
		{
			olFocusRect.left = omSplitterRect.right;
		}
		olFocusRect.top		 = omGanttRect.top - 1;
		olFocusRect.bottom	 = omGanttRect.bottom;

		//draw the big FocusRect 
		dc.DrawFocusRect (olFocusRect);

		//draw the small FocusRect
		olFocusRect.left  += 1;
		olFocusRect.right -= 1;
		olFocusRect.top	   = omCurrBarRect.top - 1;
		olFocusRect.bottom = omCurrBarRect.bottom;
		dc.DrawFocusRect (olFocusRect);
	}

// Draw a focus rect for the Found Background Bar (if there is one)
	if ((omBarMode != "NORMAL") && (bmFoundBkBar != FALSE))
	{
		//copy the bmpBackgroundAndBkBarsAndBars-bitmap on the REAL screen
		//memDC.SelectObject (&bmpGantt);
		//dc.BitBlt ( x, y, dx, dy, &memDC, x, y, SRCCOPY);
		olFocusRect.left   = omGanttRect.left + pomTimeScale->GetXFromTime (omLines[imCurrBarLine].omBackBars[imCurrBarPos].Begin);
		olFocusRect.right  = omGanttRect.left + pomTimeScale->GetXFromTime (omLines[imCurrBarLine].omBackBars[imCurrBarPos].End);
		olFocusRect.top		 = omGanttRect.top - 1;
		olFocusRect.bottom = omGanttRect.bottom;
		if (olFocusRect.left < omSplitterRect.right)	//don't draw over the splitter and the tab...!
		{
			olFocusRect.left = omSplitterRect.right;
		}

		//draw the big FocusRect 
		dc.DrawFocusRect (olFocusRect);

		//draw the small FocusRect
		olFocusRect.left  += 1;
		olFocusRect.right -= 1;
		olFocusRect.top	   = imRefreshY;
		olFocusRect.bottom = olFocusRect.top + omTabLineHeight.GetAt (imCurrBarLine);
		dc.DrawFocusRect (olFocusRect);
	}
}

//**********************************************************
//*** sets the splitter
//***
//*** created from: MWO				date: ???
//***
//*** changes:
//*** vers.		date		who		what
//***
//**********************************************************
long CUGanttCtrl::GetSplitterPosition() 
{
	return SplitterPosition;
}
void CUGanttCtrl::SetSplitterPosition(long nNewValue) 
{
	SplitterPosition = nNewValue;
	SetModifiedFlag();
}

//**********************************************************
//*** with or without the 2nd vertical scroller
//***
//*** created from: RRO				date: 28.11.00
//***
//*** changes:
//*** vers.		date		who		what
//***
//**********************************************************
BOOL CUGanttCtrl::GetWith2ndScroller() 
{
	return With2ndScroller;
}
void CUGanttCtrl::SetWith2ndScroller(BOOL bNewValue) 
{
	With2ndScroller = bNewValue;
	Handle2ndScroller();
	SetModifiedFlag();
}

//**********************************************************
//*** with or without the horizontal scrollbar
//***
//*** created from: RRO				date: 06.11.00
//***
//*** changes:
//*** vers.		date		who		what
//***
//**********************************************************
BOOL CUGanttCtrl::GetWithHorizontalScroller() 
{
	return WithHorizontalScroller;
}
void CUGanttCtrl::SetWithHorizontalScroller(BOOL bNewValue) 
{
	WithHorizontalScroller = bNewValue;
	HandleHorizontalScroller();
	SetModifiedFlag();
}

void CUGanttCtrl::OnTabMouseMove(UINT nFlags, CPoint point)
{
  IntSetCursor (LoadCursor (NULL, IDC_ARROW));
	switch(nFlags)
	{
	case MK_LBUTTON:
		if(omSplitterRect.PtInRect(point) == FALSE && isSplitterUpdating == false)
		{
			IntSetCursor (LoadCursor (NULL, IDC_ARROW));
		}
		else
		{
  	  HCURSOR hCurs = LoadCursor(NULL, IDC_SIZEWE);
		  IntSetCursor(hCurs);
			int ilSplitW = omSplitterRect.right - omSplitterRect.left;
			int ilXOffset = point.x - omSplitterRect.left;
			isSplitterUpdating = true;

			SplitterPosition = point.x - imCurrSplitterOffset;
			CalcAreaRects();
		}
		break;
	case MK_RBUTTON:
		break;
	case MK_SHIFT:
		break;
	case MK_CONTROL:
		break;
	default:
		break;
	}
}

void CUGanttCtrl::OnMouseMove(UINT nFlags, CPoint point) 
{
	if (GetCursor() != m_nCursor)
	{
		SetCursor(m_nCursor);
	}

	omTolerantRect = omSplitterRect;
	omTolerantRect.InflateRect(2, 0, 2, 0);
	CRect olClientRect;
	GetClientRect (&olClientRect);
	
	if (omTolerantRect.PtInRect (point) == TRUE)		// over the Splitter
	{
		IntSetCursor (LoadCursor (NULL, IDC_SIZEWE));
		HighlightLine (point);
	}
	else if (omGanttRect.PtInRect (point) == TRUE)		// over the Gantt
	{
		OnMouseMoveGantt (nFlags, point);	
	}
	//else if (omTotalTabRect.PtInRect (point) == TRUE)	//over the Tab
	//{
	//	OnTabMouseMove (nFlags, point);
	//}
	else
	{
		IntSetCursor (LoadCursor (NULL, IDC_ARROW));
		HighlightLine (point);
		
		//MWO TO DO: handle current column
		/*		bool blDetectedCol = false;
		int ilWCount = 0;
		if(point.x > omCurrentTabRect.right)
		{
			for(int i = 0; i < omTabHeaderLen.GetSize(); i++)
			{
				ilWCount += (int)omTabHeaderLen[i]; 
				if(point.x < ilWCount)
			{
			ilCol = i;
			i = omTabHeaderLen.GetSize();
			blDetectedCol = true;
		}*/
	}

	switch(nFlags)
	{
	case MK_LBUTTON:
		if(omSplitterRect.PtInRect (point) == FALSE && isSplitterUpdating == false)
		{
			IntSetCursor (LoadCursor (NULL, IDC_ARROW));
		}
		else
		{
			if(m_splitterMovable == TRUE)
			{
				IntSetCursor (LoadCursor (NULL, IDC_SIZEWE));
				int ilSplitW  = omSplitterRect.right - omSplitterRect.left;
				int ilXOffset = point.x - omSplitterRect.left;
				isSplitterUpdating = true;
				
				SplitterPosition = point.x - imCurrSplitterOffset;
				if (SplitterPosition < 1)
					SplitterPosition = 1;
				if (SplitterPosition > (olClientRect.right - 50))
					SplitterPosition = (olClientRect.right - 50);
				CalcAreaRects();
				
				InvalidateControl();
			}
			return;
		}
		break;
	case MK_RBUTTON:
		break;
	case MK_SHIFT:
		break;
	case MK_CONTROL:
		break;
	default:
		break;
	}
	//	COleControl::OnMouseMove(nFlags, point);
}


//**********************************************************
//*** just return true for a better handle of your own cursors;
//*** else there will be a flickering cursur while moving your
//*** mouse... 
//***
//*** created from: RRO				date: 26.08.00
//***
//*** changes:
//*** vers.		date		who		what
//***	0.1		28.11.00	RRO		test the runtimeclass of CWnd,
//***								to set the cursor over the scroller
//***
//**********************************************************
BOOL CUGanttCtrl::OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message) 
{
	if (pWnd->IsKindOf (RUNTIME_CLASS (CScrollBar)))
	{
		IntSetCursor (LoadCursor (NULL, IDC_ARROW));
	}
	return TRUE;	
	//return COleControl::OnSetCursor(pWnd, nHitTest, message);
}

//**********************************************************
//*** handles the LButtonDown-event of the control
//*** looks for the area of the click and 
//*** "leads the events" to the specialized method
//***
//***
//*** created from: RRO				date: 26.08.00
//***
//*** changes:
//*** vers.		date		who		what
//***	0.1		20.12.00	RRO		forward tab-events
//***
//**********************************************************
void CUGanttCtrl::OnLButtonDown(UINT nFlags, CPoint point) 
{
	SetCapture();

	//was the click in the Gantt-area?
	if (omGanttRect.PtInRect(point) == TRUE)
	{
		OnLButtonDownGantt (nFlags, point);	
	}
	//was the click in the Tab-area?
	else if (omTotalTabRect.PtInRect(point) == TRUE)
	{
		OnLButtonDownTab (nFlags, point);
	}
	else
	{
		switch(nFlags)
		{
		case MK_LBUTTON:
			if(/*omSplitterRect*/omTolerantRect.PtInRect(point) == FALSE)
			{
				IntSetCursor(LoadCursor(NULL, IDC_ARROW));
			}
			else
			{
			  IntSetCursor(LoadCursor(NULL, IDC_SIZEWE));
				imCurrSplitterOffset = point.x - omSplitterRect.left;
			}
			break;
		default:
			break;
		}
	}

/*	RRO: the following was too slow (reason=???)
	was the click in the splitter-area?
	else if (omSplitterRect.PtInRect(point) == TRUE)
	{
		OnLButtonDownSplitter (nFlags, point);
	}
*/

	//COleControl::OnLButtonDown(nFlags, point);
}

//**********************************************************
//*** handles the RButtonDown-event of the control
//*** looks for the area of the click and 
//*** "leads the events" to the specialized method
//***
//*** created from: RRO				date: 27.11.00
//***
//*** changes:
//*** vers.		date		who		what
//***	0.1		20.12.00	RRO		forward tab-events
//***
//**********************************************************
void CUGanttCtrl::OnRButtonDown(UINT nFlags, CPoint point) 
{
	//was the click in the Gantt-area?
	if (omGanttRect.PtInRect(point) == TRUE)
	{
		OnRButtonDownGantt (nFlags, point);	
	}
	//was the click in the Tab-area?
	else if (omTotalTabRect.PtInRect(point) == TRUE)
	{
		OnRButtonDownTab (nFlags, point);
	}
	//COleControl::OnRButtonDown(nFlags, point);
}

//**********************************************************
//*** handles the RButtonDown-event of the control
//*** looks for the area of the click and 
//*** "leads the events" to the specialized method
//***
//***
//*** created from: RRO				date: 27.11.00
//***
//*** changes:
//*** vers.		date		who		what
//***
//**********************************************************
void CUGanttCtrl::OnRButtonDownGantt(UINT nFlags, CPoint point)
{
	BOOL blFoundBar		= FALSE;
	BOOL blFoundBkBar	= FALSE;

	LineData *polLine	= NULL;
	int ilBarHeight		= (int)(TabLineHeight/(m_subbarStackCount+1)) - 2;
	int ilTopOfLine		= GetTopOfLine (imCurrentHighlightLine);
	int ilTopOfBar		= 0;
	int ilFlags			= 0;

	//prepare ilFlags
	if (nFlags & MK_CONTROL)
		ilFlags += MK_CONTROL;
	if (nFlags & MK_LBUTTON)
		ilFlags += MK_LBUTTON;
	if (nFlags & MK_MBUTTON)
		ilFlags += MK_MBUTTON;
	if (nFlags & MK_RBUTTON)
		ilFlags += MK_RBUTTON;
	if (nFlags & MK_SHIFT)
		ilFlags += MK_SHIFT;

	if ((imCurrentHighlightLine > -1) && (imCurrentHighlightLine < omLines.GetSize()))
		polLine = &omLines[imCurrentHighlightLine];

	if ((nFlags & MK_RBUTTON) && (polLine != NULL))
	{
		//look first, if the click was on a bar
		if ((imCurrentHighlightLine > -1) && (imCurrentHighlightLine < omLines.GetSize()))
		{
			int i = polLine->omBars.GetSize() - 1;
			while ((i >= 0) && (blFoundBar != TRUE))
			{
				if (GetTimeFromX (point.x) > polLine->omBars[i].Begin)
				{
					if (GetTimeFromX (point.x) < polLine->omBars[i].End)
					{
						//now exclude other bars of an overlapping
						ilTopOfBar = ilTopOfLine + (BarOverlapOffset * polLine->omBars[i].OverlapLevel) + 2;
						if ((point.y > ilTopOfBar) && (point.y < (ilTopOfBar + ilBarHeight)))
						//if ((point.y > ilTopOfBar) && (point.y < (ilTopOfBar + TabLineHeight - 2)))
						{
							blFoundBar = TRUE;
							FireOnRButtonDownBar (polLine->omBars[i].Key, ilFlags);
						}
					}
				}
				i--;
			}
			if (blFoundBar != TRUE)
			{
				// let's look for a sub-bar...
				BarData* polSubBar = GetSubBarByMousePosition (point);
				if (polSubBar != NULL)
				{
					FireOnRButtonDownSubBar (_bstr_t (LPCTSTR (polSubBar->MainbarKey)), _bstr_t (LPCTSTR (polSubBar->Key)), ilFlags);
				}
				else
				{
					i = polLine->omBackBars.GetSize() - 1;
					while ((i >= 0) && (blFoundBkBar != TRUE))
					{
						if (GetTimeFromX (point.x) > polLine->omBackBars[i].Begin)
						{
							if (GetTimeFromX(point.x) < polLine->omBackBars[i].End)
							{
								blFoundBkBar	= TRUE;
								FireOnRButtonDownBkBar (polLine->omBackBars[i].Key, ilFlags);
							}
						}
						i--;
					}
					if (blFoundBkBar != TRUE)
					{
						FireOnRButtonDownBkGantt ();
					}
				}
			}
		}
	}
}

//**********************************************************
//*** handles the RButtonDown-event in the Tab-area
//***
//*** created from: RRO				date: 20.12.00
//***
//*** changes:
//*** vers.		date		who		what
//***
//**********************************************************
void CUGanttCtrl::OnRButtonDownTab(UINT nFlags, CPoint point)
{
	short slColumn	= 0;
	int	  ilFlags	= 0;
	int   ilLeft	= 0;
	long  llRow		= imCurrentHighlightLine;

	//prepare ilFlags
	if (nFlags & MK_CONTROL)
		ilFlags += MK_CONTROL;
	if (nFlags & MK_LBUTTON)
		ilFlags += MK_LBUTTON;
	if (nFlags & MK_MBUTTON)
		ilFlags += MK_MBUTTON;
	if (nFlags & MK_RBUTTON)
		ilFlags += MK_RBUTTON;
	if (nFlags & MK_SHIFT)
		ilFlags += MK_SHIFT;

	//look, if the click was below table-area
	if (imCurrentHighlightLine >= omLines.GetSize())
	{
		slColumn = -1;
		llRow	 = -1;
	}
	else
	{
		//the click was on the top line of the table
		if (point.y < TimeScaleHeaderHeight)
		{
			llRow = 0;
		}
		//look for the column
		while (ilLeft < point.x)
		{
			ilLeft += omTabHeaderLen[slColumn];
			slColumn++;
		}
	}
	//now fire the column, the row and the nFlags
	FireOnRButtonDownTab (llRow, slColumn, ilFlags);
}

//**********************************************************
//*** handles the LButtonUp-event of the control
//***
//*** created from: MWO				date: ???
//***
//*** changes:
//*** date		who		what
//*** 27.11.00	rro		all
//*** 26.08.02	rro		- checking overlap-level of old bar-time
//***					- tidy up
//***
//**********************************************************
void CUGanttCtrl::OnLButtonUp(UINT nFlags, CPoint point) 
{
	imCurrSplitterOffset = -1;
	isSplitterUpdating = false;
	omTolerantRect.InflateRect (-4, 0);

	if ((imCurrBarLine > -1) && (omLines.GetSize() > 0))
	{
		LineData *polLine = &omLines[imCurrBarLine];

		// handle the overlap-level of the bar
		HandleOverlapLevelOfBar (omCurrBarKey, TRUE);

		if (omBarMode == "MOVE" || omBarMode == "SIZE")
		{
			if (bmFoundBar == TRUE || bmFoundBkBar == TRUE)
			{
				// handle the old times of the bar
				HandleOverlapLevelOfLineAndTime (imCurrBarLine, m_dMoveBarOldBegin , m_dMoveBarOldEnd);

				// fire the event to the container
				FireEndOfSizeOrMove(polLine->omBars[imCurrBarPos].Key, imCurrBarLine);
			}
		}
		HandleOverlapLevelOfLine (imCurrBarLine); //looking after the max. overlap-offset
		HandleLineHight (imCurrBarLine); // adapting line-height to the max. overlap-offset
	}

	omBarMode = "NORMAL";

	// Stop auto scrolling:
	m_AutoScrollDirection = SCROLL_NOSCROLL;

	IntSetCursor(LoadCursor(NULL, IDC_ARROW));

	ReleaseCapture();

	//COleControl::OnLButtonUp(nFlags, point);
}

BOOL CUGanttCtrl::OnEraseBkgnd(CDC* pDC) 
{
	BOOL blRet = COleControl::OnEraseBkgnd(pDC);
	return blRet;
}

long CUGanttCtrl::GetTimeScaleDuration() 
{
	return imTimeScaleDuration;
}

void CUGanttCtrl::SetTimeScaleDuration(long nNewValue) 
{
	imTimeScaleDuration = nNewValue;

	SetModifiedFlag();
}

//**********************************************************
//*** handles the VScroll-event
//***
//*** created from: MWO				date: ???
//***
//*** changes:
//*** vers.		date		who		what
//***	0.1		28.11.00	RRO		the 2nd scroller moved with delay
//***								and tidy up
//***
//**********************************************************
void CUGanttCtrl::OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar) 
{
	int ilMax, ilMin;
	pomVScroll->GetScrollRange (&ilMin, &ilMax);

	switch(nSBCode)
	{
		case SB_BOTTOM:			//Scroll to bottom
			break;
		case SB_ENDSCROLL:		//End scroll
			break;
		case SB_LINEDOWN:		//Scroll one line down
			{
				if (pomVScroll->GetScrollPos() == ilMax)
				{
					break;
				}
				else
				{
					pomVScroll->SetScrollPos (pomVScroll->GetScrollPos() + 1);
				}
			}
			break;
		case SB_LINEUP:			//Scroll one line up
			{
				if (pomVScroll->GetScrollPos() == 0)
				{
					break;
				}
				else
				{
					pomVScroll->SetScrollPos (pomVScroll->GetScrollPos() - 1);
				}
			}
			break;
		case SB_PAGEDOWN:		//Scroll one page down
			pomVScroll->SetScrollPos (pomVScroll->GetScrollPos() + imMaxVisibleLines - 2);
			break;
		case SB_PAGEUP:			//Scroll one page up
			pomVScroll->SetScrollPos (pomVScroll->GetScrollPos() - imMaxVisibleLines - 2);
			break;
		case SB_TOP:			//Scroll to top
			break;
		case SB_THUMBPOSITION:	//Scroll to the absolute position
			pomVScroll->SetScrollPos ((int)nPos);
			break;
		case SB_THUMBTRACK:		//Drag scroll box to specified position
 			pomVScroll->SetScrollPos ((int)nPos);
			::SetCapture(pomVScroll->m_hWnd ); 
			break;
		default:
			ReleaseCapture();
			break;

	}

	if(With2ndScroller != FALSE)
	{
		if(pomTabVScroll != NULL)
		{
			pomTabVScroll->SetScrollPos (pomVScroll->GetScrollPos());
		}
	}
	if(bmFireHScrollEvent == true)
	{
		FireOnHScroll(pomVScroll->GetScrollPos());
	}

	imMaxVisibleLines = GetNumberOfVisibleLines ();
	if (m_autoSizeLineHeightToFit)
	{
		//we have to handle the line-hights
		HandleOverlapLevelOfVisibleLines();
	}
	pomTimeScale->Invalidate();
	InvalidateControl();

	COleControl::OnVScroll(nSBCode, nPos, pScrollBar);
}

//**********************************************************
//*** just look after doing a refresh, therefore it's 
//*** important to calc the "imMaxVisibleLines" and
//*** the AreaRects
//***
//*** created from: RRO				date: 20.11.00
//***
//*** changes:
//*** vers.		date		who		what
//***	0.1		07.12.00	RRO		adapting the size of the
//***								gantt-bitmap
//***
//**********************************************************
void CUGanttCtrl::OnSize(UINT nType, int cx, int cy) 
{
	CalcAreaRects();
	imMaxVisibleLines = GetNumberOfVisibleLines ();

	//handle the bitmap
	if (bmpGantt.m_hObject != NULL)
		bmpGantt.DeleteObject();
	CClientDC dc (this);
	CDC dcMem;
	dcMem.CreateCompatibleDC (&dc);
	bmpGantt.CreateCompatibleBitmap	(&dc, omGanttRect.right, omGanttRect.bottom);
	if (bmpGantt.m_hObject != NULL)
	{
		return;
	}
	dcMem.DeleteDC();

	bmDrawALL = TRUE;
	InvalidateControl();
}

long CUGanttCtrl::GetTabHighlightColor() 
{
	return TabHighlightColor;
}
void CUGanttCtrl::SetTabHighlightColor(long nNewValue) 
{
	TabHighlightColor = nNewValue;
	SetModifiedFlag();
}

void CUGanttCtrl::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar) 
{
	CRect olRect;
	GetClientRect (&olRect);
	CRect olTMRect;

	if(pScrollBar != pomHScroll)
		return;
//---------
//Calculate the number of possible lines
	int ilMax, ilMin;
	int ilTotalMinutes;
	int ilPos = 0;
	pomHScroll->GetScrollRange(&ilMin, &ilMax);

	switch(nSBCode)
	{
		case SB_BOTTOM:
			return;
			//pomVScroll->SetScrollPos(1000);
			break;
		case SB_ENDSCROLL:
			return;
			//pomVScroll->SetScrollPos(1000);
			break;
		case SB_LINEDOWN:
			{
				COleDateTimeSpan olTS;
				olTS = TimeFrameTo - TimeFrameFrom;//imTimeScaleDuration*60;
				ilTotalMinutes = (int)olTS.GetTotalMinutes() - imTimeScaleDuration*60;
				ilPos = pomHScroll->GetScrollPos() + (int)(60*1000/ilTotalMinutes);
				if(ilPos >= 1000)
				{
					ilPos = 1000;
					omStartTime = omStartTime + COleDateTimeSpan(0,0,ilTotalMinutes,0);//TimeFrameFrom;
				}
				else 
				{
					omStartTime = omStartTime + COleDateTimeSpan(0,0,60,0);
				}
				if(omStartTime > TimeFrameTo)
				{
					omStartTime = TimeFrameTo - COleDateTimeSpan(0,0,imTimeScaleDuration*60,0);
				}
				if(omStartTime < TimeFrameFrom)
				{
					omStartTime = TimeFrameFrom;
				}
				pomTimeScale->SetDisplayStartTime(omStartTime);
				pomHScroll->SetScrollPos(ilPos,TRUE);
				
			}
			break;
		case SB_LINEUP:
			{
				COleDateTimeSpan olTS;
				olTS = TimeFrameTo - TimeFrameFrom;//imTimeScaleDuration*60;
				ilTotalMinutes = (int)olTS.GetTotalMinutes() - imTimeScaleDuration*60;
				ilPos = pomHScroll->GetScrollPos() - (int)(60*1000/ilTotalMinutes);
				if(ilPos <= 0)
				{
					ilPos = 0;
					omStartTime = TimeFrameFrom;
				}
				else
				{
					omStartTime = omStartTime - COleDateTimeSpan(0,0,60,0);
				}
				if(omStartTime > TimeFrameTo)
				{
					omStartTime = TimeFrameTo - COleDateTimeSpan(0,0,ilTotalMinutes,0);
				}
				if(omStartTime < TimeFrameFrom)
				{
					omStartTime = TimeFrameFrom;
				}
				pomTimeScale->SetDisplayStartTime(omStartTime);
				pomHScroll->SetScrollPos(ilPos,TRUE);
			}
			break;
		case SB_PAGEDOWN:
			{
				COleDateTimeSpan olTS;
				olTS = TimeFrameTo - TimeFrameFrom;//imTimeScaleDuration*60;
				ilTotalMinutes = (int)olTS.GetTotalMinutes() - imTimeScaleDuration*60;
				ilPos = pomHScroll->GetScrollPos() +
						(int)((((imTimeScaleDuration*60)/2)*1000)/ilTotalMinutes);
				if(ilPos >= 1000)
				{
					ilPos = 1000; 
					omStartTime = TimeFrameTo/*omStartTime */- COleDateTimeSpan(0,0,imTimeScaleDuration*60/*ilTotalMinutes*/,0);//TimeFrameFrom;
				}
				else
				{
					omStartTime = omStartTime + COleDateTimeSpan(0,0,(int)((imTimeScaleDuration*60)/2),0);
				}
				if(omStartTime > TimeFrameTo)
				{
					omStartTime = TimeFrameTo - COleDateTimeSpan(0,0,ilTotalMinutes,0);
				}
				if(omStartTime < TimeFrameFrom)
				{
					omStartTime = TimeFrameFrom;
				}
				pomTimeScale->SetDisplayStartTime(omStartTime);
				pomHScroll->SetScrollPos(ilPos,TRUE);
			}
			break;
		case SB_PAGEUP:
			{
				COleDateTimeSpan olTS;
				olTS = TimeFrameTo - TimeFrameFrom;//imTimeScaleDuration*60;
				ilTotalMinutes = (int)olTS.GetTotalMinutes() - imTimeScaleDuration*60;
				ilPos = pomHScroll->GetScrollPos() - 
						(int)((((imTimeScaleDuration*60)/2)*1000)/ilTotalMinutes);
				if(ilPos <= 0)
				{
					ilPos = 0;
					omStartTime = TimeFrameFrom;
				}
				else
				{
					omStartTime = omStartTime - COleDateTimeSpan(0,0,(int)((imTimeScaleDuration*60)/2),0);
				}
				if(omStartTime > TimeFrameTo)
				{
					omStartTime = TimeFrameTo - COleDateTimeSpan(0,0,ilTotalMinutes,0);
				}
				if(omStartTime < TimeFrameFrom)
				{
					omStartTime = TimeFrameFrom;
				}
				pomTimeScale->SetDisplayStartTime(omStartTime);
				pomHScroll->SetScrollPos(ilPos,TRUE);
			}
			break;
		case SB_TOP:
			return;
			break;
		case SB_THUMBPOSITION:
			break; 
		case SB_THUMBTRACK:
			{
				COleDateTimeSpan olTS;
				olTS = TimeFrameTo - TimeFrameFrom;//imTimeScaleDuration*60;
				ilTotalMinutes = (int)olTS.GetTotalMinutes() - imTimeScaleDuration*60;
				omStartTime = TimeFrameFrom + COleDateTimeSpan(0,0,(int)((ilTotalMinutes*nPos/1000)),0);
				if(omStartTime > TimeFrameTo)
				{
					omStartTime = TimeFrameTo - COleDateTimeSpan(0,0,ilTotalMinutes,0);
				}
				if(omStartTime < TimeFrameFrom)
				{
					omStartTime = TimeFrameFrom;
				}
				pomTimeScale->SetDisplayStartTime(omStartTime);
				pomHScroll->SetScrollPos(nPos,TRUE);
				::SetCapture(pomHScroll->m_hWnd ); 
			}
			break;
		default:
			ReleaseCapture();
	}

	if (m_autoSizeLineHeightToFit)
	{
		//we have to handle the line-hights
		HandleOverlapLevelOfVisibleLines();
	}

	pomTimeScale->Invalidate();
	InvalidateControl ();

	FireActualTimescaleDate (pomTimeScale->GetDisplayStartTime());
//	COleControl::OnHScroll (nSBCode, nPos, pScrollBar);
}

void CUGanttCtrl::MakeSampleData()
{
	int ilCurrKey = 0;
	int ilCurrBarKey = 0;
	int ilFromOffset = 30;
	int ilToOffset = 120;
	int ilBkBarLen = 480; // = 8 hours
	int ilBarLen = 120;
	COleDateTime olOrigTime, olBarFrom, olBarTo, olBkFrom, olBkTo;
	olOrigTime = COleDateTime::GetCurrentTime();
	olOrigTime = COleDateTime(olOrigTime.GetYear(), olOrigTime.GetMonth(), olOrigTime.GetDay(),
							  0, 0, 0);
	LineData *polLine;
	BarData  *polBkBar;
	BarData *polBar;
	for(int i = 0; i < 30; i++)
	{
		polLine = new LineData();
		polLine->TabValues = CString("Joe,Smith");
		polLine->Key = NumberToCString(++ilCurrKey);
		//BkBars
		polBkBar = new BarData();
		olBkFrom = olOrigTime + COleDateTimeSpan(0, 0, ilFromOffset, 0);
		olBkTo = olOrigTime + COleDateTimeSpan(0, 0, ilBkBarLen-120, 0);
		polBkBar->Begin = olBkFrom;
		polBkBar->End   = olBkTo;
		polBkBar->LineKey = polLine->Key;
		polBkBar->Key = NumberToCString(++ilCurrBarKey);
		polLine->omBackBars.Add(polBkBar);
		polBkBar = new BarData();
		olBkFrom = olOrigTime + COleDateTimeSpan(0, 0, ilFromOffset + ilBkBarLen + 120, 0);
		olBkTo = olBkFrom + COleDateTimeSpan(0, 0, ilBkBarLen-300, 0);
		polBkBar->Begin = olBkFrom;
		polBkBar->End   = olBkTo;
		polBkBar->LineKey = polLine->Key;
		polBkBar->Key = NumberToCString(++ilCurrBarKey);
		polLine->omBackBars.Add(polBkBar);
		//Bars
		polBar = new BarData();
		olBarFrom = olOrigTime + COleDateTimeSpan(0, 0, 15, 0);
		olBarTo = olOrigTime + COleDateTimeSpan(0, 0, ilBarLen-30, 0);
		polBar->LineKey		= polLine->Key;
		polBar->Key			= NumberToCString(++ilCurrBarKey);
		polBar->Begin		= olBarFrom;
		polBar->End			= olBarTo;
		polBar->BarText		= "Task " + NumberToCString(ilCurrBarKey);;
		polBar->BackColor	= COLORREF(RGB(0,255,0));
		polLine->omBars.Add(polBar);
		polBar = new BarData();
		olBarFrom = olOrigTime + COleDateTimeSpan(0, 3, 30, 0);
		olBarTo = olBarFrom + COleDateTimeSpan(0, 0, ilBarLen, 0);
		polBar->LineKey		= polLine->Key;
		polBar->Key			= NumberToCString(++ilCurrBarKey);
		polBar->Begin		= olBarFrom;
		polBar->End			= olBarTo;
		polBar->BarText		= "Task " + NumberToCString(ilCurrBarKey);
		polBar->BackColor	= COLORREF(RGB(255,0,0));
		polLine->omBars.Add(polBar);
		//
		omLines.Add(polLine);

		polLine = new LineData();
		polLine->TabValues = CString("Jim,Miller");
		polLine->Key = NumberToCString(++ilCurrKey);
		polBkBar = new BarData();
		olBkFrom = olOrigTime + COleDateTimeSpan(0, 0, ilFromOffset+30, 0);
		olBkTo   = olBkFrom   + COleDateTimeSpan(0, 0, ilBkBarLen, 0);
		polBkBar->Begin = olBkFrom;
		polBkBar->End   = olBkTo;
		polBkBar->LineKey = polLine->Key;
		polBkBar->Key = NumberToCString(++ilCurrBarKey);
		polLine->omBackBars.Add(polBkBar);
		//
		polBar = new BarData();
		olBarFrom = olOrigTime + COleDateTimeSpan(0, 0, 140, 0);
		olBarTo = olBarFrom + COleDateTimeSpan(0, 0, ilBarLen+50, 0);
		polBar->LineKey		= polLine->Key;
		polBar->Key			= NumberToCString(++ilCurrBarKey);
		polBar->Begin		= olBarFrom;
		polBar->End			= olBarTo;
		polBar->BarText		= "Task  " + NumberToCString(ilCurrBarKey);;
		polBar->TextColor	= COLORREF(RGB(255,255,255));
		polBar->BackColor	= COLORREF(RGB(128,128,128));
		polLine->omBars.Add(polBar);
		polBar = new BarData();
		olBarFrom = olOrigTime + COleDateTimeSpan(0, 6, 10, 0);
		olBarTo = olBarFrom + COleDateTimeSpan(0, 0, ilBarLen-50, 0);
		polBar->LineKey		= polLine->Key;
		polBar->Key			= NumberToCString(++ilCurrBarKey);
		polBar->Begin		= olBarFrom;
		polBar->End			= olBarTo;
		polBar->BarText		= "Task " + NumberToCString(ilCurrBarKey);;
		polBar->BackColor	= COLORREF(RGB(255,255,0));
		polLine->omBars.Add(polBar);
		//
		omLines.Add(polLine);

		polLine = new LineData();
		polLine->TabValues = CString("John,Doe");
		polLine->Key = NumberToCString(++ilCurrKey);
		polBkBar = new BarData();
		olBkFrom = olOrigTime + COleDateTimeSpan(0, 0, ilFromOffset+30, 0);
		olBkTo   = olBkFrom   + COleDateTimeSpan(0, 0, ilBkBarLen-120, 0);
		polBkBar->Begin = olBkFrom;
		polBkBar->End   = olBkTo;
		polBkBar->LineKey = polLine->Key;
		polBkBar->Key = NumberToCString(++ilCurrBarKey);
		polLine->omBackBars.Add(polBkBar);
		//
		polBar = new BarData();
		olBarFrom = olOrigTime + COleDateTimeSpan(0, 0, 90, 0);
		olBarTo = olBarFrom + COleDateTimeSpan(0, 0, ilBarLen+10, 0);
		polBar->LineKey		= polLine->Key;
		polBar->Key			= NumberToCString(++ilCurrBarKey);
		polBar->Begin		= olBarFrom;
		polBar->End			= olBarTo;
		polBar->BarText		= "Task " + NumberToCString(ilCurrBarKey);;
		polBar->TextColor	= COLORREF(RGB(255,255,255));
		polBar->BackColor	= COLORREF(RGB(0,128,128));
		polBar->Shape		= RAISED;
		polLine->omBars.Add(polBar);
		polBar = new BarData();
		olBarFrom = olOrigTime + COleDateTimeSpan(0, 5, 10, 0);
		olBarTo = olBarFrom + COleDateTimeSpan(0, 0, ilBarLen+40, 0);
		polBar->LineKey		= polLine->Key;
		polBar->Key			= NumberToCString(++ilCurrBarKey);
		polBar->Begin		= olBarFrom;
		polBar->End			= olBarTo;
		polBar->BarText		= "Task " + NumberToCString(ilCurrBarKey);;
		polBar->BackColor	= COLORREF(RGB(0,255,255));
		polLine->omBars.Add(polBar);
		//
		omLines.Add(polLine);

		polLine = new LineData();
		polLine->TabValues = CString("Steve,Dowald");
		polLine->Key = NumberToCString(++ilCurrKey);
		polBkBar = new BarData();
		olBkFrom = olOrigTime + COleDateTimeSpan(0, 0, ilFromOffset+60, 0);
		olBkTo   = olBkFrom   + COleDateTimeSpan(0, 0, ilBkBarLen+60, 0);
		polBkBar->Begin = olBkFrom;
		polBkBar->End   = olBkTo;
		polBkBar->LineKey = polLine->Key;
		polBkBar->Key = NumberToCString(++ilCurrBarKey);
		polLine->omBackBars.Add(polBkBar);
		//
		polBar = new BarData();
		olBarFrom = olOrigTime + COleDateTimeSpan(0, 0, 120, 0);
		olBarTo = olBarFrom + COleDateTimeSpan(0, 0, ilBarLen-90, 0);
		polBar->LineKey		= polLine->Key;
		polBar->Key			= NumberToCString(++ilCurrBarKey);
		polBar->Begin		= olBarFrom;
		polBar->End			= olBarTo;
		polBar->BarText		= "Task " + NumberToCString(ilCurrBarKey);;
		polBar->TextColor	= COLORREF(RGB(255,255,255));
		polBar->BackColor	= COLORREF(RGB(128,128,128));
		polLine->omBars.Add(polBar);
		polBar = new BarData();
		olBarFrom = olOrigTime + COleDateTimeSpan(0, 6, 20, 0);
		olBarTo = olBarFrom + COleDateTimeSpan(0, 0, ilBarLen+60, 0);
		polBar->LineKey		= polLine->Key;
		polBar->Key			= NumberToCString(++ilCurrBarKey);
		polBar->Begin		= olBarFrom;
		polBar->End			= olBarTo;
		polBar->BarText		= "Task " + NumberToCString(ilCurrBarKey);;
		polBar->BackColor	= COLORREF(RGB(255,0,0));
		polBar->Shape		= INSET;
		polLine->omBars.Add(polBar);
		//
		omLines.Add(polLine);

		polLine = new LineData();
		polLine->TabValues = CString("Alan,Martin");
		polLine->Key = NumberToCString(++ilCurrKey);
		polBkBar = new BarData();
		olBkFrom = olOrigTime + COleDateTimeSpan(0, 0, ilFromOffset+75, 0);
		olBkTo   = olBkFrom   + COleDateTimeSpan(0, 0, ilBkBarLen+75, 0);
		polBkBar->Begin = olBkFrom;
		polBkBar->End   = olBkTo;
		polBkBar->LineKey = polLine->Key;
		polBkBar->Key = NumberToCString(++ilCurrBarKey);
		polLine->omBackBars.Add(polBkBar);
		//
		polBar = new BarData();
		olBarFrom = olOrigTime + COleDateTimeSpan(0, 0, 160, 0);
		olBarTo = olBarFrom + COleDateTimeSpan(0, 0, ilBarLen+90, 0);
		polBar->LineKey		= polLine->Key;
		polBar->Key			= NumberToCString(++ilCurrBarKey);
		polBar->Begin		= olBarFrom;
		polBar->End			= olBarTo;
		polBar->BarText		= "Task " + NumberToCString(ilCurrBarKey);;
		polBar->TextColor	= COLORREF(RGB(255,255,255));
		polBar->BackColor	= COLORREF(RGB(0,128,0));
		polLine->omBars.Add(polBar);
		polBar = new BarData();
		olBarFrom = olOrigTime + COleDateTimeSpan(0, 6, 50, 0);
		olBarTo = olBarFrom + COleDateTimeSpan(0, 0, ilBarLen, 0);
		polBar->LineKey		= polLine->Key;
		polBar->Key			= NumberToCString(++ilCurrBarKey);
		polBar->Begin		= olBarFrom;
		polBar->End			= olBarTo;
		polBar->BarText		= "Task " + NumberToCString(ilCurrBarKey);;
		polBar->TextColor	= COLORREF(RGB(255,255,255));
		polBar->BackColor	= COLORREF(RGB(0,128,128));
		polLine->omBars.Add(polBar);
		polBar = new BarData();
		olBarFrom = olOrigTime + COleDateTimeSpan(0, 9, 30, 0);
		olBarTo = olBarFrom + COleDateTimeSpan(0, 0, ilBarLen-20, 0);
		polBar->LineKey		= polLine->Key;
		polBar->Key			= NumberToCString(++ilCurrBarKey);
		polBar->Begin		= olBarFrom;
		polBar->End			= olBarTo;
		polBar->BarText		= "Task " + NumberToCString(ilCurrBarKey);;
		polBar->BackColor	= COLORREF(RGB(255,128,128));
		polLine->omBars.Add(polBar);
		//
		omLines.Add(polLine);

		polLine = new LineData();
		polLine->TabValues = CString("Fred,Funkstone");
		polLine->Key = NumberToCString(++ilCurrKey);
		polBkBar = new BarData();
		olBkFrom = olOrigTime + COleDateTimeSpan(0, 0, ilFromOffset+90, 0);
		olBkTo   = olBkFrom   + COleDateTimeSpan(0, 0, ilBkBarLen, 0);
		polBkBar->Begin = olBkFrom;
		polBkBar->End   = olBkTo;
		polBkBar->LineKey = polLine->Key;
		polBkBar->Key = NumberToCString(++ilCurrBarKey);
		polLine->omBackBars.Add(polBkBar);
		//
		polBar = new BarData();
		olBarFrom = olOrigTime + COleDateTimeSpan(0, 0, 210, 0);
		olBarTo = olBarFrom + COleDateTimeSpan(0, 0, ilBarLen-50, 0);
		polBar->LineKey		= polLine->Key;
		polBar->Key			= NumberToCString(++ilCurrBarKey);
		polBar->Begin		= olBarFrom;
		polBar->End			= olBarTo;
		polBar->BarText		= "Task " + NumberToCString(ilCurrBarKey);;
		polBar->Shape		= RAISED;
		polBar->BackColor	= COLORREF(RGB(255,0,0));
		polLine->omBars.Add(polBar);
		polBar = new BarData();
		olBarFrom = olOrigTime + COleDateTimeSpan(0, 7, 0, 0);
		olBarTo = olBarFrom + COleDateTimeSpan(0, 0, ilBarLen+10, 0);
		polBar->LineKey		= polLine->Key;
		polBar->Key			= NumberToCString(++ilCurrBarKey);
		polBar->Begin		= olBarFrom;
		polBar->End			= olBarTo;
		polBar->BarText		= "Task " + NumberToCString(ilCurrBarKey);;
		polBar->Shape		= INSET;
		polBar->BackColor	= COLORREF(RGB(0,255,0));
		polLine->omBars.Add(polBar);
		polBar = new BarData();
		olBarFrom = olOrigTime + COleDateTimeSpan(0, 9, 30, 0);
		olBarTo = olBarFrom + COleDateTimeSpan(0, 0, ilBarLen-90, 0);
		polBar->LineKey		= polLine->Key;
		polBar->Key			= NumberToCString(++ilCurrBarKey);
		polBar->Begin		= olBarFrom;
		polBar->End			= olBarTo;
		polBar->BarText		= "Task " + NumberToCString(ilCurrBarKey);;
		polBar->BackColor	= COLORREF(RGB(0,255,0));
		polLine->omBars.Add(polBar);
		//
		omLines.Add(polLine);

		polLine = new LineData();
		polLine->TabValues = CString("Ron,Fugy");
		polLine->Key = NumberToCString(++ilCurrKey);
		polBkBar = new BarData();
		olBkFrom = olOrigTime + COleDateTimeSpan(0, 0, ilFromOffset+120, 0);
		olBkTo   = olBkFrom   + COleDateTimeSpan(0, 0, ilBkBarLen-60, 0);
		polBkBar->Begin = olBkFrom;
		polBkBar->End   = olBkTo;
		polBkBar->LineKey = polLine->Key;
		polBkBar->Key = NumberToCString(++ilCurrBarKey);
		polLine->omBackBars.Add(polBkBar);
		omLines.Add(polLine);

		polLine = new LineData();
		polLine->TabValues = CString("Clare,Winter");
		polLine->Key = NumberToCString(++ilCurrKey);
		polBkBar = new BarData();
		olBkFrom = olOrigTime + COleDateTimeSpan(0, 0, ilFromOffset+120, 0);
		olBkTo   = olBkFrom   + COleDateTimeSpan(0, 0, ilBkBarLen-30, 0);
		polBkBar->Begin = olBkFrom;
		polBkBar->End   = olBkTo;
		polBkBar->LineKey = polLine->Key;
		polBkBar->Key = NumberToCString(++ilCurrBarKey);
		polLine->omBackBars.Add(polBkBar);
		omLines.Add(polLine);

		polLine = new LineData();
		polLine->TabValues = CString("Don,Summer");
		polLine->Key = NumberToCString(++ilCurrKey);
		polBkBar = new BarData();
		olBkFrom = olOrigTime + COleDateTimeSpan(0, 0, ilFromOffset+150, 0);
		olBkTo   = olBkFrom   + COleDateTimeSpan(0, 0, ilBkBarLen, 0);
		polBkBar->Begin = olBkFrom;
		polBkBar->End   = olBkTo;
		polBkBar->LineKey = polLine->Key;
		polBkBar->Key = NumberToCString(++ilCurrBarKey);
		polLine->omBackBars.Add(polBkBar);
		omLines.Add(polLine);

		polLine = new LineData();
		polLine->TabValues = CString("Hank,Slow");
		polLine->Key = NumberToCString(++ilCurrKey);
		polBkBar = new BarData();
		olBkFrom = olOrigTime + COleDateTimeSpan(0, 0, ilFromOffset+165, 0);
		olBkTo   = olBkFrom   + COleDateTimeSpan(0, 0, ilBkBarLen+30, 0);
		polBkBar->Begin = olBkFrom;
		polBkBar->End   = olBkTo;
		polBkBar->LineKey = polLine->Key;
		polBkBar->Key = NumberToCString(++ilCurrBarKey);
		polLine->omBackBars.Add(polBkBar);
		omLines.Add(polLine);

		polLine = new LineData();
		polLine->TabValues = CString("Bill,Brown");
		polLine->Key = NumberToCString(++ilCurrKey);
		polBkBar = new BarData();
		olBkFrom = olOrigTime + COleDateTimeSpan(0, 0, ilFromOffset+180, 0);
		olBkTo   = olBkFrom   + COleDateTimeSpan(0, 0, ilBkBarLen-30, 0);
		polBkBar->Begin = olBkFrom;
		polBkBar->End   = olBkTo;
		polBkBar->LineKey = polLine->Key;
		polBkBar->Key = NumberToCString(++ilCurrBarKey);
		polLine->omBackBars.Add(polBkBar);
		omLines.Add(polLine);

		polLine = new LineData();
		polLine->TabValues = CString("Sam,Davis");
		polLine->Key = NumberToCString(++ilCurrKey);
		polBkBar = new BarData();
		olBkFrom = olOrigTime + COleDateTimeSpan(0, 0, ilFromOffset+180, 0);
		olBkTo   = olBkFrom   + COleDateTimeSpan(0, 0, ilBkBarLen+30, 0);
		polBkBar->Begin = olBkFrom;
		polBkBar->End   = olBkTo;
		polBkBar->LineKey = polLine->Key;
		polBkBar->Key = NumberToCString(++ilCurrBarKey);
		polLine->omBackBars.Add(polBkBar);
		omLines.Add(polLine);

		polLine = new LineData();
		polLine->TabValues = CString("Will,Diner");
		polLine->Key = NumberToCString(++ilCurrKey);
		polBkBar = new BarData();
		olBkFrom = olOrigTime + COleDateTimeSpan(0, 0, ilFromOffset+210, 0);
		olBkTo   = olBkFrom   + COleDateTimeSpan(0, 0, ilBkBarLen+30, 0);
		polBkBar->Begin = olBkFrom;
		polBkBar->End   = olBkTo;
		polBkBar->LineKey = polLine->Key;
		polBkBar->Key = NumberToCString(++ilCurrBarKey);
		polLine->omBackBars.Add(polBkBar);
		omLines.Add(polLine);

		polLine = new LineData();
		polLine->TabValues = CString("Alana,Miles");
		polLine->Key = NumberToCString(++ilCurrKey);
		polBkBar = new BarData();
		olBkFrom = olOrigTime + COleDateTimeSpan(0, 0, ilFromOffset+210, 0);
		olBkTo   = olBkFrom   + COleDateTimeSpan(0, 0, ilBkBarLen+60, 0);
		polBkBar->Begin = olBkFrom;
		polBkBar->End   = olBkTo;
		polBkBar->LineKey = polLine->Key;
		polBkBar->Key = NumberToCString(++ilCurrBarKey);
		polLine->omBackBars.Add(polBkBar);
		omLines.Add(polLine);

		polLine = new LineData();
		polLine->TabValues = CString("Tim,Glue");
		polLine->Key = NumberToCString(++ilCurrKey);
		polBkBar = new BarData();
		olBkFrom = olOrigTime + COleDateTimeSpan(0, 0, ilFromOffset+240, 0);
		olBkTo   = olBkFrom   + COleDateTimeSpan(0, 0, ilBkBarLen, 0);
		polBkBar->Begin = olBkFrom;
		polBkBar->End   = olBkTo;
		polBkBar->LineKey = polLine->Key;
		polBkBar->Key = NumberToCString(++ilCurrBarKey);
		polLine->omBackBars.Add(polBkBar);
		omLines.Add(polLine);

		polLine = new LineData();
		polLine->TabValues = CString("Jane,Fonda");
		polLine->Key = NumberToCString(++ilCurrKey);
		polBkBar = new BarData();
		olBkFrom = olOrigTime + COleDateTimeSpan(0, 0, ilFromOffset+270, 0);
		olBkTo   = olBkFrom   + COleDateTimeSpan(0, 0, ilBkBarLen, 0);
		polBkBar->Begin = olBkFrom;
		polBkBar->End   = olBkTo;
		polBkBar->LineKey = polLine->Key;
		polBkBar->Key = NumberToCString(++ilCurrBarKey);
		polLine->omBackBars.Add(polBkBar);
		omLines.Add(polLine);

		polLine = new LineData();
		polLine->TabValues = CString("Elvis,Presley");
		polLine->Key = NumberToCString(++ilCurrKey);
		polBkBar = new BarData();
		olBkFrom = olOrigTime + COleDateTimeSpan(0, 0, ilFromOffset+270, 0);
		olBkTo   = olBkFrom   + COleDateTimeSpan(0, 0, ilBkBarLen, 0);
		polBkBar->Begin = olBkFrom;
		polBkBar->End   = olBkTo;
		polBkBar->LineKey = polLine->Key;
		polBkBar->Key = NumberToCString(++ilCurrBarKey);
		polLine->omBackBars.Add(polBkBar);
		omLines.Add(polLine);

		polLine = new LineData();
		polLine->TabValues = CString("Paul,Grant");
		polLine->Key = NumberToCString(++ilCurrKey);
		polBkBar = new BarData();
		olBkFrom = olOrigTime + COleDateTimeSpan(0, 0, ilFromOffset+270, 0);
		olBkTo   = olBkFrom   + COleDateTimeSpan(0, 0, ilBkBarLen, 0);
		polBkBar->Begin = olBkFrom;
		polBkBar->End   = olBkTo;
		polBkBar->LineKey = polLine->Key;
		polBkBar->Key = NumberToCString(++ilCurrBarKey);
		polLine->omBackBars.Add(polBkBar);
		omLines.Add(polLine);

		polLine = new LineData();
		polLine->TabValues = CString("Led,Zeppelin");
		polLine->Key = NumberToCString(++ilCurrKey);
		polBkBar = new BarData();
		olBkFrom = olOrigTime + COleDateTimeSpan(0, 0, ilFromOffset+300, 0);
		olBkTo   = olBkFrom   + COleDateTimeSpan(0, 0, ilBkBarLen-30, 0);
		polBkBar->Begin = olBkFrom;
		polBkBar->End   = olBkTo;
		polBkBar->LineKey = polLine->Key;
		polBkBar->Key = NumberToCString(++ilCurrBarKey);
		polLine->omBackBars.Add(polBkBar);
		omLines.Add(polLine);

		polLine = new LineData();
		polLine->TabValues = CString("Care,Flinn");
		polLine->Key = NumberToCString(++ilCurrKey);
		polBkBar = new BarData();
		olBkFrom = olOrigTime + COleDateTimeSpan(0, 0, ilFromOffset+720, 0);
		olBkTo   = olBkFrom   + COleDateTimeSpan(0, 0, ilBkBarLen, 0);
		polBkBar->Begin = olBkFrom;
		polBkBar->End   = olBkTo;
		polBkBar->LineKey = polLine->Key;
		polBkBar->Key = NumberToCString(++ilCurrBarKey);
		polLine->omBackBars.Add(polBkBar);
		omLines.Add(polLine);
	}
	for (int i = 0; i < omLines.GetSize(); i++)
	{
		omTabLineHeight.Add(20);
		omLines[i].MaxOverlapLevel = 0;
		for (int j = 0; j < omLines[i].omBars.GetSize(); j++)
		{
			omLines[i].omBars[j].OverlapLevel = 0;
		}
	}
}

void CUGanttCtrl::OnTimer(UINT nIDEvent) 
{
	switch (nIDEvent)
	{
		case 0:
			// updating the current time in the gantt and in the timescale
			omCurrentTime = GetCurrentTime();
			pomTimeScale->UpdateCurrentTimeLine(omCurrentTime);

			bmDrawGANTT = TRUE;
			pomTimeScale->Invalidate();
			InvalidateControl();
			MSG olMsg;
			while(::PeekMessage(&olMsg,NULL,0,0,FALSE))
			{
				::GetMessage(&olMsg,NULL,0,0);
				::TranslateMessage(&olMsg);
				::DispatchMessage(&olMsg);
			}
			break;

		case 1:
			// Handle auto scroll:
			// Allow auto scroll ?
			if (m_autoScroll)
			{
				//Yes: decrease timer, until you can scroll:
				m_nScrollTime--;
				if (m_nScrollTime <= 0)
				{
					// Scroll to ?:
					switch (m_AutoScrollDirection)
					{
					case SCROLL_UP:
						OnVScroll(SB_LINEUP, 0, pomVScroll);
						break;
					case SCROLL_DOWN:
						OnVScroll(SB_LINEDOWN, 0, pomVScroll);
						break;
					case SCROLL_LEFT:
						OnHScroll(SB_LINELEFT, 0, pomHScroll);
						break;
					case SCROLL_RIGHT:
						OnHScroll(SB_LINERIGHT, 0, pomHScroll);
						break;
					}  
					m_nScrollTime = m_nScrollInterval / 10;
				}
			}
			break;
		default:
			break;
	}
	COleControl::OnTimer(nIDEvent);
}

//**********************************************************
//*** inserts a line 
//***
//***
//*** created from: MWO				date:	???
//***
//*** changes:
//*** vers.		date		who		what
//***	0.1		07.11.00	RRO		new declaration for the LineSeparators
//***	0.2		15.11.00	RRO		killed init of the LineSeparators, because 
//***								it's got its own method (SetLineSeparator)
//***
//**********************************************************
BOOL CUGanttCtrl::AddLineAt(long LineNo, LPCTSTR KeyValue, LPCTSTR TabString) 
{
	LineData *polNewLine = new LineData;

	polNewLine->Key					= KeyValue;
	polNewLine->TabValues			= CString (TabString);

	if (LineNo < omLines.GetSize() && LineNo != -1)
	{
		omLines.InsertAt(LineNo, polNewLine);
		HandleLineHight (LineNo);
	}
	else
	{
		omLines.Add(polNewLine);
	}
	omTabLineHeight.Add(TabLineHeight);
	TabLines = omLines.GetSize();
	HandleOverlapLevelOfLine (LineNo);
	HandleLineHight  (LineNo);
	return TRUE;
}

//**********************************************************
//*** inserts a backbar
//***
//*** created from: MWO				date:	???
//***
//*** changes:
//*** vers.		date		who		what
//***
//**********************************************************
BOOL CUGanttCtrl::AddBkBarToLine(long LineNo, LPCTSTR BkBarKey, DATE StartTime, DATE EndTime, 
								 long BarColor, LPCTSTR BarText) 
{
	BOOL blRet = TRUE;
	if(LineNo >= 0 && LineNo < omLines.GetSize())
	{
		BarData *polBar = new BarData;
		polBar->BackColor	= BarColor;
		polBar->BarText		= CString (BarText);
		polBar->Key			= CString (BkBarKey);
		polBar->LineNo		= LineNo;
		polBar->LineKey		= omLines[LineNo].Key;
		polBar->Begin		= COleDateTime  (StartTime);
		polBar->End			= COleDateTime  (EndTime);
		CString olF = polBar->Begin.Format  ("%d.%m.%Y-%H:%M"); 
		CString olT = polBar->End.Format	("%d.%m.%Y-%H:%M"); 
		omLines[LineNo].omBackBars.Add  (polBar);
		//omLines[LineNo].omBackBars.Sort (CompareBkBarsByBegin);
		mapBkBars.SetAt((const char*)polBar->Key.GetBuffer(0), (void*)polBar);

	}
	else
	{
		blRet = FALSE;
	}
	return blRet;
}

//**********************************************************
//*** handles the OnLButtonDblClk-event
//***
//***
//*** created from: RRO				date: 13.10.00
//***
//*** changes:
//*** vers.		date		who		what
//***	0.1		20.12.00	RRO		forward tab-events
//***
//**********************************************************
void CUGanttCtrl::OnLButtonDblClk(UINT nFlags, CPoint point) 
{
	//was the click in the Gantt-area?
	if (omGanttRect.PtInRect(point) == TRUE)
	{
		OnLButtonDblClkGantt (nFlags, point);
	}
	//was the click in the Tab-area?
	else if (omTotalTabRect.PtInRect(point) == TRUE)
	{
		OnLButtonDblClkTab (nFlags, point);
	}
	//COleControl::OnLButtonDblClk(nFlags, point);
}


//**********************************************************
//*** makes a refresh of the specified area
//*** possible: GANTT, TAB, SPLITTER, ALL, "", TIMESCALE
//***
//***
//*** created from: RRO				date: 22.08.00
//***
//*** changes:
//*** vers.		date		who		what
//***
//**********************************************************
void CUGanttCtrl::RefreshArea(LPCTSTR Hint) 
{
	CString olStr = Hint;
	if (olStr.Find("ALL") != -1)
	{
		bmDrawALL = TRUE;
	}
	else
	{
		bmDrawALL = FALSE;
		if (olStr.Find("TAB") != -1)
			bmDrawTAB = TRUE;

		if (olStr.Find("GANTT") != -1)
			bmDrawGANTT	= TRUE;

		if (olStr.Find("SPLITTER") != -1)
			bmDrawSPLITTER = TRUE;

		if (olStr.Find("TIMESCALE") != -1)
			bmDrawTIMESCALE = TRUE;

		if (olStr.Find("GLINE") != -1)
			bmDrawGLINE = TRUE;
	}
	InvalidateControl();
}

//**********************************************************
//*** inserts a bar (on a background bar)
//*** 
//***
//***
//*** created from: RRO				date: 22.08.00
//***
//*** changes:
//*** vers.		date		who		what
//***
//**********************************************************
BOOL CUGanttCtrl::AddBarToLine(long LineNo, LPCTSTR Key, DATE StartTime, DATE EndTime, 
							   LPCTSTR BarText, long Shape, long BackColor, long TextColor,
							   BOOL SplitColor, long LeftColor, long RightColor,
							   LPCTSTR LeftOutsideText, LPCTSTR RightOutsideText,
							   long LeftTextColor, long RightTextColor)
{
	BOOL blRet = TRUE;
	if(LineNo >= 0 && LineNo < omLines.GetSize())
	{
		BarData *polBar	= new BarData;

		polBar->LineNo			 = LineNo;
		polBar->LineKey			 = omLines[LineNo].Key;
		polBar->Key				 = CString (Key);
		polBar->Begin			 = COleDateTime (StartTime);
		polBar->End				 = COleDateTime (EndTime);
		polBar->BarText			 = CString (BarText);
		polBar->Shape			 = Shape;
		polBar->BackColor		 = BackColor;
		polBar->TextColor		 = TextColor;
		polBar->SplitColor		 = SplitColor;
		polBar->LeftBackColor	 = LeftColor;
		polBar->RightBackColor	 = RightColor;
		polBar->LeftOutsideText  = LeftOutsideText;
		polBar->RightOutsideText = RightOutsideText;
		polBar->LeftTextColor	 = LeftTextColor;
		polBar->RightTextColor	 = RightTextColor;

		omLines[LineNo].omBars.Add  (polBar);
		mapBars.SetAt((const char*)polBar->Key.GetBuffer(0), (void*)polBar);

		HandleOverlapLevelOfBar  (polBar->Key, TRUE);
  		HandleOverlapLevelOfLine (LineNo);
		HandleLineHight  (LineNo);
		UpdateSingleRect (Key);
	}
	else
	{
		blRet = FALSE;
	}

	return blRet;
}

//**********************************************************
//*** deletes the bar with the specified key
//*** 
//***
//*** created from: RRO				date: 22.08.00
//***
//*** changes:
//*** vers.		date		who		what
//***
//**********************************************************
BOOL CUGanttCtrl::DeleteBarByKey(LPCTSTR Key) 
{
	long llLine, llPos;

	llLine	= GetBarLineByKey (Key);
	llPos	= GetBarPosInLine (Key, llLine);

	if ((llLine > -1) && (llPos > -1))
	{
		LineData *polLine = &omLines[llLine];
		COleDateTime olOldStart = polLine->omBars[llPos].Begin;
		COleDateTime olOldEnd = polLine->omBars[llPos].End;
		polLine->omBars.DeleteAt(llPos);
		mapBars.RemoveKey(Key);
		//handle the timeframe of the line where the bar was deleted
		HandleOverlapLevelOfLineAndTime (llLine, olOldStart, olOldEnd);
		HandleOverlapLevelOfLine (llLine);
		HandleLineHight (llLine);

		bmDrawGANTT = TRUE;
		bmDrawTAB	= TRUE;
		InvalidateControl();
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}

//**********************************************************
//*** deletes the backgroundbar with the specified key
//*** 
//***
//*** created from: RRO				date: 22.08.00
//***
//*** changes:
//*** vers.		date		who		what
//***
//**********************************************************
BOOL CUGanttCtrl::DeleteBkBarByKey(LPCTSTR Key) 
{
	long llLine, llPos;

	llLine	= GetBkBarLineByKey (Key);
	llPos	= GetBkBarPosInLine (Key, llLine);

	if ((llLine > -1) && (llPos > -1))
	{
		omLines[llLine].omBackBars.DeleteAt(llPos);
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}

//**********************************************************
//*** delivers the line of the bar of the specified key
//*** 
//***
//*** created from: RRO				date: 22.08.00
//***
//*** changes:
//*** vers.		date		who		what
//***
//**********************************************************
long CUGanttCtrl::GetBarLineByKey(LPCTSTR opKey)
{
	long llRet = -1;
	for (int i = 0; i < omLines.GetSize(); i++)
	{
		for (int j = 0; j < omLines[i].omBars.GetSize(); j++)
		{
			if (omLines[i].omBars[j].Key == opKey)
			{
				llRet = i;
				return llRet;
			}
		}
	}
	return llRet;
}

//**********************************************************
//*** delivers the position of the bar of the specified key
//*** in the specified line
//***
//***
//*** created from: RRO				date: 22.08.00
//***
//*** changes:
//*** vers.		date		who		what
//***
//**********************************************************
long CUGanttCtrl::GetBarPosInLine(LPCTSTR opKey, long lpLineNo)
{
	long llRet = -1;

	if (lpLineNo > -1)
	{
		for (int i = 0; i < omLines[lpLineNo].omBars.GetSize(); i++)
		{
			if (omLines[lpLineNo].omBars[i].Key == opKey)
			{
				llRet = i;
				return llRet;
			}
		}
	}
	return llRet;
}

//**********************************************************
//*** delivers the line of the backgroundbar of the specified key
//*** 
//***
//***
//*** created from: RRO				date: 22.08.00
//***
//*** changes:
//*** vers.		date		who		what
//***
//**********************************************************
long CUGanttCtrl::GetBkBarLineByKey(LPCTSTR olKey) 
{
	long llRet = -1;
	for (int i = 0; i < omLines.GetSize(); i++)
	{
		for (int j = 0; j < omLines[i].omBackBars.GetSize(); j++)
		{
			if (omLines[i].omBackBars[j].Key == olKey)
			{
				llRet = i;
				return llRet;
			}
		}
	}
	return llRet;
}

//**********************************************************
//*** delivers the position of the backgroundbar of the 
//*** specified key in the specified line
//***
//***
//*** created from: RRO				date: 22.08.00
//***
//*** changes:
//*** vers.		date		who		what
//***
//**********************************************************
long CUGanttCtrl::GetBkBarPosInLine(LPCTSTR olKey, long llLineNo) 
{
	long llRet = -1;

	if ((llLineNo > -1) && (llLineNo < omLines.GetSize()))
	{
		for (int i = 0; i < omLines[llLineNo].omBackBars.GetSize(); i++)
		{
			if (omLines[llLineNo].omBackBars[i].Key == olKey)
			{
				llRet = i;
				return llRet;
			}
		}
	}
	return llRet;
}

//**********************************************************
//*** delivers the keys of all backgroundbars of the
//*** specified line in a string, separated by ";"
//***
//***
//*** created from: RRO				date: 23.08.00
//***
//*** changes:
//*** vers.		date		who		what
//***
//**********************************************************
BSTR CUGanttCtrl::GetBkBarsByLine(long llLineNo) 
{
	CString strResult;
	CString strHelp;

	if ((llLineNo > -1) && (llLineNo < omLines.GetSize()))
	{
		strHelp = "";
		for (int i = 0; i < omLines[llLineNo].omBackBars.GetSize(); i++)
		{
			strHelp += omLines[llLineNo].omBackBars[i].Key + ";";
		}
		strResult = strHelp;
	}

	return strResult.AllocSysString();
}

//**********************************************************
//*** delivers the keys of all bars of the specified
//*** line in a string, separated by ";"
//***
//***
//*** created from: RRO				date: 23.08.00
//***
//*** changes:
//*** vers.		date		who		what
//***
//**********************************************************
BSTR CUGanttCtrl::GetBarsByLine(long llLineNo) 
{
	CString strResult;
	CString strHelp;

	if ((llLineNo > -1) && (llLineNo < omLines.GetSize()))
	{
		strHelp = "";
		for (int i = 0; i < omLines[llLineNo].omBars.GetSize(); i++)
		{
			strHelp += omLines[llLineNo].omBars[i].Key + ";";
		}
		strResult = strHelp;
	}

	return strResult.AllocSysString();
}

//**********************************************************
//*** delivers the keys of all bars of the specified
//*** backgroundbar in a string, separated by ";"
//***
//***
//*** created from: RRO				date: 08.09.00
//***
//*** changes:
//*** vers.		date		who		what
//***
//**********************************************************
BSTR CUGanttCtrl::GetBarsByBkBar(LPCTSTR olBkBarKey) 
{
	CString strResult;
	CString strHelp;
	int ilLineNo = GetBkBarLineByKey (olBkBarKey);

	if (ilLineNo > -1)
	{
		strHelp = "";
		LineData *polLine		= &omLines[ilLineNo];
		int ilPos				= GetBkBarPosInLine (olBkBarKey, ilLineNo);
		BarData *polBkBar		= &polLine->omBackBars[ilPos];
		COleDateTime olBegin	= polBkBar->Begin;
		COleDateTime olEnd		= polBkBar->End;
		COleDateTime olBeginBar;
		COleDateTime olEndBar;

		for (int i = 0; i < polLine->omBars.GetSize(); i++)
		{
			BarData *polBar	= &polLine->omBars[i];
			olBeginBar		= polBar->Begin;
			olEndBar		= polBar->End;

			BOOL blIsOverlapped	= IsReallyOverlapped (olBegin, olEnd, olBeginBar, olEndBar);
			BOOL blIsWithIn		= IsWithIn			 (olBegin, olEnd, olBeginBar, olEndBar);
			BOOL blIsInvolved	= (blIsOverlapped || blIsWithIn);

			if (blIsInvolved != FALSE)
				strHelp += polBar->Key + ";";
		}
		strResult = strHelp;
	}
	return strResult.AllocSysString();
}
//**********************************************************
//*** Detects the current bar under the mouse
//*** Gantt-area
//***
//***
//*** created from: MWO				date: 21.08.2002
//***
//*** changes:
//*** rro, 22.08.02: don't reset global members for remembering
//***				 the clicked one!
//***					
//***
//**********************************************************
CString CUGanttCtrl::GetBarByMousePosition(CPoint point)
{
	CString olRet = "";
	bool blFoundBar = false;
	LineData *polLine = NULL;
	BarData *polBar = NULL;
	COleDateTime olXTime = GetTimeFromX (point.x);
	int ilBarPos;

	if(imCurrentHighlightLine == -1)
	{
		return olRet;
	}

	if (imCurrentHighlightLine < omLines.GetSize() && omLines.GetSize() > 0)
	{
		polLine = &omLines[imCurrentHighlightLine];
	}

	int ilTopOfLine = GetTopOfLine (imCurrentHighlightLine);
	int ilTopOfBar;
	int ilBarHeight = (int)(TabLineHeight/(m_subbarStackCount+1)) - 2;

	//to detect the right bar, look first, if the click was on a bar
	if (polLine != NULL)
	{
		ilBarPos = polLine->omBars.GetSize();

		while (ilBarPos > 0 && blFoundBar == false)
		{
			ilBarPos--;
			polBar = &polLine->omBars[ilBarPos];

			if (olXTime > polBar->Begin)
			{
				if (olXTime < polBar->End)
				{
					//now exclude other bars of an overlapping
					ilTopOfBar = (BarOverlapOffset * polBar->OverlapLevel) + 2 + ilTopOfLine;
					if ((point.y > ilTopOfBar) && (point.y < (ilTopOfBar + ilBarHeight)))
					{
						olRet = polBar->Key;
						blFoundBar = true;
					}
				}
			}
		}
	}

	return olRet;
}

//**********************************************************
//*** Detects the current sub-bar under the point
//***
//***
//*** created from: rro				date: 30.08.2002
//***
//*** changes:
//***					
//***
//**********************************************************
BarData* CUGanttCtrl::GetSubBarByMousePosition(CPoint point)
{
	BarData* polRetBar = NULL;
	bool blFoundBar = false;
	LineData *polLine = NULL;
	BarData *polBar = NULL;
	BarData *polSubBar = NULL;
	COleDateTime olXTime = GetTimeFromX (point.x);
	int ilBarPos;
	int ilSubBarPos;

	if(imCurrentHighlightLine == -1)
	{
		return NULL;
	}

	if (imCurrentHighlightLine < omLines.GetSize() && omLines.GetSize() > 0)
	{
		polLine = &omLines[imCurrentHighlightLine];
	}

	int ilTopOfLine = GetTopOfLine (imCurrentHighlightLine);
	int ilTopOfBar;
	int ilBottomOfBar;
	int ilBarHeight = (int)(TabLineHeight/(m_subbarStackCount+1)) - 2;

	//to detect the right bar, look first, if the click was on a bar
	if (polLine != NULL)
	{
		ilBarPos = polLine->omBars.GetSize();

		while (ilBarPos > 0 && blFoundBar == false)
		{
			ilBarPos--;
			polBar = &polLine->omBars[ilBarPos];

			ilSubBarPos = polBar->omSubBars.GetSize();
			while (ilSubBarPos > 0 && blFoundBar == false)
			{
				ilSubBarPos--;
				polSubBar = &polBar->omSubBars[ilSubBarPos];

				if (olXTime > polSubBar->Begin)
				{
					if (olXTime < polSubBar->End)
					{
						//now exclude other bars of an overlapping
						ilTopOfBar		 = ilTopOfLine;
						ilTopOfBar		+= ((polSubBar->OverlapLevel - 1 ) * BarOverlapOffset) + ilBarHeight;
						ilTopOfBar		+= (int)(TabLineHeight / 8);
						ilBottomOfBar	 = ilTopOfBar + ilBarHeight - (int)(TabLineHeight/8);
						if (point.y > ilTopOfBar && point.y < ilBottomOfBar)
						{
							polRetBar = polSubBar;
							blFoundBar = true;
						}
					}
				}
			}
		}
	}

	return polRetBar;
}
//**********************************************************
//*** Detects the current back bar under the mouse
//*** Gantt-area
//***
//***
//*** created from: MWO				date: 21.08.2002
//***
//*** changes:
//***
//**********************************************************
CString CUGanttCtrl::GetBkBarByMousePosition(CPoint point)
{
	CString olRet = "";
	bool blFoundBar = false;
	LineData *polLine = NULL;
	BarData *polBar = NULL;
	COleDateTime olXTime = GetTimeFromX (point.x);
	int ilBarPos;

	if(imCurrentHighlightLine == -1)
	{
		return olRet;
	}

	if (imCurrentHighlightLine < omLines.GetSize() && omLines.GetSize() > 0)
	{
		polLine = &omLines[imCurrentHighlightLine];
	}

	int ilTopOfLine = GetTopOfLine (imCurrentHighlightLine);

	//to detect the right bar, look first, if the click was on a bar
	if (polLine != NULL)
	{
		ilBarPos = polLine->omBackBars.GetSize();

		while (ilBarPos > 0 && blFoundBar == false)
		{
			ilBarPos--;
			polBar = &polLine->omBackBars[ilBarPos];

			if (olXTime > polBar->Begin)
			{
				if (olXTime < polBar->End)
				{
					olRet = polBar->Key;
					blFoundBar = true;
				}
			}
		}
	}

	return olRet;
}

//**********************************************************
//*** handles the LButtonDown-event in the
//*** Gantt-area
//***
//***
//*** created from: RRO				date: 26.08.00
//***
//*** changes:
//*** vers.		date		who		what
//***	0.1		20.11.00	RRO		correction of detecting overlapping
//***								while searching for the correct bar
//***
//**********************************************************
void CUGanttCtrl::OnLButtonDownGantt(UINT nFlags, CPoint point)
{

	bmFoundBar		= FALSE;
	bmFoundBkBar	= FALSE;
	omBarMode		= "NORMAL";

	m_AutoScrollDirection = SCROLL_NOSCROLL;

	int ilFlags		= 0;

	//prepare ilFlags
	if (nFlags & MK_CONTROL)
		ilFlags += MK_CONTROL;
	if (nFlags & MK_LBUTTON)
		ilFlags += MK_LBUTTON;
	if (nFlags & MK_MBUTTON)
		ilFlags += MK_MBUTTON;
	if (nFlags & MK_RBUTTON)
		ilFlags += MK_RBUTTON;
	if (nFlags & MK_SHIFT)
		ilFlags += MK_SHIFT;

	LineData *polLine;

	if ((imCurrentHighlightLine > -1) && (imCurrentHighlightLine < omLines.GetSize()) && (omLines.GetSize() > 0))
	{
		polLine = &omLines[imCurrentHighlightLine];
	}
	else
	{
		return;
	}

	CalcTopOfLine (imCurrentHighlightLine); //you need "imCurrYTop" when there is overlapping

	if (nFlags & MK_LBUTTON)
	{
		//look first, if the click was on a bar
		CString olKey;
		olKey = GetBarByMousePosition (point);

		if (olKey.GetLength() > 0)
		{
			//initialize some members to remember the bar we've found
			bmFoundBar		= TRUE;

			pomBar = GetBarByKey (olKey);
			imCurrBarLine = imCurrentHighlightLine;
			imCurrBarPos = GetBarPosInLine (olKey, imCurrentHighlightLine);
			omCurrBarKey = olKey;
			omCurrBarRect = GetBarRectByKey (pomBar->Key);

			m_dMoveBarOldBegin = pomBar->Begin;
			m_dMoveBarOldEnd = pomBar->End;
			m_MoveSubBarsOldBegin.DeleteAll();
			m_MoveSubBarsOldEnd.DeleteAll();
			for (int ilCntSubBar = 0; ilCntSubBar < pomBar->omSubBars.GetSize(); ilCntSubBar++)
			{
				COleDateTime *polBegin = new COleDateTime();
				COleDateTime *polEnd   = new COleDateTime();
				*polBegin = pomBar->omSubBars[ilCntSubBar].Begin;
				*polEnd = pomBar->omSubBars[ilCntSubBar].End;
				m_MoveSubBarsOldBegin.Add (polBegin);
				m_MoveSubBarsOldEnd.Add (polEnd);
			}


			//handles the cursor-state and omBarMode (e.g. MOVE)!
			HandleCursorGantt(point, nFlags, GetXFromTime(pomBar->Begin), GetXFromTime(pomBar->End));

			if (!(nFlags & MK_CONTROL))
			{
				int ilOldOverlapLevel = 0;
				int ilNewOverlapLevel = 0;
				int ilDiffOverlapLevel = 0;

				ilOldOverlapLevel = pomBar->OverlapLevel;
				HandleOverlapLevelOfBar (olKey, TRUE);
				HandleOverlapLevelOfLine (imCurrBarLine);
				HandleLineHight (imCurrBarLine);

				// Update the cursor position to make sure that the cursor will always be over
				// the bar we just click on.
				ilNewOverlapLevel		= pomBar->OverlapLevel;
				ilDiffOverlapLevel		= ilNewOverlapLevel - ilOldOverlapLevel;
				CPoint olMousePosition	= point;
				
				olMousePosition.Offset (0, ilDiffOverlapLevel * BarOverlapOffset);
				ClientToScreen(&olMousePosition);
				::SetCursorPos(olMousePosition.x, olMousePosition.y);
				IntSetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
				
				//drag&drop - section
				if (m_enableDragDropBar == TRUE)
				{
					int nStringSize = olKey.GetLength();
					omDataSource = new COleDataSource();
					HGLOBAL hString = ::GlobalAlloc (GMEM_SHARE, nStringSize + 1); 
					LPVOID  pString = ::GlobalLock  (hString);
					ASSERT(pString != NULL);
					memcpy(pString, olKey, nStringSize);
					((char*)pString)[nStringSize] = '\0';// oder 0x00;
					::GlobalUnlock (hString);
					omDataSource->CacheGlobalData(CF_GANTT, hString);
					DROPEFFECT dropEffect = omDataSource->DoDragDrop(
						DROPEFFECT_MOVE|DROPEFFECT_COPY, CRect(0, 0, 0, 0));
					delete omDataSource;
				}
			}

			//fire the event to the container
			FireOnLButtonDownBar (_bstr_t (LPCTSTR (olKey)), ilFlags);

		}
		else
		{
			BarData* polSubBar = GetSubBarByMousePosition (point);
			if (polSubBar != NULL)
			{
				FireOnLButtonDownSubBar (_bstr_t (LPCTSTR (polSubBar->MainbarKey)), _bstr_t (LPCTSTR (polSubBar->Key)), ilFlags);
			}
			else
			{
				olKey = GetBkBarByMousePosition (point);

				if (olKey.GetLength() > 0)
				{
					bmFoundBkBar = TRUE;
					BarData *polBar = GetBkBarByKey (olKey);
					m_dMoveBkBarOldBegin = polBar->Begin;
					m_dMoveBkBarOldEnd = polBar->End;
					imCurrBarLine	= imCurrentHighlightLine;
					imCurrBarPos	= GetBkBarPosInLine(olKey, imCurrentHighlightLine);
					if (!(nFlags & MK_CONTROL))
					{
						FireOnLButtonDownBkBar (_bstr_t (LPCTSTR (olKey)), ilFlags);
					}
					else
					{
						HandleCursorGantt(point, nFlags, GetXFromTime(polBar->Begin), GetXFromTime(polBar->End));
					}
				}
				if (bmFoundBkBar != TRUE)
				{
					FireOnLButtonDownBkGantt ();
				}
			}
		}
	}
}

//**********************************************************
//*** handles the LButtonDown-event in the Tab-area
//***
//*** created from: RRO				date: 20.12.00
//***
//*** changes:
//*** vers.		date		who		what
//***
//**********************************************************
void CUGanttCtrl::OnLButtonDownTab(UINT nFlags, CPoint point)
{
	short slColumn	= 0;
	int	  ilFlags	= 0;
	int   ilLeft	= 0;
	long  llRow		= imCurrentHighlightLine;

	//prepare ilFlags
	if (nFlags & MK_CONTROL)
		ilFlags += MK_CONTROL;
	if (nFlags & MK_LBUTTON)
		ilFlags += MK_LBUTTON;
	if (nFlags & MK_MBUTTON)
		ilFlags += MK_MBUTTON;
	if (nFlags & MK_RBUTTON)
		ilFlags += MK_RBUTTON;
	if (nFlags & MK_SHIFT)
		ilFlags += MK_SHIFT;

	//look, if the click was below table-area
	if (imCurrentHighlightLine >= omLines.GetSize())
	{
		slColumn = -1;
		llRow	 = -1;
	}
	else
	{
		//the click was on the top line of the table
		if (point.y < TimeScaleHeaderHeight)
		{
			llRow = -1;
		}
		//look for the column
		while (ilLeft < point.x)
		{
			ilLeft += omTabHeaderLen[slColumn];
			slColumn++;
		}
		if(slColumn >= 1)
		{
			slColumn--;
		}
	}
	//now fire the column, the row and the nFlags
	FireOnLButtonDownTab (llRow, slColumn, ilFlags);
}

//**********************************************************
//*** handles the OnLButtonDblClk-event in the
//*** Gantt-area
//***
//***
//*** created from: RRO				date: 13.10.00
//***
//*** changes:
//*** vers.		date		who		what
//***
//**********************************************************
void CUGanttCtrl::OnLButtonDblClkGantt(UINT nFlags, CPoint point)
{
	bmFoundBar		= FALSE;
	bmFoundBkBar	= FALSE;

	LineData *polLine	= NULL;
	int ilBarHeight = (int)(TabLineHeight/(m_subbarStackCount+1)) - 2;
	int ilTopOfLine		= GetTopOfLine (imCurrentHighlightLine);
	int ilTopOfBar		= 0;
	int ilFlags			= 0;
	int ilEndPt			= -1;
	int ilStartPt		= -1;
	int ilStartOffsetPt = -1;
	int ilRange			= -1;
	short ilPercent		= -1;

	//prepare ilFlags
	if (nFlags & MK_CONTROL)
		ilFlags += MK_CONTROL;
	if (nFlags & MK_LBUTTON)
		ilFlags += MK_LBUTTON;
	if (nFlags & MK_MBUTTON)
		ilFlags += MK_MBUTTON;
	if (nFlags & MK_RBUTTON)
		ilFlags += MK_RBUTTON;
	if (nFlags & MK_SHIFT)
		ilFlags += MK_SHIFT;

	if ((imCurrentHighlightLine > -1) && (imCurrentHighlightLine < omLines.GetSize()))
		polLine = &omLines[imCurrentHighlightLine];

	if ((nFlags & MK_LBUTTON) && (polLine != NULL))
	{
		//look first, if the click was on a bar
		if ((imCurrentHighlightLine > -1) && (imCurrentHighlightLine < omLines.GetSize()))
		{
			int i = polLine->omBars.GetSize() - 1;
			while ((i >= 0) && (bmFoundBar != TRUE))
			{
				if (GetTimeFromX (point.x) > polLine->omBars[i].Begin)
				{
					if (GetTimeFromX (point.x) < polLine->omBars[i].End)
					{
						//now exclude other bars of an overlapping
						ilTopOfBar = ilTopOfLine + (BarOverlapOffset * polLine->omBars[i].OverlapLevel) + 2;
						if ((point.y > ilTopOfBar) && (point.y < (ilTopOfBar + ilBarHeight)))
						{
							bmFoundBar = TRUE;
							FireOnLButtonDblClkBar (polLine->omBars[i].Key, ilFlags);
							//MWO: 19.08.2002
							ilEndPt = GetXFromTime(polLine->omBars[i].End);
							ilStartPt = GetXFromTime(polLine->omBars[i].Begin);
							ilRange = ilEndPt - ilStartPt;
							ilStartOffsetPt = point.x - ilStartPt;
							if (ilRange > 0 )
							{
								ilPercent = (int)((ilStartOffsetPt*100)/ilRange);
								FireOnLButtonDblClkPercentBar(polLine->omBars[i].Key, ilPercent, ilFlags);
							}
							//END MWO: 19.08.2002
						}
					}
				}
				i--;
			}
			if (bmFoundBar != TRUE)
			{
				// let's look for a sub-bar...
				BarData* polSubBar = GetSubBarByMousePosition (point);
				if (polSubBar != NULL)
				{
					FireOnLButtonDblClkSubBar (_bstr_t (LPCTSTR (polSubBar->MainbarKey)), _bstr_t (LPCTSTR (polSubBar->Key)), ilFlags);
				}
				else
				{
					// ...we didn't find anything yet - now look for a background-bar
					i = polLine->omBackBars.GetSize() - 1;
					while ((i >= 0) && (bmFoundBkBar != TRUE))
					{
						if (GetTimeFromX (point.x) > polLine->omBackBars[i].Begin)
						{
							if (GetTimeFromX(point.x) < polLine->omBackBars[i].End)
							{
								bmFoundBkBar	= TRUE;
								FireOnLButtonDblClkBkBar (polLine->omBackBars[i].Key, ilFlags);
								//MWO: 19.08.2002
								ilEndPt = GetXFromTime(polLine->omBackBars[i].End);
								ilStartPt = GetXFromTime(polLine->omBackBars[i].Begin);
								ilRange = ilEndPt - ilStartPt;
								ilStartOffsetPt = point.x - ilStartPt;
								if (ilRange > 0 )
								{
									ilPercent = (int)((ilStartOffsetPt*100)/ilRange);
									FireOnLButtonDblClkPercentBar(polLine->omBars[i].Key, ilPercent, ilFlags);
								}
								//END MWO: 19.08.2002
							}
						}
						i--;
					}
					if (bmFoundBkBar != TRUE)
					{
						FireOnLButtonDblClkBkGantt ();
					}
				}
			}
		}
	}
}

//**********************************************************
//*** handles the LButtonDblClk-event in the Tab-area
//***
//*** created from: RRO				date: 20.12.00
//***
//*** changes:
//*** vers.		date		who		what
//***
//**********************************************************
void CUGanttCtrl::OnLButtonDblClkTab(UINT nFlags, CPoint point)
{
	short slColumn	= 0;
	int	  ilFlags	= 0;
	int   ilLeft	= 0;
	long  llRow		= imCurrentHighlightLine;

	//prepare ilFlags
	if (nFlags & MK_CONTROL)
		ilFlags += MK_CONTROL;
	if (nFlags & MK_LBUTTON)
		ilFlags += MK_LBUTTON;
	if (nFlags & MK_MBUTTON)
		ilFlags += MK_MBUTTON;
	if (nFlags & MK_RBUTTON)
		ilFlags += MK_RBUTTON;
	if (nFlags & MK_SHIFT)
		ilFlags += MK_SHIFT;

	//look, if the click was below table-area
	if (imCurrentHighlightLine >= omLines.GetSize())
	{
		slColumn = -1;
		llRow	 = -1;
	}
	else
	{
		//the click was on the top line of the table
		if (point.y < TimeScaleHeaderHeight)
		{
			llRow = -1;
		}
		//look for the column
		while (ilLeft < point.x)
		{
			ilLeft += omTabHeaderLen[slColumn];
			slColumn++;
		}
		if(slColumn >= 1)
		{
			slColumn--;
		}
	}
	//now fire the column, the row and the nFlags
	FireOnLButtonDblClkTab (llRow, slColumn, ilFlags);
}

//**********************************************************
//*** handles the OnMouseMove-event in the
//*** Gantt-area
//***
//***
//*** created from: RRO				date: 26.08.00
//***
//*** changes:
//*** vers.		date		who		what
//***
//**********************************************************
void CUGanttCtrl::OnMouseMoveGantt(UINT nFlags, CPoint point)
{
	FireTimeFromX (DATE(GetTimeFromX(point.x)));

	LineData *polCurrHighlightLine	= NULL;
	LineData *polCurrBarLine		= NULL;
	BarData	 *polCurrBar			= NULL;

	if ((imCurrentHighlightLine > -1) && (imCurrentHighlightLine < omLines.GetSize()) && (omLines.GetSize() > 0))
		polCurrHighlightLine	= &omLines[imCurrentHighlightLine];
	if ((imCurrBarLine > -1) && (omLines.GetSize() > 0))
		polCurrBarLine			= &omLines[imCurrBarLine];

	if (!(nFlags & MK_CONTROL))
	{
		omBarMode = "NORMAL";
		m_AutoScrollDirection = SCROLL_NOSCROLL;
	}

	// Handle Autoscroll, when sizing or moving:
	if ((omBarMode == "SIZE") || (omBarMode == "MOVE"))
	{
		CRect olRect;
		olRect = omGanttRect;
		
		if (point.x >= (olRect.right - m_nScrollInset))
		{
			// Mouse moved to right border: Start scrolling right if not already done:
			if (m_AutoScrollDirection != SCROLL_RIGHT)
			{
				m_nScrollTime = m_nScrollDelay / 10;
				m_AutoScrollDirection = SCROLL_RIGHT;
			}
		}
		else if (point.x <= olRect.left + m_nScrollInset)
		{
			// Mouse moved to left border: Start scrolling left if not already done:
			if (m_AutoScrollDirection != SCROLL_LEFT)
			{
				m_nScrollTime = m_nScrollDelay / 10;
				m_AutoScrollDirection = SCROLL_LEFT;
			}
		}
		else
		{
			//Stop scrolling if mouse moved elsewhere:
			m_AutoScrollDirection = SCROLL_NOSCROLL;
		}
	}
	
	if (omBarMode == "SIZE")
	{
		if (bmFoundBar == TRUE)
		{
			polCurrBar = &polCurrBarLine->omBars[imCurrBarPos];
			
			if ((imBarOffsetLeft > -1) && (point.x < imMaxLeft))				//we change the begin of the bar
			{
				FireChangeBarDate(polCurrBar->Key,
					GetTimeFromX (point.x - imBarOffsetLeft),
					polCurrBar->End);
			}
			else if ((imBarOffsetRight > -1) && (point.x > imMaxRight))			//we change the end of the bar
			{
				FireChangeBarDate(polCurrBar->Key,
					polCurrBar->Begin,
					GetTimeFromX (point.x + imBarOffsetRight));
			}
		}
		else if (bmFoundBkBar == TRUE)
		{
			polCurrBar = &polCurrBarLine->omBackBars[imCurrBarPos];
			
			if (imBarOffsetLeft > -1)											//we change the begin of a backbar
			{
				FireChangeBkBarDate(polCurrBar->Key,
					GetTimeFromX (point.x - imBarOffsetLeft),
					polCurrBar->End);
			}
			else
			{
				FireChangeBkBarDate(polCurrBar->Key,							//we change the end of a backbar
					polCurrBar->Begin,
					GetTimeFromX (point.x + imBarOffsetRight));
			}
		}
	}
	else if (omBarMode == "MOVE")
	{
		if (bmFoundBar == TRUE)
		{
			polCurrBar = &polCurrBarLine->omBars[imCurrBarPos];
			
			FireChangeBarDate(polCurrBar->Key,								//we change begin & end
				GetTimeFromX (point.x - imBarOffsetLeft),
				GetTimeFromX (point.x + imBarOffsetRight));
		}
		else if (bmFoundBkBar == TRUE)
		{
			polCurrBar = &polCurrBarLine->omBackBars[imCurrBarPos];
			
			FireChangeBkBarDate(polCurrBar->Key,								//we change begin & end
				GetTimeFromX (point.x - imBarOffsetLeft),
				GetTimeFromX (point.x + imBarOffsetRight));
		}
	}
	else
	{
		//just moving your mouse by holding Ctrl
		if ((nFlags & MK_CONTROL) && (!(nFlags & MK_LBUTTON)))
		{
			BOOL blFoundBar		= FALSE;
			BOOL blFoundBkBar	= FALSE;
			//look first, if click was on a bar
			if ((imCurrentHighlightLine > -1) && (omLines.GetSize()) && (imCurrentHighlightLine <= omLines.GetSize()))
			{
				for(int i = 0; i < polCurrHighlightLine->omBars.GetSize(); i++)
				{
					if(GetTimeFromX(point.x) > polCurrHighlightLine->omBars[i].Begin)
					{
						if (GetTimeFromX(point.x) < polCurrHighlightLine->omBars[i].End)
						{
							blFoundBar = TRUE;
							HandleCursorGantt(point, nFlags,
								GetXFromTime(polCurrHighlightLine->omBars[i].Begin),
								GetXFromTime(polCurrHighlightLine->omBars[i].End));
						}
					}
				}
				//there was no bar at the click-point, so look, if there's a backgroundbar
				if(blFoundBar != TRUE)
				{
					for(int i = 0; i < polCurrHighlightLine->omBackBars.GetSize(); i++)
					{
						if(GetTimeFromX(point.x) > polCurrHighlightLine->omBackBars[i].Begin)
						{
							if (GetTimeFromX(point.x) < polCurrHighlightLine->omBackBars[i].End)
							{
								blFoundBkBar = TRUE;
								HandleCursorGantt(point, nFlags,
									GetXFromTime(polCurrHighlightLine->omBackBars[i].Begin),
									GetXFromTime(polCurrHighlightLine->omBackBars[i].End));
							}
						}
					}
				}
				if ((blFoundBar == FALSE) && (blFoundBkBar == FALSE))
				{
					IntSetCursor(LoadCursor(NULL, IDC_ARROW));
				}
			}
		}
	}
	if (!(nFlags & MK_CONTROL))
	{
		IntSetCursor(LoadCursor(NULL, IDC_ARROW));
	}
	
	HighlightLine (point);
	FireActualLineNo (imCurrentHighlightLine);
	//MWO: 21.08.2002
	//Handle simple Mouse move and fire if it was a bar or a back bar
	CString olKey;
	olKey = GetBarByMousePosition(point);
	if (olKey != CString(""))
	{
		FireOnMouseMoveBar(olKey.GetBuffer(0));
	}
	else
	{
		olKey = GetBkBarByMousePosition(point);
		if(olKey != CString(""))
		{
			FireOnMouseMoveBkBar(olKey.GetBuffer(0));
		}
	}
}

//**********************************************************
//*** handles the highlighting of the TAB-line
//***
//***
//*** created from: RRO				date: 26.08.00
//***
//*** changes:
//*** vers.		date		who		what
//***   0.1		06.09.00	RRO		dynamic line hight!
//**********************************************************
void CUGanttCtrl::HighlightLine(CPoint point)
{
	//Heighlight the current line in tab area	
	int ilTopY	= omGanttRect.top;
	int ilPos	= pomVScroll->GetScrollPos();

	if (point.y <= TimeScaleHeaderHeight)
	{
		return;
	}

		while (point.y > ilTopY)
		{
			//MWO
			if(ilPos < omLines.GetSize())
			{
				ilTopY += omTabLineHeight[ilPos];
				ilPos++;
			}
			else{point.y = ilTopY;}
			//END MWO
		}
	if(imCurrentHighlightLine != ilPos - 1)
	{
		imCurrentHighlightLine = ilPos - 1;
		bmDrawSPLITTER = TRUE;
		bmDrawTAB	   = TRUE;
    bmDrawALL = FALSE;
//    bmDrawGANTT = FALSE;
//    bmDrawGLINE = FALSE;
		InvalidateControl (&omCurrentTabRect);
	}
}

//**********************************************************
//*** handles the cursor in the Gantt-area
//***
//***
//*** created from: RRO				date: 28.08.00
//***
//*** changes:
//*** vers.		date		who		what
//***
//**********************************************************
void CUGanttCtrl::HandleCursorGantt(CPoint point, UINT nFlags, int imLeft, int imRight)
{
	if ((point.x <= imLeft + ResizeAreaWidth) || (point.x >= imRight - ResizeAreaWidth))
	{
		//the click was in the border area
		IntSetCursor(LoadCursor(NULL, IDC_SIZEWE));
		CalcTopOfLine (imCurrBarLine);

		//test the nFlags
		if (!((nFlags & MK_CONTROL) && (nFlags & MK_LBUTTON)))
		{
			omBarMode = "NORMAL";
			m_AutoScrollDirection = SCROLL_NOSCROLL;
		}
		else
		{
			omBarMode = "SIZE";

			//init the offset of the (background)bar
			if (point.x <= imLeft + ResizeAreaWidth)
			{
				imBarOffsetLeft  = point.x - imLeft;
				imMaxLeft		 = imRight - ResizeMinimalWidth;
				imMaxRight		 = -1;
				imBarOffsetRight = -1;
			}
			else
			{
				imBarOffsetRight = imRight - point.x;
				imMaxRight		 = imLeft  + ResizeMinimalWidth;
				imMaxLeft		 = -1;
				imBarOffsetLeft  = -1;
			}
		}
	}
	else if ((point.x > imLeft + ResizeAreaWidth) && (point.x < imRight - ResizeAreaWidth))
	{
		//the click was in the body
		IntSetCursor(LoadCursor(NULL, IDC_SIZEALL));
		CalcTopOfLine(imCurrBarLine);

		if (!((nFlags & MK_CONTROL) && (nFlags & MK_LBUTTON)))
		{
			omBarMode = "NORMAL";
			m_AutoScrollDirection = SCROLL_NOSCROLL;
		}
		else
		{
			omBarMode = "MOVE";
		}

		imBarOffsetLeft  = point.x - imLeft;
		imBarOffsetRight = imRight - point.x;
	}
	//there was nothing at the point
	else
	{
		IntSetCursor(LoadCursor(NULL, IDC_ARROW));
		omBarMode		 = "NORMAL";
		m_AutoScrollDirection = SCROLL_NOSCROLL;
		imBarOffsetLeft  = -1;
		imBarOffsetRight = -1;
	}
}

//-----------------------------
//--------- drag & drop - stuff
//-----------------------------
void CUGanttCtrl::OnEnableDragDropBarChanged() 
{
	SetModifiedFlag();
}

void CUGanttCtrl::OnEnableDragDropBkBarChanged() 
{
	SetModifiedFlag();
}

//*********************************************************
//*** this is by beginning drag&drop;
//*** "enter" means 2 cases:
//*** - drag&drop something within this window
//*** - getting s.th. from anywhere
//***
//*** created from: RRO				date: 01.09.00
//***
//*** changes:
//*** vers.		date		who		what
//***
//**********************************************************
DROPEFFECT CUGanttCtrl::OnDragEnter(CWnd* pWnd, COleDataObject* pDataObject, DWORD dwKeyState, CPoint point)
{
  DROPEFFECT olDropEffect = DROPEFFECT_NONE;
  
  m_AutoScrollDirection = SCROLL_NOSCROLL;
  
  if (!m_SelfIsDragging && pDataObject->IsDataAvailable (CF_GANTT))
  {
    m_CanAcceptDrop = true;
    olDropEffect = (dwKeyState & MK_CONTROL) 
						? DROPEFFECT_COPY
            : DROPEFFECT_MOVE;
    
    HGLOBAL gmem  = pDataObject->GetGlobalData (CF_GANTT);
    CString olKey = LPCTSTR (GlobalLock (gmem));
    omCurrBarKey  = olKey;
    
    FireOnDragEnter (_bstr_t (LPCTSTR(olKey)));
  }
  return olDropEffect;
}

//**********************************************************
//*** this is during drag&drop;
//*** you have to look if you allow the user droping
//***
//*** created from: RRO				date: 01.09.00
//***
//*** changes:
//*** vers.		date		who		what
//***	0.1		12.10.00	RRO		now it's the container who controls!
//**********************************************************
DROPEFFECT CUGanttCtrl::OnDragOver(CWnd* pWnd, COleDataObject* pDataObject, DWORD dwKeyState, CPoint point)
{

	HGLOBAL gmem  = pDataObject->GetGlobalData(CF_GANTT);
	CString olKey = LPCTSTR(GlobalLock(gmem));

  CRect olRect;
  olRect = omGanttRect;

  // Handle auto scroll:
  if (point.y >= (olRect.bottom - m_nScrollInset))
  {
    //Mouse moved to bottom border: Start scrolling down if not already done:
    if (m_AutoScrollDirection != SCROLL_DOWN)
    {
      m_nScrollTime = m_nScrollDelay / 10;
      m_AutoScrollDirection = SCROLL_DOWN;
    }
  }
  else if (point.y <= olRect.top + m_nScrollInset)
  {
    //Mouse moved to top border: Start scrolling up if not already done:
    if (m_AutoScrollDirection != SCROLL_UP)
    {
      m_nScrollTime = m_nScrollDelay / 10;
      m_AutoScrollDirection = SCROLL_UP;
    }
  }
  else
  {
    // Stop auto scroll if mouse moved elsewhere
    m_AutoScrollDirection = SCROLL_NOSCROLL;
  }

	FireOnDragOver (_bstr_t (LPCTSTR (olKey)), imCurrentHighlightLine);

	HighlightLine  (point);	//it is for better orientation...

	DROPEFFECT olDropEffect = DROPEFFECT_NONE;
	if (m_CanAcceptDrop)
	{
		if (m_enableDragDrop == TRUE)
		{
			olDropEffect = (dwKeyState & MK_CONTROL) ? DROPEFFECT_COPY : DROPEFFECT_MOVE;

			GlobalUnlock (gmem);
			GlobalFree   (gmem);
		}
		else
		{
			olDropEffect = DROPEFFECT_NONE;
		}
	}

	if (!pDataObject->IsDataAvailable (CF_GANTT)) 
	{
		return DROPEFFECT_NONE;
	}    

	return olDropEffect;
}

//**********************************************************
//*** this is on the end of drag&drop;
//*** you have to look what to do with the dropped "thing"
//***
//*** created from: RRO				date: 01.09.00
//***
//*** changes:
//*** vers.		date		who		what
//***
//**********************************************************
BOOL CUGanttCtrl::OnDrop(CWnd* pWnd, COleDataObject* pDataObject, DROPEFFECT dropEffect, CPoint point)
{
  m_AutoScrollDirection = SCROLL_NOSCROLL;

	HighlightLine (point);
	HGLOBAL gmem  = pDataObject->GetGlobalData (CF_GANTT);
	CString olKey = LPCTSTR (GlobalLock (gmem));

	FireOnDrop (_bstr_t (LPCTSTR (olKey)), imCurrentHighlightLine);

	return TRUE;
}

//**********************************************************
//*** this is during drag&drop;
//*** you have to look what to do when leaving the window...
//***
//*** created from: RRO				date: 01.09.00
//***
//*** changes:
//*** vers.		date		who		what
//***
//**********************************************************
void CUGanttCtrl::OnDragLeave(CWnd* pWnd)
{
	m_CanAcceptDrop = false;
	FireOnDragLeave (_bstr_t (LPCTSTR (omCurrBarKey)));
}

//**********************************************************
//*** this is for the container: it can forbid drag&drop
//***
//*** created from: RRO				date: 01.09.00
//***
//*** changes:
//*** vers.		date		who		what
//***
//**********************************************************
void CUGanttCtrl::OnEnableDragDropChanged() 
{
	SetModifiedFlag();
}

//---------------------------------
//------- Get & Set Helper
//---------------------------------

//**********************************************************
//*** the following two methods are for the sensitive area;
//*** their size is relevant for resizing the (bk)bars
//***
//*** created from: RRO				date: 04.09.00
//***
//*** changes:
//*** vers.		date		who		what
//***
//**********************************************************
long CUGanttCtrl::GetResizeAreaWidth() 
{
	return ResizeAreaWidth;
}
void CUGanttCtrl::SetResizeAreaWidth(long nNewValue) 
{
	ResizeAreaWidth = nNewValue;
	SetModifiedFlag();
}

//**********************************************************
//*** the following two methods are for the minimal resize
//*** width of the (bk)bars; you can't reduce the duration 
//*** of a bar any more when this size is reached
//***
//*** created from: RRO				date: 04.09.00
//***
//*** changes:
//*** vers.		date		who		what
//***
//**********************************************************
long CUGanttCtrl::GetResizeMinimalWidth() 
{
	return ResizeMinimalWidth;
}
void CUGanttCtrl::SetResizeMinimalWidth(long nNewValue) 
{
	ResizeMinimalWidth = nNewValue;
	SetModifiedFlag();
}

//**********************************************************
//*** returns the number of the actually visible lines;
//*** this depends on the position of the vScrollbar and
//*** the individual lineheight saved in omTabLineHeight 
//***
//*** created from: RRO				date: 04.09.00
//***
//*** changes:
//*** vers.		date		who		what
//***	0.1		20.11.00	RRO		always one line too much
//***								=> "... - 1" at the end
//***
//**********************************************************
int CUGanttCtrl::GetNumberOfVisibleLines()
{
	int ilNumber = 0;
	int ilPos	 = pomVScroll->GetScrollPos();
	int ilRest	 = omGanttRect.bottom - omGanttRect.top;

	while (ilRest > 0 && ((ilNumber+ilPos) < omLines.GetSize()))
	{
		ilRest -= omTabLineHeight.GetAt(ilNumber + ilPos);
		ilNumber++;
	}
	return ilNumber - 1;
}

//**********************************************************
//*** calculates the top of the line
//*** DrawBarsOfLine needs it for the refresh-rect for
//*** fast redrawing. The result is in "imCurrYTop".
//***
//*** created from: RRO				date: 05.09.00
//***
//*** changes:
//*** vers.		date		who		what
//***
//**********************************************************
void CUGanttCtrl::CalcTopOfLine(long lpLineNo)
{
	imRefreshY = omGanttRect.top;
	for (int i = pomVScroll->GetScrollPos(); i < lpLineNo; i++)
	{
		imRefreshY += omTabLineHeight.GetAt(i);
	}
}

//**********************************************************
//*** refreshes one line of the GanttArea
//***
//*** created from: RRO				date: 05.09.00
//***
//*** changes:
//*** vers.		date		who		what
//***
//**********************************************************
void CUGanttCtrl::RefreshGanttLine(long LineNo) 
{
	if (omBarMode == "NORMAL")
	{
		imRefreshY		= GetTopOfLine (LineNo);
		imCurrBarLine	= LineNo;
	}
	bmDrawALL		= FALSE;
	bmDrawGLINE		= TRUE;
 	Invalidate();
}

//**********************************************************
//*** loop again and again until there is no more bar in the
//*** overlapping found.
//***
//*** created from: RRO				date: 06.09.00
//***
//*** changes:
//*** date		who		what
//*** 26.08.02	rro		- removing unnecessary stuff
//***					- returning sorted array by start-time
//***
//**********************************************************
void CUGanttCtrl::GetOverlappedBarsFromTime (long lpLineNo, COleDateTime opTime1, COleDateTime opTime2, CUIntArray &romBarNoList)
{
    LineData *polLine	= &omLines[lpLineNo];
	BarData* polBar		= NULL;
    int ilBarCount		= polLine->omBars.GetSize();
	int i;

	// Prepare the empty array of the involvement flag for each bar
	CCSPtrArray<BOOL> IsInvolvedBar;
	for (i = 0; i < ilBarCount; i++)
	{
		IsInvolvedBar.NewAt(i, FALSE);
	}

	// The overlapping group is at first determine by the given "opTime1" and "opTime2".
	// But if the bar we find is exceed this period of time, extend searching to both 
	// left side and right side in the next round.
	for (i = 0; i < ilBarCount; i++)
	{
		polBar = &polLine->omBars[i];
		BOOL blIsOverlapped			= IsReallyOverlapped (polBar->Begin, polBar->End, opTime1, opTime2);
		BOOL blIsWithIn				= IsWithIn (polBar->Begin, polBar->End, opTime1, opTime2);// this IsWithIn() fix the real thin bar bug
		BOOL blIsInvolved			= (blIsOverlapped || blIsWithIn);

		if (IsInvolvedBar[i] == FALSE && blIsInvolved)	// some more new involved bars?
		{
			IsInvolvedBar[i] = TRUE;
			opTime1 = min(opTime1, polBar->Begin);
			opTime2 = max(opTime2, polBar->End);
			i = -1; // we have to start again, the timeframe grew
		}
	}

	// Create the list of involved bars, then store them to "romBarNoList"
	// We will return "romBarNoList" with bar-positions sorted by their start-time
	romBarNoList.RemoveAll();
	bool blAdd;
	int j;
	for (i = 0; i < ilBarCount; i++)
	{
		if (IsInvolvedBar[i])
		{
			// insert sorted by the start-times
			blAdd = false;
			j = 0;
			while ((blAdd == false) && (j < romBarNoList.GetSize()))
			{
				if (m_overlappingRightBarTop == FALSE)
				{
					if (polLine->omBars[i].Begin < polLine->omBars[romBarNoList[j]].Begin)
					{
						blAdd = true;
						romBarNoList.InsertAt (j, i);
					}
				}
				else
				{
					if (polLine->omBars[i].Begin > polLine->omBars[romBarNoList[j]].Begin)
					{
						blAdd = true;
						romBarNoList.InsertAt (j, i);
					}
				}
				j++;
			}
			if (blAdd == false)
			{
				romBarNoList.Add(i);
			}
		}
	}
	IsInvolvedBar.DeleteAll();
}

//**********************************************************
//*** returns the maximal overlap level of the line
//***
//*** created from: RRO				date: 06.09.00
//***
//*** changes:
//*** vers.		date		who		what
//***
//**********************************************************
int CUGanttCtrl::GetMaxOverlapLevel(long lpLineNo)
{
	if (lpLineNo > -1 && lpLineNo < omLines.GetSize())
	{
		return omLines[lpLineNo].MaxOverlapLevel;
	}
	else
	{
		return 0;
	}
}

//**********************************************************
//*** Handles the overlap level of the specified bar.
//*** When you have moved (horizontal) or droped (vertical)
//*** one bar, you have to re-init the overlap level of all
//*** involved bars.
//***
//*** created from: RRO				date: 06.09.00
//***
//*** changes:
//*** date		who		what
//*** 26.08.02	rro		- correcting all
//***
//**********************************************************
void CUGanttCtrl::HandleOverlapLevelOfBar(CString olKey, BOOL blTop)
{
	CUIntArray olBarNoList;

	long llLineNo = GetBarLineByKey (olKey);

	BarData *polBar = NULL;
	BarData *polLastBar = NULL;
	int ilDecreaseOverlapOffset = 0;
	int ilPos;
	int ilMaxGrpPos;

	LineData *polLine;

	if (llLineNo > -1)
	{
		polLine = &omLines[llLineNo];
		ilPos			= GetBarPosInLine (olKey, llLineNo);
		ilMaxGrpPos		= ilPos;
		BarData *polBar	= &polLine->omBars[ilPos];

		// getting all involved bars
		GetOverlappedBarsFromTime (llLineNo, polBar->Begin, polBar->End, olBarNoList);
		int ilListCount	= olBarNoList.GetSize();

		for (int i = 0; i < ilListCount; i++)
		{
			polBar = &polLine->omBars[olBarNoList[i]];
			ilMaxGrpPos = max(ilMaxGrpPos, GetBarPosInLine (polBar->Key, llLineNo));
			if (polLastBar != NULL)
			{
				if (!DoBarsOverlap(polLastBar, polBar))
				{
					ilDecreaseOverlapOffset++;
				}
			}

			polBar->OverlapLevel = i - ilDecreaseOverlapOffset;

			for (int j = 0; j < polBar->omSubBars.GetSize(); j++)
			{
				polBar->omSubBars[j].OverlapLevel = polBar->OverlapLevel + 1;
			}

			polLastBar = polBar;
		}

		if (blTop == TRUE)
		{
			// we have to put the bar we have to check to the top.
			// so it must be the last bar in this group in the array
			if (ilListCount > 1)
			{
				// handle the bar
				BarData *polOldBar = &polLine->omBars[ilPos];
				BarData *polNewBar = NULL;
				BarData *polSubBar = NULL;
				polNewBar = polOldBar;
				polLine->omBars.NewAt (ilMaxGrpPos + 1, *polNewBar);
				polLine->omBars.DeleteAt (ilPos);
				imCurrBarPos = ilMaxGrpPos;
				pomBar = &polLine->omBars[ilMaxGrpPos];
				mapBars.SetAt (pomBar->Key, pomBar);
				
				// handle the sub-bars of the bar
				m_MoveSubBarsOldBegin.DeleteAll();
				m_MoveSubBarsOldEnd.DeleteAll();
				for (int ilCntSubBar = 0; ilCntSubBar < pomBar->omSubBars.GetSize(); ilCntSubBar++)
				{
					COleDateTime *polBegin = new COleDateTime();
					COleDateTime *polEnd   = new COleDateTime();
					polSubBar = &pomBar->omSubBars[ilCntSubBar];
					*polBegin = polSubBar->Begin;
					*polEnd = polSubBar->End;
					m_MoveSubBarsOldBegin.Add (polBegin);
					m_MoveSubBarsOldEnd.Add (polEnd);
					mapSubBars.SetAt((const char*)polSubBar->Key.GetBuffer(0), (void*)polSubBar);
				}
			}
		}
	}
}

//**********************************************************
//*** Handles the overlap level of the specified time frame.
//*** This functions detects all bars belonging to the time
//*** window and handles their overlapping.
//*** 
//***
//*** created from: RRO				date: 26.08.02
//***
//*** changes:
//*** date		who		what
//***
//**********************************************************
void CUGanttCtrl::HandleOverlapLevelOfLineAndTime(long lpLineNo, COleDateTime opTimeStart, COleDateTime opTimeEnd)
{
	CUIntArray olBarNoList;

	BarData *polBar = NULL;

	LineData *polLine;

	if (lpLineNo > -1 && lpLineNo < omLines.GetSize())
	{
		polLine = &omLines[lpLineNo];

		// getting all involved bars
		GetOverlappedBarsFromTime (lpLineNo, opTimeStart, opTimeEnd, olBarNoList);
		int ilListCount	= olBarNoList.GetSize();

		for (int i = 0; i < ilListCount; i++)
		{
			polBar = &polLine->omBars[olBarNoList[i]];
			HandleOverlapLevelOfBar (polBar->Key, FALSE);
		}
	}
}

//**********************************************************
//*** Handles the overlap level of the specified line.
//*** The MaxOverlapLevel of the line is the highest
//*** overlap level of its bars.
//***
//*** created from: RRO				date: 06.09.00
//***
//*** changes:
//*** date		who		what
//*** 03.09.02	rro		looking for the max. overlap-level
//***					only in the visible area
//***
//**********************************************************
void CUGanttCtrl::HandleOverlapLevelOfLine(long lpLineNo)
{
	int ilLines = omLines.GetSize();
	int ilLevel;
	int ilOldLevel;
	COleDateTime olStartTime = pomTimeScale->GetDisplayStartTime ();
	COleDateTime olEndTime = olStartTime + pomTimeScale->GetDisplayDuration();

	if (lpLineNo > -1 && lpLineNo < ilLines)
	{
		LineData *polLine	= &omLines[lpLineNo];
		BarData *polBar		= NULL;
		ilOldLevel			= polLine->MaxOverlapLevel;
		polLine->MaxOverlapLevel = 0;

		for (int i = 0; i < polLine->omBars.GetSize(); i++)
		{
			polBar = &polLine->omBars[i];

			if (!m_autoSizeLineHeightToFit
				|| IsBetween(polBar->Begin, olStartTime, olEndTime)
				|| IsBetween(polBar->End, olStartTime, olEndTime))
			{
				ilLevel = polBar->OverlapLevel;
				polLine->MaxOverlapLevel = max(polLine->MaxOverlapLevel, ilLevel);
			}
		}

		// handle the line-hight only if something changed
		if (polLine->MaxOverlapLevel != ilOldLevel)
		{
			HandleLineHight(lpLineNo);
		}
	}
}

void CUGanttCtrl::HandleOverlapLevelOfVisibleLines()
{
	int ilStartIdx = 0;
	if (pomVScroll != NULL)
	{
		ilStartIdx = pomVScroll->GetScrollPos ();
	}

	int ilEndIdx = __min(TabLines, ilStartIdx + imMaxVisibleLines) + 1;

	for (int i = ilStartIdx; i < ilEndIdx; i++)
	{
		HandleOverlapLevelOfLine(i);
	}
}

//**********************************************************
//*** Handles the height of the specified line.
//*** It depends on the MaxOverlapLevel of the line.
//*** There is also a reserve: every X level-steps the
//*** linehight will increase ("X" means set by the container)
//***
//*** created from: RRO				date: 06.09.00
//***
//*** changes:
//*** vers.		date		who		what
//***	0.1		07.09.00	RRO		variable offset
//**********************************************************
void CUGanttCtrl::HandleLineHight(long lpLineNo)
{

	LineData *polLine = &omLines[lpLineNo];

	int ilOldHeight = omTabLineHeight[lpLineNo];	//this is the actual line height
	int ilNewHeight = TabLineHeight;				//this is the minimum line height for all
	if (polLine->MaxOverlapLevel > 0)
	{
		if (BarNumberExpandLine > 0)
		{
			ilNewHeight += ((div(polLine->MaxOverlapLevel,BarNumberExpandLine).quot + 1) *
				(BarNumberExpandLine * BarOverlapOffset));
		}
		else
		{
			ilNewHeight += (polLine->MaxOverlapLevel) * BarOverlapOffset;
		}
	}

	omTabLineHeight[lpLineNo] = ilNewHeight + omLines[lpLineNo].LineSeparatorHeightComplete;
	imMaxVisibleLines		  = GetNumberOfVisibleLines ();
}

//**********************************************************
//*** example:
//*** for max bar overlap level 0,1,2 we will use the same lineheight,
//*** for max bar overlap level 3,4,5 we will use the same lineheight too,...
//*** The no. of overlap levels of the same line height
//*** you can set in "BarNumberExpandLine"
//***
//*** created from: RRO				date: 07.09.00
//***
//*** changes:
//*** vers.		date		who		what
//***
//**********************************************************
short CUGanttCtrl::GetBarNumberExpandLine() 
{
	return BarNumberExpandLine;
}
void CUGanttCtrl::SetBarNumberExpandLine(short nNewValue) 
{
	BarNumberExpandLine = nNewValue;
	for (int i = 0; i < omLines.GetSize(); i++)
	{
		HandleLineHight (i);
	}

	SetModifiedFlag();
}

//**********************************************************
//*** this is the offset for overlapping bars
//*** that means, how many pixel are between the top(s) of
//*** two (ore more) overlapping bars
//***
//*** created from: RRO				date: 07.09.00
//***
//*** changes:
//*** vers.		date		who		what
//***
//**********************************************************
short CUGanttCtrl::GetBarOverlapOffset() 
{
	return BarOverlapOffset;
}
void CUGanttCtrl::SetBarOverlapOffset(short nNewValue) 
{
	BarOverlapOffset = nNewValue;
	for (int i = 0; i < omLines.GetSize(); i++)
	{
		HandleLineHight (i);
	}
	SetModifiedFlag();
}

//**********************************************************
//*** if the container allows to change bar's date
//***
//*** created from: RRO				date: 16.10.00
//***
//*** changes:
//*** date		who		what
//*** 26.08.02	rro		we have to check the overlap-level
//***					of the old times of the bar
//***
//**********************************************************
void CUGanttCtrl::SetBarDate(LPCTSTR Key, DATE Begin, DATE Ende, BOOL HandleOverlap) 
{
	long llLine = GetBarLineByKey (Key);

	if (llLine > -1)
	{
		BarData *polBar = GetBarByKey (Key);
		if (polBar != NULL)
		{
			// remember the old times
			COleDateTime olOldStart = polBar->Begin;
			COleDateTime olOldEnd	= polBar->End;

			//handle the new times of the bar
			polBar->Begin	= Begin;
			polBar->End		= Ende;

			if (HandleOverlap)
			{
				//handle the old times of the bar
				HandleOverlapLevelOfLineAndTime (llLine, olOldStart, olOldEnd);

				//handle the new times of the bar
				HandleOverlapLevelOfBar (Key, TRUE);

				// handle the overlap-level of the line
				HandleOverlapLevelOfLine (llLine);
			}

			// updates bar-rect for drawing arrows
			UpdateSingleRect (Key);

			// update for setting the focus-rect
			omCurrBarRect.left  = GetXFromTime (Begin);
			omCurrBarRect.right = GetXFromTime (Ende);

			// look if we have to move the sub-bars too
			if (m_autoMoveSubBarsWithBar == TRUE)
			{
				COleDateTimeSpan olDiff = polBar->Begin - olOldStart;
				for (int i = 0; i < polBar->omSubBars.GetSize(); i++)
				{
					polBar->omSubBars[i].Begin += olDiff;
					polBar->omSubBars[i].End += olDiff;
				}
			}
		}
	}
}

//**********************************************************
//*** if the container allows to change backbar's date
//***
//*** created from: RRO				date: 16.10.00
//***
//*** changes:
//*** vers.		date		who		what
//***
//**********************************************************
void CUGanttCtrl::SetBkBarDate(LPCTSTR Key, DATE Begin, DATE Ende) 
{
	long llLine, llPos;
	llLine = GetBkBarLineByKey (Key);
	if (llLine > -1)
	{
		llPos = GetBkBarPosInLine (Key, llLine);
		omLines[llLine].omBackBars[llPos].Begin = Begin;
		omLines[llLine].omBackBars[llPos].End = Ende;
	}
}

//**********************************************************
//*** sets the background color of a bar
//***
//*** created from: RRO				date: 16.10.00
//***
//*** changes:
//*** vers.		date		who		what
//***
//**********************************************************
void CUGanttCtrl::SetBarColor(LPCTSTR Key, long Color) 
{
	long llLine, llPos;
	llLine = GetBarLineByKey (Key);
	if (llLine > -1)
	{
		llPos = GetBarPosInLine(Key, llLine);
		omLines[llLine].omBars[llPos].BackColor  = Color;
		omLines[llLine].omBars[llPos].SplitColor = FALSE;
	}
}

//**********************************************************
//*** sets the background color of a background bar
//***
//*** created from: RRO				date: 16.10.00
//***
//*** changes:
//*** vers.		date		who		what
//***
//**********************************************************
void CUGanttCtrl::SetBkBarColor(LPCTSTR Key, long Color) 
{
	long llLine, llPos;
	llLine = GetBkBarLineByKey (Key);
	if (llLine > -1)
	{
		llPos = GetBkBarPosInLine (Key, llLine);
		omLines[llLine].omBackBars[llPos].BackColor = Color;
	}
}

//**********************************************************
//*** sets the text color of a bar
//***
//*** created from: RRO				date: 16.10.00
//***
//*** changes:
//*** vers.		date		who		what
//***
//**********************************************************
void CUGanttCtrl::SetBarTextColor(LPCTSTR Key, long Color) 
{
	long llLine, llPos;
	llLine = GetBarLineByKey (Key);
	if (llLine > -1)
	{
		llPos = GetBarPosInLine(Key, llLine);
		omLines[llLine].omBars[llPos].TextColor = Color;
	}
}

//**********************************************************
//*** sets the text color of a  background bar
//***
//*** created from: RRO				date: 16.10.00
//***
//*** changes:
//*** vers.		date		who		what
//***
//**********************************************************
void CUGanttCtrl::SetBkBarTextColor(LPCTSTR Key, long Color) 
{
	long llLine, llPos;
	llLine = GetBkBarLineByKey (Key);
	if (llLine > -1)
	{
		llPos = GetBkBarPosInLine (Key, llLine);
		omLines[llLine].omBackBars[llPos].TextColor = Color;
	}
}



//**********************************************************
//*** delivers the top of a line
//***
//*** created from: ???				date: ???
//***
//*** changes:
//*** vers.		date		who		what
//***	0.1		28.11.00	RRO		if the line is on top of the visible area,
//***								the return value must be negative
//***
//**********************************************************
int CUGanttCtrl::GetTopOfLine(long LineNo)
{
	int ilPos = pomVScroll->GetScrollPos();
	int ilRet = omGanttRect.top;;
	if (LineNo > ilPos)
	{
		for (int i = pomVScroll->GetScrollPos(); i < LineNo; i++)
		{
			ilRet += omTabLineHeight.GetAt(i);
		}
	}
	else
	{
		while (ilPos > LineNo)
		{
			ilPos--;
			ilRet -= omTabLineHeight.GetAt(ilPos);
		}
	}
	return ilRet;
}

//**********************************************************
//*** draws all bars of the line on the bitmap
//***
//*** created from: RRO				date: 01.11.00
//***
//*** changes:
//*** vers.		date		who		what
//***
//**********************************************************
void CUGanttCtrl::DrawBarsOfLine(CBitmap &romBitmap, long lpLineNo, int ipCurrY)
{
	int ilFromX;
	int ilToX;
	if ((romBitmap.m_hObject	== NULL) || (lpLineNo < 0))
		return;

	BarData  *polBar  = NULL;
	LineData *polLine = NULL;
	polLine = &omLines[lpLineNo];

	int ilBarHeight = (int)(TabLineHeight/(m_subbarStackCount+1)) - 2;

	if (polLine != NULL)
	{
		//do some calculations to get the position of the line you want to draw
		CRect olBarRect;
		CRect olRect	= omGanttRect;
		olRect.top		= ipCurrY;
		olRect.bottom	= olRect.top + omTabLineHeight.GetAt (lpLineNo);

		CClientDC dc (this);
		CDC dcMem;
		dcMem.CreateCompatibleDC (&dc);
		dcMem.SelectObject (&romBitmap);
		
		CBrush olBlackBrush	  (COLORREF (RGB (  0,  0,  0)));
		
		CPen   olBlackPen	  (PS_SOLID, 1, COLORREF (RGB (0,0,0)));
		CPen   olGrayPen	  (PS_SOLID, 1, COLORREF (RGB (128,128,128)));
		CPen   olWhitePen	  (PS_SOLID, 1, COLORREF (RGB (255,255,255)));
		
		CPen	*pOldPen	= dcMem.GetCurrentPen   ();
		CFont	*pOldFont	= dcMem.SelectObject    (&omTabFont);
		CBrush	*pOldBrush	= dcMem.GetCurrentBrush ();
		
		//draw left and right outside texts for each Bar
		for (int i = 0; i < polLine->omBars.GetSize(); i++)
		{
			polBar = &polLine->omBars[i];

			int ilOverlapLevelBar  = polBar->OverlapLevel;
			int ilOverlapLevelLine = polLine->MaxOverlapLevel;

			//calculate the bar rect (top and bottom); it depends on the overlap-level!
			olBarRect		  = olRect;
			olBarRect.top	 += (2 + ilOverlapLevelBar * BarOverlapOffset);	//at least it is 2
			olBarRect.bottom  = olBarRect.top + TabLineHeight - 2;

			// Are you moving or sizing this Bar ???
			if ((omBarMode != "NORMAL") && 
				(imCurrBarPos == i) && 
				(imCurrBarLine == lpLineNo) &&
				(bmFoundBar == TRUE))
			{
				//Yes: Draw it's left/right outside texts at their old position
				ilFromX = omGanttRect.left + pomTimeScale->GetXFromTime(m_dMoveBarOldBegin);
				ilToX = omGanttRect.left + pomTimeScale->GetXFromTime(m_dMoveBarOldEnd);
			}
			else
			{
				//No: Draw it's left/right outside texts at their normal position
				ilFromX = omGanttRect.left + pomTimeScale->GetXFromTime (polBar->Begin);
				ilToX	= omGanttRect.left + pomTimeScale->GetXFromTime (polBar->End);
			}
			
			if (ilFromX < omGanttRect.left)
				ilFromX = omGanttRect.left;
			if (ilToX   > omGanttRect.right) 
				ilToX   = omGanttRect.right;

			//calculate the bar rect (left and right)
			if (IsOverlapped(ilFromX, ilToX, omGanttRect.left, omGanttRect.right))
			{
				olBarRect.left	  = ilFromX;
				olBarRect.right   = ilToX;

				// Draw left outside text if there is any
				if (polBar->LeftOutsideText.Trim() != "")
				{
					CRect olTextRect = olBarRect;
					olTextRect.right = olTextRect.left  - 2;
					olTextRect.left	 = olTextRect.right - olBarRect.Width();
					dcMem.SetBkMode (TRANSPARENT);
					dcMem.SetTextColor (polBar->LeftTextColor);
					dcMem.DrawText (polBar->LeftOutsideText, olTextRect, DT_RIGHT | DT_VCENTER | DT_SINGLELINE);
				}
				
				// Draw right outside text if there is any
				if (polBar->RightOutsideText.Trim() != "")
				{
					CRect olTextRect = olBarRect;
					olTextRect.left  = olTextRect.right + 2;
					olTextRect.right = olTextRect.left  + olBarRect.Width();
					dcMem.SetBkMode (TRANSPARENT);
					dcMem.SetTextColor (polBar->RightTextColor);
					dcMem.DrawText (polBar->RightOutsideText, olTextRect, DT_LEFT | DT_VCENTER | DT_SINGLELINE);
				}
			}
		}

		COLORREF olOldTextColor = dcMem.GetTextColor();

		DECORATION_OBJECT *polDecoObj = NULL;
		DECORATION_OBJECT *polTriangleDecoObj = NULL;

		//UINT textAllignment = DT_CENTER | DT_VCENTER | DT_SINGLELINE;
		UINT textAllignment = DT_LEFT | DT_VCENTER | DT_SINGLELINE;
		//draw bar for bar in the line
		for (int i = 0; i < polLine->omBars.GetSize(); i++)
		{
			polBar = &polLine->omBars[i];

			int ilOverlapLevelBar  = polBar->OverlapLevel;
			int ilOverlapLevelLine = polLine->MaxOverlapLevel;

			//calculate the bar rect (top and bottom); it depends on the overlap-level!
			olBarRect		  = olRect;
			olBarRect.top	 += (2 + ilOverlapLevelBar * BarOverlapOffset);	//at least it is 2
			ilBarHeight = (int)(TabLineHeight/(m_subbarStackCount+1)) - 2;
			olBarRect.bottom  = olBarRect.top + ilBarHeight;

			// Are you moving or sizing this Bar ???
			if ((omBarMode != "NORMAL") && 
				(imCurrBarPos == i) && 
				(imCurrBarLine == lpLineNo) &&
				(bmFoundBar == TRUE))
			{
				//Yes: Draw it at it's old position
				ilFromX = omGanttRect.left + pomTimeScale->GetXFromTime(m_dMoveBarOldBegin);
				ilToX = omGanttRect.left + pomTimeScale->GetXFromTime(m_dMoveBarOldEnd);
			}
			else
			{
				//No: Draw it at it's normal position
				ilFromX = omGanttRect.left + pomTimeScale->GetXFromTime (polBar->Begin);
				ilToX	= omGanttRect.left + pomTimeScale->GetXFromTime (polBar->End);
			}

			if (ilFromX < omGanttRect.left)
				ilFromX = omGanttRect.left;
			if (ilToX   > omGanttRect.right) 
				ilToX   = omGanttRect.right;

			//calculate the bar rect (left and right)
			if (IsOverlapped(ilFromX, ilToX, omGanttRect.left, omGanttRect.right))
			{
				olBarRect.left	  = ilFromX;
				olBarRect.right   = ilToX;

				//draw the body of the bar
				CBrush olBrush;
				if (polBar->SplitColor != TRUE)
				{
					olBrush.CreateSolidBrush (polBar->BackColor);
					dcMem.FillRect (&olBarRect, &olBrush);
				}
				else
				{
					//draw the left half
					CRect olHalfRect = olBarRect;
					olHalfRect.right = olHalfRect.left  + (olBarRect.Width() / 2);
					olBrush.CreateSolidBrush (polBar->LeftBackColor);
					dcMem.FillRect (&olHalfRect, &olBrush);
					//draw the right half
					olHalfRect.right = olBarRect.right;
					olHalfRect.left	 = olHalfRect.right - (olBarRect.Width() / 2) - 1;
					olBrush.DeleteObject();
					olBrush.CreateSolidBrush (polBar->RightBackColor);
					dcMem.FillRect (&olHalfRect, &olBrush);
				}

				polDecoObj = NULL;
				polTriangleDecoObj = NULL;
				mapBarDecoObjects.Lookup (polBar->Key.GetBuffer(0), (void*&)polDecoObj);
				mapBarTriangleObjects.Lookup (polBar->Key.GetBuffer(0), (void*&)polTriangleDecoObj);

				switch (polBar->Shape)
				{
				case NORMAL:
					{
						dcMem.FrameRect (&olBarRect, &olBlackBrush);
						dcMem.SetBkMode (TRANSPARENT);
						if(polDecoObj != NULL)
						{
							DrawDecoObject(&dcMem, polDecoObj, olBarRect);
						}
						if(polTriangleDecoObj != NULL)
						{
							DrawDecoObject(&dcMem, polTriangleDecoObj, olBarRect);
						}
						dcMem.SetTextColor (polBar->TextColor);
						
						dcMem.DrawText (polBar->BarText, olBarRect, textAllignment);
					}
					break;
				case RAISED:
					{
						dcMem.SelectObject (&olWhitePen);
						dcMem.MoveTo (olBarRect.left,  olBarRect.bottom - 1);
						dcMem.LineTo (olBarRect.left,  olBarRect.top);
						dcMem.LineTo (olBarRect.right, olBarRect.top);
						
						dcMem.SelectObject (&olGrayPen);
						dcMem.MoveTo (olBarRect.left  + 1, olBarRect.bottom - 2);
						dcMem.LineTo (olBarRect.right - 1, olBarRect.bottom - 2);
						dcMem.LineTo (olBarRect.right - 1, olBarRect.top);
						
						dcMem.SelectObject (&olBlackPen);
						dcMem.MoveTo (olBarRect.left,  olBarRect.bottom - 1);
						dcMem.LineTo (olBarRect.right, olBarRect.bottom - 1);
						dcMem.LineTo (olBarRect.right, olBarRect.top);
						
						dcMem.SetBkMode (TRANSPARENT);
						if(polDecoObj != NULL)
						{
							DrawDecoObject(&dcMem, polDecoObj, olBarRect);
						}
						if(polTriangleDecoObj != NULL)
						{
							DrawDecoObject(&dcMem, polTriangleDecoObj, olBarRect);
						}
						dcMem.SetTextColor (polBar->TextColor);
						dcMem.DrawText (polBar->BarText, olBarRect, textAllignment);
					}
					break;
				case INSET:
					{
						dcMem.SelectObject (&olGrayPen);
						dcMem.MoveTo (olBarRect.left,  olBarRect.bottom - 1);
						dcMem.LineTo (olBarRect.left,  olBarRect.top);
						dcMem.LineTo (olBarRect.right, olBarRect.top);
						
						dcMem.SelectObject (&olBlackPen);
						dcMem.MoveTo (olBarRect.left  + 1, olBarRect.bottom - 1);
						dcMem.LineTo (olBarRect.left  + 1, olBarRect.top + 1);
						dcMem.LineTo (olBarRect.right - 1, olBarRect.top + 1);
						
						dcMem.SelectObject (&olWhitePen);
						dcMem.MoveTo (olBarRect.left + 1, olBarRect.bottom - 1);
						dcMem.LineTo (olBarRect.right,    olBarRect.bottom - 1);
						dcMem.LineTo (olBarRect.right,    olBarRect.top - 1);
						
						dcMem.SetTextColor (polBar->TextColor);
						dcMem.SetBkMode (TRANSPARENT);
						if(polDecoObj != NULL)
						{
							DrawDecoObject(&dcMem, polDecoObj, olBarRect);
						}
						if(polTriangleDecoObj != NULL)
						{
							DrawDecoObject(&dcMem, polTriangleDecoObj, olBarRect);
						}
						dcMem.DrawText (polBar->BarText, olBarRect, textAllignment);
					}
					break;
				default:
					break;
				}
			}

			//===========================================
			//MWO: 19.08.2002 Subbar functionality
			if (m_subbarStackCount > 0) //else we'd have devision by zero
			{
				for (int j = 0; j < polBar->omSubBars.GetSize(); j++)
				{
					BarData *polSubbar;
					polSubbar = &polBar->omSubBars[j];

					int ilOverlapLevelBar  = polSubbar->OverlapLevel;
					int ilOverlapLevelLine = polLine->MaxOverlapLevel;

					//===========================================
					// calculate the bar rect (top and bottom); it depends on the overlap-level!
					CRect olSubBarRect = olRect;

					// the top of the sub-bar must be below the bottom of its main-bar.
					// calculating the bottom of its main-bar:
					olSubBarRect.top   += (((ilOverlapLevelBar - 1) * BarOverlapOffset) + ilBarHeight);

					// calculating an offset in dependancy of the general line-height:
					olSubBarRect.top   += (int)(TabLineHeight/8);

					// calculating the bottom of the sub-bar
					olSubBarRect.bottom = (int)(olSubBarRect.top + ilBarHeight - (int)(TabLineHeight/8));
					if (olSubBarRect.bottom > olRect.bottom - (int)(TabLineHeight / 10))
					{
						olSubBarRect.bottom = olRect.bottom - (int)(TabLineHeight / 10);
					}

					// Are we in moving or sizing mode of this Bar ???
					if ((omBarMode != "NORMAL") && 
						(imCurrBarPos == i) && 
						(imCurrBarLine == lpLineNo) &&
						(bmFoundBar == TRUE))
					{
						//Yes: Draw it at it's old position
						if (j < m_MoveSubBarsOldBegin.GetSize() && j < m_MoveSubBarsOldEnd.GetSize())
						{
							ilFromX = omGanttRect.left + pomTimeScale->GetXFromTime (m_MoveSubBarsOldBegin[j]);
							ilToX	= omGanttRect.left + pomTimeScale->GetXFromTime (m_MoveSubBarsOldEnd[j]);
						}
						else
						{
							ilFromX = omGanttRect.left + pomTimeScale->GetXFromTime (polSubbar->Begin);
							ilToX	= omGanttRect.left + pomTimeScale->GetXFromTime (polSubbar->End);
						}
					}
					else
					{
						//No: Draw it at it's normal position
						ilFromX = omGanttRect.left + pomTimeScale->GetXFromTime (polSubbar->Begin);
						ilToX	= omGanttRect.left + pomTimeScale->GetXFromTime (polSubbar->End);
					}
					
					if (ilFromX < omGanttRect.left)
						ilFromX = omGanttRect.left;
					if (ilToX   > omGanttRect.right) 
						ilToX   = omGanttRect.right;
					
					//===========================================
					//calculate the bar rect (left and right)
					if (IsOverlapped(ilFromX, ilToX, omGanttRect.left, omGanttRect.right))
					{
						olSubBarRect.left	= ilFromX;
						olSubBarRect.right	= ilToX;
						
						//draw the body of the bar
						CBrush olBrush;
						if (polSubbar->SplitColor != TRUE)
						{
							olBrush.CreateSolidBrush (polSubbar->BackColor);
							dcMem.FillRect (&olSubBarRect, &olBrush);
						}
						else
						{
							//draw the left half
							CRect olHalfRect = olSubBarRect;
							olHalfRect.right = olHalfRect.left  + (olSubBarRect.Width() / 2);
							olBrush.CreateSolidBrush (polSubbar->LeftBackColor);
							dcMem.FillRect (&olHalfRect, &olBrush);
							//draw the right half
							olHalfRect.right = olSubBarRect.right;
							olHalfRect.left	 = olHalfRect.right - (olSubBarRect.Width() / 2) - 1;
							olBrush.DeleteObject();
							olBrush.CreateSolidBrush (polSubbar->RightBackColor);
							dcMem.FillRect (&olHalfRect, &olBrush);
						}

						// getting the decoration-object (if there is one assigned)
						polDecoObj = NULL;
						mapBarDecoObjects.Lookup (polSubbar->Key.GetBuffer(0), (void*&)polDecoObj);

						polTriangleDecoObj = NULL;
						mapBarTriangleObjects.Lookup (polBar->Key.GetBuffer(0), (void*&)polTriangleDecoObj);
						switch (polSubbar->Shape)
						{
						case NORMAL:
							{
								dcMem.FrameRect (&olSubBarRect, &olBlackBrush);
								dcMem.SetBkMode (TRANSPARENT);
								if(polDecoObj != NULL)
								{
									DrawDecoObject(&dcMem, polDecoObj, olSubBarRect);
								}
								if(polTriangleDecoObj != NULL)
								{
									DrawDecoObject(&dcMem, polTriangleDecoObj, olBarRect);
								}
								dcMem.SetTextColor (polSubbar->TextColor);
								dcMem.DrawText (polSubbar->BarText, olSubBarRect, textAllignment);
							}
							break;
						case RAISED:
							{
								dcMem.SelectObject (&olWhitePen);
								dcMem.MoveTo (olSubBarRect.left,  olSubBarRect.bottom - 1);
								dcMem.LineTo (olSubBarRect.left,  olSubBarRect.top);
								dcMem.LineTo (olSubBarRect.right, olSubBarRect.top);
								
								dcMem.SelectObject (&olGrayPen);
								dcMem.MoveTo (olSubBarRect.left  + 1, olSubBarRect.bottom - 2);
								dcMem.LineTo (olSubBarRect.right - 1, olSubBarRect.bottom - 2);
								dcMem.LineTo (olSubBarRect.right - 1, olSubBarRect.top);
								
								dcMem.SelectObject (&olBlackPen);
								dcMem.MoveTo (olSubBarRect.left,  olSubBarRect.bottom - 1);
								dcMem.LineTo (olSubBarRect.right, olSubBarRect.bottom - 1);
								dcMem.LineTo (olSubBarRect.right, olSubBarRect.top);
								
								dcMem.SetBkMode (TRANSPARENT);
								if(polDecoObj != NULL)
								{
									DrawDecoObject(&dcMem, polDecoObj, olSubBarRect);
								}
								if(polTriangleDecoObj != NULL)
								{
									DrawDecoObject(&dcMem, polTriangleDecoObj, olBarRect);
								}
								dcMem.SetTextColor (polSubbar->TextColor);
								dcMem.DrawText (polSubbar->BarText, olSubBarRect, textAllignment);
							}
							break;
						case INSET:
							{
								dcMem.SelectObject (&olGrayPen);
								dcMem.MoveTo (olSubBarRect.left,  olSubBarRect.bottom - 1);
								dcMem.LineTo (olSubBarRect.left,  olSubBarRect.top);
								dcMem.LineTo (olSubBarRect.right, olSubBarRect.top);
								
								dcMem.SelectObject (&olBlackPen);
								dcMem.MoveTo (olSubBarRect.left  + 1, olSubBarRect.bottom - 1);
								dcMem.LineTo (olSubBarRect.left  + 1, olSubBarRect.top + 1);
								dcMem.LineTo (olSubBarRect.right - 1, olSubBarRect.top + 1);
								
								dcMem.SelectObject (&olWhitePen);
								dcMem.MoveTo (olSubBarRect.left + 1, olSubBarRect.bottom - 1);
								dcMem.LineTo (olSubBarRect.right,    olSubBarRect.bottom - 1);
								dcMem.LineTo (olSubBarRect.right,    olSubBarRect.top - 1);
								
								dcMem.SetTextColor (polSubbar->TextColor);
								dcMem.SetBkMode (TRANSPARENT);
								if(polDecoObj != NULL)
								{
									DrawDecoObject(&dcMem, polDecoObj, olSubBarRect);
								}
								if(polTriangleDecoObj != NULL)
								{
									DrawDecoObject(&dcMem, polTriangleDecoObj, olBarRect);
								}
								dcMem.DrawText (polSubbar->BarText, olSubBarRect, textAllignment);
							}
							break;
						default:
							break;
						}
					}
				}
			}
			//END MWO: 19.08.2002 Subbar functionality
			//============================================
		} //for (i = 0; i < polLine->omBars.GetSize(); i++)

		dcMem.SetTextColor (olOldTextColor);
		dcMem.SelectObject (pOldPen);
		dcMem.SelectObject (pOldFont);
		dcMem.SelectObject (pOldBrush);
	}
}

//**********************************************************
//*** draws the entire backgroun
//***
//*** created from: MWO				date: 25.06.03
//***
//*** changes:
//*** vers.		date		who		what
//***
//**********************************************************
void CUGanttCtrl::DrawBodyBackGround(CBitmap &romBitmap)
{
	if (romBitmap.m_hObject	== NULL) 
		return;
	
	CClientDC dc (this);
	CDC dcMem;
	dcMem.CreateCompatibleDC (&dc);
	dcMem.SelectObject (&romBitmap);
	CRect olRect; //omGanttRect;
	GetClientRect(&olRect);
	CBrush	*pOldBrush = dcMem.GetCurrentBrush();
	CBrush brBackground(::GetSysColor(COLOR_BTNFACE)); // (RGB (192,192,192));
	if (m_lifeStyle == FALSE)
	{
		dcMem.FillRect (olRect, &brBackground);	
		dcMem.SelectObject (pOldBrush);
	}
	else
	{
		DrawLifeStyle(&dcMem, olRect, omLifeStyle.Body_ColorIdx, omLifeStyle.Body_Type, omLifeStyle.Body_StyleUp, omLifeStyle.Body_Percent);
	}
}

//**********************************************************
//*** draws the backbars of the line on the bitmap
//***
//*** created from: RRO				date: 01.11.00
//***
//*** changes:
//*** vers.		date		who		what
//***
//**********************************************************
void CUGanttCtrl::DrawBkBarsOfLine(CBitmap &romBitmap, long lpLineNo, int ipCurrY)
{
//	return;
	if (romBitmap.m_hObject	== NULL) 
		return;
	
	int ilFromX;
	int ilToX;

	CClientDC dc (this);
	CDC dcMem;
	dcMem.CreateCompatibleDC (&dc);
	dcMem.SelectObject (&romBitmap);

	CRect olRect	= omGanttRect;
	olRect.top		= ipCurrY;
	olRect.bottom	= olRect.top + omTabLineHeight.GetAt (lpLineNo);

	CPen	olRedP (PS_SOLID, 1, COLORREF (RGB (255,  0,  0)));
	CBrush	*pOldBrush = dcMem.GetCurrentBrush();

	//first delete the line you want to draw
	CBrush brBackground(::GetSysColor(COLOR_BTNFACE));// (RGB (192,192,192));

	//MWO: 25.06.03 ==> background drawing changed so commented out line below
	//dcMem.FillRect (olRect, &brBackground);	
	//END MWO: 25.06.03

	//don't get a conflict with the bkbars and the line-separators!
	if (omLines[lpLineNo].LineSeparatorHeightComplete > 0)
		olRect.bottom -= (omLines[lpLineNo].LineSeparatorHeightComplete);
	
	for (int i = 0; i < omLines[lpLineNo].omBackBars.GetSize(); i++)
	{
		CBrush olBkBrush (omLines[lpLineNo].omBackBars[i].BackColor);
		
		if ((omBarMode != "NORMAL") && 
			(imCurrBarPos == i) && 
			(imCurrBarLine == lpLineNo) &&
			(bmFoundBkBar == TRUE))
		{
			ilFromX = omGanttRect.left + pomTimeScale->GetXFromTime(m_dMoveBkBarOldBegin);
			ilToX = omGanttRect.left + pomTimeScale->GetXFromTime(m_dMoveBkBarOldEnd);
		}
		else
		{
			ilFromX = omGanttRect.left + pomTimeScale->GetXFromTime (omLines[lpLineNo].omBackBars[i].Begin);
			ilToX   = omGanttRect.left + pomTimeScale->GetXFromTime (omLines[lpLineNo].omBackBars[i].End);
		}
		
		if (ilFromX < omGanttRect.left)
			ilFromX = omGanttRect.left;
		if (ilToX   > omGanttRect.right) 
			ilToX   = omGanttRect.right;
		
		// draw the background bar
		if (IsOverlapped (ilFromX, ilToX, omGanttRect.left, omGanttRect.right))
		{
			olRect.left		= ilFromX;
			olRect.right	= ilToX;
			dcMem.FillRect (&olRect, &olBkBrush);
		}
	}

	//draw the red vertical line of the actual time
	int ilCurrTimeX = omGanttRect.left + pomTimeScale->GetXFromTime(omCurrentTime);
	if (IsBetween (ilCurrTimeX, omGanttRect.left, omGanttRect.right))
	{
		dcMem.SelectObject (&olRedP);
		dcMem.MoveTo (ilCurrTimeX, olRect.top);
		dcMem.LineTo (ilCurrTimeX, olRect.bottom);
	}

	//draw the LineSeparator if there is one
	if (omLines[lpLineNo].LineSeparator != FALSE)
	{
		CPen *pOldPen = dcMem.SelectObject (&omLines[lpLineNo].LineSeparatorPen);
		olRect.left  = omGanttRect.left;
		olRect.right = omGanttRect.right;
		for (int i = 0; i < omLines[lpLineNo].LineSeparatorHeight; i++)
		{
			dcMem.SetBkMode (TRANSPARENT);
			dcMem.MoveTo (olRect.left,  olRect.bottom + i + omLines[lpLineNo].LineSeparatorBegin);
			dcMem.LineTo (olRect.right, olRect.bottom + i + omLines[lpLineNo].LineSeparatorBegin);
		}
		dcMem.SelectObject (pOldPen);
		dcMem.SelectObject (pOldBrush);
	}
}

//**********************************************************
//*** returns the rect of a bar by its key
//***
//*** created from: RRO				date: 02.11.00
//***
//*** changes:
//*** date		who		what
//*** 09.11.00	rro		pay attention to various line height
//*** 29.08.02	rro		MWO added SubBarStack, so we've got 
//***					a new bar-height
//***
//**********************************************************
CRect CUGanttCtrl::GetBarRectByKey(CString omKey)
{
	CRect olRect;
	int ilLineNo = GetBarLineByKey (omKey);
	int ilBarHeight = (int)(TabLineHeight/(m_subbarStackCount+1)) - 2;

	if (ilLineNo > -1)
	{
		int ilPos = GetBarPosInLine (omKey, ilLineNo);
		BarData *polBar = &omLines[ilLineNo].omBars[ilPos];

		olRect.left		= GetXFromTime (polBar->Begin);
		olRect.right	= GetXFromTime (polBar->End);
		olRect.top		= GetTopOfLine (ilLineNo) + (2 + polBar->OverlapLevel * BarOverlapOffset);
		olRect.bottom	= olRect.top + ilBarHeight;
		//olRect.bottom	= olRect.top + TabLineHeight - 2;
	}
	else
	{
		olRect.SetRectEmpty();
	}
	return olRect;
}

//**********************************************************
//*** sets a line separator
//***
//*** created from: RRO				date: 08.11.00
//***
//*** changes:
//*** vers.		date		who		what
//***
//**********************************************************
void CUGanttCtrl::SetLineSeparator(long LineNo, short LSHeight, long LSColor, short LSPen, short TotalHeight, short LSBegin) 
{
	if ((LineNo > -1) && (LineNo < omLines.GetSize()))
	{
		LineData *polLine = &omLines[LineNo];

		polLine->LineSeparator					= TRUE;
		polLine->LineSeparatorHeight			= LSHeight;
		polLine->LineSeparatorHeightComplete	= TotalHeight;
		polLine->LineSeparatorBegin				= LSBegin;
		polLine->LineSeparatorColor				= LSColor;

		//get rid of the old pen if there was one and create the new one
		if (polLine->LineSeparatorPen.m_hObject != NULL)
			polLine->LineSeparatorPen.DeleteObject();
		polLine->LineSeparatorPen.CreatePen (LSPen , 1, (COLORREF)LSColor);

		HandleLineHight (LineNo);
	}
}

//**********************************************************
//*** deletes a line separator
//***
//*** created from: RRO				date: 08.11.00
//***
//*** changes:
//*** vers.		date		who		what
//***
//**********************************************************
void CUGanttCtrl::DeleteLineSeparator(long LineNo) 
{
	if ((LineNo > -1) && (LineNo < omLines.GetSize()))
	{
		LineData *polLine = &omLines[LineNo];

		polLine->LineSeparator				 = FALSE;
		polLine->LineSeparatorHeight		 = 0;
		polLine->LineSeparatorHeightComplete = 0;
		polLine->LineSeparatorColor			 = 0;

		if (polLine->LineSeparatorPen.m_hObject != NULL)
			polLine->LineSeparatorPen.DeleteObject();

		HandleLineHight (LineNo);
	}
}

//**********************************************************
//*** sets an arrow from the start of the first bar to the
//*** start of the second bar
//***
//*** created from: RRO				date: 09.11.00
//***
//*** changes:
//*** vers.		date		who		what
//***
//**********************************************************
void CUGanttCtrl::SetStart2Start(LPCTSTR BarKeySrc, LPCTSTR BarKeyDest) 
{
	AddBarRect (BarKeySrc);
	AddBarRect (BarKeyDest);

	CUGanttArrowData *polNewArrow = new CUGanttArrowData;
	
	polNewArrow->BarSrc		= BarKeySrc;
	polNewArrow->BarDest	= BarKeyDest;
	polNewArrow->ArrowTyp	= 0;

	omArrows.Add (polNewArrow);
	bmWithArrows = TRUE;
}

//**********************************************************
//*** sets an arrow from the start of the first bar to the
//*** end of the second bar
//***
//*** created from: RRO				date: 09.11.00
//***
//*** changes:
//*** vers.		date		who		what
//***
//**********************************************************
void CUGanttCtrl::SetStart2Finish(LPCTSTR BarKeySrc, LPCTSTR BarKeyDest) 
{
	AddBarRect (BarKeySrc);
	AddBarRect (BarKeyDest);

	CUGanttArrowData *polNewArrow = new CUGanttArrowData;
	
	polNewArrow->BarSrc		= BarKeySrc;
	polNewArrow->BarDest	= BarKeyDest;
	polNewArrow->ArrowTyp	= 1;

	omArrows.Add (polNewArrow);
	bmWithArrows = TRUE;
}

//**********************************************************
//*** sets an arrow from the end of the first bar to the
//*** start of the second bar
//***
//*** created from: RRO				date: 09.11.00
//***
//*** changes:
//*** vers.		date		who		what
//***
//**********************************************************
void CUGanttCtrl::SetFinish2Start(LPCTSTR BarKeySrc, LPCTSTR BarKeyDest) 
{
	AddBarRect (BarKeySrc);
	AddBarRect (BarKeyDest);

	CUGanttArrowData *polNewArrow = new CUGanttArrowData;
	
	polNewArrow->BarSrc		= BarKeySrc;
	polNewArrow->BarDest	= BarKeyDest;
	polNewArrow->ArrowTyp	= 2;

	omArrows.Add (polNewArrow);
	bmWithArrows = TRUE;
}

//**********************************************************
//*** sets an arrow from the end of the first bar to the
//*** end of the second bar
//***
//*** created from: RRO				date: 09.11.00
//***
//*** changes:
//*** vers.		date		who		what
//***
//**********************************************************
void CUGanttCtrl::SetFinish2Finish(LPCTSTR BarKeySrc, LPCTSTR BarKeyDest) 
{
	AddBarRect (BarKeySrc);
	AddBarRect (BarKeyDest);

	CUGanttArrowData *polNewArrow = new CUGanttArrowData;
	
	polNewArrow->BarSrc		= BarKeySrc;
	polNewArrow->BarDest	= BarKeyDest;
	polNewArrow->ArrowTyp	= 3;

	omArrows.Add (polNewArrow);
	bmWithArrows = TRUE;
}

//**********************************************************
//*** deletes the arrow-relation between to bars;
//*** pay attention: don't delete the rect of a bar in the
//*** map if it's involved in another relation
//***
//*** created from: RRO				date: 09.11.00
//***
//*** changes:
//*** vers.		date		who		what
//***
//**********************************************************
void CUGanttCtrl::DeleteX2X(LPCTSTR BarKeySrc, LPCTSTR BarKeyDest) 
{
	CUGanttRect	*polGanttRect;
	BOOL blFoundSrc  = FALSE;
	BOOL blFoundDest = FALSE;;

	int i=0;
	//delete the object in omArrows
	for (i = 0; i < omArrows.GetSize(); i++)
	{
		if (BarKeySrc == omArrows[i].BarSrc)
			if (BarKeyDest == omArrows[i].BarDest)
				omArrows.DeleteAt(i);
	}

	//look, if the bar is involved in another arrow-relation; take care of deleting the rect
	i = 0;
	while ((i < omArrows.GetSize()) && ((blFoundSrc != TRUE) || (blFoundDest != TRUE)))
	{
		if (blFoundSrc  != TRUE)
			if (omArrows[i].BarSrc  == BarKeySrc)
				blFoundSrc  = TRUE;

		if (blFoundDest != TRUE)
			if (omArrows[i].BarDest == BarKeyDest)
				blFoundDest = TRUE;
	}

	//delete the rects in the map if they are not needed any more (lookups will be faster)
	if (blFoundSrc  != TRUE)
	{
		mapBarRect.Lookup (BarKeySrc, (CObject*&)polGanttRect);
		delete polGanttRect;
		mapBarRect.RemoveKey (BarKeySrc);
	}

	if (blFoundDest != TRUE)
	{
		mapBarRect.Lookup (BarKeyDest, (CObject*&)polGanttRect);
		delete polGanttRect;
		mapBarRect.RemoveKey (BarKeyDest);
	}

	//set the "draw mode"
	if (omArrows.GetSize() < 1)
		bmWithArrows = FALSE;
}

//**********************************************************
//*** draws the arrows on the bitmap (probably the bitmap 
//*** "bmpBackgroundAndBkBars")
//***
//*** created from: RRO				date: 13.11.00
//***
//*** changes:
//*** vers.		date		who		what
//***
//**********************************************************
void CUGanttCtrl::DrawArrows(CBitmap &romBitmap)
{
	CClientDC dc (this);
	CDC dcMem;
	dcMem.CreateCompatibleDC (&dc);
	dcMem.SelectObject (&romBitmap);

	CUGanttArrowData	*polArrowData = NULL;
	CUGanttRect			*polRectSrc,
						*polRectDest;

	CPoint		olStartPoint,
				olFinishPoint;

	short		slDIV = (short)(smArrowWidth / 2);	//example: (short)36 / 10 = 3
	short		slMOD = smArrowWidth % 2;			//example: 36 % 10 = 6

	//init the pen
	CPen	olArrowPenNormal,
			olArrowPenSelect;
	olArrowPenNormal.CreatePen (0 , smArrowWidth, omArrowColorNormal);
	olArrowPenSelect.CreatePen (0 , smArrowWidth, omArrowColorSelected);


	//draw all arrows
	for (int i = 0; i < omArrows.GetSize(); i++)
	{
		polArrowData = &omArrows[i];
	
		//choose the pen
		if (polArrowData->IsSelected != TRUE)
			dcMem.SelectObject (&olArrowPenNormal);
		else
			dcMem.SelectObject (&olArrowPenSelect);

		//init the rects of the two bars you want to connect
		mapBarRect.Lookup (polArrowData->BarSrc,  (CObject*&)polRectSrc);
		mapBarRect.Lookup (polArrowData->BarDest, (CObject*&)polRectDest);

		switch (polArrowData->ArrowTyp)
		{
			case 0:		//Start2Start
								//init the points
				olStartPoint  = CPoint ( polRectSrc->m_rect.left, polRectSrc->m_rect.top  + (TabLineHeight / 2));
				olFinishPoint = CPoint (polRectDest->m_rect.left, polRectDest->m_rect.top + (TabLineHeight / 2));

				//look after the height of the bars
				if (polRectSrc->m_rect.bottom < polRectDest->m_rect.top)		//***src above dest
				{
					//you always draw a bit to the left
					dcMem.MoveTo (olStartPoint);
					dcMem.LineTo (olStartPoint.x - smArrowWidth - 4, olStartPoint.y);			//left

					if ((olStartPoint.x + (smArrowWidth * 2) + 8) < olFinishPoint.x)
					{
						dcMem.LineTo (olStartPoint.x - smArrowWidth - 4, olFinishPoint.y);		//down
						dcMem.LineTo (olFinishPoint);											//right
					}
					else
					{
						dcMem.LineTo (olFinishPoint.x - (smArrowWidth * 2) - 14, olStartPoint.y);	//still left
						dcMem.LineTo (olFinishPoint.x - (smArrowWidth * 2) - 14, olFinishPoint.y);	//down
						dcMem.LineTo (olFinishPoint);												//right
					}
				}
				else if (polRectSrc->m_rect.top > polRectDest->m_rect.bottom)	//***src below dest
				{
					//you always draw a bit to the right
					dcMem.MoveTo (olStartPoint);
					dcMem.LineTo (olStartPoint.x - smArrowWidth - 4, olStartPoint.y);			//left

					if ((olStartPoint.x + (smArrowWidth * 2) + 8) < olFinishPoint.x)
					{
						dcMem.LineTo (olStartPoint.x - smArrowWidth - 4, olFinishPoint.y);		//up
						dcMem.LineTo (olFinishPoint);											//right
					}
					else
					{
						dcMem.LineTo (olFinishPoint.x - (smArrowWidth * 2) - 14, olStartPoint.y);	//still left
						dcMem.LineTo (olFinishPoint.x - (smArrowWidth * 2) - 14, olFinishPoint.y);	//up
						dcMem.LineTo (olFinishPoint);												//right
					}
				}
				else															//***probably same line
				{
					if (polRectSrc->m_rect.right < (olFinishPoint.x - 5))
					{
						dcMem.MoveTo ( olStartPoint);
						dcMem.LineTo ( olStartPoint.x - smArrowWidth - 4, olStartPoint.y);			//left
						dcMem.LineTo ( olStartPoint.x - smArrowWidth - 4, olFinishPoint.y + (TabLineHeight / 2) + smArrowWidth + 2);		//down
						dcMem.LineTo (olFinishPoint.x - (smArrowWidth * 2) - 14, olFinishPoint.y + (TabLineHeight / 2) + smArrowWidth + 2);	//right
						dcMem.LineTo (olFinishPoint.x - (smArrowWidth * 2) - 14, olFinishPoint.y);	//up
						dcMem.LineTo (olFinishPoint);												//right
					}
					else
					{
						dcMem.MoveTo ( olStartPoint);
						dcMem.LineTo ( olStartPoint.x - smArrowWidth - 4, olStartPoint.y);			//right
						dcMem.LineTo ( olStartPoint.x - smArrowWidth - 4, olFinishPoint.y + (TabLineHeight / 2) + smArrowWidth + 2);		//down
						dcMem.LineTo (olFinishPoint.x - (smArrowWidth * 2) - 14, olFinishPoint.y + (TabLineHeight / 2) + smArrowWidth + 2);	//left
						dcMem.LineTo (olFinishPoint.x - (smArrowWidth * 2) - 14, olFinishPoint.y);	//up
						dcMem.LineTo (olFinishPoint);												//left
					}
	
				}
				
				//draw the arrow-head
				if (smArrowHead == 0)
				{
					if (slMOD != 0)
					{
						dcMem.MoveTo (olFinishPoint.x - 1, olFinishPoint.y);
						dcMem.LineTo (olFinishPoint.x - (slDIV * 2 + slMOD + 3), olFinishPoint.y - (slDIV + 3));
						dcMem.MoveTo (olFinishPoint.x - 1, olFinishPoint.y);
						dcMem.LineTo (olFinishPoint.x - (slDIV * 2 + slMOD + 3), olFinishPoint.y + (slDIV + 3));
					}
					else
					{
						dcMem.MoveTo (olFinishPoint.x - 1, olFinishPoint.y - 1);
						dcMem.LineTo (olFinishPoint.x - (slDIV * 2 + slMOD + 3), olFinishPoint.y - (slDIV + 3));
						dcMem.MoveTo (olFinishPoint.x - 1, olFinishPoint.y);
						dcMem.LineTo (olFinishPoint.x - (slDIV * 2 + slMOD + 3), olFinishPoint.y + (slDIV + 2));
					}
				}

				break;
			case 1:		//Start2Finish
				//init the points
				olStartPoint  = CPoint ( polRectSrc->m_rect.left,  polRectSrc->m_rect.top  + (TabLineHeight / 2));
				olFinishPoint = CPoint (polRectDest->m_rect.right, polRectDest->m_rect.top + (TabLineHeight / 2));

				//look after the height of the bars
				if (polRectSrc->m_rect.bottom < polRectDest->m_rect.top)		//***src above dest
				{
					//you always draw a bit to the left
					dcMem.MoveTo (olStartPoint);
					dcMem.LineTo (olStartPoint.x - smArrowWidth - 4, olStartPoint.y);			//left

					if ((olStartPoint.x - (smArrowWidth * 2) - 8) > olFinishPoint.x)
					{
						dcMem.LineTo (olStartPoint.x - smArrowWidth - 4, olFinishPoint.y);		//down
						dcMem.LineTo (olFinishPoint);											//left
					}
					else
					{
						dcMem.LineTo ( olStartPoint.x - smArrowWidth -  4, olStartPoint.y + (TabLineHeight / 2) + smArrowWidth + 2);		//down
						dcMem.LineTo (olFinishPoint.x + (smArrowWidth * 2) + 8, olStartPoint.y + (TabLineHeight / 2) + smArrowWidth + 2);	//right
						dcMem.LineTo (olFinishPoint.x + (smArrowWidth * 2) + 8, olFinishPoint.y);	//down
						dcMem.LineTo (olFinishPoint);												//left
					}
				}
				else if (polRectSrc->m_rect.top > polRectDest->m_rect.bottom)	//***src below dest
				{
					//you always draw a bit to the right
					dcMem.MoveTo (olStartPoint);
					dcMem.LineTo (olStartPoint.x - smArrowWidth - 4, olStartPoint.y);			//left

					if ((olStartPoint.x - (smArrowWidth * 2) - 8) > olFinishPoint.x)
					{
						dcMem.LineTo (olStartPoint.x - smArrowWidth - 4, olFinishPoint.y);		//up
						dcMem.LineTo (olFinishPoint);											//left
					}
					else
					{
						dcMem.LineTo ( olStartPoint.x - smArrowWidth -  4,      olStartPoint.y - (TabLineHeight / 2) - smArrowWidth - 2);	//up
						dcMem.LineTo (olFinishPoint.x + (smArrowWidth * 2) + 8, olStartPoint.y - (TabLineHeight / 2) - smArrowWidth - 2);	//right
						dcMem.LineTo (olFinishPoint.x + (smArrowWidth * 2) + 8, olFinishPoint.y);	//up
						dcMem.LineTo (olFinishPoint);												//left
					}
				}
				else															//***probably same line
				{
					if ((olStartPoint.x - 5) > olFinishPoint.x)
					{
						dcMem.MoveTo (olStartPoint);
						dcMem.LineTo (olStartPoint.x - smArrowWidth - 4, olStartPoint.y);			//left
						dcMem.LineTo (olStartPoint.x - smArrowWidth - 4, olFinishPoint.y);			//???
						dcMem.LineTo (olFinishPoint);												//left
					}
					else //now we've got a problem....draw below the two bars
					{
						dcMem.MoveTo ( olStartPoint);
						dcMem.LineTo ( olStartPoint.x - smArrowWidth -  4, olStartPoint.y);			//left
						dcMem.LineTo ( olStartPoint.x - smArrowWidth -  4, olFinishPoint.y + (TabLineHeight / 2) + smArrowWidth + 2);		//down
						dcMem.LineTo (olFinishPoint.x + smArrowWidth + 10, olFinishPoint.y + (TabLineHeight / 2) + smArrowWidth + 2);		//right
						dcMem.LineTo (olFinishPoint.x + smArrowWidth + 10, olFinishPoint.y);		//up
						dcMem.LineTo (olFinishPoint);												//left
					}
				}

				//draw the arrow-head
				if (smArrowHead == 0)
				{
					if (slMOD != 0)
					{
						dcMem.MoveTo (olFinishPoint.x + 1, olFinishPoint.y);
						dcMem.LineTo (olFinishPoint.x + (slDIV * 2 + slMOD + 3), olFinishPoint.y - (slDIV + 3));
						dcMem.MoveTo (olFinishPoint.x + 1, olFinishPoint.y);
						dcMem.LineTo (olFinishPoint.x + (slDIV * 2 + slMOD + 3), olFinishPoint.y + (slDIV + 3));
					}
					else
					{
						dcMem.MoveTo (olFinishPoint.x, olFinishPoint.y + 1);
						dcMem.LineTo (olFinishPoint.x + (slDIV * 2 + slMOD + 3), olFinishPoint.y - (slDIV + 3));
						dcMem.MoveTo (olFinishPoint.x + 1, olFinishPoint.y);
						dcMem.LineTo (olFinishPoint.x + (slDIV * 2 + slMOD + 3), olFinishPoint.y + (slDIV + 2));
					}
				}								

				break;
			case 2:		//Finish2Start
				//init the points
				olStartPoint  = CPoint (polRectSrc->m_rect.right, polRectSrc->m_rect.top  + (TabLineHeight / 2));
				olFinishPoint = CPoint (polRectDest->m_rect.left, polRectDest->m_rect.top + (TabLineHeight / 2));

				//look after the height of the bars
				if (polRectSrc->m_rect.bottom < polRectDest->m_rect.top)		//***src above dest
				{
					//you always draw a bit to the right
					dcMem.MoveTo (olStartPoint);
					dcMem.LineTo (olStartPoint.x + smArrowWidth + 4, olStartPoint.y);			//right

					if ((olStartPoint.x + (smArrowWidth * 2) + 13) < olFinishPoint.x)
					{
						dcMem.LineTo (olStartPoint.x + smArrowWidth + 4, olFinishPoint.y);		//down
						dcMem.LineTo (olFinishPoint);											//right
					}
					else
					{
						dcMem.LineTo ( olStartPoint.x + smArrowWidth +  4, olStartPoint.y + (TabLineHeight / 2) + smArrowWidth + 2);		//down
						dcMem.LineTo (olFinishPoint.x - smArrowWidth - 10, olStartPoint.y + (TabLineHeight / 2) + smArrowWidth + 2);		//left
						dcMem.LineTo (olFinishPoint.x - smArrowWidth - 10, olFinishPoint.y);	//down
						dcMem.LineTo (olFinishPoint);											//right
					}
				}
				else if (polRectSrc->m_rect.top > polRectDest->m_rect.bottom)	//***src below dest
				{
					//you always draw a bit to the right
					dcMem.MoveTo (olStartPoint);
					dcMem.LineTo (olStartPoint.x + smArrowWidth + 4, olStartPoint.y);			//right

					if ((olStartPoint.x + (smArrowWidth * 2) + 13) < olFinishPoint.x)
					{
						dcMem.LineTo (olStartPoint.x + smArrowWidth + 4, olFinishPoint.y);		//up
						dcMem.LineTo (olFinishPoint);											//right
					}
					else
					{
						dcMem.LineTo ( olStartPoint.x + smArrowWidth +  4, olStartPoint.y - (TabLineHeight / 2) - smArrowWidth - 2);		//up
						dcMem.LineTo (olFinishPoint.x - smArrowWidth - 10, olStartPoint.y - (TabLineHeight / 2) - smArrowWidth - 2);		//left
						dcMem.LineTo (olFinishPoint.x - smArrowWidth - 10, olFinishPoint.y);	//up
						dcMem.LineTo (olFinishPoint);											//right
					}
				}
				else															//***probably same line
				{
					if ((olStartPoint.x + 5) < olFinishPoint.x)
					{
						dcMem.MoveTo (olStartPoint);
						dcMem.LineTo (olStartPoint.x + smArrowWidth + 4, olStartPoint.y);			//right
						dcMem.LineTo (olStartPoint.x + smArrowWidth + 4, olFinishPoint.y);			//???
						dcMem.LineTo (olFinishPoint);												//right
					}
					else //now we've got a problem....draw below the two bars
					{
						dcMem.MoveTo ( olStartPoint);
						dcMem.LineTo ( olStartPoint.x + smArrowWidth +  4, olStartPoint.y);			//right
						dcMem.LineTo ( olStartPoint.x + smArrowWidth +  4, olFinishPoint.y + (TabLineHeight / 2) + smArrowWidth + 2);		//down
						dcMem.LineTo (olFinishPoint.x - smArrowWidth - 10, olFinishPoint.y + (TabLineHeight / 2) + smArrowWidth + 2);		//left
						dcMem.LineTo (olFinishPoint.x - smArrowWidth - 10, olFinishPoint.y);		//up
						dcMem.LineTo (olFinishPoint);												//right
					}
				}
				
				//draw the arrow-head
				if (smArrowHead == 0)
				{
					if (slMOD != 0)
					{
						dcMem.MoveTo (olFinishPoint.x - 1, olFinishPoint.y);
						dcMem.LineTo (olFinishPoint.x - (slDIV * 2 + slMOD + 3), olFinishPoint.y - (slDIV + 3));
						dcMem.MoveTo (olFinishPoint.x - 1, olFinishPoint.y);
						dcMem.LineTo (olFinishPoint.x - (slDIV * 2 + slMOD + 3), olFinishPoint.y + (slDIV + 3));
					}
					else
					{
						dcMem.MoveTo (olFinishPoint.x - 1, olFinishPoint.y - 1);
						dcMem.LineTo (olFinishPoint.x - (slDIV * 2 + slMOD + 3), olFinishPoint.y - (slDIV + 3));
						dcMem.MoveTo (olFinishPoint.x - 1, olFinishPoint.y);
						dcMem.LineTo (olFinishPoint.x - (slDIV * 2 + slMOD + 3), olFinishPoint.y + (slDIV + 2));
					}
				}

				break;
			case 3:		//Finish2Finish
				//init the points
				olStartPoint  = CPoint ( polRectSrc->m_rect.right, polRectSrc->m_rect.top  + (TabLineHeight / 2));
				olFinishPoint = CPoint (polRectDest->m_rect.right, polRectDest->m_rect.top + (TabLineHeight / 2));

				//look after the height of the bars
				if (polRectSrc->m_rect.bottom < polRectDest->m_rect.top)		//***src above dest
				{
					//you always draw a bit to the right
					dcMem.MoveTo (olStartPoint);
					dcMem.LineTo (olStartPoint.x + smArrowWidth + 4, olStartPoint.y);			//right

					if ((olStartPoint.x - (smArrowWidth * 2) - 8) > olFinishPoint.x)
					{
						dcMem.LineTo (olStartPoint.x + smArrowWidth + 4, olFinishPoint.y);		//down
						dcMem.LineTo (olFinishPoint);											//left
					}
					else
					{
						dcMem.LineTo (olFinishPoint.x + (smArrowWidth * 2) + 14, olStartPoint.y);	//still right
						dcMem.LineTo (olFinishPoint.x + (smArrowWidth * 2) + 14, olFinishPoint.y);	//down
						dcMem.LineTo (olFinishPoint);												//left
					}
				}
				else if (polRectSrc->m_rect.top > polRectDest->m_rect.bottom)	//***src below dest
				{
					//you always draw a bit to the right
					dcMem.MoveTo (olStartPoint);
					dcMem.LineTo (olStartPoint.x + smArrowWidth + 4, olStartPoint.y);			//right

					if ((olStartPoint.x - (smArrowWidth * 2) - 8) > olFinishPoint.x)
					{
						dcMem.LineTo (olStartPoint.x + smArrowWidth + 4, olFinishPoint.y);		//up
						dcMem.LineTo (olFinishPoint);											//left
					}
					else
					{
						dcMem.LineTo (olFinishPoint.x + (smArrowWidth * 2) + 14, olStartPoint.y);	//still right
						dcMem.LineTo (olFinishPoint.x + (smArrowWidth * 2) + 14, olFinishPoint.y);	//up
						dcMem.LineTo (olFinishPoint);												//left
					}
				}
				else															//***probably same line
				{
					if ((olStartPoint.x + 5) < polRectDest->m_rect.left)
					{
						dcMem.MoveTo ( olStartPoint);
						dcMem.LineTo ( olStartPoint.x + smArrowWidth + 4, olStartPoint.y);			//right
						dcMem.LineTo ( olStartPoint.x + smArrowWidth + 4, olFinishPoint.y + (TabLineHeight / 2) + smArrowWidth + 2);		//down
						dcMem.LineTo (olFinishPoint.x + (smArrowWidth * 2) + 14, olFinishPoint.y + (TabLineHeight / 2) + smArrowWidth + 2);	//right
						dcMem.LineTo (olFinishPoint.x + (smArrowWidth * 2) + 14, olFinishPoint.y);	//up
						dcMem.LineTo (olFinishPoint);												//left
					}
					else //now we've got a problem....draw below the two bars
					{
						dcMem.MoveTo ( olStartPoint);
						dcMem.LineTo ( olStartPoint.x + smArrowWidth + 4, olStartPoint.y);			//right
						dcMem.LineTo ( olStartPoint.x + smArrowWidth + 4, olFinishPoint.y + (TabLineHeight / 2) + smArrowWidth + 2);		//down
						dcMem.LineTo (olFinishPoint.x + (smArrowWidth * 2) + 14, olFinishPoint.y + (TabLineHeight / 2) + smArrowWidth + 2);	//left
						dcMem.LineTo (olFinishPoint.x + (smArrowWidth * 2) + 14, olFinishPoint.y);	//up
						dcMem.LineTo (olFinishPoint);												//left
					}
				}
				
				//draw the arrow-head
				if (smArrowHead == 0)
				{
					if (slMOD != 0)
					{
						dcMem.MoveTo (olFinishPoint.x + 1, olFinishPoint.y);
						dcMem.LineTo (olFinishPoint.x + (slDIV * 2 + slMOD + 3), olFinishPoint.y - (slDIV + 3));
						dcMem.MoveTo (olFinishPoint.x + 1, olFinishPoint.y);
						dcMem.LineTo (olFinishPoint.x + (slDIV * 2 + slMOD + 3), olFinishPoint.y + (slDIV + 3));
					}
					else
					{
						dcMem.MoveTo (olFinishPoint.x, olFinishPoint.y + 1);
						dcMem.LineTo (olFinishPoint.x + (slDIV * 2 + slMOD + 3), olFinishPoint.y - (slDIV + 3));
						dcMem.MoveTo (olFinishPoint.x + 1, olFinishPoint.y);
						dcMem.LineTo (olFinishPoint.x + (slDIV * 2 + slMOD + 3), olFinishPoint.y + (slDIV + 2));
					}
				}				
				break;
			default:
				break;
		}
	}
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// - created from:	MWO		date:	19.08.2002
// -
// - changes:		who		when		what
// -
// - description:	Draws the decoration for a bar
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
void CUGanttCtrl::DrawDecoObject(CDC *pdc, DECORATION_OBJECT *polDecoObj, CRect &opHRect)
{
	int ilCount;
	CRect olHRect;
	int i;
	if(pdc == NULL || polDecoObj == NULL)
		return;
	CBrush *pOldBrush;
	CBrush *polBrush = NULL;
	olHRect = opHRect;
	CPen olBlackPen(PS_SOLID,1,COLORREF(RGB(0,0,0))),  *pOldPen;
	pOldPen = pdc->SelectObject(&olBlackPen);

	olHRect.bottom = opHRect.bottom - 1;
	if(polDecoObj->AsTriangle == false)
	{
		ilCount = polDecoObj->LocationList.GetSize();
		for (i=0; i < ilCount; i++)
		{
			CRect olBarRect;		//Rect for a bar
			CRgn  olTriangleRgn;    //Region for a triangle
			CPoint olPoints[3];		//Array of points to describe the region
			int    ilWidth;			//Width of the triangle leg or width of the bar in pix

			ilWidth = atoi(polDecoObj->PixelList[i].GetBuffer(0));

			//TL,BL,BR,BL,L,R,BTR,BTL
			if(polDecoObj->LocationList[i] == "TL")		//TOPLEFT
			{
				olPoints[0].x = olHRect.left+1;
				olPoints[0].y = olHRect.top + ilWidth ;
				olPoints[1].x = olHRect.left+1;
				olPoints[1].y = olHRect.top+1;
				olPoints[2].x = olHRect.left + ilWidth ; 
				olPoints[2].y = olHRect.top;
				olTriangleRgn.CreatePolygonRgn(olPoints, 3, WINDING);
				polBrush = new CBrush();
				polBrush->CreateSolidBrush((COLORREF)atoi(polDecoObj->ColorList[i]));
				pOldBrush = pdc->SelectObject(polBrush);
				pdc->FillRgn(&olTriangleRgn, polBrush);
				pdc->SelectObject(pOldBrush);
				delete polBrush;
			} //END "TL"
			if(polDecoObj->LocationList[i] == "TR")	//TOPRIGHT
			{
				olPoints[0].x = olHRect.right;
				olPoints[0].y = olHRect.top + ilWidth ;
				olPoints[1].x = olHRect.right;
				olPoints[1].y = olHRect.top+1;
				olPoints[2].x = olHRect.right - ilWidth ; 
				olPoints[2].y = olHRect.top+1;
				olTriangleRgn.CreatePolygonRgn(olPoints, 3, WINDING);
				polBrush = new CBrush();
				polBrush->CreateSolidBrush((COLORREF)atoi(polDecoObj->ColorList[i]));
				pOldBrush = pdc->SelectObject(polBrush);
				pdc->FillRgn(&olTriangleRgn, polBrush);
				pdc->SelectObject(pOldBrush);
				delete polBrush;
			} //END TR
			if(polDecoObj->LocationList[i] == "BL") //BOTTOMLEFT
			{
				olPoints[0].x = olHRect.left+1;
				olPoints[0].y = olHRect.bottom-1 - ilWidth;
				olPoints[1].x = olHRect.left+1;
				olPoints[1].y = olHRect.bottom-1;
				olPoints[2].x = olHRect.left+1 + ilWidth; 
				olPoints[2].y = olHRect.bottom-1;
				olTriangleRgn.CreatePolygonRgn(olPoints, 3, WINDING);
				polBrush = new CBrush();
				polBrush->CreateSolidBrush((COLORREF)atoi(polDecoObj->ColorList[i]));
				pOldBrush = pdc->SelectObject(polBrush);
				pdc->FillRgn(&olTriangleRgn, polBrush);
				pdc->SelectObject(pOldBrush);
				delete polBrush;
			} //END BL
			if(polDecoObj->LocationList[i] == "BR") //BOTTOMRIGHT
			{
				olPoints[0].x = olHRect.right-1;
				olPoints[0].y = olHRect.bottom-1 - ilWidth;
				olPoints[1].x = olHRect.right-1;
				olPoints[1].y = olHRect.bottom-1;
				olPoints[2].x = olHRect.right-1 - ilWidth; 
				olPoints[2].y = olHRect.bottom-1;
				olTriangleRgn.CreatePolygonRgn(olPoints, 3, WINDING);
				polBrush = new CBrush();
				polBrush->CreateSolidBrush((COLORREF)atoi(polDecoObj->ColorList[i]));
				pOldBrush = pdc->SelectObject(polBrush);
				pdc->FillRgn(&olTriangleRgn, polBrush);
				pdc->SelectObject(pOldBrush);
				delete polBrush;
			} //END "BR"
			if(polDecoObj->LocationList[i] == "BTL") //BigTriangleLeft
			{
				int ilMid=(int)((olHRect.bottom - olHRect.top)/2);
				olPoints[0].x = olHRect.left+1;
				olPoints[0].y = olHRect.top;
				olPoints[1].x = olHRect.left+1+ilWidth;
				olPoints[1].y = olHRect.top+ilMid;
				olPoints[2].x = olHRect.left+1; 
				olPoints[2].y = olHRect.bottom-1;
				olTriangleRgn.CreatePolygonRgn(olPoints, 3, WINDING);
				polBrush = new CBrush();
				polBrush->CreateSolidBrush((COLORREF)atoi(polDecoObj->ColorList[i]));
				pOldBrush = pdc->SelectObject(polBrush);
				pdc->FillRgn(&olTriangleRgn, polBrush);
				pdc->SelectObject(pOldBrush);
				delete polBrush;
			} //END "BTL"
			if(polDecoObj->LocationList[i] == "BTR") //BigTriangleRight
			{
				int ilMid=(int)((olHRect.bottom - olHRect.top)/2);
				olPoints[0].x = olHRect.right-1;
				olPoints[0].y = olHRect.top;
				olPoints[1].x = olHRect.right-1-ilWidth;
				olPoints[1].y = olHRect.top+ilMid;
				olPoints[2].x = olHRect.right-1; 
				olPoints[2].y = olHRect.bottom-1;
				olTriangleRgn.CreatePolygonRgn(olPoints, 3, WINDING);
				polBrush = new CBrush();
				polBrush->CreateSolidBrush((COLORREF)atoi(polDecoObj->ColorList[i]));
				pOldBrush = pdc->SelectObject(polBrush);
				pdc->FillRgn(&olTriangleRgn, polBrush);
				pdc->SelectObject(pOldBrush);
				delete polBrush;
			} //END "BTR"
			if(polDecoObj->LocationList[i] == "L")  //BAR LEFT
			{
				//-1: use the full width of the cell
				if(ilWidth == -1)
				{
					ilWidth = olHRect.right - olHRect.left;
				}
				olBarRect.top = olHRect.top;
				olBarRect.left = olHRect.left+1;
				olBarRect.right = olHRect.left+1 + ilWidth;
				olBarRect.bottom = olHRect.bottom;
				polBrush = new CBrush();
				polBrush->CreateSolidBrush((COLORREF)atoi(polDecoObj->ColorList[i]));
				pOldBrush = pdc->SelectObject(polBrush);
				pdc->FillRect(&olBarRect, polBrush);
				pdc->SelectObject(pOldBrush);
				delete polBrush;
			} //END "L"
			if(polDecoObj->LocationList[i] == "R")  //BAR RRIGHT
			{
				//-1: use the full width of the cell
				if(ilWidth == -1)
				{
					ilWidth = olHRect.right - olHRect.left;
				}
				olBarRect.top = olHRect.top;
				olBarRect.left = olHRect.right-1 - ilWidth;
				olBarRect.right = olHRect.right-1;
				olBarRect.bottom = olHRect.bottom-1;
				polBrush = new CBrush();
				polBrush->CreateSolidBrush((COLORREF)atoi(polDecoObj->ColorList[i]));
				pOldBrush = pdc->SelectObject(polBrush);
				pdc->FillRect(&olBarRect, polBrush);
				pdc->SelectObject(pOldBrush);
				delete polBrush;
			} //END "R"
			if(polDecoObj->LocationList[i] == "T")  //BAR TOP
			{
				olBarRect.top = olHRect.top;
				olBarRect.left = olHRect.left+1;
				olBarRect.right = olHRect.right - 1;
				olBarRect.bottom = olHRect.top+ilWidth;
				polBrush = new CBrush();
				polBrush->CreateSolidBrush((COLORREF)atoi(polDecoObj->ColorList[i]));
				pOldBrush = pdc->SelectObject(polBrush);
				pdc->FillRect(&olBarRect, polBrush);
				pdc->SelectObject(pOldBrush);
				delete polBrush;
			} //END "L"
			if(polDecoObj->LocationList[i] == "B")  //BAR BOTTOM
			{
				olBarRect.top = olHRect.bottom-1-ilWidth;
				olBarRect.left = olHRect.left+1;
				olBarRect.right = olHRect.right - 1;
				olBarRect.bottom = olHRect.bottom-1;
				polBrush = new CBrush();
				polBrush->CreateSolidBrush((COLORREF)atoi(polDecoObj->ColorList[i]));
				pOldBrush = pdc->SelectObject(polBrush);
				pdc->FillRect(&olBarRect, polBrush);
				pdc->SelectObject(pOldBrush);
				delete polBrush;
			} //END "L"
		}
	}//if(polDecoObj->AsTriangle == false)
	else
	{
		CRect olBarRect;		//Rect for a bar
		CRgn  olTriangleRgn;    //Region for a triangle
		CPoint olPoints[3];		//Array of points to describe the region
		int    ilWidth;			//Width of the triangle leg or width of the bar in pix

		ilWidth = olHRect.bottom - olHRect.top;
		//Triangle - Left
		if(polDecoObj->TriangleLeftColor > -1)
		{
			int ilMid=(int)((olHRect.bottom - olHRect.top)/2);
			olPoints[0].x = olHRect.left+1;
			olPoints[0].y = olHRect.top;
			olPoints[1].x = olHRect.left+1+ilWidth;
			olPoints[1].y = olHRect.top;
			olPoints[2].x = olHRect.left+1; 
			olPoints[2].y = olHRect.bottom-1;
			olTriangleRgn.CreatePolygonRgn(olPoints, 3, WINDING);
			polBrush = new CBrush();
			polBrush->CreateSolidBrush((COLORREF)polDecoObj->TriangleLeftColor);
			pOldBrush = pdc->SelectObject(polBrush);
			pdc->FillRgn(&olTriangleRgn, polBrush);
			pdc->SelectObject(pOldBrush);
			delete polBrush;
			pdc->MoveTo(olPoints[1]);
			olPoints[2].y = olHRect.bottom;
			pdc->LineTo(olPoints[2]);
		}
		//Triangle - Right
		if(polDecoObj->TriangleRightColor > -1)
		{
			int ilMid=(int)((olHRect.bottom - olHRect.top)/2);
			olPoints[0].x = olHRect.right-1;
			olPoints[0].y = olHRect.top;
			olPoints[1].x = olHRect.right-1-ilWidth;
			olPoints[1].y = olHRect.bottom-1;
			olPoints[2].x = olHRect.right-1; 
			olPoints[2].y = olHRect.bottom-1;
			olTriangleRgn.CreatePolygonRgn(olPoints, 3, WINDING);
			polBrush = new CBrush();
			polBrush->CreateSolidBrush((COLORREF)polDecoObj->TriangleRightColor);
			pOldBrush = pdc->SelectObject(polBrush);
			pdc->FillRgn(&olTriangleRgn, polBrush);
			pdc->SelectObject(pOldBrush);
			delete polBrush;
			pdc->MoveTo(olPoints[0]);
			olPoints[1].y = olHRect.bottom;
			pdc->LineTo(olPoints[1]);
		}
	}
	pdc->SelectObject(pOldPen);
	
}


//**********************************************************
//*** adds the rect of the specified bar to the map
//***
//*** created from: RRO				date: 13.11.00
//***
//*** changes:
//*** vers.		date		who		what
//***
//**********************************************************
void CUGanttCtrl::AddBarRect(CString BarKey)
{
	CUGanttRect *polLookUpRect;
	if (mapBarRect.Lookup (BarKey, (CObject*&)polLookUpRect) == 0)
	{
		CUGanttRect *polGanttRect = new CUGanttRect (GetBarRectByKey (BarKey));
		mapBarRect.SetAt (BarKey, polGanttRect);
	}
}

//**********************************************************
//*** the width of the arrow; effects the whole arrow
//***
//*** created from: RRO				date: 13.11.00
//***
//*** changes:
//*** vers.		date		who		what
//***
//**********************************************************
short CUGanttCtrl::GetArrowWidth() 
{
	return smArrowWidth;
}
void CUGanttCtrl::SetArrowWidth(short nNewValue) 
{
	smArrowWidth = nNewValue;
	SetModifiedFlag();
}

//**********************************************************
//*** the head of the arrow (at the moment only one typ is
//*** implemented)
//***
//*** created from: RRO				date: 13.11.00
//***
//*** changes:
//*** vers.		date		who		what
//***
//**********************************************************
short CUGanttCtrl::GetArrowHead() 
{
	return smArrowHead;
}
void CUGanttCtrl::SetArrowHead(short nNewValue) 
{
	smArrowHead = nNewValue;
	SetModifiedFlag();
}

//**********************************************************
//*** the normal arrow-color
//***
//*** created from: RRO				date: 13.11.00
//***
//*** changes:
//*** vers.		date		who		what
//***
//**********************************************************
long CUGanttCtrl::GetArrowColorNormal() 
{
	return omArrowColorNormal;
}
void CUGanttCtrl::SetArrowColorNormal(long nNewValue) 
{
	omArrowColorNormal = COLORREF (nNewValue);
	SetModifiedFlag();
}

//**********************************************************
//*** this is the color of the selected arrow
//***
//*** created from: RRO				date: 13.11.00
//***
//*** changes:
//*** vers.		date		who		what
//***
//**********************************************************
long CUGanttCtrl::GetArrowColorSelected() 
{
	return omArrowColorSelected;
}
void CUGanttCtrl::SetArrowColorSelected(long nNewValue) 
{
	omArrowColorSelected = COLORREF (nNewValue);
	SetModifiedFlag();
}

//**********************************************************
//*** when you change bar's begin and/or end, you have to
//*** update its rect in the map
//***
//*** created from: RRO				date: 13.11.00
//***
//*** changes:
//*** vers.		date		who		what
//***
//**********************************************************
void CUGanttCtrl::UpdateSingleRect(CString olKey)
{
	//has the bar an arrow-relation? If yes: update its rect in the map
	CUGanttRect	*polGanttRect;
	if (mapBarRect.Lookup (olKey, (CObject*&)polGanttRect) != 0)
	{
		polGanttRect->m_rect = GetBarRectByKey(olKey);
	}
}

//**********************************************************
//*** e.g. if you scroll you have to update all rects in the map
//***
//*** created from: RRO				date: 13.11.00
//***
//*** changes:
//*** vers.		date		who		what
//***
//**********************************************************
void CUGanttCtrl::UpdateAllRects()
{
	CString olRetKey;
	CUGanttRect *polGanttRect;
	POSITION pos = mapBarRect.GetStartPosition();

	while (pos != NULL)
	{
		mapBarRect.GetNextAssoc (pos, olRetKey, (CObject*&)polGanttRect);
		polGanttRect->m_rect = GetBarRectByKey (olRetKey);
	}
}

//**********************************************************
//*** sets the text of the left outside text of a bar
//***
//*** created from: RRO				date: 23.11.00
//***
//*** changes:
//*** vers.		date		who		what
//***
//**********************************************************
void CUGanttCtrl::SetBarLeftOutsideText(LPCTSTR Key, LPCTSTR Text) 
{
	long llLine, llPos;
	llLine = GetBarLineByKey (Key);
	if (llLine > -1)
	{
		llPos = GetBarPosInLine (Key, llLine);
		omLines[llLine].omBars[llPos].LeftOutsideText = Text;
	}
}

//**********************************************************
//*** sets the text of the right outside text of a bar
//***
//*** created from: RRO				date: 23.11.00
//***
//*** changes:
//*** vers.		date		who		what
//***
//**********************************************************
void CUGanttCtrl::SetBarRightOutsideText(LPCTSTR Key, LPCTSTR Text) 
{
	long llLine, llPos;
	llLine = GetBarLineByKey (Key);
	if (llLine > -1)
	{
		llPos = GetBarPosInLine (Key, llLine);
		omLines[llLine].omBars[llPos].RightOutsideText = Text;
	}
}

//**********************************************************
//*** sets the color of the left outside text
//***
//*** created from: RRO				date: 23.11.00
//***
//*** changes:
//*** vers.		date		who		what
//***
//**********************************************************
void CUGanttCtrl::SetBarLeftOutsideTextColor(LPCTSTR Key, long Color) 
{
	long llLine, llPos;
	llLine = GetBarLineByKey (Key);
	if (llLine > -1)
	{
		llPos = GetBarPosInLine (Key, llLine);
		omLines[llLine].omBars[llPos].LeftTextColor = COLORREF (Color);
	}
}

//**********************************************************
//*** sets the color of the right outside text
//***
//*** created from: RRO				date: 23.11.00
//***
//*** changes:
//*** vers.		date		who		what
//***
//**********************************************************
void CUGanttCtrl::SetBarRightOutsideTextColor(LPCTSTR Key, long Color) 
{
	long llLine, llPos;
	llLine = GetBarLineByKey (Key);
	if (llLine > -1)
	{
		llPos = GetBarPosInLine (Key, llLine);
		omLines[llLine].omBars[llPos].RightTextColor = COLORREF (Color);
	}
}

//**********************************************************
//*** sets the split-colors of a bar
//***
//*** created from: RRO				date: 27.11.00
//***
//*** changes:
//*** vers.		date		who		what
//***
//**********************************************************
void CUGanttCtrl::SetBarSplitColor(LPCTSTR Key, long LeftColor, long RightColor) 
{
	long llLine;
	llLine = GetBarLineByKey (Key);
	if (llLine > -1)
	{
		BarData *polBar = &omLines[llLine].omBars[GetBarPosInLine (Key, llLine)];
		polBar->SplitColor		= TRUE;
		polBar->LeftBackColor	= COLORREF(LeftColor);
		polBar->RightBackColor	= COLORREF(RightColor);
	}
}

//**********************************************************
//*** sets the split-colors of a bar
//***
//*** created from: RRO				date: 27.11.00
//***
//*** changes:
//*** vers.		date		who		what
//***
//**********************************************************
void CUGanttCtrl::DeleteSplitColor(LPCTSTR Key) 
{
	long llLine;
	llLine = GetBarLineByKey (Key);
	if (llLine > -1)
	{
		BarData *polBar = &omLines[llLine].omBars[GetBarPosInLine (Key, llLine)];
		polBar->SplitColor = FALSE;
	}
}

//**********************************************************
//*** sets the style of a bar
//***
//*** created from: RRO				date: 30.11.00
//***
//*** changes:
//*** vers.		date		who		what
//***
//**********************************************************
void CUGanttCtrl::SetBarStyle(LPCTSTR Key, short BarStyle) 
{
	long llLine;
	llLine = GetBarLineByKey (Key);
	if (llLine > -1)
	{
		BarData *polBar = &omLines[llLine].omBars[GetBarPosInLine (Key, llLine)];
		polBar->Shape = BarStyle;
	}
}

void CUGanttCtrl::OnOverlappingRightBarTopChanged() 
{
	LineData *polLine;

	omBarMode = "NORMAL";
	bmFoundBar = FALSE;
	bmFoundBkBar = FALSE;

	SetModifiedFlag();

	for (int i = 0; i < omLines.GetSize(); i++)
	{
		polLine = &omLines[i];
		for (int j = 0; j < polLine->omBars.GetSize(); j++)
		{
			HandleOverlapLevelOfBar (polLine->omBars[j].Key, FALSE);
		}
	}

	Invalidate();
}

void CUGanttCtrl::OnAutoScrollChanged() 
{
	SetModifiedFlag();
}

//**********************************************************
//*** sets the cursor for the control and remebers it so
//*** it can be restored if windows changed it
//*** always use this method instead of directly call
//*** SetCursor().
//***
//*** returns TRUE if the cursor has changed
//***
//*** created from: RSC				date: 12.03.01
//***
//*** changes:
//*** vers.		date		who		what
//***
//**********************************************************
BOOL CUGanttCtrl::IntSetCursor(HCURSOR hpCursor)
{
  BOOL blRC = FALSE;

// Only accept cursor changes, when not moving or sizing
  if (omBarMode == "NORMAL")
  {
    m_nCursor = hpCursor;
    SetCursor(hpCursor);
    blRC = TRUE;
  }
  return blRC;
}

//**********************************************************
//*** adds a new time marker, or changes the color of an
//*** existing (if there is one at the position)
//***
//*** returns TRUE if a new Marker is added or FALSE if
//*** a Marker exists and it's color was changed
//***
//*** created from: RSC				date: 12.03.01
//***
//*** changes:
//*** vers.		date		who		what
//***
//**********************************************************
BOOL CUGanttCtrl::SetTimeMarker(DATE Time, long Color) 
{
  int            ilPos = 0;
  CUGanttStatic *polTimeMarker;
  CRect          olRect = omGanttRect;
  
// Look if there is already a marker for this time
  ilPos = GetTimeMarker(Time);

  if (ilPos < 0)
  {
// No, there isn't !
    polTimeMarker = new CUGanttStatic();
    olRect.left = GetXFromTime(Time);
    olRect.right = olRect.left + 1;
    polTimeMarker->omBkColor = Color;
    polTimeMarker->omTime = Time;
    polTimeMarker->Create(NULL, WS_CHILD | SS_SIMPLE, olRect, this, 0);

// Show it only if it is in the gantt area:
    if (TimeMarkerVisible(polTimeMarker) != FALSE)
    {
      polTimeMarker->ShowWindow(SW_SHOWNORMAL);
    }
    else
    {
      polTimeMarker->ShowWindow(SW_HIDE);
    }

// Add it to Time Marker array
    omTimeMarkers.Add(polTimeMarker);

    return TRUE;
  }
  else
  {
// Yes, there is, so change color
    polTimeMarker = &omTimeMarkers[ilPos];
    polTimeMarker->omBkColor = Color;
    polTimeMarker->Invalidate();
    // Set color here

    return FALSE;
  }
}


//**********************************************************
//*** deletes an existing time marker
//***
//*** returns TRUE if successful and FALSE if the time
//*** marker don't exist
//***
//*** created from: RSC				date: 12.03.01
//***
//*** changes:
//*** vers.		date		who		what
//***
//**********************************************************
BOOL CUGanttCtrl::DeleteTimeMarker(DATE Time) 
{
  int ilPos = 0;

  // Look if there is a merker for this time
  ilPos = GetTimeMarker(Time);

  if (ilPos >= 0)
  {
    // Yes, there isn't !
    omTimeMarkers.DeleteAt(ilPos);

    return TRUE;
  }
  else
  {
    // No, there isn't
    return FALSE;
  }
}

//**********************************************************
//*** removes all time markers
//***
//*** created from: RSC				date: 12.03.01
//***
//*** changes:
//*** vers.		date		who		what
//***
//**********************************************************
void CUGanttCtrl::DeleteAllTimeMarkers() 
{
  omTimeMarkers.DeleteAll();
}

long CUGanttCtrl::GetTimeMarker(DATE Time) 
{
  int            ilPos = 0;
  COleDateTime   olTime = Time;

  // Look if there is a merker for this time
  while ((ilPos < omTimeMarkers.GetSize()) &&
         (omTimeMarkers[ilPos].omTime != olTime))
  {
    ilPos++;
  }


  if (ilPos >= omTimeMarkers.GetSize())
  {
    ilPos = -1;
  }

	return ilPos;
}


//**********************************************************
//*** checks, if the time marker is in the Gantt Area
//*** (is visible)
//***
//*** created from: RSC				date: 12.03.01
//***
//*** changes:
//*** vers.		date		who		what
//***
//**********************************************************
BOOL CUGanttCtrl::TimeMarkerVisible(CUGanttStatic *popTimeMarker)
{
  CRect olRect;
  BOOL blRC = TRUE;

  popTimeMarker->GetClientRect(&olRect);
  popTimeMarker->ClientToScreen(&olRect);
  ScreenToClient(&olRect);

  if ((olRect.left < omGanttRect.left) ||
      (olRect.left > omGanttRect.right))
  {
    blRC = FALSE;
  }

  return blRC;
}


void CUGanttCtrl::OnScrollDelayChanged() 
{
	// TODO: Add notification handler code

	SetModifiedFlag();
}

void CUGanttCtrl::OnScrollInsetChanged() 
{
	// TODO: Add notification handler code

	SetModifiedFlag();
}

void CUGanttCtrl::OnScrollIntervalChanged() 
{
	// TODO: Add notification handler code

	SetModifiedFlag();
}

void CUGanttCtrl::OnBarFontNameChanged() 
{
/*	m_barFontName  = CString(lpszNewValue);
	omTabHeaderFont.DeleteObject();
	if(m_barFontBold  == TRUE)
	{
		omTabHeaderFont.CreateFont( m_barFontSize , 0, 0, 0, FW_BOLD, 0, 0, 0, 0, OUT_CHARACTER_PRECIS, CLIP_DEFAULT_PRECIS, 
								 PROOF_QUALITY, DEFAULT_PITCH, m_barFontName  );
	}
	else
	{
		omTabHeaderFont.CreateFont( m_barFontSize, 0, 0, 0, FW_NORMAL, 0, 0, 0, 0, OUT_CHARACTER_PRECIS, CLIP_DEFAULT_PRECIS, 
								 PROOF_QUALITY, DEFAULT_PITCH, m_barFontName );
	}
	SetModifiedFlag();
	InvalidateControl();
*/
	SetModifiedFlag();
}

void CUGanttCtrl::OnBarFontSizeChanged() 
{
	// TODO: Add notification handler code
/*	omTabHeaderFont.DeleteObject();
	if(m_barFontBold  == TRUE)
	{
		omTabHeaderFont.CreateFont( m_barFontSize , 0, 0, 0, FW_BOLD, 0, 0, 0, 0, OUT_CHARACTER_PRECIS, CLIP_DEFAULT_PRECIS, 
								 PROOF_QUALITY, DEFAULT_PITCH, m_barFontName  );
	}
	else
	{
		omTabHeaderFont.CreateFont( m_barFontSize, 0, 0, 0, FW_NORMAL, 0, 0, 0, 0, OUT_CHARACTER_PRECIS, CLIP_DEFAULT_PRECIS, 
								 PROOF_QUALITY, DEFAULT_PITCH, m_barFontName );
	}
	SetModifiedFlag();
	InvalidateControl();
*/
}

void CUGanttCtrl::OnTabBodyFontSizeChanged() 
{
	SetModifiedFlag();
	omTabFont.DeleteObject();
	if(m_tabBodyFontBold == TRUE)
	{
		omTabFont.CreateFont( TabFontSize, 0, 0, 0, FW_BOLD, 0, 0, 0, DEFAULT_CHARSET, OUT_CHARACTER_PRECIS, CLIP_DEFAULT_PRECIS, 
								 PROOF_QUALITY, DEFAULT_PITCH, m_tabFontName  );
	}
	else
	{
		omTabFont.CreateFont( TabFontSize, 0, 0, 0, FW_NORMAL, 0, 0, 0, DEFAULT_CHARSET, OUT_CHARACTER_PRECIS, CLIP_DEFAULT_PRECIS, 
								 PROOF_QUALITY, DEFAULT_PITCH, m_tabFontName );
	}
	SetModifiedFlag();
	InvalidateControl();

	SetModifiedFlag();
}

void CUGanttCtrl::OnTabHeaderFontBoldChanged() 
{
	SetModifiedFlag();
	omTabHeaderFont.DeleteObject();
	if(m_tabHeaderFontBold == TRUE)
	{
		omTabHeaderFont.CreateFont( TabHeaderFontSize  , 0, 0, 0, FW_BOLD, 0, 0, 0, DEFAULT_CHARSET, OUT_CHARACTER_PRECIS, CLIP_DEFAULT_PRECIS, 
								 PROOF_QUALITY, DEFAULT_PITCH, TabHeaderFontName  );
	}
	else
	{
		omTabHeaderFont.CreateFont( TabHeaderFontSize , 0, 0, 0, FW_NORMAL, 0, 0, 0, DEFAULT_CHARSET, OUT_CHARACTER_PRECIS, CLIP_DEFAULT_PRECIS, 
								 PROOF_QUALITY, DEFAULT_PITCH, TabHeaderFontName  );
	}
	SetModifiedFlag();
	InvalidateControl();

}

void CUGanttCtrl::OnTabBodyFontBoldChanged() 
{
	omTabFont.DeleteObject();
	if(m_tabBodyFontBold == TRUE)
	{
		omTabFont.CreateFont( TabFontSize   , 0, 0, 0, FW_BOLD, 0, 0, 0, DEFAULT_CHARSET, OUT_CHARACTER_PRECIS, CLIP_DEFAULT_PRECIS, 
								 PROOF_QUALITY, DEFAULT_PITCH, m_tabFontName   );
	}
	else
	{
		omTabFont.CreateFont( TabFontSize , 0, 0, 0, FW_NORMAL, 0, 0, 0, DEFAULT_CHARSET, OUT_CHARACTER_PRECIS, CLIP_DEFAULT_PRECIS, 
								 PROOF_QUALITY, DEFAULT_PITCH, m_tabFontName  );
	}
	SetModifiedFlag();
	InvalidateControl();


	SetModifiedFlag();
}

void CUGanttCtrl::OnTabFontNameChanged() 
{
	omTabFont.DeleteObject();
	if(m_tabBodyFontBold == TRUE)
	{
		omTabFont.CreateFont( TabFontSize   , 0, 0, 0, FW_BOLD, 0, 0, 0, DEFAULT_CHARSET, OUT_CHARACTER_PRECIS, CLIP_DEFAULT_PRECIS, 
								 PROOF_QUALITY, DEFAULT_PITCH, m_tabFontName   );
	}
	else
	{
		omTabFont.CreateFont( TabFontSize , 0, 0, 0, FW_NORMAL, 0, 0, 0, DEFAULT_CHARSET, OUT_CHARACTER_PRECIS, CLIP_DEFAULT_PRECIS, 
								 PROOF_QUALITY, DEFAULT_PITCH, m_tabFontName  );
	}
	SetModifiedFlag();
	InvalidateControl();


	SetModifiedFlag();
}

void CUGanttCtrl::OnBarFontBoldChanged() 
{
	// TODO: Add notification handler code

	SetModifiedFlag();
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// - created from:	MWO		date:	25.04.2002
// -
// - changes:		who		when		what
// -
// - description:	Now it's possible to set triangles in each corner of a cell wit pixel width and color
//					Moreover its possible to draw bars on the left and right side of a cell
//
// - parameters:	ID:	Unique ID of the object
//					Locationlist: e.g. "TL,BL,BR,BL,L,R,BTL,BTR,T,B"
//						TL=TopLeft,BL=BottomLeft,BR=BottomRight,BL=BottomLeft,L=Bar Left,R=Bar Right
//						BTL=BigTriagleLeft,BTR=BigTriangleRight,T=BarTop,B=BarBottom
//					PixelList: comma separated list, leg of triangle or width of the bar in pixels
//					ColorList: Comma separated colors
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
void CUGanttCtrl::DecorationObjectCreate(LPCTSTR ID, LPCTSTR LocationList, LPCTSTR PixelList, LPCTSTR ColorList) 
{
	int					i;
	BOOL				blDoesExist		= FALSE;
	DECORATION_OBJECT*	polDecoObj		= NULL;
	int ilCount1 = 0, 
		ilCount2 = 0, 
		ilCount3 = 0;
	CStringArray olLocationArr,
				 olPixelArr,
				 olColorArr;

	for (i = 0; i < omDecoObjects.GetSize(); i++)
	{
		if (omDecoObjects[i].ID == ID)
		{
			blDoesExist = TRUE;
			polDecoObj = &omDecoObjects[i];
		}
	}

	if (blDoesExist != TRUE)
	{
		polDecoObj = new DECORATION_OBJECT;
		omDecoObjects.Add (polDecoObj);
	}

	ExtractTextLineFast(polDecoObj->PixelList, (char *)PixelList, ",");
	ExtractTextLineFast(polDecoObj->LocationList, (char *)LocationList, ",");
	ExtractTextLineFast(polDecoObj->ColorList, (char *)ColorList, ",");
	polDecoObj->ID = ID;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// - created from:	MWO		date:	25.04.2002
// -
// - changes:		who		when		what
// -
BarData* CUGanttCtrl::GetBarByKey(LPCTSTR opKey)
{
	BarData* polBar = NULL;
	mapBars.Lookup(opKey, (void*&)polBar);
	return polBar;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// - created from:	MWO		date:	25.04.2002
// -
// - changes:		who		when		what
// -
BarData* CUGanttCtrl::GetBkBarByKey(LPCTSTR opKey)
{
	BarData* polBar = NULL;
	mapBkBars.Lookup(opKey, (void*&)polBar);
	return polBar;
}


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// - created from:	MWO		date:	25.04.2002
// -
// - changes:		who		when		what
// -
void CUGanttCtrl::DecorationObjectSetToBar(LPCTSTR ID, LPCTSTR BarKey) 
{
	BarData *polBar;
	DECORATION_OBJECT*	polDecoObj = NULL;
	polBar = GetBarByKey(BarKey);
	if(polBar != NULL)
	{
		polDecoObj = GetDecorationObject(ID);
		if(polDecoObj != NULL)
		{
			mapBarDecoObjects.SetAt(BarKey, (void*)polDecoObj);
		}
	}
	
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// - created from:	MWO		date:	25.04.2002
// -
// - changes:		who		when		what
// -
void CUGanttCtrl::DecorationObjectSetToBkBar(LPCTSTR ID, LPCTSTR BkBarKey) 
{
	BarData *polBar;
	DECORATION_OBJECT*	polDecoObj = NULL;
	polBar = GetBkBarByKey(BkBarKey);
	if(polBar != NULL)
	{
		polDecoObj = GetDecorationObject(ID);
		if(polDecoObj != NULL)
		{
			mapBkBarDecoObjects.SetAt(BkBarKey, (void*)polDecoObj);
		}
	}
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// - created from:	MWO		date:	25.04.2002
// -
// - changes:		who		when		what
// -
DECORATION_OBJECT* CUGanttCtrl::GetDecorationObject(LPCTSTR ID)
{
	DECORATION_OBJECT *polDeco = NULL;
	for(int i = 0; i < omDecoObjects.GetSize(); i++)
	{
		if(omDecoObjects[i].ID == ID)
		{ 
			return &omDecoObjects[i];
		}
	}
	return NULL;
}

void CUGanttCtrl::DecorationObjectResetBar(LPCTSTR ID, LPCTSTR BarKey) 
{
	DECORATION_OBJECT *polDecoObj = NULL;
	if (mapBarDecoObjects.Lookup (BarKey, (void*&)polDecoObj) == TRUE)
	{
		mapBarDecoObjects.RemoveKey(BarKey);
		bmDrawGANTT = TRUE;
		InvalidateControl();
	}
}

void CUGanttCtrl::DecorationObjectResetBkBar(LPCTSTR ID, LPCTSTR BkBarKey) 
{
	// TODO: Add your dispatch handler code here

}

BSTR CUGanttCtrl::DecorationObjectsGetByBarKey(LPCTSTR BarKey) 
{
	CString strResult;
	// TODO: Add your dispatch handler code here

	return strResult.AllocSysString();
}

BSTR CUGanttCtrl::DecorationObjectsGetByBkBarKey(LPCTSTR BkBarKey) 
{
	CString strResult;
	// TODO: Add your dispatch handler code here

	return strResult.AllocSysString();
}

void CUGanttCtrl::OnSubbarStackCountChanged() 
{
	// TODO: Add notification handler code

	SetModifiedFlag();
}

void CUGanttCtrl::AddSubBar(LPCTSTR MainBarKey, LPCTSTR Key, DATE StartTime, DATE EndTime, LPCTSTR BarText, long Shape, long BackColor, long TextColor, long SplitColor, long LeftColor, long RightColor, LPCTSTR LeftOutsideText, LPCTSTR RightOutsideText, long LeftTextColor, long RightTextColor) 
{
	BarData *polMainBar = NULL;
	polMainBar = GetBarByKey(MainBarKey);
	if(polMainBar != NULL)
	{
		BarData *polBar	= new BarData;

		polBar->LineNo			 = polMainBar->LineNo;
		polBar->LineKey			 = omLines[polMainBar->LineNo].Key;
		polBar->MainbarKey		 = MainBarKey;
		polBar->Key				 = CString (Key);
		polBar->Begin			 = COleDateTime (StartTime);
		polBar->End				 = COleDateTime (EndTime);
		polBar->BarText			 = CString (BarText);
		polBar->Shape			 = Shape;
		polBar->BackColor		 = BackColor;
		polBar->TextColor		 = TextColor;
		polBar->SplitColor		 = SplitColor;
		polBar->LeftBackColor	 = LeftColor;
		polBar->RightBackColor	 = RightColor;
		polBar->LeftOutsideText  = LeftOutsideText;
		polBar->RightOutsideText = RightOutsideText;
		polBar->LeftTextColor	 = LeftTextColor;
		polBar->RightTextColor	 = RightTextColor;
		polBar->OverlapLevel	 = polMainBar->OverlapLevel + 1;
		polBar->isSubbar = true;

		polMainBar->omSubBars.Add(polBar);
		mapSubBars.SetAt((const char*)polBar->Key.GetBuffer(0), (void*)polBar);

		UpdateSingleRect (Key);
	}
}

void CUGanttCtrl::RemoveSubBar(LPCTSTR MainKey, LPCTSTR Key) 
{
	BarData *polMainBar = NULL;
	BarData *polSubBar = NULL;

	polMainBar = GetBarByKey(MainKey);
	if(polMainBar != NULL)
	{
		for (int i = 0; i < polMainBar->omSubBars.GetSize(); i++)
		{
			polSubBar = &polMainBar->omSubBars[i];
			if (polSubBar->Key == CString (Key))
			{
				mapSubBars.RemoveKey (Key);
				polMainBar->omSubBars.RemoveAt(i);
				return;
			}
		}
	}
}

void CUGanttCtrl::OnUTCOffsetInHoursChanged() 
{
	omCurrentTime = this->GetCurrentTime();
	pomTimeScale->UpdateCurrentTimeLine(omCurrentTime);

	bmDrawGANTT = TRUE;
	InvalidateControl();	

	SetModifiedFlag();
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// - delivers true, if the two bars are overlapping
// -
// - created from:	RRO		date:	22.08.2002
// -
// - changes:		who		when		what
// -
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
bool CUGanttCtrl::DoBarsOverlap(BarData *ropBar1, BarData *ropBar2)
{
	COleDateTime olStartTime1	= ropBar1->Begin;
	COleDateTime olEndTime1		= ropBar1->End;
	COleDateTime olStartTime2	= ropBar2->Begin;
	COleDateTime olEndTime2		= ropBar2->End;

	BOOL blIsReallyOverlapped	= IsReallyOverlapped (olStartTime1, olEndTime1, olStartTime2, olEndTime2);
	BOOL blIsWithIn				= IsWithIn (olStartTime1, olEndTime1, olStartTime2, olEndTime2);

	if (blIsReallyOverlapped || blIsWithIn)
	{
		return true;
	}
	else
	{
		return false;
	}
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// - move subbars with their main-bar
// -
// - created from:	RRO		date:	30.08.2002
// -
// - changes:		who		when		what
// -
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
void CUGanttCtrl::OnAutoMoveSubBarsWithBarChanged() 
{
	SetModifiedFlag();
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// - if we are scrolling we can let the line-hight adapt automatically to the max. overlap-level
// - of the lines visible and their bars in the visible time-frame.
// -
// - created from:	RRO		date:	03.09.2002
// -
// - changes:		who		when		what
// -
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
void CUGanttCtrl::OnAutoSizeLineHeightToFitChanged() 
{
	SetModifiedFlag();
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// - as we need the same current time in the gantt and in the timescale, we can call this function
// - and store the result in the member "omCurrentTime"
// -
// - created from:	RRO		date:	03.09.2002
// -
// - changes:		who		when		what
// -
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
COleDateTime CUGanttCtrl::GetCurrentTime()
{
	COleDateTime olCurrTime = COleDateTime::GetCurrentTime();;

	COleDateTimeSpan olDiff;
	olDiff.SetDateTimeSpan(0, m_uTCOffsetInHours, 0, 0);

	olCurrTime += olDiff;

	return olCurrTime;
}

long CUGanttCtrl::GetLineNoByKey(LPCTSTR Key) 
{
	for (long i = 0; i < omLines.GetSize(); i++)
	{
		if(omLines[i].Key == Key)
		{
			return i;
		}
	}
	return -1;

}


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// - to get a visible grouping the column in tab and the alternal color are set
// - this means, for the same value as the line before no line separator will be 
// - painted and the column value will not be painted ==> this is like a tree view
// - and store the result in the member "omCurrentTime"
// - created from:	MWO		date:	11.02.2002
// -
// - changes:		who		when		what
// -
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
void CUGanttCtrl::TabSetGroupColumn(short ColNo, long AlternateColor) 
{
	if(ColNo < omGroupCols.GetSize() && ColNo < omGroupAlternateColor.GetSize())
	{
		omGroupCols[ColNo] = 0;
		omGroupAlternateColor[ColNo] = AlternateColor;
	}
	InvalidateControl();
}

void CUGanttCtrl::TabResetAllGroups() 
{
	for( int i = 0; i < omGroupCols.GetSize(); i++)
	{
		omGroupCols[i] = -1;
	}
	InvalidateControl();
}

void CUGanttCtrl::TabResetGroupsColumn(short ColNo) 
{
	if(ColNo < omGroupCols.GetSize() && ColNo < omGroupAlternateColor.GetSize())
	{
		omGroupCols[ColNo] = -1;
		omGroupAlternateColor[ColNo] = -1;
	}
	InvalidateControl();
}


void CUGanttCtrl::SortTab(LPCTSTR ColNo, BOOL bAscending, BOOL bCaseSensitive) 
{
	CUIntArray		olSortColumns;
	CUIntArray		olColWidth;
	CUIntArray		olFillRight;

	CStringArray	olColumns;
	CStringArray	olStrArr;
	CStringArray	olFillArr;

	CString			olTmpStr;

	//fill in the columns you want to sort
	
	omLastSortOrder.RemoveAll();
	ExtractTextLineFast (olColumns, (char*)ColNo, ",");
	int ilColNo;
	int i;
	for ( i = 0; i < olColumns.GetSize(); i++)
	{
		ilColNo = atoi(olColumns[i]);
		if ((ilColNo  < omTabHeader.GetSize()) && (ilColNo > -1))
		{
			olSortColumns.Add (ilColNo);
			omLastSortOrder.Add (ilColNo);
		}
	}

	//get the number of characters in the columns
/*from TAB.ocx	
	olTmpStr = m_columnWidthString;
	ExtractTextLineFast (olStrArr, olTmpStr.GetBuffer(0), ",");
	for (i = 0; i < olStrArr.GetSize(); i++)
	{
		olColWidth.Add (atoi (olStrArr[i].GetBuffer(0)));
	}
End from TAB.ocx*/
	while(olColWidth.GetSize() < omTabHeader.GetSize())
	{
		olColWidth.Add (20);	//default value
	}

	//look if you have to fill it up at the begin or at the end
/*from TAB.ocx	
	olTmpStr = m_columnAlignmentString;
	ExtractTextLineFast (olFillArr, olTmpStr.GetBuffer(0), ",");
	for (i = 0; i < olFillArr.GetSize(); i++)
	{
		if (olFillArr[i] != "L")
		{
			olFillRight.Add (1);
		}
		else
		{
			olFillRight.Add (0);
		}
	}
End from TAB.ocx*/
	while(olFillRight.GetSize() < omTabHeader.GetSize())
	{
		olFillRight.Add (0);	//default value
	}

	//build the sort-string
	setlocale( LC_COLLATE, "deu" );
/*	if (m_setLocaleLanguageString = "")
	{
		setlocale( LC_COLLATE, "deu" );
	}
	else
	{
		setlocale( LC_COLLATE, m_setLocaleLanguageString );
	}
*/
	int ilStrLen;
	for (i = 0; i < omLines.GetSize(); i++)
	{
		char *cpResult;
		CString	strTmp;
		CString strSort;
		strSort.Format ("%6ld", i);

		for (int j = olSortColumns.GetSize() - 1; j > -1; j--)
		{
			cpResult = (char*)malloc(omLines[i].TabValues.GetLength() + 1);
			GetDataItem (cpResult, omLines[i].TabValues.GetBuffer(0), (int)olSortColumns[j] + 1, ',' ,"","\0\0");
			strTmp = _T(cpResult);
			free (cpResult);

			ilStrLen = strTmp.GetLength();

			if (ilStrLen > (int)olColWidth[olSortColumns[j]])
			{
				strSort.Insert (0, strTmp.Left(olColWidth[olSortColumns[j]]));
			}
			else
			{

				if (olFillRight[olSortColumns[j]] != 1)	//fill up with SPACE at the right
				{
					for (int k = ilStrLen; k < (int)olColWidth[(int)olSortColumns[j]]; k++)
					{
						strTmp += CString(" ");
					}
				}
				else									//fill up with SPACE at the left
				{
					for (int k = ilStrLen; k < (int)olColWidth[(int)olSortColumns[j]]; k++)
					{
						strTmp.Insert (0," ");
					}
				}
				strSort.Insert (0, strTmp);
			}
		}
		char *cpSrc = strSort.GetBuffer(0);
		char *cpDst;
		char buf[1000], *bufp = buf;
		int ilSizeT = 1 + strxfrm( bufp, cpSrc, 0 );
		cpDst = (char*)malloc(ilSizeT + 2);
		strxfrm(cpDst, (const char*)cpSrc, (size_t)(ilSizeT));
		omLines[i].SortString = _T(cpDst);
		free (cpDst);
	}

	//now you can sort!
	if (bAscending)
	{
		if (bCaseSensitive)
		{
			omLines.Sort (CompareAscendingCase);
		}
		else
		{
			omLines.Sort (CompareAscendingNoCase);
		}
	}
	else
	{
		if (bCaseSensitive)
		{
			omLines.Sort (CompareDescendingCase);
		}
		else
		{
			omLines.Sort (CompareDescendingNoCase);
		}
	}
//	bmTabIsSorted = true;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// - This method returns the column value for LinNo, ColNo
// -
// - created from:	MWO		date:	12.02.03
// -
// - changes:		who		when		what
// -
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
BSTR CUGanttCtrl::TabGetColumnValue(long LineNo, long ColNo) 
{
	CString strResult;
	char *cpResult;

	if (LineNo > -1)
	{
		if (LineNo < omLines.GetSize())
		{
			cpResult = (char*)malloc(omLines[LineNo].TabValues.GetLength() + 1);
			GetDataItem (cpResult, omLines[LineNo].TabValues.GetBuffer(0), ColNo + 1, ',',"","\0\0");
			strResult = _T(cpResult);
			free (cpResult);
		}
	}

	return strResult.AllocSysString();

}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// - This method returns key for a line
// -
// - created from:	MWO		date:	12.02.03
// -
// - changes:		who		when		what
// -
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
BSTR CUGanttCtrl::TabGetLineKey(long LineNo) 
{
	CString strResult;
	if (LineNo > -1)
	{
		if (LineNo < omLines.GetSize())
		{
			strResult = omLines[LineNo].Key;
		}
	}
	return strResult.AllocSysString();
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// - This method returns the line comma separated
// -
// - created from:	MWO		date:	12.02.03
// -
// - changes:		who		when		what
// -
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
BSTR CUGanttCtrl::TabGetLineValues(long LineNo) 
{
	CString strResult;
	if((LineNo >= 0) && (LineNo < omLines.GetSize()))
	{
		strResult = omLines[LineNo].TabValues;
	}
	return strResult.AllocSysString();
}


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// - This method defines a triangle as a special kind of decoration object and
// -	assigns it directly to a bar. If the object still exists with the same color
//      the existing object will be used otherwise it will be allocated.
//	 You can use this method to assign one element to different bar. They will not be reallocated.
//   This is just for easy and comfortable using.
// -
// - created from:	MWO		date:	12.02.03
// -
// - changes:		who		when		what
// -
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
void CUGanttCtrl::SetBarTriangles(LPCTSTR BarKey, LPCTSTR TriangleID, long LeftColor, long RightColor, BOOL FullTriangle) 
{
	int					i;
	BOOL				blDoesExist		= FALSE;
	DECORATION_OBJECT*	polDecoObj		= NULL;

// *** First create the decoobject internally
	for (i = 0; i < omDecoObjects.GetSize(); i++)
	{
		if (omDecoObjects[i].ID == TriangleID)
		{
			blDoesExist = TRUE;
			polDecoObj = &omDecoObjects[i];
		}
	}

	if (blDoesExist != TRUE)
	{
		polDecoObj = new DECORATION_OBJECT;
		omDecoObjects.Add (polDecoObj);
	}
	if(FullTriangle == TRUE)
	{
		polDecoObj->AsFull = true;
	}
	else
	{
		polDecoObj->AsFull = false;
	}
	polDecoObj->AsTriangle = true;
	polDecoObj->TriangleLeftColor = LeftColor;
	polDecoObj->TriangleRightColor = RightColor;
	polDecoObj->ID = TriangleID;

//*** Second map it to a bar
	BarData *polBar;
	polBar = GetBarByKey(BarKey);
	if(polBar != NULL)
	{
		if(polDecoObj != NULL)
		{
			mapBarTriangleObjects.SetAt(BarKey, (void*)polDecoObj);
		}
	}
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// - This method returns the start time of a bar with key
// -
// - created from:	MWO		date:	07.05.2004
// -
// - changes:		who		when		what
// -
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
DATE CUGanttCtrl::GetBarStartTime(LPCTSTR BarKey) 
{
	COleDateTime myRet;
	BarData* polBar = NULL;
	mapBars.Lookup(BarKey, (void*&)polBar);
	if(polBar != NULL)
	{
		myRet = polBar->Begin;
	}

	return (DATE) myRet.m_dt;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// - This method returns the end time of a bar with key
// -
// - created from:	MWO		date:	07.05.2004
// -
// - changes:		who		when		what
// -
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
DATE CUGanttCtrl::GetBarEndTime(LPCTSTR BarKey) 
{
	COleDateTime myRet;
	BarData* polBar = NULL;
	mapBars.Lookup(BarKey, (void*&)polBar);
	if(polBar != NULL)
	{
		myRet = polBar->End;
	}

	return (DATE) myRet.m_dt;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// - This method returns the start time of a background bar with key
// -
// - created from:	MWO		date:	07.05.2004
// -
// - changes:		who		when		what
// -
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
DATE CUGanttCtrl::GetBkBarStartTime(LPCTSTR BarKey) 
{
	COleDateTime myRet;
	BarData* polBar = NULL;
	mapBkBars.Lookup(BarKey, (void*&)polBar);
	if(polBar != NULL)
	{
		myRet = polBar->Begin;
	}

	return (DATE) myRet.m_dt;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// - This method returns the end time of a background bar with key
// -
// - created from:	MWO		date:	07.05.2004
// -
// - changes:		who		when		what
// -
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
DATE CUGanttCtrl::GetBkBarEndTime(LPCTSTR BarKey) 
{
	COleDateTime myRet;
	BarData* polBar = NULL;
	mapBkBars.Lookup(BarKey, (void*&)polBar);
	if(polBar != NULL)
	{
		myRet = polBar->End;
	}

	return (DATE) myRet.m_dt;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// - This method returns the BAR_RECORD with key
// -
// - created from:	MWO		date:	09.05.2003
// -
// - changes:		who		when		what
// -
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
VARIANT CUGanttCtrl::GetBarRecord(LPCTSTR BarKey) 
{
	VARIANT vaResult;
	VariantInit(&vaResult);
	SAFEARRAYBOUND sab;
	IRecordInfo *pRI = NULL;
	SAFEARRAY *pSa;
	HRESULT hr;
	GUID reglib = {0x6782E133, 0x3223, 0x11D4, { 0x99, 0x6A, 0x0, 0x0, 0x86, 0x3D, 0xE9, 0x5C}};
	hr = GetRecordInfoFromGuids(reglib, 1, 0, 0x409, BAR_RECORD_GUID, &pRI);
	if (FAILED(hr))
		AfxMessageBox("hr failed");

	sab.cElements = 1;
	sab.lLbound = 0;
	hr = GetRecordInfoFromGuids(reglib, 1, 0, 0x409, BAR_RECORD_GUID, &pRI);
	
	pSa = SafeArrayCreateEx(VT_RECORD, 1, &sab, pRI);
	if(pRI != NULL)
		pRI->Release();
	PVOID pvData;
	SafeArrayAccessData(pSa, &pvData );
	struct _BAR_RECORD *pmyTf;
	pmyTf = (struct _BAR_RECORD *)pvData;
	BarData* polBar = NULL;
	mapBars.Lookup(BarKey, (void*&)polBar);
	if (polBar != NULL)
	{

		pmyTf[0].Key				= polBar->Key.AllocSysString(); 
		pmyTf[0].LineKey			= polBar->LineKey.AllocSysString();
		pmyTf[0].LineNo				= polBar->LineNo;
		pmyTf[0].Begin				= polBar->Begin.m_dt;
		pmyTf[0].End				= polBar->End.m_dt;
		pmyTf[0].BarText			= polBar->BarText.AllocSysString();
		pmyTf[0].LeftOutsideText	= polBar->LeftOutsideText.AllocSysString();
		pmyTf[0].RightOutsideText	=polBar->RightOutsideText.AllocSysString();
		pmyTf[0].Shape				= polBar->Shape;
		pmyTf[0].BackColor			= (long)polBar->BackColor;
		pmyTf[0].LeftBackColor		= (long)polBar->LeftBackColor;
		pmyTf[0].RightBackColor		= (long)polBar->RightBackColor;
		pmyTf[0].TextColor			= (long)polBar->TextColor;
		pmyTf[0].LeftTextColor		= (long)polBar->LeftTextColor;
		pmyTf[0].RightTextColor		= (long)polBar->RightTextColor;
		pmyTf[0].OverlapLevel		= polBar->OverlapLevel;
		pmyTf[0].SplitColor			= polBar->SplitColor;
		pmyTf[0].WithTextOutside	= polBar->WithTextOutside;
		pmyTf[0].WithArrowFrom		= polBar->WithArrowFrom;
		pmyTf[0].WithArrowTo		= polBar->WithArrowTo;
		pmyTf[0].MainbarKey			= polBar->MainbarKey.AllocSysString();

		DECORATION_OBJECT *polTriangleDecoObj = NULL;
		mapBarTriangleObjects.Lookup (polBar->Key.GetBuffer(0), (void*&)polTriangleDecoObj);
		if(polTriangleDecoObj != NULL)
		{
			pmyTf[0].leftTriangleColor = polTriangleDecoObj->TriangleLeftColor;
			pmyTf[0].rightTriangleColor = polTriangleDecoObj->TriangleRightColor;
		}
		if(polBar->isSubbar == true)
		{
			pmyTf[0].isSubbar			= TRUE;
		}
		else
		{
			pmyTf[0].isSubbar			= FALSE;
		}
	}
	else
	{
		pmyTf[0].Key = CString("").AllocSysString();
	}
	SafeArrayUnaccessData(pSa);
	V_VT(&vaResult) = VT_ARRAY|VT_RECORD;
	V_ARRAY(&vaResult) = pSa;
	return vaResult;

/*	VARIANT vaResult;
	VariantInit(&vaResult);
	IRecordInfo *pRI;
	HRESULT hr;
	GUID reglib = {0x6782E133, 0x3223, 0x11D4, { 0x99, 0x6A, 0x0, 0x0, 0x86, 0x3D, 0xE9, 0x5C}};
	hr = GetRecordInfoFromGuids(reglib, 1, 0, 0x409, BAR_RECORD_GUID, &pRI);
	if (FAILED(hr))
		AfxMessageBox("hr failed");

	BarData* polBar = NULL;
	mapBars.Lookup(BarKey, (void*&)polBar);
	if(polBar != NULL)
	{
		CopyBarToOleBar(&omOleBar, polBar);
	}
	vaResult.vt = VT_RECORD;
	vaResult.pvRecord = pRI->RecordCreate();
	vaResult.pRecInfo = pRI;
	pRI->RecordCopy(&omOleBar, vaResult.pvRecord);
	pRI = NULL;
	return vaResult;
*/
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// - Initializes the BAR_RECORD structure to be passed by OLE
// -
// - created from:	MWO		date:	09.05.2003
// -
// - changes:		who		when		what
// -
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
void CUGanttCtrl::InitOleBar(struct _BAR_RECORD *pOleBar)
{
	pOleBar->Key				= SysAllocString(L""); 
	pOleBar->LineKey			= SysAllocString(L""); 
	pOleBar->LineNo				= -1;
	pOleBar->Begin				= COleDateTime::GetCurrentTime().m_dt;
	pOleBar->End				= COleDateTime::GetCurrentTime().m_dt;
	pOleBar->BarText			= SysAllocString(L""); 
	pOleBar->LeftOutsideText	= SysAllocString(L""); 
	pOleBar->RightOutsideText	= SysAllocString(L""); 
	pOleBar->Shape				= -1;
	pOleBar->BackColor			= -1;
	pOleBar->LeftBackColor		= -1;
	pOleBar->RightBackColor		= -1;
	pOleBar->TextColor			= -1;
	pOleBar->LeftTextColor		= -1;
	pOleBar->RightTextColor		= -1;
	pOleBar->OverlapLevel		= -1;
	pOleBar->leftTriangleColor  = -1;
	pOleBar->rightTriangleColor = -1;
	pOleBar->SplitColor			= FALSE;
	pOleBar->WithTextOutside	= FALSE;
	pOleBar->WithArrowFrom		= FALSE;
	pOleBar->WithArrowTo		= FALSE;
	pOleBar->MainbarKey			= SysAllocString(L""); 
	pOleBar->isSubbar			= FALSE;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// - This method copies the BarData to BAR_RECORD to get them passed by OLE
// -
// - created from:	MWO		date:	09.05.2003
// -
// - changes:		who		when		what
// -
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
void CUGanttCtrl::CopyBarToOleBar(struct _BAR_RECORD *pOleBar, BarData *pBar)
{
	pOleBar->Key				= pBar->Key.AllocSysString(); 
	pOleBar->LineKey			= pBar->LineKey.AllocSysString();
	pOleBar->LineNo				= pBar->LineNo;
	pOleBar->Begin				= pBar->Begin.m_dt;
	pOleBar->End				= pBar->End.m_dt;
	pOleBar->BarText			= pBar->BarText.AllocSysString();
	pOleBar->LeftOutsideText	= pBar->LeftOutsideText.AllocSysString();
	pOleBar->RightOutsideText	=pBar->RightOutsideText.AllocSysString();
	pOleBar->Shape				= pBar->Shape;
	pOleBar->BackColor			= (long)pBar->BackColor;
	pOleBar->LeftBackColor		= (long)pBar->LeftBackColor;
	pOleBar->RightBackColor		= (long)pBar->RightBackColor;
	pOleBar->TextColor			= (long)pBar->TextColor;
	pOleBar->LeftTextColor		= (long)pBar->LeftTextColor;
	pOleBar->RightTextColor		= (long)pBar->RightTextColor;
	pOleBar->OverlapLevel		= pBar->OverlapLevel;
	pOleBar->SplitColor			= pBar->SplitColor;
	pOleBar->WithTextOutside	= pBar->WithTextOutside;
	pOleBar->WithArrowFrom		= pBar->WithArrowFrom;
	pOleBar->WithArrowTo		= pBar->WithArrowTo;
	pOleBar->MainbarKey			= pBar->MainbarKey.AllocSysString();

	DECORATION_OBJECT *polTriangleDecoObj = NULL;
	mapBarTriangleObjects.Lookup (pBar->Key.GetBuffer(0), (void*&)polTriangleDecoObj);
	if(polTriangleDecoObj != NULL)
	{
		pOleBar->leftTriangleColor = polTriangleDecoObj->TriangleLeftColor;
		pOleBar->rightTriangleColor = polTriangleDecoObj->TriangleRightColor;
	}
	if(pBar->isSubbar == true)
	{
		pOleBar->isSubbar			= TRUE;
	}
	else
	{
		pOleBar->isSubbar			= FALSE;
	}

}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// - This method copies the BAR_RECORD to BarData to easily inset then into 
//   the local memory
// -
// - created from:	MWO		date:	19.05.2003
// -
// - changes:		who		when		what
// -
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
void CUGanttCtrl::CopyOleBarTo(BarData *pBarstruct, _BAR_RECORD *pOleBar)
{
	pBarstruct->Key				= pOleBar->Key				;
	pBarstruct->LineKey			= pOleBar->LineKey			;
	pBarstruct->LineNo			= pOleBar->LineNo				;
	pBarstruct->Begin				= pOleBar->Begin				 ;
	pBarstruct->End				= pOleBar->End				;
	pBarstruct->BarText			= pOleBar->BarText			;
	pBarstruct->LeftOutsideText	= pOleBar->LeftOutsideText	;
	pBarstruct->RightOutsideText	= pOleBar->RightOutsideText	;
	pBarstruct->Shape				= pOleBar->Shape				;
	pBarstruct->BackColor			= pOleBar->BackColor			;
	pBarstruct->LeftBackColor		= pOleBar->LeftBackColor		;
	pBarstruct->RightBackColor	= pOleBar->RightBackColor		;
	pBarstruct->TextColor			= pOleBar->TextColor			;
	pBarstruct->LeftTextColor		= pOleBar->LeftTextColor		;
	pBarstruct->RightTextColor	= pOleBar->RightTextColor		;
	pBarstruct->OverlapLevel		= pOleBar->OverlapLevel		;
	pBarstruct->SplitColor		= pOleBar->SplitColor			;
	pBarstruct->WithTextOutside	= pOleBar->WithTextOutside	;
	pBarstruct->WithArrowFrom		= pOleBar->WithArrowFrom		;
	pBarstruct->WithArrowTo		= pOleBar->WithArrowTo		;
	pBarstruct->MainbarKey		= pOleBar->MainbarKey			;

	CString TriangleID;
	TriangleID.Format("%ld%ld", pOleBar->leftTriangleColor, pOleBar->rightTriangleColor);
	SetBarTriangles(pBarstruct->Key.GetBuffer(0), TriangleID.GetBuffer(0), pOleBar->leftTriangleColor, pOleBar->rightTriangleColor, TRUE); 
	pBarstruct->isSubbar = false;
	
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// - This method returns a SAFEARRAY with BAR_RECORDS of LineNo to the container
// - Collects all bar in a line and returns them as array to container.
// -
// - created from:	MWO		date:	09.05.2003
// -
// - changes:		who		when		what
// -
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
VARIANT CUGanttCtrl::GetBarRecordsByLine(long LineNo) 
{
	VARIANT vaResult;
	VariantInit(&vaResult);

	GUID reglib = {0x6782E133, 0x3223, 0x11D4, { 0x99, 0x6A, 0x0, 0x0, 0x86, 0x3D, 0xE9, 0x5C}};
	SAFEARRAYBOUND sab;
	IRecordInfo *pRI = NULL;
	HRESULT hr;
	SAFEARRAY *pSa;
	if ((LineNo > -1) && (LineNo < omLines.GetSize()))
	{
		sab.cElements = omLines[LineNo].omBars.GetSize();
		sab.lLbound = 0;
		hr = GetRecordInfoFromGuids(/*IID_DUGantt*/reglib, 1, 0, 0x409, BAR_RECORD_GUID, &pRI);
		
		pSa = SafeArrayCreateEx(VT_RECORD, 1, &sab, pRI);
		if(pRI != NULL)
			pRI->Release();
		PVOID pvData;
		SafeArrayAccessData(pSa, &pvData );
		struct _BAR_RECORD *pmyBar;	
		pmyBar = (struct _BAR_RECORD *)pvData;
		for (int i = 0; i < omLines[LineNo].omBars.GetSize(); i++)
		{
			InitOleBar(&(pmyBar[i]));
			CopyBarToOleBar(&(pmyBar[i]), &omLines[LineNo].omBars[i]);
		}
	}

	SafeArrayUnaccessData(pSa);
	V_VT(&vaResult) = VT_ARRAY|VT_RECORD;
	V_ARRAY(&vaResult) = pSa;
	return vaResult;
}


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// - This method collects the spaces/gaps between the timeframestart, 
//   the bars and the timeframeend
// - Returns a SAFEARRAY of _TIME_FRAMES
// -
// - created from:	MWO		date:	09.05.2003
// -
// - changes:		who		when		what
// -
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
VARIANT CUGanttCtrl::GetGaps(long LineNo) 
{
	VARIANT vaResult;
	VariantInit(&vaResult);
	SAFEARRAYBOUND sab;
	IRecordInfo *pRI = NULL;
	HRESULT hr;
	CCSPtrArray<BarData> olBarArr;
	SAFEARRAY *pSa;
					//uuid of odl {0x6782E133, 0x3223, 0x11D4, { 0x99, 0x6A, 0x0, 0x0, 0x86, 0x3D, 0xE9, 0x5C}
	GUID reglib = {0x6782E133, 0x3223, 0x11D4, { 0x99, 0x6A, 0x0, 0x0, 0x86, 0x3D, 0xE9, 0x5C}};// = { 0x909e6310, 0x8157, 0x11d7, { 0x80, 0xdc, 0x0, 0xd0, 0xb7, 0xe2, 0xa4, 0x67 }};;
	if ((LineNo > -1) && (LineNo < omLines.GetSize()))
	{
		int i;
		for (i = 0; i < omLines[LineNo].omBars.GetSize(); i++)
		{
			olBarArr.NewAt(olBarArr.GetSize(), omLines[LineNo].omBars[i]);
		}
		olBarArr.Sort(CompareBarsByBegin);
		for ( i = olBarArr.GetSize()-1; i >= 0; i--)
		{
			if(i-1 >= 0)
			{
				CString olB1, olB2;
				olB1 = olBarArr[i].Begin.Format("%d.%m.%Y-%H:%M:%S");
				olB2 = olBarArr[i-1].End.Format("%d.%m.%Y-%H:%M:%S");
				//MWO: Bitte so lassen, denn der direkte abgleich der Zeiten geht nicht (????)
				// MS macht das intern �ber einen double und das funktioniert nicht, 
				//darum den Umweg �ber den String!!!!!!!!!!!
				// UND: ICH BIN NICHT BL��D
				if(IsOverlapped(olBarArr[i-1].Begin, olBarArr[i-1].End, olBarArr[i].Begin, olBarArr[i].End) 
					|| olB1 == olB2)//olBarArr[i-1].End == olBarArr[i].Begin)
				{
					olBarArr[i-1].End = olBarArr[i].End;
					olBarArr.DeleteAt(i);
				}
			}
		}

		//Append artificial bar start for timeframe start at the begin
		// and a second for timeframe end to get the loop easy and without
		// unnecessary ifs
		BarData olTFStart, olTFEnd;
		olTFStart.Begin = TimeFrameFrom;
		olTFStart.End = TimeFrameFrom;
		olTFEnd.Begin = TimeFrameTo;
		olTFEnd.End = TimeFrameTo;
		long llBarCount = olBarArr.GetSize();
		if(llBarCount > 0)
		{
			// Check if the first bar starts before the TimeFrameFrom
			// And only if not ==> insert the artificial bar at the beginning
			// ===> The same vice versa with the end
			if(!IsBetween(TimeFrameFrom, olBarArr[0].Begin, olBarArr[0].End))
			{
				olBarArr.NewAt(0, olTFStart);
			}
			llBarCount = olBarArr.GetSize();
			if(!IsBetween(TimeFrameTo, olBarArr[llBarCount-1].Begin, olBarArr[llBarCount-1].End))
			{
				olBarArr.NewAt(llBarCount, olTFEnd);
			}
		}
		else
		{
			//No bars in line
			olBarArr.NewAt(0, olTFStart);
			olBarArr.NewAt(1, olTFEnd);
		}
		//Calc new because it might be more now
		llBarCount = olBarArr.GetSize();
		sab.cElements = llBarCount-1;
		sab.lLbound = 0;
		hr = GetRecordInfoFromGuids(reglib/*IID_DUGantt*/, 1, 0, 0x409, /*reg*/MY_TF_GUID, &pRI);
		
		pSa = SafeArrayCreateEx(VT_RECORD, 1, &sab, pRI);
		if(pRI != NULL)
			pRI->Release();
		PVOID pvData;
		SafeArrayAccessData(pSa, &pvData );
		struct _UTIME_FRAME *pmyTf;
		pmyTf = (struct _UTIME_FRAME *)pvData;
		for ( i = 0; i < llBarCount; i++)
		{
			if((i+1) < llBarCount)
			{
				pmyTf[i].fStart = olBarArr[i].End.m_dt;
				pmyTf[i].fEnd = olBarArr[i+1].Begin.m_dt;
			}
		}
	}

	SafeArrayUnaccessData(pSa);
	V_VT(&vaResult) = VT_ARRAY|VT_RECORD;
	V_ARRAY(&vaResult) = pSa;
	olBarArr.DeleteAll();
	return vaResult;
}


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// - This method collects the spaces/gaps between the parameter FromDateTime and ToDateTime
//   the bars and the timeframeend
// - Returns a SAFEARRAY of _TIME_FRAMES
// -
// - created from:	MWO		date:	09.05.2003
// -
// - changes:		who		when		what
// -
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
VARIANT CUGanttCtrl::GetGapsForTimeframe(long LineNo, DATE FromDateTime, DATE ToDateTime) 
{
	VARIANT vaResult;
	VariantInit(&vaResult);
	SAFEARRAYBOUND sab;
	IRecordInfo *pRI = NULL;
	HRESULT hr;
	CCSPtrArray<BarData> olBarArr;
	SAFEARRAY *pSa;
					//uuid of odl {0x6782E133, 0x3223, 0x11D4, { 0x99, 0x6A, 0x0, 0x0, 0x86, 0x3D, 0xE9, 0x5C}
	GUID reglib = {0x6782E133, 0x3223, 0x11D4, { 0x99, 0x6A, 0x0, 0x0, 0x86, 0x3D, 0xE9, 0x5C}};// = { 0x909e6310, 0x8157, 0x11d7, { 0x80, 0xdc, 0x0, 0xd0, 0xb7, 0xe2, 0xa4, 0x67 }};;
	if ((LineNo > -1) && (LineNo < omLines.GetSize()))
	{
		int i;
		for ( i = 0; i < omLines[LineNo].omBars.GetSize(); i++)
		{
			if( omLines[LineNo].omBars[i].Begin.m_dt > FromDateTime  && omLines[LineNo].omBars[i].Begin.m_dt <= ToDateTime)
				olBarArr.NewAt(olBarArr.GetSize(), omLines[LineNo].omBars[i]);
			if(IsBetween(FromDateTime , omLines[LineNo].omBars[i].Begin.m_dt, omLines[LineNo].omBars[i].End.m_dt))
				olBarArr.NewAt(olBarArr.GetSize(), omLines[LineNo].omBars[i]);
		}
		olBarArr.Sort(CompareBarsByBegin);
		for ( i = olBarArr.GetSize()-1; i >= 0; i--)
		{
			if(i-1 >= 0)
			{
				CString olB1, olB2;
				olB1 = olBarArr[i].Begin.Format("%d.%m.%Y-%H:%M:%S");
				olB2 = olBarArr[i-1].End.Format("%d.%m.%Y-%H:%M:%S");
				//MWO: Bitte so lassen, denn der direkte abgleich der Zeiten geht nicht (????)
				// MS macht das intern �ber einen double und das funktioniert nicht, 
				//darum den Umweg �ber den String!!!!!!!!!!!
				// UND: ICH BIN NICHT BL��D
				if(IsOverlapped(olBarArr[i-1].Begin, olBarArr[i-1].End, olBarArr[i].Begin, olBarArr[i].End) 
					|| olB1 == olB2)//olBarArr[i-1].End == olBarArr[i].Begin)
				{
					olBarArr[i-1].End = olBarArr[i].End;
					olBarArr.DeleteAt(i);
				}
			}
		}

		//Append artificial bar start for timeframe start at the begin
		// and a second for timeframe end to get the loop easy and without
		// unnecessary ifs
		BarData olTFStart, olTFEnd;
		olTFStart.Begin = FromDateTime;
		olTFStart.End = FromDateTime;
		olTFEnd.Begin = ToDateTime;
		olTFEnd.End = ToDateTime;
		long llBarCount = olBarArr.GetSize();
		if(llBarCount > 0)
		{
			// Check if the first bar starts before the TimeFrameFrom
			// And only if not ==> insert the artificial bar at the beginning
			// ===> The same vice versa with the end
			if(!IsBetween(FromDateTime, olBarArr[0].Begin.m_dt, olBarArr[0].End.m_dt))
			{
				olBarArr.NewAt(0, olTFStart);
			}
			llBarCount = olBarArr.GetSize();
			if(!IsBetween(ToDateTime, olBarArr[llBarCount-1].Begin.m_dt, olBarArr[llBarCount-1].End.m_dt))
			{
				olBarArr.NewAt(llBarCount, olTFEnd);
			}
		}
		else
		{
			//No bars in line
			olBarArr.NewAt(0, olTFStart);
			olBarArr.NewAt(1, olTFEnd);
		}
		//Calc new because it might be more now
		llBarCount = olBarArr.GetSize();
		sab.cElements = llBarCount-1;
		sab.lLbound = 0;
		hr = GetRecordInfoFromGuids(reglib/*IID_DUGantt*/, 1, 0, 0x409, /*reg*/MY_TF_GUID, &pRI);
		
		pSa = SafeArrayCreateEx(VT_RECORD, 1, &sab, pRI);
		if(pRI != NULL)
			pRI->Release();
		PVOID pvData;
		SafeArrayAccessData(pSa, &pvData );
		struct _UTIME_FRAME *pmyTf;
		pmyTf = (struct _UTIME_FRAME *)pvData;
		for ( i = 0; i < llBarCount; i++)
		{
			if((i+1) < llBarCount)
			{
				pmyTf[i].fStart = olBarArr[i].End.m_dt;
				pmyTf[i].fEnd = olBarArr[i+1].Begin.m_dt;
			}
		}
	}

	SafeArrayUnaccessData(pSa);
	V_VT(&vaResult) = VT_ARRAY|VT_RECORD;
	V_ARRAY(&vaResult) = pSa;
	olBarArr.DeleteAll();
	return vaResult;}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// - This method returns the total count of all bars in the gantt
// - Returns count
// -
// - created from:	MWO		date:	09.05.2003
// -
// - changes:		who		when		what
// -
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
long CUGanttCtrl::GetTotalBarCount() 
{
	long i;
	long count;

	count = 0;
	for ( i = 0; i < omLines.GetSize(); i++)
	{
		count += omLines[i].omBars.GetSize();
	}
	return count;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// - This method adds a bar to the gantt along the data in _BAR_RECORD
// - Returns count
// -
// - created from:	MWO		date:	19.05.2003
// -
// - changes:		who		when		what
// -
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
void CUGanttCtrl::AddBarRecord(const VARIANT FAR& pBar) 
{
	struct _BAR_RECORD  *pmyBar;
	if(pBar.vt == VT_RECORD)
	{
		BarData *polBar = new BarData();
		pmyBar = (struct _BAR_RECORD *)pBar.pvRecord;
		CopyOleBarTo(polBar, pmyBar);
		DeleteBarByKey (polBar->Key.GetBuffer(0));
		omLines[polBar->LineNo].omBars.Add  (polBar);
		mapBars.SetAt((const char*)polBar->Key.GetBuffer(0), (void*)polBar);

		HandleOverlapLevelOfBar  (polBar->Key, TRUE);
  		HandleOverlapLevelOfLine (polBar->LineNo);
		HandleLineHight  (polBar->LineNo);
		UpdateSingleRect (polBar->Key);
	}
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// - This method deletes all bar without destroying the lines
// - If withBackgroundBars = TRUE then the backbars will deleted as well
// -
// - created from:	MWO		date:	19.05.2003
// -
// - changes:		who		when		what
// -
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
void CUGanttCtrl::DeleteAllBars(BOOL withBackgroundBars) 
{
	long cnt;
	long cnt2;
	long i;
	long j;
	cnt = omLines.GetSize();
	for( i = 0; i < cnt; i++ )
	{
		cnt2 = omLines[i].omBars.GetSize();
		for( j = cnt2 - 1; j >= 0; j--)
		{
			DeleteBarByKey(omLines[i].omBars[j].Key.GetBuffer(0));
		}
		cnt2 = omLines[i].omBackBars.GetSize();
		for( j = cnt2 - 1; j >= 0; j--)
		{
			if(withBackgroundBars == TRUE)
			{
				DeleteBarByKey(omLines[i].omBackBars[j].Key.GetBuffer(0));
			}
		}
	}
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// - This method returns the line in which the bar is
// -
// - created from:	MWO		date:	20.05.2003
// -
// - changes:		who		when		what
// -
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
long CUGanttCtrl::GetLineNoByBarKey(LPCTSTR Key) 
{
	BarData* polBar = NULL;
	mapBars.Lookup(Key, (void*&)polBar);
	if(polBar != NULL)
	{
		return polBar->LineNo;
	}

	return -1;
}

void CUGanttCtrl::OnLifeStyleChanged() 
{
	if(pomTimeScale != NULL)	
	{
		pomTimeScale->m_lifeStyle = this->m_lifeStyle;
		pomTimeScale->Invalidate();
	}

	SetModifiedFlag();
}


void CUGanttCtrl::DrawLifeStyle(CDC *pdc, CRect opRect, int MyColor, int ipMode, BOOL DrawDown, int ipPercent)
{
    int intBLUESTART = 255;
    int intBLUEEND = 0;
    double intBANDHEIGHT = 1.01;//15;
    int intSHADOWSTART = 64;
    int intSHADOWCOLOR = 0;
    int intTEXTSTART = 0;
    int intTEXTCOLOR = 15;
    int intRed = 1;
    int intGreen = 2;
    int intBlue = 4;
    int intBackRed = 8;
    int intBackGreen = 16;
    int intBackBlue = 32;
    float sngBlueCur;
    float sngBlueStep;
    long intFormHeight;
    long intFormWidth;
    long intX;
    long intY;
    int iColor;
	int ilColorValue = 255;

	ilColorValue = (int)(ilColorValue/100)*ipPercent;
    long iRed, iBlue, iGreen;
    CString prntText;
    COLORREF ReturnColor;
	CPen *pOldPen;
	sngBlueCur = 0;
	
	//pOldPen = pdc->SelectObject(&olPen);
    ReturnColor = COLORREF(RGB(255,255,255)); //vbWhite
    if( MyColor >= 0)
	{
        intFormHeight = opRect.bottom - opRect.top; //MyPanel.ScaleHeight
        intFormWidth =  opRect.right - opRect.left; //MyPanel.ScaleWidth
    
        iColor = MyColor;
        sngBlueCur = (float)intBLUESTART;
    
        if( DrawDown == TRUE)
		{
            if( ipMode == 0)
			{
                sngBlueStep = (float)((float)(intBANDHEIGHT * (intBLUEEND - intBLUESTART)) / intFormHeight);
				sngBlueStep = (float)((sngBlueStep/100)*ipPercent);
				//if(sngBlueStep == 0) sngBlueStep = 1;
                for( intY = 0; intY <= intFormHeight; intY+= (int)intBANDHEIGHT)
				{
					CPen olPen;
                    if( iColor & intBlue) iBlue = (long)sngBlueCur;
                    if( iColor & intRed) iRed = (long)sngBlueCur;
                    if( iColor & intGreen) iGreen = (long)sngBlueCur;
                    if( iColor & intBackBlue) iBlue = 255 - (long)sngBlueCur;
                    if( iColor & intBackRed) iRed = 255 - (long)sngBlueCur;
                    if( iColor & intBackGreen) iGreen = 255 - (long)sngBlueCur;

					olPen.CreatePen(PS_SOLID, 1, COLORREF(RGB(iRed, iGreen, iBlue)));
					pOldPen = pdc->SelectObject(&olPen);
					pdc->MoveTo(0, intY);
					pdc->LineTo(intFormWidth, intY);
                    //MyPanel.Line (-1, intY - 1)-(intFormWidth, intY + intB&HEIGHT), RGB(iRed, iGreen, iBlue), BF
                    sngBlueCur = sngBlueCur + sngBlueStep;
                    if( intY == 0 ) ReturnColor = COLORREF(RGB(iRed, iGreen, iBlue));
					pdc->SelectObject(pOldPen); 
                }
			}
            if(ipMode == 1)
			{
                sngBlueStep = (float)((float)(intBANDHEIGHT * (intBLUEEND - intBLUESTART)) / intFormWidth);
				sngBlueStep = (float)((sngBlueStep/100)*ipPercent);
                for( intX = 0;  intX <= intFormWidth; intX += (int)intBANDHEIGHT)
				{
					CPen olPen;
                    if( iColor & intBlue ) iBlue = (long)sngBlueCur;
                    if( iColor & intRed ) iRed = (long)sngBlueCur;
                    if( iColor & intGreen ) iGreen = (long)sngBlueCur;
                    if( iColor & intBackBlue ) iBlue = 255 - (long)sngBlueCur;
                    if( iColor & intBackRed ) iRed = 255 - (long)sngBlueCur;
                    if( iColor & intBackGreen ) iGreen = 255 - (long)sngBlueCur;
					olPen.CreatePen(PS_SOLID, 1, COLORREF(RGB(iRed, iGreen, iBlue)));
					pOldPen = pdc->SelectObject(&olPen);
					pdc->MoveTo(intX, 0);
					pdc->LineTo(intX + (int)intBANDHEIGHT, intFormHeight);
                    //MyPanel.Line (intX - 1, -1)-(intX + intBANDHEIGHT, intFormHeight), RGB(iRed, iGreen, iBlue), BF
                    sngBlueCur = sngBlueCur + sngBlueStep;
					pdc->SelectObject(pOldPen); 
                }
			}
		}
        else
		{
            if(  ipMode == 0 )
			{
                sngBlueStep = (float)((float)(intBANDHEIGHT * (intBLUEEND - intBLUESTART)) / intFormHeight);
				sngBlueStep = (float)((sngBlueStep/100)*ipPercent);
                for( intY = intFormHeight; intY >= -1; intY -= (int)intBANDHEIGHT)
				{
					CPen olPen;
                    if( iColor & intBlue ) iBlue = (long)sngBlueCur;
                    if( iColor & intRed ) iRed = (long)sngBlueCur;
                    if( iColor & intGreen ) iGreen = (long)sngBlueCur;
                    if( iColor & intBackBlue ) iBlue = 255 - (long)sngBlueCur;
                    if( iColor & intBackRed ) iRed = 255 - (long)sngBlueCur;
                    if( iColor & intBackGreen ) iGreen = 255 - (long)sngBlueCur;
					olPen.CreatePen(PS_SOLID, 1, COLORREF(RGB(iRed, iGreen, iBlue)));
					pOldPen = pdc->SelectObject(&olPen);
					pdc->MoveTo(0, intY);
					pdc->LineTo(intFormWidth, intY + (int)intBANDHEIGHT);
                    //MyPanel.Line (-1, intY - 1)-(intFormWidth, intY + intBANDHEIGHT), RGB(iRed, iGreen, iBlue), BF
                    sngBlueCur = sngBlueCur + sngBlueStep;
					pdc->SelectObject(pOldPen); 
                }
			}
            if( ipMode == 1)
			{
                sngBlueStep = (float)((float)(intBANDHEIGHT * (intBLUEEND - intBLUESTART)) / intFormWidth);
				sngBlueStep = (float)((sngBlueStep/100)*ipPercent);
                for( intX = intFormWidth; intX >= 0; intX -= (int)intBANDHEIGHT)
				{
					CPen olPen;
                    if( iColor & intBlue ) iBlue = (long)sngBlueCur;
                    if( iColor & intRed ) iRed = (long)sngBlueCur;
                    if( iColor & intGreen ) iGreen = (long)sngBlueCur;
                    if( iColor & intBackBlue ) iBlue = 255 - (long)sngBlueCur;
                    if( iColor & intBackRed ) iRed = 255 - (long)sngBlueCur;
                    if( iColor & intBackGreen ) iGreen = 255 - (long)sngBlueCur;
					olPen.CreatePen(PS_SOLID, 1, COLORREF(RGB(iRed, iGreen, iBlue)));
					pOldPen = pdc->SelectObject(&olPen);
					pdc->MoveTo(intX, 0);
					pdc->LineTo(intX /*+ (int)intBANDHEIGHT*/, intFormHeight);
                    //MyPanel.Line (intX - 1, -1)-(intX + intBANDHEIGHT, intFormHeight), RGB(iRed, iGreen, iBlue), BF
                    sngBlueCur = sngBlueCur + sngBlueStep;
					pdc->SelectObject(pOldPen); 
                }
			}
		}
		if( ipMode == 2)
		{
            sngBlueStep = (float)((float)(intBANDHEIGHT * (intBLUEEND - intBLUESTART)) / intFormHeight)*2;
			sngBlueStep = (float)((sngBlueStep/100)*ipPercent);
			//if(sngBlueStep == 0) sngBlueStep = 1;
            for( intY = ((int)intFormHeight/2); intY >= 0; intY-= (int)intBANDHEIGHT)
			{
				CPen olPen;
                if( iColor & intBlue) iBlue = (long)sngBlueCur;
                if( iColor & intRed) iRed = (long)sngBlueCur;
                if( iColor & intGreen) iGreen = (long)sngBlueCur;
                if( iColor & intBackBlue) iBlue = 255 - (long)sngBlueCur;
                if( iColor & intBackRed) iRed = 255 - (long)sngBlueCur;
                if( iColor & intBackGreen) iGreen = 255 - (long)sngBlueCur;

				olPen.CreatePen(PS_SOLID, 1, COLORREF(RGB(iRed, iGreen, iBlue)));
				pOldPen = pdc->SelectObject(&olPen);
				pdc->MoveTo(0, intY);
				pdc->LineTo(intFormWidth, intY);
                //MyPanel.Line (-1, intY - 1)-(intFormWidth, intY + intB&HEIGHT), RGB(iRed, iGreen, iBlue), BF
                sngBlueCur = sngBlueCur + sngBlueStep;
                if( intY == 0 ) ReturnColor = COLORREF(RGB(iRed, iGreen, iBlue));
				pdc->SelectObject(pOldPen); 
            }
			sngBlueCur = -1;
            for( intY = (int)intFormHeight/2; intY <= intFormHeight; intY+= (int)intBANDHEIGHT)
			{
				CPen olPen;
                if( iColor & intBlue) iBlue = (long)sngBlueCur;
                if( iColor & intRed) iRed = (long)sngBlueCur;
                if( iColor & intGreen) iGreen = (long)sngBlueCur;
                if( iColor & intBackBlue) iBlue = 255 - (long)sngBlueCur;
                if( iColor & intBackRed) iRed = 255 - (long)sngBlueCur;
                if( iColor & intBackGreen) iGreen = 255 - (long)sngBlueCur;

				olPen.CreatePen(PS_SOLID, 1, COLORREF(RGB(iRed, iGreen, iBlue)));
				pOldPen = pdc->SelectObject(&olPen);
				pdc->MoveTo(0, intY);
				pdc->LineTo(intFormWidth, intY);
                //MyPanel.Line (-1, intY - 1)-(intFormWidth, intY + intB&HEIGHT), RGB(iRed, iGreen, iBlue), BF
                sngBlueCur = sngBlueCur + sngBlueStep;
                if( intY == 0 ) ReturnColor = COLORREF(RGB(iRed, iGreen, iBlue));
				pdc->SelectObject(pOldPen); 
            }
		}
		if( ipMode == 3)
		{
		}
    }
}


void CUGanttCtrl::OnVersionChanged() 
{
	// TODO: Add notification handler code

	SetModifiedFlag();
}

void CUGanttCtrl::OnBuildDateChanged() 
{
	// TODO: Add notification handler code

	SetModifiedFlag();
}

BSTR CUGanttCtrl::GetGapsForTimeFrame2(long lineNo, DATE FromDateTime, DATE ToDateTime) 
{
	CString strResult;
	CCSPtrArray<BarData> olBarArr;

	if ((lineNo > -1) && (lineNo < omLines.GetSize()))
	{
		int i;
		for ( i = 0; i < omLines[lineNo].omBars.GetSize(); i++)
		{
			if( omLines[lineNo].omBars[i].Begin.m_dt > FromDateTime  && omLines[lineNo].omBars[i].Begin.m_dt <= ToDateTime)
				olBarArr.NewAt(olBarArr.GetSize(), omLines[lineNo].omBars[i]);
			if(IsBetween(FromDateTime , omLines[lineNo].omBars[i].Begin.m_dt, omLines[lineNo].omBars[i].End.m_dt))
				olBarArr.NewAt(olBarArr.GetSize(), omLines[lineNo].omBars[i]);
		}
		olBarArr.Sort(CompareBarsByBegin);
		for ( i = olBarArr.GetSize()-1; i >= 0; i--)
		{
			if(i-1 >= 0)
			{
				CString olB1, olB2;
				olB1 = olBarArr[i].Begin.Format("%d.%m.%Y-%H:%M:%S");
				olB2 = olBarArr[i-1].End.Format("%d.%m.%Y-%H:%M:%S");
				//MWO: Bitte so lassen, denn der direkte abgleich der Zeiten geht nicht (????)
				// MS macht das intern �ber einen double und das funktioniert nicht, 
				//darum den Umweg �ber den String!!!!!!!!!!!
				// UND: ICH BIN NICHT BL��D
				if(IsOverlapped(olBarArr[i-1].Begin, olBarArr[i-1].End, olBarArr[i].Begin, olBarArr[i].End) 
					|| olB1 == olB2)//olBarArr[i-1].End == olBarArr[i].Begin)
				{
					olBarArr[i-1].End = olBarArr[i].End;
					olBarArr.DeleteAt(i);
				}
			}
		}

		//Append artificial bar start for timeframe start at the begin
		// and a second for timeframe end to get the loop easy and without
		// unnecessary ifs
		BarData olTFStart, olTFEnd;
		olTFStart.Begin = FromDateTime;
		olTFStart.End = FromDateTime;
		olTFEnd.Begin = ToDateTime;
		olTFEnd.End = ToDateTime;
		long llBarCount = olBarArr.GetSize();
		if(llBarCount > 0)
		{
			// Check if the first bar starts before the TimeFrameFrom
			// And only if not ==> insert the artificial bar at the beginning
			// ===> The same vice versa with the end
			if(!IsBetween(FromDateTime, olBarArr[0].Begin.m_dt, olBarArr[0].End.m_dt))
			{
				olBarArr.NewAt(0, olTFStart);
			}
			llBarCount = olBarArr.GetSize();
			if(!IsBetween(ToDateTime, olBarArr[llBarCount-1].Begin.m_dt, olBarArr[llBarCount-1].End.m_dt))
			{
				olBarArr.NewAt(llBarCount, olTFEnd);
			}
		}
		else
		{
			//No bars in line
			olBarArr.NewAt(0, olTFStart);
			olBarArr.NewAt(1, olTFEnd);
		}
		//Calc new because it might be more now
		llBarCount = olBarArr.GetSize();
		for ( i = 0; i < llBarCount; i++)
		{
			if((i+1) < llBarCount)
			{
				strResult += olBarArr[i].End.Format("%Y%m%d%H%M00") + ",";
				strResult += olBarArr[i+1].Begin.Format("%Y%m%d%H%M00") + '\n';
			}
		}
		if(strResult.GetLength() > 0)
		{
			strResult = strResult.Left(strResult.GetLength() - 1);
		}
	}

	olBarArr.DeleteAll();
	return strResult.AllocSysString();
}

void CUGanttCtrl::ScrollToLine(long LineNo) 
{
	if(pomVScroll != NULL)
	{
		bmFireHScrollEvent = false;
		pomVScroll->SetScrollPos(LineNo);
		bmFireHScrollEvent = true;
	}
}

void CUGanttCtrl::OnSplitterMovableChanged() 
{
	// TODO: Add notification handler code

	SetModifiedFlag();
}

void CUGanttCtrl::GetVersionInfo()
{
	HMODULE hModule = AfxGetInstanceHandle();
	VersionInfo olVersionInfo;
	if (VersionInfo::GetVersionInfo(hModule,olVersionInfo))
	{
		m_version = olVersionInfo.omFileVersion;
	}

	m_buildDate = CString(__DATE__) + " - " + CString(__TIME__);

}


CString GetFileName( CString fileName, int num, int num2, CString fileExt )
{
	CString st;
	st.Format( "_%03d_%03d", num, num2 );
	return fileName + st + fileExt;
}

void WriteToFile(CString fileName, CString content)
{
   // Post: Write vector to text file(fileName) line by line

	std::ofstream outfile;
       
   outfile.open(fileName);
   
   if (!outfile) {
       MessageBox(NULL,"Failed to write to randomPlaylistPathData.txt file","Error",
                  MB_OK|MB_ICONERROR);
   }
   else{
	   outfile << content;
	  // for (int i=0; i<content.size(); i++) {
	  //         
			//outfile << content.at(i) << "\n";
	  // }
	       
	   outfile.close();
   }
}


//
// Returns the file portion from a path
//
CString GetFileOnly(LPCTSTR Path)
{
// Strip off the path and return just the filename part
	CString temp = (LPCTSTR) Path; // Force CString to make a copy
	::PathStripPath(temp.GetBuffer(0));
	temp.ReleaseBuffer(-1);
	return temp;
}

//
// Returns the folder portion from a path
//
CString GetFolderOnly(LPCTSTR Path)
{
// Strip off the file name so we can direct the file scanning dialog to go
// back to the same directory as before.
	CString temp = (LPCTSTR) Path; // Force CString to make a copy
	::PathRemoveFileSpec(temp.GetBuffer(0));
	temp.ReleaseBuffer(-1);
	return temp;
} 


BOOL CUGanttCtrl::SaveToFile(LPCTSTR pFileName)
{
	COleDateTimeSpan ts = TimeFrameTo - TimeFrameFrom;
	return SaveToFileWithTime(pFileName, TimeFrameFrom, ((int) ts.GetTotalMinutes())+1, 30 );
}

long CUGanttCtrl::GetNextVScrollPos( int curStartPos, long height)
{
	int nextStartPos = -1;
	long ilCurrY = 0;//TimeScaleHeaderHeight;
	int maxLines = curStartPos + imMaxVisibleLines;
	int i = -1;
	for (i = curStartPos; ((i < TabLines) && (i < maxLines )); i++)
	{
		ilCurrY += omTabLineHeight.GetAt (i);
		if (ilCurrY > height)
		{
			nextStartPos = i-2;
			break;
		}
	}
	if ((nextStartPos<0) && (i>2)) nextStartPos = i-2;
	return nextStartPos;
}

long CUGanttCtrl::GetGanttHeight()
{
	CRect olRect;
	this->GetClientRect(&olRect);
	return olRect.Height() - TimeScaleHeaderHeight;
}

BOOL CUGanttCtrl::SaveToFileWithTime(LPCTSTR pFileName, 
							 DATE fromDateTime,
							 int toMinute, int overlapMinute)
{
	if (toMinute<=0) return FALSE;//Invalid Time Frame
	if (overlapMinute<0) overlapMinute=0;
	COleDateTime lFromDateTime, lToDateTime;
	lFromDateTime = fromDateTime;
	lToDateTime = lFromDateTime + COleDateTimeSpan(0,0, toMinute, 0) ;

	if (lFromDateTime<TimeFrameFrom) lFromDateTime = TimeFrameFrom;
	if (lToDateTime>TimeFrameTo) lToDateTime = TimeFrameTo;
	if (lFromDateTime>=lToDateTime) return FALSE;

	//this->BeginWaitCursor();

	CString fileExt(PathFindExtension( pFileName ));
	CString fileName( pFileName );
	BOOL curHSBar = WithHorizontalScroller;

	//CBitmap orgBmpGantt = bmpGantt;
	//CRect orgGanttRect = omGanttRect;
	//CClientDC dc (this);

	//bmpGantt.CreateCompatibleBitmap	(&dc, omGanttRect.right, omGanttRect.bottom);

	//pomVScroll->ShowScrollBar(FALSE);
	//pomHScroll->ShowScrollBar(FALSE);
	
	CalcAreaRects();
	
	int orgVScrollPos = pomVScroll->GetScrollPos();
	int orgHScrollPos = pomHScroll->GetScrollPos();
	int orgMaxVisibleLines = imMaxVisibleLines;

	//Get DisplayDuration Increment (to increate 90% of actual Display Duration to show the overlpa time)
	COleDateTimeSpan displayDura = pomTimeScale->GetDisplayDuration();	
	double displayDuraInMinutes = displayDura.GetTotalMinutes();
	//COleDateTimeSpan displayDuraInc = COleDateTimeSpan( 0, 0, (int) (displayDuraInMinutes * 0.9), 0 );
	COleDateTimeSpan displayDuraInc = COleDateTimeSpan( 0, 0, (int) (displayDuraInMinutes - overlapMinute ), 0 );
	double displayDuraIncInMinutes = displayDuraInc.GetTotalMinutes();
	
	COleDateTimeSpan totDura =  lToDateTime - lFromDateTime ;

	int cntDura = (int) totDura.GetTotalMinutes() / (int) displayDuraIncInMinutes;//No. of Duration to step through
	if (((int) totDura.GetTotalMinutes() % (int) displayDuraIncInMinutes) != 0)
	{
		cntDura ++;
	}

	int startLineNo, endLineNo, visibleLineNo,cntLineStep;
	pomVScroll->GetScrollRange( &startLineNo, &endLineNo);
	cntLineStep = (endLineNo-startLineNo) / orgMaxVisibleLines;
	if (((endLineNo-startLineNo) % orgMaxVisibleLines)!=0) cntLineStep++;

	int width, height;
	//this->SetControlSize( 19840, 8000);
	this->GetControlSize( &width, &height );


	imMaxVisibleLines = orgMaxVisibleLines + 5;
	SetWithHorizontalScroller( FALSE );
	long lGanttHeight = GetGanttHeight();

	fileName = fileName.Mid(0, fileName.GetLength() - fileExt.GetLength() );

	int cnt = 1;
	
	COleDateTime lOldStartTime = omStartTime;

	omGanttRect.bottom = omCurrentTabRect.bottom;
	CString htmlContent;
	htmlContent.Append( "<html><body><Table>" );
	int scrollPos = 0;
	for(int i=0; i<cntLineStep; i++)
	{
		pomVScroll->SetScrollPos( scrollPos, FALSE );
		htmlContent.Append( "<tr>" );
		COleDateTime dtFrom = lFromDateTime;

		for (int j=0; j<cntDura; j++)
		{
			if (dtFrom>=lToDateTime) break;
			CString tempFileName = GetFileName(fileName, i+1, j+1, fileExt);

			htmlContent.AppendFormat( "<td><img src=%s  alt='image' /></td>", GetFileOnly(tempFileName) );
			SaveScreenToFile( tempFileName,  dtFrom);
			dtFrom = dtFrom + displayDuraInc;
			
		}
		htmlContent.Append( "</tr>" );
		//scrollPos += orgMaxVisibleLines;		
		scrollPos = GetNextVScrollPos( scrollPos, lGanttHeight );
		if ((scrollPos<0) || (scrollPos>TabLines)) break;
	}
	htmlContent.Append( "</Table></body></html>" );

	WriteToFile( fileName + ".html", htmlContent );
	
	this->SetControlSize( width, height);
	imMaxVisibleLines = orgMaxVisibleLines;
	SetWithHorizontalScroller( curHSBar );
	omStartTime = lOldStartTime;

	pomVScroll->SetScrollPos(orgVScrollPos);
	pomHScroll->SetScrollPos(orgHScrollPos);
		
	InvalidateControl();
	//this->EndWaitCursor();
	return true;
}

void CUGanttCtrl::DrawTimeMarker(CDC *pDc, bool bSaveOption)
{
		CRect olRect;
		GetClientRect(&olRect);
		//CalcAreaRects();
		int ilPos;
		int ilWidth;
		CUGanttStatic *polTimeMarker;
		CRect olTMRect;

		//bmDrawALL = TRUE;

	//Move the time markers
		ilPos = 0;
		while (ilPos < omTimeMarkers.GetSize())
		{
			polTimeMarker = &omTimeMarkers[ilPos];
			
			polTimeMarker->GetWindowRect(&olTMRect);
			ScreenToClient(&olTMRect);
			
			// Calculate the new position of the time marker
			ilWidth = olTMRect.right - olTMRect.left;
			olTMRect.left = GetXFromTime(polTimeMarker->omTime);
			olTMRect.right = olTMRect.left + ilWidth;
			if (pomHScroll != NULL)
			{
				CRect olScrollBarRect;
				pomHScroll->GetWindowRect(&olScrollBarRect);
				olTMRect.bottom = olRect.bottom - (olScrollBarRect.bottom - olScrollBarRect.top);
			}
			else
			{
				olTMRect.bottom = olRect.bottom; 
			}
			
			polTimeMarker->MoveWindow(&olTMRect, TRUE);
			
			//Is the TimeMarker inside the Gantt Area ?
			if (TimeMarkerVisible(polTimeMarker) != FALSE)
			{
				if (bSaveOption==true)
				{
					polTimeMarker->MyDraw(pDc, olTMRect );
				}
				else
				{
				polTimeMarker->ShowWindow(SW_SHOWNORMAL);
				}
			}
			else
			{
				polTimeMarker->ShowWindow(SW_HIDE);
			}
			
			ilPos++;
		}

}

void CUGanttCtrl::DrawTimeScaleMark( CDC *pdc )
{
	RECT rtTimeScale;

	pomTimeScale->GetClientRect( &rtTimeScale );
	CClientDC dc3( pomTimeScale ); 
	CDC dcMem3;

	dcMem3.CreateCompatibleDC(&dc3);
	CBitmap bmp3;
	bmp3.CreateCompatibleBitmap( &dc3,
		rtTimeScale.right - rtTimeScale.left,
		rtTimeScale.bottom - rtTimeScale.top );
	dcMem3.SelectObject( &bmp3 );

	pomTimeScale->MyDraw( &dcMem3);

	pdc->BitBlt( 
		omTimeScaleRect.left +1,
		omTimeScaleRect.top+1,
		rtTimeScale.right - rtTimeScale.left,
		rtTimeScale.bottom - rtTimeScale.top,
		&dcMem3,
		0,0,
		SRCCOPY );

	dcMem3.DeleteDC();
}

BOOL CUGanttCtrl::SaveScreenToFile(LPCTSTR fileName, COleDateTime dtFrom)
{
	omStartTime = dtFrom;
	WithTimeScale = TRUE;
	pomTimeScale->SetDisplayStartTime( omStartTime );

	CRect olRect;
	GetClientRect(&olRect);
	

	CClientDC dc (this);
	CDC dcMem;

	dcMem.CreateCompatibleDC(&dc);
	CImage img2;
    CBitmap lBmp2;
	olRect.right -= imTabVScrollWidth;
	olRect.bottom -= imTabHScrollHeight;
	//now create the bitmaps - without the moving bitmap, because its size changes!
	lBmp2.CreateCompatibleBitmap(&dc, olRect.right , olRect.bottom);

	dcMem.SelectObject(&lBmp2);
	
	OnMyDraw( &dcMem, olRect, olRect, true );

	/*
	
	DrawTab( &dcMem );
	DrawSplitter( &dcMem );
	DrawTimeScale( &dcMem );
	DrawGanttBmp( &dcMem );
	
	
*/

	//--------------Show Date Time - Start
	CFont *polOldFont;

	polOldFont = dcMem.SelectObject(&omTabFont);	

	dcMem.SetBkMode(TRANSPARENT);
	
	CRect olHTextRect;
	olHTextRect.left = 2;
	olHTextRect.top = 0;
	olHTextRect.right = olHTextRect.left + 550;
	olHTextRect.bottom = olHTextRect.top + TabHeaderFontSize;

	dcMem.SetTextColor(RGB(0,0,128));

	SYSTEMTIME sysTime;
	GetSystemTime( &sysTime );
	CTime time(sysTime);
	COleDateTime dt1;
	dt1 = COleDateTime::GetCurrentTime();
	CString stTime ;	
	stTime.Append( "Saved@");
	stTime.Append( dt1.Format("%d-%b-%Y %H:%M.") );
	//stTime.Append( ". Date : " + dtFrom.Format("%d-%b-%Y %H") );
	dcMem.DrawText( stTime, olHTextRect, DT_LEFT );
	//--------------Show Date Time - End
//------------------------

	img2.Attach( lBmp2 );
	img2.Save(fileName);
	img2.Detach();
	dcMem.DeleteDC();
	return true;
	
}





