﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ufis.Data.Ceda;
using Ufis.Entities;
using Ufis.Data;

namespace Ufis.Security.Ceda
{
    /// <summary>
    /// Represents a functionality to authorize user to connect to Ufis system.
    /// </summary>
    public class CedaUserAuthentication : IUfisUserAuthentication
    {
        /// <summary>
        /// Gets or sets the Ufis.Data.EntityDataContextBase used by this instance of 
        /// Ufis.Security.Ceda.UserAuthentication
        /// </summary>
        public EntityDataContextBase DataContext { get; set; }
        
        /// <summary>
        /// Initializes the new instance of Ufis.Security.Ceda.UserAuthentication class.
        /// </summary>
        public CedaUserAuthentication() 
        { 
        }

        /// <summary>
        /// Initializes the new instance of Ufis.Security.Ceda.UserAuthentication class.
        /// </summary>
        /// <param name="connection">The Ufis.Data.Ceda.CedaEntityConnection used by this instance of 
        /// Ufis.Security.Ceda.UserAuthentication</param>        
        public CedaUserAuthentication(EntityDataContextBase dataContext)
        {
            DataContext = dataContext;
        }

        /// <summary>
        /// Register the module to ceda server.
        /// </summary>
        /// <param name="registrationString">A registration string to register.</param>
        /// <returns>Error message (if any).</returns>
        public string RegisterModule(string registrationString)
        {
            string strResult = string.Empty;

            DataCommandParameterCollection parameters = new CedaCommandParameterCollection();
            parameters[0] = DataContext.Connection.HomeAirport;
            parameters[1] = "APPL,SUBD,FUNC,FUAL,TYPE,STAT";
            parameters[2] = registrationString;

            DataCommand command = new DataCommand()
                { CommandText = "SMI", CommandTimeout = 360, Parameters = parameters };
            try
            {
                DataContext.ExecuteNonQueryCommand(command);
            }
            catch (DataException exCeda)
            {
                strResult = exCeda.Message;
            }
            catch (Exception ex)
            {
                strResult = ex.Message;
            }

            return strResult;
        }

        /// <summary>
        /// Authenticate the user to ceda system.
        /// </summary>
        /// <param name="userName">The user's name.</param>
        /// <param name="password">The user's password.</param>
        /// <param name="privileges">The user privileges.</param>
        /// <param name="message">The additional information returned from server.</param>
        /// <returns></returns>
        public AuthenticationResult AuthenticateUser(string userName, string password, 
            UserPrivileges privileges, SecurityInfo info)
        {
            if (DataContext.Connection == null)
                throw new ArgumentNullException(null, "Connection for UserAuthentication cannot be null");

            if (privileges == null)
                throw new ArgumentNullException("privileges");

            if (info == null)
                throw new ArgumentNullException("info");
            
            AuthenticationResult result = AuthenticationResult.Succeeded;

            DataCommandParameterCollection parameters = new CedaCommandParameterCollection();
            parameters[0] = DataContext.Connection.HomeAirport;
            parameters[1] = "USID,PASS,APPL,WKST";
            parameters[2] = string.Format("{0},{1},{2},{3}", userName, password,
                DataContext.Connection.ApplicationName, DataContext.Connection.WorkstationName);

            DataCommand command = new DataCommand()
                { CommandText = "GPR", CommandTimeout = 360, Parameters = parameters };
            try
            {
                DataRowCollection rows = DataContext.ExecuteQueryCommand(command);
                int intRowCount = rows.Count;
                if (intRowCount > 0 && rows[0][0] == "[PRV]")
                {
                    int intStart = 1;
                    if (intRowCount > 1)
                    {
                        string strItem = rows[1][0];
                        if (strItem.IndexOf("[DAY]") >= 0)
                        {
                            string strDays = strItem.Substring(5);
                            int intDays;
                            if (int.TryParse(strDays, out intDays))
                            {
                                info.RemainingDaysToChangePassword = intDays;
                            }

                            intStart = 2;
                        }
                    }

                    for (int i = intStart; i < rows.Count; i++ )
                    {
                        DataRow row = rows[i];
                        if (row.ItemArray.Length >= 3)
                        {
                            string strKey = row[1];
                            string strRight = row[2];
                            privileges.Add(strKey, strRight);
                        }
                    }

                    UserAccessRight initModuRight = privileges["InitModu"];
                    if (initModuRight == UserAccessRight.Activated && DataContext.Connection.ApplicationName != "BDPS-SEC")
                    {
                        result = AuthenticationResult.NeedToInitializeModule;
                    }
                }
                else
                {
                    result = AuthenticationResult.FailedUnknownReason;
                }
            }
            catch (DataException exCeda)
            {
                string strErrorMessage = exCeda.Message;
                switch (strErrorMessage)
                {
                    case "LOGINERROR INVALID_USER":
                        result = AuthenticationResult.InvalidUserName;
                        break;
                    case "LOGINERROR INVALID_APPLICATION":
                        result = AuthenticationResult.InvalidApplication;
                        break;
                    case "LOGINERROR INVALID_PASSWORD":
                        result = AuthenticationResult.InvalidPassword;
                        break;
                    case "LOGINERROR DEACTIVATE_USER":
                        result = AuthenticationResult.UserDeactivated;
                        break;
                    case "LOGINERROR EXPIRED_USER":
                        result = AuthenticationResult.UserExpired;
                        break;
                    case "LOGINERROR EXPIRED_APPLICATION":
                        result = AuthenticationResult.ApplicationExpired;
                        break;
                    case "LOGINERROR EXPIRED_WORKSTATION":
                        result = AuthenticationResult.WorkstationExpired;
                        break;
                    case "LOGINERROR DISABLED_USER":
                        result = AuthenticationResult.UserDisabled;
                        break;
                    case "LOGINERROR DISABLED_APPLICATION":
                        result = AuthenticationResult.ApplicationDisabled;
                        break;
                    case "LOGINERROR DISABLED_WORKSTATION":
                        result = AuthenticationResult.WorkstationDisabled;
                        break;
                    case "LOGINERROR UNDEFINED_PROFILE":
                        result = AuthenticationResult.UndefinedProfile;
                        break;
                    case "LOGINERROR OVERDUE LOGIN":
                        result = AuthenticationResult.OverdueLogin;
                        info.InactiveDaysAllowed = GetNumberOfAllowedInactiveDays();
                        break;
                    case "LOGINERROR INVALID_PASSWORD DEACTIVATE_USER":
                        result = AuthenticationResult.InvalidPasswordUserDeactivated;
                        break;
                    default:
                        const string USER_MUST_CHANGE_PASSWORD_LOGIN_ERROR = "LOGINERROR MUST_CHANGE_PASSWORD_FIRST";
                        if (strErrorMessage.StartsWith(USER_MUST_CHANGE_PASSWORD_LOGIN_ERROR))
                        {
                            if (strErrorMessage.Length > USER_MUST_CHANGE_PASSWORD_LOGIN_ERROR.Length)
                                info.AdditionalErrorMessage = strErrorMessage.Substring(USER_MUST_CHANGE_PASSWORD_LOGIN_ERROR.Length);
                            
                            result = AuthenticationResult.UserMustChangePassword;
                            
                        }
                        else
                        {
                            result = AuthenticationResult.FailedUnknownReason;
                        }
                        break;
                }
            }
            catch
            {
                result = AuthenticationResult.FailedUnknownReason;
            }

            return result;
        }

        /// <summary>
        /// Change user's password.
        /// </summary>
        /// <param name="userName">The user's name.</param>
        /// <param name="oldPassword">The user's old password.</param>
        /// <param name="newPassword">The user's new password.</param>
        /// <returns>Change password result.</returns>
        public ChangePasswordResult ChangePassword(string userName, string oldPassword, string newPassword)
        {
            ChangePasswordResult result = ChangePasswordResult.Succeeded;

            if (DataContext.Connection == null)
            {
                throw new ArgumentNullException(null, "Connection for UserAuthentication cannot be null");
            }

            DataCommandParameterCollection parameters = new CedaCommandParameterCollection();
            parameters[0] = DataContext.Connection.HomeAirport;
            parameters[1] = "USID,OLDP,NEWP";
            parameters[2] = string.Format("{0},{1},{2}", userName, oldPassword, newPassword);
            parameters[3] = "NEWPWD";

            DataCommand command = new DataCommand()
                { CommandText = "SEC", CommandTimeout = 360, Parameters = parameters };
            try
            {
                DataContext.ExecuteNonQueryCommand(command);
            }
            catch (DataException exCeda)
            {
                string strErrorMessage = exCeda.Message;
                switch (strErrorMessage)
                {
                    case "LOGINERROR INVALID_USER":
                    case "INVALID_USER":
                        result = ChangePasswordResult.InvalidUser;
                        break;
                    case "LOGINERROR INVALID_PASSWORD":
                    case "INVALID_PASSWORD":
                        result = ChangePasswordResult.InvalidPassword;
                        break;
                    default:
                        result = ChangePasswordResult.FailedUnknownReason;
                        break;
                }
            }
            catch
            {
                result = ChangePasswordResult.FailedUnknownReason;
            }

            return result;
        }

        /// <summary>
        /// Get password rule.
        /// </summary>
        /// <returns>Password rule.</returns>
        public PasswordRule GetPasswordRule()
        {
            PasswordRule passwordRule = new PasswordRule();

            CedaEntitySelectCommand selectCommand = new CedaEntitySelectCommand()
            {
                AttributeList = "[Identifier],[Value]",
                EntityWhereClause = "WHERE [ApplicationName] = 'SEC'"
            };

            EntityCollectionBase<EntDbUserDefinedParameter> udParameters;
            try
            {
                udParameters = DataContext.OpenEntityCollection<EntDbUserDefinedParameter>(selectCommand);
            }
            catch
            {
                passwordRule = null;
                return passwordRule;
            }

            udParameters.BroadcastReceived += (o, e) => e.Handled = true; //ignore broadcast
            if (udParameters != null)
            {
                foreach (EntDbUserDefinedParameter udParameter in udParameters)
                {
                    switch (udParameter.Identifier)
                    {
                        case "ID_MAND_NUMBER":
                            passwordRule.MinNumericChars = SecurityUtils.StringToInt(udParameter.Value);
                            break;
                        case "ID_MAND_CAPITAL":
                            passwordRule.MinCapitalLetters = SecurityUtils.StringToInt(udParameter.Value);
                            break;
                        case "ID_MAND_SMALL":
                            passwordRule.MinSmallLetters = SecurityUtils.StringToInt(udParameter.Value);
                            break;
                        case "ID_PWD_LENGTH":
                            passwordRule.MinLength = SecurityUtils.StringToInt(udParameter.Value);
                            break;
                        case "ID_PWD_CONSEC":
                            passwordRule.MaxIdenticalCharsWithUserName = SecurityUtils.StringToInt(udParameter.Value);
                            break;
                    }
                }
                udParameters.Close();
            }
            else
            {
                passwordRule = null;
            }

            return passwordRule;
        }

        /// <summary>
        /// Get the number of allowed inactive days.
        /// </summary>
        /// <returns>Number of allowed inactive days.</returns>
        private int GetNumberOfAllowedInactiveDays()
        {
            int intResult = 0;

            CedaEntitySelectCommand selectCommand = new CedaEntitySelectCommand()
            {
                AttributeList = "[Value]",
                EntityWhereClause = "WHERE [Identifier] = 'ID_USER_DEACT' AND [ApplicationName] = 'SEC'"
            };

            EntityCollectionBase<EntDbUserDefinedParameter> udParameters;
            try
            {
                udParameters = DataContext.OpenEntityCollection<EntDbUserDefinedParameter>(selectCommand);
            }
            catch
            {
                intResult = 0;
                return intResult;
            }

            udParameters.BroadcastReceived += (o, e) => e.Handled = true; //ignore broadcast
            if (udParameters != null)
            {
                if (udParameters.Count > 0)
                {
                    EntDbUserDefinedParameter parameterTable = udParameters[0];
                    intResult = SecurityUtils.StringToInt(parameterTable.Value);
                }
                udParameters.Close();
            }
            else
            {
                intResult = 0;
            }

            return intResult;
        }
    }
}
