//****************************************************************************
// File: BCSERV.c
//
// Purpose: Contains main message loop and application entry point
//
// Functions:
//    WinMain() - initializes everything and enters message loop
//
//****************************************************************************

#include <windows.h>
#include <winsock.h>
#include <time.h>
#include <ufis.h>
#include <netutil.h>
#include <bcserv32.h>

// Broadcast buffer
#define MAX_BC_BUF_ENTRIES  (64)

// Traces
#define DEFAULT_TRACE_FN	"c:\\tmp\\bcserv32.log"        
#define TRACE_FN_LEN		(80)                 
#define DEFAULT_BC_FN		"C:\\TMP\\BCDATA.DAT"   
#define FIRSTBC				20000     
#define MAXBCNUM			30000     

// Data buffer size
#define DEFAULT_DATA_BUF_SIZE (0x8000)

#define PACKET_LEN     			(1024)  

// Static Variables         
char ufis_ct[] = __TIMESTAMP__;					/* Compile time */
static SOCKET gl_bc_socket = INVALID_SOCKET;	/* Socket for broadcast 			*/
static HGLOBAL bc_handle = NULL; 				/* Handle for GetBc buffer			*/
static COMMIF FAR *Packet = NULL;				/* Pointer to GetBc buffer			*/
static char bc_data_buffer[PACKET_LEN];			/* broadcast data buffer			*/
static char pcgEmptyPacket[PACKET_LEN+2];			/* broadcast data buffer			*/
static BC_HEAD FAR *Pbc_head_gl = NULL;			/* Pointer to GetBc buffer			*/
static int last_bc_num = 0;						/* Last received broadcast number 	*/
static LPSTR Pbc_buffer = NULL;					/* Broadcast buffer					*/
static int bcb_first = 0;						/*		First entry					*/                   
static int bcb_no = 0;							/*		Number of entries			*/                   
static HGLOBAL bcb_handle = NULL; 				/* 		Handle 						*/ 
static int wt_prio = WT_ERROR;					/* WriteTrace priority				*/
//static int wt_prio = WT_DEBUG;				/* WriteTrace priority				*/
static short sgNextBcNum = 0;
static short sgLastInvalidBC = 0;
static short sgThisBcPartOne = 0;


/* ******************************************************************** */
/* External variables							*/
/* ******************************************************************** */

FILE	*outpt = NULL; 			/* The logging file */ 
static char trace_buf [9172];  		/* The trace buffer */
FILE *prgBcFile = NULL;

// Network Constants                                                      
            
#define UFIS_SERVICE_NAME 		"UFIS"
#define SYMAP_SERVICE_NAME 		"SYMAP"
#define UFIS_BC_SERVICE_NAME 	"UFIS_BC"
#define UFIS_PORT_NO 			(3350)
#define SYMAP_PORT_NO 			(3450)

     
#define CfgSegSize 				(1024-12)     

int FAR PASCAL BcBufAdd (short spBcNum);
int FAR PASCAL AddToBcLostList(short spBcNum,BOOL bpAdd);
int FAR PASCAL do_filter(LPSTR Pdata,char *pcpTmp);

// Local function prototypes.
int PASCAL WinMain (HINSTANCE , HINSTANCE , LPSTR, int);
static void DoFree ( HGLOBAL handle);
static LPSTR DoMalloc (long buflen, HGLOBAL *Phandle );
void snap (LPSTR buf, int len);
static int AdjustBcNum (int new_num);
static int FAR PASCAL InitComm  (LPSTR IpAddr);


time_t tgBcTimeStamps[MAXBCNUM+1];

//***********************************************************************
// Function: WinMain
//
// Purpose: Called by Windows on app startup.  Initializes everything,
//          and enters a message loop.
//
// Parameters:
//    hInstance     == Handle to _this_ instance.
//    hPrevInstance == Handle to last instance of app.
//    lpCmdLine     == Command Line passed into app.
//    nCmdShow      == How app should come up (i.e. minimized/normal)
//
// Returns: Return value from PostQuitMessage.
//
// Comments:
//
// History:  Date       Author        Reason
//           3/11/92    BS            Created
//****************************************************************************

int PASCAL WinMain (HINSTANCE  hInstance,  // This instance
                    HINSTANCE  hPrevInstance,                  // Last instance
                    LPSTR lpCmdLine,   // Command Line
                    int nCmdShow)      // Minimized or Normal?
{
   	MSG msg;         
   	int rc = RC_SUCCESS;
   	int len=PACKET_LEN;
	int i=0;
	SOCKERR serr=0;
	
   	if (!hPrevInstance)
      if (!InitApplication(hInstance))
         return (FALSE);

   	if (!InitInstance(hInstance, nCmdShow))
      return (FALSE);
                

	rc = InitComm(lpCmdLine);
	 
	sprintf(trace_buf,"Main: Nach InitComm rc= %d IpAddr= %Fs ",rc, lpCmdLine); 
	WriteTrace(trace_buf, WT_DEBUG);
	sprintf(trace_buf,"Main: Instance=%x HWND=%x",hInstance,ghWnd); 
	WriteTrace(trace_buf, WT_DEBUG);
	
	if (rc != RC_SUCCESS)
		return RC_FAIL;
		
//	if (bc_handle == NULL)
//		Packet = (COMMIF FAR *) DoMalloc ( (long)PACKET_LEN, &bc_handle);
//	if (Packet == NULL)
//		rc = RC_FAIL;

	Packet = (COMMIF FAR *) bc_data_buffer;
	    
    rc = WSAAsyncSelect (gl_bc_socket,ghWnd,UFIS_SYNC, FD_READ); 

//	SetTimer(ghWnd,UFIS_TIMER,1000,NULL);
    if (rc < 0)
    {
    	serr = WSAGetLastError();
		sprintf(trace_buf,"WSAAsyncSelect: %s\n",SockerrToString( serr ) );
		WriteTrace(trace_buf, WT_ERROR);
    }
    
   	while (rc == 0 && GetMessage(&msg,             // Put Message Here
                     NULL,             // Handle of window receiving msg
                     (UINT) NULL,             // lowest message to examine
                     (UINT) NULL))            // highest message to examine
   	{
      TranslateMessage(&msg);          // Translates virtual key codes
      DispatchMessage(&msg);           // Dispatches message to window
   	}                            
   	
   	return (msg.wParam);                // Returns the value from PostQuitMessage
}


// -----------------------------------------------------------------
//
// Function: InitCom
//
// Purpose : Initialization 
//
// Params  : IpAddr:	IP address of own host
//			 CedaHost:	Name of CEDA host
//
// Returns : 0 Operation was successful. 
//			 < 0: Communication error
//			 > 0: Error code from ceda
//
// Comments: 
//           
// -----------------------------------------------------------------
static int FAR PASCAL InitComm  (LPSTR IpAddr)
{                 
	int rc = RC_SUCCESS;
    int broadcast_bool=1;
	char pclTmpPath[400],pclFileName[500];

    if(getenv("UFISTMP") != NULL)
		strcpy(pclTmpPath, getenv("UFISTMP"));
	else
		strcpy(pclTmpPath, "C:\\TMP");

	sprintf(pclFileName, "%s\\bcserv32.log", pclTmpPath);

	outpt = (FILE *) fopen(pclFileName, "w");
	if (outpt == NULL)     
	{
		rc =  RC_INIT_FAIL;
		return rc;
    } /* end if */  


	sprintf(trace_buf,"BCSERV32 4.3.1 %s",ufis_ct); 


	sprintf(pclFileName, "%s\\BCDATA.DAT", pclTmpPath);
	prgBcFile = (FILE *) fopen (pclFileName,"wb");
	if (prgBcFile == NULL)     
	{
		rc =  RC_INIT_FAIL;
		sprintf(trace_buf,"File %s not created",pclFileName); 
		WriteTrace(trace_buf, WT_DEBUG);              
		return rc;
    } /* end if */  
	else
	{
		char pclTmpBuf[PACKET_LEN+4];
		memset(pclTmpBuf,0,sizeof(pclTmpBuf));
		fseek(prgBcFile,FIRSTBC+(MAXBCNUM*PACKET_LEN),SEEK_SET);
		fwrite(pclTmpBuf,PACKET_LEN,1,prgBcFile);
		fflush(prgBcFile);

		sprintf(trace_buf,"File <%s> created", pclFileName); 
		WriteTrace(trace_buf, WT_DEBUG);              
	}

	memset(pcgEmptyPacket,0,PACKET_LEN);
	
    if (rc == RC_SUCCESS)
    {
     	rc = InitWinsock (IpAddr);
    }


	/* Enable broadcast */
    if (rc == RC_SUCCESS)
    { 
    	rc = CreateSocket( (SOCKET FAR *) &gl_bc_socket, SOCK_DGRAM, 
    					UFIS_BC_SERVICE_NAME, TRUE );
    }
	sprintf(trace_buf,"Nach CreateSocket (bc) %d",rc); 
	WriteTrace(trace_buf, WT_DEBUG);              
	
	return rc;
} /* InitComm */            


// -----------------------------------------------------------------
//
// Function: CleanupCom
//
// Purpose : Cleanup of the Comm
//
// Params  : none
//
// Returns : 0 Operation was successful. 
//			 < 0: Communication error
//			 > 0: Error code from ceda
//
// Comments: 
//           
// -----------------------------------------------------------------
int FAR PASCAL CleanupCom  (void)
{
	int rc = RC_SUCCESS;
	

	(void) ResetSocket (gl_bc_socket);
    gl_bc_socket = INVALID_SOCKET;
    
	rc = CleanupWinsock();

	if (bc_handle != NULL)
	{
		DoFree (bc_handle);
		bc_handle = NULL;
	}

	if (outpt != NULL)
	{
		fclose (outpt); 
		outpt = NULL;
	}
	if (prgBcFile != NULL)
	{
		fclose(prgBcFile);
		prgBcFile = NULL;
	}

	
	return rc;
} /* end CleanupCom */
                 
// -----------------------------------------------------------------
//
// Function: DoMalloc
//
// Purpose : Simple malloc, the handle has to be used for DoFree !
//
// Params  : see below
//
// Returns : Pointer: 	Operation was successful. 
//			 NULL:  	error
//
// Comments: 
//           
// -----------------------------------------------------------------

static LPSTR DoMalloc (long buflen, HGLOBAL *Phandle )
{                      
	int rc = RC_SUCCESS;
	LPSTR Pbuf = NULL;

	*Phandle = GlobalAlloc (GHND, (DWORD) buflen);
	if (*Phandle == NULL)
	{
		sprintf(trace_buf,"DoMalloc:GlobalAlloc failed"); WriteTrace(trace_buf, WT_ERROR);
	 	rc = RC_INIT_FAIL;
	}
    
    if (rc == RC_SUCCESS)
    {
        Pbuf = (LPSTR) GlobalLock (*Phandle);
		if (Pbuf == NULL)
		{
			sprintf(trace_buf,"DoMalloc:GlobalLock failed"); WriteTrace(trace_buf, WT_ERROR);
	 		rc = RC_INIT_FAIL;
		}
    }           
    
    if (rc == RC_SUCCESS)
    {
		sprintf(trace_buf,"DoMalloc:%ld Bytes done handle = %d",buflen,*Phandle);
		WriteTrace(trace_buf, WT_DEBUG);
    	return Pbuf;                                               
    }
    else
    {
    	return NULL;
    }  /* end if */
}   /* end DoMalloc */

// -----------------------------------------------------------------
//
// Function: DoFree
//
// Purpose : Simple free, the handle comes from a DoMalloc call
//
// Params  : see below
//
// Returns : none
//
// Comments: 
//           
// -----------------------------------------------------------------

static void DoFree ( HGLOBAL handle)
{
	GlobalUnlock (handle);
	GlobalFree (handle);
	sprintf(trace_buf,"DoFree:done handle = %d", handle); WriteTrace(trace_buf, WT_DEBUG);
}


void snap (LPSTR buf, int buflen)
{
 	int i; 
 	LPSTR Pact=buf;
 	
    if ( outpt != NULL)
    {
   	  for (i=0; i<buflen; i++)
 	  {
 		if (buflen - i >= 16)
 		{
 			sprintf (trace_buf, 
 			"%02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x",
            (unsigned char) *(Pact+0),(unsigned char) *(Pact+1),
            (unsigned char) *(Pact+2),(unsigned char) *(Pact+3),
            (unsigned char) *(Pact+4),(unsigned char) *(Pact+5),
            (unsigned char) *(Pact+6),(unsigned char) *(Pact+7),
            (unsigned char) *(Pact+8),(unsigned char) *(Pact+9),
            (unsigned char) *(Pact+10),(unsigned char) *(Pact+11),
            (unsigned char) *(Pact+12),(unsigned char) *(Pact+13),
            (unsigned char) *(Pact+14),(unsigned char) *(Pact+15));
			WriteTrace(trace_buf, WT_DEBUG);
			trace_buf[0] = EOS;
            Pact += 16;
            i += 15;
        }
        else
        {
 			sprintf (trace_buf + strlen (trace_buf), "%02x ", (unsigned char) *(Pact+0));
 			Pact++;         	
        } 
      }
	  WriteTrace(trace_buf, WT_DEBUG);
	}
} /* end DoFree */


// -----------------------------------------------------------------
//
// Function: WriteTrace
//
// Purpose : Write the buffer to the trace file, if trace is enabled
//
// Params  : buffer
//
// Returns : none
//
// Comments: 
//           
// -----------------------------------------------------------------

void WriteTrace ( LPSTR buf, int prio)
{
	/***********************/
	char timebuf[20];
	
    if ( outpt != NULL && prio >= wt_prio)
    {
    	_strdate(timebuf);
    	timebuf[8] = ' ';
    	_strtime(timebuf + 9);
		fprintf (outpt, "%s: %s\n", timebuf, buf);
		fflush (outpt);   
	}
	/*****************/
}
// -----------------------------------------------------------------
//
// Function: GetTraceBuf
//
// Purpose : Interface to the trace buffer
//
// Params  : none
//
// Returns : Pointer to global trace buffer
//
// Comments: 
//           
// -----------------------------------------------------------------
char *GetTraceBuf  (void)  
{
	return trace_buf;
}


//****************************************************************************
// Function: MainWndProc
//
// Purpose: Message handler for main overlapped window.
//
// Parameters:
//    hWnd    == Handle to _this_ window.
//    message == Message to process.
//    wParam  == WORD parameter -- depends on message
//    lParam  == LONG parameter -- depends on message
//
// Returns: Depends on message.
//
// Comments:
//
// History:  Date       Author        Reason
//           1/27/92    DAW           Created
//****************************************************************************

long FAR PASCAL MainWndProc (HWND hWnd,
                             unsigned message,
                             WORD wParam,
                             LONG lParam)
{  
   int len=0;
   int rc=RC_SUCCESS;
   static short slLastBcNum = 0;
	char clServerName[124];
		
   switch (message)
      {


   // On WM_DESTROY, terminate this app by posting a WM_QUIT message.

   case WM_DESTROY:
	  	sprintf(trace_buf,"MainWndProc:  PostQuitMessage  empfangen"); 
	  	WriteTrace(trace_buf, WT_DEBUG);
	  	(void) CleanupCom ();
      	PostQuitMessage(0);
      	break;

   case UFIS_SYNC:
//	  	sprintf(trace_buf,"MainWndProc:  UFIS_SYNC  empfangen"); 
//	  	WriteTrace(trace_buf, WT_DEBUG);
		len=PACKET_LEN;
   	  	rc = ReceiveData (gl_bc_socket, (LPSTR) Packet, (int FAR *) &len);

//		Removed error reporting. Done in netutil.c

//		if (rc != RC_NOT_FOUND)
//		{
//			sprintf(trace_buf,"MainWndProc: Nach ReceiveData %d len = %d",rc, len); 
//			WriteTrace(trace_buf, WT_DEBUG);
//		}
//	
//		{
//			COMMIF  *Pbc = Packet;				
//			BC_HEAD  *Pbc_head;		
//			Pbc_head = (BC_HEAD  *) Packet->data;
//
//
//			strcpy(clServerName,Pbc_head->orig_name);
//
//			sprintf(trace_buf,"%05d:Nach ReceiveData  rc= %d <%s>",__LINE__,
//					rc,Pbc_head->orig_name); 
//			WriteTrace(trace_buf, WT_DEBUG);
//			//snap ((LPSTR)Packet, 32);
//		}

		//sprintf(trace_buf,"MainWndProc: Nach ReceiveData: Status = %d , Len = %d",rc, len); 
		//WriteTrace(trace_buf, WT_DEBUG);

   		if (rc == RC_SUCCESS)
    	{ 
			char pclTmp[1024] = "";

			//	snap ((LPSTR)Packet, 32);
		//WriteTrace(trace_buf, WT_DEBUG);
			rc = do_filter((LPSTR) Packet,pclTmp);				           
		sprintf(trace_buf,"do_filter rd=%d <%s>",rc, pclTmp); 
		WriteTrace(trace_buf, WT_DEBUG);
		//	snap ((LPSTR)Packet, 32);
			if (rc == RC_SUCCESS)
			{
			   	COMMIF  *Pbc = Packet;				
   				BC_HEAD  *Pbc_head;

				Pbc_head = (BC_HEAD  *) Packet->data;
				/****************
				Pbc_head->bc_num = ntohs(Pbc_head->bc_num);
				Pbc_head->tot_buf = ntohs(Pbc_head->tot_buf);
				Pbc_head->act_buf = ntohs(Pbc_head->act_buf);
				Pbc_head->rc = Pbc_head->rc;   // always 0
				Pbc_head->tot_size = ntohs(Pbc_head->tot_size);
				Pbc_head->cmd_size = ntohs(Pbc_head->cmd_size);
				Pbc_head->data_size = ntohs(Pbc_head->data_size);
				*****************/
				fseek(prgBcFile,FIRSTBC+(Pbc_head->bc_num*PACKET_LEN),SEEK_SET);
				fwrite(Packet,PACKET_LEN,1,prgBcFile);
				fflush(prgBcFile);
				if (Pbc_head->act_buf  == 1)
				{
					sgThisBcPartOne = Pbc_head->bc_num;
				}

				sprintf(trace_buf,"BC received with BcNum %d/%d",Pbc_head->bc_num,ntohs(Pbc_head->bc_num));
				WriteTrace(trace_buf, WT_DEBUG);

				if (!slLastBcNum)
				{
					slLastBcNum = Pbc_head->bc_num;
				}
				if (Pbc_head->tot_buf == Pbc_head->act_buf && sgThisBcPartOne != 0)
				{
					//BcBufAdd(sgThisBcPartOne);
					BcBufAdd(Pbc_head->bc_num);
					sprintf(trace_buf,"BcBufAdd called with BcNum %d",Pbc_head->bc_num);
					WriteTrace(trace_buf, WT_DEBUG);
				}
				else 
				{
					sprintf(trace_buf,"BC-part received with BcNum %d",Pbc_head->bc_num);
					WriteTrace(trace_buf, WT_DEBUG);
				}


				if((sgNextBcNum != 0) && (Pbc_head->bc_num != 0) && (Pbc->command != PACKET_RESEND))
				{
					//This section solves the problem of delayed or duplicated broadcasts,
					//which has been observed in the network of SATS.
					//The solution is to handle such broadcasts as Re-Sent messages internally.
					short slDiff = 0;
					short slMaxDiff = 1000;
					BOOL	blIgnore = FALSE;
					
					if (Pbc_head->bc_num < sgNextBcNum)
					{
						// 2 Possible cases: 1. A delayed BC. 2. An invalid BC (diff > ???)
						
							slDiff = sgNextBcNum - Pbc_head->bc_num;
							
							if (slDiff > slMaxDiff)
							{
								blIgnore = TRUE;							// Ignore the packet.
							}
							else
							{
								Pbc->command = PACKET_RESEND;	// Treat as a resent packet
							}
					}
					else if (Pbc_head->bc_num > sgNextBcNum)
					{
						// 2 Possible cases: 1. Delayed BC and wraparound. 2. Lost BCs
						
						slDiff = Pbc_head->bc_num - sgNextBcNum;
						
						if (slDiff >= (30000 - slMaxDiff))
						{
							// Here we have a wraparound situation, so treat as resent packet
							
							Pbc->command = PACKET_RESEND;	// Treat as a resent packet
						}
						else if (slDiff <= slMaxDiff)
						{
							// Here we have a lost BC situation
						}
						else
						{
							blIgnore = TRUE;								// Ignore the packet.
						}
						
					}
					
					if (blIgnore)
					{

						sprintf(trace_buf,"Invalid BcNum %d",Pbc_head->bc_num);
						WriteTrace(trace_buf, WT_ERROR);


						if ( sgLastInvalidBC > 0)
						{
							// Check for BC range change and possibly send a "CLO" to shutdown
							// If not, the next invalid BC will cause the normal Lost BC condition
						}
						else
						{
							sgLastInvalidBC = Pbc_head->bc_num;
							break;	// Get out of here
						}
					}
					else
					{
						sgLastInvalidBC = 0;
					}

				}

				if (Pbc->command == PACKET_RESEND)
				{
					AddToBcLostList((short)Pbc_head->bc_num,FALSE);
					sprintf(trace_buf,"BC-Reread BcNum %d",Pbc_head->bc_num);
					WriteTrace(trace_buf, WT_DEBUG);
				}
				else if(sgNextBcNum != 0 && sgNextBcNum != Pbc_head->bc_num &&
					slLastBcNum != Pbc_head->bc_num)
				{
					short slLc; 

					sprintf(trace_buf,"Broadcasts lost from %d to %d",
						sgNextBcNum,Pbc_head->bc_num-1);
					WriteTrace(trace_buf, WT_ERROR);
					if (sgNextBcNum < Pbc_head->bc_num)
					{  
						for(slLc = sgNextBcNum; slLc < Pbc_head->bc_num; slLc++)
						{
							AddToBcLostList((short)slLc,TRUE);
							sprintf(trace_buf,"BC-Lost BcNum %d",slLc);
							WriteTrace(trace_buf, WT_DEBUG);
							// overwrite old invalid BC 
							fseek(prgBcFile,FIRSTBC+(slLc*PACKET_LEN),SEEK_SET);
							fwrite(pcgEmptyPacket,PACKET_LEN,1,prgBcFile);
						}
					}
					fflush(prgBcFile);
					BcBufAdd((short)(Pbc_head->bc_num-1));
				}
				else
				{
					sprintf(trace_buf,"Broadcast not lost sgNextBcNum=%d,Pbc_head->bc_num=%d",
						sgNextBcNum,Pbc_head->bc_num);
					WriteTrace(trace_buf, WT_DEBUG);
					sgNextBcNum = Pbc_head->bc_num;
				}
				if (Pbc->command != PACKET_RESEND)
				{
					sgNextBcNum = Pbc_head->bc_num+1;
					
					//Handle wrap around
					
					if (sgNextBcNum == 30001)
					{
						sgNextBcNum = 1;
					}
					
					slLastBcNum = Pbc_head->bc_num; 
				}
				{
			   	COMMIF  *Pbc = Packet;				
   				BC_HEAD  *Pbc_head;		
				Pbc_head = (BC_HEAD  *) Packet->data;

				sprintf(trace_buf,"do_filter successful  rc= %d <%s>",
						rc,Pbc_head->orig_name); 
				WriteTrace(trace_buf, WT_DEBUG);
				//snap ((LPSTR)Packet, 32);
				}
			}
			else
			{
				/****************/
			   	COMMIF  *Pbc = Packet;				
   				BC_HEAD  *Pbc_head;		

				Pbc_head = (BC_HEAD  *) Packet->data;



				sprintf(trace_buf,"do_filter failed  rc= %d received: <%s> expected: <%s>, BcNum = %d",
						rc,clServerName,Pbc_head->orig_name,Pbc_head->bc_num); 

			//	sprintf(trace_buf,"do_filter failed  rc= %d <%s>, BcNum = %d",
			//			rc,Pbc_head->orig_name,Pbc_head->bc_num); 
				WriteTrace(trace_buf, WT_DEBUG);
				//snap ((LPSTR)Packet, 32);
				/****************/
			}
     	}
   		break;

   case UFIS_TIMER:
	//		rc = BcDistribute();
   		break;
   // This message terminates bcserv.exe
   case UFIS_QUIT:
	  	sprintf(trace_buf,"MainWndProc:  UFIS_QUIT  empfangen",rc); 
	  	WriteTrace(trace_buf, WT_DEBUG);
	  	rc = CleanupCom ();
       	PostQuitMessage(0);
   		break;
   		
   // We didn't handle, pass to DefWindowProc.

   default:
      return DefWindowProc(hWnd, message, wParam, lParam);
      }


   return ((long)NULL);
}


