﻿#region ===== Source Code Information =====
/*
Project Name    : Ufis.Entities
Project Type    : Class Library
Platform        : Microsoft Visual C#.NET
File Name       : EntDbCheckInCounterAllocation.cs

Version         : 1.0.0
Created Date    : 16 - Feb - 2012
Complete Date   : 16 - Feb - 2012
Created By      : Phyoe Khaing Min

Date Reason Updated By
---------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------

Copyright (C) All rights reserved.
*/
#endregion

using System;

namespace Ufis.Entities
{
    [Entity(SerializedName = "CCATAB")]
    public class EntDbCheckInCounterAllocation : ValidityBaseEntity
    {
        //	Aircraft Type 3-Letter Code (IATA), Foreign Key of AFTTAB.ACT3
        private string _AircraftType;
        [Entity(SerializedName = "ACT3", MaxLength = 5)]
        public string AircraftType
        {
            get { return _AircraftType; }
            set
            {
                if (_AircraftType != value)
                {
                    _AircraftType = value;
                    OnPropertyChanged("AircraftType");
                }
            }
        }
        //Record created at (date)
        private string _RecordDate;
        [Entity(SerializedName = "CDAT", MaxLength = 14)]
        public string RecordDate
        {
            get { return _RecordDate; }
            set
            {
                if (_RecordDate != value)
                {
                    _RecordDate = value;
                    OnPropertyChanged("RecordDate");
                }
            }
        }
        //	URNOs of related GRMTAB entry, Foreign key of GRMTAB.Urno
        private string _GroupAssignCode;
        [Entity(SerializedName = "CGRU", MaxLength = 256)]
        public string GroupAssignCode
        {
            get { return _GroupAssignCode; }
            set
            {
                if (_GroupAssignCode != value)
                {
                    _GroupAssignCode = value;
                    OnPropertyChanged("GroupAssignCode");
                }
            }
        }
        //Check-in Counter actual start
        private string _CICActualStartTime;
        [Entity(SerializedName = "CKBA", MaxLength = 14)]
        public string CICActualStartTime
        {
            get { return _CICActualStartTime; }
            set
            {
                if (_CICActualStartTime != value)
                {
                    _CICActualStartTime = value;
                    OnPropertyChanged("CICActualStartTime");
                }
            }
        }
        //Check-in Counter scheduled start
        private string _CICScheduleStartTime;
        [Entity(SerializedName = "CKBS", MaxLength = 14)]
        public string CICScheduleStartTime
        {
            get { return _CICScheduleStartTime; }
            set
            {
                if (_CICScheduleStartTime != value)
                {
                    _CICScheduleStartTime = value;
                    OnPropertyChanged("CICScheduleStartTime");
                }
            }
        }
        //Check-in Counter scheduled end
        private string _CICScheduleEndTime;
        [Entity(SerializedName = "CKES", MaxLength = 14)]
        public string CICScheduleEndTime
        {
            get { return _CICScheduleEndTime; }
            set
            {
                if (_CICScheduleEndTime != value)
                {
                    _CICScheduleEndTime = value;
                    OnPropertyChanged("CICScheduleEndTime");
                }
            }
        }
        //Check-in Counter actual end
        private string _CICActualEndTime;
        [Entity(SerializedName = "CKEA", MaxLength = 14)]
        public string CICActualEndTime
        {
            get { return _CICActualEndTime; }
            set
            {
                if (_CICActualEndTime != value)
                {
                    _CICActualEndTime = value;
                    OnPropertyChanged("CICActualEndTime");
                }
            }
        }
        //	Check-in Counter
        private string _CheckInCounter;
        [Entity(SerializedName = "CKIC", MaxLength = 5)]
        public string CheckInCounter
        {
            get { return _CheckInCounter; }
            set
            {
                if (_CheckInCounter != value)
                {
                    _CheckInCounter = value;
                    OnPropertyChanged("CheckInCounter");
                }
            }
        }
        //Fixed Check-in Counter
        private string _FixCheckInCounter;
        [Entity(SerializedName = "CKIF", MaxLength = 1)]
        public string FixCheckInCounter
        {
            get { return _FixCheckInCounter; }
            set
            {
                if (_FixCheckInCounter != value)
                {
                    _FixCheckInCounter = value;
                    OnPropertyChanged("FixCheckInCounter");
                }
            }
        }
        //Terminal Check-in Counter
        private string _TerminalCheckInCounter;
        [Entity(SerializedName = "CKIT", MaxLength = 1)]
        public string TerminalCheckInCounter
        {
            get { return _TerminalCheckInCounter; }
            set
            {
                if (_TerminalCheckInCounter != value)
                {
                    _TerminalCheckInCounter = value;
                    OnPropertyChanged("TerminalCheckInCounter");
                }
            }
        }
        //Number of corresponding CodeShare
        private long? _NoOfCorCodeShare;
        [Entity(SerializedName = "COIX")]
        public long? NoOfCorCodeShare
        {
            get { return _NoOfCorCodeShare; }
            set
            {
                if (_NoOfCorCodeShare != value)
                {
                    _NoOfCorCodeShare = value;
                    OnPropertyChanged("NoOfCorCodeShare");
                }
            }
        }
        //Check-in Counter opening time (min. before departure)
        private string _CICOpenTime;
        [Entity(SerializedName = "COPN", MaxLength = 3)]
        public string CICOpenTime
        {
            get { return _CICOpenTime; }
            set
            {
                if (_CICOpenTime != value)
                {
                    _CICOpenTime = value;
                    OnPropertyChanged("CICOpenTime");
                }
            }
        }
        //Check-In Typ (Common oder Flight)
        private string _CIType;
        [Entity(SerializedName = "CTYP", MaxLength = 1)]
        public string CIType
        {
            get { return _CIType; }
            set
            {
                if (_CIType != value)
                {
                    _CIType = value;
                    OnPropertyChanged("CIType");
                }
            }
        }
        //Text for Display and POPs
        private string _TDPOP;
        [Entity(SerializedName = "DISP", MaxLength = 60)]
        public string TDPOP
        {
            get { return _TDPOP; }
            set
            {
                if (_TDPOP != value)
                {
                    _TDPOP = value;
                    OnPropertyChanged("TDPOP");
                }
            }
        }
        //Complete Flight No, Foriegn Key of AFTTAB.FLNO
        private string _FLightNo;
        [Entity(SerializedName = "FLNO", MaxLength = 9)]
        public string FLightNo
        {
            get { return _FLightNo; }
            set
            {
                if (_FLightNo != value)
                {
                    _FLightNo = value;
                    OnPropertyChanged("FLightNo");
                }
            }
        }
        //Flight URNO No, Foreign key from AFTTAB.URNO
        private long? _FLightURNo;
        [Entity(SerializedName = "FLNU")]
        public long? FLightURNo
        {
            get { return _FLightURNo; }
            set
            {
                if (_FLightURNo != value)
                {
                    _FLightURNo = value;
                    OnPropertyChanged("FLightURNo");
                }
            }
        }
        //Ground handling Service Contract URNO, Foreign key from GHPTAB.URNO
        private long? _GHPUrno;
        [Entity(SerializedName = "GHPU")]
        public long? GHPUrno
        {
            get { return _GHPUrno; }
            set
            {
                if (_GHPUrno != value)
                {
                    _GHPUrno = value;
                    OnPropertyChanged("GHPUrno");
                }
            }
        }
        //Ground handling Service URNO, Foreign key from GHSTAB.URNO
        private long? _GHSUrno;
        [Entity(SerializedName = "GHSU")]
        public long? GHSUrno
        {
            get { return _GHSUrno; }
            set
            {
                if (_GHSUrno != value)
                {
                    _GHSUrno = value;
                    OnPropertyChanged("GHSUrno");
                }
            }
        }
        //Premise Member, Foreign Key of GPMTAB.Urno
        private long? _PremiseMember;
        [Entity(SerializedName = "GPMU")]
        public long? PremiseMember
        {
            get { return _PremiseMember; }
            set
            {
                if (_PremiseMember != value)
                {
                    _PremiseMember = value;
                    OnPropertyChanged("PremiseMember");
                }
            }
        }
        //Home Airport of Allocated Counter
        private string _HPAllocateCounter;
        [Entity(SerializedName = "HOME", MaxLength = 3)]
        public string HPAllocateCounter
        {
            get { return _HPAllocateCounter; }
            set
            {
                if (_HPAllocateCounter != value)
                {
                    _HPAllocateCounter = value;
                    OnPropertyChanged("HPAllocateCounter");
                }
            }
        }
        //Remark Code
        private string _RemarkCode;
        [Entity(SerializedName = "LARC", MaxLength = 4)]
        public string RemarkCode
        {
            get { return _RemarkCode; }
            set
            {
                if (_RemarkCode != value)
                {
                    _RemarkCode = value;
                    OnPropertyChanged("RemarkCode");
                }
            }
        }
        //Status
        private string _Status;
        [Entity(SerializedName = "LAST", MaxLength = 1)]
        public string Status
        {
            get { return _Status; }
            set
            {
                if (_Status != value)
                {
                    _Status = value;
                    OnPropertyChanged("Status");
                }
            }
        }
        //Record Protocol Flag
        private string _RCProtocolFlag;
        [Entity(SerializedName = "PRFL", MaxLength = 1)]
        public string RCProtocolFlag
        {
            get { return _RCProtocolFlag; }
            set
            {
                if (_RCProtocolFlag != value)
                {
                    _RCProtocolFlag = value;
                    OnPropertyChanged("RCProtocolFlag");
                }
            }
        }
        //Remark
        private string _Remark;
        [Entity(SerializedName = "REMA", MaxLength = 60)]
        public string Remark
        {
            get { return _Remark; }
            set
            {
                if (_Remark != value)
                {
                    _Remark = value;
                    OnPropertyChanged("Remark");
                }
            }
        }
        //Conflict Status
        private string _ConflictStatus;
        [Entity(SerializedName = "STAT", MaxLength = 10)]
        public string ConflictStatus
        {
            get { return _ConflictStatus; }
            set
            {
                if (_ConflictStatus != value)
                {
                    _ConflictStatus = value;
                    OnPropertyChanged("ConflictStatus");
                }
            }
        }
        //Scheduled Time Departure
        private DateTime? _SCTDeparture;
        [Entity(SerializedName = "STOD")]
        public DateTime? SCTDeparture
        {
            get { return _SCTDeparture; }
            set
            {
                if (_SCTDeparture != value)
                {
                    _SCTDeparture = value;
                    OnPropertyChanged("SCTDeparture");
                }
            }
        }
    }
}
