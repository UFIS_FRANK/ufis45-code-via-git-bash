﻿#region ===== Source Code Information =====
/*
Project Name    : Ufis.Entities
Project Type    : Class Library
Platform        : Microsoft Visual C#.NET
File Name       : EntDbAircraftFamily.cs

Version         : 1.0.0
Created Date    : 23 - Sep - 2011
Complete Date   : 23 - Sep - 2011
Developed By    : Phyoe Khaing Min

Date Reason Updated By
---------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------

Copyright (C) All rights reserved.
*/
#endregion
using System;

namespace Ufis.Entities
{
    [Entity(SerializedName = "AFMTAB")] 
    public class EntDbAircraftFamily : BaseEntity
    {
        private string _code;
        private string _name;

        [Entity(SerializedName = "AFMC", MaxLength = 5, IsMandatory = true, IsUnique = true)]
        public string Code 
        { 
            get { return _code; }         
            set
            {
                if (_code != value)
                {
                    _code = value;
                    OnPropertyChanged("Code");
                }
            }
        }

        [Entity(SerializedName = "ANAM", MaxLength = 32)]
        public string Name 
        {
            get { return _name; }
            set
            {
                if (_name != value)
                {
                    _name = value;
                    OnPropertyChanged("Name");
                }
            }
        } 
    }
}
