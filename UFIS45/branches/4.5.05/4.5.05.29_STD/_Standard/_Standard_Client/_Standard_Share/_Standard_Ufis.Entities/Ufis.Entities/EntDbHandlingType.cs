﻿using System;

namespace Ufis.Entities
{
    [Entity(SerializedName = "HTYTAB")] 
    public class EntDbHandlingType : ValidityBaseEntity
    {
        private string _code;
        private string _name;
        private string _arrivalDepartureId;

        [Entity(SerializedName = "HTYP", MaxLength = 2, IsMandatory = true, IsUnique = true)]
        public string Code 
        { 
            get { return _code; }         
            set
            {
                if (_code != value)
                {
                    _code = value;
                    OnPropertyChanged("Code");
                }
            }
        }

        [Entity(SerializedName = "HNAM", MaxLength = 30)]
        public string Name 
        {
            get { return _name; }
            set
            {
                if (_name != value)
                {
                    _name = value;
                    OnPropertyChanged("Name");
                }
            }
        }

        [Entity(SerializedName = "ADID", MaxLength = 1)]
        public string ArrivalDepartureId
        {
            get { return _arrivalDepartureId; }
            set
            {
                if (_arrivalDepartureId != value)
                {
                    _arrivalDepartureId = value;
                    OnPropertyChanged("ArrivalDepartureId");
                }
            }
        }
    }
}
