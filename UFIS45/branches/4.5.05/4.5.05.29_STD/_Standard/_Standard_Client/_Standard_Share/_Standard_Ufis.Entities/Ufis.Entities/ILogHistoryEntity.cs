﻿using System;

namespace Ufis.Entities
{
    public interface ILogHistory
    {
        string CreatedBy { get; set; }
        DateTime? CreationDate { get; set; }
        string ModifiedBy { get; set; }
        DateTime? ModificationDate { get; set; }
    }
}
