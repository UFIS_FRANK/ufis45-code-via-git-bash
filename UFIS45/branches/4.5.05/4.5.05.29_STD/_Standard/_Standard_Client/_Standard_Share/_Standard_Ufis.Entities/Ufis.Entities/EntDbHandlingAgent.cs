﻿using System;

namespace Ufis.Entities
{
    [Entity(SerializedName = "HAGTAB")] 
    public class EntDbHandlingAgent : ValidityBaseEntity
    {
        private string _code;
        private string _name;
        private string _phoneNo;
        private string _faxNo;

        [Entity(SerializedName = "HSNA", MaxLength = 5, IsMandatory = true, IsUnique = true)]
        public string Code 
        { 
            get { return _code; }         
            set
            {
                if (_code != value)
                {
                    _code = value;
                    OnPropertyChanged("Code");
                }
            }
        }

        [Entity(SerializedName = "HNAM", MaxLength = 40)]
        public string Name 
        {
            get { return _name; }
            set
            {
                if (_name != value)
                {
                    _name = value;
                    OnPropertyChanged("Name");
                }
            }
        }

        [Entity(SerializedName = "TELE", MaxLength = 10)]
        public string PhoneNo
        {
            get { return _phoneNo; }
            set
            {
                if (_phoneNo != value)
                {
                    _phoneNo = value;
                    OnPropertyChanged("PhoneNo");
                }
            }
        }

        [Entity(SerializedName = "FAXN", MaxLength = 10)]
        public string FaxNo
        {
            get { return _faxNo; }
            set
            {
                if (_faxNo != value)
                {
                    _faxNo = value;
                    OnPropertyChanged("FaxNo");
                }
            }
        } 
    }
}
