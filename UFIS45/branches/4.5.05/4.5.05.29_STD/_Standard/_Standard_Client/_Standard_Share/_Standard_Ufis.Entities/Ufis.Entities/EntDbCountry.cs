﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Ufis.Entities
{
    [Entity(SerializedName = "COUTAB")]
    public class EntDbCountry : BaseEntity
    {
        private string _code2Letters;
        private string _code3Letters;
        private string _name;
        private string _continentCode;

        [Entity(SerializedName = "COC2", MaxLength = 2, IsMandatory = true, IsUnique = true)]
        public string Code2Letters
        {
            get { return _code2Letters; }
            set
            {
                if (_code2Letters != value)
                {
                    _code2Letters = value;
                    OnPropertyChanged("Code2Letters");
                }
            }
        }

        [Entity(SerializedName = "COC3", MaxLength = 3)]
        public string Code3Letters
        {
            get { return _code3Letters; }
            set
            {
                if (_code3Letters != value)
                {
                    _code3Letters = value;
                    OnPropertyChanged("Code3Letters");
                }
            }
        }

        [Entity(SerializedName = "CONM", MaxLength = 60)]
        public string Name
        {
            get { return _name; }
            set
            {
                if (_name != value)
                {
                    _name = value;
                    OnPropertyChanged("Name");
                }
            }
        }

        [Entity(SerializedName = "CNTC", MaxLength = 2)]
        public string ContinentCode
        {
            get { return _continentCode; }
            set
            {
                if (_continentCode != value)
                {
                    _continentCode = value;
                    OnPropertyChanged("ContinentCode");
                }
            }
        }
    }
}
