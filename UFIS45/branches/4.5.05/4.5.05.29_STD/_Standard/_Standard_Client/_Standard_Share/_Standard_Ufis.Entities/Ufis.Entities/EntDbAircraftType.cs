﻿#region ===== Source Code Information =====
/*
Project Name    : Ufis.Entities
Project Type    : Class Library
Platform        : Microsoft Visual C#.NET
File Name       : EntDbAircraftType.cs

Version         : 1.0.0
Created Date    : 23 - Sep - 2011
Complete Date   : 23 - Sep - 2011
Developed By    : Phyoe Khaing Min

Date Reason Updated By
---------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------

Copyright (C) All rights reserved.
*/
#endregion
using System;

namespace Ufis.Entities
{
    [Entity(SerializedName = "ACTTAB")]
    public class EntDbAircraftType : ValidityBaseEntity
    {
        public string _IATACode;
        public string _ICAOCode;
        public string _ICAOCodeModified;
        public string _name;
        public string _bodyType;
        public bool _noDockingSystem;
        public double? _height;
        public double? _wingspan;
        public double? _length;
        public int? _seats;
        public int? _engineCount;
        public string _engineType;

        [Entity(SerializedName = "ACT3", MaxLength = 3)]
        public string IATACode
        { 
            get { return _IATACode; }         
            set
            {
                if (_IATACode != value)
                {
                    _IATACode = value;
                    OnPropertyChanged("IATACode");
                }
            }
        }

        [Entity(SerializedName = "ACT5", MaxLength = 5, IsMandatory = true)]
        public string ICAOCode
        {
            get { return _ICAOCode; }
            set
            {
                if (_ICAOCode != value)
                {
                    _ICAOCode = value;
                    OnPropertyChanged("ICAOCode");
                }
            }
        }

        [Entity(SerializedName = "ACTI", MaxLength = 5)]
        public string ICAOCodeModified
        {
            get { return _ICAOCodeModified; }
            set
            {
                if (_ICAOCodeModified != value)
                {
                    _ICAOCodeModified = value;
                    OnPropertyChanged("ICAOCodeModified");
                }
            }
        }

        [Entity(SerializedName = "ACFN", MaxLength = 35)]
        public string Name
        {
            get { return _name; }
            set
            {
                if (_name != value)
                {
                    _name = value;
                    OnPropertyChanged("Name");
                }
            }
        }

        [Entity(SerializedName = "ACBT", MaxLength = 1)]
        public string BodyType
        {
            get { return _bodyType; }
            set
            {
                if (_bodyType != value)
                {
                    _bodyType = value;
                    OnPropertyChanged("BodyType");
                }
            }
        }

        [Entity(SerializedName = "NADS", SourceDataType = typeof(String), MaxLength = 1, TrueValue = "X", FalseValue = " ")]
        public bool NoDockingSystem
        {
            get { return _noDockingSystem; }
            set
            {
                if (_noDockingSystem != value)
                {
                    _noDockingSystem = value;
                    OnPropertyChanged("NoDockingSystem");
                }
            }
        }

        [Entity(SerializedName = "ACHE", SourceDataType = typeof(String), MaxLength = 5)]
        public double? Height
        {
            get { return _height; }
            set
            {
                if (_height != value)
                {
                    _height = value;
                    OnPropertyChanged("Height");
                }
            }
        }

        [Entity(SerializedName = "ACWS", SourceDataType = typeof(String), MaxLength = 5)]
        public double? Wingspan
        {
            get { return _wingspan; }
            set
            {
                if (_wingspan != value)
                {
                    _wingspan = value;
                    OnPropertyChanged("Wingspan");
                }
            }
        }

        [Entity(SerializedName = "ACLE", SourceDataType = typeof(String), MaxLength = 5)]
        public double? Length
        {
            get { return _length; }
            set
            {
                if (_length != value)
                {
                    _length = value;
                    OnPropertyChanged("Length");
                }
            }
        }

        [Entity(SerializedName = "SEAT", SourceDataType = typeof(String), MaxLength = 3)]
        public int? Seats
        {
            get { return _seats; }
            set
            {
                if (_seats != value)
                {
                    _seats = value;
                    OnPropertyChanged("Seats");
                }
            }
        }

        [Entity(SerializedName = "ENNO", SourceDataType = typeof(String), MaxLength = 1)]
        public int? EngineCount
        {
            get { return _engineCount; }
            set
            {
                if (_engineCount != value)
                {
                    _engineCount = value;
                    OnPropertyChanged("EngineCount");
                }
            }
        }

        [Entity(SerializedName = "ENTY", MaxLength = 5)]
        public string EngineType
        {
            get { return _engineType; }
            set
            {
                if (_engineType != value)
                {
                    _engineType = value;
                    OnPropertyChanged("EngineType");
                }
            }
        }
    }
}
