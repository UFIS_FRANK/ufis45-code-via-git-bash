﻿using System;

namespace Ufis.Entities
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Property)]
    public class EntityAttribute : System.Attribute 
    {
        public string SerializedName { get; set; }
        public Type SourceDataType { get; set; }
        public bool IsPrimaryKey { get; set; }
        public bool IsUnique { get; set; }
        public bool IsMandatory { get; set; }
        public bool IsReadOnly { get; set; }
        public int MaxLength { get; set; }
        public object TrueValue { get; set; }
        public object FalseValue { get; set; }
    }
}
