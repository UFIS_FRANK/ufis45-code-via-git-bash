﻿using System;

namespace Ufis.Entities
{
    [Entity(SerializedName = "SSRTAB")]
    public class EntDbSeasonalFlightSchedule : BaseEntity
    {
        private string _seasonName;
        private string _seasonKey; 
        private DateTime? _validFrom;
        private DateTime? _validTo;
        private string _daysOfOperation;
        private int? _frequency;
        private int? _nightsStop;
        private string _aircraftIATACode;
        private string _aircraftICAOCode;
        private string _flightOperationalType;
        private string _fullFlightNumberOfArrival;
        private string _airlineIATACodeOfArrival;
        private string _airlineICAOCodeOfArrival;
        private string _flightNumberOfArrival;
        private string _flightSuffixOfArrival;
        private int? _codeShareCountOfArrival;
        private string _codeShareListOfArrival;
        private TimeSpan? _standardTimeOfArrival;
        private TimeSpan? _standardTimeOfDepartureFromOrigin;
        private string _originAirportIATACode;
        private string _originAirportICAOCode;
        private string _viaAirportIATACodeOfArrival;
        private string _viaAirportICAOCodeOfArrival;
        private int? _viaCountOfArrival;
        private string _viaListOfArrival;
        private string _natureCodeOfArrival;
        private string _remarkOfArrival;
        private string _fullFlightNumberOfDeparture;
        private string _airlineIATACodeOfDeparture;
        private string _airlineICAOCodeOfDeparture;
        private string _flightNumberOfDeparture;
        private string _flightSuffixOfDeparture;
        private int? _codeShareCountOfDeparture;
        private string _codeShareListOfDeparture;
        private TimeSpan? _standardTimeOfDeparture;
        private TimeSpan? _standardTimeOfArrivalAtDestination;
        private string _destinationAirportIATACode;
        private string _destinationAirportICAOCode;
        private string _viaAirportIATACodeOfDeparture;
        private string _viaAirportICAOCodeOfDeparture;
        private int? _viaCountOfDeparture;
        private string _viaListOfDeparture;
        private string _natureCodeOfDeparture;
        private string _remarkOfDeparture;
        private string _url1;
        private string _url2;
        private string _url3;
        private string _serviceTypeOfArrival;
        private string _serviceTypeOfDeparture;

        /// <summary>
        /// Property get and set for Season
        /// </summary>
        [Entity(SerializedName = "SEAS", MaxLength = 6)]
        public string SeasonName
        {
            get { return _seasonName; }
            set
            {
                if (_seasonName != value)
                {
                    _seasonName = value;
                    OnPropertyChanged("SeasonName");
                }
            }
        }

        [Entity(SerializedName = "SKEY", MaxLength = 6)]
        public string SeasonKey
        {
            get { return _seasonKey; }
            set
            {
                if (_seasonKey != value)
                {
                    _seasonKey = value;
                    OnPropertyChanged("SeasonKey");
                }
            }
        }

        [EntityAttribute(SerializedName = "VAFR", SourceDataType = typeof(String), MaxLength = 8)]
        public DateTime? ValidFrom
        {
            get { return _validFrom; }
            set
            {
                if (_validFrom != value)
                {
                    _validFrom = value;
                    OnPropertyChanged("ValidFrom");
                }
            }
        }

        [EntityAttribute(SerializedName = "VATO", SourceDataType = typeof(String), MaxLength = 8)]
        public DateTime? ValidTo
        {
            get { return _validTo; }
            set
            {
                if (_validTo != value)
                {
                    _validTo = value;
                    OnPropertyChanged("ValidTo");
                }
            }
        }

        /// <summary>
        /// Property get and set for Days Of Operation
        /// </summary>
        [Entity(SerializedName = "DOOP", MaxLength = 7)]
        public string DaysOfOperation
        {
            get { return _daysOfOperation; }
            set
            {
                if (_daysOfOperation != value)
                {
                    _daysOfOperation = value;
                    OnPropertyChanged("DaysOfOperation");
                }
            }
        }

        /// <summary>
        /// Property get and set for Frequency during the period
        /// </summary>
        [Entity(SerializedName = "FREQ", MaxLength = 1)]
        public int? Frequency
        {
            get { return _frequency; }
            set
            {
                if (_frequency != value)
                {
                    _frequency = value;
                    OnPropertyChanged("Frequency");
                }
            }
        }

        /// <summary>
        /// Property get and set for Days Offset bewteen Arrival and Departure Flight
        /// </summary>
        [Entity(SerializedName = "NSTP", MaxLength = 3)]
        public int? NightsStop
        {
            get { return _nightsStop; }
            set
            {
                if (_nightsStop != value)
                {
                    _nightsStop = value;
                    OnPropertyChanged("NightsStop");
                }
            }
        }

        /// <summary>
        /// Property get and set for Aircraft IATA Code
        /// </summary>
        [Entity(SerializedName = "ACT3", MaxLength = 3)]
        public string AircraftIATACode
        {
            get { return _aircraftIATACode; }
            set
            {
                if (_aircraftIATACode != value)
                {
                    _aircraftIATACode = value;
                    OnPropertyChanged("AircraftIATACode");
                }
            }
        }

        /// <summary>
        /// Property get and set for Aircraft ICAO Code
        /// </summary>
        [Entity(SerializedName = "ACT5", MaxLength = 5)]
        public string AircraftICAOCode
        {
            get { return _aircraftICAOCode; }
            set
            {
                if (_aircraftICAOCode != value)
                {
                    _aircraftICAOCode = value;
                    OnPropertyChanged("AircraftICAOCode");
                }
            }
        }

        /// <summary>
        /// Property get and set for Flight Type
        /// </summary>
        [Entity(SerializedName = "FTYP", MaxLength = 1)]
        public string FlightOperationalType
        {
            get { return _flightOperationalType; }
            set
            {
                if (_flightOperationalType != value)
                {
                    _flightOperationalType = value;
                    OnPropertyChanged("FlightOperationalType");
                }
            }
        }

        /// <summary>
        /// Property get and set for Arrival Full Flight Number
        /// </summary>
        [Entity(SerializedName = "FLTA", MaxLength = 9)]
        public string FullFlightNumberOfArrival
        {
            get { return _fullFlightNumberOfArrival; }
            set
            {
                if (_fullFlightNumberOfArrival != value)
                {
                    _fullFlightNumberOfArrival = value;
                    OnPropertyChanged("FullFlightNumberOfArrival");
                }
            }
        }

        /// <summary>
        /// Property get and set for Arrival Airline IATA Code
        /// </summary>
        [Entity(SerializedName = "AL2A", MaxLength = 2)]
        public string AirlineIATACodeOfArrival
        {
            get { return _airlineIATACodeOfArrival; }
            set
            {
                if (_airlineIATACodeOfArrival != value)
                {
                    _airlineIATACodeOfArrival = value;
                    OnPropertyChanged("AirlineIATACodeOfArrival");
                }
            }
        }

        /// <summary>
        /// Property get and set for Arrival Airline ICAO Code
        /// </summary>
        [Entity(SerializedName = "AL3A", MaxLength = 2)]
        public string AirlineICAOCodeOfArrival
        {
            get { return _airlineICAOCodeOfArrival; }
            set
            {
                if (_airlineICAOCodeOfArrival != value)
                {
                    _airlineICAOCodeOfArrival = value;
                    OnPropertyChanged("AirlineICAOCodeOfArrival");
                }
            }
        }

        /// <summary>
        /// Property get and set for Arrival Flight Number Only
        /// </summary>
        [Entity(SerializedName = "FLNA", MaxLength = 5)]
        public string FlightNumberOfArrival
        {
            get { return _flightNumberOfArrival; }
            set
            {
                if (_flightNumberOfArrival != value)
                {
                    _flightNumberOfArrival = value;
                    OnPropertyChanged("FlightNumberOfArrival");
                }
            }
        }

        /// <summary>
        /// Property get and set for Arrival Flight Suffix
        /// </summary>
        [Entity(SerializedName = "FLSA", MaxLength = 1)]
        public string FlightSuffixOfArrival
        {
            get { return _flightSuffixOfArrival; }
            set
            {
                if (_flightSuffixOfArrival != value)
                {
                    _flightSuffixOfArrival = value;
                    OnPropertyChanged("FlightSuffixOfArrival");
                }
            }
        }

        /// <summary>
        /// Property get and set for code share count for Arrival Route
        /// </summary>
        [Entity(SerializedName = "JCNA", MaxLength = 2)]
        public int? CodeShareCountOfArrival
        {
            get { return _codeShareCountOfArrival; }
            set
            {
                if (_codeShareCountOfArrival != value)
                {
                    _codeShareCountOfArrival = value;
                    OnPropertyChanged("CodeShareCountOfArrival");
                }
            }
        }

        /// <summary>
        /// Property get and set for code share list for Arrival Route
        /// </summary>
        [Entity(SerializedName = "JFNA", MaxLength = 110)]
        public string CodeShareListOfArrival
        {
            get { return _codeShareListOfArrival; }
            set
            {
                if (_codeShareListOfArrival != value)
                {
                    _codeShareListOfArrival = value;
                    OnPropertyChanged("CodeShareListOfArrival");
                }
            }
        }

        /// <summary>
        /// Property get and set for Standard time of arrival for Arrival Route
        /// </summary>
        [Entity(SerializedName = "STOA", MaxLength = 4)]
        public TimeSpan? StandardTimeOfArrival
        {
            get { return _standardTimeOfArrival; }
            set
            {
                if (_standardTimeOfArrival != value)
                {
                    _standardTimeOfArrival = value;
                    OnPropertyChanged("StandardTimeOfArrival");
                }
            }
        }

        /// <summary>
        /// Property get and set for Standard time of departure from origin for Arrival Route
        /// </summary>
        [Entity(SerializedName = "STDA", MaxLength = 4)]
        public TimeSpan? StandardTimeOfDepartureFromOrigin
        {
            get { return _standardTimeOfDepartureFromOrigin; }
            set
            {
                if (_standardTimeOfDepartureFromOrigin != value)
                {
                    _standardTimeOfDepartureFromOrigin = value;
                    OnPropertyChanged("StandardTimeOfDepartureFromOrigin");
                }
            }
        }

        /// <summary>
        /// Property get and set for Arrival Route Origin IATA Code
        /// </summary>
        [Entity(SerializedName = "ORG3", MaxLength = 3)]
        public string OriginAirportIATACode
        {
            get { return _originAirportIATACode; }
            set
            {
                if (_originAirportIATACode != value)
                {
                    _originAirportIATACode = value;
                    OnPropertyChanged("OriginAirportIATACode");
                }
            }
        }

        /// <summary>
        /// Property get and set for Arrival Route Origin ICAO Code
        /// </summary>
        [Entity(SerializedName = "ORG4", MaxLength = 4)]
        public string OriginAirportICAOCode
        {
            get { return _originAirportICAOCode; }
            set
            {
                if (_originAirportICAOCode != value)
                {
                    _originAirportICAOCode = value;
                    OnPropertyChanged("OriginAirportICAOCode");
                }
            }
        }

        /// <summary>
        /// Property get and set for last via IATA Code for Arrival Route
        /// </summary>
        [Entity(SerializedName = "VI3A", MaxLength = 3)]
        public string ViaAirportIATACodeOfArrival
        {
            get { return _viaAirportIATACodeOfArrival; }
            set
            {
                if (_viaAirportIATACodeOfArrival != value)
                {
                    _viaAirportIATACodeOfArrival = value;
                    OnPropertyChanged("ViaAirportIATACodeOfArrival");
                }
            }
        }

        /// <summary>
        /// Property get and set for last via ICAO Code for Arrival Route
        /// </summary>
        [Entity(SerializedName = "VI4A", MaxLength = 3)]
        public string ViaAirportICAOCodeOfArrival
        {
            get { return _viaAirportICAOCodeOfArrival; }
            set
            {
                if (_viaAirportICAOCodeOfArrival != value)
                {
                    _viaAirportICAOCodeOfArrival = value;
                    OnPropertyChanged("ViaAirportICAOCodeOfArrival");
                }
            }
        }

        /// <summary>
        /// Property get and set for via count for Arrival Route
        /// </summary>
        [Entity(SerializedName = "VINA", MaxLength = 2)]
        public int? ViaCountOfArrival
        {
            get { return _viaCountOfArrival; }
            set
            {
                if (_viaCountOfArrival != value)
                {
                    _viaCountOfArrival = value;
                    OnPropertyChanged("ViaCountOfArrival");
                }
            }
        }

        /// <summary>
        /// Property get and set for list of via for Arrival Route
        /// </summary>
        [Entity(SerializedName = "VILA", MaxLength = 256)]
        public string ViaListOfArrival
        {
            get { return _viaListOfArrival; }
            set
            {
                if (_viaListOfArrival != value)
                {
                    _viaListOfArrival = value;
                    OnPropertyChanged("ViaListOfArrival");
                }
            }
        }

        /// <summary>
        /// Property get and set for nature code for Arrival Route
        /// </summary>
        [Entity(SerializedName = "TTPA", MaxLength = 5)]
        public string NatureCodeOfArrival
        {
            get { return _natureCodeOfArrival; }
            set
            {
                if (_natureCodeOfArrival != value)
                {
                    _natureCodeOfArrival = value;
                    OnPropertyChanged("NatureCodeOfArrival");
                }
            }
        }

        /// <summary>
        /// Property get and set for remark for Arrival Route
        /// </summary>
        [Entity(SerializedName = "REMA", MaxLength = 256)]
        public string RemarkOfArrival
        {
            get { return _remarkOfArrival; }
            set
            {
                if (_remarkOfArrival != value)
                {
                    _remarkOfArrival = value;
                    OnPropertyChanged("RemarkOfArrival");
                }
            }
        }

        /// <summary>
        /// Property get and set for Departure Full Flight Number
        /// </summary>
        [Entity(SerializedName = "FLTD", MaxLength = 9)]
        public string FullFlightNumberOfDeparture
        {
            get { return _fullFlightNumberOfDeparture; }
            set
            {
                if (_fullFlightNumberOfDeparture != value)
                {
                    _fullFlightNumberOfDeparture = value;
                    OnPropertyChanged("FullFlightNumberOfDeparture");
                }
            }
        }

        /// <summary>
        /// Property get and set for Departure Airline IATA Code
        /// </summary>
        [Entity(SerializedName = "AL2D", MaxLength = 2)]
        public string AirlineIATACodeOfDeparture
        {
            get { return _airlineIATACodeOfDeparture; }
            set
            {
                if (_airlineIATACodeOfDeparture != value)
                {
                    _airlineIATACodeOfDeparture = value;
                    OnPropertyChanged("AirlineIATACodeOfDeparture");
                }
            }
        }

        /// <summary>
        /// Property get and set for Departure Airline ICAO Code
        /// </summary>
        [Entity(SerializedName = "AL3D", MaxLength = 2)]
        public string AirlineICAOCodeOfDeparture
        {
            get { return _airlineICAOCodeOfDeparture; }
            set
            {
                if (_airlineICAOCodeOfDeparture != value)
                {
                    _airlineICAOCodeOfDeparture = value;
                    OnPropertyChanged("AirlineICAOCodeOfDeparture");
                }
            }
        }

        /// <summary>
        /// Property get and set for Departure Flight Number Only
        /// </summary>
        [Entity(SerializedName = "FLND", MaxLength = 5)]
        public string FlightNumberOfDeparture
        {
            get { return _flightNumberOfDeparture; }
            set
            {
                if (_flightNumberOfDeparture != value)
                {
                    _flightNumberOfDeparture = value;
                    OnPropertyChanged("FlightNumberOfDeparture");
                }
            }
        }

        /// <summary>
        /// Property get and set for Departure Flight Suffix
        /// </summary>
        [Entity(SerializedName = "FLSD", MaxLength = 1)]
        public string FlightSuffixOfDeparture
        {
            get { return _flightSuffixOfDeparture; }
            set
            {
                if (_flightSuffixOfDeparture != value)
                {
                    _flightSuffixOfDeparture = value;
                    OnPropertyChanged("FlightSuffixOfDeparture");
                }
            }
        }

        /// <summary>
        /// Property get and set for code share count for Departure Route
        /// </summary>
        [Entity(SerializedName = "JCND", MaxLength = 2)]
        public int? CodeShareCountOfDeparture
        {
            get { return _codeShareCountOfDeparture; }
            set
            {
                if (_codeShareCountOfDeparture != value)
                {
                    _codeShareCountOfDeparture = value;
                    OnPropertyChanged("CodeShareCountOfDeparture");
                }
            }
        }

        /// <summary>
        /// Property get and set for code share list for Departure Route
        /// </summary>
        [Entity(SerializedName = "JFND", MaxLength = 110)]
        public string CodeShareListOfDeparture
        {
            get { return _codeShareListOfDeparture; }
            set
            {
                if (_codeShareListOfDeparture != value)
                {
                    _codeShareListOfDeparture = value;
                    OnPropertyChanged("CodeShareListOfDeparture");
                }
            }
        }

        /// <summary>
        /// Property get and set for Standard time of Departure for Departure Route
        /// </summary>
        [Entity(SerializedName = "STOD", MaxLength = 4)]
        public TimeSpan? StandardTimeOfDeparture
        {
            get { return _standardTimeOfDeparture; }
            set
            {
                if (_standardTimeOfDeparture != value)
                {
                    _standardTimeOfDeparture = value;
                    OnPropertyChanged("StandardTimeOfDeparture");
                }
            }
        }

        /// <summary>
        /// Property get and set for Standard time of arrival at destination for Departure Route
        /// </summary>
        [Entity(SerializedName = "STAD", MaxLength = 4)]
        public TimeSpan? StandardTimeOfArrivalAtDestination
        {
            get { return _standardTimeOfArrivalAtDestination; }
            set
            {
                if (_standardTimeOfArrivalAtDestination != value)
                {
                    _standardTimeOfArrivalAtDestination = value;
                    OnPropertyChanged("StandardTimeOfArrivalAtDestination");
                }
            }
        }

        /// <summary>
        /// Property get and set for Departure Route Destination IATA Code
        /// </summary>
        [Entity(SerializedName = "DES3", MaxLength = 3)]
        public string DestinationAirportIATACode
        {
            get { return _destinationAirportIATACode; }
            set
            {
                if (_destinationAirportIATACode != value)
                {
                    _destinationAirportIATACode = value;
                    OnPropertyChanged("DestinationAirportIATACode");
                }
            }
        }

        /// <summary>
        /// Property get and set for Departure Route Destination ICAO Code
        /// </summary>
        [Entity(SerializedName = "DES4", MaxLength = 4)]
        public string DestinationAirportICAOCode
        {
            get { return _destinationAirportICAOCode; }
            set
            {
                if (_destinationAirportICAOCode != value)
                {
                    _destinationAirportICAOCode = value;
                    OnPropertyChanged("DestinationAirportICAOCode");
                }
            }
        }

        /// <summary>
        /// Property get and set for last via IATA Code for Departure Route
        /// </summary>
        [Entity(SerializedName = "VI3D", MaxLength = 3)]
        public string ViaAirportIATACodeOfDeparture
        {
            get { return _viaAirportIATACodeOfDeparture; }
            set
            {
                if (_viaAirportIATACodeOfDeparture != value)
                {
                    _viaAirportIATACodeOfDeparture = value;
                    OnPropertyChanged("ViaAirportIATACodeOfDeparture");
                }
            }
        }

        /// <summary>
        /// Property get and set for last via ICAO Code for Departure Route
        /// </summary>
        [Entity(SerializedName = "VI4D", MaxLength = 3)]
        public string ViaAirportICAOCodeOfDeparture
        {
            get { return _viaAirportICAOCodeOfDeparture; }
            set
            {
                if (_viaAirportICAOCodeOfDeparture != value)
                {
                    _viaAirportICAOCodeOfDeparture = value;
                    OnPropertyChanged("ViaAirportICAOCodeOfDeparture");
                }
            }
        }

        /// <summary>
        /// Property get and set for via count for Departure Route
        /// </summary>
        [Entity(SerializedName = "VIND", MaxLength = 2)]
        public int? ViaCountOfDeparture
        {
            get { return _viaCountOfDeparture; }
            set
            {
                if (_viaCountOfDeparture != value)
                {
                    _viaCountOfDeparture = value;
                    OnPropertyChanged("ViaCountOfDeparture");
                }
            }
        }

        /// <summary>
        /// Property get and set for list of via for Departure Route
        /// </summary>
        [Entity(SerializedName = "VILD", MaxLength = 256)]
        public string ViaListOfDeparture
        {
            get { return _viaListOfDeparture; }
            set
            {
                if (_viaListOfDeparture != value)
                {
                    _viaListOfDeparture = value;
                    OnPropertyChanged("ViaListOfDeparture");
                }
            }
        }

        /// <summary>
        /// Property get and set for nature code for Departure Route
        /// </summary>
        [Entity(SerializedName = "TTPD", MaxLength = 5)]
        public string NatureCodeOfDeparture
        {
            get { return _natureCodeOfDeparture; }
            set
            {
                if (_natureCodeOfDeparture != value)
                {
                    _natureCodeOfDeparture = value;
                    OnPropertyChanged("NatureCodeOfDeparture");
                }
            }
        }

        /// <summary>
        /// Property get and set for remark for Departure Route
        /// </summary>
        [Entity(SerializedName = "REMD", MaxLength = 256)]
        public string RemarkOfDeparture
        {
            get { return _remarkOfDeparture; }
            set
            {
                if (_remarkOfDeparture != value)
                {
                    _remarkOfDeparture = value;
                    OnPropertyChanged("RemarkOfDeparture");
                }
            }
        }

        /// <summary>
        /// Property get and set for Url1
        /// </summary>
        [Entity(SerializedName = "URL1", MaxLength = 2000)]
        public string Url1
        {
            get { return _url1; }
            set
            {
                if (_url1 != value)
                {
                    _url1 = value;
                    OnPropertyChanged("Url1");
                }
            }
        }

        /// <summary>
        /// Property get and set for Url2
        /// </summary>
        [Entity(SerializedName = "URL2", MaxLength = 2000)]
        public string Url2
        {
            get { return _url2; }
            set
            {
                if (_url2 != value)
                {
                    _url2 = value;
                    OnPropertyChanged("Url2");
                }
            }
        }

        /// <summary>
        /// Property get and set for Url3
        /// </summary>
        [Entity(SerializedName = "URL3", MaxLength = 2000)]
        public string Url3
        {
            get { return _url3; }
            set
            {
                if (_url3 != value)
                {
                    _url3 = value;
                    OnPropertyChanged("Url3");
                }
            }
        }

        /// <summary>
        /// Property get and set for STYA
        /// </summary>
        [Entity(SerializedName = "STYA", MaxLength = 2)]
        public string ServiceTypeOfArrival
        {
            get { return _serviceTypeOfArrival; }
            set
            {
                if (_serviceTypeOfArrival != value)
                {
                    _serviceTypeOfArrival = value;
                    OnPropertyChanged("ServiceTypeOfArrival");
                }
            }
        }

        /// <summary>
        /// Property get and set for STYD
        /// </summary>
        [Entity(SerializedName = "STYD", MaxLength = 2)]
        public string ServiceTypeOfDeparture
        {
            get { return _serviceTypeOfDeparture; }
            set
            {
                if (_serviceTypeOfDeparture != value)
                {
                    _serviceTypeOfDeparture = value;
                    OnPropertyChanged("ServiceTypeOfDeparture");
                }
            }
        }
    }
}
