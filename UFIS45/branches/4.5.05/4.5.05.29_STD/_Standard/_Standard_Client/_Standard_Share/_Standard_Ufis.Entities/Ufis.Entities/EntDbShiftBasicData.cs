﻿#region ===== Source Code Information =====
/*
Project Name    : Ufis.Entities
Project Type    : Class Library
Platform        : Microsoft Visual C#.NET
File Name       : EntDbShiftBasicData.cs

Version         : 1.0.0
Created Date    : 22 - Dec - 2011
Complete Date   : 22 - Dec - 2011
Created By      : Siva

Date Reason Updated By
---------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------

Copyright (C) All rights reserved.
*/
#endregion
using System;

namespace Ufis.Entities
{
    [Entity(SerializedName = "BSDTAB")]
    public class EntDbShiftBasicData : ValidityBaseEntity
    {
        private string _shiftFlag;
        private string _breakDuration1;
        private string _breakDuration2;
        private bool _breakFlag;
        private string _breakLocationFrom;
        private string _breakLocationRelative;
        private string _breakLocationTo;
        private string _basicShiftCode;
        private string _externalShiftCode;
        private string _shortShiftName;
        private string _shiftName;
        private string _shiftSAPCode;
        private string _contractCode;
        private string _ORGTABCode;
        private string _earlierShiftBegin;
        private string _PFCTABCode;
        private bool _DELTABFlag;
        private string _shiftEnd;
        private string _flag;
        private string _remark;
        private string _regularShiftBegin;
        private string _regularShiftDuration;
        private string _extensionShiftTime;
        private string _reductionShiftTime;
        private string _flagType;
        //private DateTime? _validFrom;
        //private DateTime? _validTo;


        [Entity(SerializedName = "BEWC", MaxLength = 5)]
        public string ShiftFlag
        {
            get { return _shiftFlag; }
            set
            {
                if (_shiftFlag != value)
                {
                    _shiftFlag = value;
                    OnPropertyChanged("ShiftFlag");
                }
            }
        }

        [Entity(SerializedName = "BKD1", MaxLength = 4)]
        public string FirstBreakDuration
        {
            get { return _breakDuration1; }
            set
            {
                if (_breakDuration1 != value)
                {
                    _breakDuration1 = value;
                    OnPropertyChanged("FirstBreakDuration");
                }
            }
        }

        [Entity(SerializedName = "BKD2", MaxLength = 4)]
        public string SecondBreakDuration
        {
            get { return _breakDuration2; }
            set
            {
                if (_breakDuration2 != value)
                {
                    _breakDuration2 = value;
                    OnPropertyChanged("SecondBreakDuration");
                }
            }
        }

        [Entity(SerializedName = "BKDP", SourceDataType = typeof(String), MaxLength = 1, TrueValue = "X", FalseValue = " ")]
        public bool BreakFlag
        {
            get { return _breakFlag; }
            set
            {
                if (_breakFlag != value)
                {
                    _breakFlag = value;
                    OnPropertyChanged("BreakFlag");
                }
            }
        }

        [Entity(SerializedName = "BKF1", MaxLength = 4)]
        public string BreakLocationFrom
        {
            get { return _breakLocationFrom; }
            set
            {
                if (_breakLocationFrom != value)
                {
                    _breakLocationFrom = value;
                    OnPropertyChanged("BreakLocationFrom");
                }
            }
        }

        [Entity(SerializedName = "BKR1", MaxLength = 1)]
        public string BreakLocationRelative
        {
            get { return _breakLocationRelative; }
            set
            {
                if (_breakLocationRelative != value)
                {
                    _breakLocationRelative = value;
                    OnPropertyChanged("BreakLocationRelative");
                }
            }
        }

        [Entity(SerializedName = "BKT1", MaxLength = 4)]
        public string BreakLocationTo
        {
            get { return _breakLocationTo; }
            set
            {
                if (_breakLocationTo != value)
                {
                    _breakLocationTo = value;
                    OnPropertyChanged("BreakLocationTo");
                }
            }
        }

        [Entity(SerializedName = "BSDC", MaxLength = 8)]
        public string BasicShiftCode
        {
            get { return _basicShiftCode; }
            set
            {
                if (_basicShiftCode != value)
                {
                    _basicShiftCode = value;
                    OnPropertyChanged("BasicShiftCode");
                }
            }
        }

        [Entity(SerializedName = "BSDE", MaxLength = 8)]
        public string ExternalShiftCode
        {
            get { return _externalShiftCode; }
            set
            {
                if (_externalShiftCode != value)
                {
                    _externalShiftCode = value;
                    OnPropertyChanged("ExternalShiftCode");
                }
            }
        }

        [Entity(SerializedName = "BSDK", MaxLength = 12)]
        public string ShortShiftName
        {
            get { return _shortShiftName; }
            set
            {
                if (_shortShiftName != value)
                {
                    _shortShiftName = value;
                    OnPropertyChanged("ShortShiftName");
                }
            }
        }

        [Entity(SerializedName = "BSDN", MaxLength = 40)]
        public string ShiftName
        {
            get { return _shiftName; }
            set
            {
                if (_shiftName != value)
                {
                    _shiftName = value;
                    OnPropertyChanged("ShiftName");
                }
            }
        }

        [Entity(SerializedName = "BSDS", MaxLength = 3)]
        public string ShiftSAPCode
        {
            get { return _shiftSAPCode; }
            set
            {
                if (_shiftSAPCode != value)
                {
                    _shiftSAPCode = value;
                    OnPropertyChanged("ShiftSAPCode");
                }
            }
        }

        [Entity(SerializedName = "CTRC", MaxLength = 5)]
        public string ContractCode
        {
            get { return _contractCode; }
            set
            {
                if (_contractCode != value)
                {
                    _contractCode = value;
                    OnPropertyChanged("ContractCode");
                }
            }
        }

        [Entity(SerializedName = "DPT1", MaxLength = 8)]
        public string ORGTABCode
        {
            get { return _ORGTABCode; }
            set
            {
                if (_ORGTABCode != value)
                {
                    _ORGTABCode = value;
                    OnPropertyChanged("ORGTABCode");
                }
            }
        }

        [Entity(SerializedName = "ESBG", MaxLength = 4)]
        public string EarliertShiftBegin
        {
            get { return _earlierShiftBegin; }
            set
            {
                if (_earlierShiftBegin != value)
                {
                    _earlierShiftBegin = value;
                    OnPropertyChanged("EarliertShiftBegin");
                }
            }
        }

        [Entity(SerializedName = "FCTC", MaxLength = 5)]
        public string PFCTABCode
        {
            get { return _PFCTABCode; }
            set
            {
                if (_PFCTABCode != value)
                {
                    _PFCTABCode = value;
                    OnPropertyChanged("PFCTABCode");
                }
            }
        }

        [Entity(SerializedName = "FDEL", SourceDataType = typeof(String), MaxLength = 1, TrueValue = "X", FalseValue = " ")]
        public bool DELTABFlag
        {
            get { return _DELTABFlag; }
            set
            {
                if (_DELTABFlag != value)
                {
                    _DELTABFlag = value;
                    OnPropertyChanged("DELTABFlag");
                }
            }
        }

        [Entity(SerializedName = "LSEN", MaxLength = 4)]
        public string ShiftEnd
        {
            get { return _shiftEnd; }
            set
            {
                if (_shiftEnd != value)
                {
                    _shiftEnd = value;
                    OnPropertyChanged("ShiftEnd");
                }
            }
        }


        [Entity(SerializedName = "PRFL", MaxLength = 1)]
        public string Flag
        {
            get { return _flag; }
            set
            {
                if (_flag != value)
                {
                    _flag = value;
                    OnPropertyChanged("Flag");
                }
            }
        }


        [Entity(SerializedName = "REMA", MaxLength = 60)]
        public string Remark
        {
            get { return _remark; }
            set
            {
                if (_remark != value)
                {
                    _remark = value;
                    OnPropertyChanged("Remark");
                }
            }
        }

        [Entity(SerializedName = "RGSB", MaxLength = 14)]
        public string RegularShiftBegin
        {
            get { return _regularShiftBegin; }
            set
            {
                if (_regularShiftBegin != value)
                {
                    _regularShiftBegin = value;
                    OnPropertyChanged("RegularShiftBegin");
                }
            }
        }

        [Entity(SerializedName = "SDU1", MaxLength = 4)]
        public string RegularShiftDuration
        {
            get { return _regularShiftDuration; }
            set
            {
                if (_regularShiftDuration != value)
                {
                    _regularShiftDuration = value;
                    OnPropertyChanged("RegularShiftDuration");
                }
            }
        }

        [Entity(SerializedName = "SEX1", MaxLength = 4)]
        public string ExtensionShiftTime
        {
            get { return _extensionShiftTime; }
            set
            {
                if (_extensionShiftTime != value)
                {
                    _extensionShiftTime = value;
                    OnPropertyChanged("ExtensionShiftTime");
                }
            }
        }

        [Entity(SerializedName = "SSH1", MaxLength = 4)]
        public string ReductionShiftTime
        {
            get { return _reductionShiftTime; }
            set
            {
                if (_reductionShiftTime != value)
                {
                    _reductionShiftTime = value;
                    OnPropertyChanged("ReductionShiftTime");
                }
            }
        }


        [Entity(SerializedName = "TYPE", MaxLength = 1)]
        public string FlagType
        {
            get { return _flagType; }
            set
            {
                if (_flagType != value)
                {
                    _flagType = value;
                    OnPropertyChanged("FlagType");
                }
            }
        }


        //[Entity(SerializedName = "VAFR", SourceDataType = typeof(String), IsMandatory = true)]
        //public DateTime? ValidFrom
        //{
        //    get { return _validFrom; }
        //    set
        //    {
        //        if (_validFrom != value)
        //        {
        //            _validFrom = value;
        //            OnPropertyChanged("ValidFrom");
        //        }
        //    }
        //}

        //[Entity(SerializedName = "VATO", SourceDataType = typeof(String), IsMandatory = true)]
        //public DateTime? ValidTo
        //{
        //    get { return _validTo; }
        //    set
        //    {
        //        if (_validTo != value)
        //        {
        //            _validTo = value;
        //            OnPropertyChanged("ValidTo");
        //        }
        //    }
        //}

    }
}
