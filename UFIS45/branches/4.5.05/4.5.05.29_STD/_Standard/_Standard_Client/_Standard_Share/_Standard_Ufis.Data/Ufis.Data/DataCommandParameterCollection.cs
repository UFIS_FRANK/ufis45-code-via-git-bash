﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Ufis.Data
{
    public class DataCommandParameterCollection
    {
        private readonly List<object> _parameters;

        public void Add(object param)
        {
            _parameters.Add(param);
        }

        public void Remove(object param)
        {
            _parameters.Remove(param);
        }

        public void RemoveAt(int index)
        {
            _parameters.RemoveAt(index);
        }

        public void Clear()
        {
            _parameters.Clear();
        }

        public int Count
        {
            get
            {
                return _parameters.Count;
            }
        }
        
        public object this[int index]
        {
            get
            {
            	return _parameters[index];
            }
            set
            {
                _parameters[index] = value;
            }
        }

        public DataCommandParameterCollection()
        {
            _parameters = new List<object>();
        }
    }
}
