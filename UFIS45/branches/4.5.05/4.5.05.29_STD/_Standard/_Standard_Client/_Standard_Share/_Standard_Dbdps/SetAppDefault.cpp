// SetAppDefault.cpp : implementation file

#include <stdafx.h>
#include <DBDPS.h>
#include <Globals.h>

#ifdef ABBCCS_RELEASE
#include <UfisCEDA.h>
#else 
#include <UfisSWH.h>
#endif

//-----------------------------------------------------------------------
// GetStringFromIniFile(pKeyName, pDefaultString, pIniFile)
//	
//	The function gets value of the key pKeyName from ini file INI_FILE 
//  (if no value found, then give the pDefaultString back)
//-----------------------------------------------------------------------
void GetStringFromIniFile(LPCTSTR pKeyName, LPTSTR pDefaultString, LPCTSTR pIniFile)
{
	DWORD ret;
	DWORD size = 0;
	char ReturnedString[LEN_RETURN_VALUE];

	ret = GetPrivateProfileString(
				APP_NAME,
				pKeyName,
				pDefaultString,
				ReturnedString,
				sizeof ReturnedString,
				pIniFile);

	_tcscpy(pDefaultString, ReturnedString);
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//*** 02.12.1999 SHA ***
//*** READ FROM GLOBAL***
void GetStringGlobalFromIniFile(LPCTSTR pKeyName, LPTSTR pDefaultString, LPCTSTR pIniFile)
{
	DWORD ret;
	DWORD size = 0;
	char ReturnedString[LEN_RETURN_VALUE];

	ret = GetPrivateProfileString(
				"GLOBAL",
				pKeyName,
				pDefaultString,
				ReturnedString,
				sizeof ReturnedString,
				pIniFile);

	_tcscpy(pDefaultString, ReturnedString);
}
//-----------------------------------------------------------------------
// LoadIniFile
//	
//	The function gets profile values from ini file
//-----------------------------------------------------------------------
BOOL LoadIniFile()
{
	TCHAR myString[LEN_RETURN_VALUE];

	_tcscpy(myString, DEF_STR);
	GetStringFromIniFile(_T("HOSTNAME"), myString, INI_FILE);	//get the value of HOSTNAME key
	if(_tcscmp(myString, DEF_STR) == 0)	//if the key does not exists
	{
		AfxMessageBox("LoadIniFile(). HOSTNAME not found.", MB_OK + MB_ICONSTOP);
		return FALSE;
	}
	gHostName = myString;	//set the value

	_tcscpy(myString, DEF_STR);
	GetStringFromIniFile(_T("HOSTTYPE"), myString, INI_FILE);	//get the value of HOSTTYPE key
	if(_tcscmp(myString, DEF_STR) == 0)	//if the key does not exists
	{
		AfxMessageBox("LoadIniFile(). HOSTTYPE not found.", MB_OK + MB_ICONSTOP);
		return FALSE;
	}
	gHostType = myString;	//set the value

	_tcscpy(myString, DEF_STR);
	//GetStringFromIniFile(_T("TABLEEXT"), myString, INI_FILE);	//get the value of TABLEEXT key
	//*** 02.12.1999 SHA ***
	GetStringGlobalFromIniFile(_T("TABLEEXTENSION"), myString, INI_FILE);	//get the value of TABLEEXT key
	if(_tcscmp(myString, DEF_STR) == 0)	//if the key does not exists
	{
		AfxMessageBox("LoadIniFile(). TABLEEXT not found.", MB_OK + MB_ICONSTOP);
		return FALSE;
	}
	gTableExt = myString;	//set the value
	
	_tcscpy(myString, DEF_STR);
	//GetStringFromIniFile(_T("HOMEAIRPORT"), myString, INI_FILE);	//get the value of HOMEAIRPORT key
	//*** 02.12.1999 SHA ***
	GetStringGlobalFromIniFile(_T("HOMEAIRPORT"), myString, INI_FILE);	//get the value of HOMEAIRPORT key
	if(_tcscmp(myString, DEF_STR) == 0)	//if the key does not exists
	{
		AfxMessageBox("LoadIniFile(). HOMEAIRPORT not found.", MB_OK + MB_ICONSTOP);
		return FALSE;
	}
	gHomeairport = myString;	//set the value

	//*** 15.11.99 SHA ***
	_tcscpy(myString, DEF_STR);
	GetStringFromIniFile(_T("SYSTABLOG"), myString, INI_FILE);	//get the value of HOMEAIRPORT key
	if(_tcscmp(myString, DEF_STR) == 0)	//if the key does not exists
	{
		//AfxMessageBox("LoadIniFile(). File for SYSTAB-Loging not found.", MB_OK + MB_ICONSTOP);
		//return FALSE;
		//*** DEFAULT FILE FOR THE SYSTAB-LOGING ***
		gSystabLogFile = "C:\\tmp\\DBDPS_LOADSYSTAB.LOG";
	}
	gSystabLogFile = myString;	//set the value

	//*** 16.11.99 SHA ***
	_tcscpy(myString, "#");
	GetStringFromIniFile(_T("TABLE_SETTINGS"), myString, INI_FILE);	//get the value of HOMEAIRPORT key
	//if(_tcscmp(myString, DEF_STR) == 0)	//if the key does not exists
	//{
	//	gSettingsPath = "#";
	//}
	gSettingsPath = myString;	//set the value
	if (gSettingsPath .Right(1)!="\\")
		gSettingsPath = gSettingsPath  +"\\";

	//*** 09.12.1999 SHA ***
	gConfirmUpdate = false;
	_tcscpy(myString, "#");
	GetStringFromIniFile(_T("CONFIRM_UPDATE"), myString, INI_FILE);	//get the value of HOMEAIRPORT key
	if(_tcscmp(myString, "YES") == 0)	//if the key does not exists
	{
		gConfirmUpdate = true;
	}
	
	//*** 20.01.1999 SHA ***
	_tcscpy(myString, "#");
	GetStringFromIniFile(_T("FONT"), myString, INI_FILE);	//get the value of HOMEAIRPORT key
	if(_tcscmp(myString, "#") == 0)	//if the key does not exists
	{
		gStandardFont="";
	}
	else
		gStandardFont = myString;

	//*** 03.12.1999 SHA ***
	strcpy(CCSCedaData::pcmTableExt, gTableExt);
	strcpy(CCSCedaData::pcmHomeAirport, gHomeairport);
	strcpy(CCSCedaData::pcmApplName, APP_NAME);



	return TRUE;
}

//-----------------------------------------------------------------------
// SetAppDefaultValues
//	
//	The function sets application's default values
//-----------------------------------------------------------------------
BOOL SetAppDefaultValues()
{

	if(!LoadIniFile())	//get profile values from ini file
		return FALSE;
#ifdef ABBCCS_RELEASE
	gComHandle.Initialize((LPSTR)(LPCTSTR)gHostName, (LPSTR)(LPCTSTR)gHostType);
#endif

	GetWorkstationName(gWorkstationName);
	return TRUE;
}

