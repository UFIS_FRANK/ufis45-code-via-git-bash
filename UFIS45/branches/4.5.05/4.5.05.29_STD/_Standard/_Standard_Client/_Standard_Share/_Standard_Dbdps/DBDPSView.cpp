// DBDPSView.cpp : implementation of the CDBDPSView class
//
#include <CCSBcHandle.h>

#include <stdafx.h>

#include <MainFrm.h>

#include <DBDPS.h>
#include <DBDPSDoc.h>
#include <DBDPSView.h>
#include <Globals.h>
#include <SelectDlg.h>
#include <FilterSortDlg.h>
#include <WDayChk.h>

#ifdef ABBCCS_RELEASE
#include <CCSCedaCom.h>
#include <CCSDefines.h>
#endif

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDBDPSView

IMPLEMENT_DYNCREATE(CDBDPSView, CGXBrowserView)

BEGIN_MESSAGE_MAP(CDBDPSView, CGXBrowserView)
	//{{AFX_MSG_MAP(CDBDPSView)
	ON_COMMAND_RANGE(ID_VIEW_MIN, ID_VIEW_MAX, OnDoFile)
	ON_COMMAND(ID_VIEW_MORE, OnMore)
	ON_COMMAND(ID_EXPORT, OnExport)
	ON_COMMAND(ID_SORT, OnSort)
	ON_COMMAND(ID_FILTER_SORT, OnFilterSort)
	ON_COMMAND(ID_DELETE_ROW, OnDeleteRow)
	ON_COMMAND(ID_EDIT_PASTEROW, OnEditPasteRow)
	ON_COMMAND(ID_EDIT_PASTEREGION, OnEditPasteRegion)
	ON_COMMAND(ID_EDIT_UNDO_PASTEREGION, OnEditUndoPasteRegion)


	ON_UPDATE_COMMAND_UI(ID_EXPORT, OnUpdateExport)
	ON_UPDATE_COMMAND_UI(ID_FILE_PRINT, OnUpdateFilePrint)
	ON_UPDATE_COMMAND_UI(ID_FILE_PRINT_PREVIEW, OnUpdateFilePrintPreview)
	ON_UPDATE_COMMAND_UI(ID_FILE_PRINT_SETUP, OnUpdateFilePrintSetup)
	ON_UPDATE_COMMAND_UI(ID_SORT, OnUpdateSort)
	ON_UPDATE_COMMAND_UI(ID_FILTER_SORT, OnUpdateFilterSort)
	ON_UPDATE_COMMAND_UI(ID_DELETE_ROW, OnUpdateDeleteRow)
	ON_UPDATE_COMMAND_UI(ID_EDIT_CUT, OnUpdateEditCut)	
	ON_UPDATE_COMMAND_UI(ID_EDIT_COPY, OnUpdateEditCopy)	
	ON_UPDATE_COMMAND_UI(ID_EDIT_PASTE, OnUpdateEditPaste)	
	ON_UPDATE_COMMAND_UI(ID_EDIT_PASTEROW, OnUpdateEditPasteRow)
	ON_UPDATE_COMMAND_UI(ID_EDIT_PASTEREGION, OnUpdateEditPasteRegion)
	ON_UPDATE_COMMAND_UI(ID_EDIT_UNDO_PASTEREGION, OnUpdateEditUndoPasteRegion)

	//}}AFX_MSG_MAP
	// Standard printing commands
	//*** 16.11.99 SHA ***
	ON_COMMAND(ID_SEARCH_DLG, OnFind)
	ON_COMMAND(ID_SAVE_SETTINGS, OnSaveSettings)
	ON_COMMAND(ID_DELETE_SETTINGS, OnDeleteSettings)

	ON_COMMAND(ID_FILE_PRINT, CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, CView::OnFilePrintPreview)
#ifdef ABBCCS_RELEASE
	ON_MESSAGE(WM_BCADD, OnBcMessage)
#endif
END_MESSAGE_MAP()

namespace WDayControlAttributes
{
	static const struct CWDayCheckListComboBox::Attributes* WeekOfDayAttributes = 0;

	void WDayRegister()
	{
		{
			static const int Items = NUM_WEEKDAYS;
			static CStringArray Name;
			Name.SetSize(Items);

			const LCTYPE DayId[] = 
			{
				LOCALE_SABBREVDAYNAME1,
				LOCALE_SABBREVDAYNAME2,
				LOCALE_SABBREVDAYNAME3,
				LOCALE_SABBREVDAYNAME4,
				LOCALE_SABBREVDAYNAME5,
				LOCALE_SABBREVDAYNAME6,
				LOCALE_SABBREVDAYNAME7
			/*
				LOCALE_SDAYNAME1,
				LOCALE_SDAYNAME2,
				LOCALE_SDAYNAME3,
				LOCALE_SDAYNAME4,
				LOCALE_SDAYNAME5,
				LOCALE_SDAYNAME6,
				LOCALE_SDAYNAME7
			*/
			};
				
			TCHAR buff[MAX_VALUE_LENGTH];

			const LCTYPE* pDayID = DayId;
			for( int dCount = 0; dCount < NUM_WEEKDAYS; dCount++)
			{
				if( !GetLocaleInfo(LOCALE_USER_DEFAULT,*pDayID++,buff,sizeof(buff)))
				{
					Name[0]= _T("Mon");
					Name[1]= _T("Tue");
					Name[2]= _T("Wed");
					Name[3]= _T("Thu");
					Name[4]= _T("Fri");
					Name[5]= _T("Sat");
					Name[6]= _T("Sun");
					return;
				}
				//CString txt(buff);
				//txt.AnsiToOem();
				//Name[dCount]= txt;
				Name[dCount]= buff;
			}

			static CStringArray Checked;
			Checked.SetSize(Items);
			Checked[0]= _T("1");
			Checked[1]= _T("2");
			Checked[2]= _T("3");
			Checked[3]= _T("4");
			Checked[4]= _T("5");
			Checked[5]= _T("6");
			Checked[6]= _T("7");
				
			static CStringArray Unchecked; 
			Unchecked.SetSize(Items);
			Unchecked[0]= EMPTY_DAY;
			Unchecked[1]= EMPTY_DAY;
			Unchecked[2]= EMPTY_DAY;
			Unchecked[3]= EMPTY_DAY;
			Unchecked[4]= EMPTY_DAY;
			Unchecked[5]= EMPTY_DAY;
			Unchecked[6]= EMPTY_DAY;

			static CUIntArray VirtKey; 
			VirtKey.SetSize(Items);
			VirtKey[0]= '1';
			VirtKey[1]= '2';
			VirtKey[2]= '3';
			VirtKey[3]= '4';
			VirtKey[4]= '5';
			VirtKey[5]= '6';
			VirtKey[6]= '7';

			static CString Separator;
			static bool SmartSeparator = false;
			static CString DefaultCode;

			WDayControlAttributes::WeekOfDayAttributes = new CWDayCheckListComboBox::Attributes
				(
					Items,
					Name,
					Separator,
					SmartSeparator,
					Checked,
					Unchecked,
					VirtKey,
					DefaultCode
				);
		}
	}
}

/////////////////////////////////////////////////////////////////////////////
// CDBDPSView construction/destruction

CDBDPSView::CDBDPSView()
{
}

CDBDPSView::~CDBDPSView()
{
	delete (void *)WDayControlAttributes::WeekOfDayAttributes;	// frees the wday control's struct

#ifdef ABBCCS_RELEASE
	gComHandle.UnRegisterBcWindow((CWnd *) this);
#endif
}


BOOL CDBDPSView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CGXBrowserView::PreCreateWindow(cs);
}

int CDBDPSView::OnCreate( LPCREATESTRUCT lpCreateStruct )
{
	return CGXBrowserView::OnCreate(lpCreateStruct);
}

/////////////////////////////////////////////////////////////////////////////
// CDBDPSView drawing

void CDBDPSView::OnDraw(CDC* pDC)
{
	CDBDPSDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);

	// Delegate drawing to the base class
	CGXBrowserView::OnDraw(pDC);
}

/////////////////////////////////////////////////////////////////////////////
// CDBDPSView printing

BOOL CDBDPSView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// default preparation
	return DoPreparePrinting(pInfo);
}

void CDBDPSView::OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo)
{
	// TODO: add extra initialization before printing
	((CMainFrame*) AfxGetMainWnd())->ShowDialogBar(SW_HIDE);
	CGXBrowserGrid::OnGridBeginPrinting(pDC, pInfo);
}

void CDBDPSView::OnEndPrinting(CDC* pDC, CPrintInfo* pInfo)
{
	// TODO: add cleanup after printing
	CGXBrowserGrid::OnGridEndPrinting(pDC, pInfo);
	((CMainFrame*) AfxGetMainWnd())->ShowDialogBar(SW_SHOW);
}

/////////////////////////////////////////////////////////////////////////////
// CDBDPSView diagnostics

#ifdef _DEBUG
void CDBDPSView::AssertValid() const
{
	CGXBrowserView::AssertValid();
}

void CDBDPSView::Dump(CDumpContext& dc) const
{
	CGXBrowserView::Dump(dc);
}

CDBDPSDoc* CDBDPSView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CDBDPSDoc)));
	return (CDBDPSDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CDBDPSView message handlers

void CDBDPSView::OnInitialUpdate() 
{

	CGXBrowserView::OnInitialUpdate();

	WDayControlAttributes::WDayRegister();	

	// setting of the print properties
	{
		CGXProperties* myProperties = GetParam()->GetProperties();
		const BOOL doSetProperties = myProperties == NULL;

		if ( doSetProperties)
			myProperties = new CGXProperties;

		ASSERT(myProperties);

		myProperties = GetParam()->GetProperties();
		myProperties->SetCenterHorizontal(FALSE);
		myProperties->SetCenterVertical(FALSE);

		//------------------------------------- 

		if( doSetProperties)
			GetParam()->SetProperties(myProperties);
	}

	//*** 15.11.99 SHA ***
	GetParam()->SetSortRowsOnDblClk(TRUE);

	OnQueryChange();
#ifdef ABBCCS_RELEASE
	gComHandle.RegisterBcWindow((CWnd *) this);
#endif
}


// Supply the record count to the grid...
long CDBDPSView::OnGetRecordCount()
{
	return gGRTable.GetSize();
}

// Supply the appropriate value to the grid...
BOOL CDBDPSView::GetStyleRowCol(ROWCOL nRow, ROWCOL nCol, CGXStyle & style, GXModifyType mt, int nType)
{
	BOOL bRet = CGXBrowserView::GetStyleRowCol(nRow, nCol, style, mt, nType);
	if(nType >= 0)
	{
		if(nCol != 0)
		{
			if(nRow == 0)
			{
				CString msg(gCurrentFieldsDesc->GetAt(MapCol[nCol])->mToolTip);
				style.SetUserAttribute(GX_IDS_UA_TOOLTIPTEXT, msg);
			}
		}
		if (nRow > 0 && nCol > 0)
		{
			{
				ROWCOL nCurRow, nCurCol;
				static ROWCOL nPrevRow = 0, nPrevCol = 0;

				if (GetCurrentCell(nCurRow, nCurCol))
				{
					if( nCurRow != nPrevRow)
						RedrawRowCol( nPrevRow, nPrevCol, GX_INVALIDATE);

					nPrevRow = nCurRow;
					nPrevCol = nCurCol;
				}
			}
			
			static UINT myPreviousMode = noMode;
			CGXBrowseParam* pBrowseData = GetBrowseParam();

			if (pBrowseData->m_nEditMode != myPreviousMode &&
					pBrowseData->m_nEditMode != noMode)
				RedrawRowCol(CGXRange().SetRows(pBrowseData->m_nCurrentRow), GX_INVALIDATE);

			myPreviousMode = pBrowseData->m_nEditMode;

			if (pBrowseData->m_nEditMode != noMode &&
					gCurrentFieldsDesc->GetAt(MapCol[nCol])->mRequired &&
					pBrowseData->m_nCurrentRow == nRow)
				GetDocument()->SetStyleColor(style, CL_YELLOW);
			else
				if ((int)nRow > gGRTable.GetSize())
					GetDocument()->SetStyleColor(style, CL_STANDART);	// new row - do nothing
				else
				{
					int RealRow = *GetDocument()->mIndexMap.GetAt(nRow - 1); // get real row
					int myCellState = gGRTable.GetAt(RealRow)->GetAt(MapCol[nCol])->mCellState;
					if (myCellState & CELL_DB_INVALID)
						GetDocument()->SetStyleColor(style, CL_LIGHT_RED);	// the data from database is invalid
					else 
						GetDocument()->SetStyleColor(style, CL_STANDART);	// tha data is OK
				}
			if (pBrowseData->m_paDirtyFieldStruct[nCol - 1].bDirty)
				if ((pBrowseData->m_nCurrentRow == nRow))
					GetDocument()->CheckValueSetColor(MapCol[nCol], pBrowseData->m_paDirtyFieldStruct[nCol - 1].sValue, style);
		}
	}
	return bRet;
}

// Store the changes made in the grid into your external data source...
BOOL CDBDPSView::StoreStyleRowCol(
							ROWCOL nRow, ROWCOL nCol, 
							const CGXStyle *pStyle, GXModifyType mt, int nType)
{
	BOOL bRval;
	if (nRow > 0 && nCol > 0)
	{
		CGXStyle myStyle = *pStyle;
		if (GetDocument()->CheckValueSetValue(MapCol[nCol], pStyle->GetValue(), myStyle))
			bRval = CGXBrowserView::StoreStyleRowCol(nRow, nCol, &myStyle, mt, nType);
		else
			bRval = CGXBrowserView::StoreStyleRowCol(nRow, nCol, pStyle, mt, nType);
	}
	else
		bRval = CGXBrowserView::StoreStyleRowCol(nRow, nCol, pStyle, mt, nType);
	
	if (nType == -1)
		return bRval;
	else if((nType >= 0) && pStyle)
	{
		if (nRow == 0 || nCol == 0 || !pStyle->GetIncludeValue()) return false;
		return true;		
	}
	return false;
}


//////////////////////////////////////////////////////////////////////////////

// Supply the value to the cells on demand...
BOOL CDBDPSView::OnLoadCellStyle(ROWCOL nRow, ROWCOL nCol, 
											  CGXStyle &style, LPCTSTR pszExistingValue)
{
	int nField = nCol - 1;

	if (pszExistingValue == NULL && nRow >= GetFirstRow())
	{
		if ((!m_bNoValueNeeded) && (nField != -1) && (nField <= (short)GetColCount()))
		{
			if ((nRow  > 0) && (nCol > 0)) GetDocument()->GetGRCellValue(nRow - 1, MapCol[nCol], style);
		}
		else style.SetEnabled(FALSE);
	}
	return TRUE;
}

// Store the changes made by the user back into the datasource...
void CDBDPSView::OnFlushCellValue(ROWCOL nRow, 
												ROWCOL nCol, LPCTSTR pszChangedValue)
{
	// cannot handle this.
	if (pszChangedValue == NULL)
		return;

	int nField = nCol - 1;
	if (nField < 0 || 
		nField > (short)gCurrentFieldsDesc->GetUpperBound())
	{
		// This case will happen if new unbound	columns were inserted, 
		// e.g. a sum based on other fields in the row.

		// Possible solution to avoid this message is to set
		// the column read-only or static or override this method.

		SetWarningText(_T("You have tried to modify the value of an unbound field!"));
		AfxThrowNotSupportedException();
		return;
	}
	CString myValue;
	if (!GetDocument()->ValidGRValue(MapCol[nCol], pszChangedValue, myValue))
	{
		SetWarningText(_T("There is still at least one invalid field in the record."));
		AfxThrowNotSupportedException();
	}
	else
	{
		CGRCell *myGRCell = GetDocument()->mChangingRecord.GetAt(MapCol[nCol]);
		GetDocument()->TransGRToCell(MapCol[nCol], pszChangedValue, myGRCell->mStrValue, myGRCell->mNumValue);
		myGRCell->mCellState = CELL_UPDATED;
	}
}

short CDBDPSView::GetFieldFromCol(ROWCOL nCol)
{
	return (short) (GetColIndex(nCol) - GetHeaderCols());
}

long CDBDPSView::GetRecordFromRow(ROWCOL nRow)
{
	return (long) GetRowIndex(nRow) - (long) GetHeaderRows();
}


BOOL CDBDPSView::OnFlushRecord(ROWCOL nRow, CString * ps)
{
	GetDocument()->ClearChangingRecord();
	switch (GetBrowseParam()->m_nEditMode)
	{
		case addnew:
		{
			if (CGXBrowserView::OnFlushRecord(nRow, ps))
			{
				if (!GetDocument()->InsertRecord())
				{
					//SetWarningText(_T("Insert failed."));
					//AfxThrowNotSupportedException();
					return false;
				}
				return true;
			}
			break;
		}
		case edit:
			if (CGXBrowserView::OnFlushRecord(nRow, ps))
			{
				if (!GetDocument()->UpdateRecord(nRow - 1))
				{
					//SetWarningText(_T("Update failed."));
					//*** 03.12.1999 SHA ***
					//AfxThrowNotSupportedException();
					return false;
				}
				return true;
			}
			break;
	}
	return false;
}


// custom comapare function
extern "C" static int __cdecl __gxcomparelong(const void* p1, const void* p2)
{
	long l = *(long*) p1 - *(long*) p2;

	return (l>0) ? 1 : (l<0) ? -1 : 0;
}


void CDBDPSView::DeleteRows(const CRowColArray & awRowStart, const CRowColArray & awRowEnd)
{
	// This function takes care of deleting multiple noncontiguous rows by 
	// deleting rows from bottom up.
	// It also adjusts the m_awRowIndex array which is used to map the current row to the 
	// original row before sorting/moving occured.

	if (!gCanMainDeleteRow || !gCanTableDeleteRow)
	{
		MessageBeep(-1);
		return;
	}
	
	CGXBrowseParam* pBrowseData = GetBrowseParam();

	BOOL bUndoWasEnabled = GetParam()->IsEnableUndo();
	GetParam()->EnableUndo(FALSE);
	BOOL bLock = LockUpdate(TRUE);

	// Temporary array containing the records to be deleted
	long* alRecords = NULL;

	// First, sort the array of records
	ROWCOL nCount = 0;
	for (int index = awRowEnd.GetUpperBound(); index >= 0; index--)
		nCount += awRowEnd[index] - awRowStart[index] + 1;

	if(nCount > 1)
		MessageBeep(-1);
	else
	{
		int nIndex = 0;
		alRecords = new long[nCount];

		// Fill alRecords
		for (index = awRowEnd.GetUpperBound(); index >= 0; index--)
		{
			ROWCOL nRecords = awRowEnd[index]-awRowStart[index]+1;
			for (ROWCOL n = 0; n < nRecords; n++)
				alRecords[nIndex++] = GetRecordFromRow(awRowStart[index]+n);
		}

		// Sort the records to be deleted
		qsort(alRecords, (size_t) nCount, sizeof(long), __gxcomparelong);


		// Start deleting rows one at a time from bottom to top
		for (index = (int) nCount-1; index >= 0; index--)
		{
			long lRecord = alRecords[index];
			//GetDocument()->m_data.StoreRemoveRows(lRecord, lRecord);
			GetDocument()->DeleteRecord(lRecord - 1);

			// Decrease the row index of all those rows that are below this deleted row
			for (int n = 0; n <= pBrowseData->m_awRowIndex.GetUpperBound(); n++)
			{
				if (pBrowseData->m_awRowIndex[n] > alRecords[index]+GetHeaderRows())
					pBrowseData->m_awRowIndex[n]--;
			}
		}

		// Deleted the unwanted entries in m_awRowIndex
		for (index = awRowEnd.GetUpperBound(); index >= 0; index--)
		{
			if ((int) awRowStart[index] < pBrowseData->m_awRowIndex.GetSize())
				pBrowseData->m_awRowIndex.RemoveAt((int) awRowStart[index],
					min((int) awRowEnd[index], pBrowseData->m_awRowIndex.GetUpperBound())
					-(int) awRowStart[index]+1);
		}

		// Remove selection
		SetSelection(0);

		// Let CGXGridCore adjust hidden rows, row heights,
		// floating and covered cells, row styles.

		for (int i = awRowEnd.GetUpperBound(); i >= 0; i--)
			CGXGridCore::StoreRemoveRows(awRowStart[i],awRowEnd[i]);

		delete alRecords;	// free the memory
	}
	GetParam()->EnableUndo(bUndoWasEnabled);
	LockUpdate(bLock);

	if(nCount > 1)
		return;

	Redraw();
}

BOOL CDBDPSView::OnDeleteCell(ROWCOL nRow, ROWCOL nCol)
{
	CRowColArray Array;

	if((GetSelectedCols(Array) == 1) && (GetSelectedRows(Array) == 1))
		return TRUE;
	MessageBeep(-1);
	return FALSE;
}

//-----------------------------------------------------------------------
// OnExport
//	
//	The function exports the actual grid data into tab-delimited file with
// extension .txt
//-----------------------------------------------------------------------
// CHANGES:
//-----------------------------------------------------------------------
//	DATE       | AUTHOR            | DESCRIPTION
//-----------------------------------------------------------------------
// 19.07.1999 | Martin Danihel    | default extension 'xls'
//-----------------------------------------------------------------------
void CDBDPSView::OnExport() 
{
	// prepare filter
	char strbuf[] = _T("Worksheet Files (*.xls)|*.xls|Text Files (*.txt)|*.txt|All Files (*.*)|*.*||");
	CString defExtension = _T("xls");
	CString finalPath;
	CString extension;
	CFile *file = NULL;
	CDBDPSDoc *pDoc = GetDocument();
	ASSERT_VALID(pDoc);

	CFileDialog dlgFile(FALSE, LPCTSTR(defExtension), NULL, OFN_HIDEREADONLY|OFN_OVERWRITEPROMPT, strbuf);

	// prepare file dialog box
	(dlgFile.m_ofn).lpstrTitle = _T("Export to tab-delimited text file");
	(dlgFile.m_ofn).Flags |= OFN_CREATEPROMPT | OFN_PATHMUSTEXIST;

	if(dlgFile.DoModal() == IDCANCEL)
		return;

	finalPath = _T(dlgFile.GetPathName());
 
	extension = _T(dlgFile.GetFileExt());
	if((extension != defExtension) && (extension != _T("txt")))
	{
		int position;
		if((position=finalPath.ReverseFind('\\'))!=-1)
		{
			finalPath=finalPath.Left(position+1);
			finalPath+=_T(dlgFile.GetFileTitle()+'.'+defExtension);
		}   // if((position=finalPath.ReverseFind('\\'))!=-1)
	}   // if(extension!=defExtension)

	TRY
	{
		// create new file
		file=new CFile(LPCTSTR(finalPath),CFile::modeCreate | CFile::modeWrite);
  
		if(file!=NULL)
		{
			// Write some useful structures
			CopyTextToFile(*file, CGXRange(0, 1, GetRowCount(), GetColCount()));
			pDoc->ReleaseFile(file, FALSE);    // close file and release memory
		}   // if(file!=NULL)
	}   // end TRY

	CATCH(CFileException, e)
	{
		if(file)
		{
			pDoc->ReleaseFile(file,TRUE); // causes abort with no exceptions
			file=NULL;
		}
    
		#ifdef _DEBUG
			afxDump << "File could not be opened " << e->m_cause << "\n";
		#endif

		e->ReportError();
	}

	CATCH(CMemoryException, e)
	{
		file = NULL;

		#ifdef _DEBUG
			e->ReportError();
		#endif
	}
    
	END_CATCH        
}

//-----------------------------------------------------------------------
// OnFilterSort
//	
//	The function shows the FilterSort dialog with actual filter and sort
// clause
//-----------------------------------------------------------------------
// CHANGES:
//-----------------------------------------------------------------------
//	DATE       | AUTHOR            | DESCRIPTION
//-----------------------------------------------------------------------
// 02.05.1999 | Martin Danihel    | Saved views adjustment
//-----------------------------------------------------------------------
void CDBDPSView::OnFilterSort() 
{
	gStyleBuffer.ClearUndo();

	BOOL myChange = FALSE;	// changed flag
	CFilterSortDlg filterDlg;
	filterDlg.m_TableName = gTableName;
	filterDlg.m_FilterClause = gWhereClause.Right(gWhereClause.GetLength() - 
												((CString) WHERE_STRING).GetLength());	// default filter clause
	filterDlg.m_SortClause = gSortClause;	// default sort clause
	filterDlg.m_ViewUrno = ((CMainFrame*) AfxGetMainWnd())->GetUrnoFromSelectedView();
	filterDlg.m_ShowView = TRUE;	// show the view with urno m_ViewUrno in view combo box

	if(filterDlg.DoModal() == IDOK)
	{
		if(filterDlg.m_FilterClause != gWhereClause)	// filter clause is changed
		{
			myChange = TRUE;

			if (!GetDocument()->FillDataStructure(gTableName, gTableExt, filterDlg.m_FilterClause))
			{
				((CMainFrame*) AfxGetMainWnd())->RefreshCombo();
				((CMainFrame*) AfxGetMainWnd())->SetSavedViewSelection(_T(""));	// no selection
				return;
			}
		}

		if(filterDlg.m_SortClause != gSortClause)	// sort clause is changed
		{
			myChange = TRUE;
			ClearSortedFields();
			if(!ConvertFieldsToSortIndex(gTableName, filterDlg.m_SortClause))
			{
				AfxMessageBox("OnFilterSort(). Table cannot be sorted.", MB_OK + MB_ICONSTOP);
				((CMainFrame*) AfxGetMainWnd())->RefreshCombo();
				((CMainFrame*) AfxGetMainWnd())->SetSavedViewSelection(filterDlg.m_ViewUrno);
				return;
			}
			else
				GetDocument()->DoSort();
		}
		if(myChange)
			OnQueryChange();
	}

	((CMainFrame*) AfxGetMainWnd())->RefreshCombo();
	((CMainFrame*) AfxGetMainWnd())->SetSavedViewSelection(filterDlg.m_ViewUrno);
}

//-----------------------------------------------------------------------
// OnSort
//	
//	The function sorts the actual grid data
//-----------------------------------------------------------------------
void CDBDPSView::OnSort() 
{
	gStyleBuffer.ClearUndo();
	GetDocument()->DoSort();
	Redraw();
}

//-----------------------------------------------------------------------
// OnDeleteRow
//	
//	The function deletes the current row
//-----------------------------------------------------------------------
void CDBDPSView::OnDeleteRow() 
{
	CRowColArray awRows;
	ROWCOL myRowCount = GetSelectedRows(awRows);
	if(myRowCount > 1)
	{
		MessageBeep(-1);
		return;
	}

	ROWCOL nRow, nCol;
	if (!GetCurrentCell(nRow,nCol)) 
		return;
	if (CanAppend())
	{
		if (nRow < 0 || nRow >= GetRowCount()) 
			return;
	}
	else
		if (nRow < 0 || nRow > GetRowCount()) 
			return;

	GetDocument()->DeleteRecord(nRow - 1);
	Redraw();	
}

//-----------------------------------------------------------------------
// OnEditPasteRow()
//	
//	The function covers the row pasting
//-----------------------------------------------------------------------
void CDBDPSView::OnEditPasteRow() 
{	
	gStyleBuffer.ClearUndo();

	ROWCOL myCurRow, myCurCol;
	GetCurrentCell(myCurRow, myCurCol);

	int myUndoCol = 0;
	for (ROWCOL myCol = 1; myCol <= GetColCount(); myCol++)
	{
		CFieldInfo *myFieldInfo = gCurrentFieldsDesc->GetAt(MapCol[myCol]);
		if (myFieldInfo->mReadOnly)
			continue;
		SetStyleRange(CGXRange(myCurRow, myCol), *gStyleBuffer.mStyles.GetAt(myUndoCol++));
	}
}

//-----------------------------------------------------------------------
// OnEditPasteRegion()
//	
//	The function covers the region (column) pasting
//-----------------------------------------------------------------------
void CDBDPSView::OnEditPasteRegion() 
{
	gStyleBuffer.ClearUndo();

	CRowColArray awRows;
	ROWCOL myRowCount = GetSelectedRows(awRows);
	for (int i = 0; i < awRows.GetSize(); i++)
	{
		ROWCOL myRow = awRows.GetAt(i);
		if (myRow == 0) 
			continue;
		CGXStyle myStyle;
		GetDocument()->GetGRCellValue(myRow - 1, MapCol[gStyleBuffer.m_ColNum], myStyle);

		SetCurrentCell(myRow, gStyleBuffer.m_ColNum);		
		gStyleBuffer.mNowUndoUpdate = true;
		SetStyleRange(CGXRange(myRow, gStyleBuffer.m_ColNum), *gStyleBuffer.mStyles.GetAt(0));
		Update();

		CGXStyle *myStylePointer = new CGXStyle(myStyle);
		gStyleBuffer.mUndoStyles.Add(myStylePointer);
		gStyleBuffer.mUndoRows.Add(awRows.GetAt(i));
		gStyleBuffer.mCanUndoPastRegion = true;
	}
}

//-----------------------------------------------------------------------
// OnEditUndoPasteRegion()
//	
//	The function undoes the region (column) pasting
//-----------------------------------------------------------------------
void CDBDPSView::OnEditUndoPasteRegion() 
{
	gStyleBuffer.mIsInUndoTransaction = true;

	for (int i = 0; i < gStyleBuffer.mUndoRows.GetSize(); i++)
	{
		ROWCOL myRow = gStyleBuffer.mUndoRows.GetAt(i);
		SetCurrentCell(myRow, gStyleBuffer.m_ColNum);		
		gStyleBuffer.mNowUndoUpdate = true;
		SetStyleRange(CGXRange(myRow, gStyleBuffer.m_ColNum), *gStyleBuffer.mUndoStyles.GetAt(i));
		Update();
	}
	gStyleBuffer.ClearUndo();
}

//-----------------------------------------------------------------------
// EnableMenuItem
//	
//	The function enabled menu item if some table is load, otherwise
// disabled it
//-----------------------------------------------------------------------
void CDBDPSView::EnableMenuItem(CCmdUI* pCmdUI)
{
	CMenu *pMenu = AfxGetMainWnd()->GetMenu();		// get application menu

	if(pMenu!=NULL)
		if(gTableLoad && (GetBrowseParam()->m_nEditMode == noMode))
			pCmdUI->Enable(TRUE);
		else
			pCmdUI->Enable(FALSE);
}

//-----------------------------------------------------------------------
// EnableSec2MenuItem
//	
//	The function enabled menu item on security status (for modules with
// two-level security (general and table level security))
//-----------------------------------------------------------------------
void CDBDPSView::EnableSec2MenuItem
	(
		CCmdUI* pCmdUI,
		LPCTSTR pObjectId,
		LPCTSTR pControlId
	)
{
	CString myStatus;

	myStatus = GetSecurityStatus(pObjectId, pControlId);
	if(myStatus == SEC_STAT_ACT)
	{
		if(gTableLoad)
		{
			myStatus = GetSecurityStatus(gTableName + gTableExt, pControlId);
			if(myStatus == SEC_STAT_ACT)
			{
				EnableMenuItem(pCmdUI);
				return;
			}
		}
		else
		{
			EnableMenuItem(pCmdUI);
			return;
		}
	}

	CMenu *pMenu = AfxGetMainWnd()->GetMenu();		// get application menu
	if(pMenu != NULL)
		pCmdUI->Enable(FALSE);	// disable menu item
}

// message handlers

// standart message handler
void CDBDPSView::OnUpdateExport(CCmdUI* pCmdUI) 
{
	EnableSec2MenuItem(pCmdUI, _T("MAIN"), _T("FILE_EXPORT"));
}

// standart message handler
//void CDBDPSView::FilePrint(CCmdUI* pCmdUI) 
void CDBDPSView::OnUpdateFilePrint(CCmdUI* pCmdUI) 
{
	EnableSec2MenuItem(pCmdUI, _T("MAIN"), _T("FILE_PRINT"));
}

// standart message handler
void CDBDPSView::OnUpdateFilePrintPreview(CCmdUI* pCmdUI) 
{
	CString myStatus = GetSecurityStatus(_T("MAIN"), _T("FILE_PRINT_PREVIEW"));
	if(myStatus == SEC_STAT_ACT)
	{
		EnableMenuItem(pCmdUI);
		return;
	}

	CMenu *pMenu = AfxGetMainWnd()->GetMenu();		// get application menu
	if(pMenu != NULL)
		pCmdUI->Enable(FALSE);	// disable menu item
}

// standart message handler
void CDBDPSView::OnUpdateFilePrintSetup(CCmdUI* pCmdUI) 
{
	CMenu *pMenu = AfxGetMainWnd()->GetMenu();		// get application menu
	if(pMenu != NULL)
	{
		CString myStatus = GetSecurityStatus(_T("MAIN"), _T("FILE_PAGE_SETUP"));
		if(myStatus == SEC_STAT_ACT)
			pCmdUI->Enable(TRUE);	// enable menu item
		else
			pCmdUI->Enable(FALSE);	// disable menu item
	}
}

// standart message handler
void CDBDPSView::OnUpdateFilterSort(CCmdUI* pCmdUI)
{
	EnableSec2MenuItem(pCmdUI, _T("MAIN"), _T("VIEW_FILTER_SORT_DLG"));
}

// standart message handler
void CDBDPSView::OnUpdateSort(CCmdUI* pCmdUI) 
{
	EnableSec2MenuItem(pCmdUI, _T("MAIN"), _T("VIEW_SORT_UPDATE"));
}

// standart message handler
void CDBDPSView::OnUpdateDeleteRow(CCmdUI* pCmdUI) 
{
	EnableSec2MenuItem(pCmdUI, _T("MAIN"), _T("DELETE_ROW"));
}

// standart message handler
void CDBDPSView::OnUpdateEditCut(CCmdUI* pCmdUI)
{
	CString myStatus = GetSecurityStatus(_T("MAIN"), _T("EDIT_CUT"));
	if(myStatus == SEC_STAT_ACT && CanCut())
		pCmdUI->Enable(TRUE);
	else
		pCmdUI->Enable(FALSE);
}

// standart message handler
void CDBDPSView::OnUpdateEditCopy(CCmdUI* pCmdUI)
{
	CString myStatus = GetSecurityStatus(_T("MAIN"), _T("EDIT_COPY"));
	if(myStatus == SEC_STAT_ACT && CanCopy())
		pCmdUI->Enable(TRUE);
	else
		pCmdUI->Enable(FALSE);
}

// standart message handler
void CDBDPSView::OnUpdateEditPaste(CCmdUI* pCmdUI)
{
	CString myStatus = GetSecurityStatus(_T("MAIN"), _T("EDIT_PASTE"));
	if(myStatus == SEC_STAT_ACT && CanPaste())
		pCmdUI->Enable(TRUE);
	else
		pCmdUI->Enable(FALSE);
}

// standart message handler
void CDBDPSView::OnUpdateEditPasteRow(CCmdUI* pCmdUI)
{
	CString myStatus = GetSecurityStatus(_T("MAIN"), _T("EDIT_PASTE_ROW"));
	if(myStatus == SEC_STAT_ACT)
	{
		if (!gStyleBuffer.mCanPastRow)
			pCmdUI->Enable(FALSE);
		else 
		{
			CRowColArray myRows;
			if (GetSelectedRows(myRows) > 1)
				pCmdUI->Enable(FALSE);
			else 
				EnableMenuItem(pCmdUI);
		}
	}
	else
		pCmdUI->Enable(FALSE);
}

// standart message handler
void CDBDPSView::OnUpdateEditPasteRegion(CCmdUI* pCmdUI)
{
	CString myStatus = GetSecurityStatus(_T("MAIN"), _T("EDIT_PASTE_REGION"));
	if (myStatus != SEC_STAT_ACT)
	{
		pCmdUI->Enable(FALSE);
		return;
	}

	ROWCOL myCurRow, myCurCol;
	GetCurrentCell(myCurRow, myCurCol);

	if (!gStyleBuffer.mCanPastRegion)
		pCmdUI->Enable(FALSE);
	else if (myCurCol != gStyleBuffer.m_ColNum)		// only to the same column
		pCmdUI->Enable(FALSE);
	else 
	{
		CRowColArray myCols;
		if (GetSelectedCols(myCols) > 1)
			pCmdUI->Enable(FALSE);
		else 
			EnableMenuItem(pCmdUI);
	}
}

// standart message handler
void CDBDPSView::OnUpdateEditUndoPasteRegion(CCmdUI* pCmdUI)
{
	if (gStyleBuffer.mIsInUndoTransaction)
		gStyleBuffer.ClearUndo();

	ROWCOL myCurRow, myCurCol;
	GetCurrentCell(myCurRow, myCurCol);

	if (!gStyleBuffer.mCanUndoPastRegion)
		pCmdUI->Enable(FALSE);
	else 
		EnableMenuItem(pCmdUI);
}

//-----------------------------------------------------------------------
// OnDoFile
//	
//	The message handler of selecting some table in menu item Open
//-----------------------------------------------------------------------
// CHANGES:
//-----------------------------------------------------------------------
//	DATE       | AUTHOR            | DESCRIPTION
//-----------------------------------------------------------------------
// 09.06.1999 | Martin Danihel    | Added security functionality
//-----------------------------------------------------------------------
afx_msg void CDBDPSView::OnDoFile( UINT nID )
{
	gStyleBuffer.ClearUndo();

	if(gTableLoad)
		if(GetBrowseParam()->m_nEditMode != noMode)
		{
			AfxMessageBox("OnDoFile(). Current table is not in valid state. First validate current record.", MB_OK + MB_ICONSTOP);
			return;
		}
	
	// locate the open submenu
	CMenu* pOpenSubmenu = NULL;
	CMenu* pTopMenu = AfxGetMainWnd()->GetMenu();
	CMenu* pMenu;
	int iPos;
	for (iPos = pTopMenu->GetMenuItemCount() - 1; iPos >= 0; iPos--)
	{
		pMenu = pTopMenu->GetSubMenu(iPos);
		if (pMenu && pMenu->GetMenuItemID(0) == -1)	//-1 = submenu
		{
			CMenu* pSubMenu = pMenu->GetSubMenu(0);
			if (pSubMenu && pSubMenu->GetMenuItemID(0) == ID_VIEW_MIN)
			{
				pOpenSubmenu = pSubMenu;
				break;
			}
		}
	}
	if(pOpenSubmenu == NULL)
	{
		AfxMessageBox("OnDoFile(). Submenu not found.", MB_OK + MB_ICONSTOP);
		return;
	}

	CString mySelectLongTableName;
	pOpenSubmenu->GetMenuString(nID, mySelectLongTableName, MF_BYCOMMAND);

	BOOL myFound = FALSE;
	CString mySelectTable;
	CString myTableName;
	CString *myLongTableName;
	for(int j = 0; j < gTables.GetSize(); j++)
	{
		myTableName = gTables.GetAt(j);
		if (!gLongTableNames.Lookup(myTableName + gTableExt, myLongTableName))	// if a long name exists set this
		{
			AfxMessageBox("OnOK(). Long table name not found.", MB_OK + MB_ICONSTOP);
			return;
		}
		if(*myLongTableName == mySelectLongTableName)
		{
			mySelectTable = myTableName;
			myFound = TRUE;
		}
		if(myFound) break;
	}

	CString myStatus;	// security status

	myStatus = GetSecurityStatus(mySelectTable + gTableExt, _T("VIEW_FILTER_SORT_DLG"));
	if(myStatus != SEC_STAT_ACT)	// set view 'default'
	{
		CSavedViews mySavedViews;

		LoadSavedViews(mySelectTable, &mySavedViews, TRUE);	// load saved views with 'default' view
		for(int i = 0; i < mySavedViews.GetSize(); i++)
		{
			CString UrnoText;
			CString ViewName;
			CString TableName;
			CString tmpFilterText;
			CString FilterText;
			CString SortText;

			mySavedViews.GetAt(i, UrnoText, ViewName, TableName, tmpFilterText, SortText);
			if(ViewName == DEF_VIEW_NAME)	// compare the view names
			{
				if(tmpFilterText.GetLength() > 0)
					FilterText = FilterText + WHERE_STRING + tmpFilterText;

				if (!GetDocument()->FillDataStructure(mySelectTable, gTableExt, FilterText))
				{
					((CMainFrame*) AfxGetMainWnd())->RefreshCombo();
					((CMainFrame*) AfxGetMainWnd())->SetSavedViewSelection(_T(""));	// no selection
					return;
				}

				ClearSortedFields();
				if(!ConvertFieldsToSortIndex(mySelectTable, SortText))
				{
					AfxMessageBox("OnDoFile(). Table cannot be sorted.", MB_OK + MB_ICONSTOP);
					((CMainFrame*) AfxGetMainWnd())->RefreshCombo();
					((CMainFrame*) AfxGetMainWnd())->SetSavedViewSelection(UrnoText);
					return;
				}
				else
					GetDocument()->DoSort();

				OnQueryChange();
				((CMainFrame*) AfxGetMainWnd())->RefreshCombo();
				((CMainFrame*) AfxGetMainWnd())->SetSavedViewSelection(UrnoText);
			}
		}
	}
	else
	{
		CFilterSortDlg filterDlg;
		filterDlg.m_TableName = mySelectTable;
		filterDlg.m_FilterClause.Empty();	// no default filter clause
		filterDlg.m_SortClause.Empty();	// no default sort clause
		filterDlg.m_ViewUrno = ((CMainFrame*) AfxGetMainWnd())->GetUrnoFromSelectedView();
		filterDlg.m_ShowView = FALSE;	// do not show the view with urno m_ViewUrno in view combo box

		if(filterDlg.DoModal() == IDOK)
		{
			if (!GetDocument()->FillDataStructure(mySelectTable, gTableExt, filterDlg.m_FilterClause))
			{
				((CMainFrame*) AfxGetMainWnd())->RefreshCombo();
				((CMainFrame*) AfxGetMainWnd())->SetSavedViewSelection(_T(""));	// no selection
				return;
			}

			ClearSortedFields();
			if(!ConvertFieldsToSortIndex(mySelectTable, filterDlg.m_SortClause))
			{
				AfxMessageBox("OnDoFile(). Table cannot be sorted.", MB_OK + MB_ICONSTOP);
				((CMainFrame*) AfxGetMainWnd())->RefreshCombo();
				((CMainFrame*) AfxGetMainWnd())->SetSavedViewSelection(filterDlg.m_ViewUrno);
				return;
			}
			else
				GetDocument()->DoSort();

			OnQueryChange();
		}
		((CMainFrame*) AfxGetMainWnd())->RefreshCombo();
		((CMainFrame*) AfxGetMainWnd())->SetSavedViewSelection(filterDlg.m_ViewUrno);
	}
}

//-----------------------------------------------------------------------
// OnMore
//	
//	The message handler of selecting Weitere... in menu item Open
//-----------------------------------------------------------------------
afx_msg void CDBDPSView::OnMore()
{
	gStyleBuffer.ClearUndo();

	CString myTable;
	CSelectDlg selectDlg;

	if (selectDlg.DoModal() == IDOK)
	{
		if (!selectDlg.m_myChoose.IsEmpty())
		{
			if(gTableLoad)
				if(GetBrowseParam()->m_nEditMode != noMode)
				{
					AfxMessageBox("OnMore(). Current table is not in valid state. First validate current record.", MB_OK + MB_ICONSTOP);
					return;
				}

			myTable = selectDlg.m_myChoose;
			CString myStatus;	// security status

			myStatus = GetSecurityStatus(myTable + gTableExt, _T("VIEW_FILTER_SORT_DLG"));
			if(myStatus != SEC_STAT_ACT)	// set view 'default'
			{
				CSavedViews mySavedViews;

				LoadSavedViews(myTable, &mySavedViews, TRUE);	// load saved views with 'default' view
				for(int i = 0; i < mySavedViews.GetSize(); i++)
				{
					CString UrnoText;
					CString ViewName;
					CString TableName;
					CString tmpFilterText;
					CString FilterText;
					CString SortText;

					mySavedViews.GetAt(i, UrnoText, ViewName, TableName, tmpFilterText, SortText);
					if(ViewName == DEF_VIEW_NAME)	// compare the view names
					{
						if(tmpFilterText.GetLength() > 0)
							FilterText = FilterText + WHERE_STRING + tmpFilterText;

						if (!GetDocument()->FillDataStructure(myTable, gTableExt, FilterText))
						{
							((CMainFrame*) AfxGetMainWnd())->RefreshCombo();
							((CMainFrame*) AfxGetMainWnd())->SetSavedViewSelection(_T(""));	// no selection
							return;
						}

						ClearSortedFields();
						if(!ConvertFieldsToSortIndex(myTable, SortText))
						{
							AfxMessageBox("OnDoFile(). Table cannot be sorted.", MB_OK + MB_ICONSTOP);
							((CMainFrame*) AfxGetMainWnd())->RefreshCombo();
							((CMainFrame*) AfxGetMainWnd())->SetSavedViewSelection(UrnoText);
							return;
						}
						else
							GetDocument()->DoSort();

						OnQueryChange();
						((CMainFrame*) AfxGetMainWnd())->RefreshCombo();
						((CMainFrame*) AfxGetMainWnd())->SetSavedViewSelection(UrnoText);
					}
				}
			}
			else
			{
				CFilterSortDlg filterDlg;
				filterDlg.m_TableName = myTable;
				filterDlg.m_FilterClause.Empty();	// no default filter clause
				filterDlg.m_SortClause.Empty();	// no default sort clause
				filterDlg.m_ViewUrno = ((CMainFrame*) AfxGetMainWnd())->GetUrnoFromSelectedView();
				filterDlg.m_ShowView = FALSE;	// do not show the view with urno m_ViewUrno in view combo box

				if(filterDlg.DoModal() == IDOK)
				{
					if (!GetDocument()->FillDataStructure(myTable, gTableExt, filterDlg.m_FilterClause))
					{
						((CMainFrame*) AfxGetMainWnd())->RefreshCombo();
						((CMainFrame*) AfxGetMainWnd())->SetSavedViewSelection(_T(""));	// no selection
						return;
					}

					ClearSortedFields();
					if(!ConvertFieldsToSortIndex(myTable, filterDlg.m_SortClause))
					{
						AfxMessageBox("OnMore(). Table cannot be sorted.", MB_OK + MB_ICONSTOP);
						((CMainFrame*) AfxGetMainWnd())->RefreshCombo();
						((CMainFrame*) AfxGetMainWnd())->SetSavedViewSelection(filterDlg.m_ViewUrno);
						return;
					}
					else
						GetDocument()->DoSort();

					OnQueryChange();
				}
				((CMainFrame*) AfxGetMainWnd())->RefreshCombo();
				((CMainFrame*) AfxGetMainWnd())->SetSavedViewSelection(filterDlg.m_ViewUrno);
			}
		}
	}
}



//-----------------------------------------------------------------------
// OnBcMessage
//	
//	The message handler of broadcasts
//-----------------------------------------------------------------------
afx_msg LONG CDBDPSView::OnBcMessage(UINT wParam, LONG /*lParam*/)
{
#ifdef ABBCCS_RELEASE
	gBcHandle.GetBc(wParam);
#endif
	return TRUE;
}

//-----------------------------------------------------------------------
// OnQueryChange
//	
//	The function refreshed the grid when data changed
//-----------------------------------------------------------------------
void CDBDPSView::OnQueryChange()
{
	if(!gTableLoad)
	{
		SetColCount(0);
		InitBrowserSettings();
		return;
	}

	BOOL bLockOld = LockUpdate(TRUE);
	GetParam()->EnableUndo(FALSE);

	CWDayCheckListComboBox *gWdayControl  = new CWDayCheckListComboBox(this, 9000,9001/*random id*/);
	RegisterControl(GX_IDS_CTRL_CHECKLIST_COMBOBOX, gWdayControl); 

	// the map identifiers
	mMaxVisibleCols = 0;
	
	int nCols = gCurrentFieldsDesc->GetSize();
	SetColCount(nCols, GX_INVALIDATE);

	//*** 16.11.99 SHA ***
	ReadSettings();

	//*** SET ROWHEIGHT OF THE HEADER ***
	if (imRowHeight!=0)
		SetRowHeight(0,0,imRowHeight);

	//if (atoi(omColWidth.GetAt(0))!=0)
	//*** FIRST (MARKER) COLUMN ***
	//	SetColWidth(0, 0,atoi(omColWidth.GetAt(0)));

	for (int myCol = 0; myCol < nCols; myCol++)
	{
		CFieldInfo *myFieldInfo = gCurrentFieldsDesc->GetAt(myCol);
		if (!myFieldInfo->mVisible)
			continue;
		MapCol[++mMaxVisibleCols] = myCol;

		ROWCOL myGridCol = mMaxVisibleCols;		

		// Initialize the column headers
		SetValueRange(CGXRange(0, myGridCol), myFieldInfo->mLongHeader);

		int myColWidth = (int)((double)myFieldInfo->mCellWidth * WIDTH_CONST);
		if (myColWidth > MAX_COL_WIDTH) myColWidth = MAX_COL_WIDTH;
		if (myColWidth < MIN_COL_WIDTH) myColWidth = MIN_COL_WIDTH;

		//*** 16.11.99 SHA ***
		if (omColWidth.GetSize()==0 || myCol>omColWidth.GetSize()-2)// || atoi(omColWidth.GetAt(myCol))<=0)
			SetColWidth(myGridCol, myGridCol, myColWidth);
		else
			SetColWidth(myGridCol, myGridCol,atoi(omColWidth.GetAt(myCol+1)));

		// Initialize the column styles
		switch (myFieldInfo->mLGType)			// necessary conversions
		{						// for every LGType there are the particular CGXStyle settings
		
		case LG_UPPC:
		case LG_TRIM:
		case LG_CHAR:
			SetStyleRange(CGXRange().SetCols(myGridCol),
				CGXStyle()
					.SetReadOnly(myFieldInfo->mReadOnly)
					.SetControl(GX_IDS_CTRL_EDIT)
					//*** 99-12-21 SHA ***
					//*** TEMP!!! ***
					//.SetControl(GX_IDS_CTRL_RICHEDIT)
					//.SetFont(CGXFont().SetFaceName(_T("Times New Roman")))
					.SetFont(CGXFont().SetFaceName(_T(gStandardFont)))
					.SetMaxLength(myFieldInfo->mDBLength));
			break;
		case LG_USER:
		case LG_CUSR:
			SetStyleRange(CGXRange().SetCols(myGridCol),
				CGXStyle()
					.SetReadOnly(myFieldInfo->mReadOnly)
					.SetControl(GX_IDS_CTRL_EDIT));
			break;
		case LG_DATE:
			SetStyleRange(CGXRange().SetCols(myGridCol),
				CGXStyle()
					.SetReadOnly(myFieldInfo->mReadOnly)
					.SetControl(GX_IDS_CTRL_DATETIME)
					.SetUserAttribute(GX_IDS_UA_DATEMAX, MAX_DATE) 
					.SetUserAttribute(GX_IDS_UA_DATEMIN, MIN_DATE) 
					.SetUserAttribute(GX_IDS_UA_DATEVALIDMODE, _T("2"))
					.SetUserAttribute(GX_IDS_UA_CUSTOMFORMAT, _T("dd.MM.yyyy")));
					//*** 30.09.99 SHA ***
					//***  FOR TEST ONLY!***
					//.SetUserAttribute(GX_IDS_UA_CUSTOMFORMAT, _T("dd/MM/yyyy")));
					
			break;
		case LG_TIME:
			SetStyleRange(CGXRange().SetCols(myGridCol),
				CGXStyle()
					.SetReadOnly(myFieldInfo->mReadOnly)
					.SetControl(GX_IDS_CTRL_DATETIMENOCAL)
					.SetUserAttribute(GX_IDS_UA_CUSTOMFORMAT, _T("HH:mm")));
			break;
		case LG_WDAY:
			SetStyleRange(CGXRange().SetCols(myGridCol),
				CGXStyle()
					.SetReadOnly(myFieldInfo->mReadOnly)
					.SetControl(GX_IDS_CTRL_CHECKLIST_COMBOBOX)
					.SetItemDataPtr((void*)WDayControlAttributes::WeekOfDayAttributes));
			break;
		case LG_REAL:
		case LG_CURR:
			SetStyleRange(CGXRange().SetCols(myGridCol),
				CGXStyle()
					.SetReadOnly(myFieldInfo->mReadOnly)
					.SetControl(GX_IDS_CTRL_EDIT)
					.SetValueType(GX_VT_NUMERIC)
					.SetFormat(GX_FMT_FIXED)
					.SetPlaces(NUM_CON_REAL_NUMS)
					.SetMaxLength(myFieldInfo->mDBLength));
			break;
		case LG_LONG:
		case LG_NMBR:
			SetStyleRange(CGXRange().SetCols(myGridCol),
				CGXStyle()
					.SetReadOnly(myFieldInfo->mReadOnly)
					.SetControl(GX_IDS_CTRL_EDIT)
					.SetValueType(GX_VT_NUMERIC)
					.SetFormat(GX_FMT_FIXED)
					.SetPlaces(NUM_CON_LONG_NUMS)
					.SetMaxLength(myFieldInfo->mDBLength));
			break;
		case LG_BOOL: 
			SetStyleRange(CGXRange().SetCols(myGridCol),
				CGXStyle()
					.SetReadOnly(myFieldInfo->mReadOnly)
					.SetUserAttribute(GX_IDS_UA_CHECKBOX_CHECKED, BOOLEAN_TYPE[BOOL_TRUE])
					.SetUserAttribute(GX_IDS_UA_CHECKBOX_UNCHECKED, BOOLEAN_TYPE[BOOL_FALSE])
					.SetTriState(!myFieldInfo->mRequired)
					.SetControl(GX_IDS_CTRL_CHECKBOX3D));
			break;
//h - start
		case LG_BOL1: 
			SetStyleRange(CGXRange().SetCols(myGridCol),
				CGXStyle()
					.SetReadOnly(myFieldInfo->mReadOnly)
					.SetUserAttribute(GX_IDS_UA_CHECKBOX_CHECKED, BOL1_TYPE[BOL1_TRUE])
					.SetUserAttribute(GX_IDS_UA_CHECKBOX_UNCHECKED, BOL1_TYPE[BOL1_FALSE])
					.SetTriState(!myFieldInfo->mRequired)
					.SetControl(GX_IDS_CTRL_CHECKBOX3D));
			break;
//h - end
		//*** 18.11.1999 SHA ***
		case LG_BOL2: 
			SetStyleRange(CGXRange().SetCols(myGridCol),
				CGXStyle()
					.SetReadOnly(myFieldInfo->mReadOnly)
					.SetUserAttribute(GX_IDS_UA_CHECKBOX_CHECKED, BOL2_TYPE[BOL2_TRUE])
					.SetUserAttribute(GX_IDS_UA_CHECKBOX_UNCHECKED, BOL2_TYPE[BOL2_FALSE])
					//.SetTriState(!myFieldInfo->mRequired)
					.SetControl(GX_IDS_CTRL_CHECKBOX3D));
			break;

		case LG_LSTU:
		case LG_CDAT:
			SetStyleRange(CGXRange().SetCols(myGridCol),
				CGXStyle()
					.SetReadOnly(myFieldInfo->mReadOnly)
					.SetControl(GX_IDS_CTRL_DATETIMENOCAL)
					.SetUserAttribute(GX_IDS_UA_CUSTOMFORMAT, _T("dd.MM.yyyy HH:mm")));
			break;
		case LG_DTIM: 
			SetStyleRange(CGXRange().SetCols(myGridCol),
				CGXStyle()
					.SetReadOnly(myFieldInfo->mReadOnly)
					.SetControl(GX_IDS_CTRL_DATETIME)
					.SetUserAttribute(GX_IDS_UA_DATEMAX, MAX_DATE) 
					.SetUserAttribute(GX_IDS_UA_DATEMIN, MIN_DATE) 
					.SetUserAttribute(GX_IDS_UA_DATEVALIDMODE, _T("2"))
					.SetUserAttribute(GX_IDS_UA_CUSTOMFORMAT, _T("dd.MM.yyyy HH:mm")));
			break;
		case LG_SLST:
			SetStyleRange(CGXRange().SetCols(myGridCol),
				CGXStyle()
					.SetReadOnly(myFieldInfo->mReadOnly)
					.SetControl(GX_IDS_CTRL_TEXTFIT)
					.SetChoiceList(myFieldInfo->mChoiceList));
			break;
		case LG_MLST:
			SetStyleRange(CGXRange().SetCols(myGridCol),
				CGXStyle()
					.SetReadOnly(myFieldInfo->mReadOnly)
					.SetControl(GX_IDS_CTRL_CHECKLIST_COMBOBOX)
					.SetItemDataPtr((void*)myFieldInfo->mMultichoiceAtt));
			break;
		default:			// other types will be taken as are
			SetStyleRange(CGXRange().SetCols(myGridCol),
				CGXStyle()
					.SetControl(GX_IDS_CTRL_EDIT)
					.SetReadOnly(true));
			break;
		}			
	}
	
	SetColCount(mMaxVisibleCols, GX_INVALIDATE);

	// Initialize browser like look and feel
	InitBrowserSettings();

	GetParam()->EnableMoveCols(FALSE);	// do not allow the column moving
	GetParam()->EnableMoveRows(FALSE);
	m_nClipboardFlags = GX_DNDTEXT;

	EnableGridToolTips();		

	SetTopRow(1, GX_INVALIDATE);		
	SetLeftCol(1, GX_INVALIDATE);
	LockUpdate(bLockOld);
	Redraw();						// redraw the grid
	SetCurrentCell(1, 1, GX_SMART);	// set the cursor position to 1,1
	SetFocus();
}

BOOL CDBDPSView::CanAppend()
{
	//*** 31.08.99 SHA ***
	return gTableLoad;

	if (!gCanMainInsertRow || 
		!gCanTableInsertRow ||
		!gCanMainUpdateRow ||
		!gCanTableUpdateRow ||
		!gCanTableUpdateFields)
		return false;
	return gTableLoad;		// only if the table is loaded
}

void CDBDPSView::OnMovedCurrentCell(ROWCOL nRow, ROWCOL nCol)
{
	
	CGXBrowserView::OnMovedCurrentCell(nRow, nCol);
	// deselect the marked record if move the cursor vertically
	static ROWCOL bodyRow = 0; 
	static ROWCOL bodyCol = 0; 

	if (nRow && ((bodyRow != nRow) || (bodyCol != nCol)))
	{
		bodyRow = nRow;
		bodyCol = nCol;

		if (!IsSelectingCells())
			SetSelection(NULL);				// Deselect the whole table
	}
}

BOOL CDBDPSView::Cut()
{
	gStyleBuffer.ClearUndo();
	return CGXBrowserView::Cut();
}

BOOL CDBDPSView::Copy()
{
	gStyleBuffer.Clear();
	if (GetBrowseParam()->m_nEditMode != noMode)
		return CGXBrowserView::Copy();

	ROWCOL myCurRow, myCurCol;
	GetCurrentCell(myCurRow, myCurCol);

	CRowColArray myRows, myCols;
	ROWCOL myRowCount = GetSelectedRows(myRows);
	ROWCOL myColCount = GetSelectedCols(myCols);
	if (myRowCount == 1 && myColCount >= GetColCount())
	{					// the whole one and only one line is selected => row clipboard
		gStyleBuffer.mCanPastRow = gCanTableUpdateFields;

		if (myColCount == 1)		// also possible for the region
			if (!gCurrentFieldsDesc->GetAt(MapCol[myCurCol])->mReadOnly)
			{
				gStyleBuffer.mCanPastRegion = true;
				gStyleBuffer.m_ColNum = myCurCol;
			}
		for (ROWCOL myCol = 1; myCol <= GetColCount(); myCol++) // copying
		{	
			CFieldInfo *myFieldInfo = gCurrentFieldsDesc->GetAt(MapCol[myCol]);
			if (!myFieldInfo->mVisible || myFieldInfo->mReadOnly)
				continue;
			CGXStyle *myStyle = new CGXStyle;
			GetDocument()->GetGRCellValue(myCurRow - 1, MapCol[myCol], *myStyle);
			gStyleBuffer.mStyles.Add(myStyle);
		}
	}
	else if (myRowCount == 1 && myColCount == 1)
	{	// plnit region clipboard

		if (!gCurrentFieldsDesc->GetAt(MapCol[myCurCol])->mReadOnly)
		{
			gStyleBuffer.mCanPastRegion = true;
			gStyleBuffer.m_ColNum = myCurCol;
		}

		CGXStyle *myStyle = new CGXStyle;
		GetDocument()->GetGRCellValue(myCurRow - 1, MapCol[myCurCol], *myStyle);
		gStyleBuffer.mStyles.Add(myStyle);
	}

	return CGXBrowserView::Copy();
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//*** 15.11.99 SHA ***
/*
BOOL CDBDPSView::OnLButtonClickedRowCol(ROWCOL nRow, ROWCOL nCol, UINT nFlags, CPoint pt)	
{

	if (nRow == 0 && nCol > 0)
	{
		//ROWCOL nRow, nCol;
		//if (!GetCurrentCell(nRow, nCol))
			//return; // no column selected

		// Specify column to be sorted (could also be several columns)
		CGXSortInfoArray sortInfo;
		sortInfo.SetSize(1);
		sortInfo[0].nRC = nCol;

		// If this column is already sorted, toggle sort order
		CGXGridParam* pParam = GetParam();
		if (pParam->m_nLastSortedCol != nCol || !pParam->m_bSortAscending)
			sortInfo[0].sortOrder = CGXSortInfo::ascending;
		else
			sortInfo[0].sortOrder = CGXSortInfo::descending;

		SortRows(CGXRange().SetTable(), sortInfo);
	}

return true;
}*/
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void CDBDPSView::OnFind()
{
	//*** 16.11.99 SHA ***
	OnShowFindReplaceDialog(TRUE);

}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void CDBDPSView::OnTextNotFound(LPCTSTR)
{
	//*** 16.11.99 SHA ***
	//*** EXTENSION FOR SEARCH-FUNCTION, SEARCH IN ALL NEXT COLUMNS ***

	ROWCOL		ilRow, ilCol, ilColAnz;
	BOOL		blFound = FALSE;
	ilColAnz = GetColCount();
	GX_FR_STATE	*pslState = GXGetLastFRState();

	if ( GetCurrentCell( &ilRow, &ilCol ) && pslState )
	{
		if ( pslState->bNext )		//  d.h Abw�rts suchen
		{
			while ( !blFound && ( ilCol < ilColAnz ) ) //  und noch nicht in letzter Spalte
			{
				ilCol ++;
				ilRow = GetHeaderRows ()+1;
				blFound = FindText( ilRow, ilCol, true ) ;
			}
		}
		else 
		{	//  d.h Aufw�rts suchen
			while ( !blFound &&  ( ilCol > GetHeaderCols () + 1 ) )	
			{
				ilCol --;
				ilRow = GetRowCount ();
				blFound = FindText( ilRow, ilCol, true );
			}
		}
		if ( blFound )
			return;

	}
	MessageBeep(0);
	AfxMessageBox("No occurence found.", MB_OK + MB_ICONSTOP);
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void CDBDPSView::OnSaveSettings()
{
	//*** 16.11.99 SHA ***
	//*** SAVE USER SETTINGS IN INI-FILE ***

	CString olColWidths;
	CString olDmy;
	int ilColWidth;
	int ilCols;

	if (gSettingsPath!="#")
	{
		ilCols=GetColCount();
		olColWidths ="";

		for (int i=0;i<=ilCols;i++)
		{
			ilColWidth=GetColWidth(i);
			if (ilColWidth<10)
				ilColWidth=10;
			olDmy.Format("%i",ilColWidth);
			olColWidths = olColWidths + olDmy + ",";
		}

		WritePrivateProfileString(gTableName,"COLSIZE",olColWidths,gSettingsPath);

		int ilRowHeight=GetRowHeight(0);

		olDmy.Format("%i",ilRowHeight);
		WritePrivateProfileString(gTableName,"ROWHEIGHT",olDmy,gSettingsPath);
	}
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void CDBDPSView::OnDeleteSettings()
{
	//*** 16.11.99 SHA ***
	//*** DELETE THE USER-SETTINGS ***

	CString olMessage;
	
	olMessage.LoadString(IDS_STRING_DBDPS_0001);

	if (MessageBox(olMessage,"DBDPS",MB_ICONQUESTION|MB_YESNO)==IDYES)	
	{
		WritePrivateProfileString(gTableName,"COLSIZE",NULL,gSettingsPath);
		WritePrivateProfileString(gTableName,"ROWHEIGHT",NULL,gSettingsPath);
		WritePrivateProfileString(gTableName,NULL,NULL,gSettingsPath);
	}
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void CDBDPSView::ReadSettings()
{
	//*** READ THE USER-SETTINGS COLWIDTH / ROWHEIGHT HEADER ***
	//TCHAR olDmy[LEN_RETURN_VALUE];
	TCHAR olDmy[512];
	CString olDmyStr;

	omColWidth.RemoveAll();
	imRowHeight = 0;

	if (gSettingsPath!="#")
	{
		_tcscpy(olDmy, "#");
		ReadIniFile(gTableName,"ROWHEIGHT", olDmy, gSettingsPath);	
		if (olDmy=="#")
			imRowHeight = 0;
		else
			imRowHeight = atoi(olDmy);

		_tcscpy(olDmy, "#");
		ReadIniFile(gTableName,"COLSIZE", olDmy, gSettingsPath);	
		if (olDmy=="#")
			omColWidth.RemoveAll();
		else
		{
			omColWidth.RemoveAll();
			olDmyStr.Format("%s",olDmy);
			ConvertToArray(olDmyStr,omColWidth);
		}
	}
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void CDBDPSView::ReadIniFile(LPCTSTR pSection, LPCTSTR pKeyName, LPTSTR pDefaultString, LPCTSTR pIniFile)
{
	//*** 16.11.99 SHA ***

	DWORD ret;
	DWORD size = 0;
	//char ReturnedString[LEN_RETURN_VALUE];
	char ReturnedString[512];

	ret = GetPrivateProfileString(
				pSection,
				pKeyName,
				pDefaultString,
				ReturnedString,
				sizeof ReturnedString,
				pIniFile);

	_tcscpy(pDefaultString, ReturnedString);
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void CDBDPSView::ConvertToArray(CString &olIn, CStringArray &olOut)
{
	//*** 16.11.99 SHA ***
	//*** CONVERT ','-DELIMITED STRING TO ARRAY ***

	int ilPos1,ilPos2;
	CString olDmy;

	ilPos2 = olIn.Find(",",0);
	ilPos1=0;

	if (ilPos2==-1)
	{
		olOut.RemoveAll();
	}
	else
	{
		while (ilPos2!=-1)
		{
			CString olDmy=olIn.Mid(ilPos1+1,(ilPos2-ilPos1-1));
			ilPos1 = ilPos2;
			ilPos2 = olIn.Find(",",ilPos2+1);

			olOut.Add(olDmy);
		}
	}
}