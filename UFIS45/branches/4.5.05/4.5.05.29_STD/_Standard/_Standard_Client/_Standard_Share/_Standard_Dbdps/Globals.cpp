//***********************************************************************
// Globals.cpp
//***********************************************************************
#include <stdafx.h>
#include <Globals.h>
#include <DBDPSDoc.h>

#ifdef ABBCCS_RELEASE

#include <CCSCedaCom.h>
#include <CCSDdx.h>
#include <CCSLog.h>
#include <CCSBcHandle.h>

//*** 02.12.1999 SHA ***
#include <CedaBasicData.h>

#endif

#ifdef ABBCCS_RELEASE
#include <UfisCEDA.h>
#else 
#include <UfisSWH.h>
#endif

bool gTableLoad = false;

// aliases for special characters
const CString CLIENT_CHARS = _T("\042\047\054\002\015");	// 34,39,44,10,13
const CString SERVER_CHARS = _T("\260\261\262\264\263");	// 176,177,178,180,179

CTablesDesc gTablesDesc;			// descriptive structure
CStringArray gTables;				// the table name collection
CTableNames gLongTableNames;		// the long table name collection
CGRTable gGRTable;					// the internal data structure
CFieldsToBeSorted gSortedFields;	// fields to be sorted
CFieldsDesc *gCurrentFieldsDesc;	// the description fields of the current table 
int gURNONumBufIndex;				// the index to the URNO array
int gURNONumBuf[MAX_URNOS];		// the array for the MAX_URNOS URNOs
CUrnoMap gUrnoMap;					// the URNO map 
TCHAR gDecimalSymbol;				// the decimal symbol

CWDayCheckListComboBox *gWdayControl;	// Wday control pointer providing its methods

#ifdef ABBCCS_RELEASE
CCSLog gLog(APP_NAME);
CCSDdx gDdx;
CCSCedaCom gComHandle(&gLog, APP_NAME);
CCSBcHandle gBcHandle(&gDdx, &gComHandle, &gLog);
CCSBasic		ogCCSBasic;		// lib
#endif

//*** 15.11.99 SHA ***
CString		gSystabLogFile;		//*** LOG-File fro SYSTAB loading ***
//*** 16.11.99 SHA ***
CString		gSettingsPath;		//*** PATH TO USER-DEFINED SETTINGS ***

//*** 09.12.1999 SHA ***
bool		gConfirmUpdate;		////*** CONFIRM UPDATE BEFORE SAVE TO DB ***

//*** 20.01.2000 SHA ***
CString		gStandardFont;		//*** STANDARF FONT NAME ***

CString gParameterPassword;		//*** Password via parameter ***
CString gParameterUser;			//*** user via parameter ***

 
CMapStringToString ogTableKeys;	//*** KEYS TO THE TABLE-UNIQUE KEYS ***
								//*** E.G. ALTTAB -> 'ALTN-VARO-VATO' ***


//*** 02.12.1999 SHA ***
char pcgHome[4]      = "HAJ";
CedaBasicData	ogBCD(pcgHome, &gDdx, &gBcHandle, &gComHandle, &ogCCSBasic);

CString gHomeairport;	// homeairport
LPTSTR gDataPtr;			// Data pointer
LPTSTR gSecPtr;			// security data pointer
int gSecRowCount = 0;		// number of security rows
CString gHostName = _T("XXX");	// set default host name
CString gHostType = _T("XXX");	// set default host type
CString gUserName = _T("ANONYMOUS");	// set default user name
CString gTableExt = _T("XXX");	// set default tables extension
CString gTableName = _T("XXX");	// current table name
CString gLongTableName;				// current long table name

TCHAR gWorkstationName[64];
CString gErrorMessage;

CString gWhereClause;	// set default where clause
CString gSortClause;		// set default sort clause

CCopPast gStyleBuffer;				// Style buffer
CSecurityPtrMap gSecurityMap;

bool gCanMainInsertRow;
bool gCanTableInsertRow;
bool gCanMainUpdateRow;
bool gCanTableUpdateRow;
bool gCanMainDeleteRow;
bool gCanTableDeleteRow;
bool gCanTableUpdateFields;
BOOL gRegisteringApp = FALSE;

LPCTSTR SYS_FIELD[] =		// the list of SYSTAB field names
{
	_T("URNO"),
	_T("TANA"),					
	_T("FTAN"),					
	_T("FINA"),
	_T("FITY"),
	_T("TATY"),
	_T("FELE"),
	_T("ADDI"),
	_T("TYPE"),
	_T("TTYP"),
//	_T("TZON"),	//	systab, but not currently used 
//	_T("APC3"),	// apttab
//	_T("TICH"), // apttab
//	_T("TDI1"), // apttab
//	_T("TDI2"), // apttab
	_T("REFE"),
	_T("CLSR"),
	_T("LABL"),
	_T("REQF"),
	_T("TOLT"),
	_T("SORT"), 
	_T("DEFV") 
};

const int SYS_FIELD_LENGTH[] =		// the list of SYSTAB field lengths
{
	URNO_LENGTH,	// URNO
	TANA_LENGTH,	// TANA					
	6,			// FTAN					
	FINA_LENGTH,	// FINA
	3,			// FITY
	10,		// TATY
	8,			// FELE
	1024,		// ADDI
	4,			// TYPE
	1,			// TTYP
//	3,			// TZON	systab, but not currently used 
//	3,			// APC3	apttab
//	14,		// TICH	apttab
//	4,			// TDI1	apttab	
//	4,			// TDI2	apttab
	(FINA_LENGTH + TANA_LENGTH + 1),		// REFE
	1,			// CLSR
	64,		// LABL
	1,			// REQF
	128,		// TOLT
	4,			// SORT 
	1024		// DEFV 
};

LPCTSTR TAB_FIELD[] =		// the list of TABTAB field names
{
	_T("URNO"),					
	_T("LTNA"),					
	_T("LNAM"),	
	//*** 02.12.1999 SHA ***			
	_T("DVIW"),	
};

const int TAB_FIELD_LENGTH[] =		// the list of TABTAB field lengths
{
	URNO_LENGTH,	// URNO
	6,			// LTNA					
	64,		// LNAM					
};

LPCTSTR BOOLEAN_TYPE[] = 
{
	_T("Y"),
	_T("N"),
	_T("")
};

//h - start
LPCTSTR BOL1_TYPE[] = 
{
	_T("1"),
	_T("0"),
	_T("")
};
//h - end
//*** 18.11.1999 SHA ***
LPCTSTR BOL2_TYPE[] = 
{
	_T("X"),
	_T(" "),
	_T("")
};


LPCTSTR DATABASE_TYPE[] = 
{
	_T("C"),
	_T("VC2"),
	_T("N")
};

LPCTSTR LOGICAL_TYPE[] =				// the names for the logical types
{
	_T("TRIM"),				
	_T("UPPC"),				
	_T("USER"),				
	_T("CUSR"),				

	_T("CHAR"),				

	_T("DATE"),				
	_T("TIME"),				
	
	_T("WDAY"),				
	
	_T("REAL"),				
	_T("CURR"),				
	
	_T("LONG"),				
	_T("NMBR"),			

	_T("BOOL"),			
//h - start
	_T("BOL1"),			
//h - end
//*** 18.11.1999 SHA ***
	_T("BOL2"),			

	_T("URNO"),			
	_T("FKEY"),			
	_T("FURN"),			
	_T("FREL"),			

	_T("LSTU"),			
	_T("CDAT"),			
	_T("DTIM"),			

	_T("SLST"),
	_T("MLST")			
};

// database values for ETimeType
LPCTSTR TIME_TYPE[] = 
{
	_T("U"),				// TIME_UTC
	_T("L"),				// TIME_LOCAL
	// _T("Z"),				// TIME_ZONE
	_T("N")				// TIME_NOT_USED
};

//-----------------------------------------------------------------------
// ~CFieldInfo()
//	
//	The destructor
//-----------------------------------------------------------------------
CFieldInfo::~CFieldInfo()
{
	if (mMultichoiceAtt) 
	{
		delete &mMultichoiceAtt->Separator;
		delete &mMultichoiceAtt->DefaultCode;

		((CStringArray*)&mMultichoiceAtt->Name)->RemoveAll();
		((CStringArray*)&mMultichoiceAtt->Checked)->RemoveAll();
		((CStringArray*)&mMultichoiceAtt->Unchecked)->RemoveAll();
		((CUIntArray*)&mMultichoiceAtt->VirtKey)->RemoveAll();

		delete (CStringArray*)&mMultichoiceAtt->Name;
		delete (CStringArray*)&mMultichoiceAtt->Checked;
		delete (CStringArray*)&mMultichoiceAtt->Unchecked;
		delete (CUIntArray*)&mMultichoiceAtt->VirtKey;

		delete mMultichoiceAtt;
	}
}

//-----------------------------------------------------------------------
// Global functions
//-----------------------------------------------------------------------

//-----------------------------------------------------------------------
// GetToken
//	
//	The function provides the token from the buffer
//-----------------------------------------------------------------------
bool GetToken
	(
		LPCTSTR &FromBuffer,
		LPTSTR ToBuffer
	)
{
	int i = 0;
	while (*FromBuffer != DELIMITER_CHAR && *FromBuffer != LF_CHAR && *FromBuffer && i < MAX_VALUE_LENGTH)
	{
		if (*FromBuffer == CR_CHAR)	
		{
			FromBuffer++;
			continue;
		}
		*ToBuffer++ = *FromBuffer++;
		i++;
	}
	*ToBuffer = 0;

	if (i >= MAX_VALUE_LENGTH)
		while (*FromBuffer != DELIMITER_CHAR && *FromBuffer != LF_CHAR && *FromBuffer)
			FromBuffer++;

	if (!*FromBuffer) return false;
	FromBuffer++;
	return true;
}

//-----------------------------------------------------------------------
// ConvertStringToServer
//	
//	The function transforms the cell value to the database value
//-----------------------------------------------------------------------
void ConvertStringToServer(CString &pText)
{
	int index;

	for (int i = 0; i < pText.GetLength(); i++)
		if ((index = CLIENT_CHARS.Find(pText.GetAt(i))) != -1)
			pText.SetAt(i, SERVER_CHARS[index]);
}

//-----------------------------------------------------------------------
// ConvertStringToClient
//	
//	The function transforms the cell value to the client value
//-----------------------------------------------------------------------
void ConvertStringToClient(CString &pText)
{
	int index;

	for (int i = 0; i < pText.GetLength(); i++)
		if ((index = SERVER_CHARS.Find(pText.GetAt(i))) != -1)
			pText.SetAt(i, CLIENT_CHARS[index]);
}

//-----------------------------------------------------------------------
// SpecialConversion
//	
//	The function transforms the cell value to the filter clause value
//-----------------------------------------------------------------------
void SpecialConversion(CString &pText)
{
	ConvertStringToServer(pText);

	for(int i = 0; i < pText.GetLength(); i++)
		if(pText.GetAt(i) == SERVER_CHARS.GetAt(1))
			pText.SetAt(i, CLIENT_CHARS[1]);
}

//-----------------------------------------------------------------------
// LoadSavedViews
//	
//	The function fills parameter pSavedViews with saved views from the
// database (where clause = pWhereClause)
//-----------------------------------------------------------------------
// CHANGES:
//-----------------------------------------------------------------------
//	DATE       | AUTHOR            | DESCRIPTION
//-----------------------------------------------------------------------
// 14.04.1999 | Martin Danihel    | Added 'default' view functionality
//-----------------------------------------------------------------------
// 21.04.1999 | Martin Danihel    | 'default' view from database 
//-----------------------------------------------------------------------
// 28.06.1999 | Martin Danihel    | Error by FilterSortText (TrimLeft(),
//            |                   | TrimRight() on bad place)
//-----------------------------------------------------------------------
// 30.06.1999 | Martin Danihel    | Added security functionality
//-----------------------------------------------------------------------
BOOL LoadSavedViews
	(
		CString pTableName,
		CSavedViews* pSavedViews,
		BOOL pDefault
	)
{
	CString ParamTableName;
	CString ParamWhereClause;
	CString ParamTableFields;
	CString ParamSortClause;
	int ret;

	pSavedViews->RemoveAll();

	if(pDefault)	// get the 'default' view
	{
		// get the 'default' view from VCDTAB
		ParamTableName = VCDTAB + gTableExt;
		ParamWhereClause.Empty();
		ParamWhereClause = ParamWhereClause + WHERE_STRING +
			_T("APPN = '") + APP_NAME + _T("'") +
			AND_STRING +
			_T("PKNO = '") + gUserName + _T("'") +
			AND_STRING +
			_T("CKEY = '") + pTableName + gTableExt + _T("'") +
			AND_STRING +
			_T("CTYP = '") + DEF_VIEW_NAME + _T("'");

#ifdef ABBCCS_RELEASE
		ParamTableFields = _T("TEXT");
#else
		ParamTableFields = _T("TXT2");
#endif

		ParamSortClause = SYS_FIELD[F_URNO];

		ret = DBDPSCallCeda(UFIS_SELECT, ParamTableName, ParamSortClause,
								ParamWhereClause, ParamTableFields, NULL);
		if (ret == RC_NOT_FOUND)
		{
			// get the 'default' view from TABTAB, after it wasn't found in VCDTAB
			ParamTableName = TABTAB + gTableExt;
			ParamWhereClause.Empty();
			ParamWhereClause = ParamWhereClause + WHERE_STRING +
				_T("LTNA = '") + pTableName + gTableExt + _T("'");
			ParamTableFields = _T("DVIW");
			ParamSortClause = SYS_FIELD[F_URNO];

			ret = DBDPSCallCeda(UFIS_SELECT, ParamTableName, ParamSortClause,
									ParamWhereClause, ParamTableFields, NULL);
			if (ret == RC_NOT_FOUND)
			{
				// set the 'default' view general
				CString tmpUrnoText = DEF_VIEW_URNO;
				CString tmpViewName = DEF_VIEW_NAME;
				CString tmpFilter;
				CString tmpSort;
				pSavedViews->Add(tmpUrnoText, tmpViewName, gTableName, tmpFilter, tmpSort);
			}	
			else
			{
				if (ret)
				{
					AfxMessageBox("LoadSavedViews(). Error on CallCeda.", MB_OK + MB_ICONSTOP);
					return FALSE;
				}

				HGLOBAL myHandle;
				GetBufferHandle(UFIS_BUF, &myHandle);
				LPTSTR DataPtr = (LPTSTR)GlobalLock(myHandle);
				TCHAR Value[MAX_VALUE_LENGTH];
				LPTSTR ValuePtr = Value;
				CString UrnoText = DEF_VIEW_URNO;
				CString ViewName = DEF_VIEW_NAME;
				CString TableName = pTableName + gTableExt;
				CString FilterSortText;
				int	position;
				CString FilterText;
				CString SortText;
				int myRowCount = 1;
				int myColumnCount = 1;

				for(int i = 0; i < myRowCount; i++)
				{
					for(int j = 0; j < myColumnCount; j++)
					{
						if(!GetToken(DataPtr, ValuePtr) && 
								(i != myRowCount - 1) && (j != myColumnCount - 1))
						{
							AfxMessageBox("LoadSavedViews(). CallCeda returned invalid data buffer.", MB_OK + MB_ICONSTOP);
							return false;
						}

						if(!_tcscmp(Value, DB_NULL_VALUE))
							*Value = 0;

						CString myTempValue(ValuePtr);
						ConvertStringToClient(myTempValue);
						_tcscpy(ValuePtr, myTempValue);

						// copy values
						switch(j)
						{
							case 0:
								FilterSortText = Value;
								ConvertStringToClient(FilterSortText);
								position = FilterSortText.Find(FILTER_SORT_SEPARATOR);
								if(position == -1)
								{
									FilterText = FilterSortText;
									FilterText.TrimLeft();
									FilterText.TrimRight();
									SortText.Empty();
								}
								else
								{
									FilterText = FilterSortText.Left(position);
									FilterText.TrimLeft();
									FilterText.TrimRight();
									SortText = FilterSortText.Right(FilterSortText.GetLength() - 
										position - 1);
									SortText.TrimLeft();
									SortText.TrimRight();
								}
								SpecialConversion(FilterText);
								pSavedViews->Add(UrnoText, ViewName, TableName, FilterText, SortText);
						}
					}
				}
			}
		}	
		else
		{
			if (ret)
			{
				AfxMessageBox("LoadSavedViews(). Error on CallCeda.", MB_OK + MB_ICONSTOP);
				return FALSE;
			}

			HGLOBAL myHandle;
			GetBufferHandle(UFIS_BUF, &myHandle);
			LPTSTR DataPtr = (LPTSTR)GlobalLock(myHandle);
			TCHAR Value[MAX_VALUE_LENGTH];
			LPTSTR ValuePtr = Value;
			CString UrnoText = DEF_VIEW_URNO;
			CString ViewName = DEF_VIEW_NAME;
			CString TableName = pTableName + gTableExt;
			CString FilterSortText;
			int	position;
			CString FilterText;
			CString SortText;
			int myRowCount = 1;
			int myColumnCount = 1;

			for(int i = 0; i < myRowCount; i++)
			{
				for(int j = 0; j < myColumnCount; j++)
				{
					if(!GetToken(DataPtr, ValuePtr) && 
							(i != myRowCount - 1) && (j != myColumnCount - 1))
					{
						AfxMessageBox("LoadSavedViews(). CallCeda returned invalid data buffer.", MB_OK + MB_ICONSTOP);
						return false;
					}

					if(!_tcscmp(Value, DB_NULL_VALUE))
						*Value = 0;

					CString myTempValue(ValuePtr);
					ConvertStringToClient(myTempValue);
					_tcscpy(ValuePtr, myTempValue);

					// copy values
					switch(j)
					{
						case 0:
							FilterSortText = Value;
							ConvertStringToClient(FilterSortText);
							position = FilterSortText.Find(FILTER_SORT_SEPARATOR);
							if(position == -1)
							{
								FilterText = FilterSortText;
								FilterText.TrimLeft();
								FilterText.TrimRight();
								SortText.Empty();
							}
							else
							{
								FilterText = FilterSortText.Left(position);
								FilterText.TrimLeft();
								FilterText.TrimRight();
								SortText = FilterSortText.Right(FilterSortText.GetLength() - 
									position - 1);
								SortText.TrimLeft();
								SortText.TrimRight();
							}
							SpecialConversion(FilterText);
							pSavedViews->Add(UrnoText, ViewName, TableName, FilterText, SortText);
					}
				}
			}
		}
	}

	ParamTableName = VCDTAB + gTableExt;
	ParamWhereClause.Empty();
	ParamWhereClause = ParamWhereClause + WHERE_STRING +
		_T("APPN = '") + APP_NAME + _T("'") +
		AND_STRING +
		_T("PKNO = '") + gUserName + _T("'") +
		AND_STRING +
		_T("CTYP <> '") + DEF_VIEW_NAME + _T("'");
	if(!(pTableName.IsEmpty()))
		ParamWhereClause = ParamWhereClause +
			AND_STRING +
			_T("CKEY = '") + pTableName + gTableExt + _T("'");

	ParamTableFields = _T("URNO");
	ParamTableFields = ParamTableFields +
		DELIMITER_CHAR +
		_T("CTYP") +
		DELIMITER_CHAR +
		_T("CKEY") +
#ifdef ABBCCS_RELEASE
		DELIMITER_CHAR +_T("TEXT");
#else
		DELIMITER_CHAR +_T("TXT2");
#endif

	ParamSortClause = SYS_FIELD[F_URNO];

	ret = DBDPSCallCeda(UFIS_SELECT, ParamTableName, ParamSortClause,
							ParamWhereClause, ParamTableFields, NULL);
	if (ret == RC_NOT_FOUND)
		return TRUE;
	if (ret)
	{
		AfxMessageBox("LoadSavedViews(). Error on CallCeda.", MB_OK + MB_ICONSTOP);
		return FALSE;
	}

	HGLOBAL myHandle;
	GetBufferHandle(UFIS_BUF, &myHandle);
	LPTSTR DataPtr = (LPTSTR)GlobalLock(myHandle);
	TCHAR Value[MAX_VALUE_LENGTH];
	LPTSTR ValuePtr = Value;
	CString UrnoText;
	CString ViewName;
	CString TableName;
	CString FilterSortText;
	int	position;
	CString FilterText;
	CString SortText;
	int myRowCount = GetNumberOfLines(UFIS_BUF);
	int myColumnCount = 4;
	CString myStatus;	// security status

	for(int i = 0; i < myRowCount; i++)
	{
		for(int j = 0; j < myColumnCount; j++)
		{
			if(!GetToken(DataPtr, ValuePtr) && 
					(i != myRowCount - 1) && (j != myColumnCount - 1))
			{
				AfxMessageBox("LoadSavedViews(). CallCeda returned invalid data buffer.", MB_OK + MB_ICONSTOP);
				return false;
			}

			if(!_tcscmp(Value, DB_NULL_VALUE))
				*Value = 0;

			CString myTempValue(ValuePtr);
			ConvertStringToClient(myTempValue);
			_tcscpy(ValuePtr, myTempValue);

			// copy values
			switch(j)
			{
				case 0:
					UrnoText = Value;
					UrnoText.TrimLeft();
					UrnoText.TrimRight();
					break;
				case 1:
					ViewName = Value;
					ViewName.TrimLeft();
					ViewName.TrimRight();
					ConvertStringToClient(ViewName);
					break;
				case 2:
					TableName = Value;
					TableName.TrimLeft();
					TableName.TrimRight();
					ConvertStringToClient(TableName);
					break;
				case 3:
					FilterSortText = Value;
					ConvertStringToClient(FilterSortText);
					position = FilterSortText.Find(FILTER_SORT_SEPARATOR);
					if(position == -1)
					{
						FilterText = FilterSortText;
						FilterText.TrimLeft();
						FilterText.TrimRight();
						SortText.Empty();
					}
					else
					{
						FilterText = FilterSortText.Left(position);
						FilterText.TrimLeft();
						FilterText.TrimRight();
						SortText = FilterSortText.Right(FilterSortText.GetLength() - 
							position - 1);
						SortText.TrimLeft();
						SortText.TrimRight();
					}
					SpecialConversion(FilterText);

					myStatus = GetSecurityStatus(TableName, _T("FILE_OPEN"));
					if(myStatus == SEC_STAT_ACT)	// add table to the list of available tables
						pSavedViews->Add(UrnoText, ViewName, TableName, FilterText, SortText);
			}
		}
	}

	return TRUE;
}

//-----------------------------------------------------------------------
// ConvertFieldsToSortIndex
//	
//-----------------------------------------------------------------------
BOOL ConvertFieldsToSortIndex(CString &pTableName, const CString &pSortFields)
{
	CFieldsDesc *myFieldDesc;
	CString mySortFields = pSortFields;

	gSortClause = mySortFields;	// sets the actual sort clause in the application

	if(mySortFields.GetLength() < (SYS_FIELD_LENGTH[F_FINA] + 1))
		return TRUE;

	if (!gTablesDesc.Lookup(pTableName, myFieldDesc))
	{
		CString OutputStr;
		CString SysTable(SYSTAB + gTableExt);
		OutputStr.Format(_T("Table %s is not descripted in the %s."), 
								pTableName,
								SysTable);
		AfxMessageBox((LPCTSTR)OutputStr, MB_OK + MB_ICONSTOP);
		return FALSE;
	}

	CString myFieldName;
	BOOL myAscending;

	while(TRUE)
	{
		myFieldName = mySortFields.Left(SYS_FIELD_LENGTH[F_FINA]);
		mySortFields = mySortFields.Right(mySortFields.GetLength() - SYS_FIELD_LENGTH[F_FINA]);
		if(mySortFields.Left(1) == '-')
			myAscending = FALSE;
		else
			myAscending = TRUE;

		for(int i = 0; i < myFieldDesc->GetSize(); i++)
			if(myFieldName == myFieldDesc->GetAt(i)->mFieldName)
			{
				CSortFieldInfo *mySortField = new CSortFieldInfo;
				gSortedFields.Add(mySortField);
				mySortField->mFieldIndex = i;
				if(myAscending)
					mySortField->mSortInfo = STRCMP_A;
				else
					mySortField->mSortInfo = STRCMP_D;

				break;	//for
			}

		if(i == myFieldDesc->GetSize())
			return FALSE;

		if(mySortFields.GetLength() < (SYS_FIELD_LENGTH[F_FINA] + 1))
			break;
		else
			mySortFields = mySortFields.Right(mySortFields.GetLength() - 2);
	}

	return TRUE;
}

//-----------------------------------------------------------------------
// ClearSortedFields
//	
//	The function clears the gSortedFields
//-----------------------------------------------------------------------
void ClearSortedFields()
{
	for (int i = 0; i < gSortedFields.GetSize(); i++)
		delete gSortedFields.GetAt(i);			// delete the cell 
	gSortedFields.RemoveAll();						// remove all the items
}

//-----------------------------------------------------------------------
// GridCommpare()
//	
//	The function performs the special compare algorithm 
//-----------------------------------------------------------------------
int GridCompare(const void *arg1, const void *arg2)
{
	int myRow1 = *(int*)arg1;
	int myRow2 = *(int*)arg2;
	
	for (int i = 0; i < gSortedFields.GetSize(); i++)
	{
		CSortFieldInfo *mySortFieldInfo = gSortedFields.GetAt(i);
		int myCol = mySortFieldInfo->mFieldIndex;
		CFieldInfo *myFieldInfo = gCurrentFieldsDesc->GetAt(myCol);

		if (myFieldInfo->mIsString)
			if (myFieldInfo->mUseCollate)
			{
				CString &myValue1 = gGRTable.GetAt(myRow1)->GetAt(myCol)->mStrValue;
				CString &myValue2 = gGRTable.GetAt(myRow2)->GetAt(myCol)->mStrValue;
				if (mySortFieldInfo->mSortInfo == STRCMP_A)
				{
					int ret = myValue1.Collate(myValue2);
					if (ret) return ret;
				}
				else
				{
					int ret = myValue2.Collate(myValue1);
					if (ret) return ret;
				}
			}
			else
			{
				CString &myValue1 = gGRTable.GetAt(myRow1)->GetAt(myCol)->mStrValue;
				CString &myValue2 = gGRTable.GetAt(myRow2)->GetAt(myCol)->mStrValue;
				if (mySortFieldInfo->mSortInfo == STRCMP_A)
				{
					if (myValue1 < myValue2) return -1;
					if (myValue1 > myValue2) return 1;
				}
				else
				{
					if (myValue1 > myValue2) return -1;
					if (myValue1 < myValue2) return 1;
				}
			}
		else
		{
			double myNumValue1 = gGRTable.GetAt(myRow1)->GetAt(myCol)->mNumValue;
			double myNumValue2 = gGRTable.GetAt(myRow2)->GetAt(myCol)->mNumValue;
			if (mySortFieldInfo->mSortInfo == STRCMP_A)
			{
				if (myNumValue1 < myNumValue2) return -1;
				if (myNumValue1 > myNumValue2) return 1;
			}
			else
			{
				if (myNumValue1 > myNumValue2) return -1;
				if (myNumValue1 < myNumValue2) return 1;
			}
		}
	}
	if (*gUrnoMap.GetAt(myRow1) < *gUrnoMap.GetAt(myRow2)) return -1;

	return 1;
}

//-----------------------------------------------------------------------
// GetNewURNO
//	
//	The function provides the new URNO from the URNO buffer
//-----------------------------------------------------------------------
bool GetNewURNO(CString &NewURNO)
{
	TCHAR myURNOStr[URNO_LENGTH + 1];
	if (gURNONumBufIndex >= MAX_URNOS)
	{
		TCHAR URNOStrBuf[MAX_URNOS * (URNO_LENGTH + 1)];
		LPTSTR URNOStrBufPtr = URNOStrBuf;

		if (!GetManyURNO(URNOStrBufPtr))		// get new buffer of MAX_URNOS URNOs
			return false;
		for (int i = 0; i < MAX_URNOS; i++)
		{
			if (!GetToken(URNOStrBufPtr, myURNOStr) && i != MAX_URNOS - 1) // getToken finishes earlier
				return false;
			gURNONumBuf[i] = _ttoi(myURNOStr);
			if (gURNONumBuf[i] <= 0) 
				return false;
		}
		gURNONumBufIndex = 0;
	}
	_itot(gURNONumBuf[gURNONumBufIndex++], myURNOStr, 10);	// conversion
	NewURNO = myURNOStr;
	return true;
}


//-----------------------------------------------------------------------
// GetManyURNO
//	
//	The function fills the URNO buffer with MAX_URNOS URNOs
//-----------------------------------------------------------------------
bool GetManyURNO(LPTSTR pURNOStrBuf)
{
    TCHAR dest1[12];      // Normally CEDA
    TCHAR cmd[12];        // Command (RTA, URT, IRT, DRT, ...)
    TCHAR object[512];    // Table Name (multiple table are allowed)
    TCHAR seq[512];       // Sort Clause    
    TCHAR tws[36];        // Time Window Start 
    TCHAR twe[36];        // Time Window end
    TCHAR selection[1024];			// Selection criterium, normally a WHERE clause
    TCHAR fields[1024];				// DB field, normally database fields

	_tcscpy(dest1, _T("CEDA"));
	_tcscpy(cmd, UFIS_GMU);
	_tcscpy(object, _T(""));

	_tcscpy(seq, _T(""));

	_tcscpy(tws, _T(""));
	_tcscpy(twe, _T(""));

	_tcscpy(selection, _T(""));
	
	_tcscpy(fields, _T(""));

	_tcscpy(pURNOStrBuf, MAX_URNOS_STRING);

	gErrorMessage.Empty();

	TCHAR myWorkstation[64];
	_tcscpy(myWorkstation, gWorkstationName);
	int ret = CallCeda(myWorkstation, dest1, myWorkstation,	
						cmd, object, seq,
						tws,twe, selection,
						fields, pURNOStrBuf, "RETURN");
	if (ret) gErrorMessage = pURNOStrBuf;
//h - start
		int myPos = gErrorMessage.Find(_T("ORA-"));
		if (myPos != -1)
		{
			CString myNumber(((LPCTSTR)gErrorMessage) + (myPos + 4) * sizeof(TCHAR), 5);
			gErrorMessage.Format(_T("Database error (Error-No. %s)"), myNumber);
		}
//h - end

	if (ret) return false;
	return true;
}

//-----------------------------------------------------------------------
// DBDPSCallCeda
//	
// 
//-----------------------------------------------------------------------
int DBDPSCallCeda(LPCTSTR pUfisCommand,
				  LPCTSTR pTableName,
				  LPCTSTR pSortClause,
				  LPCTSTR pWhereClause,
				  LPCTSTR pFields,
				  LPCTSTR pValues)
{
    TCHAR dest1[12];      // Normally CEDA
    TCHAR cmd[12];        // Command (RTA, URT, IRT, DRT, ...)
    TCHAR object[512];    // Table Name (multiple table are allowed)
    TCHAR seq[512];       // Sort Clause    
    TCHAR tws[36];        // Time Window Start 
    TCHAR twe[36];        // Time Window end
    TCHAR selection[1024];			// Selection criterium, normally a WHERE clause
    TCHAR fields[1024];				// DB field, normally database fields
    TCHAR pclData[70000];				// Data

	_tcscpy(dest1, _T("CEDA"));
	_tcscpy(cmd, pUfisCommand);
	_tcscpy(object, pTableName);
	
	if (pSortClause != NULL) 
	{
		ASSERT(_tcslen(pSortClause) <= sizeof(seq));
		_tcscpy(seq, pSortClause);
	}
	else _tcscpy(seq, _T(""));

	_tcscpy(tws, _T(""));
	_tcscpy(twe, gHomeairport + DELIMITER_CHAR + gTableExt + DELIMITER_CHAR + APP_NAME);

	if (pWhereClause != NULL) 
	{
		ASSERT(_tcslen(pWhereClause) <= sizeof(selection));
		_tcscpy(selection, pWhereClause);
	}
	else _tcscpy(selection, _T(""));
	
	if (pFields != NULL) 
	{
		ASSERT(_tcslen(pFields) <= sizeof(fields));
		_tcscpy(fields, pFields);
	}
	else _tcscpy(fields, _T(""));

	if (pValues != NULL) 
	{
		ASSERT(_tcslen(pValues) <= sizeof(pclData));
		_tcscpy(pclData, pValues);
	}
	else _tcscpy(pclData, _T(""));

	gErrorMessage.Empty();

	TCHAR myWorkstation[64];
	_tcscpy(myWorkstation, gWorkstationName);
	int ret = CallCeda(myWorkstation, dest1, myWorkstation,	
						cmd, object, seq,
						tws,twe, selection,
						fields, pclData, UFIS_BUF);
						//fields, pclData, (_tcscmp(cmd, UFIS_INSERT) ? UFIS_BUF : "RETURN"));
	if (ret) 
	{
#ifdef ABBCCS_RELEASE
//h - start
		gErrorMessage = pclData;

		//SPECIAL ERROR WITHOUT PASSWORT
		//***2000-04-10 SHA
		if (ret==-7 || ret==-3)
		{
			gErrorMessage="Server not available";
		}
		int myPos = gErrorMessage.Find(_T("ORA-"));
		if (myPos != -1)
		{
			CString myNumber(((LPCTSTR)gErrorMessage) + (myPos + 4) * sizeof(TCHAR), 5);
			gErrorMessage.Format(_T("Database error (Error-No. %s)"), myNumber);
		}
//h - end

		if (!_tcsicmp(pclData, DB_NO_DATA_FOUND)) return RC_NOT_FOUND;
#else
		if (ret == RC_NOT_FOUND) return RC_NOT_FOUND;
#endif
	}
	return ret;
}

//-----------------------------------------------------------------------
// ProcessBroadcasts
//	
//	The function processes broadcasts
//-----------------------------------------------------------------------
void ProcessBroadcasts
	(
		void * pInstance,
		int pDdxType,
		void * pDataPointer,
		CString &pInstanceName
	)
{
	((CDBDPSDoc *) pInstance)->ProcessBc(pDdxType, pDataPointer, pInstanceName);
}

//-----------------------------------------------------------------------
// GetSecurityStatus
//		(
//			LPCTSTR pObjectId,
//			LPCTSTR pControlId
//		)
//	
//	The function gets the security status of the apllication module
//-----------------------------------------------------------------------
LPCTSTR GetSecurityStatus
	(
		LPCTSTR pObjectId,
		LPCTSTR pControlId
	)
{
	CSecurityObjectCollection *mySOC;
	CSecurityObject *mySO;
	CString myStatus = SEC_STAT_DEF;

	if (!gSecurityMap.Lookup(pObjectId, mySOC))
		return myStatus;

	if (!mySOC->Lookup(pControlId, mySO))
		return myStatus;

	return mySO->mStatus;
}
