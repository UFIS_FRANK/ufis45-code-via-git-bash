Attribute VB_Name = "mdlMain"
Option Explicit

Global gsAppName As String
Global gsUserName As String
Global gbDebug As Boolean

'### GLOBAL UTC OFFSET IN HOURS
Global gUTCOffset As Single

Sub Main()
    Dim strRetLogin As String
    Dim strCommand As String

    'gsAppName = "HASConfigTool"
    gsAppName = "EfficiencyReports"
    
    strCommand = Command()
    If strCommand = "DEBUG" Then
        gbDebug = True
    Else
        gbDebug = False
    End If

    Load frmMain
    frmMain.Visible = False

    'do the login
    InitLoginControl
    If gbDebug = False Then
        strRetLogin = frmMain.AATLoginControl1.ShowLoginDialog
    Else
        strRetLogin = "OK"
    End If

    'look after the login result
    If strRetLogin = "CANCEL" Or strRetLogin = "ERROR" Then
        frmMain.UfisCom1.CleanupCom
        End
    Else
        frmMain.UfisCom1.CleanupCom
        gsUserName = frmMain.AATLoginControl1.GetUserName()
        'frmMain.SetPrivStat
        
        Load frmData
        frmData.InitialLoadData


        If gbDebug = True Then
            frmData.Visible = True
        End If

        frmMain.RefreshView
        frmMain.StatusBar1.Panels(1).Text = "Ready."
        frmMain.Show 'vbModal
    End If
    
End Sub

Private Sub InitLoginControl()
    Dim strRegister As String
    ' giving the login control an UfisCom to do the login
    Set frmMain.AATLoginControl1.UfisComCtrl = frmMain.UfisCom1

    ' setting some information to be displayed on the login control
    frmMain.AATLoginControl1.ApplicationName = gsAppName
    frmMain.AATLoginControl1.VersionString = "4.5.0.1"
    frmMain.AATLoginControl1.InfoCaption = "Info about Efficiency Reports"
    frmMain.AATLoginControl1.InfoButtonVisible = True
    frmMain.AATLoginControl1.InfoUfisVersion = "UFIS Version 4.5"
    frmMain.AATLoginControl1.InfoAppVersion = CStr("EfficiencyReports 4.5.0.1")
    frmMain.AATLoginControl1.InfoCopyright = "� 2001-2005 UFIS Airport Solutions GmbH"
    frmMain.AATLoginControl1.InfoAAT = "UFIS Airport Solutions GmbH"
    frmMain.AATLoginControl1.UserNameLCase = False 'not automatic upper case letters
    frmMain.AATLoginControl1.ApplicationName = "EfficiencyReports"
    frmMain.AATLoginControl1.LoginAttempts = 3

    'strRegister = "HASConfigTool,InitModu,InitModu,Initialize (InitModu),B,-"
    strRegister = "WKSAdmin,InitModu,InitModu,Initialize (InitModu),B,-"
    strRegister = strRegister & ",TAB_Configuration,m_TAB_Configuration,Action,A,1"
    frmMain.AATLoginControl1.RegisterApplicationString = strRegister
End Sub
