﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using Ufis.Entities;

namespace Ufis.ADLoginUser
{
    public class ADLoginUser
    {
        /// <summary>
        /// Get Current Windows Login username and domain name
        /// </summary>
        /// <returns>Return List string array that contain user name and domain name</returns>
        public static List<string> GetADLoginUserList()
        {
            List<string> sLoginUser = new List<string>();
            

            sLoginUser.Add(WindowsIdentity.GetCurrent().Name.Contains("\\") == true ? WindowsIdentity.GetCurrent().Name.Substring(WindowsIdentity.GetCurrent().Name.IndexOf("\\") + 1) : WindowsIdentity.GetCurrent().Name);
            sLoginUser.Add(System.Environment.UserDomainName);            

            return sLoginUser;
        }
        /// <summary>
        /// Get Current Windows Login user name and domain name
        /// </summary>
        /// <returns>Return Entity that contain Windows Login UserName and Domain Name</returns>
        public EntADLoginUser GetADLoginUserEntity()
        {
            EntADLoginUser objADUser = new EntADLoginUser();

            objADUser.UserName = WindowsIdentity.GetCurrent().Name.Contains("\\") == true ? WindowsIdentity.GetCurrent().Name.Substring(WindowsIdentity.GetCurrent().Name.IndexOf("\\") + 1) : WindowsIdentity.GetCurrent().Name;
            objADUser.DomainName = System.Environment.UserDomainName;

            return objADUser;
        }
    }
    public class EntADLoginUser : Entity
    {
        private string _UserName;
        public string UserName
        {
            get { return _UserName; }
            set
            {
            	if (_UserName != value)
                {
                    _UserName = value;
                    OnPropertyChanged (UserName);
                }
            }
        }

        private string _DomainName;
        public string DomainName
        {
            get { return _DomainName; }

            set
            {
                if (_DomainName != value)
                {
                    _DomainName = value;
                    OnPropertyChanged(DomainName);
                }
            }
        }
    }
}
