#if !defined(AFX_UCEDIT_H__092C9802_B042_4BA4_B0AE_59E6FACE380F__INCLUDED_)
#define AFX_UCEDIT_H__092C9802_B042_4BA4_B0AE_59E6FACE380F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

// UCEdit.h : main header file for UCEDIT.DLL

#if !defined( __AFXCTL_H__ )
	#error include 'afxctl.h' before including this file
#endif

#include "resource.h"       // main symbols

/////////////////////////////////////////////////////////////////////////////
// CUCEditApp : See UCEdit.cpp for implementation.

class CUCEditApp : public COleControlModule
{
public:
	BOOL InitInstance();
	int ExitInstance();
};

extern const GUID CDECL _tlid;
extern const WORD _wVerMajor;
extern const WORD _wVerMinor;

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_UCEDIT_H__092C9802_B042_4BA4_B0AE_59E6FACE380F__INCLUDED)
