#if !defined(AFX_AATEDIT5CTL_H__C3865253_FE62_11D4_90BC_00010204AA51__INCLUDED_)
#define AFX_AATEDIT5CTL_H__C3865253_FE62_11D4_90BC_00010204AA51__INCLUDED_
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * 
 *	AATEdit5:	ActiveX Wrapper for CCSEdit
 *
 *	This ActiveX control is a wrapper for the existing functionality
 *	of the old CCSEdit MFC Control.
 *	
 *	ABB Airport Technologies GmbH 2001
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 *		Author			Date			Comment
 *	------------------------------------------------------------------
 *
 *		cla	AAT/ID		09/02/2001		Initial version
 *		cla AAT/ID		21/02/2001		SetTypeToInt added
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <CCSEdit.h>
// Aatedit5Ctl.h : Declaration of the CAatedit5Ctrl ActiveX Control class.

/////////////////////////////////////////////////////////////////////////////
// CAatedit5Ctrl : See Aatedit5Ctl.cpp for implementation.

class CAatedit5Ctrl : public COleControl
{
	DECLARE_DYNCREATE(CAatedit5Ctrl)

// Constructor
public:
	CAatedit5Ctrl();
	CFont omMSSansSerif_Regular_8;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAatedit5Ctrl)
	public:
	virtual void OnDraw(CDC* pdc, const CRect& rcBounds, const CRect& rcInvalid);
	virtual void DoPropExchange(CPropExchange* pPX);
	virtual void OnResetState();
	//}}AFX_VIRTUAL

// Implementation

	CCSEdit	omEdit;
	CRect	omEditRect;
protected:
	~CAatedit5Ctrl();

	DECLARE_OLECREATE_EX(CAatedit5Ctrl)    // Class factory and guid
	DECLARE_OLETYPELIB(CAatedit5Ctrl)      // GetTypeInfo
	DECLARE_PROPPAGEIDS(CAatedit5Ctrl)     // Property page IDs
	DECLARE_OLECTLTYPE(CAatedit5Ctrl)		// Type name and misc status

// Message maps
	//{{AFX_MSG(CAatedit5Ctrl)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

// Dispatch maps
	//{{AFX_DISPATCH(CAatedit5Ctrl)
	afx_msg void SetBkColor(long BackColor);
	afx_msg void SetTypeToDouble(long PrePoint, long PostPoint, double From, double To);
	afx_msg void SetTypeToMoney(long PrePoint, long PostPoint, double From, double To);
	afx_msg void SetTypeToTime(BOOL Req, BOOL ChangeDay);
	afx_msg void SetTypeToDate(BOOL Req);
	afx_msg void SetInitText(LPCTSTR IniText, BOOL ChangeColor);
	afx_msg void SetReadOnly(BOOL ReadOnly);
	afx_msg void SetTextConstraint(long MinLength, long MaxLength, BOOL EmptyValid);
	afx_msg void SetFormat(LPCTSTR FormatStr);
	afx_msg void SetRegularExpression(LPCTSTR Expr);
	afx_msg BOOL CheckRegMatch(LPCTSTR Val);
	afx_msg void SetRange(long From, long To);
	afx_msg void SetPrecision(long PrePoint, long PostPoint);
	afx_msg void SetTextColor(long TextColor);
	afx_msg void SetTextChangedColor(long TextChangedColor);
	afx_msg BSTR GetWindowText();
	afx_msg void SetSecState(LPCTSTR key);
	afx_msg long GetTextColor();
	afx_msg void SetTextErrColor(long ErrColor);
	afx_msg BOOL GetStatus();
	afx_msg void SetStatus(BOOL status);
	afx_msg BOOL IsChanged();
	afx_msg void SetTypeToString(LPCTSTR Format, long MaxLength, long MinLength);
	afx_msg void SetTypeToInt(long MaxVal, long MinVal);
	afx_msg void SetTextLimit(long MinLen, long MaxLen,BOOL EmptyValid);
	//}}AFX_DISPATCH
	DECLARE_DISPATCH_MAP()

	afx_msg void AboutBox();

// Event maps
	//{{AFX_EVENT(CAatedit5Ctrl)
	void FireChange()
		{FireEvent(eventidChange,EVENT_PARAM(VTS_NONE));}
	void FireLButtonDown()
		{FireEvent(eventidLButtonDown,EVENT_PARAM(VTS_NONE));}
	void FireKillFocus()
		{FireEvent(eventidKillFocus,EVENT_PARAM(VTS_NONE));}
	//}}AFX_EVENT
	DECLARE_EVENT_MAP()

public:
	virtual LRESULT WindowProc( UINT message, WPARAM wParam, LPARAM lParam );

// Dispatch and event IDs
public:
	enum {
	//{{AFX_DISP_ID(CAatedit5Ctrl)
	dispidSetBkColor = 1L,
	dispidSetTypeToDouble = 2L,
	dispidSetTypeToMoney = 3L,
	dispidSetTypeToTime = 4L,
	dispidSetTypeToDate = 5L,
	dispidSetInitText = 6L,
	dispidSetReadOnly = 7L,
	dispidSetTextConstraint = 8L,
	dispidSetFormat = 9L,
	dispidSetRegularExpression = 10L,
	dispidCheckRegMatch = 11L,
	dispidSetRange = 12L,
	dispidSetPrecision = 13L,
	dispidSetTextColor = 14L,
	dispidSetTextChangedColor = 15L,
	dispidGetWindowText = 16L,
	dispidSetSecState = 17L,
	dispidGetTextColor = 18L,
	dispidSetTextErrColor = 19L,
	dispidGetStatus = 20L,
	dispidSetStatus = 21L,
	dispidIsChanged = 22L,
	dispidSetTypeToString = 23L,
	eventidChange = 1L,
	eventidLButtonDown = 2L,
	eventidKillFocus = 3L,
	//}}AFX_DISP_ID
	};
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_AATEDIT5CTL_H__C3865253_FE62_11D4_90BC_00010204AA51__INCLUDED)
