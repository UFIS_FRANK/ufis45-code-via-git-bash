﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ufis.MVVM.ViewModel;
using Ufis.Entities;
using Ufis.Data;
using Ufis.MVVM.Command;
using System.Windows.Input;
using DevExpress.Xpf.Grid;
using System.IO;
using Ufis.Utilities;
using System.Windows;
using System.Windows.Data;
using System.ComponentModel;
using DevExpress.Xpf.Core;

namespace Ufis.UserLayout
{
    /// <summary>
    /// Represent the view model for user defined layout related window.
    /// </summary>
    public class UserDefinedLayoutViewModel: WorkspaceViewModel
    {
        private const string DELETE_WARNING_TEXT =
            "Do you really want to delete the selected layout?";
        private const string DELETE_WARNING_CAPTION = "Confirmation";

        private EntityCollectionBase<EntDbUserDefinedLayout> _userDefinedLayouts;
        RelayCommand _saveViewCommand;
        RelayCommand _saveViewAsCommand;
        RelayCommand _deleteViewCommand;

        /// <summary>
        /// Gets or sets the application name that owns the user define layouts.
        /// </summary>
        public string ApplicationName { get; set; }

        /// <summary>
        /// Gets or sets the application section name for the user define layouts.
        /// </summary>
        public string SectionName { get; set; }

        /// <summary>
        /// Gets or sets the application subsection name for the user define layouts.
        /// </summary>
        public string SubsectionName { get; set; }

        /// <summary>
        /// Gets or sets the DevExpress.Xpf.Grid.GridControl object from which the layout will be saved.
        /// </summary>
        public GridControl LayoutGridControl { get; set; }

        /// <summary>
        /// Gets or sets the user defined layouts collection.
        /// </summary>
        public EntityCollectionBase<EntDbUserDefinedLayout> UserDefinedLayouts
        {
            get
            {
                return _userDefinedLayouts;
            }
            set
            {
                if (_userDefinedLayouts == value)
                    return;

                _userDefinedLayouts = value;

                OnPropertyChanged("UserDefinedLayouts");
            }
        }

        /// <summary>
        /// Gets the filtered view of user defined layouts.
        /// </summary>
        public ListCollectionView FilteredUserDefinedLayouts { get; private set; }

        /// <summary>
        /// Gets the second instance of filtered view of user defined layouts.
        /// </summary>
        public ListCollectionView SecondFilteredUserDefinedLayouts { get; private set; }

        /// <summary>
        /// Creates the filtered view of user defined layouts base on the application name, section name and subsection name.
        /// </summary>
        public void FilterView()
        {
            FilteredUserDefinedLayouts = GetFilteredView(true);
            SecondFilteredUserDefinedLayouts = GetFilteredView(false);
        }

        private ListCollectionView GetFilteredView(bool asDefaultView)
        {
            ListCollectionView view;
            if (asDefaultView)
                view = (ListCollectionView)CollectionViewSource.GetDefaultView(UserDefinedLayouts);
            else
                view = (ListCollectionView)new CollectionViewSource { Source = UserDefinedLayouts }.View;

            if (view != null)
            {
                view.Filter = item =>
                {
                    EntDbUserDefinedLayout udLayout = item as EntDbUserDefinedLayout;
                    if (udLayout == null)
                        return false;
                    else
                        return (udLayout.ApplicationName == ApplicationName &&
                                udLayout.Section == SectionName &&
                                udLayout.Subsection == SubsectionName);
                };

                view.SortDescriptions.Clear();
                view.SortDescriptions.Add(
                        new SortDescription("Name", ListSortDirection.Ascending));
                view.Refresh();
            }

            return view;
        }

        /// <summary>
        /// Returns a command that save the user defined layout.
        /// </summary>
        public ICommand SaveViewCommand
        {
            get
            {
                if (_saveViewCommand == null)
                    _saveViewCommand = new RelayCommand(SaveView);

                return _saveViewCommand;
            }
        }

        /// <summary>
        /// Returns a command that save the user defined layout with other name.
        /// </summary>
        public ICommand SaveViewAsCommand
        {
            get
            {
                if (_saveViewAsCommand == null)
                    _saveViewAsCommand = new RelayCommand(SaveViewAs);

                return _saveViewAsCommand;
            }
        }

        /// <summary>
        /// Returns a command that delete the user defined layout.
        /// </summary>
        public ICommand DeleteViewCommand
        {
            get
            {
                if (_deleteViewCommand == null)
                    _deleteViewCommand = new RelayCommand(DeleteView);

                return _deleteViewCommand;
            }
        }

        private void SaveView(object param)
        {
            if (LayoutGridControl == null)
                return;

            string strLayoutValue = string.Empty;
            using (Stream newLayout = new MemoryStream())
            {
                LayoutGridControl.SaveLayoutToStream(newLayout);
                strLayoutValue = UfisDxLayoutConverter.DxToUfisGridLayout(newLayout);
            }

            EntDbUserDefinedLayout currentUserDefinedLayout = (EntDbUserDefinedLayout)FilteredUserDefinedLayouts.CurrentItem;            
            currentUserDefinedLayout.Value = strLayoutValue;
            currentUserDefinedLayout.SetEntityState(Entity.EntityState.Modified);

            try
            {
                UserDefinedLayouts.Save(currentUserDefinedLayout);
            }
            catch (Exception e)
            {
                DXMessageBox.Show("Unable to save the layout!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void SaveViewAs(object param)
        {
            if (LayoutGridControl == null)
                return;

            string strLayoutName = param.ToString();
            if (IsLayoutNameUnique(strLayoutName))
            {
                string strLayoutValue = string.Empty;
                using (Stream newLayout = new MemoryStream())
                {
                    LayoutGridControl.SaveLayoutToStream(newLayout);
                    strLayoutValue = UfisDxLayoutConverter.DxToUfisGridLayout(newLayout);
                }

                try
                {
                    int intUrno = (int)UserDefinedLayouts.DataContext.EntityAdapter.GetUniqueId();

                    EntDbUserDefinedLayout currentUserDefinedLayout = (EntDbUserDefinedLayout)FilteredUserDefinedLayouts.CurrentItem;
                    EntDbUserDefinedLayout newUdLayout = new EntDbUserDefinedLayout();
                    currentUserDefinedLayout.CopyEntityAttributes(newUdLayout, true);
                    newUdLayout.Urno = intUrno;
                    newUdLayout.Name = strLayoutName;
                    newUdLayout.Value = strLayoutValue;

                    UserDefinedLayouts.Add(newUdLayout);
                    UserDefinedLayouts.Save(newUdLayout);

                    //move current item to newly save layout
                    FilteredUserDefinedLayouts.MoveCurrentTo(newUdLayout);
                }
                catch (Exception e)
                {
                    DXMessageBox.Show("Unable to save the layout!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            else
            {
                DXMessageBox.Show("Layout name is not unique!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private bool IsLayoutNameUnique(string layoutName)
        {
            return !UserDefinedLayouts.Any(l => l.ApplicationName.Equals(ApplicationName) && 
                                                l.Section.Equals(SectionName) && 
                                                l.Name.Equals(layoutName));
        }

        private void DeleteView(object param)
        {
            if (param == null)
            {
                DXMessageBox.Show("Please select the view to delete!", "Delete View", MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }

            MessageBoxResult result = DXMessageBox.Show(DELETE_WARNING_TEXT, DELETE_WARNING_CAPTION,
                MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.No);
            if (result == MessageBoxResult.Yes)
            {
                EntDbUserDefinedLayout udLayout = (EntDbUserDefinedLayout)param;
                try
                {
                    udLayout.SetEntityState(Entity.EntityState.Deleted);

                    UserDefinedLayouts.Save(udLayout);
                    UserDefinedLayouts.Remove(udLayout);
                }
                catch (Exception e)
                {
                    DXMessageBox.Show("Unable to delete the layout!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }
    }
}
