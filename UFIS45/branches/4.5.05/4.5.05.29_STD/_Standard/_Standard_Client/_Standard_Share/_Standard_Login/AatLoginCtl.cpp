// AatLoginCtl.cpp : Implementation of the CAatLoginCtrl ActiveX Control class.

#include "stdafx.h"
#include "AatLogin.h"
#include "AatLoginCtl.h"
#include "AatLoginPpg.h"
#include "LoginDlg.h"
#include "DummyDlg1.h"
#include "VersionInfo.h"
#include <DUfisCom.h>
#include "UfisCom.h"
#include <winsock.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


IMPLEMENT_DYNCREATE(CAatLoginCtrl, COleControl)


/////////////////////////////////////////////////////////////////////////////
// Message map

BEGIN_MESSAGE_MAP(CAatLoginCtrl, COleControl)
	//{{AFX_MSG_MAP(CAatLoginCtrl)
	// NOTE - ClassWizard will add and remove message map entries
	//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG_MAP
	ON_OLEVERB(AFX_IDS_VERB_PROPERTIES, OnProperties)
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// Dispatch map

BEGIN_DISPATCH_MAP(CAatLoginCtrl, COleControl)
	//{{AFX_DISPATCH_MAP(CAatLoginCtrl)
	DISP_PROPERTY_EX(CAatLoginCtrl, "RegisterApplicationString", GetRegisterApplicationString, SetRegisterApplicationString, VT_BSTR)
	DISP_PROPERTY_EX(CAatLoginCtrl, "ApplicationName", GetApplicationName, SetApplicationName, VT_BSTR)
	DISP_PROPERTY_EX(CAatLoginCtrl, "LoginAttempts", GetLoginAttempts, SetLoginAttempts, VT_I2)
	DISP_PROPERTY_EX(CAatLoginCtrl, "VersionString", GetVersionString, SetVersionString, VT_BSTR)
	DISP_PROPERTY_EX(CAatLoginCtrl, "InfoButtonVisible", GetInfoButtonVisible, SetInfoButtonVisible, VT_BOOL)
	DISP_PROPERTY_EX(CAatLoginCtrl, "InfoUfisVersion", GetInfoUfisVersion, SetInfoUfisVersion, VT_BSTR)
	DISP_PROPERTY_EX(CAatLoginCtrl, "InfoCopyright", GetInfoCopyright, SetInfoCopyright, VT_BSTR)
	DISP_PROPERTY_EX(CAatLoginCtrl, "InfoAAT", GetInfoAAT, SetInfoAAT, VT_BSTR)
	DISP_PROPERTY_EX(CAatLoginCtrl, "InfoCaption", GetInfoCaption, SetInfoCaption, VT_BSTR)
	DISP_PROPERTY_EX(CAatLoginCtrl, "UfisComCtrl", GetUfisComCtrl, SetUfisComCtrl, VT_DISPATCH)
	DISP_PROPERTY_EX(CAatLoginCtrl, "InfoAppVersion", GetInfoAppVersion, SetInfoAppVersion, VT_BSTR)
	DISP_PROPERTY_EX(CAatLoginCtrl, "UserNameLCase", GetUserNameLCase, SetUserNameLCase, VT_BOOL)
	DISP_PROPERTY_EX(CAatLoginCtrl, "Version", GetVersion, SetVersion, VT_BSTR)
	DISP_PROPERTY_EX(CAatLoginCtrl, "BuildDate", GetBuildDate, SetBuildDate, VT_BSTR)
	DISP_PROPERTY_EX(CAatLoginCtrl, "UseVersionInfo", GetUseVersionInfo, SetUseVersionInfo, VT_BOOL)
	DISP_FUNCTION(CAatLoginCtrl, "ShowLoginDialog", ShowLoginDialog, VT_BSTR, VTS_NONE)
	DISP_FUNCTION(CAatLoginCtrl, "DoLoginSilentMode", DoLoginSilentMode, VT_BSTR, VTS_BSTR VTS_BSTR)
	DISP_FUNCTION(CAatLoginCtrl, "GetPrivileges", GetPrivileges, VT_BSTR, VTS_BSTR)
	DISP_FUNCTION(CAatLoginCtrl, "GetUserName", GetUserName, VT_BSTR, VTS_NONE)
	DISP_FUNCTION(CAatLoginCtrl, "GetUserPassword", GetUserPassword, VT_BSTR, VTS_NONE)
	DISP_FUNCTION(CAatLoginCtrl, "WriteErrToLog", WriteErrToLog, VT_BOOL, VTS_BSTR VTS_BSTR VTS_PDISPATCH)
	DISP_FUNCTION(CAatLoginCtrl, "GetPrivilegList", GetPrivilegList, VT_BSTR, VTS_NONE)
	DISP_FUNCTION(CAatLoginCtrl, "GetWorkstationName", GetWorkstationName, VT_BSTR, VTS_NONE)
	DISP_FUNCTION(CAatLoginCtrl, "GetRealWorkstationName", GetRealWorkstationName, VT_BSTR, VTS_NONE)
	//}}AFX_DISPATCH_MAP
	DISP_FUNCTION_ID(CAatLoginCtrl, "AboutBox", DISPID_ABOUTBOX, AboutBox, VT_EMPTY, VTS_NONE)
END_DISPATCH_MAP()


/////////////////////////////////////////////////////////////////////////////
// Event map

BEGIN_EVENT_MAP(CAatLoginCtrl, COleControl)
	//{{AFX_EVENT_MAP(CAatLoginCtrl)
	// NOTE - ClassWizard will add and remove event map entries
	//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_EVENT_MAP
END_EVENT_MAP()


/////////////////////////////////////////////////////////////////////////////
// Property pages

// TODO: Add more property pages as needed.  Remember to increase the count!
BEGIN_PROPPAGEIDS(CAatLoginCtrl, 1)
	PROPPAGEID(CAatLoginPropPage::guid)
END_PROPPAGEIDS(CAatLoginCtrl)


/////////////////////////////////////////////////////////////////////////////
// Initialize class factory and guid

IMPLEMENT_OLECREATE_EX(CAatLoginCtrl, "AATLOGIN.AatLoginCtrl.1",
	0x17d7e209, 0x2dec, 0x48bc, 0x83, 0xe2, 0x3e, 0x9f, 0x43, 0xb9, 0xd, 0x59)


/////////////////////////////////////////////////////////////////////////////
// Type library ID and version

IMPLEMENT_OLETYPELIB(CAatLoginCtrl, _tlid, _wVerMajor, _wVerMinor)


/////////////////////////////////////////////////////////////////////////////
// Interface IDs

const IID BASED_CODE IID_DAatLogin =
		{ 0xbb73c4e9, 0x3e22, 0x4d7e, { 0x8e, 0xe3, 0xa1, 0xbf, 0xa9, 0xa6, 0x39, 0xfe } };
const IID BASED_CODE IID_DAatLoginEvents =
		{ 0x63e2943b, 0xe15e, 0x4827, { 0xa9, 0x35, 0x5f, 0x68, 0xb8, 0x6a, 0x36, 0xdb } };


/////////////////////////////////////////////////////////////////////////////
// Control type information

static const DWORD BASED_CODE _dwAatLoginOleMisc =
	OLEMISC_ACTIVATEWHENVISIBLE |
	OLEMISC_SETCLIENTSITEFIRST |
	OLEMISC_INSIDEOUT |
	OLEMISC_CANTLINKINSIDE |
	OLEMISC_RECOMPOSEONRESIZE;

IMPLEMENT_OLECTLTYPE(CAatLoginCtrl, IDS_AATLOGIN, _dwAatLoginOleMisc)


/////////////////////////////////////////////////////////////////////////////
// CAatLoginCtrl::CAatLoginCtrlFactory::UpdateRegistry -
// Adds or removes system registry entries for CAatLoginCtrl

BOOL CAatLoginCtrl::CAatLoginCtrlFactory::UpdateRegistry(BOOL bRegister)
{
	// TODO: Verify that your control follows apartment-model threading rules.
	// Refer to MFC TechNote 64 for more information.
	// If your control does not conform to the apartment-model rules, then
	// you must modify the code below, changing the 6th parameter from
	// afxRegApartmentThreading to 0.

	if (bRegister)
		return AfxOleRegisterControlClass(
			AfxGetInstanceHandle(),
			m_clsid,
			m_lpszProgID,
			IDS_AATLOGIN,
			IDB_AATLOGIN,
			afxRegApartmentThreading,
			_dwAatLoginOleMisc,
			_tlid,
			_wVerMajor,
			_wVerMinor);
	else
		return AfxOleUnregisterClass(m_clsid, m_lpszProgID);
}


/////////////////////////////////////////////////////////////////////////////
// CAatLoginCtrl::CAatLoginCtrl - Constructor

CAatLoginCtrl::CAatLoginCtrl()
{
	InitializeIIDs(&IID_DAatLogin, &IID_DAatLoginEvents);

	// TODO: Initialize your control's instance data here.
	int		serr;
    WSADATA wsadata;
    serr = WSAStartup(0x0101, &wsadata );

    // retrieve the configuration path from OS environment
    if (getenv("CEDA") == NULL)
        strcpy(this->pcmConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
    else
        strcpy(this->pcmConfigPath, getenv("CEDA"));

	if (!this->SetVersionInfo())
	{
		this->SetUserNameLCase(FALSE);
		this->SetInfoCaption("Info about this application");
		this->SetInfoAAT("ABB Airport Technologies GmbH / Information Systems");
		this->SetInfoUfisVersion("UFIS Version 4.5");
		this->SetInfoAppVersion("LoginCtrl 01.01.20xx / 4.5.x.x");	
		this->SetInfoCopyright("Copyright 2003 AAT/I");
		this->SetInfoButtonVisible(FALSE);
		this->SetLoginAttempts(3);
		this->SetRegisterApplicationString("");
		this->SetApplicationName("");
	}


	this->pomUfisCom  = NULL;
	this->pomDummyDlg = NULL;

}


/////////////////////////////////////////////////////////////////////////////
// CAatLoginCtrl::~CAatLoginCtrl - Destructor

CAatLoginCtrl::~CAatLoginCtrl()
{
	// TODO: Cleanup your control's instance data here.
	WSACleanup();

	this->omStatusMap.RemoveAll();

	if (this->pomDummyDlg != NULL)
	{
		pomDummyDlg->DestroyWindow();
		delete pomDummyDlg;
	}
}


/////////////////////////////////////////////////////////////////////////////
// CAatLoginCtrl::OnDraw - Drawing function

void CAatLoginCtrl::OnDraw(CDC* pdc, const CRect& rcBounds, const CRect& rcInvalid)
{
	// TODO: Replace the following code with your own drawing code.
	pdc->FillRect(rcBounds, CBrush::FromHandle((HBRUSH)GetStockObject(WHITE_BRUSH)));
	pdc->Ellipse(rcBounds);
}


/////////////////////////////////////////////////////////////////////////////
// CAatLoginCtrl::DoPropExchange - Persistence support

void CAatLoginCtrl::DoPropExchange(CPropExchange* pPX)
{
	ExchangeVersion(pPX, MAKELONG(_wVerMinor, _wVerMajor));
	COleControl::DoPropExchange(pPX);

	// TODO: Call PX_ functions for each persistent custom property.

}


/////////////////////////////////////////////////////////////////////////////
// CAatLoginCtrl::OnResetState - Reset control to default state

void CAatLoginCtrl::OnResetState()
{
	COleControl::OnResetState();  // Resets defaults found in DoPropExchange

	// TODO: Reset any other control state here.
}


/////////////////////////////////////////////////////////////////////////////
// CAatLoginCtrl::AboutBox - Display an "About" box to the user

void CAatLoginCtrl::AboutBox()
{
	CDialog dlgAbout(IDD_ABOUTBOX_AATLOGIN);
	dlgAbout.DoModal();
}


/////////////////////////////////////////////////////////////////////////////
// CAatLoginCtrl message handlers

BSTR CAatLoginCtrl::ShowLoginDialog() 
{
	// TODO: Add your dispatch handler code here
	if (this->GetUfisComCtrl() == NULL)
	{
#if 1
		this->pomDummyDlg = new CDummyDlg();
		this->pomDummyDlg->Create(IDD_DUMMY,this);
		this->pomDummyDlg->ShowWindow(SW_HIDE);
		this->pomUfisCom = this->pomDummyDlg->GetUfisComCtrl();
#else
	CWnd *pParent = this->GetParent();
	if (pParent != NULL)							
	{
		// create UFIS COM control window
		CUfisCom olUfisCom;
		BOOL blCreated = olUfisCom.Create(NULL,WS_CHILD|WS_DISABLED,CRect(0,0,100,100),pParent,4712);
		ASSERT(blCreated);
		IUnknown *pIUnk = olUfisCom.GetControlUnknown();
		IDispatch *pIDispatch;
		HRESULT res = pIUnk->QueryInterface(IID_IDispatch,(void **)&pIDispatch);
		this->pomUfisCom = pIDispatch;
		olUfisCom.Detach();
	}
#endif
	}

	this->omLoginResult = "";

	imLoginAttempts = 3;
    if (this->omLoginAttempts > 0)
	{
		imLoginAttempts = this->omLoginAttempts;
	}

	while (this->omLoginResult != "OK" && this->omLoginResult != "CANCEL" &&  imLoginAttempts > 0)
	{
		CLoginDlg olDlg(this,this);

		olDlg.DoModal();

		--imLoginAttempts;
	}

	return this->omLoginResult.AllocSysString();
}

BSTR CAatLoginCtrl::DoLoginSilentMode(LPCTSTR strUserName, LPCTSTR strUserPassword) 
{
	// TODO: Add your dispatch handler code here
	if (this->GetUfisComCtrl() == NULL)
	{
#if 1
		this->pomDummyDlg = new CDummyDlg();
		this->pomDummyDlg->Create(IDD_DUMMY,this);
		this->pomDummyDlg->ShowWindow(SW_HIDE);
		this->pomUfisCom = this->pomDummyDlg->GetUfisComCtrl();
#else
	CWnd *pParent = this->GetParent();
	if (pParent != NULL)							
	{
		// create UFIS COM control window
		CUfisCom olUfisCom;
		BOOL blCreated = olUfisCom.Create(NULL,WS_CHILD|WS_DISABLED,CRect(0,0,100,100),pParent,4712);
		ASSERT(blCreated);
		IUnknown *pIUnk = olUfisCom.GetControlUnknown();
		IDispatch *pIDispatch;
		HRESULT res = pIUnk->QueryInterface(IID_IDispatch,(void **)&pIDispatch);
		this->pomUfisCom = pIDispatch;
		olUfisCom.Detach();
	}
#endif
	}

	CLoginDlg olDlg(this);

	CString strResult = olDlg.DoSilentLogin(strUserName,strUserPassword);

	return strResult.AllocSysString();
}

BSTR CAatLoginCtrl::GetRegisterApplicationString() 
{
	// TODO: Add your property handler here
	return this->omRegisterApplicationString.AllocSysString();
}

void CAatLoginCtrl::SetRegisterApplicationString(LPCTSTR lpszNewValue) 
{
	// TODO: Add your property handler here
	this->omRegisterApplicationString = lpszNewValue;
	SetModifiedFlag();
}

BSTR CAatLoginCtrl::GetApplicationName() 
{
	// TODO: Add your property handler here
	return this->omApplicationName.AllocSysString();
}

void CAatLoginCtrl::SetApplicationName(LPCTSTR lpszNewValue) 
{
	// TODO: Add your property handler here
	this->omApplicationName = lpszNewValue;
	SetModifiedFlag();
}

short CAatLoginCtrl::GetLoginAttempts() 
{
	// TODO: Add your property handler here

	return this->omLoginAttempts;
}

void CAatLoginCtrl::SetLoginAttempts(short nNewValue) 
{
	// TODO: Add your property handler here
	this->omLoginAttempts = nNewValue;
	SetModifiedFlag();
}

BSTR CAatLoginCtrl::GetVersionString() 
{
	// TODO: Add your property handler here
	return this->omVersionString.AllocSysString();
}

void CAatLoginCtrl::SetVersionString(LPCTSTR lpszNewValue) 
{
	// TODO: Add your property handler here
	this->omVersionString = lpszNewValue;
	SetModifiedFlag();
}

BSTR CAatLoginCtrl::GetPrivileges(LPCTSTR strItem) 
{
	CString strResult;
	// TODO: Add your dispatch handler code here
	CString olKey(strItem);
	this->omStatusMap.Lookup(olKey,strResult);

	return strResult.AllocSysString();
}

BSTR CAatLoginCtrl::GetUserName() 
{
	// TODO: Add your dispatch handler code here
	return this->omUserName.AllocSysString();
}

BSTR CAatLoginCtrl::GetUserPassword() 
{
	// TODO: Add your dispatch handler code here
	return this->omPassword.AllocSysString();
}

BOOL CAatLoginCtrl::GetInfoButtonVisible() 
{
	// TODO: Add your property handler here
	return this->bmInfoButtonVisible;
}

void CAatLoginCtrl::SetInfoButtonVisible(BOOL bNewValue) 
{
	// TODO: Add your property handler here
	this->bmInfoButtonVisible = bNewValue;
	SetModifiedFlag();
}

BSTR CAatLoginCtrl::GetInfoUfisVersion() 
{
	// TODO: Add your property handler here
	return this->omUfisVersion.AllocSysString();
}

void CAatLoginCtrl::SetInfoUfisVersion(LPCTSTR lpszNewValue) 
{
	// TODO: Add your property handler here
	this->omUfisVersion = lpszNewValue;
	SetModifiedFlag();
}

BSTR CAatLoginCtrl::GetInfoCopyright() 
{
	// TODO: Add your property handler here

	return this->omInfoCopyright.AllocSysString();
}

void CAatLoginCtrl::SetInfoCopyright(LPCTSTR lpszNewValue) 
{
	// TODO: Add your property handler here
	this->omInfoCopyright = lpszNewValue;
	SetModifiedFlag();
}

BSTR CAatLoginCtrl::GetInfoAAT() 
{
	// TODO: Add your property handler here

	return this->omInfoAAT.AllocSysString();
}

void CAatLoginCtrl::SetInfoAAT(LPCTSTR lpszNewValue) 
{
	// TODO: Add your property handler here
	this->omInfoAAT = lpszNewValue;
	SetModifiedFlag();
}

BSTR CAatLoginCtrl::GetInfoCaption() 
{
	// TODO: Add your property handler here

	return this->omInfoCaption.AllocSysString();
}

void CAatLoginCtrl::SetInfoCaption(LPCTSTR lpszNewValue) 
{
	// TODO: Add your property handler here
	this->omInfoCaption = lpszNewValue;
	SetModifiedFlag();
}

LPDISPATCH CAatLoginCtrl::GetUfisComCtrl() 
{
	// TODO: Add your property handler here
	
	LPDISPATCH lpDispatch = this->pomUfisCom;
	if (lpDispatch != NULL)
	{
		ULONG nRef = lpDispatch->AddRef();
	}

	return lpDispatch;
}

void CAatLoginCtrl::SetUfisComCtrl(LPDISPATCH newValue) 
{
	// TODO: Add your property handler here
	this->pomUfisCom = newValue;

	SetModifiedFlag();
}

BSTR CAatLoginCtrl::GetInfoAppVersion() 
{
	// TODO: Add your property handler here

	return this->omInfoAppVersion.AllocSysString();
}

void CAatLoginCtrl::SetInfoAppVersion(LPCTSTR lpszNewValue) 
{
	// TODO: Add your property handler here
	this->omInfoAppVersion = lpszNewValue;
	SetModifiedFlag();
}

BOOL CAatLoginCtrl::GetUserNameLCase() 
{
	// TODO: Add your property handler here

	return this->bmUserNameLCase;
}

void CAatLoginCtrl::SetUserNameLCase(BOOL bNewValue) 
{
	// TODO: Add your property handler here
	this->bmUserNameLCase = bNewValue;
	SetModifiedFlag();
}

BSTR CAatLoginCtrl::GetVersion() 
{
	// TODO: Add your property handler here

	return this->omVersion.AllocSysString();
}

void CAatLoginCtrl::SetVersion(LPCTSTR lpszNewValue) 
{
	// TODO: Add your property handler here
	this->omVersion = lpszNewValue;
	SetModifiedFlag();
}

BSTR CAatLoginCtrl::GetBuildDate() 
{
	// TODO: Add your property handler here

	return this->omBuildDate.AllocSysString();
}

void CAatLoginCtrl::SetBuildDate(LPCTSTR lpszNewValue) 
{
	// TODO: Add your property handler here
	this->omBuildDate = lpszNewValue;
	SetModifiedFlag();
}

BOOL CAatLoginCtrl::WriteErrToLog(LPCTSTR sFileName, LPCTSTR sFunction, LPDISPATCH FAR* ErrObj) 
{
	// TODO: Add your dispatch handler code here

	return TRUE;
}

CString CAatLoginCtrl::GetHomeAirport()
{
	static CString olHopo = "";
	if (olHopo.GetLength() == 0)
	{
		if (this->pomDummyDlg == NULL)
		{
			_DUfisCom olUfisCom;
			olUfisCom.AttachDispatch(this->GetUfisComCtrl(),TRUE);
			olHopo = olUfisCom.GetHomeAirport();						
		}

		if (olHopo.GetLength() == 0)
		{
			char pclValue[512];
			::GetPrivateProfileString(this->omApplicationName,"HOMEAIRPORT","",pclValue,sizeof(pclValue),this->pcmConfigPath);
			if (strlen(pclValue) == 0)
			{
				::GetPrivateProfileString("GLOBAL","HOMEAIRPORT","",pclValue,sizeof(pclValue),this->pcmConfigPath);
			}

			olHopo = pclValue;
		}
	}

	return olHopo;
}

bool CAatLoginCtrl::GetWarning(CString &opApp, CString &opCustomer)
{
	bool blRet = false;
	CString olText;

	char pclValue[512];
			
	::GetPrivateProfileString("GLOBAL","UNAUTHORIZED_ACCESS_WARNING","",pclValue,sizeof(pclValue),this->pcmConfigPath);

	olText = CString(pclValue); 

	if(!olText.IsEmpty())
	{

		int ilPos = olText.Find('-');

		if(ilPos > 0)
		{
			opApp = olText.Left(ilPos);
			
			opCustomer = olText.Right(olText.GetLength() - ilPos -1);
			return true;
		}
	}

	return blRet;
}




CString CAatLoginCtrl::TableExtension()
{
	static CString olTabExt = "";
	if (olTabExt.GetLength() == 0)
	{
		if (this->pomDummyDlg == NULL)
		{
			_DUfisCom olUfisCom;
			olUfisCom.AttachDispatch(this->GetUfisComCtrl(),TRUE);
			olTabExt = olUfisCom.GetTableExt();					
		}
		
		if (olTabExt.GetLength() == 0)
		{
			char pclValue[512];
			::GetPrivateProfileString("GLOBAL","TABLEEXTENSION","",pclValue,sizeof(pclValue),this->pcmConfigPath);
			olTabExt = pclValue;
		}
	}

	return olTabExt;

}

CString CAatLoginCtrl::HostName()
{
	static CString olHostName = "";
	if (olHostName.GetLength() == 0)
	{
		if (this->pomDummyDlg == NULL)
		{
			_DUfisCom olUfisCom;
			olUfisCom.AttachDispatch(this->GetUfisComCtrl(),TRUE);
			olHostName = olUfisCom.GetServerName();
		}

		if (olHostName.GetLength() == 0)
		{
			char pclValue[512];
			::GetPrivateProfileString(this->omApplicationName,"HOSTNAME","",pclValue,sizeof(pclValue),this->pcmConfigPath);
			if (strlen(pclValue) == 0)
			{
				::GetPrivateProfileString("GLOBAL","HOSTNAME","",pclValue,sizeof(pclValue),this->pcmConfigPath);
			}

			olHostName = pclValue;
		}
	}

	return olHostName;

}

CString CAatLoginCtrl::HostType()
{
	static CString olHostType = "";
	if (olHostType.GetLength() == 0)
	{
		if (this->pomDummyDlg == NULL)
		{
			_DUfisCom olUfisCom;
			olUfisCom.AttachDispatch(this->GetUfisComCtrl(),TRUE);
			olHostType = olUfisCom.GetAccessMethod();
		}

		if (olHostType.GetLength() == 0)
		{
			char pclValue[512];
			::GetPrivateProfileString(this->omApplicationName,"HOSTTYPE","",pclValue,sizeof(pclValue),this->pcmConfigPath);
			if (strlen(pclValue) == 0)
			{
				::GetPrivateProfileString("GLOBAL","HOSTTYPE","",pclValue,sizeof(pclValue),this->pcmConfigPath);
			}
			olHostType = pclValue;
		}
	}

	return olHostType;

}

BOOL CAatLoginCtrl::SetVersionInfo()
{
	VersionInfo olVersionInfo;

	// Get my version info at first
	BOOL blResult = VersionInfo::GetVersionInfo(AfxGetInstanceHandle(),olVersionInfo);
	if (blResult)
	{
		this->SetVersion(olVersionInfo.omFileVersion);
	}
	else
	{
		this->SetVersion("N/A");

	}

	this->SetBuildDate(CString(__DATE__) + " - " + CString(__TIME__));

	// get the version info of the executable next
	blResult = VersionInfo::GetVersionInfo(NULL,olVersionInfo);	
	if (blResult)
	{
		CString olAppVersion;
		olVersionInfo.omFileVersion.Replace(',','.');
		olAppVersion.Format("%s %s %s",olVersionInfo.omProductName,olVersionInfo.omFileVersion, olVersionInfo.omSpecialBuild);

		this->SetVersionString(olVersionInfo.omFileVersion);
		this->SetUserNameLCase(FALSE);
		this->SetInfoCaption("Info about this application");
		this->SetInfoAAT(olVersionInfo.omCompanyName);
		this->SetInfoUfisVersion(olVersionInfo.omProductVersion);
		this->SetInfoAppVersion(olAppVersion);	
		this->SetInfoCopyright(olVersionInfo.omLegalCopyright);
		this->SetInfoButtonVisible(FALSE);
		this->SetLoginAttempts(3);
		this->SetRegisterApplicationString("");
		this->SetApplicationName(olVersionInfo.omInternalName);

		this->omTwe.Format("%s,%s,%s,%s",this->GetHomeAirport(),this->TableExtension(),olVersionInfo.omProductName,olVersionInfo.omFileVersion);
		this->omInfoAppName = olVersionInfo.omProductName;

	}


	return blResult;
}


BOOL	CAatLoginCtrl::GetCedaIsConnected()
{
	return this->bmCedaIsConnected;
}
void	CAatLoginCtrl::SetCedaIsConnected(BOOL blConnected)
{
	this->bmCedaIsConnected = blConnected;
}

CMapStringToString *CAatLoginCtrl::GetStatusMap()
{
	return &this->omStatusMap;
}

CString CAatLoginCtrl::GetLoginResult()
{
	return this->omLoginResult;
}

void CAatLoginCtrl::SetLoginResult(const CString& ropString)
{
	this->omLoginResult = ropString;
}

BOOL CAatLoginCtrl::GetUseVersionInfo() 
{
	// TODO: Add your property handler here

	return this->bmUseVersionInfo;
}

void CAatLoginCtrl::SetUseVersionInfo(BOOL bNewValue) 
{
	// TODO: Add your property handler here
	this->bmUseVersionInfo = bNewValue;
	SetModifiedFlag();
}

BSTR CAatLoginCtrl::GetPrivilegList() 
{
	CString strResult;
	// TODO: Add your dispatch handler code here

	CString olKey,olValue;
	for (POSITION pos = this->omStatusMap.GetStartPosition(); pos != NULL;)
	{
		this->omStatusMap.GetNextAssoc(pos,olKey,olValue);
		strResult += olKey;
		strResult += ',';
		strResult += olValue;
		if (pos != NULL)
		{
			strResult += ';';
		}
	}


	return strResult.AllocSysString();
}

BSTR CAatLoginCtrl::GetWorkstationName()
{
	CString strResult;

	char	pclTmpBuf[512];
	char	pclConfigPath[512];
	static BOOL	blUseIpAddress = -1;
	char	ws_name[512];

	if (blUseIpAddress == -1)
	{
		blUseIpAddress = FALSE;
	    if (getenv("CEDA") == NULL)
	        strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
	    else
	        strcpy(pclConfigPath, getenv("CEDA"));
	
		GetPrivateProfileString("GLOBAL","USEWORKSTATIONNAME","YES",pclTmpBuf,512,pclConfigPath);
		if (stricmp(pclTmpBuf,"NO") == 0)
		{
			blUseIpAddress = TRUE;
		}
	}
	
	if (gethostname(ws_name,130) == SOCKET_ERROR) 
	{
		return strResult.AllocSysString();
	}
	else if (blUseIpAddress)
	{
		struct hostent *polHostEnt = gethostbyname(ws_name);
		if (polHostEnt != NULL)
		{
			IN_ADDR olIpAddress = (*(IN_ADDR *)polHostEnt->h_addr_list[0]);
			sprintf(ws_name,"%X",olIpAddress.S_un.S_addr);
		}
		else
		{
			return strResult.AllocSysString();
		}
	}

	strResult = ws_name;
	return strResult.AllocSysString();
}

BSTR CAatLoginCtrl::GetRealWorkstationName()
{
	CString strResult;
	char	ws_name[512];

	if (gethostname(ws_name,130) == SOCKET_ERROR) 
	{
		return strResult.AllocSysString();
	}

	strResult = ws_name;
	return strResult.AllocSysString();
}

CString CAatLoginCtrl::GetTwe()
{
	this->omTwe.Format("%s,%s,%s,%s",this->GetHomeAirport(),this->TableExtension(),this->omApplicationName,this->omVersionString);
	this->omTwe = this->omTwe.Left(32);
	return this->omTwe;
}

//=======================================================================
// MWO:			16.07.2003
// Parameter:	DC, The rect, the color, the mode, up/down, and percent of 
//				black/white
//				
// Return:		-
//
// Description: If Life style is set the function is used to draw the color 
//				gradient
//				
//=======================================================================
void CAatLoginCtrl::DrawLifeStyle(CDC *pdc, CRect opRect, int MyColor, int ipMode, BOOL DrawDown, int ipPercent)
{
    int intBLUESTART = 255;
    int intBLUEEND = 0;
    double intBANDHEIGHT = 1.01;//15;
    int intSHADOWSTART = 64;
    int intSHADOWCOLOR = 0;
    int intTEXTSTART = 0;
    int intTEXTCOLOR = 15;
    int intRed = 1;
    int intGreen = 2;
    int intBlue = 4;
    int intBackRed = 8;
    int intBackGreen = 16;
    int intBackBlue = 32;
    float sngBlueCur;
    float sngBlueStep;
    long intFormHeight;
    long intFormWidth;
    long intX;
    long intY;
    int iColor;
	int ilColorValue = 255;

	ilColorValue = (int)(ilColorValue/100)*ipPercent;
    long iRed, iBlue, iGreen;
    CString prntText;
    COLORREF ReturnColor;
	CPen *pOldPen;
	sngBlueCur = 0;
	
	//MWO: 17.07.2003 Draw with bitmap
/*	CDC myMemDC;
	CBitmap bmpArea;
	if (bmpArea.m_hObject != NULL)
		bmpArea.DeleteObject();

	myMemDC.CreateCompatibleDC (pdc);
	CRect myR;
	myR = opRect;
	myR.right += 1;
	bmpArea.CreateCompatibleBitmap	(pdc, myR.right, myR.bottom);
	myMemDC.SelectObject (&bmpArea);
*/
	//pOldPen = pdc->SelectObject(&olPen);
    ReturnColor = COLORREF(RGB(255,255,255)); //vbWhite
    if( MyColor >= 0)
	{
        intFormHeight = opRect.bottom - opRect.top; //MyPanel.ScaleHeight
        intFormWidth =  opRect.right - opRect.left; //MyPanel.ScaleWidth
    
        iColor = MyColor;
        sngBlueCur = (float)intBLUESTART;
    
        if( DrawDown == TRUE)
		{
            if( ipMode == 0)
			{
                sngBlueStep = (float)((float)(intBANDHEIGHT * (intBLUEEND - intBLUESTART)) / intFormHeight);
				sngBlueStep = (float)((sngBlueStep/100)*ipPercent);
				//if(sngBlueStep == 0) sngBlueStep = 1;
                for( intY = opRect.top ; intY <= opRect.bottom ; intY+= (int)intBANDHEIGHT)
//                for( intY = 1; intY <= intFormHeight; intY+= (int)intBANDHEIGHT)
				{
					CPen olPen;
                    if( iColor & intBlue) iBlue = (long)sngBlueCur;
                    if( iColor & intRed) iRed = (long)sngBlueCur;
                    if( iColor & intGreen) iGreen = (long)sngBlueCur;
                    if( iColor & intBackBlue) iBlue = 255 - (long)sngBlueCur;
                    if( iColor & intBackRed) iRed = 255 - (long)sngBlueCur;
                    if( iColor & intBackGreen) iGreen = 255 - (long)sngBlueCur;

					olPen.CreatePen(PS_SOLID, 1, COLORREF(RGB(iRed, iGreen, iBlue)));
					pOldPen = pdc->SelectObject(&olPen);
					//pdc->MoveTo(0, intY);
					pdc->MoveTo(opRect.left+1, intY);
					pdc->LineTo(/*intFormWidth*/opRect.right, intY);
                    sngBlueCur = sngBlueCur + sngBlueStep;
                    if( intY == 0 ) ReturnColor = COLORREF(RGB(iRed, iGreen, iBlue));
					pdc->SelectObject(pOldPen); 
                }
			}
            if(ipMode == 1)
			{
                sngBlueStep = (float)((float)(intBANDHEIGHT * (intBLUEEND - intBLUESTART)) / intFormWidth);
				sngBlueStep = (float)((sngBlueStep/100)*ipPercent);
                for( intX = opRect.left+1;  intX <= intFormWidth; intX += (int)intBANDHEIGHT)
                //for( intX = 0;  intX <= intFormWidth; intX += (int)intBANDHEIGHT)
				{
					CPen olPen;
                    if( iColor & intBlue ) iBlue = (long)sngBlueCur;
                    if( iColor & intRed ) iRed = (long)sngBlueCur;
                    if( iColor & intGreen ) iGreen = (long)sngBlueCur;
                    if( iColor & intBackBlue ) iBlue = 255 - (long)sngBlueCur;
                    if( iColor & intBackRed ) iRed = 255 - (long)sngBlueCur;
                    if( iColor & intBackGreen ) iGreen = 255 - (long)sngBlueCur;
					olPen.CreatePen(PS_SOLID, 1, COLORREF(RGB(iRed, iGreen, iBlue)));
					pOldPen = pdc->SelectObject(&olPen);
					pdc->MoveTo(intX, 1);
					pdc->LineTo(intX + (int)intBANDHEIGHT, intFormHeight);
                    //MyPanel.Line (intX - 1, -1)-(intX + intBANDHEIGHT, intFormHeight), RGB(iRed, iGreen, iBlue), BF
                    sngBlueCur = sngBlueCur + sngBlueStep;
					pdc->SelectObject(pOldPen); 
                }
			}
		}
        else
		{
            if(  ipMode == 0 )
			{
                sngBlueStep = (float)((float)(intBANDHEIGHT * (intBLUEEND - intBLUESTART)) / intFormHeight);
				sngBlueStep = (float)((sngBlueStep/100)*ipPercent);
                for( intY = opRect.bottom; intY >= opRect.top; intY -= (int)intBANDHEIGHT)
//                for( intY = intFormHeight; intY >= -1; intY -= (int)intBANDHEIGHT)
				{
					CPen olPen;
                    if( iColor & intBlue ) iBlue = (long)sngBlueCur;
                    if( iColor & intRed ) iRed = (long)sngBlueCur;
                    if( iColor & intGreen ) iGreen = (long)sngBlueCur;
                    if( iColor & intBackBlue ) iBlue = 255 - (long)sngBlueCur;
                    if( iColor & intBackRed ) iRed = 255 - (long)sngBlueCur;
                    if( iColor & intBackGreen ) iGreen = 255 - (long)sngBlueCur;
					olPen.CreatePen(PS_SOLID, 1, COLORREF(RGB(iRed, iGreen, iBlue)));
					pOldPen = pdc->SelectObject(&olPen);
//					pdc->MoveTo(0, intY);
					pdc->MoveTo(opRect.left, intY);
					pdc->LineTo(opRect.right, intY);
					//pdc->LineTo(intFormWidth, intY + (int)intBANDHEIGHT);
                    //MyPanel.Line (-1, intY - 1)-(intFormWidth, intY + intBANDHEIGHT), RGB(iRed, iGreen, iBlue), BF
                    sngBlueCur = sngBlueCur + sngBlueStep;
					pdc->SelectObject(pOldPen); 
                }
			}
            if( ipMode == 1)
			{
                sngBlueStep = (float)((float)(intBANDHEIGHT * (intBLUEEND - intBLUESTART)) / intFormWidth);
				sngBlueStep = (float)((sngBlueStep/100)*ipPercent);
                for( intX = opRect.right; intX >= opRect.left; intX -= (int)intBANDHEIGHT)
//                for( intX = intFormWidth; intX >= 0; intX -= (int)intBANDHEIGHT)
				{
					CPen olPen;
                    if( iColor & intBlue ) iBlue = (long)sngBlueCur;
                    if( iColor & intRed ) iRed = (long)sngBlueCur;
                    if( iColor & intGreen ) iGreen = (long)sngBlueCur;
                    if( iColor & intBackBlue ) iBlue = 255 - (long)sngBlueCur;
                    if( iColor & intBackRed ) iRed = 255 - (long)sngBlueCur;
                    if( iColor & intBackGreen ) iGreen = 255 - (long)sngBlueCur;
					olPen.CreatePen(PS_SOLID, 1, COLORREF(RGB(iRed, iGreen, iBlue)));
					pOldPen = pdc->SelectObject(&olPen);
					pdc->MoveTo(intX, opRect.top);
					pdc->LineTo(intX /*+ (int)intBANDHEIGHT*/, opRect.bottom );
                    //MyPanel.Line (intX - 1, -1)-(intX + intBANDHEIGHT, intFormHeight), RGB(iRed, iGreen, iBlue), BF
                    sngBlueCur = sngBlueCur + sngBlueStep;
					pdc->SelectObject(pOldPen); 
                }
			}
		}
		if( ipMode == 2)
		{
            sngBlueStep = (float)((float)(intBANDHEIGHT * (intBLUEEND - intBLUESTART)) / intFormHeight)*2;
			sngBlueStep = (float)((sngBlueStep/100)*ipPercent);
			//if(sngBlueStep == 0) sngBlueStep = 1;
			int myFrom;
			int myTo;
			myTo = opRect.top;
			myFrom = (int)(opRect.bottom -  (int)((opRect.bottom -opRect.top)/2) );
            for( intY = myFrom; intY >= myTo; intY-= (int)intBANDHEIGHT)
//            for( intY = ((int)intFormHeight/2); intY >= 0; intY-= (int)intBANDHEIGHT)
			{
				CPen olPen;
                if( iColor & intBlue) iBlue = (long)sngBlueCur;
                if( iColor & intRed) iRed = (long)sngBlueCur;
                if( iColor & intGreen) iGreen = (long)sngBlueCur;
                if( iColor & intBackBlue) iBlue = 255 - (long)sngBlueCur;
                if( iColor & intBackRed) iRed = 255 - (long)sngBlueCur;
                if( iColor & intBackGreen) iGreen = 255 - (long)sngBlueCur;

				olPen.CreatePen(PS_SOLID, 1, COLORREF(RGB(iRed, iGreen, iBlue)));
				pOldPen = pdc->SelectObject(&olPen);
				pdc->MoveTo(opRect.left+1, intY);
//				pdc->MoveTo(0, intY);
				pdc->LineTo(opRect.left + intFormWidth, intY);
                //MyPanel.Line (-1, intY - 1)-(intFormWidth, intY + intB&HEIGHT), RGB(iRed, iGreen, iBlue), BF
                sngBlueCur = sngBlueCur + sngBlueStep;
                if( intY == 0 ) ReturnColor = COLORREF(RGB(iRed, iGreen, iBlue));
				pdc->SelectObject(pOldPen); 
            }
			sngBlueCur = -1;
			myFrom = (int)(opRect.top +  (int)((opRect.bottom -opRect.top)/2) );
			myTo = opRect.bottom;
            for( intY = myFrom; intY <= myTo; intY+= (int)intBANDHEIGHT)
//            for( intY = (int)intFormHeight/2; intY <= intFormHeight; intY+= (int)intBANDHEIGHT)
			{
				CPen olPen;
                if( iColor & intBlue) iBlue = (long)sngBlueCur;
                if( iColor & intRed) iRed = (long)sngBlueCur;
                if( iColor & intGreen) iGreen = (long)sngBlueCur;
                if( iColor & intBackBlue) iBlue = 255 - (long)sngBlueCur;
                if( iColor & intBackRed) iRed = 255 - (long)sngBlueCur;
                if( iColor & intBackGreen) iGreen = 255 - (long)sngBlueCur;

				olPen.CreatePen(PS_SOLID, 1, COLORREF(RGB(iRed, iGreen, iBlue)));
				pOldPen = pdc->SelectObject(&olPen);
				pdc->MoveTo(opRect.left+1, intY);
//				pdc->MoveTo(0, intY);
				pdc->LineTo(opRect.left + intFormWidth, intY);
                //MyPanel.Line (-1, intY - 1)-(intFormWidth, intY + intB&HEIGHT), RGB(iRed, iGreen, iBlue), BF
                sngBlueCur = sngBlueCur + sngBlueStep;
                if( intY == 0 ) ReturnColor = COLORREF(RGB(iRed, iGreen, iBlue));
				pdc->SelectObject(pOldPen); 
            }
		}
		if( ipMode == 3)
		{
		}
    }
//	pdc->BitBlt( myR.left, myR.top, myR.right, myR.bottom, &myMemDC, myR.left, myR.top, SRCCOPY);

}
