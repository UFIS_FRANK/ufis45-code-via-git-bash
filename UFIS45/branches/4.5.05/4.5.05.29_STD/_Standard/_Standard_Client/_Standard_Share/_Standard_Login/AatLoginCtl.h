#if !defined(AFX_AATLOGINCTL_H__961A2B4B_A86A_43DA_A374_324BA73B018F__INCLUDED_)
#define AFX_AATLOGINCTL_H__961A2B4B_A86A_43DA_A374_324BA73B018F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

// AatLoginCtl.h : Declaration of the CAatLoginCtrl ActiveX Control class.
class CDummyDlg;
/////////////////////////////////////////////////////////////////////////////
// CAatLoginCtrl : See AatLoginCtl.cpp for implementation.

class CAatLoginCtrl : public COleControl
{
	DECLARE_DYNCREATE(CAatLoginCtrl)

// Constructor
public:
	CAatLoginCtrl();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAatLoginCtrl)
	public:
	virtual void OnDraw(CDC* pdc, const CRect& rcBounds, const CRect& rcInvalid);
	virtual void DoPropExchange(CPropExchange* pPX);
	virtual void OnResetState();
	//}}AFX_VIRTUAL

// Implementation
protected:
	~CAatLoginCtrl();

	DECLARE_OLECREATE_EX(CAatLoginCtrl)    // Class factory and guid
	DECLARE_OLETYPELIB(CAatLoginCtrl)      // GetTypeInfo
	DECLARE_PROPPAGEIDS(CAatLoginCtrl)     // Property page IDs
	DECLARE_OLECTLTYPE(CAatLoginCtrl)		// Type name and misc status

// Message maps
	//{{AFX_MSG(CAatLoginCtrl)
		// NOTE - ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

// Dispatch maps
	//{{AFX_DISPATCH(CAatLoginCtrl)
	afx_msg BSTR GetRegisterApplicationString();
	afx_msg void SetRegisterApplicationString(LPCTSTR lpszNewValue);
	afx_msg BSTR GetApplicationName();
	afx_msg void SetApplicationName(LPCTSTR lpszNewValue);
	afx_msg short GetLoginAttempts();
	afx_msg void SetLoginAttempts(short nNewValue);
	afx_msg BSTR GetVersionString();
	afx_msg void SetVersionString(LPCTSTR lpszNewValue);
	afx_msg BOOL GetInfoButtonVisible();
	afx_msg void SetInfoButtonVisible(BOOL bNewValue);
	afx_msg BSTR GetInfoUfisVersion();
	afx_msg void SetInfoUfisVersion(LPCTSTR lpszNewValue);
	afx_msg BSTR GetInfoCopyright();
	afx_msg void SetInfoCopyright(LPCTSTR lpszNewValue);
	afx_msg BSTR GetInfoAAT();
	afx_msg void SetInfoAAT(LPCTSTR lpszNewValue);
	afx_msg BSTR GetInfoCaption();
	afx_msg void SetInfoCaption(LPCTSTR lpszNewValue);
	afx_msg LPDISPATCH GetUfisComCtrl();
	afx_msg void SetUfisComCtrl(LPDISPATCH newValue);
	afx_msg BSTR GetInfoAppVersion();
	afx_msg void SetInfoAppVersion(LPCTSTR lpszNewValue);
	afx_msg BOOL GetUserNameLCase();
	afx_msg void SetUserNameLCase(BOOL bNewValue);
	afx_msg BSTR GetVersion();
	afx_msg void SetVersion(LPCTSTR lpszNewValue);
	afx_msg BSTR GetBuildDate();
	afx_msg void SetBuildDate(LPCTSTR lpszNewValue);
	afx_msg BOOL GetUseVersionInfo();
	afx_msg void SetUseVersionInfo(BOOL bNewValue);
	afx_msg BSTR ShowLoginDialog();
	afx_msg BSTR DoLoginSilentMode(LPCTSTR strUserName, LPCTSTR strUserPassword);
	afx_msg BSTR GetPrivileges(LPCTSTR strItem);
	afx_msg BSTR GetUserName();
	afx_msg BSTR GetUserPassword();
	afx_msg BOOL WriteErrToLog(LPCTSTR sFileName, LPCTSTR sFunction, LPDISPATCH FAR* ErrObj);
	afx_msg BSTR GetPrivilegList();
	afx_msg BSTR GetWorkstationName();
	afx_msg BSTR GetRealWorkstationName();
	//}}AFX_DISPATCH
	DECLARE_DISPATCH_MAP()

	afx_msg void AboutBox();

// Event maps
	//{{AFX_EVENT(CAatLoginCtrl)
	//}}AFX_EVENT
	DECLARE_EVENT_MAP()

// Dispatch and event IDs
public:
	CString HostType();
	CString HostName();
	CString TableExtension();
	CString GetHomeAirport();
	BOOL	GetCedaIsConnected();
	void	SetCedaIsConnected(BOOL blConnected);
	CMapStringToString	*GetStatusMap();	
	CString	GetLoginResult();
	void	SetLoginResult(const CString& ropString);
	void	DrawLifeStyle(CDC *pdc, CRect opRect, int MyColor, int ipMode, BOOL DrawDown, int ipPercent);
	CString	GetTwe();
	bool GetWarning(CString &opApp, CString &opCustomer);

	int imLoginAttempts;

	enum {
	//{{AFX_DISP_ID(CAatLoginCtrl)
	dispidRegisterApplicationString = 1L,
	dispidApplicationName = 2L,
	dispidLoginAttempts = 3L,
	dispidVersionString = 4L,
	dispidInfoButtonVisible = 5L,
	dispidInfoUfisVersion = 6L,
	dispidInfoCopyright = 7L,
	dispidInfoAAT = 8L,
	dispidInfoCaption = 9L,
	dispidUfisComCtrl = 10L,
	dispidInfoAppVersion = 11L,
	dispidUserNameLCase = 12L,
	dispidVersion = 13L,
	dispidBuildDate = 14L,
	dispidUseVersionInfo = 15L,
	dispidShowLoginDialog = 16L,
	dispidDoLoginSilentMode = 17L,
	dispidGetPrivileges = 18L,
	dispidGetUserName = 19L,
	dispidGetUserPassword = 20L,
	dispidWriteErrToLog = 21L,
	dispidGetPrivilegList = 22L,
	//}}AFX_DISP_ID
	};


private:
	BOOL		SetVersionInfo();
private:
	char		pcmConfigPath[512];
	CDummyDlg	*pomDummyDlg;
	IDispatch	*pomUfisCom;
	CString		omRegisterApplicationString;
	CString		omApplicationName;
	short		omLoginAttempts;
	CString		omVersionString;
	BOOL		bmInfoButtonVisible;
	CString		omUfisVersion;
	CString		omInfoCopyright;
	CString		omInfoAAT;
	CString		omInfoCaption;
	CString		omInfoAppVersion;
	BOOL		bmUserNameLCase;
	CString		omVersion;
	CString		omBuildDate;
	BOOL		bmUseVersionInfo;
	BOOL		bmCedaIsConnected;
	CMapStringToString	omStatusMap;
	CString		omLoginResult;
	CString		omUserName;
	CString		omPassword;
	CString		omTwe;
	CString		omInfoAppName;

	friend class CRegisterDlg;
	friend class CLoginDlg;
	friend class CInfoDlg;
	friend class CPasswordDlg;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_AATLOGINCTL_H__961A2B4B_A86A_43DA_A374_324BA73B018F__INCLUDED)
