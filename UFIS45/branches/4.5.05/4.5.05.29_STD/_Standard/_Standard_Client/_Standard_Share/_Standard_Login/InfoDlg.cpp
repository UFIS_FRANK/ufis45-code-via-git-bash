// InfoDlg.cpp : implementation file
//

#include "stdafx.h"
#include "AatLogin.h"
#include "InfoDlg.h"
#include "AAtLoginCtl.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CInfoDlg dialog

CInfoDlg::CInfoDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CInfoDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CInfoDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	this->pomLoginCtrl = (CAatLoginCtrl *)pParent;
}

CInfoDlg::CInfoDlg(CWnd* pParent,CAatLoginCtrl *popLoginCtrl)
	: CDialog(CInfoDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CInfoDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	this->pomLoginCtrl = popLoginCtrl;
}


void CInfoDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CInfoDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	DDX_Control(pDX, IDC_UFIS_VERSION,m_LblUfisVersion);
	DDX_Control(pDX, IDC_APPLICATION,m_LblApplication);
	DDX_Control(pDX, IDC_COPYRIGHT,m_LblCopyright);
	DDX_Control(pDX, IDC_AAT,m_LblAat);
	DDX_Control(pDX, IDC_LABEL1,m_LblLabel1);
	DDX_Control(pDX, IDC_LABEL2,m_LblLabel2);
	DDX_Control(pDX, IDC_LABEL3,m_LblLabel3);
	DDX_Control(pDX, IDC_CONNECTION,m_LblConnection);
	DDX_Control(pDX, IDC_USER,m_LblUser);
	DDX_Control(pDX, IDC_LOGINTIME,m_LblNumberLogin);
	DDX_Control(pDX, IDOK,m_BtnOk);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CInfoDlg, CDialog)
	//{{AFX_MSG_MAP(CInfoDlg)
	ON_WM_PAINT()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CInfoDlg message handlers

BOOL CInfoDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
    this->SetWindowText(pomLoginCtrl->omInfoCaption);

    this->m_LblLabel1.SetWindowText(LoadStg(IDS_STRING1036));
    this->m_LblLabel2.SetWindowText(LoadStg(IDS_STRING1037));
    this->m_LblLabel3.SetWindowText(LoadStg(IDS_STRING1038));

    this->m_LblUfisVersion.SetWindowText(pomLoginCtrl->omUfisVersion);
	this->m_LblApplication.SetWindowText(pomLoginCtrl->omInfoAppVersion);
	this->m_LblAat.SetWindowText(pomLoginCtrl->omInfoAAT);
	this->m_LblCopyright.SetWindowText(pomLoginCtrl->omInfoCopyright);

    CString olConnection = pomLoginCtrl->HostName() + " / " + pomLoginCtrl->HostType();
	this->m_LblConnection.SetWindowText(olConnection);

    this->m_LblUser.SetWindowText("XXX");
    this->m_LblNumberLogin.SetWindowText("");
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CInfoDlg::OnPaint() 
{
	// TODO: Add your message handler code here
	if (pomLoginCtrl != NULL)
	{
		CPaintDC dc(this); // device context for painting
		CRect olRect;
		GetClientRect( &olRect );
		pomLoginCtrl->DrawLifeStyle(&dc,olRect,7,0,true,70);
	}
}
