// AatLoginPpg.cpp : Implementation of the CAatLoginPropPage property page class.

#include "stdafx.h"
#include "AatLogin.h"
#include "AatLoginPpg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


IMPLEMENT_DYNCREATE(CAatLoginPropPage, COlePropertyPage)


/////////////////////////////////////////////////////////////////////////////
// Message map

BEGIN_MESSAGE_MAP(CAatLoginPropPage, COlePropertyPage)
	//{{AFX_MSG_MAP(CAatLoginPropPage)
	// NOTE - ClassWizard will add and remove message map entries
	//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// Initialize class factory and guid

IMPLEMENT_OLECREATE_EX(CAatLoginPropPage, "AATLOGIN.AatLoginPropPage.1",
	0xb2a39d88, 0xbe94, 0x4b3c, 0xa9, 0x97, 0x35, 0xf9, 0xf7, 0x98, 0x77, 0x65)


/////////////////////////////////////////////////////////////////////////////
// CAatLoginPropPage::CAatLoginPropPageFactory::UpdateRegistry -
// Adds or removes system registry entries for CAatLoginPropPage

BOOL CAatLoginPropPage::CAatLoginPropPageFactory::UpdateRegistry(BOOL bRegister)
{
	if (bRegister)
		return AfxOleRegisterPropertyPageClass(AfxGetInstanceHandle(),
			m_clsid, IDS_AATLOGIN_PPG);
	else
		return AfxOleUnregisterClass(m_clsid, NULL);
}


/////////////////////////////////////////////////////////////////////////////
// CAatLoginPropPage::CAatLoginPropPage - Constructor

CAatLoginPropPage::CAatLoginPropPage() :
	COlePropertyPage(IDD, IDS_AATLOGIN_PPG_CAPTION)
{
	//{{AFX_DATA_INIT(CAatLoginPropPage)
	// NOTE: ClassWizard will add member initialization here
	//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_DATA_INIT
}


/////////////////////////////////////////////////////////////////////////////
// CAatLoginPropPage::DoDataExchange - Moves data between page and properties

void CAatLoginPropPage::DoDataExchange(CDataExchange* pDX)
{
	//{{AFX_DATA_MAP(CAatLoginPropPage)
	// NOTE: ClassWizard will add DDP, DDX, and DDV calls here
	//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_DATA_MAP
	DDP_PostProcessing(pDX);
}


/////////////////////////////////////////////////////////////////////////////
// CAatLoginPropPage message handlers
