// RegisterDlg.cpp : implementation file
//

#include "stdafx.h"
#include "AatLogin.h"
#include "RegisterDlg.h"
#include "AatLoginCtl.h"
#include <DUfisCom.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CRegisterDlg dialog


CRegisterDlg::CRegisterDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CRegisterDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CRegisterDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	this->pomLoginCtrl = (CAatLoginCtrl *)pParent;
}


void CRegisterDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CRegisterDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	DDX_Control(pDX, IDC_TIME,		m_TxtTime);
	DDX_Control(pDX, IDC_TEXT,		m_TxtText);
	DDX_Control(pDX, IDC_ABORT,		m_BtnAbort);
	DDX_Control(pDX, IDC_START,		m_BtnStart);
	DDX_Control(pDX, IDC_REGISTER,	m_BtnRegister);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CRegisterDlg, CDialog)
	//{{AFX_MSG_MAP(CRegisterDlg)
		// NOTE: the ClassWizard will add message map macros here
	ON_BN_CLICKED(IDC_ABORT,	OnAbort)
	ON_BN_CLICKED(IDC_REGISTER, OnRegister)
	ON_BN_CLICKED(IDC_START,	OnStart)
    ON_WM_TIMER()
	ON_WM_PAINT()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CRegisterDlg message handlers

BOOL CRegisterDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	CWnd *polWnd = GetDlgItem(IDC_START);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING1010));
	}

	polWnd = GetDlgItem(IDC_ABORT);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING1011));
	}

	polWnd = GetDlgItem(IDC_REGISTER);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING1012));
	}

	CString olCaptionText;
	CString	olCaptionTime;

#if	0
	// is the application registered yet?
    if (this->pomLoginCtrl->GetStatusMap()->GetCount() == 1)		// ..no, it isn't
	{
		this->m_BtnStart.ShowWindow(SW_HIDE);

		olCaptionText.Format(LoadStg(IDS_STRING1014),this->pomLoginCtrl->omApplicationName);

		olCaptionTime = "";
	}
	else if (this->pomLoginCtrl->GetStatusMap()->GetCount() > 1)	// ...yes, it is
	{
		olCaptionText.Format(LoadStg(IDS_STRING1015),this->pomLoginCtrl->omApplicationName);

		olCaptionTime.Format(LoadStg(IDS_STRING1013),15);

		SetTimer(4711,1000,NULL);
		imTimer = 15;

	}
#else
		olCaptionText.Format(LoadStg(IDS_STRING1015),this->pomLoginCtrl->omApplicationName);

		olCaptionTime.Format(LoadStg(IDS_STRING1013),15);

		SetTimer(4711,1000,NULL);
		imTimer = 15;
#endif
    this->SetWindowText(this->pomLoginCtrl->omApplicationName);

    this->m_TxtText.SetWindowText(olCaptionText);
    this->m_TxtTime.SetWindowText(olCaptionTime);

	return TRUE;  
}

//---------------------------------------------------------------------------

void CRegisterDlg::OnAbort() 
{
	KillTimer(4711);
	this->pomLoginCtrl->SetLoginResult("CANCEL");
	CDialog::OnCancel();
}

//---------------------------------------------------------------------------

void CRegisterDlg::OnRegister() 
{
	KillTimer(4711);

	CString olFmt;
	olFmt.Format(LoadStg(IDS_STRING1016),pomLoginCtrl->omApplicationName);
	m_TxtTime.SetWindowText(olFmt);
	m_TxtTime.InvalidateRect(NULL);
	m_TxtTime.UpdateWindow();

	CWaitCursor olWait;
	m_BtnAbort.EnableWindow(FALSE);	
	m_BtnStart.EnableWindow(FALSE);	
	m_BtnRegister.EnableWindow(FALSE);

	
	_DUfisCom olUfisCom;
	olUfisCom.AttachDispatch(pomLoginCtrl->GetUfisComCtrl(),TRUE);

	
    olUfisCom.SetTwe(pomLoginCtrl->GetTwe());

    if (olUfisCom.CallServer("SMI",pomLoginCtrl->GetHomeAirport(), "APPL,SUBD,FUNC,FUAL,TYPE,STAT",pomLoginCtrl->omRegisterApplicationString, "", "360") == 0)
	{

	}
	else
	{
		MessageBox(olUfisCom.GetLastErrorMessage(),LoadStg(IDS_ERRORCAPTION),MB_ICONSTOP);
	}

	this->pomLoginCtrl->SetLoginResult("CANCEL");
	CDialog::OnCancel();
}

//---------------------------------------------------------------------------

void CRegisterDlg::OnStart() 
{
	KillTimer(4711);
	this->pomLoginCtrl->SetLoginResult("OK");
	CDialog::OnOK();
}

//---------------------------------------------------------------------------
void CRegisterDlg::OnTimer(UINT nIDEvent)
{
	CDialog::OnTimer(nIDEvent);
	CString olTmpTime;
	imTimer--;
	if(imTimer == 0)
	{
		KillTimer(4711);
		OnStart();
	}
	else
	{
		olTmpTime.Format(LoadStg(IDS_STRING1013),imTimer);
		m_TxtTime.SetWindowText(olTmpTime);
	}
}

void CRegisterDlg::OnPaint() 
{
	// TODO: Add your message handler code here
	if (pomLoginCtrl != NULL)
	{
		CPaintDC dc(this); // device context for painting
		CRect olRect;
		GetClientRect( &olRect );
		pomLoginCtrl->DrawLifeStyle(&dc,olRect,7,0,true,70);
	}
}

