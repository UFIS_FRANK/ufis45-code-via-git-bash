// Machine generated IDispatch wrapper class(es) created with ClassWizard
/////////////////////////////////////////////////////////////////////////////
// _DUCom wrapper class

class _DUCom : public COleDispatchDriver
{
public:
	_DUCom() {}		// Calls COleDispatchDriver default constructor
	_DUCom(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	_DUCom(const _DUCom& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:
	CString GetApplicationName();
	void SetApplicationName(LPCTSTR);
	CString GetPort();
	void SetPort(LPCTSTR);
	CString GetHopo();
	void SetHopo(LPCTSTR);
	CString GetServerName();
	void SetServerName(LPCTSTR);
	CString GetTableExtension();
	void SetTableExtension(LPCTSTR);
	CString GetUserName();
	void SetUserName(LPCTSTR);
	CString GetWorkstationName();
	void SetWorkstationName(LPCTSTR);
	CString GetSendTimeoutSeconds();
	void SetSendTimeoutSeconds(LPCTSTR);
	CString GetReceiveTimeoutSeconds();
	void SetReceiveTimeoutSeconds(LPCTSTR);
	CString GetRecordSeparator();
	void SetRecordSeparator(LPCTSTR);
	CString GetCedaIdentifier();
	void SetCedaIdentifier(LPCTSTR);
	CString GetVersion();
	void SetVersion(LPCTSTR);
	CString GetBuildDate();
	void SetBuildDate(LPCTSTR);
	CString GetSQLAccessMethod();
	void SetSQLAccessMethod(LPCTSTR);
	CString GetSQLConnectionString();
	void SetSQLConnectionString(LPCTSTR);
	long GetPacketSize();
	void SetPacketSize(long);
	CString GetErrorSimulation();
	void SetErrorSimulation(LPCTSTR);

// Operations
public:
	CString GetLastError();
	BOOL CedaAction(LPCTSTR Command, LPCTSTR DBTable, LPCTSTR DBFields, LPCTSTR DBData, LPCTSTR DBWhere);
	long GetBufferCount();
	CString GetRecord(long RecordNo);
};
/////////////////////////////////////////////////////////////////////////////
// _DUComEvents wrapper class

class _DUComEvents : public COleDispatchDriver
{
public:
	_DUComEvents() {}		// Calls COleDispatchDriver default constructor
	_DUComEvents(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	_DUComEvents(const _DUComEvents& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
	void PackageReceived(long ipPackage);
};
