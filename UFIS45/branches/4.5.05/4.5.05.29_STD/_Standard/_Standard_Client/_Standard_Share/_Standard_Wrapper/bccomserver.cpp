// Machine generated IDispatch wrapper class(es) created with ClassWizard

#include "stdafx.h"
#include "bccomserver.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif



/////////////////////////////////////////////////////////////////////////////
// _IBcComAtlEvents properties

/////////////////////////////////////////////////////////////////////////////
// _IBcComAtlEvents operations


/////////////////////////////////////////////////////////////////////////////
// IBcComAtl properties

/////////////////////////////////////////////////////////////////////////////
// IBcComAtl operations

void IBcComAtl::SetPID(LPCTSTR strAppl, LPCTSTR strPID)
{
	static BYTE parms[] =
		VTS_BSTR VTS_BSTR;
	InvokeHelper(0x1, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 strAppl, strPID);
}

void IBcComAtl::SetFilter(LPCTSTR strTable, LPCTSTR strCommand, long bOwnBC, LPCTSTR application)
{
	static BYTE parms[] =
		VTS_BSTR VTS_BSTR VTS_I4 VTS_BSTR;
	InvokeHelper(0x2, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 strTable, strCommand, bOwnBC, application);
}

void IBcComAtl::TransmitFilter()
{
	InvokeHelper(0x3, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IBcComAtl::StartBroadcasting()
{
	InvokeHelper(0x4, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IBcComAtl::StopBroadcasting()
{
	InvokeHelper(0x5, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}
