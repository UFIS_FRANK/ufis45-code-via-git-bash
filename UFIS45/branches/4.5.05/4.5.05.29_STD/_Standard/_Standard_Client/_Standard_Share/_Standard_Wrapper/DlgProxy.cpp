// DlgProxy.cpp : implementation file
//

#include "stdafx.h"
#include "_Standard_Wrapper.h"
#include "DlgProxy.h"
#include "_Standard_WrapperDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// C_Standard_WrapperDlgAutoProxy

IMPLEMENT_DYNCREATE(C_Standard_WrapperDlgAutoProxy, CCmdTarget)

C_Standard_WrapperDlgAutoProxy::C_Standard_WrapperDlgAutoProxy()
{
	EnableAutomation();
	
	// To keep the application running as long as an automation 
	//	object is active, the constructor calls AfxOleLockApp.
	AfxOleLockApp();

	// Get access to the dialog through the application's
	//  main window pointer.  Set the proxy's internal pointer
	//  to point to the dialog, and set the dialog's back pointer to
	//  this proxy.
	ASSERT (AfxGetApp()->m_pMainWnd != NULL);
	ASSERT_VALID (AfxGetApp()->m_pMainWnd);
	ASSERT_KINDOF(C_Standard_WrapperDlg, AfxGetApp()->m_pMainWnd);
	m_pDialog = (C_Standard_WrapperDlg*) AfxGetApp()->m_pMainWnd;
	m_pDialog->m_pAutoProxy = this;
}

C_Standard_WrapperDlgAutoProxy::~C_Standard_WrapperDlgAutoProxy()
{
	// To terminate the application when all objects created with
	// 	with automation, the destructor calls AfxOleUnlockApp.
	//  Among other things, this will destroy the main dialog
	if (m_pDialog != NULL)
		m_pDialog->m_pAutoProxy = NULL;
	AfxOleUnlockApp();
}

void C_Standard_WrapperDlgAutoProxy::OnFinalRelease()
{
	// When the last reference for an automation object is released
	// OnFinalRelease is called.  The base class will automatically
	// deletes the object.  Add additional cleanup required for your
	// object before calling the base class.

	CCmdTarget::OnFinalRelease();
}

BEGIN_MESSAGE_MAP(C_Standard_WrapperDlgAutoProxy, CCmdTarget)
	//{{AFX_MSG_MAP(C_Standard_WrapperDlgAutoProxy)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

BEGIN_DISPATCH_MAP(C_Standard_WrapperDlgAutoProxy, CCmdTarget)
	//{{AFX_DISPATCH_MAP(C_Standard_WrapperDlgAutoProxy)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_DISPATCH_MAP
END_DISPATCH_MAP()

// Note: we add support for IID_I_Standard_Wrapper to support typesafe binding
//  from VBA.  This IID must match the GUID that is attached to the 
//  dispinterface in the .ODL file.

// {D71A84E9-65DA-4EC7-86FD-1FBF170727D8}
static const IID IID_I_Standard_Wrapper =
{ 0xd71a84e9, 0x65da, 0x4ec7, { 0x86, 0xfd, 0x1f, 0xbf, 0x17, 0x7, 0x27, 0xd8 } };

BEGIN_INTERFACE_MAP(C_Standard_WrapperDlgAutoProxy, CCmdTarget)
	INTERFACE_PART(C_Standard_WrapperDlgAutoProxy, IID_I_Standard_Wrapper, Dispatch)
END_INTERFACE_MAP()

// The IMPLEMENT_OLECREATE2 macro is defined in StdAfx.h of this project
// {EE9068A0-EBDF-4887-8C67-2B8D8D63E319}
IMPLEMENT_OLECREATE2(C_Standard_WrapperDlgAutoProxy, "_Standard_Wrapper.Application", 0xee9068a0, 0xebdf, 0x4887, 0x8c, 0x67, 0x2b, 0x8d, 0x8d, 0x63, 0xe3, 0x19)

/////////////////////////////////////////////////////////////////////////////
// C_Standard_WrapperDlgAutoProxy message handlers
