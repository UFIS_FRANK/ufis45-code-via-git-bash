using System;
using System.IO;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Text;
using Microsoft.Win32;
using Ufis.Utils;
using Ufis.Data;

namespace Data_Changes
{
    /// <summary>
    /// Summary description for frmView.
    /// </summary>
    public class frmView : System.Windows.Forms.Form
    {
        #region ----- myMembers

        IDatabase myDB = null;
        string myEndSign = "'";
        //		string myEndSignLike = "%'";
        public string mySelection = "";
        public string mySelectionDeleted = "";
        public string myWindowText = "";
        public ArrayList myLocation = new ArrayList();
        //		string mySelRotation = " [ROTATIONS]";
        string mySelFlnu = " AND FLTN = '";
        string mySelFlns = " AND FLNS = '";
        string mySelAlc2 = " AND ALC2 = '";
        string mySelAlc3 = " AND ALC3 = '";
        string mySelAct3 = " AND ACT3 = '";
        string mySelAct5 = " AND ACT5 = '";
        string mySelTtyp = " AND TTYP = '";
        string mySelStyp = " AND STYP = '";
        string mySelRegn = " AND REGN = '";
        string mySelCsgn = " AND CSGN = '";
        public string omSelectedTabPage = "Flight Data";
        string[] arrFieldCodes;
        private bool bFirstTime = true;

        string mySaveFile = "C:\\Ufis\\System\\DATACHANGE_VIEW.txt";
        public static frmView myThis;
        private bool m_ACRloaded = false;
        public bool m_CSGNloaded = false;

        //Common CKI related members
        string strFileNameCKI = "C:\\Ufis\\System\\DATACHANGE_CKI.txt";
        public string m_selCKI = "";

        #endregion ----- myMembers

        private System.Windows.Forms.ImageList imageList2;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.ImageList imageButtons;
        private System.Windows.Forms.PictureBox pictureBoxButton;
        private System.Windows.Forms.CheckBox cbUTC;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnNew;
        private System.Windows.Forms.Button btnApply;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.ComponentModel.IContainer components;
        private ITable omTabTab = null; //nr
        private ITable omSysTab = null; //nr
        public string omTableCode = "";

        private string[] arrTableNames;
        private System.Windows.Forms.TextBox txtACTUrno;
        private System.Windows.Forms.TabPage FlightData;
        public System.Windows.Forms.RadioButton rbDeletedFlights;
        public System.Windows.Forms.RadioButton rbChanged;
        public System.Windows.Forms.RadioButton rbFilterButtons;
        private System.Windows.Forms.CheckBox cbRunway;
        private System.Windows.Forms.CheckBox cbExit;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox cbPosition;
        private System.Windows.Forms.CheckBox cbCheckin;
        private System.Windows.Forms.CheckBox cbLounge;
        private System.Windows.Forms.CheckBox cbBelt;
        private System.Windows.Forms.CheckBox cbGate;
        public System.Windows.Forms.CheckBox cbTakeOver;
        private System.Windows.Forms.Button btnNow;
        public System.Windows.Forms.DateTimePicker dateTimePickerTo;
        private System.Windows.Forms.Label lblTo;
        public System.Windows.Forms.DateTimePicker dateTimePickerFrom;
        private System.Windows.Forms.Label lblFrom;
        private System.Windows.Forms.CheckBox cbArrival;
        private System.Windows.Forms.TextBox txtCallSignValue;
        private System.Windows.Forms.TextBox txtACT5Value;
        private System.Windows.Forms.TextBox txtALT3Value;
        private System.Windows.Forms.TextBox txtFLNUValue;
        private System.Windows.Forms.TextBox txtFLTSValue;
        private System.Windows.Forms.TextBox txtREGNValue;
        private System.Windows.Forms.TextBox txtALTValue;
        private System.Windows.Forms.TextBox txtSTYValue;
        private System.Windows.Forms.TextBox txtNATValue;
        private System.Windows.Forms.TextBox txtACTValue;
        private System.Windows.Forms.Label lblCallSign;
        private System.Windows.Forms.CheckBox cbDeparture;
        private System.Windows.Forms.Button btnACR;
        private System.Windows.Forms.Button btnSTY;
        private System.Windows.Forms.Button btnNAT;
        private System.Windows.Forms.Button btnALC;
        private System.Windows.Forms.Label lblREGN;
        private System.Windows.Forms.Label lblSTY;
        private System.Windows.Forms.Label lblNAT;
        private System.Windows.Forms.Label lblFLNU;
        private System.Windows.Forms.Label lblALC;
        private System.Windows.Forms.Button btnACT;
        private System.Windows.Forms.Label lblACT;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.RadioButton radioButton1;
        public System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.Button button5;
        public System.Windows.Forms.RadioButton radioButton3;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.Label label12; 
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.RadioButton radioButton4;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.TabPage BasicData;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.ComboBox cbTable;
        public System.Windows.Forms.CheckBox cbTakeOver_BD;
        private System.Windows.Forms.Button button7;
        public System.Windows.Forms.DateTimePicker dateTimePickerTo_BD;
        private System.Windows.Forms.Label label16;
        public System.Windows.Forms.DateTimePicker dateTimePickerFrm_BD;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.TabControl tabControl1;
        private CheckBox cbTowing;
        private TabPage CCAData;
        private DateTimePicker dtpOpenTo;
        private DateTimePicker dtpOpenFrom;
        private Label label21;
        private Label label19;
        private TextBox txtCounter;
        private DateTimePicker dtpCloseTo;
        private DateTimePicker dtpCloseFrom;
        private PictureBox pictureBox4;
        private GroupBox groupBox2;
        private Label label23;
        private Label label20;
        private GroupBox groupBox1;
        private Label label22;
        // Array of Table Names
        private string[] arrTableCodes;

        public bool IsTowing
        {
            get { return cbTowing.Checked; }
        }

        public bool IsArrival
        {
            get { return cbArrival.Checked; }
        }

        public bool IsDeparture
        {
            get { return cbDeparture.Checked; }
        }

        public frmView()
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();
            myThis = this;
            //
            // TODO: Add any constructor code after InitializeComponent call
            //

            Ufis.Utils.IniFile myIni = new IniFile("C:\\Ufis\\system\\DATACHANGE.INI");

            string strTables = myIni.IniReadValue("BASIC_DATA", "TABLES");

            if (strTables == "")
            {
                this.tabControl1.TabPages.Remove(BasicData);
            }

            string strPath = myIni.path;

            if (strPath.Length > 0)
            {
                //string mySaveFile = "C:\\Ufis\\System\\DATACHANGE_VIEW.txt";
                mySaveFile = strPath.Substring(0, 2) + "\\Ufis\\System\\DATACHANGE_VIEW.txt";
                //string strFileNameCKI = "C:\\Ufis\\System\\DATACHANGE_CKI.txt";
                strFileNameCKI = strPath.Substring(0, 2) +  "\\Ufis\\System\\DATACHANGE_CKI.txt";

            }

            //Check for CKI TAB
            //string strTables = myIni.IniReadValue("BASIC_DATA", "TABLES");


        }

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        public static frmView GetThis()
        {
            return myThis;
        }

        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmView));
            this.imageList2 = new System.Windows.Forms.ImageList(this.components);
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.imageButtons = new System.Windows.Forms.ImageList(this.components);
            this.pictureBoxButton = new System.Windows.Forms.PictureBox();
            this.cbUTC = new System.Windows.Forms.CheckBox();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnNew = new System.Windows.Forms.Button();
            this.btnApply = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.txtACTUrno = new System.Windows.Forms.TextBox();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.cbRunway = new System.Windows.Forms.CheckBox();
            this.cbExit = new System.Windows.Forms.CheckBox();
            this.cbPosition = new System.Windows.Forms.CheckBox();
            this.cbCheckin = new System.Windows.Forms.CheckBox();
            this.cbLounge = new System.Windows.Forms.CheckBox();
            this.cbBelt = new System.Windows.Forms.CheckBox();
            this.cbGate = new System.Windows.Forms.CheckBox();
            this.btnNow = new System.Windows.Forms.Button();
            this.btnACR = new System.Windows.Forms.Button();
            this.btnSTY = new System.Windows.Forms.Button();
            this.btnNAT = new System.Windows.Forms.Button();
            this.btnALC = new System.Windows.Forms.Button();
            this.btnACT = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.FlightData = new System.Windows.Forms.TabPage();
            this.cbTowing = new System.Windows.Forms.CheckBox();
            this.rbDeletedFlights = new System.Windows.Forms.RadioButton();
            this.rbChanged = new System.Windows.Forms.RadioButton();
            this.rbFilterButtons = new System.Windows.Forms.RadioButton();
            this.label1 = new System.Windows.Forms.Label();
            this.cbTakeOver = new System.Windows.Forms.CheckBox();
            this.dateTimePickerTo = new System.Windows.Forms.DateTimePicker();
            this.lblTo = new System.Windows.Forms.Label();
            this.dateTimePickerFrom = new System.Windows.Forms.DateTimePicker();
            this.lblFrom = new System.Windows.Forms.Label();
            this.cbArrival = new System.Windows.Forms.CheckBox();
            this.txtCallSignValue = new System.Windows.Forms.TextBox();
            this.txtACT5Value = new System.Windows.Forms.TextBox();
            this.txtALT3Value = new System.Windows.Forms.TextBox();
            this.txtFLNUValue = new System.Windows.Forms.TextBox();
            this.txtFLTSValue = new System.Windows.Forms.TextBox();
            this.txtREGNValue = new System.Windows.Forms.TextBox();
            this.txtALTValue = new System.Windows.Forms.TextBox();
            this.txtSTYValue = new System.Windows.Forms.TextBox();
            this.txtNATValue = new System.Windows.Forms.TextBox();
            this.txtACTValue = new System.Windows.Forms.TextBox();
            this.lblCallSign = new System.Windows.Forms.Label();
            this.cbDeparture = new System.Windows.Forms.CheckBox();
            this.lblREGN = new System.Windows.Forms.Label();
            this.lblSTY = new System.Windows.Forms.Label();
            this.lblNAT = new System.Windows.Forms.Label();
            this.lblFLNU = new System.Windows.Forms.Label();
            this.lblALC = new System.Windows.Forms.Label();
            this.lblACT = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.label11 = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.radioButton3 = new System.Windows.Forms.RadioButton();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.radioButton4 = new System.Windows.Forms.RadioButton();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.BasicData = new System.Windows.Forms.TabPage();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label18 = new System.Windows.Forms.Label();
            this.cbTable = new System.Windows.Forms.ComboBox();
            this.cbTakeOver_BD = new System.Windows.Forms.CheckBox();
            this.dateTimePickerTo_BD = new System.Windows.Forms.DateTimePicker();
            this.label16 = new System.Windows.Forms.Label();
            this.dateTimePickerFrm_BD = new System.Windows.Forms.DateTimePicker();
            this.label17 = new System.Windows.Forms.Label();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.CCAData = new System.Windows.Forms.TabPage();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label23 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.dtpCloseTo = new System.Windows.Forms.DateTimePicker();
            this.dtpCloseFrom = new System.Windows.Forms.DateTimePicker();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label22 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.dtpOpenTo = new System.Windows.Forms.DateTimePicker();
            this.dtpOpenFrom = new System.Windows.Forms.DateTimePicker();
            this.txtCounter = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxButton)).BeginInit();
            this.FlightData.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.BasicData.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.CCAData.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            this.SuspendLayout();
            // 
            // imageList2
            // 
            this.imageList2.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList2.ImageStream")));
            this.imageList2.TransparentColor = System.Drawing.Color.White;
            this.imageList2.Images.SetKeyName(0, "");
            this.imageList2.Images.SetKeyName(1, "");
            this.imageList2.Images.SetKeyName(2, "");
            this.imageList2.Images.SetKeyName(3, "");
            this.imageList2.Images.SetKeyName(4, "");
            this.imageList2.Images.SetKeyName(5, "");
            this.imageList2.Images.SetKeyName(6, "");
            this.imageList2.Images.SetKeyName(7, "");
            this.imageList2.Images.SetKeyName(8, "");
            this.imageList2.Images.SetKeyName(9, "");
            this.imageList2.Images.SetKeyName(10, "");
            this.imageList2.Images.SetKeyName(11, "");
            this.imageList2.Images.SetKeyName(12, "");
            this.imageList2.Images.SetKeyName(13, "");
            this.imageList2.Images.SetKeyName(14, "");
            this.imageList2.Images.SetKeyName(15, "");
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.White;
            this.imageList1.Images.SetKeyName(0, "");
            this.imageList1.Images.SetKeyName(1, "");
            this.imageList1.Images.SetKeyName(2, "");
            this.imageList1.Images.SetKeyName(3, "");
            this.imageList1.Images.SetKeyName(4, "");
            this.imageList1.Images.SetKeyName(5, "");
            this.imageList1.Images.SetKeyName(6, "");
            this.imageList1.Images.SetKeyName(7, "");
            this.imageList1.Images.SetKeyName(8, "");
            this.imageList1.Images.SetKeyName(9, "");
            this.imageList1.Images.SetKeyName(10, "");
            this.imageList1.Images.SetKeyName(11, "");
            this.imageList1.Images.SetKeyName(12, "");
            this.imageList1.Images.SetKeyName(13, "");
            this.imageList1.Images.SetKeyName(14, "");
            this.imageList1.Images.SetKeyName(15, "");
            this.imageList1.Images.SetKeyName(16, "");
            // 
            // imageButtons
            // 
            this.imageButtons.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageButtons.ImageStream")));
            this.imageButtons.TransparentColor = System.Drawing.Color.White;
            this.imageButtons.Images.SetKeyName(0, "");
            this.imageButtons.Images.SetKeyName(1, "");
            this.imageButtons.Images.SetKeyName(2, "");
            this.imageButtons.Images.SetKeyName(3, "");
            this.imageButtons.Images.SetKeyName(4, "");
            this.imageButtons.Images.SetKeyName(5, "");
            // 
            // pictureBoxButton
            // 
            this.pictureBoxButton.BackColor = System.Drawing.Color.Transparent;
            this.pictureBoxButton.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBoxButton.Location = new System.Drawing.Point(0, 0);
            this.pictureBoxButton.Name = "pictureBoxButton";
            this.pictureBoxButton.Size = new System.Drawing.Size(512, 36);
            this.pictureBoxButton.TabIndex = 98;
            this.pictureBoxButton.TabStop = false;
            this.pictureBoxButton.Paint += new System.Windows.Forms.PaintEventHandler(this.pictureBoxButton_Paint);
            // 
            // cbUTC
            // 
            this.cbUTC.Appearance = System.Windows.Forms.Appearance.Button;
            this.cbUTC.BackColor = System.Drawing.Color.Transparent;
            this.cbUTC.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbUTC.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cbUTC.ImageIndex = 16;
            this.cbUTC.ImageList = this.imageList1;
            this.cbUTC.Location = new System.Drawing.Point(0, -1);
            this.cbUTC.Name = "cbUTC";
            this.cbUTC.Size = new System.Drawing.Size(80, 36);
            this.cbUTC.TabIndex = 12;
            this.cbUTC.TabStop = false;
            this.cbUTC.Text = "&UTC";
            this.cbUTC.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.toolTip1.SetToolTip(this.cbUTC, "Switch to UTC/local times");
            this.cbUTC.UseVisualStyleBackColor = false;
            this.cbUTC.CheckedChanged += new System.EventHandler(this.cbUTC_CheckedChanged);
            // 
            // btnClose
            // 
            this.btnClose.BackColor = System.Drawing.Color.Transparent;
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnClose.ImageIndex = 0;
            this.btnClose.ImageList = this.imageButtons;
            this.btnClose.Location = new System.Drawing.Point(320, -1);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(80, 36);
            this.btnClose.TabIndex = 16;
            this.btnClose.TabStop = false;
            this.btnClose.Text = "&Close";
            this.toolTip1.SetToolTip(this.btnClose, "Close the view window");
            this.btnClose.UseVisualStyleBackColor = false;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnNew
            // 
            this.btnNew.BackColor = System.Drawing.Color.Transparent;
            this.btnNew.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNew.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnNew.ImageIndex = 3;
            this.btnNew.ImageList = this.imageButtons;
            this.btnNew.Location = new System.Drawing.Point(240, -1);
            this.btnNew.Name = "btnNew";
            this.btnNew.Size = new System.Drawing.Size(80, 36);
            this.btnNew.TabIndex = 15;
            this.btnNew.TabStop = false;
            this.btnNew.Text = "&New";
            this.toolTip1.SetToolTip(this.btnNew, "Define a new current view");
            this.btnNew.UseVisualStyleBackColor = false;
            this.btnNew.Click += new System.EventHandler(this.btnNew_Click);
            // 
            // btnApply
            // 
            this.btnApply.BackColor = System.Drawing.Color.Transparent;
            this.btnApply.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnApply.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnApply.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnApply.ImageIndex = 1;
            this.btnApply.ImageList = this.imageButtons;
            this.btnApply.Location = new System.Drawing.Point(160, -1);
            this.btnApply.Name = "btnApply";
            this.btnApply.Size = new System.Drawing.Size(80, 36);
            this.btnApply.TabIndex = 14;
            this.btnApply.TabStop = false;
            this.btnApply.Text = "&Apply";
            this.toolTip1.SetToolTip(this.btnApply, "Apply the current view definition and load the data");
            this.btnApply.UseVisualStyleBackColor = false;
            this.btnApply.Click += new System.EventHandler(this.btnApply_Click);
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.Color.Transparent;
            this.btnSave.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSave.ImageIndex = 5;
            this.btnSave.ImageList = this.imageButtons;
            this.btnSave.Location = new System.Drawing.Point(80, -1);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(80, 36);
            this.btnSave.TabIndex = 13;
            this.btnSave.TabStop = false;
            this.btnSave.Text = "&Save";
            this.toolTip1.SetToolTip(this.btnSave, "Save the current view definition");
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // txtACTUrno
            // 
            this.txtACTUrno.Location = new System.Drawing.Point(392, 80);
            this.txtACTUrno.Name = "txtACTUrno";
            this.txtACTUrno.Size = new System.Drawing.Size(80, 20);
            this.txtACTUrno.TabIndex = 11;
            this.txtACTUrno.TabStop = false;
            this.txtACTUrno.Tag = "ACTU";
            this.txtACTUrno.Visible = false;
            this.txtACTUrno.TextChanged += new System.EventHandler(this.txtACTUrno_TextChanged);
            // 
            // cbRunway
            // 
            this.cbRunway.Appearance = System.Windows.Forms.Appearance.Button;
            this.cbRunway.BackColor = System.Drawing.Color.Transparent;
            this.cbRunway.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbRunway.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cbRunway.ImageIndex = 2;
            this.cbRunway.Location = new System.Drawing.Point(400, 16);
            this.cbRunway.Name = "cbRunway";
            this.cbRunway.Size = new System.Drawing.Size(80, 24);
            this.cbRunway.TabIndex = 176;
            this.cbRunway.TabStop = false;
            this.cbRunway.Text = "&Runway";
            this.cbRunway.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.toolTip1.SetToolTip(this.cbRunway, "Select/deselect the Check-in source");
            this.cbRunway.UseVisualStyleBackColor = false;
            this.cbRunway.CheckedChanged += new System.EventHandler(this.cbRunway_CheckedChanged);
            // 
            // cbExit
            // 
            this.cbExit.Appearance = System.Windows.Forms.Appearance.Button;
            this.cbExit.BackColor = System.Drawing.Color.Transparent;
            this.cbExit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbExit.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cbExit.ImageIndex = 2;
            this.cbExit.Location = new System.Drawing.Point(256, 16);
            this.cbExit.Name = "cbExit";
            this.cbExit.Size = new System.Drawing.Size(64, 24);
            this.cbExit.TabIndex = 175;
            this.cbExit.TabStop = false;
            this.cbExit.Text = "&Exit";
            this.cbExit.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.toolTip1.SetToolTip(this.cbExit, "Select/deselect the Lounge source");
            this.cbExit.UseVisualStyleBackColor = false;
            this.cbExit.CheckedChanged += new System.EventHandler(this.cbExit_CheckedChanged);
            // 
            // cbPosition
            // 
            this.cbPosition.Appearance = System.Windows.Forms.Appearance.Button;
            this.cbPosition.BackColor = System.Drawing.Color.Transparent;
            this.cbPosition.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbPosition.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cbPosition.ImageIndex = 2;
            this.cbPosition.Location = new System.Drawing.Point(0, 16);
            this.cbPosition.Name = "cbPosition";
            this.cbPosition.Size = new System.Drawing.Size(64, 24);
            this.cbPosition.TabIndex = 170;
            this.cbPosition.TabStop = false;
            this.cbPosition.Text = "&Position";
            this.cbPosition.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.toolTip1.SetToolTip(this.cbPosition, "Select/deselect the position source");
            this.cbPosition.UseVisualStyleBackColor = false;
            this.cbPosition.CheckedChanged += new System.EventHandler(this.cbPosition_CheckedChanged);
            // 
            // cbCheckin
            // 
            this.cbCheckin.Appearance = System.Windows.Forms.Appearance.Button;
            this.cbCheckin.BackColor = System.Drawing.Color.Transparent;
            this.cbCheckin.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbCheckin.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cbCheckin.ImageIndex = 2;
            this.cbCheckin.Location = new System.Drawing.Point(320, 16);
            this.cbCheckin.Name = "cbCheckin";
            this.cbCheckin.Size = new System.Drawing.Size(80, 24);
            this.cbCheckin.TabIndex = 174;
            this.cbCheckin.TabStop = false;
            this.cbCheckin.Text = "&Check-In";
            this.cbCheckin.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.toolTip1.SetToolTip(this.cbCheckin, "Select/deselect the Check-in source");
            this.cbCheckin.UseVisualStyleBackColor = false;
            this.cbCheckin.CheckedChanged += new System.EventHandler(this.cbCheckin_CheckedChanged);
            // 
            // cbLounge
            // 
            this.cbLounge.Appearance = System.Windows.Forms.Appearance.Button;
            this.cbLounge.BackColor = System.Drawing.Color.Transparent;
            this.cbLounge.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbLounge.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cbLounge.ImageIndex = 2;
            this.cbLounge.Location = new System.Drawing.Point(192, 16);
            this.cbLounge.Name = "cbLounge";
            this.cbLounge.Size = new System.Drawing.Size(64, 24);
            this.cbLounge.TabIndex = 173;
            this.cbLounge.TabStop = false;
            this.cbLounge.Text = "&Lounge";
            this.cbLounge.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.toolTip1.SetToolTip(this.cbLounge, "Select/deselect the Lounge source");
            this.cbLounge.UseVisualStyleBackColor = false;
            this.cbLounge.CheckedChanged += new System.EventHandler(this.cbLounge_CheckedChanged);
            // 
            // cbBelt
            // 
            this.cbBelt.Appearance = System.Windows.Forms.Appearance.Button;
            this.cbBelt.BackColor = System.Drawing.Color.Transparent;
            this.cbBelt.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbBelt.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cbBelt.ImageIndex = 2;
            this.cbBelt.Location = new System.Drawing.Point(128, 16);
            this.cbBelt.Name = "cbBelt";
            this.cbBelt.Size = new System.Drawing.Size(64, 24);
            this.cbBelt.TabIndex = 172;
            this.cbBelt.TabStop = false;
            this.cbBelt.Text = "&Baggage";
            this.cbBelt.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.toolTip1.SetToolTip(this.cbBelt, "Select/deselect the Baggage source");
            this.cbBelt.UseVisualStyleBackColor = false;
            this.cbBelt.CheckedChanged += new System.EventHandler(this.cbBelt_CheckedChanged);
            // 
            // cbGate
            // 
            this.cbGate.Appearance = System.Windows.Forms.Appearance.Button;
            this.cbGate.BackColor = System.Drawing.Color.Transparent;
            this.cbGate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbGate.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cbGate.ImageIndex = 2;
            this.cbGate.Location = new System.Drawing.Point(64, 16);
            this.cbGate.Name = "cbGate";
            this.cbGate.Size = new System.Drawing.Size(64, 24);
            this.cbGate.TabIndex = 171;
            this.cbGate.TabStop = false;
            this.cbGate.Text = "&Gate";
            this.cbGate.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.toolTip1.SetToolTip(this.cbGate, "Select/deselect the Gate source");
            this.cbGate.UseVisualStyleBackColor = false;
            this.cbGate.CheckedChanged += new System.EventHandler(this.cbGate_CheckedChanged);
            // 
            // btnNow
            // 
            this.btnNow.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnNow.ImageIndex = 4;
            this.btnNow.ImageList = this.imageList1;
            this.btnNow.Location = new System.Drawing.Point(264, 88);
            this.btnNow.Name = "btnNow";
            this.btnNow.Size = new System.Drawing.Size(20, 20);
            this.btnNow.TabIndex = 167;
            this.btnNow.TabStop = false;
            this.toolTip1.SetToolTip(this.btnNow, "Change the time to now");
            this.btnNow.Click += new System.EventHandler(this.btnNow_Click);
            // 
            // btnACR
            // 
            this.btnACR.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnACR.ImageIndex = 15;
            this.btnACR.ImageList = this.imageList2;
            this.btnACR.Location = new System.Drawing.Point(256, 408);
            this.btnACR.Name = "btnACR";
            this.btnACR.Size = new System.Drawing.Size(20, 20);
            this.btnACR.TabIndex = 155;
            this.btnACR.TabStop = false;
            this.toolTip1.SetToolTip(this.btnACR, "Open look-up list for Registrations");
            this.btnACR.Click += new System.EventHandler(this.btnACR_Click);
            // 
            // btnSTY
            // 
            this.btnSTY.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnSTY.ImageIndex = 15;
            this.btnSTY.ImageList = this.imageList2;
            this.btnSTY.Location = new System.Drawing.Point(256, 376);
            this.btnSTY.Name = "btnSTY";
            this.btnSTY.Size = new System.Drawing.Size(20, 20);
            this.btnSTY.TabIndex = 154;
            this.btnSTY.TabStop = false;
            this.toolTip1.SetToolTip(this.btnSTY, "Open look-up list for Service types");
            this.btnSTY.Click += new System.EventHandler(this.btnSTY_Click);
            // 
            // btnNAT
            // 
            this.btnNAT.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnNAT.ImageIndex = 15;
            this.btnNAT.ImageList = this.imageList2;
            this.btnNAT.Location = new System.Drawing.Point(256, 344);
            this.btnNAT.Name = "btnNAT";
            this.btnNAT.Size = new System.Drawing.Size(20, 20);
            this.btnNAT.TabIndex = 153;
            this.btnNAT.TabStop = false;
            this.toolTip1.SetToolTip(this.btnNAT, "Open look-up list for Nature codes");
            this.btnNAT.Click += new System.EventHandler(this.btnNAT_Click);
            // 
            // btnALC
            // 
            this.btnALC.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnALC.ImageIndex = 15;
            this.btnALC.ImageList = this.imageList2;
            this.btnALC.Location = new System.Drawing.Point(256, 248);
            this.btnALC.Name = "btnALC";
            this.btnALC.Size = new System.Drawing.Size(20, 20);
            this.btnALC.TabIndex = 151;
            this.btnALC.TabStop = false;
            this.toolTip1.SetToolTip(this.btnALC, "Open look-up list for Airlines");
            this.btnALC.Click += new System.EventHandler(this.btnALC_Click);
            // 
            // btnACT
            // 
            this.btnACT.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnACT.ImageIndex = 15;
            this.btnACT.ImageList = this.imageList2;
            this.btnACT.Location = new System.Drawing.Point(256, 312);
            this.btnACT.Name = "btnACT";
            this.btnACT.Size = new System.Drawing.Size(20, 20);
            this.btnACT.TabIndex = 152;
            this.btnACT.TabStop = false;
            this.toolTip1.SetToolTip(this.btnACT, "Open look-up list for A/C Types");
            this.btnACT.Click += new System.EventHandler(this.btnACT_Click);
            // 
            // button1
            // 
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button1.ImageIndex = 15;
            this.button1.ImageList = this.imageList2;
            this.button1.Location = new System.Drawing.Point(248, 232);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(20, 20);
            this.button1.TabIndex = 154;
            this.button1.TabStop = false;
            this.toolTip1.SetToolTip(this.button1, "Open look-up list for Service types");
            // 
            // button2
            // 
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button2.ImageIndex = 15;
            this.button2.ImageList = this.imageList2;
            this.button2.Location = new System.Drawing.Point(248, 264);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(20, 20);
            this.button2.TabIndex = 155;
            this.button2.TabStop = false;
            this.toolTip1.SetToolTip(this.button2, "Open look-up list for Registrations");
            // 
            // button3
            // 
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button3.ImageIndex = 15;
            this.button3.ImageList = this.imageList2;
            this.button3.Location = new System.Drawing.Point(240, 320);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(20, 20);
            this.button3.TabIndex = 154;
            this.button3.TabStop = false;
            this.toolTip1.SetToolTip(this.button3, "Open look-up list for Service types");
            // 
            // button4
            // 
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button4.ImageIndex = 15;
            this.button4.ImageList = this.imageList2;
            this.button4.Location = new System.Drawing.Point(240, 352);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(20, 20);
            this.button4.TabIndex = 155;
            this.button4.TabStop = false;
            this.toolTip1.SetToolTip(this.button4, "Open look-up list for Registrations");
            // 
            // button5
            // 
            this.button5.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button5.ImageIndex = 15;
            this.button5.ImageList = this.imageList2;
            this.button5.Location = new System.Drawing.Point(248, 288);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(20, 20);
            this.button5.TabIndex = 153;
            this.button5.TabStop = false;
            this.toolTip1.SetToolTip(this.button5, "Open look-up list for Nature codes");
            // 
            // button6
            // 
            this.button6.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button6.ImageIndex = 15;
            this.button6.ImageList = this.imageList2;
            this.button6.Location = new System.Drawing.Point(248, 256);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(20, 20);
            this.button6.TabIndex = 152;
            this.button6.TabStop = false;
            this.toolTip1.SetToolTip(this.button6, "Open look-up list for A/C Types");
            // 
            // button7
            // 
            this.button7.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button7.ImageIndex = 4;
            this.button7.ImageList = this.imageList1;
            this.button7.Location = new System.Drawing.Point(320, 64);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(20, 20);
            this.button7.TabIndex = 190;
            this.button7.TabStop = false;
            this.toolTip1.SetToolTip(this.button7, "Change the time to now");
            this.button7.Visible = false;
            // 
            // FlightData
            // 
            this.FlightData.Controls.Add(this.cbTowing);
            this.FlightData.Controls.Add(this.rbDeletedFlights);
            this.FlightData.Controls.Add(this.rbChanged);
            this.FlightData.Controls.Add(this.rbFilterButtons);
            this.FlightData.Controls.Add(this.cbRunway);
            this.FlightData.Controls.Add(this.cbExit);
            this.FlightData.Controls.Add(this.label1);
            this.FlightData.Controls.Add(this.cbPosition);
            this.FlightData.Controls.Add(this.cbCheckin);
            this.FlightData.Controls.Add(this.cbLounge);
            this.FlightData.Controls.Add(this.cbBelt);
            this.FlightData.Controls.Add(this.cbGate);
            this.FlightData.Controls.Add(this.cbTakeOver);
            this.FlightData.Controls.Add(this.btnNow);
            this.FlightData.Controls.Add(this.dateTimePickerTo);
            this.FlightData.Controls.Add(this.lblTo);
            this.FlightData.Controls.Add(this.dateTimePickerFrom);
            this.FlightData.Controls.Add(this.lblFrom);
            this.FlightData.Controls.Add(this.cbArrival);
            this.FlightData.Controls.Add(this.txtCallSignValue);
            this.FlightData.Controls.Add(this.txtACT5Value);
            this.FlightData.Controls.Add(this.txtALT3Value);
            this.FlightData.Controls.Add(this.txtFLNUValue);
            this.FlightData.Controls.Add(this.txtFLTSValue);
            this.FlightData.Controls.Add(this.txtREGNValue);
            this.FlightData.Controls.Add(this.txtALTValue);
            this.FlightData.Controls.Add(this.txtSTYValue);
            this.FlightData.Controls.Add(this.txtNATValue);
            this.FlightData.Controls.Add(this.txtACTValue);
            this.FlightData.Controls.Add(this.lblCallSign);
            this.FlightData.Controls.Add(this.cbDeparture);
            this.FlightData.Controls.Add(this.btnACR);
            this.FlightData.Controls.Add(this.btnSTY);
            this.FlightData.Controls.Add(this.btnNAT);
            this.FlightData.Controls.Add(this.btnALC);
            this.FlightData.Controls.Add(this.lblREGN);
            this.FlightData.Controls.Add(this.lblSTY);
            this.FlightData.Controls.Add(this.lblNAT);
            this.FlightData.Controls.Add(this.lblFLNU);
            this.FlightData.Controls.Add(this.lblALC);
            this.FlightData.Controls.Add(this.btnACT);
            this.FlightData.Controls.Add(this.lblACT);
            this.FlightData.Controls.Add(this.pictureBox1);
            this.FlightData.Controls.Add(this.textBox1);
            this.FlightData.Controls.Add(this.label2);
            this.FlightData.Controls.Add(this.label3);
            this.FlightData.Controls.Add(this.radioButton1);
            this.FlightData.Controls.Add(this.radioButton2);
            this.FlightData.Controls.Add(this.label4);
            this.FlightData.Controls.Add(this.label5);
            this.FlightData.Controls.Add(this.label6);
            this.FlightData.Controls.Add(this.button1);
            this.FlightData.Controls.Add(this.label7);
            this.FlightData.Controls.Add(this.label8);
            this.FlightData.Controls.Add(this.button2);
            this.FlightData.Controls.Add(this.button3);
            this.FlightData.Controls.Add(this.label9);
            this.FlightData.Controls.Add(this.label10);
            this.FlightData.Controls.Add(this.button4);
            this.FlightData.Controls.Add(this.pictureBox2);
            this.FlightData.Controls.Add(this.label11);
            this.FlightData.Controls.Add(this.textBox2);
            this.FlightData.Controls.Add(this.textBox3);
            this.FlightData.Controls.Add(this.textBox4);
            this.FlightData.Controls.Add(this.button5);
            this.FlightData.Controls.Add(this.radioButton3);
            this.FlightData.Controls.Add(this.textBox5);
            this.FlightData.Controls.Add(this.label12);
            this.FlightData.Controls.Add(this.label13);
            this.FlightData.Controls.Add(this.radioButton4);
            this.FlightData.Controls.Add(this.label14);
            this.FlightData.Controls.Add(this.label15);
            this.FlightData.Controls.Add(this.button6);
            this.FlightData.Location = new System.Drawing.Point(4, 22);
            this.FlightData.Name = "FlightData";
            this.FlightData.Size = new System.Drawing.Size(488, 446);
            this.FlightData.TabIndex = 0;
            this.FlightData.Text = "Flight Data";
            this.FlightData.UseVisualStyleBackColor = true;
            // 
            // cbTowing
            // 
            this.cbTowing.Location = new System.Drawing.Point(256, 188);
            this.cbTowing.Name = "cbTowing";
            this.cbTowing.Size = new System.Drawing.Size(104, 24);
            this.cbTowing.TabIndex = 180;
            this.cbTowing.Text = "Towing";
            // 
            // rbDeletedFlights
            // 
            this.rbDeletedFlights.Location = new System.Drawing.Point(120, 120);
            this.rbDeletedFlights.Name = "rbDeletedFlights";
            this.rbDeletedFlights.Size = new System.Drawing.Size(104, 24);
            this.rbDeletedFlights.TabIndex = 179;
            this.rbDeletedFlights.Text = "Deleted Flights";
            this.rbDeletedFlights.CheckedChanged += new System.EventHandler(this.chDeleted_CheckedChanged);
            // 
            // rbChanged
            // 
            this.rbChanged.Location = new System.Drawing.Point(120, 144);
            this.rbChanged.Name = "rbChanged";
            this.rbChanged.Size = new System.Drawing.Size(136, 24);
            this.rbChanged.TabIndex = 178;
            this.rbChanged.Text = "All Changes for ...";
            this.rbChanged.CheckedChanged += new System.EventHandler(this.Check_chDeleted);
            // 
            // rbFilterButtons
            // 
            this.rbFilterButtons.Location = new System.Drawing.Point(120, 168);
            this.rbFilterButtons.Name = "rbFilterButtons";
            this.rbFilterButtons.Size = new System.Drawing.Size(120, 24);
            this.rbFilterButtons.TabIndex = 177;
            this.rbFilterButtons.Text = "Location Changes";
            this.rbFilterButtons.CheckedChanged += new System.EventHandler(this.rbFilterButtons_CheckedChanged);
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.SystemColors.ControlText;
            this.label1.Dock = System.Windows.Forms.DockStyle.Top;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Lime;
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(488, 14);
            this.label1.TabIndex = 169;
            this.label1.Text = "Include Location Changes for :";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // cbTakeOver
            // 
            this.cbTakeOver.Location = new System.Drawing.Point(264, 56);
            this.cbTakeOver.Name = "cbTakeOver";
            this.cbTakeOver.Size = new System.Drawing.Size(104, 32);
            this.cbTakeOver.TabIndex = 168;
            this.cbTakeOver.Text = "Take over to changes";
            // 
            // dateTimePickerTo
            // 
            this.dateTimePickerTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePickerTo.Location = new System.Drawing.Point(120, 88);
            this.dateTimePickerTo.Name = "dateTimePickerTo";
            this.dateTimePickerTo.Size = new System.Drawing.Size(128, 20);
            this.dateTimePickerTo.TabIndex = 164;
            this.dateTimePickerTo.Value = new System.DateTime(2010, 12, 22, 0, 0, 0, 0);
            // 
            // lblTo
            // 
            this.lblTo.BackColor = System.Drawing.Color.Transparent;
            this.lblTo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTo.Location = new System.Drawing.Point(32, 88);
            this.lblTo.Name = "lblTo";
            this.lblTo.Size = new System.Drawing.Size(64, 16);
            this.lblTo.TabIndex = 166;
            this.lblTo.Text = "Flights To:";
            // 
            // dateTimePickerFrom
            // 
            this.dateTimePickerFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePickerFrom.Location = new System.Drawing.Point(120, 64);
            this.dateTimePickerFrom.Name = "dateTimePickerFrom";
            this.dateTimePickerFrom.Size = new System.Drawing.Size(128, 20);
            this.dateTimePickerFrom.TabIndex = 163;
            this.dateTimePickerFrom.Value = new System.DateTime(2010, 12, 22, 0, 0, 0, 0);
            // 
            // lblFrom
            // 
            this.lblFrom.BackColor = System.Drawing.Color.Transparent;
            this.lblFrom.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFrom.Location = new System.Drawing.Point(32, 64);
            this.lblFrom.Name = "lblFrom";
            this.lblFrom.Size = new System.Drawing.Size(80, 16);
            this.lblFrom.TabIndex = 165;
            this.lblFrom.Text = "Flights From:";
            // 
            // cbArrival
            // 
            this.cbArrival.Location = new System.Drawing.Point(256, 169);
            this.cbArrival.Name = "cbArrival";
            this.cbArrival.Size = new System.Drawing.Size(104, 24);
            this.cbArrival.TabIndex = 160;
            this.cbArrival.Text = "Arrival";
            // 
            // txtCallSignValue
            // 
            this.txtCallSignValue.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCallSignValue.Location = new System.Drawing.Point(120, 280);
            this.txtCallSignValue.Name = "txtCallSignValue";
            this.txtCallSignValue.Size = new System.Drawing.Size(128, 20);
            this.txtCallSignValue.TabIndex = 158;
            this.txtCallSignValue.Tag = "CSGNU_VAL";
            // 
            // txtACT5Value
            // 
            this.txtACT5Value.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtACT5Value.Location = new System.Drawing.Point(184, 312);
            this.txtACT5Value.Name = "txtACT5Value";
            this.txtACT5Value.Size = new System.Drawing.Size(64, 20);
            this.txtACT5Value.TabIndex = 141;
            this.txtACT5Value.Tag = "ACT5U_VAL";
            this.txtACT5Value.Leave += new System.EventHandler(this.txtACT5Value_Leave);
            // 
            // txtALT3Value
            // 
            this.txtALT3Value.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtALT3Value.Location = new System.Drawing.Point(184, 248);
            this.txtALT3Value.Name = "txtALT3Value";
            this.txtALT3Value.Size = new System.Drawing.Size(64, 20);
            this.txtALT3Value.TabIndex = 139;
            this.txtALT3Value.Tag = "ALT3U_VAL";
            this.txtALT3Value.Leave += new System.EventHandler(this.txtALT3Value_Leave);
            // 
            // txtFLNUValue
            // 
            this.txtFLNUValue.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtFLNUValue.Location = new System.Drawing.Point(120, 216);
            this.txtFLNUValue.MaxLength = 5;
            this.txtFLNUValue.Name = "txtFLNUValue";
            this.txtFLNUValue.Size = new System.Drawing.Size(88, 20);
            this.txtFLNUValue.TabIndex = 136;
            this.txtFLNUValue.Tag = "FLNU_VAL";
            this.txtFLNUValue.Leave += new System.EventHandler(this.txtFLNUValue_TextChanged);
            // 
            // txtFLTSValue
            // 
            this.txtFLTSValue.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtFLTSValue.Location = new System.Drawing.Point(224, 216);
            this.txtFLTSValue.Name = "txtFLTSValue";
            this.txtFLTSValue.Size = new System.Drawing.Size(24, 20);
            this.txtFLTSValue.TabIndex = 137;
            this.txtFLTSValue.Tag = "FLTS_VAL";
            // 
            // txtREGNValue
            // 
            this.txtREGNValue.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtREGNValue.Location = new System.Drawing.Point(120, 408);
            this.txtREGNValue.Name = "txtREGNValue";
            this.txtREGNValue.Size = new System.Drawing.Size(128, 20);
            this.txtREGNValue.TabIndex = 144;
            this.txtREGNValue.Tag = "REGNU_VAL";
            this.txtREGNValue.Leave += new System.EventHandler(this.txtREGNValue_Leave);
            // 
            // txtALTValue
            // 
            this.txtALTValue.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtALTValue.Location = new System.Drawing.Point(120, 248);
            this.txtALTValue.Name = "txtALTValue";
            this.txtALTValue.Size = new System.Drawing.Size(64, 20);
            this.txtALTValue.TabIndex = 138;
            this.txtALTValue.Tag = "ALTU_VAL";
            this.txtALTValue.Leave += new System.EventHandler(this.txtALTValue_Leave);
            // 
            // txtSTYValue
            // 
            this.txtSTYValue.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtSTYValue.Location = new System.Drawing.Point(120, 376);
            this.txtSTYValue.Name = "txtSTYValue";
            this.txtSTYValue.Size = new System.Drawing.Size(128, 20);
            this.txtSTYValue.TabIndex = 143;
            this.txtSTYValue.Tag = "STYU_VAL";
            this.txtSTYValue.Leave += new System.EventHandler(this.txtSTYValue_Leave);
            // 
            // txtNATValue
            // 
            this.txtNATValue.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtNATValue.Location = new System.Drawing.Point(120, 344);
            this.txtNATValue.Name = "txtNATValue";
            this.txtNATValue.Size = new System.Drawing.Size(128, 20);
            this.txtNATValue.TabIndex = 142;
            this.txtNATValue.Tag = "NATU_VAL";
            this.txtNATValue.Leave += new System.EventHandler(this.txtNATValue_Leave);
            // 
            // txtACTValue
            // 
            this.txtACTValue.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtACTValue.Location = new System.Drawing.Point(120, 312);
            this.txtACTValue.Name = "txtACTValue";
            this.txtACTValue.Size = new System.Drawing.Size(64, 20);
            this.txtACTValue.TabIndex = 140;
            this.txtACTValue.Tag = "ACTU_VAL";
            this.txtACTValue.Leave += new System.EventHandler(this.txtACTValue_Leave);
            // 
            // lblCallSign
            // 
            this.lblCallSign.BackColor = System.Drawing.Color.Transparent;
            this.lblCallSign.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCallSign.Location = new System.Drawing.Point(32, 280);
            this.lblCallSign.Name = "lblCallSign";
            this.lblCallSign.Size = new System.Drawing.Size(72, 16);
            this.lblCallSign.TabIndex = 159;
            this.lblCallSign.Text = "Call Sign:";
            // 
            // cbDeparture
            // 
            this.cbDeparture.Location = new System.Drawing.Point(256, 208);
            this.cbDeparture.Name = "cbDeparture";
            this.cbDeparture.Size = new System.Drawing.Size(104, 24);
            this.cbDeparture.TabIndex = 157;
            this.cbDeparture.Text = "Departure";
            // 
            // lblREGN
            // 
            this.lblREGN.BackColor = System.Drawing.Color.Transparent;
            this.lblREGN.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblREGN.Location = new System.Drawing.Point(32, 408);
            this.lblREGN.Name = "lblREGN";
            this.lblREGN.Size = new System.Drawing.Size(72, 16);
            this.lblREGN.TabIndex = 150;
            this.lblREGN.Text = "Registration:";
            // 
            // lblSTY
            // 
            this.lblSTY.BackColor = System.Drawing.Color.Transparent;
            this.lblSTY.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSTY.Location = new System.Drawing.Point(32, 376);
            this.lblSTY.Name = "lblSTY";
            this.lblSTY.Size = new System.Drawing.Size(72, 16);
            this.lblSTY.TabIndex = 149;
            this.lblSTY.Text = "Servie Type:";
            // 
            // lblNAT
            // 
            this.lblNAT.BackColor = System.Drawing.Color.Transparent;
            this.lblNAT.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNAT.Location = new System.Drawing.Point(32, 344);
            this.lblNAT.Name = "lblNAT";
            this.lblNAT.Size = new System.Drawing.Size(80, 16);
            this.lblNAT.TabIndex = 148;
            this.lblNAT.Text = "Nature Code:";
            // 
            // lblFLNU
            // 
            this.lblFLNU.BackColor = System.Drawing.Color.Transparent;
            this.lblFLNU.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFLNU.Location = new System.Drawing.Point(32, 216);
            this.lblFLNU.Name = "lblFLNU";
            this.lblFLNU.Size = new System.Drawing.Size(80, 16);
            this.lblFLNU.TabIndex = 145;
            this.lblFLNU.Text = "Flightnumber:";
            // 
            // lblALC
            // 
            this.lblALC.BackColor = System.Drawing.Color.Transparent;
            this.lblALC.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblALC.Location = new System.Drawing.Point(32, 248);
            this.lblALC.Name = "lblALC";
            this.lblALC.Size = new System.Drawing.Size(64, 16);
            this.lblALC.TabIndex = 146;
            this.lblALC.Text = "Airline:";
            // 
            // lblACT
            // 
            this.lblACT.BackColor = System.Drawing.Color.Transparent;
            this.lblACT.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblACT.Location = new System.Drawing.Point(32, 312);
            this.lblACT.Name = "lblACT";
            this.lblACT.Size = new System.Drawing.Size(80, 16);
            this.lblACT.TabIndex = 147;
            this.lblACT.Text = "Aircraft Type:";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(488, 446);
            this.pictureBox1.TabIndex = 156;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Paint += new System.Windows.Forms.PaintEventHandler(this.pictureBox1_Paint);
            // 
            // textBox1
            // 
            this.textBox1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textBox1.Location = new System.Drawing.Point(120, 264);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(128, 20);
            this.textBox1.TabIndex = 144;
            this.textBox1.Tag = "REGNU_VAL";
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(24, 264);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(72, 16);
            this.label2.TabIndex = 150;
            this.label2.Text = "Registration:";
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(24, 136);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(72, 16);
            this.label3.TabIndex = 159;
            this.label3.Text = "Call Sign:";
            // 
            // radioButton1
            // 
            this.radioButton1.Location = new System.Drawing.Point(112, 40);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(120, 24);
            this.radioButton1.TabIndex = 162;
            this.radioButton1.Text = "All Changes for ...";
            // 
            // radioButton2
            // 
            this.radioButton2.Location = new System.Drawing.Point(112, 16);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(104, 24);
            this.radioButton2.TabIndex = 161;
            this.radioButton2.Text = "Deleted Flights";
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(24, 232);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(72, 16);
            this.label4.TabIndex = 149;
            this.label4.Text = "Servie Type:";
            // 
            // label5
            // 
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(24, 200);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(80, 16);
            this.label5.TabIndex = 148;
            this.label5.Text = "Nature Code:";
            // 
            // label6
            // 
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(24, 72);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(80, 16);
            this.label6.TabIndex = 145;
            this.label6.Text = "Flightnumber:";
            // 
            // label7
            // 
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(24, 104);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(64, 16);
            this.label7.TabIndex = 146;
            this.label7.Text = "Airline:";
            // 
            // label8
            // 
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(24, 168);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(80, 16);
            this.label8.TabIndex = 147;
            this.label8.Text = "Aircraft Type:";
            // 
            // label9
            // 
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(16, 192);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(64, 16);
            this.label9.TabIndex = 146;
            this.label9.Text = "Airline:";
            // 
            // label10
            // 
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(16, 256);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(80, 16);
            this.label10.TabIndex = 147;
            this.label10.Text = "Aircraft Type:";
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox2.Location = new System.Drawing.Point(0, 0);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(488, 446);
            this.pictureBox2.TabIndex = 156;
            this.pictureBox2.TabStop = false;
            // 
            // label11
            // 
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(16, 160);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(80, 16);
            this.label11.TabIndex = 145;
            this.label11.Text = "Flightnumber:";
            // 
            // textBox2
            // 
            this.textBox2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textBox2.Location = new System.Drawing.Point(120, 320);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(128, 20);
            this.textBox2.TabIndex = 143;
            this.textBox2.Tag = "STYU_VAL";
            // 
            // textBox3
            // 
            this.textBox3.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textBox3.Location = new System.Drawing.Point(120, 288);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(128, 20);
            this.textBox3.TabIndex = 142;
            this.textBox3.Tag = "NATU_VAL";
            // 
            // textBox4
            // 
            this.textBox4.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textBox4.Location = new System.Drawing.Point(184, 256);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(64, 20);
            this.textBox4.TabIndex = 141;
            this.textBox4.Tag = "ACT5U_VAL";
            // 
            // radioButton3
            // 
            this.radioButton3.Location = new System.Drawing.Point(104, 104);
            this.radioButton3.Name = "radioButton3";
            this.radioButton3.Size = new System.Drawing.Size(104, 24);
            this.radioButton3.TabIndex = 161;
            this.radioButton3.Text = "Deleted Flights";
            // 
            // textBox5
            // 
            this.textBox5.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textBox5.Location = new System.Drawing.Point(120, 352);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(128, 20);
            this.textBox5.TabIndex = 144;
            this.textBox5.Tag = "REGNU_VAL";
            // 
            // label12
            // 
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(16, 352);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(72, 16);
            this.label12.TabIndex = 150;
            this.label12.Text = "Registration:";
            // 
            // label13
            // 
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(16, 224);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(72, 16);
            this.label13.TabIndex = 159;
            this.label13.Text = "Call Sign:";
            // 
            // radioButton4
            // 
            this.radioButton4.Location = new System.Drawing.Point(104, 128);
            this.radioButton4.Name = "radioButton4";
            this.radioButton4.Size = new System.Drawing.Size(120, 24);
            this.radioButton4.TabIndex = 162;
            this.radioButton4.Text = "All Changes for ...";
            // 
            // label14
            // 
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(16, 320);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(72, 16);
            this.label14.TabIndex = 149;
            this.label14.Text = "Servie Type:";
            // 
            // label15
            // 
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(16, 288);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(80, 16);
            this.label15.TabIndex = 148;
            this.label15.Text = "Nature Code:";
            // 
            // BasicData
            // 
            this.BasicData.Controls.Add(this.panel1);
            this.BasicData.Controls.Add(this.label18);
            this.BasicData.Controls.Add(this.cbTable);
            this.BasicData.Controls.Add(this.cbTakeOver_BD);
            this.BasicData.Controls.Add(this.button7);
            this.BasicData.Controls.Add(this.dateTimePickerTo_BD);
            this.BasicData.Controls.Add(this.label16);
            this.BasicData.Controls.Add(this.dateTimePickerFrm_BD);
            this.BasicData.Controls.Add(this.label17);
            this.BasicData.Controls.Add(this.pictureBox3);
            this.BasicData.Location = new System.Drawing.Point(4, 22);
            this.BasicData.Name = "BasicData";
            this.BasicData.Size = new System.Drawing.Size(488, 446);
            this.BasicData.TabIndex = 1;
            this.BasicData.Text = "Basic Data";
            this.BasicData.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(8, 144);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(464, 296);
            this.panel1.TabIndex = 194;
            // 
            // label18
            // 
            this.label18.BackColor = System.Drawing.Color.Transparent;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(40, 113);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(72, 16);
            this.label18.TabIndex = 193;
            this.label18.Text = "Table";
            // 
            // cbTable
            // 
            this.cbTable.ItemHeight = 13;
            this.cbTable.Location = new System.Drawing.Point(177, 112);
            this.cbTable.Name = "cbTable";
            this.cbTable.Size = new System.Drawing.Size(128, 21);
            this.cbTable.TabIndex = 192;
            this.cbTable.SelectedIndexChanged += new System.EventHandler(this.cbTable_SelectedIndexChanged);
            this.cbTable.SelectedValueChanged += new System.EventHandler(this.OnTableChange);
            // 
            // cbTakeOver_BD
            // 
            this.cbTakeOver_BD.Checked = true;
            this.cbTakeOver_BD.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbTakeOver_BD.Location = new System.Drawing.Point(320, 32);
            this.cbTakeOver_BD.Name = "cbTakeOver_BD";
            this.cbTakeOver_BD.Size = new System.Drawing.Size(104, 32);
            this.cbTakeOver_BD.TabIndex = 191;
            this.cbTakeOver_BD.Text = "Take over to changes";
            this.cbTakeOver_BD.Visible = false;
            // 
            // dateTimePickerTo_BD
            // 
            this.dateTimePickerTo_BD.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePickerTo_BD.Location = new System.Drawing.Point(177, 64);
            this.dateTimePickerTo_BD.Name = "dateTimePickerTo_BD";
            this.dateTimePickerTo_BD.Size = new System.Drawing.Size(128, 20);
            this.dateTimePickerTo_BD.TabIndex = 187;
            this.dateTimePickerTo_BD.Value = new System.DateTime(2010, 12, 22, 23, 59, 59, 0);
            // 
            // label16
            // 
            this.label16.BackColor = System.Drawing.Color.Transparent;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(40, 64);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(80, 16);
            this.label16.TabIndex = 189;
            this.label16.Text = "Changes To:";
            // 
            // dateTimePickerFrm_BD
            // 
            this.dateTimePickerFrm_BD.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePickerFrm_BD.Location = new System.Drawing.Point(177, 40);
            this.dateTimePickerFrm_BD.Name = "dateTimePickerFrm_BD";
            this.dateTimePickerFrm_BD.Size = new System.Drawing.Size(128, 20);
            this.dateTimePickerFrm_BD.TabIndex = 186;
            this.dateTimePickerFrm_BD.Value = new System.DateTime(2010, 12, 22, 0, 0, 0, 0);
            // 
            // label17
            // 
            this.label17.BackColor = System.Drawing.Color.Transparent;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(40, 40);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(88, 16);
            this.label17.TabIndex = 188;
            this.label17.Text = "Changes From:";
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox3.Location = new System.Drawing.Point(0, 0);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(488, 446);
            this.pictureBox3.TabIndex = 157;
            this.pictureBox3.TabStop = false;
            this.pictureBox3.Paint += new System.Windows.Forms.PaintEventHandler(this.pictureBox3_Paint);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.FlightData);
            this.tabControl1.Controls.Add(this.BasicData);
            this.tabControl1.Controls.Add(this.CCAData);
            this.tabControl1.ItemSize = new System.Drawing.Size(63, 18);
            this.tabControl1.Location = new System.Drawing.Point(8, 40);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(496, 472);
            this.tabControl1.TabIndex = 115;
            this.tabControl1.SelectedIndexChanged += new System.EventHandler(this.TabPageChanged);
            // 
            // CCAData
            // 
            this.CCAData.Controls.Add(this.groupBox2);
            this.CCAData.Controls.Add(this.groupBox1);
            this.CCAData.Controls.Add(this.txtCounter);
            this.CCAData.Controls.Add(this.label21);
            this.CCAData.Controls.Add(this.pictureBox4);
            this.CCAData.Location = new System.Drawing.Point(4, 22);
            this.CCAData.Name = "CCAData";
            this.CCAData.Size = new System.Drawing.Size(488, 446);
            this.CCAData.TabIndex = 2;
            this.CCAData.Text = "Common CKI";
            this.CCAData.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.Transparent;
            this.groupBox2.Controls.Add(this.label23);
            this.groupBox2.Controls.Add(this.label20);
            this.groupBox2.Controls.Add(this.dtpCloseTo);
            this.groupBox2.Controls.Add(this.dtpCloseFrom);
            this.groupBox2.Location = new System.Drawing.Point(9, 95);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(470, 63);
            this.groupBox2.TabIndex = 160;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Scheduled Close";
            
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(248, 27);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(20, 13);
            this.label23.TabIndex = 4;
            this.label23.Text = "To";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(6, 27);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(30, 13);
            this.label20.TabIndex = 4;
            this.label20.Text = "From";
            // 
            // dtpCloseTo
            // 
            this.dtpCloseTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpCloseTo.Location = new System.Drawing.Point(271, 24);
            this.dtpCloseTo.Name = "dtpCloseTo";
            this.dtpCloseTo.Size = new System.Drawing.Size(184, 20);
            this.dtpCloseTo.TabIndex = 6;
            // 
            // dtpCloseFrom
            // 
            this.dtpCloseFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpCloseFrom.Location = new System.Drawing.Point(39, 24);
            this.dtpCloseFrom.Name = "dtpCloseFrom";
            this.dtpCloseFrom.Size = new System.Drawing.Size(184, 20);
            this.dtpCloseFrom.TabIndex = 5;
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.Controls.Add(this.label22);
            this.groupBox1.Controls.Add(this.label19);
            this.groupBox1.Controls.Add(this.dtpOpenTo);
            this.groupBox1.Controls.Add(this.dtpOpenFrom);
            this.groupBox1.Location = new System.Drawing.Point(9, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(470, 67);
            this.groupBox1.TabIndex = 159;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Scheduled Open";
            
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(248, 32);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(20, 13);
            this.label22.TabIndex = 3;
            this.label22.Text = "To";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(6, 31);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(30, 13);
            this.label19.TabIndex = 2;
            this.label19.Text = "From";
            // 
            // dtpOpenTo
            // 
            this.dtpOpenTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpOpenTo.Location = new System.Drawing.Point(271, 28);
            this.dtpOpenTo.Name = "dtpOpenTo";
            this.dtpOpenTo.Size = new System.Drawing.Size(184, 20);
            this.dtpOpenTo.TabIndex = 1;
            // 
            // dtpOpenFrom
            // 
            this.dtpOpenFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpOpenFrom.Location = new System.Drawing.Point(39, 28);
            this.dtpOpenFrom.Name = "dtpOpenFrom";
            this.dtpOpenFrom.Size = new System.Drawing.Size(184, 20);
            this.dtpOpenFrom.TabIndex = 0;
            // 
            // txtCounter
            // 
            this.txtCounter.Location = new System.Drawing.Point(58, 180);
            this.txtCounter.Name = "txtCounter";
            this.txtCounter.Size = new System.Drawing.Size(407, 20);
            this.txtCounter.TabIndex = 7;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(9, 183);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(44, 13);
            this.label21.TabIndex = 4;
            this.label21.Text = "Counter";
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox4.Location = new System.Drawing.Point(0, 0);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(488, 446);
            this.pictureBox4.TabIndex = 158;
            this.pictureBox4.TabStop = false;
            this.pictureBox4.Paint += new System.Windows.Forms.PaintEventHandler(this.pictureBox4_Paint);
            // 
            // frmView
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(512, 518);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.txtACTUrno);
            this.Controls.Add(this.cbUTC);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnNew);
            this.Controls.Add(this.btnApply);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.pictureBoxButton);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmView";
            this.Text = "Location Changes: Define Flights and Locations";
            this.Load += new System.EventHandler(this.frmView_Load);
            this.Activated += new System.EventHandler(this.frmView_Activated);
            this.Closing += new System.ComponentModel.CancelEventHandler(this.frmView_Closing);
            this.Resize += new System.EventHandler(this.frmView_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxButton)).EndInit();
            this.FlightData.ResumeLayout(false);
            this.FlightData.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.BasicData.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.CCAData.ResumeLayout(false);
            this.CCAData.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion

        private void pictureBox1_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
        {
            //UT.DrawGradient(e.Graphics, 90f, pictureBox1, Color.WhiteSmoke, Color.LightGray);
            UT.DrawGradient(e.Graphics, 90f, pictureBox1, Color.AliceBlue, Color.LightSteelBlue);//Color.CornflowerBlue);

        }
        private void pictureBox3_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
        {
            //UT.DrawGradient(e.Graphics, 90f, pictureBox3, Color.WhiteSmoke, Color.LightGray);
            UT.DrawGradient(e.Graphics, 90f, pictureBox3, Color.AliceBlue, Color.LightSteelBlue);//Color.CornflowerBlue);

        }

        private void frmView_Resize(object sender, System.EventArgs e)
        {
            pictureBox1.Invalidate();
            pictureBox3.Invalidate();
            pictureBoxButton.Invalidate();
        }

        private void frmView_Load(object sender, System.EventArgs e)
        {
            myDB = UT.GetMemDB();

            cbUTC.Parent = pictureBoxButton;
            btnSave.Parent = pictureBoxButton;
            btnApply.Parent = pictureBoxButton;
            btnNew.Parent = pictureBoxButton;
            btnClose.Parent = pictureBoxButton;

            cbTakeOver.Parent = pictureBox1;
            btnNow.Parent = pictureBox1;
            btnALC.Parent = pictureBox1;
            btnACT.Parent = pictureBox1;
            btnNAT.Parent = pictureBox1;
            btnSTY.Parent = pictureBox1;
            btnACR.Parent = pictureBox1;

            lblFrom.Parent = pictureBox1;
            lblTo.Parent = pictureBox1;
            lblFLNU.Parent = pictureBox1;
            lblALC.Parent = pictureBox1;
            lblACT.Parent = pictureBox1;
            lblNAT.Parent = pictureBox1;
            lblSTY.Parent = pictureBox1;
            lblREGN.Parent = pictureBox1;
            lblCallSign.Parent = pictureBox1;

            cbPosition.Parent = pictureBox1;
            cbGate.Parent = pictureBox1;
            cbBelt.Parent = pictureBox1;
            cbLounge.Parent = pictureBox1;
            cbExit.Parent = pictureBox1;
            cbCheckin.Parent = pictureBox1;
            cbArrival.Parent = pictureBox1;
            cbDeparture.Parent = pictureBox1;
            cbRunway.Parent = pictureBox1;
            rbChanged.Parent = pictureBox1;
            rbDeletedFlights.Parent = pictureBox1;
            rbFilterButtons.Parent = pictureBox1;
            cbTowing.Parent = pictureBox1;

            dateTimePickerFrm_BD.Parent = pictureBox3;
            dateTimePickerTo_BD.Parent = pictureBox3;
            cbTakeOver_BD.Parent = pictureBox3;
            label16.Parent = pictureBox3;
            label17.Parent = pictureBox3;
            label18.Parent = pictureBox3;
            cbTable.Parent = pictureBox3;
            button7.Parent = pictureBox3;
            panel1.Parent = pictureBox3;

            groupBox1.Parent = pictureBox4;
            groupBox2.Parent = pictureBox4;

            string strDTFormat = "dd.MM.yyyy - HH:mm";
            //DateTime dtTmpFrom = new DateTime(dateTimePickerFrom.Value.Year, dateTimePickerFrom.Value.Month, dateTimePickerFrom.Value.Day, 0, 0, 0, 0);
            //DateTime dtTmpTo = new DateTime(dateTimePickerTo.Value.Year, dateTimePickerTo.Value.Month, dateTimePickerTo.Value.Day, 23, 59, 59, 0);
            DateTime dtNow = DateTime.Now;
            DateTime dtTmpFrom = new DateTime(dtNow.Year, dtNow.Month, dtNow.Day, 0, 0, 0, 0);
            DateTime dtTmpTo = new DateTime(dtNow.Year, dtNow.Month, dtNow.Day, 23, 59, 59, 0);

            SetDTControls(strDTFormat, dtTmpFrom, dtTmpTo);

            InitWithFile();
            InitCCKSelections();
            CheckChecked();
            Check_chDeleted();
            ReadRegistry();
            this.Focus();
            dateTimePickerFrom.Focus();
        }

        private void SetDTControls(string strDTFormat, DateTime dtTmpFrom, DateTime dtTmpTo)
        {
            dateTimePickerFrom.CustomFormat = strDTFormat;
            dateTimePickerFrom.Value = dtTmpFrom;
            dateTimePickerTo.CustomFormat = strDTFormat;
            dateTimePickerTo.Value = dtTmpTo;

            dateTimePickerFrm_BD.CustomFormat = strDTFormat;
            dateTimePickerFrm_BD.Value = dtTmpFrom;
            dateTimePickerTo_BD.CustomFormat = strDTFormat;
            dateTimePickerTo_BD.Value = dtTmpTo;
            
            //*********************************
            //START DateTime fromat for Common CKI tab
            //*********************************

            //Schedule Open 
            dtpOpenFrom.CustomFormat = strDTFormat;
            dtpOpenFrom.Value = dtTmpFrom;
            dtpOpenTo.CustomFormat = strDTFormat;
            dtpOpenTo.Value = dtTmpTo;

            //Schedule close 
            dtpCloseFrom.CustomFormat = strDTFormat;
            dtpCloseFrom.Value = dtTmpFrom;
            dtpCloseTo.CustomFormat = strDTFormat;
            dtpCloseTo.Value = dtTmpTo;
            //*********************************
            //END DateTime fromat for Common CKI tab
            //*********************************

        }

        private void SetValueCCKTab(DateTime dtOpenFrom, DateTime dtOpenTo,DateTime dtCloseFrom, DateTime dtCloseTo,string counter)
        {
            //Schedule Open 
            dtpOpenFrom.Value = dtOpenFrom;
            dtpOpenTo.Value = dtOpenTo;

            //Schedule close 
            dtpCloseFrom.Value = dtCloseFrom;
            dtpCloseTo.Value = dtCloseTo;

            //Counter
            txtCounter.Text = counter.Trim();
            
        }
        private void WriteRegistry()
        {
            string theKey = "Software\\UFIS\\DATACHANGES\\ViewDialog";
            RegistryKey rk = Registry.CurrentUser.CreateSubKey(theKey);
            if (rk == null)
            {
                rk.CreateSubKey(theKey);
                rk = Registry.CurrentUser.OpenSubKey(theKey, true);
            }

            rk.SetValue("X", this.Left.ToString());
            rk.SetValue("Y", this.Top.ToString());
            rk.SetValue("Width", this.Width.ToString());
            rk.SetValue("Height", this.Height.ToString());
            rk.Close();
        }

        private void ReadRegistry()
        {
            int myX = -1, myY = -1, myW = -1, myH = -1;
            string regVal = "";
            string theKey = "Software\\UFIS\\DATACHANGES\\ViewDialog";
            RegistryKey rk = Registry.CurrentUser.OpenSubKey(theKey, true);
            if (rk != null)
            {
                regVal = rk.GetValue("X", "-1").ToString();
                myX = Convert.ToInt32(regVal);
                regVal = rk.GetValue("Y", "-1").ToString();
                myY = Convert.ToInt32(regVal);
                regVal = rk.GetValue("Width", "-1").ToString();
                myW = Convert.ToInt32(regVal);
                regVal = rk.GetValue("Height", "-1").ToString();
                myH = Convert.ToInt32(regVal);
            }
            if (myW != -1 && myH != -1)
            {
                this.Left = myX;
                this.Top = myY;
                this.Width = myW;
                this.Height = myH;
            }
        }
        private void InitCCKSelections()
        {
            FileStream fsCCKText;
            try
            {
                fsCCKText = new FileStream(strFileNameCKI, FileMode.OpenOrCreate);

                StreamReader fstr_in = new StreamReader(fsCCKText);
                string[] txtArray = null;
                string txtLine;
                while ((txtLine = fstr_in.ReadLine()) != null)
                {
                    txtArray = txtLine.Split(new char[] { '|' });
                }
                if (txtArray.Length > 0)
                {
                    //txtArray[0] - Schedule Open From
                    //txtArray[1] - Schedule Open To
                    //txtArray[2] - Schedule Close From
                    //txtArray[3] - Schedule Close To
                    //txtArray[4] - Counter

                    SetValueCCKTab(UT.CedaTimeToDateTime(txtArray[0]),
                        UT.CedaTimeToDateTime(txtArray[1]),
                        UT.CedaTimeToDateTime(txtArray[2]),
                        UT.CedaTimeToDateTime(txtArray[3]),
                        txtArray[4]);
                }
            }
            catch(Exception)
            {
            }
            
        }
        private void InitWithFile()
        {
            string txtLine;
            string[] txtArray;
            UT.IsTimeInUtc = false;
            cbUTC.Checked = false;
            cbUTC.Text = "Local";
            cbPosition.Checked = true;
            cbGate.Checked = true;
            cbBelt.Checked = true;
            cbLounge.Checked = true;
            cbCheckin.Checked = true;
            cbExit.Checked = true;
            cbRunway.Checked = true;


            FileStream fin;
            try
            {
                fin = new FileStream(mySaveFile, FileMode.OpenOrCreate);
                
            }
            catch (IOException exc)
            {
                //MessageBox.Show(this, mySaveFile, exc.Message + "Read Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }


            StreamReader fstr_in = new StreamReader(fin);
            //			StreamReader datei = File.OpenText(mySaveFile);
            //			while (datei.Peek() != -1)
            while ((txtLine = fstr_in.ReadLine()) != null)
            {
                cbUTC.Text = "Local";
                //				txtLine = datei.ReadLine();
                txtArray = txtLine.Split(new char[] { '|' });

                if (txtArray.Length >= 14)
                {
                    string[] cbArray = txtArray[1].Split(new char[] { ',' });
                    for (int i = 0; i < cbArray.Length; i++)
                    {
                        if (cbArray.GetValue(i).ToString() == "1")
                        {
                            if (i == 0)
                            {
                                cbUTC.Checked = true;
                                UT.IsTimeInUtc = true;
                                cbUTC.Text = "UTC";
                            }
                            if (i == 1)
                                cbPosition.Checked = true;
                            if (i == 2)
                                cbGate.Checked = true;
                            if (i == 3)
                                cbBelt.Checked = true;
                            if (i == 4)
                                cbLounge.Checked = true;
                            if (i == 5)
                                cbCheckin.Checked = true;
                            if (i == 6)
                                cbRunway.Checked = true;
                        }
                        else
                        {
                            if (i == 0)
                            {
                                cbUTC.Checked = false;
                                UT.IsTimeInUtc = false;
                                cbUTC.Text = "Local";
                            }
                            if (i == 1)
                                cbPosition.Checked = false;
                            if (i == 2)
                                cbGate.Checked = false;
                            if (i == 3)
                                cbBelt.Checked = false;
                            if (i == 4)
                                cbLounge.Checked = false;
                            if (i == 5)
                                cbCheckin.Checked = false;
                            if (i == 6)
                                cbRunway.Checked = false;
                        }
                    }

                    dateTimePickerFrom.Value = UT.CedaTimeToDateTime(txtArray[2]);
                    dateTimePickerTo.Value = UT.CedaTimeToDateTime(txtArray[3]);
                    txtFLNUValue.Text = txtArray[4];
                    txtFLTSValue.Text = txtArray[5];
                    txtALTValue.Text = txtArray[6];
                    txtALT3Value.Text = txtArray[7];
                    txtACTValue.Text = txtArray[8];
                    txtACT5Value.Text = txtArray[9];
                    txtNATValue.Text = txtArray[10];
                    txtSTYValue.Text = txtArray[11];
                    txtREGNValue.Text = txtArray[12];
                    txtCallSignValue.Text = txtArray[13];//SISL
                }

                rbChanged.Checked = false;
                rbFilterButtons.Checked = false;
                cbArrival.Checked = false;
                cbDeparture.Checked = false;
                cbTowing.Checked = false;
                if (txtArray.Length >= 17)//SISL
                {
                    if (txtArray[14] == "1")
                        rbChanged.Checked = true;
                    if (txtArray[15] == "1")
                        cbArrival.Checked = true;
                    if (txtArray[16] == "1")
                        cbDeparture.Checked = true;
                }

                if (rbChanged.Checked == false)
                {
                    cbArrival.Visible = false;
                    cbDeparture.Visible = false;
                    cbTowing.Visible = false;
                }

                if (txtArray.Length >= 18)
                {
                    if (txtArray[17] == "1")
                        cbExit.Checked = true;
                    else
                        cbExit.Checked = false;
                }

                if (txtArray.Length >= 19)
                {
                    if (txtArray[18] == "1")
                        cbTakeOver.Checked = true;
                }
                if (txtArray.Length >= 20)
                {
                    if (txtArray[19] == "1")
                        rbDeletedFlights.Checked = true;
                }
                if (txtArray.Length >= 21)
                {
                    if (txtArray[20] == "1")
                        rbFilterButtons.Checked = true;
                }
                if (txtArray.Length >= 22)
                {
                    dateTimePickerFrm_BD.Value = UT.CedaTimeToDateTime(txtArray[21]);
                }
                if (txtArray.Length >= 23)
                {
                    dateTimePickerTo_BD.Value = UT.CedaTimeToDateTime(txtArray[22]);
                }
                if (txtArray.Length >= 24)
                {
                    if (txtArray[23] == "1")
                        cbTakeOver_BD.Checked = true;
                }
                if (txtArray.Length >= 25)
                {
                    if (txtArray[24] == "1")
                        cbTowing.Checked = true;
                }

                Check_chDeleted();
                CheckChecked();
            }
            //			datei.Close();
            fstr_in.Close();
        }

        private void btnACT_Click(object sender, System.EventArgs e)
        {
            System.Windows.Forms.DialogResult olResult;
            frmChoiceList olDlg = new frmChoiceList();
            olDlg.currTable = "ACT";
            olDlg.lblCaption.Text = "Aircrafts";
            olResult = olDlg.ShowDialog();
            if (olResult == DialogResult.OK)
            {
                int sel = olDlg.tabList.GetCurrentSelected();
                string strUrno = olDlg.tabList.GetColumnValue(sel, 0);
                txtACTUrno.Text = strUrno;
                txtACTValue.Text = olDlg.tabList.GetColumnValue(sel, 1);
                txtACTValue.ForeColor = Color.Black;
                txtACT5Value.Text = olDlg.tabList.GetColumnValue(sel, 2);
                txtACT5Value.ForeColor = Color.Black;
            }
            CheckValid();
        }

        private void txtACTValue_Leave(object sender, System.EventArgs e)
        {
            string strValue = txtACTValue.Text;
            if (strValue == "")
            {
                txtACTUrno.Text = "";
                txtACTValue.ForeColor = Color.Black;
                return; //Need no check
            }
            ITable myTab = myDB["ACT"];
            bool blFound = false;
            for (int i = 0; i < myTab.Count && blFound == false; i++)
            {
                if (strValue == myTab[i]["IATA-Code"])
                {
                    txtACTUrno.Text = myTab[i]["URNO"];
                    blFound = true;
                    break;
                }
            }

            if (blFound == true)
                txtACTValue.ForeColor = Color.Black;
            else
                txtACTValue.ForeColor = Color.Red;

            CheckValid();
        }

        private void btnNow_Click(object sender, System.EventArgs e)
        {
            DateTime olNow;
            if (UT.IsTimeInUtc == true)
            {
                olNow = DateTime.UtcNow;
            }
            else
            {
                olNow = DateTime.Now;
            }
            // Selected Tab Page
            if ("Basic Data" == omSelectedTabPage)
                dateTimePickerTo_BD.Value = olNow;
            else
                dateTimePickerTo.Value = olNow;
        }

        // check if any checkbox for locations are activated
        // if not set the label above to red because nothing can be found without a location
        private void CheckChecked()
        {
            if (cbPosition.Checked || cbGate.Checked || cbBelt.Checked || cbLounge.Checked || cbCheckin.Checked || cbExit.Checked || cbRunway.Checked)
                label1.ForeColor = Color.Lime;
            else
                label1.ForeColor = Color.Red;

            CheckValid();
        }

        // check for a valid search
        // if any control is red (invalid) the apply and save will be disabled
        private bool CheckValid()
        {
            btnApply.Enabled = true;
            btnSave.Enabled = true;

            if (label1.ForeColor == Color.Red
                || txtFLNUValue.ForeColor == Color.Red
                || txtFLTSValue.ForeColor == Color.Red
                || txtALTValue.ForeColor == Color.Red
                || txtALT3Value.ForeColor == Color.Red
                || txtACTValue.ForeColor == Color.Red
                || txtACT5Value.ForeColor == Color.Red
                || txtNATValue.ForeColor == Color.Red
                || txtSTYValue.ForeColor == Color.Red
                || txtREGNValue.ForeColor == Color.Red
                || txtCallSignValue.ForeColor == Color.Red)
            {
                btnApply.Enabled = false;
                btnSave.Enabled = false;
                return false;
            }

            return true;
        }

        // toggle for checked/unchecked
        private void cbPosition_CheckedChanged(object sender, System.EventArgs e)
        {
            if (cbPosition.Checked == true)
                cbPosition.BackColor = Color.LightGreen;
            else
                cbPosition.BackColor = Color.Transparent;

            cbPosition.Refresh();
            CheckChecked();
        }

        // toggle for checked/unchecked
        private void cbGate_CheckedChanged(object sender, System.EventArgs e)
        {
            if (cbGate.Checked == true)
                cbGate.BackColor = Color.LightGreen;
            else
                cbGate.BackColor = Color.Transparent;

            cbGate.Refresh();
            CheckChecked();
        }

        // toggle for checked/unchecked
        private void cbBelt_CheckedChanged(object sender, System.EventArgs e)
        {
            if (cbBelt.Checked == true)
                cbBelt.BackColor = Color.LightGreen;
            else
                cbBelt.BackColor = Color.Transparent;

            cbBelt.Refresh();
            CheckChecked();
        }

        // toggle for checked/unchecked
        private void cbLounge_CheckedChanged(object sender, System.EventArgs e)
        {
            if (cbLounge.Checked == true)
                cbLounge.BackColor = Color.LightGreen;
            else
                cbLounge.BackColor = Color.Transparent;

            cbLounge.Refresh();
            CheckChecked();
        }

        // toggle for checked/unchecked
        private void cbCheckin_CheckedChanged(object sender, System.EventArgs e)
        {
            if (cbCheckin.Checked == true)
                cbCheckin.BackColor = Color.LightGreen;
            else
                cbCheckin.BackColor = Color.Transparent;

            cbCheckin.Refresh();
            CheckChecked();
        }

        private void cbRunway_CheckedChanged(object sender, System.EventArgs e)
        {
            if (cbRunway.Checked == true)
                cbRunway.BackColor = Color.LightGreen;
            else
                cbRunway.BackColor = Color.Transparent;

            cbRunway.Refresh();
            CheckChecked();
        }

        private void pictureBoxButton_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
        {
            //UT.DrawGradient(e.Graphics, 90f, pictureBoxButton, Color.WhiteSmoke, Color.LightGray);
            UT.DrawGradient(e.Graphics, 90f, pictureBoxButton, Color.AliceBlue, Color.LightSteelBlue);//Color.CornflowerBlue);

        }

        // do nothing just close
        private void btnClose_Click(object sender, System.EventArgs e)
        {
            this.Close();
        }

        // toggle for utc/local request
        private void cbUTC_CheckedChanged(object sender, System.EventArgs e)
        {
            if (cbUTC.Checked == true)
            {
                cbUTC.Text = "UTC";
                UT.IsTimeInUtc = true;
            }
            else
            {
                cbUTC.Text = "Local";
                UT.IsTimeInUtc = false;
            }

            cbUTC.Refresh();
        }

        // check for valid input (invalid is red)
        private void txtNATValue_Leave(object sender, System.EventArgs e)
        {
            string strValue = txtNATValue.Text;
            if (strValue == "")
            {
                txtACTUrno.Text = "";
                txtNATValue.ForeColor = Color.Black;
                return; //Need no check
            }
            ITable myTab = myDB["NAT"];
            bool blFound = false;

            string[] strArr = strValue.Split('/');
            string strValueTtyp = "";
            if (strArr.Length >= 1)
                strValueTtyp = strArr[0];

            for (int i = 0; i < myTab.Count && blFound == false; i++)
            {
                string t = myTab[i]["TTYP"];
                if (strValueTtyp == myTab[i]["Type"])
                {
                    txtACTUrno.Text = myTab[i]["URNO"];
                    blFound = true;
                    break;
                }
            }
            if (blFound == true)
                txtNATValue.ForeColor = Color.Black;
            else
                txtNATValue.ForeColor = Color.Red;

            CheckValid();
        }

        // check for valid input (invalid is red)
        private void txtSTYValue_Leave(object sender, System.EventArgs e)
        {
            string strValue = txtSTYValue.Text;
            if (strValue == "")
            {
                txtACTUrno.Text = "";
                txtSTYValue.ForeColor = Color.Black;
                return; //Need no check
            }
            ITable myTab = myDB["STY"];
            bool blFound = false;

            string[] strArr = strValue.Split('/');
            string strValueStyp = "";
            if (strArr.Length >= 1)
                strValueStyp = strArr[0];

            for (int i = 0; i < myTab.Count && blFound == false; i++)
            {
                if (strValueStyp == myTab[i]["Type"])
                {
                    txtACTUrno.Text = myTab[i]["URNO"];
                    blFound = true;
                    break;
                }
            }
            if (blFound == true)
                txtSTYValue.ForeColor = Color.Black;
            else
                txtSTYValue.ForeColor = Color.Red;

            CheckValid();
        }

        // call ChoiceList
        private void btnALC_Click(object sender, System.EventArgs e)
        {
            System.Windows.Forms.DialogResult olResult;
            frmChoiceList olDlg = new frmChoiceList();
            olDlg.currTable = "ALT";
            olDlg.lblCaption.Text = "Airlines";
            olResult = olDlg.ShowDialog();
            if (olResult == DialogResult.OK)
            {
                int sel = olDlg.tabList.GetCurrentSelected();
                string strUrno = olDlg.tabList.GetColumnValue(sel, 0);
                txtACTUrno.Text = strUrno;
                txtALTValue.Text = olDlg.tabList.GetColumnValue(sel, 1);
                txtALTValue.ForeColor = Color.Black;
                txtALT3Value.Text = olDlg.tabList.GetColumnValue(sel, 2);
                txtALT3Value.ForeColor = Color.Black;
            }
            CheckValid();
        }

        // call ChoiceList
        private void btnNAT_Click(object sender, System.EventArgs e)
        {
            System.Windows.Forms.DialogResult olResult;
            frmChoiceList olDlg = new frmChoiceList();
            olDlg.currTable = "NAT";
            olDlg.lblCaption.Text = "Natures";
            olResult = olDlg.ShowDialog();
            if (olResult == DialogResult.OK)
            {
                int sel = olDlg.tabList.GetCurrentSelected();
                string strUrno = olDlg.tabList.GetColumnValue(sel, 0);
                txtACTUrno.Text = strUrno;
                txtNATValue.Text = olDlg.tabList.GetColumnValue(sel, 1) + "/" + olDlg.tabList.GetColumnValue(sel, 2);
                txtNATValue.ForeColor = Color.Black;
            }
            CheckValid();
        }

        // call ChoiceList
        private void btnSTY_Click(object sender, System.EventArgs e)
        {
            System.Windows.Forms.DialogResult olResult;
            frmChoiceList olDlg = new frmChoiceList();
            olDlg.currTable = "STY";
            olDlg.lblCaption.Text = "Services";
            olResult = olDlg.ShowDialog();
            if (olResult == DialogResult.OK)
            {
                int sel = olDlg.tabList.GetCurrentSelected();
                string strUrno = olDlg.tabList.GetColumnValue(sel, 0);
                txtACTUrno.Text = strUrno;
                txtSTYValue.Text = olDlg.tabList.GetColumnValue(sel, 1) + "/" + olDlg.tabList.GetColumnValue(sel, 2);
                txtSTYValue.ForeColor = Color.Black;
            }
            CheckValid();
        }

        // call ChoiceList. ACTTAB not loaded automaticlly in frmData (too many reccords for the startup) 
        // if it hasn't been loaded once the user will be ask if he want.
        private void btnACR_Click(object sender, System.EventArgs e)
        {
            if (!m_ACRloaded)
            {
                DialogResult results = MessageBox.Show(this, "Will you load the ACRTAB ? (This could take some time!)", "Loading ACRTAB ?", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);
                if (results == DialogResult.Yes)
                {
                    this.Cursor = Cursors.WaitCursor;
                    m_ACRloaded = true;
                    frmData DataForm = new frmData();
                    DataForm.Hide();
                    DataForm.LoadACRData();
                    this.Cursor = Cursors.Arrow;
                }
            }

            if (m_ACRloaded)
            {
                System.Windows.Forms.DialogResult olResult;
                frmChoiceList olDlg = new frmChoiceList();
                olDlg.currTable = "ACR";
                olDlg.lblCaption.Text = "Registrations";
                olResult = olDlg.ShowDialog();
                if (olResult == DialogResult.OK)
                {
                    int sel = olDlg.tabList.GetCurrentSelected();
                    string strUrno = olDlg.tabList.GetColumnValue(sel, 0);
                    txtACTUrno.Text = strUrno;
                    string val1 = olDlg.tabList.GetColumnValue(sel, 1);
                    string val2 = olDlg.tabList.GetColumnValue(sel, 2);
                    string val3 = olDlg.tabList.GetColumnValue(sel, 3);
                    string strValue = val1 + "/" + val2 + "-" + val3;
                    txtREGNValue.Text = strValue;
                    txtREGNValue.ForeColor = Color.Black;
                }
                CheckValid();
            }
        }

        // check for valid input (invalid is red)
        private void txtALTValue_Leave(object sender, System.EventArgs e)
        {
            string strValue = txtALTValue.Text;
            if (strValue == "")
            {
                txtACTUrno.Text = "";
                txtALTValue.ForeColor = Color.Black;
                return; //Need no check
            }
            ITable myTab = myDB["ALT"];
            bool blFound = false;
            for (int i = 0; i < myTab.Count && blFound == false; i++)
            {
                if (strValue == myTab[i]["IATA-Code"])
                {
                    txtALT3Value.Text = myTab[i]["ICAO-Code"];
                    txtACTUrno.Text = myTab[i]["URNO"];
                    blFound = true;
                    break;
                }
            }
            if (blFound == true)
                txtALTValue.ForeColor = Color.Black;
            else
                txtALTValue.ForeColor = Color.Red;

            CheckValid();
        }

        // check for valid input (invalid is red)
        private void txtREGNValue_Leave(object sender, System.EventArgs e)
        {
            string strValue = txtREGNValue.Text;
            if (strValue == "")
            {
                txtACTUrno.Text = "";
                txtREGNValue.ForeColor = Color.Black;
                return; //Need no check
            }
            ITable myTab = myDB["ACR"];
            bool blFound = false;

            if (myTab == null)
                return;

            string[] strArr = strValue.Split('/');
            string strValueRegn = "";
            if (strArr.Length >= 1)
                strValueRegn = strArr[0];

            for (int i = 0; i < myTab.Count && blFound == false; i++)
            {
                if (strValueRegn == myTab[i]["Registration"])
                {
                    txtACTUrno.Text = myTab[i]["URNO"];
                    blFound = true;
                    break;
                }
            }
            if (blFound == true)
                txtREGNValue.ForeColor = Color.Black;
            else
                txtREGNValue.ForeColor = Color.Red;

            CheckValid();
        }

        // toggle for check/uncheck all location-boxes
        private void label1_Click(object sender, System.EventArgs e)
        {
            if (label1.ForeColor == Color.Red)
            {
                cbPosition.Checked = true;
                cbBelt.Checked = true;
                cbGate.Checked = true;
                cbExit.Checked = true;
                cbLounge.Checked = true;
                cbCheckin.Checked = true;
                cbRunway.Checked = true;
                label1.ForeColor = Color.Lime;
            }
            else
            {
                cbPosition.Checked = false;
                cbBelt.Checked = false;
                cbGate.Checked = false;
                cbExit.Checked = false;
                cbLounge.Checked = false;
                cbCheckin.Checked = false;
                cbRunway.Checked = false;
                label1.ForeColor = Color.Red;
            }
            CheckChecked();
        }

        // clears all entries in the view
        private void btnNew_Click(object sender, System.EventArgs e)
        {
            txtACTValue.Text = "";
            txtACT5Value.Text = "";
            txtALTValue.Text = "";
            txtALT3Value.Text = "";
            txtFLNUValue.Text = "";
            txtFLTSValue.Text = "";
            txtNATValue.Text = "";
            txtREGNValue.Text = "";
            txtSTYValue.Text = "";
            txtCallSignValue.Text = "";
        }

        //saves the entries to mySaveFile (for next time call, default) and fill the selection for the flights
        private void btnSave_Click(object sender, System.EventArgs e)
        {
            if (!CheckValid() || !Check_cbAllFields())
            {
                this.DialogResult = DialogResult.None;
                return;
            }
            else
                this.DialogResult = DialogResult.OK;

            this.Hide();

            string txtSep = "|";
            string txtChecked = "";
            string txtView = "<Default>";
            txtChecked += cbUTC.Checked ? "1," : "0,";
            txtChecked += cbPosition.Checked ? "1," : "0,";
            txtChecked += cbGate.Checked ? "1," : "0,";
            txtChecked += cbBelt.Checked ? "1," : "0,";
            txtChecked += cbLounge.Checked ? "1," : "0,";
            txtChecked += cbCheckin.Checked ? "1," : "0,";
            txtChecked += cbRunway.Checked ? "1" : "0";

            string strFrom = UT.DateTimeToCeda(dateTimePickerFrom.Value);
            string strTo = UT.DateTimeToCeda(dateTimePickerTo.Value);

            string txtFile = txtView + txtSep + txtChecked + txtSep + strFrom + txtSep + strTo + txtSep
                + txtFLNUValue.Text + txtSep + txtFLTSValue.Text + txtSep
                + txtALTValue.Text + txtSep + txtALT3Value.Text + txtSep
                + txtACTValue.Text + txtSep + txtACT5Value.Text + txtSep
                + txtNATValue.Text + txtSep + txtSTYValue.Text + txtSep
                + txtREGNValue.Text + txtSep + txtCallSignValue.Text + txtSep;

            txtChecked = "";
            txtChecked += rbChanged.Checked ? "1" : "0";
            txtChecked += txtSep;
            txtChecked += cbArrival.Checked ? "1" : "0";
            txtChecked += txtSep;
            txtChecked += cbDeparture.Checked ? "1" : "0";
            txtChecked += txtSep;
            txtChecked += cbExit.Checked ? "1" : "0";
            txtChecked += txtSep;
            txtChecked += cbTakeOver.Checked ? "1" : "0";
            txtChecked += txtSep;
            txtChecked += rbDeletedFlights.Checked ? "1" : "0";
            txtChecked += txtSep;
            txtChecked += rbFilterButtons.Checked ? "1" : "0";
            txtChecked += txtSep;
            txtFile += txtChecked;//SISL

            //Basic Data Tab controls
            strFrom = UT.DateTimeToCeda(dateTimePickerFrm_BD.Value);
            strTo = UT.DateTimeToCeda(dateTimePickerTo_BD.Value);
            txtChecked = cbTakeOver_BD.Checked ? "1" : "0";

            txtFile += strFrom + txtSep + strTo + txtSep + txtChecked;

            txtFile += txtSep + (cbTowing.Checked ? "1" : "0");

            if (File.Exists(mySaveFile))
            {
                File.Delete(mySaveFile);
            }
            /*
                        StreamWriter datei = File.AppendText(mySaveFile);

                        try
                        {
                            datei.WriteLine(txtFile);
                        }
                        catch (IOException exc)
                        {
                            MessageBox.Show(this, mySaveFile, exc.Message + "Save Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        }
            */
            FileStream fout = null;
            try
            {
                fout = new FileStream(mySaveFile, FileMode.OpenOrCreate);
            }
            catch (IOException exc)
            {
                MessageBox.Show(this, mySaveFile, exc.Message + "Save Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }

            StreamWriter fstr_out = new StreamWriter(fout);
            try
            {
                fstr_out.Write(txtFile);
            }
            catch (IOException exc)
            {
                MessageBox.Show(this, mySaveFile, exc.Message + "Write Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            fstr_out.Close();
            //			datei.Close();

            SaveCCKTabSelection();


            if ((rbDeletedFlights.Checked == true) && ("Flight Data" == omSelectedTabPage))
            {
                FillSelectionDeleted();
            }
            else if (omSelectedTabPage == "Common CKI")
            {
                m_selCKI = FillCKISelection();
            }
            else
            {
                FillSelection();
            }

            

        }
       
        private void SaveCCKTabSelection()
        {
            try
            {
                string txtSep = "|";
                string txtChecked = "";
                string txtFile = "";

                string strOpenFrom = UT.DateTimeToCeda(dtpOpenFrom.Value);
                txtFile += strOpenFrom + txtSep;
                string strOpenTo = UT.DateTimeToCeda(dtpOpenTo.Value);
                txtFile += strOpenTo + txtSep;
                string strCloseFrom = UT.DateTimeToCeda(dtpCloseFrom.Value);
                txtFile += strCloseFrom + txtSep;
                string strCloseTo = UT.DateTimeToCeda(dtpCloseTo.Value);
                txtFile += strCloseTo + txtSep;
                string strCounter = txtCounter.Text.Trim();
                txtFile += strCounter;

                File.Delete(strFileNameCKI);
                FileStream fout = null;
                try
                {
                    fout = new FileStream(strFileNameCKI, FileMode.OpenOrCreate);
                }
                catch (IOException exc)
                {
                    MessageBox.Show(this, strFileNameCKI, exc.Message + "Save Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }

                StreamWriter fstr_out = new StreamWriter(fout);
                try
                {
                    fstr_out.Write(txtFile);
                }
                catch (IOException exc)
                {
                    MessageBox.Show(this, strFileNameCKI, exc.Message + "Write Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
                fstr_out.Close();
                fout.Close();
            }
            catch(Exception)
            {
            }

        }
        private string FillCKISelection()
        {
            string selCKI = "";
            //Scheduled open 
            DateTime dtOpenFrom = dtpOpenFrom.Value;
            DateTime dtOpenTo = dtpOpenTo.Value;
            DateTime datFrom, datTo;
            if (UT.IsTimeInUtc == false)
            {
                datFrom = UT.LocalToUtc(dtOpenFrom);
                datTo = UT.LocalToUtc(dtOpenTo);
            }
            else
            {
                datFrom = dtOpenFrom;
                datTo = dtOpenTo;
            }
            string strOpenFrom = UT.DateTimeToCeda(datFrom);
            string strOpenTo = UT.DateTimeToCeda(datTo);
            selCKI = "  (CKBS >= '" + strOpenFrom + "' AND CKBS <='" + strOpenTo + "') ";

            //Scheduled open 
            DateTime dtCloseFrom = dtpCloseFrom.Value;
            DateTime dtCloseTo = dtpCloseTo.Value;
            if (UT.IsTimeInUtc == false)
            {
                datFrom = UT.LocalToUtc(dtCloseFrom);
                datTo = UT.LocalToUtc(dtCloseTo);
            }
            else
            {
                datFrom = dtCloseFrom;
                datTo = dtCloseTo;
            }
            string strCloseFrom = UT.DateTimeToCeda(datFrom);
            string strCloseTo = UT.DateTimeToCeda(datTo);
            //selCKI += " AND CKES BETWEEN '" + strCloseFrom + "' AND '" + strCloseTo + "'";
            selCKI += "  AND (CKES >= '" + strCloseFrom + "' AND CKES <='" + strCloseTo + "') ";


            //counter
            string strCnt = txtCounter.Text.Trim();
            if (!string.IsNullOrEmpty(strCnt))
            {
                selCKI += " AND CKIC='" + strCnt + "'";
            }

            string txtTimeStyle = " [LOCAL] ";
            if (UT.IsTimeInUtc == true)
                txtTimeStyle = " [UTC] ";

           
            myWindowText = "Common CKI Change:" + txtTimeStyle + "Timeframe for Common CKI [" + dateTimePickerFrom.Value.ToString() + "] - [" +
                dateTimePickerTo.Value.ToString() + "]";
            return selCKI;
        }

       
        //mySelectionDeleted
        private void FillSelectionDeleted()
        {
            string txtTimeStyle = " [LOCAL] ";
            if (UT.IsTimeInUtc == true)
                txtTimeStyle = " [UTC] ";


            myWindowText = "Data Change:" + txtTimeStyle + "Timeframe for Deleted Flights [" + dateTimePickerFrom.Value.ToString() + "] - [" +
                dateTimePickerTo.Value.ToString() + "]";


            DateTime datFrom = dateTimePickerFrom.Value;
            DateTime datTo = dateTimePickerTo.Value;
            string strFrom = "";
            string strTo = "";

            if (UT.IsTimeInUtc == false)
            {
                datFrom = UT.LocalToUtc(datFrom);
                datTo = UT.LocalToUtc(datTo);
            }
            strFrom = UT.DateTimeToCeda(datFrom);
            strTo = UT.DateTimeToCeda(datTo);

            mySelectionDeleted = "AND (STOX BETWEEN '" + strFrom + "' AND '" + strTo + "') AND TRAN = 'D'";

        }
        
        private void FillSelection()
        {
            // If it is Flight Data Tab Page
            if ("Flight Data" == omSelectedTabPage) // Flight Data
            {
                string strFrom = UT.DateTimeToCeda(dateTimePickerFrom.Value);
                string strTo = UT.DateTimeToCeda(dateTimePickerTo.Value);

                string txtTimeStyle = " [LOCAL] ";
                if (UT.IsTimeInUtc == true)
                    txtTimeStyle = " [UTC] ";

                myWindowText = "Location Change:" + txtTimeStyle + "Timeframe for Flights [" + dateTimePickerFrom.Value.ToString() + "] - [" +
                    dateTimePickerTo.Value.ToString() + "]";

                DateTime datFrom = dateTimePickerFrom.Value;
                DateTime datTo = dateTimePickerTo.Value;
                if (UT.IsTimeInUtc == false)
                {
                    datFrom = UT.LocalToUtc(datFrom);
                    datTo = UT.LocalToUtc(datTo);
                }
                strFrom = UT.DateTimeToCeda(datFrom);
                strTo = UT.DateTimeToCeda(datTo);

                mySelection = "WHERE ((TIFA BETWEEN '" + strFrom + "' AND '" + strTo +
                    "') OR (TIFD BETWEEN '" + strFrom + "' and '" + strTo + "'))";

                if (txtFLNUValue.Text != "")
                    //				mySelection += mySelFlnu + txtFLNUValue.Text + myEndSignLike;
                    mySelection += mySelFlnu + txtFLNUValue.Text + myEndSign;
                if (txtFLTSValue.Text != "")
                    mySelection += mySelFlns + txtFLTSValue.Text + myEndSign;
                if (txtALTValue.Text != "")
                    mySelection += mySelAlc2 + txtALTValue.Text + myEndSign;
                if (txtALT3Value.Text != "")
                    mySelection += mySelAlc3 + txtALT3Value.Text + myEndSign;
                if (txtACTValue.Text != "")
                    mySelection += mySelAct3 + txtACTValue.Text + myEndSign;
                if (txtACT5Value.Text != "")
                    mySelection += mySelAct5 + txtACT5Value.Text + myEndSign;
                if (txtNATValue.Text != "")
                {
                    string[] strArr = txtNATValue.Text.Split('/');
                    string strValueTtyp = "";
                    if (strArr.Length >= 1)
                        strValueTtyp = strArr[0];
                    else
                        strValueTtyp = txtNATValue.Text;

                    mySelection += mySelTtyp + strValueTtyp + myEndSign;
                }
                if (txtSTYValue.Text != "")
                {
                    string[] strArr = txtSTYValue.Text.Split('/');
                    string strValueStyp = "";
                    if (strArr.Length >= 1)
                        strValueStyp = strArr[0];
                    else
                        strValueStyp = txtSTYValue.Text;

                    mySelection += mySelStyp + strValueStyp + myEndSign;
                }
                if (txtREGNValue.Text != "")
                {
                    string[] strArr = txtREGNValue.Text.Split('/');
                    string strValueRegn = "";
                    if (strArr.Length >= 1)
                        strValueRegn = strArr[0];
                    else
                        strValueRegn = txtREGNValue.Text;

                    mySelection += mySelRegn + strValueRegn + myEndSign;
                }
                if (txtCallSignValue.Text != "")
                {
                    string[] strArr = txtCallSignValue.Text.Split('/');
                    string strValueCsgn = "";
                    if (strArr.Length >= 1)
                        strValueCsgn = strArr[0];
                    else
                        strValueCsgn = txtCallSignValue.Text;

                    string strCsgn = txtCallSignValue.Text;


                    txtFLNUValue.Text = strCsgn.Substring(3, strCsgn.Length - 3);
                    txtALT3Value.Text = strCsgn.Substring(0, 3);

                    mySelection += mySelCsgn + strValueCsgn + myEndSign;
                }

                //			mySelection += mySelRotation;
                //			mySelection += "";

                mySelection = string.Format("WHERE RKEY IN ( SELECT UNIQUE(RKEY) FROM AFTTAB {0})", mySelection);
                if (!cbArrival.Checked)
                {
                    mySelection += " AND ADID<>'A'";
                }
                if (!cbDeparture.Checked)
                {
                    mySelection += " AND ADID<>'D'";
                }
                if (!cbTowing.Checked)
                {
                    mySelection += " AND NOT (FTYP IN ('T','G'))";
                }

                myLocation.Clear();
                if (cbPosition.Checked)
                    myLocation.Add("PST");
                if (cbGate.Checked)
                    myLocation.Add("GAT");
                if (cbBelt.Checked)
                    myLocation.Add("BLT");
                if (cbLounge.Checked)
                    myLocation.Add("WRO");
                if (cbExit.Checked)
                    myLocation.Add("EXT");
                if (cbCheckin.Checked)
                    myLocation.Add("CCA");
                if (cbRunway.Checked)
                    myLocation.Add("RWY");
            }
            
            else // BasicData Tab Page is the Selection
            {
                string strFrom = UT.DateTimeToCeda(dateTimePickerFrm_BD.Value);
                string strTo = UT.DateTimeToCeda(dateTimePickerTo_BD.Value);

                string txtTimeStyle = " [LOCAL] ";
                if (UT.IsTimeInUtc == true)
                    txtTimeStyle = " [UTC] ";

                myWindowText = "Location Change:" + txtTimeStyle + "Timeframe for Flights [" + dateTimePickerFrom.Value.ToString() + "] - [" +
                    dateTimePickerTo.Value.ToString() + "]";

                DateTime datFrom = dateTimePickerFrm_BD.Value;
                DateTime datTo = dateTimePickerTo_BD.Value;
                if (UT.IsTimeInUtc == false)
                {
                    datFrom = UT.LocalToUtc(datFrom);
                    datTo = UT.LocalToUtc(datTo);
                }
                strFrom = UT.DateTimeToCeda(datFrom);
                strTo = UT.DateTimeToCeda(datTo);

                mySelection = "WHERE (TIME BETWEEN '" + strFrom + "' AND '" + strTo + "')";

                // Add the Filter conditions...
                int i = 0;
                string strValue = "";
                foreach (System.Windows.Forms.Control olc in panel1.Controls)
                {
                    if (olc.GetType().ToString() == "System.Windows.Forms.TextBox")
                    {
                        strValue = olc.Text.Trim();
                        if (strValue != "" && i < arrFieldCodes.Length)
                        {
                            mySelection += " AND " + arrFieldCodes[i] + "='" + strValue + myEndSign;
                        }
                        i++;
                    }
                }
            }
        }
        private void txtALT3Value_Leave(object sender, System.EventArgs e)
        {
            string strValue = txtALT3Value.Text;
            if (strValue == "")
            {
                txtACTUrno.Text = "";
                txtALT3Value.ForeColor = Color.Black;
                return; //Need no check
            }
            ITable myTab = myDB["ALT"];
            bool blFound = false;
            for (int i = 0; i < myTab.Count && blFound == false; i++)
            {
                if (strValue == myTab[i]["ICAO-Code"])
                {
                    txtALTValue.Text = myTab[i]["IATA-Code"];
                    txtACTUrno.Text = myTab[i]["URNO"];
                    blFound = true;
                    break;
                }
            }

            if (blFound == true)
                txtALT3Value.ForeColor = Color.Black;
            else
                txtALT3Value.ForeColor = Color.Red;

            CheckValid();
        }

        private void txtACT5Value_Leave(object sender, System.EventArgs e)
        {
            string strValue = txtACT5Value.Text;
            if (strValue == "")
            {
                txtACTUrno.Text = "";
                txtACT5Value.ForeColor = Color.Black;
                return; //Need no check
            }
            ITable myTab = myDB["ACT"];
            bool blFound = false;
            for (int i = 0; i < myTab.Count && blFound == false; i++)
            {
                if (strValue == myTab[i]["ICAO-Code"])
                {
                    txtACTUrno.Text = myTab[i]["URNO"];
                    blFound = true;
                    break;
                }
            }

            if (blFound == true)
                txtACT5Value.ForeColor = Color.Black;
            else
                txtACT5Value.ForeColor = Color.Red;

            CheckValid();
        }

        private bool Check_cbAllFields()
        {
            bool bLPrompt = false;
            string strPrompt = "";
            if ("Basic Data" == omSelectedTabPage) // Flight Data
            {
                if (cbTable.SelectedIndex == -1)
                {
                    bLPrompt = true;
                    strPrompt = "Please Select Basic Data Table";
                }
            }
            else if (rbChanged.Checked == true)
            {
                if (cbArrival.Checked == false && cbDeparture.Checked == false)
                {
                    bLPrompt = true;
                    strPrompt = "Arrival and/or Departure has to be checked.";
                }
                if (txtFLNUValue.Text.Equals("") || txtALT3Value.Text.Equals(""))
                {
                    if (txtCallSignValue.Text.Equals("")) //nr
                    {
                        bLPrompt = true;
                        strPrompt += "\nFlightnumber and Airline(ICAO-Code) or Call Sign has to be filled.";
                    }
                }
            }

            if (bLPrompt)
            {
                MessageBox.Show(this, strPrompt);
                return false;
            }

            return true;
        }

        private void btnApply_Click(object sender, System.EventArgs e)
        {
            if (!CheckValid() || !Check_cbAllFields())
            {
                this.DialogResult = DialogResult.None;
                return;
            }
            else
                this.DialogResult = DialogResult.OK;


            this.Hide();


            if ((rbDeletedFlights.Checked == true) && ("Flight Data" == omSelectedTabPage))
            {
                FillSelectionDeleted();
            }
            else if ("Common CKI" == omSelectedTabPage)
            {
                m_selCKI = FillCKISelection();
            }
            else
            {
                FillSelection();
            }

        }

        private void frmView_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {

            WriteRegistry();
        }

        private void frmView_Activated(object sender, System.EventArgs e)
        {
            dateTimePickerFrom.Focus();


            /*
            Ufis.Utils.IniFile myIni = new IniFile("C:\\Ufis\\system\\DATACHANGE.INI");

            string strTables = myIni.IniReadValue("BASIC_DATA", "TABLES");

            if(strTables == "")
            {
                this.tabControl1.TabPages.Remove(BasicData);
            }
            */
        }

        private void Check_chDeleted(object sender, System.EventArgs e)
        {
            Check_chDeleted();
        }

        private void Check_chDeleted()
        {
            if (rbChanged.Checked == true)
            {
                if (label1.ForeColor == Color.Red)
                {
                    label1.ForeColor = Color.Lime;
                }
                label1.Enabled = false;
                btnApply.Enabled = true;
                btnSave.Enabled = true;
                cbPosition.Checked = true;
                cbGate.Checked = true;
                cbExit.Checked = true;
                cbBelt.Checked = true;
                cbLounge.Checked = true;
                cbCheckin.Checked = true;
                cbRunway.Checked = true;

                btnApply.Enabled = true;
                btnSave.Enabled = true;

                cbPosition.Enabled = false;
                cbGate.Enabled = false;
                cbBelt.Enabled = false;
                cbLounge.Enabled = false;
                cbCheckin.Enabled = false;
                cbRunway.Enabled = false;
                cbExit.Enabled = false;
                txtFLNUValue.BackColor = Color.Yellow;
                //				txtALTValue.BackColor = Color.Yellow;
                txtALT3Value.BackColor = Color.Yellow;
                txtCallSignValue.BackColor = Color.Yellow;
                cbArrival.Visible = true;
                cbDeparture.Visible = true;
                cbTowing.Visible = true;
                btnALC.Enabled = true;
                btnACR.Enabled = false;
                btnACT.Enabled = false;
                btnNAT.Enabled = false;
                btnSTY.Enabled = false;
                txtSTYValue.Enabled = false;
                txtNATValue.Enabled = false;
                txtACTValue.Enabled = false;
                txtACT5Value.Enabled = false;
                txtREGNValue.Enabled = false;

            }
            else
            {
                cbPosition.Enabled = true;
                cbGate.Enabled = true;
                cbBelt.Enabled = true;
                cbLounge.Enabled = true;
                cbCheckin.Enabled = true;
                cbRunway.Enabled = true;
                cbExit.Enabled = true;
                label1.Enabled = true;
                CheckChecked();

                txtFLNUValue.BackColor = Color.White;
                //				txtALTValue.BackColor = Color.White;
                txtALT3Value.BackColor = Color.White;
                txtCallSignValue.BackColor = Color.White;
                cbArrival.Visible = false;
                cbDeparture.Visible = false;
                cbTowing.Visible = false;
                btnACR.Enabled = true;
                btnACT.Enabled = true;
                btnNAT.Enabled = true;
                btnSTY.Enabled = true;
                txtSTYValue.Enabled = true;
                txtNATValue.Enabled = true;
                txtACTValue.Enabled = true;
                txtACT5Value.Enabled = true;
                txtREGNValue.Enabled = true;
            }
        }

        private void chDeleted_CheckedChanged(object sender, System.EventArgs e)
        {
            if (rbDeletedFlights.Checked == true)
            {
                cbPosition.Enabled = false;
                cbGate.Enabled = false;
                cbBelt.Enabled = false;
                cbLounge.Enabled = false;
                cbCheckin.Enabled = false;
                cbRunway.Enabled = false;
                cbExit.Enabled = false;
                label1.Enabled = false;

                btnACR.Enabled = false;
                btnACT.Enabled = false;
                btnNAT.Enabled = false;
                btnSTY.Enabled = false;
                btnALC.Enabled = false;

                txtFLNUValue.BackColor = Color.LightGoldenrodYellow;
                txtALT3Value.BackColor = Color.LightGoldenrodYellow;
                txtCallSignValue.BackColor = Color.LightGoldenrodYellow;

                txtALTValue.Enabled = false;
                txtFLNUValue.Enabled = false;
                txtFLTSValue.Enabled = false;
                txtALT3Value.Enabled = false;
                txtCallSignValue.Enabled = false;

                txtSTYValue.Enabled = false;
                txtNATValue.Enabled = false;
                txtACTValue.Enabled = false;
                txtACT5Value.Enabled = false;
                txtREGNValue.Enabled = false;

            }
            else
            {
                cbPosition.Enabled = true;
                cbGate.Enabled = true;
                cbBelt.Enabled = true;
                cbLounge.Enabled = true;
                cbCheckin.Enabled = true;
                cbRunway.Enabled = true;
                cbExit.Enabled = true;
                label1.Enabled = true;

                btnACR.Enabled = true;
                btnACT.Enabled = true;
                btnNAT.Enabled = true;
                btnSTY.Enabled = true;
                btnALC.Enabled = true;

                txtFLNUValue.BackColor = Color.White;
                txtALT3Value.BackColor = Color.White;
                txtCallSignValue.BackColor = Color.White;

                txtALTValue.Enabled = true;
                txtFLNUValue.Enabled = true;
                txtFLTSValue.Enabled = true;
                txtALT3Value.Enabled = true;
                txtCallSignValue.Enabled = true;

                txtSTYValue.Enabled = true;
                txtNATValue.Enabled = true;
                txtACTValue.Enabled = true;
                txtACT5Value.Enabled = true;
                txtREGNValue.Enabled = true;
            }
        }

        private void cbArrival_CheckedChanged(object sender, System.EventArgs e)
        {
            //			CheckValid();		
        }

        private void cbDeparture_CheckedChanged(object sender, System.EventArgs e)
        {
            //			CheckValid();		
        }

        private void txtFLNUValue_TextChanged(object sender, System.EventArgs e)
        {
            //			CheckValid();		
        }

        private void txtALT3Value_TextChanged(object sender, System.EventArgs e)
        {
            //			CheckValid();		
        }

        private void txtFLNUValue_KeyPress(object sender, System.Windows.Forms.KeyPressEventArgs e)
        {
            if (!System.Char.IsNumber(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        // toggle for checked/unchecked
        private void cbExit_CheckedChanged(object sender, System.EventArgs e)
        {
            if (cbExit.Checked == true)
                cbExit.BackColor = Color.LightGreen;
            else
                cbExit.BackColor = Color.Transparent;

            cbExit.Refresh();
            CheckChecked();
        }
        private void txtACTUrno_TextChanged(object sender, System.EventArgs e)
        {

        }
        /// <summary>
        /// Validates the Input Call Sign Data.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtCallSignValue_Leave(object sender, System.EventArgs e)
        {
            string strValue = txtCallSignValue.Text;
            if (strValue == "")
            {
                txtACTUrno.Text = "";
                txtCallSignValue.ForeColor = Color.Black;
                return; //Need no check
            }
            if (!m_CSGNloaded)
            {
                LoadCallSignData();
            }

            ITable myTab = myDB["AFT"];
            bool blFound = false;

            if ((myTab == null) || (myTab.Count == 0))
                return;

            string[] strArr = strValue.Split('/');
            string strValueCsgn = "";
            if (strArr.Length >= 1)
                strValueCsgn = strArr[0];

            for (int i = 0; i < myTab.Count && blFound == false; i++)
            {
                if (strValueCsgn == myTab[i]["CallSign"])
                {
                    txtACTUrno.Text = myTab[i]["URNO"];
                    blFound = true;
                    break;
                }
            }
            if (blFound == true)
                txtCallSignValue.ForeColor = Color.Black;
            else
                txtCallSignValue.ForeColor = Color.Red;

            CheckValid();
        }

        /// <summary>
        /// Prepares the Query and Loads the Call Sign data.
        /// </summary>
        private void LoadCallSignData()
        {
            DateTime datFrom = dateTimePickerFrom.Value;
            DateTime datTo = dateTimePickerTo.Value;
            if (UT.IsTimeInUtc == false)
            {
                datFrom = UT.LocalToUtc(datFrom);
                datTo = UT.LocalToUtc(datTo);
            }
            string strFrom = UT.DateTimeToCeda(datFrom);
            string strTo = UT.DateTimeToCeda(datTo);

            string myTimeSelection = "WHERE ((TIFA BETWEEN '" + strFrom + "' AND '" + strTo + "') OR (TIFD BETWEEN '" + strFrom + "' and '" + strTo + "'))";

            m_CSGNloaded = true;
            frmData DataForm = new frmData();
            DataForm.Hide();
            DataForm.LoadAFTCallSignData(myTimeSelection);
        }

        private void rbFilterButtons_CheckedChanged(object sender, System.EventArgs e)
        {
            if (rbFilterButtons.Checked == true)
            {
                cbPosition.Enabled = true;
                cbGate.Enabled = true;
                cbBelt.Enabled = true;
                cbLounge.Enabled = true;
                cbCheckin.Enabled = true;
                cbRunway.Enabled = true;
                cbExit.Enabled = true;
                label1.Enabled = true;

                btnACR.Enabled = true;
                btnACT.Enabled = true;
                btnNAT.Enabled = true;
                btnSTY.Enabled = true;

                txtSTYValue.Enabled = true;
                txtFLTSValue.Enabled = true;
                txtNATValue.Enabled = true;
                txtACTValue.Enabled = true;
                txtACT5Value.Enabled = true;
                txtREGNValue.Enabled = true;

            }

        }
        private void OnTableChange(object sender, System.EventArgs e)
        {
            RemovePanelcontrols();

            int indexTable = cbTable.SelectedIndex; // Get Selected Item Index
            omTableCode = arrTableCodes[indexTable];

            Ufis.Utils.IniFile myIni = new IniFile("C:\\Ufis\\system\\DATACHANGE.INI");

            string strTables = myIni.IniReadValue(omTableCode, "IDENTIFIER");
            arrFieldCodes = strTables.Split(',');
            string[] arrFieldNames = new string[arrFieldCodes.Length];

            // Get the Field Names using Field codes
            omSysTab = myDB.Bind("SYS", "SYS", "URNO,FINA,TANA,ADDI,LOGD", "10,4,3,32,7", "URNO,FINA,TANA,ADDI");
            omSysTab.Clear();
            string StrTmpWhere = "WHERE TANA='" + arrTableCodes[indexTable] + "' ORDER BY FINA";
            omSysTab.Load(StrTmpWhere);

            bool bFound = false;
            string strTmp = "";
            for (int i = 0; i < arrFieldCodes.Length; i++)
            {
                for (int j = 0; j < omSysTab.Count; j++)
                {
                    // Check for the Field Code
                    if (arrFieldCodes[i] == omSysTab[j]["FINA"])
                    {
                        strTmp = omSysTab[j]["ADDI"]; //First Character with UpperCase
                        if (strTmp.Length > 1)
                        {
                            strTmp = strTmp.Substring(0, 1).ToUpper() + strTmp.Substring(1);
                        }

                        arrFieldNames[i] = strTmp;
                        bFound = true;
                        break;
                    }
                }
                if (bFound == false) //If the Field codes in the INI file are wrong
                {
                    MessageBox.Show("Invalid Field Codes in the INI file", "Daga Changes", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                bFound = false;
            }

            // Add the controls for the Selected Tab
            int pos_Y = 6;
            string strCtrlName = "";
            for (int i = 0; i < arrFieldCodes.Length; i++)
            {
                strCtrlName = "lbl" + i.ToString();
                AddControl(new Label(), new Point(32, pos_Y), new Size(130, 25), arrFieldNames[i], strCtrlName);

                strCtrlName = "txtBox" + i.ToString();
                AddControl(new TextBox(), new Point(170, pos_Y), new Size(128, 20), " ", strCtrlName);

                pos_Y += 30;
            }
            panel1.Refresh();
        }
        /// <summary>
        /// 
        /// </summary>
        private void CreateTableNames()
        {

            //Read the DATACHANGE.INI file and display the corresponding Table names in the combo Box
            Ufis.Utils.IniFile myIni = new IniFile("C:\\Ufis\\system\\DATACHANGE.INI");

            string strTables = myIni.IniReadValue("BASIC_DATA", "TABLES");
            string strTablesToAdd = string.Empty;
            string strTableNameToAdd = string.Empty;

            arrTableCodes = strTables.Split(',');
            

            if (arrTableCodes.Length > 0)
            {
                omTabTab = myDB.Bind("TAB", "TAB", "URNO,Table-Code,Table-Name", "10,3,32", "URNO,LTNA,LNAM");
                omTabTab.Clear();
                omTabTab.Load("ORDER BY LTNA");

                bool bFound = false;
                for (int i = 0; i < arrTableCodes.Length; i++)
                {
                    for (int j = 0; j < omTabTab.Count; j++)
                    {
                        // Add the Table Name
                        if (arrTableCodes[i] == omTabTab[j]["Table-Code"])
                        {
                            if (string.IsNullOrEmpty(strTablesToAdd))
                            {
                                strTablesToAdd = arrTableCodes[i];
                            }
                            else
                            {
                                strTablesToAdd = strTablesToAdd + "," + arrTableCodes[i];
                            }

                            bFound = true; 
                            break;
                        }
                    }
                    if (bFound == false) //If the Table codes in the file are wrong
                    {
                        //MessageBox.Show("Invalid Table Codes in the INI file", "Daga Changes", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        //return;
                       
                        //MessageBox.Show("The table " + arrTableCodes[i] + " is not found in Database!", "Data Changes", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    bFound = false;
                }
                if (strTablesToAdd.Length > 0)
                {
                    arrTableCodes = strTablesToAdd.Split(',');
                    arrTableNames = new string[arrTableCodes.Length];
                    for (int i = 0; i < arrTableCodes.Length; i++)
                    {
                        for (int j = 0; j < omTabTab.Count; j++)
                        {
                            // Add the Table Name
                            if (arrTableCodes[i] == omTabTab[j]["Table-Code"])
                            {
                                arrTableNames[i] = omTabTab[j]["Table-Name"];
                            }
                        }

                    }
                    
                    cbTable.Items.AddRange(arrTableNames);
                }
            }
        }

        private void AddControl(Control aControl, Point Location, Size ctrlSize, string strText, string strName)
        {
            aControl.Location = Location;
            //			aControl.BackColor = System.Drawing.Color.Transparent;
            aControl.Size = ctrlSize;
            aControl.Text = strText;
            aControl.TabIndex = TabIndex;
            aControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
            aControl.Name = strName;
            panel1.Controls.Add(aControl);
        }
        /// <summary>
        /// Remove all the Dynamic Controls
        /// </summary>
        private void RemovePanelcontrols()
        {
            panel1.Controls.Clear();
            panel1.Refresh();

            foreach (System.Windows.Forms.Control olc in panel1.Controls)
            {
                panel1.Controls.Remove(olc);
            }


        }

        /// <summary>
        /// Select the Selected TabPage
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TabPageChanged(object sender, System.EventArgs e)
        {
            omSelectedTabPage = this.tabControl1.SelectedTab.Text;

            if (omSelectedTabPage == "Basic Data" && bFirstTime == true)
            {
                CreateTableNames();
                bFirstTime = false;
            }

            if (omSelectedTabPage == "Basic Data")
            {
                this.btnSave.Enabled = false;
                this.btnNew.Enabled = false;
            }
            else
            {
                this.btnNew.Enabled = true;
                this.btnSave.Enabled = true;
            }
        }

        private void cbTable_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            if (omSelectedTabPage == "Basic Data")
            {
                this.btnSave.Enabled = false;
                this.btnNew.Enabled = false;
            }
            else
            {
                this.btnNew.Enabled = true;
                this.btnSave.Enabled = true;
            }
        }

        private void pictureBox4_Paint(object sender, PaintEventArgs e)
        {
            UT.DrawGradient(e.Graphics, 90f, pictureBox4, Color.AliceBlue, Color.LightSteelBlue);
        }

        //private void groupBox1_Paint(object sender, PaintEventArgs e)
        //{
        //    UT.DrawGradient(e.Graphics, 90f, groupBox1, Color.AliceBlue, Color.LightSteelBlue);//Color.CornflowerBlue);
        //}

        //private void groupBox2_Paint(object sender, PaintEventArgs e)
        //{
        //    UT.DrawGradient(e.Graphics, 90f, groupBox2, Color.AliceBlue, Color.LightSteelBlue);//Color.CornflowerBlue);
        //}

       
    }
}
