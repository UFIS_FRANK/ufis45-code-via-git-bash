﻿using System;
using System.Linq;
using System.Collections.Generic;

using Ufis.Data;
using Ufis.Entities;
using Ufis.MVVM.ViewModel;
using Ufis.AllocationPlan.DataAccess;
using Ufis.AllocationPlan.Entities;



namespace Ufis.AllocationPlan
{
    public class AllocationPlanViewModel : WorkspaceViewModel
    {
        public WorkspaceViewModel _workspace;
        IList<entlcAllocationPlan> _itemSource;

        public void GetAllocationPlanList(string sFrom, string sTo)
        {
            //Get Arrival Flight by date and time range
            EntityCollectionBase<EntDbFlight> objArrivalFlights = DlAllocationPlan.LoadArrivalFlightListByDateRange(sFrom, sTo);            
            
            IList<entlcAllocationPlan> objAllocationPlan = new List<entlcAllocationPlan>();

            foreach (EntDbFlight tmpArrivalFlights in objArrivalFlights)
            {
                entlcAllocationPlan TmpobjAllocationPlan = new entlcAllocationPlan();
                {

                    EntityCollectionBase<EntDbFlight> objDepartureFlight = DlAllocationPlan.GetDepartureFlightByRKEY(tmpArrivalFlights.RelationKey.Value, tmpArrivalFlights.Urno);

                    TmpobjAllocationPlan.Arrival= tmpArrivalFlights.FullFlightNumber;
                    TmpobjAllocationPlan.From = tmpArrivalFlights.OriginAirportIATACode;
                    TmpobjAllocationPlan.STA = tmpArrivalFlights.StandardTimeOfArrival.Value.ToString("hh:mm").Replace(":","");

                    if (objDepartureFlight.Count > 0)
                    {                        
                        foreach (EntDbFlight qResult in objDepartureFlight)
                        {                           
                            
                            TmpobjAllocationPlan.Departure=  qResult.FullFlightNumber;
                            TmpobjAllocationPlan.TO = qResult.DestinationAirportIATACode;
                            TmpobjAllocationPlan.STD= qResult.StandardTimeOfDeparture.Value.ToString("hh:mm").Replace(":","");
                            TmpobjAllocationPlan.DPax = qResult.TotalNumberOfPassengers.ToString();

                            EntityCollectionBase<EntDbCheckInCounterAllocation> objCICAllocation = DlAllocationPlan.GetCheckInCounter(qResult.FullFlightNumber, qResult.Urno);
                            
                            foreach (EntDbCheckInCounterAllocation sResult in objCICAllocation)
                            {
                                TmpobjAllocationPlan.Counter = sResult.CheckInCounter; 
                            }

                            objCICAllocation = null;
                            objDepartureFlight = null;
                        }
                    }
                    else
                    {
                        TmpobjAllocationPlan.Departure=  "";
                        TmpobjAllocationPlan.TO = "";
                        TmpobjAllocationPlan.STD="";
                        TmpobjAllocationPlan.DPax = "";
                    }
                    TmpobjAllocationPlan.Type = tmpArrivalFlights.AircraftIATACode;
                    TmpobjAllocationPlan.Reg = tmpArrivalFlights.RegistrationNumber;
                    //Need to check for if data only have departure
                    TmpobjAllocationPlan.Stand = tmpArrivalFlights.PositionOfArrival;                    
                    TmpobjAllocationPlan.Gate = tmpArrivalFlights.GateOfArrival1;
                    TmpobjAllocationPlan.Belt = tmpArrivalFlights.Belt1;
                    TmpobjAllocationPlan.APax = tmpArrivalFlights.TotalNumberOfPassengers.ToString();                    
                }
                objAllocationPlan.Add(TmpobjAllocationPlan);
                TmpobjAllocationPlan = null;
            }           
            
            ItemSource = objAllocationPlan;    
        }
            
        
        
        
        public IList<entlcAllocationPlan> ItemSource
        {
            get
            {
                return _itemSource;
            }
            protected set
            {
                if (_itemSource != value)
                {
                    _itemSource = value;
                    OnPropertyChanged("ItemSource");
                }
            }
        }

    }

}
