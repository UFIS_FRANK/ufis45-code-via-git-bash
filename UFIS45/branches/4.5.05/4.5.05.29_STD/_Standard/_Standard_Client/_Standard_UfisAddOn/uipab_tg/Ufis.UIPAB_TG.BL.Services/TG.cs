using System;
using System.Collections;
using System.Text;
using System.Data;

using Ufis.UIPAB_TG.BL;
using Ufis.Data;
using Ufis.Utils;

namespace Ufis.UIPAB_TG.BL.Services
{
	/// <summary>
	/// Summary description for TG.
	/// </summary>
	public class TG
	{
		private static Basic basicService = null;
		private DateTime from = DateTime.UtcNow;
		private DateTime to	  = DateTime.UtcNow;

		public TG(Basic basicService,DateTime from,DateTime to)
		{
			//
			// TODO: Add constructor logic here
			//
			if (TG.basicService == null)
			{
				TG.basicService = basicService;
				this.from = from;
				this.to	  = to;

				basicService.OnWhereClause += new Basic.WhereClause(WhereClauseHandler);

				TG.basicService.AddDatabaseSchema("C:\\Ufis_Bin\\Debug\\TG_Schema.xsd","C:\\Ufis_Bin\\Debug\\TG_Schema.xml");
				TG.basicService.CreateDatabaseBindings();
			}
		}

		public bool SetTimeFrame(DateTime from,DateTime to)
		{
			this.from = from;
			this.to	  = to;
			return true;
		}

		public bool LoadDatabase()
		{
			return TG.basicService.LoadDatabase();
		}

		public bool GetBusinessCaseData(string businessCase,out DataSet ds)
		{
			DataSet flight = new Ufis.UIPAB_TG.BL.Entities.TG_Flight();
			return TG.basicService.FillInitialDataSet(businessCase,out ds);
		}

		public bool GetFlightData(out Entities.TG_Flight flight)
		{
			flight = null;
			flight = new Ufis.UIPAB_TG.BL.Entities.TG_Flight();
			//========================== ACT Data
			ITable iActTab = TG.basicService.Database["AircraftTypes"];

			//=================== AFT Data 
			ITable iAftTab = TG.basicService.Database["Flight"];

			//======== ACT data to DataSet

			for (int i = 0; i < iActTab.Count; i++)
			{
				IRow row = iActTab[i];
				Entities.TG_Flight.AircraftTypeRow  actRow = flight.AircraftType.NewAircraftTypeRow();
				actRow.Code3 = row["ACT3"];
				flight.AircraftType.AddAircraftTypeRow(actRow);
			}

			//========================== CCA Data
			ITable iCcaTab = TG.basicService.Database["Counter"];

			for (int i = 0; i < iAftTab.Count; i++)
			{
				IRow row = iAftTab[i];

				Entities.TG_Flight.FlightRow flightRow = flight.Flight.NewFlightRow();
				flightRow.FlightNumber = row["FLNO"];
				flightRow.Id		   = row["URNO"];
				flightRow.Type		   = row["ADID"];
				flightRow.AcCode 	   = row["ACT3"];
				if (flightRow.Type == "A")
					flightRow.ActualTime = row["TIFA"];
				else
					flightRow.ActualTime = row["TIFD"];

				flight.Flight.AddFlightRow(flightRow);

				IRow[] ccaRows = TG.basicService.GetMultipleRowsFromTableIndex("Counter","FLNU",row["URNO"]);
				for (int j = 0; j < ccaRows.Length; j++)
				{
					Entities.TG_Flight.CounterRow counterRow = flight.Counter.NewCounterRow();
					counterRow.Begin = UT.CedaFullDateToDateTime ( ccaRows[j]["CKBS"]);
					counterRow.End   = UT.CedaFullDateToDateTime ( ccaRows[j]["CKES"].ToString());
					counterRow.Flnu  = ccaRows[j]["FLNU"];
					counterRow.Id	 = ccaRows[j]["URNO"];
					counterRow.Counter = ccaRows[j]["CKIC"];
					flight.Counter.AddCounterRow(counterRow);
				}

			}

			return true;
		}

		private string WhereClauseHandler(string logicalTableName)
		{
			string where = string.Empty;

			switch(logicalTableName)
			{
				case "Flight":
					where = string.Format("WHERE ((ADID = 'A' AND TIFA BETWEEN '{0}' AND '{1}') OR (ADID = 'D' AND TIFD BETWEEN '{0}' AND '{1}')) [ROTATIONS]  ORDER BY JCNT DESC",UT.DateTimeToCeda(this.from),UT.DateTimeToCeda(this.to));
					break;
				case "AircraftTypes":
					where = "ORDER BY ACT3";
					break;
				case "Counter":
					StringBuilder str = new StringBuilder();
			
					ITable iAftTab = TG.basicService.Database["Flight"];
					for( int i = 0; i < iAftTab.Count; i++)
					{
						if(i + 1 < iAftTab.Count)
							str.Append(iAftTab[i]["URNO"] + ",");
						else
							str.Append(iAftTab[i]["URNO"]);
					}
					where = string.Format("WHERE FLNU IN ({0})", str.ToString());

					break;
			}

			return where;
		}

		public bool Validate(DataRow row,out ArrayList errorMessages)
		{
			return TG.basicService.Validate(row,out errorMessages);
		}

		public bool Save(DataSet ds,out ArrayList errorMessages)
		{
			return TG.basicService.Save(ds,out errorMessages);
		}

		public bool GetBusinessDataPattern(DataColumn column,out string pattern)
		{
			return TG.basicService.GetBusinessDataPattern(column,out pattern);	
		}

		public bool InsertRow(DataTable dt,out ArrayList errorMessages)
		{
			return TG.basicService.InsertRow(dt,out errorMessages);
		}

		public bool DeleteRow(DataRow dr,out ArrayList errorMessages)
		{
			return TG.basicService.DeleteRow(dr,out errorMessages);
		}

	}
}
