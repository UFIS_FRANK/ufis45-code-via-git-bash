/// Job Information 
///   Related to Database
/// AUNG MOE : 20080708
///
///  JOBTAB
///  ======
///      UAFT (Flight URNO)
///          Arrival Flight Urno 
///                  if (DETY = 1) Arrival Demand
///          Departure Flight Urno 
///                  if (DETY = 2) Departure Demand
///                  if (DETY = 0) Turnaround Demand
///      UJTY
///          2000 ==> STAFF
///          2020 ==> EQUIPMENT
///          2007 ==> EMP/Shift
///      
///      RETY (Resource Type)
///          100 ==> Personal
///          010 ==> Equipment
///          001 ==> Location
///
///      DETY (Demand Type)
///          1 ==> Arrival Demand (UAFT will be Arrival UAFT)
///          2 ==> Departure Demand (UAFT will be Departure UAFT)
///          0 ==> Turnaround Demand (UAFT will be Departure UAFT)
///              Same Job Record will be used for Arrival Flight as well
///
///      UTPL (Job Template)
///          Take APPL='RULE_AFT'
///

using System;
using System.Collections.Generic;
using System.Text;

using System.Collections;
using System.Data;

using Ufis.Data;
using Ufis.Utils;

using Ufis.Status_Manager.Ent;
using Ufis.Status_Manager.DS;

namespace Ufis.Status_Manager.DB
{
    public class DbJob
    {
        private static DbJob _this = null;
        private static object _lockJobTableProp = new object();
        private static object _lockForJobTableLoading = new object();

        private IDatabase _myDB = null;
        private ITable _jobTable = null;

        private IDatabase MyDb
        {
            get
            {
                if (_myDB == null) _myDB = UT.GetMemDB();
                return _myDB;
            }
        }

        private ITable JobTable
        {
            get
            {
                _jobTable = MyDb["JOB"];
                if (_jobTable == null)
                {
                    lock (_lockJobTableProp)
                    {
                        _jobTable = MyDb["JOB"];//Get and check again
                        if (_jobTable == null)
                        {
                            //_jobTable = MyDb.Bind("JOB", "JOB",
                            //    "URNO,UAFT,UTPL,USTF,UEQU,UDSR,UJTY,FCCO,ACFR,ACTO,DETY,WGPC",
                            //    "10,10,10,10,10,10,10,10,14,14,1,5",
                            //    "URNO,UAFT,UTPL,USTF,UEQU,UDSR,UJTY,FCCO,ACFR,ACTO,DETY,WGPC");
                            _jobTable = MyDb.Bind("JOB", "JOB",
                                "URNO,UAFT,UTPL,USTF,UEQU,UDSR,UJTY,FCCO,DETY,WGPC",
                                "10,10,10,10,10,10,10,10,1,5",
                                "URNO,UAFT,UTPL,USTF,UEQU,UDSR,UJTY,FCCO,DETY,WGPC");
                            _jobTable.UseTrackChg = true;
                            _jobTable.TrackChgImmediately = false;//Not to track the changes until "StartTrackChg" for the particular row.
                        }
                    }
                }
                return _jobTable;
            }
            set
            {
                _jobTable = value;
            }
        }

        public bool AllowToSaveToDatabase
        {
            get
            {
                return JobTable.AllowToSaveToDatabase;
            }
            set
            {
                JobTable.AllowToSaveToDatabase = value;
            }
        }

        public ITable GetJobTable()
        {
            return JobTable;
        }

        private DbJob()
        {
        }

        public static DbJob GetInstance()
        {
            if (_this == null) _this = new DbJob();
            return _this;
        }

        private static void LogMsg(string msg)
        {
            UT.LogMsg("DbJob:" + msg );
        }

        public void LockJobTableForWrite( )
        {
            LogMsg("B4 Lock Job Write");
            JobTable.Lock.AcquireWriterLock(System.Threading.Timeout.Infinite);
            LogMsg("AF Lock Job Write");
        }

        public System.Threading.LockCookie UpgradeToWrite()
        {
            System.Threading.LockCookie lc;
            lc = JobTable.Lock.UpgradeToWriterLock(new TimeSpan(0, 5, 0));
            return lc;
        }

        public void DowngradFromWrite(ref System.Threading.LockCookie lc)
        {
            JobTable.Lock.DowngradeFromWriterLock(ref lc);
        }

        public void ReleaseJobTableWrite()
        {
            if (JobTable.Lock.IsWriterLockHeld)
            {
                LogMsg("B4 Release Lock Job Write");
                JobTable.Lock.ReleaseWriterLock();
                LogMsg("AF Release Lock Job Write");
            }
        }

        public void LockJobTableForRead()
        {
            LogMsg("B4 Lock Job Read");
            JobTable.Lock.AcquireReaderLock(System.Threading.Timeout.Infinite);
            LogMsg("AF Lock Job Read");
        }

        public void ReleaseJobTableRead()
        {
            if (JobTable.Lock.IsReaderLockHeld)
            {
                LogMsg("B4 Release Lock Job Read");
                JobTable.Lock.ReleaseReaderLock();
                LogMsg("AF Release Lock Job Read");
            }
        }

        /// <summary>
        /// Add Job Info record to DsJob
        /// </summary>
        /// <param name="row">Memory Db row of a job</param>
        /// <param name="dsJob">Job Dataset</param>
        /// <param name="arrUaft">arrival Flight Uaft</param>
        /// <param name="depUaft">departure Flight Uaft</param>
        private void AddJobInfoToDsJob(IRow row, DSJob dsJob, string arrUaft, string depUaft)
        {
            DSJob.JOBRow dtRow = dsJob.JOB.NewJOBRow();

            //dtRow.ACFR = row["ACFR"].ToString();
            //dtRow.ACTO = row["ACTO"].ToString();
            dtRow.FCCO = row["FCCO"].ToString();

            dtRow.URNO = row["URNO"].ToString();
            dtRow.UAFT = row["UAFT"].ToString();
            dtRow.UTPL = row["UTPL"].ToString();
            dtRow.USTF = row["USTF"].ToString();
            dtRow.UEQU = row["UEQU"].ToString();
            dtRow.UDSR = row["UDSR"].ToString();
            dtRow.UJTY = row["UJTY"].ToString();
            dtRow.DETY = row["DETY"].ToString();
            //dtRow.WGPC = row["WGPC"].ToString();
            dtRow.WGPC = GetWgpc(dtRow.UDSR);

            if ((dtRow.USTF==null) || (dtRow.USTF == "0")) dtRow.USTF = "";
            if ((dtRow.UEQU==null) || (dtRow.UEQU == "0")) dtRow.UEQU = "";
            if ((dtRow.UDSR == null) || (dtRow.UDSR == "0")) dtRow.UDSR = "";

            dsJob.JOB.AddJOBRow(dtRow);
            if (dtRow.DETY == "0") //Turnaround demand
            {                
                DSJob.JOBRow dtTurnRow = dsJob.JOB.NewJOBRow();
                foreach (DataColumn col in dsJob.JOB.Columns)
                {
                    dtTurnRow[col.ColumnName] = dtRow[col.ColumnName];
                }
                if (dtRow.UAFT == depUaft)
                {//Original Info is departure flight with turnaround demand
                    //Add same infor except URNO with 'A' Prefix and UAFT as arrUaft
                    dtTurnRow.URNO = "A" + dtRow.URNO;
                    dtTurnRow.UAFT = arrUaft;
                    dsJob.JOB.AddJOBRow(dtTurnRow);
                }
                else if (dtRow.UAFT==arrUaft)
                {//Original Info is arrival flight with turnaround demand
                    //Add same infor except URNO with 'D' Prefix and UAFT as arrUaft
                    dtTurnRow.URNO = "D" + dtRow.URNO;
                    dtTurnRow.UAFT = depUaft;
                    dsJob.JOB.AddJOBRow(dtTurnRow);
                }
            }
        }

        //public void GetJobInfoForFlight(string uaft, DSJob dsJob,
        //    out string[] ustfStArr, out string[] uequStArr, out string[] udrrStArr)
        //{
        //    ustfStArr = null;
        //    uequStArr = null;
        //    udrrStArr = null;

        //    Hashtable htUstf = new Hashtable();
        //    Hashtable htUequ = new Hashtable();
        //    Hashtable htUdrr = new Hashtable();
        //    try
        //    {
        //        LockJobTableForRead();
        //        IRow[] rows = JobTable.RowsByIndexValue("UAFT", uaft);
        //        if (rows != null)
        //        {
        //            int cnt = rows.Length;
        //            for (int i = 0; i < cnt; i++)
        //            {
        //                IRow row = rows[i];
        //                AddJobInfoToDsJob(row, dsJob);

        //                #region Populate USTF Data
        //                try
        //                {
        //                    string ustf = row["USTF"].ToString();
        //                    if ((ustf != "") && (ustf!="0"))
        //                        htUstf.Add(ustf, ustf);
        //                }
        //                catch (Exception) { }
        //                #endregion
        //                #region Populate UEQU Data
        //                try
        //                {
        //                    string uequ = row["UEQU"].ToString();
        //                    if ((uequ != "") && (uequ!="0"))
        //                        htUequ.Add(uequ, uequ);
        //                }
        //                catch (Exception) { }
        //                #endregion
        //                #region Populate UDSR Data
        //                try
        //                {
        //                    string udrr = row["UDSR"].ToString();
        //                    if ((udrr != "") && (udrr!="0"))
        //                        htUdrr.Add(udrr, udrr);
        //                }
        //                catch (Exception) { }
        //                #endregion
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    finally
        //    {
        //        ReleaseJobTableRead();
        //    }

        //    #region Prepare USTF Array
        //    ustfStArr = new string[htUstf.Count];
        //    int idxUstf = 0;
        //    foreach (object key in htUstf.Keys)
        //    {
        //        ustfStArr[idxUstf] = key.ToString();
        //        idxUstf++;
        //    }
        //    #endregion

        //    #region Prepare UEQU Array
        //    uequStArr = new string[htUequ.Count];
        //    int idxUequ = 0;
        //    foreach (object key in htUequ.Keys)
        //    {
        //        uequStArr[idxUequ] = key.ToString();
        //        idxUequ++;
        //    }
        //    #endregion

        //    #region Prepare UDRR Array
        //    udrrStArr = new string[htUdrr.Count];
        //    int idxUdrr = 0;
        //    foreach (object key in htUdrr.Keys)
        //    {
        //        udrrStArr[idxUdrr] = key.ToString();
        //        idxUdrr++;
        //    }
        //    #endregion
        //}

        public void GetJobInfoForFlight(string arrUaft, string depUaft, DSJob dsJob,
            out string[] ustfStArr, out string[] uequStArr, out string[] udrrStArr)
        {
            ustfStArr = null;
            uequStArr = null;
            udrrStArr = null;

            Hashtable htUstf = new Hashtable();
            Hashtable htUequ = new Hashtable();
            Hashtable htUdrr = new Hashtable();
            try
            {
                LockJobTableForRead();
                #region Process for Departure Flight
                
                IRow[] rows = JobTable.RowsByIndexValue("UAFT", depUaft);
                if (rows != null)
                {
                    int cnt = rows.Length;
                    for (int i = 0; i < cnt; i++)
                    {
                        IRow row = rows[i];
                        AddJobInfoToDsJob(row, dsJob, arrUaft, depUaft);

                        #region Populate USTF Data
                        try
                        {
                            string ustf = row["USTF"].ToString();
                            if ((ustf != "") && (ustf != "0"))
                                htUstf.Add(ustf, ustf);
                        }
                        catch (Exception) { }
                        #endregion
                        #region Populate UEQU Data
                        try
                        {
                            string uequ = row["UEQU"].ToString();
                            if ((uequ != "") && (uequ != "0"))
                                htUequ.Add(uequ, uequ);
                        }
                        catch (Exception) { }
                        #endregion
                        #region Populate UDSR Data
                        try
                        {
                            string udrr = row["UDSR"].ToString();
                            if ((udrr != "") && (udrr != "0"))
                                htUdrr.Add(udrr, udrr);
                        }
                        catch (Exception) { }
                        #endregion
                    }
                }
                #endregion
                #region Process for Arrival Flight
                rows = JobTable.RowsByIndexValue("UAFT", arrUaft);
                if (rows != null)
                {
                    int cnt = rows.Length;
                    for (int i = 0; i < cnt; i++)
                    {
                        IRow row = rows[i];
                        AddJobInfoToDsJob(row, dsJob, arrUaft, depUaft);

                        #region Populate USTF Data
                        try
                        {
                            string ustf = row["USTF"].ToString();
                            if ((ustf != "") && (ustf != "0"))
                                htUstf.Add(ustf, ustf);
                        }
                        catch (Exception) { }
                        #endregion
                        #region Populate UEQU Data
                        try
                        {
                            string uequ = row["UEQU"].ToString();
                            if ((uequ != "") && (uequ != "0"))
                                htUequ.Add(uequ, uequ);
                        }
                        catch (Exception) { }
                        #endregion
                        #region Populate UDSR Data
                        try
                        {
                            string udrr = row["UDSR"].ToString();
                            if ((udrr != "") && (udrr != "0"))
                                htUdrr.Add(udrr, udrr);
                        }
                        catch (Exception) { }
                        #endregion
                    }
                }
                #endregion
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                ReleaseJobTableRead();
            }

            #region Prepare USTF Array
            ustfStArr = new string[htUstf.Count];
            int idxUstf = 0;
            foreach (object key in htUstf.Keys)
            {
                ustfStArr[idxUstf] = key.ToString();
                idxUstf++;
            }
            #endregion

            #region Prepare UEQU Array
            uequStArr = new string[htUequ.Count];
            int idxUequ = 0;
            foreach (object key in htUequ.Keys)
            {
                uequStArr[idxUequ] = key.ToString();
                idxUequ++;
            }
            #endregion

            #region Prepare UDRR Array
            udrrStArr = new string[htUdrr.Count];
            int idxUdrr = 0;
            foreach (object key in htUdrr.Keys)
            {
                udrrStArr[idxUdrr] = key.ToString();
                idxUdrr++;
            }
            #endregion
        }

        private Hashtable _htWgpc = new Hashtable();

        private void AddWgpc(string udsr, string wgpc)
        {
            try
            {
                if (_htWgpc.ContainsKey(udsr)) _htWgpc[udsr] = wgpc;
                else _htWgpc.Add(udsr, wgpc);
            }
            catch (Exception)
            {
            }
        }

        private string GetWgpc(string udsr)
        {
            string wgpc = "";

            if ((udsr == null) || (udsr == "") || (udsr == "0"))
            {
                //Do nothing.
            }
            else
            {
                lock (_lockForJobTableLoading)
                {
                    if (_htWgpc.ContainsKey(udsr))
                    {
                        wgpc = _htWgpc[udsr].ToString();
                    }
                    else
                    {
                        wgpc = LoadAndGetWgpc(udsr);
                    }
                }
            }
            return wgpc;
        }


        private string LoadAndGetWgpc(string udsr)
        {
            String wgpc = "";
            System.Threading.LockCookie lc=new System.Threading.LockCookie();
            bool hasReaderLock = JobTable.Lock.IsReaderLockHeld;

            try
            {
                if (hasReaderLock) { lc = UpgradeToWrite(); }
                else LockJobTableForWrite();
                string strWhere = string.Format(" WHERE (UDSR='{0}') AND (UJTY ='2007')", udsr);

                JobTable.Load(strWhere);
                IRow[] rows = JobTable.RowsByIndexValue("UDSR", udsr);
                if ((rows != null) && (rows.Length > 0))
                {
                    int cnt = rows.Length;
                    for (int i = 0; i < cnt; i++)
                    {
                        if (rows[i]["UJTY"].Trim() == "2007")
                        {
                            wgpc = rows[i]["WGPC"];
                            if (wgpc == "") continue;
                            string uDsr = rows[i]["UDSR"].Trim();                            
                            AddWgpc(uDsr, wgpc);
                            break;
                        }
                    }
                }
            }
            catch (Exception)
            {
            }
            finally
            {
                if (hasReaderLock) DowngradFromWrite(ref lc);
                else ReleaseJobTableWrite();
            }
            return wgpc;
        }

        private void LoadWgpc()
        {
            int rowCnt = JobTable.Count ;
            Hashtable htUdsr = new Hashtable();
            for (int i = 0; i < rowCnt; i++)
            {
                IRow row = JobTable[i];
                string uDsr = row["UDSR"].Trim();
                if (string.IsNullOrEmpty(uDsr) || uDsr == "0") continue;
                if (!htUdsr.ContainsKey(uDsr))
                {
                    htUdsr.Add(uDsr, uDsr);
                }
            }

            int udsrCnt = htUdsr.Count;
            int cnt = 0;
            StringBuilder sb = new StringBuilder();
            ArrayList arrUdsr = new ArrayList();
            foreach (DictionaryEntry de in htUdsr)
            {
                string uDsr = de.Key.ToString().Trim();
                if (cnt == 200)
                {
                    arrUdsr.Add(sb.ToString());
                    sb = null;
                    sb = new StringBuilder();
                    cnt = 0;
                }
                cnt++;
                if (cnt == 1) sb.Append("'" + uDsr + "'");
                else sb.Append(",'" + uDsr + "'");
            }
            if (cnt != 0)
            {
                arrUdsr.Add(sb.ToString());
                sb = null;
            }
            LoadWgpc(arrUdsr);
            cnt = JobTable.Count;
        }

        private void LoadWgpc(ArrayList arrUdsr)
        {
            try
            {
                LockJobTableForWrite();
                int cnt = arrUdsr.Count;
                for (int i = 0; i < cnt; i++)
                {
                    if (arrUdsr[i] == null) continue;
                    string st = arrUdsr[i].ToString();
                    if ((st == null) || (st.Trim() == "")) continue;
                    string strWhere = string.Format(" WHERE (UDSR IN '{0}') AND (UJTY ='2007')", st);
                    JobTable.Load(strWhere);
                }
                IRow[] rows = JobTable.RowsByIndexValue("UJTY", "2007");
                if (rows != null)
                {
                    int rowCnt = rows.Length;
                    for (int i = 0; i < rowCnt; i++)
                    {
                        IRow row = rows[i];
                        string ujty = row["UJTY"].Trim();
                        if (ujty == "2007")
                        {
                            AddWgpc(row["UDSR"].Trim(), row["WGPC"]);
                        }
                    }
                }
            }
            catch (Exception)
            {
            }
            finally
            {
                ReleaseJobTableWrite();
            }
        }

        private bool _loading = false;

        public bool IsLoadingData
        {
            get { return _loading; }
        }

        public string LoadJobData(ArrayList uaftArrStForSelection, string frCedaDateTime, string toCedaDateTime )
        {
            string result = "";
            lock (_lockForJobTableLoading)
            {
                //Select the JOBTAB records according to the flight

                string strWhere = "";
                try
                {
                    _loading = true;
                    LockJobTableForWrite();
                    DeleteJobIndex();
                    JobTable.Clear();

                    LogMsg("LoadJOBData: Start");
                    JobTable.TimeFields = "ACFR,ACTO";
                    JobTable.TimeFieldsInitiallyInUtc = true;
                    JobTable.TimeFieldsCurrentlyInUtc = true;

                    #region Shift Group Info
                    string frDt = UT.DateTimeToCeda(UT.CedaFullDateToDateTime(frCedaDateTime).AddDays(-2));
                    string toDt = UT.DateTimeToCeda(UT.CedaFullDateToDateTime(toCedaDateTime).AddDays(1));
                    strWhere = string.Format(" WHERE (UJTY ='2007') AND (ACTO>'{0}' AND ACTO<'{1}') ", frDt, toDt);
                    JobTable.Load(strWhere);

                    int jobCnt = JobTable.Count;
                    ITable table = JobTable;
                    ITable shiftTable = JobTable;
                    for (int j = 0; j < jobCnt; j++)
                    {
                        IRow jobRow = table[j];
                        if (jobRow["UJTY"] == "2007")
                        {
                            AddWgpc(jobRow["UDSR"], jobRow["WGPC"]);
                        }
                    }
                    #endregion

                    LogMsg("LoadJOBData: b4 load");
                    int cntUaftArrSt = uaftArrStForSelection.Count;
                    for (int i = 0; i < cntUaftArrSt; i++)
                    {
                        try
                        {
                            if (uaftArrStForSelection[i].ToString() != "")
                            {   //UJTY is 2000 ==> STAFF
                                //UJTY is 2020 ==> EQUIPMENT
                                strWhere = " WHERE UJTY IN ('2000','2020') AND UAFT IN (" + uaftArrStForSelection[i] + ")";
                                JobTable.Load(strWhere);
                            }
                        }
                        catch (Exception ex)
                        {
                            LogMsg("LoadJobData:Err:" + ex.Message);
                        }
                    }
                    JobTable.Sort("UAFT", true);
                    CreateJobIndex();

                    JobTable.Command("insert", ",IRT,");
                    JobTable.Command("update", ",URT,");
                    JobTable.Command("delete", ",DRT,");
                    JobTable.Command("read", ",GFR,");

                    LogMsg("JOBTAB " + JobTable.Count + " Records loaded");

                    result = JobTable.Count.ToString();
                    //LoadWgpc();
                }
                catch (Exception ex)
                {
                    LogMsg("LoadJOBData:Err:" + ex.Message);
                }
                finally
                {
                    _loading = false;
                    ReleaseJobTableWrite();
                }
            }
            return result;
        }

        private void CreateJobIndex()
        {
            JobTable.CreateIndex("URNO", "URNO");
            JobTable.CreateIndex("UAFT", "UAFT");
            JobTable.CreateIndex("UDSR", "UDSR");
            JobTable.CreateIndex("UJTY", "UJTY");
        }

        private void DeleteJobIndex()
        {
            try
            {
                JobTable.DeleteIndex("URNO");
                JobTable.DeleteIndex("UAFT");
                JobTable.DeleteIndex("UDSR");
                JobTable.DeleteIndex("UJTY");
            }
            catch (Exception)
            {
            }
        }

        private bool GetUaftFromJobChangedInfo(DatabaseTableEventArgs eventArgs,
            out string orgUaft, out string newUaft)
        {
            object newData = null;
            object orgData = null;
            orgUaft = "";
            newUaft = "";
            bool found = eventArgs.GetDataFor("UAFT", out orgData, out newData);
            if (found)
            {
                if (orgData != null)
                {
                    orgUaft = orgData.ToString().Trim();
                    if (orgUaft == "0") orgUaft = "";
                }
                if (newData != null)
                {
                    newUaft = newData.ToString().Trim();
                    if (newUaft == "0") newUaft = "";
                }
            }
            return found;
        }


        /// <summary>
        /// Check whether the record with given urno of job is needed or not.
        /// If 'removeInd' is true, and it is unwanted record, remove it.
        /// </summary>
        /// <param name="uJob">Job Urno</param>
        /// <param name="removeInd">Remove Indicator</param>
        /// <returns>Unwanted Data ==> True, Otherwise ==> False</returns>
        public bool IsUnwantedData(string uJob, bool removeInd)
        {
            bool unwantedData = false;
            try
            {
                if (removeInd)
                {
                    this.LockJobTableForWrite();
                }
                else this.LockJobTableForRead();
                IRow[] rows = JobTable.RowsByIndexValue("URNO", uJob);
                if (rows != null)
                {
                    int cnt = rows.Length;
                    for (int i = 0; i < cnt; i++)
                    {
                        if (rows[i]["UJTY"].Trim().IndexOf(",2000,2020,2007,") > 0)
                        {
                            unwantedData = true;
                            if (removeInd)
                            {
                                JobTable.Remove(rows[i]);
                            }
                        }
                    }
                }
            }
            catch (Exception)
            {
            }
            finally
            {
                if (removeInd) this.ReleaseJobTableWrite();
                else this.ReleaseJobTableRead();
            }

            return unwantedData;
        }

        public bool GetJobChangedInfo(object Sender, DatabaseTableEventArgs eventArgs,
            out State eventState, out string uJob, out string orgUaft, out string newUaft)
        {
            uJob = "";
            orgUaft = "";
            newUaft = "";
            bool found = false;

            uJob = eventArgs.row["URNO"];
            eventState = eventArgs.row.Status;
            LogMsg("GetJobChangedInfo:" + uJob + ":" + eventState.ToString());

            try
            {
                //if ((eventState == State.Created) || (eventState == State.Modified)) ComputeEmpShiftInfo(uJob);
            }
            catch (Exception ex)
            {
                LogMsg("GetJobChangedInfo:Err:" + ex.Message);
            }

            switch (eventState)
            {
                case State.Created:
                    if (IsUnwantedData(uJob, true))
                    {
                        found = true;
                    }
                    else
                    {
                        found = GetUaftFromJobChangedInfo(eventArgs, out orgUaft, out newUaft);
                        ComputeEmpShiftInfo(uJob);
                    }
                    break;
                case State.Deleted:
                    found = GetUaftFromJobChangedInfo(eventArgs, out orgUaft, out newUaft);
                    break;
                case State.Modified:
                    #region Modify
                    //Job Record was modified
                    ComputeEmpShiftInfo(uJob);
                    bool hasChanges = false;
                    int cnt = eventArgs.modifiedFields.Count;
                    string modFieldName = "";
                    for (int i = 0; i < cnt; i++)
                    {
                        string fieldName = eventArgs.modifiedFields[i].ToString();
                        if (JobTable.FieldList.Contains(fieldName))
                        {
                            if (eventArgs.modifiedFields[i].ToString() != eventArgs.previousData[i].ToString())
                            {
                                hasChanges = true;
                                //break;
                                modFieldName += fieldName + ",";
                            }
                        }
                    }
                    if (hasChanges)
                    {
                        LogMsg("GetJobChangedInfo:Upd[" + uJob + "]: changes in [" + modFieldName + "]");
                        object newData = null;
                        object orgData = null;
                        found = eventArgs.GetDataFor("UAFT", out orgData, out newData);
                        if (found)
                        {
                            if (orgData != null)
                            {
                                orgUaft = orgData.ToString().Trim();
                            }
                            if (newData != null)
                            {
                                newUaft = newData.ToString().Trim();
                            }
                        }
                    }
                    else
                    {
                        found = true;
                        LogMsg("GetJobChangedInfo:Upd[" + uJob + "]: No changes.");
                    }
                    #endregion
                    break;
                case State.Unchanged:
                    if (IsUnwantedData(uJob, true))
                    {
                        found = true;
                    }
                    else
                    {
                        found = GetUaftFromJobChangedInfo(eventArgs, out orgUaft, out newUaft);
                        ComputeEmpShiftInfo(uJob);
                    }
                    break;
            }

            return found;
        }

        public bool GetUaft(string uJob, out string uAft)
        {
            uAft = "";
            bool found = false;
            
            try
            {
                this.LockJobTableForRead();
                IRow[] rows = JobTable.RowsByIndexValue("URNO", uJob);
                if ((rows != null) && (rows.Length > 0))
                {
                    uAft = rows[0]["UAFT"].Trim();
                    found = true;
                }
            }
            catch (Exception)
            {
            }
            finally
            {
                this.ReleaseJobTableRead();
            }
            if (uAft == "0") uAft = "";
            return found;
        }

        public void ComputeEmpShiftInfo(string uJob)
        {
            try
            {
                this.LockJobTableForRead();
                IRow[] rows = JobTable.RowsByIndexValue("URNO", uJob);
                if ((rows != null) && (rows.Length > 0))
                {
                    string uDsr = rows[0]["UDSR"].Trim();
                    string ujty = rows[0]["UJTY"].Trim();
                    switch( ujty)
                    {
                        case "2007"://Change Group. Need to update in all the records.
                            AddWgpc( uDsr, rows[0]["WGPC"]);
                            break;
                        //case "2000"://Update the Group in this record.
                        //    break;
                        //case "2020":
                        //    break;
                    }
                }
            }
            catch (Exception)
            {
            }
            finally
            {
                this.ReleaseJobTableRead();
            }
        }

    }
}
