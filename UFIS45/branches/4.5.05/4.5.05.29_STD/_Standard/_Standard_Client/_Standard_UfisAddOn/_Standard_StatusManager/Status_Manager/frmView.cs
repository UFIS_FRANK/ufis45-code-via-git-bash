using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Text;
using Ufis.Utils;
using Ufis.Data;
using Microsoft.Win32;

using Ufis.Status_Manager.Ctrl;
using Ufis.Status_Manager.Util;

namespace Ufis.Status_Manager.UI
{
	/// <summary>
	/// Summary description for frmView.
	/// </summary>
	public class frmView : System.Windows.Forms.Form
	{
		#region ----- MyMembers

		private IDatabase myDB = null;
		private ITable myHSS = null;
		private ITable myALT = null;
		private ITable myNAT = null;
		private AxTABLib.AxTAB myViewsTab;
		private StringBuilder bufferHSS = new StringBuilder(10000);
		private StringBuilder bufferALT = new StringBuilder(10000);
		private StringBuilder bufferNAT = new StringBuilder(10000);

		#endregion ----- MyMembers

		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.PictureBox picBody;
		private System.Windows.Forms.Label lblTo;
		private System.Windows.Forms.Label lblFrom;
		public System.Windows.Forms.DateTimePicker dtTo;
		public System.Windows.Forms.DateTimePicker dtFrom;
		private System.Windows.Forms.PictureBox picStatusSectionFilter;
		private AxTABLib.AxTAB tabSectionSource;
		private AxTABLib.AxTAB tabSectionTarget;
		private System.Windows.Forms.ImageList imageListArrows;
		private System.Windows.Forms.Button btnSectionSource;
		private System.Windows.Forms.Button bntSectionTarget;
		private System.Windows.Forms.Button btnAirlineTarget;
		private System.Windows.Forms.Button btnAirlineSource;
		private AxTABLib.AxTAB tabAirlineTarget;
		private AxTABLib.AxTAB tabAirlineSource;
		private System.Windows.Forms.Button btnNatureTarget;
		private System.Windows.Forms.Button btnNatureSource;
		private AxTABLib.AxTAB tabNatureTarget;
		private AxTABLib.AxTAB tabNatureSource;
		private System.Windows.Forms.ComboBox cbViewName;
		private System.Windows.Forms.Label lblViewName;
		private System.Windows.Forms.Button btnOK;
		private System.Windows.Forms.Button btnCancel;
		private System.Windows.Forms.ImageList imageButtons;
		private System.Windows.Forms.Panel panelBody;
		private System.Windows.Forms.PictureBox picAirlines;
		private System.Windows.Forms.PictureBox picNatures;
		private System.Windows.Forms.Panel panelStatusSections;
		private System.Windows.Forms.Panel panelAirlines;
		private System.Windows.Forms.Panel panelNatures;
		private System.Windows.Forms.TextBox txtSection;
		private System.Windows.Forms.TextBox txtAirline;
		private System.Windows.Forms.TextBox txtNature;
		private System.Windows.Forms.CheckBox cbStatueSections;
		private System.Windows.Forms.CheckBox cbAirlines;
		private System.Windows.Forms.CheckBox cbNatures;
		private System.Windows.Forms.Button btnNew;
		private System.Windows.Forms.Button btnDelete;
		private System.Windows.Forms.Panel panelButtons;
		private System.Windows.Forms.PictureBox picButtons;
		private System.Windows.Forms.ToolTip toolTip1;
		private System.Windows.Forms.Button btnSave;
		private System.Windows.Forms.Label lblCurrentActiveView;
		private System.ComponentModel.IContainer components;

		public frmView(AxTABLib.AxTAB pTabViews)
		{
			myViewsTab = pTabViews;
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmView));
            this.label1 = new System.Windows.Forms.Label();
            this.picBody = new System.Windows.Forms.PictureBox();
            this.lblTo = new System.Windows.Forms.Label();
            this.lblFrom = new System.Windows.Forms.Label();
            this.dtTo = new System.Windows.Forms.DateTimePicker();
            this.dtFrom = new System.Windows.Forms.DateTimePicker();
            this.picStatusSectionFilter = new System.Windows.Forms.PictureBox();
            this.tabSectionSource = new AxTABLib.AxTAB();
            this.tabSectionTarget = new AxTABLib.AxTAB();
            this.imageListArrows = new System.Windows.Forms.ImageList(this.components);
            this.btnSectionSource = new System.Windows.Forms.Button();
            this.bntSectionTarget = new System.Windows.Forms.Button();
            this.btnAirlineTarget = new System.Windows.Forms.Button();
            this.btnAirlineSource = new System.Windows.Forms.Button();
            this.tabAirlineTarget = new AxTABLib.AxTAB();
            this.tabAirlineSource = new AxTABLib.AxTAB();
            this.picAirlines = new System.Windows.Forms.PictureBox();
            this.btnNatureTarget = new System.Windows.Forms.Button();
            this.btnNatureSource = new System.Windows.Forms.Button();
            this.tabNatureTarget = new AxTABLib.AxTAB();
            this.tabNatureSource = new AxTABLib.AxTAB();
            this.picNatures = new System.Windows.Forms.PictureBox();
            this.cbViewName = new System.Windows.Forms.ComboBox();
            this.lblViewName = new System.Windows.Forms.Label();
            this.btnOK = new System.Windows.Forms.Button();
            this.imageButtons = new System.Windows.Forms.ImageList(this.components);
            this.btnCancel = new System.Windows.Forms.Button();
            this.panelBody = new System.Windows.Forms.Panel();
            this.panelButtons = new System.Windows.Forms.Panel();
            this.lblCurrentActiveView = new System.Windows.Forms.Label();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnNew = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.picButtons = new System.Windows.Forms.PictureBox();
            this.panelNatures = new System.Windows.Forms.Panel();
            this.cbNatures = new System.Windows.Forms.CheckBox();
            this.txtNature = new System.Windows.Forms.TextBox();
            this.panelAirlines = new System.Windows.Forms.Panel();
            this.cbAirlines = new System.Windows.Forms.CheckBox();
            this.txtAirline = new System.Windows.Forms.TextBox();
            this.panelStatusSections = new System.Windows.Forms.Panel();
            this.cbStatueSections = new System.Windows.Forms.CheckBox();
            this.txtSection = new System.Windows.Forms.TextBox();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.picBody)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picStatusSectionFilter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabSectionSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabSectionTarget)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabAirlineTarget)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabAirlineSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picAirlines)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabNatureTarget)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabNatureSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picNatures)).BeginInit();
            this.panelBody.SuspendLayout();
            this.panelButtons.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picButtons)).BeginInit();
            this.panelNatures.SuspendLayout();
            this.panelAirlines.SuspendLayout();
            this.panelStatusSections.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Black;
            resources.ApplyResources(this.label1, "label1");
            this.label1.ForeColor = System.Drawing.Color.Lime;
            this.label1.Name = "label1";
            // 
            // picBody
            // 
            resources.ApplyResources(this.picBody, "picBody");
            this.picBody.Name = "picBody";
            this.picBody.TabStop = false;
            this.picBody.Paint += new System.Windows.Forms.PaintEventHandler(this.picBody_Paint);
            // 
            // lblTo
            // 
            this.lblTo.BackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.lblTo, "lblTo");
            this.lblTo.Name = "lblTo";
            // 
            // lblFrom
            // 
            this.lblFrom.BackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.lblFrom, "lblFrom");
            this.lblFrom.Name = "lblFrom";
            // 
            // dtTo
            // 
            this.dtTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            resources.ApplyResources(this.dtTo, "dtTo");
            this.dtTo.Name = "dtTo";
            // 
            // dtFrom
            // 
            this.dtFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            resources.ApplyResources(this.dtFrom, "dtFrom");
            this.dtFrom.Name = "dtFrom";
            // 
            // picStatusSectionFilter
            // 
            this.picStatusSectionFilter.BackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.picStatusSectionFilter, "picStatusSectionFilter");
            this.picStatusSectionFilter.Name = "picStatusSectionFilter";
            this.picStatusSectionFilter.TabStop = false;
            this.picStatusSectionFilter.Paint += new System.Windows.Forms.PaintEventHandler(this.picStatusSectionFilter_Paint);
            // 
            // tabSectionSource
            // 
            resources.ApplyResources(this.tabSectionSource, "tabSectionSource");
            this.tabSectionSource.Name = "tabSectionSource";
            this.tabSectionSource.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("tabSectionSource.OcxState")));
            this.toolTip1.SetToolTip(this.tabSectionSource, resources.GetString("tabSectionSource.ToolTip"));
            this.tabSectionSource.SendLButtonDblClick += new AxTABLib._DTABEvents_SendLButtonDblClickEventHandler(this.tabSectionSource_SendLButtonDblClick);
            // 
            // tabSectionTarget
            // 
            resources.ApplyResources(this.tabSectionTarget, "tabSectionTarget");
            this.tabSectionTarget.Name = "tabSectionTarget";
            this.tabSectionTarget.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("tabSectionTarget.OcxState")));
            this.toolTip1.SetToolTip(this.tabSectionTarget, resources.GetString("tabSectionTarget.ToolTip"));
            this.tabSectionTarget.SendLButtonDblClick += new AxTABLib._DTABEvents_SendLButtonDblClickEventHandler(this.tabSectionTarget_SendLButtonDblClick);
            // 
            // imageListArrows
            // 
            this.imageListArrows.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageListArrows.ImageStream")));
            this.imageListArrows.TransparentColor = System.Drawing.Color.White;
            this.imageListArrows.Images.SetKeyName(0, "");
            this.imageListArrows.Images.SetKeyName(1, "");
            this.imageListArrows.Images.SetKeyName(2, "");
            this.imageListArrows.Images.SetKeyName(3, "");
            this.imageListArrows.Images.SetKeyName(4, "");
            this.imageListArrows.Images.SetKeyName(5, "");
            this.imageListArrows.Images.SetKeyName(6, "");
            this.imageListArrows.Images.SetKeyName(7, "");
            // 
            // btnSectionSource
            // 
            resources.ApplyResources(this.btnSectionSource, "btnSectionSource");
            this.btnSectionSource.Name = "btnSectionSource";
            this.toolTip1.SetToolTip(this.btnSectionSource, resources.GetString("btnSectionSource.ToolTip"));
            this.btnSectionSource.Click += new System.EventHandler(this.btnSectionSource_Click);
            // 
            // bntSectionTarget
            // 
            resources.ApplyResources(this.bntSectionTarget, "bntSectionTarget");
            this.bntSectionTarget.Name = "bntSectionTarget";
            this.toolTip1.SetToolTip(this.bntSectionTarget, resources.GetString("bntSectionTarget.ToolTip"));
            this.bntSectionTarget.Click += new System.EventHandler(this.bntSectionTarget_Click);
            // 
            // btnAirlineTarget
            // 
            resources.ApplyResources(this.btnAirlineTarget, "btnAirlineTarget");
            this.btnAirlineTarget.Name = "btnAirlineTarget";
            this.toolTip1.SetToolTip(this.btnAirlineTarget, resources.GetString("btnAirlineTarget.ToolTip"));
            this.btnAirlineTarget.Click += new System.EventHandler(this.btnAirlineTarget_Click);
            // 
            // btnAirlineSource
            // 
            resources.ApplyResources(this.btnAirlineSource, "btnAirlineSource");
            this.btnAirlineSource.Name = "btnAirlineSource";
            this.toolTip1.SetToolTip(this.btnAirlineSource, resources.GetString("btnAirlineSource.ToolTip"));
            this.btnAirlineSource.Click += new System.EventHandler(this.btnAirlineSource_Click);
            // 
            // tabAirlineTarget
            // 
            resources.ApplyResources(this.tabAirlineTarget, "tabAirlineTarget");
            this.tabAirlineTarget.Name = "tabAirlineTarget";
            this.tabAirlineTarget.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("tabAirlineTarget.OcxState")));
            this.toolTip1.SetToolTip(this.tabAirlineTarget, resources.GetString("tabAirlineTarget.ToolTip"));
            this.tabAirlineTarget.SendLButtonDblClick += new AxTABLib._DTABEvents_SendLButtonDblClickEventHandler(this.tabAirlineTarget_SendLButtonDblClick);
            // 
            // tabAirlineSource
            // 
            resources.ApplyResources(this.tabAirlineSource, "tabAirlineSource");
            this.tabAirlineSource.Name = "tabAirlineSource";
            this.tabAirlineSource.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("tabAirlineSource.OcxState")));
            this.toolTip1.SetToolTip(this.tabAirlineSource, resources.GetString("tabAirlineSource.ToolTip"));
            this.tabAirlineSource.SendLButtonDblClick += new AxTABLib._DTABEvents_SendLButtonDblClickEventHandler(this.tabAirlineSource_SendLButtonDblClick);
            // 
            // picAirlines
            // 
            this.picAirlines.BackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.picAirlines, "picAirlines");
            this.picAirlines.Name = "picAirlines";
            this.picAirlines.TabStop = false;
            this.picAirlines.Paint += new System.Windows.Forms.PaintEventHandler(this.picAirlines_Paint);
            // 
            // btnNatureTarget
            // 
            resources.ApplyResources(this.btnNatureTarget, "btnNatureTarget");
            this.btnNatureTarget.Name = "btnNatureTarget";
            this.toolTip1.SetToolTip(this.btnNatureTarget, resources.GetString("btnNatureTarget.ToolTip"));
            this.btnNatureTarget.Click += new System.EventHandler(this.btnNatureTarget_Click);
            // 
            // btnNatureSource
            // 
            resources.ApplyResources(this.btnNatureSource, "btnNatureSource");
            this.btnNatureSource.Name = "btnNatureSource";
            this.toolTip1.SetToolTip(this.btnNatureSource, resources.GetString("btnNatureSource.ToolTip"));
            this.btnNatureSource.Click += new System.EventHandler(this.btnNatureSource_Click);
            // 
            // tabNatureTarget
            // 
            resources.ApplyResources(this.tabNatureTarget, "tabNatureTarget");
            this.tabNatureTarget.Name = "tabNatureTarget";
            this.tabNatureTarget.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("tabNatureTarget.OcxState")));
            this.toolTip1.SetToolTip(this.tabNatureTarget, resources.GetString("tabNatureTarget.ToolTip"));
            this.tabNatureTarget.SendLButtonDblClick += new AxTABLib._DTABEvents_SendLButtonDblClickEventHandler(this.tabNatureTarget_SendLButtonDblClick);
            // 
            // tabNatureSource
            // 
            resources.ApplyResources(this.tabNatureSource, "tabNatureSource");
            this.tabNatureSource.Name = "tabNatureSource";
            this.tabNatureSource.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("tabNatureSource.OcxState")));
            this.toolTip1.SetToolTip(this.tabNatureSource, resources.GetString("tabNatureSource.ToolTip"));
            this.tabNatureSource.SendLButtonDblClick += new AxTABLib._DTABEvents_SendLButtonDblClickEventHandler(this.tabNatureSource_SendLButtonDblClick);
            // 
            // picNatures
            // 
            this.picNatures.BackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.picNatures, "picNatures");
            this.picNatures.Name = "picNatures";
            this.picNatures.TabStop = false;
            this.picNatures.Paint += new System.Windows.Forms.PaintEventHandler(this.picNatures_Paint);
            // 
            // cbViewName
            // 
            resources.ApplyResources(this.cbViewName, "cbViewName");
            this.cbViewName.Name = "cbViewName";
            this.cbViewName.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cbViewName_KeyPress);
            this.cbViewName.TextChanged += new System.EventHandler(this.cbViewName_TextChanged);
            this.cbViewName.SelectedValueChanged += new System.EventHandler(this.cbViewName_SelectedValueChanged);
            // 
            // lblViewName
            // 
            this.lblViewName.BackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.lblViewName, "lblViewName");
            this.lblViewName.Name = "lblViewName";
            // 
            // btnOK
            // 
            this.btnOK.BackColor = System.Drawing.Color.Transparent;
            this.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            resources.ApplyResources(this.btnOK, "btnOK");
            this.btnOK.ImageList = this.imageButtons;
            this.btnOK.Name = "btnOK";
            this.toolTip1.SetToolTip(this.btnOK, resources.GetString("btnOK.ToolTip"));
            this.btnOK.UseVisualStyleBackColor = false;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // imageButtons
            // 
            this.imageButtons.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageButtons.ImageStream")));
            this.imageButtons.TransparentColor = System.Drawing.Color.White;
            this.imageButtons.Images.SetKeyName(0, "");
            this.imageButtons.Images.SetKeyName(1, "");
            this.imageButtons.Images.SetKeyName(2, "");
            this.imageButtons.Images.SetKeyName(3, "");
            this.imageButtons.Images.SetKeyName(4, "");
            this.imageButtons.Images.SetKeyName(5, "");
            // 
            // btnCancel
            // 
            this.btnCancel.BackColor = System.Drawing.Color.Transparent;
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            resources.ApplyResources(this.btnCancel, "btnCancel");
            this.btnCancel.ImageList = this.imageButtons;
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.UseVisualStyleBackColor = false;
            // 
            // panelBody
            // 
            this.panelBody.Controls.Add(this.panelButtons);
            this.panelBody.Controls.Add(this.panelNatures);
            this.panelBody.Controls.Add(this.panelAirlines);
            this.panelBody.Controls.Add(this.panelStatusSections);
            this.panelBody.Controls.Add(this.dtFrom);
            this.panelBody.Controls.Add(this.dtTo);
            this.panelBody.Controls.Add(this.lblTo);
            this.panelBody.Controls.Add(this.lblFrom);
            this.panelBody.Controls.Add(this.lblViewName);
            this.panelBody.Controls.Add(this.cbViewName);
            this.panelBody.Controls.Add(this.picBody);
            resources.ApplyResources(this.panelBody, "panelBody");
            this.panelBody.Name = "panelBody";
            // 
            // panelButtons
            // 
            this.panelButtons.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panelButtons.Controls.Add(this.lblCurrentActiveView);
            this.panelButtons.Controls.Add(this.btnSave);
            this.panelButtons.Controls.Add(this.btnCancel);
            this.panelButtons.Controls.Add(this.btnNew);
            this.panelButtons.Controls.Add(this.btnDelete);
            this.panelButtons.Controls.Add(this.btnOK);
            this.panelButtons.Controls.Add(this.picButtons);
            resources.ApplyResources(this.panelButtons, "panelButtons");
            this.panelButtons.Name = "panelButtons";
            // 
            // lblCurrentActiveView
            // 
            this.lblCurrentActiveView.BackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.lblCurrentActiveView, "lblCurrentActiveView");
            this.lblCurrentActiveView.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblCurrentActiveView.Name = "lblCurrentActiveView";
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.btnSave, "btnSave");
            this.btnSave.ImageList = this.imageButtons;
            this.btnSave.Name = "btnSave";
            this.toolTip1.SetToolTip(this.btnSave, resources.GetString("btnSave.ToolTip"));
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnNew
            // 
            this.btnNew.BackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.btnNew, "btnNew");
            this.btnNew.ImageList = this.imageButtons;
            this.btnNew.Name = "btnNew";
            this.toolTip1.SetToolTip(this.btnNew, resources.GetString("btnNew.ToolTip"));
            this.btnNew.UseVisualStyleBackColor = false;
            this.btnNew.Click += new System.EventHandler(this.btnNew_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.BackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.btnDelete, "btnDelete");
            this.btnDelete.ImageList = this.imageButtons;
            this.btnDelete.Name = "btnDelete";
            this.toolTip1.SetToolTip(this.btnDelete, resources.GetString("btnDelete.ToolTip"));
            this.btnDelete.UseVisualStyleBackColor = false;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // picButtons
            // 
            resources.ApplyResources(this.picButtons, "picButtons");
            this.picButtons.Name = "picButtons";
            this.picButtons.TabStop = false;
            this.picButtons.Paint += new System.Windows.Forms.PaintEventHandler(this.picButtons_Paint);
            // 
            // panelNatures
            // 
            this.panelNatures.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panelNatures.Controls.Add(this.btnNatureSource);
            this.panelNatures.Controls.Add(this.cbNatures);
            this.panelNatures.Controls.Add(this.txtNature);
            this.panelNatures.Controls.Add(this.tabNatureSource);
            this.panelNatures.Controls.Add(this.btnNatureTarget);
            this.panelNatures.Controls.Add(this.tabNatureTarget);
            this.panelNatures.Controls.Add(this.picNatures);
            resources.ApplyResources(this.panelNatures, "panelNatures");
            this.panelNatures.Name = "panelNatures";
            // 
            // cbNatures
            // 
            resources.ApplyResources(this.cbNatures, "cbNatures");
            this.cbNatures.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbNatures.Name = "cbNatures";
            // 
            // txtNature
            // 
            resources.ApplyResources(this.txtNature, "txtNature");
            this.txtNature.Name = "txtNature";
            this.toolTip1.SetToolTip(this.txtNature, resources.GetString("txtNature.ToolTip"));
            this.txtNature.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNature_KeyPress);
            this.txtNature.TextChanged += new System.EventHandler(this.txtNature_TextChanged);
            // 
            // panelAirlines
            // 
            this.panelAirlines.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panelAirlines.Controls.Add(this.cbAirlines);
            this.panelAirlines.Controls.Add(this.txtAirline);
            this.panelAirlines.Controls.Add(this.btnAirlineTarget);
            this.panelAirlines.Controls.Add(this.btnAirlineSource);
            this.panelAirlines.Controls.Add(this.tabAirlineTarget);
            this.panelAirlines.Controls.Add(this.tabAirlineSource);
            this.panelAirlines.Controls.Add(this.picAirlines);
            resources.ApplyResources(this.panelAirlines, "panelAirlines");
            this.panelAirlines.Name = "panelAirlines";
            // 
            // cbAirlines
            // 
            resources.ApplyResources(this.cbAirlines, "cbAirlines");
            this.cbAirlines.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbAirlines.Name = "cbAirlines";
            // 
            // txtAirline
            // 
            this.txtAirline.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            resources.ApplyResources(this.txtAirline, "txtAirline");
            this.txtAirline.Name = "txtAirline";
            this.toolTip1.SetToolTip(this.txtAirline, resources.GetString("txtAirline.ToolTip"));
            this.txtAirline.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtAirline_KeyPress);
            this.txtAirline.TextChanged += new System.EventHandler(this.txtAirline_TextChanged);
            // 
            // panelStatusSections
            // 
            this.panelStatusSections.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panelStatusSections.Controls.Add(this.cbStatueSections);
            this.panelStatusSections.Controls.Add(this.txtSection);
            this.panelStatusSections.Controls.Add(this.tabSectionTarget);
            this.panelStatusSections.Controls.Add(this.btnSectionSource);
            this.panelStatusSections.Controls.Add(this.tabSectionSource);
            this.panelStatusSections.Controls.Add(this.bntSectionTarget);
            this.panelStatusSections.Controls.Add(this.picStatusSectionFilter);
            resources.ApplyResources(this.panelStatusSections, "panelStatusSections");
            this.panelStatusSections.Name = "panelStatusSections";
            // 
            // cbStatueSections
            // 
            resources.ApplyResources(this.cbStatueSections, "cbStatueSections");
            this.cbStatueSections.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbStatueSections.Name = "cbStatueSections";
            // 
            // txtSection
            // 
            resources.ApplyResources(this.txtSection, "txtSection");
            this.txtSection.Name = "txtSection";
            this.toolTip1.SetToolTip(this.txtSection, resources.GetString("txtSection.ToolTip"));
            this.txtSection.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtSection_KeyPress);
            this.txtSection.TextChanged += new System.EventHandler(this.txtSection_TextChanged);
            // 
            // frmView
            // 
            resources.ApplyResources(this, "$this");
            this.Controls.Add(this.panelBody);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MinimizeBox = false;
            this.Name = "frmView";
            this.ShowInTaskbar = false;
            this.Load += new System.EventHandler(this.frmView_Load);
            ((System.ComponentModel.ISupportInitialize)(this.picBody)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picStatusSectionFilter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabSectionSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabSectionTarget)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabAirlineTarget)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabAirlineSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picAirlines)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabNatureTarget)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabNatureSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picNatures)).EndInit();
            this.panelBody.ResumeLayout(false);
            this.panelButtons.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picButtons)).EndInit();
            this.panelNatures.ResumeLayout(false);
            this.panelNatures.PerformLayout();
            this.panelAirlines.ResumeLayout(false);
            this.panelAirlines.PerformLayout();
            this.panelStatusSections.ResumeLayout(false);
            this.panelStatusSections.PerformLayout();
            this.ResumeLayout(false);

		}
		#endregion

		private void picBody_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
		{
			UT.DrawGradient(e.Graphics, 90f, picBody, Color.WhiteSmoke, Color.LightGray);			
		}

		private void frmView_Load(object sender, System.EventArgs e)
		{
			DateTime olFrom;
			DateTime olTo;

			myDB = UT.GetMemDB();
			myHSS = myDB["HSS"];
			myALT = myDB["ALT"];
			myNAT = myDB["NAT"];

			lblCurrentActiveView.Text = "Active View in Main Window: " + CFC.CurrentViewName;
			olFrom = DateTime.Now;
			olFrom = UT.TimeFrameFrom; //new DateTime(olFrom.Year, olFrom.Month, olFrom.Day, 0,0,0);
			olTo   = UT.TimeFrameTo; //new DateTime(olFrom.Year, olFrom.Month, olFrom.Day, 23,59,0);
			dtFrom.Value = olFrom;
			dtTo.Value = olTo;
			dtFrom.CustomFormat = "dd.MM.yyyy - HH:mm";
			dtTo.CustomFormat = "dd.MM.yyyy - HH:mm";

			lblFrom.Parent = picBody;
			lblTo.Parent = picBody;
			lblViewName.Parent = picBody;

			btnNew.Parent = picButtons;
			btnDelete.Parent = picButtons;
			btnOK.Parent = picButtons;
			btnCancel.Parent = picButtons;
			btnSave.Parent = picButtons;
			lblCurrentActiveView.Parent = picButtons;

			btnSectionSource.Parent = picStatusSectionFilter;
			bntSectionTarget.Parent = picStatusSectionFilter;
			cbStatueSections.Parent = picStatusSectionFilter;
			btnAirlineSource.Parent = picAirlines;
			btnAirlineTarget.Parent = picAirlines;
			cbAirlines.Parent = picAirlines;
			btnNatureSource.Parent = picNatures;
			btnNatureTarget.Parent = picNatures;
			cbNatures.Parent = picNatures;

			NewView();
			int i = 0;
			cbViewName.Items.Add(CtrlView.CFC_DEFAULT_VIEW);
			for( i = 0; i < myViewsTab.GetLineCount(); i++)
			{
				string strName = myViewsTab.GetFieldValue(i, "NAME");
				cbViewName.Items.Add(strName);
				if(strName == CFC.CurrentViewName)
				{
					cbViewName_SelectedValueChanged(this, null);
					cbViewName.Text = CFC.CurrentViewName;
				}
			}
			if(CFC.CurrentViewName == CtrlView.CFC_DEFAULT_VIEW)
			{
				cbViewName.Text = CFC.CurrentViewName;
				EnableControlsForDefault(false);
				btnDelete.Enabled = false;
				btnSave.Enabled = false;
			}
			cbViewName.Refresh();

			UpdateButtons();

		}

		private void InitTabs()
		{
			tabSectionSource.ResetContent();
			tabSectionSource.HeaderString = "Name,URNO";
			tabSectionSource.HeaderLengthString = "180,80";
			tabSectionSource.LogicalFieldList = "NAME,URNO";
			tabSectionSource.LifeStyle = true;
			tabSectionSource.CursorLifeStyle = true;
			tabSectionSource.SetTabFontBold(true);
			tabSectionSource.FontName = "Arial";
			tabSectionSource.DisplayBackColor = UT.colYellow;
			tabSectionSource.ShowHorzScroller(true);

			tabSectionTarget.ResetContent();
			tabSectionTarget.HeaderString = "Name,URNO";
			tabSectionTarget.HeaderLengthString = "180,80";
			tabSectionTarget.LogicalFieldList = "NAME,URNO";
			tabSectionTarget.LifeStyle = true;
			tabSectionTarget.CursorLifeStyle = true;
			tabSectionTarget.SetTabFontBold(true);
			tabSectionTarget.FontName = "Arial";
			tabSectionTarget.DisplayBackColor = UT.colLightGreen;
			tabSectionTarget.DisplayTextColor = UT.colBlack;
			tabSectionTarget.ShowHorzScroller(true);

			tabAirlineSource.ResetContent();
			tabAirlineSource.HeaderString = "ALC2,ALC3,URNO";
			tabAirlineSource.HeaderLengthString = "60,60,80";
			tabAirlineSource.LogicalFieldList = "ALC2,ALC3,URNO";
			tabAirlineSource.LifeStyle = true;
			tabAirlineSource.CursorLifeStyle = true;
			//tabAirlineSource.ShowHorzScroller(true);
			tabAirlineSource.SetTabFontBold(true);
			tabAirlineSource.FontName = "Arial";
			tabAirlineSource.DisplayBackColor = UT.colYellow;
			tabAirlineSource.ShowHorzScroller(true);

			tabAirlineTarget.ResetContent();
			tabAirlineTarget.HeaderString = "ALC2,ALC3,URNO";
			tabAirlineTarget.HeaderLengthString = "60,60,80";
			tabAirlineTarget.LogicalFieldList = "ALC2,ALC3,URNO";
			tabAirlineTarget.LifeStyle = true;
			tabAirlineTarget.CursorLifeStyle = true;
			tabAirlineTarget.SetTabFontBold(true);
			tabAirlineTarget.FontName = "Arial";
			//tabAirlineTarget.ShowHorzScroller(true);
			tabAirlineTarget.DisplayBackColor = UT.colLightGreen;
			tabAirlineTarget.DisplayTextColor = UT.colBlack;
			tabAirlineTarget.ShowHorzScroller(true);

			tabNatureSource.ResetContent();
			tabNatureSource.HeaderString = "Name,URNO";
			tabNatureSource.HeaderLengthString = "180,80";
			tabNatureSource.LogicalFieldList = "TNAM,URNO";
			tabNatureSource.LifeStyle = true;
			tabNatureSource.CursorLifeStyle = true;
			tabNatureSource.SetTabFontBold(true);
			//tabNatureSource.ShowHorzScroller(true);
			tabNatureSource.FontName = "Arial";
			tabNatureSource.DisplayBackColor = UT.colYellow;
			tabNatureSource.ShowHorzScroller(true);

			tabNatureTarget.ResetContent();
			tabNatureTarget.HeaderString = "Name,URNO";
			tabNatureTarget.HeaderLengthString = "180,80";
			tabNatureTarget.LogicalFieldList = "TNAM,URNO";
			tabNatureTarget.FontName = "Arial";
			tabNatureTarget.LifeStyle = true;
			tabNatureTarget.CursorLifeStyle = true;
			tabNatureTarget.SetTabFontBold(true);
			//tabNatureTarget.ShowHorzScroller(true);
			tabNatureTarget.DisplayBackColor = UT.colLightGreen;
			tabNatureTarget.DisplayTextColor = UT.colBlack;
			tabNatureTarget.ShowHorzScroller(true);
		}

		private void picStatusSectionFilter_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
		{
			UT.DrawGradient(e.Graphics, 0f, picStatusSectionFilter, Color.WhiteSmoke, Color.LightGray);			
		}

		private void picAirlines_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
		{
			UT.DrawGradient(e.Graphics, 0f, picAirlines, Color.WhiteSmoke, Color.LightGray);			
		}

		private void picNatures_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
		{
			UT.DrawGradient(e.Graphics, 0f, picNatures, Color.WhiteSmoke, Color.LightGray);			
		}

		private void btnAirlineTarget_Click(object sender, System.EventArgs e)
		{
			tabAirlineSource.InsertBuffer(tabAirlineTarget.GetBuffer(0, tabAirlineTarget.GetLineCount()-1, "\n"), "\n");
			tabAirlineTarget.ResetContent();
			tabAirlineTarget.Sort("0,1", true, true);
			tabAirlineSource.Sort("0,1", true, true);
			UpdateButtons();
		}

		private void btnAirlineSource_Click(object sender, System.EventArgs e)
		{
			tabAirlineTarget.InsertBuffer(tabAirlineSource.GetBuffer(0, tabAirlineSource.GetLineCount()-1, "\n"), "\n");
			tabAirlineSource.ResetContent();
			tabAirlineTarget.Sort("0,1", true, true);
			tabAirlineSource.Sort("0,1", true, true);
			UpdateButtons();
		}

		private void btnSectionSource_Click(object sender, System.EventArgs e)
		{
			tabSectionTarget.InsertBuffer(tabSectionSource.GetBuffer(0, tabSectionSource.GetLineCount()-1, "\n"), "\n");
			tabSectionSource.ResetContent();
			tabSectionSource.Sort("0", true, true);
			tabSectionTarget.Sort("0", true, true);
			UpdateButtons();
		}

		private void bntSectionTarget_Click(object sender, System.EventArgs e)
		{
			tabSectionSource.InsertBuffer(tabSectionTarget.GetBuffer(0, tabSectionTarget.GetLineCount()-1, "\n"), "\n");
			tabSectionTarget.ResetContent();
			tabSectionSource.Sort("0", true, true);
			tabSectionTarget.Sort("0", true, true);
			UpdateButtons();
		}
		private void btnNatureSource_Click(object sender, System.EventArgs e)
		{
			tabNatureTarget.InsertBuffer(tabNatureSource.GetBuffer(0, tabNatureSource.GetLineCount()-1, "\n"), "\n");
			tabNatureSource.ResetContent();
			tabNatureSource.Sort("0", true, true);
			tabNatureTarget.Sort("0", true, true);
			UpdateButtons();
		}

		private void btnNatureTarget_Click(object sender, System.EventArgs e)
		{
			tabNatureSource.InsertBuffer(tabNatureTarget.GetBuffer(0, tabNatureTarget.GetLineCount()-1, "\n"), "\n");
			tabNatureTarget.ResetContent();
			tabNatureSource.Sort("0", true, true);
			tabNatureTarget.Sort("0", true, true);
			UpdateButtons();
		}

		private void picButtons_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
		{
			UT.DrawGradient(e.Graphics, 90f, picButtons, Color.WhiteSmoke, Color.Gray);			
		}

		private void NewView()
		{
			int i = 0;
			string strValues = "";
			InitTabs();
		
			if(bufferHSS.Length == 0)
			{
				for( i = 0; i < myHSS.Count; i++)	
				{
					strValues = myHSS[i]["NAME"] + "," + myHSS[i]["URNO"];
					bufferHSS.Append(strValues + "\n");
					//tabSectionSource.InsertTextLine(strValues, false);
				}
			}
			tabSectionSource.InsertBuffer(bufferHSS.ToString(), "\n");
			btnSectionSource.Text = myHSS.Count.ToString();
			if(bufferALT.Length == 0)
			{
				for( i = 0; i < myALT.Count; i++)	
				{
					strValues = myALT[i]["ALC2"] + "," + myALT[i]["ALC3"] + "," + myALT[i]["URNO"];
					bufferALT.Append(strValues + "\n");
					//tabAirlineSource.InsertTextLine(strValues, false);
				}
			}
			tabAirlineSource.InsertBuffer(bufferALT.ToString(), "\n");
			btnAirlineSource.Text = myALT.Count.ToString();
			if(bufferNAT.Length == 0)
			{
				for( i = 0; i < myNAT.Count; i++)	
				{
					strValues = myNAT[i]["TNAM"] + "," + myNAT[i]["URNO"];
					bufferNAT.Append(strValues + "\n");
					//tabNatureSource.InsertTextLine(strValues, false);
				}
			}
			tabNatureSource.InsertBuffer(bufferNAT.ToString(), "\n");
			btnNatureSource.Text = myNAT.Count.ToString();
			tabSectionSource.Sort("0", true, true);
			tabSectionTarget.Sort("0", true, true);
			tabAirlineSource.Sort("0,1", true, true);
			tabAirlineTarget.Sort("0,1", true, true);
			tabNatureSource.Sort("0", true, true);
			tabNatureTarget.Sort("0", true, true);
			cbViewName.Focus();
		}

		private void tabSectionSource_SendLButtonDblClick(object sender, AxTABLib._DTABEvents_SendLButtonDblClickEvent e)
		{
			if(e.lineNo > -1)
			{
				tabSectionTarget.InsertTextLine( tabSectionSource.GetFieldValues(e.lineNo, "NAME,URNO"), true);
				tabSectionSource.DeleteLine(e.lineNo);
				tabSectionSource.Sort("0", true, true);
				tabSectionTarget.Sort("0", true, true);
			}
			UpdateButtons();
		}

		private void tabSectionTarget_SendLButtonDblClick(object sender, AxTABLib._DTABEvents_SendLButtonDblClickEvent e)
		{
			if(e.lineNo > -1)
			{
				tabSectionSource.InsertTextLine( tabSectionTarget.GetFieldValues(e.lineNo, "NAME,URNO"), true);
				tabSectionTarget.DeleteLine(e.lineNo);
				tabSectionSource.Sort("0", true, true);
				tabSectionTarget.Sort("0", true, true);
			}
			UpdateButtons();
		}

		private void tabAirlineSource_SendLButtonDblClick(object sender, AxTABLib._DTABEvents_SendLButtonDblClickEvent e)
		{
			if(e.lineNo > -1)
			{
				tabAirlineTarget.InsertTextLine( tabAirlineSource.GetFieldValues(e.lineNo, "ALC2,ALC3,URNO"), true);
				tabAirlineSource.DeleteLine(e.lineNo);
				tabAirlineTarget.Sort("0,1", true, true);
				tabAirlineSource.Sort("0,1", true, true);
			}
			UpdateButtons();
		}

		private void tabAirlineTarget_SendLButtonDblClick(object sender, AxTABLib._DTABEvents_SendLButtonDblClickEvent e)
		{
			if(e.lineNo > -1)
			{
				tabAirlineSource.InsertTextLine( tabAirlineTarget.GetFieldValues(e.lineNo, "ALC2,ALC3,URNO"), true);
				tabAirlineTarget.DeleteLine(e.lineNo);
				tabAirlineTarget.Sort("0,1", true, true);
				tabAirlineSource.Sort("0,1", true, true);
			}
			UpdateButtons();
		}

		private void tabNatureSource_SendLButtonDblClick(object sender, AxTABLib._DTABEvents_SendLButtonDblClickEvent e)
		{
			if(e.lineNo > -1)
			{
				tabNatureTarget.InsertTextLine( tabNatureSource.GetFieldValues(e.lineNo, "TNAM,URNO"), true);
				tabNatureSource.DeleteLine(e.lineNo);
				tabNatureSource.Sort("0", true, true);
				tabNatureTarget.Sort("0", true, true);
			}
			UpdateButtons();
		}

		private void tabNatureTarget_SendLButtonDblClick(object sender, AxTABLib._DTABEvents_SendLButtonDblClickEvent e)
		{
			if(e.lineNo > -1)
			{
				tabNatureSource.InsertTextLine( tabNatureTarget.GetFieldValues(e.lineNo, "TNAM,URNO"), true);
				tabNatureTarget.DeleteLine(e.lineNo);
				tabNatureSource.Sort("0", true, true);
				tabNatureTarget.Sort("0", true, true);
			}
			UpdateButtons();
		}
		private void UpdateButtons()
		{
			btnSectionSource.Text = tabSectionSource.GetLineCount().ToString();
			bntSectionTarget.Text = tabSectionTarget.GetLineCount().ToString();
			btnAirlineSource.Text = tabAirlineSource.GetLineCount().ToString();
			btnAirlineTarget.Text = tabAirlineTarget.GetLineCount().ToString();
			btnNatureSource.Text = tabNatureSource.GetLineCount().ToString();
			btnNatureTarget.Text = tabNatureTarget.GetLineCount().ToString();
			tabSectionSource.Refresh();
			tabSectionTarget.Refresh();
			tabAirlineSource.Refresh();
			tabAirlineTarget.Refresh();
			tabNatureSource.Refresh();
			tabNatureTarget.Refresh();
		}

		private void btnNew_Click(object sender, System.EventArgs e)
		{
			NewView();
			UpdateButtons();
			cbViewName.Text = "";
		}

		private void btnOK_Click(object sender, System.EventArgs e)
		{
			string strMessage = "";
			strMessage = SaveView(true);
			if(strMessage != "")
			{
				MessageBox.Show(this, strMessage, "Save Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
				this.DialogResult = DialogResult.None;
			}
			else
			{
				this.Cursor = Cursors.WaitCursor;

				CFC.CurrentViewName = cbViewName.Text;
				this.DialogResult = DialogResult.OK;
				this.Cursor = Cursors.Arrow;
			}
		}
		private string CheckValidity()
		{
			string strMessage = "";
			if(cbViewName.Text == "")
			{
				strMessage = "Please insert a View Name!\n";
			}
//MWO: Not necessary to check the timeframe AMA+MWO 05.01.2004
//			if(dtFrom.Value < UT.TimeFrameFrom || dtFrom.Value > UT.TimeFrameTo ||
//				dtTo.Value < UT.TimeFrameFrom || dtTo.Value > UT.TimeFrameTo)
//			{
//				strMessage += "Please define a timeframe, which is inside the loaded time frame!\n";
//			}
			return strMessage;
		}

		private void cbViewName_SelectedValueChanged(object sender, System.EventArgs e)
		{
			string strName = cbViewName.Text;
			string strValues = "";
			int i = 0;
			int j = 0;
			bool blFound = false;
			int idx = -1;
			NewView();
			cbViewName.Text = strName;
			if(strName == CtrlView.CFC_DEFAULT_VIEW)
			{
				EnableControlsForDefault(false);
				return;
			}
			EnableControlsForDefault(true);
			for( i = 0; i < myViewsTab.GetLineCount() && blFound == false; i++)
			{
				if(strName == myViewsTab.GetFieldValue(i, "NAME"))
				{
					idx = i;
					blFound = true;
				}
			}
			if(idx > -1) // then the view was found
			{
				dtFrom.Value = UT.CedaFullDateToDateTime(myViewsTab.GetFieldValue(idx, "FROM"));
				dtTo.Value   = UT.CedaFullDateToDateTime(myViewsTab.GetFieldValue(idx, "TO"));
				string [] arrHSS = myViewsTab.GetFieldValue(idx, "HSS").Split('|');
				string [] arrALT = myViewsTab.GetFieldValue(idx, "ALT").Split('|');
				string [] arrNAT = myViewsTab.GetFieldValue(idx, "NAT").Split('|');
				StringBuilder sbALT = new StringBuilder(10000);

				if(arrHSS.Length > 0)
				{
					j =  arrHSS.Length - 1;
					for( i = tabSectionSource.GetLineCount()-1; i >= 0 && j >= 0; i--)
					{
						if(tabSectionSource.GetFieldValues(i, "URNO") == arrHSS[j])
						{
							tabSectionTarget.InsertTextLine( tabSectionSource.GetFieldValues(i, "NAME,URNO"), false);
							tabSectionSource.DeleteLine(i);
							j--;
						}
					}
				}
				if(arrALT.Length > 0)
				{
					j = arrALT.Length-1;
					tabAirlineSource.ShowVertScroller(false);
					for( i = tabAirlineSource.GetLineCount()-1; i >= 0 && j >= 0; i--)
					{
						if(tabAirlineSource.GetFieldValues(i, "URNO") == arrALT[j])
						{
							strValues  = tabAirlineSource.GetColumnValue(i, 0) + ",";
							strValues += tabAirlineSource.GetColumnValue(i, 1) + ",";
							strValues += tabAirlineSource.GetColumnValue(i, 2) + ",";
							//tabAirlineTarget.InsertTextLine( tabAirlineSource.GetFieldValues(i, "ALC2,ALC3,URNO"), false);
							sbALT.Append(strValues + "\n");
							tabAirlineSource.DeleteLine(i);
							j--;
						}
					}
					tabAirlineTarget.InsertBuffer(sbALT.ToString(), "\n");
					tabAirlineSource.ShowVertScroller(true);
				}
				if(arrNAT.Length > 0)
				{
					j = arrNAT.Length-1;
					for( i = tabNatureSource.GetLineCount()-1; i >= 0 && j >= 0; i--)
					{
						if(tabNatureSource.GetFieldValues(i, "URNO") == arrNAT[j])
						{
							tabNatureTarget.InsertTextLine( tabNatureSource.GetFieldValues(i, "TNAM,URNO"), false);
							tabNatureSource.DeleteLine(i);
							j--;
						}
					}
				}
				tabSectionSource.Sort("0", true, true);
				tabSectionTarget.Sort("0", true, true);
				tabAirlineSource.Sort("0,1", true, true);
				tabAirlineTarget.Sort("0,1", true, true);
				tabNatureSource.Sort("0", true, true);
				tabNatureTarget.Sort("0", true, true);
				UpdateButtons();
			}
		}

		private void btnDelete_Click(object sender, System.EventArgs e)
		{
			string strName = cbViewName.Text;
			if(strName == CtrlView.CFC_DEFAULT_VIEW)
			{
				return; //Nothing to do because it's <DEFAULT>
			}
			bool blFound = false;
			int idx = cbViewName.FindStringExact(strName, -1);
			if(idx > -1)
			{
				cbViewName.Items.RemoveAt(idx);
			}
			for(int i = 0; i < myViewsTab.GetLineCount() && blFound == false; i++)
			{
				if(strName == myViewsTab.GetFieldValue(i, "NAME"))
				{
					blFound = true; 
					myViewsTab.DeleteLine(i);
					myViewsTab.WriteToFile("C:\\Ufis\\System\\STATMGR_VIEWS.dat", false);
				}
			}
			cbViewName.Text = "";
			if(strName == CFC.CurrentViewName)
			{
				CFC.CurrentViewName = CtrlView.CFC_DEFAULT_VIEW;
			}
			NewView();
		}

		private void txtSection_TextChanged(object sender, System.EventArgs e)
		{
			SearchForTabEntry(txtSection, tabSectionSource, 1);
		}

		private void txtAirline_TextChanged(object sender, System.EventArgs e)
		{
			SearchForTabEntry(txtAirline, tabAirlineSource, 2);
		}

		private void txtNature_TextChanged(object sender, System.EventArgs e)
		{
			SearchForTabEntry(txtNature, tabNatureSource, 1);
		}

		private void SearchForTabEntry(System.Windows.Forms.TextBox txtBox, 
									   AxTABLib.AxTAB tabTab,
									   int StartColumns)
		{
			string strText = "," + txtBox.Text;
			string strValues = "";
			int idxOrig = -1;
			bool blFound = false;
			int i = 0;
			int idx = tabTab.GetCurrentSelected();
			idxOrig = idx; //to remember for the wrap
			if(idx == -1) idx = 0;
			for( i = idx; i < tabTab.GetLineCount() && blFound == false; i++)
			{
				strValues = ",";
				for(int j = 0; j < StartColumns; j++)
				{
					strValues += tabTab.GetColumnValue(i, j) + ",";
				}
				if(strValues.IndexOf(strText, 0) > -1)
				{
					blFound = true;
					tabTab.SetCurrentSelection(i);
					tabTab.OnVScrollTo(i);
				}
			}
			if(blFound == false)
			{
				for( i = 0; i <= idxOrig && blFound == false; i++)
				{
					strValues = ",";
					for(int j = 0; j < StartColumns; j++)
					{
						strValues += tabTab.GetColumnValue(i, j) + ",";
					}
					if(strValues.IndexOf(strText, 0) > -1)
					{
						blFound = true;
						tabTab.SetCurrentSelection(i);
						tabTab.OnVScrollTo(i);
					}
				}
			}
		}

		private void txtSection_KeyPress(object sender, System.Windows.Forms.KeyPressEventArgs e)
		{
			if(e.KeyChar == (char)13)
			{
				int currSel = tabSectionSource.GetCurrentSelected();
				if(currSel >= 0)
				{
					tabSectionTarget.InsertTextLine( tabSectionSource.GetFieldValues(currSel, "NAME,URNO"), true);
					tabSectionSource.DeleteLine(currSel);
					tabSectionTarget.Sort("0", true, true);
					UpdateButtons();
				}
			}
		}

		private void txtAirline_KeyPress(object sender, System.Windows.Forms.KeyPressEventArgs e)
		{
			if(e.KeyChar == (char)13)
			{
				int currSel = tabAirlineSource.GetCurrentSelected();
				if(currSel >= 0)
				{
					tabAirlineTarget.InsertTextLine( tabAirlineSource.GetFieldValues(currSel, "ALC2,ALC3,URNO"), true);
					tabAirlineSource.DeleteLine(currSel);
					tabAirlineTarget.Sort("0,1", true, true);
					UpdateButtons();
				}
			}
		}

		private void txtNature_KeyPress(object sender, System.Windows.Forms.KeyPressEventArgs e)
		{
			if(e.KeyChar == (char)13)
			{
				int currSel = tabNatureSource.GetCurrentSelected();
				if(currSel >= 0)
				{
					tabNatureTarget.InsertTextLine( tabNatureSource.GetFieldValues(currSel, "TNAM,URNO"), true);
					tabNatureSource.DeleteLine(currSel);
					tabNatureTarget.Sort("0", true, true);
					UpdateButtons();
				}
			}
		}
		private void EnableControlsForDefault(bool enable)
		{
			if(enable == true)
			{
				tabSectionSource.Enabled = true;
				tabSectionTarget.Enabled = true;
				tabAirlineSource.Enabled = true;
				tabAirlineTarget.Enabled = true;
				tabNatureSource.Enabled = true;
				tabNatureTarget.Enabled = true;
				tabSectionSource.DisplayBackColor = UT.colYellow;
				tabAirlineSource.DisplayBackColor = UT.colYellow;
				tabNatureSource.DisplayBackColor = UT.colYellow;

				txtSection.Enabled = true;
				txtAirline.Enabled = true;
				txtNature.Enabled = true;
				btnAirlineSource.Enabled = true;
				btnAirlineTarget.Enabled = true;
				btnSectionSource.Enabled = true;
				bntSectionTarget.Enabled = true;
				btnNatureSource.Enabled = true;
				btnNatureTarget.Enabled = true;
			}
			else
			{
				tabSectionSource.Enabled = false;
				tabSectionTarget.Enabled = false;
				tabAirlineSource.Enabled = false;
				tabAirlineTarget.Enabled = false;
				tabNatureSource.Enabled = false;
				tabNatureTarget.Enabled = false;
				tabSectionSource.DisplayBackColor = UT.colLightGray;
				tabAirlineSource.DisplayBackColor = UT.colLightGray;
				tabNatureSource.DisplayBackColor = UT.colLightGray;

				txtSection.Enabled = false;
				txtAirline.Enabled = false;
				txtNature.Enabled = false;
				btnAirlineSource.Enabled = false;
				btnAirlineTarget.Enabled = false;
				btnSectionSource.Enabled = false;
				bntSectionTarget.Enabled = false;
				btnNatureSource.Enabled = false;
				btnNatureTarget.Enabled = false;
			}
		}

		private void cbViewName_TextChanged(object sender, System.EventArgs e)
		{
			string strText = cbViewName.Text;
			if(strText != CtrlView.CFC_DEFAULT_VIEW)
			{
				EnableControlsForDefault(true);
			}
			else
			{
				EnableControlsForDefault(false);
			}
			if(strText == "")
			{
				btnOK.Enabled = false;
				btnDelete.Enabled = false;
				btnSave.Enabled = false;
			}
			else
			{
				btnOK.Enabled = true;
				btnDelete.Enabled = true;
				btnSave.Enabled = true;
			}
		}

		private void btnSave_Click(object sender, System.EventArgs e)
		{	
			string strMessage = SaveView(false);
			if(strMessage != "")
			{
				MessageBox.Show(this, strMessage, "Save Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
			}
		}

        /// <summary>
        /// Save and [set as Default View]
        /// </summary>
        /// <param name="setAsDefaultView">set as Default View</param>
        /// <returns></returns>
		private string SaveView(bool setAsDefaultView)
		{
			string strMessage = "";
			strMessage = CheckValidity();
			int i = 0;
			if(strMessage == "")
			{
				string strValues = "";
				string strName = cbViewName.Text;
				string strHSS = "";
				string strALT = "";
				string strNAT = "";
				string strFrom = "";
				string strTo = "";

				if(strName != CtrlView.CFC_DEFAULT_VIEW)
				{
					strHSS = tabSectionTarget.GetBufferByFieldList(0, tabSectionTarget.GetLineCount()-1, "URNO", "|");
					strALT = tabAirlineTarget.GetBufferByFieldList(0, tabAirlineTarget.GetLineCount()-1, "URNO", "|");
					strNAT = tabNatureTarget.GetBufferByFieldList(0, tabNatureTarget.GetLineCount()-1, "URNO", "|");
					strFrom = UT.DateTimeToCeda( dtFrom.Value );
					strTo   = UT.DateTimeToCeda( dtTo.Value );
					for( i = myViewsTab.GetLineCount()-1; i >= 0 ; i--)
					{
						if(myViewsTab.GetFieldValue(i, "NAME") == strName)
						{
							myViewsTab.DeleteLine(i);
						}
					}
					//"NAME,FROM,TO,HSS,ALT,NAT";

                    //Save in Local File
                    //strValues = strName + "," + strFrom + "," + strTo + "," + strHSS + "," + strALT + "," + strNAT;
                    //myViewsTab.InsertTextLine(strValues, true);   
					//myViewsTab.WriteToFile("C:\\Ufis\\System\\STATMGR_VIEWS.dat", false);

                    //Save in Database.
                    if (CtrlView.GetInstance().SaveView(setAsDefaultView, strName, strFrom, strTo, strHSS, strALT, strNAT))
                    {
                        strValues = strName + "," + strFrom + "," + strTo + "," + strHSS + "," + strALT + "," + strNAT;
                        myViewsTab.InsertTextLine(strValues, true);
                        myViewsTab.Sort("0", true, false);
                    }
                    else
                    {
                        MessageBox.Show("Fail to save!!!");
                    }
                    
				}
				if(cbViewName.FindStringExact(strName) == -1)
				{
					cbViewName.Items.Add(strName);
				}
			}
			return strMessage;
		}

		private void cbViewName_KeyPress(object sender, System.Windows.Forms.KeyPressEventArgs e)
		{
			if(e.KeyChar == (char)44)// check for comma
			{
				e.Handled = true;
			}
		}
	}
}
