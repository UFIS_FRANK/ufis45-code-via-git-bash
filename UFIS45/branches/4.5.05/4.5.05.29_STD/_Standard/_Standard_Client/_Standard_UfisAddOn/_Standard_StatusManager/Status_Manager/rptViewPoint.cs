using System;
using System.Drawing;
using DataDynamics.ActiveReports;
//using DataDynamics.ActiveReports.Document;
using Ufis.Utils;

using Ufis.Status_Manager.Ctrl;

namespace Ufis.Status_Manager.Rpt
{
	public class rptViewPointRun : ActiveReport3
	{
		int imImageSize = 48;
		System.Windows.Forms.PictureBox myPic = null;
		System.Windows.Forms.PictureBox srcPic = null;
		AxTABLib.AxTAB tabCurrFlights = null;
		AxTABLib.AxTAB myPosDefs = null;
		System.Windows.Forms.ImageList imageList = null;
		public rptViewPointRun(System.Windows.Forms.PictureBox pic, AxTABLib.AxTAB tabP, 
							   AxTABLib.AxTAB tabF, System.Windows.Forms.ImageList iL, int imgSize )
		{
			imImageSize = imgSize;
			imageList = iL; 
			myPosDefs = tabP;
			tabCurrFlights = tabF;
			srcPic = pic;
			InitializeReport();
		} 

		private void ActiveReport2_ReportStart(object sender, System.EventArgs eArgs)
		{
			myPic = new System.Windows.Forms.PictureBox();
			myPic = ((System.Windows.Forms.PictureBox)this.CustomControl1.Control);		
			myPic.Image = srcPic.Image;
			myPic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
			this.myPic.Paint +=new System.Windows.Forms.PaintEventHandler(myPic_Paint);
		}

		#region ActiveReports Designer generated code
		private DataDynamics.ActiveReports.PageHeader PageHeader;
		private DataDynamics.ActiveReports.Label Label1;
		private DataDynamics.ActiveReports.Detail Detail;
		private DataDynamics.ActiveReports.CustomControl CustomControl1;
		private DataDynamics.ActiveReports.PageFooter PageFooter;
		public void InitializeReport()
		{
			this.LoadLayout(this.GetType(), "Ufis.Status_Manager.rptViewPoint.rpx");
			this.PageHeader = ((DataDynamics.ActiveReports.PageHeader)(this.Sections["PageHeader"]));
			this.Detail = ((DataDynamics.ActiveReports.Detail)(this.Sections["Detail"]));
			this.PageFooter = ((DataDynamics.ActiveReports.PageFooter)(this.Sections["PageFooter"]));
			this.Label1 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[0]));
			this.CustomControl1 = ((DataDynamics.ActiveReports.CustomControl)(this.Detail.Controls[0]));
			// Attach Report Events
			this.ReportStart += new System.EventHandler(this.ActiveReport2_ReportStart);
		}

		#endregion

		private void myPic_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
		{
			Graphics g = e.Graphics;
			int ilCircleSize = (int)(imImageSize*.66);
			Pen penAqua = new Pen(Brushes.LightGray, 2);
			Pen penRed = new Pen(Brushes.Red, 4);
			int i=0;
			for( i = 0; i < tabCurrFlights.GetLineCount(); i++)
			{
				int llLine=0;
				PointF pointText;
				int myFontSize = 10;
				Font olFont;
				olFont = new Font("Courier New", imImageSize/5, FontStyle.Bold);

				myPosDefs.GetLinesByIndexValue("POS", tabCurrFlights.GetFieldValue(i, "POS"), 0);
				string strFlightUrno = tabCurrFlights.GetFieldValue(i, "URNO");
				String strAdid  = tabCurrFlights.GetFieldValue(i, "ADID");
				String strFlno  = tabCurrFlights.GetFieldValue(i, "FLNO");
				String strTisa  = tabCurrFlights.GetFieldValue(i, "TISA");
				String strTisd  = tabCurrFlights.GetFieldValue(i, "TISD");
				llLine = myPosDefs.GetNextResultLine();
				if(llLine > -1)
				{
					try
					{
						int x = -1;
						int y = -1;
						String strX = myPosDefs.GetFieldValue(llLine, "X");
						String strY = myPosDefs.GetFieldValue(llLine, "Y");
						string strRotate = myPosDefs.GetFieldValue(llLine, "ROTATE");
						String strAlign = myPosDefs.GetFieldValue(llLine, "ALIGN");
						x = Convert.ToInt32(strX);
						y = Convert.ToInt32(strY);

						int iw = imImageSize;
						float iTextSize = 0;
						int ih = iw;
						int ix = x - (iw/2);
						int iy = y - (ih/2);
						int cx = x + (iw/2);
						int cy = y + (ih/2);
						int nx = (iw/2)+ix;
						int ny = (ih/2)+iy;
						Point [] myPoints;
						
						Rectangle myR = new Rectangle(x - (iw/2), y - (ih/2), iw, ih);
						int idxImage=0;
						if(strAdid == "A")
						{
							idxImage = GetTisaColor(strTisa);
						}
						else
						{
							idxImage = GetTisdColor(strTisd);
						}
                        System.Drawing.Image im = (System.Drawing.Image)imageList.Images[idxImage];
									
						pointText = new PointF(-100, -100);
						iTextSize = g.MeasureString(strFlno, olFont).Width;
						switch(strAlign)
						{
							case "0":
								pointText = new PointF((float)x-(iw/2)-(iTextSize/2), (float)y-iw/2);//-(myFontSize/2));//-15);
								if(strAdid == "A" || strRotate != "J")
								{
									myPoints = new Point[] {new Point(nx,iy+ih+(ih/6)),
															   new Point(ix-(iw/6), ny),
															   new Point(ix+iw+(iw/6), ny)};
									g.DrawImage(im, myPoints);
								}
								else
								{
									myPoints = new Point[] {new Point(nx,           iy-(ih/6)),
															   new Point(ix+iw+(iw/6), ny),
															   new Point(ix-(iw/6),    ny)};
									g.DrawImage(im, myPoints);
								}
								break;
							case "1":
								pointText = new PointF((float)x-(iTextSize/2), (float)y-(iw/2)-myFontSize-(myFontSize/2));//-15);
								if(strAdid == "A" || strRotate != "J")
								{
									myPoints = new Point[] {new Point(myR.Left, myR.Bottom),
															   new Point(myR.Left, myR.Top),
															   new Point(myR.Right,  myR.Bottom)};
									g.DrawImage(im, myPoints);
								}
								else
								{
									myPoints = new Point[] {new Point(myR.Right, myR.Top),
															   new Point(myR.Right, myR.Bottom),
															   new Point(myR.Left,  myR.Top)};
									g.DrawImage(im, myPoints);
								}
								break;
							case "2":// 315�
								pointText = new PointF((float)x+(iw/2)-(iTextSize/2)+(myFontSize/2), (float)y-(iw/2)-(myFontSize/2));//-15);
								if(strAdid == "A" || strRotate != "J")
								{
									myPoints = new Point[] {new Point(ix-(iw/6), ny),
															   new Point(nx,        iy-(ih/6)),
															   new Point(nx,        iy+ih+(ih/6))};
									g.DrawImage(im, myPoints);
								}
								else
								{
									myPoints = new Point[] {new Point(ix+iw+(iw/6), ny),
															   new Point(nx,           iy+ih+(ih/6)),
															   new Point(nx,           iy-(ih/6))};
									g.DrawImage(im, myPoints);
								}
								break;
							case "3":
								pointText = new PointF((float)x+(iw/2), (float)y-(myFontSize/2));//-15);
								if(strAdid == "A" || strRotate != "J")
								{
									g.DrawImage(im, myR);
								}
								else
								{
									myPoints = new Point[] {new Point(myR.Right, myR.Bottom),
															   new Point(myR.Left, myR.Bottom),
															   new Point(myR.Right,  myR.Top)};
									g.DrawImage(im, myPoints);
								}
								break;
							case "4": //45�
								//pointText = new PointF((float)x-(iw/2)-(iTextSize/2), (float)y-iw/2);//-(myFontSize/2));//-15);
								pointText = new PointF((float)x+(iw/2)-(iTextSize/2)+myFontSize, (float)y+(iw/2)-(myFontSize/2));//-15);
								if(strAdid == "A" || strRotate != "J")
								{
									myPoints = new Point[] {new Point(nx,           iy-(ih/6)),
															   new Point(ix+iw+(iw/6), ny),
															   new Point(ix-(iw/6),    ny)};
									g.DrawImage(im, myPoints);
								}
								else
								{
									myPoints = new Point[] {new Point(nx,iy+ih+(ih/6)),
															   new Point(ix-(iw/6), ny),
															   new Point(ix+iw+(iw/6), ny)};
									g.DrawImage(im, myPoints);
								}
								break;
							case "5"://90�
								pointText = new PointF((float)x-(iTextSize/2), (float)y+(iw/2));//-15);
								if(strAdid == "A" || strRotate != "J")
								{
									myPoints = new Point[] {new Point(myR.Right, myR.Top),
															   new Point(myR.Right, myR.Bottom),
															   new Point(myR.Left,  myR.Top)};
									g.DrawImage(im, myPoints);
								}
								else
								{
									myPoints = new Point[] {new Point(myR.Left, myR.Bottom),
															   new Point(myR.Left, myR.Top),
															   new Point(myR.Right,  myR.Bottom)};
									g.DrawImage(im, myPoints);
								}
								break;
							case "6":
								pointText = new PointF((float)x-(iw/2)-(iTextSize/2), (float)y+(iw/2)-myFontSize);//-15);
								if(strAdid == "A" || strRotate != "J")
								{
									myPoints = new Point[] {new Point(ix+iw+(iw/6), ny),
															   new Point(nx,           iy+ih+(ih/6)),
															   new Point(nx,           iy-(ih/6))};
									g.DrawImage(im, myPoints);
								}
								else
								{
									myPoints = new Point[] {new Point(ix-(iw/6), ny),
															   new Point(nx,        iy-(ih/6)),
															   new Point(nx,        iy+ih+(ih/6))};
									g.DrawImage(im, myPoints);
								}
								break;
							case "7"://180�
								pointText = new PointF((float)x-(iw/2)-(iTextSize), (float)y-(myFontSize/2));//-15);
								if(strAdid == "A" || strRotate != "J")
								{
									myPoints = new Point[] {new Point(myR.Right, myR.Bottom),
															   new Point(myR.Left, myR.Bottom),
															   new Point(myR.Right,  myR.Top)};
									g.DrawImage(im, myPoints);
								}
								else
								{
									g.DrawImage(im, myR);
								}
								break;
							default:
								break;
						}
						g.DrawString(strFlno, olFont, Brushes.Blue, pointText);
						//Random olR = new Random();
						//int iR = olR.Next(5);
						//if(iR == 1)
						string strUrno = tabCurrFlights.GetFieldValue(i, "URNO");
						int CflCount = 0;
						int AckCount = 0;
						int TotalCount = 0;
						int OSTCount = 0;
                        //CFC.HasFlightStatusConflicts(strUrno, ref CflCount, ref AckCount, ref TotalCount, ref OSTCount);
                        CtrlCfl.GetInstance().GetConflictCount(strUrno, ref CflCount, ref AckCount, ref TotalCount, ref OSTCount);
						if(CflCount > 0)
						{
							g.FillEllipse(Brushes.Red, x-5, y-5, 10,10);
						}
						else if(AckCount > 0)
						{
							g.FillEllipse(Brushes.Orange, x-5, y-5, 10,10);
						}
					}
					catch(System.Exception err)
					{
						UT.AddException("rptViewPoint", "myPic_Paint", err.Message.ToString(), "", "");
						//MessageBox.Show(err.Message);
					}
				}
			}
			penAqua.Dispose();
			penRed.Dispose();
		}
		/// <summary>
		/// returns the index in the imagelists collection
		/// for the time status of arrival
		/// </summary>
		/// <param name="strTisa"></param>
		/// <returns></returns>
		private int GetTisaColor(String strTisa)
		{
			int retIDX=0;
			switch(strTisa)
			{
				case "S": //Sched
					retIDX = 8;//colGray;
					break;
				case "E": //Est.
					retIDX = 7;//colWhite
					break;
				case "T": //TMO
					retIDX = 12;//colYellow
					break;
				case "L": //Landed
					retIDX = 9;//colGreen
					break;
				case "O": //Onblock
					retIDX = 6;//colLime
					break;
			}
			return retIDX;
		}//END int GetTisaColor(String strTisa)
		/// <summary>
		/// returns the index in the imagelists collection
		/// for the time status of departure
		/// </summary>
		/// <param name="strTisa"></param>
		/// <returns></returns>
		private int GetTisdColor(String strTisa)
		{
			int retIDX=0;
			switch(strTisa)
			{
				case "O": //Offblock
					retIDX = 6;//colGray;
					break;
				case "A": //Est.
					retIDX = 9;//colWhite
					break;
				case "S": //TMO
					retIDX = 8;//colYellow
					break;
				case "E": //Landed
					retIDX = 7;//colGreen
					break;
			}
			return retIDX;
		}

	}
}
