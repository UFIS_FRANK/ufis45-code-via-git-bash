﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

namespace Ufis.Status_Manager.Ent
{
    public class EntFlight
    {
        public enum FlightProblemEnumType
        {
            NoProblem = 0,
            FlightProblem = 1,
            ManProblem = 2,
            EquipmentProblem = 4,
            StatusProblem = 8
        }

        public int Id { get; set; }
        public string FlightNo { get; set; }
        public string ADID { get; set; }
        public DateTime StandardTime { get; set; }
        public DateTime OnOffBlock { get; set; }
        public string Nature { get; set; }
        public string OrgDes { get; set; }
        public string ActType { get; set; }
        public EntPosition Position { get; set; }
        public FlightProblemEnumType FlightProblems { get; set; }
        public EntObjectCollection Objects { get; set; }
        public EntTimeCollection Times { get; set; }
        
        public bool Contains(Point point, double maxDistance)
        {
            //calculate the distance between the point and centerPoint
            int xDist = this.Position.CenterPoint.X - point.X;
            int yDist = this.Position.CenterPoint.Y - point.Y;
            double dist = Math.Sqrt(Math.Pow(xDist, 2) + Math.Pow(yDist, 2));
            return (dist <= maxDistance);
        }
    }

    public class EntFlightCollection : List<EntFlight>
    {
        private string[] flights =
            new string[]
            {
                ""
            };

        public void GenerateFlights(EntPositions positions)
        {
            Random rnd = new Random();
            int flightProblemCount = 16;

            //int posIdx = 0;
            //foreach (string flightItem in flights)
            //{
            //    string[] flightData = flightItem.Split(',');

            //    EntFlight flight = new EntFlight();
            //    flight.Id = Convert.ToInt32(flightData[0]);
            //    flight.FlightNo = flightData[1];
            //    flight.ADID = flightData[2];
            //    flight.Position = positions[posIdx];
            //    flight.FlightProblems = (EntFlight.FlightProblemEnumType)rnd.Next(0, flightProblemCount - 1);

            //    this.Add(flight);

            //    posIdx++;
            //}

            int flightId = 0;
            foreach (EntPosition position in positions)
            {
                EntFlight flight = new EntFlight();
                flight.Id = flightId;
                flight.FlightNo = "SQ 123";
                flight.ADID = "A";
                flight.Position = position;
                flight.FlightProblems = (EntFlight.FlightProblemEnumType)rnd.Next(0, flightProblemCount - 1);

                this.Add(flight);
            }
        }

        public EntFlightCollection()
            : this(false, null)
        {
        }

        public EntFlightCollection(bool autoGenerate, EntPositions positions)
        {
            if (autoGenerate)
            {
                GenerateFlights(positions);
            }
        }
    }
}