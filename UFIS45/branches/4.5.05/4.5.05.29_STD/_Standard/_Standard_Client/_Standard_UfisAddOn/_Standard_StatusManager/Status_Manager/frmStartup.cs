using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using Ufis.Utils ;

namespace Ufis.Status_Manager.UI
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class frmStartup : System.Windows.Forms.Form
	{
		private double myOpacity = 0.9f;


		private System.Windows.Forms.PictureBox pictureBox1;
		private System.Windows.Forms.PictureBox pictureBox2;
		private System.Windows.Forms.TextBox txtApplication;
		public System.Windows.Forms.TextBox txtCurrentStatus;
		public System.Windows.Forms.TextBox txtDoneStatus;
		public static frmStartup myThis;
		private System.Timers.Timer timerSplash;
		private System.Timers.Timer timerClose;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public frmStartup()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			myThis = this;
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		/// <summary>
		/// returns the this reference to get access from outside the class
		/// </summary>
		/// <returns>Reference to frmStartup</returns>
		public static frmStartup GetThis()
		{
			return myThis;
		}
		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmStartup));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.txtApplication = new System.Windows.Forms.TextBox();
            this.txtCurrentStatus = new System.Windows.Forms.TextBox();
            this.txtDoneStatus = new System.Windows.Forms.TextBox();
            this.timerSplash = new System.Timers.Timer();
            this.timerClose = new System.Timers.Timer();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.timerSplash)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.timerClose)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            resources.ApplyResources(this.pictureBox1, "pictureBox1");
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox2
            // 
            resources.ApplyResources(this.pictureBox2, "pictureBox2");
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.TabStop = false;
            // 
            // txtApplication
            // 
            this.txtApplication.BorderStyle = System.Windows.Forms.BorderStyle.None;
            resources.ApplyResources(this.txtApplication, "txtApplication");
            this.txtApplication.Name = "txtApplication";
            // 
            // txtCurrentStatus
            // 
            this.txtCurrentStatus.BorderStyle = System.Windows.Forms.BorderStyle.None;
            resources.ApplyResources(this.txtCurrentStatus, "txtCurrentStatus");
            this.txtCurrentStatus.Name = "txtCurrentStatus";
            // 
            // txtDoneStatus
            // 
            this.txtDoneStatus.BorderStyle = System.Windows.Forms.BorderStyle.None;
            resources.ApplyResources(this.txtDoneStatus, "txtDoneStatus");
            this.txtDoneStatus.Name = "txtDoneStatus";
            // 
            // timerSplash
            // 
            this.timerSplash.Interval = 50;
            this.timerSplash.SynchronizingObject = this;
            this.timerSplash.Elapsed += new System.Timers.ElapsedEventHandler(this.timerSplash_Elapsed);
            // 
            // timerClose
            // 
            this.timerClose.Interval = 75;
            this.timerClose.SynchronizingObject = this;
            this.timerClose.Elapsed += new System.Timers.ElapsedEventHandler(this.timerClose_Elapsed);
            // 
            // frmStartup
            // 
            resources.ApplyResources(this, "$this");
            this.Controls.Add(this.txtDoneStatus);
            this.Controls.Add(this.txtCurrentStatus);
            this.Controls.Add(this.txtApplication);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.Name = "frmStartup";
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.frmStartup_Paint);
            this.Load += new System.EventHandler(this.frmStartup_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.timerSplash)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.timerClose)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		#endregion

		private void frmStartup_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
		{
			UT.DrawGradient(e.Graphics , 90f, this, Color.WhiteSmoke , Color.DarkGray  );			
		}

		private void frmStartup_Load(object sender, System.EventArgs e)
		{
			this.Text = "Startup Status-Manager on server " + UT.ServerName;
		}

		private void timerSplash_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
		{
			myOpacity += 0.1;
			this.Opacity = myOpacity;
			if(myOpacity >= 1)
			{
				timerSplash.Enabled = false;
			}
		}
		public void PrepareClose()
		{
			timerClose.Enabled = true;
		}

		private void timerClose_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
		{
			myOpacity -= 0.2;
			this.Opacity = myOpacity;
			pictureBox1.Refresh();
			txtCurrentStatus.Refresh();
			txtDoneStatus.Refresh();
			if(myOpacity <= 0f)
			{
				timerClose.Stop();
				timerClose.Enabled = false;
				this.Close();
			}
		}

	}
}
