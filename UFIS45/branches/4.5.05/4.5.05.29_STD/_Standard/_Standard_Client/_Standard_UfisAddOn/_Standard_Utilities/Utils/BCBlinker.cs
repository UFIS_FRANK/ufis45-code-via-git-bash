using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;

namespace Ufis.Utils
{
	/// <summary>
	/// BCBlinker is the boradcast traffic light to indicate that the application
	/// is currently processing broadcasts. The control is a visual component, which
	/// displays two cirlces - one in blue and the other one in light green. The next 
	/// broadcast toggles both circles.
	/// </summary>
	/// <remarks>
	/// To use it in you application, you should grag &amp; drop this control to your
	/// main window form. You should have an instance of any kind of a <B>DataExchange</B>
	/// class, which is responsible to hand over the broadcasts from the <see cref="Ufis.Data"/> 
	/// namespace classes into you application. For reading an example of this
	/// DataChanges class please read <see cref="Ufis.Data.DatabaseEventArgs"/>
	/// </remarks>
	public class BCBlinker : System.Windows.Forms.UserControl
	{
		private System.Windows.Forms.PictureBox tr1;
		private System.Windows.Forms.PictureBox tr2;
		/// <summary>
		/// Indicates whether the left or the right circle are to be hightlighted.
		/// </summary>
		/// <remarks>none.</remarks>
		public bool bmLeft = true;
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		/// <summary>
		/// Default constructor for the component.
		/// </summary>
		/// <remarks>none.</remarks>
		public BCBlinker()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();

			// TODO: Add any initialization after the InitializeComponent call

		}
		/// <summary>
		/// Forces the control to toggle the two circle colours to indicat visually
		/// that a broadcast was received.
		/// </summary>
		/// <remarks>For each received broadcast you should call the
		/// <B>BCBlinker.Blink()</B> method to toggle the blue and green
		/// circles. This gives the user of your application the information
		/// about receiving actively broadcasts.</remarks>
		public void Blink()
		{
			if (bmLeft == true)
				bmLeft = false;
			else
				bmLeft = true;
			tr1.Invalidate();
			tr2.Invalidate();
		}
		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">Threaded indicator.</param>
		/// <remarks>none.</remarks>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Component Designer generated code
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.tr1 = new System.Windows.Forms.PictureBox();
			this.tr2 = new System.Windows.Forms.PictureBox();
			this.SuspendLayout();
			// 
			// tr1
			// 
			this.tr1.BackColor = System.Drawing.Color.Transparent;
			this.tr1.Location = new System.Drawing.Point(4, 4);
			this.tr1.Name = "tr1";
			this.tr1.Size = new System.Drawing.Size(16, 16);
			this.tr1.TabIndex = 0;
			this.tr1.TabStop = false;
			this.tr1.Paint += new System.Windows.Forms.PaintEventHandler(this.tr1_Paint);
			// 
			// tr2
			// 
			this.tr2.BackColor = System.Drawing.Color.Transparent;
			this.tr2.Location = new System.Drawing.Point(24, 4);
			this.tr2.Name = "tr2";
			this.tr2.Size = new System.Drawing.Size(16, 16);
			this.tr2.TabIndex = 1;
			this.tr2.TabStop = false;
			this.tr2.Paint += new System.Windows.Forms.PaintEventHandler(this.tr2_Paint);
			// 
			// BCBlinker
			// 
			this.BackColor = System.Drawing.SystemColors.Control;
			this.Controls.Add(this.tr2);
			this.Controls.Add(this.tr1);
			this.Location = new System.Drawing.Point(4, 4);
			this.Name = "BCBlinker";
			this.Size = new System.Drawing.Size(44, 24);
			this.Load += new System.EventHandler(this.BCBlinker_Load);
			this.ResumeLayout(false);

		}
		#endregion

		private void tr1_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
		{
			if (bmLeft == true)
			{
				
				SolidBrush olB = new SolidBrush(Color.LightGreen);
				e.Graphics.FillEllipse(olB, 0,0,tr1.Width , tr1.Height);
				olB.Dispose();
			}
			else
			{
				SolidBrush olB = new SolidBrush(Color.Blue);
				e.Graphics.FillEllipse(olB, 0,0,tr1.Width , tr1.Height);
				olB.Dispose();
			}
		}

		private void tr2_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
		{
			if (bmLeft == false)
			{
				SolidBrush olB = new SolidBrush(Color.LightGreen );
				e.Graphics.FillEllipse(olB, 0, 0, tr2.Width , tr2.Height);
				olB.Dispose();
			}
			else
			{
				SolidBrush olB = new SolidBrush(Color.Blue);
				e.Graphics.FillEllipse(olB, 0, 0, tr2.Width , tr2.Height);
				olB.Dispose();
			}
		}

		private void BCBlinker_Load(object sender, System.EventArgs e)
		{
		
		}
	}
}
