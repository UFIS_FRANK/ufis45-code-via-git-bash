﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ufis.Utils
{
    public class Utility
    {
        public static string DateToHourDivString(DateTime dateTime, DateTime date)
        {
            TimeSpan dtDiff = dateTime.Subtract(date.Date);

            var day = 0;
            var hour = 0;
            var min = 0;

            var stHour = string.Empty;
            var stMin = string.Empty;
            var result = string.Empty;

            if (dtDiff.Hours < 0 || dtDiff.Hours < 0 || dtDiff.Minutes < 0)
            {
                day = 1 + (-1 * dtDiff.Days);
                hour = 24 + dtDiff.Hours;
                min = 60 + dtDiff.Minutes;
                if (min > 0 && min < 60)
                {
                    hour = hour - 1;
                }
                if (min == 60)
                {
                    min = 0;
                }

                if (hour.ToString().Length < 2)
                    stHour = "0" + hour.ToString();
                else
                    stHour = hour.ToString();

                if (min.ToString().Length < 2)
                    stMin = "0" + min.ToString();
                else
                    stMin = min.ToString();

                if (day > 0)
                    result = stHour + ":" + stMin + "-" + day.ToString();
                else
                    result = stHour + ":" + stMin;
            }
            else
            {
                day = dtDiff.Days;
                hour = dtDiff.Hours;
                min = dtDiff.Minutes;

                if (hour.ToString().Length < 2)
                    stHour = "0" + hour.ToString();
                else
                    stHour = hour.ToString();

                if (min.ToString().Length < 2)
                    stMin = "0" + min.ToString();
                else
                    stMin = min.ToString();
                if (day > 0)
                    result = stHour + ":" + stMin + "+" + day.ToString();
                else
                    result = stHour + ":" + stMin;
            }
            return result;
        }

        public static string HourStringToDate(string time, DateTime date)
        {
            return string.Empty;
        }

        public static DateTime HourStringToDateTime(string time, DateTime dateTime)
        {
            DateTime result = DateTime.MinValue;

            try
            {
                result = new DateTime(dateTime.Year, dateTime.Month, dateTime.Day);

                if (time.Contains(":"))
                {
                    if (Convert.ToInt16(time.Substring(0, 2)) < 24 && Convert.ToInt16(time.Substring(3, 2)) < 60)
                    {
                        if (time.Contains("+"))
                        {
                            String[] hourMin = time.Split('+');

                            if (hourMin.Length == 2)
                            {
                                foreach (var item in hourMin)
                                {
                                    String[] delimiter = item.Split(':');

                                    if (delimiter.Length == 2)
                                    {
                                        if (delimiter[0].Trim().Length == 2 && delimiter[1].Trim().Length == 2)
                                        {
                                            result = result.AddHours(Convert.ToDouble(delimiter[0]));
                                            result = result.AddMinutes(Convert.ToDouble(delimiter[1]));
                                        }
                                        else
                                        {
                                            throw new ApplicationException("Invalid Time format");
                                        }
                                    }
                                    else if (delimiter.Length == 1)
                                    {
                                        if (delimiter[0].Trim().Length == 4)
                                        {
                                            result = result.AddHours(Convert.ToDouble(time.Substring(0, 2)));
                                            result = result.AddMinutes(Convert.ToDouble(time.Substring(2, 2)));
                                        }
                                        else if (delimiter[0].Trim().Length < 2)
                                        {
                                            result = result.AddDays(Convert.ToDouble(delimiter[0]));
                                        }
                                        else
                                        {
                                            throw new ApplicationException("Invalid Time format");
                                        }
                                    }
                                    else
                                    {
                                        throw new ApplicationException("Invalid Time format");
                                    }
                                }
                            }
                            else
                            {
                                throw new ApplicationException("Invalid Time format");
                            }
                        }
                        else if (time.Contains("-"))
                        {
                            String[] hourMin = time.Split('-');

                            if (hourMin.Length == 2)
                            {
                                foreach (var item in hourMin)
                                {
                                    String[] delimiter = item.Split(':');

                                    if (delimiter.Length == 2)
                                    {
                                        if (delimiter[0].Trim().Length == 2 && delimiter[1].Trim().Length == 2)
                                        {
                                            result = result.AddHours(Convert.ToDouble(delimiter[0]));
                                            result = result.AddMinutes(Convert.ToDouble(delimiter[1]));
                                        }
                                        else
                                        {
                                            throw new ApplicationException("Invalid Time format");
                                        }
                                    }
                                    else if (delimiter.Length == 1)
                                    {
                                        if (delimiter[0].Trim().Length == 4)
                                        {
                                            result = result.AddHours(Convert.ToDouble(time.Substring(0, 2)));
                                            result = result.AddMinutes(Convert.ToDouble(time.Substring(2, 2)));
                                        }
                                        else if (delimiter[0].Trim().Length < 2)
                                        {
                                            result = result.AddDays((-1) * Convert.ToDouble(delimiter[0]));
                                        }
                                        else
                                        {
                                            throw new ApplicationException("Invalid Time format");
                                        }
                                    }
                                    else
                                    {
                                        throw new ApplicationException("Invalid Time format");
                                    }
                                }
                            }
                            else
                            {
                                throw new ApplicationException("Invalid Time format");
                            }
                        }
                        else
                        {
                            String[] delimiter = time.Split(':');

                            if (delimiter.Length == 2)
                            {
                                if (delimiter[0].Trim().Length == 2 && delimiter[1].Trim().Length == 2)
                                {
                                    result = result.AddHours(Convert.ToDouble(delimiter[0]));
                                    result = result.AddMinutes(Convert.ToDouble(delimiter[1]));
                                }
                                else
                                {
                                    throw new ApplicationException("Invalid Time format");
                                }
                            }
                            else
                            {
                                throw new ApplicationException("Invalid Time format");
                            }
                        }
                    }
                    else
                    {
                        throw new ApplicationException("Invalid Time format");
                    }

                }
                else
                {
                    if (Convert.ToInt16(time.Substring(0, 2)) < 24 && Convert.ToInt16(time.Substring(2, 2)) < 60)
                    {
                        if (time.Contains("+"))
                        {
                            String[] hourMin = time.Split('+');

                            if (hourMin.Length == 2)
                            {
                                if (hourMin[0].Trim().Length == 4)
                                {
                                    result = result.AddHours(Convert.ToDouble(hourMin[0].Substring(0, 2)));
                                    result = result.AddMinutes(Convert.ToDouble(hourMin[0].Substring(2, 2)));
                                }
                                else if (hourMin[1].Trim().Length < 2)
                                {
                                    result = result.AddDays(Convert.ToDouble(hourMin[1]));
                                }
                                else
                                {
                                    throw new ApplicationException("Invalid Time format");
                                }
                            }
                            else
                            {
                                throw new ApplicationException("Invalid Time format");
                            }
                        }
                        else if (time.Contains("-"))
                        {
                            String[] hourMin = time.Split('-');

                            if (hourMin.Length == 2)
                            {
                                if (hourMin[0].Trim().Length == 4)
                                {
                                    result = result.AddHours(Convert.ToDouble(time.Substring(0, 2)));
                                    result = result.AddMinutes(Convert.ToDouble(time.Substring(2, 2)));
                                }
                                else if (hourMin[1].Trim().Length < 2)
                                {
                                    result = result.AddDays((-1) * Convert.ToDouble(hourMin[1]));
                                }
                                else
                                {
                                    throw new ApplicationException("Invalid Time format");
                                }
                            }
                            else
                            {
                                throw new ApplicationException("Invalid Time format");
                            }
                        }
                        else if (time.Trim().Length == 4)
                        {
                            result = result.AddHours(Convert.ToDouble(time.Substring(0, 2)));
                            result = result.AddMinutes(Convert.ToDouble(time.Substring(2, 2)));
                        }
                        else
                        {
                            throw new ApplicationException("Invalid Time format");
                        }
                    }
                    else
                    {
                        throw new ApplicationException("Invalid Time format");
                    }
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Invalid Time format");
            }

            return result;
        }

        public static string HourStringToDateTimeString(string time, DateTime dateTime, string format)
        {
            DateTime result = DateTime.MinValue;

            try
            {
                result = new DateTime(dateTime.Year, dateTime.Month, dateTime.Day);

                if (time.Contains("+"))
                {
                    String[] hourMin = time.Split('+');

                    if (hourMin.Length == 2)
                    {
                        foreach (var item in hourMin)
                        {
                            String[] delimiter = item.Split(':');

                            if (delimiter.Length == 2)
                            {
                                if (delimiter[0].Trim().Length <= 2 && delimiter[1].Trim().Length <= 2 && Convert.ToInt16(delimiter[0]) < 24 && Convert.ToInt16(delimiter[1]) < 60)
                                {
                                    result = result.AddHours(Convert.ToDouble(delimiter[0]));
                                    result = result.AddMinutes(Convert.ToDouble(delimiter[1]));
                                }
                                else
                                {
                                    throw new ApplicationException("Invalid Time format");
                                }
                            }
                            else if (delimiter.Length == 1)
                            {
                                if (item.Trim().Length == 4)
                                {
                                    if (Convert.ToInt16(time.Substring(0, 2)) < 24 && Convert.ToInt16(time.Substring(2, 2)) < 60)
                                    {
                                        result = result.AddHours(Convert.ToDouble(time.Substring(0, 2)));
                                        result = result.AddMinutes(Convert.ToDouble(time.Substring(2, 2)));
                                    }
                                    else
                                    {
                                        throw new ApplicationException("Invalid Time format");
                                    }

                                }
                                else
                                {
                                    if (delimiter[0].Trim().Length <= 2)
                                    {
                                        result = result.AddDays(Convert.ToDouble(delimiter[0]));
                                    }
                                    else
                                    {
                                        throw new ApplicationException("Invalid Time format");
                                    }
                                }
                            }
                            else
                            {
                                throw new ApplicationException("Invalid Time format");
                            }
                        }
                    }
                    else
                    {
                        throw new ApplicationException("Invalid Time format");
                    }
                }
                else if (time.Contains("-"))
                {

                    String[] hourMin = time.Split('-');

                    if (hourMin.Length == 2)
                    {
                        foreach (var item in hourMin)
                        {
                            String[] delimiter = item.Split(':');
                            if (delimiter.Length == 2)
                            {
                                if (delimiter[0].Trim().Length <= 2 && delimiter[1].Trim().Length <= 2 && Convert.ToInt16(delimiter[0]) < 24 && Convert.ToInt16(delimiter[1]) < 60)
                                {
                                    result = result.AddHours(Convert.ToDouble(delimiter[0]));
                                    result = result.AddMinutes(Convert.ToDouble(delimiter[1]));
                                }
                                else
                                {
                                    throw new ApplicationException("Invalid Time format");
                                }
                            }
                            else if (delimiter.Length == 1)
                            {
                                if (item.Trim().Length == 4)
                                {
                                    if (Convert.ToInt16(time.Substring(0, 2)) < 24 && Convert.ToInt16(time.Substring(2, 2)) < 60)
                                    {
                                        result = result.AddHours(Convert.ToDouble(time.Substring(0, 2)));
                                        result = result.AddMinutes(Convert.ToDouble(time.Substring(2, 2)));
                                    }
                                    else
                                    {
                                        throw new ApplicationException("Invalid Time format");
                                    }
                                }
                                else
                                {
                                    if (delimiter[0].Trim().Length <= 2)
                                    {
                                        result = result.AddDays((-1) * Convert.ToDouble(delimiter[0]));
                                    }
                                    else
                                    {
                                        throw new ApplicationException("Invalid Time format");
                                    }
                                }

                            }
                            else
                            {
                                throw new ApplicationException("Invalid Time format");
                            }
                        }
                    }
                    else
                    {
                        throw new ApplicationException("Invalid Time format");
                    }
                }
                else
                {
                    if (time.Contains(":"))
                    {
                        String[] delimiter = time.Split(':');
                        if (delimiter.Length == 2)
                        {
                            if (Convert.ToInt16(delimiter[0]) < 24 && Convert.ToInt16(delimiter[1]) < 60)
                            {
                                result = result.AddHours(Convert.ToDouble(delimiter[0]));
                                result = result.AddMinutes(Convert.ToDouble(delimiter[1]));
                            }
                            else
                            {
                                throw new ApplicationException("Invalid Time format");
                            }
                        }
                        else
                        {
                            throw new ApplicationException("Invalid Time format");
                        }
                    }
                    else
                    {
                        if (time.Trim().Length == 4)
                        {
                            if (Convert.ToInt16(time.Substring(0, 2)) < 24 && Convert.ToInt16(time.Substring(2, 2)) < 60)
                            {
                                result = result.AddHours(Convert.ToDouble(time.Substring(0, 2)));
                                result = result.AddMinutes(Convert.ToDouble(time.Substring(2, 2)));
                            }
                            else
                            {
                                throw new ApplicationException("Invalid Time format");
                            }

                        }
                        else
                        {
                            throw new ApplicationException("Invalid Time format");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Invalid Time format");
            }

            return result.ToString(format);
        }
    }
}
