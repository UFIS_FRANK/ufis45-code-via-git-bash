using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace Ufis.Utils
{
	/// <summary>
	/// An instance of this class shows a simple dialog with with the application's title, 
	/// version. The about box contains a grid (Tab.ocx) with the information about all
	/// used Ufis comonents and their version including the build date.
	/// </summary>
	/// <remarks>Please design a button or a menu item in you application
	/// to display this about box in order to give the user the indformation
	/// about the release and the used components including the build dates and
	/// the revisions. This is a <B>MUST</B> for all applications.</remarks>
	public class frmAbout : System.Windows.Forms.Form
	{
		private System.Windows.Forms.PictureBox pictureBox1;
		private System.Windows.Forms.PictureBox pictureBox2;
		private System.Windows.Forms.Label lblApplication;
		private System.Windows.Forms.Label lblVersion;
		private System.Windows.Forms.Label lblCompany;
		private AxTABLib.AxTAB VersionTab;
		/// <summary>
		/// This string represents the application name and must be set from the application.
		/// </summary>
		/// <remarks>none.</remarks>
		public string strAppName = "";
		/// <summary>
		/// This string contains the version of the application and must be set from outside.
		/// Usually it should contain the major release number and the application's version, 
		/// e.g. 4.5.1.8
		/// </summary>
		/// <remarks>none.</remarks>
		public string strAppVersion = "";
		/// <summary>
		/// This string represents the ATL-Servers, which are used by this application.
		/// The string must be set from the application to be shown in the about box.
		/// </summary>
		/// <remarks>none.</remarks>
		public string strAtlServers = "";
		private AxUCOMLib.AxUCom myUCom;
		private AxUFISCOMLib.AxUfisCom myUfisCom;
		private AxUGANTTLib.AxUGantt myGantt;
		private System.Timers.Timer timer1;
		private double myOpacity = 0;
		private System.Timers.Timer timer2;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		/// <remarks>none.</remarks>
		private System.ComponentModel.Container components = null;

		/// <summary>
		/// Default constructor.
		/// </summary>
		/// <remarks>none.</remarks>
		public frmAbout()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Cleanup the reseources beeing in use.
		/// </summary>
		/// <param name="disposing">Threaded indicator.</param>
		/// <remarks>none.</remarks>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(frmAbout));
			this.pictureBox1 = new System.Windows.Forms.PictureBox();
			this.pictureBox2 = new System.Windows.Forms.PictureBox();
			this.lblApplication = new System.Windows.Forms.Label();
			this.lblVersion = new System.Windows.Forms.Label();
			this.lblCompany = new System.Windows.Forms.Label();
			this.VersionTab = new AxTABLib.AxTAB();
			this.myUCom = new AxUCOMLib.AxUCom();
			this.myUfisCom = new AxUFISCOMLib.AxUfisCom();
			this.myGantt = new AxUGANTTLib.AxUGantt();
			this.timer1 = new System.Timers.Timer();
			this.timer2 = new System.Timers.Timer();
			((System.ComponentModel.ISupportInitialize)(this.VersionTab)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.myUCom)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.myUfisCom)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.myGantt)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.timer1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.timer2)).BeginInit();
			this.SuspendLayout();
			// 
			// pictureBox1
			// 
			this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
			this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
			this.pictureBox1.Location = new System.Drawing.Point(8, 8);
			this.pictureBox1.Name = "pictureBox1";
			this.pictureBox1.Size = new System.Drawing.Size(40, 32);
			this.pictureBox1.TabIndex = 0;
			this.pictureBox1.TabStop = false;
			// 
			// pictureBox2
			// 
			this.pictureBox2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.pictureBox2.Location = new System.Drawing.Point(0, 0);
			this.pictureBox2.Name = "pictureBox2";
			this.pictureBox2.Size = new System.Drawing.Size(496, 246);
			this.pictureBox2.TabIndex = 1;
			this.pictureBox2.TabStop = false;
			this.pictureBox2.Paint += new System.Windows.Forms.PaintEventHandler(this.pictureBox2_Paint);
			// 
			// lblApplication
			// 
			this.lblApplication.BackColor = System.Drawing.Color.Transparent;
			this.lblApplication.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblApplication.Location = new System.Drawing.Point(72, 8);
			this.lblApplication.Name = "lblApplication";
			this.lblApplication.Size = new System.Drawing.Size(120, 16);
			this.lblApplication.TabIndex = 2;
			this.lblApplication.Text = "Application";
			// 
			// lblVersion
			// 
			this.lblVersion.BackColor = System.Drawing.Color.Transparent;
			this.lblVersion.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblVersion.Location = new System.Drawing.Point(72, 32);
			this.lblVersion.Name = "lblVersion";
			this.lblVersion.Size = new System.Drawing.Size(120, 16);
			this.lblVersion.TabIndex = 3;
			this.lblVersion.Text = "Version";
			// 
			// lblCompany
			// 
			this.lblCompany.BackColor = System.Drawing.Color.Transparent;
			this.lblCompany.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblCompany.Location = new System.Drawing.Point(288, 32);
			this.lblCompany.Name = "lblCompany";
			this.lblCompany.Size = new System.Drawing.Size(160, 16);
			this.lblCompany.TabIndex = 4;
			this.lblCompany.Text = "UFIS Airport Solutions GmbH";
			// 
			// VersionTab
			// 
			this.VersionTab.Location = new System.Drawing.Point(16, 72);
			this.VersionTab.Name = "VersionTab";
			this.VersionTab.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("VersionTab.OcxState")));
			this.VersionTab.Size = new System.Drawing.Size(456, 136);
			this.VersionTab.TabIndex = 5;
			// 
			// myUCom
			// 
			this.myUCom.Enabled = true;
			this.myUCom.Location = new System.Drawing.Point(432, 40);
			this.myUCom.Name = "myUCom";
			this.myUCom.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("myUCom.OcxState")));
			this.myUCom.Size = new System.Drawing.Size(100, 50);
			this.myUCom.TabIndex = 6;
			// 
			// myUfisCom
			// 
			this.myUfisCom.Enabled = true;
			this.myUfisCom.Location = new System.Drawing.Point(424, 88);
			this.myUfisCom.Name = "myUfisCom";
			this.myUfisCom.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("myUfisCom.OcxState")));
			this.myUfisCom.Size = new System.Drawing.Size(100, 50);
			this.myUfisCom.TabIndex = 7;
			// 
			// myGantt
			// 
			this.myGantt.Location = new System.Drawing.Point(432, 0);
			this.myGantt.Name = "myGantt";
			this.myGantt.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("myGantt.OcxState")));
			this.myGantt.Size = new System.Drawing.Size(100, 50);
			this.myGantt.TabIndex = 8;
			this.myGantt.Visible = false;
			// 
			// timer1
			// 
			this.timer1.Interval = 50;
			this.timer1.SynchronizingObject = this;
			this.timer1.Elapsed += new System.Timers.ElapsedEventHandler(this.timer1_Elapsed);
			// 
			// timer2
			// 
			this.timer2.Enabled = true;
			this.timer2.SynchronizingObject = this;
			this.timer2.Elapsed += new System.Timers.ElapsedEventHandler(this.timer2_Elapsed);
			// 
			// frmAbout
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(496, 246);
			this.Controls.Add(this.myGantt);
			this.Controls.Add(this.myUfisCom);
			this.Controls.Add(this.myUCom);
			this.Controls.Add(this.VersionTab);
			this.Controls.Add(this.lblCompany);
			this.Controls.Add(this.lblVersion);
			this.Controls.Add(this.lblApplication);
			this.Controls.Add(this.pictureBox1);
			this.Controls.Add(this.pictureBox2);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "frmAbout";
			this.Opacity = 0;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "frmAbout";
			this.Load += new System.EventHandler(this.frmAbout_Load);
			((System.ComponentModel.ISupportInitialize)(this.VersionTab)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.myUCom)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.myUfisCom)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.myGantt)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.timer1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.timer2)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		private void frmAbout_Load(object sender, System.EventArgs e)
		{
			pictureBox1.Parent = this.pictureBox2;
			lblApplication.Parent = this.pictureBox2;
			lblCompany.Parent = this.pictureBox2;
			lblVersion.Parent = this.pictureBox2;
			VersionTab.ResetContent();
			VersionTab.FontName = "Arial";
			VersionTab.FontSize = 16;
			VersionTab.LineHeight = 19;
			//VersionTab.left = picIcon.left
		    string strApp="";
			strApp.PadLeft(66, ' ');
			VersionTab.HeaderString = "Component,Version,Built,Remark" + strApp;
			VersionTab.HeaderLengthString = "100,100,100,100";
			VersionTab.HeaderAlignmentString = "C,C,C,L";
			VersionTab.ColumnAlignmentString = "L,C,C,L";
			VersionTab.EmptyAreaRightColor = UT.colWhite;
			VersionTab.SetMainHeaderValues( "4", "Integrated Ufis Components", "");
			VersionTab.SetMainHeaderFont(16, false, false, true, 0, "Arial");
			VersionTab.MainHeader = true;
			VersionTab.AutoSizeByHeader = true;
			VersionTab.SetFieldSeparator("|");
			VersionTab.Refresh();
			string strValues = "";
			strValues += "UGantt.ocx" + "|" + myGantt.Version.ToString() + "|" + myGantt.BuildDate.ToString() + "|                                                                                  \n"; 
			strValues += "Tab.ocx" + "|" + VersionTab.Version.ToString() + "|" + VersionTab.BuildDate.ToString() + "|              \n"; 
			strValues += "UCom.ocx" + "|" + myUCom.Version.ToString() + "|" + myUCom.BuildDate.ToString() + "|              \n"; 
			strValues += "UfisCom.ocx" + "|" + myUfisCom.Version.ToString() + "|" + myUfisCom.BuildDate.ToString() + "|              \n"; 
			strValues = strValues.Replace(",", ".");
			VersionTab.InsertBuffer(strValues, "\n");
			VersionTab.AutoSizeColumns();
			lblApplication.Text = strAppName;
			this.Text = strAppName;
			lblVersion.Text = strAppVersion;
		}

		private void timer1_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
		{
			myOpacity += 0.1;
			this.Opacity = myOpacity;
			//this.Refresh();
			if(myOpacity >= 1)
			{
				timer1.Enabled = false;
			}
		}

		private void pictureBox2_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
		{
			UT.DrawGradient(e.Graphics, 90f, pictureBox2, Color.WhiteSmoke, Color.DarkGray);
		}

		private void timer2_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
		{
			timer1.Enabled = true;
			timer2.Enabled = false;
		}
	}
}
