VERSION 5.00
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "TAB.ocx"
Begin VB.Form frmData2 
   Caption         =   "frmData2"
   ClientHeight    =   12015
   ClientLeft      =   165
   ClientTop       =   555
   ClientWidth     =   13890
   LinkTopic       =   "Form1"
   ScaleHeight     =   12015
   ScaleWidth      =   13890
   StartUpPosition =   3  'Windows Default
   Begin TABLib.TAB TabSTF 
      Height          =   870
      Left            =   75
      TabIndex        =   25
      Tag             =   "{=TABLE=}STFTAB{=FIELDS=}URNO,PENO,LANM,FINM,PERC,SHNM"
      Top             =   375
      Width           =   6705
      _Version        =   65536
      _ExtentX        =   11827
      _ExtentY        =   1535
      _StockProps     =   64
   End
   Begin TABLib.TAB TabJOB 
      Height          =   1215
      Left            =   90
      TabIndex        =   11
      Tag             =   "{=TABLE=}JOBTAB{=FIELDS=}URNO,UAFT,USTF,ACFR,ACTO,STAT,UALO,UAID,DETY,UJTY,TEXT,UDSR,JOUR,UTPL,PLFR,PLTO,UEQU,UPRM"
      Top             =   2595
      Width           =   6705
      _Version        =   65536
      _ExtentX        =   11827
      _ExtentY        =   2143
      _StockProps     =   64
   End
   Begin TABLib.TAB TabPoolJOB 
      Height          =   1620
      Left            =   120
      TabIndex        =   59
      Tag             =   "{=TABLE=}JOBTAB{=FIELDS=}URNO,UAFT,USTF,ACFR,ACTO,STAT,UALO,UAID,DETY,UJTY,TEXT,UDSR,JOUR"
      Top             =   4080
      Width           =   6705
      _Version        =   65536
      _ExtentX        =   11827
      _ExtentY        =   2857
      _StockProps     =   64
   End
   Begin TABLib.TAB TabDEM 
      Height          =   1950
      Left            =   6945
      TabIndex        =   0
      Tag             =   "{=TABLE=}DEMTAB{=FIELDS=}URNO,DETY,OURI,OURO,URUD,DETY"
      Top             =   3645
      Width           =   5460
      _Version        =   65536
      _ExtentX        =   9631
      _ExtentY        =   3440
      _StockProps     =   64
   End
   Begin TABLib.TAB TabALO 
      Height          =   690
      Left            =   6945
      TabIndex        =   1
      Tag             =   "{=TABLE=}ALOTAB{=FIELDS=}URNO,ALOC,ALOD,REFT,ALOT"
      Top             =   2610
      Width           =   5460
      _Version        =   65536
      _ExtentX        =   9631
      _ExtentY        =   1217
      _StockProps     =   64
   End
   Begin TABLib.TAB TabAFT 
      Height          =   1080
      Left            =   75
      TabIndex        =   2
      Tag             =   $"frmData2.frx":0000
      Top             =   6165
      Width           =   9105
      _Version        =   65536
      _ExtentX        =   16060
      _ExtentY        =   1905
      _StockProps     =   64
   End
   Begin TABLib.TAB TabTLX 
      Height          =   690
      Left            =   7035
      TabIndex        =   3
      Tag             =   "{=TABLE=}TLXTAB{=FIELDS=}URNO,FLNU,TXT1,TXT2,TTYP,TIME"
      Top             =   9855
      Width           =   2115
      _Version        =   65536
      _ExtentX        =   3731
      _ExtentY        =   1217
      _StockProps     =   64
   End
   Begin TABLib.TAB TabSPF 
      Height          =   690
      Left            =   7050
      TabIndex        =   4
      Tag             =   "{=TABLE=}SPFTAB{=FIELDS=}URNO,SURN,PRIO,VPFR,VPTO,CODE"
      Top             =   8790
      Width           =   2115
      _Version        =   65536
      _ExtentX        =   3731
      _ExtentY        =   1217
      _StockProps     =   64
   End
   Begin TABLib.TAB TabDRR 
      Height          =   630
      Left            =   9480
      TabIndex        =   5
      Tag             =   "{=TABLE=}DRRTAB{=FIELDS=}URNO,SCOD,SCOO,AVFR,AVTO,ROSS,ROSL,REMA,SDAY,FCTC,OABS"
      Top             =   7770
      Width           =   2055
      _Version        =   65536
      _ExtentX        =   3625
      _ExtentY        =   1111
      _StockProps     =   64
   End
   Begin TABLib.TAB TabConfig 
      Height          =   1800
      Left            =   6945
      TabIndex        =   6
      Top             =   360
      Width           =   5445
      _Version        =   65536
      _ExtentX        =   9604
      _ExtentY        =   3175
      _StockProps     =   64
   End
   Begin TABLib.TAB TabRPF 
      Height          =   690
      Left            =   4785
      TabIndex        =   7
      Tag             =   "{=TABLE=}RPFTAB{=FIELDS=}URNO,URUD,FCCO,UPFC"
      Top             =   7755
      Width           =   2115
      _Version        =   65536
      _ExtentX        =   3731
      _ExtentY        =   1217
      _StockProps     =   64
   End
   Begin TABLib.TAB TabBLT 
      Height          =   690
      Left            =   75
      TabIndex        =   8
      Tag             =   "{=TABLE=}BLTTAB{=FIELDS=}URNO,BNAM"
      Top             =   7725
      Width           =   2115
      _Version        =   65536
      _ExtentX        =   3731
      _ExtentY        =   1217
      _StockProps     =   64
   End
   Begin TABLib.TAB TabTemp 
      Height          =   630
      Left            =   11910
      TabIndex        =   9
      Top             =   7755
      Width           =   645
      _Version        =   65536
      _ExtentX        =   1138
      _ExtentY        =   1111
      _StockProps     =   64
   End
   Begin TABLib.TAB TabSOR 
      Height          =   690
      Left            =   75
      TabIndex        =   10
      Tag             =   "{=TABLE=}SORTAB{=FIELDS=}URNO,SURN,VPFR,VPTO,CODE"
      Top             =   1515
      Width           =   6705
      _Version        =   65536
      _ExtentX        =   11827
      _ExtentY        =   1217
      _StockProps     =   64
   End
   Begin TABLib.TAB TabCIC 
      Height          =   690
      Left            =   75
      TabIndex        =   12
      Tag             =   "{=TABLE=}CICTAB{=FIELDS=}URNO,CNAM"
      Top             =   8745
      Width           =   2115
      _Version        =   65536
      _ExtentX        =   3731
      _ExtentY        =   1217
      _StockProps     =   64
   End
   Begin TABLib.TAB TabEQU 
      Height          =   690
      Left            =   75
      TabIndex        =   13
      Tag             =   "{=TABLE=}EQUTAB{=FIELDS=}URNO,ENAM"
      Top             =   9810
      Width           =   2115
      _Version        =   65536
      _ExtentX        =   3731
      _ExtentY        =   1217
      _StockProps     =   64
   End
   Begin TABLib.TAB TabGAT 
      Height          =   690
      Left            =   2430
      TabIndex        =   14
      Tag             =   "{=TABLE=}GATTAB{=FIELDS=}URNO,GNAM"
      Top             =   7755
      Width           =   2115
      _Version        =   65536
      _ExtentX        =   3731
      _ExtentY        =   1217
      _StockProps     =   64
   End
   Begin TABLib.TAB TabPST 
      Height          =   690
      Left            =   2430
      TabIndex        =   15
      Tag             =   "{=TABLE=}PSTTAB{=FIELDS=}URNO,PNAM"
      Top             =   8760
      Width           =   2115
      _Version        =   65536
      _ExtentX        =   3731
      _ExtentY        =   1217
      _StockProps     =   64
   End
   Begin TABLib.TAB TabWRO 
      Height          =   690
      Left            =   2430
      TabIndex        =   16
      Tag             =   "{=TABLE=}WROTAB{=FIELDS=}URNO,WNAM"
      Top             =   9825
      Width           =   2115
      _Version        =   65536
      _ExtentX        =   3731
      _ExtentY        =   1217
      _StockProps     =   64
   End
   Begin TABLib.TAB TabEXT 
      Height          =   690
      Left            =   75
      TabIndex        =   17
      Tag             =   "{=TABLE=}EXTTAB{=FIELDS=}URNO,ENAM"
      Top             =   10860
      Width           =   2115
      _Version        =   65536
      _ExtentX        =   3731
      _ExtentY        =   1217
      _StockProps     =   64
   End
   Begin TABLib.TAB TabJOD 
      Height          =   690
      Left            =   2430
      TabIndex        =   18
      Tag             =   "{=TABLE=}JODTAB{=FIELDS=}URNO,UJOB,UDEM"
      Top             =   10890
      Width           =   2115
      _Version        =   65536
      _ExtentX        =   3731
      _ExtentY        =   1217
      _StockProps     =   64
   End
   Begin TABLib.TAB TabSGR 
      Height          =   690
      Left            =   4710
      TabIndex        =   19
      Tag             =   "{=TABLE=}SGRTAB{=FIELDS=}URNO,GRPN"
      Top             =   8745
      Width           =   2115
      _Version        =   65536
      _ExtentX        =   3731
      _ExtentY        =   1217
      _StockProps     =   64
   End
   Begin TABLib.TAB TabRUD 
      Height          =   690
      Left            =   4695
      TabIndex        =   20
      Tag             =   "{=TABLE=}RUDTAB{=FIELDS=}URNO,UGHS,VREF"
      Top             =   9840
      Width           =   2115
      _Version        =   65536
      _ExtentX        =   3731
      _ExtentY        =   1217
      _StockProps     =   64
   End
   Begin TABLib.TAB TabSER 
      Height          =   690
      Left            =   4695
      TabIndex        =   21
      Tag             =   "{=TABLE=}SERTAB{=FIELDS=}URNO,SNAM"
      Top             =   10920
      Width           =   2115
      _Version        =   65536
      _ExtentX        =   3731
      _ExtentY        =   1217
      _StockProps     =   64
   End
   Begin TABLib.TAB TabJTY 
      Height          =   690
      Left            =   7020
      TabIndex        =   22
      Tag             =   "{=TABLE=}JTYTAB{=FIELDS=}URNO,NAME"
      Top             =   7755
      Width           =   2115
      _Version        =   65536
      _ExtentX        =   3731
      _ExtentY        =   1217
      _StockProps     =   64
   End
   Begin TABLib.TAB TabCCC 
      Height          =   690
      Left            =   7020
      TabIndex        =   23
      Tag             =   "{=TABLE=}CCCTAB{=FIELDS=}URNO,CICC"
      Top             =   10950
      Width           =   2115
      _Version        =   65536
      _ExtentX        =   3731
      _ExtentY        =   1217
      _StockProps     =   64
   End
   Begin TABLib.TAB TabODA 
      Height          =   630
      Left            =   9480
      TabIndex        =   24
      Tag             =   "{=TABLE=}ODATAB{=FIELDS=}URNO,SDAC,FREE,TYPE"
      Top             =   8790
      Width           =   2055
      _Version        =   65536
      _ExtentX        =   3625
      _ExtentY        =   1111
      _StockProps     =   64
   End
   Begin TABLib.TAB TabDRG 
      Height          =   630
      Left            =   9510
      TabIndex        =   26
      Tag             =   "{=TABLE=}DRGTAB{=FIELDS=}URNO,SDAY,WGPC,STFU"
      Top             =   9945
      Width           =   2055
      _Version        =   65536
      _ExtentX        =   3625
      _ExtentY        =   1111
      _StockProps     =   64
   End
   Begin TABLib.TAB TabSWG 
      Height          =   630
      Left            =   9525
      TabIndex        =   27
      Tag             =   "{=TABLE=}SWGTAB{=FIELDS=}URNO,VPFR,VPTO,CODE,SURN"
      Top             =   10995
      Width           =   2055
      _Version        =   65536
      _ExtentX        =   3625
      _ExtentY        =   1111
      _StockProps     =   64
   End
   Begin TABLib.TAB TabREM 
      Height          =   630
      Left            =   9465
      TabIndex        =   28
      Tag             =   "{=TABLE=}REMTAB{=FIELDS=}URNO,RURN,TEXT"
      Top             =   6600
      Width           =   2055
      _Version        =   65536
      _ExtentX        =   3625
      _ExtentY        =   1111
      _StockProps     =   64
   End
   Begin TABLib.TAB TabFlightInfo 
      Height          =   630
      Left            =   11595
      TabIndex        =   29
      Tag             =   "{=TABLE=}ODATAB{=FIELDS=}URNO,SDAC,FREE,TYPE"
      Top             =   8775
      Width           =   2055
      _Version        =   65536
      _ExtentX        =   3625
      _ExtentY        =   1111
      _StockProps     =   64
   End
   Begin TABLib.TAB TabDLG 
      Height          =   630
      Left            =   11610
      TabIndex        =   61
      Tag             =   "{=TABLE=}DLGTAB{=FIELDS=}URNO,UJOB,SDAY,WGPC"
      Top             =   9975
      Width           =   2055
      _Version        =   65536
      _ExtentX        =   3625
      _ExtentY        =   1111
      _StockProps     =   64
   End
   Begin TABLib.TAB TabDPX 
      Height          =   630
      Left            =   11640
      TabIndex        =   63
      Tag             =   "{=TABLE=}DPXTAB{=FIELDS=}PRID,NAME,NATL,LANG,GEND,OWHC,PRMT,SEAT,ABDT,ABSR,ALOC,REMA,TFLU,URNO"
      Top             =   10995
      Width           =   2055
      _Version        =   65536
      _ExtentX        =   3625
      _ExtentY        =   1111
      _StockProps     =   64
   End
   Begin VB.Label Label32 
      Caption         =   "DPXTAB:"
      Height          =   210
      Left            =   11670
      TabIndex        =   64
      Top             =   10785
      Width           =   1140
   End
   Begin VB.Label Label31 
      Caption         =   "DLGTAB:"
      Height          =   210
      Left            =   11640
      TabIndex        =   62
      Top             =   9735
      Width           =   1140
   End
   Begin VB.Label Label30 
      Caption         =   "JOBTAB=>POOLJOBS:"
      Height          =   210
      Left            =   90
      TabIndex        =   60
      Top             =   3870
      Width           =   2235
   End
   Begin VB.Label Label1 
      Caption         =   "STFTAB:"
      Height          =   210
      Left            =   90
      TabIndex        =   58
      Top             =   135
      Width           =   2235
   End
   Begin VB.Label Label4 
      Caption         =   "Config-File:"
      Height          =   240
      Left            =   6915
      TabIndex        =   57
      Top             =   0
      Width           =   2580
   End
   Begin VB.Label Label2 
      Caption         =   "SORTAB:"
      Height          =   210
      Left            =   75
      TabIndex        =   56
      Top             =   1230
      Width           =   2235
   End
   Begin VB.Label Label3 
      Caption         =   "JOBTAB:"
      Height          =   210
      Left            =   75
      TabIndex        =   55
      Top             =   2355
      Width           =   2235
   End
   Begin VB.Label Label5 
      Caption         =   "AFTTAB:"
      Height          =   210
      Left            =   75
      TabIndex        =   54
      Top             =   5835
      Width           =   2235
   End
   Begin VB.Label Label6 
      Caption         =   "ALOTAB:"
      Height          =   210
      Left            =   6975
      TabIndex        =   53
      Top             =   2370
      Width           =   2235
   End
   Begin VB.Label Label7 
      Caption         =   "BLTTAB:"
      Height          =   210
      Left            =   75
      TabIndex        =   52
      Top             =   7485
      Width           =   2235
   End
   Begin VB.Label Label8 
      Caption         =   "CICTAB:"
      Height          =   210
      Left            =   0
      TabIndex        =   51
      Top             =   8505
      Width           =   2235
   End
   Begin VB.Label Label9 
      Caption         =   "EQUTAB:"
      Height          =   210
      Left            =   75
      TabIndex        =   50
      Top             =   9570
      Width           =   2235
   End
   Begin VB.Label Label10 
      Caption         =   "GATTAB:"
      Height          =   210
      Left            =   2430
      TabIndex        =   49
      Top             =   7560
      Width           =   2235
   End
   Begin VB.Label Label11 
      Caption         =   "PSTTAB:"
      Height          =   210
      Left            =   2430
      TabIndex        =   48
      Top             =   8550
      Width           =   2235
   End
   Begin VB.Label Label12 
      Caption         =   "WROTAB:"
      Height          =   210
      Left            =   2430
      TabIndex        =   47
      Top             =   9585
      Width           =   2235
   End
   Begin VB.Label Label13 
      Caption         =   "EXTTAB:"
      Height          =   210
      Left            =   75
      TabIndex        =   46
      Top             =   10620
      Width           =   2235
   End
   Begin VB.Label Label14 
      Caption         =   "DEMTAB:"
      Height          =   210
      Left            =   6945
      TabIndex        =   45
      Top             =   3420
      Width           =   2235
   End
   Begin VB.Label Label15 
      Caption         =   "JODTAB:"
      Height          =   210
      Left            =   2415
      TabIndex        =   44
      Top             =   10680
      Width           =   2235
   End
   Begin VB.Label Label16 
      Caption         =   "RPFTAB:"
      Height          =   210
      Left            =   4770
      TabIndex        =   43
      Top             =   7545
      Width           =   2235
   End
   Begin VB.Label Label18 
      Caption         =   "SGRTAB:"
      Height          =   210
      Left            =   4695
      TabIndex        =   42
      Top             =   8550
      Width           =   2235
   End
   Begin VB.Label Label17 
      Caption         =   "RUDTAB:"
      Height          =   210
      Left            =   4680
      TabIndex        =   41
      Top             =   9630
      Width           =   2235
   End
   Begin VB.Label Label19 
      Caption         =   "SERTAB:"
      Height          =   210
      Left            =   4680
      TabIndex        =   40
      Top             =   10710
      Width           =   2235
   End
   Begin VB.Label Label20 
      Caption         =   "JTYTAB:"
      Height          =   210
      Left            =   7005
      TabIndex        =   39
      Top             =   7545
      Width           =   2235
   End
   Begin VB.Label Label21 
      Caption         =   "SPFTAB:"
      Height          =   210
      Left            =   7020
      TabIndex        =   38
      Top             =   8595
      Width           =   2235
   End
   Begin VB.Label Label22 
      Caption         =   "TLXTAB:"
      Height          =   210
      Left            =   7005
      TabIndex        =   37
      Top             =   9660
      Width           =   2235
   End
   Begin VB.Label Label23 
      Caption         =   "CCCTAB:"
      Height          =   210
      Left            =   6945
      TabIndex        =   36
      Top             =   10710
      Width           =   2235
   End
   Begin VB.Label Label24 
      Caption         =   "DRRTAB:"
      Height          =   210
      Left            =   9465
      TabIndex        =   35
      Top             =   7470
      Width           =   2235
   End
   Begin VB.Label Label25 
      Caption         =   "ODATAB:"
      Height          =   210
      Left            =   9480
      TabIndex        =   34
      Top             =   8580
      Width           =   1140
   End
   Begin VB.Label Label26 
      Caption         =   "DRGTAB:"
      Height          =   210
      Left            =   9525
      TabIndex        =   33
      Top             =   9735
      Width           =   1140
   End
   Begin VB.Label Label27 
      Caption         =   "SWGTAB:"
      Height          =   210
      Left            =   9555
      TabIndex        =   32
      Top             =   10785
      Width           =   1140
   End
   Begin VB.Label Label28 
      Caption         =   "REMTAB:"
      Height          =   210
      Left            =   9450
      TabIndex        =   31
      Top             =   6300
      Width           =   2235
   End
   Begin VB.Label Label29 
      Caption         =   "tmpFlightInfo:"
      Height          =   210
      Left            =   11595
      TabIndex        =   30
      Top             =   8565
      Width           =   1140
   End
End
Attribute VB_Name = "frmData2"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public strFieldSeparatorTabDatabase As String
Public strFieldSeparatorTabConfig As String
Public strServer As String
Public strHOPO As String
Public strTableExt As String
Public strConfigFile As String
Public strAccessMethod As String
Public strOrgUnit As String
Public strFunction As String
Public strStfUrno As String

Public strFLT As String
Public strGAT As String
Public strBRK As String
Public strSPE As String
Public strCCC As String
Public strCCI As String
Public strFID As String

Private Sub Form_Load()
    ' set the field separators
    strFieldSeparatorTabDatabase = ","
    strFieldSeparatorTabConfig = "="

    ' read entries from ceda.ini
    strAccessMethod = "CEDA"
    strHOPO = GetIniEntry("", gsAppName, "GLOBAL", "HOMEAIRPORT", "XXX")
    strServer = GetIniEntry("", gsAppName, "GLOBAL", "HOSTNAME", "XXX")
    strTableExt = GetIniEntry("", gsAppName, "GLOBAL", "TABLEEXTENSION", "TAB")
    strConfigFile = GetIniEntry("", gsAppName, "GLOBAL", "CONFIG_FILE", "XXX")

    ' read the config file
    If ExistFile(strConfigFile) = False Then
        MsgBox "The config-file for the application " & gsAppName & " does not exist!" & vbCrLf & _
                "Please check the setting in ceda.ini:" & vbCrLf & _
                "[" & gsAppName & "]" & vbCrLf & _
                "..." & vbCrLf & _
                "CONFIG_FILE=..." & vbCrLf & vbCrLf & _
                "The application terminates!", vbCritical, "No configuration file"
        mdlMain.ExitApplication
    Else
        ReadConfigFile
    End If

    ' get the UTC-difference
    sglUTCOffsetHours = GetUTCOffset
End Sub

Public Function InitialLoad(bpHandleSplashScreen As Boolean, bpLoadBasicdata As Boolean, strAftUrnos As String, strTplUrno As String) As Boolean
    Dim blRet As Boolean
    Dim strWhere As String
    Dim strToday As String
    Dim strFrom As String
    Dim strTo As String
    Dim strCfg As String
    Dim dateFrom As Date
    Dim dateTo As Date
    Dim dateTo2 As Date
    Dim strTmp As String
    Dim ilIdx As Integer
    Dim strStfUrnos As String
    Dim strJobUrnos As String
    Dim tmpJobUrnos As String
    Dim strPreparedAftUrnos As String
    Dim i As Long
    Dim cnt As Long

    If bpLoadBasicdata = True Then
        InitTabs
    End If

    blRet = True
    
    '-------------
    'Date: 26 May 2009
    'Desc: revise Where Clause
    'Modi: igu
    '-------------
    Dim llCounter As Long
    Dim strAftUrnoArr As Variant
    Dim strTplUrNoArr As Variant
    Dim strAftUrnoItem As String
    Dim strTplUrnoItem As String
    
    strAftUrnoArr = Split(strAftUrnos, ";")
    strTplUrNoArr = Split(strTplUrno, ";")
    strWhere = ""
    For llCounter = 0 To UBound(strAftUrnoArr)
        strAftUrnoItem = strAftUrnoArr(llCounter)
        strTplUrnoItem = strTplUrNoArr(llCounter)
        If strAftUrnoItem <> "" Then
            cnt = ItemCount(strAftUrnoItem, ",")
            strPreparedAftUrnos = ""
            For i = 0 To cnt - 1
                strPreparedAftUrnos = strPreparedAftUrnos & "'" & _
                    GetRealItem(strAftUrnoItem, i, ",") + "'"
                If i + 1 < cnt Then strPreparedAftUrnos = strPreparedAftUrnos & ","
            Next i
            
            If strWhere = "" Then
                strWhere = strWhere & "WHERE "
            Else
                strWhere = strWhere & " OR "
            End If
            strWhere = strWhere & "(UAFT IN (" & strPreparedAftUrnos & ") AND " & _
                "UTPL = '" + strTplUrnoItem + "')"
        End If
    Next llCounter
    '-------------
    
    ' init the splash screen
    If bpHandleSplashScreen = True Then
        frmSplash.Visible = True
        frmSplash.List1.Clear
        frmSplash.ProgressBar1.Max = 30
        frmSplash.List1.AddItem "Loading data..."
        frmSplash.Refresh
    Else
        frmMain.txtReloading.Text = " Please wait while reloading user data..."
    End If

    ' loading JOBTAB data
    HandleSplashScreen "Loading JOBTAB (flight jobs)...", bpHandleSplashScreen
    If (GetConfigEntry("JOB_LOAD_TIMEFRAME", strOrgUnit, strCfg) = False) Then
        MsgBoxNoCfgEntry "JOB_LOAD_TIMEFRAME"
    End If
    strFrom = GetItem(strCfg, 1, ",")
    strTo = GetItem(strCfg, 2, ",")
    dateFrom = DateAdd("h", CDbl(strFrom), Now) - (sglUTCOffsetHours / 24)
    dateTo = DateAdd("h", CDbl(strTo), Now) - (sglUTCOffsetHours / 24)
    If (GetConfigEntry("JOB_TYPE_UJTY", strOrgUnit, strCfg) = False) Then
        MsgBoxNoCfgEntry "JOB_TYPE_UJTY"
    End If
    'strWhere = "WHERE UAFT IN (" & strPreparedAftUrnos & ") AND UTPL='" + strTplUrno + "'" <---> igu on 26 May 2009
    LoadData TabJOB, strWhere
    ilIdx = GetRealItemNo(TabJOB.LogicalFieldList, "JOUR")
    TabJOB.IndexCreate "JOUR", ilIdx
    ilIdx = GetRealItemNo(TabJOB.LogicalFieldList, "URNO")
    TabJOB.IndexCreate "URNO", ilIdx
    'kkh on 20/11/2008 refresh TabJOB base on the Flight URNO selected
    TabJOB.IndexCreate "UEQU", ilIdx
    
    'loading PoolJobs
    strStfUrnos = TabJOB.SelectDistinct("2", "'", "'", ",", True)
    If strStfUrnos <> "" Then
        dateTo2 = DateAdd("h", 48, dateTo)
        strWhere = "WHERE USTF IN (" & strStfUrnos & ") AND (ACTO BETWEEN '" & _
            Format(dateFrom, "YYYYMMDDhhmmss") & "' AND '" & Format(dateTo2, "YYYYMMDDhhmmss") & _
            "') AND (ACFR <= '" & Format(dateTo, "YYYYMMDDhhmmss") & "' AND ACFR >= '" & Format(dateFrom, "YYYYMMDDhhmmss") & "') AND UJTY='2007'"
        LoadData TabPoolJOB, strWhere
        'TabJOB.IndexCreate "URNO", 0 'no index here - created after the filtering in "FilterJobs"
        ilIdx = GetItemNo(TabPoolJOB.LogicalFieldList, "USTF") - 1
        TabPoolJOB.IndexCreate "USTF", ilIdx
        ilIdx = GetItemNo(TabPoolJOB.LogicalFieldList, "JOUR") - 1
        TabPoolJOB.IndexCreate "JOUR", ilIdx
        ilIdx = GetItemNo(TabPoolJOB.LogicalFieldList, "URNO") - 1
        TabPoolJOB.IndexCreate "URNO", ilIdx
        
        'loading DLGTAB
        strJobUrnos = TabPoolJOB.SelectDistinct("0", "'", "'", ",", True)
    '    strWhere = "WHERE SDAY ='" & Format(Now, "YYYYMMDD") & "' AND UJOB IN (" & strJobUrnos & ")"
        If Len(strJobUrnos) > 0 Then 'igu on 05 Jan 2010
            strWhere = "WHERE UJOB IN (" & strJobUrnos & ")"
            LoadData TabDLG, strWhere
        End If 'igu on 05 Jan 2010
        ilIdx = GetRealItemNo(TabDLG.LogicalFieldList, "UJOB")
        TabDLG.IndexCreate "UJOB", CStr(ilIdx)
        
        ' loading STFTAB data
        HandleSplashScreen "Loading STFTAB (employee data)...", bpHandleSplashScreen
    '    strWhere = "WHERE PENO = '" & gsUserName & "'"
        strWhere = "WHERE URNO IN (" & strStfUrnos & ")"
        LoadData TabSTF, strWhere
        If TabSTF.GetLineCount < 1 Then
            MsgBox "The employee with the personnel number '" & gsUserName & "' does not exist!" & vbCrLf & _
                    "The application terminates!", vbCritical, "Employee not found"
            mdlMain.ExitApplication
        End If
        ilIdx = GetItemNo(TabSTF.LogicalFieldList, "URNO") - 1
        TabSTF.IndexCreate "URNO", ilIdx
        strStfUrno = TabSTF.GetFieldValue(0, "URNO")
    
        ' loading SORTAB data
        HandleSplashScreen "Loading SORTAB (organizational assignments)...", bpHandleSplashScreen
        strWhere = "WHERE SURN IN (" & strStfUrnos & ")"
        'strWhere = "WHERE SURN = '" & strStfUrno & "'"
        LoadData TabSOR, strWhere
        strToday = Format(Now, "YYYYMMDD")
        strOrgUnit = GetBestFitRecord(TabSOR, strStfUrno, "SURN", "CODE", strToday)
    
        ' loading SPFTAB data
        HandleSplashScreen "Loading SPFTAB (connection between staff and function)...", bpHandleSplashScreen
    '    strWhere = "WHERE SURN = '" & strStfUrno & "' AND PRIO = '1'"
        strWhere = "WHERE SURN IN (" & strStfUrnos & ") AND PRIO = '1'"
        LoadData TabSPF, strWhere
        strToday = Format(Now, "YYYYMMDD")
        strFunction = GetBestFitRecord(TabSPF, strStfUrno, "SURN", "CODE", strToday)
    
        ' loading JTYTAB data
        HandleSplashScreen "Loading JTYTAB (job types)...", bpHandleSplashScreen
        strWhere = ""
        If bpLoadBasicdata = True Then
            LoadData TabJTY, strWhere
            ilIdx = GetItemNo(TabJTY.LogicalFieldList, "NAME") - 1
            TabJTY.IndexCreate "NAME", ilIdx
            TabJTY.IndexCreate "URNO", 0
            InitJTY
        End If
    
    
        ' loading JODTAB data
        HandleSplashScreen "Loading JODTAB (connection between jobs and demands)...", bpHandleSplashScreen
        strTmp = TabJOB.SelectDistinct(0, "'", "'", ",", True)
        If Len(strTmp) > 0 Then
            strWhere = "WHERE UJOB IN (" & strTmp & ")"
            LoadData TabJOD, strWhere
        End If
        ilIdx = GetItemNo(TabJOD.LogicalFieldList, "UJOB") - 1
        TabJOD.IndexCreate "UJOB", ilIdx
    
        ' loading DEMTAB data
        HandleSplashScreen "Loading DEMTAB (demands)...", bpHandleSplashScreen
        ilIdx = GetItemNo(TabJOD.LogicalFieldList, "UDEM") - 1
        strTmp = TabJOD.SelectDistinct(CStr(ilIdx), "'", "'", ",", True)
        If Len(strTmp) > 0 Then
            strWhere = "WHERE URNO IN (" & strTmp & ")"
            LoadData TabDEM, strWhere
        End If
        TabDEM.IndexCreate "URNO", 0
    
        ' loading AFTTAB data
        HandleSplashScreen "Loading AFTTAB (flights)...", bpHandleSplashScreen
        TabTemp.ResetContent
        ilIdx = GetItemNo(TabJOB.LogicalFieldList, "UAFT") - 1
        strTmp = TabJOB.SelectDistinct(CStr(ilIdx), "", "", ",", True)
        ilIdx = GetItemNo(TabDEM.LogicalFieldList, "OURI") - 1
        strTmp = strTmp & "," & TabDEM.SelectDistinct(CStr(ilIdx), "", "", ",", True)
        ilIdx = GetItemNo(TabDEM.LogicalFieldList, "OURO") - 1
        strTmp = strTmp & "," & TabDEM.SelectDistinct(CStr(ilIdx), "", "", ",", True)
        TabTemp.InsertBuffer strTmp, ","
        strTmp = TabTemp.SelectDistinct("0", "", "", ",", True)
        If Len(strTmp) > 0 Then
            While (InStr(strTmp, ",,") > 0)
                strTmp = Replace(strTmp, ",,", ",")
            Wend
            While (Left(strTmp, 1) = ",")
                strTmp = Right(strTmp, Len(strTmp) - 1)
            Wend
            While (Right(strTmp, 1) = ",")
                strTmp = Left(strTmp, Len(strTmp) - 1)
            Wend
            strWhere = "WHERE URNO IN (" & strTmp & ")"
            LoadData TabAFT, strWhere
            TabAFT.IndexCreate "URNO", 0
            TabAFT.IndexCreate "RKEY", 1
        End If
    
        ' loading REMTAB data
        If Len(strTmp) > 0 Then
            HandleSplashScreen "Loading REMTAB (remarks)...", bpHandleSplashScreen
            strWhere = "WHERE RURN IN (" & strTmp & ") AND APPL='OPSS-PM'"
            LoadData TabREM, strWhere
            ilIdx = GetItemNo(TabREM.LogicalFieldList, "RURN") - 1
            TabREM.IndexCreate "RURN", ilIdx
        End If
    
        ' loading ALOTAB data
        HandleSplashScreen "Loading ALOTAB (allocation types)...", bpHandleSplashScreen
        strWhere = "WHERE ALOT = '0'"
        If bpLoadBasicdata = True Then
            LoadData TabALO, strWhere
            TabALO.IndexCreate "URNO", 0
        End If
    
        ' loading BLTTAB data
        HandleSplashScreen "Loading BLTTAB (baggage belts)...", bpHandleSplashScreen
        strWhere = ""
        If bpLoadBasicdata = True Then
            LoadData TabBLT, strWhere
            TabBLT.IndexCreate "URNO", 0
        End If
    
        ' loading CICTAB data
        HandleSplashScreen "Loading CICTAB (checkin counters)...", bpHandleSplashScreen
        strWhere = ""
        If bpLoadBasicdata = True Then
            LoadData TabCIC, strWhere
            TabCIC.IndexCreate "URNO", 0
        End If
    
        ' loading CCCTAB data
        HandleSplashScreen "Loading CCCTAB (common checkin counters)...", bpHandleSplashScreen
        strWhere = ""
        If bpLoadBasicdata = True Then
            LoadData TabCCC, strWhere
            TabCCC.IndexCreate "URNO", 0
        End If
    
        ' loading EQUTAB data
        HandleSplashScreen "Loading EQUTAB (equipments)...", bpHandleSplashScreen
        strWhere = ""
        If bpLoadBasicdata = True Then
            LoadData TabEQU, strWhere
            TabEQU.IndexCreate "URNO", 0
        End If
    
        ' loading EXTTAB data
        HandleSplashScreen "Loading EXTTAB (exits)...", bpHandleSplashScreen
        strWhere = ""
        If bpLoadBasicdata = True Then
            LoadData TabEXT, strWhere
            TabEXT.IndexCreate "URNO", 0
        End If
    
        ' loading GATTAB data
        HandleSplashScreen "Loading GATTAB (gates)...", bpHandleSplashScreen
        strWhere = ""
        If bpLoadBasicdata = True Then
            LoadData TabGAT, strWhere
            TabGAT.IndexCreate "URNO", 0
        End If
    
        ' loading PSTTAB data
        HandleSplashScreen "Loading PSTTAB (parking stands)...", bpHandleSplashScreen
        strWhere = ""
        If bpLoadBasicdata = True Then
            LoadData TabPST, strWhere
            TabPST.IndexCreate "URNO", 0
        End If
    
        ' loading WROTAB data
        HandleSplashScreen "Loading WROTAB (waiting rooms)...", bpHandleSplashScreen
        strWhere = ""
        If bpLoadBasicdata = True Then
            LoadData TabWRO, strWhere
            TabWRO.IndexCreate "URNO", 0
        End If
    
        ' loading RPFTAB data
        HandleSplashScreen "Loading RPFTAB (connection between demands and functions)...", bpHandleSplashScreen
        ilIdx = GetItemNo(TabDEM.LogicalFieldList, "URUD") - 1
        strTmp = TabDEM.SelectDistinct(CStr(ilIdx), "'", "'", ",", True)
        strWhere = ""
        If Len(strTmp) > 0 Then
            strWhere = "WHERE URUD IN (" & strTmp & ")"
            LoadData TabRPF, strWhere
        End If
        ilIdx = GetItemNo(TabRPF.LogicalFieldList, "URUD") - 1
        TabRPF.IndexCreate "URUD", ilIdx
    
        ' loading RUDTAB data
        HandleSplashScreen "Loading RUDTAB (Rule demands)...", bpHandleSplashScreen
        If Len(strTmp) > 0 Then
            strWhere = "WHERE URNO IN (" & strTmp & ")"
            LoadData TabRUD, strWhere
        End If
        TabRUD.IndexCreate "URNO", 0
    
        ' loading SERTAB data
        HandleSplashScreen "Loading SERTAB (Ground handling services)...", bpHandleSplashScreen
        ilIdx = GetItemNo(TabRUD.LogicalFieldList, "UGHS") - 1
        strTmp = TabRUD.SelectDistinct(CStr(ilIdx), "'", "'", ",", True)
        If Len(strTmp) > 0 Then
            strWhere = "WHERE URNO IN (" & strTmp & ")"
            LoadData TabSER, strWhere
        End If
        TabSER.IndexCreate "URNO", 0
    
        ' loading SGRTAB data
        HandleSplashScreen "Loading SGRTAB (static groups)...", bpHandleSplashScreen
        strWhere = "WHERE TABN = 'PFC'"
        If bpLoadBasicdata = True Then
            LoadData TabSGR, strWhere
            TabSGR.IndexCreate "URNO", 0
        End If
    
        ' loading DRRTAB data
        HandleSplashScreen "Loading DRRTAB (daily roster records)...", bpHandleSplashScreen
        If (GetConfigEntry("SHIFT_TIMEFRAME", strOrgUnit, strCfg) = False) Then
            MsgBoxNoCfgEntry "SHIFT_TIMEFRAME"
        End If
        strFrom = GetItem(strCfg, 1, ",")
        strTo = GetItem(strCfg, 2, ",")
        dateFrom = DateAdd("d", CDbl(strFrom), Now) ' DRRTAB.SDAY is local
        dateTo = DateAdd("d", CDbl(strTo), Now)
        strFrom = Format(dateFrom, "YYYYMMDD")
        strTo = Format(dateTo, "YYYYMMDD")
        strWhere = "WHERE STFU = '" & strStfUrno & "' AND SDAY between '" & strFrom & "' AND '" & strTo & "' ORDER BY SDAY,ROSL"
        LoadData TabDRR, strWhere
        TabDRR.IndexCreate "URNO", 0
    
        ' loading DRGTAB data
        HandleSplashScreen "Loading DRGTAB (Working group assignments daily)...", bpHandleSplashScreen
        strWhere = "WHERE STFU = '" & strStfUrno & "' AND SDAY between '" & strFrom & "' AND '" & strTo & "'"
        LoadData TabDRG, strWhere 'same where-statement as for DRRTAB
        ilIdx = GetItemNo(TabDRG.LogicalFieldList, "SDAY") - 1
        TabDRG.IndexCreate "SDAY", ilIdx
        ilIdx = GetItemNo(TabDRG.LogicalFieldList, "STFU") - 1
        TabDRG.IndexCreate "STFU", ilIdx
    
        ' loading SWGTAB data
        HandleSplashScreen "Loading SWGTAB (Working group assignments)...", bpHandleSplashScreen
        strWhere = "WHERE SURN IN (" & strStfUrnos & ")"
        LoadData TabSWG, strWhere
        ilIdx = GetItemNo(TabSWG.LogicalFieldList, "SURN") - 1
        TabSWG.IndexCreate "SURN", CStr(ilIdx)
        ilIdx = GetItemNo(TabSWG.LogicalFieldList, "VPFR") - 1
        TabSWG.Sort CStr(ilIdx), True, True
    
        ' loading ODATAB data
        HandleSplashScreen "Loading ODATAB (absences)...", bpHandleSplashScreen
        strWhere = ""
        If bpLoadBasicdata = True Then
            LoadData TabODA, strWhere
            ilIdx = GetItemNo(TabODA.LogicalFieldList, "SDAC") - 1
            TabODA.IndexCreate "SDAC", ilIdx
        End If
    Else
        blRet = False
    End If
    ' that we see all columns
    ResizeTabs

    frmSplash.Visible = False
    frmSplash.Hide
    Unload frmSplash
    InitialLoad = blRet
End Function

Private Sub InitTabs()
    InitTabGeneral TabSTF
    InitTabForCedaConnection TabSTF
    InitTabGeneral TabSOR
    InitTabForCedaConnection TabSOR
    InitTabGeneral TabSPF
    InitTabForCedaConnection TabSPF
    InitTabGeneral TabJOB
    InitTabForCedaConnection TabJTY
    InitTabGeneral TabJTY
    InitTabForCedaConnection TabJOB
    InitTabGeneral TabJOD
    InitTabForCedaConnection TabJOD
    InitTabGeneral TabPoolJOB
    InitTabForCedaConnection TabPoolJOB
    InitTabGeneral TabDEM
    InitTabForCedaConnection TabDEM
    InitTabGeneral TabAFT
    InitTabForCedaConnection TabAFT
    InitTabGeneral TabREM
    InitTabForCedaConnection TabREM
    InitTabGeneral TabALO
    InitTabForCedaConnection TabALO
    InitTabGeneral TabBLT
    InitTabForCedaConnection TabBLT
    InitTabGeneral TabCIC
    InitTabForCedaConnection TabCIC
    InitTabGeneral TabCCC
    InitTabForCedaConnection TabCCC
    InitTabGeneral TabEQU
    InitTabForCedaConnection TabEQU
    InitTabGeneral TabEXT
    InitTabForCedaConnection TabEXT
    InitTabGeneral TabGAT
    InitTabForCedaConnection TabGAT
    InitTabGeneral TabPST
    InitTabForCedaConnection TabPST
    InitTabGeneral TabWRO
    InitTabForCedaConnection TabWRO
    InitTabGeneral TabRPF
    InitTabForCedaConnection TabRPF
    InitTabGeneral TabRUD
    InitTabForCedaConnection TabRUD
    InitTabGeneral TabSER
    InitTabForCedaConnection TabSER
    InitTabGeneral TabSGR
    InitTabForCedaConnection TabSGR
    InitTabGeneral TabDRR
    InitTabForCedaConnection TabDRR
    InitTabGeneral TabDRG
    InitTabForCedaConnection TabDRG
    InitTabGeneral TabSWG
    InitTabForCedaConnection TabSWG
    InitTabGeneral TabODA
    InitTabForCedaConnection TabODA
    InitTabGeneral TabTLX   'no loading at startup
    InitTabForCedaConnection TabTLX
    InitTabGeneral TabDLG
    InitTabForCedaConnection TabDLG
    '-------------
    'Date: 23 June 2009
    'Desc: Init TabDPX
    'Modi: igu
    '-------------
    InitTabGeneral TabDPX
    InitTabForCedaConnection TabDPX
End Sub

Private Sub ResizeTabs()
    TabSTF.AutoSizeColumns
    TabSOR.AutoSizeColumns
    TabSPF.AutoSizeColumns
    TabJTY.AutoSizeColumns
    TabJOB.AutoSizeColumns
    TabPoolJOB.AutoSizeColumns
    TabDLG.AutoSizeColumns
    TabJOD.AutoSizeColumns
    TabDEM.AutoSizeColumns
    TabAFT.AutoSizeColumns
    TabREM.AutoSizeColumns
    TabALO.AutoSizeColumns
    TabBLT.AutoSizeColumns
    TabCIC.AutoSizeColumns
    TabCCC.AutoSizeColumns
    TabEQU.AutoSizeColumns
    TabEXT.AutoSizeColumns
    TabGAT.AutoSizeColumns
    TabPST.AutoSizeColumns
    TabWRO.AutoSizeColumns
    TabRPF.AutoSizeColumns
    TabRUD.AutoSizeColumns
    TabSER.AutoSizeColumns
    TabSGR.AutoSizeColumns
    TabDRR.AutoSizeColumns
    TabDRG.AutoSizeColumns
    TabSWG.AutoSizeColumns
    TabODA.AutoSizeColumns
    '-------------
    'Date: 23 June 2009
    'Desc: Autosize TabDPX
    'Modi: igu
    '-------------
    TabDPX.AutoSizeColumns
End Sub

Private Sub HandleSplashScreen(sMessage As String, bpHandleSplashScreen As Boolean)
    If bpHandleSplashScreen = True Then
        If frmSplash.ProgressBar1.Value >= 30 Then
            frmSplash.ProgressBar1.Value = 0
        End If
        frmSplash.List1.AddItem (sMessage)
        frmSplash.ProgressBar1.Value = frmSplash.ProgressBar1.Value + 1
        frmSplash.List1.TopIndex = frmSplash.List1.ListCount - 1
        frmSplash.List1.Refresh
    Else
        frmMain.txtReloading.Text = frmMain.txtReloading.Text & "."
        frmMain.txtReloading.Refresh
    End If
End Sub

Public Sub MsgBoxNoCfgEntry(sParameter As String)
    MsgBox "The parameter '" & sParameter & "' does not exist in the config file!" & vbCrLf & _
            "The application terminates!", vbCritical, "Parameter not found"
    mdlMain.ExitApplication
End Sub

Private Sub InitTabGeneral(ByRef rTab As TABLib.Tab)
    rTab.ResetContent
    rTab.FontName = "Courier New"
    rTab.HeaderFontSize = "18"
    rTab.FontSize = "16"
    rTab.EnableHeaderSizing True
    rTab.ShowHorzScroller True
    rTab.AutoSizeByHeader = True
    rTab.SetFieldSeparator strFieldSeparatorTabDatabase

    Dim ilCnt As Integer
    Dim i As Integer
    Dim strFields As String
    Dim strHeaderLengthString As String
    GetKeyItem strFields, rTab.Tag, "{=FIELDS=}", "{="
    ilCnt = ItemCount(strFields, ",")
    For i = 1 To ilCnt Step 1
        strHeaderLengthString = strHeaderLengthString & "10,"
    Next i
    strHeaderLengthString = Left(strHeaderLengthString, Len(strHeaderLengthString) - 1)
    rTab.HeaderLengthString = strHeaderLengthString

    strFields = Replace(strFields, ",", strFieldSeparatorTabDatabase)
    rTab.HeaderString = strFields
    rTab.LogicalFieldList = strFields
End Sub

Private Sub InitTabForCedaConnection(ByRef rTab As TABLib.Tab)
    'do the initializing for the CEDA-connection
    rTab.CedaServerName = strServer
    rTab.CedaPort = "3357"
    rTab.CedaHopo = strHOPO
    rTab.CedaCurrentApplication = gsAppName
    rTab.CedaTabext = strTableExt
    rTab.CedaUser = gsUserName
    rTab.CedaWorkstation = frmSplash.AATLoginControl1.GetWorkStationName
    rTab.CedaSendTimeout = "120"
    rTab.CedaReceiveTimeout = "240"
    rTab.CedaRecordSeparator = Chr(10)
    rTab.CedaIdentifier = gsAppName
End Sub

Public Sub LoadData(ByRef rTab As TABLib.Tab, ByRef pWhere As String, Optional bReset As Boolean = True)
    Dim strCommand As String
    Dim strTable As String
    Dim strFieldList As String
    Dim strData As String
    Dim strWhere As String

    strCommand = "RT"
    If bReset = True Then
        rTab.ResetContent
    End If

    GetKeyItem strTable, rTab.Tag, "{=TABLE=}", "{="
    GetKeyItem strFieldList, rTab.Tag, "{=FIELDS=}", "{="
    strData = ""
    strWhere = pWhere
    If Len(strWhere) = 0 Then
        GetKeyItem strWhere, rTab.Tag, "{=WHERE=}", "{="
    End If
    If rTab.CedaAction(strCommand, strTable, strFieldList, strData, strWhere) = False Then
        MsgBox "Error loading " & strTable & "!" & vbCrLf & _
            "Field list: " & strFieldList & vbCrLf & _
            "Where statement: " & strWhere, vbCritical, "Error reading data (RT)"
            rTab.GetLastCedaError
    End If
End Sub

Public Function LoadPRMInfo(ByVal bpHandleSplashScreen As Boolean, ByVal bpLoadBasicdata As Boolean, _
    ByVal strJobUrno As String, ByVal strAftUrnos As String, ByVal strTplUrno As String) As Boolean 'igu on 14 Jun 2010
'Public Function LoadPRMInfo(bpHandleSplashScreen As Boolean, bpLoadBasicdata As Boolean, strAftUrnos As String, strTplUrno As String) As Boolean 'igu on 14 Jun 2010
    '-------------
    'Date: 23 June 2009
    'Desc: Retrieve PRM Info
    'Crea: igu
    '-------------
    Dim blRet As Boolean
    Dim strWhere As String
    Dim strToday As String
    Dim strFrom As String
    Dim strTo As String
    Dim strCfg As String
    Dim dateFrom As Date
    Dim dateTo As Date
    Dim dateTo2 As Date
    Dim strTmp As String
    Dim ilIdx As Integer
    Dim strStfUrnos As String
    Dim strJobUrnos As String
    Dim tmpJobUrnos As String
    Dim strPreparedAftUrnos As String
    Dim i As Long
    Dim cnt As Long
    Dim strDPX_UrNo As String

    If bpLoadBasicdata = True Then
        InitTabs
    End If

    blRet = True
    
    If strAftUrnos <> "" Then
        cnt = ItemCount(strAftUrnos, ",")
        strPreparedAftUrnos = ""
        For i = 0 To cnt - 1
            strPreparedAftUrnos = strPreparedAftUrnos & "'" & _
                GetRealItem(strAftUrnos, i, ",") + "'"
            If i + 1 < cnt Then strPreparedAftUrnos = strPreparedAftUrnos & ","
        Next i
        
        strWhere = "WHERE (UAFT IN (" & strPreparedAftUrnos & ") AND " & _
            "UTPL = '" & strTplUrno & "')"
            
        strWhere = strWhere & " AND (URNO = '" & strJobUrno & "')" 'igu on 14 Jun 2010
    End If
    
    ' init the splash screen
    If bpHandleSplashScreen = True Then
        frmSplash.Visible = True
        frmSplash.List1.Clear
        frmSplash.ProgressBar1.Max = 30
        frmSplash.List1.AddItem "Loading data..."
        frmSplash.Refresh
    Else
        frmMain.txtReloading.Text = " Please wait while reloading user data..."
    End If

    ' loading JOBTAB data
    HandleSplashScreen "Loading JOBTAB (flight jobs)...", bpHandleSplashScreen
    If (GetConfigEntry("JOB_LOAD_TIMEFRAME", strOrgUnit, strCfg) = False) Then
        MsgBoxNoCfgEntry "JOB_LOAD_TIMEFRAME"
    End If
    strFrom = GetItem(strCfg, 1, ",")
    strTo = GetItem(strCfg, 2, ",")
    dateFrom = DateAdd("h", CDbl(strFrom), Now) - (sglUTCOffsetHours / 24)
    dateTo = DateAdd("h", CDbl(strTo), Now) - (sglUTCOffsetHours / 24)
    If (GetConfigEntry("JOB_TYPE_UJTY", strOrgUnit, strCfg) = False) Then
        MsgBoxNoCfgEntry "JOB_TYPE_UJTY"
    End If
    'strWhere = "WHERE UAFT IN (" & strPreparedAftUrnos & ") AND UTPL='" + strTplUrno + "'" <---> igu on 26 May 2009
    LoadData TabJOB, strWhere
    ilIdx = GetRealItemNo(TabJOB.LogicalFieldList, "JOUR")
    TabJOB.IndexCreate "JOUR", ilIdx
    ilIdx = GetRealItemNo(TabJOB.LogicalFieldList, "URNO")
    TabJOB.IndexCreate "URNO", ilIdx
    'kkh on 20/11/2008 refresh TabJOB base on the Flight URNO selected
    TabJOB.IndexCreate "UEQU", ilIdx
    
    'loading PoolJobs
    strStfUrnos = TabJOB.SelectDistinct("2", "'", "'", ",", True)
    If strStfUrnos <> "" Then
'        dateTo2 = DateAdd("h", 48, dateTo)
'        strWhere = "WHERE USTF IN (" & strStfUrnos & ") AND (ACTO BETWEEN '" & _
'            Format(dateFrom, "YYYYMMDDhhmmss") & "' AND '" & Format(dateTo2, "YYYYMMDDhhmmss") & _
'            "') AND (ACFR <= '" & Format(dateTo, "YYYYMMDDhhmmss") & "' AND ACFR >= '" & Format(dateFrom, "YYYYMMDDhhmmss") & "') AND UJTY='2007'"
'        LoadData TabPoolJOB, strWhere
'        'TabJOB.IndexCreate "URNO", 0 'no index here - created after the filtering in "FilterJobs"
'        ilIdx = GetItemNo(TabPoolJOB.LogicalFieldList, "USTF") - 1
'        TabPoolJOB.IndexCreate "USTF", ilIdx
'        ilIdx = GetItemNo(TabPoolJOB.LogicalFieldList, "JOUR") - 1
'        TabPoolJOB.IndexCreate "JOUR", ilIdx
'        ilIdx = GetItemNo(TabPoolJOB.LogicalFieldList, "URNO") - 1
'        TabPoolJOB.IndexCreate "URNO", ilIdx
'
'        'loading DLGTAB
'        strJobUrnos = TabPoolJOB.SelectDistinct("0", "'", "'", ",", True)
'    '    strWhere = "WHERE SDAY ='" & Format(Now, "YYYYMMDD") & "' AND UJOB IN (" & strJobUrnos & ")"
'        strWhere = "WHERE UJOB IN (" & strJobUrnos & ")"
'        LoadData TabDLG, strWhere
'        ilIdx = GetRealItemNo(TabDLG.LogicalFieldList, "UJOB")
'        TabDLG.IndexCreate "UJOB", CStr(ilIdx)
'
'        ' loading STFTAB data
'        HandleSplashScreen "Loading STFTAB (employee data)...", bpHandleSplashScreen
'    '    strWhere = "WHERE PENO = '" & gsUserName & "'"
'        strWhere = "WHERE URNO IN (" & strStfUrnos & ")"
'        LoadData TabSTF, strWhere
'        If TabSTF.GetLineCount < 1 Then
'            MsgBox "The employee with the personnel number '" & gsUserName & "' does not exist!" & vbCrLf & _
'                    "The application terminates!", vbCritical, "Employee not found"
'            mdlMain.ExitApplication
'        End If
'        ilIdx = GetItemNo(TabSTF.LogicalFieldList, "URNO") - 1
'        TabSTF.IndexCreate "URNO", ilIdx
'        strStfUrno = TabSTF.GetFieldValue(0, "URNO")
'
'        ' loading SORTAB data
'        HandleSplashScreen "Loading SORTAB (organizational assignments)...", bpHandleSplashScreen
'        strWhere = "WHERE SURN IN (" & strStfUrnos & ")"
'        'strWhere = "WHERE SURN = '" & strStfUrno & "'"
'        LoadData TabSOR, strWhere
'        strToday = Format(Now, "YYYYMMDD")
'        strOrgUnit = GetBestFitRecord(TabSOR, strStfUrno, "SURN", "CODE", strToday)
'
'        ' loading SPFTAB data
'        HandleSplashScreen "Loading SPFTAB (connection between staff and function)...", bpHandleSplashScreen
'    '    strWhere = "WHERE SURN = '" & strStfUrno & "' AND PRIO = '1'"
'        strWhere = "WHERE SURN IN (" & strStfUrnos & ") AND PRIO = '1'"
'        LoadData TabSPF, strWhere
'        strToday = Format(Now, "YYYYMMDD")
'        strFunction = GetBestFitRecord(TabSPF, strStfUrno, "SURN", "CODE", strToday)
'
'        ' loading JTYTAB data
'        HandleSplashScreen "Loading JTYTAB (job types)...", bpHandleSplashScreen
'        strWhere = ""
'        If bpLoadBasicdata = True Then
'            LoadData TabJTY, strWhere
'            ilIdx = GetItemNo(TabJTY.LogicalFieldList, "NAME") - 1
'            TabJTY.IndexCreate "NAME", ilIdx
'            TabJTY.IndexCreate "URNO", 0
'            InitJTY
'        End If


        ' loading JODTAB data
        HandleSplashScreen "Loading JODTAB (connection between jobs and demands)...", bpHandleSplashScreen
        strTmp = TabJOB.SelectDistinct(0, "'", "'", ",", True)
        If Len(strTmp) > 0 Then
            strWhere = "WHERE UJOB IN (" & strTmp & ")"
            LoadData TabJOD, strWhere
        End If
        ilIdx = GetItemNo(TabJOD.LogicalFieldList, "UJOB") - 1
        TabJOD.IndexCreate "UJOB", ilIdx

        ' loading DEMTAB data
        HandleSplashScreen "Loading DEMTAB (demands)...", bpHandleSplashScreen
        ilIdx = GetItemNo(TabJOD.LogicalFieldList, "UDEM") - 1
        strTmp = TabJOD.SelectDistinct(CStr(ilIdx), "'", "'", ",", True)
        If Len(strTmp) > 0 Then
            strWhere = "WHERE URNO IN (" & strTmp & ")"
            LoadData TabDEM, strWhere
        End If
        TabDEM.IndexCreate "URNO", 0
    
        ' loading AFTTAB data
        HandleSplashScreen "Loading AFTTAB (flights)...", bpHandleSplashScreen
        TabTemp.ResetContent
        ilIdx = GetItemNo(TabJOB.LogicalFieldList, "UAFT") - 1
        strTmp = TabJOB.SelectDistinct(CStr(ilIdx), "", "", ",", True)
        ilIdx = GetItemNo(TabDEM.LogicalFieldList, "OURI") - 1
        strTmp = strTmp & "," & TabDEM.SelectDistinct(CStr(ilIdx), "", "", ",", True)
        ilIdx = GetItemNo(TabDEM.LogicalFieldList, "OURO") - 1
        strTmp = strTmp & "," & TabDEM.SelectDistinct(CStr(ilIdx), "", "", ",", True)
        TabTemp.InsertBuffer strTmp, ","
        strTmp = TabTemp.SelectDistinct("0", "", "", ",", True)
        If Len(strTmp) > 0 Then
            While (InStr(strTmp, ",,") > 0)
                strTmp = Replace(strTmp, ",,", ",")
            Wend
            While (Left(strTmp, 1) = ",")
                strTmp = Right(strTmp, Len(strTmp) - 1)
            Wend
            While (Right(strTmp, 1) = ",")
                strTmp = Left(strTmp, Len(strTmp) - 1)
            Wend
            strWhere = "WHERE URNO IN (" & strTmp & ")"
            LoadData TabAFT, strWhere
            TabAFT.IndexCreate "URNO", 0
            TabAFT.IndexCreate "RKEY", 1
        End If
    
        ' loading REMTAB data
        If Len(strTmp) > 0 Then
            HandleSplashScreen "Loading REMTAB (remarks)...", bpHandleSplashScreen
            strWhere = "WHERE RURN IN (" & strTmp & ") AND APPL='OPSS-PM'"
            LoadData TabREM, strWhere
            ilIdx = GetItemNo(TabREM.LogicalFieldList, "RURN") - 1
            TabREM.IndexCreate "RURN", ilIdx
        End If

'        ' loading ALOTAB data
'        HandleSplashScreen "Loading ALOTAB (allocation types)...", bpHandleSplashScreen
'        strWhere = "WHERE ALOT = '0'"
'        If bpLoadBasicdata = True Then
'            LoadData TabALO, strWhere
'            TabALO.IndexCreate "URNO", 0
'        End If
'
'        ' loading BLTTAB data
'        HandleSplashScreen "Loading BLTTAB (baggage belts)...", bpHandleSplashScreen
'        strWhere = ""
'        If bpLoadBasicdata = True Then
'            LoadData TabBLT, strWhere
'            TabBLT.IndexCreate "URNO", 0
'        End If
'
'        ' loading CICTAB data
'        HandleSplashScreen "Loading CICTAB (checkin counters)...", bpHandleSplashScreen
'        strWhere = ""
'        If bpLoadBasicdata = True Then
'            LoadData TabCIC, strWhere
'            TabCIC.IndexCreate "URNO", 0
'        End If
'
'        ' loading CCCTAB data
'        HandleSplashScreen "Loading CCCTAB (common checkin counters)...", bpHandleSplashScreen
'        strWhere = ""
'        If bpLoadBasicdata = True Then
'            LoadData TabCCC, strWhere
'            TabCCC.IndexCreate "URNO", 0
'        End If
'
'        ' loading EQUTAB data
'        HandleSplashScreen "Loading EQUTAB (equipments)...", bpHandleSplashScreen
'        strWhere = ""
'        If bpLoadBasicdata = True Then
'            LoadData TabEQU, strWhere
'            TabEQU.IndexCreate "URNO", 0
'        End If
'
'        ' loading EXTTAB data
'        HandleSplashScreen "Loading EXTTAB (exits)...", bpHandleSplashScreen
'        strWhere = ""
'        If bpLoadBasicdata = True Then
'            LoadData TabEXT, strWhere
'            TabEXT.IndexCreate "URNO", 0
'        End If
'
'        ' loading GATTAB data
'        HandleSplashScreen "Loading GATTAB (gates)...", bpHandleSplashScreen
'        strWhere = ""
'        If bpLoadBasicdata = True Then
'            LoadData TabGAT, strWhere
'            TabGAT.IndexCreate "URNO", 0
'        End If
'
'        ' loading PSTTAB data
'        HandleSplashScreen "Loading PSTTAB (parking stands)...", bpHandleSplashScreen
'        strWhere = ""
'        If bpLoadBasicdata = True Then
'            LoadData TabPST, strWhere
'            TabPST.IndexCreate "URNO", 0
'        End If
'
'        ' loading WROTAB data
'        HandleSplashScreen "Loading WROTAB (waiting rooms)...", bpHandleSplashScreen
'        strWhere = ""
'        If bpLoadBasicdata = True Then
'            LoadData TabWRO, strWhere
'            TabWRO.IndexCreate "URNO", 0
'        End If
'
'        ' loading RPFTAB data
'        HandleSplashScreen "Loading RPFTAB (connection between demands and functions)...", bpHandleSplashScreen
'        ilIdx = GetItemNo(TabDEM.LogicalFieldList, "URUD") - 1
'        strTmp = TabDEM.SelectDistinct(CStr(ilIdx), "'", "'", ",", True)
'        strWhere = ""
'        If Len(strTmp) > 0 Then
'            strWhere = "WHERE URUD IN (" & strTmp & ")"
'            LoadData TabRPF, strWhere
'        End If
'        ilIdx = GetItemNo(TabRPF.LogicalFieldList, "URUD") - 1
'        TabRPF.IndexCreate "URUD", ilIdx
'
'        ' loading RUDTAB data
'        HandleSplashScreen "Loading RUDTAB (Rule demands)...", bpHandleSplashScreen
'        If Len(strTmp) > 0 Then
'            strWhere = "WHERE URNO IN (" & strTmp & ")"
'            LoadData TabRUD, strWhere
'        End If
'        TabRUD.IndexCreate "URNO", 0
'
'        ' loading SERTAB data
'        HandleSplashScreen "Loading SERTAB (Ground handling services)...", bpHandleSplashScreen
'        ilIdx = GetItemNo(TabRUD.LogicalFieldList, "UGHS") - 1
'        strTmp = TabRUD.SelectDistinct(CStr(ilIdx), "'", "'", ",", True)
'        If Len(strTmp) > 0 Then
'            strWhere = "WHERE URNO IN (" & strTmp & ")"
'            LoadData TabSER, strWhere
'        End If
'        TabSER.IndexCreate "URNO", 0
'
'        ' loading SGRTAB data
'        HandleSplashScreen "Loading SGRTAB (static groups)...", bpHandleSplashScreen
'        strWhere = "WHERE TABN = 'PFC'"
'        If bpLoadBasicdata = True Then
'            LoadData TabSGR, strWhere
'            TabSGR.IndexCreate "URNO", 0
'        End If
'
'        ' loading DRRTAB data
'        HandleSplashScreen "Loading DRRTAB (daily roster records)...", bpHandleSplashScreen
'        If (GetConfigEntry("SHIFT_TIMEFRAME", strOrgUnit, strCfg) = False) Then
'            MsgBoxNoCfgEntry "SHIFT_TIMEFRAME"
'        End If
'        strFrom = GetItem(strCfg, 1, ",")
'        strTo = GetItem(strCfg, 2, ",")
'        dateFrom = DateAdd("d", CDbl(strFrom), Now) ' DRRTAB.SDAY is local
'        dateTo = DateAdd("d", CDbl(strTo), Now)
'        strFrom = Format(dateFrom, "YYYYMMDD")
'        strTo = Format(dateTo, "YYYYMMDD")
'        strWhere = "WHERE STFU = '" & strStfUrno & "' AND SDAY between '" & strFrom & "' AND '" & strTo & "' ORDER BY SDAY,ROSL"
'        LoadData TabDRR, strWhere
'        TabDRR.IndexCreate "URNO", 0
'
'        ' loading DRGTAB data
'        HandleSplashScreen "Loading DRGTAB (Working group assignments daily)...", bpHandleSplashScreen
'        strWhere = "WHERE STFU = '" & strStfUrno & "' AND SDAY between '" & strFrom & "' AND '" & strTo & "'"
'        LoadData TabDRG, strWhere 'same where-statement as for DRRTAB
'        ilIdx = GetItemNo(TabDRG.LogicalFieldList, "SDAY") - 1
'        TabDRG.IndexCreate "SDAY", ilIdx
'        ilIdx = GetItemNo(TabDRG.LogicalFieldList, "STFU") - 1
'        TabDRG.IndexCreate "STFU", ilIdx
'
'        ' loading SWGTAB data
'        HandleSplashScreen "Loading SWGTAB (Working group assignments)...", bpHandleSplashScreen
'        strWhere = "WHERE SURN IN (" & strStfUrnos & ")"
'        LoadData TabSWG, strWhere
'        ilIdx = GetItemNo(TabSWG.LogicalFieldList, "SURN") - 1
'        TabSWG.IndexCreate "SURN", CStr(ilIdx)
'        ilIdx = GetItemNo(TabSWG.LogicalFieldList, "VPFR") - 1
'        TabSWG.Sort CStr(ilIdx), True, True
'
'        ' loading ODATAB data
'        HandleSplashScreen "Loading ODATAB (absences)...", bpHandleSplashScreen
'        strWhere = ""
'        If bpLoadBasicdata = True Then
'            LoadData TabODA, strWhere
'            ilIdx = GetItemNo(TabODA.LogicalFieldList, "SDAC") - 1
'            TabODA.IndexCreate "SDAC", ilIdx
'        End If

        ' loading DPXTAB data
        HandleSplashScreen "Loading DPXTAB (passengers)...", bpHandleSplashScreen
        strDPX_UrNo = TabJOB.GetFieldValue(0, "UPRM")
        strWhere = "WHERE URNO = '" & strDPX_UrNo & "'"
        'If bpLoadBasicdata = True Then
            LoadData TabDPX, strWhere
            ilIdx = GetItemNo(TabDPX.LogicalFieldList, "PRID") - 1
            TabDPX.IndexCreate "PRID", ilIdx
        'End If
    Else
        blRet = False
    End If
    ' that we see all columns
    ResizeTabs

    frmSplash.Visible = False
    frmSplash.Hide
    Unload frmSplash
    LoadPRMInfo = blRet
End Function

Public Function LoadTrackSheet(bpHandleSplashScreen As Boolean, bpLoadBasicdata As Boolean, strAftUrnos As String) As Boolean
    '-------------
    'Date: 05 Jan 2010
    'Desc: Retrieve Track Sheet
    'Crea: igu
    '-------------
    Dim blRet As Boolean
    Dim strWhere As String
    Dim strToday As String
    Dim strFrom As String
    Dim strTo As String
    Dim strCfg As String
    Dim dateFrom As Date
    Dim dateTo As Date
    Dim dateTo2 As Date
    Dim strTmp As String
    Dim ilIdx As Integer
    Dim strStfUrnos As String
    Dim strJobUrnos As String
    Dim tmpJobUrnos As String
    Dim strPreparedAftUrnos As String
    Dim i As Long
    Dim cnt As Long
    Dim strAftUrNo As String

    If bpLoadBasicdata = True Then
        InitTabs
    End If

    blRet = True
    
    If strAftUrnos <> "" Then
        cnt = ItemCount(strAftUrnos, ",")
        strPreparedAftUrnos = ""
        For i = 0 To cnt - 1
            'strPreparedAftUrnos = strPreparedAftUrnos & "'" & _
                GetRealItem(strAftUrnos, i, ",") + "'" 'igu on 28 Apr 2010
            strPreparedAftUrnos = strPreparedAftUrnos & "'" & _
                GetRealItem(strAftUrnos, i, ",") & "'" 'igu on 28 Apr 2010
            If i + 1 < cnt Then strPreparedAftUrnos = strPreparedAftUrnos & ","
        Next i
        
        ' init the splash screen
        If bpHandleSplashScreen = True Then
            frmSplash.Visible = True
            frmSplash.List1.Clear
            frmSplash.ProgressBar1.Max = 30
            frmSplash.List1.AddItem "Loading data..."
            frmSplash.Refresh
        Else
            frmMain.txtReloading.Text = " Please wait while reloading user data..."
        End If
        
        ' loading DPXTAB data
        HandleSplashScreen "Loading DPXTAB (passengers)...", bpHandleSplashScreen
        strWhere = "WHERE FLNU IN (" & strPreparedAftUrnos & ")"
        LoadData TabDPX, strWhere
        
        ' loading JOBTAB data
        HandleSplashScreen "Loading JOBTAB (flight jobs)...", bpHandleSplashScreen
        strTmp = ""
        For i = 0 To TabDPX.Lines - 1
            strTmp = strTmp & "," & TabDPX.GetFieldValue(i, "URNO")
        Next i
        If strTmp <> "" Then
            strTmp = Mid(strTmp, 2)
            'strWhere = "WHERE UPRM IN (" & strTmp & ") " & _
                "AND UAFT IN (" & strPreparedAftUrnos & ")" 'igu on 26 May 2010
            strWhere = "WHERE UPRM IN (" & strTmp & ")" 'igu on 26 May 2010
            LoadData TabJOB, strWhere
            ilIdx = GetRealItemNo(TabJOB.LogicalFieldList, "UPRM")
            TabJOB.IndexCreate "UPRM", ilIdx
            
            'loading PoolJobs
            strTmp = ""
            For i = 0 To TabJOB.Lines - 1
                'strTmp = strTmp & "," & TabJOB.GetFieldValue(i, "JOUR") 'igu on 28 Apr 2010
                strTmp = strTmp & ",'" & TabJOB.GetFieldValue(i, "JOUR") & "'" 'igu on 28 Apr 2010
            Next i
            If strTmp <> "" Then
                strTmp = Mid(strTmp, 2)
                strWhere = "WHERE URNO IN (" & strTmp & ")"
                LoadData TabPoolJOB, strWhere
                ilIdx = GetRealItemNo(TabPoolJOB.LogicalFieldList, "URNO")
                TabPoolJOB.IndexCreate "URNO", ilIdx
            End If
        End If
        
        ' loading AFTTAB data
        HandleSplashScreen "Loading AFTTAB (flights)...", bpHandleSplashScreen
        strTmp = ""
        For i = 0 To TabDPX.Lines - 1
            If Val(TabDPX.GetFieldValue(i, "TFLU")) <> 0 Then
                strTmp = strTmp & ",'" & TabDPX.GetFieldValue(i, "TFLU") & "'"
            End If
        Next i
        strPreparedAftUrnos = strPreparedAftUrnos & strTmp
        strWhere = "WHERE URNO IN (" & strPreparedAftUrnos & ")"
        LoadData TabAFT, strWhere
        TabAFT.IndexCreate "URNO", 0
        
        ' loading STFTAB data
        HandleSplashScreen "Loading STFTAB (employee data)...", bpHandleSplashScreen
        strTmp = ""
        For i = 0 To TabJOB.Lines - 1
            strTmp = strTmp & "," & TabJOB.GetFieldValue(i, "USTF")
        Next i
        If strTmp <> "" Then
            strTmp = Mid(strTmp, 2)
            strWhere = "WHERE URNO IN (" & strTmp & ")"
            LoadData TabSTF, strWhere
            ilIdx = GetRealItemNo(TabSTF.LogicalFieldList, "URNO")
            TabSTF.IndexCreate "URNO", ilIdx
        End If
       
    Else
        blRet = False
    End If
    ' that we see all columns
    ResizeTabs

    frmSplash.Visible = False
    frmSplash.Hide
    Unload frmSplash
    LoadTrackSheet = blRet
End Function

Private Sub ConnectUfisCom()
    frmSplash.UfisCom1.SetCedaPerameters frmSplash.AATLoginControl1.GetUserName, strHOPO, strTableExt
    frmSplash.UfisCom1.InitCom strServer, strAccessMethod
End Sub

Private Sub DisconnetUfisCom()
    frmSplash.UfisCom1.CleanupCom
End Sub

Private Function GetUTCOffset() As Single
    Dim strUtcArr() As String
    Dim strArr() As String
    Dim CurrStr As String
    Dim tmpStr As String

    Dim istrRet As Integer
    Dim count As Integer
    Dim i As Integer

    GetUTCOffset = 0

    ConnectUfisCom

    If frmSplash.UfisCom1.CallServer("GFR", "", "", "", "[CONFIG]", "360") = 0 Then
        tmpStr = frmSplash.UfisCom1.GetDataBuffer(True)
    End If
    strArr = Split(tmpStr, Chr(10))
    count = UBound(strArr)
    For i = 0 To count
        CurrStr = strArr(i)
        istrRet = InStr(1, CurrStr, "UTCD", 0)
        If istrRet <> 0 Then
            strUtcArr = Split(strArr(i), ",")
            GetUTCOffset = CSng(strUtcArr(1) / 60)
            Exit For
        End If
    Next i
    DisconnetUfisCom
End Function

Private Sub ReadConfigFile()
    TabConfig.ResetContent
    TabConfig.HeaderLengthString = "100,1000"
    TabConfig.EnableHeaderSizing True
    TabConfig.EnableInlineEdit True
    TabConfig.ShowHorzScroller True
    TabConfig.SetFieldSeparator strFieldSeparatorTabConfig
    TabConfig.HeaderString = "Parameter" & strFieldSeparatorTabConfig & "Setting"
    TabConfig.ReadFromFile strConfigFile
End Sub

Public Function GetConfigEntry(ByRef sParameter As String, ByRef sOrgCode As String, ByRef sReturn) As Boolean
    Dim blRet As Boolean
    Dim strDefault As String
    Dim strFoundOrgCode As String
    Dim strValue As String
    Dim strLines As String
    Dim ilCnt As Integer
    Dim i As Integer

    blRet = False
    sReturn = ""

    strLines = TabConfig.GetLinesByColumnValue(0, sParameter, 0)
    If Len(strLines) > 0 Then
        ilCnt = ItemCount(strLines, ",")
        For i = 1 To ilCnt Step 1
            strValue = TabConfig.GetColumnValue(CLng(GetItem(strLines, i, ",")), 1)
            strFoundOrgCode = GetItem(strValue, 1, ";")

            If strFoundOrgCode = sOrgCode Then
                ' we found the value for the OrgUnit of the logged in employee
                sReturn = GetItem(strValue, 2, ";")
                GetConfigEntry = True
                Exit Function
            ElseIf strFoundOrgCode = "DEFAULT" Then
                ' we found the default setting for that parameter
                strDefault = GetItem(strValue, 2, ";")
            End If
        Next i
        sReturn = strDefault
        blRet = True
    End If

    GetConfigEntry = blRet
End Function

Private Function GetBestFitRecord(rTab As TABLib.Tab, sSearchValue As String, sSearchColumn As String, sReturnColumn As String, sReferenceDate As String) As String
    GetBestFitRecord = ""
    Dim llBestFit As Long
    Dim llTmp As Long
    Dim llTo As Long
    Dim llReferenceDate As Long
    Dim i As Integer
    Dim ilCnt As Integer
    Dim llLineNo As Long

    Dim strTmp As String
    Dim strLineNo As String
    Dim ilIdxSearchColumn As String
    Dim ilIdxVPFR As String
    Dim ilIdxVPTO As String

    ' look if columns do exist and remember their position
    ilIdxSearchColumn = GetItemNo(rTab.LogicalFieldList, sSearchColumn) - 1
    ilIdxVPFR = GetItemNo(rTab.LogicalFieldList, "VPFR") - 1
    ilIdxVPTO = GetItemNo(rTab.LogicalFieldList, "VPTO") - 1

    If (ilIdxSearchColumn > -1 And ilIdxVPFR > -1 And ilIdxVPTO > -1 And IsNumeric(sReferenceDate) = True) Then
        llReferenceDate = CLng(sReferenceDate)
        strLineNo = rTab.GetLinesByColumnValue(ilIdxSearchColumn, sSearchValue, 0)
        If Len(strLineNo) > 0 Then
            ilCnt = ItemCount(strLineNo, ",")
            llBestFit = 0

            For i = 1 To ilCnt Step 1
                llLineNo = CLng(GetItem(strLineNo, i, ","))
                strTmp = rTab.GetFieldValue(llLineNo, "VPFR")
                If Len(strTmp) > 8 Then
                    llTmp = CLng(Left(strTmp, 8))
                    If llTmp <= llReferenceDate Then
                        strTmp = rTab.GetFieldValue(llLineNo, "VPTO")
                        If Len(strTmp) > 7 Then
                            llTo = CLng(Left(strTmp, 8))
                        Else
                            llTo = 99999999
                        End If

                        If llTmp > llBestFit And llTo >= llReferenceDate Then
                            llBestFit = llTmp
                            GetBestFitRecord = rTab.GetFieldValue(llLineNo, sReturnColumn)
                        End If
                    End If
                End If
            Next i
        End If
    End If
End Function

Public Function GetJobTimes(sJobUrno As String) As String
    Dim strLineNo  As String
    Dim strRet As String
    Dim strTmp As String
    Dim dateTmp As Date
    Dim llLine As Long
    
    TabJOB.SetInternalLineBuffer True

    strLineNo = TabJOB.GetLinesByIndexValue("URNO", sJobUrno, 0)
    llLine = TabJOB.GetNextResultLine
    If llLine > -1 Then
        'SHOW_TIME_OF_JOB=DEFAULT;FLT,GAT,CCI,BRK,SPE,CCC,FID)
        Dim strUJTY As String
        Dim strItem As String
        Dim strLineNoJty As String

        strUJTY = TabJOB.GetFieldValue(llLine, "UJTY")
        strLineNoJty = TabJTY.GetLinesByIndexValue("URNO", strUJTY, 0)

        If IsNumeric(strLineNoJty) = True Then
            strItem = "," & TabJTY.GetFieldValue(CLng(strLineNoJty), "NAME") & ","
            If InStr(1, frmMain.strSHOW_TIME_OF_JOB, strItem, vbTextCompare) > 0 Then
                strTmp = TabJOB.GetFieldValue(llLine, "ACFR")
                If Len(strTmp) > 0 Then
                    dateTmp = CedaFullDateToVb(strTmp) + (sglUTCOffsetHours / 24)
                    strRet = Format(dateTmp, "hh:mm") & "/" & Format(dateTmp, "DD") & "-"
                Else
                     strRet = strRet & "n.a.-"
                End If
                strTmp = TabJOB.GetFieldValue(llLine, "ACTO")
                If Len(strTmp) > 0 Then
                    dateTmp = CedaFullDateToVb(strTmp) + (sglUTCOffsetHours / 24)
                    strRet = strRet & Format(dateTmp, "hh:mm") & "/" & Format(dateTmp, "DD")
                Else
                    strRet = strRet & "n.a."
                End If
            End If
        End If
    End If
    TabJOB.SetInternalLineBuffer False
    GetJobTimes = strRet
End Function

Public Function GetJobType(sJobUrno As String) As String
    Dim strRet As String
    Dim strType As String
    Dim llLine As Long
    Dim strDemUrno As String
    
    TabJOD.SetInternalLineBuffer True
    TabDEM.SetInternalLineBuffer True
    strRet = TabJOD.GetLinesByIndexValue("UJOB", sJobUrno, 0)
    llLine = TabJOD.GetNextResultLine
    If llLine > -1 Then
        strDemUrno = TabJOD.GetFieldValue(llLine, "UDEM")
        strRet = TabDEM.GetLinesByIndexValue("URNO", strDemUrno, 0)
        llLine = TabDEM.GetNextResultLine
        If llLine > -1 Then
            strType = TabDEM.GetFieldValue(llLine, "DETY")
        End If
    End If
    
    TabJOD.SetInternalLineBuffer False
    TabDEM.SetInternalLineBuffer False
    GetJobType = strType
End Function

Public Function GetJobFunction(sJobUrno As String) As String
    Dim strLineNo As String
    Dim strSearch As String
    Dim strRet As String
    Dim ilDemandCnt As Integer

    strRet = ""

    strLineNo = TabJOD.GetLinesByIndexValue("UJOB", sJobUrno, 0)
    If IsNumeric(strLineNo) = True Then
        ' if there are duplicated demands, take the last on
        ilDemandCnt = ItemCount(strLineNo, ",")
        strLineNo = GetItem(strLineNo, ilDemandCnt, ",")

        strSearch = TabJOD.GetFieldValue(CLng(strLineNo), "UDEM")
        strLineNo = TabDEM.GetLinesByIndexValue("URNO", strSearch, 0)
        If IsNumeric(strLineNo) = True Then
            strSearch = TabDEM.GetFieldValue(CLng(strLineNo), "URUD")
            strLineNo = TabRPF.GetLinesByIndexValue("URUD", strSearch, 0)
            If IsNumeric(strLineNo) = True Then
                strSearch = TabRPF.GetFieldValue(CLng(strLineNo), "FCCO")
                If Len(strSearch) > 0 Then
                    strRet = strSearch
                Else
                    strSearch = TabRPF.GetFieldValue(CLng(strLineNo), "UPFC")
                    strLineNo = TabSGR.GetLinesByIndexValue("URNO", strSearch, 0)
                    If IsNumeric(strLineNo) = True Then
                        strRet = TabSGR.GetFieldValue(CLng(strLineNo), "GRPN")
                    End If
                End If
            End If
        End If
    End If

    ' we take the function of the shift
    If Len(strRet) = 0 Then
        strLineNo = TabJOB.GetLinesByIndexValue("URNO", sJobUrno, 0)
        If IsNumeric(strLineNo) = True Then
            Dim strUdsr As String
            strUdsr = TabJOB.GetFieldValue(CLng(strLineNo), "UDSR")
            strLineNo = TabDRR.GetLinesByIndexValue("URNO", strUdsr, 0)
            If IsNumeric(strLineNo) = True Then
                strRet = TabDRR.GetFieldValue(CLng(strLineNo), "FCTC")
            End If
        End If
    End If

    GetJobFunction = strRet
End Function

Public Function GetJobDuty(sJobUrno As String) As String
    Dim strLineNo As String
    Dim strSearch As String
    Dim ilIdx As Integer
    Dim strUJTY As String
    Dim strAftUrnoInbound As String
    Dim strAftUrnoOutbound As String
    Dim strFlnoInbound As String
    Dim strFlnoOutbound As String
    Dim strFlno As String
    Dim strDETY As String
    Dim ilDemandCnt As Integer

    GetJobDuty = ""

    strLineNo = TabJOB.GetLinesByIndexValue("UJOB", sJobUrno, 0)
    If IsNumeric(strLineNo) = True Then
        strUJTY = TabJOB.GetFieldValue(CLng(strLineNo), "UJTY")
        Select Case strUJTY
            Case strFLT:
                ' get the FLNO of flight of the job
                strLineNo = TabAFT.GetLinesByIndexValue("URNO", TabJOB.GetFieldValue(CLng(strLineNo), "UAFT"), 0)
                If IsNumeric(strLineNo) = True Then
                    'strFlno = TabAFT.GetFieldValue(CLng(strLineNo), "FLNO")
                    'igu on 21/04/2011
                    'take call sign when FLNO is empty
                    strFlno = GetFlnoOrCsgn(TabAFT, CLng(strLineNo))
                End If

                ' look if there is a demand - then take FLNO(s) of the demand
                strLineNo = TabJOD.GetLinesByIndexValue("UJOB", sJobUrno, 0)
                If IsNumeric(strLineNo) = True Then
                    ' if there are duplicated demands, take the last on
                    ilDemandCnt = ItemCount(strLineNo, ",")
                    strLineNo = GetItem(strLineNo, ilDemandCnt, ",")

                    strSearch = TabJOD.GetFieldValue(CLng(strLineNo), "UDEM")
                    strLineNo = TabDEM.GetLinesByIndexValue("URNO", strSearch, 0)
                    If IsNumeric(strLineNo) = True Then
                        ' get AFT-urnos and demand type
                        strAftUrnoInbound = TabDEM.GetFieldValue(CLng(strLineNo), "OURI")
                        strAftUrnoOutbound = TabDEM.GetFieldValue(CLng(strLineNo), "OURO")
                        strDETY = TabDEM.GetFieldValue(CLng(strLineNo), "DETY")

                        ' get inbound FLNO
                        strLineNo = TabAFT.GetLinesByIndexValue("URNO", strAftUrnoInbound, 0)
                        If IsNumeric(strLineNo) = True Then
                            'strFlnoInbound = TabAFT.GetFieldValue(CLng(strLineNo), "FLNO")
                            'igu on 21/04/2011
                            'take call sign when FLNO is empty
                            strFlnoInbound = GetFlnoOrCsgn(TabAFT, CLng(strLineNo))
                        End If

                        ' get outbound FLNO
                        strLineNo = TabAFT.GetLinesByIndexValue("URNO", strAftUrnoOutbound, 0)
                        If IsNumeric(strLineNo) = True Then
                            'strFlnoOutbound = TabAFT.GetFieldValue(CLng(strLineNo), "FLNO")
                            'igu on 21/04/2011
                            'take call sign when FLNO is empty
                            strFlnoOutbound = GetFlnoOrCsgn(TabAFT, CLng(strLineNo))
                        End If

                        If strDETY = "0" Then
                            ' it is a turnaround
                            GetJobDuty = strFlnoInbound & "/" & strFlnoOutbound
                        ElseIf strDETY = "1" Then
                            ' it is an inbound
                            GetJobDuty = strFlnoInbound
                        ElseIf strDETY = "2" Then
                            ' it is an outbound
                            GetJobDuty = strFlnoOutbound
                        End If
                    End If
                Else
                    GetJobDuty = strFlno
                End If
            Case strBRK:
                GetJobDuty = "Break"
            Case strFID:
                GetJobDuty = GetJobServiceName(sJobUrno)
            Case strGAT, strCCI:
                GetJobDuty = GetAlidForJob(TabJOB.GetFieldValue(CLng(strLineNo), "UAID"), TabJOB.GetFieldValue(CLng(strLineNo), "UALO"))
            Case strCCC:
                GetJobDuty = GetCciCounter(TabJOB.GetFieldValue(CLng(strLineNo), "URNO"))
            Case Else: 'e.g. strSPE
                GetJobDuty = TabJOB.GetFieldValue(CLng(strLineNo), "TEXT")
        End Select
    End If
End Function

Private Function GetCciCounter(sJobUrno As String) As String
    Dim strLineNo As String
    Dim strSearch As String
    Dim ilDemandCnt As Integer

    GetCciCounter = ""
    strLineNo = TabJOD.GetLinesByIndexValue("UJOB", sJobUrno, 0)
    If IsNumeric(strLineNo) = True Then
        ' if there are duplicated demands, take the last on
        ilDemandCnt = ItemCount(strLineNo, ",")
        strLineNo = GetItem(strLineNo, ilDemandCnt, ",")

        strSearch = TabJOD.GetFieldValue(CLng(strLineNo), "UDEM")
        strLineNo = TabDEM.GetLinesByIndexValue("URNO", strSearch, 0)
        If IsNumeric(strLineNo) = True Then
            strSearch = TabDEM.GetFieldValue(CLng(strLineNo), "URUD")
            strLineNo = TabRUD.GetLinesByIndexValue("URNO", strSearch, 0)
            If IsNumeric(strLineNo) = True Then
                strSearch = TabRUD.GetFieldValue(CLng(strLineNo), "VREF")
                strLineNo = TabCCC.GetLinesByIndexValue("URNO", strSearch, 0)
                If IsNumeric(strLineNo) = True Then
                    GetCciCounter = "CCI " & TabCCC.GetFieldValue(CLng(strLineNo), "CICC")
                End If
            End If
        End If
    End If
End Function

Private Function GetAlidForJob(sUAID As String, sUALO As String) As String
    Dim strLineNo As String
    Dim strSearch As String

    strLineNo = TabALO.GetLinesByIndexValue("URNO", sUALO, 0)

    If IsNumeric(strLineNo) = True Then
        Dim strTable As String
        strTable = Left(TabALO.GetFieldValue(CLng(strLineNo), "REFT"), 3)

        If strTable = "GAT" Then
            strLineNo = TabGAT.GetLinesByIndexValue("URNO", sUAID, 0)
            If IsNumeric(strLineNo) = True Then
                GetAlidForJob = "Gate: " & TabGAT.GetFieldValue(CLng(strLineNo), "GNAM")
            End If
        ElseIf strTable = "PST" Then
            strLineNo = TabPST.GetLinesByIndexValue("URNO", sUAID, 0)
            If IsNumeric(strLineNo) = True Then
                GetAlidForJob = "Park. Stand : " & TabPST.GetFieldValue(CLng(strLineNo), "PNAM")
            End If
        ElseIf strTable = "CIC" Then
            strLineNo = TabCIC.GetLinesByIndexValue("URNO", sUAID, 0)
            If IsNumeric(strLineNo) = True Then
                GetAlidForJob = "CIC Desk: " & TabCIC.GetFieldValue(CLng(strLineNo), "CNAM")
            End If
        Else
            strLineNo = TabSGR.GetLinesByIndexValue("URNO", sUAID, 0)
            If IsNumeric(strLineNo) = True Then
                GetAlidForJob = TabSGR.GetFieldValue(CLng(strLineNo), "GRPN")
            End If
        End If
    End If
End Function

Public Function GetJobServiceName(ByRef sJobUrno As String) As String
    Dim strLineNo As String
    Dim strSearch As String
    Dim ilDemandCnt As Integer

    strLineNo = TabJOD.GetLinesByIndexValue("UJOB", sJobUrno, 0)
    If IsNumeric(strLineNo) = True Then
        ' if there are duplicated demands, take the last on
        ilDemandCnt = ItemCount(strLineNo, ",")
        strLineNo = GetItem(strLineNo, ilDemandCnt, ",")

        strSearch = TabJOD.GetFieldValue(CLng(strLineNo), "UDEM")
        strLineNo = TabDEM.GetLinesByIndexValue("URNO", strSearch, 0)
        If IsNumeric(strLineNo) = True Then
            strSearch = TabDEM.GetFieldValue(CLng(strLineNo), "URUD")
            strLineNo = TabRUD.GetLinesByIndexValue("URNO", strSearch, 0)
            If IsNumeric(strLineNo) = True Then
                strSearch = TabRUD.GetFieldValue(CLng(strLineNo), "UGHS")
                strLineNo = TabSER.GetLinesByIndexValue("URNO", strSearch, 0)
                If IsNumeric(strLineNo) = True Then
                    GetJobServiceName = TabSER.GetFieldValue(CLng(strLineNo), "SNAM")
                End If
            End If
        End If
    End If
End Function

Private Sub InitJTY()
    Dim strLineNo As String

    strLineNo = TabJTY.GetLinesByIndexValue("NAME", "FLT", 0)
    If IsNumeric(strLineNo) = True Then strFLT = TabJTY.GetFieldValue(CLng(strLineNo), "URNO")

    strLineNo = TabJTY.GetLinesByIndexValue("NAME", "GAT", 0)
    If IsNumeric(strLineNo) = True Then strGAT = TabJTY.GetFieldValue(CLng(strLineNo), "URNO")

    strLineNo = TabJTY.GetLinesByIndexValue("NAME", "BRK", 0)
    If IsNumeric(strLineNo) = True Then strBRK = TabJTY.GetFieldValue(CLng(strLineNo), "URNO")

    strLineNo = TabJTY.GetLinesByIndexValue("NAME", "SPE", 0)
    If IsNumeric(strLineNo) = True Then strSPE = TabJTY.GetFieldValue(CLng(strLineNo), "URNO")

    strLineNo = TabJTY.GetLinesByIndexValue("NAME", "CCC", 0)
    If IsNumeric(strLineNo) = True Then strCCC = TabJTY.GetFieldValue(CLng(strLineNo), "URNO")

    strLineNo = TabJTY.GetLinesByIndexValue("NAME", "CCI", 0)
    If IsNumeric(strLineNo) = True Then strCCI = TabJTY.GetFieldValue(CLng(strLineNo), "URNO")

    strLineNo = TabJTY.GetLinesByIndexValue("NAME", "FID", 0)
    If IsNumeric(strLineNo) = True Then strFID = TabJTY.GetFieldValue(CLng(strLineNo), "URNO")
End Sub

Public Function GetFlightInformation(sJobUrno As String) As String
    Dim strLineNo As String
    Dim strSearch As String
    Dim strAftUrnoInbound As String
    Dim strAftUrnoOutbound As String
    Dim strDETY As String
    Dim strUAFT As String
    Dim strRet As String
    Dim ilDemandCnt As Integer

    GetFlightInformation = ""
    strRet = ""

    strLineNo = TabJOB.GetLinesByIndexValue("UJOB", sJobUrno, 0)
    If IsNumeric(strLineNo) = True Then
        ' get the FLNO of flight of the job
        strUAFT = TabJOB.GetFieldValue(CLng(strLineNo), "UAFT")

        ' look if there is a demand - then take FLNO(s) of the demand
        strLineNo = TabJOD.GetLinesByIndexValue("UJOB", sJobUrno, 0)
        If IsNumeric(strLineNo) = True Then
            ' if there are duplicated demands, take the last on
            ilDemandCnt = ItemCount(strLineNo, ",")
            strLineNo = GetItem(strLineNo, ilDemandCnt, ",")

            strSearch = TabJOD.GetFieldValue(CLng(strLineNo), "UDEM")
            strLineNo = TabDEM.GetLinesByIndexValue("URNO", strSearch, 0)
            If IsNumeric(strLineNo) = True Then
                ' we found a demand - get AFT-urnos and demand type
                strAftUrnoInbound = TabDEM.GetFieldValue(CLng(strLineNo), "OURI")
                strAftUrnoOutbound = TabDEM.GetFieldValue(CLng(strLineNo), "OURO")
                strDETY = TabDEM.GetFieldValue(CLng(strLineNo), "DETY")

                If strDETY = "0" Then
                    ' it is a turnaround
                    strRet = GetInboundData(strAftUrnoInbound, True, True)
                    strRet = strRet & "," & GetOutboundData(strAftUrnoOutbound, False, False)
                ElseIf strDETY = "1" Then
                    ' it is an inbound
                    strRet = GetInboundData(strAftUrnoInbound, True, True)
                    strRet = strRet & ",,,,"
                ElseIf strDETY = "2" Then
                    ' it is an outbound
                    strRet = ",,,,"
                    strRet = strRet & GetOutboundData(strAftUrnoOutbound, True, True)
                Else
                    strRet = ",,,,,,,,,"
                End If
            End If
        Else
            ' there is no demand - we have to take UAFT from JOBTAB to get flight information
            strRet = GetInboundData(strUAFT, True, True)
            strRet = strRet & "," & GetOutboundData(strUAFT, False, False)
        End If
        strRet = strRet & "," & GetAftGate(strUAFT)
    End If
    GetFlightInformation = strRet
End Function

Private Function GetInboundData(sAftUrno As String, bWithRegn As Boolean, bWithAct As Boolean)
    Dim strRet As String
    Dim strLineNo As String
    Dim dateTmp As Date
    Dim strTmp As String

    strLineNo = TabAFT.GetLinesByIndexValue("URNO", sAftUrno, 0)
    If IsNumeric(strLineNo) = True Then
        Dim strADID As String
        strADID = TabAFT.GetFieldValue(CLng(strLineNo), "ADID")

        If strADID = "A" Or strADID = "B" Then
            'strRet = strRet & TabAFT.GetFieldValue(CLng(strLineNo), "FLNO") & ","
            'igu on 21/04/2011
            'take call sign when FLNO is empty
            strRet = strRet & GetFlnoOrCsgn(TabAFT, CLng(strLineNo)) & ","
            strTmp = TabAFT.GetFieldValue(CLng(strLineNo), "STOA")
            If Len(strTmp) > 0 Then
                dateTmp = CedaFullDateToVb(strTmp) + (sglUTCOffsetHours / 24)
                strRet = strRet & Format(dateTmp, "hh:mm") & "/" & Format(dateTmp, "DD") & ","
            Else
                strRet = strRet & ","
            End If
            strTmp = TabAFT.GetFieldValue(CLng(strLineNo), "ETAI")
            If Len(strTmp) > 0 Then
                dateTmp = CedaFullDateToVb(strTmp) + (sglUTCOffsetHours / 24)
                strRet = strRet & Format(dateTmp, "hh:mm") & "/" & Format(dateTmp, "DD") & ","
            Else
                strRet = strRet & ","
            End If

            strRet = strRet & TabAFT.GetFieldValue(CLng(strLineNo), "ORG3") & "/"
            strRet = strRet & TabAFT.GetFieldValue(CLng(strLineNo), "ORG4")

            If bWithRegn = True Then
                strRet = strRet & "," & TabAFT.GetFieldValue(CLng(strLineNo), "REGN")
            End If
            If bWithAct = True Then
                strRet = strRet & "," & TabAFT.GetFieldValue(CLng(strLineNo), "ACT3") & "/"
                strRet = strRet & TabAFT.GetFieldValue(CLng(strLineNo), "ACT5")
            End If
        Else
            strRet = strRet & ",,,"
            If bWithRegn = True Then strRet = strRet & ","
            If bWithAct = True Then strRet = strRet & ","
        End If
    Else
        strRet = ",,,"
        If bWithRegn = True Then strRet = strRet & ","
        If bWithAct = True Then strRet = strRet & ","
    End If
    GetInboundData = strRet
End Function

Private Function GetOutboundData(sAftUrno As String, bWithRegn As Boolean, bWithAct As Boolean)
    Dim strRet As String
    Dim strLineNo As String
    Dim dateTmp As Date
    Dim strTmp As String

    GetOutboundData = ""
    strLineNo = TabAFT.GetLinesByIndexValue("URNO", sAftUrno, 0)
    If IsNumeric(strLineNo) = True Then
        Dim strADID As String
        strADID = TabAFT.GetFieldValue(CLng(strLineNo), "ADID")
        If strADID = "D" Or strADID = "B" Then
            If bWithRegn = True Then
                strRet = strRet & TabAFT.GetFieldValue(CLng(strLineNo), "REGN") & ","
            End If
            If bWithAct = True Then
                strRet = strRet & TabAFT.GetFieldValue(CLng(strLineNo), "ACT3") & "/"
                strRet = strRet & TabAFT.GetFieldValue(CLng(strLineNo), "ACT5") & ","
            End If

            'strRet = strRet & TabAFT.GetFieldValue(CLng(strLineNo), "FLNO") & ","
            'igu on 21/04/2011
            'take call sign when FLNO is empty
            strRet = strRet & GetFlnoOrCsgn(TabAFT, CLng(strLineNo)) & ","
            strTmp = TabAFT.GetFieldValue(CLng(strLineNo), "STOD")
            If Len(strTmp) > 0 Then
                dateTmp = CedaFullDateToVb(strTmp) + (sglUTCOffsetHours / 24)
                strRet = strRet & Format(dateTmp, "hh:mm") & "/" & Format(dateTmp, "DD") & ","
            Else
                strRet = strRet & ","
            End If
            strTmp = TabAFT.GetFieldValue(CLng(strLineNo), "ETDI")
            If Len(strTmp) > 0 Then
                dateTmp = CedaFullDateToVb(strTmp) + (sglUTCOffsetHours / 24)
                strRet = strRet & Format(dateTmp, "hh:mm") & "/" & Format(dateTmp, "DD") & ","
            Else
                strRet = strRet & ","
            End If
            strRet = strRet & TabAFT.GetFieldValue(CLng(strLineNo), "DES3") & "/"
            strRet = strRet & TabAFT.GetFieldValue(CLng(strLineNo), "DES4")
        Else
            strRet = strRet & ",,,"
            If bWithRegn = True Then strRet = strRet & ","
            If bWithAct = True Then strRet = strRet & ","
        End If
    Else
        strRet = ",,,"
        If bWithRegn = True Then strRet = strRet & ","
        If bWithAct = True Then strRet = strRet & ","
    End If
    GetOutboundData = strRet
End Function

Public Function GetJobLocation(sJobUrno As String) As String
    Dim strLineNo As String
    GetJobLocation = ""

    strLineNo = TabJOB.GetLinesByIndexValue("UJOB", sJobUrno, 0)
    If IsNumeric(strLineNo) = True Then
        Dim strUALO As String
        Dim strUAID As String

        strUALO = TabJOB.GetFieldValue(CLng(strLineNo), "UALO")
        strUAID = TabJOB.GetFieldValue(CLng(strLineNo), "UAID")
        GetJobLocation = GetAlidForJob(strUAID, strUALO)
    End If
End Function

Public Function GetJobStatus(sJobUrno As String, sSTAT As String) As String
    GetJobStatus = ""

    Dim strSTAT As String

    Dim blPlanned As Boolean
    Dim blInformed As Boolean
    Dim blConfirmed As Boolean 'has the meaning of "started"
    Dim blFinished As Boolean

    If Len(sSTAT) = 0 Then
        Dim strLineNo As String
        strLineNo = TabJOB.GetLinesByIndexValue("UJOB", sJobUrno, 0)
        If IsNumeric(strLineNo) = True Then
            strSTAT = TabJOB.GetFieldValue(CLng(strLineNo), "STAT")
        End If
    Else
        strSTAT = sSTAT
    End If

    GetJobStatusDetails strSTAT, blPlanned, blInformed, blConfirmed, blFinished

    If blPlanned = True Then
        If blInformed = True Then
            GetJobStatus = "Informed"
        Else
            GetJobStatus = "Planned"
        End If
    ElseIf blConfirmed = True Then ' meaning of "started"
        GetJobStatus = "Confirmed"
    ElseIf blFinished = True Then
        GetJobStatus = "Finished"
    Else
        GetJobStatus = ""
    End If
End Function

Public Function SetNextJobStatus(sJobUrno As String) As String
    Dim strLineNo As String
    Dim strRet As String

    strLineNo = TabJOB.GetLinesByIndexValue("URNO", sJobUrno, 0)

    If IsNumeric(strLineNo) = True Then
        Dim strCfg As String
        Dim strSTAT As String
        Dim blPlanned As Boolean
        Dim blInformed As Boolean
        Dim blConfirmed As Boolean 'has the meaning of "started"
        Dim blFinished As Boolean
        Dim strActualSTAT As String
        Dim strNextSTAT As String
        Dim ilIdx As Integer

        strSTAT = TabJOB.GetFieldValue(CLng(strLineNo), "STAT")
        GetJobStatusDetails strSTAT, blPlanned, blInformed, blConfirmed, blFinished
        While Len(strSTAT) < 10
            strSTAT = strSTAT & " " 'it's easier to handle later
        Wend

        ' checking out the actual status
        If blPlanned = True Then
            If blInformed = True Then
                strActualSTAT = "INFORM"
            Else
                strActualSTAT = "PLAN"
            End If
        ElseIf blConfirmed = True Then ' meaning of "started"
            strActualSTAT = "CONFIRM"
        ElseIf blFinished = True Then
            strActualSTAT = "END"
        Else
            strActualSTAT = ""
        End If

        If (GetConfigEntry("WORKFLOW", strOrgUnit, strCfg) = False) Then
            MsgBoxNoCfgEntry "WORKFLOW"
        Else
            ' we have to set the next status depending on the workflow
            ilIdx = GetRealItemNo(strCfg, strActualSTAT)
            If ilIdx > -1 Then
                strNextSTAT = GetRealItem(strCfg, ilIdx + 1, ",")
                If Len(strNextSTAT) > 0 Then
                    Select Case strNextSTAT
                        Case "INFORM":
                            strSTAT = Trim(Left(strSTAT, 4) & "1" & Mid(strSTAT, 6))
                            strRet = "Informed"
                        Case "CONFIRM":
                            strSTAT = Trim("C" & Mid(strSTAT, 2))
                            strRet = "Confirmed"
                        Case "FINISH":
                            strSTAT = Trim("F" & Mid(strSTAT, 2))
                            strRet = "Finished"
                        Case Else:
                    End Select

                    ' set the value internal
                    TabJOB.SetFieldValues CLng(strLineNo), "STAT", strSTAT

                    ' set the values in the database
                    ConnectUfisCom
                    Dim strFieldList As String
                    Dim strData As String
                    Dim strWhere As String
                    strData = TabJOB.GetFieldValues(CLng(strLineNo), "URNO,STAT")
                    strData = strData & "," & gsUserName & "," & GetActualCedaUtcTime("YYYYMMDDhhmmss")
                    strFieldList = "URNO,STAT,USEU,LSTU"
                    strWhere = "WHERE URNO = '" & TabJOB.GetFieldValue(CLng(strLineNo), "URNO") & "'"
                    If frmSplash.UfisCom1.CallServer("URT", "JOBTAB", strFieldList, strData, strWhere, "360") <> 0 Then
                        MsgBox "Failed to update the job status!", vbCritical, "Database error"
                    End If
                    DisconnetUfisCom
                End If
            End If
        End If
    End If

    SetNextJobStatus = strRet
End Function

Private Function GetJobStatusDetails(sSTAT As String, brPlanned As Boolean, brInformed As Boolean, brConfirmed As Boolean, brFinished As Boolean)
'[0] Job Status 'P'lanned,'C'onfirmed or 'F'inished
'[1] '1' = Manually changed else '0'
'[2] Non-break-jobs:'1' = Ignore this job in automatic allocation (Fix Flag) else '0' Break-jobs:'1' = Coffee break (unpaid break)'0' = Normal break.
'[3] used for CASP process (Softlab)
'[4] Normal Jobs: '1' = job is planned, the emp has been informed of the job but has not yet started else '0'. Pool Jobs: 'A' = employee is absent else '0'.
'[5] '1' = job has been printed else '0'
'[6] - [9] Unused.

    Dim strTmp As String

    brPlanned = False
    brInformed = False
    brConfirmed = False
    brFinished = False

    If Len(sSTAT) > 0 Then
        'checking first character
        strTmp = Left(sSTAT, 1)
        Select Case strTmp
            Case "P":
                brPlanned = True
            Case "C":
                brConfirmed = True
            Case "F":
                brFinished = True
        End Select
    End If

    If Len(sSTAT) > 4 Then
        ' checking the 5th character
        strTmp = Mid(sSTAT, 5, 1)
        If strTmp = "1" Then
            brInformed = True
        End If
    End If
End Function

Private Function GetActualCedaUtcTime(sFormat As String) As String
    Dim dateTmp As Date
    Dim strRet As String

    dateTmp = Now - (sglUTCOffsetHours / 24)
    strRet = Format(dateTmp, sFormat)
    GetActualCedaUtcTime = strRet
End Function

Public Function FilterJobs()
    Dim ilIdx As Integer
    Dim ilIdxAvto As Integer
    Dim strCfg As String
    Dim i As Integer
    Dim strTmp As String

    ' sort jobs by start time
    ilIdx = GetItemNo(TabJOB.LogicalFieldList, "ACFR") - 1
    ilIdxAvto = GetItemNo(TabJOB.LogicalFieldList, "AVTO") - 1
    TabJOB.Sort CStr(ilIdx) & "," & CStr(ilIdxAvto), True, True

    ' filtering: JOB_STATUS_DISPLAY (possible: PLANNED,STARTED,CONFIRMED,FINISHED)
    If (GetConfigEntry("JOB_STATUS_DISPLAY", strOrgUnit, strCfg) = False) Then
        MsgBoxNoCfgEntry "JOB_STATUS_DISPLAY"
    Else
        Dim strSTAT As String
        For i = TabJOB.GetLineCount - 1 To 0 Step -1
            strSTAT = TabJOB.GetFieldValue(CLng(i), "STAT")
            strTmp = UCase(GetJobStatus("", strSTAT))
            If InStr(strCfg, strTmp) = 0 Then
                TabJOB.DeleteLine i
            End If
        Next i
    End If

    ' filtering JOB_NUMBER_DISPLAY (e.g.-2,10)
    If (GetConfigEntry("JOB_NUMBER_DISPLAY", strOrgUnit, strCfg) = False) Then
        MsgBoxNoCfgEntry "JOB_NUMBER_DISPLAY"
    Else
        Dim strNow As String
        Dim ilFrom As Integer
        Dim ilTo As Integer

        ilFrom = CInt(GetItem(strCfg, 1, ","))
        ilTo = CInt(GetItem(strCfg, 2, ","))

        ' searching the (best fitting) actual job
        If TabJOB.GetLineCount > 0 Then
            i = 0
            strNow = GetActualCedaUtcTime("YYYYMMDDhhmmss")
            While TabJOB.GetFieldValue(i + 1, "ACFR") < strNow And i <= TabJOB.GetLineCount
                i = i + 1
            Wend

            ' don't show too many jobs in the future
            While (i + ilTo) < TabJOB.GetLineCount - 1
                TabJOB.DeleteLine TabJOB.GetLineCount - 1
            Wend

            ' don't show too many jobs in the past
            While (i + ilFrom) > 0
                TabJOB.DeleteLine 0
                i = i - 1
            Wend
        End If
    End If

    ilIdx = GetRealItemNo(TabJOB.LogicalFieldList, "URNO")
    TabJOB.IndexCreate "URNO", ilIdx
    'TabJOB.IndexCreate "UAFT", 1
End Function

Public Function IsAllowedToChangeJobStatus(sJobUrno As String)
    Dim strCfg As String
    Dim blRet As Boolean
    Dim i As Integer

    blRet = False

    'checking the INTERACTION_STATUS (e.g.-1,3)
    If (GetConfigEntry("INTERACTION_STATUS", strOrgUnit, strCfg) = False) Then
        MsgBoxNoCfgEntry "INTERACTION_STATUS"
    Else
        Dim strLineNo As String
        Dim strNow As String
        Dim ilFrom As Integer
        Dim ilTo As Integer

        ilFrom = CInt(GetItem(strCfg, 1, ","))
        ilTo = CInt(GetItem(strCfg, 2, ","))

        If TabJOB.GetLineCount > 0 Then
            ' searching the (best fitting) actual job
            i = 0
            strNow = GetActualCedaUtcTime("YYYYMMDDhhmmss")
            While TabJOB.GetFieldValue(i + 1, "ACFR") < strNow And i <= TabJOB.GetLineCount
                i = i + 1
            Wend

            ' getting the LineNo of sJobUrno
            strLineNo = TabJOB.GetLinesByIndexValue("URNO", sJobUrno, 0)
            If IsNumeric(strLineNo) = True Then
                If (CInt(strLineNo) >= (i + ilFrom)) And (CInt(strLineNo) <= (i + ilTo)) Then
                    blRet = True
                End If
            End If
        End If
    End If
    IsAllowedToChangeJobStatus = blRet
End Function

Public Function GetAftUrnosByJob(sJobUrno As String) As String
    Dim strLineNo As String
    Dim strSearch As String
    Dim strAftUrnoInbound As String
    Dim strAftUrnoOutbound As String

    Dim strUAFT As String
    Dim strRet As String
    Dim ilDemandCnt As Integer

    GetAftUrnosByJob = ""
    strRet = ""

'MWO ==> Quatsch!    strLineNo = TabJOB.GetLinesByIndexValue("UJOB", sJobUrno, 0)
    strLineNo = TabJOB.GetLinesByIndexValue("URNO", sJobUrno, 0)
    If IsNumeric(strLineNo) = True Then
        ' get the FLNO of flight of the job
        strUAFT = TabJOB.GetFieldValue(CLng(strLineNo), "UAFT")

        ' look if there is a demand - then take FLNO(s) of the demand
        strLineNo = TabJOD.GetLinesByIndexValue("UJOB", sJobUrno, 0)
        If IsNumeric(strLineNo) = True Then
            ' if there are duplicated demands, take the last on
            ilDemandCnt = ItemCount(strLineNo, ",")
            strLineNo = GetItem(strLineNo, ilDemandCnt, ",")

            strSearch = TabJOD.GetFieldValue(CLng(strLineNo), "UDEM")
            strLineNo = TabDEM.GetLinesByIndexValue("URNO", strSearch, 0)
            If IsNumeric(strLineNo) = True Then
                ' we found a demand - get AFT-urnos and demand type
                strAftUrnoInbound = TabDEM.GetFieldValue(CLng(strLineNo), "OURI")
                strAftUrnoOutbound = TabDEM.GetFieldValue(CLng(strLineNo), "OURO")

                If Len(strAftUrnoInbound) > 0 And strAftUrnoInbound <> "0" Then
                    strRet = strAftUrnoInbound
                End If
                If Len(strAftUrnoOutbound) > 0 And strAftUrnoOutbound <> "0" Then
                    If Len(strRet) > 0 Then
                        strRet = strRet & ","
                    End If
                    strRet = strRet & strAftUrnoOutbound
                End If
            Else
                strRet = strUAFT
            End If
        Else
            strRet = strUAFT
        End If
    Else
        strRet = strUAFT
    End If

    GetAftUrnosByJob = strRet
End Function

Public Function GetActualShiftTime() As String
    Dim strRet As String
    Dim strNow As String
    Dim i As Integer
    Dim blFound As Boolean
    Dim dateTmp As Date
    Dim strAVFR As String
    Dim strAVTO As String

    i = 0
    blFound = False
    strNow = Format(Now, "YYYYMMDDHHmmss")

    While i < TabDRR.GetLineCount And blFound = False
        If TabDRR.GetFieldValue(i, "ROSS") = "A" Then
            strAVFR = TabDRR.GetFieldValue(i, "AVFR")
            strAVTO = TabDRR.GetFieldValue(i, "AVTO")
            If ((strAVFR < strNow And strAVTO > strNow) Or (strAVFR > strNow)) Then
                blFound = True
                strRet = Mid(strAVFR, 9, 2) & ":" & Mid(strAVFR, 11, 2) & "/" & Mid(strAVFR, 7, 2) & " - "
                strRet = strRet & Mid(strAVTO, 9, 2) & ":" & Mid(strAVTO, 11, 2) & "/" & Mid(strAVTO, 7, 2)
            End If
        End If
        i = i + 1
    Wend
    GetActualShiftTime = strRet
End Function

Public Function IsFreeOrSleepday(sOdaSdac As String) As Boolean
    Dim strLineNo As String
    Dim blRet As Boolean

    blRet = False
    strLineNo = TabODA.GetLinesByIndexValue("SDAC", sOdaSdac, 0)

    If IsNumeric(strLineNo) = True Then
        Dim strOdaFree As String
        Dim strOdaType As String
        strOdaFree = TabODA.GetFieldValue(CLng(strLineNo), "FREE")
        strOdaType = TabODA.GetFieldValue(CLng(strLineNo), "TYPE")
        If UCase(strOdaFree) = "X" Or UCase(strOdaType) = "S" Then
            blRet = True
        End If
    End If

    IsFreeOrSleepday = blRet
End Function

Public Function GetShiftData(sDrrUrno As String) As String
    Dim strLineNo As String
    Dim strRet As String
    Dim strTmp As String
    Dim strSday As String
    Dim strScod As String

    strLineNo = TabDRR.GetLinesByIndexValue("URNO", sDrrUrno, 0)

    If IsNumeric(strLineNo) = True Then
        ' add the date
        strTmp = TabDRR.GetFieldValue(CLng(strLineNo), "SDAY")
        strSday = strTmp
        If Len(strTmp) > 7 Then
            strRet = strRet & Mid(strTmp, 7, 2) & "-" & Mid(strTmp, 5, 2) & "-" & Left(strTmp, 4)
        End If
        strRet = strRet & ","

        
        strScod = TabDRR.GetFieldValue(CLng(strLineNo), "SCOD")
        
        If Len(Trim(strScod)) > 0 Then
            ' add the shift code
            strRet = strRet & strScod & ","

            ' add the shift times
            strTmp = TabDRR.GetFieldValue(CLng(strLineNo), "AVFR")
            If Len(strTmp) > 13 Then
                strRet = strRet & Mid(strTmp, 9, 2) & ":" & Mid(strTmp, 11, 2) & "/" & Mid(strTmp, 7, 2)
            End If
            strRet = strRet & "-"
            strTmp = TabDRR.GetFieldValue(CLng(strLineNo), "AVTO")
            If Len(strTmp) > 13 Then
                strRet = strRet & Mid(strTmp, 9, 2) & ":" & Mid(strTmp, 11, 2) & "/" & Mid(strTmp, 7, 2)
            End If
            strRet = strRet & ","
            
            strTmp = TabODA.GetLinesByIndexValue("SDAC", strScod, 0)
            If IsNumeric(strTmp) = True Then
                ' it is an absence -> no function, team
                 strRet = strRet & "-,-,"
            Else
                ' add the function
                strRet = strRet & TabDRR.GetFieldValue(CLng(strLineNo), "FCTC") & ","
        
                ' add the team
                strRet = strRet & GetTeamCode(strSday) & ","
            End If
    
            'add the remark
            strRet = strRet & TabDRR.GetFieldValue(CLng(strLineNo), "REMA")
        Else
            ' shift is "deleted"
            strRet = strRet & "-,-,-,-,"
        End If
    Else
        strRet = ",,,,,"
    End If

    GetShiftData = strRet
End Function

' getting the working group
Public Function GetTeamCode(sDay As String) As String
    Dim strRet As String
    Dim strLineNo As String

    strLineNo = TabDRG.GetLinesByIndexValue("SDAY", sDay, 0)
    If IsNumeric(strLineNo) = True Then
        strRet = TabDRG.GetFieldValue(CLng(strLineNo), "WGPC")
    Else
        strRet = GetBestFitRecord(TabSWG, strStfUrno, "SURN", "CODE", sDay)
    End If

    GetTeamCode = strRet
End Function

Public Function GetTeamCodeByJob(sJobUrno As String) As String
    Dim strLineNo As String
    Dim strUdsr As String
    Dim strSday As String
    Dim strRet As String

    strRet = ""

    strLineNo = TabJOB.GetLinesByIndexValue("URNO", sJobUrno, 0)
    If IsNumeric(strLineNo) = True Then
        strUdsr = TabJOB.GetFieldValue(CLng(strLineNo), "UDSR")
        strLineNo = TabDRR.GetLinesByIndexValue("URNO", strUdsr, 0)
        If IsNumeric(strLineNo) = True Then
            strSday = TabDRR.GetFieldValue(CLng(strLineNo), "SDAY")
            strRet = GetTeamCode(strSday)
        End If
    End If

    GetTeamCodeByJob = strRet
End Function

Public Function GetRecallDays() As String
    Dim strRet As String
    Dim strDrrOabs As String
    Dim strDrrScod As String
    Dim strDrrSday As String
    Dim strLineNo As String
    Dim i As Integer

    For i = 0 To TabDRR.GetLineCount - 1
        strDrrOabs = TabDRR.GetFieldValue(i, "OABS")
        If Len(strDrrOabs) > 0 Then
            strDrrScod = TabDRR.GetFieldValue(i, "SCOD")
            strLineNo = TabODA.GetLinesByIndexValue("SDAC", strDrrScod, 0)
            If strLineNo = "" And strDrrScod <> "" Then
                strDrrSday = TabDRR.GetFieldValue(i, "SDAY")
                If Len(strDrrSday) > 7 Then
                    If Len(strRet) > 0 Then
                        strRet = strRet & ","
                    End If
                    strRet = strRet & Mid(strDrrSday, 7, 2) & "-" & Mid(strDrrSday, 5, 2) & "-" & Left(strDrrSday, 4)
                End If
            End If
        End If
    Next i
    GetRecallDays = strRet
End Function

Public Function GetJobFlightRemark(strAftUrnos As String) As String
    Dim strRet As String
    Dim i As Integer
    Dim ilCnt As Integer
    Dim blArrDep As Boolean
    Dim strTmp As String

    blArrDep = False
    strRet = ""

    ' get the urnos of the flight(s) belonging to this job
    'strAftUrnos = GetAftUrnosByJob(sJobUrno)
    ilCnt = ItemCount(strAftUrnos, ",")
    If ilCnt > 0 Then
        Dim strLineNo As String
        Dim ilIdx  As Integer

        ilIdx = GetItemNo(TabREM.LogicalFieldList, "RURN") - 1

        If ilCnt > 1 Then
            blArrDep = True 'this job has an inbound and an outbound flight
        End If

        For i = 1 To ilCnt Step 1
            strLineNo = TabREM.GetLinesByIndexValue("RURN", GetItem(strAftUrnos, i, ","), 0)
            If IsNumeric(strLineNo) = True Then
                strTmp = CleanString(TabREM.GetFieldValue(CLng(strLineNo), "TEXT"), FOR_CLIENT, True)
                If blArrDep = True And i = 1 And Len(strTmp) > 0 Then
                    strRet = strRet & "ARR: "
                ElseIf blArrDep = True And i = 2 And Len(strTmp) > 0 Then
                    If Len(strRet) > 0 Then
                        strRet = strRet & ", "
                    End If
                    strRet = strRet & "DEP: "
                End If
                strRet = strRet & strTmp
            End If
        Next i
    End If

    GetJobFlightRemark = strRet
End Function

Private Function GetAftGate(ByRef sAftUrno As String) As String
    'GTA1,GTD1
    Dim strLineNo As String
    Dim strRet As String

    strLineNo = frmData.TabAFT.GetLinesByIndexValue("URNO", sAftUrno, 0)
    If IsNumeric(strLineNo) = True Then
        Dim strADID As String
        Dim strGTA1 As String
        Dim strGTD1 As String

        strADID = TabAFT.GetFieldValue(CLng(strLineNo), "ADID")

        If strADID = "A" Or strADID = "B" Then
            strGTA1 = TabAFT.GetFieldValue(CLng(strLineNo), "GTA1")
        End If

        If strADID = "D" Or strADID = "B" Then
            strGTD1 = TabAFT.GetFieldValue(CLng(strLineNo), "GTD1")
        End If

        If Len(strGTA1) > 0 And Len(strGTD1) > 0 Then
            strRet = strGTA1 & "/" & strGTD1
        Else
            If Len(strGTA1) > 0 Then
                strRet = strGTA1
            ElseIf Len(strGTD1) > 0 Then
                strRet = strGTD1
            End If
        End If
    End If
    GetAftGate = strRet
End Function

'-----------------------------------------------------------------------
'MWO: Creates the joblist for the rotation of the Job with urno as parameter
' returns the 2 (or one) AFT urnos as comma delimited string
'-----------------------------------------------------------------------
Public Function CreateFlightInfoJobs(strJobUrno As String) As String
    Dim strRet As String
    Dim llLine As Long
    Dim llLineEqu As Long
    Dim strRkey As String
    Dim strUrno As String
    Dim strStfUrno As String
    Dim strEquUrno As String
    Dim strAftUrnos As String
    Dim strAftUrNo As String
    Dim strServiceName As String
    Dim llItemNo As Long
    Dim llTabLine As Long
    Dim strTeam As String
    Dim strEmp As String
    Dim strFunc As String
    Dim strFlightInfoEmployee As String
    Dim i As Long
    Dim j As Long
    
    strAftUrnos = GetAftUrnosByJob(strJobUrno)
'MWO TO DO: Read all jobs for this strAftUrnos into a tmp tab and walk through this tab
'           instead of TabJOB, because TabJOB contains all job for the current employee
'           and not all jobs for the flight rotation
'           ==> Should read with WHERE UAFT in (strAftUrnos)
    
    
    TabFlightInfo.ResetContent
    'kkh on 20/11/2008 refresh TabJOB base on the Flight URNO selected
    'TabFlightInfo.HeaderString = "URNO,TYPE,SERV,TEAM,FUNC,EMPL,ACFR,ACTO"
    'igu on 26/05/2009 add one column for sorting purpose
    'TabFlightInfo.HeaderString = "URNO,TYPE,SERV,TEAM,FUNC,EMPL,EQIP,ACFR,ACTO"
    TabFlightInfo.HeaderString = "URNO,TYPE,SERV,TEAM,FUNC,EMPL,EQIP,ACFR,ACTO,SORTCOL"
    
    TabFlightInfo.LogicalFieldList = TabFlightInfo.HeaderString
    'kkh on 20/11/2008 refresh TabJOB base on the Flight URNO selected
    'igu on 26/05/2009 add one column for sorting purpose
    'TabFlightInfo.ColumnWidthString = "10,4,32,10,10,32,32,14,14"
    TabFlightInfo.ColumnWidthString = "10,4,32,10,10,32,32,14,14,14"
    TabFlightInfo.EnableHeaderSizing True
    TabFlightInfo.ShowHorzScroller True

    'First get the URNOs of the AFT Rotation and store it in strAftUrnos
    TabJOB.SetInternalLineBuffer True
    TabAFT.SetInternalLineBuffer True
    TabSTF.SetInternalLineBuffer True
    
    
'    For i = 0 To ItemCount(strAftUrnos, ",") - 1
'        strAftUrno = GetRealItem(strAftUrnos, i, ",")
        For j = 0 To TabJOB.GetLineCount - 1
'        strRet = TabJOB.GetLinesByIndexValue("UAFT", strAftUrno, 0)
'        llLine = TabJOB.GetNextResultLine
'        While llLine > -1
'        If TabJOB.GetFieldValue(j, "UAFT") = strAftUrno Then
            TabFlightInfo.InsertTextLine ",,,,,,,,", False
            llTabLine = TabFlightInfo.GetLineCount - 1
            TabFlightInfo.SetFieldValues llTabLine, "URNO", TabJOB.GetFieldValue(j, "URNO")
            TabFlightInfo.SetFieldValues llTabLine, "TYPE", GetJobType(TabJOB.GetFieldValue(j, "URNO"))
            TabFlightInfo.SetFieldValues llTabLine, "EMPL", TabJOB.GetFieldValue(j, "USTF")
'kkh on     '20/11/2008 refresh TabJOB base on the Flight URNO selected
            TabFlightInfo.SetFieldValues llTabLine, "EQIP", TabJOB.GetFieldValue(j, "UEQU")
            TabFlightInfo.SetFieldValues llTabLine, "ACFR", GetJobTimes(TabJOB.GetFieldValue(j, "URNO")) 'TabJOB.GetFieldValue(j, "ACFR")
            'TabFlightInfo.SetFieldValues llTabLine, "ACTO", TabJOB.GetFieldValue(j, "ACTO")
            llLine = TabJOB.GetNextResultLine
'        End If
'        Wend
        Next j
'    Next i
    TabFlightInfo.Sort "5", True, True   ' Sort for ACFR = Job start

    TabJOB.SetInternalLineBuffer False
    
    strFlightInfoEmployee = GetFlightInfoEmployee()
    
    For i = 0 To TabFlightInfo.GetLineCount - 1
        strUrno = TabFlightInfo.GetFieldValue(i, "URNO")
        strServiceName = GetJobServiceName(strUrno)
        strFunc = GetJobFunction(strUrno)
        strStfUrno = TabFlightInfo.GetFieldValue(i, "EMPL")
        strRet = TabSTF.GetLinesByIndexValue("URNO", strStfUrno, 0)
        llLine = TabSTF.GetNextResultLine
        If llLine > -1 Then
            If (strFlightInfoEmployee = "PENO") Then
                strEmp = TabSTF.GetFieldValues(llLine, "FINM") + " " + TabSTF.GetFieldValues(llLine, "PENO")
            Else
                strEmp = TabSTF.GetFieldValues(llLine, "FINM") + " " + GetJobRemarks(strUrno)
            End If
        'kkh on 20/11/2008 refresh TabJOB base on the Flight URNO selected
        Else
            strEquUrno = TabFlightInfo.GetFieldValue(i, "EQIP")
            strRet = TabEQU.GetLinesByIndexValue("URNO", strEquUrno, 0)
            If strRet > "-1" Then
                strEmp = TabEQU.GetFieldValues(strRet, "ENAM")
            End If
        End If
        strTeam = GetStaffTeam(strUrno, strStfUrno)
        If strTeam = "" Then
            strTeam = GetTeamCodeByJob(strUrno)
        End If
        TabFlightInfo.SetFieldValues i, "TEAM,SERV,EMPL,FUNC", strTeam + "," + strServiceName + "," + strEmp + "," + strFunc
        
        'igu on 26/05/2009 set sorting column's value
        TabFlightInfo.SetFieldValues i, "SORTCOL", _
            IIf(strFunc = "PRM*", "1", "0") & _
            TabJOB.GetFieldValue(i, "ACFR")
    Next i
    'igu on 26/05/2009 sort tab by sorting column
    TabFlightInfo.Sort CStr(TabFlightInfo.GetColumnCount - 1), True, True
    
    TabAFT.SetInternalLineBuffer False
    TabSTF.SetInternalLineBuffer False
    CreateFlightInfoJobs = strAftUrnos
End Function

Public Function GetStaffTeam(sJobUrno As String, sStfUrno) As String
    Dim strRet As String
    Dim llLine As Long
    Dim blFound As Boolean
    Dim strUJTY As String
    Dim strUJOB As String
    Dim strUSTF As String
    Dim strWGPC As String
    Dim strJour As String
    Dim strSday As String

    blFound = False

    'TabSWG.SetInternalLineBuffer True
    TabPoolJOB.SetInternalLineBuffer True
    TabDLG.SetInternalLineBuffer True
    TabDRG.SetInternalLineBuffer True
    TabJOB.SetInternalLineBuffer True
    
    strUSTF = sStfUrno

    strRet = TabJOB.GetLinesByIndexValue("URNO", sJobUrno, 0)
    llLine = TabJOB.GetNextResultLine
    If llLine > -1 Then
        ' getting the SDAY
        Dim olSday As Date
        strSday = TabJOB.GetFieldValue(llLine, "ACFR")
        If Len(strSday) > 0 Then
            olSday = CedaFullDateToVb(strSday) + (sglUTCOffsetHours / 24)
            'olSday = DateAdd("h", sglUTCOffsetHours, CedaFullDateToVb(strSday))
            strSday = Format(olSday, "YYYYMMDD")
        End If

        strJour = TabJOB.GetFieldValue(llLine, "JOUR")
        strRet = TabPoolJOB.GetLinesByIndexValue("URNO", strJour, 0)
        llLine = TabPoolJOB.GetNextResultLine
        If llLine > -1 Then
            strUJTY = TabPoolJOB.GetFieldValue(llLine, "UJTY")
            If strUJTY = "2007" Then
                strUJOB = TabPoolJOB.GetFieldValue(llLine, "URNO")
                strRet = TabDLG.GetLinesByIndexValue("UJOB", strUJOB, 0)
                llLine = TabDLG.GetNextResultLine
                If llLine > -1 Then
                    strWGPC = TabDLG.GetFieldValue(llLine, "WGPC")
                    If strWGPC <> "" Then
                        blFound = True
                    End If
                End If
            End If
        End If
    End If
    
    'Search for Daily roster groups
    If blFound = False Then
        strRet = TabDRG.GetLinesByIndexValue("STFU", strUSTF, 0)
        llLine = TabDRG.GetNextResultLine
        If llLine > -1 Then
            strWGPC = TabDRG.GetFieldValue(llLine, "WGPC")
            If strWGPC <> "" And TabDRG.GetFieldValue(llLine, "SDAY") = strSday Then
                blFound = True
            End If
        End If
    End If
    
    If blFound = False Then
        strWGPC = GetBestFitRecord(frmData2.TabSWG, strUSTF, "SURN", "CODE", strSday)
'        strRet = TabSWG.GetLinesByIndexValue("SURN", strUSTF, 0)
'        llLine = TabSWG.GetNextResultLine
'        If llLine > -1 Then
'            strWGPC = TabSWG.GetFieldValue(llLine, "CODE")
'        Else
'            strWGPC = ""
'        End If
    End If
    GetStaffTeam = strWGPC
    TabSWG.SetInternalLineBuffer False
    TabPoolJOB.SetInternalLineBuffer False
    TabDLG.SetInternalLineBuffer False
    TabDRG.SetInternalLineBuffer False
    TabJOB.SetInternalLineBuffer False
End Function

Private Function GetFlightInfoEmployee() As String
    Dim strFlightInfoEmployee As String
    
    If (GetConfigEntry("FLIGHTINFOEMPLOYEE", strOrgUnit, strFlightInfoEmployee) = False) Then
        strFlightInfoEmployee = "PENO"
    End If
           
    GetFlightInfoEmployee = strFlightInfoEmployee
End Function

Private Function GetJobRemarks(strJobUrno As String) As String
    Dim strRet As String
    Dim llLine As Long
    Dim strJour As String
    
    TabJOB.SetInternalLineBuffer True
    strRet = TabJOB.GetLinesByIndexValue("URNO", strJobUrno, 0)
    llLine = TabJOB.GetNextResultLine
    If (llLine > -1) Then
        strJour = TabJOB.GetFieldValue(llLine, "JOUR")
    Else
        strJour = ""
    End If
    
    TabJOB.SetInternalLineBuffer False
    
    If strJour <> "" Then
        TabPoolJOB.SetInternalLineBuffer True
        strRet = TabPoolJOB.GetLinesByIndexValue("URNO", strJour, 0)
        llLine = TabPoolJOB.GetNextResultLine
        If (llLine > -1) Then
            GetJobRemarks = TabPoolJOB.GetFieldValue(llLine, "TEXT")
        Else
            GetJobRemarks = ""
        End If
        TabPoolJOB.SetInternalLineBuffer False
    End If
End Function


Private Sub TabFlightInfo_SendLButtonDblClick(ByVal LineNo As Long, ByVal ColNo As Long)
    TabFlightInfo.Left = 0
    TabFlightInfo.Top = 0
    TabFlightInfo.Width = Me.Width
    TabFlightInfo.Height = Me.Height
End Sub


