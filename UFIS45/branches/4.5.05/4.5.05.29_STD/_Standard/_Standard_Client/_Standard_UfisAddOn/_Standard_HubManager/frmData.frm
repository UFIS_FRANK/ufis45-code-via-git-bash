VERSION 5.00
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "TAB.ocx"
Object = "{7BDE1363-DF9A-4D96-A2A0-4E85E0191F0F}#1.0#0"; "AatLogin.ocx"
Object = "{A2F31E92-C74F-11D3-A251-00500437F607}#1.0#0"; "UfisCom.ocx"
Object = "{6FBA474E-43AC-11CE-9A0E-00AA0062BB4C}#1.0#0"; "SYSINFO.OCX"
Begin VB.Form frmData 
   Caption         =   "Hidden Data"
   ClientHeight    =   9405
   ClientLeft      =   165
   ClientTop       =   555
   ClientWidth     =   12420
   LinkTopic       =   "Form1"
   ScaleHeight     =   9405
   ScaleWidth      =   12420
   StartUpPosition =   3  'Windows Default
   Begin TABLib.TAB tabData 
      Height          =   1275
      Index           =   0
      Left            =   120
      TabIndex        =   0
      Tag             =   $"frmData.frx":0000
      Top             =   330
      Width           =   5055
      _Version        =   65536
      _ExtentX        =   8916
      _ExtentY        =   2249
      _StockProps     =   64
   End
   Begin TABLib.TAB tabData 
      Height          =   1275
      Index           =   3
      Left            =   120
      TabIndex        =   1
      Tag             =   "LOATAB;URNO,FLNU,FLNO,DSSN,RURN,TYPE,STYP,SSTP,SSST,APC3,VALU,ADDI,HOPO;"
      Top             =   5160
      Width           =   5055
      _Version        =   65536
      _ExtentX        =   8916
      _ExtentY        =   2249
      _StockProps     =   64
   End
   Begin AATLOGINLib.AatLogin ULogin 
      Height          =   825
      Left            =   6360
      TabIndex        =   4
      Top             =   240
      Width           =   1140
      _Version        =   65536
      _ExtentX        =   2011
      _ExtentY        =   1455
      _StockProps     =   0
   End
   Begin UFISCOMLib.UfisCom aUfis 
      Left            =   5400
      Top             =   240
      _Version        =   65536
      _ExtentX        =   1508
      _ExtentY        =   767
      _StockProps     =   0
   End
   Begin SysInfoLib.SysInfo SysInfo 
      Left            =   7680
      Top             =   240
      _ExtentX        =   1005
      _ExtentY        =   1005
      _Version        =   393216
   End
   Begin TABLib.TAB tabData 
      Height          =   1005
      Index           =   13
      Left            =   120
      TabIndex        =   5
      Tag             =   $"frmData.frx":00FC
      Top             =   2040
      Width           =   3210
      _Version        =   65536
      _ExtentX        =   5662
      _ExtentY        =   1773
      _StockProps     =   64
   End
   Begin TABLib.TAB tabData 
      Height          =   1275
      Index           =   5
      Left            =   120
      TabIndex        =   7
      Tag             =   "CONTAB;URNO,ALC2,ALC3,OAL2,OAL3,PCON,MCON,CCON,CSAM,PSAM,MSAM,CDAT,LSTU,USEC,USEU;ORDER BY ALC2,ALC3,CDAT"
      Top             =   3360
      Width           =   2535
      _Version        =   65536
      _ExtentX        =   4471
      _ExtentY        =   2249
      _StockProps     =   64
   End
   Begin TABLib.TAB tabSubLOA 
      Height          =   1095
      Left            =   5400
      TabIndex        =   9
      Top             =   1560
      Width           =   4935
      _Version        =   65536
      _ExtentX        =   8705
      _ExtentY        =   1931
      _StockProps     =   64
   End
   Begin TABLib.TAB tabHold 
      Height          =   1005
      Left            =   7200
      TabIndex        =   11
      Top             =   8040
      Width           =   2535
      _Version        =   65536
      _ExtentX        =   4471
      _ExtentY        =   1773
      _StockProps     =   64
   End
   Begin TABLib.TAB tabUrnos 
      Height          =   1395
      Left            =   7200
      TabIndex        =   13
      Top             =   6300
      Width           =   1695
      _Version        =   65536
      _ExtentX        =   2990
      _ExtentY        =   2461
      _StockProps     =   64
   End
   Begin TABLib.TAB tabReread 
      Height          =   975
      Left            =   7200
      TabIndex        =   15
      Top             =   5100
      Width           =   5055
      _Version        =   65536
      _ExtentX        =   8916
      _ExtentY        =   1720
      _StockProps     =   64
   End
   Begin VB.Label Label7 
      Caption         =   "tmp for reread of data"
      Height          =   315
      Left            =   7200
      TabIndex        =   16
      Top             =   4800
      Width           =   3435
   End
   Begin VB.Label Label9 
      Caption         =   "Next Urno"
      Height          =   195
      Left            =   7200
      TabIndex        =   14
      Top             =   6120
      Width           =   1695
   End
   Begin VB.Label Label10 
      Caption         =   "Hold-Flights"
      Height          =   255
      Left            =   7200
      TabIndex        =   12
      Top             =   7800
      Width           =   2535
   End
   Begin VB.Label lblRecCount 
      Caption         =   "Subset of LOATAB (ALTEA)"
      Height          =   255
      Index           =   14
      Left            =   5400
      TabIndex        =   10
      Top             =   1200
      Width           =   2835
   End
   Begin VB.Label lblRecCount 
      Caption         =   "CONTAB"
      Height          =   255
      Index           =   5
      Left            =   600
      TabIndex        =   8
      Top             =   3120
      Width           =   2055
   End
   Begin VB.Label lblRecCount 
      Caption         =   "CFITAB"
      Height          =   255
      Index           =   13
      Left            =   120
      TabIndex        =   6
      Top             =   1680
      Width           =   2055
   End
   Begin VB.Label lblRecCount 
      Caption         =   "AFTTAB"
      Height          =   255
      Index           =   0
      Left            =   120
      TabIndex        =   3
      Top             =   120
      Width           =   2835
   End
   Begin VB.Label lblRecCount 
      Caption         =   "LOATAB"
      Height          =   255
      Index           =   3
      Left            =   120
      TabIndex        =   2
      Top             =   4800
      Width           =   2835
   End
End
Attribute VB_Name = "frmData"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public Function GetNextUrno() As String
    Dim cnt As Long
    Dim ret As String
    Dim i As Long
    
    cnt = tabUrnos.GetLineCount
    If cnt = 0 Then
        SetServerParameters
        If aUfis.CallServer("GMU", "", "*", "200", "", "120") = 0 Then
            cnt = aUfis.GetBufferCount
            For i = 0 To cnt - 1
                tabUrnos.InsertBuffer aUfis.GetBufferLine(i), ","
            Next i
        End If
    End If
    ret = tabUrnos.GetFieldValue(0, "URNO")
    tabUrnos.DeleteLine 0
    tabUrnos.Refresh
    GetNextUrno = ret
End Function

Public Sub SetServerParameters()
    aUfis.HomeAirport = strHopo
    aUfis.TableExt = strTableExt
    aUfis.Module = "HUB-Manager" & "," & Me.GetApplVersion(True)
    aUfis.Twe = ""
    aUfis.Tws = "0"
    'aUfis.WorkStation = GetWorkStationName()
    aUfis.UserName = strCurrentUser
End Sub

Private Sub TabRunQueries(TabObject As TABLib.TAB, ByVal DBTable As String, ByVal DBFields As String, _
ByVal FieldName As String, colValues As Collection, Optional ByVal ExtraWhereClause As String = "")
    Dim strWhere As String
    Dim i As Integer
    
    For i = 1 To colValues.count
        strWhere = "WHERE " & FieldName & " IN (" & colValues.Item(i) & ")" & _
            " " & ExtraWhereClause
            
        TabObject.CedaAction "RT", DBTable, DBFields, "", strWhere
    Next i
End Sub

'igu on 27 Jul 2010
'generate value list collection for items in WHERE-IN clause
Private Function GenerateValueListCollection(TabObject As TABLib.TAB, _
ByVal FieldName As String, Optional ByVal Prefix As String = "", Optional Suffix As String = "", _
Optional ByVal MaxNumOfValuesPerClause As Long = 300) As Collection
    
    'Dim intCount As Integer 'igu on 03/02/2012
    Dim lngCount As Long 'igu on 03/02/2012
    Dim strValueList As String
    'Dim i As Integer, j As Integer 'igu on 03/02/2012
    Dim i As Integer, j As Long 'igu on 03/02/2012
    Dim colValues As Collection
    Dim intCollCount As Integer
    'Dim intStart As Integer, intEnd As Integer 'igu on 03/02/2012
    Dim lngStart As Long, lngEnd As Long 'igu on 03/02/2012
    
    Set colValues = New Collection
    
    lngCount = TabObject.GetLineCount
    
    intCollCount = CLng(lngCount / MaxNumOfValuesPerClause) + 1
    For i = 1 To intCollCount
        lngStart = MaxNumOfValuesPerClause * (i - 1)
        lngEnd = lngStart + (MaxNumOfValuesPerClause - 1)
        If lngEnd > lngCount - 1 Then
            lngEnd = lngCount - 1
        End If
    
        strValueList = ""
        For j = lngStart To lngEnd
            strValueList = strValueList & "," & Prefix & TabObject.GetFieldValue(j, FieldName) & Suffix
        Next j
        strValueList = Mid(strValueList, 2)
        
        colValues.Add strValueList
    Next i
    
    Set GenerateValueListCollection = colValues
End Function

Public Sub LoadData()
    Dim myTab As TABLib.TAB
    Dim ilTabCnt As Integer
    Dim arr() As String
    Dim arrFields() As String
    Dim ilCount As Integer
    Dim strWhere As String
    Dim i As Long
    Dim fName As String
    Dim strOldValue As String
    Dim ilIdx As Long
    
    Dim colUrnos As Collection  'igu on 27 Jul 2010
    
    frmLoad.lblStatus.Visible = True
    frmLoad.pbrProses.Visible = True
    frmLoad.pbrProses.Min = 0
    frmLoad.pbrProses.Max = tabData.count
    
    For Each myTab In tabData
        If myTab.Index <> 3 Then 'Not LOATAB
            myTab.ResetContent
            'DoEvents
            arr = Split(myTab.Tag, ";")
            If UBound(arr) = 2 Then
                arrFields = Split(arr(1), ",")
                strWhere = arr(2)
                myTab.ResetContent
                myTab.AutoSizeByHeader = True
                myTab.HeaderString = ""
                myTab.HeaderLengthString = ""
                myTab.HeaderString = arr(1)
                myTab.EnableInlineEdit True
                myTab.CedaServerName = strServer
                myTab.CedaPort = "3357"
                myTab.CedaHopo = strHopo
                myTab.CedaCurrentApplication = "HUB-Manager" & "," & GetApplVersion(True)
                myTab.CedaTabext = "TAB"
                myTab.CedaUser = strCurrentUser
                myTab.CedaWorkstation = aUfis.WorkStation
                myTab.CedaSendTimeout = "3"
                myTab.CedaReceiveTimeout = "240"
                myTab.CedaRecordSeparator = Chr(10)
                myTab.CedaIdentifier = "HUB-Manager"
                'myTab.ShowHorzScroller True
                myTab.ShowHorzScroller False
                myTab.EnableHeaderSizing True
                For i = 0 To UBound(arrFields)
                    If i = 0 Then
                        myTab.HeaderLengthString = "80,"
                    Else
                        myTab.HeaderLengthString = myTab.HeaderLengthString + "80,"
                    End If
                Next i
                myTab.LogicalFieldList = arr(1)
                
                frmLoad.pbrProses.Value = frmLoad.pbrProses.Value + 1
                frmLoad.lblStatus.Caption = "Loading " & arr(0) & " ..."
                frmLoad.lblStatus.Refresh
    'Special treatment for AFTTAB
                If arr(0) = "AFTTAB" Then
                    strWhere = "WHERE (((TIFA BETWEEN '" + strTimeFrameFrom + "' AND '" + strTimeFrameTo + "') AND ADID IN ('A','B')) OR " + _
                        "((TIFD BETWEEN '" + strTimeFrameFrom + "' AND '" + strTimeFrameTo + "') AND ADID IN ('D','B'))) AND " + _
                        "FTYP IN ('S', 'O', 'X') AND ALC2 IN ('SQ','MI') " 'igu on 10/01/2012
                    'strWhere = "WHERE ((TIFA BETWEEN '" + strTimeFrameFrom + "' AND '" + strTimeFrameTo + "') AND FTYP IN ('S', 'O', 'X') AND ADID IN ('A','B') OR (TIFD BETWEEN '" + strTimeFrameFrom + "' and '" + strTimeFrameTo + "') AND FTYP IN ('S', 'O', 'X') AND ADID IN ('D','B')) AND ALC2 IN ('SQ','MI')" 'igu on 10/01/2012
                End If
    'SRLTAB Handling
                If arr(0) = "SRLTAB" Then
                    myTab.ColumnWidthString = "10,10,1,4,1,13,14,14,5,1,11,12,14,14,32,32"
    '                    strWhere = "WHERE FLNU IN ("
    '                    For i = 0 To tabData(0).GetLineCount - 1
    '                        If i Mod 300 = 0 Then
    '                            strWhere = strWhere + tabData(0).GetFieldValue(i, "URNO") + ") AND USEC='HUBMGR'"
    '                            myTab.CedaAction "RT", arr(0), arr(1), "", strWhere
    '                            frmStartup.txtCurrentStatus = "Loading " & arr(0) & " ..."
    '                            frmStartup.txtCurrentStatus.Refresh
    '                            frmStartup.txtDoneStatus = arr(0) & " : " & CStr(myTab.GetLineCount) & " records loaded"
    '                            frmStartup.txtDoneStatus.Refresh
    '                            strWhere = "WHERE FLNU IN ("
    '                        Else
    '                            If (i) = tabData(0).GetLineCount - 1 Then
    '                                strWhere = strWhere + tabData(0).GetFieldValue(i, "URNO") + ") AND USEC='HUBMGR'"
    '                            Else
    '                                strWhere = strWhere + tabData(0).GetFieldValue(i, "URNO") + ","
    '                            End If
    '                        End If
    '                    Next i
    '                    myTab.CedaAction "RT", arr(0), arr(1), "", strWhere
                    Call TabRunQueries(myTab, arr(0), arr(1), "FLNU", colUrnos, _
                        "AND USEC='HUBMGR'") 'igu on 27 Jul 2010
                    
                    frmLoad.pbrProses.Value = frmLoad.pbrProses.Value + 1
                    frmLoad.lblStatus.Caption = "Loading " & arr(0) & " ..."
                    frmLoad.lblStatus.Refresh
                End If
    'kkh on 16/07/2008 P2 Store Decision into CFITAB
    'CFITAB Handling
                If arr(0) = "CFITAB" Then
                    'myTab.ColumnWidthString = "10,10,10,10,10,10,10,10,10,10,11"
                    myTab.ColumnWidthString = "10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,11"
    '                    strWhere = "WHERE AURN IN ("
    '                    For i = 0 To tabData(0).GetLineCount - 1
    '                        If i Mod 300 = 0 Then
    '                            strWhere = strWhere + tabData(0).GetFieldValue(i, "URNO") + ") AND TYPE='HUB'"
    '                            myTab.CedaAction "RT", arr(0), arr(1), "", strWhere
    '                            frmStartup.txtCurrentStatus = "Loading " & arr(0) & " ..."
    '                            frmStartup.txtCurrentStatus.Refresh
    '                            frmStartup.txtDoneStatus = arr(0) & " : " & CStr(myTab.GetLineCount) & " records loaded"
    '                            frmStartup.txtDoneStatus.Refresh
    '                            strWhere = "WHERE AURN IN ("
    '                        Else
    '                            If (i) = tabData(0).GetLineCount - 1 Then
    '                                strWhere = strWhere + tabData(0).GetFieldValue(i, "URNO") + ") AND TYPE='HUB'"
    '                            Else
    '                                strWhere = strWhere + tabData(0).GetFieldValue(i, "URNO") + ","
    '                            End If
    '                        End If
    '                    Next i
    '                    myTab.CedaAction "RT", arr(0), arr(1), "", strWhere
                    Call TabRunQueries(myTab, arr(0), arr(1), "AURN", colUrnos, _
                        "AND TYPE='HUB'") 'igu on 27 Jul 2010
                End If
    'Customize for GOCC ON 04/03/2008
    'AVLTAB Handling
                If (arr(0) = "AVLTAB") Then
                    myTab.ColumnWidthString = "10,10,11,11,11,11,11,11,11,11"
    '                    strWhere = "WHERE RURN IN ("
    '                    For i = 0 To tabData(0).GetLineCount - 1
    '                        If i Mod 300 = 0 Then
    '                            strWhere = strWhere + tabData(0).GetFieldValue(i, "URNO") + ")"
    '                            myTab.CedaAction "RT", arr(0), arr(1), "", strWhere
    '                            frmStartup.txtCurrentStatus = "Loading " & arr(0) & " ..."
    '                            frmStartup.txtCurrentStatus.Refresh
    '                            frmStartup.txtDoneStatus = arr(0) & " : " & CStr(myTab.GetLineCount) & " records loaded"
    '                            frmStartup.txtDoneStatus.Refresh
    '                            strWhere = "WHERE RURN IN ("
    '                        Else
    '                            If (i) = tabData(0).GetLineCount - 1 Then
    '                                strWhere = strWhere + tabData(0).GetFieldValue(i, "URNO") + ")"
    '                            Else
    '                                strWhere = strWhere + tabData(0).GetFieldValue(i, "URNO") + ","
    '                            End If
    '                        End If
    '                    Next i
    '                    myTab.CedaAction "RT", arr(0), arr(1), "", strWhere
                    Call TabRunQueries(myTab, arr(0), arr(1), "RURN", colUrnos) 'igu on 27 Jul 2010
                End If
    
    'Special treatment for LOADTAB
                If arr(0) = "LOATAB" Then
                    tabData(3).ColumnWidthString = "10,10,10,10,10,10,10,10,10,10,10,10,10"
    '                    strWhere = "WHERE FLNU IN ("
    '                    For i = 0 To tabData(0).GetLineCount - 1
    '                        If i Mod 300 = 0 Then
    '                            'strWhere = strWhere + tabData(0).GetFieldValue(i, "URNO") + ")" + _
    '                                       " AND DSSN IN ('PTM','KRI') AND TYPE IN ('PAX','PXB','BAG','BWT') AND STYP IN ('E','B','F') " + _
    '                                       " AND SSST=' ' AND (SSTP='TD' OR SSTP=' ' OR SSTP='T')"
    '                            strWhere = strWhere + tabData(0).GetFieldValue(i, "URNO") + ")" + _
    '                            " AND DSSN IN ('PTM','KRI') AND TYPE IN ('PAX','PXB','BAG','BWT') AND STYP IN ('E','B','F','U') " + _
    '                            " AND SSST=' ' AND (SSTP='TD' OR SSTP=' ' OR SSTP='T') AND FLNO <> ' ' order by APC3, ADDI"
    '
    '                            myTab.CedaAction "RT", arr(0), arr(1), "", strWhere
    '                            frmStartup.txtCurrentStatus = "Loading " & arr(0) & " ..."
    '                            frmStartup.txtCurrentStatus.Refresh
    '                            frmStartup.txtDoneStatus = arr(0) & " : " & CStr(myTab.GetLineCount) & " records loaded"
    '                            frmStartup.txtDoneStatus.Refresh
    '                            strWhere = "WHERE FLNU IN ("
    '                        Else
    '                            If (i) = tabData(0).GetLineCount - 1 Then
    '                                'strWhere = strWhere + tabData(0).GetFieldValue(i, "URNO") + ")" + _
    '                                           " AND DSSN IN ('PTM','KRI') AND TYPE IN ('PAX','PXB','BAG','BWT') AND STYP IN ('E','B','F') " + _
    '                                           " AND SSST=' ' AND (SSTP='TD' OR SSTP=' ' OR SSTP='T')"
    '                                strWhere = strWhere + tabData(0).GetFieldValue(i, "URNO") + ")" + _
    '                                           " AND DSSN IN ('PTM','KRI') AND TYPE IN ('PAX','PXB','BAG','BWT') AND STYP IN ('E','B','F','U') " + _
    '                                           " AND SSST=' ' AND (SSTP='TD' OR SSTP=' ' OR SSTP='T') AND FLNO <> ' ' order by APC3, ADDI"
    '                            Else
    '                                strWhere = strWhere + tabData(0).GetFieldValue(i, "URNO") + ","
    '                            End If
    '                        End If
    '                    Next i
    '                    myTab.CedaAction "RT", arr(0), arr(1), "", strWhere
                    'Call TabRunQueries(myTab, arr(0), arr(1), "FLNU", colUrnos, _
                        "AND DSSN IN ('PTM','KRI') AND TYPE IN ('PAX','PXB','BAG','BWT') AND STYP IN ('E','B','F') " & _
                        " AND SSST=' ' AND (SSTP='TD' OR SSTP=' ' OR SSTP='T')")  'igu on 27 Jul 2010 'igu on 23/08/2011: ALTEA
                    Call TabRunQueries(myTab, arr(0), arr(1), "FLNU", colUrnos, _
                        "AND DSSN IN ('PTM','KRI','ALT') AND TYPE IN ('PAX','PXB','BAG','BWT') AND STYP IN ('E','B','F') " & _
                        " AND SSST=' ' AND (SSTP='TD' OR SSTP=' ' OR SSTP='T')") 'igu on 23/08/2011: ALTEA
                End If
    'LOATAB with Mail
                If arr(0) = "LOATABM" Then
                    arr(0) = "LOATAB"
    '                    strWhere = "WHERE FLNU IN ("
    '                    For i = 0 To tabData(0).GetLineCount - 1
    '                        If i Mod 300 = 0 Then
    '                            strWhere = strWhere + tabData(0).GetFieldValue(i, "URNO") + ")" + _
    '                                       " AND DSSN='HUB' AND TYPE='LOA' AND STYP='M' "
    '                            myTab.CedaAction "RT", arr(0), arr(1), "", strWhere
    '                            strWhere = "WHERE FLNU IN ("
    '                        Else
    '                            If (i) = tabData(0).GetLineCount - 1 Then
    '                                strWhere = strWhere + tabData(0).GetFieldValue(i, "URNO") + ")" + _
    '                                           " AND DSSN='HUB' AND TYPE='LOA' AND STYP='M'"
    '                            Else
    '                                strWhere = strWhere + tabData(0).GetFieldValue(i, "URNO") + ","
    '                            End If
    '                        End If
    '                    Next i
    '                    myTab.CedaAction "RT", arr(0), arr(1), "", strWhere
                    Call TabRunQueries(myTab, arr(0), arr(1), "FLNU", colUrnos, _
                        "AND DSSN='HUB' AND TYPE='LOA' AND STYP='M'") 'igu on 27 Jul 2010
                End If
    'LOATAB with Cargo
                If arr(0) = "LOATABC" Then
                    arr(0) = "LOATAB"
    '                    strWhere = "WHERE FLNU IN ("
    '                    For i = 0 To tabData(0).GetLineCount - 1
    '                        If i Mod 300 = 0 Then
    '                            strWhere = strWhere + tabData(0).GetFieldValue(i, "URNO") + ")" + _
    '                                       " AND DSSN='HUB' AND TYPE='LOA' AND STYP='C'"
    '                            myTab.CedaAction "RT", arr(0), arr(1), "", strWhere
    '                            strWhere = "WHERE FLNU IN ("
    '                        Else
    '                            If (i) = tabData(0).GetLineCount - 1 Then
    '                                strWhere = strWhere + tabData(0).GetFieldValue(i, "URNO") + ")" + _
    '                                           " AND DSSN='HUB' AND TYPE='LOA' AND STYP='C'"
    '                            Else
    '                                strWhere = strWhere + tabData(0).GetFieldValue(i, "URNO") + ","
    '                            End If
    '                        End If
    '                    Next i
    '                    myTab.CedaAction "RT", arr(0), arr(1), "", strWhere
                    Call TabRunQueries(myTab, arr(0), arr(1), "FLNU", colUrnos, _
                        "AND DSSN='HUB' AND TYPE='LOA' AND STYP='C'") 'igu on 27 Jul 2010
                End If
                'kkh on 16/07/2008 P2 Store Decision into CFITAB
                If arr(0) <> "ROTATIONS" And arr(0) <> "LOATAB" And _
                   arr(0) <> "LOATABM" And arr(0) <> "LOATABC" And arr(0) <> "AVLTAB" And arr(0) <> "CFITAB" Then
                    myTab.CedaAction "RT", arr(0), arr(1), "", strWhere
                End If
                myTab.RedrawTab
                
                ilTabCnt = ilTabCnt + 1
                myTab.AutoSizeByHeader = True
                myTab.AutoSizeColumns
                myTab.myTag = myTab.Left & "," & myTab.Top & "," & myTab.Height & "," & myTab.Width
                
                'igu on 27 Jul 2010
                'generate the urnos collection
                '-----------------------------------------
                If colUrnos Is Nothing Then
                    Set colUrnos = GenerateValueListCollection(tabData(0), "URNO")
                End If
                '-----------------------------------------
            End If
        End If
    Next
        
'    tabCfg.ResetContent
'    tabCfg.HeaderLengthString = "100,100"
'    tabCfg.HeaderString = "FIELD,VALUE"
'    tabCfg.LogicalFieldList = tabCfg.HeaderString
'    tabCfg.EnableHeaderSizing True
'    tabCfg.AutoSizeByHeader = True
'    'tabCfg.readFromFile "C:\Ufis\Appl\HUBMANAGER.cfg"
'    tabCfg.readFromFile UFIS_APPL & "\HUBMANAGER.cfg"
'    tabCfg.AutoSizeColumns

    tabHold.ResetContent
    tabHold.HeaderLengthString = "100,100"
    tabHold.HeaderString = "URNO,STATUS"
    tabHold.LogicalFieldList = tabHold.HeaderString
    tabHold.EnableHeaderSizing True
    tabHold.AutoSizeByHeader = True
    'tabHold.readFromFile "C:\Ufis\Appl\HUB_FLT_HOLD.cfg"
    tabHold.ReadFromFile UFIS_APPL & "\HUB_FLT_HOLD.cfg"
    tabHold.AutoSizeColumns
    tabHold.SetInternalLineBuffer True
    tabHold.IndexCreate "URNO", 0
    'kkh on 28/11/2008 Display KRI as higher priority then PTM message for all flights, take out the sort
    tabData(0).Sort "1,3", True, True
'    tabData(1).Sort "1", True, True
'    tabData(2).ResetContent
    tabData(0).IndexCreate "URNO", 0
    tabData(0).IndexCreate "RKEY", 1
    tabData(0).IndexCreate "FLNO", 4
    ilIdx = GetRealItemNo(tabData(0).LogicalFieldList, "DES3")
    tabData(0).IndexCreate "DES3", ilIdx
    'kkh 31/07/2008 VIAL
    tabData(0).IndexCreate "VIAL", 38
    tabData(0).SetInternalLineBuffer True
'    'kkh on 28/11/2008 Display KRI as higher priority then PTM message for all flights, take out the sort
'    tabData(3).Sort "1,2,3", True, True
'    'tabData(3).Sort "14,3", True, True
'    tabData(3).IndexCreate "URNO", 0
'    tabData(3).IndexCreate "FLNU", 1
'    tabData(3).IndexCreate "RTAB", 12
'    tabData(3).SetInternalLineBuffer True
'    tabData(4).SetInternalLineBuffer True
'    tabData(4).Sort "0,1", True, True
'    For i = 0 To tabData(4).GetLineCount - 1
'        tabData(4).SetFieldValues i, "PRFL", ""
'    Next i
'    tabData(4).IndexCreate "ALC2", 0
'    tabData(4).IndexCreate "ALC3", 1
    tabUrnos.ResetContent
    tabUrnos.HeaderString = "URNO"
    tabUrnos.LogicalFieldList = "URNO"
    tabUrnos.HeaderLengthString = "100"
    tabUrnos.SetFieldSeparator Chr(15)


'    tabReread.ResetContent
'    tabReread.AutoSizeByHeader = True
'    tabReread.HeaderString = ""
'    tabReread.HeaderLengthString = ""
'    tabReread.HeaderString = arr(1)
'    tabReread.EnableInlineEdit True
'    tabReread.CedaServerName = strServer
'    tabReread.CedaPort = "3357"
'    tabReread.CedaHopo = strHopo
'    tabReread.CedaCurrentApplication = "HUB-Manager" & "," & GetApplVersion(True)
'    tabReread.CedaTabext = "TAB"
'    tabReread.CedaUser = strCurrentUser '"HUBMNG"
'    tabReread.CedaWorkstation = ULogin.GetWorkStationName  'aUfis.GetWorkStationName ' "wks104"
'    tabReread.CedaSendTimeout = "3"
'    tabReread.CedaReceiveTimeout = "240"
'    tabReread.CedaRecordSeparator = Chr(10)
'    tabReread.CedaIdentifier = "HUB-Manager"
'    'tabReread.ShowHorzScroller True
'    tabReread.ShowHorzScroller False
'    tabReread.EnableHeaderSizing True
'
'
'    'MakeRotationData
'    tabData(2).AutoSizeByHeader = True
'    tabData(2).AutoSizeColumns
'    tabCompressedView.myTag = tabCompressedView.Left & "," & tabCompressedView.Top & "," & tabCompressedView.Height & "," & tabCompressedView.Width
    tabData(5).SetInternalLineBuffer True
    tabData(5).IndexCreate "URNO", 0
    tabData(5).IndexCreate "ALC2", 1
    tabData(5).IndexCreate "ALC3", 2
    tabData(5).Sort "1,2", True, True

'    tabData(8).IndexCreate "URNO", 0
'    tabData(8).IndexCreate "ACT3", 1
'    tabData(8).SetInternalLineBuffer True
'    tabData(8).Sort "1", True, True
'    tabData(9).IndexCreate "URNO", 0
'    tabData(9).IndexCreate "REGN", 1
'    tabData(9).Sort "1", True, True
'    tabData(9).SetInternalLineBuffer True

'    'make RFLD and RTAB in all TAB for LADTAB data empty
'    For i = 0 To tabData(3).GetLineCount - 1
'        tabData(3).SetFieldValues i, "RFLD,RTAB", ","
'    Next i
'    For i = 0 To tabData(6).GetLineCount - 1
'        tabData(6).SetFieldValues i, "RFLD,RTAB", ","
'    Next i
'    For i = 0 To tabData(7).GetLineCount - 1
'        tabData(7).SetFieldValues i, "RFLD,RTAB", ","
'    Next i
'    tabData(6).IndexCreate "URNO", 0
'    tabData(7).IndexCreate "URNO", 0
'    tabData(6).IndexCreate "FLNU", 1
'    tabData(7).IndexCreate "FLNU", 1
'    tabData(6).IndexCreate "IDNT", 7 ' Departure flight URNO is stored in this field
'    tabData(7).IndexCreate "IDNT", 7
'    tabData(6).SetInternalLineBuffer True
'    tabData(7).SetInternalLineBuffer True
'
'    'Special SRLTAB handling
'    'Detect double FLNUs and remove them except of the last one, which is the newest
'    tabData(10).Sort "1,12", False, True
'    tabData(10).SetInternalLineBuffer True
'    strOldValue = ""
'    For i = tabData(10).GetLineCount - 1 To 0 Step -1
'        If i - 1 >= 0 Then
'            strOldValue = tabData(10).GetFieldValue(i, "FLNU")
'            If strOldValue = tabData(10).GetFieldValue(i - 1, "FLNU") Then
'                tabData(10).DeleteLine (i)
'            End If
'        End If
'    Next
'    tabData(10).IndexCreate "FLNU", 1
'
'    'kkh AVLTAB
'    tabData(12).IndexCreate "RURN", 1
'
    'kkh CFITAB
    tabData(13).IndexCreate "URNO", 0
    tabData(13).IndexCreate "AURN", 1
    tabData(13).IndexCreate "DURN", 2

    If TimesInUTC = False Then
        frmLoad.pbrProses.Value = frmLoad.pbrProses.Value + 1
        frmLoad.lblStatus.Caption = "Converting UTC to local ..."
        frmLoad.lblStatus.Refresh
        ChageAftToLocal
    End If
End Sub

Public Sub ChageAftToLocal()
    Dim cnt As Long
    Dim fldCnt As Integer
    Dim i As Long
    Dim j As Long
    Dim strField As String
    Dim strVal As String
    Dim myDat As Date
    
    cnt = tabData(0).GetLineCount - 1
    fldCnt = ItemCount(strAftTimeFields, ",")
    For i = 0 To cnt
        For j = 0 To fldCnt
            strField = GetRealItem(strAftTimeFields, CInt(j), ",")
            strVal = tabData(0).GetFieldValue(i, strField)
            If Trim(strVal) <> "" Then
                myDat = CedaFullDateToVb(strVal)
                If TimesInUTC = False Then
                    myDat = DateAdd("h", UTCOffset, myDat)
                End If
                strVal = Format(myDat, "YYYYMMDDhhmmss")
                tabData(0).SetFieldValues i, strField, strVal
            End If
        Next j
    Next i
    tabData(0).Refresh
End Sub

Public Function GetUTCOffset() As Single
    Dim strUtcArr() As String
    Dim strArr() As String
    Dim CurrStr As String
    Dim tmpStr As String

    Dim istrRet As Integer
    Dim count As Integer
    Dim i As Long

    GetUTCOffset = 0
    aUfis.HomeAirport = strHopo
    aUfis.InitCom strServer, "CEDA"
    aUfis.Twe = strHopo + "," + strTableExt + "," + strAppl
    aUfis.SetCedaPerameters strAppl, strHopo, strTableExt
    SetServerParameters
    If aUfis.CallServer("GFR", "", "", "", "[CONFIG]", "360") = 0 Then
        tmpStr = aUfis.GetDataBuffer(True)
    End If
    strArr = Split(tmpStr, Chr(10))
    count = UBound(strArr)
    For i = 0 To count
        CurrStr = strArr(i)
        istrRet = InStr(1, CurrStr, "UTCD", 0)
        If istrRet <> 0 Then
            strUtcArr = Split(strArr(i), ",")
            i = count + 1
            GetUTCOffset = CSng(strUtcArr(1) / 60)
        End If
    Next i
    aUfis.CleanupCom
End Function

Public Function LoginProcedure() As Boolean
    Dim LoginAnsw As String
    Dim tmpRegStrg As String
    Dim strRet As String
    
    Set ULogin.UfisComCtrl = aUfis
    ULogin.ApplicationName = "HUB-Manager"
    tmpRegStrg = "HUB-Manager" & ","
    'FKTTAB:  SUBD,FUNC,FUAL,TYPE,STAT
    tmpRegStrg = tmpRegStrg & "InitModu,InitModu,Initialisieren (InitModu),B,-"
    
    'kkh on 09/07/2008 P2 Security Features for function/button
    'build the register string for BDPS-SEC
    tmpRegStrg = tmpRegStrg & ",General,Load,Load,B,1"
    tmpRegStrg = tmpRegStrg & ",General,Now,Now,B,1"
    tmpRegStrg = tmpRegStrg & ",General,Compressed_Flights,Compressed Flights,B,1"
    tmpRegStrg = tmpRegStrg & ",General,Hourly_Flights,Hourly Flights,B,1"
    tmpRegStrg = tmpRegStrg & ",General,Mail/Cargo,Mail/Cargo,B,1"
    tmpRegStrg = tmpRegStrg & ",General,Plain_Gantt,Plain Gantt,B,1"
    tmpRegStrg = tmpRegStrg & ",General,Filter,Filter,B,1"
    tmpRegStrg = tmpRegStrg & ",General,Gate,Gate,B,1"
    tmpRegStrg = tmpRegStrg & ",General,Connection_Times,Connection Times,B,1"
    tmpRegStrg = tmpRegStrg & ",General,Hourly_Pax,Hourly Pax,B,1"
    tmpRegStrg = tmpRegStrg & ",General,Setup,Setup,B,1"
    tmpRegStrg = tmpRegStrg & ",General,View_Log,View Log,B,1"
    
    tmpRegStrg = tmpRegStrg & ",General,Area_1,Area 1,B,1"
    tmpRegStrg = tmpRegStrg & ",General,Area_2,Area 2,B,1"
    tmpRegStrg = tmpRegStrg & ",General,Area_3,Area 3,B,1"
    tmpRegStrg = tmpRegStrg & ",General,Area_All,Area All,B,1"
    
    tmpRegStrg = tmpRegStrg & ",Screen 3,Telex,Telex,B,1"
    tmpRegStrg = tmpRegStrg & ",Screen 3,Print,Print,B,1"
    tmpRegStrg = tmpRegStrg & ",Screen 3,Insert,Insert,B,1"
    tmpRegStrg = tmpRegStrg & ",Screen 3,Delete,Delete,B,1"
    tmpRegStrg = tmpRegStrg & ",Screen 3,Send_Telex,Send Telex,B,1"
    tmpRegStrg = tmpRegStrg & ",Screen 3,Excel,Excel,B,1"
    tmpRegStrg = tmpRegStrg & ",Screen 3,Combo_Decision,Combo Decision,C,1"
    
    tmpRegStrg = tmpRegStrg & ",Compressed Flights,Reorganize,Reorganize,B,1"
    tmpRegStrg = tmpRegStrg & ",Compressed Flights,Shrink_to_critical,Shrink to critical,B,1"
    
    'Append any other Privileges
    'tmpRegStrg = tmpRegStrg & ",Entry,Entry,Entry,F,1"
    'tmpRegStrg = tmpRegStrg & ",Feature,Feature,Feature,F,1"
    ULogin.RegisterApplicationString = tmpRegStrg
    ULogin.VersionString = GetApplVersion(True)
    ULogin.InfoCaption = "Info about HUB-Manager"
    ULogin.InfoButtonVisible = True
    ULogin.InfoUfisVersion = "UFIS Version 4.5"
    ULogin.InfoAppVersion = CStr("HUB-Manager " + GetApplVersion(True))
    ULogin.InfoCopyright = "� 2003-2005 UFIS Airport Solutions GmbH"
    ULogin.InfoAAT = "UFIS Airport Solutions GmbH"
    
    
    strRet = ULogin.ShowLoginDialog
    LoginAnsw = ULogin.GetPrivileges("InitModu")
    If LoginAnsw <> "" And strRet <> "CANCEL" Then LoginProcedure = True Else LoginProcedure = False
End Function

Public Function GetApplVersion(ShowExtended As Boolean) As String
    Dim tmpTxt As String
    Dim tmpMajor As String
    Dim tmpMinor As String
    Dim tmpRevis As String
    tmpMajor = App.Major
    tmpMinor = App.Minor
    tmpRevis = App.Revision
    tmpTxt = ""
    Select Case SysInfo.OSPlatform
        Case 0  'Win32s
        Case 1  'Win 95/98
        Case 2  'Win NT
            If SysInfo.OSVersion > 4 Then
                'Win 2000
                If ShowExtended Then tmpRevis = "0." & tmpRevis
            Else
                'Win NT 4
                If ShowExtended Then
                    tmpMinor = Right("0000" & tmpMinor, 2)
                    tmpRevis = Right("0000" & tmpRevis, 4)
                End If
            End If
        Case Else
    End Select
    tmpTxt = tmpTxt & tmpMajor & "." & tmpMinor & "." & tmpRevis
    GetApplVersion = tmpTxt
End Function

