VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form frmLoad 
   Caption         =   "Load Data for time frame ..."
   ClientHeight    =   1470
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   6075
   ControlBox      =   0   'False
   Icon            =   "frmLoad.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   1470
   ScaleWidth      =   6075
   StartUpPosition =   1  'CenterOwner
   Begin VB.PictureBox picFrame 
      Appearance      =   0  'Flat
      AutoRedraw      =   -1  'True
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   2595
      Left            =   0
      ScaleHeight     =   2595
      ScaleWidth      =   6135
      TabIndex        =   0
      Top             =   0
      Width           =   6135
      Begin VB.TextBox txtTimeFrom 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   310
         Left            =   2520
         TabIndex        =   2
         ToolTipText     =   "Time from"
         Top             =   240
         Width           =   555
      End
      Begin MSComCtl2.DTPicker DTPickerFrom 
         Height          =   315
         Left            =   1080
         TabIndex        =   1
         Top             =   240
         Width           =   1455
         _ExtentX        =   2566
         _ExtentY        =   556
         _Version        =   393216
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Format          =   16449537
         CurrentDate     =   39899
      End
      Begin VB.TextBox txtTimeTo 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   310
         Left            =   4560
         TabIndex        =   4
         ToolTipText     =   "Time from"
         Top             =   240
         Width           =   555
      End
      Begin VB.CommandButton cmdLoad 
         Caption         =   "&Load Data"
         Default         =   -1  'True
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1800
         TabIndex        =   5
         Top             =   1080
         Width           =   1155
      End
      Begin VB.CommandButton cmdCancel 
         Cancel          =   -1  'True
         Caption         =   "&Cancel"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   3000
         TabIndex        =   6
         Top             =   1080
         Width           =   1155
      End
      Begin VB.CommandButton cmdToday 
         Caption         =   "&Today"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   5160
         TabIndex        =   7
         Top             =   240
         Width           =   735
      End
      Begin MSComCtl2.DTPicker DTPickerTo 
         Height          =   315
         Left            =   3120
         TabIndex        =   3
         Top             =   240
         Width           =   1455
         _ExtentX        =   2566
         _ExtentY        =   556
         _Version        =   393216
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Format          =   16449537
         CurrentDate     =   39899
      End
      Begin VB.Label Label1 
         BackStyle       =   0  'Transparent
         Caption         =   "Time frame:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   60
         TabIndex        =   8
         Top             =   300
         Width           =   1035
      End
   End
End
Attribute VB_Name = "frmLoad"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public wasCancel As Boolean

Private Sub cmdCancel_Click() 'igu on 24 Nov 2009
    wasCancel = True
    Unload Me
End Sub

Private Sub cmdLoad_Click()
    'Const HOUR_TOLERANCE = 6  'igu on 9 Dec 2009  'igu on 22 Jan 2010
    Const BACKWARD_HOUR_TOLERANCE = -6  'igu on 22 Jan 2010
    Const FORWARD_HOUR_TOLERANCE = 12  'igu on 22 Jan 2010

    Dim fromDat As Date
    Dim toDat As Date
    Dim strFrom As String
    Dim strTo As String
    
    Me.Hide
    Me.MousePointer = vbHourglass
    If bgfrmCompressedFlightsOpen = True Then
        Unload frmCompressedFligths
    End If
       
    'kkh on 27/03/2009 Change date selection
'    strFrom = txtDateFrom.Tag + txtTimeFrom
'    strTo = txtDateTo.Tag + txtTimeTo
    strFrom = Format(DTPickerFrom, "YYYYMMDD") + txtTimeFrom
    strTo = Format(DTPickerTo, "YYYYMMDD") + txtTimeTo
    fromDat = CedaFullDateToVb(strFrom)
    toDat = CedaFullDateToVb(strTo)
    'fromDat = DateAdd("h", -12, fromDat)  'igu on 9 Dec 2009
    'toDat = DateAdd("h", 12, toDat)  'igu on 9 Dec 2009
    'fromDat = DateAdd("h", -HOUR_TOLERANCE, fromDat) 'igu on 9 Dec 2009  'igu on 22 Jan 2010
    'toDat = DateAdd("h", HOUR_TOLERANCE, toDat) 'igu on 9 Dec 2009  'igu on 22 Jan 2010
    fromDat = DateAdd("h", BACKWARD_HOUR_TOLERANCE, fromDat) 'igu on 22 Jan 2010
    toDat = DateAdd("h", FORWARD_HOUR_TOLERANCE, toDat) 'igu on 22 Jan 2010
    
    'kkh on 21/01/2008 Load the flights base on local time frame(user enter)
    UTCOffset = frmData.GetUTCOffset
    fromDat = DateAdd("h", -UTCOffset, fromDat)
    toDat = DateAdd("h", -UTCOffset, toDat)
    
    strFrom = Format(fromDat, "YYYYMMDDhhmmss")
    strTo = Format(toDat, "YYYYMMDDhhmmss")
    fromDat = CedaFullDateToVb(strFrom)
    toDat = CedaFullDateToVb(strTo)
    
    strTimeFrameFrom = strFrom
    strTimeFrameTo = strTo

    If strShowHiddenData = "" Then
        frmData.Hide
    Else
        frmData.Show
    End If

    frmStartup.Show
    SetFormOnTop frmStartup, True
    frmStartup.Refresh
    
    frmStartup.Top = CInt(frmStartup.Tag)
    SetFormOnTop frmStartup, True
    frmData.LoadData
    frmMain.gantt(0).ResetContent
    frmMain.gantt(1).ResetContent
    frmMain.tabTab(0).ResetContent
    frmMain.tabTab(1).ResetContent
    frmMain.MyInit

    frmMain.Show
    frmMain.Refresh
    ApplicationIsStarted = True
    Me.MousePointer = vbNormal
    Unload Me
    
    'kkh on 03/07/2008 solve frmStarup did not close after Loading
    SetFormOnTop frmStartup, False
    frmStartup.Hide
    frmStartup.Visible = False
End Sub

Private Sub SetDefaultDateRange(ByVal StartDate As Date, ByVal HourDifference As Single)  'igu on 24 Nov 2009
    Dim dtmEndDate As Date
    
    DTPickerFrom.Value = StartDate
    txtTimeFrom.Text = Format(StartDate, "hhmm")
    
    dtmEndDate = DateAdd("h", HourDifference, StartDate)
    DTPickerTo.Value = dtmEndDate
    txtTimeTo = Format(dtmEndDate, "hhmm")
    
    DrawBackGround picFrame, 7, True, True
End Sub

Private Sub cmdToday_Click()
    Dim today As Date
    Dim strDate As String
    Dim strTime As String
    today = Now
'    strDate = Format(today, "dd.mm.YYYY")
    'kkh on 27/03/2009 Change date selection
'    txtDateFrom = strDate
'    txtDateTo = strDate
    DTPickerFrom.Value = today
    DTPickerTo.Value = today
    txtTimeFrom = "0000"
    txtTimeTo = "2359"
'    CheckDateField txtDateFrom, "dd.mm.yyyy", False
'    CheckDateField txtDateTo, "dd.mm.yyyy", False
    DrawBackGround picFrame, 7, True, True
End Sub

'Private Sub Command1_Click() 'igu on 24 Nov 2009
'    wasCancel = True
'    Unload Me
'End Sub

Private Sub Form_Load()
    'Const HOUR_DIFF = 12 'igu on 24 Nov 2009
    Const HOUR_DIFF = 15 'igu on 9 Dec 2009

    wasCancel = False
    'cmdToday_Click igu on 24 Nov 2009
    Call SetDefaultDateRange(Now, HOUR_DIFF) 'igu on 24 Nov 2009
End Sub

'Private Sub txtDateFrom_Change()
'    CheckDateField txtDateFrom, "dd.mm.yyyy", False
'End Sub
'
'Private Sub txtDateTo_Change()
'    CheckDateField txtDateTo, "dd.mm.yyyy", False
'End Sub

Private Sub txtTimeFrom_Change()
    If Len(txtTimeFrom.Text) <> "4" Then
        txtTimeFrom.backColor = vbRed
        txtTimeFrom.ForeColor = vbWhite
    Else
        txtTimeFrom.backColor = vbWindowBackground
        txtTimeFrom.ForeColor = vbWindowText
    End If
End Sub

