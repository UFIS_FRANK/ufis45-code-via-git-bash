VERSION 5.00
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "TAB.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmFeedingArrivals 
   Caption         =   "Arrival Flight Feeding"
   ClientHeight    =   7065
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   3465
   Icon            =   "frmFeedingArrivals.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   7065
   ScaleWidth      =   3465
   StartUpPosition =   3  'Windows Default
   Begin MSComctlLib.ProgressBar pbrProgress 
      Height          =   255
      Left            =   0
      TabIndex        =   1
      Top             =   6840
      Visible         =   0   'False
      Width           =   2535
      _ExtentX        =   4471
      _ExtentY        =   450
      _Version        =   393216
      Appearance      =   0
   End
   Begin TABLib.TAB tabFlights 
      Height          =   7035
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   3435
      _Version        =   65536
      _ExtentX        =   6059
      _ExtentY        =   12409
      _StockProps     =   64
   End
End
Attribute VB_Name = "frmFeedingArrivals"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private strCurrAftUrno As String
Public strCalledFrom As String

Public Sub SetFlight(strUrno As String)
    strCurrAftUrno = strUrno
End Sub

Private Sub Form_Load()
    'ShowFeedings
    Dim sngLeft As Single
    Dim sngTop As Single
    Dim sngWidth As Single
    Dim sngHeight As Single
    
    
'Read position from registry and move the window
    sngLeft = Val(GetSetting(appName:="HUB_Manager_Feedings", _
                        section:="Startup", _
                        Key:="myLeft", _
                        Default:="-1"))
    sngTop = Val(GetSetting(appName:="HUB_Manager_Feedings", _
                        section:="Startup", _
                        Key:="myTop", _
                        Default:="-1"))
    sngWidth = Val(GetSetting(appName:="HUB_Manager_Feedings", _
                        section:="Startup", _
                        Key:="myWidth", _
                        Default:="-1"))
    sngHeight = Val(GetSetting(appName:="HUB_Manager_Feedings", _
                        section:="Startup", _
                        Key:="myHeight", _
                        Default:="-1"))
                        
    If sngTop = -1 Or sngLeft = -1 Or sngWidth = -1 Or sngHeight = -1 Then
    Else
        sngLeft = Max(sngLeft, 0)
        sngTop = Max(sngTop, 0)
        sngWidth = Min(sngWidth, Screen.Width - 6000)
        sngHeight = Min(sngHeight, Screen.Height - 6000)
    
        Me.Move sngLeft, sngTop, sngWidth, sngHeight
    End If
End Sub

Public Sub ShowFeedings()
    Dim i As Long
    Dim cnt As Long
    Dim strRawFlno As String
    Dim strRawFlnoCopy As String
    Dim strFlno As String
    Dim strFlightDay As String
    Dim arr() As String
    Dim llAftLine As Long
    Dim llAftLineArrival As Long
    Dim llLoaTabLine As Long
    Dim strLine As String
    Dim strAftUrno2 As String
    Dim strArrUrno As String
    Dim strAdid As String
    Dim strValues As String
    Dim dummy1 As String, dummy2 As String, dummy3 As String
    Dim strStoa As String
    Dim strFlnu As String
    Dim datStoa As Date
    Dim strTifa As String
    Dim datTifa As Date
    Dim tabIDX As Integer
    Dim strFlnoList As String
    Dim strDepFlno As String
    
    tabFlights.ResetContent
    tabFlights.HeaderString = "URNO,FLNO,Time"
    tabFlights.LogicalFieldList = "URNO,FLNO,TIFD"
    tabFlights.HeaderLengthString = "0,100,100"
    tabFlights.DateTimeSetColumn 2
    tabFlights.DateTimeSetColumnFormat 2, "YYYYMMDDhhmmss", "hh':'mm'/'DD"

    tabFlights.SetUniqueFields ("URNO")
    
    frmData.tabData(0).GetLinesByIndexValue "URNO", strCurrAftUrno, 0
    llAftLine = frmData.tabData(0).GetNextResultLine
    If llAftLine > -1 Then
        strDepFlno = frmData.tabData(0).GetFieldValue(llAftLine, "FLNO")
        Me.Caption = strDepFlno & " feedings ... "
    End If
    
    If strCalledFrom = "MAIN" Then
        tabIDX = 3
    End If
    If strCalledFrom = "MAIL" Then
        tabIDX = 6
    End If
    If strCalledFrom = "CARGO" Then
        tabIDX = 7
    End If
        
'---------------
'igu on 12/01/2011
'Improve the logic
'---------------

'    For i = 0 To frmData.tabData(tabIDX).GetLineCount - 1
'        'First of all get the corresponding arrival flight
'        'to identify the correct stoa and this is to get the
'        'correct Departure flight
'        strAdid = ""
'        strFlnu = frmData.tabData(tabIDX).GetFieldValue(i, "FLNU")
'        strLine = frmData.tabData(0).GetLinesByIndexValue("URNO", strFlnu, 0)
'        llAftLine = frmData.tabData(0).GetNextResultLine
'        If llAftLine > -1 Then
'            strStoa = frmData.tabData(0).GetFieldValue(llAftLine, "STOA")
'            strAdid = frmData.tabData(0).GetFieldValue(llAftLine, "ADID")
'        End If
'        If strAdid = "A" And Trim(strStoa) <> "" Then
'            datStoa = CedaFullDateToVb(strStoa)
'            strRawFlno = frmData.tabData(tabIDX).GetFieldValue(i, "FLNO")
'            arr = Split(strRawFlno, "/")
'            If UBound(arr) = 1 Then
'                strFlno = arr(0)
'                strFlightDay = arr(1)
'            Else
'                strFlno = strRawFlno
'                strFlightDay = ""
'            End If
'            strFlno = StripAftFlno(strFlno, dummy1, dummy2, dummy3)
'            llAftLine = frmMain.GetCorrectDepartureByFlightDay(datStoa, strFlightDay, strFlno)
'            If Trim(strFlightDay) <> "" Then
'                strRawFlnoCopy = strFlno + "/" + strFlightDay
'            Else
'                strRawFlnoCopy = strFlno
'            End If
'            'llAftLine = GetCorrectDepartureLine(Key, opStoa, strFlno)
'            If llAftLine > -1 Then
'                strAftUrno2 = frmData.tabData(0).GetFieldValue(llAftLine, "URNO")
'                If strAftUrno2 = strCurrAftUrno Then
'                    strArrUrno = frmData.tabData(tabIDX).GetFieldValue(i, "FLNU")
'                    strLine = frmData.tabData(0).GetLinesByIndexValue("URNO", strArrUrno, 0)
'                    llAftLineArrival = frmData.tabData(0).GetNextResultLine
'                    If llAftLineArrival > -1 Then
'                        strAdid = frmData.tabData(0).GetFieldValue(llAftLineArrival, "ADID")
'                        If strAdid = "A" Then
'                            strTifa = frmData.tabData(0).GetFieldValue(llAftLineArrival, "TIFA")
'                            If Trim(strTifa) <> "" Then
'                                datTifa = CedaFullDateToVb(strTifa)
'                            End If
'                            strValues = frmData.tabData(0).GetFieldValues(llAftLineArrival, "URNO,FLNO,TIFA")
'                            tabFlights.InsertTextLine strValues, False
'                            If strCalledFrom = "MAIN" Then
'                                If HasArrivalConflict(strArrUrno, datTifa, False) = True Then
'                                    tabFlights.SetLineColor tabFlights.GetLineCount - 1, vbWhite, vbRed
'                                End If
'                            End If
'                        End If
'                    End If
'                End If
'            End If
'        End If 'adid = "A"
'        llAftLine = -1
'    Next i

    DoEvents

    pbrProgress.Min = 0
    pbrProgress.Max = frmData.tabData(0).GetLineCount
    pbrProgress.value = 0
    pbrProgress.Visible = True

    For i = 0 To frmData.tabData(0).GetLineCount - 1
        'First of all get the corresponding arrival flight
        'to identify the correct stoa and this is to get the
        'correct Departure flight
        strStoa = frmData.tabData(0).GetFieldValue(i, "STOA")
        strAdid = frmData.tabData(0).GetFieldValue(i, "ADID")
        If strAdid = "A" And Trim(strStoa) <> "" Then
            strFlnoList = Chr(0)
            strFlnu = frmData.tabData(0).GetFieldValue(i, "URNO")
            strLine = frmData.tabData(tabIDX).GetLinesByIndexValue("FLNU", strFlnu, 0)
            llAftLine = frmData.tabData(tabIDX).GetNextResultLine
            Do While llAftLine > -1
                strRawFlno = frmData.tabData(tabIDX).GetFieldValue(llAftLine, "FLNO")
                If InStr(strFlnoList, Chr(0) & strRawFlno & Chr(0)) = 0 Then
                    strFlnoList = strFlnoList & strRawFlno & Chr(0)
                    arr = Split(strRawFlno, "/")
                    If UBound(arr) = 1 Then
                        strFlno = arr(0)
                        strFlightDay = arr(1)
                    Else
                        strFlno = strRawFlno
                        strFlightDay = ""
                    End If
                    strFlno = StripAftFlno(strFlno, dummy1, dummy2, dummy3)
                    If strFlno = strDepFlno Then
                        datStoa = CedaFullDateToVb(strStoa)
                        llAftLine = frmMain.GetCorrectDepartureByFlightDay(datStoa, strFlightDay, strFlno)
                        If Trim(strFlightDay) <> "" Then
                            strRawFlnoCopy = strFlno + "/" + strFlightDay
                        Else
                            strRawFlnoCopy = strFlno
                        End If
                        If llAftLine > -1 Then
                            strAftUrno2 = frmData.tabData(0).GetFieldValue(llAftLine, "URNO")
                            If strAftUrno2 = strCurrAftUrno Then
                                strTifa = frmData.tabData(0).GetFieldValue(i, "TIFA")
                                If Trim(strTifa) <> "" Then
                                    datTifa = CedaFullDateToVb(strTifa)
                                End If
                                strValues = frmData.tabData(0).GetFieldValues(i, "URNO,FLNO,TIFA")
                                tabFlights.InsertTextLine strValues, False
                                If strCalledFrom = "MAIN" Then
                                    If HasArrivalConflict(strFlnu, datTifa, False) = True Then
                                        tabFlights.SetLineColor tabFlights.GetLineCount - 1, vbWhite, vbRed
                                    End If
                                End If
                            End If
                        End If
                    End If
                End If
                llAftLine = frmData.tabData(tabIDX).GetNextResultLine
            Loop
        End If 'adid = "A"
        
        pbrProgress.value = pbrProgress.value + 1
        
'        DoEvents
    Next i
    pbrProgress.Visible = False
'---------------
'igu on 12/01/2011
'end of Improve the logic
'---------------

    tabFlights.Sort "2", True, True
    tabFlights.Refresh
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    Dim sngLeft As Single
    Dim sngTop As Single
    Dim sngHeight As Single
    Dim sngWidth As Single
    
    sngLeft = Me.Left
    sngTop = Me.Top
    sngWidth = Me.Width
    sngHeight = Me.Height
    
    SaveSetting "HUB_Manager_Feedings", _
                "Startup", _
                "myLeft", _
                CStr(sngLeft)
    SaveSetting "HUB_Manager_Feedings", _
                "Startup", _
                "myTop", _
                CStr(sngTop)
    SaveSetting "HUB_Manager_Feedings", _
                "Startup", _
                "myWidth", _
                CStr(sngWidth)
    SaveSetting "HUB_Manager_Feedings", _
                "Startup", _
                "myHeight", _
                CStr(sngHeight)
                
    If UnloadMode = 0 Then
        Cancel = True
        Me.Hide
    End If
End Sub

Private Sub Form_Resize()
    On Error Resume Next

    Me.tabFlights.Top = 30
    Me.tabFlights.Width = Me.ScaleWidth - 30
    Me.tabFlights.Height = Me.ScaleHeight - 30
    
    Me.pbrProgress.Width = Me.ScaleWidth
    Me.pbrProgress.Top = Me.ScaleHeight - Me.pbrProgress.Height
End Sub

Private Sub tabFlights_RowSelectionChanged(ByVal LineNo As Long, ByVal Selected As Boolean)
    Dim strUrno As String
    
    If Selected = True Then
        strUrno = Me.tabFlights.GetFieldValue(LineNo, "URNO")
        frmMain.SelectMainGanttBar strUrno, -1
    End If
End Sub

Private Sub tabFlights_SendLButtonClick(ByVal LineNo As Long, ByVal ColNo As Long)
'    Dim strUrno As String
'
'    strUrno = Me.tabFlights.GetFieldValue(LineNo, "URNO")
'    frmMain.SelectMainGanttBar strUrno, -1
End Sub

Public Function HasArrivalConflict(strUrno As String, opStoa As Date, setConflictColor As Boolean) As Boolean
    Dim strVal As String
    Dim strEmptLine As String
    Dim llLine As Long
    Dim llAftLine As Long
    Dim strRawFlno As String
    Dim strFlno As String
    Dim strFlightDay As String
    Dim strLineValues As String
    Dim llCurrLine As Long
    Dim strAdid As String
    Dim arr() As String
    Dim datStod As Date
    Dim strVALU As String
    Dim strArrUrno As String
    Dim strDepUrno As String
    Dim dummy1 As String, dummy2 As String, dummy3 As String
    Dim hasConflict As Boolean
    Dim strConnFlightUrno As String
    
    frmData.tabData(3).GetLinesByIndexValue "FLNU", strUrno, 0
    llLine = frmData.tabData(3).GetNextResultLine
    llCurrLine = 0
    
    strArrUrno = strUrno
    hasConflict = False
    While (llLine <> -1 And hasConflict = False)
        ' Insert an empty line
        strRawFlno = frmData.tabData(3).GetFieldValue(llLine, "FLNO")
        arr = Split(strRawFlno, "/")
        If UBound(arr) = 1 Then
            strFlno = arr(0)
            strFlightDay = arr(1)
        Else
            strFlno = strRawFlno
        End If
        strFlno = StripAftFlno(strFlno, dummy1, dummy2, dummy3)
        llAftLine = frmMain.GetCorrectDepartureByFlightDay(opStoa, strFlightDay, strFlno)
        If llAftLine <> -1 Then
            strAdid = frmData.tabData(0).GetFieldValue(llAftLine, "ADID")
            strConnFlightUrno = frmData.tabData(0).GetFieldValue(llAftLine, "URNO")
            If strAdid = "D" Then 'only if departure
                strDepUrno = frmData.tabData(0).GetFieldValue(llAftLine, "URNO")
                hasConflict = frmMain.CheckConnectionTimes(strArrUrno, strDepUrno, llCurrLine, llAftLine, setConflictColor)
                If hasConflict = True And (strConnFlightUrno = strCurrAftUrno) Then
                    hasConflict = True
                End If
            End If
        End If
        llLine = frmData.tabData(3).GetNextResultLine
        ReDim arr(0)
        llCurrLine = llCurrLine + 1
    Wend
    HasArrivalConflict = hasConflict
End Function

