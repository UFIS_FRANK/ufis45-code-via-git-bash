VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{65E121D4-0C60-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCHRT20.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "Comdlg32.ocx"
Begin VB.Form frmHourlyDistribution 
   Caption         =   "Hourly Distribution of flights ..."
   ClientHeight    =   7155
   ClientLeft      =   165
   ClientTop       =   555
   ClientWidth     =   9825
   Icon            =   "frmHourlyDistribution.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   7155
   ScaleWidth      =   9825
   StartUpPosition =   3  'Windows Default
   Begin MSComDlg.CommonDialog CommonDialog1 
      Left            =   5445
      Top             =   6390
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   255
      Left            =   0
      TabIndex        =   9
      Top             =   6900
      Width           =   9825
      _ExtentX        =   17330
      _ExtentY        =   450
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
      EndProperty
   End
   Begin VB.ComboBox cbChart 
      Height          =   315
      ItemData        =   "frmHourlyDistribution.frx":030A
      Left            =   11160
      List            =   "frmHourlyDistribution.frx":030C
      Style           =   2  'Dropdown List
      TabIndex        =   7
      Top             =   360
      Width           =   2115
   End
   Begin VB.CommandButton cmdDateFwd 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   4020
      Picture         =   "frmHourlyDistribution.frx":030E
      Style           =   1  'Graphical
      TabIndex        =   6
      Top             =   6540
      Width           =   615
   End
   Begin VB.CommandButton cmdDateBack 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   60
      Picture         =   "frmHourlyDistribution.frx":0650
      Style           =   1  'Graphical
      TabIndex        =   5
      Top             =   6540
      Width           =   615
   End
   Begin VB.CommandButton cmdPrint 
      Caption         =   "&Print"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   11880
      TabIndex        =   2
      Top             =   6480
      Width           =   1395
   End
   Begin MSChart20Lib.MSChart chart 
      Height          =   5535
      Left            =   60
      OleObjectBlob   =   "frmHourlyDistribution.frx":09F2
      TabIndex        =   0
      Top             =   780
      Width           =   12795
   End
   Begin VB.Label lblAppearance 
      Caption         =   "Chart Appearance:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Left            =   9480
      TabIndex        =   8
      Top             =   420
      Width           =   1635
   End
   Begin VB.Label lblCurrentView 
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Current View: XX.XX.XXX"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00C000C0&
      Height          =   315
      Left            =   660
      TabIndex        =   4
      Top             =   6540
      Width           =   3375
   End
   Begin VB.Label lblTooltip 
      BackColor       =   &H00000000&
      Caption         =   "        To get detailed information, please click slowly twice onto a bar in the chart!!"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H0000FF00&
      Height          =   315
      Left            =   60
      TabIndex        =   3
      Top             =   0
      Width           =   13200
   End
   Begin VB.Line Line1 
      BorderWidth     =   2
      X1              =   0
      X2              =   13320
      Y1              =   720
      Y2              =   720
   End
   Begin VB.Label lblHeader 
      Caption         =   "Arrivals Timeframe: XX.XX.XXX - YY.YY.YYYY"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   60
      TabIndex        =   1
      Top             =   360
      Width           =   5835
   End
End
Attribute VB_Name = "frmHourlyDistribution"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public SimpleListShown As Boolean
Public omCurrentDate As Date
'****************************************
'** Privates
Private omStartDate As Date
Private omEndDate As Date

Private Sub cbChart_Click()
    Dim idx As Integer
    Dim arr() As String
    Dim myChartType As Long
    Dim i As Integer

    arr = Split(cbChart.Text, "|")
    myChartType = CInt(arr(1))
    chart.ChartType = myChartType
End Sub

Private Sub chart_PointSelected(Series As Integer, DataPoint As Integer, MouseFlags As Integer, Cancel As Integer)
    Dim myHour As Integer
    Dim strAdid As String
    myHour = DataPoint - 1
    
    If myHour >= 0 Then
        frmSimpleList.myHour = myHour
        If Series = 1 Then 'Arrival
            frmSimpleList.omAdid = "A"
            strAdid = "A"
        End If
        If Series = 2 Then 'Departure
            frmSimpleList.omAdid = "D"
            strAdid = "D"
        End If
        If SimpleListShown = False Then
            SimpleListShown = True
            frmSimpleList.omListMode = "HOURLYFLIGHTS"
            frmSimpleList.Show
            frmSimpleList.ZOrder
        Else
            frmSimpleList.ShowFlights myHour, strAdid
            frmSimpleList.ZOrder
            frmSimpleList.tabList.Refresh
        End If
    End If
End Sub


Private Sub cmdDateBack_Click()
    Dim datNew As Date
    Dim strCurrDate As String
    Dim strTmpCurrDate As String
    Dim strTmpEndData As String
    Dim strTmpStartData As String
    
    datNew = DateAdd("d", -1, omCurrentDate)
    strTmpEndData = Format(omEndDate, "YYYYMMDD000000")
    strTmpStartData = Format(omStartDate, "YYYYMMDD000000")
    If datNew >= omStartDate Then
        strTmpCurrDate = Format(datNew, "YYYYMMDD000000")
        omCurrentDate = CedaFullDateToVb(strTmpCurrDate)
        If strTmpCurrDate = strTmpEndData Then
            cmdDateFwd.Enabled = False
        Else
            cmdDateFwd.Enabled = True
        End If
        If strTmpStartData < strTmpCurrDate Then
            cmdDateBack.Enabled = True
        Else
            cmdDateBack.Enabled = False
        End If
        Show3D
    End If
    lblCurrentView = "Current Date: " + Format(omCurrentDate, "DD.MM.YYYY")
End Sub

Private Sub cmdDateFwd_Click()
    Dim datNew As Date
    Dim strCurrDate As String
    Dim strTmpCurrDate As String
    Dim strTmpEndData As String
    Dim strTmpStartData As String
    
    datNew = DateAdd("d", 1, omCurrentDate)
    strTmpEndData = Format(omEndDate, "YYYYMMDD000000")
    strTmpStartData = Format(omStartDate, "YYYYMMDD000000")
    If datNew <= omEndDate Then
        strTmpCurrDate = Format(datNew, "YYYYMMDD000000")
        omCurrentDate = CedaFullDateToVb(strTmpCurrDate)
        If strTmpCurrDate = strTmpEndData Then
            cmdDateFwd.Enabled = False
        Else
            cmdDateFwd.Enabled = True
        End If
        If strTmpStartData < strTmpCurrDate Then
            cmdDateBack.Enabled = True
        Else
            cmdDateBack.Enabled = False
        End If
        Show3D
    End If
    lblCurrentView = "Current Date: " + Format(omCurrentDate, "DD.MM.YYYY")
End Sub


Private Sub cmdPrint_Click()
    Dim oldFrmColor As Long
    Dim ollblColor As Long
    Dim oldOrient As Integer
    Dim myDrawMode As Integer
    Dim oldScaleH As Integer
    Dim oldScaleW As Integer
    
    CommonDialog1.CancelError = True
    On Error Resume Next
    ' display the dialog
    CommonDialog1.ShowPrinter
    If Err.Number = 32755 Then Exit Sub ' user cancelled
'    If CommonDialog1.Orientation = cdlLandscape Then
'        Printer.Orientation = cdlLandscape
'    Else
'        Printer.Orientation = cdlPortrait
'    End If
    
    Me.Hide
    oldScaleH = Me.ScaleHeight
    oldScaleW = Me.ScaleWidth
    Me.Move Me.Left, Me.Top, 16035, 10440
    cmdPrint.Visible = False
    lblTooltip.Visible = False
    cmdDateBack.Visible = False
    cmdDateFwd.Visible = False
    lblAppearance.Visible = False
    cbChart.Visible = False
    oldFrmColor = Me.backColor
    ollblColor = lblHeader.backColor
    Me.backColor = vbWhite
    lblHeader.backColor = vbWhite
    lblCurrentView.backColor = vbWhite
    oldOrient = Printer.Orientation
    chart.Left = chart.Left + 500
    lblHeader.Left = lblHeader.Left + 500
    lblCurrentView.Left = lblCurrentView.Left + 500
    lblCurrentView.BorderStyle = 0
    Printer.Orientation = vbPRORLandscape
    Me.PrintForm
    chart.Left = chart.Left - 500
    lblHeader.Left = lblHeader.Left - 500
    lblCurrentView.Left = lblCurrentView.Left - 500
    Printer.Orientation = oldOrient
    cmdPrint.Visible = True
    lblTooltip.Visible = True
    cmdDateBack.Visible = True
    cmdDateFwd.Visible = True
    Me.backColor = oldFrmColor
    lblAppearance.Visible = True
    cbChart.Visible = True
    lblHeader.backColor = ollblColor
    lblCurrentView.backColor = ollblColor
    lblCurrentView.BorderStyle = 1
    Printer.EndDoc
    Me.Move Me.Left, Me.Top, oldScaleW, oldScaleH
    Me.Show
End Sub



Private Sub Form_Load()
    Dim fromDate As Date
    Dim toDate As Date
    Dim strTmpDate As String
    Dim strVal As String
    Dim sngLeft As Single
    Dim sngTop As Single
    Dim sngWidth As Single
    Dim sngHeight As Single
    
'Read position from registry and move the window
    sngLeft = Val(GetSetting(appName:="HUB_Manager_HourlyDistribution", _
                        section:="Startup", _
                        Key:="myLeft", _
                        Default:="-1"))
    sngTop = Val(GetSetting(appName:="HUB_Manager_HourlyDistribution", _
                        section:="Startup", _
                        Key:="myTop", _
                        Default:="-1"))
    sngWidth = Val(GetSetting(appName:="HUB_Manager_HourlyDistribution", _
                        section:="Startup", _
                        Key:="myWidth", _
                        Default:="-1"))
    sngHeight = Val(GetSetting(appName:="HUB_Manager_HourlyDistribution", _
                        section:="Startup", _
                        Key:="myHeight", _
                        Default:="-1"))
                        
    If sngTop = -1 Or sngLeft = -1 Or sngWidth = -1 Or sngHeight = -1 Then
    Else
        sngLeft = Max(sngLeft, 0)
        sngTop = Max(sngTop, 0)
        sngWidth = Min(sngWidth, Screen.Width - 6000)
        sngHeight = Min(sngHeight, Screen.Height - 6000)
    
        Me.Move sngLeft, sngTop, sngWidth, sngHeight
    End If
'END: Read position from registry and move the window
    
    
    ' trick is to hide the style number in the right part
    ' to get it back by clicking the combobox and extract it
    ' to set the correct enum value
    strVal = "2D Area                                                     |" & VtChChartType2dArea
    cbChart.AddItem strVal
    strVal = "2D Bar                                                     |" & VtChChartType2dBar
    cbChart.AddItem strVal
    strVal = "2D Line                                                     |" & VtChChartType2dLine
    cbChart.AddItem strVal
    strVal = "2D Pie                                                     |" & VtChChartType2dPie
    cbChart.AddItem strVal
    strVal = "2D Step                                                     |" & VtChChartType2dStep
    cbChart.AddItem strVal
    strVal = "3D Area                                                     |" & VtChChartType3dArea
    cbChart.AddItem strVal
    strVal = "3D Bar                                                     |" & VtChChartType3dBar
    cbChart.AddItem strVal
    strVal = "3D Line                                                     |" & VtChChartType3dLine
    cbChart.AddItem strVal
    strVal = "3D Step                                                     |" & VtChChartType3dStep
    cbChart.AddItem strVal
    
    fromDate = CedaFullDateToVb(strTimeFrameFrom)
    toDate = CedaFullDateToVb(strTimeFrameTo)
    omStartDate = fromDate
    omEndDate = toDate
    strTmpDate = Format(fromDate, "YYYYMMDD000000")
    omCurrentDate = CedaFullDateToVb(strTmpDate)
    cmdDateBack.Enabled = False
    Show3D
    SimpleListShown = False
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    Dim sngLeft As Single
    Dim sngTop As Single
    Dim sngHeight As Single
    Dim sngWidth As Single
    
    sngLeft = Me.Left
    sngTop = Me.Top
    sngWidth = Me.Width
    sngHeight = Me.Height
    
    SaveSetting "HUB_Manager_HourlyDistribution", _
                "Startup", _
                "myLeft", _
                CStr(sngLeft)
    SaveSetting "HUB_Manager_HourlyDistribution", _
                "Startup", _
                "myTop", _
                CStr(sngTop)
    SaveSetting "HUB_Manager_HourlyDistribution", _
                "Startup", _
                "myWidth", _
                CStr(sngWidth)
    SaveSetting "HUB_Manager_HourlyDistribution", _
                "Startup", _
                "myHeight", _
                CStr(sngHeight)
End Sub

Private Sub Form_Resize()
    Dim llLeftA As Long
    Dim llLabel As Long
    Dim llPrint As Long
    Dim llChart As Long
    
    llLeftA = 375
    llLabel = 375
    llChart = 600
    cmdDateBack.Top = Me.ScaleHeight - 575
    cmdDateFwd.Top = Me.ScaleHeight - 575
    lblCurrentView.Top = Me.ScaleHeight - 575
    cmdPrint.Top = Me.ScaleHeight - 575
    chart.Height = (Me.ScaleHeight - 800) - chart.Top
    cmdPrint.Left = Me.ScaleWidth - 1455
    chart.Width = (Me.ScaleWidth - 540) - chart.Left
    lblAppearance.Left = Me.ScaleWidth - 3855
    cbChart.Left = Me.ScaleWidth - 2175
End Sub

Private Sub Form_Unload(Cancel As Integer)
    frmMain.bgHourlyIsActive = False
    frmMain.RefreshAllControls
End Sub


Private Sub Show3D()
    Dim i, ilCol As Integer
    Dim c As Integer
    Dim ilVal As Integer
    Dim strVal As String
    Dim ilCharVal As Integer
    Dim strTif As String
    Dim strAdid As String
    Dim strCurrDate As String
    Dim strTifDate As String
    lblCurrentView = "Current Date: " + Format(omCurrentDate, "DD.MM.YYYY")
'**Init the chart object
    chart.RowCount = 24
    chart.ColumnCount = 2
    For ilCol = 0 To 1
        For i = 1 To 24
            chart.column = ilCol + 1
            chart.Row = i
            chart.RowLabel = CStr(i - 1) + ":00"
            chart.Data = 0 'Form1.tabResult.GetColumnValue(0, i - 1)
            c = chart.RowLabelCount
        Next i
    Next ilCol
'load the data for the char object and add values to the bars
'Initial it will be done for the arrivals
    For i = 0 To frmData.tabData(0).GetLineCount - 1
        strAdid = frmData.tabData(0).GetFieldValue(i, "ADID")
        If (strAdid = "A") Then
            strTif = frmData.tabData(0).GetFieldValue(i, "TIFA")
            strTifDate = Mid(strTif, 1, 8)
            strCurrDate = Format(omCurrentDate, "YYYYMMDD")
            If strCurrDate = strTifDate Then
                chart.column = 1
                chart.ColumnLabel = "Arrivals"
                strVal = Mid(strTif, 9, 2)
                ilVal = CInt(strVal)
                chart.Row = ilVal + 1
                ilCharVal = CInt(chart.Data)
                ilCharVal = ilCharVal + 1
                chart.Data = ilCharVal
            End If
        End If
        If (strAdid = "D") Then
            strTif = frmData.tabData(0).GetFieldValue(i, "TIFD")
            strTifDate = Mid(strTif, 1, 8)
            strCurrDate = Format(omCurrentDate, "YYYYMMDD")
            If strCurrDate = strTifDate Then
                chart.column = 2
                chart.ColumnLabel = "Departures"
                strVal = Mid(strTif, 9, 2)
                ilVal = CInt(strVal)
                chart.Row = ilVal + 1
                ilCharVal = CInt(chart.Data)
                ilCharVal = ilCharVal + 1
                chart.Data = ilCharVal
            End If
        End If
    Next i
    chart.ShowLegend = True
    lblHeader = "Arr./Dep. Timeframe: " & CedaFullDateToVb(strTimeFrameFrom) & _
                " - " & CedaFullDateToVb(strTimeFrameTo)
End Sub


