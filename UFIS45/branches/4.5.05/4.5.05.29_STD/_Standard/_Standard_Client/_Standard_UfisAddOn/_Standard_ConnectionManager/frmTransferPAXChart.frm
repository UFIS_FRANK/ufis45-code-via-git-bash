VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{65E121D4-0C60-11D2-A9FC-0000F8754DA1}#2.0#0"; "mschrt20.ocx"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "Comdlg32.ocx"
Begin VB.Form frmTransferPAXChart 
   Caption         =   "Hourly Distribution of Passengers ..."
   ClientHeight    =   5820
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   10485
   Icon            =   "frmTransferPAXChart.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   5820
   ScaleWidth      =   10485
   StartUpPosition =   3  'Windows Default
   Begin MSComDlg.CommonDialog CommonDialog1 
      Left            =   7020
      Top             =   5175
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.ComboBox cbType 
      Height          =   315
      Left            =   8580
      TabIndex        =   12
      Text            =   "cbType"
      Top             =   360
      Width           =   1755
   End
   Begin VB.ComboBox cbRelatedTimes 
      Height          =   315
      Left            =   6240
      TabIndex        =   10
      Text            =   "cbRelatedTimes"
      Top             =   360
      Width           =   1755
   End
   Begin VB.CommandButton cmdPrint 
      Caption         =   "&Print"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   8940
      TabIndex        =   4
      Top             =   5220
      Width           =   1395
   End
   Begin VB.CommandButton cmdDateBack 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   60
      Picture         =   "frmTransferPAXChart.frx":030A
      Style           =   1  'Graphical
      TabIndex        =   3
      Top             =   5220
      Width           =   615
   End
   Begin VB.CommandButton cmdDateFwd 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   4020
      Picture         =   "frmTransferPAXChart.frx":06AC
      Style           =   1  'Graphical
      TabIndex        =   2
      Top             =   5220
      Width           =   615
   End
   Begin VB.ComboBox cbChart 
      Height          =   315
      ItemData        =   "frmTransferPAXChart.frx":09EE
      Left            =   5700
      List            =   "frmTransferPAXChart.frx":09F0
      Style           =   2  'Dropdown List
      TabIndex        =   1
      Top             =   5220
      Visible         =   0   'False
      Width           =   1035
   End
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   255
      Left            =   0
      TabIndex        =   0
      Top             =   5565
      Width           =   10485
      _ExtentX        =   18494
      _ExtentY        =   450
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
      EndProperty
   End
   Begin MSChart20Lib.MSChart chart 
      Height          =   4335
      Left            =   60
      OleObjectBlob   =   "frmTransferPAXChart.frx":09F2
      TabIndex        =   5
      Top             =   780
      Width           =   10275
   End
   Begin VB.Label lblType 
      BackStyle       =   0  'Transparent
      Caption         =   "Type:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Left            =   8040
      TabIndex        =   13
      Top             =   420
      Width           =   495
   End
   Begin VB.Label lblRelTime 
      BackStyle       =   0  'Transparent
      Caption         =   "Rel. Time:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Left            =   5340
      TabIndex        =   11
      Top             =   420
      Width           =   915
   End
   Begin VB.Label lblHeader 
      Caption         =   "Arrivals Timeframe: aa.aa.aa - aa.aa.aa"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   60
      TabIndex        =   9
      Top             =   360
      Width           =   5055
   End
   Begin VB.Line Line1 
      BorderWidth     =   2
      X1              =   0
      X2              =   10560
      Y1              =   720
      Y2              =   720
   End
   Begin VB.Label lblTooltip 
      Alignment       =   2  'Center
      BackColor       =   &H00000000&
      Caption         =   "Select related time and type of chart in comboboxes"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H0000FF00&
      Height          =   315
      Left            =   60
      TabIndex        =   8
      Top             =   0
      Width           =   10440
   End
   Begin VB.Label lblCurrentView 
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Current View: XX.XX.XXX"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00C000C0&
      Height          =   315
      Left            =   660
      TabIndex        =   7
      Top             =   5220
      Width           =   3375
   End
   Begin VB.Label lblAppearance 
      Caption         =   "Chart Appearance:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Left            =   4800
      TabIndex        =   6
      Top             =   5280
      Visible         =   0   'False
      Width           =   855
   End
End
Attribute VB_Name = "frmTransferPAXChart"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public SimpleListShown As Boolean
Public omCurrentDate As Date
'****************************************
'** Privates
Private omStartDate As Date
Private omEndDate As Date
Private FullScreen As Boolean
Private ig1440Values(1440) As Integer
Private ig24Values(24) As Integer
Private strCurrType As String
Private strCurrRelTime As String
Private strCurrTimeField As String


Private Sub cbChart_Click()
    Dim idx As Integer
    Dim arr() As String
    Dim myChartType As Long
    Dim i As Integer

    arr = Split(cbChart.Text, "|")
    myChartType = CInt(arr(1))
    chart.ChartType = myChartType
End Sub

Private Sub cbRelatedTimes_Change()
    strCurrRelTime = cbRelatedTimes.Text
    Show3D
End Sub

Private Sub cbRelatedTimes_Click()
    strCurrRelTime = cbRelatedTimes.Text
    Show3D
End Sub

Private Sub cbType_Change()
    strCurrType = cbType.Text
    Show3D
End Sub

Private Sub cbType_Click()
    strCurrType = cbType.Text
    Show3D
End Sub

Private Sub chart_PointSelected(Series As Integer, DataPoint As Integer, MouseFlags As Integer, Cancel As Integer)
    Dim myHour As Integer
    Dim strAdid As String
    myHour = DataPoint - 1
    
    If myHour >= 0 Then
        frmSimpleList.myHour = myHour
        If Series = 1 Then 'Arrival
            frmSimpleList.omAdid = "A"
            strAdid = "A"
        End If
        If Series = 2 Then 'Departure
            frmSimpleList.omAdid = "D"
            strAdid = "D"
        End If
        If SimpleListShown = False Then
            SimpleListShown = True
            frmSimpleList.omListMode = "HOURLYFLIGHTS"
            frmSimpleList.Show
            frmSimpleList.ZOrder
        Else
            frmSimpleList.ShowFlights myHour, strAdid
            frmSimpleList.ZOrder
            frmSimpleList.tabList.Refresh
        End If
    End If
End Sub

Private Sub chart_SeriesSelected(Series As Integer, MouseFlags As Integer, Cancel As Integer)
    'MsgBox Series
End Sub


Private Sub cmdDateBack_Click()
    Dim datNew As Date
    Dim strCurrDate As String
    Dim strTmpCurrDate As String
    Dim strTmpEndData As String
    Dim strTmpStartData As String
    
    datNew = DateAdd("d", -1, omCurrentDate)
    strTmpEndData = Format(omEndDate, "YYYYMMDD000000")
    strTmpStartData = Format(omStartDate, "YYYYMMDD000000")
    If datNew >= omStartDate Then
        strTmpCurrDate = Format(datNew, "YYYYMMDD000000")
        omCurrentDate = CedaFullDateToVb(strTmpCurrDate)
        If strTmpCurrDate = strTmpEndData Then
            cmdDateFwd.Enabled = False
        Else
            cmdDateFwd.Enabled = True
        End If
        If strTmpStartData < strTmpCurrDate Then
            cmdDateBack.Enabled = True
        Else
            cmdDateBack.Enabled = False
        End If
        Show3D
    End If
    lblCurrentView = "Current Date: " + Format(omCurrentDate, "DD.MM.YYYY")
End Sub

Private Sub cmdDateFwd_Click()
    Dim datNew As Date
    Dim strCurrDate As String
    Dim strTmpCurrDate As String
    Dim strTmpEndData As String
    Dim strTmpStartData As String
    
    datNew = DateAdd("d", 1, omCurrentDate)
    strTmpEndData = Format(omEndDate, "YYYYMMDD000000")
    strTmpStartData = Format(omStartDate, "YYYYMMDD000000")
    If datNew <= omEndDate Then
        strTmpCurrDate = Format(datNew, "YYYYMMDD000000")
        omCurrentDate = CedaFullDateToVb(strTmpCurrDate)
        If strTmpCurrDate = strTmpEndData Then
            cmdDateFwd.Enabled = False
        Else
            cmdDateFwd.Enabled = True
        End If
        If strTmpStartData < strTmpCurrDate Then
            cmdDateBack.Enabled = True
        Else
            cmdDateBack.Enabled = False
        End If
        Show3D
    End If
    lblCurrentView = "Current Date: " + Format(omCurrentDate, "DD.MM.YYYY")
End Sub


Private Sub cmdPrint_Click()
    Dim oldFrmColor As Long
    Dim ollblColor As Long
    Dim oldOrient As Integer
    Dim myDrawMode As Integer
    Dim oldScaleH As Integer
    Dim oldScaleW As Integer
    
    CommonDialog1.CancelError = True
    On Error Resume Next
    ' display the dialog
    CommonDialog1.ShowPrinter
    If Err.Number = 32755 Then Exit Sub ' user cancelled
'    If CommonDialog1.Orientation = cdlLandscape Then
'        Printer.Orientation = cdlLandscape
'    Else
'        Printer.Orientation = cdlPortrait
'    End If
    
    
    Me.Hide
    oldScaleH = Me.ScaleHeight
    oldScaleW = Me.ScaleWidth
    Me.Move Me.left, Me.top, 16035, 10440
    cmdPrint.Visible = False
    lblTooltip.Visible = False
    cmdDateBack.Visible = False
    cmdDateFwd.Visible = False
    lblAppearance.Visible = False
    cbChart.Visible = False
    oldFrmColor = Me.backColor
    ollblColor = lblHeader.backColor
    Me.backColor = vbWhite
    lblHeader.backColor = vbWhite
    lblCurrentView.backColor = vbWhite
    oldOrient = Printer.Orientation
    chart.left = chart.left + 500
    lblHeader.left = lblHeader.left + 500
    lblCurrentView.left = lblCurrentView.left + 500
    lblCurrentView.BorderStyle = 0
    Printer.Orientation = vbPRORLandscape
    Me.PrintForm
    chart.left = chart.left - 500
    lblHeader.left = lblHeader.left - 500
    lblCurrentView.left = lblCurrentView.left - 500
    Printer.Orientation = oldOrient
    cmdPrint.Visible = True
    lblTooltip.Visible = True
    cmdDateBack.Visible = True
    cmdDateFwd.Visible = True
    Me.backColor = oldFrmColor
'    lblAppearance.Visible = True
'    cbChart.Visible = True
    lblHeader.backColor = ollblColor
    lblCurrentView.backColor = ollblColor
    lblCurrentView.BorderStyle = 1
    Printer.EndDoc
 '   Me.Visible = True
    Me.Move Me.left, Me.top, oldScaleW, oldScaleH
    Me.Show
End Sub



Private Sub Form_Load()
    Dim fromDate As Date
    Dim toDate As Date
    Dim strTmpDate As String
    Dim strVal As String
    Dim ilLeft As Integer
    Dim ilTop As Integer
    Dim ilWidth As Integer
    Dim ilHeight As Integer
    
'Read position from registry and move the window
'    ilLeft = GetSetting(AppName:="HUB_Manager_HourlyPAX", _
'                        section:="Startup", _
'                        Key:="myLeft", _
'                        Default:=-1)
'    ilTop = GetSetting(AppName:="HUB_Manager_HourlyPAX", _
'                        section:="Startup", _
'                        Key:="myTop", _
'                        Default:=-1)
'    ilWidth = GetSetting(AppName:="HUB_Manager_HourlyPAX", _
'                        section:="Startup", _
'                        Key:="myWidth", _
'                        Default:=-1)
'    ilHeight = GetSetting(AppName:="HUB_Manager_HourlyPAX", _
'                        section:="Startup", _
'                        Key:="myHeight", _
'                        Default:=-1)
'    If ilTop = -1 Or ilLeft = -1 Or ilWidth = -1 Or ilHeight = -1 Then
'    Else
'        Me.Move ilLeft, ilTop, ilWidth, ilHeight
'    End If
'END: Read position from registry and move the window
    
    
    ' trick is to hide the style number in the right part
    ' to get it back by clicking the combobox and extract it
    ' to set the correct enum value
    strVal = "2D Area                                                     |" & VtChChartType2dArea
    cbChart.AddItem strVal
    strVal = "2D Bar                                                     |" & VtChChartType2dBar
    cbChart.AddItem strVal
    strVal = "2D Line                                                     |" & VtChChartType2dLine
    cbChart.AddItem strVal
    strVal = "2D Pie                                                     |" & VtChChartType2dPie
    cbChart.AddItem strVal
    strVal = "2D Step                                                     |" & VtChChartType2dStep
    cbChart.AddItem strVal
    strVal = "3D Area                                                     |" & VtChChartType3dArea
    cbChart.AddItem strVal
    strVal = "3D Bar                                                     |" & VtChChartType3dBar
    cbChart.AddItem strVal
    strVal = "3D Line                                                     |" & VtChChartType3dLine
    cbChart.AddItem strVal
    strVal = "3D Step                                                     |" & VtChChartType3dStep
    cbChart.AddItem strVal

    
    cbRelatedTimes.Clear
    cbRelatedTimes.AddItem "Scheduled"
    cbRelatedTimes.AddItem "Estimated"
    cbRelatedTimes.AddItem "Actual"
    cbRelatedTimes.Text = "Scheduled"
    cbType.Clear
    cbType.AddItem "Arrival"
    cbType.AddItem "Departure"
    cbType.AddItem "Ground Timeframe"
    cbType.Text = "Arrival"

    strCurrType = "Arrival"
    strCurrRelTime = "Scheduled"
    
    fromDate = CedaFullDateToVb(strTimeFrameFrom)
    toDate = CedaFullDateToVb(strTimeFrameTo)
    omStartDate = fromDate
    omEndDate = toDate
    strTmpDate = Format(fromDate, "YYYYMMDD000000")
    omCurrentDate = CedaFullDateToVb(strTmpDate)
    cmdDateBack.Enabled = False
    Show3D
    SimpleListShown = False
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    Dim ilLeft As Integer
    Dim ilTop As Integer
    Dim ilHeight As Integer
    Dim ilWidth As Integer
    
    ilLeft = Me.left
    ilTop = Me.top
    ilWidth = Me.Width
    ilHeight = Me.Height
'    SaveSetting "HUB_Manager_HourlyPAX", _
'                "Startup", _
'                "myLeft", _
'                ilLeft
'    SaveSetting "HUB_Manager_HourlyPAX", _
'                "Startup", _
'                "myTop", ilTop
'    SaveSetting "HUB_Manager_HourlyPAX", _
'                "Startup", _
'                "myWidth", ilWidth
'    SaveSetting "HUB_Manager_HourlyPAX", _
'                "Startup", _
'                "myHeight", ilHeight
End Sub

Private Sub Form_Resize()
    Dim llLeftA As Long
    Dim llLabel As Long
    Dim llPrint As Long
    Dim llChart As Long
    
    llLeftA = 375
    llLabel = 375
    llChart = 600
    cmdDateBack.top = Me.ScaleHeight - 575
    cmdDateFwd.top = Me.ScaleHeight - 575
    lblCurrentView.top = Me.ScaleHeight - 575
    cmdPrint.top = Me.ScaleHeight - 575
    chart.Height = (Me.ScaleHeight - 800) - chart.top
    cmdPrint.left = Me.ScaleWidth - 1455
    chart.Width = (Me.ScaleWidth - 540) - chart.left
    lblAppearance.left = Me.ScaleWidth - 3855
    cbChart.left = Me.ScaleWidth - 2175
End Sub

Private Sub Form_Unload(Cancel As Integer)
    frmMain.bgHourlyIsActive = False
    frmMain.RefreshAllControls
End Sub


Private Sub Show3D()
    Dim i As Long, ilCol As Long
    Dim c As Integer
    Dim ilVal As Integer
    Dim strVal As String
    Dim ilCharVal As Integer
    Dim strTif As String
    Dim strAdid As String
    Dim strUrno As String
    Dim strStoa As String
    Dim strCurrDate As String
    Dim strDate As String
    Dim datStoa As Date
    
    lblCurrentView = "Current Date: " + Format(omCurrentDate, "DD.MM.YYYY")
    Me.MousePointer = vbHourglass
    
    For i = 0 To 1440
        ig1440Values(i) = 0
    Next i
    For i = 0 To 24
        ig24Values(i) = 0
    Next i

'load the data for the char object and add values to the bars
'Initial it will be done for the arrivals
    For i = 0 To frmData.tabData(0).GetLineCount - 1
        strAdid = frmData.tabData(0).GetFieldValue(i, "ADID")
        If (strAdid = "A") Then
            strTif = frmData.tabData(0).GetFieldValue(i, "TIFA")
            strUrno = frmData.tabData(0).GetFieldValue(i, "URNO")
            strStoa = frmData.tabData(0).GetFieldValue(i, "STOA")
            If Trim(strStoa) <> "" Then
                datStoa = CedaFullDateToVb(strStoa)
            End If
            strDate = Mid(strTif, 1, 8)
            strCurrDate = Format(omCurrentDate, "YYYYMMDD")
            Select Case (strCurrType)
                Case "Arrival"
                    HandleArrival strUrno, datStoa
                Case "Departure"
                    HandleDeparture strUrno, datStoa
                Case "Ground Timeframe"
                    HandleGroundTime strUrno, datStoa
            End Select
        End If
    Next i
'**Init the chart object
    If chart.Chart3d = False Then
        chart.RowCount = 24
        chart.ColumnCount = 1
        For i = 1 To 24
            chart.column = ilCol + 1
            chart.Row = i
            chart.RowLabel = CStr(i - 1) + ":00"
            chart.column = 1
            chart.Data = ig24Values(i - 1)
            c = chart.RowLabelCount
        Next i
    Else
        chart.RowCount = 1440
        chart.ColumnCount = 1
        For i = 1 To 1440
            chart.column = ilCol + 1
            chart.Row = i
            If i Mod 60 = 0 Then
                chart.RowLabel = CStr(i / 60) + ":00"
            Else
                chart.RowLabel = ""
            End If
            chart.Data = ig1440Values(i - 1)
            c = chart.RowLabelCount
        Next i
    End If
    
    chart.ShowLegend = False
    lblHeader = "Transfer Pax: " & CedaFullDateToVb(strTimeFrameFrom) & _
                " - " & CedaFullDateToVb(strTimeFrameTo)
    Me.MousePointer = 0
End Sub

Public Sub HandleArrival(strAftUrno As String, opStoa As Date)
    Dim llLine As Long
    Dim llAftLine As Long
    Dim strRet As String
    Dim strFlno As String
    Dim strRawFlno As String
    Dim strPriorFlno As String
    Dim strDepDate As String
    Dim strExTime As String
    Dim strFlightDay As String
    Dim strCurrDate  As String
    Dim strAdid As String
    Dim strVALU As String
    Dim i As Long
    Dim llMinute As Long
    Dim llHour As Long
    Dim idxArray As Long
    Dim strFDate As String
    
    strCurrDate = Format(omCurrentDate, "YYYYMMDD")
    
    idxArray = Val(Format(opStoa, "HH"))
    strRet = frmData.tabData(3).GetLinesByIndexValue("FLNU", strAftUrno, 0)
    llLine = frmData.tabData(3).GetNextResultLine
    While llLine <> -1
        strFlno = frmData.tabData(3).GetFieldValue(llLine, "FLNO")
        strFlightDay = MakeCorrectFlno(strFlno)
        If strFlno <> strPriorFlno Then
            llAftLine = frmMain.GetCorrectDepartureByFlightDay(opStoa, strFlightDay, strFlno)
        End If
        If llAftLine <> -1 Then
            strAdid = frmData.tabData(0).GetFieldValue(llAftLine, "ADID")
            If strAdid = "D" Then 'only if departure
                strFDate = frmData.tabData(0).GetFieldValue(llAftLine, GetRelTimeField_Dep())
                If strFDate = "" Then
                    strFDate = frmData.tabData(0).GetFieldValue(llAftLine, "STOD")
                End If
                strExTime = strFDate
                strExTime = Mid(strExTime, 1, 8)
                If strCurrDate = strExTime Then
                    If chart.Chart3d = False Then
                        strVALU = frmData.tabData(3).GetFieldValue(llLine, "VALU")
                        If idxArray < 25 Then
                            ig24Values(idxArray) = ig24Values(idxArray) + Val(strVALU)
                        End If
                    End If
                End If
            End If
        End If
        
        llLine = frmData.tabData(3).GetNextResultLine
        strPriorFlno = strFlno
        '********** To identify equal FLNOs
        strFlno = frmData.tabData(3).GetFieldValue(llLine, "FLNO")
        strFlightDay = MakeCorrectFlno(strFlno)
        llAftLine = -1 'Prevent more than one
    Wend
End Sub
Public Sub HandleDeparture(strAftUrno As String, opStoa As Date)
    Dim llLine As Long
    Dim llAftLine As Long
    Dim strRet As String
    Dim strFlno As String
    Dim strRawFlno As String
    Dim strPriorFlno As String
    Dim strDepDate As String
    Dim strExTime As String
    Dim strFlightDay As String
    Dim strCurrDate  As String
    Dim strAdid As String
    Dim strVALU As String
    Dim i As Long
    Dim llMinute As Long
    Dim llHour As Long
    Dim idxArray As Long
    Dim strFDate As String
    
    strCurrDate = Format(omCurrentDate, "YYYYMMDD")
    
    strRet = frmData.tabData(3).GetLinesByIndexValue("FLNU", strAftUrno, 0)
    llLine = frmData.tabData(3).GetNextResultLine
    While llLine <> -1
        strFlno = frmData.tabData(3).GetFieldValue(llLine, "FLNO")
        strFlightDay = MakeCorrectFlno(strFlno)
        If strFlno <> strPriorFlno Then
            llAftLine = frmMain.GetCorrectDepartureByFlightDay(opStoa, strFlightDay, strFlno)
        End If
        If llAftLine <> -1 Then
            strAdid = frmData.tabData(0).GetFieldValue(llAftLine, "ADID")
            If strAdid = "D" Then 'only if departure
                strFDate = frmData.tabData(0).GetFieldValue(llAftLine, GetRelTimeField_Dep())
                If strFDate = "" Then
                    strFDate = frmData.tabData(0).GetFieldValue(llAftLine, "STOD")
                End If
                strExTime = strFDate
                strExTime = Mid(strExTime, 1, 8)
                If strCurrDate = strExTime Then
                    If chart.Chart3d = False Then
                        strVALU = frmData.tabData(3).GetFieldValue(llLine, "VALU")
                        idxArray = Val(Mid(strFDate, 9, 2))
                        If idxArray < 25 Then
                            ig24Values(idxArray) = ig24Values(idxArray) + Val(strVALU)
                        End If
                    End If
                End If
            End If
        End If
        
        llLine = frmData.tabData(3).GetNextResultLine
        strPriorFlno = strFlno
        '********** To identify equal FLNOs
        strFlno = frmData.tabData(3).GetFieldValue(llLine, "FLNO")
        strFlightDay = MakeCorrectFlno(strFlno)
        llAftLine = -1 'Prevent more than one
    Wend
End Sub
Public Sub HandleGroundTime(strAftUrno As String, opStoa As Date)
    Dim llLine As Long
    Dim llAftLine As Long
    Dim strRet As String
    Dim strFlno As String
    Dim strRawFlno As String
    Dim strPriorFlno As String
    Dim strDepDate As String
    Dim strExTime As String
    Dim strFlightDay As String
    Dim strCurrDate  As String
    Dim strAdid As String
    Dim strVALU As String
    Dim i As Long
    Dim llMinute As Long
    Dim llHour As Long
    Dim idxArray As Long
    Dim strFDate As String
    Dim datArrival As Date
    Dim datDeparture As Date
    Dim cntHours As Long
    Dim llStartHour As Long
    
    strCurrDate = Format(omCurrentDate, "YYYYMMDD")
    
    'Get the arrival Time
    strRet = frmData.tabData(0).GetLinesByIndexValue("URNO", strAftUrno, 0)
    llLine = frmData.tabData(0).GetNextResultLine
    If llLine > -1 Then
        strFDate = frmData.tabData(0).GetFieldValue(llLine, GetRelTimeField_Arr())
        If Trim(strFDate) = "" Then
            strFDate = frmData.tabData(0).GetFieldValue(llLine, "STOA")
        End If
    End If
    llStartHour = Val(Mid(strFDate, 9, 2))
    
    datArrival = CedaFullDateToVb(strFDate)
    
    strRet = frmData.tabData(3).GetLinesByIndexValue("FLNU", strAftUrno, 0)
    llLine = frmData.tabData(3).GetNextResultLine
    While llLine <> -1
        strFlno = frmData.tabData(3).GetFieldValue(llLine, "FLNO")
        strFlightDay = MakeCorrectFlno(strFlno)
        If strFlno <> strPriorFlno Then
            llAftLine = frmMain.GetCorrectDepartureByFlightDay(opStoa, strFlightDay, strFlno)
        End If
        If llAftLine <> -1 Then
            strAdid = frmData.tabData(0).GetFieldValue(llAftLine, "ADID")
            If strAdid = "D" Then 'only if departure
                strFDate = frmData.tabData(0).GetFieldValue(llAftLine, GetRelTimeField_Dep())
                If strFDate = "" Then
                    strFDate = frmData.tabData(0).GetFieldValue(llAftLine, "STOD")
                End If
                strExTime = strFDate
                strExTime = Mid(strExTime, 1, 8)
                If strCurrDate = strExTime Then
                    If chart.Chart3d = False Then
                        datDeparture = CedaFullDateToVb(strFDate)
                        cntHours = DateDiff("h", datArrival, datDeparture)
                        strVALU = frmData.tabData(3).GetFieldValue(llLine, "VALU")
                        idxArray = llStartHour 'Val(Mid(strFDate, 9, 2))
                        For i = 0 To cntHours - 1
                            If idxArray < 25 Then
                                ig24Values(idxArray) = ig24Values(idxArray) + Val(strVALU)
                            End If
                            idxArray = idxArray + 1
                        Next i
                    End If
                End If
            End If
        End If
        
        llLine = frmData.tabData(3).GetNextResultLine
        strPriorFlno = strFlno
        '********** To identify equal FLNOs
        strFlno = frmData.tabData(3).GetFieldValue(llLine, "FLNO")
        strFlightDay = MakeCorrectFlno(strFlno)
        llAftLine = -1 'Prevent more than one
    Wend
End Sub
'----------------------------------------------------------------------
' Strips the FLNO and extracts the flight day which will be returned
'----------------------------------------------------------------------
Public Function MakeCorrectFlno(ByRef strRawFlno As String) As String
    Dim dummy1 As String, dummy2 As String, dummy3 As String
    Dim arr() As String
    Dim strFlno As String
    Dim strFlightDay As String

    arr = Split(strRawFlno, "/")
    If UBound(arr) = 1 Then
        strFlno = arr(0)
        strFlightDay = arr(1)
    Else
        strFlno = strRawFlno
    End If
    strFlno = StripAftFlno(strFlno, dummy1, dummy2, dummy3)
    strRawFlno = strFlno
    MakeCorrectFlno = strFlightDay
End Function

Public Function GetRelTimeField_Dep()
    If strCurrRelTime = "Scheduled" Then strCurrTimeField = "STOD"
    If strCurrRelTime = "Estimated" Then strCurrTimeField = "ETDI"
    If strCurrRelTime = "Actual" Then strCurrTimeField = "TIFD"
    GetRelTimeField_Dep = strCurrTimeField
End Function

Public Function GetRelTimeField_Arr()
    If strCurrRelTime = "Scheduled" Then strCurrTimeField = "STOA"
    If strCurrRelTime = "Estimated" Then strCurrTimeField = "ETAI"
    If strCurrRelTime = "Actual" Then strCurrTimeField = "TIFA"
    GetRelTimeField_Arr = strCurrTimeField
End Function

