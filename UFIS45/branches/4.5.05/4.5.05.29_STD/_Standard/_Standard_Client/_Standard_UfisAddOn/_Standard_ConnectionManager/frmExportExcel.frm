VERSION 5.00
Begin VB.Form frmExportExcel 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Select Report Type"
   ClientHeight    =   1185
   ClientLeft      =   6840
   ClientTop       =   7260
   ClientWidth     =   6030
   Icon            =   "frmExportExcel.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1185
   ScaleWidth      =   6030
   ShowInTaskbar   =   0   'False
   Begin VB.OptionButton opt2 
      Caption         =   "Sort by flight"
      Height          =   495
      Left            =   480
      TabIndex        =   3
      Top             =   600
      Width           =   3255
   End
   Begin VB.OptionButton opt1 
      Caption         =   "Sort by Passenger Name"
      Height          =   495
      Left            =   480
      TabIndex        =   2
      Top             =   120
      Value           =   -1  'True
      Width           =   3255
   End
   Begin VB.CommandButton CancelButton 
      Caption         =   "Cancel"
      Height          =   375
      Left            =   4680
      TabIndex        =   1
      Top             =   600
      Width           =   1215
   End
   Begin VB.CommandButton OKButton 
      Caption         =   "OK"
      Height          =   375
      Left            =   4680
      TabIndex        =   0
      Top             =   120
      Width           =   1215
   End
End
Attribute VB_Name = "frmExportExcel"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Option Explicit

Private Sub CancelButton_Click()
    frmMain.cmdExcel.backColor = vbButtonFace
    frmMain.cmdExcel.Value = 0
    Unload frmExportExcel
End Sub

Private Sub OKButton_Click()
    If opt1.Value = True Then
        frmMain.expExcel "PN"
    Else
        If opt2.Value = True Then
            frmMain.expExcel "FLT"
        End If
    End If
    frmMain.cmdExcel.backColor = vbButtonFace
    frmMain.cmdExcel.Value = 0
    Unload frmExportExcel
End Sub

Private Sub Form_Unload(cancel As Integer)
    frmMain.cmdExcel.backColor = vbButtonFace
    frmMain.cmdExcel.Value = 0
    Unload frmExportExcel
End Sub

