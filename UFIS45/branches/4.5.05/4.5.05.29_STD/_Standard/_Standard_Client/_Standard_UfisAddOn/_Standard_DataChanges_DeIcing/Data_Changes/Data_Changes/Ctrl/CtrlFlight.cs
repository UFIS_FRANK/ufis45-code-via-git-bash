﻿using System;
using System.Collections.Generic;
using System.Text;

using Ufis.Data;
using Ufis.Utils;

namespace Data_Changes.Ctrl
{
    public class CtrlFlight
    {
        public ITable GetFlightTable()
        {
            IDatabase myDB = UT.GetMemDB();
            ITable aftTable = myDB["AFT"];
            return aftTable;
        }

        public void LoadFlightData()
        {
        }

        public bool HasFlightData()
        {
            ITable aftTable = GetFlightTable();
            bool has = false;
            if ((aftTable == null) || (aftTable.Count < 1)) has = false;
            else has = true;

            return has;
        }

        public IRow GetFlightRow(string flightId)
        {
            IRow row = null;
            ITable aftTable = GetFlightTable();
            if (aftTable != null && aftTable.Count > 0)
            {
                IRow[] aftRows = aftTable.RowsByIndexValue("URNO", flightId);
                if (aftRows.Length > 0)
                {
                    row = aftRows[0];
                }
            }

            return row;

        }
    }
}
