VERSION 5.00
Begin VB.Form frmSetup 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Setup ..."
   ClientHeight    =   3210
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   6960
   Icon            =   "frmSetup.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3210
   ScaleWidth      =   6960
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.PictureBox pic 
      AutoRedraw      =   -1  'True
      Height          =   3210
      Left            =   0
      ScaleHeight     =   3150
      ScaleWidth      =   6885
      TabIndex        =   0
      Top             =   0
      Width           =   6945
      Begin VB.TextBox txtMin 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Index           =   5
         Left            =   5040
         MaxLength       =   3
         TabIndex        =   13
         Text            =   "30"
         Top             =   2160
         Width           =   1275
      End
      Begin VB.TextBox txtMin 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Index           =   4
         Left            =   5040
         MaxLength       =   3
         TabIndex        =   12
         Text            =   "30"
         Top             =   1260
         Width           =   1275
      End
      Begin VB.TextBox txtMin 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Index           =   3
         Left            =   5040
         MaxLength       =   3
         TabIndex        =   11
         Text            =   "30"
         Top             =   1710
         Width           =   1275
      End
      Begin VB.TextBox txtMin 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Index           =   1
         Left            =   1545
         MaxLength       =   3
         TabIndex        =   8
         Text            =   "30"
         Top             =   1710
         Width           =   1275
      End
      Begin VB.TextBox txtMin 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Index           =   0
         Left            =   1545
         MaxLength       =   3
         TabIndex        =   3
         Text            =   "30"
         Top             =   1260
         Width           =   1275
      End
      Begin VB.TextBox txtMin 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Index           =   2
         Left            =   1545
         MaxLength       =   3
         TabIndex        =   2
         Text            =   "30"
         Top             =   2160
         Width           =   1275
      End
      Begin VB.CommandButton cmdSave 
         Caption         =   "&Save"
         Default         =   -1  'True
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   2933
         TabIndex        =   1
         Top             =   2700
         Width           =   1095
      End
      Begin VB.Label Label9 
         BackStyle       =   0  'Transparent
         Caption         =   "Cargo:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   4050
         TabIndex        =   16
         Top             =   2205
         Width           =   915
      End
      Begin VB.Label Label4 
         BackStyle       =   0  'Transparent
         Caption         =   "Mail:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   4050
         TabIndex        =   15
         Top             =   1755
         Width           =   915
      End
      Begin VB.Label Label3 
         BackStyle       =   0  'Transparent
         Caption         =   "Baggage:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   4050
         TabIndex        =   14
         Top             =   1305
         Width           =   915
      End
      Begin VB.Label Label2 
         BackStyle       =   0  'Transparent
         Caption         =   "Departure:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00C000C0&
         Height          =   195
         Left            =   3735
         TabIndex        =   10
         Top             =   945
         Width           =   1005
      End
      Begin VB.Label Label1 
         BackStyle       =   0  'Transparent
         Caption         =   "Arrival:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00C000C0&
         Height          =   195
         Left            =   180
         TabIndex        =   9
         Top             =   900
         Width           =   825
      End
      Begin VB.Line Line1 
         BorderWidth     =   2
         X1              =   3510
         X2              =   3510
         Y1              =   495
         Y2              =   2565
      End
      Begin VB.Label Label8 
         BackStyle       =   0  'Transparent
         Caption         =   "Baggage:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   555
         TabIndex        =   7
         Top             =   1305
         Width           =   915
      End
      Begin VB.Label Label7 
         BackStyle       =   0  'Transparent
         Caption         =   "Mail:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   555
         TabIndex        =   6
         Top             =   1755
         Width           =   915
      End
      Begin VB.Label Label6 
         BackStyle       =   0  'Transparent
         Caption         =   "Cargo:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   555
         TabIndex        =   5
         Top             =   2205
         Width           =   915
      End
      Begin VB.Label Label5 
         BackColor       =   &H80000007&
         Caption         =   "Please specify the on-block and STD/ETD timeframe in minutes for automatic confirmation conflicts"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H0000FF00&
         Height          =   510
         Left            =   0
         TabIndex        =   4
         Top             =   0
         Width           =   6885
      End
   End
End
Attribute VB_Name = "frmSetup"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdSave_Click()
    frmData.tabCfg.ResetContent
    frmData.tabCfg.InsertTextLine "ARR_BAGGAGE," & txtMin(0), False
    frmData.tabCfg.InsertTextLine "ARR_MAIL," & txtMin(1), False
    frmData.tabCfg.InsertTextLine "ARR_CARGO," & txtMin(2), False
    frmData.tabCfg.InsertTextLine "DEP_BAGGAGE," & txtMin(3), False
    frmData.tabCfg.InsertTextLine "DEP_MAIL," & txtMin(4), False
    frmData.tabCfg.InsertTextLine "DEP_CARGO," & txtMin(5), False
    strARR_BaggageDefault = Val(txtMin(0))
    strARR_MailDefault = Val(txtMin(1))
    strARR_CargoDefault = Val(txtMin(2))
    strDEP_BaggageDefault = Val(txtMin(3))
    strDEP_MailDefault = Val(txtMin(4))
    strDEP_CargoDefault = Val(txtMin(5))
    'frmData.tabCfg.WriteToFile "C:\Ufis\Appl\ULDMANAGER.cfg", False
    frmData.tabCfg.WriteToFile UFIS_APPL & "\ULDMANAGER.cfg", False
    Unload Me
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyEscape Then
        Unload Me
    End If
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    If KeyAscii = Chr(vbKeyEscape) Then
        Unload Me
    End If
End Sub

Private Sub Form_Load()
    DrawBackGround pic, 7, True, True, 40
    txtMin(0) = strARR_BaggageDefault
    txtMin(1) = strARR_MailDefault
    txtMin(2) = strARR_CargoDefault
    txtMin(3) = strDEP_BaggageDefault
    txtMin(4) = strDEP_MailDefault
    txtMin(5) = strDEP_CargoDefault
End Sub

Private Sub txtMin_KeyPress(Index As Integer, KeyAscii As Integer)
    If Not ((KeyAscii >= 48 And KeyAscii <= 57) Or KeyAscii = 8) Then
        KeyAscii = 0
    End If
End Sub
