Attribute VB_Name = "MainModule"
Option Explicit
Public strTimeFrameFrom As String
Public strTimeFrameTo As String
Public datTimeMarkerStart As Date
Public datTimeMarkerEnd As Date
Public strTableExt  As String
Public strAppl As String
Public strHopo As String
Public strServer As String
Public strTestServer As String
Public strTestHopo As String
Public strAllFtyps As String
Public strShowHiddenData As String
Public readFromFile As Boolean

Public strCurrentUser As String

Public igTmpIDX As Integer
Public igAPP_IDX As Integer

Public UTCOffset As Integer

'********
'Colors in FIPS
Public colGray As Long
Public colLightGray As Long
Public colWhite As Long
Public colYellow As Long
Public colGreen As Long
Public colOrange As Long
Public colLime As Long
Public colBlue As Long
Public colLightBlue As Long
Public colorCorrespondingBar As Long

'***CFG DATA
Public colorArrivalBar As Long
Public colorDepartureBar As Long
Public colorCurrentBar As Long
Public DetailGanttTimeFrame As Long
Public MinDepartureBarLen As Long 'Minumum length of the departure bar
Public MinArrivalBarLen As Long   'Minumum length of the arrival bar
Public ShowHints As Boolean       ' shows hints or not
Public TimesInUTC As Boolean       ' True if UTC, false if local time
                                 ' shall be displayed
                                 
Public strReadLoadFrom As String
Public strReadLoadTo As String

Public strUserTimeFrameFrom As String
Public strUserTimeFrameTo As String
                                 
'***END CFG DATA
Public strScriptSourceCode As String
Public dataFromFile As Boolean
'***Default connectio times
Public strARR_BaggageDefault As String
Public strARR_MailDefault As String
Public strARR_CargoDefault As String
Public strDEP_BaggageDefault As String
Public strDEP_MailDefault As String
Public strDEP_CargoDefault As String
Public strAftTimeFields As String ' to be used for UTC to local convertation
Public bgAutoLogin As Boolean

Public bgfrmCompressedFlightsOpen As Boolean

Public Sub Main()
    Dim strAutoLogin As String
    
    dataFromFile = True
    'TO DO: Read setup values from ceda.ini or registry
    MinDepartureBarLen = 15
    MinArrivalBarLen = 15
    ShowHints = True
    colorCurrentBar = 16711935
    colorCorrespondingBar = vbYellow
    colorArrivalBar = 16777215
    colorDepartureBar = 16777152
    DetailGanttTimeFrame = 2 'hours
    strServer = GetIniEntry("", "ULDMANAGER", "GLOBAL", "HOSTNAME", "")
    'TO DO: Remove all strTestServer and strTestHopo
    strTestServer = "Tonga"
    strTestHopo = "ATH"
    strHopo = GetIniEntry("", "ULDMANAGER", "GLOBAL", "HOMEAIRPORT", "")
    strTableExt = GetIniEntry("", "ULDMANAGER", "GLOBAL", "TABLEEXTENSION", "")
    strShowHiddenData = GetIniEntry("", "ULDMANAGER", "GLOBAL", "HIDDENDATA", "")
    strAutoLogin = GetIniEntry("", "ULDMANAGER", "GLOBAL", "AUTOLOGIN", "")
    readFromFile = False
    strAppl = "ULD_MGR"

    strAllFtyps = "S,O,OO,OS,SO"
    strAftTimeFields = "STOA,STOD,TIFA,TIFD,TMOA,ETAI,ETDI,ONBL,OFBL,PABS,PAES,PDBS,PDES,LAND,AIRB"
    
    'Login procedure
    If strAutoLogin = "TRUE" Then
        bgAutoLogin = True
    Else
        bgAutoLogin = False
    End If
    If bgAutoLogin = False And InStr(1, command, "/CONNECTED") <> 1 Then
        If frmData.LoginProcedure = False Then
            End
        End If
    End If
    
    strCurrentUser = frmData.ULogin.GetUserName
    If strCurrentUser = "" Then strCurrentUser = GetWindowsUserName
    'strCurrentUser = "Default"
    colGray = RGB(128, 128, 128)
    colWhite = RGB(255, 255, 255)
    colYellow = RGB(255, 255, 0)
    colOrange = RGB(255, 193, 164)
    colGreen = RGB(0, 128, 0)
    colLime = RGB(0, 255, 0)
    colBlue = RGB(0, 0, 255)
    colLightBlue = 16777152
    colLightGray = RGB(235, 235, 235) '14737632
    strARR_BaggageDefault = "30"
    strARR_MailDefault = "30"
    strARR_CargoDefault = "30"
    strDEP_BaggageDefault = "30"
    strDEP_MailDefault = "30"
    strDEP_CargoDefault = "30"
    TimesInUTC = True
    
    StartUpApplication
    bgfrmCompressedFlightsOpen = False
    
    
End Sub

Public Sub StartUpApplication()
    Dim myDate As Date
    
    frmStartup.Show
    UTCOffset = frmData.GetUTCOffset
    frmLoad.Show vbModal, frmStartup
    If frmLoad.wasCancel = True Then
        End
    End If
    frmMain.LoadBCProxy
    frmData.LoadData
    Unload frmStartup
'    frmStartup.Hide
'    frmStartup.Move 99999, 99999
    frmMain.Show
    frmMain.ZOrder
    myDate = CedaFullDateToVb(strReadLoadFrom)
    frmMain.txtArrDateFrom = Format(myDate, "dd.mm.yyyy")
    frmMain.txtArrDateTo = Format(myDate, "dd.mm.yyyy")
    myDate = CedaFullDateToVb(strReadLoadTo)
    frmMain.txtDepDateFrom = Format(myDate, "dd.mm.yyyy")
    frmMain.txtDepDateTo = Format(myDate, "dd.mm.yyyy")
End Sub

Public Sub SetAllFormsOnTop(SetValue As Boolean)
    '
End Sub

Public Sub ShutDownApplication(AskBack As Boolean)
    Dim i As Integer
    Dim cnt As Integer
    Dim RetVal As Integer
    RetVal = 1
    If AskBack Then RetVal = 1 'MyMsgBox.CallAskUser(0, 0, 0, "Application Control", "Do you want to exit ?", "ask", "Yes,No;F", UserAnswer)
    If RetVal = 1 Then
        On Error Resume Next
        ShutDownRequested = True
        cnt = Forms.count - 1
        For i = cnt To 0 Step -1
            Unload Forms(i)
        Next
        'Here we shut down
        End
    End If
    'If not, we come back
End Sub

Public Sub HandleBroadCast(ReqId As String, DestName As String, RecvName As String, CedaCmd As String, ObjName As String, Seq As String, Tws As String, Twe As String, CedaSqlKey As String, Fields As String, Data As String, BcNum As String)
    '
End Sub
Private Sub HandleShutDown(Abort As Boolean)
    If Not Abort Then ShutDownApplication False
    End
End Sub

'********************************
' HELPER FUNCTIONS

'------------------------------------------------------------
' Returns the len for this flight.
' TO DO: returns now the default value, ==> must calculate
' the time in respect to other criteria
Public Function GetArrivalBarLenInMinutes(Urno As String, Tifa As String) As Integer

    GetArrivalBarLenInMinutes = MinArrivalBarLen
End Function
'------------------------------------------------------------
' Returns the len for this flight.
' TO DO: returns now the default value, ==> must calculate
' the time in respect to other criteria
Public Function GetDepartureBarLenInMinutes(Urno As String, Tifa As String) As Integer
    GetDepartureBarLenInMinutes = MinDepartureBarLen
End Function
'TO DO: If necessary for both arr and dep implement this
Public Function HasConnectionConflicts(strAftUrno As String) As Boolean
    HasConnectionConflicts = False
End Function

Public Function DrawBackGround(MyPanel As PictureBox, MyColor As Integer, DrawHoriz As Boolean, DrawDown As Boolean, pPercent As Integer) As Long
    Const intBLUESTART% = 255
    Const intBLUEEND% = 0
    Const intBANDHEIGHT% = 15
    Const intSHADOWSTART% = 64
    Const intSHADOWCOLOR% = 0
    Const intTEXTSTART% = 0
    Const intTEXTCOLOR% = 15
    Const intRed% = 1
    Const intGreen% = 2
    Const intBlue% = 4
    Const intBackRed% = 8
    Const intBackGreen% = 16
    Const intBackBlue% = 32
    Dim sngBlueCur As Single
    Dim sngBlueStep As Single
    Dim intFormHeight As Single
    Dim intFormWidth As Single
    Dim intX As Single
    Dim intY As Single
    Dim iColor As Integer
    Dim iRed As Single, iBlue As Single, iGreen As Single
    Dim prntText As String
    Dim ReturnColor As Long
    ReturnColor = vbWhite
    If MyColor >= 0 Then
        intFormHeight = MyPanel.ScaleHeight
        intFormWidth = MyPanel.ScaleWidth
        If intFormHeight > 0 And intFormWidth > 0 Then
            iColor = MyColor
            sngBlueCur = intBLUESTART
        
            If DrawDown Then
                If DrawHoriz Then
                    sngBlueStep = intBANDHEIGHT * (intBLUEEND - intBLUESTART) / intFormHeight
                    sngBlueStep = sngBlueStep / 100 * pPercent
                    For intY = 0 To intFormHeight Step intBANDHEIGHT
                        If iColor And intBlue Then iBlue = sngBlueCur
                        If iColor And intRed Then iRed = sngBlueCur
                        If iColor And intGreen Then iGreen = sngBlueCur
                        If iColor And intBackBlue Then iBlue = 255 - sngBlueCur
                        If iColor And intBackRed Then iRed = 255 - sngBlueCur
                        If iColor And intBackGreen Then iGreen = 255 - sngBlueCur
                        MyPanel.Line (-1, intY - 1)-(intFormWidth, intY + intBANDHEIGHT), RGB(iRed, iGreen, iBlue), BF
                        sngBlueCur = sngBlueCur + sngBlueStep
                        If intY = 0 Then ReturnColor = RGB(iRed, iGreen, iBlue)
                    Next intY
                Else
                    sngBlueStep = intBANDHEIGHT * (intBLUEEND - intBLUESTART) / intFormWidth
                    sngBlueStep = sngBlueStep / 100 * pPercent
                    For intX = 0 To intFormWidth Step intBANDHEIGHT
                        If iColor And intBlue Then iBlue = sngBlueCur
                        If iColor And intRed Then iRed = sngBlueCur
                        If iColor And intGreen Then iGreen = sngBlueCur
                        If iColor And intBackBlue Then iBlue = 255 - sngBlueCur
                        If iColor And intBackRed Then iRed = 255 - sngBlueCur
                        If iColor And intBackGreen Then iGreen = 255 - sngBlueCur
                        MyPanel.Line (intX - 1, -1)-(intX + intBANDHEIGHT, intFormHeight), RGB(iRed, iGreen, iBlue), BF
                        sngBlueCur = sngBlueCur + sngBlueStep
                    Next intX
                End If
            Else
                If DrawHoriz Then
                    sngBlueStep = intBANDHEIGHT * (intBLUEEND - intBLUESTART) / intFormHeight
                    sngBlueStep = sngBlueStep / 100 * pPercent
                    For intY = intFormHeight To 0 Step -intBANDHEIGHT
                        If iColor And intBlue Then iBlue = sngBlueCur
                        If iColor And intRed Then iRed = sngBlueCur
                        If iColor And intGreen Then iGreen = sngBlueCur
                        If iColor And intBackBlue Then iBlue = 255 - sngBlueCur
                        If iColor And intBackRed Then iRed = 255 - sngBlueCur
                        If iColor And intBackGreen Then iGreen = 255 - sngBlueCur
                        MyPanel.Line (-1, intY - 1)-(intFormWidth, intY + intBANDHEIGHT), RGB(iRed, iGreen, iBlue), BF
                        sngBlueCur = sngBlueCur + sngBlueStep
                    Next intY
                Else
                    sngBlueStep = intBANDHEIGHT * (intBLUEEND - intBLUESTART) / intFormWidth
                    sngBlueStep = sngBlueStep / 100 * pPercent
                    For intX = intFormWidth To 0 Step -intBANDHEIGHT
                        If iColor And intBlue Then iBlue = sngBlueCur
                        If iColor And intRed Then iRed = sngBlueCur
                        If iColor And intGreen Then iGreen = sngBlueCur
                        If iColor And intBackBlue Then iBlue = 255 - sngBlueCur
                        If iColor And intBackRed Then iRed = 255 - sngBlueCur
                        If iColor And intBackGreen Then iGreen = 255 - sngBlueCur
                        MyPanel.Line (intX - 1, -1)-(intX + intBANDHEIGHT, intFormHeight), RGB(iRed, iGreen, iBlue), BF
                        sngBlueCur = sngBlueCur + sngBlueStep
                    Next intX
                End If
            End If
        End If
    End If
    DrawBackGround = ReturnColor
End Function


