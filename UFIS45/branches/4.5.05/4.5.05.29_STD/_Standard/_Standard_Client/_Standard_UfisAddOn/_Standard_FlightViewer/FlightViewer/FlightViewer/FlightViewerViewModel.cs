﻿
using System;
using System.Linq;
using System.ComponentModel;
using System.Windows.Data;
using System.Windows.Input;
using System.Collections.Generic;
using Ufis.Data;
using Ufis.Entities;
using Ufis.MVVM.ViewModel;
using FlightViewer.Helpers;
using FlightViewer.DataAccess;
using Ufis.Utilities;
using Ufis.Data.Ceda;

namespace FlightViewer.FlightViewer
{
    /// <summary>
    /// The ViewModel for the application's main window.
    /// </summary>
    public class FlightViewerViewModel : WorkspaceViewModel
    {
        #region Private or Public Variable(s) Declaration 

        public WorkspaceViewModel _workspace;         
        
        #endregion

        #region Item Source
        public EntityCollectionBase<EntDbSeason> Season { get; protected set; }
        #endregion

        #region Constructor & Methods
        public FlightViewerViewModel()
        {
            //Disply Project name at title bar
            DisplayName = HpAppInfo.Current.ProductTitle;
            //Bind data to combo box
            Season = DlFlightViewer.GetSeason();
        }
        #endregion // Constructor

        #region Workspace

        public WorkspaceViewModel Workspace
        {
            get
            {
                return _workspace;
            }
            set
            {
                if (_workspace == value)
                    return;

                //Detach the old workspace event handler
                if (_workspace != null)
                    _workspace.RequestClose -= OnWorkspaceRequestClose;

                //Attach the new workspace event handler
                _workspace = value;
                if (_workspace != null)
                    _workspace.RequestClose += OnWorkspaceRequestClose;

                OnPropertyChanged("Workspace");
            }
        }

        void OnWorkspaceRequestClose(object sender, EventArgs e)
        {
            WorkspaceViewModel workspace = sender as WorkspaceViewModel;
            workspace.Dispose();
        }
        #endregion // Workspaces

        #region UI Control Reference
        /// <summary>
        /// Search Season according to Search criteria
        /// </summary>
        /// <param name="sSeasonName">System.String containing sSeason Name</param>
        /// <param name="sAirport">System.String Containing sValidFrom</param>
        /// <param name="sAirline">System.String Containing sValidTot</param>
        public void SearchSeason(string sSeasonName, DateTime dtValidFrom, DateTime dtValidTo,
                                string sAirportInfo, Boolean bAirportEI, string sFlightInfo, Boolean bFlightEI, char cAirport, char cFlight)
        {
            DataErrorCollection objDE = new DataErrorCollection();
            try
            {
                if (sSeasonName != "")
                {
                    string sAirportEI = "";
                    string sFlightEI = "";

                    string sValidFrom = dtValidFrom.ToString("yyyyMMdd");
                    string sValidTo = dtValidTo.ToString("yyyyMMdd");
                    
                    if (sAirportInfo != "")
                    {
                        sAirportInfo = sAirportInfo.ToUpper();
                        sAirportInfo = string.Format("'{0}'", sAirportInfo.Replace(" ", "','"));
                        if (bAirportEI)
                        {
                            sAirportEI = "IN(";
                        }
                        else
                        {
                            sAirportEI = "NOT IN(";
                        }
                    }
                    if (sFlightInfo != "")
                    {
                        sFlightInfo = sFlightInfo.ToUpper();
                        sFlightInfo = string.Format("'{0}'", sFlightInfo.Replace(" ", "','"));
                        if (bFlightEI)
                        {
                            sFlightEI = "IN(";
                        }
                        else
                        {
                            sFlightEI = "NOT IN(";
                        }
                    }
                    //Get Data
                    ItemSource = DlFlightViewer.GetFlightViewerList(sSeasonName, sValidFrom, sValidTo,
                                                                sAirportInfo, sAirportEI, sFlightInfo, sFlightEI, cAirport, cFlight);
                } 
            }
            catch (Exception ex)
            {
                objDE.Add(new DataError(ex.Message, ex));               
            }
        }

        EntityCollectionBase<EntDbSeasonalFlightSchedule> _itemSource;
        public EntityCollectionBase<EntDbSeasonalFlightSchedule> ItemSource
        {
            get
            {
                return _itemSource;
            }
            protected set
            {
                if (_itemSource != value)
                {
                    _itemSource = value;
                    OnPropertyChanged("ItemSource");
                }
            }
        }
        #endregion
    }
        
}