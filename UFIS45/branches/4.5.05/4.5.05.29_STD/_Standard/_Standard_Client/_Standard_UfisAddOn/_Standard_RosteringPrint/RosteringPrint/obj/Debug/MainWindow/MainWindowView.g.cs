﻿#pragma checksum "..\..\..\MainWindow\MainWindowView.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "07B3439BCE7AA12EF8B295525DE6395A"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.239
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using BDPSUIF.BasicData;
using DevExpress.Core;
using DevExpress.Xpf.Bars;
using DevExpress.Xpf.Core;
using DevExpress.Xpf.Core.Serialization;
using DevExpress.Xpf.Core.ServerMode;
using DevExpress.Xpf.Docking;
using DevExpress.Xpf.Editors;
using DevExpress.Xpf.Editors.DataPager;
using DevExpress.Xpf.Editors.DateNavigator;
using DevExpress.Xpf.Editors.Filtering;
using DevExpress.Xpf.Editors.Popups;
using DevExpress.Xpf.Editors.Popups.Calendar;
using DevExpress.Xpf.Editors.Settings;
using DevExpress.Xpf.Editors.Settings.Extension;
using DevExpress.Xpf.Editors.Validation;
using DevExpress.Xpf.Grid;
using DevExpress.Xpf.Grid.LookUp;
using DevExpress.Xpf.Grid.TreeList;
using DevExpress.Xpf.NavBar;
using DevExpress.Xpf.Ribbon;
using RosteringPrint.Helpers;
using RosteringPrint.MainWindow;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;
using Ufis.MVVM.ViewModel;
using Ufis.Resources;


namespace RosteringPrint.MainWindow {
    
    
    /// <summary>
    /// MainWindowView
    /// </summary>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
    public partial class MainWindowView : System.Windows.Controls.UserControl, System.Windows.Markup.IComponentConnector {
        
        
        #line 27 "..\..\..\MainWindow\MainWindowView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal DevExpress.Xpf.Bars.BarManager barManager;
        
        #line default
        #line hidden
        
        
        #line 29 "..\..\..\MainWindow\MainWindowView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal DevExpress.Xpf.Bars.BarButtonItem barButtonRecordInsert;
        
        #line default
        #line hidden
        
        
        #line 39 "..\..\..\MainWindow\MainWindowView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal DevExpress.Xpf.Bars.BarButtonItem barButtonRecordUpdate;
        
        #line default
        #line hidden
        
        
        #line 49 "..\..\..\MainWindow\MainWindowView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal DevExpress.Xpf.Bars.BarButtonItem barButtonRecordDelete;
        
        #line default
        #line hidden
        
        
        #line 59 "..\..\..\MainWindow\MainWindowView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal DevExpress.Xpf.Bars.BarButtonItem barButtonRecordCopy;
        
        #line default
        #line hidden
        
        
        #line 69 "..\..\..\MainWindow\MainWindowView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal DevExpress.Xpf.Bars.BarButtonItem barButtonFilterRecords;
        
        #line default
        #line hidden
        
        
        #line 74 "..\..\..\MainWindow\MainWindowView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal DevExpress.Xpf.Bars.BarCheckItem barCheckShowAutoFilterRow;
        
        #line default
        #line hidden
        
        
        #line 79 "..\..\..\MainWindow\MainWindowView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal DevExpress.Xpf.Bars.BarButtonItem barButtonPreview;
        
        #line default
        #line hidden
        
        
        #line 85 "..\..\..\MainWindow\MainWindowView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal DevExpress.Xpf.Ribbon.RibbonControl ribbonControl;
        
        #line default
        #line hidden
        
        
        #line 87 "..\..\..\MainWindow\MainWindowView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal DevExpress.Xpf.Ribbon.RibbonPage ribbonPageOperation;
        
        #line default
        #line hidden
        
        
        #line 88 "..\..\..\MainWindow\MainWindowView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal DevExpress.Xpf.Ribbon.RibbonPageGroup ribbonPageGroupOperation;
        
        #line default
        #line hidden
        
        
        #line 94 "..\..\..\MainWindow\MainWindowView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal DevExpress.Xpf.Ribbon.RibbonPageGroup ribbonPageGroupFindAndFilter;
        
        #line default
        #line hidden
        
        
        #line 98 "..\..\..\MainWindow\MainWindowView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal DevExpress.Xpf.Ribbon.RibbonPageGroup ribbonPageGroupPrint;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/RosteringPrint;component/mainwindow/mainwindowview.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\MainWindow\MainWindowView.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.barManager = ((DevExpress.Xpf.Bars.BarManager)(target));
            return;
            case 2:
            this.barButtonRecordInsert = ((DevExpress.Xpf.Bars.BarButtonItem)(target));
            return;
            case 3:
            this.barButtonRecordUpdate = ((DevExpress.Xpf.Bars.BarButtonItem)(target));
            return;
            case 4:
            this.barButtonRecordDelete = ((DevExpress.Xpf.Bars.BarButtonItem)(target));
            return;
            case 5:
            this.barButtonRecordCopy = ((DevExpress.Xpf.Bars.BarButtonItem)(target));
            return;
            case 6:
            this.barButtonFilterRecords = ((DevExpress.Xpf.Bars.BarButtonItem)(target));
            return;
            case 7:
            this.barCheckShowAutoFilterRow = ((DevExpress.Xpf.Bars.BarCheckItem)(target));
            return;
            case 8:
            this.barButtonPreview = ((DevExpress.Xpf.Bars.BarButtonItem)(target));
            
            #line 83 "..\..\..\MainWindow\MainWindowView.xaml"
            this.barButtonPreview.ItemClick += new DevExpress.Xpf.Bars.ItemClickEventHandler(this.barButtonPreview_ItemClick);
            
            #line default
            #line hidden
            return;
            case 9:
            this.ribbonControl = ((DevExpress.Xpf.Ribbon.RibbonControl)(target));
            return;
            case 10:
            this.ribbonPageOperation = ((DevExpress.Xpf.Ribbon.RibbonPage)(target));
            return;
            case 11:
            this.ribbonPageGroupOperation = ((DevExpress.Xpf.Ribbon.RibbonPageGroup)(target));
            return;
            case 12:
            this.ribbonPageGroupFindAndFilter = ((DevExpress.Xpf.Ribbon.RibbonPageGroup)(target));
            return;
            case 13:
            this.ribbonPageGroupPrint = ((DevExpress.Xpf.Ribbon.RibbonPageGroup)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

