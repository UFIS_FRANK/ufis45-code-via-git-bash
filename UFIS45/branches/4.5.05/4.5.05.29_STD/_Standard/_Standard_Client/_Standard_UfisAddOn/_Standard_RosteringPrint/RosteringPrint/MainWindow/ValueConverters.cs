﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Data;
using Ufis.Entities;
using RosteringPrint.Helpers;
using DevExpress.Xpf.Docking;

namespace RosteringPrint.MainWindow
{
    [ValueConversion(typeof(object), typeof(bool))]
    public class CanSaveViewConverter : IValueConverter
    {
        #region IValueConverter Members

        public object Convert(object value, Type targetType,
            object parameter, System.Globalization.CultureInfo culture)
        {
            EntDbUserDefinedLayout udLayout = (EntDbUserDefinedLayout)value;

            return (udLayout != null && udLayout.UserId == HpUser.ActiveUser.UserId);
        }

        public object ConvertBack(object value, Type targetType,
            object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }



    [ValueConversion(typeof(object), typeof(bool))]
    public class CanDoOperationConverter : IValueConverter
    {
        #region IValueConverter Members

        public object Convert(object value, Type targetType,
            object parameter, System.Globalization.CultureInfo culture)
        {
            EntDbUserDefinedLayout udLayout = (EntDbUserDefinedLayout)value;

            return (udLayout != null);
        }

        public object ConvertBack(object value, Type targetType,
            object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
