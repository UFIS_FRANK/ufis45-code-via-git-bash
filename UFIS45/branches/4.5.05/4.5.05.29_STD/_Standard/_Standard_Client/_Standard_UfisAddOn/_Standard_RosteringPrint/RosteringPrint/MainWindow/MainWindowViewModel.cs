﻿using RosteringPrint.Helpers;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Ufis.MVVM.ViewModel;
using Ufis.Security;
using Ufis.Utilities;
using Ufis.MVVM.Command;
using System.Windows.Input;

namespace RosteringPrint.MainWindow
{
    /// <summary>
    /// The ViewModel for the application's main window.
    /// </summary>
    public class MainWindowViewModel : WorkspaceViewModel
    {
        #region Fields

        ReadOnlyCollection<MenuViewModel> _menus;
        WorkspaceViewModel _workspace;

        #endregion // Fields

        #region Constructor

        public MainWindowViewModel()
        {
            DisplayName = HpAppInfo.Current.ProductTitle;
            CreateCommands(HpUser.Privileges);
        }

        #endregion // Constructor

        #region Commands

        /// <summary>
        /// Returns a read-only list of commands 
        /// that the UI can display and execute.
        /// </summary>
        public ReadOnlyCollection<MenuViewModel> Commands
        {
            get
            {
                if (_menus == null)
                {
                    List<MenuViewModel> cmds = CreateCommands(HpUser.Privileges);
                    _menus = new ReadOnlyCollection<MenuViewModel>(cmds);
                }
                return _menus;
            }
        }

        private List<MenuViewModel> CreateCommands(UserPrivileges userPrivileges)
        {
            const string LARGE_IMAGE_URI = "pack://application:,,,/Ufis.Resources.ImageLibrary;v4.5.0.0;8f51493079604dd3;component/application_32x32.png";

            System.Windows.Media.Imaging.BitmapImage largeImage = new System.Windows.Media.Imaging.BitmapImage(new Uri(LARGE_IMAGE_URI));
            largeImage.Freeze();
            MenuViewModel.MenuGroup menuGroup = new MenuViewModel.MenuGroup()
            {
                Caption = "Menu",
                Image = largeImage
            };

            List<MenuViewModel> lstMenuViewModels = new List<MenuViewModel>();
            foreach (AppMenu appMenu in HpMenus.MenuList)
            {
                UserAccessRight userAccessRight = userPrivileges[appMenu.Name];
                if (userAccessRight == UserAccessRight.Unspecified)
                    userAccessRight = UserAccessRight.Hidden;

                if (userAccessRight != UserAccessRight.Hidden)
                {
                    lstMenuViewModels.Add(
                        new MenuViewModel(
                            appMenu.ToString(),
                            new RelayCommand(OpenBasicData))
                        {
                            ImageSmall = appMenu.ImageSmall,
                            ImageLarge = appMenu.ImageLarge,
                            Group = menuGroup
                        });
                }
            }

            return lstMenuViewModels;
        }

        #endregion // Commands

        #region Workspace

        public WorkspaceViewModel Workspace
        {
            get
            {
                return _workspace;
            }
            set
            {
                if (_workspace == value)
                    return;

                //Detach the old workspace event handler
                if (_workspace != null)
                    _workspace.RequestClose -= OnWorkspaceRequestClose;

                //Attach the new workspace event handler
                _workspace = value;
                if (_workspace != null)
                    _workspace.RequestClose += OnWorkspaceRequestClose;

                OnPropertyChanged("Workspace");
            }
        }

        void OnWorkspaceRequestClose(object sender, EventArgs e)
        {
            WorkspaceViewModel workspace = sender as WorkspaceViewModel;

            workspace.Dispose();
        }

        #endregion // Workspaces

        #region Private Helpers

        private void OpenBasicData(object param)
        {
            string strBasicData = param.ToString();

            Mouse.OverrideCursor = Cursors.Wait;
            
            BasicData.BasicDataViewModel workspace = HpBasicData.OpenBasicDataViewModel(strBasicData);
            if (workspace != null)
                Workspace = workspace;

            Mouse.OverrideCursor = null;
        }

        //private void worker_DoWork(object sender, DoWorkEventArgs e)
        //{
        //    FlightRepository repos = new FlightRepository();
        //    FlightDataListViewModel workspace = new FlightDataListViewModel();
        //    workspace.Flights = repos.GetFlights();

        //    this.Workspaces.Add(workspace);
        //    this.Workspace = workspace;

        //    //workspace.IsLoading = true;
        //}

        //private void worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        //{
        //    //DataListViewModel workspace = (DataListViewModel)this.Workspace;
        //    //workspace.IsLoading = false;
        //}

        //void CreateNewCustomer()
        //{
        //    Customer newCustomer = Customer.CreateNewCustomer();
        //    CustomerViewModel workspace = new CustomerViewModel(newCustomer, _customerRepository);
        //    this.Workspaces.Add(workspace);
        //    this.SetActiveWorkspace(workspace);
        //}

        //void ShowAllCustomers()
        //{
        //    AllCustomersViewModel workspace =
        //        this.Workspaces.FirstOrDefault(vm => vm is AllCustomersViewModel)
        //        as AllCustomersViewModel;

        //    if (workspace == null)
        //    {
        //        workspace = new AllCustomersViewModel(_customerRepository);
        //        this.Workspaces.Add(workspace);
        //    }

        //    this.SetActiveWorkspace(workspace);
        //}

        //void SetActiveWorkspace(WorkspaceViewModel workspace)
        //{
        //    if (workspace == null)
        //    {
        //        this.Workspace = workspace;
        //    }
        //    else
        //    {
        //        Debug.Assert(this.Workspaces.Contains(workspace));

        //        ICollectionView collectionView = CollectionViewSource.GetDefaultView(this.Workspaces);
        //        if (collectionView != null)
        //            collectionView.MoveCurrentTo(workspace);
        //    }
        //}
    
        }
        #endregion // Private Helpers
    }