VERSION 5.00
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "TAB.ocx"
Begin VB.Form SeasonData 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Valid Seasons"
   ClientHeight    =   2835
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5595
   BeginProperty Font 
      Name            =   "Courier New"
      Size            =   8.25
      Charset         =   161
      Weight          =   700
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "SeasonData.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   ScaleHeight     =   2835
   ScaleWidth      =   5595
   StartUpPosition =   3  'Windows Default
   Begin VB.CheckBox chkWork 
      Caption         =   "Close"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   0
      Left            =   30
      Style           =   1  'Graphical
      TabIndex        =   1
      Top             =   0
      Width           =   855
   End
   Begin TABLib.TAB SeaList 
      Height          =   2475
      Index           =   0
      Left            =   60
      TabIndex        =   0
      Top             =   330
      Width           =   5535
      _Version        =   65536
      _ExtentX        =   9763
      _ExtentY        =   4366
      _StockProps     =   64
   End
   Begin VB.Frame DlgFrame 
      BorderStyle     =   0  'None
      Height          =   315
      Left            =   0
      TabIndex        =   2
      Top             =   0
      Visible         =   0   'False
      Width           =   5565
      Begin VB.CheckBox chkWork 
         Caption         =   "OK"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   1
         Left            =   4710
         Style           =   1  'Graphical
         TabIndex        =   3
         Top             =   0
         Width           =   855
      End
      Begin VB.Label CurImpFile 
         BackColor       =   &H0000FFFF&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Left            =   2730
         TabIndex        =   5
         Top             =   60
         Width           =   1905
      End
      Begin VB.Label Label1 
         BackColor       =   &H0000FFFF&
         Caption         =   "Select the valid season for file"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Left            =   30
         TabIndex        =   4
         Top             =   60
         Width           =   2655
      End
   End
End
Attribute VB_Name = "SeasonData"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub chkWork_Click(Index As Integer)
    Dim SelLine As Long
    If chkWork(Index).Value = 0 Then chkWork(Index).BackColor = vbButtonFace
    Select Case Index
        Case 0  'Close
            If chkWork(Index).Value = 1 Then Me.Hide
            chkWork(Index).Value = 0
        Case 1  'OK
            If chkWork(Index).Value = 1 Then
                SelLine = SeaList(0).GetCurrentSelected
                If SelLine >= 0 Then
                    DlgFrame.Tag = SeaList(0).GetLineValues(SelLine)
                    Me.Hide
                    chkWork(Index).Value = 0
                End If
            End If
        Case Else
    End Select
    If chkWork(Index).Value = 1 Then chkWork(Index).BackColor = LightestGreen
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF1 Then ShowHtmlHelp Me, "", ""
End Sub

Private Sub Form_Load()
    InitLists
End Sub
Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    If UnloadMode = 0 Then
        Me.Hide
        Cancel = True
    End If
End Sub

Private Sub InitLists()
Dim myHeaderList As String
Dim HdrLenLst As String
Dim i As Integer
Dim j As Integer
Dim tmpLen As Integer
    For i = 0 To 0
        SeaList(i).ShowHorzScroller False
        SeaList(i).ResetContent
        myHeaderList = "SEAS,BEGIN .,END   .,REMARK                        .,VPFR,VPTO,URNO,UTVH"
        HdrLenLst = ""
        j = 0
        Do
            j = j + 1
            tmpLen = Len(GetItem(myHeaderList, j, ","))
            If tmpLen > 0 Then
                HdrLenLst = HdrLenLst & TextWidth(Space(tmpLen)) / Screen.TwipsPerPixelX + 6 & ","
            End If
        Loop While tmpLen > 0
        SeaList(i).FontName = "Courier New"
        SeaList(i).SetTabFontBold True
        SeaList(i).ShowHorzScroller True
        SeaList(i).HeaderFontSize = 14
        SeaList(i).FontSize = 14
        SeaList(i).LineHeight = 15
        myHeaderList = Replace(myHeaderList, ".", " ", 1, -1, vbBinaryCompare)
        SeaList(i).HeaderLengthString = HdrLenLst
        SeaList(i).HeaderString = myHeaderList
    Next
End Sub

Public Sub LoadSeaTab()
Dim RetCode As Integer
Dim SeaLine As String
Dim tmpRecDat As String
Dim tmpResult As String
Dim tmpSqlKey As String
Dim tmpFldLst As String
Dim tmpVpfr As String
Dim tmpVpto As String
Dim tmpData As String
Dim llCount As Long
Dim i As Long
    SeaList(0).ResetContent
    tmpSqlKey = ""
    tmpFldLst = "SEAS,VPFR,VPTO,BEME,URNO"
    RetCode = UfisServer.CallCeda(tmpResult, "RTA", "SEATAB", tmpFldLst, " ", tmpSqlKey, "VPFR", 1, False, False)
    llCount = UfisServer.DataBuffer(1).GetLineCount - 1
    For i = 0 To llCount
        tmpRecDat = UfisServer.DataBuffer(1).GetLineValues(i)
        tmpResult = ""
        tmpResult = tmpResult & GetItem(tmpRecDat, 1, ",") & ","
        tmpVpfr = Left(GetItem(tmpRecDat, 2, ","), 8)
        tmpData = DecodeSsimDayFormat(tmpVpfr, "CEDA", "SSIM2")
        tmpResult = tmpResult & tmpData & ","
        tmpVpto = Left(GetItem(tmpRecDat, 3, ","), 8)
        tmpData = DecodeSsimDayFormat(tmpVpto, "CEDA", "SSIM2")
        tmpResult = tmpResult & tmpData & ","
        tmpResult = tmpResult & GetItem(tmpRecDat, 4, ",") & ","
        tmpResult = tmpResult & tmpVpfr & ","
        tmpResult = tmpResult & tmpVpto & ","
        tmpResult = tmpResult & GetItem(tmpRecDat, 5, ",") & ","
        SeaList(0).InsertTextLineAt 0, tmpResult, True
    Next
    UfisServer.DataBuffer(1).ResetContent
    'SeaLine = SeaList(0).GetLinesByColumnValue(0, "W00", 1)
    'If SeaLine = "" Then SeaLine = "3"
    'i = Val(SeaLine)
    'SeaList(0).SetCurrentSelection i
End Sub
Public Function GetValidSeason(ChkDay As String) As String
    Dim Result As String
    Dim tmpData As String
    Dim tmpVpfr As String
    Dim tmpVpto As String
    Dim MaxLine As Long
    Dim CurLine As Long
    Result = ""
    MaxLine = SeaList(0).GetLineCount - 1
    CurLine = 0
    While CurLine <= MaxLine
        tmpData = SeaList(0).GetLineValues(CurLine)
        tmpVpfr = GetItem(tmpData, 5, ",")
        tmpVpto = GetItem(tmpData, 6, ",")
        If (ChkDay <= tmpVpto) And (ChkDay >= tmpVpfr) Then
            Result = tmpData & "," & vbLf & CStr(CurLine)
            SeaList(0).SetCurrentSelection CurLine
            CurLine = MaxLine
        End If
        CurLine = CurLine + 1
    Wend
    'MsgBox "Result of 'GetValidSeason' = <" & Result & ">" & vbNewLine & vbNewLine & "Rcords Checked: " & MaxLine
    GetValidSeason = Result
End Function

Public Function GetSeasonByName(SeaCode As String) As String
    Dim Result As String
    Dim MaxLine As Long
    Dim CurLine As Long
    Result = ""
    MaxLine = SeaList(0).GetLineCount - 1
    CurLine = 0
    While CurLine <= MaxLine
        If Trim(SeaList(0).GetColumnValue(CurLine, 0)) = SeaCode Then
            Result = SeaList(0).GetLineValues(CurLine)
            SeaList(0).SetCurrentSelection CurLine
            CurLine = MaxLine
        End If
        CurLine = CurLine + 1
    Wend
    'MsgBox "Result of 'GetValidSeason' = <" & Result & ">" & vbNewLine & vbNewLine & "Rcords Checked: " & MaxLine
    GetSeasonByName = Result
End Function

Public Function GetSeasonByUser(ImpFileName As String) As String
    DlgFrame.Tag = ""
    CurImpFile.Caption = ImpFileName
    DlgFrame.Visible = True
    DlgFrame.ZOrder
    Me.Show vbModal
    DlgFrame.Visible = False
    GetSeasonByUser = DlgFrame.Tag
End Function

Private Sub SeaList_SendLButtonDblClick(Index As Integer, ByVal LineNo As Long, ByVal ColNo As Long)
    If LineNo >= 0 Then chkWork(1).Value = 1
End Sub
