VERSION 5.00
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "tab.ocx"
Begin VB.Form MyConfig 
   Caption         =   "Process Configuration Layout"
   ClientHeight    =   5385
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   7950
   Icon            =   "MyConfig.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   5385
   ScaleWidth      =   7950
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton btnClose 
      Caption         =   "Close"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   30
      TabIndex        =   1
      Top             =   30
      Width           =   855
   End
   Begin TABLib.TAB CfgTab 
      Height          =   3645
      Index           =   0
      Left            =   30
      TabIndex        =   0
      Top             =   360
      Width           =   7575
      _Version        =   65536
      _ExtentX        =   13361
      _ExtentY        =   6429
      _StockProps     =   0
   End
End
Attribute VB_Name = "MyConfig"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub btnClose_Click()
    Me.Hide
End Sub

Private Sub Form_Load()
    InitConfig
End Sub
Private Sub InitConfig()
    CfgTab(0).ResetContent
    CfgTab(0).FontName = "Courier New"
    CfgTab(0).SetTabFontBold True
    CfgTab(0).ShowHorzScroller True
    CfgTab(0).HeaderFontSize = 17
    CfgTab(0).FontSize = 17
    CfgTab(0).LineHeight = 17
    CfgTab(0).EmptyAreaRightColor = vbWhite
    CfgTab(0).HeaderString = "NAME,TYPE,SIZE,MAIN,FILE,VIEW,COMP,ROTA,Meaning,Internal Remark"
    CfgTab(0).HeaderLengthString = "40,40,40,40,40,40,40,40,150,4000"
    CfgTab(0).ColumnWidthString = "4,4,4,4,4,4,4,4,40,100"
    CfgTab(0).ColumnAlignmentString = "L,L,R,R,R,R,R,R,L,L"
    CfgTab(0).InsertTextLine "VPFR,DATE,8,,,,,,Valid Period Begin,", False
    CfgTab(0).InsertTextLine "VPTO,DATE,8,,,,,,Valid Period End,", False
    CfgTab(0).InsertTextLine "DPFR,DATE,8,,,,,,Period Begin First Sector,", False
    CfgTab(0).InsertTextLine "STOD,DATI,12,,,,,,Scheduled Time of Departure,", False
    CfgTab(0).InsertTextLine "STOA,DATI,12,,,,,,Scheduled Time of Arrival,", False
    CfgTab(0).InsertTextLine "SKED,DATI,12,,,,,,Scheduled Time (Arr/Dep),", False
    CfgTab(0).InsertTextLine "FREQ,FREQ,7,,,,,,Any Daily Frequency,", False
    CfgTab(0).InsertTextLine "FRQW,LONG,1,,,,,,Weekly Frequency,", False
    CfgTab(0).InsertTextLine "FREA,FREQ,7,,,,,,Frequency Arrival at Home,", False
    CfgTab(0).InsertTextLine "FRED,FREQ,7,,,,,,Frequency Departure at Home,", False
    CfgTab(0).InsertTextLine "FRQA,FREQ,7,,,,,,Frequency Arrival at any Station,", False
    CfgTab(0).InsertTextLine "FRQD,FREQ,7,,,,,,Frequency Departure at any Station,", False
    
    CfgTab(0).AutoSizeByHeader = True
    CfgTab(0).AutoSizeColumns
End Sub

Private Sub Form_Resize()
    CfgTab(0).Width = Me.ScaleWidth - (2 * CfgTab(0).Left)
    CfgTab(0).Height = Me.ScaleHeight - CfgTab(0).Top - 30
    
End Sub
