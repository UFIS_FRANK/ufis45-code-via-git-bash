VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.ocx"
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "TAB.ocx"
Begin VB.Form Validation 
   Caption         =   "Basic Data"
   ClientHeight    =   5415
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   11400
   Icon            =   "Validation.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   5415
   ScaleWidth      =   11400
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   3
      Top             =   5130
      Width           =   11400
      _ExtentX        =   20108
      _ExtentY        =   503
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   19579
         EndProperty
      EndProperty
   End
   Begin TABLib.TAB BasicData 
      Height          =   3645
      Index           =   0
      Left            =   30
      TabIndex        =   0
      Top             =   30
      Width           =   3525
      _Version        =   65536
      _ExtentX        =   6218
      _ExtentY        =   6429
      _StockProps     =   64
   End
   Begin TABLib.TAB BasicData 
      Height          =   3645
      Index           =   1
      Left            =   3630
      TabIndex        =   1
      Top             =   30
      Width           =   3525
      _Version        =   65536
      _ExtentX        =   6218
      _ExtentY        =   6429
      _StockProps     =   64
   End
   Begin TABLib.TAB BasicData 
      Height          =   3645
      Index           =   2
      Left            =   7230
      TabIndex        =   2
      Top             =   30
      Width           =   4140
      _Version        =   65536
      _ExtentX        =   7302
      _ExtentY        =   6429
      _StockProps     =   64
   End
End
Attribute VB_Name = "Validation"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub BasicData_SendLButtonDblClick(Index As Integer, ByVal LineNo As Long, ByVal ColNo As Long)
    If LineNo = -1 Then
        BasicData(Index).Sort CStr(ColNo), True, True
        BasicData(Index).AutoSizeColumns
        BasicData(Index).RedrawTab
        BasicIndex Index
    End If
End Sub

Private Sub Form_Load()
    InitTabs
End Sub

Private Sub InitTabs()
    Dim SqlKey As String
    Screen.MousePointer = 11
    SqlKey = "WHERE HOPO='" & HomeAirport & "'"
    BasicInit 0, "ALTTAB", "ALC2,ALC3,VAFR,VATO,URNO", SqlKey, "Airline Codes"
    BasicInit 1, "APTTAB", "APC3,APC4,VAFR,VATO,URNO", SqlKey, "Airport Codes"
    BasicInit 2, "ACTTAB", "ACT3,ACT5,ACTI,VAFR,VATO,URNO", SqlKey, "Aircraft Types"
    Screen.MousePointer = 0
End Sub

Public Sub BasicInit(Index As Integer, TableName As String, Fields As String, SqlKey As String, MainTitle As String)
    Dim ColNo As Long
    BasicData(Index).ResetContent
    BasicData(Index).HeaderString = Fields
    BasicData(Index).LogicalFieldList = Fields
    BasicData(Index).HeaderLengthString = CreateLengthList(Fields)
    BasicData(Index).FontName = "Courier New"
    BasicData(Index).SetMainHeaderValues "5", MainTitle, ""
    BasicData(Index).SetMainHeaderFont 17, False, False, True, 0, "Courier New"
    BasicData(Index).MainHeader = True
    BasicData(Index).FontSize = 17
    BasicData(Index).LineHeight = 17
    BasicData(Index).HeaderFontSize = 17
    BasicData(Index).SetTabFontBold True
    BasicData(Index).LifeStyle = True
    BasicData(Index).CursorLifeStyle = True
    ColNo = CLng(GetRealItemNo(Fields, "VAFR"))
    If ColNo >= 0 Then
        BasicData(Index).DateTimeSetColumn ColNo
        BasicData(Index).DateTimeSetInputFormatString ColNo, "YYYYMMDDhhmm"
        BasicData(Index).DateTimeSetOutputFormatString ColNo, "DDMMMYY'/'hh':'mm"
    End If
    ColNo = CLng(GetRealItemNo(Fields, "VATO"))
    If ColNo >= 0 Then
        BasicData(Index).DateTimeSetColumn ColNo
        BasicData(Index).DateTimeSetInputFormatString ColNo, "YYYYMMDDhhmm"
        BasicData(Index).DateTimeSetOutputFormatString ColNo, "DDMMMYY'/'hh':'mm"
    End If
    BasicData(Index).CreateDecorationObject "CellDup", "R,T,B,L", "2,2,2,2", Str(vbBlack) & "," & Str(LightGray) & "," & Str(vbBlack) & "," & Str(LightGray)
    BasicData(Index).AutoSizeByHeader = True
    BasicData(Index).ShowHorzScroller True
    BasicData(Index).CedaCurrentApplication = UfisServer.ModName.Text & "," & UfisServer.GetApplVersion(True)
    BasicData(Index).CedaHopo = UfisServer.HOPO.Text
    BasicData(Index).CedaIdentifier = "IDX"
    BasicData(Index).CedaPort = "3357"
    BasicData(Index).CedaReceiveTimeout = "250"
    BasicData(Index).CedaRecordSeparator = vbLf
    BasicData(Index).CedaSendTimeout = "250"
    BasicData(Index).CedaServerName = UfisServer.HostName.Text
    BasicData(Index).CedaTabext = UfisServer.TblExt.Text
    BasicData(Index).CedaUser = gsUserName
    BasicData(Index).CedaWorkstation = UfisServer.GetMyWorkStationName
    If CedaIsConnected Then
        BasicData(Index).CedaAction "RTA", TableName, Fields, "", SqlKey
        If UfisServer.HostName.Tag = "TEST" Then
            'If UfisServer.HostName.Text <> "LOCAL" Then BasicData(Index).WriteToFile "c:\tmp\TlxBasicData" & CStr(Index) & ".txt", False
            If UfisServer.HostName.Text <> "LOCAL" Then BasicData(Index).WriteToFile UFIS_TMP & "\TlxBasicData" & CStr(Index) & ".txt", False
        End If
    Else
        If UfisServer.HostName.Tag = "TEST" Then
            'If UfisServer.HostName.Text = "LOCAL" Then BasicData(Index).ReadFromFile "c:\tmp\TlxBasicData" & CStr(Index) & ".txt"
            If UfisServer.HostName.Text = "LOCAL" Then BasicData(Index).ReadFromFile UFIS_TMP & "\TlxBasicData" & CStr(Index) & ".txt"
        End If
    End If
    BasicData(Index).Sort "1", True, True
    BasicData(Index).Sort "0", True, True
    BasicData(Index).AutoSizeColumns
    BasicData(Index).RedrawTab
    BasicIndex Index
End Sub
Private Sub BasicIndex(Index As Integer)
    Dim IdxFields As String
    Dim FldNo As Long
    Dim FldName As String
    Dim MaxLine As Long
    Dim CurLine As Long
    Dim LineNo As Long
    Dim OldLine As Long
    Dim CurVal As String
    Dim OldVal As String
    MaxLine = BasicData(Index).GetLineCount - 1
    IdxFields = BasicData(Index).LogicalFieldList
    FldNo = 0
    FldName = GetRealItem(IdxFields, FldNo, ",")
    While FldName <> ""
        BasicData(Index).IndexDestroy FldName
        BasicData(Index).IndexCreate FldName, FldNo
        If FldNo <= 1 Then
            OldVal = ""
            For CurLine = 0 To MaxLine
                LineNo = BasicData(Index).IndexGetDataLineNo(FldName, CurLine)
                CurVal = BasicData(Index).GetFieldValue(LineNo, FldName)
                If (OldVal <> "") And (OldVal = CurVal) Then
                    BasicData(Index).SetDecorationObject LineNo, FldNo, "CellDup"
                    BasicData(Index).SetDecorationObject OldLine, FldNo, "CellDup"
                    BasicData(Index).SetLineColor LineNo, vbBlack, LightYellow
                    BasicData(Index).SetLineColor OldLine, vbBlack, LightYellow
                End If
                OldVal = CurVal
                OldLine = LineNo
            Next
        End If
        FldNo = FldNo + 1
        FldName = GetRealItem(IdxFields, FldNo, ",")
    Wend
End Sub

Public Function BasicCheck(Index As Integer, Field1 As String, Look1 As String, Field2 As String, Look2 As String) As Boolean
    Dim IsOk As Boolean
    Dim HitCount As Long
    Dim LineNo As Long
    Dim tmpLook As String
    IsOk = True
    BasicData(Index).SetInternalLineBuffer True
    If (Look1 = "") Or (Look2 = "") Then
        If Look1 <> "" Then
            IsOk = False
            HitCount = Val(BasicData(Index).GetLinesByIndexValue(Field1, Look1, 0))
            If HitCount > 0 Then
                LineNo = BasicData(Index).GetNextResultLine
                Look2 = BasicData(Index).GetFieldValue(LineNo, Field2)
                IsOk = True
            End If
        End If
        If Look2 <> "" Then
            IsOk = False
            HitCount = Val(BasicData(Index).GetLinesByIndexValue(Field2, Look2, 0))
            If HitCount > 0 Then
                LineNo = BasicData(Index).GetNextResultLine
                tmpLook = BasicData(Index).GetFieldValue(LineNo, Field1)
                If Look1 = "" Then Look1 = tmpLook
                IsOk = True
            End If
        End If
    Else
    End If
    
    BasicData(Index).SetInternalLineBuffer False
    BasicCheck = IsOk
End Function

Private Function CreateLengthList(FieldList As String) As String
    Dim ItmCnt As Integer
    Dim ItmDat As String
    Dim Result As String
    ItmDat = "START"
    ItmCnt = 0
    Result = "32"
    While ItmDat <> ""
        ItmCnt = ItmCnt + 1
        ItmDat = GetItem(FieldList, ItmCnt, ",")
        If ItmDat <> "" Then
            Result = Result + ",32"
        End If
    Wend
    CreateLengthList = Result
End Function


Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    If UnloadMode = 0 Then
        Me.Hide
        Cancel = True
    End If
End Sub

Private Sub Form_Resize()
    Dim NewSize As Long
    Dim i As Integer
    NewSize = Me.ScaleHeight - BasicData(0).Top - StatusBar1.Height - 30
    If NewSize > 300 Then
        For i = 0 To BasicData.UBound
            BasicData(i).Height = NewSize
        Next
    End If
End Sub

Public Function CheckPeriodData(Vpfr As String, Vpto As String, Frqd As String, Frqw As String, FirstValidDay As String, RetFrqDays As Boolean) As Integer
Dim Result As Integer
Dim varVpfr As Variant
Dim varVpto As Variant
Dim varFday As Variant
Dim strWkdy As String
Dim intDayCnt As Integer
Dim tmpFrqd As String
Dim ChrPos As Integer
Dim FirstDay As String
'Dim tmpdata As String used for: check if the date is valid
    Result = 0
    FirstValidDay = ""
    tmpFrqd = Trim(Frqd)
    tmpFrqd = Replace(tmpFrqd, "0", ".", 1, -1, vbBinaryCompare)
    If Len(tmpFrqd) <> 7 Or (tmpFrqd = ".......") Then Result = -1
    If Result = 0 Then
        If Vpto < Vpfr Then Result = -2
    End If
    If Result = 0 Then
        varVpfr = CedaDateToVb(Vpfr)
        varVpto = CedaDateToVb(Vpto)
        If varVpto < varVpfr Then Result = -2
        If Not CheckValidDate(Vpfr) Then Result = -5
        If Not CheckValidDate(Vpto) Then Result = -6
    End If
    If Result = 0 Then
        varFday = varVpfr
        'to check the operational days
        'we run only one week (7 days) through the period frame
        intDayCnt = 0
        Do
            intDayCnt = intDayCnt + 1
            strWkdy = Trim(Str(Weekday(varFday, vbMonday)))
            If InStr(tmpFrqd, strWkdy) > 0 Then
                tmpFrqd = Replace(tmpFrqd, strWkdy, ".", 1, 1, vbBinaryCompare)
                If FirstValidDay = "" Then FirstValidDay = Format(varFday, "yyyymmdd")
            End If
            varFday = DateAdd("d", 1, varFday)
        Loop While (varFday <= varVpto) And (intDayCnt < 7)
        If tmpFrqd <> "......." Then
            Result = -3
            If RetFrqDays Then
                Result = 0
                For intDayCnt = 1 To 7
                    strWkdy = Mid(tmpFrqd, intDayCnt, 1)
                    If strWkdy <> "." Then Frqd = Replace(Frqd, strWkdy, ".", 1, -1, vbBinaryCompare)
                Next
                If Frqd = "......." Then Result = -4
            End If
        End If
    End If
    CheckPeriodData = Result
End Function


