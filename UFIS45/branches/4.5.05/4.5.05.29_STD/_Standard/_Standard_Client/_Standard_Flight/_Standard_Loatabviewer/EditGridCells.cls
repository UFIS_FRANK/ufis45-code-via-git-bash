VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "EditGridCells"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"No"
Attribute VB_Ext_KEY = "Collection" ,"EditGridCell"
Attribute VB_Ext_KEY = "Member0" ,"EditGridCell"
Option Explicit

Private mCol As Collection
Private mvarGrid As EditGrid

Private Sub Class_Initialize()
    Set mCol = New Collection
End Sub

'Public Function Add(column As String, row As String, Definition As String, Priorities As String) As EditGridCell igu on 05 Apr 2010
Public Function Add(column As String, row As String, Definition As String, Priorities As String, _
    Optional ByVal AutoSave As Boolean = False, Optional ByVal SaveToExtTable As String = "") As EditGridCell 'igu on 05 Apr 2010
    
    Dim cell As EditGridCell
    Set cell = New EditGridCell
    cell.ParentGrid = mvarGrid
    cell.column = column
    cell.row = row
    cell.Priorities = Priorities
    cell.Definition = Definition
    cell.AutoSave = AutoSave  'igu on 05 Apr 2010
    cell.SaveToExtTable = SaveToExtTable 'igu on 05 Apr 2010
    mCol.Add cell, column & row
    Set Add = cell
    
End Function

Public Sub InsertValue(URNO As String, source As String, TypeKey As String, Value As String)
    Dim cell As EditGridCell
    For Each cell In mCol
        If (cell.CellType = "Interface" Or (cell.CellType = "User" And source = "USR")) And cell.TypeKey = TypeKey Then
            cell.URNO = URNO
            cell.SetValue source, Value
        'Changed on 15/08/2006, if celltype = 'Compute'
        Else
            If cell.TypeKey = TypeKey Then
                cell.URNO = URNO
            End If
        End If
    Next
End Sub

Public Sub ClearAll()
    Dim cell As EditGridCell
    For Each cell In mCol
        cell.Clear
    Next
End Sub

'changed on 15/08/2006 if CellType = Compute, the value need to be capture
Public Sub GetChanges(changes As Collection)
    Dim cell As EditGridCell
    For Each cell In mCol
        'If cell.CellType = "User" Then
        If cell.CellType = "User" Or cell.CellType = "Compute" Then
            If cell.State <> "Unchanged" Then
                'Changed on 17/08/2006
                If cell.CellType = "Compute" And cell.Priorities = "ComputeStore" Then
                    If cell.URNO = "" Then
                        changes.Add "Inserted" & ";" & cell.URNO & "," & frmMain.txtAFTURNO.Text & ",USR," & cell.TypeKey & "," & cell.Value
                    Else
                        changes.Add "Modified" & ";" & cell.URNO & "," & frmMain.txtAFTURNO.Text & ",USR," & cell.TypeKey & "," & cell.Value
                    End If
                Else
                    'MsgBox "in user" & cell.State & cell.URNO & "," & frmMain.txtAFTURNO.Text
                    'MsgBox "in user" & cell.TypeKey & cell.CellType & "," & cell.Value
                    If cell.State = "Inserted" Then
                        cell.URNO = ""
                    End If
                    changes.Add cell.State & ";" & cell.URNO & "," & frmMain.txtAFTURNO.Text & ",USR," & cell.TypeKey & "," & cell.Value
                End If
            End If
        End If
    Next
End Sub
'kkh to insert / update the lastest figures for Total on board for Departure flight
'Public Sub GetInitialChanges(changes As Collection) 'igu on 10 Nov 2009
'Public Sub GetInitialChanges(changes As Collection, ByVal RowNumber As Integer) 'igu on 10 Nov 2009 'igu on 05 Apr 2010
Public Sub GetInitialChanges(changes As Collection) 'igu on 05 Apr 2010
    Dim cell As EditGridCell
    For Each cell In mCol
        'If cell.CellType = "Compute" And cell.row = "14" Then 'igu on 10 Nov 2009
        'If cell.CellType = "Compute" And cell.row = CStr(RowNumber) Then 'igu on 10 Nov 2009 'igu on 05 Apr 2010
        If cell.AutoSave And (cell.CellType = "User" Or cell.CellType = "Compute") Then 'igu on 05 Apr 2010
            changes.Add "Create" & ";" & cell.URNO & "," & frmMain.txtAFTURNO.Text & ",USR," & cell.TypeKey & "," & cell.Value
        End If
    Next
End Sub


Public Sub ApplyCellStyles()
    Dim cell As EditGridCell
    For Each cell In mCol
        cell.ApplyCellStyle
    Next
End Sub

Public Sub UpdateGrid()
    Dim cell As EditGridCell
    For Each cell In mCol
        cell.UpdateGrid
    Next
End Sub

Public Sub ComputeAll()
    Dim cell As EditGridCell
    For Each cell In mCol
        If cell.CellType = "Compute" Then ComputeCell cell
    Next
End Sub

Public Sub ResetState()
    Dim cell As EditGridCell
    For Each cell In mCol
        cell.ResetState
    Next
End Sub

'igu on 14 Dec 2009
'add new function, blank + blank = blank, blank + number = number, number + number = number
Private Function AddEx(ByVal Value1, ByVal Value2)
    If IsEmpty(Value1) Then Value1 = ""
    If IsEmpty(Value2) Then Value2 = ""

    If (Not IsNumeric(Value1)) And (Not IsNumeric(Value2)) Then
        AddEx = ""
    ElseIf (IsNumeric(Value1)) And (Not IsNumeric(Value2)) Then
        AddEx = Val(Value1)
    ElseIf (Not IsNumeric(Value1)) And (IsNumeric(Value2)) Then
        AddEx = Val(Value2)
    Else
        AddEx = Val(Value1) + Val(Value2)
    End If
End Function

'igu on 14 Dec 2009
'add new function, blank - blank = blank, blank - number = number, number - number = number
Private Function MinusEx(ByVal Value1, ByVal Value2)
    If IsEmpty(Value1) Then Value1 = ""
    If IsEmpty(Value2) Then Value2 = ""

    If (Not IsNumeric(Value1)) And (Not IsNumeric(Value2)) Then
        MinusEx = ""
    ElseIf (IsNumeric(Value1)) And (Not IsNumeric(Value2)) Then
        MinusEx = Val(Value1)
    ElseIf (Not IsNumeric(Value1)) And (IsNumeric(Value2)) Then
        MinusEx = -Val(Value2)
    Else
        MinusEx = Val(Value1) - Val(Value2)
    End If
End Function

Public Sub ComputeCell(cell As EditGridCell)
    Dim step As Variant
    Dim Value As String
    Dim values() As String
    Dim operation As String
    'changed on 15/08/2006
    Dim operands_1() As String
    Dim operand As Variant
    Dim operands() As String
    Dim operandValue As String
'   Dim Result As Long 'igu on 14 Dec 2009
    Dim Result 'igu on 14 Dec 2009
    Dim i As Long
    
'    Result = 0 'igu on 14 Dec 2009
    For Each step In cell.ComputeSteps
        'changed on 15/08/2006
        If InStr(step, ":") Then
        
            values = Split(step, ":")
            operation = values(0)
            'changed on 15/08/2006
            If InStr(values(1), ";User") Then
                operands_1 = Split(values(1), ";User")
                operands = Split(operands_1(0), ",")
            Else
                operands = Split(values(1), ",")
            End If
            
            For Each operand In operands
                values = Split(operand, "|")
                For i = 0 To UBound(values)
                    Value = item(values(i)).Value
                    If Not Value = "" Then Exit For
                Next
'                If Value = "" Then Value = "0" 'igu on 14 Dec 2009
'                If operation = "SUM" Then 'igu on 14 Dec 2009
'                    Result = Result + Val(Value) 'igu on 14 Dec 2009
'                Else 'igu on 14 Dec 2009
'                    If operation = "SUB" Then 'igu on 14 Dec 2009
'                        Result = Result - Val(Value) 'igu on 14 Dec 2009
'                    End If 'igu on 14 Dec 2009
'                End If 'igu on 14 Dec 2009
                Select Case operation 'igu on 14 Dec 2009
                    Case "SUM", "SUB" 'igu on 14 Dec 2009
                        If Value = "" Then Value = "0" 'igu on 14 Dec 2009
                        If IsEmpty(Result) Then Result = 0 'igu on 14 Dec 2009
                        If Result = "" Then Result = 0 'igu on 14 Dec 2009
                        If operation = "SUM" Then 'igu on 14 Dec 2009
                            Result = Val(Result) + Val(Value) 'igu on 14 Dec 2009
                        Else 'igu on 14 Dec 2009
                            Result = Val(Result) - Val(Value) 'igu on 14 Dec 2009
                        End If 'igu on 14 Dec 2009
                    Case "ADD" 'igu on 14 Dec 2009
                        Result = AddEx(Result, Value) 'igu on 14 Dec 2009
                    Case "MIN" 'igu on 14 Dec 2009
                        Result = MinusEx(Result, Value) 'igu on 14 Dec 2009
                End Select  'igu on 14 Dec 2009
            Next
        End If
    Next
    'cell.SetValue "Compute", Str(Result) 'igu on 14 Dec 2009
    cell.SetValue "Compute", CStr(Result) 'igu on 14 Dec 2009
    
    'mvarGrid.TabControl.SetFieldValues Val(cell.row) - 1, cell.column, LTrim(Str(Result)) 'igu on 14 Dec 2009
    mvarGrid.TabControl.SetFieldValues Val(cell.row) - 1, cell.column, LTrim(CStr(Result)) 'igu on 14 Dec 2009
End Sub

Public Property Set ParentGrid(vData As EditGrid)
    Set mvarGrid = vData
End Property

Public Property Get ParentGrid() As EditGrid
    Set ParentGrid = mvarGrid
End Property

Public Property Get item(vntIndexKey As Variant) As EditGridCell
Attribute item.VB_UserMemId = 0
  Set item = mCol(vntIndexKey)
End Property

Public Property Get Count() As Long
    Count = mCol.Count
End Property

Public Sub Remove(vntIndexKey As Variant)
    mCol.Remove vntIndexKey
End Sub

Public Property Get NewEnum() As IUnknown
    Set NewEnum = mCol.[_NewEnum]
End Property

Private Sub Class_Terminate()
    Set mCol = Nothing
End Sub

