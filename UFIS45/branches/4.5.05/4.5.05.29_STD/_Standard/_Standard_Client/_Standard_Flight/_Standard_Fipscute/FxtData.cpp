// FxtData.cpp: implementation of the FxtData class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "CCSGlobl.h"
#include "cuteif.h"
#include "FxtData.h"
#include "RecordSet.h"
#include "CCSPtrArray.h"
#include "CedaBasicData.h"
#include "BasicData.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

FxtData::FxtData()
{

}

FxtData::~FxtData()
{

}



bool FxtData::DBGetFreetext(long lpFldu, int ipNum, CString &ropText) const
{
	if (ipNum != 1 && ipNum != 2)
		return false;
		
	CString olFldu;
	olFldu.Format("%ld", lpFldu);
	CString olSort;
	olSort.Format("%d", ipNum);


	CCSPtrArray<RecordSet> rlRecords;

	if (ogBCD.GetRecordsExt("FXT", "FLDU", "SORT", olFldu, olSort, &rlRecords) > 0)
	{
		// record loaded!
		ropText = ((rlRecords[0])[ogBCD.GetFieldIndex("FXT", "TEXT")]);
		return true;
	}
	else
	{
		// load FXT records!
		CString olSel;
		olSel.Format("WHERE FLDU = %ld", lpFldu);
		ogBCD.Read(CString("FXT"), olSel);

		if (ogBCD.GetField("FXT", "SORT", olSort, "TEXT", ropText))
			return true;
		else
			return false;
	}
}


/*
bool FxtData::DBGetFreetext(long lpFldu, int ipNum, CString &ropText) const
{
	CString olFldu;
	olFldu.Format("%ld", lpFldu);
	CString olSort;
	olSort.Format("%d", ipNum);


	RecordSet *prlFreeRecord = NULL;
	if (ipNum == 1)
	{
		prlFreeRecord = prmFree1Record;
	}
	if (ipNum == 2)
	{
		prlFreeRecord = prmFree2Record;
	}
	
	if (!prlFreeRecord)
		return false;



	if ((*prlFreeRecord)[ogBCD.GetFieldIndex("FXT", "FLDU")] == olFldu)
	{
		// record loaded! return value
		ropText = (*prlFreeRecord)[ogBCD.GetFieldIndex("FXT", "TEXT")];
		return true;
	}
	else
	{
		// load FXT records!
		CString olSel;
		olSel.Format("WHERE FLDU = %ld", lpFldu);
		ogBCD.Read(CString("FXT"), olSel);

		ogBCD.GetRecord("FXT", "SORT", "1", *prmFree1Record);
		ogBCD.GetRecord("FXT", "SORT", "2", *prmFree2Record);


		if ((*prlFreeRecord)[ogBCD.GetFieldIndex("FXT", "FLDU")] == olFldu)
		{
			// record loaded! return value
			ropText = (*prlFreeRecord)[ogBCD.GetFieldIndex("FXT", "TEXT")];
			return true;
		}
		else
			return false;
	}
}
*/



bool FxtData::DBUpdateFreetext(long lpFldu, int ipNum, const CString &ropText) const
{
	if (ipNum != 1 && ipNum != 2)
		return false;

	CString olFldu;
	olFldu.Format("%ld", lpFldu);
	CString olSort;
	olSort.Format("%d", ipNum);
	

	CCSPtrArray<RecordSet> rlRecords;

	if (ogBCD.GetRecordsExt("FXT", "FLDU", "SORT", olFldu, olSort, &rlRecords) > 0)
	{
		// fxt-record loaded!! Update it!
		ogBCD.SetField("FXT", "URNO", (rlRecords[0])[ogBCD.GetFieldIndex("FXT", "URNO")], "TEXT", ropText, false);
		return ogBCD.Save("FXT");
	}
	else
	{
		// try to load an existing record
		CString olSel;
		olSel.Format("WHERE FLDU = %ld AND SORT = '%s'", lpFldu, olSort);
		ogBCD.Read(CString("FXT"), olSel);

		if (ogBCD.GetRecordsExt("FXT", "FLDU", "SORT", olFldu, olSort, &rlRecords) > 0)
		{
			// fxt-record loaded!! Update it!
			ogBCD.SetField("FXT", "URNO", (rlRecords[0])[ogBCD.GetFieldIndex("FXT", "URNO")], "TEXT", ropText, false);
			return ogBCD.Save("FXT");
		}
		else
		{
			// fxt-record does not exist!
			CString olNextUrno;
			olNextUrno.Format("%ld", ogBasicData.GetNextUrno());
			// create new fxt record
			RecordSet olNewFree(ogBCD.GetFieldCount("FXT"));
			olNewFree[ogBCD.GetFieldIndex("FXT", "URNO")] = olNextUrno;
			olNewFree[ogBCD.GetFieldIndex("FXT", "FLDU")] = olFldu;
			olNewFree[ogBCD.GetFieldIndex("FXT", "SORT")] = olSort;
			olNewFree[ogBCD.GetFieldIndex("FXT", "TEXT")] = ropText;

			return ogBCD.InsertRecord("FXT", olNewFree, true);
		}
	}
}



/*
bool FxtData::DBUpdateFreetext(long lpFldu, int ipNum, const CString &ropText) const
{
	CString olFldu;
	olFldu.Format("%ld", lpFldu);
	CString olSort;
	olSort.Format("%d", ipNum);
	
	RecordSet *prlFreeRecord = NULL;
	if (ipNum == 1)
	{
		prlFreeRecord = prmFree1Record;
	}
	if (ipNum == 2)
	{
		prlFreeRecord = prmFree2Record;
	}
	
	if (!prlFreeRecord)
		return false;



	if ((*prlFreeRecord)[ogBCD.GetFieldIndex("FXT", "FLDU")] == olFldu)
	{
		// fxt-record exists!! Update it!
		ogBCD.SetField("FXT", "URNO", (*prlFreeRecord)[ogBCD.GetFieldIndex("FXT", "URNO")], "TEXT", ropText, false);
		return ogBCD.Save("FXT");
	}
	else
	{
		CString olNextUrno;
		olNextUrno.Format("%ld", ogBasicData.GetNextUrno());
		// create new flz record
		RecordSet olNewFree(ogBCD.GetFieldCount("FXT"));
		olNewFree[ogBCD.GetFieldIndex("FXT", "URNO")] = olNextUrno;
		olNewFree[ogBCD.GetFieldIndex("FXT", "FLDU")] = olFldu;
		olNewFree[ogBCD.GetFieldIndex("FXT", "SORT")] = olSort;
		olNewFree[ogBCD.GetFieldIndex("FXT", "TEXT")] = ropText;

		return ogBCD.InsertRecord("FXT", olNewFree, true);
	}

}
*/





