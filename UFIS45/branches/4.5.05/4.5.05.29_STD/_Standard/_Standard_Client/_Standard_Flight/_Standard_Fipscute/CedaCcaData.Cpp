// CedaCcaData.cpp
 
#include "stdafx.h"
#include <afxwin.h>
#include "BasicData.h"
#include "CCSGlobl.h"
#include "CedaCcaData.h"
#include "CCSBcHandle.h"
#include "CCSCedadata.h"
#include "CuteIF.h"



//--PROCESS-CF---------------------------------------------------------------------------------------------

static void  ProcessCcaCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	((CedaCcaData *)vpInstance)->ProcessBc(ipDDXType,vpDataPointer,ropInstanceName);		//(CCADATA *)vpDataPointer
}



/*
static int Compare(const CCADATA **e1, const CCADATA **e2)
{
	return ( (  CString((**e1).Ckic) ==CString((**e2).Ckic)) ? 0 : ( (CString((**e1).Ckic) > CString((**e2).Ckic) ? 1 : -1)));
}
*/


//--CEDADATA-----------------------------------------------------------------------------------------------

CedaCcaData::CedaCcaData() : CCSCedaData(&ogCommHandler)
{
    BEGIN_CEDARECINFO(CCADATA, CcaDataRecInfo)
		CCS_FIELD_DATE		(Cdat,"CDAT","Erstellungsdatum", 1)
		CCS_FIELD_DATE		(Ckba,"CKBA","Belegung Checkin-Schalter aktueller Beginn", 1)
		CCS_FIELD_DATE		(Ckbs,"CKBS","Belegung Checkin-Schalter geplanter Beginn", 1)
		CCS_FIELD_DATE		(Ckea,"CKEA","Belegung Checkin-Schalter aktuelles Ende", 1)
		CCS_FIELD_DATE		(Ckes,"CKES","Belegung Checkin-Schalter geplantes Ende", 1)
		CCS_FIELD_CHAR_TRIM	(Ckic,"CKIC","Checkin-Schalter", 1)
		CCS_FIELD_CHAR_TRIM	(Ckif,"CKIF","fester Checkin-Schalter", 1)
		CCS_FIELD_CHAR_TRIM	(Ckit,"CKIT","Terminal Checkin-Schalter", 1)
		CCS_FIELD_CHAR_TRIM	(Ctyp,"CTYP","Check-In Typ (Common oder Flight)", 1)
		CCS_FIELD_LONG		(Flnu,"FLNU","Eindeutige Datensatz-Nr. des zugeteilten Fluges", 1)
		CCS_FIELD_DATE		(Lstu,"LSTU","Datum letzte Änderung", 1)
		CCS_FIELD_CHAR_TRIM	(Prfl,"PRFL","Protokollierungskennzeichen", 1)
		CCS_FIELD_LONG		(Urno,"URNO","Eindeutige Datensatz-Nr.", 1)
		CCS_FIELD_CHAR_TRIM	(Usec,"USEC","Anwender (Ersteller)", 1)
		CCS_FIELD_CHAR_TRIM	(Useu,"USEU","Anwender (letzte Änderung)", 1)
		CCS_FIELD_CHAR_TRIM	(Rema,"REMA","Bemerkung", 1)
		CCS_FIELD_CHAR_TRIM	(Disp,"DISP","Bemerkung", 1)
		CCS_FIELD_CHAR_TRIM	(Act3,"ACT3", "Aircarfttype 3 Letter", 1)
		// FLNO in CCATAB will not be updated in every case!!
		// use FLNO in AFTTAB instead!
		//CCS_FIELD_CHAR_TRIM	(Flno,"FLNO", "Flightnumber", 1) 
		CCS_FIELD_CHAR_TRIM	(Stat,"STAT", "Conflictstatus of Cca", 1)
		CCS_FIELD_LONG		(Ghpu,"Ghpu", "", 1)
		CCS_FIELD_LONG		(Gpmu,"Gpmu", "", 1)
		CCS_FIELD_LONG		(Ghsu,"Ghsu", "", 1)
		CCS_FIELD_CHAR_TRIM	(Cgru,"Cgru", "", 1)
		CCS_FIELD_DATE		(Stod,"STOD","", 1)
		CCS_FIELD_INT		(Copn,"COPN","Schalteröffnungszeit (in Min. vor Abflug)", 1)
  END_CEDARECINFO

    // Copy the record structure
    for (int i = 0; i < sizeof(CcaDataRecInfo)/sizeof(CcaDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&CcaDataRecInfo[i],sizeof(CEDARECINFO));
        omRecInfo.Add(prpCedaRecInfo);
	}
    // Initialize table names and field names
    strcpy(pcmTableName,"CCA");
    //sprintf(pcmListOfFields,"CDAT,CKBA,CKBS,CKEA,CKES,CKIC,CKIF,CKIT,CTYP,FLNU,LSTU,PRFL,URNO,USEC,USEU,REMA,DISP,ACT3,FLNO,STAT,GHPU,GPMU,GHSU,CGRU,STOD,COPN");
    sprintf(pcmListOfFields,"CDAT,CKBA,CKBS,CKEA,CKES,CKIC,CKIF,CKIT,CTYP,FLNU,LSTU,PRFL,URNO,USEC,USEU,REMA,DISP,ACT3,STAT,GHPU,GPMU,GHSU,CGRU,STOD,COPN");
	pcmFieldList = pcmListOfFields; //because in CedaData pcmFieldList is just a pointer
	
	omData.RemoveAll();

	Register();
}

//--~CEDADATA----------------------------------------------------------------------------------------------

CedaCcaData::~CedaCcaData(void)
{
	UnRegister();
	omRecInfo.DeleteAll();
	ClearAll();
}


//--REGISTER----------------------------------------------------------------------------------------------

void CedaCcaData::Register(void)
{
	ogDdx.Register((void *)this,BC_CCA_CHANGE,	CString("CCADATA"), CString("Cca-changed"),	ProcessCcaCf);
	ogDdx.Register((void *)this,BC_CCA_NEW,		CString("CCADATA"), CString("Cca-new"),		ProcessCcaCf);
	ogDdx.Register((void *)this,BC_CCA_DELETE,	CString("CCADATA"), CString("Cca-deleted"),	ProcessCcaCf);
}


void CedaCcaData::UnRegister(void)
{
	ogDdx.UnRegister(this,NOTUSED);
}


//--CLEAR-ALL----------------------------------------------------------------------------------------------

void CedaCcaData::ClearAll(bool bpWithRegistration)
{
    omUrnoMap.RemoveAll();

	for (int i = 0; i < omData.GetSize(); i++)
		delete omData[i];
    omData.RemoveAll();

	
	omAftData.ClearAll();

	if(bpWithRegistration)
	{
		ogDdx.UnRegister(this,NOTUSED);
	}
}




//--PROCESS-BC---------------------------------------------------------------------------------------------

void  CedaCcaData::ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{

	struct BcStruct *prlCcaData = (struct BcStruct *) vpDataPointer;

	
	if(ipDDXType == BC_CCA_NEW)
	{
		CCADATA rlCca; 
		GetRecordFromItemList(&rlCca, prlCcaData->Fields, prlCcaData->Data);
		if (rlCca.Ckic == omCurrCkic)
		{
			// load new record
			CString olSel;
			olSel.Format("WHERE URNO=%ld", rlCca.Urno);
			Read(olSel, true);
		}
	}

	if(ipDDXType == BC_CCA_CHANGE)
	{
		CCADATA *prlCca = new CCADATA;
		GetRecordFromItemList(prlCca, prlCcaData->Fields, prlCcaData->Data);
		if (GetRecByUrno(prlCca->Urno) != NULL)
		{
			// record already loaded
			if (prlCca->Ckic == omCurrCkic)
			{
				// record has correct ckic
				UpdateInternal(prlCca);
			}
			else
			{
				// record has another ckic
				DeleteInternal(prlCca);
			}
		}
		else if (prlCca->Ckic == omCurrCkic)
		{
			// load new record
			CString olSel;
			olSel.Format("WHERE URNO=%ld", prlCca->Urno);
			Read(olSel, true);
		}
		delete prlCca;
	}


	if(ipDDXType == BC_CCA_DELETE)
	{
		CString olUrno;
		if (GetListItemByField(CString(prlCcaData->Data), CString(prlCcaData->Fields), CString("URNO"), olUrno) == true)
		{
			CCADATA *prlCca = GetRecByUrno(atol(olUrno));
			if (prlCca != NULL)
			{
				DeleteInternal(prlCca);
			}
		}
	}

}

//---------------------------------------------------------------------------------------------------------



//--INSERT-INTERNAL--------------------------------------------------------------------------------------

bool CedaCcaData::InsertInternal(CCADATA *prpCca, bool bpDdx /* = true */)
{
	omData.Add(prpCca);//Update omData
	omUrnoMap.SetAt((void *)prpCca->Urno, prpCca);
	if (bpDdx)
		ogDdx.DataChanged((void *)this, CCA_NEW,(void *)prpCca ); //Update Viewer
    return true;
}


//--DELETE-INTERNAL----------------------------------------------------------------------------------------

bool CedaCcaData::DeleteInternal(CCADATA *prpCca, bool bpDdx /* = true */)
{
	if (bpDdx)
		ogDdx.DataChanged((void *)this,CCA_DELETE,(void *)prpCca); //Update Viewer
	omUrnoMap.RemoveKey((void *)prpCca->Urno);
	int ilCcaCount = omData.GetSize();
	for (int ilLc = 0; ilLc < ilCcaCount; ilLc++)
	{
		if (((CCADATA *)omData[ilLc])->Urno == prpCca->Urno)
		{
			delete omData[ilLc];
			omData.RemoveAt(ilLc);//Update omData
			break;
		}
	}
    return true;
}

//--UPDATE-INTERNAL--------------------------------------------------------------------------------------------

bool CedaCcaData::UpdateInternal(CCADATA *prpCca, bool bpDdx /* = true */)
{
	CCADATA *prlCca = GetRecByUrno(prpCca->Urno);
	if (prlCca != NULL)
	{
		*prlCca = *prpCca; //Update omData
		ResetCcaFlno();
		if (bpDdx)
			ogDdx.DataChanged((void *)this,CCA_CHANGE,(void *)prlCca); //Update Viewer
		return true;

	}
    return false;
}



/*
//--READ-ALL-----------------------------------------------------------------------------------------------

//-----------------------------------------------------------------------------------------------

bool CedaCcaData::ReadAllFlights(CString &opWhere)
{
	if(omSelection == opWhere)
		return false; 

	ClearAll(false);

	omSelection = CString("CKBS<>' ' AND CKIC<>' ' AND ") + opWhere;

	CString olSelection = CString("WHERE ") + omSelection;

	char pclSel[1024];
    
	strcpy(pclSel, olSelection );

	return Read(pclSel);

} 
*/


bool CedaCcaData::Read(CString opWhere, bool bpDdx)
{
    // Select data from the database
	bool ilRc = true;


	if(opWhere.IsEmpty())
	{	
		ilRc = CedaAction("RT");
		CCuteIFApp::CheckCedaError(*this);
	}
	else
	{
		ilRc = CedaAction("RT", opWhere.GetBuffer(0));
		CCuteIFApp::CheckCedaError(*this);
	}

	if (ilRc != true)
	{
		return false;
	}
	// Load data from CedaData into the dynamic array of record
	for (int ilLc = 0; ilRc == true; ilLc++)
	{
		CCADATA *prlCca = new CCADATA;
		if ((ilRc = GetBufferRecord(ilLc,prlCca)) == true)
		{
			if(UpdateInternal(prlCca, bpDdx))
			{
				delete prlCca;
			}
			else
			{
				InsertInternal(prlCca, bpDdx);
			}
		}
		else
		{
			delete prlCca;
		}
	}

	ReadAllRelFlights();

    return true;
}






bool CedaCcaData::ReadAllRelFlights()
{
	// collect all flight urnos
	CString olAftUrnos;
	CString olUrno;
	const CCADATA *prlCca;
	int ilCcaCount = omData.GetSize();
	for (int ilLc = 0; ilLc < ilCcaCount; ilLc++)
	{
		prlCca = (CCADATA *)omData[ilLc];
		if (prlCca->Ctyp[0] != 'C' && prlCca->Flnu > 0)
		{
			olUrno.Format("%ld,", prlCca->Flnu);
			olAftUrnos += olUrno;
		}			
	}
	if (olAftUrnos.GetLength() > 0)
		olAftUrnos = olAftUrnos.Left(olAftUrnos.GetLength() - 1);
	

	// Split urnolist in 100 pieces blocks and read Aft records
	CStringArray olUrnosLists;
	CString olSelection;
	for(int i = SplitItemList(olAftUrnos, &olUrnosLists, 100) - 1; i >= 0; i--)
	{
		olSelection.Format("WHERE URNO IN (%s)", olUrnosLists[i]);
		omAftData.Read(olSelection, false);
	}

	ResetCcaFlno();

	return true;
}


// set the flno in CCADATA to the flno of AFTDATA
bool CedaCcaData::ResetCcaFlno()
{
	CCADATA *prlCca;
	const AFTDATA *prlAft;
	int ilCcaCount = omData.GetSize();
	for (int ilLc = 0; ilLc < ilCcaCount; ilLc++)
	{
		prlCca = (CCADATA *)omData[ilLc];
		if (prlCca->Ctyp[0] != 'C' && prlCca->Flnu > 0)
		{
			prlAft = omAftData.GetRecByUrno(prlCca->Flnu);
			if (prlAft != NULL)
			{
				strncpy(prlCca->Flno, prlAft->Flno, DBFIELDLEN_CCA_FLNO);
			}
		}
	}

	return true;
}


bool CedaCcaData::Save(CCADATA *prpCca)
{
	if (prpCca == NULL)
		return false;

	bool olRc = true;
	CString olData;
	CString olSel;
	if (prpCca->Urno < 1)
	{
		// insert record
		prpCca->Urno = ogBasicData.GetNextUrno(); 

		MakeCedaData(&omRecInfo, olData, prpCca);

		olRc = CedaAction("IRT", "", "", olData.GetBuffer(0));
		CCuteIFApp::CheckCedaError(*this);

		//TRACE("CedaCcaData::Save: Insert: %s", olData);

	}
	else
	{
		// update record
		olSel.Format("WHERE URNO = %ld", prpCca->Urno);

		MakeCedaData(&omRecInfo, olData, prpCca);

		olRc = CedaAction("URT", olSel.GetBuffer(0), "", olData.GetBuffer(0));
		CCuteIFApp::CheckCedaError(*this);

		//TRACE("CedaCcaData::Save: Update: %s  %s", olSel, olData);
	}
	
	return olRc;
}


bool CedaCcaData::SaveDiff(CCADATA *prpNewCca)
{
	if (prpNewCca == NULL)
		return false;

	if (prpNewCca->Urno < 1 ||
		GetRecByUrno(prpNewCca->Urno) == NULL)
	{
		// old record not present!!
		// save whole record!!
		return Save(prpNewCca);
	}

	bool olRc = true;

	// get existing record
	CCADATA *prlOldCca = GetRecByUrno(prpNewCca->Urno);

	// update only differences
	CString olSel;
	olSel.Format("WHERE URNO = %ld", prpNewCca->Urno);


	CString olFieldList = CString(pcmFieldList);
	CString olListOfData;
	MakeCedaData(&omRecInfo, olListOfData, olFieldList, prlOldCca, prpNewCca);

	if(olFieldList.IsEmpty())
	{
		// nothing has changed!
		return true;
	}


	//MakeCedaData(&omRecInfo, olData, prpCca);
	olRc = CedaAction("URT", pcmTableName, olFieldList.GetBuffer(0), olSel.GetBuffer(0),"", olListOfData.GetBuffer(0), "BUF1"); 

	//olRc = CedaAction("URT", olSel.GetBuffer(0), "", olData.GetBuffer(0));
	CCuteIFApp::CheckCedaError(*this);
	
	return olRc;
}


/*
CCADATA *CedaCcaData::ReadCuteRecord(const CString &ropCkic)
{
	rmCuteCcaData.Urno = 0;
	strncpy(rmCuteCcaData.Ckic, ropCkic, 6);

    // Select data from the database
	CTime olCurrUtc = CTime::GetCurrentTime();
	ogBasicData.LocalToUtc(olCurrUtc);

	CString olSel;
	olSel.Format("WHERE CKIC = '%s' CKBA <> ' ' AND CKBA <= '%s' AND (CKEA >= '%s' OR CKEA = ' ')", 
		ropCkic, CTimeToDBString(olCurrUtc, TIMENULL), CTimeToDBString(olCurrUtc, TIMENULL));

	if (CedaAction("RT", olSel.GetBuffer(0)) != true)
	{
		return NULL;
	}

	if (GetBufferSize() > 0)
	{
		GetBufferRecord(0, &rmCuteCcaData);
	}

	if (rmCuteCcaData.Urno < 1)
	{
		return NULL;
	}
	else
	{
		return &rmCuteCcaData;
	}
}
*/

bool CedaCcaData::ReadCkic(const CString &ropCkic, bool bpDdx /* = true */)
{
	omCurrCkic = ropCkic;

    // Select data from the database
	CTime olCurrUtc = CTime::GetCurrentTime();
	ogBasicData.LocalToUtc(olCurrUtc);
	CTime olCurrUtc15 = olCurrUtc + CTimeSpan(0, 0, 15, 0);

	CString olCurrUtcStr = CTimeToDBString(olCurrUtc, TIMENULL);
	CString olCurrUtc15Str = CTimeToDBString(olCurrUtc15, TIMENULL);


	CString olSel;
	olSel.Format("WHERE CKIC = '%s' AND ((CKBS <= '%s' AND CKES >= '%s') OR (CKBA <> ' ' AND CKBA <= '%s' AND (CKEA >= '%s' OR CKEA = ' ')))", 
		ropCkic, olCurrUtc15Str, olCurrUtcStr, olCurrUtc15Str, olCurrUtcStr);

	ClearAll(false);

	return Read(olSel, bpDdx);
}


bool CedaCcaData::ReadCkic(const CString &ropCkic, long lpFlnu, bool bpDdx /* = true */)
{
	omCurrCkic = ropCkic;


	CString olSel;
	olSel.Format("WHERE CKIC = '%s' AND FLNU = '%ld'", ropCkic, lpFlnu); 

	ClearAll(false);

	return Read(olSel, bpDdx);
}





bool CedaCcaData::ReadUrno(long lpUrno, const CString &ropCkic, bool bpDdx /* = true */)
{
	omCurrCkic = ropCkic;


	CString olSel;
	olSel.Format("WHERE URNO = '%ld'", lpUrno);

	ClearAll(false);

	return Read(olSel, bpDdx);
}



//--GET-BY-URNO--------------------------------------------------------------------------------------------

CCADATA *CedaCcaData::GetRecByUrno(long lpUrno) const
{
	CCADATA  *prlCca;
	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlCca) == TRUE)
	{
		return prlCca;
	}
	return NULL;
}


CCADATA *CedaCcaData::GetRecByFlnuCkic(long lpFlnu, const CString &ropCkic) const
{

	CCADATA *prlCca;
	int ilCcaCount = omData.GetSize();
	for (int ilLc = 0; ilLc < ilCcaCount; ilLc++)
	{
		prlCca = (CCADATA *)omData[ilLc];

		if (prlCca->Flnu == lpFlnu && strcmp(prlCca->Ckic, ropCkic) == 0)
			return prlCca;
			
	}
	return NULL;
}


bool CedaCcaData::GetAlc3FromRelFlight(const CCADATA *prpCca, CString &ropAlc) const
{
	if (prpCca == NULL) 
		return false;

	AFTDATA *prlAft = omAftData.GetRecByUrno(prpCca->Flnu);
	if (prlAft == NULL)
		return false;

	ropAlc = prlAft->Alc3;

	return true;
}



/*
bool CedaCcaData::GetOpenCcas(CPtrArray *popOpenCcas) const
{
	if (popOpenCcas == NULL) return false;
	popOpenCcas->RemoveAll();

	CCADATA *prlCca;
	int ilCcaCount = omData.GetSize();
	for (int ilLc = 0; ilLc < ilCcaCount; ilLc++)
	{
		prlCca = (CCADATA *)omData[ilLc];

		if (IsOpen(*prlCca))
			popOpenCcas->Add(prlCca);
			
	}
	return true;
}
*/

bool CedaCcaData::GetMostRecentScheduled(CPtrArray *popCcas)
{
	if (popCcas == NULL) return false;
	popCcas->RemoveAll();

	// get current utc time
	CTime olCurrUtc = CTime::GetCurrentTime();
	ogBasicData.LocalToUtc(olCurrUtc);
	CTime olCurrUtc15 = olCurrUtc + CTimeSpan(0, 0, 15, 0);

	// collect all ccas at the most recent moment(with the same open time) and not actual opened
	CTime olNext = TIMENULL;
	CCADATA *prlCca;
	AFTDATA *prlAft;
	int ilCcaCount = omData.GetSize();
	for (int ilLc = 0; ilLc < ilCcaCount; ilLc++)
	{
		prlAft = NULL;
		prlCca = (CCADATA *)omData[ilLc];
		if (prlCca->Ctyp[0] != 'C')
		{
			if ((prlAft = omAftData.GetRecByUrno(prlCca->Flnu)) != NULL)
			if (prlAft->Ftyp[0] != 'O')
			{
				continue; // Only operational flights!
			}
		}

		if (prlCca->Ckbs < olCurrUtc15 && prlCca->Ckes > olCurrUtc && 
			(prlCca->Ckba == TIMENULL || prlCca->Ckea > olCurrUtc))
		{
			if (olNext == TIMENULL || prlCca->Ckbs - olCurrUtc < olNext - olCurrUtc) 
			{
				// found a better moment
				olNext = prlCca->Ckbs;
				popCcas->RemoveAll();
				popCcas->Add(prlCca);
			}
			else if (prlCca->Ckbs == olNext)
			{
				popCcas->Add(prlCca);
			}
		}

	}

	return true;
}


/*
bool CedaCcaData::IsOpen(const CCADATA &ropCca) const
{
	CTime olCurrUtc = CTime::GetCurrentTime();
	ogBasicData.LocalToUtc(olCurrUtc);

	if (ropCca.Ckba != TIMENULL && ropCca.Ckba <= olCurrUtc && 
		(ropCca.Ckea == TIMENULL || ropCca.Ckea > olCurrUtc))
		return true;

	return false;
}

*/