// FxtData.h: interface for the FxtData class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_FXTDATA_H__9F7FA606_F046_11D4_8008_0050DA1CACF1__INCLUDED_)
#define AFX_FXTDATA_H__9F7FA606_F046_11D4_8008_0050DA1CACF1__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class FxtData  
{
public:
	FxtData();
	virtual ~FxtData();


	bool DBGetFreetext(long lpFldu, int ipNum, CString &ropText) const;
	bool DBUpdateFreetext(long lpFldu, int ipNum, const CString &ropText) const;


};

#endif // !defined(AFX_FXTDATA_H__9F7FA606_F046_11D4_8008_0050DA1CACF1__INCLUDED_)
