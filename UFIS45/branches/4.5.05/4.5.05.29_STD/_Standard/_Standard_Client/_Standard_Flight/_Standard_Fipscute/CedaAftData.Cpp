// CedaAftData.cpp
 
#include "stdafx.h"
#include <afxwin.h>
#include "BasicData.h"
#include "CCSGlobl.h"
#include "CedaAftData.h"
#include "CCSBcHandle.h"
#include "CCSCedadata.h"
#include "CedaCcaData.h"
#include "CuteIF.h"


//--PROCESS-CF---------------------------------------------------------------------------------------------

static void  ProcessBc(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	((CedaAftData *)vpInstance)->ProcessAftBc(ipDDXType,vpDataPointer,ropInstanceName);
}




//--CEDADATA-----------------------------------------------------------------------------------------------

CedaAftData::CedaAftData() : CCSCedaData(&ogCommHandler)
{
    BEGIN_CEDARECINFO(AFTDATA, AftDataRecInfo)
		CCS_FIELD_LONG		(Urno,"URNO","Eindeutige Datensatz-Nr.", 1)
		CCS_FIELD_CHAR_TRIM	(Alc2,"ALC2","Fluggesellschaft (Airline 2-Letter Code)", 1)
		CCS_FIELD_CHAR_TRIM	(Alc3,"ALC3","Fluggesellschaft (Airline 3-Letter Code)", 1)
		CCS_FIELD_CHAR_TRIM	(Flno,"FLNO","komplette Flugnummer (Airline, Nummer, Suffix)", 1)
		CCS_FIELD_CHAR_TRIM	(Flns,"FLNS","Suffix", 1)
		CCS_FIELD_CHAR_TRIM	(Fltn,"FLTN","Flugnummer", 1)
		CCS_FIELD_CHAR_TRIM	(Ftyp,"FTYP","Flighttype", 1)
		CCS_FIELD_DATE		(Tifd,"TIFD","Beste Abflugzeit", 1)
		CCS_FIELD_CHAR_TRIM	(Gtd1,"GTD1","Gate 1 Abflug", 1)
		CCS_FIELD_CHAR_TRIM	(Gtd2,"GTD2","Doppelgate Abflug", 1)
		CCS_FIELD_CHAR_TRIM	(Gta1,"GTA1","Gate 1 Ankunft", 1)
		CCS_FIELD_CHAR_TRIM	(Gta2,"GTA2","Gate 2 Ankunft", 1)
		CCS_FIELD_DATE		(Gd1b,"GD1B","Belegung Gate 1 Abflug geplanter Beginn", 1)
		CCS_FIELD_DATE		(Gd1e,"GD1E","Belegung Gate 1 Abflug geplantes Ende", 1)
		CCS_FIELD_DATE		(Gd2b,"GD2B","Belegung Gate 2 Abflug geplanter Beginn", 1)
		CCS_FIELD_DATE		(Gd2e,"GD2E","Belegung Gate 2 Abflug geplantes Ende", 1)
		CCS_FIELD_DATE		(Ga1b,"GA1B","Belegung Gate 1 Ankunft geplanter Beginn", 1)
		CCS_FIELD_DATE		(Ga1e,"GA1E","Belegung Gate 1 Ankunft geplantes Ende", 1)
		CCS_FIELD_DATE		(Ga2b,"GA2B","Belegung Gate 2 Ankunft geplanter Beginn", 1)
		CCS_FIELD_DATE		(Ga2e,"GA2E","Belegung Gate 2 Ankunft geplantes Ende", 1)
		CCS_FIELD_DATE		(Gd1x,"GD1X","Belegung Gate 1 Abflug aktueller Beginn", 1)
		CCS_FIELD_DATE		(Gd1y,"GD1Y","Belegung Gate 1 Abflug aktuelles Ende", 1)
		CCS_FIELD_DATE		(Gd2x,"GD2X","Belegung Gate 2 Abflug aktueller Beginn", 1)
		CCS_FIELD_DATE		(Gd2y,"GD2Y","Belegung Gate 2 Abflug aktuelles Ende", 1)
		CCS_FIELD_DATE		(Ga1x,"GA1X","Belegung Gate 1 Ankunft aktueller Beginn", 1)
		CCS_FIELD_DATE		(Ga1y,"GA1Y","Belegung Gate 1 Ankunft aktuelles Ende", 1)
		CCS_FIELD_DATE		(Ga2x,"GA2X","Belegung Gate 2 Ankunft aktueller Beginn", 1)
		CCS_FIELD_DATE		(Ga2y,"GA2Y","Belegung Gate 2 Ankunft aktuelles Ende", 1)		
		CCS_FIELD_CHAR_TRIM	(Remp,"REMP","FIDS Remark", 1)
  END_CEDARECINFO

    // Copy the record structure
    for (int i = 0; i < sizeof(AftDataRecInfo)/sizeof(AftDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&AftDataRecInfo[i],sizeof(CEDARECINFO));
        omRecInfo.Add(prpCedaRecInfo);
	}
    // Initialize table names and field names
    strcpy(pcmTableName,"AFT");
    sprintf(pcmListOfFields,"URNO,ALC2,ALC3,FLNO,FLNS,FLTN,FTYP,TIFD,GTD1,GTD2,GTA1,GTA2,GD1B,GD1E,GD2B,GD2E,GA1B,GA1E,GA2B,GA2E,GD1X,GD1Y,GD2X,GD2Y,GA1X,GA1Y,GA2X,GA2Y,REMP");
	pcmFieldList = pcmListOfFields; //because in CedaData pcmFieldList is just a pointer
	
	Register();

	omData.RemoveAll();
}

//--~CEDADATA----------------------------------------------------------------------------------------------

CedaAftData::~CedaAftData(void)
{
	UnRegister();
	omRecInfo.DeleteAll();
	ClearAll();
}


//--REGISTER----------------------------------------------------------------------------------------------

void CedaAftData::Register(void)
{
	ogDdx.Register((void *)this, BC_FLIGHT_NEW,	CString("AFTDATA"), CString("Aft-new"),	ProcessBc);
	ogDdx.Register((void *)this, BC_FLIGHT_CHANGE,	CString("AFTDATA"), CString("Aft-changed"),	ProcessBc);
	ogDdx.Register((void *)this, BC_FLIGHT_DELETE,	CString("AFTDATA"), CString("Aft-deleted"),	ProcessBc);
}


void CedaAftData::UnRegister(void)
{
	ogDdx.UnRegister(this,NOTUSED);
}


//--CLEAR-ALL----------------------------------------------------------------------------------------------

void CedaAftData::ClearAll(bool bpWithRegistration)
{
    omUrnoMap.RemoveAll();
	//omCkicMap.RemoveAll();

	for (int i = 0; i < omData.GetSize(); i++)
		delete omData[i];
    omData.RemoveAll();

	if(bpWithRegistration)
	{
		ogDdx.UnRegister(this,NOTUSED);
	}
}




//--PROCESS-BC---------------------------------------------------------------------------------------------

void  CedaAftData::ProcessAftBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{

	struct BcStruct *prlAftData = (struct BcStruct *) vpDataPointer;



	if(ipDDXType == BC_FLIGHT_NEW)
	{
		/*
		AFTDATA rlAft;
		GetRecordFromItemList(&rlAft, prlAftData->Fields, prlAftData->Data);
		int asdf = 0;
		*/
	}		


	if(ipDDXType == BC_FLIGHT_CHANGE)
	{
		AFTDATA rlAft;
		GetRecordFromItemList(&rlAft, prlAftData->Fields, prlAftData->Data);

		if (GetRecByUrno(rlAft.Urno) != NULL)
		{
			// loaded record has changed! 
			ReloadData();
		}
		else
		{
			if (!omCurrGate.IsEmpty())
			{
				// check if relevant fields has been changed
				if (IsChangeRelevant(prlAftData->Fields, prlAftData->Data) == true)
					ReloadData();
			}
		}
	}


	if(ipDDXType == BC_FLIGHT_DELETE)
	{
		CString olUrno;
		if (GetListItemByField(CString(prlAftData->Data), CString(prlAftData->Fields), CString("URNO"), olUrno) == true)
		{
			AFTDATA *prlAft = GetRecByUrno(atol(olUrno));
			if (prlAft != NULL)
			{
				DeleteInternal(prlAft);
			}
		}
	}
}

//---------------------------------------------------------------------------------------------------------



bool CedaAftData::IsChangeRelevant(const char *prpBcFields, const char *prpBcData)
{
	if (prpBcFields == NULL || prpBcData == NULL)
		return false;

	// check if gate has changed and is now current gate!
	CString olGate;
	if (GetListItemByField(CString(prpBcData), CString(prpBcFields), CString("GTD1"), olGate) == true)
	{
		if (olGate == omCurrGate)
			return true;
	}
	if (GetListItemByField(CString(prpBcData), CString(prpBcFields), CString("GTD2"), olGate) == true)
	{
		if (olGate == omCurrGate)
			return true;
	}

	// check if allocation times has been changed!
	CString olFields(prpBcFields);

	if (olFields.Find("GD1B") != -1) return true;
	if (olFields.Find("GD1E") != -1) return true;
	if (olFields.Find("GD1X") != -1) return true;
	//if (olFields.Find("GD1Y") != -1) return true;

	if (olFields.Find("GD2B") != -1) return true;
	if (olFields.Find("GD2E") != -1) return true;
	if (olFields.Find("GD2X") != -1) return true;
	//if (olFields.Find("GD2Y") != -1) return true;

	return false;
}



//--INSERT-INTERNAL--------------------------------------------------------------------------------------

bool CedaAftData::InsertInternal(AFTDATA *prpAft, bool bpDdx /* = true */)
{
	omData.Add(prpAft);//Update omData
	omUrnoMap.SetAt((void *)prpAft->Urno, prpAft);
	if (bpDdx)
		ogDdx.DataChanged((void *)this, FLIGHT_NEW,(void *)prpAft ); //Update Viewer
    return true;
}


//--DELETE-INTERNAL----------------------------------------------------------------------------------------

bool CedaAftData::DeleteInternal(AFTDATA *prpAft, bool bpDdx /* = true */)
{
	if (bpDdx)
		ogDdx.DataChanged((void *)this,FLIGHT_DELETE,(void *)prpAft); //Update Viewer
	omUrnoMap.RemoveKey((void *)prpAft->Urno);
	int ilAftCount = omData.GetSize();
	for (int ilLc = 0; ilLc < ilAftCount; ilLc++)
	{
		if (((AFTDATA *)omData[ilLc])->Urno == prpAft->Urno)
		{
			delete omData[ilLc];
			omData.RemoveAt(ilLc);//Update omData
			break;
		}
	}
    return true;
}

//--UPDATE-INTERNAL--------------------------------------------------------------------------------------------

bool CedaAftData::UpdateInternal(AFTDATA *prpAft, bool bpDdx /* = true */)
{
	AFTDATA *prlAft = GetRecByUrno(prpAft->Urno);
	if (prlAft != NULL)
	{
		*prlAft = *prpAft; //Update omData
		if (bpDdx)
			ogDdx.DataChanged((void *)this,FLIGHT_CHANGE,(void *)prlAft); //Update Viewer
		return true;

	}
    return false;
}



/*
//--READ-ALL-----------------------------------------------------------------------------------------------

//-----------------------------------------------------------------------------------------------

bool CedaAftData::ReadAllFlights(CString &opWhere)
{
	if(omSelection == opWhere)
		return false; 

	ClearAll(false);

	omSelection = CString("CKBS<>' ' AND CKIC<>' ' AND ") + opWhere;

	CString olSelection = CString("WHERE ") + omSelection;

	char pclSel[1024];
    
	strcpy(pclSel, olSelection );

	return Read(pclSel);

} 
*/


bool CedaAftData::Read(CString opWhere, bool bpDdx /* = true */)
{
	if (opWhere.IsEmpty())
		return false;

    // Select data from the database


	bool blRet = CedaAction("GFR", "", pcmListOfFields, opWhere.GetBuffer(0), "", "", "BUF1");
	CCuteIFApp::CheckCedaError(*this);

	if (blRet != true)
	{
		return false;
	}

	// Load data from CedaData into the dynamic array of record
	for (int ilLc = 0; blRet == true; ilLc++)
	{
		AFTDATA *prlAft = new AFTDATA;
		if ((blRet = GetBufferRecord(ilLc,prlAft)) == true)
		{
			if(UpdateInternal(prlAft, bpDdx))
			{
				delete prlAft;
			}
			else
			{
				InsertInternal(prlAft, bpDdx);
			}
		}
		else
		{
			delete prlAft;
		}
	}
    return true;

}



bool CedaAftData::ReadGateRecs(const CString &ropGate, bool bpDdx /* = true */)
{
	omCurrGate = ropGate;

    // Select data from the database
	CTime olCurrUtc = CTime::GetCurrentTime();
	ogBasicData.LocalToUtc(olCurrUtc);
	CTime olCurrUtc15 = olCurrUtc + CTimeSpan(0, 0, 15, 0);
	CTime olCurrUtcPD = olCurrUtc - CTimeSpan(0, 12, 0, 0);
	CTime olCurrUtcND = olCurrUtc + CTimeSpan(0, 12, 0, 0);

	CString olCurrUtcStr = CTimeToDBString(olCurrUtc, TIMENULL);
	CString olCurrUtc15Str = CTimeToDBString(olCurrUtc15, TIMENULL);
	CString olCurrUtcPDStr = CTimeToDBString(olCurrUtcPD, TIMENULL);
	CString olCurrUtcNDStr = CTimeToDBString(olCurrUtcND, TIMENULL);


	CString olSelGate1;
	olSelGate1.Format("(GTD1 = '%s' AND ((GD1B <= '%s' AND GD1E >= '%s') OR (GD1X <> ' ' AND GD1X <= '%s' AND (GD1Y >= '%s' OR GD1Y = ' '))))", 
		ropGate, olCurrUtc15Str, olCurrUtcStr, olCurrUtc15Str, olCurrUtcStr);
	CString olSelGate2;
	olSelGate2.Format("(GTD2 = '%s' AND ((GD2B <= '%s' AND GD2E >= '%s') OR (GD2X <> ' ' AND GD2X <= '%s' AND (GD2Y >= '%s' OR GD2Y = ' '))))", 
		ropGate, olCurrUtc15Str, olCurrUtcStr, olCurrUtc15Str, olCurrUtcStr);

	omCurrSel.Format("WHERE (TIFD BETWEEN '%s' AND '%s') AND (%s OR %s)", 
		olCurrUtcPDStr, olCurrUtcNDStr, olSelGate1, olSelGate2);

	ClearAll(false);

	return Read(omCurrSel, bpDdx);
}




bool CedaAftData::ReadCcaFlights(const CStringArray &ropFlnus, bool bpDdx /* = true */ )
{
	omCurrGate.Empty();
	// create urno-list string
	CString olUrnos;
	for (int i = 0; i < ropFlnus.GetSize(); i++)
	{
		olUrnos += ropFlnus[i];
		olUrnos += ',';
	}

	if (olUrnos.IsEmpty())
		return false;

	olUrnos = olUrnos.Left(olUrnos.GetLength() - 1);


	ClearAll(false);

	// read records
	CStringArray olUrnoLists;
	CString olWhere;
	bool blRet = false;
	if (SplitItemList(olUrnos, &olUrnoLists, 250) > 0)
	{
		omCurrSel.Format("WHERE URNO IN (%s)", olUrnoLists[0]);
		blRet = Read(omCurrSel, bpDdx);
	}
	return blRet;
}



bool CedaAftData::ReadUrno(long lpUrno, bool bpDdx /* = true */ )
{
	omCurrGate.Empty();


	ClearAll(false);

	// read records
	omCurrSel.Format("WHERE URNO = '%ld'", lpUrno);
	return Read(omCurrSel, bpDdx);
}



bool CedaAftData::ReadOneFlight(long lpUrno, AFTDATA &ropFlight) // const 
{
	if (lpUrno < 1)
		return false;

	CString olSel;
	olSel.Format("WHERE URNO = '%ld'", lpUrno);


	bool blRet = CedaAction("GFR", "", pcmListOfFields, olSel.GetBuffer(0), "", "", "BUF1");
	CCuteIFApp::CheckCedaError(*this);
	if (blRet != true)
	{
		return false;
	}

	if (!GetBufferRecord(0, &ropFlight))
		return false;


    return true;
}



bool CedaAftData::ReloadData()
{
	ClearAll(false);

	return Read(omCurrSel);
}


bool CedaAftData::Update(AFTDATA *prpAft)
{
	if (prpAft == NULL)
		return false;

	bool olRc = true;
	AFTDATA *prlOldAft = GetRecByUrno(prpAft->Urno);
	if (prlOldAft == NULL)
		return false;

	// update record
	CString olSel;
	olSel.Format("WHERE URNO = %ld", prpAft->Urno);

	CString olData;
	CString olFields(pcmFieldList);	
	MakeCedaData(&omRecInfo, olData, olFields, prlOldAft, prpAft);

	if(olFields.IsEmpty())
		return true;
	
	bool blRet = CedaAction("UFR", pcmTableName, olFields.GetBuffer(0), olSel.GetBuffer(0),"", olData.GetBuffer(0), "BUF1", false); 
	CCuteIFApp::CheckCedaError(*this);
	
	return blRet;
}





//--GET-BY-URNO--------------------------------------------------------------------------------------------

AFTDATA *CedaAftData::GetRecByUrno(long lpUrno) const
{
	AFTDATA  *prlAft;
	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlAft) == TRUE)
	{
		return prlAft;
	}
	return NULL;
}





bool CedaAftData::GetMostRecentScheduled(CPtrArray *popAfts)
{
	if (popAfts == NULL) return false;
	popAfts->RemoveAll();

	CTime olOpenS;
	CTime olCloseS;
	CTime olOpenA;

	// get current utc time
	CTime olCurrUtc = CTime::GetCurrentTime();
	ogBasicData.LocalToUtc(olCurrUtc);
	CTime olCurrUtc15 = olCurrUtc + CTimeSpan(0, 0, 15, 0);

	// collect all afts at the most recent moment(with the same open time) and not actual opened
	CTime olNext = TIMENULL;
	AFTDATA *prlAft;
	int ilAftCount = omData.GetSize();
	for (int ilLc = 0; ilLc < ilAftCount; ilLc++)
	{
		prlAft = (AFTDATA *)omData[ilLc];
		// Only operational flights!
		if (prlAft->Ftyp[0] != 'O')
			continue;

		if (strcmp(prlAft->Gtd1, omCurrGate) == 0)
		{
			olOpenS = prlAft->Gd1b;
			olCloseS = prlAft->Gd1e;
			olOpenA = prlAft->Gd1x;
		}
		else if (strcmp(prlAft->Gtd2, omCurrGate) == 0)
		{
			olOpenS = prlAft->Gd2b;
			olCloseS = prlAft->Gd2e;
			olOpenA = prlAft->Gd2x;
		}
		else
			continue;

		if (olOpenS < olCurrUtc15 && olCloseS > olCurrUtc && olOpenA == TIMENULL)
		{
			if (olNext == TIMENULL || olOpenS - olCurrUtc < olNext - olCurrUtc) 
			{
				// found a better moment
				olNext = olOpenS;
				popAfts->RemoveAll();
				popAfts->Add(prlAft);
			}
			else if (olOpenS == olNext)
			{
				popAfts->Add(prlAft);
			}
		}

	}

	return true;
}





/*
bool CedaAftData::GetOpenGateRecs(CPtrArray *popOpenGateRecs) const
{
	if (popOpenGateRecs == NULL) return false;
	popOpenGateRecs->RemoveAll();

	AFTDATA *prlAft;
	int ilCount = omData.GetSize();
	for (int i = 0; i < ilCount; i++)
	{
		prlAft = (AFTDATA *)omData[i];

		if (IsOpen(*prlAft))
			popOpenGateRecs->Add(prlAft);
			
	}
	return true;
}


bool CedaAftData::IsOpen(const AFTDATA &ropAft) const
{
	CTime olCurrUtc = CTime::GetCurrentTime();
	ogBasicData.LocalToUtc(olCurrUtc);

	if (strcmp(ropAft.Gtd1, omCurrGate) == 0)
	{
		if (ropAft.Gd1x != TIMENULL && ropAft.Gd1x <= olCurrUtc && 
			(ropAft.Gd1y == TIMENULL || ropAft.Gd1y > olCurrUtc))
			return true;
	}
	if (strcmp(ropAft.Gtd2, omCurrGate) == 0)
	{
		if (ropAft.Gd2x != TIMENULL && ropAft.Gd2x <= olCurrUtc && 
			(ropAft.Gd2y == TIMENULL || ropAft.Gd2y > olCurrUtc))
			return true;
	}

	return false;
}

*/

