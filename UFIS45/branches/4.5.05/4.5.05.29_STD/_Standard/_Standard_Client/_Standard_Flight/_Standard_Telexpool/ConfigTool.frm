VERSION 5.00
Begin VB.Form ConfigTool 
   Caption         =   "TelexPool Configuration Tool"
   ClientHeight    =   10560
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   10635
   LinkTopic       =   "Form1"
   ScaleHeight     =   10560
   ScaleWidth      =   10635
   StartUpPosition =   3  'Windows Default
   Begin VB.CheckBox chkLoad 
      Caption         =   "Load"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   7080
      Style           =   1  'Graphical
      TabIndex        =   106
      Top             =   10080
      Width           =   735
   End
   Begin VB.CheckBox chkSave 
      Caption         =   "Save"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   7920
      Style           =   1  'Graphical
      TabIndex        =   105
      Top             =   10080
      Width           =   735
   End
   Begin VB.TextBox CfgTextFilter 
      Height          =   285
      Index           =   3
      Left            =   7560
      TabIndex        =   104
      Top             =   4920
      Width           =   2535
   End
   Begin VB.TextBox CfgTextFilter 
      Height          =   285
      Index           =   2
      Left            =   7560
      TabIndex        =   103
      Top             =   4560
      Width           =   2535
   End
   Begin VB.TextBox CfgTextFilter 
      Height          =   285
      Index           =   1
      Left            =   7560
      TabIndex        =   102
      Top             =   4200
      Width           =   2535
   End
   Begin VB.TextBox CfgTextFilterType 
      Height          =   285
      Index           =   1
      Left            =   6840
      TabIndex        =   99
      Top             =   4200
      Width           =   615
   End
   Begin VB.CheckBox chkRead 
      Caption         =   "Read"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   8760
      Style           =   1  'Graphical
      TabIndex        =   98
      Top             =   10080
      Width           =   735
   End
   Begin VB.CheckBox chkClose 
      Caption         =   "&Close"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   9600
      Style           =   1  'Graphical
      TabIndex        =   97
      Top             =   10080
      Width           =   735
   End
   Begin VB.Frame Frame9 
      Caption         =   "Miscellaneous"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1695
      Left            =   5520
      TabIndex        =   91
      Top             =   8160
      Width           =   4815
      Begin VB.TextBox CfgTestSetServerTime 
         Height          =   285
         Left            =   2040
         TabIndex        =   96
         Top             =   1320
         Width           =   2535
      End
      Begin VB.CheckBox CfgTestSaveLocalData 
         Caption         =   "Test Save Local Data"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   120
         TabIndex        =   94
         Top             =   960
         Width           =   2295
      End
      Begin VB.CheckBox CfgTestModeLocal 
         Caption         =   "Test Mode Local"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   93
         Top             =   600
         Width           =   1935
      End
      Begin VB.CheckBox CfgTestCaseMode 
         Caption         =   "Test Case Mode"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   120
         TabIndex        =   92
         Top             =   240
         Width           =   2175
      End
      Begin VB.Label Label34 
         Caption         =   "Test Set Server Time :"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   95
         Top             =   1320
         Width           =   1935
      End
   End
   Begin VB.Frame Frame8 
      Caption         =   "SITA Interface"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2535
      Left            =   240
      TabIndex        =   76
      Top             =   7920
      Width           =   4575
      Begin VB.CheckBox CfgSitaOutSmi 
         Caption         =   "SITA Out SMI"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   2280
         TabIndex        =   90
         Top             =   2160
         Width           =   2055
      End
      Begin VB.CheckBox CfgSitaOutMsgId 
         Caption         =   "SITA Out Msg Id"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   89
         Top             =   2160
         Width           =   2055
      End
      Begin VB.CheckBox CfgSitaOutOrigin 
         Caption         =   "SITA Out Origin"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   2280
         TabIndex        =   88
         Top             =   1800
         Width           =   2055
      End
      Begin VB.CheckBox CfgAddressFilter 
         Caption         =   "Address Filter"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   87
         Top             =   1800
         Width           =   2055
      End
      Begin VB.TextBox CfgAddressNoCheck 
         Height          =   285
         Left            =   1440
         TabIndex        =   86
         Text            =   "KRIS,ATC,AFTN,SCOR"
         Top             =   1440
         Width           =   2895
      End
      Begin VB.TextBox CfgSitaOutPrio 
         Height          =   285
         Left            =   1440
         TabIndex        =   85
         Text            =   "QU"
         Top             =   1080
         Width           =   735
      End
      Begin VB.TextBox CfgSitaOutType 
         Height          =   285
         Left            =   1440
         TabIndex        =   84
         Text            =   "SITA_NODE"
         Top             =   720
         Width           =   1095
      End
      Begin VB.TextBox CfgHomeTlxAddr 
         Height          =   285
         Left            =   3600
         TabIndex        =   83
         Text            =   "[????]"
         Top             =   360
         Width           =   735
      End
      Begin VB.TextBox CfgWksSitaAddr 
         Height          =   285
         Left            =   1440
         TabIndex        =   82
         Text            =   "[????]"
         Top             =   360
         Width           =   735
      End
      Begin VB.Label Label33 
         Caption         =   "No Check :"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   81
         Top             =   1440
         Width           =   1815
      End
      Begin VB.Label Label32 
         Caption         =   "SITA Out Prio :"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   80
         Top             =   1080
         Width           =   1695
      End
      Begin VB.Label Label31 
         Caption         =   "SITA Out Type :"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   79
         Top             =   720
         Width           =   1815
      End
      Begin VB.Label Label30 
         Caption         =   "Home Tlx Addr :"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   2280
         TabIndex        =   78
         Top             =   360
         Width           =   1815
      End
      Begin VB.Label Label29 
         Caption         =   "SITA Address :"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   77
         Top             =   360
         Width           =   1815
      End
   End
   Begin VB.Frame Frame7 
      Caption         =   "Interface to Other Applications"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2295
      Left            =   5520
      TabIndex        =   65
      Top             =   5760
      Width           =   4815
      Begin VB.TextBox CfgImportTool 
         Height          =   285
         Left            =   2040
         TabIndex        =   75
         Text            =   "c:\ufis\appl\ImportFlights.exe"
         Top             =   1920
         Width           =   2535
      End
      Begin VB.TextBox CfgUldInfo 
         Height          =   285
         Left            =   2040
         TabIndex        =   74
         Text            =   "E"
         Top             =   1440
         Width           =   495
      End
      Begin VB.TextBox CfgLoaInfo 
         Height          =   285
         Left            =   2040
         TabIndex        =   73
         Text            =   "D,C"
         Top             =   1080
         Width           =   495
      End
      Begin VB.TextBox CfgPaxInfo 
         Height          =   285
         Left            =   2040
         TabIndex        =   72
         Text            =   "A,B"
         Top             =   720
         Width           =   495
      End
      Begin VB.TextBox CfgLoadTabViewer 
         Height          =   285
         Left            =   2040
         TabIndex        =   71
         Text            =   "c:\ufis\appl\LoadTabViewer.exe"
         Top             =   360
         Width           =   2535
      End
      Begin VB.Label Label28 
         Caption         =   "Import Tool:"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   70
         Top             =   1920
         Width           =   1215
      End
      Begin VB.Label Label27 
         Caption         =   "ULD Info :"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   69
         Top             =   1440
         Width           =   1935
      End
      Begin VB.Label Label26 
         Caption         =   "Load Info :"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   68
         Top             =   1080
         Width           =   1815
      End
      Begin VB.Label Label25 
         Caption         =   "Pax Info :"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   67
         Top             =   720
         Width           =   1695
      End
      Begin VB.Label Label24 
         Caption         =   "LoadTab Viewer :"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   66
         Top             =   360
         Width           =   1815
      End
   End
   Begin VB.Frame Frame6 
      Caption         =   "Online Windows Parameters"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1815
      Left            =   240
      TabIndex        =   54
      Top             =   6000
      Width           =   4575
      Begin VB.TextBox CfgOnlineAhead 
         Alignment       =   2  'Center
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "0"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1031
            SubFormatType   =   1
         EndProperty
         Height          =   285
         Left            =   3600
         TabIndex        =   64
         Text            =   "16"
         Top             =   1440
         Width           =   495
      End
      Begin VB.TextBox CfgOnlinePrior 
         Alignment       =   2  'Center
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "0"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1031
            SubFormatType   =   1
         EndProperty
         Height          =   285
         Left            =   1440
         TabIndex        =   63
         Text            =   "-2"
         Top             =   1440
         Width           =   495
      End
      Begin VB.TextBox CfgOnlineMaxLines 
         Alignment       =   2  'Center
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "0"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1031
            SubFormatType   =   1
         EndProperty
         Height          =   285
         Left            =   2040
         TabIndex        =   62
         Text            =   "500"
         Top             =   1080
         Width           =   615
      End
      Begin VB.TextBox CfgRecvNoShow 
         Height          =   285
         Left            =   2040
         TabIndex        =   61
         Top             =   720
         Width           =   2415
      End
      Begin VB.TextBox CfgOnlineNoShow 
         Height          =   285
         Left            =   2040
         TabIndex        =   60
         Top             =   360
         Width           =   2415
      End
      Begin VB.Label Label23 
         Caption         =   "Ahead :"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   2280
         TabIndex        =   59
         Top             =   1440
         Width           =   855
      End
      Begin VB.Label Label22 
         Caption         =   "Prior :"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   58
         Top             =   1440
         Width           =   975
      End
      Begin VB.Label Label21 
         Caption         =   "Max. No. Lines :"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   57
         Top             =   1080
         Width           =   1695
      End
      Begin VB.Label Label20 
         Caption         =   "Receive No Show :"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   56
         Top             =   720
         Width           =   1575
      End
      Begin VB.Label Label19 
         Caption         =   "No Show Types :"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   55
         Top             =   360
         Width           =   1695
      End
   End
   Begin VB.Frame Frame5 
      Caption         =   "Main Window Parameters"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1935
      Left            =   240
      TabIndex        =   41
      Top             =   3960
      Width           =   4575
      Begin VB.TextBox CfgMainAhead 
         Alignment       =   2  'Center
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "0"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1031
            SubFormatType   =   1
         EndProperty
         Height          =   285
         Left            =   3600
         TabIndex        =   53
         Text            =   "16"
         Top             =   1440
         Width           =   495
      End
      Begin VB.TextBox CfgMainPrior 
         Alignment       =   2  'Center
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "0"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1031
            SubFormatType   =   1
         EndProperty
         Height          =   285
         Left            =   1440
         TabIndex        =   52
         Text            =   "-4"
         Top             =   1440
         Width           =   495
      End
      Begin VB.TextBox CfgDateFormat 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   1440
         TabIndex        =   51
         Text            =   "dd.mm.yyyy"
         Top             =   1080
         Width           =   1095
      End
      Begin VB.TextBox CfgFontSize 
         Alignment       =   2  'Center
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "0"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1031
            SubFormatType   =   1
         EndProperty
         Height          =   285
         Left            =   3600
         TabIndex        =   47
         Text            =   "14"
         Top             =   720
         Width           =   495
      End
      Begin VB.TextBox CfgPrintSize 
         Alignment       =   2  'Center
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "0"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1031
            SubFormatType   =   1
         EndProperty
         Height          =   285
         Left            =   1440
         TabIndex        =   46
         Text            =   "12"
         Top             =   720
         Width           =   495
      End
      Begin VB.CheckBox CfgFontBold 
         Caption         =   "Font Bold"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   2280
         TabIndex        =   43
         Top             =   360
         Width           =   1935
      End
      Begin VB.CheckBox CfgStartOnTop 
         Caption         =   "Start On Top"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   42
         Top             =   360
         Value           =   1  'Checked
         Width           =   1815
      End
      Begin VB.Label Label18 
         Caption         =   "Ahead :"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   2280
         TabIndex        =   50
         Top             =   1440
         Width           =   855
      End
      Begin VB.Label Label17 
         Caption         =   "Prior :"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   49
         Top             =   1440
         Width           =   735
      End
      Begin VB.Label Label16 
         Caption         =   "Date Format :"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   48
         Top             =   1080
         Width           =   1935
      End
      Begin VB.Label Label15 
         Caption         =   "Font Size :"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   2280
         TabIndex        =   45
         Top             =   720
         Width           =   1575
      End
      Begin VB.Label Label14 
         Caption         =   "Print Size :"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   44
         Top             =   720
         Width           =   1455
      End
   End
   Begin VB.Frame Frame4 
      Caption         =   "Telex Text Filter"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2535
      Left            =   5520
      TabIndex        =   34
      Top             =   3120
      Width           =   4815
      Begin VB.TextBox CfgTextFilterType 
         Height          =   285
         Index           =   3
         Left            =   1320
         TabIndex        =   101
         Top             =   1800
         Width           =   615
      End
      Begin VB.TextBox CfgTextFilterType 
         Height          =   285
         Index           =   2
         Left            =   1320
         TabIndex        =   100
         Top             =   1440
         Width           =   615
      End
      Begin VB.CheckBox CfgIgnoreEmpty 
         Caption         =   "Ignore Empty Telexes"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   40
         Top             =   2160
         Value           =   1  'Checked
         Width           =   2895
      End
      Begin VB.TextBox CfgTextFilter 
         Height          =   285
         Index           =   0
         Left            =   2040
         TabIndex        =   39
         Top             =   720
         Width           =   2535
      End
      Begin VB.TextBox CfgTextFilterType 
         Height          =   285
         Index           =   0
         Left            =   1320
         TabIndex        =   38
         Top             =   720
         Width           =   615
      End
      Begin VB.TextBox CfgAllTypesTextFilter 
         Height          =   285
         Left            =   2040
         TabIndex        =   37
         Top             =   360
         Width           =   2535
      End
      Begin VB.Label Label13 
         Caption         =   "Type Filters :"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   36
         Top             =   720
         Width           =   1695
      End
      Begin VB.Label Label12 
         Caption         =   "Filter for all Types :"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   35
         Top             =   360
         Width           =   1695
      End
   End
   Begin VB.Frame Frame3 
      Caption         =   "Folder Configuration"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2895
      Left            =   5520
      TabIndex        =   18
      Top             =   120
      Width           =   4815
      Begin VB.CheckBox CfgShowFolders 
         Caption         =   "Show Folders"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   2880
         TabIndex        =   33
         Top             =   2520
         Value           =   1  'Checked
         Width           =   1815
      End
      Begin VB.TextBox CfgMaxFolderRows 
         Alignment       =   2  'Center
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "0"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1031
            SubFormatType   =   1
         EndProperty
         Height          =   285
         Left            =   2040
         TabIndex        =   32
         Text            =   "3"
         Top             =   2520
         Width           =   375
      End
      Begin VB.TextBox CfgFolderSize 
         Alignment       =   2  'Center
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "0"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1031
            SubFormatType   =   1
         EndProperty
         Height          =   285
         Left            =   2040
         TabIndex        =   31
         Text            =   "43"
         Top             =   2160
         Width           =   375
      End
      Begin VB.TextBox CfgMainNoShow 
         Height          =   285
         Left            =   2040
         TabIndex        =   30
         Top             =   1800
         Width           =   2535
      End
      Begin VB.TextBox CfgRecvTypes 
         Height          =   285
         Left            =   2040
         TabIndex        =   29
         Top             =   1440
         Width           =   2535
      End
      Begin VB.TextBox CfgFolderTypCodes 
         Height          =   285
         Left            =   2040
         TabIndex        =   28
         Top             =   1080
         Width           =   2535
      End
      Begin VB.TextBox CfgFolderCaptions 
         Height          =   285
         Left            =   2040
         TabIndex        =   27
         Top             =   720
         Width           =   2535
      End
      Begin VB.TextBox CfgTelexTypes 
         Height          =   285
         Left            =   2040
         TabIndex        =   26
         Top             =   360
         Width           =   2535
      End
      Begin VB.Label Label11 
         Caption         =   "Max Folder Rows :"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   25
         Top             =   2520
         Width           =   1815
      End
      Begin VB.Label Label10 
         Caption         =   "Folder Size :"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   24
         Top             =   2160
         Width           =   1695
      End
      Begin VB.Label Label9 
         Caption         =   "No Show Types :"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   23
         Top             =   1800
         Width           =   1815
      End
      Begin VB.Label Label8 
         Caption         =   "Receive Types :"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   22
         Top             =   1440
         Width           =   1935
      End
      Begin VB.Label Label7 
         Caption         =   "Folder Type Codes :"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   21
         Top             =   1080
         Width           =   1815
      End
      Begin VB.Label Label6 
         Caption         =   "Folder Captions :"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   20
         Top             =   720
         Width           =   1935
      End
      Begin VB.Label Label5 
         Caption         =   "Telex Types :"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   19
         Top             =   360
         Width           =   1815
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "Application Layout"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1815
      Left            =   240
      TabIndex        =   9
      Top             =   2040
      Width           =   4575
      Begin VB.CheckBox CfgShowInfo 
         Caption         =   "Show Info"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   2280
         TabIndex        =   17
         Top             =   1440
         Width           =   2055
      End
      Begin VB.CheckBox CfgShowFlightList 
         Caption         =   "Show Flight List"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   2280
         TabIndex        =   16
         Top             =   1080
         Width           =   1935
      End
      Begin VB.CheckBox CfgShowSentList 
         Caption         =   "Show Sent List"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   2280
         TabIndex        =   15
         Top             =   720
         Width           =   1935
      End
      Begin VB.CheckBox CfgShowSent 
         Caption         =   "Show Sent"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   2280
         TabIndex        =   14
         Top             =   360
         Width           =   1815
      End
      Begin VB.CheckBox CfgShowSend 
         Caption         =   "Show Send"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   13
         Top             =   1440
         Width           =   1815
      End
      Begin VB.CheckBox CfgShowRecv 
         Caption         =   "Show Received"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   12
         Top             =   1080
         Value           =   1  'Checked
         Width           =   1935
      End
      Begin VB.CheckBox CfgShowMain 
         Caption         =   "Show Main"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   11
         Top             =   720
         Value           =   1  'Checked
         Width           =   1815
      End
      Begin VB.CheckBox CfgShowIntro 
         Caption         =   "Show Introduction"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   10
         Top             =   360
         Value           =   1  'Checked
         Width           =   1815
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Captions and Titles"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1815
      Left            =   240
      TabIndex        =   0
      Top             =   120
      Width           =   4575
      Begin VB.TextBox CfgSentListCaption 
         Height          =   285
         Left            =   2040
         TabIndex        =   8
         Text            =   "Sent Telexes"
         Top             =   1440
         Width           =   2415
      End
      Begin VB.TextBox CfgRecvListCaption 
         Height          =   285
         Left            =   2040
         TabIndex        =   7
         Text            =   "Received Telexes"
         Top             =   1080
         Width           =   2415
      End
      Begin VB.TextBox CfgRecvCaption 
         Height          =   285
         Left            =   2040
         TabIndex        =   6
         Text            =   "Received Telexes"
         Top             =   720
         Width           =   2415
      End
      Begin VB.TextBox CfgMainCaption 
         Height          =   285
         Left            =   2040
         TabIndex        =   5
         Text            =   "Telex Pool"
         Top             =   360
         Width           =   2415
      End
      Begin VB.Label Label4 
         Caption         =   "Sent List Caption :"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   4
         Top             =   1440
         Width           =   1695
      End
      Begin VB.Label Label3 
         Caption         =   "Receive List Caption :"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   3
         Top             =   1080
         Width           =   1815
      End
      Begin VB.Label Label2 
         Caption         =   "Receive Caption :"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   2
         Top             =   720
         Width           =   1575
      End
      Begin VB.Label Label1 
         Caption         =   "Main Caption :"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   1
         Top             =   360
         Width           =   1455
      End
   End
End
Attribute VB_Name = "ConfigTool"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Form_Load()
    Dim itm As Integer

    CfgMainCaption.Text = "Telex Pool"
    CfgRecvCaption.Text = "Received Telexes"
    CfgRecvListCaption.Text = "Received Telexes"
    CfgSentListCaption = "Sent Telexes"
    
    CfgShowIntro.Value = 1
    CfgShowMain.Value = 1
    CfgShowRecv.Value = 1
    CfgShowSend.Value = 0
    CfgShowSent.Value = 0
    CfgShowSentList.Value = 0
    CfgShowFlightList.Value = 0
    CfgShowInfo.Value = 0
    
    CfgStartOnTop.Value = 1
    CfgFontBold.Value = 0
    CfgPrintSize.Text = "12"
    CfgFontSize.Text = "14"
    CfgDateFormat.Text = "dd.mm.yyyy"
    CfgMainPrior.Text = "-4"
    CfgMainAhead.Text = "16"
    
    CfgOnlineNoShow.Text = ""
    CfgRecvNoShow.Text = ""
    CfgOnlineMaxLines.Text = "500"
    CfgOnlinePrior.Text = "-2"
    CfgOnlineAhead.Text = "16"
    
    CfgTelexTypes.Text = AllTelexTypes
    CfgFolderCaptions.Text = AllTelexTypes
    CfgFolderTypCodes.Text = AllTelexTypes
    CfgRecvTypes.Text = AllTelexTypes
    CfgMainNoShow.Text = ""
    CfgFolderSize.Text = "43"
    CfgMaxFolderRows.Text = "3"
    CfgShowFolders.Value = 1
    
    CfgAllTypesTextFilter.Text = ""
    For itm = 0 To 3
        CfgTextFilterType(itm).Text = ""
        CfgTextFilter(itm).Text = ""
    Next
    CfgIgnoreEmpty.Value = 1
    
    'CfgLoadTabViewer.Text = "C:\Ufis\Appl\LoadtabViewer.exe"
    CfgLoadTabViewer.Text = UFIS_APPL & "\LoadtabViewer.exe"
    CfgPaxInfo.Text = "A,B"
    CfgLoaInfo.Text = "D,C"
    CfgUldInfo.Text = "E"
    'CfgImportTool.Text = "C:\Ufis\Appl\ImportFlights.exe"
    CfgImportTool.Text = UFIS_APPL & "\ImportFlights.exe"
    
    CfgWksSitaAddr.Text = "[????]"
    CfgHomeTlxAddr.Text = "[????]"
    CfgSitaOutType.Text = "SITA_NODE"
    CfgSitaOutPrio.Text = "QU"
    CfgAddressNoCheck.Text = "KRIS,ATC,AFTN,SCOR"
    CfgAddressFilter.Value = 0
    CfgSitaOutOrigin.Value = 0
    CfgSitaOutMsgId.Value = 0
    CfgSitaOutSmi.Value = 0
    
    CfgTestCaseMode.Value = 0
    CfgTestModeLocal.Value = 0
    CfgTestSaveLocalData.Value = 0
    CfgTestSetServerTime.Text = ""
End Sub

Private Sub chkClose_Click()

    chkClose.Value = 0
    ConfigTool.Visible = False
End Sub

Private Sub chkRead_Click()
    Dim tmpVal As String
    Dim tmpTypeList As String
    Dim tmpType As String
    Dim tmpCode As String
    Dim tmpFilter As String
    Dim itm As Integer

    If chkRead.Value = 1 Then
        CfgMainCaption.Text = GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "MAIN_CAPTION", CfgMainCaption.Text)
        CfgRecvCaption.Text = GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "RECV_CAPTION", CfgRecvCaption.Text)
        CfgRecvListCaption.Text = GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "RECV_LIST_CAPTION", CfgRecvListCaption.Text)
        CfgSentListCaption = GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "SENT_LIST_CAPTION", CfgSentListCaption.Text)
    
        If GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "SHOW_INTRO", "YES") = "YES" Then
            CfgShowIntro.Value = 1
        Else
            CfgShowIntro.Value = 0
        End If
        If GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "SHOW_MAIN", "YES") = "YES" Then
            CfgShowMain.Value = 1
        Else
            CfgShowMain.Value = 0
        End If
        If GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "SHOW_RECV", "YES") = "YES" Then
            CfgShowRecv.Value = 1
        Else
            CfgShowRecv.Value = 0
        End If
        If GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "SHOW_SEND", "NO") = "NO" Then
            CfgShowSend.Value = 0
        Else
            CfgShowSend.Value = 1
        End If
        If GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "SHOW_SENT", "NO") = "NO" Then
            CfgShowSent.Value = 0
        Else
            CfgShowSent.Value = 1
        End If
        If GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "SHOW_SENT_LIST", "NO") = "NO" Then
            CfgShowSentList.Value = 0
        Else
            CfgShowSentList.Value = 1
        End If
        If GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "SHOW_FLIGHT_LIST", "NO") = "NO" Then
            CfgShowFlightList.Value = 0
        Else
            CfgShowFlightList.Value = 1
        End If
        If GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "SHOW_INFO", "NO") = "NO" Then
            CfgShowInfo.Value = 0
        Else
            CfgShowInfo.Value = 1
        End If
    
        If GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "START_ON_TOP", "YES") = "YES" Then
            CfgStartOnTop.Value = 1
        Else
            CfgStartOnTop.Value = 0
        End If
        If GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "FONT_BOLD", "NO") = "NO" Then
            CfgFontBold.Value = 0
        Else
            CfgFontBold.Value = 1
        End If
        CfgPrintSize.Text = GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "PRINT_SIZE", CfgPrintSize.Text)
        CfgFontSize.Text = GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "FONT_SIZE", CfgFontSize.Text)
        CfgDateFormat.Text = GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "DATE_FORMAT", CfgDateFormat.Text)
        CfgMainPrior.Text = GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "MAIN_PRIOR", CfgMainPrior.Text)
        CfgMainAhead.Text = GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "MAIN_AHEAD", CfgMainAhead.Text)
    
        CfgTelexTypes.Text = GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "ALL_TELEX_TYPES", AllTelexTypes)
        CfgFolderCaptions.Text = GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "FOLDER_CAPTIONS", CfgTelexTypes.Text)
        CfgFolderTypCodes.Text = GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "FOLDER_TYP_CODES", CfgFolderCaptions.Text)
        CfgRecvTypes.Text = GetIniEntry(myIniFullName, "ONLINE_WINDOWS", "", "RECV_TYPES", AllTelexTypes)
        CfgMainNoShow.Text = GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "MAIN_NOSHOW", CfgMainNoShow.Text)
        CfgFolderSize.Text = GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "FOLDER_SIZE", CfgFolderSize.Text)
        CfgMaxFolderRows.Text = GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "MAX_FOLDER_ROWS", CfgMaxFolderRows.Text)
        If GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "SHOW_FOLDERS", "YES") = "YES" Then
            CfgShowFolders.Value = 1
        Else
            CfgShowFolders.Value = 0
        End If
    
        CfgOnlineNoShow.Text = GetIniEntry(myIniFullName, "ONLINE_WINDOWS", "", "ONLINE_NOSHOW", CfgMainNoShow.Text)
        CfgRecvNoShow.Text = GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "RECV_NOSHOW", CfgOnlineNoShow.Text)
        CfgOnlineMaxLines.Text = GetIniEntry(myIniFullName, "ONLINE_WINDOWS", "", "ONLINE_MAX_LINES", CfgOnlineMaxLines.Text)
        CfgOnlinePrior.Text = GetIniEntry(myIniFullName, "ONLINE_WINDOWS", "", "ONLINE_PRIOR", CfgOnlinePrior.Text)
        CfgOnlineAhead.Text = GetIniEntry(myIniFullName, "ONLINE_WINDOWS", "", "ONLINE_AHEAD", CfgOnlineAhead.Text)
    
        'Telex Text Filter
        CfgAllTypesTextFilter.Text = GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "ALL_TYPES_TEXT_FILTER", CfgAllTypesTextFilter.Text)
        For itm = 0 To 3
            CfgTextFilterType(itm).Text = ""
            CfgTextFilter(itm).Text = ""
        Next
        tmpTypeList = GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "TEXT_FILTER_TYPES", "")
        itm = 1
        tmpType = GetItem(tmpTypeList, itm, ",")
        While tmpType <> "" And itm < 5
            CfgTextFilterType(itm - 1).Text = tmpType
            tmpCode = tmpType & "_TEXT_FILTER"
            tmpFilter = GetIniEntry(myIniFullName, "MAIN_WINDOW", "", tmpCode, "")
            CfgTextFilter(itm - 1).Text = tmpFilter
            itm = itm + 1
            tmpType = GetItem(tmpTypeList, itm, ",")
        Wend
        If GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "IGNORE_EMPTY", "YES") = "YES" Then
            CfgIgnoreEmpty.Value = 1
        Else
            CfgIgnoreEmpty.Value = 0
        End If
    
        CfgLoadTabViewer.Text = GetIniEntry(myIniFullName, "LOATAB_INFO", "", "LOADTAB_VIEWER", CfgLoadTabViewer.Text)
        CfgLoadTabViewer.Text = GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "LOADTAB_VIEWER", CfgLoadTabViewer.Text)
        CfgPaxInfo.Text = GetIniEntry(myIniFullName, "LOATAB_INFO", "", "PAX_INFO", CfgPaxInfo.Text)
        CfgLoaInfo.Text = GetIniEntry(myIniFullName, "LOATAB_INFO", "", "LOA_INFO", CfgLoaInfo.Text)
        CfgUldInfo.Text = GetIniEntry(myIniFullName, "LOATAB_INFO", "", "ULD_INFO", CfgUldInfo.Text)
        CfgImportTool.Text = GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "IMPORT_TOOL", CfgImportTool.Text)
    
        CfgHomeTlxAddr.Text = GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "HOMETLXADDR", CfgHomeTlxAddr.Text)
        CfgWksSitaAddr.Text = GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "WKS_SITA_ADDR", CfgHomeTlxAddr.Text)
        CfgSitaOutType.Text = GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "SITA_OUT_TYPE", CfgSitaOutType.Text)
        CfgSitaOutPrio.Text = GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "SITA_OUT_PRIO", CfgSitaOutPrio.Text)
        CfgAddressNoCheck.Text = GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "ADDRESS_NOCHECK", CfgAddressNoCheck.Text)
        If GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "ADDRESS_FILTER", "NO") = "NO" Then
            CfgAddressFilter.Value = 0
        Else
            CfgAddressFilter.Value = 1
        End If
        If GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "SITA_OUT_ORIGIN", "NO") = "NO" Then
            CfgSitaOutOrigin.Value = 0
        Else
            CfgSitaOutOrigin.Value = 1
        End If
        If GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "SITA_OUT_MSGID", "NO") = "NO" Then
            CfgSitaOutMsgId.Value = 0
        Else
            CfgSitaOutMsgId.Value = 1
        End If
        If GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "SITA_OUT_SMI", "NO") = "NO" Then
            CfgSitaOutSmi.Value = 0
        Else
            CfgSitaOutSmi.Value = 1
        End If
    
        tmpVal = GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "TEST_CASE_MODE", "")
        If tmpVal <> "" Then
            If tmpVal = "NO" Then
                CfgTestCaseMode.Value = 0
            Else
                CfgTestCaseMode.Value = 1
            End If
        Else
            If GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "TESTCASE_MODE", "NO") = "NO" Then
                CfgTestCaseMode.Value = 0
            Else
                CfgTestCaseMode.Value = 1
            End If
        End If
        If GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "TEST_MODE_LOCAL", "NO") = "NO" Then
            CfgTestModeLocal.Value = 0
        Else
            CfgTestModeLocal.Value = 1
        End If
        If GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "TEST_SAVE_LOCAL_DATA", "NO") = "NO" Then
            CfgTestSaveLocalData.Value = 0
        Else
            CfgTestSaveLocalData.Value = 1
        End If
        CfgTestSetServerTime.Text = GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "TEST_SET_SERVER_TIME", CfgTestSetServerTime.Text)
    
        chkRead.Value = 0
    End If
End Sub

Private Sub chkSave_Click()
    Dim RetVal As Integer
    Dim ActResult As String
    Dim ActCmd As String
    Dim ActTable As String
    Dim ActFldLst As String
    Dim ActDatLst As String
    Dim ActCondition As String
    Dim ActOrder As String
    Dim tmpText As String
    Dim tmpOnlineText As String
    Dim tmpLoaTabText As String
    Dim itm As Integer
    Dim tmpTypeList As String
    Dim tmpConfig As String
    Dim tmpItem As String
    
    If chkSave.Value = 1 Then
        Screen.MousePointer = 11
        ActResult = ""
        ActCmd = "DRT"
        ActTable = "VCDTAB"
        ActFldLst = ""
        ActDatLst = ""
        ActCondition = "WHERE APPN = 'TelexPool' AND CKEY = 'Config' AND PKNO = 'andreas'"
        ActOrder = ""
        RetVal = UfisServer.CallCeda(ActResult, ActCmd, ActTable, ActFldLst, ActDatLst, ActCondition, ActOrder, 0, True, False)
        
        ActCmd = "IRT"
        ActFldLst = "URNO,HOPO,APPN,CKEY,PKNO,CTYP,TEXT"
        ActCondition = ""
        tmpText = "MAIN_CAPTION = " & CfgMainCaption.Text & vbLf & _
                  "RECV_CAPTION = " & CfgRecvCaption.Text & vbLf & _
                  "RECV_LIST_CAPTION = " & CfgRecvListCaption.Text & vbLf & _
                  "SENT_LIST_CAPTION = " & CfgSentListCaption.Text & vbLf
        
        If CfgShowIntro.Value = 1 Then
            tmpText = tmpText & "SHOW_INTRO = YES" & vbLf
        Else
            tmpText = tmpText & "SHOW_INTRO = NO" & vbLf
        End If
        If CfgShowMain.Value = 1 Then
            tmpText = tmpText & "SHOW_MAIN = YES" & vbLf
        Else
            tmpText = tmpText & "SHOW_MAIN = NO" & vbLf
        End If
        If CfgShowRecv.Value = 1 Then
            tmpText = tmpText & "SHOW_RECV = YES" & vbLf
        Else
            tmpText = tmpText & "SHOW_RECV = NO" & vbLf
        End If
        If CfgShowSend.Value = 1 Then
            tmpText = tmpText & "SHOW_SEND = YES" & vbLf
        Else
            tmpText = tmpText & "SHOW_SEND = NO" & vbLf
        End If
        If CfgShowSent.Value = 1 Then
            tmpText = tmpText & "SHOW_SENT = YES" & vbLf
        Else
            tmpText = tmpText & "SHOW_SENT = NO" & vbLf
        End If
        If CfgShowSentList.Value = 1 Then
            tmpText = tmpText & "SHOW_SENT_LIST = YES" & vbLf
        Else
            tmpText = tmpText & "SHOW_SENT_LIST = NO" & vbLf
        End If
        If CfgShowFlightList.Value = 1 Then
            tmpText = tmpText & "SHOW_FLIGHT_LIST = YES" & vbLf
        Else
            tmpText = tmpText & "SHOW_FLIGHT_LIST = NO" & vbLf
        End If
        If CfgShowInfo.Value = 1 Then
            tmpText = tmpText & "SHOW_INFO = YES" & vbLf
        Else
            tmpText = tmpText & "SHOW_INFO = NO" & vbLf
        End If

        If CfgStartOnTop.Value = 1 Then
            tmpText = tmpText & "START_ON_TOP = YES" & vbLf
        Else
            tmpText = tmpText & "START_ON_TOP = NO" & vbLf
        End If
        If CfgFontBold.Value = 1 Then
            tmpText = tmpText & "FONT_BOLD = YES" & vbLf
        Else
            tmpText = tmpText & "FONT_BOLD = NO" & vbLf
        End If
        tmpText = tmpText & "PRINT_SIZE = " & CfgPrintSize.Text & vbLf & _
                            "FONT_SIZE = " & CfgFontSize.Text & vbLf & _
                            "DATE_FORMAT = " & CfgDateFormat.Text & vbLf & _
                            "MAIN_PRIOR = " & CfgMainPrior.Text & vbLf & _
                            "MAIN_AHEAD = " & CfgMainAhead.Text & vbLf
        
        tmpText = tmpText & "ALL_TELEX_TYPES = " & CfgTelexTypes.Text & vbLf & _
                            "FOLDER_CAPTIONS = " & CfgFolderCaptions.Text & vbLf & _
                            "FOLDER_TYP_CODES = " & CfgFolderTypCodes.Text & vbLf & _
                            "MAIN_NOSHOW = " & CfgMainNoShow.Text & vbLf & _
                            "FOLDER_SIZE = " & CfgFolderSize.Text & vbLf & _
                            "MAX_FOLDER_ROWS = " & CfgMaxFolderRows.Text & vbLf
        If CfgShowFolders.Value = 1 Then
            tmpText = tmpText & "SHOW_FOLDERS = YES" & vbLf
        Else
            tmpText = tmpText & "SHOW_FOLDERS = NO" & vbLf
        End If
        tmpOnlineText = "RECV_TYPES = " & CfgRecvTypes.Text & vbLf
        
        tmpText = tmpText & "RECV_NOSHOW = " & CfgTelexTypes.Text & vbLf
        tmpOnlineText = tmpOnlineText & "ONLINE_NOSHOW = " & CfgOnlineNoShow.Text & vbLf & _
                                        "ONLINE_MAX_LINES = " & CfgOnlineMaxLines.Text & vbLf & _
                                        "ONLINE_PRIOR = " & CfgOnlinePrior.Text & vbLf & _
                                        "ONLINE_AHEAD = " & CfgOnlineAhead.Text & vbLf
    
        'Telex Text Filter
        tmpText = tmpText & "ALL_TYPES_TEXT_FILTER = " & CfgAllTypesTextFilter.Text & vbLf
        tmpTypeList = ""
        tmpConfig = ""
        For itm = 0 To 3
            tmpItem = CfgTextFilterType(itm)
            If Len(tmpItem) > 0 Then
                If Len(tmpTypeList) > 0 Then
                    tmpTypeList = tmpTypeList & ","
                End If
                tmpTypeList = tmpTypeList & tmpItem
                tmpConfig = tmpConfig & tmpItem & "_TEXT_FILTER = " & CfgTextFilter(itm).Text & vbLf
            End If
        Next
        tmpText = tmpText & "TEXT_FILTER_TYPES = " & tmpTypeList & vbLf
        If Len(tmpConfig) > 0 Then
            tmpText = tmpText & tmpConfig
        End If
        If CfgIgnoreEmpty.Value = 1 Then
            tmpText = tmpText & "IGNORE_EMPTY = YES" & vbLf
        Else
            tmpText = tmpText & "IGNORE_EMPTY = NO" & vbLf
        End If

        tmpText = tmpText & "IMPORT_TOOL = " & CfgImportTool.Text & vbLf
        tmpLoaTabText = "LOADTAB_VIEWER = " & CfgLoadTabViewer.Text & vbLf & _
                        "PAX_INFO = " & CfgPaxInfo.Text & vbLf & _
                        "LOA_INFO = " & CfgLoaInfo.Text & vbLf & _
                        "ULD_INFO = " & CfgUldInfo.Text & vbLf

        tmpText = tmpText & "HOMETLXADDR = " & CfgHomeTlxAddr.Text & vbLf & _
                            "WKS_SITA_ADDR = " & CfgWksSitaAddr.Text & vbLf & _
                            "SITA_OUT_TYPE = " & CfgSitaOutType.Text & vbLf & _
                            "SITA_OUT_PRIO = " & CfgSitaOutPrio.Text & vbLf & _
                            "ADDRESS_NOCHECK = " & CfgAddressNoCheck.Text & vbLf
        If CfgAddressFilter.Value = 1 Then
            tmpText = tmpText & "ADDRESS_FILTER = YES" & vbLf
        Else
            tmpText = tmpText & "ADDRESS_FILTER = NO" & vbLf
        End If
        If CfgSitaOutOrigin.Value = 1 Then
            tmpText = tmpText & "SITA_OUT_ORIGIN = YES" & vbLf
        Else
            tmpText = tmpText & "SITA_OUT_ORIGIN = NO" & vbLf
        End If
        If CfgSitaOutMsgId.Value = 1 Then
            tmpText = tmpText & "SITA_OUT_MSGID = YES" & vbLf
        Else
            tmpText = tmpText & "SITA_OUT_MSGID = NO" & vbLf
        End If
        If CfgSitaOutSmi.Value = 1 Then
            tmpText = tmpText & "SITA_OUT_SMI = YES" & vbLf
        Else
            tmpText = tmpText & "SITA_OUT_SMI = NO" & vbLf
        End If

        If CfgTestCaseMode.Value = 1 Then
            tmpText = tmpText & "TEST_CASE_MODE = YES" & vbLf
        Else
            tmpText = tmpText & "TEST_CASE_MODE = NO" & vbLf
        End If
        If CfgTestModeLocal.Value = 1 Then
            tmpText = tmpText & "TEST_MODE_LOCAL = YES" & vbLf
        Else
            tmpText = tmpText & "TEST_MODE_LOCAL = NO" & vbLf
        End If
        If CfgTestSaveLocalData.Value = 1 Then
            tmpText = tmpText & "TEST_SAVE_LOCAL_DATA = YES" & vbLf
        Else
            tmpText = tmpText & "TEST_SAVE_LOCAL_DATA = NO" & vbLf
        End If
        tmpText = tmpText & "TEST_SET_SERVER_TIME = " & CfgTestSetServerTime.Text & vbLf

        tmpText = CleanString(tmpText, FOR_SERVER, False)
        ActDatLst = UfisServer.UrnoPoolGetNext & "," & _
                    UfisServer.HOPO & "," & _
                    "TelexPool,Config,andreas," & _
                    "MAIN_WINDOW," & _
                    tmpText
        RetVal = UfisServer.CallCeda(ActResult, ActCmd, ActTable, ActFldLst, ActDatLst, ActCondition, ActOrder, 0, True, False)
        tmpOnlineText = CleanString(tmpOnlineText, FOR_SERVER, False)
        ActDatLst = UfisServer.UrnoPoolGetNext & "," & _
                    UfisServer.HOPO & "," & _
                    "TelexPool,Config,andreas," & _
                    "ONLINE_WINDOWS," & _
                    tmpOnlineText
        RetVal = UfisServer.CallCeda(ActResult, ActCmd, ActTable, ActFldLst, ActDatLst, ActCondition, ActOrder, 0, True, False)
        tmpLoaTabText = CleanString(tmpLoaTabText, FOR_SERVER, False)
        ActDatLst = UfisServer.UrnoPoolGetNext & "," & _
                    UfisServer.HOPO & "," & _
                    "TelexPool,Config,andreas," & _
                    "LOATAB_INFO," & _
                    tmpLoaTabText
        RetVal = UfisServer.CallCeda(ActResult, ActCmd, ActTable, ActFldLst, ActDatLst, ActCondition, ActOrder, 0, True, False)
        Screen.MousePointer = 0
        chkSave.Value = 0
    End If
End Sub

Private Sub chkLoad_Click()
    Dim RetVal As Integer
    Dim ActResult As String
    Dim ActCmd As String
    Dim ActTable As String
    Dim ActFldLst As String
    Dim ActDatLst As String
    Dim ActCondition As String
    Dim ActOrder As String
    Dim ConfigFile As String
    Dim CurDataFile As String

    If chkLoad.Value = 1 Then
        HiddenMain.SetStatusText 2, UCase("CONFIG TOOL READING FROM VCDTAB")
        Screen.MousePointer = 11
        ActResult = ""
        ActCmd = "RT"
        ActTable = "VCDTAB"
        ActFldLst = "TEXT"
        ActDatLst = ""
        ActCondition = "WHERE CTYP = 'MAIN_WINDOW' AND APPN = 'TelexPool' AND CKEY = 'Config' AND PKNO = 'andreas'"
        ActOrder = ""
        RetVal = UfisServer.CallCeda(ActResult, ActCmd, ActTable, ActFldLst, ActDatLst, ActCondition, ActOrder, 0, True, False)
        ActResult = CleanString(ActResult, FOR_CLIENT, False)
        ConfigFile = "[MAIN_WINDOW]" & vbCr & vbLf & ActResult
        
        ActCondition = "WHERE CTYP = 'ONLINE_WINDOWS' AND APPN = 'TelexPool' AND CKEY = 'Config' AND PKNO = 'andreas'"
        RetVal = UfisServer.CallCeda(ActResult, ActCmd, ActTable, ActFldLst, ActDatLst, ActCondition, ActOrder, 0, True, False)
        ActResult = CleanString(ActResult, FOR_CLIENT, False)
        ConfigFile = ConfigFile & vbCr & vbLf & "[ONLINE_WINDOWS]" & vbCr & vbLf & ActResult
        
        ActCondition = "WHERE CTYP = 'LOATAB_INFO' AND APPN = 'TelexPool' AND CKEY = 'Config' AND PKNO = 'andreas'"
        RetVal = UfisServer.CallCeda(ActResult, ActCmd, ActTable, ActFldLst, ActDatLst, ActCondition, ActOrder, 0, True, False)
        ActResult = CleanString(ActResult, FOR_CLIENT, False)
        ConfigFile = ConfigFile & vbCr & vbLf & "[LOATAB_INFO]" & vbCr & vbLf & ActResult
        
        'CurDataFile = "c:\tmp\TelexPool.txt"
        CurDataFile = UFIS_TMP & "\TelexPool.txt"
        Open CurDataFile For Output As #1
        Print #1, ConfigFile
        Close 1
        Screen.MousePointer = 0
        HiddenMain.SetStatusText 2, UCase("")
        
        chkLoad.Value = 0
    End If
End Sub

