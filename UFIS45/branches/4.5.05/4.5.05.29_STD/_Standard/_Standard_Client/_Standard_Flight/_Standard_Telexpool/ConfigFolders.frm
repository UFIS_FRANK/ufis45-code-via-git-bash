VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "TAB.ocx"
Begin VB.Form ConfigFolders 
   Caption         =   "Configuration of Folders"
   ClientHeight    =   7725
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   10650
   Icon            =   "ConfigFolders.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   7725
   ScaleWidth      =   10650
   Begin VB.Frame Frame2 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1515
      Left            =   2190
      TabIndex        =   23
      Top             =   3720
      Width           =   3165
   End
   Begin VB.Frame fraTlxTypes 
      Caption         =   "SITA"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2475
      Left            =   5430
      TabIndex        =   21
      Top             =   420
      Width           =   1035
      Begin TABLib.TAB TlxType 
         Height          =   1185
         Index           =   1
         Left            =   90
         TabIndex        =   22
         Top             =   240
         Width           =   855
         _Version        =   65536
         _ExtentX        =   1508
         _ExtentY        =   2090
         _StockProps     =   64
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Telex Folder Config"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1515
      Left            =   60
      TabIndex        =   15
      Top             =   3720
      Width           =   2055
      Begin VB.CheckBox chkWork 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   10
         Left            =   90
         Style           =   1  'Graphical
         TabIndex        =   20
         Top             =   1140
         Width           =   855
      End
      Begin VB.CheckBox chkWork 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   9
         Left            =   90
         Style           =   1  'Graphical
         TabIndex        =   19
         Top             =   840
         Width           =   855
      End
      Begin VB.CheckBox chkWork 
         Caption         =   "Hold-File"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   8
         Left            =   90
         Style           =   1  'Graphical
         TabIndex        =   18
         Top             =   540
         Width           =   855
      End
      Begin VB.CheckBox chkWork 
         Caption         =   "System"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   7
         Left            =   90
         Style           =   1  'Graphical
         TabIndex        =   17
         Top             =   240
         Width           =   855
      End
      Begin TABLib.TAB TlxType 
         Height          =   1185
         Index           =   0
         Left            =   1080
         TabIndex        =   16
         Top             =   240
         Width           =   855
         _Version        =   65536
         _ExtentX        =   1508
         _ExtentY        =   2090
         _StockProps     =   64
      End
   End
   Begin VB.Frame fraUserGroups 
      Caption         =   "Telex Pool User Groups"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2475
      Left            =   6540
      TabIndex        =   13
      Top             =   420
      Width           =   2745
      Begin TABLib.TAB AllGroups 
         Height          =   2025
         Left            =   90
         TabIndex        =   14
         Top             =   240
         Width           =   2385
         _Version        =   65536
         _ExtentX        =   4207
         _ExtentY        =   3572
         _StockProps     =   64
      End
   End
   Begin VB.Frame fraAssignedGroups 
      Caption         =   "Assigned User Groups"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1215
      Left            =   60
      TabIndex        =   10
      Top             =   5340
      Width           =   5295
      Begin TABLib.TAB GroupMembers 
         Height          =   765
         Index           =   0
         Left            =   90
         TabIndex        =   11
         Top             =   240
         Width           =   2535
         _Version        =   65536
         _ExtentX        =   4471
         _ExtentY        =   1349
         _StockProps     =   64
      End
      Begin TABLib.TAB GroupMembers 
         Height          =   765
         Index           =   1
         Left            =   2670
         TabIndex        =   12
         Top             =   240
         Width           =   2535
         _Version        =   65536
         _ExtentX        =   4471
         _ExtentY        =   1349
         _StockProps     =   64
      End
   End
   Begin VB.Frame fraTlxFolders 
      Caption         =   "Telex Pool Folders"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3195
      Left            =   60
      TabIndex        =   8
      Top             =   420
      Width           =   5295
      Begin TABLib.TAB TlxFolders 
         Height          =   2865
         Left            =   90
         TabIndex        =   9
         Top             =   240
         Width           =   5115
         _Version        =   65536
         _ExtentX        =   9022
         _ExtentY        =   5054
         _StockProps     =   64
      End
   End
   Begin VB.CheckBox chkWork 
      Caption         =   "Test"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   6
      Left            =   4380
      Style           =   1  'Graphical
      TabIndex        =   7
      Top             =   30
      Width           =   855
   End
   Begin VB.CheckBox chkWork 
      Caption         =   "Delete"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   5
      Left            =   3510
      Style           =   1  'Graphical
      TabIndex        =   6
      Top             =   30
      Width           =   855
   End
   Begin VB.CheckBox chkWork 
      Caption         =   "Update"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   4
      Left            =   2640
      Style           =   1  'Graphical
      TabIndex        =   5
      Top             =   30
      Width           =   855
   End
   Begin VB.CheckBox chkWork 
      Caption         =   "New"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   3
      Left            =   1770
      Style           =   1  'Graphical
      TabIndex        =   4
      Top             =   30
      Width           =   855
   End
   Begin VB.CheckBox chkWork 
      Caption         =   "Save"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   2
      Left            =   900
      Style           =   1  'Graphical
      TabIndex        =   3
      Top             =   30
      Width           =   855
   End
   Begin VB.CheckBox chkWork 
      Caption         =   "Refresh"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   1
      Left            =   30
      Style           =   1  'Graphical
      TabIndex        =   2
      Top             =   30
      Width           =   855
   End
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   1
      Top             =   7440
      Width           =   10650
      _ExtentX        =   18785
      _ExtentY        =   503
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   18256
         EndProperty
      EndProperty
   End
   Begin VB.CheckBox chkWork 
      Caption         =   "&Close"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   0
      Left            =   5250
      Style           =   1  'Graphical
      TabIndex        =   0
      Top             =   30
      Width           =   855
   End
End
Attribute VB_Name = "ConfigFolders"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub chkWork_Click(Index As Integer)
    If chkWork(Index).Value = 1 Then
        chkWork(Index).BackColor = LightGreen
        Select Case Index
            Case 0  'Close
                'Me.Hide
                'TelexPoolHead.chkAssFile.Value = 0
                chkWork(Index).Value = 0
            Case 1  'Refresh
                'LoadUsers
            Case 7  'System
                chkWork(8).Value = 0
            Case 8  'HoldFile
                chkWork(7).Value = 0
            Case Else
                chkWork(Index).Value = 0
        End Select
    Else
        chkWork(Index).BackColor = vbButtonFace
        Select Case Index
            Case 7  'System
                chkWork(8).Value = 1
            Case 8  'HoldFile
                chkWork(7).Value = 1
            Case Else
        End Select
    End If
End Sub

Private Sub Form_Load()
    InitMyForm
End Sub

Private Sub InitMyForm()
    Dim i As Integer
    StatusBar1.ZOrder
    AllGroups.ResetContent
    AllGroups.LogicalFieldList = "STAT,USID,NAME,REMA,URNO"
    AllGroups.FontName = "Courier New"
    AllGroups.SetTabFontBold True
    AllGroups.HeaderFontSize = 17
    AllGroups.FontSize = 17
    AllGroups.LineHeight = 17
    AllGroups.HeaderString = "S,Grp,Full Name,Remark,URNO"
    AllGroups.HeaderLengthString = "10,10,10,10,10"
    AllGroups.SetMainHeaderValues "5", "Group Definitions", ""
    AllGroups.SetMainHeaderFont 17, False, False, True, 0, "Courier New"
    AllGroups.SetColumnBoolProperty 0, "1", "0"
    AllGroups.SetBoolPropertyReadOnly 0, True
    AllGroups.ShowHorzScroller True
    AllGroups.ShowVertScroller True
    AllGroups.LifeStyle = True
    AllGroups.CursorLifeStyle = True
    AllGroups.MainHeader = True
    AllGroups.AutoSizeByHeader = True
    AllGroups.InsertTextLine "1,TM1,12345678901234567890123456789012,123456789012,123456789", False
    AllGroups.InsertTextLine "1,TM2,12345678901234567890123456789012,123456789012,123456789", False
    AllGroups.InsertTextLine "1,TM3,12345678901234567890123456789012,123456789012,123456789", False
    AllGroups.InsertTextLine "0,TM4,12345678901234567890123456789012,123456789012,123456789", False
    AllGroups.InsertTextLine "1,TM5,12345678901234567890123456789012,123456789012,123456789", False
    AllGroups.AutoSizeColumns

    TlxFolders.ResetContent
    TlxFolders.LogicalFieldList = "STAT,USID,NAME,REMA,URNO"
    TlxFolders.FontName = "Courier New"
    TlxFolders.SetTabFontBold True
    TlxFolders.HeaderFontSize = 17
    TlxFolders.FontSize = 17
    TlxFolders.LineHeight = 17
    TlxFolders.HeaderString = "S,T,TAB,Description,Remark,URNO"
    TlxFolders.HeaderLengthString = "10,10,10,10,10,10"
    TlxFolders.SetMainHeaderValues "6", "Configured Telex Folders", ""
    TlxFolders.SetMainHeaderFont 17, False, False, True, 0, "Courier New"
    TlxFolders.SetColumnBoolProperty 0, "1", "0"
    TlxFolders.ShowHorzScroller True
    TlxFolders.ShowVertScroller True
    TlxFolders.LifeStyle = True
    TlxFolders.CursorLifeStyle = True
    TlxFolders.MainHeader = True
    TlxFolders.AutoSizeByHeader = True
    TlxFolders.InsertTextLine "1,H,ABC,12345678901234567890123456789012,123456789012,123456789", False
    TlxFolders.InsertTextLine "1,H,MVT,12345678901234567890123456789012,123456789012,123456789", False
    TlxFolders.InsertTextLine "1,H,OTH,12345678901234567890123456789012,123456789012,123456789", False
    TlxFolders.InsertTextLine "0,H,PAX,12345678901234567890123456789012,123456789012,123456789", False
    TlxFolders.InsertTextLine "1,H,PFA,12345678901234567890123456789012,123456789012,123456789", False
    TlxFolders.InsertTextLine "1,S,ATC,12345678901234567890123456789012,123456789012,123456789", False
    TlxFolders.AutoSizeColumns
    
    For i = 0 To 1
        GroupMembers(i).ResetContent
        GroupMembers(i).LogicalFieldList = "STAT,USID,NAME,REMA,URNO"
        GroupMembers(i).FontName = "Courier New"
        GroupMembers(i).SetTabFontBold True
        GroupMembers(i).HeaderFontSize = 17
        GroupMembers(i).FontSize = 17
        GroupMembers(i).LineHeight = 17
        GroupMembers(i).HeaderString = "S,Grp,Full Name,Remark,URNO"
        GroupMembers(i).HeaderLengthString = "10,10,10,10,10"
        GroupMembers(i).SetColumnBoolProperty 0, "1", "0"
        GroupMembers(i).ShowHorzScroller True
        GroupMembers(i).ShowVertScroller True
        GroupMembers(i).LifeStyle = True
        GroupMembers(i).CursorLifeStyle = True
    Next
    GroupMembers(0).SetMainHeaderValues "5", "Available Groups", ""
    GroupMembers(0).SetMainHeaderFont 17, False, False, True, 0, "Courier New"
    GroupMembers(0).MainHeader = True
    GroupMembers(0).AutoSizeByHeader = True
    GroupMembers(1).SetMainHeaderValues "5", "Selected Groups", ""
    GroupMembers(1).SetMainHeaderFont 17, False, False, True, 0, "Courier New"
    GroupMembers(1).MainHeader = True
    GroupMembers(1).AutoSizeByHeader = True
    GroupMembers(0).InsertTextLine "1,TM1,12345678901234567890123456789012,123456789012,123456789", False
    GroupMembers(0).InsertTextLine "1,TM5,12345678901234567890123456789012,123456789012,123456789", False
    GroupMembers(1).InsertTextLine "1,TM2,12345678901234567890123456789012,123456789012,123456789", False
    GroupMembers(1).InsertTextLine "1,TM3,12345678901234567890123456789012,123456789012,123456789", False
    GroupMembers(1).InsertTextLine "1,TM4,12345678901234567890123456789012,123456789012,123456789", False
    GroupMembers(0).AutoSizeColumns
    GroupMembers(1).AutoSizeColumns

    For i = 0 To 1
        TlxType(i).ResetContent
        TlxType(i).LogicalFieldList = "STAT,USID,NAME,REMA,URNO"
        TlxType(i).FontName = "Courier New"
        TlxType(i).SetTabFontBold True
        TlxType(i).HeaderFontSize = 17
        TlxType(i).FontSize = 17
        TlxType(i).LineHeight = 17
        TlxType(i).HeaderString = "TYPE,         Full Name,Remark"
        TlxType(i).HeaderLengthString = "10,10,10,10"
        TlxType(i).ShowVertScroller True
        TlxType(i).LifeStyle = True
        TlxType(i).CursorLifeStyle = True
    Next
    TlxType(0).AutoSizeByHeader = True
    TlxType(0).InsertTextLine "SAM,,,,,", False
    TlxType(0).InsertTextLine "SRM,,,,,", False
    TlxType(0).InsertTextLine "SLC,,,,,", False
    TlxType(0).AutoSizeColumns
    
    TlxType(1).SetMainHeaderValues "5", "", ""
    TlxType(1).SetMainHeaderFont 17, False, False, True, 0, "Courier New"
    TlxType(1).MainHeader = True
    
    TlxType(1).ShowHorzScroller True
    TlxType(1).AutoSizeByHeader = True
    TlxType(1).InsertTextLine "MVT,,,,,", False
    TlxType(1).InsertTextLine "LDM,,,,,", False
    TlxType(1).InsertTextLine "CPM,,,,,", False
    TlxType(1).InsertTextLine "PTM,,,,,", False
    TlxType(1).InsertTextLine "DLS,,,,,", False
    TlxType(1).InsertTextLine "SAM,,,,,", False
    TlxType(1).InsertTextLine "SRM,,,,,", False
    TlxType(1).InsertTextLine "SLC,,,,,", False
    TlxType(1).AutoSizeColumns

End Sub
Private Sub Form_Resize()
    Dim NewSize As Long
    NewSize = Me.ScaleWidth - fraUserGroups.Left - fraTlxFolders.Left
    If NewSize > 600 Then
        fraUserGroups.Width = NewSize
        NewSize = NewSize - 180
        AllGroups.Width = NewSize
    End If
    NewSize = Me.ScaleHeight - StatusBar1.Height - fraAssignedGroups.Top - 60
    If NewSize > 600 Then
        fraAssignedGroups.Height = NewSize
    End If
    NewSize = NewSize - GroupMembers(0).Top - 90
    If NewSize > 600 Then
        GroupMembers(0).Height = NewSize
        GroupMembers(1).Height = NewSize
    End If
    NewSize = Me.ScaleHeight - StatusBar1.Height - fraUserGroups.Top - 60
    If NewSize > 600 Then
        fraUserGroups.Height = NewSize
        fraTlxTypes.Height = NewSize
    End If
    NewSize = NewSize - AllGroups.Top - 90
    If NewSize > 600 Then
        AllGroups.Height = NewSize
        TlxType(1).Height = NewSize
    End If
End Sub
