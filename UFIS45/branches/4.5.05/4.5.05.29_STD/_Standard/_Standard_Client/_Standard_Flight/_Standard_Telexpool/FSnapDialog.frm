VERSION 5.00
Begin VB.Form FSnapDialog 
   Caption         =   "Snap to Monitor Edge Demo"
   ClientHeight    =   1890
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   4680
   LinkTopic       =   "Form1"
   ScaleHeight     =   1890
   ScaleWidth      =   4680
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton Command1 
      Cancel          =   -1  'True
      Caption         =   "Close"
      Default         =   -1  'True
      Height          =   375
      Left            =   1860
      TabIndex        =   1
      Top             =   1320
      Width           =   1095
   End
   Begin VB.Label Label1 
      Caption         =   "Label1"
      Height          =   975
      Left            =   300
      TabIndex        =   0
      Top             =   240
      Width           =   4155
   End
End
Attribute VB_Name = "FSnapDialog"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private m_Snap As CSnapDialog

Private Sub Command1_Click()
   Unload Me
End Sub

Private Sub Form_Load()
   ' Setting up is a snap!
   Set m_Snap = New CSnapDialog
   m_Snap.hWnd = Me.hWnd
   
   ' Tell user what to do.
   Label1.Caption = "Drag this form around on your monitor(s), " & _
      "bringing it close to the edges.  Note that when you get " & _
      "within 15 pixels (configurable in code), the form 'snaps' " & _
      "to the edge(s)."
End Sub



