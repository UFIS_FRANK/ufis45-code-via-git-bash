#if !defined(AFX_BLTDIAGRAM_H__DAC99481_8801_11D1_B43F_0000B45A33F5__INCLUDED_)
#define AFX_STAFFDIAGRAM_H__DAC99481_8801_11D1_B43F_0000B45A33F5__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// BltDiagram.h : header file
//
 
#include <BltDiaViewer.h>
#include <CCSClientWnd.h>
#include <CCS3dStatic.h>
#include <CCSButtonCtrl.h>
#include <CCSTimeScale.h>
#include <CViewer.h>
#include <BltChart.h>

/////////////////////////////////////////////////////////////////////////////
// BltDiagram frame

enum 
{
	BLT_ZEITRAUM_PAGE,
	BLT_FLUGSUCHEN_PAGE,
	BLT_GEOMETRIE_PAGE
};

class BltDiagram : public CFrameWnd
{
	DECLARE_DYNCREATE(BltDiagram)
public:
	BltDiagram();           // protected constructor used by dynamic creation
	~BltDiagram();

	CDialogBar omDialogBar;

	void GetCurView( char * pcpView);
	void ViewSelChange(char *pcpView);

	void ActivateTimer();
	void DeActivateTimer();

    void PositionChild();
    void SetTSStartTime(CTime opTSStartTime);
	void UpdateComboBox();
	void ChangeViewTo(const char *pcpViewName,bool RememberPositions = true);
	
	void PrePlanMode(BOOL bpToSet,CTime opPrePlanTime);
	void ToggleOnline(void);
    void OnFirstChart();
    void OnLastChart();
	bool ShowFlight(long lpUrno);
	void UpdateWoResButton();
    void UpdateKonflikteDlg();
	void UpdateChangesButton(CString& opText, COLORREF opColor);

	bool GetLoadedFlights(CCSPtrArray<DIAFLIGHTDATA> &opFlights);
	void SaveToReg();
	void ShowTime(const CTime &ropTime);
	void SetFocusToDiagram();  //PRF 8363 

	// TLE: UFIS 3768 ========================
	// Disable the button in CDialogBar will 
	// do by ON_UPDATE_COMMAND_UI message.

	void OnDisableViewButton(CCmdUI *pCmdUI);
	void OnDisableNowButton(CCmdUI *pCmdUI);
	void OnDisableSearchButton(CCmdUI *pCmdUI);
	void OnDisableFWRButton(CCmdUI *pCmdUI);
	void OnDisableChangeButton(CCmdUI *pCmdUI);
	void OnDisableAllocationButton(CCmdUI *pCmdUI);
	void OnDisableOfflineButton(CCmdUI *pCmdUI);
	void OnDisableExpandButton(CCmdUI *pCmdUI);
	void OnDisableAttentionButton(CCmdUI *pCmdUI);
	void OnDisableOverviewButton(CCmdUI *pCmdUI);
	void OnDisableCheckInButton(CCmdUI *pCmdUI);
	void OnDisablePrintButton(CCmdUI *pCmdUI);
	void OnDisableCloseButton(CCmdUI *pCmdUI);

	// =======================================

// Attributes
public:

	BltDiagramViewer omViewer;

	CString omOldWhere;
	bool LoadFlights(const char *pspView = NULL);

    CCS3DStatic omTime;
    CCS3DStatic omDate;
    CCS3DStatic omTSDate;

    CButton m_KlebeFkt;
    CCSTimeScale omTimeScale;
    CBitmapButton omBB1, omBB2;
    
    CCSClientWnd omClientWnd;
    CStatusBar omStatusBar;
    
    CTime omStartTime;
    CTimeSpan omDuration;
    CTime omTSStartTime;
    CTimeSpan omTSDuration;
    CTimeSpan omTSInterval;
    
	// Grouping
    // BltChart *pomChart;
	CPtrArray omChartArray;

    int imFirstVisibleChart;
    int imStartTimeScalePos;
    
	CString omCaptionText;
	bool bmIsViewOpen;
	bool bmOnSize;

public:
	CCSButtonCtrl m_CB_BltAttention;
	CCSButtonCtrl m_CB_Offline;
	CCSButtonCtrl m_CB_WoRes;
	CCSButtonCtrl m_CB_Changes;


// Redisplay all methods for DDX call back function
public:
	BOOL bmNoUpdatesNow;
	

	bool bmRepaintAll;

	void RedisplayAll();
// Time Band data, use the TIMENULL value for hiding the marker
public:
	void SetTimeBand(CTime opStartTime, CTime opEndTime);
	void UpdateTimeBand();
 	void SetMarkTime(CTime opStartTime, CTime opEndTime);




private:

	bool UpdateDia();
	void ResetStateVariables();


	CTime omTimeBandStartTime;
	CTime omTimeBandEndTime;

    CPoint omMaxTrackSize;
    CPoint omMinTrackSize;
    COLORREF lmBkColor;
    COLORREF lmTextColor;
    COLORREF lmHilightColor;


	BltOverviewTableDlg *pomBltOverviewTableDlg;
	BltKonflikteDlg *pomBltKonflikteDlg;
	BltCcaTableDlg *pomBltCcaTableDlg;
	CString m_key;

	// Grouping
void OnUpdatePrevNext(void);
void SetPosAreaButtonColor(int ipGroupno);
void SetAllAreaButtonsColor(void);
CListBox *GetBottomMostGantt();


// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(BltDiagram)
	virtual BOOL DestroyWindow();
	//}}AFX_VIRTUAL

// Implementation

	// Generated message map functions
	//{{AFX_MSG(BltDiagram)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnWoRes();
	afx_msg void OnOverview();
	afx_msg void OnCca();
	afx_msg void OnAttention();
	afx_msg void OnOffline();
	afx_msg void OnBeenden();
	afx_msg void OnDestroy();
	afx_msg void OnAnsicht();
	afx_msg void OnInsert();
	afx_msg void OnClose();
    afx_msg void OnGetMinMaxInfo(MINMAXINFO FAR* lpMMI);
    afx_msg void OnPaint();
    afx_msg BOOL OnEraseBkgnd(CDC* pDC);
    afx_msg void OnSize(UINT nType, int cx, int cy);
    afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
    afx_msg void OnNextChart();
    afx_msg void OnPrevChart();
    afx_msg void OnTimer(UINT nIDEvent);
    afx_msg LONG OnPositionChild(WPARAM wParam, LPARAM lParam);
    afx_msg LONG OnUpdateDiagram(WPARAM wParam, LPARAM lParam);
	afx_msg void OnViewSelChange();
	afx_msg void OnCloseupView();
	afx_msg void OnZeit();
	afx_msg void OnSearch();
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg LONG RepaintAll(WPARAM wParam, LPARAM lParam);
	afx_msg void OnActivate(UINT nState, CWnd* pWndOther, BOOL bMinimized);
	afx_msg void OnExpand();
	afx_msg void OnPrint();
	afx_msg void OnChanges();
	afx_msg void OnAllocate();
	afx_msg void OnUndo();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.


#endif // !defined(AFX_STAFFDIAGRAM_H__DAC99481_8801_11D1_B43F_0000B45A33F5__INCLUDED_)
