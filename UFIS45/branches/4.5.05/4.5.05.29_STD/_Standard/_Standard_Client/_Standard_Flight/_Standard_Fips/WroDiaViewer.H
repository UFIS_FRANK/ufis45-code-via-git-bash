// stviewer.h : header file
//

#ifndef _WRODIAVIEWER_H_
#define _WRODIAVIEWER_H_

#include <cviewer.h>
#include <CCSPrint.h>
#include <DiaCedaFlightData.h>
#include <CedaBasicData.h>
#include <Utils.h>

struct WRODIA_INDICATORDATA
{
	// standard data for indicators in general GanttBar
	CTime StartTime; 
	CTime EndTime;
	COLORREF Color;
};
struct WRODIA_BARDATA 
{
	long Urno;
	long AUrno;
	long DUrno;
	long ARkey;
	long DRkey;
	int WroNo;
    CString Text;
    CTime StartTime;
    CTime EndTime;
	CTime GatBegin;
	CTime GatEnd;
	CTime WroBegin;
	CTime WroEnd;
	CTime BltBegin;
	CTime BltEnd;
	CTime Tmo;
	CTime Land;
	CTime Onbl;
	CTime Ofbl;
	CTime Slot;
	CTime Airb;

	CString Belt;
	CString Gate;
	CString Wro;
	CString StatusText;
    int FrameType;
    int MarkerType;
    CBrush *MarkerBrush;
	CBrush *TriangleLeft;
	CBrush *TriangleRight;
	COLORREF TextColor;
	COLORREF TriangelColorRight;
	COLORREF TriangelColorLeft;
    int OverlapLevel;       // zero for the top-most level, each level lowered by 4 pixels
	CCSPtrArray<WRODIA_INDICATORDATA> Indicators;

	CString SpecialREQ;
	int SpecialREQStaus;

	WRODIA_BARDATA(void)
	{
		AUrno = 0;
		DUrno = 0;
		ARkey = 0;
		DRkey = 0;
		StartTime = TIMENULL;
		EndTime = TIMENULL;
		GatBegin = TIMENULL;
		GatEnd = TIMENULL;
		WroBegin = TIMENULL;
		WroEnd = TIMENULL;
		BltBegin = TIMENULL;
		BltEnd = TIMENULL;
		Tmo = TIMENULL;
		Land = TIMENULL;
		Onbl = TIMENULL;
		Ofbl = TIMENULL; 
		Slot = TIMENULL;
		Airb = TIMENULL;
		TextColor = COLORREF(BLACK);
		SpecialREQ = "";
		SpecialREQStaus = NO_REQ;
	}
};
struct WRODIA_BKBARDATA
{
	long AUrno;
	long DUrno;
	long ARkey;
	long DRkey;
    CString Text;
	CTime StartTime;
    CTime EndTime;
    CBrush *MarkerBrush;	// background bar will be only displayed in FRAMENONE, MARKFULL
    CBrush *SepBrush;	
	WRODIA_BKBARDATA(void)
	{
		AUrno = 0;
		DUrno = 0;
		ARkey = 0;
		DRkey = 0;
		Text = "";
		StartTime = TIMENULL;
		EndTime  = TIMENULL;
		MarkerBrush = NULL;
	}
};
struct WRODIA_LINEDATA 
{
	long Urno;
	CString Wnam;
	CString Term;
	long AUrno;
	long DUrno;
	long ARkey;
	long DRkey;
	CTime StartTime;
	CTime EndTime;
    CString Text;
	bool IsExpanded;
    int MaxOverlapLevel;    // maximum number of overlapped level of bars in this line
    CCSPtrArray<WRODIA_BARDATA> Bars;		// in order of painting (leftmost is the bottommost)
    CCSPtrArray<WRODIA_BKBARDATA> BkBars;	// background bar
    CCSPtrArray<int> TimeOrder;				// maintain overlapped bars, see below
	COLORREF TextColor;
	COLORREF TextColor2;
	COLORREF TextColor3;
	bool bmFullHeight;
	WRODIA_LINEDATA(void)
	{
		IsExpanded = false;
		bmFullHeight = false;
	}
};
struct WRODIA_GROUPDATA 
{
	int GroupNo;
	CString GroupName;			// Gruppen ID
	CString GroupType;			// 
	CCSPtrArray<CString> Codes; // For future Airlinecodes
	// standard data for groups in general Diagram
    CString Text;
	CBrush *BkBrush;
	CBrush *SepBrush;
    CCSPtrArray<WRODIA_LINEDATA> Lines;
	bool bmFullHeight;
	WRODIA_GROUPDATA(void)
	{
		bmFullHeight = false;
	}
};

// Attention:
// Element "TimeOrder" in the struct "WRODIA_LINEDATA".
//
// This array will maintain bars sorted by StartTime of each bar.
// You can see it the same way you see an array of index in the array WRODIA_BARDATA[].
// For example, if there are five bars in the first overlapped group, and their
// overlap level are [3, 0, 4, 1, 2]. So the indexes to the WRODIA_BARDATA[] sorted by
// the beginning of time should be [1, 3, 4, 0, 2]. You can see that the indexes
// in these two arrays are cross-linked together. Please notice that we define
// the overlap level to be the same number as the order of the bar when sorted
// by StartTime.
//
// However, I keep the relative offset in this array, not the absolute offset.
// The relative offset helps us separate each group of bar more efficiently.
// For example, using the same previous example, the relative offset of index
// to the array WRODIA_BARDATA[] kept in this array would be [+1, +2, +3, -3, -2].
//
// For a given "i", the bar in the same overlap group will always have the
// same value of "i - Bars[i + TimeOrder[i]].OverlapLevel". This value will be the
// index of the first member in that group.
//
// Notes: This bar order array should be implemented with CCSArray <int>.


/////////////////////////////////////////////////////////////////////////////
// WroDiagramViewer window

class WroGantt;
class WroChart;

class WroDiagramViewer: public CViewer
{
	friend WroGantt;

// Constructions
public:
    WroDiagramViewer();
    ~WroDiagramViewer();

	int  AdjustBar(DIAFLIGHTDATA *prpAFlight, DIAFLIGHTDATA *prpDFlight);
	int  FindFirstBar(long lpUrno); 

	void Attach(CWnd *popAttachWnd);
	void ChangeViewTo(const char *pcpViewName, CTime opStartTime, CTime opEndTime);
	bool CalculateTimes(const DIAFLIGHTDATA &rrpFlight, CTime &opStart, CTime &opEnd) const;
	void UpdateLineHeights();
	int GetItemID(int ipGroupNo, int ipLineNo) const;
	void PrintGantt(WroChart *popChart);
	bool GetLoadedFlights(CCSPtrArray<DIAFLIGHTDATA> &opFlights);

// Internal data processing routines
private:
	void PrepareGrouping();
	void PrepareFilter();
	void PrepareSorter();
	bool IsPassFilter(DIAFLIGHTDATA *prpFlight);
	bool IsPassFilter(RecordSet &ropRec);
	//bool IsPassFilter(DiaCedaFlightData::RKEYLIST *prpRot);
	int CompareGroup(WRODIA_GROUPDATA *prpGroup1, WRODIA_GROUPDATA *prpGroup2);
	int CompareLine(WRODIA_LINEDATA *prpLine1, WRODIA_LINEDATA *prpLine2);

	void MakeGroups();
	bool MakeGroupAll();
//	void MakeLines();
//	void MakeLine(WRODIA_GROUPDATA &rrpGroup, RecordSet &ropRec);
	void MakeBars();
	void MakeBars(DIAFLIGHTDATA *prlFlight);
//	void MakeLine(RecordSet &ropRec);
//	void MakeLineData(WRODIA_LINEDATA *prlLine, RecordSet &ropRec);
//	void MakeLine(RecordSet &ropRot, bool bpRECfromBLK = false);
	void MakeLine(WRODIA_GROUPDATA &rrpGroup, RecordSet &ropRec, bool bpRECfromBLK = false);
	void MakeLineData(WRODIA_LINEDATA *prlLine, RecordSet &ropRot, bool bpRECfromBLK = false);
	void MakeTriangleColors(DIAFLIGHTDATA *prpFlightA, DIAFLIGHTDATA *prpFlightD, WRODIA_BARDATA *prpBar);

	void MakeBarData(WRODIA_BARDATA *prlBar, DIAFLIGHTDATA *popFlight, int ipWro);
	void MakeBar(int ipGroupno, int ipLineno, DIAFLIGHTDATA *popFlight, int ipWro);
	long GetRkeyFromRotation(DiaCedaFlightData::RKEYLIST *popRotation);
	
	//bool GetFlightsInRotation(DiaCedaFlightData::RKEYLIST *popRotation, DIAFLIGHTDATA *prpFlightA, DIAFLIGHTDATA *prpFlightD);
	DIAFLIGHTDATA * GetFlightAInRotation(DiaCedaFlightData::RKEYLIST *popRotation);
	DIAFLIGHTDATA * GetFlightDInRotation(DiaCedaFlightData::RKEYLIST *popRotation);

	CString GroupText(int ipGroupNo);
	CString LineText(DiaCedaFlightData::RKEYLIST *popRot);
	CString BarText(DiaCedaFlightData::RKEYLIST *popRot);
	CString BarTextAndValues(WRODIA_BARDATA *prlBar);
	CString StatusText(DiaCedaFlightData::RKEYLIST *popRot);
	int FindGroup(CString opGroupName);
	bool FindLine(CString opWnam, CUIntArray &ropGroups, CUIntArray &ropLines);
	bool FindGroupsForType(CUIntArray &ropGroupNos, CString opType, CString opPfc);
	BOOL FindDutyBar(long lpUrno, int &ripGroupno, int &ripLineno);
	bool FindDutyBars(long lpUrno, CUIntArray &ropGroups, CUIntArray &ropLines);
	BOOL FindBar(long lpUrno, int &ripGroupno, int &ripLineno, int &ripBarno);
	bool FindBarsGlobal(long lpUrno, CUIntArray &ropGroups, 
						CUIntArray &ropLines, CUIntArray &ropBars);

	bool GetGroupAndLineNoOfItem(int ipItemID, int &ripGroupNo, int &ripLineNo) const;
 	bool GetFullHeight(int ipGroupNo, int ipLineno) const;
	void UpdateWroLines(CString opPos);

	void PrintPrepareLineData(int ipGroupNo,int ipLineno,CCSPtrArray<PRINTBARDATA> &ropPrintLine);
	void PrintDiaArea(WroChart *popChart);
	void PrintDiagramHeader();
	void PrintPrepareBKLineData(int ipGroupNo,int ipLineno,CCSPtrArray<PRINTBARDATA> &ropPrintBKLine);
	int	igFirstGroupOnPage;

// Operations
private:
	int omGeometryFontIndex;
	CTimeSpan omGeometryTimeSpan;
	bool bmGeometryTimeLines;
	bool bmGeometryMinimize;
	CTime omStartShowTime;
	int   imGeometrieHours;
	CImageList* omImageListNotValid;
public:
	void SelectBar(const WRODIA_BARDATA *prpBar, bool bpHighLight = false);
	void DeSelectAll();
    int GetGroupCount() const;
    WRODIA_GROUPDATA *GetGroup(int ipGroupno);
    CString GetGroupText(int ipGroupno);
    CString GetGroupTopScaleText(int ipGroupno);
	int GetGroupColorIndex(int ipGroupno,CTime opStart,CTime opEnd);
    int GetLineCount(int ipGroupno) const;
	int GetAllLineCount();
    WRODIA_LINEDATA *GetLine(int ipGroupno, int ipLineno);
    CString GetLineText(int ipGroupno, int ipLineno);
	bool GetAllLinesText( CStringArray &opWros ) const;
    int GetMaxOverlapLevel(int ipGroupno, int ipLineno);
	bool IsExpandedLine(int ipGroupno, int ipLineno);
	void SetExpanded(int ipGroupno, int ipLineno);
	int GetVisualMaxOverlapLevel(int ipGroupno, int ipLineno);
    int GetBkBarCount(int ipGroupno, int ipLineno);
    WRODIA_BKBARDATA *GetBkBar(int ipGroupno, int ipLineno, int ipBkBarno);
    CString GetBkBarText(int ipGroupno, int ipLineno, int ipBkBarno);
    int GetBarCount(int ipGroupno, int ipLineno);
    WRODIA_BARDATA *GetBar(int ipGroupno, int ipLineno, int ipBarno);
    CString GetBarText(int ipGroupno, int ipLineno, int ipBarno);
    CString GetStatusBarText(int ipGroupno, int ipLineno, int ipBarno);
	int GetIndicatorCount(int ipGroupno, int ipLineno, int ipBarno);
	WRODIA_INDICATORDATA *GetIndicator(int ipGroupno, int ipLineno, int ipBarno,
		int ipIndicatorno);

	bool CheckBkBarParameters(int ipGroupno, int ipLineno, int ipBkBarno) const;
	bool CheckBarParameters(int ipGroupno, int ipLineno, int ipBarno) const;
	bool CheckBarParameters(int ipGroupno, int ipLineno) const;

    void DeleteAll();
    int CreateGroup(WRODIA_GROUPDATA *prpGroup);
    void DeleteGroup(int ipGroupno);
    int CreateLine(WRODIA_GROUPDATA &rrpGroup, WRODIA_LINEDATA *prpLine);
    void DeleteLine(int ipGroupno, int ipLineno);
    int CreateBkBar(int ipGroupno, int ipLineno, WRODIA_BKBARDATA *prpBkBar);
    void DeleteBkBar(int ipGroupno, int ipLineno, int ipBkBarno);
    int CreateBar(int ipGroupno, int ipLineno, WRODIA_BARDATA *prpBar, BOOL bpFrontBar = TRUE);
    void DeleteBar(int ipGroupno, int ipLineno, int ipBarno, bool bpSetOverlapColor = true);
    int CreateIndicator(int ipGroupno, int ipLineno, int ipBarno,
    	WRODIA_INDICATORDATA *prpIndicator);
	void DeleteIndicator(int ipGroupno, int ipLineno, int ipBarno, int ipIndicator);

    int GetBarnoFromTime(int ipGroupno, int ipLineno, CTime opTime1, CTime opTime2,
        int ipOverlapLevel1, int ipOverlapLevel2);
    int GetBkBarnoFromTime(int ipGroupno, int ipLineno, CTime opTime1, CTime opTime2);
	void AllowUpdates(BOOL bpNoUpdatesNow);
	int GetGeometryFontIndex(){return omGeometryFontIndex;}
	CTimeSpan GetGeometryTimeSpan(){return omGeometryTimeSpan;}
	bool GetGeometryTimeLines(){return bmGeometryTimeLines;}
	bool GetGeometryMinimize(){return bmGeometryMinimize;}
	CTime GetGeometrieStartTime(){return omStartShowTime;}
	void MakeMasstab();
	void SetCnams( CString opCnams ) { omCnams = opCnams;};

	bool GetDispoZeitraumFromTo(CTime &ropFrom, CTime &ropTo);
// Private helper routines
private:
	void GetOverlappedBarsFromTime(int ipGroupno, int ipLineno,
		CTime opTime1, CTime opTime2, CUIntArray &ropBarnoList);
//	void GetGroupsFromViewer(CCSPtrArray<WRODIA_GROUPDATA> &ropGroups);
	bool WroDiagramViewer::GetGroupsFromViewer(CStringArray &ropGroups);
// Attributes used for filtering condition
private:
	int omGroupBy;			// enumerated value -- define in "stviewer.cpp"
	CStringArray omSortOrder;
	CTime omStartTime;
	CTime omEndTime;
// Attributes
private:
	CWnd *pomAttachWnd;
	CBrush omBkBrush;
	CBrush omBkBrush2;
	CBrush omBreakBrush;
	CBrush omBkBreakBrush;
	CBrush omWIFBkBrush;
	CBrush* omBrush;
	CString omCnams;

// Methods which handle changes (from Data Distributor)
public:
    CCSPtrArray<WRODIA_GROUPDATA> omGroups;
	void ProcessFlightInsert(CUIntArray *popRkeys);
	void ProcessFlightChange(DIAFLIGHTDATA *prpFlight);
	void ProcessFlightDelete(DIAFLIGHTDATA *prpFlight, bool bpUpdateView = true);
	void ProcessWroChange(RecordSet *prpRecord);
	void ProcessBlkChange( RecordSet *popBlkRecord);
	void ProcessWroBarUpdate(long *prpUrno,int ipDDXType);


	CBrush *GetBarColor(DIAFLIGHTDATA *prpFlight, int ipBelt);
	void SetKonfliktColor(int ipGroupno, int ipLineno);
	COLORREF GetTextColor(DIAFLIGHTDATA *prpFlight, char cpFPart, int ipBelt);



public:
	BOOL bmNoUpdatesNow;
	BOOL bmIsFirstGroupOnPage;
///////////////////////////////////////////////////////////////////////////////////////////
// Printing routines
private:
	CCSPrint *pomPrint;

public:
};

/////////////////////////////////////////////////////////////////////////////

#endif
