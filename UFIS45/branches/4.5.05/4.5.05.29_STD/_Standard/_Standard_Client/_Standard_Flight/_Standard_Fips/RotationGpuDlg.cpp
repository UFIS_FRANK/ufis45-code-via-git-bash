// RotationGpuDlg.cpp : implementation file
//

#include <stdafx.h>
#include <fpms.h>
#include <RotationGpuDlg.h>
#include <CedaGpaData.h>
#include <PrivList.h>
#include <CCSGlobl.h>
#include <CCSBcHandle.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// RotationGpuDlg dialog


RotationGpuDlg::RotationGpuDlg(CWnd* pParent /*=NULL*/, ROTATIONDLGFLIGHTDATA *prpFlight, bool bpLocal )
	: CDialog(RotationGpuDlg::IDD, pParent)
{
	prmFlight = prpFlight;
	bmLocal = bpLocal;


	//{{AFX_DATA_INIT(RotationGpuDlg)
	//}}AFX_DATA_INIT
}


void RotationGpuDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(RotationGpuDlg)
	DDX_Control(pDX, IDC_GAAE3, m_CE_Gaae3);
	DDX_Control(pDX, IDC_GAAE2, m_CE_Gaae2);
	DDX_Control(pDX, IDC_GAAE1, m_CE_Gaae1);
	DDX_Control(pDX, IDC_GAAB3, m_CE_Gaab3);
	DDX_Control(pDX, IDC_GAAB2, m_CE_Gaab2);
	DDX_Control(pDX, IDC_GAAB1, m_CE_Gaab1);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(RotationGpuDlg, CDialog)
	//{{AFX_MSG_MAP(RotationGpuDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// RotationGpuDlg message handlers

void RotationGpuDlg::OnOK() 
{

	if(!m_CE_Gaae1.GetStatus() || !m_CE_Gaae2.GetStatus() || !m_CE_Gaae3.GetStatus() ||
	   !m_CE_Gaab1.GetStatus() || !m_CE_Gaab2.GetStatus() || !m_CE_Gaab3.GetStatus())
	{
		MessageBox(GetString(ST_BADFORMAT), GetString(ST_FEHLER));

		return;
	}


	CString olGaab;
	CString olGaae;
		

	GPADATA *prlGpa;

	CTime olSto;

	if(CString(prmFlight->Adid) == "A")
		olSto = prmFlight->Stoa;
	else
		olSto = prmFlight->Stod;


	// 1 //////////////////////////////////

	m_CE_Gaae1.GetWindowText(olGaae);
	m_CE_Gaab1.GetWindowText(olGaab);

	CString olUser = CString(pcgUser); 
	CTime olTime = CTime::GetCurrentTime();
	if(bmLocal) ogBasicData.LocalToUtc(olTime);


	prlGpa = ogGpaData.GetGpa(1);
	if(prlGpa != NULL)
	{
		prlGpa->Gaae = 	HourStringToDate( olGaae, olSto);
		prlGpa->Gaab = 	HourStringToDate( olGaab, olSto);

		if(bmLocal) ogBasicData.LocalToUtc(prlGpa->Gaab);
		if(bmLocal) ogBasicData.LocalToUtc(prlGpa->Gaae);
		
		prlGpa->Lstu = olTime;
		strcpy(prlGpa->Useu, olUser);

		prlGpa->IsChanged = DATA_CHANGED;
		ogGpaData.Save(prlGpa);
	}
	else
	{
		if(!olGaab.IsEmpty() || !olGaae.IsEmpty())
		{
			prlGpa = new GPADATA;
			prlGpa->Flnu = prmFlight->Urno;
			prlGpa->Aseq = 1;
			//prlGpa->Urno = ogBasicData.GetNextUrno();
			prlGpa->Gaae = 	HourStringToDate( olGaae, olSto);
			prlGpa->Gaab = 	HourStringToDate( olGaab, olSto);

			if(bmLocal) ogBasicData.LocalToUtc(prlGpa->Gaab);
			if(bmLocal) ogBasicData.LocalToUtc(prlGpa->Gaae);
			prlGpa->IsChanged = DATA_NEW;
			prlGpa->Cdat = olTime;
			strcpy(prlGpa->Usec, olUser);

			ogGpaData.Save(prlGpa);
		}
	}

	// 2 //////////////////////////////////

	m_CE_Gaae2.GetWindowText(olGaae);
	m_CE_Gaab2.GetWindowText(olGaab);


	prlGpa = ogGpaData.GetGpa(2);
	if(prlGpa != NULL)
	{
		prlGpa->Gaae = 	HourStringToDate( olGaae, olSto);
		prlGpa->Gaab = 	HourStringToDate( olGaab, olSto);
		if(bmLocal) ogBasicData.LocalToUtc(prlGpa->Gaab);
		if(bmLocal) ogBasicData.LocalToUtc(prlGpa->Gaae);

		prlGpa->Lstu = olTime;
		strcpy(prlGpa->Useu, olUser);
		
		prlGpa->IsChanged = DATA_CHANGED;
		ogGpaData.Save(prlGpa);
	}
	else
	{
		if(!olGaab.IsEmpty() || !olGaae.IsEmpty())
		{
			prlGpa = new GPADATA;
			prlGpa->Flnu = prmFlight->Urno;
			prlGpa->Aseq = 2;
			//prlGpa->Urno = ogBasicData.GetNextUrno();
			prlGpa->Gaae = 	HourStringToDate( olGaae, olSto);
			prlGpa->Gaab = 	HourStringToDate( olGaab, olSto);
			if(bmLocal) ogBasicData.LocalToUtc(prlGpa->Gaab);
			if(bmLocal) ogBasicData.LocalToUtc(prlGpa->Gaae);

			prlGpa->Cdat = olTime;
			strcpy(prlGpa->Usec, olUser);
			prlGpa->IsChanged = DATA_NEW;

			ogGpaData.Save(prlGpa);
		}
	}

	// 3 //////////////////////////////////

	m_CE_Gaae3.GetWindowText(olGaae);
	m_CE_Gaab3.GetWindowText(olGaab);


	prlGpa = ogGpaData.GetGpa(3);
	if(prlGpa != NULL)
	{
		prlGpa->Gaae = 	HourStringToDate( olGaae, olSto);
		prlGpa->Gaab = 	HourStringToDate( olGaab, olSto);
		prlGpa->IsChanged = DATA_CHANGED;

		prlGpa->Lstu = olTime;
		strcpy(prlGpa->Useu, olUser);
		
		if(bmLocal) ogBasicData.LocalToUtc(prlGpa->Gaab);
		if(bmLocal) ogBasicData.LocalToUtc(prlGpa->Gaae);
		ogGpaData.Save(prlGpa);
	}
	else
	{
		if(!olGaab.IsEmpty() || !olGaae.IsEmpty())
		{
			prlGpa = new GPADATA;
			prlGpa->Flnu = prmFlight->Urno;
			prlGpa->Aseq = 3;
			//prlGpa->Urno = ogBasicData.GetNextUrno();
			prlGpa->Gaae = 	HourStringToDate( olGaae, olSto);
			prlGpa->Gaab = 	HourStringToDate( olGaab, olSto);
			if(bmLocal) ogBasicData.LocalToUtc(prlGpa->Gaab);
			if(bmLocal) ogBasicData.LocalToUtc(prlGpa->Gaae);
			prlGpa->IsChanged = DATA_NEW;
			prlGpa->Cdat = olTime;
			strcpy(prlGpa->Usec, olUser);

			ogGpaData.Save(prlGpa);
		}
	}




	CDialog::OnOK();
}

BOOL RotationGpuDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	m_CE_Gaab1.SetTypeToTime(false, true);
	m_CE_Gaab2.SetTypeToTime(false, true);
	m_CE_Gaab3.SetTypeToTime(false, true);

	m_CE_Gaae1.SetTypeToTime(false, true);
	m_CE_Gaae2.SetTypeToTime(false, true);
	m_CE_Gaae3.SetTypeToTime(false, true);



	m_CE_Gaae1.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_Gpu"));	
	m_CE_Gaae2.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_Gpu"));	
	m_CE_Gaae3.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_Gpu"));	


	m_CE_Gaab1.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_Gpu"));	
	m_CE_Gaab2.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_Gpu"));	
	m_CE_Gaab3.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_Gpu"));	


	ogGpaData.Read(prmFlight->Urno);
	
	
	GPADATA *prlGpa;




	CTime olSto;

	if(CString(prmFlight->Adid) == "A")
		olSto = prmFlight->Stoa;
	else
		olSto = prmFlight->Stod;




	prlGpa = ogGpaData.GetGpa(1);
	if(prlGpa != NULL)
	{
		if(bmLocal) ogBasicData.UtcToLocal(prlGpa->Gaab);
		if(bmLocal) ogBasicData.UtcToLocal(prlGpa->Gaae);

		m_CE_Gaab1.SetInitText(DateToHourDivString(prlGpa->Gaab, olSto));
		m_CE_Gaae1.SetInitText(DateToHourDivString(prlGpa->Gaae, olSto));
	}


	prlGpa = ogGpaData.GetGpa(2);
	if(prlGpa != NULL)
	{
		if(bmLocal) ogBasicData.UtcToLocal(prlGpa->Gaab);
		if(bmLocal) ogBasicData.UtcToLocal(prlGpa->Gaae);

		m_CE_Gaab2.SetInitText(DateToHourDivString(prlGpa->Gaab, olSto));
		m_CE_Gaae2.SetInitText(DateToHourDivString(prlGpa->Gaae, olSto));
	}

	prlGpa = ogGpaData.GetGpa(3);
	if(prlGpa != NULL)
	{
		if(bmLocal) ogBasicData.UtcToLocal(prlGpa->Gaab);
		if(bmLocal) ogBasicData.UtcToLocal(prlGpa->Gaae);

		m_CE_Gaab3.SetInitText(DateToHourDivString(prlGpa->Gaab, olSto));
		m_CE_Gaae3.SetInitText(DateToHourDivString(prlGpa->Gaae, olSto));
	}


	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}
