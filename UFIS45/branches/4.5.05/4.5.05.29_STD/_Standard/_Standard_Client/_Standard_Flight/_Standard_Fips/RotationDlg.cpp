// RotationDlg.cpp : implementation file
//
// Modification History: 
// 16-nov-00	rkr		OnAgent ge�ndert (PRF Athen)
// 17-nov-00	rkr		Aufruf vom Agentdialog unabh�ngig von Datenstruktur
// 22-nov-00	rkr		HandlePostFlight() and CheckPostFlight() 
//						for handling postflights added;

#include <stdafx.h>
#include <fpms.h>
#include <RotationDlg.h>
#include <CCSDdx.h>
#include <CCSTime.h>
#include <CCSTable.h>
#include <CCSGlobl.h>
#include <RotationTableViewer.h>
#include <RotationDlgCedaFlightData.h>
#include <RotationJoinDlg.h>
#include <ButtonListDlg.h>
#include <SeasonDlgCedaFlightData.h>
#include <PrivList.h>
#include <RotationRegnDlg.h>
#include <RotationAltDlg.h>
#include <RotationActDlg.h>
#include <RotationAptDlg.h>
#include <RotGroundDlg.h>
#include <RotationHAIDlg.h>
#include <RotationGpuDlg.h>
#include <RotationLoadDlg.h>
#include <RotationLoadDlgATH.h>
//#include <ViaTableCtrl.h>
#include <SeasonDlg.h>
#include <Utils.h>
#include <CedaFpeData.h>
#include <CedaCraData.h> 
#include <ReasonOverviewDlg.h>
#include <SpotAllocation.h>

#include <CedaBasicData.h>
#include <AwBasicDataDlg.h>
#include <RotationVipDlg.h>
#include <CedaCountData.h>
#include <AskBox.h>
#include <CedaFlightUtilsData.h>
#include <CcaCedaFlightData.H>
#include <ListBoxDlg.h>

#include <FlightDiagram.h>
#include <BltDiagram.h>
#include <CcaDiagram.h>
#include <GatDiagram.h>
#include <PosDiagram.h>
#include <WroDiagram.h>
#include <SeasonTableDlg.h>
#include <RotationTables.h>
#include <DailyScheduleTableDlg.h>
#include <process.h>
#include <RotationHtaDlg.h>
#include <CedaAptLocalUtc.h>
#include <RotationPcaDlg.h>
#include <RotationPlbDlg.h>
#include <MultiDelayDlg.h>
#include <ListStringArrayDlg.h>  
#include <CedaBlaData.h>
#include <CedaVipData.h>
#include <CCSCedaData.h>



#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

RotationDlg *pomThis;

#define MAX_CIC_TABLE_LINES 30
#define IDC_BTCRC_PSTA  41000 
#define IDC_BTCRC_AGTA1 41001 
#define IDC_BTCRC_AGTA2 41002 
#define IDC_BTCRC_RESOA 41003 
#define IDC_BTCRC_PSTD  41004 
#define IDC_BTCRC_DGTA1 41005 
#define IDC_BTCRC_DGTA2 41006 
#define IDC_BTCRC_RESOD 41007 
#define IDC_BTCRC_RGD1  41008 
#define IDC_BTCRC_RGD2  41009 
#define IDC_BTCRC_DATATOOL 41010
#define IDC_LBL_POS_ARR_DAILY 41011
#define IDC_LBL_POS_DEP_DAILY 41012
#define IDC_BTCRC_CASH_COMMENT  41013
#define IDC_LBL_CASH_COMMENT  41014
#define IDC_BTCRC_CASH_ARR  41016
#define IDC_BTCRC_CASH_DEP  41017

#define IDC_EDIT_CHOCKSON  41018
#define IDC_LBL_CHOCKSON    41019
#define IDC_LBL_CHOCKSOF    41020
#define IDC_EDIT_CHOCKSOF  41021

#define IDC_CBFPSA	41022   
#define IDC_CBFGA1	41023
#define IDC_CBFGA2	41024
#define IDC_CBFBL1	41025
#define IDC_CBFBL2	41026
#define IDC_CBFPSD	41027   
#define IDC_CBFGD1	41028
#define IDC_CBFGD2	41029

#define IDC_BTCRC_ACTIVITY    41030 
#define IDC_BTCRC_PAY_ARR	  41031
#define IDC_BTCRC_PAY_DEP     41032

#define IDC_BTCRC_ABLT1 41033 
#define IDC_BTCRC_ABLT2 41034 

#define IDC_BTCRC_ARR_AOG 41035 
#define IDC_BTCRC_DEP_AOG 41036
#define IDC_BTCRC_ARR_ADHOC 41037
#define IDC_BTCRC_DEP_ADHOC 41038
#define IDC_BTCRC_EFPS 41039
#define IDC_BTCRC_MTOW	41040 //lgp
#define IDC_LBL_MTOW	41041 //lgp
#define PERMIT_REQ "Permit Reqd"

/*
#define IDC_APCA 999999
#define IDC_DPCA 999998
#define IDC_APLB 999997
#define IDC_DPLB 999996
*/

int RotationDlg::CompareCcas(const CCADATA **e1, const CCADATA **e2)
{
	if (strlen((**e1).Ckic) == 0 && (**e1).Ckbs == TIMENULL)
		return 1;
	if (strlen((**e2).Ckic) == 0 && (**e2).Ckbs == TIMENULL)
		return -1;

	if (strlen((**e1).Ckic) == 0)
		return 1;
	if (strlen((**e2).Ckic) == 0)
		return -1;

	if (strcmp((**e1).Ckic, (**e2).Ckic) > 0)
		return 1;
	if (strcmp((**e1).Ckic, (**e2).Ckic) < 0)
		return -1;

	return 0;
}


int RotationDlg::CompareRotationFlight(const ROTATIONDLGFLIGHTDATA **e1, const ROTATIONDLGFLIGHTDATA **e2)
{

	if((**e2).Adid[0] == 'B' && (**e1).Adid[0] == 'B')
	{
		CTime olTime1;
		CTime olTime2;

		if((**e1).Tifa == TIMENULL)
			olTime1 = (**e1).Tifd;
		else
			olTime1 = (**e1).Tifa;

		if((**e2).Tifa == TIMENULL)
			olTime2 = (**e2).Tifd;
		else
			olTime2 = (**e2).Tifa;

		return ((olTime1 == olTime2) ? 0 : ((olTime1 > olTime2) ? 1 : -1));
	}
	else
	{
		return strcmp((**e1).Adid, (**e2).Adid);

	}

}




/////////////////////////////////////////////////////////////////////////////
// RotationDlg dialog


// Local function prototype
static void FlightTableCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName);
static void ProcessDcfCf(void *popInstance, int ipDDXType,
						 void *vpDataPointer, CString &ropInstanceName);


RotationDlg::RotationDlg(CWnd* pParent)
	: omCcaData(CCA_ROTDLG_NEW, CCA_ROTDLG_CHANGE, CCA_ROTDLG_DELETE), CDialog(RotationDlg::IDD, pParent)
{

	//{{AFX_DATA_INIT(RotationDlg)
	m_AVip = FALSE;
	m_DVip = FALSE;
	m_DFpsd = FALSE;
	m_AFpsa = FALSE;
	m_ABas1 = _T("");
	m_ABae1 = _T("");
	m_ABas2 = _T("");
	m_ABae2 = _T("");
	m_DTsat = _T("");
	m_DTobt = _T("");
	m_Regn = _T("");
	m_Act3 = _T("");
	m_Act5 = _T("");
	m_Ming = _T("");
	m_Acws = _T("");
	m_Acle = _T("");
	m_Paid = FALSE;
	m_AAirb = _T("");
	m_AFltn = _T("");
	m_AFlns = _T("");
	m_AFlti = _T("");
	m_AB1ba = _T("");
	m_AB1ea = _T("");
	m_AB2ba = _T("");
	m_AB2ea = _T("");
	m_ABlt1 = _T("");
	m_ABlt2 = _T("");
	m_DBlt1 = _T("");
	m_DB1ba = _T("");
	m_DB1ea = _T("");
	m_DTmb1 = _T("");

	m_ACxx = FALSE;
	m_ADcd1 = _T("");
	m_ADcd2 = _T("");
	m_ADiverted = FALSE;
	m_ADtd1 = _T("");
	m_ADtd2 = _T("");
	m_AEtai = _T("");
	m_AEtoa = _T("");
	m_AExt1 = _T("");
	m_AExt2 = _T("");
	m_AGa1x = _T("");
	m_AGa2x = _T("");
	m_AGa1y = _T("");
	m_AGa2y = _T("");
	m_AGta1 = _T("");
	m_AGta2 = _T("");
	m_AHtyp = _T("");
	m_AIfra = _T("");
	m_ALand = _T("");
	m_ALastChange = _T("");
	m_ALastChangeTime = _T("");
	m_ANxti = _T("");
	m_AOfbl = _T("");
	m_AOnbe = _T("");
	m_AOnbl = _T("");
	m_APsta = _T("");
	m_ARem1 = _T("");
	m_ARem3 = _T("");
	m_DRem3 = _T("");
	m_ARemp = _T("");
	m_ARerouted = FALSE;
	m_ARwya = _T("");
	m_AStev = _T("");
	m_ASte2 = _T("");
	m_ASte3 = _T("");
	m_ASte4 = _T("");
	m_AStoa = _T("");
	m_AStod = _T("");
	m_AStyp = _T("");
	m_ATga1 = _T("");
	m_ATga2 = _T("");
	m_ATmb1 = _T("");
	m_ATmb2 = _T("");
	m_ATmoa = _T("");
	m_ATtyp = _T("");
	m_DAirb = _T("");
	m_DAlc3 = _T("");
	m_DCxx = FALSE;
	m_DDcd1 = _T("");
	m_DDcd2 = _T("");
	m_DDes3 = _T("");
	m_DDes4 = _T("");
	m_DDiverted = FALSE;
	m_DDtd1 = _T("");
	m_DDtd2 = _T("");
	m_DEtdc = _T("");
	m_DEtdi = _T("");
	m_DEtod = _T("");
	m_DFlns = _T("");
	m_DGd1x = _T("");
	m_DGd1y = _T("");
	m_DGd2x = _T("");
	m_DGd2y = _T("");
	m_DGtd1 = _T("");
	m_DGtd2 = _T("");
	m_DHtyp = _T("");
	m_DIfrd = _T("");
	m_DIskd = _T("");
	m_DLastChange = _T("");
	m_DLastChangeTime = _T("");
	m_DNxti = _T("");
	m_DOfbl = _T("");
//	m_DPdba = _T("");
	m_DPstd = _T("");
	m_DRem1 = _T("");
	m_DRemp = _T("");
	m_DGatLogo = _T("");
	m_DGatLogo2 = _T("");
	m_DRerouted = FALSE;
	m_DRwyd = _T("");
	m_DSlot = _T("");
	m_DStev = _T("");
	m_DSte2 = _T("");
	m_DSte3 = _T("");
	m_DSte4 = _T("");
	m_DStoa = _T("");
	m_DStod = _T("");
	m_DStyp = _T("");
	m_DTgd1 = _T("");
	m_DTgd2 = _T("");
	m_DTtyp = _T("");
	m_DTwr1 = _T("");
	m_DTwr3 = _T("");
	m_DWro1 = _T("");
	m_DWro3 = _T("");
	m_ADes3 = _T("");
	m_AOrg3 = _T("");
	m_AOrg3L = _T("");
	m_ATet1 = _T("");
	m_ATet2 = _T("");
//	m_DPdea = _T("");
	m_DW1ba = _T("");
	m_DW3ba = _T("");
	m_DW1ea = _T("");
	m_DW3ea = _T("");
//	m_APaea = _T("");
	m_DOrg3 = _T("");
	m_AAlc3 = _T("");
	m_DFltn = _T("");
	m_DFlti = _T("");
//	m_APaba = _T("");
	m_ACsgn = _T("");
	m_DCsgn = _T("");
	m_ABbaa = _T("");
	m_DBbfa = _T("");
	m_DBaz1 = _T("");
	m_DBaz4 = _T("");
	m_DBao1 = _T("");
	m_DBac1 = _T("");
	m_DBao4 = _T("");
	m_DBac4 = _T("");
//	m_DPdba = _T("");
	m_ADivr = _T("");
	m_DDivr = _T("");
	m_AEtdi = _T("");
	m_DCiFr = _T("");
	m_GoAround = FALSE;
	//}}AFX_DATA_INIT


	pomParent = pParent;


	bmDlgEnabled = true;

	bmIsAFfnoShown = false;
	bmIsDFfnoShown = false;

	bmDeparture = false;
	bmArrival = false;
	bmIsCheckInNew = true;


	pomAJfnoTable = new CCSTable;
    pomDJfnoTable = new CCSTable;
	pomDCinsTable = new CCSTable;	
	pomDChuTable = new CCSTable;	

	polRotationALoadDlg = NULL;
	polRotationDLoadDlg = NULL;


	polRotationAViaDlg = NULL;
	polRotationDViaDlg = NULL;
	pomAVia = NULL;
	pomDVia = NULL;

	pomArrPermitsButton = NULL;
	pomDepPermitsButton = NULL;
/*
	pomAPca = NULL;
	pomDPca = NULL;
	pomAPlb = NULL;
	pomDPlb = NULL;
*/
	prmAFlight = new ROTATIONDLGFLIGHTDATA;
	prmDFlight = new ROTATIONDLGFLIGHTDATA;
	prmAFlightSave = new ROTATIONDLGFLIGHTDATA;
	prmDFlightSave = new ROTATIONDLGFLIGHTDATA;

	pomBTCRCFPSA  = new CCSButtonCtrl; 
	pomBTCRCAGTA1 = new CCSButtonCtrl;
	pomBTCRCAGTA2 = new CCSButtonCtrl;
	pomBTCRCRESOA = new CCSButtonCtrl;
	pomBTCRCFPSD  = new CCSButtonCtrl; 
	pomBTCRCDGTD1 = new CCSButtonCtrl;
	pomBTCRCDGTD2 = new CCSButtonCtrl;
	pomBTCRCRESOD = new CCSButtonCtrl;
	pomBTCRCABLT1 = new CCSButtonCtrl;
	pomBTCRCABLT2 = new CCSButtonCtrl;

	pomBTFlightDataTool = new CCSButtonCtrl;//AM 20070924
	pomBTActivity = new CCSButtonCtrl;//UFIS-1120

	pomLblPosArr = new CStatic();//AM 20071109
	pomLblPosDep = new CStatic();//AM 20071109

	if (ogPositionText.IsEmpty())
		omPositionText = "Position:";
	else
		omPositionText = ogPositionText + ":";

	pomDRgd1 = new CComboBox;
	pomDRgd2 = new CComboBox;
	
	pomBMTOW = new CCSEdit();//lgp
	pomLMTOW = new CStatic();//lgp
	
	pomBTCRCom = new CCSEdit(); //Added by Christine for Cash Comment
	pomLBLCCom = new CStatic(); //Added by Christine for Cash Comment
	m_CashArrBtn = new CCSButtonCtrl();
	m_CashDepBtn = new CCSButtonCtrl();
	
	pomCEChocksOn = new CCSEdit(); 
	pomLBLCEChocksOn = new CStatic(); 
	pomCEChocksOf = new CCSEdit(); 
	pomLBLCEChocksOf = new CStatic(); 

	//Fixed resources
	pomCBFPSA = new CButton;
	pomCBFGA1 = new CButton;
	pomCBFGA2 = new CButton;
	pomCBFBL1 = new CButton;
	pomCBFBL2 = new CButton;
	pomCBFPSD = new CButton;
	pomCBFGD1 = new CButton;
	pomCBFGD2 = new CButton;


	pomCBAAOG = new CCSButtonCtrl();
	pomCBDAOG = new CCSButtonCtrl();
	pomCBAAHOC = new CCSButtonCtrl();
	pomCBDAHOC = new CCSButtonCtrl();
	pomCBEFPS = new CCSButtonCtrl();

	pomAModeOfPay = new CCSButtonCtrl();
	pomDModeOfPay = new CCSButtonCtrl();
	polAModeOfPay = NULL;
	polDModeOfPay = NULL;

	m_pToolTip = NULL;



    CDialog::Create(RotationDlg::IDD, NULL);

	pomMultiDelayDlg = NULL;
	SetWindowPos(&wndTop,0,0,0,0, SWP_NOMOVE | SWP_HIDEWINDOW | SWP_NOSIZE);
//	SetWindowPos(&wndTopMost,0,0,0,0, SWP_NOMOVE | SWP_HIDEWINDOW | SWP_NOSIZE);
	m_key = "DialogPosition\\DailyDialog";

	//ModifyStyle();


	

	pomThis = this;
}

bool RotationDlg::ConvertStructUtcTOLocal(ROTATIONDLGFLIGHTDATA *prpAFlight, ROTATIONDLGFLIGHTDATA *prpDFlight)
{
	if (!prpAFlight && !prpDFlight)
		return false;

	if (prpAFlight)
	{
		ogRotationDlgFlights.StructUtcToLocal((void*)prpAFlight, "");
		if (bgRealLocal)
		{
			ConvertLocalTOUtc (prpAFlight, true, true);
			ConvertUtcTOLocal (prpAFlight, true);
		}
	}
	if (prpDFlight)
	{
		ogRotationDlgFlights.StructUtcToLocal((void*)prpDFlight, "");
		if (bgRealLocal)
		{
			ConvertLocalTOUtc (prpDFlight, false, true);
			ConvertUtcTOLocal (prpDFlight, false);
		}
	}

	return true;
}

bool RotationDlg::ConvertStructLocalTOUtc(ROTATIONDLGFLIGHTDATA *prpAFlight, ROTATIONDLGFLIGHTDATA *prpDFlight)
{
	if (!prpAFlight && !prpDFlight)
		return false;

	if (prpAFlight)
	{
		ogRotationDlgFlights.StructLocalToUtc((void*)prpAFlight, "");
		if (bgRealLocal)
		{
			ConvertUtcTOLocal (prpAFlight, true, true);
			ConvertLocalTOUtc (prpAFlight, true);
		}
	}
	if (prpDFlight)
	{
		ogRotationDlgFlights.StructLocalToUtc((void*)prpDFlight, "");
		if (bgRealLocal)
		{
			ConvertUtcTOLocal (prpDFlight, false, true);
			ConvertLocalTOUtc (prpDFlight, false);
		}
	}

	return true;
}


bool RotationDlg::ConvertUtcTOLocal (ROTATIONDLGFLIGHTDATA *prpFlight, bool bpArr, bool bpHopo)
{
	if (!prpFlight)
		return false;

	if (bpArr)
	{
		if (bpHopo)
		{
			CedaAptLocalUtc::AptUtcToLocal (prpFlight->Stod, CString(pcgHome4));
			CedaAptLocalUtc::AptUtcToLocal (prpFlight->Airb, CString(pcgHome4));
			CedaAptLocalUtc::AptUtcToLocal (prpFlight->Airu, CString(pcgHome4));
			CedaAptLocalUtc::AptUtcToLocal (prpFlight->Ofbl, CString(pcgHome4));
			CedaAptLocalUtc::AptUtcToLocal (prpFlight->Ofbu, CString(pcgHome4));
			CedaAptLocalUtc::AptUtcToLocal (prpFlight->Etdi, CString(pcgHome4));
			CedaAptLocalUtc::AptUtcToLocal (prpFlight->Etdu, CString(pcgHome4));
		}
		else
		{
			CedaAptLocalUtc::AptUtcToLocal (prpFlight->Stod, CString(prpFlight->Org4));
			CedaAptLocalUtc::AptUtcToLocal (prpFlight->Airb, CString(prpFlight->Org4));
			CedaAptLocalUtc::AptUtcToLocal (prpFlight->Airu, CString(prpFlight->Org4));
			CedaAptLocalUtc::AptUtcToLocal (prpFlight->Ofbl, CString(prpFlight->Org4));
			CedaAptLocalUtc::AptUtcToLocal (prpFlight->Ofbu, CString(prpFlight->Org4));
			CedaAptLocalUtc::AptUtcToLocal (prpFlight->Etdi, CString(prpFlight->Org4));
			CedaAptLocalUtc::AptUtcToLocal (prpFlight->Etdu, CString(prpFlight->Org4));
		}
	}
	else
	{
		if (bpHopo)
			CedaAptLocalUtc::AptUtcToLocal (prpFlight->Stoa, CString(pcgHome4));
		else
			CedaAptLocalUtc::AptUtcToLocal (prpFlight->Stoa, CString(prpFlight->Des4));
	}


	return true;
}

bool RotationDlg::ConvertLocalTOUtc (ROTATIONDLGFLIGHTDATA *prpFlight, bool bpArr, bool bpHopo)
{
	if (!prpFlight)
		return false;

	if (bpArr)
	{
		if (bpHopo)
		{
			CedaAptLocalUtc::AptLocalToUtc (prpFlight->Stod, CString(pcgHome4));
			CedaAptLocalUtc::AptLocalToUtc (prpFlight->Airb, CString(pcgHome4));
			CedaAptLocalUtc::AptLocalToUtc (prpFlight->Airu, CString(pcgHome4));
			CedaAptLocalUtc::AptLocalToUtc (prpFlight->Ofbl, CString(pcgHome4));
			CedaAptLocalUtc::AptLocalToUtc (prpFlight->Ofbu, CString(pcgHome4));
			CedaAptLocalUtc::AptLocalToUtc (prpFlight->Etdi, CString(pcgHome4));
			CedaAptLocalUtc::AptLocalToUtc (prpFlight->Etdu, CString(pcgHome4));
		}
		else
		{
			CedaAptLocalUtc::AptLocalToUtc (prpFlight->Stod, CString(prpFlight->Org4));
			CedaAptLocalUtc::AptLocalToUtc (prpFlight->Airb, CString(prpFlight->Org4));
			CedaAptLocalUtc::AptLocalToUtc (prpFlight->Airu, CString(prpFlight->Org4));
			CedaAptLocalUtc::AptLocalToUtc (prpFlight->Ofbl, CString(prpFlight->Org4));
			CedaAptLocalUtc::AptLocalToUtc (prpFlight->Ofbu, CString(prpFlight->Org4));
			CedaAptLocalUtc::AptLocalToUtc (prpFlight->Etdi, CString(prpFlight->Org4));
			CedaAptLocalUtc::AptLocalToUtc (prpFlight->Etdu, CString(prpFlight->Org4));
		}
	}
	else
	{
		if (bpHopo)
			CedaAptLocalUtc::AptLocalToUtc (prpFlight->Stoa, CString(pcgHome4));
		else
			CedaAptLocalUtc::AptLocalToUtc (prpFlight->Stoa, CString(prpFlight->Des4));
	}


	return true;
}

void RotationDlg::SetModus(bool bpRotationReadOnly)
{
	if (bmRotationReadOnly != bpRotationReadOnly)
		bmRotationReadOnly = bpRotationReadOnly;
}

//bool RotationDlg::NewData(CWnd* pParent, long lpRkey, long lpUrno, CString opAdid, bool bpLocalTime)
bool RotationDlg::NewData(CWnd* pParent, long lpRkey, long lpUrno, CString opAdid, bool bpLocalTime, bool bpRotationReadOnly)
{
	

	if(!(ogPrivList.GetStat("ROTATIONDLG_Dialog") == '1' || ogPrivList.GetStat("ROTATIONDLG_Dialog") == ' ') )	
	{	
		ShowWindow(SW_HIDE);
		return true;
	}
	
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
	
	ClearAll();
	omAdid = opAdid;
	bmLocalTime = bpLocalTime;
	bmRotationReadOnly = bpRotationReadOnly;

	bmLocked = false;
	m_CB_Lock.SetColors(::GetSysColor(COLOR_BTNFACE), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
	m_CB_Lock.SetWindowText(GetString(IDS_STRING2875));


	if(lpRkey == 0)
		lpRkey = ogRotationDlgFlights.GetRkey(lpUrno);
	
	pomParent = pParent;
	lmCalledBy = lpUrno;
	lmRkey = lpRkey;


	Register();



	ogRotationDlgFlights.ReadRotation( lpRkey, lpUrno, true);

    bool blFlightFound = ProcessRotationChange();

	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));

	if (!blFlightFound)
	{
		ogRotationDlgFlights.ClearAll();
	
		ogDdx.UnRegister(this,NOTUSED);
		ClearAll();
 		return false;
	}


//MWO: 13.04.05 Multi Delay codes
	if(bgMulitpleDelayCodes == true)
	{
		CString olWhere;
		tabDelayArrival.ResetContent();
		if(prmAFlight->Urno != 0)
		{
			char pclUrno[20]="";
			sprintf(pclUrno, "%d", prmAFlight->Urno);
			olWhere = "WHERE FURN=" + CString(pclUrno);
			if(tabDelayArrival.CedaAction("RT", "DCFTAB", tabDelayArrival.GetLogicalFieldList(), "", olWhere.GetBuffer(0)) == TRUE)
			{
			}
			
			olWhere = "WHERE FURN=" + CString(pclUrno);
			
			sprintf(pclUrno, "%ld", tabDelayArrival.GetLineCount());
			
			//CString olText = "Delays (" + CString(pclUrno) + ")";

			
			CString olText = "Delays (" + GetDelayCount(&tabDelayArrival) + ")";

		
			m_DelayArrBtn.SetWindowText(olText);

			if (m_CB_Ok.m_hWnd)
			    m_DelayArrBtn.SetFont(m_CB_Ok.GetFont());

			if(tabDelayArrival.GetLineCount() > 0)
				m_DelayArrBtn.SetColors(RGB(255,128, 0), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
			else
				m_DelayArrBtn.SetColors(::GetSysColor(COLOR_BTNFACE), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
			m_DelayArrBtn.UpdateWindow();
		}
		else
		{
			m_DelayArrBtn.SetColors(::GetSysColor(COLOR_BTNFACE), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
			m_DelayArrBtn.SetWindowText("Delay");
			m_DelayArrBtn.UpdateWindow();
		}
		
		tabDelayDeparture.ResetContent();
		if(prmDFlight->Urno != 0)
		{
			char pclUrno[20]="";
			sprintf(pclUrno, "%d", prmDFlight->Urno);
			olWhere = "WHERE FURN=" + CString(pclUrno);
			if(tabDelayDeparture.CedaAction("RT", "DCFTAB", tabDelayDeparture.GetLogicalFieldList(), "", olWhere.GetBuffer(0)) == TRUE)
			{
//				tabDelayDeparture.InsertTextLine("URNO,FURN,DURN,Y,REMA,DURA,CDAT,LSTU,USEC,USEU", TRUE);
			}
			sprintf(pclUrno, "%ld", tabDelayDeparture.GetLineCount());
			CString olText = "Delays (" + GetDelayCount(&tabDelayDeparture)  + ")";
			m_DelayDepBtn.SetWindowText(olText);

			if (m_CB_Ok.m_hWnd)
			    m_DelayDepBtn.SetFont(m_CB_Ok.GetFont());

			if(tabDelayDeparture.GetLineCount() > 0)
				m_DelayDepBtn.SetColors(RGB(255,128, 0), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
			else
				m_DelayDepBtn.SetColors(::GetSysColor(COLOR_BTNFACE), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
			m_DelayDepBtn.UpdateWindow();
		}
		else
		{
			m_DelayDepBtn.SetColors(::GetSysColor(COLOR_BTNFACE), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
			m_DelayDepBtn.SetWindowText("Delay");
			m_DelayDepBtn.UpdateWindow();
		}
	}
//MWO: 13.04.05 Multi Delay codes

	

	SetWndPos();
	
	bmInit = true;
	
	return true;
}


RotationDlg::~RotationDlg()
{
	ogRotationDlgFlights.ClearAll();
	
	ogDdx.UnRegister(this,NOTUSED);
	ClearAll();

	if (pomDRgd1) delete pomDRgd1;
	if (pomDRgd2) delete pomDRgd2;

	if(pomAJfnoTable != NULL)
		delete pomAJfnoTable;
//	if(pomAViaCtrl != NULL)
//		delete pomAViaCtrl;
	if(pomDJfnoTable != NULL)
		delete pomDJfnoTable;
//	if(pomDViaCtrl != NULL)
//		delete pomDViaCtrl;
	if(pomDCinsTable != NULL)
		delete pomDCinsTable;

	if(pomDChuTable != NULL)
		delete pomDChuTable;


	if(prmDFlight != NULL)
		delete prmDFlight;
	if(prmAFlight != NULL)
		delete prmAFlight;
	if(prmDFlightSave != NULL)
		delete prmDFlightSave;
	if(prmAFlightSave != NULL)
		delete prmAFlightSave;

	if (pomAVia)
		delete pomAVia;

	if (pomDVia)
		delete pomDVia;

	if(pomArrPermitsButton)
		delete pomArrPermitsButton;

	if(pomDepPermitsButton)
		delete pomDepPermitsButton;

	if (pomBTFlightDataTool)
		delete pomBTFlightDataTool;
/*
	if (pomAPca)
		delete pomAPca;
	if (pomDPca)
		delete pomDPca;
	if (pomAPlb)
		delete pomAPlb;
	if (pomDPlb)
		delete pomDPlb;
*/
	if (polRotationAViaDlg)
	{
		polRotationAViaDlg->DestroyWindow();
		delete polRotationAViaDlg;
		polRotationAViaDlg = NULL;
	}

	if (polRotationDViaDlg)
	{
		polRotationDViaDlg->DestroyWindow();
		delete polRotationDViaDlg;
		polRotationDViaDlg = NULL;
	}

	if (polRotationALoadDlg)
	{
		polRotationALoadDlg->DestroyWindow();
		delete polRotationALoadDlg;
		polRotationALoadDlg = NULL;
	}

	if (polRotationDLoadDlg)
	{
		polRotationDLoadDlg->DestroyWindow();
		delete polRotationDLoadDlg;
		polRotationDLoadDlg = NULL;
	}

	/*
	if (polAModeOfPay)
	{
		polAModeOfPay->DestroyWindow();
		delete polAModeOfPay;
		polAModeOfPay = NULL;
	}
	
	if (polDModeOfPay)
	{
		polDModeOfPay->DestroyWindow();
		delete polDModeOfPay;
		polDModeOfPay = NULL;
	}
	*/

	//Kill the process
	if (mdEFPSProcId!=NULL)			
	{		
		killProcess( mdEFPSProcId );	
	}	
	delete m_pToolTip;

}



void RotationDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(RotationDlg)
	DDX_Control(pDX, IDC_CAR, m_LB_CAROSEL);
	DDX_Control(pDX, IDC_KEY2, m_LB_KEY2);
	DDX_Control(pDX, IDC_KEY1, m_LB_KEY1);
	DDX_Control(pDX, IDC_NEWVIPA, m_CB_VipADlg);
	DDX_Control(pDX, IDC_NEWVIPD, m_CB_VipDDlg);
	DDX_Control(pDX, IDC_ACXXREASON, m_CL_ACxxReason);
	DDX_Control(pDX, IDC_DCXXREASON, m_CL_DCxxReason);
	DDX_Control(pDX, IDC_ACBNFES, m_CB_ANfes);
	DDX_Control(pDX, IDC_DCBNFES, m_CB_DNfes);
	DDX_Control(pDX, IDC_ANFES, m_CE_ANfes);
	DDX_Control(pDX, IDC_DNFES, m_CE_DNfes);
	DDX_Control(pDX, IDC_AVIP, m_CB_AVip);
	DDX_Control(pDX, IDC_DVIP, m_CB_DVip);
	DDX_Control(pDX, IDC_DFPSD, m_CB_DFpsd);
	DDX_Control(pDX, IDC_AFPSA, m_CB_AFpsa);
	DDX_Control(pDX, IDC_FL_REPORT_ARR, m_CB_ARep);
	DDX_Control(pDX, IDC_FL_REPORT_DEP, m_CB_DRep);
	DDX_Control(pDX, IDC_DDELAY, m_CB_DDelay);
	DDX_Control(pDX, IDC_AGPU, m_CB_AGpu);
	DDX_Control(pDX, IDC_DGPU, m_CB_DGpu);
	DDX_Control(pDX, IDC_DLOAD, m_CB_DLoad);
	DDX_Control(pDX, IDC_ALOAD, m_CB_ALoad);
	DDX_Control(pDX, IDC_ALOG, m_CB_ALog);
	DDX_Control(pDX, IDC_DLOG, m_CB_DLog);
	DDX_Control(pDX, IDC_DCICREM4, m_CB_DCicRem4);
	DDX_Control(pDX, IDC_DCICREM3, m_CB_DCicRem3);
	DDX_Control(pDX, IDC_DCICREM2, m_CB_DCicRem2);
	DDX_Control(pDX, IDC_DCICREM1, m_CB_DCicRem1);
	DDX_Control(pDX, IDC_DCICCS4, m_CB_DCicCodeShare4);
	DDX_Control(pDX, IDC_DCICCS3, m_CB_DCicCodeShare3);
	DDX_Control(pDX, IDC_DCICCS2, m_CB_DCicCodeShare2);
	DDX_Control(pDX, IDC_DCICCS1, m_CB_DCicCodeShare1);
	DDX_Control(pDX, IDC_TOWING, m_CB_Towing);
	DDX_Control(pDX, IDC_REQ, m_CB_REQ);
	DDX_Control(pDX, IDC_ADIVR, m_CE_ADivr);
	DDX_Control(pDX, IDC_DDIVR, m_CE_DDivr);
	DDX_Control(pDX, IDC_DRETTAXI, m_CB_DRetTaxi);
	DDX_Control(pDX, IDC_DRETFLIGHT, m_CB_DRetFlight);
	DDX_Control(pDX, IDC_ARETTAXI, m_CB_ARetTaxi);
	DDX_Control(pDX, IDC_ARETFLIGHT, m_CB_ARetFlight);
	DDX_Control(pDX, IDC_DGD2D, m_CB_DGd2d);
	DDX_Control(pDX, IDC_DBAZ4, m_CE_DBaz4);
	DDX_Control(pDX, IDC_DBAZ1, m_CE_DBaz1);
	DDX_Control(pDX, IDC_DBAO1, m_CE_DBao1);
	DDX_Control(pDX, IDC_DBAC1, m_CE_DBac1);
	DDX_Control(pDX, IDC_DBAO4, m_CE_DBao4);
	DDX_Control(pDX, IDC_DBAC4, m_CE_DBac4);
	DDX_Control(pDX, IDC_ABAS1, m_CE_ABas1);
	DDX_Control(pDX, IDC_ABAE1, m_CE_ABae1);
	DDX_Control(pDX, IDC_ABAS2, m_CE_ABas2);
	DDX_Control(pDX, IDC_ABAE2, m_CE_ABae2);
	DDX_Control(pDX, IDC_DBBFA, m_CE_DBbfa);
	DDX_Control(pDX, IDC_ABBAA, m_CE_ABbaa);
	DDX_Control(pDX, IDC_SEASON_MASK, m_CB_SeasonMask);
	DDX_Control(pDX, IDC_DCSGN, m_CE_DCsgn);
	DDX_Control(pDX, IDC_ACSGN, m_CE_ACsgn);
	DDX_Control(pDX, IDC_PAID, m_CB_Paid);
	DDX_Control(pDX, IDC_DTTYPLIST, m_CB_DTtypList);
	DDX_Control(pDX, IDC_DDCD1LIST, m_CB_DDcd1List);
	DDX_Control(pDX, IDC_ATTYPLIST, m_CB_ATtypList);
	DDX_Control(pDX, IDC_DREMP, m_CL_DRemp);
	DDX_Control(pDX, IDC_AREMP, m_CL_ARemp);
	DDX_Control(pDX, IDC_DGATLOGO, m_CL_DGatLogo);
	DDX_Control(pDX, IDC_DGATLOGO2, m_CL_DGatLogo2);
	DDX_Control(pDX, IDOK, m_CB_Ok);
	DDX_Control(pDX, IDC_LOCK, m_CB_Lock);
	DDX_Control(pDX, IDC_AEXT2LIST, m_CB_AExt2List);
	DDX_Control(pDX, IDC_AEXT1LIST, m_CB_AExt1List);
	DDX_Control(pDX, IDC_ACT35LIST, m_CB_Act35List);
	DDX_Control(pDX, IDC_DPSTDLIST, m_CB_DPstdList);
	DDX_Control(pDX, IDC_DCINSLIST4, m_CB_DCinsList4);
	DDX_Control(pDX, IDC_DCINSLIST3, m_CB_DCinsList3);
	DDX_Control(pDX, IDC_APSTALIST, m_CB_APstaList);
	DDX_Control(pDX, IDC_DWRO1LIST, m_CB_DWro1List);
	DDX_Control(pDX, IDC_DCINSLIST2, m_CB_DCinsList2);
	DDX_Control(pDX, IDC_DCINSLIST1, m_CB_DCinsList1);
	DDX_Control(pDX, IDC_DCICLOGO1, m_CB_DCicLogo1);
	DDX_Control(pDX, IDC_DCICLOGO2, m_CB_DCicLogo2);
	DDX_Control(pDX, IDC_DCICLOGO3, m_CB_DCicLogo3);
	DDX_Control(pDX, IDC_DCICLOGO4, m_CB_DCicLogo4);
	DDX_Control(pDX, IDC_DCINSBORDER, m_CE_DCinsBorder);
	DDX_Control(pDX, IDC_DCHUBORDER, m_CE_DChuBorder);
	DDX_Control(pDX, IDC_DALC3LIST3, m_CB_DAlc3List3);
	DDX_Control(pDX, IDC_DALC3LIST2, m_CB_DAlc3List2);
	DDX_Control(pDX, IDC_AAlc3LIST3, m_CB_AAlc3List3);
	DDX_Control(pDX, IDC_AAlc3LIST2, m_CB_AAlc3List2);
	DDX_Control(pDX, IDC_STATIC_ANKUNFT, m_CS_Ankunft);
	DDX_Control(pDX, IDC_STATIC_ABFLUG, m_CS_Abflug);
	DDX_Control(pDX, IDC_DGD2X, m_CE_DGd2x);
	DDX_Control(pDX, IDC_ARWYA, m_CE_ARwya);
	DDX_Control(pDX, IDC_MING, m_CE_Ming);
	DDX_Control(pDX, IDC_DFLTN, m_CE_DFltn);
	DDX_Control(pDX, IDC_DFLTI, m_CE_DFlti);
 	DDX_Control(pDX, IDC_AALC3, m_CE_AAlc3);
	DDX_Control(pDX, IDC_DORG3, m_CE_DOrg3);
	DDX_Control(pDX, IDC_JOIN, m_CB_Join);
	DDX_Control(pDX, IDC_DW1EA, m_CE_DW1ea);
	DDX_Control(pDX, IDC_DW1BA, m_CE_DW1ba);
	DDX_Control(pDX, IDC_ATET2, m_CE_ATet2);
	DDX_Control(pDX, IDC_ATET1, m_CE_ATet1);
	DDX_Control(pDX, IDC_AORG3, m_CE_AOrg3);
	DDX_Control(pDX, IDC_AORG3L, m_CE_AOrg3L);
	DDX_Control(pDX, IDC_ADES3, m_CE_ADes3);
	DDX_Control(pDX, IDC_DTOBT, m_CE_DTobt);
	DDX_Control(pDX, IDC_DTSAT, m_CE_DTsat);
	DDX_Control(pDX, IDC_CLB_ARR, m_CB_CkeckList_Arr);
	DDX_Control(pDX, IDC_SPLIT, m_CB_Split);
	DDX_Control(pDX, IDC_CLB_DEP, m_CB_CkeckList_Dep);
	DDX_Control(pDX, IDC_OBJECT, m_CB_Object);
	DDX_Control(pDX, IDC_DWRO1, m_CE_DWro1);
	DDX_Control(pDX, IDC_DTWR1, m_CE_DTwr1);
	DDX_Control(pDX, IDC_DTTYP, m_CE_DTtyp);
	DDX_Control(pDX, IDC_DTGD2, m_CE_DTgd2);
	DDX_Control(pDX, IDC_DTGD1, m_CE_DTgd1);
	DDX_Control(pDX, IDC_DTelex, m_CB_DTelex);
	DDX_Control(pDX, IDC_DSTYPLIST, m_CB_DStypList);
	DDX_Control(pDX, IDC_DSTYP, m_CE_DStyp);
	DDX_Control(pDX, IDC_DSTOD, m_CE_DStod);
	DDX_Control(pDX, IDC_DSTOA, m_CE_DStoa);
	DDX_Control(pDX, IDC_DSTEV, m_CE_DStev);
	DDX_Control(pDX, IDC_DSTE2, m_CE_DSte2);
	DDX_Control(pDX, IDC_DSTE3, m_CE_DSte3);
	DDX_Control(pDX, IDC_DSTE4, m_CE_DSte4);
	DDX_Control(pDX, IDC_DSLOT, m_CE_DSlot);
	DDX_Control(pDX, IDC_DSHOWJFNO, m_CB_DShowJfno);
	DDX_Control(pDX, IDC_DRWYD, m_CE_DRwyd);
	DDX_Control(pDX, IDC_DREM1, m_CE_DRem1);
	DDX_Control(pDX, IDC_DREM3, m_CE_DRem3);
	DDX_Control(pDX, IDC_AREM3, m_CE_ARem3);
	DDX_Control(pDX, IDC_DPSTD, m_CE_DPstd);
	DDX_Control(pDX, IDC_DOFBL, m_CE_DOfbl);
	DDX_Control(pDX, IDC_DNXTI, m_CE_DNxti);
	DDX_Control(pDX, IDC_DLASTCHANGE, m_CE_DLastChange);
	DDX_Control(pDX, IDC_DLASTCHANGETIME, m_CE_DLastChangeTime);
	DDX_Control(pDX, IDC_DJFNOBORDER, m_CE_DJfnoBorder);
	DDX_Control(pDX, IDC_DISKD, m_CE_DIskd);
	DDX_Control(pDX, IDC_DIFRD, m_CE_DIfrd);
	DDX_Control(pDX, IDC_DHTYPLIST, m_CB_DHtypList);
	DDX_Control(pDX, IDC_DHTYP, m_CE_DHtyp);
	DDX_Control(pDX, IDC_DGTD2, m_CE_DGtd2);
	DDX_Control(pDX, IDC_DGTD1, m_CE_DGtd1);
	DDX_Control(pDX, IDC_DGTA1LIST, m_CB_DGta1List);
	DDX_Control(pDX, IDC_DGTA2LIST, m_CB_DGta2List);
	DDX_Control(pDX, IDC_DGD2Y, m_CE_DGd2y);
	DDX_Control(pDX, IDC_DGD1Y, m_CE_DGd1y);
	DDX_Control(pDX, IDC_DGD1X, m_CE_DGd1x);
	DDX_Control(pDX, IDC_DFLNS, m_CE_DFlns);
	DDX_Control(pDX, IDC_DETOD, m_CE_DEtod);
	DDX_Control(pDX, IDC_DETDI, m_CE_DEtdi);
	DDX_Control(pDX, IDC_DETDC, m_CE_DEtdc);
	DDX_Control(pDX, IDC_DDTD2, m_CE_DDtd2);
	DDX_Control(pDX, IDC_DDTD1, m_CE_DDtd1);
	DDX_Control(pDX, IDC_DDES3LIST, m_CB_DDes3List);
	DDX_Control(pDX, IDC_DDES3, m_CE_DDes3);
	DDX_Control(pDX, IDC_DDES4, m_CE_DDes4);
	DDX_Control(pDX, IDC_DDCD2LIST, m_CB_DDcd2List);
	DDX_Control(pDX, IDC_DDCD2, m_CE_DDcd2);
	DDX_Control(pDX, IDC_DDCD1, m_CE_DDcd1);
	DDX_Control(pDX, IDC_DCXX, m_CB_DCxx);
	DDX_Control(pDX, IDC_DRerouted, m_CB_DRerouted);
	DDX_Control(pDX, IDC_DDiverted, m_CB_DDiverted);
	DDX_Control(pDX, IDC_DALC3LIST, m_CB_DAlc3List);
	DDX_Control(pDX, IDC_DALC3, m_CE_DAlc3);
	DDX_Control(pDX, IDC_DAIRB, m_CE_DAirb);
	DDX_Control(pDX, IDC_ATTYP, m_CE_ATtyp);
	DDX_Control(pDX, IDC_ATMOA, m_CE_ATmoa);
	DDX_Control(pDX, IDC_ATMB2, m_CE_ATmb2);
	DDX_Control(pDX, IDC_ATMB1, m_CE_ATmb1);
	DDX_Control(pDX, IDC_ATGA2, m_CE_ATga2);
	DDX_Control(pDX, IDC_ATGA1, m_CE_ATga1);
	DDX_Control(pDX, IDC_ATelex, m_CB_ATelex);
	DDX_Control(pDX, IDC_ASTYPLIST, m_CB_AStypList);
	DDX_Control(pDX, IDC_ASTYP, m_CE_AStyp);
	DDX_Control(pDX, IDC_ASTOD, m_CE_AStod);
	DDX_Control(pDX, IDC_ASTOA, m_CE_AStoa);
	DDX_Control(pDX, IDC_ASTEV, m_CE_AStev);
	DDX_Control(pDX, IDC_ASTE2, m_CE_ASte2);
	DDX_Control(pDX, IDC_ASTE3, m_CE_ASte3);
	DDX_Control(pDX, IDC_ASTE4, m_CE_ASte4);
	DDX_Control(pDX, IDC_ASHOWJFNO, m_CB_AShowJfno);
	DDX_Control(pDX, IDC_ARerouted, m_CB_ARerouted);
	DDX_Control(pDX, IDC_AREM1, m_CE_ARem1);
	DDX_Control(pDX, IDC_APSTA, m_CE_APsta);
	DDX_Control(pDX, IDC_AORG3LIST, m_CB_AOrg3List);
	DDX_Control(pDX, IDC_AONBL, m_CE_AOnbl);
	DDX_Control(pDX, IDC_AONBE, m_CE_AOnbe);
	DDX_Control(pDX, IDC_AOFBL, m_CE_AOfbl);
	DDX_Control(pDX, IDC_ANXTI, m_CE_ANxti);
	DDX_Control(pDX, IDC_ALASTCHANGE, m_CE_ALastChange);
	DDX_Control(pDX, IDC_ALASTCHANGETIME, m_CE_ALastChangeTime);
	DDX_Control(pDX, IDC_ALAND, m_CE_ALand);
	DDX_Control(pDX, IDC_AJFNOBORDER, m_CE_AJfnoBorder);
	DDX_Control(pDX, IDC_AIFRA, m_CE_AIfra);
	DDX_Control(pDX, IDC_AHTYPLIST, m_CB_AHtypList);
	DDX_Control(pDX, IDC_AHTYP, m_CE_AHtyp);
	DDX_Control(pDX, IDC_AGTA2LIST, m_CB_AGta2List);
	DDX_Control(pDX, IDC_AGTA1LIST, m_CB_AGta1List);
	DDX_Control(pDX, IDC_AGTA2, m_CE_AGta2);
	DDX_Control(pDX, IDC_AGTA1, m_CE_AGta1);
	DDX_Control(pDX, IDC_AGENTS, m_CB_Agents);
	DDX_Control(pDX, IDC_AGA2Y, m_CE_AGa2y);
	DDX_Control(pDX, IDC_AGA1Y, m_CE_AGa1y);
	DDX_Control(pDX, IDC_AGA2X, m_CE_AGa2x);
	DDX_Control(pDX, IDC_AGA1X, m_CE_AGa1x);
	DDX_Control(pDX, IDC_AEXT2, m_CE_AExt2);
	DDX_Control(pDX, IDC_AEXT1, m_CE_AExt1);
	DDX_Control(pDX, IDC_AETOA, m_CE_AEtoa);
	DDX_Control(pDX, IDC_AETAI, m_CE_AEtai);
	DDX_Control(pDX, IDC_ADTD2, m_CE_ADtd2);
	DDX_Control(pDX, IDC_ADTD1, m_CE_ADtd1);
	DDX_Control(pDX, IDC_ADiverted, m_CB_ADiverted);
	DDX_Control(pDX, IDC_ADCD2LIST, m_CB_ADcd2List);
	DDX_Control(pDX, IDC_ADCD2, m_CE_ADcd2);
	DDX_Control(pDX, IDC_ADCD1LIST, m_CB_ADcd1List);
	DDX_Control(pDX, IDC_ADCD1, m_CE_ADcd1);
	DDX_Control(pDX, IDC_ACXX, m_CB_ACxx);
	DDX_Control(pDX, IDC_ABLT2LIST, m_CB_ABlt2List);
	DDX_Control(pDX, IDC_ABLT2, m_CE_ABlt2);
	DDX_Control(pDX, IDC_ABLT1LIST, m_CB_ABlt1List);
	DDX_Control(pDX, IDC_ABLT1, m_CE_ABlt1);
	DDX_Control(pDX, IDC_AB2EA, m_CE_AB2ea);
	DDX_Control(pDX, IDC_AB2BA, m_CE_AB2ba);
	DDX_Control(pDX, IDC_AB1BA, m_CE_AB1ba);
	DDX_Control(pDX, IDC_AB1EA, m_CE_AB1ea);
	DDX_Control(pDX, IDC_AFLNS, m_CE_AFlns);
	DDX_Control(pDX, IDC_AFLTN, m_CE_AFltn);
	DDX_Control(pDX, IDC_DBLT1, m_CE_DBlt1);
	DDX_Control(pDX, IDC_DBLT1LIST, m_CB_DBlt1List);
	DDX_Control(pDX, IDC_DB1EA, m_CE_DB1ea);
	DDX_Control(pDX, IDC_DB1BA, m_CE_DB1ba);
	DDX_Control(pDX, IDC_DTMB1, m_CE_DTmb1);
	DDX_Control(pDX, IDC_AFLTI, m_CE_AFlti);
	DDX_Control(pDX, IDC_AAlc3LIST, m_CB_AAlc3List);
	DDX_Control(pDX, IDC_AAIRB, m_CE_AAirb);
	DDX_Control(pDX, IDC_ACT5, m_CE_Act5);
	DDX_Control(pDX, IDC_ACT3, m_CE_Act3);
	DDX_Control(pDX, IDC_REGN, m_CE_Regn);
	DDX_Control(pDX, IDC_ACWS, m_CE_Acws);
	DDX_Control(pDX, IDC_ACLE, m_CE_Acle);
	DDX_Text(pDX, IDC_REGN, m_Regn);
	DDX_Text(pDX, IDC_ACT3, m_Act3);
	DDX_Text(pDX, IDC_ACT5, m_Act5);
	DDX_Text(pDX, IDC_MING, m_Ming);
	DDX_Text(pDX, IDC_ACWS, m_Acws);
	DDX_Text(pDX, IDC_ACLE, m_Acle);
	DDX_Check(pDX, IDC_PAID, m_Paid);
	DDX_Text(pDX, IDC_AAIRB, m_AAirb);
	DDX_Text(pDX, IDC_AFLTN, m_AFltn);
	DDX_Text(pDX, IDC_AFLTI, m_AFlti);
	DDX_Text(pDX, IDC_AFLNS, m_AFlns);
	DDX_Text(pDX, IDC_AB1BA, m_AB1ba);
	DDX_Text(pDX, IDC_DB1BA, m_DB1ba);
	DDX_Text(pDX, IDC_AB1EA, m_AB1ea);
	DDX_Text(pDX, IDC_DB1EA, m_DB1ea);
	DDX_Text(pDX, IDC_AB2BA, m_AB2ba);
	DDX_Text(pDX, IDC_AB2EA, m_AB2ea);
	DDX_Text(pDX, IDC_ABLT1, m_ABlt1);
	DDX_Text(pDX, IDC_ABLT2, m_ABlt2);
	DDX_Text(pDX, IDC_DBLT1, m_DBlt1);
	DDX_Text(pDX, IDC_DTMB1, m_DTmb1);
	DDX_Check(pDX, IDC_ACXX, m_ACxx);
	DDX_Text(pDX, IDC_ADCD1, m_ADcd1);
	DDX_Text(pDX, IDC_ADCD2, m_ADcd2);
	DDX_Check(pDX, IDC_ADiverted, m_ADiverted);
	DDX_Check(pDX, IDC_DDiverted, m_DDiverted);
	DDX_Text(pDX, IDC_ADTD1, m_ADtd1);
	DDX_Text(pDX, IDC_ADTD2, m_ADtd2);
	DDX_Text(pDX, IDC_AETAI, m_AEtai);
	DDX_Text(pDX, IDC_AETOA, m_AEtoa);
	DDX_Text(pDX, IDC_AEXT1, m_AExt1);
	DDX_Text(pDX, IDC_AEXT2, m_AExt2);
	DDX_Text(pDX, IDC_AGA1X, m_AGa1x);
	DDX_Text(pDX, IDC_AGA2X, m_AGa2x);
	DDX_Text(pDX, IDC_AGA1Y, m_AGa1y);
	DDX_Text(pDX, IDC_AGA2Y, m_AGa2y);
	DDX_Text(pDX, IDC_AGTA1, m_AGta1);
	DDX_Text(pDX, IDC_AGTA2, m_AGta2);
	DDX_Text(pDX, IDC_AHTYP, m_AHtyp);
	DDX_Text(pDX, IDC_AIFRA, m_AIfra);
	DDX_Text(pDX, IDC_ALAND, m_ALand);
	DDX_Text(pDX, IDC_ALASTCHANGE, m_ALastChange);
	DDX_Text(pDX, IDC_ALASTCHANGETIME, m_ALastChangeTime);
	DDX_Text(pDX, IDC_ANXTI, m_ANxti);
	DDX_Text(pDX, IDC_AOFBL, m_AOfbl);
	DDX_Text(pDX, IDC_AONBE, m_AOnbe);
	DDX_Text(pDX, IDC_AONBL, m_AOnbl);
	DDX_Text(pDX, IDC_APSTA, m_APsta);
	DDX_Text(pDX, IDC_AREM1, m_ARem1);
	DDX_Text(pDX, IDC_AREM3, m_ARem3);
	DDX_Text(pDX, IDC_DREM3, m_DRem3);
	DDX_Text(pDX, IDC_ABAS1, m_ABas1);
	DDX_Text(pDX, IDC_ABAE1, m_ABae1);
	DDX_Text(pDX, IDC_ABAS2, m_ABas2);
	DDX_Text(pDX, IDC_ABAE2, m_ABae2);
	DDX_Check(pDX, IDC_AVIP, m_AVip);
	DDX_Check(pDX, IDC_DVIP, m_DVip);
	DDX_Check(pDX, IDC_AFPSA, m_AFpsa);
	DDX_Check(pDX, IDC_DFPSD, m_DFpsd);
	DDX_Check(pDX, IDC_ARerouted, m_ARerouted);
	DDX_Check(pDX, IDC_DRerouted, m_DRerouted);
	DDX_Text(pDX, IDC_ARWYA, m_ARwya);
	DDX_Text(pDX, IDC_ASTEV, m_AStev);
	DDX_Text(pDX, IDC_ASTE2, m_ASte2);
	DDX_Text(pDX, IDC_ASTE3, m_ASte3);
	DDX_Text(pDX, IDC_ASTE4, m_ASte4);
	DDX_Text(pDX, IDC_ASTOA, m_AStoa);
	DDX_Text(pDX, IDC_ASTOD, m_AStod);
	DDX_Text(pDX, IDC_ASTYP, m_AStyp);
	DDX_Text(pDX, IDC_ATGA1, m_ATga1);
	DDX_Text(pDX, IDC_ATGA2, m_ATga2);
	DDX_Text(pDX, IDC_ATMB1, m_ATmb1);
	DDX_Text(pDX, IDC_ATMB2, m_ATmb2);
	DDX_Text(pDX, IDC_ATMOA, m_ATmoa);
	DDX_Text(pDX, IDC_ATTYP, m_ATtyp);
	DDX_Text(pDX, IDC_DAIRB, m_DAirb);
	DDX_Text(pDX, IDC_DALC3, m_DAlc3);
	DDX_Check(pDX, IDC_DCXX, m_DCxx);
	DDX_Text(pDX, IDC_DDCD1, m_DDcd1);
	DDX_Text(pDX, IDC_DDCD2, m_DDcd2);
	DDX_Text(pDX, IDC_DDES3, m_DDes3);
	DDX_Text(pDX, IDC_DDES4, m_DDes4);
	DDX_Text(pDX, IDC_DDTD1, m_DDtd1);
	DDX_Text(pDX, IDC_DDTD2, m_DDtd2);
	DDX_Text(pDX, IDC_DETDC, m_DEtdc);
	DDX_Text(pDX, IDC_DETDI, m_DEtdi);
	DDX_Text(pDX, IDC_DETOD, m_DEtod);
	DDX_Text(pDX, IDC_DFLNS, m_DFlns);
	DDX_Text(pDX, IDC_DGD1X, m_DGd1x);
	DDX_Text(pDX, IDC_DGD1Y, m_DGd1y);
	DDX_Text(pDX, IDC_DGD2X, m_DGd2x);
	DDX_Text(pDX, IDC_DGD2Y, m_DGd2y);
	DDX_Text(pDX, IDC_DGTD1, m_DGtd1);
	DDX_Text(pDX, IDC_DGTD2, m_DGtd2);
	DDX_Text(pDX, IDC_DHTYP, m_DHtyp);
	DDX_Text(pDX, IDC_DIFRD, m_DIfrd);
	DDX_Text(pDX, IDC_DISKD, m_DIskd);
	DDX_Text(pDX, IDC_DLASTCHANGE, m_DLastChange);
	DDX_Text(pDX, IDC_DLASTCHANGETIME, m_DLastChangeTime);
	DDX_Text(pDX, IDC_DNXTI, m_DNxti);
	DDX_Text(pDX, IDC_DOFBL, m_DOfbl);
	DDX_Text(pDX, IDC_DPSTD, m_DPstd);
	DDX_Text(pDX, IDC_DREM1, m_DRem1);
	DDX_Text(pDX, IDC_DRWYD, m_DRwyd);
	DDX_Text(pDX, IDC_DSLOT, m_DSlot);
	DDX_Text(pDX, IDC_DSTEV, m_DStev);
	DDX_Text(pDX, IDC_DSTE2, m_DSte2);
	DDX_Text(pDX, IDC_DSTE3, m_DSte3);
	DDX_Text(pDX, IDC_DSTE4, m_DSte4);
	DDX_Text(pDX, IDC_DSTOA, m_DStoa);
	DDX_Text(pDX, IDC_DSTOD, m_DStod);
	DDX_Text(pDX, IDC_DSTYP, m_DStyp);
	DDX_Text(pDX, IDC_DTGD1, m_DTgd1);
	DDX_Text(pDX, IDC_DTGD2, m_DTgd2);
	DDX_Text(pDX, IDC_DTTYP, m_DTtyp);
	DDX_Text(pDX, IDC_DTWR1, m_DTwr1);
	DDX_Text(pDX, IDC_DWRO1, m_DWro1);
	DDX_Text(pDX, IDC_ADES3, m_ADes3);
	DDX_Text(pDX, IDC_AORG3, m_AOrg3);
	DDX_Text(pDX, IDC_AORG3L, m_AOrg3L);
	DDX_Text(pDX, IDC_ATET1, m_ATet1);
	DDX_Text(pDX, IDC_ATET2, m_ATet2);
	DDX_Text(pDX, IDC_DW1BA, m_DW1ba);
	DDX_Text(pDX, IDC_DW1EA, m_DW1ea);
	DDX_Text(pDX, IDC_DORG3, m_DOrg3);
	DDX_Text(pDX, IDC_AALC3, m_AAlc3);
	DDX_Text(pDX, IDC_DFLTN, m_DFltn);
	DDX_Text(pDX, IDC_DFLTI, m_DFlti);
	DDX_Text(pDX, IDC_ACSGN, m_ACsgn);
	DDX_Text(pDX, IDC_DCSGN, m_DCsgn);
	DDX_Text(pDX, IDC_ABBAA, m_ABbaa);
	DDX_Text(pDX, IDC_DBBFA, m_DBbfa);
	DDX_Text(pDX, IDC_DBAZ1, m_DBaz1);
	DDX_Text(pDX, IDC_DBAZ4, m_DBaz4);
	DDX_Text(pDX, IDC_DBAO1, m_DBao1);
	DDX_Text(pDX, IDC_DBAC1, m_DBac1);
	DDX_Text(pDX, IDC_DBAO4, m_DBao4);
	DDX_Text(pDX, IDC_DBAC4, m_DBac4);
	DDX_Text(pDX, IDC_ADIVR, m_ADivr);
	DDX_Text(pDX, IDC_DDIVR, m_DDivr);
	DDX_Control(pDX, IDC_AETDI, m_CE_AEtdi);
	DDX_Text(pDX, IDC_DTOBT, m_DTobt);
	DDX_Text(pDX, IDC_DTSAT, m_DTsat);
	DDX_Text(pDX, IDC_AETDI, m_AEtdi);
	DDX_Control(pDX, IDC_DWRO3LIST, m_CB_DWro3List);
	DDX_Control(pDX, IDC_DWRO3, m_CE_DWro3);
	DDX_Text(pDX, IDC_DWRO3, m_DWro3);
	DDX_Control(pDX, IDC_DTWR3, m_CE_DTwr3);
	DDX_Text(pDX, IDC_DTWR3, m_DTwr3);
	DDX_Control(pDX, IDC_DW3BA, m_CE_DW3ba);
	DDX_Text(pDX, IDC_DW3BA, m_DW3ba);
	DDX_Control(pDX, IDC_DW3EA, m_CE_DW3ea);
	DDX_Text(pDX, IDC_DW3EA, m_DW3ea);
	DDX_Text(pDX, IDC_CIFR_CITO, m_DCiFr);
	DDX_Control(pDX, IDC_CIFR_CITO, m_CE_DCiFr);
	DDX_Control(pDX, IDC_CHU_FR_TO, m_CE_ChuFrTo);
	DDX_Control(pDX, IDC_LOAD_VIEW_ARR, m_CB_Load_View_Arr);
	DDX_Control(pDX, IDC_LOAD_VIEW_DEP, m_CB_Load_View_Dep);
	DDX_Control(pDX, IDC_APCA, m_CB_APca);
	DDX_Control(pDX, IDC_DPCA, m_CB_DPca);
	DDX_Control(pDX, IDC_APLB, m_CB_APlb);
	DDX_Control(pDX, IDC_DPLB, m_CB_DPlb);
	DDX_Control(pDX, IDC_DCINSBORDEREXT, m_CE_DCinsBorderExt);
	DDX_Control(pDX, IDC_COMBO_AHISTORY, m_ComboAHistory);
	DDX_Control(pDX, IDC_COMBO_DHISTORY, m_ComboDHistory);
	DDX_Control(pDX, IDC_COMBO_CCA_XN, m_ComboDcins);
	DDX_Control(pDX, IDC_DISKD2, m_CS_DISKD);
	DDX_Control(pDX, IDC_GOAROUND, m_CB_GoAround);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(RotationDlg, CDialog)
	//{{AFX_MSG_MAP(RotationDlg)
	ON_BN_CLICKED(IDC_AVIP, OnAVip)
	ON_BN_CLICKED(IDC_DVIP, OnDVip)
	ON_BN_CLICKED(IDC_AFPSA, OnAFpsa)
	ON_BN_CLICKED(IDC_DFPSD, OnDFpsd)
	ON_BN_CLICKED(IDC_ACBNFES, OnANfes)
	ON_BN_CLICKED(IDC_DCBNFES, OnDNfes)
	ON_BN_CLICKED(IDC_FL_REPORT_ARR, OnARep)
	ON_BN_CLICKED(IDC_FL_REPORT_DEP, OnDRep)
	ON_BN_CLICKED(IDC_ARRPERMITSBUTTON, onArrPermitsButton)
	ON_BN_CLICKED(IDC_DEPPERMITSBUTTON, onDepPermitsButton)
	ON_BN_CLICKED(IDC_DDELAY, OnDDelay)
	ON_BN_CLICKED(IDC_NEWVIPA, OnVipADlg)
	ON_BN_CLICKED(IDC_NEWVIPD, OnVipDDlg)
	ON_BN_CLICKED(IDC_LOCK, OnLock)
	ON_BN_CLICKED(IDC_SEASON_MASK, OnSeasonMask)
	ON_BN_CLICKED(IDC_ASHOWJFNO, OnAshowjfno)
	ON_BN_CLICKED(IDC_DSHOWJFNO, OnDshowjfno)
	ON_BN_CLICKED(IDC_SPLIT, OnSplit)
	ON_BN_CLICKED(IDC_JOIN, OnJoin)
	ON_BN_CLICKED(IDC_PAID, OnPaid)
	ON_MESSAGE(WM_EDIT_KILLFOCUS, OnEditKillfocus)
	ON_MESSAGE(WM_EDIT_CHANGED, OnEditChanged)
	ON_MESSAGE(WM_TABLE_IPEDIT_KILLFOCUS, OnTableIPEditKillfocus)
	ON_BN_CLICKED(IDC_AAlc3LIST, OnAAlc3LIST)
	ON_BN_CLICKED(IDC_AAlc3LIST2, OnAAlc3LIST2)
	ON_BN_CLICKED(IDC_AAlc3LIST3, OnAAlc3LIST3)
	ON_BN_CLICKED(IDC_ABLT1LIST, OnAblt1list)
	ON_BN_CLICKED(IDC_ABLT2LIST, OnAblt2list)
	ON_BN_CLICKED(IDC_DBLT1LIST, OnDblt1list)
	ON_BN_CLICKED(IDC_ADCD1LIST, OnAdcd1list)
	ON_BN_CLICKED(IDC_ADCD2LIST, OnAdcd2list)
	ON_BN_CLICKED(IDC_AGTA1LIST, OnAgta1list)
	ON_BN_CLICKED(IDC_AGTA2LIST, OnAgta2list)
	ON_BN_CLICKED(IDC_AHTYPLIST, OnAhtyplist)
	ON_BN_CLICKED(IDC_AORG3LIST, OnAorg3list)
	ON_BN_CLICKED(IDC_ASTYPLIST, OnAstyplist)
	ON_BN_CLICKED(IDC_ATTYPLIST, OnAttyplist)
	ON_BN_CLICKED(IDC_DALC3LIST, OnDalc3list)
	ON_BN_CLICKED(IDC_DALC3LIST2, OnDalc3list2)
	ON_BN_CLICKED(IDC_DALC3LIST3, OnDalc3list3)
	ON_BN_CLICKED(IDC_DCINSLIST1, OnDcinslist1)
	ON_BN_CLICKED(IDC_DCINSLIST2, OnDcinslist2)
	ON_BN_CLICKED(IDC_DDCD1LIST, OnDdcd1list)
	ON_BN_CLICKED(IDC_DDCD2LIST, OnDdcd2list)
	ON_BN_CLICKED(IDC_DDES3LIST, OnDdes3list)
	ON_BN_CLICKED(IDC_DGTA1LIST, OnDgta1list)
	ON_BN_CLICKED(IDC_DGTA2LIST, OnDgta2list)
	ON_BN_CLICKED(IDC_DHTYPLIST, OnDhtyplist)
	ON_BN_CLICKED(IDC_DSTYPLIST, OnDstyplist)
	ON_BN_CLICKED(IDC_DTTYPLIST, OnDttyplist)
	ON_BN_CLICKED(IDC_DWRO1LIST, OnDwro1list)
	ON_BN_CLICKED(IDC_DPSTDLIST, OnDpstdlist)
	ON_BN_CLICKED(IDC_APSTALIST, OnApstalist)
	ON_BN_CLICKED(IDC_DCINSLIST3, OnDcinslist3)
	ON_BN_CLICKED(IDC_DCINSLIST4, OnDcinslist4)
	ON_BN_CLICKED(IDC_AEXT1LIST, OnAext1list)
	ON_BN_CLICKED(IDC_AEXT2LIST, OnAext2list)
	ON_BN_CLICKED(IDC_ACT35LIST, OnAct35list)
	ON_BN_CLICKED(IDC_DCICLOGO1, OnDcicLogo1)
	ON_BN_CLICKED(IDC_DCICLOGO2, OnDcicLogo2)
	ON_BN_CLICKED(IDC_DCICLOGO3, OnDcicLogo3)
	ON_BN_CLICKED(IDC_DCICLOGO4, OnDcicLogo4)
	ON_BN_CLICKED(IDC_DCXX, OnDcxx)
	ON_BN_CLICKED(IDC_ACXX, OnAcxx)
	ON_BN_CLICKED(IDC_DGD2D, OnDgd2d)
	ON_BN_CLICKED(IDC_ADiverted, OnADiverted)
	ON_BN_CLICKED(IDC_DDiverted, OnDDiverted)
	ON_BN_CLICKED(IDC_ARerouted, OnARerouted)
	ON_BN_CLICKED(IDC_DRerouted, OnDRerouted)
	ON_CBN_SELCHANGE(IDC_DGATLOGO, OnSelchangeDGatLogo)
	ON_CBN_SELCHANGE(IDC_DGATLOGO2, OnSelchangeDGatLogo)
	ON_CBN_SELCHANGE(IDC_DREMP, OnSelchangeDremp)
	ON_CBN_SELCHANGE(IDC_AREMP, OnSelchangeAremp)
	ON_CBN_SELCHANGE(IDC_BTCRC_RGD1, OnSelchangeAremp)
	ON_CBN_SELCHANGE(IDC_BTCRC_RGD2, OnSelchangeAremp)
	ON_BN_CLICKED(IDC_CLB_DEP, OnCkeckList_Dep)
	ON_BN_CLICKED(IDC_DTelex, OnDTelex)
	ON_BN_CLICKED(IDC_ATelex, OnATelex)
	ON_WM_KEYDOWN()
	ON_BN_CLICKED(IDC_CLB_ARR, OnCkeckList_Arr)
	ON_BN_CLICKED(IDC_AGENTS, OnAgent)
	ON_BN_CLICKED(IDC_OBJECT, OnObject)
	ON_BN_CLICKED(IDC_TOWING, OnTowing)
	ON_BN_CLICKED(IDC_REQ, OnREQ)
	ON_BN_CLICKED(IDC_ARETFLIGHT, OnAretflight)
	ON_BN_CLICKED(IDC_ARETTAXI, OnArettaxi)
	ON_BN_CLICKED(IDC_DRETFLIGHT, OnDretflight)
	ON_BN_CLICKED(IDC_DRETTAXI, OnDrettaxi)
	ON_BN_CLICKED(IDC_DCICREM1, OnDcicrem1)
	ON_BN_CLICKED(IDC_DCICREM2, OnDcicrem2)
	ON_BN_CLICKED(IDC_DCICREM3, OnDcicrem3)
	ON_BN_CLICKED(IDC_DCICREM4, OnDcicrem4)
	ON_BN_CLICKED(IDC_DCICCS1, OnDcicCodeShare1)
	ON_BN_CLICKED(IDC_DCICCS2, OnDcicCodeShare2)
	ON_BN_CLICKED(IDC_DCICCS3, OnDcicCodeShare3)
	ON_BN_CLICKED(IDC_DCICCS4, OnDcicCodeShare4)
	ON_BN_CLICKED(IDC_AGPU, OnAgpu)
	ON_BN_CLICKED(IDC_DGPU, OnDgpu)
	ON_BN_CLICKED(IDC_ALOAD, OnAload)
	ON_BN_CLICKED(IDC_DLOAD, OnDload)
	ON_BN_CLICKED(IDC_ALOG, OnALog)
	ON_BN_CLICKED(IDC_DLOG, OnDLog)
	ON_MESSAGE(WM_EDIT_RBUTTONDOWN, OnEditRButtonDown)
	ON_MESSAGE(WM_TABLE_IPEDIT, OnTableIPEdit)
	ON_BN_CLICKED(IDC_DWRO3LIST, OnDwro3list)
	ON_BN_CLICKED(IDC_LOAD_VIEW_ARR, OnAViewLoad)
	ON_BN_CLICKED(IDC_LOAD_VIEW_DEP, OnDViewLoad)
	ON_MESSAGE(WM_VIA, OnViaChanged)
	ON_BN_CLICKED(IDC_AVIA, OnAVia)
	ON_BN_CLICKED(IDC_DELAYBTN_ARRIVAL, OnDelaybtnArrival)
	ON_BN_CLICKED(IDC_DELAYBTN_DEPARTURE, OnDelaybtnDeparture)
	ON_BN_CLICKED(IDC_DVIA, OnDVia)
	ON_NOTIFY_EX( TTN_NEEDTEXT, 0, OnToolTipNotify )
	ON_WM_SIZE()
	ON_BN_CLICKED(IDC_APCA, OnAPca)
	ON_BN_CLICKED(IDC_DPCA, OnDPca)
	ON_BN_CLICKED(IDC_APLB, OnAPlb)
	ON_BN_CLICKED(IDC_DPLB, OnDPlb)
	ON_MESSAGE(WM_EDIT_LBUTTONDBLCLK, OnEditDbClk)

	ON_CBN_SELCHANGE(IDC_COMBO_AHISTORY, OnSelchangeAHistory)
	ON_CBN_SELCHANGE(IDC_COMBO_DHISTORY, OnSelchangeDHistory)
	ON_WM_RBUTTONDOWN()
	ON_CBN_SELCHANGE(IDC_COMBO_CCA_XN, OnSelchangeDcins)
	ON_BN_CLICKED(IDC_BTCRC_PSTA,  OnClickCrcPsta)  //PRF 8379 
	ON_BN_CLICKED(IDC_BTCRC_AGTA1, OnClickCrcAGta1) 
	ON_BN_CLICKED(IDC_BTCRC_AGTA2, OnClickCrcAGta2) 
	ON_BN_CLICKED(IDC_BTCRC_RESOA, OnClickCrcResoA) 
	ON_BN_CLICKED(IDC_BTCRC_PSTD,  OnClickCrcPstd) 
	ON_BN_CLICKED(IDC_BTCRC_DGTA1, OnClickCrcDGtd1) 
	ON_BN_CLICKED(IDC_BTCRC_DGTA2, OnClickCrcDGtd2) 
	ON_BN_CLICKED(IDC_BTCRC_RESOD, OnClickCrcResoD) 
	ON_BN_CLICKED(IDC_BTCRC_ABLT1, OnClickCrcABlt1) 
	ON_BN_CLICKED(IDC_BTCRC_ABLT2, OnClickCrcABlt2)
	ON_BN_CLICKED(IDC_BTCRC_DATATOOL, OnClickCrcDataTool)
	ON_CBN_SELCHANGE(IDC_ACXXREASON, OnSelchangeACxxReason)
	ON_CBN_SELCHANGE(IDC_DCXXREASON, OnSelchangeDCxxReason)
	ON_BN_CLICKED(IDC_GOAROUND, OnGoAround)
	ON_BN_CLICKED(IDC_BTCRC_CASH_ARR, OnCashArr)
	ON_BN_CLICKED(IDC_BTCRC_CASH_DEP, OnCashDep)
	ON_BN_CLICKED(IDC_CBFPSA, OnFixedAllocaton) //For Fixed Allocation - PRF 8405
	ON_BN_CLICKED(IDC_CBFGA1, OnFixedAllocaton) 
	ON_BN_CLICKED(IDC_CBFGA2, OnFixedAllocaton) 
	ON_BN_CLICKED(IDC_CBFBL1, OnFixedAllocaton) 
	ON_BN_CLICKED(IDC_CBFBL2, OnFixedAllocaton) 
	ON_BN_CLICKED(IDC_CBFPSD, OnFixedAllocaton) 
	ON_BN_CLICKED(IDC_CBFGD1, OnFixedAllocaton) 
	ON_BN_CLICKED(IDC_CBFGD2, OnFixedAllocaton) 
	ON_MESSAGE(WM_EDIT_CHANGED, OnEditChanged)
	ON_BN_CLICKED(IDC_BTCRC_ACTIVITY, OnClickActivities)
	ON_BN_CLICKED(IDC_BTCRC_PAY_ARR, OnAPay)
	ON_BN_CLICKED(IDC_BTCRC_PAY_DEP, OnDPay)
	ON_BN_CLICKED(IDC_CKI_4, OnDcinsmal4)
	ON_BN_CLICKED(IDC_BTCRC_ARR_AOG, OnAAog)
	ON_BN_CLICKED(IDC_BTCRC_DEP_AOG, OnDAog)
	ON_BN_CLICKED(IDC_BTCRC_ARR_ADHOC, OnAAdhoc)
	ON_BN_CLICKED(IDC_BTCRC_DEP_ADHOC, OnDAdhoc)
	ON_BN_CLICKED(IDC_BTCRC_EFPS, OnEFPS)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// RotationDlg message handlers


void RotationDlg::ClearAll()
{
	bmIsAFfnoShown = false;
	bmIsDFfnoShown = false;

	
	omReasons.blAGta1Resf = false;
	omReasons.blAGta2Resf = false;
	omReasons.blDGtd1Resf = false;
	omReasons.blDGtd1Resf = false;
	omReasons.blPstaResf  = false;
	omReasons.blPstdResf  = false;
	omReasons.blABlt1Resf = false; 
	omReasons.blABlt2Resf = false;

	bmInit = false;

	ogRotationDlgFlights.ClearAll();
	omCcaData.ClearAll();
	
	ogDdx.UnRegister(this,NOTUSED);

	ROTATIONDLGFLIGHTDATA *prlFlight = new ROTATIONDLGFLIGHTDATA;	

	*prmAFlight = *prlFlight;
	*prmAFlightSave = *prlFlight;
	*prmDFlight = *prlFlight;
	*prmDFlightSave = *prlFlight;
	
	delete prlFlight;

	omAJfno.DeleteAll();
	omDJfno.DeleteAll();
	omDCins.DeleteAll();
	omDCinsSave.DeleteAll();
}



BOOL RotationDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

     EnableToolTips(TRUE);
	m_key = "DialogPosition\\DailyDialog";


	InitTables();	
	SetSecState();

	//Init editfields
	
	if (bgShowWingspan)
	{
		m_CE_Acws.ShowWindow(SW_SHOW);
		m_CE_Acle.ShowWindow(SW_SHOW);
		m_CE_Acws.EnableWindow(false);
		m_CE_Acle.EnableWindow(false);
	} else
	{
		m_CE_Acws.ShowWindow(SW_HIDE);
		m_CE_Acle.ShowWindow(SW_HIDE);
		CWnd* polWnd = GetDlgItem(IDC_ACLE_LABEL); 
		if (polWnd)
			polWnd->ShowWindow(SW_HIDE);
		polWnd = GetDlgItem(IDC_ACWS_LABEL); 
		if (polWnd)
			polWnd->ShowWindow(SW_HIDE);

	}

	/*
	if(bgShowVIPNearBridge)
	{

		CRect olRect;
		m_CE_APsta.GetWindowRect( olRect );
		
		ScreenToClient(olRect);	
		olRect.top = olRect.top ; 
		olRect.left = olRect.left + 50; 
		olRect.bottom = olRect.bottom; 
		olRect.right = olRect.right + 50; 
		m_CB_AVip.MoveWindow(olRect);

		if (m_CB_Ok.m_hWnd)
		{
			m_CB_AVip.SetFont(m_CB_Ok.GetFont());
		}

		m_CE_DPstd.GetWindowRect( olRect );
		ScreenToClient(olRect);

		olRect.top = olRect.top ; 
		olRect.left = olRect.left + 50; 
		olRect.bottom = olRect.bottom; 
		olRect.right = olRect.right + 50; 
		m_CB_DVip.MoveWindow(olRect);

		if (m_CB_Ok.m_hWnd)
		{
			m_CB_DVip.SetFont(m_CB_Ok.GetFont());
		}
		
	}

  */
	if (strcmp(pcgHome, "ATH") != 0)
	{
		m_CB_GoAround.ShowWindow(SW_HIDE);
	}


	if(bgVIPFlag)
	{
		m_CB_AVip.ShowWindow(SW_SHOW);
		m_CB_DVip.ShowWindow(SW_SHOW);
	}
	else
	{
		m_CB_AVip.ShowWindow(SW_HIDE);
		m_CB_DVip.ShowWindow(SW_HIDE);
	}

	if(bgFixedRes)
	{
		m_CB_AFpsa.ShowWindow(SW_SHOW);
		m_CB_DFpsd.ShowWindow(SW_SHOW);
	}
	else
	{
		m_CB_AFpsa.ShowWindow(SW_HIDE);
		m_CB_DFpsd.ShowWindow(SW_HIDE);
	}



	m_CE_DCinsBorder.SetReadOnly();
	m_CE_DChuBorder.SetReadOnly();
	m_CE_ADes3.SetReadOnly();
	m_CE_DOrg3.SetReadOnly();
	m_CE_AOnbe.SetReadOnly();
	m_CE_ALastChange.SetReadOnly();
	m_CE_ALastChangeTime.SetReadOnly();
	m_CE_DLastChange.SetReadOnly();
	m_CE_DLastChangeTime.SetReadOnly();
	m_CE_AJfnoBorder.SetReadOnly();
	m_CE_DJfnoBorder.SetReadOnly();
	m_CE_DCiFr.SetReadOnly();

	m_CE_ChuFrTo.SetReadOnly();


	m_CE_ABas1.SetReadOnly();
	m_CE_ABae1.SetReadOnly();
	m_CE_ABas2.SetReadOnly();
	m_CE_ABae2.SetReadOnly();


	if(!bgFLBag)
	{
		m_CE_ABas1.ShowWindow(SW_HIDE);
		m_CE_ABae1.ShowWindow(SW_HIDE);
		m_CE_ABas2.ShowWindow(SW_HIDE);
		m_CE_ABae2.ShowWindow(SW_HIDE);


		CWnd* polWnd = GetDlgItem(IDC_FLBAG); 
		if (polWnd)
			polWnd->ShowWindow(SW_HIDE);


	}

	if(!bgChuteDisplay)
	{
		m_CE_ChuFrTo.ShowWindow(SW_HIDE);

		CWnd* polWnd = GetDlgItem(IDC_LB_CHUTE); 
		if (polWnd)
			polWnd->ShowWindow(SW_HIDE);

	}

	m_CE_AStoa.SetBKColor(YELLOW);
	m_CE_AOrg3.SetBKColor(YELLOW);
	/* //UFIS-1097 - Commented Out - Start
	if(bgNatureAutoCalc)
		m_CE_AStyp.SetBKColor(YELLOW);
	else
		m_CE_ATtyp.SetBKColor(YELLOW);
	*/ //UFIS-1097 - Commented Out - End

	if(bgNatureServiceMand)
	{
		m_CE_AStyp.SetBKColor(YELLOW);
		m_CE_ATtyp.SetBKColor(YELLOW);
	}
	else if(bgNatureAutoCalc)//UFIS-1097 - Start
	{
		m_CE_AStyp.SetBKColor(YELLOW);
	}
	else
	{
		if (bgServiceMandatory)
		{
			m_CE_AStyp.SetBKColor(YELLOW);
		}
		if (bgNatureMandatory)
		{
			m_CE_ATtyp.SetBKColor(YELLOW);
		}		
	}//UFIS-1097 - End



	m_CE_DStod.SetBKColor(YELLOW);
//	m_CE_DDes3.SetBKColor(YELLOW);
	m_CE_DDes4.SetBKColor(YELLOW);

	/*//UFIS-1097 - Commented Out - Start
	if(bgNatureAutoCalc)
		m_CE_DStyp.SetBKColor(YELLOW);
	else
		m_CE_DTtyp.SetBKColor(YELLOW); 
	*///UFIS-1097 - Commented Out - End
	
	if(bgNatureServiceMand)
	{
		m_CE_DStyp.SetBKColor(YELLOW);
		m_CE_DTtyp.SetBKColor(YELLOW); 
	}
	else if(bgNatureAutoCalc)//UFIS-1097 - Start
	{
		m_CE_DStyp.SetBKColor(YELLOW);
	}
	else
	{
		if (bgServiceMandatory)
		{
			m_CE_DStyp.SetBKColor(YELLOW);
		}
		if (bgNatureMandatory)
		{
			m_CE_DTtyp.SetBKColor(YELLOW);
		}		
	}//UFIS-1097 - End

	m_CE_Ming.SetTypeToString("###",3,0);
	m_CE_Act3.SetTypeToString("XXX",3,0);
//	m_CE_Act3.SetBKColor(YELLOW);
	m_CE_Act5.SetTypeToString("XXXXX",5,0);
	m_CE_Act5.SetBKColor(YELLOW);
	
	//Ankunft
	//
/*	//pca must create a button because dialog if full!!!!
	if(!pomAPca)
	{
		pomAPca = new CButton;
		pomAPca->Create( GetString(IDS_STRING2662), SW_HIDE | WS_CHILD , CRect(0,0,0,0), this, IDC_APCA ); 
	}
	if(pomAPca)
	{
		pomAPca->SetWindowPos( NULL, 0, 0, 20, 14,  SWP_NOZORDER );
		CRect olRect;
		m_CB_AGpu.GetWindowRect( olRect );
		ScreenToClient(olRect);
//		int delta = olRect.Width();
		olRect.top = olRect.bottom + 2; 
		olRect.bottom = olRect.top + 14; 


		pomAPca->MoveWindow(olRect);
		pomAPca->ShowWindow(SW_SHOW);
	}
	//
	//plb must create a button because dialog if full!!!!
	if(!pomAPlb)
	{
		pomAPlb = new CButton;
		pomAPlb->Create( GetString(IDS_STRING2663), SW_HIDE | WS_CHILD , CRect(0,0,0,0), this, IDC_APLB ); 
	}
	if(pomAPlb)
	{
		pomAPlb->SetWindowPos( NULL, 0, 0, 20, 14,  SWP_NOZORDER );
		CRect olRect;
		pomAPca->GetWindowRect( olRect );
		ScreenToClient(olRect);
//		int delta = olRect.Width();
		olRect.left = olRect.right + 2; 
		olRect.right = olRect.left + 20; 


		pomAPlb->MoveWindow(olRect);
		pomAPlb->ShowWindow(SW_SHOW);
	}
*/

	//vias must create a button because dialog if full!!!!
	if(!pomAVia)
	{
		pomAVia = new CCSButtonCtrl;
		pomAVia->Create( GetString(IDS_STRING299), SW_HIDE | WS_CHILD | BS_OWNERDRAW | WS_TABSTOP, CRect(0,0,0,0), this, IDC_AVIA ); 
	}

	CRect olRect;
	CRect olRect1;
	m_CE_AOrg3.GetWindowRect( olRect );
	m_CE_ADes3.GetWindowRect( olRect1 );
	ScreenToClient(olRect);
	ScreenToClient(olRect1);

	olRect.top -= 4;
	
	int delta = (olRect1.left - olRect.right)/2;
	olRect.left = olRect.right + delta -45 - 20; 
	olRect.right = olRect.left + 90 -20; 
	olRect.bottom = olRect.top + 23; 

	if(pomAVia)
	{
		pomAVia->SetWindowPos( NULL, 0, 0, 90, 23,  SWP_NOZORDER );
		pomAVia->MoveWindow(olRect);

		if (m_CB_Ok.m_hWnd)
			pomAVia->SetFont(m_CB_Ok.GetFont());

		pomAVia->ShowWindow(SW_SHOW);

		//if(ogPrivList.GetStat("ROTATIONDLG_CB_AShowVia") == '-')					// TLE: UFIS 3768 
			pomAVia->SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_AShowVia"));
	}

/*
//	pomAViaCtrl = new ViaTableCtrl(this,"APC4,FIDS,STOA,STOD");
	pomAViaCtrl = new ViaTableCtrl(this,"HEAD,APC4,FIDS,STOA,ETA,ATA,STOD,ETD,ATD");
	
	pomAViaCtrl->SetSecStat(ogPrivList.GetStat("ROTATIONDLG_CB_AShowVia"));


	m_CE_AOrg3.GetWindowRect( olRect );

	ScreenToClient(olRect);

	//SetWindowPos( NULL, 0, 0, 268, 35,  SWP_NOZORDER );

	olRect.top = olRect.top-20;
	olRect.bottom = 0;
	olRect.left = olRect.right + 40;
	olRect.right = 0;


	pomAViaCtrl->SetPosition(olRect);
*/

	m_CE_AAlc3.SetTypeToString("X|#X|#X|#",3,2);
	m_CE_AFltn.SetTypeToString("#####",5,2);
	m_CE_AFlns.SetTypeToString("X",1,0);
	m_CE_AFlti.SetTypeToString("A",1,0);

	m_CE_ACsgn.SetTypeToString("X|#X|#X|#X|#X|#X|#X|#X|#",8,0);
	
	/*//UFIS-1097 - Commented Out - Start
	if(bgNatureAutoCalc)
	{
		m_CE_ATtyp.SetTypeToString("XXXXX",5,0);
		m_CE_AStyp.SetTypeToString("XX",2,1);
	}
	else
	{
		m_CE_ATtyp.SetTypeToString("XXXXX",5,1);
		m_CE_AStyp.SetTypeToString("XX",2,0);
	}
	*///UFIS-1097 - Commented Out - End

	if(bgNatureServiceMand)
	{
		m_CE_ATtyp.SetTypeToString("XXXXX",5,1);
		m_CE_AStyp.SetTypeToString("XX",2,1);
	}
	else if(bgNatureAutoCalc) //UFIS-1097 - Start
	{
		m_CE_ATtyp.SetTypeToString("XXXXX",5,0);
		m_CE_AStyp.SetTypeToString("XX",2,1);
	}
	else
	{
		m_CE_ATtyp.SetTypeToString("XXXXX",5,0);
		m_CE_AStyp.SetTypeToString("XX",2,0);
		if (bgServiceMandatory)
		{
			m_CE_AStyp.SetTypeToString("XX",2,1);
		}
		if(bgNatureMandatory)
		{
			m_CE_ATtyp.SetTypeToString("XXXXX",5,1);
		}
	} //UFIS-1097 - Start


	m_CE_AHtyp.SetTypeToString("XX",2,0);
	
	CString olTmp;	
	olTmp.Format("X(%d)", igLengthStev);
	m_CE_AStev.SetTypeToString(olTmp,igLengthStev,0);
	m_CE_ASte2.SetTypeToString("XXX",3,0);
	m_CE_ASte3.SetTypeToString("XXX",3,0);
	m_CE_ASte4.SetTypeToString("XXX",3,0);

	m_CE_AOrg3.SetTypeToString("XXXX",4,0);
	m_CE_AOrg3L.SetTypeToString("XXX",3,0);
	m_CE_AStoa.SetTypeToTime(true, true);
	m_CE_AStod.SetTypeToTime(false, true);

	m_CE_AOfbl.SetTypeToTime(false, true);
	m_CE_AOnbl.SetTypeToTime(false, true);
	m_CE_AAirb.SetTypeToTime(false, true);
	m_CE_ALand.SetTypeToTime(false, true);
	
	m_CE_AEtai.SetTypeToTime(false, true);
	m_CE_AEtoa.SetTypeToTime(false, true);
	//m_CE_AOnbe.SetTypeToTime(false, true);
	m_CE_ANxti.SetTypeToTime(false, true);
	m_CE_ATmoa.SetTypeToTime(false, true);

//	m_CE_ARwya.SetTypeToString("XXXX");
	m_CE_ARwya.SetTypeToString("x|#x|#x|#x|#",4,0);
	m_CE_AIfra.SetTypeToString("X",1,0);


	m_CE_ADtd1.SetTypeToString("####",4,0);
	m_CE_ADtd2.SetTypeToString("####",4,0);
	m_CE_ADcd1.SetTypeToString("XX",2,0);
	m_CE_ADcd2.SetTypeToString("XX",2,0);

	


	m_CE_APsta.SetTypeToString("x|#x|#x|#x|#x|#",5,0);
//	m_CE_APaba.SetTypeToTime(false, true);
	m_CE_AEtdi.SetTypeToTime(false, true);
//	m_CE_APaea.SetTypeToTime(false, true);
	m_CE_ABbaa.SetTypeToTime(false, true);
	
	
	m_CE_AGta1.SetTypeToString("x|#x|#x|#x|#x|#",5,0);
	m_CE_AGta2.SetTypeToString("x|#x|#x|#x|#x|#",5,0);
	m_CE_ATga1.SetTypeToString("x",1,0);
	m_CE_ATga2.SetTypeToString("x",1,0);
	m_CE_AGa1x.SetTypeToTime(false, true);
	m_CE_AGa2x.SetTypeToTime(false, true);
	m_CE_AGa1y.SetTypeToTime(false, true);
	m_CE_AGa2y.SetTypeToTime(false, true);

	
	m_CE_ABlt1.SetTypeToString("x|#x|#x|#x|#x|#",5,0);
	m_CE_ABlt2.SetTypeToString("x|#x|#x|#x|#x|#",5,0);
	m_CE_ATmb1.SetTypeToString("x",1,0);
	m_CE_ATmb2.SetTypeToString("x",1,0);
	m_CE_AB1ba.SetTypeToTime(false, true);
	m_CE_AB2ba.SetTypeToTime(false, true);
	m_CE_AB1ea.SetTypeToTime(false, true);
	m_CE_AB2ea.SetTypeToTime(false, true);




	if (bgUseDepBelts) {
		m_CE_DBlt1.SetTypeToString("x|#x|#x|#x|#x|#",5,0);
		m_CE_DB1ba.SetTypeToTime(false, true);
		m_CE_DB1ea.SetTypeToTime(false, true);
		m_CE_DTmb1.SetTypeToString("x",1,0);
	}

	// Disable the default menu which appear by rightclicking
	//    on the CCSEdit -box
	m_CE_AB1ba.UnsetDefaultMenu();
	m_CE_AB1ea.UnsetDefaultMenu();
	m_CE_AB2ba.UnsetDefaultMenu();
	m_CE_AB2ea.UnsetDefaultMenu();
	m_CE_DB1ba.UnsetDefaultMenu();
	m_CE_DB1ea.UnsetDefaultMenu();

	m_CE_AExt1.SetTypeToString("x|#x|#x|#x|#x|#",5,0); 
	m_CE_AExt2.SetTypeToString("x|#x|#x|#x|#x|#",5,0);
	m_CE_ATet1.SetTypeToString("x",1,0);
	m_CE_ATet2.SetTypeToString("x",1,0);

	m_CE_ARem1.SetTextLimit(0,255,true);
	m_CE_ARem3.SetTextLimit(0,255,true);
	m_CE_DRem3.SetTextLimit(0,255,true);

	m_CE_ANfes.SetTypeToTime(false, true);
	
	//Abflug
	//vias must create a button because dialog if full!!!!
	if(!pomDVia)
	{
		pomDVia = new CCSButtonCtrl;
		pomDVia->Create( GetString(IDS_STRING299), SW_HIDE | WS_CHILD | BS_OWNERDRAW | WS_TABSTOP, CRect(0,0,0,0), this, IDC_DVIA ); 
	}

	m_CE_DOrg3.GetWindowRect( olRect );
	m_CE_DDes3.GetWindowRect( olRect1 );
	ScreenToClient(olRect);
	ScreenToClient(olRect1);

	olRect.top -= 4;

	delta = (olRect1.left - olRect.right)/2;
	olRect.left = olRect.right + delta -45 + 20;
	olRect.right = olRect.left + 90 - 20;
	olRect.bottom = olRect.top + 23; 

	if (pomDVia)
	{
		pomDVia->SetWindowPos( NULL, 0, 0, 90, 23,  SWP_NOZORDER );
		pomDVia->MoveWindow(olRect);

		if (m_CB_Ok.m_hWnd)
			pomDVia->SetFont(m_CB_Ok.GetFont());

		pomDVia->ShowWindow(SW_SHOW);

		//if(ogPrivList.GetStat("ROTATIONDLG_CB_DShowVia") == '-')					// TLE: UFIS 3768
			pomDVia->SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_DShowVia"));
	}
/*
//	pomDViaCtrl = new ViaTableCtrl(this,"APC4,FIDS,STOA,STOD");
	pomDViaCtrl = new ViaTableCtrl(this,"HEAD,APC4,FIDS,STOA,ETA,ATA,STOD,ETD,ATD");

	pomDViaCtrl->SetSecStat(ogPrivList.GetStat("ROTATIONDLG_CB_DShowVia"));
	
	m_CE_DOrg3.GetWindowRect( olRect );

	ScreenToClient(olRect);

	//SetWindowPos( NULL, 0, 0, 268, 35,  SWP_NOZORDER );
	olRect.top = olRect.top-20;
	olRect.bottom = 0;
	olRect.left = olRect.right + 40;
	olRect.right = 0;

	pomDViaCtrl->SetPosition(olRect);
*/

	m_CE_DAlc3.SetTypeToString("X|#X|#X|#",3,2);
	m_CE_DFltn.SetTypeToString("#####",5,1);
	m_CE_DFlns.SetTypeToString("X",1,0);
	m_CE_DFlti.SetTypeToString("A",1,0);

	m_CE_DCsgn.SetTypeToString("X|#X|#X|#X|#X|#X|#X|#X|#",8,0);
	
	/* //UFIS-1097 - Commented Out - Start
	if(bgNatureAutoCalc)
	{
		m_CE_DTtyp.SetTypeToString("XXXXX",5,0);
		m_CE_DStyp.SetTypeToString("XX",2,1);
	}
	else
	{
		m_CE_DTtyp.SetTypeToString("XXXXX",5,1);
		m_CE_DStyp.SetTypeToString("XX",2,0);
	}
	*/ //UFIS-1097 - Commented Out - End

	if(bgNatureServiceMand)
	{
		m_CE_DTtyp.SetTypeToString("XXXXX",5,1);
		m_CE_DStyp.SetTypeToString("XX",2,1);
	}
	else if(bgNatureAutoCalc) //UFIS-1097 - Start
	{
		m_CE_DTtyp.SetTypeToString("XXXXX",5,0);
		m_CE_DStyp.SetTypeToString("XX",2,1);
	}
	else
	{
		m_CE_DTtyp.SetTypeToString("XXXXX",5,0);
		m_CE_DStyp.SetTypeToString("XX",2,0);
		if (bgServiceMandatory)
		{
			m_CE_DStyp.SetTypeToString("XX",2,1);
		}
		if(bgNatureMandatory)
		{
			m_CE_DTtyp.SetTypeToString("XXXXX",5,1);
		}
	} //UFIS-1097 - Start

	m_CE_DHtyp.SetTypeToString("XX",2,0);
	
	olTmp.Format("X(%d)", igLengthStev);
	m_CE_DStev.SetTypeToString(olTmp,igLengthStev,0);
	m_CE_DSte2.SetTypeToString("XXX",3,0);
	m_CE_DSte3.SetTypeToString("XXX",3,0);
	m_CE_DSte4.SetTypeToString("XXX",3,0);

	m_CE_DDes3.SetTypeToString("XXX");
	m_CE_DDes4.SetTypeToString("XXXX");
	m_CE_DStod.SetTypeToTime(true, true);
	m_CE_DStoa.SetTypeToTime(false, true);
	
	m_CE_DOfbl.SetTypeToTime(false, true);
	m_CE_DAirb.SetTypeToTime(false, true);
	

	if(bgColorDisabledFields)
	{
		m_CE_DOfbl.DisabledBKColor(true);

	}


	m_CE_DTobt.DisabledBKColor(true);


	m_CE_DEtdi.SetTypeToTime(false, true);
	m_CE_DEtod.SetTypeToTime(false, true);
	
	m_CE_DEtdc.SetTypeToTime(false, true);
	m_CE_DIskd.SetTypeToTime(false, true);
	m_CE_DNxti.SetTypeToTime(false, true);
	m_CE_DSlot.SetTypeToTime(false, true);

//	m_CE_DRwyd.SetTypeToString("XXXX",4,0);
	m_CE_DRwyd.SetTypeToString("x|#x|#x|#x|#",4,0);
	m_CE_DIfrd.SetTypeToString("X",1,0);


	m_CE_DDtd1.SetTypeToString("####",4,0);
	m_CE_DDtd2.SetTypeToString("####",4,0);
	m_CE_DDcd1.SetTypeToString("XX",2,0);
	m_CE_DDcd2.SetTypeToString("XX",2,0);

	m_CE_DNfes.SetTypeToTime(false, true);
		
	
	m_CE_DPstd.SetTypeToString("x|#x|#x|#x|#x|#",5,0);
//	m_CE_DPdba.SetTypeToTime(false, true);
//	m_CE_DPdea.SetTypeToTime(false, true);
	m_CE_DBbfa.SetTypeToTime(false, true);
	
	m_CE_DGtd1.SetTypeToString("x|#x|#x|#x|#x|#",5,0);
	m_CE_DGtd2.SetTypeToString("x|#x|#x|#x|#x|#",5,0);
	m_CE_DTgd1.SetTypeToString("x",1,0);
	m_CE_DTgd2.SetTypeToString("x",1,0);
	m_CE_DGd1x.SetTypeToTime(false, true);
	m_CE_DGd2x.SetTypeToTime(false, true);
	m_CE_DGd1y.SetTypeToTime(false, true);
	m_CE_DGd2y.SetTypeToTime(false, true);
	
	m_CE_DWro1.SetTypeToString("x|#x|#x|#x|#x|#",5,0);
	m_CE_DWro3.SetTypeToString("x|#x|#x|#x|#x|#",5,0);
	m_CE_DTwr1.SetTypeToString("x",1,0);
	m_CE_DTwr3.SetTypeToString("x",1,0);
	m_CE_DW1ba.SetTypeToTime(false, true);
	m_CE_DW3ba.SetTypeToTime(false, true);
	m_CE_DW1ea.SetTypeToTime(false, true);
	m_CE_DW3ea.SetTypeToTime(false, true);

	m_CE_DBao1.SetTypeToTime(false, true);
	m_CE_DBac1.SetTypeToTime(false, true);
	m_CE_DBao4.SetTypeToTime(false, true);
	m_CE_DBac4.SetTypeToTime(false, true);

	m_CE_DRem1.SetTextLimit(0,255,true);
	m_CE_Regn.SetTypeToString("X|#X|#X|#X|#X|#X|#X|#X|#X|#X|#X|#X|#",12,0);


	if(bgColorDisabledFields)
	{
		m_CE_DOfbl.DisabledBKColor(true);
		m_CE_DAirb.DisabledBKColor(true);
		m_CE_DEtdi.DisabledBKColor(true);

		m_CE_AOnbl.DisabledBKColor(true);
		m_CE_ALand.DisabledBKColor(true);
		m_CE_AEtai.DisabledBKColor(true);
	}


	//Create the Controls dynamically - PRF 8379			
	CreateDynamicControls(); 

	CWnd* polWnd = (CWnd*) GetDlgItem(IDC_BTCRC_MTOW);	// TLE: UFIS 3768 
	if (polWnd)
		pomBMTOW->SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_MTow"));

	//MWO: 13.04.05 Delay Codes with DCFTAB

	CRect rectDelay;
	rectDelay.top = 0;
	rectDelay.left = 0;
	rectDelay.right = 400;
	rectDelay.bottom = 200;
	tabDelayArrival.Create ( "", WS_CHILD, rectDelay, this, 1);
	rectDelay.left = 420;
	rectDelay.right = 800;
	tabDelayDeparture.Create ( "", WS_CHILD, rectDelay, this, 1);
	InitTabs();

	if(bgMulitpleDelayCodes == true)
	{
		
		CWnd* polWnd = GetDlgItem(IDC_ADELAY	); 
		if (polWnd)
			polWnd->ShowWindow(SW_HIDE);

		//Then hide the delay code controls for AFTTAB
		//ARRIVAL
		m_CE_ADcd1.ShowWindow(SW_HIDE);
		m_CB_ADcd1List.ShowWindow(SW_HIDE);
		m_CE_ADtd1.ShowWindow(SW_HIDE);
		m_CE_ADcd2.ShowWindow(SW_HIDE);
		m_CB_ADcd2List.ShowWindow(SW_HIDE);
		m_CE_ADtd2.ShowWindow(SW_HIDE);
		//DEPARTURE
		m_CE_DDcd1.ShowWindow(SW_HIDE);
		m_CB_DDcd1List.ShowWindow(SW_HIDE);
		m_CE_DDtd1.ShowWindow(SW_HIDE);
		m_CE_DDcd2.ShowWindow(SW_HIDE);
		m_CB_DDcd2List.ShowWindow(SW_HIDE);
		m_CE_DDtd2.ShowWindow(SW_HIDE);

		CRect tmpR;
		m_CB_ARep.GetClientRect(tmpR);
		m_CB_ARep.ClientToScreen(tmpR);
		this->ScreenToClient(tmpR);
		//tmpR.right += 80;
		tmpR.top += 30;
		tmpR.bottom += 30;
		m_DelayArrBtn.Create( "Delays", WS_VISIBLE|WS_CHILD|BS_OWNERDRAW|WS_TABSTOP, tmpR, this, IDC_DELAYBTN_ARRIVAL);
		//Set button color to normal
		m_DelayArrBtn.SetColors(::GetSysColor(COLOR_BTNFACE), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));

		if(ogPrivList.GetStat("RotationDlg.DelayCodeButton") != '1')
			m_DelayArrBtn.EnableWindow(false);
		m_CB_DRep.GetClientRect(tmpR);
		m_CB_DRep.ClientToScreen(tmpR);
		this->ScreenToClient(tmpR);
		tmpR.top += 25;
		tmpR.bottom += 25;
		m_DelayDepBtn.Create( "Delays", WS_VISIBLE|WS_CHILD|BS_OWNERDRAW|WS_TABSTOP, tmpR, this, IDC_DELAYBTN_DEPARTURE);
		m_DelayDepBtn.SetColors(::GetSysColor(COLOR_BTNFACE), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
		if(ogPrivList.GetStat("RotationDlg.DelayCodeButton") != '1')
			m_DelayDepBtn.EnableWindow(false);
	}

	
	// 20071119 RST PRF 8741 workaround because the dropdown window is not shown (only a )	

    CRect r;
    m_ComboDHistory.GetClientRect(&r);
    m_ComboDHistory.SetWindowPos(NULL, 0, 0, r.right, 200, SWP_NOMOVE | SWP_NOZORDER);

    m_ComboAHistory.GetClientRect(&r);
    m_ComboAHistory.SetWindowPos(NULL, 0, 0, r.right, 200, SWP_NOMOVE | SWP_NOZORDER);

    m_CL_ARemp.GetClientRect(&r);
    m_CL_ARemp.SetWindowPos(NULL, 0, 0, r.right, 200, SWP_NOMOVE | SWP_NOZORDER);

    m_CL_DRemp.GetClientRect(&r);
    m_CL_DRemp.SetWindowPos(NULL, 0, 0, r.right, 200, SWP_NOMOVE | SWP_NOZORDER);


    m_CL_ACxxReason.GetClientRect(&r);
    m_CL_ACxxReason.SetWindowPos(NULL, 0, 0, r.right, 200, SWP_NOMOVE | SWP_NOZORDER);

    m_CL_DCxxReason.GetClientRect(&r);
    m_CL_DCxxReason.SetWindowPos(NULL, 0, 0, r.right, 200, SWP_NOMOVE | SWP_NOZORDER);


	if(pomDRgd1 != NULL)
	{
		pomDRgd1->GetClientRect(&r);
	    pomDRgd1->SetWindowPos(NULL, 0, 0, r.right, 200, SWP_NOMOVE | SWP_NOZORDER);
	}

	if(pomDRgd2 != NULL)
	{
		pomDRgd2->GetClientRect(&r);
	    pomDRgd2->SetWindowPos(NULL, 0, 0, r.right, 200, SWP_NOMOVE | SWP_NOZORDER);
	}


	// 20071119 RST 


	m_CL_ACxxReason.SetDroppedWidth( 200 );	
	m_CL_DCxxReason.SetDroppedWidth( 200 );	


	for(int k = 0; k < 50; k++)	
		omCicLogoArray.Add("");

//	ArrangeTabsOrder();

	 
	//Fixed for not seeing the button
//	CWnd* polWnd = GetDlgItem(IDC_DCINSMAL4); 
//	polWnd->ShowWindow(SW_SHOW);	
//	polWnd->EnableWindow(TRUE);
	
	m_resizeHelper.Init(this->m_hWnd);
	m_resizeHelper.Fix(IDC_AREMP,DlgResizeHelper::kNoHFix,DlgResizeHelper::kHeight);
	m_resizeHelper.Fix(IDC_DREMP,DlgResizeHelper::kNoHFix,DlgResizeHelper::kHeight);


	
	this->GetWindowRect(&olRect);
	GetDialogFromReg(olRect, m_key);
	SetWindowPos(&wndTop, olRect.left, olRect.top, olRect.Width(), olRect.Height(), SWP_HIDEWINDOW);// | SWP_NOMOVE );

	//END MWO: 13.04.05 Delay Codes with DCFTAB
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void RotationDlg::OnSize(UINT nType, int cx, int cy)  
{
	CDialog::OnSize(nType, cx, cy);
	
	// TODO: Add your message handler code here
	m_resizeHelper.OnSize();

	if (this->pomDCinsTable != NULL && ::IsWindow(this->pomDCinsTable->m_hWnd))
	{
		
		CRect olrectTable;
		m_CE_DCinsBorder.GetWindowRect(&olrectTable);
		ScreenToClient(olrectTable);
		olrectTable.DeflateRect(2,2);     // hiding the CTable window border
		this->pomDCinsTable->SetPosition(olrectTable.left, olrectTable.right, olrectTable.top, olrectTable.bottom);

		if (bgCnamAtr)
		{
			m_CE_DCinsBorderExt.GetWindowRect(&olrectTable);
			ScreenToClient(olrectTable);
			olrectTable.DeflateRect(2,2);     // hiding the CTable window border
			this->pomDCinsTable->SetPosition(olrectTable.left, olrectTable.right, olrectTable.top, olrectTable.bottom);

			CRect olRectBorder;
			CRect olRectBorderExt;
			m_CE_DCiFr.GetWindowRect( olRectBorder );
			m_CE_DCinsBorderExt.GetWindowRect( olRectBorderExt );
			ScreenToClient(olRectBorder);
			ScreenToClient(olRectBorderExt);

			olRectBorder.right = olRectBorderExt.right;
			m_CE_DCiFr.MoveWindow(olRectBorder,TRUE);

		}

		this->pomDCinsTable->DisplayTable();
	}

	if (this->pomDJfnoTable != NULL && ::IsWindow(this->pomDJfnoTable->m_hWnd))
	{
		CRect olrectTable;
		m_CE_DJfnoBorder.GetWindowRect(&olrectTable);
		ScreenToClient(olrectTable);
		olrectTable.DeflateRect(2,2);     // hiding the CTable window border
		this->pomDJfnoTable->SetPosition(olrectTable.left, olrectTable.right, olrectTable.top, olrectTable.bottom);

		this->pomDJfnoTable->DisplayTable();
	}

	if (this->pomAJfnoTable != NULL && ::IsWindow(this->pomAJfnoTable->m_hWnd))
	{
		CRect olrectTable;
		m_CE_AJfnoBorder.GetWindowRect(&olrectTable);
		ScreenToClient(olrectTable);
		olrectTable.DeflateRect(2,2);     // hiding the CTable window border
		this->pomAJfnoTable->SetPosition(olrectTable.left, olrectTable.right, olrectTable.top, olrectTable.bottom);

		this->pomAJfnoTable->DisplayTable();
	}


	if(m_CL_ARemp.m_hWnd != NULL)
	{

		CRect r;
		m_ComboDHistory.GetClientRect(&r);
		m_ComboDHistory.SetWindowPos(NULL, 0, 0, r.right, 200, SWP_NOMOVE | SWP_NOZORDER);

		m_ComboAHistory.GetClientRect(&r);
		m_ComboAHistory.SetWindowPos(NULL, 0, 0, r.right, 200, SWP_NOMOVE | SWP_NOZORDER);

		m_CL_ARemp.GetClientRect(&r);
		m_CL_ARemp.SetWindowPos(NULL, 0, 0, r.right, 200, SWP_NOMOVE | SWP_NOZORDER);

		m_CL_DRemp.GetClientRect(&r);
		m_CL_DRemp.SetWindowPos(NULL, 0, 0, r.right, 200, SWP_NOMOVE | SWP_NOZORDER);


		m_CL_ACxxReason.GetClientRect(&r);
		m_CL_ACxxReason.SetWindowPos(NULL, 0, 0, r.right, 200, SWP_NOMOVE | SWP_NOZORDER);

		m_CL_DCxxReason.GetClientRect(&r);
		m_CL_DCxxReason.SetWindowPos(NULL, 0, 0, r.right, 200, SWP_NOMOVE | SWP_NOZORDER);


		if(pomDRgd1 != NULL)
		{
			pomDRgd1->GetClientRect(&r);
			pomDRgd1->SetWindowPos(NULL, 0, 0, r.right, 200, SWP_NOMOVE | SWP_NOZORDER);
		}

		if(pomDRgd2 != NULL)
		{
			pomDRgd2->GetClientRect(&r);
			pomDRgd2->SetWindowPos(NULL, 0, 0, r.right, 200, SWP_NOMOVE | SWP_NOZORDER);
		}


		// 20071119 RST 


		m_CL_ACxxReason.SetDroppedWidth( 200 );	
		m_CL_DCxxReason.SetDroppedWidth( 200 );	


		m_CL_DGatLogo.GetClientRect(&r);
		m_CL_DGatLogo.SetWindowPos(NULL, 0, 0, r.right, 200, SWP_NOMOVE | SWP_NOZORDER);
		m_CL_DGatLogo2.GetClientRect(&r);
		m_CL_DGatLogo2.SetWindowPos(NULL, 0, 0, r.right, 200, SWP_NOMOVE | SWP_NOZORDER);

		m_CL_DGatLogo.SetDroppedWidth( 400 );	
		m_CL_DGatLogo2.SetDroppedWidth( 400 );	




	}


	if(pomArrPermitsButton)
	{
		CRect tmpR;
		m_CB_AAlc3List.GetWindowRect(tmpR);
		//ClientToScreen(tmpR);
		this->ScreenToClient(tmpR);
		tmpR.left -= 21;
		tmpR.right -= 21;
		pomArrPermitsButton->SetWindowPos(this, tmpR.left, tmpR.top, 0, 0, SWP_NOSIZE | SWP_NOZORDER);

	}


	if(pomDepPermitsButton)
	{
		CRect tmpR;
		m_CB_DAlc3List.GetWindowRect(tmpR);
		//ClientToScreen(tmpR);
		this->ScreenToClient(tmpR);
		tmpR.left -= 21;
		tmpR.right -= 21;
		pomDepPermitsButton->SetWindowPos(this, tmpR.left, tmpR.top, 0, 0, SWP_NOSIZE | SWP_NOZORDER);

	}



	this->Invalidate();
}

/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
// Fill the fields and show data in the dialog
//
void RotationDlg::InitDialog( bool bpArrival, bool bpDeparture) 
{
	bmChanged = false;
	bmActSelect = true;
	bmCheckRegn = true;

	CCSEDIT_ATTRIB rlAttribC1;
	CCSEDIT_ATTRIB rlAttribC2;
	CCSEDIT_ATTRIB rlAttribC3;
	CCSEDIT_ATTRIB rlAttribC4;

	CCSPtrArray<TABLE_COLUMN> olColList;
	TABLE_COLUMN rlColumnData;

	int ilLc ;



// Ankunft ///////////////////////////////////////////////////////////////////


	if(bpArrival)
	{
		// for reason of change - save original values
		omPsta = CString(prmAFlight->Psta);
		omGta1 = CString(prmAFlight->Gta1);
		omGta2 = CString(prmAFlight->Gta2);
		omBlt1 = CString(prmAFlight->Blt1);
		omBlt2 = CString(prmAFlight->Blt2);


		if(bgShowCashComment)
		{
			omPcom = CString(prmAFlight->Pcom);
		}

		// load WAW
		if (strcmp(pcgHome, "WAW") == 0 && prmAFlight->Urno > 0)
		{
			CString olCaption = CString(prmAFlight->Flno);
			olCaption.TrimRight();
			if (olCaption.IsEmpty())
				olCaption = CString(prmAFlight->Csgn);

			CString olDes = CString(prmAFlight->Des3) + CString(prmAFlight->Des4);
			CString olOrg = CString(prmAFlight->Org3) + CString(prmAFlight->Org4);

			if (!polRotationALoadDlg)
			{
				bool blEnable = true;
				if(ogPrivList.GetStat("ROTATIONDLG_CE_Load") != '1')
					blEnable = false;

				polRotationALoadDlg = new RotationLoadDlgWAW(this, "A", olCaption, prmAFlight->Urno, CString(""), prmAFlight->Stoa, bmLocalTime, blEnable, olDes, olOrg);
				polRotationALoadDlg->Create(RotationLoadDlgWAW::IDD);
				polRotationALoadDlg->SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_Load"));
			}
			else
				polRotationALoadDlg->SetData("A", olCaption, prmAFlight->Urno, prmAFlight->Vial, prmAFlight->Stoa, bmLocalTime, olDes, olOrg);
		}


// 		if(bmLocalTime) ogRotationDlgFlights.StructUtcToLocal((void*)prmAFlight, "");
 		if(bmLocalTime) ConvertStructUtcTOLocal(prmAFlight, NULL);
	
		if(strcmp(pcgHome, prmAFlight->Org3) == 0)
		{
			m_CE_AStod.SetBKColor(YELLOW);
			m_CE_AStod.SetTypeToTime(true, true);
			m_CE_AStod.SetTextLimit(4,7,false);

			if(strcmp(prmAFlight->Ftyp, "Z") == 0)
				m_CS_Ankunft.SetWindowText(GetString(IDS_STRING1780) + CString(" ")+ prmAFlight->Stoa.Format("%d.%m.%Y"));
			else
			{
				if(strcmp(prmAFlight->Ftyp, "B") == 0)
					m_CS_Ankunft.SetWindowText(GetString(IDS_STRING1778) + CString(" ")+ prmAFlight->Stoa.Format("%d.%m.%Y"));
				else
					m_CS_Ankunft.SetWindowText(GetString(IDS_STRING340) + CString(" ")+ prmAFlight->Stoa.Format("%d.%m.%Y"));
			}
		}
		else
		{
			m_CE_AStod.SetBKColor(WHITE);
			m_CE_AStod.SetTypeToTime(false, true);
			m_CE_AStod.SetTextLimit(0,7,true);
			m_CS_Ankunft.SetWindowText(GetString(IDS_STRING330) + CString(" ") + prmAFlight->Stoa.Format("%d.%m.%Y"));
		}


		if(strcmp(prmAFlight->Ftyp, "X") == 0)
		{
			m_CE_AOnbl.SetReadOnly();
			m_CE_ALand.SetReadOnly();
		}
		else
		{
			m_CE_AOnbl.SetReadOnly(false);
			m_CE_ALand.SetReadOnly(false);
		}

		if(ogFpeData.bmFlightPermitsEnabled)
		{
			if(!pomArrPermitsButton)
			{
				CRect tmpR;
				m_CB_AAlc3List.GetClientRect(tmpR);
				m_CB_AAlc3List.ClientToScreen(tmpR);
				this->ScreenToClient(tmpR);
				tmpR.left -= 25;
				tmpR.right -= 25;

				pomArrPermitsButton = new CCSButtonCtrl;
				pomArrPermitsButton->Create( "", SW_HIDE | WS_CHILD | BS_OWNERDRAW, tmpR, this, IDC_ARRPERMITSBUTTON ); 
			}
			SetArrPermitsButton();
		}

		CWnd* polWnd = (CWnd*) GetDlgItem(IDC_ARRPERMITSBUTTON);	// TLE: UFIS 3768 
		if (polWnd)
			pomArrPermitsButton->SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_APermit"));

		CString olFlno(prmAFlight->Flno);
		olFlno.TrimRight();
		if(olFlno.IsEmpty())
		{
			m_CE_AOrg3.SetTextLimit(0,4,true);
			m_CE_AOrg3.SetBKColor(WHITE);
		}
		// color Etai
		if(strcmp(prmAFlight->Stea, "U") == 0)
			m_CE_AEtai.SetBKColor(RGB(255,0,0));
		else
		{
			if((strcmp(prmAFlight->Stea, "C") == 0) || (strcmp(prmAFlight->Stea, "E") == 0) || (strcmp(prmAFlight->Stea, "A") == 0))
				m_CE_AEtai.SetBKColor(RGB(16,189,239));
			else
				m_CE_AEtai.SetBKColor(RGB(255,255,255));
		}

		// color Airb
		if(strcmp(prmAFlight->Stab, "U") == 0)
			m_CE_AAirb.SetBKColor(RGB(255,0,0));
		else
		{
			if((strcmp(prmAFlight->Stab, "A") == 0) || (strcmp(prmAFlight->Stab, "D") == 0))
				m_CE_AAirb.SetBKColor(RGB(16,189,239));
			else
				m_CE_AAirb.SetBKColor(RGB(255,255,255));
		}

		// color Land
		if(strcmp(prmAFlight->Stld, "U") == 0)
			m_CE_ALand.SetBKColor(RGB(255,0,0));
		else
		{
			if((strcmp(prmAFlight->Stld, "A") == 0) || (strcmp(prmAFlight->Stld, "D") == 0))
				m_CE_ALand.SetBKColor(RGB(16,189,239));
			else
				m_CE_ALand.SetBKColor(RGB(255,255,255));
		}

		// color Ofbl
		if(strcmp(prmAFlight->Stof, "U") == 0)
			m_CE_AOfbl.SetBKColor(RGB(255,0,0));
		else
		{
			if((strcmp(prmAFlight->Stof, "S") == 0) || (strcmp(prmAFlight->Stof, "D") == 0))
				m_CE_AOfbl.SetBKColor(RGB(16,189,239));
			else
				m_CE_AOfbl.SetBKColor(RGB(255,255,255));
		}


		// color Onbl
		if(strcmp(prmAFlight->Ston, "U") == 0)
			m_CE_AOnbl.SetBKColor(RGB(255,0,0));
		else
		{
			if((strcmp(prmAFlight->Ston, "S") == 0) || (strcmp(prmAFlight->Ston, "D") == 0))
				m_CE_AOnbl.SetBKColor(RGB(16,189,239));
			else
				m_CE_AOnbl.SetBKColor(RGB(255,255,255));
		}



		// color Tmoa
		if(strcmp(prmAFlight->Sttm, "U") == 0)
		{
			if(prmAFlight->Tmoa != TIMENULL)
				m_CE_ATmoa.SetBKColor(RGB(255,0,0));
		}
		else
		{
			if(strcmp(prmAFlight->Sttm, "C") == 0)
				m_CE_ATmoa.SetBKColor(RGB(16,189,239));
			else
				m_CE_ATmoa.SetBKColor(RGB(255,255,255));
		}

		// color Etdi
		if(strcmp(prmAFlight->Sted, "U") == 0)
			m_CE_AEtdi.SetBKColor(RGB(255,0,0));
		else
		{
			if((strcmp(prmAFlight->Sted, "C") == 0) || (strcmp(prmAFlight->Sted, "E") == 0) || (strcmp(prmAFlight->Sted, "A") == 0))
				m_CE_AEtdi.SetBKColor(RGB(16,189,239));
			else
				m_CE_AEtdi.SetBKColor(RGB(255,255,255));
		}

		
		
		if(bgShowChock)
		{
			// color Chock on
			if(strcmp(prmAFlight->Stcn, "U") == 0)
				pomCEChocksOn->SetBKColor(RGB(255,0,0));
			else
			{
				if((strcmp(prmAFlight->Stcn, "S") == 0) || (strcmp(prmAFlight->Stcn, "D") == 0))
					pomCEChocksOn->SetBKColor(RGB(16,189,239));
				else
					pomCEChocksOn->SetBKColor(RGB(255,255,255));
			}
		}

		/*

		if((strcmp(prmAFlight->Org3, pcgHome) == 0) && (strcmp(prmAFlight->Des3, pcgHome) == 0)) 
			m_CS_Ankunft.SetWindowText(GetString(IDS_STRING340) + CString(" ")+ prmAFlight->Stoa.Format("%d.%m.%Y"));
		else
			m_CS_Ankunft.SetWindowText(GetString(IDS_STRING330) + CString(" ") + prmAFlight->Stoa.Format("%d.%m.%Y"));

		*/

		// Weitere Flugnummern
		ogRotationDlgFlights.GetJfnoArray(&omAJfno, prmAFlight);

/*		if(omAJfno.GetSize() > 0)
			OnAshowjfno();
		else
			AShowJfnoButton();
*/		

		pomAJfnoTable->ResetContent();
		
		rlAttribC1.Type = KT_STRING;
		rlAttribC1.Format = "XXX";
		rlAttribC1.TextMinLenght = 0;
		rlAttribC1.TextMaxLenght = 3;
		rlAttribC1.Style = ES_UPPERCASE;

		rlAttribC2.Type = KT_STRING;
		rlAttribC2.Format = "#####";
		rlAttribC2.TextMinLenght = 0;
		rlAttribC2.TextMaxLenght = 5;

		rlAttribC3.Type = KT_STRING;
		rlAttribC3.Format = "X";
		rlAttribC3.TextMinLenght = 0;
		rlAttribC3.TextMaxLenght = 1;
		rlAttribC3.Style = ES_UPPERCASE;

		rlColumnData.SeparatorType = SEPA_NONE;
		rlColumnData.Font = &ogMS_Sans_Serif_8;
		rlColumnData.VerticalSeparator = SEPA_NORMAL;
		rlColumnData.HorizontalSeparator = SEPA_NONE;
		rlColumnData.Alignment = COLALIGN_LEFT;


		if(ogPrivList.GetStat("ROTATIONDLG_CB_AShowJfno") != '1')
			rlColumnData.BkColor = RGB(192,192,192);
		else
			rlColumnData.BkColor = RGB(255,255,255);


		for (ilLc = 0; ilLc < omAJfno.GetSize(); ilLc++)
		{
			rlColumnData.Text = omAJfno[ilLc].Alc3;
			rlColumnData.Text.TrimRight();
			rlColumnData.EditAttrib = rlAttribC1;
			olColList.NewAt(olColList.GetSize(), rlColumnData);
			rlColumnData.Text = omAJfno[ilLc].Fltn;
			rlColumnData.Text.TrimRight();
			rlColumnData.EditAttrib = rlAttribC2;
			olColList.NewAt(olColList.GetSize(), rlColumnData);
			rlColumnData.Text = omAJfno[ilLc].Flns;
			rlColumnData.Text.TrimRight();
			rlColumnData.EditAttrib = rlAttribC3;
			olColList.NewAt(olColList.GetSize(), rlColumnData);
			pomAJfnoTable->AddTextLine(olColList, (void*)(NULL));
			olColList.DeleteAll();
		}
		for (ilLc = omAJfno.GetSize(); ilLc < 9; ilLc++)
		{
			rlColumnData.Text = "";
			rlColumnData.EditAttrib = rlAttribC1;
			olColList.NewAt(olColList.GetSize(), rlColumnData);
			rlColumnData.Text = "";
			rlColumnData.EditAttrib = rlAttribC2;
			olColList.NewAt(olColList.GetSize(), rlColumnData);
			rlColumnData.Text = "";
			rlColumnData.EditAttrib = rlAttribC3;
			olColList.NewAt(olColList.GetSize(), rlColumnData);
			pomAJfnoTable->AddTextLine(olColList, (void*)(NULL));
			olColList.DeleteAll();
		}

		pomAJfnoTable->DisplayTable();
/*
		if(omAJfno.GetSize() > 0)
			OnAshowjfno();
		else
			AShowJfnoButton();
*/
		
		// Vias
		CString olCaption = CString(prmAFlight->Flno);
		olCaption.TrimRight();
		if (olCaption.IsEmpty())
			olCaption = CString(prmAFlight->Csgn);

		if (!polRotationAViaDlg)
		{
			bool blEnable = true;
			if(ogPrivList.GetStat("ROTATIONDLG_CB_AShowVia") != '1')
				blEnable = false;

			polRotationAViaDlg = new RotationViaDlg(this, "A", olCaption, prmAFlight->Urno, prmAFlight->Vial, prmAFlight->Stoa, bmLocalTime, blEnable);
			polRotationAViaDlg->Create(RotationViaDlg::IDD);
			polRotationAViaDlg->SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_AShowVia"));
		}
		else
			polRotationAViaDlg->SetData("A", olCaption, prmAFlight->Urno, prmAFlight->Vial, prmAFlight->Stoa, bmLocalTime);


		if( CString(prmAFlight->Ftyp)	== "Z" || CString(prmAFlight->Ftyp)	== "B")
		{
			if (polRotationAViaDlg)
				polRotationAViaDlg->Enable(false);
		}
		else
		{
			if (polRotationAViaDlg)
				polRotationAViaDlg->Enable(true); 
		}
/*		

		if( CString(prmAFlight->Ftyp)	== "Z" || CString(prmAFlight->Ftyp)	== "B")
		{
			pomAViaCtrl->bmEdit = false;
		}
		else
		{
			pomAViaCtrl->bmEdit = true;
		}


		pomAViaCtrl->SetViaList(prmAFlight->Vial, prmAFlight->Stoa, bmLocalTime);
*/

		m_ACxx = FALSE;
		
		m_AVip = FALSE;
		m_AFpsa = FALSE;

		m_CB_AVip.SetCheck(FALSE);
		m_CB_AFpsa.SetCheck(FALSE);

		m_CB_ACxx.SetCheck(FALSE);
		m_ADiverted = FALSE;
		m_CB_ADiverted.SetCheck(FALSE);
		m_ARerouted = FALSE;
		m_CB_ARerouted.SetCheck(FALSE);
		m_CB_ARetFlight.SetCheck(FALSE);
		m_CB_ARetTaxi.SetCheck(FALSE);

		m_CE_ADivr.EnableWindow(FALSE);

		// AIA 4.6 special
		m_GoAround = FALSE;
		m_CB_GoAround.SetCheck(m_GoAround);
	
		m_CB_AVip.SetCheck(FALSE);
		if(strcmp(prmAFlight->Vipa, "X") == 0)
		{
			m_AVip = TRUE;
			m_CB_AVip.SetCheck(m_AVip);
		}

		if (bgDailyFixedRes) 
		{
			if(strcmp(prmAFlight->Fpsa, "X") == 0)
			{
				pomCBFPSA->SetCheck(TRUE);
				
			}
			else
			{
				pomCBFPSA->SetCheck(FALSE);
				//pomBTCRCFPSA->ShowWindow(SW_HIDE);
			}

			if(strcmp(prmAFlight->Fbl1, "X") == 0)
			{
				pomCBFBL1->SetCheck(TRUE);
				
			}
			else
			{
				pomCBFBL1->SetCheck(FALSE);
				//pomBTCRCABLT1->ShowWindow(SW_HIDE);
				
			}

			if(strcmp(prmAFlight->Fbl2, "X") == 0)
			{
				pomCBFBL2->SetCheck(TRUE);
				
			}
			else
			{
				pomCBFBL2->SetCheck(FALSE);
				//pomBTCRCABLT2->ShowWindow(SW_HIDE);
			}

			if(strcmp(prmAFlight->Fga1, "X") == 0)
			{
				pomCBFGA1->SetCheck(TRUE);
				
			}
			else
			{
				pomCBFGA1->SetCheck(FALSE);
				//pomBTCRCAGTA1->ShowWindow(SW_HIDE);
			}
		
			if(strcmp(prmAFlight->Fga2, "X") == 0)
			{
				pomCBFGA2->SetCheck(TRUE);
			
			}
			else
			{
				pomCBFGA2->SetCheck(FALSE);
				//pomBTCRCAGTA2->ShowWindow(SW_HIDE);
			}
		}
		else
		{
			m_CB_AFpsa.SetCheck(FALSE);
			if(strcmp(prmAFlight->Fpsa, "X") == 0)
			{
				m_AFpsa = TRUE;
				m_CB_AFpsa.SetCheck(m_AFpsa);
			}
		}

		if(bgShowAOG)
		{
			if(strcmp(prmAFlight->Faog, "X") == 0)
			{
				pomCBAAOG->SetCheck(TRUE);
			}
			else
			{
				pomCBAAOG->SetCheck(FALSE);
			}
		}

		if(bgShowADHOC)
		{
			if(strcmp(prmAFlight->Adho, "X") == 0)
			{
				pomCBAAHOC->SetCheck(TRUE);
			}
			else
			{
				pomCBAAHOC->SetCheck(FALSE);
			}
		}
		

		if(strcmp(prmAFlight->Baz6, "G") == 0)
		{
			m_GoAround = TRUE;
			m_CB_GoAround.SetCheck(m_GoAround);
		}
		// AIA 4.6 special end
		

		if(strcmp(prmAFlight->Ftyp, "X") == 0)
		{
			m_ACxx = TRUE;
			m_CB_ACxx.SetCheck(TRUE);
		}
		if(strcmp(prmAFlight->Ftyp, "D") == 0)
		{
			m_ADiverted = TRUE;
			m_CB_ADiverted.SetCheck(TRUE);
			m_CE_ADivr.EnableWindow(TRUE);
		}
		if(strcmp(prmAFlight->Ftyp, "R") == 0)
		{
			m_ARerouted = TRUE;
			m_CB_ARerouted.SetCheck(TRUE);
		}


		if(strcmp(prmAFlight->Ftyp, "Z") == 0)
		{
			m_CB_ARetFlight.SetCheck(TRUE);
		}

		if(strcmp(prmAFlight->Ftyp, "B") == 0)
		{
			m_CB_ARetTaxi.SetCheck(TRUE);
		}


		if(bgCxxReason)
		{
			if( CString(prmAFlight->Ftyp)	== "X" )
			{
				if(ogPrivList.GetStat("ROTATIONDLG_CE_ACxxReason") == '-')
				{
					m_CL_ACxxReason.ShowWindow(SW_HIDE);
				}
				else
				{
					m_CL_ACxxReason.ShowWindow(SW_SHOW);
					if(ogPrivList.GetStat("ROTATIONLG_CE_ACxxReason") == '0')
					{
						m_CL_ACxxReason.EnableWindow(FALSE);
					}
				}
			}
			else
			{
				m_CL_ACxxReason.ShowWindow(SW_HIDE);
			}
		}





		omAAlc3 = CString(prmAFlight->Alc3);
		omAAlc2 = CString(prmAFlight->Alc2);

		m_AAlc3 = CString(prmAFlight->Flno);
		m_AAlc3 = m_AAlc3.Left(3);
		m_AAlc3.TrimRight();
		m_AFltn = CString(prmAFlight->Fltn);
		m_AFlns = CString(prmAFlight->Flns);
		m_AFlti = prmAFlight->Flti;

		m_CE_AFlti.SetBKColor(RGB(255,255,255));
		if ((int) strlen(prmAFlight->Dssf) > ogDssfFltiId && ogDssfFltiId != -1)
		{
			if (prmAFlight->Dssf[ogDssfFltiId] == 'U')
				m_CE_AFlti.SetBKColor(RGB(255,0,0));
		}

		if(bgNatureAutoCalc)
		{
			m_CE_ATtyp.SetBKColor(RGB(255,255,255));
			if ((int) strlen(prmAFlight->Dssf) > ogDssfTtypId && ogDssfTtypId != -1)
			{
				if (prmAFlight->Dssf[ogDssfTtypId] == 'U')
					m_CE_ATtyp.SetBKColor(RGB(255,0,0));
			}
		}
		
		strcpy(pcmAOrg4, prmAFlight->Org4);
		strcpy(pcmAOrg3, prmAFlight->Org3);

		m_ADes3 = CString(pcgHome);
		m_AOrg3 = CString(prmAFlight->Org4);
		m_AOrg3L = CString(prmAFlight->Org3);
		m_ATtyp = CString(prmAFlight->Ttyp);
		m_AHtyp = CString(prmAFlight->Htyp);
		m_AStyp = CString(prmAFlight->Styp);
		m_AStev = CString(prmAFlight->Stev);
		m_ASte2 = CString(prmAFlight->Ste2);
		m_ASte3 = CString(prmAFlight->Ste3);
		m_ASte4 = CString(prmAFlight->Ste4);
		m_ARem1 = CString(prmAFlight->Rem1);
		m_ARem3 = CString(prmAFlight->Dela);
		m_AStod = DateToHourDivString(prmAFlight->Stod, prmAFlight->Stoa);
		m_AStoa = DateToHourDivString(prmAFlight->Stoa, prmAFlight->Stoa);

		m_AEtai = DateToHourDivString(prmAFlight->Etai, prmAFlight->Stoa);
		m_AEtoa = DateToHourDivString(prmAFlight->Etoa, prmAFlight->Stoa);
		m_AOnbe = DateToHourDivString(prmAFlight->Onbe, prmAFlight->Stoa);
		m_ANxti = DateToHourDivString(prmAFlight->Nxti, prmAFlight->Stoa);

		m_AOnbl = DateToHourDivString(prmAFlight->Onbl, prmAFlight->Stoa);
		m_AOfbl = DateToHourDivString(prmAFlight->Ofbl, prmAFlight->Stoa);
		m_AAirb = DateToHourDivString(prmAFlight->Airb, prmAFlight->Stoa);
		m_ALand = DateToHourDivString(prmAFlight->Land, prmAFlight->Stoa);

		m_ATmoa = DateToHourDivString(prmAFlight->Tmoa, prmAFlight->Stoa);


		m_ABas1 = DateToHourDivString(prmAFlight->Bas1, prmAFlight->Stoa);
		m_ABae1 = DateToHourDivString(prmAFlight->Bae1, prmAFlight->Stoa);
		m_ABas2 = DateToHourDivString(prmAFlight->Bas2, prmAFlight->Stoa);
		m_ABae2 = DateToHourDivString(prmAFlight->Bae2, prmAFlight->Stoa);

		if(bgShowChock)
		{

			
			pomCEChocksOn->SetInitText(DateToHourDivString(prmAFlight->Chon, prmAFlight->Stoa));
			pomCEChocksOf->SetInitText(DateToHourDivString(prmDFlight->Chof, prmDFlight->Stod));
		}


	////////////// CXX Reason

	if(prmAFlight != NULL && bgCxxReason)
	{

		m_CL_ACxxReason.ResetContent();
		m_CL_ACxxReason.AddString(" ");
		CString olCxxr = CString(prmAFlight->Cxxr);
		olCxxr.TrimLeft();

		int	ilDIndex = 0;

		CString olItem;

		int ilCount = ogBCD.GetDataCount("CRC_CXX");
		ogBCD.SetSort("CRC_CXX", "CODE+", true);
		for(int i = 0; i < ilCount; i++)
		{
			olItem = ogBCD.GetFields("CRC_CXX",i , "CODE,REMA,URNO" , "   ", FILLBLANK);
			m_CL_ACxxReason.AddString(olItem);
			if( olItem.Find(olCxxr) >= 0 && !olCxxr.IsEmpty())
				ilDIndex = i + 1;
		}
		
		m_CL_ACxxReason.SetCurSel(ilDIndex);

	}	
	////////////// Cxx Reason end


		if(bgNightFlightSlot)
		{
			if(prmAFlight->Nfes != TIMENULL)
			{
				m_CE_ANfes.ShowWindow(SW_SHOW);
				m_CB_ANfes.EnableWindow(TRUE);
				m_CE_ANfes.EnableWindow(TRUE);
				m_CE_ANfes.SetInitText( DateToHourDivString(prmAFlight->Nfes, prmAFlight->Stoa) );
				m_CB_ANfes.SetCheck(TRUE);
			}
			else
			{
				m_CE_ANfes.SetInitText("" );
				m_CE_ANfes.ShowWindow(SW_HIDE);
				m_CE_ANfes.EnableWindow(FALSE);
				m_CB_ANfes.SetCheck(FALSE);
			}
		}


		/*
		
		CString olN;
		CString olA;

		if(CString(prmAFlight->Dcd1).IsEmpty())
			m_ADcd1 = "";
		else
		{
			bool blRet = ogBCD.GetField("DEN", "DECN", "DECA", CString(prmAFlight->Dcd1), olN, olA ); 

			if ( bgDelaycodeNumeric)
				m_ADcd1 = olN;
			else
				m_ADcd1 = olA;
		}

		if(CString(prmAFlight->Dcd2).IsEmpty())
			m_ADcd2 = "";
		else
		{

			bool blRet = ogBCD.GetField("DEN", "DECN", "DECA", CString(prmAFlight->Dcd2), olN, olA ); 

			if ( bgDelaycodeNumeric)
				m_ADcd2 = olN;
			else
				m_ADcd2 = olA;
		}
		*/
		m_ADcd1 = CString(prmAFlight->Dcd1);
		m_ADcd2 = CString(prmAFlight->Dcd2);

		
		m_ADtd1 = CString(prmAFlight->Dtd1);
		m_ADtd2 = CString(prmAFlight->Dtd2);

		m_ARwya = CString(prmAFlight->Rwya);
		m_AIfra = CString(prmAFlight->Ifra);
		//always IFR (" ") if not VFR ("V")
		if(m_AIfra != "V")
			m_AIfra = "";

		m_ADivr = CString(prmAFlight->Divr);

		m_ALastChange = prmAFlight->Lstu .Format("  %d.%m.%Y / %H:%M  ") + CString(prmAFlight->Useu);
		m_ALastChangeTime = CString(prmAFlight->Chgi);

		m_ABbaa = DateToHourDivString(prmAFlight->Bbaa, prmAFlight->Stoa);

//		m_APaba = DateToHourDivString(prmAFlight->Paba, prmAFlight->Stoa);
		m_AEtdi = DateToHourDivString(prmAFlight->Etdi, prmAFlight->Stoa);
//		m_APaea = DateToHourDivString(prmAFlight->Paea, prmAFlight->Stoa);
		m_AGa1x = DateToHourDivString(prmAFlight->Ga1x, prmAFlight->Stoa);
		m_AGa2x = DateToHourDivString(prmAFlight->Ga2x, prmAFlight->Stoa);
		m_AGa1y = DateToHourDivString(prmAFlight->Ga1y, prmAFlight->Stoa);
		m_AGa2y = DateToHourDivString(prmAFlight->Ga2y, prmAFlight->Stoa);
		m_AB1ba = DateToHourDivString(prmAFlight->B1ba, prmAFlight->Stoa);
		m_AB2ba = DateToHourDivString(prmAFlight->B2ba, prmAFlight->Stoa);
		m_AB1ea = DateToHourDivString(prmAFlight->B1ea, prmAFlight->Stoa);
		m_AB2ea = DateToHourDivString(prmAFlight->B2ea, prmAFlight->Stoa);
		
		m_APsta = CString(prmAFlight->Psta);
		m_AGta1 = CString(prmAFlight->Gta1);
		m_AGta2 = CString(prmAFlight->Gta2);
		m_ATga1 = CString(prmAFlight->Tga1);
		m_ATga2 = CString(prmAFlight->Tga2);
		m_ABlt1 = CString(prmAFlight->Blt1);
		m_ABlt2 = CString(prmAFlight->Blt2);
		m_ATmb1 = CString(prmAFlight->Tmb1);
		m_ATmb2 = CString(prmAFlight->Tmb2);
		m_AExt1 = CString(prmAFlight->Ext1);
		m_AExt2 = CString(prmAFlight->Ext2);
		m_ATet1 = CString(prmAFlight->Tet1);
		m_ATet2 = CString(prmAFlight->Tet2);

		m_CE_ACsgn.SetInitText(prmAFlight->Csgn);

 		m_CE_ADes3.SetInitText(pcgHome + CString(" - ") + pcgHome4);
		m_CE_AOrg3.SetInitText(m_AOrg3);
		m_CE_AOrg3L.SetInitText(m_AOrg3L);
		m_CE_ADivr.SetInitText(m_ADivr);
		m_CE_AAlc3.SetInitText(m_AAlc3);
		m_CE_AFltn.SetInitText(m_AFltn);
		m_CE_AFlns.SetInitText(m_AFlns);
		m_CE_AFlti.SetInitText(m_AFlti);
		m_CE_ATtyp.SetInitText(m_ATtyp);
		m_CE_AHtyp.SetInitText(m_AHtyp);
		m_CE_AStyp.SetInitText(m_AStyp);
		m_CE_AStev.SetInitText(m_AStev);
		m_CE_ASte2.SetInitText(m_ASte2);
		m_CE_ASte3.SetInitText(m_ASte3);
		m_CE_ASte4.SetInitText(m_ASte4);
		m_CE_AOrg3.SetInitText(m_AOrg3);
		m_CE_ARem1.SetInitText(m_ARem1);
		m_CE_ARem3.SetInitText(m_ARem3);
		m_CE_AEtai.SetInitText(m_AEtai);
		m_CE_AStod.SetInitText(m_AStod);
		m_CE_AStoa.SetInitText(m_AStoa);
		m_CE_AEtai.SetInitText(m_AEtai);
		m_CE_AEtoa.SetInitText(m_AEtoa);
		m_CE_AOnbe.SetInitText(m_AOnbe);
		m_CE_ANxti.SetInitText(m_ANxti);
		m_CE_AOnbl.SetInitText(m_AOnbl);
		m_CE_AOfbl.SetInitText(m_AOfbl);
		m_CE_AAirb.SetInitText(m_AAirb);
		m_CE_ALand.SetInitText(m_ALand);
		m_CE_ATmoa.SetInitText(m_ATmoa);
		m_CE_ADtd1.SetInitText(m_ADtd1);
		m_CE_ADtd2.SetInitText(m_ADtd2);
		m_CE_ADcd1.SetInitText(m_ADcd1);
		m_CE_ADcd2.SetInitText(m_ADcd2);
		m_CE_ARwya.SetInitText(m_ARwya);
		m_CE_AIfra.SetInitText(m_AIfra);
		m_CE_APsta.SetInitText(m_APsta);
//		m_CE_APaba.SetInitText(m_APaba);
		m_CE_AEtdi.SetInitText(m_AEtdi);
//		m_CE_APaea.SetInitText(m_APaea);
		m_CE_AGta1.SetInitText(m_AGta1);
		m_CE_AGta2.SetInitText(m_AGta2);
		m_CE_ATga1.SetInitText(m_ATga1);
		m_CE_ATga2.SetInitText(m_ATga2);
		m_CE_AGa1x.SetInitText(m_AGa1x);
		m_CE_AGa2x.SetInitText(m_AGa2x);
		m_CE_AGa1y.SetInitText(m_AGa1y);
		m_CE_AGa2y.SetInitText(m_AGa2y);
		m_CE_ABlt1.SetInitText(m_ABlt1);
		m_CE_ABlt2.SetInitText(m_ABlt2);
		m_CE_ATmb1.SetInitText(m_ATmb1);
		m_CE_ATmb2.SetInitText(m_ATmb2);
		m_CE_AB1ba.SetInitText(m_AB1ba);
		m_CE_AB2ba.SetInitText(m_AB2ba);
		m_CE_AB1ea.SetInitText(m_AB1ea);
		m_CE_AB2ea.SetInitText(m_AB2ea);
		m_CE_AExt1.SetInitText(m_AExt1);
		m_CE_AExt2.SetInitText(m_AExt2);
		m_CE_ATet1.SetInitText(m_ATet1);
		m_CE_ATet2.SetInitText(m_ATet2);
		m_CE_ABbaa.SetInitText(m_ABbaa);

		m_CE_ABas1.SetInitText(m_ABas1);
		m_CE_ABae1.SetInitText(m_ABae1);
		m_CE_ABas2.SetInitText(m_ABas2);
		m_CE_ABae2.SetInitText(m_ABae2);


		m_CE_ALastChange.SetInitText(m_ALastChange);
		m_CE_ALastChangeTime.SetInitText(m_ALastChangeTime);

		//Added the Comment for Paid
		//Christine
		if(bgShowCashComment)
		{
			pomBTCRCom->SetInitText(prmAFlight->Pcom);
		}

		//Chocks On
		if(bgShowChock)
		{
			//pomCEChocksOn->SetBKColor(WHITE);
			pomCEChocksOn->SetTypeToTime(false, true);
			pomCEChocksOn->SetInitText(DateToHourDivString(prmAFlight->Chon, prmAFlight->Stoa));
			
		}

		//For arrival Mode of Payment
		if(bgShowPayDetail)
		{
			pomAModeOfPay->ShowWindow(TRUE);
		}

		polWnd = (CWnd*) GetDlgItem(IDC_BTCRC_PAY_ARR);	// TLE: UFIS 3768 
		if (polWnd)
			pomAModeOfPay->SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_APay"));

		CString tempMtow = prmAFlight->Mtow; //lgp
		tempMtow.TrimLeft('0'); //lgp
		pomBMTOW->SetInitText(tempMtow); //lgp
	}




// Abflug ///////////////////////////////////////////////////////////////////
	if(bpDeparture)
	{

		// for reason of change - save original values
		omPstd = CString(prmDFlight->Pstd);
		omGtd1 = CString(prmDFlight->Gtd1);
		omGtd2 = CString(prmDFlight->Gtd2);

				
		// load WAW
		if (strcmp(pcgHome, "WAW") == 0 && prmDFlight->Urno > 0)
		{
			CString olCaption = CString(prmDFlight->Flno);
			olCaption.TrimRight();
			if (olCaption.IsEmpty())
				olCaption = CString(prmDFlight->Csgn);

			CString olDes = CString(prmDFlight->Des3) + CString(prmDFlight->Des4);
			if (!polRotationDLoadDlg)
			{
				bool blEnable = true;
				if(ogPrivList.GetStat("ROTATIONDLG_CE_Load") != '1')
					blEnable = false;

				polRotationDLoadDlg = new RotationLoadDlgWAW(this, "D", olCaption, prmDFlight->Urno, prmDFlight->Vial, prmDFlight->Stod, bmLocalTime, blEnable, olDes);
				polRotationDLoadDlg->Create(RotationLoadDlgWAW::IDD);
				polRotationDLoadDlg->SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_Load"));
			}
			else
				polRotationDLoadDlg->SetData("D", olCaption, prmDFlight->Urno, prmDFlight->Vial, prmDFlight->Stod, bmLocalTime, olDes);
		}


// 		if(bmLocalTime) ogRotationDlgFlights.StructUtcToLocal((void*)prmDFlight, "");
 		if(bmLocalTime) ConvertStructUtcTOLocal(NULL, prmDFlight);

		if(strcmp(prmDFlight->Des3 ,pcgHome) == 0)
		{
			m_CE_DStoa.SetBKColor(YELLOW);
			m_CE_DStoa.SetTypeToTime(true, true);
			m_CE_DStoa.SetTextLimit(4,7,false);

			
			if(strcmp(prmDFlight->Ftyp, "Z") == 0)
					m_CS_Abflug.SetWindowText(GetString(IDS_STRING1780) + CString(" ")+ prmDFlight->Stod.Format("%d.%m.%Y"));
			else
			{
				if(strcmp(prmDFlight->Ftyp, "B") == 0)
					m_CS_Abflug.SetWindowText(GetString(IDS_STRING1778) + CString(" ")+ prmDFlight->Stod.Format("%d.%m.%Y"));
				else
					m_CS_Abflug.SetWindowText(GetString(IDS_STRING340) + CString(" ")+ prmDFlight->Stod.Format("%d.%m.%Y"));
			}
					
		}
		else
		{
			m_CE_DStoa.SetBKColor(WHITE);
			m_CE_DStoa.SetTypeToTime(false, true);
			m_CE_DStoa.SetTextLimit(0,7,true);
			m_CS_Abflug.SetWindowText(GetString(IDS_STRING338) + CString(" ")+ prmDFlight->Stod.Format("%d.%m.%Y"));
		}

		if(ogFpeData.bmFlightPermitsEnabled)
		{
			if(!pomDepPermitsButton)
			{
				CRect tmpR;
				m_CB_DAlc3List.GetClientRect(tmpR);
				m_CB_DAlc3List.ClientToScreen(tmpR);
				this->ScreenToClient(tmpR);
				tmpR.left -= 25;
				tmpR.right -= 25;

				pomDepPermitsButton = new CCSButtonCtrl;
				pomDepPermitsButton->Create( "", SW_HIDE | WS_CHILD | BS_OWNERDRAW, tmpR, this, IDC_DEPPERMITSBUTTON ); 
			}
			SetDepPermitsButton();
		}

		CWnd* polWnd = (CWnd*) GetDlgItem(IDC_DEPPERMITSBUTTON);	// TLE: UFIS 3768 
		if (polWnd)
			pomDepPermitsButton->SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_DPermit"));

		m_CB_DRetFlight.SetCheck(FALSE);
		m_CB_DRetTaxi.SetCheck(FALSE);
		m_CB_DCxx.SetCheck(FALSE);
		m_DDiverted = FALSE;
		m_CB_DDiverted.SetCheck(FALSE);
		m_DRerouted = FALSE;
		m_CB_DRerouted.SetCheck(FALSE);

		m_CE_DDivr.EnableWindow(FALSE);

		m_CB_DGd2d.SetCheck(FALSE);
		if(strcmp(prmDFlight->Gd2d, "X") == 0)
		{
			m_DGd2d = TRUE;
			m_CB_DGd2d.SetCheck(TRUE);
		}

		m_CB_DVip.SetCheck(FALSE);
		if(strcmp(prmDFlight->Vipd, "X") == 0)
		{
			m_DVip = TRUE;
			m_CB_DVip.SetCheck(m_DVip);
		}

		


		if(bgNightFlightSlot)
		{
			if(prmDFlight->Nfes != TIMENULL)
			{
				m_CE_DNfes.ShowWindow(SW_SHOW);
				m_CB_DNfes.EnableWindow(TRUE);
				m_CE_DNfes.EnableWindow(TRUE);
				m_CE_DNfes.SetInitText( DateToHourDivString(prmDFlight->Nfes, prmDFlight->Stod) );
				m_CB_DNfes.SetCheck(TRUE);
			}
			else
			{
				m_CE_DNfes.SetInitText( "" );
				m_CE_DNfes.ShowWindow(SW_HIDE);
				m_CE_DNfes.EnableWindow(FALSE);
				m_CB_DNfes.SetCheck(FALSE);
			}
		}


		if(strcmp(prmDFlight->Ftyp, "X") == 0)
		{
			m_DCxx = TRUE;
			m_CB_DCxx.SetCheck(TRUE);
			m_CE_DOfbl.SetReadOnly();
			m_CE_DAirb.SetReadOnly();
		}
		else
		{
			if(strcmp(prmDFlight->Ftyp, "O") == 0)
			{
				m_DCxx = FALSE;
				m_CB_DCxx.SetCheck(FALSE);
			}
			m_CE_DOfbl.SetReadOnly(false);
			m_CE_DAirb.SetReadOnly(false);

			if(strcmp(prmDFlight->Ftyp, "D") == 0)
			{
				m_DDiverted = TRUE;
				m_CB_DDiverted.SetCheck(TRUE);
				m_CE_DDivr.EnableWindow(TRUE);
			}
			if(strcmp(prmDFlight->Ftyp, "R") == 0)
			{
				m_DRerouted = TRUE;
				m_CB_DRerouted.SetCheck(TRUE);
			}
			if(strcmp(prmDFlight->Ftyp, "Z") == 0)
			{
				m_CB_DRetFlight.SetCheck(TRUE);
			}

			if(strcmp(prmDFlight->Ftyp, "B") == 0)
			{
				m_CB_DRetTaxi.SetCheck(TRUE);
			}
		}


		if(bgDailyFixedRes)
		{
			if(strcmp(prmDFlight->Fpsd, "X") == 0)
			{
				pomCBFPSD->SetCheck(TRUE);
			
			}
			else
			{
				pomCBFPSD->SetCheck(FALSE);
				//pomBTCRCFPSD->ShowWindow(SW_HIDE);
			}

			if(strcmp(prmDFlight->Fgd1, "X") == 0)
			{
				pomCBFGD1->SetCheck(TRUE);
				
			}
			else
			{
				pomCBFGD1->SetCheck(FALSE);
				//pomBTCRCDGTD1->ShowWindow(SW_HIDE);
			}

			if(strcmp(prmDFlight->Fgd2, "X") == 0)
			{
				pomCBFGD2->SetCheck(TRUE);
			}
			else
			{
				pomCBFGD2->SetCheck(FALSE);
				//pomBTCRCDGTD2->ShowWindow(SW_HIDE);
			}
		}
		else
		{
			m_CB_DFpsd.SetCheck(FALSE);
			if(strcmp(prmDFlight->Fpsd, "X") == 0)
			{
				m_DFpsd = TRUE;
				m_CB_DFpsd.SetCheck(m_DFpsd);
			}
		}

		if(bgShowAOG)
		{
			if(strcmp(prmDFlight->Faog, "X") == 0)
			{
				pomCBDAOG->SetCheck(TRUE);
			}
			else
			{
				pomCBDAOG->SetCheck(FALSE);
			}
		}

		if(bgShowADHOC)
		{
			if(strcmp(prmDFlight->Adho, "X") == 0)
			{
				pomCBDAHOC->SetCheck(TRUE);
			}
			else
			{
				pomCBDAHOC->SetCheck(FALSE);
			}
		}

		if(bgCxxReason)
		{
			if( CString(prmDFlight->Ftyp)	== "X" )
			{
				if(ogPrivList.GetStat("ROTATIONDLG_CE_DCxxReason") == '-')
				{
					m_CL_DCxxReason.ShowWindow(SW_HIDE);
				}
				else
				{
					m_CL_DCxxReason.ShowWindow(SW_SHOW);
					if(ogPrivList.GetStat("ROTATIONDLG_CE_DCxxReason") == '0')
					{
						m_CL_DCxxReason.EnableWindow(FALSE);
					}
				}
			}
			else
			{
				m_CL_DCxxReason.ShowWindow(SW_HIDE);
				m_CL_DCxxReason.SetCurSel(0);
			}
		}



		CString olFlno(prmDFlight->Flno);
		olFlno.TrimRight();
		if(olFlno.IsEmpty())
		{
			m_CE_DDes3.SetTextLimit(0,3,true);
			m_CE_DDes3.SetBKColor(WHITE);
			m_CE_DDes4.SetTextLimit(0,4,true);
			m_CE_DDes4.SetBKColor(WHITE);
		}
		// color Etdi
		if(strcmp(prmDFlight->Sted, "U") == 0)
			m_CE_DEtdi.SetBKColor(RGB(255,0,0));
		else
		{
			if((strcmp(prmDFlight->Sted, "C") == 0) || (strcmp(prmDFlight->Sted, "E") == 0) || (strcmp(prmDFlight->Sted, "A") == 0))
				m_CE_DEtdi.SetBKColor(RGB(16,189,239));
			else
				m_CE_DEtdi.SetBKColor(RGB(255,255,255));
		}

		// color Airb
		if(strcmp(prmDFlight->Stab, "U") == 0)
			m_CE_DAirb.SetBKColor(RGB(255,0,0));
		else
		{
			if((strcmp(prmDFlight->Stab, "A") == 0) || (strcmp(prmDFlight->Stab, "D") == 0))
				m_CE_DAirb.SetBKColor(RGB(16,189,239));
			else
				m_CE_DAirb.SetBKColor(RGB(255,255,255));
		}


		// color Ofbl
		if(strcmp(prmDFlight->Stof, "U") == 0)
			m_CE_DOfbl.SetBKColor(RGB(255,0,0));
		else
		{
			if((strcmp(prmDFlight->Stof, "S") == 0) || (strcmp(prmDFlight->Stof, "D") == 0))
				m_CE_DOfbl.SetBKColor(RGB(16,189,239));
			else
				m_CE_DOfbl.SetBKColor(RGB(255,255,255));
		}



		// color Slot
		if(strcmp(prmDFlight->Stsl, "U") == 0)
		{
			if(prmDFlight->Slot != TIMENULL)
				m_CE_DSlot.SetBKColor(RGB(255,0,0));
		}
		else
		{
			if(strcmp(prmDFlight->Stsl, "C") == 0)
				m_CE_DSlot.SetBKColor(RGB(16,189,239));
			else
				m_CE_DSlot.SetBKColor(RGB(255,255,255));
		}


		/*
		if((strcmp(prmDFlight->Org3, pcgHome) == 0) && (strcmp(prmDFlight->Des3, pcgHome) == 0)) 
			m_CS_Abflug.SetWindowText(GetString(IDS_STRING340) + CString(" ")+ prmDFlight->Stod.Format("%d.%m.%Y"));
		else
			m_CS_Abflug.SetWindowText(GetString(IDS_STRING338) + CString(" ")+ prmDFlight->Stod.Format("%d.%m.%Y"));
		*/
		

		m_DOrg3 = CString(pcgHome);



		//Weitere Flugnummern

		ogRotationDlgFlights.GetJfnoArray(&omDJfno, prmDFlight);

		pomDJfnoTable->ResetContent();
		

		rlAttribC1.Type = KT_STRING;
		rlAttribC1.Format = "XXXX";
		rlAttribC1.TextMinLenght = 0;
		rlAttribC1.TextMaxLenght = 4;
		rlAttribC1.Style = ES_UPPERCASE;

		rlAttribC2.Type = KT_STRING;
		rlAttribC2.Format = "#####";
		rlAttribC2.TextMinLenght = 0;
		rlAttribC2.TextMaxLenght = 5;

		rlAttribC3.Type = KT_STRING;
		rlAttribC3.Format = "X";
		rlAttribC3.TextMinLenght = 0;
		rlAttribC3.TextMaxLenght = 1;
		rlAttribC3.Style = ES_UPPERCASE;


		rlColumnData.SeparatorType = SEPA_NONE;
		rlColumnData.Font = &ogMS_Sans_Serif_8;
		rlColumnData.VerticalSeparator = SEPA_NORMAL;
		rlColumnData.HorizontalSeparator = SEPA_NONE;
		rlColumnData.Alignment = COLALIGN_LEFT;


		if(ogPrivList.GetStat("ROTATIONDLG_CB_DShowJfno") != '1')
			rlColumnData.BkColor = RGB(192,192,192);
		else
			rlColumnData.BkColor = RGB(255,255,255);


		for ( ilLc = 0; ilLc < omDJfno.GetSize(); ilLc++)
		{
			rlColumnData.Text = omDJfno[ilLc].Alc3;
			rlColumnData.Text.TrimRight();
			rlColumnData.EditAttrib = rlAttribC1;
			olColList.NewAt(olColList.GetSize(), rlColumnData);
			rlColumnData.Text = omDJfno[ilLc].Fltn;
			rlColumnData.Text.TrimRight();
			rlColumnData.EditAttrib = rlAttribC2;
			olColList.NewAt(olColList.GetSize(), rlColumnData);
			rlColumnData.Text = omDJfno[ilLc].Flns;
			rlColumnData.Text.TrimRight();
			rlColumnData.EditAttrib = rlAttribC3;
			olColList.NewAt(olColList.GetSize(), rlColumnData);
			pomDJfnoTable->AddTextLine(olColList, (void*)(NULL));
			olColList.DeleteAll();
		}
		for (ilLc = omDJfno.GetSize(); ilLc < 9; ilLc++)
		{
			rlColumnData.Text = "";
			rlColumnData.EditAttrib = rlAttribC1;
			olColList.NewAt(olColList.GetSize(), rlColumnData);
			rlColumnData.Text = "";
			rlColumnData.EditAttrib = rlAttribC2;
			olColList.NewAt(olColList.GetSize(), rlColumnData);
			rlColumnData.Text = "";
			rlColumnData.EditAttrib = rlAttribC3;
			olColList.NewAt(olColList.GetSize(), rlColumnData);
			pomDJfnoTable->AddTextLine(olColList, (void*)(NULL));
			olColList.DeleteAll();
		}
		pomDJfnoTable->DisplayTable();

/*
		if(omDJfno.GetSize() > 0)
			OnDshowjfno();
		else
			DShowJfnoButton();
*/

		// Vias
		CString olCaption = CString(prmDFlight->Flno);
		olCaption.TrimRight();
		if (olCaption.IsEmpty())
			olCaption = CString(prmDFlight->Csgn);

		if (!polRotationDViaDlg)
		{
			bool blEnable = true;
			if(ogPrivList.GetStat("ROTATIONDLG_CB_DShowVia") != '1')
				blEnable = false;

			polRotationDViaDlg = new RotationViaDlg(this, "D", olCaption, prmDFlight->Urno, prmDFlight->Vial, prmDFlight->Stod, bmLocalTime, blEnable);
			polRotationDViaDlg->Create(RotationViaDlg::IDD);
			polRotationDViaDlg->SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_DShowVia"));
		}
		else
			polRotationDViaDlg->SetData("D", olCaption, prmDFlight->Urno, prmDFlight->Vial, prmDFlight->Stod, bmLocalTime);

		if( CString(prmDFlight->Ftyp)	== "Z" || CString(prmDFlight->Ftyp)	== "B")
		{
			if (polRotationDViaDlg)
				polRotationDViaDlg->Enable(false);
		}
		else
		{
			if (polRotationDViaDlg)
				polRotationDViaDlg->Enable(true);
		}

/*
		if( CString(prmDFlight->Ftyp)	== "Z" || CString(prmDFlight->Ftyp)	== "B")
		{
			pomDViaCtrl->bmEdit = false;
		}
		else
		{
			pomDViaCtrl->bmEdit = true;
		}


		pomDViaCtrl->SetViaList(prmDFlight->Vial, prmDFlight->Stod, bmLocalTime);
*/


		omDAlc3 = CString(prmDFlight->Alc3);
		omDAlc2 = CString(prmDFlight->Alc2);

		m_DAlc3 = CString(prmDFlight->Flno);
		m_DAlc3 = m_DAlc3.Left(3);
		m_DAlc3.TrimRight();
		m_DFltn = CString(prmDFlight->Fltn);
		m_DFlns = CString(prmDFlight->Flns);
		m_DFlti = prmDFlight->Flti;

		m_CE_DFlti.SetBKColor(RGB(255,255,255));
		if ((int) strlen(prmDFlight->Dssf) > ogDssfFltiId && ogDssfFltiId != -1)
		{
			if (prmDFlight->Dssf[ogDssfFltiId] == 'U')
 			m_CE_DFlti.SetBKColor(RGB(255,0,0));
		}

		if(bgNatureAutoCalc)
		{
			m_CE_DTtyp.SetBKColor(RGB(255,255,255));
			if ((int) strlen(prmDFlight->Dssf) > ogDssfTtypId && ogDssfTtypId != -1)
			{
				if (prmDFlight->Dssf[ogDssfTtypId] == 'U')
					m_CE_DTtyp.SetBKColor(RGB(255,0,0));
			}
		}

		if(bgShowChock)
		{
			// color Chock of
			if(strcmp(prmDFlight->Stcf, "U") == 0)
				pomCEChocksOf->SetBKColor(RGB(255,0,0));
			else
			{
				if((strcmp(prmDFlight->Stcf, "S") == 0) || (strcmp(prmDFlight->Stcf, "D") == 0))
					pomCEChocksOf->SetBKColor(RGB(16,189,239));
				else
					pomCEChocksOf->SetBKColor(RGB(255,255,255));
			}
		}
		

		strcpy(pcmDDes4, prmDFlight->Des4);
		strcpy(pcmDDes3, prmDFlight->Des3);

		m_DDivr = CString(prmDFlight->Divr);

		m_DDes3 = CString(prmDFlight->Des3);
		m_DDes4 = CString(prmDFlight->Des4);
		m_DTtyp = CString(prmDFlight->Ttyp);
		m_DHtyp = CString(prmDFlight->Htyp);
		m_DStyp = CString(prmDFlight->Styp);
		m_DStev = CString(prmDFlight->Stev);
		m_DSte2 = CString(prmDFlight->Ste2);
		m_DSte3 = CString(prmDFlight->Ste3);
		m_DSte4 = CString(prmDFlight->Ste4);
		m_DStod = DateToHourDivString(prmDFlight->Stod, prmDFlight->Stod);
		m_DStoa = DateToHourDivString(prmDFlight->Stoa, prmDFlight->Stod);
		m_DEtdi = DateToHourDivString(prmDFlight->Etdi, prmDFlight->Stod);
		m_DEtod = DateToHourDivString(prmDFlight->Etod, prmDFlight->Stod);
		m_DIskd = DateToHourDivString(prmDFlight->Iskd, prmDFlight->Stod);
		

		if(bgCDMPhase1)
		{
			m_DTobt = DateToHourDivString(prmDFlight->Tobt, prmDFlight->Stod);
			m_DTobt += CString(" ") + CString(prmDFlight->Tobf);
			m_DTsat = DateToHourDivString(prmDFlight->Tsat, prmDFlight->Stod);

			m_CS_DISKD.SetWindowText("TOBT/TSAT");

		}		

		m_DNxti = DateToHourDivString(prmDFlight->Nxti, prmDFlight->Stod);
		m_DEtdc = DateToHourDivString(prmDFlight->Etdc, prmDFlight->Stod);
		m_DOfbl = DateToHourDivString(prmDFlight->Ofbl, prmDFlight->Stod);
		m_DAirb = DateToHourDivString(prmDFlight->Airb, prmDFlight->Stod);
		m_DSlot = DateToHourDivString(prmDFlight->Slot, prmDFlight->Stod);

		/*
		CString olN;
		CString olA;

		if(CString(prmDFlight->Dcd1).IsEmpty())
			m_DDcd1 = "";
		else
		{
			bool blRet = ogBCD.GetField("DEN", "DECN", "DECA", CString(prmDFlight->Dcd1), olN, olA ); 

			if ( bgDelaycodeNumeric)
				m_DDcd1 = olN;
			else
				m_DDcd1 = olA;
		}

		if(CString(prmDFlight->Dcd2).IsEmpty())
			m_DDcd2 = "";
		else
		{

			bool blRet = ogBCD.GetField("DEN", "DECN", "DECA", CString(prmDFlight->Dcd2), olN, olA ); 

			if ( bgDelaycodeNumeric)
				m_DDcd2 = olN;
			else
				m_DDcd2 = olA;

		}
		*/

		m_DDcd1 = CString(prmDFlight->Dcd1);
		m_DDcd2 = CString(prmDFlight->Dcd2);
		m_DDtd1 = CString(prmDFlight->Dtd1);
		m_DDtd2 = CString(prmDFlight->Dtd2);


		m_DRwyd = CString(prmDFlight->Rwyd);
		m_DIfrd = CString(prmDFlight->Ifrd);
		m_DLastChange = prmDFlight->Lstu.Format("  %d.%m.%Y / %H:%M  ") +  CString(prmDFlight->Useu);
		m_DLastChangeTime = CString(prmDFlight->Chgi);
//		m_DPdba = DateToHourDivString(prmDFlight->Pdba, prmDFlight->Stod);
//		m_DPdea = DateToHourDivString(prmDFlight->Pdea, prmDFlight->Stod);
		m_DBbfa = DateToHourDivString(prmDFlight->Bbfa, prmDFlight->Stod);
		m_DGd1x = DateToHourDivString(prmDFlight->Gd1x, prmDFlight->Stod);
		m_DGd2x = DateToHourDivString(prmDFlight->Gd2x, prmDFlight->Stod);
		m_DGd1y = DateToHourDivString(prmDFlight->Gd1y, prmDFlight->Stod);
		m_DGd2y = DateToHourDivString(prmDFlight->Gd2y, prmDFlight->Stod);
		m_DW1ba = DateToHourDivString(prmDFlight->W1ba, prmDFlight->Stod);
		m_DW3ba = DateToHourDivString(prmDFlight->W2ba, prmDFlight->Stod);
		m_DW1ea = DateToHourDivString(prmDFlight->W1ea, prmDFlight->Stod);
		m_DW3ea = DateToHourDivString(prmDFlight->W2ea, prmDFlight->Stod);
		m_DPstd = CString(prmDFlight->Pstd);
		m_DGtd1 = CString(prmDFlight->Gtd1);
		m_DGtd2 = CString(prmDFlight->Gtd2);
		m_DTgd1 = CString(prmDFlight->Tgd1);
		m_DTgd2 = CString(prmDFlight->Tgd2);
		m_DWro1 = CString(prmDFlight->Wro1);
		m_DWro3 = CString(prmDFlight->Wro2);
		m_DTwr1 = CString(prmDFlight->Twr1);

		CString olTerm;
		ogBCD.GetField("WRO", "WNAM", m_DWro3, "TERM", olTerm);
		m_DTwr3 = CString(olTerm);

		m_DRem1 = CString(prmDFlight->Rem1);
		m_DRem3 = CString(prmDFlight->Dela);

		m_DIfrd = CString(prmDFlight->Ifrd);
		if(m_DIfrd != "V")
			m_DIfrd = "";

		if (bgCnamAtr)
		{
			m_DCiFr = GetCnamExt(CString(prmDFlight->Ckif), CString(prmDFlight->Ckit));
		}
		else
		{
				if(CString(prmDFlight->Ckf2).IsEmpty())
					m_DCiFr = CString(prmDFlight->Ckif) + CString("-") + CString(prmDFlight->Ckit);
				else
					m_DCiFr = CString(prmDFlight->Ckif) + CString("-") + CString(prmDFlight->Ckit) + CString(" / ") + CString(prmDFlight->Ckf2) + CString("-") + CString(prmDFlight->Ckt2);
		}

		if (bgUseDepBelts) {
			m_DBlt1 = CString(prmDFlight->Blt1);
			m_DTmb1 = CString(prmDFlight->Tmb1);
			m_DB1ba = DateToHourDivString(prmDFlight->B1ba, prmDFlight->Stod);
			m_DB1ea = DateToHourDivString(prmDFlight->B1ea, prmDFlight->Stod);
			if (prmDFlight->Urno > 0) {
				//if(strcmp(prmDFlight->Ftyp, "X") == 0) {
					EnableDepBelts(true);
				//} else {
				//	EnableDepBelts(false);
				//}
			}

			
		}
		
		if (bgUseDepBelts) {
		m_CE_DBlt1.SetInitText(m_DBlt1);
		m_CE_DB1ba.SetInitText(m_DB1ba);
		m_CE_DB1ea.SetInitText(m_DB1ea);
		m_CE_DTmb1.SetInitText(m_DTmb1);
		}

		m_DBaz1 = CString(prmDFlight->Baz1);
		m_DBaz4 = CString(prmDFlight->Baz4);
		m_DBao1 = DateToHourDivString(prmDFlight->Bao1, prmDFlight->Stod);
		m_DBac1 = DateToHourDivString(prmDFlight->Bac1, prmDFlight->Stod);
		m_DBao4 = DateToHourDivString(prmDFlight->Bao4, prmDFlight->Stod);
		m_DBac4 = DateToHourDivString(prmDFlight->Bac4, prmDFlight->Stod);

		m_CE_DBaz1.SetInitText(m_DBaz1);
		m_CE_DBaz4.SetInitText(m_DBaz4);
		m_CE_DBao1.SetInitText(m_DBao1);
		m_CE_DBac1.SetInitText(m_DBac1);
		m_CE_DBao4.SetInitText(m_DBao4);
		m_CE_DBac4.SetInitText(m_DBac4);

		m_CE_DCsgn.SetInitText(prmDFlight->Csgn);
 
		m_CE_DCiFr.SetInitText(m_DCiFr);
		m_CE_DOrg3.SetInitText(pcgHome + CString(" - ") + pcgHome4);
		m_CE_DDes3.SetInitText(m_DDes3);
		m_CE_DDes4.SetInitText(m_DDes4);
		m_CE_DDivr.SetInitText(m_DDivr);
		m_CE_DAlc3.SetInitText(m_DAlc3);
		m_CE_DFltn.SetInitText(m_DFltn);
		m_CE_DFlns.SetInitText(m_DFlns);
		m_CE_DFlti.SetInitText(m_DFlti);
		m_CE_DTtyp.SetInitText(m_DTtyp);
		m_CE_DHtyp.SetInitText(m_DHtyp);
		m_CE_DStyp.SetInitText(m_DStyp);
		m_CE_DStev.SetInitText(m_DStev);
		m_CE_DSte2.SetInitText(m_DSte2);
		m_CE_DSte3.SetInitText(m_DSte3);
		m_CE_DSte4.SetInitText(m_DSte4);
		m_CE_DRem1.SetInitText(m_DRem1);
		m_CE_DRem3.SetInitText(m_DRem3);
		m_CE_DEtdi.SetInitText(m_DEtdi);
		m_CE_DStod.SetInitText(m_DStod);
		m_CE_DStoa.SetInitText(m_DStoa);
		m_CE_DOfbl.SetInitText(m_DOfbl);
		m_CE_DAirb.SetInitText(m_DAirb);
		m_CE_DEtdi.SetInitText(m_DEtdi);
		m_CE_DEtod.SetInitText(m_DEtod);
		m_CE_DEtdc.SetInitText(m_DEtdc);
		m_CE_DIskd.SetInitText(m_DIskd);


		if(bgCDMPhase1)
		{
			m_CE_DTobt.SetInitText(m_DTobt);
			m_CE_DTsat.SetInitText(m_DTsat);

			if(CString(prmDFlight->Tobf) == "C")
				m_CE_DTobt.SetBKColor(RGB(0,220,0));
			else
				m_CE_DTobt.SetBKColor(SILVER);
		}


		m_CE_DSlot.SetInitText(m_DSlot);
		m_CE_DNxti.SetInitText(m_DNxti);
		m_CE_DDtd1.SetInitText(m_DDtd1);
		m_CE_DDtd2.SetInitText(m_DDtd2);
		m_CE_DDcd1.SetInitText(m_DDcd1);
		m_CE_DDcd2.SetInitText(m_DDcd2);
		m_CE_DRwyd.SetInitText(m_DRwyd);
		m_CE_DIfrd.SetInitText(m_DIfrd);
		m_CE_DPstd.SetInitText(m_DPstd);
//		m_CE_DPdba.SetInitText(m_DPdba);
//		m_CE_DPdea.SetInitText(m_DPdea);
		m_CE_DGtd1.SetInitText(m_DGtd1);
		m_CE_DGtd2.SetInitText(m_DGtd2);
		m_CE_DTgd1.SetInitText(m_DTgd1);
		m_CE_DTgd2.SetInitText(m_DTgd2);
		m_CE_DGd1x.SetInitText(m_DGd1x);
		m_CE_DGd2x.SetInitText(m_DGd2x);
		m_CE_DGd1y.SetInitText(m_DGd1y);
		m_CE_DGd2y.SetInitText(m_DGd2y);
		m_CE_DWro1.SetInitText(m_DWro1);
		m_CE_DWro3.SetInitText(m_DWro3);
		m_CE_DTwr1.SetInitText(m_DTwr1);
		m_CE_DTwr3.SetInitText(m_DTwr3);
		m_CE_DW1ba.SetInitText(m_DW1ba);
		m_CE_DW3ba.SetInitText(m_DW3ba);
		m_CE_DW1ea.SetInitText(m_DW1ea);
		m_CE_DW3ea.SetInitText(m_DW3ea);
		m_CE_DBbfa.SetInitText(m_DBbfa);
		m_CE_DLastChange.SetInitText(m_DLastChange);
		m_CE_DLastChangeTime.SetInitText(m_DLastChangeTime);

	

			//Chocks On
		if(bgShowChock)
		{
		
			//pomCEChocksOf->SetBKColor(WHITE);
			pomCEChocksOf->SetTypeToTime(false, true);
			pomCEChocksOf->SetInitText(DateToHourDivString(prmDFlight->Chof, prmDFlight->Stod));
		}

		//For departure Mode of Payment
		if(bgShowPayDetail)
		{
			pomDModeOfPay->ShowWindow(TRUE);
		}

		polWnd = (CWnd*) GetDlgItem(IDC_BTCRC_PAY_DEP);	// TLE: UFIS 3768 
		if (polWnd)
			pomDModeOfPay->SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_DPay"));

		if(!bmInit)		
		{
			if(prmDFlight->Urno > 0)
			{
				omCcaData.Register();
				CString olFlnu;
				olFlnu.Format("%ld", prmDFlight->Urno);
				omCcaData.SetFilter(false, olFlnu);

				ReadCcaData();

				if(bgChuteDisplay)
				{
					omChaData.SetFilter(olFlnu);

					ReadChaData();
				}
			}
		}

		if(!bmInit && bgFIDSLogo)		
		{
			if(prmDFlight->Urno > 0)
			{
				ReadFlzData();
			}
		}


		DShowCinsTable();	
		DShowChaTable();	

		bmAutoSetBaz1 = false;
		bmAutoSetBaz4 = false;
//		bmAutoSetBaz1 = true;
//		bmAutoSetBaz4 = true;

		CString olBaz1;
		CString olBaz4;
		CCADATA *prlCca;

		CTime opStart = TIMENULL;
		CTime opEnd	  = TIMENULL;


		for( int i = 0; i < omDCins.GetSize(); i++)
		{
			prlCca = &omDCins[i];

			if(CString(prmDFlight->Flti) == "M")
			{
				if( !CString(prlCca->Ckic).IsEmpty())
				{
					if(ogBCD.GetField("CIC", "CNAM", prlCca->Ckic, "CICR") == "I")
						olBaz1 = ogBCD.GetField("CIC", "CNAM", prlCca->Ckic, "CBAZ");
					else
						olBaz4 = ogBCD.GetField("CIC", "CNAM", prlCca->Ckic, "CBAZ");
				}
			}
			else
			{
				if( !CString(prlCca->Ckic).IsEmpty())
				{
					olBaz1 = ogBCD.GetField("CIC", "CNAM", prlCca->Ckic, "CBAZ");
					if(!olBaz1.IsEmpty())
						break;
				}
			}
			if(prlCca->Ckba != TIMENULL)
			{
				opStart = prlCca->Ckba;
			}
			else
			{
				if(prlCca->Ckbs != TIMENULL)
				{
					opStart = prlCca->Ckbs;
				}
			}

			if(prlCca->Ckea != TIMENULL)
			{
				opEnd = prlCca->Ckea;
			}
			else
			{
				if(prlCca->Ckes != TIMENULL)
				{
					opEnd = prlCca->Ckes;
				}
			}
		}

		if( CString(prmDFlight->Baz1) == olBaz1 && prmDFlight->Bao1 == opStart && prmDFlight->Bac1 == opEnd)
			bmAutoSetBaz1 = true;

		if( CString(prmDFlight->Baz4) == olBaz4 && prmDFlight->Bao4 == opStart && prmDFlight->Bac4 == opEnd)
			bmAutoSetBaz4 = true;

		/*
		if(!CString(prmDFlight->Baz1).IsEmpty())
		{
			prmDFlight->Bao1 = opStart;
			prmDFlight->Bac1 = opEnd;
		}
		else
		{
			prmDFlight->Bao1 = TIMENULL;
			prmDFlight->Bac1 = TIMENULL;
		}

		if(!CString(prmDFlight->Baz4).IsEmpty())
		{
			prmDFlight->Bao4 = opStart;
			prmDFlight->Bac4 = opEnd;
		}
		else
		{
			prmDFlight->Bao4 = TIMENULL;
			prmDFlight->Bac4 = TIMENULL;
		}
		*/


	} // if(bpDeparture)


	if(prmAFlight->Urno > 0)
	{
		m_Regn = CString(prmAFlight->Regn);
		m_Act3 = CString(prmAFlight->Act3);
		m_Act5 = CString(prmAFlight->Act5);
		m_Ming = CString(prmAFlight->Ming);
	}
	else if(prmDFlight->Urno > 0)
	{
		m_Regn = CString(prmDFlight->Regn);
		m_Act3 = CString(prmDFlight->Act3);
		m_Act5 = CString(prmDFlight->Act5);
		m_Ming = CString(prmDFlight->Ming);
	}

	m_CE_Ming.SetInitText(m_Ming);
	m_CE_Act3.SetInitText(m_Act3);
	m_CE_Act5.SetInitText(m_Act5);
	m_CE_Regn.SetInitText(m_Regn);

	if (bgShowWingspan)
	{
		CString olTmp;
		CCSPtrArray<RecordSet> prlRecords;

		int ilCount = ogBCD.GetRecordsExt("ACT","ACT3","ACT5",m_Act3,m_Act5, &prlRecords);
	    if (ilCount == 1)
		{
			m_Acle = prlRecords[0].Values[ogBCD.GetFieldIndex("ACT", "ACLE")];
			m_Acws = prlRecords[0].Values[ogBCD.GetFieldIndex("ACT", "ACWS")];
		}

		m_CE_Acle.SetInitText(m_Acle);
		m_CE_Acws.SetInitText(m_Acws);

	}
	m_CL_ARemp.ResetContent();
	m_CL_DRemp.ResetContent();

	m_CL_ARemp.AddString(" ");
	m_CL_DRemp.AddString(" ");
	
	int ilAIndex = 0;	
	int ilDIndex = 0;	
	CString olCode;
	CString olItem;

	int ilCount = ogBCD.GetDataCount("FID_ARR");
	ogBCD.SetSort("FID_ARR", ogFIDRemarkField+"+", true);

	CString olCodeA = CString(prmAFlight->Remp);
	CString olCodeD = CString(prmDFlight->Remp);

	olCodeA.TrimRight();
	olCodeD.TrimRight();

	for(int i = 0; i < ilCount; i++)
	{
		olItem = ogBCD.GetFields("FID_ARR",i , "CODE," + ogFIDRemarkField, "  ", FILLBLANK);
		m_CL_ARemp.AddString(olItem);
		olCode = ogBCD.GetField("FID_ARR",i , "CODE" );
		
		if((olCode.Find(olCodeA) >= 0) && (!olCodeA.IsEmpty()))
			ilAIndex = i + 1;
	}

	ilCount = ogBCD.GetDataCount("FID_DEP");
	ogBCD.SetSort("FID_DEP", ogFIDRemarkField+"+", true);
	for(i = 0; i < ilCount; i++)
	{
		olItem = ogBCD.GetFields("FID_DEP",i , "CODE," + ogFIDRemarkField, "  ", FILLBLANK);
		m_CL_DRemp.AddString(olItem);
		olCode = ogBCD.GetField("FID_DEP",i , "CODE" );
		
		if((olCode.Find(olCodeD) >= 0) && (!olCodeD.IsEmpty()))
			ilDIndex = i + 1;
	}
	

	m_CL_ARemp.SetCurSel(ilAIndex);
	m_CL_DRemp.SetCurSel(ilDIndex);



	////////////// Gate logo

	if(prmDFlight != NULL)
	{
		CString olFlgu = omFlzData.GetGatFlgu("1");

		m_CL_DGatLogo.ResetContent();
		m_CL_DGatLogo.AddString(" ");

		ilDIndex = 0;

		ilCount = ogBCD.GetDataCount("FLG_GAT");
		ogBCD.SetSort("FLG_GAT", "LGSN+", true);
		for(i = 0; i < ilCount; i++)
		{
			olItem = ogBCD.GetFields("FLG_GAT",i , "LGSN,LGFN,URNO" , " ", FILLBLANK);
			m_CL_DGatLogo.AddString(olItem);
			
			if(!olFlgu.IsEmpty() && olItem.Find(olFlgu) > 0)
				ilDIndex = i + 1;
		}
		

		m_CL_DGatLogo.SetCurSel(ilDIndex);


		olFlgu = omFlzData.GetGatFlgu("2");

		m_CL_DGatLogo2.ResetContent();
		m_CL_DGatLogo2.AddString(" ");

		ilDIndex = 0;

		ilCount = ogBCD.GetDataCount("FLG_GAT");
		ogBCD.SetSort("FLG_GAT", "LGSN+", true);
		for(i = 0; i < ilCount; i++)
		{
			olItem = ogBCD.GetFields("FLG_GAT",i , "LGSN,LGFN,URNO" , " ", FILLBLANK);
			m_CL_DGatLogo2.AddString(olItem);
			
			if(!olFlgu.IsEmpty() && olItem.Find(olFlgu) > 0)
				ilDIndex = i + 1;
		}
		m_CL_DGatLogo2.SetCurSel(ilDIndex);
	}	
	////////////// Gate logo end



	////////////// CXX Reason

	if(prmDFlight != NULL && bgCxxReason)
	{

		m_CL_DCxxReason.ResetContent();
		m_CL_DCxxReason.AddString(" ");

		CString olCxxr = CString(prmDFlight->Cxxr);
		olCxxr.TrimLeft();

		ilDIndex = 0;

		ilCount = ogBCD.GetDataCount("CRC_CXX");
		ogBCD.SetSort("CRC_CXX", "CODE+", true);
		for(i = 0; i < ilCount; i++)
		{
			olItem = ogBCD.GetFields("CRC_CXX",i , "CODE,REMA,URNO" , "   ", FILLBLANK);
			m_CL_DCxxReason.AddString(olItem);
			if( olItem.Find(olCxxr) >= 0 && !olCxxr.IsEmpty())
				ilDIndex = i + 1;
		}
		
		m_CL_DCxxReason.SetCurSel(ilDIndex);

	}	
	////////////// Cxx Reason end


	pomDRgd1->ResetContent();
	pomDRgd1->AddString(" ");
	pomDRgd2->ResetContent();
	pomDRgd2->AddString(" ");

	CString olCodeRgd1 = CString(prmDFlight->Rgd1);
	CString olCodeRgd2 = CString(prmDFlight->Rgd2);
	int ilRgd1Index = 0;
	int ilRgd2Index = 0;


	ilCount = ogBCD.GetDataCount("FID_GAT");
	ogBCD.SetSort("FID_GAT", ogFIDRemarkField+"+", true);
	for(i = 0; i < ilCount; i++)
	{
		olItem = ogBCD.GetFields("FID_GAT",i , "CODE," + ogFIDRemarkField, "  ", FILLBLANK);
		pomDRgd1->AddString(olItem);
		pomDRgd2->AddString(olItem);
		olCode = ogBCD.GetField("FID_GAT",i , "CODE" );
		
		if((olCode.Find(olCodeRgd1) >= 0) && (!olCodeRgd1.IsEmpty()))
			ilRgd1Index = i + 1;
		if((olCode.Find(olCodeRgd2) >= 0) && (!olCodeRgd2.IsEmpty()))
			ilRgd2Index = i + 1;
	}


	pomDRgd1->SetCurSel(ilRgd1Index);
	pomDRgd2->SetCurSel(ilRgd2Index);



	//Update the cash button
	char clPaid;
	CString olAlc3;
	// AFTPAID can be 'B','K','P'
	if(prmDFlight->Urno != 0)
	{
		olAlc3 = prmDFlight->Alc3;
		if(strlen(prmDFlight->Paid) == 0)
			clPaid = ' ';
		else
			clPaid = prmDFlight->Paid[0];
	}
	else
	{
		olAlc3 = prmAFlight->Alc3;
		if(strlen(prmAFlight->Paid) == 0)
			clPaid = ' ';
		else
			clPaid = prmAFlight->Paid[0];
	}

	


		UpdateCashButton(olAlc3, clPaid);
	

/*	
	int ilCount = ogBCD.GetDataCount("FID");
	ogBCD.SetSort("FID", ogFIDRemarkField+"+", true);


	CString olCodeA = CString(prmAFlight->Remp);
	CString olCodeD = CString(prmDFlight->Remp);

	olCodeA.TrimRight();
	olCodeD.TrimRight();

	for(int i = 0; i < ilCount; i++)
	{
		olItem = ogBCD.GetFields("FID",i , "CODE," + ogFIDRemarkField, "  ", FILLBLANK);
		m_CL_ARemp.AddString(olItem);
		m_CL_DRemp.AddString(olItem);
		olCode = ogBCD.GetField("FID",i , "CODE" );
		
		if((olCode.Find(olCodeA) >= 0) && (!olCodeA.IsEmpty()))
			ilAIndex = i + 1;
		
		if((olCode.Find(olCodeD) >= 0) && (!olCodeD.IsEmpty()))
			ilDIndex = i + 1;
	}
	
	m_CL_ARemp.SetCurSel(ilAIndex);
	m_CL_DRemp.SetCurSel(ilDIndex);
*/

		if(bpArrival && bpDeparture)
		{
			bmChanged = false;
			EnableGlobal();
			EnableArrival();
			EnableDeparture();
		}
		else
		{
			if(bpArrival)
			{
				EnableGlobal();
				EnableArrival();
		}
		else
		{
			EnableGlobal();
			EnableDeparture();
		}
	}

	if (bgCnamAtr)
	{
		m_CE_DCinsBorder.ShowWindow(SW_HIDE);

		if (ogFactor_CCA.IsEmpty())
		{
			m_ComboDcins.ShowWindow(SW_HIDE);
		}

		if (!ogFactor_CCA.IsEmpty())
		{
			m_CE_DCinsBorder.ShowWindow(SW_HIDE);
			m_ComboDcins.ShowWindow(SW_SHOW);

			CRect olRectBorder;
			CRect olRectBorderExt;
			m_CE_DCiFr.GetWindowRect( olRectBorder );
			m_ComboDcins.GetWindowRect( olRectBorderExt );
			ScreenToClient(olRectBorder);
			ScreenToClient(olRectBorderExt);

			olRectBorderExt.top = olRectBorder.top;
			m_ComboDcins.MoveWindow(olRectBorderExt,TRUE);
		}
		else
		{
			m_CE_DCinsBorder.ShowWindow(SW_HIDE);

			CRect olRectBorder;
			CRect olRectBorderExt;
			m_CE_DCiFr.GetWindowRect( olRectBorder );
			m_CE_DCinsBorderExt.GetWindowRect( olRectBorderExt );
			ScreenToClient(olRectBorder);
			ScreenToClient(olRectBorderExt);

			olRectBorder.right = olRectBorderExt.right;
			m_CE_DCiFr.MoveWindow(olRectBorder,TRUE);
		}
	}
	else
	{
		m_CE_DCinsBorderExt.ShowWindow(SW_HIDE);
	}

	// Set Towing-Button colour
	CheckTowingButton();
	CheckViaButton();
	CheckCodeShareButton();
	CheckREQButton();
	CheckFlightReportButton();

	//is it a postflight?
	HandlePostFlight(); 

	//dataChanges
	SetHistoryNamesForEdit();
	InitHistoryCombos();
	InitDcinsCombo();

	m_CB_Ok.SetFocus();


} 

void RotationDlg::SetArrPermitsButton()
{
	if(pomArrPermitsButton == NULL)
		return;
	
	CString olWhere = ""; //lgp
	CString olUrno = "";
	CString olPern = ""; 
	CString olStox = CTimeToDBString(ConvertFlightPermitTime(prmAFlight->Stoa), prmAFlight->Stoa);
	
	
	if(ogFpeData.FtypEnabled(prmAFlight->Ftyp))
	{
		pomArrPermitsButton->EnableWindow(TRUE);
		
		FPEDATA *prlFpe = NULL;
		if(bgShowADHOC || bgSeasonShowADHOC)
		{
			prlFpe = ogFpeData.GetBestPermitForFlight(ConvertFlightPermitTime(prmAFlight->Stoa), true, prmAFlight->Regn, prmAFlight->Alc3, prmAFlight->Fltn, prmAFlight->Flns,prmAFlight->Csgn, &omArrFlightPermitInfo);
			if(prlFpe != NULL)
			{
				olUrno.Format("%ld", prlFpe->Urno);//lgptest
				olPern = prlFpe->Pern;
			}
		}
		else
		{
			prlFpe = ogFpeData.GetBestPermitForFlight(ConvertFlightPermitTime(prmAFlight->Stoa), true, prmAFlight->Regn, prmAFlight->Alc3, prmAFlight->Fltn, prmAFlight->Flns, &omArrFlightPermitInfo);
			if(prlFpe != NULL)
			{
				olUrno.Format("%ld", prlFpe->Urno);//lgptest
				olPern = prlFpe->Pern;
			}
		}

		if(prlFpe != NULL)		// TLE: UFIS 3697
			strcpy(omTmpAPer, prlFpe->Pern);	// Might be either 'Permit Reqd' or valid Permit Number
		else
			strcpy(omTmpAPer, "");

		if(prlFpe != NULL && (strcmp(prlFpe->Pern, "Permit Reqd") != 0))
		{
			pomArrPermitsButton->SetColors(LIME,GRAY,WHITE);
		}
		else
		{
			if(bgShowADHOC || bgSeasonShowADHOC)
			{
				if(strcmp(prmAFlight->Adho, "X") == 0)
				{
					pomArrPermitsButton->SetColors(RED,GRAY,WHITE);
					if(!olPern.IsEmpty() && strcmp(olPern, "Permit Reqd") !=0)
						pomArrPermitsButton->SetColors(LIME,GRAY,WHITE);
				}
				else
				{
					pomArrPermitsButton->SetColors(SILVER,GRAY,WHITE);
				}
			}
			else
			{
				pomArrPermitsButton->SetColors(RED,GRAY,WHITE);
			}
		}
	}
	else
	{
		pomArrPermitsButton->EnableWindow(FALSE);
		pomArrPermitsButton->SetColors(GRAY,GRAY,WHITE);
	}
}

void RotationDlg::SetDepPermitsButton()
{
	if(pomDepPermitsButton == NULL)
		return;
	
	CString olWhere = ""; //lgp
	CString olUrno = "";
	CString olPern = ""; 
	CString olStox = CTimeToDBString(ConvertFlightPermitTime(prmDFlight->Stod), prmDFlight->Stod);
	
	if(ogFpeData.FtypEnabled(prmDFlight->Ftyp))
	{
		pomDepPermitsButton->EnableWindow(TRUE);
		FPEDATA *prlFpe = NULL;
		if(bgShowADHOC || bgSeasonShowADHOC)
		{
			prlFpe = ogFpeData.GetBestPermitForFlight(ConvertFlightPermitTime(prmDFlight->Stod), false, prmDFlight->Regn, prmDFlight->Alc3, prmDFlight->Fltn, prmDFlight->Flns,prmDFlight->Csgn, &omDepFlightPermitInfo);
			if(prlFpe != NULL)
			{
				olUrno.Format("%ld", prlFpe->Urno);//lgptest
				olPern = prlFpe->Pern;
			}
		}
		else
		{
			prlFpe = ogFpeData.GetBestPermitForFlight(ConvertFlightPermitTime(prmDFlight->Stod), false, prmDFlight->Regn, prmDFlight->Alc3, prmDFlight->Fltn, prmDFlight->Flns, &omDepFlightPermitInfo);
			if(prlFpe != NULL)
			{
				olUrno.Format("%ld", prlFpe->Urno);//lgptest
				olPern = prlFpe->Pern;
			}
		}

		if(prlFpe != NULL)		// TLE: UFIS 3697
			strcpy(omTmpDPer, prlFpe->Pern);	// Might be either 'Permit Reqd' or valid Permit Number
		else
			strcpy(omTmpDPer, "");
		
		if(prlFpe != NULL && (strcmp(prlFpe->Pern, "Permit Reqd") != 0))
		{
			pomDepPermitsButton->SetColors(LIME,GRAY,WHITE);
		}
		else
		{
			if(bgShowADHOC || bgSeasonShowADHOC)
			{
				if(strcmp(prmDFlight->Adho, "X") == 0)
				{
					pomDepPermitsButton->SetColors(RED,GRAY,WHITE);
					if(!olPern.IsEmpty() && strcmp(olPern, "Permit Reqd") !=0)
						pomDepPermitsButton->SetColors(LIME,GRAY,WHITE);
				}
				else
					pomDepPermitsButton->SetColors(SILVER,GRAY,WHITE);				
			}
			else
			{
				pomDepPermitsButton->SetColors(RED,GRAY,WHITE);
			}
		}
	}
	else
	{
		pomDepPermitsButton->EnableWindow(FALSE);
		pomDepPermitsButton->SetColors(GRAY,GRAY,WHITE);
	}
	
}
 
LRESULT RotationDlg::OnEditDbClk( UINT wParam, LPARAM lParam)
{

	CCS_TRY

	CCSEDITNOTIFY *prlNotify = (CCSEDITNOTIFY*)lParam;

	if (prlNotify->Name.IsEmpty()) 
		return 0L;

	CallHistory(prlNotify->Name);

	CCS_CATCH_ALL	

	return 0L;
}


bool RotationDlg::InitHistoryCombos()
{
	if(prmAFlight->Urno > 0) 
	{
		m_ComboAHistory.ResetContent();
		CString olFieldList = GetString(IDS_AHISTORY);

		CStringArray olFieldArray;
		int ilFieldArray = ExtractItemList(olFieldList, &olFieldArray, ',');

		for (int l=0; l<olFieldArray.GetSize(); l++)
		{
			m_ComboAHistory.AddString(olFieldArray.GetAt(l));
		}
	}

	if(prmDFlight->Urno > 0) 
	{
		m_ComboDHistory.ResetContent();
		CString olFieldList = GetString(IDS_DHISTORY);

		CStringArray olFieldArray;
		int ilFieldArray = ExtractItemList(olFieldList, &olFieldArray, ',');

		for (int l=0; l<olFieldArray.GetSize(); l++)
		{
			m_ComboDHistory.AddString(olFieldArray.GetAt(l));
		}
	}

	return true;
}


bool RotationDlg::SetHistoryNamesForEdit()
{
//	if(prmAFlight->Urno > 0) 
	{
		m_CE_ABas1.SetName("AFTABAS1D");
		m_CE_ABae1.SetName("AFTABAE1D");
		m_CE_ABas2.SetName("AFTABAS2D");
		m_CE_ABae2.SetName("AFTABAE2D");
		m_CE_AAlc3.SetName("AFTAALC3C");
		m_CE_AFltn.SetName("AFTAFLTNC");
		m_CE_AFlns.SetName("AFTAFLNSC");
		m_CE_AFlti.SetName("AFTAFLTIC");
		m_CE_ATtyp.SetName("AFTATTYPC");
		m_CE_AStyp.SetName("AFTASTYPC");
		m_CE_AStev.SetName("AFTASTEVC");
		m_CE_ASte2.SetName("AFTASTE2C");
		m_CE_ASte3.SetName("AFTASTE3C");
		m_CE_ASte4.SetName("AFTASTE4C");
		m_CE_AOrg3.SetName("AFTAORG4C");
		m_CE_AOrg3L.SetName("AFTAORG3C");
		m_CE_AStoa.SetName("AFTASTOAD");
		m_CE_AStod.SetName("AFTASTODD");
		m_CE_AEtai.SetName("AFTAETAID");
		m_CE_APsta.SetName("AFTAPSTAC");
		m_CE_AEtdi.SetName("AFTAETDID");
		m_CE_AOfbl.SetName("AFTAOFBLD");
		m_CE_AAirb.SetName("AFTAAIRBD");
		m_CE_AEtoa.SetName("AFTAETOAD");
		m_CE_AOnbe.SetName("AFTAONBED");
		m_CE_ANxti.SetName("AFTANXTID");
		m_CE_ALand.SetName("AFTALANDD");
		m_CE_AOnbl.SetName("AFTAONBLD");
		m_CE_ATmoa.SetName("AFTATMOAD");
		m_CE_ARwya.SetName("AFTARWYAC");
		m_CE_AIfra.SetName("AFTAIFRAC");
		m_CE_ADcd1.SetName("AFTADCD1C");
		m_CE_ADcd2.SetName("AFTADCD2C");
		m_CE_ADtd1.SetName("AFTADTD1C");
		m_CE_ADtd2.SetName("AFTADTD2C");
		m_CE_ABbaa.SetName("AFTABBAAC");
		m_CE_AGta1.SetName("AFTAGTA1C");
		m_CE_AGta2.SetName("AFTAGTA2C");
		m_CE_AGa1x.SetName("AFTAGA1XD");
		m_CE_AGa2x.SetName("AFTAGA2XD");
		m_CE_AGa1y.SetName("AFTAGA1YD");
		m_CE_AGa2y.SetName("AFTAGA2YD");
		m_CE_ABlt1.SetName("AFTABLT1C");
		m_CE_ABlt2.SetName("AFTABLT2C");
		m_CE_AB1ba.SetName("AFTAB1BAD");
		m_CE_AB2ba.SetName("AFTAB2BAD");
		m_CE_AB1ea.SetName("AFTAB1EAD");
		m_CE_AB2ea.SetName("AFTAB2EAD");
		m_CE_AHtyp.SetName("AFTAHTYPC");

		if (bgUseDepBelts) {
		m_CE_DBlt1.SetName("AFTABLT1C");
		m_CE_DB1ba.SetName("AFTAB1BAD");
		m_CE_DB1ea.SetName("AFTAB1EAD");
		}

		if (strcmp(pcgHome, "ATH") == 0)
		{
//			m_CE_AFBag.SetName("AFTABAS1D");
//			m_CE_ALBag.SetName("AFTABAE1D");
		}

		m_CE_AExt1.SetName("AFTAEXT1C");
		m_CE_AExt2.SetName("AFTAEXT2C");
		m_CE_ARem1.SetName("AFTAREM1C");
		m_CE_ARem3.SetName("AFTADELAC");
		m_CE_ADivr.SetName("AFTADIVRC");
		m_CE_ACsgn.SetName("AFTACSGNC");

		m_CE_Act3.SetName("AFTRACT3C");
		m_CE_Act5.SetName("AFTRACT5C");
		m_CE_Regn.SetName("AFTRREGNC");
		m_CE_Ming.SetName("AFTRMINGC");

	
	}
//	if(prmAFlight->Urno > 0) 
	{
		m_CE_DAlc3.SetName("AFTDALC3C");
		m_CE_DFltn.SetName("AFTDFLTNC");
		m_CE_DFlns.SetName("AFTDFLNSC");
		m_CE_DFlti.SetName("AFTDFLTIC");
		m_CE_DTtyp.SetName("AFTDTTYPC");
		m_CE_DStyp.SetName("AFTDSTYPC");
		m_CE_DStev.SetName("AFTDSTEVC");
		m_CE_DSte2.SetName("AFTDSTE2C");
		m_CE_DSte3.SetName("AFTDSTE3C");
		m_CE_DSte4.SetName("AFTDSTE4C");
		m_CE_DDes3.SetName("AFTDDES3C");
		m_CE_DDes4.SetName("AFTDDES4C");
		m_CE_DStoa.SetName("AFTDSTOAD");
		m_CE_DStod.SetName("AFTDSTODD");
		m_CE_DEtdi.SetName("AFTDETDID");
		m_CE_DPstd.SetName("AFTDPSTDC");
		m_CE_DOfbl.SetName("AFTDOFBLD");
		m_CE_DAirb.SetName("AFTDAIRBD");
		m_CE_DEtod.SetName("AFTDETODD");
		m_CE_DEtdc.SetName("AFTDETDCD");
		m_CE_DSlot.SetName("AFTDSLOTD");
		m_CE_DRwyd.SetName("AFTDRWYDC");
		m_CE_DIfrd.SetName("AFTDIFRDC");
		m_CE_DIskd.SetName("AFTDISKDC");
		m_CE_DNxti.SetName("AFTDNXTID");
		m_CE_DBbfa.SetName("AFTDBBFAD");
		m_CE_DDcd1.SetName("AFTDDCD1C");
		m_CE_DDcd2.SetName("AFTDDCD2C");
		m_CE_DDtd1.SetName("AFTDDTD1C");
		m_CE_DDtd2.SetName("AFTDDTD2C");

		if (strcmp(pcgHome, "ATH") == 0)
		{
			m_CE_DGtd1.SetName("AFTDGD1PC");
			m_CE_DGtd2.SetName("AFTDGD2PC");
		}
		else
		{
			m_CE_DGtd1.SetName("AFTDGTD1C");
			m_CE_DGtd2.SetName("AFTDGTD2C");
		}

		m_CE_DGd1x.SetName("AFTDGD1XD");
		m_CE_DGd2x.SetName("AFTDGD2XD");
		m_CE_DGd1y.SetName("AFTDGD1YD");
		m_CE_DGd2y.SetName("AFTDGD2YD");
		m_CE_DWro1.SetName("AFTDWRO1C");
		m_CE_DW1ba.SetName("AFTDW1BAD");
		m_CE_DW1ea.SetName("AFTDW1EAD");
		m_CE_DWro3.SetName("AFTDWRO2C");
		m_CE_DW3ba.SetName("AFTDW2BAD");
		m_CE_DW3ea.SetName("AFTDW2EAD");
		m_CE_DBaz1.SetName("AFTDBAZ1C");
		m_CE_DBao1.SetName("AFTDBAO1D");
		m_CE_DBac1.SetName("AFTDBAC1D");
		m_CE_DBaz4.SetName("AFTDBAZ4C");
		m_CE_DBao4.SetName("AFTDBAO4D");
		m_CE_DBac4.SetName("AFTDBAC4D");
		m_CE_DRem1.SetName("AFTDREM1C");
		m_CE_DRem3.SetName("AFTDDELAC");
		m_CE_DCsgn.SetName("AFTDCSGNC");
		m_CE_DHtyp.SetName("AFTDHTYPC");

		m_CE_Act3.SetName("AFTRACT3C");
		m_CE_Act5.SetName("AFTRACT5C");
		m_CE_Regn.SetName("AFTRREGNC");
		m_CE_Ming.SetName("AFTRMINGC");
	}

	return true;
}


void RotationDlg::CallHistory(CString opField)
{
	if (opField.IsEmpty())
		return;

	CString olTable = opField.Left(3);
	CString olField = opField.Mid(4,4);
	CString olAdid = opField.Mid(3,1);
	CString olType = opField.Right(1);

	//----------
	char clStat;
	clStat = ogPrivList.GetStat("ROTATIONDLG_CL_AHistory");

	if(olAdid == "A")
	{
		if(clStat != '1')
			return;
	}

	clStat = ogPrivList.GetStat("ROTATIONDLG_CL_DHistory");

	if(olAdid == "D")
	{
		if(clStat != '1')
			return;
	}

	clStat = ogPrivList.GetStat("ROTATIONDLG_CL_RHistory"); 

	if(olAdid == "R")
	{
		if(clStat != '1')
			return;
	}
	//----------


	char Abuffer[20];
	char Dbuffer[20];


	char pclConfigPath[256];
	char pclExcelPath[256];

	if (getenv("CEDA") == NULL)
		strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
	else
		strcpy(pclConfigPath, getenv("CEDA"));

	GetPrivateProfileString(ogAppName, "DATA_CHANGES", "DEFAULT",
		pclExcelPath, sizeof pclExcelPath, pclConfigPath);


	if(!strcmp(pclExcelPath, "DEFAULT"))
	{
		MessageBox(GetString(IDS_DATACHANGES), GetString(ST_FEHLER), MB_ICONERROR);
		return;
	}
	char slRunTxt[512] = ""; 

	CString olTimeMode = "U";
	if (bmLocalTime)
		olTimeMode = "L";

	if (!bgUTCCONVERTIONS)
		olTimeMode = "L";

	CString olTowingUrnos = "0";
	if(olAdid == "A" || olAdid == "D")
	{
		char pclSelection[256];
		char pclUrno[12];

		//if(olField.CompareNoCase("XXXX") == 0) //Meant for "All Fields" history
		{			
			RotGDlgCedaFlightData olGDlgCedaFlightData;

			if (olAdid == "D")
				sprintf(pclSelection, "WHERE RKEY = %ld AND FTYP = '%c'", olGDlgCedaFlightData.GetRkey(prmDFlight->Urno), 'T');
			else
				sprintf(pclSelection, "WHERE RKEY = %ld AND FTYP = '%c'", olGDlgCedaFlightData.GetRkey(prmAFlight->Urno), 'T');

			CCSPtrArray<ROTGDLGFLIGHTDATA> olRotGDlgFlightData;
			if(olGDlgCedaFlightData.ReadSpecial(&olRotGDlgFlightData,pclSelection,"URNO,FTYP,RKEY",true) == true)
			{
				olTowingUrnos.Empty();
				for(int i = 0 ; i < olRotGDlgFlightData.GetSize(); i++)
				{
					ROTGDLGFLIGHTDATA* prpFlight = &olRotGDlgFlightData[i];
					olTowingUrnos += CString(itoa(prpFlight->Urno,pclUrno,10)) + (i < (olRotGDlgFlightData.GetSize() - 1) ? ";" : "");
				}
				if(olAdid != "A")
				{
					if(olTowingUrnos.GetLength() > 1)
					{
						olTowingUrnos += ";" + CString(ltoa(prmDFlight->Urno,Dbuffer,10));
					}
					else
					{
						 olTowingUrnos = CString(ltoa(prmDFlight->Urno,Dbuffer,10));
					}
				}				
			}
		}
	}

	if (olAdid == "A")
	{
		sprintf(slRunTxt,"%s;%s,%s,%s,%s,%s,%s,%s,%s", ltoa(prmAFlight->Urno,Abuffer,10), olTowingUrnos, olTable, olField, "FIELD", olTimeMode, "", olType, CString(pcgUser));
	}
	else
	{
		if (olAdid == "D")
		{
			if (strcmp(pcgHome, "WAW") == 0 && olTowingUrnos != "0")
				sprintf(slRunTxt,"%s;%s,%s,%s,%s,%s,%s,%s,%s", "0", olTowingUrnos, olTable, olField, "FIELD", olTimeMode, "", olType, CString(pcgUser));
			else
				sprintf(slRunTxt,"%s;%s,%s,%s,%s,%s,%s,%s,%s", "0", ltoa(prmDFlight->Urno,Dbuffer,10), olTable, olField, "FIELD", olTimeMode, "", olType, CString(pcgUser));
		}
		else
		{
			sprintf(slRunTxt,"%s;%s,%s,%s,%s,%s,%s,%s,%s", ltoa(prmAFlight->Urno,Abuffer,10), olTowingUrnos/*ltoa(prmDFlight->Urno,Dbuffer,10)*/, olTable, olField, "FIELD", olTimeMode, "", olType, CString(pcgUser));
		}
	}


	char *args[4];
	args[0] = "child";
	args[1] = slRunTxt;
	args[2] = NULL;
	args[3] = NULL;
	int ilReturn = _spawnv(_P_NOWAIT,pclExcelPath,args);
}


void RotationDlg::OnSelchangeAHistory() 
{
	CString olOperator;
	int ilCur = m_ComboAHistory.GetCurSel();
	m_ComboAHistory.GetLBText(ilCur, olOperator);

	CString olFieldList = GetString(IDS_AHISTORY_FIELD);

	CStringArray olFieldArray;
	int ilFieldArray = ExtractItemList(olFieldList, &olFieldArray, ',');

	if (olFieldArray.GetSize() > ilCur)
	{
		CString olField = olFieldArray.GetAt(ilCur);
		if (olField.IsEmpty())
			return;

		CallHistory(olField);
	}
	else
		return;

}

void RotationDlg::OnSelchangeDHistory() 
{
	CString olOperator;
	int ilCur = m_ComboDHistory.GetCurSel();
	m_ComboDHistory.GetLBText(ilCur, olOperator);

	CString olFieldList = GetString(IDS_DHISTORY_FIELD);

	CStringArray olFieldArray;
	int ilFieldArray = ExtractItemList(olFieldList, &olFieldArray, ',');

	if (olFieldArray.GetSize() > ilCur)
	{
		CString olField = olFieldArray.GetAt(ilCur);
		if (olField.IsEmpty())
			return;

		CallHistory(olField);
	}
	else
		return;

}

 
void RotationDlg::ArrivalTifd(CTime& opTime) 
{
	opTime = TIMENULL;

	if(!m_AAirb.IsEmpty())
	{
		opTime = HourStringToDate(m_AAirb, prmAFlight->Stoa);
	}
	else
	{
		if(!m_AOfbl.IsEmpty())
		{
			opTime = HourStringToDate(m_AOfbl, prmAFlight->Stoa);
		}
		else
		{
			if(!m_AEtdi.IsEmpty())
			{
				opTime = HourStringToDate(m_AEtdi, prmAFlight->Stoa);
			}
			else
			{
				if(!m_AStod.IsEmpty())
				{
					opTime = HourStringToDate(m_AStod, prmAFlight->Stoa);
				}
			}
		}
	}
}

void RotationDlg::ArrivalTifa(CTime& opTime) 
{
	opTime = TIMENULL;
	if(!m_ALand.IsEmpty())
	{
		opTime = HourStringToDate(m_ALand, prmAFlight->Stoa);
	}
	else
	{
		if(!m_AOnbl.IsEmpty())
		{
			opTime = HourStringToDate(m_AOnbl, prmAFlight->Stoa);
		}
		else
		{
			if(!m_AEtai.IsEmpty())
			{
				opTime = HourStringToDate(m_AEtai, prmAFlight->Stoa);
			}
			else
			{
				if(!m_AStoa.IsEmpty())
				{
					opTime = prmAFlight->Stoa;
				}
			}
		}
	}
}

void RotationDlg::DepartureTifd(CTime& opTime) 
{
	opTime = TIMENULL;
	if(!m_DAirb.IsEmpty())
	{
		opTime = HourStringToDate(m_DAirb, prmDFlight->Stod);
	}
	else
	{
		if(!m_DOfbl.IsEmpty())
		{
			opTime = HourStringToDate(m_DOfbl, prmDFlight->Stod);
		}
		else
		{
			if(!m_DEtdi.IsEmpty())
			{
				opTime = HourStringToDate(m_DEtdi, prmDFlight->Stod);
			}
			else
			{
				if(!m_DStod.IsEmpty())
				{
					opTime = prmDFlight->Stod;
				}
			}
		}
	}
}

void RotationDlg::DepartureTifa(CTime& opTime) 
{
	opTime = TIMENULL;
	if(!m_DStoa.IsEmpty())
	{
		opTime = HourStringToDate(m_DStoa, prmDFlight->Stod);
	}
}


void RotationDlg::ArrivalSchedTifd(CTime& opTime) 
{
	opTime = TIMENULL;

	if(!m_AEtdi.IsEmpty())
	{
		opTime = HourStringToDate(m_AEtdi, prmAFlight->Stoa);
	}
	else
	{
		if(!m_AStod.IsEmpty())
		{
			opTime = HourStringToDate(m_AStod, prmAFlight->Stoa);
		}
	}
}

void RotationDlg::ArrivalSchedTifa(CTime& opTime) 
{
	opTime = TIMENULL;

	if(!m_AEtai.IsEmpty())
	{
		opTime = HourStringToDate(m_AEtai, prmAFlight->Stoa);
	}
	else
	{
		if(!m_AStoa.IsEmpty())
		{
			opTime = prmAFlight->Stoa;
		}
	}
}

void RotationDlg::DepartureSchedTifd(CTime& opTime) 
{
	opTime = TIMENULL;
	if(!m_DEtdi.IsEmpty())
	{
		opTime = HourStringToDate(m_DEtdi, prmDFlight->Stod);
	}
	else
	{
		if(!m_DStod.IsEmpty())
		{
			opTime = prmDFlight->Stod;
		}
	}
}

void RotationDlg::DepartureSchedTifa(CTime& opTime) 
{
	opTime = TIMENULL;
	if(!m_DStoa.IsEmpty())
	{
		opTime = HourStringToDate(m_DStoa, prmDFlight->Stod);
	}
}


void RotationDlg::ArrivalAktTifd(CTime& opTime) 
{
	opTime = TIMENULL;

	if(!m_AAirb.IsEmpty())
	{
		opTime = HourStringToDate(m_AAirb, prmAFlight->Stoa);
	}
	else
	{
		if(!m_AOfbl.IsEmpty())
		{
			opTime = HourStringToDate(m_AOfbl, prmAFlight->Stoa);
		}
	}
}

void RotationDlg::ArrivalAktTifa(CTime& opTime) 
{
	opTime = TIMENULL;
	if(!m_ALand.IsEmpty())
	{
		opTime = HourStringToDate(m_ALand, prmAFlight->Stoa);
	}
	else
	{
		if(!m_AOnbl.IsEmpty())
		{
			opTime = HourStringToDate(m_AOnbl, prmAFlight->Stoa);
		}
	}
}

void RotationDlg::DepartureAktTifd(CTime& opTime) 
{
	opTime = TIMENULL;
	if(!m_DAirb.IsEmpty())
	{
		opTime = HourStringToDate(m_DAirb, prmDFlight->Stod);
	}
	else
	{
		if(!m_DOfbl.IsEmpty())
		{
			opTime = HourStringToDate(m_DOfbl, prmDFlight->Stod);
		}
	}
}

void RotationDlg::DepartureAktTifa(CTime& opTime) 
{
	opTime = TIMENULL;
	if(!m_DStoa.IsEmpty())
	{
		opTime = HourStringToDate(m_DStoa, prmDFlight->Stod);
	}
}


/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
// Fill the SEASONFLIGHTDATArecords
//
void RotationDlg::FillFlightData() 
{
	CTime olTime;

	//killfocus - check for the curent field
	NextDlgCtrl(); 
	PrevDlgCtrl();

	m_CB_Ok.SetFocus();


	///////////////////////////////////////////
	// globals
	//
	m_CE_Regn.GetWindowText(m_Regn);
	m_Regn.TrimLeft();
	m_Regn.TrimRight();

	m_CE_Ming.GetWindowText(m_Ming);
	m_CE_Act3.GetWindowText(m_Act3);
	m_CE_Act5.GetWindowText(m_Act5);


	if(prmAFlight->Urno)//lgp
	{
		CString olWhere = "";
		CString olUrno;
		long llUrnoA = prmAFlight->Urno;

		olUrno.Format("%ld", llUrnoA);
		olWhere.Format("WHERE URNO = '%s'", olUrno);
		ogBCD.Read(CString("AFT"), olWhere);

		ogDQCSA = ogBCD.GetField("AFT", "URNO", olUrno, "DQCS");

		char *dqcsTmp;
		if(bgValuDQCS && ((strcmp(ogDQCSA, "C")==0) || (strcmp(ogDQCSA, "S")==0) || ((dqcsTmp = strstr(ogDQCSA, "MC"))!=NULL) ))
		{
			AfxMessageBox("The ARRIVAL FLIGHT DATA(including COMMON DATA) is freezed and won't be updated!");
		}
		else
		{
			FillFlightDataArrival();
		}
	}
	if(prmDFlight->Urno)
	{
		FillFlightDataDepart();
	}	
	
}

void RotationDlg::FillFlightDataDepart()
{
	CTime olTime;
	CString olFlno;
	CString olFlnoT;
	CString olText;
	int ilCurSel;
	CString olTmp;
	int ilJcnt = 0;	
	CString olJfno;
	int ilLines = 0;
	int i = 0;
	CString olAlc3;
	CString olFltn;
	CString olFlns;

	///////////////////////////////////////////
	// Abflug
	//

	m_DCxx = m_CB_DCxx.GetCheck(); 
	m_DVip = m_CB_DVip.GetCheck(); 
	m_DFpsd = m_CB_DFpsd.GetCheck(); 
	m_DDiverted = m_CB_DDiverted.GetCheck(); 
	m_DRerouted = m_CB_DRerouted.GetCheck(); 
	m_DGd2d= m_CB_DGd2d.GetCheck(); 
	
	m_CE_DAlc3.GetWindowText(m_DAlc3);
	m_CE_DFltn.GetWindowText(m_DFltn);
	m_CE_DFlns.GetWindowText(m_DFlns);
	m_CE_DFlti.GetWindowText(m_DFlti);
	m_CE_DCsgn.GetWindowText(m_DCsgn);
	m_CE_DDes3.GetWindowText(m_DDes3);
	m_CE_DDes4.GetWindowText(m_DDes4);
	m_CE_DOrg3.GetWindowText(m_DOrg3);
	m_CE_DTtyp.GetWindowText(m_DTtyp);
	m_CE_DHtyp.GetWindowText(m_DHtyp);
	m_CE_DStyp.GetWindowText(m_DStyp);
	m_CE_DStev.GetWindowText(m_DStev);
	m_CE_DSte2.GetWindowText(m_DSte2);
	m_CE_DSte3.GetWindowText(m_DSte3);
	m_CE_DSte4.GetWindowText(m_DSte4);
	m_CE_DRem1.GetWindowText(m_DRem1);
	m_CE_DRem3.GetWindowText(m_DRem3);
	m_CE_DEtdi.GetWindowText(m_DEtdi);
	m_CE_DStod.GetWindowText(m_DStod);
	m_CE_DStoa.GetWindowText(m_DStoa);
	m_CE_DOfbl.GetWindowText(m_DOfbl);
	m_CE_DAirb.GetWindowText(m_DAirb);
	m_CE_DEtdi.GetWindowText(m_DEtdi);
	m_CE_DEtod.GetWindowText(m_DEtod);
	m_CE_DEtdc.GetWindowText(m_DEtdc);
	m_CE_DIskd.GetWindowText(m_DIskd);
	m_CE_DSlot.GetWindowText(m_DSlot);
	m_CE_DNxti.GetWindowText(m_DNxti);
	m_CE_DDtd1.GetWindowText(m_DDtd1);
	m_CE_DDtd2.GetWindowText(m_DDtd2);
	m_CE_DDcd1.GetWindowText(m_DDcd1);
	m_CE_DDcd2.GetWindowText(m_DDcd2);
	m_CE_DRwyd.GetWindowText(m_DRwyd);
	m_CE_DIfrd.GetWindowText(m_DIfrd);
	m_CE_DPstd.GetWindowText(m_DPstd);
//	m_CE_DPdba.GetWindowText(m_DPdba);
//	m_CE_DPdea.GetWindowText(m_DPdea);
	m_CE_DGtd1.GetWindowText(m_DGtd1);
	m_CE_DGtd2.GetWindowText(m_DGtd2);
	m_CE_DTgd1.GetWindowText(m_DTgd1);
	m_CE_DTgd2.GetWindowText(m_DTgd2);
	m_CE_DGd1x.GetWindowText(m_DGd1x);
	m_CE_DGd2x.GetWindowText(m_DGd2x);
	m_CE_DGd1y.GetWindowText(m_DGd1y);
	m_CE_DGd2y.GetWindowText(m_DGd2y);
	m_CE_DWro1.GetWindowText(m_DWro1);
	m_CE_DWro3.GetWindowText(m_DWro3);
	m_CE_DTwr1.GetWindowText(m_DTwr1);
	m_CE_DTwr3.GetWindowText(m_DTwr3);
	m_CE_DW1ba.GetWindowText(m_DW1ba);
	m_CE_DW3ba.GetWindowText(m_DW3ba);
	m_CE_DW1ea.GetWindowText(m_DW1ea);
	m_CE_DW3ea.GetWindowText(m_DW3ea);
	m_CE_DBbfa.GetWindowText(m_DBbfa);
	m_CE_DDivr.GetWindowText(m_DDivr);


	m_CE_DBaz1.GetWindowText(m_DBaz1);
	m_CE_DBaz4.GetWindowText(m_DBaz4);
	m_CE_DBao1.GetWindowText(m_DBao1);
	m_CE_DBac1.GetWindowText(m_DBac1);
	m_CE_DBao4.GetWindowText(m_DBao4);
	m_CE_DBac4.GetWindowText(m_DBac4);

	if (bgUseDepBelts) {
		m_CE_DBlt1.GetWindowText(m_DBlt1);
		m_CE_DTmb1.GetWindowText(m_DTmb1);
		m_CE_DB1ba.GetWindowText(m_DB1ba);
		m_CE_DB1ea.GetWindowText(m_DB1ea);
	prmDFlight->B1ba = HourStringToDate( m_DB1ba, prmDFlight->Stod);
	prmDFlight->B1ea = HourStringToDate( m_DB1ea, prmDFlight->Stod);
	}
	
	//always IFR if not VFR ("V")
	if(m_DIfrd != "V")
		m_DIfrd = "";
	m_CE_DIfrd.SetWindowText(m_DIfrd);

	strcpy(prmDFlight->Baz1, m_DBaz1);
	strcpy(prmDFlight->Baz4, m_DBaz4);
	prmDFlight->Bao1 = HourStringToDate(m_DBao1, prmDFlight->Stod);
	prmDFlight->Bac1 = HourStringToDate(m_DBac1, prmDFlight->Stod);
	prmDFlight->Bao4 = HourStringToDate(m_DBao4, prmDFlight->Stod);
	prmDFlight->Bac4 = HourStringToDate(m_DBac4, prmDFlight->Stod);

	if(m_CE_DTtyp.IsChanged())
		strcpy(prmDFlight->Sttt,"U");

	char *dqcsTmp;
	if(!(bgValuDQCS && ((strcmp(ogDQCSA, "C")==0) || (strcmp(ogDQCSA, "S")==0) || ((dqcsTmp = strstr(ogDQCSA, "MC"))!=NULL) )))
	{
		strcpy(prmDFlight->Regn, m_Regn);
		strcpy(prmDFlight->Ming, m_Ming);
		strcpy(prmDFlight->Act3, m_Act3);
		strcpy(prmDFlight->Act5, m_Act5);
	}


	olFlno = ogRotationDlgFlights.CreateFlno(m_DAlc3, m_DFltn, m_DFlns); 
	olFlnoT = olFlno;
	olFlnoT.TrimRight();
	if (strcmp(prmDFlight->Flno, olFlnoT) != 0 && strcmp(prmDFlight->Flno, olFlno) != 0)		
		strcpy(prmDFlight->Flno, olFlno);


	strcpy(prmDFlight->Alc3,omDAlc3);
	strcpy(prmDFlight->Alc2,omDAlc2);
	strcpy(prmDFlight->Fltn,m_DFltn);
	strcpy(prmDFlight->Flti,m_DFlti);
	strcpy(prmDFlight->Csgn,m_DCsgn);
	strcpy(prmDFlight->Flns,m_DFlns);
	strcpy(prmDFlight->Ttyp,m_DTtyp);
	strcpy(prmDFlight->Htyp,m_DHtyp);
	strcpy(prmDFlight->Styp,m_DStyp);
	strcpy(prmDFlight->Stev,m_DStev);
	strcpy(prmDFlight->Ste2,m_DSte2);
	strcpy(prmDFlight->Ste3,m_DSte3);
	strcpy(prmDFlight->Ste4,m_DSte4);
	strcpy(prmDFlight->Des3,pcmDDes3);
	strcpy(prmDFlight->Org3,pcgHome);
	strcpy(prmDFlight->Des4,pcmDDes4);
	strcpy(prmDFlight->Org4,pcgHome4);
	strcpy(prmDFlight->Divr,m_DDivr);


	prmDFlight->Stod = HourStringToDate( m_DStod, prmDFlight->Stod);
//	prmDFlight->Stod = HourStringToDate( m_DStod, prmDFlightSave->Stod);
	prmDFlight->Stoa = HourStringToDate( m_DStoa, prmDFlight->Stod);
	prmDFlight->Etod = HourStringToDate(m_DEtod, prmDFlight->Stod);
	prmDFlight->Iskd = HourStringToDate(m_DIskd, prmDFlight->Stod);
	prmDFlight->Nxti = HourStringToDate(m_DNxti, prmDFlight->Stod);
	prmDFlight->Etdc = HourStringToDate(m_DEtdc, prmDFlight->Stod);

//	GetDayOfWeek(prmDFlight->Stod, prmDFlight->Dood);



	if(m_CE_DOfbl.IsChanged())	
	{
		if(!m_DOfbl.IsEmpty())
		{
			prmDFlight->Ofbu = HourStringToDate(m_DOfbl, prmDFlight->Stod);
			prmDFlightSave->Ofbu = TIMENULL; // Because this field must be saved in the DB!
		}
		else
		{
			if( strcmp(prmDFlight->Stof, "D") == 0)
			{
				prmDFlight->Ofbd = TIMENULL;	

			}
 			else if( strcmp(prmDFlight->Stof, "S") == 0)
			{
				prmDFlight->Ofbs = TIMENULL;	

			}else
			//if( strcmp(prmDFlight->Stof, "U") == 0)
			{
				prmDFlight->Ofbu = TIMENULL;	

			}
		}
	}

	
	
	if(m_CE_DAirb.IsChanged())	
	{
		if(!m_DAirb.IsEmpty())
		{
			prmDFlight->Airu = HourStringToDate(m_DAirb, prmDFlight->Stod);
			prmDFlightSave->Airu = TIMENULL; // Because this field must be saved in the DB!
		}
		else
		{
			if( strcmp(prmDFlight->Stab, "D") == 0)
			{
				prmDFlight->Aird = TIMENULL;	

			}else
			if( strcmp(prmDFlight->Stab, "A") == 0)
			{
				prmDFlight->Aira = TIMENULL;	

			}else
			//if( strcmp(prmDFlight->Stab, "U") == 0)
			{
				prmDFlight->Airu = TIMENULL;	

			}
		}
	}
	
	
	
	if(m_CE_DSlot.IsChanged())	
	{
		if(!m_DSlot.IsEmpty())
		{
			prmDFlight->Slou = HourStringToDate(m_DSlot, prmDFlight->Stod);
			prmDFlightSave->Slou = TIMENULL; // Because this field must be saved in the DB!
		}
		else
		{
			if( strcmp(prmDFlight->Stsl, "C") == 0)
			{
				prmDFlight->Ctot = TIMENULL;	

			}else
			//if( strcmp(prmDFlight->Stsl, "U") == 0)
			{
				prmDFlight->Slou = TIMENULL;	

			}
		}
	}
	
	
	
	if(m_CE_DEtdi.IsChanged())	
	{
		if(!m_DEtdi.IsEmpty())
		{
			prmDFlight->Etdu = HourStringToDate(m_DEtdi, prmDFlight->Stod);
			prmDFlightSave->Etdu = TIMENULL; // Because this field must be saved in the DB!
		}
		else
		{
			if (strcmp(pcgHome, "FCO") == 0)
			{
				prmDFlight->Etdu = TIMENULL;	
				if (prmDFlightSave->Etdu == TIMENULL)
					prmDFlightSave->Etdu = GetCurrentTime();
			}
			else
			{
				if( strcmp(prmDFlight->Sted, "A") == 0)
				{
					prmDFlight->Etda = TIMENULL;	

				}else
				if( strcmp(prmDFlight->Sted, "E") == 0)
				{
					prmDFlight->Etde = TIMENULL;	

				}else
				if( strcmp(prmDFlight->Sted, "C") == 0)
				{
					prmDFlight->Etdc = TIMENULL;	

				}else
				if( strcmp(prmDFlight->Sted, "U") == 0)
				{
					prmDFlight->Etdu = TIMENULL;	
				}
				if( strcmp(prmDFlight->Sted, "S") == 0)
				{
					prmDFlight->Etdi = TIMENULL;	
				}
			}
		}
	}

	if(m_DDtd1.GetLength() > 0)
		FillLeftByChar(m_DDtd1, '0' , 4);

	if(m_DDtd1.GetLength() > 0)
		FillLeftByChar(m_DDtd2, '0' , 4);


	strcpy(prmDFlight->Dtd1, m_DDtd1);
	strcpy(prmDFlight->Dtd2, m_DDtd2);

	strcpy(prmDFlight->Dcd1, m_DDcd1);
	strcpy(prmDFlight->Dcd2, m_DDcd2);
	strcpy(prmDFlight->Rwyd, m_DRwyd);
	strcpy(prmDFlight->Ifrd, m_DIfrd);
	strcpy(prmDFlight->Pstd, m_DPstd);
	strcpy(prmDFlight->Gtd1, m_DGtd1);
	strcpy(prmDFlight->Gtd2, m_DGtd2);
	strcpy(prmDFlight->Tgd1, m_DTgd1);
	strcpy(prmDFlight->Tgd2, m_DTgd2);
	strcpy(prmDFlight->Wro1, m_DWro1);
	strcpy(prmDFlight->Wro2, m_DWro3);
	strcpy(prmDFlight->Twr1, m_DTwr1);
//	strcpy(prmDFlight->Twr2, m_DTwr3);
	prmDFlight->Gd1x = HourStringToDate( m_DGd1x, prmDFlight->Stod);
	prmDFlight->Gd2x = HourStringToDate( m_DGd2x, prmDFlight->Stod);
//	prmDFlight->Pdba = HourStringToDate( m_DPdba, prmDFlight->Stod);
//	prmDFlight->Pdea = HourStringToDate( m_DPdea, prmDFlight->Stod);
	prmDFlight->Gd1y = HourStringToDate( m_DGd1y, prmDFlight->Stod);
	prmDFlight->Gd2y = HourStringToDate( m_DGd2y, prmDFlight->Stod);
	prmDFlight->W1ba = HourStringToDate( m_DW1ba, prmDFlight->Stod);
	prmDFlight->W2ba = HourStringToDate( m_DW3ba, prmDFlight->Stod);
	prmDFlight->W1ea = HourStringToDate( m_DW1ea, prmDFlight->Stod);
	prmDFlight->W2ea = HourStringToDate( m_DW3ea, prmDFlight->Stod);
	prmDFlight->Bbfa = HourStringToDate( m_DBbfa, prmDFlight->Stod);
	strcpy(prmDFlight->Rem1,m_DRem1);
	strcpy(prmDFlight->Dela,m_DRem3);
/*	strcpy(prmDFlight->Ming,m_Ming);
	strcpy(prmDFlight->Act3,m_Act3);
	strcpy(prmDFlight->Act5,m_Act5);*/


	if (bgDailyFixedRes) 
	{
		if(pomCBFPSD->GetCheck() == TRUE)
			strcpy(prmDFlight->Fpsd, "X");
		else
			strcpy(prmDFlight->Fpsd, "");

		if(pomCBFGD1->GetCheck() == TRUE)
			strcpy(prmDFlight->Fgd1, "X");
		else
			strcpy(prmDFlight->Fgd1, "");

		if(pomCBFGD2->GetCheck() == TRUE)
			strcpy(prmDFlight->Fgd2, "X");
		else
			strcpy(prmDFlight->Fgd2, "");

	
	}
	else
	{
		if(m_DFpsd == TRUE)
			strcpy(prmDFlight->Fpsd, "X");
		else
			strcpy(prmDFlight->Fpsd, " ");
	}

	if(bgShowAOG)
	{
		if(pomCBDAOG->GetCheck() == TRUE)
		{
			strcpy(prmDFlight->Faog, "X");
		}
		else
		{
			strcpy(prmDFlight->Faog, "");
		}

	}

	if(bgShowADHOC)
	{
		if(pomCBDAHOC->GetCheck() == TRUE)
		{
			strcpy(prmDFlight->Adho, "X");
		}
		else
		{
			strcpy(prmDFlight->Adho, "");
		}
	}
		
	if (bgUseDepBelts) {
		strcpy(prmDFlight->Blt1,m_DBlt1);
		strcpy(prmDFlight->Tmb1, m_DTmb1);
		prmDFlight->B1ba = HourStringToDate( m_DB1ba, prmDFlight->Stod);
		prmDFlight->B1ea = HourStringToDate( m_DB1ea, prmDFlight->Stod);
	}



	if (bmChanged)
		strcpy(prmDFlight->Ftyp, "O");

	if(m_DCxx == TRUE)
		strcpy(prmDFlight->Ftyp, "X");

	if(m_DDiverted == TRUE)
		strcpy(prmDFlight->Ftyp, "D");

	if(m_DRerouted == TRUE)
		strcpy(prmDFlight->Ftyp, "R");

	if(m_CB_DRetFlight.GetCheck())
		strcpy(prmDFlight->Ftyp, "Z");

	if(m_CB_DRetTaxi.GetCheck())
		strcpy(prmDFlight->Ftyp, "B");


	if(m_DVip == TRUE)
		strcpy(prmDFlight->Vipd, "X");
	else
		strcpy(prmDFlight->Vipd, " ");

	


	if(m_CB_DNfes.GetCheck())
	{
		m_CE_DNfes.GetWindowText(olText);
		olTime = HourStringToDate( olText, prmDFlight->Stod);
		prmDFlight->Nfes = olTime;
	}
	else
	{
		prmDFlight->Nfes = TIMENULL;
	}


	if(bgCxxReason)
	{
		if((ilCurSel = m_CL_DCxxReason.GetCurSel()) == CB_ERR || strcmp(prmDFlight->Ftyp, "X") != 0)
		{
			strcpy(prmDFlight->Cxxr, "");
		}
		else
		{
			m_CL_DCxxReason.GetLBText(ilCurSel, olTmp);
			olTmp = olTmp.Left(5);
			olTmp.TrimRight();
			if(!olTmp.IsEmpty())
				strcpy(prmDFlight->Cxxr, olTmp);
			else
				strcpy(prmDFlight->Cxxr, "");
		}

	}




	if(m_DGd2d == TRUE)
		strcpy(prmDFlight->Gd2d, "X");
	else
		strcpy(prmDFlight->Gd2d, "");


	strcpy(prmDFlight->Rem1,m_DRem1);
	if(m_DRem1.IsEmpty())
		strcpy(prmDFlight->Isre,"");
	else
		strcpy(prmDFlight->Isre,"+");



	if((ilCurSel = m_CL_DRemp.GetCurSel()) == CB_ERR)
		strcpy(prmDFlight->Remp, "");
	else
	{
		m_CL_DRemp.GetLBText(ilCurSel, olTmp);
		olTmp = olTmp.Left(4);
		olTmp.TrimRight();
		if(!olTmp.IsEmpty())
			strcpy(prmDFlight->Remp, olTmp);
		else
			strcpy(prmDFlight->Remp, "");
	}



	if((ilCurSel = pomDRgd1->GetCurSel()) == CB_ERR)
		strcpy(prmDFlight->Rgd1, "");
	else
	{
		pomDRgd1->GetLBText(ilCurSel, olTmp);
		olTmp = olTmp.Left(4);
		olTmp.TrimRight();
		if(!olTmp.IsEmpty())
			strcpy(prmDFlight->Rgd1, olTmp);
		else
			strcpy(prmDFlight->Rgd1, "");
	}

	if((ilCurSel = pomDRgd2->GetCurSel()) == CB_ERR)
		strcpy(prmDFlight->Rgd2, "");
	else
	{
		pomDRgd1->GetLBText(ilCurSel, olTmp);
		olTmp = olTmp.Left(4);
		olTmp.TrimRight();
		if(!olTmp.IsEmpty())
			strcpy(prmDFlight->Rgd2, olTmp);
		else
			strcpy(prmDFlight->Rgd2, "");
	}


	///////////////////////////////////////////////
	// weitere Flugnummern abflug
	//

	strcpy(prmDFlight->Jfno, "");
	ilJcnt = 0;
	olJfno = "";

	ilLines = pomDJfnoTable->GetLinesCount();
	for( i = 0; i < ilLines; i++)
	{
		pomDJfnoTable->GetTextFieldValue(i, 0, olAlc3);
		pomDJfnoTable->GetTextFieldValue(i, 1, olFltn);
		pomDJfnoTable->GetTextFieldValue(i, 2, olFlns);
		if(!olAlc3.IsEmpty() || !olFltn.IsEmpty())
		{
			olJfno +=  ogRotationDlgFlights.CreateFlno(olAlc3, olFltn, olFlns);
			ilJcnt++;
		}
	}
	olJfno.TrimRight();
	strcpy(prmDFlight->Jfno, olJfno);
	if (ilJcnt > 0)
		itoa(ilJcnt, prmDFlight->Jcnt, 10);


	///////////////////////////////////////////////
	// vias ankunft
	//
	if (polRotationDViaDlg)
		polRotationDViaDlg->GetViaList(prmDFlight->Vial);
/*
	olViaList = pomDViaCtrl->GetViaList();
	olViaList.TrimRight();
	strcpy(prmDFlight->Vial, olViaList);
*/
	
	CString olMax = "   ";
	CString olMin = "zzz";

	CString olCkic;
	CString olCkit;
	CString olDisp;
	CTime olCkba;
	CTime olCkea;
	CTime olCkbs;
	CTime olCkes;
	
	CCADATA *prlCca;
	CString olTmpStr;
	CTime olDRef; 
	CString olCicLogo;
	

	if(pomDCinsTable != NULL)
	{
		olDRef = prmDFlightSave->Stod;
		//olDRef = prmDFlight->Stod;

		CString olgggg = olDRef.Format("%Y%m%d %H:%M");

		if(bmLocalTime) ogBasicData.UtcToLocal(olDRef);


		ilLines = pomDCinsTable->GetLinesCount();
		for( i = 0; i < ilLines; i++)
		{
			if (bgCnamAtr)
			{
				pomDCinsTable->GetTextFieldValue(i, 0, olCkic);
				pomDCinsTable->GetTextFieldValue(i, 2, olCkit);

				pomDCinsTable->GetTextFieldValue(i, 3, olTmp);
				olCkbs = HourStringToDate(olTmp, olDRef);
				if(bmLocalTime) ogBasicData.LocalToUtc(olCkbs);
				
				pomDCinsTable->GetTextFieldValue(i, 4, olTmp);
				olCkes = HourStringToDate(olTmp, olDRef); 
				if(bmLocalTime) ogBasicData.LocalToUtc(olCkes);

				pomDCinsTable->GetTextFieldValue(i, 5, olTmp);
				olCkba = HourStringToDate(olTmp, olDRef); 
				if(bmLocalTime) ogBasicData.LocalToUtc(olCkba);

				pomDCinsTable->GetTextFieldValue(i, 6, olTmp);
				olCkea = HourStringToDate(olTmp, olDRef); 
				if(bmLocalTime) ogBasicData.LocalToUtc(olCkea);

				pomDCinsTable->GetTextFieldValue(i, 7, olDisp);
				
				if(bgFIDSLogo)
				{
					olCicLogo = "";
					pomDCinsTable->GetTextFieldValue(i, 8, olCicLogo);
					omCicLogoArray[i] = olCicLogo;
				}

			}
			else
			{
				pomDCinsTable->GetTextFieldValue(i, 0, olCkic);
				pomDCinsTable->GetTextFieldValue(i, 1, olCkit);

				pomDCinsTable->GetTextFieldValue(i, 2, olTmp);
				olCkbs = HourStringToDate(olTmp, olDRef);
				if(bmLocalTime) ogBasicData.LocalToUtc(olCkbs);
				
				pomDCinsTable->GetTextFieldValue(i, 3, olTmp);
				olCkes = HourStringToDate(olTmp, olDRef); 
				if(bmLocalTime) ogBasicData.LocalToUtc(olCkes);

				pomDCinsTable->GetTextFieldValue(i, 4, olTmp);
				olCkba = HourStringToDate(olTmp, olDRef); 
				if(bmLocalTime) ogBasicData.LocalToUtc(olCkba);

				pomDCinsTable->GetTextFieldValue(i, 5, olTmp);
				olCkea = HourStringToDate(olTmp, olDRef); 
				if(bmLocalTime) ogBasicData.LocalToUtc(olCkea);

				pomDCinsTable->GetTextFieldValue(i, 6, olDisp);

				if(bgFIDSLogo)
				{
					olCicLogo = "";
					pomDCinsTable->GetTextFieldValue(i, 7, olCicLogo);
					omCicLogoArray[i] = olCicLogo;
				}

			}

			if(i >= omDCins.GetSize())
			{
				prlCca = new CCADATA;
				omDCins.Add(prlCca);

				// set cca stod
				prlCca->Stod = olDRef;
				if(bmLocalTime) ogBasicData.LocalToUtc(prlCca->Stod);

				// set cca flno
				olTmpStr = prmDFlight->Flno;
				olTmpStr.TrimRight();
				strcpy(prlCca->Flno, olTmpStr);
			
				// set cca act3
				strcpy(prlCca->Act3, prmDFlight->Act3);			


			}
			else
			{
				prlCca = &omDCins[i];
			}

			strcpy(prlCca->Ckic, olCkic);
			strcpy(prlCca->Ckit, olCkit);
			strcpy(prlCca->Disp, olDisp);
			prlCca->Ckba = olCkba;
 			prlCca->Ckea = olCkea;
			prlCca->Ckbs = olCkbs;
 			prlCca->Ckes = olCkes;
 			prlCca->Coix = olCicCoIxs[i];


			olCkic.TrimRight();

			if((olCkic < olMin) && !olCkic.IsEmpty())
				olMin = olCkic;

			if((olCkic > olMax) && !olCkic.IsEmpty())
				olMax = olCkic;
		
		
		}

		olMax.TrimRight();
		olMin.TrimRight();

		// in Athens the flight set CKIF and CKIT automatically !!
		if (strcmp(pcgHome, "ATH") != 0 && strcmp(pcgHome, "LIS") != 0)
		{
			if(olMax != "   " && !olMax.IsEmpty())
				strcpy(prmDFlight->Ckit, olMax);
			else
				strcpy(prmDFlight->Ckit, "");
		
			if(olMin != "zzz" && !olMin.IsEmpty())
				strcpy(prmDFlight->Ckif, olMin);
			else
				strcpy(prmDFlight->Ckif, "");
		}
	}

	/////////////////////////////////////////////////////////////////////////
	//// Get carousel times
	CString olBaz1 = "";
	CString olBaz4 = "";
	CTime opStart = TIMENULL;
	CTime opEnd	  = TIMENULL;
	CTime Start = TIMENULL;
	CTime End	  = TIMENULL;

//	strcpy(prmDFlight->Flti,"M");
	//bool
	for( i = 0; i < omDCins.GetSize(); i++)
	{
		prlCca = &omDCins[i];

		if( !CString(prlCca->Ckic).IsEmpty())
		{
			if(CString(prmDFlight->Flti) == "M")
			{
				if( !CString(prlCca->Ckic).IsEmpty())
				{
					if(ogBCD.GetField("CIC", "CNAM", prlCca->Ckic, "CICR") == "I")
						olBaz1 = ogBCD.GetField("CIC", "CNAM", prlCca->Ckic, "CBAZ");
					else
						olBaz4 = ogBCD.GetField("CIC", "CNAM", prlCca->Ckic, "CBAZ");
				}
			}
			else
			{
				if( !CString(prlCca->Ckic).IsEmpty())
				{
					olBaz1 = ogBCD.GetField("CIC", "CNAM", prlCca->Ckic, "CBAZ");
					//if(!olBaz1.IsEmpty())
					//	break;
				}
			}

			if(prlCca->Ckba != TIMENULL)
			{
				Start = prlCca->Ckba;
			}
			else
			{
				if(prlCca->Ckbs != TIMENULL)
				{
					Start = prlCca->Ckbs;
				}
			}

			if(prlCca->Ckea != TIMENULL)
			{
				End = prlCca->Ckea;
			}
			else
			{
				if(prlCca->Ckes != TIMENULL)
				{
					End = prlCca->Ckes;
				}
			}

			if (Start != TIMENULL && opStart == TIMENULL)
				opStart = Start;

			if (Start < opStart)
				opStart = Start;

			if (End > opEnd)
				opEnd = End;
		}
	}
	//// END (get carousel times)
	////////////////////////////////////////////////////////////////////////

	bool olBaz1IsChanged = m_CE_DBaz1.IsChanged();
	bool olBao1IsChanged = m_CE_DBao1.IsChanged();
	bool olBac1IsChanged = m_CE_DBac1.IsChanged();



	if( bmAutoSetBaz1 && (CString(prmDFlight->Baz1).IsEmpty() || olBaz1.IsEmpty() ||(!m_CE_DBaz1.IsChanged() && !m_CE_DBao1.IsChanged() && !m_CE_DBac1.IsChanged())))
	{
		if (m_CE_DBaz1.IsChanged())
			m_CE_DBaz1.GetWindowText(olBaz1);

		strcpy(prmDFlight->Baz1, olBaz1);
		if (!olBaz1.IsEmpty())
		{
			if(opStart != TIMENULL && !m_CE_DBao1.IsChanged())
				prmDFlight->Bao1 = opStart;
			if(opEnd != TIMENULL && !m_CE_DBac1.IsChanged())
				prmDFlight->Bac1 = opEnd;
		}
		else
		{
			prmDFlight->Bao1 = TIMENULL;
			prmDFlight->Bac1 = TIMENULL;
		}
	}



	if(bmAutoSetBaz4 && (CString(prmDFlight->Baz4).IsEmpty() || olBaz4.IsEmpty() ||
		( !m_CE_DBaz4.IsChanged() && !m_CE_DBao4.IsChanged() && !m_CE_DBac4.IsChanged())))
	{ 
		if (m_CE_DBaz4.IsChanged())
			m_CE_DBaz4.GetWindowText(olBaz4);		

		strcpy(prmDFlight->Baz4, olBaz4);
		if (!olBaz4.IsEmpty())
		{
			if(opStart != TIMENULL && !m_CE_DBao4.IsChanged())
				prmDFlight->Bao4 = opStart;
			if(opEnd != TIMENULL && !m_CE_DBac4.IsChanged())
				prmDFlight->Bac4 = opEnd;
		}
		else
		{
			prmDFlight->Bao4 = TIMENULL;
			prmDFlight->Bac4 = TIMENULL;
		}
	}
}

void RotationDlg::FillFlightDataArrival()
{
	CTime olTime;

	///////////////////////////////////////////
	// Ankunft
	//
	m_GoAround = m_CB_GoAround.GetCheck(); 

	m_ACxx = m_CB_ACxx.GetCheck(); 
	m_AVip = m_CB_AVip.GetCheck(); 
	m_AFpsa = m_CB_AFpsa.GetCheck(); 
	m_ADiverted = m_CB_ADiverted.GetCheck(); 
	m_ARerouted = m_CB_ARerouted.GetCheck(); 


	m_CE_AAlc3.GetWindowText(m_AAlc3);
	m_CE_AFltn.GetWindowText(m_AFltn);
	m_CE_AFlns.GetWindowText(m_AFlns);
	m_CE_AFlti.GetWindowText(m_AFlti);
	m_CE_ACsgn.GetWindowText(m_ACsgn);
	m_CE_ATtyp.GetWindowText(m_ATtyp);
	m_CE_AHtyp.GetWindowText(m_AHtyp);
	m_CE_AStyp.GetWindowText(m_AStyp);
	m_CE_AStev.GetWindowText(m_AStev);
	m_CE_ASte2.GetWindowText(m_ASte2);
	m_CE_ASte3.GetWindowText(m_ASte3);
	m_CE_ASte4.GetWindowText(m_ASte4);
	m_CE_ADes3.GetWindowText(m_ADes3);
	m_CE_AOrg3.GetWindowText(m_AOrg3);
	m_CE_AOrg3L.GetWindowText(m_AOrg3L);
	m_CE_AEtai.GetWindowText(m_AEtai);
	m_CE_AStod.GetWindowText(m_AStod);
	m_CE_AStoa.GetWindowText(m_AStoa);
	m_CE_AEtai.GetWindowText(m_AEtai);
	m_CE_AEtoa.GetWindowText(m_AEtoa);
	m_CE_AOnbe.GetWindowText(m_AOnbe);
	m_CE_ANxti.GetWindowText(m_ANxti);
	m_CE_AOnbl.GetWindowText(m_AOnbl);
	m_CE_AOfbl.GetWindowText(m_AOfbl);
	m_CE_AAirb.GetWindowText(m_AAirb);
	m_CE_ALand.GetWindowText(m_ALand);
	m_CE_ATmoa.GetWindowText(m_ATmoa);
	m_CE_ADtd1.GetWindowText(m_ADtd1);
	m_CE_ADtd2.GetWindowText(m_ADtd2);
	m_CE_ADcd1.GetWindowText(m_ADcd1);
	m_CE_ADcd2.GetWindowText(m_ADcd2);
	m_CE_ARwya.GetWindowText(m_ARwya);
	m_CE_AIfra.GetWindowText(m_AIfra);
	m_CE_APsta.GetWindowText(m_APsta);
//	m_CE_APaba.GetWindowText(m_APaba);
	m_CE_AEtdi.GetWindowText(m_AEtdi);
//	m_CE_APaea.GetWindowText(m_APaea);
	m_CE_AGta1.GetWindowText(m_AGta1);
	m_CE_AGta2.GetWindowText(m_AGta2);
	m_CE_ATga1.GetWindowText(m_ATga1);
	m_CE_ATga2.GetWindowText(m_ATga2);
	m_CE_AGa1x.GetWindowText(m_AGa1x);
	m_CE_AGa2x.GetWindowText(m_AGa2x);
	m_CE_AGa1y.GetWindowText(m_AGa1y);
	m_CE_AGa2y.GetWindowText(m_AGa2y);
	m_CE_ABlt1.GetWindowText(m_ABlt1);
	m_CE_ABlt2.GetWindowText(m_ABlt2);
	m_CE_ATmb1.GetWindowText(m_ATmb1);
	m_CE_ATmb2.GetWindowText(m_ATmb2);
	m_CE_AB1ba.GetWindowText(m_AB1ba);
	m_CE_AB2ba.GetWindowText(m_AB2ba);
	m_CE_AB1ea.GetWindowText(m_AB1ea);
	m_CE_AB2ea.GetWindowText(m_AB2ea);


	m_CE_AExt1.GetWindowText(m_AExt1);
	m_CE_AExt2.GetWindowText(m_AExt2);
	m_CE_ATet1.GetWindowText(m_ATet1);
	m_CE_ATet2.GetWindowText(m_ATet2);
	m_CE_ARem1.GetWindowText(m_ARem1);
	m_CE_ARem3.GetWindowText(m_ARem3);
	m_CE_ABbaa.GetWindowText(m_ABbaa);
	m_CE_ADivr.GetWindowText(m_ADivr);

	//always IFR if not VFR ("V")
	if(m_AIfra != "V")
		m_AIfra = "";
	m_CE_AIfra.SetWindowText(m_AIfra);



	if(m_CE_ATtyp.IsChanged())
		strcpy(prmAFlight->Sttt,"U");


	strcpy(prmAFlight->Regn, m_Regn);
	strcpy(prmAFlight->Ming, m_Ming);
	strcpy(prmAFlight->Act3, m_Act3);
	strcpy(prmAFlight->Act5, m_Act5);


	CString olFlno = ogRotationDlgFlights.CreateFlno(m_AAlc3, m_AFltn, m_AFlns); 
	CString olFlnoT(olFlno);
	olFlnoT.TrimRight();
	if (strcmp(prmAFlight->Flno, olFlnoT) != 0 && strcmp(prmAFlight->Flno, olFlno) != 0)		
		strcpy(prmAFlight->Flno, olFlno);


	strcpy(prmAFlight->Alc3,omAAlc3);
	strcpy(prmAFlight->Alc2,omAAlc2);
	strcpy(prmAFlight->Fltn,m_AFltn);
	strcpy(prmAFlight->Flti,m_AFlti);
	strcpy(prmAFlight->Csgn,m_ACsgn);
	strcpy(prmAFlight->Flns,m_AFlns);
	strcpy(prmAFlight->Ttyp,m_ATtyp);
	strcpy(prmAFlight->Htyp,m_AHtyp);
	strcpy(prmAFlight->Styp,m_AStyp);
	strcpy(prmAFlight->Stev,m_AStev);
	strcpy(prmAFlight->Ste2,m_ASte2);
	strcpy(prmAFlight->Ste3,m_ASte3);
	strcpy(prmAFlight->Ste4,m_ASte4);

	CString sRet1 ;
	CString sRet2 ; 
	// check if field contains a valid airport
	ogBCD.GetField( "APT", "APC3", "APC4", m_ADivr, sRet1, sRet2 );

	if (strcmp(pcgHome, "PVG") == 0)
		strcpy(prmAFlight->Divr,sRet1);
	else
		strcpy(prmAFlight->Divr,m_ADivr);

	strcpy(prmAFlight->Des3,pcgHome);
	strcpy(prmAFlight->Org3,pcmAOrg3);
	strcpy(prmAFlight->Org4,pcmAOrg4);
	strcpy(prmAFlight->Des4,pcgHome4);


	prmAFlight->Stoa = HourStringToDate( m_AStoa, prmAFlight->Stoa);
//	prmAFlight->Stoa = HourStringToDate( m_AStoa, prmAFlightSave->Stoa);
	prmAFlight->Stod = HourStringToDate(m_AStod, prmAFlight->Stoa);
	prmAFlight->Etoa = HourStringToDate(m_AEtoa, prmAFlight->Stoa);

	// will be set by the fight process !!
	//prmAFlight->Onbe = HourStringToDate(m_AOnbe, prmAFlight->Stoa);
	prmAFlight->Nxti = HourStringToDate(m_ANxti, prmAFlight->Stoa);
	

	if(m_CE_ALand.IsChanged())	
	{
		if(!m_ALand.IsEmpty())
		{
			prmAFlight->Lndu = HourStringToDate(m_ALand, prmAFlight->Stoa);
			prmAFlightSave->Lndu = TIMENULL; // Because this field must be saved in the DB!
		}
		else
		{
			if( strcmp(prmAFlight->Stld, "D") == 0)
			{
				prmAFlight->Lndd = TIMENULL;	

			}else
			if( strcmp(prmAFlight->Stld, "A") == 0)
			{
				prmAFlight->Lnda = TIMENULL;	

			}else
			{
				prmAFlight->Lndu = TIMENULL;	

			}
		}
	}

	if(m_CE_AEtai.IsChanged())	
	{
		if(!m_AEtai.IsEmpty())
		{
			prmAFlight->Etau = HourStringToDate(m_AEtai, prmAFlight->Stoa);
			prmAFlightSave->Etau = TIMENULL; // Because this field must be saved in the DB!
		}
		else
		{
			if( strcmp(prmAFlight->Stea, "A") == 0)
			{
				prmAFlight->Etaa = TIMENULL;	

			}else
			if( strcmp(prmAFlight->Stea, "E") == 0)
			{
				prmAFlight->Etae = TIMENULL;	

			}else
			if( strcmp(prmAFlight->Stea, "C") == 0)
			{
				prmAFlight->Etac = TIMENULL;	

			}else
			{
				prmAFlight->Etau = TIMENULL;	

			}
		}
	}

	if(m_CE_AOnbl.IsChanged())	
	{
		if(!m_AOnbl.IsEmpty())
		{
			prmAFlight->Onbu = HourStringToDate(m_AOnbl, prmAFlight->Stoa);
			prmAFlightSave->Onbu = TIMENULL; // Because this field must be saved in the DB!
		}
		else
		{
			if( strcmp(prmAFlight->Ston, "D") == 0)
			{
				prmAFlight->Onbd = TIMENULL;	

			}else
			if( strcmp(prmAFlight->Ston, "S") == 0)
			{
				prmAFlight->Onbs = TIMENULL;	

			}else
			//if( strcmp(prmAFlight->Ston, "U") == 0)
			{
				prmAFlight->Onbu = TIMENULL;	

			}
		}
	}

	if(m_CE_AOfbl.IsChanged())	
	{
		if(!m_AOfbl.IsEmpty())
		{
			prmAFlight->Ofbu = HourStringToDate(m_AOfbl, prmAFlight->Stoa);
			prmAFlightSave->Ofbu = TIMENULL; // Because this field must be saved in the DB!
		}
		else
		{
			if( strcmp(prmAFlight->Stof, "D") == 0)
			{
				prmAFlight->Ofbd = TIMENULL;	

			}else
			if( strcmp(prmAFlight->Stof, "S") == 0)
			{
				prmAFlight->Ofbs = TIMENULL;	

			}else
			//if( strcmp(prmAFlight->Stof, "U") == 0)
			{
				prmAFlight->Ofbu = TIMENULL;	

			}
		}
	}



	if(m_CE_AAirb.IsChanged())	
	{
		if(!m_AAirb.IsEmpty())
		{
			prmAFlight->Airu = HourStringToDate(m_AAirb, prmAFlight->Stoa);
			prmAFlightSave->Airu = TIMENULL; // Because this field must be saved in the DB!
		}
		else
		{
			if( strcmp(prmAFlight->Stab, "D") == 0)
			{
				prmAFlight->Aird = TIMENULL;	

			}else
			if( strcmp(prmAFlight->Stab, "A") == 0)
			{
				prmAFlight->Aira = TIMENULL;	

			}else
			//if( strcmp(prmAFlight->Stab, "U") == 0)
			{
				prmAFlight->Airu = TIMENULL;	

			}
		}
	}


	if(m_CE_ATmoa.IsChanged())	
	{
		if(!m_ATmoa.IsEmpty())
		{
			prmAFlight->Tmau = HourStringToDate(m_ATmoa, prmAFlight->Stoa);
			prmAFlightSave->Tmau = TIMENULL; // Because this field must be saved in the DB!
		}
		else
		{
			if( strcmp(prmAFlight->Sttm, "A") == 0)
			{
				prmAFlight->Tmaa = TIMENULL;	

			}else
			//if( strcmp(prmAFlight->Sttm, "U") == 0)
			{
				prmAFlight->Tmau = TIMENULL;	

			}
		}
	}

	if(m_CE_AEtdi.IsChanged())	
	{
		if(!m_AEtdi.IsEmpty())
		{
			prmAFlight->Etdu = HourStringToDate(m_AEtdi, prmAFlight->Stoa);
			prmAFlightSave->Etdu = TIMENULL; // Because this field must be saved in the DB!
		}
		else
		{
			if( strcmp(prmAFlight->Sted, "A") == 0)
			{
				prmAFlight->Etda = TIMENULL;	

			}else
			if( strcmp(prmAFlight->Sted, "E") == 0)
			{
				prmAFlight->Etde = TIMENULL;	

			}else
			if( strcmp(prmAFlight->Sted, "C") == 0)
			{
				prmAFlight->Etdc = TIMENULL;	

			}else
			if( strcmp(prmAFlight->Sted, "U") == 0)
			{
				prmAFlight->Etdu = TIMENULL;	

			}
			if( strcmp(prmAFlight->Sted, "S") == 0)
			{
				prmAFlight->Etai = TIMENULL;	

			}
		}
	}

	if(m_ADtd1.GetLength() > 0)
		FillLeftByChar(m_ADtd1, '0' , 4);

	if(m_ADtd2.GetLength() > 0)
		FillLeftByChar(m_ADtd2, '0' , 4);


	strcpy(prmAFlight->Dtd1,m_ADtd1);
	strcpy(prmAFlight->Dtd2,m_ADtd2);

	strcpy(prmAFlight->Dcd1,m_ADcd1);
	strcpy(prmAFlight->Dcd2,m_ADcd2);
	strcpy(prmAFlight->Rwya,m_ARwya);
	strcpy(prmAFlight->Ifra,m_AIfra);
	strcpy(prmAFlight->Psta,m_APsta);
	strcpy(prmAFlight->Gta1,m_AGta1);
	strcpy(prmAFlight->Gta2,m_AGta2);
	strcpy(prmAFlight->Blt1,m_ABlt1);
	strcpy(prmAFlight->Blt2,m_ABlt2);
	strcpy(prmAFlight->Ext1,m_AExt1);
	strcpy(prmAFlight->Ext2,m_AExt2);
	strcpy(prmAFlight->Rem1,m_ARem1);
	strcpy(prmAFlight->Dela,m_ARem3);
	strcpy(prmAFlight->Ming,m_Ming);
	strcpy(prmAFlight->Act3,m_Act3);
	strcpy(prmAFlight->Act5,m_Act5);
	strcpy(prmAFlight->Psta, m_APsta);
	strcpy(prmAFlight->Gta1, m_AGta1);
	strcpy(prmAFlight->Gta2, m_AGta2);
	strcpy(prmAFlight->Tga1, m_ATga1);
	strcpy(prmAFlight->Tga2, m_ATga2);
	strcpy(prmAFlight->Blt1, m_ABlt1);
	strcpy(prmAFlight->Blt2, m_ABlt2);
	strcpy(prmAFlight->Tmb1, m_ATmb1);
	strcpy(prmAFlight->Tmb2, m_ATmb2);
	strcpy(prmAFlight->Ext1, m_AExt1);
	strcpy(prmAFlight->Ext2, m_AExt2);
	strcpy(prmAFlight->Tet1, m_ATet1);
	strcpy(prmAFlight->Tet2, m_ATet2);

	strcpy(prmAFlight->Rem1,m_ARem1);
	if(m_ARem1.IsEmpty())
		strcpy(prmAFlight->Isre,"");
	else
		strcpy(prmAFlight->Isre,"+");

//	GetDayOfWeek(prmAFlight->Stoa, prmAFlight->Dooa);

//	prmAFlight->Paba = HourStringToDate( m_APaba, prmAFlight->Stoa);
//	prmAFlight->Paea = HourStringToDate( m_APaea, prmAFlight->Stoa);
	prmAFlight->B1ba = HourStringToDate( m_AB1ba, prmAFlight->Stoa);
	prmAFlight->B2ba = HourStringToDate( m_AB2ba, prmAFlight->Stoa);
	prmAFlight->B1ea = HourStringToDate( m_AB1ea, prmAFlight->Stoa);
	prmAFlight->B2ea = HourStringToDate( m_AB2ea, prmAFlight->Stoa);
	prmAFlight->Ga1x = HourStringToDate( m_AGa1x, prmAFlight->Stoa);
	prmAFlight->Ga2x = HourStringToDate( m_AGa2x, prmAFlight->Stoa);
	prmAFlight->Ga1y = HourStringToDate( m_AGa1y, prmAFlight->Stoa);
	prmAFlight->Ga2y = HourStringToDate( m_AGa2y, prmAFlight->Stoa);
	prmAFlight->Bbaa = HourStringToDate( m_ABbaa, prmAFlight->Stoa);

	if (bgDailyFixedRes) 
	{
		if(pomCBFPSA->GetCheck() == TRUE)
			strcpy(prmAFlight->Fpsa, "X");
		else
			strcpy(prmAFlight->Fpsa, "");

		if(pomCBFBL1->GetCheck() == TRUE)
			strcpy(prmAFlight->Fbl1, "X");
		else
			strcpy(prmAFlight->Fbl1, "");

		if(pomCBFBL2->GetCheck() == TRUE)
			strcpy(prmAFlight->Fbl2, "X");
		else
			strcpy(prmAFlight->Fbl2, "");

		if(pomCBFGA1->GetCheck() == TRUE)
			strcpy(prmAFlight->Fga1, "X");
		else
			strcpy(prmAFlight->Fga1, "");

		if(pomCBFGA2->GetCheck() == TRUE)
			strcpy(prmAFlight->Fga2, "X");
		else
			strcpy(prmAFlight->Fga2, "");
	}
	else
	{
		if(m_AFpsa == TRUE)
			strcpy(prmAFlight->Fpsa, "X");
		else
			strcpy(prmAFlight->Fpsa, " ");
	}

	if(bgShowAOG)
	{
		if(pomCBAAOG->GetCheck() == TRUE)
		{
			strcpy(prmAFlight->Faog, "X");
		}
		else
		{
			strcpy(prmAFlight->Faog, "");
		}

	}

	if(bgShowADHOC)
	{
		if(pomCBAAHOC->GetCheck() == TRUE)
		{
			strcpy(prmAFlight->Adho, "X");
		}
		else
		{
			strcpy(prmAFlight->Adho, "");
		}
	}

	int ilCurSel;
	CString olTmp;


	if((ilCurSel = m_CL_ARemp.GetCurSel()) == CB_ERR)
		strcpy(prmAFlight->Remp, "");
	else
	{
		m_CL_ARemp.GetLBText(ilCurSel, olTmp);
		olTmp = olTmp.Left(4);
		olTmp.TrimRight();
		if(!olTmp.IsEmpty())
			strcpy(prmAFlight->Remp, olTmp);
		else
			strcpy(prmAFlight->Remp, "");
	}



	if (bmChanged)
		strcpy(prmAFlight->Ftyp, "O");
	
	if(m_ACxx == TRUE)
		strcpy(prmAFlight->Ftyp, "X");

	if(m_ADiverted == TRUE)
		strcpy(prmAFlight->Ftyp, "D");

	if(m_ARerouted == TRUE)
		strcpy(prmAFlight->Ftyp, "R");


	if(m_CB_ARetFlight.GetCheck())
		strcpy(prmAFlight->Ftyp, "Z");

	if(m_CB_ARetTaxi.GetCheck())
		strcpy(prmAFlight->Ftyp, "B");

	// AIA special
	if(m_GoAround == TRUE)
		strcpy(prmAFlight->Baz6, "G");
	else
		strcpy(prmAFlight->Baz6, "");
	// AIA special end

	
	if(m_AVip == TRUE)
		strcpy(prmAFlight->Vipa, "X");
	else
		strcpy(prmAFlight->Vipa, " ");

	


	if(bgCxxReason)
	{
		if((ilCurSel = m_CL_ACxxReason.GetCurSel()) == CB_ERR || strcmp(prmAFlight->Ftyp, "X") != 0)
		{
			strcpy(prmAFlight->Cxxr, "");
		}
		else
		{
			m_CL_ACxxReason.GetLBText(ilCurSel, olTmp);
			olTmp = olTmp.Left(5);
			olTmp.TrimRight();
			if(!olTmp.IsEmpty())
				strcpy(prmAFlight->Cxxr, olTmp);
			else
				strcpy(prmAFlight->Cxxr, "");
		}

	}
	CString olText;	

	if(m_CB_ANfes.GetCheck())
	{
		m_CE_ANfes.GetWindowText(olText);
		olTime = HourStringToDate( olText, prmAFlight->Stoa);
		prmAFlight->Nfes = olTime;
	}
	else
	{
		prmAFlight->Nfes = TIMENULL;
	}
	
	//Payment Comment
	if(bgShowCashComment)
	{

		if(pomBTCRCom->IsChanged())
		{
			CString tmpText;	
			((CCSEdit*)GetDlgItem(IDC_BTCRC_CASH_COMMENT))->GetWindowText(tmpText);
			strcpy(prmAFlight->Pcom, tmpText);
			strcpy(prmDFlight->Pcom, tmpText);
			
		}
	}
	
	//lgp
	if(pomBMTOW->IsChanged())//lgp
	{
		CString tmpText;	
		((CCSEdit*)GetDlgItem(IDC_BTCRC_MTOW))->GetWindowText(tmpText);
		strcpy(prmAFlight->Mtow, tmpText);
		strcpy(prmDFlight->Mtow, tmpText);

	}

	if(bgShowChock)
	{
		
		CString tmpText;
		pomCEChocksOn->GetWindowText(tmpText);

		if(pomCEChocksOn->IsChanged())
		{

			if(tmpText.GetLength() > 0)
			{
				prmAFlight->Chou = HourStringToDate( tmpText, prmAFlight->Stoa);
				prmAFlightSave->Chou = TIMENULL;
			}
			else
			{
				if( strcmp(prmAFlight->Stcn, "S") == 0 )
				{
					prmAFlight->Cho1 = TIMENULL;
				}
				else if (strcmp(prmAFlight->Stcn, "D") == 0)
				{
					prmAFlight->Cho2 = TIMENULL;
				}
				else
				{
					prmAFlight->Chou =TIMENULL;	
				}
			}
		}

		tmpText="";
		pomCEChocksOf->GetWindowText(tmpText);

		if(pomCEChocksOf->IsChanged())
		{
			if(tmpText.GetLength() > 0)
			{
				prmDFlight->Chfu = HourStringToDate( tmpText, prmDFlight->Stod);
				prmDFlightSave->Chfu = TIMENULL;
			}
			else
			{
				if( strcmp(prmDFlight->Stcf, "S") == 0 )
				{
					prmDFlight->Chf1 = TIMENULL;	
				}
				else if (strcmp(prmDFlight->Stcf, "D") == 0)
				{
					prmDFlight->Chf2 = TIMENULL;	

				}
				else
				{
					prmDFlight->Chfu =TIMENULL;	
				}
			}
		}
	}

	///////////////////////////////////////////////
	// weitere Flugnummern ankunft
	//
	CString olAlc3;
	CString olFltn;
	CString olFlns;

	strcpy(prmAFlight->Jfno, "");
	int ilJcnt = 0;
	CString olJfno;

	int ilLines = pomAJfnoTable->GetLinesCount();
	for(int i = 0; i < ilLines; i++)
	{
		pomAJfnoTable->GetTextFieldValue(i, 0, olAlc3);
		pomAJfnoTable->GetTextFieldValue(i, 1, olFltn);
		pomAJfnoTable->GetTextFieldValue(i, 2, olFlns);
		if(!olAlc3.IsEmpty() || !olFltn.IsEmpty())
		{
			olJfno +=   ogRotationDlgFlights.CreateFlno(olAlc3, olFltn, olFlns);
			ilJcnt++;
		}
	}
	olJfno.TrimRight();
	strcpy(prmAFlight->Jfno, olJfno);
	if (ilJcnt > 0)
		itoa(ilJcnt, prmAFlight->Jcnt, 10);


	///////////////////////////////////////////////
	// vias ankunft
	//
	if (polRotationAViaDlg)
		polRotationAViaDlg->GetViaList(prmAFlight->Vial);
/*
	CString olViaList = pomAViaCtrl->GetViaList();
	olViaList.TrimRight();
	strcpy(prmAFlight->Vial, olViaList);
*/
}








static void FlightTableCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName)
{
    RotationDlg *polDlg = (RotationDlg *)popInstance;
 
	if(ipDDXType == RG_DLG_ROTATION_CHANGE || ipDDXType == RG_DLG_FLIGHT_CHANGE || ipDDXType == R_DLG_FLIGHT_CHANGE)
	{
		polDlg->CheckTowingButton();
		polDlg->CheckREQButton();
		polDlg->CheckFlightReportButton();

	}

    if(ipDDXType == DIACHA_CHANGE || ipDDXType == DIACHA_DELETE)
		polDlg->ProcessChaChange((CHADATA *)vpDataPointer); 


    if(ipDDXType == R_DLG_FLIGHT_CHANGE)
        polDlg->ProcessFlightChange((ROTATIONDLGFLIGHTDATA *)vpDataPointer);

    if(ipDDXType == R_DLG_ROTATION_CHANGE)
//	    polDlg->ProcessRotationChange((ROTATIONDLGFLIGHTDATA *)vpDataPointer);
	    polDlg->ProcessRotationChange();

 	if (ipDDXType == CCA_ROTDLG_CHANGE || ipDDXType == CCA_ROTDLG_NEW || ipDDXType == CCA_ROTDLG_DELETE)
        polDlg->ProcessCCA((CCADATA *)vpDataPointer, ipDDXType);

	if (ipDDXType == R_DLG_RELOAD_CCA)
		polDlg->ReloadCCA();

	if (ipDDXType == FPE_INSERT || ipDDXType == FPE_UPDATE || ipDDXType == FPE_DELETE)
		polDlg->ProcessFpeChange();
}

static void ProcessDcfCf(void *popInstance, int ipDDXType,
						 void *vpDataPointer, CString &ropInstanceName)
{
    RotationDlg *polDlg = (RotationDlg *)popInstance;
	polDlg->ProcessDCF(vpDataPointer, ipDDXType);
}









///////////////////////////////////////////////////////////////////////////
// Cancel
//
void RotationDlg::OnCancel() 
{

	if(bmChanged)
	{
		if (!CheckPostFlight())
		{
			if(MessageBox(GetString(IDS_STRING942), GetString(ST_FRAGE), MB_ICONEXCLAMATION | MB_YESNO) == IDNO)
					return;
		}
	}

	bmChanged = false;
	m_CB_Ok.SetColors(::GetSysColor(COLOR_BTNFACE), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
	//PRF 8379 
	pomBTCRCFPSA->SetColors(::GetSysColor(COLOR_BTNFACE), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
	pomBTCRCAGTA1->SetColors(::GetSysColor(COLOR_BTNFACE), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
	pomBTCRCAGTA2->SetColors(::GetSysColor(COLOR_BTNFACE), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
	pomBTCRCFPSD->SetColors(::GetSysColor(COLOR_BTNFACE), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
	pomBTCRCDGTD1->SetColors(::GetSysColor(COLOR_BTNFACE), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
	pomBTCRCDGTD2->SetColors(::GetSysColor(COLOR_BTNFACE), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
	pomBTCRCABLT1->SetColors(::GetSysColor(COLOR_BTNFACE), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
	pomBTCRCABLT2->SetColors(::GetSysColor(COLOR_BTNFACE), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
	//PRF 8379

	ogRotationDlgFlights.ClearAll();
	ClearAll();
	ShowWindow(SW_HIDE);
}

///////////////////////////////////////////////////////////////////////////
void RotationDlg::ResetDatesForFlight() 
{
	if (bmArrival && prmAFlight && prmAFlight->Urno != 0)
	{
		CTime olRef = prmAFlightSave->Stoa;
		if (bmLocalTime)
			ogBasicData.UtcToLocal(olRef);

		prmAFlight->Stoa = olRef;
	}
	if (bmDeparture && prmDFlight && prmDFlight->Urno != 0)
	{
		CTime olRef = prmDFlightSave->Stod;
		if (bmLocalTime)
			ogBasicData.UtcToLocal(olRef);

		prmDFlight->Stod = olRef;
	}
}
//
void RotationDlg::OnOK() 
{
/*
	CString olAlc;
	CString olFltn;
	CString olFlns;
	CString olCsgn;

	if (prmAFlight && prmAFlight->Urno != 0)
	{
		m_CE_AAlc3.GetWindowText(olAlc);
		m_CE_AFltn.GetWindowText(olFltn);
		m_CE_AFlns.GetWindowText(olFlns);
		m_CE_ACsgn.GetWindowText(olCsgn);
		CString olFlno = CreateFlno(olAlc, olFltn, olFlns); 
		if (olFlno.IsEmpty() && olCsgn.IsEmpty())
		{
			//"Flightnumber or Callsign must be filled!\n"
			CFPMSApp::MyTopmostMessageBox(this, GetString(IDS_FLNO_OR_CSGN), GetString(ST_FEHLER), MB_ICONWARNING);
			return;
		}
	}

	if (prmDFlight && prmDFlight->Urno != 0)
	{
		m_CE_DAlc3.GetWindowText(olAlc);
		m_CE_DFltn.GetWindowText(olFltn);
		m_CE_DFlns.GetWindowText(olFlns);
		m_CE_DCsgn.GetWindowText(olCsgn);
		CString olFlno = CreateFlno(olAlc, olFltn, olFlns); 
		if (olFlno.IsEmpty() && olCsgn.IsEmpty())
		{
			//"Flightnumber or Callsign must be filled!\n"
			CFPMSApp::MyTopmostMessageBox(this, GetString(IDS_FLNO_OR_CSGN), GetString(ST_FEHLER), MB_ICONWARNING);
			return;
		}
	}
*/


	//check of duplicated single flights
	if (prmAFlight && prmAFlight->Urno != 0)
	{
		if(	m_CE_AStoa.IsChanged() || m_CE_AAlc3.IsChanged() || m_CE_AFltn.IsChanged() || m_CE_AFlns.IsChanged())
		{
			CString olAlc;
			CString olFltn;
			CString olFlns;
			CString olStoa;
			m_CE_AAlc3.GetWindowText(olAlc);
			m_CE_AFltn.GetWindowText(olFltn);
			m_CE_AFlns.GetWindowText(olFlns);
			m_CE_AStoa.GetWindowText(olStoa);

			CTime olTimeStoa = HourStringToDate( olStoa, prmAFlight->Stoa);

			CString olText;
			olText =  IsDupFlight(prmAFlight->Urno, "A", olAlc, olFltn, olFlns, olTimeStoa, bmLocalTime);
			if (!olText.IsEmpty())
			{
				if(CFPMSApp::MyTopmostMessageBox(this,olText, GetString(IDS_WARNING), MB_YESNO) != IDYES)
				{
					ResetDatesForFlight();
					return;
				}
			}
		}
	}

	if (prmDFlight->Urno != 0)
	{
		if(	m_CE_DStod.IsChanged() || m_CE_DAlc3.IsChanged() || m_CE_DFltn.IsChanged() || m_CE_DFlns.IsChanged())
		{
			CString olAlc;
			CString olFltn;
			CString olFlns;
			CString olStod;
			m_CE_DAlc3.GetWindowText(olAlc);
			m_CE_DFltn.GetWindowText(olFltn);
			m_CE_DFlns.GetWindowText(olFlns);
			m_CE_DStod.GetWindowText(olStod);

			CTime olTimeStod = HourStringToDate( olStod, prmDFlight->Stod);

			CString olText;
			olText =  IsDupFlight(prmDFlight->Urno, "D", olAlc, olFltn, olFlns, olTimeStod, bmLocalTime);
			if (!olText.IsEmpty())
			{
				if(CFPMSApp::MyTopmostMessageBox(this,olText, GetString(IDS_WARNING), MB_YESNO) != IDYES)
				{
					ResetDatesForFlight();
					return;
				}
			}
		}
	}


	CString olGMessage;
	CString olAMessage;
	CString olDMessage;

	//error - PRF 8379 
	if(bgReasonFlag)
	{
		CString olReasonSelect ="";

	/*	if(bgDailyFixedRes)
		{
			if(omReasons.blPstaResf && (strcmp(prmAFlight->Fpsa, "X") == 0))
				olReasonSelect += GetString(IDS_STRING877) + ",\n";
			if(omReasons.blAGta1Resf && (strcmp(prmAFlight->Fga1, "X") == 0))
				olReasonSelect += GetString(IDS_STRING878) + ",\n";
			if(omReasons.blAGta2Resf && (strcmp(prmAFlight->Fga2, "X") == 0))
				olReasonSelect += GetString(IDS_STRING879) + ",\n";
			if(omReasons.blABlt1Resf && (strcmp(prmAFlight->Fbl1, "X") == 0) )
				olReasonSelect += GetString(IDS_STRING1278) + ",\n";
			if(omReasons.blABlt2Resf && (strcmp(prmAFlight->Fbl2, "X") == 0))
				olReasonSelect += GetString(IDS_STRING1279) + ",\n";

			if(omReasons.blPstdResf && (strcmp(prmDFlight->Fpsd, "X") == 0))
				olReasonSelect += GetString(IDS_STRING891) + ",\n";
			if(omReasons.blDGtd1Resf && (strcmp(prmDFlight->Fgd1, "X") == 0) )
				olReasonSelect += GetString(IDS_STRING892) + ",\n";
			if(omReasons.blDGtd2Resf && (strcmp(prmDFlight->Fgd2, "X") == 0) )
				olReasonSelect += GetString(IDS_STRING893) + ",\n";
		}
		else
		{*/
				if(omReasons.blPstaResf)
					olReasonSelect += GetString(IDS_STRING877) + ",\n";
				if(omReasons.blAGta1Resf)
					olReasonSelect += GetString(IDS_STRING878) + ",\n";
				if(omReasons.blAGta2Resf)
					olReasonSelect += GetString(IDS_STRING879) + ",\n";
				if(omReasons.blABlt1Resf)
					olReasonSelect += GetString(IDS_STRING1278) + ",\n";
				if(omReasons.blABlt2Resf)
					olReasonSelect += GetString(IDS_STRING1279) + ",\n";

				if(omReasons.blPstdResf)
					olReasonSelect += GetString(IDS_STRING891) + ",\n";
				if(omReasons.blDGtd1Resf)
					olReasonSelect += GetString(IDS_STRING892) + ",\n";
				if(omReasons.blDGtd2Resf)
					olReasonSelect += GetString(IDS_STRING893) + ",\n";
		/*}*/

		if(olReasonSelect.GetLength() > 0) 
		{
			olReasonSelect.TrimRight(",\n");
			olReasonSelect += " Reasons not selected";
			MessageBox(olReasonSelect, GetString(IDS_WARNING), MB_ICONWARNING|MB_OK);
			return;
		}

	}

	ogBCD.SetObject("AFT","URNO,DQCS"); 

	CString olWhere = ""; //lgp
	CString olUrno;
	if(prmDFlight->Urno)
	{
		long llUrnoD = prmDFlight->Urno;

		olUrno.Format("%ld", llUrnoD);
		olWhere.Format("WHERE URNO = '%s'", olUrno);
		ogBCD.Read(CString("AFT"), olWhere);		
		ogBCD.GetField("AFT", "URNO", olUrno, "DQCS", ogDQCSD);
		ogDQCSD = ogBCD.GetField("AFT", "URNO", olUrno, "DQCS");
	}

	char *dqcsTmp;
	if(bgValuDQCS && ((strcmp(ogDQCSD, "C")==0) || (strcmp(ogDQCSD, "S")==0) || ((dqcsTmp = strstr(ogDQCSD, "MC"))!=NULL) ))
	{
		m_CB_Ok.EnableWindow(FALSE);
		AfxMessageBox("ALL FLIGHT DATA(Arrival&Depart&Common) is freezed and won't be updated!");
	}
	else
	{	
		FillFlightData();
	}

	if(!CheckAll(olGMessage, olAMessage, olDMessage))
	{
		if(olGMessage.IsEmpty() && olAMessage.IsEmpty() && olDMessage.IsEmpty())
		{
			ResetDatesForFlight();
			return;
		}

		if(!olGMessage.IsEmpty())
			olGMessage = GetString(IDS_STRING972) + olGMessage; 

		if(!olAMessage.IsEmpty())
			olAMessage = GetString(IDS_STRING973) + olAMessage; 
		
		if(!olDMessage.IsEmpty())
			olDMessage = GetString(IDS_STRING974) + olDMessage; 
		
//		MessageBox(olGMessage + olAMessage + olDMessage, GetString(ST_FEHLER), MB_ICONWARNING);
		CFPMSApp::MyTopmostMessageBox(this, olGMessage + olAMessage + olDMessage, GetString(ST_FEHLER), MB_ICONWARNING);
		ResetDatesForFlight();
		return;
	}


	olGMessage.Empty();
	olAMessage.Empty();
	olDMessage.Empty();
	// Second check
	if(!CheckAll2(olGMessage, olAMessage, olDMessage))
	{
		if(olGMessage.IsEmpty() && olAMessage.IsEmpty() && olDMessage.IsEmpty())
		{
			ResetDatesForFlight();
			return;
		}

		if(!olGMessage.IsEmpty())
			olGMessage = GetString(IDS_STRING972) + olGMessage; 

		if(!olAMessage.IsEmpty())
			olAMessage = GetString(IDS_STRING973) + olAMessage; 
		
		if(!olDMessage.IsEmpty())
			olDMessage = GetString(IDS_STRING974) + olDMessage; 

		CString olWarningText;
		olWarningText.Format("%s%s%s\n%s", olGMessage, olAMessage, olDMessage, GetString(IDS_STRING2037));
		if (CFPMSApp::MyTopmostMessageBox(this, olWarningText, GetString(IDS_WARNING), MB_ICONWARNING | MB_YESNO) != IDYES)
		{
			ResetDatesForFlight();
			return;
		}
	}


	// additional belt check
	if (!AddBltCheck())
	{
		ResetDatesForFlight();
		return;
	}


	// Arrival Flight ID changed?
	if (strcmp(prmAFlight->Flti, prmAFlightSave->Flti) != 0)
	{
		m_AFlti.TrimRight();
		CString olMess;
 		if (m_AFlti.IsEmpty())
		{
			olMess.Format(GetString(IDS_STRING1994), prmAFlight->Flno);
		}
		else
		{
			olMess.Format(GetString(IDS_STRING1993), prmAFlight->Flno);
		}
		if (MessageBox(olMess, GetString(ST_FRAGE), MB_ICONQUESTION | MB_YESNO) != IDYES)
		{
			ResetDatesForFlight();
			return;
		}
	}
	
	if(bgNatureAutoCalc)
	{
		if (strcmp(prmAFlight->Ttyp, prmAFlightSave->Ttyp) != 0)
		{
			m_ATtyp.TrimRight();
			CString olMess;
 			if (m_ATtyp.IsEmpty())
			{
				olMess.Format(GetString(IDS_NATURE_AUTOMAT), prmAFlight->Flno);
			}
			else
			{
				olMess.Format(GetString(IDS_NATURE_MANUAL), prmAFlight->Flno);
			}
			if (CFPMSApp::MyTopmostMessageBox(this,olMess, GetString(ST_FRAGE), MB_ICONQUESTION | MB_YESNO) != IDYES)
			{
				ResetDatesForFlight();
				return;
			}
		}
	}

	// Departure Flight ID changed?
	if (strcmp(prmDFlight->Flti, prmDFlightSave->Flti) != 0)
	{
		m_DFlti.TrimRight();
		CString olMess;
 		if (m_DFlti.IsEmpty())
		{
			olMess.Format(GetString(IDS_STRING1994), prmDFlight->Flno);
		}
		else
		{
			olMess.Format(GetString(IDS_STRING1993), prmDFlight->Flno);
		}
		if (MessageBox(olMess, GetString(ST_FRAGE), MB_ICONQUESTION | MB_YESNO) != IDYES)
		{
			ResetDatesForFlight();
			return;
		}
	}

	//Inserting reason records into the CRATAB - PRF 8379 
	if(bgReasonFlag)
	{
		if(bmArrival)
		{
			if((omReasons.olPstaUrno.GetLength() > 0 ) || (omReasons.olAGta1Urno.GetLength() > 0 ) ||
				(omReasons.olAGta2Urno.GetLength() > 0 ) || (strcmp(prmAFlight->Psta,m_APsta) != 0) ||
				(omReasons.olABlt1Urno.GetLength() > 0 ) || (omReasons.olABlt2Urno.GetLength() > 0 ) ||
				(strcmp(prmAFlight->Gta1,m_AGta1) != 0) || (strcmp(prmAFlight->Gta2,m_AGta2) != 0) ||
				(strcmp(prmAFlight->Blt1,m_ABlt1) != 0) || (strcmp(prmAFlight->Blt2,m_ABlt2) != 0))
			{
				CRADATA *prlCRADATA = new CRADATA();
				
				if(bgGatPosLocal)			
					prlCRADATA->Cdat = CTime::GetCurrentTime();
				else
					GetCurrentUtcTime(prlCRADATA->Cdat);
				
				prlCRADATA->Furn = prmAFlight->Urno;
				strcpy(prlCRADATA->Hopo, pcgHome);
				strcpy(prlCRADATA->Usec, ogBasicData.omUserID);
				prlCRADATA->IsChanged = true;

				if((omReasons.olPstaUrno.GetLength() > 0 ) && (CString(prmAFlightSave->Psta) != m_APsta))
				{
					prlCRADATA->Urno = ogBasicData.GetNextUrno();
					prlCRADATA->Curn = atof(omReasons.olPstaUrno);
					strcpy(prlCRADATA->Oval,omPsta);
					strcpy(prlCRADATA->Nval,m_APsta);
					strcpy(prlCRADATA->Aloc,"PSTA");
					ogCraData.InsertDB(prlCRADATA, false);
				}
				
				if((omReasons.olAGta1Urno.GetLength() > 0 )&& (CString(prmAFlightSave->Gta1) != m_AGta1))
				{
					prlCRADATA->Urno = ogBasicData.GetNextUrno();
					prlCRADATA->Curn = atof(omReasons.olAGta1Urno);
					strcpy(prlCRADATA->Oval,omGta1);
					strcpy(prlCRADATA->Nval,m_AGta1);
					strcpy(prlCRADATA->Aloc,"GTA1");
					ogCraData.InsertDB(prlCRADATA, false);
				}

				if((omReasons.olAGta2Urno.GetLength() > 0 ) && (CString(prmAFlightSave->Gta2) != m_AGta2))
				{
					prlCRADATA->Urno = ogBasicData.GetNextUrno();
					prlCRADATA->Curn = atof(omReasons.olAGta2Urno);
					strcpy(prlCRADATA->Oval,omGta2);
					strcpy(prlCRADATA->Nval,m_AGta2);
					strcpy(prlCRADATA->Aloc,"GTA2");
					ogCraData.InsertDB(prlCRADATA, false);
				}

				if((omReasons.olABlt1Urno.GetLength() > 0 ) && (CString(prmAFlightSave->Blt1 ) != m_ABlt1))
				{
					prlCRADATA->Urno = ogBasicData.GetNextUrno();
					prlCRADATA->Curn = atof(omReasons.olABlt1Urno);
					strcpy(prlCRADATA->Oval,omBlt1);
					strcpy(prlCRADATA->Nval,m_ABlt1);
					strcpy(prlCRADATA->Aloc,"BLT1");
					ogCraData.InsertDB(prlCRADATA, false);
				}

				if((omReasons.olABlt2Urno.GetLength() > 0 ) && (CString(prmAFlightSave->Blt2 ) != m_ABlt1))
				{
					prlCRADATA->Urno = ogBasicData.GetNextUrno();
					prlCRADATA->Curn = atof(omReasons.olABlt1Urno);
					strcpy(prlCRADATA->Oval,omBlt2);
					strcpy(prlCRADATA->Nval,m_ABlt2);
					strcpy(prlCRADATA->Aloc,"BLT2");
					ogCraData.InsertDB(prlCRADATA, false);
				}
				delete prlCRADATA;
			}
		}

		if(bmDeparture)
		{
			if((omReasons.olPstdUrno.GetLength() > 0 )   || (omReasons.olDGtd1Urno.GetLength() > 0 ) ||
				(omReasons.olDGtd2Urno.GetLength() > 0 ) || (strcmp(prmDFlight->Pstd,m_DPstd) != 0)  ||
				(strcmp(prmDFlight->Gtd1,m_DGtd1) != 0)  || (strcmp(prmDFlight->Gtd2,m_DGtd2) != 0))
			{
				CRADATA *prlCRADATA = new CRADATA();

				if(bgGatPosLocal)			
					prlCRADATA->Cdat = CTime::GetCurrentTime();
				else
					GetCurrentUtcTime(prlCRADATA->Cdat);
				
				prlCRADATA->Furn = prmDFlight->Urno;
				strcpy(prlCRADATA->Hopo, pcgHome);
				strcpy(prlCRADATA->Usec, ogBasicData.omUserID);
				prlCRADATA->IsChanged = true;

				if((omReasons.olPstdUrno.GetLength() > 0 )  && (CString(prmDFlightSave->Pstd) != m_DPstd))
				{
					prlCRADATA->Urno = ogBasicData.GetNextUrno();
					prlCRADATA->Curn = atof(omReasons.olPstdUrno);
					strcpy(prlCRADATA->Oval,omPstd);
					strcpy(prlCRADATA->Nval,m_DPstd);
					strcpy(prlCRADATA->Aloc,"PSTD");
					ogCraData.InsertDB(prlCRADATA, false);
				}
				
				if((omReasons.olDGtd1Urno.GetLength() > 0 )  && (CString(prmDFlightSave->Gtd1) != m_DGtd1))
				{
					prlCRADATA->Urno = ogBasicData.GetNextUrno();
					prlCRADATA->Curn = atof(omReasons.olDGtd1Urno);
					strcpy(prlCRADATA->Oval,omGtd1);
					strcpy(prlCRADATA->Nval,m_DGtd1);
					strcpy(prlCRADATA->Aloc,"GTD1");
					ogCraData.InsertDB(prlCRADATA, false);
				}

				if((omReasons.olDGtd2Urno.GetLength() > 0 )  && (CString(prmDFlightSave->Gtd2) != m_DGtd2))
				{
					prlCRADATA->Urno = ogBasicData.GetNextUrno();
					prlCRADATA->Curn = atof(omReasons.olDGtd2Urno);
					strcpy(prlCRADATA->Oval,omGtd2);
					strcpy(prlCRADATA->Nval,m_DGtd2);
					strcpy(prlCRADATA->Aloc,"GTD2");
					ogCraData.InsertDB(prlCRADATA, false);
				}
				delete prlCRADATA;
			}
		}
	}



	if(bgNatureAutoCalc)
	{
		if (strcmp(prmDFlight->Ttyp, prmDFlightSave->Ttyp) != 0)
		{
			m_DTtyp.TrimRight();
			CString olMess;
 			if (m_DTtyp.IsEmpty())
			{
				olMess.Format(GetString(IDS_NATURE_AUTOMAT), prmDFlight->Flno);
			}
			else
			{
				olMess.Format(GetString(IDS_NATURE_MANUAL), prmDFlight->Flno);
			}
			if (CFPMSApp::MyTopmostMessageBox(this,olMess, GetString(ST_FRAGE), MB_ICONQUESTION | MB_YESNO) != IDYES)
			{
				ResetDatesForFlight();
				return;
			}
		}
	}
	
	m_CB_Ok.SetColors(RGB(0,255, 0), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
	m_CB_Ok.UpdateWindow();

	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));

	Register();
	
	if(bmArrival)
	{
		if(	prmAFlight->Urno != 0)
		{
//			if(bmLocalTime) ogRotationDlgFlights.StructLocalToUtc((void*)prmAFlight);
	 		if(bmLocalTime) ConvertStructLocalTOUtc(prmAFlight, NULL);

			if(!ogRotationDlgFlights.UpdateFlight(prmAFlight, prmAFlightSave))
			{
			}

//			if(bmLocalTime) ogRotationDlgFlights.StructUtcToLocal((void*)prmAFlight);
	 		if(bmLocalTime) ConvertStructUtcTOLocal(prmAFlight, NULL);

		}
	}
	if(bmDeparture)
	{
		if(	(prmDFlight->Urno != 0))
		{
// 			if(bmLocalTime) ogRotationDlgFlights.StructLocalToUtc((void*)prmDFlight);
	 		if(bmLocalTime) ConvertStructLocalTOUtc(NULL, prmDFlight);

			omCcaData.UpdateSpecial(omDCins, omDCinsSave, prmDFlight->Urno);
			bmChanged = false;
			ogRotationDlgFlights.UpdateFlight(prmDFlight, prmDFlightSave);

//			if(bmLocalTime) ogRotationDlgFlights.StructUtcToLocal((void*)prmDFlight);
	 		if(bmLocalTime) ConvertStructUtcTOLocal(NULL, prmDFlight);

			if(bgFIDSLogo )
				SaveLogo();

			GetCcas();


		}
	}


	bmChanged = false;
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	m_CB_Ok.SetColors(::GetSysColor(COLOR_BTNFACE), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));


//##### PRF 2055 NUR POPS44
	// if both legs cxx or noop -> don�t split the rotation -> join again !
	if(	prmAFlight && prmDFlight && prmAFlight->Urno != 0 && prmDFlight->Urno != 0 && CString(pcgHome) != "WAW")
	{
		if(		(strcmp(prmDFlight->Ftyp, "X") == 0 || strcmp(prmDFlight->Ftyp, "N") == 0)
			&&  (strcmp(prmAFlight->Ftyp, "X") == 0 || strcmp(prmAFlight->Ftyp, "N") == 0) )
		{
			ROTATIONDLGFLIGHTDATA* prlAFlight = NULL;
			ROTATIONDLGFLIGHTDATA* prlDFlight = NULL;

			char pclSelectionArr[256];
			sprintf(pclSelectionArr, " URNO = %ld", prmAFlight->Urno);

			ogRotationDlgFlights.ReadFlights(pclSelectionArr);
			prlAFlight = ogRotationDlgFlights.GetFlightByUrno(prmAFlight->Urno);

			char pclSelectionDep[256];
			sprintf(pclSelectionDep, " URNO = %ld", prmDFlight->Urno);

			ogRotationDlgFlights.ReadFlights(pclSelectionDep);
			prlDFlight = ogRotationDlgFlights.GetFlightByUrno(prmDFlight->Urno);

			if (prlAFlight && prlDFlight)
			{
				CedaFlightUtilsData olFlightUtilsData;
				olFlightUtilsData.JoinFlight(prlAFlight->Urno, prlAFlight->Rkey, prlDFlight->Urno, prlDFlight->Rkey, true);
			}
		}
	}
//##### PRF 2055 NUR POPS44


	//Towing popup 
	if(bgTowingPopup)
	{
		
		if(pogRotGroundDlg == NULL)
		{
		  pogRotGroundDlg = new RotGroundDlg(this);
		  pogRotGroundDlg->Create(IDD_ROTGROUND);

		}
		if(CheckPostFlight())
			pogRotGroundDlg->setStatus(RotGroundDlg::Status::POSTFLIGHT);
		else
			pogRotGroundDlg->setStatus(RotGroundDlg::Status::UNKNOWN);
		if(prmAFlight->Urno > 0)
		{
			pogRotGroundDlg->NewData(this, prmAFlight->Rkey, prmAFlight->Urno, 'A', bmLocalTime);
		}
		else
		{
			pogRotGroundDlg->NewData(this, prmDFlight->Rkey, prmDFlight->Urno, 'D', bmLocalTime);
		}

		int licount = pogRotGroundDlg->omMovements.GetSize();
		//int ilCount ;
		bool lbTowDlgPopup;
		lbTowDlgPopup = true;
		for(int i =0;i<licount;i++)
		{
			ROTGDLGFLIGHTDATA *prlMovement = NULL;
			prlMovement = &pogRotGroundDlg->omMovements[i];
			if (prlMovement->Stod != TIMENULL)
			{
				if(prlMovement->Psta == m_DPstd)
					lbTowDlgPopup = false;
				else
					lbTowDlgPopup = true;

			}
		}

		if(	prmAFlight && prmDFlight && prmAFlight->Urno != 0 && prmDFlight->Urno != 0 )
		{
			if(m_APsta != m_DPstd && lbTowDlgPopup && !(m_APsta.IsEmpty() || m_DPstd.IsEmpty()) )
		{
			
			AskBox Dlg(this,"Question","Arrival and Departure  Position are different.Do you want to update the Towing?","Yes","No");
			int iRet = Dlg.DoModal();
			if(iRet == 1)
			{
				
				pogRotGroundDlg->SetWindowPos(&wndTop,0,0,0,0,SWP_SHOWWINDOW | SWP_NOSIZE | SWP_NOMOVE );

			}
			
		}
		}		
	}
	/*
	if(bmLocked)
	{
		NewData(pomParent, lmRkey, lmCalledBy, omAdid, bmLocalTime, bmRotationReadOnly);
	}
	*/
	PrepareAndSavePermit();//UFIS-3720
	bmLocked = false;
	m_CB_Lock.SetColors(::GetSysColor(COLOR_BTNFACE), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
	m_CB_Lock.SetWindowText(GetString(IDS_STRING2875));

	if(bgDailyFixedRes)
	{
		omReasons.blAGta1Resf = false;
		omReasons.blAGta2Resf = false;
		omReasons.blDGtd1Resf = false;
		omReasons.blDGtd1Resf = false;
		omReasons.blPstaResf  = false;
		omReasons.blPstdResf  = false;
		omReasons.blABlt1Resf = false; 
		omReasons.blABlt2Resf = false;
	}

}




void RotationDlg::OnSplit() 
{
	if((prmAFlight->Urno != 0) && (prmDFlight->Urno != 0))
	{

		CedaFlightUtilsData omFlightUtilsData;
		omFlightUtilsData.SplitFlight(prmAFlight->Urno, prmAFlight->Rkey, false);
	}
}




void RotationDlg::OnJoin() 
{
	if((prmAFlight->Urno != 0) && (prmDFlight->Urno != 0))
		return;

	if((prmAFlight->Urno == 0) && (prmDFlight->Urno == 0))
		return;

	RotationJoinDlg *pomDlg;
	
	if(prmDFlight->Urno != 0)
		pomDlg = new RotationJoinDlg(this, prmDFlight, 'D', bmLocalTime);
	else
		pomDlg = new RotationJoinDlg(this, prmAFlight, 'A', bmLocalTime);

	CFPMSApp::UnsetTopmostWnds();
	pomDlg->DoModal();
	CFPMSApp::SetTopmostWnds();
	
	delete pomDlg;
}


////////////////////////////////////////////////////////////////////////////////////////////////
// users edits in dialod window "Daily Rotation Data" mask IDD_ROTATION are handled here for each (sub)window
//  dlg field   variable  (sub)window  description
//  ----------- --------- ------------ ----------------------------------
//	IDC_AORG3L  m_AOrg3L  m_CE_AOrg3L  Origin IATA airport code
//	IDC_AORG3   m_AOrg3   m_CE_AOrg3   Origin ICAO airport code
//	IDC_ADIVR   m_ADivr   m_CE_ADivr   Diverted airport code
//  IDC_APSTA   m_APsta   m_CE_APsta   Parking Bay
//
LONG RotationDlg::OnEditKillfocus( UINT wParam, LPARAM lParam)
{		
	CCSEDITNOTIFY *prlNotify = (CCSEDITNOTIFY*)lParam;
	CString olTmp;

	CStringArray olReqFields;
	CStringArray olForecastDataArr;
	CStringArray olForecastDataDep;

	olForecastDataArr.Add("A");
	olForecastDataArr.Add(CString(omAAlc2));
	olForecastDataArr.Add(CString(omAAlc3));

	olForecastDataDep.Add("D");
	olForecastDataDep.Add(CString(omDAlc2));
	olForecastDataDep.Add(CString(omDAlc3));

	CString olVal("");
	m_CE_Act3.GetWindowText(olVal);
	olForecastDataArr.Add(CString(olVal));
	olForecastDataDep.Add(CString(olVal));

	m_CE_Act5.GetWindowText(olVal);
	olForecastDataArr.Add(CString(olVal));
	olForecastDataDep.Add(CString(olVal));

	m_CE_ATtyp.GetWindowText(olVal);
	olForecastDataArr.Add(CString(olVal));
	m_CE_DTtyp.GetWindowText(olVal);
	olForecastDataDep.Add(CString(olVal));

	CMapStringToString olForcastMapArr;
	CMapStringToString olForcastMapDep;
	CreateForcastMap (olForcastMapArr, olForecastDataArr);
	CreateForcastMap (olForcastMapDep, olForecastDataDep);


	if(((UINT)m_CE_DAlc3.imID == wParam) && (!prlNotify->Text.IsEmpty()))
	{
		omDAlc2 = "";
		omDAlc3 = "";
		CString olStod; 

		if(prmDFlight != NULL)
			olStod = CTimeToDBString( prmDFlight->Stod, prmDFlight->Stod);

		
		prlNotify->UserStatus = true;
		if(!(prlNotify->Status = ogBCD.GetField("ALT", "ALC2", "ALC3", prlNotify->Text, omDAlc2, omDAlc3, olStod )))
		{
			omDAlc2 = "";
			omDAlc3 = "";
			RotationAltDlg olDlg(this, prlNotify->Text);
			if(olDlg.DoModal() == IDOK)
			{
				prlNotify->Status = true;
				omDAlc2 = olDlg.m_Alc3;
				omDAlc3 = olDlg.m_Alc2;
			}
			m_CE_DAlc3.SetInitText(omDAlc2, true);
		}
		if(omDAlc2.IsEmpty() && omDAlc3.IsEmpty())
		{
			m_CE_DDes3.SetTextLimit(0,3,true);
			m_CE_DDes3.SetBKColor(WHITE);
			m_CE_DDes4.SetTextLimit(0,4,true);
			m_CE_DDes4.SetBKColor(WHITE);
		}
		else
		{
			m_CE_DDes3.SetTextLimit(3,3,false);
//			m_CE_DDes3.SetBKColor(YELLOW);
			m_CE_DDes4.SetTextLimit(4,4,false);
			m_CE_DDes4.SetBKColor(YELLOW);
		}
		return 0L;
	}

	if(((UINT)m_CE_AAlc3.imID == wParam) && (!prlNotify->Text.IsEmpty()))
	{
		omAAlc2 = "";
		omAAlc3 = "";
		prlNotify->UserStatus = true;

		CString olStoa; 

		if(prmAFlight != NULL)
			olStoa = CTimeToDBString( prmAFlight->Stoa, prmAFlight->Stoa);


		if(!(prlNotify->Status = ogBCD.GetField("ALT", "ALC2", "ALC3", prlNotify->Text, omAAlc2, omAAlc3, olStoa )))
		{
			omAAlc2 = "";
			omAAlc3 = "";
			RotationAltDlg olDlg(this, prlNotify->Text);
			if(olDlg.DoModal() == IDOK)
			{
				prlNotify->Status = true;
				omAAlc2 = olDlg.m_Alc3;
				omAAlc3 = olDlg.m_Alc2;
			}
			m_CE_AAlc3.SetInitText(omAAlc2, true);
		}
		if(omAAlc2.IsEmpty() && omAAlc3.IsEmpty())
		{
			m_CE_ADes3.SetTextLimit(0,4,true);
			m_CE_ADes3.SetBKColor(WHITE);
		}
		else
		{
			m_CE_ADes3.SetTextLimit(4,4,false);
			m_CE_ADes3.SetBKColor(YELLOW);
		}
		return 0L;
	}

	// 050303 MVy: ccs window id for diverted text and text is not empty
	if( m_CE_ADivr.imID == wParam && !prlNotify->Text.IsEmpty() )
	{
		CString sRet1 ;
		CString sRet2 ; 
		// check if field contains a valid airport
		prlNotify->Status = ogBCD.GetField( "APT", "APC3", "APC4", prlNotify->Text, sRet1, sRet2 );
		prlNotify->UserStatus = !prlNotify->Status ;		// set field red, if data not found in database
		m_CE_ADivr.SetStatus( prlNotify->Status );		// don�t know if necessary
	};

//	if(((UINT)m_CE_AOrg3.imID == wParam) && (!prlNotify->Text.IsEmpty()))
//	if(((UINT)m_CE_AOrg3.imID == wParam))
	if((((UINT)m_CE_AOrg3.imID == wParam) || ((UINT)m_CE_AOrg3L.imID == wParam)) && (!prlNotify->Text.IsEmpty()))
	{
		prlNotify->Text.Remove(' ');
		if (prlNotify->Text.IsEmpty())
		{
			prlNotify->Status = false;
			return 0L;
		}
		strcpy(pcmAOrg3, "");
		strcpy(pcmAOrg4, "");
		CString olTmp3;
		CString olTmp4;
		
		if(prlNotify->Status = ogBCD.GetField("APT", "APC3", "APC4", prlNotify->Text, olTmp3, olTmp4 ))
		{
			strcpy(pcmAOrg3, olTmp3);
			strcpy(pcmAOrg4, olTmp4);
		}
		else
		{
			RotationAptDlg olDlg(this, prlNotify->Text);
			if(olDlg.DoModal() == IDOK)
			{
				prlNotify->Status = true;
				strcpy(pcmAOrg3, olDlg.m_Apc3);
				strcpy(pcmAOrg4, olDlg.m_Apc4);
			}
		}
		CString olOld = "";
		m_CE_AOrg3.GetWindowText(olOld);
		if (olTmp4 != olOld)
			m_CE_AOrg3.SetInitText(pcmAOrg4, true);

		olOld = "";
		m_CE_AOrg3L.GetWindowText(olOld);
		if (olTmp3 != olOld)
			m_CE_AOrg3L.SetInitText(pcmAOrg3, true);

		if(strcmp(pcmAOrg3 ,pcgHome) == 0)
		{
			m_CE_AStod.SetBKColor(YELLOW);
			m_CE_AStod.SetTypeToTime(true, true);
			m_CE_AStod.SetTextLimit(4,7,false);
			m_CS_Ankunft.SetWindowText(GetString(IDS_STRING340) + CString(" ")+ prmAFlight->Stoa.Format("%d.%m.%Y"));
		}
		else
		{
			m_CE_AStod.SetBKColor(WHITE);
			m_CE_AStod.SetTypeToTime(false, true);
			m_CE_AStod.SetTextLimit(0,7,true);
			m_CS_Ankunft.SetWindowText(GetString(IDS_STRING330) + CString(" ") + prmAFlight->Stoa.Format("%d.%m.%Y"));
		}
		return 0L;
	}

	if((((UINT)m_CE_DDes3.imID == wParam) || ((UINT)m_CE_DDes4.imID == wParam)) && (!prlNotify->Text.IsEmpty()))
	{
		if((UINT)m_CE_DDes3.imID == wParam)
		{
			if (prlNotify->Text.IsEmpty())
				return 0L;
		}

		if((UINT)m_CE_DDes4.imID == wParam)
		{
			if (prlNotify->Text.IsEmpty())
			{
				prlNotify->Status = false;
				return 0L;
			}
		}

		strcpy(pcmDDes3, "");
		strcpy(pcmDDes4, "");
		CString olTmp3;
		CString olTmp4;
		
		if(prlNotify->Status = ogBCD.GetField("APT", "APC3", "APC4", prlNotify->Text, olTmp3, olTmp4 ))
		{
			strcpy(pcmDDes3, olTmp3);
			strcpy(pcmDDes4, olTmp4);
		}
		else
		{
			RotationAptDlg olDlg(this, prlNotify->Text);
			if(olDlg.DoModal() == IDOK)
			{
				prlNotify->Status = true;
				strcpy(pcmDDes3, olDlg.m_Apc3);
				strcpy(pcmDDes4, olDlg.m_Apc4);
			}
		}

		CString olOld = "";
		m_CE_DDes4.GetWindowText(olOld);
		if (olTmp4 != olOld)
			m_CE_DDes4.SetInitText(pcmDDes4, true);

		olOld = "";
		m_CE_DDes3.GetWindowText(olOld);
		if (olTmp3 != olOld)
			m_CE_DDes3.SetInitText(pcmDDes3, true);


		if(strcmp(pcmDDes3 ,pcgHome) == 0)
		{
			m_CE_DStoa.SetBKColor(YELLOW);
			m_CE_DStoa.SetTypeToTime(true, true);
			m_CE_DStoa.SetTextLimit(4,7,false);
			m_CS_Abflug.SetWindowText(GetString(IDS_STRING340) + CString(" ")+ prmDFlight->Stod.Format("%d.%m.%Y"));
		}
		else
		{
			m_CE_DStoa.SetBKColor(WHITE);
			m_CE_DStoa.SetTypeToTime(false, true);
			m_CE_DStoa.SetTextLimit(0,7,true);
			m_CS_Abflug.SetWindowText(GetString(IDS_STRING338) + CString(" ")+ prmDFlight->Stod.Format("%d.%m.%Y"));
		}
		return 0L;
	}
	


/*
	if(((UINT)m_CE_Act3.imID == wParam) && (!prlNotify->Text.IsEmpty()))
	{
		prlNotify->UserStatus = true;
		prlNotify->Status = ogBCD.GetField("ACT", "ACT3", prlNotify->Text, "ACT5", olTmp);
		if(prlNotify->Status)
		{
			m_CE_Act5.SetInitText(olTmp, true);
		}
	}


	if(((UINT)m_CE_Act5.imID == wParam) && (!prlNotify->Text.IsEmpty()))
	{
		prlNotify->UserStatus = true;
		prlNotify->Status = ogBCD.GetField("ACT", "ACT5", prlNotify->Text, "ACT3", olTmp);
		if(prlNotify->Status)
		{
			m_CE_Act3.SetInitText(olTmp, true);
		}
	}



	CString olRegn;
	m_CE_Regn.GetWindowText(olRegn);

	if( !olRegn.IsEmpty() && ( ((UINT)m_CE_Act5.imID == wParam) || ((UINT)m_CE_Act3.imID == wParam) ) && prlNotify->Status)
	{
		bool blRet = false;
		CString olAct3;
		CString olAct5;

		CString olWhere;
		olRegn.TrimLeft();
		if(!olRegn.IsEmpty())
		{
			olWhere.Format("WHERE REGN = '%s'", olRegn);
			ogBCD.Read( "ACR", olWhere);
		}
		
		
		if(ogBCD.GetFields("ACR", "REGN", olRegn, "ACT3", "ACT5", olAct3, olAct5))
		{
			if((prlNotify->Text != olAct3) && (prlNotify->Text != olAct5))
			{
				if(MessageBox(GetString(IDS_STRING945), GetString(ST_FRAGE), MB_YESNO) == IDYES)				
				{
					m_CE_Act3.GetWindowText(olAct3);
					m_CE_Act5.GetWindowText(olAct5);

					ogBCD.GetField("ACT", "ACT3","ACT5", prlNotify->Text, olAct3, olAct5);

					ogBCD.SetField("ACR", "REGN", olRegn, "ACT3", olAct3, false);
					ogBCD.SetField("ACR", "REGN", olRegn, "ACT5", olAct5, false);
					ogBCD.Save("ACR");
				}
				else
				{
					m_CE_Act3.SetInitText( olAct3, true);
					m_CE_Act5.SetInitText( olAct5, true);
				}
			}
		}
	}


	if(((UINT)m_CE_Regn.imID == wParam) && (!prlNotify->Text.IsEmpty()))
	{
		CString olAct3;
		CString olAct5;
		prlNotify->Status = true;

		
		CString olWhere;
		prlNotify->Text.TrimLeft();
		if(!prlNotify->Text.IsEmpty())
		{
			olWhere.Format("WHERE REGN = '%s'", prlNotify->Text);
			ogBCD.Read( "ACR", olWhere);
		}
		
		if(prlNotify->Status = ogBCD.GetFields("ACR", "REGN", prlNotify->Text, "ACT3", "ACT5", olAct3, olAct5))
		{
			m_CE_Act3.SetInitText(olAct3, true);
			m_CE_Act5.SetInitText(olAct5, true);
		}
		else
		{
			m_CE_Act3.GetWindowText(olAct3);
			m_CE_Act5.GetWindowText(olAct5);

			RotationRegnDlg olDlg(this, prlNotify->Text, olAct3, olAct5);
			if(olDlg.DoModal() == IDOK)
			{
				m_CE_Act5.SetInitText(olDlg.m_Act5, true);
				m_CE_Act3.SetInitText(olDlg.m_Act3, true);
				prlNotify->Status = true;
			}
			else
				m_CE_Regn.SetInitText("", true);

		}
		return 0L;
	}

*/	

	if(((UINT)m_CE_Act3.imID == wParam))
	{
		if (!bmActSelect)
		{
			prlNotify->UserStatus = true;

			CString olWhere;
			if (prlNotify->Text.IsEmpty())
				prlNotify->Text = " ";

			CString olTmp3 = prlNotify->Text;
			CString olTmp5;
			m_CE_Act5.GetWindowText(olTmp5);
			if (olTmp5.IsEmpty())
				olTmp5 = " ";

			CCSPtrArray<RecordSet> prlRecords;
			int ilCount = ogBCD.GetRecordsExt("ACT","ACT3","ACT5",olTmp3,olTmp5, &prlRecords);
			prlRecords.DeleteAll();

			if (ilCount != 1)
			{
				ilCount = ogBCD.GetRecordsExt("ACT","ACT3","HOPO",prlNotify->Text,CString(pcgHome), &prlRecords);
				prlRecords.DeleteAll();

				if (ilCount > 1)
				{
					OnAct3list();
				}
				else
				{
					if(prlNotify->Status = ogBCD.GetField("ACT", "ACT3", prlNotify->Text, "ACT5", olTmp ))
					{
						CString olOld = "";
						m_CE_Act5.GetWindowText(olOld);
						if (olTmp != olOld)
							m_CE_Act5.SetInitText(olTmp);
					}
					else
					{
						RotationActDlg olDlg(this, prlNotify->Text);
						if(olDlg.DoModal() == IDOK)
						{
							prlNotify->Status = true;
							m_CE_Act3.SetInitText(olDlg.m_Act3);
							m_CE_Act5.SetInitText(olDlg.m_Act5);
						}
					}

				}

			}
		}
	}


	if(((UINT)m_CE_Act5.imID == wParam))
	{
		if (!bmActSelect)
		{
			prlNotify->UserStatus = true;

			CString olWhere;
			if (prlNotify->Text.IsEmpty())
				prlNotify->Text = " ";

			CString olTmp5 = prlNotify->Text;
			CString olTmp3;
			m_CE_Act3.GetWindowText(olTmp3);
			if (olTmp3.IsEmpty())
				olTmp3 = " ";

			CCSPtrArray<RecordSet> prlRecords;
			int ilCount = ogBCD.GetRecordsExt("ACT","ACT3","ACT5",olTmp3,olTmp5, &prlRecords);
			prlRecords.DeleteAll();

			if (ilCount != 1)
			{
				ilCount = ogBCD.GetRecordsExt("ACT","ACT5","HOPO",prlNotify->Text,CString(pcgHome), &prlRecords);
				prlRecords.DeleteAll();

				if (ilCount > 1)
				{
					OnAct35list();
				}
				else
				{
					if(prlNotify->Status = ogBCD.GetField("ACT", "ACT5", prlNotify->Text, "ACT3", olTmp ))
					{
						CString olOld = "";
						m_CE_Act3.GetWindowText(olOld);
						if (olTmp != olOld)
							m_CE_Act3.SetInitText(olTmp);
					}
					else
					{
						RotationActDlg olDlg(this, prlNotify->Text);
						if(olDlg.DoModal() == IDOK)
						{
							prlNotify->Status = true;
							m_CE_Act3.SetInitText(olDlg.m_Act3);
							m_CE_Act5.SetInitText(olDlg.m_Act5);
						}
					}
				}

			}
		}
	}

	if(((UINT)m_CE_Regn.imID == wParam) && (prlNotify->Text.IsEmpty()))
	{

			m_CE_Act5.EnableWindow(TRUE);
			m_CE_Act3.EnableWindow(TRUE);
			GetDlgItem(IDC_ACT35LIST)->EnableWindow(TRUE);

	}



	CString olRegn;
	m_CE_Regn.GetWindowText(olRegn);




	if( !olRegn.IsEmpty() && ( ((UINT)m_CE_Act5.imID == wParam) || ((UINT)m_CE_Act3.imID == wParam) ) && prlNotify->Status)
	{
		if (!bmCheckRegn)
			return 0L;

		bool blRet = false;
		CString olAct3;
		CString olAct5;

		CString olWhere;
		olRegn.TrimLeft();
		olRegn.TrimRight();
		if(!olRegn.IsEmpty())
		{
			olWhere.Format("WHERE REGN = '%s'", olRegn);
			ogBCD.Read( "ACR", olWhere);
		}
		else
			return 0L;
		

		if(ogBCD.GetFields("ACR", "REGN", olRegn, "ACT3", "ACT5", olAct3, olAct5))
		{

			CString olTmp3;
			CString olTmp5;
			m_CE_Act3.GetWindowText(olTmp3);
			m_CE_Act5.GetWindowText(olTmp5);

			if (olTmp3 == " ") olTmp3.Empty();
			if (olTmp5 == " ") olTmp5.Empty();
			if ( (olTmp3 != olAct3) || (olTmp5 != olAct5) )
			{
				bool blIsIn = true;

				if (olTmp3.IsEmpty() && olTmp5.IsEmpty())
					blIsIn = false;

				CString olWhere;
				if (olTmp3.IsEmpty())
					olTmp3 = " ";
				if (olTmp5.IsEmpty())
					olTmp5 = " ";

				CCSPtrArray<RecordSet> prlRecords;
				int ilCount = ogBCD.GetRecordsExt("ACT","ACT3","ACT5",olTmp3,olTmp5, &prlRecords);
				prlRecords.DeleteAll();

				if(!blIsIn || ilCount == 0)
				{
					CFPMSApp::MyTopmostMessageBox(this,GetString(IDS_STRING342), GetString(ST_FEHLER), MB_ICONWARNING);
					return 0L;
				}

				bmCheckRegn = false;

				if(ogPrivList.GetStat("REGN_AC_CHANGE") == '1' || ogPrivList.GetStat("REGN_AC_CHANGE") == ' ')
				{

					if(MessageBox(GetString(IDS_STRING945), GetString(ST_FRAGE), MB_YESNO) == IDYES)				
					{
						m_CE_Act3.GetWindowText(olAct3);
						m_CE_Act5.GetWindowText(olAct5);

						ogBCD.GetField("ACT", "ACT3","ACT5", prlNotify->Text, olAct3, olAct5);

						ogBCD.SetField("ACR", "REGN", olRegn, "ACT3", olAct3, false);
						ogBCD.SetField("ACR", "REGN", olRegn, "ACT5", olAct5, false);
						ogBCD.Save("ACR");
					}
					else
					{
						m_CE_Act3.SetInitText( olAct3, true);
						m_CE_Act5.SetInitText( olAct5, true);
					}
					bmActSelect = true;
				}
				m_CE_Act3.SetInitText( olAct3, true);
				m_CE_Act5.SetInitText( olAct5, true);

			}
		}
	}


	if(((UINT)m_CE_Regn.imID == wParam) && (!prlNotify->Text.IsEmpty()))
	{
		CString olMtow;	//lgp
		CString olAct3;
		CString olAct5;
		prlNotify->Status = true;

		if((ogPrivList.GetStat("REGN_AC_CHANGE") != '1' && ogPrivList.GetStat("REGN_AC_CHANGE") != ' '))
		{
		
			m_CE_Act5.EnableWindow(FALSE);
			m_CE_Act3.EnableWindow(FALSE);
			GetDlgItem(IDC_ACT35LIST)->EnableWindow(FALSE);
		}
		else
		{
			m_CE_Act5.EnableWindow(TRUE);
			m_CE_Act3.EnableWindow(TRUE);
			GetDlgItem(IDC_ACT35LIST)->EnableWindow(TRUE);
		}

		

		CString olWhere;
		prlNotify->Text.TrimLeft();
		prlNotify->Text.TrimRight();
		if(!prlNotify->Text.IsEmpty())
		{
			olWhere.Format("WHERE REGN = '%s'", prlNotify->Text);
			ogBCD.Read( "ACR", olWhere);
		}
		else
			return 0L;
		
		if(prlNotify->Status = ogBCD.GetFields("ACR", "REGN", prlNotify->Text, "ACT3", "ACT5", olAct3, olAct5))
		{
			m_CE_Act3.SetInitText(olAct3, true);
			m_CE_Act5.SetInitText(olAct5, true);
		}
		else
		{
			m_CE_Act3.GetWindowText(olAct3);
			m_CE_Act5.GetWindowText(olAct5);


			if(ogPrivList.GetStat("REGN_INSERT") == '1' || ogPrivList.GetStat("REGN_INSERT") == ' ')
			{

				RotationRegnDlg olDlg(this, prlNotify->Text, olAct3, olAct5);
				if(olDlg.DoModal() == IDOK)
				{
					m_CE_Act5.SetInitText(olDlg.m_Act5, true);
					m_CE_Act3.SetInitText(olDlg.m_Act3, true);
					prlNotify->Status = true;
				}
				else
					//m_CE_Regn.SetInitText("", true);
					m_CE_Regn.SetInitText(m_Regn);	// TLE: UFIS 3697
			}
			else
			{
					prlNotify->UserStatus = true;
					prlNotify->Status = false;

			}

		}
		bmActSelect = true;



		if(prlNotify->Status = ogBCD.GetField("ACR", "REGN", olRegn, "MTOW", olMtow))//lgp
		{
			if(olMtow.IsEmpty())
			{	
				pomBMTOW->SetInitText("", true);
			}
			else{
				olMtow.TrimLeft('0');
				pomBMTOW->SetInitText(olMtow, true);
			}
		}


	return 0L;
	}

	if( (UINT)m_CE_AGta1.imID == wParam)
	{
		if(prlNotify->Text.IsEmpty())
		{
			CString olOld = "";
			m_CE_ATga1.GetWindowText(olOld);
			if (olOld != "")
				m_CE_ATga1.SetInitText("",true);
		}
		else
		{
			prlNotify->UserStatus = true;

			if( ogPrivList.GetStat("GATE_NAME_CHECK") == '-')
			{
				prlNotify->Status = true;
				return 0L;
			}
			
			if(prlNotify->Status = ogBCD.GetField("GAT_PORT", "GNAM", prlNotify->Text, "TERM", olTmp ))
			{
				CString olOld = "";
				m_CE_ATga1.GetWindowText(olOld);
				if (olTmp != olOld)
					m_CE_ATga1.SetInitText(olTmp,true);
	
				if (prlNotify->IsChanged || prmAFlight->Ga1b == TIMENULL || prmAFlight->Ga1e == TIMENULL)
				{
					prmAFlight->ClearAGat(1);
				
					prmAFlight->Ga1b = prmAFlight->Tifa;

					CTimeSpan olDura;
					if (!blDatGatDura)
					{
						GetGatDefAllocDur(prlNotify->Text, olDura, true);
					}
					else
					{
						olReqFields.RemoveAll();
						olReqFields.Add("BAA4");
						GetDefAllocFC(CString("GAT"), prlNotify->Text, olForcastMapArr, olReqFields);

						for (int i=0; i<olReqFields.GetSize(); i++)
						{
							CString olDuration("");
							olForcastMapArr.Lookup(olReqFields.GetAt(i), olDuration);
							int ilDura = atoi(olDuration);
							if (ilDura == 0)
								ilDura = 30;

							olDura = CTimeSpan(0,0,ilDura,0);
						}
					}


					prmAFlight->Ga1e = prmAFlight->Tifa + olDura;
				}
			}
		}
		return 0L;
	}


	if( (UINT)m_CE_AGta2.imID == wParam)
	{
		if(prlNotify->Text.IsEmpty())
		{
			CString olOld = "";
			m_CE_ATga2.GetWindowText(olOld);
			if (olOld != "")
				m_CE_ATga2.SetInitText("",true);
		}
		else
		{
			prlNotify->UserStatus = true;

			if( ogPrivList.GetStat("GATE_NAME_CHECK") == '-')
			{
				prlNotify->Status = true;
				return 0L;
			}
			
			if(prlNotify->Status = ogBCD.GetField("GAT_PORT", "GNAM", prlNotify->Text, "TERM", olTmp ))
			{
				CString olOld = "";
				m_CE_ATga2.GetWindowText(olOld);
				if (olTmp != olOld)
					m_CE_ATga2.SetInitText(olTmp,true);

				if (prlNotify->IsChanged || prmAFlight->Ga2b == TIMENULL || prmAFlight->Ga2e == TIMENULL)
				{
					prmAFlight->ClearAGat(2);

					prmAFlight->Ga2b = prmAFlight->Tifa;

					CTimeSpan olDura;
					if (!blDatGatDura)
					{
						GetGatDefAllocDur(prlNotify->Text, olDura, true);
					}
					else
					{
						olReqFields.RemoveAll();
						olReqFields.Add("BAA5");
						GetDefAllocFC(CString("GAT"), prlNotify->Text, olForcastMapArr, olReqFields);

						for (int i=0; i<olReqFields.GetSize(); i++)
						{
							CString olDuration("");
							olForcastMapArr.Lookup(olReqFields.GetAt(i), olDuration);
							int ilDura = atoi(olDuration);
							if (ilDura == 0)
								ilDura = 30;

							olDura = CTimeSpan(0,0,ilDura,0);
						}
					}

//					CTimeSpan olDura;
//					GetGatDefAllocDur(prlNotify->Text, olDura);
					prmAFlight->Ga2e = prmAFlight->Tifa + olDura;		
				}
			}
		}
		return 0L;
	}


	if( (UINT)m_CE_DGtd1.imID == wParam)
	{
		if(prlNotify->Text.IsEmpty())
		{
			CString olOld = "";
			m_CE_DTgd1.GetWindowText(olOld);
			if (olOld != "")
			{
				m_CE_DTgd1.SetInitText("",true);
				if(bgFIDSLogo)
				{
					m_CL_DGatLogo.SetCurSel(0);
				}
			}
		}
		else
		{
			prlNotify->UserStatus = true;


			if( ogPrivList.GetStat("GATE_NAME_CHECK") == '-')
			{
				prlNotify->Status = true;
				return 0L;
			}

				if(prlNotify->Status = ogBCD.GetField("GAT_GATE", "GNAM", prlNotify->Text, "TERM", olTmp ))
				{
					CString olOld = "";
					m_CE_DTgd1.GetWindowText(olOld);
					if (olTmp != olOld)
						m_CE_DTgd1.SetInitText(olTmp,true);
				
					if (prlNotify->IsChanged || prmDFlight->Gd1b == TIMENULL || prmDFlight->Gd1e == TIMENULL)
					{
						prmDFlight->ClearDGat(1);

						prmDFlight->Gd1e = prmDFlight->Tifd;

						CTimeSpan olDura;
						if (!blDatGatDura)
						{
							GetGatDefAllocDur(prlNotify->Text, olDura);
						}
						else
						{
							olReqFields.RemoveAll();
							olReqFields.Add("BAD4");
							GetDefAllocFC(CString("GAT"), prlNotify->Text, olForcastMapDep, olReqFields);

							for (int i=0; i<olReqFields.GetSize(); i++)
							{
								CString olDuration("");
								olForcastMapDep.Lookup(olReqFields.GetAt(i), olDuration);
								int ilDura = atoi(olDuration);
								if (ilDura == 0)
									ilDura = 30;

								olDura = CTimeSpan(0,0,ilDura,0);
							}
						}
	//					CTimeSpan olDura;
	//					GetGatDefAllocDur(prlNotify->Text, olDura);

						prmDFlight->Gd1b = prmDFlight->Tifd - olDura;	
					}
			}
		}
	}



	if( (UINT)m_CE_DGtd2.imID == wParam)
	{
		if(prlNotify->Text.IsEmpty())
		{
			CString olOld = "";
			m_CE_DTgd2.GetWindowText(olOld);
			if (olOld != "")
			{
				m_CE_DTgd2.SetInitText("",true);
				if(bgFIDSLogo)
				{
					m_CL_DGatLogo2.SetCurSel(0);
				}
			}
		}
		else
		{
			prlNotify->UserStatus = true;
			
			if( ogPrivList.GetStat("GATE_NAME_CHECK") == '-')
			{
				prlNotify->Status = true;
				return 0L;
			}

			if(prlNotify->Status = ogBCD.GetField("GAT_GATE", "GNAM", prlNotify->Text, "TERM", olTmp ))
			{
				CString olOld = "";
				m_CE_DTgd2.GetWindowText(olOld);
				if (olTmp != olOld)
					m_CE_DTgd2.SetInitText(olTmp,true);

				if (prlNotify->IsChanged || prmDFlight->Gd2b == TIMENULL || prmDFlight->Gd2e == TIMENULL)
				{
					prmDFlight->ClearDGat(2);
				
					prmDFlight->Gd2e = prmDFlight->Tifd;

					CTimeSpan olDura;
					if (!blDatGatDura)
					{
						GetGatDefAllocDur(prlNotify->Text, olDura);
					}
					else
					{
						olReqFields.RemoveAll();
						olReqFields.Add("BAD5");
						GetDefAllocFC(CString("GAT"), prlNotify->Text, olForcastMapDep, olReqFields);

						for (int i=0; i<olReqFields.GetSize(); i++)
						{
							CString olDuration("");
							olForcastMapDep.Lookup(olReqFields.GetAt(i), olDuration);
							int ilDura = atoi(olDuration);
							if (ilDura == 0)
								ilDura = 30;

							olDura = CTimeSpan(0,0,ilDura,0);
						}
					}
//					CTimeSpan olDura;
//					GetGatDefAllocDur(prlNotify->Text, olDura);

					prmDFlight->Gd2b = prmDFlight->Tifd - olDura;	
				}
			}
		}
		return 0L;
	}




	if( (UINT)m_CE_ABlt1.imID == wParam)
	{
		if(prlNotify->Text.IsEmpty())
		{
			CString olOld = "";
			m_CE_ATmb1.GetWindowText(olOld);
			if (olOld != "")
				m_CE_ATmb1.SetInitText("",true);
		}
		else
		{
			

			prlNotify->UserStatus = true;
			if(prlNotify->Status = ogBCD.GetField("BLT", "BNAM", prlNotify->Text, "TERM", olTmp ))
			{
				CString olOld = "";
				m_CE_ATmb1.GetWindowText(olOld);
				if (olTmp != olOld)
					m_CE_ATmb1.SetInitText(olTmp,true);

				CString olExt;
				if (ogPosDiaFlightData.GetExit(prlNotify->Text, 1, olExt))
				{
					m_CE_AExt1.GetWindowText(olOld);
					if (olExt != olOld)
						m_CE_AExt1.SetInitText(olExt,true);

					if(ogBCD.GetField("EXT", "ENAM", olExt, "TERM", olTmp ))
					{
						m_CE_ATet1.GetWindowText(olOld);
						if (olTmp != olOld)
							m_CE_ATet1.SetInitText(olTmp, true);
					}
				}

				if (prlNotify->IsChanged || prmAFlight->B1bs == TIMENULL || prmAFlight->B1es == TIMENULL)
				{
					prmAFlight->ClearBlt(1);
				
					prmAFlight->B1bs = prmAFlight->Tifa;

					CTimeSpan olDura;
					if (!bgUseBeltAllocTime) 
					GetBltDefAllocDur(prlNotify->Text, olDura);
					else
					{
				//		GetBltDefAllocDur(prmAFlight->Flti,prmAFlight->Act3, prmAFlight->Act5, prmAFlight->Org3, prmAFlight->Org4,prlNotify->Text, olDura);
						CString olAct3;
						CString olAct5;
						CString olFlti;
						CString olOrg3;
						CString olOrg4;

						m_CE_AOrg3L.GetWindowText(olOrg3);
						m_CE_AOrg3.GetWindowText(olOrg4);
						m_CE_AFlti.GetWindowText(olFlti);
						m_CE_Act3.GetWindowText(olAct3);
						m_CE_Act5.GetWindowText(olAct5);
						GetBltDefAllocDur((LPCSTR)olFlti,(LPCSTR)olAct3,(LPCSTR)olAct5,(LPCSTR)olOrg3,(LPCSTR)olOrg4,prlNotify->Text, olDura);
					}
					prmAFlight->B1es = prmAFlight->Tifa + olDura;		
				}
			}
		}
		return 0L;
	}



	if( (UINT)m_CE_ABlt2.imID == wParam)
	{
		if(prlNotify->Text.IsEmpty())
		{
			CString olOld = "";
			m_CE_ATmb2.GetWindowText(olOld);
			if (olOld != "")
				m_CE_ATmb2.SetInitText("",true);
		}
		else
		{
			
			prlNotify->UserStatus = true;
			if(prlNotify->Status = ogBCD.GetField("BLT", "BNAM", prlNotify->Text, "TERM", olTmp ))
			{
				CString olOld = "";
				m_CE_ATmb2.GetWindowText(olOld);
				if (olTmp != olOld)
					m_CE_ATmb2.SetInitText(olTmp,true);

				CString olExt;
				if (ogPosDiaFlightData.GetExit(prlNotify->Text, 2, olExt))
				{
					m_CE_AExt2.GetWindowText(olOld);
					if (olExt != olOld)
						m_CE_AExt2.SetInitText(olExt,true);

					if(ogBCD.GetField("EXT", "ENAM", olExt, "TERM", olTmp ))
					{
						m_CE_ATet2.GetWindowText(olOld);
						if (olTmp != olOld)
							m_CE_ATet2.SetInitText(olTmp, true);
					}
				}

				if (prlNotify->IsChanged || prmAFlight->B2bs == TIMENULL || prmAFlight->B2es == TIMENULL)
				{
					prmAFlight->ClearBlt(2);

					prmAFlight->B2bs = prmAFlight->Tifa;

					CTimeSpan olDura;
					if (!bgUseBeltAllocTime) 
					GetBltDefAllocDur(prlNotify->Text, olDura);
					else
					{
						CString olAct3;
						CString olAct5;
						CString olFlti;
						CString olOrg3;
						CString olOrg4;

						m_CE_AOrg3L.GetWindowText(olOrg3);
						m_CE_AOrg3.GetWindowText(olOrg4);
						m_CE_AFlti.GetWindowText(olFlti);
						m_CE_Act3.GetWindowText(olAct3);
						m_CE_Act5.GetWindowText(olAct5);
						GetBltDefAllocDur((LPCSTR)olFlti,(LPCSTR)olAct3,(LPCSTR)olAct5,(LPCSTR)olOrg3,(LPCSTR)olOrg4,prlNotify->Text, olDura);
					}
					//	GetBltDefAllocDur(prmAFlight->Flti,prmAFlight->Act3, prmAFlight->Act5, prmAFlight->Org3, prmAFlight->Org4,prlNotify->Text, olDura);
					prmAFlight->B2es = prmAFlight->Tifa + olDura;			
				}
			}
		}
		return 0L;
	}



	if(((UINT)m_CE_AExt1.imID == wParam) || ((UINT)m_CE_AExt2.imID == wParam))
	{
		if(prlNotify->Text.IsEmpty())
		{
			if((UINT)m_CE_AExt1.imID == wParam)
			{
				CString olOld = "";
				m_CE_ATet1.GetWindowText(olOld);
				if ("" != olOld)
					m_CE_ATet1.SetInitText("", true);
			}
			if((UINT)m_CE_AExt2.imID == wParam)
			{
				CString olOld = "";
				m_CE_ATet2.GetWindowText(olOld);
				if ("" != olOld)
					m_CE_ATet2.SetInitText("", true);
			}
		}
		else
		{
			prlNotify->UserStatus = true;
			if(prlNotify->Status = ogBCD.GetField("EXT", "ENAM", prlNotify->Text, "TERM", olTmp ))
			{
				if((UINT)m_CE_AExt1.imID == wParam)
				{
					CString olOld = "";
					m_CE_ATet1.GetWindowText(olOld);
					if (olTmp != olOld)
						m_CE_ATet1.SetInitText(olTmp, true);
				}
				if((UINT)m_CE_AExt2.imID == wParam)
				{
					CString olOld = "";
					m_CE_ATet2.GetWindowText(olOld);
					if (olTmp != olOld)
						m_CE_ATet2.SetInitText(olTmp, true);
				}
			}
		}
		return 0L;
	}

	if((UINT)m_CE_DWro1.imID == wParam)
	{
		if(prlNotify->Text.IsEmpty())
		{
			CString olOld = "";
			m_CE_DTwr1.GetWindowText(olOld);
			if ("" != olOld)
				m_CE_DTwr1.SetInitText("", true);
		}
		else
		{
			prlNotify->UserStatus = true;
			if(prlNotify->Status = ogBCD.GetField("WRO", "WNAM", prlNotify->Text, "TERM", olTmp ))
			{
				CString olOld = "";
				m_CE_DTwr1.GetWindowText(olOld);
				if (olTmp != olOld)
					m_CE_DTwr1.SetInitText(olTmp, true);

				if (prlNotify->IsChanged || prmDFlight->W1bs == TIMENULL || prmDFlight->W1es == TIMENULL)
				{
					prmDFlight->ClearWro();

					prmDFlight->W1es = prmDFlight->Tifd;

					CTimeSpan olDura;
					GetWroDefAllocDur(prlNotify->Text, olDura);
					prmDFlight->W1bs = prmDFlight->Tifd - olDura;
				}
			}
		}
		return 0L;
	}

	if((UINT)m_CE_DWro3.imID == wParam)
	{
		if(prlNotify->Text.IsEmpty())
		{
			CString olOld = "";
			m_CE_DTwr3.GetWindowText(olOld);
			if ("" != olOld)
				m_CE_DTwr3.SetInitText("", true);
		}
		else
		{
			prlNotify->UserStatus = true;
			if(prlNotify->Status = ogBCD.GetField("WRO", "WNAM", prlNotify->Text, "TERM", olTmp ))
			{
				CString olOld = "";
				m_CE_DTwr3.GetWindowText(olOld);
				if (olTmp != olOld)
					m_CE_DTwr3.SetInitText(olTmp, true);

				if (prlNotify->IsChanged || prmDFlight->W2bs == TIMENULL || prmDFlight->W2es == TIMENULL)
				{
					prmDFlight->ClearWro();

					prmDFlight->W2es = prmDFlight->Tifd;

					CTimeSpan olDura;
					GetWroDefAllocDur(prlNotify->Text, olDura);
					prmDFlight->W2bs = prmDFlight->Tifd - olDura;
				}
			}
		}
		return 0L;
	}


	//////////////////////////////////////////////////////////
	if(prlNotify->Text.IsEmpty())
		return 0L;


	if(((UINT)m_CE_ATtyp.imID == wParam) || ((UINT)m_CE_DTtyp.imID == wParam))
	{
		prlNotify->Status = ogBCD.GetField("NAT", "TTYP", prlNotify->Text, "URNO", olTmp );
		prlNotify->UserStatus = true;
		return 0L;
	}

	if(((UINT)m_CE_AHtyp.imID == wParam) || ((UINT)m_CE_DHtyp.imID == wParam))
	{
		prlNotify->Status = ogBCD.GetField("HTY", "HTYP", prlNotify->Text, "URNO", olTmp );
		prlNotify->UserStatus = true;
		return 0L;
	}

	if(((UINT)m_CE_AStyp.imID == wParam) || ((UINT)m_CE_DStyp.imID == wParam))
	{
		prlNotify->Status = ogBCD.GetField("STY", "STYP", prlNotify->Text, "URNO", olTmp );
		prlNotify->UserStatus = true;
		return 0L;
	}

	if( (UINT)m_CE_APsta.imID == wParam)
	{
		prlNotify->Status = ogBCD.GetField("PST", "PNAM", prlNotify->Text, "URNO", olTmp );
		prlNotify->UserStatus = true;

		//Changed while fixing for the PRF 8495
		if(bgRelatedGatePosition)
		{
			if(prlNotify->Status && prlNotify->IsChanged)
			{
				if( !CString(prmAFlight->Ttyp).IsEmpty() )
				{
					if(!FindInStrArray(ogSpotAllocation.omGatNatNoAllocList, prmAFlight->Ttyp))
					{

						CString olAirBridge;
						ogBCD.GetField("PST", "URNO", olTmp, "BRGS", olAirBridge);

						if(!olAirBridge.IsEmpty())
						{
							CStringArray olConnectedGates;
							ogBasicData.GetRelatedGatesForPosition(olTmp, olConnectedGates);
							
							CString olStrGat1 = "",olStrGat2 = "";

							if(olConnectedGates.GetSize() > 0)
							{
								if(olConnectedGates.GetSize() >= 2)
								{
									olStrGat1 = olConnectedGates.GetAt(0);	
									olStrGat2 = olConnectedGates.GetAt(1);	
								}
								else
								{
									olStrGat1 = olConnectedGates.GetAt(0);
									olStrGat2 = "";
								}

							}
							m_CE_AGta1.SetWindowText(olStrGat1);
							m_CE_AGta2.SetWindowText(olStrGat2);	
						}
						else
						{
							m_CE_AGta1.SetWindowText("");
							m_CE_AGta2.SetWindowText("");	
						}
					}	
				}
			}
		}

		if(prlNotify->Status && (prlNotify->IsChanged || prmAFlight->Pabs == TIMENULL || prmAFlight->Paes == TIMENULL))
		{
			prmAFlight->ClearAPos();

			prmAFlight->Pabs = prmAFlight->Tifa;

			CTimeSpan olDura;
			if (!blDatPosDura)
			{
				GetPosDefAllocDur(prlNotify->Text, atoi(prmAFlight->Ming), olDura);			
			}
			else
			{
				olReqFields.RemoveAll();
				olReqFields.Add("BAA4");
				GetDefAllocFC(CString("PST"), prlNotify->Text, olForcastMapArr, olReqFields);

				for (int i=0; i<olReqFields.GetSize(); i++)
				{
					CString olDuration("");
					olForcastMapArr.Lookup(olReqFields.GetAt(i), olDuration);
					int ilDura = atoi(olDuration);
					if (ilDura == 0)
						ilDura = 30;

					olDura = CTimeSpan(0,0,ilDura,0);
				}
			}
//			CTimeSpan olDura;
//			GetPosDefAllocDur(prlNotify->Text, atoi(prmAFlight->Ming), olDura);			
			prmAFlight->Paes = prmAFlight->Tifa + olDura;

			if (prmDFlight)
			{
				if (CString(prmDFlight->Pstd).IsEmpty() && prmDFlight->Pdbs == TIMENULL && prmDFlight->Pdes == TIMENULL)
				{
					prmDFlight->Pdes = prmDFlight->Tifd;

					CTimeSpan olDuraDep;
					if (!blDatPosDura)
					{
						GetPosDefAllocDur(prlNotify->Text, atoi(prmDFlight->Ming), olDuraDep);			
					}
					else
					{
						olReqFields.RemoveAll();
						olReqFields.Add("BAA5");
						GetDefAllocFC(CString("PST"), prlNotify->Text, olForcastMapDep, olReqFields);

						for (int i=0; i<olReqFields.GetSize(); i++)
						{
							CString olDuration("");
							olForcastMapDep.Lookup(olReqFields.GetAt(i), olDuration);
							int ilDura = atoi(olDuration);
							if (ilDura == 0)
								ilDura = 30;

							olDuraDep = CTimeSpan(0,0,ilDura,0);
						}
					}
//					CTimeSpan olDuraDep;
//					GetPosDefAllocDur(prlNotify->Text, atoi(prmDFlight->Ming), olDuraDep);						
					prmDFlight->Pdbs = prmDFlight->Tifd - olDuraDep;


				}
//////////////////////////////////////
				if (CString(prmDFlight->Pstd).IsEmpty())
				{
						m_CE_DPstd.SetWindowText(prlNotify->Text);
						if(prmDFlight != NULL)
						{
							if(bgRelatedGatePosition)
							{

								if( !CString(prmDFlight->Ttyp).IsEmpty() )
								{
									if(!FindInStrArray(ogSpotAllocation.omGatNatNoAllocList, prmDFlight->Ttyp))
									{
										CString olAirBridge;
										ogBCD.GetField("PST", "PNAM", prlNotify->Text, "BRGS", olAirBridge);

										if(!olAirBridge.IsEmpty())
										{

											CStringArray olConnectedGates;
											ogBCD.GetField("PST", "PNAM", prlNotify->Text, "URNO", olTmp );
											ogBasicData.GetRelatedGatesForPosition(olTmp, olConnectedGates);
											
											CString olStrGat1="",olStrGat2="";

											if(olConnectedGates.GetSize() > 0)
											{
												if(olConnectedGates.GetSize() >= 2)
												{
													olStrGat1 = olConnectedGates.GetAt(0);	
													olStrGat2 = olConnectedGates.GetAt(1);	
												}
												else
												{
													olStrGat1 = olConnectedGates.GetAt(0);
													olStrGat2 = "";
												}
											}

											m_CE_DGtd1.SetWindowText(olStrGat1);
											m_CE_DGtd2.SetWindowText(olStrGat2);
										}
										else
										{
											m_CE_DGtd1.SetWindowText("");
											m_CE_DGtd2.SetWindowText("");
										}
									}
								}

							}
						
				

					}
//////////////////////////////////////


				
				}
			}
		}
	}

	if( (UINT)m_CE_DPstd.imID == wParam)
	{
		prlNotify->Status = ogBCD.GetField("PST", "PNAM", prlNotify->Text, "URNO", olTmp );
		prlNotify->UserStatus = true;

		//Changed while fixing for the PRF 8494
		if(bgRelatedGatePosition)
		{
			if(prlNotify->Status && prlNotify->IsChanged)
			{

				if( !CString(prmDFlight->Ttyp).IsEmpty() )
				{
					if(!FindInStrArray(ogSpotAllocation.omGatNatNoAllocList, prmDFlight->Ttyp))
					{

						CString olAirBridge;
						ogBCD.GetField("PST", "URNO", olTmp, "BRGS", olAirBridge);

						if(!olAirBridge.IsEmpty())
						{
							CStringArray olConnectedGates;
							ogBasicData.GetRelatedGatesForPosition(olTmp, olConnectedGates);
							
							CString olStrGat1 = "",olStrGat2 = "";

							if(olConnectedGates.GetSize() > 0)
							{
								if(olConnectedGates.GetSize() >= 2)
								{
									olStrGat1 = olConnectedGates.GetAt(0);	
									olStrGat2 = olConnectedGates.GetAt(1);	
								}
								else
								{
									olStrGat1 = olConnectedGates.GetAt(0);
									olStrGat2 = "";
								}
								  m_CE_DGtd1.SetWindowText(olStrGat1);
								  m_CE_DGtd2.SetWindowText(olStrGat2);	

							}
						}
						else
						{
							  m_CE_DGtd1.SetWindowText("");
							  m_CE_DGtd2.SetWindowText("");	
						}
					}
				}


			}
		}
		
		if(prlNotify->Status && (prlNotify->IsChanged || prmDFlight->Pdbs == TIMENULL || prmDFlight->Pdes == TIMENULL))
		{
			prmDFlight->ClearDPos();

			prmDFlight->Pdes = prmDFlight->Tifd;

			CTimeSpan olDura;
			if (!blDatPosDura)
			{
				GetPosDefAllocDur(prlNotify->Text, atoi(prmDFlight->Ming), olDura);			
			}
			else
			{
				olReqFields.RemoveAll();
				olReqFields.Add("BAA5");
				GetDefAllocFC(CString("PST"), prlNotify->Text, olForcastMapDep, olReqFields);

				for (int i=0; i<olReqFields.GetSize(); i++)
				{
					CString olDuration("");
					olForcastMapDep.Lookup(olReqFields.GetAt(i), olDuration);
					int ilDura = atoi(olDuration);
					if (ilDura == 0)
						ilDura = 30;

					olDura = CTimeSpan(0,0,ilDura,0);
				}
			}
//			CTimeSpan olDura;
//			GetPosDefAllocDur(prlNotify->Text, atoi(prmDFlight->Ming), olDura);						

			prmDFlight->Pdbs = prmDFlight->Tifd - olDura;
		}
	}



	if((UINT)m_CE_AIfra.imID == wParam)
	{
		m_CE_AIfra.GetWindowText(m_AIfra);
		if( m_AIfra != "V")
			m_AIfra = "";
		m_CE_AIfra.SetWindowText(m_AIfra);
	}

	if((UINT)m_CE_DIfrd.imID == wParam)
	{
		m_CE_DIfrd.GetWindowText(m_DIfrd);
		if( m_DIfrd != "V")
			m_DIfrd = "";
		m_CE_DIfrd.SetWindowText(m_DIfrd);
	}



	if((UINT)m_CE_APsta.imID == wParam)
	{
		CString olGpuStat = ogBCD.GetField("PST", "PNAM", prlNotify->Text, "GPUS");
		
		if(olGpuStat != "X")
			m_CB_AGpu.EnableWindow(FALSE);
		else
			m_CB_AGpu.EnableWindow(TRUE);


	}

	if((UINT)m_CE_DPstd.imID == wParam)
	{
		CString olGpuStat = ogBCD.GetField("PST", "PNAM", prlNotify->Text, "GPUS");

		if(olGpuStat != "X")
			m_CB_DGpu.EnableWindow(FALSE);
		else
			m_CB_DGpu.EnableWindow(TRUE);

	
	}

	if((((UINT)m_CE_ADcd1.imID == wParam) || ((UINT)m_CE_ADcd2.imID == wParam)) ||
	   (((UINT)m_CE_DDcd1.imID == wParam) || ((UINT)m_CE_DDcd2.imID == wParam)))
	{
		prlNotify->UserStatus = true;
		
		prlNotify->Status = ogBCD.GetField("DEN", "DECN", prlNotify->Text, "DECN", olTmp );
		
		if(!prlNotify->Status )
			prlNotify->Status = ogBCD.GetField("DEN", "DECA", prlNotify->Text, "DECA", olTmp );

		return 0L;
	}



	if((UINT)m_CE_ARwya.imID == wParam)
	{
		CString olRnam;
		CString olRnum;
		prlNotify->UserStatus = true;
		if(prlNotify->Status = ogBCD.GetField("RWY", "RNUM", prlNotify->Text, "RNAM", olRnam ))
		{
			m_CE_ARwya.SetInitText(olRnam, true);
		}
		else
		{
			prlNotify->Status = ogBCD.GetField("RWY", "RNAM", prlNotify->Text, "RNUM", olRnum );
		}
		return 0L;
	}

	if((UINT)m_CE_DRwyd.imID == wParam)
	{
		CString olRnam;
		CString olRnum;
		prlNotify->UserStatus = true;

		if(prlNotify->Status = ogBCD.GetField("RWY", "RNUM", prlNotify->Text, "RNAM", olRnam ))
		{
			m_CE_DRwyd.SetInitText(olRnam, true);
		}
		else
		{
			prlNotify->Status = ogBCD.GetField("RWY", "RNAM", prlNotify->Text, "RNUM", olRnum );
		}
		return 0L;
	}
		
	return 0L;
}

LONG RotationDlg::OnEditChanged( UINT wParam, LPARAM lParam)
{
	CCSEDITNOTIFY *prlNotify = (CCSEDITNOTIFY*)lParam;

	if(!bmInit)
		return 0L;


   if(prlNotify != NULL)
   {
		CString olFieldName = prlNotify->SourceControl->Name();
		if(olFieldName.GetLength() >= 9)
		{
			CString olTable = olFieldName.Left(3);
			CString olField = olFieldName.Mid(3,5);
			CString olAdid = olFieldName.Mid(3,1);
			CString olType = olFieldName.Right(1);

			
			if(ogRotationDlgAutoLockFields.Find(olField) >= 0 && !olField.IsEmpty())
			{
				if(olField == "RREGN")
				{
					if(!prlNotify->Text.IsEmpty() )
						AutoLock();
				}
				else
					AutoLock();
			}
		}

		//if(olFieldName.GetLength() == 0)
		//	return 0L;


   }

	if((UINT)m_CE_Act3.imID == wParam || (UINT)m_CE_Act5.imID == wParam)
	{
		bmActSelect = false;
		bmCheckRegn = true;
	}

// begin : reject if opening times
	CString olTmp;

//	arr-gate1
	if((UINT)m_CE_AGta1.imID == wParam)
	{
		m_CE_AGta1.GetWindowText(olTmp);

		CString olGate = prmAFlightSave->Gta1;

		if(olTmp != olGate && strcmp(prmAFlight->Fga1, "X") == 0)
		{
			CFPMSApp::MyTopmostMessageBox(this, GetString(IDS_STRING2951), GetString(IDS_WARNING), MB_OK | MB_ICONWARNING);
		}

		if (prmAFlightSave->Ga1x != TIMENULL && !olGate.IsEmpty())
		{
			if (olTmp != olGate)
			{
				m_CE_AGta1.SetInitText(olGate);
//				MessageBox( GetString(IDS_STRING_GATE_ISOPEN), GetString(ST_FEHLER), MB_OK | MB_ICONERROR);								
				CFPMSApp::MyTopmostMessageBox(this, GetString(IDS_STRING_GATE_ISOPEN), GetString(ST_FEHLER), MB_OK | MB_ICONERROR);
			}
		}
		CheckForChangeReasons(pomBTCRCAGTA1,m_AGta1,olTmp,&(omReasons.blAGta1Resf), 'A'); //PRF 8379 
	}
//	arr-gate2
	if((UINT)m_CE_AGta2.imID == wParam)
	{
		m_CE_AGta2.GetWindowText(olTmp);

		CString olGate = prmAFlightSave->Gta2;

		if(olTmp != olGate && strcmp(prmAFlight->Fga2, "X") == 0)
		{
			CFPMSApp::MyTopmostMessageBox(this, GetString(IDS_STRING2951), GetString(IDS_WARNING), MB_OK | MB_ICONWARNING);
		}

		if (prmAFlightSave->Ga2x != TIMENULL && !olGate.IsEmpty())
		{
			if (olTmp!= olGate)
			{
				m_CE_AGta2.SetInitText(olGate);
//				MessageBox( GetString(IDS_STRING_GATE_ISOPEN), GetString(ST_FEHLER), MB_OK | MB_ICONERROR);								
				CFPMSApp::MyTopmostMessageBox(this, GetString(IDS_STRING_GATE_ISOPEN), GetString(ST_FEHLER), MB_OK | MB_ICONERROR);
			}
		}
		CheckForChangeReasons(pomBTCRCAGTA2,m_AGta2,olTmp,&(omReasons.blAGta2Resf), 'A'); //PRF 8379 
		
	}


//prf 8180	
if(bgRotationmask==true)
{
	if((UINT)m_CE_AB1ba.imID==wParam||(UINT)m_CE_AB1ea.imID==wParam||(UINT)m_CE_AB2ba.imID==wParam||(UINT)m_CE_AB2ea.imID==wParam)
	{
	
	CString olB1ba;
    CString olB1ea;
	CString olB2ba;
	CString olB2ea;
	CString olObl;
	m_CE_AB1ba.GetWindowText(olB1ba);
	m_CE_AB1ea.GetWindowText(olB1ea);
	m_CE_AB2ba.GetWindowText(olB2ba);
	m_CE_AB2ea.GetWindowText(olB2ea);
    m_CE_AOnbl.GetWindowText(olObl); 
	  if(prmAFlightSave->Onbl==TIMENULL)
		{
		if((olObl.IsEmpty() && !olB1ba.IsEmpty())||(olObl.IsEmpty() && !olB1ea.IsEmpty())||(olObl.IsEmpty() && !olB2ba.IsEmpty())||(olObl.IsEmpty() && !olB2ea.IsEmpty())||MAROON==m_CE_AOnbl.GetTextColor())
			{	
		CFPMSApp::MyTopmostMessageBox(this, GetString(IDS_STRING2779), GetString(ST_FEHLER), MB_OK | MB_ICONERROR);
        m_CE_AB1ba.SetInitText("");
		m_CE_AB1ea.SetInitText("");
		m_CE_AB2ba.SetInitText("");
	    m_CE_AB2ea.SetInitText("");	
		m_CE_AOnbl.SetInitText("");
			}	
		}      	
	 
	}
}			
	

//prf 8177

if(bgRotationmask==true) 
{
  if((UINT)m_CE_DOfbl.imID == wParam)
	{
		if(prmAFlightSave->Stoa!=TIMENULL)
		{
			CString olObl;
			CString olOfbl;
			m_CE_AOnbl.GetWindowText(olObl);
			m_CE_DOfbl.GetWindowText(olOfbl);
			if(prmAFlightSave->Onbl==TIMENULL)
			{
				if((olObl.IsEmpty()&& !olOfbl.IsEmpty())||MAROON==m_CE_AOnbl.GetTextColor())
				{
					 CFPMSApp::MyTopmostMessageBox(this, GetString(IDS_STRING2780), GetString(ST_FEHLER), MB_OK | MB_ICONERROR);
					 m_CE_DOfbl.SetInitText("");
					 m_CE_AOnbl.SetInitText("");
				}
	 
			}

		}
	}

  //Changed for the PRF 8177
  if((UINT)m_CE_AOnbl.imID == wParam)
  {
	  CString olOfbl;
	  CString olATD;
	  CString olOnbl;
	  m_CE_DOfbl.GetWindowText(olOfbl);
	  m_CE_DAirb.GetWindowText(olATD);
	  m_CE_AOnbl.GetWindowText(olOnbl);	
	  if((!olOfbl.IsEmpty() || !olATD.IsEmpty()) && olOnbl.IsEmpty())
	  {
		CFPMSApp::MyTopmostMessageBox(this, GetString(IDS_ONBL_ERRMSG), GetString(ST_FEHLER), MB_OK | MB_ICONERROR);
		m_CE_AOnbl.SetWindowText(m_AOnbl);
	  }

  }

}


if (bgUseDepBelts) {
		//	dep-belt1
	if((UINT)m_CE_DBlt1.imID == wParam)
	{
		m_CE_DBlt1.GetWindowText(olTmp);

		CString olAkt = prmDFlightSave->Blt1;

		

		if (prmDFlightSave->B1ba != TIMENULL && !olAkt.IsEmpty())
		{
			if (olTmp != olAkt)
			{
				m_CE_DBlt1.SetInitText(olAkt);
//				MessageBox( GetString(IDS_STRING_BELT_ISOPEN), GetString(ST_FEHLER), MB_OK | MB_ICONERROR);								
				CFPMSApp::MyTopmostMessageBox(this, GetString(IDS_STRING_BELT_ISOPEN), GetString(ST_FEHLER), MB_OK | MB_ICONERROR);
			}
		}
	}
}

	//	arr-belt1
	if((UINT)m_CE_ABlt1.imID == wParam)
	{
		m_CE_ABlt1.GetWindowText(olTmp);

		CString olAkt = prmAFlightSave->Blt1;

		if(olTmp != olAkt && strcmp(prmAFlight->Fbl1, "X") == 0)
		{
			CFPMSApp::MyTopmostMessageBox(this, GetString(IDS_STRING2951), GetString(IDS_WARNING), MB_OK | MB_ICONWARNING);
		}
		if (prmAFlightSave->B1ba != TIMENULL && !olAkt.IsEmpty())
		{
			if (olTmp != olAkt)
			{
				m_CE_ABlt1.SetInitText(olAkt);
//				MessageBox( GetString(IDS_STRING_BELT_ISOPEN), GetString(ST_FEHLER), MB_OK | MB_ICONERROR);								
				CFPMSApp::MyTopmostMessageBox(this, GetString(IDS_STRING_BELT_ISOPEN), GetString(ST_FEHLER), MB_OK | MB_ICONERROR);
			}
		}
		
		CheckForChangeReasons(pomBTCRCABLT1,m_ABlt1,olTmp,&(omReasons.blABlt1Resf), 'A');
		


	}
//	arr-belt2
	if((UINT)m_CE_ABlt2.imID == wParam)
	{
		m_CE_ABlt2.GetWindowText(olTmp);

		CString olAkt = prmAFlightSave->Blt2;

		if(olTmp != olAkt && strcmp(prmAFlight->Fbl2, "X") == 0)
		{
			CFPMSApp::MyTopmostMessageBox(this, GetString(IDS_STRING2951), GetString(IDS_WARNING), MB_OK | MB_ICONWARNING);
		}
		if (prmAFlightSave->B2ba != TIMENULL && !olAkt.IsEmpty())
		{
			if (olTmp != olAkt)
			{
				m_CE_ABlt2.SetInitText(olAkt);
//				MessageBox( GetString(IDS_STRING_BELT_ISOPEN), GetString(ST_FEHLER), MB_OK | MB_ICONERROR);								
				CFPMSApp::MyTopmostMessageBox(this, GetString(IDS_STRING_BELT_ISOPEN), GetString(ST_FEHLER), MB_OK | MB_ICONERROR);
			}
		}
		CheckForChangeReasons(pomBTCRCABLT2,m_ABlt2,olTmp,&(omReasons.blABlt2Resf), 'A');
	
	}
//	arr-position
	if((UINT)m_CE_APsta.imID == wParam)
	{
		m_CE_APsta.GetWindowText(olTmp);

		CString olAkt = prmAFlightSave->Psta;

		if(olTmp != olAkt && strcmp(prmAFlight->Fpsa, "X") == 0)
		{
			CFPMSApp::MyTopmostMessageBox(this, GetString(IDS_STRING2951), GetString(IDS_WARNING), MB_OK | MB_ICONWARNING);
		}

		if (prmAFlightSave->Onbl != TIMENULL && !olAkt.IsEmpty())
		{
			if (olTmp != olAkt)
			{
				
				m_CE_APsta.SetInitText(olAkt);
//				MessageBox( GetString(IDS_STRING1546), GetString(ST_FEHLER), MB_OK | MB_ICONERROR);								
				CFPMSApp::MyTopmostMessageBox(this, GetString(IDS_STRING1546), GetString(IDS_WARNING), MB_OK | MB_ICONERROR);
			}
		}
		CheckForChangeReasons(pomBTCRCFPSA,m_APsta,olTmp,&(omReasons.blPstaResf), 'A'); //PRF 8379 
		
	}
//	dep-gate1
	if((UINT)m_CE_DGtd1.imID == wParam)
	{
		m_CE_DGtd1.GetWindowText(olTmp);

		CString olGate = prmDFlightSave->Gtd1;

		if(olTmp != olGate && strcmp(prmDFlight->Fgd1, "X") == 0)
		{
			CFPMSApp::MyTopmostMessageBox(this, GetString(IDS_STRING2951), GetString(IDS_WARNING), MB_OK | MB_ICONWARNING);
		}

		if (prmDFlightSave->Gd1x != TIMENULL && !olGate.IsEmpty())
		{
			if (olTmp != olGate)
			{
				m_CE_DGtd1.SetInitText(olGate);
//				MessageBox( GetString(IDS_STRING_GATE_ISOPEN), GetString(ST_FEHLER), MB_OK | MB_ICONERROR);								
				CFPMSApp::MyTopmostMessageBox(this, GetString(IDS_STRING_GATE_ISOPEN), GetString(ST_FEHLER), MB_OK | MB_ICONERROR);
			}
		}
		CheckForChangeReasons(pomBTCRCDGTD1,m_DGtd1,olTmp,&(omReasons.blDGtd1Resf), 'D'); //PRF 8379 
		
	}
//	dep-gate2
	if((UINT)m_CE_DGtd2.imID == wParam)
	{
		m_CE_DGtd2.GetWindowText(olTmp);

		CString olGate = prmDFlightSave->Gtd2;

		if(olTmp != olGate && strcmp(prmDFlight->Fgd2, "X") == 0)
		{
			CFPMSApp::MyTopmostMessageBox(this, GetString(IDS_STRING2951), GetString(IDS_WARNING), MB_OK | MB_ICONWARNING);
		}

		if (prmDFlightSave->Gd2x != TIMENULL && !olGate.IsEmpty())
		{
			if (olTmp != olGate)
			{
				m_CE_DGtd2.SetInitText(olGate);
//				MessageBox( GetString(IDS_STRING_GATE_ISOPEN), GetString(ST_FEHLER), MB_OK | MB_ICONERROR);								
				CFPMSApp::MyTopmostMessageBox(this, GetString(IDS_STRING_GATE_ISOPEN), GetString(ST_FEHLER), MB_OK | MB_ICONERROR);
			}
		}
		CheckForChangeReasons(pomBTCRCDGTD2,m_DGtd2,olTmp,&(omReasons.blDGtd2Resf), 'D'); //PRF 8379 
		
	}
//	dep-position
	if((UINT)m_CE_DPstd.imID == wParam)
	{
		m_CE_DPstd.GetWindowText(olTmp);

		CString olAkt = prmDFlightSave->Pstd;

		if(olTmp != olAkt && strcmp(prmDFlight->Fpsd, "X") == 0)
		{
			CFPMSApp::MyTopmostMessageBox(this, GetString(IDS_STRING2951), GetString(IDS_WARNING), MB_OK | MB_ICONWARNING);
		}

		if (prmDFlightSave->Ofbl != TIMENULL && !olAkt.IsEmpty())
		{
			if (olTmp != olAkt)
			{
				m_CE_DPstd.SetInitText(olAkt);
//				MessageBox( GetString(IDS_STRING1546), GetString(ST_FEHLER), MB_OK | MB_ICONERROR);								
				CFPMSApp::MyTopmostMessageBox(this, GetString(IDS_STRING1546), GetString(ST_FEHLER), MB_OK | MB_ICONERROR);
			}
		}
		CheckForChangeReasons(pomBTCRCFPSD,m_DPstd,olTmp,&(omReasons.blPstdResf), 'D'); //PRF 8379 
		
	}
//	dep-wro
	if((UINT)m_CE_DWro1.imID == wParam)
	{
		m_CE_DWro1.GetWindowText(olTmp);

		CString olAkt = prmDFlightSave->Wro1;

		if (prmDFlightSave->W1ba != TIMENULL && !olAkt.IsEmpty())
		{
			if (olTmp != olAkt)
			{
				m_CE_DWro1.SetInitText(olAkt);
//				MessageBox( GetString(IDS_STRING_LOUNGE_ISOPEN), GetString(ST_FEHLER), MB_OK | MB_ICONERROR);								
				CFPMSApp::MyTopmostMessageBox(this, GetString(IDS_STRING_LOUNGE_ISOPEN), GetString(ST_FEHLER), MB_OK | MB_ICONERROR);
			}
		}
	}
//	dep-wro
	if((UINT)m_CE_DWro3.imID == wParam)
	{
		m_CE_DWro3.GetWindowText(olTmp);

		CString olAkt = prmDFlightSave->Wro2;

		if (prmDFlightSave->W2ba != TIMENULL && !olAkt.IsEmpty())
		{
			if (olTmp != olAkt)
			{
				m_CE_DWro3.SetInitText(olAkt);
//				MessageBox( GetString(IDS_STRING_LOUNGE_ISOPEN), GetString(ST_FEHLER), MB_OK | MB_ICONERROR);								
				CFPMSApp::MyTopmostMessageBox(this, GetString(IDS_STRING_LOUNGE_ISOPEN), GetString(ST_FEHLER), MB_OK | MB_ICONERROR);
			}
		}
	}
// end : reject if opening times




	if(!bmChanged)
	{
		m_CB_Ok.SetColors(RGB(255,0,0), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
	}
	bmChanged = true;
	return 0L;
}



LONG RotationDlg::OnTableIPEditKillfocus( UINT wParam, LPARAM lParam)
{
		
	
	CCSIPEDITNOTIFY *prlNotify = (CCSIPEDITNOTIFY*)lParam;

	if(prlNotify->Text.IsEmpty())
		return 0L;
	CString olTmp;
	CString olTmp2;


	if(((prlNotify->SourceTable == pomAJfnoTable) && (prlNotify->Column == 0)) ||
	   ((prlNotify->SourceTable == pomDJfnoTable) && (prlNotify->Column == 0)))
	{
		if(prlNotify->Text.GetLength() == 3)
		{
			prlNotify->Status = ogBCD.GetField("ALT", "ALC3", prlNotify->Text, "ALC2", olTmp );
		}
		else
		{
			prlNotify->Status = ogBCD.GetField("ALT", "ALC2", prlNotify->Text, "ALC3", olTmp );
		}
		prlNotify->UserStatus = true;
		return 0L;
	}

	if((prlNotify->SourceTable == pomDCinsTable) && (prlNotify->Column == 0))
	{
		prlNotify->UserStatus = true;
		if(prlNotify->Status = ogBCD.GetField("CIC", "CNAM", prlNotify->Text, "TERM", olTmp ))
		{
			pomDCinsTable->SetIPValue(prlNotify->Line, 0, prlNotify->Text);
			if (bgCnamAtr)
			{
				pomDCinsTable->SetIPValue(prlNotify->Line, 2, olTmp);
				ogBCD.GetField("CIC", "CNAM", prlNotify->Text, "CATR", olTmp );
				pomDCinsTable->SetIPValue(prlNotify->Line, 1, olTmp);
			}
			else
				pomDCinsTable->SetIPValue(prlNotify->Line, 1, olTmp);
		}
	}

	
	
	if((prlNotify->SourceTable == pomDCinsTable) && ( (prlNotify->Column == 7 && bgCnamAtr) || (prlNotify->Column == 6 && !bgCnamAtr) ))
	{
		prlNotify->UserStatus = true;
	
		if(bgcheckingRemarkEdit)
		{		
			prlNotify->UserStatus = true;
			return 0L;
		}
		
		prlNotify->Status = ogBCD.GetField("FID_CIC", "CODE", prlNotify->Text, "CODE", olTmp );


	}

	/*
	if(bgCnamAtr)
	pomDCinsTable->GetTextFieldValue(ilLine, 7, olText);
	else
	pomDCinsTable->GetTextFieldValue(ilLine, 6, olText);

	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "FID_CIC","CODE,"+ogFIDRemarkField, "CODE+", olText);
	polDlg->SetSecState("ROTATIONDLG_CE_DCins");
	if(polDlg->DoModal() == IDOK)
	{
		if(bgCnamAtr)
		pomDCinsTable->SetIPValue(ilLine, 7, polDlg->GetField("CODE"));
        else
		pomDCinsTable->SetIPValue(ilLine, 6, polDlg->GetField("CODE"));	



		if(prlNotify->Status = ogBCD.GetField("CIC", "CNAM", prlNotify->Text, "TERM", olTmp ))
		{
			pomDCinsTable->SetIPValue(prlNotify->Line, 0, prlNotify->Text);
			if (bgCnamAtr)
			{
				pomDCinsTable->SetIPValue(prlNotify->Line, 2, olTmp);
				ogBCD.GetField("CIC", "CNAM", prlNotify->Text, "CATR", olTmp );
				pomDCinsTable->SetIPValue(prlNotify->Line, 1, olTmp);
			}
			else
				pomDCinsTable->SetIPValue(prlNotify->Line, 1, olTmp);
		}
	}
	
	
	*/
	
	
	return 0L;
}

LONG RotationDlg::OnTableIPEdit( UINT wParam, LPARAM lParam)
{		
	CCSTABLENOTIFY *prlNotify = (CCSTABLENOTIFY*)lParam;
	//	reject a new checkin-counter if counter is already open


	if(prlNotify && (prlNotify->SourceTable == pomDCinsTable) && (prlNotify->Column == 0))
	{
		return CcaHasNoOpentime (prlNotify->Line);
	}


//rkr05042001
	// disable the cic if open (now you can�t hit with TAB)
	if(prlNotify && (prlNotify->SourceTable == pomDCinsTable))
	{
		for (int i = 0; i < omDCinsSave.GetSize(); i++)
		{
			CCADATA *prlCca = &omDCinsSave[i];

			if(prlCca && prlCca->Ckba != TIMENULL)
			{
				bool test = pomDCinsTable->SetColumnEditable(i, 0, false);
			}
			if(prlCca && prlCca->Ckba == TIMENULL)
			{
				bool test = pomDCinsTable->SetColumnEditable(i, 0, true);
			}
		}
	}
//rkr05042001


	return 0L;
}

LONG RotationDlg::CcaHasNoOpentime (int ipLineNo)
{
	if (ipLineNo >= 0 && ipLineNo < omDCinsSave.GetSize())
	{
		CCADATA *prlCca = &omDCinsSave[ipLineNo];

		if(prlCca && prlCca->Ckba != TIMENULL)
		{
//			MessageBox( GetString(IDS_STRING_CCA_ISOPEN), GetString(ST_FEHLER), MB_OK | MB_ICONERROR);								
			CFPMSApp::MyTopmostMessageBox(this, GetString(IDS_STRING_CCA_ISOPEN), GetString(ST_FEHLER), MB_OK | MB_ICONERROR);
			m_CB_Ok.SetFocus();
			return -1L;
		}
	}

	return 0L;
}

void RotationDlg::SaveToReg()
{
	BOOL ok = WriteDialogToReg((CWnd*) this, m_key);
}


BOOL RotationDlg::DestroyWindow() 
{
	SaveToReg();
	delete pomBTCRCFPSA; 
	delete pomBTCRCAGTA1;
	delete pomBTCRCAGTA2;
	delete pomBTCRCRESOA;
	delete pomBTCRCFPSD; 
	delete pomBTCRCDGTD1;
	delete pomBTCRCDGTD2;
	delete pomBTCRCRESOD;
	delete pomBTCRCABLT1;
	delete pomBTCRCABLT2;

	/*
	delete pomCBFPSA; 
	delete pomCBFGA1;
	delete pomCBFGA2;
	delete pomCBFBL1;
	delete pomCBFBL2;
	delete pomCBFPSD;
	delete pomCBFGD1;
	delete pomCBFGD2;*/

	delete pomCBAAOG;
	delete pomCBDAOG;
	delete pomCBAAHOC;
	delete pomCBDAHOC;


	BOOL blRet =  CDialog::DestroyWindow();
	if(blRet)
	{
		delete this;
		pogRotationDlg = NULL;
	}
	return blRet;
}


void RotationDlg::CheckTowingButton(void)
{

//    CCSPtrArray<ROTATIONDLGFLIGHTDATA> olRotCopy(ogRotationDlgFlights.omData);
	ogRotationDlgFlights.omData.Sort(CompareRotationFlight);
//	olRotCopy.Sort(CompareRotationFlight);

	ROTATIONDLGFLIGHTDATA *prlFlight;

	int i = 0;

	if (prmAFlight->Urno > 0)
	{
		// at first search arrival flight
		for (i = 0; i < ogRotationDlgFlights.omData.GetSize(); i++)
		{
			prlFlight = &ogRotationDlgFlights.omData[i];
			
			if (prlFlight->Urno == prmAFlight->Urno)
			{
				break; // arr found !! 
			}
		}
	}

	// then count towings to dep
	int ilCount = 0;
	for (; i < ogRotationDlgFlights.omData.GetSize(); i++)
	{
		prlFlight = &ogRotationDlgFlights.omData[i];
		
		

		//if (CString("GT").Find(prlFlight->Ftyp) >= 0)
		//	ilCount++;

		if(strcmp(prlFlight->Ftyp, "G") == 0 || strcmp(prlFlight->Ftyp, "T") == 0)
		{
			ilCount++;
		}
		

		if (prmDFlight->Urno > 0 && prlFlight->Urno == prmDFlight->Urno)
			break;
	}
		

	CString olButtonText;
	COLORREF olButtonColor;
	if (ilCount > 0)
	{
		olButtonColor = ogColors[19];
		olButtonText.Format(GetString(IDS_STRING1936), ilCount);

	}
	else
	{
		olButtonText.Format(GetString(IDS_STRING1936), 0);
		olButtonColor = ::GetSysColor(COLOR_BTNFACE);
	}

	m_CB_Towing.SetWindowText(olButtonText);
	m_CB_Towing.SetColors(olButtonColor, ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
	m_CB_Towing.UpdateWindow();
	

	//MWO
	//olRotCopy.DeleteAll();
	//END MWO
	return; 
}

void RotationDlg::CheckFlightReportButton(void)
{
	bool blHighlightArr = false;
	bool blHighlightDep = false;

//	sprintf (prmAFlight->Sreq, "x;2;3");
//	sprintf (prmDFlight->Sreq, "x;2;4");

	if (prmAFlight->Urno > 0)
	{
		if (strlen(prmAFlight->Baa1) > 0)
			blHighlightArr = true;
	}

	if (prmDFlight->Urno > 0)
	{
		if (strlen(prmDFlight->Baa1) > 0)
			blHighlightDep = true;
	}

	COLORREF olButtonColor;

	if (blHighlightArr)
		olButtonColor = ogColors[IDX_ORANGE];
	else
		olButtonColor = ::GetSysColor(COLOR_BTNFACE);

	m_CB_ARep.SetColors(olButtonColor, ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
	m_CB_ARep.UpdateWindow();

	if (blHighlightDep)
		olButtonColor = ogColors[IDX_ORANGE];
	else
		olButtonColor = ::GetSysColor(COLOR_BTNFACE);

	m_CB_DRep.SetColors(olButtonColor, ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
	m_CB_DRep.UpdateWindow();
}


void RotationDlg::CheckREQButton(void)
{
	CString olButtonText;
	COLORREF olButtonColor;

	int ilRead = 0;
	int ilNotRead = 0;
	int ilTmp = -1;

	CString olSpecialREQ;
//	sprintf (prmAFlight->Sreq, "X,2,3");
//	sprintf (prmDFlight->Sreq, "X,2,4");

	CString olSreq1;
	olSreq1.Format(",%s,%s", prmAFlight->Baa2, prmAFlight->Baa3);
	CString olSreq2;
	olSreq2.Format(",%s,%s", prmDFlight->Baa2, prmDFlight->Baa3);

	if (prmAFlight->Urno > 0 && prmDFlight->Urno > 0)
		ilTmp = GetSpecialREQ (olSreq1, olSreq2, olSpecialREQ, ilRead, ilNotRead);
	else
	{
		if (prmAFlight->Urno > 0)
			ilTmp = GetSpecialREQ (olSreq1, "", olSpecialREQ, ilRead, ilNotRead);

		if (prmDFlight->Urno > 0)
			ilTmp = GetSpecialREQ (olSreq2, "", olSpecialREQ, ilRead, ilNotRead);
	}

	if ((ilRead + ilNotRead) > 0)
	{
		if (ilNotRead > 0)
			olButtonColor = ogColors[IDX_ORANGE];
		else
			olButtonColor = ogColors[IDX_WHITE];

		olButtonText.Format(GetString(IDS_REQ), ilRead, ilNotRead);

	}
	else
	{
		olButtonText.Format(GetString(IDS_REQ), 0, 0);
		olButtonColor = ::GetSysColor(COLOR_BTNFACE);
	}

	m_CB_REQ.SetWindowText(olButtonText);
	m_CB_REQ.SetColors(olButtonColor, ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
	m_CB_REQ.UpdateWindow();
}


//*****************************************
//MWO: 13.04.05 Multi Delay codes
// Initialize the tabocx for arrival and 
// departure in oder to read the delay codes
// for the fligts. This is tab preparation
// only the read will be performed, when the
// flight data is set to the rotation dialog.
void RotationDlg::InitTabs()
{
	//Arrival	
	tabDelayArrival.ResetContent();
	tabDelayArrival.SetHeaderString("URNO,FURN,DURN,READ,REMA,DURA,CDAT,LSTU,USEC,USEU,DECS");
	tabDelayArrival.SetLogicalFieldList("URNO,FURN,DURN,READ,REMA,DURA,CDAT,LSTU,USEC,USEU,DECS");
	tabDelayArrival.SetHeaderLengthString("100,100,100,100,100,100,100,100,100,100,100");
    tabDelayArrival.SetCedaServerName(ogCommHandler.pcmRealHostName);
    tabDelayArrival.SetCedaPort("3357");
    tabDelayArrival.SetCedaHopo(pcgHome);
    tabDelayArrival.SetCedaCurrentApplication("FIPS");
    tabDelayArrival.SetCedaTabext("TAB");
    tabDelayArrival.SetCedaUser( ogCommHandler.pcmUser);
    tabDelayArrival.SetCedaWorkstation(ogCommHandler.pcmHostName);
    tabDelayArrival.SetCedaSendTimeout(3);
    tabDelayArrival.SetCedaReceiveTimeout(240);
    tabDelayArrival.SetCedaRecordSeparator("\n");
    tabDelayArrival.SetCedaIdentifier("FIPS");
    tabDelayArrival.ShowHorzScroller( TRUE );
    tabDelayArrival.EnableHeaderSizing( TRUE);
	//Departure
	tabDelayDeparture.ResetContent();
	tabDelayDeparture.SetHeaderString("URNO,FURN,DURN,READ,REMA,DURA,CDAT,LSTU,USEC,USEU,DECS");
	tabDelayDeparture.SetLogicalFieldList("URNO,FURN,DURN,READ,REMA,DURA,CDAT,LSTU,USEC,USEU,DECS");
	tabDelayDeparture.SetHeaderLengthString("100,100,100,100,100,100,100,100,100,100,100");
    tabDelayDeparture.SetCedaServerName(ogCommHandler.pcmRealHostName);
    tabDelayDeparture.SetCedaPort("3357");
    tabDelayDeparture.SetCedaHopo(pcgHome);
    tabDelayDeparture.SetCedaCurrentApplication("FIPS");
    tabDelayDeparture.SetCedaTabext("TAB");
    tabDelayDeparture.SetCedaUser( ogCommHandler.pcmUser);
    tabDelayDeparture.SetCedaWorkstation(ogCommHandler.pcmHostName);
    tabDelayDeparture.SetCedaSendTimeout(3);
    tabDelayDeparture.SetCedaReceiveTimeout(240);
    tabDelayDeparture.SetCedaRecordSeparator("\n");
    tabDelayDeparture.SetCedaIdentifier("FIPS");
    tabDelayDeparture.ShowHorzScroller( TRUE );
    tabDelayDeparture.EnableHeaderSizing( TRUE);
}

void RotationDlg::InitTables()
{
	int ili;
	CRect olRectBorder;
	m_CE_AJfnoBorder.GetWindowRect( olRectBorder );
	ScreenToClient(olRectBorder);
	olRectBorder.DeflateRect(2,2);

	pomAJfnoTable->SetHeaderSpacing(0);
	pomAJfnoTable->SetMiniTable();
	pomAJfnoTable->SetTableEditable(true);

	//rkr04042001
	pomAJfnoTable->SetIPEditModus(true);

    pomAJfnoTable->SetTableData(this, olRectBorder.left, olRectBorder.right, olRectBorder.top, olRectBorder.bottom);
	pomAJfnoTable->SetSelectMode(0);

	CCSPtrArray <TABLE_HEADER_COLUMN> omHeaderDataArray;

	pomAJfnoTable->SetShowSelection(false);
	pomAJfnoTable->ResetContent();
	
	TABLE_HEADER_COLUMN *prlHeader[9];

	prlHeader[0] = new TABLE_HEADER_COLUMN;
	prlHeader[0]->Alignment = COLALIGN_CENTER;
	prlHeader[0]->Length = 36; 
	prlHeader[0]->Font = &ogCourier_Regular_10;
	prlHeader[0]->Text = CString("");

	prlHeader[1] = new TABLE_HEADER_COLUMN;
	prlHeader[1]->Alignment = COLALIGN_CENTER;
	prlHeader[1]->Length = 46; 
	prlHeader[1]->Font = &ogCourier_Regular_10;
	prlHeader[1]->Text = CString("");

	prlHeader[2] = new TABLE_HEADER_COLUMN;
	prlHeader[2]->Alignment = COLALIGN_CENTER;
	prlHeader[2]->Length = 15; 
	prlHeader[2]->Font = &ogCourier_Regular_10;
	prlHeader[2]->Text = CString("");

	for(ili = 0; ili < 3; ili++)
	{
		omHeaderDataArray.Add(prlHeader[ili]);
	}
	pomAJfnoTable->SetHeaderFields(omHeaderDataArray);
	pomAJfnoTable->SetDefaultSeparator();
	pomAJfnoTable->SetTableEditable(true);
	omHeaderDataArray.DeleteAll();
	pomAJfnoTable->DisplayTable();



	m_CE_DJfnoBorder.GetWindowRect( olRectBorder );
	ScreenToClient(olRectBorder);
	olRectBorder.DeflateRect(2,2);

	pomDJfnoTable->SetHeaderSpacing(0);
	pomDJfnoTable->SetMiniTable();
	pomDJfnoTable->SetTableEditable(true);

	//rkr04042001
	pomDJfnoTable->SetIPEditModus(true);

	pomDJfnoTable->SetSelectMode(0);
    pomDJfnoTable->SetTableData(this, olRectBorder.left, olRectBorder.right, olRectBorder.top, olRectBorder.bottom);


	pomDJfnoTable->SetShowSelection(false);
	pomDJfnoTable->ResetContent();
	
	prlHeader[0] = new TABLE_HEADER_COLUMN;
	prlHeader[0]->Alignment = COLALIGN_CENTER;
	prlHeader[0]->Length = 36; 
	prlHeader[0]->Font = &ogCourier_Regular_10;
	prlHeader[0]->Text = CString("");

	prlHeader[1] = new TABLE_HEADER_COLUMN;
	prlHeader[1]->Alignment = COLALIGN_CENTER;
	prlHeader[1]->Length = 46; 
	prlHeader[1]->Font = &ogCourier_Regular_10;
	prlHeader[1]->Text = CString("");

	prlHeader[2] = new TABLE_HEADER_COLUMN;
	prlHeader[2]->Alignment = COLALIGN_CENTER;
	prlHeader[2]->Length = 15; 
	prlHeader[2]->Font = &ogCourier_Regular_10;
	prlHeader[2]->Text = CString("");

	for(ili = 0; ili < 3; ili++)
	{
		omHeaderDataArray.Add(prlHeader[ili]);
	}
	pomDJfnoTable->SetHeaderFields(omHeaderDataArray);
	pomDJfnoTable->SetDefaultSeparator();
	omHeaderDataArray.DeleteAll();
	pomDJfnoTable->DisplayTable();






	if (bgCnamAtr)
		m_CE_DCinsBorderExt.GetWindowRect( olRectBorder );
	else
		m_CE_DCinsBorder.GetWindowRect( olRectBorder );

	pomDCinsTable->ResetContent();
	pomDCinsTable->SetHeaderSpacing(0);
	pomDCinsTable->SetMiniTable();
	pomDCinsTable->SetTableEditable(true);

	//rkr04042001
	pomDCinsTable->SetIPEditModus(true);

	ScreenToClient(olRectBorder);
	olRectBorder.DeflateRect(2,2);
    pomDCinsTable->SetTableData(this, olRectBorder.left, olRectBorder.right, olRectBorder.top, olRectBorder.bottom);
	pomDCinsTable->SetSelectMode(0);


	pomDCinsTable->SetShowSelection(false);
	pomDCinsTable->ResetContent();
	

	int ilCol = 7;
	if (bgCnamAtr)
	{
		prlHeader[0] = new TABLE_HEADER_COLUMN;
		prlHeader[0]->Alignment = COLALIGN_CENTER;
		prlHeader[0]->Length = 40; 
		prlHeader[0]->Font = &ogCourier_Regular_10;
		prlHeader[0]->Text = CString("");

		prlHeader[1] = new TABLE_HEADER_COLUMN;
		prlHeader[1]->Alignment = COLALIGN_CENTER;
		prlHeader[1]->Length = 40; 
		prlHeader[1]->Font = &ogCourier_Regular_10;
		prlHeader[1]->Text = CString("");

		prlHeader[2] = new TABLE_HEADER_COLUMN;
		prlHeader[2]->Alignment = COLALIGN_CENTER;
		prlHeader[2]->Length = 12; 
		prlHeader[2]->Font = &ogCourier_Regular_10;
		prlHeader[2]->Text = CString("");

		prlHeader[3] = new TABLE_HEADER_COLUMN;
		prlHeader[3]->Alignment = COLALIGN_CENTER;
		prlHeader[3]->Length = 46; 
		prlHeader[3]->Font = &ogCourier_Regular_10;
		prlHeader[3]->Text = CString("");

		prlHeader[4] = new TABLE_HEADER_COLUMN;
		prlHeader[4]->Alignment = COLALIGN_CENTER;
		prlHeader[4]->Length = 46; 
		prlHeader[4]->Font = &ogCourier_Regular_10;
		prlHeader[4]->Text = CString("");


		prlHeader[5] = new TABLE_HEADER_COLUMN;
		prlHeader[5]->Alignment = COLALIGN_CENTER;
		prlHeader[5]->Length = 46; 
		prlHeader[5]->Font = &ogCourier_Regular_10;
		prlHeader[5]->Text = CString("");

		prlHeader[6] = new TABLE_HEADER_COLUMN;
		prlHeader[6]->Alignment = COLALIGN_CENTER;
		prlHeader[6]->Length = 46; 
		prlHeader[6]->Font = &ogCourier_Regular_10;
		prlHeader[6]->Text = CString("");

		prlHeader[7] = new TABLE_HEADER_COLUMN;
		prlHeader[7]->Alignment = COLALIGN_CENTER;
		prlHeader[7]->Length = 70; 
		prlHeader[7]->Font = &ogCourier_Regular_10;
		prlHeader[7]->Text = CString("");

		ilCol = 8;

		if(bgFIDSLogo)
		{
			prlHeader[8] = new TABLE_HEADER_COLUMN;
			prlHeader[8]->Alignment = COLALIGN_CENTER;
			prlHeader[8]->Length = 70; 
			prlHeader[8]->Font = &ogCourier_Regular_10;
			prlHeader[8]->Text = CString("");
			ilCol = 9;
		}
	}
	else
	{
		prlHeader[0] = new TABLE_HEADER_COLUMN;
		prlHeader[0]->Alignment = COLALIGN_CENTER;
		prlHeader[0]->Length = 40; 
		prlHeader[0]->Font = &ogCourier_Regular_10;
		prlHeader[0]->Text = CString("");

		prlHeader[1] = new TABLE_HEADER_COLUMN;
		prlHeader[1]->Alignment = COLALIGN_CENTER;
		prlHeader[1]->Length = 12; 
		prlHeader[1]->Font = &ogCourier_Regular_10;
		prlHeader[1]->Text = CString("");

		prlHeader[2] = new TABLE_HEADER_COLUMN;
		prlHeader[2]->Alignment = COLALIGN_CENTER;
		prlHeader[2]->Length = 46; 
		prlHeader[2]->Font = &ogCourier_Regular_10;
		prlHeader[2]->Text = CString("");

		prlHeader[3] = new TABLE_HEADER_COLUMN;
		prlHeader[3]->Alignment = COLALIGN_CENTER;
		prlHeader[3]->Length = 46; 
		prlHeader[3]->Font = &ogCourier_Regular_10;
		prlHeader[3]->Text = CString("");


		prlHeader[4] = new TABLE_HEADER_COLUMN;
		prlHeader[4]->Alignment = COLALIGN_CENTER;
		prlHeader[4]->Length = 46; 
		prlHeader[4]->Font = &ogCourier_Regular_10;
		prlHeader[4]->Text = CString("");

		prlHeader[5] = new TABLE_HEADER_COLUMN;
		prlHeader[5]->Alignment = COLALIGN_CENTER;
		prlHeader[5]->Length = 46; 
		prlHeader[5]->Font = &ogCourier_Regular_10;
		prlHeader[5]->Text = CString("");

		prlHeader[6] = new TABLE_HEADER_COLUMN;
		prlHeader[6]->Alignment = COLALIGN_CENTER;
		prlHeader[6]->Length = 70; 
		prlHeader[6]->Font = &ogCourier_Regular_10;
		prlHeader[6]->Text = CString("");

		if(bgFIDSLogo)
		{
			prlHeader[7] = new TABLE_HEADER_COLUMN;
			prlHeader[7]->Alignment = COLALIGN_CENTER;
			prlHeader[7]->Length = 70; 
			prlHeader[7]->Font = &ogCourier_Regular_10;
			prlHeader[7]->Text = CString("");
			ilCol = 8;
		}

	}


	for(ili = 0; ili < ilCol; ili++)
	{
		omHeaderDataArray.Add(prlHeader[ili]);
	}
	pomDCinsTable->SetHeaderFields(omHeaderDataArray);
	pomDCinsTable->SetDefaultSeparator();
	omHeaderDataArray.DeleteAll();
	pomDCinsTable->DisplayTable();


	/////////////////////////////////////////////////



	m_CE_DChuBorder.GetWindowRect( olRectBorder );

	pomDChuTable->ResetContent();
	pomDChuTable->SetHeaderSpacing(0);
	pomDChuTable->SetMiniTable();
	pomDChuTable->SetTableEditable(true);

	//rkr04042001
	pomDChuTable->SetIPEditModus(false);

	ScreenToClient(olRectBorder);
	olRectBorder.DeflateRect(2,2);
    pomDChuTable->SetTableData(this, olRectBorder.left, olRectBorder.right, olRectBorder.top, olRectBorder.bottom);
	pomDChuTable->SetSelectMode(0);


	pomDChuTable->SetShowSelection(false);
	pomDChuTable->ResetContent();
	

	ilCol = 5;
		prlHeader[0] = new TABLE_HEADER_COLUMN;
		prlHeader[0]->Alignment = COLALIGN_CENTER;
		prlHeader[0]->Length = 40; 
		prlHeader[0]->Font = &ogCourier_Regular_10;
		prlHeader[0]->Text = CString("");

		prlHeader[1] = new TABLE_HEADER_COLUMN;
		prlHeader[1]->Alignment = COLALIGN_CENTER;
		prlHeader[1]->Length = 46; 
		prlHeader[1]->Font = &ogCourier_Regular_10;
		prlHeader[1]->Text = CString("");

		prlHeader[2] = new TABLE_HEADER_COLUMN;
		prlHeader[2]->Alignment = COLALIGN_CENTER;
		prlHeader[2]->Length = 46; 
		prlHeader[2]->Font = &ogCourier_Regular_10;
		prlHeader[2]->Text = CString("");

		prlHeader[3] = new TABLE_HEADER_COLUMN;
		prlHeader[3]->Alignment = COLALIGN_CENTER;
		prlHeader[3]->Length = 46; 
		prlHeader[3]->Font = &ogCourier_Regular_10;
		prlHeader[3]->Text = CString("");


		prlHeader[4] = new TABLE_HEADER_COLUMN;
		prlHeader[4]->Alignment = COLALIGN_CENTER;
		prlHeader[4]->Length = 46; 
		prlHeader[4]->Font = &ogCourier_Regular_10;
		prlHeader[4]->Text = CString("");




	for(ili = 0; ili < ilCol; ili++)
	{
		omHeaderDataArray.Add(prlHeader[ili]);
	}
	pomDChuTable->SetHeaderFields(omHeaderDataArray);
	pomDChuTable->SetDefaultSeparator();
	omHeaderDataArray.DeleteAll();
	pomDChuTable->DisplayTable();








}







/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
// CHECKALL


bool RotationDlg::CheckAll2(CString &opGMess, CString &opAMess, CString &opDMess) 
{
	if((prmAFlight->Urno == 0) && (prmDFlight->Urno == 0))
	{
		return true;
	}


	if (prmAFlight->Urno != 0)
	{
		CTime olAOfbl;
		CTime olAAirb;

		olAAirb = HourStringToDate(m_AAirb, prmAFlight->Stoa);
		olAOfbl = HourStringToDate(m_AOfbl, prmAFlight->Stoa);

		if((olAOfbl != TIMENULL) && (olAAirb != TIMENULL))
		{
			if( strcmp(prmAFlight->Stof, "U") == 0 && strcmp(prmAFlight->Stab, "U") == 0 )
			{
				//then flight will not correct the ofbl
				if(olAOfbl > olAAirb) 
					opAMess += GetString(IDS_STRING386) + CString("\n");//"Ofblock nach Start!\n";
			}
		}


		if((olAOfbl == TIMENULL) && (olAAirb != TIMENULL))
		{
			// flight setzt ofbl
			//opAMess += GetString(IDS_STRING387) + CString("\n");//"kein Ofblock vor Start!\n";
		}



		CTime SchedATifd = -1;
		CTime SchedATifa = -1;
		ArrivalSchedTifd(SchedATifd);
		ArrivalSchedTifa(SchedATifa);

		CTime AktATifd = -1;
		CTime AktATifa = -1;
		ArrivalAktTifd(AktATifd);
		ArrivalAktTifa(AktATifa);

		if (bmLocalTime && bgRealLocal)
		{
/*
			CedaAptLocalUtc::AptLocalToUtc (SchedATifa, "");
			CedaAptLocalUtc::AptLocalToUtc (AktATifa, "");
			CedaAptLocalUtc::AptLocalToUtc (SchedATifd, CString(pcmAOrg4));
			CedaAptLocalUtc::AptLocalToUtc (AktATifd, CString(pcmAOrg4));
*/
		}

		// check if Tifd at origin is smaller than Tifa at Home
		if((AktATifa != TIMENULL) && (AktATifd != TIMENULL))
		{
			if(AktATifa < AktATifd)
				opAMess += GetString(IDS_STRING2535) + CString("\n");//Actual Time for Arrival at Home before Actual Time of Departure at Origin
		}
		else
		{
			if((SchedATifa != TIMENULL) && (SchedATifd != TIMENULL))
			{
				if(SchedATifa < SchedATifd)
					opAMess += GetString(IDS_STRING2532) + CString("\n");//Planning Time for Arrival at Home before Planning Time of Departure at Origin
			}
		}

	}

	if (prmDFlight->Urno != 0)
	{

		CTime SchedDTifd = -1;
		CTime SchedDTifa = -1;
		DepartureSchedTifd(SchedDTifd);
		DepartureSchedTifa(SchedDTifa);

		CTime AktDTifd = -1;
		CTime AktDTifa = -1;
		DepartureAktTifd(AktDTifd);
		DepartureAktTifa(AktDTifa);

		if (bmLocalTime && bgRealLocal)
		{
/*
			CedaAptLocalUtc::AptLocalToUtc (SchedDTifd, "");
			CedaAptLocalUtc::AptLocalToUtc (AktDTifd, "");
			CedaAptLocalUtc::AptLocalToUtc (SchedDTifa, CString(pcmDDes4));
			CedaAptLocalUtc::AptLocalToUtc (AktDTifa, CString(pcmDDes4));
*/
		}

		// check if Tifd at origin is smaller than Tifa at Home
		if((AktDTifa != TIMENULL) && (AktDTifd != TIMENULL))
		{
			if(AktDTifa < AktDTifd)
				opAMess += GetString(IDS_STRING2537) + CString("\n");//Actual Time for Departure at Home after Planning Time of Arrival at Destination
		}
		else
		{
			if((SchedDTifa != TIMENULL) && (SchedDTifd != TIMENULL))
			{
				if(SchedDTifa < SchedDTifd)
					opAMess += GetString(IDS_STRING2533) + CString("\n");//Planning Time for Departure at Home after Planning Time of Arrival at Destination
			}
		}
	}

	if (prmDFlight->Urno != 0 && prmAFlight->Urno != 0)
	{
	//	check for tifa/tifd
		CTime ATifa = -1;
		CTime DTifd = -1;

		ArrivalSchedTifa(ATifa);
		DepartureSchedTifd(DTifd);

		if((ATifa != TIMENULL) && (DTifd != TIMENULL))
		{
			if(ATifa > DTifd)
				opGMess += GetString(IDS_STRING2534) + CString("\n");//Planning Time for Arrival at Home after Planning Time of Departure at Home
		}
	}

	if(opGMess.IsEmpty() && opAMess.IsEmpty() && opDMess.IsEmpty())
		return true;
	else
		return false;

}


bool RotationDlg::CheckAll(CString &opGMess, CString &opAMess, CString &opDMess) 
{
	if((prmAFlight->Urno == 0) && (prmDFlight->Urno == 0))
	{
		return true;
	}

	m_CE_Regn.GetWindowText(m_Regn); 
	m_Regn.Remove('\n');
	m_Regn.TrimLeft();
	m_Regn.TrimRight();
	m_CE_Regn.SetWindowText(m_Regn);


	if(m_AStoa.IsEmpty())
	{
		bmArrival = false;
		if (prmAFlight->Urno != 0)
			opAMess += GetString(IDS_STRING350) + CString("\n");//"Planm��ige Ankunftszeit!\n";
	}
	else
	{
		bmArrival = true;
	}

	if(m_DStod.IsEmpty())
	{
		bmDeparture = false;
		if (prmDFlight->Urno != 0)
			opDMess += GetString(IDS_STRING384) + CString("\n");//"Planm��ige Abflugszeit!\n";
	}
	else
	{
		bmDeparture = true;
	}


	if(!bmArrival && !bmDeparture)
	{
		return false;
	}


	bool blRet = true;


	CString olBuffer;


	CTime olAOnbl;
	CTime olAOfbl;
	CTime olAAirb;
	CTime olALand;

	olALand = HourStringToDate(m_ALand, prmAFlight->Stoa);;

	olAAirb = HourStringToDate(m_AAirb, prmAFlight->Stoa);

	olAOfbl = HourStringToDate(m_AOfbl, prmAFlight->Stoa);

	olAOnbl = HourStringToDate(m_AOnbl, prmAFlight->Stoa);


	CTime olDOnbl;
	CTime olDOfbl; 
	CTime olDAirb;
	CTime olDLand;


	olDAirb = HourStringToDate(m_DAirb, prmDFlight->Stod);

	olDOfbl = HourStringToDate(m_DOfbl, prmDFlight->Stod);


	olDOnbl = HourStringToDate(prmDFlight->Onbl.Format("%H:%M"), prmDFlight->Stod);

	olDLand = HourStringToDate(prmDFlight->Land.Format("%H:%M"), prmDFlight->Stod);



	if(!m_CE_Regn.GetStatus() && !m_Regn.IsEmpty())
		opGMess += GetString(IDS_STRING341) + CString("\n");//"LFZ-Kennzeichen"; 

/*	CString olBuffer2;
	m_CE_Act3.GetWindowText(olBuffer);
	m_CE_Act5.GetWindowText(olBuffer2);
	if(!m_CE_Act3.GetStatus() || !m_CE_Act5.GetStatus() || olBuffer.IsEmpty() || olBuffer2.IsEmpty())
		opGMess += GetString(IDS_STRING342) + CString("\n");//"A/C Typ\n"; 
*/		
	//act
	bool blIsIn = true;
	if (m_Act5.IsEmpty() && m_Act3.IsEmpty())
		blIsIn = false;

	CString olWhere;
	if (m_Act3.IsEmpty())
		m_Act3 = "";
	if (m_Act5.IsEmpty())
		m_Act5 = " ";

	CCSPtrArray<RecordSet> prlRecords;
	int ilCount = ogBCD.GetRecordsExt("ACT","ACT3","ACT5",m_Act3,m_Act5, &prlRecords);
	prlRecords.DeleteAll();

	if(!m_CE_Act3.GetStatus() || !m_CE_Act5.GetStatus() || !blIsIn || ilCount == 0)
		opGMess += GetString(IDS_STRING342);//"A/C Typ\n";  

	//ming
	if(!m_CE_Ming.GetStatus())
		opGMess += GetString(IDS_STRING343) + CString("\n");//"Min.G/T \n"; 


//	check for tifa/tifd
	CTime ATifa = -1;
	CTime DTifd = -1;

	ArrivalAktTifa(ATifa);
	DepartureAktTifd(DTifd);

	if(bmArrival && bmDeparture)
	{
		if((ATifa != TIMENULL) && (DTifd != TIMENULL))
		{
			if(ATifa > DTifd)
				opGMess += GetString(IDS_STRING2536) + CString("\n");//Actual Time for  Arrival at Home before Actual Time of Departure at Home
		}
	}

	if((olAOnbl != TIMENULL) && (olDOfbl != TIMENULL))
	{
		if(olAOnbl > olDOfbl) 
			opGMess += GetString(IDS_STRING345) + CString("\n");//"Offblock vor Onblock!\n";

	}


	if((olALand != TIMENULL) && (olDAirb != TIMENULL))
	{
		if(olALand > olDAirb) 
			opGMess += GetString(IDS_STRING346) + CString("\n");//"Start ist vor Landung!\n";
	}




	if(bmArrival)
	{

		CString olAlc;
		CString olFltn;
		CString olFlns;
		CString olCsgn;
		m_CE_AAlc3.GetWindowText(olAlc);
		m_CE_AFltn.GetWindowText(olFltn);
		m_CE_AFlns.GetWindowText(olFlns);
		m_CE_ACsgn.GetWindowText(olCsgn);
		CString olAFlno = CreateFlno(olAlc, olFltn, olFlns); 
		if (olAFlno.IsEmpty() && olCsgn.IsEmpty())
		{
			//"Flightnumber or Callsign must be filled!\n"
 			opAMess += GetString(IDS_FLNO_OR_CSGN) + CString("\n");
		}

/*		CString olFlno(prmAFlight->Flno); 

		if(!(m_AAlc3.IsEmpty() && m_AFltn.IsEmpty() && m_AFlns.IsEmpty()) && olFlno.IsEmpty())
			opAMess += GetString(IDS_STRING347) + CString("\n");//"Flugnummer\n"; 
*/
//		if (m_CE_AFlti.IsChanged() && 
//			(!m_CE_AFlti.GetStatus() || (m_AFlti != "I" && m_AFlti != "D" && !m_AFlti.IsEmpty())))
		if ( m_CE_AFlti.IsChanged() && !m_CE_AFlti.GetStatus() )
			opAMess += GetString(IDS_STRING1992) + CString("\n");//"Flight ID\n";

		// weitere Flugnummern

		bool bl0;
		bool bl1;
		bool bl2;
		CString  olAlc3;

		int ilLines = pomAJfnoTable->GetLinesCount();
		for(int i = 0; i < ilLines; i++)
		{
			pomAJfnoTable->GetTextFieldValue(i, 0, olAlc3);
			pomAJfnoTable->GetTextFieldValue(i, 1, olFltn);
			pomAJfnoTable->GetTextFieldValue(i, 2, olFlns);
			if(!olAlc3.IsEmpty() && !olFltn.IsEmpty()/* && !olFlns.IsEmpty()*/)
			{
				bl0 = pomAJfnoTable->GetCellStatus(i, 0); 
				bl1 = pomAJfnoTable->GetCellStatus(i, 1); 
				bl2 = pomAJfnoTable->GetCellStatus(i, 2); 
				if((!bl0) || (!bl1) || (!bl2))
				{
					olBuffer.Format(GetString(IDS_STRING348),i+1);
					opAMess += olBuffer; 
				}
			}
		}



		if(!m_CE_AOrg3.GetStatus())
			opAMess += GetString(IDS_STRING349) + CString("\n");//"Ausgangsflughafencode!\n"; 

		CString olText4;
		m_CE_AOrg3.GetWindowText(olText4);
		if (olText4.IsEmpty())
			opAMess += GetString(IDS_STRING349) + CString("\n");//"Ausgangsflughafencode!\n"; 

		if(!m_CE_AStoa.GetStatus())
			opAMess += GetString(IDS_STRING384) + CString("\n");//"Planm��ige Ankunftszeit!\n";
		
		if(!m_CE_AStod.GetStatus())
			opAMess += GetString(IDS_STRING351) + CString("\n");//"Planm��ige Abflugszeit!\n";
		
		if(!m_CE_AEtai.GetStatus())
			opAMess += GetString(IDS_STRING352) + CString("\n");//"Ertwartete Ankunftszeit!\n";

		if(!m_CE_AEtdi.GetStatus())
			opAMess += GetString(IDS_STRING385) + CString("\n");//"Ertwartete Abflugsszeit!\n";

		if(!m_CE_ANfes.GetStatus())
			opAMess += GetString(IDS_STRING2868) + CString("\n");


		if((olAOnbl != TIMENULL) && (olALand != TIMENULL))
		{
			if(olAOnbl < olALand) 
				opAMess += GetString(IDS_STRING353) + CString("\n");//"Onblock vor Landung!\n";

		}

		if((olAOnbl != TIMENULL) && (olALand == TIMENULL))
		{
				opAMess += GetString(IDS_STRING354) + CString("\n");//"Onblock ohne Landung!\n";

		}
		
/*
		if((olAOfbl != TIMENULL) && (olAAirb != TIMENULL))
		{
			if(olAOfbl > olAAirb) 
				opAMess += GetString(IDS_STRING386) + CString("\n");//"Ofblock nach Start!\n";
		}


		if((olAOfbl == TIMENULL) && (olAAirb != TIMENULL))
		{
			opAMess += GetString(IDS_STRING387) + CString("\n");//"kein Ofblock vor Start!\n";
		}
*/


		// Vias
		if (polRotationAViaDlg)
			opAMess += polRotationAViaDlg->GetStatus();

//		opAMess += pomAViaCtrl->GetStatus();

		if(!m_CE_ADivr.GetStatus())
			opAMess += GetString(IDS_STRING2708) + CString("\n");//"Code Diverted\n";


		if(!m_CE_ATtyp.GetStatus())
			opAMess += GetString(IDS_STRING360) + CString("\n");//"Verkehrsart\n";

		if(!m_CE_AStyp.GetStatus())
			opAMess += GetString(IDS_STRING2396) + CString("\n");//"Service\n";

		if(!m_CE_AHtyp.GetStatus())
			opAMess += GetString(IDS_STRING2666) + CString("\n");//"Agent\n";

		if(!m_CE_APsta.GetStatus())
			opAMess += GetString(IDS_STRING361) + CString("\n");//"Postionsbezeichnung\n";

		//if(!m_CE_APaba.GetStatus())
		//	opAMess += GetString(IDS_STRING362) + CString("\n");//"Beginn der Postionsbelegung\n";

		//if(!m_CE_APaea.GetStatus())
		//	opAMess += GetString(IDS_STRING363) + CString("\n");//"Ende der Postionsbelegung\n";

		if((prmAFlight->Paba != TIMENULL) && (prmAFlight->Paea != TIMENULL))
		{
			if(prmAFlight->Paea <= prmAFlight->Paba)
				opAMess += GetString(IDS_STRING364) + CString("\n");//"Ende der Postionsbelegung ist vor Beginn\n";
		}


		if(!m_CE_AGta1.GetStatus())
			opAMess += GetString(IDS_STRING365) + CString("\n");//"Gatebezeichnung 1\n";

		if(!m_CE_ARwya.GetStatus())
			opAMess += GetString(IDS_STRING2450) + CString("\n");//"Rwy-bezeichnung \n";

		if(!m_CE_AGa1x.GetStatus())
			opAMess += GetString(IDS_STRING366) + CString("\n");//"Beginn der Gatebelegung 1\n";

		if(!m_CE_AGa1y.GetStatus())
			opAMess += GetString(IDS_STRING367) + CString("\n");//"Ende der Gatebelegung 1\n";

		if((prmAFlight->Ga1x != TIMENULL) && (prmAFlight->Ga1y != TIMENULL))
		{
			if(prmAFlight->Ga1x > prmAFlight->Ga1y)
				opAMess += GetString(IDS_STRING368) + CString("\n");//"Ende der Gatebelegung 1 ist vor Beginn\n";
		}

		if(!m_CE_AGta2.GetStatus())
			opAMess += GetString(IDS_STRING369) + CString("\n");//"Gatebezeichnung 2\n";

		if(!m_CE_AGa2x.GetStatus())
			opAMess += GetString(IDS_STRING370) + CString("\n");//"Beginn der Gatebelegung 2\n";

		if(!m_CE_AGa2y.GetStatus())
			opAMess += GetString(IDS_STRING371) + CString("\n");//"Ende der Gatebelegung 2\n";

		if((prmAFlight->Ga2x != TIMENULL) && (prmAFlight->Ga1y != TIMENULL))
		{
			if(prmAFlight->Ga2x > prmAFlight->Ga2y && prmAFlight->Ga2y != TIMENULL)
				opAMess += GetString(IDS_STRING372) + CString("\n");//"Ende der Gatebelegung 2 ist vor Beginn\n";
		}

		if(!m_CE_ABlt1.GetStatus())
			opAMess += GetString(IDS_STRING373) + CString("\n");//"Bezeichnung des Gep�ckbandes 1\n";

		if(!m_CE_AB1ba.GetStatus())
			opAMess += GetString(IDS_STRING374) + CString("\n");//"Beginn der Belegung des Gep�ckbandes 1\n";

		if(!m_CE_AB1ea.GetStatus())
			opAMess += GetString(IDS_STRING375) + CString("\n");//"Ende der Belegung des Gep�ckbandes 1\n";

		if((prmAFlight->B1ba != TIMENULL) && (prmAFlight->B1ea != TIMENULL))
		{
			if(prmAFlight->B1ba > prmAFlight->B1ea)
				opAMess += GetString(IDS_STRING376) + CString("\n");//"Ende der Belegung des Gep�ckbandes 1 ist vor Beginn\n";
		}

		if(!m_CE_ABlt2.GetStatus())
			opAMess += GetString(IDS_STRING377) + CString("\n");//"Bezeichnung des Gep�ckbandes 2\n";

		if(!m_CE_AB2ba.GetStatus())
			opAMess += GetString(IDS_STRING378) + CString("\n");//"Beginn der Belegung des Gep�ckbandes 2\n";

		if(!m_CE_AB2ea.GetStatus())
			opAMess += GetString(IDS_STRING379) + CString("\n");//"Ende der Belegung des Gep�ckbandes 2\n";

		if((prmAFlight->B2ba != TIMENULL) && (prmAFlight->B2ea != TIMENULL))
		{
			if(prmAFlight->B2ba > prmAFlight->B2ea)
				opAMess += GetString(IDS_STRING380) + CString("\n");//"Ende der Belegung des Gep�ckbandes 2 ist vor Beginn\n";
		}


		if(!m_CE_AExt1.GetStatus())
			opAMess += GetString(IDS_STRING381) + CString("\n");//"Bezeichnung des Ausganges 1\n";

		if(!m_CE_AExt2.GetStatus())
			opAMess += GetString(IDS_STRING382) + CString("\n");//"Bezeichnung des Ausganges 2\n";

		//delay
		if(!m_CE_ADcd1.GetStatus())
			opAMess += GetString(IDS_STRING2570) + CString("\n");//"delay1
		if(!m_CE_ADcd2.GetStatus())
			opAMess += GetString(IDS_STRING2571) + CString("\n");//"delay2


	
		if(bgShowPayDetail && pomCBAAHOC->GetCheck() == TRUE)
		{
			//if(strcmp(prmAFlight->Mopa,"")==0 && m_CB_AAdhoc.GetCheck()==TRUE)
			if(strcmp(prmAFlight->Mopa,"")==0)
			{
				opAMess += "Payment Details" + CString("\n");
			}
		}			
			
	}


	//////////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////////
	if(bmDeparture)
	{
		CString olAlc;
		CString olFltn;
		CString olFlns;
		CString olCsgn;
		m_CE_DAlc3.GetWindowText(olAlc);
		m_CE_DFltn.GetWindowText(olFltn);
		m_CE_DFlns.GetWindowText(olFlns);
		m_CE_DCsgn.GetWindowText(olCsgn);
		CString olDFlno = CreateFlno(olAlc, olFltn, olFlns); 
		if (olDFlno.IsEmpty() && olCsgn.IsEmpty())
		{
			//"Flightnumber or Callsign must be filled!\n"
 			opDMess += GetString(IDS_FLNO_OR_CSGN) + CString("\n");
		}
/*
		CString olFlno(prmDFlight->Flno); 

		if(!(m_DAlc3.IsEmpty() && m_DFltn.IsEmpty() && m_DFlns.IsEmpty()) && olFlno.IsEmpty())
			opDMess += GetString(IDS_STRING347) + CString("\n");//"Flugnummer\n"; 
*/
//		if (m_CE_DFlti.IsChanged() &&
//			(!m_CE_DFlti.GetStatus() || (m_DFlti != "I" && m_DFlti !="D" && !m_DFlti.IsEmpty())))
		if ( m_CE_DFlti.IsChanged() && !m_CE_DFlti.GetStatus() )
 			opDMess += GetString(IDS_STRING1992) + CString("\n");//"Flight ID\n";

		
		// weitere Flugnummern
		CString  olAlc3;

		int ilLines = pomDJfnoTable->GetLinesCount();
		for(int i = 0; i < ilLines; i++)
		{
			pomDJfnoTable->GetTextFieldValue(i, 0, olAlc3);
			pomDJfnoTable->GetTextFieldValue(i, 1, olFltn);
			pomDJfnoTable->GetTextFieldValue(i, 2, olFlns);
			if(!olAlc3.IsEmpty() && !olFltn.IsEmpty()/* && !olFlns.IsEmpty()*/)
			{
				if((!pomDJfnoTable->GetCellStatus(i, 0)) || (!pomDJfnoTable->GetCellStatus(i, 1)) || (!pomDJfnoTable->GetCellStatus(i, 2)))
				{
					olBuffer.Format(GetString(IDS_STRING348), i+1);//"Weitere Flugnummer in Zeile %d\n"
					opDMess += olBuffer; 
				}
			}
		}



//		if(!m_CE_DDes3.GetStatus())
//		if(!m_CE_DDes3.GetStatus() && !m_CE_DDes4.GetStatus())
//			opDMess += GetString(IDS_STRING383) + CString("\n");//"Bestimmungsflughafencode!\n"; 

		CString olTextD4;
		m_CE_DDes4.GetWindowText(olTextD4);
		if (olTextD4.IsEmpty() || !m_CE_DDes4.GetStatus())
			opDMess += GetString(IDS_STRING383) + CString("\n");//"Bestimmungsflughafencode!\n"; 


		if(!m_CE_DStod.GetStatus())
			opDMess += GetString(IDS_STRING384) + CString("\n");//"Planm��ige Abflugszeit!\n";
		
		if(!m_CE_DStoa.GetStatus())
			opDMess += GetString(IDS_STRING350) + CString("\n");//"Planm��ige Ankunftszeit!\n";

		if(!m_CE_DEtdi.GetStatus())
			opDMess += GetString(IDS_STRING385) + CString("\n");//"Ertwartete Abflugsszeit!\n";


		if((olDOfbl != TIMENULL) && (olDAirb != TIMENULL))
		{
			if(olDOfbl > olDAirb) 
				opDMess += GetString(IDS_STRING386) + CString("\n");//"Ofblock nach Start!\n";

		}


		if((olDOfbl == TIMENULL) && (olDAirb != TIMENULL))
		{
			opAMess += GetString(IDS_STRING387) + CString("\n");//"kein Ofblock vor Start!\n";
		}


		
		// Vias

		if (polRotationDViaDlg)
			opAMess += polRotationDViaDlg->GetStatus();
//		opDMess += pomDViaCtrl->GetStatus();



		if(!m_CE_DTtyp.GetStatus())
			opDMess += GetString(IDS_STRING360) + CString("\n");//"Verkehrsart\n";

		if(!m_CE_DStyp.GetStatus())
			opDMess += GetString(IDS_STRING2396) + CString("\n");//"Service\n";

		if(!m_CE_DHtyp.GetStatus())
			opDMess += GetString(IDS_STRING2666) + CString("\n");//"Agent\n";


		if(!m_CE_DPstd.GetStatus())
			opDMess += GetString(IDS_STRING361) + CString("\n");//"Postionsbezeichnung\n";

		//if(!m_CE_DPdba.GetStatus())
		//	opDMess += GetString(IDS_STRING362) + CString("\n");//"Beginn der Postionsbelegung\n";

		//if(!m_CE_DPdea.GetStatus())
		//	opDMess += GetString(IDS_STRING363) + CString("\n");//"Ende der Postionsbelegung\n";

		if((prmDFlight->Pdea != TIMENULL) && (prmDFlight->Pdba != TIMENULL))
		{
			if(prmDFlight->Pdba >= prmDFlight->Pdea)
				opDMess += GetString(IDS_STRING364) + CString("\n");//"Ende der Postionsbelegung ist vor Beginn\n";
		}

		if(!m_CE_DRwyd.GetStatus())
			opDMess += GetString(IDS_STRING2450) + CString("\n");//"Rwy-bezeichnung \n";

		if(!m_CE_DGtd1.GetStatus())
			opDMess += GetString(IDS_STRING365) + CString("\n");//"Gatebezeichnung 1\n";

		if(!m_CE_DGd1x.GetStatus())
			opDMess += GetString(IDS_STRING366) + CString("\n");//"Beginn der Gatebelegung 1\n";

		if(!m_CE_DGd1y.GetStatus())
			opDMess += GetString(IDS_STRING367) + CString("\n");//"Ende der Gatebelegung 1\n";

		if((prmDFlight->Gd1x != TIMENULL) && (prmDFlight->Gd1y != TIMENULL))
		{
			if(prmDFlight->Gd1y < prmDFlight->Gd1x)
				opDMess += GetString(IDS_STRING368) + CString("\n");//"Ende der Gatebelegung 1 ist vor Beginn\n";
		}

		if(!m_CE_DGtd2.GetStatus())
			opDMess += GetString(IDS_STRING369) + CString("\n");//"Gatebezeichnung 2\n";

		if(!m_CE_DGd2x.GetStatus())
			opDMess += GetString(IDS_STRING370) + CString("\n");//"Beginn der Gatebelegung 2\n";

		if(!m_CE_DGd2y.GetStatus())
			opDMess += GetString(IDS_STRING371) + CString("\n");//"Ende der Gatebelegung 2\n";

		/*
		if((prmDFlight->Gd2x != TIMENULL) && (prmDFlight->Gd1y != TIMENULL))
		{
			if(prmDFlight->Gd2x > prmDFlight->Gd2y)
				opDMess += GetString(IDS_STRING372) + CString("\n");//"Ende der Gatebelegung 2 ist vor Beginn\n";
		}
		*/

		// check carousel times
		if(!m_CE_DBao1.GetStatus())
			opDMess += GetString(IDS_STRING1988) + CString("\n"); 
		if(!m_CE_DBac1.GetStatus())
			opDMess += GetString(IDS_STRING1989) + CString("\n"); 
		if(!m_CE_DBao4.GetStatus())
			opDMess += GetString(IDS_STRING1990) + CString("\n"); 
		if(!m_CE_DBac4.GetStatus())
			opDMess += GetString(IDS_STRING1991) + CString("\n"); 


		if(!m_CE_DWro1.GetStatus())
			opDMess += GetString(IDS_STRING388) + CString("\n");//"Bezeichnung des Warteraumes\n";

		if(!m_CE_DWro3.GetStatus())
			opDMess += GetString(IDS_STRING388) + CString("\n");//"Bezeichnung des Warteraumes\n";

		if(!m_CE_DW1ba.GetStatus())
			opDMess += GetString(IDS_STRING389) + CString("\n");//"Beginn der Belegung des Warteraumes 1\n";

		if(!m_CE_DW3ba.GetStatus())
			opDMess += GetString(IDS_STRING389) + CString("\n");//"Beginn der Belegung des Warteraumes 1\n";

		if(!m_CE_DW1ea.GetStatus())
			opDMess += GetString(IDS_STRING390) + CString("\n");//"Ende der Belegung des Warteraumes \n";

		if(!m_CE_DW3ea.GetStatus())
			opDMess += GetString(IDS_STRING390) + CString("\n");//"Ende der Belegung des Warteraumes \n";

		if((prmDFlight->W1ba != TIMENULL) && (prmDFlight->W1ea != TIMENULL))
		{
			if(prmDFlight->W1ea < prmDFlight->W1ba)
				opDMess += GetString(IDS_STRING391) + CString("\n");//"Ende der Belegung des Warteraumes ist vor Beginn\n";
		}

		if(!m_CE_DNfes.GetStatus())
			opDMess += GetString(IDS_STRING2869) + CString("\n");


		//delay
		if(!m_CE_DDcd1.GetStatus())
			opDMess += GetString(IDS_STRING2570) + CString("\n");//"delay1
		if(!m_CE_DDcd2.GetStatus())
			opDMess += GetString(IDS_STRING2571) + CString("\n");//"delay2

		if (bgUseDepBelts) {

				if(!m_CE_DBlt1.GetStatus())
			opDMess += GetString(IDS_STRING373) + CString("\n");//"Bezeichnung des Gep�ckbandes 1\n";

		if(!m_CE_DB1ba.GetStatus())
			opDMess += GetString(IDS_STRING374) + CString("\n");//"Beginn der Belegung des Gep�ckbandes 1\n";

		if(!m_CE_DB1ea.GetStatus())
			opDMess += GetString(IDS_STRING375) + CString("\n");//"Ende der Belegung des Gep�ckbandes 1\n";

		if((prmDFlight->B1ba != TIMENULL) && (prmDFlight->B1ea != TIMENULL))
		{
			if(prmDFlight->B1ba > prmDFlight->B1ea)
				opDMess += GetString(IDS_STRING376) + CString("\n");//"Ende der Belegung des Gep�ckbandes 1 ist vor Beginn\n";
		}
		}

		CString olTmp;
		CString olTmp2;

		CTime olCkbs;
		CTime olCkes;
		CTime olCkba;
		CTime olCkea;
		CString olDisp;
		bool blStatus;

		// Checkinschalter
		if(pomDCinsTable != NULL)
		{
			ilLines = pomDCinsTable->GetLinesCount();
			for( i = 0; i < ilLines; i++)
			{

				if (bgCnamAtr)
				{
					pomDCinsTable->GetTextFieldValue(i, 3, olTmp);
					pomDCinsTable->GetTextFieldValue(i, 4, olTmp2);
					olCkbs = HourStringToDate(olTmp, prmDFlight->Stod); 
					olCkes = HourStringToDate(olTmp2, prmDFlight->Stod); 
					pomDCinsTable->GetTextFieldValue(i, 5, olTmp);
					pomDCinsTable->GetTextFieldValue(i, 6, olTmp2);
					olCkba = HourStringToDate(olTmp, prmDFlight->Stod); 
					olCkea = HourStringToDate(olTmp2, prmDFlight->Stod); 
					pomDCinsTable->GetTextFieldValue(i, 7, olDisp);

				}
				else
				{
					pomDCinsTable->GetTextFieldValue(i, 2, olTmp);
					pomDCinsTable->GetTextFieldValue(i, 3, olTmp2);
					olCkbs = HourStringToDate(olTmp, prmDFlight->Stod); 
					olCkes = HourStringToDate(olTmp2, prmDFlight->Stod); 
					pomDCinsTable->GetTextFieldValue(i, 4, olTmp);
					pomDCinsTable->GetTextFieldValue(i, 5, olTmp2);
					olCkba = HourStringToDate(olTmp, prmDFlight->Stod); 
					olCkea = HourStringToDate(olTmp2, prmDFlight->Stod); 
					pomDCinsTable->GetTextFieldValue(i, 6, olDisp);
				}

				if (bgCnamAtr)
					blStatus = pomDCinsTable->GetCellStatus(i, 7);
				else
					blStatus = pomDCinsTable->GetCellStatus(i, 6);

				if(!blStatus && !olDisp.IsEmpty())
				{
					olBuffer.Format(GetString(IDS_STRING2132),i);//"FIDS Remark"
					opDMess += olBuffer; 
				}



				if(!pomDCinsTable->GetCellStatus(i, 0))
				{
					olBuffer.Format(GetString(IDS_STRING392),i);//"Bezeichnung Check-In Schalter in Zeile %d\n"
					opDMess += olBuffer; 
				}
				if(!pomDCinsTable->GetCellStatus(i, 2))
				{
					olBuffer.Format(GetString(IDS_STRING393),i); // "Check-In Schalter �ffnungszeit von in Zeile %d\n"
					opDMess += olBuffer; 
				}
				if(!pomDCinsTable->GetCellStatus(i, 3))
				{
					olBuffer.Format(GetString(IDS_STRING394),i); // "Check-In Schalter �ffnungszeit bis in Zeile %d\n"
					opDMess += olBuffer; 
				}


				if(!pomDCinsTable->GetCellStatus(i, 4))
				{
					olBuffer.Format(GetString(IDS_STRING1803), i); // "Check-In Schalter �ffnungszeit von in Zeile %d\n"
					opDMess += olBuffer; 
				}
				if(!pomDCinsTable->GetCellStatus(i, 5))
				{
					olBuffer.Format(GetString(IDS_STRING1804), i); // "Check-In Schalter �ffnungszeit bis in Zeile %d\n"
					opDMess += olBuffer; 
				}



				if(((olCkbs != TIMENULL) && (olCkes != TIMENULL) && olCkbs > olCkes) ||
					((olCkba != TIMENULL) && (olCkea != TIMENULL) && olCkba > olCkea))
				{
					// Allocation of Check-In Counter in line %d: Start time is after end time.\n
					olBuffer.Format(GetString(IDS_STRING395),i);
					opDMess += olBuffer; 
				}
				if ((olCkbs == TIMENULL && olCkes != TIMENULL) ||
					(olCkba == TIMENULL && olCkea != TIMENULL))
				{
					// Allocation of Check-In Counter in line %d: End time without start time!\n
					olBuffer.Format(GetString(IDS_STRING2011), i);
					opDMess += olBuffer; 
				}


			}
		}

		if(bgShowPayDetail && pomCBDAHOC->GetCheck() == TRUE)
		{
			//if(strcmp(prmDFlight->Mopa,"")==0 && m_CB_DAdhoc.GetCheck()==TRUE)
			if(strcmp(prmDFlight->Mopa,"")==0)
			{
				opDMess += "Payment Details" + CString("\n");
			}
		}			

	}

	if(opGMess.IsEmpty() && opAMess.IsEmpty() && opDMess.IsEmpty())
		return true;
	else
		return false;
}


void RotationDlg::OnAAlc3LIST() 
{

	CString olText;
	m_CE_AAlc3.GetWindowText(olText);

	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "ALT","ALC2,ALC3,ALFN", "ALC2+,ALC3+,ALFN+", omAAlc2+","+omAAlc3);
//	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "ALT","ALC2,ALC3,ALFN", "ALFN+,ALC2+", olText);
	polDlg->SetSecState("ROTATIONDLG_CE_AAlc3");
	if(polDlg->DoModal() == IDOK)
	{
		omAAlc2 = polDlg->GetField("ALC2");	
		omAAlc3 = polDlg->GetField("ALC3");	
		if(omAAlc2.IsEmpty())
			m_CE_AAlc3.SetInitText(omAAlc3, true);	
		else
			m_CE_AAlc3.SetInitText(omAAlc2, true);	
	}
	delete polDlg;

	CString olTmp;
	m_CE_AAlc3.GetWindowText(olTmp);	
	if(olTmp.IsEmpty())
	{
		m_CE_AOrg3.SetTextLimit(0,4,true);
		m_CE_AOrg3.SetBKColor(WHITE);
	}
	else
	{
		m_CE_AOrg3.SetTextLimit(4,4,false);
		m_CE_AOrg3.SetBKColor(YELLOW);
	}
	m_CE_AAlc3.SetFocus();	
}

void RotationDlg::OnDalc3list() 
{
	CString olText;
	m_CE_DAlc3.GetWindowText(olText);
	
	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "ALT","ALC2,ALC3,ALFN", "ALC2+,ALC3+,ALFN+", omDAlc2+","+omDAlc3);
//	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "ALT","ALC2,ALC3,ALFN", "ALFN+,ALC2+", olText);
	polDlg->SetSecState("ROTATIONDLG_CE_DAlc3");
	if(polDlg->DoModal() == IDOK)
	{
		omDAlc2 = polDlg->GetField("ALC2");	
		omDAlc3 = polDlg->GetField("ALC3");	
		if(omDAlc2.IsEmpty())
			m_CE_DAlc3.SetInitText(omDAlc3, true);	
		else
			m_CE_DAlc3.SetInitText(omDAlc2, true);	
	}
	delete polDlg;

	CString olTmp;
	m_CE_DAlc3.GetWindowText(olTmp);	
	if(olTmp.IsEmpty())
	{
		m_CE_DDes3.SetTextLimit(0,3,true);
		m_CE_DDes3.SetBKColor(WHITE);
		m_CE_DDes4.SetTextLimit(0,4,true);
		m_CE_DDes4.SetBKColor(WHITE);
	}
	else
	{
		m_CE_DDes3.SetTextLimit(3,3,false);
//		m_CE_DDes3.SetBKColor(YELLOW);
		m_CE_DDes4.SetTextLimit(4,4,false);
		m_CE_DDes4.SetBKColor(YELLOW);
	}
	m_CE_DAlc3.SetFocus();	


}







void RotationDlg::OnAblt1list() 
{
	CString olText;
	m_CE_ABlt1.GetWindowText(olText);

	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "BLT","BNAM,TERM", "BNAM+,TERM+", olText);
	polDlg->SetSecState("ROTATIONDLG_CE_ABlt1");
	if(polDlg->DoModal() == IDOK)
	{
		CString olBlt1 = polDlg->GetField("BNAM");
		m_CE_ABlt1.SetInitText(polDlg->GetField("BNAM"), true);	
		m_CE_ATmb1.SetInitText(polDlg->GetField("TERM"), true);	
		CheckForChangeReasons(pomBTCRCABLT1,m_ABlt1,olBlt1,&(omReasons.blABlt1Resf), 'A');
		
		m_CE_ABlt1.SetFocus();
	}
	delete polDlg;
}


void RotationDlg::OnAblt2list() 
{
	CString olText;
	m_CE_ABlt2.GetWindowText(olText);

	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "BLT","BNAM,TERM", "BNAM+,TERM+", olText);
	polDlg->SetSecState("ROTATIONDLG_CE_ABlt2");
	if(polDlg->DoModal() == IDOK)
	{
		CString olBlt2 = polDlg->GetField("BNAM");
		m_CE_ABlt2.SetInitText(polDlg->GetField("BNAM"), true);	
		m_CE_ATmb2.SetInitText(polDlg->GetField("TERM"), true);	
		CheckForChangeReasons(pomBTCRCABLT2,m_ABlt2,olBlt2,&(omReasons.blABlt2Resf), 'A');
		
		m_CE_ABlt2.SetFocus();
	}
	delete polDlg;
	
}
void RotationDlg::OnDblt1list() 
{
	CString olText;
	m_CE_DBlt1.GetWindowText(olText);

	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "BLT","BNAM,TERM", "BNAM+,TERM+", olText);
	polDlg->SetSecState("ROTATIONDLG_CE_DBlt1");
	if(polDlg->DoModal() == IDOK)
	{
		m_CE_DBlt1.SetInitText(polDlg->GetField("BNAM"), true);	
		m_CE_DTmb1.SetInitText(polDlg->GetField("TERM"), true);	
		m_CE_DBlt1.SetFocus();
	}
	delete polDlg;
}


/*
void RotationDlg::OnAct35list() 
{
	CString olText;
	m_CE_Act3.GetWindowText(olText);

	
	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "ACT","ACT3,ACT5,ACFN", "ACFN+,ACT3+", olText);
	polDlg->SetSecState("ROTATIONDLG_CE_Act3");
	if(polDlg->DoModal() == IDOK)
	{
		m_CE_Act3.SetInitText(polDlg->GetField("ACT3"), true);	
		m_CE_Act5.SetInitText(polDlg->GetField("ACT5"), true);	
		m_CE_Act3.SetFocus();	
	}
	delete polDlg;
}
*/

void RotationDlg::OnAct35list() 
{
	CString olText3;
	m_CE_Act3.GetWindowText(olText3);
	CString olText5;
	m_CE_Act5.GetWindowText(olText5);

	bool blSelAct3 = false;
	if (prmAFlightSave && CString(prmAFlightSave->Act5).IsEmpty())
		blSelAct3 = true;
	if (prmDFlightSave && CString(prmDFlightSave->Act5).IsEmpty())
		blSelAct3 = true;

	if (olText5.IsEmpty() && !olText3.IsEmpty() && blSelAct3)
	{
		OnAct3list();
		return;
	}

	CCS_TRY
	
	CString olSel = olText5 + "," + olText3;

	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "ACT","ACT5,ACT3,ACFN", "ACT5+,ACT3,+ACFN+", olSel);
	polDlg->SetSecState("ROTATIONDLG_CE_Act3");

	if(polDlg->DoModal() == IDOK)
	{
		if (bmActSelect)
			m_CE_Act5.SetFocus();

		m_CE_Act3.SetInitText(polDlg->GetField("ACT3"), true);	
		m_CE_Act5.SetInitText(polDlg->GetField("ACT5"), true);	

		bmActSelect = true;
		bmCheckRegn = true;
	}
	else
		m_CE_Act5.SetInitText(olText5, true);	

	delete polDlg;

	CCS_CATCH_ALL	
}

void RotationDlg::OnAct3list() 
{
	CString olText3;
	m_CE_Act3.GetWindowText(olText3);
	CString olText5;
	m_CE_Act5.GetWindowText(olText5);

	bool blSelAct5 = false;
	if (prmAFlightSave && CString(prmAFlightSave->Act3).IsEmpty())
		blSelAct5 = true;
	if (prmDFlightSave && CString(prmDFlightSave->Act3).IsEmpty())
		blSelAct5 = true;

	if (olText3.IsEmpty() && !olText5.IsEmpty() && blSelAct5)
	{
		OnAct35list();
		return;
	}

	CCS_TRY

	CString olSel = olText3 + "," + olText5;

	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "ACT","ACT3,ACT5,ACFN", "ACT3+,ACT5+,ACFN+", olSel);
	polDlg->SetSecState("ROTATIONDLG_CE_Act3");

	if(polDlg->DoModal() == IDOK)
	{
		if (bmActSelect)
			m_CE_Act5.SetFocus();

		m_CE_Act3.SetInitText(polDlg->GetField("ACT3"), true);	
		m_CE_Act5.SetInitText(polDlg->GetField("ACT5"), true);	

		bmActSelect = true;
		bmCheckRegn = true;
	}
	else
		m_CE_Act3.SetInitText(olText3, true);	

	delete polDlg;

	CCS_CATCH_ALL	

}

void RotationDlg::OnAext1list() 
{
	CString olText;
	m_CE_AExt1.GetWindowText(olText);

	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "EXT","ENAM,TERM", "ENAM+", olText);
	polDlg->SetSecState("ROTATIONDLG_CE_AExt1");
	if(polDlg->DoModal() == IDOK)
	{
		m_CE_AExt1.SetInitText(polDlg->GetField("ENAM"), true);	
		m_CE_ATet1.SetInitText(polDlg->GetField("TERM"), true);	
	}
	delete polDlg;
}


void RotationDlg::OnAext2list() 
{
	CString olText;
	m_CE_AExt2.GetWindowText(olText);

	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "EXT","ENAM,TERM", "ENAM+", olText);
	polDlg->SetSecState("ROTATIONDLG_CE_AExt2");
	if(polDlg->DoModal() == IDOK)
	{
		m_CE_AExt2.SetInitText(polDlg->GetField("ENAM"), true);	
		m_CE_ATet2.SetInitText(polDlg->GetField("TERM"), true);	
	}
	delete polDlg;
}

void RotationDlg::OnAgta1list() 
{
	CString olText;
	m_CE_AGta1.GetWindowText(olText);

	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "GAT_PORT","GNAM,TERM", "GNAM+", olText);
	polDlg->SetSecState("ROTATIONDLG_CE_AGta1");
	if(polDlg->DoModal() == IDOK)
	{
		CString olGta1 = polDlg->GetField("GNAM");
		// For Getting the Reason for change flag - PRF 8379 
		CheckForChangeReasons(pomBTCRCAGTA1,m_AGta1,olGta1,&(omReasons.blAGta1Resf), 'A');
		
		m_CE_AGta1.SetInitText(olGta1, true);	
		m_CE_ATga1.SetInitText(polDlg->GetField("TERM"), true);	
		m_CE_AGta1.SetFocus();
	}
	delete polDlg;
}

void RotationDlg::OnAgta2list() 
{
	CString olText;
	m_CE_AGta2.GetWindowText(olText);

	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "GAT_PORT","GNAM,TERM", "GNAM+", olText);
	polDlg->SetSecState("ROTATIONDLG_CE_AGta2");
	if(polDlg->DoModal() == IDOK)
	{
		CString olGta2 = polDlg->GetField("GNAM");			
		// For Getting the Reason for change flag - PRF 8379 
		CheckForChangeReasons(pomBTCRCAGTA2,m_AGta2,olGta2,&(omReasons.blAGta2Resf), 'A');
		
		m_CE_AGta2.SetInitText(olGta2, true);	
		m_CE_ATga2.SetInitText(polDlg->GetField("TERM"), true);	
		m_CE_AGta2.SetFocus();
	}
	delete polDlg;
}


/*
void RotationDlg::OnAorg3list() 
{
	CString olText;
	m_CE_AOrg3.GetWindowText(olText);
	
	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "APT","APC3,APC4,APFN", "APFN+", olText);
	polDlg->SetSecState("ROTATIONDLG_CE_AOrg3");
	if(polDlg->DoModal() == IDOK)
	{
		m_CE_AOrg3.SetInitText(polDlg->GetField("APC4"), true);	
		m_CE_AOrg3.SetFocus();	
	}
	delete polDlg;
}
*/
void RotationDlg::OnAorg3list() 
{
	CString olText;
	CString olFields ("APC3,APC4,APFN");
	m_CE_AOrg3L.GetWindowText(olText);
	if (olText.IsEmpty())
	{
		m_CE_AOrg3.GetWindowText(olText);
		olFields = "APC4,APC3,APFN";
	}

//	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "APT","APC3,APC4,APFN", "APFN+", olText);
	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "APT", olFields, "APFN+", olText);
	polDlg->SetSecState("ROTATIONDLG_CE_AOrg3");
	if(polDlg->DoModal() == IDOK)
	{
		m_CE_AOrg3.SetInitText(polDlg->GetField("APC4"), true);	
		m_CE_AOrg3L.SetInitText(polDlg->GetField("APC3"), true);	
		m_CE_AOrg3.SetFocus();	
	}
	delete polDlg;
}


void RotationDlg::OnApstalist() 
{



	CString olText;
	m_CE_APsta.GetWindowText(olText);

	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "PST","PNAM", "PNAM+", olText);
	polDlg->SetSecState("ROTATIONDLG_CE_APsta");
	if(polDlg->DoModal() == IDOK)
	{
		CString olPosition = polDlg->GetField("PNAM"); 
		// For Getting the Reason for change flag - PRF 8379 
		CheckForChangeReasons(pomBTCRCFPSA,m_APsta,olPosition,&(omReasons.blPstaResf), 'A'); 
	
		m_CE_APsta.SetInitText(olPosition, true);	
		m_CE_APsta.SetFocus();	
		m_CE_APsta.SetFocus();
	}
	delete polDlg;
	
}



void RotationDlg::OnDdes3list() 
{
	CString olText;
	CString olFields ("APC3,APC4,APFN");
	m_CE_DDes3.GetWindowText(olText);
	if (olText.IsEmpty())
	{
		m_CE_DDes4.GetWindowText(olText);
		olFields = "APC4,APC3,APFN";
	}

//	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "APT","APC3,APC4,APFN", "APFN+", olText);
	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "APT", olFields, "APFN+", olText);
	polDlg->SetSecState("ROTATIONDLG_CE_DDes4");
	if(polDlg->DoModal() == IDOK)
	{
		m_CE_DDes4.SetInitText(polDlg->GetField("APC4"), true);	
		m_CE_DDes3.SetInitText(polDlg->GetField("APC3"), true);
		m_CE_DDes4.SetFocus();	
	}
	delete polDlg;

/*

	CString olText;
	m_CE_DDes4.GetWindowText(olText);

	
	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "APT","APC3,APC4,APFN", "APFN+", olText);
	polDlg->SetSecState("ROTATIONDLG_CE_DDes4");
	if(polDlg->DoModal() == IDOK)
	{
		m_CE_DDes3.SetInitText(polDlg->GetField("APC3"), true);	
		m_CE_DDes4.SetInitText(polDlg->GetField("APC4"), true);	
		m_CE_DDes4.SetFocus();	
	}
	delete polDlg;
*/
}


void RotationDlg::OnDgta1list() 
{
	CString olText;
	m_CE_DGtd1.GetWindowText(olText);

	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "GAT_GATE","GNAM,TERM", "GNAM+", olText);
	polDlg->SetSecState("ROTATIONDLG_CE_DGtd1");
	if(polDlg->DoModal() == IDOK)
	{
		CString olGtd1 = polDlg->GetField("GNAM");			;
		// For Getting the Reason for change flag - PRF 8379 
		CheckForChangeReasons(pomBTCRCDGTD1,m_DGtd1,olGtd1,&(omReasons.blDGtd1Resf), 'D');
		
		m_CE_DGtd1.SetInitText(olGtd1, true);	
		m_CE_DTgd1.SetInitText(polDlg->GetField("TERM"), true);	
		m_CE_DGtd1.SetFocus();
	}
	delete polDlg;

}

void RotationDlg::OnDgta2list() 
{
	CString olText;
	m_CE_DGtd2.GetWindowText(olText);

	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "GAT_GATE","GNAM,TERM", "GNAM+", olText);
	polDlg->SetSecState("ROTATIONDLG_CE_DGtd2");
	if(polDlg->DoModal() == IDOK)
	{
		CString olGtd2 = polDlg->GetField("GNAM");
		// For Getting the Reason for change flag - PRF 8379 
		CheckForChangeReasons(pomBTCRCDGTD2,m_DGtd2,olGtd2,&(omReasons.blDGtd2Resf), 'D');
		
		m_CE_DGtd2.SetInitText(olGtd2, true);	
		m_CE_DTgd2.SetInitText(polDlg->GetField("TERM"), true);	
		m_CE_DGtd2.SetFocus();
	}
	delete polDlg;
}


void RotationDlg::OnDpstdlist() 
{
	CString olText;
	m_CE_DPstd.GetWindowText(olText);

	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "PST","PNAM", "PNAM+", olText);
	polDlg->SetSecState("ROTATIONDLG_CE_DPstd");
	if(polDlg->DoModal() == IDOK)
	{
		CString olPosition = polDlg->GetField("PNAM");
		// For Getting the Reason for change flag - PRF 8379 
		CheckForChangeReasons(pomBTCRCFPSD,m_DPstd,olPosition,&(omReasons.blPstdResf), 'D');
		
		m_CE_DPstd.SetInitText(olPosition, true);	
		m_CE_DPstd.SetFocus();
	}
	delete polDlg;
}


void RotationDlg::OnDwro1list() 
{
	CString olText;
	m_CE_DWro1.GetWindowText(olText);

	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "WRO","WNAM,TERM", "WNAM+", olText);
	polDlg->SetSecState("ROTATIONDLG_CE_DWro1");
	if(polDlg->DoModal() == IDOK)
	{
		m_CE_DWro1.SetInitText(polDlg->GetField("WNAM"), true);	
		m_CE_DTwr1.SetInitText(polDlg->GetField("TERM"), true);	
		m_CE_DWro1.SetFocus();
	}
	delete polDlg;
}

void RotationDlg::OnDwro3list() 
{
	CString olText;
	m_CE_DWro3.GetWindowText(olText);

	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "WRO","WNAM,TERM", "WNAM+", olText);
	polDlg->SetSecState("ROTATIONDLG_CE_DWro3");
	if(polDlg->DoModal() == IDOK)
	{
		m_CE_DWro3.SetInitText(polDlg->GetField("WNAM"), true);	
		m_CE_DTwr3.SetInitText(polDlg->GetField("TERM"), true);	
		m_CE_DWro3.SetFocus();
	}
	delete polDlg;
}




void RotationDlg::OnDttyplist() 
{
	CString olText;
	m_CE_DTtyp.GetWindowText(olText);

	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "NAT","TTYP,TNAM", "TNAM+", olText);
	polDlg->SetSecState("ROTATIONDLG_CE_DTtyp");
	if(polDlg->DoModal() == IDOK)
	{
		m_CE_DTtyp.SetInitText(polDlg->GetField("TTYP"), true);	
	}
	delete polDlg;
}

void RotationDlg::OnAttyplist() 
{
	CString olText;
	m_CE_ATtyp.GetWindowText(olText);

	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "NAT","TTYP,TNAM", "TNAM+", olText);
	polDlg->SetSecState("ROTATIONDLG_CE_ATtyp");
	if(polDlg->DoModal() == IDOK)
	{
		m_CE_ATtyp.SetInitText(polDlg->GetField("TTYP"), true);	
	}
	delete polDlg;
}




void RotationDlg::OnDstyplist() 
{
	CString olText;
	m_CE_DStyp.GetWindowText(olText);

	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "STY","STYP,SNAM", "SNAM+", olText);
	polDlg->SetSecState("ROTATIONDLG_CE_DStyp");
	if(polDlg->DoModal() == IDOK)
	{
		m_CE_DStyp.SetInitText(polDlg->GetField("STYP"), true);	
	}
	delete polDlg;
}



void RotationDlg::OnAstyplist() 
{
	CString olText;
	m_CE_AStyp.GetWindowText(olText);

	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "STY","STYP,SNAM", "SNAM+", olText);
	polDlg->SetSecState("ROTATIONDLG_CE_AStyp");
	if(polDlg->DoModal() == IDOK)
	{
		m_CE_AStyp.SetInitText(polDlg->GetField("STYP"), true);	
	}
	delete polDlg;
}



void RotationDlg::OnAhtyplist() 
{
	CString olText;
	m_CE_AHtyp.GetWindowText(olText);

	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "HTY","HTYP,HNAM", "HNAM+", olText);
	if(polDlg->DoModal() == IDOK)
	{
		m_CE_AHtyp.SetInitText(polDlg->GetField("HTYP"), true);	
	}
	delete polDlg;
}




void RotationDlg::OnDhtyplist() 
{
	CString olText;
	m_CE_DHtyp.GetWindowText(olText);

	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "HTY","HTYP,HNAM", "HNAM+", olText);
	if(polDlg->DoModal() == IDOK)
	{
		m_CE_DHtyp.SetInitText(polDlg->GetField("HTYP"), true);	
	}
	delete polDlg;
}




/////////////////////////////////////////////////////////////////////////////////
//Tables !!



void RotationDlg::OnAAlc3LIST2() 
{
	int ilLine = pomAJfnoTable->pomListBox->GetTopIndex();

	CString olText;
	pomAJfnoTable->GetTextFieldValue(ilLine, 0, olText);
	CString olFields = "ALC2,ALC3,ALFN";
	CString olSort   = "ALC2+,ALC3+,ALFN+";
	if (olText.GetLength() == 3)
	{
		olFields = "ALC3,ALC2,ALFN";
		olSort   = "ALC3+,ALC2+,ALFN+";
	}

	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "ALT",olFields, olSort, olText);
	
//	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "ALT","ALC2,ALC3,ALFN", "ALFN+", olText);
	polDlg->SetSecState("ROTATIONDLG_CB_AShowJfno");
	if(polDlg->DoModal() == IDOK)
	{
		CString olAlc2 = polDlg->GetField("ALC2");	
		CString olAlc3 = polDlg->GetField("ALC3");	
		if(olAlc2.IsEmpty())
			pomAJfnoTable->SetIPValue(ilLine, 0, olAlc3);
		else
			pomAJfnoTable->SetIPValue(ilLine, 0, olAlc2);

//		pomAJfnoTable->SetIPValue(ilLine, 0, polDlg->GetField("ALC2"));
	}
	delete polDlg;
}

void RotationDlg::OnAAlc3LIST3() 
{

	int ilLine = pomAJfnoTable->pomListBox->GetTopIndex()+1;

	CString olText;
	pomAJfnoTable->GetTextFieldValue(ilLine, 0, olText);
	CString olFields = "ALC2,ALC3,ALFN";
	CString olSort   = "ALC2+,ALC3+,ALFN+";
	if (olText.GetLength() == 3)
	{
		olFields = "ALC3,ALC2,ALFN";
		olSort   = "ALC3+,ALC2+,ALFN+";
	}

	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "ALT",olFields, olSort, olText);
	
//	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "ALT","ALC2,ALC3,ALFN", "ALFN+", olText);
	polDlg->SetSecState("ROTATIONDLG_CB_AShowJfno");
	if(polDlg->DoModal() == IDOK)
	{
		CString olAlc2 = polDlg->GetField("ALC2");	
		CString olAlc3 = polDlg->GetField("ALC3");	
		if(olAlc2.IsEmpty())
			pomAJfnoTable->SetIPValue(ilLine, 0, olAlc3);
		else
			pomAJfnoTable->SetIPValue(ilLine, 0, olAlc2);

//		pomAJfnoTable->SetIPValue(ilLine, 0, polDlg->GetField("ALC2"));
	}
	delete polDlg;
}

void RotationDlg::OnDalc3list2() 
{
	int ilLine = pomDJfnoTable->pomListBox->GetTopIndex();

	CString olText;
	pomDJfnoTable->GetTextFieldValue(ilLine, 0, olText);
	CString olFields = "ALC2,ALC3,ALFN";
	CString olSort   = "ALC2+,ALC3+,ALFN+";
	if (olText.GetLength() == 3)
	{
		olFields = "ALC3,ALC2,ALFN";
		olSort   = "ALC3+,ALC2+,ALFN+";
	}

	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "ALT",olFields, olSort, olText);

//	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "ALT","ALC2,ALC3,ALFN", "ALFN+", olText);
	polDlg->SetSecState("ROTATIONDLG_CB_DShowJfno");
	if(polDlg->DoModal() == IDOK)
	{
		CString olAlc2 = polDlg->GetField("ALC2");	
		CString olAlc3 = polDlg->GetField("ALC3");	
		if(olAlc2.IsEmpty())
			pomDJfnoTable->SetIPValue(ilLine, 0, olAlc3);
		else
			pomDJfnoTable->SetIPValue(ilLine, 0, olAlc2);

//		pomDJfnoTable->SetIPValue(ilLine, 0, polDlg->GetField("ALC2"));
	}
	delete polDlg;
	
}

void RotationDlg::OnDalc3list3() 
{
	int ilLine = pomDJfnoTable->pomListBox->GetTopIndex()+1;

	CString olText;
	pomDJfnoTable->GetTextFieldValue(ilLine, 0, olText);
	CString olFields = "ALC2,ALC3,ALFN";
	CString olSort   = "ALC2+,ALC3+,ALFN+";
	if (olText.GetLength() == 3)
	{
		olFields = "ALC3,ALC2,ALFN";
		olSort   = "ALC3+,ALC2+,ALFN+";
	}

	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "ALT",olFields, olSort, olText);
	
//	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "ALT","ALC2,ALC3,ALFN", "ALFN+", olText);
	polDlg->SetSecState("ROTATIONDLG_CB_DShowJfno");
	if(polDlg->DoModal() == IDOK)
	{
		CString olAlc2 = polDlg->GetField("ALC2");	
		CString olAlc3 = polDlg->GetField("ALC3");	
		if(olAlc2.IsEmpty())
			pomDJfnoTable->SetIPValue(ilLine, 0, olAlc3);
		else
			pomDJfnoTable->SetIPValue(ilLine, 0, olAlc2);

//		pomDJfnoTable->SetIPValue(ilLine, 0, polDlg->GetField("ALC2"));
	}
	delete polDlg;
}



void RotationDlg::OnDcinslist1() 
{

	CCS_TRY

	int ilLine = pomDCinsTable->pomListBox->GetTopIndex();

	CString olText;
	pomDCinsTable->GetTextFieldValue(ilLine, 0, olText);

	AwBasicDataDlg *polDlg = NULL;
	if (bgCnamAtr)
		polDlg = new AwBasicDataDlg(this, "CIC","CNAM,CATR,TERM", "CNAM+", olText);
	else
		polDlg = new AwBasicDataDlg(this, "CIC","CNAM,TERM", "CNAM+", olText);

	polDlg->SetSecState("SEASONDLG_CE_DCins");
	if(polDlg->DoModal() == IDOK)
	{
		//	reject a new checkin-counter if counter is already open
		if (CcaHasNoOpentime (ilLine) == 0L)
		{
			if (bgCnamAtr)
			{
				pomDCinsTable->SetIPValue(ilLine, 0, polDlg->GetField("CNAM"));
				pomDCinsTable->SetIPValue(ilLine, 1, polDlg->GetField("CATR"));
				pomDCinsTable->SetIPValue(ilLine, 2, polDlg->GetField("TERM"));
			}
			else
			{
				pomDCinsTable->SetIPValue(ilLine, 0, polDlg->GetField("CNAM"));
				pomDCinsTable->SetIPValue(ilLine, 1, polDlg->GetField("TERM"));
			}
		}
	}
	delete polDlg;

	CCS_CATCH_ALL

}

void RotationDlg::OnDcinslist2() 
{

	CCS_TRY

	int ilLine = pomDCinsTable->pomListBox->GetTopIndex() + 1; 

	CString olText;
	pomDCinsTable->GetTextFieldValue(ilLine, 0, olText);

	AwBasicDataDlg *polDlg = NULL;
	if (bgCnamAtr)
		polDlg = new AwBasicDataDlg(this, "CIC","CNAM,CATR,TERM", "CNAM+", olText);
	else
		polDlg = new AwBasicDataDlg(this, "CIC","CNAM,TERM", "CNAM+", olText);

	polDlg->SetSecState("SEASONDLG_CE_DCins");
	if(polDlg->DoModal() == IDOK)
	{
		//	reject a new checkin-counter if counter is already open
		if (CcaHasNoOpentime (ilLine) == 0L)
		{
			if (bgCnamAtr)
			{
				pomDCinsTable->SetIPValue(ilLine, 0, polDlg->GetField("CNAM"));
				pomDCinsTable->SetIPValue(ilLine, 1, polDlg->GetField("CATR"));
				pomDCinsTable->SetIPValue(ilLine, 2, polDlg->GetField("TERM"));
			}
			else
			{
				pomDCinsTable->SetIPValue(ilLine, 0, polDlg->GetField("CNAM"));
				pomDCinsTable->SetIPValue(ilLine, 1, polDlg->GetField("TERM"));
			}
		}
	}
	delete polDlg;

	CCS_CATCH_ALL

}

void RotationDlg::OnDcinslist3() 
{

	CCS_TRY

	int ilLine = pomDCinsTable->pomListBox->GetTopIndex() + 2;

	CString olText;
	pomDCinsTable->GetTextFieldValue(ilLine, 0, olText);

	AwBasicDataDlg *polDlg = NULL;
	if (bgCnamAtr)
		polDlg = new AwBasicDataDlg(this, "CIC","CNAM,CATR,TERM", "CNAM+", olText);
	else
		polDlg = new AwBasicDataDlg(this, "CIC","CNAM,TERM", "CNAM+", olText);

	polDlg->SetSecState("SEASONDLG_CE_DCins");
	if(polDlg->DoModal() == IDOK)
	{
		//	reject a new checkin-counter if counter is already open
		if (CcaHasNoOpentime (ilLine) == 0L)
		{
			if (bgCnamAtr)
			{
				pomDCinsTable->SetIPValue(ilLine, 0, polDlg->GetField("CNAM"));
				pomDCinsTable->SetIPValue(ilLine, 1, polDlg->GetField("CATR"));
				pomDCinsTable->SetIPValue(ilLine, 2, polDlg->GetField("TERM"));
			}
			else
			{
				pomDCinsTable->SetIPValue(ilLine, 0, polDlg->GetField("CNAM"));
				pomDCinsTable->SetIPValue(ilLine, 1, polDlg->GetField("TERM"));
			}
		}
	}
	delete polDlg;

	CCS_CATCH_ALL

}

void RotationDlg::OnDcinslist4() 
{

	CCS_TRY

	int ilLine = pomDCinsTable->pomListBox->GetTopIndex() + 3;

	CString olText;
	pomDCinsTable->GetTextFieldValue(ilLine, 0, olText);

	AwBasicDataDlg *polDlg = NULL;
	if (bgCnamAtr)
		polDlg = new AwBasicDataDlg(this, "CIC","CNAM,CATR,TERM", "CNAM+", olText);
	else
		polDlg = new AwBasicDataDlg(this, "CIC","CNAM,TERM", "CNAM+", olText);

	polDlg->SetSecState("SEASONDLG_CE_DCins");
	if(polDlg->DoModal() == IDOK)
	{
		//	reject a new checkin-counter if counter is already open
		if (CcaHasNoOpentime (ilLine) == 0L)
		{
			if (bgCnamAtr)
			{
				pomDCinsTable->SetIPValue(ilLine, 0, polDlg->GetField("CNAM"));
				pomDCinsTable->SetIPValue(ilLine, 1, polDlg->GetField("CATR"));
				pomDCinsTable->SetIPValue(ilLine, 2, polDlg->GetField("TERM"));
			}
			else
			{
				pomDCinsTable->SetIPValue(ilLine, 0, polDlg->GetField("CNAM"));
				pomDCinsTable->SetIPValue(ilLine, 1, polDlg->GetField("TERM"));
			}
		}
	}
	delete polDlg;

	CCS_CATCH_ALL

}

void RotationDlg::ManageDelayCode(int ID, const CString &ropSecStr)
{
	if (bgDelaycodeNumeric)
		ManageBasicData(ID, "DEN", "DECN,DECA,ALC3,DENA", "DECN+,ALC3", "DECN", ropSecStr );
	else
		ManageBasicData(ID, "DEN", "DECA,DECN,ALC3,DENA", "DECA+,ALC3", "DECA", ropSecStr );
}

void RotationDlg::ManageBasicData(int ID, CString opTab, CString opFields, CString opSort, CString opShow, const CString &ropSecStr)
{
	CWnd* olWnd = (CWnd*) GetDlgItem(ID);
	if (! olWnd)
		return;

	CString olText;
	olWnd->GetWindowText(olText);

	if(opTab == "DEN")
		olText = olText + CString(",") + olText;

	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, opTab, opFields, opSort, olText );
	polDlg->SetSecState(ropSecStr);
	if(polDlg->DoModal() == IDOK)
	{
		olWnd->SetWindowText(polDlg->GetField(opShow));	
	}

	delete polDlg;
	return;
}

void RotationDlg::OnAdcd1list() 
{
	ManageDelayCode(IDC_ADCD1, "ROTATIONDLG_CE_ADcd1");
}

void RotationDlg::OnAdcd2list() 
{
	ManageDelayCode(IDC_ADCD2, "ROTATIONDLG_CE_ADcd2");
}


void RotationDlg::OnDdcd1list() 
{
	ManageDelayCode(IDC_DDCD1, "ROTATIONDLG_CE_DDcd1");
}

void RotationDlg::OnDdcd2list() 
{
	ManageDelayCode(IDC_DDCD2, "ROTATIONDLG_CE_DDcd2");
}



void RotationDlg::OnPaid() 
{
	if ((MessageBox( GetString(IDS_STRING1595),GetString(ST_FRAGE),(MB_ICONQUESTION | MB_YESNO | MB_DEFBUTTON2))) == IDYES)
	{
		ogRotationDlgFlights.UpdateFlight(prmAFlight->Urno, "PAID", "P");
		ogRotationDlgFlights.UpdateFlight(prmDFlight->Urno, "PAID", "P");

		prmAFlight->Paid[0]='P';
		prmDFlight->Paid[0]='P';

		//Get comment and Save the Comment
	/*	
		CString strCom = "";
		((CCSEdit*)GetDlgItem(IDC_BTCRC_CASH_COMMENT))->GetWindowText(strCom);
		int i = strCom.GetLength();
		if(i>0)
		{
			char *dstr = new char[i];
			strcpy(dstr, strCom);

			bool blRet1 = ogRotationDlgFlights.UpdateFlight(prmAFlight->Urno, "PCOM", dstr);
			bool blRet2 = ogRotationDlgFlights.UpdateFlight(prmDFlight->Urno, "PCOM", dstr);

		
		}
		*/

	}
	else
	{
		ogRotationDlgFlights.UpdateFlight(prmAFlight->Urno, "PAID", "B");
		ogRotationDlgFlights.UpdateFlight(prmDFlight->Urno, "PAID", "B");

		prmAFlight->Paid[0]='B';
		prmDFlight->Paid[0]='B';

	}


	//Update the cash button
	char clPaid;
	CString olAlc3;
	// AFTPAID can be 'B','K','P'
	if(prmDFlight->Urno != 0)
	{
		olAlc3 = prmDFlight->Alc3;
		if(strlen(prmDFlight->Paid) == 0)
			clPaid = ' ';
		else
			clPaid = prmDFlight->Paid[0];
	}
	else
	{
		olAlc3 = prmAFlight->Alc3;
		if(strlen(prmAFlight->Paid) == 0)
			clPaid = ' ';
		else
			clPaid = prmAFlight->Paid[0];
	}

	UpdateCashButton(olAlc3, clPaid);

}


void RotationDlg::OnSelchangeDGatLogo() 
{
	OnEditChanged(0,0);	
}



void RotationDlg::OnSelchangeDremp() 
{
	OnEditChanged(0,0);	
}

void RotationDlg::OnSelchangeAremp() 
{
	OnEditChanged(0,0);	
}

void RotationDlg::ReloadCCA()
{

	CCS_TRY

	if (prmDFlight->Urno == 0)
		return;

	CString olWhere;
	olWhere.Format("WHERE FLNU = %d", prmDFlight->Urno);
	char pclWhere[512];
	strcpy(pclWhere,olWhere);
	omCcaData.Read(pclWhere, true);
	
	CCAFLIGHTDATA rlCcaFlight;
	if(bmLocalTime) ConvertStructLocalTOUtc(NULL, prmDFlight);
	rlCcaFlight = *prmDFlight;
	if(bmLocalTime) ConvertStructUtcTOLocal(NULL, prmDFlight);

	omCcaData.CheckDemands(&rlCcaFlight);

	GetCcas();
	DShowCinsTable();
	DShowChaTable();

	CCS_CATCH_ALL

}


void RotationDlg::ProcessRotationChange(ROTATIONDLGFLIGHTDATA *prpFlight)
{
	if (prpFlight)
		NewData(this, prpFlight->Rkey, prpFlight->Urno, prpFlight->Adid, bmLocalTime);
	
	return;
}


void RotationDlg::ProcessFlightChange(ROTATIONDLGFLIGHTDATA *prpFlight)
{
	if(prpFlight == NULL)
	{
		ProcessRotationChange();
		return;
	}
	
	if(prmAFlight->Urno == prpFlight->Urno)
	{
		*prmAFlight		= *prpFlight;
		*prmAFlightSave = *prpFlight;
		InitDialog(true,false);
	}
	
	if(prmDFlight->Urno == prpFlight->Urno)
	{
		*prmDFlight		= *prpFlight;
		*prmDFlightSave = *prpFlight;
		
		InitDialog(false, true);
		
		ReadCcaData();
		DShowCinsTable();	
		ReadChaData();
		DShowChaTable();	
	}
	
	char clPaid;
	CString olAlc3;
	// AFTPAID can be 'B','K','P'
	if(prmDFlight->Urno != 0)
	{
		olAlc3 = prmDFlight->Alc3;
		if(strlen(prmDFlight->Paid) == 0)
			clPaid = ' ';
		else
			clPaid = prmDFlight->Paid[0];
	}
	else
	{
		olAlc3 = prmAFlight->Alc3;
		if(strlen(prmAFlight->Paid) == 0)
			clPaid = ' ';
		else
			clPaid = prmAFlight->Paid[0];
	}
	
	UpdateCashButton(olAlc3, clPaid);
	
	
	
}



bool RotationDlg::ProcessRotationChange()
{

	ROTATIONDLGFLIGHTDATA *prlFlight;
	ROTATIONDLGFLIGHTDATA *prlFlight2;

	prlFlight = new ROTATIONDLGFLIGHTDATA;

	*prmAFlight		= *prlFlight;
	*prmAFlightSave = *prlFlight;
	*prmDFlight		= *prlFlight;
	*prmDFlightSave = *prlFlight;
	delete prlFlight;

	bmInit = false;

	if((prlFlight = ogRotationDlgFlights.GetFlightByUrno(lmCalledBy)) == NULL)
	{
		return false;
	}
	else
	{
		lmRkey = prlFlight->Rkey;

		if((strcmp(prlFlight->Des4, pcgHome4) == 0) && (!((strcmp(prlFlight->Adid, "B") == 0) && omAdid == "D")))
		{
			*prmAFlight		= *prlFlight;
			*prmAFlightSave = *prlFlight;
			if((prlFlight2 = ogRotationDlgFlights.GetDeparture(prlFlight)) != NULL)
			{
				*prmDFlight		= *prlFlight2;
				*prmDFlightSave = *prlFlight2;
			}
		}
		else
		{
			*prmDFlight		= *prlFlight;
			*prmDFlightSave = *prlFlight;
			if((prlFlight2 = ogRotationDlgFlights.GetArrival(prlFlight)) != NULL)
			{
				*prmAFlight		= *prlFlight2;
				*prmAFlightSave = *prlFlight2;
			}
		}
	}
	InitDialog(true, true);

	
	char clPaid;
	CString olAlc3;
	if(prmDFlight->Urno != 0)
	{
		olAlc3 = prmDFlight->Alc3;
		if(strlen(prmDFlight->Paid) == 0)
			clPaid = ' ';
		else
			clPaid = prmDFlight->Paid[0];
	}
	else
	{
		olAlc3 = prmAFlight->Alc3;
		if(strlen(prmAFlight->Paid) == 0)
			clPaid = ' ';
		else
			clPaid = prmAFlight->Paid[0];
	}

	UpdateCashButton(olAlc3, clPaid); 
	
	
	UpdateVipButton(); 
 

	return true;
}


void RotationDlg::OnDTelex() 
{
	CTimeSpan olSpan(1,0,0,0);

	CTime olTimeStoa = prmDFlight->Stoa;
	CTime olTimeStod = prmDFlight->Stod;

	if (bmLocalTime) ogBasicData.LocalToUtc(olTimeStoa);
	if (bmLocalTime) ogBasicData.LocalToUtc(olTimeStod);

	CTime olTimeFrom = olTimeStod - olSpan;
	CString olFrom   = olTimeFrom.Format("%Y%m%d%H%M00");
	
	CTime olTimeTo = olTimeStod + olSpan;
	CString olTo   = olTimeTo.Format("%Y%m%d%H%M00");

 	CString olTif = olTimeStod.Format("%Y%m%d%H%M00");

	pogButtonList->CallTelexpool(prmDFlight->Urno, CString(prmDFlight->Alc3), CString(prmDFlight->Fltn),
		CString(prmDFlight->Flns), olFrom, olTo, CString(prmDFlight->Regn),
		CString(prmDFlight->Flda), CString(prmDFlight->Adid), olTimeStoa, olTimeStod);
}



void RotationDlg::OnATelex() 
{
	CTimeSpan olSpan(1,0,0,0);

	CTime olTimeStoa = prmAFlight->Stoa;
	CTime olTimeStod = prmAFlight->Stod;

	if (bmLocalTime) ogBasicData.LocalToUtc(olTimeStoa);
	if (bmLocalTime) ogBasicData.LocalToUtc(olTimeStod);

	CTime olTimeFrom = olTimeStoa - olSpan;
	CString olFrom   = olTimeFrom.Format("%Y%m%d%H%M00");
	
	CTime olTimeTo = olTimeStoa + olSpan;
	CString olTo   = olTimeTo.Format("%Y%m%d%H%M00");

 	CString olTif = olTimeStoa.Format("%Y%m%d%H%M00");

	pogButtonList->CallTelexpool(prmAFlight->Urno, CString(prmAFlight->Alc3), CString(prmAFlight->Fltn),
		CString(prmAFlight->Flns), olFrom, olTo, CString(prmAFlight->Regn),
		CString(prmAFlight->Flda), CString(prmAFlight->Adid), olTimeStoa, olTimeStod);

}

void RotationDlg::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	// TODO: Add your message handler code here and/or call default

	
	if(nChar == 13)
		OnOK();
	else
		CDialog::OnKeyDown(nChar, nRepCnt, nFlags);
}

//********************************************************
//MWO: 15.04.05: For multiple delay codes the broadcast
//               processing.
//********************************************************
void RotationDlg::ProcessDCF(void *data, int ipDDXType)
{
	BcStruct *pS = (BcStruct*)data;
	
	//Extract the urno
	CString olFlightUrno;
	CString olDCFUrno;
	GetListItemByField(CString(pS->Data), CString(pS->Fields), CString("FURN"), olFlightUrno);
	GetListItemByField(CString(pS->Data), CString(pS->Fields), CString("URNO"), olDCFUrno);
	int ilFURN = atoi(olFlightUrno.GetBuffer(0));
	CTAB *pTab = NULL; 
	CCSButtonCtrl *polButton=NULL;
	if(prmAFlight->Urno == ilFURN)
	{
		pTab = &tabDelayArrival;
		polButton = &m_DelayArrBtn;
	}
	else if(prmDFlight->Urno == ilFURN)
	{
		pTab = &tabDelayDeparture;
		polButton = &m_DelayDepBtn;
	}

	if(strcmp(pS->Cmd, "DRT") == 0)
	 {
		  
		  	CString olUrnoList = GetUrnoListFromSelection(pS->Selection);
			CStringArray olUrnoArray;

			int ilSizeArray = ExtractItemList(olUrnoList, &olUrnoArray, ',');
			CString llUrno;

			for(int i = 0; i < ilSizeArray; i++)
			{
					//llUrno = atol(olUrnoArray[i]);
				llUrno = olUrnoArray[i];
						//Removing arrival tab
				for(long i = 0; i < tabDelayArrival.GetLineCount(); i++)
				{
					CString tmp = tabDelayArrival.GetFieldValue(i, "URNO");
					if(tabDelayArrival.GetFieldValue(i, "URNO") == llUrno)
					{
						tabDelayArrival.DeleteLine(i);
						break;
					}
				}
				tabDelayArrival.RedrawTab();
				tabDelayArrival.Refresh();

				CString olText = "Delays (" + GetDelayCount(&tabDelayArrival) + ")";
		
				m_DelayArrBtn.SetWindowText(olText);

				if (m_CB_Ok.m_hWnd)
					m_DelayArrBtn.SetFont(m_CB_Ok.GetFont());

				if(tabDelayArrival.GetLineCount() > 0)
					m_DelayArrBtn.SetColors(RGB(255,128, 0), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
				else
					m_DelayArrBtn.SetColors(::GetSysColor(COLOR_BTNFACE), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
				m_DelayArrBtn.UpdateWindow();

				//Removing departure tab
				for(long j = 0; j < tabDelayDeparture.GetLineCount(); j++)
				{
					CString tmp = tabDelayDeparture.GetFieldValue(j, "URNO");
					if(tabDelayDeparture.GetFieldValue(j, "URNO") == llUrno)
					{
						tabDelayDeparture.DeleteLine(j);
						break;
					}
				}

				tabDelayDeparture.RedrawTab();
				tabDelayDeparture.Refresh();

				CString oldepText = "Delays (" + GetDelayCount(&tabDelayDeparture) + ")";
		
				m_DelayDepBtn.SetWindowText(oldepText);

				if (m_CB_Ok.m_hWnd)
					m_DelayDepBtn.SetFont(m_CB_Ok.GetFont());

				if(tabDelayDeparture.GetLineCount() > 0)
					m_DelayDepBtn.SetColors(RGB(255,128, 0), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
				else
					m_DelayDepBtn.SetColors(::GetSysColor(COLOR_BTNFACE), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
				m_DelayDepBtn.UpdateWindow();
			
			}

		  

	 }


	if(strcmp(pS->Cmd, "DRT") == 0)
	{
		//Added by Christine
		//ResetTabs();
	
	}
	

	if(pTab != NULL)
	{

		if(strcmp(pS->Cmd, "IRT") == 0)
		{
			bool blFound = false;
			//This BC comes twice ?? Der Mist ist echt passiert und zwar dauernd. MWO!
			for(long i = 0; i < pTab->GetLineCount(); i++)
			{
				if(pTab->GetFieldValue(i, "URNO") == olDCFUrno)
				{
					pTab->SetFieldValues(i, pS->Fields, pS->Data);
					blFound = true;
					break;
				}
			}
			if(blFound == false)
			{
				CString strValues;
				int ilCnt = GetItemCount(pS->Fields, ',');
				for(int i = 0; i < ilCnt-1; i++)
				{
					strValues+=",";
				}
				pTab->InsertTextLine(strValues, false);
				int ilIdx = pTab->GetLineCount()-1;
				if(ilIdx >= 0)
				{
					pTab->SetFieldValues(ilIdx, pS->Fields, pS->Data);
				}
			}
		}
		else if(strcmp(pS->Cmd, "URT") == 0)
		{
			for(long i = 0; i < pTab->GetLineCount(); i++)
			{
				if(pTab->GetFieldValue(i, "URNO") == olDCFUrno)
				{
					pTab->SetFieldValues(i, pS->Fields, pS->Data);
					break;
				}
			}
		}
		else if(strcmp(pS->Cmd, "DRT") == 0)
		{
			for(long i = 0; i < pTab->GetLineCount(); i++)
			{
				if(pTab->GetFieldValue(i, "URNO") == olDCFUrno)
				{
					pTab->DeleteLine(i);
					break;
				}
			}
		}
	}
	
	

	if(polButton != NULL && pTab != NULL)
	{
		char pclText[20]="";
		sprintf(pclText, "%ld", pTab->GetLineCount());
		CString olText = "Delays (" + GetDelayCount(pTab) + ")";
		polButton->SetWindowText(olText);
		if(pTab->GetLineCount() > 0)
			polButton->SetColors(RGB(255,128, 0), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
		else
			polButton->SetColors(::GetSysColor(COLOR_BTNFACE), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
		polButton->UpdateWindow();
		
		pTab->RedrawTab();
		pTab->Refresh();

		if (pomMultiDelayDlg && pomMultiDelayDlg->bmOpen)
		{
			if(prmAFlight->Urno == ilFURN)
				OnDelaybtnArrival();

			if(prmDFlight->Urno == ilFURN)
				OnDelaybtnDeparture();
		}
	}
}

void RotationDlg::ProcessFpeChange()
{
	SetArrPermitsButton();
	SetDepPermitsButton();
}

void RotationDlg::ProcessCCA(CCADATA *prpCca, int ipDDXType)
{
	if(prmDFlight->Urno <= 0) 
		return;
		

	/*
	long llBaseID = atol(prpBc->Tws);

	if(llBaseID == IDM_COMMONCCA)
		return;
	


	int i;
	CCADATA *prlCca;
	CedaCcaData olData;

	prlCca = new CCADATA;
	olData.GetRecordFromItemList(prlCca,prpBc->Fields,prpBc->Data);
	
	if(prlCca->Urno == 0)
	{
		CString olSelection = CString(prpBc->Selection);

		if(olSelection.Find("URNO") >= 0)
		{
			if (olSelection.Find('\'') != -1)
			{
				prlCca->Urno = ogSeasonDlgFlights.GetUrnoFromSelection(prpBc->Selection);
			}
			else
			{
				int ilFirst = olSelection.Find("=")+1;
				int ilLast  = olSelection.GetLength();
				prlCca->Urno = atol(olSelection.Mid(ilFirst,ilLast-ilFirst));
			}
		}
	}	

	if(((prlCca->Urno == 0) || (prlCca->Flnu != prmDFlight->Urno )) && (ipDDXType != BC_CCA_DELETE) )
	{
		delete prlCca;
		return;
	}	

	if(bmLocalTime)	omCcaData.StructUtcToLocal((void*)prlCca);

	omDCinsSave.DeleteAll();

	for (i = omDCins.GetSize() - 1; i >= 0 ; i--)
	{
		if((omDCins[i].Urno == 0) || ( (ipDDXType == BC_CCA_DELETE)  && (omDCins[i].Urno == prlCca->Urno)))
			omDCins.DeleteAt(i);
	}

	if(ipDDXType != BC_CCA_DELETE)
	{
		bool blInsert = false;

		for (i = omDCins.GetSize() - 1; i >= 0 ; i--)
		{
			if(omDCins[i].Urno == prlCca->Urno)
			{
				if((prlCca->Ckbs == TIMENULL) && (strcmp(prlCca->Ckic, "") == 0))
				{
					omDCins.DeleteAt(i);
				}
				else
				{
					*(&omDCins[i]) = *prlCca;
				}
				blInsert = true;
				break;
			}
		}

		if(!blInsert)
		{
			omDCins.New(*prlCca);

		}

	}
	delete prlCca;

	for (i = 0; i < omDCins.GetSize() ; i++)
	{
		omDCinsSave.New(omDCins[i]);
	}

  */

	if (ipDDXType == CCA_ROTDLG_NEW || ipDDXType == CCA_ROTDLG_DELETE)
	{
		if (ipDDXType == CCA_ROTDLG_DELETE)
		{
			// delete the record in omCcaData at first
			omCcaData.DeleteInternal(prpCca, false);

		}

		CCAFLIGHTDATA rlCcaFlight;

//		if(bmLocalTime) ogRotationDlgFlights.StructLocalToUtc((void*)prmDFlight);
//		rlCcaFlight = *prmDFlight;
 //		if(bmLocalTime) ogRotationDlgFlights.StructUtcToLocal((void*)prmDFlight);
 		if(bmLocalTime) ConvertStructLocalTOUtc(NULL, prmDFlight);
		rlCcaFlight = *prmDFlight;
 		if(bmLocalTime) ConvertStructUtcTOLocal(NULL, prmDFlight);

		omCcaData.CheckDemands(&rlCcaFlight);
	}

	GetCcas();
	DShowCinsTable();
	DShowChaTable();


/*

	CStringArray olData;
	CStringArray olFields;
	bool blReload = false;	

	long llUrno = 0;
	long llFlnu = 0;

	int ilFieldCount = ExtractItemList(CString(prpBc->Fields), &olFields);
	int ilDataCount = ExtractItemList(CString(prpBc->Data), &olData);


	if(ilFieldCount != ilDataCount)
		return;


	for(int i = ilFieldCount - 1; i >= 0; i--)
	{

		if(olFields[i] == "FLNU")
		{
			llFlnu = atol(olData[i]);
			if(llFlnu == prmDFlight->Urno)
				blReload = true;
		}
		if(olFields[i] == "URNO")
		{
			llUrno = atol(olData[i]);
		}
	}



	CString olSelection = CString(prpBc->Selection);

	if(olSelection.Find("URNO") >= 0)
	{
		if (olSelection.Find('\'') != -1)
		{
			llUrno = ogRotationFlights.GetUrnoFromSelection(prpBc->Selection);
		}
		else
		{
			int ilFirst = olSelection.Find("=")+1;
			int ilLast  = olSelection.GetLength();
			llUrno = atol(olSelection.Mid(ilFirst,ilLast-ilFirst));
		}
	}



	if(!blReload && (llUrno > 0))
	{

		for (i = 0; i < omDCins.GetSize(); i++)
		{
			if(omDCins[i].Urno == llUrno)
				blReload = true;
		}
	}


	if(blReload)
	{
		omDCins.DeleteAll();
		omDCinsSave.DeleteAll();
		
		if(prmDFlight->Urno > 0)
		{
			CedaCcaData olData;
			char pclWhere[128];
			sprintf(pclWhere,"WHERE FLNU = %d", prmDFlight->Urno);
			olData.ReadSpecial(omDCins, pclWhere);
			for(int i = 0; i < omDCins.GetSize(); i++)
			{
				omDCinsSave.New(omDCins[i]);
			}
		}

		DShowCinsTable();

	}

*/

}

void RotationDlg::ReadFlzData()
{
	if (prmDFlight->Urno == 0)
		return;
	CString olWhere;

	char buffer[12];

	/*
	omFlzData.Register();
	CString olFlnu;
	olFlnu.Format("%ld", prmDFlight->Urno);
	omCcaData.SetFilter(false, olFlnu);
	*/


	for(int i = 0; i < omCcaData.omData.GetSize(); i++)
	{
		if(omCcaData.omData[i].Urno > 0)
		{
			ltoa(omCcaData.omData[i].Urno , buffer, 10);
			olWhere += CString(buffer) + CString(",");
		}
	}

	if(olWhere.IsEmpty())
		olWhere.Format("%d", prmDFlight->Urno);
	else
	{
		ltoa(prmDFlight->Urno , buffer, 10);
		olWhere += buffer;
	}


	omFlzData.Read(olWhere);

}





void RotationDlg::ReadCcaData()
{
	if (prmDFlight->Urno == 0)
		return;

	CString olWhere;
	olWhere.Format("FLNU = %d", prmDFlight->Urno);
	omCcaData.ReadSpecial(olWhere);

	CCAFLIGHTDATA rlCcaFlight;
/*
	if(bmLocalTime) ogRotationDlgFlights.StructLocalToUtc((void*)prmDFlight);
	rlCcaFlight = *prmDFlight;
 	if(bmLocalTime) ogRotationDlgFlights.StructUtcToLocal((void*)prmDFlight);
*/
 	if(bmLocalTime) ConvertStructLocalTOUtc(NULL, prmDFlight);
	rlCcaFlight = *prmDFlight;
 	if(bmLocalTime) ConvertStructUtcTOLocal(NULL, prmDFlight);

	omCcaData.CheckDemands(&rlCcaFlight);

	GetCcas();
}



void RotationDlg::ReadChaData()
{
	if (prmDFlight->Urno == 0)
		return;

	CString olWhere;
	olWhere.Format("WHERE RURN = %d", prmDFlight->Urno);
	omChaData.Read(olWhere.GetBuffer(0) , true, false);

}






void RotationDlg::GetCcas()
{

	if(prmDFlight->Urno <= 0)
		return;


	omDCins.DeleteAll();
	for(int i = 0; i < omCcaData.omData.GetSize(); i++)
	{
		omDCins.New(omCcaData.omData[i]);
	}
	omDCins.Sort(CompareCcas);	

	// create 'save'-records
	omDCinsSave.DeleteAll();
	for(i = 0; i < omDCins.GetSize(); i++)
	{
		omDCinsSave.New(omDCins[i]);
	}


}


void RotationDlg::DShowCinsTable()
{
	CCSEDIT_ATTRIB rlAttribC1;
	CCSEDIT_ATTRIB rlAttribC2;
	CCSEDIT_ATTRIB rlAttribC3;
	CCSEDIT_ATTRIB rlAttribC4;
	CCSEDIT_ATTRIB rlAttribC5;

	CCSPtrArray<TABLE_COLUMN> olColList;
	TABLE_COLUMN rlColumnData;

	int ilLc ;
	
	
	pomDCinsTable->ResetContent();


	rlAttribC1.Style = ES_UPPERCASE | WS_TABSTOP;


	rlAttribC2.Format = "X";
	rlAttribC2.TextMinLenght = 0;
	rlAttribC2.TextMaxLenght = 1;
	rlAttribC2.Style = ES_UPPERCASE | WS_TABSTOP;

	rlAttribC3.Type = KT_TIME;
	rlAttribC3.ChangeDay	= true;
	rlAttribC3.TextMaxLenght = 7;
	rlAttribC3.Style =  WS_TABSTOP;

	rlAttribC4.Type = KT_TIME;
	rlAttribC4.ChangeDay	= true;
	rlAttribC4.TextMaxLenght = 7;
	rlAttribC4.Style =  WS_TABSTOP;

	//For PRF 8174
	rlAttribC5.Type = KT_STRING;
	rlAttribC5.ChangeDay = true;
	rlAttribC5.TextMaxLenght = 60;
	rlAttribC5.Style =  WS_TABSTOP;


	rlColumnData.Font = &ogMS_Sans_Serif_8;
	rlColumnData.Alignment = COLALIGN_LEFT;

	if(ogPrivList.GetStat("ROTATIONDLG_CE_DCins") != '1')
		rlColumnData.BkColor = RGB(192,192,192);

	olCicCoIxs.SetSize(MAX_CIC_TABLE_LINES);

	CTime olTmpTime;
	for (ilLc = 0; ilLc < omDCins.GetSize(); ilLc++)
	{
		rlColumnData.blIsEditable = true;	
		rlColumnData.Text = omDCins[ilLc].Ckic;
		rlColumnData.EditAttrib = rlAttribC1;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		if (bgCnamAtr)
		{
			CString olAtr("");
			CString olnam = omDCins[ilLc].Ckic;
			ogBCD.GetField("CIC", "CNAM", omDCins[ilLc].Ckic, "CATR", olAtr );
			rlColumnData.Text = olAtr;
			rlColumnData.EditAttrib = rlAttribC1;
			rlColumnData.blIsEditable = false;
			olColList.NewAt(olColList.GetSize(), rlColumnData);
		}

		rlColumnData.blIsEditable = false;
		rlColumnData.Text = omDCins[ilLc].Ckit;
		rlColumnData.EditAttrib = rlAttribC2;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.blIsEditable = true;
		olTmpTime = omDCins[ilLc].Ckbs;
		if(bmLocalTime) ogBasicData.UtcToLocal(olTmpTime);
		rlColumnData.Text = DateToHourDivString(olTmpTime, prmDFlight->Stod);//omDCins[ilLc].Ckbs.Format("%H:%M");
		rlColumnData.EditAttrib = rlAttribC3;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		
		olTmpTime = omDCins[ilLc].Ckes;
		if(bmLocalTime) ogBasicData.UtcToLocal(olTmpTime);
		rlColumnData.Text = DateToHourDivString(olTmpTime, prmDFlight->Stod);//omDCins[ilLc].Ckes.Format("%H:%M");
		rlColumnData.EditAttrib = rlAttribC4;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		
		olTmpTime = omDCins[ilLc].Ckba;
		if(bmLocalTime) ogBasicData.UtcToLocal(olTmpTime);
		rlColumnData.Text = DateToHourDivString(olTmpTime, prmDFlight->Stod);//omDCins[ilLc].Ckbs.Format("%H:%M");
		rlColumnData.EditAttrib = rlAttribC3;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		
		olTmpTime = omDCins[ilLc].Ckea;
		if(bmLocalTime) ogBasicData.UtcToLocal(olTmpTime);
		rlColumnData.Text = DateToHourDivString(olTmpTime, prmDFlight->Stod);//omDCins[ilLc].Ckes.Format("%H:%M");
		rlColumnData.EditAttrib = rlAttribC4;
		if(bgRotationmask==true) 
		{
		if(prmDFlight->Airb!=TIMENULL)
		 {  	
			 rlColumnData.blIsEditable = false;
		 }
		}
		 olColList.NewAt(olColList.GetSize(), rlColumnData);
		
		
		rlColumnData.Text = omDCins[ilLc].Disp;//omDCins[ilLc].Ckes.Format("%H:%M");
		
		/* RST 8493
		if(bgcheckingRemarkEdit)
			rlColumnData.EditAttrib = rlAttribC5;
		else
			rlColumnData.EditAttrib = rlAttribC4;
		*/

		// RST 8493
		rlColumnData.EditAttrib = rlAttribC5;


		rlColumnData.blIsEditable = true;

		olColList.NewAt(olColList.GetSize(), rlColumnData);

		

		if (bgFIDSLogo)
		{
			rlColumnData.Text = omFlzData.GetCicLogoName(omDCins[ilLc].Urno);
			rlColumnData.EditAttrib = rlAttribC5;
			rlColumnData.blIsEditable = false;
			olColList.NewAt(olColList.GetSize(), rlColumnData);
		}


		pomDCinsTable->AddTextLine(olColList, (void*)(NULL));
		olColList.DeleteAll();

		// set code share index array
		olCicCoIxs.SetAt(ilLc, omDCins[ilLc].Coix);
	}

	for (ilLc = omDCins.GetSize(); ilLc < MAX_CIC_TABLE_LINES; ilLc++)
	{
		rlColumnData.blIsEditable = true;	
		rlColumnData.Text = "";
		rlColumnData.EditAttrib = rlAttribC1;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = "";
		if (bgCnamAtr)
		{
			rlColumnData.EditAttrib = rlAttribC2;
		    olColList.NewAt(olColList.GetSize(), rlColumnData);
		}
		rlColumnData.EditAttrib = rlAttribC2;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = "";
		rlColumnData.EditAttrib = rlAttribC3;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = "";
		rlColumnData.EditAttrib = rlAttribC4;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = "";
		rlColumnData.EditAttrib = rlAttribC3;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = "";
		rlColumnData.EditAttrib = rlAttribC4;
		if(bgRotationmask==true) 
		{
		if(prmDFlight->Airb!=TIMENULL)
		 {  	
			 rlColumnData.blIsEditable = false;
		 }		 
		}
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = "";

		if(bgcheckingRemarkEdit)
			rlColumnData.EditAttrib = rlAttribC5;
		else
			rlColumnData.EditAttrib = rlAttribC4;
		
//		rlColumnData.blIsEditable = false;	
		rlColumnData.blIsEditable = true;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		
		if (bgFIDSLogo)
		{
			rlColumnData.blIsEditable = false;	
			rlColumnData.EditAttrib = rlAttribC5;
		    olColList.NewAt(olColList.GetSize(), rlColumnData);
		}
		
		
		pomDCinsTable->AddTextLine(olColList, (void*)(NULL));
		olColList.DeleteAll();

		// set code share index array
		olCicCoIxs.SetAt(ilLc, 0);	
	}
	pomDCinsTable->DisplayTable();
}



///////////////////////////////////////////////////////////////////////////////////////////////////////
//  weiter Flugnummer - Ankunft
//
void RotationDlg::OnAshowjfno() 
{

	if(!bmIsAFfnoShown)
	{
		char clStat = ogPrivList.GetStat("ROTATIONDLG_CB_AShowJfno");
		m_CB_AShowJfno.ShowWindow(SW_HIDE);
		m_CE_AJfnoBorder.ShowWindow(SW_SHOW);
		m_CB_AAlc3List2.ShowWindow(SW_SHOW);
		m_CB_AAlc3List3.ShowWindow(SW_SHOW);
	    pomAJfnoTable->ShowWindow(SW_SHOW);
		SetpWndStatAll(ogPrivList.GetStat("ROTATIONDLG_CB_AShowJfno"),pomAJfnoTable);
		if(clStat == '1')
			pomAJfnoTable->MakeInplaceEdit(0, 0);
	}
	bmIsAFfnoShown = true;



}


void RotationDlg::AShowJfnoButton() 
{
	m_CB_AShowJfno.ShowWindow(SW_SHOW);
	m_CE_AJfnoBorder.ShowWindow(SW_HIDE);
	m_CB_AAlc3List2.ShowWindow(SW_HIDE);
	m_CB_AAlc3List3.ShowWindow(SW_HIDE);
    pomAJfnoTable->ShowWindow(SW_HIDE);
	bmIsAFfnoShown = false;

}



///////////////////////////////////////////////////////////////////////////////////////////////////////
//  weiter Flugnummer - Abflug
//
void RotationDlg::OnDshowjfno() 
{
	if(!bmIsDFfnoShown)
	{

		char clStat = ogPrivList.GetStat("ROTATIONDLG_CB_DShowJfno");

		m_CB_DShowJfno.ShowWindow(SW_HIDE);

		m_CE_DJfnoBorder.ShowWindow(SW_SHOW);
		m_CB_DAlc3List2.ShowWindow(SW_SHOW);
		m_CB_DAlc3List3.ShowWindow(SW_SHOW);
	    pomDJfnoTable->ShowWindow(SW_SHOW);
		SetpWndStatAll(ogPrivList.GetStat("ROTATIONDLG_CB_DShowJfno"),pomDJfnoTable);
		if(clStat == '1')
			pomDJfnoTable->MakeInplaceEdit(0, 0);



	}
	bmIsDFfnoShown = true;
}


void RotationDlg::DShowJfnoButton() 
{
	m_CB_DShowJfno.ShowWindow(SW_SHOW);

	m_CE_DJfnoBorder.ShowWindow(SW_HIDE);
	m_CB_DAlc3List2.ShowWindow(SW_HIDE);
	m_CB_DAlc3List3.ShowWindow(SW_HIDE);

    pomDJfnoTable->ShowWindow(SW_HIDE);
	bmIsDFfnoShown = false;

}




void RotationDlg::SetSecState()
{
	if (strcmp(pcgHome, "ATH") == 0)
		m_CB_GoAround.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_GoAround"));

	m_CB_DRetTaxi.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_DRetTaxi"));	
	m_CB_DRetFlight.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_DRetFlight"));	
	m_CB_ARetTaxi.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_ARetTaxi"));	
	m_CB_ARetFlight.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_ARetFlight"));	

	m_CE_ADivr.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_ADiverted"));
	//m_CE_DDivr.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_DDiverted"));
	m_CE_DDivr.SetSecState('-');

	m_CB_Ok.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_Ok"));			
	
	m_CB_Agents.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_Agents"));
	m_CB_CkeckList_Arr.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_CLP_ARR"));
	m_CB_CkeckList_Dep.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_CLP_DEP"));
	m_CB_Object.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_Object"));
	
	m_CB_Split.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_Teilen"));		
	m_CB_Join.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_Verbinden"));	

	m_CB_VipADlg.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_VIP"));
	m_CB_VipDDlg.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_VIP"));

	m_CB_Towing.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_Towing"));
	m_CB_SeasonMask.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_SEASONMASK"));
	m_CB_Paid.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_Paid"));
	m_CE_Ming.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_Ming"));	
	m_CE_Regn.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_Regn"));	
	m_CE_Act3.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_Act3"));	
	m_CE_Act5.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_Act5"));	
	m_CB_Act35List.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_Act35List"));	

	SetWndStatAll(ogPrivList.GetStat("ROTATIONDLG_CL_ARemp"), m_CL_ARemp);
	SetWndStatAll(ogPrivList.GetStat("ROTATIONDLG_CL_DRemp"), m_CL_DRemp);		


	if(bgRotationDlgLock)
	{
		if(ogPrivList.GetStat("ROTATIONDLG_CB_Lock") =='1')
			m_CB_Lock.ShowWindow(SW_SHOW);
		else
			m_CB_Lock.ShowWindow(SW_HIDE);

	}
	else
	{
		m_CB_Lock.ShowWindow(SW_HIDE);
	}

	m_CB_AVip.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_AVip"));
	m_CB_AFpsa.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_AFpsa"));

	m_CE_AAirb.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_AAirb"));
	m_CB_AExt2List.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_AExt2List"));	
	m_CB_AExt1List.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_AExt1List"));	
	m_CB_APstaList.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_APstaList"));	
	m_CB_ATelex.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_ATelex"));		
	m_CB_AStypList.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_AStypList"));	
	m_CB_ADcd1List.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_ADcd1List"));	
	m_CB_ADcd2List.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_ADcd2List"));	
	m_CB_ARerouted.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_ARerouted"));	
	m_CB_AAlc3List.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_AAlc3List"));	
	m_CB_AAlc3List2.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_AAlc3List23"));	
	m_CB_AAlc3List3.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_AAlc3List23"));	
	m_CB_ADiverted.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_ADiverted"));	
	m_CB_ACxx.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_ACxx"));
	m_CB_ABlt1List.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_ABlt1List"));	
	m_CB_AHtypList.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_AHtypList"));
	m_CB_ABlt2List.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_ABlt2List"));	
	m_CB_AGta2List.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_AGta2List"));	
	m_CB_AGta1List.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_AGta1List"));	
	m_CB_AOrg3List.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_AOrg3List"));	
	m_CB_ATtypList.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_ATtypList"));	
	m_CE_AOfbl.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_AOfbl"));		
//	m_CE_APaba.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_APaba"));		
	m_CE_AEtdi.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_AEtdi"));		
	m_CE_ARwya.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_ARwya"));		
	m_CE_AAlc3.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_AAlc3"));		
//	m_CE_APaea.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_APaea"));		
	m_CE_ATet2.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_ATet2"));		
	m_CE_ATet1.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_ATet1"));		
	m_CE_AOrg3.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_AOrg3"));		
	m_CE_AOrg3L.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_AOrg3"));		
	m_CE_ADes3.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_ADes3"));		
	m_CE_ATtyp.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_ATtyp"));		
	m_CE_ATmoa.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_ATmoa"));		
	m_CE_ATmb2.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_ATmb2"));		
	m_CE_ATmb1.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_ATmb1"));		
	m_CE_ATga2.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_ATga2"));		
	m_CE_ATga1.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_ATga1"));		
	m_CE_AStyp.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_AStyp"));		
	m_CE_AStod.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_AStod"));		
	m_CE_AStoa.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_AStoa"));		
	m_CE_AStev.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_AStev"));		
	m_CE_ASte2.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_ASte2"));		
	m_CE_ASte3.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_ASte3"));		
	m_CE_ASte4.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_ASte4"));		
	m_CE_ARem1.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_ARem1"));		
	m_CE_APsta.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_APsta"));		
	m_CE_AOnbl.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_AOnbl"));		
	m_CE_AOnbe.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_AOnbe"));		
	m_CE_AOfbl.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_AOfbl"));		
	m_CE_ANxti.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_ANxti"));		
	m_CE_ALand.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_ALand"));		
	m_CE_AIfra.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_AIfra"));		
	m_CE_AHtyp.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_AHtyp"));		
	m_CE_AGta2.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_AGta2"));		
	m_CE_AGta1.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_AGta1"));		
	m_CE_AGa2y.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_AGa2y"));		
	m_CE_AGa1y.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_AGa1y"));		
	m_CE_AGa2x.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_AGa2x"));		
	m_CE_AGa1x.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_AGa1x"));		
	m_CE_AExt2.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_AExt2"));		
	m_CE_AExt1.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_AExt1"));		
	m_CE_AEtoa.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_AEtoa"));		
	m_CE_AEtai.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_AEtai"));		
	m_CE_ADtd2.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_ADtd2"));		
	m_CE_ADtd1.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_ADtd1"));		
	m_CE_ADcd2.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_ADcd2"));		
	m_CE_ADcd1.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_ADcd1"));		
	m_CE_ABlt2.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_ABlt2"));		
	m_CE_ABlt1.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_ABlt1"));	
	m_CE_AB2ea.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_AB2ea"));		
	m_CE_AB2ba.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_AB2ba"));		
	m_CE_AB1ea.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_AB1ea"));		
	m_CE_AB1ba.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_AB1ba"));	
	if (bgUseDepBelts) {
		m_CB_DBlt1List.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_DBlt1List"));	
		m_CE_DBlt1.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_DBlt1"));	
		m_CE_DTmb1.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_DTmb1"));
		m_CE_DB1ea.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_DB1ea"));		
		m_CE_DB1ba.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_DB1ba"));	

	} else {
		CWnd* polWnd = GetDlgItem(IDC_DBLT1); 
		if (polWnd)
			polWnd->ShowWindow(SW_HIDE);
		polWnd = GetDlgItem(IDC_DTMB1); 
		if (polWnd)
			polWnd->ShowWindow(SW_HIDE);
		polWnd = GetDlgItem(IDC_DB1EA); 
		if (polWnd)
			polWnd->ShowWindow(SW_HIDE);
		polWnd = GetDlgItem(IDC_DB1BA); 
		if (polWnd)
			polWnd->ShowWindow(SW_HIDE);
		polWnd = GetDlgItem(IDC_DBLTLABEL); 
		if (polWnd)
			polWnd->ShowWindow(SW_HIDE);
		polWnd = GetDlgItem(IDC_DBLT1LIST); 
		if (polWnd)
			polWnd->ShowWindow(SW_HIDE);
		
	}
	m_CE_AFlns.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_AFlns"));		
	m_CE_AFltn.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_AFltn"));		
	m_CE_AFlti.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_AFlti"));		
	m_CE_ACsgn.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_ACsgn"));		
	m_CE_ABbaa.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_ABbaa"));		
	m_CE_DBbfa.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_DBbfa"));		
	m_CE_ALastChange.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_ALastChange"));	
	m_CE_ALastChangeTime.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_ALastChangeTime"));	

	if(ogPrivList.GetStat("ROTATIONDLG_CL_AHistory") == '-')
	{
		CWnd* polWnd = GetDlgItem(IDC_STATIC_AHISTORY); 
		if (polWnd)
			polWnd->ShowWindow(SW_HIDE);
	}

	if(ogPrivList.GetStat("ROTATIONDLG_CL_DHistory") == '-')
	{
		CWnd* polWnd = GetDlgItem(IDC_STATIC_DHISTORY); 
		if (polWnd)
			polWnd->ShowWindow(SW_HIDE);
	}

	
	m_CB_DTtypList.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_DTtypList"));	
	m_CB_DDcd1List.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_DDcd1List"));	
	m_CB_DCinsList1.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_DCinsList"));	
	m_CB_DCinsList2.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_DCinsList"));	
	m_CB_DCinsList3.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_DCinsList"));	
	m_CB_DCinsList4.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_DCinsList"));	

	m_CB_DVip.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_DVip"));
	m_CB_DFpsd.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_DFpsd"));


	if(bgAdditionalRemark)
	{
		m_CE_ARem3.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_ARem3"));		
		m_CE_DRem3.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_DRem3"));		

		CRect olRect;
		m_CE_ARem1.GetWindowRect( olRect );
		ScreenToClient(olRect);
		olRect.right = olRect.left + ((olRect.right - olRect.left)/2);
		m_CE_ARem1.MoveWindow(olRect);

		m_CE_DRem1.GetWindowRect( olRect );
		ScreenToClient(olRect);
		olRect.right = olRect.left + ((olRect.right - olRect.left)/2);
		m_CE_DRem1.MoveWindow(olRect);
	}
	else
	{
		m_CE_ARem3.ShowWindow(SW_HIDE);		
		m_CE_DRem3.ShowWindow(SW_HIDE);	
	}


	if(bgFIDSLogo)
	{
		m_CB_DCicLogo1.SetSecState(ogPrivList.GetStat("FIDS_CKI_LOGO"));	
		m_CB_DCicLogo2.SetSecState(ogPrivList.GetStat("FIDS_CKI_LOGO"));	
		m_CB_DCicLogo3.SetSecState(ogPrivList.GetStat("FIDS_CKI_LOGO"));	
		m_CB_DCicLogo4.SetSecState(ogPrivList.GetStat("FIDS_CKI_LOGO"));	
	}
	else
	{
		m_CB_DCicLogo1.ShowWindow(SW_HIDE);	
		m_CB_DCicLogo2.ShowWindow(SW_HIDE);	
		m_CB_DCicLogo3.ShowWindow(SW_HIDE);	
		m_CB_DCicLogo4.ShowWindow(SW_HIDE);	
	}


	if(bgChuteDisplay)
	{
		if(ogPrivList.GetStat("ROTATIONDLG_CE_DChutes") == '-')
		{
			pomDChuTable->ShowWindow(SW_HIDE);
			m_CE_ChuFrTo.ShowWindow(SW_HIDE);	
			CWnd* polWnd = GetDlgItem(IDC_LB_CHUTE); 
			if (polWnd)
				polWnd->ShowWindow(SW_HIDE);
			polWnd = GetDlgItem(IDC_DCHUBORDER); 
			if (polWnd)
				polWnd->ShowWindow(SW_HIDE);
		}
	}
	else
	{
		CWnd* polWnd = GetDlgItem(IDC_LB_CHUTE); 
		if (polWnd)
			polWnd->ShowWindow(SW_HIDE);
		
		polWnd = GetDlgItem(IDC_DCHUBORDER); 
		if (polWnd)
			polWnd->ShowWindow(SW_HIDE);

		m_CE_ChuFrTo.ShowWindow(SW_HIDE);	
		pomDChuTable->ShowWindow(SW_HIDE);
	}





	if(ogPrivList.GetStat("ROTATIONDLG_CB_DCinsList") == '-')
	{
			m_ComboDcins.ShowWindow(SW_HIDE);
	}
	else if(ogPrivList.GetStat("SEASONDLG_CB_DCinsList") == '0')
	{
			m_ComboDcins.EnableWindow(FALSE);
	}
	if (ogFactor_CCA.IsEmpty())
	{
			m_ComboDcins.ShowWindow(SW_HIDE);
	}


	m_CB_DCicRem1.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_DCicRem"));	
	m_CB_DCicRem2.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_DCicRem"));	
	m_CB_DCicRem3.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_DCicRem"));	
	m_CB_DCicRem4.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_DCicRem"));	
	

	m_CB_DCicCodeShare1.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_DCicCodeShare"));	
	m_CB_DCicCodeShare2.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_DCicCodeShare"));	
	m_CB_DCicCodeShare3.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_DCicCodeShare"));	
	m_CB_DCicCodeShare4.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_DCicCodeShare"));	
	

	m_CB_DPstdList.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_DPstdList"));	
	m_CB_DWro1List.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_DWro1List"));	
	m_CB_DHtypList.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_DHtypList"));	
	m_CB_DTelex.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_DTelex"));	
	m_CB_DStypList.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_DStypList"));	
	m_CB_DGta1List.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_DGta1List"));	
	m_CB_DGta2List.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_DGta2List"));	
	m_CB_DDes3List.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_DDes3List"));	
	m_CB_DDcd2List.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_DDcd2List"));	
	m_CB_DAlc3List.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_DAlc3List"));	
	m_CB_DAlc3List2.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_DAlc3List23"));	
	m_CB_DAlc3List3.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_DAlc3List23"));	
	m_CB_DCxx.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_DCxx"));			
	//m_CB_DDiverted.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_DDiverted"));	
	//m_CB_DRerouted.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_DRerouted"));	
	m_CB_DDiverted.SetSecState('-');	
	m_CB_DRerouted.SetSecState('-');	
	m_CE_DGd2x.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_DGd2x"));		
	m_CE_DFltn.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_DFltn"));		
	m_CE_DFlti.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_DFlti"));		
	m_CE_DOrg3.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_DOrg3"));		
	m_CE_DW1ea.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_DW1ea"));		
	m_CE_DW3ea.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_DW3ea"));		
	m_CE_DW1ba.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_DW1ba"));		
	m_CE_DW3ba.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_DW3ba"));		
//	m_CE_DPdea.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_DPdea"));		
//	m_CE_DPdba.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_DPdba"));		
	m_CE_DWro1.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_DWro1"));		
	m_CE_DWro3.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_DWro3"));		
	m_CE_DTwr1.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_DTwr1"));		
	m_CE_DTwr3.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_DTwr3"));		
	m_CE_DTtyp.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_DTtyp"));		
	m_CE_DTgd2.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_DTgd2"));		
	m_CE_DTgd1.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_DTgd1"));		
	m_CE_DStyp.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_DStyp"));		
	m_CE_DStod.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_DStod"));		
	m_CE_DStoa.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_DStoa"));		
	m_CE_DStev.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_DStev"));		
	m_CE_DSte2.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_DSte2"));		
	m_CE_DSte3.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_DSte3"));		
	m_CE_DSte4.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_DSte4"));		
	m_CE_DSlot.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_DSlot"));		
	m_CE_DRwyd.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_DRwyd"));		
	m_CE_DRem1.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_DRem1"));		
	m_CE_DPstd.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_DPstd"));		
	m_CE_DOfbl.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_DOfbl"));		
	m_CE_DNxti.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_DNxti"));		
	m_CE_DIskd.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_DIskd"));		

	if(bgCDMPhase1)
	{
		m_CE_DIskd.ShowWindow(SW_HIDE);

		m_CE_DTobt.ShowWindow(SW_SHOW);
		m_CE_DTsat.ShowWindow(SW_SHOW);

//		m_CE_DTobt:EnableWindow(FALSE);
		m_CE_DTsat.EnableWindow(FALSE);

	}
	else
	{
		m_CE_DTobt.ShowWindow(SW_HIDE);
		m_CE_DTsat.ShowWindow(SW_HIDE);
	}


	m_CE_DIfrd.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_DIfrd"));		
	m_CE_DHtyp.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_DHtyp"));		
	m_CE_DGtd2.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_DGtd2"));		
	m_CE_DGtd1.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_DGtd1"));		
	m_CE_DGd2y.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_DGd2y"));		
	m_CE_DGd1y.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_DGd1y"));		
	m_CE_DGd1x.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_DGd1x"));		
	m_CE_DFlns.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_DFlns"));		
	m_CE_DEtod.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_DEtod"));		
	m_CE_DEtdi.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_DEtdi"));		
	m_CE_DEtdc.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_DEtdc"));		
	m_CE_DDtd2.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_DDtd2"));		
	m_CE_DDtd1.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_DDtd1"));		
	m_CE_DDes3.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_DDes3"));		
	m_CE_DDes4.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_DDes4"));		
	m_CE_DDcd2.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_DDcd2"));		
	m_CE_DDcd1.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_DDcd1"));		
	m_CE_DAlc3.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_DAlc3"));		
	m_CE_DAirb.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_DAirb"));		
	m_CE_DCsgn.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_DCsgn"));		
	m_CE_DLastChange.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_DLastChange"));	
	m_CE_DLastChangeTime.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_DLastChangeTime"));	


	m_CE_DBaz1.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_DBaz1"));
	m_CE_DBaz4.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_DBaz4"));
	m_CE_DBao1.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_DBao1"));
	m_CE_DBac1.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_DBac1"));
	m_CE_DBao4.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_DBao4"));
	m_CE_DBac4.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_DBac4"));
	m_CB_DGd2d.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_DGd2d"));


	m_CB_Load_View_Arr.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_ALoadView"));	
	m_CB_Load_View_Dep.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_DLoadView"));	


	//if(ogPrivList.GetStat("ROTATIONDLG_CB_AShowJfno") == '-')
	//	m_CB_AShowJfno.SetSecState('-');
	m_CB_AShowJfno.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_AShowJfno"));	// TLE: UFIS 3768 


	//if(ogPrivList.GetStat("ROTATIONDLG_CB_DShowJfno") == '-')
	//	m_CB_DShowJfno.SetSecState('-');
	m_CB_DShowJfno.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_DShowJfno"));	// TLE: UFIS 3768 


	m_CB_ALoad.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_Load"));
	m_CB_DLoad.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_Load"));

/*
	if(ogPrivList.GetStat("ROTATIONDLG_CE_Load") == '-')
	{
		m_CB_ALoad.SetSecState('-');
		m_CB_DLoad.SetSecState('-');
	}
	else
	{
		m_CB_ALoad.SetSecState('1');
		m_CB_DLoad.SetSecState('1');
	}
*/
/*
	if(ogPrivList.GetStat("ROTATIONDLG_CE_Log") == '1')
	{
		m_CB_ALog.SetSecState('1');
		m_CB_DLog.SetSecState('1');
	}
	else
	{
		m_CB_ALog.SetSecState('-');
		m_CB_DLog.SetSecState('-');
	}
*/
	m_CB_ALog.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_Log"));	
	m_CB_DLog.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_Log"));	

	//if(ogPrivList.GetStat("ROTATIONDLG_CB_ARep") == '-')
	m_CB_ARep.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_ARep"));	

	//if(ogPrivList.GetStat("ROTATIONDLG_CB_DRep") == '-')
	m_CB_DRep.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_DRep"));	

	//if(ogPrivList.GetStat("ROTATIONDLG_CB_DDelay") == '-')
	m_CB_DDelay.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_DDelay"));	

	//if(ogPrivList.GetStat("ROTATIONDLG_CB_REQ") == '-')
	m_CB_REQ.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_REQ"));	

	CWnd* polWnd = GetDlgItem(IDC_ADELAY); 
	if (polWnd)
	{
		if(ogPrivList.GetStat("ROTATIONDLG_CB_ADelay") == '-')
		{
			polWnd->ShowWindow(FALSE);
		}
		else if(ogPrivList.GetStat("ROTATIONDLG_CB_ADelay") == '0')
		{
			polWnd->ShowWindow(TRUE);
			polWnd->EnableWindow(FALSE);
		}
	}


	if(ogPrivList.GetStat("ROTATIONDLG_CE_Gpu") == '-')
	{
		m_CB_AGpu.SetSecState('-');
		m_CB_DGpu.SetSecState('-');
	}
	else
	{
		m_CB_AGpu.SetSecState('1');
		m_CB_DGpu.SetSecState('1');
	}

	m_CB_APca.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_Pca"));
	m_CB_APlb.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_Plb"));
	m_CB_DPca.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_Pca"));
	m_CB_DPlb.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_Plb"));


	if (!bgFlightIDEditable)
	{
		// enable editing of flight id
//		m_CE_DFlti.SetSecState('0');
//		m_CE_AFlti.SetSecState('0');
	}

	//hide flightreport if no ceda.ini section
	char pclConfigPath[256];
	char pclExcelPath[256];

	if (getenv("CEDA") == NULL)
		strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
	else
		strcpy(pclConfigPath, getenv("CEDA"));

	GetPrivateProfileString(ogAppName, "FLIGHTREPORTVIEWER", "DEFAULT",
		pclExcelPath, sizeof pclExcelPath, pclConfigPath);

	if(!strcmp(pclExcelPath, "DEFAULT"))
	{
		m_CB_ARep.SetSecState('-');
		m_CB_DRep.SetSecState('-');
		m_CB_REQ.SetSecState('-');
		m_CB_DDelay.SetSecState('-');
		if (strcmp(pcgHome, "PVG") == 0)
			m_CB_DDelay.SetSecState('-');
	}


	//hide CHECK_LIST_PROCESSING if no ceda.ini section
	GetPrivateProfileString(ogAppName, "CHECK_LIST_PROCESSING", "DEFAULT",
		pclExcelPath, sizeof pclExcelPath, pclConfigPath);

	if(!strcmp(pclExcelPath, "DEFAULT"))
	{
		m_CB_CkeckList_Arr.SetSecState('-');
		m_CB_CkeckList_Dep.SetSecState('-');
	}

	//hide LOGTABVIEWER if no ceda.ini section
	GetPrivateProfileString(ogAppName, "LOGTABVIEWER", "DEFAULT",
		pclExcelPath, sizeof pclExcelPath, pclConfigPath);

	if(!strcmp(pclExcelPath, "DEFAULT"))
	{
		m_CB_ALog.SetSecState('-');
		m_CB_DLog.SetSecState('-');
	}

	//hide LOATABVIEWER if no ceda.ini section
	GetPrivateProfileString(ogAppName, "LOATABVIEWER", "DEFAULT",
		pclExcelPath, sizeof pclExcelPath, pclConfigPath);

	if(!strcmp(pclExcelPath, "DEFAULT"))
	{
		m_CB_Load_View_Arr.SetSecState('-');
		m_CB_Load_View_Dep.SetSecState('-');
	}

	if(bgFIDSLogo)
	{
		SetWndStatAll(ogPrivList.GetStat("FIDS_GAT_LOGO"), m_CL_DGatLogo);		
		SetWndStatAll(ogPrivList.GetStat("FIDS_GAT_LOGO"), m_CL_DGatLogo2);		
	}
	else
	{
		SetWndStatAll('-', m_CL_DGatLogo);		
		SetWndStatAll('-', m_CL_DGatLogo2);		
	}


		if(ogPrivList.GetStat("ROTATIONDLG_CE_ACxxReason") == '-' || !bgCxxReason)
		{
				m_CL_ACxxReason.ShowWindow(SW_HIDE);
		}
		else if(ogPrivList.GetStat("ROTATIONDLG_CE_ACxxReason") == '0')
		{
				m_CL_ACxxReason.EnableWindow(FALSE);
		}

		if(ogPrivList.GetStat("ROTATIONDLG_CE_DCxxReason") == '-' || !bgCxxReason)
		{
				m_CL_DCxxReason.ShowWindow(SW_HIDE);
		}
		else if(ogPrivList.GetStat("ROTATIONDLG_CE_DCxxReason") == '0')
		{
				m_CL_DCxxReason.EnableWindow(FALSE);
		}


		if(bgNightFlightSlot)
		{
			m_CE_ANfes.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_ANfes"));
			m_CB_ANfes.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_ANfes"));
			m_CE_DNfes.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_DNfes"));
			m_CB_DNfes.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_DNfes"));
		}
		else
		{
			m_CE_ANfes.ShowWindow(SW_HIDE);
			m_CB_ANfes.ShowWindow(SW_HIDE);
			m_CE_DNfes.ShowWindow(SW_HIDE);
			m_CB_DNfes.ShowWindow(SW_HIDE);
		}



}





void RotationDlg::EnableArrival()
{
	BOOL blEnable = FALSE;

	if(prmAFlight->Urno > 0)  
		blEnable = TRUE;

	if (bmRotationReadOnly)
		blEnable = FALSE;

	if(!blEnable)
	{
		AShowJfnoButton();
	}

	if( (CString(prmAFlight->Ftyp)	== "Z" || CString(prmAFlight->Ftyp)	== "B" ) && strcmp(pcgHome, "ATH") != 0)
	{
		if (polRotationAViaDlg)
			polRotationAViaDlg->Enable(false);

		m_CE_ARem1.EnableWindow(FALSE);
	}
	else
	{
		if (polRotationAViaDlg)
			polRotationAViaDlg->Enable(blEnable);

		m_CE_ARem1.EnableWindow(blEnable);
	}

/*

	pomAViaCtrl->Enable(blEnable);


	if( CString(prmAFlight->Ftyp)	== "Z" || CString(prmAFlight->Ftyp)	== "B")
	{
		pomAViaCtrl->Enable(FALSE);
		m_CE_ARem1.EnableWindow(FALSE);
	}
	else
	{
		pomAViaCtrl->Enable(blEnable);
		m_CE_ARem1.EnableWindow(blEnable);
	}
*/

	if( blEnable == TRUE && CString(prmAFlight->Ftyp)	== "D")
	{
		m_CE_ADivr.EnableWindow(TRUE);
	}
	else
	{
		m_CE_ADivr.EnableWindow(FALSE);
	}

	char clStat = ogPrivList.GetStat("ROTATIONDLG_CL_ARemp");

	m_CL_ARemp.ShowWindow(SW_SHOW);

	if(clStat == '1')
	{
		m_CL_ARemp.EnableWindow(blEnable);
	}
	else
	{
		if(clStat == '0')
		{
			m_CL_ARemp.EnableWindow(FALSE);
		}
		else
		{
			m_CL_ARemp.ShowWindow(SW_HIDE);
		}

	}

	//----------
	clStat = ogPrivList.GetStat("ROTATIONDLG_CL_AHistory");

	m_ComboAHistory.ShowWindow(SW_SHOW);

	if(clStat == '1')
	{
		m_ComboAHistory.EnableWindow(blEnable);
	}
	else
	{
		if(clStat == '0')
		{
			m_ComboAHistory.EnableWindow(FALSE);
		}
		else
		{
			m_ComboAHistory.ShowWindow(SW_HIDE);
		}
	}
	//----------


	m_CB_ALoad.EnableWindow(blEnable);
	m_CB_ALog.EnableWindow(blEnable);
	m_CB_ARep.EnableWindow(blEnable);
	m_CB_AGpu.EnableWindow(blEnable);
	m_CB_APca.EnableWindow(blEnable);
	m_CB_APlb.EnableWindow(blEnable);

	m_CB_CkeckList_Arr.EnableWindow(blEnable);

	if(blEnable)
	{
		CString olPst = CString(prmAFlight->Psta);
		if(!olPst.IsEmpty())
		{
			CString olGpuStat = ogBCD.GetField("PST", "PNAM", olPst, "GPUS");

			if(olGpuStat != "X")
				m_CB_AGpu.EnableWindow(FALSE);

		}
		else
		{
			m_CB_AGpu.EnableWindow(FALSE);
		}
	}


	m_CB_AVip.EnableWindow(blEnable);

	if(bgDailyFixedRes)
	{
		m_CB_AFpsa.EnableWindow(FALSE);
	}
	else
	{
		m_CB_AFpsa.EnableWindow(blEnable);
	}

	m_CB_ARetFlight.EnableWindow(blEnable);
	m_CB_ARetTaxi.EnableWindow(blEnable);

	m_CB_VipADlg.EnableWindow(blEnable);	

	m_CB_Load_View_Arr.EnableWindow(blEnable);

	m_CB_AExt1List.EnableWindow(blEnable);
	m_CB_AExt2List.EnableWindow(blEnable);
	m_CB_APstaList.EnableWindow(blEnable);
	m_CB_ATtypList.EnableWindow(blEnable);

//	m_CB_DStodCalc.EnableWindow(blEnable);
	m_CB_AAlc3List3.EnableWindow(blEnable);
	m_CB_AAlc3List2.EnableWindow(blEnable);
	m_CS_Ankunft.EnableWindow(blEnable);
	m_CE_APsta.EnableWindow(blEnable);
	m_CE_AAlc3.EnableWindow(blEnable);
	m_CE_ATet2.EnableWindow(blEnable);
	m_CE_ATet1.EnableWindow(blEnable);
	m_CE_AOrg3.EnableWindow(blEnable);
	m_CE_AOrg3L.EnableWindow(blEnable);
	m_CE_ADes3.EnableWindow(blEnable);
	m_CE_ATtyp.EnableWindow(blEnable);
	m_CE_ATmoa.EnableWindow(blEnable);
	m_CE_ATmb2.EnableWindow(blEnable);
	m_CE_ATmb1.EnableWindow(blEnable);
	m_CE_ATga2.EnableWindow(blEnable);
	m_CE_ATga1.EnableWindow(blEnable);
	m_CB_ATelex.EnableWindow(blEnable);
	m_CB_AStypList.EnableWindow(blEnable);
	m_CE_AStyp.EnableWindow(blEnable);
	m_CE_AStod.EnableWindow(blEnable);
	m_CE_AStoa.EnableWindow(blEnable);
	m_CE_AStev.EnableWindow(blEnable);
	if (bgUseAddKeyfields)
	{
		m_CE_ASte2.ShowWindow(SW_SHOW);
		m_CE_ASte3.ShowWindow(SW_SHOW);
		m_CE_ASte4.ShowWindow(SW_SHOW);
		m_CE_ASte2.EnableWindow(blEnable);
		m_CE_ASte3.EnableWindow(blEnable);
		m_CE_ASte4.EnableWindow(blEnable);
	} else
	{
		m_CE_ASte2.ShowWindow(SW_HIDE);
		m_CE_ASte3.ShowWindow(SW_HIDE);
		m_CE_ASte4.ShowWindow(SW_HIDE);
	}
	m_CB_AShowJfno.EnableWindow(blEnable);
	m_CB_ARerouted.EnableWindow(blEnable);
	m_CE_APsta.EnableWindow(blEnable);
	m_CB_AOrg3List.EnableWindow(blEnable);
	m_CE_AOnbl.EnableWindow(blEnable);
	m_CE_AOfbl.EnableWindow(blEnable);
	m_CE_ANxti.EnableWindow(blEnable);
	m_CE_ALastChange.EnableWindow(blEnable);
	m_CE_ALastChangeTime.EnableWindow(blEnable);
	m_CE_ALand.EnableWindow(blEnable);
	m_CE_AIfra.EnableWindow(blEnable);
	m_CB_AHtypList.EnableWindow(blEnable);
	m_CE_AHtyp.EnableWindow(blEnable);
	m_CB_AGta2List.EnableWindow(blEnable);
	m_CB_AGta1List.EnableWindow(blEnable);
	m_CE_AGta2.EnableWindow(blEnable);
	m_CE_AGta1.EnableWindow(blEnable);
	m_CE_AGa2y.EnableWindow(blEnable);
	m_CE_AGa1y.EnableWindow(blEnable);
	m_CE_ABbaa.EnableWindow(blEnable);
	m_CE_AGa2x.EnableWindow(blEnable);
	m_CE_AGa1x.EnableWindow(blEnable);
	m_CE_AExt2.EnableWindow(blEnable);
	m_CE_AExt1.EnableWindow(blEnable);
	m_CE_AEtoa.EnableWindow(blEnable);
	m_CE_AEtai.EnableWindow(blEnable);
	m_CE_ADtd2.EnableWindow(blEnable);
	m_CE_ADtd1.EnableWindow(blEnable);
	m_CB_ADiverted.EnableWindow(blEnable);
	m_CB_ADcd2List.EnableWindow(blEnable);
	m_CE_ADcd2.EnableWindow(blEnable);
	m_CB_ADcd1List.EnableWindow(blEnable);
	m_CE_ADcd1.EnableWindow(blEnable);
	m_CB_ACxx.EnableWindow(blEnable);
	m_CB_ABlt2List.EnableWindow(blEnable);
	m_CE_ABlt2.EnableWindow(blEnable);
	m_CB_ABlt1List.EnableWindow(blEnable);
	m_CE_ABlt1.EnableWindow(blEnable);
	m_CE_AB2ea.EnableWindow(blEnable);
	m_CE_AB2ba.EnableWindow(blEnable);
	m_CE_AB1ea.EnableWindow(blEnable);
	m_CE_AB1ba.EnableWindow(blEnable);

	
	m_CE_AFlns.EnableWindow(blEnable);
	m_CE_AFltn.EnableWindow(blEnable);
	m_CE_AFlti.EnableWindow(blEnable);
	m_CB_AAlc3List.EnableWindow(blEnable);
	m_CE_AAirb.EnableWindow(blEnable);
	m_CE_ARwya.EnableWindow(blEnable);
	m_CE_ACsgn.EnableWindow(blEnable);

//	m_CE_APaba.ShowWindow(SW_HIDE);
	m_CE_AEtdi.ShowWindow(SW_SHOW);
	m_CE_AEtdi.EnableWindow(blEnable);
//	m_CE_APaea.ShowWindow(SW_HIDE);
	//prf 8176	
	

	m_CB_ANfes.EnableWindow(blEnable);

	if(bgAdditionalRemark)
	{
		m_CE_ARem3.EnableWindow(blEnable);
	}


	m_CB_GoAround.EnableWindow(blEnable);



	if(bgRotationmask==true) 
	{
	  if(prmAFlight->Land!=TIMENULL)
	  {
	  m_CB_ACxx.EnableWindow(FALSE);
	  }
	}

	if(ogFpeData.bmFlightPermitsEnabled && pomArrPermitsButton != NULL)
		pomArrPermitsButton->ShowWindow(blEnable ? SW_SHOW : SW_HIDE);


	m_CE_AJfnoBorder.EnableWindow(FALSE);

//	if(omAJfno.GetSize() > 0)
	if(bmIsAFfnoShown && omAJfno.GetSize() > 0)
		OnAshowjfno();
	else
		AShowJfnoButton();

	m_CB_AShowJfno.EnableWindow(blEnable);



	if( CString(prmAFlight->Ftyp)	== "Z" )
	{
		m_CE_AAirb.EnableWindow(FALSE);	
		m_CE_AOfbl.EnableWindow(FALSE);	
	}


	if( CString(prmAFlight->Ftyp)	== "B" )
	{
		m_CE_ALand.ShowWindow(SW_HIDE);	
		m_CE_AAirb.ShowWindow(SW_HIDE);	
		m_CE_AOfbl.EnableWindow(FALSE);	
	}
	else
	{  
		m_CE_ALand.ShowWindow(SW_SHOW);	
		m_CE_AAirb.ShowWindow(SW_SHOW);	
	}


	if(( CString(prmAFlight->Ftyp)	== "Z" ) || (CString(prmAFlight->Ftyp)	== "B" ))
	{
		m_CE_AOrg3.EnableWindow(FALSE);	
		m_CE_AOrg3L.EnableWindow(FALSE);	
		m_CB_AOrg3List.EnableWindow(FALSE);	
	}
	else
	{
		m_CE_AOrg3.EnableWindow(blEnable);	
		m_CE_AOrg3L.EnableWindow(blEnable);	
		m_CB_AOrg3List.EnableWindow(blEnable);	
	}


	if(prmAFlight->Airb == TIMENULL || prmAFlight->Ofbl == TIMENULL)
	{
		m_CB_ARetFlight.EnableWindow(FALSE);
	}
	else
	{
		m_CB_ARetFlight.EnableWindow(blEnable);
	}

/*
	if (CString(prmAFlight->Adid) == "B")
	{
		if(prmAFlight->Ofbl == TIMENULL)
		{
			m_CB_ARetTaxi.EnableWindow(FALSE);
		}
		else
		{
			m_CB_ARetTaxi.EnableWindow(blEnable);
			if(prmAFlight->Airb != TIMENULL)
				m_CB_ARetTaxi.EnableWindow(FALSE);
		}
	}
	else
	{
		m_CB_ARetTaxi.EnableWindow(FALSE);
		m_CB_ARetFlight.EnableWindow(FALSE);
	}
*/


//##
	if (CString(prmAFlight->Adid) == "B" && CString(prmAFlight->Ftyp) != "O")
	{

		if(prmAFlight->Ofbl == TIMENULL)
		{
			m_CB_ARetTaxi.EnableWindow(FALSE);
		}
		else
		{
			m_CB_ARetTaxi.EnableWindow(blEnable);
			if(prmAFlight->Airb != TIMENULL && CString(prmAFlight->Ftyp) != "B")
				m_CB_ARetTaxi.EnableWindow(FALSE);

			if(prmAFlight->Airb != TIMENULL && CString(prmAFlight->Ftyp) == "B")
				m_CB_ARetFlight.EnableWindow(FALSE);
		}
	}
	else
	{
		m_CB_ARetTaxi.EnableWindow(FALSE);
		m_CB_ARetFlight.EnableWindow(FALSE);
	}
//###

	if(bgShowChock)
	{
		pomCEChocksOn->EnableWindow(blEnable);
	}

	if(bgDailyFixedRes)
	{
		pomCBFPSA->EnableWindow(blEnable);
		pomCBFGA1->EnableWindow(blEnable);
		pomCBFGA2->EnableWindow(blEnable);
		pomCBFBL1->EnableWindow(blEnable);
		pomCBFBL2->EnableWindow(blEnable);
	}

	if(bgShowCashButtonInBothFlight)
	{
		m_CashDepBtn.EnableWindow(blEnable);
	}

}



void RotationDlg::EnableGlobal()
{
	BOOL blEnable = FALSE;

	if((prmAFlight->Urno > 0) || (prmDFlight->Urno > 0))
		blEnable = TRUE;

	if (bmRotationReadOnly)
		blEnable = FALSE;

	m_CE_Act5.EnableWindow(blEnable);
	m_CE_Act3.EnableWindow(blEnable);
	m_CE_Regn.EnableWindow(blEnable);
	m_CB_Join.EnableWindow(blEnable);
	m_CE_Ming.EnableWindow(blEnable);
	m_CB_Split.EnableWindow(blEnable);
//	m_CB_Rotation.EnableWindow(blEnable);
	m_CB_Ok.EnableWindow(blEnable);

	//only aktivate towing if there is an arr
	if(prmAFlight->Urno == 0)
		m_CB_Towing.EnableWindow(FALSE);
	else
		m_CB_Towing.EnableWindow(blEnable);

	//m_CB_Paid.EnableWindow(blEnable);
	//m_CB_Object.EnableWindow(blEnable);
	//m_CB_Status.EnableWindow(blEnable);

	// additional Text for caption
	CString olTimes;
	if (bmLocalTime)
		olTimes = GetString(IDS_STRING1921);
	else
		olTimes = GetString(IDS_STRING1920);
	CString olCaption = GetString(IDS_STRING1974);
	olCaption += " ("+olTimes+")";
	SetWindowText(olCaption);

}



void RotationDlg::EnableDeparture()
{
	
	BOOL blEnable = FALSE;

	if(prmDFlight->Urno > 0) 
		blEnable = TRUE;

	if (bmRotationReadOnly)
		blEnable = FALSE;

	if(blEnable)
	{
		pomDCinsTable->ShowWindow(SW_SHOW);
		SetpWndStatAll(ogPrivList.GetStat("ROTATIONDLG_CE_DCins"),pomDCinsTable);
	}
	else
	{
		pomDCinsTable->ShowWindow(SW_HIDE);
	}


	if(bgFIDSLogo)
	{
		SetWndStatAll(ogPrivList.GetStat("FIDS_GAT_LOGO"), m_CL_DGatLogo);		
		SetWndStatAll(ogPrivList.GetStat("FIDS_GAT_LOGO"), m_CL_DGatLogo2);	
		
		m_CL_DGatLogo.EnableWindow(blEnable);
		m_CL_DGatLogo2.EnableWindow(blEnable);

		m_CL_DGatLogo.SetDroppedWidth( 400 );	
		m_CL_DGatLogo2.SetDroppedWidth( 400 );	
	
	}
	else
	{
		SetWndStatAll('-', m_CL_DGatLogo);		
		SetWndStatAll('-', m_CL_DGatLogo2);		
	}




	if(bgChuteDisplay)
	{
		if(ogPrivList.GetStat("ROTATIONDLG_CE_DChutes") == '-')
		{
			pomDChuTable->ShowWindow(SW_HIDE);
			m_CE_ChuFrTo.ShowWindow(SW_HIDE);	
			CWnd* polWnd = GetDlgItem(IDC_LB_CHUTE); 
			if (polWnd)
				polWnd->ShowWindow(SW_HIDE);
			polWnd = GetDlgItem(IDC_DCHUBORDER); 
			if (polWnd)
				polWnd->ShowWindow(SW_HIDE);
		}
		else
		{
			if(!blEnable)
				pomDChuTable->ShowWindow(SW_HIDE);
		}
	}
	else
	{
		CWnd* polWnd = GetDlgItem(IDC_LB_CHUTE); 
		if (polWnd)
			polWnd->ShowWindow(SW_HIDE);
		
		polWnd = GetDlgItem(IDC_DCHUBORDER); 
		if (polWnd)
			polWnd->ShowWindow(SW_HIDE);

		m_CE_ChuFrTo.ShowWindow(SW_HIDE);	
		pomDChuTable->ShowWindow(SW_HIDE);
	}






	char clStat = ogPrivList.GetStat("ROTATIONDLG_CL_DRemp");

	m_CL_DRemp.ShowWindow(SW_SHOW);

	if(clStat == '1')
	{
		m_CL_DRemp.EnableWindow(blEnable);
	}
	else
	{
		if(clStat == '0')
		{
			m_CL_DRemp.EnableWindow(FALSE);
		}
		else
		{
			m_CL_DRemp.ShowWindow(SW_HIDE);
		}

	}

	pomDRgd1->ShowWindow(SW_SHOW);
	pomDRgd2->ShowWindow(SW_SHOW);

	if(!bgGateFIDSRemark)
	{
		pomDRgd1->ShowWindow(SW_HIDE);
		pomDRgd2->ShowWindow(SW_HIDE);
	}
	else
	{
		char clStat = ogPrivList.GetStat("ROTATIONDLG_CB_Rgd1");
		switch (clStat)
		{
			case '1': pomDRgd1->EnableWindow(TRUE); break;
			case '0': pomDRgd1->EnableWindow(FALSE); break;
			case '-': pomDRgd1->ShowWindow(SW_HIDE); break;
		}
		clStat = ogPrivList.GetStat("ROTATIONDLG_CB_Rgd2");
		switch (clStat)
		{
			case '1': pomDRgd2->EnableWindow(TRUE); break;
			case '0': pomDRgd2->EnableWindow(FALSE); break;
			case '-': pomDRgd2->ShowWindow(SW_HIDE); break;
		}
	}



	//----------
	clStat = ogPrivList.GetStat("ROTATIONDLG_CL_DHistory");

	m_ComboDHistory.ShowWindow(SW_SHOW);

	if(clStat == '1')
	{
		m_ComboDHistory.EnableWindow(blEnable);
	}
	else
	{
		if(clStat == '0')
		{
			m_ComboDHistory.EnableWindow(FALSE);
		}
		else
		{
			m_ComboDHistory.ShowWindow(SW_HIDE);
		}
	}
	//----------
	clStat = ogPrivList.GetStat("ROTATIONDLG_CB_DCinsList");

	m_ComboDcins.ShowWindow(SW_SHOW);

	if(clStat == '1')
	{
		m_ComboDcins.EnableWindow(blEnable);
	}
	else
	{
		if(clStat == '0')
		{
			m_ComboDcins.EnableWindow(FALSE);
		}
		else
		{
			m_ComboDcins.ShowWindow(SW_HIDE);
		}
	}

	if (ogFactor_CCA.IsEmpty())
	{
		m_ComboDcins.ShowWindow(SW_HIDE);
	}
	//---------


	if( (CString(prmDFlight->Ftyp)	== "Z" || CString(prmDFlight->Ftyp)	== "B") && strcmp(pcgHome, "ATH") != 0)
	{
		if (polRotationDViaDlg)
			polRotationDViaDlg->Enable(false);

		m_CE_DRem1.EnableWindow(FALSE);
	}
	else
	{
		if (polRotationDViaDlg)
			polRotationDViaDlg->Enable(blEnable);

		m_CE_DRem1.EnableWindow(blEnable);
	}
/*

	if( CString(prmDFlight->Ftyp)	== "Z" || CString(prmDFlight->Ftyp)	== "B")
	{
		pomDViaCtrl->Enable(FALSE);
		m_CE_DRem1.EnableWindow(FALSE);
	}
	else
	{
		pomDViaCtrl->Enable(blEnable);
		m_CE_DRem1.EnableWindow(blEnable);
	}
*/
	if( blEnable == TRUE && CString(prmDFlight->Ftyp)	== "D")
	{
		m_CE_DDivr.EnableWindow(TRUE);
	}
	else
	{
		m_CE_DDivr.EnableWindow(FALSE);
	}


	m_CB_DVip.EnableWindow(blEnable);
	m_CB_DFpsd.EnableWindow(blEnable);

	m_CB_DLoad.EnableWindow(blEnable);
	m_CB_DLog.EnableWindow(blEnable);
	m_CB_DRep.EnableWindow(blEnable);
	m_CB_DDelay.EnableWindow(blEnable);
	m_CB_DGpu.EnableWindow(blEnable);
	m_CB_DPca.EnableWindow(blEnable);
	m_CB_DPlb.EnableWindow(blEnable);

	if(blEnable)
	{
		CString olPst = CString(prmDFlight->Pstd);
		if(!olPst.IsEmpty())
		{
			CString olGpuStat = ogBCD.GetField("PST", "PNAM", olPst, "GPUS");

			if(olGpuStat != "X")
				m_CB_DGpu.EnableWindow(FALSE);
		}
		else
		{
			m_CB_DGpu.EnableWindow(FALSE);
		}
	}
	m_CB_VipDDlg.EnableWindow(blEnable);	


	m_CB_CkeckList_Dep.EnableWindow(blEnable);
	m_CB_Load_View_Dep.EnableWindow(blEnable);

	m_CB_DRetFlight.EnableWindow(blEnable);
	m_CB_DRetTaxi.EnableWindow(blEnable);


	m_CE_DBaz1.EnableWindow(blEnable);
	m_CE_DBaz4.EnableWindow(blEnable);
	m_CE_DBao1.EnableWindow(blEnable);
	m_CE_DBac1.EnableWindow(blEnable);
	m_CE_DBao4.EnableWindow(blEnable);
	m_CE_DBac4.EnableWindow(blEnable);


	m_CB_DWro1List.EnableWindow(blEnable);
	m_CB_DPstdList.EnableWindow(blEnable);

//	m_CB_DStodCalc.EnableWindow(blEnable);
	m_CB_DCinsList1.EnableWindow(blEnable);
	m_CB_DCinsList2.EnableWindow(blEnable);
	m_CB_DCinsList3.EnableWindow(blEnable);
	m_CB_DCinsList4.EnableWindow(blEnable);

	m_CB_DCicRem1.EnableWindow(blEnable);
	m_CB_DCicRem2.EnableWindow(blEnable);
	m_CB_DCicRem3.EnableWindow(blEnable);
	m_CB_DCicRem4.EnableWindow(blEnable);

	m_CB_DCicCodeShare1.EnableWindow(blEnable);
	m_CB_DCicCodeShare2.EnableWindow(blEnable);
	m_CB_DCicCodeShare3.EnableWindow(blEnable);
	m_CB_DCicCodeShare4.EnableWindow(blEnable);

	m_CE_DOrg3.EnableWindow(blEnable);
	m_CE_DFltn.EnableWindow(blEnable);
	m_CE_DFlti.EnableWindow(blEnable);
	m_CE_DW1ea.EnableWindow(blEnable);
	m_CE_DW3ea.EnableWindow(blEnable);
	m_CE_DW1ba.EnableWindow(blEnable);
	m_CE_DW3ba.EnableWindow(blEnable);
	m_CS_Abflug.EnableWindow(blEnable);
	m_CB_DAlc3List3.EnableWindow(blEnable);
	m_CB_DAlc3List2.EnableWindow(blEnable);
	m_CE_DWro1.EnableWindow(blEnable);
	m_CE_DWro3.EnableWindow(blEnable);
	m_CE_DTwr1.EnableWindow(blEnable);
	m_CE_DTwr3.EnableWindow(blEnable);
	m_CB_DTtypList.EnableWindow(blEnable);
	m_CE_DTtyp.EnableWindow(blEnable);
	m_CE_DTgd2.EnableWindow(blEnable);
	m_CE_DTgd1.EnableWindow(blEnable);
	m_CB_DTelex.EnableWindow(blEnable);
	m_CB_DStypList.EnableWindow(blEnable);
	m_CE_DStyp.EnableWindow(blEnable);
	m_CE_DStod.EnableWindow(blEnable);
	m_CE_DStoa.EnableWindow(blEnable);
	m_CE_DStev.EnableWindow(blEnable);
	if (bgUseAddKeyfields) 
	{
		m_CE_DSte2.ShowWindow(SW_SHOW);
		m_CE_DSte3.ShowWindow(SW_SHOW);
		m_CE_DSte4.ShowWindow(SW_SHOW);
		m_CE_DSte2.EnableWindow(blEnable);
		m_CE_DSte3.EnableWindow(blEnable);
		m_CE_DSte4.EnableWindow(blEnable);
	} else
	{
		m_CE_DSte2.ShowWindow(SW_HIDE);
		m_CE_DSte3.ShowWindow(SW_HIDE);
		m_CE_DSte4.ShowWindow(SW_HIDE);
	}
	m_CE_DSlot.EnableWindow(blEnable);
	m_CB_DShowJfno.EnableWindow(blEnable);
	m_CE_DRwyd.EnableWindow(blEnable);
	m_CE_DPstd.EnableWindow(blEnable);
	m_CE_DBbfa.EnableWindow(blEnable);
	m_CE_DOfbl.EnableWindow(blEnable);
	m_CE_DNxti.EnableWindow(blEnable);
	m_CE_DLastChange.EnableWindow(blEnable);
	m_CE_DLastChangeTime.EnableWindow(blEnable);
	m_CE_DIskd.EnableWindow(blEnable);
	
	if(bgCDMPhase1)
	{
		m_CE_DIskd.EnableWindow(FALSE);
	}
	
	m_CE_DIfrd.EnableWindow(blEnable);
	m_CB_DHtypList.EnableWindow(blEnable);
	m_CE_DHtyp.EnableWindow(blEnable);
	m_CE_DGtd2.EnableWindow(blEnable);
	m_CE_DGtd1.EnableWindow(blEnable);
	m_CB_DGta1List.EnableWindow(blEnable);
	m_CB_DGta2List.EnableWindow(blEnable);
	m_CE_DGd2y.EnableWindow(blEnable);
	m_CE_DGd1y.EnableWindow(blEnable);
	m_CE_DGd1x.EnableWindow(blEnable);
	m_CE_DFlns.EnableWindow(blEnable);
	m_CE_DEtod.EnableWindow(blEnable);
	m_CE_DEtdi.EnableWindow(blEnable);
	m_CE_DEtdc.EnableWindow(blEnable);
	m_CE_DDtd2.EnableWindow(blEnable);
	m_CE_DDtd1.EnableWindow(blEnable);
	m_CB_DDes3List.EnableWindow(blEnable);
	m_CE_DDes3.EnableWindow(blEnable);
	m_CE_DDes4.EnableWindow(blEnable);
	m_CB_DDcd2List.EnableWindow(blEnable);
	m_CE_DDcd2.EnableWindow(blEnable);
	m_CB_DDcd1List.EnableWindow(blEnable);
	m_CE_DDcd1.EnableWindow(blEnable);
	m_CB_DCxx.EnableWindow(blEnable);
	m_CB_DAlc3List.EnableWindow(blEnable);
	m_CE_DAlc3.EnableWindow(blEnable);
	m_CE_DAirb.EnableWindow(blEnable);
	m_CE_DGd2x.EnableWindow(blEnable);
	m_CE_DCsgn.EnableWindow(blEnable);
	m_CB_DDiverted.EnableWindow(blEnable);
	m_CB_DRerouted.EnableWindow(blEnable);

	if(ogFpeData.bmFlightPermitsEnabled && pomDepPermitsButton != NULL)
		pomDepPermitsButton->ShowWindow(blEnable ? SW_SHOW : SW_HIDE);

//	m_CE_DPdea.ShowWindow(SW_HIDE);
//	m_CE_DPdba.ShowWindow(SW_HIDE);
	//prf 8176

	m_CB_DNfes.EnableWindow(blEnable);
	


	if (bgUseDepBelts)
	{
		CWnd* polWnd = GetDlgItem(IDC_DBLT1); 
		if (polWnd)
			polWnd->EnableWindow(blEnable);
		polWnd = GetDlgItem(IDC_DTMB1); 
		if (polWnd)
			polWnd->EnableWindow(blEnable);
		polWnd = GetDlgItem(IDC_DB1EA); 
		if (polWnd)
			polWnd->EnableWindow(blEnable);
		polWnd = GetDlgItem(IDC_DB1BA); 
		if (polWnd)
			polWnd->EnableWindow(blEnable);
		polWnd = GetDlgItem(IDC_DBLTLABEL); 
		if (polWnd)
			polWnd->EnableWindow(blEnable);
		polWnd = GetDlgItem(IDC_DBLT1LIST); 
		if (polWnd)
			polWnd->EnableWindow(blEnable);
		
	}






	if(bgAdditionalRemark)
	{
		m_CE_DRem3.EnableWindow(blEnable);
	}


  if(bgRotationmask==true) 
	{
 	 if(prmDFlight->Airb!=TIMENULL)
	 {  
	  m_CB_DCxx.EnableWindow(FALSE);
    
	 }
	}


	m_CE_DJfnoBorder.EnableWindow(FALSE);
	m_CE_DCinsBorder.EnableWindow(FALSE);



	if(bmIsDFfnoShown && omDJfno.GetSize() > 0)
//	if(omDJfno.GetSize() > 0)
		OnDshowjfno();
	else
		DShowJfnoButton();


	m_CB_DShowJfno.EnableWindow(blEnable);


	if(prmDFlight->Airb == TIMENULL || prmDFlight->Ofbl == TIMENULL)
	{
		m_CB_DRetFlight.EnableWindow(FALSE);
	}
	else
	{
		m_CB_DRetFlight.EnableWindow(blEnable);
	}


	if(prmDFlight->Ofbl == TIMENULL)
	{
		m_CB_DRetTaxi.EnableWindow(FALSE);
	}
	else
	{
/*		m_CB_DRetTaxi.EnableWindow(blEnable);
		if(prmDFlight->Airb != TIMENULL)
			m_CB_DRetTaxi.EnableWindow(FALSE);
*/
		m_CB_DRetTaxi.EnableWindow(blEnable);
		if(prmDFlight->Airb != TIMENULL && CString(prmDFlight->Ftyp) != "B")
			m_CB_DRetTaxi.EnableWindow(FALSE);

		if(prmDFlight->Airb != TIMENULL && CString(prmDFlight->Ftyp) == "B")
			m_CB_DRetFlight.EnableWindow(FALSE);
	}



	if( CString(prmDFlight->Ftyp)	== "Z" )
	{
		m_CE_DAirb.EnableWindow(FALSE);	
		m_CE_DOfbl.EnableWindow(FALSE);	
	}


	if( CString(prmDFlight->Ftyp)	== "B" )
	{
		m_CE_DAirb.ShowWindow(SW_HIDE);	
		m_CE_DOfbl.EnableWindow(FALSE);	
	}
	else
	{
		m_CE_DAirb.ShowWindow(SW_SHOW);	
	}


	if( CString(prmDFlight->Adid) == "B" && CString(prmDFlight->Ftyp)	== "O")
	{
		m_CB_DRetTaxi.EnableWindow(FALSE);
		m_CB_DRetFlight.EnableWindow(FALSE);
	}


	if(( CString(prmDFlight->Ftyp)	== "Z" ) || (CString(prmDFlight->Ftyp)	== "B" ))
	{
		m_CE_DDes3.EnableWindow(FALSE);	
		m_CE_DDes4.EnableWindow(FALSE);	
		m_CB_DDes3List.EnableWindow(FALSE);	
	}
	else
	{
		m_CE_DDes3.EnableWindow(blEnable);	
		m_CE_DDes4.EnableWindow(blEnable);	
		m_CB_DDes3List.EnableWindow(blEnable);	
	}

	m_CE_DCinsBorderExt.EnableWindow(FALSE);

//prf 8175 
/*	if(bgRotationmask==true) 
	{
 	  if(prmDFlight->Airb!=TIMENULL)
	  {
 
	     if(pomDCinsTable != NULL)
		 {
			int ilLines = pomDCinsTable->GetLinesCount();
			
			for( int i = 0; i < ilLines; i++)
			{
				if(bgCnamAtr)				
					bool test =	pomDCinsTable->SetColumnEditable(i,6,false);
				else
					bool test =	pomDCinsTable->SetColumnEditable(i,5,false);
			}
		 }
	  }

	} */  
	
	if (prmDFlight->Urno > 0) 
	{
		//if(strcmp(prmDFlight->Ftyp, "X") == 0) {
			EnableDepBelts(true);
		//} else {
		//	EnableDepBelts(false);
		//}
	}
	
	
	if(bgShowChock)
	{
		pomCEChocksOf->EnableWindow(blEnable);
	}

	if(bgDailyFixedRes)
	{
		pomCBFPSD->EnableWindow(blEnable);
		pomCBFGD1->EnableWindow(blEnable);
		pomCBFGD2->EnableWindow(blEnable);
	}

	if(bgShowCashButtonInBothFlight)
	{
		m_CashDepBtn.EnableWindow(blEnable);
	}

	//CWnd* polWnd = GetDlgItem(IDC_DCINSMAL4); 
	//polWnd->ShowWindow(SW_SHOW);	
	//polWnd->EnableWindow(TRUE);

}

void RotationDlg::SetWndPos()
{
	CRect olRect;

	GetWindowRect(&olRect);

	int ilWidth = olRect.right -olRect.left;
	int ilWhichMonitor = ogCfgData.GetMonitorForWindow(CString(MON_DAILYROTDLG_STRING));
	int ilMonitors = ogCfgData.GetMonitorForWindow(CString(MON_COUNT_STRING));
	int left, top, right, bottom;
	int ilCXMonitor;

	bottom = ::GetSystemMetrics(SM_CYSCREEN);

	top = ( (bottom - (olRect.bottom -olRect.top ) ) / 2);  


	if(ilWhichMonitor == 1)
	{
		switch(ilMonitors)
		{
		case 1:
			ilCXMonitor = ::GetSystemMetrics(SM_CXSCREEN);
			left = (int)((ilCXMonitor-ilWidth)/2);
			right = left + ilWidth;//GetSystemMetrics(SM_CXSCREEN);
			break;
		case 2:
			ilCXMonitor = (int)(::GetSystemMetrics(SM_CXSCREEN)/2);
			left = (int)((ilCXMonitor-ilWidth)/2);
			right = left + ilWidth;//GetSystemMetrics(SM_CXSCREEN);
			break;
		case 3:
			ilCXMonitor = (int)(::GetSystemMetrics(SM_CXSCREEN)/3);
			left = (int)((ilCXMonitor-ilWidth)/2);
			right = left + ilWidth;//GetSystemMetrics(SM_CXSCREEN);
			break;
		default:
			ilCXMonitor = ::GetSystemMetrics(SM_CXSCREEN);
			left = (int)((ilCXMonitor-ilWidth)/2);
			right = left + ilWidth;//GetSystemMetrics(SM_CXSCREEN);
			break;
		}
	}
	else if(ilWhichMonitor == 2)
	{
		switch(ilMonitors)
		{
		case 1:
			ilCXMonitor = ::GetSystemMetrics(SM_CXSCREEN);
			left = (int)((ilCXMonitor-ilWidth)/2);
			right = left + ilWidth;//GetSystemMetrics(SM_CXSCREEN);
			break;
		case 2:
			ilCXMonitor = (int)(::GetSystemMetrics(SM_CXSCREEN)/2);
			left = (int)((ilCXMonitor-ilWidth)/2)+(ilCXMonitor);
			right = left + ilWidth;//GetSystemMetrics(SM_CXSCREEN);
			break;
		case 3:
			ilCXMonitor = (int)(::GetSystemMetrics(SM_CXSCREEN)/3);
			left = (int)((ilCXMonitor-ilWidth)/2)+(ilCXMonitor);
			right = left + ilWidth;//GetSystemMetrics(SM_CXSCREEN);
			break;
		default:
			right = GetSystemMetrics(SM_CXSCREEN);
			break;
		}
	}
	else if(ilWhichMonitor == 3)
	{
		switch(ilMonitors)
		{
		case 1:
			ilCXMonitor = ::GetSystemMetrics(SM_CXSCREEN);
			left = (int)((ilCXMonitor-ilWidth)/2);
			right = left + ilWidth;//GetSystemMetrics(SM_CXSCREEN);
			break;
		case 2:
			ilCXMonitor = (int)(::GetSystemMetrics(SM_CXSCREEN)/2);
			left = (int)((ilCXMonitor-ilWidth)/2);
			right = left + ilWidth;//GetSystemMetrics(SM_CXSCREEN);
			break;
		case 3:
			ilCXMonitor = (int)(::GetSystemMetrics(SM_CXSCREEN)/3);
			left = (int)((ilCXMonitor-ilWidth)/2)+(2*ilCXMonitor);
			right = left + ilWidth;//GetSystemMetrics(SM_CXSCREEN);
			break;
		default:
			right = GetSystemMetrics(SM_CXSCREEN);
			break;
		}
	}



	//MoveWindow(CRect(left, top, right, bottom));
//	SetWindowPos(&wndTop,left , top, 0, 0, SWP_SHOWWINDOW | SWP_NOSIZE);
	SetWindowPos(&wndTop, 0,0,0,0, SWP_SHOWWINDOW | SWP_NOSIZE | SWP_NOMOVE);


}



void RotationDlg::OnAgent() 
{
	if(prmAFlight->Urno > 0 || prmDFlight->Urno > 0)
	{
		if(bgEnhancedHandlingAgentAssignment )
		{

			RotationHTADlg olRotationHTADlg(this, prmAFlight, prmDFlight);
			olRotationHTADlg.DoModal();

		}
		else
		{

			//alc3 der Fluggesellschaften
			CString opAlc3StrArr ("");
			CString opAlc3StrDep ("");

			if(prmAFlight != NULL)
				opAlc3StrArr.Format("%s", prmAFlight->Alc3);
			if(prmDFlight != NULL)
				opAlc3StrDep.Format("%s", prmDFlight->Alc3);

			if (opAlc3StrArr.GetLength() == 0 && opAlc3StrDep.GetLength() == 0)
				return;

			RotationHAIDlg polRotationHAIDlg(this, opAlc3StrArr, opAlc3StrDep);
			if (polRotationHAIDlg.DoModal() == IDOK)
			{
			}
		}
	}
}

void RotationDlg::OnObject() 
{
/*	if(prmAFlight->Urno >= 0)
		ogDdx.DataChanged((void *)this, SHOW_FLIGHT, (void*)&(prmAFlight->Urno));
	else
		ogDdx.DataChanged((void *)this, SHOW_FLIGHT, (void*)&(prmDFlight->Urno));
*/
	bool blResult = false;
	bool blResultTmp = false;

	long lpAUrno = -1;
	long lpDUrno = -1;

	if (prmAFlight)
		lpAUrno = prmAFlight->Urno;
	if (prmDFlight)
		lpDUrno = prmDFlight->Urno;


			if (pogBltDiagram != NULL)
			{
				if (lpAUrno != -1)
					blResult = pogBltDiagram->ShowFlight(lpAUrno);
			}
			if (pogGatDiagram != NULL)
			{
				if (lpAUrno != -1)
					blResult = pogGatDiagram->ShowFlight(lpAUrno, 'A');
				if (lpDUrno != -1)
					blResultTmp = pogGatDiagram->ShowFlight(lpDUrno, 'D');
				blResult = blResult || blResultTmp;
			}
			if (pogPosDiagram != NULL)
			{
				if (lpAUrno != -1) 
					blResult = pogPosDiagram->ShowFlight(lpAUrno, true);
				else if (lpDUrno != -1)
					blResult = pogPosDiagram->ShowFlight(lpDUrno, false);
			}
			if (pogWroDiagram != NULL)
			{
				if (lpDUrno != -1)
					blResult = pogWroDiagram->ShowFlight(lpDUrno);
			}
			if (pogFlightDiagram != NULL)
			{
				if (lpAUrno != -1) 
					pogFlightDiagram->ShowFlight(&lpAUrno);
				else if (lpDUrno != -1)
					pogFlightDiagram->ShowFlight(&lpDUrno);
			}
		/* TODO TVO
		case CCADIA:
			if (pogCcaDiagram != NULL)
			{
				if (lpAUrno != -1)
					blResult = pogCcaDiagram->ShowFlight(lpAUrno);
			}
		break;
		*/
			if (pogSeasonTableDlg != NULL)
			{
				pogSeasonTableDlg->DeselectAll();
				if (lpAUrno != -1) {
					blResult = pogSeasonTableDlg->ShowFlight(lpAUrno);
					pogSeasonTableDlg->SelectFlight(lpAUrno);
				}
				if (lpDUrno != -1)
				{
					blResultTmp = pogSeasonTableDlg->ShowFlight(lpDUrno);
					pogSeasonTableDlg->SelectFlight(lpDUrno);
				}
				blResult = blResult || blResultTmp;
			}
			if (pogRotationTables != NULL)
			{
				//pogRotationTables->DeselectAll();
				if (lpAUrno != -1) {
					blResult = pogRotationTables->ShowFlight(lpAUrno);
					pogRotationTables->SelectFlight(lpAUrno);
				}
				if (lpDUrno != -1)
				{
					blResultTmp = pogRotationTables->ShowFlight(lpDUrno);
					pogRotationTables->SelectFlight(lpDUrno);
				}
				blResult = blResult || blResultTmp;
			}
			if (pogDailyScheduleTableDlg != NULL)
			{
//				pogDailyScheduleTableDlg->DeselectAll();
				if (lpAUrno != -1) {
					blResult = pogDailyScheduleTableDlg->ShowFlight(lpAUrno);
					pogDailyScheduleTableDlg->SelectFlight(lpAUrno);
				}
				else if (lpDUrno != -1)
				{
					blResultTmp = pogDailyScheduleTableDlg->ShowFlight(lpDUrno);
					pogDailyScheduleTableDlg->SelectFlight(lpDUrno);
				}
				blResult = blResult || blResultTmp;
			}



}


void RotationDlg::OnCkeckList_Arr() 
{
	if (prmAFlight->Urno <= 0)
		return;

	char pclConfigPath[256];
	char pclExcelPath[256];

	if (getenv("CEDA") == NULL)
		strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
	else
		strcpy(pclConfigPath, getenv("CEDA"));

	GetPrivateProfileString(ogAppName, "CHECK_LIST_PROCESSING", "DEFAULT",
		pclExcelPath, sizeof pclExcelPath, pclConfigPath);


	if(!strcmp(pclExcelPath, "DEFAULT"))
		MessageBox(GetString(IDS_STRING2630), GetString(ST_FEHLER), MB_ICONERROR);


	CPoint point;
	::GetCursorPos(&point);

	CString olUser = ogBasicData.omUserID;
//	CString olPermits = ogPrivList.GetStat("ROTATIONDLG_CB_CLP_ARR");
	CString olPermits = "1;1;1";

	CString olTime = "U";
	if (bmLocalTime)
		olTime = "L";

	if (CheckPostFlight())
		olPermits = "0;0;1";

	char slRunTxt[512] = ""; 
	sprintf( slRunTxt,"%d,%s,%s,%s", prmAFlight->Urno, olUser, olPermits, olTime );

	char *args[4];
	args[0] = "child";
	args[1] = slRunTxt;
	args[2] = NULL;
	args[3] = NULL;
	int ilReturn = _spawnv(_P_NOWAIT,pclExcelPath,args);
}

void RotationDlg::OnCkeckList_Dep() 
{
	if (prmDFlight->Urno <= 0)
		return;

	char pclConfigPath[256];
	char pclExcelPath[256];

	if (getenv("CEDA") == NULL)
		strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
	else
		strcpy(pclConfigPath, getenv("CEDA"));

	GetPrivateProfileString(ogAppName, "CHECK_LIST_PROCESSING", "DEFAULT",
		pclExcelPath, sizeof pclExcelPath, pclConfigPath);


	if(!strcmp(pclExcelPath, "DEFAULT"))
		MessageBox(GetString(IDS_STRING2630), GetString(ST_FEHLER), MB_ICONERROR);


	CPoint point;
	::GetCursorPos(&point);

	CString olUser = ogBasicData.omUserID;
//	CString olPermits = ogPrivList.GetStat("ROTATIONDLG_CB_CLP_DEP");
	CString olPermits = "1;1;1";

	CString olTime = "U";
	if (bmLocalTime)
		olTime = "L";

	if (CheckPostFlight())
		olPermits = "0;0;1";

	char slRunTxt[512] = ""; 
	sprintf( slRunTxt,"%d,%s,%s,%s", prmDFlight->Urno, olUser, olPermits, olTime );

	char *args[4];
	args[0] = "child";
	args[1] = slRunTxt;
	args[2] = NULL;
	args[3] = NULL;
	int ilReturn = _spawnv(_P_NOWAIT,pclExcelPath,args);
}





void RotationDlg::OnTowing() 
{
	if(pogRotGroundDlg == NULL)
	{
		pogRotGroundDlg = new RotGroundDlg(this);
		pogRotGroundDlg->Create(IDD_ROTGROUND);
	}

	if (CheckPostFlight())
		pogRotGroundDlg->setStatus(RotGroundDlg::Status::POSTFLIGHT);
	else
		pogRotGroundDlg->setStatus(RotGroundDlg::Status::UNKNOWN);
	pogRotGroundDlg->SetWindowPos(&wndTopMost, 0,0,0,0, SWP_SHOWWINDOW | SWP_NOSIZE | SWP_NOMOVE );

	if(prmAFlight->Urno > 0)
	{
		pogRotGroundDlg->NewData(this, prmAFlight->Rkey, prmAFlight->Urno, 'A', bmLocalTime);
	}
	else
	{
		pogRotGroundDlg->NewData(this, prmDFlight->Rkey, prmDFlight->Urno, 'D', bmLocalTime);
	}

}

void RotationDlg::OnREQ() 
{
	char pclConfigPath[256];
	char pclExcelPath[256];

	if (getenv("CEDA") == NULL)
		strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
	else
		strcpy(pclConfigPath, getenv("CEDA"));

	GetPrivateProfileString(ogAppName, "FLIGHTREPORTVIEWER", "DEFAULT",
		pclExcelPath, sizeof pclExcelPath, pclConfigPath);


	if(!strcmp(pclExcelPath, "DEFAULT"))
		MessageBox(GetString(IDS_STRING2575), GetString(ST_FEHLER), MB_ICONERROR);


	CPoint point;
	::GetCursorPos(&point);

	CString olUser = ogBasicData.omUserID;
	CString olPermits = ogPrivList.GetStat("ROTATIONDLG_CB_REQ");

	CString olTime = "U";
	if (bmLocalTime)
		olTime = "L";

	if (CheckPostFlight())
		olPermits = "0";

	char slRunTxt[512] = ""; 
	sprintf( slRunTxt,"SPECIALREQ,%d,%d|%s,%s,%s|%d,%d", prmAFlight->Urno, prmDFlight->Urno, olUser, olPermits, olTime, point.x, point.y );

	char *args[4];
	args[0] = "child";
	args[1] = slRunTxt;
	args[2] = NULL;
	args[3] = NULL;
	int ilReturn = _spawnv(_P_NOWAIT,pclExcelPath,args);
}

CTime RotationDlg::ConvertFlightPermitTime(CTime opTime)
{
	if(bmLocalTime)
		CedaAptLocalUtc::AptLocalToUtc(opTime, CString(pcgHome4));
	return opTime;
}

//bool StartFlightPermitsAppl(CTime opTime, bool bpArrival, CString opRegn, CString opAlco, CString opFlno, CString opSufx, CString opCsgn="",CString opAct3="",CString opAct5="",CString opSDT="",CString opMode="0");
void RotationDlg::onArrPermitsButton() 
{
	CString olMode("0");

	CString olWhere = ""; //lgp
	CString olUrno = "";
	CString olPern = ""; 
	CString olStox = CTimeToDBString(ConvertFlightPermitTime(prmAFlight->Stoa), prmAFlight->Stoa);

	FPEDATA *prlFpe = NULL;
	if(bgShowADHOC || bgSeasonShowADHOC)
	{
		prlFpe = ogFpeData.GetBestPermitForFlight(ConvertFlightPermitTime(prmAFlight->Stoa), true, prmAFlight->Regn, prmAFlight->Alc3, prmAFlight->Fltn, prmAFlight->Flns,prmAFlight->Csgn, &omArrFlightPermitInfo);
		//if(omDepFlightPermitInfo != "No Flight Permit")
		if(prlFpe != NULL)
		{
			olUrno.Format("%ld", prlFpe->Urno);//lgptest
			olPern = prlFpe->Pern;
		}
	}
	else
	{
		prlFpe = ogFpeData.GetBestPermitForFlight(ConvertFlightPermitTime(prmAFlight->Stoa), true, prmAFlight->Regn, prmAFlight->Alc3, prmAFlight->Fltn, prmAFlight->Flns, &omArrFlightPermitInfo);
		//if(omDepFlightPermitInfo != "No Flight Permit")
		if(prlFpe != NULL)
		{
			olUrno.Format("%ld", prlFpe->Urno);//lgptest
			olPern = prlFpe->Pern;
		}
	}

	if(bgShowADHOC)
	{
		if(strcmp(prmAFlight->Adho,"X") == 0)
		{
			olMode="1";
		}
	}

	if(pomCBAAHOC->GetCheck() == TRUE)
		ogFpeData.StartFlightPermitsApplS(olPern, ConvertFlightPermitTime(prmAFlight->Stoa), true, prmAFlight->Regn, "", "", "",prmAFlight->Csgn,prmAFlight->Act3,prmAFlight->Act5,olMode,omPerUrno);
	else
		ogFpeData.StartFlightPermitsApplS(olPern, ConvertFlightPermitTime(prmAFlight->Stoa), true, prmAFlight->Regn, prmAFlight->Alc3, prmAFlight->Fltn, prmAFlight->Flns,prmAFlight->Csgn,prmAFlight->Act3,prmAFlight->Act5,olMode,omPerUrno);
}

void RotationDlg::onDepPermitsButton() 
{
	CString olMode("0");
	
	//	ogBCD.SetObject("FPE","URNO,PERN,CSGN"); 
	CString olWhere = ""; //lgp
	CString olUrno = "";
	CString olPern = ""; 
	CString olStod = CTimeToDBString(ConvertFlightPermitTime(prmDFlight->Stod), prmDFlight->Stod);
	
	olWhere.Format("WHERE CSGN = '%s' AND VAFR >= '%s' AND VATO <= '%s' AND ADID = '%s'", CString(prmDFlight->Csgn), olStod, olStod, CString(prmDFlight->Adid));
	//	ogBCD.Read(CString("FPE"), olWhere);		
	//olUrno = ogBCD.GetField("FPE", "CSGN", CString(prmDFlight->Csgn), "URNO");
	//	omPerUrno = ogBCD.GetField("FPE", "CSGN", CString(prmAFlight->Csgn), "URNO");
	//olPern = ogBCD.GetField("FPE", "CSGN", CString(prmDFlight->Csgn), "PERN");
	//	olPern = ogBCD.GetField("FPE", "URNO", omPerUrno, "PERN");
	
	FPEDATA *prlFpe = NULL;
	if(bgShowADHOC || bgSeasonShowADHOC)
	{
		prlFpe = ogFpeData.GetBestPermitForFlight(ConvertFlightPermitTime(prmDFlight->Stod), false, prmDFlight->Regn, prmDFlight->Alc3, prmDFlight->Fltn, prmDFlight->Flns,prmDFlight->Csgn, &omDepFlightPermitInfo);
		//if(omDepFlightPermitInfo != "No Flight Permit")
		if(prlFpe != NULL)
		{
			olUrno.Format("%ld", prlFpe->Urno);//lgptest
			olPern = prlFpe->Pern;
		}
	}
	else
	{
		prlFpe = ogFpeData.GetBestPermitForFlight(ConvertFlightPermitTime(prmDFlight->Stod), false, prmDFlight->Regn, prmDFlight->Alc3, prmDFlight->Fltn, prmDFlight->Flns, &omDepFlightPermitInfo);
		//if(omDepFlightPermitInfo != "No Flight Permit")
		if(prlFpe != NULL)
		{
			olUrno.Format("%ld", prlFpe->Urno);//lgptest
			olPern = prlFpe->Pern;
		}
	}
	
	if(bgShowADHOC)
	{
		if(strcmp(prmDFlight->Adho,"X") == 0)
		{
			olMode="1";
		}
	}
	if(pomCBDAHOC->GetCheck() == TRUE)
		ogFpeData.StartFlightPermitsApplS(olPern, ConvertFlightPermitTime(prmDFlight->Stod), false, prmDFlight->Regn, "", "", "",prmDFlight->Csgn,prmDFlight->Act3,prmDFlight->Act5,olMode,omPerUrno);
	else
		ogFpeData.StartFlightPermitsApplS(olPern, ConvertFlightPermitTime(prmDFlight->Stod), false, prmDFlight->Regn, prmDFlight->Alc3, prmDFlight->Fltn, prmDFlight->Flns,prmDFlight->Csgn,prmDFlight->Act3,prmDFlight->Act5,olMode,omPerUrno);
}

void RotationDlg::OnARep() 
{
	if (prmAFlight->Urno <= 0)
		return;

	char pclConfigPath[256];
	char pclExcelPath[256];

	if (getenv("CEDA") == NULL)
		strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
	else
		strcpy(pclConfigPath, getenv("CEDA"));

	GetPrivateProfileString(ogAppName, "FLIGHTREPORTVIEWER", "DEFAULT",
		pclExcelPath, sizeof pclExcelPath, pclConfigPath);


	if(!strcmp(pclExcelPath, "DEFAULT"))
		MessageBox(GetString(IDS_STRING2575), GetString(ST_FEHLER), MB_ICONERROR);

	CPoint point;
	::GetCursorPos(&point);

	CString olUser = ogBasicData.omUserID;
	CString olPermits = ogPrivList.GetStat("ROTATIONDLG_CB_ARep");

	CString olTime = "U";
	if (bmLocalTime)
		olTime = "L";

	if (CheckPostFlight())
		olPermits = "0";

	char slRunTxt[512] = ""; 
	sprintf( slRunTxt,"FLIGHTREPORT_ARR,%d,%d|%s,%s,%s|%d,%d", prmAFlight->Urno, prmDFlight->Urno, olUser, olPermits, olTime, point.x, point.y );

	char *args[4];
	args[0] = "child";
	args[1] = slRunTxt;
	args[2] = NULL;
	args[3] = NULL;
	int ilReturn = _spawnv(_P_NOWAIT,pclExcelPath,args);
}

void RotationDlg::OnDRep() 
{
	if (prmDFlight->Urno <= 0)
		return;

	char pclConfigPath[256];
	char pclExcelPath[256];

	if (getenv("CEDA") == NULL)
		strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
	else
		strcpy(pclConfigPath, getenv("CEDA"));

	GetPrivateProfileString(ogAppName, "FLIGHTREPORTVIEWER", "DEFAULT",
		pclExcelPath, sizeof pclExcelPath, pclConfigPath);


	if(!strcmp(pclExcelPath, "DEFAULT"))
		MessageBox(GetString(IDS_STRING2575), GetString(ST_FEHLER), MB_ICONERROR);


	CPoint point;
	::GetCursorPos(&point);

	CString olUser = ogBasicData.omUserID;
	CString olPermits = ogPrivList.GetStat("ROTATIONDLG_CB_DRep");

	CString olTime = "U";
	if (bmLocalTime)
		olTime = "L";

	if (CheckPostFlight())
		olPermits = "0";

	char slRunTxt[512] = ""; 
	sprintf( slRunTxt,"FLIGHTREPORT_DEP,%d,%d|%s,%s,%s|%d,%d", prmAFlight->Urno, prmDFlight->Urno, olUser, olPermits, olTime, point.x, point.y );

	char *args[4];
	args[0] = "child";
	args[1] = slRunTxt;
	args[2] = NULL;
	args[3] = NULL;
	int ilReturn = _spawnv(_P_NOWAIT,pclExcelPath,args);
}

void RotationDlg::OnDDelay() 
{
	if (prmDFlight->Urno <= 0) 
		return;

	char pclConfigPath[256];
	char pclExcelPath[256];

	if (getenv("CEDA") == NULL)
		strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
	else
		strcpy(pclConfigPath, getenv("CEDA"));

	GetPrivateProfileString(ogAppName, "FLIGHTREPORTVIEWER", "DEFAULT",
		pclExcelPath, sizeof pclExcelPath, pclConfigPath);


	if(!strcmp(pclExcelPath, "DEFAULT"))
		MessageBox(GetString(IDS_STRING2575), GetString(ST_FEHLER), MB_ICONERROR);


	CPoint point;
	::GetCursorPos(&point);

	CString olUser = ogBasicData.omUserID;
	CString olPermits = ogPrivList.GetStat("ROTATIONDLG_CB_DDelay");

	CString olTime = "U";
	if (bmLocalTime)
		olTime = "L";

	if (CheckPostFlight())
		olPermits = "0";

	char slRunTxt[512] = ""; 
	sprintf( slRunTxt,"DELAYLOG,%d,%d|%s,%s,%s|%d,%d", prmAFlight->Urno, prmDFlight->Urno, olUser, olPermits, olTime, point.x, point.y );

	char *args[4];
	args[0] = "child";
	args[1] = slRunTxt;
	args[2] = NULL;
	args[3] = NULL;
	int ilReturn = _spawnv(_P_NOWAIT,pclExcelPath,args);
}



void RotationDlg::OnSeasonMask() 
{	
	if (bmRotationReadOnly)
		pogSeasonDlg->NewData(pomParent, lmRkey, lmCalledBy, DLG_CHANGE_DIADATA, bmLocalTime, omAdid);
	else
		pogSeasonDlg->NewData(pomParent, lmRkey, lmCalledBy, DLG_CHANGE, bmLocalTime, omAdid);


//	pogSeasonDlg->NewData(pomParent, lmRkey, lmCalledBy, DLG_CHANGE, bmLocalTime, omAdid);
}


/*
void RotationDlg::OnAawi() 
{
	
	CString olApc;
	m_CE_AOrg3.GetWindowText(olApc);

	ShowAwi( olApc); 
}

void RotationDlg::OnDawi() 
{
	CString olApc;
	m_CE_DDes3.GetWindowText(olApc);

	ShowAwi( olApc); 

}
*/

void RotationDlg::ShowAwi( CString olApc) 
{
	
	CString olText;
	CString olWhere;

	CString olApc3;
	CString olApc4;

	ogBCD.GetField("APT", "APC3", "APC4", olApc, olApc3, olApc4);


	if( !olApc3.IsEmpty() || !olApc4.IsEmpty())
	{

		olWhere.Format("WHERE APC3 = '%s' AND APC4 = '%s' AND VPFR < '%s' AND ( VPTO > '%s' OR VPTO = ' ')", olApc3, olApc4, CTime::GetCurrentTime().Format("%Y%m%d%H%M00"), CTime::GetCurrentTime().Format("%Y%m%d%H%M00"));

		ogBCD.Read("AWI", olWhere);

		if( ogBCD.GetDataCount("AWI") == 0)
		{
			MessageBox( GetString(IDS_STRING1682), GetString(ST_HINWEIS), MB_OK );			
		}
		else
		{

			olText = olApc3 + CString("  ") + olApc4 + CString("  "); 
			olText += ogBCD.GetField("APT", "APC4", olApc4, CString("APFN"));
			olText += CString("\n\n"); 

			olText += GetString(IDS_STRING1683) + ogBCD.GetField("AWI", 0, "WCOD") + CString("\n");
			
			olText += GetString(IDS_STRING1684) + ogBCD.GetField("AWI", 0, "TEMP") + CString("\n");
			olText += GetString(IDS_STRING1685) + ogBCD.GetField("AWI", 0, "HUMI") + CString("\n");
			olText += GetString(IDS_STRING1686) + ogBCD.GetField("AWI", 0, "WIND") + CString("\n");
			olText += GetString(IDS_STRING1687) + ogBCD.GetField("AWI", 0, "WDIR") + CString("\n");
			olText += GetString(IDS_STRING1688) + ogBCD.GetField("AWI", 0, "VISI") + CString("\n");
			olText += GetString(IDS_STRING1689) + CString("\n") + ogBCD.GetField("AWI", 0, "MSGT") + CString("\n");

			MessageBox( olText, GetString(IDS_STRING1690), MB_OK );			
		
		}
	}
}


void RotationDlg::OnDgd2d() 
{
	OnEditChanged(0,0);
}


void RotationDlg::OnAcxx() 
{
	m_CB_ARetFlight.SetCheck(FALSE);
	m_CB_ARetTaxi.SetCheck(FALSE);
	m_CB_ADiverted.SetCheck(FALSE);
	m_CB_ARerouted.SetCheck(FALSE);
	m_CE_ADivr.EnableWindow(FALSE);
	OnEditChanged(0,0);

	if(m_CB_ACxx.GetCheck()  && bgCxxReason)
	{
		if(ogPrivList.GetStat("ROTATIONDLG_CE_ACxxReason") == '-')
		{
			m_CL_ACxxReason.ShowWindow(SW_HIDE);
		}
		else
		{
			m_CL_ACxxReason.ShowWindow(SW_SHOW);
			if(ogPrivList.GetStat("ROTATIONDLG_CE_ACxxReason") == '0')
			{
				m_CL_ACxxReason.EnableWindow(FALSE);
			}
		}
	}
	else
	{
		m_CL_ACxxReason.ShowWindow(SW_HIDE);
	}



}

void RotationDlg::OnADiverted() 
{
	m_CB_ARetFlight.SetCheck(FALSE);
	m_CB_ARetTaxi.SetCheck(FALSE);
	m_CB_ACxx.SetCheck(FALSE);
	m_CB_ARerouted.SetCheck(FALSE);

	if(m_CB_ADiverted.GetCheck() == TRUE)
		m_CE_ADivr.EnableWindow(TRUE);
	else
		m_CE_ADivr.EnableWindow(FALSE);


	OnEditChanged(0,0);	
}

void RotationDlg::OnARerouted() 
{
	m_CB_ARetFlight.SetCheck(FALSE);
	m_CB_ARetTaxi.SetCheck(FALSE);
	m_CB_ADiverted.SetCheck(FALSE);
	m_CB_ACxx.SetCheck(FALSE);
	m_CE_ADivr.EnableWindow(FALSE);
	OnEditChanged(0,0);	
}

void RotationDlg::OnAretflight() 
{
	m_CB_ADiverted.SetCheck(FALSE);
	m_CB_ARerouted.SetCheck(FALSE);
	m_CB_ACxx.SetCheck(FALSE);
	m_CB_ARetTaxi.SetCheck(FALSE);
	m_CE_ADivr.EnableWindow(FALSE);
	OnEditChanged(0,0);
}

void RotationDlg::OnArettaxi() 
{
	m_CB_ADiverted.SetCheck(FALSE);
	m_CB_ARerouted.SetCheck(FALSE);
	m_CB_ACxx.SetCheck(FALSE);
	m_CB_ARetFlight.SetCheck(FALSE);
	m_CE_ADivr.EnableWindow(FALSE);

	OnEditChanged(0,0);
}


void RotationDlg::EnableDepBelts(boolean bpEnable) {

	if (bgUseDepBelts) {
		if (bpEnable) {
	m_CB_DBlt1List.EnableWindow(true);
	m_CE_DTmb1.EnableWindow(true);
	m_CE_DBlt1.EnableWindow(true);
	m_CE_DB1ea.EnableWindow(true);
	m_CE_DB1ba.EnableWindow(true);

	} else {
	m_CB_DBlt1List.EnableWindow(false);
	m_CE_DTmb1.EnableWindow(false);
	m_CE_DBlt1.EnableWindow(false);
	m_CE_DB1ea.EnableWindow(false);
	m_CE_DB1ba.EnableWindow(false);

	/*
	m_CE_DTmb1.SetWindowText("");
	m_CE_DBlt1.SetWindowText("");
	m_CE_DB1ea.SetWindowText("");
	m_CE_DB1ba.SetWindowText("");
	m_DBlt1 = "";
	m_DB1ea = "";
	m_DB1ba = "";
	*/
	}
	}
}


void RotationDlg::OnDcxx() 
{
	m_CB_DRetFlight.SetCheck(FALSE);
	m_CB_DRetTaxi.SetCheck(FALSE);
	m_CB_DDiverted.SetCheck(FALSE);
	m_CB_DRerouted.SetCheck(FALSE);
	m_CE_DDivr.EnableWindow(FALSE);
	OnEditChanged(0,0);

	if(m_CB_DCxx.GetCheck() && bgCxxReason)
	{
		if(ogPrivList.GetStat("ROTATIONDLG_CE_DCxxReason") == '-')
		{
			m_CL_DCxxReason.ShowWindow(SW_HIDE);
		}
		else
		{
			m_CL_DCxxReason.ShowWindow(SW_SHOW);
			if(ogPrivList.GetStat("ROTATIONDLG_CE_DCxxReason") == '0')
			{
				m_CL_DCxxReason.EnableWindow(FALSE);
			}
		}
	}
	else
	{
		m_CL_DCxxReason.ShowWindow(SW_HIDE);
		m_CL_DCxxReason.SetCurSel(0);
	}

	if(m_CB_DCxx.GetCheck()) {
		EnableDepBelts(true);
	} else {
		EnableDepBelts(false);
	}

}


void RotationDlg::OnDDiverted() 
{
	m_CB_DRetFlight.SetCheck(FALSE);
	m_CB_DRetTaxi.SetCheck(FALSE);
	m_CB_DCxx.SetCheck(FALSE);
	m_CB_DRerouted.SetCheck(FALSE);

	if(m_CB_DDiverted.GetCheck() == TRUE)
		m_CE_DDivr.EnableWindow(TRUE);
	else
		m_CE_DDivr.EnableWindow(FALSE);

	OnEditChanged(0,0);	
}


void RotationDlg::OnDRerouted() 
{
	m_CB_DRetFlight.SetCheck(FALSE);
	m_CB_DRetTaxi.SetCheck(FALSE);
	m_CB_DDiverted.SetCheck(FALSE);
	m_CB_DCxx.SetCheck(FALSE);
	m_CE_DDivr.EnableWindow(FALSE);
	OnEditChanged(0,0);	
}





void RotationDlg::OnDretflight() 
{
	m_CB_DCxx.SetCheck(FALSE);
	m_CB_DRetTaxi.SetCheck(FALSE);
	m_CB_DDiverted.SetCheck(FALSE);
	m_CB_DRerouted.SetCheck(FALSE);
	m_CE_DDivr.EnableWindow(FALSE);
	OnEditChanged(0,0);
}

void RotationDlg::OnDrettaxi() 
{
	m_CB_DRetFlight.SetCheck(FALSE);
	m_CB_DCxx.SetCheck(FALSE);
	m_CB_DDiverted.SetCheck(FALSE);
	m_CB_DRerouted.SetCheck(FALSE);
	m_CE_DDivr.EnableWindow(FALSE);
	OnEditChanged(0,0);
}


void RotationDlg::OnDcicCodeShare1() 
{
	int ilLine = pomDCinsTable->pomListBox->GetTopIndex();
	int ilSel = olCicCoIxs[ilLine];

	if (ShowCodeShareListBox(ilSel))
	{
		olCicCoIxs[ilLine] = ilSel;	
		m_CB_Ok.SetColors(RGB(255,0,0), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
		bmChanged = true;
	}

	return;
}


void RotationDlg::OnDcicCodeShare2() 
{
	int ilLine = pomDCinsTable->pomListBox->GetTopIndex() + 1;
	int ilSel = olCicCoIxs[ilLine];

	if (ShowCodeShareListBox(ilSel))
	{
		olCicCoIxs[ilLine] = ilSel;	
		m_CB_Ok.SetColors(RGB(255,0,0), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
		bmChanged = true;
	}

	return;
}


void RotationDlg::OnDcicCodeShare3() 
{
	int ilLine = pomDCinsTable->pomListBox->GetTopIndex() + 2;
	int ilSel = olCicCoIxs[ilLine];

	if (ShowCodeShareListBox(ilSel))
	{
		olCicCoIxs[ilLine] = ilSel;	
		m_CB_Ok.SetColors(RGB(255,0,0), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
		bmChanged = true;
	}

	return;
}


void RotationDlg::OnDcicCodeShare4() 
{
	int ilLine = pomDCinsTable->pomListBox->GetTopIndex() + 3;
	int ilSel = olCicCoIxs[ilLine];

	if (ShowCodeShareListBox(ilSel))
	{
		olCicCoIxs[ilLine] = ilSel;	
		m_CB_Ok.SetColors(RGB(255,0,0), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
		bmChanged = true;
	}

	return;
}


bool RotationDlg::ShowCodeShareListBox(int &ripSel)
{
	// create listbox dlg
	ListBoxDlg olDlg(this, GetString(IDS_STRING2112), CString(""), &ripSel);
	olDlg.SetSize(CSize(300,300));
	// add flno of master flight
	olDlg.AddItem(CString(prmDFlight->Flno));
	// add flnos of code share flights
	CString olItem;
	for (int i = 0; i < omDJfno.GetSize(); i++)
	{
		olItem.Format("%s%s%s", omDJfno[i].Alc3, omDJfno[i].Fltn, omDJfno[i].Flns);		
		olDlg.AddItem(olItem);
	}

	// start dlg
	if(olDlg.DoModal() == IDOK)
	{
		return true;
	}
	return false;
}



void RotationDlg::OnDcicrem1() 
{

	int ilLine = pomDCinsTable->pomListBox->GetTopIndex();

	CString olText;
	if(bgCnamAtr)
	pomDCinsTable->GetTextFieldValue(ilLine, 7, olText);
	else
	pomDCinsTable->GetTextFieldValue(ilLine, 6, olText);

	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "FID_CIC","CODE,"+ogFIDRemarkField, "CODE+", olText);
	polDlg->SetSecState("ROTATIONDLG_CE_DCins");
	if(polDlg->DoModal() == IDOK)
	{
		if(bgCnamAtr)
		pomDCinsTable->SetIPValue(ilLine, 7, polDlg->GetField("CODE"));
        else
		pomDCinsTable->SetIPValue(ilLine, 6, polDlg->GetField("CODE"));	
	}
	delete polDlg;
	
}

void RotationDlg::OnDcicrem2() 
{

	int ilLine = pomDCinsTable->pomListBox->GetTopIndex()+1;

	CString olText;
	if(bgCnamAtr)
	pomDCinsTable->GetTextFieldValue(ilLine, 7, olText);
	else
	pomDCinsTable->GetTextFieldValue(ilLine, 6, olText);
	
	
	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "FID_CIC","CODE,"+ogFIDRemarkField, "CODE+", olText);
	polDlg->SetSecState("ROTATIONDLG_CE_DCins");
	if(polDlg->DoModal() == IDOK)
	{
	  if(bgCnamAtr)
		pomDCinsTable->SetIPValue(ilLine, 7, polDlg->GetField("CODE"));
        else
		pomDCinsTable->SetIPValue(ilLine, 6, polDlg->GetField("CODE"));	
	}
	delete polDlg;
	
}

void RotationDlg::OnDcicrem3() 
{
	int ilLine = pomDCinsTable->pomListBox->GetTopIndex()+2;

	CString olText;

	if(bgCnamAtr)
	pomDCinsTable->GetTextFieldValue(ilLine, 7, olText);
	else
	pomDCinsTable->GetTextFieldValue(ilLine, 6, olText);
	
	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "FID_CIC","CODE,"+ogFIDRemarkField, "CODE+", olText);
	polDlg->SetSecState("ROTATIONDLG_CE_DCins");
	if(polDlg->DoModal() == IDOK)
	{
		if(bgCnamAtr)
		pomDCinsTable->SetIPValue(ilLine, 7, polDlg->GetField("CODE"));
        else
		pomDCinsTable->SetIPValue(ilLine, 6, polDlg->GetField("CODE"));
	}
	delete polDlg;
}

void RotationDlg::OnDcicrem4() 
{
	int ilLine = pomDCinsTable->pomListBox->GetTopIndex()+3;

	CString olText;
   if(bgCnamAtr)
	pomDCinsTable->GetTextFieldValue(ilLine, 7, olText);
	else
	pomDCinsTable->GetTextFieldValue(ilLine, 6, olText);
	
	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "FID_CIC","CODE,"+ogFIDRemarkField, "CODE+", olText);
	polDlg->SetSecState("ROTATIONDLG_CE_DCins");
	if(polDlg->DoModal() == IDOK)
	{
	  if(bgCnamAtr)
		pomDCinsTable->SetIPValue(ilLine, 7, polDlg->GetField("CODE"));
        else
		pomDCinsTable->SetIPValue(ilLine, 6, polDlg->GetField("CODE"));
	}
	delete polDlg;
	
}

void RotationDlg::OnAgpu() 
{
	RotationGpuDlg olDlg(this, prmAFlight, bmLocalTime);
	olDlg.DoModal();
}

void RotationDlg::OnDgpu() 
{
	RotationGpuDlg olDlg(this, prmDFlight, bmLocalTime);
	olDlg.DoModal();

}

void RotationDlg::OnAPca() 
{
	RotationPcaDlg olDlg(this, prmAFlight, bmLocalTime);
	olDlg.DoModal();
}
void RotationDlg::OnDPca() 
{
	RotationPcaDlg olDlg(this, prmDFlight, bmLocalTime);
	olDlg.DoModal();
}
void RotationDlg::OnAPlb() 
{
	CTime ctBaseTime=prmAFlight->Onbl;
	if(ctBaseTime == NULL || ctBaseTime == -1)
	{
			
		MessageBox(GetString(IDS_STRING2907),GetString(ST_FEHLER));
		return;
	}
	else
	{
	
		RotationPlbDlg olDlg(this, prmAFlight, bmLocalTime);
		olDlg.DoModal();
	}
}
void RotationDlg::OnDPlb() 
{
	CTime ctBaseTime=prmDFlight->Ofbl;
	if(ctBaseTime == NULL || ctBaseTime == -1)
	{
			
		MessageBox(GetString(IDS_STRING2908),GetString(ST_FEHLER));
		return;
	}
	else
	{
		RotationPlbDlg olDlg(this, prmDFlight, bmLocalTime);
		olDlg.DoModal();
	}
}

void RotationDlg::OnAload() 
{
	char pclConfigPath[256];
	char pclExcelPath[256];
	
	if (getenv("CEDA") == NULL)
		strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
	else
		strcpy(pclConfigPath, getenv("CEDA"));
	
	GetPrivateProfileString(ogAppName, "EDITLOAD", "DEFAULT",
		pclExcelPath, sizeof pclExcelPath, pclConfigPath);
	
	
	if(strcmp(pclExcelPath, "DEFAULT") == 0)
	{
		if (strcmp(pcgHome, "WAW") == 0)
		{
			if(prmAFlight->Urno > 0)
			{
				if (!polRotationALoadDlg)
				{
					CString olCaption = CString(prmAFlight->Flno);
					olCaption.TrimRight();
					if (olCaption.IsEmpty())
						olCaption = CString(prmAFlight->Csgn);
					
					bool blEnable = true;
					if(ogPrivList.GetStat("ROTATIONDLG_CE_Load") != '1')
						blEnable = false;
					
					CString olDes = CString(prmAFlight->Des3) + CString(prmAFlight->Des4);
					CString olOrg = CString(prmAFlight->Org3) + CString(prmAFlight->Org4);
					
					polRotationALoadDlg = new RotationLoadDlgWAW(this, "A", olCaption, prmAFlight->Urno, prmAFlight->Vial, prmAFlight->Stod, bmLocalTime, blEnable, olDes, olOrg);
					polRotationALoadDlg->Create(RotationLoadDlgWAW::IDD);
					polRotationALoadDlg->SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_AShowVia"));
				}
				
				CPoint point;
				::GetCursorPos(&point);
				//polRotationALoadDlg->SetWindowPos(&wndTop, point.x, point.y,0,0, SWP_SHOWWINDOW | SWP_NOSIZE);// | SWP_NOMOVE );
				
				CRect olRect;
				this->GetWindowRect(&olRect);
				polRotationALoadDlg->SetWindowPos(&wndTop, olRect.left + 20, olRect.top + 540 ,0,0, SWP_SHOWWINDOW | SWP_NOSIZE);// | SWP_NOMOVE );
			}
			
			
			return;
		}
		
		if (strcmp(pcgHome, "DXB") == 0)
		{
			RotationLoadDlg olDlg(this, prmAFlight);
			olDlg.DoModal();
		}
		else
		{
			RotationLoadDlgATH olDlg(this, prmAFlight, !CheckPostFlight());
			olDlg.DoModal();
		}
	}
	else
	{
		long UtcLocal = 0;
		CTime olTUtc = prmAFlight->Stoa;
		if (bmLocalTime)
		{
			ogBasicData.UtcToLocal(olTUtc);
			CTimeSpan olSpan = olTUtc - prmAFlight->Stoa;
			UtcLocal = olSpan.GetTotalMinutes();
			
		}
		
		char slRunTxt[512] = ""; 
		CString olReadOnly = "";
		if (CheckPostFlight())
			olReadOnly = "RO";
		
		if(ogPrivList.GetStat("ROTATIONDLG_CE_Load") == '0')
			olReadOnly = "RO"; 
		
		
		//Edit parameters -> total 10 parameters
		//sprintf( slRunTxt,"%d,,,,%d,EDIT,%s,%s", prmAFlight->Urno, UtcLocal, olReadOnly, "A" );
		CString olUser = ogBasicData.omUserID;
		//sprintf( slRunTxt,"%d,,,,%d,EDIT,%s,%s,%s,%s", prmAFlight->Urno, UtcLocal, olReadOnly, "A",olUser,pcgPasswd);
		char *dqcsTmp;
		if(bgValuDQCS && ((strcmp(ogDQCSA, "C")==0) || (strcmp(ogDQCSA, "S")==0) || ((dqcsTmp = strstr(ogDQCSA, "LC"))!=NULL) ))//lgp
		{
			if(bgUseMultiAirport)
				sprintf( slRunTxt,"%d,,,,%d,EDIT,%s,%s,%s,%s,%s", prmAFlight->Urno, UtcLocal, olReadOnly, "A",olUser,pcgPasswd,pcgTableExt);
			else
				sprintf( slRunTxt,"%d,,,,%d,EDIT,%s,%s,%s,%s", prmAFlight->Urno, UtcLocal, olReadOnly, "A",olUser,pcgPasswd);
		}
		else
		{
			if(bgUseMultiAirport)
				sprintf( slRunTxt,"%d,,,,%d,EDIT,%s,%s,%s,%s,%s", prmAFlight->Urno, UtcLocal, "", "A",olUser,pcgPasswd,pcgTableExt);
			else
				sprintf( slRunTxt,"%d,,,,%d,EDIT,%s,%s,%s,%s", prmAFlight->Urno, UtcLocal, "", "A",olUser,pcgPasswd);
		}
		
		GetPrivateProfileString(ogAppName, pclExcelPath, "DEFAULT",
			pclExcelPath, sizeof pclExcelPath, pclConfigPath);
		
		
		if(!strcmp(pclExcelPath, "DEFAULT"))
		{
			MessageBox(GetString(IDS_STRING1478), GetString(ST_FEHLER), MB_ICONERROR);
			return;
		}
		
		
		char *args[4];
		args[0] = "child";
		args[1] = slRunTxt;
		args[2] = NULL;
		args[3] = NULL;
		
		//MessageBox(args);
		int ilReturn = _spawnv(_P_NOWAIT,pclExcelPath,args);
	}
}

void RotationDlg::OnDload() 
{
	char pclConfigPath[256];
	char pclExcelPath[256];
	
	if (getenv("CEDA") == NULL)
		strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
	else
		strcpy(pclConfigPath, getenv("CEDA"));
	
	GetPrivateProfileString(ogAppName, "EDITLOAD", "DEFAULT",
		pclExcelPath, sizeof pclExcelPath, pclConfigPath);
	
	
	if(strcmp(pclExcelPath, "DEFAULT") == 0)
	{
		if(strcmp(pcgHome, "WAW") == 0)
		{
			if(prmDFlight->Urno > 0)
			{
				if (!polRotationDLoadDlg)
				{
					CString olCaption = CString(prmDFlight->Flno);
					olCaption.TrimRight();
					if (olCaption.IsEmpty())
						olCaption = CString(prmDFlight->Csgn);
					
					CString olDes = CString(prmDFlight->Des3) + CString(prmDFlight->Des4);
					
					bool blEnable = true;
					if(ogPrivList.GetStat("ROTATIONDLG_CE_Load") != '1')
						blEnable = false;
					
					polRotationDLoadDlg = new RotationLoadDlgWAW(this, "D", olCaption, prmDFlight->Urno, prmDFlight->Vial, prmDFlight->Stod, bmLocalTime, blEnable, olDes);
					polRotationDLoadDlg->Create(RotationViaDlg::IDD);
					polRotationDLoadDlg->SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_Load"));
				}
				
				CRect olRect;
				this->GetWindowRect(&olRect);
				polRotationDLoadDlg->SetWindowPos(&wndTop, olRect.left + 20, olRect.top + 540 ,0,0, SWP_SHOWWINDOW | SWP_NOSIZE);// | SWP_NOMOVE );
				
				
				return;
			}
		}	
		
		if (strcmp(pcgHome, "DXB") == 0)
		{
			RotationLoadDlg olDlg(this, prmDFlight);
			olDlg.DoModal();
		}
		else
		{
			RotationLoadDlgATH olDlg(this, prmDFlight, !CheckPostFlight());
			olDlg.DoModal();
		}
	}
	else
	{
		long UtcLocal = 0;
		CTime olTUtc = prmDFlight->Stod;
		if (bmLocalTime)
		{
			ogBasicData.UtcToLocal(olTUtc);
			CTimeSpan olSpan = olTUtc - prmDFlight->Stod;
			UtcLocal = olSpan.GetTotalMinutes();
			
		}
		
		char slRunTxt[512] = ""; 
		CString olReadOnly = "";
		if (CheckPostFlight())
			olReadOnly = "RO";
		
		if(ogPrivList.GetStat("ROTATIONDLG_CE_Load") == '0')
			olReadOnly = "RO";
		
		CString olUser = ogBasicData.omUserID;
		//sprintf( slRunTxt,"%d,,,,%d,EDIT,%s,%s,%s,%s", prmAFlight->Urno, UtcLocal, olReadOnly, "A",olUser,pcgPasswd);
		char *dqcsTmp;
		if(bgValuDQCS && ((strcmp(ogDQCSD, "C")==0) || (strcmp(ogDQCSD, "S")==0) || ((dqcsTmp = strstr(ogDQCSD, "LC"))!=NULL) ))//lgp
		{
			if(bgUseMultiAirport)
				sprintf( slRunTxt,"%d,,,,%d,EDIT,%s,%s,%s,%s,%s", prmDFlight->Urno, UtcLocal, olReadOnly,"D" ,olUser,pcgPasswd,pcgTableExt);
			else
				sprintf( slRunTxt,"%d,,,,%d,EDIT,%s,%s,%s,%s", prmDFlight->Urno, UtcLocal, olReadOnly,"D" ,olUser,pcgPasswd);
		}
		else
		{
			if(bgUseMultiAirport)
				sprintf( slRunTxt,"%d,,,,%d,EDIT,%s,%s,%s,%s,%s", prmDFlight->Urno, UtcLocal, "","D" ,olUser,pcgPasswd,pcgTableExt);
			else
				sprintf( slRunTxt,"%d,,,,%d,EDIT,%s,%s,%s,%s", prmDFlight->Urno, UtcLocal, "","D" ,olUser,pcgPasswd);
		}
		
		
		
		GetPrivateProfileString(ogAppName, pclExcelPath, "DEFAULT",
			pclExcelPath, sizeof pclExcelPath, pclConfigPath);
		
		
		if(!strcmp(pclExcelPath, "DEFAULT"))
		{
			MessageBox(GetString(IDS_STRING1478), GetString(ST_FEHLER), MB_ICONERROR);
			return;
		}
		
		
		char *args[4];
		args[0] = "child";
		args[1] = slRunTxt;
		args[2] = NULL;
		args[3] = NULL;
		int ilReturn = _spawnv(_P_NOWAIT,pclExcelPath,args);
	}
}

void RotationDlg::OnALog() 
{
	if (prmAFlight->Urno <= 0)
		return;
	
	char pclConfigPath[256];
	char pclExcelPath[256];
	
	if (getenv("CEDA") == NULL)
		strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
	else
		strcpy(pclConfigPath, getenv("CEDA"));
	
	GetPrivateProfileString(ogAppName, "LOGTABVIEWER", "DEFAULT",
		pclExcelPath, sizeof pclExcelPath, pclConfigPath);
	
	
	if(!strcmp(pclExcelPath, "DEFAULT"))
		MessageBox(GetString(IDS_STRING2540), GetString(ST_FEHLER), MB_ICONERROR);
	
	
	char *args[4];
	char slRunTxt[256] = "";
	char buffer[64] = "";
	args[0] = "child";
	sprintf( slRunTxt,"AFTTAB,%s,",ltoa(prmAFlight->Urno,buffer,10) );
	args[1] = slRunTxt;
	args[2] = NULL;
	args[3] = NULL;
	int ilReturn = _spawnv(_P_NOWAIT,pclExcelPath,args);
}

void RotationDlg::OnDLog() 
{
	if (prmDFlight->Urno <= 0)
		return;

	char pclConfigPath[256];
	char pclExcelPath[256];

	if (getenv("CEDA") == NULL)
		strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
	else
		strcpy(pclConfigPath, getenv("CEDA"));

	GetPrivateProfileString(ogAppName, "LOGTABVIEWER", "DEFAULT",
		pclExcelPath, sizeof pclExcelPath, pclConfigPath);


	if(!strcmp(pclExcelPath, "DEFAULT"))
		MessageBox(GetString(IDS_STRING2540), GetString(ST_FEHLER), MB_ICONERROR);


	char *args[4];
	char slRunTxt[256] = "";
	char buffer[64] = "";
	args[0] = "child";
	sprintf( slRunTxt,"AFTTAB,%s,",ltoa(prmDFlight->Urno,buffer,10) );
	args[1] = slRunTxt;
	args[2] = NULL;
	args[3] = NULL;
	int ilReturn = _spawnv(_P_NOWAIT,pclExcelPath,args);
}

LRESULT RotationDlg::OnEditRButtonDown(UINT wParam, LPARAM lParam) 
{
	// Insert current time in baggage-belt begin- and end-editbox
 	CCSEDITNOTIFY *prlNotify = (CCSEDITNOTIFY *) lParam;
 	CTime olTime = CTime::GetCurrentTime();
 	if (!bmLocalTime)
	{
//		ogRotationDlgFlights.LocalToUtc(olTime);
		ogBasicData.LocalToUtc(olTime);
	}



	if (prlNotify->SourceControl == &m_CE_AB1ba)
	{
 		m_CE_AB1ba.SetWindowText(DateToHourDivString(olTime, olTime));
	}
	if (prlNotify->SourceControl == &m_CE_AB1ea)
	{
 		m_CE_AB1ea.SetWindowText(DateToHourDivString(olTime, olTime));
	}
	if (prlNotify->SourceControl == &m_CE_AB2ba)
	{
 		m_CE_AB2ba.SetWindowText(DateToHourDivString(olTime, olTime));
	}
	if (prlNotify->SourceControl == &m_CE_AB2ea)
	{
 		m_CE_AB2ea.SetWindowText(DateToHourDivString(olTime, olTime));
	}
	if (prlNotify->SourceControl == &m_CE_ATmoa)
	{
 		m_CE_ATmoa.SetWindowText(DateToHourDivString(olTime, olTime));
	}
	return 0L;
	
}

void RotationDlg::SetCashCommentDisplay()
{

	if(bgShowCashComment)
	{
	
		char clStat = ogPrivList.GetStat("ROTATIONDLG_CE_Cash");
		switch (clStat)
		{
			case '1': 
					if(bgShowCashComment)
					{
					  pomBTCRCom->EnableWindow(TRUE); 
					  pomBTCRCom->ShowWindow(SW_SHOW); 
					  pomLBLCCom->ShowWindow(SW_SHOW);
					}
					else
					{
						pomBTCRCom->ShowWindow(SW_HIDE); 
						pomLBLCCom->ShowWindow(SW_HIDE);
					}
				break;
			case '0': pomBTCRCom->EnableWindow(FALSE); 
					  pomLBLCCom->EnableWindow(FALSE);
				break;
			case '-': pomBTCRCom->ShowWindow(SW_HIDE); 
					  pomLBLCCom->ShowWindow(SW_HIDE);
				break;
		}
	}
	else
	{
		pomBTCRCom->ShowWindow(SW_HIDE); 
		pomLBLCCom->ShowWindow(SW_HIDE);

	}
	
}



BOOL RotationDlg::HandlePostFlight()
{
	// postflight: no changes allowed
	if (CheckPostFlight())
	{
		if (GetDlgItem(IDC_JOIN)) 
			m_CB_Join.EnableWindow(FALSE);

		if (GetDlgItem(IDC_SPLIT)) 
			m_CB_Split.EnableWindow(FALSE);

		if (GetDlgItem(IDC_PAID)) 
		{
			m_CB_Paid.EnableWindow(FALSE);
			pomLBLCCom->EnableWindow(FALSE);
			pomBTCRCom->EnableWindow(FALSE);	
		}

		if(bgShowCashButtonInBothFlight)
		{
			m_CashArrBtn.EnableWindow(FALSE);
			m_CashDepBtn.EnableWindow(FALSE);
		}

		if (GetDlgItem(IDOK)) 
			m_CB_Ok.EnableWindow(FALSE);
		
		if (polRotationAViaDlg)
			polRotationAViaDlg->Enable(false);

		if (polRotationDViaDlg)
			polRotationDViaDlg->Enable(false);

		return TRUE;
	}

	return FALSE;
}

BOOL RotationDlg::CheckPostFlight()
{
	BOOL blPost = FALSE;

	if (prmAFlight && prmDFlight)
	{
		if (prmAFlight->Tifa != TIMENULL && prmDFlight->Tifd != TIMENULL)
		{
			if (IsPostFlight(prmAFlight->Tifa,bmLocalTime,prmAFlight->Prfl) && IsPostFlight(prmDFlight->Tifd,bmLocalTime,prmDFlight->Prfl))
				blPost = TRUE;
		}
		else if (prmDFlight->Tifd != TIMENULL)
		{
			if (IsPostFlight(prmDFlight->Tifd,bmLocalTime,prmDFlight->Prfl))
				blPost = TRUE;
		}
		else if (prmAFlight->Tifa != TIMENULL)
		{
			if (IsPostFlight(prmAFlight->Tifa,bmLocalTime,prmAFlight->Prfl))
				blPost = TRUE;
		}
	}
	else if (!prmAFlight && prmDFlight)
	{
		if (prmDFlight->Tifd != TIMENULL)
		{
			if (IsPostFlight(prmDFlight->Tifd,bmLocalTime,prmDFlight->Prfl))
				blPost = TRUE;
		}
	}
	else if (prmAFlight && !prmDFlight)
	{
		if (prmAFlight->Tifa != TIMENULL)
		{
			if (IsPostFlight(prmAFlight->Tifa,bmLocalTime,prmAFlight->Prfl))
				blPost = TRUE;
		}
	}

	if (blPost)
		ModifyWindowText(this, GetString(IDS_STRINGWND_POSTFLIGHT), FALSE);
	else
		ModifyWindowText(this, GetString(IDS_STRINGWND_POSTFLIGHT), TRUE);

	return blPost;
}


bool RotationDlg::AddBltCheck()
{

	CString olWarningText;
	bool blWarning = false;
	CString olAddText;

	if (!bgExtBltCheck)
		return true;

	// only if arrival flight present
	if (prmAFlight->Urno > 0) {
	// only proceed if the baggage belt allocation values or STA, ETA has been changed!
	if (m_CE_ABlt1.IsChanged() || m_CE_ATmb1.IsChanged() || m_CE_AB1ba.IsChanged() || m_CE_AB1ea.IsChanged() ||
		m_CE_ABlt2.IsChanged() || m_CE_ATmb2.IsChanged() || m_CE_AB2ba.IsChanged() || m_CE_AB2ea.IsChanged() ||
		m_CE_AStoa.IsChanged() || m_CE_AEtai.IsChanged() || m_CE_ATmoa.IsChanged())
	{
	//// check opening time of belt 1
	if (!prmAFlight->CheckBltOpeningTime(1, olAddText))
	{
		CString olTmp;
		olTmp.Format(GetString(IDS_STRING2033), olAddText);

		olWarningText += olTmp;
		olWarningText += '\n';
		blWarning = true;
	}
	//// check closing time of belt 1
	if (!prmAFlight->CheckBltClosingTime(1, olAddText))
	{
		CString olTmp;
		olTmp.Format(GetString(IDS_STRING2035), olAddText);

		olWarningText += olTmp;
		olWarningText += '\n';
		blWarning = true;
	}
	//// check opening time of belt 2
	if (!prmAFlight->CheckBltOpeningTime(2, olAddText))
	{
		CString olTmp;
		olTmp.Format(GetString(IDS_STRING2034), olAddText);

		olWarningText += olTmp;
 		olWarningText += '\n';
		blWarning = true;
	}
	//// check closing time of belt 2
	if (!prmAFlight->CheckBltClosingTime(2, olAddText))
	{
		CString olTmp;
		olTmp.Format(GetString(IDS_STRING2036), olAddText);

		olWarningText += olTmp;
		olWarningText += '\n';
		blWarning = true;
	}
	//// check if two belts are allocated
	if (strlen(prmAFlight->Blt1) > 0 && strlen(prmAFlight->Blt2) > 0)
	{
		olWarningText += GetString(IDS_STRING2043);
		olWarningText += '\n';
		blWarning = true;
	}
	} 
	}
	if (bgUseDepBelts && prmDFlight->Urno > 0) {
	// only proceed if the baggage belt allocation values or STD, ETD has been changed!
	if (m_CE_DBlt1.IsChanged() || m_CE_DTmb1.IsChanged() || m_CE_DB1ba.IsChanged() || m_CE_DB1ea.IsChanged() ||
		m_CE_DStod.IsChanged() || m_CE_DEtdi.IsChanged() )
	{

	//// check opening time of belt 1
	if (!prmDFlight->CheckDepBltOpeningTime(1, olAddText))
	{
		CString olTmp;
		olTmp.Format(GetString(IDS_STRING2033), olAddText);

		olWarningText += olTmp;
		olWarningText += '\n';
		blWarning = true;
	}

	
	//// check closing time of belt 1
	if (!prmDFlight->CheckDepBltClosingTime(1, olAddText))
	{
		CString olTmp;
		olTmp.Format(GetString(IDS_STRING2035), olAddText);

		olWarningText += olTmp;
		olWarningText += '\n';
		blWarning = true;
	}

	}
	}



	// Display message box with warnings
	if (blWarning)
	{
		olWarningText += CString("\n") + GetString(IDS_STRING2037);
		if (CFPMSApp::MyTopmostMessageBox(this, olWarningText, GetString(IDS_WARNING), MB_ICONWARNING | MB_YESNO) == IDYES)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	return true;
}

void RotationDlg::OnAViewLoad() 
{
	char pclConfigPath[256];
	char pclExcelPath[256];

	if (getenv("CEDA") == NULL)
		strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
	else
		strcpy(pclConfigPath, getenv("CEDA"));

	GetPrivateProfileString(ogAppName, "LOATABVIEWER", "DEFAULT",
		pclExcelPath, sizeof pclExcelPath, pclConfigPath);


	if(!strcmp(pclExcelPath, "DEFAULT"))
		MessageBox(GetString(IDS_STRING1478), GetString(ST_FEHLER), MB_ICONERROR);


	long UtcLocal = 0;
	CTime olTUtc = prmAFlight->Stoa;
	if (bmLocalTime)
	{
		ogBasicData.UtcToLocal(olTUtc);
		CTimeSpan olSpan = olTUtc - prmAFlight->Stoa;
		UtcLocal = olSpan.GetTotalMinutes();

	}

	char *args[4];
	char slRunTxt[256] = "";
	char buffer[64] = "";
	args[0] = "child";
	//sprintf(slRunTxt,"%s",ltoa(prmAFlight->Urno,buffer,10));
	CString olUser = ogBasicData.omUserID;
		//sprintf( slRunTxt,"%d,,,,%d,EDIT,%s,%s,%s,%s", prmAFlight->Urno, UtcLocal, olReadOnly, "A",olUser,pcgPasswd);
	sprintf( slRunTxt,"%d,,,,%d,,,%s,%s,%s", prmAFlight->Urno, UtcLocal, "",olUser,pcgPasswd);
	
	
	args[1] = slRunTxt;
	args[2] = NULL;
	args[3] = NULL;

	
	int ilReturn = _spawnv(_P_NOWAIT,pclExcelPath,args);
}

void RotationDlg::OnDViewLoad() 
{
	char pclConfigPath[256];
	char pclExcelPath[256];

	if (getenv("CEDA") == NULL)
		strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
	else
		strcpy(pclConfigPath, getenv("CEDA"));

	GetPrivateProfileString(ogAppName, "LOATABVIEWER", "DEFAULT",
		pclExcelPath, sizeof pclExcelPath, pclConfigPath);


	if(!strcmp(pclExcelPath, "DEFAULT"))
		MessageBox(GetString(IDS_STRING1478), GetString(ST_FEHLER), MB_ICONERROR);

	long UtcLocal = 0;
	CTime olTUtc = prmDFlight->Stod;
	if (bmLocalTime)
	{
		ogBasicData.UtcToLocal(olTUtc);
		CTimeSpan olSpan = olTUtc - prmDFlight->Stod;
		UtcLocal = olSpan.GetTotalMinutes();

	}

	char *args[4];
	char slRunTxt[256] = "";
//	char buffer[64];
	args[0] = "child";
	//sprintf(slRunTxt,"%s",ltoa(prmDFlight->Urno,buffer,10));
	CString olUser = ogBasicData.omUserID;
	sprintf( slRunTxt,"%d,,,,%d,,,%s,%s,%s", prmDFlight->Urno, UtcLocal, "",olUser,pcgPasswd);
	
	args[1] = slRunTxt;
	args[2] = NULL;
	args[3] = NULL;
	int ilReturn = _spawnv(_P_NOWAIT,pclExcelPath,args);
}





LRESULT RotationDlg::OnViaChanged(WPARAM wParam,LPARAM lParam) 
{
	ldiv_t div_result;

	if (polRotationAViaDlg && prmAFlight != NULL)
	{
		polRotationAViaDlg->GetViaList(prmAFlight->Vial);
		CString olAVial = prmAFlight->Vial;
		div_result = ldiv( olAVial.GetLength(), 120);
		itoa(div_result.quot, prmAFlight->Vian ,3);
		olAVial.TrimRight();
		strcpy(prmAFlight->Vial, olAVial);
	}

	if (polRotationDViaDlg && prmDFlight != NULL)
	{
		polRotationDViaDlg->GetViaList(prmDFlight->Vial);
		CString olDVial = prmDFlight->Vial;
		div_result = ldiv( olDVial.GetLength(), 120);
		itoa(div_result.quot, prmDFlight->Vian ,3);
		olDVial.TrimRight();
		strcpy(prmDFlight->Vial, olDVial);
	}


	CheckViaButton();

	return 0L;
}





void RotationDlg::OnAVia() 
{
	if(prmAFlight->Urno > 0)
	{
		if (!polRotationAViaDlg)
		{
			CString olCaption = CString(prmAFlight->Flno);
			olCaption.TrimRight();
			if (olCaption.IsEmpty())
				olCaption = CString(prmAFlight->Csgn);

			bool blEnable = true;
			if(ogPrivList.GetStat("ROTATIONDLG_CB_AShowVia") != '1')
				blEnable = false;

			polRotationAViaDlg = new RotationViaDlg(this, "A", olCaption, prmAFlight->Urno, prmAFlight->Vial, prmAFlight->Stoa, bmLocalTime, blEnable);
			polRotationAViaDlg->Create(RotationViaDlg::IDD);
			polRotationAViaDlg->SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_AShowVia"));
		}

		CPoint point;
		::GetCursorPos(&point);
		polRotationAViaDlg->SetWindowPos(&wndTop, point.x, point.y,0,0, SWP_SHOWWINDOW | SWP_NOSIZE);// | SWP_NOMOVE );

	}
}

void RotationDlg::OnDVia() 
{
	if(prmDFlight->Urno > 0)
	{
		if (!polRotationDViaDlg)
		{
			CString olCaption = CString(prmDFlight->Flno);
			olCaption.TrimRight();
			if (olCaption.IsEmpty())
				olCaption = CString(prmDFlight->Csgn);

			bool blEnable = true;
			if(ogPrivList.GetStat("ROTATIONDLG_CB_DShowVia") != '1')
				blEnable = false;

			polRotationDViaDlg = new RotationViaDlg(this, "D", olCaption, prmDFlight->Urno, prmDFlight->Vial, prmDFlight->Stod, bmLocalTime, blEnable);
			polRotationDViaDlg->Create(RotationViaDlg::IDD);
			polRotationDViaDlg->SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_DShowVia"));
		}

		CPoint point;
		::GetCursorPos(&point);
		polRotationDViaDlg->SetWindowPos(&wndTop, point.x, point.y,0,0, SWP_SHOWWINDOW | SWP_NOSIZE);// | SWP_NOMOVE );

	}
}


int RotationDlg::OnToolHitTest(CPoint opPoint,TOOLINFO *pTI) const
{
	static int ilLastToolTip = -1;

	CWnd *olpWnd = NULL;
	int ilItemId = -1;
	CRect olRect;


		olpWnd = (CWnd*) GetDlgItem(IDC_ASHOWJFNO);
		olpWnd->GetWindowRect(olRect);
		this->ScreenToClient(olRect);
		if (olRect.PtInRect(opPoint))
		{
			CString olToolTip = "Code Share Flights: ";
			CString olJfno (prmAFlight->Jfno);

			if (!olJfno.IsEmpty())
			{
				while(!olJfno.IsEmpty())
				{
					if (olJfno.GetLength() < 9)
					{
						int anz = 9 - olJfno.GetLength();
						for (int i=0; i<anz; i++) 
							olJfno.Insert(10000, ' ');
					}
					CString olAlc3 = olJfno.Left(3);
					CString olFltn = olJfno.Mid(3,5);
					CString olFlns = olJfno.Mid(8,1);
					olAlc3.TrimLeft();
					olFlns.TrimLeft();
					olFltn.TrimLeft();
					olJfno = olJfno.Right(olJfno.GetLength() - 9);
					CString olTmp = olAlc3 + olFltn + olFlns + " / ";
					olToolTip += olTmp;
				}
			}

			char *olpChar = new char [256];
			// fill tooltip structure
			sprintf(olpChar, olToolTip);
			pTI->cbSize = sizeof(*pTI);
			pTI->uFlags = TTF_IDISHWND;
			pTI->hwnd   = m_hWnd;
			pTI->uId    = (UINT) olpWnd->m_hWnd;
			pTI->lpszText = olpChar;
			return 1;

//			ilItemId = i;
//			break;
		}

		olpWnd = (CWnd*) GetDlgItem(IDC_DSHOWJFNO);
		olpWnd->GetWindowRect(olRect);
		this->ScreenToClient(olRect);
		if (olRect.PtInRect(opPoint))
		{
			CString olToolTip = "Code Share Flights: ";
			CString olJfno (prmDFlight->Jfno);

			if (!olJfno.IsEmpty())
			{
				while(!olJfno.IsEmpty())
				{
					if (olJfno.GetLength() < 9)
					{
						int anz = 9 - olJfno.GetLength();
						for (int i=0; i<anz; i++) 
							olJfno.Insert(10000, ' ');
					}
					CString olAlc3 = olJfno.Left(3);
					CString olFltn = olJfno.Mid(3,5);
					CString olFlns = olJfno.Mid(8,1);
					olAlc3.TrimLeft();
					olFlns.TrimLeft();
					olFltn.TrimLeft();
					olJfno = olJfno.Right(olJfno.GetLength() - 9);
					CString olTmp = olAlc3 + olFltn + olFlns + " / ";
					olToolTip += olTmp;
				}
			}

			char *olpChar = new char [256];
			// fill tooltip structure
			sprintf(olpChar, olToolTip);
			pTI->cbSize = sizeof(*pTI);
			pTI->uFlags = TTF_IDISHWND;
			pTI->hwnd   = m_hWnd;
			pTI->uId    = (UINT) olpWnd->m_hWnd;
			pTI->lpszText = olpChar;
			return 1;
		}


	// cursor over cellNr
//	for (int i = 0; i < omCells.GetSize(); i++)
//	{
/*		olpWnd = (CButton*) m_CB_AVia;
		if (!olpWnd)
			return -1;
*/
		olpWnd = (CWnd*) GetDlgItem(IDC_AVIA);
		olpWnd->GetWindowRect(olRect);
		this->ScreenToClient(olRect);
		if (olRect.PtInRect(opPoint))
		{
			CString olToolTip = "Via Stations";
			CString olVia;
			if (polRotationAViaDlg)
				olVia = polRotationAViaDlg->GetToolTip();

			if (!olVia.IsEmpty())
				olToolTip = olVia;

			char *olpChar = new char [256];
			// fill tooltip structure
			sprintf(olpChar, olToolTip);
			pTI->cbSize = sizeof(*pTI);
			pTI->uFlags = TTF_IDISHWND;
			pTI->hwnd   = m_hWnd;
			pTI->uId    = (UINT) olpWnd->m_hWnd;
			pTI->lpszText = olpChar;
			return 1;

//			ilItemId = i;
//			break;
		}

		olpWnd = (CWnd*) GetDlgItem(IDC_DVIA);
		olpWnd->GetWindowRect(olRect);
		this->ScreenToClient(olRect);
		if (olRect.PtInRect(opPoint))
		{
			CString olToolTip = "Via Stations";
			CString olVia;
			if (polRotationDViaDlg)
				olVia = polRotationDViaDlg->GetToolTip();

			if (!olVia.IsEmpty())
				olToolTip = olVia;

			char *olpChar = new char [256];
			// fill tooltip structure
			sprintf(olpChar, olToolTip);
			pTI->cbSize = sizeof(*pTI);
			pTI->uFlags = TTF_IDISHWND;
			pTI->hwnd   = m_hWnd;
			pTI->uId    = (UINT) olpWnd->m_hWnd;
			pTI->lpszText = olpChar;
			return 1;

//			ilItemId = i;
//			break;
		}
		if(bgMulitpleDelayCodes == true)
		{
			olpWnd = (CWnd*) GetDlgItem(IDC_DELAYBTN_ARRIVAL);
			olpWnd->GetWindowRect(olRect);
			this->ScreenToClient(olRect);
			if (olRect.PtInRect(opPoint))
			{
				CString olToolTip = "";
				char *olpChar = new char [256];
				olToolTip = pomThis->GetToolTipForDelaysArrival();
				// fill tooltip structure
				sprintf(olpChar, olToolTip);
				pTI->cbSize = sizeof(*pTI);
				pTI->uFlags = TTF_IDISHWND;
				pTI->hwnd   = m_hWnd;
				pTI->uId    = (UINT) olpWnd->m_hWnd;
				pTI->lpszText = olpChar;
				return 1;
			}
			olpWnd = (CWnd*) GetDlgItem(IDC_DELAYBTN_DEPARTURE);
			olpWnd->GetWindowRect(olRect);
			this->ScreenToClient(olRect);
			if (olRect.PtInRect(opPoint))
			{
				CString olToolTip = "";
				char *olpChar = new char [256];
				olToolTip = pomThis->GetToolTipForDelaysDepature();
				// fill tooltip structure
				sprintf(olpChar, olToolTip);
				pTI->cbSize = sizeof(*pTI);
				pTI->uFlags = TTF_IDISHWND;
				pTI->hwnd   = m_hWnd;
				pTI->uId    = (UINT) olpWnd->m_hWnd;
				pTI->lpszText = olpChar;
				return 1;
			}
		}

		if(ogFpeData.bmFlightPermitsEnabled)
		{
			olpWnd = (CWnd*) GetDlgItem(IDC_ARRPERMITSBUTTON);
			olpWnd->GetWindowRect(olRect);
			this->ScreenToClient(olRect);
			if (olRect.PtInRect(opPoint))
			{
				CString olToolTip = "";
				char *olpChar = new char [256];
				olToolTip = omArrFlightPermitInfo;
				// fill tooltip structure
				sprintf(olpChar, olToolTip);
				pTI->cbSize = sizeof(*pTI);
				pTI->uFlags = TTF_IDISHWND;
				pTI->hwnd   = m_hWnd;
				pTI->uId    = (UINT) olpWnd->m_hWnd;
				pTI->lpszText = olpChar;
				return 1;
			}

			olpWnd = (CWnd*) GetDlgItem(IDC_DEPPERMITSBUTTON);
			olpWnd->GetWindowRect(olRect);
			this->ScreenToClient(olRect);
			if (olRect.PtInRect(opPoint))
			{
				CString olToolTip = "";
				char *olpChar = new char [256];
				olToolTip = omDepFlightPermitInfo;
				// fill tooltip structure
				sprintf(olpChar, olToolTip);
				pTI->cbSize = sizeof(*pTI);
				pTI->uFlags = TTF_IDISHWND;
				pTI->hwnd   = m_hWnd;
				pTI->uId    = (UINT) olpWnd->m_hWnd;
				pTI->lpszText = olpChar;
				return 1;
			}
		}
		return -1;
}
//******************************************************
//MWO: Prepare the tooltip for the current dalay button
CString RotationDlg::GetToolTipForDelaysArrival()
{
	CString olRet = "";
	for(int i = 0; i < tabDelayArrival.GetLineCount(); i++)
	{
		CString strCode = "";
		CString strValue = tabDelayArrival.GetFieldValue(i, "DURN");
		strCode = ogBCD.GetField("DEN", "URNO", strValue, "DECA");
		olRet += strCode + CString(",") + tabDelayArrival.GetFieldValue(i, "DURA") + CString("|");
	}
	return olRet;
}
//******************************************************
//MWO: Prepare the tooltip for the current dalay button
CString RotationDlg::GetToolTipForDelaysDepature()
{
	CString olRet = "";
	for(int i = 0; i < tabDelayDeparture.GetLineCount(); i++)
	{
		CString strCode = "";
		CString strValue = tabDelayDeparture.GetFieldValue(i, "DURN");
		strCode = ogBCD.GetField("DEN", "URNO", strValue, "DECA");
		olRet += strCode + CString(",") + tabDelayDeparture.GetFieldValue(i, "DURA") + CString("|");
	}
	return olRet;
}


BOOL RotationDlg::OnToolTipNotify( UINT id, NMHDR * pNMHDR, LRESULT * pResult )
{
    TOOLTIPTEXT *pTTT = (TOOLTIPTEXT *)pNMHDR;
    UINT nID =pNMHDR->idFrom;
    if (pTTT->uFlags & TTF_IDISHWND)
    {
        // idFrom is actually the HWND of the tool
        nID = ::GetDlgCtrlID((HWND)nID);
        if(nID)
        {
            pTTT->lpszText = MAKEINTRESOURCE(nID);
            pTTT->hinst = AfxGetResourceHandle();
            return(TRUE);
        }
    }

    return(FALSE);
}

void RotationDlg::CheckViaButton(void)
{
	if (!pomAVia || !pomDVia)
		return;

	CString olButtonText;
	COLORREF olButtonColor;

	//arr
	int ilCount = atoi(prmAFlight->Vian);
	if (ilCount > 0)
	{
		olButtonColor = ogColors[19];
		olButtonText.Format(GetString(IDS_VIAN), ilCount);

	}
	else
	{
		olButtonText.Format(GetString(IDS_VIAN), 0);
		olButtonColor = ::GetSysColor(COLOR_BTNFACE);
	}

	pomAVia->SetWindowText(olButtonText);
	pomAVia->SetColors(olButtonColor, ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
	pomAVia->UpdateWindow();//BS_OWNERDRAW


	//dep
	ilCount = atoi(prmDFlight->Vian);
	if (ilCount > 0)
	{
		olButtonColor = ogColors[19];
		olButtonText.Format(GetString(IDS_VIAN), ilCount);

	}
	else
	{
		olButtonText.Format(GetString(IDS_VIAN), 0);
		olButtonColor = ::GetSysColor(COLOR_BTNFACE);
	}

	pomDVia->SetWindowText(olButtonText);
	pomDVia->SetColors(olButtonColor, ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
	pomDVia->UpdateWindow();
	
	return; 
}

void RotationDlg::CheckCodeShareButton(void) 
{
	CString olButtonText;
	COLORREF olButtonColor;

	//arr
	int ilCount = omAJfno.GetSize();
	if (ilCount > 0)
	{
		olButtonColor = ogColors[19];
		olButtonText.Format(GetString(IDS_CODE_SHARE), ilCount);

	}
	else
	{
		olButtonText.Format(GetString(IDS_CODE_SHARE), 0);
		olButtonColor = ::GetSysColor(COLOR_BTNFACE);
	}

	m_CB_AShowJfno.SetWindowText(olButtonText);
	m_CB_AShowJfno.SetColors(olButtonColor, ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
	m_CB_AShowJfno.UpdateWindow();


	//dep
	ilCount = omDJfno.GetSize();
	if (ilCount > 0)
	{
		olButtonColor = ogColors[19];
		olButtonText.Format(GetString(IDS_CODE_SHARE), ilCount);

	}
	else
	{
		olButtonText.Format(GetString(IDS_CODE_SHARE), 0);
		olButtonColor = ::GetSysColor(COLOR_BTNFACE);
	}

	m_CB_DShowJfno.SetWindowText(olButtonText);
	m_CB_DShowJfno.SetColors(olButtonColor, ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
	m_CB_DShowJfno.UpdateWindow();
	
	return; 
}



//***************************************
// MWO 14.04.05 When the Delay button is 
// clicked to edit multiple delays for
// arrival flight
void RotationDlg::OnDelaybtnArrival()
{
	if(prmAFlight->Urno > 0)
	{
		if (bgUseMultiDelayTool)			
		{			
			char urno[16] = "";		
			ltoa(prmAFlight->Urno, urno,10);			
			LaunchTool( "FDLY", urno, "", "", "CDID" ,bmLocalTime);		
		}			
		else			
		{			
			if(pomMultiDelayDlg != NULL)
			{
				pomMultiDelayDlg->DestroyWindow();
				pomMultiDelayDlg = NULL;
			}
			pomMultiDelayDlg = new MultiDelayDlg(this, prmAFlight, &tabDelayArrival);
		}
	}
}

//***************************************
// MWO 14.04.05 When the Delay button is 
// clicked to edit multiple delays for
// departure flight
void RotationDlg::OnDelaybtnDeparture()
{
	if(prmDFlight->Urno > 0)
	{
		if (bgUseMultiDelayTool)			
		{			
			char urno[16] = "";	
			ltoa(prmDFlight->Urno, urno,10);		
			LaunchTool( "FDLY", "", urno, "", "CDID" ,bmLocalTime);		
		}			
		else			
		{			
			if(pomMultiDelayDlg != NULL)
			{
				pomMultiDelayDlg->DestroyWindow();
				pomMultiDelayDlg = NULL;
			}
			pomMultiDelayDlg = new MultiDelayDlg(this, prmDFlight, &tabDelayDeparture);
		}
	}
}

//void RotationDlg::OnRButtonDown(UINT nFlags, CPoint point) 
//{
//	CRect olRect;
//    GetItemRect(IDC_ARRPERMITSBUTTON, &olRect);
//    if(olRect.PtInRect(point))
//	{
//		MessageBox(omArrFlightPermitInfo,"",MB_OK);
//	}
//	else
//	{
//		GetItemRect(IDC_DEPPERMITSBUTTON, &olRect);
//		if(olRect.PtInRect(point))
//		{
//			MessageBox(omDepFlightPermitInfo,"",MB_OK);
//		}
//	}
//	
//	CDialog::OnRButtonDown(nFlags, point);
//}

bool RotationDlg::InitDcinsCombo()
{
	if(prmDFlight->Urno > 0) 
	{
		CWnd* olWnd = (CWnd*) GetDlgItem(IDC_COMBO_CCA_XN);

		if (ogFactor_CCA.IsEmpty() && olWnd)
			olWnd->ShowWindow(SW_HIDE);


		m_ComboDcins.ResetContent();
		//CString olFieldList = GetString(IDS_DCINS);
		CString olFieldList = ogFactor_CCA;

		CStringArray olFieldArray;
		int ilFieldArray = ExtractItemList(olFieldList, &olFieldArray, ',');

		for (int l=0; l<olFieldArray.GetSize(); l++)
		{
			m_ComboDcins.AddString(olFieldArray.GetAt(l));
		}
		m_ComboDcins.SetCurSel(0);

	}

	return true;
}

void RotationDlg::OnSelchangeDcins() 
{
	CString olOperator;
	int ilCur = m_ComboDcins.GetCurSel();
	m_ComboDcins.GetLBText(ilCur, olOperator);

//	CString olFieldList = GetString(IDS_DCINS);
	CString olFieldList = ogFactor_CCA;

	CStringArray olFieldArray;
	int ilFieldArray = ExtractItemList(olFieldList, &olFieldArray, ',');

	if (olFieldArray.GetSize() > ilCur)
	{
		CString olField = olFieldArray.GetAt(ilCur);
		if (olField.IsEmpty())
			return;

		ilFieldArray = ExtractItemList(olField, &olFieldArray, ' ');

		if (ilFieldArray == 2)
		{
			int ilFactor = atoi(olFieldArray.GetAt(1));
			CallDcins(ilFactor);
		}
	}
	else
		return;

}

bool RotationDlg::CallDcins(int ipFactor) 
{
	CCS_TRY

	int i = pomDCinsTable->pomListBox->GetTopIndex();

	CString olCkic;
	CString olCkit;
	CString olCkbs;
	CString olCkes;
	CString olCatr;

	if (bgCnamAtr)
	{
		pomDCinsTable->GetTextFieldValue(i, 0, olCkic);
		pomDCinsTable->GetTextFieldValue(i, 1, olCatr);
		pomDCinsTable->GetTextFieldValue(i, 2, olCkit);
		pomDCinsTable->GetTextFieldValue(i, 3, olCkbs);
		pomDCinsTable->GetTextFieldValue(i, 4, olCkes);
	}
	else
	{
		pomDCinsTable->GetTextFieldValue(i, 0, olCkic);
		pomDCinsTable->GetTextFieldValue(i, 1, olCkit);
		pomDCinsTable->GetTextFieldValue(i, 2, olCkbs);
		pomDCinsTable->GetTextFieldValue(i, 3, olCkes);
	}


	ogBCD.SetSort("CIC","CNAM+",true);

	int ilCount = ogBCD.GetDataCount("CIC");

	CString  olCnam;

	for(int j = 0; j < ilCount; j++)
	{
		olCnam = ogBCD.GetField("CIC",j,"CNAM");

		if(olCnam == olCkic)
			break;
	}

	if (bgCnamAtr)
	{
		if (j == ilCount)
		{
			for (int k=1; k<ipFactor; k++)
			{
				pomDCinsTable->SetIPValue(i + k, 0, "");
				pomDCinsTable->SetIPValue(i + k, 1, "");
				pomDCinsTable->SetIPValue(i + k, 2, "");
			}
		}
		else
		{
			for (int k=1; k<ipFactor; k++)
			{
				if (ogBCD.GetField("CIC",j + k,"CNAM") != "NULL")
				{
					pomDCinsTable->SetIPValue(i + k, 0, ogBCD.GetField("CIC",j + k,"CNAM"));
					pomDCinsTable->SetIPValue(i + k, 1, ogBCD.GetField("CIC",j + k,"CATR"));
					pomDCinsTable->SetIPValue(i + k, 2, ogBCD.GetField("CIC",j + k,"TERM"));
					pomDCinsTable->SetIPValue(i + k, 3, olCkbs);
					pomDCinsTable->SetIPValue(i + k, 4, olCkes); 
				}
			}
		}
	}
	else
	{
		if (j == ilCount)
		{
			for (int k=1; k<ipFactor; k++)
			{
				pomDCinsTable->SetIPValue(i + k, 0, "");
				pomDCinsTable->SetIPValue(i + k, 1, "");
			}
		}
		else
		{
			for (int k=1; k<ipFactor; k++)
			{
				if (ogBCD.GetField("CIC",j + k,"CNAM") != "NULL")
				{
					pomDCinsTable->SetIPValue(i + k, 0, ogBCD.GetField("CIC",j + k,"CNAM"));
					pomDCinsTable->SetIPValue(i + k, 1, ogBCD.GetField("CIC",j + k,"TERM"));
					pomDCinsTable->SetIPValue(i + k, 2, olCkbs);
					pomDCinsTable->SetIPValue(i + k, 3, olCkes);
				}
			}
		}
	}

	CCS_CATCH_ALL

	return true;
}

//Create the Controls dynamically - PRF 8379		
void RotationDlg::CreateDynamicControls()
{
	CRect olWindowRect;
	CRect olWindowBTRect;	



	//Arrival
	((CWnd*)GetDlgItem(IDC_APSTALIST))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);

	olWindowBTRect.left 	= olWindowRect.left - (olWindowRect.Width()+ 1);
	olWindowBTRect.top 		= olWindowRect.top; 
	olWindowBTRect.right 	= olWindowBTRect.left + olWindowRect.Width();
	olWindowBTRect.bottom 	= olWindowRect.bottom;
	pomBTCRCFPSA->Create("..", WS_VISIBLE | BS_PUSHBUTTON | BS_OWNERDRAW | WS_TABSTOP,olWindowBTRect,this,IDC_BTCRC_PSTA);
	pomBTCRCFPSA->SetFont(((CWnd*)GetDlgItem(IDC_APSTALIST))->GetFont());

	((CWnd*)GetDlgItem(IDC_APSTALIST))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);

	olWindowBTRect.left 	= olWindowRect.left - (olWindowRect.Width());
	olWindowBTRect.top 		= olWindowRect.bottom + 5;
	olWindowBTRect.right 	= olWindowBTRect.left + 100;
	olWindowBTRect.bottom 	= olWindowBTRect.top  + olWindowRect.Height() + 8 ;
	
	pomBTCRCRESOA->Create(GetString(IDS_STRING2803), WS_VISIBLE | BS_PUSHBUTTON |BS_OWNERDRAW | WS_TABSTOP, olWindowBTRect,this,IDC_BTCRC_RESOA);
	pomBTCRCRESOA->SetFont(((CWnd*)GetDlgItem(IDC_APSTALIST))->GetFont());


	((CWnd*)GetDlgItem(IDC_AGTA1LIST))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);

	olWindowBTRect.left 	= olWindowRect.left - (olWindowRect.Width()+ 1);
	olWindowBTRect.top 		= olWindowRect.top; 
	olWindowBTRect.right 	= olWindowBTRect.left + olWindowRect.Width();
	olWindowBTRect.bottom 	= olWindowRect.bottom;
	pomBTCRCAGTA1->Create("..", WS_VISIBLE | BS_PUSHBUTTON | BS_OWNERDRAW | WS_TABSTOP,olWindowBTRect,this,IDC_BTCRC_AGTA1);
	pomBTCRCAGTA1->SetFont(((CWnd*)GetDlgItem(IDC_AGTA1LIST))->GetFont());


	((CWnd*)GetDlgItem(IDC_AGTA2LIST))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);

	olWindowBTRect.left 	= olWindowRect.left - (olWindowRect.Width()+ 1);
	olWindowBTRect.top 		= olWindowRect.top; 
	olWindowBTRect.right 	= olWindowBTRect.left + olWindowRect.Width();
	olWindowBTRect.bottom 	= olWindowRect.bottom;
	pomBTCRCAGTA2->Create("..", WS_VISIBLE | BS_PUSHBUTTON | BS_OWNERDRAW | WS_TABSTOP,olWindowBTRect,this,IDC_BTCRC_AGTA2);
	pomBTCRCAGTA2->SetFont(((CWnd*)GetDlgItem(IDC_AGTA2LIST))->GetFont());

	((CWnd*)GetDlgItem(IDC_ABLT1LIST))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);

	olWindowBTRect.left 	= olWindowRect.left - (olWindowRect.Width()+ 1);
	olWindowBTRect.top 		= olWindowRect.top; 
	olWindowBTRect.right 	= olWindowBTRect.left + olWindowRect.Width();
	olWindowBTRect.bottom 	= olWindowRect.bottom;
	pomBTCRCABLT1->Create("..", WS_VISIBLE | BS_PUSHBUTTON | BS_OWNERDRAW | WS_TABSTOP,olWindowBTRect,this,IDC_BTCRC_ABLT1);
	pomBTCRCABLT1->SetFont(((CWnd*)GetDlgItem(IDC_AGTA1LIST))->GetFont());


	((CWnd*)GetDlgItem(IDC_ABLT2LIST))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);

	olWindowBTRect.left 	= olWindowRect.left - (olWindowRect.Width()+ 1);
	olWindowBTRect.top 		= olWindowRect.top; 
	olWindowBTRect.right 	= olWindowBTRect.left + olWindowRect.Width();
	olWindowBTRect.bottom 	= olWindowRect.bottom;
	pomBTCRCABLT2->Create("..", WS_VISIBLE | BS_PUSHBUTTON | BS_OWNERDRAW | WS_TABSTOP,olWindowBTRect,this,IDC_BTCRC_ABLT2);
	pomBTCRCABLT2->SetFont(((CWnd*)GetDlgItem(IDC_AGTA2LIST))->GetFont());


	//Departure
	((CWnd*)GetDlgItem(IDC_DPSTDLIST))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);

	olWindowBTRect.left 	= olWindowRect.left - (olWindowRect.Width()+ 1);
	olWindowBTRect.top 		= olWindowRect.top; 
	olWindowBTRect.right 	= olWindowBTRect.left + olWindowRect.Width();
	olWindowBTRect.bottom 	= olWindowRect.bottom;
	pomBTCRCFPSD->Create("..", WS_VISIBLE | BS_PUSHBUTTON | BS_OWNERDRAW | WS_TABSTOP,olWindowBTRect,this,IDC_BTCRC_PSTD);
	pomBTCRCFPSD->SetFont(((CWnd*)GetDlgItem(IDC_DPSTDLIST))->GetFont());


	((CWnd*)GetDlgItem(IDC_DPSTDLIST))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);
/*
	olWindowBTRect.left 	= olWindowRect.left - (olWindowRect.Width()+ 1);
	olWindowBTRect.right 	= olWindowBTRect.left + 86;
	
	olWindowBTRect.top 		= olWindowRect.bottom + 15; 
	olWindowBTRect.bottom 	= olWindowBTRect.top  + olWindowRect.Height();*/

	olWindowBTRect.left 	= olWindowRect.left - (olWindowRect.Width());
	olWindowBTRect.top 		= olWindowRect.bottom + 5;
	olWindowBTRect.right 	= olWindowBTRect.left + 100;
	olWindowBTRect.bottom 	= olWindowBTRect.top  + olWindowRect.Height() + 8 ;
	
	pomBTCRCRESOD->Create(GetString(IDS_STRING2803), WS_VISIBLE | BS_PUSHBUTTON | WS_TABSTOP | BS_OWNERDRAW,olWindowBTRect,this,IDC_BTCRC_RESOD);
	pomBTCRCRESOD->SetFont(((CWnd*)GetDlgItem(IDC_STATIC_DHISTORY))->GetFont());



	((CWnd*)GetDlgItem(IDC_DGTA1LIST))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);

	olWindowBTRect.left 	= olWindowRect.left - (olWindowRect.Width()+ 1);
	olWindowBTRect.top 		= olWindowRect.top; 
	olWindowBTRect.right 	= olWindowBTRect.left + olWindowRect.Width();
	olWindowBTRect.bottom 	= olWindowRect.bottom;
	pomBTCRCDGTD1->Create("..", WS_VISIBLE | BS_PUSHBUTTON | BS_OWNERDRAW,olWindowBTRect,this,IDC_BTCRC_DGTA1);
	pomBTCRCDGTD1->SetFont(((CWnd*)GetDlgItem(IDC_DGTA1LIST))->GetFont());


	((CWnd*)GetDlgItem(IDC_DGTA2LIST))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);

	olWindowBTRect.left 	= olWindowRect.left - (olWindowRect.Width()+ 1);
	olWindowBTRect.top 		= olWindowRect.top; 
	olWindowBTRect.right 	= olWindowBTRect.left + olWindowRect.Width();
	olWindowBTRect.bottom 	= olWindowRect.bottom;
	pomBTCRCDGTD2->Create("..", WS_VISIBLE | BS_PUSHBUTTON | BS_OWNERDRAW,olWindowBTRect,this,IDC_BTCRC_DGTA2);
	pomBTCRCDGTD2->SetFont(((CWnd*)GetDlgItem(IDC_DGTA2LIST))->GetFont());


	//For GOCC Thailand
	//Add a button to launch "Flight Data Tool" application
	//IDC_SEASON_MASK ==> Existing Button Id to get
	//        1. the location (GetWindowRect)
	//        2. Font (GetFont())

	((CWnd*)GetDlgItem(IDC_SEASON_MASK))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);

	olWindowBTRect.left 	= olWindowRect.left - (olWindowRect.Width()+ 1) - 50;
	olWindowBTRect.top 		= olWindowRect.top; 
	olWindowBTRect.right 	= olWindowBTRect.left + olWindowRect.Width();
	olWindowBTRect.bottom 	= olWindowRect.bottom;
	pomBTFlightDataTool->Create("Data Tool", WS_VISIBLE | BS_PUSHBUTTON | BS_OWNERDRAW,olWindowBTRect,this,IDC_BTCRC_DATATOOL);
	pomBTFlightDataTool->SetFont(((CWnd*)GetDlgItem(IDC_SEASON_MASK))->GetFont());

	
    if (bgShowFlightDataToolButton)
	{//show "Flight Data Tool" Button
		pomBTFlightDataTool->ShowWindow(SW_SHOW);
	}
	else
	{//hide
		pomBTFlightDataTool->ShowWindow(SW_HIDE);
	}

	//AM 20071109   To change the Label 'Position:' to 'Parking Bay'
	//              according to the 'POSITION_TEXT' setting in 'ceda.ini'
	//AM 20071109 - Changes Start

	((CWnd*)GetDlgItem(IDC_LBL_BOARDING_ARR))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);
	
	olWindowBTRect.left 	= olWindowRect.left - 100;
	olWindowBTRect.top 		= olWindowRect.top; 
	olWindowBTRect.right 	= olWindowBTRect.left + 80;
	olWindowBTRect.bottom 	= olWindowRect.bottom;

	pomLblPosArr->Create(omPositionText, WS_VISIBLE | SS_LEFT ,olWindowBTRect,this,IDC_LBL_POS_ARR_DAILY);
	pomLblPosArr->SetFont(((CWnd*)GetDlgItem(IDC_LBL_BOARDING_ARR))->GetFont());	
	pomLblPosArr->ShowWindow(SW_SHOW);

	((CWnd*)GetDlgItem(IDC_LBL_BOARDING_DEP))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);
	
	olWindowBTRect.left 	= olWindowRect.left - 80;
	olWindowBTRect.top 		= olWindowRect.top; 
	olWindowBTRect.right 	= olWindowBTRect.left + 70;
	olWindowBTRect.bottom 	= olWindowRect.bottom;

	pomLblPosDep->Create(omPositionText, WS_VISIBLE | SS_LEFT ,olWindowBTRect,this,IDC_LBL_POS_DEP_DAILY);
	pomLblPosDep->SetFont(((CWnd*)GetDlgItem(IDC_LBL_BOARDING_DEP))->GetFont());	
	pomLblPosDep->ShowWindow(SW_SHOW);
	//AM 20071109 - Changes End


	//End of GOCC Thailand add-in
	
	if(!bgReasonFlag)
	{
		pomBTCRCFPSA->ShowWindow(SW_HIDE);		
		pomBTCRCFPSD->ShowWindow(SW_HIDE);		
		pomBTCRCDGTD1->ShowWindow(SW_HIDE);
		pomBTCRCDGTD2->ShowWindow(SW_HIDE);
		pomBTCRCAGTA1->ShowWindow(SW_HIDE);
		pomBTCRCAGTA2->ShowWindow(SW_HIDE);
		pomBTCRCRESOA->ShowWindow(SW_HIDE);
		pomBTCRCRESOD->ShowWindow(SW_HIDE);
		pomBTCRCABLT1->ShowWindow(SW_HIDE);;
		pomBTCRCABLT2->ShowWindow(SW_HIDE);;

	}
	else
	{
		pomBTCRCRESOA->SetSecState( ogPrivList.GetStat("ROTATIONDLG_CE_BTCRCRESOA"));		
		pomBTCRCRESOD->SetSecState( ogPrivList.GetStat("ROTATIONDLG_CE_BTCRCRESOD"));		
		
		if(ogPrivList.GetStat("ROTATIONDLG_CE_AGta1") != '1')		
			pomBTCRCAGTA1->ShowWindow(SW_HIDE);
		if(ogPrivList.GetStat("ROTATIONDLG_CE_AGta2") != '1')		
			pomBTCRCAGTA2->ShowWindow(SW_HIDE);
		if(ogPrivList.GetStat("ROTATIONDLG_CE_DGtd1") != '1')		
			pomBTCRCDGTD1->ShowWindow(SW_HIDE);
		if(ogPrivList.GetStat("ROTATIONDLG_CE_DGtd2") != '1')		
			pomBTCRCDGTD2->ShowWindow(SW_HIDE);

	}



		((CWnd*)GetDlgItem(IDC_BTCRC_DGTA1))->GetWindowRect(olWindowRect);
		ScreenToClient(olWindowRect);

		olWindowBTRect.left 	= olWindowRect.left - (olWindowRect.Width() * 3 + 1);
		olWindowBTRect.top 		= olWindowRect.top; 
		olWindowBTRect.right 	= olWindowBTRect.left + olWindowRect.Width() * 3;
		olWindowBTRect.bottom 	= olWindowRect.bottom+ olWindowRect.Width() * 5;
		pomDRgd1->Create( WS_VISIBLE | CBS_DROPDOWNLIST | WS_VSCROLL ,olWindowBTRect,this,IDC_BTCRC_RGD1);
		pomDRgd1->SetFont(((CWnd*)GetDlgItem(IDC_DGTA1LIST))->GetFont());

		CDC* pDC = pomDRgd1->GetDC();
		pomDRgd1->SetDroppedWidth( 150 );
		pomDRgd1->ReleaseDC(pDC);

		((CWnd*)GetDlgItem(IDC_BTCRC_DGTA2))->GetWindowRect(olWindowRect);
		ScreenToClient(olWindowRect);


		olWindowBTRect.left 	= olWindowRect.left - (olWindowRect.Width() * 3 + 1);
		olWindowBTRect.top 		= olWindowRect.top; 
		olWindowBTRect.right 	= olWindowBTRect.left + olWindowRect.Width() * 3;
		olWindowBTRect.bottom 	= olWindowRect.bottom + olWindowRect.Width() * 5;
		pomDRgd2->Create( WS_VISIBLE | CBS_DROPDOWNLIST  | WS_VSCROLL ,olWindowBTRect,this,IDC_BTCRC_RGD2);
		pomDRgd2->SetFont(((CWnd*)GetDlgItem(IDC_DGTA1LIST))->GetFont());
	
		pDC = pomDRgd2->GetDC();
		pomDRgd2->SetDroppedWidth( 150 );
		pomDRgd2->ReleaseDC(pDC);

	if(!bgGateFIDSRemark)
	{
		pomDRgd1->ShowWindow(SW_HIDE);
		pomDRgd2->ShowWindow(SW_HIDE);
	}
	else
	{
		char clStat = ogPrivList.GetStat("ROTATIONDLG_CB_Rgd1");
		switch (clStat)
		{
			case '1': pomDRgd1->EnableWindow(TRUE); break;
			case '0': pomDRgd1->EnableWindow(FALSE); break;
			case '-': pomDRgd1->ShowWindow(SW_HIDE); break;
		}
		clStat = ogPrivList.GetStat("ROTATIONDLG_CB_Rgd2");
		switch (clStat)
		{
			case '1': pomDRgd2->EnableWindow(TRUE); break;
			case '0': pomDRgd2->EnableWindow(FALSE); break;
			case '-': pomDRgd2->ShowWindow(SW_HIDE); break;
		}
	}

	//Add Comment for Cash button
	//Christine	
	((CWnd*)GetDlgItem(IDC_PAID))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);
	CRect olTmpRect;
	 
	olWindowBTRect.right 	= olWindowRect.left - 10;
	olWindowBTRect.bottom 	= olWindowRect.bottom + 5;
	olWindowBTRect.left 	= olWindowBTRect.right  - 150;
	olWindowBTRect.top 		= olWindowRect.top + 5; 

	DWORD dwStyle = WS_CHILD|WS_VISIBLE|WS_TABSTOP|ES_AUTOHSCROLL;

	pomBTCRCom->CreateEx(WS_EX_CLIENTEDGE , _T("EDIT"), "", dwStyle, olWindowBTRect, this, IDC_BTCRC_CASH_COMMENT);
	//pomBTCRCom->Create( WS_VISIBLE | CBS_DROPDOWNLIST | WS_VSCROLL ,olWindowBTRect,this,IDC_BTCRC_CASH_COMMENT);
	pomBTCRCom ->SetFont(((CWnd*)GetDlgItem(IDC_ACLE))->GetFont());
	
	

	//Added Comment lable
	//IDC_LBL_CASH_COMMENT
	
	((CWnd*)GetDlgItem(IDC_BTCRC_CASH_COMMENT))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);
	olWindowBTRect.left 	= olWindowRect.left;
	olWindowBTRect.top 		= olWindowRect.top - 18; 
	olWindowBTRect.right 	= olWindowRect.right;
	olWindowBTRect.bottom 	= olWindowRect.bottom - 22;	
	pomLBLCCom->Create("Comment for Payment", WS_VISIBLE | SS_LEFT ,olWindowBTRect,this,IDC_LBL_CASH_COMMENT);
	pomLBLCCom->SetFont(((CWnd*)GetDlgItem(IDC_LBL_BOARDING_DEP))->GetFont());	
	
	//lgp
	((CWnd*)GetDlgItem(IDC_REGN))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);
	//CRect olTmpRect;	 
	olWindowBTRect.left 	= olWindowRect.right + 2;
	olWindowBTRect.bottom 	= olWindowRect.bottom;
	olWindowBTRect.right 	= olWindowBTRect.left  + 78;
	olWindowBTRect.top 		= olWindowRect.top; 
	pomBMTOW->CreateEx(WS_EX_CLIENTEDGE , _T("EDIT"), "", dwStyle, olWindowBTRect, this, IDC_BTCRC_MTOW);

	pomBMTOW ->SetFont(((CWnd*)GetDlgItem(IDC_ACLE))->GetFont());
	
	//lgp_Mtow_label
	((CWnd*)GetDlgItem(IDC_REGN))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);
	olWindowBTRect.left 	= olWindowRect.right + 2;
	olWindowBTRect.bottom 	= olWindowRect.bottom - 19;
	olWindowBTRect.right 	= olWindowBTRect.left  + 78;
	olWindowBTRect.top 		= olWindowRect.top - 17; 
	pomLMTOW->Create("MTOW:", WS_VISIBLE | SS_LEFT ,olWindowBTRect,this,IDC_LBL_MTOW);
	pomLMTOW->SetFont(((CWnd*)GetDlgItem(IDC_LBL_BOARDING_DEP))->GetFont());	
	
	//UFIS-1120 AM:20111031
	((CWnd*)GetDlgItem(IDC_SEASON_MASK))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);
	olWindowBTRect.left 	= olWindowRect.left - 150;
	olWindowBTRect.top 		= olWindowRect.top; 
	olWindowBTRect.right 	= olWindowBTRect.left + 50;
	olWindowBTRect.bottom 	= olWindowRect.bottom;	
	pomBTActivity->Create("Activities", WS_VISIBLE | BS_PUSHBUTTON | BS_OWNERDRAW | WS_TABSTOP,olWindowBTRect,this,IDC_BTCRC_ACTIVITY);
	pomBTActivity->SetFont(((CWnd*)GetDlgItem(IDC_SEASON_MASK))->GetFont());

	if(!bgDailyShowActivities)
	{
		pomBTActivity->ShowWindow(SW_HIDE);
	}
	
	SetCashCommentDisplay();
	
	
	char pclConfigPath[256];

    if (getenv("CEDA") == NULL)
        strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
    else
        strcpy(pclConfigPath, getenv("CEDA"));

	char pc[100];
	GetPrivateProfileString( ogAppName, "LABLE_KEYCODE", "", pc, sizeof( pc ) -1, pclConfigPath );
	
	if(strlen(pc) > 0)
	{
		m_LB_KEY1.SetWindowText(pc);
		m_LB_KEY2.SetWindowText(pc);
	}

	char pc2[100];
	GetPrivateProfileString( ogAppName, "LABLE_CAROUSEL", "", pc2, sizeof( pc2 ) -1, pclConfigPath);
	
	
	if(strlen(pc2) > 0)
	{
		m_LB_CAROSEL.SetWindowText(pc2); 	
	}

	SetCashButtonInBothFlights();

	SetChocksOnOffControls();
	
	SetFixedControls();

	SetModeOfPayControls();

	SetAOGControl();
	SetADHOCControl();
	SetEFPSControl();
	
}		

void RotationDlg::OnClickCrcPsta() 
{	
	// Get all the reasons related to position //PRF 8379 
	if((omReasons.blPstaResf == true) || (!omReasons.olPstaUrno.IsEmpty()))
	{
		omReasons.olPstaUrno = CFPMSApp::GetSelectedReason(this,"POSF",omReasons.olPstaUrno);
		if(omReasons.olPstaUrno.GetLength()>0)
		{
			omReasons.blPstaResf = false;
			pomBTCRCFPSA->SetColors(::GetSysColor(COLOR_BTNFACE), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
		}
	}
}

void RotationDlg::OnClickCrcAGta1() 
{
	if((omReasons.blAGta1Resf == true) || (!omReasons.olAGta1Urno.IsEmpty()))
	{
		omReasons.olAGta1Urno = CFPMSApp::GetSelectedReason(this,"GATF",omReasons.olAGta1Urno);
		if(omReasons.olAGta1Urno.GetLength()>0)
		{
			omReasons.blAGta1Resf = false;
			pomBTCRCAGTA1->SetColors(::GetSysColor(COLOR_BTNFACE), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
		}
	}
}

void RotationDlg::OnClickCrcAGta2() 
{
	if((omReasons.blAGta2Resf == true) || (!omReasons.olAGta2Urno .IsEmpty()))
	{
		omReasons.olAGta2Urno = CFPMSApp::GetSelectedReason(this,"GATF",omReasons.olAGta2Urno);
		if(omReasons.olAGta2Urno.GetLength()>0)
		{
			omReasons.blAGta2Resf = false;
			pomBTCRCAGTA2->SetColors(::GetSysColor(COLOR_BTNFACE), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
		}
	}
}


void RotationDlg::OnClickCrcPstd()  
{
	// Get all the reasons related to position //PRF 8379 
	if((omReasons.blPstdResf == true) || (!omReasons.olPstdUrno.IsEmpty()))
	{
		omReasons.olPstdUrno = CFPMSApp::GetSelectedReason(this,"POSF",omReasons.olPstdUrno);
		if(omReasons.olPstdUrno.GetLength()>0)
		{
			omReasons.blPstdResf = false;
			pomBTCRCFPSD->SetColors(::GetSysColor(COLOR_BTNFACE), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
		}
	}
}

void RotationDlg::OnClickCrcDGtd1() 
{
	if((omReasons.blDGtd1Resf == true) || (!omReasons.olDGtd1Urno.IsEmpty()))
	{
		omReasons.olDGtd1Urno = CFPMSApp::GetSelectedReason(this,"GATF",omReasons.olDGtd1Urno);
		if(omReasons.olDGtd1Urno.GetLength()>0)
		{
			omReasons.blDGtd1Resf = false;
			pomBTCRCDGTD1->SetColors(::GetSysColor(COLOR_BTNFACE), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
		}
	}
}

void RotationDlg::OnClickCrcDGtd2() 
{
	if((omReasons.blDGtd2Resf == true) || (!omReasons.olDGtd2Urno.IsEmpty()))
	{
		omReasons.olDGtd2Urno = CFPMSApp::GetSelectedReason(this,"GATF",omReasons.olDGtd2Urno);
		if(omReasons.olDGtd2Urno.GetLength()>0)
		{
			omReasons.blDGtd2Resf = false;
			pomBTCRCDGTD2->SetColors(::GetSysColor(COLOR_BTNFACE), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
		}
	}
}

void RotationDlg::OnClickCrcABlt1() 
{
	if((omReasons.blABlt1Resf == true) || (!omReasons.olABlt1Urno.IsEmpty()))
	{
		omReasons.olABlt1Urno = CFPMSApp::GetSelectedReason(this,"BELT",omReasons.olABlt1Urno);
		if(omReasons.olABlt1Urno.GetLength()>0)
		{
			omReasons.blABlt1Resf = false;
			pomBTCRCABLT1->SetColors(::GetSysColor(COLOR_BTNFACE), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
		}
	}
}

void RotationDlg::OnClickCrcABlt2() 
{
	if((omReasons.blABlt2Resf == true) || (!omReasons.olABlt2Urno.IsEmpty()))
	{
		omReasons.olABlt2Urno = CFPMSApp::GetSelectedReason(this,"BELT",omReasons.olABlt2Urno);
		if(omReasons.olABlt2Urno.GetLength()>0)
		{
			omReasons.blABlt2Resf = false;
			pomBTCRCABLT2->SetColors(::GetSysColor(COLOR_BTNFACE), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
		}
	}
}



void RotationDlg::OnClickCrcResoA() 
{
	if(prmAFlight != NULL)
	{
		ReasonOverviewDlg polReasonOverviewDlg(this, prmAFlight->Urno);
		polReasonOverviewDlg.DoModal();
	}
}


void RotationDlg::OnClickCrcResoD() 
{
	if(prmDFlight != NULL)
	{
		ReasonOverviewDlg polReasonOverviewDlg(this, prmDFlight->Urno);
		polReasonOverviewDlg.DoModal();
	}
}

void RotationDlg::CheckForChangeReasons(CCSButtonCtrl* ropButtonCtrl,
										const CString& ropValue,
										const CString& ropChangedValue,
										bool* bpReasonFlag, char cpAdid)
{
	if(bgReasonFlag && (strlen(ropValue) > 0))
	{
		// Check the difference in position selection			
		if(strcmp(ropValue,ropChangedValue) != 0)
		{
			CTime olbesttime; 	
			if(cpAdid == 'A')
			{
				if(prmAFlight->Tifa == NULL)
				{
					olbesttime = prmAFlight->Stoa;
				}
				else
				{
					olbesttime = prmAFlight->Tifa;
				}
			}
			else if(cpAdid == 'D')
			{				
				if(prmDFlight->Tifd == NULL)
				{
					olbesttime = prmDFlight->Stod;
				}
				else
				{
					olbesttime = prmDFlight->Tifd;
				}
			}
			else
			{
				return;
			}

			CTime olCurrentTime ;
			
			if(bgGatPosLocal)			
				olCurrentTime = CTime::GetCurrentTime();						
			else
				GetCurrentUtcTime(olCurrentTime);

			CTime olDiffTime = olbesttime - ogXminutesBufferTime;

			TRACE("\n Curr Time %s", CTime::GetCurrentTime().Format("%H:%M:%S"));
			TRACE("\n diff Time %s", olDiffTime.Format("%H:%M:%S"));
			TRACE("\n best Time %s", olbesttime.Format("%H:%M:%S"));

			if(olDiffTime < olCurrentTime)
			{
				ropButtonCtrl->SetColors(RGB(255,0,0), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
				*bpReasonFlag = true;
			}
		}
		else
		{
			ropButtonCtrl->SetColors(::GetSysColor(COLOR_BTNFACE), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
			*bpReasonFlag = false;
		}
	}
}


void RotationDlg::OnClickCrcDataTool() 
{
	//MessageBox("Data Tool Click");
	LaunchFlightDataTool( "1" );
}


//////////////////////////////////////////////////////////////////////////////
///Launch 'Flight Data Tool'
///In	==> opPermit - String for permission of the Flight Data Tool application
//////////////////////////////////////////////////////////////////////////////
void RotationDlg::LaunchFlightDataTool(CString opPermit) 
{
	if (prmAFlight->Urno <= 0)
		return;

	char pclConfigPath[256];
	char pclExcelPath[256];

	if (getenv("CEDA") == NULL)
		strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
	else
		strcpy(pclConfigPath, getenv("CEDA"));

	GetPrivateProfileString(ogAppName, "FLIGHT_DATA_TOOL", "DEFAULT",
		pclExcelPath, sizeof pclExcelPath, pclConfigPath);


	if(!strcmp(pclExcelPath, "DEFAULT"))
		MessageBox(GetString(IDS_STRING2575), GetString(ST_FEHLER), MB_ICONERROR);

	CPoint point;
	::GetCursorPos(&point);

	CString olUser = ogBasicData.omUserID;
	CString olPermits ;
	//olPermits = ogPrivList.GetStat("ROTATIONDLG_CB_ARep");
	olPermits = opPermit;

	CString olTime = "U";
	if (bmLocalTime)
		olTime = "L";

	char slRunTxt[512] = ""; 
	sprintf( slRunTxt,"ALERT_,%d,%d|%s,%s,%s|%d,%d", prmAFlight->Urno, prmDFlight->Urno, olUser, olPermits, olTime, point.x, point.y );
	//MessageBox( slRunTxt );

	char *args[4];
	args[0] = "child";
	args[1] = slRunTxt;
	args[2] = NULL;
	args[3] = NULL;
	int ilReturn = _spawnv(_P_NOWAIT,pclExcelPath,args);
}


void RotationDlg::SaveLogo() 
{
	if(prmDFlight == NULL)
		return;

	int ilCurSel;

	CString olGatFlgu = "";
	CString olGatFlgu2 = "";

	if((ilCurSel = m_CL_DGatLogo.GetCurSel()) != CB_ERR)
	{
		m_CL_DGatLogo.GetLBText(ilCurSel, olGatFlgu);
		olGatFlgu = olGatFlgu.Right(11);
		olGatFlgu.TrimRight();
		olGatFlgu.TrimLeft();
	}
	if((ilCurSel = m_CL_DGatLogo2.GetCurSel()) != CB_ERR)
	{
		m_CL_DGatLogo2.GetLBText(ilCurSel, olGatFlgu2);
		olGatFlgu2 = olGatFlgu2.Right(11);
		olGatFlgu2.TrimRight();
		olGatFlgu2.TrimLeft();
	}



	omFlzData.SaveGatLogo(prmDFlight->Urno, olGatFlgu, olGatFlgu2);


	// CIC

	
	omFlzData.SaveCicLogo(omDCins, omCicLogoArray);



}




void RotationDlg::OnDcicLogo1() 
{

	CCS_TRY

	int ilLine = pomDCinsTable->pomListBox->GetTopIndex();
	int ilCol = 7;

	if (bgCnamAtr)
		ilCol = 8;

	CString olText;
	pomDCinsTable->GetTextFieldValue(ilLine, ilCol, olText);

	AwBasicDataDlg *polDlg = NULL;

	polDlg = new AwBasicDataDlg(this, "FLG_CKI","ALC2,ALC3,LGSN,LGFN", "LGSN+", olText,true);

	//polDlg->SetSecState("SEASONDLG_CE_DCins");
	
	if(polDlg->DoModal() == IDOK)
	{
		pomDCinsTable->SetIPValue(ilLine, ilCol, polDlg->GetField("LGSN"));
	}
	delete polDlg;

	CCS_CATCH_ALL

}


void RotationDlg::OnDcicLogo2() 
{
	CCS_TRY

	int ilLine = pomDCinsTable->pomListBox->GetTopIndex();
	int ilCol = 7;

	if (bgCnamAtr)
		ilCol = 8;

	CString olText;
	pomDCinsTable->GetTextFieldValue(ilLine + 1, ilCol, olText);

	AwBasicDataDlg *polDlg = NULL;

	polDlg = new AwBasicDataDlg(this, "FLG_CKI","ALC2,ALC3,LGSN,LGFN", "LGSN+", olText, true);

	//polDlg->SetSecState("SEASONDLG_CE_DCins");
	
	if(polDlg->DoModal() == IDOK)
	{
		pomDCinsTable->SetIPValue(ilLine + 1, ilCol, polDlg->GetField("LGSN"));
	}
	delete polDlg;

	CCS_CATCH_ALL
}

void RotationDlg::OnDcicLogo3() 
{
	CCS_TRY

	int ilLine = pomDCinsTable->pomListBox->GetTopIndex();
	int ilCol = 7;

	if (bgCnamAtr)
		ilCol = 8;

	CString olText;
	pomDCinsTable->GetTextFieldValue(ilLine + 2, ilCol, olText);

	AwBasicDataDlg *polDlg = NULL;

	polDlg = new AwBasicDataDlg(this, "FLG_CKI","ALC2,ALC3,LGSN,LGFN", "LGSN+", olText, true);

	//polDlg->SetSecState("SEASONDLG_CE_DCins");
	
	if(polDlg->DoModal() == IDOK)
	{
		pomDCinsTable->SetIPValue(ilLine + 2, ilCol, polDlg->GetField("LGSN"));
	}
	delete polDlg;

	CCS_CATCH_ALL
}

void RotationDlg::OnDcicLogo4() 
{
	CCS_TRY

	int ilLine = pomDCinsTable->pomListBox->GetTopIndex();
	int ilCol = 7;

	if (bgCnamAtr)
		ilCol = 8;

	CString olText;
	pomDCinsTable->GetTextFieldValue(ilLine + 3, ilCol, olText);

	AwBasicDataDlg *polDlg = NULL;

	polDlg = new AwBasicDataDlg(this, "FLG_CKI","ALC2,ALC3,LGSN,LGFN", "LGSN+", olText, true);

	//polDlg->SetSecState("SEASONDLG_CE_DCins");
	
	if(polDlg->DoModal() == IDOK)
	{
		pomDCinsTable->SetIPValue(ilLine + 3, ilCol, polDlg->GetField("LGSN"));
	}
	delete polDlg;

	CCS_CATCH_ALL


}



void RotationDlg::OnDNfes()
{
	if(m_CB_DNfes.GetCheck())
	{
		m_CE_DNfes.ShowWindow(SW_SHOW);
		m_CE_DNfes.EnableWindow(TRUE);
	}
	else
	{
		m_CE_DNfes.ShowWindow(SW_HIDE);
		m_CE_DNfes.EnableWindow(FALSE);
	}
	OnEditChanged( 0,0);

}



void RotationDlg::OnANfes()
{
	if(m_CB_ANfes.GetCheck())
	{
		m_CE_ANfes.ShowWindow(SW_SHOW);
		m_CE_ANfes.EnableWindow(TRUE);
	}
	else
	{
		m_CE_ANfes.ShowWindow(SW_HIDE);
		m_CE_ANfes.EnableWindow(FALSE);
	}
	OnEditChanged( 0,0);

}


void RotationDlg::OnSelchangeACxxReason() 
{
	OnEditChanged(0,0);	
}

void RotationDlg::OnSelchangeDCxxReason() 
{
	OnEditChanged(0,0);	
}





void RotationDlg::DShowChaTable()
{
	if(!bgChuteDisplay)
		return;


	CCSEDIT_ATTRIB rlAttribC1;
	CCSEDIT_ATTRIB rlAttribC2;
	CCSEDIT_ATTRIB rlAttribC3;
	CCSEDIT_ATTRIB rlAttribC4;
	CCSEDIT_ATTRIB rlAttribC5;

	CCSPtrArray<TABLE_COLUMN> olColList;
	TABLE_COLUMN rlColumnData;

	int ilLc ;

	pomDChuTable->ResetContent();


	rlAttribC1.Style = ES_UPPERCASE;


	rlAttribC2.Format = "X";
	rlAttribC2.TextMinLenght = 0;
	rlAttribC2.TextMaxLenght = 1;
	rlAttribC2.Style = ES_UPPERCASE;

	rlAttribC3.Type = KT_TIME;
	rlAttribC3.ChangeDay	= true;
	rlAttribC3.TextMaxLenght = 7;

	rlAttribC4.Type = KT_TIME;
	rlAttribC4.ChangeDay	= true;
	rlAttribC4.TextMaxLenght = 7;

	//For PRF 8174
	rlAttribC5.Type = KT_STRING;
	rlAttribC5.ChangeDay = true;
	rlAttribC5.TextMaxLenght = 60;


	rlColumnData.Font = &ogMS_Sans_Serif_8;
	rlColumnData.Alignment = COLALIGN_LEFT;

	//if(ogPrivList.GetStat("ROTATIONDLG_CE_DCins") != '1')
	//	rlColumnData.BkColor = RGB(192,192,192);

	CHADATA *prlCha;

	CString olLnam;

	CString olMax = "   ";
	CString olMin = "zzz";


	CTime olTmpTime;
	for (ilLc = 0; ilLc < omChaData.omData.GetSize(); ilLc++)
	{
		prlCha = &omChaData.omData[ilLc];

		olLnam = CString(prlCha->Lnam);

		rlColumnData.blIsEditable = false;	
		rlColumnData.Text = olLnam;
		rlColumnData.EditAttrib = rlAttribC1;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		olTmpTime = prlCha->Tifb;
		if(bmLocalTime) ogBasicData.UtcToLocal(olTmpTime);
		rlColumnData.Text = DateToHourDivString(olTmpTime, prmDFlight->Stod);
		rlColumnData.EditAttrib = rlAttribC3;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		
		olTmpTime = prlCha->Tife;
		if(bmLocalTime) ogBasicData.UtcToLocal(olTmpTime);
		rlColumnData.Text = DateToHourDivString(olTmpTime, prmDFlight->Stod);
		rlColumnData.EditAttrib = rlAttribC4;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		
		olTmpTime = prlCha->Stob;
		if(bmLocalTime) ogBasicData.UtcToLocal(olTmpTime);
		rlColumnData.Text = DateToHourDivString(olTmpTime, prmDFlight->Stod);
		rlColumnData.EditAttrib = rlAttribC3;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		
		olTmpTime = prlCha->Stoe;
		if(bmLocalTime) ogBasicData.UtcToLocal(olTmpTime);
		rlColumnData.Text = DateToHourDivString(olTmpTime, prmDFlight->Stod);
		rlColumnData.EditAttrib = rlAttribC4;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		
		pomDChuTable->AddTextLine(olColList, (void*)(NULL));
		olColList.DeleteAll();


		olLnam.TrimRight();

		if((olLnam < olMin) && !olLnam.IsEmpty())
			olMin = olLnam;

		if((olLnam > olMax) && !olLnam.IsEmpty())
			olMax = olLnam;
		
		olMax.TrimRight();
		olMin.TrimRight();

	}


	pomDChuTable->DisplayTable();

	if(olMin != "zzz")
	{
		if(olMin != olMax)
			m_CE_ChuFrTo.SetWindowText(olMin + CString("-") + olMax);
		else
			m_CE_ChuFrTo.SetWindowText(olMin );
	}
	else
		m_CE_ChuFrTo.SetWindowText(CString(" ") );


}


void RotationDlg::ProcessChaChange(CHADATA *prpCha)
{
	if(prmDFlight->Urno <= 0) 
		return;

	if(prpCha->Rurn = prmDFlight->Urno)
		DShowChaTable();


}



void RotationDlg::OnLock() 
{
	if(bmLocked)
	{

		if(bmChanged)
		{
			if (!CheckPostFlight())
			{
				if(MessageBox(GetString(IDS_STRING942), GetString(ST_FRAGE), MB_ICONEXCLAMATION | MB_YESNO) == IDNO)
						return;
			}
		}
		
		bmChanged = false;
		m_CB_Ok.SetColors(::GetSysColor(COLOR_BTNFACE), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));

		bmLocked = false;
		m_CB_Lock.SetColors(::GetSysColor(COLOR_BTNFACE), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
		NewData(pomParent, lmRkey, lmCalledBy, omAdid, bmLocalTime, bmRotationReadOnly);
		m_CB_Lock.SetWindowText(GetString(IDS_STRING2875));
	}
	else
	{
		bmLocked = true;
		m_CB_Lock.SetColors( RGB(255,255, 0) , ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
		ogRotationDlgFlights.UnRegister();
		ogDdx.UnRegister(this,NOTUSED);
		m_CB_Lock.SetWindowText(GetString(IDS_STRING2876));
	
	}
}


void RotationDlg::Register() 
{

	ogRotationDlgFlights.UnRegister();
	ogRotationDlgFlights.Register();

	ogDdx.UnRegister(this,NOTUSED);

    ogDdx.Register(this, RG_DLG_FLIGHT_CHANGE, CString("FLIGHTDLG"), CString("Flight Update/new"), FlightTableCf);
    ogDdx.Register(this, RG_DLG_ROTATION_CHANGE, CString("FLIGHTDLG"), CString("Flight Update/new"), FlightTableCf);
    ogDdx.Register(this, R_DLG_FLIGHT_CHANGE, CString("FLIGHTDLG"), CString("Flight Update/new"), FlightTableCf);
    ogDdx.Register(this, R_DLG_ROTATION_CHANGE, CString("FLIGHTDLG"), CString("Flight Update/new"), FlightTableCf);
	ogDdx.Register(this, CCA_ROTDLG_NEW,	CString("CCADATA"), CString("Cca-changed"),	FlightTableCf);
	ogDdx.Register(this, CCA_ROTDLG_DELETE,	CString("CCADATA"), CString("Cca-changed"),	FlightTableCf);
	ogDdx.Register(this, CCA_ROTDLG_CHANGE,	CString("CCADATA"), CString("Cca-changed"),	FlightTableCf);
	ogDdx.Register(this, R_DLG_RELOAD_CCA,	CString("ROTATIONDLG"), CString("Cca-Reload"),	FlightTableCf);
	ogDdx.Register(this, DIACHA_CHANGE,	CString("FLIGHTDIAVIEWER"), CString("Flight Changed"), FlightTableCf);
	ogDdx.Register(this, DIACHA_DELETE,	CString("FLIGHTDIAVIEWER"), CString("Flight Changed"), FlightTableCf);

	ogDdx.Register((void *)this,BC_DCF_INSERT,	CString("DCFDATA"), CString("DCF-changed"),	ProcessDcfCf);
	ogDdx.Register((void *)this,BC_DCF_UPDATE,		CString("DCFDATA"), CString("DCF-new"),		ProcessDcfCf);
	ogDdx.Register((void *)this,BC_DCF_DELETE,	CString("DCFDATA"), CString("DCF-deleted"),	ProcessDcfCf);
	
	ogDdx.Register((void *)this, FPE_INSERT, CString("FPEDATA"), CString("FPE Insert"),	FlightTableCf);
	ogDdx.Register((void *)this, FPE_UPDATE, CString("FPEDATA"), CString("FPE Update"),	FlightTableCf);
	ogDdx.Register((void *)this, FPE_DELETE, CString("FPEDATA"), CString("FPE Delete"),	FlightTableCf);


}



void RotationDlg::AutoLock() 
{
	if(!bmLocked)
	{
		bmLocked = true;
		m_CB_Lock.SetColors( RGB(0,255, 0) , ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
		ogRotationDlgFlights.UnRegister();
		ogDdx.UnRegister(this,NOTUSED);
		m_CB_Lock.SetWindowText(GetString(IDS_STRING2876));
	
	}

}


void RotationDlg::OnGoAround() 
{
	OnEditChanged(0,0);

	CTimeSpan olSpan = CTimeSpan(0,0,15,0);

 	CTime olTime = CTime::GetCurrentTime();
 	CTime olTime2 = CTime::GetCurrentTime();


 	if (!bmLocalTime)
	{
		ogBasicData.LocalToUtc(olTime);
	}
	olTime += olSpan;


	ogBasicData.LocalToUtc(olTime2);
	CString olRem = " GOAROUND " + olTime2.Format("%H:%M");; 


	if( m_CB_GoAround.GetCheck() )
	{
		m_AEtai = DateToHourDivString(olTime, prmAFlight->Stoa);
		m_CE_AEtai.SetWindowText(m_AEtai);
		m_CE_AEtai.SetFocus();
		m_CB_GoAround.SetFocus();

		m_CE_ARem1.GetWindowText(m_ARem1);

		m_ARem1 += olRem;
		m_CE_ARem1.SetWindowText(m_ARem1);

	}

}



void RotationDlg::OnAVip()
{
	OnEditChanged( 0,0);
}

void RotationDlg::OnDVip()
{
	OnEditChanged( 0,0);
}

void RotationDlg::OnAFpsa()
{
	OnEditChanged( 0,0);
}

void RotationDlg::OnDFpsd()
{
	OnEditChanged( 0,0);
}



void RotationDlg::OnVipADlg() 
{
	if(prmAFlight->Urno > 0 )
	{
		RotationVipDlg polRotationVipDlg(this, prmAFlight);
		if (CheckPostFlight())
			polRotationVipDlg.setStatus(RotationVipDlg::Status::POSTFLIGHT);
		else
			polRotationVipDlg.setStatus(RotationVipDlg::Status::UNKNOWN);

		if (polRotationVipDlg.DoModal() == IDOK)
		{
		}
	}

	UpdateVipButton(true);
}


void RotationDlg::OnVipDDlg() 
{
	if(prmDFlight->Urno > 0 )
	{
		RotationVipDlg polRotationVipDlg(this, prmDFlight);
		if (CheckPostFlight())
			polRotationVipDlg.setStatus(RotationVipDlg::Status::POSTFLIGHT);
		else
			polRotationVipDlg.setStatus(RotationVipDlg::Status::UNKNOWN);

		if (polRotationVipDlg.DoModal() == IDOK)
		{
		}
	}

	UpdateVipButton(true);

}


void RotationDlg::UpdateVipButton(bool blRead) 
{

	CString olWhere;

	long llAUrno = 0;
	long llDUrno = 0;


	int llCountA = 0;
	int llCountD = 0;


	if(prmAFlight != NULL )
	{
		llAUrno = prmAFlight->Urno;
	}
	if(prmDFlight != NULL )
	{
		llDUrno = prmDFlight->Urno;
	}

	if(blRead)
		ogVipData.Read( llAUrno, llDUrno);

	llCountA = ogVipData.GetCount( llAUrno);
	llCountD = ogVipData.GetCount( llDUrno);

	CString olText;

	olText.Format("%s (%d)", GetString(IDS_STRING1556), llCountA, 10);

	m_CB_VipADlg.SetWindowText(olText);

	olText.Format("%s (%d)", GetString(IDS_STRING1556), llCountD, 10);

	m_CB_VipDDlg.SetWindowText(olText);



	if( llCountA > 0)
	{
		m_CB_VipADlg.SetColors(TOWING, ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
	}
	else
	{
		m_CB_VipADlg.SetColors(::GetSysColor(COLOR_BTNFACE), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
	}

	if( llCountD > 0)
	{
		m_CB_VipDDlg.SetColors(TOWING, ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
	}
	else
	{
		m_CB_VipDDlg.SetColors(::GetSysColor(COLOR_BTNFACE), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
	}


}




CString RotationDlg::GetDelayCount(CTAB *tab)
{
	long  cntTab = tab->GetLines();
	long i =0;
	int cntdelay=0;
	for(i=0;i<cntTab;i++)
	{
		
		CString tmp = tab->GetFieldValue(i,"DECS");

		if(tmp == "")
		{
			cntdelay++;
		}
		else
		{
		
			tmp.TrimRight();

			if(tmp.GetLength() > 0)
			{
				continue;
			}
			else
			{
				cntdelay++;
			}
		}
	}
	CString strDelayCnt;
	strDelayCnt.Format("%ld",cntdelay);
	
	return strDelayCnt;
}

void RotationDlg::ResetTabs()
{
	if(bgMulitpleDelayCodes == true)
	{
		CString olWhere;
		tabDelayArrival.ResetContent();
		if(prmAFlight->Urno != 0)
		{
			char pclUrno[20]="";
			sprintf(pclUrno, "%d", prmAFlight->Urno);
			olWhere = "WHERE FURN=" + CString(pclUrno);
			if(tabDelayArrival.CedaAction("RT", "DCFTAB", tabDelayArrival.GetLogicalFieldList(), "", olWhere.GetBuffer(0)) == TRUE)
			{
			}
			
			olWhere = "WHERE FURN=" + CString(pclUrno);
			
			sprintf(pclUrno, "%ld", tabDelayArrival.GetLineCount());
			
			//CString olText = "Delays (" + CString(pclUrno) + ")";

			
			CString olText = "Delays (" + GetDelayCount(&tabDelayArrival) + ")";

		
			m_DelayArrBtn.SetWindowText(olText);

			if (m_CB_Ok.m_hWnd)
			    m_DelayArrBtn.SetFont(m_CB_Ok.GetFont());

			if(tabDelayArrival.GetLineCount() > 0)
				m_DelayArrBtn.SetColors(RGB(255,128, 0), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
			else
				m_DelayArrBtn.SetColors(::GetSysColor(COLOR_BTNFACE), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
			m_DelayArrBtn.UpdateWindow();
		}
		else
		{
			m_DelayArrBtn.SetColors(::GetSysColor(COLOR_BTNFACE), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
			m_DelayArrBtn.SetWindowText("Delay");
			m_DelayArrBtn.UpdateWindow();
		}
		
		tabDelayDeparture.ResetContent();
		if(prmDFlight->Urno != 0)
		{
			char pclUrno[20]="";
			sprintf(pclUrno, "%d", prmDFlight->Urno);
			olWhere = "WHERE FURN=" + CString(pclUrno);
			if(tabDelayDeparture.CedaAction("RT", "DCFTAB", tabDelayDeparture.GetLogicalFieldList(), "", olWhere.GetBuffer(0)) == TRUE)
			{
//				tabDelayDeparture.InsertTextLine("URNO,FURN,DURN,Y,REMA,DURA,CDAT,LSTU,USEC,USEU", TRUE);
			}
			sprintf(pclUrno, "%ld", tabDelayDeparture.GetLineCount());
			CString olText = "Delays (" + GetDelayCount(&tabDelayDeparture)  + ")";
			m_DelayDepBtn.SetWindowText(olText);

			if (m_CB_Ok.m_hWnd)
			    m_DelayDepBtn.SetFont(m_CB_Ok.GetFont());

			if(tabDelayDeparture.GetLineCount() > 0)
				m_DelayDepBtn.SetColors(RGB(255,128, 0), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
			else
				m_DelayDepBtn.SetColors(::GetSysColor(COLOR_BTNFACE), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
			m_DelayDepBtn.UpdateWindow();
		}
		else
		{
			m_DelayDepBtn.SetColors(::GetSysColor(COLOR_BTNFACE), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
			m_DelayDepBtn.SetWindowText("Delay");
			m_DelayDepBtn.UpdateWindow();
		}
	}
}




void RotationDlg::SetCashButtonInBothFlights()
{
	//m_CB_Paid.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_Paid"));
	

	CRect olWindowRect;
	CRect olWindowBTRect;
	
	CFont* tmp = ((CWnd*) this -> GetDlgItem(IDC_NEWVIPA))->GetFont();
	DWORD buttonStyle = WS_VISIBLE | BS_PUSHBUTTON | BS_OWNERDRAW |WS_TABSTOP;

	((CWnd*)GetDlgItem(IDC_NEWVIPA))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);

	olWindowBTRect.top 		= olWindowRect.top; 
	olWindowBTRect.bottom 	= olWindowRect.bottom;
	//olWindowBTRect.left 	= olWindowRect.left - 80;
	olWindowBTRect.left 	= olWindowRect.left - 50;
	olWindowBTRect.right 	= olWindowRect.left - 2;

	olWindowBTRect.bottom 	= olWindowRect.bottom;
	m_CashArrBtn.Create("Cash", buttonStyle ,olWindowBTRect,this,IDC_BTCRC_CASH_ARR);
	m_CashArrBtn.SetFont(tmp);

	((CWnd*)GetDlgItem(IDC_NEWVIPD))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);

	olWindowBTRect.top 		= olWindowRect.top; 
	olWindowBTRect.bottom 	= olWindowRect.bottom;
	olWindowBTRect.left 	= olWindowRect.left - 50;
	olWindowBTRect.right 	= olWindowRect.left - 2;

	m_CashDepBtn.Create("Cash", buttonStyle ,olWindowBTRect,this,IDC_BTCRC_CASH_DEP);
	m_CashDepBtn.SetFont(tmp);

	m_CashArrBtn.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_Paid"));
	m_CashDepBtn.SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_Paid"));

	if(!bgShowCashButtonInBothFlight)
	{
		m_CashArrBtn.ShowWindow(SW_HIDE);
		m_CashDepBtn.ShowWindow(SW_HIDE);
	}
}

 void RotationDlg::OnCashArr()
 {
	if(bgShowCashButtonInBothFlight)
	{
		if ((MessageBox( GetString(IDS_STRING1595),GetString(ST_FRAGE),(MB_ICONQUESTION | MB_YESNO | MB_DEFBUTTON2))) == IDYES)
		{
			ogRotationDlgFlights.UpdateFlight(prmAFlight->Urno, "PAID", "P");
			strcpy(prmAFlight->Paid ,"P");
			if(bgCopyCashToOther)
			{
				ogRotationDlgFlights.UpdateFlight(prmDFlight->Urno, "PAID", "P");
				strcpy(prmDFlight->Paid ,"P");
			}
		}
		else
		{
			ogRotationDlgFlights.UpdateFlight(prmAFlight->Urno, "PAID", "B");
			strcpy(prmAFlight->Paid ,"B");
			if(bgCopyCashToOther)
			{
				ogRotationDlgFlights.UpdateFlight(prmDFlight->Urno, "PAID", "B");
				strcpy(prmDFlight->Paid ,"B");
			}
		}
	}
 }

 
 void RotationDlg::OnCashDep()
 {
	if(bgShowCashButtonInBothFlight)
	{
		if ((MessageBox( GetString(IDS_STRING1595),GetString(ST_FRAGE),(MB_ICONQUESTION | MB_YESNO | MB_DEFBUTTON2))) == IDYES)
		{
			ogRotationDlgFlights.UpdateFlight(prmDFlight->Urno, "PAID", "P");
			strcpy(prmDFlight->Paid ,"P");
			if(bgCopyCashToOther)
			{
				ogRotationDlgFlights.UpdateFlight(prmAFlight->Urno, "PAID", "P");
				strcpy(prmAFlight->Paid ,"P");
			}
		}
		else
		{
			ogRotationDlgFlights.UpdateFlight(prmDFlight->Urno, "PAID", "B");
			strcpy(prmDFlight->Paid ,"B");
			if(bgCopyCashToOther)
			{
				ogRotationDlgFlights.UpdateFlight(prmAFlight->Urno, "PAID", "B");
				strcpy(prmAFlight->Paid ,"B");

			}
		}
	}
 }

void RotationDlg::SetChocksOnOffControls()
{
	CRect olWindowRect;
	CRect olWindowBTRect;

	CFont* tmp = ((CWnd*) this -> GetDlgItem(IDC_NEWVIPA))->GetFont();
	DWORD staticStyle = WS_CHILD| WS_VISIBLE |  SS_CENTER   ;
	DWORD editStyle = WS_CHILD | WS_VISIBLE |WS_BORDER |WS_TABSTOP;
	

	((CWnd*)GetDlgItem(IDC_ALAND))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);

	olWindowBTRect.top 		= olWindowRect.top; 
	olWindowBTRect.bottom 	= olWindowRect.bottom;
	olWindowBTRect.left 	= olWindowRect.left - 90;
	olWindowBTRect.right 	= olWindowRect.left - 35;
	olWindowBTRect.bottom 	= olWindowRect.bottom;
	
	pomCEChocksOn->CreateEx(WS_EX_CLIENTEDGE , _T("EDIT"), "", editStyle, olWindowBTRect, this, IDC_EDIT_CHOCKSON);
	pomCEChocksOn->SetFont(tmp);

	((CWnd*)GetDlgItem(IDC_EDIT_CHOCKSON))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);
	olWindowBTRect.top 		= olWindowRect.top ; 
	olWindowBTRect.bottom 	= olWindowRect.bottom;
	olWindowBTRect.left 	= olWindowRect.left - 30;
	olWindowBTRect.right 	= olWindowRect.left - 1;

	olWindowBTRect.bottom 	= olWindowRect.bottom;
	pomLBLCEChocksOn->Create("OCK:", staticStyle,olWindowBTRect,this,IDC_LBL_CHOCKSON);
	pomLBLCEChocksOn->SetFont(tmp);


	((CWnd*)GetDlgItem(IDC_DAIRB))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);

	olWindowBTRect.top 		= olWindowRect.top; 
	olWindowBTRect.bottom 	= olWindowRect.bottom;
	olWindowBTRect.left 	= olWindowRect.left + 55;
	olWindowBTRect.right 	= olWindowRect.left + 90;

	olWindowBTRect.bottom 	= olWindowRect.bottom;
	pomLBLCEChocksOf->Create("OFC:", staticStyle,olWindowBTRect,this,IDC_LBL_CHOCKSOF);
	pomLBLCEChocksOf->SetFont(tmp);

	((CWnd*)GetDlgItem(IDC_LBL_CHOCKSOF))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);

	olWindowBTRect.top 		= olWindowRect.top; 
	olWindowBTRect.bottom 	= olWindowRect.bottom;
	olWindowBTRect.left 	= olWindowRect.left + 35;
	olWindowBTRect.right 	= olWindowRect.left + 85;

	olWindowBTRect.bottom 	= olWindowRect.bottom;
	pomCEChocksOf->CreateEx(WS_EX_CLIENTEDGE , _T("EDIT"), "", editStyle, olWindowBTRect, this, IDC_EDIT_CHOCKSOF);
	pomCEChocksOf->SetFont(tmp);

	if(!bgShowChock)
	{
		pomCEChocksOn->ShowWindow(SW_HIDE);
		pomLBLCEChocksOn->ShowWindow(SW_HIDE);
		pomLBLCEChocksOf->ShowWindow(SW_HIDE);
		pomCEChocksOf->ShowWindow(SW_HIDE);
	}
	else
	{
		pomCEChocksOn->SetName("AFTACHON");
		//pomCEChocksOf->SetName("AFTACHOF");
		pomCEChocksOf->SetName("AFTDCHOF");		// TLE: UFIS 3783

	}

	CWnd *polWnd;

	polWnd = (CWnd*) GetDlgItem(IDC_EDIT_CHOCKSON);	// TLE: UFIS 3768 
	if (polWnd)
		pomCEChocksOn->SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_ChockOn"));

	polWnd = (CWnd*) GetDlgItem(IDC_EDIT_CHOCKSOF);	// TLE: UFIS 3768 
	if (polWnd)
		pomCEChocksOf->SetSecState(ogPrivList.GetStat("ROTATIONDLG_CE_ChockOf"));

}

void RotationDlg::SetFixedControls()
{
	CRect olWindowRect;
	CRect olWindowCBRect;


	if(bgReasonFlag)
	{
		((CWnd*)GetDlgItem(IDC_BTCRC_PSTA))->GetWindowRect(olWindowRect);
	}
	else
	{
		((CWnd*)GetDlgItem(IDC_APSTALIST))->GetWindowRect(olWindowRect);
	}
	ScreenToClient(olWindowRect);

	
	olWindowCBRect.left 	= olWindowRect.left		-13;
	olWindowCBRect.top 		= olWindowRect.top		+ 3; 
	olWindowCBRect.right 	= olWindowCBRect.left	+10;
	olWindowCBRect.bottom 	= olWindowCBRect.top	+12;
	pomCBFPSA->Create(NULL, WS_VISIBLE | BS_AUTOCHECKBOX | WS_TABSTOP,olWindowCBRect,this,IDC_CBFPSA);
	
	if(bgReasonFlag)
	{
		((CWnd*)GetDlgItem(IDC_BTCRC_AGTA1))->GetWindowRect(olWindowRect);
	}
	else
	{
		((CWnd*)GetDlgItem(IDC_AGTA1LIST))->GetWindowRect(olWindowRect);
	}
	
	ScreenToClient(olWindowRect);
	olWindowCBRect.left 	= olWindowRect.left		-13;
	olWindowCBRect.top 		= olWindowRect.top		+ 3; 
	olWindowCBRect.right 	= olWindowCBRect.left	+10;
	olWindowCBRect.bottom 	= olWindowCBRect.top	+12;
	pomCBFGA1->Create(NULL, WS_VISIBLE | BS_AUTOCHECKBOX | WS_TABSTOP,olWindowCBRect,this,IDC_CBFGA1);

	if(bgReasonFlag)
	{
		((CWnd*)GetDlgItem(IDC_BTCRC_AGTA2))->GetWindowRect(olWindowRect);
	}
	else
	{
		((CWnd*)GetDlgItem(IDC_AGTA2LIST))->GetWindowRect(olWindowRect);
	}
	
	
	ScreenToClient(olWindowRect);
	olWindowCBRect.left 	= olWindowRect.left		-13;
	olWindowCBRect.top 		= olWindowRect.top		+ 3; 
	olWindowCBRect.right 	= olWindowCBRect.left	+10;
	olWindowCBRect.bottom 	= olWindowCBRect.top	+12;
	pomCBFGA2->Create(NULL, WS_VISIBLE | BS_AUTOCHECKBOX | WS_TABSTOP,olWindowCBRect,this,IDC_CBFGA2);

	if(bgReasonFlag)
	{
		((CWnd*)GetDlgItem(IDC_BTCRC_ABLT1))->GetWindowRect(olWindowRect);
	}
	else
	{
		((CWnd*)GetDlgItem(IDC_ABLT1LIST))->GetWindowRect(olWindowRect);
	}
	ScreenToClient(olWindowRect);
	olWindowCBRect.left 	= olWindowRect.left		-13;
	olWindowCBRect.top 		= olWindowRect.top		+ 3; 
	olWindowCBRect.right 	= olWindowCBRect.left	+10;
	olWindowCBRect.bottom 	= olWindowCBRect.top	+12;
	pomCBFBL1->Create(NULL, WS_VISIBLE | BS_AUTOCHECKBOX | WS_TABSTOP,olWindowCBRect,this,IDC_CBFBL1);

	if(bgReasonFlag)
	{
		((CWnd*)GetDlgItem(IDC_BTCRC_ABLT2))->GetWindowRect(olWindowRect);
	}
	else
	{
		((CWnd*)GetDlgItem(IDC_ABLT2LIST))->GetWindowRect(olWindowRect);
	}
	ScreenToClient(olWindowRect);
	olWindowCBRect.left 	= olWindowRect.left		-13;
	olWindowCBRect.top 		= olWindowRect.top		+ 3; 
	olWindowCBRect.right 	= olWindowCBRect.left	+10;
	olWindowCBRect.bottom 	= olWindowCBRect.top	+12;
	pomCBFBL2->Create(NULL, WS_VISIBLE | BS_AUTOCHECKBOX | WS_TABSTOP,olWindowCBRect,this,IDC_CBFBL2);

	if(bgReasonFlag)
	{
		((CWnd*)GetDlgItem(IDC_BTCRC_PSTD))->GetWindowRect(olWindowRect);
	}
	else
	{
		((CWnd*)GetDlgItem(IDC_DPSTDLIST))->GetWindowRect(olWindowRect);
	}
	
	
	ScreenToClient(olWindowRect);
	olWindowCBRect.left 	= olWindowRect.left		-13;
	olWindowCBRect.top 		= olWindowRect.top		+ 3; 
	olWindowCBRect.right 	= olWindowCBRect.left	+10;
	olWindowCBRect.bottom 	= olWindowCBRect.top	+12;
	pomCBFPSD->Create(NULL, WS_VISIBLE | BS_AUTOCHECKBOX | WS_TABSTOP,olWindowCBRect,this,IDC_CBFPSD);


	if(bgReasonFlag)
	{
		((CWnd*)GetDlgItem(IDC_BTCRC_DGTA1))->GetWindowRect(olWindowRect);
	}
	else
	{
		((CWnd*)GetDlgItem(IDC_DGTA1LIST))->GetWindowRect(olWindowRect);
	}
	
	
	ScreenToClient(olWindowRect);
	olWindowCBRect.left 	= olWindowRect.left		-13;
	olWindowCBRect.top 		= olWindowRect.top		+ 3; 
	olWindowCBRect.right 	= olWindowCBRect.left	+10;
	olWindowCBRect.bottom 	= olWindowCBRect.top	+12;
	pomCBFGD1->Create(NULL, WS_VISIBLE | BS_AUTOCHECKBOX | WS_TABSTOP,olWindowCBRect,this,IDC_CBFGD1);

	if(bgReasonFlag)
	{
		((CWnd*)GetDlgItem(IDC_BTCRC_DGTA2))->GetWindowRect(olWindowRect);
	}
	else
	{
		((CWnd*)GetDlgItem(IDC_DGTA2LIST))->GetWindowRect(olWindowRect);
	}
	
	ScreenToClient(olWindowRect);
	olWindowCBRect.left 	= olWindowRect.left		-13;
	olWindowCBRect.top 		= olWindowRect.top		+ 3; 
	olWindowCBRect.right 	= olWindowCBRect.left	+10;
	olWindowCBRect.bottom 	= olWindowCBRect.top	+12;
	pomCBFGD2->Create(NULL, WS_VISIBLE | BS_AUTOCHECKBOX | WS_TABSTOP,olWindowCBRect,this,IDC_CBFGD2);

	pomCBFPSA->EnableToolTips(TRUE);
	pomCBFGA1->EnableToolTips(TRUE);
	pomCBFGA2->EnableToolTips(TRUE);
	pomCBFBL1->EnableToolTips(TRUE);
	pomCBFBL2->EnableToolTips(TRUE);
	pomCBFPSD->EnableToolTips(TRUE);
	pomCBFGD1->EnableToolTips(TRUE);
	pomCBFGD2->EnableToolTips(TRUE);

	m_pToolTip = new CToolTipCtrl;
   if(!m_pToolTip->Create(this))
   {
	   TRACE("Unable To create ToolTip\n");
	   
   }


   //if (m_pToolTip->AddTool(pomCBFPSA,"Fixed"))
   //{
	//   TRACE("Unable to add OK button to the tooltip\n");
   //}

   m_pToolTip->AddTool(pomCBFPSA,"Fixed");
   m_pToolTip->AddTool(pomCBFGA1,"Fixed");
   m_pToolTip->AddTool(pomCBFGA2,"Fixed");
   m_pToolTip->AddTool(pomCBFBL1,"Fixed");
   m_pToolTip->AddTool(pomCBFBL2,"Fixed");
   m_pToolTip->AddTool(pomCBFPSD,"Fixed");
   m_pToolTip->AddTool(pomCBFGD1,"Fixed");
   m_pToolTip->AddTool(pomCBFGD2,"Fixed");

   m_pToolTip->Activate(TRUE);

	

	if(bgDailyFixedRes) 
	{
		m_CB_AFpsa.ShowWindow(SW_HIDE);
		m_CB_DFpsd.ShowWindow(SW_HIDE);

		
	}
	else
	{
		
		pomCBFPSA->ShowWindow(SW_HIDE);
		pomCBFGA1->ShowWindow(SW_HIDE);
		pomCBFGA2->ShowWindow(SW_HIDE);
		pomCBFBL1->ShowWindow(SW_HIDE);
		pomCBFBL2->ShowWindow(SW_HIDE);
		pomCBFPSD->ShowWindow(SW_HIDE);
		pomCBFGD1->ShowWindow(SW_HIDE);
		pomCBFGD2->ShowWindow(SW_HIDE);
	}


}

void RotationDlg::SetModeOfPayControls()
{
	CRect olWindowRect;
	CRect olWindowBTRect;

	CRect olWindowTmpRect;
	
	CFont* tmp = ((CWnd*) this -> GetDlgItem(IDC_NEWVIPA))->GetFont();
	DWORD buttonStyle = WS_VISIBLE | BS_PUSHBUTTON | BS_OWNERDRAW | WS_TABSTOP;

	((CWnd*)GetDlgItem(IDC_AVIA))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);
	
 
	olWindowBTRect.left 	= olWindowRect.left;
	olWindowBTRect.right 	= olWindowRect.right;
	olWindowBTRect.top 		= olWindowRect.top + 30; 
	olWindowBTRect.bottom 	=  olWindowRect.bottom +30;
	

	
	pomAModeOfPay->Create("Pay Details", buttonStyle ,olWindowBTRect,this,IDC_BTCRC_PAY_ARR);
	pomAModeOfPay->SetFont(tmp);

	((CWnd*)GetDlgItem(IDC_DVIA))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);

	olWindowBTRect.left 	= olWindowRect.left;
	olWindowBTRect.right 	= olWindowRect.right;
	olWindowBTRect.top 		= olWindowRect.top + 30; 
	olWindowBTRect.bottom 	=  olWindowRect.bottom +30;

	pomDModeOfPay->Create("Pay Details", buttonStyle ,olWindowBTRect,this,IDC_BTCRC_PAY_DEP);
	pomDModeOfPay->SetFont(tmp);

	

	if(!bgShowPayDetail)
	{
		pomAModeOfPay->ShowWindow(SW_HIDE);
		pomDModeOfPay->ShowWindow(SW_HIDE);
	}
}

//For Fixed Allocation - PRF 8405
void RotationDlg::OnFixedAllocaton()
{
	CCS_TRY

	OnEditChanged(0,0);

	CCS_CATCH_ALL
}


//UFIS-1120
void RotationDlg::OnClickActivities() 
{
	CString olPermits = "1";
	char alAUrno[16] = "";		
	ltoa(prmAFlight->Urno, alAUrno,10);
	char alDUrno[16] = "";		
	ltoa(prmDFlight->Urno, alDUrno,10);
	LaunchTool( "ACTIVITIES", alAUrno, alDUrno, olPermits, "ACTIVITIES" );
}

void RotationDlg::OnAPay() 
{

	CModeOfPay dlg  = new CModeOfPay(this,prmAFlight->Urno, "A");
	polAModeOfPay= &dlg;
	
	polAModeOfPay->SetFlightNo(prmAFlight->Urno);
	polAModeOfPay->SetFlightType("A");
	polAModeOfPay->SetMode("Update");
	polAModeOfPay->SetRotaionFlight(prmAFlight);

	int ret = polAModeOfPay->DoModal();

	OnEditChanged(0,0);
	
}

void RotationDlg::OnDPay() 
{
	
	CModeOfPay dlg  = new CModeOfPay(this,prmDFlight->Urno, "D");
	polDModeOfPay= &dlg;
	
	polDModeOfPay->SetFlightNo(prmDFlight->Urno);
	polDModeOfPay->SetFlightType("D");
	polDModeOfPay->SetMode("Update");
	polDModeOfPay->SetRotaionFlight(prmDFlight);
	int ret = polDModeOfPay->DoModal();

	OnEditChanged(0,0);
	
}


void RotationDlg::UpdateCashButton2(const CString &ropAlc3, char cpPaid) 
{
	CString olCash(""); 

	SetCashCommentDisplay();

	if (bmRotationReadOnly)
	{
		HandlePostFlight();
		return;
	}
		

	ogBCD.GetField("ALT", "ALC3", ropAlc3, "CASH", olCash);

	//need to update with the cash from AFTTAB

	m_CB_Paid.ShowWindow(SW_SHOW);

	if(bgShowCashComment)
	{
		 pomLBLCCom->ShowWindow(SW_SHOW);
		 pomBTCRCom->ShowWindow(SW_SHOW); 
	}

	if (olCash == "K")
	{
		m_CB_Paid.ShowWindow(SW_HIDE);

		
		if(bgShowCashComment)
		{
		 pomLBLCCom->ShowWindow(SW_HIDE);
		 pomBTCRCom->ShowWindow(SW_HIDE); 
		}
	}
	else
	{
		if(cpPaid == 'B')
		{
			m_CB_Paid.SetWindowText(GetString(IDS_STRING1249));
			m_CB_Paid.SetColors(RGB(255, 0, 0), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));

		}
		else
		{
			m_CB_Paid.SetWindowText(GetString(IDS_STRING1248));
			m_CB_Paid.SetColors(::GetSysColor(COLOR_BTNFACE), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));

			
		}
	
			SetWndStatAll(ogPrivList.GetStat("ROTATIONDLG_CB_Paid"),m_CB_Paid);	
		
		//is it a postflight?
		HandlePostFlight();
	}
} 




CString RotationDlg::GetMopa(long flighturno)
{
//	char clPaid;	
	CString olSelection;
	olSelection.Format("WHERE URNO='%d'",flighturno);

	CString olDummyTableName="AFT";
	ogBCD.SetObject(olDummyTableName, "URNO,MOPA");
	ogBCD.SetObjectDesc(olDummyTableName, "Configuration");
	ogBCD.Read(olDummyTableName,olSelection);

	int ndxPaid = ogBCD.GetFieldIndex( olDummyTableName, "MOPA");	
	int nItems = ogBCD.GetDataCount( olDummyTableName );

	CString olPaid;
	for( int ndx = 0 ; ndx < nItems ; ndx++ )		// for each entry
	{
		RecordSet record ;
		if( ogBCD.GetRecord( olDummyTableName, ndx, record ) )
		{
			olPaid = record[ ndxPaid ];
			
		}
	}


	
	return olPaid;
}


void RotationDlg::UpdateCashButton(const CString &ropAlc3, char cpPaid) 
{
	
	if (bmRotationReadOnly)
	{
		HandlePostFlight();
		return;
	}		

	
	//need to update with the cash from AFTTAB
	CString olCash(""); 
	m_CB_Paid.ShowWindow(SW_SHOW);
	
	SetCashCommentDisplay();

	if (strcmp(pcgHome, "BKK") == 0) // for bankok only
	{
		cpPaid = ' '; 
		if(bgShowCashButtonInBothFlight)
		{
			m_CB_Paid.ShowWindow(SW_HIDE);

			cpPaid = prmAFlight->Paid[0];

			if(cpPaid == 'B')
			{
				m_CashArrBtn.SetWindowText(GetString(IDS_STRING2939));
				m_CashArrBtn.SetColors(RGB(255, 0, 0), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
			}
			else if(cpPaid == 'P')
			{
				m_CashArrBtn.SetWindowText(GetString(IDS_STRING1248));
				m_CashArrBtn.SetColors(::GetSysColor(COLOR_BTNFACE), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
				
			}
			else
			{
				m_CashArrBtn.ShowWindow(SW_HIDE);
			}

			cpPaid = prmDFlight->Paid[0];

			if(cpPaid == 'B')
			{
				m_CashDepBtn.SetWindowText(GetString(IDS_STRING2939));
				m_CashDepBtn.SetColors(RGB(255, 0, 0), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
			}
			else if(cpPaid == 'P')
			{
				m_CashDepBtn.SetWindowText(GetString(IDS_STRING1248));
				m_CashDepBtn.SetColors(::GetSysColor(COLOR_BTNFACE), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
				
			}
			else
			{
				m_CashDepBtn.ShowWindow(SW_HIDE);
			}

		
			HandlePostFlight();
			

		}
		else
		{
			m_CB_Paid.ShowWindow(SW_SHOW);
			if(bgShowCashComment)
			{
				pomLBLCCom->ShowWindow(SW_SHOW);
				pomBTCRCom->ShowWindow(SW_SHOW); 
			}
			
			if(prmDFlight->Urno != 0)
			{
				
				if(strlen(prmDFlight->Paid) == 0)
					cpPaid = ' ';
				else
					cpPaid = prmDFlight->Paid[0];
			}
			else
			{
				if(strlen(prmAFlight->Paid) == 0)
					cpPaid = ' ';
				else
					cpPaid = prmAFlight->Paid[0];
			}
		

			if(cpPaid == 'B')
			{
				m_CB_Paid.SetWindowText(GetString(IDS_STRING1249));
				m_CB_Paid.SetColors(RGB(255, 0, 0), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
			}
			else if(cpPaid == 'P')
			{
				m_CB_Paid.SetWindowText(GetString(IDS_STRING1248));
				m_CB_Paid.SetColors(::GetSysColor(COLOR_BTNFACE), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
			}
			else
			{
				m_CB_Paid.ShowWindow(SW_HIDE);
				if(bgShowCashComment)
				{
					pomLBLCCom->ShowWindow(SW_HIDE);
					pomBTCRCom->ShowWindow(SW_HIDE); 
				}
			}

			HandlePostFlight();
			
		}
	}
	else
	{
		if(bgShowCashButtonInBothFlight)
		{
			m_CB_Paid.ShowWindow(SW_HIDE);

			CString olAMopa("");
			olAMopa= GetMopa(prmAFlight->Urno);
			olAMopa.TrimRight();
			CString olDMopa("");
			olDMopa= GetMopa(prmDFlight->Urno);
			olDMopa.TrimRight();

			if(olAMopa.GetLength()>0)
			{
				olCash = olAMopa;
			}

			if (olCash == "K")
				m_CashArrBtn.ShowWindow(SW_HIDE);
			else
			{
				cpPaid = prmAFlight->Paid[0];
				if(cpPaid == 'B')
				{
					m_CashArrBtn.SetWindowText(GetString(IDS_STRING2939));
					m_CashArrBtn.SetColors(RGB(255, 0, 0), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
				}
				else if(cpPaid == 'K')
				{
					if(olAMopa.GetLength()>0)
					{
						m_CashArrBtn.ShowWindow(SW_SHOW);
						m_CashArrBtn.SetWindowText(GetString(IDS_STRING2939));
						m_CashArrBtn.SetColors(RGB(255, 0, 0), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
					}
					else
					{	
						m_CashArrBtn.ShowWindow(SW_SHOW);
						m_CashArrBtn.SetWindowText(GetString(IDS_STRING1248));
						m_CashArrBtn.SetColors(::GetSysColor(COLOR_BTNFACE), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
					}
				}
				else
				{
					m_CashArrBtn.SetWindowText(GetString(IDS_STRING1248));
					m_CashArrBtn.SetColors(::GetSysColor(COLOR_BTNFACE), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
				}

				HandlePostFlight();
			}

			if(olDMopa.GetLength()>0)
			{
				olCash = olDMopa;
			}

			if (olCash == "K")
				m_CashDepBtn.ShowWindow(SW_HIDE);
			else
			{
				cpPaid = prmDFlight->Paid[0];
				if(cpPaid == 'B')
				{
					m_CashDepBtn.SetWindowText(GetString(IDS_STRING2939));
					m_CashDepBtn.SetColors(RGB(255, 0, 0), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
				}
				else if(cpPaid == 'K')
				{
					if(olDMopa.GetLength()>0)
					{
						m_CashDepBtn.ShowWindow(SW_SHOW);
						m_CashDepBtn.SetWindowText(GetString(IDS_STRING2939));
						m_CashDepBtn.SetColors(RGB(255, 0, 0), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
					}
					else
					{
						m_CashDepBtn.ShowWindow(SW_SHOW);
						m_CashDepBtn.SetWindowText(GetString(IDS_STRING1248));
						m_CashDepBtn.SetColors(::GetSysColor(COLOR_BTNFACE), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
					}
				}
				else
				{
					m_CashDepBtn.SetWindowText(GetString(IDS_STRING1248));
					m_CashDepBtn.SetColors(::GetSysColor(COLOR_BTNFACE), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
				}

				HandlePostFlight();
			}

		}
		else
		{
			if(bgShowCashComment)
			{
				pomLBLCCom->ShowWindow(SW_SHOW);
				pomBTCRCom->ShowWindow(SW_SHOW); 
			}

			if (olCash == "K")
			{
				m_CB_Paid.ShowWindow(SW_HIDE);
				if(bgShowCashComment)
				{
					pomLBLCCom->ShowWindow(SW_HIDE);
					pomBTCRCom->ShowWindow(SW_HIDE); 
				}
			}
			else
			{
			
				if(prmDFlight->Urno != 0)
				{
					
					if(strlen(prmDFlight->Paid) == 0)
						cpPaid = ' ';
					else
						cpPaid = prmDFlight->Paid[0];
				}
				else
				{
					if(strlen(prmAFlight->Paid) == 0)
						cpPaid = ' ';
					else
						cpPaid = prmAFlight->Paid[0];
				}

				if(cpPaid == 'B')
				{
					m_CB_Paid.SetWindowText(GetString(IDS_STRING1249));
					m_CB_Paid.SetColors(RGB(255, 0, 0), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
				}
				else
				{
					m_CB_Paid.SetWindowText(GetString(IDS_STRING1248));
					m_CB_Paid.SetColors(::GetSysColor(COLOR_BTNFACE), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
				}

				HandlePostFlight();
			}
		}
	}

	//if(strcmp(prmAFlight->Mopa, "K") == 0 && strcmp(prmDFlight->Mopa, "K") == 0)//lgpbank
	if((strcmp(prmAFlight->Mopa, "K") == 0 && strcmp(prmDFlight->Mopa, "K") == 0) 
		|| (strcmp(prmAFlight->Mopa, "K") == 0 && prmDFlight->Urno == 0)
		|| (strcmp(prmDFlight->Mopa, "K") == 0 && prmAFlight->Urno == 0)
		|| (prmDFlight->Urno == 0 && prmAFlight->Urno == 0)
		|| (strcmp(prmAFlight->Mopa, "") == 0 && strcmp(prmDFlight->Mopa, "") == 0))
	{
		m_CB_Paid.ShowWindow(SW_HIDE);
	}
} 



void RotationDlg::ArrangeTabsOrder()
{
	
	int arrControlsID[500];
	
	int i =0;


	arrControlsID[i]=IDCANCEL;i++;
	arrControlsID[i]=IDC_SPLIT;i++;
	arrControlsID[i]=IDC_JOIN;i++;
	arrControlsID[i]=IDC_AGENTS;i++;
	arrControlsID[i]=IDC_OBJECT;i++;
	arrControlsID[i]=IDC_TOWING; i++;
	arrControlsID[i]=IDC_REQ; i++;
	arrControlsID[i]=IDC_LOCK; i++;
	arrControlsID[i]=IDC_BTCRC_ACTIVITY;i++;
	arrControlsID[i]=IDC_SEASON_MASK; i++;
	arrControlsID[i]=IDC_REGN; i++;
	arrControlsID[i]=IDC_ACT3; i++;
	arrControlsID[i]=IDC_ACT5; i++;
	arrControlsID[i]=IDC_ACT35LIST; i++;
	arrControlsID[i]=IDC_MING; i++;
	arrControlsID[i]=IDC_ACWS; i++;
	arrControlsID[i]=IDC_ACLE; i++;
	arrControlsID[i]=IDC_PAID; i++;
	arrControlsID[i]=IDC_AAlc3LIST; i++;
	arrControlsID[i]=IDC_AALC3; i++;
	arrControlsID[i]=IDC_AFLTN; i++;
	arrControlsID[i]=IDC_AFLNS; i++;
	arrControlsID[i]=IDC_AFLTI; i++;
	arrControlsID[i]=IDC_AVIP; i++;

	arrControlsID[i]=IDC_ACSGN; i++;
	if(bgShowCashButtonInBothFlight)
	{
		arrControlsID[i]=IDC_BTCRC_CASH_ARR; i++;
	}
	arrControlsID[i]=IDC_NEWVIPA; i++;
	arrControlsID[i]=IDC_ATelex; i++;
	arrControlsID[i]=IDC_DALC3LIST; i++;
	arrControlsID[i]=IDC_DALC3; i++;
	arrControlsID[i]=IDC_DFLTN; i++;
	arrControlsID[i]=IDC_DFLNS; i++;
	arrControlsID[i]=IDC_DFLTI; i++;

	arrControlsID[i]=IDC_DVIP; i++;

	arrControlsID[i]=IDC_DCSGN; i++;
	if(bgShowCashButtonInBothFlight)
	{
		arrControlsID[i]=IDC_BTCRC_CASH_DEP; i++;
	}

	arrControlsID[i]=IDC_NEWVIPD; i++;
	arrControlsID[i]=IDC_DTelex; i++;
	arrControlsID[i]=IDC_ASHOWJFNO; i++;

	arrControlsID[i]=IDC_ASHOWJFNO; i++;
	arrControlsID[i]=IDC_AJFNOBORDER; i++;
	arrControlsID[i]=IDC_AAlc3LIST2; i++;
	arrControlsID[i]=IDC_AAlc3LIST3; i++;
	arrControlsID[i]=IDC_ACXXREASON; i++;
	arrControlsID[i]=IDC_ACXX; i++;
	arrControlsID[i]=IDC_ARerouted; i++;
	arrControlsID[i]=IDC_ADiverted; i++;
	arrControlsID[i]=IDC_ARETFLIGHT; i++;
	arrControlsID[i]=IDC_ARETTAXI; i++;

	arrControlsID[i]=IDC_DSHOWJFNO; i++;
	arrControlsID[i]=IDC_DJFNOBORDER; i++;
	arrControlsID[i]=IDC_DALC3LIST2; i++;
	arrControlsID[i]=IDC_DALC3LIST3; i++;
	arrControlsID[i]=IDC_DCXXREASON; i++;
	arrControlsID[i]=IDC_DCXX; i++;
	arrControlsID[i]=IDC_DRerouted; i++;
	arrControlsID[i]=IDC_DDiverted; i++;
	arrControlsID[i]=IDC_DDIVR; i++;
	arrControlsID[i]=IDC_DRETFLIGHT; i++;
	arrControlsID[i]=IDC_DRETTAXI; i++;

	arrControlsID[i]=IDC_ATTYPLIST; i++;
	arrControlsID[i]=IDC_ATTYP; i++;
	arrControlsID[i]=IDC_ASTYPLIST; i++;
	arrControlsID[i]=IDC_ASTYP; i++;
	arrControlsID[i]=IDC_AHTYPLIST; i++;
	arrControlsID[i]=IDC_AHTYP; i++;

	arrControlsID[i]=IDC_ASTEV; i++;
	arrControlsID[i]=IDC_ASTE2; i++;
	arrControlsID[i]=IDC_ASTE3; i++;
	arrControlsID[i]=IDC_ASTE4; i++;


	arrControlsID[i]=IDC_DTTYPLIST; i++;
	arrControlsID[i]=IDC_DTTYP; i++;
	arrControlsID[i]=IDC_DSTYPLIST; i++;
	arrControlsID[i]=IDC_DSTYP; i++;
	arrControlsID[i]=IDC_DHTYPLIST; i++;
	arrControlsID[i]=IDC_DHTYP; i++;

	arrControlsID[i]=IDC_DSTEV; i++;
	arrControlsID[i]=IDC_DSTE2; i++;
	arrControlsID[i]=IDC_DSTE3; i++;
	arrControlsID[i]=IDC_DSTE4; i++;


	arrControlsID[i]=IDC_AORG3LIST; i++;
	arrControlsID[i]=IDC_AORG3L; i++;
	arrControlsID[i]=IDC_AORG3; i++;

	if(pomAVia)
	{
		arrControlsID[i]=IDC_AVIA; i++;
	}

	arrControlsID[i]=IDC_GOAROUND; i++;
	arrControlsID[i]=IDC_ADES3; i++;


	arrControlsID[i]=IDC_DORG3; i++;

	if(pomDVia)
	{
		arrControlsID[i]=IDC_DVIA; i++;
	}

	arrControlsID[i]=IDC_DCBNFES; i++;
	arrControlsID[i]=IDC_DNFES; i++;
	arrControlsID[i]=IDC_DDES3LIST; i++;
	arrControlsID[i]=IDC_DDES3; i++;
	arrControlsID[i]=IDC_DDES4; i++;

	arrControlsID[i]=IDC_ASTOD; i++;
	arrControlsID[i]=IDC_ACBNFES; i++;
	arrControlsID[i]=IDC_ANFES; i++;
	arrControlsID[i]=IDC_ASTOA; i++;

	arrControlsID[i]=IDC_DSTOD; i++;
	arrControlsID[i]=IDC_DISKD; i++;
	arrControlsID[i]=IDC_DTSAT; i++;
	arrControlsID[i]=IDC_DSTOA; i++;

	arrControlsID[i]=IDC_AETDI; i++;
	arrControlsID[i]=IDC_AETOA; i++;
	arrControlsID[i]=IDC_AETAI; i++;
	arrControlsID[i]=IDC_DETDI; i++;
	arrControlsID[i]=IDC_DETOD; i++;

	arrControlsID[i]=IDC_AOFBL; i++;
	arrControlsID[i]=IDC_AAIRB; i++;

	if(bgShowChock)
	{
		arrControlsID[i]=IDC_EDIT_CHOCKSON; i++;
	}

	arrControlsID[i]=IDC_ALAND; i++;
	arrControlsID[i]=IDC_AONBL; i++;
	arrControlsID[i]=IDC_DOFBL; i++;

	arrControlsID[i]=IDC_DAIRB; i++;
	if(bgShowChock)
	{
		arrControlsID[i]=IDC_EDIT_CHOCKSOF; i++;
	}

	arrControlsID[i]=IDC_FL_REPORT_ARR; i++;
	arrControlsID[i]=IDC_AONBE; i++;
	arrControlsID[i]=IDC_ANXTI; i++;
	arrControlsID[i]=IDC_DETDC; i++;
	arrControlsID[i]=IDC_DNXTI; i++;

	arrControlsID[i]=IDC_DDELAY; i++;
	arrControlsID[i]=IDC_FL_REPORT_DEP; i++;

	arrControlsID[i]=IDC_ADCD1; i++;
	arrControlsID[i]=IDC_ADCD1LIST; i++;
	arrControlsID[i]=IDC_ADTD1; i++;
	arrControlsID[i]=IDC_ADCD2; i++;
	arrControlsID[i]=IDC_ADCD2LIST; i++;
	arrControlsID[i]=IDC_ADTD2; i++;

	if(bgMulitpleDelayCodes == true)
	{
		arrControlsID[i]=IDC_DELAYBTN_ARRIVAL; i++;
	}

	arrControlsID[i]=IDC_ATMOA; i++;
	arrControlsID[i]=IDC_ARWYA; i++;
	arrControlsID[i]=IDC_AIFRA; i++;

	arrControlsID[i]=IDC_DSLOT; i++;
	arrControlsID[i]=IDC_DRWYD; i++;
	arrControlsID[i]=IDC_DIFRD; i++;

	if(bgMulitpleDelayCodes == true)
	{
		arrControlsID[i]=IDC_DELAYBTN_DEPARTURE; i++;
	}

	arrControlsID[i]=IDC_DDCD1; i++;
	arrControlsID[i]=IDC_DDCD1LIST; i++;
	arrControlsID[i]=IDC_DDTD1; i++;
	arrControlsID[i]=IDC_DDCD2; i++;
	arrControlsID[i]=IDC_DDCD2LIST; i++;
	arrControlsID[i]=IDC_DDTD2; i++;

	arrControlsID[i]=IDC_CLB_ARR; i++;
	arrControlsID[i]=IDC_ALOAD; i++;
	arrControlsID[i]=IDC_LOAD_VIEW_ARR; i++;
	arrControlsID[i]=IDC_ALOG; i++;
	arrControlsID[i]=IDC_DLOAD; i++;
	arrControlsID[i]=IDC_LOAD_VIEW_DEP; i++;
	arrControlsID[i]=IDC_DLOG; i++;
	arrControlsID[i]=IDC_CLB_DEP; i++;
	arrControlsID[i]=IDC_DBLT1LIST; i++;
	arrControlsID[i]=IDC_DBLT1; i++;
	arrControlsID[i]=IDC_DTMB1; i++;
	arrControlsID[i]=IDC_DB1BA; i++;
	arrControlsID[i]=IDC_DB1EA; i++;

	if(bgDailyFixedRes)
	{
		arrControlsID[i]=IDC_CBFPSA; i++;
	}

	if(bgReasonFlag)
	{
		//arrControlsID[i]=IDC_BTCRC_PSTA; i++;
	}

	arrControlsID[i]=IDC_APSTALIST; i++;
	arrControlsID[i]=IDC_APSTA; i++;
	arrControlsID[i]=IDC_ABBAA; i++;

	if(bgDailyFixedRes)
	{
		arrControlsID[i]=IDC_CBFGA1; i++;
	}

	arrControlsID[i]=IDC_AGTA1LIST; i++;
	arrControlsID[i]=IDC_AGTA1; i++;
	arrControlsID[i]=IDC_ATGA1; i++;
	arrControlsID[i]=IDC_AGA1X; i++;
	arrControlsID[i]=IDC_AGA1Y; i++;

	
	if(bgDailyFixedRes)
	{
		arrControlsID[i]=IDC_CBFPSD; i++;
	}

	arrControlsID[i]=IDC_DPSTDLIST; i++;
	arrControlsID[i]=IDC_DPSTD; i++;
	arrControlsID[i]=IDC_DBBFA; i++;
	arrControlsID[i]=IDC_DGATLOGO; i++;


	if(bgDailyFixedRes)
	{
		arrControlsID[i]=IDC_CBFGD1; i++;
	}
	arrControlsID[i]=IDC_DGTA1LIST; i++;
	arrControlsID[i]=IDC_DGTD1; i++;
	arrControlsID[i]=IDC_DTGD1; i++;
	arrControlsID[i]=IDC_DGD1X ; i++;	
	arrControlsID[i]=IDC_DGD1Y; i++;

	if(bgReasonFlag)
	{
		arrControlsID[i]=IDC_BTCRC_RESOA; i++;
	}

	arrControlsID[i]=IDC_AGPU; i++;
	arrControlsID[i]=IDC_APLB; i++;
	arrControlsID[i]=IDC_APCA; i++;

		
	if(bgDailyFixedRes)
	{
		arrControlsID[i]=IDC_CBFGA2; i++;
	}

	arrControlsID[i]=IDC_AGTA2LIST; i++;
	arrControlsID[i]=IDC_AGTA2; i++;
	arrControlsID[i]=IDC_ATGA2; i++;
	arrControlsID[i]=IDC_AGA2X; i++;
	arrControlsID[i]=IDC_AGA2Y; i++;

	if(bgReasonFlag)
	{
		arrControlsID[i]=IDC_BTCRC_RESOD; i++;
	}



	arrControlsID[i]=IDC_DGPU; i++;
	arrControlsID[i]=IDC_DPLB; i++;
	arrControlsID[i]=IDC_DPCA; i++;
	arrControlsID[i]=IDC_DGATLOGO2; i++;


	if(bgDailyFixedRes)
	{
		arrControlsID[i]=IDC_CBFGD2; i++;
	}
	arrControlsID[i]=IDC_DGTA2LIST; i++;
	arrControlsID[i]=IDC_DGTD2; i++;
	arrControlsID[i]=IDC_DTGD2; i++;
	arrControlsID[i]=IDC_DGD2X; i++;
	arrControlsID[i]=IDC_DGD2Y; i++;

	if(bgDailyFixedRes)
	{
		arrControlsID[i]=IDC_CBFBL1; i++;
	}

	arrControlsID[i]=IDC_ABLT1LIST; i++;
	arrControlsID[i]=IDC_ABLT1; i++;
	arrControlsID[i]=IDC_ATMB1; i++;
	arrControlsID[i]=IDC_AB1BA; i++;
	arrControlsID[i]=IDC_AB1EA; i++;
	arrControlsID[i]=IDC_AEXT1LIST; i++;
	arrControlsID[i]=IDC_AEXT1; i++;
	arrControlsID[i]=IDC_ATET1; i++;
	//arrControlsID[i]=IDC_DCINSMAL4; i++;
	arrControlsID[i]=IDC_DWRO1LIST; i++;
	arrControlsID[i]=IDC_DWRO1; i++;
	arrControlsID[i]=IDC_DTWR1; i++;
	arrControlsID[i]=IDC_DW1BA; i++;
	arrControlsID[i]=IDC_DW1EA; i++;


	if(bgDailyFixedRes)
	{
		arrControlsID[i]=IDC_CBFBL2; i++;
	}
	arrControlsID[i]=IDC_ABLT2LIST; i++;
	arrControlsID[i]=IDC_ABLT2; i++;
	arrControlsID[i]=IDC_ATMB2; i++;
	arrControlsID[i]=IDC_AB2BA; i++;
	arrControlsID[i]=IDC_AB2EA; i++;
	arrControlsID[i]=IDC_AEXT2LIST; i++;
	arrControlsID[i]=IDC_AEXT2; i++;
	arrControlsID[i]=IDC_ATET2; i++;



	arrControlsID[i]=IDC_DCICLOGO1; i++;
	arrControlsID[i]=IDC_DCINSLIST1; i++;
	arrControlsID[i]=IDC_DCICREM1; i++;
	arrControlsID[i]=IDC_DCICCS1; i++;
	arrControlsID[i]=IDC_DCINSBORDER; i++;
	arrControlsID[i]=IDC_DCINSBORDEREXT; i++;
	arrControlsID[i]=IDC_DWRO3LIST; i++;
	arrControlsID[i]=IDC_DWRO3; i++;
	arrControlsID[i]=IDC_DTWR3; i++;
	arrControlsID[i]=IDC_DW3BA; i++;
	arrControlsID[i]=IDC_DW3EA; i++;

	arrControlsID[i]=IDC_ABAS1; i++;
	arrControlsID[i]=IDC_ABAE1; i++;
	arrControlsID[i]=IDC_DCICLOGO2; i++;
	arrControlsID[i]=IDC_DCINSLIST2; i++;
	arrControlsID[i]=IDC_DCICREM2; i++;
	arrControlsID[i]=IDC_DCICCS2; i++;
	arrControlsID[i]=IDC_DBAZ1; i++;
	arrControlsID[i]=IDC_DBAO1; i++;
	arrControlsID[i]=IDC_DBAC1; i++;
	arrControlsID[i]=IDC_ABAS2; i++;
	arrControlsID[i]=IDC_ABAE2; i++;
	arrControlsID[i]=IDC_DCICLOGO3; i++;
	arrControlsID[i]=IDC_DCINSLIST3; i++;
	arrControlsID[i]=IDC_DCICREM3; i++;
	arrControlsID[i]=IDC_DCICCS3; i++;
	arrControlsID[i]=IDC_DBAZ4; i++;
	arrControlsID[i]=IDC_DBAO4; i++;
	arrControlsID[i]=IDC_DBAC4; i++;
	arrControlsID[i]=IDC_DCICLOGO4; i++;
	arrControlsID[i]=IDC_DCINSLIST4; i++;
	arrControlsID[i]=IDC_DCICREM4; i++;
	arrControlsID[i]=IDC_DCICCS4; i++;

	arrControlsID[i]=IDC_COMBO_AHISTORY; i++;
	arrControlsID[i]=IDC_COMBO_DHISTORY; i++;

	arrControlsID[i]=IDC_AREM1; i++;
	arrControlsID[i]=IDC_DREM1; i++;
	arrControlsID[i]=IDC_AREMP; i++;
	arrControlsID[i]=IDC_DREMP; i++;


	CWnd* pWndPrev = ((CWnd*)this->GetDlgItem(IDOK));
	CWnd* pWnd = NULL;

	for (int j=0;j<i ;j++)
	{
		pWnd = ((CWnd*)this->GetDlgItem(arrControlsID[j]));
		if(pWnd->IsWindowEnabled())
		{
			pWnd->SetWindowPos(pWndPrev,0,0,0,0,SWP_NOSIZE | SWP_NOMOVE );
			pWndPrev = pWnd;
		}
	}

}

/*
void RotationDlg::ArrangeTabsOrderTables()
{
	/*
	if(ipLineno <= omLines.GetSize()-1)
	 {
		 if(ipColumnno <= omLines[ipLineno].Columns.GetSize())
		 {
			omLines[ipLineno].Columns[ipColumnno].BkColor   = rlBkColor;
			omLines[ipLineno].Columns[ipColumnno].TextColor = rlTextColor;
			if (m_hWnd != NULL)     // list box is exist?
			{
				CRect rcItem;
				if (pomDCinsTable->pomListBox->GetItemRect(ipLineno, rcItem) == LB_ERR 
					// ||  pomListBox->SetCaretIndex(ipLineNo) == LB_ERR
					)
				  {
						//RC(false);
						return false;
				  }
				pomListBox->InvalidateRect(rcItem);
			}
		 }
	 }

	if(pomDCinsTable != NULL)
	{
		int ilLines = pomDCinsTable->GetLinesCount();
		for( i = 0; i < ilLines; i++)
		{

		}
	}
}
*/

BOOL RotationDlg::PreTranslateMessage(MSG* pMsg) 
{
	// TODO: Add your specialized code here and/or call the base class
#if 0
	if(pMsg->message == WM_PAINT)//*lgp
	{
		//MessageBox("get focus");
		if(!omArrFlightPermitInfo.IsEmpty() && strcmp(omArrFlightPermitInfo, "Permit Reqd") !=0)
		{
			
			CString olWhere = ""; //lgp
			CString olUrno = "";
			CString olPern = ""; 
			CString olStoa = CTimeToDBString(ConvertFlightPermitTime(prmAFlight->Stoa), prmAFlight->Stoa);
			/*if(bmLocalTime) 
			{
			ConvertStructLocalTOUtc(prmAFlight, NULL);
			}
			CString olStoa = CTimeToDBString(prmAFlight->Stoa, prmAFlight->Stoa);*/
			

			/*FPEDATA *prlFpe = NULL;
			if(bgShowADHOC || bgSeasonShowADHOC)
			{
				prlFpe = ogFpeData.GetBestPermitForFlight(ConvertFlightPermitTime(prmDFlight->Stod), false, prmDFlight->Regn, prmDFlight->Alc3, prmDFlight->Fltn, prmDFlight->Flns,prmDFlight->Csgn, &omDepFlightPermitInfo);
				//if(omDepFlightPermitInfo != "No Flight Permit")
				if(prlFpe != NULL)
				{
					olUrno.Format("%ld", prlFpe->Urno);//lgptest
					olPern = prlFpe->Pern;
				}
			}
			else
			{
				prlFpe = ogFpeData.GetBestPermitForFlight(ConvertFlightPermitTime(prmDFlight->Stod), false, prmDFlight->Regn, prmDFlight->Alc3, prmDFlight->Fltn, prmDFlight->Flns, &omDepFlightPermitInfo);
				//if(omDepFlightPermitInfo != "No Flight Permit")
				if(prlFpe != NULL)
				{
					olUrno.Format("%ld", prlFpe->Urno);//lgptest
					olPern = prlFpe->Pern;
				}
			}*/ 
			
			if(!olPern.IsEmpty() && strcmp(olPern, "Permit Reqd") !=0)
			{
				//			m_CE_APer.SetWindowText(olPern);
				if(ogFpeData.bmFlightPermitsEnabled)
				{
					omArrFlightPermitInfo = olPern;
				}
				pomArrPermitsButton->SetColors(LIME,GRAY,WHITE);//*lgpt
			}
		}
	}
#endif	
	
	if (NULL != m_pToolTip)
		m_pToolTip->RelayEvent(pMsg);
	return CDialog::PreTranslateMessage(pMsg);
}


// 050302 MVy: code taken identically from CSeasonDlg::OnDcinsmal4()
void RotationDlg::OnDcinsmal4() 
{
	CCS_TRY

	int i = pomDCinsTable->pomListBox->GetTopIndex();

	CString olCkic;
	CString olCkit;
	CString olCkbs;
	CString olCkes;
	CString olCatr;

	if (bgCnamAtr)
	{
		pomDCinsTable->GetTextFieldValue(i, 0, olCkic);
		pomDCinsTable->GetTextFieldValue(i, 1, olCatr);
		pomDCinsTable->GetTextFieldValue(i, 2, olCkit);
		pomDCinsTable->GetTextFieldValue(i, 3, olCkbs);
		pomDCinsTable->GetTextFieldValue(i, 4, olCkes);
	}
	else
	{
		pomDCinsTable->GetTextFieldValue(i, 0, olCkic);
		pomDCinsTable->GetTextFieldValue(i, 1, olCkit);
		pomDCinsTable->GetTextFieldValue(i, 2, olCkbs);
		pomDCinsTable->GetTextFieldValue(i, 3, olCkes);
	}


	bool blNumericSort = false;


	if(strcmp(pcgHome, "LIS") == 0)
		blNumericSort = true;

	ogBCD.SetSort("CIC","CNAM+",true,blNumericSort);

	int ilCount = ogBCD.GetDataCount("CIC");

	CString  olCnam;

	for(int j = 0; j < ilCount; j++)
	{
		olCnam = ogBCD.GetField("CIC",j,"CNAM");

		if(olCnam == olCkic)
			break;
	}

	if (bgCnamAtr)
	{
		if (j == ilCount)
		{
			pomDCinsTable->SetIPValue(i + 1, 0, "");
			pomDCinsTable->SetIPValue(i + 1, 1, "");
			pomDCinsTable->SetIPValue(i + 1, 2, "");
			pomDCinsTable->SetIPValue(i + 2, 0, "");
			pomDCinsTable->SetIPValue(i + 2, 1, "");
			pomDCinsTable->SetIPValue(i + 2, 2, "");
			pomDCinsTable->SetIPValue(i + 3, 0, "");
			pomDCinsTable->SetIPValue(i + 3, 1, "");
			pomDCinsTable->SetIPValue(i + 3, 2, "");
		}
		else
		{
			if (ogBCD.GetField("CIC",j + 1,"CNAM") != "NULL")
			{
			pomDCinsTable->SetIPValue(i + 1, 0, ogBCD.GetField("CIC",j + 1,"CNAM"));
				pomDCinsTable->SetIPValue(i + 1, 1, ogBCD.GetField("CIC",j + 1,"CATR"));
				pomDCinsTable->SetIPValue(i + 1, 2, ogBCD.GetField("CIC",j + 1,"TERM"));
			}
			if (ogBCD.GetField("CIC",j + 2,"CNAM") != "NULL")
			{
				pomDCinsTable->SetIPValue(i + 2, 0, ogBCD.GetField("CIC",j + 2,"CNAM"));
				pomDCinsTable->SetIPValue(i + 2, 1, ogBCD.GetField("CIC",j + 1,"CATR"));
				pomDCinsTable->SetIPValue(i + 2, 2, ogBCD.GetField("CIC",j + 2,"TERM"));
			}
			if (ogBCD.GetField("CIC",j + 3,"CNAM") != "NULL")
			{
				pomDCinsTable->SetIPValue(i + 3, 0, ogBCD.GetField("CIC",j + 3,"CNAM"));
				pomDCinsTable->SetIPValue(i + 3, 1, ogBCD.GetField("CIC",j + 1,"CATR"));
				pomDCinsTable->SetIPValue(i + 3, 2, ogBCD.GetField("CIC",j + 3,"TERM"));
			}
/*
			pomDCinsTable->SetIPValue(i + 1, 0, ogBCD.GetField("CIC",j + 1,"CNAM"));
			pomDCinsTable->SetIPValue(i + 1, 1, ogBCD.GetField("CIC",j + 1,"CATR"));
			pomDCinsTable->SetIPValue(i + 1, 2, ogBCD.GetField("CIC",j + 1,"TERM"));
			pomDCinsTable->SetIPValue(i + 2, 0, ogBCD.GetField("CIC",j + 2,"CNAM"));
			pomDCinsTable->SetIPValue(i + 2, 1, ogBCD.GetField("CIC",j + 2,"CATR"));
			pomDCinsTable->SetIPValue(i + 2, 2, ogBCD.GetField("CIC",j + 2,"TERM"));
			pomDCinsTable->SetIPValue(i + 3, 0, ogBCD.GetField("CIC",j + 3,"CNAM"));
			pomDCinsTable->SetIPValue(i + 3, 1, ogBCD.GetField("CIC",j + 3,"CATR"));
			pomDCinsTable->SetIPValue(i + 3, 2, ogBCD.GetField("CIC",j + 3,"TERM"));
			*/
		}

		pomDCinsTable->SetIPValue(i + 1, 3, olCkbs);
		pomDCinsTable->SetIPValue(i + 1, 4, olCkes);

		pomDCinsTable->SetIPValue(i + 2, 3, olCkbs);
		pomDCinsTable->SetIPValue(i + 2, 4, olCkes);
		
		pomDCinsTable->SetIPValue(i + 3, 3, olCkbs);
		pomDCinsTable->SetIPValue(i + 3, 4, olCkes);
	}
	else
	{
		if (j == ilCount)
		{
			pomDCinsTable->SetIPValue(i + 1, 0, "");
			pomDCinsTable->SetIPValue(i + 1, 1, "");
			pomDCinsTable->SetIPValue(i + 2, 0, "");
			pomDCinsTable->SetIPValue(i + 2, 1, "");
			pomDCinsTable->SetIPValue(i + 3, 0, "");
			pomDCinsTable->SetIPValue(i + 3, 1, "");
		}
		else
		{
			if (ogBCD.GetField("CIC",j + 1,"CNAM") != "NULL")
			{
			pomDCinsTable->SetIPValue(i + 1, 0, ogBCD.GetField("CIC",j + 1,"CNAM"));
			pomDCinsTable->SetIPValue(i + 1, 1, ogBCD.GetField("CIC",j + 1,"TERM"));
			}
			if (ogBCD.GetField("CIC",j + 2,"CNAM") != "NULL")
			{
			pomDCinsTable->SetIPValue(i + 2, 0, ogBCD.GetField("CIC",j + 2,"CNAM"));
			pomDCinsTable->SetIPValue(i + 2, 1, ogBCD.GetField("CIC",j + 2,"TERM"));
			}
			if (ogBCD.GetField("CIC",j + 3,"CNAM") != "NULL")
			{
			pomDCinsTable->SetIPValue(i + 3, 0, ogBCD.GetField("CIC",j + 3,"CNAM"));
			pomDCinsTable->SetIPValue(i + 3, 1, ogBCD.GetField("CIC",j + 3,"TERM"));
			}
/*
			pomDCinsTable->SetIPValue(i + 1, 0, ogBCD.GetField("CIC",j + 1,"CNAM"));
			pomDCinsTable->SetIPValue(i + 1, 1, ogBCD.GetField("CIC",j + 1,"TERM"));
			pomDCinsTable->SetIPValue(i + 2, 0, ogBCD.GetField("CIC",j + 2,"CNAM"));
			pomDCinsTable->SetIPValue(i + 2, 1, ogBCD.GetField("CIC",j + 2,"TERM"));
			pomDCinsTable->SetIPValue(i + 3, 0, ogBCD.GetField("CIC",j + 3,"CNAM"));
			pomDCinsTable->SetIPValue(i + 3, 1, ogBCD.GetField("CIC",j + 3,"TERM"));
*/
		}

		pomDCinsTable->SetIPValue(i + 1, 2, olCkbs);
		pomDCinsTable->SetIPValue(i + 1, 3, olCkes);

		pomDCinsTable->SetIPValue(i + 2, 2, olCkbs);
		pomDCinsTable->SetIPValue(i + 2, 3, olCkes);
		
		pomDCinsTable->SetIPValue(i + 3, 2, olCkbs);
		pomDCinsTable->SetIPValue(i + 3, 3, olCkes);
	}

	CCS_CATCH_ALL
	
}


void RotationDlg::SetAOGControl()
{
	CRect olWindowRect;
	CRect olWindowCBRect;
	CFont* tmp = ((CWnd*) this -> GetDlgItem(IDC_ARerouted))->GetFont();

	((CWnd*)GetDlgItem(IDC_ARerouted))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);
	olWindowCBRect.left 	= olWindowRect.left;
	olWindowCBRect.right 	= olWindowRect.right;
	olWindowCBRect.top 		= olWindowRect.top + 20; 
	olWindowCBRect.bottom 	=  olWindowRect.bottom +20;
	pomCBAAOG->Create("AOG", WS_VISIBLE | BS_AUTOCHECKBOX | WS_TABSTOP,olWindowCBRect,this,IDC_BTCRC_ARR_AOG);
	pomCBAAOG->SetFont(tmp);

	

	((CWnd*)GetDlgItem(IDC_DRerouted))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);
	olWindowCBRect.left 	= olWindowRect.left;
	olWindowCBRect.right 	= olWindowRect.right;
	olWindowCBRect.top 		= olWindowRect.top + 20; 
	olWindowCBRect.bottom 	=  olWindowRect.bottom +20;
	pomCBDAOG->Create("AOG", WS_VISIBLE | BS_AUTOCHECKBOX | WS_TABSTOP,olWindowCBRect,this,IDC_BTCRC_DEP_AOG);
	pomCBDAOG->SetFont(tmp);

	if(bgShowAOG)
	{
		pomCBAAOG->SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_AOG"));
		pomCBDAOG->SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_AOG"));
	}
	else
	{
		pomCBAAOG->ShowWindow(FALSE);
		pomCBDAOG->ShowWindow(FALSE);
	}
}

void RotationDlg::SetADHOCControl()
{
	CRect olWindowRect;
	CRect olWindowCBRect;
	CFont* tmp = ((CWnd*) this -> GetDlgItem(IDC_ARerouted))->GetFont();

	((CWnd*)GetDlgItem(IDC_ARETTAXI))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);	
	olWindowCBRect.left 	= olWindowRect.left;
	olWindowCBRect.right 	= olWindowRect.right;
	olWindowCBRect.top 		= olWindowRect.top + 20; 
	olWindowCBRect.bottom 	=  olWindowRect.bottom + 20;
	pomCBAAHOC->Create("Adhoc", WS_VISIBLE | BS_AUTOCHECKBOX | WS_TABSTOP,olWindowCBRect,this,IDC_BTCRC_ARR_ADHOC);
	pomCBAAHOC->SetFont(tmp);

	((CWnd*)GetDlgItem(IDC_DRETTAXI))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);
	olWindowCBRect.left 	= olWindowRect.left;
	olWindowCBRect.right 	= olWindowRect.right;
	olWindowCBRect.top 		= olWindowRect.top + 20; 
	olWindowCBRect.bottom 	=  olWindowRect.bottom +20;
	pomCBDAHOC->Create("Adhoc", WS_VISIBLE | BS_AUTOCHECKBOX | WS_TABSTOP,olWindowCBRect,this,IDC_BTCRC_DEP_ADHOC);
	pomCBDAHOC->SetFont(tmp);

	if(bgShowADHOC)
	{
		pomCBAAHOC->SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_ADHOC"));
		pomCBDAHOC->SetSecState(ogPrivList.GetStat("ROTATIONDLG_CB_ADHOC"));
	}
	else
	{
		pomCBAAHOC->ShowWindow(FALSE);
		pomCBDAHOC->ShowWindow(FALSE);
	}
}


void RotationDlg::OnAAog()
{
	OnEditChanged(0,0);	
}
void RotationDlg::OnDAog()
{
	OnEditChanged(0,0);	
}
void RotationDlg::OnAAdhoc()
{
	OnEditChanged(0,0);	
}
void RotationDlg::OnDAdhoc()
{
	OnEditChanged(0,0);	
}

void RotationDlg::SetEFPSControl()
{
	CRect olWindowRect;
	CRect olWindowBTRect;

	CFont* tmp = ((CWnd*) this -> GetDlgItem(IDC_REQ))->GetFont();
	DWORD buttonStyle = WS_VISIBLE | BS_PUSHBUTTON | BS_OWNERDRAW | WS_TABSTOP;

	((CWnd*)GetDlgItem(IDC_REQ))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect); 
	olWindowBTRect.left 	= olWindowRect.right + 2;
	olWindowBTRect.right 	= olWindowRect.right + 80;
	olWindowBTRect.top 		= olWindowRect.top; 
	olWindowBTRect.bottom 	=  olWindowRect.bottom;
	
	pomCBEFPS->Create("EFPS", buttonStyle ,olWindowBTRect,this,IDC_BTCRC_EFPS);
	pomCBEFPS->SetFont(tmp);

	if(!bgShowEFPS)
	{
		pomCBEFPS->ShowWindow(FALSE);
	}
}

void RotationDlg::OnEFPS()
{
	CString olPermits = "1";
	char alAUrno[16] = "";		
	ltoa(prmAFlight->Urno, alAUrno,10);
	char alDUrno[16] = "";		
	ltoa(prmDFlight->Urno, alDUrno,10);
	
	char pclCheck[256];
	char pclConfigPath[256];
	
	if (getenv("CEDA") == NULL)
		strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
	else
		strcpy(pclConfigPath, getenv("CEDA"));
	
	GetPrivateProfileString(ogAppName, "EFPS" , "FIPS", pclCheck, sizeof pclCheck, pclConfigPath);	
	mdEFPSProcId = LaunchTool( "EFPS", alAUrno, alDUrno, olPermits, "EFPS",pclCheck , mdEFPSProcId );

}

void RotationDlg::PrepareAndSavePermit() 
{//UFIS-3720
	try
	{
		if ( (prmAFlight) && (prmAFlight->Urno > 0) &&
			 (prmAFlightSave!=NULL) && 
			 (strcmp(prmAFlight->Adho,"X")==0)
			 )
		{
			if ( (prmAFlight->Act3 != prmAFlightSave->Act3) ||
				 (prmAFlight->Act5 != prmAFlightSave->Act5) ||
				 (prmAFlight->Regn != prmAFlightSave->Regn)
				)
			{
				PrepareAndSavePermit( prmAFlight, "A" );
			}
		}

		if ((prmDFlight) && (prmDFlight->Urno > 0) &&
			(prmDFlightSave!=NULL) && 
			(strcmp(prmDFlight->Adho,"X")==0)
			)
		{
			if ( (prmDFlight->Act3 != prmDFlightSave->Act3) ||
				 (prmDFlight->Act5 != prmDFlightSave->Act5) ||
				 (prmDFlight->Regn != prmDFlightSave->Regn)
				)
			{
				PrepareAndSavePermit( prmDFlight, "D" );
			}
		}

	}catch(...){}
	
}

void RotationDlg::PrepareAndSavePermit(ROTATIONDLGFLIGHTDATA *prpFlight,CString strADID) 
{
	try
	{

		CString olPern;
		CString olRegn;
		CString olAct3;
		CTime opTime;
		bool bRegnChanges = false;
		bool bActChanges = false;

		int ilDoop = 0;
		
		bool bSave=false;
		m_CE_Regn.GetWindowText(olRegn);
		m_CE_Act3.GetWindowText(olAct3);

		if(strADID == "A") 
		{
				bSave=true;
				olPern = omTmpAPer;

				if(strcmp(prpFlight->Regn,prmAFlightSave->Regn) != 0)
					bRegnChanges = true;

				if(strcmp(prpFlight->Act3,prmAFlightSave->Act3) != 0)
					bActChanges = true;

				CTime tmp1 = prmAFlight->Stoa;
				//ogBasicData.UtcToLocal(tmp1);		in RotationDlg, prmAFlight->Stoa already Local

				TRACE("\n\n %s", tmp1.Format("%Y%m%d%H%M%S"));

				CTime tmp = HourStringToDate( m_AStoa, tmp1);

				TRACE("\n\n %s", tmp1.Format("%Y%m%d%H%M%S"));

				opTime = ConvertFlightPermitTime(tmp);

				TRACE("\n\n %s", opTime.Format("%Y%m%d%H%M%S"));
		}
		else
		{
				bSave=true;
				olPern = omTmpDPer;

				if(strcmp(prpFlight->Regn,prmDFlightSave->Regn) != 0)
					bRegnChanges = true;

				if(strcmp(prpFlight->Act3,prmDFlightSave->Act3) != 0)
					bActChanges = true;

				CTime tmp1 = prmDFlight->Stod;
				//ogBasicData.UtcToLocal(tmp1);		in RotationDlg, prmAFlight->Stod already Local

				CTime tmp = HourStringToDate( m_DStod, tmp1);
				opTime = ConvertFlightPermitTime(tmp);
		}

		m_CE_Regn.GetWindowText(olRegn);

		bool bFpeRecordExist = false;
		FPEDATA *prlFpe = GetValidPermitExist(strADID, prpFlight->Regn, opTime); 

		if(prlFpe == NULL)
			bFpeRecordExist = false;
		else
			bFpeRecordExist = true;

		// if(bSave)
		if(bSave && bFpeRecordExist == false && 
		  (strcmp(olPern, "") == 0 || 
		  (bRegnChanges == true && strcmp(olPern, "Permit Reqd") != 0 && strcmp(olPern, "") != 0)))		// TLE: UFIS 3697
		{
			// Insert new FPE record only when,
			// 1. Permit Request is empty (first time request)
			// 2. Registration changes and no existing Valid Flight Permit for that Registration
			//    and Permit already acquired for the old registration

			FPEDATA *prlFpe = new FPEDATA();

			prlFpe->Urno = ogBasicData.GetNextUrno("FPE");

			CString olMtow = "";
			CString opRegn(prpFlight->Regn);
			if(!opRegn.IsEmpty())
			{
				CString olWhere;
				olWhere.Format("WHERE REGN = '%s'", opRegn);
				ogBCD.Read( "ACR", olWhere);
				ogBCD.GetField("ACR", "REGN", opRegn, "MTOW", olMtow);
				olMtow.TrimLeft('0');
			}

			if(strADID == "A")
			{
				strcpy(prlFpe->Pern, "Permit Reqd");
				ilDoop = opTime.GetDayOfWeek() - 1;
				ilDoop = (ilDoop == 0) ? 7 : ilDoop;

				strcpy(prlFpe->Csgn,m_ACsgn);

				prlFpe->Vafr = opTime;
				prlFpe->Vato = opTime;
			}
			else
			{
				strcpy(prlFpe->Pern, "Permit Reqd");
				ilDoop = opTime.GetDayOfWeek() - 1;
				ilDoop = (ilDoop == 0) ? 7 : ilDoop;
				strcpy(prlFpe->Csgn,m_DCsgn);

				prlFpe->Vafr = opTime;
				prlFpe->Vato = opTime;
			}

			strcpy(prlFpe->Act3,m_Act3);
			strcpy(prlFpe->Act5,m_Act5);
			strcpy(prlFpe->Adid,strADID);
			strcpy(prlFpe->Sufx,prpFlight->Flns);
			strcpy(prlFpe->Mtow,olMtow);
			char pclDays[10];
			GetDayOfWeek(prmAFlight->Stoa, pclDays);
			strcpy(prlFpe->Doop,pclDays);
			strcpy(prlFpe->Regn,m_Regn);

			/*if(strcmp(prmFlight->Adho,"X")==0)
			{
				strcpy(prlFpe->Alco,"");
				strcpy(prlFpe->Flno,"");
			}
			else
			{
				strcpy(prlFpe->Alco,prmFlight->Alc3);
				strcpy(prlFpe->Flno,prmFlight->Fltn);
			}*/
	
			ogFpeData.InsertDB(prlFpe);

			delete prlFpe;
		}
		else if(bSave && (bRegnChanges == true || bActChanges == true) && prlFpe != NULL)	// TLE: UFIS 3697
		{
			// Please make sure the ogFpeData is updated from BC
			strcpy(prlFpe->Regn,olRegn);
			strcpy(prlFpe->Act3,olAct3);
			strcpy(prlFpe->Act5,m_Act5);
			ogFpeData.UpdateDB(prlFpe);
		}
		
	}catch(...){}
	
}

FPEDATA* RotationDlg::GetValidPermitExist(CString strADID, CString opRegistration, CTime opTime)
{
	FPEDATA *prlFpe = NULL;

	if(strADID == "A")
	{
		if(bgShowADHOC || bgSeasonShowADHOC)
			prlFpe = ogFpeData.GetBestPermitForFlight(opTime, true, opRegistration, prmAFlight->Alc3, prmAFlight->Fltn, prmAFlight->Flns,prmAFlight->Csgn, &omArrFlightPermitInfo);
		else
			prlFpe = ogFpeData.GetBestPermitForFlight(opTime, true, opRegistration, prmAFlight->Alc3, prmAFlight->Fltn, prmAFlight->Flns, &omArrFlightPermitInfo);
	}
	else
	{
		if(bgShowADHOC || bgSeasonShowADHOC)
			prlFpe = ogFpeData.GetBestPermitForFlight(opTime, false, opRegistration, prmDFlight->Alc3, prmDFlight->Fltn, prmDFlight->Flns,prmDFlight->Csgn, &omDepFlightPermitInfo);
		else
			prlFpe = ogFpeData.GetBestPermitForFlight(opTime, false, opRegistration, prmDFlight->Alc3, prmDFlight->Fltn, prmDFlight->Flns, &omDepFlightPermitInfo);
	}
	
	return prlFpe;
}