#if !defined(AFX_LISTBOXDLG_H__5EF2F571_9E1E_11D1_837C_0080AD1DC701__INCLUDED_)
#define AFX_LISTBOXDLG_H__5EF2F571_9E1E_11D1_837C_0080AD1DC701__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// ListBoxDlg.h : header file
//

#include <resrc1.h>

/////////////////////////////////////////////////////////////////////////////
// ListBoxDlg dialog

class ListBoxDlg : public CDialog
{
// Construction
public:
	ListBoxDlg(CWnd* pParent, const CString &ropHeader, const CString &ropText, int *pipSel);
	~ListBoxDlg();

	bool AddItem(const CString &ropStr);
	bool SetSize(const CSize &ropSize);

private:
// Dialog Data
	//{{AFX_DATA(ListBoxDlg)
	enum { IDD = IDD_LISTBOXDLG };
	CButton	m_CB_OK;
	CButton	m_CB_Cancel;
	CStatic	m_CS_Text;
	CListBox	m_CL_List;
	//}}AFX_DATA

	CString omHeader;
	CString omText;
	int *pimSel;
	CSize omSize;

	CStringArray olListBoxItems;

	CWnd *pomParent;

protected:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(ListBoxDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation

	// Generated message map functions
	//{{AFX_MSG(ListBoxDlg)
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnDblclkList();
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_LISTBOXDLG_H__5EF2F571_9E1E_11D1_837C_0080AD1DC701__INCLUDED_)
