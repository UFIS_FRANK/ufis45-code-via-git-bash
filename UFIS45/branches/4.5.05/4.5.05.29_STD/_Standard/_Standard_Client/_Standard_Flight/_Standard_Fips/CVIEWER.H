
#ifndef __CVIEWER_H__
#define __CVIEWER_H__

#include <CedaCfgData.h>
#include <CCSGlobl.h>
                                 
#ifndef HKEY_CLASSES_ROOT
    #define HKEY_CLASSES_ROOT           (( HKEY ) 0x80000000 )
#endif
#ifndef HKEY_CURRENT_USER
	#define HKEY_CURRENT_USER           (( HKEY ) 0x80000001 )
#endif
#ifndef HKEY_LOCAL_MACHINE
	#define HKEY_LOCAL_MACHINE          (( HKEY ) 0x80000002 )
#endif
#ifndef HKEY_USERS
	#define HKEY_USERS                  (( HKEY ) 0x80000003 )
#endif



struct ViewerFilter
{
	CTime omFrom;
	CTime omTo;
	CString omDays;
	CTime omLstu;
	CTime omFlukoDate;
	CString omAlc;
	CString omFltn;
	CString omFlns;
	CString omRegn;
	CString omAct;
	CString omOrgDes;
	CString omNature;
	CString omKeycode;
	bool bmArrivals;
	bool bmDepartures;
	bool bmRotations;
	CString omFtyps;
	bool bmAdhoc;

	ViewerFilter();
	void ClearAll();
};



//////////////////////////////////////////////////////////////////////////////////
// MWO: 07.10.1996 
// now we have to read, create and set all views from database. The registry
// will not be used any more. We keep the method-call devices in the same manner.
// For calls there will be no changes and it is transparent like nothing has
// changed. the release, which uses the registry is saved as backup for
// possible problems.
//

class CViewer: public CObject
{
public:
    CViewer(); 
    ~CViewer();
    
public:
	CStringArray omFlightSearchConnection;
	CStringArray omGpeSearchConnection;
	void	SetViewerKey(CString strKey);
	BOOL    CreateView(CString strView, const CStringArray &possibleFilters, bool bpWithDBDelete = true);
	bool	CreateFilter(CString strView, CString &opFilter);
	
	
	void	GetViews(CStringArray &strArray);
    BOOL    SelectView(CString strView);
	CString SelectView();
    BOOL    DeleteView(CString strView, bool bpWithDBDelete = true);
    
	void	GetFilterPage(CStringArray &strArray);
    void    SetFilter(CString strFilter, const CStringArray &opFilter);
    bool	GetFilter(CString strFilter, CStringArray &opFilter);
	bool    GetGroupList(CString &ropGroupList, FipsModules ipModul); 

    void    SetSort(CString opSortName, const CStringArray &opValues);
    void	GetSort(CString opSortName, CStringArray &opSort);
    
	void    SetSearch(CString opSearchName, const CStringArray &opValues);
	void    GetSearch(CString opSearchName, CStringArray &opValues);
/*
	bool	GetFlightWhereString(CString &ropWhere, bool &bpRotation);
	bool	GetZeitraumWhereString(CString &ropWhere, bool &bpRotation, CString &ropFOGTAB, bool bmWithAcOnGround = false);

	bool	GetFlightSearchWhereString(CString &ropWhere, bool &bpRotation, CString &ropFOGTAB, bool bmWithAcOnGround = false, CTime opFrom = TIMENULL, CTime opTo = TIMENULL);
*/
	bool	GetFlightWhereString(CString &ropWhere, bool &bpRotation, CString& ropWhereTime = CString(""), bool bpOngroundInFuture = false);
	bool	GetZeitraumWhereString(CString &ropWhere, bool &bpRotation, CString &ropFOGTAB, bool bmWithAcOnGround = false, bool bpOngroundInFuture = false);

	bool	GetFlightSearchWhereString(CString &ropWhere, bool &bpRotation, CString &ropFOGTAB, bool bmWithAcOnGround = false, CTime opFrom = TIMENULL, CTime opTo = TIMENULL, CString& ropWhereTime = CString(""), bool bpOngroundInFuture = false);

	void	GetFlightTimeSpan(CString &ropFrom, CString &ropTo);
	void	GetTimeSpanAndFtyp(CTime &ropFrom, CTime &ropTo, CString &ropFtyps);
	void	GetViewerFilter(struct ViewerFilter &ropFilter);
	int		GetDOO();
	// PRF 8382
	CString GetUnifilterWhereString();

	void	SetTimeScale(CStringArray &opValues);
	void	GetTimeScale(CStringArray &opValues);

    void    SetGroup(CString strGroup);
	CString GetGroup();
	VIEW_VIEWNAMES * GetActiveView();
	VIEW_TEXTDATA * GetActiveFilter(CString opFilter);
	void SafeDataToDB(CString opViewName);
	CString GetBaseViewName();
	CString GetViewName();
	void SetModul(CString opName);
	void GetAdid(CString& opAdidIn);

	bool GetStev(CString &ropStev);


private:
	CString	m_BaseViewName;
	CString	m_ViewName;
	CString omModul;
	CString omAdidIn;

private:
    BOOL    CheckKey(char* pStrKey);
	BOOL	CreateKey(char* pStrKey);
	BOOL	DeleteKey(char* pStrKey);
	BOOL	SetValue(char* pStrKey, char* pStrValue);
	BOOL	GetValue(char* pStrKey, char* pStrValue);
	BOOL	DeleteFilter(CString strView);
};

#endif //__CVIEWER_H__
