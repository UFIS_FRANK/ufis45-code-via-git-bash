#if !defined(AFX_FLNOASKDLG_H__727B86C1_5D07_11D1_8317_0080AD1DC701__INCLUDED_)
#define AFX_FLNOASKDLG_H__727B86C1_5D07_11D1_8317_0080AD1DC701__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include <CedaFlnoData.h>

// FlnoAskDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// FlnoAskDlg dialog

class FlnoAskDlg : public CDialog
{
// Construction
public:
	FlnoAskDlg(CString opSeas, CString opAlc, CWnd* pParent = NULL);   // standard constructor
	
	~FlnoAskDlg(void);
	void SetMessage(CString opMessage);
	void RemoveAllMessage();
	BOOL DestroyWindow(void);

	BOOL OnInitDialog();

	CString omAlc;
	CString omSeas;

	FLNODATA *pomFlno;

// Dialog Data
	//{{AFX_DATA(FlnoAskDlg)
	enum { IDD = IDD_FLNOASK };
	CButton	m_CB_Cancel;
	CButton	m_CB_Ok;
	CListBox	m_FlnoList;
	BOOL	m_CC_ViewActiv;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(FlnoAskDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(FlnoAskDlg)
	virtual void OnCancel();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_FLNOASKDLG_H__727B86C1_5D07_11D1_8317_0080AD1DC701__INCLUDED_)
