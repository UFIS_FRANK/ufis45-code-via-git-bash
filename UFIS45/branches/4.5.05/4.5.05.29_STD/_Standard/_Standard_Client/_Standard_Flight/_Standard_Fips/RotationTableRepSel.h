#if !defined(AFX_ROTATIONTABLEREPSEL_H__279F2536_4E0A_11D6_8042_0001022205EE__INCLUDED_)
#define AFX_ROTATIONTABLEREPSEL_H__279F2536_4E0A_11D6_8042_0001022205EE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// RotationTableRepSel.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// RotationTableRepSel dialog

class RotationTableRepSel : public CDialog
{
// Construction
public:
	RotationTableRepSel(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(RotationTableRepSel)
	enum { IDD = IDD_DIALOG3 };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(RotationTableRepSel)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(RotationTableRepSel)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ROTATIONTABLEREPSEL_H__279F2536_4E0A_11D6_8042_0001022205EE__INCLUDED_)
