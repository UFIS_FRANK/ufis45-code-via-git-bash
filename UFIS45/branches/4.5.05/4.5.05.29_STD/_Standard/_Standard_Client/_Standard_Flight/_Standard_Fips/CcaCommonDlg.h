#ifndef AFX_CCACOMMONDLG_H__183C77C2_B4D6_11D1_8154_0000B43C4B01__INCLUDED_
#define AFX_CCACOMMONDLG_H__183C77C2_B4D6_11D1_8154_0000B43C4B01__INCLUDED_

// CheckinModifyDlg.h : Header-Datei
//
#include <resource.h>		// Hauptsymbole
#include <CCSEdit.h>
#include <CedaCcaData.h>
#include <CedaFlzData.h>
#include "TimeRange.h"

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CCcaCommonDlg 

class CCcaCommonDlg : public CDialog
{
// Konstruktion
public:
	CCcaCommonDlg(CWnd* pParent = NULL);
	~CCcaCommonDlg();


	void NewData(CCADATA *popCCA);


// Dialogfelddaten
	//{{AFX_DATA(CCcaCommonDlg)
	enum { IDD = IDD_CCA_COMMON_DLG };
	CListBox	m_LstErrors;
	CCSEdit	m_CE_Disp;
	CCSEdit	m_CE_Terminal;
	CCSEdit	m_CE_Schalter;
	CCSEdit	m_CE_Fluggs;
	CCSEdit	m_CE_Beginn_d;
	CCSEdit	m_CE_Beginn_t;
	CCSEdit	m_CE_Ende_t;
	CCSEdit	m_CE_Ende_d;
	CCSEdit	m_CE_ActStartTime;
	CCSEdit	m_CE_ActStartDate;
	CCSEdit	m_CE_ActEndTime;
	CCSEdit	m_CE_ActEndDate;
	CCSEdit	m_CE_Rem;
	CCSEdit	m_CE_Logo;
	CString	m_Beginn_t;
	CString	m_Beginn_d;
	CString	m_Ende_d;
	CString	m_Ende_t;
	CString m_ActStartTime;
	CString m_ActStartDate;
	CString m_ActEndTime;
	CString m_ActEndDate;
	CString	m_Rem;
	CString	m_Logo;
	CString	m_Fluggs;
	CString	m_Schalter;
	CString	m_Terminal;
	CString	m_Disp;
	CButton m_CB_FidsRemList;
	CButton	m_CB_PreCheckIn;
	BOOL	m_PreCheckIn;
	CCSEdit	m_CE_Catr; 
	CButton m_CB_AirlGrpList; 
	CButton	m_CB_DCicLogo1;
	//}}AFX_DATA


// Überschreibungen
	// Vom Klassen-Assistenten generierte virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(CCcaCommonDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	//}}AFX_VIRTUAL

// Implementierung
protected:

	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(CCcaCommonDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	afx_msg void OnFidsRemList();
	afx_msg void OnClose();
	afx_msg LONG OnEditChanged( UINT wParam, LPARAM lParam);
	afx_msg LONG OnEditKillfocus( UINT wParam, LPARAM lParam);
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	virtual void OnCancel();
	afx_msg void OnAirlGrpList();
	afx_msg void OnDcicLogo1();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:

	CCADATA omCCA;
	CedaCcaData omCcaData;	
	CedaFlzData omFlzData;	

	void AddErrorString( CString &sError );
	void AddErrorString( const char *pcError );
	LONG OnEditKillfocus_CE_Fluggs( CCSEDITNOTIFY* pCcsEditNotify );
	LONG OnEditKillfocus_CE_Schalter( CCSEDITNOTIFY* pCcsEditNotify );
	unsigned long m_ulUrnoValidCounter ;		// store the urno of the current counter if valid
	
	/*
	bool Check_Beginn( CCSEDITNOTIFY* pCcsEditNotify, CCSEdit& WndDate, CCSEdit& WndTime );		// 050329 MVy: check date and time, return true if ok
	bool Check_Ende( CCSEDITNOTIFY* pCcsEditNotify, CCSEdit& WndDate, CCSEdit& WndTime );
	bool Check_Date( CCSEDITNOTIFY* pCcsEditNotify, CCSEdit& WndDate, CCSEdit& WndTime, char* pcInfo );
	/**/

	CTime GetDateTime( CCSEdit& WndDate, CCSEdit& WndTime );
	bool CheckPeriod( CTimeRange& range, bool canbeempty = false );
	void UpdateAllFields();		// 050407 MVy: read data from edit windows, pressing ok within an editbox will not cause focus lost before OnOk
	bool CheckAllFieldsValid();		// 050407 MVy: verify values of fields, return true only if ALL fields are valid
	bool AllowOk( bool error = false );		// 050407 MVy: helper to enable or disable the ok button, disable on any error, enable only after checking all fields

	CTimeRange m_rngSchedule ;
	void SetTimeSchedule();
	LONG OnEditKillfocus_CE_Beginn_d( CCSEDITNOTIFY* pCcsEditNotify );
	LONG OnEditKillfocus_CE_Beginn_t( CCSEDITNOTIFY* pCcsEditNotify );
	LONG OnEditKillfocus_CE_Ende_d( CCSEDITNOTIFY* pCcsEditNotify );
	LONG OnEditKillfocus_CE_Ende_t( CCSEDITNOTIFY* pCcsEditNotify );

	CTimeRange m_rngActual ;
	void SetTimeActual();
	LONG OnEditKillfocus_CE_ActStartDate( CCSEDITNOTIFY* pCcsEditNotify );
	LONG OnEditKillfocus_CE_ActStartTime( CCSEDITNOTIFY* pCcsEditNotify );
	LONG OnEditKillfocus_CE_ActEndDate( CCSEDITNOTIFY* pCcsEditNotify );
	LONG OnEditKillfocus_CE_ActEndTime( CCSEDITNOTIFY* pCcsEditNotify );
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio fügt zusätzliche Deklarationen unmittelbar vor der vorhergehenden Zeile ein.

#endif // AFX_CCACOMMONDLG_H__183C77C2_B4D6_11D1_8154_0000B43C4B01__INCLUDED_
