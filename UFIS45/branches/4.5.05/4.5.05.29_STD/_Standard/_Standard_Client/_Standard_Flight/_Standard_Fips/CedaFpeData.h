// Class for Fpeipment
#ifndef _CEDAFPEDATA_H_
#define _CEDAFPEDATA_H_

#include <CCSCedaData.h>
#include <CCSPtrArray.h>

/////////////////////////////////////////////////////////////////////////////
// Record structure declaration

struct FpeDataStruct
{
	long	Urno;
	CTime	Vafr;
	CTime	Vato;
	char	Pern[33];
	char	Adid[2];
	char	Alco[4];
	char	Flno[11];
	char	Sufx[2];
	char	Regn[13];
	char	Doop[8];
	char	AtdNo[12]; //ATD Number
	char 	 Csgn[10]; 	// Call- Sign
	char 	 Act3[5]; 	// Flugzeug-3-Letter Code (IATA)
	char 	 Act5[7]; 	// Flugzeug-5-Letter Code
	char 	 Mtow[13]; 	// MTow

	// internal fields
	int		Score;

	int      IsChanged;

	FpeDataStruct(void)
	{
		IsChanged = DATA_UNCHANGED;
		Urno = 0L;
		strcpy(Pern,"");
		strcpy(Adid,"");
		strcpy(Alco,"");
		strcpy(Flno,"");
		strcpy(Sufx,"");
		strcpy(Regn,"");
		strcpy(Doop,"");
		strcpy(AtdNo,"");
		strcpy(Csgn,"");
		strcpy(Act3,"");
		strcpy(Act5,"");
	}
};

typedef struct FpeDataStruct FPEDATA;


/////////////////////////////////////////////////////////////////////////////
// Class declaration

class CedaFpeData: public CCSCedaData
{

// Attributes
public:
    CCSPtrArray <FPEDATA> omData;
	CMapPtrToPtr omUrnoMap;
	CMapStringToPtr omAlcoMap;
	CMapStringToPtr omRegnMap;	//Regn Map
	CMapStringToPtr omCsgnMap;	//Regn Map

	char pcmFlightPermitsPath[256];
	char pcmFList[512];
	CString omFtyps;
	bool bmFlightPermitsEnabled;

// Operations
public:
	CedaFpeData();
	~CedaFpeData();
	bool ReadFpeData();
	FPEDATA* GetFpeByUrno(long lpUrno);
	void ProcessFpeBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
	bool FlightHasPermit(CTime opTime, bool bpArrival, CString opRegn, CString opAlco, CString opFlno, CString opSufx,CString opCsgn="");
	FPEDATA* GetBestPermitForFlight(CTime opTime, bool bpArrival, CString opRegn, CString opAlco, CString opFlno, CString opSufx, CString *popPermitInfo = NULL);
	FPEDATA* GetBestPermitForFlight(CTime opTime, bool bpArrival, CString opRegn, CString opAlco, CString opFlno, CString opSufx,CString opCsgn , CString *popPermitInfo = NULL);
	
	int TestMatch(FPEDATA *prpFpe, CTime opTime, bool bpArrival, CString opRegn, CString opFlno, CString opSufx, CString *popTrace = NULL);
	bool CheckDoop(CString opFpeDoop, CTime opFlightTime);
	//bool StartFlightPermitsAppl(CTime opTime, bool bpArrival, CString opRegn, CString opAlco, CString opFlno, CString opSufx);
	//bool StartFlightPermitsAppl(CTime opTime, bool bpArrival, CString opRegn, CString opAlco, CString opFlno, CString opSufx,CString opCsgn="");
	bool StartFlightPermitsAppl(CTime opTime, bool bpArrival, CString opRegn, CString opAlco, CString opFlno, CString opSufx, CString opCsgn="",CString opAct3="",CString opAct5="",CString opMode="0");
	bool StartFlightPermitsApplS(CString olPern, CTime opTime, bool bpArrival, CString opRegn, CString opAlco, CString opFlno, CString opSufx, CString opCsgn="",CString opAct3="",CString opAct5="",CString opMode="0",CString opPerUrno="");
	bool FtypEnabled(const char *pcpFtyp);
	void AddFpeToCsgnMap(FPEDATA *prpFpe);
	void DeleteFpeFromCsgnMap(const char *pcpCsgn, long lpFpeUrno);
	bool InsertDB(FPEDATA *prlFpe);
	void SetFieldList(void);
	CTime UtcToLocal(CTime opTime);
	//void AssignPer(CString afCsgn, CString afRegn, CString afStox, CString afAdid);
	void AssignPer(CTime opTime, bool bpArrival, CString opRegn, CString opAlco, CString opFlno, CString opSufx,CString opCsgn="");
	bool UpdateDB(FPEDATA *prlFpe);

private:
	FPEDATA *AddFpeInternal(FPEDATA &rrpFpe);
	
	void AddFpeToAlcoMap(FPEDATA *prpFpe);
	void AddFpeToRegnMap(FPEDATA *prpFpe); 
	
	void DeleteFpeInternal(long lpUrno);

	void DeleteFpeFromAlcoMap(const char *pcpAlco, long lpFpeUrno);
	void DeleteFpeFromRegnMap(const char *pcpRegn, long lpFpeUrno);
	
	void ClearAll();
	
	void DisplayTime(CTime opTime);
	bool Save(FPEDATA *prlFpe);
};


extern CedaFpeData ogFpeData;
#endif _CEDAFPEDATA_H_
