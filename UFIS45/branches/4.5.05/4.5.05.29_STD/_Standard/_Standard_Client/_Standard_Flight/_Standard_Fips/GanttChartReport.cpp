// GanttChartReport.cpp : implementation file
//

#include <stdafx.h>
#include <GanttChartReport.h>
#include <GanttBarReport.h>
#include <CCSGlobl.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

IMPLEMENT_DYNCREATE(GanttChartReport, CScrollView)


#define SPACE_KO_LEFT	10
#define SPACE_KO_TOP	30
#define SPACE_KO_BOTTOM	45

/////////////////////////////////////////////////////////////////////////////
// GanttChartReport
GanttChartReport::GanttChartReport()
{

	char pclTmpText[512], pclConfigPath[512];
	lmCharSet = DEFAULT_CHARSET;
	if (getenv("CEDA") == NULL)
		strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
	else
		strcpy(pclConfigPath, getenv("CEDA"));

	GetPrivateProfileString(ogAppName, "CHARSET", "DEFAULT", pclTmpText, sizeof pclTmpText, pclConfigPath);
	if(!strcmp(pclTmpText, "ARABIC"))
		lmCharSet = ARABIC_CHARSET;

}


GanttChartReport::GanttChartReport(CWnd* ppParent, CRect olRect, CString opXAxe, 
								   CString opYAxe, CColor opColor)
{

	omRectWnd = olRect;
	omXAxe = opXAxe;
	omYAxe = opYAxe;
	omColor = opColor;
	imArrivalHeight = 0;
	imArrivalData =0;
	imDeparture = 0;

}


GanttChartReport::~GanttChartReport()
{

	omBars.DeleteAll();

}


BEGIN_MESSAGE_MAP(GanttChartReport, CScrollView)
	//{{AFX_MSG_MAP(GanttChartReport)
	ON_WM_CONTEXTMENU()
	ON_WM_ERASEBKGND()
	ON_WM_LBUTTONDBLCLK()
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_WM_MOUSEMOVE()
	ON_WM_RBUTTONDOWN()
	ON_WM_CREATE()
	// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// GanttChartReport message handlers


void GanttChartReport::OnInitialUpdate()
{

	CScrollView::OnInitialUpdate();

	CSize olSizeTotal;
	CPaintDC dc(GetParent());

    LOGFONT logFont;
    memset(&logFont, 0, sizeof(LOGFONT));

	logFont.lfCharSet= lmCharSet;
    logFont.lfHeight = - MulDiv(7, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_NORMAL;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_ROMAN;
	logFont.lfQuality = PROOF_QUALITY;
    lstrcpy(logFont.lfFaceName, "Arial");
    omArial_7.CreateFontIndirect(&logFont);
	dc.SelectObject(&omArial_7);

	// Mindestbreite eines Balkens (= 4 digits) bestimmen
	CSize olSizeMinX = dc.GetTextExtent("9999");

	// tats�chliche Breite eines Balkens ermitteln
	int ilBarWidth = 0;
	if (imDataCount > 0)
		                 // Breite - linker Rand - rechter Rand - Platz lassen
		ilBarWidth = ((omRectWnd.Width() - SPACE_KO_LEFT - 10 - 15) / imDataCount);

	if(ilBarWidth < 40)
		ilBarWidth = 40;

	// wenn srcollen Breite eines Balkens auf ermittelte Mindestberiete setzen
	if (olSizeMinX.cx > ilBarWidth)
		imBarWidth = olSizeMinX.cx + 4; // wegen Rand des Rectangles + 4;
	else
		imBarWidth = ilBarWidth;
								           
	// L�nge der X-Achse = Anzahl Balken * Breite eines Balken + Rahmen - linker + rechter Rand
	//imSizeX = imDataCount * (imBarWidth + 3) - 32;

	// L�nge der X-Achse = (Anzahl Balken+1 (samit das Ende des Balkens erreicht wird) * Breite eines Balken + Abstand
	if (imBarWidth == olSizeMinX.cx + 4)
		imSizeX = (imDataCount+1) * imBarWidth + 15;
	else
		imSizeX = (imDataCount) * imBarWidth + 15;
	if (imSizeX < omRectWnd.Width())
		imSizeX = omRectWnd.Width();

	if (imSizeX-20 < omRectWnd.Width())
		olSizeTotal.cx = omRectWnd.Width();
	else
		olSizeTotal.cx = imSizeX;
	olSizeTotal.cy = omRectWnd.Height() - 25;

	SetScrollSizes(MM_TEXT, olSizeTotal);

}


BOOL GanttChartReport::OnEraseBkgnd(CDC* pDC) 
{

    // TODO: Add your message handler code here and/or call default
	COLORREF llBkColor;
	llBkColor = RGB(255, 255, 255);

    CRect olClipRect;
    pDC->GetClipBox(&olClipRect);

    CBrush olBrush(llBkColor);
    CBrush *polOldBrush = pDC->SelectObject(&olBrush);
    pDC->PatBlt(olClipRect.left, olClipRect.top,
        olClipRect.Width(), olClipRect.Height(),
        PATCOPY);
    pDC->SelectObject(polOldBrush);
    
    return TRUE;

}


void GanttChartReport::OnDraw(CDC* pDC)
{

    CRect olClipRect;
	pDC->GetClipBox(&olClipRect);
	

	// --------------------
	// Malen des KO-Systems
	// --------------------
	// kleine Schrift w�hlen
    LOGFONT logFont;
    memset(&logFont, 0, sizeof(LOGFONT));

	CFont olArial_8;
	logFont.lfCharSet= lmCharSet;
    logFont.lfHeight = - MulDiv(8, pDC->GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_NORMAL;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_ROMAN;
	logFont.lfQuality = PROOF_QUALITY;
    lstrcpy(logFont.lfFaceName, "Arial");
    olArial_8.CreateFontIndirect(&logFont);
	pDC->SelectObject(&olArial_8);


	// Setzen des Ausgangspunktes
	pDC->MoveTo(SPACE_KO_LEFT, SPACE_KO_TOP);
	// Y-Achse
	pDC->LineTo(SPACE_KO_LEFT, omRectWnd.Height() - SPACE_KO_BOTTOM);

	// Text der Y-Achse
	CSize olSize = pDC->GetTextExtent(omYAxe);
	pDC->TextOut(SPACE_KO_LEFT + 5, SPACE_KO_TOP, omYAxe);
	
	//Pfeil der Y-Achse
	CPoint olPoints[3];;
	olPoints[0].x = SPACE_KO_LEFT - 3;
	olPoints[0].y = SPACE_KO_TOP;

	olPoints[1].x = SPACE_KO_LEFT + 3;
	olPoints[1].y = SPACE_KO_TOP;

	olPoints[2].x = SPACE_KO_LEFT;
	olPoints[2].y = SPACE_KO_TOP - 6;

	pDC->SetPolyFillMode(ALTERNATE);
	pDC->Polygon(olPoints, 3);

	// X-Achse
	pDC->LineTo((imSizeX - 20), omRectWnd.Height() - SPACE_KO_BOTTOM);
	// Text der X-Achse
	olSize = pDC->GetTextExtent(omXAxe);
	pDC->TextOut((imSizeX - 20) - olSize.cx, omRectWnd.Height() - 20  - olSize.cy, omXAxe);

	//Pfeil der X-Achse
	pDC->MoveTo(omRectWnd.Width() - SPACE_KO_LEFT, omRectWnd.Height() - SPACE_KO_BOTTOM);
	olPoints[0].x = imSizeX - 20;
	olPoints[0].y = omRectWnd.Height() - SPACE_KO_BOTTOM - 3;

	olPoints[1].x = imSizeX - 20;
	olPoints[1].y = omRectWnd.Height() - SPACE_KO_BOTTOM + 3;

	olPoints[2].x = (imSizeX - 20) + 6;
	olPoints[2].y = omRectWnd.Height() - SPACE_KO_BOTTOM;

	pDC->SetPolyFillMode(ALTERNATE);
	pDC->Polygon(olPoints, 3);

	// Balken erzeugen und ausgeben
	if (omData.GetSize() == omXAxeText.GetSize())
	{
		for (int i = 0; i < omData.GetSize(); i++)
		{
			//Changed for the PRF 8494 by sisl
			//initialize the Arrival bar height
			imArrivalHeight = GetArrivalBarHeight(i);
			imArrivalData = omArrivalData[i];
			imDeparture = omDepartureData[i];
			omBars.Add(new GanttBarReport(this, omColor, omData[i], omXAxeText[i]));
			if (omBarText.GetSize() > 0)
				omBars[i].Draw(pDC, SPACE_KO_LEFT, i, GetBarHeight(i), omBarText[i]);
			else
				omBars[i].Draw(pDC, SPACE_KO_LEFT, i, GetBarHeight(i));
		}
	}
	else
	{
		MessageBox("Fehler in den Daten", "GanttChart");
	}

}


void GanttChartReport::SetData(const CUIntArray& opData)
{

	omData.Copy(opData);
	imDataCount = omData.GetSize();

	SetHighAndLowData();

}

//This function is used to Set the Arrival Flight Data 
//added for the PRF 8494 by sisl
void GanttChartReport::SetArrivalData(const CUIntArray& opData)
{
	omArrivalData.Copy(opData);
}

//This function is used to set the Departure Flight Data 
//added for the PRF 8494 by sisl
void GanttChartReport::SetDepartureData(const CUIntArray& opData)
{
	omDepartureData.Copy(opData);
}


void GanttChartReport::SetXAxeText(const CStringArray& opXAxeText)
{

	omXAxeText.Copy(opXAxeText);

	if (omXAxeText.GetSize() < omData.GetSize())
	{
		// Rest mit Leerstrings f�llen
		for (int i = omXAxeText.GetSize(); i < omData.GetSize(); i++)
			omXAxeText.Add("");
	}

}


void GanttChartReport::SetBarText(const CStringArray& opBarText)
{

	omBarText.Copy(opBarText);

	if (omBarText.GetSize() < omData.GetSize())
	{
		// Rest mit Leerstrings f�llen
		for (int i = omBarText.GetSize(); i < omData.GetSize(); i++)
			omBarText.Add("");
	}

}


int GanttChartReport::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{

	lpCreateStruct->style &= WS_VSCROLL;
	if (CScrollView::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	return 0;

}


int GanttChartReport::GetGanttYBeginn()
{
	
	return omRectWnd.Height() - SPACE_KO_BOTTOM;

}


int GanttChartReport::GetGanttYBeginnPrint()
{

	return (int)(omRectPrn.Height() - (25 * dmFactorY));

}


int GanttChartReport::GetGanttBarWidthPrint()
{

	// Breite - linker Rand - rechter Rand - Platz lassen 
	return (int)((omRectPrn.Width() - (SPACE_KO_LEFT * dmFactorX) - (10 * dmFactorX) - (5 * dmFactorX)) / imDataCount);

}


int GanttChartReport::GetGanttBarWidth()
{

	
	return imBarWidth;

}


void GanttChartReport::SetHighAndLowData()
{

	// groessten und kleinsten Wert extrahieren
	int ilCompare;
	if (omData.GetSize() > 0)
	{
		imLow  = omData[0];
		imHigh = omData[0];

		for (int i = 0; i < omData.GetSize(); i++)
		{
			ilCompare = omData[i];
			if (ilCompare > imHigh)
				imHigh = ilCompare;
			if (ilCompare < imLow)
				imLow = ilCompare;
		}
	}

}


int GanttChartReport::GetBarHeight(int ipIndex)
{

	if (ipIndex > omData.GetSize())
		return 0;

	double dl = (double)(omRectWnd.Height() - 90) / (double)imHigh;

	// Balkenh�he = (Gesamthoehe / h�chster Wert) * aktueller Wert
	return (int)(dl * omData[ipIndex]);

}

//This function is used to get the Arrival Bar Height
//Added for the PRF 8494 by sisl
int GanttChartReport::GetArrivalBarHeight(int ipIndex)
{

	if (ipIndex > omArrivalData.GetSize())
		return 0;

	double dl = (double)(omRectWnd.Height() - 90) / (double)imHigh;

	// Balkenh�he = (Gesamthoehe / h�chster Wert) * aktueller Wert
	return (int)(dl * omArrivalData[ipIndex]);

}

//Get the Arrival Bar Height while printing //PRF 8494
int GanttChartReport::GetArrivalBarHeightPrint(int ipIndex)
{
	if (ipIndex > omArrivalData.GetSize())
		return 0;

	double dl = (double)(omRectPrn.Height() - (120 * dmFactorY)) / (double)imHigh;

	// Balkenh�he = (Gesamthoehe / h�chster Wert) * aktueller Wert
	int ret = (int)(dl * omArrivalData[ipIndex]);
	return ret;
}

int GanttChartReport::GetBarHeightPrint(int ipIndex)
{

	if (ipIndex > omData.GetSize())
		return 0;

	double dl = (double)(omRectPrn.Height() - (120 * dmFactorY)) / (double)imHigh;

	// Balkenh�he = (Gesamthoehe / h�chster Wert) * aktueller Wert
	int ret = (int)(dl * omData[ipIndex]);
	return ret;

}


void GanttChartReport::Print(CDC* popDC, CCSPrint* popPrint, CString opHeader, CString opFooterLeft)
{

	popDC->SetMapMode(MM_TEXT);

	popDC->GetClipBox(&omRectPrn);


    CPaintDC dc(this);
    CRect olClipRectWnd;
    dc.GetClipBox(&olClipRectWnd);

	// Umrechnungsfaktor f�rs Drucken berechnen
	int ilPixMonX = dc.GetDeviceCaps(LOGPIXELSX);
	int ilPixMonY = dc.GetDeviceCaps(LOGPIXELSY);

	int ilPixPrintX = popDC->GetDeviceCaps(LOGPIXELSX);
	int ilPixPrintY = popDC->GetDeviceCaps(LOGPIXELSY);

	dmFactorX = (double)ilPixPrintX / (double)ilPixMonX;
	dmFactorY = (double)ilPixPrintY / (double)ilPixMonY;

	// verkleinern wegen Kopf- und Fusszeile
	omRectPrn.top += (long)(25 * dmFactorX);
	omRectPrn.bottom -= (long)(25 * dmFactorX);

	omHeader = opHeader;
	omFooterLeft = opFooterLeft;
	PrintHeader(popPrint, opHeader);
	PrintFooter(popPrint, opFooterLeft, 1);
	PrintCoordinateSystem(popDC);

	PrintBars(popDC, popPrint);

	// Druck abschlie�en
	popPrint->omCdc.EndPage();
	popPrint->omCdc.EndDoc();

}


void GanttChartReport::PrintHeader(CCSPrint* popPrint, CString opHeader)
{

	popPrint->PrintUIFHeader("", opHeader, popPrint->imFirstLine-10);

}


void GanttChartReport::PrintFooter(CCSPrint* popPrint, CString opFooterLeft, int ipPageNo)
{

	CString olFooterRight;

	olFooterRight.Format("Page: %d", ipPageNo);
	popPrint->PrintUIFFooter(opFooterLeft,"", olFooterRight);

}


// --------------------
// Malen des KO-Systems
// --------------------
void GanttChartReport::PrintCoordinateSystem(CDC* popDC)
{

	// kleine Schrift w�hlen
    LOGFONT logFont;
    memset(&logFont, 0, sizeof(LOGFONT));

	CFont olArial_8;
	logFont.lfCharSet= lmCharSet;
    logFont.lfHeight = - MulDiv(8, popDC->GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_NORMAL;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_ROMAN;
	logFont.lfQuality = PROOF_QUALITY;
    lstrcpy(logFont.lfFaceName, "Arial");
    olArial_8.CreateFontIndirect(&logFont);
	popDC->SelectObject(&olArial_8);
 
	// Setzen des Ausgangspunktes +35 wegen Footer
	popDC->MoveTo((int)(SPACE_KO_LEFT * dmFactorX), (int)(80 * dmFactorY));
	// Y-Achse
	popDC->LineTo((int)(SPACE_KO_LEFT * dmFactorX), (int)(omRectPrn.Height() - (25 * dmFactorY)));

	// Text der Y-Achse
	CSize olSize = popDC->GetTextExtent(omYAxe);
	popDC->TextOut((int)((SPACE_KO_LEFT + 5) * dmFactorX), (int)(80 * dmFactorY), omYAxe);
	
	//Pfeil der Y-Achse
	CPoint olPoints[3];;
	olPoints[0].x = (long)((SPACE_KO_LEFT - 3) * dmFactorX);
	olPoints[0].y = (long)(80 * dmFactorY);

	olPoints[1].x = (long)((SPACE_KO_LEFT + 3) * dmFactorX);
	olPoints[1].y = (long)(80 * dmFactorY);

	olPoints[2].x = (long)(SPACE_KO_LEFT * dmFactorX);
	olPoints[2].y = (long)(74 * dmFactorY);

	popDC->SetPolyFillMode(ALTERNATE);
	popDC->Polygon(olPoints, 3);

	// X-Achse
	popDC->LineTo((int)(omRectPrn.Width() - (SPACE_KO_LEFT * dmFactorX)), (int)(omRectPrn.Height() - (25 * dmFactorY)));
	// Text der X-Achse
	olSize = popDC->GetTextExtent(omXAxe);
	popDC->TextOut((int)(omRectPrn.Width() - (SPACE_KO_LEFT * dmFactorX) - olSize.cx), (int)(omRectPrn.Height() - olSize.cy), omXAxe);

	// Pfeil der X-Achse
	popDC->MoveTo((int)(omRectPrn.Width() - (SPACE_KO_LEFT * dmFactorX)), (int)(omRectPrn.Height() - (25 * dmFactorY)));
	olPoints[0].x = (long)(omRectPrn.Width() - (SPACE_KO_LEFT * dmFactorX));
	olPoints[0].y = (long)(omRectPrn.Height() - (25 * dmFactorY) - (3 * dmFactorY));

	olPoints[1].x = (long)(omRectPrn.Width() - (SPACE_KO_LEFT * dmFactorX));
	olPoints[1].y = (long)(omRectPrn.Height() - (25 * dmFactorY) + (3 * dmFactorY));

	olPoints[2].x = (long)(omRectPrn.Width() - (SPACE_KO_LEFT * dmFactorX) + (6 * dmFactorX));
	olPoints[2].y = (long)(omRectPrn.Height() - (25 * dmFactorY));

	popDC->SetPolyFillMode(ALTERNATE);
	popDC->Polygon(olPoints, 3);

}


void GanttChartReport::PrintBars(CDC* popDC, CCSPrint* popPrint)
{

	CString olBarText;
	// Balken erzeugen und ausgeben
	if (omData.GetSize() == omXAxeText.GetSize())
	{
		int ilBarNo = 0;
		int ilPageNo = 1;
		for (int i = 0; i < omData.GetSize(); i++, ilBarNo++)
		{
			if (omBarText.GetSize() > 0)
				olBarText = omBarText[i];
			else
				olBarText = "";

			//Fix of PRF 8494
			//Get the Bar Data 
			imArrivalHeight = GetArrivalBarHeightPrint(i);
			imArrivalData	= omArrivalData[i];
			imDeparture		= omDepartureData[i];

			if (!omBars[i].Print(popDC, (int)(SPACE_KO_LEFT * dmFactorX), ilBarNo, (int)((GetBarHeightPrint(i))), dmFactorY, olBarText))
			{
				// neue Seite
				popDC->EndPage();
				// von vorne auf neuer Seite mit Balkennummer beginnen
				ilBarNo = 0;
				// i zur�cksetzen, da aktuelles i nicht gedruckt wurde
				--i;
				// auf neue Seite Kop- und Fusszeile drucken
				PrintHeader(popPrint, omHeader);
				PrintFooter(popPrint, omFooterLeft, ++ilPageNo);
			}
		}
	}
	else
	{
		MessageBox("Fehler in den Daten", "GanttChart");
	}

}


BOOL GanttChartReport::Create(LPCTSTR lpszClassName, LPCTSTR lpszWindowName, DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID, CCreateContext* pContext) 
{
	// TODO: Add your specialized code here and/or call the base class

	CWnd::Create(lpszClassName, lpszWindowName, dwStyle, rect, pParentWnd, nID, pContext);

	return TRUE;
}


CFont* GanttChartReport::GetGanttChartFont()
{

	return &omArial_7;

}
