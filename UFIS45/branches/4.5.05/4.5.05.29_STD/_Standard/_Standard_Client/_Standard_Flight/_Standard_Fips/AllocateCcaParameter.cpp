// AllocateCcaParameter.cpp: Implementierungsdatei
//

#include <stdafx.h>
#include <fpms.h>
#include <AllocateCcaParameter.h>
#include <ccsglobl.h>
#include <basicdata.h>
#include <resrc1.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld AllocateCcaParameter 


AllocateCcaParameter::AllocateCcaParameter(CWnd* pParent /*=NULL*/, bool bpResetOnly)
	: CDialog(AllocateCcaParameter::IDD, pParent)
{
	//{{AFX_DATA_INIT(AllocateCcaParameter)
		// HINWEIS: Der Klassen-Assistent f�gt hier Elementinitialisierung ein
	//}}AFX_DATA_INIT
	bmResetOnly = bpResetOnly;
}


void AllocateCcaParameter::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(AllocateCcaParameter)
	DDX_Control(pDX, IDC_OVERLAP, m_CB_Overlap);
	DDX_Control(pDX, IDC_TIME_FROM,    m_CE_Time_From);
	DDX_Control(pDX, IDC_TIME_TO,    m_CE_Time_To);
	DDX_Control(pDX, IDC_DATE_TO,    m_CE_Date_To);
	DDX_Control(pDX, IDC_DATE_FROM,    m_CE_Date_From);
		// HINWEIS: Der Klassen-Assistent f�gt hier DDX- und DDV-Aufrufe ein
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(AllocateCcaParameter, CDialog)
	//{{AFX_MSG_MAP(AllocateCcaParameter)
	ON_BN_CLICKED(IDC_RESET, OnReset)
	ON_BN_CLICKED(IDC_CANCEL, OnCancel)
	ON_BN_CLICKED(IDC_ALL, OnAll)
	ON_BN_CLICKED(IDC_NOTALLOCATED, OnNotallocated)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten AllocateCcaParameter 
BOOL AllocateCcaParameter::OnInitDialog() 
{
	CDialog::OnInitDialog();
	CPoint point;
	::GetCursorPos(&point);
	this->SetWindowPos(&wndTop, point.x, point.y,0,0, SWP_SHOWWINDOW | SWP_NOSIZE);// | SWP_NOMOVE );

	CWnd *polWnd;

	if(bmResetOnly)
	{
		polWnd = GetDlgItem(IDC_ALL);
		polWnd->EnableWindow(FALSE);
		polWnd = GetDlgItem(IDC_NOTALLOCATED);
		polWnd->EnableWindow(FALSE);
	}

	CRect r;
	if(bgAutoAllocTimeFrame)
	{
		m_CE_Time_From.SetTypeToTime();
		m_CE_Time_To.SetTypeToTime();
		m_CE_Date_From.SetTypeToDate();
		m_CE_Date_To.SetTypeToDate();
	}
	else
	{
		m_CE_Time_From.ShowWindow(SW_HIDE);
		m_CE_Time_To.ShowWindow(SW_HIDE);
		m_CE_Date_From.ShowWindow(SW_HIDE);
		m_CE_Date_To.ShowWindow(SW_HIDE);
		GetDlgItem(IDC_FROMTO_FRAME)->ShowWindow(false);

		int ilMoveUp = 75;
		this->GetWindowRect(r);
		r.bottom -= ilMoveUp;
		this->MoveWindow(r);
		
	}



	//UpdateData(FALSE);
	// TODO: Add extra initialization here
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void AllocateCcaParameter::OnAll() 
{
	if(!GetAllocTimeFrame() )
		return;

	if(MessageBox( GetString(IDS_STRING1772), GetString(IDS_STRING1811), MB_YESNO ) != IDYES)
		return;

	if(m_CB_Overlap.GetCheck())
		EndDialog(3);
	else
		EndDialog(0);

}

void AllocateCcaParameter::OnNotallocated() 
{
	if(!GetAllocTimeFrame() )
		return;

	
	if(MessageBox( GetString(IDS_STRING1772), GetString(IDS_STRING1811), MB_YESNO ) != IDYES)
		return;

	if(m_CB_Overlap.GetCheck())
		EndDialog(4);	
	else
		EndDialog(1);	

}

void AllocateCcaParameter::OnReset() 
{
	if(!GetAllocTimeFrame() )
		return;

	
	if(MessageBox( GetString(IDS_STRING1772), GetString(IDS_STRING1812), MB_YESNO ) != IDYES)
		return;

	EndDialog(2);	
}

void AllocateCcaParameter::OnCancel() 
{
	if(!GetAllocTimeFrame() )
		return;

	EndDialog(-1);	
}


bool AllocateCcaParameter::GetAllocTimeFrame() 
{
	if(!m_CE_Date_From.GetStatus() || !m_CE_Date_To.GetStatus() || !m_CE_Time_From.GetStatus() || !m_CE_Time_To.GetStatus())
	{
		MessageBox( GetString(IDS_STRING2649) , GetString(ST_FEHLER), MB_ICONERROR | MB_OK);
		return false;		
	}

	omFrom = CTime(1990,1,1,1,1,1);
	omTo = CTime(2037,1,1,1,1,1);

	CString olDateFrom;
	CString olDateTo;
	CString olTimeFrom;
	CString olTimeTo;

	m_CE_Date_From.GetWindowText(olDateFrom);
	m_CE_Time_From.GetWindowText(olTimeFrom);
	m_CE_Date_To.GetWindowText(olDateTo);
	m_CE_Time_To.GetWindowText(olTimeTo);

	if(!olDateFrom.IsEmpty() && !olTimeFrom.IsEmpty())
	{
		omFrom = DateHourStringToDate(olDateFrom, olTimeFrom);
		if (bgGatPosLocal)
			ogBasicData.LocalToUtc(omFrom);
	}

	if(!olDateTo.IsEmpty() && !olTimeTo.IsEmpty())
	{
		omTo = DateHourStringToDate(olDateTo, olTimeTo);
		if (bgGatPosLocal)
			ogBasicData.LocalToUtc(omTo);
	}
	return true;

}







