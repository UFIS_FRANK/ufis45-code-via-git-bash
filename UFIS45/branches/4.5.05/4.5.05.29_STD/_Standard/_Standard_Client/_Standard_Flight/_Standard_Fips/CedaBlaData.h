// CedaBlaData.h

#ifndef __CEDABLADATA__
#define __CEDABLADATA__
 
#include "stdafx.h"
#include "basicdata.h"

//---------------------------------------------------------------------------------------------------------
// Record structure declaration

//@See: CedaBlaData
//@Doc:	A structure for reading Duty data. We read all data from database and put them
//		into this struct and store the data in omData.  

//---------------------------------------------------------------------------------------------------------
struct BLADATA 
{
	char 	 Ftyp[4]; 	// Flight ID
	CTime	 Cdat; 		// Erstellungsdatum
	CTime	 Lstu; 		// Datum letzte �nderung
	char 	 Agrp[42];	// Grund der Sperrung
	char 	 Ogrp[42];	// Grund der Sperrung
	int      Time;      // Allocation Time in minutes
	long 	 Urno; 		// Eindeutige Datensatz-Nr.
	char 	 Usec[34]; 	// Anwender (Ersteller)
	char 	 Useu[34]; 	// Anwender (letzte �nderung)
//	CTime	 Vafr; 		// G�ltig von
//	CTime	 Vato; 		// G�ltig bis

	//DataCreated by this class
	int      IsChanged;

	BLADATA(void)
	{ memset(this,'\0',sizeof(*this));
	 Cdat=-1;Lstu=-1;//Vafr=-1;Vato=-1;
	}

}; // end BlaDataStrukt


//---------------------------------------------------------------------------------------------------------
// Class declaratino

//@Memo: Read and write Bla(duty) data
//@See: CedaData, CedaDutyData
/*@Doc:
  Reads and writes Bla(duty) data from and to database. Stores Bla data in memory and
  provides methods for searching and retrieving Blas from its internal list. Does
  some data preparations.

  Data of this class has to be updated by CEDA through broadcasts. This procedure and the
  necessary methods will be described later with the description of broadcast handling.

  {\bf CedaBlaData} handles data from the server process. 
  See the specification for table descriptions.*/

class CedaBlaData: public CCSCedaData
{
// Attributes
public:
    //@ManMemo: A map, containing the Urno field of all loaded Blas.
    CMapPtrToPtr omUrnoMap;
//	CMapStringToPtr omBnamMap;

    //@ManMemo: BLADATA records read by ReadAllBla().
    CCSPtrArray<BLADATA> omData;

	void SetTableName(CString opTableName)
	{
		strcpy(pcmTableName, opTableName.GetBuffer(0));
	}
// Operations
public:
    //@ManMemo: Default constructor
    /*@Doc:
      Initialize {\bf omRecInfo} for offsets and types for each fields in {\bf BLADATA},
      the {\bf pcmTableName} with table name {\bf BlaLSG}, the data members {\bf pcmFieldList}
      contains a list of used fields of {\bf BLADATA}.
    */
    CedaBlaData();
	~CedaBlaData();
	void Register(void);
	void ClearAll(void);

    //@ManMemo: Read all Blas.
    /*@Doc:
      Read all Blas. 

      This method will clear {\bf omData} and {\bf omKeyMap}. Convert each record
      to a BLADATA and store it in {\bf omData} and the Urno field in {\bf omKeyMap}.
    */
    bool ReadAllBlas();
    //@AreMemo: Read all Bla-Data corresponding to Where-condition
	bool ReadSpecial(CCSPtrArray<BLADATA> &ropBlt,char *pspWhere);
    //@ManMemo: Add a new Bla and store the new data to the Database.
	bool InsertBla(BLADATA *prpBla,BOOL bpSendDdx = TRUE);
    //@ManMemo: Add data for a new Bla to the internal data.
	bool InsertBlaInternal(BLADATA *prpBla);
	//@ManMemo: Change the data of a special Bla
	bool UpdateBla(BLADATA *prpBla,BOOL bpSendDdx = TRUE);
    //@AreMemo: Change the data of a special Bla in the internal data.
	bool UpdateBlaInternal(BLADATA *prpBla);
    //@ManMemo: Delete a special Bla, also in the Database.
	bool DeleteBla(long lpUrno);
    //@ManMemo: Delete a special Bla only in the internal data.
	bool DeleteBlaInternal(BLADATA *prpBla);
    //@AreMemo: Selects all Blas with a special Urno.
	BLADATA  *GetBlaByUrno(long lpUrno);
    //@ManMemo: Search a Bla with a special Key.
//	CString BlaExists(BLADATA *prpBla);
    //@AreMemo: Search a Bnam
//	bool ExistBnam(char* popBnam);
	//@ManMemo: Writes a special Bla to the Database.
	bool SaveBla(BLADATA *prpBla);
    //@ManMemo: Contains the valid data for all existing Blas.
	char pcmBlaFieldList[2048];
    //@ManMemo: Handle Broadcasts for Blas.
	void ProcessBlaBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
//	long GetUrnoByBlt(char *pspPst);
//	BLADATA *GetBltByBnam(char *pspBnam);


	// Private methods
private:
    void PrepareBlaData(BLADATA *prpBlaData);
};


extern CedaBlaData ogBlaData;

//---------------------------------------------------------------------------------------------------------
#endif //__CEDABLADATA__
