#ifndef AFX_ROTGROUNDDLG_H__6858C611_6588_11D1_80FD_0000B43C4B01__INCLUDED_
#define AFX_ROTGROUNDDLG_H__6858C611_6588_11D1_80FD_0000B43C4B01__INCLUDED_

// RotGroundDlg.h : Header-Datei
//

#include <CCSTable.h> 
#include <CCSPtrArray.h> 
#include <CCSEdit.h> 
#include <RotGDlgCedaFlightData.h> 
#include <DlgResizeHelper.h>

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld RotGroundDlg 

class RotGroundDlg : public CDialog
{
public:
	//Dialogstatus
	enum Status 
	{
		UNKNOWN		,
		POSTFLIGHT
	};

// Konstruktion
public:
	RotGroundDlg(CWnd* pParent = NULL);   // Standardkonstruktor
	~RotGroundDlg();

// Dialogfelddaten
	//{{AFX_DATA(RotGroundDlg)
	enum { IDD = IDD_ROTGROUND };
	CCSButtonCtrl	m_CB_OK;
	CCSButtonCtrl	m_CB_Delete;
	CEdit	m_CE_Regn;
	CEdit	m_CE_Act5;
	CEdit	m_CE_Act3;
	CEdit	m_CE_DPstd;
	CEdit	m_CE_AFlno;
	CStatic	m_CS_GMBorder;
	CEdit	m_CE_AEtai;
	CEdit	m_CE_APsta;
	CEdit	m_CE_DFlno;
	CEdit	m_CE_DStod;
	CEdit	m_CE_DOfbl;
	CEdit	m_CE_DEtdi;
	CEdit	m_CE_AStoa;
	CEdit	m_CE_AOnbl;
	CString	m_AFlno;
	CString	m_AOnbl;
	CString	m_AStoa;
	CString	m_DEtdi;
	CString	m_DOfbl;
	CString	m_DPstd;
	CString	m_DStod;
	CString	m_DFlno;
	CString	m_APsta;
	CString	m_AEtai;
	CEdit	m_CE_ADura;
	CEdit	m_CE_DDura;
	CCSButtonCtrl	m_CB_ReaOverview; 
	CCSButtonCtrl	m_CB_Reasons;     

	//}}AFX_DATA

	bool bmIPEdit;
	CCSTable *pomTable;


	void NewData(CWnd* pParent, long lpRkey, long lpUrno, char cpFPart, bool bpLocalTime, bool bpAuto = false, bool bpOk = true);

	void InitTable();
	void InitDialog( CCSPtrArray<ROTGDLGFLIGHTDATA> *prpRotation);
	void ClearAll();

	void InitialPostFlight(BOOL);

	void FillTableLine(int ipLineNo, ROTGDLGFLIGHTDATA *prpFlight);

	bool CheckAll(int &ipFirstLine, int &ipFirstColumn);
	bool SaveAll();
	bool GetAllData();

	void setStatus(Status epStatus = UNKNOWN);
	Status getStatus() const;


	CCSPtrArray<ROTGDLGFLIGHTDATA> omMovements;
	CCSPtrArray<ROTGDLGFLIGHTDATA> omMovementSave;

	ROTGDLGFLIGHTDATA *prmAFlight;
	ROTGDLGFLIGHTDATA *prmDFlight;

	void ProcessFlightChange(ROTGDLGFLIGHTDATA *prpFlight);
	void SaveToReg();

	long lmCallByUrno;
	char cmCallFPart;

	long lmCallByRkey;

	CWnd *pomParent;


	CString omAct3;
	CString omAct5;
	CString omRegn;

	CTime omRefDat;

	CString omFirstReason;
	bool bmReasonRequired;

// Überschreibungen
	// Vom Klassen-Assistenten generierte virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(RotGroundDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	//virtual BOOL DestroyWindow();
	//}}AFX_VIRTUAL

public:

	RotGDlgCedaFlightData omRotGDlgFlights;

private:
	Status emStatus;
//	void InitialPostFlight(BOOL);
	void InitialStatus();
	BOOL CheckPostFlight();
	void SetSecState();
	bool VerifyRotation();
	void VerifyMovement(ROTGDLGFLIGHTDATA *prpMovement, CTime& opTifd, CTime& opTifa, CString& opDepTimeError, CString& opArrTimeError);

	static int CompareRotationFlight(const ROTGDLGFLIGHTDATA **e1, const ROTGDLGFLIGHTDATA **e2);

	DlgResizeHelper m_resizeHelper;
	CString m_key; 

	CMapWordToPtr omMapWordToPtr;
	int imIdxFtyp;
	int imIdxRtow;
	int imIdxPstd;
	int imIdxStdDate;
	int imIdxStdTime;
	int imIdxOfbl;
	int imIdxPsta;
	int imIdxStaTime;
	int imIdxOnbl;
	int imIdxRemk;
	int imIdxLstu;
	int imIdxUseu;
	int imIdxCdat;
	int imIdxUsec;

// Implementierung
protected:

	bool bmLocalTime;

	bool InsertTableHeadline();
	int GetFirstFreeLine();
	bool GetLastPos(CString &ropLastPos);

	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(RotGroundDlg)
	virtual BOOL OnInitDialog();
	virtual void OnCancel();
	virtual void OnOK();
	afx_msg LONG OnTableIPEditKillfocus( UINT wParam, LPARAM lParam);
    afx_msg LONG OnTableLButtonDown(UINT wParam, LONG lParam);
	afx_msg void OnDelete();
	afx_msg void OnNewTowing();
	afx_msg void OnFlight();
	afx_msg void OnSize(UINT nType, int cx, int cy); 
	afx_msg void OnDestroy();
	afx_msg void OnReasonsOverview();		
	afx_msg void OnReasonsList();		
    afx_msg LONG OnTableReturnPressed(UINT wParam, LONG lParam);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio fügt zusätzliche Deklarationen unmittelbar vor der vorhergehenden Zeile ein.

#endif // AFX_ROTGROUNDDLG_H__6858C611_6588_11D1_80FD_0000B43C4B01__INCLUDED_
