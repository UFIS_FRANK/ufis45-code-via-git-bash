#ifndef AFX_REPORTSELECTDLG_H__8BBC00F2_8B6C_11D1_8126_0000B43C4B01__INCLUDED_
#define AFX_REPORTSELECTDLG_H__8BBC00F2_8B6C_11D1_8126_0000B43C4B01__INCLUDED_

// ReportSelectDlg.h : Header-Datei
//

#include <ReportTableDlg.h>
#include <SeasonCedaFlightData.h>
#include <DlgResizeHelper.h>


// 2/99 hbe : Portionsweises Einlesen der Flugdaten ( RotationDlgCedaFlightData )  

// Zeitr�ume f�r partial read
enum READRANGE { RRDefault=7, RRDay=1, RRWeek=7, RRFortnight=14, RRMonth=28 } ;	

struct PARTIALREAD
{
	CString WhereClause ;	// gesamte WHERE-BEDINGUNG
	CTime StartTime ;		// Tag von
	CTime EndTime ;			// Tag bis
	int Range	;			// entspr. READRANGE
	int Case ;				// Reportkennung ( case 5-7 = Messestatistik / case 14 = POPS Season )

	void *omData ;			// Flugdaten
	CString Fields ;		// Feldliste
	bool onlyAFT ;			// AKtuelle oder mit Archive
	bool Sort ;				// Sortierung ?
	int	SortArt ;			// welche Sortierung
	
	PARTIALREAD() ;
	void Preset() ;		// Vorbelegung f�r Hauptanwendung ( alle POPS case 14 ! )
} ;


enum { X_TERMDEF=0, X_TERMNO=0, X_TERM1, X_TERM2, X_TERM12 } ;		// Kennung Terminal CheckBox (X) 


/////////////////////////////////////////////////////////////////////////////
// Vorbelegungswerte f�r wiederholten Dlg-Aufruf

class CLastDlgValues
{

private:

	// Zeitraum
	CString Date ;
	CString MinDate ;
	CString MaxDate ;
	CTime Time ;
	CTime MinTime ;
	CTime MaxTime ;
	CTime MinTimeRange ;
	CTime MaxTimeRange ;

	CString Airlines ;
	CString GpuAirline ;
	CString Regen ;
	CString AcType ;
	CString Nature ;
	CString Origin ;
	CString Destin ;

	CString DelayAirline ;
	CString DelayCode ;
	CString DelayMinutes ;

	int Movement ;
	int LoadPaxType ;
	int IATA_ICAO ;

/*	// Daily OPS-Plan
	bool Season ;	
	bool SelAirl ;	
	int Term ;
	CString Airlines ;
	CString AirlineEx ;
	CString Verkehr ;
	CString VerkehrTag ;

	CString Select ;	// Airlinecodes
	CString Saison ;	// Saisonkennung ( case 8 )	
	CString Messe ;		// freier Text ( case 4 )	
	CString Flno ;		// Flugnummer ( case 2 )	
	CString Minuten ;	// Flugnummer ( case 3 + 6 )	
	int Radio ;			// Buttons ( case 11 )
*/

public:
	// Konstruktor
	CLastDlgValues() ;
	// Destruktor dient als Reset !
	~CLastDlgValues() ;	

	void SetPreset(int ipTable, CTime & pStart, CTime & pEnd, CString & pSelect);
	
	CString GetDate() { return Date ; } ;
	CString GetMinDate() { return MinDate ; } ;
	CString GetMaxDate() { return MaxDate ; } ;
	CTime GetTime() { return Time ; } ;
	CTime GetMinTime() { return MinTime ; } ;
	CTime GetMaxTime() { return MaxTime ; } ;
	CTime GetMinTimeRange() { return MinTimeRange ; } ;
	CTime GetMaxTimeRange() { return MaxTimeRange ; } ;

	CString GetAirlines() { return Airlines ; } ;
	CString GetGpuAirline () { return GpuAirline ; } ;
	CString GetRegen() { return Regen ; } ;
	CString GetAcType() { return AcType ; } ;
	CString GetNature() { return Nature ; } ;
	CString GetOrigin() { return Origin ; } ;
	CString GetDestin() { return Destin ; } ;
	CString GetDelayAirline () { return DelayAirline ; } ;
	CString GetDelayCode () { return DelayCode ; } ;
	CString GetDelayMinutes () { return DelayMinutes ; } ;

	int GetMovement() { return Movement ; } ;
	int GetLoadPaxType() { return LoadPaxType ; } ;
	int GetIATA_ICAO() { return IATA_ICAO ; } ;


/*	CString GetAirlineEx() { return AirlineEx ; } ;
	CString GetVerkehr() { return Verkehr ; } ;
	CString GetSelect() { return Select ; } ;
	CString GetSaison() { return Saison ; } ;
	CString GetMesse() { return Messe ; } ;
	CString GetFlno() { return Flno ; } ;
	CString GetMinuten() { return Minuten ; } ;
	bool GetSeason() { return Season ; } ;
	int GetTerm() { return Term ; } ;
	int GetRadio() { return Radio ; } ;
	CString GetVerkehrTag() { return VerkehrTag ; } ;
	bool GetSelAirl() { return SelAirl ; } ;
*/
	void SetDate( CString opMin = "" ) { Date = opMin ; } ;
	void SetMinDate( CString opMin = "" ) { MinDate = opMin ; } ;
	void SetMaxDate(CString opMax = "" ) {  MaxDate = opMax ; } ;
	void SetTime( CTime opMin = TIMENULL ) { Time = opMin ; } ;
	void SetMinTime( CTime opMin = TIMENULL ) { MinTime = opMin ; } ;
	void SetMaxTime(CTime opMax = TIMENULL ) {  MaxTime = opMax ; } ;
	void SetMinTimeRange( CTime opMin = TIMENULL ) { MinTimeRange = opMin ; } ;
	void SetMaxTimeRange( CTime opMax = TIMENULL ) { MaxTimeRange = opMax ; } ;

	void SetAirlines( CString opStr = "") { Airlines = opStr ; } ;
	void SetGpuAirline( CString opStr = "") { GpuAirline = opStr ; } ;
	void SetRegen( CString opStr = "") { Regen = opStr ; } ;
	void SetAcType( CString opStr = "") { AcType = opStr ; } ;
	void SetNature( CString opStr = "") { Nature = opStr ; } ;
	void SetOrigin( CString opStr = "") { Origin = opStr ; } ;
	void SetDestin( CString opStr = "") { Destin = opStr ; } ;
	void SetDelayAirline( CString opStr = "") { DelayAirline = opStr ; } ;
	void SetDelayCode( CString opStr = "") { DelayCode = opStr ; } ;
	void SetDelayMinutes( CString opStr = "") { DelayMinutes = opStr ; } ;

	void SetMovement( int ipT = 1 ) { Movement = ipT ; } ;
	void SetLoadPaxType( int ipT = 1 ) { LoadPaxType = ipT ; } ;
	void SetIATA_ICAO( int ipT = 1 ) { IATA_ICAO = ipT ; } ;



/*	void SetAirlineEx( CString opStr = "") { AirlineEx = opStr ; } ;
	void SetVerkehr( CString opStr = "" ) { Verkehr = opStr ; } ;
	void SetSelect( CString opStr = "" ) { Select = opStr ; } ;
	void SetSaison( CString opStr = "" ) { Saison = opStr ; } ;
	void SetSeason( bool bpS = false ) { Season = bpS ; } ;
	void SetTerm( int ipT = X_TERMDEF ) { Term = ipT ; } ;
	void SetRadio( int ipT = 1 ) { Radio = ipT ; } ;
	void SetMesse( CString opStr = "" ) { Messe = opStr ; } ;
	void SetFlno( CString opStr = "" ) { Flno = opStr ; } ;
	void SetMinuten( CString opStr = "180" ) { Minuten = opStr ; } ;
	void SetVerkehrTag( CString opStr = "1" ) { VerkehrTag = opStr ; } ;
	void SetSelAirl( bool bpS = true ) { SelAirl = bpS ; } ;
*/
} ;



/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CReportSelectDlg 

class CReportSelectDlg : public CDialog
{
// Konstruktion
public:

	CReportSelectDlg(CWnd* pParent = NULL);   // Standardkonstruktor
	~CReportSelectDlg();
	void SaveToReg();

	// nur f�r Season
	// bool ReadPartialWhere( const char *pspWhere, UINT ipRange = RRDefault );
	// f�r Season + Statistiken
	bool ReadPartialWhere( PARTIALREAD *prpPartial, CProgressCtrl *prpProgress = NULL );

	CCSPtrArray<ROTATIONDLGFLIGHTDATA> prmFlight;

	class CLastDlgValues omLastValues ;		// Presetwerte f�r Dlg

// Dialogfelddaten
	//{{AFX_DATA(CReportSelectDlg)
	enum { IDD = IDD_REPORTSELECT };
	CButton	m_CE_UtcToLocal;
	CButton	m_ST_Progress;
	CProgressCtrl	m_PR_Read;
	CButton	m_CR_List1;
	CButton	m_CR_List2;
	CButton	m_CR_List3;
	CButton	m_CR_List4;
	CButton	m_CR_List5;
	CButton	m_CR_List6;
	CButton	m_CR_List7;
	CButton	m_CR_List8;
	CButton	m_CR_List9;
	CButton	m_CR_List10;
	CButton	m_CR_List11;
	CButton	m_CR_List12;
	CButton	m_CR_List13;
	CButton	m_CR_List14;
	CButton	m_CR_List15;
	CButton	m_CR_List16;
	CButton	m_CR_List18;
	CButton	m_CR_List19;
	CButton	m_CR_List20;
	CButton	m_CR_List21;
	CButton	m_CR_List22;
	CButton	m_CR_List23;
	CButton	m_CR_List24;
	CButton	m_CR_List25;
	CButton	m_CR_List26;
	CButton	m_CR_List27;
	CButton m_CR_List28;
	int		m_List1;
	//}}AFX_DATA


// �berschreibungen
	// Vom Klassen-Assistenten generierte virtuelle Funktions�berschreibungen
	//{{AFX_VIRTUAL(CReportSelectDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterst�tzung
	//}}AFX_VIRTUAL

	int imArrDep;	// 0 = Arr; 1= Dep

// Implementierung
	void MakeUtc( CString& ropMin, CString& ropMax ) ;
	void ShowStatus() ;
	void HideStatus() ;


protected:

	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(CReportSelectDlg)
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	virtual void OnCancel();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnDestroy();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
	CViewer *pomCurrentViewer;


private:
	CReportTableDlg *pomReportTableDlg;
	CString omDefaultWhereSto ;		// Default Where-Clause �ber Standard Time of ...
	CString omDefaultWhereTif ;		// Default Where-Clause �ber Timeframe ...

	DlgResizeHelper m_resizeHelper;
	CString m_key;

	bool StartTableDlg();
};

//---------------------------------------------------------------------------------------------------------

extern SeasonCedaFlightData omCedaFlightData;
extern RotationDlgCedaFlightData omDailyCedaFlightData;	// f�r Daily OPS-Plan


#endif // AFX_REPORTSELECTDLG_H__8BBC00F2_8B6C_11D1_8126_0000B43C4B01__INCLUDED_
