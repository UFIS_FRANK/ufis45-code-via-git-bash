
// CPP-FILE 

#include <stdafx.h>
#include <afxwin.h>
#include <CCSGlobl.h>
#include <CCSCedaData.h>
#include <ccsddx.h>
#include <CedaFlzData.h>
#include <CedaCcaData.h>
#include <BasicData.h>
#include <CedaBasicData.h>


CedaFlzData ogFlzData;

CedaFlzData::CedaFlzData()
{
	// Create an array of CEDARECINFO for FLZDATA
	BEGIN_CEDARECINFO(FLZDATA,FlzDataRecInfo)
		CCS_FIELD_LONG(Urno,"URNO", "Eindeutige Datensatz-Nr.", 1)
		CCS_FIELD_LONG(Flgu,"FLGU", "FLGTAB URNO Relation", 1)
		CCS_FIELD_LONG(Rurn,"RURN", "", 1)
		CCS_FIELD_CHAR_TRIM(Utyp,"UTYP","", 1)
		CCS_FIELD_CHAR_TRIM(Lnam,"LNAM","", 1)
		CCS_FIELD_CHAR_TRIM(Lseq,"LSEQ","", 1)
	END_CEDARECINFO //(FLZDATA)

	// Copy the record structure
	for (int i=0; i< sizeof(FlzDataRecInfo)/sizeof(FlzDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&FlzDataRecInfo[i],sizeof(CEDARECINFO));
		omRecInfo.Add(prpCedaRecInfo);
	} // end for

	// initialize field names
	strcpy(pcmTableName,"FLZ");
	strcpy(pcmFList,"URNO,FLGU,RURN,UTYP,LNAM,LSEQ");
	pcmFieldList = pcmFList;


}; // end Constructor


static void ProcessFlzCf(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);


CedaFlzData::~CedaFlzData()
{
	omRecInfo.DeleteAll();
	omData.DeleteAll();
}



void CedaFlzData::Register(void)
{

	ogDdx.Register((void *)this,BC_FLZ_CHANGE,	CString("FLZDATA"), CString("Flz-changed"),	ProcessFlzCf);
	ogDdx.Register((void *)this,BC_FLZ_NEW,		CString("FLZDATA"), CString("Flz-new"),		ProcessFlzCf);
	ogDdx.Register((void *)this,BC_FLZ_DELETE,	CString("FLZDATA"), CString("Flz-deleted"),	ProcessFlzCf);
}

void CedaFlzData::UnRegister(void)
{
	ogDdx.UnRegister(this,NOTUSED);
}



void CedaFlzData::Clear()
{
	omUrnoMap.RemoveAll();
	omData.DeleteAll();
	omRurns = "";
}

void CedaFlzData::Read( CString  opRurnList, bool bpAdd)
{
	if(bpAdd)
	{
		omRurns += CString(",") + opRurnList;
	}
	else
	{
		Clear();
		omRurns = opRurnList;
	}

	char pclSel[10000];
	CStringArray olUrnoLists;

	CString olSelection;

	for(int i = SplitItemList(opRurnList, &olUrnoLists, 100) - 1; i >= 0; i--)
	{
		olSelection = CString("WHERE RURN IN (");
		olSelection += olUrnoLists[i] + CString(")");

		strcpy(pclSel, olSelection );

		if (!opRurnList.IsEmpty())
		{

			if (CedaAction("RT", pclSel) == false)
			{
				return;
			}

			bool ilRc = true;

			for (int ilLc = 0; ilRc == true; ilLc++)
			{
				FLZDATA *prlFlz = new FLZDATA;
				if ((ilRc = GetBufferRecord(ilLc,prlFlz)) == true)
				{
					InsertInternal(prlFlz);
				}
				else
				{
					delete prlFlz;
				}
			}

		}
	}



}



FLZDATA *CedaFlzData::GetFlzByUrno(  long lpUrno)
{

	int ilCount = omData.GetSize();

	FLZDATA *prlFlz;	
	for (int i = 0; i < ilCount; i++)
	{
		prlFlz = &omData[i];
		if ( omData[i].Urno == lpUrno)
		{
			return &omData[i];
		}
	}

	return NULL;
}


CString CedaFlzData::GetGatFlgu(CString opLSeq)
{
	CString opRet("") ;
	char buffer[12];	

	if(opLSeq.IsEmpty())
		return opRet;

	int ilCount = omData.GetSize();

	FLZDATA *prlFlz;	
	for (int i = 0; i < ilCount; i++)
	{
		prlFlz = &omData[i];
		if ( strcmp(omData[i].Utyp, "G") ==  0 && strcmp(omData[i].Lseq, opLSeq) ==  0 )
		{
			ltoa(omData[i].Flgu, buffer, 10);

			opRet = CString(buffer);	
			
			break;
		}
	}

	return opRet;

}


CString CedaFlzData::GetCicLogoName(long lpCcaUrno)
{
	CString opRet("");

	if(lpCcaUrno <= 0)
		return opRet;	

	char buffer[12];	

	int ilCount = omData.GetSize();

	FLZDATA *prlFlz;	
	for (int i = 0; i < ilCount; i++)
	{
		prlFlz = &omData[i];
		if ( strcmp(omData[i].Utyp, "C") ==  0 && omData[i].Rurn == lpCcaUrno )
		{
			ltoa(omData[i].Flgu, buffer, 10);

			ogBCD.GetField("FLG_CKI", "URNO", CString(buffer), "LGSN", opRet );
			
			break;
		}
	}
	return opRet;

}






bool CedaFlzData::ReadFlnus(const CString &ropFlnus) {
	CString olSelection;
	CStringArray olUrnosLists;
	bool blRc;
	bool blRet = false;

	Clear();

	// Split urnolist in 100 pieces blocks
	for(int i = SplitItemList(ropFlnus, &olUrnosLists, 100) - 1; i >= 0; i--)
	{
		olSelection.Format("WHERE FLNU IN (%s)", olUrnosLists[i]);
		// call CEDA
		blRet = CedaAction("RT", olSelection.GetBuffer(olSelection.GetLength()));
 		blRc = blRet;
		// Read in internal buffer
		while (blRc == true)
		{
			FLZDATA *prlFlz = new FLZDATA;
			if ((blRc = GetFirstBufferRecord(prlFlz)) == true)
			{
				if(UpdateInternal(prlFlz))
				{
					delete prlFlz;
				}
				else
				{
					InsertInternal(prlFlz);
				}
			}
			else 
			{
				delete prlFlz;
			}
		}

	} 
	TRACE("CedaFlzData::ReadFlnus: Result: %ld records read!\n", omData.GetSize());

    return blRet;
}






void CedaFlzData::InsertInternal(FLZDATA *prpFlz)
{
	omData.Add(prpFlz);
	omUrnoMap.SetAt((void *)prpFlz->Urno,prpFlz);

}




///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////


bool CedaFlzData::UpdateInternal(FLZDATA *prpFlz)
{
	int ilCount = omData.GetSize();
	for (int i = 0; i < ilCount; i++)
	{
		if (omData[i].Urno == prpFlz->Urno)
		{
			omData[i] = *prpFlz;
			return true;
		}
	}
	return false;
}






void CedaFlzData::DeleteInternal(FLZDATA *prpFlz)
{
	omUrnoMap.RemoveKey((void *)prpFlz->Urno);
	int ilCount = omData.GetSize();
	for (int i = 0; i < ilCount; i++)
	{
		if (omData[i].Urno == prpFlz->Urno)
		{
			omData.DeleteAt(i);//Update omData
			break;
		}
	}
}


bool CedaFlzData::Save(FLZDATA *prpFlz)
{
	bool olRc = true;
	CString olListOfData;
	char pclSelection[124];
	char pclData[524];

	if (prpFlz->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}

	switch(prpFlz->IsChanged)   // New job, insert into database
	{
	case DATA_NEW:
		prpFlz->Urno = ogBasicData.GetNextUrno();
		MakeCedaData(&omRecInfo,olListOfData,prpFlz);
		strcpy(pclData,olListOfData);
		olRc = CedaAction("IRT","","",pclData);
		prpFlz->IsChanged = DATA_UNCHANGED;

		InsertInternal(prpFlz);
		break;
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpFlz->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpFlz);
		strcpy(pclData,olListOfData);
		olRc = CedaAction("URT",pclSelection,"",pclData);
		prpFlz->IsChanged = DATA_UNCHANGED;
		UpdateInternal(prpFlz);
		break;
	case DATA_DELETED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpFlz->Urno);
		olRc = CedaAction("DRT",pclSelection);
		DeleteInternal(prpFlz);
		break;
	}

	return true;
}



bool CedaFlzData::SaveGatLogo(long lpFlightUrno, CString opGatFlgu, CString opGatFlgu2)
{

	CString opRet ;
	//char buffer[12];	
	
	int ilCount = omData.GetSize();

	bool blGatLogo1 = false;
	bool blGatLogo2 = false;

	FLZDATA *prlFlz = NULL;	

	for (int i = ilCount -1 ; i >= 0; i--)
	{
		prlFlz = &omData[i];
		if ( strcmp(omData[i].Utyp, "G") ==  0)
		{

			if( strcmp(prlFlz->Lseq,"1") ==  0 )
			{

				if(prlFlz->Flgu == atol(opGatFlgu) && atol( opGatFlgu) != 0)
				{
					blGatLogo1 = true;
				}
				else
				{
					if(atol( opGatFlgu) == 0)
					{
						prlFlz->IsChanged = DATA_DELETED;
						Save(prlFlz);
						blGatLogo1 = true;
					}
					else
					{
						prlFlz->Flgu = atol(opGatFlgu);
						prlFlz->Rurn = lpFlightUrno;
						strcpy(prlFlz->Utyp, "G");
						strcpy(prlFlz->Lseq, "1");
						prlFlz->IsChanged = DATA_CHANGED;
						Save(prlFlz);
						blGatLogo1 = true;
					}
				}
			}
			if( strcmp(prlFlz->Lseq,"2") ==  0 )
			{

				if(prlFlz->Flgu == atol(opGatFlgu2) && atol( opGatFlgu2) != 0)
				{
					blGatLogo2 = true;
				}
				else
				{
					if(atol( opGatFlgu2) == 0)
					{
						prlFlz->IsChanged = DATA_DELETED;
						Save(prlFlz);
						blGatLogo2 = true;
					}
					else
					{
						prlFlz->Flgu = atol(opGatFlgu2);
						prlFlz->Rurn = lpFlightUrno;
						strcpy(prlFlz->Lseq, "2");
						strcpy(prlFlz->Utyp, "G");
						prlFlz->IsChanged = DATA_CHANGED;
						Save(prlFlz);
						blGatLogo2 = true;
					}
				}
			}

		}
	}



	if(!blGatLogo1 && atol( opGatFlgu) != 0 )
	{
		prlFlz = new FLZDATA;
		prlFlz->Flgu = atol(opGatFlgu);
		prlFlz->Rurn = lpFlightUrno;
		strcpy(prlFlz->Utyp, "G");
		strcpy(prlFlz->Lseq, "1");
		prlFlz->IsChanged = DATA_NEW;
		prlFlz->IsUsed = true;
		blGatLogo1 = true;
		Save(prlFlz);
	}

	if(!blGatLogo2 && atol( opGatFlgu2) != 0)
	{
		prlFlz = new FLZDATA;
		prlFlz->Flgu = atol(opGatFlgu2);
		prlFlz->Rurn = lpFlightUrno;
		strcpy(prlFlz->Lseq, "2");
		strcpy(prlFlz->Utyp, "G");
		prlFlz->IsChanged = DATA_NEW;
		prlFlz->IsUsed = true;
		blGatLogo2 = true;
		Save(prlFlz);
	}


	return true;

}


bool CedaFlzData::SaveCicLogo(CCSPtrArray<CCADATA> &ropCca, CStringArray &omCicLogoArray)
{

	int ilCount = omData.GetSize();
	CString olFlgUrno;
	bool blFound;

	CCADATA *prlCca;

	FLZDATA *prlFlz = NULL;	
	for (int i = 0; i < ilCount; i++)
	{
		omData[i].IsUsed = false;
	}


	for (int j = 0; j < ropCca.GetSize(); j++)
	{
		prlCca = &ropCca[j];
		blFound = false;

		olFlgUrno = "";
		ogBCD.GetField("FLG_CKI", "LGSN", omCicLogoArray[j], "URNO", olFlgUrno );

		if( !CString(prlCca->Ckic).IsEmpty() )
		{
			for (i = ilCount - 1; i >= 0; i--)
			{
				prlFlz = &omData[i];

				if(prlFlz->Rurn == prlCca->Urno)
				{
					
					
					if( atol( olFlgUrno) != prlFlz->Flgu  && atol( olFlgUrno) != 0)
					{

						omData[i].IsUsed = true;
						prlFlz->Flgu = atol(olFlgUrno);
						strcpy(prlFlz->Lnam, prlCca->Ckic);
						prlFlz->IsChanged = DATA_CHANGED;	
						Save(prlFlz);
						blFound = true;
						break;
					}

					if( atol( olFlgUrno) == prlFlz->Flgu  && atol( olFlgUrno) != 0)
					{

						omData[i].IsUsed = true;
						blFound = true;
						break;
					}
				
				}
			}
			if(!blFound)
			{
				if(atol( olFlgUrno) != 0)
				{
					prlFlz = new FLZDATA;
					prlFlz->Flgu = atol(olFlgUrno);
					strcpy(prlFlz->Lnam, prlCca->Ckic);
					prlFlz->Rurn = prlCca->Urno;
					strcpy(prlFlz->Utyp, "C");
					prlFlz->IsChanged = DATA_NEW;
					Save(prlFlz);
				}
			}
		}
	}







	for (i = ilCount - 1; i >= 0; i--)
	{
		prlFlz = &omData[i];
		if ( !prlFlz->IsUsed &&  strcmp(prlFlz->Utyp, "C") ==  0 )
		{
			prlFlz->IsChanged = DATA_DELETED;	
			Save(prlFlz);
		}
	}
	
	return true;

}



static void  ProcessFlzCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
    CedaFlzData *polViewer = (CedaFlzData *)vpInstance;

	polViewer->ProcessBc(ipDDXType,vpDataPointer,ropInstanceName);		
}




//--PROCESS-BC---------------------------------------------------------------------------------------------

void  CedaFlzData::ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	if (vpDataPointer == NULL)
		return;

	struct BcStruct *prlFLZDATA;			//if(prlFlz->Ctyp == "C")
	prlFLZDATA = (struct BcStruct *) vpDataPointer;
	CString olBcFields(prlFLZDATA->Fields);
	CString olBcData(prlFLZDATA->Data);

	FLZDATA *prlFlz;
	

	if(ipDDXType == BC_FLZ_NEW)
	{
		prlFlz = new FLZDATA;
		GetRecordFromItemList(prlFlz,prlFLZDATA->Fields,prlFLZDATA->Data);
		if(prlFlz->Urno > 0)
		{
			if(IsPassFilter(prlFlz))
			{
				InsertInternal(prlFlz);
				return;
			}
		}
		delete prlFlz;
	}

	if(ipDDXType == BC_FLZ_CHANGE)
	{
		CString olUrno;
		long llUrno;
		if (GetListItemByField(olBcData, olBcFields, CString("URNO"), olUrno))
		{
			llUrno = atol(olUrno);
		}
		else
			return;

		FLZDATA *prlFlzOld = GetFlzByUrno(llUrno);
		if(prlFlzOld != NULL)
		{
			FillRecord((void *)prlFlzOld, CString(prlFLZDATA->Fields), CString(prlFLZDATA->Data)) ;
		}
		return;

	}
	if(ipDDXType == BC_FLZ_DELETE)
	{
		CString olUrno;
		long llUrno;

		CString olSelection = CString(prlFLZDATA->Selection);
		if (olSelection.Find('\'') != -1)
		{
			llUrno = GetUrnoFromSelection(prlFLZDATA->Selection);
		}
		else
		{
			int ilFirst = olSelection.Find("=")+1;
			int ilLast  = olSelection.GetLength();
			llUrno = atol(olSelection.Mid(ilFirst,ilLast-ilFirst));
		}
		

		prlFlz = GetFlzByUrno(llUrno);
		if(prlFlz != NULL)
		{
			DeleteInternal(prlFlz);
		}
		return;
	}

	
}


bool  CedaFlzData::IsPassFilter(FLZDATA *prlFlz)
{
	bool blRet = false;
	char buffer[64];

	ltoa(prlFlz->Rurn, buffer, 10);

	if(omRurns.Find(buffer) >= 0)
		blRet = true;

	return blRet;

}





