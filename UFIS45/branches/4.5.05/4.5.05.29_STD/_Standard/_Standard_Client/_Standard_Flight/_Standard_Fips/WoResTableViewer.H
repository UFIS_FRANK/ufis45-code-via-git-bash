// Modification History: 
//	061100	rkr		Table um die Spalten ETA(i), ETD(i), NAture(A/DTtyp) f�r arrival und departure erweitert



#ifndef __WoResTableViewer_H__
#define __WoResTableViewer_H__

#include <stdafx.h>
#include <Fpms.h>
#include <CCSGlobl.h>
#include <CCSTable.h>
#include <DiaCedaFlightData.h>
#include <CViewer.h>

/////////////////////////////////////////////////////////////////////////////
// Record structure declaration

struct WORESTABLE_LINEDATA
{
	long AUrno;
	long DUrno;
	CString		AFlno;
	CTime		AStoa;
	CTime		AEtai;
	CString		AOrg3;
	CString		ATtyp;
	CString		APos;
	CString   AGate1;		// 050309 MVy: Gate1 added for arrival
	CString   AGate2;		// 050309 MVy: Gate2 added for arrival
	CString		ABelt1;
	CString		ABelt2;
	CString		Act3;
	CString		Regn;

	CString		DFlno;
	CTime		DStod;
	CTime		DEtdi;
	CString		DDes3;
	CString		DTtyp;
	CString		DPos;
	CString		DGate1;
	CString		DGate2;		// 050309 MVy: Gate2 added for departure
	CString		DWro;
	CString     Towing; 
 	//CString		Remark;

	WORESTABLE_LINEDATA(void)
	{ 
		AUrno = 0;
		DUrno = 0;
		AFlno.Empty();
		AStoa = TIMENULL;
		AEtai = TIMENULL;
		AOrg3.Empty();
		ATtyp.Empty();
		APos.Empty();
		AGate1.Empty();		// 050309 MVy: Gate1 added for arrival
		AGate2.Empty();		// 050309 MVy: Gate2 added for arrival
		ABelt1.Empty();
		ABelt2.Empty();
		Act3.Empty();
		Regn.Empty();
		DFlno.Empty();
		DStod = TIMENULL;
		DEtdi = TIMENULL;
	 	DDes3.Empty();
		DTtyp.Empty();
		DPos.Empty();
		DGate1.Empty();
		DGate2.Empty();		// 050309 MVy: Gate2 added for departure
		DWro.Empty();
		Towing.Empty();
 		//Remark.Empty();

	}
};



/////////////////////////////////////////////////////////////////////////////
// WoResTableViewer

// 050309 MVy: Gate2 added, value increased from 20 to 23
#define WORESTABLE_COLCOUNT 24

/////////////////////////////////////////////////////////////////////////////
// Class declaration of WoResTableViewer

//@Man:
//@Memo: WoResTableViewer
//@See:  STDAFX, CCSCedaData, CedaDIAFLIGHTDATA, CCSPTRARRAY,  CCSTable
/*@Doc:
  No comment on this up to now.
*/
class WoResTableViewer : public CViewer
{
// Constructions
public:
    //@ManMemo: Default constructor
    WoResTableViewer(WoResTableDlg *popParentDlg);
    //@ManMemo: Default destructor
    ~WoResTableViewer();

  	void Attach(CCSTable *popAttachWnd);
    void ChangeViewTo(FipsModules ipMode, bool bpTowing = false);
	FipsModules imMode;


	bool bmTowing;

	int GetCount(FipsModules ipMode) const;

	void UnRegister();

	void ProcessFlightChange(const DIAFLIGHTDATA &rrpFlight);
	bool ProcessFlightDelete(const DIAFLIGHTDATA &rrpFlight);


private:

	static int imTableColCharWidths[WORESTABLE_COLCOUNT];
	CString omTableHeadlines[WORESTABLE_COLCOUNT];
	int imTableColWidths[WORESTABLE_COLCOUNT];
	//int imPrintColWidths[WORESTABLE_COLCOUNT];

	// Table fonts and widths
	CFont &romTableHeaderFont; 
	CFont &romTableLinesFont;
	const float fmTableHeaderFontWidth; 
	const float fmTableLinesFontWidth;

	bool IsPassFilter(const DIAFLIGHTDATA &rrpFlight, char cpFPart, FipsModules ipMode) const;
 
    void MakeLines(CCSPtrArray<DIAFLIGHTDATA> *popData = NULL);
	//void MakeLineFlight(const DIAFLIGHTDATA &rrpFlight, int &ripLineNo1, int &ripLineNo2);
	int MakeLine(const DIAFLIGHTDATA *prpAFlight, const DIAFLIGHTDATA *prpDFlight);
	void MakeLineData(const DIAFLIGHTDATA *prpAFlight, const DIAFLIGHTDATA *prpDFlight, WORESTABLE_LINEDATA &rrpLine) const;

	bool UtcToLocal(WORESTABLE_LINEDATA &rrpLine) const;

 	int  CreateLine(WORESTABLE_LINEDATA &rrpLine);
	void DeleteLine(int ipLineno);
	bool FindLine(long lpUrno, char cpFPart, int &ripLineno) const;
	
	void DeleteAll();
	void UpdateDisplay();
	//void SelectLine(long ilCurrUrno);
	void InsertDisplayLine( int ipLineNo);

	void DrawHeader();
	void MakeColList(WORESTABLE_LINEDATA *prlLine, CCSPtrArray<TABLE_COLUMN> &olColList);

	bool InsertFlight(const DIAFLIGHTDATA &rrpFlight);
 
	int CompareLines(const WORESTABLE_LINEDATA &rrpLine1, const WORESTABLE_LINEDATA &rrpLine2) const; 


private:
	CCSTable *pomTable;
	WoResTableDlg *pomParentDlg;
    CCSPtrArray<WORESTABLE_LINEDATA> omLines;



};

#endif //__WoResTableViewer_H__
