// RotationTableReportSelectExcel.cpp : implementation file
//

#include "stdafx.h"
#include "fpms.h"
#include "rotationtablereportselectexcel.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// RotationTableReportSelectExcel dialog


RotationTableReportSelectExcel::RotationTableReportSelectExcel(CWnd* pParent /*=NULL*/)
	: CDialog(RotationTableReportSelectExcel::IDD, pParent)
{
	//{{AFX_DATA_INIT(RotationTableReportSelectExcel)
	m_bDeparture = TRUE ;
	m_bArrival = TRUE ;
	m_bAirborn = TRUE ;
	m_bLanded = TRUE ;
	//}}AFX_DATA_INIT
}


void RotationTableReportSelectExcel::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(RotationTableReportSelectExcel)
	DDX_Check(pDX, IDC_CHK_DEP, m_bDeparture);
	DDX_Check(pDX, IDC_CHK_ARR, m_bArrival);
	DDX_Check(pDX, IDC_CHK_AIR, m_bAirborn);
	DDX_Check(pDX, IDC_CHK_LAN, m_bLanded);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(RotationTableReportSelectExcel, CDialog)
	//{{AFX_MSG_MAP(RotationTableReportSelectExcel)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// RotationTableReportSelectExcel message handlers
