
// CPP-FILE 

#include <stdafx.h>
#include <afxwin.h>
#include <CCSGlobl.h>
#include <CCSCedaData.h>
#include <ccsddx.h>
#include <CedaCflData.h>
#include <BasicData.h>
#include <Utils.h>
#include <Posdiagram.h>
#include <Gatdiagram.h>
#include <Bltdiagram.h>
#include <Wrodiagram.h>
#include <Flightdiagram.h>
#include <Konflikte.h>
#include <ButtonListDlg.h>

#include "fpms.h"		// 050307 MVy: access program specific configuration data and regarding helper stuff

void ProcessCflCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName);


CedaCflData::CedaCflData()
{
	// Create an array of CEDARECINFO for CFLDATA
	BEGIN_CEDARECINFO(CFLDATA,GrmDataRecInfo)
		CCS_FIELD_LONG(Urno,"URNO", "", 1)
		CCS_FIELD_CHAR_TRIM(Hopo,"HOPO", "", 1)
		CCS_FIELD_DATE(Time ,"TIME", "", 1)
		CCS_FIELD_CHAR_TRIM(Meno,"MENO", "", 1)
		CCS_FIELD_CHAR_TRIM(Mety,"METY", "", 1)
		CCS_FIELD_CHAR_TRIM(Prio,"PRIO", "", 1)
		CCS_FIELD_CHAR_TRIM(Rtab,"RTAB", "", 1)
		CCS_FIELD_LONG(Rurn,"RURN", "", 1)
		CCS_FIELD_CHAR_TRIM(Flst,"FLST", "", 1)
		CCS_FIELD_CHAR_TRIM(Nval,"NVAL", "", 1)
		CCS_FIELD_CHAR_TRIM(Oval,"OVAL", "", 1)
		CCS_FIELD_CHAR_TRIM(Akus,"AKUS", "", 1)
		CCS_FIELD_DATE(Akti ,"AKTI", "", 1)
		CCS_FIELD_CHAR_TRIM(Akst,"AKST", "", 1)
		CCS_FIELD_CHAR_TRIM(Addi,"ADDI", "", 1)
	END_CEDARECINFO //(CFLDATA)

	// Copy the record structure
	for (int i=0; i< sizeof(GrmDataRecInfo)/sizeof(GrmDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&GrmDataRecInfo[i],sizeof(CEDARECINFO));
		omRecInfo.Add(prpCedaRecInfo);
	} // end for

	// initialize field names
	strcpy(pcmTableName,"CFL");
	strcpy(pcmFList,"URNO,HOPO,TIME,MENO,METY,PRIO,RTAB,RURN,FLST,NVAL,OVAL,AKUS,AKTI,AKST,ADDI");

	pcmFieldList = pcmFList;


}; // end Constructor


static int Time(const CFLDATA **e1, const CFLDATA **e2)
{
		return ((**e1).Time == (**e2).Time)? 0: 
				((**e1).Time >  (**e2).Time)? 1: -1;
}



void CedaCflData::Register()
{
	ogDdx.Register((void *)this, BC_CFL_CHANGE, CString("BC_CFL_CHANGE"), CString("JobDataChange"),ProcessCflCf);
	ogDdx.Register((void *)this, BC_CFL_NEW, CString("BC_CFL_CHANGE"), CString("JobDataChange"), ProcessCflCf);
	ogDdx.Register((void *)this, CFL_RELOAD, CString("CFL_RELOAD"), CString("JobDataChange"), ProcessCflCf);
}



void CedaCflData::UnRegister()
{
	ogDdx.UnRegister(this,NOTUSED);
}




CedaCflData::~CedaCflData()
{
	omRecInfo.DeleteAll();
	omData.DeleteAll();
}


void CedaCflData::SetModId(DWORD	opModId)
{
	dwModId = opModId;

}



void  ProcessCflCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	CedaCflData *polInst = (CedaCflData*)vpInstance;

	polInst->ProcessCflBc(ipDDXType,vpDataPointer,ropInstanceName);
}




void CedaCflData::ClearAll()
{
	omUrnoMap.RemoveAll();
	omRtabRurnMenoAddiMap.RemoveAll();
	omData.DeleteAll();
}



bool CedaCflData::Read(const CString &ropRurns, bool bpDdx)
{
	
	omRurns = ropRurns;
	if(omRurns.IsEmpty())
		return true;

	CStringArray olRkeysLists;

	char pclSelection[7000] = "";
	CString olSelection;
	bool ilRc = true;
	bool blRet = true;
	for(int i = SplitItemList(omRurns, &olRkeysLists, 250) - 1; i >= 0; i--)
	{

		//olSelection = CString("WHERE RURN IN (");
		//olSelection += olRkeysLists[i] + CString(")");
		//strcpy(pclSelection, olSelection);

//		int iTempLen = sprintf( pclSelection, "WHERE RURN IN (%s)", olRkeysLists[i] );
		// 050307 MVy: do regarding to ceda.ini entry wheter current user see only his own confirmed conflicts
		if( __CanSeeOnlyOwnConfirmations() )
		{
			int iTempLen = sprintf( pclSelection, "WHERE RURN IN (%s) AND ((METY = 'FI'", olRkeysLists[i] );
			sprintf( pclSelection + iTempLen, " and AKUS = '%s') OR METY = '01')", pcgUser );
		}
		else
		{
			int iTempLen = sprintf( pclSelection, "WHERE RURN IN (%s)", olRkeysLists[i] );
		}

		if (!CedaAction("RT", pclSelection))
		{
			blRet = false;
		}

		int ilCount = GetBufferSize();

		ilRc = true;
		for (int ilLc = 0; ilRc == true; ilLc++)
		{
			CFLDATA *prlCfl = new CFLDATA;
			if ((ilRc = GetBufferRecord(ilLc,prlCfl)) == true)
			{
				InsertInternal(prlCfl, bpDdx);
			}
			else
			{
				delete prlCfl;
			}
		}

	}

    return blRet;
}

bool CedaCflData::ReadReload(const CString &ropUrnos, bool bpDdx)
{
	CStringArray olRkeysLists;
	char pclSelection[7000] = "";
	CString olSelection;
	bool ilRc = true;
	bool blRet = true;
	for(int i = SplitItemList(ropUrnos, &olRkeysLists, 500) - 1; i >= 0; i--)
	{

		olSelection = CString("WHERE URNO IN (");
		olSelection += olRkeysLists[i] + CString(")");
		strcpy(pclSelection, olSelection);

		if (!CedaAction("RT", pclSelection))
		{
			blRet = false;
		}

		int ilCount = GetBufferSize();

		ilRc = true;
		for (int ilLc = 0; ilRc == true; ilLc++)
		{
			CFLDATA *prlCfl = new CFLDATA;
			if ((ilRc = GetBufferRecord(ilLc,prlCfl)) == true)
			{
				char buffer[32];
				ltoa(prlCfl->Rurn, buffer, 10);
				if (omRurns.Find(buffer) >= 0)
				{
					int ilType = atoi(prlCfl->Meno);
					ogKonflikte.ConfirmInternal(prlCfl->Rurn, ilType);
					InsertInternal(prlCfl, bpDdx);
				}
				else
					delete prlCfl;
			}
			else
			{
				delete prlCfl;
			}
		}

	}

    return blRet;
}


bool CedaCflData::AddRurn(CString &opRurn)
{

	bool ilRc = true;


	if(omRurns.Find(opRurn) < 0)
	{
	
	
		omRurns += CString(",") + opRurn;

		char pclSelection[100] = "";


		sprintf(pclSelection, "WHERE RURN = %s", opRurn);

		bool blRet = CedaAction("RT", pclSelection);
		ilRc = blRet;

		int ilCount = GetBufferSize();

		for (int ilLc = 0; ilRc == true; ilLc++)
		{
			CFLDATA *prlCfl = new CFLDATA;
			if ((ilRc = GetBufferRecord(ilLc,prlCfl)) == true)
			{
				InsertInternal(prlCfl);
			}
			else
			{
				delete prlCfl;
			}
		}
	}

    return true;
}





CFLDATA *CedaCflData::GetCflByUrno(long lpUrno)
{
	CFLDATA  *prlCfl;
	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlCfl) == TRUE)
	{
		return prlCfl;
	}
	return NULL;
}



void CedaCflData::InsertInternal(CFLDATA *prpCfl, bool bpDdx)
{	
	omData.Add(prpCfl);
	prpCfl->dwModId = dwModId ;

	omUrnoMap.SetAt((void *)prpCfl->Urno,prpCfl);
	CString olMapKey;
	olMapKey.Format("%s%ld%s%s", prpCfl->Rtab, prpCfl->Rurn, prpCfl->Meno, prpCfl->Addi);
	omRtabRurnMenoAddiMap.SetAt(olMapKey ,prpCfl);

	if(bpDdx)
		ogDdx.DataChanged((void *)this,CFL_NEW,(void *)prpCfl);

}



bool CedaCflData::UpdateInternal(CFLDATA *prpCfl)
{
	CFLDATA  *prlCfl;
	if (omUrnoMap.Lookup((void *)prpCfl->Urno,(void *& )prlCfl) == TRUE)
	{
		CString olMapKey;
		olMapKey.Format("%s%ld%s%s", prlCfl->Rtab, prlCfl->Rurn, prlCfl->Meno, prlCfl->Addi);
		omRtabRurnMenoAddiMap.RemoveKey(olMapKey);

		*prlCfl = *prpCfl;
		prlCfl->dwModId = dwModId ;

		olMapKey.Format("%s%ld%s%s", prpCfl->Rtab, prpCfl->Rurn, prpCfl->Meno, prpCfl->Addi);
		omRtabRurnMenoAddiMap.SetAt(olMapKey, prlCfl);

		ogDdx.DataChanged((void *)this,CFL_CHANGE,(void *)prpCfl);
		return true;
	}
	return false;
}



bool CedaCflData::GetCflDataByRurnUrno(CCSPtrArray<CFLDATA> &ropCflList, long lpRurn)
{
	CFLDATA *prlCfl;
	POSITION pos;
	void *pVoid;
	int ilCount = 0;
	for( pos = omUrnoMap.GetStartPosition(); pos != NULL; )
	{
		omUrnoMap.GetNextAssoc( pos, pVoid , (void *&)prlCfl );
		if(prlCfl->Rurn == lpRurn)
		{
			ropCflList.Add(prlCfl);
			ilCount++;
		}
	}
	if(ilCount == 0)
	{
		return false;
	}
	ropCflList.Sort(Time);

	/*
	TRACE("\n");

	for(int i = 0; i < ropCflList.GetSize(); i++)
	{
		TRACE("\n %s", ropCflList[i].Time.Format("%H:%M"));

	}
	TRACE("\n");
	*/


	return true;
}



CFLDATA *CedaCflData::GetCflByRurnMenoAddi(long lpRurn, int ipMeno, const CString &ropAddi)
{
	CString olMeno;
	olMeno.Format("%d", ipMeno);
	for (int i=0; i < omData.GetSize(); i++)
	{
		if (omData[i].Rurn == lpRurn && 
			strcmp(omData[i].Meno, olMeno) == 0 && 
			strcmp(omData[i].Addi, ropAddi) == 0)
			return &omData[i];
	}
	return NULL;
}




void  CedaCflData::ProcessCflBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	CFLDATA *prlCfl;

	struct BcStruct *prlBcData;			//if(prlCca->Ctyp == "C")
	prlBcData = (struct BcStruct *) vpDataPointer;
	
	char buffer[64];
	
	switch(ipDDXType)
	{
	case CFL_RELOAD:
		{	
//			ClearAll();
//			Read(omRurns, true); 

//				ReadReload(prlBcData->Data, true); 
//				ogKonflikte.CheckAttentionButton();

				if (strcmp(prlBcData->Dest1,CCSCedaData::pcmUser) == 0 && strcmp(prlBcData->Dest2,CCSCedaData::pcmReqId) == 0)
				{
					ReadReload(prlBcData->Data, true); 
/*
					if (pogPosDiagram)
						pogPosDiagram->RedisplayAll();
					if (pogBltDiagram)
						pogBltDiagram->RedisplayAll();
					if (pogGatDiagram)
						pogGatDiagram->RedisplayAll();
					if (pogWroDiagram)
						pogWroDiagram->RedisplayAll();

	 				//kann flightdiagram.h hier nicht mehr aufl�sen
					if (pogButtonList)
						pogButtonList->UpdateFlightDia();
*/
					ogKonflikte.CheckAttentionButton(true);
				}
				else
				{
					// 050307 MVy: do regarding to ceda.ini entry whether current user see only his own confirmed conflicts
					if( !__CanSeeOnlyOwnConfirmations() )		// do only if user should see not only his own confirmations
					{
						ReadReload(prlBcData->Data, true); 
						ogKonflikte.CheckAttentionButton(true);
					};
				}
		}
		break;
	case BC_CFL_NEW:
		{

			prlCfl = new CFLDATA;
			GetRecordFromItemList(prlCfl,prlBcData->Fields,prlBcData->Data);

			itoa(prlCfl->Rurn, buffer,10);


			if(omRurns.Find(buffer) < 0)
			{
				delete prlCfl;
				return;

			}
			if(prlCfl->Urno != 0 && prlCfl->Rurn != 0)		
			{
				InsertInternal(prlCfl);

				if (bgConfFromCfltab)
				{
					ROTATIONFLIGHTDATA *prlFlight = ogRotationFlights.GetFlightByUrno(prlCfl->Rurn);
					if (prlFlight)
					{
						ogKonflikte.CreateCflConflict(prlFlight);
					}
					if (!prlFlight)
					{
						DIAFLIGHTDATA* prlFlightDia = ogPosDiaFlightData.GetFlightByUrno(prlCfl->Rurn);
						if (prlFlightDia)
						{
							ogKonflikte.CreateCflConflict(prlFlightDia);
						}
					}
				}
			}
			else
			{
				delete prlCfl;
			}
		}
		break;
	case BC_CFL_CHANGE:
		{
			prlCfl = new CFLDATA;
			GetRecordFromItemList(prlCfl,prlBcData->Fields,prlBcData->Data);

			itoa(prlCfl->Rurn, buffer,10);


			if(omRurns.Find(buffer) < 0)
			{
				delete prlCfl;
				return;

			}

			//prlCfl->Urno = 100095126;

			if (prlCfl->Urno == 0)
				prlCfl->Urno = MyGetUrnoFromSelection(prlBcData->Selection);

			if(prlCfl->Urno != 0 && prlCfl->Rurn != 0)		
			{
				UpdateInternal(prlCfl);

				if (bgConfFromCfltab)
				{
					ROTATIONFLIGHTDATA *prlFlight = ogRotationFlights.GetFlightByUrno(prlCfl->Rurn);
					if (prlFlight)
					{
						ogKonflikte.CreateCflConflict(prlFlight);
					}
					if (!prlFlight)
					{
						DIAFLIGHTDATA* prlFlightDia = ogPosDiaFlightData.GetFlightByUrno(prlCfl->Rurn);
						if (prlFlightDia)
						{
							ogKonflikte.CreateCflConflict(prlFlightDia);
						}
					}
				}
			}
			else
			{
				delete prlCfl;
			} 			
		}
		break;
	default:
		break;
	}
}



bool CedaCflData::IsConfirmed(const CString &ropRtab, long lpFUrno, char cpFPart, long lpRelFUrno, char cpRelFPart, int ipMeno) const
{
	CFLDATA *prlCfl;

	CString olAddiField;
	olAddiField.Format("%c|%ld|%c|", cpFPart, lpRelFUrno, cpRelFPart);

	CString olLookupStr;
	olLookupStr.Format("%s%ld%d%s", ropRtab, lpFUrno, ipMeno, olAddiField);

	if (omRtabRurnMenoAddiMap.Lookup(olLookupStr,(void *& )prlCfl) == TRUE)
	{
		CString olUser(prlCfl->Akus);
		olUser.TrimRight();

		//olUser = "";

		if(olUser.IsEmpty())
			return false;
		else
		{
			// 050307 MVy: do regarding to ceda.ini entry wheter current user see only his own confirmed conflicts
			//	just check that it is for me otherwise sth. had gone wrong reading the data
			if( __CanSeeOnlyOwnConfirmations() )
			{
				//ASSERT( olUser == pcgUser );
				if ( olUser == pcgUser )
					return true;
				else
					return false;
			};
			return true;	
		};
	}

	return false;
}

/*
	lpFUrno:	Urno of Flight whitch has the confict
	cpFPart:	'A': Arrival part has conflict / 'D': Departure part has conflict
	lpRelFUrno:	Urno of Flight whitch lpFUrno has a conflict
	cpRelFPart:	'A': lpFUrno has a conflict with the arrival part of lpRelFUrno
				'D': lpFUrno has a conflict with the departure part of lpRelFUrno
*/
bool CedaCflData::CreateConfirmed(long lpFUrno, char cpFPart, long lpRelFUrno, char cpRelFPart, int ipMeno, bool bpRelHdl , CString opConfText)
{
	if (ipMeno > 9999)
	{
		TRACE("CedaCflData::CreateConfirmed: Invalid Conflict Type!\n");
		return false;
	}


	bool blRet = true;
	CFLDATA rlCfl;

	strcpy(rlCfl.Hopo, pcgHome);

	strcpy(rlCfl.Flst, opConfText);

	strcpy(rlCfl.Mety, "FI");
	sprintf(rlCfl.Meno, "%4d", ipMeno);

	strcpy(rlCfl.Rtab, "AFT");
	rlCfl.Rurn = lpFUrno;

	CString olAddiField;
	olAddiField.Format("%c|%ld|%c|", cpFPart, lpRelFUrno, cpRelFPart);
	strncpy(rlCfl.Addi, olAddiField, 65);

	strcpy(rlCfl.Prio, "01");
	// confirmed conflict
	strcpy(rlCfl.Akus, pcgUser);
	rlCfl.Akti = CTime::GetCurrentTime();


	rlCfl.Urno = ogBasicData.GetNextUrno(); 

	CString olListOfData;
 	MakeCedaData(&omRecInfo, olListOfData, &rlCfl);

	if (bpRelHdl)
	{
		// only collect DB updates
		omRelCflIRTData.Add(olListOfData);
		CString olUrno;
		olUrno.Format("%ld", rlCfl.Urno);
		omRelCflURTUrnos.Add(olUrno);
		omRelCflUrnos += olUrno + ",";

	}
	else
	{
		// update DB directly
		blRet = blRet && CedaAction("IRT","","",olListOfData.GetBuffer(0));
	}
	
	return blRet;
}

 


bool CedaCflData::Confirm(long lpFUrno, char cpFPart, long lpRelFUrno, char cpRelFPart, int ipMeno, bool bpRelHdl/*  = false */)
{
	CString olAddi;
	olAddi.Format("%c|%ld|%c|", cpFPart, lpRelFUrno, cpRelFPart);

	CFLDATA  *prlCfl = GetCflByRurnMenoAddi(lpFUrno, ipMeno, olAddi);
	if (prlCfl == NULL) return false;
	return Confirm(prlCfl->Urno, bpRelHdl);
}

bool CedaCflData::UnConfirm(CString &ropRtab, char cpFPart, char cpRelFPart, long lpFUrno, long lpRelFUrno, int ipMeno)
{
	CFLDATA *prlCfl;

	CString olAddiField;
	olAddiField.Format("%c|%ld|%c|", cpFPart, lpRelFUrno, cpRelFPart);

	CString olLookupStr;
	olLookupStr.Format("%s%ld%d%s", ropRtab, lpFUrno, ipMeno, olAddiField);
	if (omRtabRurnMenoAddiMap.Lookup(olLookupStr,(void *& )prlCfl) == TRUE)
	{
		long llUrno = prlCfl->Urno;
		char bufferTmp[128];
		itoa(llUrno, bufferTmp, 10);

		omRtabRurnMenoAddiMap.RemoveKey(olLookupStr);

		char pclSelection[1024]="";
		CString olSel = "WHERE URNO = " + CString(bufferTmp);
		sprintf(pclSelection,olSel);
		bool blRet = CedaAction("DRT",pclSelection);
		return blRet;
	}
	return true;
}

bool CedaCflData::Confirm(long lpUrno, bool bpRelHdl/*  = false */)
{
	bool blRet = false;
	CFLDATA  *prlCfl = GetCflByUrno(lpUrno);

	CString olListOfData;
	char pclSelection[124];
	char pclData[524];

	if(prlCfl != NULL)
	{
		strcpy(prlCfl->Akus, pcgUser);
		prlCfl->Akti = CTime::GetCurrentTime();

		sprintf(pclSelection, "WHERE URNO = %ld", prlCfl->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prlCfl);
		if (bpRelHdl)
		{
			// only collect DB updates ( see CedaCflData::ReleaseDBUpdate() )
			CString olData;
			olData.Format("%s,%s", prlCfl->Akus, CTimeToDBString(prlCfl->Akti, TIMENULL));
			omRelCflURTFields = "AKUS,AKTI";
			omRelCflURTData.Add(olData);
			CString olUrno;
			olUrno.Format("%ld", prlCfl->Urno);
			omRelCflURTUrnos.Add(olUrno);
			omRelCflUrnos += olUrno + ",";
		}
		else
		{
			// update the DB directly
			strcpy(pclData,olListOfData);
			CedaAction("URT",pclSelection,"",pclData);
		}
	}

	return blRet;
}



bool CedaCflData::ReleaseDBUpdate()
{
	bool blRet = true;
	CString olRelComm;

	if (omRelCflIRTData.GetSize() != 0)
	{
		CString olData;
		for (int i = 0; i < omRelCflIRTData.GetSize(); i++)
		{
			olData += omRelCflIRTData[i] + CString("\n");
		}
		CString olFieldsTmp(pcmFieldList);
		int ilFieldNum = olFieldsTmp.Replace(',', '0')+1; // count fields
		olRelComm.Format("*CMD*,%s%s,IRT,%d,%s\n%s", pcmTableName, pcgTableExt, ilFieldNum, pcmFieldList, olData);

		blRet = blRet && CedaAction("REL","SLOW,NOBC,NOACTION","", olRelComm.GetBuffer(0));

	}


	if (omRelCflDRTData.GetSize() != 0)
	{

		//olRelComm.Format("*CMD*,%s%s,DRT,-1,,%s\n%s", pcmTableName, pcgTableExt, ropFields, ropData);

	}


	if (omRelCflURTData.GetSize() != 0)
	{
		CString olData;
		for (int i = 0; i < omRelCflURTData.GetSize(); i++)
		{
			olData += omRelCflURTData[i] + CString(",") + omRelCflURTUrnos[i] + CString("\n");
		}
		CString olFieldsTmp(omRelCflURTFields);
		int ilFieldNum = olFieldsTmp.Replace(',', '0')+1; // count fields
		olRelComm.Format("*CMD*,%s%s,URT,%d,%s,[URNO=:VURNO]\n%s", pcmTableName, pcgTableExt, ilFieldNum, omRelCflURTFields, olData);

		blRet = blRet && CedaAction("REL","SLOW,NOBC,NOACTION","", olRelComm.GetBuffer(0));
	}


	if(omRelCflUrnos.GetLength() > 0)
	{
		omRelCflUrnos = omRelCflUrnos.Left(omRelCflUrnos.GetLength()-1);
		CStringArray olRkeysLists;
		for(int i = SplitItemList(omRelCflUrnos, &olRkeysLists, 500) - 1; i >= 0; i--)
		{
			// send broadcast for reloading cfl data
 			blRet = CedaAction("SBC","RELCFL", "", "", "", olRkeysLists[i].GetBuffer(0));
		}
	}
/*
	CString olCflUrno ("");
	int ilAnz = 0;
	for (int i = 0; i < omRelCflURTUrnos.GetSize(); i++)
	{
		if (i == omRelCflURTUrnos.GetSize()-1)
			ilAnz = 800;

		olCflUrno += omRelCflURTUrnos[i] + CString(",");

		if (ilAnz == 800)
		{
			if(olCflUrno.GetLength() > 0)
				olCflUrno = olCflUrno.Left(olCflUrno.GetLength()-1);

			// send broadcast for reloading cfl data
 			blRet = CedaAction("SBC","RELCFL", "", "", "", olCflUrno.GetBuffer(0));

			ilAnz = 0;
			olCflUrno = "";
		}
		else
			ilAnz++;
	}
*/
	omRelCflIRTData.RemoveAll();
	omRelCflDRTData.RemoveAll();
	omRelCflURTData.RemoveAll();
	omRelCflURTUrnos.RemoveAll();
	omRelCflUrnos = "";

	return blRet;
}

