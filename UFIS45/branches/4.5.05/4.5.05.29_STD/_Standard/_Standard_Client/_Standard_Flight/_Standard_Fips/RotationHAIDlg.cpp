// RotationHAIDlg.cpp : implementation file
//
// Modification History: 
//	161100	rkr	Dialog nach Vorgaben PRF Athen umgestellt
//	171100	rkr	Dialog unabh�ngig von Datenstruktur

#include <stdafx.h>
#include <fpms.h>
#include <CCSDdx.h>
#include <CCSTime.h>
#include <CCSGlobl.h>
#include <RotationDlgCedaFlightData.h>
#include <ButtonListDlg.h>
#include <PrivList.h>
#include <CedaBasicData.h>
#include <RotationHAIDlg.h>
#include <HAIDlg.h>
#include <AskBox.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


// Local function prototype
static void RotationHAIDlgCf(void *popInstance, int ipDDXType,void *vpDataPointer, 
							 CString &ropInstanceName);


/////////////////////////////////////////////////////////////////////////////
// RotationHAIDlg dialog

RotationHAIDlg::RotationHAIDlg(CWnd* pParent, CString opAlc3StrArr, CString opAlc3StrDep)
	           :CDialog(RotationHAIDlg::IDD, pParent),
			    pomParent(pParent),
 	            omAlc3StrArr(""),
 	            omAlc3StrDep("")

{
	//{{AFX_DATA_INIT(RotationHAIDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	ogDdx.Register(this, BCD_HAI_UPDATE, CString("ROTATIONDLG_HAI"), CString("Hai Update"), RotationHAIDlgCf);
	ogDdx.Register(this, BCD_HAI_DELETE, CString("ROTATIONDLG_HAI"), CString("Hai Delete"), RotationHAIDlgCf);
	ogDdx.Register(this, BCD_HAI_INSERT, CString("ROTATIONDLG_HAI"), CString("Hai Insert"), RotationHAIDlgCf);

	pomParent = pParent;
    omAlc3StrArr = opAlc3StrArr;
    omAlc3StrDep = opAlc3StrDep;
	pomTable = new CCSTable;

	omHAIUrnos.RemoveAll();

}


RotationHAIDlg::~RotationHAIDlg()
{

	ogDdx.UnRegister(this, NOTUSED);

	delete pomTable;

}


void RotationHAIDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(RotationHAIDlg)
	DDX_Control(pDX, IDC_GMBORDER, m_CS_GMBorder);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(RotationHAIDlg, CDialog)
	//{{AFX_MSG_MAP(RotationHAIDlg)
	ON_BN_CLICKED(IDC_CLOSE, OnClose)
	ON_BN_CLICKED(IDC_DELETE, OnDelete)
	ON_BN_CLICKED(IDC_INSERT, OnInsert)
	ON_BN_CLICKED(IDC_UPDATE, OnUpdate)
    ON_MESSAGE(WM_TABLE_LBUTTONDBLCLK, OnTableLButtonDblclk)
	ON_WM_SIZE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// RotationHAIDlg message handlers
BOOL RotationHAIDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	int ilAgentForFlight = 0;
	ilAgentForFlight = CheckUpArrOrDEP();

	if (ilAgentForFlight > 0)
	{
//		pomParent->EnableWindow(TRUE);
		
		InitTable();

		if (ilAgentForFlight == 1)
			FillTableLine(omAlc3StrArr);
		if (ilAgentForFlight == 2)
			FillTableLine(omAlc3StrDep);
	}
	else
		OnClose();
	
	m_resizeHelper.Init(this->m_hWnd);
	m_resizeHelper.Fix(IDC_CLOSE,DlgResizeHelper::kWidth,DlgResizeHelper::kHeight);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}


void RotationHAIDlg::InitDialog()
{

	InitTable();

/*	if (prmAFlight && prmAFlight->Urno > 0)
		FillTableLine(prmAFlight);
	if (prmDFlight && prmDFlight->Urno > 0)
		FillTableLine(prmDFlight);

	if (prmAFlight && prmAFlight->Urno > 0)
		FillTableLine(prmAFlight);
	if (prmDFlight && prmDFlight->Urno > 0)
		FillTableLine(prmDFlight);
*/
}

void RotationHAIDlg::OnSize(UINT nType, int cx, int cy) 
{
	CDialog::OnSize(nType, cx, cy);

	m_resizeHelper.OnSize();

	if (this->pomTable != NULL && ::IsWindow(this->pomTable->m_hWnd))
	{
		CRect olrectTable;
		m_CS_GMBorder.GetWindowRect(&olrectTable);
		ScreenToClient(olrectTable);
		olrectTable.DeflateRect(2,2);     // hiding the CTable window border
		this->pomTable->SetPosition(olrectTable.left, olrectTable.right, olrectTable.top, olrectTable.bottom);

		this->pomTable->DisplayTable();
	}



/*
		if (nType != SIZE_MINIMIZED)
		{
			// resize table
			if (::IsWindow(this->m_CS_GMBorder.m_hWnd))
			{
				CRect olrectTable;
				m_CS_GMBorder.GetWindowRect(&olrectTable);
				ScreenToClient(olrectTable);
				olrectTable.DeflateRect(2,2);     // hiding the CTable window border
				this->pomTable->SetPosition(olrectTable.left, cx-20, olrectTable.top, cy-40);

				this->pomTable->DisplayTable();
			}

			CWnd* plWnd = NULL;
			plWnd = GetDlgItem(IDC_CLOSE);
			if (plWnd) 
			{
				plWnd->SetWindowPos(this, 10,cy-40+10,0,0, SWP_NOSIZE | SWP_NOZORDER | SWP_SHOWWINDOW);
				plWnd->Invalidate();
			}
		}
*/

	this->Invalidate();
}

void RotationHAIDlg::OnClose() 
{

	// TODO: Add your control notification handler code here
	CCS_TRY

	CDialog::OnCancel();

	CCS_CATCH_ALL
	
}


void RotationHAIDlg::OnCancel()
{

	CDialog::OnCancel();

}


void RotationHAIDlg::OnDelete() 
{
/*
	// TODO: Add your control notification handler code here
	CCS_TRY

	int ilAnz = (pomTable->GetCTableListBox())->GetCurSel();

	if(ilAnz < 0)
	{
		MessageBox(GetString(IDS_STRING909), GetString(ST_HINWEIS));
		return;
	}

	if(MessageBox(GetString(ST_DATENSATZ_LOESCHEN), GetString(ST_HINWEIS), MB_YESNO ) == IDYES)
	{
		if (ilAnz < omHAIUrnos.GetSize())
		{
			ogBCD.DeleteRecord("HAI", "URNO", omHAIUrnos[ilAnz], true);
			InitDialog();
		}
	}

	CCS_CATCH_ALL
*/	
}


void RotationHAIDlg::OnInsert() 
{
/*
	// TODO: Add your control notification handler code here
	CCS_TRY

	char clType = ' ';
	if (prmAFlight->Urno > 0 && prmDFlight->Urno <= 0)
		clType = 'A';
	if (prmDFlight->Urno > 0 && prmAFlight->Urno <= 0)
		clType = 'D';
	if (prmAFlight->Urno > 0 && prmDFlight->Urno > 0)
		clType = 'R';

	HAIDlg polDlg(this, clType, 'I');

	char olUrnoA[64];
	ltoa(prmAFlight->Urno, olUrnoA, 10);
	char olUrnoD[64];
	ltoa(prmDFlight->Urno, olUrnoD, 10);

	if (clType == 'R')
	{
		polDlg.SetAftTabUrnoA(CString(olUrnoA));
		polDlg.SetAftTabUrnoD(CString(olUrnoD));
	}
	if (clType == 'A')
	{
		polDlg.SetAftTabUrnoA(CString(olUrnoA));
	}
	if (clType == 'D')
	{
		polDlg.SetAftTabUrnoD(CString(olUrnoD));
	}
	
	polDlg.DoModal();


	pomTable->ResetContent();
	if (prmAFlight && prmAFlight->Urno > 0)
		FillTableLine(prmAFlight);
	if (prmDFlight && prmDFlight->Urno > 0)
		FillTableLine(prmDFlight);

	InitDialog();

	CCS_CATCH_ALL
*/	
}


void RotationHAIDlg::OnUpdate() 
{
/*
	// TODO: Add your control notification handler code here
	CCS_TRY

	int ilAnz = (pomTable->GetCTableListBox())->GetCurSel();

	if(ilAnz < 0)
	{
		MessageBox(GetString(IDS_STRING909), GetString(ST_HINWEIS));
		return;
	}
	
	if (ilAnz >= omHAIUrnos.GetSize())
		return;

	char clType = ' ';
	if (prmAFlight->Urno > 0 && prmDFlight->Urno <= 0)
		clType = 'A';
	if (prmDFlight->Urno > 0 && prmAFlight->Urno <= 0)
		clType = 'D';
	if (prmAFlight->Urno > 0 && prmDFlight->Urno > 0)
		clType = 'R';

	if (omHAIUrnos[ilAnz] == "")
	{
		MessageBox(GetString(IDS_STRING1747), GetString(ST_HINWEIS), MB_ICONEXCLAMATION);
		return;
	}

	HAIDlg polDlg(this, clType, 'U', omHAIUrnos[ilAnz]);

	char olUrnoA[64];
	ltoa(prmAFlight->Urno, olUrnoA, 10);
	char olUrnoD[64];
	ltoa(prmDFlight->Urno, olUrnoD, 10);

	if (clType == 'R')
	{
		polDlg.SetAftTabUrnoA(CString(olUrnoA));
		polDlg.SetAftTabUrnoD(CString(olUrnoD));
	}
	if (clType == 'A')
	{
		polDlg.SetAftTabUrnoA(CString(olUrnoA));
	}
	if (clType == 'D')
	{
		polDlg.SetAftTabUrnoD(CString(olUrnoD));
	}

	polDlg.DoModal();


	pomTable->ResetContent();
	if (prmAFlight && prmAFlight->Urno > 0)
		FillTableLine(prmAFlight);
	if (prmDFlight && prmDFlight->Urno > 0)
		FillTableLine(prmDFlight);

	CCS_CATCH_ALL
*/	
}


LONG RotationHAIDlg::OnTableLButtonDblclk( UINT wParam, LPARAM lParam)
{
/*
#if 0
	// anhand der Mousposition die Spalte ermitteln
	int ilCol = 0;
	CPoint point;
    ::GetCursorPos(&point);
    pomDailyScheduleTable->pomListBox->ScreenToClient(&point);    

	UINT ipItem = wParam;
	DAILYSCHEDULE_LINEDATA *prlTableLine = NULL;
	prlTableLine = (DAILYSCHEDULE_LINEDATA *)pomDailyScheduleTable->GetTextLineData(ipItem);

	// Inhalt in der Zeile?
	if (prlTableLine != NULL)
	{
		// Spaltennummer
		ilCol = pomDailyScheduleTable->GetColumnnoFromPoint(point);
	}

	DAILYFLIGHTDATA *prlFlight;
	if((prlFlight = ogDailyCedaFlightData.GetFlightByUrno(prlTableLine->AUrno)) == NULL)
		prlFlight = ogDailyCedaFlightData.GetFlightByUrno(prlTableLine->DUrno);


	// Behandlung: Groundmovement
	if (strcmp (prlFlight->Ftyp, "G") == 0)
	{
		if(pogRotGroundDlg == NULL)
		{
			pogRotGroundDlg = new RotGroundDlg(this);
			pogRotGroundDlg->Create(IDD_ROTGROUND);
		}

		// gueltiger Wert der Spalte
		if (ilCol < 0)
			return 0L;

		// Klick auf Arrival oder Departure
		if (omViewer.GetColumnByIndex(ilCol).Left(1) == "A")
		{
			prlFlight = ogDailyCedaFlightData.GetFlightByUrno(prlTableLine->AUrno);
			if(prlFlight == NULL) 
				return 0L;
		}
		else
		{
			prlFlight = ogDailyCedaFlightData.GetFlightByUrno(prlTableLine->DUrno);
			if(prlFlight == NULL) 
				return 0L;
		}

		pogRotGroundDlg->SetWindowPos(&wndTop, 0,0,0,0, SWP_SHOWWINDOW | SWP_NOSIZE | SWP_NOMOVE );
		pogRotGroundDlg->NewData(this, prlFlight->Rkey, prlFlight->Urno);
	}
	else
	{
		if (prlTableLine != NULL)
		{
			CString olAdid("");
			//DAILYFLIGHTDATA *prlFlight;

			if((prlFlight = ogDailyCedaFlightData.GetFlightByUrno(prlTableLine->AUrno)) == NULL)
			{
				prlFlight = ogDailyCedaFlightData.GetFlightByUrno(prlTableLine->DUrno);
				olAdid = "D";
			}
			else
			{
				olAdid = "A";
			}
			
			// Rotationsmaske aufrufen
			if(prlFlight != NULL)
			{
				pogRotationDlg->NewData(this, prlFlight->Rkey, prlFlight->Urno, olAdid);
				omViewer.UpdateDisplay();
			}
		}
	}

#endif
	OnUpdate();
*/
	return 0L;

}


void RotationHAIDlg::InitTable()
{

	// TODO: Add your control notification handler code here
	CCS_TRY

	CRect olRectBorder;
	m_CS_GMBorder.GetWindowRect( olRectBorder );
	ScreenToClient(olRectBorder);
	olRectBorder.DeflateRect(2,2);

	omHAIUrnos.RemoveAll();

	pomTable->SetHeaderSpacing(0);
    pomTable->SetTableData(this, olRectBorder.left, olRectBorder.right, olRectBorder.top, olRectBorder.bottom);
	pomTable->SetSelectMode(0);
	pomTable->SetShowSelection(true);
	pomTable->ResetContent();

	CCSPtrArray <TABLE_HEADER_COLUMN> omHeaderDataArray;

	
	TABLE_HEADER_COLUMN rlHeader;

	rlHeader.Alignment = COLALIGN_CENTER;
	rlHeader.Font = &ogCourier_Regular_10;

	// Handling Task,TASK
	rlHeader.Length = 20;
	rlHeader.Text = CString("");
	omHeaderDataArray.New(rlHeader);
	// Code,HSNA
	rlHeader.Length = 5; 
	rlHeader.Text = CString("");
	omHeaderDataArray.New(rlHeader);
	// Handling Agent,HNAM
	rlHeader.Length = 40; 
	rlHeader.Text = CString("");
	omHeaderDataArray.New(rlHeader);
	// Tel.No,TELE
	rlHeader.Length = 10; 
	rlHeader.Text = CString("");
	omHeaderDataArray.New(rlHeader);
	// Fax.No,FAXN
	rlHeader.Length = 10; 
	rlHeader.Text = CString("");
	omHeaderDataArray.New(rlHeader);
	// Remark,REMA
	rlHeader.Length = 40; 
	rlHeader.Text = CString("");
	omHeaderDataArray.New(rlHeader);
	
	pomTable->SetHeaderFields(omHeaderDataArray);
	pomTable->SetDefaultSeparator();
	pomTable->SetTableEditable(false);
	omHeaderDataArray.DeleteAll();

	pomTable->DisplayTable();

	CCS_CATCH_ALL
	
}


void RotationHAIDlg::DrawHeader()
{

	CCS_TRY

	int  ilTotalLines = 0;
	bool blNewLogicLine = false;

	CCSPtrArray <TABLE_HEADER_COLUMN> omHeaderDataArray;
	TABLE_HEADER_COLUMN *prlHeader[6];
	int ilFontFactor=9;

	//Task
	prlHeader[0] = new TABLE_HEADER_COLUMN;
	prlHeader[0]->Alignment = COLALIGN_CENTER;
	prlHeader[0]->Length = 20*ilFontFactor;
	prlHeader[0]->Font = &ogCourier_Bold_10;
	prlHeader[0]->Text = GetString(IDS_STRING2016);
	//Code	
	prlHeader[1] = new TABLE_HEADER_COLUMN;
	prlHeader[1]->Alignment = COLALIGN_CENTER;
	prlHeader[1]->Length = 5*ilFontFactor;
	prlHeader[1]->Font = &ogCourier_Bold_10;
	prlHeader[1]->Text = GetString(IDS_STRING2017);
	//Handling Agent
	prlHeader[2] = new TABLE_HEADER_COLUMN;
	prlHeader[2]->Alignment = COLALIGN_CENTER;
	prlHeader[2]->Length = 40*ilFontFactor;
	prlHeader[2]->Font = &ogCourier_Bold_10;
	prlHeader[2]->Text = GetString(IDS_STRING2018);
	//TelNo
	prlHeader[3] = new TABLE_HEADER_COLUMN;
	prlHeader[3]->Alignment = COLALIGN_CENTER;
	prlHeader[3]->Length = 10*ilFontFactor;
	prlHeader[3]->Font = &ogCourier_Bold_10;
	prlHeader[3]->Text = GetString(IDS_STRING2019);
	//FaxNo
	prlHeader[4] = new TABLE_HEADER_COLUMN;
	prlHeader[4]->Alignment = COLALIGN_CENTER;
	prlHeader[4]->Length = 10*ilFontFactor;
	prlHeader[4]->Font = &ogCourier_Bold_10;
	prlHeader[4]->Text = GetString(IDS_STRING2020);
	//Remark
	prlHeader[5] = new TABLE_HEADER_COLUMN;
	prlHeader[5]->Alignment = COLALIGN_CENTER;
	prlHeader[5]->Length = 40*ilFontFactor;
	prlHeader[5]->Font = &ogCourier_Bold_10;
	prlHeader[5]->Text = GetString(IDS_STRING2021);
	for(int ili = 0; ili < 6; ili++)
	{
		omHeaderDataArray.Add(prlHeader[ili]);
	}

	pomTable->SetHeaderFields(omHeaderDataArray);
	omHeaderDataArray.DeleteAll();

	pomTable->SetDefaultSeparator();

	CCS_CATCH_ALL
	
}


void RotationHAIDlg::FillTableLine(CString& opStrAlc3)
{

	CCS_TRY

	//urno der Fluggesellschaft
	CString olUrnoStr ("");
	if (!ogBCD.GetField("ALT", "ALC3", opStrAlc3, "URNO", olUrnoStr))
		return;

	CCSPtrArray<TABLE_COLUMN> olColList;
	TABLE_COLUMN rlColumnData;

	rlColumnData.VerticalSeparator = SEPA_NONE;
	rlColumnData.SeparatorType = SEPA_NONE;
	rlColumnData.Font = &ogCourier_Regular_10;

	CCSEDIT_ATTRIB rlAttrib;
	rlAttrib.Style = ES_UPPERCASE;
	
	//recordset von allen agents der fluggesellschaft
	CCSPtrArray<RecordSet>rlHais;
	ogBCD.GetRecords("HAI", "ALTU", olUrnoStr, &rlHais);

	rlColumnData.Alignment = COLALIGN_LEFT;
	for(int i = 0; i < rlHais.GetSize(); i++)
	{
		//agent code(wird noch in HAG ben�tigt)
		CString olHsna = rlHais[i].Values[ogBCD.GetFieldIndex("HAI","HSNA")];

		//task
		/*
		//task code(wird noch in HTY ben�tigt)
		CString olHtyp = rlHais[i].Values[ogBCD.GetFieldIndex("HAI","TASK")];

		rlColumnData.Text = "";
		ogBCD.GetField("HTY", "HTYP", olHtyp, "HNAM", rlColumnData.Text);
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		*/

		rlColumnData.Text = rlHais[i].Values[ogBCD.GetFieldIndex("HAI","TASK")];
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		//code
		rlColumnData.Text = olHsna;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		//agent name
		rlColumnData.Text = "";
		ogBCD.GetField("HAG", "HSNA", olHsna, "HNAM", rlColumnData.Text);
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		//agent tel
		rlColumnData.Text = "";
		ogBCD.GetField("HAG", "HSNA", olHsna, "TELE", rlColumnData.Text);
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		//agent fax
		rlColumnData.Text = "";
		ogBCD.GetField("HAG", "HSNA", olHsna, "FAXN", rlColumnData.Text);
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		//remarm
		rlColumnData.Text = "";
		rlColumnData.Text = rlHais[i].Values[ogBCD.GetFieldIndex("HAI","REMA")];
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		pomTable->AddTextLine(olColList, NULL);
		olColList.DeleteAll();
	}

	DrawHeader();
	pomTable->DisplayTable();

	CCS_CATCH_ALL

}



static void RotationHAIDlgCf(void *popInstance, int ipDDXType,void *vpDataPointer, 
							 CString &ropInstanceName)
{

    RotationHAIDlg *polDlg = (RotationHAIDlg *)popInstance;


	if (ipDDXType == BCD_VIP_UPDATE || ipDDXType == BCD_VIP_DELETE || ipDDXType == BCD_VIP_INSERT)
		polDlg->InitDialog();

}

//check zu welcher aircraft die agents darzustellen sind, R�ckgabe 0=nichts,1=arr,2=dep
int RotationHAIDlg::CheckUpArrOrDEP() 
{
	int ilRet = 0;
	if (omAlc3StrArr.GetLength() == 0 && omAlc3StrDep.GetLength() == 0)
		return 0;

	//unterschiedliche aircrafts
	if (strcmp(omAlc3StrArr, omAlc3StrDep) != 0)// && prmAFlight != NULL && prmDFlight != NULL)
	{
		//user arr oder dep w�hlen lassen
		if (omAlc3StrArr.GetLength() > 0 && omAlc3StrDep.GetLength() > 0)
		{
			CString olMessage = GetString(IDS_STRING2022);
//			CString olMessage ("ARR and DEP Airline Code are different - Please select the Handling Agent Information for the ARR or DEP Airline");
			CString olCaption = GetString(IDS_STRING2023);
			AskBox olDlg(this, olCaption, olMessage, "ARR", "DEP");//"Select the Handling Agent"
			ilRet = olDlg.DoModal();
		}
		//nur arr belegt
		else if (omAlc3StrArr.GetLength() > 0 &&  omAlc3StrDep.GetLength() == 0) 
			ilRet = 1;
		//nur dep belegt
		else if (omAlc3StrDep.GetLength() > 0 &&  omAlc3StrArr.GetLength() == 0) 
			ilRet = 2;
		//nichts
		else
			ilRet = 0;
	}
	//gleiche aircrafts oder nur arr/dep
	else
	{
		//gleiche aircrafts (Alc3 kann leer sein!)
		if (omAlc3StrArr.GetLength() > 0)// &&  prmAFlight != NULL && prmDFlight != NULL) 
			ilRet = 1;
		else if (omAlc3StrDep.GetLength() > 0)//&&  prmAFlight != NULL && prmDFlight != NULL) 
			ilRet = 2;
		else
			ilRet = 0;
	}

	return ilRet;
}
