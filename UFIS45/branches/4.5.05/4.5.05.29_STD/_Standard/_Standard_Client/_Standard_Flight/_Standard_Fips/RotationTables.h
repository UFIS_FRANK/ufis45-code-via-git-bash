#ifndef __ROTATIONTABLES__
#define __ROTATIONTABLES__

#include <CCSClientWnd.h>
#include <CCS3DStatic.h>
#include <CCSTable.h>
#include <RotationTableViewer.h>
#include <CCSDragDropCtrl.h>
 


class RotationTables : public CFrameWnd
{
	DECLARE_DYNCREATE(RotationTables)
public:
	RotationTables();
	RotationTables(CWnd *pParent);           // protected constructor used by dynamic creation
	virtual ~RotationTables();

	void SetWndPos();

	bool ShowFlight(long lpUrno);
	bool SelectFlight(long lpUrno);
	void SaveToReg();
	
	BOOL bm_ProcessingTblChg;
	long lm_lastSelUrno1;
	long lm_lastSelUrno2;



// Attributes
	CDialogBar omDialogBar;

    CCS3DStatic *pomTSTDate;
    CCS3DStatic *pomTSTTime;
	
	void StartFlightPermitsAppl(long lpFlightUrno);
	
	RotationTableViewer omViewer;

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(Flugplan)
	public:
	virtual BOOL DestroyWindow();
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(RotationTables)
//    afx_msg void OnNextChart();
//    afx_msg void OnPrevChart();

    afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnDestroy();
	afx_msg void OnClose();
    afx_msg void OnGetMinMaxInfo(MINMAXINFO FAR* lpMMI);
    afx_msg void OnSize(UINT nType, int cx, int cy);
    afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags);
    afx_msg LONG OnPositionChild(WPARAM wParam, LPARAM lParam);
    afx_msg LONG OnUpdateDiagram(WPARAM wParam, LPARAM lParam);
    afx_msg void OnAnsicht();
    afx_msg void OnAutoNow();
    afx_msg void OnInsert();
	afx_msg void OnEnde();
    afx_msg void OnPrint();
    afx_msg void OnJustNow();
	afx_msg void OnShowAllTables();
    afx_msg void OnDetails();
    afx_msg void OnGround();
	afx_msg void OnSearch();
    afx_msg LONG OnTouchAndGo(UINT wParam, LONG lParam);
	//afx_msg void OnUpdate();
    afx_msg LONG OnPlaceRound(UINT wParam, LONG lParam);
    afx_msg LONG OnReturnTaxi(UINT wParam, LONG lParam);
	afx_msg LONG OnReturnFlight(UINT wParam, LONG lParam);
    afx_msg LONG OnTableLButtonDblclk(UINT wParam, LONG lParam);
    afx_msg LONG OnTableLButtonDown(UINT wParam, LONG lParam);
    afx_msg LONG OnTableRButtonDown(UINT wParam, LONG lParam);
    afx_msg LONG OnTableIPEdit(UINT wParam, LONG lParam);
	afx_msg LONG OnTableIPEditKillfocus( UINT wParam, LPARAM lParam);
	afx_msg LONG OnMaximizeChild(UINT wParam, LONG lParam);
	afx_msg void OnSelchangeComboAnsicht();
    afx_msg LONG OnTableReturnPressed(UINT wParam, LONG lParam);
	afx_msg LONG OnTableSelChanged( UINT wParam, LPARAM lParam);
    afx_msg LONG OnTowing(UINT wParam, LONG lParam);

	afx_msg void OnCloseupView();
	virtual void OnCancel();
	afx_msg void OnZeit();
	afx_msg void OnViewSelChange();
	afx_msg void OnExcel();
    afx_msg void OnInsert2();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()


//----hier
private:
	BOOL bmIsViewOpen;

	bool fmDlg1;
	bool fmDlg2;
	// Tabellencontrol

	// TLE: UFIS 3768 ========================
	// Disable the button in CDialogBar will 
	// do by ON_UPDATE_COMMAND_UI message.

	void OnDisableViewButton(CCmdUI *pCmdUI);
	void OnDisableAllViewButton(CCmdUI *pCmdUI);
	void OnDisableSearchButton(CCmdUI *pCmdUI);
	void OnDisableShowAllTableButton(CCmdUI *pCmdUI);
	void OnDisableInsertButton(CCmdUI *pCmdUI);
	void OnDisableAutoNowButton(CCmdUI *pCmdUI);
	void OnDisableJustNowButton(CCmdUI *pCmdUI);
	void OnDisableInsert2Button(CCmdUI *pCmdUI);
	void OnDisableFontButton(CCmdUI *pCmdUI);
	void OnDisableDetailsButton(CCmdUI *pCmdUI);
	void OnDisableExcelButton(CCmdUI *pCmdUI);
	void OnDisablePrintButton(CCmdUI *pCmdUI);
	void OnDisableCloseButton(CCmdUI *pCmdUI);

	// =======================================


// Operations 
public:
    void PositionChild();
	void UpdateComboBox();
	void ChangeViewTo(const char *pcpViewName);
	void OnUpdatePrevNext(void);
 	BOOL bmNoUpdatesNow;
	void UpdateView();


	//long lmCurrUrno;
	//int imCurrColumn;
	//int imCurrLine;
	//int imCurrTable;

	int m_DialogBarHeight;
	CCSClientWnd omClientWnd;
	CStatusBar omStatusBar;

	int imStartTimeScalePos;
    
	CString omCaptionText;

	CPtrArray omPtrArray;		// contains RotationTableCharts
	RotationTableChart* GetChartByIndex( int ndxChart );		// 050302 MVy: access RotationTableChart from untyped pointer array
	
private:
    static CPoint omMaxTrackSize;
    static CPoint omMinTrackSize;
    static COLORREF lmBkColor;
    static COLORREF lmTextColor;
    static COLORREF lmHilightColor;

	void SetCurrentTime(RotationTableChart *popChart, int ipColumn, int ipLine);
	bool OnAct3list(CString& opAct5, CString& opAct3);
	bool OnAct35list(CString& opAct5, CString& opAct3);
	CString m_key;

};

/////////////////////////////////////////////////////////////////////////////

#endif
