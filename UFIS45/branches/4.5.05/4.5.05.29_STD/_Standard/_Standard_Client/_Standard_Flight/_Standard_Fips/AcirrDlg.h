#ifndef AFX_ACIRRDLG_H__314D7AD1_B404_11D1_8153_0000B43C4B01__INCLUDED_
#define AFX_ACIRRDLG_H__314D7AD1_B404_11D1_8153_0000B43C4B01__INCLUDED_

// AcirrDlg.h : Header-Datei
//
#include <CCSEdit.h>
#include <DlgResizeHelper.h>

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CAcirrDlg 

class CAcirrDlg : public CDialog
{
// Konstruktion
public:
	CAcirrDlg(CWnd* pParent = NULL, CString opReg = "");   // Standardkonstruktor

// Dialogfelddaten
	//{{AFX_DATA(CAcirrDlg)
	enum { IDD = IDD_ACIRRDLG };
	CButton	m_CE_Apu;
	CButton	m_CB_OK;
	CCSEdit	m_CE_Rem;
	CCSEdit	m_CE_Lfzkennung;
	CString	m_Lfzkennung;
	CString	m_Rem;
	BOOL	m_Apu;
	//}}AFX_DATA
	CString omReg;	// Registrierung (LFZ-Kennzeichen bei initialisierung)

// Überschreibungen
	// Vom Klassen-Assistenten generierte virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(CAcirrDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	//}}AFX_VIRTUAL


	bool FillFields(CString opRegn);
	void SaveToReg();

// Implementierung
protected:

	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(CAcirrDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	virtual void OnCancel();
	afx_msg LONG OnEditKillfocus( UINT wParam, LPARAM lParam);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnDestroy();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:
	DlgResizeHelper m_resizeHelper;
	CString m_key;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio fügt zusätzliche Deklarationen unmittelbar vor der vorhergehenden Zeile ein.

#endif // AFX_ACIRRDLG_H__314D7AD1_B404_11D1_8153_0000B43C4B01__INCLUDED_
