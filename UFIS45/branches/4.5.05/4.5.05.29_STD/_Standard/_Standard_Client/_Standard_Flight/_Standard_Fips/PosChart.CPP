// stafchrt.cpp : implementation file
//

#include <stdafx.h>
#include <CCSGlobl.h>
#include <resource.h>
#include <FPMS.h>
#include <CCSButtonCtrl.h>
#include <CCSClientWnd.h>
#include <CCSTimeScale.h>
#include <ccsdragdropctrl.h>
#include <PosDiaViewer.h>
#include <PosGantt.h>
#include <PosDiagram.h>
//----------------------------------------------------------------------

#include <PosChart.h>

#ifdef _DEBUG
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
// PosChart
/////////////////////////////////////////////////////////////////////////////

IMPLEMENT_DYNCREATE(PosChart, CFrameWnd)

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

PosChart::PosChart()
{
    pomTimeScale = NULL;
    pomStatusBar = NULL;
    pomViewer = NULL;
    
    pomTopScaleText = NULL;
	pomCountText = NULL;

	imStartVerticalScalePos = 30;    


	imStartTopScaleTextPos = 100;

	if(bgShowMXAC)
		imStartTopScaleTextPos = 100;
	else
		imStartTopScaleTextPos = 70;
	
	
	lmBkColor = lgBkColor;
	m_bMouseTracking = FALSE;
}

PosChart::~PosChart()
{
    if (pomTopScaleText != NULL)
        delete pomTopScaleText;
	if (pomCountText != NULL)
		delete pomCountText;
}

BEGIN_MESSAGE_MAP(PosChart, CFrameWnd)
    //{{AFX_MSG_MAP(PosChart)
    ON_WM_CREATE()
	ON_WM_DESTROY()
    ON_WM_SIZE()
    ON_WM_ERASEBKGND()
    ON_WM_PAINT()
    ON_BN_CLICKED(IDC_CHARTBUTTON, OnChartButton)
    ON_MESSAGE(WM_CCSBUTTON_RBUTTONDOWN, OnChartButtonRButtonDown)
    ON_MESSAGE(WM_DRAGOVER, OnDragOver)
    ON_MESSAGE(WM_DROP, OnDrop)  
	ON_COMMAND(31, OnMenuAssign)
	ON_WM_KEYDOWN()
	ON_WM_KEYUP()
	ON_WM_WINDOWPOSCHANGED()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


int PosChart::GetHeight()
{
    if (imState == Minimized)
		/*
		if(bgFWRGantt && GetGroupNo() == 0)
			return 45;
		else
		*/
	        return imStartVerticalScalePos + imBorderSize; // height of ChartButtton/TopScaleText
    else                               
	{
		if(bgFWRGantt && GetGroupNo() == 0)
			return imStartVerticalScalePos + omGantt.GetGanttChartHeight()  + imBorderSize;
		else
			return imStartVerticalScalePos + omGantt.GetGanttChartHeight()  + imBorderSize;
	}
}

/////////////////////////////////////////////////////////////////////////////
// PosChart message handlers


int PosChart::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
    if (CFrameWnd::OnCreate(lpCreateStruct) == -1)
        return -1;
    
    // TODO: Add your specialized creation code here
     CString olStr; GetWindowText(olStr);

	if(bgShowMXAC)
		omButton.Create(olStr, BS_OWNERDRAW | WS_CHILD | WS_VISIBLE, CRect(5, 4, 90, 4 + 20), this, IDC_CHARTBUTTON);
	else
		omButton.Create(olStr, BS_OWNERDRAW | WS_CHILD | WS_VISIBLE, CRect(5, 4, 67, 4 + 20), this, IDC_CHARTBUTTON);

    omButton.SetFont(&ogMSSansSerif_Bold_8, FALSE);
     olStr = GetViewer()->GetGroupText(GetGroupNo());    
    omButton.SetWindowText(olStr);

	if(!bgDiaGrouping)
	{
		omButton.ShowWindow(SW_HIDE);
	}
     
//	if(GetGroupNo() == 0)
//		imStartVerticalScalePos = 37;

	//
    pomCountText = new CCS3DStatic(TRUE);
    pomCountText->Create("999", WS_CHILD | WS_VISIBLE,
        CRect(130 + 10, 4 + 1, imStartTopScaleTextPos - 10, (4 + 20) - 1), this);
	//
    
	CRect olRect; GetClientRect(&olRect);

    pomTopScaleText = new CCS3DStatic(TRUE);
    pomTopScaleText->Create("", WS_CHILD | WS_VISIBLE,
        CRect(imStartTopScaleTextPos + 6, 4 + 1, olRect.right - 6, (4 + 20) - 1), this);

	m_ChartWindowDragDrop.RegisterTarget(this, this);
    m_CountTextDragDrop.RegisterTarget(pomCountText, this);
    m_TopScaleTextDragDrop.RegisterTarget(pomTopScaleText, this);

    ////
    // read all data
    SetState(Normal);
    
    omGantt.SetTimeScale(GetTimeScale());
   
	omGantt.SetViewer(GetViewer(),GetGroupNo());
    omGantt.SetStatusBar(GetStatusBar());
    omGantt.SetDisplayWindow(GetStartTime(), GetStartTime() + GetInterval());
    omGantt.SetVerticalScaleWidth(imStartTopScaleTextPos);
	GetViewer()->MakeMasstab();
    omGantt.SetFonts(GetViewer()->GetGeometryFontIndex(), GetViewer()->GetGeometryFontIndex());
    omGantt.SetVerticalScaleColors(/*BLUE*/NAVY, SILVER, NAVY, YELLOW, GRAY);
	omGantt.SetGanttChartColors(NAVY, SILVER, NAVY, YELLOW);
	omGantt.bmMiniBar = GetViewer()->bmGeometryTimeLines;

	omGantt.SetTopScaleText(pomTopScaleText);


    omGantt.Create(0, CRect(olRect.left, imStartVerticalScalePos, olRect.right, olRect.bottom), this);
 
#ifndef	PICHIT_FIXED_THE_COUNTER
	char clBuf[255];
	int ilCount = GetViewer()->GetAllLineCount();
	sprintf(clBuf, "%d", ilCount);
	pomCountText->SetWindowText(clBuf);
#else
	char clBuf[255];
	int ilCount = GetViewer()->GetAllLineCount();
	pomCountText->SetWindowText(clBuf);
#endif
	pomCountText->ShowWindow(SW_HIDE);
    pomTopScaleText->SetWindowText(olStr);
    
    pomTopScaleText->GetWindowText(clBuf, sizeof(clBuf));
	//pomTopScaleText->ShowWindow(SW_HIDE);


    CRect olWindowRect; 
	GetWindowRect(&olWindowRect);
    CRect olClientRect; 
	GetClientRect(&olClientRect);
	
	imBorderSize = (olWindowRect.right - olWindowRect.left) - olClientRect.right; 

	//TRACE("\n Onsize: border_thickness: %ld -> %ld", GetGroupNo(), imBorderSize);	


	return 0;
}

void PosChart::SetMarkTime(CTime opStatTime, CTime opEndTime)
{
	omGantt.SetMarkTime(opStatTime, opEndTime);
}

void PosChart::OnDestroy() 
{
//	if (bgModal == TRUE)
//		return;
	// Id 30-Sep-96
	// This will remove a lot of warning message when the user change view.
	// If we just delete a staff chart, MFC will produce two warning message.
	// First, Revoke not called before the destructor.
	// Second, calling DestroyWindow() in CWnd::~CWnd.

	m_ChartWindowDragDrop.Revoke();
    m_ChartButtonDragDrop.Revoke();
    m_CountTextDragDrop.Revoke();
    m_TopScaleTextDragDrop.Revoke();

	omGantt.DestroyWindow();

	CFrameWnd::OnDestroy();
}


void PosChart::OnWindowPosChanged( WINDOWPOS* lpwndpos )
{
	CFrameWnd::OnWindowPosChanged(lpwndpos);
	if(bgFWRGantt && GetGroupNo() == 0 )
	{
		GetParent() -> GetParent() -> SendMessage(WM_POSITIONCHILD, 0, 0L);
	}
}

void PosChart::OnSize(UINT nType, int cx, int cy) 
{
   	CFrameWnd::OnSize(nType, cx, cy);
    
    CRect olClientRect; 
	GetClientRect(&olClientRect);

    CRect olWindowRect; 
	GetWindowRect(&olWindowRect);
	

	CRect olRect(imStartTopScaleTextPos + 6, 4 + 1, olClientRect.right - 6, (4 + 20) - 1);
    pomTopScaleText->MoveWindow(&olRect, FALSE);


	olRect.SetRect (olClientRect.left, imStartVerticalScalePos, olClientRect.right, olClientRect.bottom );

	omGantt.MoveWindow(&olRect, FALSE);
	
	
	if(bgFWRGantt && GetGroupNo() == 0 && 	pogPosDiagram->bmOnSize == false)
	{
		if (imState == Maximized)
		{
			if(cy != 0)
			{
				// for what ever reason the cy is samller than it should be??? so I calculate the Client cy 
				int ilClientHeight = olWindowRect.bottom - olWindowRect.top - imBorderSize;


				omGantt.imHeight = ilClientHeight - imStartVerticalScalePos ;
				if(omGantt.imHeight < 0) // Height should not be smaller than the statusbar
					omGantt.imHeight = 0;

				//TRACE("\n Onsize: Height set to %ld", omGantt.imHeight);	


			}
		}
	}


}

BOOL PosChart::OnEraseBkgnd(CDC* pDC) 
{
    // TODO: Add your message handler code here and/or call default
    CRect olClipRect;
    pDC->GetClipBox(&olClipRect);
    //GetClientRect(&olClipRect);

    CBrush olBrush(lmBkColor);
    CBrush *polOldBrush = pDC->SelectObject(&olBrush);
    pDC->PatBlt(olClipRect.left, olClipRect.top,
        olClipRect.Width(), olClipRect.Height(),
        PATCOPY);
    pDC->SelectObject(polOldBrush);
    
    return TRUE;
    
    //return CFrameWnd::OnEraseBkgnd(pDC);
}

void PosChart::OnPaint()
{
    CPaintDC dc(this); // device context for painting
    
    // TODO: Add your message handler code here
    
    // Do not call CFrameWnd::OnPaint() for painting messages
    CPen *polOldPen = (CPen *) dc.SelectStockObject(BLACK_PEN);

    CRect olClientRect;
    GetClientRect(&olClientRect);
    
#define imHorizontalPos imStartVerticalScalePos
#define imVerticalPos imStartTopScaleTextPos

    // draw horizontal seperator
    dc.SelectStockObject(BLACK_PEN);
    dc.MoveTo(olClientRect.left, imHorizontalPos - 2);
    dc.LineTo(olClientRect.right, imHorizontalPos - 2);
    
    dc.SelectStockObject(WHITE_PEN);
    dc.MoveTo(olClientRect.left, imHorizontalPos - 1);
    dc.LineTo(olClientRect.right, imHorizontalPos - 1);
    //

    // draw vertical seperator
    dc.SelectStockObject(BLACK_PEN);
    dc.MoveTo(imVerticalPos - 2, olClientRect.top);
    //dc.LineTo(imVerticalPos - 2, olClientRect.bottom);
    dc.LineTo(imVerticalPos - 2, imHorizontalPos - 2);
    
    dc.SelectStockObject(WHITE_PEN);
    dc.MoveTo(imVerticalPos - 1, olClientRect.top);
    //dc.LineTo(imVerticalPos - 1, olClientRect.bottom);
    dc.LineTo(imVerticalPos - 1, imHorizontalPos - 2);
    //

}

void PosChart::OnChartButton()
{
 	if (!bgDiaGrouping) 
	{
		return;
	}
   if (imState == Minimized)
        SetState(Maximized);
    else
        SetState(Minimized);

	GetParent() -> GetParent() -> SendMessage(WM_POSITIONCHILD, 0, 0L);
}

LONG PosChart::OnChartButtonRButtonDown(UINT wParam, LONG lParam)
{
	CMenu menu;
	menu.CreatePopupMenu();
	for (int i = menu.GetMenuItemCount(); i > 0; i--)
	     menu.RemoveMenu(i, MF_BYPOSITION);
    menu.AppendMenu(MF_STRING,31, "Einteilen");	// Confirm

	CPoint olPoint(LOWORD(lParam),HIWORD(lParam));
	ClientToScreen(&olPoint);
    menu.TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, olPoint.x, olPoint.y, this, NULL);

	return 0L;
}

void PosChart::OnMenuAssign()
{

}

void PosChart::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	GetParent()->SendMessage(WM_KEYDOWN, nChar, MAKELONG(LOWORD(nRepCnt), LOWORD(nFlags)));
}

void PosChart::OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	GetParent()->SendMessage(WM_KEYUP, nChar, MAKELONG(LOWORD(nRepCnt), LOWORD(nFlags)));
}


LONG PosChart::OnDragOver(UINT wParam, LONG lParam)
{
	return -1L;	// cannot accept this object
//----------------------------------------------------------------------
}

LONG PosChart::OnDrop(UINT wParam, LONG lParam)
{
    return -1L;
//----------------------------------------------------------------------
}

LONG PosChart::ProcessDropShifts(CCSDragDropCtrl *popDragDropCtrl, DROPEFFECT lpDropEffect)
{
	return 0L;
}

//-DTT Jul.23-----------------------------------------------------------
LONG PosChart::ProcessDropDutyBar(CCSDragDropCtrl *popDragDropCtrl, DROPEFFECT lpDropEffect)
{
	return 0L;
}

LONG PosChart::ProcessDropFlightBar(CCSDragDropCtrl *popDragDropCtrl, DROPEFFECT lpDropEffect)
{
	return 0L;
}
//----------------------------------------------------------------------
