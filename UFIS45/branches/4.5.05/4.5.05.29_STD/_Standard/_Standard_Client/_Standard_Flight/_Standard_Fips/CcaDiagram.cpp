// CcaDiagram.cpp : implementation file
// Modification History:
// 19-feb-01	rkr	PRF 1305 Dubai: Gantchart-Anzeige unter local unterst�tzt
//

#include <stdafx.h>
#include <FPMS.h>
#include <CcaDiagram.h>
#include <ButtonListDlg.h>
#include <CcaDiaPropertySheet.h>
#include <CcaCedaFlightData.h>
#include <CcaChart.h>
#include <CcaGantt.h>
#include <TimePacket.h>
#include <RotationISFDlg.h>
#include <CicDemandTableDlg.h>
#include <CicNoDemandTableDlg.h>
#include <CicConfTableDlg.h>
#include <CCAExpandDlg.h>
#include <DataSet.h>
#include <DelelteCounterDlg.h>
#include <AllocateCca.h>
#include <AllocateCcaParameter.h>
#include <CcaCommonTableDlg.h>
#include <FlightSearchTableDlg.h>
#include <PrivList.h>
#include <BltKonflikteDlg.h>
#include <Utils.h>
#include <SpotAllocation.h>
#include <Konflikte.h>
#include <CCSGlobl.h>
#include <process.h>
#include <Utils.h>
#include <BltOverviewTableDlg.h>
#include <UndoCkiDlg.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

static void CcaDiagramCf(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

/////////////////////////////////////////////////////////////////////////////
// CcaDiagram
extern int igAnsichtPageIndex;

IMPLEMENT_DYNCREATE(CcaDiagram, CFrameWnd)

CcaDiagram::CcaDiagram()
{
	igAnsichtPageIndex = -1;
	omMaxTrackSize = CPoint(4000/*1024*/, 1500);
	omMinTrackSize = CPoint(1024 / 4, 768 / 4);
	omViewer.SetViewerKey("CCADIA");
	omViewer.SelectView("<Default>");
    imStartTimeScalePos = 104;
    imStartTimeScalePos++;              // plus one for left border of chart
    imFirstVisibleChart = -1;
	lmBkColor = lgBkColor;
	pogCcaDiagram = this;
	bmIsViewOpen = false;
	bgKlebefunktion = false;
	pogCicDemandTableDlg = NULL;//new CicDemandTableDlg(this);
	bmRepaintAll = false;
	pomCicKonflikteDlg = NULL;
	bmTimeLine = true;
	pomBltOverviewTableDlg = NULL;
	bmAlreadyAlloc = false;	
	bmLoadFlights = false;

    lmBkColor = COLORREF(SILVER);;
    lmTextColor = COLORREF(BLACK);;
    lmHilightColor = COLORREF(BLACK);;

	bmAutoAllockInWork = false;

	omDuration = CTimeSpan(1,0,0,0);
	omStartTime = CTime::GetCurrentTime() - CTimeSpan(0,12,0,0);

	m_key = "DialogPosition\\CheckInDiagram";
	bmOnSize = false;

}

CcaDiagram::~CcaDiagram()
{

	if(pogCicDemandTableDlg != NULL)
	{
		pogCicDemandTableDlg->SaveToReg();
		delete pogCicDemandTableDlg;
	}
	if(pogCicNoDemandTableDlg != NULL)
	{
		pogCicNoDemandTableDlg->SaveToReg();
		delete pogCicNoDemandTableDlg;
	}
	if(pogCicConfTableDlg != NULL)
	{
		pogCicConfTableDlg->SaveToReg();
		delete pogCicConfTableDlg;
	}
	pogCcaDiagram = NULL;
}


BEGIN_MESSAGE_MAP(CcaDiagram, CFrameWnd)
	//{{AFX_MSG_MAP(CcaDiagram)
	ON_WM_CREATE()
	ON_WM_DESTROY()
	ON_BN_CLICKED(IDC_OFFLINE, OnOffline)
	ON_BN_CLICKED(IDC_BEENDEN, OnBeenden)
	ON_BN_CLICKED(IDC_ANSICHT, OnAnsicht)
	ON_BN_CLICKED(IDC_INSERT, OnInsert)
	ON_BN_CLICKED(IDC_CICDEMAND, OnDemand )
	ON_BN_CLICKED(IDC_CIC_CONF, OnCicConf )
	ON_BN_CLICKED(IDC_COMMON_CCA, OnCommonCca )
	ON_BN_CLICKED(IDC_EXPAND, OnExpand )
	ON_BN_CLICKED(IDC_ALLOCATE, OnAllocate)
	ON_BN_CLICKED(IDC_FLIGHTS_WITHOUT_DEM, OnFlightsWithoutResource)
	ON_BN_CLICKED(IDC_DELETE_TIMEFRAME, OnDeleteTimeFrame)
	ON_BN_CLICKED(IDC_PRINT_GANTT, OnPrintGantt)
    ON_WM_GETMINMAXINFO()
    ON_WM_PAINT()
    ON_WM_ERASEBKGND()
    ON_WM_SIZE()
    ON_WM_HSCROLL()
	ON_BN_CLICKED(IDC_PREV, OnNextChart)
	ON_BN_CLICKED(IDC_NEXT, OnPrevChart)
    ON_WM_TIMER()
    ON_MESSAGE(WM_POSITIONCHILD, OnPositionChild)
    ON_MESSAGE(WM_UPDATEDIAGRAM, OnUpdateDiagram)
    ON_CBN_SELCHANGE(IDC_VIEW, OnViewSelChange)
	ON_CBN_SELCHANGE(IDC_CHANGESCENARIO, OnScenarioChange) // TSC
	ON_CBN_CLOSEUP(IDC_VIEW, OnCloseupView)
	ON_BN_CLICKED(IDC_ZEIT, OnZeit)
	ON_BN_CLICKED(IDC_SEARCH, OnSearch)
	ON_WM_KEYDOWN()
	ON_WM_KEYUP()
	ON_WM_CLOSE()
	ON_MESSAGE(WM_REPAINT_ALL, RepaintAll)
//	ON_BN_CLICKED(IDC_GATPOS_CONF, OnGatposConf)
	ON_BN_CLICKED(IDC_CCAATTENTION, OnGatposConf)
	ON_BN_CLICKED(IDC_SCENARIO, OnScenario) //TSC
	ON_BN_CLICKED(IDC_CHANGES, OnChanges)
	ON_MESSAGE(WM_HANDLING_AGENT_COLOR_CHANGED, OnHandlingAgentColorChanged )		// 050329 MVy: color changed in setup dialog
	ON_BN_CLICKED(IDC_CCAOVERVIEW, OnOverview)
	ON_BN_CLICKED(IDC_UNDO, OnUndo)

	// TLE: UFIS 3768 ========================
	ON_UPDATE_COMMAND_UI(IDC_ANSICHT,OnDisableViewButton)
	ON_UPDATE_COMMAND_UI(IDC_ZEIT,OnDisableNowButton)
	ON_UPDATE_COMMAND_UI(IDC_SEARCH,OnDisableSearchButton)
	ON_UPDATE_COMMAND_UI(IDC_FLIGHTS_WITHOUT_DEM,OnDisableFWDButton)
	ON_UPDATE_COMMAND_UI(IDC_CICDEMAND,OnDisableFWRButton)
	ON_UPDATE_COMMAND_UI(IDC_CHANGES,OnDisableChangeButton)
	ON_UPDATE_COMMAND_UI(IDC_ALLOCATE,OnDisableAllocationButton)
	ON_UPDATE_COMMAND_UI(IDC_OFFLINE,OnDisableOfflineButton)
	ON_UPDATE_COMMAND_UI(IDC_EXPAND,OnDisableExpandButton)
	ON_UPDATE_COMMAND_UI(IDC_CCAATTENTION,OnDisableAttention)
	ON_UPDATE_COMMAND_UI(IDC_CIC_CONF,OnDisableConflictButton)
	ON_UPDATE_COMMAND_UI(IDC_COMMON_CCA,OnDisableCommonButton)
	ON_UPDATE_COMMAND_UI(IDC_DELETE_TIMEFRAME,OnDisableDeleteButton)
	ON_UPDATE_COMMAND_UI(IDC_PRINT_GANTT,OnDisablePrintButton)
	ON_UPDATE_COMMAND_UI(IDC_ZEIT,OnDisableNowButton)
	ON_UPDATE_COMMAND_UI(IDC_BEENDEN,OnDisableCloseButton)
	// =======================================

	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

static void CcaDiagramCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName)
{
	CcaDiagram *polDiagram = (CcaDiagram *)popInstance;

	switch (ipDDXType)
	{
	case CCA_VIEW_CHANGED:
        polDiagram->ViewSelChange( (char *) vpDataPointer);
		break;
	case CCA_GET_CUR_VIEW:
		polDiagram->GetCurView((char *) vpDataPointer);
		break;
	case REDISPLAY_ALL:
		polDiagram->RedisplayAll();
		break;
	case STAFFDIAGRAM_UPDATETIMEBAND:
		TIMEPACKET *polTimePacket = (TIMEPACKET *)vpDataPointer;
		polDiagram->SetTimeBand(polTimePacket->StartTime, polTimePacket->EndTime);
		polDiagram->UpdateTimeBand();
		break;
	}
}

bool CcaDiagram::GetLoadedAllocations(CMapStringToString& opCicUrnoMap)
{
	return omViewer.GetLoadedAllocations(opCicUrnoMap);
}

/////////////////////////////////////////////////////////////////////////////
// CcaDiagram message handlers
#if 0
void CcaDiagram::PositionChild()
{

    CRect olRect;
    CRect olChartRect;
    
    int ilLastY = 0;
    omClientWnd.GetClientRect(&olRect);
         
    pomChart->GetClientRect(&olChartRect);
    olChartRect.right = olRect.right;

    olChartRect.top = ilLastY;

    ilLastY += pomChart->GetHeight();
    olChartRect.bottom = ilLastY;
        
    // check
    if ((pomChart->GetState() != Minimized) &&
		(olChartRect.top < olRect.bottom) && (olChartRect.bottom > olRect.bottom))
    {
        olChartRect.bottom = olRect.bottom;
        pomChart->SetState(Normal);
    }
    //
        
    pomChart->MoveWindow(&olChartRect, FALSE);
	pomChart->ShowWindow(SW_SHOW);

    
	omClientWnd.Invalidate(TRUE);
	//SetAllStaffAreaButtonsColor();
 	UpdateTimeBand();
    
	
}
#endif
// Grouping
void CcaDiagram::PositionChild()
{
    CRect olRect;
    CRect olChartRect;
    CcaChart *polChart;
    
    int ilLastY = 0;
    omClientWnd.GetClientRect(&olRect);
	// Grouping
    for (int ilIndex = 0; ilIndex < omChartArray.GetSize(); ilIndex++)
    {
        polChart = (CcaChart *) omChartArray.GetAt(ilIndex);
		
        if (ilIndex < imFirstVisibleChart && !(bgFWRGantt && ilIndex == 0))
        {
            polChart->ShowWindow(SW_HIDE);
            continue;
        }
		
        polChart->GetWindowRect(&olChartRect);

		ScreenToClient(olChartRect);

        olChartRect.right = olRect.right;
		
        olChartRect.top = ilLastY;
		
        ilLastY += polChart->GetHeight();
        olChartRect.bottom = ilLastY;
    
		ilLastY++; 
		
        // check
        if ((polChart->GetState() != Minimized) &&
			(olChartRect.top < olRect.bottom) &&
			(olChartRect.bottom > olRect.bottom))
        {
            olChartRect.bottom = olRect.bottom;
            polChart->SetState(Maximized);
        }
        //
        
        polChart->MoveWindow(&olChartRect, FALSE);
        polChart->ShowWindow(SW_SHOW);
	}
	
    omClientWnd.Invalidate(TRUE);
	SetAllAreaButtonsColor();
	OnUpdatePrevNext();
	UpdateTimeBand();
}

// Grouping
void CcaDiagram::OnUpdatePrevNext(void)
{
	
	//	return;
	// Id 25-Sep-96
	// This will set the keyboard focus back to the bottom most list-box after this button was clicked.
	//SetFocus();
	
	if (imFirstVisibleChart == 0)
		GetDlgItem(IDC_NEXT)->EnableWindow(FALSE);
	else
		GetDlgItem(IDC_NEXT)->EnableWindow(TRUE);
	
	if (imFirstVisibleChart == omChartArray.GetUpperBound())
		GetDlgItem(IDC_PREV)->EnableWindow(FALSE);
	else
		GetDlgItem(IDC_PREV)->EnableWindow(TRUE);
}


// Grouping
void CcaDiagram::SetPosAreaButtonColor(int ipGroupno)
{
	if (bgDiaGrouping) 
	{
		CTime ilStart = omViewer.GetGeometrieStartTime(); //ogBasicData.GetTime();
		CTime ilEnd = ilStart + CTimeSpan(0,3,0,0);
		CcaChart *polChart;
		int ilColorIndex; //x;
		ilColorIndex = 0; //FIRSTCONFLICTCOLOR+(CFI_NOCONFLICT*2);
		//	ilColorIndex = omViewer.GetGroupColorIndex(ipGroupno,ilStart,ilEnd);
		
		if(ipGroupno < omChartArray.GetSize())	
		{

			polChart = (CcaChart *) omChartArray.GetAt(ipGroupno);
			if (polChart != NULL)
			{
				CCSButtonCtrl *prlButton =polChart->GetChartButtonPtr();
				if (prlButton != NULL)
				{
					if(bgFWRGantt && ipGroupno == 0)
					{
						int ilCount = omViewer.GetLineCount( ipGroupno);
						CString olStr = omViewer.GetGroupText(ipGroupno);
						olStr.Format("%s (%d)", olStr, ilCount);
						prlButton->SetWindowText(olStr);
						prlButton->Invalidate(FALSE);

					}
					if (ilColorIndex == 0) //FIRSTCONFLICTCOLOR+(CFI_NOCONFLICT*2))
					{
						prlButton->SetColors(::GetSysColor(COLOR_BTNFACE),
							::GetSysColor(COLOR_BTNSHADOW),
							::GetSysColor(COLOR_BTNHIGHLIGHT));
					}
					else
					{
						prlButton->SetColors(ogColors[ilColorIndex],
							::GetSysColor(COLOR_BTNSHADOW),
							::GetSysColor(COLOR_BTNHIGHLIGHT));
					}
				}
			}
		}
	}
}

// Grouping
void CcaDiagram::SetAllAreaButtonsColor(void)
{
	if (bgDiaGrouping) {
	int ilGroupCount = omViewer.GetGroupCount();
	for (int ilGroupno = 0; ilGroupno < ilGroupCount; ilGroupno++)
	{
		SetPosAreaButtonColor(ilGroupno);
	}
	}
}



void CcaDiagram::SetTSStartTime(CTime opTSStartTime)
{

    omTSStartTime = opTSStartTime;

	CTime olTSStartTime = omTSStartTime;
	if(bgGatPosLocal)
		ogBasicData.UtcToLocal(olTSStartTime);

    char clBuf[20];
    sprintf(clBuf, "  %02d%02d%02d  ", 
        olTSStartTime.GetDay(), olTSStartTime.GetMonth(), olTSStartTime.GetYear() % 100);
//        omTSStartTime.GetDay(), omTSStartTime.GetMonth(), omTSStartTime.GetYear() % 100);
    omTSDate.SetWindowText(clBuf);
    omTSDate.Invalidate(FALSE);
}

// Grouping
CListBox *CcaDiagram::GetBottomMostGantt()
{
	// Check the size of the area for displaying charts.
	// Pichet used "omClientWnd" not the diagram itself, so we will get the size of this window
	CRect olClientRect;
	omClientWnd.GetClientRect(olClientRect);
	omClientWnd.ClientToScreen(olClientRect);
	
	// Searching for the bottommost chart
	CcaChart *polChart;
	for (int ilLc = imFirstVisibleChart; ilLc < omChartArray.GetSize(); ilLc++)
	{
		polChart = (CcaChart *)omChartArray[ilLc];
		CRect olRect, olChartRect;
		polChart->GetClientRect(olChartRect);
		polChart->ClientToScreen(olChartRect);
		if (!olRect.IntersectRect(&olChartRect, &olClientRect))
			break;
	}
	
	// Check if the chart we have found is a valid one
	--ilLc;
	if (!(0 <= ilLc && ilLc <= omChartArray.GetSize()-1))
		return NULL;
	
	return &((CcaChart *)omChartArray[ilLc])->omGantt;
}


int CcaDiagram::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	{
	pogCicDemandTableDlg = new CicDemandTableDlg(this);
	pogCicNoDemandTableDlg = new CicNoDemandTableDlg(this);
	pogCicConfTableDlg = new CicConfTableDlg(this);

	CRect olRect;
	GetWindowRect(olRect);
/*
	pogCicNoDemandTableDlg->MoveWindow(CRect(0, olRect.top + pogButtonList->m_nDialogBarHeight + 25, 670, olRect.top + 550),FALSE);
	pogCicDemandTableDlg->MoveWindow(CRect(olRect.right - 1024, olRect.top + pogButtonList->m_nDialogBarHeight + 25, olRect.right -25, olRect.top + 550),FALSE);
	pogCicConfTableDlg->MoveWindow(CRect(olRect.right - 1024, olRect.bottom - 300, olRect.right - 25, olRect.bottom - 25),FALSE);
*/
	pogCicNoDemandTableDlg->ShowWindow(SW_HIDE);
	pogCicDemandTableDlg->ShowWindow(SW_HIDE);
	pogCicConfTableDlg->ShowWindow(SW_HIDE);
	}

	// Create ConflictDlg
	if (pomCicKonflikteDlg == NULL)
	{
		pomCicKonflikteDlg = new BltKonflikteDlg(this);
//		pomCicKonflikteDlg->SetWindowPos(&wndTop,100,200,700,400, SWP_HIDEWINDOW);
	}	

	// Create OverviewTableDlg
	if (pomBltOverviewTableDlg == NULL && bgAllocationOverview)
	{
		pomBltOverviewTableDlg = new BltOverviewTableDlg(this);
		pomBltOverviewTableDlg->SetWindowPos(&wndTop,100,200,850,400, SWP_HIDEWINDOW);
		pomBltOverviewTableDlg->SetResource("CCA");
	}


/*
	pogCicDemandTableDlg = new CicDemandTableDlg(this);
	CRect olNoResRect;
	pogCicDemandTableDlg->GetWindowRect(&olNoResRect);
	int Nleft, Ntop, Nright, Nbottom;
	CRect olThisRect;
	GetWindowRect(&olThisRect);
	Nleft = 0;
	Ntop = pogButtonList->m_nDialogBarHeight;
	Nbottom = Ntop+500;
	Nleft = GetSystemMetrics(SM_CXSCREEN) - 1024;
	Nright = GetSystemMetrics(SM_CXSCREEN);


	olNoResRect.top = Ntop;
	olNoResRect.bottom = Nbottom;
	olNoResRect.left = Nleft;
	olNoResRect.right = Nright;
	WINDOWPLACEMENT rlP;
    rlP.flags = WPF_SETMINPOSITION;
    rlP.showCmd = SW_MINIMIZE;
	rlP.ptMinPosition = CPoint(Nright - 190, pogButtonList->m_nDialogBarHeight + 1);
	rlP.ptMaxPosition = CPoint(Nleft, Ntop);
	rlP.rcNormalPosition = CRect(Nleft, Ntop - pogButtonList->m_nDialogBarHeight + 1, Nright, Nbottom);
 	pogCicDemandTableDlg->SetWindowPlacement( &rlP); 

*/


    SetTimer(0, (UINT) 60 * 1000, NULL);

//rkr130301
//    SetTimer(1, (UINT) 60 * 1000 * 2, NULL);
    SetTimer(1, (UINT) 60 * 1000 * ogPosDiaFlightData.cimBartimesUpdateMinute, NULL);  // Update bar times

	omDialogBar.Create( this, IDD_CCADIAGRAM, CBRS_TOP, IDD_CCADIAGRAM );
	SetWindowPos(&wndTop, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE);

	//CButton *polCB;


	CButton *polButton = (CButton *)omDialogBar.GetDlgItem(IDC_OFFLINE2);	
 	WINDOWPLACEMENT olWndPlace;
	polButton->GetWindowPlacement( &olWndPlace ); 
	m_CB_Offline.Create(GetString(IDS_STRING1258), WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON | BS_OWNERDRAW, olWndPlace.rcNormalPosition, &omDialogBar, IDC_OFFLINE);
    m_CB_Offline.SetFont(polButton->GetFont());
	m_CB_Offline.EnableWindow(true);
	#ifdef _FIPSVIEWER
	{
		m_CB_Offline.EnableWindow(false);
	}
	#else
	{
	}
	#endif




	CButton	*polButtonUndo = (CButton *)omDialogBar.GetDlgItem(IDC_UNDO);	
	polButtonUndo->ShowWindow(SW_HIDE);

	if(bgUndo && ogPosDiaFlightData.bmOffLine)
	{
		if(ogPosDiaFlightData.bmOffLine)
		{
			polButtonUndo->ShowWindow(SW_SHOW);
		}
	}


	// Create 'CcaAttention'-Button
	polButton = (CButton *)omDialogBar.GetDlgItem(IDC_GATPOS_CONF);	
	polButton->GetWindowPlacement( &olWndPlace ); 
	m_CB_CcaAttention.Create(GetString(IMFK_ATTENTION), WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON | BS_OWNERDRAW, olWndPlace.rcNormalPosition, &omDialogBar, IDC_CCAATTENTION);
    m_CB_CcaAttention.SetFont(polButton->GetFont());
	m_CB_CcaAttention.EnableWindow(true);
	// Update Attention-Button
	ogKonflikte.CheckAttentionButton();
//	m_CB_CcaAttention.SetSecState(ogPrivList.GetStat("CCADIAGRAMM_CB_Attention"));	

	// Create 'Wores'-Button
	polButton = (CButton *)omDialogBar.GetDlgItem(IDC_CICDEMAND2);	
	polButton->GetWindowPlacement( &olWndPlace ); 
	CString olTmp;
	olTmp.Format("%s (%d)", GetString(IDS_STRING2624),0);
	m_CB_WoRes.Create(olTmp, WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON | BS_OWNERDRAW, olWndPlace.rcNormalPosition, &omDialogBar, IDC_CICDEMAND);
    m_CB_WoRes.SetFont(polButton->GetFont());
	m_CB_WoRes.EnableWindow(true);
	m_CB_WoRes.SetSecState(ogPrivList.GetStat("CCADIAGRAMM_CB_WORes"));	

	if(!bgFWRList)
		m_CB_WoRes.ShowWindow(SW_HIDE);


	// Create 'WoDem'-Button
	polButton = (CButton *)omDialogBar.GetDlgItem(IDC_FLIGHTS_WITHOUT_DEM);	
	polButton->GetWindowPlacement( &olWndPlace ); 
	olTmp.Format("%s (%d)", GetString(IDS_STRING2625),0);
	m_CB_WoDem.Create(olTmp, WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON | BS_OWNERDRAW, olWndPlace.rcNormalPosition, &omDialogBar, IDC_FLIGHTS_WITHOUT_DEM);
    m_CB_WoDem.SetFont(polButton->GetFont());
	m_CB_WoDem.EnableWindow(true);
	m_CB_WoDem.SetSecState(ogPrivList.GetStat("CCADIAGRAMM_CB_WODemands"));	

	// Create 'WoDem'-Button
	polButton = (CButton *)omDialogBar.GetDlgItem(IDC_CIC_CONF);	
	polButton->GetWindowPlacement( &olWndPlace ); 
	olTmp.Format("%s (%d/%d)", GetString(IDS_STRING2626),0,0);
	m_CB_Conf.Create(olTmp, WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON | BS_OWNERDRAW, olWndPlace.rcNormalPosition, &omDialogBar, IDC_CIC_CONF);
    m_CB_Conf.SetFont(polButton->GetFont());
	m_CB_Conf.EnableWindow(true);
	m_CB_Conf.SetSecState(ogPrivList.GetStat("CCADIAGRAMM_CB_Conflicts"));	 

	// Create 'Changes'-Button
	polButton = (CButton *)omDialogBar.GetDlgItem(IDC_CHANGES);	
	polButton->GetWindowPlacement( &olWndPlace ); 
//	olTmp = GetString(IDS_CHANGES);
	olTmp.Format("%s (%d/%d)", GetString(IDS_CHANGES),0,0);
	m_CB_Changes.Create(olTmp, WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON | BS_OWNERDRAW, olWndPlace.rcNormalPosition, &omDialogBar, IDC_CHANGES);
    m_CB_Changes.SetFont(polButton->GetFont());
	m_CB_Changes.EnableWindow(true);
	m_CB_Changes.SetSecState(ogPrivList.GetStat("DESKTOP_CB_History")); 


    omViewer.Attach(this);
	UpdateComboBox();

    CRect olRect; GetClientRect(&olRect);
	CTime olCurrentTime;
	olCurrentTime = CTime::GetCurrentTime();
	ogBasicData.LocalToUtc(olCurrentTime);

	// CBitmapButton

    omTSStartTime = olCurrentTime;// - CTimeSpan(0, 1, 0, 0);
    omStartTime = CTime(omTSStartTime.GetYear(), omTSStartTime.GetMonth(), omTSStartTime.GetDay(),
        0, 0, 0);// - CTimeSpan(1, 0, 0, 0);

    omDuration = CTimeSpan(3, 0, 0, 0);
    omTSDuration = CTimeSpan(0, 6, 0, 0);
    omTSInterval = CTimeSpan(0, 0, 10, 0);

	//Die wollen local
    CTime olUCT = CTime::GetCurrentTime();//olCurrentTime;
    CTime olRealUTC = olCurrentTime;
    char olBuf[64];
    sprintf(olBuf, "%02d%02d/%02d%02dz",
        olUCT.GetHour(), olUCT.GetMinute(),        
        olRealUTC.GetHour(), olRealUTC.GetMinute()
    );

    
	
	CTime olLocalTime = CTime::GetCurrentTime();
	CTime olUtcTime = CTime::GetCurrentTime();
	ogBasicData.LocalToUtc(olUtcTime);

	char pclTimes[36];
	char pclDate[36];
	
	CTime olUtcTimeTmp = CTime::GetCurrentTime();
	olUtcTimeTmp -= ogUtcDiff;
	
	if(ogUtcDiff != 0)
	{
		sprintf(pclDate, "%02d.%02d.%02d z", olUtcTimeTmp.GetDay(), olUtcTimeTmp.GetMonth(), olUtcTimeTmp.GetYear() % 100);
		sprintf(pclTimes, "%02d:%02d / %02d:%02d z", olLocalTime.GetHour(), olLocalTime.GetMinute(), olUtcTimeTmp.GetHour(), olUtcTimeTmp.GetMinute());
	}
	else
	{
		sprintf(pclDate, "%02d.%02d.%02d z", olUtcTime.GetDay(), olUtcTime.GetMonth(), olUtcTime.GetYear() % 100);
		sprintf(pclTimes, "%02d:%02d / %02d:%02d z", olLocalTime.GetHour(), olLocalTime.GetMinute(), olUtcTime.GetHour(), olUtcTime.GetMinute());
	}
    
	
	omTime.Create(pclTimes, SS_CENTER | WS_CHILD | WS_VISIBLE, CRect(olRect.right - 192, 3, olRect.right - 90, 22), this);

    omDate.Create(pclDate, SS_CENTER | WS_CHILD | WS_VISIBLE,  CRect(olRect.right - 88, 3, olRect.right - 8, 22), this);
        
    omDate.SetTextColor(RGB(255,0,0));
	
	omTime.ShowWindow(SW_HIDE);
	omDate.ShowWindow(SW_HIDE);
	

    sprintf(olBuf, "%02d%02d%02d  %d",  omTSStartTime.GetDay(), omTSStartTime.GetMonth(), omTSStartTime.GetYear() % 100, GetDayOfWeek(omTSStartTime));
    int ilPos = (imStartTimeScalePos - olRect.left - 80) / 2;
    omTSDate.Create(olBuf, SS_CENTER | WS_CHILD | WS_VISIBLE,
        CRect(ilPos, 35, ilPos + 80, 52), this);

    // CBitmapButton
    omBB1.Create("PREV", BS_OWNERDRAW | WS_CHILD | WS_VISIBLE,
        CRect(olRect.right - 36, 21 + 5, olRect.right - 19, (21 + 5) + 25), this, IDC_PREV);
    omBB1.LoadBitmaps("PREVU", "PREVD", NULL, "NEXTX");

    omBB2.Create("NEXT", BS_OWNERDRAW | WS_CHILD | WS_VISIBLE,
        CRect(olRect.right - 19, 21 + 5, olRect.right - 2, (21 + 5) + 25), this, IDC_NEXT);
    omBB2.LoadBitmaps("NEXTU", "NEXTD", NULL, "NEXTX");

	if (!bgDiaGrouping)
	{
		omBB1.ShowWindow(SW_HIDE);
		omBB2.ShowWindow(SW_HIDE);
	}

	omTimeScale.lmBkColor = lgBkColor;
    omTimeScale.Create(NULL, "TimeScale", WS_CHILD | WS_VISIBLE,
        CRect(imStartTimeScalePos, 28, olRect.right - (36 + 2), 62), this, 0, NULL);
//rkr
	//Set begin to local if local
	CTime olTSStartTime = omTSStartTime;
	if(bgGatPosLocal)
		ogBasicData.UtcToLocal(olTSStartTime);
    
	omTimeScale.SetDisplayTimeFrame(olTSStartTime, omTSDuration, omTSInterval);
//	omTimeScale.SetDisplayTimeFrame(omTSStartTime, omTSDuration, omTSInterval);
	if(bgGatPosLocal)
		omTimeScale.UpdateCurrentTimeLine(olLocalTime);
	else
		omTimeScale.UpdateCurrentTimeLine(olRealUTC);
     
    // must be static
    static UINT ilIndicators[] = { ID_SEPARATOR };
    omStatusBar.Create(this, CBRS_BOTTOM | WS_CHILD | WS_VISIBLE,0);
    omStatusBar.SetIndicators(ilIndicators, sizeof(ilIndicators) / sizeof(UINT));

    UINT nID, nStyle; int cxWidth;
    omStatusBar.GetPaneInfo(0, nID, nStyle, cxWidth);
    omStatusBar.SetPaneInfo(0, nID, SBPS_STRETCH | SBPS_NORMAL, cxWidth);

    olRect.top += 64;                   // for Dialog Bar & TimeScale
    olRect.bottom -= 20;                // for Horizontal Scroll Bar
    omClientWnd.Create(NULL, "ClientWnd", WS_CHILD /*| WS_HSCROLL */ | WS_VISIBLE, olRect, this,
        0 /* IDD_CLIENTWND */, NULL);
        
	// This will fix the bug for the horizontal scroll bar in the preplan mode
    long llTSMin = (omTSStartTime - omStartTime).GetTotalMinutes();
    long llTotalMin = omDuration.GetTotalMinutes() - omTSDuration.GetTotalMinutes();
    SetScrollRange(SB_HORZ, 0, 1000, FALSE);
    SetScrollPos(SB_HORZ, (int) (1000 * llTSMin / llTotalMin), FALSE);

    CRect olPrevRect;
    omClientWnd.GetClientRect(&olRect);
    
    imFirstVisibleChart = 0;
	
	
	// Grouping
	
    int ilLastY = 0;
    olRect.SetRect(olRect.left, ilLastY, olRect.right, ilLastY);
    CcaChart *polChart;
 	int ilCount = omViewer.GetGroupCount();
	
	if (!bgDiaGrouping) 
		ilCount = 1;
	
	
   for (int ilI = 0; ilI < ilCount; ilI++)
    {
        olRect.SetRect(olRect.left, ilLastY, olRect.right, ilLastY);
        
        polChart = new CcaChart;
        polChart->SetTimeScale(&omTimeScale);
        polChart->SetViewer(&omViewer, ilI);
        polChart->SetStatusBar(&omStatusBar);
        polChart->SetStartTime(omTSStartTime);
        polChart->SetInterval(omTimeScale.GetDisplayDuration());


		if(bgFWRGantt )
		{
			if(ilI == 0)
				polChart->Create(NULL, "CcaChart", WS_OVERLAPPED | WS_THICKFRAME  | WS_CHILD | WS_VISIBLE, olRect, &omClientWnd, 0 /* IDD_CHART */, NULL);
			else
				polChart->Create(NULL, "CcaChart", WS_OVERLAPPED | WS_BORDER  | WS_CHILD | WS_VISIBLE, olRect, &omClientWnd, 0 /* IDD_CHART */, NULL);
		}
		else
			polChart->Create(NULL, "CcaChart", WS_OVERLAPPED | WS_BORDER | WS_CHILD | WS_VISIBLE, olRect, &omClientWnd,  0 /* IDD_CHART */, NULL);
        
        omChartArray.Add(polChart);
		
        ilLastY += polChart->GetHeight();
    }
    
	
    

    OnTimer(0);

	// Register DDX call back function
	//TRACE("CcaDiagram: DDX Registration\n");
	ogDdx.Register(this, REDISPLAY_ALL, CString("STAFFDIAGRAM"),
		CString("Redisplay all from What-If"), CcaDiagramCf);	// for what-if changes
	ogDdx.Register(this, STAFFDIAGRAM_UPDATETIMEBAND, CString("STAFFDIAGRAM"),
		CString("Update Time Band"), CcaDiagramCf);	// for updating the yellow lines

 	ogDdx.Register(this, CCA_VIEW_CHANGED,CString(" "), CString("DATA_RELOAD"),CcaDiagramCf);
 	ogDdx.Register(this, CCA_GET_CUR_VIEW,CString(" "), CString("DATA_RELOAD"),CcaDiagramCf);

  
	// SEC

   CButton *polCB;

	polCB = (CButton *) omDialogBar.GetDlgItem(IDC_ALLOCATE);
	SetpWndStatAll(ogPrivList.GetStat("CCADIAGRAMM_CB_Allocate"),polCB);
	#ifdef _FIPSVIEWER
	{
		SetpWndStatAll('0',polCB);
	}
	#else
	{
	}
	#endif


	polCB = (CButton *) omDialogBar.GetDlgItem(IDC_PRINT_GANTT);
	SetpWndStatAll(ogPrivList.GetStat("CCADIAGRAMM_CB_Print"),polCB);

	polCB = (CButton *) omDialogBar.GetDlgItem(IDC_SEARCH);
	SetpWndStatAll(ogPrivList.GetStat("CCADIAGRAMM_CB_Search"),polCB);


	polCB = (CButton *) omDialogBar.GetDlgItem(IDC_DELETE_TIMEFRAME);
	SetpWndStatAll(ogPrivList.GetStat("CCADIAGRAMM_CB_Delete"),polCB);
	#ifdef _FIPSVIEWER
	{
		SetpWndStatAll('0',polCB);
	}
	#else
	{
	}
	#endif

/*
	polCB = (CButton *) omDialogBar.GetDlgItem(IDC_FLIGHTS_WITHOUT_DEM);
	SetpWndStatAll(ogPrivList.GetStat("CCADIAGRAMM_CB_WODemands"),polCB);


	polCB = (CButton *) omDialogBar.GetDlgItem(IDC_CIC_CONF);
	SetpWndStatAll(ogPrivList.GetStat("CCADIAGRAMM_CB_Conflicts"),polCB);
*/

	polCB = (CButton *) omDialogBar.GetDlgItem(IDC_COMMON_CCA);
	SetpWndStatAll(ogPrivList.GetStat("CCADIAGRAMM_CB_CommonCki"),polCB);
	#ifdef _FIPSVIEWER
	{
		SetpWndStatAll('0',polCB);
	}
	#else
	{
	}
	#endif

	polCB = (CButton *) omDialogBar.GetDlgItem(IDC_EXPAND);
	SetpWndStatAll(ogPrivList.GetStat("CCADIAGRAMM_CB_Expand"),polCB);

	#ifdef _FIPSVIEWER
	{
		polCB->EnableWindow(false);
	}
	#else
	{
	}
	#endif

	polCB = (CButton *) omDialogBar.GetDlgItem(IDC_CHANGESCENARIO);
	SetpWndStatAll(ogPrivList.GetStat("CCADIAGRAMM_SCENARIO_COMBO"),polCB);

	
	polCB = (CButton *) omDialogBar.GetDlgItem(IDC_SCENARIO);
	SetpWndStatAll(ogPrivList.GetStat("CCADIAGRAMM_SCENARIO"),polCB);


	polCB = (CButton *) omDialogBar.GetDlgItem(IDC_ANSICHT);
	SetpWndStatAll(ogPrivList.GetStat("CCADIAGRAMM_CB_Ansicht"),polCB);

	if (!bgOffRelFuncCheckin)
	{
		m_CB_Offline.ShowWindow(SW_HIDE);
	}

	//overviewbutton	
	polButton = (CButton *)omDialogBar.GetDlgItem(IDC_CCAOVERVIEW);	
	if (polButton && !bgAllocationOverview)
	{
		polButton->EnableWindow(FALSE);
		polButton->ShowWindow(SW_HIDE);
	}


	ogCcaDiaFlightData.Register();	 

	//SetWndPos();	

	olRect = CRect(0, 95, ::GetSystemMetrics(SM_CXSCREEN), ::GetSystemMetrics(SM_CYSCREEN));
    CRect olTSRect(imStartTimeScalePos, 28, olRect.right - (36 + 2), 62);
    omTimeScale.MoveWindow(&olTSRect, TRUE);

	if (bgNewGantDefault)
	{
		char pclView[100];
		strcpy(pclView, "<Default>");
		ogDdx.DataChanged((void *)this, CCA_GET_CUR_VIEW,(void *)pclView );
		omViewer.SelectView(pclView);
		
		
				
		omTSDuration = omViewer.GetGeometryTimeSpan();
		olTSStartTime = omTSStartTime;
		if(bgGatPosLocal)
			ogBasicData.UtcToLocal(olTSStartTime);
		omTimeScale.SetDisplayTimeFrame(olTSStartTime, omTSDuration, omTSInterval);
		omTimeScale.Invalidate(TRUE);
		ViewSelChange(pclView);
		//ChangeViewTo(omViewer.GetViewName(), false);
	}

	// set caption
	CString olTimes;
	if (bgGatPosLocal)
		olTimes = GetString(IDS_STRING1921);
	else
		olTimes = GetString(IDS_STRING1920);
	CString olCaption = GetString(IDS_STRING1012);
	olCaption += " ("+olTimes+")";
	SetWindowText(olCaption);


    // TSC
	LoadDataCBO();

	olRect = CRect(0, 95, ::GetSystemMetrics(SM_CXSCREEN), ::GetSystemMetrics(SM_CYSCREEN));
	GetDialogFromReg(olRect, m_key);
	//SetWindowPos(&wndTop, olRect.left, olRect.top, olRect.Width(), olRect.Height(), SWP_SHOWWINDOW);// | SWP_NOMOVE );
	MoveWindow(olRect);
	SetFocusToDiagram(); //PRF 8363
	return 0;

}

void CcaDiagram::SaveToReg() 
{
	BOOL ok = WriteDialogToReg((CWnd*) this, m_key);
}


void CcaDiagram::OnBeenden() 
{
	ogCcaDiaFlightData.UnRegister();	 
	ogCcaDiaFlightData.ClearAll();	 
	DestroyWindow();	
}


void CcaDiagram::OnInsert()
{
	RotationISFDlg dlg;
	dlg.DoModal();
}

void CcaDiagram::OnSearch()
{
	pogFlightSearchTableDlg->SetUrnoFilter( &ogCcaDiaFlightData.omUrnoMap);
	pogFlightSearchTableDlg->Activate(omViewer.GetBaseViewName(), omViewer.GetViewName(), CCADIA);
}

bool CcaDiagram::ShowFlight(long lpUrno)
{
	CCAFLIGHTDATA *prlFlight = ogCcaDiaFlightData.GetFlightByUrno(lpUrno);
	
	if (prlFlight == NULL)
		return false;

	//	pomChart->omGantt.SetTopIndex(omViewer.AdjustBar(NULL, prlFlight));
	int ilChartNo;
	int index = omViewer.AdjustBar(prlFlight, NULL, ilChartNo);
	if (index != -1) 
	{
		CcaChart *polChart = (CcaChart *) omChartArray[ilChartNo];
		polChart->omGantt.SetTopIndex(index);
	}
	

	return true;
}

void CcaDiagram::OnAnsicht()
{

	//MWO
	SetFocus();
	//END MWO

	CcaDiaPropertySheet olDlg("CCADIA", this, &omViewer, 0, GetString(IDS_STRING1646));
	bmIsViewOpen = true;
	if(olDlg.DoModal() != IDCANCEL)
	{
		TRACE("\nBegin OnAnsicht:  %s", CTime::GetCurrentTime().Format("%H:%M:%S"));

		if(ogCcaDiaFlightData.bmOffLine)
		{
			ogCcaDiaFlightData.bmOffLine = false;
			ogCcaDiaFlightData.omCcaData.bmOffLine = false;
			ogCcaDiaFlightData.UpdateOfflineButtons();
		}

		omViewer.SelectView(omViewer.GetViewName());

		ogSpotAllocation.InitializePositionMatrix();

		if (olDlg.FilterChanged())
		{
			bmLoadFlights = true;
			//LoadFlights();
		}
		else
		{
			// upate status bar
			omStatusBar.SetPaneText(0, GetString(IDS_SAS_CHECK));
			omStatusBar.UpdateWindow();

			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			ogKonflikte.SASCheckAll();
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));

			// upate status bar
			omStatusBar.SetPaneText(0, "");
			omStatusBar.UpdateWindow();
		}

		/*
		
 		omViewer.MakeMasstab();
		CTime olT = omViewer.GetGeometrieStartTime();

		CTime olFrom;
		CTime olTo;
		CString olFtyps;
		omViewer.GetTimeSpanAndFtyp(olFrom, olTo, olFtyps);


		CString olS = olT.Format("%d.%m.%Y-%H:%M");

		SetTSStartTime(olT);
		omTSDuration = omViewer.GetGeometryTimeSpan();
//rkr
		//Set begin to local if local
		CTime olTSStartTime = omTSStartTime;
		if(bgGatPosLocal)
			ogBasicData.UtcToLocal(olTSStartTime);
    
		omTimeScale.SetDisplayTimeFrame(olTSStartTime, omTSDuration, omTSInterval);
//		omTimeScale.SetDisplayTimeFrame(omTSStartTime, omTSDuration, omTSInterval);
        omTimeScale.Invalidate(TRUE);
		TRACE("\nBefin Activate Tables:  %s", CTime::GetCurrentTime().Format("%H:%M:%S"));
		ActivateTables();
		TRACE("\nEnd Activate Tables:  %s", CTime::GetCurrentTime().Format("%H:%M:%S"));
		ChangeViewTo(omViewer.GetViewName(), false);
		long llTotalMin = omDuration.GetTotalMinutes() - omTSDuration.GetTotalMinutes();
		long nPos = (omTSStartTime - omStartTime).GetTotalMinutes();
		nPos = nPos * 1000L / llTotalMin;
		SetScrollPos(SB_HORZ, int(nPos), TRUE);
//YYY		ActivateTables();

		*/
		char pclView[100];
		strcpy(pclView, omViewer.GetViewName());
		ogDdx.DataChanged((void *)this, CCA_VIEW_CHANGED,(void *)pclView );


	}
	bmIsViewOpen = false;

	bmLoadFlights = false;
	
	UpdateComboBox();
	SetFocusToDiagram() ; //PRF 8363
}

bool CcaDiagram::IsPassFlightFilter(CCAFLIGHTDATA *prpFlight)
{
	bool blRet = false;
	{
		/*

		if(strcmp(prpFlight->Htyp, "K") != 0)
		{
			if(strcmp(prpFlight->Ttyp, "CG") != 0)
			{
				if(strcmp(prpFlight->Flns, "F") != 0)
				{
					if(strcmp(prpFlight->Htyp, "T") != 0)
					{
						blRet = true;
					}
				}
			}
		}
		*/
	}
	return blRet;
}

bool CcaDiagram::HasAllocatedCcas(CCSPtrArray<DIACCADATA> &ropCcaList)
{
	bool blRet = false;
	int ilCount = ropCcaList.GetSize();
	for(int i = 0; ((i < ilCount) && (blRet == false)); i++)
	{
	}
	return blRet;
}


bool CcaDiagram::LoadFlights(char *pspView)
{
	bmReadAll = true;
	CString olView;

	if(pspView != NULL)
	{
		omViewer.SelectView(pspView);
		olView = CString(pspView);
	}
	else
	{
		omViewer.SelectView(omViewer.GetViewName());
		olView = CString(omViewer.GetViewName());
	}
	
	CString olWhere;
	CTime olFrom;
	CTime olTo;
	CString olFtyps;
	int ilDOO;
	bool blRotation;



	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
	
	if(bmLoadFlights)
		omStatusBar.SetPaneText(0, GetString(IDS_STRING1205));
	
	omStatusBar.UpdateWindow();
	
	ogDataSet.InitCcaConflictStrings();


	omViewer.GetTimeSpanAndFtyp(olFrom, olTo, olFtyps);
	ogCcaDiaFlightData.SetPreSelection(olFrom, olTo, olFtyps);


	TRACE("\nBegin Load Flights:  %s", CTime::GetCurrentTime().Format("%H:%M:%S"));
	TRACE("olFrom: %s,   olTo: %s\n", olFrom.Format("%d.%m.%Y %H:%M:%S"), olTo.Format("%d.%m.%Y %H:%M:%S"));


	//now button
	CButton *polCB1 = (CButton *) omDialogBar.GetDlgItem(IDC_ZEIT);
	CTime olCurr;
	CTime olT1 = olFrom;
	CTime olT2 = olTo;
	GetCurrentUtcTime(olCurr);

	if (bgGatPosLocal)
	{
		ogBasicData.LocalToUtc(olT1);
		ogBasicData.LocalToUtc(olT2);
	}
/*
	CString olf=olT1.Format("%d.%m.%Y %H:%M:%S");
	CString olt=olT2.Format("%d.%m.%Y %H:%M:%S");
	CString olC=olCurr.Format("%d.%m.%Y %H:%M:%S");
*/
	if(olT1 < olCurr && olT2 > olCurr)
	{
		polCB1->EnableWindow(TRUE);
		bmTimeLine = true;
	}
	else
	{
		polCB1->EnableWindow(FALSE);
		bmTimeLine = false;
	}


	// set duration and starttime
	omDuration = (olTo - olFrom) + CTimeSpan(1, 0, 0, 0);
//	if (bgGatPosLocal)
//		ogBasicData.UtcToLocal(olFrom);
	omStartTime = olFrom - CTimeSpan(1, 0, 0, 0);

/*

	CTime olCurr = CTime::GetCurrentTime();
	if(bgGatPosLocal) ogBasicData.LocalToUtc(olCurr);


	CButton *polCB1 = (CButton *) omDialogBar.GetDlgItem(IDC_ZEIT);

	if(olFrom < olCurr && olTo > olCurr)
	{
		polCB1->EnableWindow(TRUE);
	}
	else
	{
		polCB1->EnableWindow(FALSE);
	}
*/

	ilDOO = omViewer.GetDOO();

	char pclSelection[7000] = "";



	TRACE("\n-----------------------------------------------");
	TRACE("\n-----------------------------------------------");
	TRACE("\n-----------------------------------------------");
	TRACE("\n-----------------------------------------------");
	TRACE("\n-----------------------------------------------");
	TRACE("\nBegin read:  %s", CTime::GetCurrentTime().Format("%H:%M:%S"));


	if (!omViewer.GetZeitraumWhereString(olWhere, blRotation, CString("") ))
		bmLoadFlights = false;
		//return false;

	//olWhere += CString(" AND FLNO > ' '");
	// PRF 8382
	////Forms the query for the Genral filter page.
	CString  olstring = omViewer.GetUnifilterWhereString();
	olstring.TrimLeft();
	olstring.TrimRight();
	if(!olstring.IsEmpty())
	{
		olWhere = olWhere + olstring;	
	}

	ViewerFilter olFilter;
	omViewer.GetViewerFilter(olFilter);
	CMapStringToString olFilterMap;
	CString olAlc;

	if(!olFilter.omAlc.IsEmpty())
	{
		bool blok = ogBCD.GetField("ALT", "ALC2", olFilter.omAlc, "URNO", olAlc);
		if(blok == false)
			blok = ogBCD.GetField("ALT", "ALC3", olFilter.omAlc, "URNO", olAlc);

		if (blok)
			olFilterMap.SetAt("FLNU",olAlc);
	}


//	ogCcaDiaFlightData.ReadAllFlights(olWhere, blRotation);
	if(bmLoadFlights)
		ogCcaDiaFlightData.ReadAllFlights(olWhere, blRotation, olFilterMap);

	TRACE("\nEnd read:  %s", CTime::GetCurrentTime().Format("%H:%M:%S"));
	TRACE("\nBegin read BLK:  %s", CTime::GetCurrentTime().Format("%H:%M:%S"));


	CString olText = CString("Blk... ");
	omStatusBar.SetPaneText(0, olText);
	omStatusBar.UpdateWindow();

	char pclWhere[512]="";
	
	sprintf(pclWhere, " WHERE (NATO >= '%s' and NAFR <= '%s') OR (NAFR <= '%s' and NATO = ' ') OR  (NATO >= '%s'  and NAFR = ' ')",
												 olFrom.Format("%Y%m%d%H%M00"), olTo.Format("%Y%m%d%H%M59"),
												 olTo.Format("%Y%m%d%H%M59"),
												 olFrom.Format("%Y%m%d%H%M00"));
	ogBCD.Read(CString("BLK"), pclWhere);


	TRACE("\nEnd read BLK:    %s", CTime::GetCurrentTime().Format("%H:%M:%S"));

	CString olCnams = ogCcaDiaFlightData.omCcaData.MakeBlkData(TIMENULL, ilDOO);

	TRACE("\nEnd Make BLK:    %s", CTime::GetCurrentTime().Format("%H:%M:%S"));

	TRACE("\n-----------------------------------------------");
	TRACE("\n-----------------------------------------------");

	omViewer.SetCnams(olCnams);
	omViewer.SetDOO(ilDOO);

	//int ilC5 = ogBCD.GetDataCount("BLK");


	//int ilBnam = ogBCD.GetFieldIndex("CIC", "CNAM");
	//ogBCD.SetSort("CIC", "TERM+,CNAM+", true); //true ==> sort immediately



//----------------------------------------------------------------------------------------------------
// MWO wenn Bernis ResHdl so weit ist, dann kann der nachfolgende Teil wieder raus bis END MWO

	TRACE("\nBeginn Demands:  %s", CTime::GetCurrentTime().Format("%H:%M:%S"));

	CCAFLIGHTDATA *prlFlight;
	POSITION pos;
	void *pVoid;
	int ilCurrentFlight=1;
	char pclPaneText[512]="";
	int ilFltAmount = ogCcaDiaFlightData.omUrnoMap.GetCount();

	//int ilAuskommentiertfuerTest;

	CCSPtrArray<DIACCADATA> olCcaChangedList;


	for( pos = ogCcaDiaFlightData.omUrnoMap.GetStartPosition(); pos != NULL; )
	{
		ogCcaDiaFlightData.omUrnoMap.GetNextAssoc( pos, pVoid , (void *&)prlFlight );
		sprintf(pclPaneText, GetString(IDS_STRING1490), ilCurrentFlight, ilFltAmount); // Regelwerk anwenden: Flug %d von %d ..........................
		omStatusBar.SetPaneText(0, pclPaneText);
		omStatusBar.UpdateWindow();
		ilCurrentFlight++;

		ogDataSet.CheckDemands(prlFlight, olCcaChangedList);
	}

	omStatusBar.SetPaneText(0, GetString(IDS_STRING1491)); // Speichern der Bedarfe..........................................
	omStatusBar.UpdateWindow();

	TRACE("\nEnd Demands:     %s", CTime::GetCurrentTime().Format("%H:%M:%S"));
	TRACE("\n-----------------------------------------------");
	TRACE("\n-----------------------------------------------");

	TRACE("\nBegin Save:  %s", CTime::GetCurrentTime().Format("%H:%M:%S"));

	//int ilAuskommentiertfuerTest2;
	//ogCcaDiaFlightData.omCcaData.Release(&olCcaChangedList, true);

//YYY
//	ogCcaDiaFlightData.omCcaData.SaveInternal(&olCcaChangedList, true);
	ogCcaDiaFlightData.omCcaData.SaveInternal(&olCcaChangedList, false);
	olCcaChangedList.RemoveAll();

	TRACE("\nEnd Save:    %s", CTime::GetCurrentTime().Format("%H:%M:%S"));
	TRACE("\n-----------------------------------------------");
	TRACE("\n-----------------------------------------------");

	olText = CString("Load tables... ");
	omStatusBar.SetPaneText(0, olText);
	omStatusBar.UpdateWindow();

 
	//END MWO
//----------------------------------------------------------------------------------------------------
	
/*
	TRACE("\nBegin tables:  %s", CTime::GetCurrentTime().Format("%H:%M:%S"));
//YYY
//	ActivateTables();

	TRACE("\nEnd tables:    %s", CTime::GetCurrentTime().Format("%H:%M:%S"));
	TRACE("\n-----------------------------------------------");
	TRACE("\n-----------------------------------------------");

*/


	TRACE("\n ANZ FLIGHT: %d\n", ogCcaDiaFlightData.omData.GetSize());
	TRACE("\n ANZ CCA   : %d\n", ogCcaDiaFlightData.omCcaData.omData.GetSize());
	TRACE("\n-----------------------------------------------");
	TRACE("\n-----------------------------------------------");

/*
	int ilBlk = 0;
	int ilFlight = 0;
	DIACCADATA *prlCca;

	for(int g = ogCcaDiaFlightData.omCcaData.omData.GetSize() -1 ; g >= 0; g-- )
	{
		prlCca = &ogCcaDiaFlightData.omCcaData.omData[g];

		if(CString(prlCca->Ctyp) == "")
			ilFlight++;

		if(CString(prlCca->Ctyp) == "N")
			ilBlk++;

	}

	TRACE("\n ANZ FLIGHTCCA: %d\n", ilFlight);
	TRACE("\n ANZ BLKCCA   : %d\n", ilBlk);
	TRACE("\n-----------------------------------------------");
	TRACE("\n-----------------------------------------------");
*/
	olText = CString("Check overlapping... ");
	omStatusBar.SetPaneText(0, olText);
	omStatusBar.UpdateWindow();
	
	TRACE("\nBegin Overlap:  %s", CTime::GetCurrentTime().Format("%H:%M:%S"));

	int ilCount = ogBCD.GetDataCount("CIC");

	for(int i = 0; i < ilCount; i++)
	{
		ogDataSet.CheckCCaForOverlapping(ogBCD.GetField("CIC", i, "CNAM"));
	}
	TRACE("\nEnd Overlap:    %s", CTime::GetCurrentTime().Format("%H:%M:%S"));
	TRACE("\n-----------------------------------------------");
	TRACE("\n-----------------------------------------------");




	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	omStatusBar.SetPaneText(0, "");
	omStatusBar.UpdateWindow();

	bmReadAll = false;
	UpdateWoDemButton();
	UpdateWoResButton();
	UpdateConfButton();

	return true;

}

void CcaDiagram::OnDestroy() 
{
	SaveToReg();

//	if (bgModal == TRUE)
//		return;
	bmNoUpdatesNow = TRUE;
	omViewer.AllowUpdates(bmNoUpdatesNow);
	ogDdx.UnRegister(&omViewer, NOTUSED);

	if (pomCicKonflikteDlg)
	{
		pomCicKonflikteDlg->SaveToReg();
		delete pomCicKonflikteDlg;
		pomCicKonflikteDlg = NULL;
	}

	if (pomBltOverviewTableDlg)
	{
		pomCicKonflikteDlg->SaveToReg();
		delete pomBltOverviewTableDlg;
		pomBltOverviewTableDlg = NULL;
	}


	// Unregister DDX call back function
	TRACE("CcaDiagram: DDX Unregistration %s\n",
		::IsWindow(GetSafeHwnd())? "": "(window is already destroyed)");
    ogDdx.UnRegister(this, NOTUSED);

	CFrameWnd::OnDestroy();
}

void CcaDiagram::OnClose() 
{
    // Ignore close -- This makes Alt-F4 keys no effect
	CFrameWnd::OnClose();
}

void CcaDiagram::OnGetMinMaxInfo(MINMAXINFO FAR* lpMMI) 
{
    // TODO: Add your message handler code here and/or call default
    CFrameWnd::OnGetMinMaxInfo(lpMMI);

    lpMMI->ptMaxTrackSize = omMaxTrackSize;
    lpMMI->ptMinTrackSize = omMinTrackSize;
}

BOOL CcaDiagram::OnEraseBkgnd(CDC* pDC) 
{
    // TODO: Add your message handler code here and/or call default
    CRect olClipRect;
    pDC->GetClipBox(&olClipRect);
    //GetClientRect(&olClipRect);

    CBrush olBrush(SILVER/*lmBkColor*/);
    CBrush *polOldBrush = pDC->SelectObject(&olBrush);
    pDC->PatBlt(olClipRect.left, olClipRect.top,
        olClipRect.Width(), olClipRect.Height(),
        PATCOPY);
    pDC->SelectObject(polOldBrush);
    
    return TRUE;
    
    //return CFrameWnd::OnEraseBkgnd(pDC);
}

void CcaDiagram::OnPaint() 
{
    CPaintDC dc(this); // device context for painting
    
    // TODO: Add your message handler code here
    CRect olRect;
    GetClientRect(&olRect);
    
    // draw horizontal line
    CPen olHPen(PS_SOLID, 1, lmHilightColor);
    CPen *polOldPen = dc.SelectObject(&olHPen);
    dc.MoveTo(olRect.left, 27); dc.LineTo(olRect.right, 27);
    dc.MoveTo(olRect.left, 62); dc.LineTo(olRect.right, 62);

    CPen olTPen(PS_SOLID, 1, lmTextColor);
    dc.SelectObject(&olTPen);
    dc.MoveTo(olRect.left, 63); dc.LineTo(olRect.right, 63);
    
    
    // draw vertical line
    dc.SelectObject(&olTPen);
    dc.MoveTo(imStartTimeScalePos - 2, 27);
    dc.LineTo(imStartTimeScalePos - 2, 63);

    dc.SelectObject(&olHPen);
    dc.MoveTo(imStartTimeScalePos - 1, 27);
    dc.LineTo(imStartTimeScalePos - 1, 63);

    dc.SelectObject(polOldPen);
    // Do not call CFrameWnd::OnPaint() for painting messages
}


void CcaDiagram::OnSize(UINT nType, int cx, int cy) 
{
	bmOnSize = true;

    CFrameWnd::OnSize(nType, cx, cy);
    
    // TODO: Add your message handler code here
    CRect olRect; GetClientRect(&olRect);

    CRect olBB1Rect(olRect.right - 36, 27 + 5, olRect.right - 19, (27 + 5) + 25);
    omBB1.MoveWindow(&olBB1Rect, TRUE);
    
    CRect olBB2Rect(olRect.right - 19, 27 + 5, olRect.right - 2, (27 + 5) + 25);
    omBB2.MoveWindow(&olBB2Rect, TRUE);

    CRect olTSRect(imStartTimeScalePos, 28, olRect.right - (36 + 2), 62);
    omTimeScale.MoveWindow(&olTSRect, TRUE);

    olRect.top += 64;                   // for Dialog Bar & TimeScale
    olRect.bottom -= 20;                // for Horizontal Scroll Bar
    // LeftTop, RightBottom
    omClientWnd.MoveWindow(&olRect, TRUE);

	PositionChild();
	//ChangeViewTo(omViewer.SelectView());//ogCfgData.rmUserSetup.STCV);
//	SetAllStaffAreaButtonsColor();

	GetClientRect(&olRect);
	
	omTime.MoveWindow( CRect(olRect.right - 192, 3, olRect.right - 90, 22));

    omDate.MoveWindow( CRect(olRect.right - 88, 3, olRect.right - 8, 22));

	bmOnSize = false;

}

void CcaDiagram::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
{
    // TODO: Add your message handler code here and/or call default
    
    //CFrameWnd::OnHScroll(nSBCode, nPos, pScrollBar);


	CTime olTSStartTime = omTSStartTime;
	//	int ilCurrLine = pomChart->omGantt.GetTopIndex();
    long llTotalMin;
    int ilPos;
    
    switch (nSBCode)
    {
        case SB_LINEUP :
            //OutputDebugString("LineUp\n\r");
            
            llTotalMin = omDuration.GetTotalMinutes() - omTSDuration.GetTotalMinutes();            
            ilPos = GetScrollPos(SB_HORZ) - int (60 * 1000L / llTotalMin);
            if (ilPos <= 0)
            {
                ilPos = 0;
                SetTSStartTime(omStartTime);
            }
            else
                SetTSStartTime(omTSStartTime - CTimeSpan(0, 0, 60, 0));

			olTSStartTime = omTSStartTime;
			if(bgGatPosLocal)
				ogBasicData.UtcToLocal(olTSStartTime);

            omTimeScale.SetDisplayStartTime(olTSStartTime);
            omTimeScale.Invalidate(TRUE);
            SetScrollPos(SB_HORZ, ilPos, TRUE);
            omClientWnd.Invalidate(FALSE);
        break;
        
        case SB_LINEDOWN :
            //OutputDebugString("LineDown\n\r");

            llTotalMin = omDuration.GetTotalMinutes() - omTSDuration.GetTotalMinutes();            
            ilPos = GetScrollPos(SB_HORZ) + int (60 * 1000L / llTotalMin);
            if (ilPos >= 1000)
            {
                ilPos = 1000;
                SetTSStartTime(omStartTime + CTimeSpan(0, 0, int (llTotalMin+1), 0));
            }
            else
                SetTSStartTime(omTSStartTime + CTimeSpan(0, 0, 60, 0));

			olTSStartTime = omTSStartTime;
			if(bgGatPosLocal)
				ogBasicData.UtcToLocal(olTSStartTime);

            omTimeScale.SetDisplayStartTime(olTSStartTime);
            omTimeScale.Invalidate(TRUE);
			SetScrollPos(SB_HORZ, ilPos, TRUE);
            omClientWnd.Invalidate(FALSE);
        break;
        
        case SB_PAGEUP :
            //OutputDebugString("PageUp\n\r");
            
            llTotalMin = omDuration.GetTotalMinutes() - omTSDuration.GetTotalMinutes();            
            ilPos = GetScrollPos(SB_HORZ)
                - int ((omTSDuration.GetTotalMinutes() / 2) * 1000L / llTotalMin);
            if (ilPos <= 0)
            {
                ilPos = 0;
                SetTSStartTime(omStartTime);
            }
            else
                SetTSStartTime(omTSStartTime - CTimeSpan(0, 0, int (omTSDuration.GetTotalMinutes() / 2), 0));

			olTSStartTime = omTSStartTime;
			if(bgGatPosLocal)
				ogBasicData.UtcToLocal(olTSStartTime);

            omTimeScale.SetDisplayStartTime(olTSStartTime);
            omTimeScale.Invalidate(TRUE);
            SetScrollPos(SB_HORZ, ilPos, TRUE);
            omClientWnd.Invalidate(FALSE);
        break;
        
        case SB_PAGEDOWN :
            //OutputDebugString("PageDown\n\r");
            
            llTotalMin = omDuration.GetTotalMinutes() - omTSDuration.GetTotalMinutes();            
            ilPos = GetScrollPos(SB_HORZ)
                + int ((omTSDuration.GetTotalMinutes() / 2) * 1000L / llTotalMin);
            if (ilPos >= 1000)
            {
                ilPos = 1000;
                SetTSStartTime(omStartTime + CTimeSpan(0, 0, int (llTotalMin+1), 0));
            }
            else
                SetTSStartTime(omTSStartTime + CTimeSpan(0, 0, int (omTSDuration.GetTotalMinutes() / 2), 0));

			olTSStartTime = omTSStartTime;
			if(bgGatPosLocal)
				ogBasicData.UtcToLocal(olTSStartTime);

            omTimeScale.SetDisplayStartTime(olTSStartTime);
            omTimeScale.Invalidate(TRUE);
            SetScrollPos(SB_HORZ, ilPos, TRUE);
            omClientWnd.Invalidate(FALSE);
        break;
        
        case SB_THUMBTRACK /* pressed, any drag time */:
            //OutputDebugString("ThumbTrack\n\r");

            llTotalMin = omDuration.GetTotalMinutes() - omTSDuration.GetTotalMinutes();
            
            SetTSStartTime(omStartTime + CTimeSpan(0, 0, int (llTotalMin * nPos / 1000), 0));
            
			olTSStartTime = omTSStartTime;
			if(bgGatPosLocal)
				ogBasicData.UtcToLocal(olTSStartTime);

            omTimeScale.SetDisplayStartTime(olTSStartTime);
            omTimeScale.Invalidate(TRUE);
            SetScrollPos(SB_HORZ, nPos, TRUE);

			//TRACE("SartTime: [%s]\n", olTSStartTime.Format("%H:%M"));
			if(bgFWRGantt && omChartArray.GetSize() > 0)
			{
				CcaChart *polChart = (CcaChart *) omChartArray.GetAt(0);
				int ilLine = omViewer.GetLineOfFirstVisibleBar(0, olTSStartTime);

				if(ilLine != -1)
					polChart->omGantt.SetTopIndex(ilLine);

			}


            omClientWnd.Invalidate(FALSE);
        break;
        case SB_THUMBPOSITION:	// the thumb was just released?
            omClientWnd.Invalidate(FALSE);
        case SB_TOP :
        //break;
        case SB_BOTTOM :
            //OutputDebugString("TopBottom\n\r");
        break;
        
        case SB_ENDSCROLL :
            //OutputDebugString("EndScroll\n\r");
            //OutputDebugString("\n\r");
        break;
    }

	//	pomChart->omGantt.SetTopIndex(ilCurrLine);

}


void CcaDiagram::OnTimer(UINT nIDEvent)
{
	if (bmTimeLine == false)
		return;

	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));

	if (nIDEvent == 1)
	{
		CTime olCurr1 = CTime::GetCurrentTime();

		// update cic, current time is set in check overlapping
		int ilCount = ogBCD.GetDataCount("CIC");

		for(int i = 0; i < ilCount; i++)
		{
			ogDataSet.CheckCCaForOverlapping(ogBCD.GetField("CIC", i, "CNAM"));
		}

		ogCcaDiaFlightData.omCcaData.UpdateCommonForFlight(true);

		pogCcaDiagram->PostMessage(WM_REPAINT_ALL, 0, 0);

		CTime olCurr2 = CTime::GetCurrentTime();

		TRACE("CcaDiagram::OnTimer: Duration of FollowTimeLine-Calculation: %s\n", (olCurr2 - olCurr1).Format("%M:%S"));
	}


	//// Update time line
	if(bmNoUpdatesNow == FALSE)
	{
		CTime olLocalTime = CTime::GetCurrentTime();
		CTime olUtcTime = CTime::GetCurrentTime();
		ogBasicData.LocalToUtc(olUtcTime);

		char pclTimes[100];
		char pclDate[100];
		


		CTime olUtcTimeTmp = CTime::GetCurrentTime();
		olUtcTimeTmp -= ogUtcDiff;
		
		if(ogUtcDiff != 0)
		{
			sprintf(pclDate, "%02d.%02d.%02d z", olUtcTimeTmp.GetDay(), olUtcTimeTmp.GetMonth(), olUtcTimeTmp.GetYear() % 100);
			sprintf(pclTimes, "%02d:%02d / %02d:%02d z", olLocalTime.GetHour(), olLocalTime.GetMinute(), olUtcTimeTmp.GetHour(), olUtcTimeTmp.GetMinute());
		}
		else
		{
			sprintf(pclDate, "%02d.%02d.%02d z", olUtcTime.GetDay(), olUtcTime.GetMonth(), olUtcTime.GetYear() % 100);
			sprintf(pclTimes, "%02d:%02d / %02d:%02d z", olLocalTime.GetHour(), olLocalTime.GetMinute(), olUtcTime.GetHour(), olUtcTime.GetMinute());
		}

		omTime.SetWindowText(pclTimes);
		omTime.Invalidate(FALSE);

		omDate.SetWindowText(pclDate);
		omDate.Invalidate(FALSE);

		// Grouping
		if(bgGatPosLocal)
			omTimeScale.UpdateCurrentTimeLine(olLocalTime);
		else
			omTimeScale.UpdateCurrentTimeLine(olUtcTime);
		
		for (int ilChartNo = 0; ilChartNo < omChartArray.GetSize(); ilChartNo++)
		{
			if(bgGatPosLocal)
				((CcaChart *)omChartArray[ilChartNo])->GetGanttPtr()->SetCurrentTime(olLocalTime);
			else
				((CcaChart *)omChartArray[ilChartNo])->GetGanttPtr()->SetCurrentTime(olUtcTime);
		}
		// Grouping	end	
		

		CFrameWnd::OnTimer(nIDEvent);
	}

	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
}

void CcaDiagram::SetMarkTime(CTime opStartTime, CTime opEndTime)
{
	for (int ilChartNo = 0; ilChartNo < omChartArray.GetSize(); ilChartNo++)
	{
		((CcaChart *)omChartArray[ilChartNo])->SetMarkTime(opStartTime, opEndTime);
	}
}

void CcaDiagram::ShowCcaDuration(CTime opCkbs, CTime opCkes)
{
	CTime olTime = opCkbs - CTimeSpan(0, 1, 0, 0);
	ShowTime(olTime);
	SetMarkTime(opCkbs, opCkes);
}

void CcaDiagram::ShowTime(const CTime &ropTime) 
{
	SetTSStartTime(ropTime);
	//TRACE("Time: [%s]\n", olTime.Format("%H:%M"));

	CTime olTSStartTime(omTSStartTime);

	omTimeScale.SetDisplayStartTime(olTSStartTime);
    omTimeScale.Invalidate(TRUE);
		
	// update scroll bar position
    long llTotalMin = omDuration.GetTotalMinutes() - omTSDuration.GetTotalMinutes();
	long nPos = (omTSStartTime - omStartTime).GetTotalMinutes();
	nPos = nPos * 1000L / llTotalMin;
    SetScrollPos(SB_HORZ, int(nPos), TRUE);

	//ChangeViewTo(omViewer.SelectView());//ogCfgData.rmUserSetup.STCV);

	CRect olRect;
	omTimeScale.GetClientRect(&olRect);
	omViewer.ChangeViewTo(omViewer.SelectView(), omTSStartTime, omTimeScale.GetTimeFromX(olRect.Width() + 38));
	PositionChild();

    omClientWnd.Invalidate(FALSE);
}

LONG CcaDiagram::OnPositionChild(WPARAM wParam, LPARAM lParam)
{
    PositionChild();
    return 0L;
}

#if 0
LONG CcaDiagram::OnUpdateDiagram(WPARAM wParam, LPARAM lParam)
{
	UpdateWoResButton();
	UpdateWoDemButton();
	UpdateConfButton();

    CString olStr;

    int ilGroupNo = HIWORD(lParam);
    int ilLineNo = LOWORD(lParam);

	int ilItemID = omViewer.GetItemID(ilGroupNo, ilLineNo);

	CTime olTime = CTime((time_t) lParam);

	char clBuf[255];
	int ilCount;

    CcaGantt *polCcaGantt = pomChart -> GetGanttPtr();
    switch (wParam)
    {
        // group message

        case UD_UPDATEGROUP :
            olStr = omViewer.GetGroupText(ilGroupNo);
            pomChart->GetChartButtonPtr()->SetWindowText(olStr);
            pomChart->GetChartButtonPtr()->Invalidate(FALSE);

            olStr = omViewer.GetGroupTopScaleText(ilGroupNo);
            pomChart->GetTopScaleTextPtr()->SetWindowText(olStr);
            pomChart->GetTopScaleTextPtr()->Invalidate(TRUE);
			//SetStaffAreaButtonColor(ilGroupNo);
        break;
        

        // line message
        case UD_INSERTLINE :
            polCcaGantt->InsertString(ilItemID, "");
			polCcaGantt->RepaintItemHeight(ilItemID);
			
			ilCount = omViewer.GetLineCount(ilGroupNo);
			sprintf(clBuf, "%d", ilCount);
			pomChart->GetCountTextPtr()->SetWindowText(clBuf);
			pomChart->GetCountTextPtr()->Invalidate(TRUE);

			//SetStaffAreaButtonColor(ilGroupNo);
            PositionChild();
        break;
        
        case UD_UPDATELINE :
            polCcaGantt->RepaintVerticalScale(ilItemID);
            polCcaGantt->RepaintGanttChart(ilItemID);
			//SetStaffAreaButtonColor(ilGroupNo);
        break;

        case UD_DELETELINE :
            polCcaGantt->DeleteString(ilItemID);
            
			ilCount = omViewer.GetLineCount(ilGroupNo);
			sprintf(clBuf, "%d", ilCount);
			pomChart->GetCountTextPtr()->SetWindowText(clBuf);
			pomChart->GetCountTextPtr()->Invalidate(TRUE);

			//SetStaffAreaButtonColor(ilGroupNo);
			PositionChild();
        break;

        case UD_UPDATELINEHEIGHT :
            polCcaGantt->RepaintItemHeight(ilItemID);
			//SetStaffAreaButtonColor(ilGroupNo);
            PositionChild();
		break;
    }

    return 0L;
}
#endif

// Grouping
LONG CcaDiagram::OnUpdateDiagram(WPARAM wParam, LPARAM lParam)
{
	UpdateWoResButton();
	UpdateWoDemButton();
	UpdateConfButton();

    CString olStr;
    int ipGroupNo = HIWORD(lParam);
    int ipLineNo = LOWORD(lParam);
	CTime olTime = CTime((time_t) lParam);
	
    CcaChart *polChart = NULL;
	if (bgDiaGrouping) 
	{
		if(ipGroupNo < omChartArray.GetSize())
			polChart = (CcaChart *) omChartArray.GetAt(ipGroupNo);
	} 
	else 
	{
		polChart = (CcaChart *) omChartArray.GetAt(0);
		if (ipGroupNo > 0) {
			ipLineNo = omViewer.GetAbsoluteLine(ipGroupNo,ipLineNo);
		}
	}

	if(polChart == NULL)
	{
		return 0L;
	}
    CcaGantt *polGantt = polChart -> GetGanttPtr();
	if(polGantt == NULL)
	{
		return 0L;
	}
	
    switch (wParam)
    {
        // group message
	case UD_INSERTGROUP :
		// CreateChild(ipGroupNo);
	if (bgDiaGrouping) {	
		if (ipGroupNo <= imFirstVisibleChart)
		{
			imFirstVisibleChart++;
			OnUpdatePrevNext();
		}
		PositionChild();
	}
        break;
		
	case UD_UPDATEGROUP :
		if (bgDiaGrouping) {
		olStr = omViewer.GetGroupText(ipGroupNo);
		polChart->GetChartButtonPtr()->SetWindowText(olStr);
		polChart->GetChartButtonPtr()->Invalidate(FALSE);
		
		olStr = omViewer.GetGroupTopScaleText(ipGroupNo);
		polChart->GetTopScaleTextPtr()->SetWindowText(olStr);
		polChart->GetTopScaleTextPtr()->Invalidate(TRUE);
		SetPosAreaButtonColor(ipGroupNo);
		}
        break;
        
	case UD_DELETEGROUP :
		if (bgDiaGrouping) {
		delete omChartArray.GetAt(ipGroupNo);
		//omChartArray.GetAt(ipGroupNo) -> DestroyWindow();
		omChartArray.RemoveAt(ipGroupNo);
		
		if (ipGroupNo <= imFirstVisibleChart)
		{
			imFirstVisibleChart--;
			OnUpdatePrevNext();
		}
		PositionChild();
		}
        break;
		
        // line message
	case UD_INSERTLINE :
		polGantt->InsertString(ipLineNo, "");
		polGantt->RepaintItemHeight(ipLineNo);
		SetPosAreaButtonColor(ipGroupNo);
		PositionChild();
        break;
        
	case UD_UPDATELINE :
		/*
		polGantt->RepaintVerticalScale(ipLineNo);
		polGantt->RepaintGanttChart(ipLineNo);
		SetPosAreaButtonColor(ipGroupNo);
		*/
		polGantt->RepaintVerticalScale(ipLineNo);
		if (polGantt->GetItemHeight(ipLineNo) != polGantt->GetLineHeight(ipLineNo))
		{
			polGantt->RepaintItemHeight(ipLineNo);
			PositionChild();
		}
		else
		{
			polGantt->RepaintGanttChart(ipLineNo);
		}
		SetPosAreaButtonColor(ipGroupNo);

        break;
		
	case UD_DELETELINE :
		polGantt->DeleteString(ipLineNo);
		SetPosAreaButtonColor(ipGroupNo);
		PositionChild();
        break;
		
	case UD_UPDATELINEHEIGHT :
		polGantt->RepaintItemHeight(ipLineNo);
		SetPosAreaButtonColor(ipGroupNo);
		PositionChild();
        break;
		
    }
	
    return 0L;
}




///////////////////////////////////////////////////////////////////////////////
// Damkerng and Pichet: Date: 5 July 1995

void CcaDiagram::UpdateComboBox()
{
	CComboBox *polCB = (CComboBox *) omDialogBar.GetDlgItem(IDC_VIEW);
	polCB->ResetContent();
	CStringArray olStrArr;
	omViewer.GetViews(olStrArr);
	for (int ilIndex = 0; ilIndex < olStrArr.GetSize(); ilIndex++)
	{
		polCB->AddString(olStrArr[ilIndex]);
	}
	CString olViewName = omViewer.GetViewName();
	if (olViewName.IsEmpty() == TRUE)
	{

		olViewName = omViewer.SelectView();//ogCfgData.rmUserSetup.STCV;
	}

	ilIndex = polCB->FindString(-1,olViewName);
		
	if (ilIndex != CB_ERR)
	{
		polCB->SetCurSel(ilIndex);
	}
}


// TSC 2009
void CcaDiagram::LoadDataCBO()
{

	if(!bgscenario_checkincounter) // CEDA
	{
		 omDialogBar.GetDlgItem(IDC_SCENARIO)			->ShowWindow(false);
		 omDialogBar.GetDlgItem(IDC_CHANGESCENARIO)		->ShowWindow(false);
		 oguseralloctepath="<Default>";
	
	}
	
	else
	{
	vCBOCICData.clear();
	
		// DIRECTORY NAME IN CBO SCENARIO CHOICE
				
	char pclConfigPath[256]="";
	char pclscenariopath[256]="";
	
	
		if (getenv("CEDA") == NULL)
			strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
		else
			strcpy(pclConfigPath, getenv("CEDA"));

		GetPrivateProfileString(ogAppName, "SCENARIOPATH", "DEFAULT",
			pclscenariopath, sizeof pclscenariopath, pclConfigPath);
	
	// THIS THE PATH
	CString path=pclscenariopath ;
    path+="\\*";

   CFileFind finder; 
   LPCTSTR pstr = NULL;;
   CString strWildcard(pstr); 
   CString strpath = pclscenariopath;
   strWildcard += strpath+"\\*.*"; 

   // start working for files 
   BOOL bWorking = finder.FindFile(strWildcard); 

   while (bWorking) 
   { 
      bWorking = finder.FindNextFile(); 

	   
						CString strua = finder.GetFileName();
						int t =  strua.Find("ua");
						int u =  strua.Find("show");
						int v =  strua.Find("usa");
						if(t==0 && u==-1 && v > 0)//// ONLY WITH ua 
								{ 
							std::string strsub  = finder.GetFileName();
							std::string strsub2 = strsub.substr(2,strsub.length()-6);// "ua" AND ".usa"
							vCBOCICData.push_back(strsub2.c_str());
								}

   } 

   finder.Close();

   
   CComboBox *polCBuser = NULL;
              polCBuser = (CComboBox *) omDialogBar.GetDlgItem(IDC_CHANGESCENARIO);

	CString olSel = "<Default>";
	if (polCBuser->GetCount() > 0 && polCBuser->GetCurSel() != -1)
		polCBuser->GetLBText(polCBuser->GetCurSel(), olSel);

			  

   polCBuser->ResetContent();
   polCBuser->AddString("<Default>");
//   polCBuser->SelectString(0,"<Default>");
			
			// FOR COMOBOBOX
			for( int g = 0;g < vCBOCICData.size() ;g++)
			{
  				polCBuser->AddString(vCBOCICData[g]);
			}	
			
		    if (polCBuser->SelectString(0,olSel) == CB_ERR)
				polCBuser->SelectString(0,"<Default>");
			UpdateData(FALSE); 
	}
}

// END TSC



void CcaDiagram::ChangeViewTo(const char *pcpViewName,bool RememberPositions)
{
	
	//PRF 8363
	//SetFocusToDiagram();

	UpdateWoResButton();
	UpdateWoDemButton();
	UpdateConfButton();

	if (bgNoScroll == TRUE)
		return;

	for (int ilLc = 0; ilLc < omChartArray.GetSize(); ilLc++)
	{
		CcaChart *polChart = (CcaChart *)omChartArray[ilLc];
		if (polChart != NULL)
		{
			if (!polChart->omGantt.bmRepaint)
			{
				bmRepaintAll = true;
				return;
			}
		}
	}

	int ilDwRef = m_dwRef;
	AfxGetApp()->DoWaitCursor(1);
	bmNoUpdatesNow = TRUE;
	omViewer.AllowUpdates(bmNoUpdatesNow);

	CCSPtrArray <int> olChartStates;
	olChartStates.RemoveAll();
	CCSPtrArray <int> olChartTopIndices;
	olChartTopIndices.RemoveAll();
	CCSPtrArray <int> olChartShownLines;

	CRect olRect; omTimeScale.GetClientRect(&olRect);

//rkr25042001
	//Set begin to local if local
	CTime olTSStartTime = omTSStartTime;
	if(bgGatPosLocal)
		ogBasicData.UtcToLocal(olTSStartTime);
//    omViewer.ChangeViewTo(pcpViewName, omTSStartTime, omTimeScale.GetTimeFromX(olRect.Width() + 38));
    omViewer.ChangeViewTo(pcpViewName, olTSStartTime, omTimeScale.GetTimeFromX(olRect.Width() + 38));

#if 0
	int ilChartState;
	int ilTopIndex;
 	if (RememberPositions == TRUE && pomChart != NULL)
	{
		ilChartState = pomChart->GetState();
		if (pomChart->GetGanttPtr() != NULL)
			ilTopIndex = pomChart->GetGanttPtr()->GetTopIndex();
	}
#endif
	int ilTopIndex = 0;
	if (! bgDiaGrouping)
	{
		if (RememberPositions == TRUE)
		{
			ilTopIndex = ((CcaChart *)omChartArray[0])->GetGanttPtr()->GetTopIndex();
		}
	}
	for (int ilIndex = 0; ilIndex < omChartArray.GetSize(); ilIndex++)
    {
		if (RememberPositions == TRUE)
		{
			int ilState = ((CcaChart *)omChartArray[ilIndex])->GetState();
			olChartStates.NewAt(ilIndex,ilState);
			
			
			int ilTopIndex = 0;
			int ilLines = 0;

			if (((CcaChart *)omChartArray[ilIndex])->GetGanttPtr() != NULL)
			{
				ilTopIndex = ((CcaChart *)omChartArray[ilIndex])->GetGanttPtr()->GetTopIndex();
				ilLines = ((CcaChart *)omChartArray[ilIndex])->GetGanttPtr()->imHeight;
			}
			
			olChartTopIndices.NewAt(ilIndex,ilTopIndex);
			olChartShownLines.NewAt(ilIndex,ilLines);

		}
		((CcaChart *)omChartArray[ilIndex])->DestroyWindow();
    }
    omChartArray.RemoveAll();

    CRect olPrevRect;
    omClientWnd.GetClientRect(&olRect);
    
	if (RememberPositions == FALSE)
	{
	    imFirstVisibleChart = 0;
	}
    
	// Grouping
	//	pomChart->GetGanttPtr()->SetCurrentTime(olCurr);
	CTime olCurr = CTime::GetCurrentTime();
	//Die wollen local
 	if(!bgGatPosLocal)
		ogBasicData.LocalToUtc(olCurr);
	
	imFirstVisibleChart = 0;
    int ilLastY = 0;
	CcaChart *polChart;
	int ilCount = omViewer.GetGroupCount();
		if (!bgDiaGrouping) 
		ilCount = 1;

    for (int ilI = 0; ilI < ilCount; ilI++)
    {
        olRect.SetRect(olRect.left, ilLastY, olRect.right, ilLastY);
        
        polChart = new CcaChart;
        polChart->SetTimeScale(&omTimeScale);
        polChart->SetViewer(&omViewer, ilI);
        polChart->SetStatusBar(&omStatusBar);
        polChart->SetStartTime(omTSStartTime);
        polChart->SetInterval(omTimeScale.GetDisplayDuration());


		if(bgFWRGantt /*&& ilI == 0*/)
		{
			if(ilI == 0)
				polChart->Create(NULL, "CcaChart", WS_OVERLAPPED | WS_THICKFRAME    | WS_CHILD | WS_VISIBLE, olRect, &omClientWnd, 0 /* IDD_CHART */, NULL);
			else
				polChart->Create(NULL, "CcaChart", WS_OVERLAPPED | WS_BORDER  | WS_CHILD | WS_VISIBLE, olRect, &omClientWnd, 0 /* IDD_CHART */, NULL);
		}
		else
			polChart->Create(NULL, "CcaChart", WS_OVERLAPPED | WS_BORDER | WS_CHILD | WS_VISIBLE, olRect, &omClientWnd, 0 /* IDD_CHART */, NULL);

		polChart->SetState(Maximized);
	
		polChart->GetGanttPtr()->SetCurrentTime(olCurr);
        omChartArray.Add(polChart);
		
        ilLastY += polChart->GetHeight() + 1;
    }

	for (int ilChartNo = 0; ilChartNo < omChartArray.GetSize(); ilChartNo++)
	{
		if (RememberPositions == TRUE)
		{
			int ilChartState = olChartStates[ilChartNo];
			int ilTopIndex = olChartTopIndices[ilChartNo];
			int ilHeight = olChartShownLines[ilChartNo];

			
			((CcaChart *)omChartArray[ilChartNo])->SetState(olChartStates[ilChartNo]);
			if (((CcaChart *)omChartArray[ilChartNo])->GetGanttPtr() != NULL)
			{
				((CcaChart *)omChartArray[ilChartNo])->GetGanttPtr()->SetTopIndex(olChartTopIndices[ilChartNo]);

				((CcaChart *)omChartArray[ilChartNo])->GetGanttPtr()->imHeight = ilHeight;
			}
		}
		else
		{
			if (((CcaChart *)omChartArray[ilChartNo])->GetGanttPtr() != NULL)
			{
				((CcaChart *)omChartArray[ilChartNo])->GetGanttPtr()->SetMaxGanttChartHeight();
			}

		}

		((CcaChart *)omChartArray[ilChartNo])->GetGanttPtr()->SetCurrentTime(olCurr);
		
		if(bgFWRGantt /*&& ilChartNo == 0*/)
		{
			CcaChart *polChart = (CcaChart *) omChartArray.GetAt(0);
			int ilLine = omViewer.GetLineOfFirstVisibleBar(0, omTSStartTime);

			if(ilLine != -1)
				polChart->omGantt.SetTopIndex(ilLine);

		}


	}      
	
	olChartStates.RemoveAll();
	olChartShownLines.RemoveAll();
	olChartTopIndices.RemoveAll();
	

	
	if (! bgDiaGrouping)
	{
		if (((CcaChart *)omChartArray[0])->GetGanttPtr() != NULL)
		{
			((CcaChart *)omChartArray[0])->GetGanttPtr()->SetTopIndex(ilTopIndex);
		}
	}
        
	if (pomCicKonflikteDlg)
		pomCicKonflikteDlg->ChangeViewTo();
 
	PositionChild();
	AfxGetApp()->DoWaitCursor(-1);
	bmNoUpdatesNow = FALSE;
	omViewer.AllowUpdates(bmNoUpdatesNow);

	if (pomBltOverviewTableDlg)
		pomBltOverviewTableDlg->Update();

	UpdateWoResButton();
	UpdateWoDemButton();
	UpdateConfButton();

}

void CcaDiagram::OnOverview()
{
	if (pomBltOverviewTableDlg != NULL)
 		pomBltOverviewTableDlg->Activate();
}


void CcaDiagram::OnGatposConf()
{	
	if (pomCicKonflikteDlg != NULL)
//		pomCicKonflikteDlg->Attention(CString("CCA"));
		pomCicKonflikteDlg->Attention(CONF_CCA);
}

void CcaDiagram::OnScenarioChange()
{
    
    char clText[64];
    CComboBox *polCBuser = (CComboBox *) omDialogBar.GetDlgItem(IDC_CHANGESCENARIO);
    polCBuser->GetLBText(polCBuser->GetCurSel(), clText);
	oguseralloctepath= clText;

	OnViewSelChange();
}


void CcaDiagram::OnViewSelChange()
{
   //PRF 8363, Change the focus to gantt chart //
	SetFocusToDiagram();

	
	if(ogCcaDiaFlightData.bmOffLine)
	{
			ogCcaDiaFlightData.bmOffLine = false;
			ogCcaDiaFlightData.omCcaData.bmOffLine = false;
			ogCcaDiaFlightData.UpdateOfflineButtons();
	}


    char clText[64];
    CComboBox *polCB = (CComboBox *) omDialogBar.GetDlgItem(IDC_VIEW);
    polCB->GetLBText(polCB->GetCurSel(), clText);
	if(strcmp(clText, "<Default>") == 0)
		return;

	ogSpotAllocation.InitializePositionMatrix();

//	UpdateDia();
	
	bmLoadFlights = true;

	ogDdx.DataChanged((void *)this, CCA_VIEW_CHANGED,(void *)clText );

////////////////////

	bmLoadFlights = false;



}


void CcaDiagram::OnCloseupView() 
{
	// Id 25-Sep-96
	// This will set the keyboard focus back to the bottom most list-box after this button was clicked.
	//MWO
	//SetFocus();
	//END MWO
}

void CcaDiagram::OnZeit()
{
	// Id 25-Sep-96
	// This will set the keyboard focus back to the bottom most list-box after this button was clicked.
	//MWO
	//SetFocus();
	//END MWO

	// set new start time
	CTime olTime = CTime::GetCurrentTime();
		ogBasicData.LocalToUtc(olTime);
	olTime -= CTimeSpan(0, 1, 0, 0);
	SetTSStartTime(olTime);

	// adjust timescale
	olTime = omTSStartTime;
	if(bgGatPosLocal)
		ogBasicData.UtcToLocal(olTime);
    omTimeScale.SetDisplayStartTime(olTime);
	//omTimeScale.SetDisplayStartTime(omTSStartTime);
    omTimeScale.Invalidate(TRUE);
		
	// update scroll bar position
    long llTotalMin = omDuration.GetTotalMinutes() - omTSDuration.GetTotalMinutes();
	long nPos = (omTSStartTime - omStartTime).GetTotalMinutes();
	nPos = nPos * 1000L / llTotalMin;
    SetScrollPos(SB_HORZ, int(nPos), TRUE);

	ChangeViewTo(omViewer.SelectView());//ogCfgData.rmUserSetup.STCV);
    omClientWnd.Invalidate(FALSE);
}


BOOL CcaDiagram::DestroyWindow() 
{

	if ((bmIsViewOpen))
	{
		MessageBeep((UINT)-1);
		return FALSE;    // don't destroy window while view property sheet is still open
	}
	pogCcaDiagram = NULL; 

	if (pogCommonCcaTable != NULL)
	{
		pogCommonCcaTable->SaveToReg();
		delete pogCommonCcaTable;
		pogCommonCcaTable = NULL;
	}
	ogDdx.UnRegister(&omViewer, NOTUSED);
	bmNoUpdatesNow = TRUE;
	omViewer.AllowUpdates(bmNoUpdatesNow);
	BOOL blRc = CWnd::DestroyWindow();
	return blRc;
}

// Grouping
void CcaDiagram::OnPrevChart()
{
    if (imFirstVisibleChart > 0)
    {
        imFirstVisibleChart--;
		OnUpdatePrevNext();
        PositionChild();
    }
}

void CcaDiagram::OnNextChart()
{
    if (imFirstVisibleChart < omChartArray.GetUpperBound())
    {
        imFirstVisibleChart++;
		OnUpdatePrevNext();
        PositionChild();
    }
}
/////////XXXXX
////////////////////////////////////////////////////////////////////////
// CcaDiagram keyboard handling


void CcaDiagram::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	// check if the control key is pressed
    BOOL blIsControl = ::GetKeyState(VK_CONTROL) & 0x8080;
		// This statement has to be fixed for using both in Windows 3.11 and NT.
		// In Windows 3.11 the bit 0x80 will be turned on if control key is pressed.
		// In Windows NT, the bit 0x8000 will be turned on if control key is pressed.

	switch (nChar) 
	{
	case VK_LEFT:
		OnHScroll(blIsControl? SB_PAGEUP: SB_LINEUP, 0, NULL);
		break;
	case VK_RIGHT:
		OnHScroll(blIsControl? SB_PAGEDOWN: SB_LINEDOWN, 0, NULL);
		break;
	case VK_HOME:
		SetScrollPos(SB_HORZ, 0, FALSE);
		OnHScroll(SB_LINEUP, 0, NULL);
		break;
	case VK_END:
		SetScrollPos(SB_HORZ, 1000, FALSE);
		OnHScroll(SB_LINEDOWN, 0, NULL);
		break;
	default:
		omDialogBar.SendMessage(WM_KEYDOWN, nChar, MAKELONG(LOWORD(nRepCnt), LOWORD(nFlags)));
		CFrameWnd::OnKeyDown(nChar, nRepCnt, nFlags);
		break;
	}
}

void CcaDiagram::OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	CFrameWnd::OnKeyUp(nChar, nRepCnt, nFlags);
}

////////////////////////////////////////////////////////////////////////
// CcaDiagram -- implementation of DDX call back function

static void CcaDiagramCf(void *popInstance, enum enumDDXTypes ipDDXType,
    void *vpDataPointer, CString &ropInstanceName)
{
	CcaDiagram *polDiagram = (CcaDiagram *)popInstance;

	TIMEPACKET *polTimePacket; 

	switch (ipDDXType)
	{
	case CCA_VIEW_CHANGED:
        polDiagram->ViewSelChange( (char *) vpDataPointer);
		break;
	case CCA_GET_CUR_VIEW:
		polDiagram->GetCurView((char *) vpDataPointer);
		break;
	case REDISPLAY_ALL:
		polDiagram->RedisplayAll();
		break;
	case STAFFDIAGRAM_UPDATETIMEBAND:
		polTimePacket = (TIMEPACKET *)vpDataPointer;
		polDiagram->SetTimeBand(polTimePacket->StartTime, polTimePacket->EndTime);
		polDiagram->UpdateTimeBand();
		break;

	}
}

void CcaDiagram::RedisplayAll()
{
	ChangeViewTo(omViewer.SelectView());//ogCfgData.rmUserSetup.STCV);
}

void CcaDiagram::SetTimeBand(CTime opStartTime, CTime opEndTime)
{
	omTimeBandStartTime = opStartTime;
	omTimeBandEndTime = opEndTime;
}

void CcaDiagram::UpdateTimeBand()
{
	// Grouping
	//	pomChart->omGantt.SetMarkTime(omTimeBandStartTime, omTimeBandEndTime);
	for (int ilLc = imFirstVisibleChart; ilLc < omChartArray.GetSize(); ilLc++)
	{
		CcaChart *polChart = (CcaChart *)omChartArray[ilLc];
		polChart->omGantt.SetMarkTime(omTimeBandStartTime, omTimeBandEndTime);
	}
}

LONG CcaDiagram::RepaintAll(WPARAM wParam, LPARAM lParam)
{
	ChangeViewTo(omViewer.GetViewName(), true);
	return 0L;
}


void CcaDiagram::OnExpand()
{
	long llMinutes = omDuration.GetTotalMinutes();

	CTime olRefDate = omStartTime + CTimeSpan(0,0, llMinutes / 2  ,0);


	CString olStev;
	omViewer.GetStev(olStev);

	
	CTime olFrom = TIMENULL;
	CTime olTo = TIMENULL;
	CString olFtyps;
	omViewer.GetTimeSpanAndFtyp(olFrom, olTo, olFtyps);


	CCAExpandDlg olDlg (this,SUB_MOD_ALL, olStev, olRefDate, olFrom , olTo);
	olDlg.DoModal();

}
void CcaDiagram::OnDeleteTimeFrame()
{
	DelelteCounterDlg *polDlg = new DelelteCounterDlg(this);
	if(polDlg->DoModal() == IDOK)
	{
		OnViewSelChange();
	}
	delete polDlg;
}

void CcaDiagram::OnPrintGantt()
{
	bool bgFirstChart = true;
	
    for (int ilIndex = 0; ilIndex < omChartArray.GetSize(); ilIndex++)
    {
        CcaChart *polChart = (CcaChart *) omChartArray.GetAt(ilIndex);
		if ((polChart->GetState() == Maximized || polChart->GetState() == Normal) &&
			omViewer.GetLineCount(ilIndex) > 0)
		{
			omViewer.PrintGantt(polChart,bgFirstChart);
			bgFirstChart = false;
		}
	}
	omViewer.PrintGanttEnd();

	/*CcaChart *polChart = (CcaChart *) omChartArray.GetAt(0);
	omViewer.PrintGantt(polChart,true);*/
}

void CcaDiagram::OnAllocate()
{

	if (bgOffRelFuncCheckin && !ogCcaDiaFlightData.bmOffLine)
	{
		MessageBox(GetString(IDS_STRING2003), GetString(ST_FEHLER), MB_OK | MB_ICONERROR);
		return;
	}


	int ilRet = 0;
	AllocateCcaParameter *polDlg = new AllocateCcaParameter(this, bmAlreadyAlloc);
	ilRet = polDlg->DoModal();

	CTime olFrom = polDlg->omFrom;
	CTime olTo = polDlg->omTo;
	
	delete polDlg;
	
	if(ilRet == -1)
		return;

	bmAutoAllockInWork = true;
	
	bool blAllowCcaOverlap = bgAllowCcaOverlap;


	if(ilRet == 4 || ilRet == 3)
		bgAllowCcaOverlap = true;
	else
		bgAllowCcaOverlap = false;


	if(ilRet == 3)
		ilRet = 0;

	if(ilRet == 4)
		ilRet = 1;


	if(ilRet != 2 && ilRet != -1)
		bmAlreadyAlloc = true;
	else
		bmAlreadyAlloc = false;


	AllocateCca olAlloc(ilRet, olFrom, olTo);


	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
	omStatusBar.SetPaneText(0, GetString(IDS_STRING1492)); // Einteilung l�uft---------------------
	omStatusBar.UpdateWindow();
	olAlloc.Allocate();
	omStatusBar.SetPaneText(0, GetString(IDS_STRING1493)); // Speichern des Plans---------------------
	omStatusBar.UpdateWindow();

	TRACE("\n-----------------------------------------------");
	TRACE("\nBegin Save Allo: %s", CTime::GetCurrentTime().Format("%H:%M:%S"));


	ogCcaDiaFlightData.omCcaData.Release(NULL);

	TRACE("\nEnd Save Allo:   %s", CTime::GetCurrentTime().Format("%H:%M:%S"));
	TRACE("\n-----------------------------------------------");

	if (strcmp(pcgHome, "AUH") != 0)
		ogCcaDiaFlightData.CheckAllFlightsBazFields();


	TRACE("\n-----------------------------------------------");
	TRACE("\nBegin repaint: %s", CTime::GetCurrentTime().Format("%H:%M:%S"));


	RepaintAll(0,0);

	TRACE("\nend repaint:   %s", CTime::GetCurrentTime().Format("%H:%M:%S"));
	TRACE("\n-----------------------------------------------");


	TRACE("\n-----------------------------------------------");
	TRACE("\nBegin tables: %s", CTime::GetCurrentTime().Format("%H:%M:%S"));

	
	ActivateTables();
	
	TRACE("\nEnd tables:   %s", CTime::GetCurrentTime().Format("%H:%M:%S"));
	TRACE("\n-----------------------------------------------");
	
	bgAllowCcaOverlap=blAllowCcaOverlap;

	bmAutoAllockInWork = false;

	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));

}



void CcaDiagram::OnCommonCca()
{

	CTime olFrom;
	CTime olTo;
	CString olFtyps;
	omViewer.GetTimeSpanAndFtyp(olFrom, olTo, olFtyps);


	if (pogCommonCcaTable == NULL)
	{
		pogCommonCcaTable = new CCcaCommonTableDlg(this, olFrom, olTo);
	}
	else
	{
		pogCommonCcaTable->SetTimeFrame(olFrom, olTo);
		pogCommonCcaTable->SetWindowPos(&wndTop, 0,0,0,0, SWP_SHOWWINDOW | SWP_NOSIZE | SWP_NOMOVE );
	}

}


void CcaDiagram::SetWndPos()
{
	CRect olRect;

	GetWindowRect(&olRect);

	int ilWidth = olRect.right -olRect.left;
	int ilWhichMonitor = ogCfgData.GetMonitorForWindow(CString(MON_CCADIA_STRING));
	int ilMonitors = ogCfgData.GetMonitorForWindow(CString(MON_COUNT_STRING));
	int left, top, right, bottom;
	int ilCXMonitor;

	bottom = ::GetSystemMetrics(SM_CYSCREEN);

	top = 0;  


	imWhichMonitor = ilWhichMonitor;
	imMonitorCount = ilMonitors;

	if(ilWhichMonitor == 1)
	{
		switch(ilMonitors)
		{
		case 1:
			ilCXMonitor = ::GetSystemMetrics(SM_CXSCREEN);
			left = (int)((ilCXMonitor-ilWidth)/2);
			right = left + ilWidth;//GetSystemMetrics(SM_CXSCREEN);
			break;
		case 2:
			ilCXMonitor = (int)(::GetSystemMetrics(SM_CXSCREEN)/2);
			left = (int)((ilCXMonitor-ilWidth)/2);
			right = left + ilWidth;//GetSystemMetrics(SM_CXSCREEN);
			break;
		case 3:
			ilCXMonitor = (int)(::GetSystemMetrics(SM_CXSCREEN)/3);
			left = (int)((ilCXMonitor-ilWidth)/2);
			right = left + ilWidth;//GetSystemMetrics(SM_CXSCREEN);
			break;
		default:
			ilCXMonitor = ::GetSystemMetrics(SM_CXSCREEN);
			left = (int)((ilCXMonitor-ilWidth)/2);
			right = left + ilWidth;//GetSystemMetrics(SM_CXSCREEN);
			break;
		}
	}
	else if(ilWhichMonitor == 2)
	{
		switch(ilMonitors)
		{
		case 1:
			ilCXMonitor = ::GetSystemMetrics(SM_CXSCREEN);
			left = (int)((ilCXMonitor-ilWidth)/2);
			right = left + ilWidth;//GetSystemMetrics(SM_CXSCREEN);
			break;
		case 2:
			ilCXMonitor = (int)(::GetSystemMetrics(SM_CXSCREEN)/2);
			left = (int)((ilCXMonitor-ilWidth)/2)+(ilCXMonitor);
			right = left + ilWidth;//GetSystemMetrics(SM_CXSCREEN);
			break;
		case 3:
			ilCXMonitor = (int)(::GetSystemMetrics(SM_CXSCREEN)/3);
			left = (int)((ilCXMonitor-ilWidth)/2)+(ilCXMonitor);
			right = left + ilWidth;//GetSystemMetrics(SM_CXSCREEN);
			break;
		default:
			right = GetSystemMetrics(SM_CXSCREEN);
			break;
		}
	}
	else if(ilWhichMonitor == 3)
	{
		switch(ilMonitors)
		{
		case 1:
			ilCXMonitor = ::GetSystemMetrics(SM_CXSCREEN);
			left = (int)((ilCXMonitor-ilWidth)/2);
			right = left + ilWidth;//GetSystemMetrics(SM_CXSCREEN);
			break;
		case 2:
			ilCXMonitor = (int)(::GetSystemMetrics(SM_CXSCREEN)/2);
			left = (int)((ilCXMonitor-ilWidth)/2);
			right = left + ilWidth;//GetSystemMetrics(SM_CXSCREEN);
			break;
		case 3:
			ilCXMonitor = (int)(::GetSystemMetrics(SM_CXSCREEN)/3);
			left = (int)((ilCXMonitor-ilWidth)/2)+(2*ilCXMonitor);
			right = left + ilWidth;//GetSystemMetrics(SM_CXSCREEN);
			break;
		default:
			right = GetSystemMetrics(SM_CXSCREEN);
			break;
		}
	}

	MoveWindow(CRect(left, top, right, ::GetSystemMetrics(SM_CYSCREEN)));


	//SetWindowPos(&wndTop,left , top, 0, 0, SWP_SHOWWINDOW | SWP_NOSIZE);



}

void CcaDiagram::OnChanges()
{
	CallHistory(CString("CCACHECKIN"));
}


void CcaDiagram::OnFlightsWithoutResource()
{
	pogCicNoDemandTableDlg->ShowWindow(SW_SHOW);
	pogCicNoDemandTableDlg->SetWindowPos(&wndTop, 0,0,0,0, SWP_SHOWWINDOW | SWP_NOSIZE | SWP_NOMOVE );
}


void CcaDiagram::OnDemand()
{
	pogCicDemandTableDlg->ShowWindow(SW_SHOW);
	pogCicDemandTableDlg->SetWindowPos(&wndTop, 0,0,0,0, SWP_SHOWWINDOW | SWP_NOSIZE | SWP_NOMOVE );

}


void CcaDiagram::OnCicConf()
{
	pogCicConfTableDlg->ShowWindow(SW_SHOW);
	pogCicConfTableDlg->SetWindowPos(&wndTop, 0,0,0,0, SWP_SHOWWINDOW | SWP_NOSIZE | SWP_NOMOVE );

}




void CcaDiagram::ActivateTables()
{
	pogCicDemandTableDlg->Reset();
	pogCicDemandTableDlg->Activate();

	pogCicNoDemandTableDlg->Reset();
	pogCicNoDemandTableDlg->Activate();
	
	pogCicConfTableDlg->Reset();
	pogCicConfTableDlg->Activate();

	UpdateWoResButton();
	UpdateWoDemButton();
	UpdateConfButton();
}

void CcaDiagram::OnOffline()
{	
	if(ogCcaDiaFlightData.bmOffLine)
	{
		if (MessageBox(GetString(IDS_STRING1772), GetString(IMFK_RELEASE), MB_YESNO | MB_ICONQUESTION ) == IDNO)
			return;
	}

	ogCcaDiaFlightData.ToggleOffline();
	bmAlreadyAlloc = false;

}

void CcaDiagram::UpdateChangesButton(CString& opText, COLORREF opColor)
{
	CString olTmp;
	olTmp.Format("%s (%s)", GetString(IDS_CHANGES),opText);

	m_CB_Changes.SetWindowText(olTmp);
	m_CB_Changes.SetColors(opColor, ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
	m_CB_Changes.UpdateWindow();
}

void CcaDiagram::UpdateWoResButton()
{
	if (bmReadAll)
		return;

	CString olTmp;
	COLORREF olButtonColor;

	int ilCount = pogCicDemandTableDlg->GetDemCount();
	if(ilCount > 0)
	{
		if(bgFwrCic)
			olButtonColor = ogColors[IDX_ORANGE];
		else
			olButtonColor = ::GetSysColor(COLOR_BTNFACE);

		olTmp.Format("%s (%d)", GetString(IDS_STRING2624),ilCount);
	}
	else
	{
		olTmp.Format("%s (%d)", GetString(IDS_STRING2624),ilCount);
		olButtonColor = ::GetSysColor(COLOR_BTNFACE);
	}

	m_CB_WoRes.SetWindowText(olTmp);
	m_CB_WoRes.SetColors(olButtonColor, ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
	m_CB_WoRes.UpdateWindow();
}


void CcaDiagram::UpdateWoDemButton()
{
	if (bmReadAll)
		return;

	CString olTmp;
	COLORREF olButtonColor;

	int ilCount = pogCicNoDemandTableDlg->GetCount();
	if(ilCount > 0)
	{
		olButtonColor = ogColors[IDX_ORANGE];
		olTmp.Format("%s (%d)", GetString(IDS_STRING2625),ilCount);

	}
	else
	{
		olTmp.Format("%s (%d)", GetString(IDS_STRING2625),0);
		olButtonColor = ::GetSysColor(COLOR_BTNFACE);
	}

	m_CB_WoDem.SetWindowText(olTmp);
	m_CB_WoDem.SetColors(olButtonColor, ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
	m_CB_WoDem.UpdateWindow();
}

void CcaDiagram::UpdateConfButton()
{
	if (bmReadAll)
		return;

	CString olTmp;
	COLORREF olButtonColor;

	int ilConf = 0;
	int ilWoDem = 0;
	pogCicConfTableDlg->GetCount(ilConf, ilWoDem);

	if(ilWoDem > 0)
		olButtonColor = ogColors[IDX_ORANGE];
	else if (ilConf > 0)
		olButtonColor = ogColors[IDX_RED];
	else
		olButtonColor = ::GetSysColor(COLOR_BTNFACE);

	olTmp.Format("%s (%d/%d)", GetString(IDS_STRING2626),ilConf,ilWoDem);

	m_CB_Conf.SetWindowText(olTmp);
	m_CB_Conf.SetColors(olButtonColor, ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
	m_CB_Conf.UpdateWindow();
}

void CcaDiagram::UpdateKonflikteDlg()
{	
	if (pomCicKonflikteDlg != NULL)
//		pomCicKonflikteDlg->UpdateDisplay();
		pomCicKonflikteDlg->ChangeViewTo();
}

void CcaDiagram::OnScenario() 
{
	CString olUser = ogBasicData.omUserID;
	CString olEditScenario = "EDIT_SCENARIO";
	CString olEditRules    = "EDIT_RULES";

	if(ogPrivList.GetStat("FIPSRules_Scenario") != '1')
		olEditScenario = "NOEDIT_SCENARIO";

	if(ogPrivList.GetStat("FIPSRules_Rules") != '1')
		olEditRules = "NOEDIT_RULES";

	CString olArgs = olUser + "," + olEditScenario + "," + olEditRules;
	char pclArgs[256] = "";
	strcpy (pclArgs, olArgs);

	char pclConfigPath[256]="";
	char pclscenarioexepath[256]="";


	if (getenv("CEDA") == NULL)
		strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
	else
		strcpy(pclConfigPath, getenv("CEDA"));

	GetPrivateProfileString(ogAppName, "FIPSRULESPATH", "DEFAULT",
		pclscenarioexepath, sizeof pclscenarioexepath, pclConfigPath);
/*	
	STARTUPINFO si; 
    PROCESS_INFORMATION pi; 

    ZeroMemory( &si, sizeof(si) ); 
    si.cb = sizeof(si); 
    ZeroMemory( &pi, sizeof(pi) ); 
*/
	char *args[4];
	args[0] = "child";
	args[1] = pclArgs;
	args[2] = NULL;
	args[3] = NULL;
	int ilReturn = _spawnv(_P_NOWAIT,pclscenarioexepath,args);

/*
	if( !CreateProcess( 
			TEXT(pclscenarioexepath),			  // No module name (use command line). 
			TEXT(pclArgs),						  // Command line. 
			NULL,					              // Process handle not inheritable. 
			NULL,								  // Thread handle not inheritable. 
			FALSE,								  // Set handle inheritance to FALSE. 
			0,									  // No creation flags. 
			NULL,								  // Use parent's environment block. 
			NULL,								  // Use parent's starting directory. 
			&si,								  // Pointer to STARTUPINFO structure. 
			&pi )								  // Pointer to PROCESS_INFORMATION structure. 
	  ) 
	{   printf( "CreateProcess failed (%d).\n", GetLastError()) ; }; 
*/	
  
//    WinExec(pclscenarioexepath,SW_SHOWNORMAL);
	
/*
	ogCcaDiaFlightData.UnRegister();	 
	ogCcaDiaFlightData.ClearAll();	 
	DestroyWindow();
*/
	
}

// 050329 MVy: color changed in setup dialog
LRESULT CcaDiagram::OnHandlingAgentColorChanged(WPARAM wParam, LPARAM lParam)
{
//	did not call CicConfTableViewer::ChangeViewTo
	RedisplayAll();
	ActivateTables();		// it is done too much, redisplaying the gantt chart due to a color change is not necessary and keeps a long time with this call, but not doing this call the conflict list dialog got no colors changed

	return 0L;

};	// OnHandlingAgentColorChanged

bool CcaDiagram::GetLoadedCca(CCSPtrArray<DIACCADATA> &opCca, CMapStringToString& opTypMap, bool bpOnlySel, bool bpMustBeInOneDay)
{
	return omViewer.GetLoadedCca(opCca, opTypMap, bpOnlySel, bpMustBeInOneDay);
}

void CcaDiagram::SetFocusToDiagram()
{
	//PRF 8363, Change the focus to gantt chart sisl//
	if (omChartArray.GetSize() > 0)
	{
		CcaGantt *polGantt = ((CcaChart *)omChartArray[0])-> GetGanttPtr();
		CWnd* polTopWindow = CWnd::GetActiveWindow();
		CWnd* polParent = ((CcaChart *)omChartArray[0])->GetParent()->GetParent();
		//if(polWroGantt != NULL)
		if(polTopWindow != NULL && polParent != NULL)
		{		
			if(polParent->m_hWnd != NULL && polTopWindow->m_hWnd == polParent->m_hWnd)
				polGantt->SetFocus();
		}
	}
}


void CcaDiagram::ViewSelChange(char *pcpView)
{

	omViewer.SelectView(pcpView);

	CComboBox *polCB = (CComboBox *) omDialogBar.GetDlgItem(IDC_VIEW);
	polCB->SetCurSel(polCB->FindString( 0, pcpView ) );

	if(strcmp(pcpView, "<Default>") == 0)
			bmLoadFlights = false;

	if (!LoadFlights(pcpView))
		return;

	UpdateDia();
	//PRF 8363
	SetFocusToDiagram();
}



void CcaDiagram::GetCurView( char * pcpView)
{
	strcpy(pcpView, omViewer.GetViewName());
}



bool CcaDiagram::UpdateDia()
{
	CString olViewName = omViewer.GetViewName();
	
	omViewer.MakeMasstab();
	CTime olT = omViewer.GetGeometrieStartTime();




	CTime olFrom;
	CTime olTo;
	CString olFtyps;
	omViewer.GetTimeSpanAndFtyp(olFrom, olTo, olFtyps);

	//if((olFrom > olT) || (olTo < olT))
	//	olT = olFrom;

	CString olS = olT.Format("%d.%m.%Y-%H:%M");
	SetTSStartTime(olT);
	omTSDuration = omViewer.GetGeometryTimeSpan();


	if(olFrom == TIMENULL)
		olFrom = CTime::GetCurrentTime();

	if(olTo == TIMENULL)
		olTo = olFrom + CTimeSpan(1, 0, 0, 0);


    
	omDuration = (olTo - olFrom) + CTimeSpan(1, 0, 0, 0);
//	if (bgGatPosLocal)
//		ogBasicData.UtcToLocal(olFrom);
	omStartTime = olFrom - CTimeSpan(1, 0, 0, 0);



	long llTotalMin = omDuration.GetTotalMinutes() - omTSDuration.GetTotalMinutes();
	long nPos = (omTSStartTime - omStartTime).GetTotalMinutes();
	nPos = nPos * 1000L / llTotalMin;
	SetScrollPos(SB_HORZ, int(nPos), TRUE);
	
//rkr
	//Set begin to local if local
	CTime olTSStartTime = omTSStartTime;
	if(bgGatPosLocal)
		ogBasicData.UtcToLocal(olTSStartTime);
    
	omTimeScale.SetDisplayTimeFrame(olTSStartTime, omTSDuration, omTSInterval);
//	omTimeScale.SetDisplayTimeFrame(omTSStartTime, omTSDuration, omTSInterval);
    omTimeScale.Invalidate(TRUE);
	SetTSStartTime(omTSStartTime);
	ChangeViewTo(olViewName.GetBuffer(0), false);

	ActivateTables();

	LoadDataCBO();

	//ogDdx.DataChanged((void *)this, CCA_VIEW_CHANGED,(void *)olViewName.GetBuffer(0) );

	SetFocusToDiagram(); //PRF 8363

		return true;
}


void CcaDiagram::OnUndo()
{
	if(pogUndoCkiDlg == NULL)
		return;

	pogUndoCkiDlg->SetWindowPos(&wndTop, 0,0,0,0, SWP_SHOWWINDOW | SWP_NOSIZE | SWP_NOMOVE);

}

// TLE: UFIS 3768 ========================

void CcaDiagram::OnDisableViewButton(CCmdUI *pCmdUI)
{
	if(ogPrivList.GetStat("CCADIAGRAMM_CB_Ansicht") == '0')
		pCmdUI->Enable(FALSE);
}
void CcaDiagram::OnDisableSearchButton(CCmdUI *pCmdUI)
{
	if(ogPrivList.GetStat("CCADIAGRAMM_CB_Search") == '0')
		pCmdUI->Enable(FALSE);
}
void CcaDiagram::OnDisableFWDButton(CCmdUI *pCmdUI)
{
	if(ogPrivList.GetStat("CCADIAGRAMM_CB_WODemands") == '0')
		pCmdUI->Enable(FALSE);
}
void CcaDiagram::OnDisableFWRButton(CCmdUI *pCmdUI)
{
	if(ogPrivList.GetStat("CCADIAGRAMM_CB_WORes") == '0')
		pCmdUI->Enable(FALSE);
}
void CcaDiagram::OnDisableChangeButton(CCmdUI *pCmdUI)
{
	if(ogPrivList.GetStat("CCADIAGRAMM_CB_Changes") == '0')
		pCmdUI->Enable(FALSE);
	else 
	{
		if(ogPrivList.GetStat("CCADIAGRAMM_CB_Changes") == '-')
			m_CB_Changes.ShowWindow(SW_HIDE);
	}
}
void CcaDiagram::OnDisableAllocationButton(CCmdUI *pCmdUI)
{
	if(ogPrivList.GetStat("CCADIAGRAMM_CB_Allocate") == '0')
		pCmdUI->Enable(FALSE);
}
void CcaDiagram::OnDisableOfflineButton(CCmdUI *pCmdUI)
{
	if(ogPrivList.GetStat("CCADIAGRAMM_CB_Allocate") == '0')	// same as auto allocate
		pCmdUI->Enable(FALSE);
	else 
	{
		if(ogPrivList.GetStat("CCADIAGRAMM_CB_Allocate") == '-')
			m_CB_Offline.ShowWindow(SW_HIDE);
	}
}
void CcaDiagram::OnDisableExpandButton(CCmdUI *pCmdUI)
{
	if(ogPrivList.GetStat("CCADIAGRAMM_CB_Expand") == '0')
		pCmdUI->Enable(FALSE);
}
void CcaDiagram::OnDisableAttention(CCmdUI *pCmdUI)
{
	if(ogPrivList.GetStat("CCADIAGRAMM_CB_Attention") == '0')
		pCmdUI->Enable(FALSE);
	else 
	{
		if(ogPrivList.GetStat("CCADIAGRAMM_CB_Attention") == '-')
			m_CB_CcaAttention.ShowWindow(SW_HIDE);
	}
}
void CcaDiagram::OnDisableConflictButton(CCmdUI *pCmdUI)
{
	if(ogPrivList.GetStat("CCADIAGRAMM_CB_Conflicts") == '0')
		pCmdUI->Enable(FALSE);
}
void CcaDiagram::OnDisableCommonButton(CCmdUI *pCmdUI)
{
	if(ogPrivList.GetStat("CCADIAGRAMM_CB_CommonCki") == '0')
		pCmdUI->Enable(FALSE);
}
void CcaDiagram::OnDisableDeleteButton(CCmdUI *pCmdUI)
{
	if(ogPrivList.GetStat("CCADIAGRAMM_CB_Delete") == '0')
		pCmdUI->Enable(FALSE);
}
void CcaDiagram::OnDisablePrintButton(CCmdUI *pCmdUI)
{
	if(ogPrivList.GetStat("CCADIAGRAMM_CB_Print") == '0')
		pCmdUI->Enable(FALSE);
}
void CcaDiagram::OnDisableNowButton(CCmdUI *pCmdUI)
{
	if(ogPrivList.GetStat("CCADIAGRAMM_CB_Now") == '0')
		pCmdUI->Enable(FALSE);
	else 
	{
		CButton *polCB = (CButton *) omDialogBar.GetDlgItem(IDC_ZEIT);
		SetpWndStatAll(ogPrivList.GetStat("CCADIAGRAMM_CB_Now"),polCB);
	}
}
void CcaDiagram::OnDisableCloseButton(CCmdUI *pCmdUI)
{
	if(ogPrivList.GetStat("CCADIAGRAMM_CB_Close") == '0')
		pCmdUI->Enable(FALSE);
	else 
	{
		CButton *polCB = (CButton *) omDialogBar.GetDlgItem(IDC_BEENDEN);
		SetpWndStatAll(ogPrivList.GetStat("CCADIAGRAMM_CB_Close"),polCB);
	}
}