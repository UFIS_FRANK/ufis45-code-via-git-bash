// PSGeometrie.cpp : implementation file
// Modification History:
// 19-feb-01	rkr	PRF 1305 Dubai: Gantchart-Anzeige unter local unterst�tzt
//

#include <stdafx.h>
#include <CCSEdit.h>
#include <CCSGlobl.h>
#include <PSGeometrie.h>
#include <CedaResGroupData.h>
#include <utils.h>
#include <BasicData.h>
#include <resrc1.h>
#include <PrivList.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// PSGeometrie property page

//IMPLEMENT_DYNCREATE(PSGeometrie, CPropertyPage)


PSGeometrie::PSGeometrie(FipsModules ipMode) : 
	CPropertyPage(PSGeometrie::IDD)
{
	//{{AFX_DATA_INIT(PSGeometrie)
	//}}AFX_DATA_INIT
    bmNotMoreThan12 = FALSE;
 	blIsInit = false;
 	bmLinesDefault = true;
	bmTimeDefault = true;
	bmVertDefault = true;
	bmDisplayBeginDefault = true;

  	switch(ipMode)
	{
	case POSDIA: 
		imResType = "PST"; 
		imResValuesId = VIEW_GEOMETRIE_POS_GROUP_ID;
  		break;
	case GATDIA: 
		imResType = "GAT";
		imResValuesId = VIEW_GEOMETRIE_GAT_GROUP_ID;
  		break;

	case BLTDIA: 
		imResType = "BLT";
		imResValuesId = VIEW_GEOMETRIE_BLT_GROUP_ID;
 		break;
	case WRODIA: 
		imResType = "WRO";
		imResValuesId = VIEW_GEOMETRIE_WRO_GROUP_ID;
 		break;
	case CCADIA: 
		imResType = "CIC"; 
		imResValuesId = VIEW_GEOMETRIE_CCA_GROUP_ID;
 		break;
	case CHADIA: 
		imResType = "CHU"; 
		imResValuesId = VIEW_GEOMETRIE_CHU_GROUP_ID;
 		break;
	default:
		imResType = "";
		imResValuesId = -1;
	}

//	for (int i = omValues.GetSize(); i < VIEW_GEOMETRIE_LAST_ID+1; omValues.Add(""),i++);

}

PSGeometrie::~PSGeometrie()
{
}
void PSGeometrie::SetCalledFrom(CString opCalledFrom)
{
	omCalledFrom = opCalledFrom;
}
void PSGeometrie::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(PSGeometrie)
	DDX_Control(pDX, IDC_TEST, m_TestText);
	DDX_Control(pDX, IDC_FONT, m_Font);
	DDX_Control(pDX, IDC_STARTDATE, m_Date);
 	DDX_Control(pDX, IDC_HEIGHT, m_Height);
	DDX_Control(pDX, IDC_FLT_TIMELINES, m_TimeLines);
 	DDX_Control(pDX, IDC_24H, m_24h);
	DDX_Control(pDX, IDC_12H, m_12h);
	DDX_Control(pDX, IDC_10H, m_10h);
	DDX_Control(pDX, IDC_8H, m_8h);
	DDX_Control(pDX, IDC_6H, m_6h);
	DDX_Control(pDX, IDC_4H, m_4h);
	DDX_Control(pDX, IDC_100PERCENT, m_100Proz);
	DDX_Control(pDX, IDC_75PERCENT, m_75Proz);
	DDX_Control(pDX, IDC_50PERCENT, m_50Proz);
 	DDX_Control(pDX, IDC_STARTTIME, m_Time);
	DDX_Control(pDX, IDC_GROUPS, m_LB_Groups);
	DDX_Control(pDX, IDC_INCGROUPS, m_LB_IncGroups);
	DDX_Control(pDX, IDC_INCLUDE, m_IncludeItems);
	DDX_Control(pDX, IDC_EXCLUDE, m_ExcludeItems);
	DDX_Control(pDX, IDC_UP, m_ItemUp);
	DDX_Control(pDX, IDC_DOWN, m_ItemDown);

	//}}AFX_DATA_MAP
	if (pDX->m_bSaveAndValidate == FALSE)
	{
		SetData();
	}
	if (pDX->m_bSaveAndValidate == TRUE)
	{
		GetData();
	}

}


BEGIN_MESSAGE_MAP(PSGeometrie, CPropertyPage)
	//{{AFX_MSG_MAP(PSGeometrie)
	ON_WM_HSCROLL()
 	ON_BN_CLICKED(IDC_10H, On10h)
	ON_BN_CLICKED(IDC_12H, On12h)
	ON_BN_CLICKED(IDC_24H, On24h)
	ON_BN_CLICKED(IDC_4H, On4h)
	ON_BN_CLICKED(IDC_6H, On6h)
	ON_BN_CLICKED(IDC_8H, On8h)
	ON_BN_CLICKED(IDC_INCLUDE, OnIncludeItems)
	ON_BN_CLICKED(IDC_EXCLUDE, OnExcludeItems)
	ON_BN_CLICKED(IDC_UP, OnItemUp)
	ON_BN_CLICKED(IDC_DOWN, OnItemDown)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// PSGeometrie message handlers

BOOL PSGeometrie::OnInitDialog() 
{
	CPropertyPage::OnInitDialog();

	SetSecState();
 
    if (bmNotMoreThan12)
         GetDlgItem(IDC_24H) -> EnableWindow(FALSE);

// it�s to late for settings, because updatedata() is done.
	//Range and Freq is now set in InitMassData() before SetPos() is called.
    m_Height.SetRange(0, 120);
	m_Height.SetTicFreq(10);
	m_Font.SetRange(0,19);
	m_Font.SetTicFreq(1);
//
 
	m_100Proz.ShowWindow(SW_HIDE);
	m_75Proz.ShowWindow(SW_HIDE);
	m_50Proz.ShowWindow(SW_HIDE);
	
	if (imResValuesId != -1)
	{
		// Init group list
		InitGroupTables();
	}
	else
	{
		// hide grouping dialog items
        GetDlgItem(IDC_GRP_GROUPBOX) -> ShowWindow(SW_HIDE);
		m_LB_Groups.ShowWindow(SW_HIDE);
		m_LB_IncGroups.ShowWindow(SW_HIDE);
		m_IncludeItems.ShowWindow(SW_HIDE);
		m_ExcludeItems.ShowWindow(SW_HIDE);
		m_ItemUp.ShowWindow(SW_HIDE);
		m_ItemDown.ShowWindow(SW_HIDE);
	}

	blIsInit = true;

 
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}


void PSGeometrie::ResetGroupsList()
{
	m_LB_Groups.ResetContent();
	CStringArray olGroupList;
	ogResGroupData.GetGroupList(imResType, olGroupList);
	for (int i = 0; i < olGroupList.GetSize(); i++)
	{
		m_LB_Groups.AddString(olGroupList[i]);
	}

}



void PSGeometrie::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar) 
{
	// TODO: Add your message handler code here and/or call default

    //CDialog::OnHScroll(nSBCode, nPos, pScrollBar);
    //UpdateData();

    m_Hour = m_Height.GetPos() / ONE_HOUR_POINT;
    //m_24h = -1;
    if (m_Hour == 4)
	{
        m_24h.SetCheck(0);
        m_12h.SetCheck(0);
        m_10h.SetCheck(0);
        m_8h.SetCheck(0);
        m_6h.SetCheck(0);
        m_4h.SetCheck(1);
	}
    else if (m_Hour == 6)
	{
        m_24h.SetCheck(0);
        m_12h.SetCheck(0);
        m_10h.SetCheck(0);
        m_8h.SetCheck(0);
        m_6h.SetCheck(1);
        m_4h.SetCheck(0);
	}
    else if (m_Hour == 8)
	{
        m_24h.SetCheck(0);
        m_12h.SetCheck(0);
        m_10h.SetCheck(0);
        m_8h.SetCheck(1);
        m_6h.SetCheck(0);
        m_4h.SetCheck(0);
	}
    else if (m_Hour == 10)
	{
        m_24h.SetCheck(0);
        m_12h.SetCheck(0);
        m_10h.SetCheck(1);
        m_8h.SetCheck(0);
        m_6h.SetCheck(0);
        m_4h.SetCheck(0);
	}
    else if (m_Hour == 12)
	{
        m_24h.SetCheck(0);
        m_12h.SetCheck(1);
        m_10h.SetCheck(0);
        m_8h.SetCheck(0);
        m_6h.SetCheck(0);
        m_4h.SetCheck(0);
	}
    else if (m_Hour == 24)
	{
        m_24h.SetCheck(1);
        m_12h.SetCheck(0);
        m_10h.SetCheck(0);
        m_8h.SetCheck(0);
        m_6h.SetCheck(0);
        m_4h.SetCheck(0);
	}
    else if (m_Hour == 0)
    {
        m_Hour = 2;
    }

    
    if (bmNotMoreThan12 && (m_Hour > 12))
        m_Hour = 12;
    m_Height.SetPos(m_Hour * ONE_HOUR_POINT);


	int ilFontPos = m_Font.GetPos();
	m_TestText.SetFont(&ogScalingFonts[ilFontPos]);
	m_TestText.UpdateWindow();
    //UpdateData(FALSE);


}

 
void PSGeometrie::On10h() 
{
    m_Height.SetPos(10 * ONE_HOUR_POINT);
}

void PSGeometrie::On12h() 
{
    m_Height.SetPos(12 * ONE_HOUR_POINT);
}

void PSGeometrie::On24h() 
{
    m_Height.SetPos(24 * ONE_HOUR_POINT);
}

void PSGeometrie::On4h() 
{
    m_Height.SetPos(4 * ONE_HOUR_POINT);
}

void PSGeometrie::On6h() 
{
    m_Height.SetPos(6 * ONE_HOUR_POINT);
}

void PSGeometrie::On8h() 
{
    m_Height.SetPos(8 * ONE_HOUR_POINT);
}


void PSGeometrie::InitMassData()
{
	ResetMassData();

	if (omValues.GetSize() < 1)
		return;

//rkr
	m_Height.SetRange(0, 120);
	m_Height.SetTicFreq(10);
	m_Font.SetRange(0,19);
	m_Font.SetTicFreq(1);


 	CString olTotalMassString = omValues[0];

 	//Ma�stab
	int ilMassFrom = olTotalMassString.Find('<');
	int ilMassTo = olTotalMassString.Find('>');
	if(ilMassFrom != -1 && ilMassTo != -1)
	{
		CString olMassString = olTotalMassString.Mid(ilMassFrom+1, ilMassTo-1);
		if(!olMassString.IsEmpty())
		{
 			int ilLinePos = olMassString.Find('F');
			int ilTimePos = olMassString.Find('Z');
			int ilVertPos = olMassString.Find('V');
			int ilShowTimePos = olMassString.Find('T');
 			if(ilLinePos != -1)
			{
				CString T = olMassString.Mid(ilLinePos+2, 1);
				if(!T.IsEmpty())
				{
					if(T == "J")
					{
						m_TimeLines.SetCheck(1);
					}
					else
					{
						m_TimeLines.SetCheck(0);
					}
					bmLinesDefault = false;
				}
			}
			if(ilTimePos != -1)
			{
				CString T = olMassString.Mid(ilTimePos+2, 2);
				if(!T.IsEmpty())
				{
					m_Hour = atoi(T);
				    m_Height.SetPos(m_Hour * ONE_HOUR_POINT);
					if (m_Hour == 4)
					{
						m_24h.SetCheck(0);
						m_12h.SetCheck(0);
						m_10h.SetCheck(0);
						m_8h.SetCheck(0);
						m_6h.SetCheck(0);
						m_4h.SetCheck(1);
					}
					else if (m_Hour == 6)
					{
						m_24h.SetCheck(0);
						m_12h.SetCheck(0);
						m_10h.SetCheck(0);
						m_8h.SetCheck(0);
						m_6h.SetCheck(1);
						m_4h.SetCheck(0);
					}
					else if (m_Hour == 8)
					{
						m_24h.SetCheck(0);
						m_12h.SetCheck(0);
						m_10h.SetCheck(0);
						m_8h.SetCheck(1);
						m_6h.SetCheck(0);
						m_4h.SetCheck(0);
					}
					else if (m_Hour == 10)
					{
						m_24h.SetCheck(0);
						m_12h.SetCheck(0);
						m_10h.SetCheck(1);
						m_8h.SetCheck(0);
						m_6h.SetCheck(0);
						m_4h.SetCheck(0);
					}
					else if (m_Hour == 12)
					{
						m_24h.SetCheck(0);
						m_12h.SetCheck(1);
						m_10h.SetCheck(0);
						m_8h.SetCheck(0);
						m_6h.SetCheck(0);
						m_4h.SetCheck(0);
					}
					else if (m_Hour == 24)
					{
						m_24h.SetCheck(1);
						m_12h.SetCheck(0);
						m_10h.SetCheck(0);
						m_8h.SetCheck(0);
						m_6h.SetCheck(0);
						m_4h.SetCheck(0);
					}
					else if (m_Hour == 0)
					{
						m_24h.SetCheck(0);
						m_12h.SetCheck(0);
						m_10h.SetCheck(0);
						m_8h.SetCheck(0);
						m_6h.SetCheck(0);
						m_4h.SetCheck(0);
						m_Hour = 2;
					}
					else
					{
						m_24h.SetCheck(0);
						m_12h.SetCheck(0);
						m_10h.SetCheck(0);
						m_8h.SetCheck(0);
						m_6h.SetCheck(0);
						m_4h.SetCheck(0);
					}
					bmTimeDefault = false;
				}
			}
			if(ilVertPos != -1)
			{
				CString T = olMassString.Mid(ilVertPos+2, 2);
				if(!T.IsEmpty())
				{
					int M = atoi(T);
					m_Font.SetPos(M);
					m_FontHeight = M;
					m_TestText.SetFont(&ogScalingFonts[m_FontHeight]);
 					bmVertDefault = false;
				}
			}
			if(ilShowTimePos != -1)
			{
				CString T = olMassString.Mid(ilShowTimePos+2, 14);
				if(!T.IsEmpty())
				{
					CTime olDate;
					olDate = DBStringToDateTime(T);
					if(olDate != TIMENULL)
					{
						CString olDateStr, olTimeStr;

						//show local if local
						if (omCalledFrom == "FLIGHTDIA")
						{
							if(bgDailyLocal)
								ogBasicData.UtcToLocal(olDate);
						}
						else
						{
							if(bgGatPosLocal)
								ogBasicData.UtcToLocal(olDate);
						}
		
						olDateStr = olDate.Format("%d.%m.%Y");
						olTimeStr = olDate.Format("%H:%M");
						m_Date.SetWindowText(olDateStr);
						m_Time.SetWindowText(olTimeStr);
						bmDisplayBeginDefault = false;
					}
					else
					{
						CString olDateStr, olTimeStr;
						olDate = CTime::GetCurrentTime();
						olDateStr = olDate.Format("%d.%m.%Y");
						olTimeStr = olDate.Format("%H:%M");
						//RST! m_Date.SetWindowText(olDateStr);
						//RST! m_Time.SetWindowText(olTimeStr);
					}
				}
				else
				{
					CString olDateStr, olTimeStr;
					CTime olDate = CTime::GetCurrentTime();
					olDateStr = olDate.Format("%d.%m.%Y");
					olTimeStr = olDate.Format("%H:%M");
					//RST! m_Date.SetWindowText(olDateStr);
					//RST! m_Time.SetWindowText(olTimeStr);
				}
			}
			else
			{
				CString olDateStr, olTimeStr;
				CTime olDate = CTime::GetCurrentTime();
				olDateStr = olDate.Format("%d.%m.%Y");
				olTimeStr = olDate.Format("%H:%M");
				//RST! m_Date.SetWindowText(olDateStr);
				//RST! m_Time.SetWindowText(olTimeStr);
			}
		}
	}

}



void PSGeometrie::ResetMassData()
{

    m_Height.SetPos(m_Hour * ONE_HOUR_POINT);
    m_Font.SetPos(m_FontHeight);

 	if(bmVertDefault == true)
	{
		m_FontHeight = 8;
		m_Font.SetPos(m_FontHeight);
		m_TestText.SetFont(&ogScalingFonts[m_FontHeight]);
	}
	if(bmTimeDefault == true)
	{
		m_10h.SetCheck(1);
		m_Height.SetPos(10 * ONE_HOUR_POINT);
	}

	m_Date.SetWindowText("");
	m_Time.SetWindowText("");
}


 
void PSGeometrie::SetData()
{	
	// init Ma�stab data
	InitMassData();

	// group data
	InitGroupTables(); 
}



void PSGeometrie::GetData()
{
	// make sure that VIEW_GEOMETRIE_LAST_ID+1 elements are in the omValues array!
	for (int i = omValues.GetSize(); i < VIEW_GEOMETRIE_LAST_ID+1; omValues.Add(""),i++);
	
	//Erst mal die Ma�stab-Elemente abfragen
	CString olMass;
 	if(m_TimeLines.GetCheck() == 1)
	{
		olMass += "F=J|";
	}
	else
	{
		olMass += "F=N|";
	}
	m_Hour = m_Height.GetPos() / ONE_HOUR_POINT;
	char pclHour[100]="";
	char pclFont[5]="";
	m_FontHeight = m_Font.GetPos();
	sprintf(pclFont, "%02d", m_FontHeight);
	sprintf(pclHour, "%02ld", m_Hour);
	olMass += "Z=" + CString(pclHour) + "|";
	olMass += "V=" + CString(pclFont) + "|";
/*	if(m_100Proz.GetCheck() == 1)
	{
		olMass += "V=0|";
	}
	else if((m_75Proz.GetCheck() == 1))
	{
		olMass += "V=1|";
	}
	else if((m_50Proz.GetCheck() == 1))
	{
		olMass += "V=2|";
	}
*/
	CString olStrDate, olStrTime;
	m_Date.GetWindowText(olStrDate);
	m_Time.GetWindowText(olStrTime);
	CTime olDate, olTime;
	olDate = DateStringToDate(olStrDate);
	if(olDate != TIMENULL)
	{ 
		olTime = HourStringToDate(olStrTime, olDate);

		//store time as utc if local
		if (omCalledFrom == "FLIGHTDIA")
		{
			if(bgDailyLocal)
				ogBasicData.LocalToUtc(olTime);
		}
		else
		{
			if(bgGatPosLocal)
				ogBasicData.LocalToUtc(olTime);
		}
		
		olMass += "T=" + olTime.Format("%Y%m%d%H%M00");
	}
	olMass = "<" + olMass + ">;";
	omValues.SetAt(0, olMass);

	// get group data
	GetGroupData(olMass);

}


void PSGeometrie::GetGroupData(CString opMass)
{
	// create comma separated string of group names
	CString olItem;
	CString olGroups;
	for (int i = 0; i < m_LB_IncGroups.GetCount(); i++)
	{
		m_LB_IncGroups.GetText(i, olItem);
		olGroups += olItem + ",";
	}
	if (m_LB_IncGroups.GetCount() > 0)
		olGroups = olGroups.Left(olGroups.GetLength()-1);

 	if (imResValuesId > -1)
	{
	//	opMass += olGroups;
		omValues.SetAt(imResValuesId, olGroups);
	}
}


void PSGeometrie::InitGroupTables()
{
	for (int j = omValues.GetSize(); j < VIEW_GEOMETRIE_LAST_ID+1; omValues.Add(""),j++);

	// Reset Groups
	m_LB_IncGroups.ResetContent();
	ResetGroupsList();

	if (imResValuesId < 0 || imResValuesId >= omValues.GetSize())
		return;

	CString olGroups = omValues[imResValuesId];	

/*
	CString olGroup;
	for (int k = 0; k < omValues.GetSize(); k++)
	{
		olGroup = omValues[k];
	}
*/

	CStringArray olItems;
	int ilPos;
	ExtractItemList(olGroups, &olItems, ',');

	ogResGroupData.RemoveNotExistingGroups(imResType, olItems);

	for (int i = 0; i < olItems.GetSize(); i++)
	{
		m_LB_IncGroups.AddString(olItems[i]);
		if ((ilPos = FindInListBox(m_LB_Groups, olItems[i])) != -1)
			m_LB_Groups.DeleteString(ilPos);
	}

}


afx_msg void PSGeometrie::OnIncludeItems()
{
	// move the selected items from group list to included groups list
	int i = 0;
	CString olItem;

	while (i < m_LB_Groups.GetCount())
	{
		if (m_LB_Groups.GetSel(i) != 0)
		{
			m_LB_Groups.GetText(i, olItem);
			m_LB_IncGroups.AddString(olItem);
			m_LB_Groups.DeleteString(i);
		}
		else
			i++;
	}

}
	


afx_msg void PSGeometrie::OnExcludeItems()
{
	// move the selected item from group list to included groups list
	int i = 0;
	CString olItem;

	while (i < m_LB_IncGroups.GetCount())
	{
		if (m_LB_IncGroups.GetSel(i) != 0)
		{
			m_LB_IncGroups.GetText(i, olItem);
			m_LB_Groups.AddString(olItem);
			m_LB_IncGroups.DeleteString(i);
		}
		else
			i++;
	}
 
}



afx_msg void PSGeometrie::OnItemUp()
{
	int ilIndex;
	if (MyGetSelItems(m_LB_IncGroups, 1, &ilIndex) == 0) 
		return;
	if (ilIndex == 0)
		return; // item is at the top

	CString olItem;
	m_LB_IncGroups.GetText(ilIndex, olItem);
	m_LB_IncGroups.DeleteString(ilIndex);
	m_LB_IncGroups.InsertString(ilIndex-1, olItem);
	m_LB_IncGroups.SetSel(ilIndex-1, TRUE);

}



afx_msg void PSGeometrie::OnItemDown()
{
	int ilIndex;
	if (MyGetSelItems(m_LB_IncGroups, 1, &ilIndex) == 0) 
		return;
	if (ilIndex == m_LB_IncGroups.GetCount()-1)
		return; // item is at the bottom

	CString olItem;
	m_LB_IncGroups.GetText(ilIndex, olItem);
	m_LB_IncGroups.DeleteString(ilIndex);
	m_LB_IncGroups.InsertString(ilIndex+1, olItem);
	m_LB_IncGroups.SetSel(ilIndex+1, TRUE);

}


void PSGeometrie::SetSecState()
{
 
	m_IncludeItems.SetSecState(ogPrivList.GetStat("VIEW_CB_IncItems"));
	m_ExcludeItems.SetSecState(ogPrivList.GetStat("VIEW_CB_ExcItems"));
	m_ItemUp.SetSecState(ogPrivList.GetStat("VIEW_CB_ItemUp"));
	m_ItemDown.SetSecState(ogPrivList.GetStat("VIEW_CB_ItemDown"));

}




