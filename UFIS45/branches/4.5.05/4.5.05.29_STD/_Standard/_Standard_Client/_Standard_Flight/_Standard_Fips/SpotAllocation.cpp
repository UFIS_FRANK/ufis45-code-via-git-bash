#include <StdAfx.h>

#include <BasicData.h>
#include <SpotAllocation.h>
#include <Konflikte.h>
#include <Utils.h>
#include <resrc1.h>
#include <process.h>
#include <CCSGlobl.h>
#include <string.h>
#include <GatDiagram.h>
#include <BltDiagram.h>
#include <PosDiagram.h>
#include <WroDiagram.h>
#include <ButtonListDlg.h>

#define IsOverlapped(start1, end1, start2, end2)    ((start1) <= (end2) && (start2) <= (end1))
#define IsWithIn(start1, end1, start2, end2)	((start1) >= (start2) && (end1) <= (end2))


// Local function prototype
static void SpotAllocationCf(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

static int CompareBKBarsByTime(const BLKDATA **e1, const BLKDATA **e2)
{
	return ((**e1).StartTime == (**e2).StartTime)? 0:
		((**e1).StartTime > (**e2).StartTime)? 1: -1;
}

static int CompareBySortFlag(const RecordSet **e1,const RecordSet **e2)
{

	CString olSort = (**e1).Values[ogBCD.GetFieldIndex("SGM", "SORT")];

	int olSort1 = atoi ((**e1).Values[ogBCD.GetFieldIndex("SGM", "SORT")]);
	int olSort2 = atoi ((**e2).Values[ogBCD.GetFieldIndex("SGM", "SORT")]);

	if (olSort1 < olSort2)
		return -1;
	else if (olSort1 > olSort2)
		return 1;
	else
		return 0;
}
/*
static int ComparePstResByPrio(const PST_RESLIST **e1, const PST_RESLIST **e2)
{
	CCS_TRY

	int ilRet = 0;
	if((**e1).Prio < (**e2).Prio)
	{
		return 1;
	}
	if((**e1).Prio > (**e2).Prio)
	{
		return -1;
	}


	CString olPstNam1 = (**e1).Pnam;
	CString olPstNam2 = (**e2).Pnam;

	if(strcmp(olPstNam1,olPstNam2) == 0)
	{
		return 0;
	}
	else
	{
		if(strcmp(olPstNam1,olPstNam2) > 0)
		{
			return 1;
		}
		else
		{
			return -1;
		}
	}

	CCS_CATCH_ALL
	return 0;

}
*/
static int CompareGatResOnlyByPrio(const GAT_RESLIST **e1, const GAT_RESLIST **e2)
{
	int ilRet = 0;
	if((**e1).Prio < (**e2).Prio)
	{
		ilRet = 1;
	}
	if((**e1).Prio > (**e2).Prio)
	{
		ilRet = -1;
	}
	if((**e1).Prio == (**e2).Prio)
	{
		ilRet = 0;
	}
	return ilRet;
}


static int ComparePstResByPrio(const PST_RESLIST **e1, const PST_RESLIST **e2)
{
	CCS_TRY

	int ilRet = 0;
	if((**e1).Prio < (**e2).Prio)
	{
		return 1;
	}
	if((**e1).Prio > (**e2).Prio)
	{
		return -1;
	}

	if((**e1).Sequence < (**e2).Sequence)
	{
		return -1;
	}
	if((**e1).Sequence > (**e2).Sequence)
	{
		return 1;
	}


	CString olPstNam1 = (**e1).Pnam;
	CString olPstNam2 = (**e2).Pnam;

	if(strcmp(olPstNam1,olPstNam2) == 0)
	{
		return 0;
	}
	else
	{
		if(strcmp(olPstNam1,olPstNam2) > 0)
		{
			return 1;
		}
		else
		{
			return -1;
		}
	}

	CCS_CATCH_ALL
	return 0;
}

static int CompareGatResByPrio(const GAT_RESLIST **e1, const GAT_RESLIST **e2)
{
	CCS_TRY

	int ilRet = 0;
	if((**e1).Prio < (**e2).Prio)
	{
		return 1;
	}
	if((**e1).Prio > (**e2).Prio)
	{
		return -1;
	}

	if((**e1).Sequence < (**e2).Sequence)
	{
		return -1;
	}
	if((**e1).Sequence > (**e2).Sequence)
	{
		return 1;
	}


	CString olPstNam1 = (**e1).Gnam;
	CString olPstNam2 = (**e2).Gnam;


	int ilUsed1 = (**e1).Used;
	int ilUsed2 = (**e2).Used;
		
	if(strcmp(olPstNam1,olPstNam2) == 0)
	{
		return 0;
	}
	else
	{

		if(ilUsed1 > ilUsed2)
		{
			return 1;
		}
		else
		{
			return -1;
		}
	}

	CCS_CATCH_ALL
	return 0;
}


static int CompareBltResByPrio(const BLT_RESLIST **e1, const BLT_RESLIST **e2)
{
	CCS_TRY

	int ilRet = 0;
	if((**e1).Prio < (**e2).Prio)
	{
		return 1;
	}
	if((**e1).Prio > (**e2).Prio)
	{
		return -1;
	}

	if((**e1).Sequence < (**e2).Sequence)
	{
		return -1;
	}
	if((**e1).Sequence > (**e2).Sequence)
	{
		return 1;
	}


	CString olPstNam1 = (**e1).Bnam;
	CString olPstNam2 = (**e2).Bnam;

	if(strcmp(olPstNam1,olPstNam2) == 0)
	{
		return 0;
	}
	else
	{
		if(strcmp(olPstNam1,olPstNam2) > 0)
		{
			return 1;
		}
		else
		{
			return -1;
		}
	}

	CCS_CATCH_ALL
	return 0;
}

SpotAllocation::SpotAllocation()
{
	pomParent = NULL;

	bmPstConf = true;
	bmGatConf = true;
	bmWroConf = true;
	bmBltConf = true;

	imMaxFBelts = 0;
	imActMaxF   = 1;
	bmIsPositionMatrixInitialized = false;
}



void SpotAllocation::SetAllocateParameters(AllocateParameters *popParameters)
{
	omParameters = *popParameters;
	bmIsPositionMatrixInitialized = false;
}





SpotAllocation::~SpotAllocation()
{
	omPstRules.DeleteAll();
	omGatRules.DeleteAll();
	omBltRules.DeleteAll();
	omWroRules.DeleteAll();
	omFlights.DeleteAll();
	omChangedUrnoMap.RemoveAll();

	ReleasePositionMatrix();

}

void SpotAllocation::SetParent( CWnd *popParent)
{
	pomParent = popParent;
}


void SpotAllocation::Init()
{


    ogDdx.UnRegister(this, NOTUSED);

	InitNatRestriction();
	CreatePositionRules();
	CreateGateRules();
	CreateBeltRules();
	CreateWroRules();

	InitPstLinksToTwy();
	
	ogDdx.Register(this, BCD_PST_CHANGE, CString("FLIGHTDIAVIEWER"), CString("PST Changed"), SpotAllocationCf);
	ogDdx.Register(this, BCD_GAT_CHANGE, CString("FLIGHTDIAVIEWER"), CString("PST Changed"), SpotAllocationCf);
	ogDdx.Register(this, BCD_BLT_CHANGE, CString("FLIGHTDIAVIEWER"), CString("PST Changed"), SpotAllocationCf);
	ogDdx.Register(this, BCD_WRO_CHANGE, CString("FLIGHTDIAVIEWER"), CString("PST Changed"), SpotAllocationCf);
	ogDdx.Register(this, BCD_NAT_CHANGE, CString("FLIGHTDIAVIEWER"), CString("PST Changed"), SpotAllocationCf);

}


static void SpotAllocationCf(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
    SpotAllocation *polViewer = (SpotAllocation *)popInstance;

	if (ipDDXType == BCD_PST_CHANGE)
	{
		polViewer->CreatePositionRules();
		polViewer->RuleChanged((RecordSet *)vpDataPointer, ipDDXType);
	}

	if (ipDDXType == BCD_GAT_CHANGE)
	{
		polViewer->CreateGateRules();
		polViewer->RuleChanged((RecordSet *)vpDataPointer, ipDDXType);
	}

	if (ipDDXType == BCD_NAT_CHANGE)
	{
		polViewer->GlobalNatureChange((RecordSet *)vpDataPointer);
	}
		
	if (ipDDXType == BCD_BLT_CHANGE)
	{
		polViewer->CreateBeltRules();
		polViewer->RuleChanged((RecordSet *)vpDataPointer, ipDDXType);
	}
		
	if (ipDDXType == BCD_WRO_CHANGE)
	{
		polViewer->RuleChanged((RecordSet *)vpDataPointer, ipDDXType);
	}
}

void SpotAllocation::RuleChanged(RecordSet* vpDataPointer, int ipDDXType)
{
	if (ipDDXType == BCD_GAT_CHANGE)
		ogDdx.DataChanged((void *)this, GAT_CHANGE,(void *)vpDataPointer );

	if (ipDDXType == BCD_PST_CHANGE)
		ogDdx.DataChanged((void *)this, PST_CHANGE,(void *)vpDataPointer );

	if (ipDDXType == BCD_BLT_CHANGE)
		ogDdx.DataChanged((void *)this, BLT_CHANGE,(void *)vpDataPointer );

	if (ipDDXType == BCD_WRO_CHANGE)
		ogDdx.DataChanged((void *)this, WRO_CHANGE,(void *)vpDataPointer );
}

void SpotAllocation::GlobalNatureChange(RecordSet* vpDataPointer)
{
		CString olTyp("");
		bool blAlga = false;
		bool blAlpo = false;
		bool blAlbb = false;
		bool blAlwr = false;
		if (GetLocalGlobalNat(vpDataPointer, olTyp, blAlga, blAlpo, blAlbb, blAlwr))
		{
			InitNatRestriction();
			CheckGatGlobalNat(olTyp, blAlga, blAlpo, blAlbb, blAlwr);
		}
}

void SpotAllocation::InitPstLinksToTwy()
{
	CString olTwy;
	CString olList;	

	int ilCount = ogBCD.GetDataCount("TWY");
	int ilCount2 = ogBCD.GetDataCount("PST");

	for(int i = 0; i < ilCount; i++)
	{
		olList = "";
		olTwy = ogBCD.GetField("TWY", i, "TNAM");

		for(int j = 0; j < ilCount2; j++)
		{
			if(olTwy == ogBCD.GetField("PST", j, "TAXI"))
			{
				olList += ogBCD.GetField("PST", j, "PNAM") + CString("|");
			}
		}

		if(!olList.IsEmpty())
			omPstLinksToTwy.Add(olList);

	}

}

void SpotAllocation::InitNatRestriction()
{

	omPstNatNoAllocListPlain = CString("");
	omGatNatNoAllocListPlain = CString("");
	omBltNatNoAllocListPlain = CString("");
	omWroNatNoAllocListPlain = CString("");

	omPstNatNoAllocList.RemoveAll();
	omGatNatNoAllocList.RemoveAll();
	omBltNatNoAllocList.RemoveAll();
	omWroNatNoAllocList.RemoveAll();
	
	int ilAlga = ogBCD.GetFieldIndex("NAT", "ALGA");
	int ilAlpo = ogBCD.GetFieldIndex("NAT", "ALPO");
	int ilAlbb = ogBCD.GetFieldIndex("NAT", "ALBB");
	int ilAlwr = ogBCD.GetFieldIndex("NAT", "ALWR");
	int ilTtyp = ogBCD.GetFieldIndex("NAT", "TTYP");


	int ilCount = ogBCD.GetDataCount("NAT");
	for(int i = 0; i < ilCount; i++)
	{
		RecordSet rlRec;
		ogBCD.GetRecord("NAT", i, rlRec);

		if(rlRec[ilAlga] != "X")
		{
			omGatNatNoAllocListPlain += ";" + rlRec[ilTtyp];
			omGatNatNoAllocList.Add(rlRec[ilTtyp]);
		}
		if(rlRec[ilAlpo] != "X")
		{
			omPstNatNoAllocListPlain += ";" + rlRec[ilTtyp];
			omPstNatNoAllocList.Add(rlRec[ilTtyp]);
		}
		if(rlRec[ilAlbb] != "X")
		{
			omBltNatNoAllocListPlain += ";" + rlRec[ilTtyp];
			omBltNatNoAllocList.Add(rlRec[ilTtyp]);
		}
		if(rlRec[ilAlwr] != "X")
		{
			omWroNatNoAllocListPlain += ";" + rlRec[ilTtyp];
			omWroNatNoAllocList.Add(rlRec[ilTtyp]);
		}
	}

	omGatNatNoAllocListPlain += ";";
	omPstNatNoAllocListPlain += ";";
	omBltNatNoAllocListPlain += ";";
	omWroNatNoAllocListPlain += ";";

}



void SpotAllocation::CreatePositionRules()
{
	omPstRules.DeleteAll();



	RecordSet rlRec;
	CStringArray olAcgrValues;
	PST_RESLIST *prlPstRes; 
	CStringArray olPralArray;
	RecordSet rlGatRec;
	
	
	ogBCD.SetSort("PST","PNAM+",true);
	int ilCount = ogBCD.GetDataCount("PST");


	for(int i = 0; i < ilCount; i++)
	{
		ogBCD.GetRecord("PST", i, rlRec);
		prlPstRes = new PST_RESLIST;
		
		prlPstRes->Urno = rlRec[ogBCD.GetFieldIndex("PST", "URNO")];
		prlPstRes->Pnam = rlRec[ogBCD.GetFieldIndex("PST", "PNAM")];
		prlPstRes->Brgs = rlRec[ogBCD.GetFieldIndex("PST", "BRGS")];

		prlPstRes->Nafr = DateStringToDate(rlRec[ogBCD.GetFieldIndex("PST", "NAFR")]);
		prlPstRes->Nato = DateStringToDate(rlRec[ogBCD.GetFieldIndex("PST", "NATO")]);

		//////////////// DXB get Terminal from relates Gates

		CStringArray ropGates;
		
		GetRelatedGates(prlPstRes->Pnam, ropGates);

		int ilCount = ropGates.GetSize();
		CString olTerm;
		for (int i = 0; i < ilCount; i++)
		{
			olTerm = "";
			
			ogBCD.GetField("GAT", "GNAM", ropGates[i], "TERM", olTerm); 

			if(prlPstRes->Term.Find(olTerm) == -1)
				prlPstRes->Term += olTerm + " ";

		}


		////////////////

		CString olAcgr = rlRec[ogBCD.GetFieldIndex("PST", "POSR")];
		ogBCD.GetField("PST", "URNO", prlPstRes->Urno, "POSR", olAcgr); 

		CString olPnam = rlRec[ogBCD.GetFieldIndex("PST", "PNAM")]; 
		
		ExtractItemList(olAcgr, &olAcgrValues, ';');

		GetAcgrFromAcgrList(prlPstRes, olAcgrValues);
		
		if(prlPstRes->Pos1 == CString(""))
		{
			prlPstRes->Mac1 = 0;
		}
		if(prlPstRes->Pos2 == CString(""))
		{
			prlPstRes->Mac2 = 0;
		}
		
		int ilC2 = ogBCD.GetDataCount("GAT");
	
		for(int j = 0; j < ilC2; j++)
		{
			ogBCD.GetRecord("GAT", j, rlGatRec);
 			if( olPnam == rlGatRec[ogBCD.GetFieldIndex("GAT", "RGA1")] )
			{
				prlPstRes->RelGats.Add(rlGatRec[ogBCD.GetFieldIndex("GAT", "GNAM")]);
			}
			if( olPnam == rlGatRec[ogBCD.GetFieldIndex("GAT", "RGA2")] )
			{
				prlPstRes->RelGats.Add(rlGatRec[ogBCD.GetFieldIndex("GAT", "GNAM")]);
			}
		}


		omPstRules.Add(prlPstRes);
	}

	omPstRules.Sort(ComparePstResByPrio);
}



void SpotAllocation::GetAcgrFromAcgrList(PST_RESLIST *prpAcgr, CStringArray &opAcgrValues)
{
	if(opAcgrValues.GetSize() < 12)
//	if(opAcgrValues.GetSize() < 28)
		return;

	CStringArray olItems;

	// aircraft wing span
	opAcgrValues[0].TrimRight();
	ExtractItemList(opAcgrValues[0], &olItems, '-');
	if(olItems.GetSize() == 2)
	{
		prpAcgr->SpanMin = atof(olItems[0]);
		prpAcgr->SpanMax = atof(olItems[1]);
	}


	// aircraft length
	opAcgrValues[1].TrimRight();
	ExtractItemList(opAcgrValues[1], &olItems, '-');
	if(olItems.GetSize() == 2)
	{
		prpAcgr->LengMin = atof(olItems[0]);
		prpAcgr->LengMax = atof(olItems[1]);
	}

	// aircraft height
	opAcgrValues[2].TrimRight();
	ExtractItemList(opAcgrValues[2], &olItems, '-');
	if(olItems.GetSize() == 2)
	{
		prpAcgr->HeighMin = atof(olItems[0]);
		prpAcgr->HeighMax = atof(olItems[1]);
	}

	//handle the rest


	// get aircraft lists
	prpAcgr->ACUserStr = opAcgrValues[3];
	ParsePrefExclAssiStr(prpAcgr->ACUserStr, prpAcgr->PreferredAC, prpAcgr->ExcludedAC, prpAcgr->AssignedAC);

	//neighbour positions
	//1
	CString olMac = opAcgrValues[5];
	if (opAcgrValues[4].IsEmpty())
		olMac = "0";
	else
		prpAcgr->PosAndMac.SetAt(opAcgrValues[4],olMac);

	//2
	olMac = opAcgrValues[7];
	if (opAcgrValues[6].IsEmpty())
		olMac = "0";
	else
		prpAcgr->PosAndMac.SetAt(opAcgrValues[6],olMac);
	
	//old version befor gatpos
	prpAcgr->Pos1 =	opAcgrValues[4];
	prpAcgr->Mac1 =	(double) atof(opAcgrValues[5]);
	prpAcgr->Pos2 =	opAcgrValues[6];
	prpAcgr->Mac2 =	(double) atof(opAcgrValues[7]);
	prpAcgr->Mult =	atoi(opAcgrValues[8]);
	prpAcgr->Prio =	atoi(opAcgrValues[9]);
	//old version befor gatpos

	// get nature code lists
	if(opAcgrValues.GetSize() > 10)
	{
	prpAcgr->NAUserStr = opAcgrValues[10];
	ParsePrefExclAssiStr(prpAcgr->NAUserStr, prpAcgr->PreferredNA, prpAcgr->ExcludedNA, prpAcgr->AssignedNA);
	}

	// get airline lists
	if(opAcgrValues.GetSize() > 11)
	{
		CString olALParamWithoutGrp;
		CString olALParamWithGroup;
		CString olTmp;
		CStringArray olArray;
		CString olStr;
		bool isGroup = false;
		olStr = opAcgrValues[11];
		if(!olStr.IsEmpty())
			ExtractItemList(olStr, &olArray, ' ');
		
		for(int i = 0; i < olArray.GetSize(); i++)
		{
			olTmp = olArray[i].GetBuffer(0);
			


			if(olTmp.Find("*") >= 0)
			{
				olTmp.Remove('*');
				olALParamWithGroup += olTmp + " ";
				isGroup = true;
			}
			else
			{

				if(olTmp.Find(":") < 0)
				{
					olALParamWithoutGrp += olTmp + " ";
				}
				else
				{
					olALParamWithoutGrp += olTmp + " ";
				}
			}			

		}

		prpAcgr->ALUserStr = opAcgrValues[11];
		ParsePrefExclAssiStr(olALParamWithoutGrp, prpAcgr->PreferredAL, prpAcgr->ExcludedAL, prpAcgr->AssignedAL);

		if(isGroup)
		{
			ParsePrefExclAssiStr( olALParamWithGroup, prpAcgr->PreferredALGroup, prpAcgr->ExcludedALGroup, prpAcgr->AssignedALGroup );
			ExpandPrefExclAssiStrWithGroups(prpAcgr->PreferredALGroup, prpAcgr->ExcludedALGroup, prpAcgr->AssignedALGroup, prpAcgr->PreferredAL, prpAcgr->ExcludedAL, prpAcgr->AssignedAL, "ALT", "ALC2", "ALC3");
		}

	}

	//neighbour positions
	//3
	if(opAcgrValues.GetSize() > 13)
	{
	olMac = opAcgrValues[13];
	if (opAcgrValues[12].IsEmpty())
		olMac = "0";
	else
		prpAcgr->PosAndMac.SetAt(opAcgrValues[12],olMac);
	}

	//4
	if(opAcgrValues.GetSize() > 15)
	{
	olMac = opAcgrValues[15];
	if (opAcgrValues[14].IsEmpty())
		olMac = "0";
	else
		prpAcgr->PosAndMac.SetAt(opAcgrValues[14],olMac);
	}

	//5
	if(opAcgrValues.GetSize() > 17)
	{
	olMac = opAcgrValues[17];
	if (opAcgrValues[16].IsEmpty())
		olMac = "0";
	else
		prpAcgr->PosAndMac.SetAt(opAcgrValues[16],olMac);
	}

	//6
	if(opAcgrValues.GetSize() > 19)
	{
	olMac = opAcgrValues[19];
	if (opAcgrValues[18].IsEmpty())
		olMac = "0";
	else
		prpAcgr->PosAndMac.SetAt(opAcgrValues[18],olMac);
	}

	//7
	if(opAcgrValues.GetSize() > 21)
	{
	olMac = opAcgrValues[21];
	if (opAcgrValues[20].IsEmpty())
		olMac = "0";
	else
		prpAcgr->PosAndMac.SetAt(opAcgrValues[20],olMac);
	}

	//8
	if(opAcgrValues.GetSize() > 23)
	{
	olMac = opAcgrValues[23];
	if (opAcgrValues[22].IsEmpty())
		olMac = "0";
	else
		prpAcgr->PosAndMac.SetAt(opAcgrValues[22],olMac);
	}

	//9
	if(opAcgrValues.GetSize() > 25)
	{
	olMac = opAcgrValues[25];
	if (opAcgrValues[24].IsEmpty())
		olMac = "0";
	else
		prpAcgr->PosAndMac.SetAt(opAcgrValues[24],olMac);
	}

	//10
	if(opAcgrValues.GetSize() > 27)
	{
	olMac = opAcgrValues[27];
	if (opAcgrValues[26].IsEmpty())
		olMac = "0";
	else
		prpAcgr->PosAndMac.SetAt(opAcgrValues[26],olMac);
	}

	//sequence
	if(opAcgrValues.GetSize() >= 29)
		prpAcgr->Sequence = atoi(opAcgrValues[28]);
	else
		prpAcgr->Sequence = 1;

	//groundtime
	if(opAcgrValues.GetSize() >= 30)
		prpAcgr->Groundtime = opAcgrValues[29];
	else
		prpAcgr->Groundtime = "";

	// get service code lists
	if(opAcgrValues.GetSize() >= 31)
	{
		prpAcgr->ServiceUserStr = opAcgrValues[30];
		ParsePrefExclAssiStr(prpAcgr->ServiceUserStr, prpAcgr->PreferredService, prpAcgr->ExcludedService, prpAcgr->AssignedService);
	}

	// get allowed flight ids Arr
	if(opAcgrValues.GetSize() >= 32)
	{
		prpAcgr->FltiArr = opAcgrValues[31];
	}

	// get allowed flight ids Dep
	if(opAcgrValues.GetSize() >= 33)
	{
		prpAcgr->FltiDep = opAcgrValues[32];
	}

	// 050301 MVy: get the specified Aircraft Registration restrictions; this was done in rule editor
	if( opAcgrValues.GetSize() >= 34  )
	{
		prpAcgr->AcrRegnUserStr = opAcgrValues[ 33 ];
		ParsePrefExclAssiStr( prpAcgr->AcrRegnUserStr, prpAcgr->PreferredAcrRegn, prpAcgr->ExcludedAcrRegn, prpAcgr->AssignedAcrRegn );
	}
	
	//For new version of FIPS Rules
	if( opAcgrValues.GetSize() > RULEFIELD_ACR_REGN )
	{
		prpAcgr->ACGroupUserStr = opAcgrValues[ RULEFIELD_ACR_REGN ];
		ParsePrefExclAssiStr( prpAcgr->ACGroupUserStr, prpAcgr->PreferredACGroup, prpAcgr->ExcludedACGroup, prpAcgr->AssignedACGroup );

		ExpandPrefExclAssiStrWithGroups(prpAcgr->PreferredACGroup, prpAcgr->ExcludedACGroup, prpAcgr->AssignedACGroup, prpAcgr->PreferredAC, prpAcgr->ExcludedAC, prpAcgr->AssignedAC, "ACT", "ACT3", "ACT5");
/*
		// get aircraft lists
		for (int i = 0; i < prpAcgr->PreferredACGroup.GetSize(); i++)
		{
			CString olGrnUrno  = ogBCD.GetFieldExt("GRN","GRPN","TABN",prpAcgr->PreferredACGroup.GetAt(i),"ACTTAB","URNO");
			
			CString olActList;
			olActList  = ogBCD.GetValueList("GRM","GURN",olGrnUrno, "VALU", ";");

			CStringArray olActUrnoArray;
			::ExtractItemList(olActList,&olActUrnoArray,';');

			for (int j = 0; j < olActUrnoArray.GetSize(); j++)
			{
				CString olAct3("");
				CString olAct5("");
				ogBCD.GetFields("ACT", "URNO", olActUrnoArray.GetAt(j), "ACT3", "ACT5", olAct3, olAct5);

				if (!olAct3.IsEmpty())
					prpAcgr->PreferredAC.Add(olAct3);

				if (!olAct5.IsEmpty())
					prpAcgr->PreferredAC.Add(olAct5);
			}

//				ParsePrefExclAssiStr(prpAcgr->ACUserStr, prpAcgr->PreferredAC, prpAcgr->ExcludedAC, prpAcgr->AssignedAC);
		}
		*/
	}

	// Destinations
	if( opAcgrValues.GetSize() >= 36  )
	{
		/* old
		prpAcgr->OrgDesUserStr = opAcgrValues[ 35 ];
		ParsePrefExclAssiStr( prpAcgr->OrgDesUserStr, prpAcgr->PreferredOrgDes, prpAcgr->ExcludedOrgDes, prpAcgr->AssignedOrgDes );
		*/

		CString olDesParamWithoutGrp;
		CString olDesParamWithGroup;
		CString olTmp;
		CStringArray olArray;
		CString olStr;
		bool isGroup = false;
		olStr = opAcgrValues[35];
		if(!olStr.IsEmpty())
			ExtractItemList(olStr, &olArray, ' ');
		
		for(int i = 0; i < olArray.GetSize(); i++)
		{
			olTmp = olArray[i].GetBuffer(0);
			


			if(olTmp.Find("*") >= 0)
			{
				olTmp.Remove('*');
				olDesParamWithGroup += olTmp + " ";
				isGroup = true;
			}
			else
			{
				olDesParamWithoutGrp += olTmp + " ";
			}			

		}
		prpAcgr->OrgDesUserStr = opAcgrValues[ 35 ];
		ParsePrefExclAssiStr( olDesParamWithoutGrp, prpAcgr->PreferredOrgDes, prpAcgr->ExcludedOrgDes, prpAcgr->AssignedOrgDes );

		if(isGroup)
		{
			ParsePrefExclAssiStr( olDesParamWithGroup, prpAcgr->PreferredDesGroup, prpAcgr->ExcludedDesGroup, prpAcgr->AssignedDesGroup );
			ExpandPrefExclAssiStrWithGroups(prpAcgr->PreferredDesGroup, prpAcgr->ExcludedDesGroup, prpAcgr->AssignedDesGroup, prpAcgr->PreferredOrgDes, prpAcgr->ExcludedOrgDes, prpAcgr->AssignedOrgDes, "APT", "APC3", "APC4");
		}




	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	if( opAcgrValues.GetSize() >= 37  )
	{
		prpAcgr->Minp = atoi(opAcgrValues[36]);
	}

	if( opAcgrValues.GetSize() >= 38  )
	{
		prpAcgr->Maxp = atoi(opAcgrValues[37]);
	}

	if( opAcgrValues.GetSize() >= 39  )
	{

		prpAcgr->FlightNoUserStr = opAcgrValues[38];
		CStringArray olArray;
		CString olTmp;
		CString olStr;
		olStr = opAcgrValues[38];
		if((!olStr.IsEmpty()) && (olStr != "0"))
			ExtractItemList(olStr, &olArray, ',');
		for(int i = 0; i < olArray.GetSize(); i++)
		{
			olTmp = olArray[i].GetBuffer(0);
			if(olTmp[0] == '!')
			{
				olTmp.Remove('!');
				prpAcgr->ExcludedFlightNo.Add(olTmp);
			}
			else if(olTmp[0] == '=')
			{
				olTmp.Remove('=');
				prpAcgr->AssignedFlightNo.Add(olTmp);
			}
			else
				prpAcgr->PreferredFlightNo.Add(olTmp);
		}
		}



}

// fills the requested fields of table in a member array with the members of a group array
void SpotAllocation::ExpandPrefExclAssiStrWithGroups(CStringArray &ropPreferredGrp, CStringArray &ropExcludedGrp, CStringArray &ropAssignedGrp, CStringArray &ropPreferred, CStringArray &ropExcluded, CStringArray &ropAssigned, CString ropTable, CString ropField1, CString ropField2) 
{
	// set basic table
	CString olTable = ropTable + CString(pcgTableExt);

	// step over preferred groups
	for (int i = 0; i < ropPreferredGrp.GetSize(); i++)
	{

		CString olTmp;
		CString olTmp2 = ropPreferredGrp.GetAt(i);
		CString olSave;
	
		int ilColonPos =  olTmp2.Find(":");

		if( ilColonPos > 0)
		{
			olTmp = olTmp2.Left(ilColonPos );
			olSave = olTmp2.Right( olTmp2.GetLength() - ilColonPos );
		}
		else
		{
			olTmp = olTmp2;
		}

		// get urno of group
		CString olGrnUrno  = ogBCD.GetFieldExt("GRN","GRPN","TABN",olTmp,olTable,"URNO");
		
		// get all members with this group urno
		CString olMemList;
		olMemList  = ogBCD.GetValueList("GRM","GURN",olGrnUrno, "VALU", ";");

		// change urno member string into an array
		CStringArray olMemUrnoArray;
		::ExtractItemList(olMemList,&olMemUrnoArray,';');

		// step over member array
		for (int j = 0; j < olMemUrnoArray.GetSize(); j++)
		{
			// get the requested table fields and add them to the list
			CString olVal1("");
			CString olVal2("");
			ogBCD.GetFields(ropTable, "URNO", olMemUrnoArray.GetAt(j), ropField1, ropField2, olVal1, olVal2);

			if (!olVal1.IsEmpty())
				ropPreferred.Add(olVal1 + olSave);

			if (!olVal2.IsEmpty())
				ropPreferred.Add(olVal2 + olSave);
		}
	}

	// step over excluded groups
	for (i = 0; i < ropExcludedGrp.GetSize(); i++)
	{

		CString olTmp;
		CString olTmp2 = ropExcludedGrp.GetAt(i);
		CString olSave;
	
		int ilColonPos =  olTmp2.Find(":");

		if( ilColonPos > 0)
		{
			olTmp = olTmp2.Left(ilColonPos );
			olSave = olTmp2.Right( olTmp2.GetLength() - ilColonPos );
		}		
		else
		{
			olTmp = olTmp2;
		}



		// get urno of group
		CString olGrnUrno  = ogBCD.GetFieldExt("GRN","GRPN","TABN",olTmp,olTable,"URNO");

		
		// get all members with this group urno
		CString olMemList;
		olMemList  = ogBCD.GetValueList("GRM","GURN",olGrnUrno, "VALU", ";");

		CString ol = ropExcludedGrp.GetAt(i);

		// change urno member string into an array
		CStringArray olMemUrnoArray;
		::ExtractItemList(olMemList,&olMemUrnoArray,';');

		// step over member array
		for (int j = 0; j < olMemUrnoArray.GetSize(); j++)
		{
			// get the requested table fields and add them to the list
			CString olVal1("");
			CString olVal2("");
			ogBCD.GetFields(ropTable, "URNO", olMemUrnoArray.GetAt(j), ropField1, ropField2, olVal1, olVal2);

			if (!olVal1.IsEmpty())
				ropExcluded.Add(olVal1 + olSave);

			if (!olVal2.IsEmpty())
				ropExcluded.Add(olVal2 + olSave);
		}
	}

	// step over Assigned groups
	for (i = 0; i < ropAssignedGrp.GetSize(); i++)
	{

		CString olTmp;
		CString olTmp2 = ropAssignedGrp.GetAt(i);
		CString olSave;
	
		int ilColonPos =  olTmp2.Find(":");

		if( ilColonPos > 0)
		{
			olTmp = olTmp2.Left(ilColonPos );
			olSave = olTmp2.Right( olTmp2.GetLength() - ilColonPos );
		}		
		else
		{
			olTmp = olTmp2;
		}



		// get urno of group
		CString olGrnUrno  = ogBCD.GetFieldExt("GRN","GRPN","TABN",olTmp,olTable,"URNO");
		
		// get all members with this group urno
		CString olMemList;
		olMemList  = ogBCD.GetValueList("GRM","GURN",olGrnUrno, "VALU", ";");

		// change urno member string into an array
		CStringArray olMemUrnoArray;
		::ExtractItemList(olMemList,&olMemUrnoArray,';');

		// step over member array
		for (int j = 0; j < olMemUrnoArray.GetSize(); j++)
		{
			// get the requested table fields and add them to the list
			CString olVal1("");
			CString olVal2("");
			ogBCD.GetFields(ropTable, "URNO", olMemUrnoArray.GetAt(j), ropField1, ropField1, olVal1, olVal2);

			if (!olVal1.IsEmpty())
				ropAssigned.Add(olVal1 + olSave);

			if (!olVal2.IsEmpty())
				ropAssigned.Add(olVal2 + olSave);
		}
	}
}


void SpotAllocation::ParsePrefExclAssiStr(const CString &ropStr, CStringArray &ropPreferred, CStringArray &ropExcluded, CStringArray &ropAssigned) const
{
	ropPreferred.RemoveAll();
	ropExcluded.RemoveAll();
	ropAssigned.RemoveAll();

	char cpFlag = ' ';
	CString olTmpStr;
	for (int i = 0; i < ropStr.GetLength(); i++)
	{
		if (ropStr[i] == ' ' && cpFlag == ' ')
			continue;

		if (ropStr[i] != ' ' && cpFlag != ' ') 
		{
			olTmpStr += ropStr[i];
			continue;
		}

		if (ropStr[i] == ' ')
		{
			if (cpFlag == 'P')
			{
				ropPreferred.Add(olTmpStr);
			}
			if (cpFlag == 'E')
			{
				ropExcluded.Add(olTmpStr);
			}
			if (cpFlag == 'A')
			{
				ropAssigned.Add(olTmpStr);				
			}
			olTmpStr.Empty();
			cpFlag = ' ';
		}
		else if (ropStr[i] == '!')
		{
			cpFlag = 'E';
		}
		else if (ropStr[i] == '=')
		{
			cpFlag = 'A';
		}
		else
		{
			cpFlag = 'P';
			olTmpStr += ropStr[i];
		}
	}

	if (cpFlag == 'P')
	{
		ropPreferred.Add(olTmpStr);
	}
	if (cpFlag == 'E')
	{
		ropExcluded.Add(olTmpStr);
	}
	if (cpFlag == 'A')
	{
		ropAssigned.Add(olTmpStr);				
	}


}




void SpotAllocation::CreateGateRules()
{
	omGatRules.DeleteAll();


	RecordSet rlRec;
	CStringArray olPralArray;
	GAT_RESLIST *prlGatRes;
	CString olNafr;
	CString olNato;
	CString olResb;	
	CStringArray olArray;


	int ilCount = ogBCD.GetDataCount("GAT");
	for(int i = 0; i < ilCount; i++)
	{
		ogBCD.GetRecord("GAT", i, rlRec);
		prlGatRes = new GAT_RESLIST;
		
		prlGatRes->Urno = rlRec[ogBCD.GetFieldIndex("GAT", "URNO")];
		prlGatRes->Gnam = rlRec[ogBCD.GetFieldIndex("GAT", "GNAM")];
		prlGatRes->Gnam.TrimLeft();
		prlGatRes->Gnam.TrimRight();
		prlGatRes->Busg = rlRec[ogBCD.GetFieldIndex("GAT", "BUSG")];
		prlGatRes->RelPos1 = rlRec[ogBCD.GetFieldIndex("GAT", "RGA1")];
		prlGatRes->RelPos2 = rlRec[ogBCD.GetFieldIndex("GAT", "RGA2")];
		
		olNafr = rlRec[ogBCD.GetFieldIndex("GAT", "NAFR")];
		olNato = rlRec[ogBCD.GetFieldIndex("GAT", "NATO")];
		prlGatRes->Nafr = DateStringToDate(olNafr);
		prlGatRes->Nato = DateStringToDate(olNato);

		olResb = rlRec[ogBCD.GetFieldIndex("GAT", "GATR")];
		ogBCD.GetField("GAT", "URNO", prlGatRes->Urno, "GATR", olResb); 

		ExtractItemList(olResb, &olArray, ';');
		if(olArray.GetSize() >= 9)
		{
			prlGatRes->Flti = olArray[0];//_T("");
			if(!olArray[1].IsEmpty())
			{
				prlGatRes->Maxp = atoi(olArray[1]);//0;
			}
			prlGatRes->Mult = atoi(olArray[2]);//0;
			if(olArray[3] == "X")
			{
				prlGatRes->Inbo = true;//FALSE;
			}
			if(olArray[4] == "X")
			{
				prlGatRes->Outb = true;//FALSE;
			}

			prlGatRes->Prio = atoi(olArray[5]);//0;

			// get naturecode lists
			prlGatRes->NAUserStr = olArray[6];
			ParsePrefExclAssiStr(prlGatRes->NAUserStr, prlGatRes->PreferredNA, prlGatRes->ExcludedNA, prlGatRes->AssignedNA);

			// get airline lists
			prlGatRes->ALUserStr = olArray[7];
			//ParsePrefExclAssiStr(prlGatRes->ALUserStr, prlGatRes->PreferredAL, prlGatRes->ExcludedAL, prlGatRes->AssignedAL);

			CString olALParamWithoutGrp;
			CString olALParamWithGroup;
			CString olTmp;
			CStringArray olArrayTmp;
			CString olStr;
			bool isGroup = false;
			olStr = olArray[7];
			if(!olStr.IsEmpty())
				ExtractItemList(olStr, &olArrayTmp,' ');
			
			for(int i = 0; i < olArrayTmp.GetSize(); i++)
			{
				olTmp = olArrayTmp[i].GetBuffer(0);
				


				if(olTmp.Find("*") >= 0)
				{
					olTmp.Remove('*');
					olALParamWithGroup += olTmp + " ";
					isGroup = true;
				}
				else
				{

					if(olTmp.Find(":") < 0)
					{
						olALParamWithoutGrp += olTmp + " ";
					}
					else
					{
						olALParamWithoutGrp += olTmp + " ";
					}
				}			

			}

			ParsePrefExclAssiStr(olALParamWithoutGrp, prlGatRes->PreferredAL, prlGatRes->ExcludedAL, prlGatRes->AssignedAL);

			if(isGroup)
			{
				ParsePrefExclAssiStr( olALParamWithGroup, prlGatRes->PreferredALGroup, prlGatRes->ExcludedALGroup, prlGatRes->AssignedALGroup );
				ExpandPrefExclAssiStrWithGroups(prlGatRes->PreferredALGroup, prlGatRes->ExcludedALGroup, prlGatRes->AssignedALGroup, prlGatRes->PreferredAL, prlGatRes->ExcludedAL, prlGatRes->AssignedAL, "ALT", "ALC2", "ALC3");
			}


			// get org/des lists
			prlGatRes->OrgDesUserStr = olArray[8];
			ParsePrefExclAssiStr(prlGatRes->OrgDesUserStr, prlGatRes->PreferredOrgDes, prlGatRes->ExcludedOrgDes, prlGatRes->AssignedOrgDes);



			CString olDesParamWithoutGrp;
			CString olDesParamWithGroup;
			olArrayTmp.RemoveAll();
			isGroup = false;
			olStr = olArray[8];
			if(!olStr.IsEmpty())
				ExtractItemList(olStr, &olArrayTmp, ' ');
			
			for(i = 0; i < olArrayTmp.GetSize(); i++)
			{
				olTmp = olArrayTmp[i].GetBuffer(0);
				


				if(olTmp.Find("*") >= 0)
				{
					olTmp.Remove('*');
					olDesParamWithGroup += olTmp + " ";
					isGroup = true;
				}
				else
				{
					olDesParamWithoutGrp += olTmp + " ";
				}			

			}
			ParsePrefExclAssiStr( olDesParamWithoutGrp, prlGatRes->PreferredOrgDes, prlGatRes->ExcludedOrgDes, prlGatRes->AssignedOrgDes );

			if(isGroup)
			{
				ParsePrefExclAssiStr( olDesParamWithGroup, prlGatRes->PreferredDesGroup, prlGatRes->ExcludedDesGroup, prlGatRes->AssignedDesGroup );
				ExpandPrefExclAssiStrWithGroups(prlGatRes->PreferredDesGroup, prlGatRes->ExcludedDesGroup, prlGatRes->AssignedDesGroup, prlGatRes->PreferredOrgDes, prlGatRes->ExcludedOrgDes, prlGatRes->AssignedOrgDes, "APT", "APC3", "APC4");
			}




		}

		if (olArray.GetSize() == 11)//old LIS-Data
		{
			prlGatRes->FltiDep = "";

			prlGatRes->Sequence = atoi(olArray[9]);

			prlGatRes->ServiceUserStr = olArray[10];
			ParsePrefExclAssiStr(prlGatRes->ServiceUserStr, prlGatRes->PreferredService, prlGatRes->ExcludedService, prlGatRes->AssignedService);

/*			if(olArray.GetSize() >= 10)
				prlGatRes->FltiDep = olArray[9];//_T("");

			if(olArray.GetSize() >= 11)
				prlGatRes->Sequence = atoi(olArray[10]);
			else
				prlGatRes->Sequence = 1;*/
		}

		// get service code lists
		if(olArray.GetSize() >= 12)
		{

			prlGatRes->FltiDep = olArray[9];//_T("");

			prlGatRes->Sequence = atoi(olArray[10]);

			prlGatRes->ServiceUserStr = olArray[11];
			ParsePrefExclAssiStr(prlGatRes->ServiceUserStr, prlGatRes->PreferredService, prlGatRes->ExcludedService, prlGatRes->AssignedService);
		}
		// get service code lists
		if(olArray.GetSize() > 21)
		{
			for (int i=12; i<22; i++)
			{
					CString olN = olArray[i];
					if (!olN.IsEmpty())
					{
						if(olArray.GetSize() >= 34)
						{

							prlGatRes->GateNeighbours.SetAt(olN, olArray[i + 12]);
						}
						else
						{
							prlGatRes->GateNeighbours.SetAt(olN,"-1");
						}

					}
			}
		}

		//New version of the FIPS Rules
		if(olArray.GetSize() >= 24)
		{
			//AirCraft Parameter
			prlGatRes->ACUserStr = olArray[22];
			ParsePrefExclAssiStr(prlGatRes->ACUserStr, prlGatRes->PreferredAC,prlGatRes->ExcludedAC, prlGatRes->AssignedAC);

			//AirCraft Groups
			prlGatRes->ACGroupUserStr = olArray[23];
			ParsePrefExclAssiStr(prlGatRes->ACGroupUserStr, prlGatRes->PreferredACGroup,prlGatRes->ExcludedACGroup, prlGatRes->AssignedACGroup);

			ExpandPrefExclAssiStrWithGroups(prlGatRes->PreferredACGroup, prlGatRes->ExcludedACGroup, prlGatRes->AssignedACGroup, prlGatRes->PreferredAC, prlGatRes->ExcludedAC, prlGatRes->AssignedAC, "ACT", "ACT3", "ACT5");
		}
		CString olTest;

		if(olArray.GetSize() >= 34)
		{
			olTest = olArray[24];

		}
		
		if(bgCommonGate)
			GetRelatedGates(prlGatRes->Gnam, prlGatRes->CommonGates, "GAT");


		// test
		/*
			if(prlGatRes->Gnam == "03")
			{
				prlGatRes->CommonGates.Add("04");
				prlGatRes->CommonGates.Add("05");
			}

			if(prlGatRes->Gnam == "04")
			{
				prlGatRes->CommonGates.Add("03");
				prlGatRes->CommonGates.Add("05");
			}
			if(prlGatRes->Gnam == "05")
			{
				prlGatRes->CommonGates.Add("04");
				prlGatRes->CommonGates.Add("03");
			}
		*/
		// test

		omGatRules.Add(prlGatRes);
	}
	omGatRules.Sort(CompareGatResByPrio);
}

void SpotAllocation::CreateWroRules()
{
	omWroRules.DeleteAll();

	RecordSet rlRec;
	WRO_RESLIST *prlWroRes;
	CStringArray olArray;

	int ilCount = ogBCD.GetDataCount("WRO");
	for(int i = 0; i < ilCount; i++)
	{
		ogBCD.GetRecord("WRO", i, rlRec);
		prlWroRes = new WRO_RESLIST;
		
		prlWroRes->Urno = rlRec[ogBCD.GetFieldIndex("WRO", "URNO")];
		prlWroRes->Wnam = rlRec[ogBCD.GetFieldIndex("WRO", "WNAM")];
		prlWroRes->Wnam.TrimLeft();
		prlWroRes->Wnam.TrimRight();
	}
}




void SpotAllocation::CreateBeltRules()
{
	omBltRules.DeleteAll();

	RecordSet rlRec;
	CStringArray olPralArray;
	BLT_RESLIST *prlBltRes;
	CStringArray olArray;

	int ilCount = ogBCD.GetDataCount("BLT");
	for(int i = 0; i < ilCount; i++)
	{
		ogBCD.GetRecord("BLT", i, rlRec);
		prlBltRes = new BLT_RESLIST;
		
		prlBltRes->Urno = rlRec[ogBCD.GetFieldIndex("BLT", "URNO")];
		prlBltRes->Bnam = rlRec[ogBCD.GetFieldIndex("BLT", "BNAM")];
		prlBltRes->Bnam.TrimLeft();
		prlBltRes->Bnam.TrimRight();

		CString olResb = rlRec[ogBCD.GetFieldIndex("BLT", "BLTR")];
		ogBCD.GetField("BLT", "URNO", prlBltRes->Urno, "BLTR", olResb); 

		//max flightcount
		CString olMaxFlight;
		ogBCD.GetField("BLT", "URNO", prlBltRes->Urno, "MAXF", olMaxFlight); 
		int ilMaxF = atoi(olMaxFlight);
		imMaxFBelts = max(ilMaxF,imMaxFBelts);

		ExtractItemList(olResb, &olArray, ';');
		if(olArray.GetSize() >= 7)
		{
			//prio
			prlBltRes->Prio = atoi(olArray[0]);
			//sequ
			prlBltRes->Sequence = atoi(olArray[1]);
			//pax
			prlBltRes->Maxp = atoi(olArray[2]);

			// get airline lists
			prlBltRes->ALUserStr = olArray[3];
			ParsePrefExclAssiStr(prlBltRes->ALUserStr, prlBltRes->PreferredAL, prlBltRes->ExcludedAL, prlBltRes->AssignedAL);

			// get org/des lists
			prlBltRes->OrgDesUserStr = olArray[4];
			ParsePrefExclAssiStr(prlBltRes->OrgDesUserStr, prlBltRes->PreferredOrgDes, prlBltRes->ExcludedOrgDes, prlBltRes->AssignedOrgDes);

			// get naturecode lists
			prlBltRes->NAUserStr = olArray[5];
			ParsePrefExclAssiStr(prlBltRes->NAUserStr, prlBltRes->PreferredNA, prlBltRes->ExcludedNA, prlBltRes->AssignedNA);

			// get service code lists
			prlBltRes->ServiceUserStr = olArray[6];
			ParsePrefExclAssiStr(prlBltRes->ServiceUserStr, prlBltRes->PreferredService, prlBltRes->ExcludedService, prlBltRes->AssignedService);
		}

		if(olArray.GetSize() >= 8 )
		{
			// get flight id
			prlBltRes->Flti = olArray[7];

			if (!bgConflictBeltFlti)
				prlBltRes->Flti = "";
		}

		//New version of the FIPS Rules
		if(olArray.GetSize() >= 10)
		{
			//AirCraft Parameter
			prlBltRes->ACUserStr = olArray[8];
			ParsePrefExclAssiStr(prlBltRes->ACUserStr, prlBltRes->PreferredAC,prlBltRes->ExcludedAC, prlBltRes->AssignedAC);

			//AirCraft Groups
			prlBltRes->ACGroupUserStr = olArray[9];
			ParsePrefExclAssiStr(prlBltRes->ACGroupUserStr, prlBltRes->PreferredACGroup,prlBltRes->ExcludedACGroup, prlBltRes->AssignedACGroup);

			ExpandPrefExclAssiStrWithGroups(prlBltRes->PreferredACGroup, prlBltRes->ExcludedACGroup, prlBltRes->AssignedACGroup, prlBltRes->PreferredAC, prlBltRes->ExcludedAC, prlBltRes->AssignedAC, "ACT", "ACT3", "ACT5");
		}


		if(olArray.GetSize() >= 11 )
		{
			prlBltRes->Maxb = atoi(olArray[10]);
		}


		omBltRules.Add(prlBltRes);
	}
	omBltRules.Sort(CompareBltResByPrio);

	imMaxFBelts = max(1,imMaxFBelts);
}


/*
bool SpotAllocation::MakeBlkData(CTime &opStartTime, CTime &opEndTime,const CString &opTabn,const CString &opName, CStringArray &opNafr, CStringArray &opNato, CStringArray &opResn, CUIntArray &opIbit)
{
    CCSPtrArray<BLKDATA> olBlkData;	// background bar


	if (opStartTime <= TIMENULL)
		return false;

	if (opEndTime <= TIMENULL)
		return false;

	CTime opTrafficDay =TIMENULL;
	int ipDOO = -1;
	CString olDOO;

	opNafr.RemoveAll();
	opNato.RemoveAll();
	opResn.RemoveAll();

	CString olObjName;
	CString olUrno;

	if (strcmp(opTabn,"CIC") == 0)
		olObjName = "CNAM";
	if (strcmp(opTabn,"CHU") == 0)
		olObjName = "CNAM";
	if (strcmp(opTabn,"BLT") == 0)
		olObjName = "BNAM";
	if (strcmp(opTabn,"PST") == 0)
		olObjName = "PNAM";
	if (strcmp(opTabn,"GAT") == 0)
		olObjName = "GNAM";
	if (strcmp(opTabn,"WRO") == 0)
		olObjName = "WNAM";

	ogBCD.GetField(opTabn, olObjName, opName, "URNO", olUrno);

	CString opObject = "BLK";
	int ilBlkUrnoIdx = ogBCD.GetFieldIndex(opObject,"URNO");
	int ilBlkBurnIdx = ogBCD.GetFieldIndex(opObject,"BURN");
	int ilBlkDaysIdx = ogBCD.GetFieldIndex(opObject,"DAYS");
	int ilBlkNafrIdx = ogBCD.GetFieldIndex(opObject,"NAFR");
	int ilBlkNatoIdx = ogBCD.GetFieldIndex(opObject,"NATO");
	int ilBlkResnIdx = ogBCD.GetFieldIndex(opObject,"RESN");
	int ilBlkTabnIdx = ogBCD.GetFieldIndex(opObject,"TABN");
	int ilBlkTypeIdx = ogBCD.GetFieldIndex(opObject,"TYPE");
	int ilBlkTifrIdx = ogBCD.GetFieldIndex(opObject,"TIFR");
	int ilBlkTitoIdx = ogBCD.GetFieldIndex(opObject,"TITO");
	int ilBlkIbitIdx = ogBCD.GetFieldIndex(opObject,"IBIT");


	CCSPtrArray<RecordSet> olRecSet;
	RecordSet olRec = NULL;
	olRecSet.RemoveAll();
	ogBCD.GetRecords(opObject, "BURN", olUrno, &olRecSet);

	for(int i = 0; i < olRecSet.GetSize(); i++)
	{
		CString olTabn = olRecSet[i].Values[ogBCD.GetFieldIndex(opObject, "TABN")];
		if (strcmp(olTabn, opTabn) == 0)
		{
			olRec = olRecSet.GetAt(i);

			CString olCStrIbit;
			olCStrIbit = olRec[ilBlkIbitIdx];
			int ilIbit = atoi(olCStrIbit);
			if ( ilIbit < 0 )
				ilIbit = 0;

			CString	olDays = olRec[ilBlkDaysIdx];

			CTime olNafr = DBStringToDateTime(olRec[ilBlkNafrIdx]);
			CTime olNato = DBStringToDateTime(olRec[ilBlkNatoIdx]);

			CTime olStartTime = opStartTime;		
			if (olNafr != TIMENULL && olStartTime < olNafr)
				olStartTime = olNafr;
						
			CTime olEndTime   = opEndTime;		
			if (olNato != TIMENULL && olEndTime > olNato)
				olEndTime = olNato;

			if (olStartTime == TIMENULL || olEndTime == TIMENULL)
				return false;

			olStartTime = CTime(olStartTime.GetYear(),olStartTime.GetMonth(),olStartTime.GetDay(),0,0,0);
			olEndTime   = CTime(olEndTime.GetYear(),olEndTime.GetMonth(),olEndTime.GetDay(),23,59,59);

			for (CTime olCurrent = olStartTime; olCurrent <= olEndTime; olCurrent += CTimeSpan(1,0,0,0))
			{
				int ilDayOfWeek = olCurrent.GetDayOfWeek() - 1;
				if (ilDayOfWeek == 0)
					ilDayOfWeek = 7;

				if (olDays.Find((char) (ilDayOfWeek + '0')) != -1)
				{
					CTime olTTifr = -1;
					CTime olTTito = -1;

					CString olStrTifr = CString(olRec[ilBlkTifrIdx]);
					CString olStrTito = CString(olRec[ilBlkTitoIdx]);
					olStrTifr.TrimLeft();
					olStrTifr.TrimRight();
					olStrTito.TrimLeft();
					olStrTito.TrimRight();

					if (olStrTifr.IsEmpty())
						olTTifr = CTime(olCurrent.GetYear(),olCurrent.GetMonth(),olCurrent.GetDay(),0,0,0);
					else
						olTTifr = HourStringToDate(olStrTifr,olCurrent);

					if (olStrTito.IsEmpty())
						olTTito = CTime(olCurrent.GetYear(),olCurrent.GetMonth(),olCurrent.GetDay(),23,59,59);
					else
					{
						olTTito = HourStringToDate(olStrTito,olCurrent);
						if (olTTito < olTTifr)
							olTTito = HourStringToDate(olStrTito,olCurrent+CTimeSpan(1,0,0,0));
					}


					CTime olTNafr = -1;
					CTime olTNato = -1;
					if (olNafr != TIMENULL && olCurrent.GetDay() == olNafr.GetDay())
						olTNafr = olNafr;
					else
						olTNafr = CTime(olCurrent.GetYear(),olCurrent.GetMonth(),olCurrent.GetDay(),0,0,0);

					if (olNato != TIMENULL && olCurrent.GetDay() == olNato.GetDay())
						olTNato = olNato;
					else
					{
						olTNato = olTTito;
						if (olNato < olTNato)
						{
							if (olNato != TIMENULL)
								olTNato = olNato;
						}
					}

					CTime olTBlkfr = olTNafr; 
					CTime olTBlkto = olTNato;
					if (olTTito != TIMENULL && olTNato != TIMENULL)
					{
						if (olCurrent.GetDay() == olEndTime.GetDay())
							olTBlkto = olTNato;
						else
							olTBlkto = olTTito;

						if (olNato < olTTito)
							olTBlkto = olTNato;

						if (olCurrent.GetDay() == olEndTime.GetDay())
						{
							if (olTTito < olTNato)
								olTBlkto = olTTito;
						}
					}

					if (olTTifr != TIMENULL && olTNafr != TIMENULL && olTTifr > olTNafr)
						olTBlkfr = olTTifr;

					if (olTBlkfr != TIMENULL && olTBlkto != TIMENULL)
					{
						CTimeSpan olSpan =  olTBlkto - olTBlkfr;
						if (olSpan.GetTotalMinutes() > 0)
						{

							if(bgMergeBlk)
							{
							    BLKDATA *polBlkData = new BLKDATA();	
								polBlkData->StartTime = olTBlkfr;
								polBlkData->EndTime = olTBlkto;
								polBlkData->Text = olRec[ilBlkResnIdx];
								polBlkData->Ibit = ilIbit;
								olBlkData.Add(polBlkData);

							}
							else
							{
								CString olVonStr = CTimeToDBString(olTBlkfr, TIMENULL);
								CString olBisStr = CTimeToDBString(olTBlkto, TIMENULL);
								opNafr.Add(olVonStr);
								opNato.Add(olBisStr);
								opResn.Add(olRec[ilBlkResnIdx]);
								opIbit.Add(ilIbit);
							}
						}
					}
				}
			}

		}
	}
	olRecSet.DeleteAll();

	if(bgMergeBlk)
	{
		MergeBKBars(olBlkData);

	    BLKDATA *polBlkData = NULL;	

		for( int i = 0; i < olBlkData.GetSize() ; i++)
		{
			polBlkData = &olBlkData[i];

			CString olVonStr = CTimeToDBString(polBlkData->StartTime, TIMENULL);
			CString olBisStr = CTimeToDBString(polBlkData->EndTime, TIMENULL);
			opNafr.Add(olVonStr);
			opNato.Add(olBisStr);
			opResn.Add(polBlkData->Text);
			opIbit.Add(polBlkData->Ibit);

		}
		olBlkData.DeleteAll();
	}


	return true;
}

*/

//#########
bool SpotAllocation::CheckPstWithTimer(DIAFLIGHTDATA *prpFlightA, DIAFLIGHTDATA *prpFlightD, CString opPst, CCSPtrArray<KonfItem> *popKonfList, CTimeSpan *popRightBuffer, CTimeSpan *popLeftBuffer, bool bpPreCheck)
{
	if(opPst.IsEmpty())
		return true;

	PST_RESLIST *prlPstRes = GetPstRes(opPst); 

	if(prlPstRes == NULL)
		return false;

	CTime olStartAlloc;
	CTime olEndAlloc;
	DIAFLIGHTDATA *prlFlight;
	bool blActFound = false;
	RecordSet olCurrActRecord;
	bool blRet = true;
	CString olConfText;


	if(prpFlightA != NULL)
		prlFlight = prpFlightA;
	else
		prlFlight = prpFlightD;


	if (!ogPosDiaFlightData.GetPstAllocTimes(prpFlightA, prpFlightD, opPst, olStartAlloc, olEndAlloc, bpPreCheck))
	{
		return false;
	}


	CTime olTime = CTime::GetCurrentTime();

	ogBasicData.LocalToUtc(olTime);

	CTimeSpan olSpan(2,0,0,0);

	olTime -= olSpan;


	if(olEndAlloc != TIMENULL && olEndAlloc < olTime)
		return true;


	char clFPart = ' ';
	if((prpFlightA != NULL) && (prpFlightD != NULL))
		clFPart			= 'B';
 
	if((prpFlightA != NULL) && (prpFlightD == NULL))
		clFPart			= 'A';

	if((prpFlightA == NULL) && (prpFlightD != NULL))
		clFPart			= 'D';

	CTime olStartAllocBuf(olStartAlloc);
	CTime olEndAllocBuf(olEndAlloc);
 
	if(popLeftBuffer != NULL && popRightBuffer != NULL)
	{
		olStartAllocBuf -= *popLeftBuffer;
		olStartAllocBuf -= *popRightBuffer;
		olEndAllocBuf += *popRightBuffer;
		olEndAllocBuf += *popLeftBuffer;
	}
	else
	{
		if (!ogPosDiaFlightData.FlightIsOfbl(prpFlightA, prpFlightD))
		{
			olStartAllocBuf -= ogPosAllocBufferTime;
			olStartAllocBuf -= ogPosAllocBufferTime;
			olEndAllocBuf += ogPosAllocBufferTime;
			olEndAllocBuf += ogPosAllocBufferTime;
		}
	}

	////////////////////////////////
	// CHECK POSITION AVAILABILITY
	CStringArray opNafr;
	CStringArray opNato;

	for (int il = 0; il < prlPstRes->BlkData.GetSize(); il++)
	{
		CTime olNafr = prlPstRes->BlkData[il].StartTime;
		CTime olNato = prlPstRes->BlkData[il].EndTime;

		if(olNafr != TIMENULL || olNato != TIMENULL)
		{	
			if(olNafr == TIMENULL)
				olNafr = CTime(1971,1,1,0,0,0);
			if(olNato == TIMENULL)
				olNato = CTime(2020,1,1,0,0,0);
		}


		if((olNafr != TIMENULL) && (olNato != TIMENULL))
		{
			if(IsReallyOverlapped(olStartAlloc, olEndAlloc, olNafr, olNato)/*, olStartAlloc, olEndAlloc) || !IsWithIn(olStartAlloc, olEndAlloc, olNafr, olNato)*/)
			{
				if(popKonfList != NULL)
				{
					//Position not available %s - %s reason: %s
					if(bgGatPosLocal) ogBasicData.UtcToLocal(olNafr);
					if(bgGatPosLocal) ogBasicData.UtcToLocal(olNato);
					olConfText.Format(GetString(IDS_STRING1714), olNafr.Format("%H:%M %d.%m.%y"), olNato.Format("%H:%M %d.%m.%y"), prlPstRes->BlkData[il].Text);
					AddToKonfList(prpFlightA, prpFlightD, 0, ' ', IDS_STRING1714, olConfText, *popKonfList);

					blRet = false;
				}
				else
				{
					return false;
				}
			}
		}
	}

	// Check max. number of aircraft at position ( overlapp )
	int ilCountActAtPst = ogPosDiaFlightData.GetFlightsAtPosition( prpFlightA, prpFlightD, opPst, olStartAllocBuf, olEndAllocBuf);

	if( prlPstRes->Mult < ilCountActAtPst)
	{
		if(popKonfList != NULL)
		{
			//Too many aircrafts at position %s: %d Max: %d
			olConfText.Format(GetString(IDS_STRING1722), opPst, ilCountActAtPst, prlPstRes->Mult);
			AddToKonfList(prpFlightA, prpFlightD, 0, ' ', IDS_STRING1722, olConfText, *popKonfList);
			
			blRet = false;
		}
		else
		{
			return false;
		}
	}

	if (!CheckPstNeighborConf(prpFlightA, prpFlightD, opPst, popKonfList, popRightBuffer, popLeftBuffer, bpPreCheck))
		blRet = false;
	return blRet;
}

// Check the position for the given flights!
// For circular flights, this routine must be called twice! One with prpFlightA and the other one with prpFlightD!
bool SpotAllocation::CheckPst(DIAFLIGHTDATA *prpFlightA, DIAFLIGHTDATA *prpFlightD, CString opPst, CCSPtrArray<KonfItem> *popKonfList, CTimeSpan *popRightBuffer, CTimeSpan *popLeftBuffer, bool bpPreCheck)
{

	if(opPst.IsEmpty())
		return true;

	PST_RESLIST *prlPstRes = GetPstRes(opPst); 

	if(prlPstRes == NULL)
		return false;

	CTime olStartAlloc;
	CTime olEndAlloc;
	DIAFLIGHTDATA *prlFlight;
	bool blActFound = false;
	RecordSet olCurrActRecord;
	bool blRet = true;
	CString olConfText;


	if(prpFlightA != NULL)
		prlFlight = prpFlightA;
	else
		prlFlight = prpFlightD;

	//ogPosDiaFlightData.CalculatePstAllocTimes(ogPosDiaFlightData.GetRotationByRkey(prlFlight->Rkey));

	if (!ogPosDiaFlightData.GetPstAllocTimes(prpFlightA, prpFlightD, opPst, olStartAlloc, olEndAlloc, bpPreCheck))
	{
		return false;
	}

	CTime olTime = CTime::GetCurrentTime();

	ogBasicData.LocalToUtc(olTime);

	CTimeSpan olSpan(2,0,0,0);

	olTime -= olSpan;


	if(olEndAlloc != TIMENULL && olEndAlloc < olTime)
		return true;



	char clFPart = ' ';
	if((prpFlightA != NULL) && (prpFlightD != NULL))
	{
		//olStartAlloc	= prpFlightA->StartPosArr;
		//olEndAlloc		= prpFlightD->EndPosDep;
		clFPart			= 'B';
	}
 
	if((prpFlightA != NULL) && (prpFlightD == NULL))
	{
		//olStartAlloc	= prpFlightA->StartPosArr;
		//olEndAlloc		= prpFlightA->EndPosArr;
		clFPart			= 'A';
	}

	if((prpFlightA == NULL) && (prpFlightD != NULL))
	{
		//olStartAlloc	= prpFlightD->StartPosDep;
		//olEndAlloc		= prpFlightD->EndPosDep;
		clFPart			= 'D';

	}


	CTime olStartAllocBuf(olStartAlloc);
	CTime olEndAllocBuf(olEndAlloc);
 
	if(popLeftBuffer != NULL && popRightBuffer != NULL)
	{
		olStartAllocBuf -= *popLeftBuffer;
		olStartAllocBuf -= *popRightBuffer;
		olEndAllocBuf += *popRightBuffer;
		olEndAllocBuf += *popLeftBuffer;
	}
	else
	{
		if (!ogPosDiaFlightData.FlightIsOfbl(prpFlightA, prpFlightD))
		{
			olStartAllocBuf -= ogPosAllocBufferTime;
			olStartAllocBuf -= ogPosAllocBufferTime;
			olEndAllocBuf += ogPosAllocBufferTime;
			olEndAllocBuf += ogPosAllocBufferTime;
		}
	}

	//////////////////////////////
	// CHECK AIRCRAFTTYP
	CCSPtrArray<RecordSet> prlRecords;
	int ilAct = ogBCD.GetRecordsExt("ACT","ACT3","ACT5",CString(prlFlight->Act3),CString(prlFlight->Act5), &prlRecords);

	if(ilAct == 0)
	{
		if(popKonfList != NULL)
		{
			//Aircrafttyp not found: %s/%s
			olConfText.Format(GetString(IDS_STRING1713), prlFlight->Act3, prlFlight->Act5);
			AddToKonfList(prpFlightA, prpFlightD, 0, ' ', IDS_STRING1713, olConfText, *popKonfList);

			blRet = false;
		}
		else
		{
			return false;
		}
	}
	else
		olCurrActRecord = prlRecords[0];

	prlRecords.DeleteAll();
/*
//for positions no termonal available (UIF)
	// Check terminal ristriction 
	if(bgTermRestrForAl)
	{
		CString olTerp;
		ogBCD.GetField("PST", "PNAM", opPst, "TERM", olTerp);
		CString olAltTerp = ogBCD.GetFieldExt("ALT","ALC2","ALC3",prpFlightA->Alc2,prpFlightA->Alc3,"TERP");
		if( !olAltTerp.IsEmpty() && !olTerp.IsEmpty())
		{
			if (olAltTerp.Find(olTerp) < 0)
			{
				if(popKonfList != NULL)
				{
					// Terminal Position %s: %s must be %s
					olConfText.Format(GetString(IDS_STRING2729), opPst, olTerp, olAltTerp);
					AddToKonfList(prpFlightA, prpFlightD, 0, ' ', IDS_STRING2726, olConfText, *popKonfList);
					blRet = false;					
				}
				else
				{
					return false;
				}
			}
		}
	}

*/
/*
	blActFound = ogBCD.GetRecord("ACT", "ACT3", CString(prlFlight->Act3), olCurrActRecord);
	if(blActFound == false)
	{
		blActFound = ogBCD.GetRecord("ACT", "ACT5", CString(prlFlight->Act5), olCurrActRecord);
	}

	if(blActFound == false)
	{
		if(popKonfList != NULL)
		{
			//Aircrafttyp not found: %s/%s
			olConfText.Format(GetString(IDS_STRING1713), prlFlight->Act3, prlFlight->Act5);
			AddToKonfList(prpFlightA, prpFlightD, 0, ' ', IDS_STRING1713, olConfText, *popKonfList);

			blRet = false;
		}
		else
		{
			return false;
		}
	}
*/
	////////////////////////////


	////////////////////////////////
	// CHECK POSITION AVAILABILITY
	CStringArray opNafr;
	CStringArray opNato;

	for (int il = 0; il < prlPstRes->BlkData.GetSize(); il++)
	{
		CTime olNafr = prlPstRes->BlkData[il].StartTime;
		CTime olNato = prlPstRes->BlkData[il].EndTime;

		if(olNafr != TIMENULL || olNato != TIMENULL)
		{	
			if(olNafr == TIMENULL)
				olNafr = CTime(1971,1,1,0,0,0);
			if(olNato == TIMENULL)
				olNato = CTime(2020,1,1,0,0,0);
		}


		if((olNafr != TIMENULL) && (olNato != TIMENULL))
		{
			if(IsReallyOverlapped(olStartAlloc, olEndAlloc, olNafr, olNato)/*, olStartAlloc, olEndAlloc) || !IsWithIn(olStartAlloc, olEndAlloc, olNafr, olNato)*/)
			{
				if(popKonfList != NULL)
				{
					//Position not available %s - %s reason: %s
					if(bgGatPosLocal) ogBasicData.UtcToLocal(olNafr);
					if(bgGatPosLocal) ogBasicData.UtcToLocal(olNato);
					olConfText.Format(GetString(IDS_STRING1714), olNafr.Format("%H:%M %d.%m.%y"), olNato.Format("%H:%M %d.%m.%y"), prlPstRes->BlkData[il].Text);
					AddToKonfList(prpFlightA, prpFlightD, 0, ' ', IDS_STRING1714, olConfText, *popKonfList);

					blRet = false;
				}
				else
				{
					return false;
				}
			}
		}
	}



	////////////////////////////


	if(bgPositionTerminalConflict)
	{
		///////////////////////////////////////////////
		// RFC 8784 DXB CHECK TERMINAL RESTRICTION  - the terminal for a position is determined from the related gates
		if(prpFlightA != NULL)
		{
			if( !CString(prpFlightA->Stev).IsEmpty() )
			{
				if( prlPstRes->Term.Find(prpFlightA->Stev) == -1)
				{
					if(popKonfList != NULL)
					{
						olConfText.Format(GetString(IDS_STRING2837), prlPstRes->Term, opPst, CString(prpFlightA->Stev));
						AddToKonfList(prpFlightA, (DIAFLIGHTDATA *) NULL, 0, ' ', IDS_STRING2837, olConfText, *popKonfList);

						blRet = false;
					}
					else
					{
						return false;
					}

				}
			}
		}

		if(prpFlightD != NULL)
		{
			if( !CString(prpFlightD->Stev).IsEmpty() )
			{
				if( prlPstRes->Term.Find(prpFlightD->Stev) ==  -1)
				{
					if(popKonfList != NULL)
					{
						olConfText.Format(GetString(IDS_STRING2837), prlPstRes->Term, opPst, CString(prpFlightD->Stev));
						AddToKonfList((DIAFLIGHTDATA *) NULL, prpFlightD , 0, ' ', IDS_STRING2837, olConfText, *popKonfList);

						blRet = false;
					}
					else
					{
						return false;
					}

				}
			}
		}

	}





	//////////////////////////
	// CHECK Flight ID

	if (bgGATPOS)
	{
		if (!ogFltiPrio.IsEmpty() && ogFltiPrio != "?")
		{
			int blPart = 1;
			CString olFltiCheck = "";
			CString olFltiAllowed = "";
			bool blTransit = false;
			// New Flight ID for Position
			if (prpFlightA && !prpFlightD)
			{
				olFltiCheck = CString(prpFlightA->Flti);
				olFltiAllowed = prlPstRes->FltiArr;
			}
			else if (!prpFlightA && prpFlightD)
			{
				blPart = 2;
				olFltiCheck = CString(prpFlightD->Flti);
				olFltiAllowed = prlPstRes->FltiDep;
			}
			else if (prpFlightA && prpFlightD)
			{
				if (CString(prpFlightA->Flno) == CString(prpFlightD->Flno))
					blTransit = true;

				if (!blTransit)
				{
					olFltiCheck = CString(prpFlightD->Flti);
					olFltiAllowed = prlPstRes->FltiDep;
					blPart = 2;
				}
				else
				{
					olFltiCheck = CString(prpFlightD->Flti);
					olFltiAllowed = prlPstRes->FltiDep;
					blPart = 2;
					//the more restricted id from ogFltiPrio
					int ilPosArr = ogFltiPrio.Find( CString(prpFlightA->Flti) );
					int ilPosDep = ogFltiPrio.Find( CString(prpFlightD->Flti) );
					if (ilPosArr < ilPosDep && ilPosArr != -1)
					{
						olFltiCheck = CString(prpFlightA->Flti);
						olFltiAllowed = prlPstRes->FltiArr;
						blPart = 1;
					}
				}
			}


/*
			CString pol ("");
			if (blPart == 1)
				pol += "Arr";
			if (blPart == 2)
				pol += "Dep";
			if (blTransit)
				pol += "/Transit";

			pol += "    FlightId = " + olFltiCheck;
			pol += "    FlightIdPos = " + olFltiAllowed;


			::MessageBox(NULL,pol,"",MB_OK);
*/
 			if(strlen(olFltiAllowed) > 0 && strlen(olFltiCheck) > 0)
			{
				if( olFltiAllowed.Find(olFltiCheck) < 0)
				{
					if(popKonfList != NULL)
					{
						//Flight ID is %s - Allowed %s IDS_STRING2688
						olConfText.Format(GetString(IDS_STRING2688), olFltiCheck, olFltiAllowed);
						AddToKonfList(prpFlightA, prpFlightD, 0, ' ', IDS_STRING2688, olConfText, *popKonfList);
						blRet = false;
					}
					else
					{
						return false;
					}
				}
			}
		}

		CMapStringToString ARRropMapBLUE;
		CMapStringToString DEPropMapBLUE;
		CMapStringToString TMPropMap;
		CStringArray ARRropBLUE;
		CStringArray DEPropBLUE;
		CStringArray TMProp;
		CString olGateIDsARR;
		CString olGateIDsDEP;

		if (prpFlightA)
		{
			if (GetResBy("GAT", "PST", opPst,ARRropBLUE,ARRropMapBLUE,TMProp,TMPropMap, prpFlightA, true, true, false, true))
			{
				POSITION pos;
				CString olKey;
				CString olValue;
				for( pos = ARRropMapBLUE.GetStartPosition(); pos != NULL; )
				{
					ARRropMapBLUE.GetNextAssoc( pos, olKey , olValue );
					GAT_RESLIST *prlGatRes = GetGatRes(olKey); 
					if(prlGatRes != NULL && !CString(prlGatRes->Flti).IsEmpty())
					{
						if( olGateIDsARR.Find( CString(prlGatRes->Flti)) < 0)
							olGateIDsARR += CString(prlGatRes->Flti) + CString("/");
					}
				}
			}
		}

		if (prpFlightD)
		{
			if (GetResBy("GAT", "PST", opPst,DEPropBLUE,DEPropMapBLUE,TMProp,TMPropMap, prpFlightD, true, true, false, true))
			{
				POSITION pos;
				CString olKey;
				CString olValue;
				for( pos = DEPropMapBLUE.GetStartPosition(); pos != NULL; )
				{
					DEPropMapBLUE.GetNextAssoc( pos, olKey , olValue );
					GAT_RESLIST *prlGatRes = GetGatRes(olKey); 
					if(prlGatRes != NULL && !CString(prlGatRes->FltiDep).IsEmpty())
					{
						if( olGateIDsDEP.Find( CString(prlGatRes->FltiDep)) < 0)
							olGateIDsDEP += CString(prlGatRes->FltiDep) + CString("/");
					}
				}
			}
		}


		if(!olGateIDsDEP.IsEmpty() || !olGateIDsARR.IsEmpty())
		{
			CString olFltiA;
			CString olFltiD;


			if(prpFlightA != NULL)
			{
				olFltiA = prpFlightA->Flti;
			}
			if(prpFlightD != NULL)
			{
				olFltiD = prpFlightD->Flti;
			}


			if(!olFltiA.IsEmpty() && !olGateIDsARR.IsEmpty())
			{
				if( olGateIDsARR.Find( CString(olFltiA)) < 0)
				{
					if(popKonfList != NULL)
					{
						//Position %s: Flight ID from the related Gate is %s must be %s 
						olConfText.Format(GetString(IDS_STRING1715),  opPst, olGateIDsARR, olFltiA);
						AddToKonfList(prpFlightA, (DIAFLIGHTDATA *) NULL, 0, ' ', IDS_STRING1715, olConfText, *popKonfList);

						blRet = false;
					}
					else
					{
						return false;
					}
				}
			}

			if(!olFltiD.IsEmpty() &&  !olGateIDsDEP.IsEmpty())
			{
				if( olGateIDsDEP.Find( CString(olFltiD)) < 0)
				{
					if(popKonfList != NULL)
					{
						//Position %s: Flight ID from the related Gate is %s must be %s 
						olConfText.Format(GetString(IDS_STRING1715),  opPst, olGateIDsDEP, olFltiD);
						AddToKonfList(NULL, prpFlightD, 0, ' ', IDS_STRING1715, olConfText, *popKonfList);

						blRet = false;
					}
					else
					{
						return false;
					}
				}
			}
		}

		ARRropMapBLUE.RemoveAll();
		DEPropMapBLUE.RemoveAll();
		TMPropMap.RemoveAll();
		ARRropBLUE.RemoveAll();
		DEPropBLUE.RemoveAll();
		TMProp.RemoveAll();

	}
/*
	else
	{
		CString olRelGatFlti;
		for (int i = 0; i < prlPstRes->RelGats.GetSize(); i++)
		{
			GAT_RESLIST *prlGatRes = GetGatRes(prlPstRes->RelGats[i]); 

			if(prlGatRes != NULL)
			{
				olRelGatFlti += prlGatRes->Flti + CString("/");
			}
		}
		olRelGatFlti = olRelGatFlti.Left(olRelGatFlti.GetLength() - 1);

		if (olRelGatFlti == "/")
			olRelGatFlti.Empty();

		if(!olRelGatFlti.IsEmpty())
		{
			CString olFltiA;
			CString olFltiD;


			if(prpFlightA != NULL)
			{
				olFltiA = prpFlightA->Flti;
			}
			if(prpFlightD != NULL)
			{
				olFltiD = prpFlightD->Flti;
			}
			if((olFltiD == "I" && olFltiA == "D") || (olFltiA == "I" && olFltiD == "D"))
			{
				olFltiD = "M";
				olFltiA = "M";
			}


			if(!olFltiA.IsEmpty())
			{
				if( olRelGatFlti.Find( CString(olFltiA)) < 0)
				{
					if(popKonfList != NULL)
					{
						//Position %s: Flight ID from the related Gate is %s must be %s 
						olConfText.Format(GetString(IDS_STRING1715),  opPst, olRelGatFlti, olFltiA);
						AddToKonfList(prpFlightA, (DIAFLIGHTDATA *) NULL, 0, ' ', IDS_STRING1715, olConfText, *popKonfList);

						blRet = false;
					}
					else
					{
						return false;
					}
				}
			}

			if(!olFltiD.IsEmpty() && olFltiA != olFltiD)
			{
				if( olRelGatFlti.Find( CString(olFltiD)) < 0)
				{
					if(popKonfList != NULL)
					{
						//Position %s: Flight ID from the related Gate is %s must be %s 
						olConfText.Format(GetString(IDS_STRING1715),  opPst, olRelGatFlti, olFltiD);
						AddToKonfList(NULL, prpFlightD, 0, ' ', IDS_STRING1715, olConfText, *popKonfList);

						blRet = false;
					}
					else
					{
						return false;
					}
				}
			}
		}
	}
	/////////////////////////////
*/
	///////////////////////////////////////////////
	// CHECK NATURE RESTRICTION FROM NATURE TABLE
	if(prpFlightA != NULL)
	{
		if( !CString(prpFlightA->Ttyp).IsEmpty() )
		{
			if(FindInStrArray(omPstNatNoAllocList, prpFlightA->Ttyp))
//			if( omPstNatNoAllocList.Find(prpFlightA->Ttyp) >= 0)
			{

				if(popKonfList != NULL)
				{
					//Global nature ristriction (%s) at position %s: %s
					olConfText.Format(GetString(IDS_STRING1712), omPstNatNoAllocListPlain, opPst, CString(prpFlightA->Ttyp));
					AddToKonfList(prpFlightA, (DIAFLIGHTDATA *) NULL, 0, ' ', IDS_STRING1712, olConfText, *popKonfList);

					blRet = false;
				}
				else
				{
					return false;
				}

			}
		}
	}

	if(prpFlightD != NULL)
	{
		if( !CString(prpFlightD->Ttyp).IsEmpty() )
		{
			if(FindInStrArray(omPstNatNoAllocList, prpFlightD->Ttyp))
//			if( omPstNatNoAllocList.Find(prpFlightD->Ttyp) >= 0)
			{
				if(popKonfList != NULL)
				{
					//Global nature ristriction (%s) at position %s: %s
					olConfText.Format(GetString(IDS_STRING1712), omPstNatNoAllocListPlain, opPst, CString(prpFlightD->Ttyp));
					AddToKonfList((DIAFLIGHTDATA *) NULL, prpFlightD, 0, ' ', IDS_STRING1712, olConfText, *popKonfList);

					blRet = false;
				}
				else
				{
					return false;
				}

			}
		}
	}

	///////////////////////////////



	///////////////////////////////
	////// CHECK PUSHBACK CONF
	CCSPtrArray<DIAFLIGHTDATA> rlFlights;
	DIAFLIGHTDATA *prlFlightTmp;
	CTimeSpan olSpan10 = CTimeSpan(0,0,10,0);

	int ilLinksCount = omPstLinksToTwy.GetSize();

	if(prpFlightD != NULL)
	{
		if(!CString(opPst).IsEmpty() && prpFlightD->Airb == TIMENULL)
		{
			for(int g = 0; g < ilLinksCount; g++)
			{
				if(omPstLinksToTwy[g].Find(CString(opPst)) >= 0)
				{
					ogPosDiaFlightData.GetFlightsAtPositions( omPstLinksToTwy[g], rlFlights);

					for(int k = rlFlights.GetSize() -1; k >= 0; k--)
					{
						prlFlightTmp =  &rlFlights[k];

						if( CString(prlFlightTmp->Adid) == "A" && prlFlightTmp->Onbl == TIMENULL)
						{
							CTimeSpan olSpan = prlFlightTmp->Tifa - prpFlightD->Tifd;

							if( olSpan10.GetTotalMinutes() >  abs(olSpan.GetTotalMinutes())  )
							{

								if(popKonfList != NULL)
								{
									//Pushback conflict at position %s with flight %s
									olConfText.Format(GetString(IDS_STRING1816), opPst, prlFlightTmp->Flno);
									AddToKonfList((DIAFLIGHTDATA *)NULL, prpFlightD, 0, ' ', IDS_STRING1816, olConfText, *popKonfList);
									blRet = false;
								}
								else
								{
									return false;
								}
								
							}
						}
					}
				}
			}	
		}
	}


/*
	if(prpFlightA != NULL)
	{
		if(!CString(opPst).IsEmpty() && prpFlightA->Onbl == TIMENULL)
		{
			for(int g = 0; g < ilLinksCount; g++)
			{
				if(omPstLinksToTwy[g].Find(CString(opPst)) >= 0)
				{
					ogPosDiaFlightData.GetFlightsAtPositions( omPstLinksToTwy[g], rlFlights);

					for(int k = rlFlights.GetSize() -1; k >= 0; k--)
					{
						prlFlightTmp =  &rlFlights[k];

						if( CString(prlFlightTmp->Adid) == "D" && prlFlightTmp->Airb == TIMENULL)
						{
							CTimeSpan olSpan = prpFlightA->Tifa - prlFlightTmp->Tifd;

							if( olSpan10.GetTotalMinutes() >  abs(olSpan.GetTotalMinutes())  )
							{

								if(popKonfList == NULL)
								{
									return false; // ?? Kein echter Konflikt?? Ama fragen!
								}
								
							}
						}
					}
				}
			}	
		}
	}
	//////////////////////////////
*/

	// Check service ristriction from pos-rules 
	if(prlPstRes->AssignedService.GetSize() > 0 || prlPstRes->PreferredService.GetSize() > 0 || prlPstRes->ExcludedService.GetSize() > 0)
	{
		if(prpFlightA != NULL)
		{
			if( strlen(prpFlightA->Styp) > 0 )
			{
				if (CheckResToResLists(prpFlightA->Styp, prlPstRes->AssignedService, prlPstRes->ExcludedService, prlPstRes->PreferredService) == false)
				{
					if(popKonfList != NULL)
					{
						//Position %s: Service ristriction (%s): %s
						olConfText.Format(GetString(IDS_STRING2631), opPst, prlPstRes->ServiceUserStr, prpFlightA->Styp);
//						AddToKonfList(prpFlightA, (DIAFLIGHTDATA *)NULL, 0, ' ', IDS_STRING1716, olConfText, *popKonfList);
						AddToKonfList(prpFlightA, (DIAFLIGHTDATA *)NULL, 0, ' ', IDS_STRING2631, olConfText, *popKonfList);
						blRet = false;
					}
					else
					{
						return false;
					}
				}
			}
		}

		if(prpFlightD != NULL)
		{
			if( strlen(prpFlightD->Styp) > 0 )
			{
				if (CheckResToResLists(prpFlightD->Styp, prlPstRes->AssignedService, prlPstRes->ExcludedService, prlPstRes->PreferredService) == false)
				{
					if(popKonfList != NULL)
					{
						//Position %s: Service ristriction (%s): %s
						olConfText.Format(GetString(IDS_STRING2631), opPst, prlPstRes->ServiceUserStr, prpFlightD->Styp);
						AddToKonfList((DIAFLIGHTDATA *)NULL, prpFlightD, 0, ' ', IDS_STRING1716, olConfText, *popKonfList);
						blRet = false;
					}
					else
					{
						return false;
					}
				}
			}
		}
	}



	// Check nature ristriction from pos-rules (  not nature table )
	// Check nature ristriction from pos-rules (  not nature table )
	if(prlPstRes->PreferredNA.GetSize() > 0 || prlPstRes->ExcludedNA.GetSize() > 0 || prlPstRes->AssignedNA.GetSize() > 0)
	{
		if(prpFlightA != NULL)
		{
			if( strlen(prpFlightA->Ttyp) > 0 )
			{
				if (CheckResToResLists(prpFlightA->Ttyp, prlPstRes->AssignedNA, prlPstRes->ExcludedNA, prlPstRes->PreferredNA) == false)
				{
					if(popKonfList != NULL)
					{
						//Position %s: Nature ristriction (%s): %s
						olConfText.Format(GetString(IDS_STRING1716), opPst, prlPstRes->NAUserStr, prpFlightA->Ttyp);
						AddToKonfList(prpFlightA, (DIAFLIGHTDATA *)NULL, 0, ' ', IDS_STRING1716, olConfText, *popKonfList);
						blRet = false;
					}
					else
					{
						return false;
					}
				}
			}
		}

		if(prpFlightD != NULL)
		{
			if( strlen(prpFlightD->Ttyp) > 0 )
			{
				if (CheckResToResLists(prpFlightD->Ttyp, prlPstRes->AssignedNA, prlPstRes->ExcludedNA, prlPstRes->PreferredNA) == false)
				{
					if(popKonfList != NULL)
					{
						//Position %s: Nature ristriction (%s): %s
									olConfText.Format(GetString(IDS_STRING1716), opPst, prlPstRes->NAUserStr, prpFlightD->Ttyp);
									AddToKonfList((DIAFLIGHTDATA *)NULL, prpFlightD, 0, ' ', IDS_STRING1716, olConfText, *popKonfList);
						blRet = false;
					}
					else
					{
						return false;
					}
				}
			}
		}
	}


	// Check aircraft restrictions at position
	if(prlPstRes->PreferredAC.GetSize() > 0 || prlPstRes->ExcludedAC.GetSize() > 0 || prlPstRes->AssignedAC.GetSize() > 0)
	{
		if (CheckResToResLists(prlFlight->Act3, prlFlight->Act5, prlPstRes->AssignedAC, prlPstRes->ExcludedAC, prlPstRes->PreferredAC) == false)
		{
			if(popKonfList != NULL)
			{
				// Position %s: Aircraft restriction (%s): %s
				olConfText.Format(GetString(IDS_STRING1717), opPst, prlPstRes->ACUserStr + prlPstRes->ACGroupUserStr, CString(prlFlight->Act3)+"/"+CString(prlFlight->Act5));
				AddToKonfList(prpFlightA, prpFlightD, 0, ' ', IDS_STRING1717, olConfText, *popKonfList);
				blRet = false;
			}
			else
			{
				return false;
			}
		}
	}


	if(prlPstRes->PreferredAL.GetSize() > 0 || prlPstRes->ExcludedAL.GetSize() > 0 || prlPstRes->AssignedAL.GetSize() > 0)
	{

		bool blReturn;
		CString olDes;
		if((prpFlightD != NULL) )
		{
			blReturn = CheckResToResLists(prpFlightD->Alc2, prpFlightD->Alc3, prpFlightD->Des3, prpFlightD->Des4, prlPstRes->AssignedAL, prlPstRes->ExcludedAL, prlPstRes->PreferredAL);

			olDes = " ("+CString(prpFlightD->Des3)+"/"+CString(prpFlightD->Des4)+")";
			if ( blReturn == false)
			{
				if(popKonfList != NULL)
				{
					if(!bgPosDiaAutoAllocateInWork)
					{
						// Position %s: Airline restriction (%s): %s
						olConfText.Format(GetString(IDS_STRING1718), opPst, prlPstRes->ALUserStr, CString(prlFlight->Alc2)+"/"+CString(prlFlight->Alc3)+olDes);
						AddToKonfList(prpFlightD, 'D', 0, ' ', IDS_STRING1718, olConfText, *popKonfList);
						blRet = false;
					}
				}
				else
				{
					return false;
				}
			}

		
		}
		else
		{
			blReturn = CheckResToResLists( prpFlightA->Alc2, prpFlightA->Alc3, CString(""), CString(""), prlPstRes->AssignedAL, prlPstRes->ExcludedAL, prlPstRes->PreferredAL);

			if ( blReturn == false)
			{
				if(popKonfList != NULL)
				{
					if(!bgPosDiaAutoAllocateInWork)
					{
						// Position %s: Airline restriction (%s): %s
						olConfText.Format(GetString(IDS_STRING1718), opPst, prlPstRes->ALUserStr, CString(prlFlight->Alc2)+"/"+CString(prlFlight->Alc3)+olDes);
						AddToKonfList(prpFlightA, 'A', 0, ' ', IDS_STRING1718, olConfText, *popKonfList);
						blRet = false;
					}
				}
				else
				{
					return false;
				}
			}
		
		
		}
	}







	/* old
	// Check airline restrictions at position
	if(prlPstRes->PreferredAL.GetSize() > 0 || prlPstRes->ExcludedAL.GetSize() > 0 || prlPstRes->AssignedAL.GetSize() > 0)
	{
		if (CheckResToResLists(prlFlight->Alc2, prlFlight->Alc3, prlPstRes->AssignedAL, prlPstRes->ExcludedAL, prlPstRes->PreferredAL) == false)
		{
			if(popKonfList != NULL)
			{
				// Position %s: Airline restriction (%s): %s
				olConfText.Format(GetString(IDS_STRING1718), opPst, prlPstRes->ALUserStr, CString(prlFlight->Alc2)+"/"+CString(prlFlight->Alc3));
				AddToKonfList(prpFlightA, prpFlightD, 0, ' ', IDS_STRING1718, olConfText, *popKonfList);
				blRet = false;
			}
			else
			{
				return false;
			}
		}
	}
	*/




	// Check org/des restrictions for gate
	if(prlPstRes->PreferredOrgDes.GetSize() > 0 || prlPstRes->ExcludedOrgDes.GetSize() > 0 || prlPstRes->AssignedOrgDes.GetSize() > 0)
	{
		CString olOrgDes3;
		CString olOrgDes4;

		CString olVia3;
		CString olVia4;

		if(prpFlightD != NULL)
		{
				olOrgDes3 = prpFlightD->Des3;
				olOrgDes4 = prpFlightD->Des4;

				if( atoi(prpFlightD->Vian) > 0)
				{
					CString olVia = CString(prpFlightD->Vial); 

					olVia3 = olVia.Mid(1,3);
					olVia4 = olVia.Mid(4,4);
				}

			if (CheckResToResLists2(olOrgDes3, olOrgDes4, olVia3, olVia4, prlPstRes->AssignedOrgDes, prlPstRes->ExcludedOrgDes, prlPstRes->PreferredOrgDes) == false)
			{
				if(popKonfList != NULL)
				{
					// Position %s: Orig/Dest restriction (%s): %s
					olConfText.Format(GetString(IDS_STRING2834), opPst, prlPstRes->OrgDesUserStr, olOrgDes3+"/"+olOrgDes4);
					//AddToKonfList(prpFlight, cpFPart, 0, ' ', IDS_STRING2006, olConfText, *popKonfList);
					AddToKonfList( NULL, prpFlightD, 0, ' ', IDS_STRING2834, olConfText, *popKonfList );
					blRet = false;
				}
				else
				{
					return false;
				}
			}
		}
	}







	// 050301 MVy: check Aircraft Registration restrictions
	//	how a hurt rule can be dicovered?
	//	the current flight (A and D) contains its registrations
	if (bgRuleforAcrRegn)
	{
		if( prlPstRes->PreferredAcrRegn.GetSize() > 0 || prlPstRes->ExcludedAcrRegn.GetSize() > 0 || prlPstRes->AssignedAcrRegn.GetSize() > 0 )		// check only if there are data specified in rule
		{
			if( CheckResToResLists( prlFlight->Regn, prlPstRes->AssignedAcrRegn, prlPstRes->ExcludedAcrRegn, prlPstRes->PreferredAcrRegn ) == false )		// continue if rule is hurt
			{
				if( popKonfList )		// if an array exists for confirmations
				{
					// Aircraft Registration %s: restriction (%s): %s"
					olConfText.Format( GetString( IDS_STRING2690 ), opPst, prlPstRes->AcrRegnUserStr, prlFlight->Regn );
					AddToKonfList( prpFlightA, prpFlightD, 0, ' ', IDS_STRING2690, olConfText, *popKonfList );
					blRet = false ;
				}
				else
				{
					return false ;
				};
			};
		}
	}

	// Check max wing size
	double dlAcws = (double) atof(olCurrActRecord[ ogBCD.GetFieldIndex("ACT","ACWS")]);

	if((dlAcws > 0) && ( (dlAcws > prlPstRes->SpanMax && prlPstRes->SpanMax > 0) ||  (dlAcws < prlPstRes->SpanMin && prlPstRes->SpanMin > 0)))
	{
		if(popKonfList != NULL)
		{
			//Aircraft wingspan at position %s: %.2f (%.2f-%.2f) %s %s
			olConfText.Format(GetString(IDS_STRING1719), opPst, dlAcws, prlPstRes->SpanMin, prlPstRes->SpanMax,  prlFlight->Act3, prlFlight->Act5);
			AddToKonfList(prpFlightA, prpFlightD, 0, ' ', IDS_STRING1719, olConfText, *popKonfList);
			blRet = false;
		}
		else
		{
			return false;
		}
	}



	// Check aircraft height
 	double dlAche = (double) atof(olCurrActRecord[ ogBCD.GetFieldIndex("ACT","ACHE")]);

	if((dlAche > 0) && ( (dlAche > prlPstRes->HeighMax && prlPstRes->HeighMax > 0) ||  ( dlAche < prlPstRes->HeighMin && prlPstRes->HeighMin > 0)))
	{
		if(popKonfList != NULL)
		{
			//Aircraft height at position %s: %.2f (%.2f-%.2f) %s/%s
			olConfText.Format(GetString(IDS_STRING1720), opPst, dlAche, prlPstRes->HeighMin, prlPstRes->HeighMax,  prlFlight->Act3, prlFlight->Act5);
			AddToKonfList(prpFlightA, prpFlightD, 0, ' ', IDS_STRING1720, olConfText, *popKonfList);
			blRet = false;
		}
		else
		{
			return false;
		}
	}


	// Check aircraft length

	double dlAcle = (double) atof(olCurrActRecord[ ogBCD.GetFieldIndex("ACT","ACLE")]);

	if((dlAcle > 0) && ( (dlAcle > prlPstRes->LengMax && prlPstRes->LengMax > 0) ||  ( dlAcle < prlPstRes->LengMin && prlPstRes->LengMin > 0)))
	{
		if(popKonfList != NULL)
		{
			//Aircraft lenght at position %s: %.2f (%.2f-%.2f) %s/%s
			olConfText.Format(GetString(IDS_STRING1721), opPst, dlAcle, prlPstRes->LengMin, prlPstRes->LengMax,  prlFlight->Act3, prlFlight->Act5);
			AddToKonfList(prpFlightA, prpFlightD, 0, ' ', IDS_STRING1721, olConfText, *popKonfList);
			blRet = false;
		}
		else
		{
			return false;
		}
	}

	/*
	if (prpFlightA != NULL && strcmp(prpFlightA->Flno, "KL 1577") == 0)
		int debug = 2;
*/

	// Check max. number of aircraft at position ( overlapp )
	int ilCountActAtPst = ogPosDiaFlightData.GetFlightsAtPosition( prpFlightA, prpFlightD, opPst, olStartAllocBuf, olEndAllocBuf);

	if( prlPstRes->Mult < ilCountActAtPst)
	{
		if(popKonfList != NULL)
		{
			//Too many aircrafts at position %s: %d Max: %d
			olConfText.Format(GetString(IDS_STRING1722), opPst, ilCountActAtPst, prlPstRes->Mult);
			AddToKonfList(prpFlightA, prpFlightD, 0, ' ', IDS_STRING1722, olConfText, *popKonfList);
			
			blRet = false;
		}
		else
		{
			return false;
		}
	}


	rlFlights.RemoveAll();
 
	///////////////////////////////
	//// Max/Min Pax

	if(bgPosRulePax)
	{
		if(prpFlightA != NULL && prlPstRes->Minp > 0)
		{
			if( atoi(prpFlightA->Paxt) > 0 )
			{
				if( atoi(prpFlightA->Paxt) <  prlPstRes->Minp)
				{
					if(popKonfList != NULL)
					{
						olConfText.Format(GetString(IDS_STRING2941), opPst, prlPstRes->Minp, atoi(prpFlightA->Paxt));
						AddToKonfList(prpFlightA, 'A', 0, ' ', IDS_STRING2941, olConfText, *popKonfList);

						blRet = false;
					}
					else
					{
						return false;
					}

				}
			}
		}
		if(prpFlightA != NULL && prlPstRes->Maxp > 0)
		{
			if( atoi(prpFlightA->Paxt) > 0 )
			{
				if( atoi(prpFlightA->Paxt) >  prlPstRes->Maxp)
				{
					if(popKonfList != NULL)
					{
						olConfText.Format(GetString(IDS_STRING2943), opPst, prlPstRes->Maxp, atoi(prpFlightA->Paxt));
						AddToKonfList(prpFlightA, 'A', 0, ' ', IDS_STRING2943, olConfText, *popKonfList);

						blRet = false;
					}
					else
					{
						return false;
					}

				}
			}
		}

		if(prpFlightD != NULL && prlPstRes->Minp > 0)
		{
			if( atoi(prpFlightD->Paxt) > 0 )
			{
				if( atoi(prpFlightD->Paxt) <  prlPstRes->Minp)
				{
					if(popKonfList != NULL)
					{
						olConfText.Format(GetString(IDS_STRING2941), opPst, prlPstRes->Minp, atoi(prpFlightD->Paxt));
						AddToKonfList(prpFlightD, 'D', 0, ' ', IDS_STRING2941, olConfText, *popKonfList);

						blRet = false;
					}
					else
					{
						return false;
					}

				}
			}
		}
		if(prpFlightD != NULL && prlPstRes->Maxp > 0)
		{
			if( atoi(prpFlightD->Paxt) > 0 )
			{
				if( atoi(prpFlightD->Paxt) >  prlPstRes->Maxp)
				{
					if(popKonfList != NULL)
					{
						olConfText.Format(GetString(IDS_STRING2943), opPst, prlPstRes->Maxp, atoi(prpFlightD->Paxt));
						AddToKonfList(prpFlightD, 'D', 0, ' ', IDS_STRING2943, olConfText, *popKonfList);

						blRet = false;
					}
					else
					{
						return false;
					}

				}
			}
		}



	}




	///////////////////////////////
	//// POSITION-GATE RELATION
	if (!bgGATPOS)
	{
		if (ogKonflikte.IsConflictActivated(IDS_STRING2001))
		{
			if (prpFlightA != NULL)
			{
				if (!CheckPosGateRelation(prpFlightA, 'A', opPst))
				{
					if(popKonfList != NULL)
					{
						//Invalid Position - Gate Relation!
						AddToKonfList(prpFlightA, (DIAFLIGHTDATA *)NULL, 0, ' ', IDS_STRING2001, GetString(IDS_STRING2001), *popKonfList);
						blRet = false;
					}
					else
					{
						return false;
					}
				}
			}
			if (prpFlightD != NULL)
			{
				if (!CheckPosGateRelation(prpFlightD, 'D', opPst))
				{
					if(popKonfList != NULL)
					{
						//Invalid Position - Gate Relation!
						AddToKonfList((DIAFLIGHTDATA *)NULL, prpFlightD, 0, ' ', IDS_STRING2001, GetString(IDS_STRING2001), *popKonfList);
						blRet = false;
					}
					else
					{
						return false;
					}
				}
			}
		}
	}


	if(bgPstRuleFlno)
	{
		//Flight No. Restriction
		if(prlPstRes->PreferredFlightNo.GetSize() > 0 || prlPstRes->ExcludedFlightNo.GetSize() > 0 || prlPstRes->AssignedFlightNo.GetSize() > 0)
		{
			if(prpFlightA != NULL)
			{
				if(strlen(prpFlightA->Flno) > 0 )
				{
					if (CheckResToResLists(prpFlightA->Flno, prlPstRes->AssignedFlightNo, prlPstRes->ExcludedFlightNo, prlPstRes->PreferredFlightNo) == false)
					{
						if(popKonfList != NULL)
						{
							olConfText.Format(GetString(IDS_STRING2990),opPst,prlPstRes->FlightNoUserStr);
							AddToKonfList(prpFlightA, (DIAFLIGHTDATA *)NULL, 0, ' ', IDS_STRING2990, olConfText, *popKonfList);
							blRet = false;
						}
						else
							return false;
						
					}

				}
			}

			if(prpFlightD != NULL)
			{
				if( strlen(prpFlightD->Flno) > 0 )
				{
					if (CheckResToResLists(prpFlightD->Flno, prlPstRes->AssignedFlightNo, prlPstRes->ExcludedFlightNo, prlPstRes->PreferredFlightNo) == false)
					{
						if(popKonfList != NULL)
						{
							olConfText.Format(GetString(IDS_STRING2990),opPst,prlPstRes->FlightNoUserStr);
							AddToKonfList((DIAFLIGHTDATA *)NULL, prpFlightD, 0, ' ', IDS_STRING2990, olConfText, *popKonfList);
							blRet = false;
						}
						else
							return false;
						
					}

				}
			}
			
		}
	}



	//// BoardingBridge - BusGate Relation
	GAT_RESLIST *prlGatRes;
		
	bool blNoConf = true;
	if (prpFlightA != NULL)
	{
		if (strlen(prpFlightA->Gta1) > 0)
		{
			prlGatRes = GetGatRes(prpFlightA->Gta1);
			if (prlGatRes)
			{
				if ((strcmp(prlPstRes->Brgs, "X") != 0 && strcmp(prlGatRes->Busg, "X") != 0))
					blNoConf = false;
			}
		}
		if (strlen(prpFlightA->Gta2) > 0)
		{
			prlGatRes = GetGatRes(prpFlightA->Gta2);
			if (prlGatRes)
			{
				if ((strcmp(prlPstRes->Brgs, "X") != 0 && strcmp(prlGatRes->Busg, "X") != 0))
					blNoConf = false;
			}
		}
		if (!blNoConf)
		{
			if(popKonfList != NULL)
			{
				// Invalid 'Pass. Boarding Bridge' - 'Bus Gate' Relation!
				AddToKonfList(prpFlightA, (DIAFLIGHTDATA *)NULL, 0, ' ', IDS_STRING2065, GetString(IDS_STRING2065), *popKonfList);
				blRet = false;
			}
			else
			{
				return false;
			}
		}
	}
	blNoConf = true;
	if (prpFlightD != NULL)
	{
		if (strlen(prpFlightD->Gtd1) > 0)
		{
			prlGatRes = GetGatRes(prpFlightD->Gtd1);
			if (prlGatRes)
			{
				if ((strcmp(prlPstRes->Brgs, "X") != 0 && strcmp(prlGatRes->Busg, "X") != 0))
					blNoConf = false;
			}
		}
		if (strlen(prpFlightD->Gtd2) > 0)
		{
			prlGatRes = GetGatRes(prpFlightD->Gtd2);
			if (prlGatRes)
			{
				if ((strcmp(prlPstRes->Brgs, "X") != 0 && strcmp(prlGatRes->Busg, "X") != 0))
					blNoConf = false;
			}
		}
		if (!blNoConf)
		{
			if(popKonfList != NULL)
			{
				// Invalid 'Pass. Boarding Bridge' - 'Bus Gate' Relation!
				AddToKonfList((DIAFLIGHTDATA *)NULL, prpFlightD, 0, ' ', IDS_STRING2065, GetString(IDS_STRING2065), *popKonfList);
				blRet = false;
			}
			else
			{
				return false;
			}
		}
	}




	if (!CheckPstNeighborConf(prpFlightA, prpFlightD, opPst, popKonfList, popRightBuffer, popLeftBuffer, bpPreCheck))
		blRet = false;


	return blRet;
}

// if (pbAutoAlloc) -> this function is called from the autoallocation. Need this information because buffertimes must be supported.
bool SpotAllocation::CheckWarningAllocationBufferTime(DIAFLIGHTDATA *prpFlightA, DIAFLIGHTDATA *prpFlightD, CString opPst, CCSPtrArray<KonfItem> *popKonfList, CTimeSpan *popRightBuffer, CTimeSpan *popLeftBuffer, bool pbAutoAlloc)
{

	if(opPst.IsEmpty())
		return true;

	PST_RESLIST *prlPstRes = GetPstRes(opPst); 

	if(prlPstRes == NULL)
		return false;

	CTime olStartAlloc;
	CTime olEndAlloc;
	bool blRet = true;
	CString olConfText;

	if (!ogPosDiaFlightData.GetPstAllocTimes(prpFlightA, prpFlightD, opPst, olStartAlloc, olEndAlloc))
	{
		return false;
	}

	//if autoalloc, check the groundtime in rule if necessary
	if (pbAutoAlloc && bgAutoAllocWithGroundTime)
	{
		CStringArray olArray;
		CString olGT = prlPstRes->Groundtime;
		ExtractItemList(olGT, &olArray, ',');
		if (olArray.GetSize() == 5)
		{
			CString olUnit1 = olArray.GetAt(0);
			CString olUnit2 = olArray.GetAt(3);
			olUnit1.Remove(' ');
			if (!olUnit1.IsEmpty())
			{
				CTimeSpan olSpan = olEndAlloc - olStartAlloc;
				int olSpanMin = olSpan.GetTotalMinutes();
				CString olValue1 = olArray.GetAt(1);
				int ilValue1 = atoi(olValue1);
				int ilValue2 = -1;
				CString olAnd = olArray.GetAt(2);
				if (!olAnd.IsEmpty())
				{
					olUnit2.Remove(' ');
					if (!olUnit2.IsEmpty())
					{
						CString olValue2 = olArray.GetAt(4);
						ilValue2 = atoi(olValue2);
					}
				}

				bool blGoodGT = false;
				int ilGT = ilValue1;
				if (ilValue2 == -1)
				{
					if (olUnit1 == "<")
					{
						if (olSpanMin < ilValue1)
							blGoodGT = true;
					}
					else if (olUnit1 == "<=")
					{
						if (olSpanMin <= ilValue1)
							blGoodGT = true;
					}
					else if (olUnit1 == "=")
					{
						if (olSpanMin == ilValue1)
							blGoodGT = true;
					}
					else if (olUnit1 == ">")
					{
						if (olSpanMin > ilValue1)
							blGoodGT = true;
					}
					else if (olUnit1 == ">=")
					{
						if (olSpanMin >= ilValue1)
							blGoodGT = true;
					}
				}
				else
				{
					if (olUnit1 == ">")
					{
						if (olSpanMin > ilValue1)
							blGoodGT = true;
					}
					else if (olUnit1 == ">=")
					{
						if (olSpanMin >= ilValue1)
							blGoodGT = true;
					}
					if (blGoodGT)
					{
						if (olUnit2 == "<")
						{
							if (olSpanMin < ilValue2)
								blGoodGT = true;
							else
								blGoodGT = false;
						}
						else if (olUnit2 == "<=")
						{
							if (olSpanMin <= ilValue2)
								blGoodGT = true;
							else
								blGoodGT = false;
						}
					}
				}

				if (!blGoodGT)
					return false;
			}
			else
				return true;
		}
		else
			return false;
	}

	DIAFLIGHTDATA *prlWarnFlight;
	if(prpFlightA != NULL)
		prlWarnFlight = prpFlightA;
	else
		prlWarnFlight = prpFlightD;

	char clFPart = ' ';
	if((prpFlightA != NULL) && (prpFlightD != NULL))
		clFPart			= 'B';
 
	if((prpFlightA != NULL) && (prpFlightD == NULL))
		clFPart			= 'A';

	if((prpFlightA == NULL) && (prpFlightD != NULL))
		clFPart			= 'D';


	CTime olStartAllocBuf(olStartAlloc);
	CTime olEndAllocBuf(olEndAlloc);
 
	if(popLeftBuffer != NULL && popRightBuffer != NULL)
	{
		olStartAllocBuf -= *popLeftBuffer;
		olEndAllocBuf += *popRightBuffer;
		if(CString(pcgHome) != "FCO")
		{
			olEndAllocBuf += *popLeftBuffer;
			olStartAllocBuf -= *popRightBuffer;
		}
	}
	else
	{
		olStartAllocBuf -= ogPosAllocBufferTime;
		olEndAllocBuf += ogPosAllocBufferTime;
		if(CString(pcgHome) != "FCO")
		{
			olStartAllocBuf -= ogPosAllocBufferTime;
			olEndAllocBuf += ogPosAllocBufferTime;
		}
	}

	// check if there is a buffertime viloation -> don�t generate a conflict but return false because they wan�t a warning.
	// in case of autoallocation and buffertime violation don�t allocate.
	CPtrArray olFlights;
	ogPosDiaFlightData.GetFlightsAtPosition(olStartAllocBuf, olEndAllocBuf, opPst, &olFlights);

	
	if(olFlights.GetSize() >= prlPstRes->Mult)
	{
		for (int i=0; i<olFlights.GetSize(); i++)
		{
			if(popKonfList != NULL)
			{
				DIAFLIGHTDATA *prlFlight = NULL;
				prlFlight = (DIAFLIGHTDATA *) olFlights[i];
				ASSERT(prlFlight);
				if(prlFlight)
				{
					olConfText.Format("%s, %s" ,GetString(IDS_STRING_TIMERANGE_BUFFER), prlFlight->Flno);
					AddToKonfList(prlWarnFlight, clFPart, 0, ' ', IDS_STRING_TIMERANGE_BUFFER, olConfText, *popKonfList);
					blRet = false;
				}
			}
			else
			{
				if (pbAutoAlloc)
					return false;
			}
		}
	}
	return blRet;
}

bool SpotAllocation::CheckGatNeighborConf(const DIAFLIGHTDATA *prpFlight, char cpFPart, 
										  const CString &opGat, int ipGatNo, CCSPtrArray<KonfItem> *popKonfList /* = NULL */, 
										  const CTimeSpan *popRightBuffer /* = NULL */, const CTimeSpan *popLeftBuffer /* = NULL */, bool bpPreCheck /*false*/)
{
	if(opGat.IsEmpty())
		return true;

	GAT_RESLIST *prlGatRes = GetGatRes(opGat); 
	if(prlGatRes == NULL)
		return false;

	bool blRet = true;

	if(! prpFlight)
		return true;


	// get alloc times
	CTime olStartAlloc;
	CTime olEndAlloc;
	CString olConfText;
	DIAFLIGHTDATA *prlFlightTmp = NULL;

	if (!ogPosDiaFlightData.GetGatAllocTimes(*prpFlight, cpFPart, opGat, ipGatNo, olStartAlloc, olEndAlloc, bpPreCheck))
	{
		return false;
	}

	// common gate

	CCSPtrArray<DIAFLIGHTDATA> ropFlights;
		
	if (bgCommonGate && cpFPart == 'D' && prlGatRes->CommonGates.GetSize() > 0)
	{
		CTime olStartAllocCommonGate = olStartAlloc + ogCommonGateBeginBuffer;

		int ilPax = 0;
		int ilCountActAtGat = ogPosDiaFlightData.GetMaxPaxOverlapAtCommonGates(olStartAlloc, olEndAlloc, prlGatRes->CommonGates, prpFlight, ilPax, ropFlights);

		if( prlGatRes->Maxp < ilPax)
		{
			// Max. passenger (%d/%d) at gate %s in common gate area 

			olConfText.Format(GetString(IDS_STRING2956), prlGatRes->Maxp, ilPax, opGat);

			if (popKonfList)
			{

				AddToKonfList(prpFlight, cpFPart, 0, ' ', IDS_STRING2956, olConfText, *popKonfList);

				for( int i = 0 ; i < ropFlights.GetSize(); i++)
				{
					prlFlightTmp = &ropFlights[i];

					ogUrnoMapConfCheck.SetAt((void *)prlFlightTmp->Rkey,prlFlightTmp);

				}
			}
			else
			{
				return false;
			}
		}
		ropFlights.RemoveAll();
	}
	// end common gate

	
	CTime  olStartAllocAdjGat = olStartAlloc - ogAdjacentGatBufferTime;
	CTime  olEndAllocAdjGat = olEndAlloc + ogAdjacentGatBufferTime;


	if(popRightBuffer != NULL)
		olStartAlloc -= *popRightBuffer;

	if(popLeftBuffer != NULL)
		olEndAlloc += *popLeftBuffer;


	if (blIncomAlGat || blIncomApGat || bgAdjacentGatRestriction)
	{
		prlFlightTmp = NULL;

		CMapStringToString olNeighbours;
		int ilCountNeigh = GetAllGatWhereImNeighbour(prlGatRes, olNeighbours);


		olConfText = "";
		CString olAllALTText ("");
		CString olAllAPTText ("");

		POSITION pos;
		for( pos = prlGatRes->GateNeighbours.GetStartPosition(); pos != NULL; )
		{
			CString olPos;
			CString olMac;
			prlGatRes->GateNeighbours.GetNextAssoc(pos, olPos ,olMac);


			DIAFLIGHTDATA *prpFlightTmp = NULL;

			if(bgAdjacentGatRestriction && olMac == "0" && strcmp(prpFlight->Adid, "D") == 0)
			{
  
				ogPosDiaFlightData.GetFlightsAtGate(olStartAllocAdjGat, olEndAllocAdjGat, olPos, prpFlight, &ropFlights);

				int ilCount = ropFlights.GetSize();

				//Gate is blocked because adjacent gate % is allocated

				olConfText.Format(GetString(IDS_STRING2847), olPos);

				if (ilCount > 0 )
				{

					if (popKonfList)
					{

						AddToKonfList(prpFlight, cpFPart, 0, ' ', IDS_STRING2847, olConfText, *popKonfList);


						for( int i = 0 ; i < ilCount; i++)
						{
							prpFlightTmp = &ropFlights[i];

							ogUrnoMapConfCheck.SetAt((void *)prpFlightTmp->Rkey,prpFlightTmp);

						}
					}
					else
					{
						return false;
					}
				}
			}



			if(blIncomAlGat)
			{
				//ALT
				ropFlights.RemoveAll();
				if (!CheckIncomp(CString("GAT"), CString("ALT"), prpFlight, olStartAlloc, olEndAlloc, olPos, prlFlightTmp, prlGatRes->Gnam, ropFlights, 0))
				{
					//"Incompatible Flights GAT - AL (%s: %s/%s) at Gate %s"
					for (int l=0; l < ropFlights.GetSize(); l++)
					{
						prlFlightTmp = &ropFlights[l];
						olConfText.Format(GetString(IDS_STRING2685),prlFlightTmp->Flno,prlFlightTmp->Alc2,prlFlightTmp->Alc3, olPos);
						if (olAllALTText.Find(olConfText) == -1)
						{
							if (!olAllALTText.IsEmpty())
//								olAllConfText += "; \n";
								olAllALTText += "; ";
							olAllALTText += olConfText;
						}
					}
					blRet = false;
				}
			}
			if(blIncomApGat)
			{
				//APT
				ropFlights.RemoveAll();
				CStringArray olAptArray;
				if (!CheckIncomp(CString("GAT"), CString("APT"), prpFlight, olStartAlloc, olEndAlloc, olPos, prlFlightTmp, prlGatRes->Gnam, ropFlights, 0, &olAptArray))
				{
					//"Incompatible Flights GAT - AP (%s: %s) at Gate %s"
					for (int l=0; l < ropFlights.GetSize(); l++)
					{
						prlFlightTmp = &ropFlights[l];
/*
						CString olVal1 ("");
						CString olVal2 ("");
						if (prlFlightTmp->Adid[0] == 'A')
						{
							olVal1 = prlFlightTmp->Org3;
							olVal2 = prlFlightTmp->Org4;
						}
						else if (prlFlightTmp->Adid[0] == 'D')
						{
							olVal1 = prlFlightTmp->Des3;
							olVal2 = prlFlightTmp->Des4;
						}
						else
						{
							olVal1 = prlFlightTmp->Des3;
							olVal2 = prlFlightTmp->Des4;

							if (strlen(prlFlightTmp->Des3) == 0 && strlen(prlFlightTmp->Des4) == 0)
							{
								olVal1 = prlFlightTmp->Org3;
								olVal2 = prlFlightTmp->Org4;
							}
						}
*/
						olConfText.Format(GetString(IDS_STRING2686),prlFlightTmp->Flno,olAptArray.GetAt(l), olPos);
						if (olAllAPTText.Find(olConfText) == -1)
						{
							if (!olAllAPTText.IsEmpty())
//								olAllConfText += "; \n";
								olAllAPTText += "; ";
							olAllAPTText += olConfText;
						}
					}
					blRet = false;
				}

			}
		}



		if (!olAllALTText.IsEmpty() && popKonfList)
		{
			AddToKonfList(prpFlight, cpFPart, 0, ' ', IDS_STRING2682, olAllALTText, *popKonfList);
		}
		if (!olAllAPTText.IsEmpty() && popKonfList)
		{
			AddToKonfList(prpFlight, cpFPart, 0, ' ', IDS_STRING2681, olAllAPTText, *popKonfList);
		}
	}

	return blRet;
}


// Check the position for the given flights!
// For circular flights, this routine must be called twice! One with prpFlightA and the other one with prpFlightD!
// !!! ONLY check Conflicts to Neighbors !!!
bool SpotAllocation::CheckPstNeighborConf(DIAFLIGHTDATA *prpFlightA, DIAFLIGHTDATA *prpFlightD, 
										  const CString &ropPst, CCSPtrArray<KonfItem> *popKonfList /* = NULL */, 
										  CTimeSpan *popRightBuffer /* = NULL */, CTimeSpan *popLeftBuffer /* = NULL */, bool bpPreCheck /*false*/)
{
	if(ropPst.IsEmpty())
		return true;

	PST_RESLIST *prlPstRes = GetPstRes(ropPst); 
	if(prlPstRes == NULL)
		return false;

	bool blRet = true;

	 DIAFLIGHTDATA *prlFlight;
	if(prpFlightA != NULL)
		prlFlight = prpFlightA;
	else if(prpFlightD != NULL)
		prlFlight = prpFlightD;
	else
		return false;


	// get alloc times
	CTime olStartAlloc;
	CTime olEndAlloc;

	if (!ogPosDiaFlightData.GetPstAllocTimes(prpFlightA, prpFlightD, ropPst, olStartAlloc, olEndAlloc, bpPreCheck))
	{
		return false;
	}
	
	
	char clFPart = ' ';
	if((prpFlightA != NULL) && (prpFlightD != NULL))
	{
		//olStartAlloc	= prpFlightA->StartPosArr;
		//olEndAlloc		= prpFlightD->EndPosDep;
		clFPart			= 'B';
	}
 
	if((prpFlightA != NULL) && (prpFlightD == NULL))
	{
		//olStartAlloc	= prpFlightA->StartPosArr;
		//olEndAlloc		= prpFlightA->EndPosArr;
		clFPart			= 'A';
	}

	if((prpFlightA == NULL) && (prpFlightD != NULL))
	{
		//olStartAlloc	= prpFlightD->StartPosDep;
		//olEndAlloc		= prpFlightD->EndPosDep;
		clFPart			= 'D';

	} 

	CTime olStartAllocBuf(olStartAlloc);
	CTime olEndAllocBuf(olEndAlloc);


	if(popLeftBuffer != NULL && popRightBuffer != NULL)
	{
		olStartAllocBuf -= *popLeftBuffer;
		olStartAllocBuf -= *popRightBuffer;
		olEndAllocBuf += *popRightBuffer;
		olEndAllocBuf += *popLeftBuffer;
	}
	else
	{
		if (!ogPosDiaFlightData.FlightIsOfbl(prpFlightA, prpFlightD))
		{
			olStartAllocBuf -= ogPosAllocBufferTime;
			olStartAllocBuf -= ogPosAllocBufferTime;
			olEndAllocBuf += ogPosAllocBufferTime;
			olEndAllocBuf += ogPosAllocBufferTime;
			if(bgWingoverBuffer)
			{
				CTimeSpan olWingoverBuffer = ogWingoverBuffer;
				if (olWingoverBuffer < ogPosAllocBufferTime)
					olWingoverBuffer = ogPosAllocBufferTime;

				if (!bgPosDiaAutoAllocateInWork)
				{
					olStartAllocBuf -= olWingoverBuffer;
					olStartAllocBuf -= olWingoverBuffer;
					olEndAllocBuf += olWingoverBuffer;
					olEndAllocBuf += olWingoverBuffer;
				}
			}
		}
	}

	CString olConfText;
	DIAFLIGHTDATA *prlFlightTmp = NULL;

	// get aircraft record
/*	RecordSet olCurrActRecord;
	bool blActFound = ogBCD.GetRecord("ACT", "ACT3", CString(prlFlight->Act3), olCurrActRecord);
	if(blActFound == false)
	{
		blActFound = ogBCD.GetRecord("ACT", "ACT5", CString(prlFlight->Act5), olCurrActRecord);
	}
*/

	CString olAcwsStr = ogBCD.GetFieldExt("ACT","ACT3","ACT5",CString(prlFlight->Act3),CString(prlFlight->Act5),"ACWS");

	bool blActFound = true;
	if (blActFound)
	{
		// Check wingoverlap at left position
		//double dlMaxDist;
 		//double dlMinDist = 7.5;  // Sicherheitsabstand zwischen zwei Flugzeugen
 		double dlMinDist = 0;  // Sicherheitsabstand wird bei den Angaben in den Stammdaten mit beruecksichtigt
		double dlSpan = (double) atof(olAcwsStr);
//		double dlSpan = (double) atof(olCurrActRecord[ ogBCD.GetFieldIndex("ACT","ACWS")]);

		prlFlightTmp = NULL;

		CMapStringToString olPosAndMacNeighbours;
		CMapStringToPtr olPosAndSpanNeighbours;
		int ilCountNeigh = GetAllPstWhereImNeighbour(prlPstRes, olPosAndMacNeighbours, olPosAndSpanNeighbours);


		olConfText = "";
		CString olAllConfText ("");
		CString olAllALTTextArr ("");
		CString olAllALTTextDep ("");
		CString olAllAPTTextArr ("");
		CString olAllAPTTextDep ("");

		POSITION pos;

		strcpy(prlFlight->BlockedPositons,"");

		for( pos = prlPstRes->PosAndMac.GetStartPosition(); pos != NULL; )
		{
			CString olPos;
			CString olMac;
			prlPstRes->PosAndMac.GetNextAssoc(pos, olPos ,olMac);
			double dlMac =	(double) atof(olMac);

			CCSPtrArray<DIAFLIGHTDATA> ropFlights;
			if (!CheckWingoverlap(prlFlight, olStartAllocBuf, olEndAllocBuf, olPos, prlPstRes->SpanMax, dlMac, dlMinDist, dlSpan, prlFlightTmp, prlPstRes->Pnam, ropFlights, 0))
			{
				if(popKonfList != NULL)
				{
					//Wingoverlap with aircraft (%s %s) at position %s
					for (int l=0; l < ropFlights.GetSize(); l++)
					{
						prlFlightTmp = &ropFlights[l];
						olConfText.Format(GetString(IDS_STRING1723),prlFlightTmp->Flno,prlFlightTmp->Act5, olPos);
						if (olAllConfText.Find(olConfText) == -1)
						{
							if (!olAllConfText.IsEmpty())
//								olAllConfText += "; \n";
								olAllConfText += "; ";
							olAllConfText += olConfText;
						}
					}
					blRet = false;
				}
				else
				{
					return false;
				}
			}

			if(blIncomAlPos)
			{
				//ALT
				ropFlights.RemoveAll();
				if (prpFlightA)
				{
					if (!CheckIncomp(CString("POS"), CString("ALT"), prpFlightA, olStartAllocBuf, olEndAllocBuf, olPos, prlFlightTmp, prlPstRes->Pnam, ropFlights, 0))
					{
//						if(blIncomAlPos)
						if(popKonfList != NULL)
						{
							//Incompatible Flights POS - AL (%s: %s/%s) at position %s
							for (int l=0; l < ropFlights.GetSize(); l++)
							{
								prlFlightTmp = &ropFlights[l];
								olConfText.Format(GetString(IDS_STRING2683),prlFlightTmp->Flno,prlFlightTmp->Alc2,prlFlightTmp->Alc3, olPos);
								if (olAllALTTextArr.Find(olConfText) == -1)
								{
									if (!olAllALTTextArr.IsEmpty())
		//								olAllConfText += "; \n";
										olAllALTTextArr += "; ";
									olAllALTTextArr += olConfText;
								}
							}
							blRet = false;
						}
						else
						{
							return false;
						}
					}
				}
				ropFlights.RemoveAll();
				if (prpFlightD)
				{
					if (!CheckIncomp(CString("POS"), CString("ALT"), prpFlightD, olStartAllocBuf, olEndAllocBuf, olPos, prlFlightTmp, prlPstRes->Pnam, ropFlights, 0))
					{
//						if(blIncomAlPos)
						if(popKonfList != NULL)
						{
							//Incompatible Flights POS - AL (%s: %s/%s) at position %s
							for (int l=0; l < ropFlights.GetSize(); l++)
							{
								prlFlightTmp = &ropFlights[l];
								olConfText.Format(GetString(IDS_STRING2683),prlFlightTmp->Flno,prlFlightTmp->Alc2,prlFlightTmp->Alc3, olPos);
								if (olAllALTTextDep.Find(olConfText) == -1)
								{
									if (!olAllALTTextDep.IsEmpty())
		//								olAllConfText += "; \n";
										olAllALTTextDep += "; ";
									olAllALTTextDep += olConfText;
								}
							}
							blRet = false;
						}
						else
						{
							return false;
						}
					}
				}
			}
			if(blIncomApPos)
			{
				//APT
				ropFlights.RemoveAll();
				if (prpFlightA)
				{
//					if (!CheckIncomp(CString("POS"), CString("APT"), prlFlight, olStartAllocBuf, olEndAllocBuf, olPos, prlFlightTmp, prlPstRes->Pnam, ropFlights, 0))
					CStringArray olAptArray;
					if (!CheckIncomp(CString("POS"), CString("APT"), prpFlightA, olStartAllocBuf, olEndAllocBuf, olPos, prlFlightTmp, prlPstRes->Pnam, ropFlights, 0, &olAptArray))
					{
	//					if(blIncomApPos)
						if(popKonfList != NULL)
						{
							//Incompatible Flights POS - AP (%s: %s/%s) at position %s
							for (int l=0; l < ropFlights.GetSize(); l++)
							{
								prlFlightTmp = &ropFlights[l];
/*
								CString olVal1 ("");
								CString olVal2 ("");
								if (prlFlightTmp->Adid[0] == 'A')
								{
									olVal1 = prlFlightTmp->Org3;
									olVal2 = prlFlightTmp->Org4;
								}
								else if (prlFlightTmp->Adid[0] == 'D')
								{
									olVal1 = prlFlightTmp->Des3;
									olVal2 = prlFlightTmp->Des4;
								}
								else
								{
									olVal1 = prlFlightTmp->Des3;
									olVal2 = prlFlightTmp->Des4;

									if (strlen(prlFlightTmp->Des3) == 0 && strlen(prlFlightTmp->Des4) == 0)
									{
										olVal1 = prlFlightTmp->Org3;
										olVal2 = prlFlightTmp->Org4;
									}
								}
*/
								olConfText.Format(GetString(IDS_STRING2684),prlFlightTmp->Flno,olAptArray.GetAt(l), olPos);
								if (olAllAPTTextArr.Find(olConfText) == -1)
								{
									if (!olAllAPTTextArr.IsEmpty())
		//								olAllConfText += "; \n";
										olAllAPTTextArr += "; ";
									olAllAPTTextArr += olConfText;
								}
							}
							blRet = false;
						}
						else
						{
							return false;
						}
					}
				}
				ropFlights.RemoveAll();
				if (prpFlightD)
				{
//					if (!CheckIncomp(CString("POS"), CString("APT"), prlFlight, olStartAllocBuf, olEndAllocBuf, olPos, prlFlightTmp, prlPstRes->Pnam, ropFlights, 0))
					CStringArray olAptArray;
					if (!CheckIncomp(CString("POS"), CString("APT"), prpFlightD, olStartAllocBuf, olEndAllocBuf, olPos, prlFlightTmp, prlPstRes->Pnam, ropFlights, 0, &olAptArray))
					{
	//					if(blIncomApPos)
						if(popKonfList != NULL)
						{
							//Incompatible Flights POS - AP (%s: %s/%s) at position %s
							for (int l=0; l < ropFlights.GetSize(); l++)
							{
								prlFlightTmp = &ropFlights[l];
/*
								CString olVal1 ("");
								CString olVal2 ("");
								if (prlFlightTmp->Adid[0] == 'A')
								{
									olVal1 = prlFlightTmp->Org3;
									olVal2 = prlFlightTmp->Org4;
								}
								else if (prlFlightTmp->Adid[0] == 'D')
								{
									olVal1 = prlFlightTmp->Des3;
									olVal2 = prlFlightTmp->Des4;
								}
								else
								{
									olVal1 = prlFlightTmp->Des3;
									olVal2 = prlFlightTmp->Des4;

									if (strlen(prlFlightTmp->Des3) == 0 && strlen(prlFlightTmp->Des4) == 0)
									{
										olVal1 = prlFlightTmp->Org3;
										olVal2 = prlFlightTmp->Org4;
									}
								}
*/
								olConfText.Format(GetString(IDS_STRING2684),prlFlightTmp->Flno,olAptArray.GetAt(l), olPos);
								if (olAllAPTTextDep.Find(olConfText) == -1)
								{
									if (!olAllAPTTextDep.IsEmpty())
		//								olAllConfText += "; \n";
										olAllAPTTextDep += "; ";
									olAllAPTTextDep += olConfText;
								}
							}
							blRet = false;
						}
						else
						{
							return false;
						}
					}
				}
			}


		}



		POSITION posSpan = olPosAndSpanNeighbours.GetStartPosition();
		for( pos = olPosAndMacNeighbours.GetStartPosition(); pos != NULL; )
		{
			CString olPos;
			CString olMac;
			olPosAndMacNeighbours.GetNextAssoc(pos, olPos ,olMac);
			double dlMac =	(double) atof(olMac);

			CString olPosSpan;
			double* dpSpan = NULL;
			olPosAndSpanNeighbours.GetNextAssoc(posSpan, olPosSpan ,(void *&)dpSpan);

			CCSPtrArray<DIAFLIGHTDATA> ropFlights;
			if (!CheckWingoverlap(prlFlight, olStartAllocBuf, olEndAllocBuf, olPos, *dpSpan, dlMac, dlMinDist, dlSpan, prlFlightTmp, prlPstRes->Pnam, ropFlights, 1))
			{
				if(popKonfList != NULL)
				{
					//Wingoverlap with aircraft (%s %s) at position %s
					for (int l=0; l < ropFlights.GetSize(); l++)
					{
						prlFlightTmp = &ropFlights[l];
						olConfText.Format(GetString(IDS_STRING1723),prlFlightTmp->Flno,prlFlightTmp->Act5, olPos);
						if (olAllConfText.Find(olConfText) == -1)
						{
							if (!olAllConfText.IsEmpty())
//								olAllConfText += "\n";
								olAllConfText += "; ";
							olAllConfText += olConfText;
						}
					}
					blRet = false;
				}
				else
				{
					return false;
				}
			}
		}


		if (!olAllConfText.IsEmpty())
		{
			AddToKonfList(prpFlightA, prpFlightD, 0, ' ', IDS_STRING1723, olAllConfText, *popKonfList);
		}
		if (!olAllALTTextArr.IsEmpty() && popKonfList && prpFlightA)
		{
			AddToKonfList(prpFlightA, prpFlightA->Adid[0], 0, ' ', IDS_STRING2680, olAllALTTextArr, *popKonfList);
		}
		if (!olAllALTTextDep.IsEmpty() && popKonfList && prpFlightD)
		{
			AddToKonfList(NULL, prpFlightD, 0, ' ', IDS_STRING2680, olAllALTTextDep, *popKonfList);
		}
		if (!olAllAPTTextArr.IsEmpty() && popKonfList && prpFlightA)
		{
			AddToKonfList(prpFlightA, prpFlightA->Adid[0], 0, ' ', IDS_STRING2679, olAllAPTTextArr, *popKonfList);
		}
		if (!olAllAPTTextDep.IsEmpty() && popKonfList && prpFlightD)
		{
			AddToKonfList(NULL, prpFlightD, 0, ' ', IDS_STRING2679, olAllAPTTextDep, *popKonfList);
		}
	}

	return blRet;


/*
	CString olConfText;
	DIAFLIGHTDATA *prlFlightTmp = NULL;

	// get aircraft record
	RecordSet olCurrActRecord;
	bool blActFound = ogBCD.GetRecord("ACT", "ACT3", CString(prlFlight->Act3), olCurrActRecord);
	if(blActFound == false)
	{
		blActFound = ogBCD.GetRecord("ACT", "ACT5", CString(prlFlight->Act5), olCurrActRecord);
	}


	if (blActFound)
	{
		// Check wingoverlap at left position
		//double dlMaxDist;
 		//double dlMinDist = 7.5;  // Sicherheitsabstand zwischen zwei Flugzeugen
 		double dlMinDist = 0;  // Sicherheitsabstand wird bei den Angaben in den Stammdaten mit beruecksichtigt
		double dlSpan = (double) atof(olCurrActRecord[ ogBCD.GetFieldIndex("ACT","ACWS")]);

		prlFlightTmp = NULL;

		if (!CheckWingoverlap(prlFlight, olStartAllocBuf, olEndAllocBuf, CString(prlPstRes->Pos1), prlPstRes->SpanMax, prlPstRes->Mac1, dlMinDist, dlSpan, prlFlightTmp, prlPstRes->Pnam))
		{
			if(popKonfList != NULL)
			{
				//Wingoverlap with aircraft (%s %s) at position %s
				olConfText.Format(GetString(IDS_STRING1723),prlFlightTmp->Flno,prlFlightTmp->Act5, prlPstRes->Pos1);
				AddToKonfList(prpFlightA, prpFlightD, 0, ' ', IDS_STRING1723, olConfText, *popKonfList);
				blRet = false;
			}
			else
			{
				return false;
			}
 
		}
 

		// Check wingoverlap at right position
		if (!CheckWingoverlap(prlFlight, olStartAllocBuf, olEndAllocBuf, CString (prlPstRes->Pos2), prlPstRes->SpanMax, prlPstRes->Mac2, dlMinDist, dlSpan, prlFlightTmp, prlPstRes->Pnam))
		{
 			if(popKonfList != NULL)
			{
				//Wingoverlap with aircraft (%s %s) at position %s
				olConfText.Format(GetString(IDS_STRING2030),prlFlightTmp->Flno,prlFlightTmp->Act5, prlPstRes->Pos2);
				AddToKonfList(prpFlightA, prpFlightD, 0, ' ', IDS_STRING2030, olConfText, *popKonfList);
				blRet = false;
			}
			else
			{
				return false;
			}
 
		}
	}
	
	return blRet;
*/

}


//	how a hurt rule can be dicovered? As example string I use the aircraft registration ACR.REGN
//	- if no regn is specified in rule the current flight can be of any regn type
//	- if a NOT is specified in rule the current flight must be of another regn type
//	- if a MUST is specified in rule the current flight must be of this regn type
//	- if some regn is specified in rule the current flight must be of this regn type, only in old version there was a difference in handling
// return a bool that tell you if the rule is valid
bool SpotAllocation::CheckResToResLists(const CString &ropTestStr, const CStringArray &ropAssignedStrs, const CStringArray &ropExcludedStrs, const CStringArray &ropPreferredStrs) const
{
	if (ropTestStr.IsEmpty()) return true;

	if (bgAutoAllocWithExactParameter || ropAssignedStrs.GetSize() > 0)// || ropPreferredStrs.GetSize() > 0 )
	{
		CStringArray olMustBeIn;
		olMustBeIn.Append(ropAssignedStrs);
		olMustBeIn.Append(ropPreferredStrs);

		if (olMustBeIn.GetSize() > 0 && !FindInStrArray(olMustBeIn, ropTestStr))
		{
			olMustBeIn.RemoveAll();
			return false;
		}
		olMustBeIn.RemoveAll();
	}

//	if (ropAssignedStrs.GetSize() > 0 && !FindInStrArray(ropAssignedStrs, ropTestStr))
//		return false;

	if (FindInStrArray(ropExcludedStrs, ropTestStr))
		return false;

	return true;
}

bool SpotAllocation::CheckResToResLists(const CString &ropTestStr1, const CString &ropTestStr2, const CStringArray &ropAssignedStrs, const CStringArray &ropExcludedStrs, const CStringArray &ropPreferredStrs) const
{
	if (ropTestStr1.IsEmpty() && ropTestStr2.IsEmpty()) return true;

	if (bgAutoAllocWithExactParameter || ropAssignedStrs.GetSize() > 0)// || ropPreferredStrs.GetSize() > 0 )
	{
		CStringArray olMustBeIn;
		olMustBeIn.Append(ropAssignedStrs);
		olMustBeIn.Append(ropPreferredStrs);

		if (olMustBeIn.GetSize() > 0
			&& !FindInStrArray(olMustBeIn, ropTestStr1) && !FindInStrArray(olMustBeIn, ropTestStr2))
		{
			olMustBeIn.RemoveAll();
			return false;
		}
		olMustBeIn.RemoveAll();
	}



//	if (ropAssignedStrs.GetSize() > 0 && 
//		!FindInStrArray(ropAssignedStrs, ropTestStr1) && !FindInStrArray(ropAssignedStrs, ropTestStr2))
//		return false;

	if (FindInStrArray(ropExcludedStrs, ropTestStr1) || FindInStrArray(ropExcludedStrs, ropTestStr2))
		return false;

	return true;
}


bool SpotAllocation::CheckResToResLists(const CString &ropTestStr1, const CString &ropTestStr2, const CString &ropTestStr3, const CString &ropTestStr4, const CStringArray &ropAssignedStrs, const CStringArray &ropExcludedStrs, const CStringArray &ropPreferredStrs) const
{
	if (ropTestStr1.IsEmpty() && ropTestStr2.IsEmpty()) return true;


	for (int i=0; i < ropExcludedStrs.GetSize(); i++)  
	{
		if (ropTestStr1 == ropExcludedStrs[i])
			return false;
		if (ropTestStr2 == ropExcludedStrs[i])
			return false;

		if ( (ropTestStr2 + CString(":") + ropTestStr4  ) == ropExcludedStrs[i])
			return false;

		if ( (ropTestStr1 + CString(":") + ropTestStr3  ) == ropExcludedStrs[i])
			return false;


		if(ropTestStr4.IsEmpty() && ropTestStr3.IsEmpty())
		{
			if(ropExcludedStrs[i].Find(ropTestStr1 + CString(":")) >= 0)
				return false;

			if(ropExcludedStrs[i].Find(ropTestStr2 + CString(":")) >= 0)
				return false;
		}

	}

	
	
	
	
	if (bgAutoAllocWithExactParameter || ropAssignedStrs.GetSize() > 0)// || ropPreferredStrs.GetSize() > 0 )
	{
		/*
		CStringArray olMustBeIn;
		olMustBeIn.Append(ropAssignedStrs);
		olMustBeIn.Append(ropPreferredStrs);

		if (olMustBeIn.GetSize() > 0 && !FindInStrArray(olMustBeIn, ropTestStr1) && !FindInStrArray(olMustBeIn, ropTestStr2))
		{
			olMustBeIn.RemoveAll();
			return false;
		}
		olMustBeIn.RemoveAll();
		*/

		for (int i=0; i < ropAssignedStrs.GetSize(); i++)
		{
			if (ropTestStr1 == ropAssignedStrs[i])
				return true;
			if (ropTestStr2 == ropAssignedStrs[i])
				return true;

			if ( (ropTestStr2 + CString(":") + ropTestStr4  ) == ropAssignedStrs[i])
				return true;

			if ( (ropTestStr1 + CString(":") + ropTestStr3  ) == ropAssignedStrs[i])
				return true;

			if(ropTestStr4.IsEmpty() && ropTestStr3.IsEmpty())
			{
				if(ropAssignedStrs[i].Find(ropTestStr1 + CString(":")) >= 0)
					return true;

				if(ropAssignedStrs[i].Find(ropTestStr2 + CString(":")) >= 0)
					return true;
			}

		}

		for ( i=0; i < ropPreferredStrs.GetSize(); i++)
		{
			if (ropTestStr1 == ropPreferredStrs[i])
				return true;
			if (ropTestStr2 == ropPreferredStrs[i])
				return true;

			if ( (ropTestStr2 + CString(":") + ropTestStr4  ) == ropPreferredStrs[i])
				return true;

			if ( (ropTestStr1 + CString(":") + ropTestStr3  ) == ropPreferredStrs[i])
				return true;


			if(ropTestStr4.IsEmpty() && ropTestStr3.IsEmpty())
			{
				if(ropPreferredStrs[i].Find(ropTestStr1 + CString(":")) >= 0)
					return true;

				if(ropPreferredStrs[i].Find(ropTestStr2 + CString(":")) >= 0)
					return true;
			}

		}



	}



//	if (ropAssignedStrs.GetSize() > 0 && 
//		!FindInStrArray(ropAssignedStrs, ropTestStr1) && !FindInStrArray(ropAssignedStrs, ropTestStr2))
//		return false;

	if(ropAssignedStrs.GetSize() == 0)
		return true;
	else
		return false;
}




bool SpotAllocation::CheckResToResLists2(const CString &ropTestStr1, const CString &ropTestStr2, const CString &ropTestStr3, const CString &ropTestStr4, const CStringArray &ropAssignedStrs, const CStringArray &ropExcludedStrs, const CStringArray &ropPreferredStrs) const
{
	if (ropTestStr1.IsEmpty() && ropTestStr2.IsEmpty()) return true;

	
	if (bgAutoAllocWithExactParameter || ropAssignedStrs.GetSize() > 0)// || ropPreferredStrs.GetSize() > 0 )
	{
		/*
		CStringArray olMustBeIn;
		olMustBeIn.Append(ropAssignedStrs);
		olMustBeIn.Append(ropPreferredStrs);

		if (olMustBeIn.GetSize() > 0 && !FindInStrArray(olMustBeIn, ropTestStr1) && !FindInStrArray(olMustBeIn, ropTestStr2))
		{
			olMustBeIn.RemoveAll();
			return false;
		}
		olMustBeIn.RemoveAll();
		*/

		for (int i=0; i < ropAssignedStrs.GetSize(); i++)
		{
			if (ropTestStr1 == ropAssignedStrs[i])
				return true;
			if (ropTestStr2 == ropAssignedStrs[i])
				return true;

			if ( (ropTestStr4 + CString(":") + ropTestStr2  ) == ropAssignedStrs[i])
				return true;

			if ( (ropTestStr3 + CString(":") + ropTestStr1  ) == ropAssignedStrs[i])
				return true;

		}

		for ( i=0; i < ropPreferredStrs.GetSize(); i++)
		{
			if (ropTestStr1 == ropPreferredStrs[i])
				return true;
			if (ropTestStr2 == ropPreferredStrs[i])
				return true;

			if ( (ropTestStr4 + CString(":") + ropTestStr2  ) == ropPreferredStrs[i])
				return true;

			if ( (ropTestStr3 + CString(":") + ropTestStr1  ) == ropPreferredStrs[i])
				return true;


		}



	}

	for (int i=0; i < ropExcludedStrs.GetSize(); i++)  
	{
		if (ropTestStr1 == ropExcludedStrs[i])
			return false;
		if (ropTestStr2 == ropExcludedStrs[i])
			return false;

		if ( (ropTestStr4 + CString(":") + ropTestStr2  ) == ropExcludedStrs[i])
			return false;

		if ( (ropTestStr3 + CString(":") + ropTestStr1  ) == ropExcludedStrs[i])
			return false;

	}


//	if (ropAssignedStrs.GetSize() > 0 && 
//		!FindInStrArray(ropAssignedStrs, ropTestStr1) && !FindInStrArray(ropAssignedStrs, ropTestStr2))
//		return false;

	if(ropAssignedStrs.GetSize() == 0)
		return true;
	else
		return false;
}








bool SpotAllocation::CheckPosGateRelation(DIAFLIGHTDATA *prpFlight, char cpFPart, const CString &ropPst)
{
	if (prpFlight == NULL) return true;

	//// Position - Gate Relation
	GAT_RESLIST *prlGatRes; 

	// check arrival
	if (cpFPart == 'A')
	{
		// gate 1
		if (strlen(prpFlight->Gta1) > 0)
		{
			prlGatRes = GetGatRes(prpFlight->Gta1);
			if (prlGatRes != NULL)
			{
				if (!(prlGatRes->RelPos1.IsEmpty() && prlGatRes->RelPos2.IsEmpty()))
				{
					if (prlGatRes->RelPos1 != ropPst && prlGatRes->RelPos2 != ropPst)
						return false;
				}
			}
		}
		// gate 2
		if (strlen(prpFlight->Gta2) > 0)
		{
			prlGatRes = GetGatRes(prpFlight->Gta2);
			if (prlGatRes != NULL)
			{
				if (!(prlGatRes->RelPos1.IsEmpty() && prlGatRes->RelPos2.IsEmpty()))
				{
					if (prlGatRes->RelPos1 != ropPst && prlGatRes->RelPos2 != ropPst)
						return false;
				}
			}
		}
	}
	// check departure
	if (cpFPart == 'D')
	{
		// gate 1
		if (strlen(prpFlight->Gtd1) > 0)
		{
			prlGatRes = GetGatRes(prpFlight->Gtd1);
			if (prlGatRes != NULL)
			{
				if (!(prlGatRes->RelPos1.IsEmpty() && prlGatRes->RelPos2.IsEmpty()))
				{
					if (prlGatRes->RelPos1 != ropPst && prlGatRes->RelPos2 != ropPst)
						return false;
				}
			}
		}
		// gate 2
		if (strlen(prpFlight->Gtd2) > 0)
		{
			prlGatRes = GetGatRes(prpFlight->Gtd2);
			if (prlGatRes != NULL)
			{
				if (!(prlGatRes->RelPos1.IsEmpty() && prlGatRes->RelPos2.IsEmpty()))
				{
					if (prlGatRes->RelPos1 != ropPst && prlGatRes->RelPos2 != ropPst)
						return false;
				}
			}
		}		
	}

	return true;
}

bool SpotAllocation::CheckIncomp(CString& opAlo, CString& opGrp,const DIAFLIGHTDATA *prpFlight, const CTime &ropStartAlloc, const CTime &ropEndAlloc, const CString &ropPos, 
									DIAFLIGHTDATA *&prpFlightTmp, const CString &ropPosFlight,CCSPtrArray<DIAFLIGHTDATA> &ropFlights, int ipDoNotBack, CStringArray* opAptArray)
{
	if (!prpFlight || ropPos.IsEmpty()) return true;

	CPtrArray rlFlights;
	if (opAlo == "POS")
	 	ogPosDiaFlightData.GetFlightsAtPosition(ropStartAlloc, ropEndAlloc, ropPos, &rlFlights);
	else if (opAlo == "GAT")
		ogPosDiaFlightData.GetFlightsAtGate(ropStartAlloc, ropEndAlloc, ropPos, prpFlight, &rlFlights);

	int ilCount = rlFlights.GetSize();
	for( int i = 0 ; i < ilCount; i++)
	{
		prpFlightTmp = (DIAFLIGHTDATA *) rlFlights[i];

		if (prpFlightTmp == prpFlight || prpFlightTmp->Rkey == prpFlight->Rkey)
			continue;

		CStringArray olVal1;
		CStringArray olVal2;
		CStringArray olVal3;
		CStringArray olVal4;
		if (opGrp == "ALT")
		{
			olVal1.Add(prpFlight->Alc2);
			olVal2.Add(prpFlight->Alc3);
			olVal3.Add(prpFlightTmp->Alc2);
			olVal4.Add(prpFlightTmp->Alc3);
		}
		else if (opGrp == "APT")
		{
			if (prpFlight->Adid[0] == 'A')
			{
				olVal1.Add(prpFlight->Org3);
				olVal2.Add(prpFlight->Org4);
			}
			else if (prpFlight->Adid[0] == 'D')
			{




				olVal1.Add(prpFlight->Des3);
				olVal2.Add(prpFlight->Des4);
			}
			else
			{
				olVal1.Add(prpFlight->Des3);
				olVal2.Add(prpFlight->Des4);

				if (strlen(prpFlight->Des3) == 0 && strlen(prpFlight->Des4) == 0)
				{
					olVal1.Add(prpFlight->Org3);
					olVal2.Add(prpFlight->Org4);
				}
			}

			CCSPtrArray<VIADATA> olVias;
			if (ogBasicData.GetViaArray(&olVias, CString(prpFlight->Vial)) > 0)
			{
				for (int i=0; i<olVias.GetSize(); i++)
				{
					olVal1.Add(olVias.GetAt(i).Apc3);
					olVal2.Add(olVias.GetAt(i).Apc4);
				}
			}


			if (prpFlightTmp->Adid[0] == 'A')
			{
				olVal3.Add(prpFlightTmp->Org3);
				olVal4.Add(prpFlightTmp->Org4);
			}
			else if (prpFlightTmp->Adid[0] == 'D')
			{
				olVal3.Add(prpFlightTmp->Des3);
				olVal4.Add(prpFlightTmp->Des4);
			}
			else
			{
				olVal3.Add(prpFlightTmp->Des3);
				olVal4.Add(prpFlightTmp->Des4);

				if (strlen(prpFlightTmp->Des3) == 0 && strlen(prpFlightTmp->Des4) == 0)
				{
					olVal3.Add(prpFlightTmp->Des3);
					olVal4.Add(prpFlightTmp->Des4);
				}
			}

			olVias.RemoveAll();
			if (ogBasicData.GetViaArray(&olVias, CString(prpFlightTmp->Vial)) > 0)
			{
				for (int i=0; i<olVias.GetSize(); i++)
				{
					olVal3.Add(olVias.GetAt(i).Apc3);
					olVal4.Add(olVias.GetAt(i).Apc4);
				}
			}
		}

		for (int i=0; i<olVal1.GetSize(); i++)
		{
			for (int j=0; j<olVal3.GetSize(); j++)
			{
				long llCheckMatrix = CheckPositionMatrix(opAlo,opGrp,ropPosFlight,ropPos,olVal1.GetAt(i),olVal2.GetAt(i),olVal3.GetAt(j),olVal4.GetAt(j));
				if(llCheckMatrix == 0L)
				{
					prpFlightTmp = ogPosDiaFlightData.GetFlightByUrno(prpFlightTmp->Urno); // Because rlFlights will be invalid when the function returns
					ropFlights.Add(prpFlightTmp);
					if (opAptArray)
						opAptArray->Add(olVal3.GetAt(j)+CString("/")+olVal4.GetAt(j));
					break;
				}
			}
		}
/*
//		long llCheckMatrix = CheckPositionMatrix(opAlo,opGrp,ropPosFlight,ropPos,olVal1,olVal2,olVal3,olVal4);
////		long llCheckMatrix = CheckPositionMatrix(CString("POS"),CString("ALT"),ropPosFlight,ropPos,CString(prpFlight->Alc2),CString(prpFlight->Alc3),CString(prpFlightTmp->Alc2),CString(prpFlightTmp->Alc3));

		if(llCheckMatrix == 0L)
		{
			prpFlightTmp = ogPosDiaFlightData.GetFlightByUrno(prpFlightTmp->Urno); // Because rlFlights will be invalid when the function returns
			ropFlights.Add(prpFlightTmp);
		}
*/
	}

	if (ropFlights.GetSize() > 0 && prpFlightTmp)
		return false;

	prpFlightTmp = NULL;
	return true;
}



///////////////////////////////////////////////////////////////
// Conflict determination based Centerline distance 

bool SpotAllocation::CheckWingoverlapWithCenterline(DIAFLIGHTDATA *prpFlight, const CTime &ropStartAlloc, const CTime &ropEndAlloc, const CString &ropPos, 
									  double dpPosSpanMax, double dpPosMac, double dpMinDist, double dpACSpan, DIAFLIGHTDATA *&prpFlightTmp, const CString &ropPosFlight,CCSPtrArray<DIAFLIGHTDATA> &ropFlights, int ipDoNotBack)
{
	if (!prpFlight || ropPos.IsEmpty()) return true;

	CString olAcwsStr;
	CPtrArray rlFlights;
 	double dlAcwsTmp; 
	double dlNewMaxWs = (dpPosSpanMax - dpACSpan) + dpPosMac;		
	bool blAcFound = false;


	CString olAfmcStr = ogBCD.GetFieldExt("ACT","ACT3","ACT5",CString(prpFlight->Act3),CString(prpFlight->Act5),"AFMC");

	double ilClearance = 7.5;

	if(olAfmcStr == "A" || olAfmcStr == "B") ilClearance = 3; 
	if(olAfmcStr == "C" ) ilClearance = 4.5; 

	double ilClearanceMax;


	if(bgPosShadowbars)
	{
		double dlSpanMin = 5;

		PST_RESLIST *prlPstRes = GetPstRes(ropPos); 
		if(prlPstRes != NULL)
				dlSpanMin = prlPstRes->SpanMin;
			
		
			if( (dpPosMac - (dpACSpan / 2) - ilClearance) < dlSpanMin)
		{
			CString olTmp = CString(prpFlight->BlockedPositons);
			
			if( olTmp.Find(ropPos) < 0 && olTmp.GetLength() < 94)
			{
				olTmp += "," + ropPos;
				strcpy(prpFlight->BlockedPositons,olTmp);
			}
			
		}
	}

 	ogPosDiaFlightData.GetFlightsAtPosition(ropStartAlloc, ropEndAlloc, ropPos, &rlFlights);

	int ilCount = rlFlights.GetSize();
	for( int i = 0 ; i < ilCount; i++)
	{
		prpFlightTmp = (DIAFLIGHTDATA *) rlFlights[i];

		if (prpFlightTmp == prpFlight || prpFlightTmp->Rkey == prpFlight->Rkey)
			continue;



		long llCheckMatrix = CheckPositionMatrix(CString("POS"),CString("ACT"),ropPosFlight,ropPos,CString(prpFlight->Act3),CString(prpFlight->Act5),CString(prpFlightTmp->Act3),CString(prpFlightTmp->Act5));

		if (llCheckMatrix == 1L)
			continue;


		CString olAfmcStrTmp = ogBCD.GetFieldExt("ACT","ACT3","ACT5",CString(prpFlightTmp->Act3),CString(prpFlightTmp->Act5),"AFMC");

		double ilClearanceTmp = 7.5;

		if(olAfmcStrTmp == "A" || olAfmcStrTmp == "B") ilClearanceTmp = 3; 
		if(olAfmcStrTmp == "C" ) ilClearanceTmp = 4.5; 


		if(ilClearanceTmp > ilClearance)
			ilClearanceMax = ilClearanceTmp;
		else
			ilClearanceMax = ilClearance;



		olAcwsStr = ogBCD.GetFieldExt("ACT","ACT3","ACT5",CString(prpFlightTmp->Act3),CString(prpFlightTmp->Act5),"ACWS");
		dlAcwsTmp = (double) atof(olAcwsStr);
			
		if(((( (dlAcwsTmp / 2) + (dpACSpan / 2) + ilClearanceMax) >= dpPosMac) && llCheckMatrix == -1L) || llCheckMatrix == 0L)
		{
			prpFlightTmp = ogPosDiaFlightData.GetFlightByUrno(prpFlightTmp->Urno); // Because rlFlights will be invalid when the function returns
			ropFlights.Add(prpFlightTmp);
		}

	//-----------------------------------------------------------
	//rkr: check back if there is conflict in the second way
		// jeder nachbar wird mit dem zu untersuchenden flight �berpr�ft
	
		if (!ipDoNotBack && llCheckMatrix == -1L)
		{

			// belegungsstruktur der position f�r die nachbarn
			PST_RESLIST *prlPstRes = GetPstRes(ropPos); 
			if(prlPstRes == NULL)
				continue;

			RecordSet olCurrActRecord;
			CCSPtrArray<RecordSet> prlRecords;
			int ilAct = ogBCD.GetRecordsExt("ACT","ACT3","ACT5",CString(prpFlightTmp->Act3),CString(prpFlightTmp->Act5), &prlRecords);
			
			if (ilAct == 0)
			{
				prlRecords.DeleteAll();
				return true;
			}
			else
				olCurrActRecord = prlRecords[0];

			// ac-spanweite des nachbarn
			double dlSpanAcNeighbour = (double) atof(olCurrActRecord[ ogBCD.GetFieldIndex("ACT","ACWS")]);



			CString olAfmcStrTmp = ogBCD.GetFieldExt("ACT","ACT3","ACT5",CString(prpFlightTmp->Act3),CString(prpFlightTmp->Act5),"AFMC");

			double ilClearanceTmp = 7.5;

			if(olAfmcStrTmp == "A" || olAfmcStrTmp == "B") ilClearanceTmp = 3; 
			if(olAfmcStrTmp == "C" ) ilClearanceTmp = 4.5; 


			if(ilClearanceTmp > ilClearance)
				ilClearanceMax = ilClearanceTmp;
			else
				ilClearanceMax = ilClearance;


			// wertermittlung vom nachbar zum aktuellen flight (next position)
			CString olMacNeighbour;
			double dlMacFlight = 0;
			if (prlPstRes->PosAndMac.Lookup(ropPosFlight, olMacNeighbour))
			{
				dlMacFlight = (double) atof(olMacNeighbour);

				if(((dlSpanAcNeighbour / 2) + (dpACSpan / 2) + ilClearanceMax > dlMacFlight  && llCheckMatrix == -1L) || llCheckMatrix == 0L)
				{
					prpFlightTmp = ogPosDiaFlightData.GetFlightByUrno(prpFlightTmp->Urno); // Because rlFlights will be invalid when the function returns
					ropFlights.Add(prpFlightTmp);
				}
			}

			prlRecords.DeleteAll();
		}
	//rkr: check back if there is conflict in the second way
	//-----------------------------------------------------------
	}

	if (ropFlights.GetSize() > 0 && prpFlightTmp)
		return false;

	prpFlightTmp = NULL;
	return true;
}
// End Conflict determination based Centerline distance 
///////////////////////////////////////////////////////////////

bool SpotAllocation::CheckWingoverlap( DIAFLIGHTDATA *prpFlight, const CTime &ropStartAlloc, const CTime &ropEndAlloc, const CString &ropPos, 
									  double dpPosSpanMax, double dpPosMac, double dpMinDist, double dpACSpan, DIAFLIGHTDATA *&prpFlightTmp, const CString &ropPosFlight,CCSPtrArray<DIAFLIGHTDATA> &ropFlights, int ipDoNotBack)
{
	if (!prpFlight || ropPos.IsEmpty()) return true;


	if(bgUseCenterline)
	{
		return CheckWingoverlapWithCenterline(prpFlight, ropStartAlloc, ropEndAlloc, ropPos, dpPosSpanMax, dpPosMac, dpMinDist, dpACSpan, prpFlightTmp, ropPosFlight,ropFlights, ipDoNotBack);
	}

	// old method using the resricted to value 

	CString olAcwsStr;
	CPtrArray rlFlights;
 	double dlAcwsTmp; 
	double dlNewMaxWs = (dpPosSpanMax - dpACSpan) + dpPosMac;		
	bool blAcFound = false;

 	ogPosDiaFlightData.GetFlightsAtPosition(ropStartAlloc, ropEndAlloc, ropPos, &rlFlights);

	int ilCount = rlFlights.GetSize();
	for( int i = 0 ; i < ilCount; i++)
	{
		prpFlightTmp = (DIAFLIGHTDATA *) rlFlights[i];

		if (prpFlightTmp == prpFlight || prpFlightTmp->Rkey == prpFlight->Rkey)
			continue;


		long llCheckMatrix = CheckPositionMatrix(CString("POS"),CString("ACT"),ropPosFlight,ropPos,CString(prpFlight->Act3),CString(prpFlight->Act5),CString(prpFlightTmp->Act3),CString(prpFlightTmp->Act5));

		if (llCheckMatrix == 1L)
			continue;


//		dlAcwsTmp = (double) atof(olAcwsStr);
		olAcwsStr = ogBCD.GetFieldExt("ACT","ACT3","ACT5",CString(prpFlightTmp->Act3),CString(prpFlightTmp->Act5),"ACWS");
		dlAcwsTmp = (double) atof(olAcwsStr);
			
		if((dlNewMaxWs - dpMinDist < dlAcwsTmp && llCheckMatrix == -1L) || llCheckMatrix == 0L)
		{
			prpFlightTmp = ogPosDiaFlightData.GetFlightByUrno(prpFlightTmp->Urno); // Because rlFlights will be invalid when the function returns
			ropFlights.Add(prpFlightTmp);
//			return false;
		}

	//-----------------------------------------------------------
	//rkr: check back if there is conflict in the second way
		// jeder nachbar wird mit dem zu untersuchenden flight �berpr�ft
	
		if (!ipDoNotBack && llCheckMatrix == -1L)
		{

			// belegungsstruktur der position f�r die nachbarn
			PST_RESLIST *prlPstRes = GetPstRes(ropPos); 
			if(prlPstRes == NULL)
				continue;

			RecordSet olCurrActRecord;
			CCSPtrArray<RecordSet> prlRecords;
			int ilAct = ogBCD.GetRecordsExt("ACT","ACT3","ACT5",CString(prpFlightTmp->Act3),CString(prpFlightTmp->Act5), &prlRecords);
			
			if (ilAct == 0)
			{
				prlRecords.DeleteAll();
				return true;
			}
			else
				olCurrActRecord = prlRecords[0];

			// ac-spanweite des nachbarn
			double dlSpanAcNeighbour = (double) atof(olCurrActRecord[ ogBCD.GetFieldIndex("ACT","ACWS")]);

			// maximale spannweite der nachbarposition
			double dlMaxSpanPosNeighbour = prlPstRes->SpanMax;

			// wertermittlung vom nachbar zum aktuellen flight (next position)
			CString olMacNeighbour;
			double dlMacFlight = 0;
			if (prlPstRes->PosAndMac.Lookup(ropPosFlight, olMacNeighbour))
			{
				dlMacFlight = (double) atof(olMacNeighbour);

				double dlNewMaxWsNeighbour = (dlMaxSpanPosNeighbour - dlSpanAcNeighbour) + dlMacFlight;

				if((dlNewMaxWsNeighbour - dpMinDist < dpACSpan && llCheckMatrix == -1L) || llCheckMatrix == 0L)
				{
					prpFlightTmp = ogPosDiaFlightData.GetFlightByUrno(prpFlightTmp->Urno); // Because rlFlights will be invalid when the function returns
					ropFlights.Add(prpFlightTmp);
				}
			}

			prlRecords.DeleteAll();
		}
	//rkr: check back if there is conflict in the second way
	//-----------------------------------------------------------
	}

	if (ropFlights.GetSize() > 0 && prpFlightTmp)
		return false;

	prpFlightTmp = NULL;
	return true;
}





PST_RESLIST *SpotAllocation::GetPstRes(const CString &ropPst)
{
	PST_RESLIST *prlPstRes = NULL;

	for(int i = omPstRules.GetSize() - 1; i >=0; i--)
	{
		//TRACE("\n%s", omPstRules[i].Pnam);
		if(omPstRules[i].Pnam == ropPst)
		{
			prlPstRes = &omPstRules[i];
			break;
		}
	}

	return prlPstRes;
}



GAT_RESLIST *SpotAllocation::GetGatRes(const CString &ropGat)
{
	GAT_RESLIST *prlGatRes = NULL;

	for(int i = omGatRules.GetSize() - 1; i >=0; i--)
	{
		if(omGatRules[i].Gnam == ropGat)
		{
			prlGatRes = &omGatRules[i];
			break;
		}
	}
	return prlGatRes;
}

BLT_RESLIST *SpotAllocation::GetBltRes(const CString &ropBlt)
{
	BLT_RESLIST *prlBltRes = NULL;

	for(int i = omBltRules.GetSize() - 1; i >=0; i--)
	{
		if(omBltRules[i].Bnam == ropBlt)
		{
			prlBltRes = &omBltRules[i];
			break;
		}
	}
	return prlBltRes;
}


WRO_RESLIST *SpotAllocation::GetWroRes(const CString &ropWro)
{
	WRO_RESLIST *prlWroRes = NULL;

	for(int i = omWroRules.GetSize() - 1; i >=0; i--)
	{
		if(omWroRules[i].Wnam == ropWro)
		{
			prlWroRes = &omWroRules[i];
			break;
		}
	}
	return prlWroRes;
}




bool SpotAllocation::CheckGatFlightId(const CString &ropGat, const CString &ropFlti)
{
	bool blRet = true;

	GAT_RESLIST *prlGatRes = GetGatRes(ropGat); 

	if(prlGatRes == NULL)
		return false;


	if(strlen(prlGatRes->Flti) > 0)
	{
		if(!ropFlti.IsEmpty())
		{
			if( CString(prlGatRes->Flti).Find( ropFlti) < 0)
			{
				blRet = false;
			}
		}
	}

	return blRet;
}


CString SpotAllocation::GetGatFlightId(CString opGat)
{
	GAT_RESLIST *prlGatRes = GetGatRes(opGat); 

	if(prlGatRes == NULL)
		return "";
	return CString(prlGatRes->Flti);	

}


bool SpotAllocation::GetLocalGlobalNat(RecordSet *prpRecord, CString& opTyp, bool& opAlga, bool& opAlpo, bool& opAlbb, bool& opAlwr)
{
	int ilAlga = ogBCD.GetFieldIndex("NAT", "ALGA");
	int ilAlpo = ogBCD.GetFieldIndex("NAT", "ALPO");
	int ilAlbb = ogBCD.GetFieldIndex("NAT", "ALBB");
	int ilAlwr = ogBCD.GetFieldIndex("NAT", "ALWR");
	int ilTtyp = ogBCD.GetFieldIndex("NAT", "TTYP");

	CString olTyp("");
	CString olAlga("");
	if (ilTtyp > -1)
	{
		olTyp  = prpRecord->Values[ilTtyp];
		opTyp  = olTyp;

		//gate
		olAlga = prpRecord->Values[ilAlga];
		if(FindInStrArray(omGatNatNoAllocList, olTyp) && olAlga == "X")
			opAlga = true;
		if(!FindInStrArray(omGatNatNoAllocList, olTyp) && olAlga != "X")
			opAlga = true;

		//position
		olAlga = prpRecord->Values[ilAlpo];
		if(FindInStrArray(omPstNatNoAllocList, olTyp) && olAlga == "X")
			opAlpo = true;
		if(!FindInStrArray(omPstNatNoAllocList, olTyp) && olAlga != "X")
			opAlpo = true;

		//belt
		olAlga = prpRecord->Values[ilAlbb];
		if(FindInStrArray(omBltNatNoAllocList, olTyp) && olAlga == "X")
			opAlbb = true;
		if(!FindInStrArray(omBltNatNoAllocList, olTyp) && olAlga != "X")
			opAlbb = true;

		//lounge
		olAlga = prpRecord->Values[ilAlwr];
		if(FindInStrArray(omWroNatNoAllocList, olTyp) && olAlga == "X")
			opAlwr = true;
		if(!FindInStrArray(omWroNatNoAllocList, olTyp) && olAlga != "X")
			opAlwr = true;

		if (!opAlga && !opAlpo && !opAlbb && !opAlwr)
			return false;

		return true;
	}
	return false;
}

bool SpotAllocation::CheckGatGlobalNat(CString& opTyp, bool& opAlga, bool& opAlpo, bool& opAlbb, bool& opAlwr)
{
	if (opTyp.IsEmpty())
		return false;

	DiaCedaFlightData::RKEYLIST *prlRkey;
	POSITION pos;
	CString olGat;
	DIAFLIGHTDATA* prlFlight = NULL;
	bool blDispGat = false;
	bool blDispPos = false;
	bool blDispWro = false;
	bool blDispBlt = false;
	bool blList = false;

	//gate
	if (opAlga)
	{
		for( pos = ogPosDiaFlightData.omGatMap.GetStartPosition(); pos != NULL; )
		{
			ogPosDiaFlightData.omGatMap.GetNextAssoc( pos, olGat , (void *&)prlRkey );
			for(int i = 0; i < prlRkey->Rotation.GetSize(); i++)
			{
				prlFlight = &(*prlRkey).Rotation[i];
				if (prlFlight && CString(prlFlight->Ttyp) == opTyp)
				{
					// Check nature ristriction from  nature table 
					ogKonflikte.RemoveKonfliktForFlight(prlFlight->Urno, MOD_ID_DIA, IDS_STRING1732);
					blDispGat = true;
					blList = true;
					if( !CString(prlFlight->Ttyp).IsEmpty() )
					{
						if(FindInStrArray(omGatNatNoAllocList, prlFlight->Ttyp))
						{
							CString olText("");
							CString olBez("");
							olText.Format(GetString(IDS_STRING1732), omGatNatNoAllocListPlain, olGat, prlFlight->Ttyp);
							if( strcmp(prlFlight->Des3, pcgHome) == 0)
							{
								olBez.Format("%-9s (A): ", prlFlight->Flno);
								olText = olBez + olText;
								ogKonflikte.AddKonflikt(KonfIdent(prlFlight->Urno, 'A', 0, ' ', IDS_STRING1732), olText, MOD_ID_DIA);
							}
							else
							{
								olBez.Format("%-9s (D): ", prlFlight->Flno);
								olText = olBez + olText;
								ogKonflikte.AddKonflikt(KonfIdent(prlFlight->Urno, 'D', 0, ' ', IDS_STRING1732), olText, MOD_ID_DIA);
							}
						}
					}
				}
			}
		}
	}

	//pos
	if (opAlpo)
	{
		for( pos = ogPosDiaFlightData.omPosMap.GetStartPosition(); pos != NULL; )
		{
			ogPosDiaFlightData.omPosMap.GetNextAssoc( pos, olGat , (void *&)prlRkey );
			for(int i = 0; i < prlRkey->Rotation.GetSize(); i++)
			{
				prlFlight = &(*prlRkey).Rotation[i];
				if (prlFlight && CString(prlFlight->Ttyp) == opTyp)
				{
					// Check nature ristriction from  nature table 
					ogKonflikte.RemoveKonfliktForFlight(prlFlight->Urno, MOD_ID_DIA, IDS_STRING1712);
					blDispPos = true;
					blList = true;
					if( !CString(prlFlight->Ttyp).IsEmpty() )
					{
						if(FindInStrArray(omPstNatNoAllocList, prlFlight->Ttyp))
						{
							CString olText("");
							CString olBez("");
							olText.Format(GetString(IDS_STRING1712), omPstNatNoAllocListPlain, olGat, prlFlight->Ttyp);
							if( strcmp(prlFlight->Des3, pcgHome) == 0)
							{
								olBez.Format("%-9s (A): ", prlFlight->Flno);
								olText = olBez + olText;
								ogKonflikte.AddKonflikt(KonfIdent(prlFlight->Urno, 'A', 0, ' ', IDS_STRING1712), olText, MOD_ID_DIA);
							}
							else
							{
								olBez.Format("%-9s (D): ", prlFlight->Flno);
								olText = olBez + olText;
								ogKonflikte.AddKonflikt(KonfIdent(prlFlight->Urno, 'D', 0, ' ', IDS_STRING1712), olText, MOD_ID_DIA);
							}
						}
					}
				}
			}
		}
	}

	//belt
	if (opAlbb)
	{
		for( pos = ogPosDiaFlightData.omBltMap.GetStartPosition(); pos != NULL; )
		{
			ogPosDiaFlightData.omBltMap.GetNextAssoc( pos, olGat , (void *&)prlRkey );
			for(int i = 0; i < prlRkey->Rotation.GetSize(); i++)
			{
				prlFlight = &(*prlRkey).Rotation[i];
				if (prlFlight && CString(prlFlight->Ttyp) == opTyp)
				{
					// Check nature ristriction from  nature table 
					ogKonflikte.RemoveKonfliktForFlight(prlFlight->Urno, MOD_ID_DIA, IDS_STRING2339);
					blDispBlt = true;
					blList = true;
					if( !CString(prlFlight->Ttyp).IsEmpty() )
					{
						if(FindInStrArray(omBltNatNoAllocList, prlFlight->Ttyp))
						{
							CString olText("");
							CString olBez("");
							olText.Format(GetString(IDS_STRING2339), omBltNatNoAllocListPlain, olGat, prlFlight->Ttyp);
							if( strcmp(prlFlight->Des3, pcgHome) == 0)
							{
								olBez.Format("%-9s (A): ", prlFlight->Flno);
								olText = olBez + olText;
								ogKonflikte.AddKonflikt(KonfIdent(prlFlight->Urno, 'A', 0, ' ', IDS_STRING2339), olText, MOD_ID_DIA);
							}
						}
					}
				}
			}
		}
	}

	//lounge
	if (opAlwr)
	{
		for( pos = ogPosDiaFlightData.omWroMap.GetStartPosition(); pos != NULL; )
		{
			ogPosDiaFlightData.omWroMap.GetNextAssoc( pos, olGat , (void *&)prlRkey );
			for(int i = 0; i < prlRkey->Rotation.GetSize(); i++)
			{
				prlFlight = &(*prlRkey).Rotation[i];
				if (prlFlight && CString(prlFlight->Ttyp) == opTyp)
				{
					// Check nature ristriction from  nature table 
					ogKonflikte.RemoveKonfliktForFlight(prlFlight->Urno, MOD_ID_DIA, IDS_STRING2338);
					blDispWro = true;
					blList = true;
					if( !CString(prlFlight->Ttyp).IsEmpty() )
					{
						if(FindInStrArray(omWroNatNoAllocList, prlFlight->Ttyp))
						{
							CString olText("");
							CString olBez("");
							olText.Format(GetString(IDS_STRING2338), omWroNatNoAllocListPlain, olGat, prlFlight->Ttyp);
							if( strcmp(prlFlight->Des3, pcgHome) == 0)
							{
							}
							else
							{
								olBez.Format("%-9s (D): ", prlFlight->Flno);
								olText = olBez + olText;
								ogKonflikte.AddKonflikt(KonfIdent(prlFlight->Urno, 'D', 0, ' ', IDS_STRING2338), olText, MOD_ID_DIA);
							}
						}
					}
				}
			}
		}
	}

	if (blList)
	{
		if (pogButtonList)
			pogButtonList->UpdateFlightDia();

		ogKonflikte.CheckAttentionButton();
	}

	if (pogGatDiagram && blDispGat)
		pogGatDiagram->RedisplayAll();
	if (pogPosDiagram && blDispPos)
		pogPosDiagram->RedisplayAll();
	if (pogBltDiagram && blDispBlt)
		pogBltDiagram->RedisplayAll();
	if (pogWroDiagram && blDispWro)
		pogWroDiagram->RedisplayAll();

	return true;
}


//bool SpotAllocation::CheckGat(const DIAFLIGHTDATA *prpFlight, char cpFPart, CString opGat, int ipGatNo , CCSPtrArray<KonfItem> *popKonfList, const CTimeSpan *popRightBuffer, const CTimeSpan *popLeftBuffer, bool bpPreCheck) 
bool SpotAllocation::CheckGat(const DIAFLIGHTDATA *prpFlight, char cpFPart, CString opGat, int ipGatNo , CCSPtrArray<KonfItem> *popKonfList, const CTimeSpan *popRightBuffer, const CTimeSpan *popLeftBuffer, bool bpPreCheck, CString opPrePos) 
//bool SpotAllocation::CheckGat(const DIAFLIGHTDATA *prpFlight, char cpFPart, CString opGat, int ipGatNo , CCSPtrArray<KonfItem> *popKonfList, const CTimeSpan *popRightBuffer, const CTimeSpan *popLeftBuffer) 
{
	if (prpFlight == NULL || (ipGatNo != 1 && ipGatNo != 2))
		return false;

	if(opGat.IsEmpty())
		return true;

	GAT_RESLIST *prlGatRes = GetGatRes(opGat); 
	if(prlGatRes == NULL)
		return false;


	bool blRet = true;

	bool blActFound = false;
	RecordSet olCurrActRecord;

	CString olConfText;

	CTime olStartAlloc = TIMENULL;
	CTime olEndAlloc = TIMENULL;

	if (!ogPosDiaFlightData.GetGatAllocTimes(*prpFlight, cpFPart, opGat, ipGatNo, olStartAlloc, olEndAlloc, bpPreCheck))
	{
		return false;
	}

 
	if(popRightBuffer != NULL)
		olStartAlloc -= *popRightBuffer;

	if(popLeftBuffer != NULL)
		olEndAlloc += *popLeftBuffer;


	// Check terminal ristriction 
	if(bgTermRestrForAl)
	{
		CString olTerg;
		ogBCD.GetField("GAT", "GNAM", opGat, "TERM", olTerg);
		CString olAltTerg = ogBCD.GetFieldExt("ALT","ALC2","ALC3",prpFlight->Alc2,prpFlight->Alc3,"TERG");
		if( !olAltTerg.IsEmpty() && !olTerg.IsEmpty())
		{
			if (olAltTerg.Find(olTerg) < 0)
			{
				if(popKonfList != NULL)
				{
					// Terminal Gate %s: %s must be %s
					olConfText.Format(GetString(IDS_STRING2728), opGat, olTerg, olAltTerg);
					AddToKonfList(prpFlight, cpFPart, 0, ' ', IDS_STRING2725, olConfText, *popKonfList);
					blRet = false;					
				}
				else
				{
					return false;
				}
			}
		}
	}

	// Check gate availablility
	CStringArray opNafr;
	CStringArray opNato;

	for (int il = 0; il < prlGatRes->BlkData.GetSize(); il++)
	{
		CTime olNafr = prlGatRes->BlkData[il].StartTime;
		CTime olNato = prlGatRes->BlkData[il].EndTime;

		if(olNafr != TIMENULL || olNato != TIMENULL)
		{	
			if(olNafr == TIMENULL)
				olNafr = CTime(1971,1,1,0,0,0);
			if(olNato == TIMENULL)
				olNato = CTime(2020,1,1,0,0,0);
		}

		if((olNafr != TIMENULL) && (olNato != TIMENULL))
		{
			if(IsReallyOverlapped(olStartAlloc, olEndAlloc, olNafr, olNato)/*, olStartAlloc, olEndAlloc) || !IsWithIn(olStartAlloc, olEndAlloc, olNafr, olNato)*/)
			{
				if(popKonfList != NULL)
				{
					if(bgGatPosLocal) ogBasicData.UtcToLocal(olNafr);
					if(bgGatPosLocal) ogBasicData.UtcToLocal(olNato);
					//Gate %s not available %s - %s reason: %s
					olConfText.Format(GetString(IDS_STRING1724), opGat, olNafr.Format("%H:%M %d.%m.%y"), olNato.Format("%H:%M %d.%m.%y"), prlGatRes->BlkData[il].Text);
					AddToKonfList(prpFlight, cpFPart, 0, ' ', IDS_STRING1724, olConfText, *popKonfList);

					blRet = false;
				}
				else
				{
					return false;
				}
			}
		}
	}

//MWO: 19.04.05: Mixed Flights checks ==================================
	CString olAddiText;


	CString olFlti = CString(prpFlight->Flti);
//	olFlti = "M";

	CString olGat1, olGat2;
	if (ipGatNo == 2 )
	{
		olGat1 = CString(prpFlight->Gtd1);
		olGat2 = opGat;
	}

	if (ipGatNo == 1 )
	{
		olGat2 = CString(prpFlight->Gtd2);
		olGat1 = opGat;
	}

	if (bgPosDiaAutoAllocateInWork && olGat1 == olGat2)
		return false;


	if((olFlti != "M" &&  CString(prpFlight->Gd2d) != "X" ) && ipGatNo == 2)
	{
		/*
		if(popKonfList != NULL)
		{
			//Flight ID for Gate %s: % must be %s 
			//olConfText.Format(GetString(IDS_STRING1725), opGat, prpFlight->Flti, prlGatRes->Flti);
			olConfText.Format(GetString(IDS_STRING1725), opGat, olGateIDs, olFlti);
			AddToKonfList(prpFlight, cpFPart, 0, ' ', IDS_STRING1725, olConfText, *popKonfList);

			blRet = false;
		}
		*/
		return false;
	}



	if(olFlti != "M" || (olFlti == "M" && ipGatNo == 1))
	{
//END MWO: 19.04.05: Mixed Flights checks ==============================


		// Flight ID
 		if(strlen(prlGatRes->Flti) > 0 || strlen(prlGatRes->FltiDep) > 0)
		{
			if(strlen(olFlti) > 0)
			{
				CString olFlightId = CString(prpFlight->Flti);

				CString olGateIDs = CString(prlGatRes->Flti);
				if (strcmp(prpFlight->Adid, "D") == 0)
					olGateIDs = CString(prlGatRes->FltiDep);

				if(!olGateIDs.IsEmpty() && olGateIDs.Find( CString(olFlti)) < 0)
				{
					if(popKonfList != NULL)
					{
						//Flight ID for Gate %s: % must be %s 
						//olConfText.Format(GetString(IDS_STRING1725), opGat, prpFlight->Flti, prlGatRes->Flti);
						olConfText.Format(GetString(IDS_STRING1725), opGat, olGateIDs, olFlti);
						AddToKonfList(prpFlight, cpFPart, 0, ' ', IDS_STRING1725, olConfText, *popKonfList);

						blRet = false;
					}
					else
					{
						return false;
					}
				}
			}
		}
 
	}

//MWO: 19.04.05: Mixed Flights checks ==============================
	if(olFlti == "M" && strcmp(prpFlight->Adid, "D") == 0)
	{
		CString olGatResFlti1;
		CString olGatResFlti2;

		if(!olGat1.IsEmpty() && !olGat2.IsEmpty() )
		{
			prlGatRes = GetGatRes(olGat1); 
			if(prlGatRes != NULL)
			{
				olGatResFlti1 = CString(prlGatRes->FltiDep);
			}

			prlGatRes = GetGatRes(olGat2); 
			if(prlGatRes != NULL)
			{
				olGatResFlti2 = CString(prlGatRes->FltiDep);
			}

			if( !olGatResFlti2.IsEmpty() && !olGatResFlti1.IsEmpty() )
			{

				if( !( (olGatResFlti1.Find("MI") >= 0 && olGatResFlti2.Find("D") >= 0) || (olGatResFlti1.Find("MD") >= 0 && olGatResFlti2.Find("I") >= 0) ) )
				{
					if(popKonfList != NULL)
					{
						//Flight ID for Gate %s: % must be %s 
						//olConfText.Format(GetString(IDS_STRING1725), opGat, prlGatRes->FltiDep, olFlti);
						olConfText.Format(GetString(IDS_STRING1725), olGat1+"/"+olGat2, olGatResFlti1+"/"+olGatResFlti2, olFlti);
						AddToKonfList(prpFlight, cpFPart, 0, ' ', IDS_STRING1725, olConfText + olAddiText, *popKonfList);

						blRet = false;
					}
					else
					{
						return false;
					}
				}
			}
		}
		else
		{
			prlGatRes = GetGatRes(opGat); 
			if(prlGatRes != NULL)
			{
				olGatResFlti1 = CString(prlGatRes->FltiDep);
			}

			if( !(olGatResFlti1.Find("MI") || olGatResFlti1.Find("MD")) )
			{
				if(popKonfList != NULL)
				{
					//Flight ID for Gate %s: % must be %s 
					olConfText.Format(GetString(IDS_STRING1725), opGat, "MI/MD", olFlti);
					AddToKonfList(prpFlight, cpFPart, 0, ' ', IDS_STRING1725, olConfText + olAddiText, *popKonfList);

					blRet = false;
				}
				else
				{
					return false;
				}
			}
		}
	}
//END MWO: 19.04.05: Mixed Flights checks ==============================

	//different Flight ID
 	if(strlen(prlGatRes->Flti) > 0 || strlen(prlGatRes->FltiDep) > 0)
	{
		CString olIds = CString(prlGatRes->Flti) + CString (" ") + CString(prlGatRes->FltiDep);
		CPtrArray olOverlapFlights;
		int ilCountGat = ogPosDiaFlightData.GetFlightsAtGate(olStartAlloc, olEndAlloc, opGat, prpFlight, &olOverlapFlights);
		if (ilCountGat > 0)
		{
			olOverlapFlights.Add((void*) prpFlight);
			bool blConflict = false;
			CString olFlightId ("");
			for (int i = 0; i < olOverlapFlights.GetSize(); i++)
			{
				DIAFLIGHTDATA *plFlight = (DIAFLIGHTDATA *)olOverlapFlights[i];
				if (plFlight && strlen(plFlight->Flti) > 0)
				{
					if( olIds.Find( CString(plFlight->Flti)) != -1)
					{
						if (olFlightId.IsEmpty())
							olFlightId = CString(plFlight->Flti);
						else
						{
							if (olFlightId != CString(plFlight->Flti))
							{
								blConflict = true;
								break;
							}
						}
					}
				}
			}
			if(popKonfList != NULL && blConflict)
			{
				for (int i = 0; i < olOverlapFlights.GetSize(); i++)
				{
					DIAFLIGHTDATA *plFlight = (DIAFLIGHTDATA *)olOverlapFlights[i];
					//Diffrent Flight ID for Gate %s: % -> Can be %s 
					if( olIds.Find( CString(plFlight->Flti)) != -1)
					{
						olConfText.Format(GetString(IDS_STRING2300), opGat, plFlight->Flti, olIds);
						AddToKonfList(plFlight, cpFPart, 0, ' ', IDS_STRING2300, olConfText, *popKonfList);
						blRet = false;
					}
				}
			}
		}
	}



	// Check max. number of flights at gate ( overlapp )
	int ilPax = 0;
//	int ilCountActAtGat = ogPosDiaFlightData.GetMaxOverlapAtGate(olStartAlloc, olEndAlloc, opGat, prpFlight);
	int ilCountActAtGat = ogPosDiaFlightData.GetMaxOverlapAtGateAndPax(olStartAlloc, olEndAlloc, opGat, prpFlight, ilPax);

	if( prlGatRes->Mult < ilCountActAtGat)
	{
		if(popKonfList != NULL)
		{
			//To many aircrafts at gate %s: %d Max: %d
			olConfText.Format(GetString(IDS_STRING1726), opGat, ilCountActAtGat, prlGatRes->Mult);
			AddToKonfList(prpFlight, cpFPart, 0, ' ', IDS_STRING1726, olConfText, *popKonfList);
			blRet = false;
		}
		else
		{
			return false;
		}
	}


	// inbound gate 
	if(strcmp(prpFlight->Des3, pcgHome) == 0)
	{
		if(!prlGatRes->Inbo)
		{
			if(popKonfList != NULL)
			{
				//Gate %s can not be use for inbound 
				olConfText.Format(GetString(IDS_STRING1727), opGat);
				AddToKonfList(prpFlight, cpFPart, 0, ' ', IDS_STRING1727, olConfText, *popKonfList);
				blRet = false;
			}
			else
			{
				return false;
			}
		}
	}
	else
	{
		if(!prlGatRes->Outb)
		{
			if(popKonfList != NULL)
			{
				//Gate %s can not be use for outbound 
				olConfText.Format(GetString(IDS_STRING1728), opGat);
				AddToKonfList(prpFlight, cpFPart, 0, ' ', IDS_STRING1728, olConfText, *popKonfList);
				blRet = false;
			}
			else
			{
				return false;
			}
		}
	}


	//max Pax

	int ilPaxt = atoi(prpFlight->Paxt);

	if(prlGatRes->Maxp > 0)
	{
//		if(prlGatRes->Maxp < ilPaxt)
		if(prlGatRes->Maxp < ilPax)
		{
			//check second gate
			CString olGate2("");
			int ilMaxp2 = 0;
			int ilPax2 = 0;
			bool blCflInserted = false;
			if (ogPosDiaFlightData.HasTwoGates(prpFlight))
			{
				if (cpFPart == 'A')
				{
					if (ipGatNo == 1)
						olGate2 = prpFlight->Gta2;
					else
						olGate2 = prpFlight->Gta1;
				}
				if (cpFPart == 'D')
				{
					if (ipGatNo == 1)
						olGate2 = prpFlight->Gtd2;
					else
						olGate2 = prpFlight->Gtd1;
				}

				if (!olGate2.IsEmpty())
				{
					GAT_RESLIST *prlGat2Res = GetGatRes(olGate2); 
					if(prlGat2Res)
					{
						CTime olStartAlloc2;
						CTime olEndAlloc2;
						int ilGate2No = 1;
						if (ipGatNo == 1)
							ilGate2No = 2;

						if (ogPosDiaFlightData.GetGatAllocTimes(*prpFlight, cpFPart, olGate2, ilGate2No, olStartAlloc2, olEndAlloc2, bpPreCheck))
						{
							if(popRightBuffer != NULL)
								olStartAlloc2 -= *popRightBuffer;

							if(popLeftBuffer != NULL)
								olEndAlloc2 += *popLeftBuffer;
							int ilCountActAtGat2 = ogPosDiaFlightData.GetMaxOverlapAtGateAndPax(olStartAlloc2, olEndAlloc2, olGate2, prpFlight, ilPax2);

							ilMaxp2 = prlGat2Res->Maxp;
							if(ilMaxp2 < ilPax2)
							{
								if(popKonfList != NULL)
								{
									//Too many Passengers at Gate %s: %d Max: %d and Gate %s: %d Max: %d
									olConfText.Format(GetString(IDS_STRING2411), opGat, ilPax, prlGatRes->Maxp, olGate2, ilPax2, ilMaxp2);
									AddToKonfList(prpFlight, cpFPart, 0, ' ', IDS_STRING1729, olConfText, *popKonfList);
									blRet = false;
									blCflInserted = true;
								}
								else
								{
									return false;
								}
							}
						}
					}
				}
			}


			if(!blCflInserted && popKonfList != NULL)
			{
				//Too many Passengers at Gate %s: %d Max: %d
				olConfText.Format(GetString(IDS_STRING2406), opGat, ilPax, prlGatRes->Maxp);
				AddToKonfList(prpFlight, cpFPart, 0, ' ', IDS_STRING1729, olConfText, *popKonfList);
				blRet = false;
			}
			else
			{
				return false;
			}
		}
	}


	// Check airline restrictions for gate
	if(prlGatRes->PreferredAL.GetSize() > 0 || prlGatRes->ExcludedAL.GetSize() > 0 || prlGatRes->AssignedAL.GetSize() > 0)
	{
		if (CheckResToResLists(prpFlight->Alc2, prpFlight->Alc3, prlGatRes->AssignedAL, prlGatRes->ExcludedAL, prlGatRes->PreferredAL) == false)
		{
			if(popKonfList != NULL)
			{
				// Gate %s: Airline restriction (%s): %s
				olConfText.Format(GetString(IDS_STRING1730), opGat, prlGatRes->ALUserStr, CString(prpFlight->Alc2)+"/"+CString(prpFlight->Alc3));
				AddToKonfList(prpFlight, cpFPart, 0, ' ', IDS_STRING1730, olConfText, *popKonfList);
				blRet = false;
			}
			else
			{
				return false;
			}
		}
	}


	// Check nature ristriction for gate
	if(prlGatRes->PreferredNA.GetSize() > 0 || prlGatRes->ExcludedNA.GetSize() > 0 || prlGatRes->AssignedNA.GetSize() > 0)
	{
		if( strlen(prpFlight->Ttyp) > 0 )
		{
			if (CheckResToResLists(prpFlight->Ttyp, prlGatRes->AssignedNA, prlGatRes->ExcludedNA, prlGatRes->PreferredNA) == false)
			{
				if(popKonfList != NULL)
				{
					// Gate %s: Nature restriction (%s): %s
					olConfText.Format(GetString(IDS_STRING1731), opGat, prlGatRes->NAUserStr, prpFlight->Ttyp);
					AddToKonfList(prpFlight, cpFPart, 0, ' ', IDS_STRING1731, olConfText, *popKonfList);
					blRet = false;					
				}
				else
				{
					return false;
				}
			}
		}
	}

	// Check service ristriction for gate
	if(prlGatRes->PreferredService.GetSize() > 0 || prlGatRes->ExcludedService.GetSize() > 0 || prlGatRes->AssignedService.GetSize() > 0)
	{
		if( strlen(prpFlight->Styp) > 0 )
		{
			if (CheckResToResLists(prpFlight->Styp, prlGatRes->AssignedService, prlGatRes->ExcludedService, prlGatRes->PreferredService) == false)
			{
				if(popKonfList != NULL)
				{
					// Gate %s: Nature restriction (%s): %s
					olConfText.Format(GetString(IDS_STRING2633), opGat, prlGatRes->ServiceUserStr, prpFlight->Styp);
					AddToKonfList(prpFlight, cpFPart, 0, ' ', IDS_STRING2633, olConfText, *popKonfList);
					blRet = false;					
				}
				else
				{
					return false;
				}
			}
		}
	}


	// Check nature ristriction from  nature table 
	if( !CString(prpFlight->Ttyp).IsEmpty() )
	{
		if(FindInStrArray(omGatNatNoAllocList, prpFlight->Ttyp))
//		if( omGatNatNoAllocList.Find(prpFlight->Ttyp) >= 0)
		{
			if(popKonfList != NULL)
			{
				//Global nature ristriction (%s) at gate %s: %s
				olConfText.Format(GetString(IDS_STRING1732), omGatNatNoAllocListPlain,opGat, prpFlight->Ttyp);
				AddToKonfList(prpFlight, cpFPart, 0, ' ', IDS_STRING1732, olConfText, *popKonfList);
				blRet = false;
			}
			else
			{
				return false;
			}
		}
	}




	// Check org/des restrictions for gate
	if(prlGatRes->PreferredOrgDes.GetSize() > 0 || prlGatRes->ExcludedOrgDes.GetSize() > 0 || prlGatRes->AssignedOrgDes.GetSize() > 0)
	{
		CString olOrgDes3;
		CString olOrgDes4;

		CString olVia3;
		CString olVia4;
		
		if (cpFPart == 'A')
		{
			olOrgDes3 = prpFlight->Org3;
			olOrgDes4 = prpFlight->Org4;
		}
		else
		{

			if( ipGatNo == 2  &&  CString(prpFlight->Gd2d) == "X"  && atoi(prpFlight->Vian) > 0)
			{
				CString olVia = CString(prpFlight->Vial); 

				olOrgDes3 = olVia.Mid(1,3);
				olOrgDes4 = olVia.Mid(4,4);

			}
			else
			{
				olOrgDes3 = prpFlight->Des3;
				olOrgDes4 = prpFlight->Des4;

				if( atoi(prpFlight->Vian) > 0)
				{
					CString olVia = CString(prpFlight->Vial); 

					olVia3 = olVia.Mid(1,3);
					olVia4 = olVia.Mid(4,4);
				}
			}
		}

		if (CheckResToResLists2(olOrgDes3, olOrgDes4, olVia3, olVia4, prlGatRes->AssignedOrgDes, prlGatRes->ExcludedOrgDes, prlGatRes->PreferredOrgDes) == false)
		{
			if(popKonfList != NULL)
			{
				// Gate %s: Orig/Dest restriction (%s): %s
				olConfText.Format(GetString(IDS_STRING2006), opGat, prlGatRes->OrgDesUserStr, olOrgDes3+"/"+olOrgDes4);
				AddToKonfList(prpFlight, cpFPart, 0, ' ', IDS_STRING2006, olConfText, *popKonfList);
				blRet = false;
			}
			else
			{
				return false;
			}
		}
	}

	// Check aircraft restrictions
	if(prlGatRes->PreferredAC.GetSize() > 0 || prlGatRes->ExcludedAC.GetSize() > 0 || prlGatRes->AssignedAC.GetSize() > 0)
	{
		if (CheckResToResLists(prpFlight->Act3, prpFlight->Act5, prlGatRes->AssignedAC, prlGatRes->ExcludedAC, prlGatRes->PreferredAC) == false)
		{
			if(popKonfList != NULL)
			{
				// Gate %s: Aircraft restriction (%s): %s
				olConfText.Format(GetString(IDS_STRING2796), opGat, prlGatRes->ACUserStr + prlGatRes->ACGroupUserStr, CString(prpFlight->Act3)+"/"+CString(prpFlight->Act5));
				AddToKonfList(prpFlight, cpFPart, 0, ' ', IDS_STRING2796, olConfText, *popKonfList);
				blRet = false;
			}
			else
			{
				return false;
			}
		}
	}




	if (!bgGATPOS)
	{
		//// Position - Gate Relation
		if (ogKonflikte.IsConflictActivated(IDS_STRING2001))
		{
			if (!prlGatRes->RelPos1.IsEmpty() || !prlGatRes->RelPos2.IsEmpty())
			{
				bool blNoConf = true;
				if (cpFPart == 'A')
				{
					if (strlen(prpFlight->Psta) > 0 || !opPrePos.IsEmpty())
					{
						if (!opPrePos.IsEmpty())
						{
							if (strcmp(prlGatRes->RelPos1, opPrePos) != 0 &&
								strcmp(prlGatRes->RelPos2, opPrePos) != 0)
								blNoConf = false;
						}
						else
						{
							if (strcmp(prlGatRes->RelPos1, prpFlight->Psta) != 0 &&
								strcmp(prlGatRes->RelPos2, prpFlight->Psta) != 0)
								blNoConf = false;
						}
					}
				}
				if (cpFPart == 'D')
				{
					if (strlen(prpFlight->Pstd) > 0 || !opPrePos.IsEmpty())
					{
						if (!opPrePos.IsEmpty())
						{
							if (strcmp(prlGatRes->RelPos1, opPrePos) != 0 &&
								strcmp(prlGatRes->RelPos2, opPrePos) != 0)
								blNoConf = false;
						}
						else
						{
							if (strcmp(prlGatRes->RelPos1, prpFlight->Pstd) != 0 &&
								strcmp(prlGatRes->RelPos2, prpFlight->Pstd) != 0)
								blNoConf = false;
						}
					}
				}

				if (!blNoConf)
				{
					if(popKonfList != NULL)
					{
						//Invalid Position - Gate Relation!
						AddToKonfList(prpFlight, cpFPart, 0, ' ', IDS_STRING2001, GetString(IDS_STRING2001), *popKonfList);
						blRet = false;
					}
					else
					{
						return false;
					}
				}
			}
		}
	}


	//// BoardingBridge - BusGate Relation
	PST_RESLIST *prlPosRes;
		
	bool blNoConf = true;
	if (cpFPart == 'A')
	{
		if (strlen(prpFlight->Psta) > 0 || !opPrePos.IsEmpty())
		{
			if (opPrePos.IsEmpty())
				prlPosRes = GetPstRes(prpFlight->Psta);
			else
				prlPosRes = GetPstRes(opPrePos);

			if (prlPosRes != NULL)
			{
				if ((strcmp(prlPosRes->Brgs, "X") != 0 && strcmp(prlGatRes->Busg, "X") != 0))
					blNoConf = false;
			}
		}
	}
	if (cpFPart == 'D')
	{
		if (strlen(prpFlight->Pstd) > 0 || !opPrePos.IsEmpty())
		{
			if (opPrePos.IsEmpty())
				prlPosRes = GetPstRes(prpFlight->Pstd);
			else
				prlPosRes = GetPstRes(opPrePos);

			if (prlPosRes != NULL)
			{
				if ((strcmp(prlPosRes->Brgs, "X") != 0 && strcmp(prlGatRes->Busg, "X") != 0))
					blNoConf = false;
			}
		}
	}

	if (!blNoConf)
	{
		if(popKonfList != NULL)
		{
			// Invalid 'Pass. Boarding Bridge' - 'Bus Gate' Relation!
			AddToKonfList(prpFlight, cpFPart, 0, ' ', IDS_STRING2065, GetString(IDS_STRING2065), *popKonfList);
			blRet = false;
		}
		else
		{
			return false;
		}
	}

	
	if (!CheckGatNeighborConf(prpFlight, cpFPart, opGat, ipGatNo, popKonfList, popRightBuffer, popLeftBuffer, bpPreCheck))
		blRet = false;


	//MWO: 19.04.05: Hier muss wohl dann die Mixed Flights checkerei eingebaut werden


	return blRet;
}


static int CompareAutoAllocate(const DiaCedaFlightData::SCORELIST **e1, const DiaCedaFlightData::SCORELIST **e2)
{
	if(bgAutoAllocWithScore)
	{
		return (((**e1).Score == (**e2).Score) ? 0 : (((**e1).Score < (**e2).Score) ? 1 : -1));
	}
	return 0;
}



static int CompareAutoAllocate(const DiaCedaFlightData::RKEYLIST **e1, const DiaCedaFlightData::RKEYLIST **e2)
{
	CCS_TRY

	//wingspan
	RecordSet olCurrActRecord;
	bool blActFoundR1 = ogBCD.GetRecord("ACT", "ACT3", ((**e1).Rotation[0].Act3), olCurrActRecord);
	if(blActFoundR1 == false)
	{
		blActFoundR1 = ogBCD.GetRecord("ACT", "ACT5", ((**e1).Rotation[0].Act5), olCurrActRecord);
	}

	double dlAcwsR1 = (double) atof(olCurrActRecord[ ogBCD.GetFieldIndex("ACT","ACWS")]);


	bool blActFoundR2 = ogBCD.GetRecord("ACT", "ACT3", ((**e2).Rotation[0].Act3), olCurrActRecord);
	if(blActFoundR2 == false)
	{
		blActFoundR2 = ogBCD.GetRecord("ACT", "ACT5", ((**e2).Rotation[0].Act5), olCurrActRecord);
	}

	double dlAcwsR2 = (double) atof(olCurrActRecord[ ogBCD.GetFieldIndex("ACT","ACWS")]);

	if (blActFoundR1 && blActFoundR2)
	{
		if (dlAcwsR1 > dlAcwsR2)
			return -1;
		if (dlAcwsR1 < dlAcwsR2)
			return 1;
	}

	//groundtime
	CTime olTifa1 = TIMENULL;
	CTime olTifd1 = TIMENULL;
	CTime olTifa2 = TIMENULL;
	CTime olTifd2 = TIMENULL;

	CTime olSchedTime1 = TIMENULL;
	CTime olSchedTime2 = TIMENULL;

	for(int i = (**e1).Rotation.GetSize() - 1; i >= 0; i--)
	{
		if(CString((**e1).Rotation[i].Adid) == "A")
		{
			olTifa1 = (**e1).Rotation[i].Tifa;
			olSchedTime1 = (**e1).Rotation[i].Stoa;
		}
		else
		{
			if (olSchedTime1 == TIMENULL)
				olSchedTime1 = (**e1).Rotation[i].Stod;

			olTifd1 = (**e1).Rotation[i].Tifd;
		}
	}

	for( i = (**e2).Rotation.GetSize() - 1; i >= 0; i--)
	{
		if(CString((**e2).Rotation[i].Adid) == "A")
		{
			olTifa2 = (**e2).Rotation[i].Tifa;
			olSchedTime2 = (**e2).Rotation[i].Stoa;
		}
		else
		{
			if (olSchedTime2 == TIMENULL)
				olSchedTime2 = (**e2).Rotation[i].Stod;

			olTifd2 = (**e2).Rotation[i].Tifd;
		}
	}
	
	CTimeSpan olSpan1;
	CTimeSpan olSpan2;

	if(olTifa2 == TIMENULL || olTifd2 == TIMENULL)
		olSpan2 = CTimeSpan(0,0,30,0);
	else
		olSpan2 = olTifd2 - olTifa2;

	if(olTifa1 == TIMENULL || olTifd1 == TIMENULL)
		olSpan1 = CTimeSpan(0,0,30,0);
	else
		olSpan1 = olTifd1 - olTifa1;

	if (olSpan1 > olSpan2)
		return -1;
	if (olSpan1 < olSpan2)
		return 1;
	
	//scheduled times
	if(olSchedTime1 != TIMENULL && olSchedTime2 != TIMENULL)
	{
		if (olSchedTime1 < olSchedTime2)
			return -1;
		if (olSchedTime1 > olSchedTime2)
			return 1;
	}

	//flightnumber
	CString olFlno1 = (**e1).Rotation[0].Flno;
	CString olFlno2 = (**e2).Rotation[0].Flno;
	
	if(strcmp(olFlno1, olFlno2) == 0)
	{
		return 0;
	}
	else
	{
		if(strcmp(olFlno1, olFlno2) > 0)
		{
			return 1;
		}
		else
		{
			return -1;
		}
	}

	CCS_CATCH_ALL

	return 0;


/*	int ilOrd1 = -1;
	int ilOrd2 = -1;
	
	CString olOrder = CString(" DRIM");

	CTime olTifa1 = TIMENULL;
	CTime olTifd1 = TIMENULL;
	CTime olTifa2 = TIMENULL;
	CTime olTifd2 = TIMENULL;


	for(int i = (**e1).Rotation.GetSize() - 1; i >= 0; i--)
	{
		ilOrd1 = max(ilOrd1, olOrder.Find(CString((**e1).Rotation[i].Flti) ));

		if(CString((**e1).Rotation[i].Adid) == "A")
			olTifa1 = (**e1).Rotation[i].Tifa;
		else
			olTifd1 = (**e1).Rotation[i].Tifd;

	}


	for( i = (**e2).Rotation.GetSize() - 1; i >= 0; i--)
	{
		ilOrd2 = max(ilOrd2, olOrder.Find(CString((**e2).Rotation[i].Flti) ));

		if(CString((**e2).Rotation[i].Adid) == "A")
			olTifa2 = (**e2).Rotation[i].Tifa;
		else
			olTifd2 = (**e2).Rotation[i].Tifd;
	
	}

	
	CTimeSpan olSpan1;
	CTimeSpan olSpan2;

	if(olTifa2 == TIMENULL || olTifd2 == TIMENULL)
		olSpan2 = CTimeSpan(0,0,30,0);
	else
		olSpan2 = olTifd2 - olTifa2;


	if(olTifa1 == TIMENULL || olTifd1 == TIMENULL)
		olSpan1 = CTimeSpan(0,0,30,0);
	else
		olSpan1 = olTifd1 - olTifa1;


	if(ilOrd1 == ilOrd2)
	{
		return (olSpan1 < olSpan2) ? 1 : -1;
	}
	else
	{
		return (ilOrd1 > ilOrd2) ? 1 : -1;
	}
*/
}



void SpotAllocation::CreateFlightList()
{

	POSITION pos;
	void *pVoid;
	DiaCedaFlightData::RKEYLIST *prlRkey;
	omPosFlights.DeleteAll();
	DiaCedaFlightData::RKEYLIST *prlRkey2;
	DIAFLIGHTDATA *prlFlightA = NULL;
	DIAFLIGHTDATA *prlFlightD = NULL;

	omFlights.DeleteAll();

	TRACE("\n  %s --> %s", omParameters.omFrom.Format("%H:%M  %d.%m.%Y"), omParameters.omTo.Format("%H:%M  %d.%m.%Y"));


	for( pos = ogPosDiaFlightData.omRkeyMap.GetStartPosition(); pos != NULL; )
	{
		ogPosDiaFlightData.omRkeyMap.GetNextAssoc( pos, pVoid , (void *&)prlRkey );

		prlFlightA = ogPosDiaFlightData.GetFlightAInRotation(prlRkey);
		prlFlightD = ogPosDiaFlightData.GetFlightDInRotation(prlRkey);

		if (prlFlightA && CheckPostFlightPosDia(prlFlightA->Urno, NULL))
			continue;
		if (prlFlightD && CheckPostFlightPosDia(prlFlightD->Urno, NULL))
			continue;

		if(prlFlightA != NULL)
		{
			// !!! NO CIRCULAR FLIGHTS (AND ALL CONNECTED) !!!
			if((CString("SOTG ").Find(CString(prlFlightA->Ftyp)) < 0) )
				prlFlightA = NULL;

		}

		if(prlFlightD != NULL)
		{
			// !!! NO CIRCULAR FLIGHTS (AND ALL CONNECTED) !!!
			if((CString("SOTG ").Find(CString(prlFlightD->Ftyp)) < 0) )
				prlFlightD = NULL;
		}

		if((prlFlightA != NULL) && 	(prlFlightD != NULL))
		{
			
			if( !IsOverlapped(prlFlightA->Stoa, prlFlightD->Stod, omParameters.omFrom, omParameters.omTo) )
				continue;
		}
		else
		{
			if(prlFlightA != NULL)
			{
				if( !IsBetween(prlFlightA->Stoa, omParameters.omFrom, omParameters.omTo) )
					continue;
			}
			else
			{
				if(prlFlightD != NULL)
				{
					if( !IsBetween(prlFlightD->Stod, omParameters.omFrom, omParameters.omTo) )
						continue;
				}
			}

		}


		if((prlFlightA != NULL) || 	(prlFlightD != NULL))
		{

			for( int i = 0; i < prlRkey->Rotation.GetSize() ; i++)
			{
				prlFlightA = &prlRkey->Rotation[i];
				
				//TRACE("\n%s %s %s %s %s", prlFlightA->Flno, prlFlightA->Adid, prlFlightA->Ftyp, prlFlightA->Tifa.Format("%d.%m.%y %H:%M"), prlFlightA->Tifd.Format("%d.%m.%y %H:%M"));		
				
				
				if(prlRkey->Rotation.GetSize() == 1 && prlFlightA->Adid[0] == 'D')
				{
					prlFlightD = prlFlightA;
					prlFlightA = NULL;
				}
				else
				{
					
					if(i + 1 <  prlRkey->Rotation.GetSize())
					{
						prlFlightD = &prlRkey->Rotation[i+1];
						
						
						if(prlFlightA->Adid[0] == 'D')
						{
							prlFlightD = prlFlightA;
							prlFlightA = NULL;
						}
						else
						{
							
							if(prlFlightD->Tifd <= prlFlightA->Tifa)
							{
								prlFlightD = NULL;
							}
							else
							{
								if(prlFlightD->Adid[0] != 'B')
									i++;
							}				
						}
					}
					else
					{
						if(prlFlightA->Adid[0] == 'D')
						{
							prlFlightD = prlFlightA;
							prlFlightA = NULL;
						}
						else
						{
							prlFlightD = NULL;
						}
					}
				}

				if((prlFlightA != NULL) || 	(prlFlightD != NULL))
				{

					prlRkey2 = new DiaCedaFlightData::RKEYLIST;

					if(prlFlightA != NULL)
						prlRkey2->Rotation.Add(prlFlightA);

					if(prlFlightD != NULL)
						prlRkey2->Rotation.Add(prlFlightD);

					omFlights.Add(prlRkey2);

					if(bgAutoAllocWithScore && omParameters.bmAllocatePst)
						SetFlightOrderScore(prlFlightA, prlFlightD);
				}
			}


		}

	}

/*
		prlRkey->Rotation.Sort(DiaCedaFlightData::CompareRotationFlight);
		
		prlRkey2 = new DiaCedaFlightData::RKEYLIST;


		for(int i = 0; i < prlRkey->Rotation.GetSize() ; i++)
		{
			prlFlightA = &prlRkey->Rotation[i];
			
			
			if( prlFlightA->Adid[0] == 'D' || prlFlightA->Adid[0] == 'A') 
			{
				prlFlight = new DIAFLIGHTDATA;
				*prlFlight = *prlFlightA;
				rlRkey.Rotation.Add(prlFlight);
			}
			else
			{
				prlFlight = new DIAFLIGHTDATA;
				*prlFlight = *prlFlightA;
				strcpy(prlFlight->Adid, "D");
				rlRkey.Rotation.Add(prlFlight);
				
				prlFlight = new DIAFLIGHTDATA;
				*prlFlight = *prlFlightA;
				strcpy(prlFlight->Adid, "A");
				rlRkey.Rotation.Add(prlFlight);
				
			}
			
		}

*/


/*
	for( pos = ogPosDiaFlightData.omRkeyMap.GetStartPosition(); pos != NULL; )
	{
		ogPosDiaFlightData.omRkeyMap.GetNextAssoc( pos, pVoid , (void *&)prlRkey );

		prlFlightA = ogPosDiaFlightData.GetFlightAInRotation(prlRkey);
		prlFlightD = ogPosDiaFlightData.GetFlightDInRotation(prlRkey);

		if (prlFlightA && CheckPostFlightPosDia(prlFlightA->Urno, NULL))
			continue;
		if (prlFlightD && CheckPostFlightPosDia(prlFlightD->Urno, NULL))
			continue;

		if(prlFlightA != NULL)
		{
			// !!! NO CIRCULAR FLIGHTS (AND ALL CONNECTED) !!!
			if((CString("SOTG ").Find(CString(prlFlightA->Ftyp)) < 0) ||
				(prlFlightA->Adid[0] == 'B'))
				prlFlightA = NULL;

		}

		if(prlFlightD != NULL)
		{
			// !!! NO CIRCULAR FLIGHTS (AND ALL CONNECTED) !!!
			if((CString("SOTG ").Find(CString(prlFlightD->Ftyp)) < 0) ||
				(prlFlightD->Adid[0] == 'B'))
				prlFlightD = NULL;
		}

		if((prlFlightA != NULL) && 	(prlFlightD != NULL))
		{
			
			if( !IsOverlapped(prlFlightA->Stoa, prlFlightD->Stod, omParameters.omFrom, omParameters.omTo) )
				continue;
		}
		else
		{
			if(prlFlightA != NULL)
			{
				if( !IsBetween(prlFlightA->Stoa, omParameters.omFrom, omParameters.omTo) )
					continue;
			}
			else
			{
				if(prlFlightD != NULL)
				{
					if( !IsBetween(prlFlightD->Stod, omParameters.omFrom, omParameters.omTo) )
						continue;
				}
			}

		}


		if((prlFlightA != NULL) || 	(prlFlightD != NULL))
		{

			prlRkey2 = new DiaCedaFlightData::RKEYLIST;

			if(prlFlightA != NULL)
				prlRkey2->Rotation.Add(prlFlightA);

			if(prlFlightD != NULL)
				prlRkey2->Rotation.Add(prlFlightD);

			omFlights.Add(prlRkey2);

			if(bgAutoAllocWithScore && omParameters.bmAllocatePst)
				SetFlightOrderScore(prlFlightA, prlFlightD);


		}

	}

	*/
  
	omFlights.Sort(CompareAutoAllocate);
	omPosFlights.Sort(CompareAutoAllocate);


	omChangedFlightsRkeyMap.RemoveAll();

}

  
bool SpotAllocation::AutoPstAllocate()
{
	omChangedUrnoMap.RemoveAll();
	omAutoAllocPst.RemoveAll();
//############# only for test ##################################################
	ofstream ofProt;
	char opTrenner[64];
	CString olFileName;
	char pclExcelPath[256];
	int ilwidth=1;

	if (bgCreateAutoAllocFiles)
	{
		char pclConfigPath[256];
		if (getenv("CEDA") == NULL)
			strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
		else
			strcpy(pclConfigPath, getenv("CEDA"));

		GetPrivateProfileString(ogAppName, "Excel", "DEFAULT",
			pclExcelPath, sizeof pclExcelPath, pclConfigPath);

		GetPrivateProfileString(ogAppName, "ExcelSeparator", ";",
			opTrenner, sizeof opTrenner, pclConfigPath);

		olFileName = "AllocationProtokollPositions";

		char pHeader[500];
		strcpy (pHeader, CCSLog::GetTmpPath());
		CString path = pHeader;
		olFileName =  path + "\\" + olFileName + ".csv";

		ofProt.open( olFileName, ios::out);
	}
//############# only for test ##################################################

 	DiaCedaFlightData::RKEYLIST *prlRkey;
	DIAFLIGHTDATA *prlFlightA = NULL;
 	DIAFLIGHTDATA *prlFlightD = NULL;
 
	CTime olStart;
	CTime olEnd;

	pomParent->SendMessage(WM_SAS_PROGESS_INIT, omFlights.GetSize()*3, 0 );
	TRACE("\n=======================================");


	CString olRecomPos;
	CStringArray olPosArray;
//	for(int k = omFlights.GetSize()-1; k >= 0 ; k--)
	bgAutoAllocWithGroundTime = true;//only allocate  if groundtime in rule matches
	bgAutoAllocWithExactParameter = false;//only allocate  if all parameters in rule matches
	for (int ilRound=1; ilRound <4; ilRound++)
	{
		CString olRound;
		if (ilRound == 1)
		{
			olRound = "########### First Round with Groundtime and without Matching all Parameters  ############";
		}
		else if (ilRound == 2)
		{
			bgAutoAllocWithGroundTime = false;
			bgAutoAllocWithExactParameter = true;
			olRound = "########### Second Round without Groundtime and Matching all Parameters ############";
		}
		else if (ilRound == 3)
		{
			bgAutoAllocWithGroundTime = false;
			bgAutoAllocWithExactParameter = false;
			olRound = "########### Third Round without Groundtime and without Matching all Parameters ############";
		}

//############# only for test ##################################################
		if (bgCreateAutoAllocFiles)
		{
			ofProt	<< endl << olRound << endl;

			if (bgNewGoodCount)
			{
				ofProt  << setw(ilwidth) << "Prio"
					<< setw(1) << opTrenner
					<< setw(ilwidth) << "Sequence"
					<< setw(1) << opTrenner
					<< setw(ilwidth) << "Pnam"
					<< setw(1) << opTrenner
					<< setw(ilwidth) << "Groundtime"
					<< setw(1) << opTrenner
					<< setw(ilwidth) << "Good Count"
					<< setw(1) << opTrenner
					<< setw(ilwidth) << "Bad Count"
					<< setw(1) << opTrenner
					<< setw(ilwidth) << "Prio/Sequ"
					<< setw(1) << opTrenner
					<< setw(ilwidth) << "Nature"
					<< setw(1) << opTrenner
					<< setw(ilwidth) << "Airline"
					<< setw(1) << opTrenner
					<< setw(ilwidth) << "ACT"
					<< setw(1) << opTrenner
					<< setw(ilwidth) << "Flight ID"
				<< endl;
			}
			else
			{
				ofProt  << setw(ilwidth) << "Prio"
					<< setw(1) << opTrenner
					<< setw(ilwidth) << "Sequence"
					<< setw(1) << opTrenner
					<< setw(ilwidth) << "Pnam"
					<< setw(1) << opTrenner
					<< setw(ilwidth) << "Groundtime"
					<< setw(1) << opTrenner
					<< setw(ilwidth) << "Good Count"
					<< setw(1) << opTrenner
					<< setw(ilwidth) << "Bad Count"
					<< setw(1) << opTrenner
					<< setw(ilwidth) << "Nature"
					<< setw(1) << opTrenner
					<< setw(ilwidth) << "Airline"
					<< setw(1) << opTrenner
					<< setw(ilwidth) << "ACT"
					<< setw(1) << opTrenner
					<< setw(ilwidth) << "Service Type"
				<< endl;
			}
		}
//############# only for test ##################################################


		for(int k=0; k<omFlights.GetSize(); k++)
		{
			olRecomPos.Empty();
			prlRkey = &omFlights[k];

 
			prlFlightA = ogPosDiaFlightData.GetFlightAInRotation(prlRkey);
			prlFlightD = ogPosDiaFlightData.GetFlightDInRotation(prlRkey);
		
			if(prlFlightD != NULL)
			{
				if(!CString(prlFlightD->Pstd).IsEmpty())
				{
					// the recommended position is the position of the departure flight
					olRecomPos = prlFlightD->Pstd;
					prlFlightD = NULL;
				}
			}

			if(prlFlightA != NULL)
			{
				if(!CString(prlFlightA->Psta).IsEmpty())
				{
					// the recommended position is the position of the arrival flight
					olRecomPos = prlFlightA->Psta;
					prlFlightA = NULL;
				}
			}

			int ilCount = 1;
			
			if(prlFlightD != NULL && prlFlightA != NULL)
			{
				CTimeSpan olDiff;
				olDiff = prlFlightD->Tifd - prlFlightA->Tifa;

				if(olDiff.GetTotalMinutes() > 1440)
				{				
					ilCount = 2;
					olRecomPos.Empty();
				}
			}

			CString olAllocatedPos = "";
			CString olTmp;
			if(prlFlightD != NULL || prlFlightA != NULL)
			{
				if (ilCount == 1)
				{
					// Allocate one position
					if (bgGATPOS)
						AutoPstAllocateFlightsGatPos(prlFlightA, prlFlightD, olRecomPos, olAllocatedPos, ofProt);
					else
						AutoPstAllocateFlights(prlFlightA, prlFlightD, olRecomPos);
				}
				else if (ilCount == 2)
				{
					// Allocate two positions
					if (bgGATPOS)
					{
						AutoPstAllocateFlightsGatPos(prlFlightA, NULL, olRecomPos, olAllocatedPos, ofProt);
 						AutoPstAllocateFlightsGatPos(NULL, prlFlightD, olRecomPos, olTmp, ofProt);
					}
					else
					{
						AutoPstAllocateFlights(prlFlightA, NULL, olRecomPos);
 						AutoPstAllocateFlights(NULL, prlFlightD, olRecomPos);
					}
				}
				olPosArray.Add(olAllocatedPos);
			}
			pomParent->SendMessage(WM_SAS_PROGESS_UPDATE, 1, 0 );
		}
	}
//############# only for test ##################################################
	ofProt.close();

	if (bgCreateAutoAllocFiles)
	{
		PrintSortedRulesPos();
	}
//############# only for test ##################################################

	bgAutoAllocWithGroundTime = false;//only allocate  if groundtime in rule matches
	bgAutoAllocWithExactParameter = false;//only allocate  if all parameters in rule matches
	return true;
}

bool SpotAllocation::PrintAutoAlloc(bool& bpPst, bool& bpGat, bool& bpBlt)
{
	if (bgCreateAutoAllocFiles)
		PrintSortedFlightsAutoAlloc(CString(""));

	if (!bgPrintAutoAlloc || !bgCreateAutoAllocFiles)
		return false;

	char pclConfigPath[256];
	char pclExcelPath[256];

	if (getenv("CEDA") == NULL)
		strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
	else
		strcpy(pclConfigPath, getenv("CEDA"));

	GetPrivateProfileString(ogAppName, "Excel", "DEFAULT",
		pclExcelPath, sizeof pclExcelPath, pclConfigPath);

	char pHeader[512];
	strcpy (pHeader, CCSLog::GetTmpPath());
	CString path = pHeader;
	CString olFileName;

	CString olNextFileName;
	if (bpPst)
	{
		olNextFileName = "SortedPositionRules";
		olFileName += path + CString("\\") + olNextFileName + ".csv";

		olNextFileName = "AllocationProtokollPositions";
		olFileName += CString(" ") + path + CString("\\") + olNextFileName + ".csv";
	}

	if (bpGat)
	{
		olNextFileName = "SortedGateRules";
		olFileName += CString(" ") + path + CString("\\") + olNextFileName + ".csv";

		olNextFileName = "AllocationProtokollGates";
		olFileName += CString(" ") + path + CString("\\") + olNextFileName + ".csv";
	}

	if (bpBlt)
	{
		olNextFileName = "SortedBeltRules";
		olFileName += CString(" ") + path + CString("\\") + olNextFileName + ".csv";

		olNextFileName = "AllocationProtokollBelts";
		olFileName += CString(" ") + path + CString("\\") + olNextFileName + ".csv";
	}

	olNextFileName = "SortedFlightsForAutoAllocation";
	olFileName += CString(" ") + path + CString("\\") + olNextFileName + ".csv";


	if (bgPrintAutoAlloc && bgCreateAutoAllocFiles)
	{
		char pclTmp[4096];
		int t = olFileName.GetLength();
		strncpy(pclTmp, olFileName, 4096); 


	   // Set up parameters to be sent: 
		char *args[4];
		args[0] = "child";
		args[1] = pclTmp;
		args[2] = NULL;
		args[3] = NULL;

		_spawnv( _P_NOWAIT , pclExcelPath, args );

	}

	return true;
}

bool SpotAllocation::PrintSortedFlightsAutoAlloc(CString& opType)
{
	char pclConfigPath[256];
	char pclExcelPath[256];
	char opTrenner[64];
	if (getenv("CEDA") == NULL)
		strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
	else
		strcpy(pclConfigPath, getenv("CEDA"));

	GetPrivateProfileString(ogAppName, "Excel", "DEFAULT",
		pclExcelPath, sizeof pclExcelPath, pclConfigPath);

	GetPrivateProfileString(ogAppName, "ExcelSeparator", ";",
		opTrenner, sizeof opTrenner, pclConfigPath);

	ofstream of;

	CString olFileName;
	if (opType == "Position")
		olFileName = "SortedFlightsForAutoAllocationPositions";
	else if (opType == "Gate")
		olFileName = "SortedFlightsForAutoAllocationGates";
	else if (opType == "Belt")
		olFileName = "SortedFlightsForAutoAllocationBelts";
	else
		olFileName = "SortedFlightsForAutoAllocation";

	char pHeader[500];
	strcpy (pHeader, CCSLog::GetTmpPath());
	CString path = pHeader;
	olFileName =  path + "\\" + olFileName + ".csv";
	int ilwidth=1;

	of.open( olFileName, ios::out);

	of.setf(ios::left, ios::adjustfield);
	of  << setw(ilwidth) << "Wingspan"
		<< setw(1) << opTrenner
		<< setw(ilwidth) << "Groundtime"
		<< setw(1) << opTrenner
		<< setw(ilwidth) << "Schedule"
		<< setw(1) << opTrenner
		<< setw(ilwidth) << "Flightnumber"
		<< setw(1) << opTrenner
		<< setw(ilwidth) << "Position"
		<< setw(1) << opTrenner
		<< setw(ilwidth) << "Gate"
		<< setw(1) << opTrenner
		<< setw(ilwidth) << "Belt"
		<< setw(1) << opTrenner
		<< setw(ilwidth) << "Lounge"
		<< endl;


	for (int l=0; l<omFlights.GetSize(); l++)
	{

		//wingspan
		DIAFLIGHTDATA *prlFlight = NULL;
		prlFlight = &omFlights[l].Rotation[0];
		RecordSet olCurrActRecord;
		bool blActFoundR1 = ogBCD.GetRecord("ACT", "ACT3", prlFlight->Act3, olCurrActRecord);
		if(blActFoundR1 == false)
		{
			blActFoundR1 = ogBCD.GetRecord("ACT", "ACT5", prlFlight->Act5, olCurrActRecord);
		}

//		double dlAcwsR1 = (double) atof(olCurrActRecord[ ogBCD.GetFieldIndex("ACT","ACWS")]);
		CString dlAcwsR1 = olCurrActRecord[ ogBCD.GetFieldIndex("ACT","ACWS")];


		//groundtime
		CTime olTifa1 = TIMENULL;
		CTime olTifd1 = TIMENULL;
		CTime olSchedTime1 = TIMENULL;
		DiaCedaFlightData::RKEYLIST *prlRkeyTest;
		prlRkeyTest = &omFlights[l];

		CString olPos;
		CString olGat;
		CString olBlt;
		CString olWro;
		CString olFlnoMap;
		GetFlnoForPrintMap(prlFlight, olFlnoMap);
		for(int i = 0; i < prlRkeyTest->Rotation.GetSize(); i++)
		{
			CString X = "(X)";
			if(CString(prlRkeyTest->Rotation[i].Adid) == "A")
			{
				olTifa1 = prlRkeyTest->Rotation[i].Tifa;
				olSchedTime1 = prlRkeyTest->Rotation[i].Stoa;

				olPos += prlRkeyTest->Rotation[i].Psta;
				CString olTmp;
				if (omAutoAllocPst.Lookup(olFlnoMap, olTmp))
					olPos += X;

				olBlt += CString(prlRkeyTest->Rotation[i].Blt1);
				if (!CString(prlRkeyTest->Rotation[i].Blt2).IsEmpty())
					olBlt += "+" + CString(prlRkeyTest->Rotation[i].Blt2);
				if (omAutoAllocBlt.Lookup(olFlnoMap, olTmp) && !olBlt.IsEmpty())
					olBlt += X;

				olGat +=  CString(prlRkeyTest->Rotation[i].Gta1);
				if (!CString(prlRkeyTest->Rotation[i].Gta2).IsEmpty())
					olGat += "+" + CString(prlRkeyTest->Rotation[i].Gta2);
				if (omAutoAllocGat.Lookup(olFlnoMap, olTmp))
					olGat += X;
			}
			else
			{
				if (olSchedTime1 == TIMENULL)
					olSchedTime1 = prlRkeyTest->Rotation[i].Stod;

				olTifd1 = prlRkeyTest->Rotation[i].Tifd;

				olPos += " / " + CString(prlRkeyTest->Rotation[i].Pstd);
				CString olTmp;
				if (omAutoAllocPst.Lookup(olFlnoMap, olTmp))
					olPos += X;

				olGat +=  " / " + CString(prlRkeyTest->Rotation[i].Gtd1);
				if (!CString(prlRkeyTest->Rotation[i].Gtd2).IsEmpty())
					olGat += "+" + CString(prlRkeyTest->Rotation[i].Gtd2);
				if (omAutoAllocGat.Lookup(olFlnoMap, olTmp))
					olGat += X;

				olWro += CString(prlRkeyTest->Rotation[i].Wro1);
				if (!CString(prlRkeyTest->Rotation[i].Wro2).IsEmpty())
					olWro += "+" + CString(prlRkeyTest->Rotation[i].Wro2);
				if (omAutoAllocWro.Lookup(olFlnoMap, olTmp))
					olWro += X;
			}
		}

		
		CTimeSpan olSpan1;

		if(olTifa1 == TIMENULL || olTifd1 == TIMENULL)
			olSpan1 = CTimeSpan(0,0,30,0);
		else
			olSpan1 = olTifd1 - olTifa1;

		char buffer[64];
		CString olSpan = itoa(olSpan1.GetTotalMinutes(),buffer,10);
		CString olSched = olSchedTime1.Format("%H:%M");
		
		of.setf(ios::left, ios::adjustfield);

		of  << setw(ilwidth) << " " << dlAcwsR1
			<< setw(1) << opTrenner
			<< setw(ilwidth) << " " << olSpan
			<< setw(1) << opTrenner
			<< setw(ilwidth) << " " << olSched
			<< setw(1) << opTrenner
			<< setw(ilwidth) << " " << olFlnoMap
			<< setw(1) << opTrenner
			<< setw(ilwidth) << " " << olPos
			<< setw(1) << opTrenner
			<< setw(ilwidth) << " " << olGat
			<< setw(1) << opTrenner
			<< setw(ilwidth) << " " << olBlt
			<< setw(1) << opTrenner
			<< setw(ilwidth) << " " << olWro
			<< endl;
	}

	of.close();

	omAutoAllocPst.RemoveAll();
	omAutoAllocGat.RemoveAll();
	omAutoAllocBlt.RemoveAll();
	omAutoAllocWro.RemoveAll();

	return true;
}

bool SpotAllocation::PrintSortedRulesPos()
{
	char pclConfigPath[256];
	char pclExcelPath[256];
	char opTrenner[64];
	if (getenv("CEDA") == NULL)
		strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
	else
		strcpy(pclConfigPath, getenv("CEDA"));

	GetPrivateProfileString(ogAppName, "Excel", "DEFAULT",
		pclExcelPath, sizeof pclExcelPath, pclConfigPath);

	GetPrivateProfileString(ogAppName, "ExcelSeparator", ";",
		opTrenner, sizeof opTrenner, pclConfigPath);

	ofstream of;

	CString olFileName = "SortedPositionRules";

	char pHeader[500];
	strcpy (pHeader, CCSLog::GetTmpPath());
	CString path = pHeader;
	olFileName =  path + "\\" + olFileName + ".csv";
	int ilwidth=1;


	of.open( olFileName, ios::out);

	of.setf(ios::left, ios::adjustfield);

	if (bgNewGoodCount)
	{
		of  << setw(ilwidth) << "Prio"
			<< setw(1) << opTrenner
			<< setw(ilwidth) << "Sequence"
			<< setw(1) << opTrenner
			<< setw(ilwidth) << "Pnam"
			<< setw(1) << opTrenner
			<< setw(ilwidth) << "Groundtime"
			<< setw(1) << opTrenner
			<< setw(ilwidth) << "Wingspan"
			<< setw(1) << opTrenner
			<< setw(ilwidth) << "Nature"
			<< setw(1) << opTrenner
			<< setw(ilwidth) << "Airline"
			<< setw(1) << opTrenner
			<< setw(ilwidth) << "ACT"
			<< setw(1) << opTrenner
			<< setw(ilwidth) << "Service"
			<< setw(1) << opTrenner
			<< setw(ilwidth) << "Flight-ID Arr"
			<< setw(1) << opTrenner
			<< setw(ilwidth) << "Flight-ID Dep"
			<< endl;
	}
	else
	{
		of  << setw(ilwidth) << "Prio"
			<< setw(1) << opTrenner
			<< setw(ilwidth) << "Sequence"
			<< setw(1) << opTrenner
			<< setw(ilwidth) << "Pnam"
			<< setw(1) << opTrenner
			<< setw(ilwidth) << "Groundtime"
			<< setw(1) << opTrenner
			<< setw(ilwidth) << "Wingspan"
			<< setw(1) << opTrenner
			<< setw(ilwidth) << "Nature"
			<< setw(1) << opTrenner
			<< setw(ilwidth) << "Airline"
			<< setw(1) << opTrenner
			<< setw(ilwidth) << "ACT"
			<< setw(1) << opTrenner
			<< setw(ilwidth) << "Service"
			<< endl;
	}

	PST_RESLIST *prlPstRes = NULL;
	for (int l=0; l<omPstRules.GetSize(); l++)
	{
		prlPstRes = &omPstRules[l];

		char clBuf[255];
		sprintf(clBuf, "%f - %f", prlPstRes->SpanMin, prlPstRes->SpanMax);
		CString olGroundtime = prlPstRes->Groundtime;
		olGroundtime.Remove(',');

		if (bgNewGoodCount)
		{
			of  << setw(ilwidth) << " " << prlPstRes->Prio
				<< setw(1) << opTrenner
				<< setw(ilwidth) << " " << prlPstRes->Sequence
				<< setw(1) << opTrenner
				<< setw(ilwidth) << " " << prlPstRes->Pnam
				<< setw(1) << opTrenner
				<< setw(ilwidth) << " " << olGroundtime
				<< setw(1) << opTrenner
				<< setw(ilwidth) << " " << clBuf
				<< setw(1) << opTrenner
				<< setw(ilwidth) << " " << prlPstRes->NAUserStr
				<< setw(1) << opTrenner
				<< setw(ilwidth) << " " << prlPstRes->ALUserStr
				<< setw(1) << opTrenner
				<< setw(ilwidth) << " " << prlPstRes->ACUserStr
				<< setw(1) << opTrenner
				<< setw(ilwidth) << " " << prlPstRes->ServiceUserStr
				<< setw(1) << opTrenner
				<< setw(ilwidth) << " " << prlPstRes->FltiArr
				<< setw(1) << opTrenner
				<< setw(ilwidth) << " " << prlPstRes->FltiDep
				<< endl;
		}
		else
		{
			of  << setw(ilwidth) << " " << prlPstRes->Prio
				<< setw(1) << opTrenner
				<< setw(ilwidth) << " " << prlPstRes->Sequence
				<< setw(1) << opTrenner
				<< setw(ilwidth) << " " << prlPstRes->Pnam
				<< setw(1) << opTrenner
				<< setw(ilwidth) << " " << olGroundtime
				<< setw(1) << opTrenner
				<< setw(ilwidth) << " " << clBuf
				<< setw(1) << opTrenner
				<< setw(ilwidth) << " " << prlPstRes->NAUserStr
				<< setw(1) << opTrenner
				<< setw(ilwidth) << " " << prlPstRes->ALUserStr
				<< setw(1) << opTrenner
				<< setw(ilwidth) << " " << prlPstRes->ACUserStr
				<< setw(1) << opTrenner
				<< setw(ilwidth) << " " << prlPstRes->ServiceUserStr
				<< endl;
		}
	}

	of.close();
	return true;
}
//############# only for test ##################################################

//############# only for test ##################################################
bool SpotAllocation::PrintSortedRulesGat()
{
	char pclConfigPath[256];
	char pclExcelPath[256];
	char opTrenner[64];
	if (getenv("CEDA") == NULL)
		strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
	else
		strcpy(pclConfigPath, getenv("CEDA"));

	GetPrivateProfileString(ogAppName, "Excel", "DEFAULT",
		pclExcelPath, sizeof pclExcelPath, pclConfigPath);

	GetPrivateProfileString(ogAppName, "ExcelSeparator", ";",
		opTrenner, sizeof opTrenner, pclConfigPath);

	ofstream of;
	CString olFileName = "SortedGateRules";

	char pHeader[500];
	strcpy (pHeader, CCSLog::GetTmpPath());
	CString path = pHeader;
	olFileName =  path + "\\" + olFileName + ".csv";
	int ilwidth=1;


	of.open( olFileName, ios::out);

	of.setf(ios::left, ios::adjustfield);

	if (bgNewGoodCount)
	{
		of  << setw(ilwidth) << "Prio"
			<< setw(1) << opTrenner
			<< setw(ilwidth) << "Sequence"
			<< setw(1) << opTrenner
			<< setw(ilwidth) << "Gnam"
			<< setw(1) << opTrenner
			<< setw(ilwidth) << "Nature"
			<< setw(1) << opTrenner
			<< setw(ilwidth) << "Airline"
			<< setw(1) << opTrenner
			<< setw(ilwidth) << "Org/Dest"
			<< setw(1) << opTrenner
			<< setw(ilwidth) << "Service"
			<< setw(1) << opTrenner
			<< setw(ilwidth) << "Flight-ID Arr"
			<< setw(1) << opTrenner
			<< setw(ilwidth) << "Flight-ID Dep"
			<< endl;
	}
	else
	{
		of  << setw(ilwidth) << "Prio"
			<< setw(1) << opTrenner
			<< setw(ilwidth) << "Sequence"
			<< setw(1) << opTrenner
			<< setw(ilwidth) << "Gnam"
			<< setw(1) << opTrenner
			<< setw(ilwidth) << "Nature"
			<< setw(1) << opTrenner
			<< setw(ilwidth) << "Airline"
			<< setw(1) << opTrenner
			<< setw(ilwidth) << "Org/Dest"
			<< setw(1) << opTrenner
			<< setw(ilwidth) << "Service"
			<< endl;
	}

	GAT_RESLIST *prlPstRes = NULL;
	for (int l=0; l<omGatRules.GetSize(); l++)
	{
		prlPstRes = &omGatRules[l];

		if (bgNewGoodCount)
		{
			of  << setw(ilwidth) << " " << prlPstRes->Prio
				<< setw(1) << opTrenner
				<< setw(ilwidth) << " " << prlPstRes->Sequence
				<< setw(1) << opTrenner
				<< setw(ilwidth) << " " << prlPstRes->Gnam
				<< setw(1) << opTrenner
				<< setw(ilwidth) << " " << prlPstRes->NAUserStr
				<< setw(1) << opTrenner
				<< setw(ilwidth) << " " << prlPstRes->ALUserStr
				<< setw(1) << opTrenner
				<< setw(ilwidth) << " " << prlPstRes->OrgDesUserStr
				<< setw(1) << opTrenner
				<< setw(ilwidth) << " " << prlPstRes->ServiceUserStr
				<< setw(1) << opTrenner
				<< setw(ilwidth) << " " << prlPstRes->Flti
				<< setw(1) << opTrenner
				<< setw(ilwidth) << " " << prlPstRes->FltiDep
				<< endl;
		}
		else
		{
			of  << setw(ilwidth) << " " << prlPstRes->Prio
				<< setw(1) << opTrenner
				<< setw(ilwidth) << " " << prlPstRes->Sequence
				<< setw(1) << opTrenner
				<< setw(ilwidth) << " " << prlPstRes->Gnam
				<< setw(1) << opTrenner
				<< setw(ilwidth) << " " << prlPstRes->NAUserStr
				<< setw(1) << opTrenner
				<< setw(ilwidth) << " " << prlPstRes->ALUserStr
				<< setw(1) << opTrenner
				<< setw(ilwidth) << " " << prlPstRes->OrgDesUserStr
				<< setw(1) << opTrenner
				<< setw(ilwidth) << " " << prlPstRes->ServiceUserStr
				<< endl;
		}
	}




	of.close();
	return true;
}
//############# only for test ##################################################

//############# only for test ##################################################
bool SpotAllocation::PrintSortedRulesBlt()
{
	char pclConfigPath[256];
	char pclExcelPath[256];
	char opTrenner[64];
	if (getenv("CEDA") == NULL)
		strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
	else
		strcpy(pclConfigPath, getenv("CEDA"));

	GetPrivateProfileString(ogAppName, "Excel", "DEFAULT",
		pclExcelPath, sizeof pclExcelPath, pclConfigPath);

	GetPrivateProfileString(ogAppName, "ExcelSeparator", ";",
		opTrenner, sizeof opTrenner, pclConfigPath);

	ofstream of;
	CString olFileName = "SortedBeltRules";

	char pHeader[500];
	strcpy (pHeader, CCSLog::GetTmpPath());
	CString path = pHeader;
	olFileName =  path + "\\" + olFileName + ".csv";
	int ilwidth=1;


	of.open( olFileName, ios::out);

	of.setf(ios::left, ios::adjustfield);
	of  << setw(ilwidth) << "Prio"
		<< setw(1) << opTrenner
		<< setw(ilwidth) << "Sequence"
		<< setw(1) << opTrenner
		<< setw(ilwidth) << "Bnam"
		<< setw(1) << opTrenner
		<< setw(ilwidth) << "Pax"
		<< setw(1) << opTrenner
		<< setw(ilwidth) << "Airline"
		<< setw(1) << opTrenner
		<< setw(ilwidth) << "Origin"
		<< setw(1) << opTrenner
		<< setw(ilwidth) << "Nature"
		<< setw(1) << opTrenner
		<< setw(ilwidth) << "Service"
		<< endl;

	BLT_RESLIST *prlPstRes = NULL;
	for (int l=0; l<omBltRules.GetSize(); l++)
	{
		prlPstRes = &omBltRules[l];

		of  << setw(ilwidth) << " " << prlPstRes->Prio
			<< setw(1) << opTrenner
			<< setw(ilwidth) << " " << prlPstRes->Sequence
			<< setw(1) << opTrenner
			<< setw(ilwidth) << " " << prlPstRes->Bnam
			<< setw(1) << opTrenner
			<< setw(ilwidth) << " " << prlPstRes->Maxp
			<< setw(1) << opTrenner
			<< setw(ilwidth) << " " << prlPstRes->ALUserStr
			<< setw(1) << opTrenner
			<< setw(ilwidth) << " " << prlPstRes->OrgDesUserStr
			<< setw(1) << opTrenner
			<< setw(ilwidth) << " " << prlPstRes->NAUserStr
			<< setw(1) << opTrenner
			<< setw(ilwidth) << " " << prlPstRes->ServiceUserStr
			<< endl;
	}




	of.close();
	return true;
}
//############# only for test ##################################################


bool SpotAllocation::CheckPosByGate(const CString& ropRecomPos, DIAFLIGHTDATA *prpFlight)
{
	if (!prpFlight)
		return true;


	if(prpFlight->Ftyp[0] == 'T' || prpFlight->Ftyp[0] == 'G')	
		return true;

	CStringArray ropBLUE;
	CMapStringToString ropMapBLUE;
	CStringArray ropRED;
	CMapStringToString ropMapRED;

	CString olGat;
	if (prpFlight->Adid[0] == 'A')
		olGat = CString(prpFlight->Gta1);
	else if (prpFlight->Adid[0] == 'D')
		olGat = CString(prpFlight->Gtd1);
	else
		return false;

	if (olGat.IsEmpty())
		return true;

	if (GetResBy("PST", "GAT", olGat,ropBLUE,ropMapBLUE,ropRED,ropMapRED, prpFlight, true))
	{
		CString olTmp;
		ropMapBLUE.Lookup(ropRecomPos, olTmp);
		if (olTmp == "IDB_LIGHTBLUE")
			return true;
	}

	return false;
}

bool SpotAllocation::AutoPstAllocateFlightsGatPos(DIAFLIGHTDATA *prlFlightA, DIAFLIGHTDATA *prlFlightD, const CString &ropRecomPos, CString& ropAllocatedPos, ofstream& of)
{
	// TSC 0909
	CCSPtrArray<PST_RESLIST> olPossiblePstUser;
	bool bpstuser = 1 ; // NORMAL
	
	// END TSC

	PST_RESLIST *prlPstRes = NULL;
	ropAllocatedPos = "";

	// At first: try to allocate the recommended position
	if (!ropRecomPos.IsEmpty())
	{
		DiaCedaFlightData::RKEYLIST *prlRkey = NULL;
		if (prlFlightA)
			prlRkey = ogPosDiaFlightData.GetRotationByRkey(prlFlightA->Rkey);
		else if (prlFlightD)
			prlRkey = ogPosDiaFlightData.GetRotationByRkey(prlFlightD->Rkey);

		DIAFLIGHTDATA *prlPosDiaFlightA = ogPosDiaFlightData.GetFlightAInRotation(prlRkey);
		DIAFLIGHTDATA *prlPosDiaFlightD = ogPosDiaFlightData.GetFlightDInRotation(prlRkey);


		if(CheckPst(prlPosDiaFlightA, prlPosDiaFlightD, ropRecomPos, NULL, &omParameters.omPstRightBuffer, &omParameters.omPstLeftBuffer, true))
//		if(CheckPst(prlFlightA, prlFlightD, ropRecomPos, NULL, &omParameters.omPstRightBuffer, &omParameters.omPstLeftBuffer, true))
		{
			if (CheckPosByGate(ropRecomPos, prlFlightA) && CheckPosByGate(ropRecomPos, prlFlightD))
			{
				AllocatePst(prlFlightA, prlFlightD, ropRecomPos);
				ropAllocatedPos = ropRecomPos;
				return true;
			}
		}
	}

	// then collect all possible positions 
	omPstRules.Sort(ComparePstResByPrio);
	CPtrArray polPossiblePstRules;
 	for(int i = 0; i < omPstRules.GetSize(); i++)
	{
		prlPstRes = &omPstRules[i];

		if(CheckPst(prlFlightA, prlFlightD, prlPstRes->Pnam, NULL, &omParameters.omPstRightBuffer, &omParameters.omPstLeftBuffer, true))
		{
			if (CheckPosByGate(prlPstRes->Pnam, prlFlightA) && CheckPosByGate(prlPstRes->Pnam, prlFlightD))
			{
				if(CheckWarningAllocationBufferTime(prlFlightA, prlFlightD, prlPstRes->Pnam, NULL, &omParameters.omPstRightBuffer, &omParameters.omPstLeftBuffer, true))
					polPossiblePstRules.Add(prlPstRes);
			}
		}
	}		

	if (polPossiblePstRules.GetSize() < 1) 
		return false;

//TSC 0909 PST

	if(oguseralloctepath!="<Default>" & oguseralloctepath!="")
	{
    		    Userallocatevector(oguseralloctepath);
	              
				std::vector <CString> vDataPST;

					bool accesstab   = 1;
															
				for (int i =0; i< vData.size(); i++)
				{
						
							// ONLY PST
								CString cstrdata=vData[i];
								CString strf="END_PST\n";
								if(strf==cstrdata)accesstab = 0;
																
								if(accesstab)
								{
						        // THE NEXT DS
								
						std::string checkedtrue = LPCTSTR(cstrdata);
						
						int r = checkedtrue.find(';');
						
						std::string strgaturno = checkedtrue.substr(0,r);
																// FOR ;          					
						std::string checked = checkedtrue.substr(r+1,checkedtrue.length());
						
						int z= 23;
									if(checked=="1\n")
									{
									vDataPST.push_back(strgaturno.c_str());
									}
								

							    }//accesstab 

								
				}//	for (int i =0; i< vData.size(); i++)


	int counter = vDataPST.size();


	PST_RESLIST *prlPstResUser = NULL;
    
	for( int m= 0; m < polPossiblePstRules.GetSize(); m++)
	{

         prlPstResUser = (PST_RESLIST*) polPossiblePstRules[m];
 	           
         PST_RESLIST *prlPstResActl= new (PST_RESLIST);

	  

		
					if (!prlPstResUser->Urno.IsEmpty())
					{
				
						for( int j = 0 ; j < vDataPST.size() ; j++)
						{
								
							if(prlPstResUser->Urno == vDataPST[j])
							{

                            prlPstResActl->Urno     = prlPstResUser->Urno;
							prlPstResActl->Pnam     = prlPstResUser->Pnam;
							prlPstResActl->Prio     = counter-j;
							prlPstResActl->Sequence = 0;  

							olPossiblePstUser.Add(prlPstResActl);
							
							}//if(prlPstResActl->Urno == vDataPST[j])

						}//for( j = 0 ; j < vDataPST.size() ; j++)
	
					}//(prlPstResActl != NULL)

	}//for( int m= 0; m < polPossiblePstRules; m++)


	if (olPossiblePstUser.GetSize() < 1) 
		return false;
    else
		bpstuser = 0;

	olPossiblePstUser.Sort(ComparePstResByPrio);

	vDataPST.clear();
}
// END TSC


	// get the best possible position
	
	// TSC
		PST_RESLIST *prlBestPstRule = NULL;
	if(bpstuser)
		prlBestPstRule = (PST_RESLIST *) polPossiblePstRules[0];
    else
		{
	      prlBestPstRule = &olPossiblePstUser[0];
	      prlBestPstRule = GetPstRes(prlBestPstRule->Pnam);
		}
	//END TSC

	float flBestGoodCount = 0.0;
	int ilBestBadCount = 0;
	CString olMatchStr;

	if(bgAutoAllocWithScore)
		GetMatchCountPst(prlFlightA, prlFlightD, prlBestPstRule, prlBestPstRule->PreferredAL, prlBestPstRule->PreferredAC, prlBestPstRule->PreferredNA, prlBestPstRule->PreferredService, prlBestPstRule->PreferredOrgDes, prlBestPstRule->PreferredFlightNo,  flBestGoodCount, ilBestBadCount);
	else
		GetMatchCountPst(prlFlightA, prlFlightD, prlBestPstRule, flBestGoodCount, ilBestBadCount, olMatchStr);  



//############# only for test ##################################################
	char opTrenner[64];
	int ilwidth = 1;
	char clBuf[255];
	CString ol0;
	CString ol1;
	CString ol2;
	CString ol3;
	CString ol4;
	CStringArray olArray;
	CString olGroundtime;
	CString olFlno;

	if (bgCreateAutoAllocFiles)
	{
		strcpy(opTrenner,";");

		if (prlFlightA)
			GetFlnoForPrintMap(prlFlightA, olFlno);
		else if (prlFlightD)
			GetFlnoForPrintMap(prlFlightD, olFlno);

		sprintf(clBuf, "%f", flBestGoodCount);

		ExtractItemList(olMatchStr, &olArray, ';');
		if (olArray.GetSize() == 4)
		{
			ol1 = olArray.GetAt(0);
			ol2 = olArray.GetAt(1);
			ol3 = olArray.GetAt(2);
			ol4 = olArray.GetAt(3);
		}
		else if (olArray.GetSize() == 5 && bgNewGoodCount)
		{
			ol1 = olArray.GetAt(0);
			ol2 = olArray.GetAt(1);
			ol3 = olArray.GetAt(2);
			ol4 = olArray.GetAt(3);
			ol0 = olArray.GetAt(4);
		}

		olGroundtime = prlBestPstRule->Groundtime;
		olGroundtime.Remove(',');

		of.setf(ios::left, ios::adjustfield);
		of  << "-----------------------------------------------------------------"
			<< endl;
		of  << olFlno
			<< endl;
		of  << "-----------------------------------------------------------------"
			<< endl;

		if (!bgNewGoodCount) 
		{
			of  << setw(ilwidth) << " " << prlBestPstRule->Prio
				<< setw(1) << opTrenner
				<< setw(ilwidth) << " " << prlBestPstRule->Sequence
				<< setw(1) << opTrenner
				<< setw(ilwidth) << " " << prlBestPstRule->Pnam
				<< setw(1) << opTrenner
				<< setw(ilwidth) << " " << olGroundtime
				<< setw(1) << opTrenner
				<< setw(ilwidth) << clBuf
				<< setw(1) << opTrenner
				<< setw(ilwidth) << " " << ilBestBadCount
				<< setw(1) << opTrenner
				<< setw(ilwidth) << " " << ol1
				<< setw(1) << opTrenner
				<< setw(ilwidth) << " " << ol2
				<< setw(1) << opTrenner
				<< setw(ilwidth) << " " << ol3
				<< setw(1) << opTrenner
				<< setw(ilwidth) << " " << ol4
				<< endl;
		}
		else
		{
			of  << setw(ilwidth) << " " << prlBestPstRule->Prio
				<< setw(1) << opTrenner
				<< setw(ilwidth) << " " << prlBestPstRule->Sequence
				<< setw(1) << opTrenner
				<< setw(ilwidth) << " " << prlBestPstRule->Pnam
				<< setw(1) << opTrenner
				<< setw(ilwidth) << " " << olGroundtime
				<< setw(1) << opTrenner
				<< setw(ilwidth) << clBuf
				<< setw(1) << opTrenner
				<< setw(ilwidth) << " " << ilBestBadCount
				<< setw(1) << opTrenner
				<< setw(ilwidth) << " " << ol0
				<< setw(1) << opTrenner
				<< setw(ilwidth) << " " << ol1
				<< setw(1) << opTrenner
				<< setw(ilwidth) << " " << ol2
				<< setw(1) << opTrenner
				<< setw(ilwidth) << " " << ol3
				<< setw(1) << opTrenner
				<< setw(ilwidth) << " " << ol4
				<< endl;
		}
	}
//############# only for test ##################################################


	// calculate the best rule
	if (!bgAutoAllocFirstRule)
	{
        // TSC
		int inumberrules = 0;
        if( bpstuser )
			inumberrules = polPossiblePstRules.GetSize();
		else
			inumberrules = olPossiblePstUser.GetSize();
		// END TSC

		for (i = 1; i < inumberrules; i++) 
		{
			float flCurrGoodCount = 0.0;
			int ilCurrBadCount = 0;
			// TSC
			if( bpstuser )
					prlPstRes = (PST_RESLIST *) polPossiblePstRules[i];
			else
			{
                    prlPstRes = &olPossiblePstUser[i];
				    prlPstRes = GetPstRes(prlPstRes->Pnam);
			}
			// END TSC

			if(bgAutoAllocWithScore)
				GetMatchCountPst(prlFlightA, prlFlightD, prlPstRes,
				prlPstRes->PreferredAL, prlPstRes->PreferredAC, prlPstRes->PreferredNA, 
				prlPstRes->PreferredService, prlPstRes->PreferredOrgDes, prlPstRes->PreferredFlightNo, flCurrGoodCount, ilCurrBadCount);
			else
				GetMatchCountPst(prlFlightA, prlFlightD, prlPstRes, flCurrGoodCount, ilCurrBadCount, olMatchStr);



			if ((ilCurrBadCount < ilBestBadCount) ||
				(ilCurrBadCount == ilBestBadCount && flCurrGoodCount > flBestGoodCount))
			{
				ilBestBadCount = ilCurrBadCount;
				flBestGoodCount = flCurrGoodCount;
				prlBestPstRule = prlPstRes;
			}

	//############# only for test ##################################################
			if (bgCreateAutoAllocFiles)
			{
				sprintf(clBuf, "%f", flCurrGoodCount);

				ExtractItemList(olMatchStr, &olArray, ';');
				if (olArray.GetSize() == 4)
				{
					ol1 = olArray.GetAt(0);
					ol2 = olArray.GetAt(1);
					ol3 = olArray.GetAt(2);
					ol4 = olArray.GetAt(3);
				}
				else if (olArray.GetSize() == 5 && bgNewGoodCount)
				{
					ol1 = olArray.GetAt(0);
					ol2 = olArray.GetAt(1);
					ol3 = olArray.GetAt(2);
					ol4 = olArray.GetAt(3);
					ol0 = olArray.GetAt(4);
				}

				olGroundtime = prlPstRes->Groundtime;
				olGroundtime.Remove(',');

				if (!bgNewGoodCount)
				{
					of  << setw(ilwidth) << " " << prlPstRes->Prio
						<< setw(1) << opTrenner
						<< setw(ilwidth) << " " << prlPstRes->Sequence
						<< setw(1) << opTrenner
						<< setw(ilwidth) << " " << prlPstRes->Pnam
						<< setw(1) << opTrenner
						<< setw(ilwidth) << " " << olGroundtime
						<< setw(1) << opTrenner
						<< setw(ilwidth) << clBuf
						<< setw(1) << opTrenner
						<< setw(ilwidth) << " " << ilCurrBadCount
						<< setw(1) << opTrenner
						<< setw(ilwidth) << " " << ol1
						<< setw(1) << opTrenner
						<< setw(ilwidth) << " " << ol2
						<< setw(1) << opTrenner
						<< setw(ilwidth) << " " << ol3
						<< setw(1) << opTrenner
						<< setw(ilwidth) << " " << ol4
						<< endl;
				}
				else
				{
					of  << setw(ilwidth) << " " << prlPstRes->Prio
						<< setw(1) << opTrenner
						<< setw(ilwidth) << " " << prlPstRes->Sequence
						<< setw(1) << opTrenner
						<< setw(ilwidth) << " " << prlPstRes->Pnam
						<< setw(1) << opTrenner
						<< setw(ilwidth) << " " << olGroundtime
						<< setw(1) << opTrenner
						<< setw(ilwidth) << clBuf
						<< setw(1) << opTrenner
						<< setw(ilwidth) << " " << ilCurrBadCount
						<< setw(1) << opTrenner
						<< setw(ilwidth) << " " << ol0
						<< setw(1) << opTrenner
						<< setw(ilwidth) << " " << ol1
						<< setw(1) << opTrenner
						<< setw(ilwidth) << " " << ol2
						<< setw(1) << opTrenner
						<< setw(ilwidth) << " " << ol3
						<< setw(1) << opTrenner
						<< setw(ilwidth) << " " << ol4
						<< endl;
				}
			}
	//############# only for test ##################################################


		}
	}

	// assign the best position to the flight(s)
	if (prlBestPstRule != NULL)
	{
		AllocatePst(prlFlightA, prlFlightD, prlBestPstRule->Pnam);
		ropAllocatedPos = prlBestPstRule->Pnam;
		omAutoAllocPst.SetAt(olFlno,ropAllocatedPos);

//############# only for test ##################################################
		if (bgCreateAutoAllocFiles)
		{
			of  << endl
				<< "Allocated Position : !!!!  " << prlBestPstRule->Pnam << "  !!!!"
				<< endl;
		}
//############# only for test ##################################################
      
		// TSC
		olPossiblePstUser.DeleteAll();

		return true;
	} 

	// TSC
	olPossiblePstUser.DeleteAll();

	return false;
}
/*
bool SpotAllocation::AutoPstAllocateFlightsGatPos(DIAFLIGHTDATA *prlFlightA, DIAFLIGHTDATA *prlFlightD, const CString &ropRecomPos)
{
	
	PST_RESLIST *prlPstRes = NULL;

	// At first: try to allocate the recommended position
	if (!ropRecomPos.IsEmpty())
	{
		if(CheckPst(prlFlightA, prlFlightD, ropRecomPos, NULL, &omParameters.omPstRightBuffer, &omParameters.omPstLeftBuffer))
		{
			if (CheckPosByGate(ropRecomPos, prlFlightA) && CheckPosByGate(ropRecomPos, prlFlightD))
			{
				AllocatePst(prlFlightA, prlFlightD, ropRecomPos);
				return true;
			}
		}
	}

	// then collect all possible positions 
	omPstRules.Sort(ComparePstResByPrio);
	CPtrArray polPossiblePstRules;
 	for(int i = 0; i < omPstRules.GetSize(); i++)
	{
		prlPstRes = &omPstRules[i];

		if(CheckPst(prlFlightA, prlFlightD, prlPstRes->Pnam, NULL, &omParameters.omPstRightBuffer, &omParameters.omPstLeftBuffer))
		{
			if (CheckPosByGate(prlPstRes->Pnam, prlFlightA) && CheckPosByGate(prlPstRes->Pnam, prlFlightD))
				polPossiblePstRules.Add(prlPstRes);
		}
	}		

	if (polPossiblePstRules.GetSize() < 1) 
		return false;

	// get the best possible position
	PST_RESLIST *prlBestPstRule = (PST_RESLIST *) polPossiblePstRules[0];
	float flBestGoodCount = 0.0;
	int ilBestBadCount = 0;
	GetMatchCountPst(prlFlightA, prlFlightD, prlBestPstRule->PreferredAL, prlBestPstRule->PreferredAC, prlBestPstRule->PreferredNA, flBestGoodCount, ilBestBadCount);  

//	float flCurrGoodCount;
//	int ilCurrBadCount;
	for (i = 1; i < polPossiblePstRules.GetSize(); i++)
	{
		float flCurrGoodCount = 0.0;
		int ilCurrBadCount = 0;
		prlPstRes = (PST_RESLIST *) polPossiblePstRules[i];
		GetMatchCountPst(prlFlightA, prlFlightD, prlPstRes->PreferredAL, prlPstRes->PreferredAC, prlPstRes->PreferredNA, flCurrGoodCount, ilCurrBadCount);
		if ((ilCurrBadCount < ilBestBadCount) ||
			(ilCurrBadCount == ilBestBadCount && flCurrGoodCount > flBestGoodCount))
		{
			ilBestBadCount = ilCurrBadCount;
			flBestGoodCount = flCurrGoodCount;
			prlBestPstRule = prlPstRes;
		}
	}

	// assign the best position to the flight(s)
	if (prlBestPstRule != NULL)
	{
		AllocatePst(prlFlightA, prlFlightD, prlBestPstRule->Pnam);
		return true;
	} 

	return false;
}
*/

bool SpotAllocation::AutoPstAllocateFlights(DIAFLIGHTDATA *prlFlightA, DIAFLIGHTDATA *prlFlightD, const CString &ropRecomPos)
{
	
	PST_RESLIST *prlPstRes = NULL;

	// At first: try to allocate the recommended position
	if (!ropRecomPos.IsEmpty())
	{
		if(CheckPst(prlFlightA, prlFlightD, ropRecomPos, NULL, &omParameters.omPstRightBuffer, &omParameters.omPstLeftBuffer))
		{
			AllocatePst(prlFlightA, prlFlightD, ropRecomPos);
			return true;
		}
	}

	// then collect all possible positions 
	CPtrArray polPossiblePstRules;
 	for(int i = 0; i < omPstRules.GetSize(); i++)
	{
		prlPstRes = &omPstRules[i];

		if(CheckPst(prlFlightA, prlFlightD, prlPstRes->Pnam, NULL, &omParameters.omPstRightBuffer, &omParameters.omPstLeftBuffer))
		{
			polPossiblePstRules.Add(prlPstRes);
		}
	}		

	if (polPossiblePstRules.GetSize() < 1) 
		return false;

	// get the best possible position
	PST_RESLIST *prlBestPstRule = (PST_RESLIST *) polPossiblePstRules[0];
	float flBestGoodCount;
	int ilBestBadCount;
	GetMatchCountPst(prlFlightA, prlFlightD, prlBestPstRule, prlBestPstRule->PreferredAL, prlBestPstRule->PreferredAC, prlBestPstRule->PreferredNA, prlBestPstRule->PreferredService, prlBestPstRule->PreferredOrgDes, prlBestPstRule->PreferredFlightNo, flBestGoodCount, ilBestBadCount);  

	float flCurrGoodCount;
	int ilCurrBadCount;
	for (i = 1; i < polPossiblePstRules.GetSize(); i++)
	{
		prlPstRes = (PST_RESLIST *) polPossiblePstRules[i];
		GetMatchCountPst(prlFlightA, prlFlightD, prlPstRes, prlPstRes->PreferredAL, prlPstRes->PreferredAC, prlPstRes->PreferredNA, prlBestPstRule->PreferredService, prlBestPstRule->PreferredOrgDes, prlBestPstRule->PreferredFlightNo, flCurrGoodCount, ilCurrBadCount);
		if ((ilCurrBadCount < ilBestBadCount) ||
			(ilCurrBadCount == ilBestBadCount && flCurrGoodCount > flBestGoodCount))
		{
			ilBestBadCount = ilCurrBadCount;
			flBestGoodCount = flCurrGoodCount;
			prlBestPstRule = prlPstRes;
		}
	}

	// assign the best position to the flight(s)
	if (prlBestPstRule != NULL)
	{
		AllocatePst(prlFlightA, prlFlightD, prlBestPstRule->Pnam);
		return true;
	} 

	return false;
}
/*
bool SpotAllocation::GetMatchCountPst(const DIAFLIGHTDATA *prpFlightA, const DIAFLIGHTDATA *prpFlightD, PST_RESLIST *prpPstRule,
									  float &rfpGoodCount, int &ripBadCount, CString& opMatchStr) const
{
	CString olNat;
	CString olAlc;
	CString olAct;
	CString olService;
	CString olNotIn = "NotIn";
	char clBuf[255];

	rfpGoodCount = 0;
	ripBadCount = 0;

	if (prpFlightA == NULL && prpFlightD == NULL)
		return false;

	CStringArray ropPreferredAL;
	CStringArray ropPreferredAC;
	CStringArray ropPreferredNA;
	CStringArray ropPreferredService;

	ropPreferredAL.Append(prpPstRule->PreferredAL);
	ropPreferredAL.Append(prpPstRule->AssignedAL);
	ropPreferredAC.Append(prpPstRule->PreferredAC);
	ropPreferredAC.Append(prpPstRule->AssignedAC);
	ropPreferredNA.Append(prpPstRule->PreferredNA);
	ropPreferredNA.Append(prpPstRule->AssignedNA);
	ropPreferredService.Append(prpPstRule->PreferredService);
	ropPreferredService.Append(prpPstRule->AssignedService);

	int ilPos = -1;
	const DIAFLIGHTDATA *prlFlight = NULL;
	
	for (int i = 0; i < 2; i++)
	{
		// check arrival and departure flight
		if (i == 0)
			prlFlight = prpFlightA;
		else
			prlFlight = prpFlightD;

		if (prlFlight != NULL)
		{		
			// check preferred nature codes
			ilPos = FindIdInStrArray(ropPreferredNA, CString(prlFlight->Ttyp)); 
			if (ilPos > -1)
			{
				ilPos++;
				rfpGoodCount += 1/(float)ilPos;
				sprintf(clBuf, "%f", 1/(float)ilPos);
				olNat += "  " + CString(prlFlight->Ttyp) + "-" + CString(clBuf);
			}
			else if (ropPreferredNA.GetSize() > 0)
			{
				ripBadCount++;
				olNat += "  " + CString(prlFlight->Ttyp) + "-" + olNotIn;
			}

			// check preferred service codes
			ilPos = FindIdInStrArray(ropPreferredService, CString(prlFlight->Styp)); 
			if (ilPos > -1)
			{
				ilPos++;
				rfpGoodCount += 1/(float)ilPos;
				sprintf(clBuf, "%f", 1/(float)ilPos);
				olService += "  " + CString(prlFlight->Styp) + "-" + CString(clBuf);
			}
			else if (ropPreferredService.GetSize() > 0)
			{
				ripBadCount++;
				olService += "  " + CString(prlFlight->Styp) + "-" + olNotIn;
			}

			// check preferred airline codes			
			if ((ilPos = FindIdInStrArray(ropPreferredAL, CString(prlFlight->Alc2))) == -1)
				ilPos = FindIdInStrArray(ropPreferredAL, CString(prlFlight->Alc3)); 
			if (ilPos > -1)
			{
				ilPos++;
				rfpGoodCount += 1/(float)ilPos;
				sprintf(clBuf, "%f", 1/(float)ilPos);
				olAlc += "  " + CString(prlFlight->Alc2) + "/" + CString(prlFlight->Alc3) + "-" + CString(clBuf);
			}
			else if (ropPreferredAL.GetSize() > 0)
			{
				ripBadCount++;
				olAlc += "  " + CString(prlFlight->Alc2) + "/" + CString(prlFlight->Alc3) + "-" + olNotIn;
			}

			// check preferred aircraft type
			if ((ilPos = FindIdInStrArray(ropPreferredAC, CString(prlFlight->Act3))) == -1)
				ilPos = FindIdInStrArray(ropPreferredAC, CString(prlFlight->Act5)); 
			if (ilPos > -1)
			{
				ilPos++;
				rfpGoodCount += 1/(float)ilPos;
				sprintf(clBuf, "%f", 1/(float)ilPos);
				olAct += "  " + CString(prlFlight->Act3) + "/" + CString(prlFlight->Act5) + "-" + CString(clBuf);
			}
			else if (ropPreferredAC.GetSize() > 0)
			{
				ripBadCount++;
				olAct += "  " + CString(prlFlight->Act3) + "/" + CString(prlFlight->Act5) + "-" + olNotIn;
			}
		}
	}

	opMatchStr = olNat + ";" + olAlc + ";" + olAct + ";" + olService;

	ropPreferredAL.RemoveAll();
	ropPreferredAC.RemoveAll();
	ropPreferredNA.RemoveAll();
	ropPreferredService.RemoveAll();

	return true;
}
*/
bool SpotAllocation::GetMatchCountPst(const DIAFLIGHTDATA *prpFlightA, const DIAFLIGHTDATA *prpFlightD, PST_RESLIST *prpPstRule, const CStringArray &ropPreferredAL, const CStringArray &ropPreferredAC, const CStringArray &ropPreferredNA,	const CStringArray &ropPreferredService, const CStringArray &ropPreferredDest,const CStringArray &ropPreferredFlightNo, 
									  float &rfpGoodCount, int &ripBadCount) const
{

	CString olOrgDes3;
	CString olOrgDes4;
	CString olVia3;
	CString olVia4;
	
	int ilRulePrio = 1;

	if(prpPstRule != NULL)
		ilRulePrio = prpPstRule->Prio;

	if(prpFlightA != NULL /*&& CString(prpFlightA->Flno) == "EY 918"*/ && (prpPstRule->Pnam == "103	" || prpPstRule->Pnam == "303"))
		int ili= 0;

	
	rfpGoodCount = 0;
	ripBadCount = 0;

	if (prpFlightA == NULL && prpFlightD == NULL)
		return false;


	int ilPos;
	const DIAFLIGHTDATA *prlFlight = NULL;
	
	for (int i = 0; i < 2; i++)
	{
		// check arrival and departure flight
		if (i == 0)
			prlFlight = prpFlightA;
		else
			prlFlight = prpFlightD;

		if (prlFlight != NULL)
		{		



			if(i == 1)			
			{

				olOrgDes3 = prlFlight->Des3;
				olOrgDes4 = prlFlight->Des4;

				if( atoi(prlFlight->Vian) > 0)
				{
					CString olVia = CString(prlFlight->Vial); 

					olVia3 = olVia.Mid(1,3);
					olVia4 = olVia.Mid(4,4);
				}
				

				if (ilPos = FindIdInStrArray(ropPreferredDest, olVia3 + CString(":") + olOrgDes3) == -1)
				{
					if (ilPos = FindIdInStrArray(ropPreferredDest, olVia4 + CString(":") + olOrgDes4) == -1)
					{

						if (ilPos = FindIdInStrArray(ropPreferredDest, olOrgDes3) == -1) 
							ilPos = FindIdInStrArray(ropPreferredDest, olOrgDes4);
					}
				}
			
				if (ilPos > -1)
				{
					ilPos++;
					rfpGoodCount +=  1000 ;
				}

			}

			// check preferred nature codes
			ilPos = FindIdInStrArray(ropPreferredNA, CString(prlFlight->Ttyp)); 
			if (ilPos > -1)
			{
				ilPos++;
				rfpGoodCount += 1/(float)ilPos;
			}
			else if (ropPreferredNA.GetSize() > 0)
			{
				ripBadCount++;
			}


			ilPos = FindIdInStrArray(ropPreferredFlightNo, CString(prlFlight->Flno)); 
			if (ilPos > -1)
			{
				ilPos++;
				//rfpGoodCount += (1/(float)ilPos) * 40;
				rfpGoodCount += 10000000;
			}



			if(i == 0)
			{
				if ((ilPos = FindIdInStrArray(ropPreferredAL, CString(prlFlight->Alc2))) == -1)
					ilPos = FindIdInStrArray(ropPreferredAL, CString(prlFlight->Alc3)); 
			}
			else
			{
				if ((ilPos = FindIdInStrArray(ropPreferredAL, CString(prlFlight->Alc2) + CString(":") + CString(prlFlight->Des3))) == -1)
				{
					if ((ilPos = FindIdInStrArray(ropPreferredAL, CString(prlFlight->Alc2) + CString(":") + CString(prlFlight->Des4))) == -1)
					{
						if ((ilPos = FindIdInStrArray(ropPreferredAL, CString(prlFlight->Alc3) + CString(":") + CString(prlFlight->Des3))) == -1)
						{
							if ((ilPos = FindIdInStrArray(ropPreferredAL, CString(prlFlight->Alc3) + CString(":") + CString(prlFlight->Des4))) == -1)
							{
								if ((ilPos = FindIdInStrArray(ropPreferredAL, CString(prlFlight->Alc2))) == -1)
									ilPos = FindIdInStrArray(ropPreferredAL, CString(prlFlight->Alc3)); 
							}
						}
					
					}
				}
			}



			if (ilPos > -1)
			{
				ilPos++;
				rfpGoodCount +=  500000 * ilRulePrio + 500000 / ilPos;

				//rfpGoodCount += (1/(float)ilPos) * 3;
				//rfpGoodCount += (1/((float)ilPos * 3)) * ilRulePrio *100;
			}

			if ((ilPos = FindIdInStrArray(ropPreferredAC, CString(prlFlight->Act3))) == -1)
				ilPos = FindIdInStrArray(ropPreferredAC, CString(prlFlight->Act5)); 
			if (ilPos > -1)
			{
				ilPos++;
//				rfpGoodCount += 1/(float)ilPos;
//				rfpGoodCount += (1/(float)ilPos) * ilRulePrio;
				rfpGoodCount +=  1000 ;
			}

			ilPos = FindIdInStrArray(ropPreferredService, CString(prlFlight->Styp)); 
			if (ilPos > -1)
			{
				ilPos++;
				//rfpGoodCount += (1/(float)ilPos) * 40;
				rfpGoodCount +=  400000 ;
			}

			if(omParameters.bmPremPax)
			{
				int ilFBPax = atoi(prlFlight->Paxf) + atoi(prlFlight->Paxb);

				if(ilFBPax > 0)
				{

					rfpGoodCount +=  3001 * ilFBPax;

				}
			}

			if(omParameters.bmTotalPax)
			{
				//int ilFBPax = atoi(prlFlight->Paxf) + atoi(prlFlight->Paxb) + atoi(prlFlight->Paxe);
				int ilTotalPax = atoi(prlFlight->Paxt);

				if(ilTotalPax > 0)
				{

					rfpGoodCount +=  3001 * (ilTotalPax / 5);

				}
			}


		}
	}

	//rfpGoodCount += ilRulePrio;
	return true;
}

bool SpotAllocation::GetMatchCountPst(const DIAFLIGHTDATA *prpFlightA, const DIAFLIGHTDATA *prpFlightD, PST_RESLIST *prpPstRule,
									  float &rfpGoodCount, int &ripBadCount, CString& opMatchStr) const
{
	CString olNat;
	CString olDest;
	CString olAlc;
	CString olAct;
	CString olService;
	CString olNotIn = "NotIn";
	char clBuf[255];
	char clBufPrio[255];

	rfpGoodCount = 0;
	ripBadCount = 0;

	if (prpFlightA == NULL && prpFlightD == NULL)
		return false;

	CStringArray ropPreferredAL;
	CStringArray ropPreferredAC;
	CStringArray ropPreferredNA;
	CStringArray ropPreferredDest;
	CStringArray ropPreferredService;

	CString olOrgDes3;
	CString olOrgDes4;
	CString olVia3;
	CString olVia4;

	ropPreferredAL.Append(prpPstRule->PreferredAL);
	ropPreferredAL.Append(prpPstRule->AssignedAL);
	ropPreferredAC.Append(prpPstRule->PreferredAC);
	ropPreferredAC.Append(prpPstRule->AssignedAC);
	ropPreferredNA.Append(prpPstRule->PreferredNA);
	ropPreferredNA.Append(prpPstRule->AssignedNA);
	ropPreferredDest.Append(prpPstRule->PreferredOrgDes);
	ropPreferredDest.Append(prpPstRule->AssignedOrgDes);
	ropPreferredService.Append(prpPstRule->PreferredService);
	ropPreferredService.Append(prpPstRule->AssignedService);
	CString olFlti("");

	int ilPos = -1;
	const DIAFLIGHTDATA *prlFlight = NULL;

	double dlNewGoodCount = 0.0;
	if(bgNewGoodCount)
	{
		if (prpPstRule->Sequence > 0)
			dlNewGoodCount = (dgFactorPrio*prpPstRule->Prio + (dgFactorSequ/prpPstRule->Sequence)) * dgFactorGoodCount;
		else
			dlNewGoodCount = (dgFactorPrio*prpPstRule->Prio + 0.0) * dgFactorGoodCount;

		sprintf(clBufPrio, "%f", (float)dlNewGoodCount);

	}
	
	for (int i = 0; i < 2; i++)
	{
		// check arrival and departure flight
		if (i == 0)
		{
			prlFlight = prpFlightA;
			olFlti = prpPstRule->FltiArr;
		}
		else
		{
			prlFlight = prpFlightD;
			olFlti = prpPstRule->FltiDep;
		}

		if (prlFlight != NULL)
		{		



			// check preferred destination
			if(i == 1)			
			{

				olOrgDes3 = prlFlight->Des3;
				olOrgDes4 = prlFlight->Des4;

				if( atoi(prlFlight->Vian) > 0)
				{
					CString olVia = CString(prlFlight->Vial); 

					olVia3 = olVia.Mid(1,3);
					olVia4 = olVia.Mid(4,4);
				}
				

				if (ilPos = FindIdInStrArray(ropPreferredDest, olVia3 + CString(":") + olOrgDes3) == -1)
				{
					if (ilPos = FindIdInStrArray(ropPreferredDest, olVia4 + CString(":") + olOrgDes4) == -1)
					{

						if (ilPos = FindIdInStrArray(ropPreferredDest, olOrgDes3) == -1) 
							ilPos = FindIdInStrArray(ropPreferredDest, olOrgDes4);
					}
				}
			
				if (ilPos > -1)
				{
					ilPos++;
					rfpGoodCount += 1/(float)ilPos;
					sprintf(clBuf, "%f", 1/(float)ilPos);
					olNat += "  " + olVia3 + CString(":") + olOrgDes3 + "-" + CString(clBuf);
				}
				else if (ropPreferredDest.GetSize() > 0)
				{
					ripBadCount++;
					olDest += "  " + olVia3 + CString(":") + olOrgDes3 + "-" + olNotIn;
				}

			}






			// check preferred nature codes
			ilPos = FindIdInStrArray(ropPreferredNA, CString(prlFlight->Ttyp)); 
			if (ilPos > -1)
			{
				ilPos++;
				rfpGoodCount += 1/(float)ilPos;
				sprintf(clBuf, "%f", 1/(float)ilPos);
				olNat += "  " + CString(prlFlight->Ttyp) + "-" + CString(clBuf);
			}
			else if (ropPreferredNA.GetSize() > 0)
			{
				ripBadCount++;
				olNat += "  " + CString(prlFlight->Ttyp) + "-" + olNotIn;
			}

			if(bgNewGoodCount)
			{
				// check preferred flight-id
				CStringArray olFltiArr;
				int ilSize = ExtractItemList(olFlti, &olFltiArr, ' ');

				ilPos = FindIdInStrArray(olFltiArr, CString(prlFlight->Flti)); 
				if (ilPos > -1)
				{
					ilPos++;
					rfpGoodCount += 1/(float)ilPos;
					sprintf(clBuf, "%f", 1/(float)ilPos);
					olService += "  " + CString(prlFlight->Flti) + "-" + CString(clBuf);
				}
				else if (olFltiArr.GetSize() > 0)
				{
					ripBadCount++;
					olService += "  " + CString(prlFlight->Flti) + "-" + olNotIn;
				}
			}
			else
			{
				// check preferred service codes
				ilPos = FindIdInStrArray(ropPreferredService, CString(prlFlight->Styp)); 
				if (ilPos > -1)
				{
					ilPos++;
					rfpGoodCount += 1/(float)ilPos;
					sprintf(clBuf, "%f", 1/(float)ilPos);
					olService += "  " + CString(prlFlight->Styp) + "-" + CString(clBuf);
				}
				else if (ropPreferredService.GetSize() > 0)
				{
					ripBadCount++;
					olService += "  " + CString(prlFlight->Styp) + "-" + olNotIn;
				}
			}


			//bgNewGoodCount:
			//We required that the AIRLINES and the A/C TYPE (written as preferential in the rule parameters) must be NOT CONSIDERED in the bad count calculation when the analysed airline or the A/C type is not present in the parameters list (it should have the same bahaviour of the empty list like the N06-N07-W05,etc) position in which no one airlines is written).
			// check preferred airline codes			
			
			if(i == 0)
			{
				CString olDest3;
				CString olDest4;
				if(prpFlightD != NULL)
				{
					olDest3 = CString(":") + CString(prpFlightD->Des3);
					olDest4 = CString(":") + CString(prpFlightD->Des4);
				}

				if ((ilPos = FindIdInStrArray(ropPreferredAL, CString(prlFlight->Alc2) + olDest3)) == -1)
				{
					if ((ilPos = FindIdInStrArray(ropPreferredAL, CString(prlFlight->Alc2) + olDest3)) == -1)
					{
						if ((ilPos = FindIdInStrArray(ropPreferredAL, CString(prlFlight->Alc3) + olDest3)) == -1)
						{
							if ((ilPos = FindIdInStrArray(ropPreferredAL, CString(prlFlight->Alc3) + olDest4)) == -1)
							{
								if ((ilPos = FindIdInStrArray(ropPreferredAL, CString(prlFlight->Alc2))) == -1)
									ilPos = FindIdInStrArray(ropPreferredAL, CString(prlFlight->Alc3)); 
							}
						}
					
					}
				}

				/* backup before test
				if ((ilPos = FindIdInStrArray(ropPreferredAL, CString(prlFlight->Alc2))) == -1)
					ilPos = FindIdInStrArray(ropPreferredAL, CString(prlFlight->Alc3)); 

				*/
			}
			else
			{
				if ((ilPos = FindIdInStrArray(ropPreferredAL, CString(prlFlight->Alc2) + CString(":") + CString(prlFlight->Des3))) == -1)
				{
					if ((ilPos = FindIdInStrArray(ropPreferredAL, CString(prlFlight->Alc2) + CString(":") + CString(prlFlight->Des4))) == -1)
					{
						if ((ilPos = FindIdInStrArray(ropPreferredAL, CString(prlFlight->Alc3) + CString(":") + CString(prlFlight->Des3))) == -1)
						{
							if ((ilPos = FindIdInStrArray(ropPreferredAL, CString(prlFlight->Alc3) + CString(":") + CString(prlFlight->Des4))) == -1)
							{
								if ((ilPos = FindIdInStrArray(ropPreferredAL, CString(prlFlight->Alc2))) == -1)
									ilPos = FindIdInStrArray(ropPreferredAL, CString(prlFlight->Alc3)); 
							}
						}
					
					}
				}
			}




			/* old
			if ((ilPos = FindIdInStrArray(ropPreferredAL, CString(prlFlight->Alc2))) == -1)
				ilPos = FindIdInStrArray(ropPreferredAL, CString(prlFlight->Alc3)); 
			*/
			
			if (ilPos > -1)
			{
				ilPos++;
				rfpGoodCount += 1/(float)ilPos;
				sprintf(clBuf, "%f", 1/(float)ilPos);
				olAlc += "  " + CString(prlFlight->Alc2) + "/" + CString(prlFlight->Alc3) + "-" + CString(clBuf);
			}
			else if (ropPreferredAL.GetSize() > 0 && !bgNewGoodCount)
			{
				ripBadCount++;
				olAlc += "  " + CString(prlFlight->Alc2) + "/" + CString(prlFlight->Alc3) + "-" + olNotIn;
			}

			//bgNewGoodCount:
			//We required that the AIRLINES and the A/C TYPE (written as preferential in the rule parameters) must be NOT CONSIDERED in the bad count calculation when the analysed airline or the A/C type is not present in the parameters list (it should have the same bahaviour of the empty list like the N06-N07-W05,etc) position in which no one airlines is written).
			// check preferred aircraft type
			if ((ilPos = FindIdInStrArray(ropPreferredAC, CString(prlFlight->Act3))) == -1)
				ilPos = FindIdInStrArray(ropPreferredAC, CString(prlFlight->Act5)); 
			if (ilPos > -1)
			{
				ilPos++;
				rfpGoodCount += 1/(float)ilPos;
				sprintf(clBuf, "%f", 1/(float)ilPos);
				olAct += "  " + CString(prlFlight->Act3) + "/" + CString(prlFlight->Act5) + "-" + CString(clBuf);
			}
			else if (ropPreferredAC.GetSize() > 0 && !bgNewGoodCount)
			{
				ripBadCount++;
				olAct += "  " + CString(prlFlight->Act3) + "/" + CString(prlFlight->Act5) + "-" + olNotIn;
			}
		}
	}

	if(bgNewGoodCount)
	{
		rfpGoodCount += dlNewGoodCount;
		opMatchStr = olNat + ";" + olAlc + ";" + olAct + ";" + olService + ";" + CString(clBufPrio);
	}
	else
	{
		opMatchStr = olNat + ";" + olAlc + ";" + olAct + ";" + olService;
	}

	ropPreferredAL.RemoveAll();
	ropPreferredAC.RemoveAll();
	ropPreferredNA.RemoveAll();
	ropPreferredService.RemoveAll();

	return true;
}

bool SpotAllocation::GetMatchCountPst(const DIAFLIGHTDATA *prpFlightA, const DIAFLIGHTDATA *prpFlightD, const CStringArray &ropPreferredAL, const CStringArray &ropPreferredAC, const CStringArray &ropPreferredNA,
									  const CStringArray &ropPreferredService, const CStringArray &ropPreferredDest, float &rfpGoodCount, int &ripBadCount, CString& opMatchStr) const
{
	CString olOrgDes3;
	CString olOrgDes4;
	CString olVia3;
	CString olVia4;
	CString olNat;
	CString olAlc;
	CString olDest;
	CString olAct;
	CString olService;
	CString olNotIn = "NotIn";
	char clBuf[255];

	rfpGoodCount = 0;
	ripBadCount = 0;

	if (prpFlightA == NULL && prpFlightD == NULL)
		return false;


	int ilPos = -1;
	const DIAFLIGHTDATA *prlFlight = NULL;
	
	for (int i = 0; i < 2; i++)
	{
		// check arrival and departure flight
		if (i == 0)
			prlFlight = prpFlightA;
		else
			prlFlight = prpFlightD;

		if (prlFlight != NULL)
		{		
			
			// check preferred destination
			if(i == 1)			
			{

				olOrgDes3 = prlFlight->Des3;
				olOrgDes4 = prlFlight->Des4;

				if( atoi(prlFlight->Vian) > 0)
				{
					CString olVia = CString(prlFlight->Vial); 

					olVia3 = olVia.Mid(1,3);
					olVia4 = olVia.Mid(4,4);
				}
				

				if (ilPos = FindIdInStrArray(ropPreferredDest, olVia3 + CString(":") + olOrgDes3) == -1)
				{
					if (ilPos = FindIdInStrArray(ropPreferredDest, olVia4 + CString(":") + olOrgDes4) == -1)
					{

						if (ilPos = FindIdInStrArray(ropPreferredDest, olOrgDes3) == -1) 
							ilPos = FindIdInStrArray(ropPreferredDest, olOrgDes4);
					}
				}
			
				if (ilPos > -1)
				{
					ilPos++;
					rfpGoodCount += 1/(float)ilPos;
					sprintf(clBuf, "%f", 1/(float)ilPos);
					olNat += "  " + olVia3 + CString(":") + olOrgDes3 + "-" + CString(clBuf);
				}
				else if (ropPreferredDest.GetSize() > 0)
				{
					ripBadCount++;
					olDest += "  " + olVia3 + CString(":") + olOrgDes3 + "-" + olNotIn;
				}

			}
			
			// check preferred nature codes
			ilPos = FindIdInStrArray(ropPreferredNA, CString(prlFlight->Ttyp)); 
			if (ilPos > -1)
			{
				ilPos++;
				rfpGoodCount += 1/(float)ilPos;
				sprintf(clBuf, "%f", 1/(float)ilPos);
				olNat += "  " + CString(prlFlight->Ttyp) + "-" + CString(clBuf);
			}
			else if (ropPreferredNA.GetSize() > 0)
			{
				ripBadCount++;
				olNat += "  " + CString(prlFlight->Ttyp) + "-" + olNotIn;
			}

			// check preferred service codes
			ilPos = FindIdInStrArray(ropPreferredService, CString(prlFlight->Styp)); 
			if (ilPos > -1)
			{
				ilPos++;
				rfpGoodCount += 1/(float)ilPos;
				sprintf(clBuf, "%f", 1/(float)ilPos);
				olService += "  " + CString(prlFlight->Styp) + "-" + CString(clBuf);
			}
			else if (ropPreferredService.GetSize() > 0)
			{
				ripBadCount++;
				olService += "  " + CString(prlFlight->Styp) + "-" + olNotIn;
			}
			
			if(i == 0)
			{
				if ((ilPos = FindIdInStrArray(ropPreferredAL, CString(prlFlight->Alc2))) == -1)
					ilPos = FindIdInStrArray(ropPreferredAL, CString(prlFlight->Alc3)); 
			}
			else
			{
				if ((ilPos = FindIdInStrArray(ropPreferredAL, CString(prlFlight->Alc2) + CString(":") + CString(prlFlight->Des3))) == -1)
				{
					if ((ilPos = FindIdInStrArray(ropPreferredAL, CString(prlFlight->Alc2) + CString(":") + CString(prlFlight->Des4))) == -1)
					{
						if ((ilPos = FindIdInStrArray(ropPreferredAL, CString(prlFlight->Alc3) + CString(":") + CString(prlFlight->Des3))) == -1)
						{
							if ((ilPos = FindIdInStrArray(ropPreferredAL, CString(prlFlight->Alc3) + CString(":") + CString(prlFlight->Des4))) == -1)
							{
								if ((ilPos = FindIdInStrArray(ropPreferredAL, CString(prlFlight->Alc2))) == -1)
									ilPos = FindIdInStrArray(ropPreferredAL, CString(prlFlight->Alc3)); 
							}
						}
					
					}
				}
			}

		
			
			/*old
			// check preferred airline codes			
			if ((ilPos = FindIdInStrArray(ropPreferredAL, CString(prlFlight->Alc2))) == -1)
				ilPos = FindIdInStrArray(ropPreferredAL, CString(prlFlight->Alc3)); 
			old*/

			if (ilPos > -1)
			{
				ilPos++;
				rfpGoodCount += (1/(float)ilPos)*3;
				sprintf(clBuf, "%f", 1/(float)ilPos);
				olAlc += "  " + CString(prlFlight->Alc2) + "/" + CString(prlFlight->Alc3) + "-" + CString(clBuf);
			}
			else if (ropPreferredAL.GetSize() > 0)
			{
				ripBadCount++;
				olAlc += "  " + CString(prlFlight->Alc2) + "/" + CString(prlFlight->Alc3) + "-" + olNotIn;
			}








			// check preferred aircraft type
			if ((ilPos = FindIdInStrArray(ropPreferredAC, CString(prlFlight->Act3))) == -1)
				ilPos = FindIdInStrArray(ropPreferredAC, CString(prlFlight->Act5)); 
			if (ilPos > -1)
			{
				ilPos++;
				rfpGoodCount += 1/(float)ilPos;
				sprintf(clBuf, "%f", 1/(float)ilPos);
				olAct += "  " + CString(prlFlight->Act3) + "/" + CString(prlFlight->Act5) + "-" + CString(clBuf);
			}
			else if (ropPreferredAC.GetSize() > 0)
			{
				ripBadCount++;
				olAct += "  " + CString(prlFlight->Act3) + "/" + CString(prlFlight->Act5) + "-" + olNotIn;
			}
		}
	}

	opMatchStr = olNat + ";" + olAlc + ";" + olAct + ";" + olService+ ";" + olDest;
	return true;
}

bool SpotAllocation::GetMatchCountGat(const DIAFLIGHTDATA *prpFlight, const CStringArray &ropPreferredAL, const CStringArray &ropPreferredOrgDes, const CStringArray &ropPreferredNA, 
									  float &rfpGoodCount, int &ripBadCount) const
{
	rfpGoodCount = 0;
	ripBadCount = 0;
	
	if (prpFlight == NULL)
		return false;

	int ilPos;
	// check preferred nature codes
	ilPos = FindIdInStrArray(ropPreferredNA, CString(prpFlight->Ttyp));
	if (ilPos > -1)
	{
		ilPos++;
		rfpGoodCount += 1/(float)ilPos;
	}
	else if (ropPreferredNA.GetSize() > 0)
	{
		ripBadCount++;
	}

	// check preferred airline codes
	if ((ilPos=FindIdInStrArray(ropPreferredAL, CString(prpFlight->Alc2))) == -1)
		ilPos=FindIdInStrArray(ropPreferredAL, CString(prpFlight->Alc3));
	if (ilPos > -1)
	{
		ilPos++;
		rfpGoodCount += 1/(float)ilPos;
	}
	else if (ropPreferredAL.GetSize() > 0)
	{
		ripBadCount++;
	}

	// check preferred origins/destinations
	CString olOrgDes3;
	CString olOrgDes4;
	CString olVia3;
	CString olVia4;
	if (IsArrivalFlight(prpFlight->Org3, prpFlight->Des3, prpFlight->Ftyp[0]))
	{
		olOrgDes3 = prpFlight->Org3;
		olOrgDes4 = prpFlight->Org4;
	}
	else
	{
		olOrgDes3 = prpFlight->Des3;
		olOrgDes4 = prpFlight->Des4;
	}

	if( atoi(prpFlight->Vian) > 0)
	{
		CString olVia = CString(prpFlight->Vial); 

		olVia3 = olVia.Mid(1,3);
		olVia4 = olVia.Mid(4,4);
	}

	
	if (ilPos = FindIdInStrArray(ropPreferredOrgDes, olVia3 + CString(":") + olOrgDes3) == -1)
	{
		if (ilPos = FindIdInStrArray(ropPreferredOrgDes, olVia4 + CString(":") + olOrgDes4) == -1)
		{

			if (ilPos = FindIdInStrArray(ropPreferredOrgDes, olOrgDes3) == -1) 
				ilPos = FindIdInStrArray(ropPreferredOrgDes, olOrgDes4);
		}
	}
	
	if (ilPos > -1) 
	{
		ilPos++;
		rfpGoodCount += 1/(float)ilPos;
	}
	else if (ropPreferredOrgDes.GetSize() > 0)
	{
		ripBadCount++;
	}

	return true;
}

bool SpotAllocation::GetMatchCountGat(const DIAFLIGHTDATA *prpFlight, GAT_RESLIST *prpPstRule, float &rfpGoodCount, int &ripBadCount, CString& opMatchStr) const
{
	CString olVia3;
	CString olVia4;
	CString olNat;
	CString olAlc;
	CString olOrg;
	CString olService;
	CString olNotIn = "NotIn";
	char clBuf[255];
	char clBufPrio[255];

	rfpGoodCount = 0;
	ripBadCount = 0;
	
	if (prpFlight == NULL)
		return false;

	CStringArray ropPreferredAL;
	CStringArray ropPreferredOrgDes;
	CStringArray ropPreferredNA;
	CStringArray ropPreferredService;

	ropPreferredAL.Append(prpPstRule->PreferredAL);
	ropPreferredAL.Append(prpPstRule->AssignedAL);
	ropPreferredOrgDes.Append(prpPstRule->PreferredOrgDes);
	ropPreferredOrgDes.Append(prpPstRule->AssignedOrgDes);
	ropPreferredNA.Append(prpPstRule->PreferredNA);
	ropPreferredNA.Append(prpPstRule->AssignedNA);
	ropPreferredService.Append(prpPstRule->PreferredService);
	ropPreferredService.Append(prpPstRule->AssignedService);

	double dlNewGoodCount = 0.0;
	if(bgNewGoodCount)
	{
		if (prpPstRule->Sequence > 0)
			dlNewGoodCount = (dgFactorPrio*prpPstRule->Prio + (dgFactorSequ/prpPstRule->Sequence)) * dgFactorGoodCount;
		else
			dlNewGoodCount = (dgFactorPrio*prpPstRule->Prio + 0.0) * dgFactorGoodCount;

		sprintf(clBufPrio, "%f", (float)dlNewGoodCount);

	}

	int ilPos = -1;
	// check preferred nature codes
	ilPos = FindIdInStrArray(ropPreferredNA, CString(prpFlight->Ttyp));
	if (ilPos > -1)
	{
		ilPos++;
		rfpGoodCount += 1/(float)ilPos;
		sprintf(clBuf, "%f", 1/(float)ilPos);
		olNat += "  " + CString(prpFlight->Ttyp) + "-" + CString(clBuf);
	}
	else if (ropPreferredNA.GetSize() > 0)
	{
		ripBadCount++;
		olNat += "  " + CString(prpFlight->Ttyp) + "-" + olNotIn;
	}


	if(bgNewGoodCount)
	{
		CString olFlti("");
		if (IsArrivalFlight(prpFlight->Org3, prpFlight->Des3, prpFlight->Ftyp[0]))
		{
			olFlti = prpPstRule->Flti;
		}
		else
		{
			olFlti = prpPstRule->FltiDep;
		}

		// check preferred flight-id
		CStringArray olFltiArr;
		int ilSize = ExtractItemList(olFlti, &olFltiArr, ' ');

		ilPos = FindIdInStrArray(olFltiArr, CString(prpFlight->Flti)); 
		if (ilPos > -1)
		{
			ilPos++;
			rfpGoodCount += 1/(float)ilPos;
			sprintf(clBuf, "%f", 1/(float)ilPos);
			olService += "  " + CString(prpFlight->Flti) + "-" + CString(clBuf);
		}
		else if (olFltiArr.GetSize() > 0)
		{
			ripBadCount++;
			olService += "  " + CString(prpFlight->Flti) + "-" + olNotIn;
		}
	}
	else
	{
		// check preferred service codes
		ilPos = FindIdInStrArray(ropPreferredService, CString(prpFlight->Styp)); 
		if (ilPos > -1)
		{
			ilPos++;
			rfpGoodCount += 1/(float)ilPos;
			sprintf(clBuf, "%f", 1/(float)ilPos);
			olService += "  " + CString(prpFlight->Styp) + "-" + CString(clBuf);
		}
		else if (ropPreferredService.GetSize() > 0)
		{
			ripBadCount++;
			olService += "  " + CString(prpFlight->Styp) + "-" + olNotIn;
		}
	}


	//bgNewGoodCount:
	//We required that the AIRLINES and the A/C TYPE (written as preferential in the rule parameters) must be NOT CONSIDERED in the bad count calculation when the analysed airline or the A/C type is not present in the parameters list (it should have the same bahaviour of the empty list like the N06-N07-W05,etc) position in which no one airlines is written).
	// check preferred airline codes
	if ((ilPos=FindIdInStrArray(ropPreferredAL, CString(prpFlight->Alc2))) == -1)
		ilPos=FindIdInStrArray(ropPreferredAL, CString(prpFlight->Alc3));
	if (ilPos > -1)
	{
		ilPos++;
		rfpGoodCount += 1/(float)ilPos;
		sprintf(clBuf, "%f", 1/(float)ilPos);
		olAlc += "  " + CString(prpFlight->Alc2) + "/" + CString(prpFlight->Alc3) + "-" + CString(clBuf);
	}
	else if (ropPreferredAL.GetSize() > 0 && !bgNewGoodCount)
	{
		ripBadCount++;
		olAlc += "  " + CString(prpFlight->Alc2) + "/" + CString(prpFlight->Alc3) + "-" + olNotIn;
	}

	if(!bgNewGoodCount)
	{
		// check preferred origins/destinations
		CString olOrgDes3;
		CString olOrgDes4;
		if (IsArrivalFlight(prpFlight->Org3, prpFlight->Des3, prpFlight->Ftyp[0]))
		{
			olOrgDes3 = prpFlight->Org3;
			olOrgDes4 = prpFlight->Org4;
		}
		else
		{
			olOrgDes3 = prpFlight->Des3;
			olOrgDes4 = prpFlight->Des4;
		}

		if( atoi(prpFlight->Vian) > 0)
		{
			CString olVia = CString(prpFlight->Vial); 

			olVia3 = olVia.Mid(1,3);
			olVia4 = olVia.Mid(4,4);
		}

		
		if (ilPos = FindIdInStrArray(ropPreferredOrgDes, olVia3 + CString(":") + olOrgDes3) == -1)
		{
			if (ilPos = FindIdInStrArray(ropPreferredOrgDes, olVia4 + CString(":") + olOrgDes4) == -1)
			{

				if (ilPos = FindIdInStrArray(ropPreferredOrgDes, olOrgDes3) == -1) 
					ilPos = FindIdInStrArray(ropPreferredOrgDes, olOrgDes4);
			}
		}

		if (ilPos > -1) 
		{
			ilPos++;
			rfpGoodCount += 1/(float)ilPos;
			sprintf(clBuf, "%f", 1/(float)ilPos);
			olOrg += "  " + olOrgDes3 + "/" + olOrgDes4 + "-" + CString(clBuf);
		}
		else if (ropPreferredOrgDes.GetSize() > 0)
		{
			ripBadCount++;
			olOrg += "  " + olOrgDes3 + "/" + olOrgDes4 + "-" + olNotIn;
		}
	}


	if(bgNewGoodCount)
	{
		rfpGoodCount += dlNewGoodCount;
		opMatchStr = olNat + ";" + olAlc + ";" + CString(clBufPrio) + ";" + olService;
	}
	else
	{
		opMatchStr = olNat + ";" + olAlc + ";" + olOrg + ";" + olService;
	}

	ropPreferredAL.RemoveAll();
	ropPreferredOrgDes.RemoveAll();
	ropPreferredNA.RemoveAll();
	ropPreferredService.RemoveAll();

	return true;
}

bool SpotAllocation::GetMatchCountGat(const DIAFLIGHTDATA *prpFlight, BLT_RESLIST *prpPstRule, float &rfpGoodCount, int &ripBadCount, CString& opMatchStr) const
{
	CString olVia3;
	CString olVia4;
	CString olNat;
	CString olAlc;
	CString olOrg;
	CString olService;
	CString olNotIn = "NotIn";
	char clBuf[255];

	rfpGoodCount = 0;
	ripBadCount = 0;
	
	if (prpFlight == NULL)
		return false;

	CStringArray ropPreferredAL;
	CStringArray ropPreferredOrgDes;
	CStringArray ropPreferredNA;
	CStringArray ropPreferredService;

	ropPreferredAL.Append(prpPstRule->PreferredAL);
	ropPreferredAL.Append(prpPstRule->AssignedAL);
	ropPreferredOrgDes.Append(prpPstRule->PreferredOrgDes);
	ropPreferredOrgDes.Append(prpPstRule->AssignedOrgDes);
	ropPreferredNA.Append(prpPstRule->PreferredNA);
	ropPreferredNA.Append(prpPstRule->AssignedNA);
	ropPreferredService.Append(prpPstRule->PreferredService);
	ropPreferredService.Append(prpPstRule->AssignedService);


	int ilPos = -1;
	// check preferred nature codes
	ilPos = FindIdInStrArray(ropPreferredNA, CString(prpFlight->Ttyp));
	if (ilPos > -1)
	{
		ilPos++;
		rfpGoodCount += 1/(float)ilPos;
		sprintf(clBuf, "%f", 1/(float)ilPos);
		olNat += "  " + CString(prpFlight->Ttyp) + "-" + CString(clBuf);
	}
	else if (ropPreferredNA.GetSize() > 0)
	{
		ripBadCount++;
		olNat += "  " + CString(prpFlight->Ttyp) + "-" + olNotIn;
	}

	// check preferred service codes
	ilPos = FindIdInStrArray(ropPreferredService, CString(prpFlight->Styp)); 
	if (ilPos > -1)
	{
		ilPos++;
		rfpGoodCount += 1/(float)ilPos;
		sprintf(clBuf, "%f", 1/(float)ilPos);
		olService += "  " + CString(prpFlight->Styp) + "-" + CString(clBuf);
	}
	else if (ropPreferredService.GetSize() > 0)
	{
		ripBadCount++;
		olService += "  " + CString(prpFlight->Styp) + "-" + olNotIn;
	}


	//bgNewGoodCount:
	//We required that the AIRLINES and the A/C TYPE (written as preferential in the rule parameters) must be NOT CONSIDERED in the bad count calculation when the analysed airline or the A/C type is not present in the parameters list (it should have the same bahaviour of the empty list like the N06-N07-W05,etc) position in which no one airlines is written).
	// check preferred airline codes
	if ((ilPos=FindIdInStrArray(ropPreferredAL, CString(prpFlight->Alc2))) == -1)
		ilPos=FindIdInStrArray(ropPreferredAL, CString(prpFlight->Alc3));
	if (ilPos > -1)
	{
		ilPos++;
		rfpGoodCount += 1/(float)ilPos;
		sprintf(clBuf, "%f", 1/(float)ilPos);
		olAlc += "  " + CString(prpFlight->Alc2) + "/" + CString(prpFlight->Alc3) + "-" + CString(clBuf);
	}
	else if (ropPreferredAL.GetSize() > 0 && !bgNewGoodCount)
	{
		ripBadCount++;
		olAlc += "  " + CString(prpFlight->Alc2) + "/" + CString(prpFlight->Alc3) + "-" + olNotIn;
	}

	// check preferred origins/destinations
	CString olOrgDes3;
	CString olOrgDes4;
	if (IsArrivalFlight(prpFlight->Org3, prpFlight->Des3, prpFlight->Ftyp[0]))
	{
		olOrgDes3 = prpFlight->Org3;
		olOrgDes4 = prpFlight->Org4;
	}
	else
	{
		olOrgDes3 = prpFlight->Des3;
		olOrgDes4 = prpFlight->Des4;
	}


	if( atoi(prpFlight->Vian) > 0)
	{
		CString olVia = CString(prpFlight->Vial); 

		olVia3 = olVia.Mid(1,3);
		olVia4 = olVia.Mid(4,4);
	}

	
	if (ilPos = FindIdInStrArray(ropPreferredOrgDes, olVia3 + CString(":") + olOrgDes3) == -1)
	{
		if (ilPos = FindIdInStrArray(ropPreferredOrgDes, olVia4 + CString(":") + olOrgDes4) == -1)
		{

			if (ilPos = FindIdInStrArray(ropPreferredOrgDes, olOrgDes3) == -1) 
				ilPos = FindIdInStrArray(ropPreferredOrgDes, olOrgDes4);
		}
	}

	if (ilPos > -1) 
	{
		ilPos++;
		rfpGoodCount += 1/(float)ilPos;
		sprintf(clBuf, "%f", 1/(float)ilPos);
		olOrg += "  " + olOrgDes3 + "/" + olOrgDes4 + "-" + CString(clBuf);
	}
	else if (ropPreferredOrgDes.GetSize() > 0)
	{
		ripBadCount++;
		olOrg += "  " + olOrgDes3 + "/" + olOrgDes4 + "-" + olNotIn;
	}

	opMatchStr = olNat + ";" + olAlc + ";" + olOrg + ";" + olService;

	ropPreferredAL.RemoveAll();
	ropPreferredOrgDes.RemoveAll();
	ropPreferredNA.RemoveAll();
	ropPreferredService.RemoveAll();

	return true;
}



bool SpotAllocation::GetPositionsByRule(DIAFLIGHTDATA *prlFlightA, DIAFLIGHTDATA *prlFlightD, CStringArray &opPsts )
{
	PST_RESLIST *prlPstRes = NULL;
	DIAFLIGHTDATA *prlFlight;

	if(prlFlightA != NULL)
	{
		prlFlight = prlFlightA;
	}
	else
	{
		prlFlight = prlFlightD;
	}


	if(prlFlightD != NULL || prlFlightA != NULL)
	{
		for( int i = 0; i < omPstRules.GetSize(); i++)
		{
			prlPstRes = &omPstRules[i];

			if(CheckPst(prlFlightA, prlFlightD, prlPstRes->Pnam, NULL, NULL, NULL, true))
			{
				opPsts.Add(prlPstRes->Pnam);
			}
		}
	}
	return true;
}


bool SpotAllocation::GetBeltsByRule(DIAFLIGHTDATA *prlFlight, CStringArray &opBelts)
{
	if (prlFlight == NULL) return false;
	
	opBelts.RemoveAll();

 	int ilBnam = ogBCD.GetFieldIndex("BLT", "BNAM");

	// scan all belts
	for(int i = 0; i < ogBCD.GetDataCount("BLT"); i++)
	{
		RecordSet rlRec;
		ogBCD.GetRecord("BLT", i, rlRec);
		// check belt
		if (strcmp(pcgHome, "ATH") == 0)
		{

//			CheckBlt(prpFlightA, opBlt, ipBltNo, CCSPtrArray<KonfItem> *popKonfList, int ipMaxCount, bool bpPreCheck)
			
			if(CheckBlt(prlFlight, rlRec[ilBnam], 1, NULL, 0, true))
			{
 				opBelts.Add(rlRec[ilBnam]);
			}
		}
		else
		{
			opBelts.Add(rlRec[ilBnam]);
		}
	}

	return true;
}


bool SpotAllocation::GetWrosByRule(DIAFLIGHTDATA *prlFlight, CStringArray &opWros)
{
	if (prlFlight == NULL) return false;
	
	opWros.RemoveAll();

 	int ilNameId = ogBCD.GetFieldIndex("WRO", "WNAM");

	// scan all waiting rooms
	for(int i = 0; i < ogBCD.GetDataCount("WRO"); i++)
	{
		RecordSet rlRec;
		ogBCD.GetRecord("WRO", i, rlRec);
		// check wro
		if(CheckWro(prlFlight, rlRec[ilNameId],1, NULL, true))
		{
 			opWros.Add(rlRec[ilNameId]);
		}
	}

	return true;
}



bool SpotAllocation::GetGatesByRule(DIAFLIGHTDATA *prlFlight, char cpFPart, CStringArray &opGates, int ipGatNo )
{
	if(prlFlight == NULL )
		return true;
	
	GAT_RESLIST *prlGatRes = NULL;

	for( int i = 0; i < omGatRules.GetSize(); i++)
	{
		prlGatRes = &omGatRules[i];

		if(prlGatRes->Gnam == "A01")
			int i = 0;

		if(CheckGat(prlFlight, cpFPart, prlGatRes->Gnam,ipGatNo, NULL, NULL, NULL, true))
		{
			opGates.Add(prlGatRes->Gnam);
		}
	}

	return true;
}






bool SpotAllocation::AllocatePst(DIAFLIGHTDATA *prlFlightA, DIAFLIGHTDATA *prlFlightD, CString opPst)
{
 
	// store the choosen Position to the flight record in the local dataset!
	if(prlFlightD != NULL)
	{
		ogPosDiaFlightData.DeleteFromKeyMap(prlFlightD);
		strcpy(prlFlightD->Pstd, opPst);

		ogPosDiaFlightData.GetBestPstTime(prlFlightD, opPst, false, false, prlFlightD->Pdes, true);
		ogPosDiaFlightData.GetBestPstTime(prlFlightD, opPst, false, true, prlFlightD->Pdbs, true);

		ogPosDiaFlightData.AddToKeyMap(prlFlightD);
		omChangedFlightsRkeyMap.SetAt((void *)prlFlightD->Rkey, NULL);
		omChangedUrnoMap.SetAt((void *)prlFlightD->Urno,prlFlightD);

	}
	if(prlFlightA != NULL)
	{
		ogPosDiaFlightData.DeleteFromKeyMap(prlFlightA);
		strcpy(prlFlightA->Psta, opPst);

		ogPosDiaFlightData.GetBestPstTime(prlFlightA, opPst, true, true, prlFlightA->Pabs, true);
		ogPosDiaFlightData.GetBestPstTime(prlFlightA, opPst, true, false, prlFlightA->Paes, true);

		ogPosDiaFlightData.AddToKeyMap(prlFlightA);
		omChangedFlightsRkeyMap.SetAt((void *)prlFlightA->Rkey, NULL);
		omChangedUrnoMap.SetAt((void *)prlFlightA->Urno,prlFlightA);
	}

/*
	// move the rule for that position to the end of the list of the rules with the same priority
	PST_RESLIST *prlPstRes = NULL;
	int ilCurrPrio = 0;

	for(int i = 0; i < omPstRules.GetSize(); i++)
	{
		if(omPstRules[i].Pnam == opPst)
		{
			prlPstRes = &omPstRules[i];
			ilCurrPrio = omPstRules[i].Prio;
			omPstRules.RemoveAt(i);
			break;

		}
	}

	bool blTreffer = false;
	for(int j = i; j < omPstRules.GetSize(); j++)
	{
		if(omPstRules[j].Prio != ilCurrPrio)
		{
			omPstRules.InsertAt(j, prlPstRes);
			blTreffer = true;
			break;
		}
	}

	if(!blTreffer)
	{
		omPstRules.InsertAt(omPstRules.GetSize(), prlPstRes);
	}

*/
	return true;
}




bool SpotAllocation::AutoGatAllocate()
{
	omAutoAllocGat.RemoveAll();

	for(int i = omGatRules.GetSize() - 1; i >=0; i--)
	{
		omGatRules[i].Used = 0;
	}



	if (bgGATPOS)
		AutoGatAllocatePstGatPos();
	else
		AutoGatAllocatePst();
	//AutoGatAllocateWithoutPst();
	return true;
}



/*
bool SpotAllocation::AutoGatAllocatePstGatPos()
{
	DiaCedaFlightData::RKEYLIST *prlRkey;
	DIAFLIGHTDATA *prlFlightA = NULL;
	DIAFLIGHTDATA *prlFlightD = NULL;

	pomParent->SendMessage(WM_SAS_PROGESS_INIT, omFlights.GetSize(), 0 );

	TRACE("\n=======================================");


	for(int k = omFlights.GetSize()-1; k >= 0 ; k--)
	{
		prlRkey = &omFlights[k];

		prlFlightA = ogPosDiaFlightData.GetFlightAInRotation(prlRkey);
		prlFlightD = ogPosDiaFlightData.GetFlightDInRotation(prlRkey);
	
		if(prlFlightD && CString(prlFlightD->Gtd1).IsEmpty())
			AutoGatAllocatePstFlightGatPos (NULL, prlFlightD);

		if(prlFlightA && CString(prlFlightA->Gta1).IsEmpty())
			AutoGatAllocatePstFlightGatPos (prlFlightA, NULL);

		pomParent->SendMessage(WM_SAS_PROGESS_UPDATE, prlRkey->Rotation.GetSize(), 0 );
	}
	return true;
}
*/

bool SpotAllocation::AutoGatAllocatePstGatPos()
{
//############# only for test ##################################################
	char pclConfigPath[256];
	char pclExcelPath[256];
	char opTrenner[64];
	ofstream ofProt;
	CString olFileName;
	char pHeader[256];
	int ilwidth=1;

	if (bgCreateAutoAllocFiles)
	{
		if (getenv("CEDA") == NULL)
			strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
		else
			strcpy(pclConfigPath, getenv("CEDA"));

		GetPrivateProfileString(ogAppName, "Excel", "DEFAULT",
			pclExcelPath, sizeof pclExcelPath, pclConfigPath);

		GetPrivateProfileString(ogAppName, "ExcelSeparator", ";",
			opTrenner, sizeof opTrenner, pclConfigPath);

		olFileName = "AllocationProtokollGates";

		strcpy (pHeader, CCSLog::GetTmpPath());
		CString path = pHeader;
		olFileName =  path + "\\" + olFileName + ".csv";

		ofProt.open( olFileName, ios::out);
	}
//############# only for test ##################################################




	DiaCedaFlightData::RKEYLIST *prlRkey;
	DIAFLIGHTDATA *prlFlightA = NULL;
	DIAFLIGHTDATA *prlFlightD = NULL;

	pomParent->SendMessage(WM_SAS_PROGESS_INIT, omFlights.GetSize(), 0 );

	TRACE("\n=======================================");

	bgAutoAllocWithGroundTime = false;//only allocate  if groundtime in rule matches
	bgAutoAllocWithExactParameter = true;//only allocate  if all parameters in rule matches
	int ilAnz = 3;
	if (bgAutoAllocGateFetchRotation)
		ilAnz = 4;

	for (int ilRot=1; ilRot <ilAnz; ilRot++)
	{
		for (int ilRound=1; ilRound <3; ilRound++)
		{
			CString olRound;
			if (ilRound == 1)
			{
				olRound = "########### First Round with Matching all Parameters  ############";
			}
			else if (ilRound == 2)
			{
				bgAutoAllocWithGroundTime = false;
				bgAutoAllocWithExactParameter = false;
				olRound = "########### Second Round without Matching all Parameters ############";
			}

			if (bgCreateAutoAllocFiles)
			{
				ofProt	<< endl << olRound << endl;

				if (bgNewGoodCount)
				{

					ofProt  << setw(ilwidth) << "Prio"
						<< setw(1) << opTrenner
						<< setw(ilwidth) << "Sequence"
						<< setw(1) << opTrenner
						<< setw(ilwidth) << "Gnam"
						<< setw(1) << opTrenner
						<< setw(ilwidth) << "Good Count"
						<< setw(1) << opTrenner
						<< setw(ilwidth) << "Bad Count"
						<< setw(1) << opTrenner
						<< setw(ilwidth) << "Prio/Sequ"
						<< setw(1) << opTrenner
						<< setw(ilwidth) << "Nature"
						<< setw(1) << opTrenner
						<< setw(ilwidth) << "Airline"
						<< setw(1) << opTrenner
						<< setw(ilwidth) << "Flight-ID"
						<< endl;
				}
				else
				{
					ofProt  << setw(ilwidth) << "Prio"
						<< setw(1) << opTrenner
						<< setw(ilwidth) << "Sequence"
						<< setw(1) << opTrenner
						<< setw(ilwidth) << "Gnam"
						<< setw(1) << opTrenner
						<< setw(ilwidth) << "Good Count"
						<< setw(1) << opTrenner
						<< setw(ilwidth) << "Bad Count"
						<< setw(1) << opTrenner
						<< setw(ilwidth) << "Nature"
						<< setw(1) << opTrenner
						<< setw(ilwidth) << "Airline"
						<< setw(1) << opTrenner
						<< setw(ilwidth) << "Org/Dest"
						<< setw(1) << opTrenner
						<< setw(ilwidth) << "Service Type"
						<< endl;
				}
			}

			CStringArray olGatArray;
			for(int k=0; k<omFlights.GetSize(); k++)
		//	for(int k = omFlights.GetSize()-1; k >= 0 ; k--)
			{
				prlRkey = &omFlights[k];

				if (bgAutoAllocGateFetchRotation)
				{
					if (ilRot == 1)
					{
						prlFlightD = ogPosDiaFlightData.GetFlightDInRotation(prlRkey);
						prlFlightA = ogPosDiaFlightData.GetFlightAInRotation(prlRkey);
						if (!prlFlightA)
							continue;
					}
					else if (ilRot == 2)
					{
						prlFlightD = ogPosDiaFlightData.GetFlightDInRotation(prlRkey);
						prlFlightA = NULL;
					}
					else if (ilRot == 3)
					{
						prlFlightA = ogPosDiaFlightData.GetFlightAInRotation(prlRkey);
						prlFlightD = NULL;
					}
				}
				else
				{
					if (ilRot == 2)
					{
						prlFlightA = ogPosDiaFlightData.GetFlightAInRotation(prlRkey);
						prlFlightD = NULL;
					}
					else
					{
						prlFlightD = ogPosDiaFlightData.GetFlightDInRotation(prlRkey);
						prlFlightA = NULL;

						if (bgAutoAllocGateFetchRotation)
						{
							prlFlightA = ogPosDiaFlightData.GetFlightAInRotation(prlRkey);
							if (ilRot == 1 && !prlFlightA)
								continue;
						}
					}
				}
			
				if(prlFlightD != NULL)
				{
					if(strlen(prlFlightD->Gtd1) != 0)
						prlFlightD = NULL;
					if(prlFlightD != NULL && strlen(prlFlightD->Pstd) == 0)
						prlFlightD = NULL;
				}
				if(prlFlightA != NULL)
				{
					if(strlen(prlFlightA->Gta1) != 0)
						prlFlightA = NULL;
					if(prlFlightA != NULL && strlen(prlFlightA->Psta) == 0)
						prlFlightA = NULL;
				}

				if(prlFlightD == NULL && prlFlightA == NULL)
					continue;


				CString olGatA = "";
				CString olGatD = "";

				if(prlFlightA && CString(prlFlightA->Gta1).IsEmpty())
					AutoGatAllocatePstFlightGatPos(prlFlightA, NULL, olGatA, ofProt);

//				if(prlFlightD && CString(prlFlightD->Gtd1).IsEmpty())
				if(prlFlightD)
					AutoGatAllocatePstFlightGatPos(NULL, prlFlightD, olGatD, ofProt);

				olGatA += CString("--") + olGatD;
				olGatArray.Add(olGatA);

				pomParent->SendMessage(WM_SAS_PROGESS_UPDATE, prlRkey->Rotation.GetSize(), 0 );
			}
		}
	}

//############# only for test ##################################################
	ofProt.close();
	if (bgCreateAutoAllocFiles)
	{
		PrintSortedRulesGat();
	}
//############# only for test ##################################################

	return true;
}


bool SpotAllocation::AutoGatAllocatePstFlightGatPos(DIAFLIGHTDATA *prpFlightA, DIAFLIGHTDATA *prpFlightD, CString& opAllocatedGate, ofstream& of)
{
	if (prpFlightA == NULL && prpFlightD == NULL)
		return false;

	opAllocatedGate = "";

	if(prpFlightD != NULL)
	{
		CString olGat1;
		if (CString(prpFlightD->Gtd1).IsEmpty())
		{
			if (GetBestGateGatPos(CString(""), prpFlightD, 'D', 1, &omParameters.omGatLeftBuffer, &omParameters.omGatRightBuffer, olGat1, of) == true)
			{
				AllocateGat(NULL, prpFlightD, olGat1, 1);
				opAllocatedGate = olGat1;
			}
		}
		//MWO: 19.04.05 Call for the second Gate 
		CString olGat2;
		if (CString(prpFlightD->Gtd2).IsEmpty())
		{
			if (GetBestGateGatPos(CString(""), prpFlightD, 'D', 2, &omParameters.omGatLeftBuffer, &omParameters.omGatRightBuffer, olGat2, of) == true)
			{
				AllocateGat(NULL, prpFlightD, olGat2, 2);
				opAllocatedGate = olGat1 + CString(" ") + olGat2;
			}
		}
		//END MWO: 19.04.05 Call for the second Gate 
	}
	if(prpFlightA != NULL)
	{
		CString olGat1;
		if (GetBestGateGatPos(CString(""), prpFlightA, 'A', 1, &omParameters.omGatLeftBuffer, &omParameters.omGatRightBuffer, olGat1, of) == true)
		{
			AllocateGat(prpFlightA, NULL, olGat1);
			opAllocatedGate = olGat1;
		}
	}

	return true;
}

bool SpotAllocation::CorrectPossibleMap(const CString& ropRes, const CString& ropBy, CStringArray& ropPossible, CMapStringToString& ropPossibleMap, CStringArray& ropNotCorrected, DIAFLIGHTDATA* prpFlight, int ipAllocNo)
{
	if(prpFlight != NULL)
	{
		CString olCorrectBy;
		if (ropRes == "GAT")
		{
			if (ropBy == "BLT")
				olCorrectBy = "EXT";
			else if (ropBy == "WRO")
				olCorrectBy = "CIC";
			else
				return false;
		}
		else
			return false;

		CStringArray ropBLUE;
		CMapStringToString ropMapBLUE;
		CStringArray ropRED;
		CMapStringToString ropMapRED;

		CString olExt;

		if (olCorrectBy == "EXT")
		{
			if(ipAllocNo == 1)
				olExt = CString(prpFlight->Ext1);
			else
				olExt = CString(prpFlight->Ext2);

			olExt = "";
		}
		else if (olCorrectBy == "CIC")
		{
			olExt = "";
		}
		else
			return false;

		if (olExt.IsEmpty())
		{

			for(int i = 0; i < ropNotCorrected.GetSize(); i++)
			{
				CString olValue;
				CString olKey = ropNotCorrected.GetAt(i);
				if (ropPossibleMap.Lookup(olKey, olValue))// is in resource chain
				{
					if (olValue == "IDB_LIGHTBLUE" || olValue == "IDB_RED")
					{
						ropPossible.Add(olKey);
					}
				}
			}
		}
		else
		{
			if (GetResBy(ropBy, olCorrectBy, olExt,ropBLUE,ropMapBLUE,ropRED,ropMapRED, prpFlight))
			{
				for(int i = 0; i < ropBLUE.GetSize(); i++)
				{
					CString olValue;
					CString olKey = ropBLUE.GetAt(i);
					if (ropMapBLUE.Lookup(olKey, olValue))// is in resource chain
					{
						if (olValue == "IDB_LIGHTBLUE")
						{
							CString olTmp;
							ropPossibleMap.Lookup(olKey, olTmp);
							if (olTmp == "IDB_LIGHTBLUE" || olTmp == "IDB_RED")
							{
								ropPossible.Add(olKey);
							}
						}
					}
				}
			}
		}
	}
	return true;
}

bool SpotAllocation::GetBestGateGatPos(const CString &ropFtliGat, DIAFLIGHTDATA *prpFlight, char cpFPart,
					 int ipGateNo, const CTimeSpan *popGatLeftBuffer, const CTimeSpan *popGatRightBuffer,
					 CString &ropBestGate, ofstream& of)
{
		
	// TSC 0909 THEN olPossibleGatUser bool = 0
	//This is for scenario porposes, to get a different 
	// Ordering and activation concerning the data stored in a file for
	// scenario. All Variables with yyyUser are scenario related items.
	CCSPtrArray<GAT_RESLIST> olPossibleGatUser;
	bool bgatuser = true; // NORMAL
    // END TSC

	if (prpFlight == NULL)
		return false;

	if (ipGateNo != 1 && ipGateNo != 2)
		return false;

	ropBestGate.Empty();
	
	CStringArray olPossibleGat;
	if (!GetGATPOSPossibleGates(prpFlight, olPossibleGat, ipGateNo))
		return false;

	//sortiertes default array
	omGatRules.Sort(CompareGatResByPrio);

//	CPtrArray olPossibleGatesRules;	
	CCSPtrArray<GAT_RESLIST> olPossibleGatesRules;
	GAT_RESLIST *prlGatRes;
	CString olCurrGate;
	for( int i = 0; i < olPossibleGat.GetSize(); i++)
	{
		olCurrGate = olPossibleGat.GetAt(i);
		if(ropFtliGat.IsEmpty() || CheckGatFlightId( olCurrGate, ropFtliGat))							
		{
			if(CheckGat(prpFlight, cpFPart, olCurrGate, ipGateNo, NULL, popGatLeftBuffer, popGatRightBuffer))
			{
				prlGatRes = GetGatRes(olCurrGate);
				//MWO: Hier k�nnte der Mixed Flights Kram auch eingebaut werden.
				if (prlGatRes != NULL)
					olPossibleGatesRules.Add(prlGatRes);
			}
		}
	}

	if (olPossibleGatesRules.GetSize() < 1) 
		return false;

	// regeln die zul�ssig sind, sortiert nach prio/sequ
	olPossibleGatesRules.Sort(CompareGatResByPrio);

	//TSC 0909 GAT 
	

	// Scenario DATEI		
    if(oguseralloctepath!="<Default>" & oguseralloctepath!="")
	{
		std::vector <CString> vDataGAT;
		Userallocatevector(oguseralloctepath);

		bool accesstab   = 0;
		int  counterdata = 0;
		bool bnextdata   = 0; 
		int dd = vData.size();
		for (int i =0; i< vData.size(); i++)
		{
					
			// ONLY GAT
			CString cstrdata=vData[i];
			CString strf="END_PST\n";
			CString strg="END_GAT\n";
			if(strf==cstrdata)accesstab = 1;
			if(strg==cstrdata)accesstab = 0;

			if(accesstab)
			{
				// THE NEXT DS
				if(bnextdata)
				{
					std::string checkedtrue = LPCTSTR(cstrdata);

					int r = checkedtrue.find(';');

					std::string strgaturno = checkedtrue.substr(0,r);
												// FOR ;          					
					std::string checked = checkedtrue.substr(r+1,checkedtrue.length());

					if(checked=="1\n")
					{
						vDataGAT.push_back(strgaturno.c_str());
					}

				}//bnextdata
				bnextdata =1;// NO END_PST

			}//accesstab 

							
		}//	for (int i =0; i< vData.size(); i++)


		int counter = vDataGAT.size();

		GAT_RESLIST *prlGatResUser=NULL;
		
		for( int j = 0; j < olPossibleGatesRules.GetSize(); j++)
		{
			prlGatResUser =  &olPossibleGatesRules[j];
			GAT_RESLIST *prlGatResActl= new (GAT_RESLIST);

			if(!prlGatResUser ->Urno.IsEmpty())
			{
				 for( int k = 0 ; k < vDataGAT.size() ; k++)
				 {
					  if(prlGatResUser->Urno == vDataGAT[k])
					  {
						prlGatResActl->Urno     = prlGatResUser->Urno;
						prlGatResActl->Gnam     = prlGatResUser->Gnam;
						prlGatResActl->Prio     = counter--;
						prlGatResActl->Sequence = 0;  
						olPossibleGatUser.Add(prlGatResActl);
					  }
				 }//for( k = 0 ; k < vDataGAT.size() ; k++)
			}//if(rlGatResUser.Urno.IsEmpty())
		}

		
		if(olPossibleGatUser.GetSize() < 1) 
			return false;
		else
			bgatuser = 0; // NOT NORMAL USER ALLOCATE GAT

		olPossibleGatUser.Sort(CompareGatResOnlyByPrio);

		vDataGAT.clear();

    		
	} //if(oguseralloctepath!="Default")

	// END TSC

	//// get the best possible gate
	 GAT_RESLIST *prlBestGatRule = NULL;

	if(bgatuser)
		prlBestGatRule  = &olPossibleGatesRules[0]; 
	else 
	{
		prlBestGatRule = &olPossibleGatUser[0];
	    prlBestGatRule = GetGatRes(prlBestGatRule->Gnam);
    }    

	float flBestGoodCount = 0.0;
	int ilBestBadCount = 0;
	CString olMatchStr;
//	GetMatchCountGat(prpFlight, prlBestGatRule->PreferredAL, prlBestGatRule->PreferredOrgDes, prlBestGatRule->PreferredNA, flBestGoodCount, ilBestBadCount);  
	GetMatchCountGat(prpFlight, prlBestGatRule, flBestGoodCount, ilBestBadCount, olMatchStr);  

//############# only for test ##################################################
	char opTrenner[64];
	strcpy(opTrenner,";");
	int ilwidth = 1;
	CString ol1;
	CString ol2;
	CString ol3;
	CString ol4;
	CStringArray olArray;
	char clBuf[255];
	CString olFlno;

	if (bgCreateAutoAllocFiles)
	{
		sprintf(clBuf, "%f", flBestGoodCount);
		ExtractItemList(olMatchStr, &olArray, ';');
		if (olArray.GetSize() == 4)
		{
			ol1 = olArray.GetAt(0);//nat
			ol2 = olArray.GetAt(1);//al
			ol3 = olArray.GetAt(2);//org  - new:prio/sequ
			ol4 = olArray.GetAt(3);//serv - new:flti
		}

		GetFlnoForPrintMap(prpFlight, olFlno);

		of.setf(ios::left, ios::adjustfield);
		of  << "-----------------------------------------------------------------"
			<< endl;
		of  << olFlno
			<< endl;
		of  << "-----------------------------------------------------------------"
			<< endl;

		if (bgNewGoodCount)
		{
			of  << setw(ilwidth) << " " << prlBestGatRule->Prio
				<< setw(1) << opTrenner
				<< setw(ilwidth) << " " << prlBestGatRule->Sequence
				<< setw(1) << opTrenner
				<< setw(ilwidth) << " " << prlBestGatRule->Gnam
				<< setw(1) << opTrenner
				<< setw(ilwidth) << " " << clBuf
				<< setw(1) << opTrenner
				<< setw(ilwidth) << " " << ilBestBadCount
				<< setw(1) << opTrenner
				<< setw(ilwidth) << " " << ol3
				<< setw(1) << opTrenner
				<< setw(ilwidth) << " " << ol1
				<< setw(1) << opTrenner
				<< setw(ilwidth) << " " << ol2
				<< setw(1) << opTrenner
				<< setw(ilwidth) << " " << ol4
				<< endl;
		}
		else
		{
			of  << setw(ilwidth) << " " << prlBestGatRule->Prio
				<< setw(1) << opTrenner
				<< setw(ilwidth) << " " << prlBestGatRule->Sequence
				<< setw(1) << opTrenner
				<< setw(ilwidth) << " " << prlBestGatRule->Gnam
				<< setw(1) << opTrenner
				<< setw(ilwidth) << " " << clBuf
				<< setw(1) << opTrenner
				<< setw(ilwidth) << " " << ilBestBadCount
				<< setw(1) << opTrenner
				<< setw(ilwidth) << " " << ol1
				<< setw(1) << opTrenner
				<< setw(ilwidth) << " " << ol2
				<< setw(1) << opTrenner
				<< setw(ilwidth) << " " << ol3
				<< setw(1) << opTrenner
				<< setw(ilwidth) << " " << ol4
				<< endl;
		}
	}
//############# only for test ##################################################
// TSC 0909
	int inumberrules = 0;
    if(bgatuser)
		inumberrules = olPossibleGatesRules.GetSize();
	else 
		inumberrules = olPossibleGatUser.GetSize();
// TSC 0909
	
	// calculate the best rule
	if (!bgAutoAllocFirstRule)
	{
		for (i = 1; i < inumberrules ; i++) //auf xxGatesRules
		{
			float flCurrGoodCount = 0.0;
			int ilCurrBadCount = 0;
		
			//TSC
			if(bgatuser)
				prlGatRes = &olPossibleGatesRules[i]; 
			else
			{
			
				prlBestGatRule = &olPossibleGatUser[i];
				prlBestGatRule = GetGatRes(prlBestGatRule->Gnam);
			
			}
			//END TSC

			GetMatchCountGat(prpFlight, prlGatRes, flCurrGoodCount, ilCurrBadCount, olMatchStr);
			// Is the examined gate better?
			if ((ilCurrBadCount < ilBestBadCount) ||
				(ilCurrBadCount == ilBestBadCount && flCurrGoodCount > flBestGoodCount))
			{
				ilBestBadCount = ilCurrBadCount;
				flBestGoodCount = flCurrGoodCount;
				prlBestGatRule = prlGatRes;
			}
	//############# only for test ##################################################

			if (bgCreateAutoAllocFiles)
			{
				sprintf(clBuf, "%f", flCurrGoodCount);

				ExtractItemList(olMatchStr, &olArray, ';');
				if (olArray.GetSize() == 4)
				{
					ol1 = olArray.GetAt(0);
					ol2 = olArray.GetAt(1);
					ol3 = olArray.GetAt(2);
					ol4 = olArray.GetAt(3);
				}

				if (bgNewGoodCount)
				{
					of  << setw(ilwidth) << " " << prlBestGatRule->Prio
						<< setw(1) << opTrenner
						<< setw(ilwidth) << " " << prlBestGatRule->Sequence
						<< setw(1) << opTrenner
						<< setw(ilwidth) << " " << prlBestGatRule->Gnam
						<< setw(1) << opTrenner
						<< setw(ilwidth) << " " << clBuf
						<< setw(1) << opTrenner
						<< setw(ilwidth) << " " << ilCurrBadCount
						<< setw(1) << opTrenner
						<< setw(ilwidth) << " " << ol3
						<< setw(1) << opTrenner
						<< setw(ilwidth) << " " << ol1
						<< setw(1) << opTrenner
						<< setw(ilwidth) << " " << ol2
						<< setw(1) << opTrenner
						<< setw(ilwidth) << " " << ol4
						<< endl;
				}
				else
				{
					of  << setw(ilwidth) << " " << prlGatRes->Prio
						<< setw(1) << opTrenner
						<< setw(ilwidth) << " " << prlGatRes->Sequence
						<< setw(1) << opTrenner
						<< setw(ilwidth) << " " << prlGatRes->Gnam
						<< setw(1) << opTrenner
						<< setw(ilwidth) << " " << clBuf
						<< setw(1) << opTrenner
						<< setw(ilwidth) << " " << ilCurrBadCount
						<< setw(1) << opTrenner
						<< setw(ilwidth) << " " << ol1
						<< setw(1) << opTrenner
						<< setw(ilwidth) << " " << ol2
						<< setw(1) << opTrenner
						<< setw(ilwidth) << " " << ol3
						<< setw(1) << opTrenner
						<< setw(ilwidth) << " " << ol4
						<< endl;
				}
			}
	//############# only for test ##################################################
		}
	}

	// return the best gate
	if (prlBestGatRule != NULL)
	{
		ropBestGate = prlBestGatRule->Gnam;
		omAutoAllocGat.SetAt(olFlno,ropBestGate);

//############# only for test ##################################################
		if (bgCreateAutoAllocFiles)
		{
			of  << endl
				<< "Allocated Gate : !!!!  " << prlBestGatRule->Gnam << "  !!!!"
				<< endl;
		}
//############# only for test ##################################################
	// TSC
	olPossibleGatUser.DeleteAll();
		
		return true;
	} 

	// TSC
	olPossibleGatUser.DeleteAll();

	return false;
}

bool SpotAllocation::GetGATPOSPossibleGates(DIAFLIGHTDATA *prpFlight, CStringArray& olPossibleGat, int ipGatNo)
{
	CStringArray ropBLUE;
	CMapStringToString ropMapBLUE;
	CStringArray ropRED;
	CMapStringToString ropMapRED;

	CString olPos;
	CString olCorrectBy;
	if (prpFlight->Adid[0] == 'A')
	{
		olPos = CString(prpFlight->Psta);
		olCorrectBy = "BLT";
	}
	else if (prpFlight->Adid[0] == 'D')
	{
		olPos = CString(prpFlight->Pstd);
		olCorrectBy = "WRO";
	}
	else
		return false;

//	only allocate if a position is set (ama while training in haj)
	if (olPos.IsEmpty())
		return false;

	if (GetResBy("GAT", "PST", olPos,ropBLUE,ropMapBLUE,ropRED,ropMapRED, prpFlight, true, ipGatNo))
	{
		POSITION pos;
		CString olKey;
		CString olValue;
		bool blDoRed = true;

		for(int i = 0; i < ropBLUE.GetSize(); i++)
		{
			CString olTmp;
			olKey = ropBLUE.GetAt(i);
			if (ropMapBLUE.Lookup(olKey, olTmp))// is in resource chain
			{
				if (olTmp == "IDB_LIGHTBLUE")
				{
					olPossibleGat.RemoveAll();
					CorrectPossibleMap ("GAT", olCorrectBy, olPossibleGat, ropMapBLUE, ropBLUE, prpFlight, 1);
					blDoRed = false;
					break;
				}
			}
		}

		if (blDoRed && olPos.IsEmpty())
		{
			for( pos = ropMapRED.GetStartPosition(); pos != NULL; )
			{
				ropMapRED.GetNextAssoc( pos, olKey , olValue );
				if (olValue == "IDB_RED")
				{
					olPossibleGat.RemoveAll();
					CorrectPossibleMap ("GAT", olCorrectBy, olPossibleGat, ropMapRED, ropRED, prpFlight, 1);
					break;
				}
			}
		}
	}
	else
		return false;

	return true;
}

bool SpotAllocation::AutoGatAllocatePst()
{
	DiaCedaFlightData::RKEYLIST *prlRkey;
	DIAFLIGHTDATA *prlFlightA = NULL;
	DIAFLIGHTDATA *prlFlightD = NULL;

	pomParent->SendMessage(WM_SAS_PROGESS_INIT, omFlights.GetSize(), 0 );

	TRACE("\n=======================================");


	for(int k = omFlights.GetSize()-1; k >= 0 ; k--)
	{
		prlRkey = &omFlights[k];

		prlFlightA = ogPosDiaFlightData.GetFlightAInRotation(prlRkey);
		prlFlightD = ogPosDiaFlightData.GetFlightDInRotation(prlRkey);
	
		if(prlFlightD != NULL)
		{
			if(strlen(prlFlightD->Gtd1) != 0)
				prlFlightD = NULL;
			if(prlFlightD != NULL && strlen(prlFlightD->Pstd) == 0)
				prlFlightD = NULL;
		}
		if(prlFlightA != NULL)
		{
			if(strlen(prlFlightA->Gta1) != 0)
				prlFlightA = NULL;
			if(prlFlightA != NULL && strlen(prlFlightA->Psta) == 0)
				prlFlightA = NULL;
		}

		if(prlFlightD == NULL && prlFlightA == NULL)
			continue;


		int ilCount = 1;
		if(prlFlightD != NULL && prlFlightA != NULL)
		{
			CTimeSpan olDiff;
			olDiff = prlFlightD->Tifd - prlFlightA->Tifa;

			if(olDiff.GetTotalMinutes() > 1440 || strcmp(prlFlightA->Psta, prlFlightD->Pstd) != 0 )
			{
				ilCount = 2;
			}
		}


		/*
		if (ilCount == 1)
		{
			AutoGatAllocatePstFlight(prlFlightA, prlFlightD);
		}
		else
		*/
		{
			AutoGatAllocatePstFlight(prlFlightA, NULL);
			AutoGatAllocatePstFlight(NULL, prlFlightD);
		}


		pomParent->SendMessage(WM_SAS_PROGESS_UPDATE, prlRkey->Rotation.GetSize(), 0 );
	}

	return true;
}



bool SpotAllocation::AutoGatAllocatePstFlight(DIAFLIGHTDATA *prpFlightA, DIAFLIGHTDATA *prpFlightD)
{
	if (prpFlightA == NULL && prpFlightD == NULL)
		return false;

	bool blTrefferA = false; 
	bool blTrefferD = false;


	if(prpFlightD != NULL && prpFlightA != NULL) 					
	{
		CString olADGat1;
		if (GetBestGate(CString(""), prpFlightA, prpFlightD, 1, &omParameters.omGatLeftBuffer, &omParameters.omGatRightBuffer, olADGat1) == true)
		{
			AllocateGat(NULL, prpFlightD, olADGat1);
			AllocateGat(prpFlightA, NULL, olADGat1);
			blTrefferA = true;
			blTrefferD = true;
		}

	}


	if(prpFlightD != NULL && !blTrefferD)
	{
		CString olGat1;
		if (GetBestGate(CString(""), prpFlightD, 'D', 1, &omParameters.omGatLeftBuffer, &omParameters.omGatRightBuffer, olGat1) == true)
		{
			AllocateGat(NULL, prpFlightD, olGat1);
			blTrefferD = true;
		}
	}
	if(prpFlightA != NULL && !blTrefferA)
	{
		CString olGat1;
		if (GetBestGate(CString(""), prpFlightA, 'A', 1, &omParameters.omGatLeftBuffer, &omParameters.omGatRightBuffer, olGat1) == true)
		{
			AllocateGat(prpFlightA, NULL, olGat1);
			blTrefferA = true;
		}
	}


	return true;
}



bool SpotAllocation::GetBestGate(const CString &ropFtliGat, const DIAFLIGHTDATA *prpFlight, char cpFPart,
					 int ipGateNo, const CTimeSpan *popGatLeftBuffer, const CTimeSpan *popGatRightBuffer, CString &ropBestGate)
{
	if (prpFlight == NULL)
		return false;

	if (ipGateNo != 1 && ipGateNo != 2)
		return false;

	ropBestGate.Empty();
	
	// collect all possible gates
	CPtrArray olPossibleGatesRules;	
	CString olCurrGate;
	GAT_RESLIST *prlGatRes;

	int ilGatCount = ogBCD.GetDataCount("GAT");
	
	for( int i = 0; i < ilGatCount; i++)
	{
		olCurrGate = ogBCD.GetField("GAT", i, "GNAM");

		if(ropFtliGat.IsEmpty() || CheckGatFlightId( olCurrGate, ropFtliGat))							
		{
			if(CheckGat(prpFlight, cpFPart, olCurrGate, ipGateNo, NULL, popGatLeftBuffer, popGatRightBuffer))
			{
				prlGatRes = GetGatRes(olCurrGate);
				if (prlGatRes != NULL)
					olPossibleGatesRules.Add(prlGatRes);
			}
		}
	}

	if (olPossibleGatesRules.GetSize() < 1) 
		return false;

	//// get the best possible gate
	GAT_RESLIST *prlBestGatRule = (GAT_RESLIST *) olPossibleGatesRules[0];
	float flBestGoodCount = 0.0;
	int ilBestBadCount = 0;
	GetMatchCountGat(prpFlight, prlBestGatRule->PreferredAL, prlBestGatRule->PreferredOrgDes, prlBestGatRule->PreferredNA, flBestGoodCount, ilBestBadCount);  
		
	for (i = 1; i < olPossibleGatesRules.GetSize(); i++)
	{
		float flCurrGoodCount = 0.0;
		int ilCurrBadCount = 0;
		prlGatRes = (GAT_RESLIST *) olPossibleGatesRules[i];
		GetMatchCountGat(prpFlight, prlGatRes->PreferredAL, prlGatRes->PreferredOrgDes, prlGatRes->PreferredNA, flCurrGoodCount, ilCurrBadCount);
		// Is the examined gate better?
		if ((ilCurrBadCount < ilBestBadCount) ||
			(ilCurrBadCount == ilBestBadCount && flCurrGoodCount > flBestGoodCount))
		{
			ilBestBadCount = ilCurrBadCount;
			flBestGoodCount = flCurrGoodCount;
			prlBestGatRule = prlGatRes;
		}
	}

	// return the best gate
	if (prlBestGatRule != NULL)
	{
		ropBestGate = prlBestGatRule->Gnam;
		return true;
	} 


	return false;
}



bool SpotAllocation::GetBestGate(const CString &ropFtliGat, const DIAFLIGHTDATA *prpFlightA, const DIAFLIGHTDATA *prpFlightD,
								 int ipGateNo, const CTimeSpan *popGatLeftBuffer, const CTimeSpan *popGatRightBuffer, CString &ropBestGate)
{
	if (prpFlightA == NULL && prpFlightD == NULL)
		return false;

	if (ipGateNo != 1 && ipGateNo != 2)
		return false;

	ropBestGate.Empty();

	// collect all possible gates
	CPtrArray olPossibleGatesRules;	
	CString olCurrGate;
	GAT_RESLIST *prlGatRes;


	int ilGatCount = ogBCD.GetDataCount("GAT");

	for(int i = 0; i < ilGatCount; i++)
	{
		olCurrGate = ogBCD.GetField("GAT", i, "GNAM");

		if(ropFtliGat.IsEmpty() || CheckGatFlightId( olCurrGate, ropFtliGat))							
		{
			if(CheckGat(prpFlightD, 'D', olCurrGate, ipGateNo, NULL, NULL, popGatRightBuffer ) && 
			   CheckGat(prpFlightA, 'A', olCurrGate, ipGateNo, NULL, popGatLeftBuffer, NULL))
			{
				prlGatRes = GetGatRes(olCurrGate);
				if (prlGatRes != NULL)
					olPossibleGatesRules.Add(prlGatRes);
			}
		}
	}

	if (olPossibleGatesRules.GetSize() < 1) 
		return false;

	///// get the best possible gate
	GAT_RESLIST *prlBestGatRule = (GAT_RESLIST *) olPossibleGatesRules[0];

	float flBestGoodCount;
	float flGoodCTmp;
	int ilBestBadCount;
	int ilBadCTmp;

	// calculate benchmark for gate
	GetMatchCountGat(prpFlightA, prlBestGatRule->PreferredAL, prlBestGatRule->PreferredOrgDes, prlBestGatRule->PreferredNA, flGoodCTmp, ilBadCTmp);
	flBestGoodCount = flGoodCTmp;
	ilBestBadCount = ilBadCTmp;
	GetMatchCountGat(prpFlightD, prlBestGatRule->PreferredAL, prlBestGatRule->PreferredOrgDes, prlBestGatRule->PreferredNA, flGoodCTmp, ilBadCTmp);
	flBestGoodCount += flGoodCTmp;
	ilBestBadCount += ilBadCTmp;

	float flCurrGoodCount;
	int ilCurrBadCount;
	for (i = 1; i < olPossibleGatesRules.GetSize(); i++)
	{
		// calculate benchmark for gate
		prlGatRes = (GAT_RESLIST *) olPossibleGatesRules[i];
		GetMatchCountGat(prpFlightA, prlGatRes->PreferredAL, prlGatRes->PreferredOrgDes, prlGatRes->PreferredNA, flGoodCTmp, ilBadCTmp);
		flCurrGoodCount = flGoodCTmp;
		ilCurrBadCount = ilBadCTmp;
		GetMatchCountGat(prpFlightD, prlGatRes->PreferredAL, prlGatRes->PreferredOrgDes, prlGatRes->PreferredNA, flGoodCTmp, ilBadCTmp);
		flCurrGoodCount += flGoodCTmp;
		ilCurrBadCount += ilBadCTmp;

		// Is the examined gate better?
		if ((ilCurrBadCount < ilBestBadCount) ||
			(ilCurrBadCount == ilBestBadCount && flCurrGoodCount > flBestGoodCount))
		{
			ilBestBadCount = ilCurrBadCount;
			flBestGoodCount = flCurrGoodCount;
			prlBestGatRule = prlGatRes;
		}
	}

	// return the best gate
	if (prlBestGatRule != NULL)
	{
		ropBestGate = prlBestGatRule->Gnam;
		return true;
	} 



	return false;
}





/*
bool SpotAllocation::AutoGatAllocateWithoutPst()
{

	DiaCedaFlightData::RKEYLIST *prlRkey;
	DIAFLIGHTDATA *prlFlightA = NULL;
	DIAFLIGHTDATA *prlFlight = NULL;
	DIAFLIGHTDATA *prlFlightD = NULL;
	GAT_RESLIST *prlGatRes = NULL;


	pomParent->SendMessage(WM_SAS_PROGESS_INIT, omFlights.GetSize(), 0 );

	int ilRotCount ;

	TRACE("\n=======================================");

	for(int k = omFlights.GetSize()-1; k >= 0 ; k--)
	{
		prlRkey = &omFlights[k];

		ilRotCount = prlRkey->Rotation.GetSize();

		prlFlightA = ogPosDiaFlightData.GetFlightAInRotation(prlRkey);
		prlFlightD = ogPosDiaFlightData.GetFlightDInRotation(prlRkey);
	
		if(prlFlightD != NULL)
		{
			if(!CString(prlFlightD->Gtd1).IsEmpty())
				prlFlightD = NULL;
			if(!CString(prlFlightD->Pstd).IsEmpty())
				prlFlightD = NULL;
		}
		if(prlFlightA != NULL)
		{
			if(!CString(prlFlightA->Gta1).IsEmpty())
				prlFlightA = NULL;
			if(!CString(prlFlightA->Psta).IsEmpty())
				prlFlightA = NULL;
		}

		if(prlFlightA != NULL)
		{
			prlFlight = prlFlightA;
		}
		else
		{
			prlFlight = prlFlightD;
		}

		int ilCount = 1;
		
		if(prlFlightD != NULL && prlFlightA != NULL)
		{
			CTimeSpan olDiff;
			olDiff = prlFlightD->Tifd - prlFlightA->Tifa;

			if(olDiff.GetTotalMinutes() > 1440)
			{
				ilCount = 2;
			}
		}


		if(prlFlightD != NULL || prlFlightA != NULL)
		{
			for(int j = 0; j < ilCount; j++)
			{
				if(ilCount == 2 && j == 0)
				{
					prlFlightD = NULL;
				}
	
				if(ilCount == 2 && j == 1)
				{
					prlFlightA = NULL;
					prlFlight = prlFlightD = ogPosDiaFlightData.GetFlightDInRotation(prlRkey);
				}
				
				bool blTrefferA = false;
				bool blTrefferD = false;

				if((prlFlightA != NULL) && (prlFlightD != NULL))
				{
					for(int i = 0; i < omGatRules.GetSize(); i++)
					{
						prlGatRes = &omGatRules[i];

						if(!prlGatRes->Pral.IsEmpty())
						{
							if((prlGatRes->Pral.Find( CString(";") + CString(prlFlightA->Alc3) + CString(";")) >= 0) ||
							   (prlGatRes->Pral.Find( CString(";") + CString(prlFlightA->Alc2) + CString(";")) >= 0) ||
							   (prlGatRes->Pral.Find( CString(";") + CString(prlFlightD->Alc3) + CString(";")) >= 0) ||
							   (prlGatRes->Pral.Find( CString(";") + CString(prlFlightD->Alc2) + CString(";")) >= 0) )
							{
								if(CheckGat(prlFlightD,prlGatRes->Gnam,1, NULL, NULL, &omParameters.omGatRightBuffer ) && CheckGat(prlFlightA,prlGatRes->Gnam,1, NULL, &omParameters.omGatLeftBuffer, NULL))
								{
									AllocateGat(NULL, prlFlightD, prlGatRes->Gnam);
									AllocateGat(prlFlightA,NULL, prlGatRes->Gnam);
									blTrefferA = true;
									blTrefferD = true;
									break;
								}
							}
						}
					}
					if(!blTrefferA)
					{
						for(int i = 0; i < omGatRules.GetSize(); i++)
						{
							prlGatRes = &omGatRules[i];

							if(prlGatRes->Pral.IsEmpty())
							{
								if(CheckGat(prlFlightD,prlGatRes->Gnam,1, NULL, NULL, &omParameters.omGatRightBuffer ) && CheckGat(prlFlightA,prlGatRes->Gnam,1, NULL, &omParameters.omGatLeftBuffer, NULL))
								{
									AllocateGat(NULL, prlFlightD, prlGatRes->Gnam);
									AllocateGat(prlFlightA,NULL, prlGatRes->Gnam);
									blTrefferA = true;
									blTrefferD = true;
									break;
								}
							}
						}
					}

				}


				if(prlFlightA != NULL && !blTrefferA)
				{
					for(int i = 0; i < omGatRules.GetSize(); i++)
					{
						prlGatRes = &omGatRules[i];

						if(!prlGatRes->Pral.IsEmpty())
						{
							if((prlGatRes->Pral.Find( CString(";") + CString(prlFlightA->Alc3) + CString(";")) >= 0) ||
							   (prlGatRes->Pral.Find( CString(";") + CString(prlFlightA->Alc2) + CString(";")) >= 0) )
							{
								if(CheckGat(prlFlightA, prlGatRes->Gnam,1, NULL, &omParameters.omGatLeftBuffer, &omParameters.omGatRightBuffer))
								{
									AllocateGat(prlFlightA,NULL,  prlGatRes->Gnam);
									blTrefferA = true;
									break;
								}
		
							}
						}
					}
					if(!blTrefferA)
					{
						for(int i = 0; i < omGatRules.GetSize(); i++)
						{
							prlGatRes = &omGatRules[i];

							if(prlGatRes->Pral.IsEmpty())
							{
								if(CheckGat(prlFlightA, prlGatRes->Gnam,1, NULL, &omParameters.omGatLeftBuffer, &omParameters.omGatRightBuffer))
								{
									AllocateGat(prlFlightA,NULL,  prlGatRes->Gnam);
									blTrefferA = true;
									break;
								}
							}
						}
					}
				}

				if(prlFlightD != NULL && !blTrefferD)
				{
					for(int i = 0; i < omGatRules.GetSize(); i++)
					{
						prlGatRes = &omGatRules[i];

						if(!prlGatRes->Pral.IsEmpty())
						{
							if((prlGatRes->Pral.Find( CString(";") + CString(prlFlightD->Alc3) + CString(";")) >= 0) ||
							   (prlGatRes->Pral.Find( CString(";") + CString(prlFlightD->Alc2) + CString(";")) >= 0) )
							{
								if(CheckGat(prlFlightD, prlGatRes->Gnam,1, NULL, &omParameters.omGatLeftBuffer, &omParameters.omGatRightBuffer))
								{
									AllocateGat(NULL,prlFlightD,  prlGatRes->Gnam);
									blTrefferD = true;
									break;
								}
							}
						}
					}
					if(!blTrefferD)
					{
						for(int i = 0; i < omGatRules.GetSize(); i++)
						{
							prlGatRes = &omGatRules[i];

							if(prlGatRes->Pral.IsEmpty())
							{
								if(CheckGat(prlFlightD, prlGatRes->Gnam,1, NULL, &omParameters.omGatLeftBuffer, &omParameters.omGatRightBuffer))
								{
									AllocateGat(NULL,prlFlightD,  prlGatRes->Gnam);
									blTrefferD = true;
									break;
								}
							}
						}
					}

				}
			}
		}
		pomParent->SendMessage(WM_SAS_PROGESS_UPDATE, 1, 0 );
	}
	return true;
}
*/


bool SpotAllocation::AllocateGat(DIAFLIGHTDATA *prlFlightA, DIAFLIGHTDATA *prlFlightD, CString opGat, int ipGat /* = 1 */)
{
//	if (ipGat == 2)
//		return true;



	GAT_RESLIST *prlGatRes = NULL;
	prlGatRes = GetGatRes(opGat);

	if(prlGatRes !=  NULL)
		prlGatRes->Used = prlGatRes->Used +1;



	if(prlFlightD != NULL)
	{
		ogPosDiaFlightData.DeleteFromKeyMap(prlFlightD);
		if(ipGat == 1)
		{
			strcpy(prlFlightD->Gtd1, opGat);
			ogPosDiaFlightData.GetBestGatTime(prlFlightD, opGat, 1, 'D', false, prlFlightD->Gd1e, true);
			ogPosDiaFlightData.GetBestGatTime(prlFlightD, opGat, 1, 'D', true,  prlFlightD->Gd1b, true);
		}
		else
		{
			strcpy(prlFlightD->Gtd2, opGat);
			ogPosDiaFlightData.GetBestGatTime(prlFlightD, opGat, 2, 'D', false, prlFlightD->Gd2e, true);
			ogPosDiaFlightData.GetBestGatTime(prlFlightD, opGat, 2, 'D', true,  prlFlightD->Gd2b, true);
		}

		ogPosDiaFlightData.AddToKeyMap(prlFlightD);
		omChangedFlightsRkeyMap.SetAt((void *)prlFlightD->Rkey, NULL);
	}
	if(prlFlightA != NULL)
	{
		ogPosDiaFlightData.DeleteFromKeyMap(prlFlightA);
		if(ipGat == 1)
		{
			strcpy(prlFlightA->Gta1, opGat);
			ogPosDiaFlightData.GetBestGatTime(prlFlightA, opGat, 1, 'A', false, prlFlightA->Ga1e, true);
			ogPosDiaFlightData.GetBestGatTime(prlFlightA, opGat, 1, 'A', true,  prlFlightA->Ga1b, true);
		}
		else
		{
			strcpy(prlFlightA->Gta2, opGat);
			ogPosDiaFlightData.GetBestGatTime(prlFlightA, opGat, 2, 'A', false, prlFlightA->Ga2e, true);
			ogPosDiaFlightData.GetBestGatTime(prlFlightA, opGat, 2, 'A', true,  prlFlightA->Ga2b, true);
		}

		ogPosDiaFlightData.AddToKeyMap(prlFlightA);
		omChangedFlightsRkeyMap.SetAt((void *)prlFlightA->Rkey, NULL);
	}

	SetTerminals(prlFlightA, CString("GAT"));
	SetTerminals(prlFlightD, CString("GAT"));

	return true;
}


bool SpotAllocation::AllocateBlt(DIAFLIGHTDATA *prlFlightA, CString opBlt, int ipBelt)
{
	if(prlFlightA != NULL)
	{
		ogPosDiaFlightData.DeleteFromKeyMap(prlFlightA);
		if(ipBelt == 1)
			strcpy(prlFlightA->Blt1, opBlt);
		else
			strcpy(prlFlightA->Blt2, opBlt);

		ogPosDiaFlightData.AddToKeyMap(prlFlightA);
		omChangedFlightsRkeyMap.SetAt((void *)prlFlightA->Rkey, NULL);
	}
	return true;
}

bool SpotAllocation::SetTerminals(DIAFLIGHTDATA *prpFlight, CString& opType)
{
	if(prpFlight != NULL && prpFlight->Adid[0] == 'A')
	{
		CString olTerm;
		if (opType == "BLT")
		{
			if (strlen(prpFlight->Blt1) > 0 && ogBCD.GetField("BLT", "BNAM", CString(prpFlight->Blt1), "TERM", olTerm))
					strcpy(prpFlight->Tmb1, olTerm);

			if (strlen(prpFlight->Blt2) > 0 && ogBCD.GetField("BLT", "BNAM", CString(prpFlight->Blt2), "TERM", olTerm))
					strcpy(prpFlight->Tmb2, olTerm);
		}

		if (opType == "EXT")
		{
			if (strlen(prpFlight->Ext1) > 0 && ogBCD.GetField("EXT", "ENAM", CString(prpFlight->Ext1), "TERM", olTerm))
					strcpy(prpFlight->Tet1, olTerm);

			if (strlen(prpFlight->Ext2) > 0 && ogBCD.GetField("EXT", "ENAM", CString(prpFlight->Ext2), "TERM", olTerm))
					strcpy(prpFlight->Tet2, olTerm);
		}

		if (opType == "GAT")
		{
			if (strlen(prpFlight->Gta1) > 0 && ogBCD.GetField("GAT_PORT", "GNAM", CString(prpFlight->Gta1), "TERM", olTerm))
					strcpy(prpFlight->Tga1, olTerm);

			if (strlen(prpFlight->Gta2) > 0 && ogBCD.GetField("GAT_PORT", "GNAM", CString(prpFlight->Gta2), "TERM", olTerm))
					strcpy(prpFlight->Tga2, olTerm);
		}
	}

	if(prpFlight != NULL && prpFlight->Adid[0] == 'D')
	{
		CString olTerm;
		if (opType == "WRO")
		{
			if (strlen(prpFlight->Wro1) > 0 && ogBCD.GetField("WRO", "WNAM", CString(prpFlight->Wro1), "TERM", olTerm))
					strcpy(prpFlight->Twr1, olTerm);
		}

		if (opType == "GAT")
		{
			if (strlen(prpFlight->Gtd1) > 0 && ogBCD.GetField("GAT_GATE", "GNAM", CString(prpFlight->Gtd1), "TERM", olTerm))
					strcpy(prpFlight->Tgd1, olTerm);

			if (strlen(prpFlight->Gtd2) > 0 && ogBCD.GetField("GAT_GATE", "GNAM", CString(prpFlight->Gtd2), "TERM", olTerm))
					strcpy(prpFlight->Tgd2, olTerm);
		}
	}

	return true;
}


bool SpotAllocation::AllocateWro(DIAFLIGHTDATA *prlFlightD, CString opWro, int ipWro)
{
	if(prlFlightD != NULL)
	{
		ogPosDiaFlightData.DeleteFromKeyMap(prlFlightD);
		if(ipWro == 1)
		{
			strcpy(prlFlightD->Wro1, opWro);
			ogPosDiaFlightData.GetBestWroTime(prlFlightD, prlFlightD->Wro1, 1, true,  prlFlightD->W1bs);
			ogPosDiaFlightData.GetBestWroTime(prlFlightD, prlFlightD->Wro1, 1, false, prlFlightD->W1es);
		}
		else
		{
			strcpy(prlFlightD->Wro2, opWro);
			ogPosDiaFlightData.GetBestWroTime(prlFlightD, prlFlightD->Wro2, 2, true,  prlFlightD->W2bs);
			ogPosDiaFlightData.GetBestWroTime(prlFlightD, prlFlightD->Wro2, 2, false, prlFlightD->W2es);
		}

		ogPosDiaFlightData.AddToKeyMap(prlFlightD);
		omChangedFlightsRkeyMap.SetAt((void *)prlFlightD->Rkey, NULL);

		SetTerminals(prlFlightD, CString("WRO"));
	}
	return true;
}






bool SpotAllocation::AutoWroAllocate()
{
	omAutoAllocWro.RemoveAll();

	DiaCedaFlightData::RKEYLIST *prlRkey;
	DIAFLIGHTDATA *prlFlightD = NULL;
	CString olWro;
	CCSPtrArray<RecordSet> olWros;


	pomParent->SendMessage(WM_SAS_PROGESS_INIT, omFlights.GetSize(), 0 );

	int ilRotCount ;

	for(int k=0; k<omFlights.GetSize(); k++)
//	for(int k = omFlights.GetSize()-1; k >= 0 ; k--)
	{
		prlRkey = &omFlights[k];

		ilRotCount = prlRkey->Rotation.GetSize();

		prlFlightD = ogPosDiaFlightData.GetFlightDInRotation(prlRkey);
	
		if (bgGATPOS)
		{
			AutoWroAllocateFlightGatPos(prlFlightD, 1);
			AutoWroAllocateFlightGatPos(prlFlightD, 2);
		}
		else
		{
			if(prlFlightD != NULL)
			{
				if(!CString(prlFlightD->Wro1).IsEmpty())
					prlFlightD = NULL;
				if(!CString(prlFlightD->Wro2).IsEmpty())
					prlFlightD = NULL;
				if(CString(prlFlightD->Gtd1).IsEmpty())
					prlFlightD = NULL;
			}

			olWros.RemoveAll();
			ogBCD.GetRecords("WRO", "GTE1", CString(prlFlightD->Gtd1), &olWros);
			//ogBCD.GetRecords("WRO", "GTE2", CString(prlFlightD->Gtd1), &olWros);

			for(int i = 0; i < olWros.GetSize(); i++)
			{
				olWro = olWros[i].Values[ogBCD.GetFieldIndex("WRO", "WNAM")];

				if(CheckWro(prlFlightD, olWro, 1, NULL, true))
				{
					AllocateWro(prlFlightD, olWro, 1);
				}
			}
		}

		pomParent->SendMessage(WM_SAS_PROGESS_UPDATE, 1, 0 );
	}
	return true;
}

bool SpotAllocation::AutoWroAllocateFlightGatPos(DIAFLIGHTDATA *prlFlightD, int ipWroNo)
{
	if (prlFlightD == NULL)
		return false;

	CString olGate;
	if (ipWroNo == 1)
	{
		if (strlen(prlFlightD->Wro1) > 0)
			return true;

		olGate = CString(prlFlightD->Gtd1);
	}
	else if (ipWroNo == 2)
	{
		if (strlen(prlFlightD->Wro2) > 0)
			return true;

		olGate = CString(prlFlightD->Gtd2);
		if (olGate.IsEmpty())
			return false;
	}
	else
		return false;


//	only allocate if a gate is set (ama while training in haj)
	if (olGate.IsEmpty())
		return false;


	CStringArray ropBLUE;
	CMapStringToString ropMapBLUE;
	CStringArray ropRED;
	CMapStringToString ropMapRED;

	if (GetResBy("WRO", "GAT", olGate,ropBLUE,ropMapBLUE,ropRED,ropMapRED, prlFlightD))
	{
		CString olFlno;
		GetFlnoForPrintMap(prlFlightD, olFlno);

		POSITION pos;
		CString olKey;
		CString olValue;
		bool blDoRed = true;

		for(int i = 0; i < ropBLUE.GetSize(); i++)
		{
			CString olValue;
			CString olKey = ropBLUE.GetAt(i);
			if (ropMapBLUE.Lookup(olKey, olValue))// is in resource chain
			{
				if (olValue == "IDB_LIGHTBLUE")
				{
					if(CheckWro(prlFlightD, olKey, 1, NULL, true))
					{
						if (AllocateWro(prlFlightD, olKey, ipWroNo))
						{
//							CString olFlno = prlFlightD->Flno;
							omAutoAllocWro.SetAt(olFlno, olKey);
							blDoRed = false;
							break;
						}
					}
				}
			}
		}
/*
		for( pos = ropMapBLUE.GetStartPosition(); pos != NULL; )
		{
			ropMapBLUE.GetNextAssoc( pos, olKey , olValue );
			if (olValue == "IDB_LIGHTBLUE")
			{
				if (AllocateWro(prlFlightD, olKey, ipWroNo))
				{
					blDoRed = false;
					break;
				}
			}
		}
*/
		if (blDoRed && olGate.IsEmpty())
		{
			for( pos = ropMapRED.GetStartPosition(); pos != NULL; )
			{
				ropMapRED.GetNextAssoc( pos, olKey , olValue );
				if (olValue == "IDB_RED")
				{
					if(CheckWro(prlFlightD, olKey, 1, NULL, true))
					{
						if (AllocateWro(prlFlightD, olKey, ipWroNo))
						{
//							CString olFlno = prlFlightD->Flno;
							omAutoAllocWro.SetAt(olFlno, olKey);
							break;
						}
					}
				}
			}
		}
	}

	return true;
}


bool SpotAllocation::CheckWro(DIAFLIGHTDATA *prpFlightD, CString opWro, int ipWroNo, CCSPtrArray<KonfItem> *popKonfList, bool bpPreCheck)
{

	if(opWro.IsEmpty() || prpFlightD == NULL || (ipWroNo != 1 && ipWroNo != 2))
		return true;

	bool blRet = true;

	CString olConfText;

	CTime olStartAlloc;
	CTime olEndAlloc;

	if (!ogPosDiaFlightData.GetWroAllocTimes(*prpFlightD, opWro, ipWroNo, olStartAlloc, olEndAlloc, bpPreCheck))
	{
		return false;
	}

	// Check terminal ristriction 
	if(bgTermRestrForAl)
	{
		CString olTerw;
		ogBCD.GetField("WRO", "WNAM", opWro, "TERM", olTerw);
		CString olAltTerw = ogBCD.GetFieldExt("ALT","ALC2","ALC3",prpFlightD->Alc2,prpFlightD->Alc3,"TERW");
		if( !olAltTerw.IsEmpty() && !olTerw.IsEmpty())
		{
			if (olAltTerw.Find(olTerw) < 0)
			{
				if(popKonfList != NULL)
				{
					// Terminal Lounge %s: %s must be %s
					olConfText.Format(GetString(IDS_STRING2730), opWro, olTerw, olAltTerw);
					AddToKonfList(NULL, prpFlightD, 0, ' ', IDS_STRING2727, olConfText, *popKonfList);
					blRet = false;					
				}
				else
				{
					return false;
				}
			}
		}
	}


	// Check Wro avaleblility
	CStringArray opNafr;
	CStringArray opNato;

	WRO_RESLIST *prlWroRes = GetWroRes(opWro); 
	if(prlWroRes != NULL)
	{
		for (int il = 0; il < prlWroRes->BlkData.GetSize(); il++)
		{
			CTime olNafr = prlWroRes->BlkData[il].StartTime;
			CTime olNato = prlWroRes->BlkData[il].EndTime;

			if(olNafr != TIMENULL || olNato != TIMENULL)
			{	
				if(olNafr == TIMENULL)
					olNafr = CTime(1971,1,1,0,0,0);
				if(olNato == TIMENULL)
					olNato = CTime(2020,1,1,0,0,0);
			}

			if((olNafr != TIMENULL) && (olNato != TIMENULL))
			{
				if(IsReallyOverlapped(olStartAlloc, olEndAlloc, olNafr, olNato)/*, olStartAlloc, olEndAlloc) || !IsWithIn(olStartAlloc, olEndAlloc, olNafr, olNato)*/)
				{
					if(popKonfList != NULL)
					{
						if(bgGatPosLocal) ogBasicData.UtcToLocal(olNafr);
						if(bgGatPosLocal) ogBasicData.UtcToLocal(olNato);
						//Gate %s not available %s - %s reason: %s
						olConfText.Format(GetString(IDS_STRING2204), olNafr.Format("%H:%M %d.%m.%y"), olNato.Format("%H:%M %d.%m.%y"),  prlWroRes->BlkData[il].Text);
						AddToKonfList((DIAFLIGHTDATA *)NULL , prpFlightD, 0, ' ', IDS_STRING2204, olConfText, *popKonfList);

						blRet = false;
					}
					else
					{
						return false;
					}
				}
			}
		}
	}


	//// Check max. number of wros ( overlapp )
//	int ilCountOverlappingWro = ogPosDiaFlightData.GetMaxOverlapAtWro(olStartAlloc, olEndAlloc, opWro, prpFlightD);
	int ilPax = 0;
	int ilCountOverlappingWro = ogPosDiaFlightData.GetMaxOverlapAtWroAndPax(olStartAlloc, olEndAlloc, opWro, prpFlightD, ilPax);

	// get max count
	int	ilMaxCount = atoi( ogBCD.GetField("WRO", "WNAM", opWro, "MAXF"));

	if(ilMaxCount == 0)
		ilMaxCount = 1;

	if( ilMaxCount < ilCountOverlappingWro)
	{
		if(popKonfList != NULL)
		{
			//Too many flights at lounge %s: %d Max: %d
			olConfText.Format(GetString(IDS_STRING1734), opWro, ilCountOverlappingWro, ilMaxCount);
			AddToKonfList(NULL, prpFlightD, 0, ' ', IDS_STRING1734, olConfText, *popKonfList);
			blRet = false;
		}
		else
		{
			return false;
		}
	}

	//max passengers
	CString	olBrca = ogBCD.GetField("WRO", "WNAM", opWro, "BRCA");
	int ilBrca = atoi(olBrca);
	if(ilBrca > 0)
	{
		if(ilBrca < ilPax)
		{
			//check second lounge
			CString olWro2("");
			int ilMaxp2 = 0;
			int ilPax2 = 0;
			bool blCflInserted = false;
			if (ogPosDiaFlightData.HasTwoWro(prpFlightD))
			{
				if (ipWroNo == 1)
					olWro2 = prpFlightD->Wro2;
				else
					olWro2 = prpFlightD->Wro1;

				if (!olWro2.IsEmpty())
				{
					CString	olBrca2 = ogBCD.GetField("WRO", "WNAM", olWro2, "BRCA");
					int ilBrca2 = atoi(olBrca2);
					if(ilBrca2 > 0)
					{
						CTime olStartAlloc2;
						CTime olEndAlloc2;
						int ilWro2No = 1;
						if (ipWroNo == 1)
							ilWro2No = 2;

						if (ogPosDiaFlightData.GetWroAllocTimes(*prpFlightD, olWro2, ilWro2No, olStartAlloc2, olEndAlloc2, bpPreCheck))
						{
							int ilCountActAtWro2 = ogPosDiaFlightData.GetMaxOverlapAtWroAndPax(olStartAlloc2, olEndAlloc2, olWro2, prpFlightD, ilPax2);

							if(ilBrca2 < ilPax2)
							{
								if(popKonfList != NULL)
								{
									//Too many Passengers at lounge %s: %d Max: %d and lounge %s: %d Max: %d
									olConfText.Format(GetString(IDS_STRING2425), opWro, ilPax, ilBrca, olWro2, ilPax2, ilBrca2);
									AddToKonfList(prpFlightD, 'D', 0, ' ', IDS_STRING2424, olConfText, *popKonfList);
									blRet = false;
									blCflInserted = true;
								}
								else
								{
									return false;
								}
							}
						}
					}
				}
			}


			if(!blCflInserted && popKonfList != NULL)
			{
				//Too many Passengers at Lounge %s: %d Max: %d
				olConfText.Format(GetString(IDS_STRING2424), opWro, ilPax, ilBrca);
				AddToKonfList(prpFlightD, 'D', 0, ' ', IDS_STRING2424, olConfText, *popKonfList);
				blRet = false;
			}
			else
			{
				return false;
			}
		}
	}

	//flight-id
	CString	olFlti = ogBCD.GetField("WRO", "WNAM", opWro, "SHGN");
	if (!olFlti.IsEmpty())
	{
		if (olFlti != CString(prpFlightD->Flti))
		{
			if(popKonfList != NULL)
			{
				//Flight ID for Lounge %s must be %s.
				olConfText.Format(GetString(IDS_STRING2410), opWro, olFlti);
				AddToKonfList(prpFlightD, 'D', 0, ' ', IDS_STRING2410, olConfText, *popKonfList);
//				AddToKonfList(NULL, prpFlightD, 0, ' ', IDS_STRING2572, olConfText, *popKonfList);
				blRet = false;
			}
			else
			{
				return false;
			}
		}
	}


	//different Flight ID
// 	if(strlen(prlGatRes->Flti) > 0)
//	{
		CPtrArray olOverlapFlights;
		int ilCountGat = ogPosDiaFlightData.GetFlightsAtWro(olStartAlloc, olEndAlloc, opWro, prpFlightD, &olOverlapFlights);
		if (ilCountGat > 0)
		{
			olOverlapFlights.Add((void*) prpFlightD);
			bool blConflict = false;
			CString olFlightId ("");
			for (int i = 0; i < olOverlapFlights.GetSize(); i++)
			{
				DIAFLIGHTDATA *plFlight = (DIAFLIGHTDATA *)olOverlapFlights[i];
				if (plFlight && strlen(plFlight->Flti) > 0)
				{
					if (olFlightId.IsEmpty())
						olFlightId = CString(plFlight->Flti);
					else
					{
						if (olFlightId != CString(plFlight->Flti))
						{
							blConflict = true;
							break;
						}
					}
				}
			}
			if(popKonfList != NULL && blConflict)
			{
				for (int i = 0; i < olOverlapFlights.GetSize(); i++)
				{
					DIAFLIGHTDATA *plFlight = (DIAFLIGHTDATA *)olOverlapFlights[i];
					//Diffrent Flight ID for Lounge %s: %
					if( plFlight && strlen(plFlight->Flti) > 0)
					{
						olConfText.Format(GetString(IDS_STRING2301), opWro, plFlight->Flti);
//						AddToKonfList(NULL, plFlight, 0, 'D', IDS_STRING2301, olConfText, *popKonfList);
						AddToKonfList(plFlight, 'D', 0, ' ', IDS_STRING2301, olConfText, *popKonfList);
						blRet = false;
					}
				}
			}
			else
			{
				if (blConflict)
					return false;
			}
		}
//	}


	// Check nature ristriction from  nature table 
	if( !CString(prpFlightD->Ttyp).IsEmpty() )
	{
		if(FindInStrArray(omWroNatNoAllocList, prpFlightD->Ttyp))
//		if( omWroNatNoAllocList.Find(prpFlightD->Ttyp) >= 0)
		{
			if(popKonfList != NULL)
			{
				//Global nature ristriction (%s) at gate %s: %s
				olConfText.Format(GetString(IDS_STRING2338), omWroNatNoAllocListPlain,opWro, prpFlightD->Ttyp);
				AddToKonfList(NULL, prpFlightD, 0, ' ', IDS_STRING2338, olConfText, *popKonfList);
				blRet = false;
			}
			else
			{
				return false;
			}
		}
	}



	// Check max. number of wro ( overlapp )
	/*
	int ilCountWro = ogPosDiaFlightData.GetWroCount(prpFlightD, opWro);

	if( 1 < ilCountWro)
	{
		if(popKonfList != NULL)
		{
			//To many flights at lounge %s: %d Max: %d
			olConfText.Format(GetString(IDS_STRING1734), opWro, ilCountWro, 1);
			AddToKonfList(NULL, prpFlightD, 0, ' ', IDS_STRING1734, olConfText, *popKonfList);
			blRet = false;
		}
		else
		{
			return false;
		}
	}
	*/



	return blRet;
}



bool SpotAllocation::ReCheckAllChangedFlights(bool bpCheckPst, bool bpCheckGat,bool bpCheckWro, bool bpCheckBlt)
{

	if(CString(pcgHome) == "FCO")
	{
		return true;
	}


 	DiaCedaFlightData::RKEYLIST *prlRkey;
	DIAFLIGHTDATA *prlPosDiaFlightA = NULL;
 	DIAFLIGHTDATA *prlPosDiaFlightD = NULL;

	DIAFLIGHTDATA rlFlightA;
	DIAFLIGHTDATA rlFlightD;

	bool blArrChanged = false;
	bool blDepChanged = false;

	POSITION pos;
	void *pDummy;
	long llRkey;
	long llRemovedConflictsCount = 0; 
	for( pos = omChangedFlightsRkeyMap.GetStartPosition(); pos != NULL; )
	{
		omChangedFlightsRkeyMap.GetNextAssoc( pos, (void *&)llRkey , (void *&)pDummy );

		prlRkey = ogPosDiaFlightData.GetRotationByRkey(llRkey);

		prlPosDiaFlightA = ogPosDiaFlightData.GetFlightAInRotation(prlRkey);
		prlPosDiaFlightD = ogPosDiaFlightData.GetFlightDInRotation(prlRkey);

		if (prlPosDiaFlightA != NULL)
			rlFlightA = *prlPosDiaFlightA;
		if (prlPosDiaFlightD != NULL)
			rlFlightD = *prlPosDiaFlightD;

		blArrChanged = false;
		blDepChanged = false;

		if (bpCheckPst)
		{
			//check arrival position
			if (prlPosDiaFlightA != NULL)
			{
				DIAFLIGHTDATA* temp = NULL;
				if (omChangedUrnoMap.Lookup((void *)prlPosDiaFlightA->Urno,(void *& )temp) == TRUE)
				{
					if (!CheckPst(prlPosDiaFlightA, NULL, rlFlightA.Psta, NULL, &omParameters.omPstRightBuffer, &omParameters.omPstLeftBuffer))
					{
						rlFlightA.ClearAPos();
						blArrChanged = true;
						llRemovedConflictsCount++;
					}
				}
			}
			//check departure position
			if (prlPosDiaFlightD != NULL)
			{
				DIAFLIGHTDATA* temp = NULL;
				if (omChangedUrnoMap.Lookup((void *)prlPosDiaFlightD->Urno,(void *& )temp) == TRUE)
				{
					if (!CheckPst(NULL, prlPosDiaFlightD, rlFlightD.Pstd, NULL, &omParameters.omPstRightBuffer, &omParameters.omPstLeftBuffer))
					{
						rlFlightD.ClearDPos();
						blDepChanged = true;
						llRemovedConflictsCount++;
					}
				}
			}

			if (bgAutoAllocRemovePosIfDifferent)
			{
				if (prlPosDiaFlightA && prlPosDiaFlightD && (CString(rlFlightD.Pstd) != CString(rlFlightA.Psta)))
				{
						rlFlightA.ClearAPos();
						blArrChanged = true;
						rlFlightD.ClearDPos();
						blDepChanged = true;
				}
			}
			else
			{
				if (prlPosDiaFlightA && prlPosDiaFlightD && (CString(rlFlightD.Pstd) != CString(rlFlightA.Psta)))
				{
						DIAFLIGHTDATA* tempA = NULL;
						DIAFLIGHTDATA* tempD = NULL;
						if (omChangedUrnoMap.Lookup((void *)prlPosDiaFlightD->Urno,(void *& )tempD) && omChangedUrnoMap.Lookup((void *)prlPosDiaFlightA->Urno,(void *& )tempA))
						{
							rlFlightA.ClearAPos();
							blArrChanged = true;
							rlFlightD.ClearDPos();
							blDepChanged = true;
						}
				}
			}
		}
		if (bpCheckGat)
		{
			if (prlPosDiaFlightA != NULL)
			{
				// check first arrival gate
				if (!CheckGat(prlPosDiaFlightA, 'A', rlFlightA.Gta1, 1, NULL, &omParameters.omGatLeftBuffer, NULL))
				{
					rlFlightA.ClearAGat(1);
					blArrChanged = true;
					llRemovedConflictsCount++;
				}
				// check second arrival gate
				if (!CheckGat(prlPosDiaFlightA, 'A', rlFlightA.Gta2, 2, NULL, &omParameters.omGatLeftBuffer, NULL))
				{
					rlFlightA.ClearAGat(2);
					blArrChanged = true;
					llRemovedConflictsCount++;
				}
			}
			if (prlPosDiaFlightD != NULL)
			{
				// check first departure gate
				if (!CheckGat(prlPosDiaFlightD, 'D', rlFlightD.Gtd1, 1, NULL, NULL, &omParameters.omGatRightBuffer))
				{
					rlFlightD.ClearDGat(1);
					blDepChanged = true;
					llRemovedConflictsCount++;
				}
				// check second departure gate
				if (!CheckGat(prlPosDiaFlightD, 'D', rlFlightD.Gtd2, 2, NULL, NULL, &omParameters.omGatRightBuffer))
				{
					rlFlightD.ClearDGat(2);
					blDepChanged = true;
					llRemovedConflictsCount++;
				}
			}
		}
		if (bpCheckBlt)
		{
			if (prlPosDiaFlightA != NULL)
			{
				// check first baggage belt
				if (!CheckBlt(prlPosDiaFlightA, rlFlightA.Blt1, 1, NULL, 0))
				{
					rlFlightA.ClearBlt(1);
					blArrChanged = true;
					llRemovedConflictsCount++;
				}
				// check second baggage belt
				if (!CheckBlt(prlPosDiaFlightA, rlFlightA.Blt2, 2, NULL, 0))
				{
					rlFlightA.ClearBlt(2);
					blArrChanged = true;
					llRemovedConflictsCount++;
				}
			}
		}
		if (bpCheckWro)
		{
			if (prlPosDiaFlightD != NULL)
			{
				// check waiting room
				if (!CheckWro(prlPosDiaFlightD, rlFlightD.Wro1, 1, NULL))
				{
					rlFlightD.ClearWro(1);
					blDepChanged = true;
					llRemovedConflictsCount++;
				}
				if (!CheckWro(prlPosDiaFlightD, rlFlightD.Wro2, 2, NULL))
				{
					rlFlightD.ClearWro(2);
					blDepChanged = true;
					llRemovedConflictsCount++;
				}
			}
		}


		if (blArrChanged)
		{
			ogPosDiaFlightData.DeleteFromKeyMap(prlPosDiaFlightA);
			*prlPosDiaFlightA = rlFlightA;
			ogPosDiaFlightData.AddToKeyMap(prlPosDiaFlightA);
		}
 
		if (blDepChanged)
		{
			ogPosDiaFlightData.DeleteFromKeyMap(prlPosDiaFlightD);
			*prlPosDiaFlightD = rlFlightD;
			ogPosDiaFlightData.AddToKeyMap(prlPosDiaFlightD);
		}
	}

	TRACE("SpotAllocation::ReCheckAllChangedFlights: %ld Conflicts were removed!\n", llRemovedConflictsCount);

	return true;
}


/*
bool SpotAllocation::AutoBltAllocate()
{
	DiaCedaFlightData::RKEYLIST *prlRkey;
	DIAFLIGHTDATA *prlFlightA = NULL;

	pomParent->SendMessage(WM_SAS_PROGESS_INIT, omFlights.GetSize(), 0 );

	for(int k = omFlights.GetSize()-1; k >= 0 ; k--)
	{
		prlRkey = &omFlights[k];

		prlFlightA = ogPosDiaFlightData.GetFlightAInRotation(prlRkey);
	
		if (bgGATPOS)
		{
			AutoBltAllocateFlightGatPos(prlFlightA, 1);
			AutoBltAllocateFlightGatPos(prlFlightA, 2);
		}
		else
		{
			// Allocate first belt
			AutoBltAllocateFlight(prlFlightA, 1);
			// Allocate second belt
			AutoBltAllocateFlight(prlFlightA, 2);		
		}
 
		pomParent->SendMessage(WM_SAS_PROGESS_UPDATE, 1, 0 );
	}
	return true;
}
*/
bool SpotAllocation::AutoBltAllocate()
{
	omAutoAllocBlt.RemoveAll();

//############# only for test ##################################################
	char pclConfigPath[256];
	char pclExcelPath[256];
	char opTrenner[64];
	ofstream ofProt;
	CString olFileName;
	char pHeader[256];
	int ilwidth=1;

	if (bgCreateAutoAllocFiles)
	{
		if (getenv("CEDA") == NULL)
			strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
		else
			strcpy(pclConfigPath, getenv("CEDA"));

		GetPrivateProfileString(ogAppName, "Excel", "DEFAULT",
			pclExcelPath, sizeof pclExcelPath, pclConfigPath);

		GetPrivateProfileString(ogAppName, "ExcelSeparator", ";",
			opTrenner, sizeof opTrenner, pclConfigPath);

		olFileName = "AllocationProtokollBelts";

		strcpy (pHeader, CCSLog::GetTmpPath());
		CString path = pHeader;
		olFileName =  path + "\\" + olFileName + ".csv";

		ofProt.open( olFileName, ios::out);
	}
//############# only for test ##################################################

	
	DiaCedaFlightData::RKEYLIST *prlRkey;
	DIAFLIGHTDATA *prlFlightA = NULL;

	pomParent->SendMessage(WM_SAS_PROGESS_INIT, omFlights.GetSize(), 0 );

	bgAutoAllocWithGroundTime = false;//only allocate  if groundtime in rule matches
	bgAutoAllocWithExactParameter = true;//only allocate  if all parameters in rule matches

	int ilCount = 1;

	//do only the last round for a better equal distribution
	if (!bgBeltAllocReferExactParameters)
		ilCount = 2;

	for (int ilRound=ilCount; ilRound <3; ilRound++)
	{
		CString olRound;
		if (ilRound == 1)
		{
			olRound = "########### Round with Matching all Parameters  ############";
		}
		else if (ilRound == 2)
		{
			bgAutoAllocWithGroundTime = false;
			bgAutoAllocWithExactParameter = false;
			olRound = "########### Round without Matching all Parameters ############";
		}

		if (bgCreateAutoAllocFiles)
		{
			ofProt	<< endl << olRound << endl;

			ofProt  << setw(ilwidth) << "Prio"
				<< setw(1) << opTrenner
				<< setw(ilwidth) << "Sequence"
				<< setw(1) << opTrenner
				<< setw(ilwidth) << "Bnam"
				<< setw(1) << opTrenner
				<< setw(ilwidth) << "Good Count"
				<< setw(1) << opTrenner
				<< setw(ilwidth) << "Bad Count"
				<< setw(1) << opTrenner
				<< setw(ilwidth) << "Nature"
				<< setw(1) << opTrenner
				<< setw(ilwidth) << "Airline"
				<< setw(1) << opTrenner
				<< setw(ilwidth) << "Origin"
				<< setw(1) << opTrenner
				<< setw(ilwidth) << "Service Type"
				<< endl;
		}

		int ilStart = 1;
		if (!bgBeltAlocEqualDistribution)
			ilStart = imMaxFBelts;

		// for a better equal distribution (increment max flights for allocation, see checkblt())
		for (imActMaxF=ilStart; imActMaxF <= imMaxFBelts; imActMaxF++)
		{
			for(int k=0; k<omFlights.GetSize(); k++)
	//		for(int k = omFlights.GetSize()-1; k >= 0 ; k--)
			{
				prlRkey = &omFlights[k];
				prlFlightA = ogPosDiaFlightData.GetFlightAInRotation(prlRkey);

				if (bgGATPOS)
				{
					AutoBltAllocateFlightGatPos(prlFlightA, 1, ofProt);
					AutoBltAllocateFlightGatPos(prlFlightA, 2, ofProt);
				}
				else
				{
					// Allocate first belt
					AutoBltAllocateFlight(prlFlightA, 1);
					// Allocate second belt
					AutoBltAllocateFlight(prlFlightA, 2);		
				}
 
				pomParent->SendMessage(WM_SAS_PROGESS_UPDATE, 1, 0 );
			}
		}
	}

//############# only for test ##################################################
	ofProt.close();

	if (bgCreateAutoAllocFiles)
	{
		PrintSortedRulesBlt();
	}
//############# only for test ##################################################

	imActMaxF = 1;
	return true;
}

bool SpotAllocation::AutoBltAllocateFlightGatPos(DIAFLIGHTDATA *prlFlightA, int ipBltNo, ofstream& of)
{
	if (prlFlightA == NULL)
		return false;

	CString olGate;
	if (ipBltNo == 1)
	{
		if (strlen(prlFlightA->Blt1) > 0)
			return true;

		olGate = CString(prlFlightA->Gta1);
	}
	else if (ipBltNo == 2)
	{
		if (strlen(prlFlightA->Blt2) > 0)
			return true;

		olGate = CString(prlFlightA->Gta2);
//		if (olGate.IsEmpty())
		if (olGate.IsEmpty() && CString(prlFlightA->Flti) != "M")//otherwise the second allocation will be blocked for "M"
			return false;
	}
	else
		return false;



//	only allocate if a gate is set (ama while training in haj)
	// but yet it should be possible to allocate belts without resourcechain
	if (!bgBeltAlocWithoutGates && olGate.IsEmpty())
		return false;


	CString olBlt;
	if (GetBestBeltGatPos(prlFlightA, olBlt, ipBltNo, of))
	{
		AllocateBltGatPos(prlFlightA, olBlt, ipBltNo);
	}

	return true;
}


bool SpotAllocation::GetBestBeltGatPos(DIAFLIGHTDATA *prpFlight, CString &opBestBelt, int ipBeltNo, ofstream& of)
{

	// TSC 0909
		CCSPtrArray<BLT_RESLIST> olPossibleBltUser;
		bool bbltuser = 1 ; // NORMAL
	// END TSC
	
	if (prpFlight == NULL)
		return false;

	if (ipBeltNo != 1 && ipBeltNo != 2)
		return false;

	opBestBelt.Empty();
	
	CStringArray olPossibleBlt;

	if (!bgBeltAlocWithoutGates)
	{
		if (!GetGATPOSPossibleBelts(prpFlight, olPossibleBlt, ipBeltNo))
			return false;
	}
	else
		GetBeltsByRule(prpFlight, olPossibleBlt);
	
	omBltRules.Sort(CompareBltResByPrio);

	// collect all possible belts
//	CPtrArray olPossibleBeltRules;	
	CCSPtrArray<BLT_RESLIST> olPossibleBeltRules;
	CString olCurrBelt;
	BLT_RESLIST *prlBltRes;

	for( int i = 0; i < olPossibleBlt.GetSize(); i++)
	{
		olCurrBelt = olPossibleBlt.GetAt(i);
		if(CheckBlt(prpFlight, olCurrBelt, ipBeltNo, NULL, 0))
		{
			prlBltRes = GetBltRes(olCurrBelt);
			if (prlBltRes != NULL)
				olPossibleBeltRules.Add(prlBltRes);
		}
	}

	if (olPossibleBeltRules.GetSize() < 1) 
		return false;

	olPossibleBeltRules.Sort(CompareBltResByPrio);

// TSC 0909 BLT

if(oguseralloctepath!="<Default>" & oguseralloctepath!="")
	{
    		    Userallocatevector(oguseralloctepath);
	              
				std::vector <CString> vDataBLT;

					bool accesstab   = 0;
					bool bnextdata   = 0;
										
				for (int i =0; i< vData.size(); i++)
				{
						
							// ONLY BLT
								CString cstrdata=vData[i];
								CString strf="END_GAT\n";
								CString strg="END_BLT\n";
								if(strf==cstrdata)accesstab = 1;
								if(strg==cstrdata)accesstab = 0;
								
								if(accesstab)
								{
						        // THE NEXT DS
								   if(bnextdata)
								   {
						std::string checkedtrue = LPCTSTR(cstrdata);
						
						int r = checkedtrue.find(';');
						
						std::string strgaturno = checkedtrue.substr(0,r);
																// FOR ;          					
						std::string checked = checkedtrue.substr(r+1,checkedtrue.length());
						
						int z= 23;
									if(checked=="1\n")
									{
									vDataBLT.push_back(strgaturno.c_str());
									}
								

							       }//bnextdata

								 bnextdata =1;// NO END_BLT


								}//accesstab 

								
				}//	for (int i =0; i< vData.size(); i++)


	int counter = vDataBLT.size();

  	BLT_RESLIST *prlBltResUser;
   
	for( int m= 0; m < olPossibleBeltRules.GetSize(); m++)
	{
	
		prlBltResUser =  &olPossibleBeltRules[m];
 				   
		BLT_RESLIST *prlBltResActl= new (BLT_RESLIST);

	 	
		if(!prlBltResUser->Urno.IsEmpty())
		{
						for( int j = 0 ; j < vDataBLT.size() ; j++)
						{
								
							if(prlBltResUser->Urno == vDataBLT[j])
							{
							prlBltResActl->Urno     = prlBltResUser->Urno;
							prlBltResActl->Bnam     = prlBltResUser->Bnam;
							prlBltResActl->Prio     = counter-j;
							prlBltResActl->Sequence = 0;  
						    
							olPossibleBltUser.Add(prlBltResActl);

							}//if(prlBltResActl->Urno == vDataBLT[j])

						}//for( j = 0 ; j < vDataBLT.size() ; j++)
	
		}//if(prlBltResUser->Urno.IsEmpty())

	}//for( int m= 0; m < olPossibleBeltRules; m++)


			if (olPossibleBltUser.GetSize() < 1) 
					return false;
			else
				bbltuser = 0;

				olPossibleBltUser.Sort(CompareBltResByPrio);

 				
				vDataBLT.clear();

}
// END TSC

//############# only for test ##################################################
	char opTrenner[64];
	strcpy(opTrenner,";");
	int ilwidth = 1;
	CString ol1;
	CString ol2;
	CString ol3;
	CString ol4;
	CStringArray olArray;
	char clBuf[255];
	CString olFlno;

	GetFlnoForPrintMap(prpFlight, olFlno);
	if (bgCreateAutoAllocFiles)
	{
//		olFlno = prpFlight->Flno;
//		GetFlnoForPrintMap(prpFlight, olFlno);

		of.setf(ios::left, ios::adjustfield);
		of  << "-----------------------------------------------------------------"
			<< endl;
		of  << olFlno
			<< endl;
		of  << "-----------------------------------------------------------------"
			<< endl;

	}
//############# only for test ##################################################


	BLT_RESLIST *prlBestBltRule = NULL;
	float flBestGoodCount = 0.0;
	int ilBestBadCount = 0;
	CString olMatchStr;

	// TSC 0909
	int inumberrules = 0;
	if(bbltuser) 
		       inumberrules = olPossibleBeltRules.GetSize();
	else
			   inumberrules = olPossibleBltUser.GetSize();
     

	for (i = 0; i < inumberrules ; i++)
	{
		float flCurrGoodCount = 0.0;
		int ilCurrBadCount = 0;
//		prlBltRes = (BLT_RESLIST *) olPossibleBeltRules[i];
		
		// TSC 
		if(	bbltuser)
			prlBltRes = &olPossibleBeltRules[i];
		else 
		{
		    prlBltRes = &olPossibleBltUser[i];
	        prlBltRes = GetBltRes(prlBltRes->Bnam);
		}
		// END TSC

		//now it is the same check for belt and gate!!
//		GetMatchCountGat(prpFlight, prlBltRes->PreferredAL, prlBltRes->PreferredOrgDes, prlBltRes->PreferredNA, prlBltRes->PreferredService, flCurrGoodCount, ilCurrBadCount, olMatchStr);
		GetMatchCountGat(prpFlight, prlBltRes, flCurrGoodCount, ilCurrBadCount, olMatchStr);

		// Is the examined belt better?
		if (	(ilCurrBadCount < ilBestBadCount)
			||	(ilCurrBadCount == ilBestBadCount && flCurrGoodCount > flBestGoodCount)
			||	i == 0)
		{
			ilBestBadCount = ilCurrBadCount;
			flBestGoodCount = flCurrGoodCount;
			prlBestBltRule = prlBltRes;
		}



//############# only for test ##################################################
		if (bgCreateAutoAllocFiles)
		{
			sprintf(clBuf, "%f", flCurrGoodCount);

			ExtractItemList(olMatchStr, &olArray, ';');
			if (olArray.GetSize() == 4)
			{
				ol1 = olArray.GetAt(0);
				ol2 = olArray.GetAt(1);
				ol3 = olArray.GetAt(2);
				ol4 = olArray.GetAt(3);
			}

			of  << setw(ilwidth) << " " << prlBltRes->Prio
				<< setw(1) << opTrenner
				<< setw(ilwidth) << " " << prlBltRes->Sequence
				<< setw(1) << opTrenner
				<< setw(ilwidth) << " " << prlBltRes->Bnam
				<< setw(1) << opTrenner
				<< setw(ilwidth) << " " << clBuf
				<< setw(1) << opTrenner
				<< setw(ilwidth) << " " << ilCurrBadCount
				<< setw(1) << opTrenner
				<< setw(ilwidth) << " " << ol1
				<< setw(1) << opTrenner
				<< setw(ilwidth) << " " << ol2
				<< setw(1) << opTrenner
				<< setw(ilwidth) << " " << ol3
				<< setw(1) << opTrenner
				<< setw(ilwidth) << " " << ol4
				<< endl;
		}
//############# only for test ##################################################

		// allocate the first rule
		if (bgAutoAllocFirstRule)
			break;

	}

	// return the best belt
	if (prlBestBltRule != NULL)
	{
		opBestBelt = prlBestBltRule->Bnam;
		omAutoAllocBlt.SetAt(olFlno,opBestBelt);



//############# only for test ##################################################
		if (bgCreateAutoAllocFiles)
		{
			of  << endl
				<< "Allocated Belt : !!!!  " << prlBestBltRule->Bnam << "  !!!!"
				<< endl;
		}
//############# only for test ##################################################

    // TSC
	olPossibleBltUser.DeleteAll();

		return true;
	} 

	// TSC
	olPossibleBltUser.DeleteAll();

	return false;
}

bool SpotAllocation::GetGATPOSPossibleBelts(DIAFLIGHTDATA *prlFlightA, CStringArray &opPossibleBlt, int ipBeltNo)
{
	if (!prlFlightA)
		return false;

	CStringArray ropBLUE;
	CMapStringToString ropMapBLUE;
	CStringArray ropRED;
	CMapStringToString ropMapRED;

	CString olGate = prlFlightA->Gta1;
	if (olGate.IsEmpty())
		olGate = prlFlightA->Gta2;

//	only allocate if a gate is set (ama while training in haj)
	if (!bgBeltAlocWithoutGates && olGate.IsEmpty())
		return false;

	if (GetResBy("BLT", "GAT", olGate,ropBLUE,ropMapBLUE,ropRED,ropMapRED, prlFlightA))
	{
		CString olExt;
		if(ipBeltNo == 1)
			olExt = CString(prlFlightA->Ext1);
		else
			olExt = CString(prlFlightA->Ext2);

		olExt = "";


		CString olKey;
		CString olValue;

		for(int i = 0; i < ropBLUE.GetSize(); i++)
		{
			CString olValue;
			CString olKey = ropBLUE.GetAt(i);
			if (ropMapBLUE.Lookup(olKey, olValue))// is in resource chain
			{
				if (olValue == "IDB_LIGHTBLUE")
				{
					if (!olExt.IsEmpty())
					{
						CStringArray ropBLUEexit;
						CMapStringToString ropMapBLUEexit;
						CStringArray ropREDexit;
						CMapStringToString ropMapREDexit;
						if (GetResBy("BLT", "EXT", olExt,ropBLUEexit,ropMapBLUEexit,ropREDexit,ropMapREDexit, prlFlightA))
						{
							if (ropMapBLUEexit.Lookup(olKey, olValue))// is in resource chain
							{
//								if (olValue == "IDB_LIGHTBLUE")
									opPossibleBlt.Add(olKey);
							}
						}
					}
					else
						opPossibleBlt.Add(olKey);
				}
			}
		}
	}
	else
		return false;

	return true;
}


bool SpotAllocation::AllocateBltGatPos(DIAFLIGHTDATA *prlFlightA, CString opBlt, int ipBelt)
{
//	only allocate if a belt is set (ama while training in haj)
	if (opBlt.IsEmpty())
		return false;

	if(prlFlightA != NULL)
	{
		CStringArray ropBLUE;
		CMapStringToString ropMapBLUE;
		CStringArray ropRED;
		CMapStringToString ropMapRED;

		CString olExt;
		if(ipBelt == 1)
			olExt = CString(prlFlightA->Ext1);
		else
			olExt = CString(prlFlightA->Ext2);

		olExt = "";

		if (olExt.IsEmpty())
		{
			if (GetResBy("EXT", "BLT", opBlt,ropBLUE,ropMapBLUE,ropRED,ropMapRED, prlFlightA))
			{
				for(int i = 0; i < ropBLUE.GetSize(); i++)
				{
					CString olValue;
					CString olKey = ropBLUE.GetAt(i);
					if (ropMapBLUE.Lookup(olKey, olValue))// is in resource chain
					{
						if (olValue == "IDB_LIGHTBLUE") //get no conf belt-exit relation
						{
							olExt = olKey;
							break;
						}
					}
				}
			}
		}
		else
		{
			if (GetResBy("BLT", "EXT", olExt,ropBLUE,ropMapBLUE,ropRED,ropMapRED, prlFlightA))
			{
				CString olTmp;
				if ( ropMapBLUE.Lookup(opBlt,olTmp) )
				{
					if (olTmp == "IDB_LIGHTBLUEBLOCKED") // look if this belt is valid for existing exit
						return false;
				}
				else
					return false;
			}
		}

		ogPosDiaFlightData.DeleteFromKeyMap(prlFlightA);
		if(ipBelt == 1)
		{
			strcpy(prlFlightA->Blt1, opBlt);
			if (!bgKeepExisting)
				strcpy(prlFlightA->Ext1, olExt);
			ogPosDiaFlightData.GetBestBltTime(prlFlightA, prlFlightA->Blt1, 1, true, prlFlightA->B1bs);
			ogPosDiaFlightData.GetBestBltTime(prlFlightA, prlFlightA->Blt1, 1, false, prlFlightA->B1es);
		}
		else
		{
			strcpy(prlFlightA->Blt2, opBlt);
			if (!bgKeepExisting)
				strcpy(prlFlightA->Ext2, olExt);
			ogPosDiaFlightData.GetBestBltTime(prlFlightA, prlFlightA->Blt2, 2, true, prlFlightA->B2bs);
			ogPosDiaFlightData.GetBestBltTime(prlFlightA, prlFlightA->Blt2, 2, false, prlFlightA->B2es);
		}

		SetTerminals(prlFlightA, CString("BLT"));
		if (!bgKeepExisting)
			SetTerminals(prlFlightA, CString("EXT"));

		ogPosDiaFlightData.AddToKeyMap(prlFlightA);
		omChangedFlightsRkeyMap.SetAt((void *)prlFlightA->Rkey, NULL);
	}
	return true;
}


bool SpotAllocation::AutoBltAllocateFlight(DIAFLIGHTDATA *prlFlightA, int ipBltNo)
{
	if (prlFlightA == NULL)
		return false;
	
	if (ipBltNo == 1)
	{
		if (strlen(prlFlightA->Blt1) > 0)
			return true;
	}
	else if (ipBltNo == 2)
	{
		if (strlen(prlFlightA->Blt2) > 0)
			return true;
	}
	else
		return false;

  	
	// Only allocate a second baggage belt if the flight has a second arrival gate!
	if (ipBltNo == 2 && strlen(prlFlightA->Gta2) < 1)
		return false;


	CStringArray olGatBlts;
	if (!GetRelatedBlts(prlFlightA, ipBltNo, olGatBlts))
		return false;


	// At first try to allocate the flight to a free baggage belt
	for(int i = 0; i < olGatBlts.GetSize(); i++)
	{	
		if(CheckBlt(prlFlightA, olGatBlts[i], ipBltNo, NULL, 1))
		{
			AllocateBlt(prlFlightA, olGatBlts[i], ipBltNo);
			return true;
		}
	}
		
	// if no free one was found, try to allocate a occupied baggage belt which allow multiple allocations 		
	for(i = 0; i < olGatBlts.GetSize(); i++)
	{	
		if(CheckBlt(prlFlightA, olGatBlts[i], ipBltNo, NULL, 0))
		{
			AllocateBlt(prlFlightA, olGatBlts[i], ipBltNo);
			return true;
		}
	}

	

	return false;
}





void SpotAllocation::SetGatSort(CString opGat)
{
							
	GAT_RESLIST *prlGatRes = NULL;

	int ilCurrPrio = 0;

	for(int i = 0; i < omGatRules.GetSize(); i++)
	{
		if(omGatRules[i].Gnam == opGat)
		{
			prlGatRes = &omGatRules[i];
			ilCurrPrio = omGatRules[i].Prio;
			omGatRules.RemoveAt(i);
			break;

		}
	}

	bool blTreffer = false;
	for(int j = i; j < omGatRules.GetSize(); j++)
	{
		if(omGatRules[j].Prio != ilCurrPrio)
		{
			omGatRules.InsertAt(j, prlGatRes);
			blTreffer = true;
			break;
		}
	}

	if(!blTreffer)
	{
		omGatRules.InsertAt(omGatRules.GetSize(), prlGatRes);
	}
}




bool SpotAllocation::CheckBlt(DIAFLIGHTDATA *prpFlightA, CString opBlt, int ipBltNo, CCSPtrArray<KonfItem> *popKonfList, int ipMaxCount, bool bpPreCheck)
{

	if(opBlt.IsEmpty() || prpFlightA == NULL || (ipBltNo != 1 && ipBltNo != 2))
		return true;

	bool blRet = true;
	CString olConfText;
	CTime olStartAlloc;
	CTime olEndAlloc;
	
	if (!ogPosDiaFlightData.GetBltAllocTimes(*prpFlightA, opBlt, ipBltNo, olStartAlloc, olEndAlloc, bpPreCheck))
		return false;

	// Check terminal ristriction 
	if(bgTermRestrForAl)
	{
		CString olTerb;
		ogBCD.GetField("BLT", "BNAM", opBlt, "TERM", olTerb);
		CString olAltTerb = ogBCD.GetFieldExt("ALT","ALC2","ALC3",prpFlightA->Alc2,prpFlightA->Alc3,"TERB");
		if( !olAltTerb.IsEmpty() && !olTerb.IsEmpty())
		{
			if (olAltTerb.Find(olTerb) < 0)
			{
				if(popKonfList != NULL)
				{
					// Terminal Belt %s: %s must be %s
					olConfText.Format(GetString(IDS_STRING2724), opBlt, olTerb, olAltTerb);
					AddToKonfList(prpFlightA, (DIAFLIGHTDATA *)NULL, 0, ' ', IDS_STRING2723, olConfText, *popKonfList);
					blRet = false;					
				}
				else
				{
					return false;
				}
			}
		}
	}


	//// Check max. number of blt ( overlapp )
	int ilPax = 0;
	int ilBag = 0;
	int ilCountOverlappingBlt = ogPosDiaFlightData.GetMaxOverlapAtBlt(olStartAlloc, olEndAlloc, opBlt, prpFlightA, ilPax, ilBag);


	// get max count
	int ilMaxCount = 0;
	if(ipMaxCount != 0)
	{
		ilMaxCount = ipMaxCount;
	}
	else
	{
		ilMaxCount = atoi( ogBCD.GetField("BLT", "BNAM", opBlt, "MAXF"));

		if(ilMaxCount == 0)
			ilMaxCount = 1;
	}

	if (bgPosDiaAutoAllocateInWork && bgBeltAlocEqualDistribution)
		ilMaxCount = min(ilMaxCount, imActMaxF);
//		ilMaxCount = imActMaxF;


	if( ilMaxCount < ilCountOverlappingBlt)
	{
		if(popKonfList != NULL)
		{
			//To many aircrafts at baggage belt %s: %d Max: %d
			olConfText.Format(GetString(IDS_STRING1736), opBlt, ilCountOverlappingBlt, ilMaxCount);
			AddToKonfList(prpFlightA, (DIAFLIGHTDATA *)NULL , 0, ' ', IDS_STRING1736, olConfText, *popKonfList);
			blRet = false;
		}
		else
		{
			return false;
		}
	}

	// Check nature ristriction from  nature table 
	if( !CString(prpFlightA->Ttyp).IsEmpty() )
	{
		if(FindInStrArray(omBltNatNoAllocList, prpFlightA->Ttyp))
//		if( omBltNatNoAllocList.Find(prpFlightA->Ttyp) >= 0)
		{
			if(popKonfList != NULL)
			{
				//Global nature ristriction (%s) at gate %s: %s
				olConfText.Format(GetString(IDS_STRING2339), omBltNatNoAllocListPlain,opBlt, prpFlightA->Ttyp);
				AddToKonfList(prpFlightA, (DIAFLIGHTDATA *)NULL , 0, ' ', IDS_STRING2339, olConfText, *popKonfList);
//				AddToKonfList(prpFlight, cpFPart, 0, ' ', IDS_STRING1732, olConfText, *popKonfList);
				blRet = false;
			}
			else
			{
				return false;
			}
		}
	}


	BLT_RESLIST *prlBltRes = GetBltRes(opBlt); 
	if(prlBltRes == NULL)
		return blRet;


	CStringArray ropBLUE;
	CMapStringToString ropMapBLUE;
	CStringArray ropRED;
	CMapStringToString ropMapRED;

	if(!bgBeltAlocWithoutGates)
	{
		if ( strlen(prpFlightA->Gta1) > 0 && opBlt.GetLength() > 0 )
		{
			if (GetResBy("BLT", "GAT", CString(prpFlightA->Gta1),ropBLUE,ropMapBLUE,ropRED,ropMapRED, prpFlightA, true, true, false, true))
			{
				CString olTmp;
				if (!ropMapBLUE.Lookup(opBlt, olTmp))
				{
					if(popKonfList != NULL)
					{
						olConfText = GetString(IDS_STRING2304);
						AddToKonfList(prpFlightA, (DIAFLIGHTDATA *)NULL , 0, ' ', IDS_STRING2304, olConfText , *popKonfList);
						blRet = false;
					}
					else
					{
						return false;
					}
				}
			}
		}
	}

	ropBLUE.RemoveAll();
	ropRED.RemoveAll();
	ropMapRED.RemoveAll();
	ropMapBLUE.RemoveAll();




	// Check Blt availability
	CStringArray opNafr;
	CStringArray opNato;

	for (int il = 0; il < prlBltRes->BlkData.GetSize(); il++)
	{
		CTime olNafr = prlBltRes->BlkData[il].StartTime;
		CTime olNato = prlBltRes->BlkData[il].EndTime;


		if(olNafr != TIMENULL || olNato != TIMENULL)
		{	
			if(olNafr == TIMENULL)
				olNafr = CTime(1971,1,1,0,0,0);
			if(olNato == TIMENULL)
				olNato = CTime(2020,1,1,0,0,0);
		}

		if((olNafr != TIMENULL) && (olNato != TIMENULL))
		{
			if(IsReallyOverlapped(olStartAlloc, olEndAlloc, olNafr, olNato)/*, olStartAlloc, olEndAlloc) || !IsWithIn(olStartAlloc, olEndAlloc, olNafr, olNato)*/)
			{
				if(popKonfList != NULL)
				{
					if(bgGatPosLocal) ogBasicData.UtcToLocal(olNafr);
					if(bgGatPosLocal) ogBasicData.UtcToLocal(olNato);
					//Gate %s not available %s - %s reason: %s
					olConfText.Format(GetString(IDS_STRING1735), olNafr.Format("%H:%M %d.%m.%y"), olNato.Format("%H:%M %d.%m.%y"),  prlBltRes->BlkData[il].Text);
					AddToKonfList(prpFlightA, (DIAFLIGHTDATA *)NULL , 0, ' ', IDS_STRING1735, olConfText, *popKonfList);

					blRet = false;
				}
				else
				{
					return false;
				}
			}
		}
	}



	//max Bag
	//long ilBag = prpFlightA->Bags;


	if(ilBag > 0 && prlBltRes->Maxb > 0)
	{
		if(prlBltRes->Maxb < ilBag)
		{
			if(popKonfList != NULL)
			{
				olConfText.Format(GetString(IDS_STRING2945), opBlt, ilBag, prlBltRes->Maxb);
				AddToKonfList(prpFlightA, (DIAFLIGHTDATA *)NULL, 0, ' ', IDS_STRING2945, olConfText, *popKonfList);
				blRet = false;
			}
			else
			{
				return false;
			}
		}
	}
	else
	{
		//max Pax
		int ilPaxt = atoi(prpFlightA->Pax2);

		if(prlBltRes->Maxp > 0)
		{
			if(prlBltRes->Maxp < ilPax)
			{
				if(popKonfList != NULL)
				{
					//To many aircrafts at baggage belt %s: %d Max: %d
					//Max Passenger of Gate %s: %s (%s)
					//olConfText.Format(GetString(IDS_STRING2136), opBlt, prlBltRes->Maxp, prpFlightA->Paxt);
	//				olConfText.Format(GetString(IDS_STRING2136), opBlt, prlBltRes->Maxp, ilPax);
					olConfText.Format(GetString(IDS_STRING2634), opBlt, ilPax, prlBltRes->Maxp);
					AddToKonfList(prpFlightA, (DIAFLIGHTDATA *)NULL, 0, ' ', IDS_STRING2634, olConfText, *popKonfList);
					blRet = false;
				}
				else
				{
					return false;
				}
			}
		}
	}


	// Check airline restrictions 
	if(prlBltRes->PreferredAL.GetSize() > 0 || prlBltRes->ExcludedAL.GetSize() > 0 || prlBltRes->AssignedAL.GetSize() > 0)
	{
		if (CheckResToResLists(prpFlightA->Alc2, prpFlightA->Alc3, prlBltRes->AssignedAL, prlBltRes->ExcludedAL, prlBltRes->PreferredAL) == false)
		{
			if(popKonfList != NULL)
			{
				// Gate %s: Airline restriction (%s): %s
				olConfText.Format(GetString(IDS_STRING2635), opBlt, prlBltRes->ALUserStr, CString(prpFlightA->Alc2)+"/"+CString(prpFlightA->Alc3));
				AddToKonfList(prpFlightA, (DIAFLIGHTDATA *)NULL, 0, ' ', IDS_STRING2635, olConfText, *popKonfList);
				blRet = false;
			}
			else
			{
				return false;
			}
		}
	}


	// Check org restrictions 
	if(prlBltRes->PreferredOrgDes.GetSize() > 0 || prlBltRes->ExcludedOrgDes.GetSize() > 0 || prlBltRes->AssignedOrgDes.GetSize() > 0)
	{
		if (CheckResToResLists(prpFlightA->Org3, prpFlightA->Org4, prlBltRes->AssignedOrgDes, prlBltRes->ExcludedOrgDes, prlBltRes->PreferredOrgDes) == false)
		{
			if(popKonfList != NULL)
			{
				// Gate %s: Airline restriction (%s): %s
				olConfText.Format(GetString(IDS_STRING2636), opBlt, prlBltRes->OrgDesUserStr, CString(prpFlightA->Org3)+"/"+CString(prpFlightA->Org4));
				AddToKonfList(prpFlightA, (DIAFLIGHTDATA *)NULL, 0, ' ', IDS_STRING2636, olConfText, *popKonfList);
				blRet = false;
			}
			else
			{
				return false;
			}
		}
	}


	// Check nature ristriction 
	if(prlBltRes->PreferredNA.GetSize() > 0 || prlBltRes->ExcludedNA.GetSize() > 0 || prlBltRes->AssignedNA.GetSize() > 0)
	{
		if( strlen(prpFlightA->Ttyp) > 0 )
		{
			if (CheckResToResLists(prpFlightA->Ttyp, prlBltRes->AssignedNA, prlBltRes->ExcludedNA, prlBltRes->PreferredNA) == false)
			{
				if(popKonfList != NULL)
				{
					// Gate %s: Nature restriction (%s): %s
					olConfText.Format(GetString(IDS_STRING2637), opBlt, prlBltRes->NAUserStr, prpFlightA->Ttyp);
					AddToKonfList(prpFlightA, (DIAFLIGHTDATA *)NULL, 0, ' ', IDS_STRING2637, olConfText, *popKonfList);
					blRet = false;					
				}
				else
				{
					return false;
				}
			}
		}
	}

	// Check service ristriction
	if(prlBltRes->PreferredService.GetSize() > 0 || prlBltRes->ExcludedService.GetSize() > 0 || prlBltRes->AssignedService.GetSize() > 0)
	{
		if( strlen(prpFlightA->Styp) > 0 )
		{
			if (CheckResToResLists(prpFlightA->Styp, prlBltRes->AssignedService, prlBltRes->ExcludedService, prlBltRes->PreferredService) == false)
			{
				if(popKonfList != NULL)
				{
					// Belt %s: service ristriction (%s): %s
					olConfText.Format(GetString(IDS_STRING2638), opBlt, prlBltRes->ServiceUserStr, prpFlightA->Styp);
					AddToKonfList(prpFlightA, (DIAFLIGHTDATA *)NULL, 0, ' ', IDS_STRING2638, olConfText, *popKonfList);
					blRet = false;					
				}
				else
				{
					return false;
				}
			}
		}
	}

	// Check aircraft restrictions
	if(prlBltRes->PreferredAC.GetSize() > 0 || prlBltRes->ExcludedAC.GetSize() > 0 || prlBltRes->AssignedAC.GetSize() > 0)
	{
		if (CheckResToResLists(prpFlightA->Act3, prpFlightA->Act5, prlBltRes->AssignedAC, prlBltRes->ExcludedAC, prlBltRes->PreferredAC) == false)
		{
			if(popKonfList != NULL)
			{
				// Belt %s: Aircraft restriction (%s): %s
				olConfText.Format(GetString(IDS_STRING2797), opBlt, prlBltRes->ACUserStr + prlBltRes->ACGroupUserStr, CString(prpFlightA->Act3)+"/"+CString(prpFlightA->Act5));
				AddToKonfList(prpFlightA, (DIAFLIGHTDATA *)NULL, 0, ' ', IDS_STRING2797, olConfText, *popKonfList);
				blRet = false;
			}
			else
			{
				return false;
			}
		}
	}


/*
	// flighid
	CMapStringToString olGatesMapRED;
	CMapStringToString olGatesMapBLUE;
	CStringArray olGatesBLUE;
	CStringArray olGatesRED;

	ogSpotAllocation.GetResBy("GAT", "BLT", olCurBlt, olGatesBLUE, olGatesMapBLUE, olGatesRED, olGatesMapRED, prpFlightA)
	GAT_RESLIST *prlGatRes;
	CString olCurrGate;
	for( int i = 0; i < olGatesBLUE.GetSize(); i++)
	{
		olCurrGate = olGatesBLUE.GetAt(i);
		prlGatRes = GetGatRes(olCurrGate);

		//to be continued ........

*/
/*
rkr:26.04.05 with the following code the autoallocation for belts didn't work (not compatible with 4.5)!!!

rkr and ama: belts can be allocate via the resourcechain because the consideration of the
fightid "M" is checked within gates, so the possible belts are set in the resourcechain from the gates
(this conflict for gateid can be set in the setup also visible within the belts).

If belts have been allocated standaolne (cead.ini::BELT_ALLOC_WITHOUT_GATES = TRUE) the relation conflict gate->belt
won't be generated.

If in this case the flightid "M" should be considered within the belts the possible beltallocations 
must be considered with the possible gateid combination within the resourcechain(bachwards from the belt to the possible gates).
This shouldn't be done yet.
*/


//MWO: 19.04.05: Mixed Flights checks ==================================
	if (bgConflictBeltFlti)
	{

		CString olAdidText;

		CString olFlti = CString(prpFlightA->Flti);
	//	olFlti = "M";

		CString olBlt1, olBlt2;
		if (ipBltNo == 2 )
		{
			olBlt1 = CString(prpFlightA->Blt1);
			olBlt2 = opBlt;
		}

		if (ipBltNo == 1 )
		{
			olBlt2 = CString(prpFlightA->Blt2);
			olBlt1 = opBlt;
		}

		if (bgPosDiaAutoAllocateInWork && olBlt1 == olBlt2)
			return false;


		if(olFlti != "M" && ipBltNo == 2)
			return false;

		bool firstBelt = false;
		if (olBlt2.IsEmpty() || olBlt1.IsEmpty())
			firstBelt = true;


//		if(olFlti != "M" || (olFlti == "M" && ipBltNo == 1))
		if(olFlti != "M" || (olFlti == "M" && firstBelt))
		{
	//END MWO: 19.04.05: Mixed Flights checks ==============================


			// Flight ID
 			if(strlen(prlBltRes->Flti) > 0)
			{
				if(strlen(olFlti) > 0)
				{
					CString olFlightId = CString(prpFlightA->Flti);

					CString olBeltIDs = CString(prlBltRes->Flti);

					if(!olBeltIDs.IsEmpty() && olBeltIDs.Find( CString(olFlti)) < 0)
					{
						if(popKonfList != NULL)
						{
							//Flight ID for Belt %s: % must be %s 
							//olConfText.Format(GetString(IDS_STRING1725), opGat, prpFlight->Flti, prlGatRes->Flti);
							olConfText.Format(GetString(IDS_STRING2722), opBlt, olBeltIDs, olFlti);
							AddToKonfList(prpFlightA, (DIAFLIGHTDATA *)NULL, 0, ' ', IDS_STRING2721, olConfText, *popKonfList);

							blRet = false;
						}
						else
						{
							return false;
						}
					}
				}
			}
 
		}

	//MWO: 19.04.05: Mixed Flights checks ==============================
		if(olFlti == "M")
		{
			CString olBltResFlti1;
			CString olBltResFlti2;

			if(!olBlt1.IsEmpty() && !olBlt2.IsEmpty() )
			{
				prlBltRes = GetBltRes(olBlt1); 
				if(prlBltRes != NULL)
				{
					olBltResFlti1 = CString(prlBltRes->Flti);
				}

				prlBltRes = GetBltRes(olBlt2); 
				if(prlBltRes != NULL)
				{
					olBltResFlti2 = CString(prlBltRes->Flti);
				}

				if( !olBltResFlti2.IsEmpty() && !olBltResFlti1.IsEmpty() )
				{

					if( !( (olBltResFlti1.Find("MI") >= 0 && olBltResFlti2.Find("D") >= 0) || (olBltResFlti1.Find("MD") >= 0 && olBltResFlti2.Find("I") >= 0) ) )
					{
						if(popKonfList != NULL)
						{
							//Flight ID for Gate %s: % must be %s 
							//olConfText.Format(GetString(IDS_STRING1725), opGat, prlGatRes->FltiDep, olFlti);
							olConfText.Format(GetString(IDS_STRING2722), olBlt1+"/"+olBlt2, olBltResFlti1+"/"+olBltResFlti2, olFlti);
							AddToKonfList(prpFlightA, (DIAFLIGHTDATA *)NULL, 0, ' ', IDS_STRING2721, olConfText, *popKonfList);

							blRet = false;
						}
						else
						{
							return false;
						}
					}
				}
			}
			else
			{
				prlBltRes = GetBltRes(opBlt); 
				if(prlBltRes != NULL)
				{
					olBltResFlti1 = CString(prlBltRes->Flti);
				}

				if( !(olBltResFlti1.Find("MI") || olBltResFlti1.Find("MD")) )
				{
					if(popKonfList != NULL)
					{
						//Flight ID for Gate %s: % must be %s 
						olConfText.Format(GetString(IDS_STRING2722), opBlt, "MI/MD", olFlti);
						AddToKonfList(prpFlightA, (DIAFLIGHTDATA *)NULL, 0, ' ', IDS_STRING2721, olConfText, *popKonfList);

						blRet = false;
					}
					else
					{
						return false;
					}
				}
			}
		}
	}
//END MWO: 19.04.05: Mixed Flights checks ==============================

	return blRet;
}

//MWO: 19.04.05 Mixed Flights considerations for Belts
//MWO: Copied directly from the SHA 4.3 version
CString SpotAllocation::GetBltFlti( CString opBlt)
{
	CCSPtrArray<RecordSet> olGates;
	CString olGate;

	ogBCD.GetRecords("GAT", "RBAB", opBlt, &olGates);


	CString olFlti = ";"; 

	for(int c = 0 ; c < olGates.GetSize(); c++)
	{
		olGate = olGates[c].Values[ogBCD.GetFieldIndex("GAT", "GNAM")];

		olFlti += GetGatFlightId(olGate) + CString(";");
	}

	olGates.DeleteAll();
	return olFlti;		
}
//MWO: 19.04.05 Mixed Flights considerations for Belts
//MWO: Copied directly from the SHA 4.3 version
bool SpotAllocation::HavePstGatRel( CString opPst)
{
	CString olCurrGate;

	ogBCD.GetField("GAT", "RGA1", opPst, "GNAM",olCurrGate);

	if(olCurrGate.IsEmpty())
		return false;
	else
		return true;

}
//MWO: 19.04.05 Mixed Flights considerations for Belts
//MWO: Copied directly from the SHA 4.3 version
CString SpotAllocation::GetPstGatRelBlt( CString opPst, int ipGatNo)
{
	CString olCurrGate;
	CString olBlt;

	if(ipGatNo == 1)
		ogBCD.GetField("GAT", "RGA1", opPst, "GNAM",olCurrGate);
	else
		ogBCD.GetField("GAT", "RGA2", opPst, "GNAM",olCurrGate);


	
	if(olCurrGate.IsEmpty())
		return "";
	else
	{
		ogBCD.GetField("GAT", CString("GNAM"), olCurrGate, CString("RBAB"), olBlt);

		return olBlt;
	}
}
//END MWO: 19.04.05 Mixed Flights considerations for Belts


void SpotAllocation::ResetAllocation(bool bpPst, bool bpGat, bool bpBlt, bool bpWro, CTime opFrom, CTime opTo)
{
	if (!bpPst && !bpGat && !bpBlt && !bpWro)
		return;
	
	DIAFLIGHTDATA *prlFlight = NULL;
	DIAFLIGHTDATA *prlFlightA = NULL;
	DIAFLIGHTDATA *prlFlightD = NULL;
	DIAFLIGHTDATA *prlFlightSave = new DIAFLIGHTDATA ;
	CTime olCurr = CTime::GetCurrentTime();
	ogBasicData.LocalToUtc(olCurr);

	
	if( ogPosDiaFlightData.omFrom > olCurr)
		 olCurr = ogPosDiaFlightData.omFrom;


	/*
	pomParent->SendMessage(WM_SAS_PROGESS_INIT, ogPosDiaFlightData.omData.GetSize(), 0 );
	
 	CString olData;
 	CString olSelection;
	CString olField = ogPosDiaFlightData.GetFieldList();
 

	for( int j = ogPosDiaFlightData.omData.GetSize() -1 ; j >= 0; j-- )
	{
		prlFlight = &ogPosDiaFlightData.omData[j];

		if (prlFlight && !CheckPostFlightPosDia(prlFlight->Urno, NULL))
		{
			*prlFlightSave = *prlFlight;

			if (ResetFlightAllocation(prlFlight, bpPst, bpGat, bpBlt, bpWro, olCurr))
				ogPosDiaFlightData.UpdateFlight(prlFlight, prlFlightSave);


		}
		pomParent->SendMessage(WM_SAS_PROGESS_UPDATE, 1, 0 );
	}

	*/
	void *pVoid;
	POSITION pos;
	DiaCedaFlightData::RKEYLIST *prlRkey;

	pomParent->SendMessage(WM_SAS_PROGESS_INIT, ogPosDiaFlightData.omRkeyMap.GetCount(), 0 );

	bool blReset;

	for( pos = ogPosDiaFlightData.omRkeyMap.GetStartPosition(); pos != NULL; )
	{
		blReset = true;

		ogPosDiaFlightData.omRkeyMap.GetNextAssoc( pos, pVoid , (void *&)prlRkey );

		prlFlightA = ogPosDiaFlightData.GetFlightAInRotation(prlRkey);
		prlFlightD = ogPosDiaFlightData.GetFlightDInRotation(prlRkey);

		if (prlFlightA && CheckPostFlightPosDia(prlFlightA->Urno, NULL))
			blReset = false;
		if (prlFlightD && CheckPostFlightPosDia(prlFlightD->Urno, NULL))
			blReset = false;


		if((prlFlightA != NULL) && 	(prlFlightD != NULL))
		{
			
			if( !IsOverlapped(prlFlightA->Stoa, prlFlightD->Stod ,opFrom, opTo) )
				blReset = false;
		}
		else
		{
			if(prlFlightA != NULL)
			{
				if( !IsBetween(prlFlightA->Stoa, opFrom, opTo) )
				blReset = false;
			}
			else
			{
				if(prlFlightD != NULL)
				{
					if( !IsBetween(prlFlightD->Stod, opFrom, opTo) )
					blReset = false;
				}
			}

		}
		if(blReset)
		{
			for(int i = prlRkey->Rotation.GetSize() - 1; i >= 0; i--)
			{
				prlFlight = &prlRkey->Rotation[i];

				if (prlFlight && !CheckPostFlightPosDia(prlFlight->Urno, NULL))
				{
					*prlFlightSave = *prlFlight;

					if (ResetFlightAllocation(prlFlight, bpPst, bpGat, bpBlt, bpWro, olCurr))
						ogPosDiaFlightData.UpdateFlight(prlFlight, prlFlightSave);


				}
			}
		}
		pomParent->SendMessage(WM_SAS_PROGESS_UPDATE, 1, 0 );

	}

 	delete prlFlightSave; 
}




bool SpotAllocation::ResetFlightAllocation(DIAFLIGHTDATA *prlFlight, bool bpPst, bool bpGat, bool bpBlt, bool bpWro, CTime& opCurrUtc)
{
	if (ogResetAllocParameter == 0)
	{
		if(prlFlight->Tifa >= opCurrUtc)
		{
			ogPosDiaFlightData.DeleteFromKeyMap(prlFlight);
				
			if(bpPst)
			{
				prlFlight->ClearAPos();
				prlFlight->ClearDPos();
			}

			if(bpGat)
			{
				prlFlight->ClearAGat(1);
				prlFlight->ClearAGat(2);
				prlFlight->ClearDGat(1);
				prlFlight->ClearDGat(2);
			}

			if(bpBlt)
			{
				prlFlight->ClearBlt(1);
				prlFlight->ClearBlt(2);
			}

			if(bpWro)
			{
				prlFlight->ClearWro(1);
				prlFlight->ClearWro(2);
			}

			ogPosDiaFlightData.AddToKeyMap(prlFlight);
			return true;
		}
		return false;
	}


	bool blRet = false;
	if (ogResetAllocParameter != 0)
	{
		ogPosDiaFlightData.DeleteFromKeyMap(prlFlight);
		if (bpPst)
		{
			if (prlFlight->Onbl == TIMENULL)
			{
				if (ogResetAllocParameter == 1 || (ogResetAllocParameter == 2 && prlFlight->Tifa > opCurrUtc))
				{
					prlFlight->ClearAPos();
					blRet = true;
				}
			}
			if (prlFlight->Ofbl == TIMENULL)
			{
				if (ogResetAllocParameter == 1 || (ogResetAllocParameter == 2 && prlFlight->Tifd > opCurrUtc))
				{
					prlFlight->ClearDPos();
					blRet = true;
				}
			}
		}

		if (bpGat)
		{
			if (prlFlight->Ga1x == TIMENULL)
			{
				if (ogResetAllocParameter == 1 || (ogResetAllocParameter == 2 && prlFlight->Ga1b > opCurrUtc))
				{
					prlFlight->ClearAGat(1);
					blRet = true;
				}
			}
			if (prlFlight->Ga2x == TIMENULL)
			{
				if (ogResetAllocParameter == 1 || (ogResetAllocParameter == 2 && prlFlight->Ga2b > opCurrUtc))
				{
					prlFlight->ClearAGat(2);
					blRet = true;
				}
			}
			if (prlFlight->Gd1x == TIMENULL)
			{
				if (ogResetAllocParameter == 1 || (ogResetAllocParameter == 2 && prlFlight->Gd1b > opCurrUtc))
				{
					prlFlight->ClearDGat(1);
					blRet = true;
				}
			}
			if (prlFlight->Gd2x == TIMENULL)
			{
				if (ogResetAllocParameter == 1 || (ogResetAllocParameter == 2 && prlFlight->Gd2b > opCurrUtc))
				{
					prlFlight->ClearDGat(2);
					blRet = true;
				}
			}
		}

		if (bpBlt)
		{
			if (prlFlight->B1ba == TIMENULL)
			{
				if (ogResetAllocParameter == 1 || (ogResetAllocParameter == 2 && prlFlight->B1bs > opCurrUtc))
				{
					prlFlight->ClearBlt(1);
					blRet = true;
				}
			}
			if (prlFlight->B2ba == TIMENULL)
			{
				if (ogResetAllocParameter == 1 || (ogResetAllocParameter == 2 && prlFlight->B2bs > opCurrUtc))
				{
					prlFlight->ClearBlt(2);
					blRet = true;
				}
			}
		}

		if (bpWro)
		{
			if (prlFlight->W1ba == TIMENULL)
			{
				if (ogResetAllocParameter == 1 || (ogResetAllocParameter == 2 && prlFlight->W1bs > opCurrUtc))
				{
					prlFlight->ClearWro(1);
					blRet = true;
				}
			}
			if (prlFlight->W2ba == TIMENULL)
			{
				if (ogResetAllocParameter == 1 || (ogResetAllocParameter == 2 && prlFlight->W2bs > opCurrUtc))
				{
					prlFlight->ClearWro(2);
					blRet = true;
				}
			}
		}

		ogPosDiaFlightData.AddToKeyMap(prlFlight);
	}

	return blRet;
}


void SpotAllocation::AddToKonfList(const DIAFLIGHTDATA *prpFlightA, const DIAFLIGHTDATA *prpFlightD, long lpRelFUrno, char cpRelFPart, int ipType, const CString &ropConfText, CCSPtrArray<KonfItem> &ropKonfList)
{
	AddToKonfList(prpFlightA, 'A', lpRelFUrno, cpRelFPart, ipType, ropConfText, ropKonfList);
	AddToKonfList(prpFlightD, 'D', lpRelFUrno, cpRelFPart, ipType, ropConfText, ropKonfList);
}




void SpotAllocation::AddToKonfList(const DIAFLIGHTDATA *prpFlight, char cpFPart, long lpRelFUrno, char cpRelFPart, int ipType, const CString &ropConfText, CCSPtrArray<KonfItem> &ropKonfList)
{
	if(prpFlight != NULL)
	{
		KonfItem *prlKonf = new KonfItem;

		prlKonf->Text = ropConfText;
		prlKonf->KonfId.RelFUrno = lpRelFUrno;
		prlKonf->KonfId.RelFPart = cpRelFPart;
		prlKonf->KonfId.Type = ipType;


		prlKonf->KonfId.FUrno = prpFlight->Urno;
		prlKonf->KonfId.FPart = cpFPart;
		ropKonfList.Add(prlKonf);
		}

}



bool SpotAllocation::GetRelatedBlts(const DIAFLIGHTDATA *prpFlight, int ipBltNo, CStringArray &ropBlts)
{
	CString olGate;

	if (ipBltNo == 1)
	{
		olGate = prpFlight->Gta1;	
	}
	else if (ipBltNo == 2)
	{
		olGate = prpFlight->Gta2;
	}
	else
		return false;


	ropBlts.RemoveAll();

	CString olBlt;
	if (!olGate.IsEmpty())
	{
		ogBCD.GetField("GAT", "GNAM", olGate, "RBAB", olBlt);
		if (!olBlt.IsEmpty())
		{
			ropBlts.Add(olBlt);
			return true;
		}
		else
			return false;
	}


	// get belts from position

	if (strlen(prpFlight->Psta) < 1)
		return false;

	CString olPos(prpFlight->Psta);
	for(int i = 0; i < omGatRules.GetSize(); i++)
	{	
		if (omGatRules[i].RelPos1 == olPos || omGatRules[i].RelPos2 == olPos)
		{
			ogBCD.GetField("GAT", CString("GNAM"), omGatRules[i].Gnam, CString("RBAB"), olBlt);
			if (!olBlt.IsEmpty())
				ropBlts.Add(olBlt);
		}
	}

	if (ropBlts.GetSize() > 0)
		return true;
	else
		return false;
}

bool SpotAllocation::GetGatPosEnv(const CString& ropRes, const CString& ropBy, CString& olALOC, CString& olTAB, CString& olNAM, CString& olNAMRes, bool& backwards)
{
	backwards = false;
	if (ropRes == "GAT")
	{
		olNAMRes = "GNAM";
		if (ropBy == "PST")
		{
			olALOC = "GATPOS";
			olTAB = "PST";
			olNAM = "PNAM";
		}
		else if (ropBy == "WRO")
		{
			backwards = true;
			olALOC = "WROGAT";
			olTAB = "WRO";
			olNAM = "WNAM";
		}
		else if (ropBy == "BLT")
		{
			backwards = true;
			olALOC = "BAGGAT";
			olTAB = "BLT";
			olNAM = "BNAM";
		}
		else if (ropBy == "GAT")
		{
			backwards = false;
			olALOC = "GATGAT";
			olTAB = "GAT";
			olNAM = "GNAM";
		}
		else
			return false;
	}
	else if (ropRes == "PST")
	{
		olNAMRes = "PNAM";
		if (ropBy == "GAT")
		{
			backwards = true;
			olALOC = "GATPOS";
			olTAB = "GAT";
			olNAM = "GNAM";
		}
		else
			return false;
	}
	else if (ropRes == "BLT")
	{
		olNAMRes = "BNAM";
		if (ropBy == "GAT")
		{
			olALOC = "BAGGAT";
			olTAB = "GAT";
			olNAM = "GNAM";
		}
		else if (ropBy == "EXT")
		{
			backwards = true;
			olALOC = "EXTBAG";
			olTAB = "EXT";
			olNAM = "ENAM";
		}
		else
			return false;
	}
	else if (ropRes == "WRO")
	{
		olNAMRes = "WNAM";
		if (ropBy == "GAT")
		{
			olALOC = "WROGAT";
			olTAB = "GAT";
			olNAM = "GNAM";
		}
		else
			return false;
	}
	else if (ropRes == "EXT")
	{
		olNAMRes = "ENAM";
		if (ropBy == "BLT")
		{
			olALOC = "EXTBAG";
			olTAB = "BLT";
			olNAM = "BNAM";
		}
		else
			return false;
	}
	else
		return false;

	return true;
}

bool SpotAllocation::ResIsAvailable(DIAFLIGHTDATA *popFlight, const CString& ropType, const CString& ropTarget, bool bpTakeBufferTimes, bool bpPreCheck, CString opPrePos, int ipResNo)
{
	if (!popFlight)
		return false;

	bool blIsAvailable = true;

	if (ropType == "GAT")
	{
		if (popFlight->Adid[0] == 'A' || popFlight->Adid[0] == 'D')
		{
			if (bpTakeBufferTimes)
				blIsAvailable = CheckGat(popFlight, popFlight->Adid[0], ropTarget, ipResNo, NULL, &omParameters.omGatLeftBuffer, &omParameters.omGatRightBuffer, bpPreCheck, opPrePos);
			else
				blIsAvailable = CheckGat(popFlight, popFlight->Adid[0], ropTarget, ipResNo, NULL, NULL, NULL, bpPreCheck, opPrePos);
		}
		else
			return false;
	}
	else if (ropType == "PST")
	{
		if (popFlight->Adid[0] == 'A')
		{
			if (bpTakeBufferTimes)
				blIsAvailable = CheckPst(popFlight, NULL, ropTarget, NULL, &omParameters.omPstRightBuffer, &omParameters.omPstLeftBuffer, bpPreCheck);
			else
				blIsAvailable = CheckPst(popFlight, NULL, ropTarget, NULL, NULL, NULL, bpPreCheck);
		}
		else if (popFlight->Adid[0] == 'D')
		{
			if (bpTakeBufferTimes)
				blIsAvailable = CheckPst(NULL, popFlight, ropTarget, NULL, &omParameters.omPstRightBuffer, &omParameters.omPstLeftBuffer, bpPreCheck);
			else
				blIsAvailable = CheckPst(NULL, popFlight, ropTarget, NULL, NULL, NULL, bpPreCheck);
		}
		else
			return false;
	}
	else if (ropType == "BLT")
	{
		if (popFlight->Adid[0] == 'A')
		{
			blIsAvailable = CheckBlt(popFlight, ropTarget, 1, NULL, 0, bpPreCheck);
		}
		else
			return false;
	}
	else if (ropType == "WRO")
	{
		if (popFlight->Adid[0] == 'D')
		{
			blIsAvailable = CheckWro(popFlight, ropTarget, 1, NULL, bpPreCheck);
		}
		else
			return false;
	}
	else if (ropType == "EXT" && popFlight->Adid[0] == 'A')
	{
		return true;
	}
	else
		return false;

	return blIsAvailable;
}




bool SpotAllocation::GetResBy(const CString& ropRes, const CString& ropBy, const CString& ropByName,CStringArray& ropGatesBLUE, CMapStringToString& ropGatesMapBLUE,CStringArray& ropGatesRED, CMapStringToString& ropGatesMapRED, DIAFLIGHTDATA* rrpFlight, bool bpTakeBufferTimes, bool bpWithoutBLK, bool bpPreCheck, bool bpOnlyInResChain /*false*/, int ipResNo)
{
//	return true;
//	if (!rrpFlight)
//		return false;

	if (ropRes.IsEmpty() || ropBy.IsEmpty())
		return false;

	CString olALOC;
	CString olTAB;
	CString olNAM;
	CString olNAMRes;
	bool backwards = false;

	if (!GetGatPosEnv(ropRes, ropBy, olALOC, olTAB, olNAM, olNAMRes, backwards))
		return false;

	CString olAloUrno;
	if (!ogBCD.GetField("ALO","ALOC",olALOC,"URNO",olAloUrno))
		return false;

	CString olByUrno;
	if (!ogBCD.GetField(olTAB,olNAM,ropByName,"URNO",olByUrno))
		int nixx = 1;//return false;

	CCSPtrArray<RecordSet> olSgmRecords;
	if (!backwards)
	{
		CString olSgrUrno;
		olSgrUrno = ogBCD.GetFieldExt("SGR","UGTY","STYP",olAloUrno,olByUrno,"URNO");

		if (olSgrUrno.IsEmpty())
			int nix = 1;//return true;

		ogBCD.GetRecords("SGM","USGR",olSgrUrno,&olSgmRecords);
	}
	else
	{
		ogBCD.GetRecordsExt("SGM", "UGTY","UVAL", olAloUrno,olByUrno, &olSgmRecords);
	}

	olSgmRecords.Sort(CompareBySortFlag);


	CString olGatUrno;
	CString olGatName;
	CMapStringToString olResMap;
	for (int i = 0; i < olSgmRecords.GetSize(); i++)
	{
		if (!backwards)
			olGatUrno = olSgmRecords[i].Values[ogBCD.GetFieldIndex("SGM", "UVAL")];
		else
			olGatUrno = olSgmRecords[i].Values[ogBCD.GetFieldIndex("SGM", "VALU")];
		
		if (ogBCD.GetField(ropRes,"URNO",olGatUrno,olNAMRes,olGatName))
			olResMap.SetAt(olGatName, "1");
		else
			continue;

//YYY
		bool blIsAvailable = true;
		if (!bpWithoutBLK)
			blIsAvailable = ResIsAvailable(rrpFlight, ropRes, olGatName, bpTakeBufferTimes, bpPreCheck, ropByName, ipResNo);

		if (blIsAvailable)
			ropGatesMapBLUE.SetAt(olGatName, "IDB_LIGHTBLUE");
		else
			ropGatesMapBLUE.SetAt(olGatName, "IDB_LIGHTBLUEBLOCKED");

		ropGatesBLUE.Add(olGatName);
//YYY

	}

	if (!bpOnlyInResChain)
	{
		int ilGnam = ogBCD.GetFieldIndex(ropRes, olNAMRes);
		
		for(i = 0; i < ogBCD.GetDataCount(ropRes); i++)
		{
	//		int l = ogBCD.GetDataCount(ropRes);
			RecordSet rlRec;
			ogBCD.GetRecord(ropRes, i, rlRec);

			CString olGnam = rlRec[ilGnam];

			bool blIsAvailable = true;
			if (!bpWithoutBLK)
				blIsAvailable = ResIsAvailable(rrpFlight, ropRes, olGnam, bpTakeBufferTimes, bpPreCheck, ropByName);

			CBitmap olBitmap;
			CString olTmp;
			if (olResMap.Lookup(olGnam, olTmp))// is in resource chain
			{
			}
			else // is not in resource chain
			{
				if (blIsAvailable)
					ropGatesMapRED.SetAt(olGnam, "IDB_RED");
				else
					ropGatesMapRED.SetAt(olGnam, "IDB_REDBLOCKED");

				ropGatesRED.Add(olGnam);
			}
		}
	}

	olSgmRecords.DeleteAll();
	olResMap.RemoveAll();


	return true;
}



/*
bool SpotAllocation::GetPostionsBy(const CString& ropBy, const CString& ropByName,CStringArray& ropGatesBLUE, CMapStringToString& ropGatesMapBLUE,CStringArray& ropGatesRED, CMapStringToString& ropGatesMapRED, const DIAFLIGHTDATA* rrpFlight)
{
	if (!rrpFlight)
		return false;

	CString olALOC;
	CString olTAB;
	CString olNAM;
	
	if (ropBy == "GAT")
	{
		olALOC = "GATPOS";
		olTAB = "GAT";
		olNAM = "GNAM;
	}
	else
		return false;

	CTime olStartAlloc;
	CTime olEndAlloc;

	if (rrpFlight->Adid[0] == 'A')
	{
		if (!ogPosDiaFlightData.GetPstAllocTimes(*rrpFlight,NULL,rrpFlight->Psta,olStartAlloc, olEndAlloc))
		{
			return false;
		}
	}
	else if (rrpFlight->Adid[0] == 'D')
	{
		if (!ogPosDiaFlightData.GetPstAllocTimes(NULL,*rrpFlight,rrpFlight->Pstd,olStartAlloc, olEndAlloc))
		{
			return false;
		}
	}
	else
		return false;

	CString olAloUrno;
	if (!ogBCD.GetField("ALO","ALOC",olALOC,"URNO",olAloUrno))
		return false;

	CString olByUrno;
	if (!ogBCD.GetField(olTAB,olNAM,ropBy,"URNO",olByUrno))
		return false;

	CString olSgrUrno;
	olSgrUrno = ogBCD.GetFieldExt("SGR","UGTY","STYP",olAloUrno,olByUrno,"URNO");

	if (olSgrUrno.IsEmpty())
		return true;

	CCSPtrArray<RecordSet> olSgmRecords;
	ogBCD.GetRecords("SGM","USGR",olSgrUrno,&olSgmRecords);

	CString olPosUrno;
	CString olPosName;
	CMapStringToString olResMap;
	for (int i = 0; i < olSgmRecords.GetSize(); i++)
	{
		olPosUrno = olSgmRecords[i].Values[ogBCD.GetFieldIndex("SGM", "UVAL")];
		
		if (ogBCD.GetField("PST","URNO",olPosUrno,"PNAM",olGatName))
			olResMap.SetAt(olGatName, "1");
	}

	int ilPnam = ogBCD.GetFieldIndex("PST", "PNAM");
	
	for(i = 0; i < ogBCD.GetDataCount("PST"); i++)
	{
		RecordSet rlRec;
		ogBCD.GetRecord("PST", i, rlRec);

		CString olPnam = rlRec[ilGnam];

		bool blIsAvailable = IsAvailable("PST",olPnam,olStartAlloc,olEndAlloc);
		CBitmap olBitmap;
		CString olTmp;
		if (olResMap.Lookup(olPnam, olTmp))// is in resource chain
		{
			if (blIsAvailable)
				ropGatesMapBLUE.SetAt(olPnam, "IDB_LIGHTBLUE");
			else
				ropGatesMapBLUE.SetAt(olPnam, "IDB_LIGHTBLUEBLOCKED");

			ropGatesBLUE.Add(olPnam);
		}
		else // is not in resource chain
		{
			if (blIsAvailable)
				ropGatesMapRED.SetAt(olPnam, "IDB_RED");
			else
				ropGatesMapRED.SetAt(olPnam, "IDB_REDBLOCKED");

			ropGatesRED.Add(olPnam);
		}
	}

	return true;
}

*/

/*
	CTime olStartAlloc;
	CTime olEndAlloc;

	if (ropRes == "GAT")
	{
		if (rrpFlight->Adid[0] == 'A')
		{
			if (!ogPosDiaFlightData.GetGatAllocTimes(*rrpFlight,rrpFlight->Adid[0],rrpFlight->Gta1, 1,olStartAlloc, olEndAlloc))
			{
				return false;
			}
		}
		else if (rrpFlight->Adid[0] == 'D')
		{
			if (!ogPosDiaFlightData.GetGatAllocTimes(*rrpFlight,rrpFlight->Adid[0],rrpFlight->Gtd1, 1,olStartAlloc, olEndAlloc))
			{
				return false;
			}
		}
		else
			return false;
	}
	else if (ropRes == "PST")
	{
		if (rrpFlight->Adid[0] == 'A')
		{
			if (!ogPosDiaFlightData.GetPstAllocTimes(rrpFlight,NULL,rrpFlight->Psta,olStartAlloc, olEndAlloc))
			{
				return false;
			}
		}
		else if (rrpFlight->Adid[0] == 'D')
		{
			if (!ogPosDiaFlightData.GetPstAllocTimes(NULL,rrpFlight,rrpFlight->Pstd,olStartAlloc, olEndAlloc))
			{
				return false;
			}
		}
		else
			return false;
	}
	else if (ropRes == "BLT")
	{
		if (rrpFlight->Adid[0] == 'A')
		{
			if (!ogPosDiaFlightData.GetBltAllocTimes(*rrpFlight,rrpFlight->Blt1, 1,olStartAlloc, olEndAlloc))
				return false;
		}
		else
			return false;
	}
	else if (ropRes == "WRO")
	{
		if (rrpFlight->Adid[0] == 'D')
		{
			if (!ogPosDiaFlightData.GetBltAllocTimes(*rrpFlight,rrpFlight->Wro1, 1,olStartAlloc, olEndAlloc))
				return false;
		}
		else
			return false;
	}
	else
		return false;
*/


int SpotAllocation::GetAllPstWhereImNeighbour(PST_RESLIST* prlPstResFrom, CMapStringToString& olPosAndMacNeighbours, CMapStringToPtr& olPosAndSpanNeighbours)
{
	if (!prlPstResFrom)
		return 0;

	PST_RESLIST *prlPstRes = NULL;
	olPosAndSpanNeighbours.RemoveAll();
	olPosAndMacNeighbours.RemoveAll();

	CString olTmp;
	for(int i = omPstRules.GetSize() - 1; i >=0; i--)
	{
		prlPstRes = &omPstRules[i];
		if (prlPstRes)
		{
			CString olMacNeighbour;
			if (prlPstRes->PosAndMac.Lookup(prlPstResFrom->Pnam, olMacNeighbour))
			{
				if (!prlPstResFrom->PosAndMac.Lookup(prlPstRes->Pnam, olTmp))
				{
					olPosAndMacNeighbours.SetAt(prlPstRes->Pnam, olMacNeighbour);
					olPosAndSpanNeighbours.SetAt(prlPstRes->Pnam, &prlPstRes->SpanMax);
				}
			}
		}
	}

	return olPosAndMacNeighbours.GetCount();
}

int SpotAllocation::GetAllGatWhereImNeighbour(GAT_RESLIST* prlPstResFrom, CMapStringToString& opNeighbours)
{
	if (!prlPstResFrom)
		return 0;

	GAT_RESLIST *prlPstRes = NULL;
	opNeighbours.RemoveAll();

	CString olTmp;
	for(int i = omGatRules.GetSize() - 1; i >=0; i--)
	{
		prlPstRes = &omGatRules[i];
		if (prlPstRes)
		{
			CString olMacNeighbour;
			if (prlPstRes->GateNeighbours.Lookup(prlPstResFrom->Gnam, olMacNeighbour))
			{
				if (!prlPstResFrom->GateNeighbours.Lookup(prlPstRes->Gnam, olTmp))
				{
					opNeighbours.SetAt(prlPstRes->Gnam, olMacNeighbour);
				}
			}
		}
	}

	return opNeighbours.GetCount();
}
/*
LONG SpotAllocation::CheckPositionMatrix(const CString& ropFromPos,const CString& ropToPos,const CString& ropFromAct3,const CString& ropFromAct5,const CString& ropToAct3,const CString& ropToAct5)
{
	CString olAlocUrno = ogBCD.GetField("ALO","ALOC","POSPOS","URNO");
	if (olAlocUrno.IsEmpty())
		return -1;	// undefined

	CString olFromPosUrno = ogBCD.GetField("PST","PNAM",ropFromPos,"URNO");

	CString olSgrUrno  = ogBCD.GetFieldExt("SGR","UGTY","STYP",olAlocUrno,olFromPosUrno,"URNO");
	if (olSgrUrno.IsEmpty())
		return -1;	// From pos undefined

	CString olToPosUrno = ogBCD.GetField("PST","PNAM",ropToPos,"URNO");

	CString olSgmUrno  = ogBCD.GetFieldExt("SGM","USGR","UVAL",olSgrUrno,olToPosUrno,"URNO");
	if (olSgmUrno.IsEmpty())
		return -1;	// To pos undefined


	olAlocUrno = ogBCD.GetField("ALO","ALOC","POSACT","URNO");
	if (olAlocUrno.IsEmpty())
		return -1;	// undefined

	olSgrUrno  = ogBCD.GetFieldExt("SGR","UGTY","STYP",olAlocUrno,olSgmUrno,"URNO");
	if (olSgrUrno.IsEmpty())
		return -1;	// From ACT undefined

	// get aircraft type from group name urnos
	CString olFromGrnList;
	olFromGrnList  = ogBCD.GetValueList("GRM","VALU", ogBCD.GetField2("ACT","ACT3","ACT5",ropFromAct3.IsEmpty() ? ropFromAct5.IsEmpty() ? CString("") : ropFromAct5: ropFromAct3 ,"URNO"), "GURN", ";");

	// get aircraft type to group name urnos
	CString olToGrnList;
	olToGrnList  = ogBCD.GetValueList("GRM","VALU", ogBCD.GetField2("ACT","ACT3","ACT5",ropToAct3.IsEmpty() ? ropToAct5.IsEmpty() ? CString("") : ropToAct5 : ropToAct3 ,"URNO"), "GURN", ";");


	olAlocUrno = ogBCD.GetField("ALO","ALOC","ACTACT","URNO");
	if (olAlocUrno.IsEmpty())
		return -1;	// undefined

	CStringArray olFromGrnArray;
	::ExtractItemList(olFromGrnList,&olFromGrnArray,';');


	CStringArray olToGrnArray;
	::ExtractItemList(olToGrnList,&olToGrnArray,';');

	for (int i = 0; i < olFromGrnArray.GetSize(); i++)
	{
		olSgmUrno  = ogBCD.GetFieldExt("SGM","USGR","UVAL",olSgrUrno,olFromGrnArray[i],"URNO");
		if (!olSgmUrno.IsEmpty())
		{
			CString olSgrUrno = ogBCD.GetFieldExt("SGR","UGTY","STYP",olAlocUrno,olSgmUrno,"URNO");
			for (int j = 0; j < olToGrnArray.GetSize(); j++)
			{
				CString olSgmUrno = ogBCD.GetFieldExt("SGM","USGR","UVAL",olSgrUrno,olToGrnArray[j],"URNO");
				if (!olSgmUrno.IsEmpty())
				{
					CString olPrfl = ogBCD.GetField("SGM","URNO",olSgmUrno,"PRFL");
					if (olPrfl == "Y")
						return 1;
					else if (olPrfl == "N")
						return 0;
					else
						return -1;
				}
			}
		}

	}		
					
		
	return -1;	// not found
}
*/

LONG SpotAllocation::CheckPositionMatrix(const CString& opAloc,const CString& opGrp,const CString& ropFromPos,const CString& ropToPos,const CString& ropFromAct3,const CString& ropFromAct5,const CString& ropToAct3,const CString& ropToAct5)
{
	//-------
	CString olAloTab ("PST");
	CString olAloNam ("PNAM");

	if (opAloc == "POS")
	{
		olAloTab = "PST";
		olAloNam = "PNAM";
	}
	else if (opAloc == "GAT")
	{
		olAloTab = "GAT";
		olAloNam = "GNAM";
	}
	else if (opAloc == "BLT")
	{
		olAloTab = "BLT";
		olAloNam = "BNAM";
	}
	else if (opAloc == "WRO")
	{
		olAloTab = "WRO";
		olAloNam = "WNAM";
	}
	//-------
	CMapStringToPtr *polPosMatrix;
	CString olVal1 ("ACT3");
	CString olVal2 ("ACT5");

	if (opGrp == "ACT")
	{
		polPosMatrix = &omPosMatrix;
		olVal1 = "ACT3";
		olVal2 = "ACT5";
	}
	else if (opGrp == "ALT")
	{
		if (opAloc == "POS")
			polPosMatrix = &omPosMatrixPosAlt;
		else if (opAloc == "GAT")
			polPosMatrix = &omPosMatrixGatAlt;

		olVal1 = "ALC2";
		olVal2 = "ALC3";
	}
	else if (opGrp == "APT")
	{
		if (opAloc == "POS")
			polPosMatrix = &omPosMatrixPosApt;
		else if (opAloc == "GAT")
			polPosMatrix = &omPosMatrixGatApt;

		olVal1 = "APC3";
		olVal2 = "APC4";
	}
	//-------

	CString olFromPosUrno = ogBCD.GetField(olAloTab,olAloNam,ropFromPos,"URNO");
	CMapStringToPtr *polPosToPosMap;
//	if (omPosMatrix.Lookup(olFromPosUrno,(void *&)polPosToPosMap))TODOO
	if (polPosMatrix->Lookup(olFromPosUrno,(void *&)polPosToPosMap))
	{
		CString olToPosUrno = ogBCD.GetField(olAloTab,olAloNam,ropToPos,"URNO");
		CMapStringToPtr *polPosToActMap;
		if (polPosToPosMap && polPosToPosMap->Lookup(olToPosUrno,(void *&)polPosToActMap))
		{
			// get aircraft type from group name urnos
			CString olFromGrnList;
			olFromGrnList  = ogBCD.GetValueList("GRM","VALU", ogBCD.GetField2(opGrp,olVal1,olVal2,ropFromAct3.IsEmpty() ? ropFromAct5.IsEmpty() ? CString("") : ropFromAct5: ropFromAct3 ,"URNO"), "GURN", ";");
			CStringArray olFromGrnArray;
			::ExtractItemList(olFromGrnList,&olFromGrnArray,';');


			// get aircraft type to group name urnos
			CString olToGrnList;
			olToGrnList  = ogBCD.GetValueList("GRM","VALU", ogBCD.GetField2(opGrp,olVal1,olVal2,ropToAct3.IsEmpty() ? ropToAct5.IsEmpty() ? CString("") : ropToAct5 : ropToAct3 ,"URNO"), "GURN", ";");

			CStringArray olToGrnArray;
			::ExtractItemList(olToGrnList,&olToGrnArray,';');

			for (int i = 0; i < olFromGrnArray.GetSize(); i++)
			{
				CMapStringToPtr *polActToActMap;
				if (polPosToActMap && polPosToActMap->Lookup(olFromGrnArray[i],(void *&)polActToActMap))
				{
					for (int j = 0; j < olToGrnArray.GetSize(); j++)
					{
						CString *polPrfl;
						if (polActToActMap && polActToActMap->Lookup(olToGrnArray[j],(void *&)polPrfl))
						{
							if (polPrfl && (*polPrfl) == "Y")
								return 1;
							else if (polPrfl && (*polPrfl) == "N")
								return 0;
							else
								return -1;
						}

					}
				}
			}

			return -1;
		}
		else
			return -1;
	}
	else
		return -1;
}

LONG SpotAllocation::CheckPositionMatrix(const CString& ropFromPos,const CString& ropToPos,const CString& ropFromAct3,const CString& ropFromAct5,const CString& ropToAct3,const CString& ropToAct5)
{
#if	1
	CString olFromPosUrno = ogBCD.GetField("PST","PNAM",ropFromPos,"URNO");
	CMapStringToPtr *polPosToPosMap;
	if (omPosMatrix.Lookup(olFromPosUrno,(void *&)polPosToPosMap))
	{
		CString olToPosUrno = ogBCD.GetField("PST","PNAM",ropToPos,"URNO");
		CMapStringToPtr *polPosToActMap;
		if (polPosToPosMap && polPosToPosMap->Lookup(olToPosUrno,(void *&)polPosToActMap))
		{
			// get aircraft type from group name urnos
			CString olFromGrnList;
			olFromGrnList  = ogBCD.GetValueList("GRM","VALU", ogBCD.GetField2("ACT","ACT3","ACT5",ropFromAct3.IsEmpty() ? ropFromAct5.IsEmpty() ? CString("") : ropFromAct5: ropFromAct3 ,"URNO"), "GURN", ";");
			CStringArray olFromGrnArray;
			::ExtractItemList(olFromGrnList,&olFromGrnArray,';');


			// get aircraft type to group name urnos
			CString olToGrnList;
			olToGrnList  = ogBCD.GetValueList("GRM","VALU", ogBCD.GetField2("ACT","ACT3","ACT5",ropToAct3.IsEmpty() ? ropToAct5.IsEmpty() ? CString("") : ropToAct5 : ropToAct3 ,"URNO"), "GURN", ";");

			CStringArray olToGrnArray;
			::ExtractItemList(olToGrnList,&olToGrnArray,';');

			for (int i = 0; i < olFromGrnArray.GetSize(); i++)
			{
				CMapStringToPtr *polActToActMap;
				if (polPosToActMap && polPosToActMap->Lookup(olFromGrnArray[i],(void *&)polActToActMap))
				{
					for (int j = 0; j < olToGrnArray.GetSize(); j++)
					{
						CString *polPrfl;
						if (polActToActMap && polActToActMap->Lookup(olToGrnArray[j],(void *&)polPrfl))
						{
							if (polPrfl && (*polPrfl) == "Y")
								return 1;
							else if (polPrfl && (*polPrfl) == "N")
								return 0;
							else
								return -1;
						}

					}
				}
			}

			return -1;
		}
		else
			return -1;
	}
	else
		return -1;
#else

	CString olAlocUrno = ogBCD.GetField("ALO","ALOC","POSPOS","URNO");
	if (olAlocUrno.IsEmpty())
		return -1;	// undefined

	CString olFromPosUrno = ogBCD.GetField("PST","PNAM",ropFromPos,"URNO");

	CString olSgrUrno  = ogBCD.GetFieldExt("SGR","UGTY","STYP",olAlocUrno,olFromPosUrno,"URNO");
	if (olSgrUrno.IsEmpty())
		return -1;	// From pos undefined

	CString olToPosUrno = ogBCD.GetField("PST","PNAM",ropToPos,"URNO");

	CString olSgmUrno  = ogBCD.GetFieldExt("SGM","USGR","UVAL",olSgrUrno,olToPosUrno,"URNO");
	if (olSgmUrno.IsEmpty())
		return -1;	// To pos undefined


	olAlocUrno = ogBCD.GetField("ALO","ALOC","POSACT","URNO");
	if (olAlocUrno.IsEmpty())
		return -1;	// undefined

	olSgrUrno  = ogBCD.GetFieldExt("SGR","UGTY","STYP",olAlocUrno,olSgmUrno,"URNO");
	if (olSgrUrno.IsEmpty())
		return -1;	// From ACT undefined

	// get aircraft type from group name urnos
	CString olFromGrnList;
	olFromGrnList  = ogBCD.GetValueList("GRM","VALU", ogBCD.GetField2("ACT","ACT3","ACT5",ropFromAct3.IsEmpty() ? ropFromAct5.IsEmpty() ? CString("") : ropFromAct5: ropFromAct3 ,"URNO"), "GURN", ";");

	// get aircraft type to group name urnos
	CString olToGrnList;
	olToGrnList  = ogBCD.GetValueList("GRM","VALU", ogBCD.GetField2("ACT","ACT3","ACT5",ropToAct3.IsEmpty() ? ropToAct5.IsEmpty() ? CString("") : ropToAct5 : ropToAct3 ,"URNO"), "GURN", ";");


	olAlocUrno = ogBCD.GetField("ALO","ALOC","ACTACT","URNO");
	if (olAlocUrno.IsEmpty())
		return -1;	// undefined

	CStringArray olFromGrnArray;
	::ExtractItemList(olFromGrnList,&olFromGrnArray,';');


	CStringArray olToGrnArray;
	::ExtractItemList(olToGrnList,&olToGrnArray,';');

	for (int i = 0; i < olFromGrnArray.GetSize(); i++)
	{
		olSgmUrno  = ogBCD.GetFieldExt("SGM","USGR","UVAL",olSgrUrno,olFromGrnArray[i],"URNO");
		if (!olSgmUrno.IsEmpty())
		{
			CString olSgrUrno = ogBCD.GetFieldExt("SGR","UGTY","STYP",olAlocUrno,olSgmUrno,"URNO");
			for (int j = 0; j < olToGrnArray.GetSize(); j++)
			{
				CString olSgmUrno = ogBCD.GetFieldExt("SGM","USGR","UVAL",olSgrUrno,olToGrnArray[i],"URNO");
				if (!olSgmUrno.IsEmpty())
				{
					CString olPrfl = ogBCD.GetField("SGM","URNO",olSgmUrno,"PRFL");
					if (olPrfl == "Y")
						return 1;
					else if (olPrfl == "N")
						return 0;
					else
						return -1;
				}
			}
		}

	}		
					
		
	return -1;	// not found
#endif

}

bool SpotAllocation::InitializePositionMatrix()
{
	if(bmIsPositionMatrixInitialized)
		return true;


	ReleasePositionMatrix();
	InitializePositionMatrix(CString("POS"), CString("ACT"));
	InitializePositionMatrix(CString("POS"), CString("APT"));
	InitializePositionMatrix(CString("POS"), CString("ALT"));
	InitializePositionMatrix(CString("GAT"), CString("APT"));
	InitializePositionMatrix(CString("GAT"), CString("ALT"));
	//Init();
	bmIsPositionMatrixInitialized = true;
	return true;

	CString olAlocUrno = ogBCD.GetField("ALO","ALOC","POSPOS","URNO");
	if (olAlocUrno.IsEmpty())
		return false;

	CString olFromPosList;
	olFromPosList  = ogBCD.GetValueList("SGR","UGTY",olAlocUrno, "URNO", ";");

	CStringArray olFromPosArray;	
	::ExtractItemList(olFromPosList,&olFromPosArray,';');


	for (int i = 0; i < olFromPosArray.GetSize(); i++)
	{
		CString olFromPosUrno = ogBCD.GetField("SGR","URNO",olFromPosArray[i],"STYP");

		CMapStringToPtr *polMapPosToPos = new CMapStringToPtr;
		omPosMatrix.SetAt(olFromPosUrno,polMapPosToPos);

		CString olToPosList;
		olToPosList  = ogBCD.GetValueList("SGM","USGR",olFromPosArray[i], "UVAL", ";");

		CStringArray olToPosArray;	
		::ExtractItemList(olToPosList,&olToPosArray,';');
		for (int j = 0; j < olToPosArray.GetSize(); j++)
		{
			CMapStringToPtr *polMapPosToAct = new CMapStringToPtr;
			polMapPosToPos->SetAt(olToPosArray[j],polMapPosToAct);

			olAlocUrno = ogBCD.GetField("ALO","ALOC","POSACT","URNO");
			if (olAlocUrno.IsEmpty())
				return false;

			CString olSgmUrno = ogBCD.GetFieldExt("SGM","USGR","UVAL",olFromPosArray[i],olToPosArray[j],"URNO");

			CString olSgrUrno  = ogBCD.GetFieldExt("SGR","UGTY","STYP",olAlocUrno,olSgmUrno,"URNO");

			CString olPosToActList;
			olPosToActList  = ogBCD.GetValueList("SGM","USGR",olSgrUrno, "UVAL", ";");

			CStringArray olPosToActArray;	
			::ExtractItemList(olPosToActList,&olPosToActArray,';');
						
			for (int k = 0; k < olPosToActArray.GetSize(); k++)
			{
				CMapStringToPtr *polMapActToAct = new CMapStringToPtr;
				polMapPosToAct->SetAt(olPosToActArray[k],polMapActToAct);
			
				olAlocUrno = ogBCD.GetField("ALO","ALOC","ACTACT","URNO");
				if (olAlocUrno.IsEmpty())
					return false;

				CString olSgmUrno = ogBCD.GetFieldExt("SGM","USGR","UVAL",olSgrUrno,olPosToActArray[k],"URNO");

				CString olSgrUrno  = ogBCD.GetFieldExt("SGR","UGTY","STYP",olAlocUrno,olSgmUrno,"URNO");
				
				CString olActToActList;
				olActToActList  = ogBCD.GetValueList("SGM","USGR",olSgrUrno, "UVAL", ";");

				CStringArray olActToActArray;	
				::ExtractItemList(olActToActList,&olActToActArray,';');

				for (int l = 0; l < olActToActArray.GetSize(); l++)
				{
					CString* polPrfl  = new CString(ogBCD.GetFieldExt("SGM","USGR","UVAL",olSgrUrno,olActToActArray[l],"PRFL"));
					polMapActToAct->SetAt(olActToActArray[l],polPrfl);
				}
				
			}
			
		}
		
	}
	bmIsPositionMatrixInitialized = true;
	return true;
}

//##
bool SpotAllocation::InitializePositionMatrix(CString& opAloc,CString& opGrp)
{
	CMapStringToPtr* omPosMatrixAlocGrp;

	if (opAloc == "POS" && opGrp == "ACT")
		omPosMatrixAlocGrp = &omPosMatrix;
	else if (opAloc == "POS" && opGrp == "APT")
		omPosMatrixAlocGrp = &omPosMatrixPosApt;
	else if (opAloc == "POS" && opGrp == "ALT")
		omPosMatrixAlocGrp = &omPosMatrixPosAlt;
	else if (opAloc == "GAT" && opGrp == "APT")
		omPosMatrixAlocGrp = &omPosMatrixGatApt;
	else if (opAloc == "GAT" && opGrp == "ALT")
		omPosMatrixAlocGrp = &omPosMatrixGatAlt;

//	ReleasePositionMatrix(omPosMatrixAlocGrp);

	CString olName ("");

	olName = opAloc + opAloc;
	CString olAlocUrno = ogBCD.GetField("ALO","ALOC",olName,"URNO");
	if (olAlocUrno.IsEmpty())
		return false;

	CString olFromPosList;
	olFromPosList  = ogBCD.GetValueList("SGR","UGTY",olAlocUrno, "URNO", ";");

	CStringArray olFromPosArray;	
	::ExtractItemList(olFromPosList,&olFromPosArray,';');


	for (int i = 0; i < olFromPosArray.GetSize(); i++)
	{
		CString olFromPosUrno = ogBCD.GetField("SGR","URNO",olFromPosArray[i],"STYP");

		CMapStringToPtr *polMapPosToPos = new CMapStringToPtr;

		omPosMatrixAlocGrp->SetAt(olFromPosUrno,polMapPosToPos);
/*
		if (opAloc == "POS" && opGrp == "ACT")
			omPosMatrix.SetAt(olFromPosUrno,polMapPosToPos);
		else if (opAloc == "POS" && opGrp == "APT")
			omPosMatrixPosApt.SetAt(olFromPosUrno,polMapPosToPos);
		else if (opAloc == "POS" && opGrp == "ALT")
			omPosMatrixPosAlt.SetAt(olFromPosUrno,polMapPosToPos);
		else if (opAloc == "GAT" && opGrp == "APT")
			omPosMatrixGatApt.SetAt(olFromPosUrno,polMapPosToPos);
		else if (opAloc == "GAT" && opGrp == "ALT")
			omPosMatrixGatAlt.SetAt(olFromPosUrno,polMapPosToPos);
*/
		CString olToPosList;
		olToPosList  = ogBCD.GetValueList("SGM","USGR",olFromPosArray[i], "UVAL", ";");

		CStringArray olToPosArray;	
		::ExtractItemList(olToPosList,&olToPosArray,';');
		for (int j = 0; j < olToPosArray.GetSize(); j++)
		{
			CMapStringToPtr *polMapPosToAct = new CMapStringToPtr;
			polMapPosToPos->SetAt(olToPosArray[j],polMapPosToAct);

			olName = opAloc + opGrp;
			olAlocUrno = ogBCD.GetField("ALO","ALOC",olName,"URNO");
			if (olAlocUrno.IsEmpty())
				return false;

			CString olSgmUrno = ogBCD.GetFieldExt("SGM","USGR","UVAL",olFromPosArray[i],olToPosArray[j],"URNO");

			CString olSgrUrno  = ogBCD.GetFieldExt("SGR","UGTY","STYP",olAlocUrno,olSgmUrno,"URNO");

			CString olPosToActList;
			olPosToActList  = ogBCD.GetValueList("SGM","USGR",olSgrUrno, "UVAL", ";");

			CStringArray olPosToActArray;	
			::ExtractItemList(olPosToActList,&olPosToActArray,';');
						
			for (int k = 0; k < olPosToActArray.GetSize(); k++)
			{
				CMapStringToPtr *polMapActToAct = new CMapStringToPtr;
				polMapPosToAct->SetAt(olPosToActArray[k],polMapActToAct);
			
				olName = opGrp + opGrp;
				olAlocUrno = ogBCD.GetField("ALO","ALOC",olName,"URNO");
				if (olAlocUrno.IsEmpty())
					return false;

				CString olSgmUrno = ogBCD.GetFieldExt("SGM","USGR","UVAL",olSgrUrno,olPosToActArray[k],"URNO");

				CString olSgrUrno  = ogBCD.GetFieldExt("SGR","UGTY","STYP",olAlocUrno,olSgmUrno,"URNO");
				
				CString olActToActList;
				olActToActList  = ogBCD.GetValueList("SGM","USGR",olSgrUrno, "UVAL", ";");

				CStringArray olActToActArray;	
				::ExtractItemList(olActToActList,&olActToActArray,';');

				for (int l = 0; l < olActToActArray.GetSize(); l++)
				{
					CString* polPrfl  = new CString(ogBCD.GetFieldExt("SGM","USGR","UVAL",olSgrUrno,olActToActArray[l],"PRFL"));
					polMapActToAct->SetAt(olActToActArray[l],polPrfl);
				}
				
			}
			
		}
		
	}

	return true;
}

bool SpotAllocation::ReleasePositionMatrix(CMapStringToPtr* omPosMatrixAlocGrp)
{
	if (!omPosMatrixAlocGrp)
		return true;

	for (POSITION pos = omPosMatrixAlocGrp->GetStartPosition(); pos != NULL; )
	{
		CString olKey;
		CMapStringToPtr* polPosToPosMap;
		omPosMatrixAlocGrp->GetNextAssoc(pos,olKey,(void *&)polPosToPosMap);
		if (polPosToPosMap)
		{
			for (POSITION pos2 = polPosToPosMap->GetStartPosition(); pos2 != NULL; )
			{
				CString olKey;
				CMapStringToPtr* polPosToActMap;
				polPosToPosMap->GetNextAssoc(pos2,olKey,(void *&)polPosToActMap);
				if (polPosToActMap)
				{
					for (POSITION pos3 = polPosToActMap->GetStartPosition(); pos3 != NULL; )
					{
						CString olKey;
						CMapStringToPtr* polActToActMap;
						polPosToActMap->GetNextAssoc(pos3,olKey,(void *&)polActToActMap);
						if (polActToActMap)
						{
							for (POSITION pos4 = polActToActMap->GetStartPosition(); pos4 != NULL; )
							{
								CString olKey;
								CString* polPrfl;
								polActToActMap->GetNextAssoc(pos4,olKey,(void *&)polPrfl);
								delete polPrfl;
							}
							delete polActToActMap;
						}
					}
					delete polPosToActMap;
				}
			}
			delete polPosToPosMap;
		}
	}

	omPosMatrixAlocGrp->RemoveAll();
	return true;
}

//##


bool SpotAllocation::ReleasePositionMatrix()
{
	ReleasePositionMatrix(&omPosMatrix);
	ReleasePositionMatrix(&omPosMatrixPosAlt);
	ReleasePositionMatrix(&omPosMatrixPosApt);
	ReleasePositionMatrix(&omPosMatrixGatAlt);
	ReleasePositionMatrix(&omPosMatrixGatApt);
	return true;


	for (POSITION pos = omPosMatrix.GetStartPosition(); pos != NULL; )
	{
		CString olKey;
		CMapStringToPtr* polPosToPosMap;
		omPosMatrix.GetNextAssoc(pos,olKey,(void *&)polPosToPosMap);
		if (polPosToPosMap)
		{
			for (POSITION pos2 = polPosToPosMap->GetStartPosition(); pos2 != NULL; )
			{
				CString olKey;
				CMapStringToPtr* polPosToActMap;
				polPosToPosMap->GetNextAssoc(pos2,olKey,(void *&)polPosToActMap);
				if (polPosToActMap)
				{
					for (POSITION pos3 = polPosToActMap->GetStartPosition(); pos3 != NULL; )
					{
						CString olKey;
						CMapStringToPtr* polActToActMap;
						polPosToActMap->GetNextAssoc(pos3,olKey,(void *&)polActToActMap);
						if (polActToActMap)
						{
							for (POSITION pos4 = polActToActMap->GetStartPosition(); pos4 != NULL; )
							{
								CString olKey;
								CString* polPrfl;
								polActToActMap->GetNextAssoc(pos4,olKey,(void *&)polPrfl);
								delete polPrfl;
							}
							delete polActToActMap;
						}
					}
					delete polPosToActMap;
				}
			}
			delete polPosToPosMap;
		}
	}

	omPosMatrix.RemoveAll();
	return true;
}

bool SpotAllocation::GetFlnoForPrintMap(const DIAFLIGHTDATA *prpFlight, CString& opFlno)
{
	DiaCedaFlightData::RKEYLIST *prlRkey = ogPosDiaFlightData.GetRotationByRkey(prpFlight->Rkey);

	for(int i = 0; i < prlRkey->Rotation.GetSize(); i++)
	{
		if(CString(prlRkey->Rotation[i].Adid) == "A")
		{
			opFlno += "  " + CString(prlRkey->Rotation[i].Flno);
		}
		else
		{
			opFlno += " / " + CString(prlRkey->Rotation[i].Flno);
		}
	}

	return true;
}


void SpotAllocation::Userallocatevector(CString userdatei)
{
						vData.clear();

						 // READ FILE FROM C

						char pclConfigPath[256]="";
						char pclscenariopath[256]="";
						
						
							if (getenv("CEDA") == NULL)
								strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
							else
								strcpy(pclConfigPath, getenv("CEDA"));

							GetPrivateProfileString(ogAppName, "SCENARIOPATH", "DEFAULT",
								pclscenariopath, sizeof pclscenariopath, pclConfigPath);
						
						// THIS THE PATH
						CString path=pclscenariopath ;	
						CString	omFileName = path +"\\ua" + userdatei + ".usa";

							FILE *stream; 
							char *s;
							char buffer[100]="";

							stream = fopen(omFileName, "rt"); 
							while ((s = fgets (buffer, sizeof (buffer), stream)) != NULL)
							{
    
								 vData.push_back(buffer); 
							}

							fclose(stream);
}





bool SpotAllocation::GetRelatedGates(const CString& ropByName, CStringArray& ropGates, CString ropBy /*'= "PST"*/)
{
	CString ropRes = "GAT";

	//TRACE("\n-----------------------");
	//TRACE("\nPos--->%s", ropByName);

	CString olALOC;
	CString olTAB;
	CString olNAM;
	CString olNAMRes;
	bool backwards = false;

	if (!GetGatPosEnv(ropRes, ropBy, olALOC, olTAB, olNAM, olNAMRes, backwards))
		return false;

	CString olAloUrno;
	if (!ogBCD.GetField("ALO","ALOC",olALOC,"URNO",olAloUrno))
		return false;

	CString olByUrno;
	if (!ogBCD.GetField(olTAB,olNAM,ropByName,"URNO",olByUrno))
		int nixx = 1;//return false;

	CCSPtrArray<RecordSet> olSgmRecords;
	if (!backwards)
	{
		CString olSgrUrno;
		olSgrUrno = ogBCD.GetFieldExt("SGR","UGTY","STYP",olAloUrno,olByUrno,"URNO");

		if (olSgrUrno.IsEmpty())
			int nix = 1;//return true;

		ogBCD.GetRecords("SGM","USGR",olSgrUrno,&olSgmRecords);
	}
	else
	{
		ogBCD.GetRecordsExt("SGM", "UGTY","UVAL", olAloUrno,olByUrno, &olSgmRecords);
	}

	olSgmRecords.Sort(CompareBySortFlag);


	CString olGatUrno;
	CString olGatName;
	CMapStringToString olResMap;
	for (int i = 0; i < olSgmRecords.GetSize(); i++)
	{
		if (!backwards)
			olGatUrno = olSgmRecords[i].Values[ogBCD.GetFieldIndex("SGM", "UVAL")];
		else
			olGatUrno = olSgmRecords[i].Values[ogBCD.GetFieldIndex("SGM", "VALU")];
		
		if (ogBCD.GetField(ropRes,"URNO",olGatUrno,olNAMRes,olGatName))
			olResMap.SetAt(olGatName, "1");
		else
			continue;


		ropGates.Add(olGatName);

		//TRACE("\nRel Gate--->%s", olGatName);
	
	}

	olSgmRecords.DeleteAll();
	olResMap.RemoveAll();


	return true;
}





void SpotAllocation::MergeBKBars(CCSPtrArray<BLKDATA> &ropBkBars)
{
    CCSPtrArray<BLKDATA> olBkBars;	// background bar

	BLKDATA *prlBKBar1;
	BLKDATA *prlBKBar2;
	BLKDATA *prlBKBarNew;


	bool blOverlapFound =  false;


	int ilLimit = ropBkBars.GetSize() * ropBkBars.GetSize();

	do
	{
		blOverlapFound =  false;
		ropBkBars.Sort(CompareBKBarsByTime);

		for( int i = 0; i < ropBkBars.GetSize() && !blOverlapFound && i < ilLimit; i++)
		{
			for( int j = 0; j < ropBkBars.GetSize() && !blOverlapFound && j < ilLimit; j++)
			{
				if(i != j)
				{
					prlBKBar1 = &ropBkBars[i];
					prlBKBar2 = &ropBkBars[j];


					if(prlBKBar1->Text == "" || prlBKBar2->Text == "")
						continue;

					if(prlBKBar1->Text.GetLength() >= 201 || prlBKBar2->Text.GetLength() >= 201 || i == ilLimit -1  || i == ilLimit - 1)
					{
						CFPMSApp::MyTopmostMessageBox(NULL, GetString(IDS_STRING2877), GetString(ST_FEHLER), MB_ICONERROR | MB_OK);
						return;
					}

					TRACE("-------------------------------------------------\n");
					TRACE(" BKBAR1   : %s - %s  %s\n", prlBKBar1->StartTime.Format("%H:%M %d.%m.%y"), prlBKBar1->EndTime.Format("%H:%M %d.%m.%y"), prlBKBar1->Text);
					TRACE(" BKBAR2   : %s - %s  %s\n", prlBKBar2->StartTime.Format("%H:%M %d.%m.%y"), prlBKBar2->EndTime.Format("%H:%M %d.%m.%y"), prlBKBar2->Text);

					TRACE("---Result:\n");

					if( IsWithIn( prlBKBar2->StartTime, prlBKBar2->EndTime, prlBKBar1->StartTime, prlBKBar1->EndTime) || 
						 IsWithIn( prlBKBar1->StartTime, prlBKBar1->EndTime, prlBKBar2->StartTime, prlBKBar2->EndTime))
					{
						if(prlBKBar2->StartTime == prlBKBar1->StartTime)
						{

							if(prlBKBar1->EndTime > prlBKBar2->EndTime)
							{
								prlBKBar1->StartTime = prlBKBar2->EndTime;
								if( prlBKBar2->Text.Find( CString(" ") + prlBKBar1->Text + CString(" ")) == -1)
								{
									prlBKBar2->Text = prlBKBar1->Text + CString(" / ") +prlBKBar2->Text;
								}
								blOverlapFound =  true;
								TRACE("1BKBAR1new: %s - %s  %s\n", prlBKBar1->StartTime.Format("%H:%M %d.%m.%y"), prlBKBar1->EndTime.Format("%H:%M %d.%m.%y"), prlBKBar1->Text);
								TRACE("1BKBAR2new: %s - %s  %s\n", prlBKBar2->StartTime.Format("%H:%M %d.%m.%y"), prlBKBar2->EndTime.Format("%H:%M %d.%m.%y"), prlBKBar2->Text);
								break;
							}
							else
							{
								if(prlBKBar1->EndTime < prlBKBar2->EndTime)
								{
									if( prlBKBar1->Text.Find( CString(" ") + prlBKBar2->Text + CString(" ")) == -1)
									{
										prlBKBar1->Text = prlBKBar1->Text + CString(" / ") +prlBKBar2->Text;
									}
									prlBKBar2->StartTime = prlBKBar1->EndTime;
									blOverlapFound =  true;
									TRACE("1BKBAR1new: %s - %s  %s\n", prlBKBar1->StartTime.Format("%H:%M %d.%m.%y"), prlBKBar1->EndTime.Format("%H:%M %d.%m.%y"), prlBKBar1->Text);
									TRACE("1BKBAR2new: %s - %s  %s\n", prlBKBar2->StartTime.Format("%H:%M %d.%m.%y"), prlBKBar2->EndTime.Format("%H:%M %d.%m.%y"), prlBKBar2->Text);
									break;
								}
								else
								{
									if( prlBKBar1->Text.Find( CString(" ") + prlBKBar2->Text + CString(" ")) == -1)
									{
										prlBKBar1->Text = prlBKBar1->Text + CString(" / ") +prlBKBar2->Text;
									}
									blOverlapFound =  true;
									TRACE("XBKBAR1new: %s - %s  %s\n", prlBKBar1->StartTime.Format("%H:%M %d.%m.%y"), prlBKBar1->EndTime.Format("%H:%M %d.%m.%y"), prlBKBar1->Text);
									TRACE("XBKBAR2new: %s - %s  %s\n", prlBKBar2->StartTime.Format("%H:%M %d.%m.%y"), prlBKBar2->EndTime.Format("%H:%M %d.%m.%y"), prlBKBar2->Text);
									ropBkBars.DeleteAt(j);

									break;
								}
							}
						}
						else
						{

							if(prlBKBar2->EndTime == prlBKBar1->EndTime)
							{
								if( prlBKBar2->Text.Find( CString(" ") + prlBKBar1->Text + CString(" ")) == -1)
								{
									prlBKBar2->Text = prlBKBar1->Text + CString(" / ") +prlBKBar2->Text;
								}
								prlBKBar1->EndTime = prlBKBar2->StartTime;
								blOverlapFound =  true;
								TRACE("3BKBAR1new: %s - %s  %s\n", prlBKBar1->StartTime.Format("%H:%M %d.%m.%y"), prlBKBar1->EndTime.Format("%H:%M %d.%m.%y"), prlBKBar1->Text);
								TRACE("3BKBAR2new: %s - %s  %s\n", prlBKBar2->StartTime.Format("%H:%M %d.%m.%y"), prlBKBar2->EndTime.Format("%H:%M %d.%m.%y"), prlBKBar2->Text);
								break;

							}
							else
							{

								prlBKBarNew = new BLKDATA;
								*prlBKBarNew = *prlBKBar1;
								ropBkBars.Add(prlBKBarNew);
								prlBKBarNew->StartTime = prlBKBar2->EndTime;
								
								prlBKBar1->EndTime = prlBKBar2->StartTime;

								if( prlBKBar2->Text.Find( CString(" ") + prlBKBar1->Text + CString(" ")) == -1)
								{
									prlBKBar2->Text = prlBKBar1->Text + CString(" / ") +prlBKBar2->Text;
								}
								blOverlapFound =  true;
								TRACE("4BKBAR1new: %s - %s  %s\n", prlBKBar1->StartTime.Format("%H:%M %d.%m.%y"), prlBKBar1->EndTime.Format("%H:%M %d.%m.%y"), prlBKBar1->Text);
								TRACE("4BKBAR2new: %s - %s  %s\n", prlBKBar2->StartTime.Format("%H:%M %d.%m.%y"), prlBKBar2->EndTime.Format("%H:%M %d.%m.%y"), prlBKBar2->Text);
								TRACE("4BKBAR3new: %s - %s  %s\n", prlBKBarNew->StartTime.Format("%H:%M %d.%m.%y"), prlBKBarNew->EndTime.Format("%H:%M %d.%m.%y"), prlBKBarNew->Text);
								break;
							}
						}
					}
					else
					{
						if( IsReallyOverlapped( prlBKBar1->StartTime, prlBKBar1->EndTime, prlBKBar2->StartTime, prlBKBar2->EndTime))
						{
							prlBKBarNew = new BLKDATA;
							*prlBKBarNew = *prlBKBar2;
							ropBkBars.Add(prlBKBarNew);

							prlBKBarNew->StartTime = prlBKBar1->EndTime;
							
							prlBKBar2->EndTime = prlBKBar1->EndTime;
							prlBKBar1->EndTime = prlBKBar2->StartTime;


							if( prlBKBar2->Text.Find( CString(" ") + prlBKBar1->Text + CString(" ")) == -1)
							{
								prlBKBar2->Text = prlBKBar1->Text + CString(" / ") + prlBKBar2->Text;
							}

							TRACE("5BKBAR1new: %s - %s  %s\n", prlBKBar1->StartTime.Format("%H:%M %d.%m.%y"), prlBKBar1->EndTime.Format("%H:%M %d.%m.%y"), prlBKBar1->Text);
							TRACE("5BKBAR2new: %s - %s  %s\n", prlBKBar2->StartTime.Format("%H:%M %d.%m.%y"), prlBKBar2->EndTime.Format("%H:%M %d.%m.%y"), prlBKBar2->Text);
							TRACE("5BKBAR3new: %s - %s  %s\n", prlBKBarNew->StartTime.Format("%H:%M %d.%m.%y"), prlBKBarNew->EndTime.Format("%H:%M %d.%m.%y"), prlBKBarNew->Text);
							blOverlapFound =  true;
							break;

						}
					}
				}
			}

		}

	} while (blOverlapFound);



    return;
}



bool SpotAllocation::MakePstWroBltBlkData( CTime opStartTime, CTime opEndTime)
{
	MakePstBlkData( opStartTime, opEndTime);
	MakeGatBlkData( opStartTime, opEndTime);
	MakeWroBlkData( opStartTime, opEndTime);
	MakeBltBlkData( opStartTime, opEndTime);
	return true;
}


bool SpotAllocation::MakePstBlkData( CTime opStartTime, CTime opEndTime)
{	
	PST_RESLIST *prlPstRes = NULL;

	for(int i = omPstRules.GetSize() - 1; i >=0; i--)
	{
		prlPstRes = &omPstRules[i];

		MakeBlkData( opStartTime, opEndTime, CString("PST"), prlPstRes->Pnam );
		
	}
	return true;
}

bool SpotAllocation::MakeGatBlkData( CTime opStartTime, CTime opEndTime)
{	
	GAT_RESLIST *prlGatRes = NULL;

	for( int i = omGatRules.GetSize() - 1; i >=0; i--)
	{
		prlGatRes = &omGatRules[i];

		MakeBlkData( opStartTime, opEndTime, CString("GAT"), prlGatRes->Gnam );
		
	}
	return true;
}

bool SpotAllocation::MakeBltBlkData( CTime opStartTime, CTime opEndTime)
{	
	BLT_RESLIST *prlBltRes = NULL;

	for( int i = omBltRules.GetSize() - 1; i >=0; i--)
	{
		prlBltRes = &omBltRules[i];

		MakeBlkData( opStartTime, opEndTime, CString("BLT"), prlBltRes->Bnam );
		
	}
	return true;
}

bool SpotAllocation::MakeWroBlkData( CTime opStartTime, CTime opEndTime)
{
	
	WRO_RESLIST *prlWroRes = NULL;

	for( int i = omWroRules.GetSize() - 1; i >=0; i--)
	{
		prlWroRes = &omWroRules[i];

		MakeBlkData( opStartTime, opEndTime, CString("WRO"), prlWroRes->Wnam );
		
	}

	return true;
}



bool SpotAllocation::MakeBlkData( CTime opStartTime, CTime opEndTime, CString &opTabn, CString &opName )
{
   
	CCSPtrArray<BLKDATA> *popBlkData;


	if (opStartTime <= TIMENULL)
		return false;

	if (opEndTime <= TIMENULL)
		return false;

	CTime opTrafficDay =TIMENULL;
	int ipDOO = -1;
	CString olDOO;


	CString olObjName;
	CString olUrno;

	if (strcmp(opTabn,"PST") == 0)
	{
		olObjName = "PNAM";
		PST_RESLIST *prlPstRes = GetPstRes(opName); 
		if(prlPstRes == NULL)
			return false;
		popBlkData = &prlPstRes->BlkData;
		popBlkData->DeleteAll();
	}

	if (strcmp(opTabn,"GAT") == 0)
	{
		olObjName = "GNAM";
		GAT_RESLIST *prlGatRes = GetGatRes(opName); 
		if(prlGatRes == NULL)
			return false;
		popBlkData = &prlGatRes->BlkData;
		popBlkData->DeleteAll();
	}


	if (strcmp(opTabn,"BLT") == 0)
	{
		olObjName = "BNAM";
		BLT_RESLIST *prlBltRes = GetBltRes(opName); 
		if(prlBltRes == NULL)
			return false;
		popBlkData = &prlBltRes->BlkData;
		popBlkData->DeleteAll();
	}

	if (strcmp(opTabn,"WRO") == 0)
	{
		olObjName = "WNAM";
		WRO_RESLIST *prlWroRes = GetWroRes(opName); 
		if(prlWroRes == NULL)
			return false;
		popBlkData = &prlWroRes->BlkData;
		popBlkData->DeleteAll();
	}



	if (strcmp(opTabn,"CIC") == 0)
		olObjName = "CNAM";
	if (strcmp(opTabn,"CHU") == 0)
		olObjName = "CNAM";

	ogBCD.GetField(opTabn, olObjName, opName, "URNO", olUrno);

	CString opObject = "BLK";
	int ilBlkUrnoIdx = ogBCD.GetFieldIndex(opObject,"URNO");
	int ilBlkBurnIdx = ogBCD.GetFieldIndex(opObject,"BURN");
	int ilBlkDaysIdx = ogBCD.GetFieldIndex(opObject,"DAYS");
	int ilBlkNafrIdx = ogBCD.GetFieldIndex(opObject,"NAFR");
	int ilBlkNatoIdx = ogBCD.GetFieldIndex(opObject,"NATO");
	int ilBlkResnIdx = ogBCD.GetFieldIndex(opObject,"RESN");
	int ilBlkTabnIdx = ogBCD.GetFieldIndex(opObject,"TABN");
	int ilBlkTypeIdx = ogBCD.GetFieldIndex(opObject,"TYPE");
	int ilBlkTifrIdx = ogBCD.GetFieldIndex(opObject,"TIFR");
	int ilBlkTitoIdx = ogBCD.GetFieldIndex(opObject,"TITO");
	int ilBlkIbitIdx = ogBCD.GetFieldIndex(opObject,"IBIT");


	CCSPtrArray<RecordSet> olRecSet;
	RecordSet olRec = NULL;
	olRecSet.RemoveAll();
	ogBCD.GetRecords(opObject, "BURN", olUrno, &olRecSet);

	for(int i = 0; i < olRecSet.GetSize(); i++)
	{
		CString olTabn = olRecSet[i].Values[ogBCD.GetFieldIndex(opObject, "TABN")];
		if (strcmp(olTabn, opTabn) == 0)
		{
			olRec = olRecSet.GetAt(i);

			CString olCStrIbit;
			olCStrIbit = olRec[ilBlkIbitIdx];
			int ilIbit = atoi(olCStrIbit);
			if ( ilIbit < 0 )
				ilIbit = 0;

			CString	olDays = olRec[ilBlkDaysIdx];

			CTime olNafr = DBStringToDateTime(olRec[ilBlkNafrIdx]);
			CTime olNato = DBStringToDateTime(olRec[ilBlkNatoIdx]);

			CTime olStartTime = opStartTime;		
			if (olNafr != TIMENULL && olStartTime < olNafr)
				olStartTime = olNafr;
						
			CTime olEndTime   = opEndTime;		
			if (olNato != TIMENULL && olEndTime > olNato)
				olEndTime = olNato;

			if (olStartTime == TIMENULL || olEndTime == TIMENULL)
				return false;

			olStartTime = CTime(olStartTime.GetYear(),olStartTime.GetMonth(),olStartTime.GetDay(),0,0,0);
			olEndTime   = CTime(olEndTime.GetYear(),olEndTime.GetMonth(),olEndTime.GetDay(),23,59,59);

			for (CTime olCurrent = olStartTime; olCurrent <= olEndTime; olCurrent += CTimeSpan(1,0,0,0))
			{
				int ilDayOfWeek = olCurrent.GetDayOfWeek() - 1;
				if (ilDayOfWeek == 0)
					ilDayOfWeek = 7;

				if (olDays.Find((char) (ilDayOfWeek + '0')) != -1)
				{
					CTime olTTifr = -1;
					CTime olTTito = -1;

					CString olStrTifr = CString(olRec[ilBlkTifrIdx]);
					CString olStrTito = CString(olRec[ilBlkTitoIdx]);
					olStrTifr.TrimLeft();
					olStrTifr.TrimRight();
					olStrTito.TrimLeft();
					olStrTito.TrimRight();

					if (olStrTifr.IsEmpty())
						olTTifr = CTime(olCurrent.GetYear(),olCurrent.GetMonth(),olCurrent.GetDay(),0,0,0);
					else
						olTTifr = HourStringToDate(olStrTifr,olCurrent);

					if (olStrTito.IsEmpty())
						olTTito = CTime(olCurrent.GetYear(),olCurrent.GetMonth(),olCurrent.GetDay(),23,59,59);
					else
					{
						olTTito = HourStringToDate(olStrTito,olCurrent);
						if (olTTito < olTTifr)
							olTTito = HourStringToDate(olStrTito,olCurrent+CTimeSpan(1,0,0,0));
					}


					CTime olTNafr = -1;
					CTime olTNato = -1;
					if (olNafr != TIMENULL && olCurrent.GetDay() == olNafr.GetDay())
						olTNafr = olNafr;
					else
						olTNafr = CTime(olCurrent.GetYear(),olCurrent.GetMonth(),olCurrent.GetDay(),0,0,0);

					if (olNato != TIMENULL && olCurrent.GetDay() == olNato.GetDay())
						olTNato = olNato;
					else
					{
						olTNato = olTTito;
						if (olNato < olTNato)
						{
							if (olNato != TIMENULL)
								olTNato = olNato;
						}
					}

					CTime olTBlkfr = olTNafr; 
					CTime olTBlkto = olTNato;
					if (olTTito != TIMENULL && olTNato != TIMENULL)
					{
						if (olCurrent.GetDay() == olEndTime.GetDay())
							olTBlkto = olTNato;
						else
							olTBlkto = olTTito;

						if (olNato < olTTito)
							olTBlkto = olTNato;

						if (olCurrent.GetDay() == olEndTime.GetDay())
						{
							if (olTTito < olTNato)
								olTBlkto = olTTito;
						}
					}

					if (olTTifr != TIMENULL && olTNafr != TIMENULL && olTTifr > olTNafr)
						olTBlkfr = olTTifr;

					if (olTBlkfr != TIMENULL && olTBlkto != TIMENULL)
					{
						// TLE: 21.08.2013 - Fix Diff Day
						// If Local Time and UTC Time are in different day, the Day is required to be shifted also.

						CTime olTmpTBlkfr = olTBlkfr;

						ogBasicData.UtcToLocal(olTmpTBlkfr);
						CTime olTmpLocalDate = CTime(olTmpTBlkfr.GetYear(), olTmpTBlkfr.GetMonth(), olTmpTBlkfr.GetDay(), 0,0,0);
						CTime olTmpUTCDate = CTime(olTBlkfr.GetYear(), olTBlkfr.GetMonth(), olTBlkfr.GetDay(), 0,0,0);

						if (olTmpLocalDate > olTmpUTCDate)
						{
							olTBlkfr -= CTimeSpan(1,0,0,0);
							olTBlkto -= CTimeSpan(1,0,0,0);
						}
						else if (olTmpLocalDate < olTmpUTCDate)
						{
							olTBlkfr += CTimeSpan(1,0,0,0);
							olTBlkto += CTimeSpan(1,0,0,0);
						}

						CTimeSpan olSpan =  olTBlkto - olTBlkfr;
						if (olSpan.GetTotalMinutes() > 0)
						{

							    BLKDATA *polBlkData = new BLKDATA();	
								polBlkData->StartTime = olTBlkfr;
								polBlkData->EndTime = olTBlkto;
								polBlkData->Text = olRec[ilBlkResnIdx];
								polBlkData->Type = olRec[ilBlkTypeIdx];
								polBlkData->Ibit = ilIbit;
								popBlkData->Add(polBlkData);
						}
					}
				}
			}

		}
	}
	olRecSet.DeleteAll();

	if(bgMergeBlk)
	{
		MergeBKBars(*popBlkData);
	}

	return true;
}



bool SpotAllocation::IsAvailable(const CString& ropType,const CString& ropName,CTime opStartAlloc,CTime opEndAlloc)
{
	if (opStartAlloc == TIMENULL || opEndAlloc == TIMENULL)
		return true;


	if(ropType == "PST")
	{
		PST_RESLIST *prlPstRes = GetPstRes(ropName); 

		if(prlPstRes == NULL)
			return true;
		
		return IsAvailable( prlPstRes->BlkData, opStartAlloc, opEndAlloc);
	}

	if(ropType == "GAT")
	{
		GAT_RESLIST *prlGatRes = GetGatRes(ropName); 

		if(prlGatRes == NULL)
			return true;
		
		return IsAvailable( prlGatRes->BlkData, opStartAlloc, opEndAlloc);
	}

	if(ropType == "BLT")
	{
		BLT_RESLIST *prlBltRes = GetBltRes(ropName); 

		if(prlBltRes == NULL)
			return true;
		
		return IsAvailable( prlBltRes->BlkData, opStartAlloc, opEndAlloc);
	}


	return true;
}


bool SpotAllocation::IsAvailable( CCSPtrArray<BLKDATA> &ropBlkData ,CTime opStartAlloc,CTime opEndAlloc)
{
	if (opStartAlloc == TIMENULL || opEndAlloc == TIMENULL)
		return true;

	CStringArray opNafr;
	CStringArray opNato;

	for (int il = 0; il < ropBlkData.GetSize(); il++)
	{
		CTime olNafr = ropBlkData[il].StartTime;
		CTime olNato = ropBlkData[il].EndTime;

		CString olVonStr = CTimeToDBString(olNafr, TIMENULL);
		CString olBisStr = CTimeToDBString(olNato, TIMENULL);

		if(olNafr != TIMENULL || olNato != TIMENULL)
		{	
			if(olNafr == TIMENULL)
				olNafr = CTime(1971,1,1,0,0,0);
			if(olNato == TIMENULL)
				olNato = CTime(2020,1,1,0,0,0);
		}


		if ((olNafr != TIMENULL) && (olNato != TIMENULL))
		{
			if (IsReallyOverlapped(opStartAlloc, opEndAlloc, olNafr, olNato))
			{
				return false;
			}
		}
	}

	return true;
}



void SpotAllocation::GetPreferredRemoteStandsToPosition(CString olPstUrno,CString &olRemoteStands)
{

	omPstRules.Sort(ComparePstResByPrio);

	PST_RESLIST *prlPstRes = NULL;

	for(int i = 0; i < omPstRules.GetSize(); i++)
	{
		prlPstRes = &omPstRules[i];

		if(omPstRules[i].Brgs != "X")
		{
			if(olRemoteStands == "")
				olRemoteStands = prlPstRes->Pnam;
			else
				olRemoteStands = olRemoteStands + "," + prlPstRes->Pnam;

		}
	}





/*	
	CString olAloUrno;
	olRemoteStands = "";
	ALODATA *polAloc = ogAloData.GetAloByName("POSPOS"); //SHA200702
	olAloUrno.Format("%d",polAloc->Urno);
	
	if(!olPstUrno.IsEmpty() && !olAloUrno.IsEmpty())
	{
		CString olSgrUrno;
		olSgrUrno = ogBCD.GetFieldExt("SGR","UGTY","STYP",CString(olAloUrno),CString(olPstUrno),"URNO");
		if (!olSgrUrno.IsEmpty())
		{
			CCSPtrArray<RecordSet> olSgmRecords;
			ogBCD.GetRecords("SGM","USGR",olSgrUrno,&olSgmRecords);

			CString olPstUrno;
			CString olPstName;
			CString olPstSort;
			CStringArray olPstSortList;
			
			for (int i = 0; i < olSgmRecords.GetSize(); i++)
			{
				olPstUrno = olSgmRecords[i].Values[ogBCD.GetFieldIndex("SGM", "UVAL")];
				olPstSort = olSgmRecords[i].Values[ogBCD.GetFieldIndex("SGM", "SORT")];
				if (ogBCD.GetField("PST","URNO",olPstUrno,"PNAM",olPstName))
					olPstSortList.InsertAt(atoi(olPstSort),olPstName);
			}

			olRemoteStands = "";
			for (int j = 0; j < olPstSortList.GetSize(); j++)
			{
				if(olPstSortList.GetAt(j) != "")
				{
					if(olRemoteStands == "")
						olRemoteStands = olPstSortList.GetAt(j);
					else
						olRemoteStands = olRemoteStands + "," + olPstSortList.GetAt(j);
				}
			}
		}
	}


	olRemoteStands.TrimLeft();

	// no preferd found - just take all remotestands
	if(olRemoteStands.IsEmpty())
	{

		int ilPnam = ogBCD.GetFieldIndex("PST", "PNAM");
		int ilBrgs = ogBCD.GetFieldIndex("PST", "BRGS");
		RecordSet rlRec;
		CString olPnam ;
		

		for(int i = 0; i < ogBCD.GetDataCount("PST"); i++)
		{
			ogBCD.GetRecord("PST", i, rlRec);

			if(rlRec[ilBrgs].IsEmpty())
			{
				if(olRemoteStands == "")
					olRemoteStands = rlRec[ilPnam];
				else
					olRemoteStands = olRemoteStands + "," + rlRec[ilPnam];
			}
		}

	}

*/
}



bool SpotAllocation::SetFlightOrderScore(DIAFLIGHTDATA *prpFlightA, DIAFLIGHTDATA *prpFlightD)
{

	PST_RESLIST *prlPstRes = NULL;
	int ilAScore = 0;
	int ilDScore = 0;

	bool blHasFlnoScore = false;
	bool blHasDestScore = false;
	bool blHasAlcScore = false;
	bool blHasACScore = false;

	int imPrefACScore = 50;

	bool blTowing = false;

	if(prpFlightA != NULL  && prpFlightD != NULL)
	{
		if(	(prpFlightA->Ftyp[0] == 'T' || prpFlightA->Ftyp[0] == 'G') &&	
			(prpFlightD->Ftyp[0] == 'T' || prpFlightD->Ftyp[0] == 'G'))	
			blTowing = true;	
	}
	
	DiaCedaFlightData::SCORELIST *prlRkey2;


	float flBestGoodCount;
	 
	int ilBestBadCount;

	for(int i = omPstRules.GetSize() - 1; i >=0; i--)
	{
		prlPstRes = &omPstRules[i];

		prlRkey2 = new DiaCedaFlightData::SCORELIST;

		if(prpFlightA != NULL)
			prlRkey2->Rotation.Add(prpFlightA);

		if(prpFlightD != NULL)
			prlRkey2->Rotation.Add(prpFlightD);

		omPosFlights.Add(prlRkey2);

		prlRkey2->Pnam = prlPstRes->Pnam; 

		flBestGoodCount = 0;
		GetMatchCountPst(prpFlightA, prpFlightD, prlPstRes, prlPstRes->PreferredAL, prlPstRes->PreferredAC, prlPstRes->PreferredNA, prlPstRes->PreferredService, prlPstRes->PreferredOrgDes, prlPstRes->PreferredFlightNo,  flBestGoodCount, ilBestBadCount);
		
		if(prlPstRes->Brgs.IsEmpty() && blTowing)
				flBestGoodCount += 10000000;


		prlRkey2->Score = (long)flBestGoodCount;

	}

	return true;
}



bool SpotAllocation::AutoPstAllocateFlightsGatPosNew(DIAFLIGHTDATA *prlFlightA, DIAFLIGHTDATA *prlFlightD, const CString &ropRecomPos, CString& ropAllocatedPos)
{

	if(CheckPst(prlFlightA, prlFlightD, ropRecomPos, NULL, &omParameters.omPstRightBuffer, &omParameters.omPstLeftBuffer, true))
	{
		if (CheckPosByGate(ropRecomPos, prlFlightA) && CheckPosByGate(ropRecomPos, prlFlightD))
		{
			AllocatePst(prlFlightA, prlFlightD, ropRecomPos);
			ropAllocatedPos = ropRecomPos;
			return true;
		}
	}
	return false;
}


bool SpotAllocation::AutoPstAllocateNew()
{
	omChangedUrnoMap.RemoveAll();
	omAutoAllocPst.RemoveAll();


 	DiaCedaFlightData::SCORELIST *prlRkey;
	DIAFLIGHTDATA *prlFlightA = NULL;
	DIAFLIGHTDATA *prlFlight = NULL;
 	DIAFLIGHTDATA *prlFlightD = NULL;
 
	CTime olStart;
	CTime olEnd;

	pomParent->SendMessage(WM_SAS_PROGESS_INIT, omPosFlights.GetSize(), 0 );
	TRACE("\n=======================================");


	CString olRecomPos;
	CStringArray olPosArray;


	for(int k=0; k<omPosFlights.GetSize(); k++)
	{
		olRecomPos.Empty();
		prlRkey = &omPosFlights[k];


		if(prlRkey->Rotation.GetSize() == 2)
		{
			prlFlightA = &prlRkey->Rotation[0];
			prlFlightD = &prlRkey->Rotation[1];
		}
		else
		{
			prlFlight = &prlRkey->Rotation[0];
			
			if(prlFlight != NULL && prlFlight->Adid[0] == 'D')	
				//prlFlightD = &prlRkey->Rotation[1];			// TLE: UFIS 3725
				prlFlightD = &prlRkey->Rotation[0];

			if(prlFlight != NULL && prlFlight->Adid[0] == 'A')	
				//prlFlightA = &prlRkey->Rotation[1];			// TLE: UFIS 3725
				prlFlightA = &prlRkey->Rotation[0];

		}


		//prlFlightA = ogPosDiaFlightData.GetFlightAInRotation(prlRkey);
		//prlFlightD = ogPosDiaFlightData.GetFlightDInRotation(prlRkey);
	
		pomParent->SendMessage(WM_SAS_PROGESS_UPDATE, 1, 0 );

		olRecomPos = prlRkey->Pnam;
		
/*
		if(prlFlightD != NULL && prlFlightA != NULL)
		{
			TRACE("\n %s -> %s %s %ld", prlFlightA->Flno, prlFlightA->Flno, olRecomPos,prlRkey->Score );

		}
  */
		if(prlFlightD != NULL)
		{
			if(!CString(prlFlightD->Pstd).IsEmpty())
			{
				continue;
			}
		}

		if(prlFlightA != NULL)
		{
			if(!CString(prlFlightA->Psta).IsEmpty())
			{
				continue;
			}
		}

		//TRACE("\n %s/%s %d %d", prlFlightA->Flno, prlFlightD->Flno, prlFlightA->Score, prlFlightD->Score);



		CString olAllocatedPos = "";
		CString olTmp;
		if(prlFlightD != NULL || prlFlightA != NULL)
		{
			AutoPstAllocateFlightsGatPosNew(prlFlightA, prlFlightD, olRecomPos, olAllocatedPos);
			olPosArray.Add(olAllocatedPos);
		}
	}
	return true;
}


long SpotAllocation::GetFlightScore(DIAFLIGHTDATA *prpFlightA, DIAFLIGHTDATA *prpFlightD, CString opPst)
{
	PST_RESLIST *prlPstRes = GetPstRes(opPst); 

	if(prlPstRes == NULL)
		return 0;

	float flBestGoodCount;
	 
	int ilBestBadCount;

	GetMatchCountPst(prpFlightA, prpFlightD, prlPstRes, prlPstRes->PreferredAL, prlPstRes->PreferredAC, prlPstRes->PreferredNA, prlPstRes->PreferredService, prlPstRes->PreferredOrgDes, prlPstRes->PreferredFlightNo,  flBestGoodCount, ilBestBadCount);

	return (long) flBestGoodCount;

}