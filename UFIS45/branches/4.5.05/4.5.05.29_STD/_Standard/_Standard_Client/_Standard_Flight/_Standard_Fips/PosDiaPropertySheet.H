// flplanps.h : header file
//



#ifndef _PosDiaPropertySheet_H_
#define _PosDiaPropertySheet_H_


#include <BasePropertySheet.h>
#include "PSUniFilterPage.h"
#include <PSSearchFlightPage.h>
//#include "PSZeitraumPage.h"
#include <PSGeometrie.h>
//#include "PSDispoRulesPage.h"
#include <StringConst.h>

//#include "PSunisortpage.h"
//#include "PSSortFlightPage.h"
//#include "PSspecfilterpage.h"

/////////////////////////////////////////////////////////////////////////////
// SeasonFlightTablePropertySheet

class PosDiaPropertySheet: public BasePropertySheet
{
// Construction
public:
	PosDiaPropertySheet(CString opCalledFrom, CWnd* pParentWnd = NULL,
		CViewer *popViewer = NULL, UINT iSelectPage = 0, LPCSTR pszCaption  = ID_SHEET_GATPOS_DIAGRAM);

	virtual void LoadDataFromViewer();
	virtual void SaveDataToViewer(CString opViewName,BOOL bpSaveToDb = TRUE);
	virtual int QueryForDiscardChanges();
	bool FilterChanged();

// Attributes
public:
	//TestPage m_TestPage;
	CPsUniFilter m_PSUniFilter;
	//CSpecFilterPage m_SpecialFilterPage;
	//CSearchFlightPage m_SearchFlightPage;
	CSearchFlightPage m_ZeitraumPage;
	//PSZeitraumPage    m_ZeitraumPage;
	PSGeometrie		  m_Geometrie;
	//PSDispoRulesPage  m_RulesPage;
	//CPSUniSortPage m_PSUniSortPage;
	//CSortFlightPage m_SpecialSortPage;
/*	FilterPage m_pageAirline;
	FilterPage m_pageArrival;
	FilterPage m_pageDeparture;
	FlightTableBoundFilterPage m_pageBound;
	FlightTableSortPage m_pageSort;
	ZeitPage m_pageZeit;
*/

};

/////////////////////////////////////////////////////////////////////////////

#endif // _SEASONFLIGHTTABLEPROPERTYSHEET_H_
