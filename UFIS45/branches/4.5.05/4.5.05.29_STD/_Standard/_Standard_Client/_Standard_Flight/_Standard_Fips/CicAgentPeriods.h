// CicAgentPeriods.h: interface for the CCicAgentPeriods class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_CICAGENTPERIODS_H__93CB324A_7171_4FD0_9EEE_3331A5352B2F__INCLUDED_)
#define AFX_CICAGENTPERIODS_H__93CB324A_7171_4FD0_9EEE_3331A5352B2F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

//#include <CCSGlobl.h>
#include <Afxcoll.h>
#include "CicAgentPeriod.h"

// 050321 MVy: to be more precisely I make a difference between ignore value (-1) on search and allow all values (0)
#define ANY_COUNTER_URNO -1

class CCicAgentPeriods : public CPtrArray
{
public:
	CCicAgentPeriods();
	virtual ~CCicAgentPeriods();

	CCicAgentPeriod* FindFirstByAgent( long &pos, long urno );
	CCicAgentPeriod* FindNextByAgent( long &pos, long urno );
	CCicAgentPeriod* FindFirstByCounter( long &pos, long urno );
	CCicAgentPeriod* FindNextByCounter( long &pos, long urno );
	CCicAgentPeriod* FindFirstByAgentAndCounter( long &pos, long urnoAgent, long urnoCounter );
	CCicAgentPeriod* FindNextByAgentAndCounter( long &pos, long urnoAgent, long urnoCounter );
	CCicAgentPeriod* FindFirstByTime( long &pos, const CTime &time );
	CCicAgentPeriod* FindNextByTime( long &pos, const CTime &time );
	bool AtEnd( long pos );		// for iteration, check if at last element
	void DeepKill();		// remove all items and their childs
	void GenerateDummies( unsigned long urnoAgent );		// helper function to generate some data for testing
	void Generate( unsigned long urnoAgent, CTimeRange *pRange = 0 );		// 050322 MVy: read data from DB regarding to specified handling agent
	CCicAgentPeriod* IsInRange( const CTime &time );		// return wheter specified time is within a period or not
	CCicAgentPeriod* IsInRange( const CTime &time, long urnoCounter );		// return wheter specified time is within a period or not, look for counter
	CCicAgentPeriod* IsInRange( const CTime &time, long urnoCounter, long urnoAgent );		// return wheter specified time is within a period or not, look for counter and agent; use ANY_COUNTER_URNO to ignore counter
	CCicAgentPeriod* CCicAgentPeriods::IsInRange( const CTimeRange &range );		// return wheter specified time range is completely within a period or not
	CCicAgentPeriod* CCicAgentPeriods::IsInRange( const CTimeRange &range, long urnoCounter );		// return wheter specified time range is completely within a period or not, look for counter
	CCicAgentPeriod* CCicAgentPeriods::IsInRange( const CTimeRange &range, long urnoCounter, long urnoAgent );		// return wheter specified time range is completely within a period or not, look for counter and agent; use ANY_COUNTER_URNO to ignore counter
	CCicAgentPeriod* IsOverlapping( const CTimeRange &range );		// return wheter specified timerange overlaps with a period or not
	CCicAgentPeriod* IsOverlapping( const CTimeRange &range, long urnoCounter );		// return wheter specified timerange overlaps with a period or not, look for counter
	CCicAgentPeriod* IsOverlapping( const CTimeRange &range, long urnoCounter, long urnoAgent );		// return wheter specified timerange overlaps with a period or not, look for counter and agent; use ANY_COUNTER_URNO to ignore counter

private:
	CBrush* m_pbrshBackground ;		// store background color / pattern for occupied ranges
	void DeleteBackBrush();		// use before generating new one
public:
	void SetBackBrush( CBrush* pBrush );		// set new brush
	void SetBackColor( unsigned long ulRGB );		// create brush with specified color
	CBrush* GetBackBrush();		// return pointer to current brush or null if no one was set
};

#endif // !defined(AFX_CICAGENTPERIODS_H__93CB324A_7171_4FD0_9EEE_3331A5352B2F__INCLUDED_)
