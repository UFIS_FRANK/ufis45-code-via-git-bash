// seasontableviewer.cpp : implementation file
// 
// Modification History: 


#include <stdafx.h>
#include <CicDemandTableViewer.h>
#include <CcsGlobl.h>
#include <iomanip.h>
#include <iostream.h>
#include <fstream.h>
#include <BasicData.h>
#include <CedaBasicData.h>
#include <DataSet.h>
#include <CcaDiagram.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif




// Local function prototype
static void FlightTableCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName);

#define IsBetween(val, start, end)  ((start) <= (val) && (val) <= (end))
#define IsOverlapped(start1, end1, start2, end2)    ((start1) <= (end2) && (start2) <= (end1))

////////////////////////////////////////////////////////////////////////////////////
// 

CICDEMANDTABLE_LINEDATA::CICDEMANDTABLE_LINEDATA()
{
	FlightUrno = 0;
	KKey = 0;
	Ckbs = -1;
	Ckes = -1;
	Urno = 0;
	Sto = -1;
	Sto2= -1;
	Freq = 0;
	Error = false;
};	// constructor

CICDEMANDTABLE_LINEDATA::~CICDEMANDTABLE_LINEDATA()
{
};	// destructor

//
////////////////////////////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////////////////////
// CicDemandTableViewer
//

CicDemandTableViewer::CicDemandTableViewer()
{
    pomTable = NULL;
	bmInit = false;
}


void CicDemandTableViewer::UnRegister()
{
	ogDdx.UnRegister(this, NOTUSED);
 
}

CicDemandTableViewer::~CicDemandTableViewer()
{
	ogDdx.UnRegister(this, NOTUSED);
    DeleteAll();
}

void CicDemandTableViewer::Attach(CCSTable *popTable)
{
    pomTable = popTable;
}

void CicDemandTableViewer::ClearAll()
{
	bmInit = false;
	ogDdx.UnRegister(this, NOTUSED);
    DeleteAll();
    pomTable->ResetContent();
}



void CicDemandTableViewer::ChangeViewTo(const char *pcpViewName)
{
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
	
	ogDdx.UnRegister(this, NOTUSED);

	ogDdx.Register(this, DIACCA_CHANGE, CString("DIACCA_CHANGE"), CString("DIACCA_CHANGE"), FlightTableCf);
	ogDdx.Register(this, DIACCA_DELETE, CString("DIACCA_CHANGE"), CString("DIACCA_CHANGE"), FlightTableCf);


    pomTable->ResetContent();
    DeleteAll();    
	MakeLines();
   	UpdateDisplay();

	bmInit = true;
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));

}







/////////////////////////////////////////////////////////////////////////////
// CicDemandTableViewer -- code specific to this class




bool CicDemandTableViewer::IsPassFilter(DIACCADATA *prpCca)
{
	bool blRet = false;

	if(prpCca != NULL)
	{
		if (prpCca->Ghsu == 0)
			return false;


		if(strcmp(prpCca->Ckic, "") == 0)
		{
			if( (strcmp(prpCca->Ctyp, "") == 0) &&
				(prpCca->Stat[4] == '0') &&
				(prpCca->Stat[5] == '0') &&
				(prpCca->Stat[6] == '0') &&
				(prpCca->Stat[7] == '0') &&
				(prpCca->Stat[8] == '0') &&
				(prpCca->Stat[9] == '0')/* &&
				(prpCca->IsChanged != DATA_DELETED)*/)
			{
				blRet = true;
			}
		}
	}
	return blRet;
}



bool CicDemandTableViewer::IsPassFilter(CCSPtrArray<DIACCADATA> &opCcas)
{
	if(opCcas.GetSize() == 0)
		return false;

	DIACCADATA *prpCca = &opCcas[0];

	bool blRet = false;
	if(prpCca != NULL)
	{
		if(strcmp(prpCca->Ckic, "") == 0)
		{
			if( (strcmp(prpCca->Ctyp, "") == 0) &&
				(prpCca->Stat[4] == '0') &&
				(prpCca->Stat[5] == '0') &&
				(prpCca->Stat[6] == '0') &&
				(prpCca->Stat[7] == '0') &&
				(prpCca->Stat[8] == '0') &&
				(prpCca->Stat[9] == '0')/* &&
				(prpCca->IsChanged != DATA_DELETED)*/)
			{
				blRet = true;
			}
		}
	}
	return blRet;
}




void CicDemandTableViewer::MakeLines()
{
	POSITION rlPos;
	CCSPtrArray<DIACCADATA> olCcas;

	CTimeSpan olStepInit (0,0,1439,59);
	CTime olFrom;
	CTime olTo;
	CTime olTempFrom;
	CTime olTempTo;
	if(bgPreviousDaycheckIn)
	{
		olFrom = ogCcaDiaFlightData.omFrom;
		olTo   = olFrom + olStepInit;

		if (olTo > ogCcaDiaFlightData.omTo)
			olTo = ogCcaDiaFlightData.omTo;

		
		CTimeSpan olTemp(0,0,1440,0);
		olTempFrom = olFrom + olTemp;
		olTempTo = olTo + olTemp ;
	}
		

	for ( rlPos = ogCcaDiaFlightData.omCcaData.omUrnoMap.GetStartPosition(); rlPos != NULL; )
	{
		DIACCADATA *prlCca;
		long llUrno;
		ogCcaDiaFlightData.omCcaData.omUrnoMap.GetNextAssoc(rlPos, (void *&)llUrno, (void *&)prlCca);
		if(prlCca != NULL)
		{
			//PRF 8084
			if(bgPreviousDaycheckIn)
			{
				if( prlCca->Stod != TIMENULL)
				{
				
					if(((prlCca->Stod >= olFrom) && (prlCca->Stod <= olTo ) )|| (((prlCca->Stod >= olTempFrom) && (prlCca->Stod <= olTempTo )) && ((prlCca->Ckbs <= olTo) && (prlCca->Ckes <= olTempTo))) )
					{
			          if(IsPassFilter(prlCca))
			          {
				        MakeLine(prlCca);
			          }
					}
				}
			}
			else
			{
				if(IsPassFilter(prlCca))
				{
					MakeLine(prlCca);
				}
			}
			
		}
	}
}


bool  CicDemandTableViewer::MakeLine(DIACCADATA *prpCca)
{
	if(prpCca != NULL)
	{
		CICDEMANDTABLE_LINEDATA rlLineData;
		MakeLineData(&rlLineData, prpCca);
		rlLineData.HandleCheckinCounterRestrictionsForHandlingAgents( prpCca );		// 050321 MVy: check current allocation wheter they are valid for current user / agent
		int ilLineNo = CreateLine(rlLineData);
		InsertDisplayLine(ilLineNo);
	}
	return true;
}


bool  CicDemandTableViewer::MakeLine( CCSPtrArray<DIACCADATA> &opCcas)
{
	CICDEMANDTABLE_LINEDATA rlLineData;
	MakeLineData(&rlLineData, opCcas);
	int ilLineNo = CreateLine(rlLineData);
	InsertDisplayLine(ilLineNo);
	return true;
}




	
void CicDemandTableViewer::MakeLineData(CICDEMANDTABLE_LINEDATA *prpLineData, CCSPtrArray<DIACCADATA> &opCcas)
{
	char buffer[65];

	if(opCcas.GetSize() > 0)
	{
		DIACCADATA *prpCca = &opCcas[0];

		prpLineData->Urno = prpCca->Urno;
		CCAFLIGHTDATA *prlFlight = ogCcaDiaFlightData.GetFlightByUrno(opCcas[0].Flnu);

		CCAFLIGHTDATA *prlFlight2;

		
		if(opCcas.GetSize() == 1)
			prlFlight2 = prlFlight;
		else
			prlFlight2 = ogCcaDiaFlightData.GetFlightByUrno(opCcas[opCcas.GetSize() - 1].Flnu);

		
		if(prlFlight != NULL)
		{

			prpLineData->FlightUrno = prlFlight->Urno;
			prpLineData->KKey = prpCca->KKey;
			prpLineData->Flno = prlFlight->Flno;

			prpLineData->Sto  = prlFlight->Stod;
			prpLineData->Sto2  = prlFlight2->Stod;
			prpLineData->Freq  = prlFlight2->Freq;

			prpLineData->Act3 = prlFlight->Act5;
			prpLineData->OrgDes = CString(prlFlight->Des3);
			prpLineData->Nose = prlFlight->Nose;
			prpLineData->Ftyp = prlFlight->Ftyp;

			prpLineData->Ckbs = prpCca->Ckbs;
			prpLineData->Ckes = prpCca->Ckes;
			prpLineData->Ckic = prpCca->Ckic;


			CString olGhpUrno;
			olGhpUrno.Format("%ld", prpCca->Ghpu);

			if(prpCca->Ghpu > 0)
			{
				prpLineData->Remark = ogBCD.GetField("GHP", "URNO", olGhpUrno, "PRNA");
				CString olCgru;
				olCgru = CString(prpCca->Cgru);
				CStringArray olStrArray;
				ExtractItemList(olCgru, &olStrArray, ';');
				int ilItemCount = olStrArray.GetSize();
				for(int i = 0; i < ilItemCount; i++)
				{
					prpLineData->CounterGroup += CString(" ") + ogBCD.GetField("GRN", "URNO", olStrArray[i], "GRPN");
				}

			}

			prpLineData->Error = false;

			if(prpCca->Ghpu != 0)
			{
				itoa(prpCca->Ghpu , buffer, 10);
				
				prpLineData->Remark = ogBCD.GetField("GHP", "URNO", CString(buffer), "PRSN");
			}


			if(prpCca->Ghsu != 0)
			{
				itoa(prpCca->Ghsu , buffer, 10);
				prpLineData->Remark += CString(" -> ") + ogBCD.GetField("GHS", "URNO", CString(buffer), "LKNM");
			}
		}
	}
}

void CicDemandTableViewer::MakeLineData(CICDEMANDTABLE_LINEDATA *prpLineData, DIACCADATA *prpCca)
{
	char buffer[65];

	if(prpCca != NULL)
	{
		prpLineData->Urno = prpCca->Urno;
		CCAFLIGHTDATA *prlFlight = ogCcaDiaFlightData.GetFlightByUrno(prpCca->Flnu);
		if(prlFlight != NULL)
		{

			prpLineData->FlightUrno = prlFlight->Urno;
			prpLineData->Flno = prlFlight->Flno;
			prpLineData->Sto  = prlFlight->Stod;
			prpLineData->Act3 = prlFlight->Act5;
			prpLineData->OrgDes = CString(prlFlight->Des3);
			prpLineData->Nose = prlFlight->Nose;
			prpLineData->Ftyp = prlFlight->Ftyp;

			prpLineData->Ckbs = prpCca->Ckbs;
			prpLineData->Ckes = prpCca->Ckes;
			prpLineData->Ckic = prpCca->Ckic;


			CString olGhpUrno;
			olGhpUrno.Format("%ld", prpCca->Ghpu);

			if(prpCca->Ghpu > 0)
			{
				prpLineData->Remark = ogBCD.GetField("GHP", "URNO", olGhpUrno, "PRNA");
				CString olCgru;
				olCgru = CString(prpCca->Cgru);
				CStringArray olStrArray;
				ExtractItemList(olCgru, &olStrArray, ';');
				int ilItemCount = olStrArray.GetSize();
				for(int i = 0; i < ilItemCount; i++)
				{
					prpLineData->CounterGroup += CString(" ") + ogBCD.GetField("GRN", "URNO", olStrArray[i], "GRPN");
				}

			}

			prpLineData->Error = false;

			if(prpCca->Ghpu != 0)
			{
				itoa(prpCca->Ghpu , buffer, 10);
				
				prpLineData->Remark = ogBCD.GetField("GHP", "URNO", CString(buffer), "PRSN");
			}


			if(prpCca->Ghsu != 0)
			{
				itoa(prpCca->Ghsu , buffer, 10);
				prpLineData->Remark += CString(" -> ") + ogBCD.GetField("GHS", "URNO", CString(buffer), "LKNM");
			}
		
			if (bgGatPosLocal)
			{
				UtcToLocal(*prpLineData);
			}
		}
	}
}





int CicDemandTableViewer::CreateLine(CICDEMANDTABLE_LINEDATA &rpLine)
{
    int ilLineCount = omLines.GetSize();

    for (int ilLineno = ilLineCount; ilLineno > 0; ilLineno--)
        //if (CompareFlight(&rpLine, &omLines[ilLineno-1]) >= 0)
		if (CompareLines(&rpLine, &omLines[ilLineno-1]) >= 0)
            break;  // should be inserted after Lines[ilLineno-1]

    omLines.NewAt(ilLineno, rpLine);
	return ilLineno;
}


void CicDemandTableViewer::DeleteLine(int ipLineno)
{
	omLines.DeleteAt(ipLineno);

	pomTable->DeleteTextLine(ipLineno);

}



bool CicDemandTableViewer::FindLine(long lpUrno, int &rilLineno)
{
	rilLineno = -1;
    for (int i = omLines.GetSize() - 1; i >= 0; i--)
	{
	  if((omLines[i].Urno == lpUrno) || (omLines[i].KKey == lpUrno))
	  {
		rilLineno = i;
		return true;
	  }
	}
	return false;
}






/////////////////////////////////////////////////////////////////////////////
// CicDemandTableViewer - CICDEMANDTABLE_LINEDATA array maintenance

void CicDemandTableViewer::DeleteAll()
{
    omLines.DeleteAll();
}


/////////////////////////////////////////////////////////////////////////////
// CicDemandTableViewer - display drawing routine



// UpdateDisplay: Load data selected by filter conditions to the display by using "omTable"
void CicDemandTableViewer::UpdateDisplay()
{
	pomTable->ResetContent();

	DrawHeader();

	CICDEMANDTABLE_LINEDATA *prlLine;
    
	for (int ilLc = 0; ilLc < omLines.GetSize(); ilLc++)
	{
		int ilColumnNo = 0;
		prlLine = &omLines[ilLc];
		CCSPtrArray<TABLE_COLUMN> olColList;
	
		MakeColList(prlLine, olColList);

		pomTable->AddTextLine(olColList, (void*)(prlLine));
		olColList.DeleteAll();
	}


    pomTable->DisplayTable();
}



void CicDemandTableViewer::InsertDisplayLine( int ipLineNo)
{
	if(!((ipLineNo >= 0) && (ipLineNo < omLines.GetSize())))
		return;
	CCSPtrArray<TABLE_COLUMN> olColList;
	MakeColList(&omLines[ipLineNo], olColList);
	pomTable->InsertTextLine(ipLineNo, olColList, &omLines[ipLineNo]);
	olColList.DeleteAll();
//YYY    pomTable->DisplayTable();
}






void CicDemandTableViewer::DrawHeader()
{
	CCSPtrArray <TABLE_HEADER_COLUMN> omHeaderDataArray;

	pomTable->SetShowSelection(true);
	pomTable->ResetContent();

	TABLE_HEADER_COLUMN rlHeader;
	rlHeader.Font = &ogCourier_Bold_10;

	rlHeader.Alignment = COLALIGN_CENTER;
	rlHeader.Length = 67; 
	rlHeader.Text =  GetString(IDS_STRING347);//CString("Flugnummer");
	omHeaderDataArray.New(rlHeader);


	rlHeader.Length = 40; 
	rlHeader.Alignment = COLALIGN_CENTER;
	rlHeader.Text = GetString(IDS_STRING316);//CString("STD");
	omHeaderDataArray.New(rlHeader);


	rlHeader.Length = 65; 
	rlHeader.Alignment = COLALIGN_CENTER;
	rlHeader.Text = GetString(IDS_STRING332);//CString("Datum");
	omHeaderDataArray.New(rlHeader);


	rlHeader.Alignment = COLALIGN_CENTER;
	rlHeader.Length = 30; 
	rlHeader.Text = GetString(IDS_STRING1447);//CString("ORG/DES");
	omHeaderDataArray.New(rlHeader);

	rlHeader.Alignment = COLALIGN_CENTER;
	rlHeader.Length = 30; 
	rlHeader.Text = GetString(IDS_STRING311);//CString("A/C");
	omHeaderDataArray.New(rlHeader);


	rlHeader.Alignment = COLALIGN_CENTER;
	rlHeader.Length = 40; 
	rlHeader.Text = GetString(IDS_STRING1073);//CString("Von");
	omHeaderDataArray.New(rlHeader);


	rlHeader.Alignment = COLALIGN_CENTER;
	rlHeader.Length = 40; 
	rlHeader.Text = GetString(HD_DSR_AVTA_TIME);//CString("bis");
	omHeaderDataArray.New(rlHeader);

	rlHeader.Alignment = COLALIGN_CENTER;
	rlHeader.Length = 50; 
	rlHeader.Text = GetString(IDS_STRING1503) ;// CString("Conf")
	omHeaderDataArray.New(rlHeader);

	rlHeader.Alignment = COLALIGN_CENTER;
	rlHeader.Length = 40; 
	rlHeader.Text = GetString(IDS_STRING1504) ;//CString("Ftyp")
	omHeaderDataArray.New(rlHeader);


	rlHeader.Alignment = COLALIGN_CENTER;
	rlHeader.Length = 300; 
	rlHeader.Text = GetString(IDS_STRING1506) ;//"Regel"
	omHeaderDataArray.New(rlHeader);

	rlHeader.Alignment = COLALIGN_CENTER;
	rlHeader.Length = 300; 
	rlHeader.Text = GetString(IDS_STRING1507) ;//"Counter Grp"
	omHeaderDataArray.New(rlHeader);


	pomTable->SetHeaderFields(omHeaderDataArray);
	omHeaderDataArray.DeleteAll();

	pomTable->SetDefaultSeparator();

}









void CicDemandTableViewer::MakeColList(CICDEMANDTABLE_LINEDATA *prlLine, CCSPtrArray<TABLE_COLUMN> &olColList)
{
		TABLE_COLUMN rlColumnData;

		if(prlLine->Error == true)
		{
			rlColumnData.TextColor = COLORREF(RED);
		}
		rlColumnData.VerticalSeparator = SEPA_NONE;
		rlColumnData.SeparatorType = SEPA_NONE;
		rlColumnData.Font = &ogCourier_Regular_9;
	
		rlColumnData.Alignment = COLALIGN_LEFT;
		rlColumnData.Text = prlLine->Flno;
		olColList.NewAt(olColList.GetSize(), rlColumnData);


		rlColumnData.Text = prlLine->Sto.Format("%H:%M");
		rlColumnData.Alignment = COLALIGN_LEFT;
		olColList.NewAt(olColList.GetSize(), rlColumnData);


		rlColumnData.Text = prlLine->Sto.Format("%d.%m.%y");
		rlColumnData.Alignment = COLALIGN_LEFT;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.Alignment = COLALIGN_LEFT;
		rlColumnData.Text = prlLine->OrgDes;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.Alignment = COLALIGN_LEFT;
		rlColumnData.Text = prlLine->Act3;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.Alignment = COLALIGN_LEFT;
		rlColumnData.Text = prlLine->Ckbs.Format("%H:%M");
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.Alignment = COLALIGN_LEFT;
		rlColumnData.Text = prlLine->Ckes.Format("%H:%M");
		olColList.NewAt(olColList.GetSize(), rlColumnData);
//MWO
		rlColumnData.Alignment = COLALIGN_LEFT;
		rlColumnData.Text = prlLine->Nose;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.Alignment = COLALIGN_LEFT;
		rlColumnData.Text = prlLine->Ftyp;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

//end MWO
		rlColumnData.Alignment = COLALIGN_LEFT;
		rlColumnData.Text = prlLine->Remark;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.Alignment = COLALIGN_LEFT;
		rlColumnData.Text = prlLine->CounterGroup;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		// 050321 MVy: set line background color
		for( int ndxCol = 0 ; ndxCol < olColList.GetSize() ; ndxCol++ )
		{
			olColList[ ndxCol ].BkColor = prlLine->clrBackground ;
		};		// for each column


}


static void FlightTableCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName)
{
    CicDemandTableViewer *polViewer = (CicDemandTableViewer *)popInstance;

	if(ipDDXType == DIACCA_CHANGE)
		polViewer->ProcessCcaChange((long *)vpDataPointer);

	if(ipDDXType == DIACCA_DELETE)
		polViewer->ProcessCcaDelete((long *)vpDataPointer);
	
	if(ipDDXType == DIACCA_CHANGE || ipDDXType == CCA_KKEY_CHANGE || ipDDXType == DIACCA_DELETE)
	{
		if (pogCcaDiagram)
			pogCcaDiagram->UpdateWoResButton();
	}
}

void CicDemandTableViewer::ProcessCcaDelete(long *plpCcaUrno)
{	
	DeleteLine(*plpCcaUrno);
}

void CicDemandTableViewer::ProcessCcaChange(long *plpCcaUrno)
{	
	DeleteLine(*plpCcaUrno);

	
	DIACCADATA *prlCca =ogCcaDiaFlightData.omCcaData.GetCcaByUrno(*plpCcaUrno);
	if(prlCca == NULL)
	{
		return;
	}

	if(IsPassFilter(prlCca))
	{
		MakeLine(prlCca);
	}
}






int CicDemandTableViewer::CompareFlight(CICDEMANDTABLE_LINEDATA *prpLine1, CICDEMANDTABLE_LINEDATA *prpLine2)
{
		return (prpLine1->Sto == prpLine2->Sto)? 0:	(prpLine1->Sto > prpLine2->Sto)? 1: -1;
}

int CicDemandTableViewer::CompareLines(CICDEMANDTABLE_LINEDATA *prpLine1, CICDEMANDTABLE_LINEDATA *prpLine2)
{

	return (prpLine1->Sto == prpLine2->Sto)? 0:	
		   (prpLine1->Sto > prpLine2->Sto)? 1: -1;
}





bool CicDemandTableViewer::DeleteLine(long lpUrno)
{
    for (int i = omLines.GetSize() - 1; i >= 0; i--)
	{
	  if((omLines[i].Urno == lpUrno) || (omLines[i].KKey == lpUrno))
	  {
		omLines.DeleteAt(i);
		pomTable->DeleteTextLine(i);
	  }
	}
	return true;
}



void CicDemandTableViewer::UtcToLocal(CICDEMANDTABLE_LINEDATA &rrpLineData)
{
	ogBasicData.UtcToLocal(rrpLineData.Sto);
	ogBasicData.UtcToLocal(rrpLineData.Sto2);
	ogBasicData.UtcToLocal(rrpLineData.Ckbs);
	ogBasicData.UtcToLocal(rrpLineData.Ckes);
}

int CicDemandTableViewer::GetDemCount ()
{
	return omLines.GetSize();
}
