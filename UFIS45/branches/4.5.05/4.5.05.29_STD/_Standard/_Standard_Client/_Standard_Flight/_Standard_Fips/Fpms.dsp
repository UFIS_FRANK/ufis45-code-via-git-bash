# Microsoft Developer Studio Project File - Name="FPMS" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Application" 0x0101

CFG=FPMS - WIN32 DEBUG
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "Fpms.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "Fpms.mak" CFG="FPMS - WIN32 DEBUG"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "FPMS - Win32 Release" (based on "Win32 (x86) Application")
!MESSAGE "FPMS - Win32 Debug" (based on "Win32 (x86) Application")
!MESSAGE "FPMS - Win32 MyDebug" (based on "Win32 (x86) Application")
!MESSAGE "FPMS - Win32 WatsonDebug" (based on "Win32 (x86) Application")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName "Fpms"
# PROP Scc_LocalPath "."
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "FPMS - Win32 Release"

# PROP BASE Use_MFC 2
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 1
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "c:\ufis_bin\Release"
# PROP Intermediate_Dir "c:\ufis_intermediate\Fpms\Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /MT /W3 /GX /O2 /I ".\\" /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /o "NUL" /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /o "NUL" /win32
# ADD BASE RSC /l 0x407 /d "NDEBUG" /d "_AFXDLL"
# ADD RSC /l 0x407 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /machine:I386
# ADD LINK32 c:\Ufis_Bin\release\ufis32.lib c:\ufis_Bin\ClassLib\release\ccsclass.lib Wsock32.lib c:\Ufis_Bin\ForaignLibs\HtmlHelp.lib /nologo /subsystem:windows /machine:I386 /out:"c:\ufis_bin\Release\Fips.exe"
# SUBTRACT LINK32 /pdb:none /map /debug

!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"

# PROP BASE Use_MFC 2
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 1
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "c:\ufis_bin\Debug"
# PROP Intermediate_Dir "c:\ufis_intermediate\Fpms\Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MDd /W3 /Gm /GX /Zi /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /MTd /W3 /Gm /GX /ZI /Od /I ".\\" /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "SATS_PERFORMANCE" /Fr /FD /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /o "NUL" /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /o "NUL" /win32
# ADD BASE RSC /l 0x407 /d "_DEBUG" /d "_AFXDLL"
# ADD RSC /l 0x809 /d "_DEBUG" /d "_ENGLISH"
# SUBTRACT RSC /x
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept
# ADD LINK32 c:\Ufis_Bin\debug\ufis32.lib c:\ufis_Bin\ClassLib\debug\ccsclass.lib Wsock32.lib c:\Ufis_Bin\ForaignLibs\HtmlHelp.lib /nologo /subsystem:windows /debug /machine:I386 /out:"c:\ufis_bin\Debug\FIPS.exe" /pdbtype:sept

!ELSEIF  "$(CFG)" == "FPMS - Win32 MyDebug"

# PROP BASE Use_MFC 1
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "FPMS___Win32_MyDebug0"
# PROP BASE Intermediate_Dir "FPMS___Win32_MyDebug0"
# PROP BASE Ignore_Export_Lib 0
# PROP BASE Target_Dir ""
# PROP Use_MFC 1
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "FPMS___Win32_MyDebug0"
# PROP Intermediate_Dir "FPMS___Win32_MyDebug0"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MTd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /FR /FD /c
# SUBTRACT BASE CPP /YX /Yc /Yu
# ADD CPP /nologo /MTd /W3 /Gm /GX /ZI /Od /I ".\\" /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /FR /FD /c
# SUBTRACT CPP /YX /Yc /Yu
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /o "NUL" /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /o "NUL" /win32
# ADD BASE RSC /l 0x809 /d "_DEBUG" /d "_ENGLISH"
# SUBTRACT BASE RSC /x
# ADD RSC /l 0x809 /d "_DEBUG" /d "_ENGLISH"
# SUBTRACT RSC /x
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 c:\Ufis_Bin\debug\ufis32.lib c:\ufis_Bin\ClassLib\debug\ccsclass.lib Wsock32.lib /nologo /subsystem:windows /debug /machine:I386 /out:"c:\ufis_bin\Debug\FIPS.exe" /pdbtype:sept
# ADD LINK32 c:\Ufis_Bin\debug\ufis32.lib c:\ufis_Bin\ClassLib\debug\ccsclass.lib c:\Ufis_Bin\debug\Wsock32.lib c:\Ufis_Bin\ForaignLibs\HtmlHelp.lib /nologo /subsystem:windows /debug /machine:I386 /out:"FPMS___Win32_MyDebug0\FIPS.exe" /pdbtype:sept
# SUBTRACT LINK32 /pdb:none

!ELSEIF  "$(CFG)" == "FPMS - Win32 WatsonDebug"

# PROP BASE Use_MFC 1
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "FPMS___Win32_WatsonDebug"
# PROP BASE Intermediate_Dir "FPMS___Win32_WatsonDebug"
# PROP BASE Ignore_Export_Lib 0
# PROP BASE Target_Dir ""
# PROP Use_MFC 1
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "c:\ufis_bin\Debug"
# PROP Intermediate_Dir "c:\ufis_intermediate\Fpms\Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MTd /W3 /Gm /GX /ZI /Od /I ".\\" /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "SATS_PERFORMANCE" /FR /FD /c
# SUBTRACT BASE CPP /YX /Yc /Yu
# ADD CPP /nologo /MTd /W3 /GX /Z7 /Od /I ".\\" /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "SATS_PERFORMANCE" /FR /FD /c
# SUBTRACT CPP /YX /Yc /Yu
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /o "NUL" /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /o "NUL" /win32
# ADD BASE RSC /l 0x809 /d "_DEBUG" /d "_ENGLISH"
# SUBTRACT BASE RSC /x
# ADD RSC /l 0x809 /d "_DEBUG" /d "_ENGLISH"
# SUBTRACT RSC /x
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 c:\Ufis_Bin\debug\ufis32.lib c:\ufis_Bin\ClassLib\debug\ccsclass.lib Wsock32.lib c:\Ufis_Bin\ForaignLibs\HtmlHelp.lib /nologo /subsystem:windows /debug /machine:I386 /out:"c:\ufis_bin\Debug\FIPS.exe" /pdbtype:sept
# ADD LINK32 c:\Ufis_Bin\debug\ufis32.lib c:\ufis_Bin\ClassLib\debug\ccsclass.lib Wsock32.lib c:\Ufis_Bin\ForaignLibs\HtmlHelp.lib /nologo /subsystem:windows /pdb:none /debug /machine:I386 /out:"c:\ufis_bin\Debug\FIPS.exe"

!ENDIF 

# Begin Target

# Name "FPMS - Win32 Release"
# Name "FPMS - Win32 Debug"
# Name "FPMS - Win32 MyDebug"
# Name "FPMS - Win32 WatsonDebug"
# Begin Group "Source"

# PROP Default_Filter ".cpp"
# Begin Source File

SOURCE=.\aatlogin.cpp
# End Source File
# Begin Source File

SOURCE=.\AcirrDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\AdditionalReport11TableViewer.cpp
# End Source File
# Begin Source File

SOURCE=.\AirportInformationDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\AllocateCca.cpp
# End Source File
# Begin Source File

SOURCE=.\AllocateCcaParameter.cpp
# End Source File
# Begin Source File

SOURCE=.\AllocationChangeDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\AltImportDelDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\AskBox.cpp
# End Source File
# Begin Source File

SOURCE=.\AwBasicDataDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\BackGround.cpp
# End Source File
# Begin Source File

SOURCE=.\BasePropertySheet.CPP
# End Source File
# Begin Source File

SOURCE=.\BasicData.cpp
# End Source File
# Begin Source File

SOURCE=.\BC_Monitor.cpp
# End Source File
# Begin Source File

SOURCE=.\BC_Monitor.h
# End Source File
# Begin Source File

SOURCE=.\bdpsuif.cpp
# End Source File
# Begin Source File

SOURCE=.\bdpsuif.h
# End Source File
# Begin Source File

SOURCE=.\BltCcaTableDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\BltCcaTableViewer.Cpp
# End Source File
# Begin Source File

SOURCE=.\BltChart.CPP
# End Source File
# Begin Source File

SOURCE=.\BltDiagram.cpp
# End Source File
# Begin Source File

SOURCE=.\BltDiaPropertySheet.CPP
# End Source File
# Begin Source File

SOURCE=.\BltDiaViewer.CPP
# End Source File
# Begin Source File

SOURCE=.\BltGantt.CPP
# End Source File
# Begin Source File

SOURCE=.\BltKonflikteDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\BltOverviewTableDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\BltOverviewTableViewer.Cpp
# End Source File
# Begin Source File

SOURCE=.\buttonlistdlg.cpp
# End Source File
# Begin Source File

SOURCE=.\CcaCedaFlightData.Cpp
# End Source File
# Begin Source File

SOURCE=.\CCAChangeTimes.cpp
# End Source File
# Begin Source File

SOURCE=.\CcaChart.CPP
# End Source File
# Begin Source File

SOURCE=.\CcaCommonDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\CcaCommonSearchDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\CcaCommonTableDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\CcaCommonTableViewer.cpp
# End Source File
# Begin Source File

SOURCE=.\CcaDiagram.cpp
# End Source File
# Begin Source File

SOURCE=.\CcaDiaPropertySheet.CPP
# End Source File
# Begin Source File

SOURCE=.\CcaDiaViewer.CPP
# End Source File
# Begin Source File

SOURCE=.\CCAExpandDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\CcaGantt.CPP
# End Source File
# Begin Source File

SOURCE=.\CCSGlobl.cpp
# End Source File
# Begin Source File

SOURCE=.\CCSPrint.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaAloData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaAloData.h
# End Source File
# Begin Source File

SOURCE=.\CedaApxData.cpp
# End Source File
# Begin Source File

SOURCE=..\..\_Standard_Share\_Standard_Classlib\CedaBasicData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaBlaData.Cpp
# End Source File
# Begin Source File

SOURCE=.\CedaBwaData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaCcaData.Cpp
# End Source File
# Begin Source File

SOURCE=.\CedaCfgData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaCflData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaChaData.Cpp
# End Source File
# Begin Source File

SOURCE=.\CedaCountData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaCraData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaDiaCcaData.Cpp
# End Source File
# Begin Source File

SOURCE=.\CedaFlightUtilsData.Cpp
# End Source File
# Begin Source File

SOURCE=.\CedaFlnoData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaFlzData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaFpeData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaGpaData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaGrmData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaHaiData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaHtaData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaImpFlightData.Cpp
# End Source File
# Begin Source File

SOURCE=.\CedaInfData.Cpp
# End Source File
# Begin Source File

SOURCE=.\CedaInitModuData.Cpp
# End Source File
# Begin Source File

SOURCE=.\CedaLoaTabData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaMopData.Cpp
# End Source File
# Begin Source File

SOURCE=.\CedaParData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaPcaData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaPcaData.h
# End Source File
# Begin Source File

SOURCE=.\CedaPlbData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaPlbData.h
# End Source File
# Begin Source File

SOURCE=.\CedaResGroupData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaValData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaVipData.cpp
# End Source File
# Begin Source File

SOURCE=.\ChaChart.CPP
# End Source File
# Begin Source File

SOURCE=.\ChaDiagram.cpp
# End Source File
# Begin Source File

SOURCE=.\ChaDiaPropertySheet.CPP
# End Source File
# Begin Source File

SOURCE=.\ChaDiaViewer.CPP
# End Source File
# Begin Source File

SOURCE=.\ChaGantt.CPP
# End Source File
# Begin Source File

SOURCE=.\CicAgentPeriod.cpp
# End Source File
# Begin Source File

SOURCE=.\CicAgentPeriods.cpp
# End Source File
# Begin Source File

SOURCE=.\CicConfTableDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\CicConfTableViewer.Cpp
# End Source File
# Begin Source File

SOURCE=.\CicDemandTableDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\CicDemandTableViewer.Cpp
# End Source File
# Begin Source File

SOURCE=.\CicNoDemandTableDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\CicNoDemandTableViewer.Cpp
# End Source File
# Begin Source File

SOURCE=.\CollectHAIDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\ConnectionAdvisor.cpp
# End Source File
# Begin Source File

SOURCE=.\CVIEWER.CPP
# End Source File
# Begin Source File

SOURCE=.\DailyCcaPrintDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\DailyCedaFlightData.Cpp
# End Source File
# Begin Source File

SOURCE=.\DailyScheduleTableDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\DailyTableViewer.cpp
# End Source File
# Begin Source File

SOURCE=.\DailyVipCedaData.cpp
# End Source File
# Begin Source File

SOURCE=.\DataSet.cpp
# End Source File
# Begin Source File

SOURCE=.\DelelteCounterDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\DGanttBarReport.cpp
# End Source File
# Begin Source File

SOURCE=.\DGanttChartReport.cpp
# End Source File
# Begin Source File

SOURCE=.\DGanttChartReportWnd.cpp
# End Source File
# Begin Source File

SOURCE=.\DiaCedaFlightData.Cpp
# End Source File
# Begin Source File

SOURCE=.\DiffPosTableDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\DiffPosTableViewer.Cpp
# End Source File
# Begin Source File

SOURCE=.\DlgResizeHelper.cpp
# End Source File
# Begin Source File

SOURCE=.\ExpandOverSeasons.cpp
# End Source File
# Begin Source File

SOURCE=.\FlightChart.CPP
# End Source File
# Begin Source File

SOURCE=.\FlightDiagram.cpp
# End Source File
# Begin Source File

SOURCE=.\FlightDiaPropertySheet.CPP
# End Source File
# Begin Source File

SOURCE=.\FlightDiaViewer.CPP
# End Source File
# Begin Source File

SOURCE=.\FlightGantt.CPP
# End Source File
# Begin Source File

SOURCE=.\FlightPermitDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\FlightSearchPropertySheet.CPP
# End Source File
# Begin Source File

SOURCE=.\FlightSearchTableDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\FlightSearchTableViewer.Cpp
# End Source File
# Begin Source File

SOURCE=.\FlnoAskDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\FPMS.cpp
# End Source File
# Begin Source File

SOURCE=.\GanttBarReport.cpp
# End Source File
# Begin Source File

SOURCE=.\GanttChartReport.cpp
# End Source File
# Begin Source File

SOURCE=.\GanttChartReportWnd.cpp
# End Source File
# Begin Source File

SOURCE=.\GatChart.CPP
# End Source File
# Begin Source File

SOURCE=.\GatDiagram.cpp
# End Source File
# Begin Source File

SOURCE=.\GatDiaPropertySheet.CPP
# End Source File
# Begin Source File

SOURCE=.\GatDiaViewer.CPP
# End Source File
# Begin Source File

SOURCE=.\GatGantt.CPP
# End Source File
# Begin Source File

SOURCE=.\GatPosConflictSetupDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\GridFenster.cpp
# End Source File
# Begin Source File

SOURCE=.\HAIDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\HTADlg.cpp
# End Source File
# Begin Source File

SOURCE=.\ImageButton.cpp
# End Source File
# Begin Source File

SOURCE=.\InfoDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\InitialLoadDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\Konflikte.cpp
# End Source File
# Begin Source File

SOURCE=.\KonflikteDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\List.cpp
# End Source File
# Begin Source File

SOURCE=.\ListBoxDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\ListStringArrayDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\LoginDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\ModeOfPay.cpp
# End Source File
# Begin Source File

SOURCE=.\MultiDelayDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\OvernightTableDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\OvernightTableViewer.Cpp
# End Source File
# Begin Source File

SOURCE=.\PopupImgMenu.cpp
# End Source File
# Begin Source File

SOURCE=.\PosChart.CPP
# End Source File
# Begin Source File

SOURCE=.\PosDiagram.cpp
# End Source File
# Begin Source File

SOURCE=.\PosDiaPropertySheet.CPP
# End Source File
# Begin Source File

SOURCE=.\PosDiaViewer.CPP
# End Source File
# Begin Source File

SOURCE=.\PosGantt.CPP
# End Source File
# Begin Source File

SOURCE=.\PosOverviewTableViewer.Cpp
# End Source File
# Begin Source File

SOURCE=.\PrintInfo.cpp
# End Source File
# Begin Source File

SOURCE=.\PrivList.cpp
# End Source File
# Begin Source File

SOURCE=.\ProgressDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\ProxyDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\PSGatGeometry.cpp
# End Source File
# Begin Source File

SOURCE=.\PSGeometrie.cpp
# End Source File
# Begin Source File

SOURCE=.\PSSearchFlightPage.cpp
# End Source File
# Begin Source File

SOURCE=.\PSSearchPosDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\PSSortFlightPage.cpp
# End Source File
# Begin Source File

SOURCE=.\PSSpecFilterPage.cpp
# End Source File
# Begin Source File

SOURCE=.\PSUniFilterPage.cpp
# End Source File
# Begin Source File

SOURCE=.\PSUniSortPage.cpp
# End Source File
# Begin Source File

SOURCE=.\ReasonOverviewDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\RegisterDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\Report16TableViewer.Cpp
# End Source File
# Begin Source File

SOURCE=.\Report19DailyCcaTableViewer.cpp
# End Source File
# Begin Source File

SOURCE=.\Report1TableViewer.Cpp
# End Source File
# Begin Source File

SOURCE=.\Report6NatCedaData.cpp
# End Source File
# Begin Source File

SOURCE=.\ReportActCedaData.cpp
# End Source File
# Begin Source File

SOURCE=.\ReportArrDepLoadPaxTableViewer.cpp
# End Source File
# Begin Source File

SOURCE=.\ReportArrDepSelctDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\ReportDailyFlightLogViewer.Cpp
# End Source File
# Begin Source File

SOURCE=.\ReportFlightDelayViewer.Cpp
# End Source File
# Begin Source File

SOURCE=.\ReportFlightGPUTableViewer.cpp
# End Source File
# Begin Source File

SOURCE=.\ReportMovements.cpp
# End Source File
# Begin Source File

SOURCE=.\ReportOvernightTableViewer.cpp
# End Source File
# Begin Source File

SOURCE=.\ReportPOPSItems.cpp
# End Source File
# Begin Source File

SOURCE=.\ReportReasonForChange.cpp
# End Source File
# Begin Source File

SOURCE=.\ReportReasonForChangeViewer.cpp
# End Source File
# Begin Source File

SOURCE=.\ReportSameACTTableViewer.cpp
# End Source File
# Begin Source File

SOURCE=.\ReportSameDESTableViewer.cpp
# End Source File
# Begin Source File

SOURCE=.\ReportSameORGTableViewer.cpp
# End Source File
# Begin Source File

SOURCE=.\ReportSameREGNTableViewer.cpp
# End Source File
# Begin Source File

SOURCE=.\ReportSameTTYPTableViewer.cpp
# End Source File
# Begin Source File

SOURCE=.\ReportSelectDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\ReportSeq1.cpp
# End Source File
# Begin Source File

SOURCE=.\ReportSeqDateTimeDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\ReportSeqDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\ReportSeqField3DateTimeDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\ReportSeqFieldDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\ReportTableDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\ReportXXXCedaData.cpp
# End Source File
# Begin Source File

SOURCE=.\ResGroupDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\RotationActDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\RotationAltDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\RotationAptDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\RotationCedaFlightData.Cpp
# End Source File
# Begin Source File

SOURCE=.\RotationDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\RotationDlgCedaFlightData.Cpp
# End Source File
# Begin Source File

SOURCE=.\RotationGpuDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\RotationHAIDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\RotationHTADlg.cpp
# End Source File
# Begin Source File

SOURCE=.\RotationISFDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\RotationJoinDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\RotationLoadDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\RotationLoadDlgATH.cpp
# End Source File
# Begin Source File

SOURCE=".\RotationLoadDlgWAW .cpp"
# End Source File
# Begin Source File

SOURCE=.\RotationPcaDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\RotationPcaDlg.h
# End Source File
# Begin Source File

SOURCE=.\RotationPlbDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\RotationPlbDlg.h
# End Source File
# Begin Source File

SOURCE=.\RotationRegnDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\RotationTableChart.cpp
# End Source File
# Begin Source File

SOURCE=.\RotationTableReportSelect.cpp
# End Source File
# Begin Source File

SOURCE=.\RotationTableReportSelectExcel.cpp
# End Source File
# Begin Source File

SOURCE=.\RotationTables.cpp
# End Source File
# Begin Source File

SOURCE=.\RotationTableViewer.cpp
# End Source File
# Begin Source File

SOURCE=.\RotationViaDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\RotationVipDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\RotGDlgCedaFlightData.Cpp
# End Source File
# Begin Source File

SOURCE=.\RotGroundDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\SeasonAskDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\SeasonCedaFlightData.Cpp
# End Source File
# Begin Source File

SOURCE=.\SeasonCollectADAskDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\SeasonCollectAskDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\SeasonCollectCedaFlightData.Cpp
# End Source File
# Begin Source File

SOURCE=.\SeasonCollectDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\SeasonDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\SeasonDlgCedaFlightData.Cpp
# End Source File
# Begin Source File

SOURCE=.\SeasonFlightPropertySheet.CPP
# End Source File
# Begin Source File

SOURCE=.\SeasonTableDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\SeasonTableViewer.Cpp
# End Source File
# Begin Source File

SOURCE=.\SetupDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\SpecialConflictDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\SpotAllocateDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\SpotAllocation.cpp
# End Source File
# Begin Source File

SOURCE=.\StdAfx.cpp

!IF  "$(CFG)" == "FPMS - Win32 Release"

!ELSEIF  "$(CFG)" == "FPMS - Win32 Debug"

# ADD CPP /Yc"stdafx.h"

!ELSEIF  "$(CFG)" == "FPMS - Win32 MyDebug"

# ADD BASE CPP /Yc"stdafx.h"
# ADD CPP /Yc"stdafx.h"

!ELSEIF  "$(CFG)" == "FPMS - Win32 WatsonDebug"

# ADD BASE CPP /Yc"stdafx.h"
# ADD CPP /Yc"stdafx.h"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\..\_Standard_Share\_Standard_Wrapper\tab.cpp
# End Source File
# Begin Source File

SOURCE=.\TABLE.CPP
# End Source File
# Begin Source File

SOURCE=.\TableLineData.cpp
# End Source File
# Begin Source File

SOURCE=.\TimeRange.cpp
# End Source File
# Begin Source File

SOURCE=.\UFISAmSink.cpp
# End Source File
# Begin Source File

SOURCE=..\..\_Standard_Share\_Standard_Wrapper\ufiscom.cpp
# End Source File
# Begin Source File

SOURCE=.\UndoCkiDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\UndoDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\UniEingabe.cpp
# End Source File
# Begin Source File

SOURCE=.\Utils.cpp
# End Source File
# Begin Source File

SOURCE=.\ViaTableCtrl.cpp
# End Source File
# Begin Source File

SOURCE=.\VIPDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\WoResTableDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\WoResTableViewer.Cpp
# End Source File
# Begin Source File

SOURCE=.\WroChart.CPP
# End Source File
# Begin Source File

SOURCE=.\WroDiagram.cpp
# End Source File
# Begin Source File

SOURCE=.\WroDiaPropertySheet.CPP
# End Source File
# Begin Source File

SOURCE=.\WroDiaViewer.CPP
# End Source File
# Begin Source File

SOURCE=.\WroGantt.CPP
# End Source File
# End Group
# Begin Group "Header"

# PROP Default_Filter ".h"
# Begin Source File

SOURCE=.\aatlogin.h
# End Source File
# Begin Source File

SOURCE=.\AcirrDlg.h
# End Source File
# Begin Source File

SOURCE=.\AdditionalReport11TableViewer.h
# End Source File
# Begin Source File

SOURCE=.\AirportInformationDlg.h
# End Source File
# Begin Source File

SOURCE=.\AllocateCca.h
# End Source File
# Begin Source File

SOURCE=.\AllocateCcaParameter.h
# End Source File
# Begin Source File

SOURCE=.\AllocationChangeDlg.h
# End Source File
# Begin Source File

SOURCE=.\AltImportDelDlg.h
# End Source File
# Begin Source File

SOURCE=.\Ansicht.h
# End Source File
# Begin Source File

SOURCE=.\AskBox.h
# End Source File
# Begin Source File

SOURCE=.\AwBasicDataDlg.h
# End Source File
# Begin Source File

SOURCE=.\BackGround.h
# End Source File
# Begin Source File

SOURCE=.\BasePropertySheet.H
# End Source File
# Begin Source File

SOURCE=.\BasicData.h
# End Source File
# Begin Source File

SOURCE=.\BltCcaTableDlg.h
# End Source File
# Begin Source File

SOURCE=.\BltCcaTableViewer.H
# End Source File
# Begin Source File

SOURCE=.\BltChart.H
# End Source File
# Begin Source File

SOURCE=.\BltDiagram.h
# End Source File
# Begin Source File

SOURCE=.\BltDiaPropertySheet.H
# End Source File
# Begin Source File

SOURCE=.\BltDiaViewer.H
# End Source File
# Begin Source File

SOURCE=.\BltGantt.H
# End Source File
# Begin Source File

SOURCE=.\BltKonflikteDlg.h
# End Source File
# Begin Source File

SOURCE=.\BltOverviewTableDlg.h
# End Source File
# Begin Source File

SOURCE=.\BltOverviewTableViewer.H
# End Source File
# Begin Source File

SOURCE=.\buttonlistdlg.h
# End Source File
# Begin Source File

SOURCE=.\CcaCedaFlightData.H
# End Source File
# Begin Source File

SOURCE=.\CCAChangeTimes.h
# End Source File
# Begin Source File

SOURCE=.\CcaChart.H
# End Source File
# Begin Source File

SOURCE=.\CcaCommonDlg.h
# End Source File
# Begin Source File

SOURCE=.\CcaCommonSearchDlg.h
# End Source File
# Begin Source File

SOURCE=.\CcaCommonTableDlg.h
# End Source File
# Begin Source File

SOURCE=.\CcaCommonTableViewer.h
# End Source File
# Begin Source File

SOURCE=.\CcaDiagram.h
# End Source File
# Begin Source File

SOURCE=.\CcaDiaPropertySheet.H
# End Source File
# Begin Source File

SOURCE=.\CcaDiaViewer.H
# End Source File
# Begin Source File

SOURCE=.\CCAExpandDlg.h
# End Source File
# Begin Source File

SOURCE=.\CcaGantt.H
# End Source File
# Begin Source File

SOURCE=.\CColor.h
# End Source File
# Begin Source File

SOURCE=.\CCSGLOBL.H
# End Source File
# Begin Source File

SOURCE=.\CCSPrint.h
# End Source File
# Begin Source File

SOURCE=.\CedaApxData.h
# End Source File
# Begin Source File

SOURCE=.\CedaBlaData.h
# End Source File
# Begin Source File

SOURCE=.\CedaBwaData.h
# End Source File
# Begin Source File

SOURCE=.\CedaCcaData.h
# End Source File
# Begin Source File

SOURCE=.\CedaCfgData.h
# End Source File
# Begin Source File

SOURCE=.\CedaCflData.h
# End Source File
# Begin Source File

SOURCE=.\CedaChaData.h
# End Source File
# Begin Source File

SOURCE=.\CedaCountData.h
# End Source File
# Begin Source File

SOURCE=.\CedaCraData.h
# End Source File
# Begin Source File

SOURCE=.\CedaDiaCcaData.h
# End Source File
# Begin Source File

SOURCE=.\CedaFlightUtilsData.H
# End Source File
# Begin Source File

SOURCE=.\CedaFlnoData.h
# End Source File
# Begin Source File

SOURCE=.\CedaFlzData.h
# End Source File
# Begin Source File

SOURCE=.\CedaFpeData.h
# End Source File
# Begin Source File

SOURCE=.\CedaGpaData.h
# End Source File
# Begin Source File

SOURCE=.\CedaGrmData.h
# End Source File
# Begin Source File

SOURCE=.\CedaHaiData.h
# End Source File
# Begin Source File

SOURCE=.\CedaHtaData.h
# End Source File
# Begin Source File

SOURCE=.\CedaImpFlightData.h
# End Source File
# Begin Source File

SOURCE=.\CedaInfData.h
# End Source File
# Begin Source File

SOURCE=.\CedaInitModuData.h
# End Source File
# Begin Source File

SOURCE=.\CedaLoaTabData.h
# End Source File
# Begin Source File

SOURCE=.\CedaMopData.h
# End Source File
# Begin Source File

SOURCE=.\CedaParData.h
# End Source File
# Begin Source File

SOURCE=.\CedaResGroupData.h
# End Source File
# Begin Source File

SOURCE=.\CedaValData.h
# End Source File
# Begin Source File

SOURCE=.\CedaVipData.h
# End Source File
# Begin Source File

SOURCE=.\ChaChart.H
# End Source File
# Begin Source File

SOURCE=.\ChaDiagram.h
# End Source File
# Begin Source File

SOURCE=.\ChaDiaPropertySheet.H
# End Source File
# Begin Source File

SOURCE=.\ChaDiaViewer.H
# End Source File
# Begin Source File

SOURCE=.\ChaGantt.H
# End Source File
# Begin Source File

SOURCE=.\CicAgentPeriod.h
# End Source File
# Begin Source File

SOURCE=.\CicAgentPeriods.h
# End Source File
# Begin Source File

SOURCE=.\CicConfTableDlg.h
# End Source File
# Begin Source File

SOURCE=.\CicConfTableViewer.H
# End Source File
# Begin Source File

SOURCE=.\CicDemandTableDlg.h
# End Source File
# Begin Source File

SOURCE=.\CicDemandTableViewer.H
# End Source File
# Begin Source File

SOURCE=.\CicNoDemandTableDlg.h
# End Source File
# Begin Source File

SOURCE=.\CicNoDemandTableViewer.H
# End Source File
# Begin Source File

SOURCE=.\ClntTypes.h
# End Source File
# Begin Source File

SOURCE=.\CollectHAIDlg.h
# End Source File
# Begin Source File

SOURCE=.\ConnectionAdvisor.h
# End Source File
# Begin Source File

SOURCE=.\CVIEWER.H
# End Source File
# Begin Source File

SOURCE=.\DailyCcaPrintDlg.h
# End Source File
# Begin Source File

SOURCE=.\DailyCedaFlightData.H
# End Source File
# Begin Source File

SOURCE=.\DailyScheduleTableDlg.h
# End Source File
# Begin Source File

SOURCE=.\DailyTableViewer.H
# End Source File
# Begin Source File

SOURCE=.\DailyVipCedaData.h
# End Source File
# Begin Source File

SOURCE=.\DATASET.H
# End Source File
# Begin Source File

SOURCE=.\DelelteCounterDlg.h
# End Source File
# Begin Source File

SOURCE=.\DGanttBarReport.h
# End Source File
# Begin Source File

SOURCE=.\DGanttChartReport.h
# End Source File
# Begin Source File

SOURCE=.\DGanttChartReportWnd.h
# End Source File
# Begin Source File

SOURCE=.\DiaCedaFlightData.H
# End Source File
# Begin Source File

SOURCE=.\DiffPosTableDlg.h
# End Source File
# Begin Source File

SOURCE=.\DiffPosTableViewer.H
# End Source File
# Begin Source File

SOURCE=.\DlgResizeHelper.h
# End Source File
# Begin Source File

SOURCE=.\ExpandOverSeasons.h
# End Source File
# Begin Source File

SOURCE=.\FlightChart.H
# End Source File
# Begin Source File

SOURCE=.\FlightDiagram.h
# End Source File
# Begin Source File

SOURCE=.\FlightDiaPropertySheet.H
# End Source File
# Begin Source File

SOURCE=.\FlightDiaViewer.H
# End Source File
# Begin Source File

SOURCE=.\FlightGantt.H
# End Source File
# Begin Source File

SOURCE=.\FlightPermitDlg.h
# End Source File
# Begin Source File

SOURCE=.\FlightSearchPropertySheet.H
# End Source File
# Begin Source File

SOURCE=.\FlightSearchTableDlg.h
# End Source File
# Begin Source File

SOURCE=.\FlightSearchTableViewer.H
# End Source File
# Begin Source File

SOURCE=.\FlnoAskDlg.h
# End Source File
# Begin Source File

SOURCE=.\FPMS.h
# End Source File
# Begin Source File

SOURCE=.\GANTT.H
# End Source File
# Begin Source File

SOURCE=.\GanttBarReport.h
# End Source File
# Begin Source File

SOURCE=.\GanttChartReport.h
# End Source File
# Begin Source File

SOURCE=.\GanttChartReportWnd.h
# End Source File
# Begin Source File

SOURCE=.\GatChart.H
# End Source File
# Begin Source File

SOURCE=.\GatDiagram.h
# End Source File
# Begin Source File

SOURCE=.\GatDiaPropertySheet.H
# End Source File
# Begin Source File

SOURCE=.\GatDiaViewer.H
# End Source File
# Begin Source File

SOURCE=.\GatGantt.H
# End Source File
# Begin Source File

SOURCE=.\GatPosConflictSetupDlg.h
# End Source File
# Begin Source File

SOURCE=.\GridFenster.h
# End Source File
# Begin Source File

SOURCE=.\HAIDlg.h
# End Source File
# Begin Source File

SOURCE=.\HTADlg.h
# End Source File
# Begin Source File

SOURCE=.\ImageButton.h
# End Source File
# Begin Source File

SOURCE=.\InfoDlg.h
# End Source File
# Begin Source File

SOURCE=.\InitialLoadDlg.h
# End Source File
# Begin Source File

SOURCE=.\Konflikte.h
# End Source File
# Begin Source File

SOURCE=.\KonflikteDlg.h
# End Source File
# Begin Source File

SOURCE=.\List.h
# End Source File
# Begin Source File

SOURCE=.\ListBoxDlg.h
# End Source File
# Begin Source File

SOURCE=.\ListStringArrayDlg.h
# End Source File
# Begin Source File

SOURCE=.\LoginDlg.h
# End Source File
# Begin Source File

SOURCE=.\ModeOfPay.h
# End Source File
# Begin Source File

SOURCE=.\MultiDelayDlg.h
# End Source File
# Begin Source File

SOURCE=.\OvernightTableDlg.h
# End Source File
# Begin Source File

SOURCE=.\OvernightViewer.H
# End Source File
# Begin Source File

SOURCE=.\PopupImgMenu.h
# End Source File
# Begin Source File

SOURCE=.\PosChart.H
# End Source File
# Begin Source File

SOURCE=.\PosDiagram.h
# End Source File
# Begin Source File

SOURCE=.\PosDiaPropertySheet.H
# End Source File
# Begin Source File

SOURCE=.\PosDiaViewer.H
# End Source File
# Begin Source File

SOURCE=.\PosGantt.H
# End Source File
# Begin Source File

SOURCE=.\PosOverviewTableViewer.H
# End Source File
# Begin Source File

SOURCE=.\PrintInfo.h
# End Source File
# Begin Source File

SOURCE=.\PrivList.h
# End Source File
# Begin Source File

SOURCE=.\ProgressDlg.h
# End Source File
# Begin Source File

SOURCE=.\ProxyDlg.h
# End Source File
# Begin Source File

SOURCE=.\PSGatGeometry.h
# End Source File
# Begin Source File

SOURCE=.\PSGeometrie.h
# End Source File
# Begin Source File

SOURCE=.\PSSearchFlightPage.h
# End Source File
# Begin Source File

SOURCE=.\PSSortFlightPage.h
# End Source File
# Begin Source File

SOURCE=.\PSSpecFilterPage.h
# End Source File
# Begin Source File

SOURCE=.\PSUniFilterPage.h
# End Source File
# Begin Source File

SOURCE=.\PSUniSortPage.h
# End Source File
# Begin Source File

SOURCE=.\ReasonOverviewDlg.h
# End Source File
# Begin Source File

SOURCE=.\RegisterDlg.h
# End Source File
# Begin Source File

SOURCE=.\Report16TableViewer.h
# End Source File
# Begin Source File

SOURCE=.\Report19DailyCcaTableViewer.h
# End Source File
# Begin Source File

SOURCE=.\Report1TableViewer.h
# End Source File
# Begin Source File

SOURCE=.\Report6NatCedaData.h
# End Source File
# Begin Source File

SOURCE=.\ReportActCedaData.h
# End Source File
# Begin Source File

SOURCE=.\ReportArrDepLoadPaxTableViewer.h
# End Source File
# Begin Source File

SOURCE=.\ReportArrDepSelctDlg.h
# End Source File
# Begin Source File

SOURCE=.\ReportDailyFlightLogViewer.h
# End Source File
# Begin Source File

SOURCE=.\ReportFlightDelayViewer.h
# End Source File
# Begin Source File

SOURCE=.\ReportFlightGPUTableViewer.h
# End Source File
# Begin Source File

SOURCE=.\ReportMovements.h
# End Source File
# Begin Source File

SOURCE=.\ReportOvernightTableViewer.h
# End Source File
# Begin Source File

SOURCE=.\ReportPOPSItems.h
# End Source File
# Begin Source File

SOURCE=.\ReportReasonForChange.h
# End Source File
# Begin Source File

SOURCE=.\ReportReasonForChangeViewer.h
# End Source File
# Begin Source File

SOURCE=.\ReportSameACTTableViewer.h
# End Source File
# Begin Source File

SOURCE=.\ReportSameDESTableViewer.h
# End Source File
# Begin Source File

SOURCE=.\ReportSameORGTableViewer.h
# End Source File
# Begin Source File

SOURCE=.\ReportSameREGNTableViewer.h
# End Source File
# Begin Source File

SOURCE=.\ReportSameTTYPTableViewer.h
# End Source File
# Begin Source File

SOURCE=.\ReportSelectDlg.h
# End Source File
# Begin Source File

SOURCE=.\ReportSeq1.h
# End Source File
# Begin Source File

SOURCE=.\ReportSeqDateTimeDlg.h
# End Source File
# Begin Source File

SOURCE=.\ReportSeqDlg.h
# End Source File
# Begin Source File

SOURCE=.\ReportSeqField3DateTimeDlg.h
# End Source File
# Begin Source File

SOURCE=.\ReportSeqFieldDlg.h
# End Source File
# Begin Source File

SOURCE=.\ReportTableDlg.h
# End Source File
# Begin Source File

SOURCE=.\ReportXXXCedaData.h
# End Source File
# Begin Source File

SOURCE=.\ResGroupDlg.h
# End Source File
# Begin Source File

SOURCE=.\resource.h
# End Source File
# Begin Source File

SOURCE=.\resrc1.h
# End Source File
# Begin Source File

SOURCE=.\RotationActDlg.h
# End Source File
# Begin Source File

SOURCE=.\RotationAltDlg.h
# End Source File
# Begin Source File

SOURCE=.\RotationAptDlg.h
# End Source File
# Begin Source File

SOURCE=.\RotationCedaFlightData.H
# End Source File
# Begin Source File

SOURCE=.\RotationDlg.h
# End Source File
# Begin Source File

SOURCE=.\RotationDlgCedaFlightData.H
# End Source File
# Begin Source File

SOURCE=.\RotationGpuDlg.h
# End Source File
# Begin Source File

SOURCE=.\RotationHAIDlg.h
# End Source File
# Begin Source File

SOURCE=.\RotationHTADlg.h
# End Source File
# Begin Source File

SOURCE=.\RotationISFDlg.h
# End Source File
# Begin Source File

SOURCE=.\RotationJoinDlg.h
# End Source File
# Begin Source File

SOURCE=.\RotationLoadDlg.h
# End Source File
# Begin Source File

SOURCE=.\RotationLoadDlgATH.h
# End Source File
# Begin Source File

SOURCE=.\RotationLoadDlgWAW.h
# End Source File
# Begin Source File

SOURCE=.\RotationRegnDlg.h
# End Source File
# Begin Source File

SOURCE=.\RotationTableChart.h
# End Source File
# Begin Source File

SOURCE=.\RotationTableReportSelect.h
# End Source File
# Begin Source File

SOURCE=.\rotationtablereportselectexcel.h
# End Source File
# Begin Source File

SOURCE=.\RotationTables.h
# End Source File
# Begin Source File

SOURCE=.\RotationTableViewer.h
# End Source File
# Begin Source File

SOURCE=.\RotationViaDlg.h
# End Source File
# Begin Source File

SOURCE=.\RotationVipDlg.h
# End Source File
# Begin Source File

SOURCE=.\RotGDlgCedaFlightData.H
# End Source File
# Begin Source File

SOURCE=.\RotGroundDlg.h
# End Source File
# Begin Source File

SOURCE=.\SeasonAskDlg.h
# End Source File
# Begin Source File

SOURCE=.\SeasonCedaFlightData.H
# End Source File
# Begin Source File

SOURCE=.\SeasonCollectADAskDlg.h
# End Source File
# Begin Source File

SOURCE=.\SeasonCollectAskDlg.h
# End Source File
# Begin Source File

SOURCE=.\SeasonCollectCedaFlightData.H
# End Source File
# Begin Source File

SOURCE=.\SeasonCollectDlg.h
# End Source File
# Begin Source File

SOURCE=.\SeasonDlg.h
# End Source File
# Begin Source File

SOURCE=.\SeasonDlgCedaFlightData.H
# End Source File
# Begin Source File

SOURCE=.\SeasonFlightPropertySheet.H
# End Source File
# Begin Source File

SOURCE=.\SeasonTableDlg.h
# End Source File
# Begin Source File

SOURCE=.\SeasonTableViewer.H
# End Source File
# Begin Source File

SOURCE=.\SetupDlg.h
# End Source File
# Begin Source File

SOURCE=.\SpecialConflictDlg.h
# End Source File
# Begin Source File

SOURCE=.\SpotAllocateDlg.h
# End Source File
# Begin Source File

SOURCE=.\SpotAllocation.h
# End Source File
# Begin Source File

SOURCE=.\StdAfx.h
# End Source File
# Begin Source File

SOURCE=.\StringConst.h
# End Source File
# Begin Source File

SOURCE=..\..\_Standard_Share\_Standard_Wrapper\tab.h
# End Source File
# Begin Source File

SOURCE=.\TABLE.H
# End Source File
# Begin Source File

SOURCE=.\TableLineData.h
# End Source File
# Begin Source File

SOURCE=.\TimeRange.h
# End Source File
# Begin Source File

SOURCE=.\UFISAmSink.h
# End Source File
# Begin Source File

SOURCE=..\..\_Standard_Share\_Standard_Wrapper\ufiscom.h
# End Source File
# Begin Source File

SOURCE=.\UndoCkiDlg.h
# End Source File
# Begin Source File

SOURCE=.\UndoDlg.h
# End Source File
# Begin Source File

SOURCE=.\UniEingabe.h
# End Source File
# Begin Source File

SOURCE=.\Utils.h
# End Source File
# Begin Source File

SOURCE=.\ViaTableCtrl.h
# End Source File
# Begin Source File

SOURCE=.\VIPDlg.h
# End Source File
# Begin Source File

SOURCE=.\WoResTableDlg.h
# End Source File
# Begin Source File

SOURCE=.\WoResTableViewer.H
# End Source File
# Begin Source File

SOURCE=.\WroChart.H
# End Source File
# Begin Source File

SOURCE=.\WroDiagram.h
# End Source File
# Begin Source File

SOURCE=.\WroDiaPropertySheet.H
# End Source File
# Begin Source File

SOURCE=.\WroDiaViewer.H
# End Source File
# Begin Source File

SOURCE=.\WroGantt.H
# End Source File
# End Group
# Begin Group "Resourcen"

# PROP Default_Filter "bmp;rc;ico"
# Begin Source File

SOURCE=.\res\about.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bitmap1.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bitmap2.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bitmap3.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00001.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00002.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00003.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00004.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00005.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00006.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00007.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00008.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00009.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00010.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00011.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00012.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00013.bmp
# End Source File
# Begin Source File

SOURCE=.\res\empty.bmp
# End Source File
# Begin Source File

SOURCE=.\res\faglogo.bmp
# End Source File
# Begin Source File

SOURCE=".\res\FPMS-MFC.ico"
# End Source File
# Begin Source File

SOURCE=.\res\fpms.ico
# End Source File
# Begin Source File

SOURCE=.\FPMS.rc
# End Source File
# Begin Source File

SOURCE=.\FPMS.rc2
# End Source File
# Begin Source File

SOURCE=.\res\Haj_BW.bmp
# End Source File
# Begin Source File

SOURCE=.\res\hajlogo.bmp
# End Source File
# Begin Source File

SOURCE=.\res\hajlogo1.bmp
# End Source File
# Begin Source File

SOURCE=.\res\ico00001.ico
# End Source File
# Begin Source File

SOURCE=.\res\ico00002.ico
# End Source File
# Begin Source File

SOURCE=.\res\icon1.ico
# End Source File
# Begin Source File

SOURCE=.\res\idr_main.ico
# End Source File
# Begin Source File

SOURCE=.\res\LightBlue.bmp
# End Source File
# Begin Source File

SOURCE=.\res\LightBlueBlocked.bmp
# End Source File
# Begin Source File

SOURCE=.\res\login.bmp
# End Source File
# Begin Source File

SOURCE=.\res\login1.bmp
# End Source File
# Begin Source File

SOURCE=.\res\login2.bmp
# End Source File
# Begin Source File

SOURCE=.\res\Logo_Arrival.bmp
# End Source File
# Begin Source File

SOURCE=.\res\Logo_ccs.bmp
# End Source File
# Begin Source File

SOURCE=.\res\Logo_Departure.bmp
# End Source File
# Begin Source File

SOURCE=.\res\nextd.bmp
# End Source File
# Begin Source File

SOURCE=.\res\nextu.bmp
# End Source File
# Begin Source File

SOURCE=.\res\nextx.bmp
# End Source File
# Begin Source File

SOURCE=.\res\notavail.bmp
# End Source File
# Begin Source File

SOURCE=.\res\notvalid.bmp
# End Source File
# Begin Source File

SOURCE=.\res\Orange.bmp
# End Source File
# Begin Source File

SOURCE=.\res\prevd.bmp
# End Source File
# Begin Source File

SOURCE=.\res\prevu.bmp
# End Source File
# Begin Source File

SOURCE=.\res\prevx.bmp
# End Source File
# Begin Source File

SOURCE=.\res\red.bmp
# End Source File
# Begin Source File

SOURCE=.\res\red1.bmp
# End Source File
# Begin Source File

SOURCE=.\res\redBlocked.bmp
# End Source File
# Begin Source File

SOURCE=.\res\TITLEBAR.bmp
# End Source File
# Begin Source File

SOURCE=.\res\ufis.ico
# End Source File
# Begin Source File

SOURCE=.\res\UFISBITMAP.bmp
# End Source File
# Begin Source File

SOURCE=.\res\ufisintro256.bmp
# End Source File
# Begin Source File

SOURCE=.\res\ufislogo.bmp
# End Source File
# Begin Source File

SOURCE=.\res\yes.bmp
# End Source File
# Begin Source File

SOURCE=.\res\yes1.bmp
# End Source File
# End Group
# Begin Source File

SOURCE=.\res\cursor2.cur
# End Source File
# Begin Source File

SOURCE=.\res\cursor3.cur
# End Source File
# Begin Source File

SOURCE=.\res\cursor4.cur
# End Source File
# Begin Source File

SOURCE=.\FPMS.reg
# End Source File
# Begin Source File

SOURCE=.\UFISAppMng.tlb
# End Source File
# End Target
# End Project
# Section FPMS : {17D7E209-2DEC-48BC-83E2-3E9F43B90D59}
# 	2:21:DefaultSinkHeaderFile:aatlogin.h
# 	2:16:DefaultSinkClass:CAatLogin
# End Section
# Section FPMS : {BB73C4E9-3E22-4D7E-8EE3-A1BFA9A639FE}
# 	2:5:Class:CAatLogin
# 	2:10:HeaderFile:aatlogin.h
# 	2:8:ImplFile:aatlogin.cpp
# End Section


