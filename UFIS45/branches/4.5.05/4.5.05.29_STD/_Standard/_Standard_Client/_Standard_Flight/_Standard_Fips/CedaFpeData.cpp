// CedaFpeData.cpp - Class for Fpeipment
//

#include <stdafx.h>
#include <BasicData.h>
#include <ccsglobl.h>
#include <RotationCedaFlightData.h>
#include <CedaCcaData.h>
#include <Konflikte.h>
#include <RotationDlgCedaFlightData.h>
#include <Utils.h>
#include <Fpms.h>
#include <ButtonListDlg.h>
#include <CedaFpeData.h>
#include <process.h>
#include <CedaBasicData.h>
#include <CedaAptLocalUtc.h>
#include <FlightPermitDlg.h>

void ProcessFpeCf(void * popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
bool bgFPermitFlag = false;
#define IsOverlapped(start1, end1, start2, end2)    ((start1) <= (end2) && (start2) <= (end1))

static int ByScore(const FPEDATA **pppFpe1, const FPEDATA **pppFpe2);
static int ByScore(const FPEDATA **pppFpe1, const FPEDATA **pppFpe2)
{
	return (int)((**pppFpe1).Score - (**pppFpe2).Score);
}


CedaFpeData::CedaFpeData()
{                  
    BEGIN_CEDARECINFO(FPEDATA, FpeDataRecInfo)
		FIELD_LONG(Urno,"URNO")
		FIELD_DATE(Vafr,"VAFR")
		FIELD_DATE(Vato,"VATO")
		FIELD_CHAR_TRIM(Pern,"PERN")
		FIELD_CHAR_TRIM(Adid,"ADID")
		FIELD_CHAR_TRIM(Alco,"ALCO")
		FIELD_CHAR_TRIM(Flno,"FLNO")
		FIELD_CHAR_TRIM(Sufx,"SUFX")
		FIELD_CHAR_TRIM(Regn,"REGN")
		FIELD_CHAR_TRIM(Doop,"DOOP")
		FIELD_CHAR_TRIM(AtdNo,"ATDN")
		FIELD_CHAR_TRIM(Csgn,"CSGN")
		FIELD_CHAR_TRIM(Act3,"ACT3")
		FIELD_CHAR_TRIM(Act5,"ACT5")
		FIELD_CHAR_TRIM(Mtow,"MTOW")
		
		END_CEDARECINFO
		
		// Copy the record structure
		for (int i = 0; i < sizeof(FpeDataRecInfo)/sizeof(FpeDataRecInfo[0]); i++)
		{
			CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
			memcpy(prpCedaRecInfo,&FpeDataRecInfo[i],sizeof(CEDARECINFO));
			omRecInfo.Add(prpCedaRecInfo);
		}
		
		ogDdx.Register((void *)this, BC_FPE_INSERT, CString("FPEDATA"), CString("FPE Insert"), ProcessFpeCf);
		ogDdx.Register((void *)this, BC_FPE_UPDATE, CString("FPEDATA"), CString("FPE Update"), ProcessFpeCf);
		ogDdx.Register((void *)this, BC_FPE_DELETE, CString("FPEDATA"), CString("FPE Delete"), ProcessFpeCf);
		
		char pclConfigPath[256];
		if (getenv("CEDA") == NULL)
			strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
		else
			strcpy(pclConfigPath, getenv("CEDA"));
		
		char pclFtyps[250];
		GetPrivateProfileString(ogAppName, "FLIGHTPERMITS", "DEFAULT", pcmFlightPermitsPath, sizeof pcmFlightPermitsPath, pclConfigPath);
		GetPrivateProfileString(ogAppName, "FLIGHTPERMITS_FLIGHTTYPES", "O,S", pclFtyps, sizeof pclFtyps, pclConfigPath);
		omFtyps = pclFtyps;
		bmFlightPermitsEnabled = (strcmp(pcmFlightPermitsPath, "DEFAULT")) ? true : false;
		
		// Initialize table names and field names
		strcpy(pcmTableName,"FPETAB");
		//pcmFieldList = "URNO,VAFR,VATO,PERN,ADID,ALCO,FLNO,SUFX,REGN,DOOP,ATDN";// This will cause crash when the string is changed.
		//Because pcmFieldList is char* and now it is pointing to const char array//*lgptest*
		
		strcpy( pcmFList, "URNO,VAFR,VATO,PERN,ADID,ALCO,FLNO,SUFX,REGN,DOOP,ATDN");
		pcmFieldList = pcmFList;
		
		
}


void ProcessFpeCf(void * popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	((CedaFpeData *)popInstance)->ProcessFpeBc(ipDDXType,vpDataPointer,ropInstanceName);
}

void CedaFpeData::ProcessFpeBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlFpeData = (struct BcStruct *) vpDataPointer;
	//	ogBasicData.LogBroadcast(prlFpeData);
	FPEDATA rlFpe, *prlFpe = NULL;
	long llUrno = GetUrnoFromSelection(prlFpeData->Selection);
	GetRecordFromItemList(&rlFpe,prlFpeData->Fields,prlFpeData->Data);
	if(llUrno == 0L) llUrno = rlFpe.Urno;
	
	switch(ipDDXType)
	{
	case BC_FPE_INSERT:
		{
			if((prlFpe = AddFpeInternal(rlFpe)) != NULL)
			{
				ogDdx.DataChanged((void *)this, FPE_INSERT, (void *)prlFpe);
			}
			break;
		}
	case BC_FPE_UPDATE:
		{
			if((prlFpe = GetFpeByUrno(llUrno)) != NULL)
			{
				CString olOldAlco = prlFpe->Alco;
				CString olOldRegn = prlFpe->Regn;
				CString olOldCsgn = prlFpe->Csgn;
				
				GetRecordFromItemList(prlFpe,prlFpeData->Fields,prlFpeData->Data);
				//PrepareDataAfterRead(prlFpe);
				if(olOldAlco != prlFpe->Alco)
				{
					DeleteFpeFromAlcoMap(olOldAlco, llUrno);
					AddFpeToAlcoMap(prlFpe);
				}
				
				if(olOldRegn != prlFpe->Regn)
				{
					DeleteFpeFromRegnMap(olOldRegn, llUrno);
					AddFpeToRegnMap(prlFpe);
				}
				
				if(bgShowADHOC || bgSeasonShowADHOC)
				{
					if(olOldCsgn != prlFpe->Csgn)
					{
						DeleteFpeFromCsgnMap(olOldCsgn, llUrno);
						AddFpeToCsgnMap(prlFpe);
					}
				}
				
				ogDdx.DataChanged((void *)this, FPE_UPDATE, (void *)prlFpe);
			}
			break;
		}
	case BC_FPE_DELETE:
		{
			if((prlFpe = GetFpeByUrno(llUrno)) != NULL)
			{
				long llFpeUrno = prlFpe->Urno;
				DeleteFpeInternal(llFpeUrno);
				ogDdx.DataChanged((void *)this, FPE_DELETE, (void *)llFpeUrno);
			}
			break;
		}
	}
}


//void CedaFpeData::AssignPer(CString afCsgn, CString afRegn, CString afStox, CString afAdid)
void CedaFpeData::AssignPer(CTime opTime, bool bpArrival, CString opRegn, CString opAlco, CString opFlno, CString opSufx,CString opCsgn)
{
	FPEDATA *prlBestFpe = NULL;
	CMapPtrToPtr *polSingleMap = NULL;

	CString olWhere = ""; //lgp
	CString olUrno = "";
	CString olPern = ""; 
	char pclSelection[256];

	if(bgShowADHOC || bgSeasonShowADHOC)
	{
		prlBestFpe = GetBestPermitForFlight(opTime,  bpArrival,  opRegn,  opAlco,  opFlno,  opSufx, opCsgn);
		if(prlBestFpe != NULL)
		{
			olUrno.Format("%ld", prlBestFpe->Urno);//lgptest
			olPern = prlBestFpe->Pern;
		}
	}
	else
	{
		prlBestFpe = ogFpeData.GetBestPermitForFlight(opTime,  bpArrival,  opRegn,  opAlco,  opFlno,  opSufx);
		if(prlBestFpe != NULL)
		{
			olUrno.Format("%ld", prlBestFpe->Urno);//lgptest
			olPern = prlBestFpe->Pern;
		}
	}

	char pclTable[64] = "FPE";
	char pclFieldList[1024] = "URNO,PERN";
	char pclData[3000] = "";
	if(strcmp(olPern, "Permit Reqd") == 0 && !olUrno.IsEmpty())
	{
		sprintf( pclSelection, "WHERE URNO = %s", olUrno);
		bool blRet = CedaAction("FPE", pclTable, pclFieldList, pclSelection, "", pclData, "BUF1");
	}

}

 
CedaFpeData::~CedaFpeData()
{
	ogDdx.UnRegister(this,NOTUSED);
	ClearAll();
}

void CedaFpeData::ClearAll()
{
	omUrnoMap.RemoveAll();
	CMapPtrToPtr *polSingleMap;
	CString olAlco;
	CString olRegn;
	for(POSITION rlPos = omAlcoMap.GetStartPosition(); rlPos != NULL; )
	{
		omAlcoMap.GetNextAssoc(rlPos, olAlco, (void *& ) polSingleMap);
		polSingleMap->RemoveAll();
		delete polSingleMap;
	}
	omAlcoMap.RemoveAll();

	for(rlPos = omRegnMap.GetStartPosition(); rlPos != NULL; )
	{
		omRegnMap.GetNextAssoc(rlPos, olRegn, (void *& ) polSingleMap);
		polSingleMap->RemoveAll();
		delete polSingleMap;
	}
	omRegnMap.RemoveAll();

	if(bgShowADHOC || bgSeasonShowADHOC)
	{
		for(rlPos = omCsgnMap.GetStartPosition(); rlPos != NULL; )
		{
			omCsgnMap.GetNextAssoc(rlPos, olRegn, (void *& ) polSingleMap);
			polSingleMap->RemoveAll();
			delete polSingleMap;
		}
		omCsgnMap.RemoveAll();
	}

	omData.DeleteAll();
}

bool CedaFpeData::ReadFpeData()
{
	
	
	bool ilRc = true;
	
	if(!bmFlightPermitsEnabled)
		return ilRc;
	
	ClearAll();
	
	//char pclCom[10] = "RT";
	char pclCom[3] = "RT";
    char pclWhere[512] = "";
	
	SetFieldList();
    //sprintf(pclWhere,"WHERE VAFR <= '%s' AND VATO >= '%s'", opTo.Format("%Y%m%d%H%M%S"), opFrom.Format("%Y%m%d%H%M%S"));
    if((ilRc = CedaAction2(pclCom, "")) != true)
		//if(CedaAction("RT","FPE",pcmFieldList,pclWhere,"","BUF1") == false)
	{
		//		ogBasicData.LogCedaError("CedaFpeData Error",omLastErrorMessage,pclCom,pcmFieldList,pcmTableName,pclWhere);
	}
	else
	{
		FPEDATA rlFpeData;
		for (int ilLc = 0; ilRc == true; ilLc++)
		{
			if ((ilRc = GetBufferRecord2(ilLc,&rlFpeData)) == true)
			{
				AddFpeInternal(rlFpeData);
			}
		}
		ilRc = true;
	}
	
	//	ogBasicData.Trace("CedaFpeData Read %d Records (Rec Size %d) %s",omData.GetSize(), sizeof(FPEDATA), pclWhere);
    return ilRc;
}

FPEDATA *CedaFpeData::AddFpeInternal(FPEDATA &rrpFpe)
{
	FPEDATA *prlFpe = new FPEDATA;
	*prlFpe = rrpFpe;
	omData.Add(prlFpe);
	omUrnoMap.SetAt((void *)prlFpe->Urno,prlFpe);
	
	AddFpeToAlcoMap(prlFpe);
	AddFpeToRegnMap(prlFpe);
	
	if(bgShowADHOC || bgSeasonShowADHOC)
	{
		AddFpeToCsgnMap(prlFpe);
	}
	return prlFpe;
}

void CedaFpeData::AddFpeToAlcoMap(FPEDATA *prpFpe)
{
	CMapPtrToPtr *polSingleMap;
	if(omAlcoMap.Lookup(prpFpe->Alco, (void *&) polSingleMap))
	{
		polSingleMap->SetAt((void *) prpFpe->Urno, prpFpe);
	}
	else
	{
		polSingleMap = new CMapPtrToPtr;
		polSingleMap->SetAt((void *) prpFpe->Urno, prpFpe);
		omAlcoMap.SetAt(prpFpe->Alco, polSingleMap);
	}
}

void CedaFpeData::AddFpeToRegnMap(FPEDATA *prpFpe)
{
	CMapPtrToPtr *polSingleMap;
	if(omRegnMap.Lookup(prpFpe->Regn, (void *&) polSingleMap))
	{
		polSingleMap->SetAt((void *) prpFpe->Urno, prpFpe);
	}
	else
	{
		polSingleMap = new CMapPtrToPtr;
		polSingleMap->SetAt((void *) prpFpe->Urno, prpFpe);
		omRegnMap.SetAt(prpFpe->Regn, polSingleMap);
	}
}

void CedaFpeData::AddFpeToCsgnMap(FPEDATA *prpFpe)
{
	CMapPtrToPtr *polSingleMap;
	if(omCsgnMap.Lookup(prpFpe->Csgn, (void *&) polSingleMap))
	{
		polSingleMap->SetAt((void *) prpFpe->Urno, prpFpe);
	}
	else
	{
		polSingleMap = new CMapPtrToPtr;
		polSingleMap->SetAt((void *) prpFpe->Urno, prpFpe);
		omCsgnMap.SetAt(prpFpe->Csgn, polSingleMap);
	}
}


void CedaFpeData::DeleteFpeInternal(long lpUrno)
{
	FPEDATA *prlFpe = NULL;
	int ilNumFpes = omData.GetSize();
	for(int ilFpe = (ilNumFpes-1); ilFpe >= 0; ilFpe--)
	{
		prlFpe = &omData[ilFpe];
		if(prlFpe->Urno == lpUrno)
		{
			DeleteFpeFromAlcoMap(prlFpe->Alco, lpUrno);
			DeleteFpeFromRegnMap(prlFpe->Regn, lpUrno);

			if(bgShowADHOC || bgSeasonShowADHOC)
			{
				DeleteFpeFromCsgnMap(prlFpe->Csgn, lpUrno);
			}
			omData.DeleteAt(ilFpe);
			break;
		}
	}
	omUrnoMap.RemoveKey((void *)lpUrno);
}

void CedaFpeData::DeleteFpeFromAlcoMap(const char *pcpAlco, long lpFpeUrno)
{
	CMapPtrToPtr *polSingleMap;
	if(omAlcoMap.Lookup(pcpAlco, (void *&) polSingleMap))
	{
		polSingleMap->RemoveKey((void *) lpFpeUrno);
	}
}


void CedaFpeData::DeleteFpeFromRegnMap(const char *pcpRegn, long lpFpeUrno)
{
	CMapPtrToPtr *polSingleMap;
	if(omRegnMap.Lookup(pcpRegn, (void *&) polSingleMap))
	{
		polSingleMap->RemoveKey((void *) lpFpeUrno);
	}
}

void CedaFpeData::DeleteFpeFromCsgnMap(const char *pcpCsgn, long lpFpeUrno)
{
	CMapPtrToPtr *polSingleMap;
	if(omCsgnMap.Lookup(pcpCsgn, (void *&) polSingleMap))
	{
		polSingleMap->RemoveKey((void *) lpFpeUrno);
	}
}

FPEDATA* CedaFpeData::GetFpeByUrno(long lpUrno)
{
	FPEDATA *prlFpe = NULL;
	omUrnoMap.Lookup((void *)lpUrno, (void *&) prlFpe);
	return prlFpe;
}

bool CedaFpeData::FlightHasPermit(CTime opTime, bool bpArrival, CString opRegn, CString opAlco, CString opFlno, CString opSufx,CString opCsgn)
{
	FPEDATA *prlBestFpe = NULL;
	CMapPtrToPtr *polSingleMap = NULL;
	//CString olTrace;

	// If it is Private Flight then check with Regestraion Number
	/*if(opAlco.IsEmpty() && !opRegn.IsEmpty())
	{
		FPEDATA *prlFpe = NULL;
		
		// Check for the Matching Record
		if(omRegnMap.Lookup(opRegn, (void *&) polSingleMap))
		{
			long llUrno;
			FPEDATA *prlFpe = NULL;
			for(POSITION rlPos = polSingleMap->GetStartPosition(); rlPos != NULL; )
			{
				polSingleMap->GetNextAssoc(rlPos, (void *&) llUrno, (void *&)prlFpe);

				bool blArrival = (!strcmp(prlFpe->Adid,"A")) ? true : false;
				
				// For Adhoc Flights, Registration is Valid for a Day
				if( (opTime > prlFpe->Vafr && opTime < prlFpe->Vato) && (blArrival == bpArrival))
					return true;
			}
		}
	}
	else if(omAlcoMap.Lookup(opAlco, (void *&) polSingleMap))
	{
		long llUrno;
		FPEDATA *prlFpe = NULL;
		int ilScore = 0, ilPrevScore = 0;
		for(POSITION rlPos = polSingleMap->GetStartPosition(); rlPos != NULL; )
		{
			polSingleMap->GetNextAssoc(rlPos, (void *&) llUrno, (void *&)prlFpe);
			if(TestMatch(prlFpe, opTime, bpArrival, opRegn, opFlno, opSufx) > 0)
				return true;
		}
	}*/

		
	if(bgShowADHOC || bgSeasonShowADHOC)
	{
		prlBestFpe = GetBestPermitForFlight(opTime,  bpArrival,  opRegn,  opAlco,  opFlno,  opSufx, opCsgn);
	}
	else
	{
		prlBestFpe = ogFpeData.GetBestPermitForFlight(opTime,  bpArrival,  opRegn,  opAlco,  opFlno,  opSufx);
	}

	//if(prlBestFpe !=NULL)
	if(prlBestFpe !=NULL && (strcmp(prlBestFpe->Pern, "Permit Reqd") != 0)) //*lgptest*
	{
		return true;
	}

	return false;
}

// opTime must be UTC
FPEDATA* CedaFpeData::GetBestPermitForFlight(CTime opTime, bool bpArrival, CString opRegn, CString opAlco, CString opFlno, CString opSufx, CString *popPermitInfo /* = NULL */)
{
	DisplayTime(opTime);
	FPEDATA *prlBestFpe = NULL;
	CMapPtrToPtr *polSingleMap = NULL;
	//CCSPtrArray <FPEDATA> olPermits;

	//Is it a Private Flight ?
	if(opAlco.IsEmpty() && !opRegn.IsEmpty())
	{
		FPEDATA *prlFpe = NULL;
		// Check for the Matching Record
		if(omRegnMap.Lookup(opRegn, (void *&) polSingleMap))
		{
			long llUrno;
			FPEDATA *prlFpe = NULL;
			for(POSITION rlPos = polSingleMap->GetStartPosition(); rlPos != NULL; )
			{
				polSingleMap->GetNextAssoc(rlPos, (void *&) llUrno, (void *&)prlFpe);

				bool blArrival = (!strcmp(prlFpe->Adid,"A")) ? true : false;
				
				// For Adhoc Flights, Registration is Valid for a Day
				if( (opTime > prlFpe->Vafr && opTime < prlFpe->Vato) && (blArrival == bpArrival))
					prlBestFpe = prlFpe;
			}
		}
	}
	else if(omAlcoMap.Lookup(opAlco, (void *&) polSingleMap))
	{
		long llUrno;
		FPEDATA *prlFpe = NULL;		
		FPEDATA prlFpeArr[10];

		FlightPermitDialog objFPDlg; // Flight Permit Object

		int ilPrevScore = 0,iSize = 0;
		for(POSITION rlPos = polSingleMap->GetStartPosition(); rlPos != NULL; )
		{
			polSingleMap->GetNextAssoc(rlPos, (void *&) llUrno, (void *&)prlFpe);
			prlFpe->Score = TestMatch(prlFpe, opTime, bpArrival, opRegn, opFlno, opSufx);
			if(prlFpe->Score > 0)
			{
				//olPermits.Add(prlFpe);
				if(prlFpe->Score >= ilPrevScore)	//Changed from > to >=
				{
					prlBestFpe = prlFpe;
					ilPrevScore = prlFpe->Score;
					objFPDlg.AddFlightPermit(prlFpe);
					iSize++;
				}
			}
		}

		if(iSize > 1 && bgFPermitFlag)
		{
			objFPDlg.DoModal(); // Display the List of Flight Permits

			// Get the Selected Flight Permit
			prlBestFpe = objFPDlg.GetSelectedFlightPermit();

			
			// If the user closes the Multi-Permit Window with out selecting a Flight Permit.
			if(prlBestFpe == NULL)
			{
				*popPermitInfo = "NO_FLIGHT_PERMIT_DISPLAY";
				return prlBestFpe;

			}
		}
	}

	bgFPermitFlag = false; // Reset the bgFPermitFlag flag

//	olPermits.Sort(ByScore);
//	popPermitInfo->Empty();
//	for(int i = 0; i < olPermits.GetSize(); i++)
//	{
//		FPEDATA *prlFpe = &olPermits[i];
//		CString olInfo;
//		olInfo.Format("PERN<%s> ADID<%s> ALCO<%s> FLNO<%s> SUFX<%s> REGN<%s> DOOP<%s>\n", prlFpe->Pern, prlFpe->Adid, prlFpe->Alco, prlFpe->Flno, prlFpe->Sufx, prlFpe->Regn, prlFpe->Doop);
//		*popPermitInfo += olInfo;
//	}
	if(popPermitInfo != NULL)
	{
		popPermitInfo->Empty();
		if(prlBestFpe != NULL)
			popPermitInfo->Format("Permit Number <%s> A/D <%s> Airline <%s> Flight Num <%s> Suf. <%s> Registration <%s> Day of Op. <%s>", prlBestFpe->Pern, prlBestFpe->Adid, prlBestFpe->Alco, prlBestFpe->Flno, prlBestFpe->Sufx, prlBestFpe->Regn, prlBestFpe->Doop);
		else
			*popPermitInfo = "No Flight Permit";
	}
	return prlBestFpe;
}

// new function opTime must be UTC
FPEDATA* CedaFpeData::GetBestPermitForFlight(CTime opTime, bool bpArrival, CString opRegn, CString opAlco, CString opFlno, CString opSufx,CString opCsgn, CString *popPermitInfo /* = NULL */)
{
	DisplayTime(opTime);
	FPEDATA *prlBestFpe = NULL;
	CMapPtrToPtr *polSingleMap = NULL;

	if(!opCsgn.IsEmpty() && prlBestFpe==NULL)
	{
		FPEDATA *prlFpe = NULL;

		if(omCsgnMap.Lookup(opCsgn, (void *&) polSingleMap))
		{
			long llUrno;
			FPEDATA *prlFpe = NULL;
			for(POSITION rlPos = polSingleMap->GetStartPosition(); rlPos != NULL; )
			{
				polSingleMap->GetNextAssoc(rlPos, (void *&) llUrno, (void *&)prlFpe);

				bool blArrival = (!strcmp(prlFpe->Adid,"A")) ? true : false;
				
				// For Adhoc Flights, exact match
				if(opRegn == "")
				{
					if ((opTime == prlFpe->Vafr && opTime == prlFpe->Vato) && (blArrival == bpArrival)
						&& strcmp(prlFpe->Pern,"Permit Reqd") != 0 && strcmp(prlFpe->Regn,"") == 0)
					{
						prlBestFpe = prlFpe;
						break;
					}
					else if( (opTime == prlFpe->Vafr && opTime == prlFpe->Vato) && (blArrival == bpArrival)
						&& strcmp(prlFpe->Pern,"Permit Reqd") == 0 && strcmp(prlFpe->Regn,"") == 0)
					{
						prlBestFpe = prlFpe;
					}
				}
				else
				{
					if( (opTime == prlFpe->Vafr && opTime == prlFpe->Vato) && (blArrival == bpArrival) 
						&& (strcmp(prlFpe->Regn,opRegn) == 0))
					{
						prlBestFpe = prlFpe;
						break;
					}
					else if( (opTime == prlFpe->Vafr && opTime == prlFpe->Vato) && (blArrival == bpArrival) 
						&& (strcmp(prlFpe->Regn,opRegn) != 0 && strcmp(prlFpe->Pern,"Permit Reqd") == 0))
						prlBestFpe = prlFpe;
				}
			}
		}		
	}

	if(!opCsgn.IsEmpty() && prlBestFpe==NULL)
	{
		FPEDATA *prlFpe = NULL;

		if(omCsgnMap.Lookup(opCsgn, (void *&) polSingleMap))
		{
			long llUrno;
			FPEDATA *prlFpe = NULL;
			for(POSITION rlPos = polSingleMap->GetStartPosition(); rlPos != NULL; )
			{
				polSingleMap->GetNextAssoc(rlPos, (void *&) llUrno, (void *&)prlFpe);

				bool blArrival = (!strcmp(prlFpe->Adid,"A")) ? true : false;
				
				// For Adhoc Flights, exact match
				if(opRegn == "")
				{
					if( (opTime > prlFpe->Vafr && opTime < prlFpe->Vato) && (blArrival == bpArrival)
						&& strcmp(prlFpe->Pern,"Permit Reqd") != 0 && strcmp(prlFpe->Regn,"") == 0)
					{
						prlBestFpe = prlFpe;
						break;
					}
					else if( (opTime > prlFpe->Vafr && opTime < prlFpe->Vato) && (blArrival == bpArrival)
						&& strcmp(prlFpe->Pern,"Permit Reqd") == 0 && strcmp(prlFpe->Regn,"") == 0)
					{
						prlBestFpe = prlFpe;
					}
				}
				else
				{
					if( (opTime > prlFpe->Vafr && opTime < prlFpe->Vato) && (blArrival == bpArrival) 
						&& (strcmp(prlFpe->Regn,opRegn) == 0))
					{
						prlBestFpe = prlFpe;	
						break;
					}
					else if ( (opTime > prlFpe->Vafr && opTime < prlFpe->Vato) && (blArrival == bpArrival) 
						&& (strcmp(prlFpe->Regn,opRegn) != 0 && strcmp(prlFpe->Pern,"Permit Reqd") == 0))
						prlBestFpe = prlFpe;
				}
			}
		}		
	}


	//Search with exact match Flightno and Csgn
	if(!opAlco.IsEmpty() && prlBestFpe==NULL)
	{
		FPEDATA *prlFpe = NULL;

		if(omAlcoMap.Lookup(opAlco, (void *&) polSingleMap))
		{
			long llUrno;
			FPEDATA *prlFpe = NULL;
			for(POSITION rlPos = polSingleMap->GetStartPosition(); rlPos != NULL; )
			{
				polSingleMap->GetNextAssoc(rlPos, (void *&) llUrno, (void *&)prlFpe);

				bool blArrival = (!strcmp(prlFpe->Adid,"A")) ? true : false;
				
				if( (opTime == prlFpe->Vafr && opTime == prlFpe->Vato) && (blArrival == bpArrival))
				{
					if(opRegn == "")
					{
						if( strcmp(prlFpe->Flno ,opFlno) == 0 && strcmp(prlFpe->Csgn ,opCsgn) == 0
							&& strcmp(prlFpe->Pern,"Permit Reqd") != 0 && strcmp(prlFpe->Regn,"") == 0)
						{
							prlBestFpe = prlFpe;
							break;
						}
						else if( strcmp(prlFpe->Flno ,opFlno) == 0 && strcmp(prlFpe->Csgn ,opCsgn) == 0
							&& strcmp(prlFpe->Pern,"Permit Reqd") == 0 && strcmp(prlFpe->Regn,"") == 0)
						{
							prlBestFpe = prlFpe;
						}
					}
					else
					{
						if( strcmp(prlFpe->Flno ,opFlno) == 0 && strcmp(prlFpe->Csgn ,opCsgn) == 0 
							&& (strcmp(prlFpe->Regn,opRegn) == 0))
						{
							prlBestFpe = prlFpe;
							break;
						}
						else if ( strcmp(prlFpe->Flno ,opFlno) == 0 && strcmp(prlFpe->Csgn ,opCsgn) == 0 
							&& (strcmp(prlFpe->Regn,opRegn) != 0 && strcmp(prlFpe->Pern,"Permit Reqd") == 0))
							prlBestFpe = prlFpe;
					}
				}
			}
		}		
	}
	

	//Search with range match Flightno and Csgn
	if(!opAlco.IsEmpty() && prlBestFpe==NULL)
	{
		FPEDATA *prlFpe = NULL;

		if(omAlcoMap.Lookup(opAlco, (void *&) polSingleMap))
		{
			long llUrno;
			FPEDATA *prlFpe = NULL;
			for(POSITION rlPos = polSingleMap->GetStartPosition(); rlPos != NULL; )
			{
				polSingleMap->GetNextAssoc(rlPos, (void *&) llUrno, (void *&)prlFpe);

				bool blArrival = (!strcmp(prlFpe->Adid,"A")) ? true : false;
				
				if( (opTime > prlFpe->Vafr && opTime < prlFpe->Vato) && (blArrival == bpArrival))
				{
					if(opRegn == "")
					{
						if( strcmp(prlFpe->Flno ,opFlno) == 0 && strcmp(prlFpe->Csgn ,opCsgn) == 0
							&& strcmp(prlFpe->Pern,"Permit Reqd") != 0 && strcmp(prlFpe->Regn,"") == 0)
						{
							prlBestFpe = prlFpe;
							break;
						}
						else if( strcmp(prlFpe->Flno ,opFlno) == 0 && strcmp(prlFpe->Csgn ,opCsgn) == 0
							&& strcmp(prlFpe->Pern,"Permit Reqd") == 0 && strcmp(prlFpe->Regn,"") == 0)
						{
							prlBestFpe = prlFpe;
						}
					}
					else
					{
						if( strcmp(prlFpe->Flno ,opFlno) == 0 && strcmp(prlFpe->Csgn ,opCsgn) == 0 
							&& (strcmp(prlFpe->Regn,opRegn) == 0))
						{
							prlBestFpe = prlFpe;
							break;
						}
						else if( strcmp(prlFpe->Flno ,opFlno) == 0 && strcmp(prlFpe->Csgn ,opCsgn) == 0 
							&& (strcmp(prlFpe->Regn,opRegn) != 0 && strcmp(prlFpe->Pern,"Permit Reqd") == 0))
							prlBestFpe = prlFpe;
					}
				}
			}
		}		
	}

	//Search with exact match flight no
	if(!opAlco.IsEmpty() && prlBestFpe==NULL)
	{
		FPEDATA *prlFpe = NULL;

		if(omAlcoMap.Lookup(opAlco, (void *&) polSingleMap))
		{
			long llUrno;
			FPEDATA *prlFpe = NULL;
			for(POSITION rlPos = polSingleMap->GetStartPosition(); rlPos != NULL; )
			{
				polSingleMap->GetNextAssoc(rlPos, (void *&) llUrno, (void *&)prlFpe);

				bool blArrival = (!strcmp(prlFpe->Adid,"A")) ? true : false;
				
				if( (opTime == prlFpe->Vafr && opTime == prlFpe->Vato) && (blArrival == bpArrival))
				{
					if(opRegn == "")
					{
						if( strcmp(prlFpe->Flno ,opFlno) == 0
							&& strcmp(prlFpe->Pern,"Permit Reqd") != 0 && strcmp(prlFpe->Regn,"") == 0)
						{
							prlBestFpe = prlFpe;
							break;
						}
						else if( strcmp(prlFpe->Flno ,opFlno) == 0
							&& strcmp(prlFpe->Pern,"Permit Reqd") == 0 && strcmp(prlFpe->Regn,"") == 0)
						{
							prlBestFpe = prlFpe;
						}
					}
					else
					{
						if( strcmp(prlFpe->Flno ,opFlno) == 0 
							&& (strcmp(prlFpe->Regn,opRegn) == 0))
						{
							prlBestFpe = prlFpe;
							break;
						}
						else if( strcmp(prlFpe->Flno ,opFlno) == 0 
							&& (strcmp(prlFpe->Regn,opRegn) != 0 && strcmp(prlFpe->Pern,"Permit Reqd") == 0))
							prlBestFpe = prlFpe;
					}
				}
			}
		}		
	}
	
	//Search with exact match flight no
	if(!opAlco.IsEmpty() && prlBestFpe==NULL)
	{
		FPEDATA *prlFpe = NULL;

		if(omAlcoMap.Lookup(opAlco, (void *&) polSingleMap))
		{
			long llUrno;
			FPEDATA *prlFpe = NULL;
			for(POSITION rlPos = polSingleMap->GetStartPosition(); rlPos != NULL; )
			{
				polSingleMap->GetNextAssoc(rlPos, (void *&) llUrno, (void *&)prlFpe);

				bool blArrival = (!strcmp(prlFpe->Adid,"A")) ? true : false;
				
				if( (opTime > prlFpe->Vafr && opTime < prlFpe->Vato) && (blArrival == bpArrival))
				{
					if(opRegn == "")
					{
						if( strcmp(prlFpe->Flno ,opFlno) == 0
							&& strcmp(prlFpe->Pern,"Permit Reqd") != 0 && strcmp(prlFpe->Regn,"") == 0)
						{
							prlBestFpe = prlFpe;
							break;
						}
						else if( strcmp(prlFpe->Flno ,opFlno) == 0
							&& strcmp(prlFpe->Pern,"Permit Reqd") == 0 && strcmp(prlFpe->Regn,"") == 0)
						{
							prlBestFpe = prlFpe;
						}
					}
					else
					{
						if( strcmp(prlFpe->Flno ,opFlno) == 0 
							&& (strcmp(prlFpe->Regn,opRegn) == 0))
						{
							prlBestFpe = prlFpe;
							break;
						}
						else if( strcmp(prlFpe->Flno ,opFlno) == 0 
							&& (strcmp(prlFpe->Regn,opRegn) != 0 && strcmp(prlFpe->Pern,"Permit Reqd") == 0))
							prlBestFpe = prlFpe;
					}
				}
			}
		}		
	}

		//Search with exact match Airline code
	if(!opAlco.IsEmpty() && prlBestFpe==NULL)
	{
		FPEDATA *prlFpe = NULL;

		if(omAlcoMap.Lookup(opAlco, (void *&) polSingleMap))
		{
			long llUrno;
			FPEDATA *prlFpe = NULL;
			for(POSITION rlPos = polSingleMap->GetStartPosition(); rlPos != NULL; )
			{
				polSingleMap->GetNextAssoc(rlPos, (void *&) llUrno, (void *&)prlFpe);

				bool blArrival = (!strcmp(prlFpe->Adid,"A")) ? true : false;
				
				if( (opTime > prlFpe->Vafr && opTime < prlFpe->Vato) && (blArrival == bpArrival))
				{
					if(opRegn == "")
					{
						if(strlen(prlFpe->Flno) ==0
							&& strcmp(prlFpe->Pern,"Permit Reqd") != 0 && strcmp(prlFpe->Regn,"") == 0)
						{
							prlBestFpe = prlFpe;
							break;
						}
						else if(strlen(prlFpe->Flno) ==0
							&& strcmp(prlFpe->Pern,"Permit Reqd") == 0 && strcmp(prlFpe->Regn,"") == 0)
						{
							prlBestFpe = prlFpe;
						}
					}
					else
					{
						if(strlen(prlFpe->Flno) ==0 
							&& (strcmp(prlFpe->Regn,opRegn) == 0 || strcmp(prlFpe->Regn,"") == 0))
						{
							prlBestFpe = prlFpe;
							break;
						}
						else if(strlen(prlFpe->Flno) ==0 
							&& (strcmp(prlFpe->Regn,opRegn) != 0 && strcmp(prlFpe->Pern,"Permit Reqd") == 0))
							prlBestFpe = prlFpe;
					}
				}
			}
		}		
	}

	

	bgFPermitFlag = false; // Reset the bgFPermitFlag flag


	if(popPermitInfo != NULL)
	{
		popPermitInfo->Empty();
		if(prlBestFpe != NULL)
			popPermitInfo->Format("Permit Number <%s> A/D <%s> Airline <%s> Flight Num <%s> Suf. <%s> Registration <%s> Day of Op. <%s>", prlBestFpe->Pern, prlBestFpe->Adid, prlBestFpe->Alco, prlBestFpe->Flno, prlBestFpe->Sufx, prlBestFpe->Regn, prlBestFpe->Doop);
		else
			*popPermitInfo = "No Flight Permit";
	}
	return prlBestFpe;
}

// returns zero for flight permit not found 
// or positive value for match, the better the match the higher the value returned
// popTrace contains or the reason why the flight was not matched else it is empty
// opTime must be UTC
int CedaFpeData::TestMatch(FPEDATA *prpFpe, CTime opTime, bool bpArrival, CString opRegn, CString opFlno, CString opSufx, CString *popTrace /* = NULL */)
{
//	DisplayTime(opTime);
	int ilScore = 1; // found
	
	if(opTime < prpFpe->Vafr || opTime > prpFpe->Vato)
	{
		if(popTrace != NULL)
			popTrace->Format("Flight time outside permit's validity:  Flight Time = %s  Permit valid from %s to %s", opTime.Format("%d.%m.%Y %H:%M"), prpFpe->Vafr.Format("%d.%m.%Y %H:%M"), prpFpe->Vato.Format("%d.%m.%Y %H:%M"));
		return 0;
	}

	bool blArrival = (!strcmp(prpFpe->Adid,"A")) ? true : false;
	if(blArrival != bpArrival)
	{
		if(popTrace != NULL)
			popTrace->Format("Arrival/Departure mismatch:  Flight Type = %s  Permit Type = %s", bpArrival ? "Arrival" : "Departure", blArrival ? "Arrival" : "Departure");
		return 0;
	}

	if(!CheckDoop(prpFpe->Doop, opTime))
	{
		if(popTrace != NULL)
			popTrace->Format("Flight and Permit day of operation do not match");
		return 0;
	}


	// the following non-mandatory fields are for more specific flights and therefore score higher than general permits if matched

	if(strlen(prpFpe->Regn) > 0)
	{
		if(strcmp(opRegn,prpFpe->Regn))
		{
			if(popTrace != NULL)
				popTrace->Format("Flight and Permit registrations do not match:  Flight Registration %s  Permit Registration %s", opRegn, prpFpe->Regn);
			return 0;
		}
		ilScore++;
	}

	// flight number and suffix form one single key
	if(strlen(prpFpe->Flno) > 0 || strlen(prpFpe->Sufx) > 0)
	{
		if(strcmp(opFlno,prpFpe->Flno) || strcmp(opSufx,prpFpe->Sufx))
		{
			if(popTrace != NULL)
				popTrace->Format("Flight and Permit flight numbers do not match:  Flight number/suffix %s/%s  Permit flight number/suffix %s/%s", opFlno, opSufx, prpFpe->Flno, prpFpe->Sufx);
			return 0;
		}
		ilScore++;
	}

	return ilScore;
}

bool CedaFpeData::CheckDoop(CString opFpeDoop, CTime opFlightTime)
{
	CTime olFlightTime = opFlightTime;

	// must convert from UTC to local to get the correct day
	CedaAptLocalUtc::AptUtcToLocal(olFlightTime, CString(pcgHome4));

	int ilFlightDoop = olFlightTime.GetDayOfWeek() - 1;
	CString olFlightDoop;
	olFlightDoop.Format("%d", (ilFlightDoop == 0) ? 7 : ilFlightDoop);
	return (opFpeDoop.Find(olFlightDoop) != -1) ? true : false;
}

// opTime must be UTC
bool CedaFpeData::StartFlightPermitsAppl(CTime opTime, bool bpArrival, CString opRegn, CString opAlco, CString opFlno, CString opSufx, CString opCsgn,CString opAct3,CString opAct5,CString opMode)
{
	DisplayTime(opTime);
	if(!bmFlightPermitsEnabled)
		return false;

	if(opTime == TIMENULL)
		return false;

	char pclFieldList[500];
	char pclData[500];
	char pclCommand[50];
	CString omArrFlightPermitInfo;

	bgFPermitFlag = true; // Set bgFPermitFlag
	FPEDATA *prlFpe = NULL;
	CTime olLocalTime = UtcToLocal(opTime);
	
	
	if(bgShowADHOC || bgSeasonShowADHOC)
	{
		prlFpe = ogFpeData.GetBestPermitForFlight(opTime, bpArrival, opRegn, opAlco, opFlno, opSufx,opCsgn, &omArrFlightPermitInfo);
	}
	else
	{
		prlFpe = ogFpeData.GetBestPermitForFlight(opTime, bpArrival, opRegn, opAlco, opFlno, opSufx, &omArrFlightPermitInfo);
	}

	if(opMode=="1")
	{

	}
	
	int ilDoop = olLocalTime.GetDayOfWeek() - 1;
	ilDoop = (ilDoop == 0) ? 7 : ilDoop;
	CString olMtow = "";
	if(!opRegn.IsEmpty())
	{
		CString olWhere;
		olWhere.Format("WHERE REGN = '%s'", opRegn);
		ogBCD.Read( "ACR", olWhere);
		ogBCD.GetField("ACR", "REGN", opRegn, "MTOW", olMtow);
		olMtow.TrimLeft('0');
	}
	
	if(bgShowADHOC || bgSeasonShowADHOC)
	{
		CString olUrno("");
		if(prlFpe != NULL)
		{
			char buffer[20];
			itoa(prlFpe->Urno , buffer, 10);
			olUrno=CString(buffer);
		}

		strcpy(pclCommand, "PERMIT");
		strcpy(pclFieldList, "ADID,ALC3,ACT3,ACT5,FLTN,FLNS,URNO,CSGN,REGN,MODE,ST,MTOW,DOOP");
		sprintf(pclData, "%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%d",bpArrival ? "A" : "D", opAlco, opAct3, opAct5, opFlno, opSufx, olUrno,opCsgn,opRegn,opMode,olLocalTime.Format("%Y%m%d%H%M00"),olMtow,ilDoop);
	}
	else
	{
		if(prlFpe != NULL)
		{
			strcpy(pclCommand, "UPDATE");
			strcpy(pclFieldList, "ALC3,FLTN,FLNS,URNO");
			sprintf(pclData, "%s,%s,%s,%ld", opAlco, opFlno, opSufx, prlFpe->Urno);
		}
		else
		{
			// Don't want to see any Flight Permit.
			if(omArrFlightPermitInfo == "NO_FLIGHT_PERMIT_DISPLAY")
			{
				omArrFlightPermitInfo.Empty();
				return true;;
			}
			strcpy(pclCommand, "INSERT");
			strcpy(pclFieldList, "ADID,ALC3,FLTN,FLNS,REGN,VAFR,VATO,DOOP,MTOW");
			sprintf(pclData, "%s,%s,%s,%s,%s,%s,%s,%d,%s", bpArrival ? "A" : "D", opAlco, opFlno, opSufx, opRegn, olLocalTime.Format("%Y%m%d000000"), olLocalTime.Format("%Y%m%d235959"), ilDoop, olMtow);
		}
	}

	char *args[7];
	args[0] = "child";
	args[1] = pcgUser;
	args[2] = pcgPasswd;
	args[3] = pclCommand;
	args[4] = pclFieldList;
	args[5] = pclData;
	args[6] = NULL;

	int ilRc = _spawnv(_P_NOWAIT,pcmFlightPermitsPath,args);

	CString olErr = "";
	switch(ilRc)
	{
		case 0:
			// no error
			break;
		case E2BIG:
			olErr = "Argument list exceeds 1024 bytes";
			break;
		case EINVAL:
			olErr = "mode argument is invalid";
			break;
		case ENOENT:
			olErr = "File or path is not found";
			break;
		case ENOEXEC:
			olErr = "Specified file is not executable or has invalid executable-file format";
			break;
		case ENOMEM:
			olErr = "Not enough memory is available to execute new process";
			break;
		default:
			olErr = "Unknown error returned when attempting to start the process";
			break;
	}

//	if(!olErr.IsEmpty())
//	{
//		CString olTxt;
//		olTxt.Format("Error starting the Flight Permits application (Ceda.ini FLIGHTPERMITS=\"%s\").\n\n%s", pcmFlightPermitsPath, olErr);
//		::MessageBox(NULL, olTxt, "Flight Permits", MB_OK | MB_ICONERROR);
//		return false;
//	}

	return true;
}


// opTime must be UTC
bool CedaFpeData::StartFlightPermitsApplS(CString olPern, CTime opTime, bool bpArrival, CString opRegn, CString opAlco, CString opFlno, CString opSufx, CString opCsgn,CString opAct3,CString opAct5,CString opMode,CString opPerUrno)
{
	DisplayTime(opTime);
	if(!bmFlightPermitsEnabled)
		return false;

	if(opTime == TIMENULL)
		return false;

	char pclFieldList[500];
	char pclData[500];
	char pclCommand[50];
	CString omArrFlightPermitInfo;

	bgFPermitFlag = true; // Set bgFPermitFlag
	FPEDATA *prlFpe = NULL;
	CTime olLocalTime = UtcToLocal(opTime);
	
	
	if(bgShowADHOC || bgSeasonShowADHOC)
	{
		prlFpe = ogFpeData.GetBestPermitForFlight(opTime, bpArrival, opRegn, opAlco, opFlno, opSufx,opCsgn, &omArrFlightPermitInfo);
	}
	else
	{
		prlFpe = ogFpeData.GetBestPermitForFlight(opTime, bpArrival, opRegn, opAlco, opFlno, opSufx, &omArrFlightPermitInfo);
	}

	if(opMode=="1")
	{

	}
	
	int ilDoop = olLocalTime.GetDayOfWeek() - 1;
	ilDoop = (ilDoop == 0) ? 7 : ilDoop;
	CString olMtow = "";
	if(!opRegn.IsEmpty())
	{
		CString olWhere;
		olWhere.Format("WHERE REGN = '%s'", opRegn);
		ogBCD.Read( "ACR", olWhere);
		ogBCD.GetField("ACR", "REGN", opRegn, "MTOW", olMtow);
		olMtow.TrimLeft('0');
	}
	
	if(bgShowADHOC || bgSeasonShowADHOC)
	{
		CString olUrno("");
		if(opPerUrno.IsEmpty())
		{
			if(prlFpe != NULL)
			{
				char buffer[20];
				itoa(prlFpe->Urno , buffer, 10);
				olUrno=CString(buffer);
			}
		}
		else
			olUrno = opPerUrno;

		strcpy(pclCommand, "PERMIT");
		strcpy(pclFieldList, "ADID,ALC3,ACT3,ACT5,FLTN,FLNS,URNO,CSGN,REGN,MODE,ST,MTOW,DOOP,PERN");
		sprintf(pclData, "%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%d,\"%s\"",bpArrival ? "A" : "D", opAlco, opAct3, opAct5, opFlno, opSufx, olUrno,opCsgn,opRegn,opMode,olLocalTime.Format("%Y%m%d%H%M00"),olMtow,ilDoop,olPern);
	}
	else
	{
		if(prlFpe != NULL)
		{
			strcpy(pclCommand, "UPDATE");
			strcpy(pclFieldList, "ALC3,FLTN,FLNS,URNO");
			sprintf(pclData, "%s,%s,%s,%ld", opAlco, opFlno, opSufx, prlFpe->Urno);
		}
		else
		{
			// Don't want to see any Flight Permit.
			if(omArrFlightPermitInfo == "NO_FLIGHT_PERMIT_DISPLAY")
			{
				omArrFlightPermitInfo.Empty();
				return true;;
			}
			strcpy(pclCommand, "INSERT");
			strcpy(pclFieldList, "ADID,ALC3,FLTN,FLNS,REGN,VAFR,VATO,DOOP,MTOW,PERN");
			sprintf(pclData, "%s,%s,%s,%s,%s,%s,%s,%d,%s,\"%s\"", bpArrival ? "A" : "D", opAlco, opFlno, opSufx, opRegn, olLocalTime.Format("%Y%m%d000000"), olLocalTime.Format("%Y%m%d235959"), ilDoop, olMtow, olPern);
		}
	}

	char *args[7];
	args[0] = "child";
	args[1] = pcgUser;
	args[2] = pcgPasswd;
	args[3] = pclCommand;
	args[4] = pclFieldList;
	args[5] = pclData;
	args[6] = NULL;

	int ilRc = _spawnv(_P_NOWAIT,pcmFlightPermitsPath,args);

	CString olErr = "";
	switch(ilRc)
	{
		case 0:
			// no error
			break;
		case E2BIG:
			olErr = "Argument list exceeds 1024 bytes";
			break;
		case EINVAL:
			olErr = "mode argument is invalid";
			break;
		case ENOENT:
			olErr = "File or path is not found";
			break;
		case ENOEXEC:
			olErr = "Specified file is not executable or has invalid executable-file format";
			break;
		case ENOMEM:
			olErr = "Not enough memory is available to execute new process";
			break;
		default:
			olErr = "Unknown error returned when attempting to start the process";
			break;
	}

//	if(!olErr.IsEmpty())
//	{
//		CString olTxt;
//		olTxt.Format("Error starting the Flight Permits application (Ceda.ini FLIGHTPERMITS=\"%s\").\n\n%s", pcmFlightPermitsPath, olErr);
//		::MessageBox(NULL, olTxt, "Flight Permits", MB_OK | MB_ICONERROR);
//		return false;
//	}

	return true;
}


bool CedaFpeData::FtypEnabled(const char *pcpFtyp)
{
	return (omFtyps.Find(pcpFtyp) != -1) ? true : false;
}

CTime CedaFpeData::UtcToLocal(CTime opTime)
{
	CedaAptLocalUtc::AptUtcToLocal(opTime, CString(pcgHome4));
	return opTime;
}

void CedaFpeData::DisplayTime(CTime opTime)
{
// for debugging only!
//	::MessageBox(NULL, opTime.Format("%d.%m.%Y %H:%M"), "Time", MB_OK | MB_ICONERROR);
}


bool CedaFpeData::InsertDB(FPEDATA *prlFpe)
{
	
	prlFpe->IsChanged = DATA_NEW;
	Save(prlFpe); //Update Database
	
    return true;
}

bool CedaFpeData::Save(FPEDATA *prlFpe)
{
	bool ilRc = true;
	CString olListOfData;
	char pclSelection[124];
	char pclData[524];

	SetFieldList();

	if (prlFpe->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}
	switch(prlFpe->IsChanged)
	{
	case DATA_NEW:
		MakeCedaData(&omRecInfo,olListOfData,prlFpe);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("IRT","","",pclData);
		prlFpe->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = %ld", prlFpe->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prlFpe);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("URT",pclSelection,"",pclData);
		prlFpe->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_DELETED:
		sprintf(pclSelection, "WHERE URNO = %ld", prlFpe->Urno);
		ilRc = CedaAction("DRT",pclSelection);
		break;
	}
    return ilRc;
}


void CedaFpeData::SetFieldList(void)
{
	if(bgShowADHOC || bgSeasonShowADHOC)
	{
		if( CString(pcmFieldList).Find("CSGN,ACT3,ACT5,MTOW") < 0 )
		{
			strcat(pcmFieldList,",CSGN,ACT3,ACT5,MTOW");
		}
	}
}

bool CedaFpeData::UpdateDB(FPEDATA *prlFpe)
{
	
	prlFpe->IsChanged = DATA_CHANGED;
	Save(prlFpe); 
	
    return true;
}