// GatPosConflictSetupDlg.cpp : implementation file
//

#include "stdafx.h"
#include "fpms.h"
#include "resrc1.h"
#include "GatPosConflictSetupDlg.h"
#include "GridFenster.h"
#include "CedaCfgData.h"
#include <BasicData.h>
#include <PrivList.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// GatPosConflictSetupDlg dialog


GatPosConflictSetupDlg::GatPosConflictSetupDlg(CWnd* pParent /*=NULL*/)
: CDialog(GatPosConflictSetupDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(GatPosConflictSetupDlg)
	// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	
	static bool blFirst = true;
	if (blFirst)
	{
		blFirst = false;
		GXInit();
	}
	
	pomConflictSetup = new CGridFenster(this);
	
	
	char pclTmpText[512], pclConfigPath[512];
	lmCharSet = DEFAULT_CHARSET;
	if (getenv("CEDA") == NULL)
		strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
	else
		strcpy(pclConfigPath, getenv("CEDA"));
	
	GetPrivateProfileString(ogAppName, "CHARSET", "DEFAULT", pclTmpText, sizeof pclTmpText, pclConfigPath);
	if(!strcmp(pclTmpText, "ARABIC"))
		lmCharSet = ARABIC_CHARSET;
	
	imColDuration = 2;
	//	 imColPriority = 3;
	imColGeneralDesktop = 3;
	imColAircraftPositions = 4;
	imColGates = 5;
	imColCheckinCounters = 6;
	imColBaggageBelts = 7;
	imColLounges = 8;
	imColFlightDia = 9;
	imColPriorityColor = 11;
	
	if (bgConflictPriority)
	{
		imColDuration = 2;
		imColPriority = 3;
		imColGeneralDesktop = 4;
		imColAircraftPositions = 5;
		imColGates = 6;
		imColCheckinCounters = 7;
		imColBaggageBelts = 8;
		imColLounges = 9;
		imColFlightDia = 10;
		imColPriorityColor = 12;
	}
	
}

int GatPosConflictSetupDlg::GetWarningThreshold()
{
		CString olTmp;
		int ilMinutes = 0;
		m_CE_WarningThreshold.GetWindowText(olTmp);
		if (olTmp.GetLength() > 0)
		{
			ilMinutes = abs(atoi(olTmp));
			char buffer[128];
			itoa(ilMinutes, buffer, 10);
			m_CE_WarningThreshold.SetInitText( CString(buffer) );
		}
	return ilMinutes;
}

void GatPosConflictSetupDlg::SetWarningThreshold(int ipMinutes)
{
			char buffer[128];
			itoa(ipMinutes, buffer, 10);
			m_CE_WarningThreshold.SetInitText( CString(buffer) );

}


GatPosConflictSetupDlg::~GatPosConflictSetupDlg()
{
	delete pomConflictSetup;
	omConflicts.DeleteAll();
	
}
void GatPosConflictSetupDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(GatPosConflictSetupDlg)
	DDX_Control(pDX, IDC_WARNINGTHRESHOLD, m_CE_WarningThreshold);
	// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(GatPosConflictSetupDlg, CDialog)
//{{AFX_MSG_MAP(GatPosConflictSetupDlg)
ON_WM_SIZE()
ON_WM_PAINT()
ON_MESSAGE(GRID_MESSAGE_CELLCLICK,OnGridMessageCellClick)
ON_MESSAGE(GRID_MESSAGE_DOUBLECLICK,OnGridMessageCellDblClick)
ON_MESSAGE(GRID_MESSAGE_ENDEDITING,OnGridMessageEndEditing)
ON_BN_CLICKED(IDC_LOAD_DEFAULT, OnLoadDefault)
ON_BN_CLICKED(IDC_SAVE_DEFAULT, OnSaveDefault)
ON_BN_CLICKED(IDC_RESET, OnReset)
//}}AFX_MSG_MAP
END_MESSAGE_MAP()


#define CanDealWithData( SEC )\
( SEC == '1' )


/////////////////////////////////////////////////////////////////////////////
// GatPosConflictSetupDlg message handlers

BOOL GatPosConflictSetupDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	if (bgConflictPriority)
	{
		m_CE_WarningThreshold.ShowWindow(SW_SHOW);  
			CWnd* polWnd = GetDlgItem(IDC_CONFLICTS);
		if (polWnd != NULL)
		{
			polWnd->ShowWindow(SW_SHOW);
		}
		
	} 
	else
	{
		m_CE_WarningThreshold.ShowWindow(SW_HIDE);
		CWnd* polWnd = GetDlgItem(IDC_CONFLICTS);
		if (polWnd != NULL)
		{
			polWnd->ShowWindow(SW_HIDE);
		}

		polWnd = GetDlgItem(IDC_WARNTHRESHOLD_LABEL);
		if (polWnd != NULL)
		{
			polWnd->ShowWindow(SW_HIDE);
		}

	}
	// TODO: Add extra initialization here
	pomConflictSetup->SubclassDlgItem(IDC_CONFLICT_SETUP, this);
	
	// initialize selection grid
	pomConflictSetup->Initialize();
	pomConflictSetup->SetAllowMultiSelect(false);
	pomConflictSetup->SetSortingEnabled(false);
	
	// disable immediate update
	pomConflictSetup->LockUpdate(TRUE);
	pomConflictSetup->GetParam()->EnableUndo(FALSE);
	// column width is static
	pomConflictSetup->GetParam()->EnableTrackColWidth(FALSE);
	// row height is static
	pomConflictSetup->GetParam()->EnableTrackRowHeight(FALSE);
	// column headers aren't numbered
	pomConflictSetup->GetParam()->SetNumberedColHeaders(FALSE);
	
	pomConflictSetup->SetColCount( 9 );
	
	// set the size of the row number
	pomConflictSetup->SetColWidth(0,0,20);
	
	// set the size of the conflict name
	pomConflictSetup->SetColWidth(1,1,300);
	
	// set the size of the conflict duration
	pomConflictSetup->SetColWidth(2,2,20);
	
	int ilNextCol = 3;
	if (bgConflictPriority)
	{
		int cols = pomConflictSetup->GetColCount();
		pomConflictSetup->SetColCount( cols +1 );
		pomConflictSetup->SetColWidth(ilNextCol,ilNextCol,20);
		ilNextCol++;
	}
	//YYY
	
	
	//for (int i = ilNextCol; i < ilNextCol + 6;i++)
	for (int i = ilNextCol; i < ilNextCol + 7;i++)
	{
		pomConflictSetup->SetColWidth(i,i,16);
	}
	
	m_cSecState_ConflictColor = ogPrivList.GetStat( "SETUP_CHK_TODAY_ONLY" );		// 050506 MVy: conflict priority color, security state
	
	if( __CanSeeConflictPriorityColor()		// 050323 MVy: PRF6981: conflict priority, make configurable, addendum
		&& CanDealWithData( m_cSecState_ConflictColor ) )		// 050506 MVy: conflict priority color, security state
	{
		// append two columns and specify their length
		int cols = pomConflictSetup->GetColCount();
		pomConflictSetup->SetColCount( cols +2 );		// 050303 MVy: two columns added, now 11
		pomConflictSetup->SetColWidth( cols +1, cols +1,  8 );		// 050303 MVy: a small gap
		pomConflictSetup->SetColWidth( cols +2, cols +2, 16 );		// 050303 MVy: Conflict Priority
	};
	
	pomConflictSetup->SetRowHeight(0, 0, 14);
	
	ogCfgData.GetConflictData(omConflicts);
	//	OnLoadDefault();
	
	pomConflictSetup->SetRowCount(omConflicts.GetSize());
	pomConflictSetup->SetScrollBarMode(SB_BOTH,gxnAutomatic|gxnEnhanced,TRUE);
	
	
	// enable immediate update
	pomConflictSetup->LockUpdate(FALSE);
	
	UpdateGrid();
	
	CWnd *polWnd = GetDlgItem(IDC_LOAD_DEFAULT);
	if (polWnd)
	{
		if(ogPrivList.GetStat("SETUP_CONFLICT_SAVE") == '0')//disabled
			polWnd->EnableWindow(FALSE);
		if(ogPrivList.GetStat("SETUP_CONFLICT_SAVE") == '-')//hidden
			polWnd->ShowWindow(SW_HIDE);
	}
	
	polWnd = GetDlgItem(IDC_SAVE_DEFAULT);
	if (polWnd)
	{
		if(ogPrivList.GetStat("SETUP_CONFLICT_LOAD") == '0')//disabled
			polWnd->EnableWindow(FALSE);
		if(ogPrivList.GetStat("SETUP_CONFLICT_LOAD") == '-')//hidden
			polWnd->ShowWindow(SW_HIDE);
	}
	
	polWnd = GetDlgItem(IDC_RESET);
	if (polWnd)
	{
		if(ogPrivList.GetStat("SETUP_CONFLICT_RESET") == '0')//disabled
			polWnd->EnableWindow(FALSE);
		if(ogPrivList.GetStat("SETUP_CONFLICT_RESET") == '-')//hidden
			polWnd->ShowWindow(SW_HIDE);
	}
	
	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}


void GatPosConflictSetupDlg::OnSize(UINT nType, int cx, int cy)  
{
	CDialog::OnSize(nType, cx, cy);
	
	// TODO: Add your message handler code here
	if (pomConflictSetup && ::IsWindow(pomConflictSetup->m_hWnd))
	{
		CRect olRect;
		pomConflictSetup->GetWindowRect(&olRect);
		this->ScreenToClient(&olRect); 
		cx = (int)(0.97 * cx);	// with aspect ratio between child window width / dialog width
		//		cy = 0.72 * cy;	// with aspect ration between child window height / dialog height
		cy = (int)(0.81 * cy);	// with aspect ration between child window height / dialog height
		pomConflictSetup->MoveWindow(olRect.TopLeft().x,olRect.TopLeft().y,cx,cy);
		pomConflictSetup->Redraw();
	}
	
}

void GatPosConflictSetupDlg::OnPaint() 
{
	CPaintDC dc(this); // device context for painting
	
	// TODO: Add your message handler code here
	CRect olRect;
	pomConflictSetup->GetWindowRect(&olRect);
	this->ScreenToClient(&olRect);
	
	
	::SetGraphicsMode(dc.m_hDC,GM_ADVANCED);
	
	CFont *polFont = this->GetFont();
	if (polFont)
	{
		CFont olFont;
		LOGFONT olLogFont;
		polFont->GetLogFont(&olLogFont);
		olLogFont.lfCharSet= lmCharSet;
		olLogFont.lfOrientation = 0;
		olLogFont.lfEscapement  = 0;
		olLogFont.lfWeight		= 700;
		olFont.CreateFontIndirect(&olLogFont);
		polFont = dc.SelectObject(&olFont);
		int x = olRect.TopLeft().x+20;
		dc.TextOut(olRect.TopLeft().x,olRect.TopLeft().y - 15,GetString(IDS_STRING2330));
		x+=300;
		dc.TextOut(x,olRect.TopLeft().y - 15,GetString(IDS_STRING2331)); 
		x+=20;
		if (bgConflictPriority)
		{
			dc.TextOut(x,olRect.TopLeft().y - 15,GetString(IDS_STRING2393)); // Prio
			x+=20;
		}
		
		dc.SelectObject(polFont);
		
		olFont.DeleteObject();
		olLogFont.lfCharSet= lmCharSet;
		olLogFont.lfOrientation = 750;
		olLogFont.lfEscapement  = 750;
		olLogFont.lfWeight		= 700;
		olFont.CreateFontIndirect(&olLogFont);
		polFont = dc.SelectObject(&olFont);
		
		dc.TextOut(x+0*16,olRect.TopLeft().y - 5,GetString(IDS_STRING2332));
		dc.TextOut(x+1*16,olRect.TopLeft().y - 5,GetString(IDS_STRING2333));
		dc.TextOut(x+2*16,olRect.TopLeft().y - 5,GetString(IDS_STRING2334));
		dc.TextOut(x+3*16,olRect.TopLeft().y - 5,GetString(IDS_STRING2335));
		dc.TextOut(x+4*16,olRect.TopLeft().y - 5,GetString(IDS_STRING2336));
		dc.TextOut(x+5*16,olRect.TopLeft().y - 5,GetString(IDS_STRING2337));
		//YYY
		dc.TextOut(x+6*16,olRect.TopLeft().y - 5,"Flight Diagram");
		if( __CanSeeConflictPriorityColor()		// 050323 MVy: PRF6981: conflict priority, make configurable, addendum
			&& CanDealWithData( m_cSecState_ConflictColor ) )		// 050506 MVy: conflict priority color, security state
		{
			dc.TextOut(x+8+7*16,olRect.TopLeft().y - 5,"Attention Color");		// 050303 MVy: draw the title with just a small gap		// 050506 MVy: title text changed
		};
		dc.SelectObject(polFont);
		olFont.DeleteObject();
	}
	
	// Do not call CDialog::OnPaint() for painting messages
}

LONG GatPosConflictSetupDlg::OnGridMessageEndEditing(WPARAM wParam,LPARAM lParam)
{
	CELLPOS *prlPos = (CELLPOS *)lParam;
	
	if (prlPos)
	{
		if (prlPos->Row > 0 && prlPos->Col == 2)
		{
			CONFLICTDATA* polConflict;
			polConflict = &omConflicts[prlPos->Row - 1];
			
			polConflict->imDuration = atoi(pomConflictSetup->GetValueRowCol(prlPos->Row,prlPos->Col));
		}
		if (bgConflictPriority)
		{
			if (prlPos->Row > 0 && prlPos->Col == 3)
			{
				CONFLICTDATA* polConflict;
				polConflict = &omConflicts[prlPos->Row - 1];
				
				polConflict->imPriority = atoi(pomConflictSetup->GetValueRowCol(prlPos->Row,prlPos->Col));
			}
		}
		
	}
	
	return 1;
}

// 050304 MVy: do same on double click
LONG GatPosConflictSetupDlg::OnGridMessageCellDblClick(WPARAM wParam,LPARAM lParam)
{
	return OnGridMessageCellClick( wParam, lParam );
};	// OnGridMessageCellDblClick

LONG GatPosConflictSetupDlg::OnGridMessageCellClick(WPARAM wParam,LPARAM lParam)
{
	CELLPOS *prlPos = (CELLPOS *)lParam;
	
	if (prlPos)
	{
		if (prlPos->Row > 0 && prlPos->Col >= imColGeneralDesktop)
		{
			CONFLICTDATA* polConflict;
			polConflict = &omConflicts[prlPos->Row - 1];
			
			CGXStyle olStyle;
			olStyle.SetEnabled(TRUE);
			olStyle.SetReadOnly(TRUE);
			
			pomConflictSetup->GetParam()->SetLockReadOnly(FALSE);
			//			pomConflictSetup->LockUpdate(FALSE);
			if (prlPos->Col == imColGeneralDesktop)
			{
				if (polConflict->bmGeneralDesktop)
				{
					polConflict->bmGeneralDesktop = false;
					pomConflictSetup->SetStyleRange(CGXRange(prlPos->Row,prlPos->Col,prlPos->Row,prlPos->Col),olStyle.SetControl(GX_IDS_CTRL_STATIC).SetValue("#BMP(229)"));
				}
				else
				{
					polConflict->bmGeneralDesktop = true;
					pomConflictSetup->SetStyleRange(CGXRange(prlPos->Row,prlPos->Col,prlPos->Row,prlPos->Col),olStyle.SetControl(GX_IDS_CTRL_STATIC).SetValue("#BMP(228)"));
				}
			} else if (prlPos->Col == imColAircraftPositions)
			{
				if (polConflict->bmAircraftPositions)
				{
					polConflict->bmAircraftPositions = false;
					pomConflictSetup->SetStyleRange(CGXRange(prlPos->Row,prlPos->Col,prlPos->Row,prlPos->Col),olStyle.SetControl(GX_IDS_CTRL_STATIC).SetValue("#BMP(229)"));
				}
				else
				{
					polConflict->bmAircraftPositions = true;
					pomConflictSetup->SetStyleRange(CGXRange(prlPos->Row,prlPos->Col,prlPos->Row,prlPos->Col),olStyle.SetControl(GX_IDS_CTRL_STATIC).SetValue("#BMP(228)"));
				}
			} else if (prlPos->Col == imColGates)
			{
				if (polConflict->bmGates)
				{
					polConflict->bmGates = false;
					pomConflictSetup->SetStyleRange(CGXRange(prlPos->Row,prlPos->Col,prlPos->Row,prlPos->Col),olStyle.SetControl(GX_IDS_CTRL_STATIC).SetValue("#BMP(229)"));
				}
				else
				{
					polConflict->bmGates = true;
					pomConflictSetup->SetStyleRange(CGXRange(prlPos->Row,prlPos->Col,prlPos->Row,prlPos->Col),olStyle.SetControl(GX_IDS_CTRL_STATIC).SetValue("#BMP(228)"));
				}
			} else if (prlPos->Col == imColCheckinCounters)
			{
				if (polConflict->bmCheckinCounters)
				{
					polConflict->bmCheckinCounters = false;
					pomConflictSetup->SetStyleRange(CGXRange(prlPos->Row,prlPos->Col,prlPos->Row,prlPos->Col),olStyle.SetControl(GX_IDS_CTRL_STATIC).SetValue("#BMP(229)"));
				}
				else
				{
					polConflict->bmCheckinCounters = true;
					pomConflictSetup->SetStyleRange(CGXRange(prlPos->Row,prlPos->Col,prlPos->Row,prlPos->Col),olStyle.SetControl(GX_IDS_CTRL_STATIC).SetValue("#BMP(228)"));
				}
			} else if (prlPos->Col == imColBaggageBelts)
			{
				if (polConflict->bmBaggageBelts)
				{
					polConflict->bmBaggageBelts = false;
					pomConflictSetup->SetStyleRange(CGXRange(prlPos->Row,prlPos->Col,prlPos->Row,prlPos->Col),olStyle.SetControl(GX_IDS_CTRL_STATIC).SetValue("#BMP(229)"));
				}
				else
				{
					polConflict->bmBaggageBelts = true;
					pomConflictSetup->SetStyleRange(CGXRange(prlPos->Row,prlPos->Col,prlPos->Row,prlPos->Col),olStyle.SetControl(GX_IDS_CTRL_STATIC).SetValue("#BMP(228)"));
				}
			} else if (prlPos->Col == imColLounges)
			{
				if (polConflict->bmLounges)
				{
					polConflict->bmLounges = false;
					pomConflictSetup->SetStyleRange(CGXRange(prlPos->Row,prlPos->Col,prlPos->Row,prlPos->Col),olStyle.SetControl(GX_IDS_CTRL_STATIC).SetValue("#BMP(229)"));
				}
				else
				{
					polConflict->bmLounges = true;
					pomConflictSetup->SetStyleRange(CGXRange(prlPos->Row,prlPos->Col,prlPos->Row,prlPos->Col),olStyle.SetControl(GX_IDS_CTRL_STATIC).SetValue("#BMP(228)"));
				}
			} else if (prlPos->Col == imColFlightDia)
			{
				if (polConflict->bmFlightDia)
				{
					polConflict->bmFlightDia = false;
					pomConflictSetup->SetStyleRange(CGXRange(prlPos->Row,prlPos->Col,prlPos->Row,prlPos->Col),olStyle.SetControl(GX_IDS_CTRL_STATIC).SetValue("#BMP(229)"));
				}
				else
				{
					polConflict->bmFlightDia = true;
					pomConflictSetup->SetStyleRange(CGXRange(prlPos->Row,prlPos->Col,prlPos->Row,prlPos->Col),olStyle.SetControl(GX_IDS_CTRL_STATIC).SetValue("#BMP(228)"));
				}
			} else if (prlPos->Col == imColPriorityColor)
			{
				if( __CanSeeConflictPriorityColor()		// 050323 MVy: PRF6981: conflict priority, make configurable, addendum
					&& CanDealWithData( m_cSecState_ConflictColor ) )		// 050506 MVy: conflict priority color, security state
					
				{
					CColorDialog dlgColor ;
					dlgColor.m_cc.Flags |= CC_RGBINIT ;		// startup with the nearest selected color
					dlgColor.m_cc.rgbResult = polConflict->clrPriority ;
					// 050303 MVy: problem: this dialog is modal, but the framework subverts this mechanism
					//		if more than one dialog is open and a prior one is closed, the program will fail continuing,
					//		for example there are null pointers
					static unsigned char ucOnlyOnce = 0 ;
					//ASSERT( !ucOnlyOnce );
					if( !ucOnlyOnce )		// this is only workaround
					{
						ucOnlyOnce++ ;
						bool bResult = dlgColor.DoModal() == IDOK ;
						ucOnlyOnce-- ;
						// problem: the grid was not updated on ok, when another dialog was tried to be opened meanwhile
						
						ASSERT( pomConflictSetup );
						if( bResult )
						{
							COLORREF color = dlgColor.GetColor();
							polConflict->clrPriority = color ;
							pomConflictSetup->SetStyleRange( 
								CGXRange( prlPos->Row, prlPos->Col, prlPos->Row, prlPos->Col ), 
								olStyle.SetInterior( color ).SetValue( "" )
								);
						};
					};
				};	// handle conflict column
			}
			}
			pomConflictSetup->GetParam()->SetLockReadOnly(TRUE);
			pomConflictSetup->RedrawRowCol(prlPos->Row,prlPos->Col,prlPos->Row,prlPos->Col);
			//			pomConflictSetup->Redraw();
		}
		else
			ToggleRow(prlPos);

	return 1;
}

void GatPosConflictSetupDlg::ToggleRow(CELLPOS *prlPos)  
{
	bool blMark = true;
	CString olBMP = "#BMP(228)";
	
	if (prlPos->Row > 0 && prlPos->Col == 1)
	{
		blMark = false;
		olBMP = "#BMP(229)";
	}
	
	if ((prlPos->Row > 0 && prlPos->Col == 0) || (prlPos->Row > 0 && prlPos->Col == 1))
	{
		CONFLICTDATA* polConflict;
		polConflict = &omConflicts[prlPos->Row - 1];
		
		CGXStyle olStyle;
		olStyle.SetEnabled(TRUE);
		olStyle.SetReadOnly(TRUE);
		
		pomConflictSetup->GetParam()->SetLockReadOnly(FALSE);
		
		polConflict->bmGeneralDesktop = blMark;
		pomConflictSetup->SetStyleRange(CGXRange(prlPos->Row,3,prlPos->Row,3),olStyle.SetControl(GX_IDS_CTRL_STATIC).SetValue(olBMP));
		
		polConflict->bmAircraftPositions = blMark;
		pomConflictSetup->SetStyleRange(CGXRange(prlPos->Row,4,prlPos->Row,4),olStyle.SetControl(GX_IDS_CTRL_STATIC).SetValue(olBMP));
		
		polConflict->bmGates = blMark;
		pomConflictSetup->SetStyleRange(CGXRange(prlPos->Row,5,prlPos->Row,5),olStyle.SetControl(GX_IDS_CTRL_STATIC).SetValue(olBMP));
		
		polConflict->bmCheckinCounters = blMark;
		pomConflictSetup->SetStyleRange(CGXRange(prlPos->Row,6,prlPos->Row,6),olStyle.SetControl(GX_IDS_CTRL_STATIC).SetValue(olBMP));
		
		polConflict->bmBaggageBelts = blMark;
		pomConflictSetup->SetStyleRange(CGXRange(prlPos->Row,7,prlPos->Row,7),olStyle.SetControl(GX_IDS_CTRL_STATIC).SetValue(olBMP));
		
		polConflict->bmLounges = blMark;
		pomConflictSetup->SetStyleRange(CGXRange(prlPos->Row,8,prlPos->Row,8),olStyle.SetControl(GX_IDS_CTRL_STATIC).SetValue(olBMP));
		
		polConflict->bmLounges = blMark;
		pomConflictSetup->SetStyleRange(CGXRange(prlPos->Row,9,prlPos->Row,9),olStyle.SetControl(GX_IDS_CTRL_STATIC).SetValue(olBMP));
		
		pomConflictSetup->GetParam()->SetLockReadOnly(TRUE);
		pomConflictSetup->RedrawRowCol(prlPos->Row,3,prlPos->Row,3);
		pomConflictSetup->RedrawRowCol(prlPos->Row,4,prlPos->Row,4);
		pomConflictSetup->RedrawRowCol(prlPos->Row,5,prlPos->Row,5);
		pomConflictSetup->RedrawRowCol(prlPos->Row,6,prlPos->Row,6);
		pomConflictSetup->RedrawRowCol(prlPos->Row,7,prlPos->Row,7);
		pomConflictSetup->RedrawRowCol(prlPos->Row,8,prlPos->Row,8);
		pomConflictSetup->RedrawRowCol(prlPos->Row,9,prlPos->Row,9);
	}
}

void GatPosConflictSetupDlg::OnReset() 
{
	ogCfgData.ResetDefaultConflicts();
	OnLoadDefault();
	OnSaveDefault();
}


void GatPosConflictSetupDlg::OnLoadDefault() 
{
	// TODO: Add your control notification handler code here
    CString olRecord;
	
	int ilRecCount = 0;
	
	CStringArray olLines;
	// read default values, if there are none, use hard coded values
	if (ogCfgData.ReadConflicts(ogCfgData.pomDefaultName,olLines) == false)
	{
		ogCfgData.GetDefaultConflictSetup(olLines);
	}
	
	//	if ( olLines.GetSize() != omConflicts.GetSize())
	{
		omConflicts.DeleteAll();
	}
	
	
	for(int i = 0; i < olLines.GetSize(); i++)
	{
		CString olRecord = olLines.GetAt(i);
		CONFLICTDATA *prlConflictData = ogCfgData.NewConflictData( olRecord );		// 050506 MVy: code moved to function
		
																					/*
																					
																					  // check, if an update is necessary
																					  if (i < omConflicts.GetSize())
																					  {
																					  CONFLICTDATA *prlOldData = &omConflicts[i];
																					  if (*prlOldData != *prlConflictData)
																					  {
																					  delete prlOldData;
																					  omConflicts.SetAt(i,prlConflictData);
																					  ilRecCount++;
																					  }
																					  else
																					  delete prlConflictData;
																					  }
		else*/
		{
			omConflicts.Add(prlConflictData);
			ilRecCount++;
		}
	}
	
	UpdateGrid();		
	
}

void GatPosConflictSetupDlg::OnSaveDefault() 
{
	// TODO: Add your control notification handler code here
	CWaitCursor olWait;
	for (int ilLc = 0; ilLc < omConflicts.GetSize(); ilLc++)
	{
		CFGDATA rlCfg = ogCfgData.MakeConflictData( ilLc, omConflicts[ ilLc ], ogCfgData.pomDefaultName );		// 050506 MVy: see similar code CedaCfgData::SetConflictData
		ogCfgData.SaveConflictData(ogCfgData.pomDefaultName,&rlCfg);
	}	
}

void GatPosConflictSetupDlg::UpdateGrid()
{
	pomConflictSetup->GetParam()->SetLockReadOnly(FALSE);
	
	pomConflictSetup->SetRowCount(omConflicts.GetSize());
	
	
	// enable immediate update
	pomConflictSetup->LockUpdate(FALSE);
	int iConflicts = omConflicts.GetSize();		// get amount of conflicts populating the grid
	
	// 050303 MVy: init the gap column before the conflict priority color field
	CGXStyle olStyle;
	olStyle.SetEnabled(FALSE);
	olStyle.SetReadOnly(TRUE);
	int cols = pomConflictSetup->GetColCount();
	pomConflictSetup->SetStyleRange( 
		CGXRange( 0, cols -1, iConflicts, cols -1 ),
		olStyle.SetInterior( COLORREF(GRAY) ).SetValue( "" )
		);
	
	for (int i = 0; i < iConflicts ; i++)
	{
		CGXStyle olStyle;
		olStyle.SetEnabled(FALSE);
		olStyle.SetReadOnly(TRUE);
		CONFLICTDATA olConflict;
		
		COLORREF olColor = COLORREF(LLGREEN);
		// 050228 MVy: hardcoded coloring for new line 30 containing Aircraft Registration restriction
		// 050301 MVy: due to problems with data read from database this comes at the end, at line 75
		if ( (i > 21 && i < 38) || i > 51 && i < 61 || i < 5 || i > 68 && i < 73 )
			olColor = COLORREF(CREAM);
		
		if (i == 22)
			olColor = COLORREF(ORANGE);
		
		olConflict = omConflicts[i];
		{
			int ilCol = 1;
			pomConflictSetup->SetStyleRange(CGXRange(i+1,ilCol,i+1,ilCol),olStyle.SetInterior(olColor).SetValue(olConflict.omName));
			ilCol++;
			/*			if (i == 60)
			//				pomConflictSetup->SetStyleRange(CGXRange(i+1,1,i+1,1),olStyle.SetValue(GetString(IDS_STRING2572)));
			pomConflictSetup->SetStyleRange(CGXRange(i+1,1,i+1,1),olStyle.SetValue(olConflict.omName));
			else
			pomConflictSetup->SetStyleRange(CGXRange(i+1,1,i+1,1),olStyle.SetValue(olConflict.omName));
			*/
			if (olConflict.imDuration == 0xFFFF)	// min duration disabled ?
			{
				pomConflictSetup->SetStyleRange(CGXRange(i+1,ilCol,i+1,ilCol),olStyle.SetInterior(COLORREF(GRAY)).SetValue(""));
				ilCol++;
			}
			else
			{
				olStyle.SetEnabled(TRUE);
				olStyle.SetReadOnly(FALSE);
				pomConflictSetup->SetStyleRange(CGXRange(i+1,ilCol,i+1,ilCol),olStyle.SetValue((LONG)olConflict.imDuration));
				ilCol++;
			}
			if (bgConflictPriority) 
			{
				olStyle.SetEnabled(TRUE);
				olStyle.SetReadOnly(FALSE);
				pomConflictSetup->SetStyleRange(CGXRange(i+1,ilCol,i+1,ilCol),olStyle.SetInterior(COLORREF(WHITE)).SetValue((LONG)olConflict.imPriority));
//				pomConflictSetup->SetStyleRange(CGXRange(i+1,ilCol,i+1,ilCol),olStyle.SetValue((LONG)olConflict.imPriority));
				ilCol++;
			}
			
			if (olConflict.bmGeneralDesktop)
			{
				olStyle.SetEnabled(TRUE);
				olStyle.SetReadOnly(TRUE);
				pomConflictSetup->SetStyleRange(CGXRange(i+1,ilCol,i+1,ilCol),olStyle.SetControl(GX_IDS_CTRL_STATIC).SetValue("#BMP(228)"));
				ilCol++;
			}
			else
			{
				olStyle.SetEnabled(TRUE);
				olStyle.SetReadOnly(TRUE);
				pomConflictSetup->SetStyleRange(CGXRange(i+1,ilCol,i+1,ilCol),olStyle.SetControl(GX_IDS_CTRL_STATIC).SetValue("#BMP(229)"));
				ilCol++;
			}
			
			if (olConflict.bmAircraftPositions)
			{
				olStyle.SetEnabled(TRUE);
				olStyle.SetReadOnly(TRUE);
				pomConflictSetup->SetStyleRange(CGXRange(i+1,ilCol,i+1,ilCol),olStyle.SetControl(GX_IDS_CTRL_STATIC).SetValue("#BMP(228)"));
				ilCol++;
			}
			else
			{
				olStyle.SetEnabled(TRUE);
				olStyle.SetReadOnly(TRUE);
				pomConflictSetup->SetStyleRange(CGXRange(i+1,ilCol,i+1,ilCol),olStyle.SetControl(GX_IDS_CTRL_STATIC).SetValue("#BMP(229)"));
				ilCol++;
			}
			
			if (olConflict.bmGates)
			{
				olStyle.SetEnabled(TRUE);
				olStyle.SetReadOnly(TRUE);
				pomConflictSetup->SetStyleRange(CGXRange(i+1,ilCol,i+1,ilCol),olStyle.SetControl(GX_IDS_CTRL_STATIC).SetValue("#BMP(228)"));
				ilCol++;
			}
			else
			{
				olStyle.SetEnabled(TRUE);
				olStyle.SetReadOnly(TRUE);
				pomConflictSetup->SetStyleRange(CGXRange(i+1,ilCol,i+1,ilCol),olStyle.SetControl(GX_IDS_CTRL_STATIC).SetValue("#BMP(229)"));
				ilCol++;
			}
			
			if (olConflict.bmCheckinCounters)
			{
				olStyle.SetEnabled(TRUE);
				olStyle.SetReadOnly(TRUE);
				pomConflictSetup->SetStyleRange(CGXRange(i+1,ilCol,i+1,ilCol),olStyle.SetControl(GX_IDS_CTRL_STATIC).SetValue("#BMP(228)"));
				ilCol++;
			}
			else
			{
				olStyle.SetEnabled(TRUE);
				olStyle.SetReadOnly(TRUE);
				pomConflictSetup->SetStyleRange(CGXRange(i+1,ilCol,i+1,ilCol),olStyle.SetControl(GX_IDS_CTRL_STATIC).SetValue("#BMP(229)"));
				ilCol++;
			}
			
			if (olConflict.bmBaggageBelts)
			{
				olStyle.SetEnabled(TRUE);
				olStyle.SetReadOnly(TRUE);
				pomConflictSetup->SetStyleRange(CGXRange(i+1,ilCol,i+1,ilCol),olStyle.SetControl(GX_IDS_CTRL_STATIC).SetValue("#BMP(228)"));
				ilCol++;
			}
			else
			{
				olStyle.SetEnabled(TRUE);
				olStyle.SetReadOnly(TRUE);
				pomConflictSetup->SetStyleRange(CGXRange(i+1,ilCol,i+1,ilCol),olStyle.SetControl(GX_IDS_CTRL_STATIC).SetValue("#BMP(229)"));
				ilCol++;
			}
			
			if (olConflict.bmLounges)
			{
				olStyle.SetEnabled(TRUE);
				olStyle.SetReadOnly(TRUE);
				pomConflictSetup->SetStyleRange(CGXRange(i+1,ilCol,i+1,ilCol),olStyle.SetControl(GX_IDS_CTRL_STATIC).SetValue("#BMP(228)"));
				ilCol++;
			}
			else
			{
				olStyle.SetEnabled(TRUE);
				olStyle.SetReadOnly(TRUE);
				pomConflictSetup->SetStyleRange(CGXRange(i+1,ilCol,i+1,ilCol),olStyle.SetControl(GX_IDS_CTRL_STATIC).SetValue("#BMP(229)"));
				ilCol++;
			}
			//YYY
			if (olConflict.bmFlightDia)
			{
				olStyle.SetEnabled(TRUE);
				olStyle.SetReadOnly(TRUE);
				pomConflictSetup->SetStyleRange(CGXRange(i+1,ilCol,i+1,ilCol),olStyle.SetControl(GX_IDS_CTRL_STATIC).SetValue("#BMP(228)"));
				ilCol++;
			}
			else
			{
				olStyle.SetEnabled(TRUE);
				olStyle.SetReadOnly(TRUE);
				pomConflictSetup->SetStyleRange(CGXRange(i+1,ilCol,i+1,ilCol),olStyle.SetControl(GX_IDS_CTRL_STATIC).SetValue("#BMP(229)"));
				ilCol++;
			}
			
			if( __CanSeeConflictPriorityColor()		// 050323 MVy: PRF6981: conflict priority, make configurable, addendum
				&& CanDealWithData( m_cSecState_ConflictColor ) )		// 050506 MVy: conflict priority color, security state
			{
				// 050303 MVy: PRF6981: conflict priority, setting the cell color
				olStyle.SetEnabled(TRUE);
				olStyle.SetReadOnly(TRUE);
				pomConflictSetup->SetStyleRange( 
					CGXRange( i+1, ilCol, i+1, ilCol ),
					olStyle.SetInterior( olConflict.clrPriority ).SetValue( "" )
					);
				ilCol++;
			};
		}
	}
	
	pomConflictSetup->GetParam()->SetLockReadOnly(true);
	pomConflictSetup->SetScrollBarMode(SB_BOTH,gxnAutomatic|gxnEnhanced,TRUE);
	pomConflictSetup->Redraw();
}


void GatPosConflictSetupDlg::SaveConflictSetup()
{
	CWaitCursor olWait;
	
    for (int ilLc = 0; ilLc < omConflicts.GetSize(); ilLc++)
	{
		ogCfgData.SetConflictData(ilLc,omConflicts[ilLc],true);
	}	
	
}
