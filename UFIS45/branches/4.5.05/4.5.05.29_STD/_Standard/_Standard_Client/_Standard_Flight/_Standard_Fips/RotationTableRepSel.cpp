// RotationTableRepSel.cpp : implementation file
//

#include <stdafx.h>
#include <fpms.h>
#include <RotationTableRepSel.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// RotationTableRepSel dialog


RotationTableRepSel::RotationTableRepSel(CWnd* pParent /*=NULL*/)
	: CDialog(RotationTableRepSel::IDD, pParent)
{
	//{{AFX_DATA_INIT(RotationTableRepSel)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void RotationTableRepSel::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(RotationTableRepSel)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(RotationTableRepSel, CDialog)
	//{{AFX_MSG_MAP(RotationTableRepSel)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// RotationTableRepSel message handlers
