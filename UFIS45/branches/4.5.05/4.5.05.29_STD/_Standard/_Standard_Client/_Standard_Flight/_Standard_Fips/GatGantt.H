// stgantt.h : header file
//

#ifndef _GATGANTT_H_
#define _GATGANTT_H_

// Special time value definition
#ifndef TIMENULL
#define TIMENULL    CTime((time_t)-1)
#endif

#include <DiaCedaFlightData.h>
#include <CCS3DStatic.h>
#include "PopupImgMenu.h" //AM 20080306



/////////////////////////////////////////////////////////////////////////////
// GatGantt window

class GatGantt: public CListBox
{
// Operations
public:
    GatGantt::GatGantt(GatDiagramViewer *popViewer = NULL, int ipGroupNo = 0,
        int ipVerticalScaleWidth = 0, int ipVerticalScaleIndent = 15,
        CFont *popVerticalScaleFont = NULL, CFont *popGanttChartFont = NULL,
        int ipGutterHeight = 2, int ipOverlapHeight = 4,
        COLORREF lpVerticalScaleTextColor = ::GetSysColor(COLOR_BTNTEXT),
        COLORREF lpVerticalScaleBackgroundColor = ::GetSysColor(COLOR_BTNFACE),
        COLORREF lpHighlightVerticalScaleTextColor = ::GetSysColor(COLOR_BTNHIGHLIGHT),
        COLORREF lpHighlightVerticalScaleBackgroundColor = ::GetSysColor(COLOR_BTNSHADOW),
		COLORREF lpVerticalScaleBackgroundColor2 = ::GetSysColor(COLOR_BTNFACE),
        COLORREF lpGanttChartTextColor = ::GetSysColor(COLOR_BTNTEXT),
        COLORREF lpGanttChartBackgroundColor = ::GetSysColor(COLOR_BTNFACE),
        COLORREF lpHighlightGanttChartTextColor = ::GetSysColor(COLOR_BTNHIGHLIGHT),
        COLORREF lpHighlightGanttChartBackgroundColor = ::GetSysColor(COLOR_BTNSHADOW));

	~GatGantt();

	BOOL Create(DWORD dwStyle, const RECT &rect, CWnd *pParentWnd, UINT nID = 0);
	// Grouping
	void SetViewer(GatDiagramViewer *popViewer,int ipGroupNo );

    void SetVerticalScaleColors(COLORREF lpTextColor, COLORREF lpBackgroundColor,
            COLORREF lpHighlightTextColor, COLORREF lpHighlightBackgroundColor, COLORREF lpBackgroundColor2);
    void SetGanttChartColors(COLORREF lpTextColor, COLORREF lpBackgroundColor,
            COLORREF lpHighlightTextColor, COLORREF lpHighlightBackgroundColor);
    void SetFonts(int index1, int index2);
	void SetTopScaleText(CCS3DStatic *popTopScaleText)
	{
		pomTopScaleText = popTopScaleText;

	};
    void SetMarkTime(CTime opMarkTimeStart, CTime opMarkTimeEnd);
    void SetStatusBar(CStatusBar *popStatusBar);
	void SetTimeScale(CCSTimeScale *popTimeScale);
    void SetVerticalScaleWidth(int ipWidth);
	void SetCurrentTime(CTime opCurrentTime);
    void SetDisplayWindow(CTime opDisplayStart, CTime opDisplayEnd, BOOL bpFixedScaling = TRUE);

    int GetGanttChartHeight();

	void RepaintVerticalScale(int ipLineno = -1, BOOL bpErase = TRUE);
    void RepaintGanttChart(int ipLineno = -1,
            CTime opStartTime = TIMENULL, CTime opEndTime = TIMENULL, BOOL bpErase = TRUE);
    void RepaintItemHeight(int ipLineno);

 	void ProcessMarkBar(DIAFLIGHTDATA *prpFlight);
	int SetMaxGanttChartHeight();


	bool bmRepaint;
	int imHeight;


private:

	// Grouping
	int imGroupNo;
	void SetVerticalScaleIndent(int ipIndent);
    //void SetFonts(CFont *popVerticalScaleFont, CFont *popGanttChartFont);
    void SetGutters(int ipGutterHeight, int ipOverlapHeight);
    void SetBorderPrecision(int ipBorderPrecision);
    void SetDisplayStart(CTime opDisplayStart);
 
    int GetLineHeight(int ipMaxOverlapLevel);
    int GetItemFromPoint(CPoint point);
    int GetBarnoFromPoint(int ipLineno, CPoint point, int ipPreLeft = 0, int ipPreRight = 1);
        // Precision is measured in pixel.
        // The purpose of the precision is for detecting more pixel when user want to resize.
        // The acceptable period of time is between [point.x - ipPreLeft, point.x + ipPreRight]
    int GetBkBarnoFromPoint(int ipLineno, CPoint point, int ipPreLeft = 0, int ipPreRight = 1);
        // Precision is measured in pixel.
        // The purpose of the precision is for detecting more pixel when user want to resize.
        // The acceptable period of time is between [point.x - ipPreLeft, point.x + ipPreRight]

	void AttachDiagram(GatDiagram *popWnd)
	{
		pomDiagram = popWnd;
	}


    virtual void DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct);
    virtual void MeasureItem(LPMEASUREITEMSTRUCT lpMeasureItemStruct);
	virtual BOOL DestroyWindow();

    void DrawVerticalScale(CDC *pDC, int itemID, const CRect &rcItem);
	void DrawBackgroundBars(CDC *pDC, int itemID, const CRect &rcItem, const CRect &rcClip);
    void DrawGanttChart(CDC *pDC, int itemID, const CRect &rcItem, const CRect &rcClip);
    void DrawTimeLines(CDC *pDC, int top, int bottom, BOOL bpWithMarkLines = FALSE);
	void DrawFocusRect(CDC *pDC, const CRect &rect);

    void UpdateGanttChart(CPoint point, BOOL bpIsControlKeyDown);
    void UpdateBarStatus(int ipLineno, int ipBarno);
    BOOL BeginMovingOrResizing(CPoint point);
    BOOL OnMovingOrResizing(CPoint point, BOOL bpIsLButtonDown);

    UINT HitTest(int ipLineno, int ipBarno, CPoint point);
    // HitTest will use the precision defined in "imBorderPrecision"
//-DTT Jul.25-----------------------------------------------------------
	BOOL IsDropOnDutyBar(CPoint point);
//----------------------------------------------------------------------
	void EnableRepaint(void);

	void DragBegin(int ipLineno, int ipBarno);
	BOOL CheckPostFlight(const GATDIA_BARDATA *prlBar);
	bool CheckIfGateIsOpen(const GATDIA_BARDATA *prpGatBar);


	GatDiagram *pomDiagram;
    GatDiagramViewer *pomViewer;

    int imVerticalScaleWidth;
	int imVerticalScaleIndent;	// number of pixels used to indent text in VerticalScale
    CFont *pomVerticalScaleFont;
    CFont *pomGanttChartFont;
	 CPoint omLastClickedPosition;
    int imBarHeight;            // calculated when SetGanttChartFont()
    int imGutterHeight;         // space between the GanttLine and the topmost-level bar
    int imLeadingHeight;        // space between the bar and the border
    int imOverlapHeight;        // space between the overlapped bar
    COLORREF lmVerticalScaleTextColor;
    COLORREF lmVerticalScaleBackgroundColor;
    COLORREF lmVerticalScaleBackgroundColor2;
    COLORREF lmHighlightVerticalScaleTextColor;
    COLORREF lmHighlightVerticalScaleBackgroundColor;
    COLORREF lmGanttChartTextColor;
    COLORREF lmGanttChartBackgroundColor;
    COLORREF lmHighlightGanttChartTextColor;
    COLORREF lmHighlightGanttChartBackgroundColor;
	GATDIA_BARDATA rmActiveBar, *prmPreviousBar;          // Bar which is currently moved or sized
	BOOL bmActiveBarSet;
	CBrush	*pomPreviousBrush;
	CRect    omPreviousRect;
	GATDIA_BARDATA rmContextBar;  // Save bar for Context Menu handling
	BOOL bmContextBarSet;
	BOOL bmActiveLineSet;  // There is a selected line ?
	long lmActiveLine;	// Urno of last selected line

    CTime omDisplayStart;
    CTime omDisplayEnd;
    int imWindowWidth;          // in pixels, for pixel/time ratio (updated by WM_SIZE)
    BOOL bmIsFixedScaling;
        // There are two mode for updating the display window when WM_SIZE was sent,
        // variabled-scaling and fixed-scaling. The mode of this operation will be
        // given in the method SetDisplayWindow().

    CTime omCurrentTime;                    // the red vertical time line
    CTime omMarkTimeStart, omMarkTimeEnd;   // two yellow vertical time lines

    CStatusBar *pomStatusBar;   // the status bar where the notification message goes
	CCSTimeScale *pomTimeScale;	// the time scale for top scale indicators display

    BOOL bmIsMouseInWindow;
    BOOL bmIsControlKeyDown;
	 BOOL bmMarkLines;
    int imHighlightLine;        // the current line that painted with highlight color
    int imCurrentBar;           // the current bar that the user places mouse over

    UINT umResizeMode;          // HTNOWHERE, HTCAPTION (moving), HTLEFT, or HTRIGHT
    CPoint omPointResize;       // the point where moving/resizing begin
    CRect omRectResize;         // current focus rectangle for moving/resizing
    int imBorderPreLeft;        // define the sensitivity when user detect bar border
    int imBorderPreRight;       // define the sensitivity when user detect bar border

	CCS3DStatic *pomTopScaleText;	

	CMenu omRButtonMenu;
	CMenu omGatesMenu;
	CPoint omMenuPoint;
	CPtrArray omMenuConfPtr;
	GATDIA_BARDATA *prmClickedBar;
	GATDIA_LINEDATA *prmClickedLine;

	CCSDragDropCtrl m_DragDropSource;
	CCSDragDropCtrl m_DragDropTarget;
//-DTT Jul.25-----------------------------------------------------------
	CPoint omDropPosition;

	int imDragItem;
	int imDragBarno;

	int imLastItemID;

//----------------------------------------------------------------------
//rkr01042001 show default allocation times
	bool ShowBestGatTime(const DIAFLIGHTDATA *prlFlight, const long llGatno, const CString &ropGat);

	bool blRubberBand;
	int imRubberStartLine;
	int imRubberEndLine;
	CPoint omRubberStartPoint;
	CPoint omRubberEndPoint;
	CTime omRubberStartTime;
	CTime omRubberEndTime;

	//GOCC. To show icon popup menu - Start
	void ShowPopupImgMenu(const int itemId, GATDIA_BARDATA* popBarData, CPoint point);
	void HidePopupImgMenu();
	void LaunchFlightDataTool(const char* pcpMsg);
	bool GetFlightDataToolPath(char* pclFlightDataToolPath);
	bool GetConfigInfo(const char* pcpConfigName, char* pcpConfigValue);
	
	CPopupImgMenu* pomPopupImgMenu;

	char pcmAlertInfo[256];
	char pcmFlightDataToolPath[512];
	DIAFLIGHTDATA *prmClickedFlightA;
	DIAFLIGHTDATA *prmClickedFlightD;

    //GOCC. To show icon popup menu - End

protected:
    // Generated message map functions
    //{{AFX_MSG(GatGantt)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnDestroy();
    afx_msg void OnSize(UINT nType, int cx, int cy);
    afx_msg BOOL OnEraseBkgnd(CDC* pDC);
    afx_msg void OnMouseMove(UINT nFlags, CPoint point);
    afx_msg BOOL OnMouseWheel(UINT nFlags, short zDelta, CPoint point);
    afx_msg void OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
    afx_msg void OnTimer(UINT nIDEvent);
    afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
    afx_msg LONG OnDragOver(UINT, LONG); 
    afx_msg LONG OnDrop(UINT, LONG); 
 	afx_msg LONG OnDragTime(UINT wParam, LONG lParam);
	afx_msg LONG OnDragEnter(UINT wParam, LONG lParam);
	afx_msg LONG OnDragLeave(UINT wParam, LONG lParam);
	afx_msg void OnGatesMenu( UINT nID  );
	afx_msg void OnDelete(  );
	afx_msg void OnCallCuteIF(  );
	afx_msg void OnRButtonDblClk(UINT nFlags, CPoint point);
	afx_msg void OnMenuConf( UINT nID  );
	afx_msg void OnConfirmAll( );
	afx_msg void OnCheckIn(  );
	afx_msg void OnBaggage( );
	afx_msg void OnMeal( );
	afx_msg void OnEditLoad(  );
	//}}AFX_MSG
    DECLARE_MESSAGE_MAP()

};
 
/////////////////////////////////////////////////////////////////////////////
//extern GatGantt *pogFlightGantt;

#endif
