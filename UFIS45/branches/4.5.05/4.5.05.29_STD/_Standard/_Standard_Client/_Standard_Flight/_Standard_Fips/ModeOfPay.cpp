// ModeOfPay.cpp : implementation file
//

#include "stdafx.h"
# include <windows.h>
#include "fpms.h"
#include "ModeOfPay.h"


#include <CCSEdit.h>
#include <SetupDlg.h>
#include <CedaCfgData.h>
#include <BasicData.h>
#include <CedaBasicData.h>
#include <CCSGlobl.h>
#include <CCSDDX.h>
#include <PrivList.h>
#include <Utils.h>
#include <RotationDlgCedaFlightData.h>
#include <CedaMopData.h>
#include <CCSCedaData.h>



#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif



//CModeOfPay(CWnd* pParent = NULL,ROTATIONDLGFLIGHTDATA prmFlight,CString strFlightType);   // standard constructor
CModeOfPay::CModeOfPay(CWnd* pParent /*=NULL*/,long lFlightUrno,CString FlightType)
	: CDialog(CModeOfPay::IDD, pParent)
{
	//{{AFX_DATA_INIT(CModeOfPay)
	//}}AFX_DATA_INIT

	olFlightUrno = lFlightUrno;
	strFlightType = FlightType;
	
	pomRFlight=NULL;
	pomSFlight=NULL;
	
}


void CModeOfPay::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CModeOfPay)
	DDX_Control(pDX, IDC_BILL_TO, m_strBillTo);
	DDX_Control(pDX, IDC_PRMK, m_strPRMK);
	DDX_Control(pDX, IDC_CASH, m_rdCash);
	DDX_Control(pDX, IDC_BANK, m_rdBank);
	DDX_Control(pDX, IDC_COLLECTED_BY, m_cbCollectedBy);
	DDX_Control(pDX, IDC_HANDLING_AGENTS, m_cbHandlingAgents);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CModeOfPay, CDialog)
	//{{AFX_MSG_MAP(CModeOfPay)
	ON_BN_CLICKED(IDC_REQUIRED, OnRequired)
	ON_BN_CLICKED(IDC_NOT_REQUIRED, OnNotRequired)
	ON_BN_CLICKED(IDC_BANK, OnBank)
	ON_BN_CLICKED(IDC_CASH, OnCash)
	ON_CBN_SELCHANGE(IDC_COLLECTED_BY, OnSelchangeCollectedBy)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CModeOfPay message handlers

BOOL CModeOfPay::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	m_Caption = strFlightType == "A" ? "Arrival" : "Departure";
	m_Caption = GetString(IDS_STRING2938) + " <" + m_Caption + ">";
	SetWindowText(m_Caption);
	
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));

	m_strBillTo.SetSecState(ogPrivList.GetStat("PAYDLG_CE_BillTo"));
	
	LoadAFTRelatedData();
	
	
	BOOL isNew=TRUE;
	isNew = HaveRecordForCFG(olFlightUrno,strFlightType);
	
	//SetInitialScreen(!isNew);
	

	//Set the access right to Bill To
	

	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}
CString CModeOfPay::LoadAFTRelatedData()
{
	CString olCoby,olHdag,olBlto,olPrmk,olAlc3,olMopa;
	

	if(pomRFlight!= NULL)
	{
		olCoby = CString(pomRFlight->Coby);
		olHdag = CString(pomRFlight->Hdag);
		olBlto = CString(pomRFlight->Blto);
		olPrmk = CString(pomRFlight->Prmk);
		olAlc3 = CString(pomRFlight->Alc3);
		olMopa = CString(pomRFlight->Mopa);
	
	}
	else if(pomSFlight!= NULL)
	{
		olCoby = CString(pomSFlight->Coby);
		olHdag = CString(pomSFlight->Hdag);
		olBlto = CString(pomSFlight->Blto);
		olPrmk = CString(pomSFlight->Prmk);
		olAlc3 = CString(pomSFlight->Alc3);
		olMopa = CString(pomSFlight->Mopa);
	}
	else
	{
		CString olSelection;
		olSelection.Format("WHERE URNO='%d'",olFlightUrno);

		CString olDummyTableName="AFT";
		
		ogBCD.SetObject(olDummyTableName, "URNO,COBY,HDAG,BLTO,PRMK,ALC3,MOPA");
		ogBCD.SetObjectDesc(olDummyTableName, "Configuration");
		ogBCD.Read(olDummyTableName,olSelection);

		int ndxCoby = ogBCD.GetFieldIndex( olDummyTableName, "COBY" );		
		int ndxHdag = ogBCD.GetFieldIndex( olDummyTableName, "HDAG" );		
		int ndxBlto = ogBCD.GetFieldIndex( olDummyTableName, "BLTO" );	
		int ndxPrmk = ogBCD.GetFieldIndex( olDummyTableName, "PRMK" );	
		int ndxAlc3 = ogBCD.GetFieldIndex( olDummyTableName, "ALC3" );	
		int ndxMopa = ogBCD.GetFieldIndex( olDummyTableName, "MOPA" );
		int nItems = ogBCD.GetDataCount( olDummyTableName );


		CString olCoby,olHdag,olBlto,olPrmk,olRrmk,olRreg,olAlc3,olMopa;
		for( int ndx = 0 ; ndx < nItems ; ndx++ )		// for each entry
		{
			RecordSet record ;
			if( ogBCD.GetRecord( olDummyTableName, ndx, record ) )
			{
				olCoby = record[ ndxCoby ];
				olHdag = record[ ndxHdag ];
				olBlto = record[ ndxBlto ];
				olPrmk = record[ ndxPrmk ];
				olAlc3 = record[ ndxAlc3 ];
				olMopa = record[ ndxMopa ];
			}
		}
	}
	

	m_strBillTo.SetWindowText(olBlto);
	m_strPRMK.SetWindowText(olPrmk);

	//LoadHandlingAgents(olHdag);
	LoadHandlingAgents(&m_cbHandlingAgents,olHdag);
	LoadCollectedBy(olCoby);

	//Bank or Cash
	if(olFlightUrno ==0)
	{
		if(olMopa != "")
		{
			if(olMopa == "B")
			{
				m_rdCash.SetCheck(1);
				OnCash();
			}
			else if(olMopa == "K")
			{
				m_rdBank.SetCheck(1);
				OnBank();
			}
		}
		else
		{
			m_rdBank.SetCheck(1);
			OnBank();
		}
	}
	else
	{
		if(olMopa != "")
		{
			if(olMopa == "B")
			{
				m_rdCash.SetCheck(1);
				OnCash();
			}
			else if(olMopa == "K")
			{
				m_rdBank.SetCheck(1);
				OnBank();
			}
		}
		else
		{
			CString olCash("");
			ogBCD.GetField("ALT", "ALC3", olAlc3, "CASH", olCash);

			if(olCash == "" || olCash=="B")
			{
				m_rdCash.SetCheck(1);
				OnCash();
			}
			else
			{
				m_rdBank.SetCheck(1);
				OnBank();
			}
		}
	}
	return "";
	
}
void CModeOfPay::LoadMOPData(CString olRreg)
{
	BOOL isExisting=TRUE;
	if(olFlightUrno>0)
	{
		isExisting = HaveRecordForCFG(olFlightUrno,strFlightType);
		if(isExisting)
		{
			LoadExistingControls(olFlightUrno,strFlightType,olRreg);
		}
		else
		{
			LoadRequiredControls(olFlightUrno,strFlightType);
		}
	}
	else
	{
			LoadRequiredControls(olFlightUrno,strFlightType);
	}
	
}
void CModeOfPay::LoadCollectedBy(CString strToSelect)
{
	m_cbCollectedBy.ResetContent();		
	m_cbCollectedBy.AddString( " " );		
	m_cbCollectedBy.AddString( "Airport" );
	m_cbCollectedBy.AddString( "H.Agent" );

	if(strToSelect== "Airport")
	{
		m_cbCollectedBy.SetCurSel(1);
	}
	else if(strToSelect== "H.Agent")
	{
		m_cbCollectedBy.SetCurSel(2);
	}
	else
	{
		m_cbCollectedBy.SetCurSel(0);
	}
}



/*void CModeOfPay::LoadHandlingAgents(CString strToSelect)
{
		m_cbHandlingAgents.ResetContent();		// remove all items from list
		m_cbHandlingAgents.AddString( "" );		// 050419 MVy: empty entry to clear box with mouse

		
 		//int ndxCol = ogBCD.GetFieldIndex( "HAG", "HSNA" );		// Table Handling Agent Column short name
		int ndxCol = ogBCD.GetFieldIndex( "HAG", "HNAM" );		// Table Handling Agent Column short name
		int ndxURNOCol = ogBCD.GetFieldIndex( "HAG", "URNO" );	
		int nItems = ogBCD.GetDataCount( "HAG" );
		m_cbHandlingAgents.InitStorage( nItems, 30 );
		
		int intToSelect=0;
		for( int ndx = 0 ; ndx < nItems ; ndx++ )		// for each entry
		{
			RecordSet record ;
			if( ogBCD.GetRecord( "HAG", ndx, record ) )
			{
				ASSERT( 0 <= ndxCol && ndxCol < record.Values.GetSize() );
				CString sItem = record[ ndxCol ];

				CString sUrnoItem = record[ ndxURNOCol ];

				if(strToSelect == sItem)
				{
					intToSelect= ndx;
				}
				m_cbHandlingAgents.AddString( sItem );

			
			}
			else
			{ ASSERT(0); };		// data not found ... maybe some inconsistency
		};	// for each agent

		m_cbHandlingAgents.SetCurSel(intToSelect);
}*/

void CModeOfPay::LoadHandlingAgents(CComboBox* ctrl,CString strToSelect)
{
		ctrl->ResetContent();		// remove all items from list
		ctrl->AddString( "" );		// 050419 MVy: empty entry to clear box with mouse

		
 		//int ndxCol = ogBCD.GetFieldIndex( "HAG", "HSNA" );		// Table Handling Agent Column short name
		int ndxCol = ogBCD.GetFieldIndex( "HAG", "HNAM" );		// Table Handling Agent Column short name
		int ndxURNOCol = ogBCD.GetFieldIndex( "HAG", "URNO" );	
		int nItems = ogBCD.GetDataCount( "HAG" );
		ctrl->InitStorage( nItems, 30 );
		
		int intToSelect=0;
		for( int ndx = 0 ; ndx < nItems ; ndx++ )		// for each entry
		{
			RecordSet record ;
			if( ogBCD.GetRecord( "HAG", ndx, record ) )
			{
				ASSERT( 0 <= ndxCol && ndxCol < record.Values.GetSize() );
				CString sItem = record[ ndxCol ];

				CString sUrnoItem = record[ ndxURNOCol ];

				if(strToSelect.GetLength() > 0 && strToSelect == sItem )
				{
					intToSelect= ndx;
				}
				ctrl->AddString( sItem );

			
			}
			else
			{ ASSERT(0); };		// data not found ... maybe some inconsistency
		};	// for each agent

		//ctrl->SetCurSel(intToSelect);

		
		if(strToSelect.GetLength() > 0)
		{
			ctrl->SetCurSel(intToSelect+1);
		}
		else
		{
			ctrl->SetCurSel(intToSelect);
		}
}


void CModeOfPay::LoadRequiredControls(long olUrno,CString strADID)
{
	
	

	CRect olWindowRect;	
	((CWnd*)GetDlgItem(IDC_REQUIRED))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);

	CString olDummyTableName(CString("CFG_") + strADID);
	olDummyTableName="CFG";
	
	int ndxCol = ogBCD.GetFieldIndex( olDummyTableName, "DESP" );		// Table Handling Agent Column short name
	int ndxURNOCol = ogBCD.GetFieldIndex( olDummyTableName, "URNO" );	
	int nItems = ogBCD.GetDataCount( olDummyTableName );


	DWORD dwStyle = WS_CHILD|WS_VISIBLE|BS_AUTOCHECKBOX ;



	long left_x= olWindowRect.left;
	long right_x= olWindowRect.right + 15;
	long top_y= 50;
	long bottom_y= 70;	

	int controlID=8000000;	

	for( int ndx = 0 ; ndx < nItems ; ndx++ )
	{
		controlID++;
		CString sItem;

		RecordSet record ;
		if( ogBCD.GetRecord( olDummyTableName, ndx, record ) )
		{
			 sItem = record[ ndxCol ];
		}
		
		
		CButton* radioButton = new CButton();
		radioButton->Create(sItem, dwStyle, CRect(left_x,top_y,right_x,bottom_y), this,controlID);
		radioButton ->SetFont(((CWnd*)GetDlgItem(IDC_REQUIRED))->GetFont());

		top_y+=30;
		bottom_y+=30;
	}	
}

BOOL CModeOfPay::HaveRecordForCFG(long olUrno,CString strADID)
{
	if(olUrno==0)
		return FALSE;
	BOOL hasRecords= FALSE;
	CString olSelection;
	olSelection.Format("WHERE RURN='%ld'",olUrno);

	ogBCD.SetObject("MOP", "URNO,RURN,VALU,CTYP,HOPO");
	ogBCD.SetObjectDesc("MOP", "Mode of Payment");
	ogBCD.Read("MOP",olSelection);

	int nItems = ogBCD.GetDataCount( "MOP" );

	if(nItems>0)
	{
		hasRecords=TRUE;
	}

	return hasRecords;
}

void CModeOfPay::LoadExistingControls(long olUrno,CString strADID,CString olRreq)
{
	
	m_rdCash.EnableWindow(FALSE);
	m_rdBank.EnableWindow(FALSE);

	CRect olWindowRect;	
	((CWnd*)GetDlgItem(IDC_REQUIRED))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);

	CString olDummyTableName("CFG");
	

	int ndxCol = ogBCD.GetFieldIndex( olDummyTableName, "DESP" );		
	int ndxURNOCol = ogBCD.GetFieldIndex( olDummyTableName, "URNO" );	
	int nItems = ogBCD.GetDataCount( olDummyTableName );

	
	DWORD dwStyle3 = WS_CHILD|WS_VISIBLE|BS_AUTOCHECKBOX ;
	DWORD dwStyle1 = WS_GROUP|WS_CHILD|BS_AUTORADIOBUTTON | WS_VISIBLE  ;
	DWORD dwStyle2 = WS_CHILD|BS_AUTORADIOBUTTON | WS_VISIBLE ;

	

	long left_x= olWindowRect.left;
	long right_x= olWindowRect.right + 15;
	long top_y= 50;
	long bottom_y= 70;	


	int controlID=8000000;	
	int controlID2=6000;	

	for( int ndx = 0 ; ndx < nItems ; ndx++ )
	{
		CString sItem;
		CString sUrno;
		RecordSet record ;
		if( ogBCD.GetRecord( olDummyTableName, ndx, record ) )
		{
			 sItem = record[ ndxCol ];
			 sUrno = record[ ndxURNOCol];
		}

		CCSPtrArray<RecordSet> omData;
		char pclWhere[128];
		sprintf(pclWhere,"WHERE CREF = '%s' AND RURN='%ld'", sUrno,olUrno);

		CString olValu ="";
		CString olAvail ="";

		if(ogBCD.ReadSpecial( "MOP", "VALU,AVIL", pclWhere, omData))
		{
			if(omData.GetSize() > 0)
			{
				RecordSet *prlRecord = &omData[0];
				olValu =  (*prlRecord)[0];
				olAvail = (*prlRecord)[1];
			}
		}

		controlID++; 
		BOOL chkBox= FALSE;

		if(olValu == "1")
		{
			chkBox = TRUE;
		}
		BOOL ctrlEnable= TRUE;
		if(olRreq=="0")
		{
			ctrlEnable=FALSE;
		}

		DrawControls(sItem,dwStyle3, CRect(left_x,top_y,right_x,bottom_y),controlID,chkBox,ctrlEnable);

		BOOL rdAvail= FALSE;
		BOOL rdNAvail= FALSE;

		if(olAvail=="1")
		{
			rdAvail= TRUE;
			rdNAvail= FALSE;
		}
		else if(olAvail=="0")
		{
			rdAvail= FALSE;
			rdNAvail= TRUE;
		}

	
		controlID2++;
		DrawControls("",dwStyle1, CRect(right_x + 10,top_y,right_x+30,bottom_y),controlID2,rdAvail,ctrlEnable);


		controlID2++;
		DrawControls("",dwStyle2, CRect(right_x + 40,top_y,right_x+70,bottom_y),controlID2,rdNAvail,ctrlEnable);

		

		top_y+=30;
		bottom_y+=30;
	}	
}

void CModeOfPay::DrawControls(CString itemText, DWORD dwStyle1,CRect rect,int controlID,BOOL CHECKED,BOOL ENABLED)
{
	CButton* control = new CButton();
	control->Create(itemText, dwStyle1, rect, this,controlID);
	control ->SetFont(((CWnd*)GetDlgItem(IDC_REQUIRED))->GetFont());
	if(CHECKED)
	{
		control->SetCheck(1);
	}

	control->EnableWindow(ENABLED);
	
}

char* CModeOfPay::GetFieldList()
{
	return m_pclFieldList;
}

char* CModeOfPay::GetDataList()
{
	return m_pclData;
}



void CModeOfPay::Save()
{
	CString olCoby("");
	int ilCurSel = m_cbCollectedBy.GetCurSel();
	m_cbCollectedBy.GetLBText(ilCurSel, olCoby);

	CString olHdag("");
	if(olCoby == "H.Agent")
	{
		ilCurSel = m_cbHandlingAgents.GetCurSel();
		m_cbHandlingAgents.GetLBText(ilCurSel, olHdag);
	}
	

	CString olBillTo("");
	m_strBillTo.GetWindowText(olBillTo);

	CString olPrmk("");
	m_strPRMK.GetWindowText(olPrmk);


	CString olMopa("");
	if(m_rdCash.GetCheck() == 1)
	{
		 olMopa= "B";
	}

	if(m_rdBank.GetCheck() == 1)
	{
		 olMopa= "K";
	}

	//Fill flight record with data
	if(pomRFlight!= NULL)
	{
		strcpy(pomRFlight->Coby,olCoby);
		strcpy(pomRFlight->Hdag,olHdag);
		strcpy(pomRFlight->Blto,olBillTo);
		strcpy(pomRFlight->Prmk,olPrmk);
		strcpy(pomRFlight->Mopa,olMopa);

		
	}


	if(pomSFlight!= NULL)
	{
		strcpy(pomSFlight->Coby,olCoby);
		strcpy(pomSFlight->Hdag,olHdag);
		strcpy(pomSFlight->Blto,olBillTo);
		strcpy(pomSFlight->Prmk,olPrmk);
		strcpy(pomSFlight->Mopa,olMopa);


	}

}

void CModeOfPay::OnOK() 
{
	Save();	
	CDialog::OnOK();	

}

void CModeOfPay::OnCancel() 
{
	CDialog::OnCancel();
}

void CModeOfPay::SetFlightNo(long lFlightUrno)
{
	olFlightUrno = lFlightUrno;
}


void CModeOfPay::SetFlightType(CString FlightType)
{
	strFlightType = FlightType;
}
void CModeOfPay::SetMode(CString ScreenMode)
{
	strMode = ScreenMode;
}

void CModeOfPay::OnRequired() 
{
	// TODO: Add your control notification handler code here	
	SetResourceSection(true);
}

void CModeOfPay::OnNotRequired() 
{
	SetResourceSection(false);
}

void CModeOfPay::OnBank() 
{
	SetPaymentSection(false);
}

void CModeOfPay::OnCash() 
{
	m_cbCollectedBy.EnableWindow(true);
	m_cbHandlingAgents.EnableWindow(false);
	OnSelchangeCollectedBy();
	m_strBillTo.EnableWindow(true);
	m_strPRMK.EnableWindow(true);
}

void CModeOfPay::SetInitialScreen(BOOL isNew)
{
	
	if(m_rdBank.GetCheck() == 1)
	{
		SetPaymentSection(false);
	}
	else if(m_rdCash.GetCheck() == 1)
	{
		OnCash();
	}
	else
	{
		SetAllSection(false);
	}

}

void CModeOfPay::SetAllSection(bool bEnable)
{
	SetPaymentSection(bEnable);
	//SetResourceSection(bEnable);
}

void CModeOfPay::SetPaymentSection(bool bEnable) 
{
	m_cbCollectedBy.EnableWindow(bEnable);
	
	m_cbHandlingAgents.EnableWindow(bEnable);

	if(ogPrivList.GetStat("PAYDLG_CE_BillTo") == '1')
	{
		m_strBillTo.EnableWindow(bEnable);
	}
	else
	{
		m_strBillTo.EnableWindow(FALSE);
	}
	//m_strPRMK.EnableWindow(bEnable);
}

void CModeOfPay::SetResourceSection(bool bEnable) 
{
	/*
	m_strRRMK.EnableWindow(bEnable);
	int nItems = GetConfigCount();
	int controlID=8000000;	
	int controlID2=6000;
	for( int ndx = 0 ; ndx < nItems ; ndx++ )
	{
		controlID++;
		CButton *polButton = (CButton *)GetDlgItem(controlID);
		polButton->EnableWindow(bEnable);
		
		
		if(strMode != "Insert")
		{
			controlID2++;
			CButton *polRadio = (CButton *)GetDlgItem(controlID2);
			polRadio->EnableWindow(bEnable);
			
			controlID2++;
			CButton *polRadio2 = (CButton *)GetDlgItem(controlID2);
			polRadio2->EnableWindow(bEnable);
		}	
	}
	*/

}

void CModeOfPay::LoadConfigForResources(CString strADID)
{

/*	CString olDummyTableName(CString("CFG_") + strADID);
	CString olSelection;
	olSelection.Format("WHERE STAT='I' AND FTYP='%s'",strADID);
	olDummyTableName="CFG";
	ogBCD.SetObject(olDummyTableName, "URNO,CODE,DESP,HOPO,FTYP");
	ogBCD.SetObjectDesc(olDummyTableName, "Configuration" + strADID);
	ogBCD.Read(olDummyTableName,olSelection);*/
}

int CModeOfPay::GetConfigCount()
{
	CString olDummyTableName="CFG";
	int nItems = ogBCD.GetDataCount( olDummyTableName );
	return nItems;

}

void CModeOfPay::OnSelchangeCollectedBy() 
{
	CString olTmp("");
	int ilCurSel = m_cbCollectedBy.GetCurSel();
	m_cbCollectedBy.GetLBText(ilCurSel, olTmp);
	if(olTmp =="H.Agent")
	{
		m_cbHandlingAgents.EnableWindow(true);
	}
	else
	{
		m_cbHandlingAgents.EnableWindow(false);
	}
	
}

BOOL CModeOfPay::DestroyWindow() 
{
	return CDialog::DestroyWindow();
}

void CModeOfPay::SetRotaionFlight(ROTATIONDLGFLIGHTDATA* pomFlight) 
{
	pomRFlight = pomFlight;
	pomSFlight=NULL;

	
}
void CModeOfPay::SetSeasonFlight(SEASONDLGFLIGHTDATA* pomFlight) 
{
	pomSFlight = pomFlight;
	pomRFlight=NULL;

	
}