// BltKonflikteDlg.cpp : implementation file
//

#include <stdafx.h>
#include <fpms.h>
#include <BltKonflikteDlg.h>
#include <Konflikte.h>
#include <RotationDlg.h>
#include <SeasonDlg.h>
#include <BltDiagram.h>
#include <CCSGlobl.h>
#include <CCSDdx.h>
#include <PrivList.h>
#include <Utils.h>
#include <process.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif



// Local function prototype
// Broadcast -Funktion
static void BltKonfTableCf(void *popInstance, int ipDDXType,void *vpDataPointer, CString &ropInstanceName);



/////////////////////////////////////////////////////////////////////////////
// BltKonflikteDlg dialog




BltKonflikteDlg::BltKonflikteDlg(CWnd* pParent /*=NULL*/)
	: CDialog(BltKonflikteDlg::IDD, pParent), imDlgBarHeight(50)
{
	//{{AFX_DATA_INIT(BltKonflikteDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT

	pomTable = NULL;
	bmIsActiv = false;
	bmIsCreated = false;
    CDialog::Create(BltKonflikteDlg::IDD, pParent);
	bmIsCreated = true;
	omUsedFor = CONF_BLT;
	
	CRect olRect;
	this->GetWindowRect(&olRect);
	OnSize(0, olRect.Width(), olRect.Height()); //Move buttons
}


BltKonflikteDlg::~BltKonflikteDlg()
{
	ogDdx.UnRegister(this,NOTUSED);
	delete pomTable;
}



void BltKonflikteDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(BltKonflikteDlg)
	DDX_Control(pDX, IDC_ALLCONFIRM, m_CB_AllConfirm);
	DDX_Control(pDX, IDC_CONFIRM, m_CB_Confirm);
	DDX_Control(pDX, IDCANCEL, m_CB_Cancel);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(BltKonflikteDlg, CDialog)
	//{{AFX_MSG_MAP(BltKonflikteDlg)
	ON_WM_SIZE()
	ON_BN_CLICKED(IDC_CONFIRM, OnConfirm)
	ON_BN_CLICKED(IDC_ALLCONFIRM, OnAllconfirm)
	ON_MESSAGE(WM_TABLE_LBUTTONDBLCLK, OnTableLButtonDblclk)
    ON_MESSAGE(WM_TABLE_RETURN_PRESSED, OnTableReturnPressed)
	ON_MESSAGE(WM_TABLE_SELCHANGE, OnTableSelChanged)
	ON_BN_CLICKED(IDC_PRINT, OnPrint)
	ON_BN_CLICKED(IDC_EXCEL, OnExcel)
	ON_WM_SIZE()
  	ON_WM_DESTROY()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// BltKonflikteDlg message handlers

BOOL BltKonflikteDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	// Register broadcasts
	ogDdx.Register(this, KONF_DELETE, CString("KONF_DELETE"),CString("BLTKONFLIKTEDLG"),	BltKonfTableCf);
	ogDdx.Register(this, KONF_INSERT, CString("KONF_INSERT"),CString("BLTKONFLIKTEDLG"),	BltKonfTableCf);
	ogDdx.Register(this, KONF_CHANGE, CString("KONF_CHANGE"),CString("BLTKONFLIKTEDLG"),	BltKonfTableCf);

//	m_CB_Confirm.SetSecState(ogPrivList.GetStat("BLTCONFLICTS_CONFIRM"));		
//	m_CB_AllConfirm.SetSecState(ogPrivList.GetStat("BLTCONFLICTS_CONFIRMALL"));		
	if (omUsedFor == CONF_BLT)
		m_key = "DialogPosition\\BaggageBeltAttention";
	else
		m_key = "DialogPosition\\CheckInAttention";

	CRect olRect;
	this->GetWindowRect(&olRect);
	GetDialogFromReg(olRect, m_key);
	SetWindowPos(&wndTop, olRect.left, olRect.top, olRect.Width(), olRect.Height(), SWP_HIDEWINDOW);// | SWP_NOMOVE );

	InitTable();

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void BltKonflikteDlg::SaveToReg()
{
	BOOL ok = WriteDialogToReg((CWnd*) this, m_key);
}


void BltKonflikteDlg::OnDestroy() 
{
	SaveToReg();
	CDialog::OnDestroy();
}

void BltKonflikteDlg::InitTable() 
{

	CRect olRect;
    GetClientRect(&olRect);
	olRect.top += imDlgBarHeight; 

	// Create Table
    pomTable = new CCSTable;
	pomTable->SetSelectMode(LBS_MULTIPLESEL | LBS_EXTENDEDSEL);
	//pomTable->SetHeaderSpacing(0);

	// Positioning table
    olRect.InflateRect(1, 1);     // hiding the CTable window border
	pomTable->SetTableData(this, olRect.left, olRect.right, olRect.top, olRect.bottom);


	CCSPtrArray <TABLE_HEADER_COLUMN> omHeaderDataArray;

	pomTable->ResetContent();
	
	// Set table header
	TABLE_HEADER_COLUMN rlHeader;
	rlHeader.Alignment = COLALIGN_LEFT;
	rlHeader.Length = 1500; 
	rlHeader.Font = &ogCourier_Regular_10;
	rlHeader.Text = GetString(IDS_STRING1429);
	omHeaderDataArray.New(rlHeader);
	
	// Set table attributes
	pomTable->SetHeaderFields(omHeaderDataArray);
	pomTable->SetDefaultSeparator();
	pomTable->SetTableEditable(false);
	omHeaderDataArray.DeleteAll();
	pomTable->DisplayTable();

}


void BltKonflikteDlg::Attention(int opUsedFor) 
{
	// Show Dialog
	omUsedFor = opUsedFor;

	if (omUsedFor == CONF_BLT)
		m_key = "DialogPosition\\BaggageBeltAttention";
	else if (omUsedFor == CONF_CCA)
		m_key = "DialogPosition\\CheckInAttention";

	CRect olRect;
	this->GetWindowRect(&olRect);
	GetDialogFromReg(olRect, m_key);
	SetWindowPos(&wndTop, olRect.left, olRect.top, olRect.Width(), olRect.Height(), SWP_SHOWWINDOW);// | SWP_NOMOVE );


//	ShowWindow(SW_SHOW);
//	SetWindowPos(&wndTop, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE);

	bmAttention = true;
	bmIsActiv = true;
	m_CB_AllConfirm.ShowWindow(SW_SHOW);
	m_CB_Confirm.ShowWindow(SW_SHOW);

	if (omUsedFor == CONF_CCA)
	{
		SetWindowText(GetString(IDS_STRING2391));
		m_CB_Confirm.SetSecState(ogPrivList.GetStat("CCACONFLICTS_CONFIRM"));		
		m_CB_AllConfirm.SetSecState(ogPrivList.GetStat("CCACONFLICTS_CONFIRMALL"));		
	}
	else
	{
		SetWindowText(GetString(IDS_STRING1899));
		m_CB_Confirm.SetSecState(ogPrivList.GetStat("BLTCONFLICTS_CONFIRM"));		
		m_CB_AllConfirm.SetSecState(ogPrivList.GetStat("BLTCONFLICTS_CONFIRMALL"));		
	}
	// Rebuild table
	pomTable->ResetContent();
	ChangeViewTo();
}


void BltKonflikteDlg::ChangeViewTo()
{
	// Rebuild table
	DeleteAll();    
	MakeLines();
	UpdateDisplay();
}


void BltKonflikteDlg::DeleteAll()
{
	omLines.DeleteAll();
}

// Create Table lines
void BltKonflikteDlg::MakeLines()
{
	BLTKONF_LINEDATA rlLine;
	const KONFENTRY *prlEntry;
	const KONFDATA  *prlData;

	// Scan all global Conflicts
	for(int i = ogKonflikte.omData.GetSize() - 1; i >= 0; i--)
	{
		prlEntry = &ogKonflikte.omData[i];
		// Scan all entrys in each conflict
		for(int j = prlEntry->Data.GetSize() - 1; j >= 0; j--)
		{
			prlData = &prlEntry->Data[j];
			// Only if conflict is configured for baggage-belt 
//			if (ogCfgData.CheckKonfliktForDisplay(prlData->KonfId.Type,CONF_BLT) == true)
			if (ogCfgData.CheckKonfliktForDisplay(prlData->KonfId.Type,omUsedFor) == true)
			{
				if((bmAttention && !prlData->Confirmed) || !bmAttention)
				{
					// Make new line
					rlLine.KonfId = prlData->KonfId;
					rlLine.Text = prlData->Text;
					rlLine.TimeOfConflict = prlData->TimeOfConflict;

					if( __CanSeeConflictPriorityColor() )		// 050323 MVy: PRF6981: conflict priority, make configurable, addendum
					{
						rlLine.clrPriority = prlData->clrPriority ;		// 050303 MVy: PRF6981: set line color
					};

					CreateLine(rlLine);
				}
			}
		}
	}
}


// Create new line
int BltKonflikteDlg::CreateLine(BLTKONF_LINEDATA &rrpKonf)
{
    int ilLineCount = omLines.GetSize();

    for (int ilLineno = ilLineCount; ilLineno > 0; ilLineno--)
        if (CompareKonf(rrpKonf, omLines[ilLineno-1]) >= 0)
            break;  // should be inserted after Lines[ilLineno-1]
	// insert line
    omLines.NewAt(ilLineno, rrpKonf);

    return ilLineno;
}


// Compare two lines for sorting
int BltKonflikteDlg::CompareKonf(const BLTKONF_LINEDATA &rrpKonf1, const BLTKONF_LINEDATA &rrpKonf2) const
{
	int	ilCompareResult = (rrpKonf1.TimeOfConflict == rrpKonf2.TimeOfConflict)? 0:
		(rrpKonf1.TimeOfConflict > rrpKonf2.TimeOfConflict)? 1: -1;
	return ilCompareResult;
}


void BltKonflikteDlg::UpdateDisplay()
{
	CCSPtrArray<TABLE_COLUMN> olColList;
	TABLE_COLUMN rlColumnData;

  	pomTable->ResetContent();
	// Scan local lines
    for (int ilLc = 0; ilLc < omLines.GetSize(); ilLc++)
	{
		rlColumnData.Text = omLines[ilLc].Text;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		// add line into table
		if( __CanSeeConflictPriorityColor() )		// 050323 MVy: PRF6981: conflict priority, make configurable, addendum
		{
			olColList[0].BkColor = omLines[ilLc].clrPriority ;		// 050303 MVy: PRF6981: set background color of cell
		};

		pomTable->AddTextLine(olColList , &omLines[ilLc]);
		olColList.DeleteAll();
    }                                           
    pomTable->DisplayTable();
	CheckPostFlightPosDia(0,this);
}



// Confirm selected conflicts
void BltKonflikteDlg::OnConfirm() 
{

	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));

	const BLTKONF_LINEDATA *prlLine;

	CListBox *polLB = pomTable->GetCTableListBox();
	if (polLB == NULL)
		return;

	int *ilItems = NULL;
	int ilAnz = polLB->GetSelCount();
	if(ilAnz > 0)
	{
		ilItems = new int[ilAnz];
	}

	if(ilItems != NULL)
		ilAnz = polLB->GetSelItems(ilAnz, ilItems);
	else
		return;


	CPtrArray olKonfIds;
	for(int i = 0; i < ilAnz; i++)
	{
		prlLine = (BLTKONF_LINEDATA *)pomTable->GetTextLineData(ilItems[i]);
		if(prlLine != NULL)
		{
			// collect records to confirm
			olKonfIds.Add((void *)&prlLine->KonfId);
		}
	}

	ogKonflikte.Confirm(olKonfIds); 
	
	if(ilItems != NULL)
		delete [] ilItems;


	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
}


// confirm all conflicts
void BltKonflikteDlg::OnAllconfirm() 
{
	if (pomTable->GetLinesCount() > 0)
	{
		if (MessageBox(GetString(IDS_STRING1968), GetString(ST_FRAGE), MB_YESNO | MB_ICONQUESTION | MB_DEFBUTTON2) == IDYES)
		{
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			// confirm all conflicts
			ogKonflikte.ConfirmAll(CONF_BLT);
			OnCancel();
			if (pogBltDiagram)
			{
				pogBltDiagram->m_CB_BltAttention.SetColors(::GetSysColor(COLOR_BTNFACE), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
			}
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
		}
	}
}





void BltKonflikteDlg::OnCancel() 
{
	omLines.DeleteAll();
	bmIsActiv = false;
	// only hide window
	ShowWindow(SW_HIDE);
	SaveToReg();
	//CDialog::OnCancel();
}


void BltKonflikteDlg::OnSize(UINT nType, int cx, int cy) 
{
	if(bmIsCreated != false && cx > 250)
	{
		CDialog::OnSize(nType, cx, cy);
	
		if (nType != SIZE_MINIMIZED)
		{
			// resize table
			pomTable->SetPosition(1, cx+1, imDlgBarHeight+1, cy+1);
			// reposition buttons
			m_CB_AllConfirm.SetWindowPos(this, cx-234,3,0,0, SWP_NOSIZE | SWP_NOZORDER | SWP_SHOWWINDOW);
			m_CB_Confirm.SetWindowPos(this, cx-159,3,0,0, SWP_NOSIZE | SWP_NOZORDER | SWP_SHOWWINDOW);
			m_CB_Cancel.SetWindowPos(this, cx-77,3,0,0, SWP_NOSIZE | SWP_NOZORDER | SWP_SHOWWINDOW);
			m_CB_AllConfirm.Invalidate();
			m_CB_Confirm.Invalidate();
			m_CB_Cancel.Invalidate();
		}
	}
}



// Broadcast function
static void BltKonfTableCf(void *popInstance, int ipDDXType,void *vpDataPointer, CString &ropInstanceName)
{
	if (vpDataPointer == NULL) 
		return;

	BltKonflikteDlg *polDlg = (BltKonflikteDlg *)popInstance;
	KonfIdent *polIdNotify = (KonfIdent *)vpDataPointer;
	// is conflict a baggage-belt conflict?
	if (!ogKonflikte.IsBltKonfliktType(polIdNotify->Type)) return;
	// choose funktion
 	if (ipDDXType == KONF_DELETE)
		polDlg->DdxDeleteKonf(*polIdNotify);
	if (ipDDXType == KONF_INSERT)
		polDlg->DdxInsertKonf(*polIdNotify);
	if (ipDDXType == KONF_CHANGE)
		polDlg->DdxChangeKonf(*polIdNotify);

}


void BltKonflikteDlg::DdxChangeKonf(const KonfIdent &rrpIdNotify)
{
	if(!bmIsActiv)
		return;
	
	DdxDeleteKonf(rrpIdNotify);

	DdxInsertKonf(rrpIdNotify);

/*
	const KONFDATA *prlData = NULL;
	BLTKONF_LINEDATA *prlLine;
	CCSPtrArray<TABLE_COLUMN> olColList;
	TABLE_COLUMN rlColumnData;
	// find intern table line
	int ilLineNo = FindLine(rrpIdNotify);

	if(ilLineNo >= 0)
	{
		// get conflict
		prlData = ogKonflikte.GetKonflikt(rrpIdNotify);

		if(prlData != NULL)
		{
			//  alter intern table line
			prlLine = &omLines[ilLineNo];
			prlLine->Text = prlData->Text;
			prlLine->TimeOfConflict = prlData->TimeOfConflict;

			// alter table line
			rlColumnData.Text = omLines[ilLineNo].Text;
			olColList.NewAt(olColList.GetSize(), rlColumnData);
			pomTable->ChangeTextLine(ilLineNo, &olColList , &omLines[ilLineNo]);
			olColList.DeleteAll();
		}
	}
*/
}


void BltKonflikteDlg::DdxInsertKonf(const KonfIdent &rrpIdNotify)
{
	if(!bmIsActiv)
		return;

	const KONFDATA *prlData = NULL;
	BLTKONF_LINEDATA rlLine;
	CCSPtrArray<TABLE_COLUMN> olColList;
	TABLE_COLUMN rlColumnData;
	int ilLineNo;

	// get new conflict
	prlData = ogKonflikte.GetKonflikt(rrpIdNotify);

	if(prlData != NULL)
	{

		if(prlData->SetBy != 0)
		{
			if (ogCfgData.CheckKonfliktForDisplay(prlData->KonfId.Type,omUsedFor) == true)
			{
				if((bmAttention && !prlData->Confirmed) || !bmAttention)
				{
					// create new intern table line
					rlLine.KonfId = rrpIdNotify;
					rlLine.Text = prlData->Text;
					rlLine.TimeOfConflict = prlData->TimeOfConflict;
					ilLineNo = CreateLine(rlLine);

					// create new line in table
					rlColumnData.Text = rlLine.Text;
					olColList.NewAt(olColList.GetSize(), rlColumnData);
					pomTable->InsertTextLine(ilLineNo, olColList , &omLines[ilLineNo]);
					olColList.DeleteAll();

				}
			}
		}
	}
}


void BltKonflikteDlg::DdxDeleteKonf(const KonfIdent &rrpIdNotify)
{
	if(!bmIsActiv)
		return;

	// find intern table line
	int ilLineNo = FindLine(rrpIdNotify);

	if(ilLineNo >= 0)
	{
		// delete intern table line
		omLines.DeleteAt(ilLineNo);
		// delete table line
		pomTable->DeleteTextLine(ilLineNo);
	}
}


int BltKonflikteDlg::FindLine(const KonfIdent &rrpKonfId) const
{
	// Scan all intern table lines
	for(int i = omLines.GetSize() - 1; i >= 0; i--)
	{
		if(omLines[i].KonfId == rrpKonfId)
			break;
	}
	return i;
}



LONG BltKonflikteDlg::OnTableLButtonDblclk( UINT wParam, LPARAM lParam)
{
	const BLTKONF_LINEDATA *prlLine = (BLTKONF_LINEDATA*)pomTable->GetTextLineData(wParam);

	if(prlLine == NULL)
		return 0L;

	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));

	// Activate 'Daily'- or 'Season'-Rotation-Mask
	const DIAFLIGHTDATA *prlFlight = ogPosDiaFlightData.GetFlightByUrno(prlLine->KonfId.FUrno);

	if(prlFlight != NULL)
	{
					
		CTime olTimeTmp;
		CString olAdid;
		if(strcmp(prlFlight->Des3, pcgHome) == 0)
		{
			olTimeTmp = prlFlight->Tifa;
			olAdid = "A";
		}
		else
		{
			olTimeTmp = prlFlight->Tifd;
			olAdid = "D";
		}

		if (olTimeTmp - CTimeSpan(2,0,0,0) < CTime::GetCurrentTime())
		{	
 			pogRotationDlg->NewData(this, prlFlight->Rkey, prlFlight->Urno, olAdid, bgGatPosLocal);
		}
		else
		{
			pogSeasonDlg->NewData(this, prlFlight->Rkey, prlFlight->Urno, DLG_CHANGE, bgGatPosLocal);
		}
	}
	
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	return 0L;
}


LONG BltKonflikteDlg::OnTableReturnPressed( UINT wParam, LPARAM lParam)
{
	OnTableLButtonDblclk(wParam, lParam); 
	return 0L;
}

LONG BltKonflikteDlg::OnTableSelChanged( UINT wParam, LPARAM lParam)
{
    int ilLineNo = pomTable->pomListBox->GetCurSel();    
	
	BLTKONF_LINEDATA *prlTableLine = NULL;
	prlTableLine = (BLTKONF_LINEDATA *)pomTable->GetTextLineData(ilLineNo);
	if (prlTableLine->KonfId.FUrno != 0)
		CheckPostFlightPosDia(prlTableLine->KonfId.FUrno,this);
	else
		CheckPostFlightPosDia(0,this);
	
	return 0L;
}

void BltKonflikteDlg::OnPrint() 
{
	// TODO: Add your control notification handler code here
	CString olFooter1,olFooter2;
	CString olTableName;
	olTableName = GetString(IDS_STRING2152);//"Attentions";
		
	int ilOrientation = PRINT_LANDSCAPE;//  PRINT_PORTRAET  PRINT_LANDSCAPE
	pomPrint = new CCSPrint((CWnd*)this,ilOrientation,45);// FirstLine(P=200,L=200),LeftOffset(P=200,L=50)
	pomPrint->imMaxLines = 38;  // (P=57,L=38)

	if (pomPrint != NULL)
	{
		if (pomPrint->InitializePrinter(ilOrientation) == TRUE)
		{
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			pomPrint->imLineNo = pomPrint->imMaxLines + 1;
			DOCINFO	rlDocInfo;
			memset(&rlDocInfo, 0, sizeof(DOCINFO));
			rlDocInfo.cbSize = sizeof( DOCINFO );
			rlDocInfo.lpszDocName = olTableName;	
			//rlDocInfo.lpszDocName = TABLENAME;	
			pomPrint->omCdc.StartDoc( &rlDocInfo );
			pomPrint->imPageNo = 0;
			int ilLines = omLines.GetSize();  

			olFooter1.Format("%s -   %s", olTableName, (CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")) );
//			GetHeader(); 

			if (ilLines == 0)
				PrintTableHeader(); // Drucken leerer Tabelle
			for(int i = 0; i < ilLines; i++ ) 
			{
				if(pomPrint->imLineNo >= pomPrint->imMaxLines)
				{
					if(pomPrint->imPageNo > 0)
					{
						olFooter2.Format(GetString(IDS_STRING1198),pomPrint->imPageNo);
						pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
						pomPrint->omCdc.EndPage();
					}
					PrintTableHeader();
				}
				if(pomPrint->imLineNo == (pomPrint->imMaxLines-1) || i == (ilLines-1))
				{
					PrintTableLine(&omLines[i],true);
				}
				else
				{
					PrintTableLine(&omLines[i],false);
				}
			}
			olFooter2.Format(GetString(IDS_STRING1198),pomPrint->imPageNo);
			pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
			pomPrint->omCdc.EndPage();
			pomPrint->omCdc.EndDoc();
		}

//		omPrintHeadHeaderArray.DeleteAll();
		SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
		delete pomPrint;
		pomPrint = NULL;
	}
	
	
}

bool BltKonflikteDlg::PrintTableLine(BLTKONF_LINEDATA *prpLine,bool bpLastLine)
{
	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;
	double dgCCSPrintFactor = 2.7 ;
	rlElement.pFont      = &pomPrint->ogCourierNew_Regular_8;
	int ilSize = 1;//omPrintHeadHeaderArray.GetSize();
	for(int i=0;i<ilSize;i++)
	{
		rlElement.Length = (int)(1000*dgCCSPrintFactor);
		if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
		rlElement.Alignment  = PRINT_LEFT;
		rlElement.FrameTop   = PRINT_FRAMETHIN;
		if(bpLastLine == true)
		{
			rlElement.FrameBottom = PRINT_FRAMETHIN;
		}
		else
		{
			rlElement.FrameBottom = PRINT_NOFRAME;
		}
//		rlElement.FrameLeft  = PRINT_NOFRAME;
//		rlElement.FrameRight = PRINT_NOFRAME;
			switch(i)
			{
				case 0:
				{
					rlElement.FrameLeft  = PRINT_FRAMETHIN;
					rlElement.Text		= prpLine->Text;
					rlElement.FrameRight = PRINT_FRAMETHIN;
				}
				break;
			}


		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();
	return true;
}

bool BltKonflikteDlg::PrintTableHeader()
{
	CString olBuffer;
	olBuffer = GetString(IDS_STRING2152);//"Attentions";

	CString olTableName;
	olTableName.Format("%s -   %s", olBuffer, (CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")) );

	pomPrint->omCdc.StartPage();
	pomPrint->imPageNo++;
	pomPrint->imLineNo = 0;
	double dgCCSPrintFactor = 2.7 ;
	pomPrint->PrintUIFHeader(olTableName, "", pomPrint->imFirstLine-10);

	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;

	rlElement.Alignment   = PRINT_LEFT;
	rlElement.FrameLeft   = PRINT_NOFRAME;
	rlElement.FrameRight  = PRINT_NOFRAME;
	rlElement.FrameTop    = PRINT_NOFRAME;
	rlElement.FrameBottom = PRINT_FRAMEMEDIUM;
	rlElement.pFont       = &pomPrint->ogCourierNew_Bold_8;

	int ilSize = 1;//omPrintHeadHeaderArray.GetSize();

	for(int i=0;i<ilSize;i++)
	{
			rlElement.Length = (int)(1000*dgCCSPrintFactor);
			if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
			rlElement.Text   = "FLIGHT               CONFLICT";//omPrintHeadHeaderArray[i].Text;
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();


	return true;

}


void BltKonflikteDlg::OnExcel() 
{
	// TODO: Add your control notification handler code here
	
	char pclConfigPath[256];
	char pclExcelPath[256];
	char pclTrenner[64];
	

    if (getenv("CEDA") == NULL)
        strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
    else
        strcpy(pclConfigPath, getenv("CEDA"));

    GetPrivateProfileString(ogAppName, "Excel", "DEFAULT",
        pclExcelPath, sizeof pclExcelPath, pclConfigPath);


    GetPrivateProfileString(ogAppName, "ExcelSeparator", ";",
        pclTrenner, sizeof pclTrenner, pclConfigPath);


	if(!strcmp(pclExcelPath, "DEFAULT"))
		CFPMSApp::MyTopmostMessageBox(this,GetString(IDS_STRING977), GetString(ST_FEHLER), MB_ICONERROR);


	CreateExcelFile(pclTrenner);

	bool test = true; //only for testing error
	if (omFileName.IsEmpty() || omFileName.GetLength() > 255 || !test)
	{
		if (omFileName.IsEmpty())
		{
			CString mess = "Filename is empty !" + omFileName;
			CFPMSApp::MyTopmostMessageBox(this, mess, GetString(ST_FEHLER), MB_ICONERROR);
			return;
		}
		if (omFileName.GetLength() > 255 )
		{
			CString mess = "Length of the Filename > 255: " + omFileName;
			CFPMSApp::MyTopmostMessageBox(this, mess, GetString(ST_FEHLER), MB_ICONERROR);
			return;
		}

		CString mess = "Filename invalid: " + omFileName;
		CFPMSApp::MyTopmostMessageBox(this, mess, GetString(ST_FEHLER), MB_ICONERROR);
		return;
	}

	char pclTmp[256];
	strcpy(pclTmp, omFileName); 

   /* Set up parameters to be sent: */
	char *args[4];
	args[0] = "child";
	args[1] = pclTmp;
	args[2] = NULL;
	args[3] = NULL;

	_spawnv( _P_NOWAIT , pclExcelPath, args );
	
}


bool BltKonflikteDlg::CreateExcelFile(CString opTrenner)
{
	ofstream of;

	CString olBuffer;
	olBuffer = GetString(IDS_STRING2152);//"Attentions";

	CString olTableName;
	olTableName.Format("%s -   %s", olBuffer, (CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")) );


	CString olFileName = olTableName;
	olFileName.Remove('*');
	olFileName.Remove('.');
	olFileName.Remove(':');
	olFileName.Remove('/');
	olFileName.Replace(" ", "_");
	
	char pHeader[256];
	strcpy (pHeader, CCSLog::GetTmpPath());
	CString path = pHeader;
	omFileName =  path + "\\" + olFileName + ".csv";

	of.open( omFileName, ios::out);
	of.setf(ios::left, ios::adjustfield);


	of  << setw(1) << olTableName
		<< endl;

	of	<< setw(1) << "FLIGHT               CONFLICT"
		<< endl;


	int ilCountLines = omLines.GetSize();

	// zeilen iterieren
	for(int ilLines = 0; ilLines < ilCountLines; ilLines++)
	{
		BLTKONF_LINEDATA rlLine = omLines[ilLines];

		of << setw(1) << rlLine.Text
		   << endl;
	}

	// stream schliessen
	of.close();

	return true;

}

