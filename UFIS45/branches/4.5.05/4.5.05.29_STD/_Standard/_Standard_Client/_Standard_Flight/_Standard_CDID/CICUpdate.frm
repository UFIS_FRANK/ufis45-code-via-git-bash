VERSION 5.00
Begin VB.Form CICUpdate 
   Caption         =   "Check-In Counter Update"
   ClientHeight    =   9240
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   13725
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   ScaleHeight     =   9240
   ScaleWidth      =   13725
   StartUpPosition =   3  'Windows Default
   Begin VB.TextBox txtDate 
      DataField       =   "NULL"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   0
      Left            =   3120
      MaxLength       =   10
      TabIndex        =   4
      Top             =   1080
      Width           =   1215
   End
   Begin VB.TextBox txtTime 
      DataField       =   "NULL"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   0
      Left            =   4320
      MaxLength       =   5
      TabIndex        =   5
      Top             =   1080
      Width           =   1215
   End
   Begin VB.TextBox txtDate 
      DataField       =   "NULL"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   1
      Left            =   5880
      MaxLength       =   10
      TabIndex        =   7
      Top             =   1080
      Width           =   1215
   End
   Begin VB.TextBox txtTime 
      DataField       =   "NULL"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   1
      Left            =   7080
      MaxLength       =   5
      TabIndex        =   8
      Top             =   1080
      Width           =   1215
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "&OK"
      Default         =   -1  'True
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   3120
      Style           =   1  'Graphical
      TabIndex        =   42
      Top             =   7200
      Width           =   1215
   End
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "&Cancel"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   5160
      TabIndex        =   43
      Top             =   7200
      Width           =   1215
   End
   Begin VB.CheckBox chkDaily 
      Caption         =   "Daily"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   4680
      TabIndex        =   11
      Top             =   1440
      Width           =   975
   End
   Begin VB.TextBox txtDays 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   3120
      MaxLength       =   7
      TabIndex        =   10
      Text            =   "txtDays"
      Top             =   1440
      Width           =   1455
   End
   Begin VB.ComboBox cboCodeShare 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   3120
      Sorted          =   -1  'True
      Style           =   2  'Dropdown List
      TabIndex        =   2
      Top             =   720
      Width           =   2055
   End
   Begin VB.PictureBox picCheckInCounters 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   4455
      Left            =   360
      ScaleHeight     =   4455
      ScaleWidth      =   9135
      TabIndex        =   20
      TabStop         =   0   'False
      Top             =   2520
      Width           =   9135
      Begin VB.CheckBox chkDelete 
         BackColor       =   &H80000015&
         Caption         =   "&Delete"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000005&
         Height          =   360
         Left            =   7455
         Style           =   1  'Graphical
         TabIndex        =   23
         TabStop         =   0   'False
         Top             =   45
         Width           =   1170
      End
      Begin VB.CheckBox chkAdd 
         BackColor       =   &H80000015&
         Caption         =   "&Add"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000005&
         Height          =   360
         Left            =   6495
         Style           =   1  'Graphical
         TabIndex        =   22
         TabStop         =   0   'False
         Top             =   45
         Width           =   960
      End
      Begin VB.CheckBox chkAll 
         BackColor       =   &H80000010&
         Caption         =   "Check1"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   320
         Left            =   360
         TabIndex        =   24
         TabStop         =   0   'False
         Top             =   430
         Width           =   180
      End
      Begin VB.VScrollBar vsbDetails 
         Height          =   4350
         Left            =   8640
         Max             =   0
         TabIndex        =   44
         TabStop         =   0   'False
         Top             =   30
         Width           =   255
      End
      Begin VB.PictureBox picHelper 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   3605
         Left            =   255
         ScaleHeight     =   3600
         ScaleWidth      =   8325
         TabIndex        =   31
         TabStop         =   0   'False
         Top             =   770
         Width           =   8325
         Begin VB.PictureBox picDetails 
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   2280
            Left            =   120
            ScaleHeight     =   2280
            ScaleWidth      =   8175
            TabIndex        =   32
            TabStop         =   0   'False
            Top             =   0
            Width           =   8175
            Begin VB.TextBox txtTer 
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   360
               Index           =   0
               Left            =   1560
               TabIndex        =   36
               Text            =   "txtTer"
               Top             =   0
               Width           =   360
            End
            Begin VB.TextBox txtCtrClose 
               DataField       =   "NULL"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   360
               Index           =   0
               Left            =   3480
               TabIndex        =   38
               Text            =   "txtCtrClose"
               Top             =   0
               Width           =   1575
            End
            Begin VB.CommandButton cmdCtrHelp 
               BackColor       =   &H00FFFFFF&
               Caption         =   "..."
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   340
               Index           =   0
               Left            =   240
               TabIndex        =   34
               TabStop         =   0   'False
               Tag             =   "CIC0"
               Top             =   0
               Width           =   360
            End
            Begin VB.TextBox txtCtrNo 
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   360
               Index           =   0
               Left            =   600
               TabIndex        =   35
               Text            =   "txtCtrNo"
               Top             =   0
               Width           =   975
            End
            Begin VB.TextBox txtActOpen 
               DataField       =   "NULL"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   360
               Index           =   0
               Left            =   5040
               TabIndex        =   39
               Text            =   "txtActOpen"
               Top             =   0
               Width           =   1575
            End
            Begin VB.TextBox txtActClose 
               DataField       =   "NULL"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   360
               Index           =   0
               Left            =   6600
               TabIndex        =   40
               Text            =   "txtActClose"
               Top             =   0
               Width           =   1575
            End
            Begin VB.TextBox txtCtrOpen 
               DataField       =   "NULL"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   360
               Index           =   0
               Left            =   1920
               TabIndex        =   37
               Text            =   "txtCtrOpen"
               Top             =   0
               Width           =   1575
            End
            Begin VB.CheckBox chkSel 
               BackColor       =   &H00FFFFFF&
               Caption         =   "Check1"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   330
               Index           =   0
               Left            =   0
               TabIndex        =   33
               TabStop         =   0   'False
               Top             =   0
               Width           =   180
            End
         End
      End
      Begin VB.Shape shpCaptions 
         BackColor       =   &H80000010&
         BackStyle       =   1  'Opaque
         BorderColor     =   &H00FFFFFF&
         Height          =   360
         Index           =   4
         Left            =   255
         Top             =   405
         Width           =   375
      End
      Begin VB.Label lblCaptions 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         Caption         =   "Ctr No."
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   0
         Left            =   975
         TabIndex        =   25
         Top             =   465
         Width           =   960
      End
      Begin VB.Label lblCaptions 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         Caption         =   "Ter"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   1
         Left            =   1935
         TabIndex        =   26
         Top             =   480
         Width           =   360
      End
      Begin VB.Label lblCaptions 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         Caption         =   "Planned Open"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   2
         Left            =   2280
         TabIndex        =   27
         Top             =   480
         Width           =   1600
      End
      Begin VB.Label lblCaptions 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         Caption         =   "Planned Close"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   3
         Left            =   3840
         TabIndex        =   28
         Top             =   480
         Width           =   1575
      End
      Begin VB.Label lblLabels 
         BackStyle       =   0  'Transparent
         Caption         =   "Check-In Counters:"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000005&
         Height          =   255
         Index           =   7
         Left            =   300
         TabIndex        =   21
         Top             =   120
         Width           =   2175
      End
      Begin VB.Shape shpBox 
         BorderColor     =   &H80000010&
         Height          =   4350
         Left            =   240
         Top             =   30
         Width           =   8400
      End
      Begin VB.Shape shpCaptions 
         BackColor       =   &H80000010&
         BackStyle       =   1  'Opaque
         BorderColor     =   &H00FFFFFF&
         Height          =   360
         Index           =   8
         Left            =   8520
         Top             =   405
         Width           =   105
      End
      Begin VB.Label lblCaptions 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         Caption         =   "Actual Open"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   4
         Left            =   5400
         TabIndex        =   29
         Top             =   480
         Width           =   1575
      End
      Begin VB.Label lblCaptions 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         Caption         =   "Actual Close"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   5
         Left            =   6960
         TabIndex        =   30
         Top             =   480
         Width           =   1575
      End
      Begin VB.Shape shpCaptions 
         BackColor       =   &H80000010&
         BackStyle       =   1  'Opaque
         BorderColor     =   &H00FFFFFF&
         Height          =   360
         Index           =   11
         Left            =   6975
         Top             =   405
         Width           =   1575
      End
      Begin VB.Shape shpCaptions 
         BackColor       =   &H80000010&
         BackStyle       =   1  'Opaque
         BorderColor     =   &H00FFFFFF&
         Height          =   360
         Index           =   10
         Left            =   5415
         Top             =   405
         Width           =   1575
      End
      Begin VB.Shape shpCaptions 
         BackColor       =   &H80000010&
         BackStyle       =   1  'Opaque
         BorderColor     =   &H00FFFFFF&
         Height          =   360
         Index           =   0
         Left            =   975
         Top             =   405
         Width           =   960
      End
      Begin VB.Shape shpCaptions 
         BackColor       =   &H80000010&
         BackStyle       =   1  'Opaque
         BorderColor     =   &H00FFFFFF&
         Height          =   360
         Index           =   1
         Left            =   1935
         Top             =   405
         Width           =   360
      End
      Begin VB.Shape shpCaptions 
         BackColor       =   &H80000010&
         BackStyle       =   1  'Opaque
         BorderColor     =   &H00FFFFFF&
         Height          =   360
         Index           =   2
         Left            =   2280
         Top             =   405
         Width           =   1600
      End
      Begin VB.Shape shpCaptions 
         BackColor       =   &H80000010&
         BackStyle       =   1  'Opaque
         BorderColor     =   &H00FFFFFF&
         Height          =   360
         Index           =   3
         Left            =   3855
         Top             =   405
         Width           =   1575
      End
      Begin VB.Shape shpCaptions 
         BackColor       =   &H80000010&
         BackStyle       =   1  'Opaque
         BorderColor     =   &H00FFFFFF&
         Height          =   360
         Index           =   9
         Left            =   600
         Top             =   405
         Width           =   390
      End
      Begin VB.Shape shpCaptions 
         BackColor       =   &H80000015&
         BackStyle       =   1  'Opaque
         BorderColor     =   &H00FFFFFF&
         Height          =   360
         Index           =   5
         Left            =   255
         Top             =   45
         Width           =   6240
      End
   End
   Begin VB.TextBox txtCSLeg 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   3120
      MaxLength       =   30
      TabIndex        =   13
      Text            =   "txtCSLeg"
      Top             =   1800
      Width           =   3255
   End
   Begin VB.TextBox txtGate 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   1
      Left            =   4920
      MaxLength       =   5
      TabIndex        =   18
      Text            =   "txtGate"
      Top             =   2160
      Width           =   1455
   End
   Begin VB.TextBox txtGate 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   0
      Left            =   3120
      MaxLength       =   5
      TabIndex        =   16
      Text            =   "txtGate"
      Top             =   2160
      Width           =   1455
   End
   Begin VB.CommandButton cmdHelp 
      BackColor       =   &H00FFFFFF&
      Caption         =   "..."
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   0
      Left            =   6360
      TabIndex        =   14
      TabStop         =   0   'False
      Tag             =   "CSLEG"
      Top             =   1800
      Width           =   375
   End
   Begin VB.CommandButton cmdHelp 
      BackColor       =   &H00FFFFFF&
      Caption         =   "..."
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   1
      Left            =   4560
      TabIndex        =   17
      TabStop         =   0   'False
      Tag             =   "GATEBELT1"
      Top             =   2160
      Width           =   375
   End
   Begin VB.CommandButton cmdHelp 
      BackColor       =   &H00FFFFFF&
      Caption         =   "..."
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   2
      Left            =   6360
      TabIndex        =   19
      TabStop         =   0   'False
      Tag             =   "GATEBELT2"
      Top             =   2160
      Width           =   375
   End
   Begin VB.Timer tmrClean 
      Enabled         =   0   'False
      Interval        =   1
      Left            =   7920
      Top             =   0
   End
   Begin VB.Timer tmrSuccess 
      Enabled         =   0   'False
      Interval        =   3000
      Left            =   7440
      Top             =   0
   End
   Begin VB.Label lblDeleted 
      BackColor       =   &H00FFFFFF&
      BackStyle       =   0  'Transparent
      Caption         =   "This code share flight has been deleted."
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   375
      Left            =   5280
      TabIndex        =   45
      Top             =   720
      Visible         =   0   'False
      Width           =   3375
   End
   Begin VB.Label lblLabels 
      BackStyle       =   0  'Transparent
      Caption         =   "Period from"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   2
      Left            =   1200
      TabIndex        =   3
      Top             =   1080
      Width           =   1215
   End
   Begin VB.Label lblLabels 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "to"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   3
      Left            =   5520
      TabIndex        =   6
      Top             =   1080
      Width           =   375
   End
   Begin VB.Label lblLabels 
      BackStyle       =   0  'Transparent
      Caption         =   "Days"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   4
      Left            =   1200
      TabIndex        =   9
      Top             =   1440
      Width           =   1215
   End
   Begin VB.Label lblLabels 
      BackStyle       =   0  'Transparent
      Caption         =   "Code Share Flight"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   1
      Left            =   1200
      TabIndex        =   1
      Top             =   720
      Width           =   1935
   End
   Begin VB.Label lblLabels 
      BackStyle       =   0  'Transparent
      Caption         =   "Routing"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   5
      Left            =   1200
      TabIndex        =   12
      Top             =   1800
      Width           =   1575
   End
   Begin VB.Label lblLabels 
      BackStyle       =   0  'Transparent
      Caption         =   "Gate"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   6
      Left            =   1200
      TabIndex        =   15
      Top             =   2160
      Width           =   1215
   End
   Begin VB.Label lblMsg 
      BackColor       =   &H00FFFFFF&
      BackStyle       =   0  'Transparent
      Caption         =   "Message"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   375
      Left            =   1200
      TabIndex        =   0
      Top             =   360
      Width           =   3375
   End
   Begin VB.Label lblSuccess 
      BackColor       =   &H00FFFFFF&
      BackStyle       =   0  'Transparent
      Caption         =   "The flight record has been successfully updated."
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   375
      Left            =   600
      TabIndex        =   41
      Top             =   6960
      Visible         =   0   'False
      Width           =   5895
   End
End
Attribute VB_Name = "CICUpdate"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public FormType As enmFormType

Private IsUserClickOnDailyCheckBox As Boolean
Private ItemSelCount As Integer
Private IsChangesFromCode As Boolean

Private FlightUrno As String

Private CurrentCSIndex As Integer

Private dtmDate(1) As Date

Private DoNotRefresh As Boolean

Private rsAft As ADODB.Recordset
Private rsAirportCodes As ADODB.Recordset
Private rsGates As ADODB.Recordset
Private rsBelts As ADODB.Recordset
Private rsCICounters As ADODB.Recordset
Private rsCSD As ADODB.Recordset
Private rsCCA As ADODB.Recordset
Private rsCSC As ADODB.Recordset

Public Sub RefreshDisplay(ByVal CedaCmd As String, ByVal TableName As String, ByVal Urno As String, Optional ByVal ChangedFields As String)
    Dim strADID As String
    Dim blnArr As Boolean
    Dim blnRefreshNeeded As Boolean

    Select Case TableName
        Case "AFTTAB"
            If IsRefreshNeeded(TableName & "_HDR", blnArr, ChangedFields) Then
                IsChangesFromCode = True
                Call PopulateCodeShareList(rsAft, False)
                IsChangesFromCode = False
            End If
            If CurrentCSIndex = 0 Then
                If Urno = FlightUrno Then
                    strADID = rsAft.Fields("ADID").Value
                    blnArr = (strADID = "A")
                    
                    If FormType = ftyArrival Then
                        If Not blnArr Then Exit Sub
                    Else
                        If blnArr Then Exit Sub
                    End If
                
                    If ChangedFields <> "" Then
                        If Not IsRefreshNeeded(TableName, blnArr, ChangedFields) Then Exit Sub
                    End If
                    
                    IsChangesFromCode = True
                    Call FillInData(rsAft, False)
                    IsChangesFromCode = False
                End If
            End If
        Case "CCATAB"
            If CurrentCSIndex = 0 Then
                If CedaCmd = "DRT" Then
                    blnRefreshNeeded = (InStr(Urno, FlightUrno) > 0)
                Else
                    Urno = rsCCA.Fields("FLNU").Value
                    If Urno = FlightUrno Then
                        blnRefreshNeeded = True
                        If CedaCmd = "URT" And ChangedFields <> "" Then
                            blnRefreshNeeded = IsRefreshNeeded(TableName, blnArr, ChangedFields)
                        End If
                    End If
                End If
                If blnRefreshNeeded Then
                    IsChangesFromCode = True
                    Call AddCICDetails(rsCCA, True)
                    IsChangesFromCode = False
                End If
            End If
        Case "CSDTAB"
            If CurrentCSIndex > 0 Then
                If CedaCmd = "DRT" Then
                    blnRefreshNeeded = (InStr(Urno, FlightUrno & "," & Trim(cboCodeShare.Text)) > 0)
                    If blnRefreshNeeded Then
                        IsChangesFromCode = True
                        Call ClearAllFields(True)
                        IsChangesFromCode = False
                    End If
                Else
                    Urno = rsCSD.Fields("RURN").Value
                    If Urno = FlightUrno Then
                        blnRefreshNeeded = True
                        If Trim(cboCodeShare.Text) = Trim(NVL(rsCSD.Fields("JFNO").Value, "")) Then
                            If CedaCmd = "URT" And ChangedFields <> "" Then
                                blnRefreshNeeded = IsRefreshNeeded(TableName, blnArr, ChangedFields)
                            End If
                        End If
                    End If
                    If blnRefreshNeeded Then
                        IsChangesFromCode = True
                        Call FillInData(rsCSD, False)
                        IsChangesFromCode = False
                    End If
                End If
            End If
        Case "CSCTAB"
            If CurrentCSIndex > 0 Then
                If CedaCmd = "DRT" Then
                    blnRefreshNeeded = (InStr(Urno, FlightUrno & "," & Trim(cboCodeShare.Text)) > 0)
                Else
                    Urno = rsCSC.Fields("RURN").Value
                    If Urno = FlightUrno Then
                        blnRefreshNeeded = True
                        If Trim(cboCodeShare.Text) = Trim(NVL(rsCSC.Fields("JFNO").Value, "")) Then
                            If CedaCmd = "URT" And ChangedFields <> "" Then
                                blnRefreshNeeded = IsRefreshNeeded(TableName, blnArr, ChangedFields)
                            End If
                        End If
                    End If
                End If
                If blnRefreshNeeded Then
                    IsChangesFromCode = True
                    Call AddCICDetails(rsCSC, False, Trim(cboCodeShare.Text))
                    IsChangesFromCode = False
                End If
            End If
    End Select
End Sub

Private Function IsRefreshNeeded(ByVal TableName As String, ByVal IsArrival As Boolean, ByVal ChangedFields As String) As Boolean
    Const AFT_HDR_FIELD_LIST = "JFNO,JCNT"
    Const AFT_ARR_FIELD_LIST = "VIAL,VIAN,ORG3,GTA1,GTA2," & _
        "BLT1,BLT2,B1BS,B1ES,B1BA,B1EA,B2BS,B2ES,B2BA,B2EA"
    Const AFT_DEP_FIELD_LIST = "VIAL,VIAN,DES3,GTD1,GTD2"
    Const CCA_FIELD_LIST = "CKIC,CKIT,CKBS,CKES,CKBA,CKEA"
    Const CSD_ARR_FIELD_LIST = "VIAL,ORG3,GTA1,GTA2" & _
        "BLT1,BLT2,B1BS,B1ES,B1BA,B1EA,B2BS,B2ES,B2BA,B2EA"
    Const CSD_DEP_FIELD_LIST = "VIAL,DES3,GTD1,GTD2"
    Const CSC_FIELD_LIST = "CKIC,CKTR,CKBS,CKES,CKBA,CKEA"
    
    Dim strFields As String
    
    Select Case TableName
        Case "AFTTAB_HDR"
            strFields = AFT_HDR_FIELD_LIST
        Case "AFTTAB"
            If IsArrival Then
                strFields = AFT_ARR_FIELD_LIST
            Else
                strFields = AFT_DEP_FIELD_LIST
            End If
        Case "CCATAB"
            strFields = CCA_FIELD_LIST
        Case "CSDTAB"
            If IsArrival Then
                strFields = CSD_ARR_FIELD_LIST
            Else
                strFields = CSD_DEP_FIELD_LIST
            End If
        Case "CSCTAB"
            strFields = CSC_FIELD_LIST
    End Select
    
    IsRefreshNeeded = IsInList(ChangedFields, strFields)
End Function

Public Sub LoadData(ByVal strUrno As String, Optional ByVal PopulateCSList As Boolean = True)
    Dim blnFLightFound As Boolean
    Dim strStdFld As String
        
    blnFLightFound = (strUrno <> "")
    If blnFLightFound Then
        rsAft.Find "URNO = " & strUrno, , , 1
        blnFLightFound = Not (rsAft.EOF)
    End If
    If blnFLightFound Then
        If FormType = ftyArrival Then
            blnFLightFound = (RTrim(rsAft.Fields("ADID").Value) = "A")
            strStdFld = "STOA"
        Else
            blnFLightFound = (RTrim(rsAft.Fields("ADID").Value) <> "A")
            strStdFld = "STOD"
        End If
    End If
    
    IsChangesFromCode = True
    If blnFLightFound Then
        FlightUrno = strUrno
        
        txtDate(0).Text = Format(rsAft.Fields(strStdFld).Value, "dd-mm-yyyy")
        txtTime(0).Text = "00:00"
        txtDate(1).Text = txtDate(0).Text
        txtTime(1).Text = "23:59"
        chkDaily.Value = vbChecked
        
        If PopulateCSList Then
            Call PopulateCodeShareList(rsAft)
        Else
            Call FillInData(rsAft)
        End If
        Call EnableAllControls(True)
        lblMsg.Caption = ""
    Else
        FlightUrno = ""
        
        Call ClearAllFields
        Call EnableAllControls(False)
        lblMsg.Caption = "Flight not found!"
    End If
    IsChangesFromCode = False
    
    cmdOK.BackColor = vbButtonFace
End Sub

Private Sub ClearAllFields(Optional ByVal ClearDataOnly As Boolean = False)
    Dim i As Integer
    
    If Not ClearDataOnly Then
        cboCodeShare.Clear
        cboCodeShare.Tag = ""
        txtDate(0).Text = Format(Date, "dd-mm-yyyy")
        txtTime(0).Text = "00:00"
        txtDate(1).Text = txtDate(0).Text
        txtTime(1).Text = "23:59"
        txtDays.Text = ""
        chkDaily.Value = vbChecked
    End If
    
    txtCSLeg.Text = ""
    txtGate(0).Text = ""
    txtGate(1).Text = ""
    
    'reset tab
    ItemSelCount = 0
    For i = 0 To txtCtrNo.UBound
        Call DeleteDetailItems(i)
    Next i
    
    If Not ClearDataOnly Then
        CurrentCSIndex = -1
    End If
    
    Call HandleScrollbar
End Sub

Private Sub EnableAllControls(ByVal Enabled As Boolean)
    Dim i As Integer
    
    cboCodeShare.Enabled = Enabled
    txtDate(0).Enabled = Enabled
    txtTime(0).Enabled = Enabled
    txtDate(1).Enabled = Enabled
    txtTime(1).Enabled = Enabled
    txtDays.Enabled = Enabled
    chkDaily.Enabled = Enabled
    txtCSLeg.Enabled = Enabled
    txtGate(0).Enabled = Enabled
    txtGate(1).Enabled = Enabled
    
    For i = 0 To cmdHelp.UBound
        cmdHelp(i).Enabled = Enabled
    Next i
    
    chkAdd.Enabled = Enabled
    chkDelete.Enabled = Enabled
    chkAll.Enabled = Enabled
    For i = 0 To txtCtrNo.UBound
        chkSel(i).Enabled = Enabled
        cmdCtrHelp(i).Enabled = Enabled
        txtCtrNo(i).Enabled = Enabled
        If FormType = ftyDeparture Then
            txtTer(i).Enabled = Enabled
        End If
        txtCtrOpen(i).Enabled = Enabled
        txtCtrClose(i).Enabled = Enabled
        txtActOpen(i).Enabled = Enabled
        txtActClose(i).Enabled = Enabled
    Next i
    vsbDetails.Enabled = Enabled
    
    cmdOK.Enabled = Enabled
    cmdCancel.Enabled = Enabled
End Sub

Private Function IsDetailEmpty(ByVal RowNum As Integer) As Boolean
    IsDetailEmpty = True
    
    If FormType = ftyDeparture Then
        IsDetailEmpty = (RowNum = 0) And _
            (Trim(txtTer(RowNum).Text) = "")
    End If
    
    IsDetailEmpty = IsDetailEmpty And _
        (Trim(txtCtrNo(RowNum).Text) = "" And _
        txtCtrOpen(RowNum).DataField = "NULL" And _
        txtCtrClose(RowNum).DataField = "NULL" And _
        txtActOpen(RowNum).DataField = "NULL" And _
        txtActClose(RowNum).DataField = "NULL")
End Function

Private Function ValidateUserInput() As Boolean
    Dim i As Integer, j As Integer
    Dim intErr As Integer
    Dim txtErr As TextBox
    Dim strMsg As String
    Dim blnFound As Boolean

    ValidateUserInput = True
    
    'code share flight cannot be empty
    If cboCodeShare.ListIndex < 0 Then
        MsgBoxEx "Please select code share flight!", vbInformation, Me.Caption, , , eCentreDialog
        cboCodeShare.SetFocus
        ValidateUserInput = False
        Exit Function
    End If
    
    'period must be correct
    For i = 0 To 1
        Select Case txtDate(i).DataField
            Case "ERROR"
                MsgBoxEx "Invalid date!", vbInformation, Me.Caption, , , eCentreDialog
                txtDate(i).SetFocus
                ValidateUserInput = False
                Exit Function
            Case "NULL"
                If i = 0 Then
                    MsgBoxEx "Invalid date!", vbInformation, Me.Caption, , , eCentreDialog
                    txtDate(i).SetFocus
                    ValidateUserInput = False
                    Exit Function
                Else
                    dtmDate(i) = DateSerial(CInt(Right(txtDate(0).DataField, 4)), _
                                        CInt(Mid(txtDate(0).DataField, 4, 2)), _
                                        CInt(Left(txtDate(0).DataField, 2)))
                End If
            Case Else
                dtmDate(i) = DateSerial(CInt(Right(txtDate(i).DataField, 4)), _
                                        CInt(Mid(txtDate(i).DataField, 4, 2)), _
                                        CInt(Left(txtDate(i).DataField, 2)))
        End Select
        
        Select Case txtTime(i).DataField
            Case "ERROR"
                MsgBoxEx "Invalid time!", vbInformation, Me.Caption, , , eCentreDialog
                txtTime(i).SetFocus
                ValidateUserInput = False
                Exit Function
            Case "NULL"
                If i = 1 Then
                    dtmDate(i) = dtmDate(i) + _
                                 TimeSerial(23, 59, 0)
                End If
            Case Else
                dtmDate(i) = dtmDate(i) + _
                             TimeSerial(CInt(Left(txtTime(i).DataField, 2)), _
                                        CInt(Right(txtTime(i).DataField, 2)), 0)
        End Select
        
        'Convert to UTC
        dtmDate(i) = LocalDateToUTC(dtmDate(i), UtcTimeDiff)
    Next i
    
    If dtmDate(0) > dtmDate(1) Then
        MsgBoxEx "Please enter valid period!", vbInformation, Me.Caption, , , eCentreDialog
        ValidateUserInput = False
        Exit Function
    End If
    
    'days cannot be empty
    If Trim(txtDays.Text) = "" Then
        MsgBoxEx "Please enter days!", vbInformation, Me.Caption, , , eCentreDialog
        txtDays.SetFocus
        ValidateUserInput = False
        Exit Function
    End If
    
    'code share legs must be valid
    If Not ValidateCSLeg(rsAirportCodes, txtCSLeg.Text) Then
        MsgBoxEx "Code share leg not valid!", vbInformation, Me.Caption, , , eCentreDialog
        txtCSLeg.SetFocus
        ValidateUserInput = False
        Exit Function
    End If
    
    'Gate must be in list
    For i = 0 To 1
        If Trim(txtGate(i).Text) <> "" Then
            rsGates.Find "GNAM = '" & txtGate(i).Text & "'", , , 1
            If rsGates.EOF Then
                MsgBoxEx "Gate not valid!", vbInformation, Me.Caption, , , eCentreDialog
                txtGate(i).SetFocus
                ValidateUserInput = False
                Exit Function
            End If
        End If
    Next i
    
    'check in counters/belts cannot be empty
    j = -1
    intErr = -1
    For i = 0 To txtCtrNo.UBound
        If Not IsDetailEmpty(i) Then
            If Trim(txtCtrNo(i).Text) = "" Then
                If j = -1 Then j = i
                If intErr = -1 Then intErr = 0
                
                txtCtrNo(i).BackColor = &HC0C0FF
            Else
                txtCtrNo(i).BackColor = vbWindowBackground
            End If
            If txtCtrOpen(i).DataField = "ERROR" Then
                If j = -1 Then j = i
                If intErr = -1 Then intErr = 1
            End If
            If txtCtrClose(i).DataField = "ERROR" Then
                If j = -1 Then j = i
                If intErr = -1 Then intErr = 2
            End If
            If txtActOpen(i).DataField = "ERROR" Then
                If j = -1 Then j = i
                If intErr = -1 Then intErr = 3
            End If
            If txtActClose(i).DataField = "ERROR" Then
                If j = -1 Then j = i
                If intErr = -1 Then intErr = 4
            End If
            
            'check in counters/belts must be in list
            If FormType = ftyArrival Then
                rsBelts.Find "BNAM = '" & txtCtrNo(i).Text & "'", , , 1
                blnFound = Not (rsBelts.EOF)
            Else
                rsCICounters.Filter = "CNAM = '" & txtCtrNo(i).Text & "'" & _
                    " AND TERM = '" & txtTer(i).Text & "'"
                blnFound = Not (rsCICounters.EOF)
                rsCICounters.Filter = adFilterNone
            End If
            If Not blnFound Then
                If j = -1 Then j = i
                If intErr = -1 Then intErr = 0
                
                txtCtrNo(i).BackColor = &HC0C0FF
            Else
                txtCtrNo(i).BackColor = vbWindowBackground
            End If
        End If
    Next i
    If j > -1 Then
        Select Case intErr
            Case 0
                Set txtErr = txtCtrNo(j)
            Case 1
                Set txtErr = txtCtrOpen(j)
            Case 2
                Set txtErr = txtCtrClose(j)
            Case 3
                Set txtErr = txtActOpen(j)
            Case 4
                Set txtErr = txtActClose(j)
        End Select
    
        If FormType = ftyArrival Then
            strMsg = "Belts"
        Else
            strMsg = "Check-in counters"
        End If
        strMsg = strMsg & " information not valid!"
        MsgBoxEx strMsg, vbInformation, Me.Caption, , , eCentreDialog
        txtErr.SetFocus
        ValidateUserInput = False
        Exit Function
    End If
End Function

Private Function SaveUpdates() As Boolean
    Dim strFlno As String
    Dim strStartDate As String
    Dim strEndDate As String
    Dim strWhere As String
    Dim strStdFld As String
    Dim strWeekdays As String
    Dim i As Integer
    Dim intDaysLength As Integer
    Dim intDay As Integer
    Dim oUFISRec As UFISRecordset
    Dim rsAftUpd As ADODB.Recordset
    Dim strUrnoList As String
    Dim strUrnos As String
    Dim strTable As String
    Dim strCommand As String
    Dim strFields As String
    Dim strDataTemp As String
    Dim strData As String
    Dim strDataUpd As String
    Dim strUrno As String
    Dim dtmSTDT As Date
    Dim dtmCtrOpen As Date
    Dim dtmCtrClose As Date
    Dim dtmActOpen As Date
    Dim dtmActClose As Date
    Dim strResult As String
    Dim lngInsertCount As Long
    Dim strACT3 As String
    Dim strFind As String
    Dim rsCS As ADODB.Recordset
    
    On Error GoTo ErrSave

    Screen.MousePointer = vbHourglass

    Call ShowFloatingMessage("Updating flight records..." & vbNewLine & "Please wait.", &H743E0C)

    SaveUpdates = False
    
    strFlno = RTrim(rsAft.Fields("FLNO").Value)
    
    strStartDate = Format(dtmDate(0), "yyyymmddhhmmss")
    strEndDate = Format(dtmDate(1), "yyyymmddhhmmss")
    
    strWhere = "WHERE FLNO = '" & strFlno & "'"
    If FormType = ftyArrival Then
        strStdFld = "STOA"
        
        strWhere = strWhere & " AND (ADID = 'A' AND " & _
            strStdFld & " BETWEEN '" & strStartDate & "' AND '" & strEndDate & "')"
    Else
        strStdFld = "STOD"
    
        strWhere = strWhere & " AND (ADID IN ('D','B') AND " & _
            strStdFld & " BETWEEN '" & strStartDate & "' AND '" & strEndDate & "')"
    End If
    
    If chkDaily.Value = vbUnchecked Then
        strWeekdays = ""
        
        intDaysLength = Len(txtDays.Text)
        For i = 1 To intDaysLength
            intDay = CInt(Mid(txtDays.Text, i, 1)) + 1
            If intDay = 8 Then intDay = 1
            
            strWeekdays = strWeekdays & ",'" & CStr(intDay) & "'"
        Next i
        If strWeekdays <> "" Then strWeekdays = Mid(strWeekdays, 2)
        
        strWhere = strWhere & " AND (TO_CHAR(TO_DATE(" & strStdFld & ", 'YYYYMMDDHH24MISS') " & _
            " + (" & CStr(UtcTimeDiff) & "/(60 * 24)), 'D') IN (" & strWeekdays & "))"
    End If

    Set oUFISRec = New UFISRecordset
    With oUFISRec
        Set .UfisServer = oUfisServer
        .ConvertSpecialChars = True
        .CheckOldVersionWhenConverting = True
        .CommandType = cmdGetFlightRecords
        .TableName = "AFTTAB"
        .ColumnList = "URNO,ADID," & strStdFld
        .ColumnTypeList = "N,C,D"
        .WhereClause = strWhere
        '.OrderByClause = strOrder
        .KeyColumn = "URNO"
        .PopulateKeyStringList = True
        
        Set rsAftUpd = .CreateRecordset
        
        strUrnoList = .KeyStringList
    End With
    
    If (rsAftUpd.BOF And rsAftUpd.EOF) Then
        Call HideFloatingMessage
        SaveUpdates = True
        Screen.MousePointer = vbDefault
        Exit Function
    End If

    If CurrentCSIndex = 0 Then 'MainFlight
        If FormType = ftyArrival Then
            strTable = "AFTTAB"
            strCommand = "UFR"
            strFields = ""
            strWhere = ""
            strUrnos = ""
            
            strDataTemp = txtGate(0).Text & "," & txtGate(1).Text & "," & _
                GetFullViaList(txtCSLeg.Text, rsAirportCodes, "A")
            strData = ""
            
            rsAftUpd.MoveFirst
            Do While Not rsAftUpd.EOF
                strUrno = CStr(rsAftUpd.Fields("URNO").Value)
                
                strFields = strFields & "GTA1,GTA2,ORG3,ORG4,VIA3,VIA4,VIAN,VIAL,BLT1,B1BS,B1ES,B1BA,B1EA,BLT2,B2BS,B2ES,B2BA,B2EA" & vbLf
                strWhere = strWhere & "WHERE URNO = " & strUrno & vbLf
                strUrnos = strUrnos & strUrno & vbLf
                
                dtmSTDT = UTCDateToLocal(rsAftUpd.Fields("STOA").Value, UtcTimeDiff)
                dtmSTDT = DateTimeToDate(dtmSTDT)
                
                strData = strData & strDataTemp
                For i = 0 To txtCtrNo.UBound
                    strData = strData & "," & txtCtrNo(i).Text
                    
                    If txtCtrOpen(i).DataField = "NULL" Then
                        strData = strData & ","
                    Else
                        dtmCtrOpen = LocalDateToUTC(ShortTimeToFullDateTime(dtmSTDT, txtCtrOpen(i).DataField), UtcTimeDiff)
                        strData = strData & "," & Format(dtmCtrOpen, "YYYYMMDDhhmmss")
                    End If

                    If txtCtrClose(i).DataField = "NULL" Then
                        strData = strData & ","
                    Else
                        dtmCtrClose = LocalDateToUTC(ShortTimeToFullDateTime(dtmSTDT, txtCtrClose(i).DataField), UtcTimeDiff)
                        strData = strData & "," & Format(dtmCtrClose, "YYYYMMDDhhmmss")
                    End If

                    If txtActOpen(i).DataField = "NULL" Then
                        strData = strData & ","
                    Else
                        dtmActOpen = LocalDateToUTC(ShortTimeToFullDateTime(dtmSTDT, txtActOpen(i).DataField), UtcTimeDiff)
                        strData = strData & "," & Format(dtmActOpen, "YYYYMMDDhhmmss")
                    End If

                    If txtActClose(i).DataField = "NULL" Then
                        strData = strData & ","
                    Else
                        dtmActClose = LocalDateToUTC(ShortTimeToFullDateTime(dtmSTDT, txtActClose(i).DataField), UtcTimeDiff)
                        strData = strData & "," & Format(dtmActClose, "YYYYMMDDhhmmss")
                    End If
                Next i
                strData = strData & vbLf
                
                rsAftUpd.MoveNext
            Loop
            
            oUfisServer.CallCeda strResult, strCommand, strTable, strFields, strData, strWhere, "", 0, False, False
            
            Call UpdateRecords(rsAft, strCommand, strFields, strData, strUrnos)
        Else 'Departure
            strTable = "AFTTAB"
            strCommand = "UFR"
            strWhere = "WHERE URNO "
            If InStr(strUrnoList, ",") > 0 Then
                strWhere = strWhere & "IN (" & strUrnoList & ")"
            Else
                strWhere = strWhere & "= " & strUrnoList
            End If
            strFields = "GTD1,GTD2,DES3,DES4,VIA3,VIA4,VIAN,VIAL,CKIF,CKIT"
            strData = txtGate(0).Text & "," & txtGate(1).Text & "," & _
                GetFullViaList(txtCSLeg.Text, rsAirportCodes, "D")
            If (IsDetailEmpty(0)) Then
                strData = strData & ",,"
            Else
                strData = strData & "," & txtCtrNo(0).Text & _
                    "," & txtCtrNo(txtCtrNo.UBound).Text
            End If

            oUfisServer.CallCeda strResult, strCommand, strTable, strFields, strData, strWhere, "", 0, False, False
            
            strUrnos = Replace(strUrnoList, ",", vbLf)
            Call UpdateRecords(rsAft, strCommand, strFields, strData, strUrnoList)

            'Delete Checkin-counters
            strTable = "CCATAB"
            strCommand = "DRT"
            strFields = ""
            strData = ""
            strWhere = "WHERE FLNU "
            If InStr(strUrnoList, ",") > 0 Then
                strWhere = strWhere & "IN (" & strUrnoList & ")"
            Else
                strWhere = strWhere & "= " & strUrnoList
            End If
            oUfisServer.CallCeda strResult, strCommand, strTable, strFields, strData, strWhere, "", 0, False, False
            
            Call UpdateRecords(rsCCA, strCommand, strFields, strData, strUrnos, "FLNU")

            'Insert Checkin-counters
            If Not (IsDetailEmpty(0)) Then

                strACT3 = RTrim(rsAft.Fields("ACT3").Value)

                strFields = "USEC,CDAT,ACT3,FLNO,URNO,FLNU,STOD,CKIC,CKIT,CKBS,CKES,CKBA,CKEA"
                
                strDataTemp = LoginUserName & "," & Format(Now, "YYYYMMDDHHmmSS") & "," & _
                    strACT3 & "," & strFlno
                strData = ""
                strDataUpd = ""

                rsAftUpd.MoveFirst
                Do While Not rsAftUpd.EOF
                    strUrno = CStr(rsAftUpd.Fields("URNO").Value)
                    
                    dtmSTDT = UTCDateToLocal(rsAftUpd.Fields("STOD").Value, UtcTimeDiff)
                    dtmSTDT = DateTimeToDate(dtmSTDT)
                    
                    For i = 0 To txtCtrNo.UBound
                        lngInsertCount = i + 1
                    
                        strData = strData & "*CMD*," & strTable & ",IRT," & _
                            CStr(UBound(Split(strFields, ",")) + 1) & "," & strFields & _
                            vbLf
                    
                        strData = strData & strDataTemp & "," & _
                            "#UrnoPoolGetNext" & CStr(lngInsertCount) & "#," & _
                            strUrno & "," & Format(rsAftUpd.Fields("STOD").Value, "YYYYMMDDHHmmSS") & "," & _
                            txtCtrNo(i).Text & "," & txtTer(i).Text
                        strDataUpd = strDataUpd & strDataTemp & "," & _
                            "#UrnoPoolGetNext" & CStr(lngInsertCount) & "#," & _
                            strUrno & "," & Format(rsAftUpd.Fields("STOD").Value, "YYYYMMDDHHmmSS") & "," & _
                            txtCtrNo(i).Text & "," & txtTer(i).Text

                        If txtCtrOpen(i).DataField = "NULL" Then
                            strData = strData & ","
                            strDataUpd = strDataUpd & ","
                        Else
                            dtmCtrOpen = LocalDateToUTC(ShortTimeToFullDateTime(dtmSTDT, txtCtrOpen(i).DataField), UtcTimeDiff)
                            strData = strData & "," & Format(dtmCtrOpen, "YYYYMMDDhhmmss")
                            strDataUpd = strDataUpd & "," & Format(dtmCtrOpen, "YYYYMMDDhhmmss")
                        End If

                        If txtCtrClose(i).DataField = "NULL" Then
                            strData = strData & ","
                            strDataUpd = strDataUpd & ","
                        Else
                            dtmCtrClose = LocalDateToUTC(ShortTimeToFullDateTime(dtmSTDT, txtCtrClose(i).DataField), UtcTimeDiff)
                            strData = strData & "," & Format(dtmCtrClose, "YYYYMMDDhhmmss")
                            strDataUpd = strDataUpd & "," & Format(dtmCtrClose, "YYYYMMDDhhmmss")
                        End If

                        If txtActOpen(i).DataField = "NULL" Then
                            strData = strData & ","
                            strDataUpd = strDataUpd & ","
                        Else
                            dtmActOpen = LocalDateToUTC(ShortTimeToFullDateTime(dtmSTDT, txtActOpen(i).DataField), UtcTimeDiff)
                            strData = strData & "," & Format(dtmActOpen, "YYYYMMDDhhmmss")
                            strDataUpd = strDataUpd & "," & Format(dtmActOpen, "YYYYMMDDhhmmss")
                        End If

                        If txtActClose(i).DataField = "NULL" Then
                            strData = strData & ","
                            strDataUpd = strDataUpd & ","
                        Else
                            dtmActClose = LocalDateToUTC(ShortTimeToFullDateTime(dtmSTDT, txtActClose(i).DataField), UtcTimeDiff)
                            strData = strData & "," & Format(dtmActClose, "YYYYMMDDhhmmss")
                            strDataUpd = strDataUpd & "," & Format(dtmActClose, "YYYYMMDDhhmmss")
                        End If
                        
                        strData = strData & vbLf
                        strDataUpd = strDataUpd & vbLf
                    Next i

                    rsAftUpd.MoveNext
                Loop

                'get the new urno for insertion
                strUrnos = vbLf
                If lngInsertCount > 0 Then
                    oUfisServer.UrnoPoolInit lngInsertCount
                    oUfisServer.UrnoPoolPrepare lngInsertCount
                    
                    strUrnos = ""
                    For i = 1 To lngInsertCount
                        strFind = "#UrnoPoolGetNext" & CStr(i) & "#"
                        strUrno = oUfisServer.UrnoPoolGetNext
                        
                        strData = Replace(strData, strFind, strUrno)
                        strDataUpd = Replace(strDataUpd, strFind, strUrno)
                        
                        strUrnos = strUrnos & strUrno & vbLf
                    Next i
                End If

                Call InsertUpdateRecords(strTable, strFields, strData)
                
                Call UpdateRecords(rsCCA, "IRT", strFields, strDataUpd, strUrnos)
            End If
        End If
    Else 'Code share flights
        If FormType = ftyArrival Then
            'Get all CS Flight records
            Set oUFISRec = New UFISRecordset
            With oUFISRec
                Set .UfisServer = oUfisServer
                .ConvertSpecialChars = True
                .CheckOldVersionWhenConverting = True
                .CommandType = cmdReadTable
                .TableName = "CSDTAB"
                .ColumnList = "URNO,STDT"
                .ColumnTypeList = "N,D"
                .WhereClause = "WHERE (RURN IN (" & strUrnoList & ")) AND " & _
                    "(JFNO = '" & cboCodeShare.Text & "')"
                
                Set rsCS = .CreateRecordset
            End With
        
            'Update CS Flight
            strTable = "CSDTAB"
            strFields = "USEU,LSTU,GTA1,GTA2,ORG3,ORG4,VIA3,VIA4,VIAL," & _
                "BLT1,B1BS,B1ES,B1BA,B1EA,BLT2,B2BS,B2ES,B2BA,B2EA"
                
            strDataTemp = LoginUserName & "," & Format(Now, "YYYYMMDDHHmmSS") & "," & _
                txtGate(0).Text & "," & txtGate(1).Text & "," & _
                GetFullViaList(txtCSLeg.Text, rsAirportCodes, "A", False)
            strData = ""
            strDataUpd = ""
            strUrnos = ""
            
            If Not (rsCS.BOF And rsCS.EOF) Then rsCS.MoveFirst
            Do While Not rsCS.EOF
                strUrno = rsCS.Fields("URNO").Value
                strUrnos = strUrnos & strUrno & vbLf

                dtmSTDT = UTCDateToLocal(rsCS.Fields("STDT").Value, UtcTimeDiff)
                dtmSTDT = DateTimeToDate(dtmSTDT)
                
                strData = strData & "*CMD*," & strTable & ",URT," & _
                    CStr(UBound(Split(strFields, ",")) + 1) & "," & strFields & _
                    ",[URNO=:VURNO]" & vbLf
            
                strData = strData & strDataTemp
                strDataUpd = strDataUpd & strDataTemp
                For i = 0 To txtCtrNo.UBound
                    strData = strData & "," & txtCtrNo(i).Text
                    strDataUpd = strDataUpd & "," & txtCtrNo(i).Text

                    If txtCtrOpen(i).DataField = "NULL" Then
                        strData = strData & ","
                        strDataUpd = strDataUpd & ","
                    Else
                        dtmCtrOpen = LocalDateToUTC(ShortTimeToFullDateTime(dtmSTDT, txtCtrOpen(i).DataField), UtcTimeDiff)
                        strData = strData & "," & Format(dtmCtrOpen, "YYYYMMDDhhmmss")
                        strDataUpd = strDataUpd & "," & Format(dtmCtrOpen, "YYYYMMDDhhmmss")
                    End If

                    If txtCtrClose(i).DataField = "NULL" Then
                        strData = strData & ","
                        strDataUpd = strDataUpd & ","
                    Else
                        dtmCtrClose = LocalDateToUTC(ShortTimeToFullDateTime(dtmSTDT, txtCtrClose(i).DataField), UtcTimeDiff)
                        strData = strData & "," & Format(dtmCtrClose, "YYYYMMDDhhmmss")
                        strDataUpd = strDataUpd & "," & Format(dtmCtrClose, "YYYYMMDDhhmmss")
                    End If

                    If txtActOpen(i).DataField = "NULL" Then
                        strData = strData & ","
                        strDataUpd = strDataUpd & ","
                    Else
                        dtmActOpen = LocalDateToUTC(ShortTimeToFullDateTime(dtmSTDT, txtActOpen(i).DataField), UtcTimeDiff)
                        strData = strData & "," & Format(dtmActOpen, "YYYYMMDDhhmmss")
                        strDataUpd = strDataUpd & "," & Format(dtmActOpen, "YYYYMMDDhhmmss")
                    End If

                    If txtActClose(i).DataField = "NULL" Then
                        strData = strData & ","
                        strDataUpd = strDataUpd & ","
                    Else
                        dtmActClose = LocalDateToUTC(ShortTimeToFullDateTime(dtmSTDT, txtActClose(i).DataField), UtcTimeDiff)
                        strData = strData & "," & Format(dtmActClose, "YYYYMMDDhhmmss")
                        strDataUpd = strDataUpd & "," & Format(dtmActClose, "YYYYMMDDhhmmss")
                    End If
                Next i
                strData = strData & "," & strUrno & vbLf
                strDataUpd = strDataUpd & vbLf
                
                rsCS.MoveNext
            Loop
            
            If strData <> "" Then
                strResult = InsertUpdateRecords(strTable, strFields, strData)
                
                Call UpdateRecords(rsCSD, "URT", strFields, strDataUpd, strUrnos)
            End If
        Else 'Departure
            strTable = "CSDTAB"
            strCommand = "URT"
            strWhere = "WHERE RURN "
            If InStr(strUrnoList, ",") > 0 Then
                strWhere = strWhere & "IN (" & strUrnoList & ")"
            Else
                strWhere = strWhere & "= " & strUrnoList
            End If
            strWhere = strWhere & " AND (JFNO = '" & cboCodeShare.Text & "')"
            strFields = "GTD1,GTD2,DES3,DES4,VIA3,VIA4,VIAL,CKIF,CKIT"

            strData = txtGate(0).Text & "," & txtGate(1).Text & _
                "," & GetFullViaList(txtCSLeg.Text, rsAirportCodes, "D", False)
            If (IsDetailEmpty(0)) Then
                strData = strData & ",,"
            Else
                strData = strData & "," & txtCtrNo(0).Text & _
                    "," & txtCtrNo(txtCtrNo.UBound).Text
            End If

            oUfisServer.CallCeda strResult, strCommand, strTable, strFields, strData, strWhere, "", 0, False, False
            
            strUrnos = Replace(strUrnoList & ",", ",", ",'" & cboCodeShare.Text & "'" & vbLf)
            Call UpdateRecords(rsCSD, strCommand, strFields, strData, strUrnos, "RURN,JFNO")

            'Delete Checkin-counters
            strTable = "CSCTAB"
            strCommand = "DRT"
            strFields = ""
            strData = ""
            strWhere = "WHERE RURN "
            If InStr(strUrnoList, ",") > 0 Then
                strWhere = strWhere & "IN (" & strUrnoList & ")"
            Else
                strWhere = strWhere & "= " & strUrnoList
            End If
            strWhere = strWhere & " AND (JFNO = '" & cboCodeShare.Text & "')"
            oUfisServer.CallCeda strResult, strCommand, strTable, strFields, strData, strWhere, "", 0, False, False
            
             Call UpdateRecords(rsCSC, strCommand, strFields, strData, strUrnos, "RURN,JFNO")

            'Insert Checkin-counters
            If Not (IsDetailEmpty(0)) Then

                strFields = "USEC,CDAT,FLNO,JFNO,URNO,RURN,ADID,STDT,CKIC,CKTR,CKBS,CKES,CKBA,CKEA"
                
                strDataTemp = LoginUserName & "," & Format(Now, "YYYYMMDDHHmmSS") & "," & _
                    strFlno & "," & cboCodeShare.Text
                strData = ""
                strDataUpd = ""

                rsAftUpd.MoveFirst
                Do While Not rsAftUpd.EOF
                    strUrno = CStr(rsAftUpd.Fields("URNO").Value)
                
                    dtmSTDT = UTCDateToLocal(rsAftUpd.Fields("STOD").Value, UtcTimeDiff)
                    dtmSTDT = DateTimeToDate(dtmSTDT)
                        
                    For i = 0 To txtCtrNo.UBound
                        lngInsertCount = i + 1
                        
                        strData = strData & "*CMD*," & strTable & ",IRT," & _
                            CStr(UBound(Split(strFields, ",")) + 1) & "," & strFields & _
                            vbLf
                        
                        strData = strData & strDataTemp & _
                            "," & "#UrnoPoolGetNext" & CStr(lngInsertCount) & "#," & _
                            strUrno & "," & rsAftUpd.Fields("ADID").Value & "," & _
                            Format(rsAftUpd.Fields("STOD").Value, "YYYYMMDDHHmmSS") & "," & _
                            txtCtrNo(i).Text & "," & txtTer(i).Text
                        strDataUpd = strDataUpd & strDataTemp & _
                            "," & "#UrnoPoolGetNext" & CStr(lngInsertCount) & "#," & _
                            strUrno & "," & rsAftUpd.Fields("ADID").Value & "," & _
                            Format(rsAftUpd.Fields("STOD").Value, "YYYYMMDDHHmmSS") & "," & _
                            txtCtrNo(i).Text & "," & txtTer(i).Text

                        If txtCtrOpen(i).DataField = "NULL" Then
                            strData = strData & ","
                            strDataUpd = strDataUpd & ","
                        Else
                            dtmCtrOpen = LocalDateToUTC(ShortTimeToFullDateTime(dtmSTDT, txtCtrOpen(i).DataField), UtcTimeDiff)
                            strData = strData & "," & Format(dtmCtrOpen, "YYYYMMDDhhmmss")
                            strDataUpd = strDataUpd & "," & Format(dtmCtrOpen, "YYYYMMDDhhmmss")
                        End If

                        If txtCtrClose(i).DataField = "NULL" Then
                            strData = strData & ","
                            strDataUpd = strDataUpd & ","
                        Else
                            dtmCtrClose = LocalDateToUTC(ShortTimeToFullDateTime(dtmSTDT, txtCtrClose(i).DataField), UtcTimeDiff)
                            strData = strData & "," & Format(dtmCtrClose, "YYYYMMDDhhmmss")
                            strDataUpd = strDataUpd & "," & Format(dtmCtrClose, "YYYYMMDDhhmmss")
                        End If

                        If txtActOpen(i).DataField = "NULL" Then
                            strData = strData & ","
                            strDataUpd = strDataUpd & ","
                        Else
                            dtmActOpen = LocalDateToUTC(ShortTimeToFullDateTime(dtmSTDT, txtActOpen(i).DataField), UtcTimeDiff)
                            strData = strData & "," & Format(dtmActOpen, "YYYYMMDDhhmmss")
                            strDataUpd = strDataUpd & "," & Format(dtmActOpen, "YYYYMMDDhhmmss")
                        End If

                        If txtActClose(i).DataField = "NULL" Then
                            strData = strData & ","
                            strDataUpd = strDataUpd & ","
                        Else
                            dtmActClose = LocalDateToUTC(ShortTimeToFullDateTime(dtmSTDT, txtActClose(i).DataField), UtcTimeDiff)
                            strData = strData & "," & Format(dtmActClose, "YYYYMMDDhhmmss")
                            strDataUpd = strDataUpd & "," & Format(dtmActClose, "YYYYMMDDhhmmss")
                        End If

                        strData = strData & vbLf
                        strDataUpd = strDataUpd & vbLf
                    Next i
                    
                    rsAftUpd.MoveNext
                Loop
                
                'get the new urno for insertion
                strUrnos = vbLf
                If lngInsertCount > 0 Then
                    oUfisServer.UrnoPoolInit lngInsertCount
                    oUfisServer.UrnoPoolPrepare lngInsertCount
            
                    strUrnos = ""
                    For i = 1 To lngInsertCount
                        strFind = "#UrnoPoolGetNext" & CStr(i) & "#"
                        strUrno = oUfisServer.UrnoPoolGetNext
                        
                        strData = Replace(strData, strFind, strUrno)
                        strDataUpd = Replace(strDataUpd, strFind, strUrno)
                        
                        strUrnos = strUrnos & strUrno & vbLf
                    Next i
                End If

                Call InsertUpdateRecords(strTable, strFields, strData)
                
                Call UpdateRecords(rsCSC, "IRT", strFields, strDataUpd, strUrnos)
            End If
        End If
    End If

    SaveUpdates = True
    Call HideFloatingMessage

    Screen.MousePointer = vbDefault

    Exit Function

ErrSave:
    Call HideFloatingMessage
    SaveUpdates = False
    Screen.MousePointer = vbDefault
End Function

Private Sub ArrangeObjects()
    Me.Caption = "Belt Update"
        
    lblLabels(7).Caption = "Belts:"
    lblCaptions(0).Caption = "Belt No."
    lblCaptions(0).Width = 1335
    shpCaptions(0).Width = lblCaptions(0).Width
    txtCtrNo(0).Width = lblCaptions(0).Width
    txtTer(0).Visible = False
    lblCaptions(1).Visible = False
    shpCaptions(1).Visible = False
    'hide "add details" options
    chkAdd.Visible = False
    shpCaptions(5).Width = 7200
    'add 1 more row for belts
    Call AddDetails
End Sub

Private Sub AddBeltDetails(rsData As ADODB.Recordset)
    Dim dtmBaseDate As Date
    Dim i As Integer
    Dim strDate As String
    Dim dtmDate As Date
    Dim nLine As Integer
    
    dtmBaseDate = DateTimeToDate(UTCDateToLocal(rsAft.Fields("STOA").Value, UtcTimeDiff))
    
    nLine = 0
    For i = 0 To 1
        txtCtrNo(i).Text = RTrim(rsData.Fields("BLT" & CStr(i + 1)).Value)
        
        If IsNull(rsData.Fields("B" & CStr(i + 1) & "BS").Value) Then
            strDate = ""
        Else
            dtmDate = rsData.Fields("B" & CStr(i + 1) & "BS").Value
            dtmDate = UTCDateToLocal(dtmDate, UtcTimeDiff)
            strDate = FullDateTimeToShortTime(dtmBaseDate, dtmDate)
        End If
        txtCtrOpen(i).Text = strDate
                
        If IsNull(rsData.Fields("B" & CStr(i + 1) & "ES").Value) Then
            strDate = ""
        Else
            dtmDate = rsData.Fields("B" & CStr(i + 1) & "ES").Value
            dtmDate = UTCDateToLocal(dtmDate, UtcTimeDiff)
            strDate = FullDateTimeToShortTime(dtmBaseDate, dtmDate)
        End If
        txtCtrClose(i).Text = strDate
        
        If IsNull(rsData.Fields("B" & CStr(i + 1) & "BA").Value) Then
            strDate = ""
        Else
            dtmDate = rsData.Fields("B" & CStr(i + 1) & "BA").Value
            dtmDate = UTCDateToLocal(dtmDate, UtcTimeDiff)
            strDate = FullDateTimeToShortTime(dtmBaseDate, dtmDate)
        End If
        txtActOpen(i).Text = strDate
        
        If IsNull(rsData.Fields("B" & CStr(i + 1) & "EA").Value) Then
            strDate = ""
        Else
            dtmDate = rsData.Fields("B" & CStr(i + 1) & "EA").Value
            dtmDate = UTCDateToLocal(dtmDate, UtcTimeDiff)
            strDate = FullDateTimeToShortTime(dtmBaseDate, dtmDate)
        End If
        txtActClose(i).Text = strDate
    Next i
End Sub

Private Sub AddCICDetails(rsData As ADODB.Recordset, ByVal MainFlight As Boolean, _
Optional ByVal CodeShareFlight As String = "")
    Dim dtmBaseDate As Date
    Dim i As Integer, j As Integer
    Dim strDate As String
    Dim dtmDate As Date
    
    dtmBaseDate = DateTimeToDate(UTCDateToLocal(rsAft.Fields("STOD").Value, UtcTimeDiff))
    
    If MainFlight Then
        rsData.Find "FLNU = " & FlightUrno, , , 1
    Else
        rsData.Filter = "RURN = " & FlightUrno & " AND JFNO = '" & CodeShareFlight & "'"
    End If
    
    i = 0
    Do While Not rsData.EOF
        If MainFlight Then
            If rsData.Fields("FLNU").Value <> FlightUrno Then Exit Do
        End If
    
        If i > txtCtrNo.UBound Then
            Call AddDetails
        End If
        
        txtCtrNo(i).Text = RTrim(rsData.Fields("CKIC").Value)
        If MainFlight Then
            txtTer(i).Text = RTrim(rsData.Fields("CKIT").Value)
        Else
            txtTer(i).Text = RTrim(rsData.Fields("CKTR").Value)
        End If
        
        If IsNull(rsData.Fields("CKBS").Value) Then
            strDate = ""
        Else
            dtmDate = rsData.Fields("CKBS").Value
            dtmDate = UTCDateToLocal(dtmDate, UtcTimeDiff)
            strDate = FullDateTimeToShortTime(dtmBaseDate, dtmDate)
        End If
        txtCtrOpen(i).Text = strDate
        
        If IsNull(rsData.Fields("CKES").Value) Then
            strDate = ""
        Else
            dtmDate = rsData.Fields("CKES").Value
            dtmDate = UTCDateToLocal(dtmDate, UtcTimeDiff)
            strDate = FullDateTimeToShortTime(dtmBaseDate, dtmDate)
        End If
        txtCtrClose(i).Text = strDate
        
        If IsNull(rsData.Fields("CKBA").Value) Then
            strDate = ""
        Else
            dtmDate = rsData.Fields("CKBA").Value
            dtmDate = UTCDateToLocal(dtmDate, UtcTimeDiff)
            strDate = FullDateTimeToShortTime(dtmBaseDate, dtmDate)
        End If
        txtActOpen(i).Text = strDate
        
        If IsNull(rsData.Fields("CKEA").Value) Then
            strDate = ""
        Else
            dtmDate = rsData.Fields("CKEA").Value
            dtmDate = UTCDateToLocal(dtmDate, UtcTimeDiff)
            strDate = FullDateTimeToShortTime(dtmBaseDate, dtmDate)
        End If
        txtActClose(i).Text = strDate
    
        rsData.MoveNext
        i = i + 1
    Loop
    
    If Not MainFlight Then
        rsData.Filter = adFilterNone
    End If
    
    If i <= txtCtrNo.UBound Then
        For j = i To txtCtrNo.UBound
            chkSel(j).Value = vbChecked
        Next j
        tmrClean.Enabled = True
    End If
End Sub

Private Sub AddDetails(Optional ByVal MoveFocus As Boolean = False)
    Dim i As Integer
    Dim nTop As Long
    Dim nLeft As Long
    Dim nTabIndex As Integer
    
    i = txtCtrNo.UBound
    
    nTabIndex = txtActClose(i).TabIndex
    
    i = i + 1
    
    nTop = txtCtrNo(i - 1).Top + txtCtrNo(i - 1).Height
    
    Load chkSel(i)
    chkSel(i).Value = vbUnchecked
    chkSel(i).Move chkSel(i - 1).Left, nTop
    chkSel(i).Visible = True
    chkSel(i).TabIndex = nTabIndex + 1
    
    Load cmdCtrHelp(i)
    cmdCtrHelp(i).Tag = "CIC" & CStr(i)
    cmdCtrHelp(i).Move cmdCtrHelp(i - 1).Left, nTop
    cmdCtrHelp(i).Visible = True
    cmdCtrHelp(i).TabIndex = nTabIndex + 2
    
    Load txtCtrNo(i)
    txtCtrNo(i).Text = ""
    txtCtrNo(i).Move txtCtrNo(i - 1).Left, nTop
    txtCtrNo(i).Visible = True
    txtCtrNo(i).TabIndex = nTabIndex + 3
    
    If FormType = ftyDeparture Then
        Load txtTer(i)
        txtTer(i).Text = ""
        txtTer(i).Move txtTer(i - 1).Left, nTop
        txtTer(i).Visible = True
        txtTer(i).TabIndex = nTabIndex + 4
    Else
        nTabIndex = nTabIndex - 1
    End If
    
    Load txtCtrOpen(i)
    txtCtrOpen(i).Text = ""
    txtCtrOpen(i).DataField = "NULL"
    txtCtrOpen(i).Move txtCtrOpen(i - 1).Left, nTop
    txtCtrOpen(i).Visible = True
    txtCtrOpen(i).TabIndex = nTabIndex + 5
    
    Load txtCtrClose(i)
    txtCtrClose(i).Text = ""
    txtCtrClose(i).DataField = "NULL"
    txtCtrClose(i).Move txtCtrClose(i - 1).Left, nTop
    txtCtrClose(i).Visible = True
    txtCtrClose(i).TabIndex = nTabIndex + 6
    
    Load txtActOpen(i)
    txtActOpen(i).Text = ""
    txtActOpen(i).DataField = "NULL"
    txtActOpen(i).Move txtActOpen(i - 1).Left, nTop
    txtActOpen(i).Visible = True
    txtActOpen(i).TabIndex = nTabIndex + 7
    
    Load txtActClose(i)
    txtActClose(i).Text = ""
    txtActClose(i).DataField = "NULL"
    txtActClose(i).Move txtActClose(i - 1).Left, nTop
    txtActClose(i).Visible = True
    txtActClose(i).TabIndex = nTabIndex + 8
    
    picDetails.Height = picDetails.Height + txtCtrNo(i).Height
    
    If MoveFocus Then
        txtCtrNo(i).SetFocus
    End If
End Sub

Private Function DeleteDetails() As Boolean
    Dim i As Integer
    Dim SwapPoint As Integer
    Dim ItemCount As Integer
    Dim intItemSelCount As Integer
    
    If ItemSelCount = 0 Then
        MsgBoxEx "Please select one or more items to delete.", vbInformation, Me.Caption, , , eCentreDialog
        Exit Function
    End If
    
    SwapPoint = -1
    ItemCount = txtCtrNo.UBound
    intItemSelCount = ItemSelCount
    For i = 0 To ItemCount
        If SwapPoint >= 0 Then
            If chkSel(i).Value = vbUnchecked Then
                chkSel(SwapPoint).Value = vbUnchecked
                Call CopyDetailItems(i, SwapPoint)
                SwapPoint = SwapPoint + 1
            End If
        Else
            If chkSel(i).Value = vbChecked Then
                SwapPoint = i
            End If
        End If
    Next i
    
    For i = 0 To intItemSelCount - 1
        Call DeleteDetailItems(ItemCount - i)
    Next i
    
    chkAll.Value = vbUnchecked
    ItemSelCount = 0
    
    DeleteDetails = True
End Function

Private Sub CopyDetailItems(ByVal FromIndex As Integer, ByVal ToIndex As Integer)
    txtCtrNo(ToIndex).Text = txtCtrNo(FromIndex).Text
    txtCtrNo(ToIndex).BackColor = vbWindowBackground
    If FormType = ftyDeparture Then
        txtTer(ToIndex).Text = txtTer(FromIndex).Text
    End If
    txtCtrOpen(ToIndex).Text = txtCtrOpen(FromIndex).Text
    txtCtrOpen(ToIndex).DataField = txtCtrOpen(FromIndex).DataField
    txtCtrClose(ToIndex).Text = txtCtrClose(FromIndex).Text
    txtCtrClose(ToIndex).DataField = txtCtrClose(FromIndex).DataField
    txtActOpen(ToIndex).Text = txtActOpen(FromIndex).Text
    txtActOpen(ToIndex).DataField = txtActOpen(FromIndex).DataField
    txtActClose(ToIndex).Text = txtActClose(FromIndex).Text
    txtActClose(ToIndex).DataField = txtActClose(FromIndex).DataField
End Sub

Private Sub DeleteDetailItems(ByVal Index As Integer)
    Dim nIndex As Integer
    
    If FormType = ftyArrival Then
        nIndex = 1
    Else
        nIndex = 0
    End If
    
    If Index <= nIndex Then
        chkSel(Index).Value = vbUnchecked
        txtCtrNo(Index).Text = ""
        txtCtrNo(Index).BackColor = vbWindowBackground
        If FormType = ftyDeparture Then
            txtTer(Index).Text = ""
        End If
        txtCtrOpen(Index).Text = ""
        txtCtrOpen(Index).DataField = "NULL"
        txtCtrClose(Index).Text = ""
        txtCtrClose(Index).DataField = "NULL"
        txtActOpen(Index).Text = ""
        txtActOpen(Index).DataField = "NULL"
        txtActClose(Index).Text = ""
        txtActClose(Index).DataField = "NULL"
    Else
        Unload chkSel(Index)
        Unload cmdCtrHelp(Index)
        Unload txtCtrNo(Index)
        If FormType = ftyDeparture Then
            Unload txtTer(Index)
        End If
        Unload txtCtrOpen(Index)
        Unload txtCtrClose(Index)
        Unload txtActOpen(Index)
        Unload txtActClose(Index)
    End If
End Sub

Private Sub PopulateCodeShareList(rsData As ADODB.Recordset, Optional ByVal NewFlight As Boolean = True)
    Dim i As Integer
    Dim CodeShareCount As Integer
    Dim CodeShareFlight As String
    Dim CodeShareItem As String
    Dim CurrentSelText As String
    Dim SelectedItem As Integer

    CodeShareCount = NVL(rsData.Fields("JCNT").Value, 0)
    CodeShareFlight = rsData.Fields("JFNO").Value
    
    If Not NewFlight Then
        If CodeShareFlight = cboCodeShare.Tag Then Exit Sub
        
        CurrentSelText = cboCodeShare.Text
        
        DoNotRefresh = True
    End If
    
    cboCodeShare.Clear
    cboCodeShare.AddItem Space(5)
    
    SelectedItem = 0
    For i = 1 To CodeShareCount
        CodeShareItem = Trim(Mid(CodeShareFlight, 9 * i - 8, 9))
        cboCodeShare.AddItem CodeShareItem
        
        If Not NewFlight Then
            If CodeShareItem = CurrentSelText Then
                SelectedItem = i
            End If
        End If
    Next i
    
    cboCodeShare.Tag = CodeShareFlight
    
    If NewFlight Then
        CurrentCSIndex = -1
        If cboCodeShare.ListCount > 0 Then cboCodeShare.ListIndex = 0
    Else
        If SelectedItem = 0 And CurrentSelText <> Space(5) Then
            cboCodeShare.AddItem Trim(CurrentSelText)
            SelectedItem = cboCodeShare.ListCount - 1
            cboCodeShare.ItemData(SelectedItem) = 1
        End If
        cboCodeShare.ListIndex = SelectedItem
        
        CurrentCSIndex = SelectedItem
        
        DoNotRefresh = False
    End If
End Sub

Private Sub FillInData(rsData As ADODB.Recordset, Optional ByVal NewFlight As Boolean = True)
    Dim blnMainFlight As Boolean
    Dim strLeg As String
    Dim strJFNO As String

    blnMainFlight = (CurrentCSIndex = 0)
    If blnMainFlight Then
        strLeg = GetViaListString(rsData.Fields("VIAL").Value, _
            NVL(rsData.Fields("VIAN").Value, 0))
        If FormType = ftyArrival Then
            strLeg = GetCSLeg(strLeg, RTrim(rsData.Fields("ORG3").Value), _
                RTrim(rsData.Fields("ADID").Value))
        Else
            strLeg = GetCSLeg(strLeg, RTrim(rsData.Fields("DES3").Value), _
                RTrim(rsData.Fields("ADID").Value))
        End If
        txtCSLeg.Text = strLeg
        If FormType = ftyArrival Then
            txtGate(0).Text = RTrim(rsData.Fields("GTA1").Value)
            txtGate(1).Text = RTrim(rsData.Fields("GTA2").Value)
            
            'Belts
            Call AddBeltDetails(rsData)
        Else
            txtGate(0).Text = RTrim(rsData.Fields("GTD1").Value)
            txtGate(1).Text = RTrim(rsData.Fields("GTD2").Value)
            
            'Check-in counters
            If NewFlight Then
                Call AddCICDetails(rsCCA, blnMainFlight)
            End If
        End If
    Else 'Code Share Flight
        strLeg = GetViaListString(rsData.Fields("VIAL").Value)
        If FormType = ftyArrival Then
            strLeg = GetCSLeg(strLeg, RTrim(rsData.Fields("ORG3").Value), _
                "A")
        Else
            strLeg = GetCSLeg(strLeg, RTrim(rsData.Fields("DES3").Value), _
                "D")
        End If
        txtCSLeg.Text = strLeg
        If FormType = ftyArrival Then
            txtGate(0).Text = RTrim(rsData.Fields("GTA1").Value)
            txtGate(1).Text = RTrim(rsData.Fields("GTA2").Value)
            
            'Belts
            Call AddBeltDetails(rsData)
        Else
            txtGate(0).Text = RTrim(rsData.Fields("GTD1").Value)
            txtGate(1).Text = RTrim(rsData.Fields("GTD2").Value)
            
            'Check-in counters
            If NewFlight Then
                strJFNO = cboCodeShare.Text
                Call AddCICDetails(rsCSC, blnMainFlight, strJFNO)
            End If
        End If
    End If
    
    cmdOK.BackColor = vbButtonFace
End Sub

Private Sub HandleScrollbar()
    vsbDetails.Min = 0
    If txtCtrNo.UBound > 9 Then
        vsbDetails.Max = txtCtrNo.UBound - 9
    Else
        vsbDetails.Max = 0
    End If
    vsbDetails.Value = vsbDetails.Max
End Sub

Private Sub MoveFocusUpDown(ObjectToMove As Object, ByVal Index As Integer, ByVal KeyCode As Integer)
    Select Case KeyCode
        Case vbKeyUp
            If Index > 0 Then
                If ObjectToMove.LBound <= Index - 1 Then
                    ObjectToMove(Index - 1).SetFocus
                End If
            End If
        Case vbKeyDown
            If ObjectToMove.UBound >= Index + 1 Then
                ObjectToMove(Index + 1).SetFocus
            End If
        Case vbKeyLeft
            SendKeys "+{TAB}"
        Case vbKeyRight
            SendKeys "{TAB}"
    End Select
End Sub

Private Sub cboCodeShare_Click()
    If DoNotRefresh Then Exit Sub
    
    If CurrentCSIndex = cboCodeShare.ListIndex Then Exit Sub
    
    CurrentCSIndex = cboCodeShare.ListIndex
    
    If CurrentCSIndex = 0 Then 'Main Flight
        Call FillInData(rsAft)
    Else
        rsCSD.Filter = "RURN = " & FlightUrno & " AND JFNO = '" & cboCodeShare.Text & "'"
        If rsCSD.EOF Then
            Call ClearAllFields(True)
        Else
            Call FillInData(rsCSD)
        End If
        rsCSD.Filter = adFilterNone
        
        lblDeleted.Visible = (cboCodeShare.ItemData(CurrentCSIndex) = 1)
    End If
    
    lblSuccess.Visible = False
    tmrSuccess.Enabled = False
End Sub

Private Sub chkAdd_Click()
    If chkAdd.Value = vbChecked Then
        Call AddDetails(True)
        Call HandleScrollbar
        
        If Not IsChangesFromCode Then cmdOK.BackColor = vbRed
    End If
    chkAdd.Value = vbUnchecked
End Sub

Private Sub chkAll_Click()
    Dim i As Integer
    
    For i = 0 To chkSel.UBound
        chkSel(i).Value = chkAll.Value
    Next i
End Sub

Private Sub chkDaily_Click()
    If IsUserClickOnDailyCheckBox Then
        If chkDaily.Value = vbChecked Then
            txtDays.Text = "1234567"
        Else
            txtDays.Text = ""
            txtDays.SetFocus
        End If
    End If
End Sub

Private Sub chkDelete_Click()
    If chkDelete.Value = vbChecked Then
        If DeleteDetails Then
            Call HandleScrollbar
            
            If Not IsChangesFromCode Then cmdOK.BackColor = vbRed
        End If
    End If
    chkDelete.Value = vbUnchecked
End Sub

Private Sub chkSel_Click(Index As Integer)
    If chkSel(Index).Value = vbChecked Then
        ItemSelCount = ItemSelCount + 1
    Else
        ItemSelCount = ItemSelCount - 1
    End If
End Sub

Private Sub cmdCancel_Click()
    chkDaily.Value = vbChecked
    
    LoadData FlightUrno, False
    
    CurrentCSIndex = -1
    Call cboCodeShare_Click
    
    cmdOK.BackColor = vbButtonFace
    cboCodeShare.SetFocus
End Sub

Private Sub cmdCtrHelp_Click(Index As Integer)
    Dim strSelectedData As String
    Dim vntSelectedData As Variant

    If FormType = ftyArrival Then 'Belt
        Set LookupForm.DataSource = rsBelts
        LookupForm.Columns = "BNAM,TERM"
        LookupForm.ColumnHeaders = "Name,Terminal"
        LookupForm.ColumnWidths = "1200,1500"
        LookupForm.KeyColumns = "BNAM"
        LookupForm.Caption = "Belts"
        LookupForm.IsOnTop = pblnIsOnTop
        LookupForm.Show vbModal, Me
        
        strSelectedData = LookupForm.SelectedData
        If strSelectedData <> "" Then
            txtCtrNo(Index).Text = RTrim(strSelectedData)
        End If
    Else 'CIC
        Set LookupForm.DataSource = rsCICounters
        LookupForm.Columns = "CNAM,TERM"
        LookupForm.ColumnHeaders = "Name,Terminal"
        LookupForm.ColumnWidths = "1200,1500"
        LookupForm.KeyColumns = "CNAM,TERM"
        LookupForm.Caption = "Check-in Counters"
        LookupForm.IsOnTop = pblnIsOnTop
        LookupForm.Show vbModal, Me
            
        strSelectedData = LookupForm.SelectedData
        If strSelectedData <> "" Then
            vntSelectedData = Split(strSelectedData, ",")
            txtCtrNo(Index).Text = RTrim(vntSelectedData(0))
            txtTer(Index).Text = RTrim(vntSelectedData(1))
        End If
    End If
    
    txtCtrNo(Index).SetFocus
End Sub

Private Sub cmdHelp_Click(Index As Integer)
    Dim strSelectedData As String

    Select Case Index
        Case 0 'Routing
            Set LookupForm.DataSource = rsAirportCodes
            LookupForm.Columns = "APC3,APC4,APFN"
            LookupForm.ColumnHeaders = "APC3,APC4,Airport Name"
            LookupForm.ColumnWidths = "600,800,6000"
            LookupForm.KeyColumns = "APC3,APC4"
            LookupForm.Caption = "Airports"
            LookupForm.IsOnTop = pblnIsOnTop
            LookupForm.Show vbModal, Me
        
            strSelectedData = LookupForm.SelectedData
            If strSelectedData <> "" Then
                strSelectedData = RTrim(Split(strSelectedData, ",")(0))
                If Trim(txtCSLeg.Text) = "" Then
                    txtCSLeg.Text = strSelectedData
                Else
                    If InStr(txtCSLeg.Text, strSelectedData) <= 0 Then
                        txtCSLeg.Text = txtCSLeg & "/" & strSelectedData
                    End If
                End If
            End If
            txtCSLeg.SetFocus
        Case 1, 2 'Gate
            Set LookupForm.DataSource = rsGates
            LookupForm.Columns = "GNAM,TERM"
            LookupForm.ColumnHeaders = "Name,Terminal"
            LookupForm.ColumnWidths = "1200,1500"
            LookupForm.KeyColumns = "GNAM"
            LookupForm.Caption = "Gates"
            LookupForm.IsOnTop = pblnIsOnTop
            LookupForm.Show vbModal, Me
        
            strSelectedData = LookupForm.SelectedData
            If strSelectedData <> "" Then
                txtGate(Index - 1).Text = RTrim(strSelectedData)
            End If
            txtGate(Index - 1).SetFocus
    End Select
End Sub

Private Sub cmdOK_Click()
    Dim strMsg As String

    If ValidateUserInput() Then
        If SaveUpdates() Then
            If FormType = ftyArrival Then
                strMsg = "The belts information"
            Else
                strMsg = "The check-in counters information"
            End If
            strMsg = strMsg & " has been successfully updated."
        
            lblSuccess.Caption = strMsg
            lblSuccess.Visible = True
            tmrSuccess.Enabled = True
            
            Call cmdCancel_Click
        Else
            strMsg = "Failed to update "
            If FormType = ftyArrival Then
                strMsg = strMsg & "the belts information."
            Else
                strMsg = strMsg & "the check-in counters information."
            End If
        
            MsgBoxEx strMsg, vbCritical, Me.Caption, , , eCentreDialog
        End If
    End If
End Sub

Private Sub Form_Load()
    If colDataSources.Exists("AFTTAB") Then
        Set rsAft = colDataSources.Item("AFTTAB")
    End If
    If colDataSources.Exists("CSDTAB") Then
        Set rsCSD = colDataSources.Item("CSDTAB")
    End If
    If colDataSources.Exists("CCATAB") Then
        Set rsCCA = colDataSources.Item("CCATAB")
    End If
    If colDataSources.Exists("CSCTAB") Then
        Set rsCSC = colDataSources.Item("CSCTAB")
    End If
    
    Set rsAirportCodes = colDataSources.Item("APTTAB")
    Set rsGates = colDataSources.Item("GATTAB")
    Set rsBelts = colDataSources.Item("BLTTAB")
    Set rsCICounters = colDataSources.Item("CICTAB")
    
    If FormType = ftyArrival Then
        Call ArrangeObjects
    End If
    
    IsUserClickOnDailyCheckBox = True
    IsChangesFromCode = False
    CurrentCSIndex = -1
    
    Call ClearAllFields
End Sub

Private Sub tmrClean_Timer()
    IsChangesFromCode = True
    Call DeleteDetails
    tmrClean.Enabled = False
    IsChangesFromCode = False
End Sub

Private Sub tmrSuccess_Timer()
    lblSuccess.Visible = False
    tmrSuccess.Enabled = False
End Sub

Private Sub txtActClose_Change(Index As Integer)
    Dim strData As String
    
    strData = CheckValidTimeExt(txtActClose(Index).Text)

    If strData = "ERROR" Then
        txtActClose(Index).BackColor = &HC0C0FF
    Else
        txtActClose(Index).BackColor = vbWindowBackground
    End If
    
    txtActClose(Index).DataField = strData
    
    If Not IsChangesFromCode Then cmdOK.BackColor = vbRed
End Sub

Private Sub txtActClose_KeyDown(Index As Integer, KeyCode As Integer, Shift As Integer)
    Call MoveFocusUpDown(txtActClose, Index, KeyCode)
End Sub

Private Sub txtActClose_KeyPress(Index As Integer, KeyAscii As Integer)
    Const VALID_NUMBERS = "0123456789+-:. "
    
    Select Case KeyAscii
        Case vbKeyBack
        Case Else
            If InStr(VALID_NUMBERS, Chr(KeyAscii)) <= 0 Then
                KeyAscii = 0
            End If
    End Select
End Sub

Private Sub txtActOpen_Change(Index As Integer)
    Dim strData As String
    
    strData = CheckValidTimeExt(txtActOpen(Index).Text)

    If strData = "ERROR" Then
        txtActOpen(Index).BackColor = &HC0C0FF
    Else
        txtActOpen(Index).BackColor = vbWindowBackground
    End If
    
    txtActOpen(Index).DataField = strData
    
    If Not IsChangesFromCode Then cmdOK.BackColor = vbRed
End Sub

Private Sub txtActOpen_KeyDown(Index As Integer, KeyCode As Integer, Shift As Integer)
    Call MoveFocusUpDown(txtActOpen, Index, KeyCode)
End Sub

Private Sub txtActOpen_KeyPress(Index As Integer, KeyAscii As Integer)
    Const VALID_NUMBERS = "0123456789+-:. "
    
    Select Case KeyAscii
        Case vbKeyBack
        Case Else
            If InStr(VALID_NUMBERS, Chr(KeyAscii)) <= 0 Then
                KeyAscii = 0
            End If
    End Select
End Sub

Private Sub txtCSLeg_Change()
    If Not IsChangesFromCode Then cmdOK.BackColor = vbRed
End Sub

Private Sub txtCSLeg_KeyPress(KeyAscii As Integer)
    KeyAscii = UCaseAsc(KeyAscii)
End Sub

Private Sub txtCtrClose_Change(Index As Integer)
    Dim strData As String
    
    strData = CheckValidTimeExt(txtCtrClose(Index).Text)

    If strData = "ERROR" Then
        txtCtrClose(Index).BackColor = &HC0C0FF
    Else
        txtCtrClose(Index).BackColor = vbWindowBackground
    End If
    
    txtCtrClose(Index).DataField = strData
    
    If Not IsChangesFromCode Then cmdOK.BackColor = vbRed
End Sub

Private Sub txtCtrClose_KeyDown(Index As Integer, KeyCode As Integer, Shift As Integer)
    Call MoveFocusUpDown(txtCtrClose, Index, KeyCode)
End Sub

Private Sub txtCtrClose_KeyPress(Index As Integer, KeyAscii As Integer)
    Const VALID_NUMBERS = "0123456789+-:. "
    
    Select Case KeyAscii
        Case vbKeyBack
        Case Else
            If InStr(VALID_NUMBERS, Chr(KeyAscii)) <= 0 Then
                KeyAscii = 0
            End If
    End Select
End Sub

Private Sub txtCtrNo_Change(Index As Integer)
    If Not IsChangesFromCode Then cmdOK.BackColor = vbRed
End Sub

Private Sub txtCtrNo_KeyDown(Index As Integer, KeyCode As Integer, Shift As Integer)
    Call MoveFocusUpDown(txtCtrNo, Index, KeyCode)
End Sub

Private Sub txtCtrNo_KeyPress(Index As Integer, KeyAscii As Integer)
    KeyAscii = UCaseAsc(KeyAscii)
End Sub

Private Sub txtCtrNo_Validate(Index As Integer, Cancel As Boolean)
    If FormType <> ftyDeparture Then Exit Sub
    
    If Trim(txtCtrNo(Index).Text) = "" Then Exit Sub
    
    rsBelts.Find "BNAM = '" & txtCtrNo(Index).Text & "'", , , 1
    If Not rsBelts.EOF Then
        txtTer(Index).Text = RTrim(rsBelts.Fields("TERM").Value)
    End If
End Sub

Private Sub txtCtrOpen_Change(Index As Integer)
    Dim strData As String
    
    strData = CheckValidTimeExt(txtCtrOpen(Index).Text)

    If strData = "ERROR" Then
        txtCtrOpen(Index).BackColor = &HC0C0FF
    Else
        txtCtrOpen(Index).BackColor = vbWindowBackground
    End If
    
    txtCtrOpen(Index).DataField = strData
    
    If Not IsChangesFromCode Then cmdOK.BackColor = vbRed
End Sub

Private Sub txtCtrOpen_KeyDown(Index As Integer, KeyCode As Integer, Shift As Integer)
    Call MoveFocusUpDown(txtCtrOpen, Index, KeyCode)
End Sub

Private Sub txtCtrOpen_KeyPress(Index As Integer, KeyAscii As Integer)
    Const VALID_NUMBERS = "0123456789+-:. "
    
    Select Case KeyAscii
        Case vbKeyBack
        Case Else
            If InStr(VALID_NUMBERS, Chr(KeyAscii)) <= 0 Then
                KeyAscii = 0
            End If
    End Select
End Sub

Private Sub txtDate_Change(Index As Integer)
    Dim strDate As String
    
    strDate = CheckValidDateExt(txtDate(Index).Text)
    If strDate = "ERROR" Then
        txtDate(Index).BackColor = &HC0C0FF
    Else
        txtDate(Index).BackColor = vbWindowBackground
    End If
    
    txtDate(Index).DataField = strDate
End Sub

Private Sub txtDate_KeyPress(Index As Integer, KeyAscii As Integer)
    Const VALID_CHARS = "0123456789-./ "
    
    Select Case KeyAscii
        Case vbKeyBack
        Case Else
            If InStr(VALID_CHARS, Chr(KeyAscii)) <= 0 Then
                KeyAscii = 0
            End If
    End Select
End Sub

Private Sub txtDate_Validate(Index As Integer, Cancel As Boolean)
    If txtDate(Index).DataField <> "ERROR" And txtDate(Index).DataField <> "NULL" Then
        txtDate(Index).Text = txtDate(Index).DataField
    End If
End Sub

Private Sub txtDays_Change()
    IsUserClickOnDailyCheckBox = False
    If Len(Trim(txtDays.Text)) = 7 Then
        chkDaily.Value = vbChecked
    Else
        chkDaily.Value = vbUnchecked
    End If
    IsUserClickOnDailyCheckBox = True
End Sub

Private Sub txtDays_KeyPress(KeyAscii As Integer)
    Const VALID_NUMBERS = "1234567"
    
    Select Case KeyAscii
        Case vbKeyBack
        Case Else
            If InStr(VALID_NUMBERS, Chr(KeyAscii)) <= 0 Then
                KeyAscii = 0
            Else
                If InStr(txtDays.Text, Chr(KeyAscii)) > 0 Then
                    If InStr(txtDays.SelText, Chr(KeyAscii)) <= 0 Then
                        KeyAscii = 0
                    End If
                End If
            End If
    End Select
End Sub

Private Sub txtGate_Change(Index As Integer)
    If Not IsChangesFromCode Then cmdOK.BackColor = vbRed
End Sub

Private Sub txtGate_KeyPress(Index As Integer, KeyAscii As Integer)
    KeyAscii = UCaseAsc(KeyAscii)
End Sub

Private Sub txtTer_Change(Index As Integer)
    If Not IsChangesFromCode Then cmdOK.BackColor = vbRed
End Sub

Private Sub txtTer_KeyDown(Index As Integer, KeyCode As Integer, Shift As Integer)
    Call MoveFocusUpDown(txtTer, Index, KeyCode)
End Sub

Private Sub txtTer_KeyPress(Index As Integer, KeyAscii As Integer)
    KeyAscii = UCaseAsc(KeyAscii)
End Sub

Private Sub txtTime_Change(Index As Integer)
    Dim strTime As String
    
    strTime = CheckValidTimeExt(txtTime(Index).Text)
    If strTime = "ERROR" Then
        txtTime(Index).BackColor = &HC0C0FF
    Else
        txtTime(Index).BackColor = vbWindowBackground
    End If
    
    txtTime(Index).DataField = strTime
End Sub

Private Sub txtTime_KeyPress(Index As Integer, KeyAscii As Integer)
    Const VALID_CHARS = "0123456789:. "
    
    Select Case KeyAscii
        Case vbKeyBack
        Case Else
            If InStr(VALID_CHARS, Chr(KeyAscii)) <= 0 Then
                KeyAscii = 0
            End If
    End Select
End Sub

Private Sub txtTime_Validate(Index As Integer, Cancel As Boolean)
    If txtTime(Index).DataField <> "ERROR" And txtTime(Index).DataField <> "NULL" Then
        txtTime(Index).Text = txtTime(Index).DataField
    End If
End Sub

Private Sub vsbDetails_Change()
    picDetails.Top = -(vsbDetails.Value * txtCtrNo(0).Height)
End Sub


