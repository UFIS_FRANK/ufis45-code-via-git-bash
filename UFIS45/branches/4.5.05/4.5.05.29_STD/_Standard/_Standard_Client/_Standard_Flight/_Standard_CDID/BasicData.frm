VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form BasicData 
   BorderStyle     =   0  'None
   Caption         =   "Common Data Input Dialog"
   ClientHeight    =   7605
   ClientLeft      =   0
   ClientTop       =   -105
   ClientWidth     =   7170
   Icon            =   "BasicData.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7605
   ScaleWidth      =   7170
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin MSComctlLib.ProgressBar pbrLoad 
      Height          =   135
      Left            =   0
      TabIndex        =   2
      Top             =   7200
      Width           =   7170
      _ExtentX        =   12647
      _ExtentY        =   238
      _Version        =   393216
      Appearance      =   1
   End
   Begin VB.Timer tmrUnload 
      Enabled         =   0   'False
      Interval        =   1000
      Left            =   7920
      Top             =   120
   End
   Begin VB.Frame fraStatus 
      BorderStyle     =   0  'None
      Enabled         =   0   'False
      Height          =   315
      Left            =   0
      TabIndex        =   0
      Top             =   7320
      Width           =   7605
      Begin VB.TextBox txtCedaStatus 
         Alignment       =   2  'Center
         BackColor       =   &H00008000&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   285
         Left            =   0
         Locked          =   -1  'True
         TabIndex        =   1
         Text            =   "CONNECTING"
         Top             =   0
         Width           =   7160
      End
   End
   Begin VB.Image imgUFIS 
      Height          =   855
      Left            =   30
      Picture         =   "BasicData.frx":058A
      Stretch         =   -1  'True
      Top             =   720
      Width           =   885
   End
   Begin VB.Image imgSystem 
      BorderStyle     =   1  'Fixed Single
      Height          =   7155
      Left            =   0
      Picture         =   "BasicData.frx":9EFC
      Top             =   0
      Width           =   7155
   End
End
Attribute VB_Name = "BasicData"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public ShowSplashScreen As Boolean
Public DataTables As String

Private IsActivated As Boolean

Private Function LoadBasicData() As Boolean
    Dim vntDataTables As Variant
    Dim vntDataTable As Variant
    Dim strDataDesc As String
        
    On Error GoTo ErrRead
    
    LoadBasicData = False
    
    vntDataTables = Split(DataTables, ",")
    
    pbrLoad.Max = UBound(vntDataTables) + 1
    pbrLoad.Min = 0
    pbrLoad.Value = 0
    pbrLoad.Visible = True
    
    For Each vntDataTable In vntDataTables
        strDataDesc = GetConfigEntry(colConfigs, "DATA_SOURCES", vntDataTable & "_DESC", "")
        
        txtCedaStatus.Text = "Loading " & strDataDesc & "..."
        txtCedaStatus.Refresh
        
        Call AddDataSource(MyMainForm.AppType, colDataSources, vntDataTable, colParameters)
        
        pbrLoad.Value = pbrLoad.Value + 1
    Next vntDataTable
    
    LoadBasicData = True
    
ErrRead:
End Function

Private Sub Form_Activate()
    If Not IsActivated Then
        DoEvents
            
        SetFormOnTop Me, True

        If DataTables <> "" Then
            Call LoadBasicData
            SetFormOnTop Me, False
            Me.Hide
        End If

        IsActivated = True
    End If
End Sub

Private Sub Form_Load()
    Dim nAdjustTop As Long

    IsActivated = False
    
    If ShowSplashScreen Then
        imgSystem.Visible = True
        imgUFIS.Visible = True
    Else
        imgSystem.Visible = False
        imgUFIS.Visible = False
    
        nAdjustTop = pbrLoad.Top
        pbrLoad.Top = pbrLoad.Top - nAdjustTop
        fraStatus.Top = fraStatus.Top - nAdjustTop
        
        txtCedaStatus.BackColor = DarkGray
        
        Me.Height = fraStatus.Top + fraStatus.Height
    End If
    If (ShowSplashScreen) And (DataTables = "") Then
        tmrUnload.Enabled = True
    End If
End Sub

Private Sub tmrUnload_Timer()
    SetFormOnTop Me, False
    Me.Hide
    tmrUnload.Enabled = False
End Sub


