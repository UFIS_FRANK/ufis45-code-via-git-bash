Attribute VB_Name = "MsgBoxExtended"
'PLACE CODE IN A STANDARD MODULE

Option Explicit

Public Enum ePosMsgBox
    eTopLeft
    eTopRight
    eTopCentre
    eBottomLeft
    eBottomRight
    eBottomCentre
    eCentreScreen
    eCentreDialog
End Enum

Private Type RECT
    Left As Long
    Top As Long
    Right As Long
    Bottom As Long
End Type

'Message API and constants
Private Declare Function UnhookWindowsHookEx Lib "user32" (ByVal zlhHook As Long) As Long
Private Declare Function GetWindowLong Lib "user32" Alias "GetWindowLongA" (ByVal hwnd As Long, ByVal nIndex As Long) As Long
Private Declare Function GetCurrentThreadId Lib "kernel32" () As Long
Private Declare Function SetWindowsHookEx Lib "user32" Alias "SetWindowsHookExA" (ByVal idHook As Long, ByVal lpfn As Long, ByVal hmod As Long, ByVal dwThreadId As Long) As Long
Private Declare Function SetWindowPos Lib "user32" (ByVal hwnd As Long, ByVal hWndInsertAfter As Long, ByVal x As Long, ByVal y As Long, ByVal cx As Long, ByVal cy As Long, ByVal wFlags As Long) As Long
Private Declare Function GetWindowRect Lib "user32" (ByVal hwnd As Long, lpRect As RECT) As Long
Private Const GWL_HINSTANCE = (-6)
Private Const SWP_NOSIZE = &H1
Private Const SWP_NOZORDER = &H4
Private Const SWP_NOACTIVATE = &H10
Private Const HCBT_ACTIVATE = 5
Private Const WH_CBT = 5

'Other APIs
Private Declare Function GetForegroundWindow Lib "user32" () As Long
Private Declare Function SystemParametersInfo Lib "user32" Alias "SystemParametersInfoA" (ByVal uAction As Long, ByVal uParam As Long, lpvParam As Any, ByVal fuWinIni As Long) As Long

' used for locating and changing the buttons
Public Declare Function FindWindowEx Lib "user32.dll" Alias "FindWindowExA" (ByVal hwndParent As Long, ByVal hwndChildAfter As Long, ByVal lpszClass As String, ByVal lpszWindow As String) As Long
Public Declare Function SetWindowText Lib "user32.dll" Alias "SetWindowTextA" (ByVal hwnd As Long, ByVal lpString As String) As Long
Public Declare Function GetClassName Lib "user32.dll" Alias "GetClassNameA" (ByVal hwnd As Long, ByVal lpClassName As String, ByVal nMaxCount As Long) As Long

Private zlhHook As Long
Private zePosition As ePosMsgBox
Private lAdjustedLeft As Long
Private lAdjustedTop As Long
Private aButtonCaptions As Variant

'Purpose   :    Displays a msgbox at a specified location on the screen
'Inputs    :    As per a standard MsgBox +
'               Position                An enumerated type which controls the screen position of the MsgBox
'Outputs   :    As per a standard Msgbox
'Notes     :

'Purpose   :    Displays a msgbox at a specified location on the screen
'Inputs    :    As per a standard MsgBox +
'               Position                An enumerated type which controls the screen position of the MsgBox
'Outputs   :    As per a standard Msgbox
'Notes     :    VB only, doesn't work in VBA

Function MsgBoxEx(Prompt As String, Optional Buttons As VbMsgBoxStyle, Optional Title, Optional HelpFile, Optional Context, Optional Position As ePosMsgBox = eCentreScreen, _
Optional ByVal AdjustedLeft As Long, Optional ByVal AdjustedTop As Long, Optional ByVal ButtonCaptions As Variant) As VbMsgBoxResult
    Dim lhInst As Long
    Dim lThread As Long
    Dim CaptionItem As Variant
    Dim i As Integer
    Dim ButtonCaption As Variant
    Dim strButtonCaptions As String

    'Set up the CBT hook
    lhInst = GetWindowLong(GetForegroundWindow, GWL_HINSTANCE)
    lThread = GetCurrentThreadId()
    zlhHook = SetWindowsHookEx(WH_CBT, AddressOf zWindowProc, lhInst, lThread)
    
    zePosition = Position
    lAdjustedLeft = AdjustedLeft
    lAdjustedTop = AdjustedTop
    
    If IsMissing(ButtonCaptions) Then
        aButtonCaptions = Null
    Else
        If VBA.IsArray(ButtonCaptions) Then
            strButtonCaptions = ""
            For i = 0 To 3
                If UBound(ButtonCaptions) >= i Then
                    strButtonCaptions = strButtonCaptions & "," & ButtonCaptions(i)
                Else
                    strButtonCaptions = strButtonCaptions & "," & Space(5)
                End If
            Next i
            strButtonCaptions = Mid(strButtonCaptions, 2)
            aButtonCaptions = Split(strButtonCaptions, ",")
        Else
            aButtonCaptions = Array(ButtonCaptions, Space(5), Space(5), Space(5))
        End If
    End If
    
    'Display the message box
    MsgBoxEx = MsgBox(Prompt, Buttons, Title, HelpFile, Context)
End Function

'Call back used by MsgboxEx
Private Function zWindowProc(ByVal lMsg As Long, ByVal wParam As Long, ByVal lParam As Long) As Long
    Dim tFormPos As RECT, tMsgBoxPos As RECT, tScreenWorkArea As RECT
    Dim lLeft As Long, lTop As Long
    Static sbRecursive As Boolean
    
    'Custom Button Caption
    Dim Btn(0 To 3) As Long
    Dim ButtonCount As Integer
    Dim T As Integer

    If lMsg = HCBT_ACTIVATE Then
        On Error Resume Next
        'A new dialog has been displayed
        tScreenWorkArea = ScreenWorkArea
        'Get the coordinates of the form and the message box so that
        'you can determine where the center of the form is located
        GetWindowRect GetForegroundWindow, tFormPos
        GetWindowRect wParam, tMsgBoxPos
        
        Select Case zePosition
        Case eCentreDialog
            lLeft = (tFormPos.Left + (tFormPos.Right - tFormPos.Left) / 2) - ((tMsgBoxPos.Right - tMsgBoxPos.Left) / 2)
            lTop = (tFormPos.Top + (tFormPos.Bottom - tFormPos.Top) / 2) - ((tMsgBoxPos.Bottom - tMsgBoxPos.Top) / 2)
        
        Case eCentreScreen
            lLeft = ((tScreenWorkArea.Right - tScreenWorkArea.Left) - (tMsgBoxPos.Right - tMsgBoxPos.Left)) / 2
            lTop = ((tScreenWorkArea.Bottom - tScreenWorkArea.Top) - (tMsgBoxPos.Bottom - tMsgBoxPos.Top)) / 2

        
        Case eTopLeft
            lLeft = tScreenWorkArea.Left
            lTop = tScreenWorkArea.Top
        
        Case eTopRight
            lLeft = tScreenWorkArea.Right - (tMsgBoxPos.Right - tMsgBoxPos.Left)
            lTop = tScreenWorkArea.Top
        
        Case eTopCentre
            lLeft = ((tScreenWorkArea.Right - tScreenWorkArea.Left) - (tMsgBoxPos.Right - tMsgBoxPos.Left)) / 2
            lTop = tScreenWorkArea.Top
        
        
        Case eBottomLeft
            lLeft = tScreenWorkArea.Left
            lTop = tScreenWorkArea.Bottom - (tMsgBoxPos.Bottom - tMsgBoxPos.Top)
        
        Case eBottomRight
            lLeft = tScreenWorkArea.Right - (tMsgBoxPos.Right - tMsgBoxPos.Left)
            lTop = tScreenWorkArea.Bottom - (tMsgBoxPos.Bottom - tMsgBoxPos.Top)
        
        Case eBottomCentre
            lLeft = ((tScreenWorkArea.Right - tScreenWorkArea.Left) - (tMsgBoxPos.Right - tMsgBoxPos.Left)) / 2
            lTop = tScreenWorkArea.Bottom - (tMsgBoxPos.Bottom - tMsgBoxPos.Top)
            
        End Select
        
        lLeft = lLeft + lAdjustedLeft
        lTop = lTop + lAdjustedTop
        
        If lLeft < 0 And sbRecursive = False Then
            'Left handside of Msgbox is off-screen - reposition in middle of screen
            sbRecursive = True
            zePosition = eCentreScreen
            zWindowProc HCBT_ACTIVATE, wParam, lParam
            sbRecursive = False
            Exit Function
        End If
        
        '---------------------
        'Custom Button Captions
        If Not IsNull(aButtonCaptions) Then
            Btn(0) = FindWindowEx(wParam, 0, vbNullString, vbNullString)
        
            Dim cName As String, Length As Long
            For T = 1 To 3
                Btn(T) = FindWindowEx(wParam, Btn(T - 1), vbNullString, vbNullString)
                ' no more windows found
                If Btn(T) = 0 Then Exit For
            Next T
            
            For T = 0 To 3
                If Btn(T) <> 0 And Btn(T) <> wParam Then
                    cName = Space(255)
                    Length = GetClassName(Btn(T), cName, 255)
                    cName = Left(cName, Length)
                    Debug.Print cName
                    If UCase(cName) = "BUTTON" Then
                        ' a button
                        SetWindowText Btn(T), aButtonCaptions(ButtonCount)
                        ButtonCount = ButtonCount + 1
                    End If
                End If
            Next T
        End If
        '--------------------

        'Position the msgbox
        SetWindowPos wParam, 0, lLeft, lTop, 10, 10, SWP_NOSIZE Or SWP_NOZORDER Or SWP_NOACTIVATE
        
        'Release the CBT hook
        UnhookWindowsHookEx zlhHook
    End If
    zWindowProc = False

End Function


'Purpose   :    Returns the screen dimensions, not including the tastbar
'Inputs    :    N/A
'Outputs   :    A type which defines the extent of the screen work area.
'Notes     :

Function ScreenWorkArea() As RECT
    Dim tScreen As RECT
    Dim lRet As Long
    Const SPI_GETWORKAREA = 48
    
    lRet = SystemParametersInfo(SPI_GETWORKAREA, vbNull, tScreen, 0)
    ScreenWorkArea = tScreen
End Function



