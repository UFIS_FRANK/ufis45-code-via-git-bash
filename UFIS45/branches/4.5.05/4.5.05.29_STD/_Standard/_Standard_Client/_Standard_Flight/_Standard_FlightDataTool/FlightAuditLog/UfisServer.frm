VERSION 5.00
Object = "{248DD890-BB45-11CF-9ABC-0080C7E7B78D}#1.0#0"; "MSWINSCK.ocx"
Object = "{A2F31E92-C74F-11D3-A251-00500437F607}#1.0#0"; "UfisCom.ocx"
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "TAB.ocx"
Object = "{6FBA474E-43AC-11CE-9A0E-00AA0062BB4C}#1.0#0"; "SYSINFO.ocx"
Object = "{7BDE1363-DF9A-4D96-A2A0-4E85E0191F0F}#1.0#0"; "AATLogin.ocx"
Begin VB.Form UfisServer 
   Caption         =   "Connection to UFIS Server (AODB)"
   ClientHeight    =   5715
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   14085
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   5715
   ScaleWidth      =   14085
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin AATLOGINLib.AatLogin LoginCall 
      Height          =   705
      Left            =   8580
      TabIndex        =   18
      Top             =   120
      Width           =   825
      _Version        =   65536
      _ExtentX        =   1455
      _ExtentY        =   1244
      _StockProps     =   0
   End
   Begin SysInfoLib.SysInfo SysInfo 
      Left            =   9540
      Top             =   120
      _ExtentX        =   1005
      _ExtentY        =   1005
      _Version        =   393216
   End
   Begin TABLib.TAB BasicData 
      Height          =   2505
      Index           =   0
      Left            =   5280
      TabIndex        =   11
      Top             =   450
      Width           =   2295
      _Version        =   65536
      _ExtentX        =   4048
      _ExtentY        =   4419
      _StockProps     =   64
   End
   Begin VB.TextBox TwsCode 
      Alignment       =   2  'Center
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   5670
      TabIndex        =   10
      ToolTipText     =   "Module Name"
      Top             =   0
      Width           =   1425
   End
   Begin VB.TextBox ModName 
      Alignment       =   2  'Center
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   4140
      TabIndex        =   9
      ToolTipText     =   "Module Name"
      Top             =   0
      Width           =   1425
   End
   Begin VB.TextBox HOPO 
      Alignment       =   2  'Center
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   2880
      TabIndex        =   8
      ToolTipText     =   "Home Airport (3LC)"
      Top             =   0
      Width           =   615
   End
   Begin VB.TextBox TblExt 
      Alignment       =   2  'Center
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   3510
      TabIndex        =   7
      ToolTipText     =   "CEDA Table Extension"
      Top             =   0
      Width           =   615
   End
   Begin VB.TextBox HostName 
      Alignment       =   2  'Center
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   0
      TabIndex        =   6
      ToolTipText     =   "Host Name"
      Top             =   0
      Width           =   1395
   End
   Begin VB.TextBox HostType 
      Alignment       =   2  'Center
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   1410
      TabIndex        =   5
      ToolTipText     =   "Host Type"
      Top             =   0
      Width           =   1395
   End
   Begin TABLib.TAB DataBuffer 
      Height          =   2475
      Index           =   0
      Left            =   30
      TabIndex        =   0
      Top             =   450
      Width           =   5025
      _Version        =   65536
      _ExtentX        =   8864
      _ExtentY        =   4366
      _StockProps     =   64
   End
   Begin MSWinsockLib.Winsock CdrhdlSock 
      Left            =   840
      Top             =   5220
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   393216
   End
   Begin UFISCOMLib.UfisCom aCeda 
      Left            =   120
      Top             =   5160
      _Version        =   65536
      _ExtentX        =   1085
      _ExtentY        =   873
      _StockProps     =   0
   End
   Begin TABLib.TAB DataBuffer 
      Height          =   1005
      Index           =   1
      Left            =   30
      TabIndex        =   1
      Top             =   2970
      Width           =   2085
      _Version        =   65536
      _ExtentX        =   3678
      _ExtentY        =   1773
      _StockProps     =   64
   End
   Begin TABLib.TAB DataBuffer 
      Height          =   1005
      Index           =   2
      Left            =   2250
      TabIndex        =   2
      Top             =   3000
      Width           =   2835
      _Version        =   65536
      _ExtentX        =   5001
      _ExtentY        =   1773
      _StockProps     =   64
   End
   Begin TABLib.TAB DataBuffer 
      Height          =   1665
      Index           =   3
      Left            =   30
      TabIndex        =   3
      Top             =   4020
      Width           =   2085
      _Version        =   65536
      _ExtentX        =   3678
      _ExtentY        =   2937
      _StockProps     =   64
   End
   Begin TABLib.TAB DataBuffer 
      Height          =   1635
      Index           =   4
      Left            =   2250
      TabIndex        =   4
      Top             =   4050
      Width           =   2835
      _Version        =   65536
      _ExtentX        =   5001
      _ExtentY        =   2884
      _StockProps     =   64
   End
   Begin TABLib.TAB BasicData 
      Height          =   2505
      Index           =   1
      Left            =   7710
      TabIndex        =   12
      Top             =   900
      Width           =   2295
      _Version        =   65536
      _ExtentX        =   4048
      _ExtentY        =   4419
      _StockProps     =   64
   End
   Begin TABLib.TAB BasicData 
      Height          =   2505
      Index           =   2
      Left            =   10110
      TabIndex        =   13
      Top             =   450
      Width           =   2295
      _Version        =   65536
      _ExtentX        =   4048
      _ExtentY        =   4419
      _StockProps     =   64
   End
   Begin TABLib.TAB BasicData 
      Height          =   2655
      Index           =   3
      Left            =   5280
      TabIndex        =   14
      Top             =   3000
      Width           =   2295
      _Version        =   65536
      _ExtentX        =   4048
      _ExtentY        =   4683
      _StockProps     =   64
   End
   Begin TABLib.TAB BasicData 
      Height          =   2655
      Index           =   4
      Left            =   7710
      TabIndex        =   15
      Top             =   3000
      Width           =   2295
      _Version        =   65536
      _ExtentX        =   4048
      _ExtentY        =   4683
      _StockProps     =   64
   End
   Begin TABLib.TAB BasicData 
      Height          =   2655
      Index           =   5
      Left            =   10110
      TabIndex        =   16
      Top             =   3000
      Width           =   2295
      _Version        =   65536
      _ExtentX        =   4048
      _ExtentY        =   4683
      _StockProps     =   64
   End
   Begin TABLib.TAB UrnoTab 
      Height          =   5205
      Left            =   12510
      TabIndex        =   17
      Top             =   450
      Width           =   1545
      _Version        =   65536
      _ExtentX        =   2725
      _ExtentY        =   9181
      _StockProps     =   64
   End
End
Attribute VB_Name = "UfisServer"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Const TlxPool = 1
Dim FipsAppMngIsConnected As Boolean
Dim NumberOfUrnos As Long
Dim ServerConfiguration As String
Dim CurTab As TABLib.Tab
Dim ShowFullTab As Boolean

Private Type BusyIndicatorType
    Left As Long
    Top As Long
    LineBusy As Variant
    LogOn As Variant
    LogOff As Variant
    NoLine As Variant
End Type
Private BusyIndicator As BusyIndicatorType
Private Initialized As Boolean
Private BcCook As Long
Private WithEvents bcProx As BcProxyLib.BcPrxy
Attribute bcProx.VB_VarHelpID = -1
Private WithEvents UFISAppMng As UFISAm
Attribute UFISAppMng.VB_VarHelpID = -1

Private Sub InitUfisServer()
'init
End Sub
Public Function GetComponentVersion(ObjName As String) As String
    Dim Result As String
    Dim tmpData As String
    On Error Resume Next
    Select Case UCase(ObjName)
        Case "BCPROXY"
            Result = bcProx.Version
            Result = Replace(Result, ",", ".", 1, -1, vbBinaryCompare)
            Result = Replace(Result, " ", "", 1, -1, vbBinaryCompare)
            tmpData = "N.A."
            tmpData = bcProx.BuildDate
            Result = Result & "," & tmpData & ",Application BroadCast Server"
            tmpData = "Application BroadCast Server"
            tmpData = bcProx.FileDescription
            Result = Result & "," & tmpData
        Case "UFISAPPMNG"
            Result = "1.1.0.0"
            Result = Replace(Result, ",", ".", 1, -1, vbBinaryCompare)
            Result = Replace(Result, " ", "", 1, -1, vbBinaryCompare)
            tmpData = "N.A."
            tmpData = UFISAppMng.BuildDate
            Result = Result & "," & tmpData & ",Intermodule Communication Server"
            tmpData = "Intermodule Communication Server"
            tmpData = UFISAppMng.FileDescription
            Result = Result & "," & tmpData
       Case Else
            Result = "N.A.,N.A.,N.A."
    End Select
    If Result = "" Then Result = "N.A.,N.A.,N.A."
    GetComponentVersion = Result
End Function
Public Function ConnectToCeda() As Boolean
    Dim sServer As String
    Dim sHopo As String
    Dim sTableExt As String
    Dim sConnectType As String
    Dim ret As Integer
    Dim ApplName As String
    Screen.MousePointer = 11
    ShowIndicator 2, False
    ShowIndicator 4, True
    aCeda.CleanupCom
    
    ShowIndicator 4, False
    ShowIndicator 3, True
    CedaIsConnected = False
    'sHopo = HOPO.txt
    'sTableExt = TblExt.Text
    ModName.Text = Left(App.Title, 10)
    sHopo = HOPO.Text
    sTableExt = TblExt.Text
    ApplName = ModName.Text
    aCeda.Tws = ""
    aCeda.Twe = ""
    ret = aCeda.SetCedaPerameters(ApplName, sHopo, sTableExt)
    ret = 0
    sServer = HostName.Text
    If sServer <> "LOCAL" Then
        sConnectType = "CEDA"
        ret = aCeda.InitCom(sServer, sConnectType)
        'MsgBox sServer
    End If
    If ret = 0 Then
        'Error: not connected
        ShowIndicator 2, True
    Else
        CedaIsConnected = True
    End If
    ShowIndicator 3, False
    Screen.MousePointer = 0
    ConnectToCeda = CedaIsConnected
End Function
Public Function DisconnectFromCeda() As Boolean
    ShowIndicator 4, True
    aCeda.CleanupCom
    CedaIsConnected = False
    DisconnectFromCeda = True
    ShowIndicator 4, False
End Function
Public Sub ConnectToBcProxy()
    On Error GoTo ErrHdl
    If CedaIsConnected Then
        Set bcProx = New BcPrxy
    End If
    Exit Sub
ErrHdl:
    MsgBox "BcProxy not registered!"
End Sub
Public Sub BcSpool(SetOnOff As Boolean)
    If SetOnOff Then bcProx.SetSpoolOn Else bcProx.SetSpoolOff
End Sub
Public Sub ConnectToApplMgr(DoConnect As Boolean)
    On Error GoTo ErrHdl
    FipsAppMngIsConnected = False
    If (CedaIsConnected = True) Or (DoConnect = True) Then
        FipsAppMngIsConnected = GetCommandLine()
        If DoConnect = True Then FipsAppMngIsConnected = True
        Set UFISAppMng = New UFISAm
        ' Must be done immediately after obtaining the pointer
        UFISAppMng.AssignAppTag TlxPool
    End If
    Exit Sub
ErrHdl:
    MsgBox "UfisAppMng not registered!"
End Sub
Public Function GetSystabData(TableCode As String, CloseConnex As Boolean) As String
    Dim GotData As Boolean
    Dim Result As String
    Dim FinaResult As String
    Dim FeleResult As String
    Dim TypeResult As String
    Dim FityResult As String
    Result = ""
    GotData = GetDataModel(TableCode, FinaResult, FeleResult, TypeResult, FityResult, CloseConnex)
    If GotData Then
        Result = Result + FinaResult + vbLf
        Result = Result + FeleResult + vbLf
        Result = Result + TypeResult + vbLf
        Result = Result + FityResult
    Else
    End If
    GetSystabData = Result
End Function
Public Function GetDataModel(TableCode As String, FinaResult As String, FeleResult As String, TypeResult As String, FityResult As String, CloseConnex As Boolean) As Boolean
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim TabLine As String
    FinaResult = ""
    FeleResult = ""
    TypeResult = ""
    FityResult = ""
    If Not CedaIsConnected Then ConnectToCeda
    If CedaIsConnected Then
        CallCeda CedaDataAnswer, "RTA", "SYSTAB", "FINA,FELE,TYPE,FITY", "", ("WHERE TANA='" & TableCode & "'"), "FINA", 0, False, False
        MaxLine = DataBuffer(0).GetLineCount - 1
        For CurLine = 0 To MaxLine
            FinaResult = FinaResult + Trim(DataBuffer(0).GetColumnValue(CurLine, 0)) + ","
            FeleResult = FeleResult + Trim(DataBuffer(0).GetColumnValue(CurLine, 1)) + ","
            TypeResult = TypeResult + Trim(DataBuffer(0).GetColumnValue(CurLine, 2)) + ","
            FityResult = FityResult + Trim(DataBuffer(0).GetColumnValue(CurLine, 3)) + ","
        Next
        If FinaResult <> "" Then
            FinaResult = Left(FinaResult, Len(FinaResult) - 1)
            FeleResult = Left(FeleResult, Len(FeleResult) - 1)
            TypeResult = Left(TypeResult, Len(TypeResult) - 1)
            FityResult = Left(FityResult, Len(FityResult) - 1)
        End If
    End If
    If CloseConnex Then DisconnectFromCeda
    If FinaResult <> "" Then GetDataModel = True Else GetDataModel = False
End Function
Public Function GetOneRecord(TableName As String, FieldList As String, UrnoValue As String, ResultData As String, CloseConnex As Boolean) As Boolean
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim TabLine As String
    Dim UseTable As String
    ResultData = ""
    UseTable = TableName
    If Not CedaIsConnected Then ConnectToCeda
    If CedaIsConnected Then
        CallCeda ResultData, "RTA", UseTable, FieldList, "", ("WHERE URNO=" & UrnoValue), "", 0, True, False
        MaxLine = DataBuffer(0).GetLineCount - 1
    End If
    If CloseConnex Then DisconnectFromCeda
    If ResultData <> "" Then GetOneRecord = True Else GetOneRecord = False
End Function

Public Function CallCeda(CedaResult As String, _
        RouterCmd As String, TableName As String, FieldList As String, DataList As String, _
        WhereClause As String, OrderKey As String, UseBuffer As Integer, _
        FullAnswer As Boolean, AutoRequest As Boolean) As Integer
    Dim RetCode As Integer
    Dim Result As String
    Dim tmpCmd As String
    Dim tmpTable As String
    Dim tmpFields As String
    Dim tmpData As String
    Dim tmpSqlKey As String
    Dim CedaAnswer As String
    Dim LineCount As Long
    Dim CurRec As Long
    Dim CurLine As String
    Dim ReqHint As String
    RetCode = CC_RC_NOT_CONNECTED
    ReqHint = "Server: " & HostName
    tmpCmd = RouterCmd
    tmpTable = TableName
    tmpFields = FieldList
    tmpData = CleanNullValues(DataList)
    tmpSqlKey = WhereClause
    'MsgBox tmpCmd & vbNewLine & tmpTable & vbNewLine & tmpFields & vbNewLine & tmpSqlKey & vbNewLine & tmpData
    'TelexPoolHead.Refresh
    If CedaIsConnected Then
        'If Not Initialized Then InitUfisServer
        'If Initialized Then
            If OrderKey <> "" Then tmpSqlKey = tmpSqlKey & " ORDER BY " & OrderKey
            If (UseBuffer >= 0) And (UseBuffer < DataBuffer.Count) Then
                ShowIndicator 1, True
                DataBuffer(UseBuffer).ResetContent
                aCeda.ClearDataBuffer
                'Open "c:\tmp\ufiscom.log" For Append As #3
                'Print #3, "---- SEND TO CEDA ----"
                'Print #3, "CMD: "; tmpCmd
                'Print #3, "TBL: "; tmpTable
                'Print #3, "FLD: "; tmpFields
                'Print #3, "SQL: "; tmpSqlKey
                'Print #3, "DAT: "; tmpData
                'Close #3
                'MsgBox aCeda.Tws
                'CedaAnswer = aCeda.Ufis(tmpCmd, tmpTable, tmpFields, tmpSqlKey, tmpData)
                aCeda.HomeAirport = HOPO.Text
                aCeda.TableExt = TblExt.Text
                aCeda.Module = ModName.Text & "," & Me.GetApplVersion(True)
                aCeda.Twe = ""
                aCeda.Tws = TwsCode.Text
                aCeda.UserName = ModName.Text
                CedaAnswer = aCeda.CallServer(tmpCmd, tmpTable, tmpFields, tmpData, tmpSqlKey, "")
                
                'MsgBox CedaAnswer
                'What's the answer ???
                'If CedaAnswer = "OK" Then
                    LineCount = aCeda.GetBufferCount - 1
                    'Open "c:\tmp\ufiscom.log" For Append As #3
                    'Print #3, "---- ANSWER ----"
                    'Print #3, "CNT: "; Str(LineCount + 1)
                    'Print #3, "---- Values ----"
                    If LineCount >= 0 Then
                        DataBuffer(UseBuffer).HeaderString = tmpFields
                        'DataBuffer(UseBuffer).HeaderLengthString = CreateLengthList(tmpFields)
                        For CurRec = 0 To LineCount
                            CurLine = aCeda.GetBufferLine(CurRec)
                            'Print #3, Str(CurRec); ": "; CurLine
                            'MainDialog.dbgTrace = CurLine & ","
                            DataBuffer(UseBuffer).InsertTextLine CurLine, False
                        Next
                        Result = "BUFFER" & UseBuffer & ",OK"
                        If FullAnswer Then
                            Result = ""
                            For CurRec = 0 To LineCount
                                Result = Result & DataBuffer(UseBuffer).GetLineValues(CurRec) & vbNewLine
                            Next
                            Result = Left(Result, Len(Result) - 2)  'Cut the last vbNewLine
                            DataBuffer(UseBuffer).ResetContent
                        End If
                        RetCode = 0
                    Else
                        ReqHint = WhereClause
                        RetCode = CC_RC_NOT_FOUND
                    End If
                    'Print #3, "----------------"
                    'Close #3
                'Else
                    'Ceda failed
                    'Result = "???"
                'End If
                ShowIndicator 1, False
            Else
                ReqHint = "ERROR 1005: Invalid Data Buffer. (" & UseBuffer & ")"
                RetCode = CC_RC_WRONG_BUFFER
            End If
        'Else
            'RetCode = CC_RC_LOST_CONNECTION
        'End If
    End If
    If RetCode <> 0 Then Result = AutoDialog(RetCode, AutoRequest, ReqHint)
    CedaResult = Result
    CallCeda = RetCode
End Function

Private Function AutoDialog(RetCode As Integer, AutoRequest As Boolean, HintText As String) As String
Dim PopUpWin As Boolean
Dim ReqText As String
Dim ReqHint As String
Dim ReqTip As String
Dim ReqButton As String
Dim ReqIcon As String
Dim MyAnswer As String
    UserAnswer = ""
    PopUpWin = True
    Select Case RetCode
        Case -3
            ReqText = "Action rejected by server:" & vbNewLine & HintText
            ReqHint = "Configured Server Name: " & HostName
            ReqTip = "Server: " & HostName
            ReqButton = ""
            ReqIcon = "stop"
            MyAnswer = "ERROR 1003: " & ReqText & "(" & HostName & ") "
        Case CC_RC_LOST_CONNECTION
            ReqText = "Lost connection."
            ReqHint = "Configured Server Name: " & HostName
            ReqTip = "Server: " & HostName
            ReqButton = ""
            ReqIcon = "netfail"
            MyAnswer = "ERROR 1002: " & ReqText & "(" & HostName & ") "
        Case CC_RC_NOT_CONNECTED
            ReqText = "Not connected !"
            ReqHint = "Configured Server Name: " & HostName
            ReqTip = "Server: " & HostName
            ReqButton = ""
            ReqIcon = "netfail"
            MyAnswer = "MSG 1001: " & ReqText & " (Server:" & HostName & ") "
        Case CC_RC_NOT_FOUND
            PopUpWin = AutoRequest
            ReqText = "No data found for this view or filter."
            ReqHint = HintText
            ReqTip = "SQL WhereClause ?"
            ReqButton = ""
            ReqIcon = "hand"
            MyAnswer = ReqText
        Case Else
            ReqText = HintText
            ReqButton = ""
            ReqIcon = "stop"
            MyAnswer = ReqText
    End Select
    If PopUpWin Then
        MyMsgBox.InfoApi 0, ReqHint, ReqTip
        If MyMsgBox.CallAskUser(0, 0, 0, "Server Control", ReqText, ReqIcon, ReqButton, UserAnswer) > 0 Then DoNothing
    End If
    AutoDialog = MyAnswer & vbNewLine & UserAnswer
End Function

Public Function ResetBuffer(UseBuffer As Integer) As Boolean
    If (UseBuffer >= 0) And (UseBuffer < DataBuffer.Count) Then
        DataBuffer(UseBuffer).ResetContent
        DataBuffer(UseBuffer).SetHeaderText " "
        ResetBuffer = True
    Else
        ResetBuffer = False
    End If
End Function

Public Function GetMyWorkStationName() As String
    GetMyWorkStationName = UfisServer.CdrhdlSock.LocalHostName
End Function

Private Sub BasicData_SendLButtonDblClick(Index As Integer, ByVal LineNo As Long, ByVal ColNo As Long)
    Dim NewVal As Long
    Dim NewTag As String
    If LineNo < 0 Then
        If BasicData(Index).Tag = "" Then
            NewTag = ""
            NewTag = NewTag & CStr(BasicData(Index).Left) & ","
            NewTag = NewTag & CStr(BasicData(Index).Top) & ","
            NewTag = NewTag & CStr(BasicData(Index).Width) & ","
            NewTag = NewTag & CStr(BasicData(Index).Height) & ","
            BasicData(Index).Tag = NewTag
            BasicData(Index).Top = DataBuffer(0).Top
            BasicData(Index).Left = DataBuffer(0).Left
            BasicData(Index).ZOrder
            Set CurTab = BasicData(Index)
            ShowFullTab = True
            Form_Resize
            'StatusBar1.ZOrder
        Else
            NewTag = BasicData(Index).Tag
            BasicData(Index).Left = Val(GetItem(NewTag, 1, ","))
            BasicData(Index).Top = Val(GetItem(NewTag, 2, ","))
            BasicData(Index).Width = Val(GetItem(NewTag, 3, ","))
            BasicData(Index).Height = Val(GetItem(NewTag, 4, ","))
            'StatusBar1.ZOrder
            ShowFullTab = False
            BasicData(Index).Tag = ""
        End If
    End If
End Sub

Private Sub bcProx_OnBcReceive(ByVal ReqId As String, ByVal DestName As String, ByVal RecvName As String, ByVal CedaCmd As String, ByVal ObjName As String, ByVal Seq As String, ByVal Tws As String, ByVal Twe As String, ByVal CedaSqlKey As String, ByVal Fields As String, ByVal Data As String, ByVal BcNum As String)
    HandleBroadCast ReqId, DestName, RecvName, CedaCmd, ObjName, Seq, Tws, Twe, CedaSqlKey, Fields, Data, BcNum
End Sub
Public Sub GetNextBcFromSpool(GetAll As Boolean)
    Dim ReqId As String
    Dim DestName As String
    Dim RecvName As String
    Dim CedaCmd As String
    Dim ObjName As String
    Dim Seq As String
    Dim Tws As String
    Dim Twe As String
    Dim CedaSqlKey As String
    Dim Fields As String
    Dim Data As String
    Dim BcNum As String
    Do
        bcProx.GetNextBufferdBC ReqId, DestName, RecvName, CedaCmd, ObjName, Seq, Tws, Twe, CedaSqlKey, Fields, Data, BcNum
        If BcNum <> "" Then HandleBroadCast ReqId, DestName, RecvName, CedaCmd, ObjName, Seq, Tws, Twe, CedaSqlKey, Fields, Data, BcNum
    Loop While (GetAll) And (BcNum <> "")
End Sub

Private Sub Form_Load()
    CedaIsConnected = False
    Initialized = False
    InitMyForm
End Sub

Private Sub InitMyForm()
    HOPO.Text = GetIniEntry("", "GLOBAL", "", "HOMEAIRPORT", "???")
    TblExt.Text = GetIniEntry("", "GLOBAL", "", "TABLEEXTENSION", "TAB")
    HostName.Text = GetIniEntry("", "GLOBAL", "", "HOSTNAME", "")
    HostType.Text = GetIniEntry("", "GLOBAL", "", "HOSTTYPE", "UNKNOWN")
    HomeAirport = HOPO.Text
    ModName.Text = App.EXEName
    TwsCode.Text = ""
    UrnoPoolInit 1
End Sub

Public Sub ShowIndicator(Index As Integer, SetVisible As Boolean)
Dim Obj As Variant
'Yes.
'I'm sure that there is surely an easier way but
'I'm not a VB Guru yet
    With BusyIndicator
        Select Case Index
            Case 1
                If .LineBusy <> Empty Then Set Obj = .LineBusy
            Case 2
                If .NoLine <> Empty Then Set Obj = .NoLine
            Case 3
                If .LogOn <> Empty Then Set Obj = .LogOn
            Case 4
                If .LogOff <> Empty Then Set Obj = .LogOff
        End Select
        If Obj <> Empty Then
            Obj.Left = .Left
            Obj.Top = .Top
            If SetVisible = True And Index = 1 Then
                If Obj.BackColor <> DarkGreen Then Obj.BackColor = DarkGreen Else Obj.BackColor = DarkestGreen
            End If
            Obj.Visible = SetVisible
            If SetVisible Then
                Obj.ZOrder
                Obj.Refresh
            End If
        End If
    End With
End Sub

Public Sub SetIndicator(PosLeft As Long, PosTop As Long, _
                        LineBusyFlag As Variant, NoLineFlag As Variant, _
                        LogOnFlag As Variant, LogOffFlag As Variant)
    With BusyIndicator
        .Left = PosLeft
        .Top = PosTop
        If LineBusyFlag <> Empty Then Set .LineBusy = LineBusyFlag
        If NoLineFlag <> Empty Then Set .NoLine = NoLineFlag
        If LogOnFlag <> Empty Then Set .LogOn = LogOnFlag
        If LogOffFlag <> Empty Then Set .LogOff = LogOffFlag
    End With
End Sub

Private Sub Form_Resize()
    Dim NewVal As Long
    If ShowFullTab Then
        NewVal = Me.ScaleWidth - (CurTab.Left * 2)
        If NewVal > 300 Then CurTab.Width = NewVal
        NewVal = Me.ScaleHeight - CurTab.Top '- StatusBar1.Height
        If NewVal > 300 Then CurTab.Height = NewVal
        CurTab.ZOrder
    End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
    If FipsAppMngIsConnected Then
        UFISAppMng.DetachApp TlxPool
    End If
    aCeda.CleanupCom
End Sub

Function GetCommandLine(Optional MaxArgs)
   'Declare variables.
   Dim C, CmdLine, CmdLnLen, InArg, i, NumArgs
   Const CONNECTED_ARG = "/CONNECTED"
   'See if MaxArgs was provided.
   If IsMissing(MaxArgs) Then MaxArgs = 1
   'Make array of the correct size.
   ReDim argArray(MaxArgs)
   NumArgs = 0: InArg = False
   'Get command line arguments.
   CmdLine = Command()
   CmdLnLen = Len(CmdLine)
   'Go thru command line one character
   'at a time.
   If CmdLnLen = 0 Then
      GetCommandLine = False
      Exit Function
   End If
   For i = 1 To CmdLnLen
      C = Mid(CmdLine, i, 1)
      'Test for space or tab.
      If (C <> " " And C <> vbTab) Then
         'Neither space nor tab.
         'Test if already in argument.
         If Not InArg Then
         'New argument begins.
         'Test for too many arguments.
            If NumArgs = MaxArgs Then Exit For
            NumArgs = NumArgs + 1
            InArg = True
         End If
         'Concatenate character to current argument.
         argArray(NumArgs) = argArray(NumArgs) & C
      Else
         'Found a space or tab.
         'Set InArg flag to False.
         InArg = False
      End If
   Next i
   'Resize array just enough to hold arguments.
   ReDim Preserve argArray(NumArgs)
   'Return Array in Function name.
   If argArray(1) = CONNECTED_ARG Then
    GetCommandLine = True
   Else
    GetCommandLine = False
   End If
End Function

Public Sub SendMessageToFips(MsgData As String)
    If FipsAppMngIsConnected Then
        UFISAppMng.TransferData 1, 1, MsgData
    Else
        If MyMsgBox.CallAskUser(0, 0, 0, "Appl Manager Control", "Not connected to FIPS (Via UfisAppMngr)", "hand", "", UserAnswer) > 0 Then DoNothing
    End If
End Sub

Private Sub UFISAppMng_ForwardData(ByVal Orig As Long, ByVal Data As String)
    If Orig = 2 Then
        MsgFromUFISAppManager Orig, Data
        If Data <> "" Then FipsAppMngIsConnected = True
    End If
End Sub

Private Function CreateLengthList(FieldList As String) As String
    Dim ItmCnt As Integer
    Dim ItmDat As String
    Dim Result As String
    ItmDat = "START"
    ItmCnt = 0
    Result = "32"
    While ItmDat <> ""
        ItmCnt = ItmCnt + 1
        ItmDat = GetItem(FieldList, ItmCnt, ",")
        If ItmDat <> "" Then
            Result = Result + ",32"
        End If
    Wend
    CreateLengthList = Result
End Function

Public Function GetServerConfig(ServerConfig As String) As Boolean
    Dim CloseConnex As Boolean
    ServerConfig = ""
    CloseConnex = False
    If Not CedaIsConnected Then
        ConnectToCeda
        CloseConnex = True
    End If
    If CedaIsConnected Then CallCeda ServerConfig, "GFR", "", "", "", "[CONFIG]", "", 0, True, False
    If CloseConnex Then DisconnectFromCeda
    If ServerConfig <> "" Then GetServerConfig = True Else GetServerConfig = False
End Function

Public Sub SetUtcTimeDiff()
    Dim UtcInfo As String
    GetServerConfig ServerConfiguration
    UtcInfo = GetServerConfigItem("UTCD", 1, "")
    If UtcInfo <> "" Then
        UtcTimeDiff = Val(UtcInfo)
    Else
        'MsgBox "Couldn't determine Utc Diff."
    End If
End Sub

Public Sub BasicDataInit(Index As Integer, SortCol As Long, TableName As String, Fields As String, SqlKey As String)
    BasicData(Index).ResetContent
    BasicData(Index).HeaderString = Fields
    BasicData(Index).HeaderLengthString = CreateLengthList(Fields)
    BasicData(Index).AutoSizeByHeader = True
    BasicData(Index).ShowHorzScroller True
    BasicData(Index).CedaCurrentApplication = ModName & "," & UfisServer.GetApplVersion(True)
    BasicData(Index).CedaHopo = HOPO
    BasicData(Index).CedaIdentifier = "IDX"
    BasicData(Index).CedaPort = "3357"
    BasicData(Index).CedaReceiveTimeout = "250"
    BasicData(Index).CedaRecordSeparator = vbLf
    BasicData(Index).CedaSendTimeout = "250"
    BasicData(Index).CedaServerName = HostName
    BasicData(Index).CedaTabext = TblExt
    BasicData(Index).CedaUser = ModName
    BasicData(Index).CedaWorkstation = GetMyWorkStationName
    If CedaIsConnected Then
        BasicData(Index).CedaAction "RTA", TableName, Fields, "", SqlKey
        If HostName.Tag = "TEST" Then
            'If HostName.Text <> "LOCAL" Then BasicData(Index).WriteToFile "c:\tmp\TlxBasicData" & CStr(Index) & ".txt", False
            If HostName.Text <> "LOCAL" Then BasicData(Index).WriteToFile UFIS_TMP & "\TlxBasicData" & CStr(Index) & ".txt", False
        End If
    Else
        If HostName.Tag = "TEST" Then
            'If HostName.Text = "LOCAL" Then BasicData(Index).ReadFromFile "c:\tmp\TlxBasicData" & CStr(Index) & ".txt"
            If HostName.Text = "LOCAL" Then BasicData(Index).ReadFromFile UFIS_TMP & "\TlxBasicData" & CStr(Index) & ".txt"
        End If
    End If
    BasicData(Index).Sort SortCol, True, True
    'BasicData(Index).AutoSizeColumns
    'BasicData(Index).Tag = "OK"
    BasicData(Index).RedrawTab
End Sub

Public Sub BasicDataSort(Index As Integer, SortCol As Long)
    BasicData(Index).Sort SortCol, True, True
End Sub

Public Function BasicLookUp(Index As Integer, LookColNo As Long, GetColNo As Long, LookValue As String, LookMethod As Integer, FirstHit As Boolean) As String
    Dim Result As String
    Dim HitLineNoList As String
    Dim CurValue As String
    Dim CurListItem As String
    Dim CurItemNo As Long
    Dim CurLine As Long
    If FirstHit Then
        'BasicData(Index).SetCurrentSelection -1
        'HitLineNoList = BasicData(Index).GetNextLineByColumnValue(LookColNo, LookValue, LookMethod)
        HitLineNoList = BasicData(Index).GetLinesByColumnValue(LookColNo, LookValue, LookMethod)
        If HitLineNoList <> "" Then
            CurLine = Val(GetRealItem(HitLineNoList, 0, ","))
            Result = BasicData(Index).GetColumnValue(CurLine, GetColNo)
        End If
    Else
        Result = ""
        HitLineNoList = BasicData(Index).GetLinesByColumnValue(LookColNo, LookValue, LookMethod)
        If HitLineNoList <> "" Then
            CurListItem = "START"
            CurItemNo = 0
            While CurListItem <> ""
                CurListItem = GetRealItem(HitLineNoList, CurItemNo, ",")
                If CurListItem <> "" Then
                    CurLine = Val(CurListItem)
                    CurValue = BasicData(Index).GetColumnValue(CurLine, GetColNo)
                    Result = Result & vbLf & CurValue
                End If
                CurItemNo = CurItemNo + 1
            Wend
            Result = Mid(Result, 2)
        End If
    End If
    BasicLookUp = Result
End Function

Public Sub UrnoPoolInit(UrnoPackages As Long)
    UrnoTab.ResetContent
    UrnoTab.ShowHorzScroller False
    UrnoTab.ShowVertScroller True
    UrnoTab.VScrollMaster = False
    UrnoTab.FontName = "Courier New"
    UrnoTab.SetTabFontBold True
    UrnoTab.HeaderFontSize = 17
    UrnoTab.FontSize = 17
    UrnoTab.LineHeight = 17
    UrnoTab.HeaderString = "URNO"
    UrnoTab.ColumnWidthString = "10"
    UrnoTab.HeaderLengthString = "70"
    UrnoTab.ResetContent
    NumberOfUrnos = UrnoPackages
    If NumberOfUrnos > 500 Then NumberOfUrnos = 500
End Sub

Public Function UrnoPoolPrepare(NeededUrnos As Long) As Long
    Dim UrnoList As String
    Dim UrnoCount As Long
    If CedaIsConnected Then
        If UrnoTab.GetLineCount < NeededUrnos Then
            UrnoCount = NeededUrnos - UrnoTab.GetLineCount
            If UrnoCount > 500 Then
                UrnoCount = 500
                NumberOfUrnos = 500
            End If
            If UrnoCount < 1 Then UrnoCount = 1
            CallCeda UrnoList, "GMU", "", "*", Trim(Str(UrnoCount)), "", "", 0, True, False
            UrnoList = Replace(UrnoList, ",", vbLf, 1, -1, vbBinaryCompare)
            UrnoTab.InsertBuffer UrnoList, vbLf
        End If
        UrnoPoolPrepare = UrnoTab.GetLineCount
    Else
        NumberOfUrnos = NeededUrnos
        UrnoPoolPrepare = NumberOfUrnos
    End If
    'UfisServer.UrnoTab.AutoSizeColumns
End Function

Public Function UrnoPoolGetNext() As String
    Dim UrnoList As String
    If CedaIsConnected Then
        If UrnoTab.GetLineCount < 1 Then
            If NumberOfUrnos < 1 Then NumberOfUrnos = 1
            CallCeda UrnoList, "GMU", "", "*", CStr(NumberOfUrnos), "", "", 0, True, False
            UrnoTab.InsertBuffer UrnoList, ","
        End If
        UrnoPoolGetNext = UrnoTab.GetColumnValue(0, 0)
        UrnoTab.DeleteLine 0
    Else
        UrnoPoolGetNext = CStr(NumberOfUrnos)
        NumberOfUrnos = NumberOfUrnos + 1
    End If
    'UrnoTab.AutoSizeColumns
End Function

Public Function GetServerConfigItem(KeyCode As String, ItemNbr As Integer, UseDefault As String) As String
    Dim Result As String
    Dim UseCfg As String
    Dim UseKey As String
    UseCfg = vbLf & ServerConfiguration
    UseKey = vbLf & KeyCode & ","
    Result = GetItem(UseCfg, 2, UseKey)
    Result = GetItem(Result, 1, vbLf)
    If ItemNbr > 0 Then Result = GetItem(Result, ItemNbr, ",")
    If Right(Result, 1) = vbLf Then Result = Left(Result, Len(Result) - 1)
    If Right(Result, 1) = vbCr Then Result = Left(Result, Len(Result) - 1)
    If Right(Result, 1) = vbLf Then Result = Left(Result, Len(Result) - 1)
    If Result = "" Then Result = UseDefault
    GetServerConfigItem = Result
End Function

Public Sub LoginApplication(WithDialog As Boolean)
    'Dim LoginAnsw As String
    'Set LoginCall.UfisComCtrl = UfisServer.aCeda
    'LoginCall.ApplicationName = "ImportTool"
    'LoginAnsw = LoginCall.DoLoginSilentMode("UFIS$ADMIN", "Passwort")
    
    'LoginCall.InfoButtonVisible = True
    'LoginCall.InfoAAT = "Hallo"
    'LoginCall.InfoCaption = "Import Tool"
    'LoginCall.RegisterApplicationString = "ImportTool"
    'LoginCall.ShowLoginDialog
    'LoginAnsw = LoginCall.GetPrivileges("Hallo sind das hier die Privileges?")
    'LoginAnsw = LoginCall.GetUserName
    'MsgBox LoginAnsw
End Sub
Public Function GetApplVersion(ShowExtended As Boolean) As String
    Dim tmpTxt As String
    Dim tmpMajor As String
    Dim tmpMinor As String
    Dim tmpRevis As String
    tmpMajor = App.Major
    tmpMinor = App.Minor
    tmpRevis = App.Revision
    tmpTxt = ""
    Select Case SysInfo.OSPlatform
        Case 0  'Win32s
        Case 1  'Win 95/98
        Case 2  'Win NT
            If SysInfo.OSVersion > 4 Then
                'Win 2000
                If ShowExtended Then tmpRevis = "0." & tmpRevis
            Else
                'Win NT 4
                If ShowExtended Then
                    tmpMinor = Right("0000" & tmpMinor, 2)
                    tmpRevis = Right("0000" & tmpRevis, 4)
                End If
            End If
        Case Else
    End Select
    tmpTxt = tmpTxt & tmpMajor & "." & tmpMinor & "." & tmpRevis
    GetApplVersion = tmpTxt
End Function

