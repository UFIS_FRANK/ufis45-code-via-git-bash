VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{6BF52A50-394A-11D3-B153-00C04F79FAA6}#1.0#0"; "wmp.dll"
Begin VB.Form TestForm 
   BackColor       =   &H00E0E0E0&
   Caption         =   "UFIS Document Reader (PDF)"
   ClientHeight    =   4155
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   5880
   Icon            =   "TestForm.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   4155
   ScaleWidth      =   5880
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton Command2 
      Caption         =   "Stop"
      Height          =   315
      Left            =   1440
      TabIndex        =   3
      Top             =   0
      Width           =   1425
   End
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   315
      Left            =   0
      TabIndex        =   1
      Top             =   3840
      Width           =   5880
      _ExtentX        =   10372
      _ExtentY        =   556
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   2
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   7276
         EndProperty
      EndProperty
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Start"
      Height          =   315
      Index           =   0
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   1335
   End
   Begin WMPLibCtl.WindowsMediaPlayer WindowsMediaPlayer1 
      Height          =   2865
      Left            =   30
      TabIndex        =   2
      Top             =   360
      Width           =   5235
      URL             =   ""
      rate            =   1
      balance         =   0
      currentPosition =   0
      defaultFrame    =   ""
      playCount       =   1
      autoStart       =   -1  'True
      currentMarker   =   0
      invokeURLs      =   -1  'True
      baseURL         =   ""
      volume          =   50
      mute            =   0   'False
      uiMode          =   "full"
      stretchToFit    =   0   'False
      windowlessVideo =   0   'False
      enabled         =   -1  'True
      enableContextMenu=   -1  'True
      fullScreen      =   0   'False
      SAMIStyle       =   ""
      SAMILang        =   ""
      SAMIFilename    =   ""
      captioningID    =   ""
      enableErrorDialogs=   0   'False
      _cx             =   9234
      _cy             =   5054
   End
End
Attribute VB_Name = "TestForm"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim IsActive As Boolean

Private Sub Command1_Click(Index As Integer)
    Dim DocName As String
    Dim DocPath As String
    Dim FullPath As String
    'DocName = "epaperA4_folder02.pdf"
    DocName = "Aircraft Switching.pdf"
    DocPath = "D:\Ufis\System\FdtPdf"
    FullPath = DocPath & "\" & DocName
    
    WindowsMediaPlayer1.URL = "d:\UfisOldLaptop\System\FlightDataTool\UFIS_promo_short256K_Stream001.wmv"
    'AcroPDF1.LoadFile FullPath
    
End Sub

Private Sub Command2_Click()
    WindowsMediaPlayer1.URL = ""
End Sub

Private Sub Form_Activate()
    If Not IsActive Then
        IsActive = True
        Command1_Click 0
    End If
End Sub

Private Sub Form_Resize()
    Dim NewWidth As Long
    Dim NewTop As Long
    Dim NewHeight As Long
    Dim NewLeft As Long
    NewTop = 360
    NewLeft = 360
    NewWidth = Me.ScaleWidth - (NewLeft * 2)
    NewHeight = Me.ScaleHeight - NewTop - StatusBar1.Height '- 60
    WindowsMediaPlayer1.Move NewLeft, NewTop, NewWidth, NewHeight
    'AcroPDF1.Move NewLeft, NewTop, NewWidth, NewHeight
End Sub

Private Sub Form_Unload(Cancel As Integer)
IsActive = False
End Sub
