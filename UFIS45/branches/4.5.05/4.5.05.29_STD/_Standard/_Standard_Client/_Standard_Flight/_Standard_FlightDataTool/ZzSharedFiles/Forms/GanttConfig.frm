VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "Comdlg32.ocx"
Object = "{6782E133-3223-11D4-996A-0000863DE95C}#1.1#0"; "UGantt.ocx"
Begin VB.Form GanttConfig 
   Caption         =   "FormGantt"
   ClientHeight    =   8070
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   13410
   Icon            =   "GanttConfig.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   8070
   ScaleWidth      =   13410
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton btnUTCOffsetInHours 
      Caption         =   "UTCOffsetInHours"
      Height          =   285
      Left            =   105
      TabIndex        =   35
      Top             =   7320
      Width           =   1635
   End
   Begin VB.TextBox txtUTCOffsetInHours 
      Height          =   285
      Left            =   1890
      TabIndex        =   34
      Text            =   "2"
      Top             =   7320
      Width           =   750
   End
   Begin VB.CheckBox chkSelectedBarToLineBottom 
      Caption         =   "SelectedBarToLineBottom"
      Enabled         =   0   'False
      Height          =   255
      Left            =   3615
      TabIndex        =   33
      Top             =   7680
      Width           =   2250
   End
   Begin VB.CheckBox chkEnableMoveBar 
      Caption         =   "EnableMoveBar"
      Height          =   255
      Left            =   3615
      TabIndex        =   32
      Top             =   5850
      Value           =   1  'Checked
      Width           =   2250
   End
   Begin VB.CheckBox chkEnableMoveBkBar 
      Caption         =   "EnableMoveBkBar"
      Height          =   255
      Left            =   3615
      TabIndex        =   31
      Top             =   6075
      Value           =   1  'Checked
      Width           =   2250
   End
   Begin VB.CheckBox chkShowDataHolding 
      Caption         =   "ShowDataHolding"
      Height          =   255
      Left            =   3615
      TabIndex        =   30
      Top             =   7455
      Width           =   2250
   End
   Begin VB.TextBox txtFireBackBarLM 
      Appearance      =   0  'Flat
      BackColor       =   &H80000000&
      BorderStyle     =   0  'None
      Height          =   1845
      Left            =   8625
      MultiLine       =   -1  'True
      TabIndex        =   29
      Top             =   5745
      Width           =   2415
   End
   Begin VB.TextBox txtFireBarLM 
      Appearance      =   0  'Flat
      BackColor       =   &H80000000&
      BorderStyle     =   0  'None
      Height          =   1845
      Left            =   6000
      MultiLine       =   -1  'True
      TabIndex        =   28
      Top             =   5745
      Width           =   2535
   End
   Begin VB.CheckBox chkAutoScroll 
      Caption         =   "AutoScroll"
      Height          =   255
      Left            =   3615
      TabIndex        =   27
      Top             =   7230
      Width           =   2250
   End
   Begin VB.CheckBox chkOverlappingRightBarTop 
      Caption         =   "OverlappingRightBarTop"
      Height          =   255
      Left            =   3615
      TabIndex        =   26
      Top             =   6990
      Width           =   2250
   End
   Begin VB.TextBox txtRefreshGanttArea 
      Height          =   285
      Left            =   1890
      TabIndex        =   25
      Text            =   "GANTT"
      Top             =   5745
      Width           =   1710
   End
   Begin VB.CommandButton Reload 
      Caption         =   "Reload"
      Height          =   285
      Left            =   1890
      TabIndex        =   24
      Top             =   5430
      Width           =   1275
   End
   Begin VB.CheckBox chkWithTimeScale 
      Caption         =   "WithTimeScale"
      Height          =   255
      Left            =   3615
      TabIndex        =   18
      Top             =   6765
      Value           =   1  'Checked
      Width           =   2250
   End
   Begin VB.TextBox txtScrollTo 
      Height          =   285
      Left            =   1890
      TabIndex        =   17
      Text            =   "29.09.00 22:00:00"
      Top             =   7005
      Width           =   1665
   End
   Begin VB.CommandButton ScrollTo 
      Caption         =   "ScrollTo"
      Height          =   285
      Left            =   105
      TabIndex        =   16
      Top             =   7005
      Width           =   1635
   End
   Begin VB.CheckBox chkHorScroller 
      Caption         =   "WithHorizontalScrollbar"
      Height          =   255
      Left            =   3615
      TabIndex        =   15
      Top             =   6540
      Value           =   1  'Checked
      Width           =   2250
   End
   Begin VB.TextBox txtRefreshGanttLine 
      Height          =   285
      Left            =   1890
      TabIndex        =   13
      Text            =   "0"
      ToolTipText     =   "Insert the Gantt-LineNo to be refreshed"
      Top             =   6060
      Width           =   750
   End
   Begin VB.CommandButton btnRefreshGanttLine 
      Caption         =   "RefreshGanttLine"
      Height          =   285
      Left            =   105
      TabIndex        =   12
      Top             =   6060
      Width           =   1635
   End
   Begin VB.CheckBox EnableDragDropBkBar 
      Caption         =   "EnableDragDropBkBar"
      Height          =   255
      Left            =   3615
      TabIndex        =   11
      Top             =   5610
      Width           =   2250
   End
   Begin VB.CheckBox EnableDragDropBar 
      Caption         =   "EnableDragDropBar"
      Height          =   255
      Left            =   3615
      TabIndex        =   10
      Top             =   5385
      Value           =   1  'Checked
      Width           =   2250
   End
   Begin MSComDlg.CommonDialog CommonDialog1 
      Left            =   2835
      Top             =   6060
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.CommandButton RefreshGanttArea 
      Caption         =   "RefreshGanttArea"
      Height          =   285
      Left            =   105
      TabIndex        =   7
      Top             =   5745
      Width           =   1635
   End
   Begin VB.TextBox txtTabHeaderString 
      Height          =   285
      Left            =   1890
      TabIndex        =   6
      Text            =   "Name,Surname"
      ToolTipText     =   "Header string comma separated, e.g. Will,Smith,1st Avenue"
      Top             =   6690
      Width           =   1665
   End
   Begin VB.TextBox txtHeaderLenString 
      Height          =   285
      Left            =   1890
      TabIndex        =   5
      Text            =   "50,80"
      ToolTipText     =   "Comma separated header length, e.g. 30,50,60"
      Top             =   6375
      Width           =   750
   End
   Begin VB.CommandButton btnSetTabHeaderText 
      Caption         =   "Set Header Text"
      Height          =   285
      Index           =   1
      Left            =   105
      TabIndex        =   4
      ToolTipText     =   "Sets the text of header comma separated, e.g. Will,Smith,1st Avenue"
      Top             =   6690
      Width           =   1635
   End
   Begin VB.CommandButton btnSetTabHeaderLength 
      Caption         =   "Set Header Length"
      Height          =   285
      Index           =   0
      Left            =   105
      TabIndex        =   3
      ToolTipText     =   "Sets the length of header comma separated, e.g. 40,90,35"
      Top             =   6375
      Width           =   1635
   End
   Begin VB.CommandButton btnResetContent 
      Caption         =   "&Reset"
      Height          =   285
      Left            =   105
      TabIndex        =   2
      Top             =   5430
      Width           =   1635
   End
   Begin VB.CheckBox chkTabScrlooer 
      Caption         =   "2nd Scrollbar"
      Height          =   255
      Left            =   3615
      TabIndex        =   0
      Top             =   6300
      Width           =   2250
   End
   Begin UGANTTLib.UGantt Gantt 
      Height          =   5355
      Left            =   60
      TabIndex        =   14
      Top             =   0
      Width           =   13290
      _Version        =   65536
      _ExtentX        =   23442
      _ExtentY        =   9446
      _StockProps     =   64
   End
   Begin VB.Label Label5 
      Caption         =   "Left gantt time:"
      ForeColor       =   &H00FF0000&
      Height          =   225
      Left            =   11190
      TabIndex        =   23
      Top             =   6690
      Width           =   1695
   End
   Begin VB.Label Label4 
      Caption         =   "Mouse/Cursor line:"
      ForeColor       =   &H00FF0000&
      Height          =   225
      Left            =   11190
      TabIndex        =   22
      Top             =   6060
      Width           =   1380
   End
   Begin VB.Label Label3 
      Caption         =   "Mouse/Cursor time:"
      ForeColor       =   &H00FF0000&
      Height          =   225
      Left            =   11190
      TabIndex        =   21
      Top             =   5430
      Width           =   1590
   End
   Begin VB.Label Label2 
      Caption         =   "Background bars:"
      ForeColor       =   &H00FF0000&
      Height          =   225
      Left            =   8625
      TabIndex        =   20
      Top             =   5430
      Width           =   1380
   End
   Begin VB.Label Label1 
      Caption         =   "Bars:"
      ForeColor       =   &H00FF0000&
      Height          =   225
      Left            =   6000
      TabIndex        =   19
      Top             =   5430
      Width           =   540
   End
   Begin VB.Label ActualTimescaleDate 
      Height          =   270
      Left            =   11505
      TabIndex        =   1
      Top             =   7005
      Width           =   1680
   End
   Begin VB.Label LabelMouse 
      Height          =   255
      Left            =   11505
      TabIndex        =   9
      Top             =   6375
      Width           =   1500
   End
   Begin VB.Label LabelGeneral 
      Height          =   255
      Left            =   11505
      TabIndex        =   8
      Top             =   5745
      Width           =   1755
   End
   Begin VB.Menu mnuBar 
      Caption         =   "&Bar"
      Visible         =   0   'False
      Begin VB.Menu mnuBarColor 
         Caption         =   "Bar&Color"
      End
      Begin VB.Menu mnuBarDate 
         Caption         =   "Bar&Date"
         Begin VB.Menu mnuBarDateBegin 
            Caption         =   "BarDate&Begin"
         End
         Begin VB.Menu mnuBarDateEnd 
            Caption         =   "BarDate&End"
         End
      End
      Begin VB.Menu Sep1 
         Caption         =   "-"
      End
      Begin VB.Menu mnuBarStyle 
         Caption         =   "Bar&Style"
      End
      Begin VB.Menu Sep2 
         Caption         =   "-"
      End
      Begin VB.Menu mnuHelp 
         Caption         =   "&Help"
      End
   End
   Begin VB.Menu mnuBkBar 
      Caption         =   "&BkBar"
      Visible         =   0   'False
      Begin VB.Menu mnuBkBarColor 
         Caption         =   "BkBar&Color"
      End
      Begin VB.Menu mnuBkBarDate 
         Caption         =   "BkBar&Date"
      End
   End
End
Attribute VB_Name = "GanttConfig"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public omCurrBarLine As Long
Public omCurrBarLineOld As Long
Public omCurrBarKey As String
Public omCurrBarLineTab As Long

Public omCurrBkBarKey As Integer
Public omCurrLineKey As Integer

Public bmIsReading As Boolean
Public bmIsConnected As Boolean

Public omBarTimeMarkerBegin As Date
Public omBarTimeMarkerEnd As Date

'- - - - - - - - - - - - - - - - - - - -
'- the buttons
'- - - - - - - - - - - - - - - - - - - -
Private Sub btnRefreshGanttLine_Click()
    Gantt.RefreshGanttLine txtRefreshGanttLine.Text
End Sub

Private Sub btnResetContent_Click()
    Gantt.ResetContent
    Gantt.RefreshArea "GANTT,TAB"
End Sub

Private Sub btnSetTabHeaderLength_Click(Index As Integer)
    Gantt.TabHeaderLengthString = txtHeaderLenString.Text
    Gantt.RefreshArea "TAB"
End Sub

Private Sub btnSetTabHeaderText_Click(Index As Integer)
    Gantt.TabSetHeaderText (txtTabHeaderString.Text)
    Gantt.RefreshArea "TAB"
End Sub

Private Sub Form_Resize()
    Gantt.Width = GanttConfig.ScaleWidth - (Gantt.Left * 2)
End Sub

Private Sub Form_Terminate()
    Unload Me
End Sub

Private Sub RefreshGanttArea_Click()
    Gantt.RefreshArea txtRefreshGanttArea.Text
End Sub

Private Sub Reload_Click()
    GanttDataPool.SendGanttData
    Gantt.RefreshArea "GANTT,TAB"
End Sub

Private Sub ScrollTo_Click()
    Gantt.ScrollTo txtScrollTo.Text
End Sub

Private Sub btnUTCOffsetInHours_Click()
    Gantt.UTCOffsetInHours = txtUTCOffsetInHours.Text
End Sub

'- - - - - - - - - - - - - - - - - - - -
'- the checkboxes
'- - - - - - - - - - - - - - - - - - - -
Private Sub chkHorScroller_Click()
    Dim val As Boolean
    val = chkHorScroller.Value
    If val = False Then
        Gantt.WithHorizontalScroller = False
    Else
        Gantt.WithHorizontalScroller = True
    End If
    Gantt.RefreshArea "GANTT,TAB"
End Sub

Private Sub chkWithTimeScale_Click()
    Dim val As Boolean
    val = chkWithTimeScale.Value
    If val = False Then
        Gantt.TimeScaleHeaderHeight = 1
    Else
        Gantt.TimeScaleHeaderHeight = 30
    End If
    Gantt.RefreshArea "GANTT,TAB"
End Sub

Private Sub chkTabScrlooer_Click()
    Dim val As Boolean
    val = chkTabScrlooer.Value
    If val = False Then
        Gantt.With2ndScroller = False
    Else
        Gantt.With2ndScroller = True
    End If
End Sub

Private Sub chkAutoScroll_Click()
    Gantt.AutoScroll = True
End Sub

Private Sub chkOverlappingRightBarTop_Click()
    If chkOverlappingRightBarTop.Value = 0 Then
        Gantt.OverlappingRightBarTop = False
    ElseIf chkOverlappingRightBarTop.Value = 1 Then
        Gantt.OverlappingRightBarTop = True
    End If
End Sub

Private Sub chkShowDataHolding_Click()
    If chkShowDataHolding.Value = 1 Then
        GanttDataBase.Show
        GanttDataPool.Show
    Else
        GanttDataBase.Hide
        GanttDataPool.Hide
    End If
End Sub

Private Sub chkSelectedBarToLineBottom_Click()
    If chkSelectedBarToLineBottom.Value = 1 Then
        Gantt.SelectedBarToLineBottom = True
    Else
        Gantt.SelectedBarToLineBottom = False
    End If
End Sub

Private Sub EnableDragDropBar_Click()
    Dim val As Boolean
    val = EnableDragDropBar.Value
    If val = False Then
        Gantt.EnableDragDropBar = False
    Else
        Gantt.EnableDragDropBar = True
    End If
End Sub

Private Sub EnableDragDropBkBar_Click()
    Dim val As Boolean
    val = EnableDragDropBkBar.Value
    If val = False Then
        Gantt.EnableDragDropBkBar = False
    Else
        Gantt.EnableDragDropBkBar = True
    End If
End Sub

'- - - - - - - - - - - - - - - - - - - - - - - - - - - -
'init
'- - - - - - - - - - - - - - - - - - - - - - - - - - - -
Private Sub Form_Load()

    App.HelpFile = "GanttHelp.hlp"
    GanttConfig.HelpContextID = 1111

   ' Gantt.ResetContent                  'delete everything in the Gantt

'- - - - - - - - - - - - - - - - - - - - - - - - - - - -
'CEDA-Area
'- - - - - - - - - - - - - - - - - - - - - - - - - - - -
    bmIsReading = False
    bmIsConnected = False

    'GanttDataBase.LoadGHD           'load the GHD-data
    'GanttDataBase.LoadDSR           'load the DSR-data
    'GanttDataBase.LoadAFT           'load the AFT-data

    'GanttDataPool.MakeGanttData   'combines the tables to the data-structur

'- - - - - - - - - - - - - - - - - - - - - - - - - - - -
'Init the Gantt
'- - - - - - - - - - - - - - - - - - - - - - - - - - - -

    'init the table line
    'InitThisGantt 0, Gantt
    'Gantt.ResetContent
    'Gantt.TabHeaderLengthString = "50,100"               'init the width of the tab-columns
    'Gantt.TabSetHeaderText ("FirstName,SecondName")      'set the tab-headlines
    'Gantt.SplitterPosition = 150                         'the splitterposition depends on the
                                                         'column-width => 50 + 100 = 150
    'Gantt.TabLines = GanttDataPool.TabGanttLines.GetLineCount            'just one more init-parameter

    'init the timescale
    'Gantt.TimeFrameFrom = CDate(#3/8/1999#)             'be careful of the day/month-order...
    'Gantt.TimeFrameTo = CDate(#3/9/1999 6:00:00 AM#)
    'Gantt.TimeScaleDuration = 10                         '10 hours are on screen
    'ActualTimescaleDate.Caption = CDate(#3/8/1999#)

    'GanttDataPool.SendGanttData   'sends the data of ogLines to the Gantt

    'GanttBarsCfg.Show
    'GanttArrowsCfg.Show
    'GanttBackBarsCfg.Show
    'GanttLinesCfg.Show
    'GanttDecoObjCfg.Show
End Sub

Private Sub Form_Unload(Cancel As Integer)
     Unload GanttDataBase
     Unload GanttDataPool
     Unload GanttArrowsCfg
     Unload GanttBackBarsCfg
     Unload GanttBarsCfg
     Unload GanttLinesCfg
     Unload GanttDecoObjCfg
End Sub

'- - - - - - - - - - - - - - - - - - - - - - - - - - - - -
'-
'-  EVENT-SINKS
'-
'- - - - - - - - - - - - - - - - - - - - - - - - - - - - -
Private Sub Gantt_ActualLineNo(ByVal ActualLineNo As Long)
    LabelMouse.Caption = "LineNo. " & ActualLineNo
End Sub

Private Sub Gantt_ActualTimescaleDate(ByVal TimescaleDate As Date)
    ActualTimescaleDate.Caption = TimescaleDate
End Sub

Private Sub Gantt_ChangeBarDate(ByVal Key As String, ByVal Begin As Date, ByVal Ende As Date)
    If chkEnableMoveBar.Value = 1 Then
        If omCurrBarKey <> Key Then
            Gantt_OnLButtonDownBar Key, -1
        End If

        'set the new date in the Gantt's data
         Gantt.SetBarDate Key, Begin, Ende, False

        'set the new date in the GanttDataPool-tab
        With GanttDataPool.TabGanttBars
            .SetColumnValue omCurrBarLineTab, 2, Begin
            .SetColumnValue omCurrBarLineTab, 3, Ende
        End With

       Gantt.RefreshGanttLine (omCurrBarLine)
    End If
End Sub

Private Sub Gantt_ChangeBkBarDate(ByVal Key As String, ByVal Begin As Date, ByVal Ende As Date)
    If chkEnableMoveBkBar.Value = 1 Then
        Dim llBkBarLine As Long
        Dim llBkBarLineInTab As Long

        llBkBarLine = GetBkBarLineByKey(Key)
        llBkBarLineInTab = GetBkBarLineInTab(Key)

        'set the new date in the Gantt's data
        Gantt.SetBkBarDate Key, Begin, Ende
        Gantt.RefreshGanttLine (llBkBarLine)

        'set the new date in the GanttDataPool-tab
        With GanttDataPool.TabGanttBars
            .SetColumnValue llBkBarLineInTab, 2, Begin
            .SetColumnValue llBkBarLineInTab, 3, Ende
        End With
    End If
End Sub

Private Sub Gantt_EndOfSizeOrMove(ByVal Key As String, ByVal LineNo As Long)
    Gantt.RefreshArea "GANTT,TAB"
End Sub

Private Sub Gantt_OnLButtonDblClkBkGantt()
    MsgBox "Nothing...", vbOKOnly, "OnLButtonDblClkBkGantt"
End Sub

Private Sub Gantt_OnLButtonDownBar(ByVal Key As String, ByVal nFlags As Integer)
    Dim strRetLF As String
    Exit Sub
    strRetLF = vbCrLf

    omCurrBarLineTab = GetBarLineInTab(Key)
    omCurrBarLine = GetBarLineByKey(Key)
    omCurrBarKey = Key

    'With GanttDataPool.TabGanttBars
        'fire the bardata to the form
         
         'txtFireBarLM.Text = "Key: " & Key & strRetLF & _
         '                         "Begin: " & .GetColumnValue(omCurrBarLineTab, 2) & strRetLF & _
         '                         "End: " & .GetColumnValue(omCurrBarLineTab, 3) & strRetLF & _
         '                         "BarText: " & .GetColumnValue(omCurrBarLineTab, 4) & strRetLF & _
         '                         "Shape: " & .GetColumnValue(omCurrBarLineTab, 5) & strRetLF & _
         '                         "BackColor: " & .GetColumnValue(omCurrBarLineTab, 6) & strRetLF & _
         '                         "TextColor: " & .GetColumnValue(omCurrBarLineTab, 7) & strRetLF & _
         '                         "SplitColor: " & CBool(.GetColumnValue(omCurrBarLineTab, 8)) & strRetLF & _
         '                         "nFlags: " & nFlags
            
        'refresh the gantt
        'gantt.RefreshArea "GANTT"
    'End With
End Sub

Private Sub Gantt_OnLButtonDownSubBar(ByVal MainBarKey As String, ByVal SubBarKey As String, ByVal nFlags As Integer)
'    MsgBox "SubBar!" & vbCrLf & "MainBarKey: " & MainBarKey & vbCrLf & _
'           "SubBarKey: " & SubBarKey & vbCrLf & _
'           "nFlags: " & nFlags, vbOKOnly, "OnLButtonDownSubBar"
End Sub

Private Sub Gantt_OnLButtonDownBkBar(ByVal Key As String, ByVal nFlags As Integer)
    Dim strRetLF As String
    Dim llBkBarLineInTab As Long

    strRetLF = vbCrLf

    llBkBarLineInTab = GetBkBarLineInTab(Key)

    If llBkBarLineInTab > -1 Then
        With GanttDataPool.TabGanttBkBars
            txtFireBackBarLM.Text = "Key: " & Key & strRetLF & _
                                "Begin: " & .GetColumnValue(llBkBarLineInTab, 2) & strRetLF & _
                                "End: " & .GetColumnValue(llBkBarLineInTab, 3) & strRetLF & _
                                "BkBarText: " & .GetColumnValue(llBkBarLineInTab, 4) & strRetLF & _
                                "Shape: " & .GetColumnValue(llBkBarLineInTab, 5) & strRetLF & _
                                "BackColor: " & .GetColumnValue(llBkBarLineInTab, 6) & strRetLF & _
                                "TextColor: " & .GetColumnValue(llBkBarLineInTab, 7) & strRetLF & _
                                "nFlags: " & nFlags
        End With
    End If
End Sub

Private Sub Gantt_OnLButtonDblClkBar(ByVal Key As String, ByVal nFlags As Integer)
    MsgBox "Bar!" & Chr(10) & Chr(10) & Chr(13) & "Key: " & Key & Chr(10) & Chr(13) & "nFlags: " & nFlags, vbOKOnly, "OnLButtonDblClkBar"
End Sub

Private Sub Gantt_OnLButtonDblClkBkBar(ByVal Key As String, ByVal nFlags As Integer)
    MsgBox "Background bar!" & Chr(10) & Chr(10) & Chr(13) & "Key: " & Key & Chr(10) & Chr(13) & "nFlags: " & nFlags, vbOKOnly, "OnLButtonDblClkBkBar"
End Sub

Private Sub Gantt_OnLButtonDownBkGantt()
    MsgBox "Nothing...", vbOKOnly, "OnLButtonDownBkGantt"
End Sub

Private Sub Gantt_OnRButtonDownBar(ByVal Key As String, ByVal nFlags As Integer)
    PopupMenu mnuBar
End Sub

Private Sub Gantt_OnRButtonDownBkBar(ByVal Key As String, ByVal nFlags As Integer)
    PopupMenu mnuBkBar
End Sub

Private Sub Gantt_OnRButtonDownBkGantt()
    MsgBox "Nothing...", vbOKOnly, "OnRButtonDownBkGantt"
End Sub

Private Sub Gantt_TimeFromX(ByVal Time As Date)
    LabelGeneral.Caption = Time
End Sub

Private Sub Gantt_OnLButtonDblClkSubBar(ByVal MainBarKey As String, ByVal SubBarKey As String, ByVal nFlags As Integer)
    MsgBox "SubBar!" & vbCrLf & "MainBarKey: " & MainBarKey & vbCrLf & _
           "SubBarKey: " & SubBarKey & vbCrLf & _
           "nFlags: " & nFlags, vbOKOnly, "OnLButtonDblClkSubBar"
End Sub

Private Sub Gantt_OnRButtonDownSubBar(ByVal MainBarKey As String, ByVal SubBarKey As String, ByVal nFlags As Integer)
    MsgBox "SubBar!" & vbCrLf & "MainBarKey: " & MainBarKey & vbCrLf & _
           "SubBarKey: " & SubBarKey & vbCrLf & _
           "nFlags: " & nFlags, vbOKOnly, "OnRButtonDownSubBar"
End Sub


'- - - - - - - - - - - - - - - - - - - - - - - - - - - - -
'- Drag&Drop-section
'- be aware: you don't get a control, you only get the key
'- of the bar you are dropping!
'- - - - - - - - - - - - - - - - - - - - - - - - - - - - -

'- - - - - - - - - - - - - - - - - - - - - - - - - - - - -
'- "Gantt_DragDropBar" is fired by the Gantt by "OnDragEnter"
'- - - - - - - - - - - - - - - - - - - - - - - - - - - - -
Private Sub Gantt_OnDragEnter(ByVal Key As String)
    Exit Sub
    Gantt_OnLButtonDownBar Key, -1
    With GanttDataPool.TabGanttBars
        If .GetColumnValue(omCurrBarLineTab, 11) = 0 Then
            Gantt.AddBarToLine omCurrBarLine, _
                .GetColumnValue(omCurrBarLineTab, 1), _
                .GetColumnValue(omCurrBarLineTab, 2), _
                .GetColumnValue(omCurrBarLineTab, 3), _
                .GetColumnValue(omCurrBarLineTab, 4), _
                .GetColumnValue(omCurrBarLineTab, 5), _
                .GetColumnValue(omCurrBarLineTab, 6), _
                .GetColumnValue(omCurrBarLineTab, 7), _
                CBool(.GetColumnValue(omCurrBarLineTab, 8)), _
                .GetColumnValue(omCurrBarLineTab, 9), _
                .GetColumnValue(omCurrBarLineTab, 10), _
                .GetColumnValue(omCurrBarLineTab, 12), _
                .GetColumnValue(omCurrBarLineTab, 13), _
                .GetColumnValue(omCurrBarLineTab, 14), _
                .GetColumnValue(omCurrBarLineTab, 15)
                
            .SetColumnValue omCurrBarLineTab, 11, "1"
        End If
    End With
End Sub

Private Sub Gantt_OnDragLeave(ByVal Key As String)
    Exit Sub
    Gantt_OnLButtonDownBar Key, -1
    Gantt.DeleteBarByKey (Key)
    Gantt.RefreshArea "GANTT"
    GanttDataPool.TabGanttBars.SetColumnValue omCurrBarLineTab, 11, "0"
End Sub

Private Sub Gantt_OnDragOver(ByVal Key As String, ByVal LineNo As Long)
    Dim strEURN As String
Exit Sub
    'if you drop a bar to another line
    If LineNo <> omCurrBarLine And LineNo < GanttDataPool.TabGanttLines.GetLineCount Then

      'delete the bar in the Gantt
        Gantt.DeleteBarByKey (Key)

      'add the bar to the new line in the Gantt
        With GanttDataPool.TabGanttBars
            Gantt.AddBarToLine LineNo, _
                .GetColumnValue(omCurrBarLineTab, 1), _
                .GetColumnValue(omCurrBarLineTab, 2), _
                .GetColumnValue(omCurrBarLineTab, 3), _
                .GetColumnValue(omCurrBarLineTab, 4), _
                .GetColumnValue(omCurrBarLineTab, 5), _
                .GetColumnValue(omCurrBarLineTab, 6), _
                .GetColumnValue(omCurrBarLineTab, 7), _
                CBool(.GetColumnValue(omCurrBarLineTab, 8)), _
                .GetColumnValue(omCurrBarLineTab, 9), _
                .GetColumnValue(omCurrBarLineTab, 10), _
                .GetColumnValue(omCurrBarLineTab, 12), _
                .GetColumnValue(omCurrBarLineTab, 13), _
                .GetColumnValue(omCurrBarLineTab, 14), _
                .GetColumnValue(omCurrBarLineTab, 15)

      'add the bar to the new line (that means to the new worker with the new EURN)
            strEURN = GanttDataPool.TabGanttLines.GetColumnValue(LineNo, 0)
            .SetColumnValue omCurrBarLineTab, 0, strEURN

      'keep the new line in mind
            omCurrBarLine = LineNo
        End With
    End If
End Sub

Private Sub Gantt_OnDrop(ByVal Key As String, ByVal LineNo As Long)
    Gantt.RefreshArea "TAB,GANTT"
End Sub

Private Sub Gantt_GotFocus()
    Gantt.RefreshArea ("ALL")
End Sub

Private Sub Gantt_LostFocus()
    GanttConfig.Gantt.RefreshArea "GANTT,TAB,SPLITTER"
End Sub

'- - - - - - - - - - - - - - - - - - - -
'- the menues
'- - - - - - - - - - - - - - - - - - - -
'- you can edit them:
'- * DblClk on the form you want to add one
'- * "Ctrl + E" to start the "Menu Editor"
'- - - - - - - - - - - - - - - - - - - -
Private Sub mnuBarDateEnd_Click()
    MsgBox "Sorry..." & Chr(10) & Chr(13) & "...not implemented yet.", vbOKOnly, "mnuBarDateEnd_Click()"
End Sub
Private Sub mnuBarDateBegin_Click()
    MsgBox "Sorry..." & Chr(10) & Chr(13) & "...not implemented yet.", vbOKOnly, "mnuBarDateBegin_Click()"
End Sub
Private Sub mnuBarColor_Click()
    MsgBox "Sorry..." & Chr(10) & Chr(13) & "...not implemented yet.", vbOKOnly, "mnuBarDateColor_Click()"
End Sub
Private Sub mnuBarStyle_Click()
    MsgBox "Sorry..." & Chr(10) & Chr(13) & "...not implemented yet.", vbOKOnly, "mnuBarStyle_Click()"
End Sub
Private Sub mnuBkBarColor_Click()
    MsgBox "Sorry..." & Chr(10) & Chr(13) & "...not implemented yet.", vbOKOnly, "mnuBkBarColor_Click()"
End Sub
Private Sub mnuBkBarDate_Click()
    MsgBox "Sorry..." & Chr(10) & Chr(13) & "...not implemented yet.", vbOKOnly, "mnuBkBarDate_Click()"
End Sub
Private Sub mnuHelp_Click()
    'DisplayHelp_On_Index Me
End Sub
