VERSION 5.00
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "TAB.ocx"
Begin VB.Form GanttDataPool 
   Caption         =   "HiddenGanttData"
   ClientHeight    =   6720
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   9465
   LinkTopic       =   "Form2"
   LockControls    =   -1  'True
   ScaleHeight     =   6720
   ScaleWidth      =   9465
   StartUpPosition =   3  'Windows Default
   Begin TABLib.TAB TabFlightBars 
      Height          =   1575
      Index           =   0
      Left            =   5850
      TabIndex        =   4
      Top             =   360
      Width           =   3255
      _Version        =   65536
      _ExtentX        =   5741
      _ExtentY        =   2778
      _StockProps     =   64
   End
   Begin VB.CommandButton Refresh 
      Caption         =   "Refresh"
      Height          =   255
      Left            =   90
      TabIndex        =   3
      Top             =   60
      Width           =   1215
   End
   Begin TABLib.TAB TabGanttBkBars 
      Height          =   1575
      Index           =   0
      Left            =   60
      TabIndex        =   2
      Top             =   3660
      Width           =   5595
      _Version        =   65536
      _ExtentX        =   9869
      _ExtentY        =   2778
      _StockProps     =   64
   End
   Begin TABLib.TAB TabGanttBars 
      Height          =   1575
      Index           =   0
      Left            =   60
      TabIndex        =   1
      Top             =   2010
      Width           =   5595
      _Version        =   65536
      _ExtentX        =   9869
      _ExtentY        =   2778
      _StockProps     =   64
   End
   Begin TABLib.TAB TabGanttLines 
      Height          =   1575
      Index           =   0
      Left            =   60
      TabIndex        =   0
      Top             =   360
      Width           =   5595
      _Version        =   65536
      _ExtentX        =   9869
      _ExtentY        =   2778
      _StockProps     =   64
   End
End
Attribute VB_Name = "GanttDataPool"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim OldTabLeft As Long
Dim OldTabTop As Long
Dim OldTabWidth As Long
Dim OldTabHeight As Long
Dim CurMaxTab As TABLib.Tab
Dim TabIsMaxi As Boolean

Public Sub InitFlightBarList(AftRkey As String, AftAurn As String, AftUrno As String)
    Dim FldList As String
    Dim FldData As String
    Dim NewTabRec As String
    Dim FldName As String
    Dim AftAdid As String
    Dim BarAdid As String
    Dim BarAurn As String
    Dim ChkAurn As String
    Dim BarUrno As String
    Dim AftFtyp As String
    Dim AftTifd As String
    Dim AftTifa As String
    Dim BarTifd As String
    Dim BarTifa As String
    Dim AftPdbs As String
    Dim AftPdes As String
    Dim AftPabs As String
    Dim AftPaes As String
    Dim BarSort As String
    Dim HitLines As Long
    Dim LineNo As Long
    Dim MaxLine As Long
    Dim CurLine As Long
    Dim SortCol As Long
    Dim FldItem As Integer
    Dim BarBgn As Date
    Dim BarEnd As Date
    Dim BarMid As Date
    Dim BarLen As Long
    Dim BarCut As Long
    
    'For test only
    'MaxLine = TabFlightBars(0).GetLineCount - 1
    'If MaxLine > 0 Then
    '    TabFlightBars(0).AutoSizeColumns
    '    TabFlightBars(0).ShowHorzScroller True
    '    Exit Sub
    'End If
    
    
    TabFlightBars(0).ResetContent
    FldList = TabFlightBars(0).LogicalFieldList
    ChkAurn = AftAurn
    If ChkAurn = "" Then ChkAurn = AftUrno
    CurLine = 0
    If TabArrFlightsIdx >= 0 Then
        TabArrFlightsTab.SetInternalLineBuffer True
        HitLines = Val(TabArrFlightsTab.GetLinesByIndexValue("RKEY", AftRkey, 0))
        If HitLines > 0 Then
            LineNo = TabArrFlightsTab.GetNextResultLine
            While LineNo >= 0
                BarUrno = Trim(TabArrFlightsTab.GetFieldValue(LineNo, "URNO"))
                If BarUrno <> ChkAurn Then
                    NewTabRec = ""
                End If
                If BarUrno = ChkAurn Then
                    NewTabRec = ""
                    FldItem = 0
                    FldName = "START"
                    While FldName <> ""
                        FldItem = FldItem + 1
                        FldName = GetItem(FldList, FldItem, ",")
                        If FldName <> "" Then
                            FldData = Trim(TabArrFlightsTab.GetFieldValue(LineNo, FldName))
                            NewTabRec = NewTabRec & FldData & ","
                        End If
                    Wend
                    TabFlightBars(0).InsertTextLine NewTabRec, False
                End If
                LineNo = TabArrFlightsTab.GetNextResultLine
            Wend
        End If
        TabArrFlightsTab.SetInternalLineBuffer False
    End If
    
    If TabArrTowingIdx >= 0 Then
        TabArrTowingTab.SetInternalLineBuffer True
        HitLines = Val(TabArrTowingTab.GetLinesByIndexValue("RKEY", AftRkey, 0))
        If HitLines > 0 Then
            LineNo = TabArrTowingTab.GetNextResultLine
            While LineNo >= 0
                BarAurn = Trim(TabArrTowingTab.GetFieldValue(LineNo, "AURN"))
                If BarAurn <> ChkAurn Then
                    NewTabRec = ""
                End If
                If BarAurn = ChkAurn Then
                    NewTabRec = ""
                    FldItem = 0
                    FldName = "START"
                    While FldName <> ""
                        FldItem = FldItem + 1
                        FldName = GetItem(FldList, FldItem, ",")
                        If FldName <> "" Then
                            FldData = Trim(TabArrTowingTab.GetFieldValue(LineNo, FldName))
                            NewTabRec = NewTabRec & FldData & ","
                        End If
                    Wend
                    TabFlightBars(0).InsertTextLine NewTabRec, False
                    CurLine = TabFlightBars(0).GetLineCount - 1
                    'Repair Towings Without TIFD/TIFA/PDBS/PDES etc
                    AftTifd = Trim(TabFlightBars(0).GetFieldValue(CurLine, "TIFD"))
                    If AftTifd = "" Then AftTifd = Trim(TabArrTowingTab.GetFieldValue(LineNo, "OFBL"))
                    If AftTifd = "" Then AftTifd = Trim(TabArrTowingTab.GetFieldValue(LineNo, "STOD"))
                    If AftTifd = "" Then AftTifd = Trim(TabFlightBars(0).GetFieldValue(CurLine, "PDES"))
                    AftTifa = Trim(TabFlightBars(0).GetFieldValue(CurLine, "TIFA"))
                    If AftTifa = "" Then AftTifa = Trim(TabArrTowingTab.GetFieldValue(LineNo, "OFBL"))
                    If AftTifa = "" Then AftTifa = Trim(TabArrTowingTab.GetFieldValue(LineNo, "STOA"))
                    If AftTifa = "" Then AftTifa = Trim(TabFlightBars(0).GetFieldValue(CurLine, "PABS"))
                    FldData = AftTifd & "," & AftTifa
                    TabFlightBars(0).SetFieldValues CurLine, "TIFD,TIFA", FldData
                End If
                        
                LineNo = TabArrTowingTab.GetNextResultLine
            Wend
        End If
        TabArrTowingTab.SetInternalLineBuffer False
    End If
    
    If TabDepFlightsIdx >= 0 Then
        TabDepFlightsTab.SetInternalLineBuffer True
        HitLines = Val(TabDepFlightsTab.GetLinesByIndexValue("RKEY", AftRkey, 0))
        If HitLines > 0 Then
            LineNo = TabDepFlightsTab.GetNextResultLine
            While LineNo >= 0
                BarAurn = Trim(TabDepFlightsTab.GetFieldValue(LineNo, "AURN"))
                If BarAurn = "" Then BarAurn = Trim(TabDepFlightsTab.GetFieldValue(LineNo, "URNO"))
                If BarAurn <> ChkAurn Then
                    NewTabRec = ""
                End If
                If BarAurn = ChkAurn Then
                    NewTabRec = ""
                    FldItem = 0
                    FldName = "START"
                    While FldName <> ""
                        FldItem = FldItem + 1
                        FldName = GetItem(FldList, FldItem, ",")
                        If FldName <> "" Then
                            FldData = Trim(TabDepFlightsTab.GetFieldValue(LineNo, FldName))
                            NewTabRec = NewTabRec & FldData & ","
                        End If
                    Wend
                    TabFlightBars(0).InsertTextLine NewTabRec, False
                End If
                LineNo = TabDepFlightsTab.GetNextResultLine
            Wend
        End If
        TabDepFlightsTab.SetInternalLineBuffer False
    End If
    
    'Preparing the Bar Type and Time Values
    'Here we always find records in the order of ARR/TOW/DEP
    MaxLine = TabFlightBars(0).GetLineCount - 1
    CurLine = 0
    For CurLine = 0 To MaxLine
        BarAdid = ""
        AftAdid = TabFlightBars(0).GetFieldValue(CurLine, "ADID")
        AftFtyp = TabFlightBars(0).GetFieldValue(CurLine, "FTYP")
        Select Case AftAdid
            Case "A"
                BarAdid = "A"
            Case "B"
                Select Case AftFtyp
                    Case "T", "G"
                            BarAdid = "B"
                    Case Else
                        If BarAdid = "" Then
                            BarAdid = "A"
                        Else
                            BarAdid = "D"
                        End If
                        TabFlightBars(0).SetFieldValues CurLine, "ADID", BarAdid
                End Select
            Case "D"
                BarAdid = "D"
            Case Else
        End Select
        Select Case BarAdid
            Case "A"
                AftTifa = TabFlightBars(0).GetFieldValue(CurLine, "TIFA")
                BarSort = BarAdid & AftTifa
            Case "B"
                AftTifd = TabFlightBars(0).GetFieldValue(CurLine, "TIFD")
                BarSort = BarAdid & AftTifd
            Case "D"
                AftTifd = TabFlightBars(0).GetFieldValue(CurLine, "TIFD")
                BarSort = BarAdid & AftTifd
            Case Else
        End Select
        TabFlightBars(0).SetFieldValues CurLine, "SORT", BarSort
    Next
    SortCol = 0
    TabFlightBars(0).Sort CStr(SortCol), True, True
    
    'Calculating the Bar Times
    MaxLine = TabFlightBars(0).GetLineCount - 1
    For CurLine = 0 To MaxLine
        BarAdid = TabFlightBars(0).GetFieldValue(CurLine, "ADID")
        AftTifa = ""
        AftTifd = ""
        AftPdbs = ""
        AftPdes = ""
        AftPabs = ""
        AftPaes = ""
        Select Case BarAdid
            Case "A"
                If CurLine < MaxLine Then
                    AftTifa = TabFlightBars(0).GetFieldValue(CurLine, "TIFA")
                    AftTifd = TabFlightBars(0).GetFieldValue(CurLine + 1, "TIFD")
                    If (AftTifa <> "") And (AftTifd <> "") Then
                        BarBgn = CedaFullDateToVb(AftTifa)
                        BarEnd = CedaFullDateToVb(AftTifd)
                        BarLen = DateDiff("n", BarBgn, BarEnd)
                        BarCut = BarLen \ 2
                        BarMid = DateAdd("n", BarCut, BarBgn)
                        AftPabs = Format(BarBgn, "yyyymmddhhmm00")
                        AftPaes = Format(BarMid, "yyyymmddhhmm00")
                        FldList = "PABS,PAES"
                        FldData = AftPabs & "," & AftPaes
                        TabFlightBars(0).SetFieldValues CurLine, FldList, FldData
                    End If
                Else
                    'Open Arrival Flight
                    AftTifa = TabFlightBars(0).GetFieldValue(CurLine, "TIFA")
                    AftTifd = "20201231235959"
                    FldList = "PABS,PAES"
                    FldData = AftTifa & "," & AftTifd
                    TabFlightBars(0).SetFieldValues CurLine, FldList, FldData
                End If
            Case "B"
                If CurLine > 0 Then
                    AftTifa = TabFlightBars(0).GetFieldValue(CurLine - 1, "TIFA")
                    AftTifd = TabFlightBars(0).GetFieldValue(CurLine, "TIFD")
                    If (AftTifa <> "") And (AftTifd <> "") Then
                        BarBgn = CedaFullDateToVb(AftTifa)
                        BarEnd = CedaFullDateToVb(AftTifd)
                        BarLen = DateDiff("n", BarBgn, BarEnd)
                        BarCut = BarLen \ 2
                        BarMid = DateAdd("n", BarCut, BarBgn)
                        AftPdbs = Format(BarMid, "yyyymmddhhmm00")
                        AftPdes = Format(BarEnd, "yyyymmddhhmm00")
                        FldList = "PDBS,PDES"
                        FldData = AftPdbs & "," & AftPdes
                        TabFlightBars(0).SetFieldValues CurLine, FldList, FldData
                    End If
                Else
                    'Orphaned Towing (ERROR)
                    AftTifd = TabFlightBars(0).GetFieldValue(CurLine, "TIFD")
                    If AftTifd <> "" Then
                        BarEnd = CedaFullDateToVb(AftTifd)
                        BarLen = -1440
                        BarBgn = DateAdd("n", BarLen, BarEnd)
                        AftPdbs = Format(BarBgn, "yyyymmddhhmm00")
                        AftPdes = Format(BarEnd, "yyyymmddhhmm00")
                        FldList = "PDBS,PDES"
                        FldData = AftPdbs & "," & AftPdes
                        TabFlightBars(0).SetFieldValues CurLine, FldList, FldData
                    End If
                End If
                If CurLine < MaxLine Then
                    AftTifa = TabFlightBars(0).GetFieldValue(CurLine, "TIFA")
                    AftTifd = TabFlightBars(0).GetFieldValue(CurLine + 1, "TIFD")
                    If (AftTifa <> "") And (AftTifd <> "") Then
                        BarBgn = CedaFullDateToVb(AftTifa)
                        BarEnd = CedaFullDateToVb(AftTifd)
                        BarLen = DateDiff("n", BarBgn, BarEnd)
                        BarCut = BarLen \ 2
                        BarMid = DateAdd("n", BarCut, BarBgn)
                        AftPabs = Format(BarBgn, "yyyymmddhhmm00")
                        AftPaes = Format(BarMid, "yyyymmddhhmm00")
                        FldList = "PABS,PAES"
                        FldData = AftPabs & "," & AftPaes
                        TabFlightBars(0).SetFieldValues CurLine, FldList, FldData
                    End If
                Else
                    'Open Towing
                    AftTifa = TabFlightBars(0).GetFieldValue(CurLine, "TIFA")
                    AftTifd = "20201231235959"
                    FldList = "PABS,PAES"
                    FldData = AftTifa & "," & AftTifd
                    TabFlightBars(0).SetFieldValues CurLine, FldList, FldData
                End If
            Case "D"
                If CurLine > 0 Then
                    AftTifa = TabFlightBars(0).GetFieldValue(CurLine - 1, "TIFA")
                    AftTifd = TabFlightBars(0).GetFieldValue(CurLine, "TIFD")
                    If (AftTifa <> "") And (AftTifd <> "") Then
                        BarBgn = CedaFullDateToVb(AftTifa)
                        BarEnd = CedaFullDateToVb(AftTifd)
                        BarLen = DateDiff("n", BarBgn, BarEnd)
                        BarCut = BarLen \ 2
                        BarMid = DateAdd("N", BarCut, BarBgn)
                        AftPdbs = Format(BarMid, "yyyymmddhhmm00")
                        AftPdes = Format(BarEnd, "yyyymmddhhmm00")
                        FldList = "PDBS,PDES"
                        FldData = AftPdbs & "," & AftPdes
                        TabFlightBars(0).SetFieldValues CurLine, FldList, FldData
                    End If
                Else
                    'Unmatched Departure Flight
                    AftTifd = TabFlightBars(0).GetFieldValue(CurLine, "TIFD")
                    If AftTifd <> "" Then
                        BarEnd = CedaFullDateToVb(AftTifd)
                        BarLen = -90
                        BarBgn = DateAdd("n", BarLen, BarEnd)
                        AftPdbs = Format(BarBgn, "yyyymmddhhmm00")
                        AftPdes = Format(BarEnd, "yyyymmddhhmm00")
                        FldList = "PDBS,PDES"
                        FldData = AftPdbs & "," & AftPdes
                        TabFlightBars(0).SetFieldValues CurLine, FldList, FldData
                    End If
                End If
            Case Else
        End Select
    Next
End Sub

Public Function GetFlightBarTimes(AftUrno As String, AftAdid As String, PosAdid As String, BarVafr As String, BarVato As String) As String
    Dim BarUrno As String
    Dim BarAdid As String
    Dim BarStat As String
    Dim CurLine As Long
    Dim MaxLine As Long
    BarStat = ""
    MaxLine = TabFlightBars(0).GetLineCount - 1
    CurLine = 0
    While CurLine <= MaxLine
        BarUrno = TabFlightBars(0).GetFieldValue(CurLine, "URNO")
        If BarUrno = AftUrno Then
            BarAdid = TabFlightBars(0).GetFieldValue(CurLine, "ADID")
            If BarAdid = AftAdid Then
                If PosAdid = "A" Then
                    BarVafr = TabFlightBars(0).GetFieldValue(CurLine, "PABS")
                    BarVato = TabFlightBars(0).GetFieldValue(CurLine, "PAES")
                End If
                If PosAdid = "D" Then
                    BarVafr = TabFlightBars(0).GetFieldValue(CurLine, "PDBS")
                    BarVato = TabFlightBars(0).GetFieldValue(CurLine, "PDES")
                End If
                BarStat = TabFlightBars(0).GetFieldValue(CurLine, "STAT")
                CurLine = MaxLine + 1
            End If
        End If
        CurLine = CurLine + 1
    Wend
    GetFlightBarTimes = BarStat
End Function


'Private Function InitGanttDataTab()
'
'    Dim strHeader, strHeaderLen As String
'    Dim strArr() As String
'    Dim i As Integer
'
'    TabGanttBkBars.ResetContent
'    TabGanttLines.ResetContent
'    TabGanttBars.ResetContent
'
'    'init TabGanttLines
'    strHeader = "EURN, SecondName, FirstName, TabBackColor, TabTextColor"
'    strHeaderLen = ""
'    strArr = Split(strHeader, ",")
'    For i = 0 To UBound(strArr)
'        If i + 1 > UBound(strArr) Then
'            strHeaderLen = strHeaderLen & "80"
'        Else
'            strHeaderLen = strHeaderLen & "80,"
'        End If
'    Next i
'    TabGanttLines.HeaderLengthString = strHeaderLen
'    TabGanttLines.HeaderString = strHeader
'
'    'init TabGanttBars and TabGanttBkBars
'    strHeader = "EURN, Key, Begin, End, BarText, Shape, BackColor, TextColor, SplitColor" & _
'        ",LeftColor, RightColor, Visible, LeftText, RightText, LeftTextColor, RightTextColor"
'    strHeaderLen = ""
'    strArr = Split(strHeader, ",")
'    For i = 0 To UBound(strArr)
'        If i + 1 > UBound(strArr) Then
'            strHeaderLen = strHeaderLen & "80"
'        Else
'            strHeaderLen = strHeaderLen & "80,"
'        End If
'    Next i
'    TabGanttBars.HeaderLengthString = strHeaderLen
'    TabGanttBars.HeaderString = strHeader
'    TabGanttBars.ShowHorzScroller True
'
'    TabGanttBkBars.HeaderLengthString = strHeaderLen
'    TabGanttBkBars.HeaderString = strHeader
'    TabGanttBkBars.ShowHorzScroller True
'
'End Function
'
'Public Sub MakeGanttData()
'
'    Dim i, j As Long
'    Dim strEURN As String
'    Dim strRet As String
'    Dim strTextLine As String
'    Dim strFlno As String
'    Dim strFkey As String
'    Dim strBarBkColor As String
'    Dim strDrti As String
'
'    Dim olLines() As String
'
'    InitGanttDataTab
'
'    'look for the workers - every worker gets only one line!
'    With GanttDataBase
'        For i = 0 To .TabGHD.GetLineCount - 1
'            strEURN = Trim(.TabGHD.GetColumnValue(i, 2))
'
'            If TabGanttLines.GetLinesByColumnValue(0, strEURN, 0) = "" And _
'            GanttDataBase.TabDSR.GetLinesByColumnValue(0, strEURN, 0) <> "" Then 'the worker has no line
'                strTextLine = strEURN + "," + _
'                    Trim(.TabDSR.GetColumnValue(.TabDSR.GetLinesByColumnValue(0, strEURN, 0), 1)) + "," + _
'                    Trim(.TabDSR.GetColumnValue(.TabDSR.GetLinesByColumnValue(0, strEURN, 0), 2)) + "," + _
'                    "16777215,0"
'                TabGanttLines.InsertTextLine strTextLine, False
'            End If
'        Next i
'
'        For i = 0 To .TabDSR.GetLineCount - 1
'            strEURN = Trim(.TabDSR.GetColumnValue(i, 0))
'
'            If TabGanttLines.GetLinesByColumnValue(0, strEURN, 0) = "" Then  'the worker still has no line
'                strTextLine = strEURN + "," + _
'                    Trim(.TabDSR.GetColumnValue(.TabDSR.GetLinesByColumnValue(0, strEURN, 0), 1)) + "," + _
'                    Trim(.TabDSR.GetColumnValue(.TabDSR.GetLinesByColumnValue(0, strEURN, 0), 2)) + "," + _
'                    "16777215,0"
'                TabGanttLines.InsertTextLine strTextLine, False
'            End If
'        Next i
'
'        'look for the bars and the background bars of the worker
'        For i = 0 To TabGanttLines.GetLineCount - 1
'            strEURN = TabGanttLines.GetColumnValue(i, 0)
'
'            'first do the bars
'            strRet = .TabGHD.GetLinesByColumnValue(2, strEURN, 1)
'            If strRet <> "" Then
'                olLines = Split(strRet, ",")
'                For j = 0 To UBound(olLines)
'                    strFlno = GetFlnoByFkey(Trim(.TabGHD.GetColumnValue(olLines(j), 1)))
'                    If strFlno = "" Then strFlno = "No data!"
'                    strDrti = .TabGHD.GetColumnValue(olLines(j), 5)
'                    strBarBkColor = vbRed
'                    If strDrti = "A" Then
'                        strBarBkColor = vbYellow
'                    End If
'                    If strDrti = "B" Then
'                        strBarBkColor = 12632064
'                    End If
'                    If strDrti = "D" Then
'                        strBarBkColor = vbGreen
'                        strTextLine = strEURN + "," + _
'                            Trim(.TabGHD.GetColumnValue(olLines(j), 0)) + "," + _
'                            CStr(CedaFullDateToVb(Trim(.TabGHD.GetColumnValue(olLines(j), 3)))) + "," + _
'                            CStr(CedaFullDateToVb(Trim(.TabGHD.GetColumnValue(olLines(j), 4)))) + "," + _
'                            strFlno + "," + _
'                            "1," + strBarBkColor + ",0,1," + CStr(vbGreen) + "," + CStr(vbWhite) + ",1," + _
'                            " ," + " ," + "0,0"
'                    Else
'                        strTextLine = strEURN + "," + _
'                            Trim(.TabGHD.GetColumnValue(olLines(j), 0)) + "," + _
'                            CStr(CedaFullDateToVb(Trim(.TabGHD.GetColumnValue(olLines(j), 3)))) + "," + _
'                            CStr(CedaFullDateToVb(Trim(.TabGHD.GetColumnValue(olLines(j), 4)))) + "," + _
'                            strFlno + "," + _
'                            "1," + strBarBkColor + ",0,0,0,0,1," + _
'                            " ," + " ," + "0,0"
'                    End If
'                    TabGanttBars.InsertTextLine strTextLine, False
'                Next j
'            End If
'
'            'now do the background bars
'            strRet = .TabDSR.GetLinesByColumnValue(0, strEURN, 1)
'            If strRet <> "" Then
'                olLines = Split(strRet, ",")
'                For j = 0 To UBound(olLines)
'                    strTextLine = strEURN + "," + _
'                        Trim(.TabDSR.GetColumnValue(olLines(j), 0)) + "," + _
'                        CStr(CedaFullDateToVb(Trim(.TabDSR.GetColumnValue(olLines(j), 3)))) + "," + _
'                        CStr(CedaFullDateToVb(Trim(.TabDSR.GetColumnValue(olLines(j), 4)))) + "," + _
'                        " " + ",0,16711680,0,1"
'                    TabGanttBkBars.InsertTextLine strTextLine, False
'                Next j
'            End If
'        Next i
'    End With
'End Sub
'
'Public Sub SendGanttData()
'    Dim i, j As Long
'    Dim strRet, strEURN As String
'    Dim olLines() As String
'
'    'init the line
'    For i = 0 To TabGanttLines.GetLineCount - 1
'        strEURN = TabGanttLines.GetColumnValue(i, 0)
'        GanttConfig.Gantt.AddLineAt i, _
'            strEURN, _
'            TabGanttLines.GetColumnValue(i, 1) + "," + _
'            TabGanttLines.GetColumnValue(i, 2)
'
'    'send the BackBars
'        strRet = TabGanttBkBars.GetLinesByColumnValue(0, strEURN, 0)
'        If strRet <> "" Then
'            olLines = Split(strRet, ",")
'            For j = 0 To UBound(olLines)
'                GanttConfig.Gantt.AddBkBarToLine i, _
'                    CStr(TabGanttBkBars.GetColumnValue(olLines(j), 1)), _
'                    TabGanttBkBars.GetColumnValue(olLines(j), 2), _
'                    TabGanttBkBars.GetColumnValue(olLines(j), 3), _
'                    TabGanttBkBars.GetColumnValue(olLines(j), 6), _
'                    TabGanttBkBars.GetColumnValue(olLines(j), 7)
'            Next j
'        End If
'
'    'send the Bars
'        strRet = TabGanttBars.GetLinesByColumnValue(0, strEURN, 0)
'        If strRet <> "" Then
'            olLines = Split(strRet, ",")
'            For j = 0 To UBound(olLines)
'                GanttConfig.Gantt.AddBarToLine i, _
'                    TabGanttBars.GetColumnValue(olLines(j), 1), _
'                    TabGanttBars.GetColumnValue(olLines(j), 2), _
'                    TabGanttBars.GetColumnValue(olLines(j), 3), _
'                    TabGanttBars.GetColumnValue(olLines(j), 4), _
'                    TabGanttBars.GetColumnValue(olLines(j), 5), _
'                    TabGanttBars.GetColumnValue(olLines(j), 6), _
'                    TabGanttBars.GetColumnValue(olLines(j), 7), _
'                    CBool(TabGanttBars.GetColumnValue(olLines(j), 8)), _
'                    TabGanttBars.GetColumnValue(olLines(j), 9), _
'                    TabGanttBars.GetColumnValue(olLines(j), 10), _
'                    TabGanttBars.GetColumnValue(olLines(j), 12), _
'                    TabGanttBars.GetColumnValue(olLines(j), 13), _
'                    TabGanttBars.GetColumnValue(olLines(j), 14), _
'                    TabGanttBars.GetColumnValue(olLines(j), 15)
'            Next j
'        End If
'    Next i
'End Sub
'
'Private Sub Refresh_Click()
'    TabGanttBars.RedrawTab
'    TabGanttBkBars.RedrawTab
'    TabGanttLines.RedrawTab
'End Sub
'
'Private Function GetFlnoByFkey(strFkey As String) As String
'    Dim strLineNo As String
'    Dim llLineNo As Long
'
'    With GanttDataBase.TabAFT
'        strLineNo = .GetLinesByColumnValue(0, strFkey, 0)
'        If strLineNo <> "" Then
'            llLineNo = strLineNo
'        End If
'        GetFlnoByFkey = .GetColumnValue(llLineNo, 1)
'    End With
'End Function

Private Sub Form_Load()
    InitTabLayout
End Sub

Private Sub InitTabLayout()
    Dim i As Integer
    Dim FldList As String
    Dim LenList As String
    Dim AliList As String
    
    FldList = "SORT,ADID,FTYP,RKEY,URNO,AURN,TIFD,TIFA,PDBS,PDES,PABS,PAES,PSTD,PSTA,STAT"
    LenList = "20,1,1,10,10,10,14,14,14,14,14,14,6,6,10"
    AliList = "L,L,L,R,R,R,L,L,L,L,L,L,L,L,L"
    
    For i = 0 To TabFlightBars.UBound
        TabFlightBars(i).ResetContent
        TabFlightBars(i).LogicalFieldList = FldList
        TabFlightBars(i).HeaderString = FldList
        TabFlightBars(i).HeaderLengthString = LenList
        TabFlightBars(i).ColumnWidthString = LenList
        TabFlightBars(i).ColumnAlignmentString = AliList
        TabFlightBars(i).AutoSizeByHeader = True
        TabFlightBars(i).AutoSizeColumns
    Next
End Sub

Private Sub TabFlightBars_SendLButtonDblClick(Index As Integer, ByVal LineNo As Long, ByVal ColNo As Long)
    If LineNo < 1 Then
        TabFlightBars(Index).Left = 60
        TabFlightBars(Index).Top = 360
        TabFlightBars(Index).Width = Me.ScaleWidth - 120
        TabFlightBars(Index).Height = Me.ScaleHeight - 420
    End If
End Sub
