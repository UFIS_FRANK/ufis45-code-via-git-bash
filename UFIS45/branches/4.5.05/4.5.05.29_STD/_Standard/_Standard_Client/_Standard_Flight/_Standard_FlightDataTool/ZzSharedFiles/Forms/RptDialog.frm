VERSION 5.00
Begin VB.Form RptDialog 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Report Options"
   ClientHeight    =   3090
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   4680
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   8.25
      Charset         =   0
      Weight          =   700
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "RptDialog.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3090
   ScaleWidth      =   4680
   ShowInTaskbar   =   0   'False
   Begin VB.CheckBox chkTask 
      Caption         =   "Close"
      Height          =   315
      Index           =   2
      Left            =   2880
      Style           =   1  'Graphical
      TabIndex        =   11
      Top             =   2670
      Width           =   1035
   End
   Begin VB.CheckBox RptCheck1 
      Caption         =   "Show Flight Panel"
      Height          =   315
      Index           =   0
      Left            =   30
      TabIndex        =   10
      Top             =   1350
      Width           =   1815
   End
   Begin VB.OptionButton RptOpt1 
      Caption         =   "Right"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   225
      Index           =   2
      Left            =   3990
      TabIndex        =   7
      Top             =   720
      Width           =   675
   End
   Begin VB.OptionButton RptOpt1 
      Caption         =   "Center"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   225
      Index           =   1
      Left            =   3180
      TabIndex        =   6
      Top             =   720
      Width           =   825
   End
   Begin VB.OptionButton RptOpt1 
      Caption         =   "Left"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Index           =   0
      Left            =   2550
      TabIndex        =   5
      Top             =   720
      Value           =   -1  'True
      Width           =   675
   End
   Begin VB.CheckBox chkTask 
      Caption         =   "Create"
      Height          =   315
      Index           =   1
      Left            =   780
      Style           =   1  'Graphical
      TabIndex        =   4
      Top             =   2670
      Width           =   1035
   End
   Begin VB.TextBox RptText 
      Height          =   315
      Index           =   0
      Left            =   30
      TabIndex        =   3
      Text            =   "Text1"
      Top             =   960
      Width           =   4605
   End
   Begin VB.CheckBox chkTask 
      Caption         =   "Print"
      Height          =   315
      Index           =   0
      Left            =   1830
      Style           =   1  'Graphical
      TabIndex        =   0
      Top             =   2670
      Width           =   1035
   End
   Begin VB.Label RptLabel 
      Caption         =   "Report Title"
      Height          =   225
      Index           =   3
      Left            =   75
      TabIndex        =   9
      Top             =   720
      Width           =   1455
   End
   Begin VB.Label RptLabel 
      Caption         =   "Alignment:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   2
      Left            =   1740
      TabIndex        =   8
      Top             =   720
      Width           =   765
   End
   Begin VB.Label RptLabel 
      Alignment       =   2  'Center
      Caption         =   "This Dialog is Under Construction"
      Height          =   225
      Index           =   1
      Left            =   870
      TabIndex        =   2
      Top             =   330
      Width           =   3555
   End
   Begin VB.Label RptLabel 
      Alignment       =   2  'Center
      Caption         =   "Label1"
      Height          =   225
      Index           =   0
      Left            =   870
      TabIndex        =   1
      Top             =   60
      Width           =   3555
   End
   Begin VB.Image Image1 
      Height          =   510
      Left            =   60
      Picture         =   "RptDialog.frx":030A
      Top             =   60
      Width           =   690
   End
End
Attribute VB_Name = "RptDialog"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim MyReport As PrintData

Private Sub chkTask_Click(Index As Integer)
    If chkTask(Index).Value = 1 Then
        Select Case Index
            Case 0
                MyReport.PrintReport True
                chkTask(Index).Value = 0
            Case 1
                MyReport.Restart
                MyReport.CreateReportFields
                chkTask(Index).Value = 0
            Case 2
                chkTask(Index).Value = 0
                MyReport.MyParentButton.Value = 0
            Case Else
                chkTask(Index).Value = 0
        End Select
    Else
    End If
End Sub

Private Sub Form_Load()
    Set MyReport = MainDialog.CurReport
    Me.Top = MyReport.Top
    Me.Left = MyReport.Left + MyReport.Width - Me.Width
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    If UnloadMode = 0 Then
        Cancel = True
    End If
End Sub

Private Sub Form_Resize()
    If Me.WindowState = vbMinimized Then
        Me.Hide
    Else
        Me.Show , MainDialog.CurReport
    End If
End Sub

Private Sub RptCheck1_Click(Index As Integer)
    If RptCheck1(Index).Value = 1 Then
        Select Case Index
            Case 0
                MyReport.Frame1.Visible = True
            Case Else
        End Select
    Else
        Select Case Index
            Case 0
                MyReport.Frame1.Visible = False
            Case Else
        End Select
    End If
End Sub

Private Sub RptOpt1_Click(Index As Integer)
    Select Case Index
        Case 0
            RptText(0).Alignment = 0
            MyReport.fldRepTitle.Alignment = ddTXLeft
        Case 1
            RptText(0).Alignment = 2
            MyReport.fldRepTitle.Alignment = ddTXCenter
        Case 2
            RptText(0).Alignment = 1
            MyReport.fldRepTitle.Alignment = ddTXRight
        Case Else
    End Select
End Sub

Private Sub RptText_Change(Index As Integer)
    MyReport.fldRepTitle.Text = RptDialog.RptText(0).Text
End Sub
