Attribute VB_Name = "MyOwnLib"
Option Explicit
Public SyncCursorBusy As Boolean
Public FormatDateTime As String
Public FormatDatePart As String
Public FormatTimePart As String
Public FormatDateOnly As String
Public FormatTimeOnly As String

Private Declare Function GetKeyState Lib "user32" (ByVal nVirtKey As Long) As Integer

'Private Declare Function HtmlHelpTopic Lib "hhctrl.ocx" Alias _
'    "HtmlHelpA" (ByVal hwnd As Long, ByVal lpHelpFile As String, _
'    ByVal wCommand As Long, ByVal dwData As Long) As Long
'Public Sub ShowHtmlHelp(ByVal tHelpFile As String, ByVal tHelpPage As String)
'    Const HH_DISPLAY_TOPIC = &H0
'    On Error Resume Next
'    ' open the help page in a modeless window
'    HtmlHelpTopic MainDialog.hwnd, tHelpFile, HH_DISPLAY_TOPIC, 0
'End Sub

Public Function CedaDateTimeAdd(CedaTime As String, AddMinutes As Long) As String
    Dim Result As String
    Dim tmpSec As String
    Dim tmpTimeValue
    tmpSec = Mid(CedaTime, 13, 2)
    tmpTimeValue = CedaFullDateToVb(CedaTime)
    tmpTimeValue = DateAdd("n", AddMinutes, tmpTimeValue)
    Result = Format(tmpTimeValue, "yyyymmddhhmmss")
    CedaDateTimeAdd = Result
End Function

Public Function GetFieldColNoList(CurTab As TABLib.Tab, FieldList As String) As String
    Dim Result As String
    Dim tmpField As String
    Dim ColNo As Integer
    Dim i As Integer
    Result = ""
    i = 0
    tmpField = "START"
    While tmpField <> ""
        i = i + 1
        tmpField = GetItem(FieldList, i, ",")
        If tmpField <> "" Then
            ColNo = GetRealItemNo(CurTab.LogicalFieldList, tmpField)
            If ColNo >= 0 Then Result = Result & CStr(ColNo) & ","
        End If
    Wend
    If Result <> "" Then Result = Left(Result, Len(Result) - 1)
    GetFieldColNoList = Result
End Function


Public Function RandomReal(HighValue As Double, LowValue As Double) As Double
     Dim RetVal As Double

     RetVal = HighValue + 0.1
     Do Until RetVal >= LowValue And RetVal <= HighValue
         RetVal = (HighValue - LowValue + 1) * Rnd + LowValue
     Loop
     RandomReal = RetVal
End Function

Public Sub CloneGridLayout(NewTab As TABLib.Tab, UseTab As TABLib.Tab)
    Dim i As Integer
    Dim CurCol As Long
    Dim MaxCol As Long
    Dim tmpData As String
    NewTab.ResetContent
    NewTab.FontName = UseTab.FontName
    NewTab.FontSize = UseTab.FontSize
    NewTab.HeaderFontSize = UseTab.HeaderFontSize
    NewTab.LineHeight = UseTab.LineHeight
    NewTab.LeftTextOffset = UseTab.LeftTextOffset
    NewTab.SetTabFontBold True
    NewTab.MainHeader = UseTab.MainHeader
    'NewTab.GridlineColor = vbBlack
    If GridLifeStyle Then
        NewTab.LifeStyle = UseTab.LifeStyle
        NewTab.CursorLifeStyle = UseTab.CursorLifeStyle
        NewTab.SelectColumnBackColor = UseTab.SelectColumnBackColor
        NewTab.SelectColumnTextColor = UseTab.SelectColumnTextColor
    End If
    NewTab.LogicalFieldList = UseTab.LogicalFieldList
    NewTab.HeaderString = UseTab.HeaderString
    NewTab.SetMainHeaderValues UseTab.GetMainHeaderRanges, UseTab.GetMainHeaderValues, UseTab.GetMainHeaderColors
    NewTab.SetMainHeaderFont UseTab.HeaderFontSize, False, False, True, 0, UseTab.FontName
    NewTab.ColumnAlignmentString = UseTab.ColumnAlignmentString
    NewTab.HeaderAlignmentString = UseTab.HeaderAlignmentString
    MaxCol = UseTab.GetColumnCount - 1
    For CurCol = 0 To MaxCol
        tmpData = UseTab.DateTimeGetInputFormatString(CurCol)
        If tmpData <> "" Then
            NewTab.DateTimeSetColumn CurCol
            NewTab.DateTimeSetInputFormatString CurCol, tmpData
            NewTab.DateTimeSetOutputFormatString CurCol, UseTab.DateTimeGetOutputFormatString(CurCol)
            NewTab.DateTimeSetUTCOffsetMinutes CurCol, UseTab.DateTimeGetUTCOffsetMinutes(CurCol)
        End If
    Next
    NewTab.ShowHorzScroller True
    NewTab.ShowVertScroller True
    NewTab.AutoSizeByHeader = True
    NewTab.AutoSizeColumns
End Sub



Public Function CreateEmptyLine(FieldList As String) As String
    Dim Result As String
    Dim ItemNo As Long
    Dim FldNam As String
    Result = ""
    ItemNo = 1
    FldNam = GetRealItem(FieldList, ItemNo, ",")
    While FldNam <> ""
        Result = Result & ","
        ItemNo = ItemNo + 1
        FldNam = GetRealItem(FieldList, ItemNo, ",")
    Wend
    CreateEmptyLine = Result
End Function
Public Function CreateCleanFieldList(FieldList As String) As String
    Dim Result As String
    Dim ItemNo As Long
    Dim FldNam As String
    Result = ""
    ItemNo = 0
    FldNam = GetRealItem(FieldList, ItemNo, ",")
    While FldNam <> ""
        If Left(FldNam, 1) <> "'" Then
            Result = Result & FldNam & ","
        End If
        ItemNo = ItemNo + 1
        FldNam = GetRealItem(FieldList, ItemNo, ",")
    Wend
    Result = Left(Result, Len(Result) - 1)
    CreateCleanFieldList = Result
End Function
Public Function MyDateFormat(CedaDate As String, FullTime As Boolean) As String
    Dim Result As String
    Dim tmpDate
    Result = ""
    If CedaDate <> "" Then
        If FullTime Then
            tmpDate = CedaFullDateToVb(CedaDate)
            Result = Format(tmpDate, "dd mmm yyyy / hh:mm")
        Else
            tmpDate = CedaDateToVb(CedaDate)
            Result = Format(tmpDate, "dd mmm yyyy")
        End If
    End If
    MyDateFormat = UCase(Result)
End Function
Public Function CheckTimeValues(CheckTime As String, AsTimes As Boolean, IsReady As Boolean) As Boolean
    Dim TimeStrg As String
    Dim tmpStrg1 As String
    Dim tmpStrg2 As String
    Dim IsValid As Boolean
    Dim tmpHours As String
    Dim tmpMinute As String
    Dim valH As Integer
    Dim valM As Integer
    Dim chkH As String
    Dim chkM As String
    IsValid = False
    TimeStrg = CheckTime
    If TimeStrg = "" Then IsValid = True
    If Not IsValid Then
        If InStr(TimeStrg, ":") > 0 Then
            If Len(TimeStrg) > 3 Then
                tmpHours = GetRealItem(TimeStrg, 0, ":")
                If Len(tmpHours) < 2 Then tmpHours = Right("00" & tmpHours, 2)
                tmpMinute = GetRealItem(TimeStrg, 1, ":")
                If Len(tmpMinute) < 2 Then tmpMinute = Right("00" & tmpMinute, 2)
                IsValid = True
            End If
        Else
            If Len(TimeStrg) > 2 Then
                tmpMinute = Right(TimeStrg, 2)
                tmpHours = Left(TimeStrg, Len(TimeStrg) - 2)
                IsValid = True
            End If
        End If
        If IsValid Then
            If Len(tmpHours) <> 2 Then IsValid = False
            If Len(tmpMinute) <> 2 Then IsValid = False
            valH = Val(tmpHours)
            chkH = Right("00" & CStr(valH), 2)
            valM = Val(tmpMinute)
            chkM = Right("00" & CStr(valM), 2)
            If chkH <> tmpHours Then IsValid = False
            If chkM <> tmpMinute Then IsValid = False
            If valM > 59 Then IsValid = False
            If (AsTimes) And valH > 23 Then IsValid = False
            If IsValid Then TimeStrg = chkH & ":" & chkM
        End If
    End If
    If IsValid Then
        tmpStrg1 = Replace(CheckTime, ":", "", 1, -1, vbBinaryCompare)
        tmpStrg2 = Replace(TimeStrg, ":", "", 1, -1, vbBinaryCompare)
        If tmpStrg1 = tmpStrg2 Then CheckTime = TimeStrg
    End If
    CheckTimeValues = IsValid
End Function

Public Function SetButtonFaceStyle(CurForm As Form)
    Dim ctl As Control
    Dim strCtlType As String
    On Error Resume Next
    For Each ctl In CurForm.Controls
        strCtlType = TypeName(ctl)
        Select Case strCtlType
            Case "OptionButton", "CheckBox", "Label"
                ctl.BackColor = MyOwnButtonFace
            Case "Frame"
                If InStr(ctl.Name, "Frame") = 0 Then
                    ctl.BackColor = MyOwnButtonFace
                End If
            'Case "StatusBar"
            '    does not work
            Case Else
     End Select
    Next
End Function

Public Sub CheckMonitorArea(UseForm As Form)
    Dim NewTop As Long
    Dim NewLeft As Long
    NewLeft = Screen.Width - UseForm.Width
    If NewLeft < 0 Then NewLeft = 0
    If NewLeft < UseForm.Left Then UseForm.Left = NewLeft
    NewTop = Screen.Height - UseForm.Height
    If NewTop < 0 Then NewTop = 0
    If NewTop < UseForm.Top Then UseForm.Top = NewTop
End Sub
Public Function GetBestScroll(SetLine As Long, OldOffset As Long, VisLines As Long) As Long
    Dim TopLine As Long
    Dim BotLine As Long
    TopLine = SetLine - OldOffset
    If TopLine < 0 Then TopLine = 0
    BotLine = TopLine + VisLines - 1
    If SetLine > BotLine Then TopLine = SetLine - VisLines + 1
    GetBestScroll = TopLine
End Function
Public Function VisibleTabLines(CurTab As TABLib.Tab) As Long
    'Assumes MainHeader, Header and HorizScrollBar
    VisibleTabLines = (((CurTab.Height - 90) / 15) / CurTab.LineHeight) - 3
End Function

Public Function SelectLookupLineNo(CurTab As TABLib.Tab, IndexName As String, LookValue As String, Selected As Boolean, MarkFields As String, ScrollTo As Boolean, CheckField As String, UseMarker As String) As Long
    Dim tmpLine As String
    Dim tmpFieldList As String
    Dim tmpColList As String
    Dim tmpCol As String
    Dim LineNo As Long
    Dim FirstLine As Long
    Dim LineCount As Long
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim ColNo As Long
    Dim ChkColNo As Long
    Dim LstColNo As Long
    Dim ForeColor As Long
    Dim BackColor As Long
    Dim CntColor(0 To 8) As Long
    Dim i As Integer
    ChkColNo = CLng(Val(GetRealItemNo(CurTab.LogicalFieldList, CheckField)))
    LstColNo = ChkColNo + 1
    If ChkColNo >= 0 Then
        CurTab.SetInternalLineBuffer True
        tmpLine = CurTab.GetLinesByColumnValue(ChkColNo, "S", 0)
        If Val(tmpLine) > 0 Then
            LineNo = CurTab.GetNextResultLine
            While LineNo >= 0
                tmpColList = CurTab.GetColumnValue(LineNo, LstColNo)
                i = 0
                tmpCol = "START"
                While tmpCol <> ""
                    i = i + 1
                    tmpCol = GetItem(tmpColList, i, ";")
                    If tmpCol <> "" Then
                        ColNo = Val(tmpCol)
                        CurTab.ResetCellDecoration LineNo, ColNo
                    End If
                    CurTab.SetColumnValue LineNo, ChkColNo, "-"
                    CurTab.SetColumnValue LineNo, LstColNo, ""
                Wend
                LineNo = CurTab.GetNextResultLine
            Wend
        End If
        CurTab.SetInternalLineBuffer False
    Else
        MaxLine = CurTab.GetLineCount - 1
        For CurLine = 0 To MaxLine
            CurTab.ResetLineDecorations CurLine
        Next
    End If
    FirstLine = -1
    LineCount = 0
    CurTab.SetInternalLineBuffer True
    tmpLine = CurTab.GetLinesByIndexValue(IndexName, LookValue, 0)
    If Val(tmpLine) > 0 Then
        tmpFieldList = MarkFields
        If tmpFieldList = "" Then tmpFieldList = GetItem(CurTab.LogicalFieldList, 1, ",")
        LineNo = CurTab.GetNextResultLine
        FirstLine = LineNo
        If LineNo >= 0 Then
            If ScrollTo = True Then
                SyncCursorBusy = True
                CurTab.OnVScrollTo LineNo
                CurTab.SetCurrentSelection LineNo
                SyncCursorBusy = False
            End If
            If Selected Then
                While LineNo >= 0
                    tmpColList = SetTabDecoCellObjects(CurTab, LineNo, "DECO", UseMarker, tmpFieldList)
                    If ChkColNo >= 0 Then
                        CurTab.SetColumnValue LineNo, ChkColNo, "S"
                        CurTab.SetColumnValue LineNo, ChkColNo + 1, tmpColList
                    End If
                    LineCount = LineCount + 1
                    LineNo = CurTab.GetNextResultLine
                Wend
            End If
        End If
    Else
        If ScrollTo = True Then
            SyncCursorBusy = True
            CurTab.SetCurrentSelection -1
            SyncCursorBusy = False
        End If
    End If
    CurTab.SetInternalLineBuffer False
    CurTab.Refresh
    SelectLookupLineNo = FirstLine
End Function

Public Function SetTabDecoCellObjects(CurTab As TABLib.Tab, LineNo As Long, ObjType As String, CellObjName As String, FieldList As String) As String
    Dim Result As String
    Dim tmpField As String
    Dim ColNo As Integer
    Dim i As Integer
    Result = ""
    i = 0
    tmpField = "START"
    While tmpField <> ""
        i = i + 1
        tmpField = GetItem(FieldList, i, ",")
        If tmpField <> "" Then
            ColNo = GetRealItemNo(CurTab.LogicalFieldList, tmpField)
            If ColNo >= 0 Then
                Select Case ObjType
                    Case "DECO"
                        CurTab.SetDecorationObject LineNo, ColNo, CellObjName
                    Case "MARKER"
                        CurTab.SetCellProperty LineNo, ColNo, CellObjName
                    Case Else
                End Select
                Result = ";" & Result & CStr(ColNo)
            End If
        End If
    Wend
    Result = Mid(Result, 2)
    SetTabDecoCellObjects = Result
End Function

Public Sub MarkLookupLines(CurTab As TABLib.Tab, IndexName As String, LookValue As String, Selected As Boolean, MarkFieldList As String, ScrollTo As Boolean, CheckField As String, UseMarker As String)
    Dim tmpLine As String
    Dim MarkField As String
    Dim LineNo As Long
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim ChkColNo As Long
    Dim LstColNo As Long
    Dim FldColNo As Long
    Dim FldNo As Integer
    ChkColNo = CLng(GetRealItemNo(CurTab.LogicalFieldList, CheckField))
    LstColNo = ChkColNo + 1
    If ChkColNo >= 0 Then
        CurTab.SetInternalLineBuffer True
        tmpLine = CurTab.GetLinesByColumnValue(ChkColNo, "S", 0)
        If Val(tmpLine) > 0 Then
            LineNo = CurTab.GetNextResultLine
            While LineNo >= 0
                FldNo = 0
                MarkField = "START"
                While MarkField <> ""
                    FldNo = FldNo + 1
                    MarkField = GetItem(MarkFieldList, FldNo, ",")
                    If MarkField <> "" Then
                        FldColNo = CLng(GetRealItemNo(CurTab.LogicalFieldList, MarkField))
                        CurTab.ResetCellDecoration LineNo, FldColNo
                    End If
                Wend
                CurTab.SetColumnValue LineNo, ChkColNo, ""
                LineNo = CurTab.GetNextResultLine
            Wend
        End If
        CurTab.SetInternalLineBuffer False
    Else
        MaxLine = CurTab.GetLineCount - 1
        For CurLine = 0 To MaxLine
            CurTab.ResetLineDecorations CurLine
        Next
    End If
    CurTab.SetInternalLineBuffer True
    tmpLine = CurTab.GetLinesByIndexValue(IndexName, LookValue, 0)
    If Val(tmpLine) > 0 Then
        LineNo = CurTab.GetNextResultLine
        If LineNo >= 0 Then
            If ScrollTo = True Then
                SyncCursorBusy = True
                CurTab.OnVScrollTo LineNo
                CurTab.SetCurrentSelection LineNo
                SyncCursorBusy = False
            End If
            If Selected Then
                While LineNo >= 0
                    FldNo = 0
                    MarkField = "START"
                    While MarkField <> ""
                        FldNo = FldNo + 1
                        MarkField = GetItem(MarkFieldList, FldNo, ",")
                        If MarkField <> "" Then
                            FldColNo = CLng(GetRealItemNo(CurTab.LogicalFieldList, MarkField))
                            CurTab.SetDecorationObject LineNo, FldColNo, UseMarker
                        End If
                    Wend
                    CurTab.SetColumnValue LineNo, ChkColNo, "S"
                    LineNo = CurTab.GetNextResultLine
                Wend
            End If
        End If
    ElseIf ScrollTo = True Then
        SyncCursorBusy = True
        CurTab.SetCurrentSelection -1
        SyncCursorBusy = False
    End If
    CurTab.SetInternalLineBuffer False
    CurTab.Refresh
End Sub

Public Sub TabAlternateColors(CurTab As TABLib.Tab, KeyFields As String, Color1 As Long, Color2 As Long)
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim ForeColor As Long
    Dim BackColor As Long
    Dim SetColor As Long
    Dim NewValues As String
    Dim OldValues As String
    OldValues = "X"
    SetColor = Color2
    MaxLine = CurTab.GetLineCount - 1
    For CurLine = 0 To MaxLine
        NewValues = CurTab.GetFieldValues(CurLine, KeyFields)
        If NewValues <> OldValues Then
            If SetColor = Color1 Then SetColor = Color2 Else SetColor = Color1
            OldValues = NewValues
        End If
        CurTab.GetLineColor CurLine, ForeColor, BackColor
        CurTab.SetLineColor CurLine, ForeColor, SetColor
    Next
    CurTab.Refresh
End Sub

Public Function TranslateColorCode(ColorCode As String, DefaultColor As Long) As Long
    Select Case UCase(ColorCode)
        Case "LIGHTYELLOW"
            TranslateColorCode = LightYellow
        Case "LIGHTGRAY", "LIGHTGREY"
            TranslateColorCode = LightGray
        Case "LIGHTGREEN"
            TranslateColorCode = LightGreen
        Case Else
            TranslateColorCode = DefaultColor
    End Select
End Function

Public Sub SetTabSortCols(CurTab As TABLib.Tab, FieldList As String)
    Dim tmpField As String
    Dim ColNo As Integer
    Dim i As Integer
    i = 0
    tmpField = "START"
    While tmpField <> ""
        i = i + 1
        tmpField = GetItem(FieldList, i, ",")
        If tmpField <> "" Then
            ColNo = GetRealItemNo(CurTab.LogicalFieldList, tmpField)
            If ColNo >= 0 Then CurTab.Sort CStr(ColNo), True, True
        End If
    Wend
    'CurTab.AutoSizeColumns
End Sub

Public Function GetLookupFieldValues(CurTab As TABLib.Tab, IndexName As String, LookValue As String, GetFields As String, HitCount As Long, FirstLine As Long) As String
    Dim Result As String
    Dim tmpLine As String
    Dim LineNo As Long
    Dim CurLine As Long
    Dim MaxLine As Long
    Result = ""
    FirstLine = -1
    CurTab.SetInternalLineBuffer True
    tmpLine = CurTab.GetLinesByIndexValue(IndexName, LookValue, 0)
    HitCount = Val(tmpLine)
    If HitCount > 0 Then
        LineNo = CurTab.GetNextResultLine
        If LineNo >= 0 Then
            FirstLine = LineNo
            Result = CurTab.GetFieldValues(LineNo, GetFields)
            While LineNo >= 0
                LineNo = CurTab.GetNextResultLine
                If LineNo >= 0 Then
                    'Might be we need all lines
                End If
            Wend
        End If
    End If
    CurTab.SetInternalLineBuffer False
    GetLookupFieldValues = Result
End Function

Public Function CreateCedaDateTimeSsimFormat(CedaDateTime As String)
    Dim tmpDate As String
    Dim tmpTime As String
    Dim tmpScnd As String
    Dim tmpMsec As String
    Dim tmpValu As String
    tmpValu = Trim(CedaDateTime)
    If Len(tmpValu) >= 8 Then
        tmpDate = Left(tmpValu, 8)
        tmpTime = Mid(tmpValu, 9)
        tmpDate = DecodeSsimDayFormat(tmpDate, "CEDA", "SSIM2")
        tmpValu = tmpDate
        If tmpTime <> "" Then
            tmpValu = tmpValu & " / "
            tmpValu = tmpValu & Left(tmpTime, 2) & ":" & Mid(tmpTime, 3, 2)
            tmpScnd = Mid(tmpTime, 5, 2)
            tmpMsec = Mid(tmpTime, 7, 3)
            If ((tmpScnd <> "") And (tmpScnd <> "00")) Or (tmpMsec <> "") Then
                tmpValu = tmpValu & ":" & tmpScnd
            End If
        End If
    End If
    CreateCedaDateTimeSsimFormat = tmpValu
End Function

Public Function SaveLoadedFile(UsePathFile As String, DataType As String, FileFilter As String) As String
    Dim FileCount As Integer
    Dim myLoadName As String
    Dim myLoadPath As String
    FileCount = GetFileAndPath(UsePathFile, myLoadName, myLoadPath)
    UfisTools.CommonDialog.DialogTitle = "Save Data As: " & DataType
    UfisTools.CommonDialog.InitDir = myLoadPath
    UfisTools.CommonDialog.FileName = myLoadName
    UfisTools.CommonDialog.Filter = FileFilter
    UfisTools.CommonDialog.CancelError = True
    On Error GoTo ErrorHandle
    UfisTools.CommonDialog.ShowSave
    If UfisTools.CommonDialog.FileName <> "" Then
        SaveLoadedFile = UfisTools.CommonDialog.FileName
    End If
    Exit Function
ErrorHandle:
    myLoadName = ""
    myLoadPath = ""
    SaveLoadedFile = ""
    Exit Function
End Function

Public Function keyCheck() As Integer
    Dim tmpMsg As String
    If GetKeyState(vbKeyTab) < 0 Then
        keyCheck = vbKeyTab
    ElseIf GetKeyState(vbKeyLeft) < 0 Then
        keyCheck = vbKeyLeft
    ElseIf GetKeyState(vbKeyRight) < 0 Then
        keyCheck = vbKeyRight
    ElseIf GetKeyState(vbKeyUp) < 0 Then
        keyCheck = vbKeyUp
    ElseIf GetKeyState(vbKeyDown) < 0 Then
        keyCheck = vbKeyDown
    End If
End Function

Public Sub SearchInTabList(CurTab As TABLib.Tab, ColNo As Long, CurText As String, NewText As Boolean, KeyCols As String, KeyValues As String)
    Dim HitLst As String
    Dim HitRow As Long
    Dim tmpText As String
    Dim LoopCount As Integer
    Dim KeyColNbr As String
    Dim KeyValue As String
    Dim tmpData As String
    Dim CheckIt As Boolean
    Dim KeyFound As Boolean
    Dim LineNo As Long
    Dim KeyCol As Long
    Dim i As Integer
    tmpText = Trim(CurText)
    If tmpText <> "" Then
        CheckIt = True
        If NewText = True Then
            LineNo = CurTab.GetCurrentSelected
            If LineNo >= 0 Then
                tmpData = CurTab.GetColumnValue(LineNo, ColNo)
                If InStr(tmpData, tmpText) > 0 Then CheckIt = False
            End If
        End If
        If CheckIt Then
            LoopCount = 0
            If KeyCols = "" Then
                Do
                    HitLst = CurTab.GetNextLineByColumnValue(ColNo, tmpText, 1)
                    If HitLst <> "" Then
                        HitRow = Val(HitLst)
                        CurTab.OnVScrollTo HitRow
                        CurTab.SetCurrentSelection HitRow
                        LoopCount = LoopCount + 1
                    Else
                        HitRow = -1
                        CurTab.SetCurrentSelection HitRow
                        LoopCount = LoopCount + 1
                    End If
                Loop While LoopCount < 2 And HitLst = ""
            Else
                SyncCursorBusy = True
                Do
                    HitLst = CurTab.GetNextLineByColumnValue(ColNo, tmpText, 1)
                    If HitLst <> "" Then
                        HitRow = Val(HitLst)
                        CurTab.OnVScrollTo HitRow
                        KeyFound = True
                        KeyCol = -1
                        i = 0
                        Do
                            i = i + 1
                            KeyColNbr = GetItem(KeyCols, i, ",")
                            If KeyColNbr <> "" Then
                                KeyValue = GetItem(KeyValues, i, ",")
                                KeyCol = Val(KeyColNbr)
                                tmpData = CurTab.GetColumnValue(HitRow, KeyCol)
                                If InStr(tmpData, KeyValue) <= 0 Then KeyFound = False
                            End If
                        Loop While (KeyFound = True) And (KeyColNbr <> "")
                        CurTab.SetCurrentSelection HitRow
                        If KeyFound = True Then LoopCount = 2
                        HitLst = ""
                    Else
                        HitRow = -1
                        CurTab.SetCurrentSelection HitRow
                        LoopCount = LoopCount + 1
                    End If
                Loop While LoopCount < 2 And HitLst = ""
                SyncCursorBusy = False
                CurTab.SetCurrentSelection HitRow
            End If
            CurTab.Refresh
        End If
    End If
End Sub

Public Sub CloneThisTab(UseTab As TABLib.Tab, NewTab As TABLib.Tab)
    NewTab.ResetContent
    NewTab.FontName = UseTab.FontName
    NewTab.FontSize = UseTab.FontSize
    NewTab.HeaderFontSize = UseTab.HeaderFontSize
    NewTab.LineHeight = UseTab.LineHeight
    NewTab.LeftTextOffset = UseTab.LeftTextOffset
    NewTab.SetTabFontBold True
    NewTab.HeaderString = UseTab.HeaderString
    NewTab.HeaderAlignmentString = UseTab.HeaderAlignmentString
    NewTab.HeaderLengthString = UseTab.HeaderLengthString
    NewTab.LogicalFieldList = UseTab.LogicalFieldList
    NewTab.ColumnAlignmentString = UseTab.ColumnAlignmentString
    NewTab.ColumnWidthString = UseTab.ColumnWidthString
    NewTab.SetMainHeaderValues UseTab.GetMainHeaderRanges, UseTab.GetMainHeaderValues, UseTab.GetMainHeaderColors
    NewTab.SetMainHeaderFont UseTab.HeaderFontSize, False, False, True, 0, UseTab.FontName
    NewTab.MainHeader = UseTab.MainHeader
    NewTab.GridlineColor = UseTab.GridlineColor
    NewTab.LifeStyle = UseTab.LifeStyle
    NewTab.CursorLifeStyle = UseTab.CursorLifeStyle
    NewTab.SelectColumnBackColor = UseTab.SelectColumnBackColor
    NewTab.SelectColumnTextColor = UseTab.SelectColumnTextColor
    NewTab.ShowHorzScroller True
    NewTab.ShowVertScroller True
    NewTab.AutoSizeByHeader = UseTab.AutoSizeByHeader
End Sub
