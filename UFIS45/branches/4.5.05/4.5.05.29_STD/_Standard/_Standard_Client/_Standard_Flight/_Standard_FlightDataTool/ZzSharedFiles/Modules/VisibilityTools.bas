Attribute VB_Name = "VisibilityTools"
Option Explicit

' *** Den Artikel zu diesem Modul finden Sie unter http://www.aboutvb.de/khw/artikel/khwwindowvisible.htm ***

Private Type RECT
    Left As Long
    Top As Long
    Right As Long
    Bottom As Long
End Type

Private Declare Function EqualRect Lib "user32" (lpRect1 As RECT, lpRect2 As RECT) As Long
Private Declare Function GetClientRect Lib "user32" (ByVal hWnd As Long, lpRect As RECT) As Long
Private Declare Function GetClipBox Lib "gdi32" (ByVal hDC As Long, lpRect As RECT) As Long

Public Declare Function PointVisible Lib "gdi32" Alias "PtVisible" (ByVal hDC As Long, ByVal x As Long, ByVal y As Long) As Long
Public Declare Function RectVisible Lib "gdi32" (ByVal hDC As Long, lpRect As RECT) As Long

Public Enum WindowVisibleConstants
    wvHidden
    wvPartialVisible
    wvCompleteVisible
End Enum

Public Function WindowVisible(hWnd As Long, ByVal hDC As Long) As WindowVisibleConstants
    Dim nClientRect As RECT
    Dim nClipRect As RECT
    Dim nTestClip As Long
    Dim nTestEqual As Boolean
    
    Const NULLREGION = 1
    Const SIMPLEREGION = 2
    Const COMPLEXREGION = 3
    
    GetClientRect hWnd, nClientRect
    nTestClip = GetClipBox(hDC, nClipRect)
    nTestEqual = CBool(EqualRect(nClientRect, nClipRect))
    Select Case nTestClip
        Case COMPLEXREGION
            WindowVisible = wvPartialVisible
        Case SIMPLEREGION
            If nTestEqual Then
                WindowVisible = wvCompleteVisible
            Else
                WindowVisible = wvPartialVisible
            End If
        Case NULLREGION
            WindowVisible = wvHidden
    End Select
End Function

Public Function WindowVisibleObj(Object As Object) As WindowVisibleConstants
    Dim nClientRect As RECT
    Dim nClipRect As RECT
    Dim nTestClip As Long
    Dim nTestEqual As Boolean
    
    Const NULLREGION = 1
    Const SIMPLEREGION = 2
    Const COMPLEXREGION = 3
    
    With Object
        GetClientRect .hWnd, nClientRect
        nTestClip = GetClipBox(.hDC, nClipRect)
    End With
    nTestEqual = CBool(EqualRect(nClientRect, nClipRect))
    Select Case nTestClip
        Case COMPLEXREGION
            WindowVisibleObj = wvPartialVisible
        Case SIMPLEREGION
            If nTestEqual Then
                WindowVisibleObj = wvCompleteVisible
            Else
                WindowVisibleObj = wvPartialVisible
            End If
        Case NULLREGION
            WindowVisibleObj = wvHidden
    End Select
End Function

