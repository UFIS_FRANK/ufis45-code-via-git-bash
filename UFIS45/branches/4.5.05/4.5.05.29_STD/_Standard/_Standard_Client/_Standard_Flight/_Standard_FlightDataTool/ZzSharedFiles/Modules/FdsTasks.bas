Attribute VB_Name = "FdsTasks"
Option Explicit
'System Record Prefix Area
'Values set in MainDialog.InitTabLayout
Public FdsCbCol As Long
Public FdsL1Col As Long
Public FdsL2Col As Long
Public FdsL3Col As Long
Public FdsL4Col As Long
Public FdsL5Col As Long
'System Record Trail Area
'These values are different per grid layout
'Values must be set per tab (SetFdsKeyCols)
Public FdsK1Col As Long
Public FdsK2Col As Long
Public FdsK3Col As Long
Public FdsK4Col As Long
Public FdsK5Col As Long
Public Sub SetFdsKeyCols(CurTab As TABLib.Tab)
    FdsK1Col = CLng(GetRealItemNo(CurTab.LogicalFieldList, "'K1'"))
    FdsK2Col = CLng(GetRealItemNo(CurTab.LogicalFieldList, "'K2'"))
    FdsK3Col = CLng(GetRealItemNo(CurTab.LogicalFieldList, "'K3'"))
    FdsK4Col = CLng(GetRealItemNo(CurTab.LogicalFieldList, "'K4'"))
    FdsK5Col = CLng(GetRealItemNo(CurTab.LogicalFieldList, "'K5'"))
End Sub
Public Sub FdsCleanGrids(Index As Integer, CurTab As TABLib.Tab)
    Dim TabProp As String
    Dim TblName As String
    Dim TblMeth As String
    Dim GridKey As String
    Dim KeyList As String
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim PrgStep As Long
    Dim FdsL5Key As String
    Dim FdsSetFld As String
    Dim FdsSetDat As String
    Dim InfoText As String
    MainDialog.lblInfo(1).Visible = True
    MainDialog.lblInfo(2).Visible = True
    TabProp = MainDialog.GetTabProperty(Index, "TABLE_INFO", 1, "")
    TblName = GetItem(TabProp, 1, ":")
    TblMeth = GetItem(TabProp, 2, ":")
    GridKey = Replace(TabProp, ",", ":", 1, -1, vbBinaryCompare)
    KeyList = MyConfig.GetFdsTableKeyFields(GridKey)
    If TblMeth <> "PLAIN" Then
        'Temporay Hotfix
        KeyList = ""
    End If
    If KeyList <> "" Then
        InfoText = "Initial Data Cleaning"
        FdsSetFld = MainDialog.FdsPrefixFields
        FdsSetFld = FdsSetFld & ",'AF','AT','DF','DT','K3'"
        MainDialog.lblInfo(1).Caption = InfoText
        MainDialog.lblInfo(2).Caption = InfoText
        MainDialog.lblInfo(1).Refresh
        MainDialog.FdsCompProgress "SUB_INIT", LightGreen, 0
        MainDialog.lblCurPos(4).Caption = "Clean Data"
        MaxLine = CurTab.GetLineCount - 1
        PrgStep = MaxLine / 20
        If PrgStep < 50 Then PrgStep = 50
        If PrgStep > 500 Then PrgStep = 500
        For CurLine = 0 To MaxLine
            If (CurLine Mod PrgStep) = 0 Then
                MainDialog.FdsCompProgress "SUB_VALUE", CurLine + 1, MaxLine + 1
                CurTab.OnVScrollTo CurLine
                'MainDialog.AdjustImpButtonPanel 4, True, CStr(Index) & ",K," & CurTabIndicator
            End If
            FdsSetDat = ""
            FdsL5Key = CurTab.GetColumnValue(CurLine, FdsL5Col)
            If FdsL5Key = "" Then FdsL5Key = "#"
            Select Case FdsL5Key
                Case "D"
                Case Else
                    FdsSetDat = ",,,,,N,,,,,"
            End Select
            If FdsSetDat <> "" Then
                CurTab.SetFieldValues CurLine, FdsSetFld, FdsSetDat
                CurTab.SetLineColor CurLine, vbBlack, vbWhite
            End If
        Next
    End If
End Sub
Public Sub FdsKeyCreation(Index As Integer, CurTab As TABLib.Tab, CurTabIndicator As String)
    Dim TabProp As String
    Dim TblName As String
    Dim TblMeth As String
    Dim GridKey As String
    Dim KeyList As String
    Dim KeyData1 As String
    Dim KeyData2 As String
    Dim GetField As String
    Dim GetValue As String
    Dim SetFields As String
    Dim SetValues As String
    Dim InfoText As String
    Dim ColSize As String
    Dim SfxChkZKey As String
    Dim SfxSetZKey As String
    Dim tmpData As String
    Dim tmpValu As String
    Dim tmpName As String
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim PrgStep As Long
    Dim KeyCol As Long
    Dim KeySize1 As Long
    Dim KeySize2 As Long
    Dim TxtColor As Long
    Dim BckColor As Long
    Dim ImpLblIdx As Integer
    Dim i As Integer
    MainDialog.lblInfo(1).Visible = True
    MainDialog.lblInfo(2).Visible = True
    TabProp = MainDialog.GetTabProperty(Index, "TABLE_INFO", 1, "")
    TblName = GetItem(TabProp, 1, ":")
    TblMeth = GetItem(TabProp, 2, ":")
    GridKey = Replace(TabProp, ",", ":", 1, -1, vbBinaryCompare)
    KeyList = MyConfig.GetFdsTableKeyFields(GridKey)
    If TblMeth <> "PLAIN" Then
        'Temporay Hotfix
        KeyList = ""
    End If
    If KeyList <> "" Then
        SfxChkZKey = "#Z*"
        SfxSetZKey = "##*"
        InfoText = "Generic Data Key Creation"
        MainDialog.lblInfo(1).Caption = InfoText
        MainDialog.lblInfo(2).Caption = InfoText
        MainDialog.lblInfo(1).Refresh
        CurTab.IndexDestroy "'K1'"
        MainDialog.FdsCompProgress "SUB_INIT", LightGreen, 0
        MainDialog.lblCurPos(4).Caption = "Data Keys"
        SetFdsKeyCols CurTab
        MaxLine = CurTab.GetLineCount - 1
        PrgStep = MaxLine / 20
        If PrgStep < 50 Then PrgStep = 50
        If PrgStep > 500 Then PrgStep = 500
        KeySize1 = 10
        KeySize2 = 10
        For CurLine = 0 To MaxLine
            If (CurLine Mod PrgStep) = 0 Then
                MainDialog.FdsCompProgress "SUB_VALUE", CurLine + 1, MaxLine + 1
                CurTab.OnVScrollTo CurLine
                MainDialog.AdjustImpButtonPanel 4, True, CStr(Index) & ",K," & CurTabIndicator
            End If
            KeyData1 = GetKeyDataFormat(Index, CurTab, CurLine, KeyList, True)
            KeyData2 = GetKeyDataFormat(Index, CurTab, CurLine, KeyList, False)
            If InStr(KeyData1, SfxChkZKey) > 0 Then
                'We keep the Main Data Key still ambiguous ...
                KeyData1 = Replace(KeyData1, SfxChkZKey, SfxSetZKey, 1, -1, vbBinaryCompare)
                '... but we create a more unique Main Sort Key and ...
                KeyData2 = Replace(KeyData2, SfxChkZKey, SfxSetZKey, 1, -1, vbBinaryCompare)
                '... and Bring Z flights up to the front line ...
                KeyData2 = Replace(KeyData2, "*?*", "*-*", 1, -1, vbBinaryCompare)
                '... or to the last line (?)
                'KeyData2 = Replace(KeyData2, "*?*", "*Z*", 1, -1, vbBinaryCompare)
                CurTab.SetColumnValue CurLine, FdsL2Col, "U"
            End If
            SetFields = "'K1','K2'"
            SetValues = KeyData1 & "," & KeyData2
            CurTab.SetFieldValues CurLine, SetFields, SetValues
            If Len(KeyData1) > KeySize1 Then KeySize1 = Len(KeyData1)
            If Len(KeyData2) > KeySize2 Then KeySize2 = Len(KeyData2)
            tmpName = ""
            BckColor = -1
            ImpLblIdx = -1
            Select Case Index
                Case TabArrFlightsIdx
                    tmpName = "FTYP"
                Case TabDepFlightsIdx
                    tmpName = "FTYP"
                Case Else
            End Select
            If tmpName <> "" Then
                tmpData = CurTab.GetFieldValue(CurLine, tmpName)
                Select Case tmpName
                    Case "FTYP"
                        Select Case tmpData
                            Case "N", "X"
                                tmpValu = tmpData
                                ImpLblIdx = 6
                            Case "Z", "B"
                                tmpValu = "R"
                                ImpLblIdx = 7
                            Case Else
                                tmpValu = ""
                        End Select
                        If tmpValu <> "" Then CurTab.SetColumnValue CurLine, FdsL1Col, tmpData
                    Case Else
                End Select
            End If
            If ImpLblIdx >= 0 Then CurTab.SetLineColor CurLine, MainDialog.CliFkeyCnt(ImpLblIdx).ForeColor, MainDialog.CliFkeyCnt(ImpLblIdx).BackColor
            DoEvents
        Next
        InfoText = "Sorting Internal Index"
        MainDialog.lblInfo(1).Caption = InfoText
        MainDialog.lblInfo(2).Caption = InfoText
        MainDialog.lblInfo(1).Refresh
        ColSize = CurTab.ColumnWidthString
        KeyCol = FdsK2Col
        SetItem ColSize, CInt(KeyCol + 1), ",", CStr(KeySize2)
        CurTab.ColumnWidthString = ColSize
        CurTab.Sort CStr(KeyCol), True, True
        KeyCol = FdsK1Col
        SetItem ColSize, CInt(KeyCol + 1), ",", CStr(KeySize1)
        CurTab.ColumnWidthString = ColSize
        CurTab.IndexCreate "'K1'", KeyCol
        CurTab.AutoSizeColumns
        MainDialog.FdsCompProgress "SUB_VALUE", 100, 100
    End If
    MainDialog.AdjustImpButtonPanel 4, True, CStr(Index) & ",K," & CurTabIndicator
End Sub

Public Sub FdsDataCheck(Index As Integer, CurTab As TABLib.Tab, ForWhat As Integer)
    Dim TabProp As String
    Dim TblName As String
    Dim TblMeth As String
    Dim GridKey As String
    Dim KeyList As String
    Dim KeyData As String
    Dim GetField As String
    Dim GetValue As String
    Dim SetFields As String
    Dim SetValues As String
    Dim InfoText As String
    Dim PrvUtcFKey As String
    Dim PrvLocFKey As String
    Dim PrvAdid As String
    Dim PrvSked As String
    Dim PrvLocDate As String
    Dim PrvUtcDate As String
    
    Dim FstFkey As String
    Dim FstAdid As String
    Dim FstSked As String
    Dim FstLocDate As String
    Dim FstUtcDate As String
    
    Dim CurUtcFKey As String
    Dim CurLocFKey As String
    Dim CurAdid As String
    Dim CurSked As String
    Dim ChkFlag As String
    Dim CurData As String
    Dim CurLocDate As String
    Dim CurUtcDate As String
    Dim CurTabIndicator As String
    Dim CurLineTag As String
    'Dim PrvLineTag As String
    Dim CurL1Data As String
    Dim CurL2Data As String
    Dim CurL3Data As String
    Dim CurL4Data As String
    Dim CurL5Data As String
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim PrvLine As Long
    Dim FstUtcLine As Long
    Dim FstLocLine As Long
    Dim PrgStep As Long
    Dim Key1Col As Long
    Dim Key2Col As Long
    Dim StatCol As Long
    Dim AnyDate 'As Variant
    Dim CheckDoubleFlights As Boolean
    Dim ReorgIndex As Boolean
    Dim IsCheched As Boolean
    Dim i As Integer
    ReorgIndex = False
    CheckDoubleFlights = False
    If Index = TabArrFlightsIdx Then CheckDoubleFlights = True
    If Index = TabDepFlightsIdx Then CheckDoubleFlights = True
    MainDialog.lblInfo(1).Visible = True
    MainDialog.lblInfo(2).Visible = True
    'ForWhat 0=Production (Needs Basic Data Check)
    'ForWhat 1=Test Server (No Need of Basic Data Check)
    Select Case ForWhat
        Case 0
            CurTabIndicator = "S"
        Case 1
            CurTabIndicator = "T"
        Case Else
            CurTabIndicator = "?"
    End Select
    SetFdsKeyCols CurTab
    StatCol = CLng(GetRealItemNo(CurTab.LogicalFieldList, "'ST'"))
    Key1Col = FdsK1Col
    Key2Col = FdsK2Col
    TabProp = MainDialog.GetTabProperty(Index, "TABLE_INFO", 1, "")
    TblName = GetItem(TabProp, 1, ":")
    TblMeth = GetItem(TabProp, 2, ":")
    GridKey = Replace(TabProp, ",", ":", 1, -1, vbBinaryCompare)
    KeyList = MyConfig.GetFdsTableKeyFields(GridKey)
    If TblMeth <> "PLAIN" Then
        'Temporay Hotfix
        KeyList = ""
    End If
    If KeyList <> "" Then
        InfoText = "Attaching Grid Information"
        MainDialog.lblInfo(1).Caption = InfoText
        MainDialog.lblInfo(2).Caption = InfoText
        MainDialog.lblInfo(1).Refresh
        MainDialog.lblCurPos(4).Caption = "Data Check"
        CurTab.SetInternalLineBuffer True
        MaxLine = Val(CurTab.GetLinesByColumnValue(FdsCbCol, "Y", 0))
        CurTab.SetInternalLineBuffer False
        If MaxLine > 0 Then
            MaxLine = CurTab.GetLineCount - 1
            PrgStep = MaxLine / 20
            If PrgStep < 50 Then PrgStep = 50
            If PrgStep > 500 Then PrgStep = 500
            For CurLine = 0 To MaxLine
                If (CurLine Mod PrgStep) = 0 Then
                    MainDialog.FdsCompProgress "SUB_VALUE", CurLine + 1, MaxLine + 1
                    CurTab.OnVScrollTo CurLine
                End If
                CurTab.ResetLineDecorations CurLine
                ChkFlag = CurTab.GetColumnValue(CurLine, FdsL2Col)
                If ChkFlag = "" Then ChkFlag = "#"
                Select Case ChkFlag
                    'Case "U", "Z"
                    '    CurTab.SetLineColor CurLine, MainDialog.CliFkeyCnt(2).ForeColor, MainDialog.CliFkeyCnt(2).BackColor
                    Case "Z"
                        CurTab.SetLineColor CurLine, MainDialog.CliFkeyCnt(3).ForeColor, MainDialog.CliFkeyCnt(3).BackColor
                    Case Else
                        CurTab.SetColumnValue CurLine, FdsL2Col, ""
                        CurTab.SetLineColor CurLine, MainDialog.CliFkeyCnt(3).ForeColor, MainDialog.CliFkeyCnt(3).BackColor
                End Select
                ChkFlag = CurTab.GetColumnValue(CurLine, FdsL3Col)
                If ChkFlag = "" Then ChkFlag = "#"
                Select Case ChkFlag
                    Case "U"
                        CurTab.SetLineColor CurLine, MainDialog.CliFkeyCnt(2).ForeColor, MainDialog.CliFkeyCnt(2).BackColor
                    'Case "Z"
                    '    CurTab.SetLineColor CurLine, MainDialog.CliFkeyCnt(3).ForeColor, MainDialog.CliFkeyCnt(3).BackColor
                    Case Else
                        CurTab.SetColumnValue CurLine, FdsL3Col, ""
                        CurTab.SetLineColor CurLine, MainDialog.CliFkeyCnt(3).ForeColor, MainDialog.CliFkeyCnt(3).BackColor
                End Select
                'CurTab.SetColumnValue CurLine, FdsCbCol, "N"
                'CurTab.SetColumnValue CurLine, FdsL3Col, ""
                CurTab.SetColumnValue CurLine, FdsL4Col, ""
                CurTab.SetColumnValue CurLine, FdsL5Col, ""
                CurTab.SetColumnValue CurLine, StatCol, ""
                DoEvents
            Next
        End If
        MainDialog.FdsCompProgress "SUB_VALUE", 100, 100
        CurTab.OnVScrollTo 0
        CurTab.Sort CStr(Key2Col), True, True
        CurTab.Refresh
        InfoText = "Data Consistency Check"
        MainDialog.lblInfo(1).Caption = InfoText
        MainDialog.lblInfo(2).Caption = InfoText
        MainDialog.lblInfo(1).Refresh
        MaxLine = CurTab.GetLineCount - 1
        PrgStep = MaxLine / 20
        If PrgStep < 50 Then PrgStep = 50
        If PrgStep > 500 Then PrgStep = 500
        PrvUtcFKey = "START"
        PrvLocFKey = "STARTSTARTSTARTSTART"
        FstUtcLine = -1
        FstLocLine = -1
        For CurLine = 0 To MaxLine
            IsCheched = False
            If (CurLine Mod PrgStep) = 0 Then
                MainDialog.FdsCompProgress "SUB_VALUE", CurLine + 1, MaxLine + 1
                CurTab.OnVScrollTo CurLine
                MainDialog.AdjustImpButtonPanel 4, True, CStr(Index) & ",B," & CurTabIndicator
            End If
            If CheckDoubleFlights Then
                CurUtcFKey = CurTab.GetColumnValue(CurLine, Key1Col)
                PrvLine = CurLine - 1
                CurAdid = CurTab.GetFieldValue(CurLine, "ADID")
                If CurAdid = "B" Then
                    If Index = TabArrFlightsIdx Then
                        CurAdid = "A"
                    ElseIf Index = TabDepFlightsIdx Then
                        CurAdid = "D"
                    End If
                End If
                PrvAdid = CurTab.GetFieldValue(PrvLine, "ADID")
                If PrvAdid = "B" Then
                    If Index = TabArrFlightsIdx Then
                        PrvAdid = "A"
                    ElseIf Index = TabDepFlightsIdx Then
                        PrvAdid = "D"
                    End If
                End If
                If PrvAdid = CurAdid Then
                    If CurAdid = "A" Then
                        CurSked = CurTab.GetFieldValue(CurLine, "STOA")
                    Else
                        CurSked = CurTab.GetFieldValue(CurLine, "STOD")
                    End If
                    CurUtcDate = Left(CurSked, 8)
                    AnyDate = CedaFullDateToVb(CurSked)
                    AnyDate = DateAdd("n", UtcTimeDiff, AnyDate)
                    CurLocDate = Format(AnyDate, "yyyymmdd")
                    If PrvAdid = "A" Then
                        PrvSked = CurTab.GetFieldValue(PrvLine, "STOA")
                    Else
                        PrvSked = CurTab.GetFieldValue(PrvLine, "STOD")
                    End If
                    PrvUtcDate = Left(PrvSked, 8)
                    AnyDate = CedaFullDateToVb(PrvSked)
                    AnyDate = DateAdd("n", UtcTimeDiff, AnyDate)
                    PrvLocDate = Format(AnyDate, "yyyymmdd")
                
                    'Check for UTC Flight Duplication
                    '(Those are Unique in LOC)
                    If CurUtcFKey = PrvUtcFKey Then
                        If FstUtcLine < 0 Then
                            FstUtcLine = PrvLine
                            FstSked = PrvSked
                            FstUtcDate = PrvUtcDate
                            FstLocDate = PrvLocDate
                        End If
                        If PrvUtcDate = CurUtcDate Then
                            IsCheched = True
                            If ReorgIndex = False Then
                                CurTab.IndexDestroy "'K1'"
                                ReorgIndex = True
                            End If
                            If FstUtcLine = PrvLine Then
                                If PrvLocDate = CurLocDate Then
                                    'It is a double flight
                                    CurData = CurTab.GetColumnValue(PrvLine, Key1Col)
                                    If InStr(CurData, "*U*") > 0 Then
                                        CurData = CurUtcFKey
                                        CurData = Replace(CurData, "@*", "U*", 1, -1, vbBinaryCompare)
                                        CurTab.SetColumnValue CurLine, Key1Col, CurData
                                    End If
                                    CurTab.SetColumnValue CurLine, FdsL3Col, "D"
                                    CurTab.SetLineColor CurLine, MainDialog.CliFkeyCnt(5).ForeColor, MainDialog.CliFkeyCnt(5).BackColor
                                Else
                                    'It is an allowed UTC Flight Duplication
                                    CurData = CurTab.GetColumnValue(CurLine, Key1Col)
                                    CurData = Replace(CurData, "@*", "U*", 1, -1, vbBinaryCompare)
                                    CurTab.SetColumnValue CurLine, Key1Col, CurData
                                    CurData = CurTab.GetColumnValue(CurLine, FdsL2Col)
                                    If CurData = "U" Then
                                        CurTab.SetColumnValue CurLine, FdsL2Col, "Z"
                                    Else
                                        CurTab.SetColumnValue CurLine, FdsL2Col, "Y"
                                    End If
                                    CurTab.SetLineColor CurLine, MainDialog.CliFkeyCnt(1).ForeColor, MainDialog.CliFkeyCnt(1).BackColor
                                End If
                            Else
                                If FstLocDate = CurLocDate Then
                                    'It is a double flight
                                    CurData = CurTab.GetColumnValue(PrvLine, Key1Col)
                                    If InStr(CurData, "*U*") > 0 Then
                                        CurData = CurUtcFKey
                                        CurData = Replace(CurData, "@*", "U*", 1, -1, vbBinaryCompare)
                                        CurTab.SetColumnValue CurLine, Key1Col, CurData
                                    End If
                                    CurTab.SetColumnValue CurLine, FdsL3Col, "D"
                                    CurTab.SetLineColor CurLine, MainDialog.CliFkeyCnt(5).ForeColor, MainDialog.CliFkeyCnt(5).BackColor
                                Else
                                    'It is an double UTC Flight Duplication
                                    CurData = CurTab.GetColumnValue(CurLine, Key1Col)
                                    CurData = Replace(CurData, "@*", "U*", 1, -1, vbBinaryCompare)
                                    CurTab.SetColumnValue CurLine, Key1Col, CurData
                                    CurData = CurTab.GetColumnValue(CurLine, FdsL2Col)
                                    If CurData = "U" Then
                                        CurTab.SetColumnValue CurLine, FdsL2Col, "Z"
                                    Else
                                        CurTab.SetColumnValue CurLine, FdsL2Col, "Y"
                                    End If
                                    CurTab.SetColumnValue CurLine, FdsL3Col, "D"
                                    CurTab.SetLineColor CurLine, MainDialog.CliFkeyCnt(5).ForeColor, MainDialog.CliFkeyCnt(5).BackColor
                                End If
                            End If
                        End If
                    Else
                        FstUtcLine = -1
                    End If
                
                    'Check for LOC Flight Duplication
                    '(Those Unique in UTC)
                    CurLocFKey = Replace(CurUtcFKey, ("*" & CurUtcDate & "*"), ("*" & CurLocDate & "*"), 1, -1, vbBinaryCompare)
                    PrvLocFKey = Replace(PrvUtcFKey, ("*" & PrvUtcDate & "*"), ("*" & PrvLocDate & "*"), 1, -1, vbBinaryCompare)
                    If Not IsCheched Then
                        If CurLocFKey = PrvLocFKey Then
                            'It's a local time duplication
                            If FstLocLine < 0 Then
                                FstLocLine = PrvLine
                                'FstUtcDate = PrvUtcDate
                                'FstLocDate = PrvLocDate
                            End If
                            If CurLocFKey = PrvLocFKey Then
                            'If PrvUtcDate <> CurUtcDate Then
                                If ReorgIndex = False Then
                                    CurTab.IndexDestroy "'K1'"
                                    ReorgIndex = True
                                End If
                                CurTab.SetLineColor CurLine, MainDialog.CliFkeyCnt(2).ForeColor, MainDialog.CliFkeyCnt(2).BackColor
                                'If FstLocLine = PrvLine Then
                                '    If PrvLocDate = CurLocDate Then
                                '        'It is a double flight
                                '        CurData = CurTab.GetColumnValue(PrvLine, Key1Col)
                                '        If InStr(CurData, "*U*") > 0 Then
                                '            CurData = CurUtcFKey
                                '            CurData = Replace(CurData, "@*", "U*", 1, -1, vbBinaryCompare)
                                '            CurTab.SetColumnValue CurLine, Key1Col, CurData
                                '        End If
                                '        CurTab.SetColumnValue CurLine, FdsL3Col, "D"
                                '        CurTab.SetLineColor CurLine, MainDialog.CliFkeyCnt(5).ForeColor, MainDialog.CliFkeyCnt(5).BackColor
                                '    Else
                                '        'It is an allowed UTC Flight Duplication
                                '        CurData = CurTab.GetColumnValue(CurLine, Key1Col)
                                '        CurData = Replace(CurData, "@*", "U*", 1, -1, vbBinaryCompare)
                                '        CurTab.SetColumnValue CurLine, Key1Col, CurData
                                '        CurData = CurTab.GetColumnValue(CurLine, FdsL2Col)
                                '        If CurData = "U" Then
                                '            CurTab.SetColumnValue CurLine, FdsL2Col, "Z"
                                '        Else
                                '            CurTab.SetColumnValue CurLine, FdsL2Col, "Y"
                                '        End If
                                '        CurTab.SetLineColor CurLine, MainDialog.CliFkeyCnt(2).ForeColor, MainDialog.CliFkeyCnt(2).BackColor
                                '    End If
                                'Else
                                '    If FstLocDate = CurLocDate Then
                                '        'It is a double flight
                                '        CurData = CurTab.GetColumnValue(PrvLine, Key1Col)
                                '        If InStr(CurData, "*U*") > 0 Then
                                '            CurData = CurUtcFKey
                                '            CurData = Replace(CurData, "@*", "U*", 1, -1, vbBinaryCompare)
                                '            CurTab.SetColumnValue CurLine, Key1Col, CurData
                                '        End If
                                '        CurTab.SetColumnValue CurLine, FdsL3Col, "D"
                                '        CurTab.SetLineColor CurLine, MainDialog.CliFkeyCnt(5).ForeColor, MainDialog.CliFkeyCnt(5).BackColor
                                '    Else
                                '        'It is an double UTC Flight Duplication
                                '        CurData = CurTab.GetColumnValue(CurLine, Key1Col)
                                '        CurData = Replace(CurData, "@*", "U*", 1, -1, vbBinaryCompare)
                                '        CurTab.SetColumnValue CurLine, Key1Col, CurData
                                '        CurData = CurTab.GetColumnValue(CurLine, FdsL2Col)
                                '        If CurData = "U" Then
                                '            CurTab.SetColumnValue CurLine, FdsL2Col, "Z"
                                '        Else
                                '            CurTab.SetColumnValue CurLine, FdsL2Col, "Y"
                                '        End If
                                '        CurTab.SetColumnValue CurLine, FdsL3Col, "D"
                                '        CurTab.SetLineColor CurLine, MainDialog.CliFkeyCnt(5).ForeColor, MainDialog.CliFkeyCnt(5).BackColor
                                '    End If
                                'End If
                            End If
                        Else
                            FstLocLine = -1
                        End If
                    End If
                End If
                PrvLocFKey = CurLocFKey
                PrvUtcFKey = CurUtcFKey
                
                'Check remaining claimed Z flight
                CurL2Data = CurTab.GetColumnValue(CurLine, FdsL2Col)
                If CurL2Data = "U" Then
                    CurL3Data = CurTab.GetColumnValue(CurLine, FdsL3Col)
                    If CurLocDate <> CurUtcDate Then
                        'Valid Z flight with missing basic flight
                        CurData = CurTab.GetColumnValue(CurLine, Key1Col)
                        CurData = Replace(CurData, "@*", "U*", 1, -1, vbBinaryCompare)
                        CurTab.SetColumnValue CurLine, Key1Col, CurData
                        CurTab.SetColumnValue CurLine, FdsL2Col, "S"
                        If CurL3Data <> "D" Then
                            CurTab.SetLineColor CurLine, MainDialog.CliFkeyCnt(1).ForeColor, MainDialog.CliFkeyCnt(1).BackColor
                        End If
                    Else
                        'That's not a Z Flight!
                        CurTab.SetColumnValue CurLine, FdsL2Col, "N"
                        If CurL3Data <> "D" Then
                            CurTab.SetLineColor CurLine, MainDialog.CliFkeyCnt(0).ForeColor, MainDialog.CliFkeyCnt(0).BackColor
                        End If
                    End If
                End If
            End If
            'CurTab.SetColumnValue CurLine, FdsCbCol, "Y"
            DoEvents
        Next
        If ReorgIndex Then
            CurTab.OnVScrollTo 0
            CurTab.Sort CStr(Key2Col), True, True
            CurTab.IndexCreate "'K1'", Key1Col
        End If
        MainDialog.FdsCompProgress "SUB_VALUE", 100, 100
        CurTab.OnVScrollTo 0
        CurTab.Refresh
        'We need to check basic data on target server too!
        'If ForWhat = 0 Then
            MainDialog.lblCurPos(4).Caption = "BASIC DATA"
            CheckBasicData Index, CurTab, TblName
        'End If
        MainDialog.FdsCompProgress "SUB_VALUE", 100, 100
        CurTab.OnVScrollTo 0
        CurTab.Refresh
    End If
    InfoText = ""
    MainDialog.lblInfo(1).Caption = InfoText
    MainDialog.lblInfo(2).Caption = InfoText
    MainDialog.lblInfo(1).Refresh
    MainDialog.AdjustImpButtonPanel 4, True, CStr(Index) & ",B," & CurTabIndicator
End Sub
Private Sub CheckBasicData(Index As Integer, CurTab As TABLib.Tab, TblName As String)
    Dim StatList As String
    Dim ColIdx As String
    Dim ChkColList As String
    Dim ChkColIdx As String
    Dim BasicCheckRules As String
    Dim ChkTabInfo As String
    Dim ChkTabName As String
    Dim ChkTabFunc As String
    Dim ChkFldList As String
    Dim ChkFldName As String
    Dim ChkFldData As String
    Dim ChkTabData As String
    Dim ChkChkRule As String
    Dim BasTabInfo As String
    Dim BasTabName As String
    Dim BasFldList As String
    Dim BasFldName As String
    Dim BasTabData As String
    Dim BasChkRule As String
    Dim HitLines As String
    Dim LineNo As Long
    Dim NextRule As String
    Dim FldList As String
    Dim FldCols As String
    Dim SqlFld As String
    Dim SqlTab As String
    Dim SqlKey As String
    Dim SqlDat As String
    Dim InfoText As String
    Dim InfoList1 As String
    Dim InfoList2 As String
    Dim ColNo As Long
    Dim StatCol As Long
    Dim SveCedaOcx As Integer
    Dim rul As Integer
    Dim fld As Integer
    Dim dat As Integer
    MainDialog.lblInfo(1).Visible = True
    MainDialog.lblInfo(2).Visible = True
    
    BasicCheckRules = ""
    
    Select Case TblName
        Case "AFTTAB"
            BasicCheckRules = BasicCheckRules & "[AFTTAB.FIELD:ALC2,ALC3=ALTTAB:ALC2,ALC3]"
            BasicCheckRules = BasicCheckRules & "[AFTTAB.FIELD:ACT3,ACT5=ACTTAB:ACT3,ACT5]"
            BasicCheckRules = BasicCheckRules & "[AFTTAB.FIELD:ORG3,VIA3,DES3,ORG4,VIA4,DES4=APTTAB:APC3,APC3,APC3,APC4,APC4,APC4]"
            BasicCheckRules = BasicCheckRules & "[AFTTAB.FIELD:TTYP=NATTAB:TTYP]"
            BasicCheckRules = BasicCheckRules & "[AFTTAB.VALUE:FLNO,ALC2,ALC3,ACT3,ACT5,ORG3,ORG4,DES3,DES4,TTYP,ADID,FTYP,FLTI]"
        Case "CCATAB"
            BasicCheckRules = BasicCheckRules & "[CCATAB.FIELD:CKIC=CICTAB:CNAM]"
            BasicCheckRules = BasicCheckRules & "[CCATAB.VALUE:FLNO]"
        Case Else
    End Select
    
    StatCol = CLng(GetRealItemNo(CurTab.LogicalFieldList, "'ST'"))
    rul = 0
    NextRule = "START"
    While NextRule <> ""
        rul = rul + 1
        NextRule = GetItem(BasicCheckRules, rul, "][")
        If NextRule <> "" Then
            ChkChkRule = GetItem(NextRule, 1, "=")
            If Left(ChkChkRule, 1) = "[" Then ChkChkRule = Mid(ChkChkRule, 2)
            If Right(ChkChkRule, 1) = "]" Then ChkChkRule = Left(ChkChkRule, Len(ChkChkRule) - 1)
            BasChkRule = GetItem(NextRule, 2, "=")
            If Left(BasChkRule, 1) = "[" Then BasChkRule = Mid(BasChkRule, 2)
            If Right(BasChkRule, 1) = "]" Then BasChkRule = Left(BasChkRule, Len(BasChkRule) - 1)
            ChkTabInfo = GetItem(ChkChkRule, 1, ":")
            ChkFldList = GetItem(ChkChkRule, 2, ":")
            ChkTabName = GetItem(ChkTabInfo, 1, ".")
            ChkTabFunc = GetItem(ChkTabInfo, 2, ".")
            BasTabInfo = GetItem(BasChkRule, 1, ":")
            BasTabName = GetItem(BasTabInfo, 1, ".")
            BasFldList = GetItem(BasChkRule, 2, ":")
            'Now we can run through field lists and functions
            Select Case ChkTabFunc
                Case "FIELD"
                    fld = 0
                    ChkFldName = "START"
                    While ChkFldName <> ""
                        fld = fld + 1
                        ChkFldName = GetItem(ChkFldList, fld, ",")
                        BasFldName = GetItem(BasFldList, fld, ",")
                        If ChkFldName <> "" Then
                            ColNo = CLng(GetRealItemNo(CurTab.LogicalFieldList, ChkFldName))
                            If ColNo >= 0 Then
                                FldCols = CStr(ColNo)
                                ColIdx = CStr(ColNo)
                                ChkColIdx = "/" & ColIdx & "/"
                                ChkTabData = CurTab.SelectDistinct(FldCols, "'", "'", ",", True)
                                ChkTabData = Replace(ChkTabData, " ", "", 1, -1, vbBinaryCompare)
                                ChkTabData = Replace(ChkTabData, "''", "", 1, -1, vbBinaryCompare)
                                ChkTabData = Replace(ChkTabData, ",,", ",", 1, -1, vbBinaryCompare)
                                If Left(ChkTabData, 1) = "," Then ChkTabData = Mid(ChkTabData, 2)
                                If Right(ChkTabData, 1) = "," Then ChkTabData = Left(ChkTabData, Len(ChkTabData) - 1)
                                FldList = BasFldName
                                SqlFld = "DISTINCT(" & BasFldName & ")"
                                SqlTab = BasTabName
                                InfoList1 = ChkFldName
                                InfoList2 = BasFldName
                                InfoText = "Basic Data Check             [ " & ChkTabName & "." & InfoList1 & " ] = [ " & BasTabName & "." & InfoList2 & " ]"
                                MainDialog.lblInfo(1).Caption = InfoText
                                MainDialog.lblInfo(2).Caption = InfoText
                                MainDialog.lblInfo(1).Refresh
                                SqlKey = "WHERE " & BasFldName & " IN (" & ChkTabData & ")"
                                SqlDat = ""
                                If ChkTabData <> "" Then
                                    'Read from Test Server
                                    SveCedaOcx = CurCedaOcx
                                    CurCedaOcx = 2
                                    If UfisServer.chkCon(CurCedaOcx).Value = 0 Then
                                        If ServerIsAvailable Then
                                            UfisServer.ConnectToCeda CurCedaIdx
                                        End If
                                    End If
                                    If UfisServer.chkCon(CurCedaOcx).Value = 1 Then
                                        UfisServer.CallCeda BasTabData, "RTA", SqlTab, SqlFld, SqlDat, SqlKey, "", 0, True, False
                                    Else
                                        BasTabData = ChkTabData
                                    End If
                                    CurCedaOcx = SveCedaOcx
                                Else
                                    BasTabData = ChkTabData
                                End If
                                BasTabData = Replace(BasTabData, vbLf, ",", 1, -1, vbBinaryCompare)
                                BasTabData = Replace(BasTabData, vbCr, "", 1, -1, vbBinaryCompare)
                                BasTabData = Replace(BasTabData, " ", "", 1, -1, vbBinaryCompare)
                                ChkTabData = Replace(ChkTabData, "'", "", 1, -1)
                                BasTabData = Replace(BasTabData, "'", "", 1, -1)
                                'We now have two lists to be compared:
                                'ChkTabData = Distinct List of values fetched from production
                                'BasTabData = Distinct List of values requested from test server
                                'We must now find out which value did not come back (is missing)
                                'Prepare both lists
                                ChkTabData = "," & ChkTabData & ","
                                BasTabData = "," & BasTabData & ","
                                'Start with 2. Item (after leading comma)!
                                dat = 1
                                ChkFldData = "START"
                                While ChkFldData <> ""
                                    dat = dat + 1
                                    ChkFldData = GetItem(ChkTabData, dat, ",")
                                    If ChkFldData <> "" Then
                                        ChkFldData = "," & ChkFldData & ","
                                        If InStr(BasTabData, ChkFldData) = 0 Then
                                            ChkFldData = Replace(ChkFldData, ",", "", 1, -1, vbBinaryCompare)
                                            CurTab.SetInternalLineBuffer True
                                            HitLines = CurTab.GetLinesByColumnValue(ColNo, ChkFldData, 0)
                                            LineNo = Val(HitLines)
                                            While LineNo >= 0
                                                LineNo = CurTab.GetNextResultLine
                                                If LineNo >= 0 Then
                                                    StatList = CurTab.GetColumnValue(LineNo, StatCol)
                                                    ChkColList = "/" & StatList
                                                    If InStr(ChkColList, ChkColIdx) = 0 Then
                                                        StatList = StatList & ColIdx & "/"
                                                        CurTab.SetColumnValue LineNo, StatCol, StatList
                                                    End If
                                                    CurTab.SetDecorationObject LineNo, ColNo, "CellError"
                                                    CurTab.SetLineColor LineNo, MainDialog.CliFkeyCnt(4).ForeColor, MainDialog.CliFkeyCnt(4).BackColor
                                                    CurTab.SetColumnValue LineNo, 4, "E"
                                                End If
                                            Wend
                                            CurTab.SetInternalLineBuffer False
                                        End If
                                    End If
                                Wend
                            End If
                        End If
                    Wend
                Case "VALUE"
                    fld = 0
                    ChkFldName = "START"
                    While ChkFldName <> ""
                        fld = fld + 1
                        ChkFldName = GetItem(ChkFldList, fld, ",")
                        If ChkFldName <> "" Then
                            ColNo = CLng(GetRealItemNo(CurTab.LogicalFieldList, ChkFldName))
                            If ColNo >= 0 Then
                                ColIdx = CStr(ColNo)
                                ChkColIdx = "/" & ColIdx & "/"
                                ChkFldData = ""
                                CurTab.SetInternalLineBuffer True
                                HitLines = CurTab.GetLinesByColumnValue(ColNo, ChkFldData, 0)
                                LineNo = Val(HitLines)
                                While LineNo >= 0
                                    LineNo = CurTab.GetNextResultLine
                                    If LineNo >= 0 Then
                                        StatList = CurTab.GetColumnValue(LineNo, StatCol)
                                        ChkColList = "/" & StatList
                                        If InStr(ChkColList, ChkColIdx) = 0 Then
                                            StatList = StatList & ColIdx & "/"
                                            CurTab.SetColumnValue LineNo, StatCol, StatList
                                        End If
                                        CurTab.SetDecorationObject LineNo, ColNo, "CellError"
                                        CurTab.SetLineColor LineNo, MainDialog.CliFkeyCnt(4).ForeColor, MainDialog.CliFkeyCnt(4).BackColor
                                        CurTab.SetColumnValue LineNo, 4, "E"
                                    End If
                                Wend
                                CurTab.SetInternalLineBuffer False
                            End If
                        End If
                    Wend
                Case "COMBI"
                Case Else
            End Select
        End If
    Wend
End Sub

Public Sub FdsRecFilter(Index As Integer, CurTab As TABLib.Tab, ForWhat As Integer)
    Dim TabProp As String
    Dim TblName As String
    Dim TblMeth As String
    Dim GridKey As String
    Dim KeyList As String
    Dim KeyData As String
    Dim GetField As String
    Dim GetValue As String
    Dim SetFields As String
    Dim SetValues As String
    Dim CurTabIndicator As String
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim PrgStep As Long
    Dim KeyCol As Long
    Dim i As Integer
    MainDialog.lblInfo(1).Visible = True
    MainDialog.lblInfo(2).Visible = True
    'ForWhat 0=Production
    'ForWhat 1=Test Server
    Select Case ForWhat
        Case 0
            CurTabIndicator = "S"
        Case 1
            CurTabIndicator = "T"
        Case Else
            CurTabIndicator = "?"
    End Select
    TabProp = MainDialog.GetTabProperty(Index, "TABLE_INFO", 1, "")
    TblName = GetItem(TabProp, 1, ":")
    TblMeth = GetItem(TabProp, 2, ":")
    GridKey = Replace(TabProp, ",", ":", 1, -1, vbBinaryCompare)
    KeyList = MyConfig.GetFdsTableKeyFields(GridKey)
    If TblMeth <> "PLAIN" Then
        'Temporay Hotfix
        KeyList = ""
    End If
    If KeyList <> "" Then
        MainDialog.lblCurPos(4).Caption = "Data Filter"
        'CurTab.IndexDestroy "'K1'"
        MainDialog.FdsCompProgress "SUB_INIT", LightGreen, 0
        MaxLine = CurTab.GetLineCount - 1
        PrgStep = MaxLine / 20
        If PrgStep < 50 Then PrgStep = 50
        For CurLine = 0 To MaxLine
            If (CurLine Mod PrgStep) = 0 Then
                MainDialog.FdsCompProgress "SUB_VALUE", CurLine + 1, MaxLine + 1
                CurTab.OnVScrollTo CurLine
            End If
            'KeyData = CurTab.GetFieldValue(CurLine, KeyList)
            'SetFields = "'K1'"
            'SetValues = KeyList
            'CurTab.SetFieldValues CurLine, SetFields, SetValues
            DoEvents
        Next
        CurTab.AutoSizeColumns
        MainDialog.FdsCompProgress "SUB_VALUE", MaxLine + 1, MaxLine + 1
    End If
    MainDialog.AdjustImpButtonPanel 4, True, CStr(Index) & ",F," & CurTabIndicator
End Sub

Public Sub FdsRecCompare(Index As Integer, SrcTab As TABLib.Tab, TgtTab As TABLib.Tab, ImpTab As TABLib.Tab)
    Dim StatList As String
    Dim ChkColList As String
    Dim ChkColIdx As String
    Dim ColIdx As String
    Dim TabProp As String
    Dim TblName As String
    Dim TblMeth As String
    Dim KeyName As String
    Dim KeyField As String
    Dim KeyValue As String
    Dim GetFields As String
    Dim GetValues As String
    Dim RelFields As String
    Dim RelValues As String
    Dim RelName As String
    Dim RelData As String
    Dim HitLines As String
    Dim SetFields As String
    Dim SetValues As String
    Dim ImpField As String
    Dim SrcValue As String
    Dim TgtValue As String
    Dim ColCtlCmp As String
    Dim ColCtlDif As String
    Dim CmpCheck As String
    Dim IsChecked As String
    Dim IsErrFlag As String
    Dim IsIgnFlag As String
    Dim InfoText As String
    Dim S3SrcKey As String
    Dim S3TgtKey As String
    Dim tmpAdid As String
    Dim tmpSked As String
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim TgtLine As Long
    Dim chkLine As Long
    Dim ImpLine As Long
    Dim PrgStep As Long
    Dim KeyCol As Long
    Dim MaxCol As Long
    Dim CurCol As Long
    Dim CmpCol As Long
    Dim RelCol As Long
    Dim CmpCnt As Long
    Dim DifCnt As Long
    Dim StatCol As Long
    Dim S3Col As Long
    Dim K4Col As Long
    Dim K5Col As Long
    Dim CurSort As Long
    Dim RecIsUnique As Boolean
    Dim RecIsEqual As Boolean
    Dim AlreadyUsed As Boolean
    Dim RecDblCnt As Long
    Dim RecDblChk As Long
    Dim SetDel As Boolean
    Dim i As Integer
    MainDialog.lblInfo(1).Visible = True
    MainDialog.lblInfo(2).Visible = True
    StatCol = CLng(GetRealItemNo(SrcTab.LogicalFieldList, "'ST'"))
    TabProp = MainDialog.GetTabProperty(Index, "TABLE_INFO", 1, "")
    TblName = GetItem(TabProp, 1, ",")
    TblMeth = GetItem(TabProp, 2, ":")
    KeyName = MyConfig.GetFdsTableKeyFields(TblName)
    If TblMeth <> "PLAIN" Then
        'Temporay Hotfix
        KeyName = ""
    End If
    If KeyName <> "" Then
        MainDialog.lblCurPos(4).Caption = "Comparison"
        MainDialog.FdsCompProgress "SUB_INIT", LightGreen, 0
        ImpTab.ResetContent
        ImpTab.IndexDestroy "'K1'"
        ImpTab.ColSelectionRemoveAll
        ImpTab.ColumnWidthString = SrcTab.ColumnWidthString
        ImpTab.ShowHorzScroller False
        ImpTab.Refresh
        MainDialog.AdjustImpButtonPanel 4, True, CStr(Index) & ",C,I"
        
        InfoText = "Attaching Production Data Information"
        MainDialog.lblInfo(1).Caption = InfoText
        MainDialog.lblInfo(2).Caption = InfoText
        MainDialog.lblInfo(1).Refresh
        CurSort = SrcTab.CurrentSortColumn
        SetFdsKeyCols SrcTab
        KeyCol = FdsK2Col
        K4Col = FdsK4Col
        K5Col = FdsK5Col
        S3Col = CLng(GetRealItemNo(SrcTab.LogicalFieldList, "'S3'"))
        If (CurSort <> KeyCol) Or (SrcTab.SortOrderASC <> True) Then
            SrcTab.Sort CStr(KeyCol), True, True
        End If
        SrcTab.IndexDestroy "'S3'"
        SrcTab.IndexCreate "'S3'", S3Col
        SrcTab.SetInternalLineBuffer True
        MaxLine = Val(SrcTab.GetLinesByColumnValue(FdsCbCol, "Y", 0))
        SrcTab.SetInternalLineBuffer False
        If MaxLine > 0 Then
            MaxLine = SrcTab.GetLineCount - 1
            PrgStep = MaxLine / 20
            If PrgStep < 50 Then PrgStep = 50
            If PrgStep > 500 Then PrgStep = 500
            For CurLine = 0 To MaxLine
                If (CurLine Mod PrgStep) = 0 Then
                    MainDialog.FdsCompProgress "SUB_VALUE", CurLine + 1, MaxLine + 1
                    SrcTab.OnVScrollTo CurLine
                End If
                IsChecked = SrcTab.GetColumnValue(CurLine, FdsCbCol)
                IsErrFlag = SrcTab.GetColumnValue(CurLine, 4)
                'IsIgnFlag = SrcTab.GetColumnValue(CurLine, 3)
                SrcTab.SetColumnValue CurLine, FdsCbCol, "N"
                If IsErrFlag <> "E" Then
                    SrcTab.ResetLineDecorations CurLine
                    'SrcTab.SetColumnValue CurLine, 2, ""
                    'SrcTab.SetColumnValue CurLine, 3, ""
                    SrcTab.SetColumnValue CurLine, StatCol, ""
                    'SrcTab.SetLineColor CurLine, MainDialog.CliFkeyCnt(3).ForeColor, MainDialog.CliFkeyCnt(3).BackColor
                End If
                DoEvents
            Next
        End If
        MainDialog.FdsCompProgress "SUB_VALUE", 100, 100
        SrcTab.OnVScrollTo 0
        SrcTab.Refresh
        
        InfoText = "Attaching Test Server Data Information"
        MainDialog.lblInfo(1).Caption = InfoText
        MainDialog.lblInfo(2).Caption = InfoText
        MainDialog.lblInfo(1).Refresh
        CurSort = TgtTab.CurrentSortColumn
        KeyCol = FdsK2Col
        If (CurSort <> KeyCol) Or (TgtTab.SortOrderASC <> True) Then
            TgtTab.Sort CStr(KeyCol), True, True
        End If
        TgtTab.IndexDestroy "'S3'"
        TgtTab.IndexCreate "'S3'", S3Col
        TgtTab.SetInternalLineBuffer True
        MaxLine = Val(TgtTab.GetLinesByColumnValue(FdsCbCol, "Y", 0))
        TgtTab.SetInternalLineBuffer False
        If MaxLine > 0 Then
            MaxLine = TgtTab.GetLineCount - 1
            PrgStep = MaxLine / 20
            If PrgStep < 50 Then PrgStep = 50
            If PrgStep > 500 Then PrgStep = 500
            For CurLine = 0 To MaxLine
                If (CurLine Mod PrgStep) = 0 Then
                    MainDialog.FdsCompProgress "SUB_VALUE", CurLine + 1, MaxLine + 1
                    'MainDialog.lblCurPos(4).Caption = CStr(CurLine)
                    TgtTab.OnVScrollTo CurLine
                End If
                IsChecked = TgtTab.GetColumnValue(CurLine, FdsCbCol)
                IsErrFlag = TgtTab.GetColumnValue(CurLine, 4)
                'IsIgnFlag = TgtTab.GetColumnValue(CurLine, 3)
                TgtTab.SetColumnValue CurLine, FdsCbCol, "N"
                If IsErrFlag <> "E" Then
                    TgtTab.ResetLineDecorations CurLine
                    'TgtTab.SetColumnValue CurLine, 0, "N"
                    'TgtTab.SetColumnValue CurLine, 2, ""
                    'TgtTab.SetColumnValue CurLine, 3, ""
                    TgtTab.SetColumnValue CurLine, StatCol, ""
                    'TgtTab.SetLineColor CurLine, MainDialog.CliFkeyCnt(3).ForeColor, MainDialog.CliFkeyCnt(3).BackColor
                End If
                DoEvents
            Next
        End If
        MainDialog.FdsCompProgress "SUB_VALUE", 100, 100
        TgtTab.OnVScrollTo 0
        TgtTab.Refresh
        
        InfoText = "Comparing"
        MainDialog.lblInfo(1).Caption = InfoText
        MainDialog.lblInfo(2).Caption = InfoText
        MainDialog.lblInfo(1).Refresh
        
        ColCtlCmp = MyConfig.GetCompareCtrlString(ImpTab, 0)
        ColCtlDif = ColCtlCmp
        
        KeyField = "'K1'"
        MaxLine = SrcTab.GetLineCount - 1
        PrgStep = MaxLine / 20
        If PrgStep < 50 Then PrgStep = 50
        If PrgStep > 500 Then PrgStep = 500
        TgtTab.SetInternalLineBuffer True
        chkLine = 0
        ImpLine = 0
        For CurLine = 0 To MaxLine
            If (CurLine Mod PrgStep) = 0 Then
                MainDialog.FdsCompProgress "SUB_VALUE", CurLine + 1, MaxLine + 1
                SrcTab.OnVScrollTo CurLine
                TgtTab.OnVScrollTo chkLine
                ImpTab.OnVScrollTo ImpLine
                MainDialog.AdjustImpButtonPanel 4, True, CStr(Index) & ",C,I"
            End If
            IsChecked = SrcTab.GetColumnValue(CurLine, FdsCbCol)
            IsErrFlag = SrcTab.GetColumnValue(CurLine, 4)
            'IsIgnFlag = SrcTab.GetColumnValue(CurLine, 3)
            If IsErrFlag = "E" Then
                SrcTab.SetColumnValue CurLine, FdsCbCol, "N"
            End If
            S3SrcKey = SrcTab.GetColumnValue(CurLine, S3Col)
            If IsErrFlag <> "E" Then
                'SrcTab.ResetLineDecorations CurLine
                KeyValue = SrcTab.GetFieldValue(CurLine, KeyField)
                HitLines = TgtTab.GetLinesByIndexValue("'K1'", KeyValue, 0)
                TgtLine = Val(HitLines)
                If (HitLines <> "") And (TgtLine > 0) Then
                    'Existing Data to be updated
                    RecDblChk = 0
                    If TgtLine = 1 Then
                        RecIsUnique = True
                        RecDblCnt = 0
                    Else
                        RecIsUnique = False
                        RecDblCnt = TgtLine
                    End If
                    MaxCol = ImpTab.GetColumnCount
                    MaxCol = MaxCol - 1
                    While TgtLine >= 0
                        TgtLine = TgtTab.GetNextResultLine
                        If TgtLine >= 0 Then
                            S3TgtKey = TgtTab.GetColumnValue(TgtLine, S3Col)
                            IsChecked = TgtTab.GetColumnValue(TgtLine, FdsCbCol)
                            chkLine = TgtLine
                            AlreadyUsed = False
                            If IsChecked <> "N" Then AlreadyUsed = True
                            If Not AlreadyUsed Then
                                SrcTab.SetColumnValue CurLine, FdsCbCol, "Y"
                                TgtTab.SetColumnValue TgtLine, FdsCbCol, "Y"
                                SrcTab.SetColumnValue CurLine, K5Col, S3TgtKey
                                TgtTab.SetColumnValue TgtLine, K4Col, S3SrcKey
                                RecDblChk = RecDblChk + 1
                                'If RecDblChk = 1 Then
                                    'GetValues = SrcTab.GetLineValues(CurLine)
                                    GetValues = TgtTab.GetLineValues(TgtLine)
                                'Else
                                    'GetValues = TgtTab.GetLineValues(TgtLine)
                                'End If
                                'Mid(GetValues, 1) = "N"
                                ImpTab.InsertTextLine GetValues, False
                                ImpLine = ImpTab.GetLineCount - 1
                                ImpTab.SetColumnValue ImpLine, FdsCbCol, "N"
                                ImpTab.SetColumnValue ImpLine, K4Col, S3SrcKey
                                ImpTab.SetColumnValue ImpLine, K5Col, S3TgtKey
                                SetFields = ""
                                SetValues = ""
                                RecIsEqual = True
                                StatList = ImpTab.GetColumnValue(ImpLine, StatCol)
                                ChkColList = "/" & StatList
                                CmpCnt = 0
                                DifCnt = 0
                                CmpCol = 0
                                For CurCol = 0 To MaxCol
                                    CmpCol = CmpCol + 1
                                    CmpCheck = Mid(ColCtlCmp, CmpCol, 1)
                                    If CmpCheck <> "0" Then
                                        SrcValue = SrcTab.GetColumnValue(CurLine, CurCol)
                                        TgtValue = TgtTab.GetColumnValue(TgtLine, CurCol)
                                        If SrcValue <> TgtValue Then
                                            ImpField = GetRealItem(SrcTab.LogicalFieldList, CurCol, ",")
                                            SetFields = SetFields & ImpField & ","
                                            SetValues = SetValues & SrcValue & ","
                                            ColIdx = CStr(CurCol)
                                            ChkColIdx = "/" & CStr(CurCol) & "/"
                                            If CmpCheck = "1" Then DifCnt = DifCnt + 1
                                            If CmpCheck = "I" Then CmpCnt = CmpCnt + 1
                                            RecIsEqual = False
                                            If InStr(ChkColList, ChkColIdx) = 0 Then
                                                ChkColList = ChkColList & ColIdx & "/"
                                            End If
                                            SrcTab.SetDecorationObject CurLine, CurCol, "CellDiff"
                                            TgtTab.SetDecorationObject TgtLine, CurCol, "CellDiff"
                                            ImpTab.SetDecorationObject ImpLine, CurCol, "CellDiff"
                                            Mid(ColCtlDif, CmpCol) = "D"
                                            If CmpCheck = "R" Then
                                                'Different Rotation
                                                CmpCnt = CmpCnt + 1
                                            End If
                                        End If
                                    End If
                                Next
                                StatList = Mid(ChkColList, 2)
                                ImpTab.SetColumnValue ImpLine, StatCol, StatList
                                ImpTab.SetFieldValues ImpLine, SetFields, SetValues
                                If RecDblChk = 1 Then
                                    If RecIsEqual = True Then
                                        ImpTab.SetLineColor ImpLine, MainDialog.CliFkeyCnt(6).ForeColor, MainDialog.CliFkeyCnt(6).BackColor
                                        'SrcTab.SetLineColor CurLine, MainDialog.CliFkeyCnt(6).ForeColor, MainDialog.CliFkeyCnt(6).BackColor
                                        'TgtTab.SetLineColor TgtLine, MainDialog.CliFkeyCnt(6).ForeColor, MainDialog.CliFkeyCnt(6).BackColor
                                        ImpTab.SetLineStatusValue ImpLine, 0
                                    Else
                                        If DifCnt > 0 Then
                                            ImpTab.SetLineColor ImpLine, MainDialog.CliFkeyCnt(3).ForeColor, MainDialog.CliFkeyCnt(3).BackColor
                                            'SrcTab.SetLineColor CurLine, MainDialog.CliFkeyCnt(3).ForeColor, MainDialog.CliFkeyCnt(3).BackColor
                                            'TgtTab.SetLineColor TgtLine, MainDialog.CliFkeyCnt(3).ForeColor, MainDialog.CliFkeyCnt(3).BackColor
                                            ImpTab.SetLineStatusValue ImpLine, 1
                                        ElseIf CmpCnt > 0 Then
                                            ImpTab.SetLineColor ImpLine, MainDialog.CliFkeyCnt(2).ForeColor, MainDialog.CliFkeyCnt(2).BackColor
                                            'SrcTab.SetLineColor CurLine, MainDialog.CliFkeyCnt(2).ForeColor, MainDialog.CliFkeyCnt(2).BackColor
                                            'TgtTab.SetLineColor TgtLine, MainDialog.CliFkeyCnt(2).ForeColor, MainDialog.CliFkeyCnt(2).BackColor
                                            ImpTab.SetLineStatusValue ImpLine, 2
                                        End If
                                    End If
                                Else
                                    ImpTab.SetLineColor ImpLine, MainDialog.CliFkeyCnt(5).ForeColor, MainDialog.CliFkeyCnt(5).BackColor
                                    'SrcTab.SetLineColor CurLine, MainDialog.CliFkeyCnt(5).ForeColor, MainDialog.CliFkeyCnt(5).BackColor
                                    ImpTab.SetLineStatusValue ImpLine, 9
                                End If
                            Else
                                If RecIsUnique = False Then
                                    SrcTab.SetColumnValue CurLine, FdsCbCol, "Y"
                                    'MERGE RECORDS in RESULT VIEW
                                    'GetValues = TgtTab.GetLineValues(TgtLine)
                                    'Mid(GetValues, 1) = "N"
                                    'ImpTab.InsertTextLine GetValues, False
                                    'ImpLine = ImpTab.GetLineCount - 1
                                    'ImpTab.SetLineColor ImpLine, MainDialog.CliFkeyCnt(5).ForeColor, MainDialog.CliFkeyCnt(5).BackColor
                                    'ImpTab.SetLineStatusValue ImpLine, 98
                                    'TgtTab.SetLineColor TgtLine, MainDialog.CliFkeyCnt(5).ForeColor, MainDialog.CliFkeyCnt(5).BackColor
                                Else
                                End If
                            End If
                        End If
                    Wend
                Else 'New Data to be inserted (Not on Right Side)
                    SrcTab.SetColumnValue CurLine, FdsCbCol, "Y"
                    GetValues = SrcTab.GetLineValues(CurLine)
                    'Mid(GetValues, 1) = "N"
                    ImpTab.InsertTextLine GetValues, False
                    ImpLine = ImpTab.GetLineCount - 1
                    ImpTab.SetColumnValue ImpLine, FdsCbCol, "N"
                    ImpTab.SetLineColor ImpLine, MainDialog.CliFkeyCnt(1).ForeColor, MainDialog.CliFkeyCnt(1).BackColor
                    ImpTab.SetLineStatusValue ImpLine, 3
                    'SrcTab.SetLineColor CurLine, MainDialog.CliFkeyCnt(1).ForeColor, MainDialog.CliFkeyCnt(1).BackColor
                End If
            Else
                'An inherited "red line" will be moved to "ignore line"
                GetValues = SrcTab.GetLineValues(CurLine)
                'Mid(GetValues, 1) = "N"
                ImpTab.InsertTextLine GetValues, False
                ImpLine = ImpTab.GetLineCount - 1
                ImpTab.SetColumnValue ImpLine, FdsCbCol, "N"
                ImpTab.SetLineColor ImpLine, MainDialog.CliFkeyCnt(0).ForeColor, MainDialog.CliFkeyCnt(0).BackColor
                ImpTab.SetLineStatusValue ImpLine, 97
                'StatList = SrcTab.GetColumnValue(CurLine, StatCol)
                RestoreCellObjectInfo ImpTab, ImpLine, "", "CellError"
                KeyValue = SrcTab.GetFieldValue(CurLine, KeyField)
                HitLines = TgtTab.GetLinesByIndexValue("'K1'", KeyValue, 0)
                TgtLine = Val(HitLines)
                If (HitLines <> "") And (TgtLine > 0) Then
                    While TgtLine >= 0
                        TgtLine = TgtTab.GetNextResultLine
                        If TgtLine >= 0 Then
                            TgtTab.SetColumnValue TgtLine, FdsCbCol, "Y"
                            'Do it only once
                            TgtLine = -1
                        End If
                    Wend
                End If
            End If
            DoEvents
        Next
        TgtTab.SetInternalLineBuffer False
        MainDialog.SetTabProperty Index, "TABLE_DIFFS", ColCtlDif, ""
        'Finally we need to identify the unused records of test server
        'Records to be deleted
        TgtTab.SetInternalLineBuffer True
        HitLines = TgtTab.GetLinesByColumnValue(FdsCbCol, "N", 0)
        TgtLine = Val(HitLines)
        If (HitLines <> "") And (TgtLine > 0) Then
            While TgtLine >= 0
                TgtLine = TgtTab.GetNextResultLine
                If TgtLine >= 0 Then
                    SetDel = True
                    GetValues = TgtTab.GetLineValues(TgtLine)
                    'Mid(GetValues, 1) = "N"
                    ImpTab.InsertTextLine GetValues, False
                    ImpLine = ImpTab.GetLineCount - 1
                    If (Index = TabArrFlightsIdx) Or (Index = TabDepFlightsIdx) Then
                        tmpAdid = ImpTab.GetFieldValue(ImpLine, "ADID")
                        tmpSked = ""
                        Select Case tmpAdid
                            Case "A"
                                tmpSked = ImpTab.GetFieldValue(ImpLine, "STOA")
                            Case "D"
                                tmpSked = ImpTab.GetFieldValue(ImpLine, "STOD")
                            Case "B"
                                tmpSked = ImpTab.GetFieldValue(ImpLine, "STOD")
                            Case Else
                        End Select
                        If tmpSked <> "" Then
                            If (tmpSked < DataFilterVpfr) Or (tmpSked > DataFilterVpto) Then
                                SetDel = False
                            End If
                        End If
                    End If
                    ImpTab.SetColumnValue ImpLine, FdsCbCol, "N"
                    If SetDel Then
                        ImpTab.SetLineColor ImpLine, MainDialog.CliFkeyCnt(4).ForeColor, MainDialog.CliFkeyCnt(4).BackColor
                        ImpTab.SetLineStatusValue ImpLine, 4
                    Else
                        ImpTab.SetColumnValue ImpLine, FdsL5Col, "-"
                        ImpTab.SetLineColor ImpLine, MainDialog.CliFkeyCnt(0).ForeColor, MainDialog.CliFkeyCnt(0).BackColor
                        ImpTab.SetLineStatusValue ImpLine, 0
                    End If
                    TgtTab.SetColumnValue TgtLine, FdsCbCol, "Y"
                    'TgtTab.SetLineColor TgtLine, MainDialog.CliFkeyCnt(4).ForeColor, MainDialog.CliFkeyCnt(4).BackColor
                End If
            Wend
        End If
        TgtTab.SetInternalLineBuffer False
        MainDialog.FdsCompProgress "SUB_VALUE", 100, 100
        KeyCol = FdsK2Col
        ImpTab.Sort CStr(KeyCol), True, True
        ImpTab.AutoSizeColumns
        ImpTab.ShowHorzScroller True
        MainDialog.AdjustImpButtonPanel 4, True, CStr(Index) & ",C,I"
        MainDialog.AdjustImpButtonPanel 4, True, CStr(Index) & ",C,S"
        MainDialog.AdjustImpButtonPanel 4, True, CStr(Index) & ",C,T"
    End If
    SrcTab.OnVScrollTo 0
    TgtTab.OnVScrollTo 0
    ImpTab.OnVScrollTo 0
    SrcTab.Refresh
    TgtTab.Refresh
    ImpTab.Refresh
End Sub

Private Function GetKeyDataFormat(Index As Integer, CurTab As TABLib.Tab, LineNo As Long, KeyList As String, GetMainKey As Boolean) As String
    Dim KeyData As String
    Dim KeyName As String
    Dim KeyRule As String
    Dim ItmData As String
    Dim tmpData As String
    Dim LocData As String
    Dim UtcData As String
    Dim AnyTime 'As Variant
    Dim Itm As Integer
    KeyData = ""
    If Left(KeyList, 5) = "RULE:" Then
        KeyRule = GetItem(KeyList, 2, ":")
        Itm = 0
        KeyName = "START"
        While KeyName <> ""
            Itm = Itm + 1
            KeyName = GetItem(KeyRule, Itm, ",")
            If KeyName <> "" Then
                ItmData = ""
                Select Case KeyName
                    Case "FLNO"
                        ItmData = CurTab.GetFieldValues(LineNo, KeyName)
                        If ItmData = "" Then ItmData = CurTab.GetFieldValues(LineNo, "CSGN")
                        ItmData = Left(ItmData & "         ", 9)
                    Case "SKDT", "SKDD"
                        tmpData = CurTab.GetFieldValues(LineNo, "ADID")
                        If tmpData = "A" Then
                            ItmData = CurTab.GetFieldValues(LineNo, "STOA")
                        Else
                            ItmData = CurTab.GetFieldValues(LineNo, "STOD")
                        End If
                        If GetMainKey = True Then
                            If KeyName = "SKDT" Then ItmData = Left(ItmData, 12)
                            If KeyName = "SKDD" Then ItmData = Left(ItmData, 8)
                        Else
                            If KeyName = "SKDT" Then ItmData = Left(ItmData, 12)
                            If KeyName = "SKDD" Then ItmData = Left(ItmData, 12)
                        End If
                    Case "VFTP"
                        tmpData = Trim(CurTab.GetFieldValues(LineNo, "FTYP"))
                        If tmpData = "" Then tmpData = "F"
                        If InStr("TG", tmpData) > 0 Then
                            ItmData = "T"
                        Else
                            ItmData = "F"
                        End If
                    Case "VSFX"
                        'A "Suffix Z" will be used in the Key value in order
                        'to achieve uniqueness of ambiguous flight schedules.
                        '(See SSIM Appendix H: UTC Flight Number Duplication)
                        'Here we create the placeholder somewhere in the key.
                        ItmData = "@*?*"
                    Case Else
                        ItmData = CurTab.GetFieldValues(LineNo, KeyName)
                End Select
                If ItmData = "" Then ItmData = "-"
                KeyData = KeyData & ItmData & ","
            End If
        Wend
        If Right(KeyData, 1) = "," Then KeyData = Left(KeyData, Len(KeyData) - 1)
    Else
        KeyData = CurTab.GetFieldValues(LineNo, KeyList)
    End If
    KeyData = Replace(KeyData, ",", "*", 1, -1, vbBinaryCompare)
    KeyData = Replace(KeyData, " ", "#", 1, -1, vbBinaryCompare)
    'Append a placeholder key for handling of duplicated records
    KeyData = KeyData & "(00)*"
    GetKeyDataFormat = KeyData
End Function
Public Sub RestoreCellObjectInfo(CurTab As TABLib.Tab, LineNo As Long, ColList As String, DecoObj As String)
    Dim CurLine As Long
    Dim BgnLine As Long
    Dim MaxLine As Long
    Dim ColNo As Long
    Dim ItmNbr As Integer
    Dim StatCol As Long
    Dim CurList As String
    Dim ChkCol As String
    Dim DecoType As String
    StatCol = CLng(GetRealItemNo(CurTab.LogicalFieldList, "'ST'"))
    If StatCol >= 0 Then
        If ColList <> "" Then CurList = ColList
        If LineNo >= 0 Then
            BgnLine = LineNo
            MaxLine = LineNo
        Else
            BgnLine = 0
            MaxLine = CurTab.GetLineCount - 1
        End If
        For CurLine = BgnLine To MaxLine
            If ColList = "" Then CurList = CurTab.GetColumnValue(CurLine, StatCol)
            ChkCol = CurList
            ItmNbr = 0
            While ChkCol <> ""
                ItmNbr = ItmNbr + 1
                ChkCol = GetItem(CurList, ItmNbr, "/")
                If ChkCol <> "" Then
                    DecoType = Right(ChkCol, 1)
                    If InStr("MDE", DecoType) = 0 Then
                        ColNo = Val(ChkCol)
                        CurTab.SetDecorationObject CurLine, ColNo, DecoObj
                    Else
                        ChkCol = Left(ChkCol, Len(ChkCol) - 1)
                        ColNo = Val(ChkCol)
                        Select Case DecoType
                            'Case "M"
                            '    CurTab.SetDecorationObject CurLine, ColNo, "Marker"
                            Case "E"
                                CurTab.SetDecorationObject CurLine, ColNo, "CellError"
                            Case "D"
                                CurTab.SetDecorationObject CurLine, ColNo, "CellDiff"
                            Case Else
                                CurTab.SetDecorationObject CurLine, ColNo, DecoObj
                        End Select
                    End If
                End If
            Wend
        Next
    End If
End Sub

Public Sub HandleRelInsert(CurKeyIdx As Integer, ImpTabIdx As Integer, ImpTab As TABLib.Tab, UseStatus As Long, SrcTab As TABLib.Tab, TgtTab As TABLib.Tab)
    Dim TabProp As String
    Dim TblName As String
    Dim TblMeth As String
    Dim GridKey As String
    Dim KeyList As String
    Dim ColCtlLst As String
    Dim RelHdlCmd As String
    Dim InsSqlFld As String
    Dim InsSqlDat As String
    Dim tmpUrno As String
    Dim InsLine As Long
    Dim MaxCount As Long
    Dim CurCount As Long
    Dim OutRecCnt As Long
    Dim TabIdx As Integer
    Dim OcxIdx As Integer
    Screen.MousePointer = 11
    MainDialog.FdsCompProgress "MAIN_INIT", LightGreen, 0
    MainDialog.lblCurPos(4).Caption = "Inserting"
    OcxIdx = CurCedaOcx
    CurCedaOcx = 3
    TabIdx = ImpTabIdx
    
    TabProp = MainDialog.GetTabProperty(TabIdx, "TABLE_INFO", 1, "")
    TblName = GetItem(TabProp, 1, ":")
    TblMeth = GetItem(TabProp, 2, ":")
    GridKey = Replace(TabProp, ",", ":", 1, -1, vbBinaryCompare)
    KeyList = MyConfig.GetFdsTableKeyFields(GridKey)
    If TblMeth <> "PLAIN" Then
        'Temporay Hotfix
        KeyList = ""
    End If
    If KeyList <> "" Then
        ColCtlLst = MyConfig.GetCompareCtrlString(ImpTab, 1)
        InsSqlFld = GetFieldsFromDiffList(ImpTab.LogicalFieldList, ColCtlLst, 1)
        RelHdlCmd = ""
        ImpTab.SetInternalLineBuffer True
        
        MaxCount = Val(ImpTab.GetLinesByStatusValue(UseStatus, 0))
        If MaxCount > 0 Then
            MainDialog.CliFkeyCnt(CurKeyIdx).Caption = CStr(MaxCount)
            MainDialog.CliFkeyCnt(CurKeyIdx).Refresh
            CurCount = 0
            OutRecCnt = 0
        
            InsLine = MaxCount
            If FdsKeepAllLinks = False Then
                UfisServer.UrnoPoolInit 50
                UfisServer.UrnoPoolPrepare InsLine
            End If
            RelHdlCmd = "*CMD*," & TblName & ",IRT," & CStr(ItemCount(InsSqlFld, ",")) & "," & InsSqlFld & vbLf
            OutRecCnt = 0
            While InsLine >= 0
                InsLine = ImpTab.GetNextResultLine
                If InsLine >= 0 Then
                    OutRecCnt = OutRecCnt + 1
                    If FdsKeepAllLinks = False Then
                        tmpUrno = UfisServer.UrnoPoolGetNext
                        'Note: After replacing of URNO some cursor synchronizations will fail!
                        ImpTab.SetFieldValues InsLine, "URNO", tmpUrno
                    End If
                    InsSqlDat = ImpTab.GetFieldValues(InsLine, InsSqlFld)
                    InsSqlDat = CleanNullValues(InsSqlDat)
                    RelHdlCmd = RelHdlCmd & InsSqlDat & vbLf
                    ImpTab.SetColumnValue InsLine, 0, "Y"
                    'ImpTab.SetLineColor InsLine, vbWhitFe, vbBlack
                    ImpTab.SetLineColor InsLine, vbBlack, MainDialog.CliFkeyCnt(8).BackColor
                    ImpTab.SetLineStatusValue InsLine, 0
                    If OutRecCnt >= 500 Then
                        Screen.MousePointer = 11
                        ImpTab.OnVScrollTo InsLine
                        ImpTab.Refresh
                        CurCount = CurCount + OutRecCnt
                        MainDialog.FdsCompProgress "MAIN_VALUE", CurCount, MaxCount
                        MainDialog.chkLeftImp(CurKeyIdx).BackColor = LightYellow
                        MainDialog.chkLeftImp(CurKeyIdx).Refresh
                        UfisServer.CallCeda CedaDataAnswer, "REL", TblName, InsSqlFld, RelHdlCmd, "LATE,NOBC,NOACTION,NOLOG", "", 0, False, False
                        'Sleep 1
                        Screen.MousePointer = 11
                        MainDialog.chkLeftImp(CurKeyIdx).BackColor = LightGrey
                        MainDialog.chkLeftImp(CurKeyIdx).Refresh
                        RelHdlCmd = "*CMD*," & TblName & ",IRT," & CStr(ItemCount(InsSqlFld, ",")) & "," & InsSqlFld & vbLf
                        MainDialog.CliFkeyCnt(CurKeyIdx).Caption = CStr(MaxCount - CurCount)
                        MainDialog.CliFkeyCnt(CurKeyIdx).Refresh
                        OutRecCnt = 0
                    End If
                End If
                DoEvents
            Wend
        End If
    End If
    ImpTab.SetInternalLineBuffer False
    If OutRecCnt > 0 Then
        CurCedaOcx = 3
        CurCount = CurCount + OutRecCnt
        MainDialog.FdsCompProgress "MAIN_VALUE", CurCount, MaxCount
        MainDialog.chkLeftImp(CurKeyIdx).BackColor = LightYellow
        MainDialog.chkLeftImp(CurKeyIdx).Refresh
        UfisServer.CallCeda CedaDataAnswer, "REL", TblName, InsSqlFld, RelHdlCmd, "LATE,NOBC,NOACTION,NOLOG", "", 0, False, False
        'Sleep 2
        MainDialog.chkLeftImp(CurKeyIdx).BackColor = LightGrey
        MainDialog.chkLeftImp(CurKeyIdx).Refresh
    End If
    ImpTab.OnVScrollTo 0
    ImpTab.Refresh
    CurCedaOcx = OcxIdx
    MainDialog.AdjustImpButtonPanel 4, True, CStr(TabIdx) & ",C,I"
    MainDialog.AdjustImpButtonPanel 5, True, CStr(TabIdx) & ",C,I"
    MainDialog.FdsCompProgress "MAIN_RESET", 0, 0

    Screen.MousePointer = 0
End Sub

Public Sub HandleRelUpdate(CurKeyIdx As Integer, ImpTabIdx As Integer, ImpTab As TABLib.Tab, UseStatus As Long, SrcTab As TABLib.Tab, TgtTab As TABLib.Tab)
    Dim TabProp As String
    Dim TblName As String
    Dim TblMeth As String
    Dim GridKey As String
    Dim KeyList As String
    Dim LineKey As String
    Dim ColCtlDif As String
    Dim RelHdlCmd As String
    Dim UpdSqlFld As String
    Dim UpdSqlDat As String
    Dim HitLine As String
    Dim tmpUrno As String
    Dim MaxCount As Long
    Dim CurCount As Long
    Dim UpdLine As Long
    Dim SrcLine As Long
    Dim TgtLine As Long
    Dim K4Col As Long
    Dim K5Col As Long
    Dim OutRecCnt As Long
    Dim TabIdx As Integer
    Dim OcxIdx As Integer
    Screen.MousePointer = 11
    MainDialog.FdsCompProgress "MAIN_INIT", LightGreen, 0
    MainDialog.lblCurPos(4).Caption = "Updating"
    OcxIdx = CurCedaOcx
    CurCedaOcx = 3
    TabIdx = ImpTabIdx
    
    TabProp = MainDialog.GetTabProperty(TabIdx, "TABLE_INFO", 1, "")
    TblName = GetItem(TabProp, 1, ":")
    TblMeth = GetItem(TabProp, 2, ":")
    GridKey = Replace(TabProp, ",", ":", 1, -1, vbBinaryCompare)
    KeyList = MyConfig.GetFdsTableKeyFields(GridKey)
    If TblMeth <> "PLAIN" Then
        'Temporay Hotfix
        KeyList = ""
    End If
    If KeyList <> "" Then
        SetFdsKeyCols SrcTab
        K4Col = FdsK4Col
        K5Col = FdsK5Col
        'S3Col = CLng(GetRealItemNo(SrcTab.LogicalFieldList, "'S3'"))
        MainDialog.chkLeftImp(CurKeyIdx).BackColor = LightGrey
        MainDialog.chkLeftImp(CurKeyIdx).Refresh
        ColCtlDif = MainDialog.GetTabProperty(TabIdx, "TABLE_DIFFS", 1, "")
        UpdSqlFld = GetFieldsFromDiffList(ImpTab.LogicalFieldList, ColCtlDif, 0)
        RelHdlCmd = ""
        ImpTab.SetInternalLineBuffer True
        
        MaxCount = Val(ImpTab.GetLinesByStatusValue(UseStatus, 0))
        If MaxCount > 0 Then
            MainDialog.CliFkeyCnt(CurKeyIdx).Caption = CStr(MaxCount)
            MainDialog.CliFkeyCnt(CurKeyIdx).Refresh
            RelHdlCmd = RelHdlCmd & "*CMD*," & TblName & ",URT," & CStr(ItemCount(UpdSqlFld, ",")) & "," & UpdSqlFld & ",[URNO=:VURNO]" & vbLf
            CurCount = 0
            OutRecCnt = 0
            UpdLine = MaxCount
            While UpdLine >= 0
                UpdLine = ImpTab.GetNextResultLine
                If UpdLine >= 0 Then
                    OutRecCnt = OutRecCnt + 1
                    tmpUrno = ImpTab.GetFieldValue(UpdLine, "URNO")
                    UpdSqlDat = ImpTab.GetFieldValues(UpdLine, UpdSqlFld)
                    UpdSqlDat = CleanNullValues(UpdSqlDat)
                    RelHdlCmd = RelHdlCmd & UpdSqlDat & "," & tmpUrno & vbLf
                    ImpTab.SetColumnValue UpdLine, FdsCbCol, "Y"
                    ImpTab.SetColumnValue UpdLine, FdsL5Col, "D"
                    ImpTab.SetLineColor UpdLine, MainDialog.CliFkeyCnt(8).ForeColor, MainDialog.CliFkeyCnt(8).BackColor
                    ImpTab.SetLineStatusValue UpdLine, 0
                    LineKey = ImpTab.GetColumnValue(UpdLine, K4Col)
                    HitLine = SrcTab.GetLinesByIndexValue("'S3'", LineKey, 0)
                    If HitLine <> "" Then
                        SrcLine = Val(HitLine)
                        SrcTab.SetColumnValue SrcLine, FdsL5Col, "D"
                        SrcTab.SetLineColor SrcLine, MainDialog.CliFkeyCnt(8).ForeColor, MainDialog.CliFkeyCnt(8).BackColor
                    End If
                    LineKey = ImpTab.GetColumnValue(UpdLine, K5Col)
                    HitLine = TgtTab.GetLinesByIndexValue("'S3'", LineKey, 0)
                    If HitLine <> "" Then
                        TgtLine = Val(HitLine)
                        TgtTab.SetColumnValue TgtLine, FdsL5Col, "D"
                        TgtTab.SetLineColor TgtLine, MainDialog.CliFkeyCnt(8).ForeColor, MainDialog.CliFkeyCnt(8).BackColor
                    End If
                    If OutRecCnt >= 500 Then
                        Screen.MousePointer = 11
                        ImpTab.OnVScrollTo UpdLine
                        ImpTab.Refresh
                        CurCount = CurCount + OutRecCnt
                        MainDialog.FdsCompProgress "MAIN_VALUE", CurCount, MaxCount
                        MainDialog.chkLeftImp(CurKeyIdx).BackColor = LightYellow
                        MainDialog.chkLeftImp(CurKeyIdx).Refresh
                        'UfisServer.CallCeda CedaDataAnswer, "REL", TblName, UpdSqlFld, RelHdlCmd, "LATE,NOBC,NOACTION,NOLOG", "", 0, False, False
                        Screen.MousePointer = 11
                        MainDialog.chkLeftImp(CurKeyIdx).BackColor = LightGrey
                        MainDialog.chkLeftImp(CurKeyIdx).Refresh
                        RelHdlCmd = "*CMD*," & TblName & ",URT," & CStr(ItemCount(UpdSqlFld, ",")) & "," & UpdSqlFld & ",[URNO=:VURNO]" & vbLf
                        MainDialog.CliFkeyCnt(CurKeyIdx).Caption = CStr(MaxCount - CurCount)
                        MainDialog.CliFkeyCnt(CurKeyIdx).Refresh
                        OutRecCnt = 0
                    End If
                End If
                DoEvents
            Wend
        End If
    End If
    ImpTab.SetInternalLineBuffer False
    If OutRecCnt > 0 Then
        Screen.MousePointer = 11
        CurCedaOcx = 3
        CurCount = CurCount + OutRecCnt
        MainDialog.FdsCompProgress "MAIN_VALUE", CurCount, MaxCount
        MainDialog.chkLeftImp(CurKeyIdx).BackColor = LightYellow
        MainDialog.chkLeftImp(CurKeyIdx).Refresh
        'UfisServer.CallCeda CedaDataAnswer, "REL", TblName, UpdSqlFld, RelHdlCmd, "LATE,NOBC,NOACTION,NOLOG", "", 0, False, False
        MainDialog.chkLeftImp(CurKeyIdx).BackColor = LightGrey
        MainDialog.chkLeftImp(CurKeyIdx).Refresh
        Screen.MousePointer = 11
    End If
    ImpTab.OnVScrollTo 0
    ImpTab.Refresh
    CurCedaOcx = OcxIdx
    MainDialog.AdjustImpButtonPanel 4, True, CStr(TabIdx) & ",C,I"
    MainDialog.AdjustImpButtonPanel 5, True, CStr(TabIdx) & ",C,I"
    MainDialog.FdsCompProgress "MAIN_RESET", 0, 0

    Screen.MousePointer = 0
End Sub

Public Sub HandleRelDelete(CurKeyIdx As Integer, ImpTabIdx As Integer, ImpTab As TABLib.Tab, UseStatus As Long, SrcTab As TABLib.Tab, TgtTab As TABLib.Tab)
    Dim TabProp As String
    Dim TblName As String
    Dim TblMeth As String
    Dim GridKey As String
    Dim KeyList As String
    Dim ColCtlDif As String
    Dim RelHdlCmd As String
    Dim DelSqlFld As String
    Dim DelSqlDat As String
    Dim tmpUrno As String
    Dim DelLine As Long
    Dim MaxCount As Long
    Dim CurCount As Long
    Dim OutRecCnt As Long
    Dim TabIdx As Integer
    Dim OcxIdx As Integer
    
    Screen.MousePointer = 11
    MainDialog.FdsCompProgress "MAIN_INIT", LightGreen, 0
    MainDialog.lblCurPos(4).Caption = "Deleting"
    OcxIdx = CurCedaOcx
    CurCedaOcx = 3
    TabIdx = ImpTabIdx
    
    
    TabProp = MainDialog.GetTabProperty(TabIdx, "TABLE_INFO", 1, "")
    TblName = GetItem(TabProp, 1, ":")
    TblMeth = GetItem(TabProp, 2, ":")
    GridKey = Replace(TabProp, ",", ":", 1, -1, vbBinaryCompare)
    KeyList = MyConfig.GetFdsTableKeyFields(GridKey)
    If TblMeth <> "PLAIN" Then
        'Temporay Hotfix
        KeyList = ""
    End If
    If KeyList <> "" Then
        MainDialog.chkLeftImp(CurKeyIdx).BackColor = LightGrey
        MainDialog.chkLeftImp(CurKeyIdx).Refresh
        RelHdlCmd = ""
        ImpTab.SetInternalLineBuffer True
        MaxCount = Val(ImpTab.GetLinesByStatusValue(UseStatus, 0))
        If MaxCount > 0 Then
            MainDialog.CliFkeyCnt(CurKeyIdx).Caption = CStr(MaxCount)
            MainDialog.CliFkeyCnt(CurKeyIdx).Refresh
            RelHdlCmd = RelHdlCmd & "*CMD*," & TblName & ",DRT,-1,[URNO=:VURNO]" & vbLf
            CurCount = 0
            OutRecCnt = 0
            DelLine = MaxCount
            While DelLine >= 0
                DelLine = ImpTab.GetNextResultLine
                If DelLine >= 0 Then
                    OutRecCnt = OutRecCnt + 1
                    tmpUrno = ImpTab.GetFieldValue(DelLine, "URNO")
                    RelHdlCmd = RelHdlCmd & tmpUrno & vbLf
                    ImpTab.SetColumnValue DelLine, 0, "Y"
                    'ImpTab.SetLineColor DelLine, vbWhite, vbBlack
                    ImpTab.SetLineColor DelLine, vbBlack, MainDialog.CliFkeyCnt(8).BackColor
                    ImpTab.SetLineStatusValue DelLine, 0
                    If OutRecCnt >= 500 Then
                        Screen.MousePointer = 11
                        ImpTab.OnVScrollTo DelLine
                        ImpTab.Refresh
                        CurCount = CurCount + OutRecCnt
                        MainDialog.FdsCompProgress "MAIN_VALUE", CurCount, MaxCount
                        MainDialog.chkLeftImp(CurKeyIdx).BackColor = LightYellow
                        MainDialog.chkLeftImp(CurKeyIdx).Refresh
                        UfisServer.CallCeda CedaDataAnswer, "REL", TblName, DelSqlFld, RelHdlCmd, "LATE,NOBC,NOACTION,NOLOG", "", 0, False, False
                        'Sleep 1
                        Screen.MousePointer = 11
                        MainDialog.chkLeftImp(CurKeyIdx).BackColor = LightGrey
                        MainDialog.chkLeftImp(CurKeyIdx).Refresh
                        RelHdlCmd = "*CMD*," & TblName & ",DRT,-1,[URNO=:VURNO]" & vbLf
                        MainDialog.CliFkeyCnt(CurKeyIdx).Caption = CStr(MaxCount - CurCount)
                        MainDialog.CliFkeyCnt(CurKeyIdx).Refresh
                        OutRecCnt = 0
                    End If
                End If
                DoEvents
            Wend
        End If
    End If
    ImpTab.SetInternalLineBuffer False
    If OutRecCnt > 0 Then
        Screen.MousePointer = 11
        CurCount = CurCount + OutRecCnt
        MainDialog.FdsCompProgress "MAIN_VALUE", CurCount, MaxCount
        MainDialog.chkLeftImp(CurKeyIdx).BackColor = LightYellow
        MainDialog.chkLeftImp(CurKeyIdx).Refresh
        UfisServer.CallCeda CedaDataAnswer, "REL", TblName, DelSqlFld, RelHdlCmd, "LATE,NOBC,NOACTION,NOLOG", "", 0, False, False
        'Sleep 1
        Screen.MousePointer = 11
        MainDialog.chkLeftImp(CurKeyIdx).BackColor = LightGrey
        MainDialog.chkLeftImp(CurKeyIdx).Refresh
    End If
    ImpTab.OnVScrollTo 0
    ImpTab.Refresh
    CurCedaOcx = OcxIdx
    MainDialog.AdjustImpButtonPanel 4, True, CStr(TabIdx) & ",C,I"
    MainDialog.AdjustImpButtonPanel 5, True, CStr(TabIdx) & ",C,I"
    MainDialog.FdsCompProgress "MAIN_RESET", 0, 0

    Screen.MousePointer = 0
End Sub

Private Function GetFieldsFromDiffList(AllFldList As String, DiffCtrl As String, ForWhat As Integer) As String
    Dim FldList As String
    Dim tmpChar As String
    Dim tmpName As String
    Dim i As Integer
    Dim i1 As Integer
    Dim i2 As Integer
    FldList = ""
    i1 = 1
    i2 = Len(DiffCtrl)
    For i = 1 To i2
        tmpChar = Mid(DiffCtrl, i, 1)
        If (tmpChar = "D") Or ((ForWhat = 1) And (tmpChar <> "0")) Then
            tmpName = GetItem(AllFldList, i, ",")
            If Left(tmpName, 1) <> "'" Then
                FldList = FldList & tmpName & ","
            End If
        End If
    Next
    If FldList <> "" Then FldList = Left(FldList, Len(FldList) - 1)
    GetFieldsFromDiffList = FldList
End Function

Public Sub FdsCreateRotationInfo(ForWhat As Integer, IdxProp As String, ArrIdx As Integer, ArrTab As TABLib.Tab, DepIdx As Integer, DepTab As TABLib.Tab)
    Dim ArrRkey As String
    Dim ArrUrno As String
    Dim ArrKyK3 As String
    Dim DepRkey As String
    Dim DepUrno As String
    Dim DepAurn As String
    Dim DepKyK3 As String
    Dim HitLines As String
    Dim HitCount As Long
    Dim GetDepFldLst As String
    Dim GetDepFldDat As String
    Dim SetArrFldLst As String
    Dim SetArrFldDat As String
    Dim GetArrFldLst As String
    Dim GetArrFldDat As String
    Dim SetDepFldLst As String
    Dim SetDepFldDat As String
    Dim ArrLine As Long
    Dim DepLine As Long
    Dim MaxLine As Long
    Dim PrgStep As Long
    Dim ImpLblIdx As Integer
    
    Select Case ForWhat
        Case 1
            'Running Through ArrTab and Search Departures
            GetDepFldLst = "FLNO,STOD,URNO"
            SetArrFldLst = "'DF','DT','K3'"
            GetArrFldLst = "FLNO,STOA,URNO"
            SetDepFldLst = "'AF','AT','K3'"
            MainDialog.FdsCompProgress "SUB_INIT", LightGreen, 0
            MainDialog.lblCurPos(4).Caption = "Departures"
            CreateTabIndex DepIdx, DepTab, IdxProp, "AURN", True
            DepTab.SetInternalLineBuffer True
            MaxLine = ArrTab.GetLineCount - 1
            PrgStep = MaxLine / 20
            If PrgStep < 50 Then PrgStep = 50
            If PrgStep > 500 Then PrgStep = 500
            For ArrLine = 0 To MaxLine
                If (ArrLine Mod PrgStep) = 0 Then
                    MainDialog.FdsCompProgress "SUB_VALUE", ArrLine + 1, MaxLine + 1
                    ArrTab.OnVScrollTo ArrLine
                End If
                ArrUrno = ArrTab.GetFieldValue(ArrLine, "URNO")
                HitLines = DepTab.GetLinesByIndexValue("AURN", ArrUrno, 0)
                If HitLines <> "" Then
                    HitCount = Val(HitLines)
                    If HitCount = 1 Then
                        ArrRkey = ArrTab.GetFieldValue(ArrLine, "RKEY")
                        DepLine = DepTab.GetNextResultLine
                        While DepLine >= 0
                            ArrKyK3 = Trim(ArrTab.GetFieldValue(ArrLine, "'K3'"))
                            DepKyK3 = Trim(DepTab.GetFieldValue(DepLine, "'K3'"))
                            DepRkey = DepTab.GetFieldValue(DepLine, "RKEY")
                            If (ArrKyK3 = "") And (DepKyK3 = "") And (DepRkey = ArrRkey) Then
                                GetDepFldDat = DepTab.GetFieldValues(DepLine, GetDepFldLst)
                                SetArrFldDat = GetDepFldDat
                                ArrTab.SetFieldValues ArrLine, SetArrFldLst, SetArrFldDat
                                GetArrFldDat = ArrTab.GetFieldValues(ArrLine, GetArrFldLst)
                                SetDepFldDat = GetArrFldDat
                                DepTab.SetFieldValues DepLine, SetDepFldLst, SetDepFldDat
                            ElseIf ArrKyK3 <> "" Then
                                'Arrival already joined
                                ImpLblIdx = 7
                                DepTab.SetColumnValue DepLine, FdsL4Col, "1"
                                DepTab.SetLineColor DepLine, MainDialog.CliFkeyCnt(ImpLblIdx).ForeColor, MainDialog.CliFkeyCnt(ImpLblIdx).BackColor
                            ElseIf DepKyK3 <> "" Then
                                'Departure already joined
                                ImpLblIdx = 7
                                DepTab.SetColumnValue DepLine, FdsL4Col, "2"
                                DepTab.SetLineColor DepLine, MainDialog.CliFkeyCnt(ImpLblIdx).ForeColor, MainDialog.CliFkeyCnt(ImpLblIdx).BackColor
                            ElseIf DepRkey <> ArrRkey Then
                                'Not Matching Rkey
                                ImpLblIdx = 7
                                DepTab.SetColumnValue DepLine, FdsL4Col, "3"
                                DepTab.SetLineColor DepLine, MainDialog.CliFkeyCnt(ImpLblIdx).ForeColor, MainDialog.CliFkeyCnt(ImpLblIdx).BackColor
                            End If
                            DepLine = DepTab.GetNextResultLine
                        Wend
                    ElseIf HitCount > 1 Then
                        ArrRkey = ArrTab.GetFieldValue(ArrLine, "RKEY")
                        DepLine = DepTab.GetNextResultLine
                        While DepLine >= 0
                            'Too many Rotation Links
                            ImpLblIdx = 4
                            DepTab.SetColumnValue DepLine, FdsL4Col, "M"
                            DepTab.SetColumnValue DepLine, FdsL5Col, "E"
                            DepTab.SetLineColor DepLine, MainDialog.CliFkeyCnt(ImpLblIdx).ForeColor, MainDialog.CliFkeyCnt(ImpLblIdx).BackColor
                            DepLine = DepTab.GetNextResultLine
                        Wend
                    End If
                End If
            Next
            DepTab.SetInternalLineBuffer False
            MainDialog.FdsCompProgress "SUB_VALUE", MaxLine + 1, MaxLine + 1
            ArrTab.OnVScrollTo 0
        Case 2
            'Running Through DepTab and Search Arrivals
            GetDepFldLst = "FLNO,STOD,URNO"
            SetArrFldLst = "'DF','DT','K3'"
            GetArrFldLst = "FLNO,STOA,URNO"
            SetDepFldLst = "'AF','AT','K3'"
            MainDialog.FdsCompProgress "SUB_INIT", LightGreen, 0
            MainDialog.lblCurPos(4).Caption = "Departures"
            'CreateTabIndex ArrIdx, ArrTab, IdxProp, "URNO", True
            ArrTab.SetInternalLineBuffer True
            MaxLine = DepTab.GetLineCount - 1
            PrgStep = MaxLine / 20
            If PrgStep < 50 Then PrgStep = 50
            If PrgStep > 500 Then PrgStep = 500
            For DepLine = 0 To MaxLine
                If (DepLine Mod PrgStep) = 0 Then
                    MainDialog.FdsCompProgress "SUB_VALUE", DepLine + 1, MaxLine + 1
                    DepTab.OnVScrollTo DepLine
                End If
                DepRkey = DepTab.GetFieldValue(DepLine, "RKEY")
                DepAurn = DepTab.GetFieldValue(DepLine, "AURN")
                DepKyK3 = Trim(DepTab.GetFieldValue(DepLine, "'K3'"))
                If (DepAurn <> "") And (DepKyK3 = "") Then
                    HitLines = ArrTab.GetLinesByIndexValue("URNO", DepAurn, 0)
                    If HitLines <> "" Then
                        HitCount = Val(HitLines)
                        If HitCount > 0 Then
                            ArrLine = ArrTab.GetNextResultLine
                            While ArrLine >= 0
                                ArrKyK3 = Trim(ArrTab.GetFieldValue(ArrLine, "'K3'"))
                                DepKyK3 = Trim(DepTab.GetFieldValue(DepLine, "'K3'"))
                                ArrRkey = ArrTab.GetFieldValue(ArrLine, "RKEY")
                                If (ArrKyK3 = "") And (DepKyK3 = "") And (DepRkey = ArrRkey) Then
                                    GetDepFldDat = DepTab.GetFieldValues(DepLine, GetDepFldLst)
                                    SetArrFldDat = GetDepFldDat
                                    ArrTab.SetFieldValues ArrLine, SetArrFldLst, SetArrFldDat
                                    GetArrFldDat = ArrTab.GetFieldValues(ArrLine, GetArrFldLst)
                                    SetDepFldDat = GetArrFldDat
                                    DepTab.SetFieldValues DepLine, SetDepFldLst, SetDepFldDat
                                ElseIf ArrKyK3 <> "" Then
                                    'Arrival already joined
                                    ImpLblIdx = 7
                                    DepTab.SetColumnValue DepLine, FdsL4Col, "4"
                                    DepTab.SetLineColor DepLine, MainDialog.CliFkeyCnt(ImpLblIdx).ForeColor, MainDialog.CliFkeyCnt(ImpLblIdx).BackColor
                                ElseIf DepKyK3 <> "" Then
                                    'Departure already joined
                                    ImpLblIdx = 7
                                    DepTab.SetColumnValue DepLine, FdsL4Col, "5"
                                    DepTab.SetLineColor DepLine, MainDialog.CliFkeyCnt(ImpLblIdx).ForeColor, MainDialog.CliFkeyCnt(ImpLblIdx).BackColor
                                ElseIf DepRkey <> ArrRkey Then
                                    'Not Matching Rkey
                                    ImpLblIdx = 7
                                    DepTab.SetColumnValue DepLine, FdsL4Col, "6"
                                    DepTab.SetLineColor DepLine, MainDialog.CliFkeyCnt(ImpLblIdx).ForeColor, MainDialog.CliFkeyCnt(ImpLblIdx).BackColor
                                End If
                                ArrLine = ArrTab.GetNextResultLine
                            Wend
                        End If
                    End If
                End If
            Next
            ArrTab.SetInternalLineBuffer False
            MainDialog.FdsCompProgress "SUB_VALUE", MaxLine + 1, MaxLine + 1
            DepTab.OnVScrollTo 0
        Case 3
        Case Else
    End Select
End Sub


Public Sub CreateTabIndex(Index As Integer, CurTab As TABLib.Tab, PropName As String, FieldName As String, RecreateIndex As Boolean)
    Dim TabIndexList As String
    Dim FldColIdx As String
    Dim FldColNo As Long
    FldColNo = -1
    FldColIdx = TranslateFieldItems(FieldName, CurTab.LogicalFieldList)
    If FldColIdx <> "" Then
        TabIndexList = MainDialog.GetTabProperty(Index, PropName, 1, "")
        If InStr(TabIndexList, FieldName) = 0 Then
            CurTab.IndexDestroy FieldName
            FldColNo = Val(FldColIdx)
        Else
            If RecreateIndex Then
                CurTab.IndexDestroy FieldName
                FldColNo = Val(FldColIdx)
            End If
        End If
    End If
    If FldColNo >= 0 Then
        CurTab.IndexCreate FieldName, FldColNo
        TabIndexList = TabIndexList & FieldName & ":"
        MainDialog.SetTabProperty Index, PropName, TabIndexList, ""
    End If
End Sub
