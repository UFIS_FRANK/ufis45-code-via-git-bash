VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.ocx"
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "TAB.ocx"
Begin VB.Form ConnexChart 
   Caption         =   "UFIS Flight Connex Chart"
   ClientHeight    =   10875
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   15165
   Icon            =   "ConnexChart.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   10875
   ScaleWidth      =   15165
   Begin VB.PictureBox RightScCon1 
      BackColor       =   &H000040C0&
      Height          =   1245
      Index           =   0
      Left            =   5880
      ScaleHeight     =   1185
      ScaleWidth      =   5565
      TabIndex        =   168
      Top             =   9030
      Visible         =   0   'False
      Width           =   5625
      Begin VB.Frame Frame8 
         Caption         =   "A/D"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   525
         Left            =   1950
         TabIndex        =   219
         Top             =   690
         Visible         =   0   'False
         Width           =   660
         Begin VB.CheckBox chkCnxAdid 
            Caption         =   "DEP"
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   1
            Left            =   60
            Style           =   1  'Graphical
            TabIndex        =   220
            Top             =   210
            Width           =   510
         End
      End
      Begin TABLib.TAB TabDepInfo 
         Height          =   465
         Index           =   0
         Left            =   30
         TabIndex        =   194
         ToolTipText     =   "Connecting Departure Flights Outside The Visible Data Set"
         Top             =   780
         Visible         =   0   'False
         Width           =   5505
         _Version        =   65536
         _ExtentX        =   9710
         _ExtentY        =   820
         _StockProps     =   64
      End
      Begin VB.PictureBox SelDepPanel 
         AutoRedraw      =   -1  'True
         BackColor       =   &H00E0E0E0&
         Height          =   720
         Index           =   0
         Left            =   30
         ScaleHeight     =   660
         ScaleWidth      =   5445
         TabIndex        =   169
         Top             =   30
         Visible         =   0   'False
         Width           =   5505
         Begin VB.PictureBox SelDepFlight 
            Appearance      =   0  'Flat
            BackColor       =   &H00E0E0E0&
            ForeColor       =   &H80000008&
            Height          =   660
            Index           =   0
            Left            =   30
            ScaleHeight     =   630
            ScaleWidth      =   5370
            TabIndex        =   170
            Top             =   0
            Width           =   5400
            Begin VB.Label lblSelDepPstd 
               Alignment       =   2  'Center
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   161
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   270
               Index           =   0
               Left            =   4695
               TabIndex        =   188
               ToolTipText     =   "Bay"
               Top             =   30
               Width           =   645
            End
            Begin VB.Label lblSelDepOfbl 
               Alignment       =   2  'Center
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   161
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   270
               Index           =   0
               Left            =   4050
               TabIndex        =   187
               ToolTipText     =   "ATD (OFB)"
               Top             =   30
               Width           =   615
            End
            Begin VB.Label lblSelDepFlti 
               Alignment       =   2  'Center
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   161
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   270
               Index           =   0
               Left            =   1125
               TabIndex        =   186
               Top             =   30
               Width           =   300
            End
            Begin VB.Label lblSelDepVia3 
               Alignment       =   2  'Center
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   161
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   270
               Index           =   0
               Left            =   870
               TabIndex        =   185
               ToolTipText     =   "Next Station"
               Top             =   330
               Width           =   555
            End
            Begin VB.Label lblSelDepEtdi 
               Alignment       =   2  'Center
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   161
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   270
               Index           =   0
               Left            =   3450
               TabIndex        =   184
               ToolTipText     =   "ETD"
               Top             =   30
               Width           =   615
            End
            Begin VB.Label lblSelDepStod 
               Alignment       =   2  'Center
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   161
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   270
               Index           =   0
               Left            =   2850
               TabIndex        =   183
               ToolTipText     =   "STD"
               Top             =   30
               Width           =   615
            End
            Begin VB.Label lblSelDepDes3 
               Alignment       =   2  'Center
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   161
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   270
               Index           =   0
               Left            =   330
               TabIndex        =   182
               ToolTipText     =   "Destination"
               Top             =   330
               Width           =   555
            End
            Begin VB.Label lblSelDepFlno 
               Alignment       =   2  'Center
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   161
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   270
               Index           =   0
               Left            =   30
               TabIndex        =   181
               Top             =   30
               Width           =   1110
            End
            Begin VB.Label lblSelDepTot3 
               Alignment       =   2  'Center
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   161
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   270
               Index           =   0
               Left            =   3750
               TabIndex        =   180
               ToolTipText     =   "Economy"
               Top             =   330
               Width           =   465
            End
            Begin VB.Label lblSelDepTot2 
               Alignment       =   2  'Center
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   161
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   270
               Index           =   0
               Left            =   3300
               TabIndex        =   179
               ToolTipText     =   "Business"
               Top             =   330
               Width           =   465
            End
            Begin VB.Label lblSelDepTot1 
               Alignment       =   2  'Center
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   161
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   270
               Index           =   0
               Left            =   2850
               TabIndex        =   178
               ToolTipText     =   "First Class"
               Top             =   330
               Width           =   465
            End
            Begin VB.Label lblSelDepCsct 
               Alignment       =   2  'Center
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   161
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   270
               Index           =   0
               Left            =   4695
               TabIndex        =   177
               ToolTipText     =   "Shortest Connection Time (Minutes)"
               Top             =   330
               Width           =   645
            End
            Begin VB.Label lblSelDepRegn 
               Alignment       =   2  'Center
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   161
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   270
               Index           =   0
               Left            =   1455
               TabIndex        =   176
               ToolTipText     =   "Aircraft"
               Top             =   30
               Width           =   855
            End
            Begin VB.Label lblSelDepAct3 
               Alignment       =   2  'Center
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   161
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   270
               Index           =   0
               Left            =   2295
               TabIndex        =   175
               ToolTipText     =   "Type"
               Top             =   30
               Width           =   525
            End
            Begin VB.Label lbSellDepTotUld 
               Alignment       =   2  'Center
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   161
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   270
               Index           =   0
               Left            =   2355
               TabIndex        =   174
               ToolTipText     =   "Total Transfer ULD (Amount)"
               Top             =   330
               Width           =   465
            End
            Begin VB.Label lblSelDepTotBag 
               Alignment       =   2  'Center
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   161
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   270
               Index           =   0
               Left            =   1905
               TabIndex        =   173
               ToolTipText     =   "Total Transfer BAG (Pieces)"
               Top             =   330
               Width           =   465
            End
            Begin VB.Label lblSelDepTotPax 
               Alignment       =   2  'Center
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   161
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   270
               Index           =   0
               Left            =   1455
               TabIndex        =   172
               ToolTipText     =   "Total Transfer PAX"
               Top             =   330
               Width           =   465
            End
            Begin VB.Label lblSelDepTot4 
               Alignment       =   2  'Center
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   161
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   270
               Index           =   0
               Left            =   4200
               TabIndex        =   171
               ToolTipText     =   "Premium Economy"
               Top             =   330
               Width           =   465
            End
            Begin VB.Image picSelDepStatus 
               Appearance      =   0  'Flat
               BorderStyle     =   1  'Fixed Single
               Height          =   270
               Index           =   0
               Left            =   30
               Picture         =   "ConnexChart.frx":0442
               ToolTipText     =   "Flight Status"
               Top             =   330
               Width           =   270
            End
         End
      End
      Begin VB.Frame fraTabCaption 
         BackColor       =   &H00FF0000&
         BorderStyle     =   0  'None
         Caption         =   "Frame9"
         Height          =   300
         Index           =   2
         Left            =   0
         TabIndex        =   193
         Top             =   0
         Width           =   9165
      End
   End
   Begin VB.PictureBox LeftScCon1 
      BackColor       =   &H000040C0&
      Height          =   1245
      Index           =   0
      Left            =   150
      ScaleHeight     =   1185
      ScaleWidth      =   5565
      TabIndex        =   147
      Top             =   9030
      Visible         =   0   'False
      Width           =   5625
      Begin VB.Frame Frame6 
         Caption         =   "A/D"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   510
         Left            =   3540
         TabIndex        =   221
         Top             =   540
         Visible         =   0   'False
         Width           =   660
         Begin VB.CheckBox chkCnxAdid 
            Caption         =   "ARR"
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   0
            Left            =   60
            Style           =   1  'Graphical
            TabIndex        =   222
            Top             =   210
            Width           =   510
         End
      End
      Begin VB.PictureBox SelArrPanel 
         AutoRedraw      =   -1  'True
         BackColor       =   &H00E0E0E0&
         Height          =   720
         Index           =   0
         Left            =   30
         ScaleHeight     =   660
         ScaleWidth      =   5445
         TabIndex        =   148
         Top             =   30
         Visible         =   0   'False
         Width           =   5505
         Begin VB.PictureBox SelArrFlight 
            Appearance      =   0  'Flat
            BackColor       =   &H80000002&
            ForeColor       =   &H80000008&
            Height          =   660
            Index           =   0
            Left            =   30
            ScaleHeight     =   630
            ScaleWidth      =   5370
            TabIndex        =   149
            Top             =   0
            Width           =   5400
            Begin VB.Image picSelArrCursor 
               Appearance      =   0  'Flat
               BorderStyle     =   1  'Fixed Single
               Height          =   270
               Index           =   0
               Left            =   30
               Picture         =   "ConnexChart.frx":058C
               ToolTipText     =   "Flight Status"
               Top             =   330
               Width           =   270
            End
            Begin VB.Label lblSelArrAct3 
               Alignment       =   2  'Center
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   161
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   270
               Index           =   0
               Left            =   2295
               TabIndex        =   167
               ToolTipText     =   "Aircraft Type"
               Top             =   30
               Width           =   525
            End
            Begin VB.Label lblSelArrRegn 
               Alignment       =   2  'Center
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   161
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   270
               Index           =   0
               Left            =   1455
               TabIndex        =   166
               ToolTipText     =   "Aircraft Registration"
               Top             =   30
               Width           =   855
            End
            Begin VB.Label lblSelArrCsct 
               Alignment       =   2  'Center
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   161
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   270
               Index           =   0
               Left            =   4695
               TabIndex        =   165
               ToolTipText     =   "Shortest Connection Time (Minutes)"
               Top             =   330
               Width           =   645
            End
            Begin VB.Label lblSelArrTot1 
               Alignment       =   2  'Center
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   161
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   270
               Index           =   0
               Left            =   2850
               TabIndex        =   164
               ToolTipText     =   "First Class"
               Top             =   330
               Width           =   465
            End
            Begin VB.Label lblSelArrTot2 
               Alignment       =   2  'Center
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   161
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   270
               Index           =   0
               Left            =   3300
               TabIndex        =   163
               ToolTipText     =   "Business"
               Top             =   330
               Width           =   465
            End
            Begin VB.Label lblSelArrTot3 
               Alignment       =   2  'Center
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   161
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   270
               Index           =   0
               Left            =   3750
               MouseIcon       =   "ConnexChart.frx":06D6
               MousePointer    =   99  'Custom
               TabIndex        =   162
               ToolTipText     =   "Economy"
               Top             =   330
               Width           =   465
            End
            Begin VB.Label lblSelArrFlno 
               Alignment       =   2  'Center
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   161
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   270
               Index           =   0
               Left            =   30
               TabIndex        =   161
               Top             =   30
               Width           =   1110
            End
            Begin VB.Label lblSelArrOrg3 
               Alignment       =   2  'Center
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   161
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   270
               Index           =   0
               Left            =   330
               TabIndex        =   160
               ToolTipText     =   "Origin"
               Top             =   330
               Width           =   555
            End
            Begin VB.Label lblSelArrStoa 
               Alignment       =   2  'Center
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   161
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   270
               Index           =   0
               Left            =   2850
               TabIndex        =   159
               ToolTipText     =   "STA"
               Top             =   30
               Width           =   615
            End
            Begin VB.Label lblSelArrEtai 
               Alignment       =   2  'Center
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   161
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   270
               Index           =   0
               Left            =   3450
               TabIndex        =   158
               ToolTipText     =   "ETA"
               Top             =   30
               Width           =   615
            End
            Begin VB.Label lblSelArrVia3 
               Alignment       =   2  'Center
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   161
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   270
               Index           =   0
               Left            =   870
               TabIndex        =   157
               ToolTipText     =   "Previous Station"
               Top             =   330
               Width           =   555
            End
            Begin VB.Label lblSelArrFlti 
               Alignment       =   2  'Center
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   161
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   270
               Index           =   0
               Left            =   1125
               TabIndex        =   156
               ToolTipText     =   "Route Identifier"
               Top             =   30
               Width           =   300
            End
            Begin VB.Label lblSelArrOnbl 
               Alignment       =   2  'Center
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   161
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   270
               Index           =   0
               Left            =   4050
               TabIndex        =   155
               ToolTipText     =   "ATA (ONB)"
               Top             =   30
               Width           =   615
            End
            Begin VB.Label lblSelArrPsta 
               Alignment       =   2  'Center
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   161
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   270
               Index           =   0
               Left            =   4695
               TabIndex        =   154
               ToolTipText     =   "Bay"
               Top             =   30
               Width           =   645
            End
            Begin VB.Image picSelArrStatus 
               Appearance      =   0  'Flat
               BorderStyle     =   1  'Fixed Single
               Height          =   270
               Index           =   0
               Left            =   30
               Picture         =   "ConnexChart.frx":09E0
               ToolTipText     =   "Flight Status"
               Top             =   330
               Width           =   270
            End
            Begin VB.Label lblSelArrTot4 
               Alignment       =   2  'Center
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   161
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   270
               Index           =   0
               Left            =   4200
               TabIndex        =   153
               ToolTipText     =   "Premium Economy"
               Top             =   330
               Width           =   465
            End
            Begin VB.Label lblSelArrTotPax 
               Alignment       =   2  'Center
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   161
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   270
               Index           =   0
               Left            =   1455
               MouseIcon       =   "ConnexChart.frx":0B2A
               MousePointer    =   99  'Custom
               TabIndex        =   152
               ToolTipText     =   "Total Transfer PAX"
               Top             =   330
               Width           =   465
            End
            Begin VB.Label lblSelArrTotBag 
               Alignment       =   2  'Center
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   161
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   270
               Index           =   0
               Left            =   1905
               MouseIcon       =   "ConnexChart.frx":13F4
               MousePointer    =   99  'Custom
               TabIndex        =   151
               ToolTipText     =   "Total Transfer BAG (Pieces)"
               Top             =   330
               Width           =   465
            End
            Begin VB.Label lblSelArrTotUld 
               Alignment       =   2  'Center
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   161
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   270
               Index           =   0
               Left            =   2355
               TabIndex        =   150
               ToolTipText     =   "Total Transfer ULD (Amount)"
               Top             =   330
               Width           =   465
            End
         End
      End
      Begin VB.Frame fraTabCaption 
         BackColor       =   &H00FF0000&
         BorderStyle     =   0  'None
         Caption         =   "Frame9"
         Height          =   300
         Index           =   1
         Left            =   0
         TabIndex        =   192
         Top             =   0
         Width           =   9165
      End
   End
   Begin VB.PictureBox MidScCon1 
      Height          =   825
      Index           =   0
      Left            =   120
      ScaleHeight     =   765
      ScaleWidth      =   10125
      TabIndex        =   143
      Top             =   8070
      Visible         =   0   'False
      Width           =   10185
      Begin VB.Timer CnxRefresh 
         Enabled         =   0   'False
         Interval        =   1000
         Left            =   8280
         Top             =   330
      End
      Begin VB.PictureBox LeftTime 
         Appearance      =   0  'Flat
         BackColor       =   &H00000000&
         ForeColor       =   &H80000008&
         Height          =   330
         Index           =   1
         Left            =   1110
         ScaleHeight     =   300
         ScaleWidth      =   0
         TabIndex        =   146
         Top             =   0
         Visible         =   0   'False
         Width           =   30
      End
      Begin VB.PictureBox RightTime 
         Appearance      =   0  'Flat
         BackColor       =   &H00000000&
         ForeColor       =   &H80000008&
         Height          =   330
         Index           =   1
         Left            =   3450
         ScaleHeight     =   300
         ScaleWidth      =   0
         TabIndex        =   145
         Top             =   0
         Visible         =   0   'False
         Width           =   30
      End
      Begin VB.PictureBox MidTime 
         Appearance      =   0  'Flat
         BackColor       =   &H000000FF&
         ForeColor       =   &H80000008&
         Height          =   330
         Index           =   1
         Left            =   2160
         ScaleHeight     =   300
         ScaleWidth      =   15
         TabIndex        =   144
         Top             =   0
         Visible         =   0   'False
         Width           =   45
      End
      Begin VB.Frame fraTabCaption 
         BackColor       =   &H00FF0000&
         BorderStyle     =   0  'None
         Caption         =   "Frame9"
         Height          =   300
         Index           =   0
         Left            =   0
         TabIndex        =   189
         Top             =   0
         Width           =   9165
         Begin VB.Label lblTabCaptionShadow 
            Alignment       =   2  'Center
            BackStyle       =   0  'Transparent
            Caption         =   "Transfer PAX (Arrival)"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9.75
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FFFFFF&
            Height          =   240
            Index           =   0
            Left            =   30
            TabIndex        =   190
            Top             =   0
            Width           =   3345
         End
         Begin VB.Label lblTabCaption 
            Alignment       =   2  'Center
            BackStyle       =   0  'Transparent
            Caption         =   "Transfer PAX (Arrival)"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9.75
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   0
            Left            =   60
            TabIndex        =   191
            Top             =   15
            Width           =   7575
         End
      End
      Begin VB.Frame CnxFilterPanel 
         Height          =   510
         Left            =   15
         TabIndex        =   213
         Top             =   240
         Width           =   7755
         Begin VB.Frame CnxMidFilterPanel 
            Height          =   510
            Left            =   1845
            TabIndex        =   223
            Top             =   0
            Width           =   4770
            Begin VB.Frame fraCnxTypeFilter 
               Height          =   510
               Left            =   405
               TabIndex        =   233
               Top             =   0
               Width           =   1650
               Begin VB.CheckBox chkCnxType 
                  Caption         =   "PAX"
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   161
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   300
                  Index           =   0
                  Left            =   60
                  Style           =   1  'Graphical
                  TabIndex        =   236
                  ToolTipText     =   "Main Filter PAX Connections"
                  Top             =   150
                  Width           =   510
               End
               Begin VB.CheckBox chkCnxType 
                  Caption         =   "BAG"
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   161
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   300
                  Index           =   1
                  Left            =   570
                  Style           =   1  'Graphical
                  TabIndex        =   235
                  ToolTipText     =   "Main Filter BAG Connections"
                  Top             =   150
                  Width           =   510
               End
               Begin VB.CheckBox chkCnxType 
                  Caption         =   "ULD"
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   161
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   300
                  Index           =   2
                  Left            =   1080
                  Style           =   1  'Graphical
                  TabIndex        =   234
                  ToolTipText     =   "Main Filter ULD Connections"
                  Top             =   150
                  Width           =   510
               End
            End
            Begin VB.Frame fraCnxClassFilter 
               Height          =   510
               Left            =   2040
               TabIndex        =   228
               Top             =   0
               Width           =   1320
               Begin VB.CheckBox chkCnxClass 
                  Caption         =   "F"
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   161
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   300
                  Index           =   0
                  Left            =   60
                  Style           =   1  'Graphical
                  TabIndex        =   232
                  ToolTipText     =   "First Class"
                  Top             =   150
                  Width           =   300
               End
               Begin VB.CheckBox chkCnxClass 
                  Caption         =   "C"
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   161
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   300
                  Index           =   1
                  Left            =   360
                  Style           =   1  'Graphical
                  TabIndex        =   231
                  ToolTipText     =   "Business"
                  Top             =   150
                  Width           =   300
               End
               Begin VB.CheckBox chkCnxClass 
                  Caption         =   "Y"
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   161
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   300
                  Index           =   2
                  Left            =   660
                  Style           =   1  'Graphical
                  TabIndex        =   230
                  ToolTipText     =   "Economy"
                  Top             =   150
                  Width           =   300
               End
               Begin VB.CheckBox chkCnxClass 
                  Caption         =   "U"
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   161
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   300
                  Index           =   3
                  Left            =   960
                  Style           =   1  'Graphical
                  TabIndex        =   229
                  ToolTipText     =   "Premium Economy"
                  Top             =   150
                  Width           =   300
               End
            End
            Begin VB.Frame fraCnxConnexFilter 
               Height          =   510
               Left            =   3345
               TabIndex        =   224
               Top             =   0
               Width           =   1020
               Begin VB.CheckBox chkCnxShort 
                  Caption         =   "S"
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   161
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   300
                  Index           =   0
                  Left            =   60
                  Style           =   1  'Graphical
                  TabIndex        =   227
                  ToolTipText     =   "Short Connections"
                  Top             =   150
                  Width           =   300
               End
               Begin VB.CheckBox chkCnxCrit 
                  Caption         =   "C"
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   161
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   300
                  Index           =   0
                  Left            =   360
                  Style           =   1  'Graphical
                  TabIndex        =   226
                  ToolTipText     =   "Critical Connections"
                  Top             =   150
                  Width           =   300
               End
               Begin VB.CheckBox chkCnxOr 
                  Caption         =   "?"
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   161
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   300
                  Index           =   0
                  Left            =   660
                  Style           =   1  'Graphical
                  TabIndex        =   225
                  ToolTipText     =   "(Future Use)"
                  Top             =   150
                  Width           =   300
               End
            End
            Begin VB.Image picDepCnxCnt 
               Appearance      =   0  'Flat
               Height          =   240
               Index           =   0
               Left            =   4425
               Picture         =   "ConnexChart.frx":1CBE
               ToolTipText     =   "Flight Status"
               Top             =   180
               Width           =   240
            End
            Begin VB.Image picArrCnxCnt 
               Appearance      =   0  'Flat
               Height          =   240
               Index           =   0
               Left            =   90
               Picture         =   "ConnexChart.frx":1E08
               ToolTipText     =   "Flight Status"
               Top             =   180
               Width           =   240
            End
         End
      End
   End
   Begin VB.PictureBox DepFlight 
      Appearance      =   0  'Flat
      BackColor       =   &H00E0E0E0&
      ForeColor       =   &H80000008&
      Height          =   660
      Index           =   0
      Left            =   7110
      ScaleHeight     =   630
      ScaleWidth      =   5370
      TabIndex        =   91
      Top             =   7260
      Width           =   5400
      Begin VB.Image picDepStatus 
         Appearance      =   0  'Flat
         BorderStyle     =   1  'Fixed Single
         Height          =   270
         Index           =   0
         Left            =   30
         Picture         =   "ConnexChart.frx":1F52
         ToolTipText     =   "Flight Status"
         Top             =   330
         Width           =   270
      End
      Begin VB.Label lblDepTot4 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   270
         Index           =   0
         Left            =   4200
         TabIndex        =   123
         ToolTipText     =   "Premium Economy"
         Top             =   330
         Width           =   465
      End
      Begin VB.Label lblDepTotPax 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   270
         Index           =   0
         Left            =   1455
         TabIndex        =   122
         ToolTipText     =   "Total Transfer PAX"
         Top             =   330
         Width           =   465
      End
      Begin VB.Label lblDepTotBag 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   270
         Index           =   0
         Left            =   1905
         TabIndex        =   121
         ToolTipText     =   "Total Transfer BAG (Pieces)"
         Top             =   330
         Width           =   465
      End
      Begin VB.Label lblDepTotUld 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   270
         Index           =   0
         Left            =   2355
         TabIndex        =   120
         ToolTipText     =   "Total Transfer ULD (Amount)"
         Top             =   330
         Width           =   465
      End
      Begin VB.Label lblDepAct3 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   270
         Index           =   0
         Left            =   2295
         TabIndex        =   105
         ToolTipText     =   "Type"
         Top             =   30
         Width           =   525
      End
      Begin VB.Label lblDepRegn 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   270
         Index           =   0
         Left            =   1455
         TabIndex        =   104
         ToolTipText     =   "Aircraft"
         Top             =   30
         Width           =   855
      End
      Begin VB.Label lblDepCsct 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   270
         Index           =   0
         Left            =   4695
         TabIndex        =   103
         ToolTipText     =   "Shortest Connection Time (Minutes)"
         Top             =   330
         Width           =   645
      End
      Begin VB.Label lblDepTot1 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   270
         Index           =   0
         Left            =   2850
         TabIndex        =   102
         ToolTipText     =   "First Class"
         Top             =   330
         Width           =   465
      End
      Begin VB.Label lblDepTot2 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   270
         Index           =   0
         Left            =   3300
         TabIndex        =   101
         ToolTipText     =   "Business"
         Top             =   330
         Width           =   465
      End
      Begin VB.Label lblDepTot3 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   270
         Index           =   0
         Left            =   3750
         TabIndex        =   100
         ToolTipText     =   "Economy"
         Top             =   330
         Width           =   465
      End
      Begin VB.Label lblDepFlno 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   270
         Index           =   0
         Left            =   30
         TabIndex        =   99
         Top             =   30
         Width           =   1110
      End
      Begin VB.Label lblDepDes3 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   270
         Index           =   0
         Left            =   330
         TabIndex        =   98
         ToolTipText     =   "Destination"
         Top             =   330
         Width           =   555
      End
      Begin VB.Label lblDepStod 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   270
         Index           =   0
         Left            =   2850
         TabIndex        =   97
         ToolTipText     =   "STD"
         Top             =   30
         Width           =   615
      End
      Begin VB.Label lblDepEtdi 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   270
         Index           =   0
         Left            =   3450
         TabIndex        =   96
         ToolTipText     =   "ETD"
         Top             =   30
         Width           =   615
      End
      Begin VB.Label lblDepVia3 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   270
         Index           =   0
         Left            =   870
         TabIndex        =   95
         ToolTipText     =   "Next Station"
         Top             =   330
         Width           =   555
      End
      Begin VB.Label lblDepFlti 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   270
         Index           =   0
         Left            =   1125
         TabIndex        =   94
         Top             =   30
         Width           =   300
      End
      Begin VB.Label lblDepOfbl 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   270
         Index           =   0
         Left            =   4050
         TabIndex        =   93
         ToolTipText     =   "ATD (OFB)"
         Top             =   30
         Width           =   615
      End
      Begin VB.Label lblDepPstd 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   270
         Index           =   0
         Left            =   4695
         TabIndex        =   92
         ToolTipText     =   "Bay"
         Top             =   30
         Width           =   645
      End
   End
   Begin VB.PictureBox ArrFlight 
      Appearance      =   0  'Flat
      BackColor       =   &H00E0E0E0&
      ForeColor       =   &H80000008&
      Height          =   660
      Index           =   0
      Left            =   120
      ScaleHeight     =   630
      ScaleWidth      =   5370
      TabIndex        =   76
      Top             =   7260
      Width           =   5400
      Begin VB.Label lblArrTotUld 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   270
         Index           =   0
         Left            =   2355
         MouseIcon       =   "ConnexChart.frx":209C
         TabIndex        =   119
         ToolTipText     =   "Total Transfer ULD (Amount)"
         Top             =   330
         Width           =   465
      End
      Begin VB.Label lblArrTotBag 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   270
         Index           =   0
         Left            =   1905
         MouseIcon       =   "ConnexChart.frx":23A6
         MousePointer    =   99  'Custom
         TabIndex        =   118
         ToolTipText     =   "Total Transfer BAG (Pieces)"
         Top             =   330
         Width           =   465
      End
      Begin VB.Label lblArrTotPax 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   270
         Index           =   0
         Left            =   1455
         MouseIcon       =   "ConnexChart.frx":26B0
         MousePointer    =   99  'Custom
         TabIndex        =   117
         ToolTipText     =   "Total Transfer PAX"
         Top             =   330
         Width           =   465
      End
      Begin VB.Label lblArrTot4 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   270
         Index           =   0
         Left            =   4200
         MouseIcon       =   "ConnexChart.frx":29BA
         TabIndex        =   116
         ToolTipText     =   "Premium Economy"
         Top             =   330
         Width           =   465
      End
      Begin VB.Image picArrStatus 
         Appearance      =   0  'Flat
         BorderStyle     =   1  'Fixed Single
         Height          =   270
         Index           =   0
         Left            =   30
         Picture         =   "ConnexChart.frx":2CC4
         ToolTipText     =   "Flight Status"
         Top             =   330
         Width           =   270
      End
      Begin VB.Label lblArrPsta 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   270
         Index           =   0
         Left            =   4695
         TabIndex        =   90
         ToolTipText     =   "Bay"
         Top             =   30
         Width           =   645
      End
      Begin VB.Label lblArrOnbl 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   270
         Index           =   0
         Left            =   4050
         TabIndex        =   89
         ToolTipText     =   "ATA (ONB)"
         Top             =   30
         Width           =   615
      End
      Begin VB.Label lblArrFlti 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   270
         Index           =   0
         Left            =   1125
         TabIndex        =   88
         ToolTipText     =   "Route Identifier"
         Top             =   30
         Width           =   300
      End
      Begin VB.Label lblArrVia3 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   270
         Index           =   0
         Left            =   870
         TabIndex        =   87
         ToolTipText     =   "Previous Station"
         Top             =   330
         Width           =   555
      End
      Begin VB.Label lblArrEtai 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   270
         Index           =   0
         Left            =   3450
         TabIndex        =   86
         ToolTipText     =   "ETA"
         Top             =   30
         Width           =   615
      End
      Begin VB.Label lblArrStoa 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   270
         Index           =   0
         Left            =   2850
         TabIndex        =   85
         ToolTipText     =   "STA"
         Top             =   30
         Width           =   615
      End
      Begin VB.Label lblArrOrg3 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   270
         Index           =   0
         Left            =   330
         TabIndex        =   84
         ToolTipText     =   "Origin"
         Top             =   330
         Width           =   555
      End
      Begin VB.Label lblArrFlno 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   270
         Index           =   0
         Left            =   30
         TabIndex        =   83
         Top             =   30
         Width           =   1110
      End
      Begin VB.Label lblArrTot3 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   270
         Index           =   0
         Left            =   3750
         MouseIcon       =   "ConnexChart.frx":2E0E
         MousePointer    =   99  'Custom
         TabIndex        =   82
         ToolTipText     =   "Economy"
         Top             =   330
         Width           =   465
      End
      Begin VB.Label lblArrTot2 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   270
         Index           =   0
         Left            =   3300
         MouseIcon       =   "ConnexChart.frx":3118
         TabIndex        =   81
         ToolTipText     =   "Business"
         Top             =   330
         Width           =   465
      End
      Begin VB.Label lblArrTot1 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   270
         Index           =   0
         Left            =   2850
         MouseIcon       =   "ConnexChart.frx":3422
         TabIndex        =   80
         ToolTipText     =   "First Class"
         Top             =   330
         Width           =   465
      End
      Begin VB.Label lblArrCsct 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   270
         Index           =   0
         Left            =   4695
         MouseIcon       =   "ConnexChart.frx":372C
         TabIndex        =   79
         ToolTipText     =   "Shortest Connection Time (Minutes)"
         Top             =   330
         Width           =   645
      End
      Begin VB.Label lblArrRegn 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   270
         Index           =   0
         Left            =   1455
         TabIndex        =   78
         ToolTipText     =   "Aircraft Registration"
         Top             =   30
         Width           =   855
      End
      Begin VB.Label lblArrAct3 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   270
         Index           =   0
         Left            =   2295
         TabIndex        =   77
         ToolTipText     =   "Aircraft Type"
         Top             =   30
         Width           =   525
      End
   End
   Begin VB.PictureBox TimeScroll2 
      Height          =   1125
      Index           =   0
      Left            =   12540
      ScaleHeight     =   1065
      ScaleWidth      =   255
      TabIndex        =   46
      Top             =   1350
      Width           =   315
      Begin VB.CheckBox chkTime3 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   2
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   62
         Top             =   810
         Width           =   255
      End
      Begin VB.CheckBox chkTime3 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   1
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   61
         Top             =   540
         Width           =   255
      End
      Begin VB.CheckBox chkTime2 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   1
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   48
         Top             =   0
         Width           =   255
      End
      Begin VB.CheckBox chkTime1 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   1
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   47
         Top             =   270
         Width           =   255
      End
   End
   Begin VB.PictureBox TimeScroll1 
      Height          =   1125
      Index           =   0
      Left            =   1380
      ScaleHeight     =   1065
      ScaleWidth      =   255
      TabIndex        =   43
      Top             =   1350
      Visible         =   0   'False
      Width           =   315
      Begin VB.CheckBox chkTime3 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   3
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   63
         Top             =   810
         Width           =   255
      End
      Begin VB.CheckBox chkTime3 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   0
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   60
         Top             =   540
         Width           =   255
      End
      Begin VB.CheckBox chkTime1 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   0
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   45
         Top             =   270
         Width           =   255
      End
      Begin VB.CheckBox chkTime2 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   0
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   44
         Top             =   0
         Width           =   255
      End
   End
   Begin VB.PictureBox HorizScroll3 
      Height          =   585
      Index           =   0
      Left            =   12510
      ScaleHeight     =   525
      ScaleWidth      =   255
      TabIndex        =   28
      Top             =   6510
      Width           =   315
      Begin VB.CheckBox chkScroll1 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   2
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   65
         Top             =   270
         Width           =   255
      End
      Begin VB.CheckBox chkScroll2 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   0
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   29
         Top             =   0
         Width           =   255
      End
   End
   Begin VB.PictureBox HorizScroll1 
      Height          =   585
      Index           =   0
      Left            =   1350
      ScaleHeight     =   525
      ScaleWidth      =   255
      TabIndex        =   26
      Top             =   6510
      Width           =   315
      Begin VB.CheckBox chkScroll1 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   1
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   64
         Top             =   270
         Width           =   255
      End
      Begin VB.CheckBox chkScroll1 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   0
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   27
         Top             =   0
         Width           =   255
      End
   End
   Begin VB.PictureBox HorizScroll2 
      Height          =   315
      Index           =   0
      Left            =   1680
      ScaleHeight     =   255
      ScaleWidth      =   8415
      TabIndex        =   24
      Top             =   6510
      Width           =   8475
      Begin VB.HScrollBar HScroll2 
         Height          =   255
         Index           =   0
         LargeChange     =   30
         Left            =   0
         Max             =   3600
         SmallChange     =   5
         TabIndex        =   25
         Top             =   0
         Width           =   8415
      End
   End
   Begin VB.PictureBox BottomRemark 
      AutoRedraw      =   -1  'True
      BackColor       =   &H00C0C0C0&
      Height          =   300
      Index           =   0
      Left            =   1680
      ScaleHeight     =   240
      ScaleWidth      =   8415
      TabIndex        =   23
      Top             =   6840
      Width           =   8475
      Begin VB.Label lblTopText 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "                                         "
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   210
         Index           =   1
         Left            =   60
         TabIndex        =   50
         Top             =   15
         Width           =   1845
      End
   End
   Begin VB.PictureBox TopRemark 
      AutoRedraw      =   -1  'True
      BackColor       =   &H00C0C0C0&
      Height          =   300
      Index           =   0
      Left            =   1710
      ScaleHeight     =   240
      ScaleWidth      =   8415
      TabIndex        =   22
      Top             =   1350
      Width           =   8475
      Begin VB.Label lblMidTime 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "00:00:00"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   240
         Index           =   0
         Left            =   1650
         TabIndex        =   218
         Top             =   0
         Visible         =   0   'False
         Width           =   735
      End
      Begin VB.Label lblRightTime 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00C0FFFF&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "00:00"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   240
         Index           =   0
         Left            =   2730
         TabIndex        =   217
         Top             =   0
         Visible         =   0   'False
         Width           =   525
      End
      Begin VB.Label lblRightDate 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00C0FFFF&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "07MAR07"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   240
         Index           =   0
         Left            =   3270
         TabIndex        =   216
         Top             =   0
         Visible         =   0   'False
         Width           =   870
      End
      Begin VB.Label lblLeftTime 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00C0FFFF&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "00:00"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   240
         Index           =   0
         Left            =   1035
         TabIndex        =   215
         Top             =   0
         Visible         =   0   'False
         Width           =   525
      End
      Begin VB.Label lblLeftDate 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00C0FFFF&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "07MAR07"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   240
         Index           =   0
         Left            =   90
         TabIndex        =   214
         Top             =   0
         Visible         =   0   'False
         Width           =   870
      End
      Begin VB.Label lblTopText 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "                     "
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   210
         Index           =   0
         Left            =   60
         TabIndex        =   49
         Top             =   15
         Width           =   945
      End
   End
   Begin VB.PictureBox WorkArea 
      AutoRedraw      =   -1  'True
      BackColor       =   &H00800000&
      Height          =   3825
      Index           =   0
      Left            =   1680
      ScaleHeight     =   3765
      ScaleWidth      =   10755
      TabIndex        =   21
      Top             =   2580
      Width           =   10815
      Begin VB.PictureBox VScrollArea 
         BackColor       =   &H00E0E0E0&
         Height          =   3525
         Index           =   0
         Left            =   90
         ScaleHeight     =   3465
         ScaleWidth      =   10515
         TabIndex        =   32
         Top             =   90
         Width           =   10575
         Begin VB.PictureBox MidScConx 
            AutoRedraw      =   -1  'True
            Height          =   1755
            Index           =   0
            Left            =   630
            ScaleHeight     =   1695
            ScaleWidth      =   8895
            TabIndex        =   109
            Top             =   1650
            Visible         =   0   'False
            Width           =   8955
            Begin VB.Timer MouseTimer 
               Enabled         =   0   'False
               Index           =   1
               Interval        =   1000
               Left            =   5970
               Top             =   780
            End
            Begin VB.Timer MouseTimer 
               Enabled         =   0   'False
               Index           =   0
               Interval        =   500
               Left            =   5550
               Top             =   780
            End
            Begin VB.PictureBox ArrCnxInfo 
               Appearance      =   0  'Flat
               AutoRedraw      =   -1  'True
               BackColor       =   &H00E0E0E0&
               ForeColor       =   &H80000008&
               Height          =   300
               Index           =   0
               Left            =   1140
               ScaleHeight     =   270
               ScaleWidth      =   1815
               TabIndex        =   137
               Top             =   60
               Visible         =   0   'False
               Width           =   1845
               Begin VB.Label ArrCnxTot4 
                  Alignment       =   2  'Center
                  Appearance      =   0  'Flat
                  BackColor       =   &H00FFFFFF&
                  BorderStyle     =   1  'Fixed Single
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   161
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  ForeColor       =   &H00000000&
                  Height          =   270
                  Index           =   0
                  Left            =   1350
                  MouseIcon       =   "ConnexChart.frx":3A36
                  TabIndex        =   141
                  ToolTipText     =   "World Class"
                  Top             =   0
                  Width           =   465
               End
               Begin VB.Label ArrCnxTot3 
                  Alignment       =   2  'Center
                  Appearance      =   0  'Flat
                  BackColor       =   &H00FFFFFF&
                  BorderStyle     =   1  'Fixed Single
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   161
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  ForeColor       =   &H00000000&
                  Height          =   270
                  Index           =   0
                  Left            =   900
                  MouseIcon       =   "ConnexChart.frx":4300
                  TabIndex        =   140
                  ToolTipText     =   "Economy"
                  Top             =   0
                  Width           =   465
               End
               Begin VB.Label ArrCnxTot2 
                  Alignment       =   2  'Center
                  Appearance      =   0  'Flat
                  BackColor       =   &H00FFFFFF&
                  BorderStyle     =   1  'Fixed Single
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   161
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  ForeColor       =   &H80000008&
                  Height          =   270
                  Index           =   0
                  Left            =   450
                  MouseIcon       =   "ConnexChart.frx":4BCA
                  MousePointer    =   99  'Custom
                  TabIndex        =   139
                  ToolTipText     =   "Business"
                  Top             =   0
                  Width           =   465
               End
               Begin VB.Label ArrCnxTot1 
                  Alignment       =   2  'Center
                  Appearance      =   0  'Flat
                  BackColor       =   &H00FFFFFF&
                  BorderStyle     =   1  'Fixed Single
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   161
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  ForeColor       =   &H80000008&
                  Height          =   270
                  Index           =   0
                  Left            =   0
                  MouseIcon       =   "ConnexChart.frx":5494
                  TabIndex        =   138
                  ToolTipText     =   "First Class"
                  Top             =   0
                  Width           =   465
               End
            End
            Begin VB.PictureBox DepLblPanel 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               ForeColor       =   &H80000008&
               Height          =   660
               Index           =   0
               Left            =   7860
               ScaleHeight     =   630
               ScaleWidth      =   435
               TabIndex        =   133
               Top             =   90
               Visible         =   0   'False
               Width           =   465
               Begin VB.Label lblDepTpax 
                  Alignment       =   2  'Center
                  Appearance      =   0  'Flat
                  BackColor       =   &H0000FF00&
                  BorderStyle     =   1  'Fixed Single
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   161
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  ForeColor       =   &H80000008&
                  Height          =   255
                  Index           =   0
                  Left            =   -30
                  TabIndex        =   136
                  ToolTipText     =   "Total Transfer Pax"
                  Top             =   -30
                  Width           =   345
               End
               Begin VB.Label lblDepTbag 
                  Alignment       =   2  'Center
                  Appearance      =   0  'Flat
                  BackColor       =   &H0000FFFF&
                  BorderStyle     =   1  'Fixed Single
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   161
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  ForeColor       =   &H80000008&
                  Height          =   240
                  Index           =   0
                  Left            =   -60
                  TabIndex        =   135
                  ToolTipText     =   "Total Pieces Baggage"
                  Top             =   210
                  Width           =   375
               End
               Begin VB.Label lblDepTuld 
                  Alignment       =   2  'Center
                  Appearance      =   0  'Flat
                  BackColor       =   &H00FF0000&
                  BorderStyle     =   1  'Fixed Single
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   161
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  ForeColor       =   &H00FFFFFF&
                  Height          =   225
                  Index           =   0
                  Left            =   -60
                  TabIndex        =   134
                  ToolTipText     =   "Amount of ULDs"
                  Top             =   435
                  Width           =   465
               End
            End
            Begin VB.PictureBox ArrLblPanel 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               ForeColor       =   &H80000008&
               Height          =   660
               Index           =   0
               Left            =   150
               ScaleHeight     =   630
               ScaleWidth      =   435
               TabIndex        =   129
               Top             =   60
               Visible         =   0   'False
               Width           =   465
               Begin VB.Label lblArrTpax 
                  Alignment       =   2  'Center
                  Appearance      =   0  'Flat
                  BackColor       =   &H0000FF00&
                  BorderStyle     =   1  'Fixed Single
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   161
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  ForeColor       =   &H80000008&
                  Height          =   225
                  Index           =   0
                  Left            =   -255
                  TabIndex        =   130
                  ToolTipText     =   "Total Transfer Pax"
                  Top             =   -15
                  Width           =   465
               End
               Begin VB.Label lblArrTuld 
                  Alignment       =   2  'Center
                  Appearance      =   0  'Flat
                  BackColor       =   &H00E0E0E0&
                  BorderStyle     =   1  'Fixed Single
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   161
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  ForeColor       =   &H00FFFFFF&
                  Height          =   225
                  Index           =   0
                  Left            =   -15
                  TabIndex        =   132
                  ToolTipText     =   "Amount of ULDs"
                  Top             =   405
                  Width           =   465
               End
               Begin VB.Label lblArrTbag 
                  Alignment       =   2  'Center
                  Appearance      =   0  'Flat
                  BackColor       =   &H00E0E0E0&
                  BorderStyle     =   1  'Fixed Single
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   161
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  ForeColor       =   &H80000008&
                  Height          =   240
                  Index           =   0
                  Left            =   -15
                  TabIndex        =   131
                  ToolTipText     =   "Total Pieces Baggage"
                  Top             =   210
                  Width           =   465
               End
            End
            Begin VB.PictureBox DepCnxInfo 
               Appearance      =   0  'Flat
               AutoRedraw      =   -1  'True
               BackColor       =   &H00000000&
               ForeColor       =   &H80000008&
               Height          =   300
               Index           =   0
               Left            =   4860
               ScaleHeight     =   270
               ScaleWidth      =   2490
               TabIndex        =   124
               Top             =   300
               Visible         =   0   'False
               Width           =   2520
               Begin VB.Label DepCnxCsct 
                  Alignment       =   2  'Center
                  Appearance      =   0  'Flat
                  BackColor       =   &H80000005&
                  BorderStyle     =   1  'Fixed Single
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   161
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  ForeColor       =   &H80000008&
                  Height          =   270
                  Index           =   0
                  Left            =   1845
                  TabIndex        =   142
                  ToolTipText     =   "Connection Time (Minutes)"
                  Top             =   0
                  Width           =   645
               End
               Begin VB.Label DepCnxTot1 
                  Alignment       =   2  'Center
                  Appearance      =   0  'Flat
                  BackColor       =   &H00FFFFFF&
                  BorderStyle     =   1  'Fixed Single
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   161
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  ForeColor       =   &H80000008&
                  Height          =   270
                  Index           =   0
                  Left            =   0
                  TabIndex        =   128
                  ToolTipText     =   "First Class"
                  Top             =   0
                  Width           =   465
               End
               Begin VB.Label DepCnxTot2 
                  Alignment       =   2  'Center
                  Appearance      =   0  'Flat
                  BackColor       =   &H00FFFFFF&
                  BorderStyle     =   1  'Fixed Single
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   161
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  ForeColor       =   &H80000008&
                  Height          =   270
                  Index           =   0
                  Left            =   450
                  TabIndex        =   127
                  ToolTipText     =   "Business"
                  Top             =   0
                  Width           =   465
               End
               Begin VB.Label DepCnxTot3 
                  Alignment       =   2  'Center
                  Appearance      =   0  'Flat
                  BackColor       =   &H00FFFFFF&
                  BorderStyle     =   1  'Fixed Single
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   161
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  ForeColor       =   &H00000000&
                  Height          =   270
                  Index           =   0
                  Left            =   900
                  TabIndex        =   126
                  ToolTipText     =   "Economy"
                  Top             =   0
                  Width           =   465
               End
               Begin VB.Label DepCnxTot4 
                  Alignment       =   2  'Center
                  Appearance      =   0  'Flat
                  BackColor       =   &H00FFFFFF&
                  BorderStyle     =   1  'Fixed Single
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   161
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  ForeColor       =   &H00000000&
                  Height          =   270
                  Index           =   0
                  Left            =   1350
                  TabIndex        =   125
                  ToolTipText     =   "World Class"
                  Top             =   0
                  Width           =   465
               End
            End
            Begin VB.PictureBox RightTime 
               Appearance      =   0  'Flat
               BackColor       =   &H00000000&
               ForeColor       =   &H80000008&
               Height          =   1440
               Index           =   0
               Left            =   3180
               ScaleHeight     =   1410
               ScaleWidth      =   0
               TabIndex        =   113
               Top             =   0
               Visible         =   0   'False
               Width           =   30
            End
            Begin VB.PictureBox LeftTime 
               Appearance      =   0  'Flat
               BackColor       =   &H00000000&
               ForeColor       =   &H80000008&
               Height          =   1440
               Index           =   0
               Left            =   870
               ScaleHeight     =   1410
               ScaleWidth      =   0
               TabIndex        =   112
               Top             =   0
               Visible         =   0   'False
               Width           =   30
            End
            Begin VB.PictureBox MidTime 
               Appearance      =   0  'Flat
               BackColor       =   &H000000FF&
               ForeColor       =   &H80000008&
               Height          =   1440
               Index           =   0
               Left            =   3390
               ScaleHeight     =   1410
               ScaleWidth      =   15
               TabIndex        =   111
               Top             =   0
               Visible         =   0   'False
               Width           =   45
            End
            Begin VB.Image picArrCnx 
               Appearance      =   0  'Flat
               Height          =   240
               Index           =   0
               Left            =   1050
               MouseIcon       =   "ConnexChart.frx":5D5E
               MousePointer    =   99  'Custom
               Picture         =   "ConnexChart.frx":6068
               Top             =   570
               Visible         =   0   'False
               Width           =   240
            End
            Begin VB.Line RightLine 
               BorderStyle     =   2  'Dash
               Index           =   1
               Visible         =   0   'False
               X1              =   4560
               X2              =   4560
               Y1              =   120
               Y2              =   900
            End
            Begin VB.Line LeftLine 
               BorderColor     =   &H00000000&
               BorderStyle     =   2  'Dash
               Index           =   1
               Visible         =   0   'False
               X1              =   4020
               X2              =   4020
               Y1              =   90
               Y2              =   900
            End
            Begin VB.Line RightLine 
               BorderStyle     =   2  'Dash
               Index           =   0
               Visible         =   0   'False
               X1              =   4470
               X2              =   4470
               Y1              =   120
               Y2              =   900
            End
            Begin VB.Line LeftLine 
               BorderColor     =   &H00000000&
               BorderStyle     =   2  'Dash
               Index           =   0
               Visible         =   0   'False
               X1              =   3930
               X2              =   3930
               Y1              =   90
               Y2              =   900
            End
            Begin VB.Image picBarCnx 
               Appearance      =   0  'Flat
               Height          =   240
               Index           =   0
               Left            =   2640
               MouseIcon       =   "ConnexChart.frx":61B2
               Picture         =   "ConnexChart.frx":62FC
               Top             =   570
               Visible         =   0   'False
               Width           =   240
            End
            Begin VB.Image CnxArrIcon 
               Appearance      =   0  'Flat
               Height          =   240
               Index           =   2
               Left            =   2040
               Picture         =   "ConnexChart.frx":6446
               Top             =   840
               Visible         =   0   'False
               Width           =   240
            End
            Begin VB.Image CnxArrIcon 
               Appearance      =   0  'Flat
               Height          =   240
               Index           =   1
               Left            =   1740
               Picture         =   "ConnexChart.frx":6590
               Top             =   840
               Visible         =   0   'False
               Width           =   240
            End
            Begin VB.Image CnxArrIcon 
               Appearance      =   0  'Flat
               Height          =   240
               Index           =   0
               Left            =   1440
               Picture         =   "ConnexChart.frx":66DA
               Top             =   840
               Visible         =   0   'False
               Width           =   240
            End
            Begin VB.Label lblCnxUldBar 
               Alignment       =   2  'Center
               Appearance      =   0  'Flat
               BackColor       =   &H00FF0000&
               BorderStyle     =   1  'Fixed Single
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   161
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   90
               Index           =   0
               Left            =   1440
               TabIndex        =   115
               Top             =   720
               Visible         =   0   'False
               Width           =   1065
            End
            Begin VB.Label lblCnxBagBar 
               Alignment       =   2  'Center
               Appearance      =   0  'Flat
               BackColor       =   &H0000FFFF&
               BorderStyle     =   1  'Fixed Single
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   161
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   105
               Index           =   0
               Left            =   1440
               TabIndex        =   114
               Top             =   630
               Visible         =   0   'False
               Width           =   1065
            End
            Begin VB.Label lblCnxPaxBar 
               Alignment       =   2  'Center
               Appearance      =   0  'Flat
               BackColor       =   &H0000FF00&
               BorderStyle     =   1  'Fixed Single
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   161
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   105
               Index           =   0
               Left            =   1440
               TabIndex        =   110
               Top             =   540
               Visible         =   0   'False
               Width           =   1065
            End
            Begin VB.Image picDepCnx 
               Appearance      =   0  'Flat
               Height          =   240
               Index           =   0
               Left            =   8370
               Picture         =   "ConnexChart.frx":6824
               Top             =   360
               Visible         =   0   'False
               Width           =   240
            End
         End
         Begin VB.PictureBox HScrollArea 
            Height          =   1125
            Index           =   0
            Left            =   1950
            ScaleHeight     =   1065
            ScaleWidth      =   6405
            TabIndex        =   37
            Top             =   120
            Width           =   6465
            Begin VB.PictureBox TimeLine2 
               Appearance      =   0  'Flat
               BackColor       =   &H000000FF&
               ForeColor       =   &H80000008&
               Height          =   1440
               Index           =   0
               Left            =   3810
               ScaleHeight     =   1410
               ScaleWidth      =   15
               TabIndex        =   38
               Top             =   0
               Width           =   45
            End
            Begin VB.PictureBox RangeLine 
               Appearance      =   0  'Flat
               BackColor       =   &H0000FFFF&
               FillColor       =   &H00FFFFFF&
               ForeColor       =   &H80000008&
               Height          =   1440
               Index           =   1
               Left            =   120
               ScaleHeight     =   1410
               ScaleWidth      =   15
               TabIndex        =   69
               Top             =   0
               Visible         =   0   'False
               Width           =   45
            End
            Begin VB.PictureBox RangeLine 
               Appearance      =   0  'Flat
               BackColor       =   &H0000FFFF&
               ForeColor       =   &H80000008&
               Height          =   1440
               Index           =   0
               Left            =   30
               ScaleHeight     =   1410
               ScaleWidth      =   15
               TabIndex        =   68
               Top             =   0
               Visible         =   0   'False
               Width           =   45
            End
            Begin VB.PictureBox ChartLine2 
               AutoRedraw      =   -1  'True
               Height          =   780
               Index           =   0
               Left            =   210
               ScaleHeight     =   720
               ScaleWidth      =   6045
               TabIndex        =   39
               Top             =   120
               Visible         =   0   'False
               Width           =   6105
               Begin VB.Image picDep4 
                  Appearance      =   0  'Flat
                  BorderStyle     =   1  'Fixed Single
                  Height          =   270
                  Index           =   0
                  Left            =   5370
                  Picture         =   "ConnexChart.frx":696E
                  Stretch         =   -1  'True
                  Top             =   45
                  Visible         =   0   'False
                  Width           =   270
               End
               Begin VB.Image picArr4 
                  Appearance      =   0  'Flat
                  BorderStyle     =   1  'Fixed Single
                  Height          =   270
                  Index           =   0
                  Left            =   2490
                  Picture         =   "ConnexChart.frx":6AB8
                  Stretch         =   -1  'True
                  Top             =   45
                  Visible         =   0   'False
                  Width           =   270
               End
               Begin VB.Image picDep3 
                  Appearance      =   0  'Flat
                  BorderStyle     =   1  'Fixed Single
                  Height          =   270
                  Index           =   0
                  Left            =   5070
                  Picture         =   "ConnexChart.frx":6C02
                  Stretch         =   -1  'True
                  Top             =   45
                  Visible         =   0   'False
                  Width           =   270
               End
               Begin VB.Image picDep2 
                  Appearance      =   0  'Flat
                  BorderStyle     =   1  'Fixed Single
                  Height          =   270
                  Index           =   0
                  Left            =   4770
                  Picture         =   "ConnexChart.frx":6D4C
                  Stretch         =   -1  'True
                  Top             =   45
                  Visible         =   0   'False
                  Width           =   270
               End
               Begin VB.Image picArr3 
                  Appearance      =   0  'Flat
                  BorderStyle     =   1  'Fixed Single
                  Height          =   270
                  Index           =   0
                  Left            =   2190
                  Picture         =   "ConnexChart.frx":6E96
                  Stretch         =   -1  'True
                  Top             =   45
                  Visible         =   0   'False
                  Width           =   270
               End
               Begin VB.Image picArr2 
                  Appearance      =   0  'Flat
                  BorderStyle     =   1  'Fixed Single
                  Height          =   270
                  Index           =   0
                  Left            =   1890
                  Picture         =   "ConnexChart.frx":6FE0
                  Stretch         =   -1  'True
                  Top             =   45
                  Visible         =   0   'False
                  Width           =   270
               End
               Begin VB.Label lblArrJob2 
                  Alignment       =   2  'Center
                  Appearance      =   0  'Flat
                  BackColor       =   &H0000FF00&
                  BorderStyle     =   1  'Fixed Single
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   161
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  ForeColor       =   &H80000008&
                  Height          =   90
                  Index           =   0
                  Left            =   420
                  TabIndex        =   75
                  Top             =   210
                  Visible         =   0   'False
                  Width           =   1095
               End
               Begin VB.Label lblArrJob1 
                  Alignment       =   2  'Center
                  Appearance      =   0  'Flat
                  BackColor       =   &H0000FF00&
                  BorderStyle     =   1  'Fixed Single
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   161
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  ForeColor       =   &H80000008&
                  Height          =   90
                  Index           =   0
                  Left            =   870
                  TabIndex        =   74
                  Top             =   90
                  Visible         =   0   'False
                  Width           =   615
               End
               Begin VB.Label lblDepJob2 
                  Alignment       =   2  'Center
                  Appearance      =   0  'Flat
                  BackColor       =   &H0000FF00&
                  BorderStyle     =   1  'Fixed Single
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   161
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  ForeColor       =   &H80000008&
                  Height          =   90
                  Index           =   0
                  Left            =   3300
                  TabIndex        =   73
                  Top             =   210
                  Visible         =   0   'False
                  Width           =   1095
               End
               Begin VB.Label lblDepJob1 
                  Alignment       =   2  'Center
                  Appearance      =   0  'Flat
                  BackColor       =   &H0000FF00&
                  BorderStyle     =   1  'Fixed Single
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   161
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  ForeColor       =   &H80000008&
                  Height          =   90
                  Index           =   0
                  Left            =   3510
                  TabIndex        =   72
                  Top             =   90
                  Visible         =   0   'False
                  Width           =   885
               End
               Begin VB.Image picArr1 
                  Appearance      =   0  'Flat
                  BorderStyle     =   1  'Fixed Single
                  Height          =   270
                  Index           =   0
                  Left            =   1590
                  Picture         =   "ConnexChart.frx":712A
                  Stretch         =   -1  'True
                  Top             =   45
                  Visible         =   0   'False
                  Width           =   270
               End
               Begin VB.Image picDep1 
                  Appearance      =   0  'Flat
                  BorderStyle     =   1  'Fixed Single
                  Height          =   270
                  Index           =   0
                  Left            =   4470
                  Picture         =   "ConnexChart.frx":7274
                  Stretch         =   -1  'True
                  Top             =   45
                  Visible         =   0   'False
                  Width           =   270
               End
               Begin VB.Label lblBarLineColor 
                  Alignment       =   2  'Center
                  Appearance      =   0  'Flat
                  BackColor       =   &H00808080&
                  BorderStyle     =   1  'Fixed Single
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   161
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  ForeColor       =   &H80000008&
                  Height          =   300
                  Index           =   0
                  Left            =   30
                  TabIndex        =   71
                  Top             =   30
                  Visible         =   0   'False
                  Width           =   285
               End
               Begin VB.Label lblTowBar 
                  Alignment       =   2  'Center
                  Appearance      =   0  'Flat
                  BackColor       =   &H80000005&
                  BorderStyle     =   1  'Fixed Single
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   161
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  ForeColor       =   &H80000008&
                  Height          =   240
                  Index           =   0
                  Left            =   2820
                  TabIndex        =   67
                  Top             =   210
                  Visible         =   0   'False
                  Width           =   375
               End
               Begin VB.Label lblDepBar 
                  Alignment       =   2  'Center
                  Appearance      =   0  'Flat
                  BackColor       =   &H0000FF00&
                  BorderStyle     =   1  'Fixed Single
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   161
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  ForeColor       =   &H80000008&
                  Height          =   270
                  Index           =   0
                  Left            =   3240
                  TabIndex        =   66
                  Top             =   30
                  Visible         =   0   'False
                  Width           =   1095
               End
               Begin VB.Label lblArrBar 
                  Alignment       =   2  'Center
                  Appearance      =   0  'Flat
                  BackColor       =   &H0000FFFF&
                  BorderStyle     =   1  'Fixed Single
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   161
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  ForeColor       =   &H80000008&
                  Height          =   270
                  Index           =   0
                  Left            =   360
                  TabIndex        =   58
                  Top             =   30
                  Visible         =   0   'False
                  Width           =   1005
               End
            End
         End
         Begin VB.PictureBox RightScale 
            BackColor       =   &H00C0C0C0&
            Height          =   1125
            Index           =   0
            Left            =   9450
            ScaleHeight     =   1065
            ScaleWidth      =   945
            TabIndex        =   35
            Top             =   120
            Width           =   1005
            Begin VB.PictureBox ChartLine3 
               AutoRedraw      =   -1  'True
               Height          =   780
               Index           =   0
               Left            =   90
               ScaleHeight     =   720
               ScaleWidth      =   705
               TabIndex        =   36
               Top             =   120
               Width           =   765
            End
         End
         Begin VB.PictureBox LeftScale 
            BackColor       =   &H00C0C0C0&
            Height          =   1125
            Index           =   0
            Left            =   30
            ScaleHeight     =   1065
            ScaleWidth      =   825
            TabIndex        =   33
            Top             =   120
            Width           =   885
            Begin VB.PictureBox ChartLine1 
               AutoRedraw      =   -1  'True
               Height          =   780
               Index           =   0
               Left            =   90
               ScaleHeight     =   720
               ScaleWidth      =   525
               TabIndex        =   34
               Top             =   120
               Width           =   585
            End
         End
      End
   End
   Begin VB.PictureBox VertScroll2 
      Height          =   2175
      Index           =   0
      Left            =   12630
      ScaleHeight     =   2115
      ScaleWidth      =   255
      TabIndex        =   18
      Top             =   2580
      Width           =   315
      Begin VB.VScrollBar VScroll2 
         Height          =   1605
         Index           =   0
         LargeChange     =   10
         Left            =   0
         Max             =   100
         TabIndex        =   20
         Top             =   0
         Width           =   255
      End
   End
   Begin VB.PictureBox TimePanel 
      BackColor       =   &H00E0E0E0&
      Height          =   825
      Index           =   0
      Left            =   1710
      ScaleHeight     =   765
      ScaleWidth      =   10605
      TabIndex        =   17
      Top             =   1650
      Width           =   10665
      Begin VB.PictureBox TimeScale 
         AutoRedraw      =   -1  'True
         Height          =   825
         Index           =   0
         Left            =   1230
         ScaleHeight     =   765
         ScaleWidth      =   5925
         TabIndex        =   31
         Top             =   -30
         Width           =   5985
         Begin VB.Timer TimeScaleTimer 
            Enabled         =   0   'False
            Interval        =   1000
            Left            =   1260
            Top             =   0
         End
         Begin VB.PictureBox TimeLine1 
            Appearance      =   0  'Flat
            BackColor       =   &H000000FF&
            ForeColor       =   &H80000008&
            Height          =   1440
            Index           =   0
            Left            =   5400
            ScaleHeight     =   1410
            ScaleWidth      =   15
            TabIndex        =   40
            Top             =   255
            Width           =   45
         End
         Begin VB.Label lblDate 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "24SEP07"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   210
            Index           =   0
            Left            =   4215
            TabIndex        =   70
            Top             =   540
            Visible         =   0   'False
            Width           =   675
         End
         Begin VB.Image icoCflRange 
            Height          =   240
            Index           =   1
            Left            =   5490
            Picture         =   "ConnexChart.frx":73BE
            Top             =   510
            Visible         =   0   'False
            Width           =   240
         End
         Begin VB.Image icoCflRange 
            Height          =   240
            Index           =   0
            Left            =   5130
            Picture         =   "ConnexChart.frx":7508
            Top             =   510
            Visible         =   0   'False
            Width           =   240
         End
         Begin VB.Label lblBgnTime 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BackColor       =   &H000080FF&
            Caption         =   "24SEP07"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   210
            Index           =   0
            Left            =   120
            TabIndex        =   59
            Top             =   540
            Visible         =   0   'False
            Width           =   855
         End
         Begin VB.Label lblMin10 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BackColor       =   &H00000000&
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   0
            Left            =   4680
            TabIndex        =   57
            Top             =   345
            Visible         =   0   'False
            Width           =   45
         End
         Begin VB.Label lblMin30 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BackColor       =   &H00000000&
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   210
            Index           =   0
            Left            =   4590
            TabIndex        =   56
            Top             =   300
            Visible         =   0   'False
            Width           =   45
         End
         Begin VB.Label lblMin1 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BackColor       =   &H00000000&
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   60
            Index           =   0
            Left            =   4860
            TabIndex        =   55
            Top             =   450
            Visible         =   0   'False
            Width           =   45
         End
         Begin VB.Label lblMin5 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BackColor       =   &H00000000&
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   120
            Index           =   0
            Left            =   4770
            TabIndex        =   54
            Top             =   390
            Visible         =   0   'False
            Width           =   45
         End
         Begin VB.Label lblMin60 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BackColor       =   &H00000000&
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   270
            Index           =   0
            Left            =   4500
            TabIndex        =   53
            Top             =   240
            Visible         =   0   'False
            Width           =   45
         End
         Begin VB.Label lblDuration 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BackColor       =   &H00C0FFFF&
            BackStyle       =   0  'Transparent
            BorderStyle     =   1  'Fixed Single
            Caption         =   "01:00"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   225
            Left            =   1140
            TabIndex        =   52
            Top             =   525
            Visible         =   0   'False
            Width           =   2895
         End
         Begin VB.Label lblHour 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "15:00"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   210
            Index           =   0
            Left            =   4305
            TabIndex        =   51
            Top             =   45
            Visible         =   0   'False
            Width           =   435
         End
         Begin VB.Label lblEndTime 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BackColor       =   &H00C0FFFF&
            BorderStyle     =   1  'Fixed Single
            Caption         =   "16:00"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   240
            Index           =   0
            Left            =   2700
            TabIndex        =   42
            Top             =   30
            Visible         =   0   'False
            Width           =   645
         End
         Begin VB.Label lblCurTime 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            Caption         =   "00:00:00"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   240
            Index           =   0
            Left            =   5070
            TabIndex        =   41
            Top             =   30
            Width           =   735
         End
      End
   End
   Begin VB.PictureBox TopPanel 
      BackColor       =   &H000040C0&
      Height          =   690
      Left            =   120
      ScaleHeight     =   630
      ScaleWidth      =   14805
      TabIndex        =   16
      Top             =   510
      Visible         =   0   'False
      Width           =   14865
      Begin VB.Frame Frame7 
         Caption         =   "Test Panel"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   615
         Left            =   4830
         TabIndex        =   210
         Top             =   0
         Width           =   1215
         Begin VB.CheckBox chkCnxTest 
            Caption         =   "CNX"
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   0
            Left            =   90
            Style           =   1  'Graphical
            TabIndex        =   212
            Top             =   240
            Width           =   525
         End
         Begin VB.CheckBox chkCnxTest 
            Caption         =   "LAY"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   1
            Left            =   615
            Style           =   1  'Graphical
            TabIndex        =   211
            Top             =   240
            Width           =   525
         End
      End
      Begin VB.Frame Frame5 
         Caption         =   "Data Filter"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   615
         Left            =   3090
         TabIndex        =   205
         Top             =   0
         Visible         =   0   'False
         Width           =   1695
         Begin VB.CheckBox chkCnxData 
            Caption         =   "ACT"
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   2
            Left            =   1110
            Style           =   1  'Graphical
            TabIndex        =   208
            Top             =   240
            Width           =   510
         End
         Begin VB.CheckBox chkCnxData 
            Caption         =   "APC"
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   1
            Left            =   600
            Style           =   1  'Graphical
            TabIndex        =   207
            Top             =   240
            Width           =   510
         End
         Begin VB.CheckBox chkCnxData 
            Caption         =   "ALC"
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   0
            Left            =   90
            Style           =   1  'Graphical
            TabIndex        =   206
            Top             =   240
            Width           =   510
         End
      End
      Begin VB.Frame Frame1 
         Caption         =   "Layout Types"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   615
         Left            =   6090
         TabIndex        =   197
         Top             =   0
         Visible         =   0   'False
         Width           =   2145
         Begin VB.CheckBox chkLayout 
            Caption         =   "5"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   6
            Left            =   3210
            Style           =   1  'Graphical
            TabIndex        =   204
            Top             =   0
            Visible         =   0   'False
            Width           =   450
         End
         Begin VB.CheckBox chkLayout 
            Caption         =   "3"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   5
            Left            =   2730
            Style           =   1  'Graphical
            TabIndex        =   203
            Top             =   0
            Visible         =   0   'False
            Width           =   450
         End
         Begin VB.CheckBox chkLayout 
            Caption         =   "0"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   0
            Left            =   2220
            Style           =   1  'Graphical
            TabIndex        =   202
            Top             =   0
            Visible         =   0   'False
            Width           =   450
         End
         Begin VB.CheckBox chkLayout 
            Caption         =   "2"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   2
            Left            =   540
            Style           =   1  'Graphical
            TabIndex        =   201
            Top             =   240
            Width           =   510
         End
         Begin VB.CheckBox chkLayout 
            Caption         =   "1"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   1
            Left            =   90
            Style           =   1  'Graphical
            TabIndex        =   200
            Top             =   240
            Width           =   450
         End
         Begin VB.CheckBox chkLayout 
            Caption         =   "3"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   3
            Left            =   1050
            Style           =   1  'Graphical
            TabIndex        =   199
            Top             =   240
            Width           =   510
         End
         Begin VB.CheckBox chkLayout 
            Caption         =   "4"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   4
            Left            =   1560
            Style           =   1  'Graphical
            TabIndex        =   198
            Top             =   240
            Width           =   510
         End
      End
   End
   Begin VB.PictureBox VertScroll1 
      Height          =   2175
      Index           =   0
      Left            =   1230
      ScaleHeight     =   2115
      ScaleWidth      =   255
      TabIndex        =   15
      Top             =   2580
      Visible         =   0   'False
      Width           =   315
      Begin VB.VScrollBar VScroll1 
         Height          =   1605
         Index           =   0
         LargeChange     =   10
         Left            =   0
         Max             =   100
         TabIndex        =   19
         Top             =   0
         Width           =   255
      End
   End
   Begin VB.PictureBox ButtonPanel 
      AutoRedraw      =   -1  'True
      Height          =   375
      Left            =   120
      ScaleHeight     =   315
      ScaleWidth      =   10425
      TabIndex        =   14
      Top             =   120
      Width           =   10485
      Begin VB.CheckBox chkWork 
         Caption         =   "Tools"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   1
         Left            =   1050
         Style           =   1  'Graphical
         TabIndex        =   196
         Top             =   0
         Width           =   1035
      End
      Begin VB.CheckBox chkWork 
         Caption         =   "Overlap"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   0
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   30
         Top             =   0
         Width           =   1035
      End
   End
   Begin VB.PictureBox LeftPanel 
      Height          =   4065
      Index           =   0
      Left            =   120
      ScaleHeight     =   4005
      ScaleWidth      =   915
      TabIndex        =   13
      Top             =   1530
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.PictureBox RightPanel 
      AutoRedraw      =   -1  'True
      AutoSize        =   -1  'True
      Height          =   5160
      Left            =   13770
      ScaleHeight     =   5100
      ScaleWidth      =   1155
      TabIndex        =   0
      Top             =   1500
      Width           =   1215
      Begin VB.CheckBox Check2 
         Caption         =   "Critical"
         Height          =   225
         Left            =   60
         TabIndex        =   209
         Top             =   4500
         Visible         =   0   'False
         Width           =   1005
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Outside"
         Height          =   225
         Left            =   60
         TabIndex        =   195
         Top             =   4260
         Visible         =   0   'False
         Width           =   1005
      End
      Begin VB.Frame fraYButtonPanel 
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   0  'None
         Height          =   1020
         Index           =   0
         Left            =   60
         TabIndex        =   4
         Top             =   390
         Width           =   1035
         Begin VB.CheckBox chkSelDeco 
            Caption         =   "CL"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   3
            Left            =   0
            Style           =   1  'Graphical
            TabIndex        =   108
            Tag             =   "SETUP"
            ToolTipText     =   "Use of Left Scale ON/OFF"
            Top             =   1320
            Visible         =   0   'False
            Width           =   510
         End
         Begin VB.CheckBox chkSelDeco 
            Caption         =   "CR"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   4
            Left            =   525
            Style           =   1  'Graphical
            TabIndex        =   107
            Tag             =   "SETUP"
            ToolTipText     =   "Use of Right Scale ON/OFF"
            Top             =   1320
            Visible         =   0   'False
            Width           =   510
         End
         Begin VB.CheckBox chkSelDeco 
            Caption         =   "CX Panel"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   2
            Left            =   0
            Style           =   1  'Graphical
            TabIndex        =   106
            Tag             =   "SETUP"
            ToolTipText     =   "Use of Left Scale ON/OFF"
            Top             =   660
            Width           =   1035
         End
         Begin VB.CheckBox chkAppl 
            Caption         =   "Close"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   0
            Left            =   0
            Style           =   1  'Graphical
            TabIndex        =   9
            Tag             =   "CLOSE"
            Top             =   0
            Width           =   1035
         End
         Begin VB.CheckBox chkUtc 
            Caption         =   "UTC"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   0
            Left            =   525
            Style           =   1  'Graphical
            TabIndex        =   8
            Tag             =   "SETUP"
            ToolTipText     =   "Switch to UTC Times"
            Top             =   330
            Width           =   510
         End
         Begin VB.CheckBox chkUtc 
            Caption         =   "LOC"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   1
            Left            =   0
            Style           =   1  'Graphical
            TabIndex        =   7
            Tag             =   "SETUP"
            ToolTipText     =   "Switch to Local Times"
            Top             =   330
            Width           =   510
         End
         Begin VB.CheckBox chkSelDeco 
            Caption         =   "RS"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   1
            Left            =   525
            Style           =   1  'Graphical
            TabIndex        =   6
            Tag             =   "SETUP"
            ToolTipText     =   "Use of Right Scale ON/OFF"
            Top             =   1170
            Visible         =   0   'False
            Width           =   510
         End
         Begin VB.CheckBox chkSelDeco 
            Caption         =   "LS"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   0
            Left            =   0
            Style           =   1  'Graphical
            TabIndex        =   5
            Tag             =   "SETUP"
            ToolTipText     =   "Use of Left Scale ON/OFF"
            Top             =   1170
            Visible         =   0   'False
            Width           =   510
         End
      End
      Begin VB.CheckBox chkTabIsVisible 
         Height          =   225
         Index           =   0
         Left            =   30
         MaskColor       =   &H8000000F&
         Style           =   1  'Graphical
         TabIndex        =   3
         Tag             =   "-1"
         Top             =   2790
         Visible         =   0   'False
         Width           =   210
      End
      Begin VB.CheckBox OnTop 
         Caption         =   "Top"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   60
         Style           =   1  'Graphical
         TabIndex        =   2
         Top             =   30
         Width           =   720
      End
      Begin VB.CheckBox chkTerminate 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   780
         Style           =   1  'Graphical
         TabIndex        =   1
         ToolTipText     =   "Bypass the Clear Function"
         Top             =   30
         Width           =   315
      End
      Begin VB.Label lblTabShadow 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Shadow"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   225
         Index           =   0
         Left            =   270
         TabIndex        =   11
         Top             =   3030
         Visible         =   0   'False
         Width           =   690
      End
      Begin VB.Label lblTabName 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Label1"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   225
         Index           =   0
         Left            =   300
         TabIndex        =   10
         Top             =   2850
         Visible         =   0   'False
         Width           =   570
      End
      Begin VB.Line linDark 
         BorderColor     =   &H00404040&
         Index           =   0
         Visible         =   0   'False
         X1              =   0
         X2              =   1200
         Y1              =   3360
         Y2              =   3360
      End
      Begin VB.Line linLight 
         BorderColor     =   &H00FFFFFF&
         Index           =   0
         Visible         =   0   'False
         X1              =   0
         X2              =   1200
         Y1              =   2760
         Y2              =   2760
      End
   End
   Begin MSComctlLib.StatusBar StatusBar 
      Align           =   2  'Align Bottom
      Height          =   315
      Left            =   0
      TabIndex        =   12
      Top             =   10560
      Width           =   15165
      _ExtentX        =   26749
      _ExtentY        =   556
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   7
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Enabled         =   0   'False
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   18203
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   6
            Alignment       =   1
            AutoSize        =   2
            Object.Width           =   1429
            MinWidth        =   1058
            TextSave        =   "12/5/2007"
         EndProperty
         BeginProperty Panel4 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   5
            Alignment       =   1
            AutoSize        =   2
            Object.Width           =   1429
            MinWidth        =   1058
            TextSave        =   "10:49 AM"
         EndProperty
         BeginProperty Panel5 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   1
            Alignment       =   1
            AutoSize        =   2
            Enabled         =   0   'False
            Object.Width           =   953
            MinWidth        =   706
            TextSave        =   "CAPS"
         EndProperty
         BeginProperty Panel6 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   2
            Alignment       =   1
            AutoSize        =   2
            Enabled         =   0   'False
            Object.Width           =   794
            MinWidth        =   706
            TextSave        =   "NUM"
         EndProperty
         BeginProperty Panel7 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   3
            Alignment       =   1
            AutoSize        =   2
            Enabled         =   0   'False
            Object.Width           =   714
            MinWidth        =   706
            TextSave        =   "INS"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Image picNeutral 
      Appearance      =   0  'Flat
      BorderStyle     =   1  'Fixed Single
      Height          =   270
      Index           =   0
      Left            =   12480
      Picture         =   "ConnexChart.frx":7652
      Stretch         =   -1  'True
      Top             =   9180
      Width           =   270
   End
   Begin VB.Image imgLookUpB 
      Height          =   240
      Left            =   12120
      Picture         =   "ConnexChart.frx":779C
      Top             =   9180
      Visible         =   0   'False
      Width           =   240
   End
   Begin VB.Image imgLookUpA 
      Height          =   240
      Left            =   11850
      Picture         =   "ConnexChart.frx":7D26
      Top             =   9180
      Visible         =   0   'False
      Width           =   240
   End
   Begin VB.Image imgCurPosDn 
      Height          =   240
      Left            =   12480
      Picture         =   "ConnexChart.frx":82B0
      Top             =   8850
      Visible         =   0   'False
      Width           =   240
   End
   Begin VB.Image imgCurPosUp 
      Height          =   240
      Left            =   12750
      Picture         =   "ConnexChart.frx":883A
      Top             =   8850
      Visible         =   0   'False
      Width           =   240
   End
   Begin VB.Image picSmallIcon 
      Height          =   240
      Index           =   6
      Left            =   13830
      Picture         =   "ConnexChart.frx":8DC4
      Top             =   8010
      Width           =   240
   End
   Begin VB.Image picSmallIcon 
      Height          =   240
      Index           =   5
      Left            =   13830
      Picture         =   "ConnexChart.frx":8F0E
      Top             =   7830
      Width           =   240
   End
   Begin VB.Image picSmallIcon 
      Height          =   240
      Index           =   4
      Left            =   13830
      Picture         =   "ConnexChart.frx":9058
      Top             =   7650
      Width           =   240
   End
   Begin VB.Image picSmallIcon 
      Height          =   240
      Index           =   3
      Left            =   13830
      Picture         =   "ConnexChart.frx":91A2
      Top             =   7470
      Width           =   240
   End
   Begin VB.Image picSmallIcon 
      Height          =   240
      Index           =   2
      Left            =   13830
      Picture         =   "ConnexChart.frx":92EC
      Top             =   7290
      Width           =   240
   End
   Begin VB.Image picSmallIcon 
      Height          =   240
      Index           =   1
      Left            =   13830
      Picture         =   "ConnexChart.frx":9436
      Top             =   7110
      Width           =   240
   End
   Begin VB.Image picSmallIcon 
      Height          =   240
      Index           =   0
      Left            =   13830
      Picture         =   "ConnexChart.frx":9580
      Top             =   6930
      Width           =   240
   End
   Begin VB.Image picInfoPics 
      Height          =   240
      Index           =   1
      Left            =   12150
      Picture         =   "ConnexChart.frx":96CA
      Top             =   8850
      Width           =   240
   End
   Begin VB.Image picInfoPics 
      Height          =   240
      Index           =   0
      Left            =   11850
      Picture         =   "ConnexChart.frx":9814
      Top             =   8850
      Width           =   240
   End
   Begin VB.Image picMarker 
      Appearance      =   0  'Flat
      BorderStyle     =   1  'Fixed Single
      Height          =   270
      Index           =   0
      Left            =   11850
      Picture         =   "ConnexChart.frx":995E
      Stretch         =   -1  'True
      Top             =   8010
      Width           =   270
   End
   Begin VB.Image picMarker 
      Appearance      =   0  'Flat
      BorderStyle     =   1  'Fixed Single
      Height          =   270
      Index           =   1
      Left            =   12180
      Picture         =   "ConnexChart.frx":9AA8
      Stretch         =   -1  'True
      Top             =   8010
      Width           =   270
   End
   Begin VB.Image picMarker 
      Appearance      =   0  'Flat
      BorderStyle     =   1  'Fixed Single
      Height          =   270
      Index           =   2
      Left            =   12510
      Picture         =   "ConnexChart.frx":9E32
      Stretch         =   -1  'True
      Top             =   8010
      Width           =   270
   End
   Begin VB.Image picComPics 
      Appearance      =   0  'Flat
      BorderStyle     =   1  'Fixed Single
      Height          =   270
      Index           =   0
      Left            =   11850
      Picture         =   "ConnexChart.frx":A274
      Stretch         =   -1  'True
      Top             =   8340
      Width           =   270
   End
   Begin VB.Image picComPics 
      Appearance      =   0  'Flat
      BorderStyle     =   1  'Fixed Single
      Height          =   270
      Index           =   1
      Left            =   12150
      Picture         =   "ConnexChart.frx":A5FE
      Stretch         =   -1  'True
      Top             =   8340
      Width           =   270
   End
   Begin VB.Image picComPics 
      Appearance      =   0  'Flat
      BorderStyle     =   1  'Fixed Single
      Height          =   270
      Index           =   2
      Left            =   12450
      Picture         =   "ConnexChart.frx":AA40
      Stretch         =   -1  'True
      Top             =   8340
      Width           =   270
   End
   Begin VB.Image picComPics 
      Appearance      =   0  'Flat
      BorderStyle     =   1  'Fixed Single
      Height          =   270
      Index           =   3
      Left            =   12750
      Picture         =   "ConnexChart.frx":AE82
      Stretch         =   -1  'True
      Top             =   8340
      Width           =   270
   End
End
Attribute VB_Name = "ConnexChart"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim MainAreaColor As Integer
Dim DataAreaColor As Integer
Dim WorkAreaColor As Integer
Dim ChartBarBackColor As Long
Dim MinimumTop As Long
Dim MinimumLeft As Long
Dim MinuteWidth As Long
Dim MaxChartHeight As Long
Dim MaxChartWidth As Long
Dim MaxBarCnt As Integer
Dim CurBarMax As Integer
Dim CurChartHeight As Long
Dim CurChartWidth As Long
Dim CurScrollHeight As Long
Dim CurScrollWidth As Long
Dim CurBarIdx As Integer
Dim PrvBarIdx As Integer
Dim DataWindowBeginUtc As String
Dim DataWindowBeginLoc As String
Dim ServerUtcTimeStr As String
Dim ServerLocTimeStr As String
Dim ChartName As String
Dim LastLocTimeCheck
Dim LastUtcTimeCheck
Dim ServerUtcTimeVal
Dim TimeScaleUtcBegin
Dim CurUtcTimeDiff As Integer
Dim CnxLayoutType As Integer
Dim CurLayoutType As Integer
Dim MinCnxLayoutWidth As Long
Dim MinCnxConnexWidth As Long
Dim CurCnxLayoutHours As Long
Dim ArrCnxIdx As Integer
Dim DepCnxIdx As Integer
Dim CnxFilterAdid As Integer
Dim CnxFilterType As Integer
Dim CnxFilterClass As Integer
Dim CnxFilterShort As Integer
Dim CnxFilterCritical As Integer
Dim ChartIsReadyForUse As Boolean
Dim CnxRangeMin As Long
Dim DontRefreshCnx As Boolean
Dim CurCnxView As Integer
Dim CnxMouseObj As String

Public Sub ToggleFlightMarker(FltAdid As String, FltBar As Integer, SetMarker As Boolean)
    Dim PicIdx As Integer
    If FltAdid = "A" Then
        If SetMarker = True Then PicIdx = 1 Else PicIdx = 0
        picArrStatus(FltBar).Picture = picMarker(PicIdx).Picture
        picArrStatus(FltBar).Tag = CStr(PicIdx)
    End If
    If FltAdid = "D" Then
        If SetMarker = True Then PicIdx = 1 Else PicIdx = 0
        picDepStatus(FltBar).Picture = picMarker(PicIdx).Picture
        picDepStatus(FltBar).Tag = CStr(PicIdx)
    End If
End Sub
Public Sub ScrollSyncExtern(RotLine As Long)
    Dim tmpGidx As String
    Dim tmpBest As String
    Dim tmpBestPos As Long
    Dim NewScroll As Integer
    Dim tmpScrVal As Integer
    Dim PosIsValid As Boolean
    Dim CedaTime
    If RotLine >= 0 Then
        tmpGidx = TabRotFlightsTab.GetFieldValue(RotLine, "CONX")
        If tmpGidx <> "------" Then
            tmpScrVal = Val(tmpGidx)
            VScroll2(0).Value = tmpScrVal
            tmpBest = TabRotFlightsTab.GetFieldValue(RotLine, "BEST")
            PosIsValid = GetTimeScalePos(tmpBest, CedaTime, tmpBestPos)
            tmpBestPos = tmpBestPos \ MinuteWidth
            If tmpBestPos > 3600 Then tmpBestPos = 3600
            If tmpBestPos < 0 Then tmpBestPos = 0
            tmpBestPos = tmpBestPos - (TimePanel(0).Width \ MinuteWidth \ 2)
            If tmpBestPos < 0 Then tmpBestPos = 0
            NewScroll = CInt(tmpBestPos)
            HScroll2(0).Value = NewScroll
            HighlightCurrentBar tmpScrVal, "CNX"
            If SyncOriginator = "CNX" Then SyncOriginator = ""
        End If
    End If
End Sub

Private Sub ArrCnxTot1_Click(Index As Integer)
    If ArrCnxTot1(Index).MousePointer = 99 Then
        DontRefreshCnx = True
        chkCnxClass(0).Value = 1
        CnxFilterClass = 1
        ShowArrConnexDetails Index
        DontRefreshCnx = False
    End If
End Sub

Private Sub ArrCnxTot2_Click(Index As Integer)
    If ArrCnxTot2(Index).MousePointer = 99 Then
        DontRefreshCnx = True
        chkCnxClass(1).Value = 1
        CnxFilterClass = 2
        ShowArrConnexDetails Index
        DontRefreshCnx = False
    End If
End Sub

Private Sub ArrCnxTot3_Click(Index As Integer)
    If ArrCnxTot3(Index).MousePointer = 99 Then
        DontRefreshCnx = True
        chkCnxClass(2).Value = 1
        CnxFilterClass = 3
        ShowArrConnexDetails Index
        DontRefreshCnx = False
    End If
End Sub

Private Sub ArrCnxTot4_Click(Index As Integer)
    If ArrCnxTot4(Index).MousePointer = 99 Then
        DontRefreshCnx = True
        chkCnxClass(3).Value = 1
        CnxFilterClass = 4
        ShowArrConnexDetails Index
        DontRefreshCnx = False
    End If
End Sub

Private Sub ArrFlight_Click(Index As Integer)
    HighlightCurrentBar Index, ChartName
End Sub

Private Sub ChartLine1_Click(Index As Integer)
    HighlightCurrentBar Index, ChartName
    LeftScale(0).ZOrder
End Sub

Private Sub ChartLine2_Click(Index As Integer)
    HighlightCurrentBar Index, ChartName
End Sub
Private Sub HighlightCurrentBar(Index As Integer, KeyOrig As String)
    Dim LineNo As Long
    Dim CurGocx As String
    Dim iCol As Integer
    If Index <> CurBarIdx Then
        If SyncOriginator = "" Then SyncOriginator = KeyOrig
        CurBarIdx = Index
        If (PrvBarIdx <> CurBarIdx) And (PrvBarIdx >= 0) Then
            ChartLine1(PrvBarIdx).BackColor = lblBarLineColor(PrvBarIdx).BackColor
            ChartLine1(PrvBarIdx).Refresh
            ChartLine3(PrvBarIdx).BackColor = lblBarLineColor(PrvBarIdx).BackColor
            ChartLine3(PrvBarIdx).Refresh
            ChartLine2(PrvBarIdx).BackColor = lblBarLineColor(PrvBarIdx).BackColor
            ChartLine2(PrvBarIdx).Refresh
        End If
        iCol = Val(lblBarLineColor(CurBarIdx).Tag)
        DrawBackGround ChartLine2(CurBarIdx), iCol, True, True
        ChartLine2(CurBarIdx).Refresh
        If ArrFlight(CurBarIdx).Visible = True Then
            DrawBackGround ChartLine1(CurBarIdx), iCol, True, True
            ChartLine1(CurBarIdx).Refresh
        End If
        If DepFlight(CurBarIdx).Visible = True Then
            DrawBackGround ChartLine3(CurBarIdx), iCol, True, True
            ChartLine3(CurBarIdx).Refresh
        End If
        If SyncOriginator = ChartName Then
            CurGocx = Right("000000" & CStr(Index), 6)
            MarkLookupLines TabRotFlightsTab, "CONX", CurGocx, True, "DRGN,AFLT,DFLT", True, "'S1'", "DecoMarkerLB"
            LineNo = TabRotFlightsTab.GetCurrentSelected
            MainDialog.RotationData_RowSelectionChanged LineNo, True
        End If
        PrvBarIdx = Index
        If SyncOriginator = KeyOrig Then SyncOriginator = ""
    End If
End Sub

Private Sub ChartLine3_Click(Index As Integer)
    HighlightCurrentBar Index, ChartName
    RightScale(0).ZOrder
End Sub

Private Sub chkAppl_Click(Index As Integer)
    If chkAppl(Index).Value = 1 Then
        chkAppl(Index).BackColor = LightGreen
        Select Case Index
            Case 0
                Me.Hide
                MainDialog.PushWorkButton "CONX_CHART", False
                chkAppl(Index).Value = 0
            Case Else
        End Select
    Else
        chkAppl(Index).BackColor = vbButtonFace
    End If
End Sub

Private Sub chkCnxAdid_Click(Index As Integer)
    If chkCnxAdid(Index).Value = 1 Then
        chkCnxAdid(Index).BackColor = LightGreen
    Else
        chkCnxAdid(Index).BackColor = vbButtonFace
    End If
End Sub

Private Sub chkCnxClass_Click(Index As Integer)
    Dim tmpTag As String
    Dim i As Integer
    Dim CurArrCnxIdx As Integer
    If chkCnxClass(Index).Value = 1 Then
        chkCnxClass(Index).BackColor = LightGreen
        tmpTag = chkCnxClass(0).Tag
        chkCnxClass(0).Tag = CStr(Index)
        CnxFilterClass = Index + 1
        CurArrCnxIdx = ArrCnxIdx
        If tmpTag <> "" Then
            i = Val(tmpTag)
            chkCnxClass(i).Value = 0
        End If
        If DontRefreshCnx = False Then
            If CurArrCnxIdx >= 0 Then
                ShowArrConnexDetails CurArrCnxIdx
            Else
                ClearConnexDetails
                ShowAllArrPaxConnex
            End If
        End If
    Else
        chkCnxClass(Index).BackColor = vbButtonFace
        tmpTag = chkCnxClass(0).Tag
        If tmpTag <> "" Then
            i = Val(tmpTag)
            If Index = i Then
                chkCnxClass(0).Tag = ""
                CnxFilterClass = 0
                If DontRefreshCnx = False Then
                    CurArrCnxIdx = ArrCnxIdx
                    If CurArrCnxIdx >= 0 Then
                        ShowArrConnexDetails CurArrCnxIdx
                    Else
                        ClearConnexDetails
                        ShowAllArrPaxConnex
                    End If
                End If
            End If
        End If
    End If
End Sub

Private Sub chkCnxCrit_Click(Index As Integer)
    Dim tmpTag As String
    Dim i As Integer
    Dim CurArrCnxIdx As Integer
    If chkCnxCrit(Index).Value = 1 Then
        chkCnxCrit(Index).BackColor = LightGreen
        tmpTag = chkCnxCrit(0).Tag
        chkCnxCrit(0).Tag = CStr(Index)
        CnxFilterCritical = Index + 1
        CurArrCnxIdx = ArrCnxIdx
        If tmpTag <> "" Then
            i = Val(tmpTag)
            chkCnxCrit(i).Value = 0
        End If
        If DontRefreshCnx = False Then
            If CurArrCnxIdx >= 0 Then
                ShowArrConnexDetails CurArrCnxIdx
            Else
                ClearConnexDetails
                ShowAllArrPaxConnex
            End If
        End If
    Else
        chkCnxCrit(Index).BackColor = vbButtonFace
        tmpTag = chkCnxCrit(0).Tag
        If tmpTag <> "" Then
            i = Val(tmpTag)
            If Index = i Then
                chkCnxCrit(0).Tag = ""
                CnxFilterCritical = 0
                If DontRefreshCnx = False Then
                    CurArrCnxIdx = ArrCnxIdx
                    If CurArrCnxIdx >= 0 Then
                        ShowArrConnexDetails CurArrCnxIdx
                    Else
                        ClearConnexDetails
                        ShowAllArrPaxConnex
                    End If
                End If
            End If
        End If
    End If
End Sub

Private Sub chkCnxShort_Click(Index As Integer)
    Dim tmpTag As String
    Dim i As Integer
    Dim CurArrCnxIdx As Integer
    If chkCnxShort(Index).Value = 1 Then
        chkCnxShort(Index).BackColor = LightGreen
        tmpTag = chkCnxShort(0).Tag
        chkCnxShort(0).Tag = CStr(Index)
        CnxFilterShort = Index + 1
        CurArrCnxIdx = ArrCnxIdx
        If tmpTag <> "" Then
            i = Val(tmpTag)
            chkCnxShort(i).Value = 0
        End If
        If DontRefreshCnx = False Then
            If CurArrCnxIdx >= 0 Then
                ShowArrConnexDetails CurArrCnxIdx
            Else
                ClearConnexDetails
                ShowAllArrPaxConnex
            End If
        End If
    Else
        chkCnxShort(Index).BackColor = vbButtonFace
        tmpTag = chkCnxShort(0).Tag
        If tmpTag <> "" Then
            i = Val(tmpTag)
            If Index = i Then
                chkCnxShort(0).Tag = ""
                CnxFilterShort = 0
                If DontRefreshCnx = False Then
                    CurArrCnxIdx = ArrCnxIdx
                    If CurArrCnxIdx >= 0 Then
                        ShowArrConnexDetails CurArrCnxIdx
                    Else
                        ClearConnexDetails
                        ShowAllArrPaxConnex
                    End If
                End If
            End If
        End If
    End If
End Sub

Private Sub chkCnxTest_Click(Index As Integer)
    If chkCnxTest(Index).Value = 1 Then
        chkCnxTest(Index).BackColor = LightGreen
        Select Case Index
            Case 0
                Check1.Visible = True
                Check2.Visible = True
            Case 1
                Frame1.Visible = True
            Case Else
        End Select
    Else
        Select Case Index
            Case 0
                Check1.Visible = False
                Check2.Visible = False
            Case 1
                Frame1.Visible = False
            Case Else
        End Select
        chkCnxTest(Index).BackColor = vbButtonFace
    End If
End Sub

Private Sub chkCnxType_Click(Index As Integer)
    Dim tmpTag As String
    Dim i As Integer
    Dim CurArrCnxIdx As Integer
    If chkCnxType(Index).Value = 1 Then
        chkCnxType(Index).BackColor = LightGreen
        tmpTag = chkCnxType(0).Tag
        chkCnxType(0).Tag = CStr(Index)
        CnxFilterType = Index + 1
        CurArrCnxIdx = ArrCnxIdx
        If tmpTag <> "" Then
            i = Val(tmpTag)
            chkCnxType(i).Value = 0
        End If
        If DontRefreshCnx = False Then
            If CurArrCnxIdx >= 0 Then
                ShowArrConnexDetails CurArrCnxIdx
            Else
                ClearConnexDetails
                ShowAllArrPaxConnex
            End If
        End If
    Else
        chkCnxType(Index).BackColor = vbButtonFace
        tmpTag = chkCnxType(0).Tag
        If tmpTag <> "" Then
            i = Val(tmpTag)
            If Index = i Then
                chkCnxType(0).Tag = ""
                CnxFilterType = 0
                If DontRefreshCnx = False Then
                    CurArrCnxIdx = ArrCnxIdx
                    If CurArrCnxIdx >= 0 Then
                        ShowArrConnexDetails CurArrCnxIdx
                    Else
                        ClearConnexDetails
                        ShowAllArrPaxConnex
                    End If
                End If
            End If
        End If
    End If
End Sub

Private Sub chkLayout_Click(Index As Integer)
    Dim tmpTag As String
    Dim i As Integer
    Dim CurArrCnxIdx As Integer
    If chkLayout(Index).Value = 1 Then
        chkLayout(Index).BackColor = LightGreen
        tmpTag = chkLayout(0).Tag
        chkLayout(0).Tag = CStr(Index)
        CurArrCnxIdx = ArrCnxIdx
        If tmpTag <> "" Then
            i = Val(tmpTag)
            If i <> Index Then chkLayout(i).Value = 0
        End If
        CnxLayoutType = Index
        If CurArrCnxIdx >= 0 Then
            If DontRefreshCnx = False Then ShowArrConnexDetails CurArrCnxIdx
        End If
        If CnxLayoutType > 1 Then CurLayoutType = CnxLayoutType
        If CurLayoutType < 1 Then CurLayoutType = 4
    Else
        chkLayout(Index).BackColor = vbButtonFace
        tmpTag = chkLayout(0).Tag
        If tmpTag <> "" Then
            i = Val(tmpTag)
            If Index = i Then
                chkLayout(Index).Value = 1
                chkLayout(Index).BackColor = LightGreen
            End If
        End If
    End If
End Sub

Private Sub chkScroll1_Click(Index As Integer)
    If chkScroll1(Index).Value = 1 Then
        Select Case Index
            Case 0
                If VertScroll1(0).Visible = True Then
                    VertScroll1(0).Visible = False
                    TimeScroll1(0).Visible = False
                Else
                    VertScroll1(0).Visible = True
                    TimeScroll1(0).Visible = True
                End If
                chkScroll1(Index).Value = 0
                Form_Resize
            Case Else
                chkScroll1(Index).Value = 0
        End Select
    Else
    End If
End Sub
Private Sub chkScroll2_Click(Index As Integer)
    If chkScroll2(Index).Value = 1 Then
        Select Case Index
            Case 0
                If VertScroll2(0).Visible = True Then
                    VertScroll2(0).Visible = False
                    TimeScroll2(0).Visible = False
                Else
                    VertScroll2(0).Visible = True
                    TimeScroll2(0).Visible = True
                End If
                chkScroll2(Index).Value = 0
                Form_Resize
            Case Else
                chkScroll2(Index).Value = 0
        End Select
    Else
    End If
End Sub

Private Sub chkSelDeco_Click(Index As Integer)
    If chkSelDeco(Index).Value = 1 Then
        chkSelDeco(Index).BackColor = LightGreen
        Select Case Index
            Case 0
                ToggleLeftScaleBars True
            Case 1
                ToggleRightScaleBars True
            Case 2
                ToggleMidCnxPanel True
                chkSelDeco(0).Value = 1
                chkSelDeco(1).Value = 1
                chkSelDeco(3).Value = 1
                chkSelDeco(4).Value = 1
            Case 3
                ToggleLeftCnxPanel True
            Case 4
                ToggleRightCnxPanel True
            Case Else
        End Select
    Else
        Select Case Index
            Case 0
                ToggleLeftScaleBars False
            Case 1
                ToggleRightScaleBars False
            Case 2
                ToggleMidCnxPanel False
                chkSelDeco(0).Value = 0
                chkSelDeco(1).Value = 0
                chkSelDeco(3).Value = 0
                chkSelDeco(4).Value = 0
            Case 3
                ToggleLeftCnxPanel False
            Case 4
                ToggleRightCnxPanel False
            Case Else
        End Select
        chkSelDeco(Index).BackColor = vbButtonFace
    End If
End Sub
Private Sub ToggleLeftCnxPanel(ShowPanel As Boolean)
    Dim i As Integer
    Dim NewLeft As Long
    Screen.MousePointer = 11
    For i = 0 To ArrCnxInfo.UBound
        If ShowPanel = True Then
            'Set ArrCnxInfo(i).Container = MidScConx(0)
            'ArrCnxInfo(i).Top = ChartLine1(i).Top + 15
            'ArrCnxInfo(i).Left = 150
            'Set ArrCnxInfo(i).Container = MidScConx(0)
        Else
            'Set ArrCnxInfo(i).Container = ChartLine2(i)
            'ArrCnxInfo(i).Top = -15
            'ArrCnxInfo(i).Left = lblArrBar(i).Left
        End If
    Next
    Screen.MousePointer = 0
End Sub

Private Sub ToggleMidCnxPanel(ShowPanel As Boolean)
    'RightScCon1(0).Left = RightScale(0).Left
    'RightScCon1(0).Width = RightScale(0).Width
    'fraTabCaption(0).Width = MidScCon1(0).Width
    'lblTabCaption(0).Width = fraTabCaption(0).Width - 240
    'lblTabCaptionShadow(0).Width = lblTabCaption(0).Width
    LeftScCon1(0).Visible = ShowPanel
    MidScConx(0).Visible = ShowPanel
    MidScCon1(0).Visible = ShowPanel
    RightScCon1(0).Visible = ShowPanel
End Sub

Private Sub ToggleRightCnxPanel(ShowPanel As Boolean)
    Dim i As Integer
    Dim NewLeft As Long
    Screen.MousePointer = 11
    NewLeft = RightTime(0).Left + 150
    For i = 0 To DepCnxInfo.UBound
        If ShowPanel = True Then
            'Set DepCnxInfo(i).Container = MidScConx(0)
            'DepCnxInfo(i).Top = ChartLine3(i).Top + 15
            'DepCnxInfo(i).Left = NewLeft
        Else
            'Set DepCnxInfo(i).Container = ChartLine2(i)
            'DepCnxInfo(i).Top = -15
            'NewLeft = lblDepBar(i).Left + lblDepBar(i).Width - DepCnxInfo(i).Width
            'DepCnxInfo(i).Left = NewLeft
        End If
    Next
    Screen.MousePointer = 0
End Sub
Private Sub ToggleLeftScaleBars(ShowLeftScale As Boolean)
    Dim i As Integer
    Dim NewLeft As Long
    Screen.MousePointer = 11
    For i = 0 To ArrFlight.UBound
        ArrFlight(i).Visible = False
        If ShowLeftScale = True Then
            Set ArrFlight(i).Container = ChartLine1(i)
            NewLeft = 30
            ArrFlight(i).Left = NewLeft
            'NewLeft = NewLeft + ArrFlight(i).Width + 30
            If lblArrBar(i).Tag = "Y" Then
                ArrFlight(i).Visible = True
            End If
        Else
            Set ArrFlight(i).Container = ChartLine2(i)
            NewLeft = lblArrBar(i).Left
            NewLeft = NewLeft - ArrFlight(i).Width - 30
            ArrFlight(i).Left = NewLeft
            If lblArrBar(i).Tag = "Y" Then
                ArrFlight(i).Visible = True
                'lblArrBar(i).Visible = True
            End If
        End If
    Next
    Screen.MousePointer = 0
    LeftScale(0).Visible = ShowLeftScale
End Sub
Private Sub ToggleRightScaleBars(ShowRightScale As Boolean)
    Dim i As Integer
    Dim NewLeft As Long
    Screen.MousePointer = 11
    For i = 0 To DepFlight.UBound
        DepFlight(i).Visible = False
        'DepLblPanel(i).Visible = False
        If ShowRightScale = True Then
            Set DepFlight(i).Container = ChartLine3(i)
            NewLeft = 30
            DepFlight(i).Left = NewLeft
            'NewLeft = NewLeft + DepFlight(i).Width + 30
            If lblDepBar(i).Tag = "Y" Then
                DepFlight(i).Visible = True
            End If
        Else
            Set DepFlight(i).Container = ChartLine2(i)
            NewLeft = lblDepBar(i).Left + lblDepBar(i).Width
            NewLeft = NewLeft + 30
            DepFlight(i).Left = NewLeft
            If lblDepBar(i).Tag = "Y" Then
                DepFlight(i).Visible = True
                'lblDepBar(i).Visible = True
            End If
        End If
    Next
    Screen.MousePointer = 0
    RightScale(0).Visible = ShowRightScale
End Sub

Private Sub chkTime1_Click(Index As Integer)
    If chkTime1(Index).Value = 1 Then
        chkTime1(Index).BackColor = LightGreen
        chkTime1(0).Value = 1
        chkTime1(1).Value = 1
    Else
        chkTime1(0).Value = 0
        chkTime1(1).Value = 0
        chkTime1(Index).BackColor = vbButtonFace
    End If
End Sub

Private Sub chkUtc_Click(Index As Integer)
    If chkUtc(Index).Value = 1 Then
        chkUtc(Index).BackColor = LightGreen
        If Index = 0 Then chkUtc(1).Value = 0 Else chkUtc(0).Value = 0
        CreateTimeScaleArea TimeScale(0)
        TimeScaleTimer_Timer
        If Index = 1 Then
            ToggleFlightBarTimes UtcTimeDiff
            CurUtcTimeDiff = UtcTimeDiff
        Else
            ToggleFlightBarTimes 0
            CurUtcTimeDiff = 0
        End If
        SetCnxArrTimeScale ArrCnxIdx
    Else
        If Index = 0 Then chkUtc(1).Value = 1 Else chkUtc(0).Value = 1
        chkUtc(Index).BackColor = vbButtonFace
    End If
End Sub

Private Sub CreateTimeScaleArea(TimeScale As PictureBox)
    Dim iMin As Integer
    Dim iMin5 As Integer
    Dim iMin10 As Integer
    Dim iMin30 As Integer
    Dim iMin60 As Integer
    Dim iHour As Integer
    Dim iBar As Integer
    Dim lWidth As Long
    Dim LeftPos As Long
    Dim LeftOff1 As Long
    Dim LeftOff2 As Long
    Dim TopPos As Long
    Dim DisplayDateTime As String
    Dim CurDate As String
    Dim CurSsimDate As String
    Dim CurUtcNowTime
    Dim CurLocNowTime
    
    If DataWindowBeginUtc = "" Then
        LastLocTimeCheck = Now
        'Create a Default UTC Time
        CurUtcNowTime = DateAdd("n", -UtcTimeDiff, LastLocTimeCheck)
        LastUtcTimeCheck = CurUtcNowTime
        'Create a Default Data Window Begin
        CurUtcNowTime = DateAdd("h", -2, CurUtcNowTime)
        DataWindowBeginUtc = Format(CurUtcNowTime, "YYYYMMDDhhmmss")
    End If
    
    If ServerUtcTimeStr = "" Then
        'Create a Default Server Reference Time
        ServerUtcTimeVal = LastUtcTimeCheck
        ServerUtcTimeStr = Format(ServerUtcTimeVal, "YYYYMMDDhhmmss")
    End If
    
    If DataFilterVpfr <> "" Then DataWindowBeginUtc = DataFilterVpfr
    'We always start at full hours in Utc
    Mid(DataWindowBeginUtc, 11) = "0000"
    lblBgnTime(0).Tag = DataWindowBeginUtc
    
    CurUtcNowTime = CedaFullDateToVb(DataWindowBeginUtc)
    TimeScaleUtcBegin = CurUtcNowTime
    CurLocNowTime = DateAdd("n", UtcTimeDiff, CurUtcNowTime)
    DataWindowBeginLoc = Format(CurLocNowTime, "YYYYMMDDhhmmss")
    
    If chkUtc(0).Value = 1 Then
        DisplayDateTime = DataWindowBeginUtc
    Else
        DisplayDateTime = DataWindowBeginLoc
    End If
    
    lblBgnTime(0).Caption = DisplayDateTime
    
    iHour = Val(Mid(DisplayDateTime, 9, 2)) - 1
    CurDate = Left(DisplayDateTime, 8)
    CurSsimDate = DecodeSsimDayFormat(CurDate, "CEDA", "SSIM2")
    lblBgnTime(0).Caption = CurSsimDate
    
    lblMin1(0).Width = 15
    lWidth = 60
    MinuteWidth = lWidth
    LeftOff1 = (lblHour(0).Width / 2) - 15
    LeftOff2 = (lblDate(0).Width / 2) - 15
    iMin5 = -1
    iMin10 = -1
    iMin30 = -1
    iMin60 = -1
    LeftPos = 1200
    For iMin = 0 To 3600
        If iMin > lblMin1.UBound Then
            Load lblMin1(iMin)
            Set lblMin1(iMin).Container = TimeScale
        End If
        lblMin1(iMin).Left = LeftPos + 15
        lblMin1(iMin).Visible = True
        If iMin Mod 5 = 0 Then
            iMin5 = iMin5 + 1
            If iMin5 > lblMin5.UBound Then
                Load lblMin5(iMin5)
                Set lblMin5(iMin5).Container = TimeScale
            End If
            lblMin5(iMin5).Left = LeftPos
            lblMin5(iMin5).Visible = True
        End If
        If iMin Mod 10 = 0 Then
            iMin10 = iMin10 + 1
            If iMin10 > lblMin10.UBound Then
                Load lblMin10(iMin10)
                Set lblMin10(iMin10).Container = TimeScale
            End If
            lblMin10(iMin10).Left = LeftPos
            lblMin10(iMin10).Visible = True
        End If
        If iMin Mod 30 = 0 Then
            iMin30 = iMin30 + 1
            If iMin30 > lblMin30.UBound Then
                Load lblMin30(iMin30)
                Set lblMin30(iMin30).Container = TimeScale
            End If
            lblMin30(iMin30).Left = LeftPos
            lblMin30(iMin30).Visible = True
        End If
        If iMin Mod 60 = 0 Then
            iMin60 = iMin60 + 1
            If iMin60 > lblMin60.UBound Then
                Load lblMin60(iMin60)
                Set lblMin60(iMin60).Container = TimeScale
                Load lblHour(iMin60)
                Set lblHour(iMin60).Container = TimeScale
                Load lblDate(iMin60)
                Set lblDate(iMin60).Container = TimeScale
            End If
            lblMin60(iMin60).Left = LeftPos
            lblMin60(iMin60).Visible = True
            lblHour(iMin60).Left = LeftPos - LeftOff1
            iHour = iHour + 1
            If iHour > 23 Then
                iHour = 0
                CurDate = CedaDateAdd(CurDate, 1)
                CurSsimDate = DecodeSsimDayFormat(CurDate, "CEDA", "SSIM2")
            End If
            lblHour(iMin60).Caption = Right("00" & CStr(iHour), 2) & ":00"
            lblHour(iMin60).ToolTipText = CurSsimDate
            lblHour(iMin60).Tag = CurDate
            lblHour(iMin60).Visible = True
            lblDate(iMin60).Caption = CurSsimDate
            lblDate(iMin60).Tag = CurDate
            lblDate(iMin60).Left = LeftPos - LeftOff2
            lblDate(iMin60).Visible = True
        End If
        LeftPos = LeftPos + lWidth
    Next
    CurChartWidth = LeftPos + 1200
    TimeScaleTimer.Enabled = True
End Sub

Public Sub CreateChartBarLayout(NeededBars As Integer)
    Dim iBar As Integer
    Dim iMin As Integer
    Dim iMax As Integer
    Dim iUse As Integer
    Dim lWidth As Long
    Dim LeftPos As Long
    Dim LeftOff As Long
    Dim TopPos As Long
    Dim LeftLblPos As Long
    Me.MousePointer = 11
    WorkArea(0).Visible = False
    iMin = ChartLine2.UBound
    If iMin > 0 Then
        TopPos = ChartLine2(iMin).Top + ChartLine2(0).Height + 60
        iMin = iMin + 1
    Else
        TopPos = ChartLine2(iMin).Top
    End If
    iMax = NeededBars - 1
    If iMax > MaxBarCnt Then iMax = MaxBarCnt
    If (iMax >= CurBarMax) Then
        For iBar = iMin To iMax
            If iBar > ChartLine2.UBound Then
                Load ChartLine1(iBar)
                Load ChartLine2(iBar)
                Load ChartLine3(iBar)
                Load lblArrBar(iBar)
                Load lblArrJob1(iBar)
                Load lblArrJob2(iBar)
                Load picArr1(iBar)
                Load picArr2(iBar)
                Load picArr3(iBar)
                Load picArr4(iBar)
                Load lblDepBar(iBar)
                Load lblDepJob1(iBar)
                Load lblDepJob2(iBar)
                Load picDep1(iBar)
                Load picDep2(iBar)
                Load picDep3(iBar)
                Load picDep4(iBar)
                Load lblTowBar(iBar)
                Load lblBarLineColor(iBar)
                Set lblArrBar(iBar).Container = ChartLine2(iBar)
                Set lblArrJob1(iBar).Container = ChartLine2(iBar)
                Set lblArrJob2(iBar).Container = ChartLine2(iBar)
                Set picArr1(iBar).Container = ChartLine2(iBar)
                Set picArr2(iBar).Container = ChartLine2(iBar)
                Set picArr3(iBar).Container = ChartLine2(iBar)
                Set picArr4(iBar).Container = ChartLine2(iBar)
                Set lblDepBar(iBar).Container = ChartLine2(iBar)
                Set lblDepJob1(iBar).Container = ChartLine2(iBar)
                Set lblDepJob2(iBar).Container = ChartLine2(iBar)
                Set picDep1(iBar).Container = ChartLine2(iBar)
                Set picDep2(iBar).Container = ChartLine2(iBar)
                Set picDep3(iBar).Container = ChartLine2(iBar)
                Set picDep4(iBar).Container = ChartLine2(iBar)
                Set lblTowBar(iBar).Container = ChartLine2(iBar)
                Set lblBarLineColor(iBar).Container = ChartLine2(iBar)
                
                Load ArrCnxInfo(iBar)
                Load ArrCnxTot1(iBar)
                Load ArrCnxTot2(iBar)
                Load ArrCnxTot3(iBar)
                Load ArrCnxTot4(iBar)
                Set ArrCnxTot1(iBar).Container = ArrCnxInfo(iBar)
                Set ArrCnxTot2(iBar).Container = ArrCnxInfo(iBar)
                Set ArrCnxTot3(iBar).Container = ArrCnxInfo(iBar)
                Set ArrCnxTot4(iBar).Container = ArrCnxInfo(iBar)
                
                Load lblCnxPaxBar(iBar)
                Load lblCnxBagBar(iBar)
                Load lblCnxUldBar(iBar)
                Load picArrCnx(iBar)
                Load picDepCnx(iBar)
                Load picBarCnx(iBar)
                
                Load DepCnxInfo(iBar)
                Load DepCnxTot1(iBar)
                Load DepCnxTot2(iBar)
                Load DepCnxTot3(iBar)
                Load DepCnxTot4(iBar)
                Load DepCnxCsct(iBar)
                Set DepCnxTot1(iBar).Container = DepCnxInfo(iBar)
                Set DepCnxTot2(iBar).Container = DepCnxInfo(iBar)
                Set DepCnxTot3(iBar).Container = DepCnxInfo(iBar)
                Set DepCnxTot4(iBar).Container = DepCnxInfo(iBar)
                Set DepCnxCsct(iBar).Container = DepCnxInfo(iBar)
                
                'Create Object
                Load ArrLblPanel(iBar)
                Load lblArrTpax(iBar)
                Load lblArrTbag(iBar)
                Load lblArrTuld(iBar)
                Set lblArrTpax(iBar).Container = ArrLblPanel(iBar)
                Set lblArrTbag(iBar).Container = ArrLblPanel(iBar)
                Set lblArrTuld(iBar).Container = ArrLblPanel(iBar)
                
                Set DepCnxInfo(iBar).Container = MidScConx(0)
            End If
            
            'LeftScale Panel
            Set ChartLine1(iBar).Container = LeftScale(0)
            ChartLine1(iBar).Top = TopPos
            ChartLine1(iBar).Left = 30
            CreateArrFlightPanel iBar
            Set ArrFlight(iBar).Container = ChartLine1(iBar)
            ArrFlight(iBar).Left = 30
            ChartLine1(iBar).Width = ArrFlight(iBar).Left + ArrFlight(iBar).Width + 90
            
            
            '===== MidScale Panel ================
            'Panel Totals PAX/BAG/ULD Arrival
            Set ArrLblPanel(iBar).Container = MidScConx(0)
            ArrLblPanel(iBar).Top = TopPos + 60
            ArrLblPanel(iBar).Left = 0
            ArrLblPanel(iBar).Visible = False
            'Graphical AnchorPoint (Arrival)
            Set picArrCnx(iBar).Container = MidScConx(0)
            picArrCnx(iBar).Top = ArrLblPanel(iBar).Top + (ArrLblPanel(iBar).Height \ 2) - (picArrCnx(iBar).Height \ 2)
            picArrCnx(iBar).Left = 90
            'The PAX/BAG/ULD Colored Bars
            Set lblCnxPaxBar(iBar).Container = MidScConx(0)
            Set lblCnxBagBar(iBar).Container = MidScConx(0)
            Set lblCnxUldBar(iBar).Container = MidScConx(0)
            lblCnxPaxBar(iBar).Top = picArrCnx(iBar).Top - 15
            lblCnxBagBar(iBar).Top = lblCnxPaxBar(iBar).Top + lblCnxPaxBar(iBar).Height - 15
            lblCnxUldBar(iBar).Top = lblCnxBagBar(iBar).Top + lblCnxBagBar(iBar).Height - 15
            lblCnxPaxBar(iBar).Left = LeftTime(0).Left
            lblCnxBagBar(iBar).Left = LeftTime(0).Left
            lblCnxUldBar(iBar).Left = LeftTime(0).Left
            lblCnxPaxBar(iBar).ZOrder
            lblCnxBagBar(iBar).ZOrder
            lblCnxUldBar(iBar).ZOrder
            
            'Panel Totals Per Class (1-4) per Connection Arrival
            Set ArrCnxInfo(iBar).Container = MidScConx(0)
            
            
            ArrCnxInfo(iBar).Top = TopPos + 60
            ArrCnxInfo(iBar).Left = 150
            
            DepCnxInfo(iBar).Top = TopPos + 60
            
            DepCnxInfo(iBar).Left = RightTime(0).Left + 120
            
            Set picDepCnx(iBar).Container = MidScConx(0)
            picDepCnx(iBar).Top = picArrCnx(iBar).Top
            picDepCnx(iBar).Left = DepCnxInfo(iBar).Left + DepCnxInfo(iBar).Width
            
            Set picBarCnx(iBar).Container = MidScConx(0)
            picBarCnx(iBar).Top = picArrCnx(iBar).Top
            picBarCnx(iBar).Left = 100
            
            lblDepBar(iBar).ZOrder
                    
            
            
            
            ArrFlight(iBar).Visible = True
            
            
            
            'RightScale Panel
            Set ChartLine3(iBar).Container = RightScale(0)
            ChartLine3(iBar).Top = TopPos
            ChartLine3(iBar).Left = 30
            CreateDepFlightPanel iBar
            Set DepFlight(iBar).Container = ChartLine3(iBar)
            DepFlight(iBar).Left = 30
            
            'Gantt Chart Panel
            Set ChartLine2(iBar).Container = HScrollArea(0)
            ChartLine2(iBar).Top = TopPos
            
            
            
            
            
            Set DepLblPanel(iBar).Container = MidScConx(0)
            DepLblPanel(iBar).Top = ArrLblPanel(iBar).Top
            DepLblPanel(iBar).Left = RightTime(0).Left
            ChartLine3(iBar).Width = DepFlight(iBar).Left + DepFlight(iBar).Width + 90
            DepLblPanel(iBar).Visible = False
            DepLblPanel(iBar).ZOrder
            picDepCnx(iBar).ZOrder
            DepFlight(iBar).Visible = True
            
            
            
            ChartLine1(iBar).Visible = True
            ChartLine2(iBar).Visible = True
            ChartLine3(iBar).Visible = True
            
            ArrCnxTot1(iBar).Visible = True
            ArrCnxTot2(iBar).Visible = True
            ArrCnxTot3(iBar).Visible = True
            ArrCnxTot4(iBar).Visible = True
            ArrCnxInfo(iBar).Visible = False
            
            DepCnxTot1(iBar).Visible = True
            DepCnxTot2(iBar).Visible = True
            DepCnxTot3(iBar).Visible = True
            DepCnxTot4(iBar).Visible = True
            DepCnxCsct(iBar).Visible = True
            DepCnxInfo(iBar).Visible = False
            
            'lblArrFlno(iBar).Caption = CStr(iBar)
            
            TopPos = TopPos + ChartLine2(0).Height + 30
        Next
        CurBarMax = iMax
    Else
        iMin = NeededBars
        iMax = CurBarMax
        CurBarMax = iMin - 1
    End If
    TopPos = ChartLine2(CurBarMax).Top + ChartLine2(0).Height
    CurChartHeight = TopPos + 30
    VScrollArea(0).Height = CurChartHeight
    HScrollArea(0).Height = CurChartHeight
    LeftScale(0).Height = CurChartHeight
    RightScale(0).Height = CurChartHeight
    TimeLine2(0).Height = CurChartHeight
    TimeLine2(0).ZOrder
    RangeLine(0).Height = CurChartHeight
    RangeLine(0).ZOrder
    RangeLine(1).Height = CurChartHeight
    RangeLine(1).ZOrder
    LeftTime(0).Height = CurChartHeight
    RightTime(0).Height = CurChartHeight
    TopPos = VScrollArea(0).Height
    VScroll1(0).Max = CurBarMax
    VScroll2(0).Max = CurBarMax
    WorkArea(0).Visible = True
    Me.MousePointer = 0
End Sub
Private Sub CreateArrFlightPanel(iBar As Integer)
    If iBar > ArrFlight.UBound Then
        Load ArrFlight(iBar)
        'Load lblArrFtyp(iBar)
        Load lblArrFlno(iBar)
        Load lblArrFlti(iBar)
        Load lblArrOrg3(iBar)
        Load lblArrVia3(iBar)
        Load lblArrStoa(iBar)
        Load lblArrEtai(iBar)
        Load lblArrOnbl(iBar)
        Load lblArrPsta(iBar)
        'Load lblArrGta1(iBar)
        Load lblArrRegn(iBar)
        Load lblArrAct3(iBar)
        Load lblArrTot1(iBar)
        Load lblArrTot2(iBar)
        Load lblArrTot3(iBar)
        Load lblArrTot4(iBar)
        Load lblArrTotPax(iBar)
        Load lblArrTotBag(iBar)
        Load lblArrTotUld(iBar)
        Load lblArrCsct(iBar)
        Load picArrStatus(iBar)
        'Set lblArrFtyp(iBar).Container = ArrFlight(iBar)
        Set lblArrFlno(iBar).Container = ArrFlight(iBar)
        Set lblArrFlti(iBar).Container = ArrFlight(iBar)
        Set lblArrOrg3(iBar).Container = ArrFlight(iBar)
        Set lblArrVia3(iBar).Container = ArrFlight(iBar)
        Set lblArrStoa(iBar).Container = ArrFlight(iBar)
        Set lblArrEtai(iBar).Container = ArrFlight(iBar)
        Set lblArrOnbl(iBar).Container = ArrFlight(iBar)
        Set lblArrPsta(iBar).Container = ArrFlight(iBar)
        'Set lblArrGta1(iBar).Container = ArrFlight(iBar)
        Set lblArrRegn(iBar).Container = ArrFlight(iBar)
        Set lblArrAct3(iBar).Container = ArrFlight(iBar)
        Set lblArrTot1(iBar).Container = ArrFlight(iBar)
        Set lblArrTot2(iBar).Container = ArrFlight(iBar)
        Set lblArrTot3(iBar).Container = ArrFlight(iBar)
        Set lblArrTot4(iBar).Container = ArrFlight(iBar)
        Set lblArrTotPax(iBar).Container = ArrFlight(iBar)
        Set lblArrTotBag(iBar).Container = ArrFlight(iBar)
        Set lblArrTotUld(iBar).Container = ArrFlight(iBar)
        Set lblArrCsct(iBar).Container = ArrFlight(iBar)
        Set picArrStatus(iBar).Container = ArrFlight(iBar)
    End If
   
    ArrFlight(iBar).Top = 30
    'ArrFlight(iBar).Height = 330
    
    'lblArrFtyp(iBar).Visible = True
    lblArrFlno(iBar).Visible = True
    lblArrFlti(iBar).Visible = True
    lblArrOrg3(iBar).Visible = True
    lblArrVia3(iBar).Visible = True
    lblArrStoa(iBar).Visible = True
    lblArrEtai(iBar).Visible = True
    lblArrOnbl(iBar).Visible = True
    lblArrPsta(iBar).Visible = True
    'lblArrGta1(iBar).Visible = True
    lblArrRegn(iBar).Visible = True
    lblArrAct3(iBar).Visible = True
    lblArrTot1(iBar).Visible = True
    lblArrTot2(iBar).Visible = True
    lblArrTot3(iBar).Visible = True
    lblArrTot4(iBar).Visible = True
    lblArrTotPax(iBar).Visible = True
    lblArrTotBag(iBar).Visible = True
    lblArrTotUld(iBar).Visible = True
    lblArrCsct(iBar).Visible = True
    picArrStatus(iBar).Visible = True
    
    lblArrTpax(iBar).Visible = True
    lblArrTbag(iBar).Visible = True
    lblArrTuld(iBar).Visible = True
End Sub

Private Sub CreateDepFlightPanel(iBar As Integer)
    If iBar > DepFlight.UBound Then
        Load DepFlight(iBar)
        'Load picDepStat(iBar)
        'Load lblDepFtyp(iBar)
        Load lblDepFlno(iBar)
        Load lblDepFlti(iBar)
        Load lblDepDes3(iBar)
        Load lblDepVia3(iBar)
        Load lblDepStod(iBar)
        Load lblDepEtdi(iBar)
        Load lblDepOfbl(iBar)
        Load lblDepPstd(iBar)
        'Load lblDepGtd1(iBar)
        Load lblDepRegn(iBar)
        Load lblDepAct3(iBar)
        Load lblDepTot1(iBar)
        Load lblDepTot2(iBar)
        Load lblDepTot3(iBar)
        Load lblDepTot4(iBar)
        Load lblDepTotPax(iBar)
        Load lblDepTotBag(iBar)
        Load lblDepTotUld(iBar)
        Load lblDepCsct(iBar)
        Load picDepStatus(iBar)
        'Set picDepStat(iBar).Container = DepFlight(iBar)
        'Set lblDepFtyp(iBar).Container = DepFlight(iBar)
        Set lblDepFlno(iBar).Container = DepFlight(iBar)
        Set lblDepFlti(iBar).Container = DepFlight(iBar)
        Set lblDepDes3(iBar).Container = DepFlight(iBar)
        Set lblDepVia3(iBar).Container = DepFlight(iBar)
        Set lblDepStod(iBar).Container = DepFlight(iBar)
        Set lblDepEtdi(iBar).Container = DepFlight(iBar)
        Set lblDepOfbl(iBar).Container = DepFlight(iBar)
        Set lblDepPstd(iBar).Container = DepFlight(iBar)
        'Set lblDepGtd1(iBar).Container = DepFlight(iBar)
        Set lblDepRegn(iBar).Container = DepFlight(iBar)
        Set lblDepAct3(iBar).Container = DepFlight(iBar)
        Set lblDepTot1(iBar).Container = DepFlight(iBar)
        Set lblDepTot2(iBar).Container = DepFlight(iBar)
        Set lblDepTot3(iBar).Container = DepFlight(iBar)
        Set lblDepTot4(iBar).Container = DepFlight(iBar)
        Set lblDepTotPax(iBar).Container = DepFlight(iBar)
        Set lblDepTotBag(iBar).Container = DepFlight(iBar)
        Set lblDepTotUld(iBar).Container = DepFlight(iBar)
        Set lblDepCsct(iBar).Container = DepFlight(iBar)
        Set picDepStatus(iBar).Container = DepFlight(iBar)
        
        Load DepLblPanel(iBar)
        Load lblDepTpax(iBar)
        Load lblDepTbag(iBar)
        Load lblDepTuld(iBar)
    End If
    DepFlight(iBar).Top = 30
    DepLblPanel(iBar).Top = 30
    
    Set lblDepTpax(iBar).Container = DepLblPanel(iBar)
    Set lblDepTbag(iBar).Container = DepLblPanel(iBar)
    Set lblDepTuld(iBar).Container = DepLblPanel(iBar)
    
    'DepFlight(iBar).Height = 330
    'picDepStat(iBar).Visible = True
    'lblDepFtyp(iBar).Visible = True
    lblDepFlno(iBar).Visible = True
    lblDepFlti(iBar).Visible = True
    lblDepDes3(iBar).Visible = True
    lblDepVia3(iBar).Visible = True
    lblDepStod(iBar).Visible = True
    lblDepEtdi(iBar).Visible = True
    lblDepOfbl(iBar).Visible = True
    lblDepPstd(iBar).Visible = True
    'lblDepGtd1(iBar).Visible = True
    lblDepRegn(iBar).Visible = True
    lblDepAct3(iBar).Visible = True
    lblDepTotPax(iBar).Visible = True
    lblDepTotBag(iBar).Visible = True
    lblDepTotUld(iBar).Visible = True
    lblDepTot1(iBar).Visible = True
    lblDepTot2(iBar).Visible = True
    lblDepTot3(iBar).Visible = True
    lblDepTot4(iBar).Visible = True
    lblDepCsct(iBar).Visible = True
    picDepStatus(iBar).Visible = True

    lblDepTpax(iBar).Visible = True
    lblDepTbag(iBar).Visible = True
    lblDepTuld(iBar).Visible = True

End Sub

Private Sub chkWork_Click(Index As Integer)
    If chkWork(Index).Value = 1 Then
        chkWork(Index).BackColor = LightGreen
        chkWork(Index).Refresh
        Select Case Index
            Case 0
                ShowBarsOverlapped True
            Case 1
                TopPanel.Visible = True
                Form_Resize
            Case Else
        End Select
    Else
        Select Case Index
            Case 0
                ShowBarsOverlapped False
            Case 1
                TopPanel.Visible = False
                Form_Resize
            Case Else
        End Select
        chkWork(Index).BackColor = vbButtonFace
    End If
End Sub
Private Sub ShowBarsOverlapped(ShowOverlap As Boolean)
    Dim ArrLeft As Long
    Dim DepLeft As Long
    Dim iBar As Integer
    Screen.MousePointer = 11
    'For iBar = 0 To ChartLine2.UBound
        'If (lblArrFtyp(iBar).Tag <> "") And (lblDepFtyp(iBar).Tag <> "") Then
        '    If ShowOverlap Then
        '        ArrFlight(iBar).Top = -30
        '        lblArrBar(iBar).Top = 0
        '        DepFlight(iBar).Top = 60
        '        lblDepBar(iBar).Top = 90
        '    Else
        '        ArrFlight(iBar).Top = 15
        '        lblArrBar(iBar).Top = 45
        '        DepFlight(iBar).Top = 15
        '        lblDepBar(iBar).Top = 45
        '    End If
        '    'End If
        'End If
    'Next
    Screen.MousePointer = 0
End Sub


Private Sub CnxFilterPanel_Click()
    MidScConx(0).ZOrder
End Sub

Private Sub CnxRefresh_Timer()
    'QuickHack
    'RefreshCurrentCnxChart
    RefreshCurrentCnxChart
    CnxRefresh.Enabled = False
End Sub

Private Sub DepFlight_Click(Index As Integer)
    HighlightCurrentBar Index, ChartName
End Sub

Public Sub Form_Activate()
    Static IsActivated As Boolean
    Dim tmpWidth As Long
    Dim tmpHeight As Long
    Dim tmpSize As Long
    If IsActivated = False Then
        If MainLifeStyle Then
            tmpWidth = Screen.Width + 2100
            tmpHeight = Screen.Height + 2100
            ButtonPanel.Width = tmpWidth
            tmpSize = ButtonPanel.Height
            ButtonPanel.Height = tmpSize * 2
            DrawBackGround ButtonPanel, WorkAreaColor, True, True
            ButtonPanel.Height = tmpSize
            
            RightPanel.Height = tmpHeight
            DrawBackGround RightPanel, WorkAreaColor, True, True
            
            tmpSize = TimeScale(0).Height
            TimeScale(0).Height = tmpSize * 6
            DrawBackGround TimeScale(0), MainAreaColor, True, True
            TimeScale(0).Height = tmpSize
            
            TopRemark(0).Width = tmpWidth
            tmpSize = TopRemark(0).Height
            TopRemark(0).Height = tmpSize * 2
            DrawBackGround TopRemark(0), MainAreaColor, True, True
            TopRemark(0).Height = tmpSize
            
            BottomRemark(0).Width = tmpWidth
            tmpSize = BottomRemark(0).Height
            BottomRemark(0).Height = tmpSize * 2
            DrawBackGround BottomRemark(0), MainAreaColor, True, True
            BottomRemark(0).Height = tmpSize
            
            WorkArea(0).Height = tmpHeight
            WorkArea(0).Width = tmpWidth
            DrawBackGround WorkArea(0), MainAreaColor, True, True
            'DrawBackGround TopPanel, WorkAreaColor, True, True
        Else
            'lblArrRow.ForeColor = vbButtonText
            'lblDepRow.ForeColor = vbButtonText
        End If
        Me.Refresh
        Me.Top = 0
        Me.Height = Screen.Height - 300
        Me.Refresh
        IsActivated = True
        chkSelDeco(2).Value = 1
    End If
End Sub
Private Sub ShowAllArrPaxConnex()
    Dim iBar As Integer
    Dim tmpData As String
    DontRefreshCnx = True
    chkLayout(0).Value = 1
    ClearConnexDetails
'DrawBackGround MidScConx(0), 7, False, True
    
    For iBar = 0 To CurBarMax
        picArrCnx(iBar).MousePointer = 0
        tmpData = lblArrCsct(iBar).Tag
        If tmpData <> "" Then
            ArrCnxIdx = -1
            CurCnxView = 0
            ShowArrConnexDetails iBar
            picArrCnx(iBar).MousePointer = 99
        End If
    Next
    ArrCnxIdx = -1
    CurCnxView = 0
    chkLayout(CurLayoutType).Value = 1
    DontRefreshCnx = False
End Sub
Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    Dim ScrollIt As Boolean
    Dim NxtIdx As Integer
    Dim TopIdx As Integer
    Dim BotIdx As Integer
    Dim LinCnt As Integer
    Dim NewVal As Integer
    If Shift = 1 Then ScrollIt = True Else ScrollIt = False
    TopIdx = VScroll2(0).Value
    LinCnt = Int(WorkArea(0).Height / (ChartLine2(0).Height + 30))
    BotIdx = TopIdx + LinCnt - 1
    Select Case KeyCode
        Case vbKeyDown
            NxtIdx = CurBarIdx + 1
            If NxtIdx > VScroll2(0).Max Then NxtIdx = VScroll2(0).Max
            If (ScrollIt = False) And (NxtIdx <= CurBarMax) Then
                HighlightCurrentBar NxtIdx, ChartName
                If CurBarIdx > BotIdx Then ScrollIt = True
            End If
            If (ScrollIt = True) And (VScroll2(0).Value < VScroll2(0).Max) Then
                VScroll2(0).Value = VScroll2(0).Value + 1
            End If
            KeyCode = 0
        Case vbKeyUp
            NxtIdx = CurBarIdx - 1
            If (ScrollIt = False) And (NxtIdx >= 0) Then
                HighlightCurrentBar NxtIdx, ChartName
                If CurBarIdx < TopIdx Then ScrollIt = True
            End If
            If (ScrollIt = True) And (VScroll2(0).Value > 0) Then
                VScroll2(0).Value = VScroll2(0).Value - 1
            End If
            KeyCode = 0
        Case vbKeyPageDown
            NxtIdx = CurBarIdx + LinCnt
            If NxtIdx > CurBarMax Then NxtIdx = CurBarMax
            If NxtIdx > VScroll2(0).Max Then NxtIdx = VScroll2(0).Max
            If (ScrollIt = False) And (NxtIdx <= CurBarMax) Then
                HighlightCurrentBar NxtIdx, ChartName
                If CurBarIdx > BotIdx Then ScrollIt = True
            End If
            If (ScrollIt = True) And (VScroll2(0).Value < VScroll2(0).Max) Then
                NxtIdx = VScroll2(0).Value + LinCnt
                If NxtIdx > CurBarMax Then NxtIdx = CurBarMax
                If NxtIdx > VScroll2(0).Max Then NxtIdx = VScroll2(0).Max
                VScroll2(0).Value = NxtIdx
            End If
            KeyCode = 0
        Case vbKeyPageUp
            NxtIdx = CurBarIdx - LinCnt
            If NxtIdx < 0 Then NxtIdx = 0
            If (ScrollIt = False) And (NxtIdx >= 0) Then
                HighlightCurrentBar NxtIdx, ChartName
                If CurBarIdx < TopIdx Then ScrollIt = True
            End If
            If (ScrollIt = True) And (VScroll2(0).Value > 0) Then
                NxtIdx = VScroll2(0).Value - LinCnt
                If NxtIdx < 0 Then NxtIdx = 0
                VScroll2(0).Value = NxtIdx
            End If
            KeyCode = 0
        Case vbKeyLeft
            Select Case Shift
                Case 0
                    NewVal = HScroll2(0).Value - 5
                Case 1
                    NewVal = HScroll2(0).Value - 1
                Case 2
                    NewVal = HScroll2(0).Value - 20
                Case Else
                    NewVal = HScroll2(0).Value - 5
            End Select
            If NewVal < 0 Then NewVal = 0
            HScroll2(0).Value = NewVal
            KeyCode = 0
        Case vbKeyRight
            Select Case Shift
                Case 0
                    NewVal = HScroll2(0).Value + 5
                Case 1
                    NewVal = HScroll2(0).Value + 1
                Case 2
                    NewVal = HScroll2(0).Value + 20
                Case Else
                    NewVal = HScroll2(0).Value + 5
            End Select
            If NewVal > HScroll2(0).Max Then NewVal = HScroll2(0).Max
            HScroll2(0).Value = NewVal
            KeyCode = 0
        Case Else
    End Select
End Sub

Private Sub Form_Load()
    ChartName = "CNX"
    CnxLayoutType = -1
    CurLayoutType = 4
    MainAreaColor = Val(GetIniEntry(myIniFullName, "MAIN", "", "MAIN_AREA_COLOR", "7"))
    WorkAreaColor = MainAreaColor
    ChartBarBackColor = LightGray
    lblBarLineColor(0).BackColor = ChartBarBackColor
    lblBarLineColor(0).Tag = "7"
    MinimumTop = 30
    MinimumLeft = 30
    CurChartWidth = 0
    CurChartHeight = 0
    ArrCnxIdx = -1
    DepCnxIdx = -1
    Me.Top = Screen.Height + 1000
    Me.Left = 0
    Me.Width = Screen.Width
    Me.Height = Screen.Height
    
    'UtcTimeDiff = 420
    chkUtc(1).Value = 1
    
    'We have a restriction of a maximum height of a picture box (Panel)
    'The max height is 245745 twips
    'So the maximum number of bars on the chart is:
    MaxBarCnt = CInt(245745 \ (ChartLine2(0).Height + 30)) - 1
    'Maximum is 302, but together with the StatusChart
    'we run out of memory so we set it to 100
    MaxBarCnt = 100
    'MaxBarCnt = 300
    CurBarMax = 0
    CurBarIdx = -1
    InitChartArea
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    chkAppl(0).Value = 1
    Cancel = True
End Sub

Private Sub Form_Resize()
    Dim NewHeight As Long
    Dim NewWidth As Long
    Dim NewLeft As Long
    Dim NewTop As Long
    Dim TimePanelSize As Long
    Dim TimePanelLeft As Long
    
    VScroll1(0).Visible = False
    VScroll2(0).Visible = False
    HScroll2(0).Visible = False
    
    NewTop = MinimumTop
    ButtonPanel.Top = NewTop
    RightPanel.Top = NewTop
    NewLeft = MinimumLeft
    ButtonPanel.Left = NewLeft
    TopPanel.Left = NewLeft
    LeftPanel(0).Left = NewLeft
    
    NewWidth = Me.ScaleWidth
    NewLeft = NewWidth
    If RightPanel.Visible = True Then
        NewLeft = NewWidth - RightPanel.Width - 15
    End If
    RightPanel.Left = NewLeft
    NewWidth = NewLeft - MinimumLeft - 15
    If NewWidth > 300 Then
        ButtonPanel.Width = NewWidth
        TopPanel.Width = NewWidth
    End If
    NewLeft = RightPanel.Left - VertScroll2(0).Width - 15
    VertScroll2(0).Left = NewLeft
    HorizScroll3(0).Left = NewLeft
    TimeScroll2(0).Left = NewLeft
    
    NewHeight = Me.ScaleHeight - StatusBar.Height - MinimumTop
    If NewHeight > 300 Then
        RightPanel.Height = NewHeight
    End If
    
    NewTop = MinimumTop
    NewTop = NewTop + ButtonPanel.Height + 15
    If TopPanel.Visible Then
        TopPanel.Top = NewTop
        NewTop = NewTop + TopPanel.Height + 15
    End If
    LeftPanel(0).Top = NewTop
    TopRemark(0).Top = NewTop
    TimeScroll1(0).Top = NewTop
    TimeScroll2(0).Top = NewTop
    
    NewHeight = NewHeight - NewTop + MinimumTop
    If NewHeight > 300 Then
        LeftPanel(0).Height = NewHeight
    End If
    
    NewTop = NewTop + TopRemark(0).Height '+ 15
    TimePanel(0).Top = NewTop
    
    NewTop = NewTop + TimePanel(0).Height + 15
    VertScroll1(0).Top = NewTop
    WorkArea(0).Top = NewTop
    VertScroll2(0).Top = NewTop
    
    NewLeft = MinimumLeft
    If LeftPanel(0).Visible Then
        NewLeft = NewLeft + LeftPanel(0).Width + 15
    End If
    TimeScroll1(0).Left = NewLeft
    TopRemark(0).Left = NewLeft
    
    NewWidth = RightPanel.Left - NewLeft - 15
    If NewWidth > 300 Then
        'TopRemark(0).Width = NewWidth
        'BottomRemark(0).Width = NewWidth
    End If
    
    If VertScroll1(0).Visible Then
        VertScroll1(0).Left = NewLeft
        HorizScroll1(0).Left = NewLeft
        NewLeft = NewLeft + VertScroll1(0).Width + 15
        WorkArea(0).Left = NewLeft
        HorizScroll2(0).Left = NewLeft
        NewWidth = RightPanel.Left - NewLeft - 15
        If VertScroll2(0).Visible = True Then NewWidth = NewWidth - VertScroll2(0).Width - 15
        If NewWidth > 300 Then
            WorkArea(0).Width = NewWidth
            If VertScroll2(0).Visible = False Then NewWidth = NewWidth - HorizScroll3(0).Width - 15
            If NewWidth > 300 Then
                HorizScroll2(0).Width = NewWidth
                HScroll2(0).Width = NewWidth - 60
            End If
        End If
    Else
        WorkArea(0).Left = NewLeft
        HorizScroll1(0).Left = NewLeft
        NewWidth = RightPanel.Left - NewLeft - 15
        If VertScroll2(0).Visible = True Then NewWidth = NewWidth - VertScroll2(0).Width - 15
        If NewWidth > 300 Then
            WorkArea(0).Width = NewWidth
            NewLeft = NewLeft + HorizScroll1(0).Width + 15
            HorizScroll2(0).Left = NewLeft
            NewWidth = NewWidth - HorizScroll1(0).Width - 15
            If VertScroll2(0).Visible = False Then NewWidth = NewWidth - HorizScroll3(0).Width - 15
            If NewWidth > 300 Then
                HorizScroll2(0).Width = NewWidth
                HScroll2(0).Width = NewWidth - 60
            End If
        End If
    End If
    
    
    TimePanel(0).Left = WorkArea(0).Left
    TimePanel(0).Width = WorkArea(0).Width
    TopRemark(0).Left = WorkArea(0).Left
    TopRemark(0).Width = WorkArea(0).Width
    
    BottomRemark(0).Left = HorizScroll2(0).Left
    BottomRemark(0).Width = HorizScroll2(0).Width
    
    NewTop = Me.ScaleHeight - StatusBar.Height - BottomRemark(0).Height
    BottomRemark(0).Top = NewTop - 30
    NewTop = NewTop - BottomRemark(0).Height - 15
    HorizScroll1(0).Top = NewTop
    HorizScroll2(0).Top = NewTop
    HorizScroll3(0).Top = NewTop
    NewHeight = NewTop - WorkArea(0).Top - 15
    If NewHeight > 300 Then
        VertScroll1(0).Height = NewHeight
        VScroll1(0).Height = NewHeight - 60
        WorkArea(0).Height = NewHeight
        VertScroll2(0).Height = NewHeight
        VScroll2(0).Height = NewHeight - 60
    End If
    
    ArrangeChartArea
    
    VScroll1(0).Visible = True
    VScroll1(0).Refresh
    VScroll2(0).Visible = True
    VScroll2(0).Refresh
    HScroll2(0).Visible = True
    HScroll2(0).Refresh
  
    WorkArea(0).Refresh
    
    Me.Refresh
End Sub

Private Sub InitChartArea()
    Dim NewColor As Long
    Dim NewTop As Long
    Dim NewLeft As Long
    Dim NewHeight As Long
    Dim NewWidth As Long
    Screen.MousePointer = 11
    
    NewColor = ChartBarBackColor
    ChartLine1(0).BackColor = NewColor
    'ArrCnxInfo(0).BackColor = NewColor
    ChartLine2(0).BackColor = NewColor
    ChartLine3(0).BackColor = NewColor
    'DepCnxInfo(0).BackColor = NewColor
    ArrFlight(0).BackColor = NewColor
    DepFlight(0).BackColor = NewColor
    LeftScale(0).BackColor = NewColor
    RightScale(0).BackColor = NewColor
    MidScConx(0).BackColor = NewColor
    'MidScCon1(0).BackColor = NewColor
    LeftScCon1(0).BackColor = vbButtonFace
    RightScCon1(0).BackColor = vbButtonFace
    
    lblArrTpax(0).Left = -15
    lblArrTbag(0).Left = -15
    lblArrTuld(0).Left = -15
    lblArrTpax(0).Height = 225
    lblArrTbag(0).Height = 240
    lblArrTuld(0).Height = 225
    lblArrTpax(0).Width = 465
    lblArrTbag(0).Width = 465
    lblArrTuld(0).Width = 465
    lblArrTpax(0).Top = -15
    lblArrTbag(0).Top = lblArrTpax(0).Top + lblArrTpax(0).Height - 15
    lblArrTuld(0).Top = lblArrTbag(0).Top + lblArrTbag(0).Height - 15
    lblArrTpax(0).BackColor = LightGreen
    lblArrTbag(0).BackColor = LightYellow
    lblArrTuld(0).BackColor = LightBlue
    lblArrTpax(0).ZOrder
    lblArrTuld(0).ZOrder
    
    lblDepTpax(0).Left = -15
    lblDepTbag(0).Left = -15
    lblDepTuld(0).Left = -15
    lblDepTpax(0).Height = 225
    lblDepTbag(0).Height = 240
    lblDepTuld(0).Height = 225
    lblDepTpax(0).Width = 465
    lblDepTbag(0).Width = 465
    lblDepTuld(0).Width = 465
    lblDepTpax(0).Top = -15
    lblDepTbag(0).Top = lblDepTpax(0).Top + lblDepTpax(0).Height - 15
    lblDepTuld(0).Top = lblDepTbag(0).Top + lblDepTbag(0).Height - 15
    lblDepTpax(0).BackColor = LightGreen
    lblDepTbag(0).BackColor = LightYellow
    lblDepTuld(0).BackColor = LightBlue
    lblDepTpax(0).ZOrder
    lblDepTuld(0).ZOrder
    
    lblBgnTime(0).BackColor = LightGray
    
    NewLeft = -30
    VScrollArea(0).Left = NewLeft
    HScrollArea(0).Left = NewLeft
    LeftScale(0).Left = NewLeft
    
    NewLeft = 30
    ChartLine1(0).Left = NewLeft
    ChartLine3(0).Left = NewLeft
    
    NewLeft = -30
    ChartLine2(0).Left = NewLeft
    
    NewTop = -30
    VScrollArea(0).Top = NewTop
    HScrollArea(0).Top = NewTop
    LeftScale(0).Top = NewTop
    RightScale(0).Top = NewTop
    MidScConx(0).Top = NewTop
    
    NewTop = 30
    ChartLine1(0).Top = NewTop
    ChartLine2(0).Top = NewTop
    ChartLine3(0).Top = NewTop
    
    NewTop = -30
    TimePanel(0).Height = TimeScroll1(0).Height - 300
    TimeScale(0).Top = NewTop
    TimeScale(0).Height = TimePanel(0).Height + 60
    
    CreateTimeScaleArea TimeScale(0)
    HScrollArea(0).Width = CurChartWidth
    VScrollArea(0).Width = CurChartWidth
    ChartLine2(0).Width = CurChartWidth
    
    ChartLine3(0).Width = ArrFlight(0).Width + 90
    ChartLine3(0).Width = DepFlight(0).Width + 90
    
    Screen.MousePointer = 11
    CreateChartBarLayout 680
    LeftScale(0).Width = ChartLine1(0).Width + 120
    RightScale(0).Width = ChartLine3(0).Width + 120
    MidScConx(0).Height = CurChartHeight
    
    Set MidScCon1(0).Container = TimePanel(0)
    MidScCon1(0).Top = -30
    MidScCon1(0).Left = LeftScale(0).Left
    MidScCon1(0).Height = TimePanel(0).Height + 60
    MidScCon1(0).Width = LeftScale(0).Width
    Set LeftScCon1(0).Container = TimePanel(0)
    LeftScCon1(0).Top = -30
    LeftScCon1(0).Left = LeftScale(0).Left
    LeftScCon1(0).Height = TimePanel(0).Height + 60
    LeftScCon1(0).Width = LeftScale(0).Width
    Set RightScCon1(0).Container = TimePanel(0)
    RightScCon1(0).Top = -30
    RightScCon1(0).Height = TimePanel(0).Height + 60
    RightScCon1(0).Left = RightScale(0).Left
    RightScCon1(0).Width = RightScale(0).Width
    
    TabDepInfo(0).Top = 0
    TabDepInfo(0).Left = 0
    TabDepInfo(0).Height = RightScCon1(0).Height - 120
    TabDepInfo(0).Width = RightScCon1(0).Width - 60
    InitDepInfoTab
    
    LeftLine(0).Y1 = 0
    LeftLine(0).Y2 = CurChartHeight
    LeftLine(1).Y1 = LeftLine(0).Y1
    LeftLine(1).Y2 = LeftLine(0).Y2
    LeftLine(1).X1 = LeftLine(0).X1 + 15
    LeftLine(1).X2 = LeftLine(0).X2 + 15
    
    RightLine(0).Y1 = LeftLine(0).Y1
    RightLine(0).Y2 = LeftLine(0).Y2
    RightLine(1).Y1 = LeftLine(1).Y1
    RightLine(1).Y2 = LeftLine(1).Y2
    RightLine(1).X1 = RightLine(0).X1 + 15
    RightLine(1).X2 = RightLine(0).X2 + 15
    
    Screen.MousePointer = 11
    
    TimeLine2(0).ZOrder
    lblCurTime(0).ZOrder
    LeftScale(0).ZOrder
    RightScale(0).ZOrder
    LeftPanel(0).ZOrder
    ButtonPanel.ZOrder
    TopPanel.ZOrder
    TimePanel(0).ZOrder
    TopRemark(0).ZOrder
    WorkArea(0).ZOrder
    BottomRemark(0).ZOrder
    RightPanel.ZOrder
    StatusBar.ZOrder
    Screen.MousePointer = 0
    
End Sub

Private Sub InitDepInfoTab()
    Dim DepInfoFields As String
    TabDepInfo(0).ResetContent
    If MainLifeStyle Then
        TabDepInfo(0).LifeStyle = True
        TabDepInfo(0).CursorLifeStyle = True
    End If
    DepInfoFields = "AFSD,FLND,STOD,DES3,PSTD,SCID,TPAX,TBAG,TULD,REMA,DURN"
    TabDepInfo(0).LogicalFieldList = DepInfoFields
    TabDepInfo(0).HeaderString = "S,Flight,STD,Dest,Bay,C,PAX,BAG,ULD,Remark                               .,DURN"
    TabDepInfo(0).HeaderLengthString = "10,10,10,10,10,10,10,10,10,10,10,10,10,10"
    TabDepInfo(0).ColumnAlignmentString = "C,L,C,C,L,C,R,R,R,L,R"
    TabDepInfo(0).HeaderAlignmentString = "C,L,C,C,L,C,R,R,R,L,R"
    TabDepInfo(0).FontName = "Arial"
    TabDepInfo(0).FontSize = 9 + 6
    TabDepInfo(0).HeaderFontSize = 9 + 6
    TabDepInfo(0).LineHeight = 9 + 8
    TabDepInfo(0).LeftTextOffset = 1
    TabDepInfo(0).SetTabFontBold True
    TabDepInfo(0).MainHeader = False
    TabDepInfo(0).DisplayBackColor = vbYellow
    TabDepInfo(0).DateTimeSetColumn 2
    TabDepInfo(0).DateTimeSetInputFormatString 2, "YYYYMMDDhhmm"
    TabDepInfo(0).DateTimeSetOutputFormatString 2, "hh':'mm'/'DD"
    TabDepInfo(0).AutoSizeByHeader = True
    TabDepInfo(0).AutoSizeColumns
    TabDepInfo(0).ShowVertScroller True
    TabDepInfo(0).Visible = True
    TabDepInfo(0).ZOrder
End Sub
Private Sub ArrangeChartArea()
    Dim NewTop As Long
    Dim NewLeft As Long
    Dim NewHeight As Long
    Dim NewWidth As Long
    Dim NewSize As Long
    Dim NewValue As Long
    Dim MinSize As Long
    Dim tmpTag As String
    Dim tmpData As String
    Dim X1 As Long
    Dim X2 As Long
    Dim X3 As Long
    Dim LastSize As Long
    Dim PrvHours As Long
    Dim NewHours As Long
    Dim MinHours As Long
    Dim MaxHours As Long
    Dim MinCnxSize As Long
    Dim MovePanel As Boolean
    Dim LayOutIdx As Integer
    Dim PrvLayOut As Integer
    Dim NxtLayOut As Integer

    NewWidth = WorkArea(0).Width + 60
    VScrollArea(0).Width = NewWidth
    NewLeft = VScrollArea(0).Width - RightScale(0).Width - 90
    RightScale(0).Left = NewLeft
    RightScCon1(0).Left = NewLeft
    MinSize = CnxMidFilterPanel.Width + 75
    NewSize = NewLeft - LeftScCon1(0).Left - LeftScCon1(0).Width
    If NewSize < MinSize Then NewSize = MinSize
    MidScCon1(0).Width = NewSize
    MidScConx(0).Width = NewSize
    
    NewSize = NewSize - 75
    CnxFilterPanel.Width = NewSize
    fraTabCaption(0).Width = NewSize + 30
    lblTabCaptionShadow(0).Width = NewSize - 60
    lblTabCaption(0).Width = NewSize - 60
    NewLeft = (NewSize - CnxMidFilterPanel.Width) \ 2
    CnxMidFilterPanel.Left = NewLeft
    
    
    CurScrollWidth = CurChartWidth - WorkArea(0).Width
    NewValue = CLng(HScroll2(0).Value)
    AdjustHorizScroll NewValue
    
    MovePanel = False
    LayOutIdx = -1
    tmpTag = MidScConx(0).Tag
    If tmpTag <> "" Then
        GetKeyItem tmpData, tmpTag, "{X3}", "{/X3}"
        LastSize = Val(tmpData)
        If LastSize <> MidScConx(0).Width Then
            GetKeyItem tmpData, tmpTag, "{H}", "{/H}"
            PrvHours = Val(tmpData)
            Select Case CurLayoutType
                Case 0, 1
                Case 2, 3
                    MinHours = 1
                    MaxHours = 6
                Case 4
                    MinHours = 2
                    MaxHours = 6
                    MinCnxSize = 5160
                    PrvLayOut = 4
                    X1 = 1605
                    X3 = 1755
                Case Else
            End Select
            X2 = MidScConx(0).Width - X1 - X3
            NewHours = X2 \ 15 \ 60
            If NewHours < MinHours Then NewHours = MinHours
            If NewHours > MaxHours Then NewHours = MaxHours
            CurCnxLayoutHours = NewHours
            X2 = NewHours * 60 * 15
            NewSize = X1 + X2 + X3
            If NewSize >= MinCnxSize Then
            Else
                'LayOutIdx = PrvLayOut
                
            End If
                'Calculation: 6Hours * 60Min * 15Twips
                'CnxRangeMin = CurCnxLayoutHours * 60
                'NewLeft = LeftTime(0).Left + (CnxRangeMin * 15)
            
        End If
    End If
    
    NewLeft = LeftScale(0).Left + LeftScale(0).Width
    NewSize = RightScale(0).Left - NewLeft
    X2 = MidScConx(0).Width
    If X2 > NewSize Then NewLeft = NewLeft - ((X2 - NewSize) \ 2)
    MidScCon1(0).Left = NewLeft
    MidScConx(0).Left = NewLeft
    MidScCon1(0).ZOrder
    MidScConx(0).ZOrder
    
    TimeScale(0).Left = HScrollArea(0).Left
    TimeScale(0).Width = HScrollArea(0).Width
    'TimeScale(0).Refresh
    
    If LayOutIdx > 0 Then
        DontRefreshCnx = True
        chkLayout(LayOutIdx).Value = 1
        DontRefreshCnx = False
    End If
    CnxRefresh.Enabled = True
    CheckSelFlightPanels
    
End Sub
Private Sub RefreshCurrentCnxChart()
    Dim CurArrCnxIdx As Integer
    CurArrCnxIdx = ArrCnxIdx
    If CurArrCnxIdx >= 0 Then
        ShowArrConnexDetails CurArrCnxIdx
    Else
        ClearConnexDetails
        ShowAllArrPaxConnex
    End If
End Sub

Private Sub fraTabCaption_DblClick(Index As Integer)
    RefreshCurrentCnxChart
End Sub

Private Sub HScroll2_Change(Index As Integer)
    Dim NewValue As Long
    NewValue = CLng(HScroll2(Index).Value)
    AdjustHorizScroll NewValue
End Sub
Private Sub HScroll2_Scroll(Index As Integer)
    Dim NewValue As Long
    NewValue = CLng(HScroll2(Index).Value)
    AdjustHorizScroll NewValue
End Sub
Private Sub AdjustHorizScroll(ScrollValue As Long)
    Dim NewLeft As Long
    NewLeft = ScrollValue * MinuteWidth
    HScrollArea(0).Left = -NewLeft
    WorkArea(0).Refresh
    TimeScale(0).Left = -NewLeft
    TimeScale(0).Refresh
    CheckSelFlightPanels
End Sub

Private Sub icoCflRange_MouseDown(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    icoCflRange(Index).Tag = CStr(X)
    RangeLine(Index).Left = icoCflRange(Index).Left + 105
    RangeLine(Index).Visible = True
    If Index = 0 Then
        ShowTimeRangeDuration True, RangeLine(Index).Left, TimeLine1(0).Left
    Else
        ShowTimeRangeDuration True, TimeLine1(0).Left, RangeLine(Index).Left
    End If
End Sub

Private Sub icoCflRange_MouseMove(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    Dim oldX As Long
    Dim newX As Long
    Dim diffX As Long
    Dim NewLeft As Long
    Dim MaxLeft As Long
    Dim MinLeft As Long
    Dim DurLeft As Long
    Dim DurRight As Long
    oldX = CLng(Val(icoCflRange(Index).Tag))
    newX = CLng(X)
    If oldX > 0 Then
        If oldX <> newX Then
            diffX = newX - oldX
            NewLeft = icoCflRange(Index).Left + diffX
            NewLeft = (NewLeft \ MinuteWidth) * MinuteWidth
            If Index = 0 Then
                MinLeft = lblMin1(0).Left
                MaxLeft = TimeLine1(0).Left - (MinuteWidth * 10)
                DurLeft = NewLeft + 105
                DurRight = TimeLine1(0).Left
            Else
                MinLeft = TimeLine1(0).Left + (MinuteWidth * 10)
                MaxLeft = CurChartWidth - 1200
                DurRight = NewLeft + 105
                DurLeft = TimeLine1(0).Left
            End If
            If (NewLeft >= MinLeft) And (NewLeft <= MaxLeft) Then
                RangeLine(Index).Visible = False
                HScrollArea(0).Refresh
                icoCflRange(Index).Left = NewLeft
                NewLeft = NewLeft + 105
                RangeLine(Index).Left = NewLeft
                RangeLine(Index).Visible = True
                RangeLine(Index).Refresh
                ShowTimeRangeDuration True, DurLeft, DurRight
            End If
        End If
    End If
End Sub
Private Function ShowTimeRangeDuration(ShowLabel As Boolean, CurLeft As Long, CurRight As Long) As Long
    Dim tmpLeft As Long
    Dim tmpRight As Long
    Dim tmpWidth As Long
    Dim tmpDuration As Long
    Dim tmpVal As Long
    Dim tmpText As String
    lblDuration.Visible = False
    If CurRight > CurLeft Then
        tmpLeft = CurLeft
        tmpRight = CurRight
    Else
        tmpLeft = CurRight
        tmpRight = CurLeft
    End If
    tmpWidth = tmpRight - tmpLeft + 15
    lblDuration.Left = tmpLeft
    lblDuration.Width = tmpWidth
    tmpDuration = tmpWidth \ MinuteWidth
    tmpVal = tmpDuration Mod 60
    tmpText = Right("00" & CStr(tmpVal), 2)
    tmpVal = (tmpDuration - tmpVal) \ 60
    tmpText = Right("00" & CStr(tmpVal), 2) & ":" & tmpText
    lblDuration.Caption = tmpText
    lblDuration.Visible = ShowLabel
    lblDuration.Refresh
    ShowTimeRangeDuration = tmpDuration
End Function

Private Sub icoCflRange_MouseUp(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    icoCflRange(Index).Tag = "-1"
    RangeLine(Index).Visible = False
    lblDuration.Visible = False
End Sub

Private Sub lblArrAct3_Click(Index As Integer)
    HighlightCurrentBar Index, ChartName
End Sub

Private Sub lblArrBar_MouseDown(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    ArrFlight(Index).ZOrder
    HighlightCurrentBar Index, ChartName
    ShowRotaTimeFrame Index, lblArrBar(Index), True
End Sub

Private Sub lblArrBar_MouseUp(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    ShowRotaTimeFrame Index, lblArrBar(Index), False
End Sub

Private Sub lblArrCsct_Click(Index As Integer)
    If lblArrCsct(Index).MousePointer = 99 Then
        DontRefreshCnx = True
        If lblArrCsct(Index).BackColor = vbRed Then
            If Index = ArrCnxIdx Then
                If chkCnxShort(0).Value = 1 Then
                    chkCnxShort(0).Value = 0
                    CnxFilterShort = 0
                Else
                    chkCnxShort(0).Value = 1
                    CnxFilterShort = Val(lblArrCsct(Index).Caption)
                End If
            Else
                chkCnxShort(0).Value = 1
                CnxFilterShort = Val(lblArrCsct(Index).Caption)
            End If
            chkCnxCrit(0).Value = 0
        Else
            If Index = ArrCnxIdx Then
                If chkCnxCrit(0).Value = 1 Then
                    chkCnxCrit(0).Value = 0
                    CnxFilterCritical = 0
                Else
                    chkCnxCrit(0).Value = 1
                    CnxFilterCritical = Val(lblArrCsct(Index).Caption)
                End If
            Else
                chkCnxCrit(0).Value = 1
                CnxFilterCritical = Val(lblArrCsct(Index).Caption)
            End If
            chkCnxShort(0).Value = 0
        End If
        ShowArrConnexDetails Index
        DontRefreshCnx = False
    End If
End Sub

Private Sub lblArrEtai_Click(Index As Integer)
    HighlightCurrentBar Index, ChartName
End Sub

Private Sub lblArrFlno_Click(Index As Integer)
    HighlightCurrentBar Index, ChartName
End Sub

Private Sub lblArrFlti_Click(Index As Integer)
    HighlightCurrentBar Index, ChartName
End Sub

Private Sub lblArrOnbl_Click(Index As Integer)
    HighlightCurrentBar Index, ChartName
End Sub

Private Sub lblArrOrg3_Click(Index As Integer)
    HighlightCurrentBar Index, ChartName
End Sub

Private Sub lblArrPsta_Click(Index As Integer)
    HighlightCurrentBar Index, ChartName
End Sub

Private Sub lblArrRegn_Click(Index As Integer)
    HighlightCurrentBar Index, ChartName
End Sub

Private Sub lblArrStoa_Click(Index As Integer)
    HighlightCurrentBar Index, ChartName
End Sub

Private Sub lblArrTot1_Click(Index As Integer)
    If lblArrTot1(Index).MousePointer = 99 Then
        DontRefreshCnx = True
        chkCnxClass(0).Value = 1
        CnxFilterClass = 1
        ShowArrConnexDetails Index
        DontRefreshCnx = False
    End If
End Sub

Private Sub lblArrTot2_Click(Index As Integer)
    If lblArrTot2(Index).MousePointer = 99 Then
        DontRefreshCnx = True
        chkCnxClass(1).Value = 1
        CnxFilterClass = 2
        ShowArrConnexDetails Index
        DontRefreshCnx = False
    End If
End Sub

Private Sub lblArrTot3_Click(Index As Integer)
    If lblArrTot3(Index).MousePointer = 99 Then
        DontRefreshCnx = True
        chkCnxClass(2).Value = 1
        CnxFilterClass = 3
        ShowArrConnexDetails Index
        DontRefreshCnx = False
    End If
End Sub

Private Sub lblArrTotPax_Click(Index As Integer)
    Dim i As Integer
    If lblArrTotPax(Index).MousePointer = 99 Then
        'Field of Arrival Flight Panel
        'Reset all filters to default
        DontRefreshCnx = True
        chkCnxAdid(0).Value = 1
        CnxFilterAdid = 0
        chkCnxType(0).Value = 1
        CnxFilterType = 1
        If chkCnxClass(0).Tag <> "" Then
            i = Val(chkCnxClass(0).Tag)
            chkCnxClass(i).Value = 0
        End If
        CnxFilterClass = 0
        chkCnxShort(0).Value = 0
        CnxFilterShort = 0
        chkCnxCrit(0).Value = 0
        CnxFilterCritical = 0
        ShowArrConnexDetails Index
        DontRefreshCnx = False
    End If
End Sub

Private Sub lblArrTpax_Click(Index As Integer)
    If lblArrTpax(Index).Caption <> "" Then
        'Field of Arrival Type Totals Panel
        'Keep all filters ...
        '... but set Type Filter to PAX
        DontRefreshCnx = True
        chkCnxType(0).Value = 1
        CnxFilterType = 1
        ShowArrConnexDetails Index
        DontRefreshCnx = False
    End If
End Sub

Private Sub lblArrTpax_MouseMove(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    TriggerMouseOverObj Index, "ARRPAX_" & CStr(Index), "ARR"
End Sub

Private Sub TriggerMouseOverObj(Index As Integer, ObjKey As String, TimerKey As String)
    Dim tmpTag As String
    Dim tmpType As String
    Dim tmpIdx As String
    Dim tmpObj As String
    Dim iBar As Integer
    If CurCnxView = 0 Then
        If CnxMouseObj <> tmpObj Then
            CnxMouseObj = tmpObj
            tmpTag = MouseTimer(0).Tag
            tmpType = GetItem(tmpTag, 1, ",")
            tmpIdx = GetItem(tmpTag, 2, ",")
            iBar = Val(tmpIdx)
            If (Index <> iBar) Or (tmpType <> TimerKey) Then
                MouseTimer(0).Enabled = False
                Select Case tmpType
                    Case "ARR"
                        ArrCnxInfo(iBar).Visible = False
                    Case "DEP"
                        DepCnxInfo(iBar).Visible = False
                    Case Else
                End Select
                MouseTimer(0).Tag = TimerKey & "," & CStr(Index)
                MouseTimer(0).Enabled = True
            End If
        End If
    End If
End Sub

Private Sub lblArrVia3_Click(Index As Integer)
    HighlightCurrentBar Index, ChartName
End Sub

Private Sub lblDepAct3_Click(Index As Integer)
    HighlightCurrentBar Index, ChartName
End Sub

Private Sub lblDepBar_MouseDown(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    DepFlight(Index).ZOrder
    HighlightCurrentBar Index, ChartName
    ShowRotaTimeFrame Index, lblDepBar(Index), True
End Sub

Private Sub lblDepBar_MouseUp(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    ShowRotaTimeFrame Index, lblDepBar(Index), False
End Sub

Private Sub lblDepDes3_Click(Index As Integer)
    HighlightCurrentBar Index, ChartName
End Sub

Private Sub lblDepEtdi_Click(Index As Integer)
    HighlightCurrentBar Index, ChartName
End Sub

Private Sub lblDepFlno_Click(Index As Integer)
    HighlightCurrentBar Index, ChartName
End Sub

Private Sub lblDepFlti_Click(Index As Integer)
    HighlightCurrentBar Index, ChartName
End Sub

Private Sub lblDepOfbl_Click(Index As Integer)
    HighlightCurrentBar Index, ChartName
End Sub

Private Sub lblDepPstd_Click(Index As Integer)
    HighlightCurrentBar Index, ChartName
End Sub

Private Sub lblDepRegn_Click(Index As Integer)
    HighlightCurrentBar Index, ChartName
End Sub

Private Sub lblDepStod_Click(Index As Integer)
    HighlightCurrentBar Index, ChartName
End Sub

Private Sub lblDepTpax_MouseMove(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    TriggerMouseOverObj Index, "DEPPAX_" & CStr(Index), "DEP"
End Sub

Private Sub lblDepVia3_Click(Index As Integer)
    HighlightCurrentBar Index, ChartName
End Sub

Private Sub lblTabCaptionShadow_Click(Index As Integer)
    MidScConx(0).ZOrder
End Sub

Private Sub lblTabCaptionShadow_DblClick(Index As Integer)
    RefreshCurrentCnxChart
End Sub

Private Sub lblTowBar_Click(Index As Integer)
    HighlightCurrentBar Index, ChartName
End Sub

Private Sub ShowRotaTimeFrame(Index As Integer, CurBarObj As Label, ShowRange As Boolean)
    Dim RotLeft As Long
    Dim RotRight As Long
    If ShowRange = True Then
        RotLeft = CurBarObj.Left - 15
        RotRight = CurBarObj.Left + CurBarObj.Width - 15
        RangeLine(0).Left = RotLeft
        RangeLine(1).Left = RotRight
        icoCflRange(0).Left = RotLeft - 105
        icoCflRange(1).Left = RotRight - 105
        RangeLine(0).Visible = True
        RangeLine(1).Visible = True
        icoCflRange(0).Visible = True
        icoCflRange(1).Visible = True
    Else
        RangeLine(0).Visible = False
        RangeLine(1).Visible = False
        icoCflRange(0).Visible = False
        icoCflRange(1).Visible = False
    End If
End Sub

Private Sub LeftScCon1_Click(Index As Integer)
    LeftScale(0).ZOrder
End Sub

Private Sub MidScConx_Click(Index As Integer)
    MidScConx(0).ZOrder
End Sub

Private Sub MidScConx_DblClick(Index As Integer)
    If chkWork(1).Value = 0 Then
        'chkWork(1).Value = 1
    Else
        'chkWork(1).Value = 0
    End If
End Sub

Private Sub MidScConx_MouseMove(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    Dim tmpTagA As String
    Dim tmpTagB As String
    Dim tmpType As String
    Dim tmpIdx As String
    Dim tmpObj As String
    Dim iBar As Integer
    If CurCnxView = 0 Then
        tmpObj = "MIDCNX"
        If CnxMouseObj <> tmpObj Then
            CnxMouseObj = tmpObj
            tmpTagA = MouseTimer(0).Tag
            tmpTagB = MouseTimer(1).Tag
            If MouseTimer(0).Enabled = False Then
                If MouseTimer(1).Enabled = True Then
                    MouseTimer(1).Enabled = False
                    tmpTagB = MouseTimer(1).Tag
                    If tmpTagB <> "" Then
                        tmpType = GetItem(tmpTagB, 1, ",")
                        tmpIdx = GetItem(tmpTagB, 2, ",")
                        iBar = Val(tmpIdx)
                        Select Case tmpType
                            Case "ARR"
                                ArrCnxInfo(iBar).Visible = False
                            Case "DEP"
                                DepCnxInfo(iBar).Visible = False
                            Case Else
                        End Select
                        MouseTimer(1).Tag = ""
                    End If
                End If
                If tmpTagA <> "" Then
                    MouseTimer(1).Tag = tmpTagA
                    MouseTimer(1).Enabled = True
                End If
            End If
            MouseTimer(0).Tag = ""
            MouseTimer(0).Enabled = False
        End If
    End If
End Sub


Private Sub MouseTimer_Timer(Index As Integer)
    Dim tmpTag As String
    Dim tmpType As String
    Dim tmpIdx As String
    Dim iBar As Integer
    If CurCnxView = 0 Then
        Select Case Index
            Case 0
                MouseTimer(0).Enabled = False
                tmpTag = MouseTimer(0).Tag
                If tmpTag <> "" Then
                    tmpType = GetItem(tmpTag, 1, ",")
                    tmpIdx = GetItem(tmpTag, 2, ",")
                    iBar = Val(tmpIdx)
                    Select Case tmpType
                        Case "ARR"
                            ArrCnxInfo(iBar).Visible = True
                        Case "DEP"
                            DepCnxInfo(iBar).Visible = True
                        Case Else
                    End Select
                End If
            Case 1
                MouseTimer(1).Enabled = False
                tmpTag = MouseTimer(1).Tag
                If tmpTag <> "" Then
                    tmpType = GetItem(tmpTag, 1, ",")
                    tmpIdx = GetItem(tmpTag, 2, ",")
                    iBar = Val(tmpIdx)
                    Select Case tmpType
                        Case "ARR"
                            ArrCnxInfo(iBar).Visible = False
                        Case "DEP"
                            DepCnxInfo(iBar).Visible = False
                        Case Else
                    End Select
                End If
                MouseTimer(1).Tag = ""
            Case Else
        End Select
    End If
End Sub

Private Sub OnTop_Click()
    If OnTop.Value = 1 Then
        OnTop.BackColor = LightGreen
        SetFormOnTop Me, True
    Else
        OnTop.BackColor = MyOwnButtonFace
        SetFormOnTop Me, False
    End If
End Sub

Private Sub picArrCnx_Click(Index As Integer)
    If picArrCnx(Index).MousePointer = 99 Then
        DontRefreshCnx = True
        chkCnxAdid(0).Value = 1
        CnxFilterAdid = 0
        'we keep the current other filters
        If ArrCnxIdx >= 0 Then
            ShowAllArrPaxConnex
        Else
            ShowArrConnexDetails Index
        End If
        DontRefreshCnx = False
    End If
End Sub

Private Sub picArrCnx_MouseMove(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    TriggerMouseOverObj Index, "ARRCNX_" & CStr(Index), "ARR"
End Sub

Private Sub picDepCnx_MouseMove(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    TriggerMouseOverObj Index, "DEPCNX_" & CStr(Index), "DEP"
End Sub

Private Sub RightScCon1_Click(Index As Integer)
    RightScale(0).ZOrder
End Sub

Private Sub TimeScaleTimer_Timer()
    Dim DisplayTime As String
    Dim DisplayDate As String
    Dim NewLeft As Long
    Dim LeftOff As Long
    Dim TimeOff As Long
    Dim RetVal As Boolean
    Dim CurLocTime
    Dim CurUtcTime
    Dim SecDiff
    
    'The time synchronization will be done based on the server time.
    'So we must calculate the differences of the elapsed time since the last timer event.
    CurLocTime = Now
    SecDiff = DateDiff("s", LastLocTimeCheck, CurLocTime)
    LastLocTimeCheck = CurLocTime
    ServerUtcTimeVal = DateAdd("s", SecDiff, ServerUtcTimeVal)
    LastUtcTimeCheck = ServerUtcTimeVal
    If chkUtc(0).Value = 1 Then
        DisplayTime = Format(LastUtcTimeCheck, "hh:mm:ss")
        DisplayDate = Format(LastUtcTimeCheck, "YYYYMMDD")
    Else
        DisplayTime = Format(LastLocTimeCheck, "hh:mm:ss")
        DisplayDate = Format(LastLocTimeCheck, "YYYYMMDD")
    End If
    DisplayDate = DecodeSsimDayFormat(DisplayDate, "CEDA", "SSIM2")
    lblCurTime(0).Caption = DisplayTime
    lblCurTime(0).ToolTipText = DisplayDate
    
    RetVal = GetTimeScalePos("", ServerUtcTimeVal, NewLeft)
    If RetVal = True Then
        NewLeft = NewLeft - 15
        TimeOff = NewLeft - TimeLine1(0).Left
        If TimeOff <> 0 Then
            lblCurTime(0).Visible = False
            TimeLine1(0).Visible = False
            TimeLine2(0).Visible = False
            LeftOff = TimeLine1(0).Left - lblCurTime(0).Left
            TimeLine1(0).Left = NewLeft
            TimeLine2(0).Left = NewLeft
            lblCurTime(0).Left = NewLeft - LeftOff
            If chkTime1(0).Value = 1 Then
                If HScroll2(0).Value < 3600 Then
                    HScroll2(0).Value = HScroll2(0).Value + 1
                End If
            End If
            TimeLine2(0).Visible = True
            TimeLine1(0).Visible = True
            lblCurTime(0).Visible = True
        End If
    Else
        lblCurTime(0).Visible = False
        TimeLine1(0).Visible = False
        TimeLine2(0).Visible = False
    End If
End Sub

Private Function GetTimeScalePos(CedaUtcTime As String, CedaTimeValue, ScalePos As Long) As Boolean
    Dim MinDiff As Long
    If CedaUtcTime <> "" Then CedaTimeValue = CedaFullDateToVb(CedaUtcTime)
    MinDiff = CLng(DateDiff("n", TimeScaleUtcBegin, CedaTimeValue))
    If (MinDiff >= 0) And (MinDiff <= CLng(lblMin1.UBound)) Then
        ScalePos = lblMin1(CInt(MinDiff)).Left
        GetTimeScalePos = True
    Else
        ScalePos = MinDiff * MinuteWidth
        GetTimeScalePos = False
    End If
End Function
Private Sub VScroll1_Change(Index As Integer)
    Dim NewValue As Long
    NewValue = CLng(VScroll1(Index).Value)
    AdjustVertScroll NewValue
    VScroll2(Index).Value = VScroll1(Index).Value
End Sub
Private Sub VScroll1_Scroll(Index As Integer)
    Dim NewValue As Long
    NewValue = CLng(VScroll1(Index).Value)
    AdjustVertScroll NewValue
    VScroll2(Index).Value = VScroll1(Index).Value
End Sub
Private Sub VScroll2_Change(Index As Integer)
    Dim NewValue As Long
    NewValue = CLng(VScroll2(Index).Value)
    AdjustVertScroll NewValue
    VScroll1(Index).Value = VScroll2(Index).Value
End Sub
Private Sub VScroll2_Scroll(Index As Integer)
    Dim NewValue As Long
    NewValue = CLng(VScroll2(Index).Value)
    AdjustVertScroll NewValue
    VScroll1(Index).Value = VScroll2(Index).Value
End Sub
Private Sub AdjustVertScroll(ScrollValue As Long)
    Dim NewTop As Long
    NewTop = ScrollValue * (ChartLine2(0).Height + 30)
    VScrollArea(0).Top = -NewTop
    WorkArea(0).Refresh
    CheckSelFlightPanels
End Sub

Private Sub CheckSelFlightPanels()
    Dim NewLeft As Long
    Dim ScrollValue As Integer
    Dim LinCnt As Integer
    Dim BotIdx As Integer
    Dim ShowPanel As Boolean
    ScrollValue = VScroll2(0).Value
    LinCnt = Int(WorkArea(0).Height / (ChartLine2(0).Height + 30))
    BotIdx = ScrollValue + LinCnt - 1
    If ArrCnxIdx >= 0 Then
        ShowPanel = False
        If chkSelDeco(2).Value = 0 Then
            NewLeft = ArrFlight(ArrCnxIdx).Left + HScrollArea(0).Left
            If NewLeft < 0 Then ShowPanel = True
            NewLeft = NewLeft - WorkArea(0).Width + ArrFlight(ArrCnxIdx).Width
            If NewLeft > 0 Then ShowPanel = True
        End If
        If ArrCnxIdx < ScrollValue Then
            picSelArrCursor(0).Picture = imgCurPosUp.Picture
            ShowPanel = True
        End If
        If ArrCnxIdx > BotIdx Then
            picSelArrCursor(0).Picture = imgCurPosDn.Picture
            ShowPanel = True
        End If
        SelArrPanel(0).Visible = ShowPanel
        If chkSelDeco(2).Value = 0 Then
            LeftScCon1(0).Visible = ShowPanel
            LeftScCon1(0).Refresh
        End If
    End If
End Sub
Private Sub ToggleFlightBarTimes(TimeOffs As Integer)
    Dim i As Integer
    For i = 0 To CurBarMax
        SetTimeFieldCaption lblArrStoa(i), TimeOffs
        SetTimeFieldCaption lblArrEtai(i), TimeOffs
        SetTimeFieldCaption lblArrOnbl(i), TimeOffs
        SetTimeFieldCaption lblDepStod(i), TimeOffs
        SetTimeFieldCaption lblDepEtdi(i), TimeOffs
        SetTimeFieldCaption lblDepOfbl(i), TimeOffs
    Next
    TabDepInfo(0).DateTimeSetUTCOffsetMinutes 2, TimeOffs
    TabDepInfo(0).Refresh
End Sub
Private Sub SetTimeFieldCaption(CurLbl As Label, TimeOffs As Integer)
    Dim tmpTag As String
    Dim tmpTxt As String
    Dim tmpTip As String
    Dim tmpDay As String
    Dim tmpTime
    tmpTag = CurLbl.Tag
    tmpTxt = ""
    tmpTip = GetItem(CurLbl.ToolTipText, 1, "[") & "  "
    If tmpTag <> "" Then
        tmpTime = CedaFullDateToVb(tmpTag)
        tmpTime = DateAdd("n", TimeOffs, tmpTime)
        tmpTxt = Format(tmpTime, "hh:mm")
        tmpDay = Format(tmpTime, "yyyymmdd")
        tmpDay = DecodeSsimDayFormat(tmpDay, "CEDA", "SSIM2")
        tmpTip = tmpTip & "[" & tmpDay & "]"
    End If
    CurLbl.Caption = tmpTxt
    CurLbl.ToolTipText = Trim(tmpTip)
End Sub

Public Sub GetFlightRotations()
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim DepLine As Long
    Dim ArrLine As Long
    Dim ArrRotLine As Long
    Dim DepRotLine As Long
    Dim iBar As Integer
    Dim jBar As Integer
    Dim tmpData As String
    Dim tmpAidx As String
    Dim tmpDidx As String
    Dim tmpRidx As String
    Dim tmpGidx As String
    Dim tmpView As String
    Dim ArrHitLine As String
    Dim DepHitLine As String
    Dim tmpHitLine As String
    Dim ArrUrno As String
    Dim ArrTifa As String
    Dim DepUrno As String
    Dim DepTifd As String
    Dim DepOfbl As String
    Dim TopPos As Long
    Dim ArrScalePos As Long
    Dim DepScalePos As Long
    Dim DepLeft As Long
    Dim ArrLeft As Long
    Dim RotWidth As Long
    Dim ArrWidth As Long
    Dim DepWidth As Long
    Dim MinDepWidth As Long
    Dim MaxDepWidth As Long
    Dim MinArrWidth As Long
    Dim MinRotWidth As Long
    Dim MaxArrWidth As Long
    Dim IdxColNo As Long
    Dim IsValidArrScalePos As Boolean
    Dim IsValidDepScalePos As Boolean
    Dim ArrVisible As Boolean
    Dim DepVisible As Boolean
    Dim GetFlights As Boolean
    Dim DepTifdValue
    Dim ArrTifaValue
    chkSelDeco(1).Value = 1
    chkSelDeco(0).Value = 1
    LeftScale(0).Refresh
    RightScale(0).Refresh
    CreateTimeScaleArea TimeScale(0)
    MaxDepWidth = MinuteWidth * 60
    MaxArrWidth = MinuteWidth * 60
    MinDepWidth = MinuteWidth * 20
    MinArrWidth = MinuteWidth * 20
    MinRotWidth = MinuteWidth * 15
    TabRotFlightsTab.IndexDestroy "CONX"
    iBar = 0
    MaxLine = TabRotFlightsTab.GetLineCount - 1
    For CurLine = 0 To MaxLine
        If iBar <= ChartLine2.UBound Then
            tmpView = "1"
            'tmpView = TabRotFlightsTab.GetFieldValue(CurLine, "VIEW")
            If tmpView = 1 Then
                ArrLine = -1
                DepLine = -1
                ArrRotLine = -1
                DepRotLine = -1
                GetFlights = False
                ArrHitLine = ""
                DepHitLine = ""
                'If GetFlights = False Then
                    ArrUrno = TabRotFlightsTab.GetFieldValue(CurLine, "AURN")
                    If ArrUrno <> "" Then
                        ArrHitLine = TabArrCnxPaxTab.GetLinesByIndexValue("AURN", ArrUrno, 0)
                        If ArrHitLine <> "" Then
                            GetFlights = True
                        End If
                    End If
                'End If
                'If GetFlights = False Then
                    DepUrno = TabRotFlightsTab.GetFieldValue(CurLine, "DURN")
                    If DepUrno <> "" Then
                        DepHitLine = TabDepCnxPaxTab.GetLinesByIndexValue("DURN", DepUrno, 0)
                        If DepHitLine <> "" Then
                            GetFlights = True
                        End If
                    End If
                'End If
        'GetFlights = True
                If GetFlights = True Then
                    tmpRidx = TabRotFlightsTab.GetFieldValue(CurLine, "RIDX")
                    tmpHitLine = TabArrFlightsTab.GetLinesByIndexValue("RIDX", tmpRidx, 0)
                    If tmpHitLine <> "" Then ArrRotLine = Val(tmpHitLine)
                    tmpHitLine = TabDepFlightsTab.GetLinesByIndexValue("RIDX", tmpRidx, 0)
                    If tmpHitLine <> "" Then DepRotLine = Val(tmpHitLine)
                    If ArrRotLine >= 0 Then
                        tmpAidx = TabRotFlightsTab.GetFieldValue(CurLine, "AIDX")
                        tmpData = TabArrFlightsTab.GetFieldValue(ArrRotLine, "'S3'")
                        If tmpAidx = tmpData Then
                            ArrLine = ArrRotLine
                        Else
                            'Here we have a problem
                            ArrLine = ArrRotLine
                        End If
                    End If
                    If DepRotLine >= 0 Then
                        tmpDidx = TabRotFlightsTab.GetFieldValue(CurLine, "DIDX")
                        tmpData = TabDepFlightsTab.GetFieldValue(DepRotLine, "'S3'")
                        If tmpDidx = tmpData Then
                            DepLine = DepRotLine
                        Else
                            'Here we have a problem
                            DepLine = DepRotLine
                        End If
                    End If
                    tmpGidx = Right("000000" & CStr(iBar), 6)
                    GetArrFlightData ArrLine, iBar
                    GetArrConnexTrigger ArrLine, iBar, ArrHitLine
                    GetDepFlightData DepLine, iBar
                    GetDepConnexTrigger DepLine, iBar, DepHitLine
                    If (ArrLine >= 0) And (DepLine >= 0) Then
                        'ROTATION
                        DepTifd = TabRotFlightsTab.GetFieldValue(CurLine, "TIFD")
                        IsValidDepScalePos = GetTimeScalePos(DepTifd, DepTifdValue, DepScalePos)
                        ArrTifa = TabRotFlightsTab.GetFieldValue(CurLine, "TIFA")
                        IsValidArrScalePos = GetTimeScalePos(ArrTifa, ArrTifaValue, ArrScalePos)
                        
                        RotWidth = DepScalePos - ArrScalePos + 15
                        If RotWidth >= MinRotWidth Then
                            lblBarLineColor(iBar).BackColor = ChartBarBackColor
                            lblBarLineColor(iBar).Tag = "7"
                        Else
                            lblBarLineColor(iBar).BackColor = vbRed
                            lblBarLineColor(iBar).Tag = "1"
                        End If
                        
                        ArrWidth = RotWidth \ 2
                        DepWidth = RotWidth - ArrWidth
                        If DepWidth > MaxDepWidth Then DepWidth = MaxDepWidth
                        If DepWidth < MinDepWidth Then DepWidth = MinDepWidth
                        DepLeft = DepScalePos - DepWidth + 15
                        ArrWidth = RotWidth
                        If ArrWidth < MinArrWidth Then ArrWidth = MinArrWidth
                        ArrLeft = ArrScalePos
                        ArrVisible = True
                        DepVisible = True
                        lblArrBar(iBar).Tag = "Y"
                        lblDepBar(iBar).Tag = "Y"
                    ElseIf DepLine >= 0 Then
                        'SINGLE DEPARTURE
                        DepTifd = TabRotFlightsTab.GetFieldValue(CurLine, "TIFD")
                        IsValidDepScalePos = GetTimeScalePos(DepTifd, DepTifdValue, DepScalePos)
                        DepWidth = MaxDepWidth
                        DepLeft = DepScalePos - DepWidth
                        ArrLeft = 0
                        ArrWidth = 150
                        ArrVisible = False
                        DepVisible = True
                        lblArrBar(iBar).Tag = "N"
                        lblDepBar(iBar).Tag = "Y"
                    Else
                        'SINGLE ARRIVAL
                        ArrTifa = TabRotFlightsTab.GetFieldValue(CurLine, "TIFA")
                        IsValidArrScalePos = GetTimeScalePos(ArrTifa, ArrTifaValue, ArrScalePos)
                        ArrWidth = MaxArrWidth
                        ArrLeft = ArrScalePos
                        DepLeft = 0
                        DepWidth = 150
                        ArrVisible = True
                        DepVisible = False
                        lblArrBar(iBar).Tag = "Y"
                        lblDepBar(iBar).Tag = "N"
                    End If
                    lblArrBar(iBar).Width = ArrWidth
                    lblDepBar(iBar).Width = DepWidth
                    lblDepBar(iBar).Left = DepLeft
                    lblArrBar(iBar).Left = ArrLeft
                    lblArrBar(iBar).Visible = ArrVisible
                    lblDepBar(iBar).Visible = DepVisible
                    lblDepBar(iBar).ZOrder
                    ArrFlight(iBar).Visible = ArrVisible
                    DepFlight(iBar).Visible = DepVisible
                    'tmpGidx = tmpView & "," & tmpGidx
                    'TabRotFlightsTab.SetFieldValues CurLine, "VIEW,CONX", tmpGidx
                    'TabRotFlightsTab.SetLineColor CurLine, vbBlack, vbWhite
                    TabRotFlightsTab.SetFieldValues CurLine, "CONX", tmpGidx
                    ChartLine2(iBar).BackColor = lblBarLineColor(iBar).BackColor
                    iBar = iBar + 1
                Else
                    'TabRotFlightsTab.SetFieldValues CurLine, "VIEW,CONX", tmpView & ",------"
                    'TabRotFlightsTab.SetLineColor CurLine, vbBlack, LightGray
                    TabRotFlightsTab.SetFieldValues CurLine, "CONX", "------"
                End If
            Else
                'TabRotFlightsTab.SetFieldValues CurLine, "VIEW,CONX", tmpView & ",------"
                'TabRotFlightsTab.SetLineColor CurLine, vbBlack, LightGray
                TabRotFlightsTab.SetFieldValues CurLine, "CONX", "------"
            End If
        Else
            'tmpView = "0"
            'TabRotFlightsTab.SetFieldValues CurLine, "VIEW,CONX", tmpView & ",------"
            'TabRotFlightsTab.SetLineColor CurLine, vbBlack, LightGray
            TabRotFlightsTab.SetFieldValues CurLine, "CONX", "------"
        End If
    Next
    jBar = iBar - 1
    If jBar < 0 Then
        jBar = 0
    End If
    TopPos = ChartLine2(jBar).Top + ChartLine2(0).Height
    CurChartHeight = TopPos + 90
    VScrollArea(0).Height = CurChartHeight
    HScrollArea(0).Height = CurChartHeight
    LeftScale(0).Height = CurChartHeight
    MidScConx(0).Height = CurChartHeight
    RightScale(0).Height = CurChartHeight
    TimeLine2(0).Height = CurChartHeight
    RangeLine(0).Height = CurChartHeight
    RangeLine(1).Height = CurChartHeight
    VScroll1(0).Max = jBar
    VScroll2(0).Max = jBar
    
    TabRotFlightsTab.AutoSizeColumns
    IdxColNo = CLng(GetRealItemNo(TabRotFlightsTab.LogicalFieldList, "CONX"))
    TabRotFlightsTab.IndexCreate "CONX", IdxColNo
    TabRotFlightsTab.Refresh
    Me.Refresh
    'chkSelDeco(1).Value = 0
    'chkSelDeco(0).Value = 0
    LeftScale(0).Refresh
    RightScale(0).Refresh
    Me.Refresh
    CnxRefresh.Enabled = False
    If ChartIsReadyForUse = False Then
        ChartIsReadyForUse = True
        CurBarIdx = -1
        HighlightCurrentBar 0, ChartName
        CnxFilterAdid = 0
        CnxFilterType = 0
        CnxFilterClass = 0
        CnxFilterShort = 0
        CnxFilterCritical = 0
        DontRefreshCnx = True
        chkCnxAdid(0).Value = 1
        chkCnxType(0).Value = 0
        DontRefreshCnx = False
        ShowAllArrPaxConnex
    Else
        RefreshCurrentCnxChart
    End If
End Sub

Private Sub GetArrFlightData(LineNo As Long, BarIdx As Integer)
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim BgnLine As Long
    Dim iBar As Integer
    Dim ArrFields As String
    Dim FldName As String
    Dim FldValue As String
    Dim tmpTag As String
    Dim ArrFldIdx As Integer
    Dim TimeOffs As Integer
    Dim BarColor As Long
    Dim FldColor As Long
    If chkUtc(1).Value = 1 Then
        TimeOffs = UtcTimeDiff
    Else
        TimeOffs = 0
    End If
    If LineNo >= 0 Then
        BgnLine = LineNo
        MaxLine = LineNo
        iBar = BarIdx
    Else
        If BarIdx >= 0 Then
            iBar = BarIdx
            'ClearArrFlight iBar
            BgnLine = 0
            MaxLine = -1
        Else
            BgnLine = 0
            MaxLine = TabArrFlightsTab.GetLineCount - 1
            iBar = 0
        End If
    End If
    ArrFields = "FTYP,FLNO,FLTI,ORG3,VIA3,STOA,ETAI,ONBL,PSTA,GTA1,REGN,ACT3,TIFA,TISA,'C3'"
    For CurLine = BgnLine To MaxLine
        ClearArrFlight iBar
        ArrFldIdx = 1
        FldName = "START"
        While FldName <> ""
            FldName = GetItem(ArrFields, ArrFldIdx, ",")
            If FldName <> "" Then
                FldValue = Trim(TabArrFlightsTab.GetFieldValue(CurLine, FldName))
                Select Case FldName
                    Case "FTYP"
                        'lblArrFtyp(iBar).Tag = FldValue
                        'lblArrFtyp(iBar).Caption = FldValue
                        'Select Case FldValue
                        '    Case "X"
                        '        lblArrBar(iBar).Caption = "CXX"
                        '        lblArrBar(iBar).Width = 600
                        '        BarColor = LightGrey
                        '    Case "N"
                        '        lblArrBar(iBar).Caption = "NOP"
                        '        lblArrBar(iBar).Width = 600
                        '        BarColor = LightGrey
                        '    Case "D"
                        '        lblArrBar(iBar).Caption = "DIV"
                        '        lblArrBar(iBar).Width = 600
                        '        BarColor = LightGrey
                        '    Case Else
                        '        lblArrBar(iBar).Caption = ""
                        '        'BarColor = LightGrey
                        'End Select
                    Case "FLNO"
                        lblArrFlno(iBar).Tag = FldValue
                        lblArrFlno(iBar).Caption = FldValue
                    Case "FLTI"
                        lblArrFlti(iBar).Tag = FldValue
                        lblArrFlti(iBar).Caption = FldValue
                    Case "ORG3"
                        lblArrOrg3(iBar).Tag = FldValue
                        lblArrOrg3(iBar).Caption = FldValue
                    Case "VIA3"
                        lblArrVia3(iBar).Tag = FldValue
                        lblArrVia3(iBar).Caption = FldValue
                    Case "STOA"
                        lblArrStoa(iBar).Tag = FldValue
                        SetTimeFieldCaption lblArrStoa(iBar), TimeOffs
                        If FldValue <> "" Then BarColor = LightGrey
                    Case "ETAI"
                        lblArrEtai(iBar).Tag = FldValue
                        SetTimeFieldCaption lblArrEtai(iBar), TimeOffs
                        If FldValue <> "" Then BarColor = vbWhite
                    Case "ONBL"
                        lblArrOnbl(iBar).Tag = FldValue
                        SetTimeFieldCaption lblArrOnbl(iBar), TimeOffs
                        If FldValue <> "" Then BarColor = vbGreen
                    Case "PSTA"
                        lblArrPsta(iBar).Tag = FldValue
                        lblArrPsta(iBar).Caption = FldValue
                    Case "GTA1"
                        'lblArrGta1(iBar).Tag = FldValue
                        'lblArrGta1(iBar).Caption = FldValue
                    Case "REGN"
                        lblArrRegn(iBar).Tag = FldValue
                        lblArrRegn(iBar).Caption = FldValue
                    Case "ACT3"
                        lblArrAct3(iBar).Tag = FldValue
                        lblArrAct3(iBar).Caption = FldValue
                    Case "TIFA"
                        tmpTag = "{AFTTIFA}" & FldValue & "{/AFTTIFA}"
                        ArrFlight(iBar).Tag = tmpTag
                    Case "TISA"
                        tmpTag = ArrFlight(iBar).Tag
                        tmpTag = tmpTag & "{AFTTISA}" & FldValue & "{/AFTTISA}"
                        ArrFlight(iBar).Tag = tmpTag
                    Case "'C3'"
                        If FldValue = "S" Then
                            picArrStatus(iBar).Picture = picMarker(1).Picture
                            picArrStatus(iBar).Tag = "1"
                        Else
                            picArrStatus(iBar).Picture = picMarker(0).Picture
                            picArrStatus(iBar).Tag = "0"
                        End If
                    Case Else
                End Select
            End If
            ArrFldIdx = ArrFldIdx + 1
        Wend
        'lblArrBar(iBar).BackColor = BarColor
        ArrFlight(iBar).Visible = True
        iBar = iBar + 1
    Next
End Sub
Private Sub GetArrConnexTrigger(ArrLineNo As Long, iBar As Integer, HitLines As String)
    Dim tmpLineNo As String
    Dim tmpData As String
    Dim tmpTifa As String
    Dim tmpTag As String
    Dim LineNo As Long
    Dim CurCTim As Long
    Dim MinCtim As Long
    Dim PaxTot1 As Integer
    Dim PaxTot2 As Integer
    Dim PaxTot3 As Integer
    Dim PaxTots As Integer
    Dim IsShort As Boolean
    Dim IsCritical As Boolean
    Dim iIdx As Integer
    'If HitLines <> "" Then
        lblArrCsct(iBar).Tag = "CNX"
    'Else
        IsShort = False
        MinCtim = 999999
        PaxTot1 = 0
        PaxTot2 = 0
        PaxTot3 = 0
        iIdx = 0
        tmpTifa = ""
        tmpLineNo = "START"
        While tmpLineNo <> ""
            iIdx = iIdx + 1
            tmpLineNo = GetItem(HitLines, iIdx, ",")
            If tmpLineNo <> "" Then
                LineNo = Val(tmpLineNo)
                tmpData = TabArrCnxPaxTab.GetFieldValue(LineNo, "TCLF")
                PaxTot1 = PaxTot1 + Val(tmpData)
                tmpData = TabArrCnxPaxTab.GetFieldValue(LineNo, "TCLC")
                PaxTot2 = PaxTot2 + Val(tmpData)
                tmpData = TabArrCnxPaxTab.GetFieldValue(LineNo, "TCLY")
                PaxTot3 = PaxTot3 + Val(tmpData)
                tmpData = TabArrCnxPaxTab.GetFieldValue(LineNo, "SCID")
                If tmpData = "X" Then IsShort = True
                If tmpData = "C" Then IsCritical = True
                tmpData = TabArrCnxPaxTab.GetFieldValue(LineNo, "CTMS")
                If tmpData <> "" Then
                    CurCTim = Val(tmpData)
                    If (CurCTim > 0) And (CurCTim < MinCtim) Then MinCtim = CurCTim
                End If
                tmpData = TabArrCnxPaxTab.GetFieldValue(LineNo, "CTMT")
                If tmpData <> "" Then
                    CurCTim = Val(tmpData)
                    If (CurCTim > 0) And (CurCTim < MinCtim) Then MinCtim = CurCTim
                    If CurCTim <= 0 Then IsCritical = True
                End If
                If tmpTifa = "" Then
                    tmpTag = ArrFlight(iBar).Tag
                    tmpTifa = TabArrCnxPaxTab.GetFieldValue(LineNo, "TIFA")
                    If tmpTifa = "" Then
                        GetKeyItem tmpTifa, tmpTag, "{AFTTIFA}", "{/AFTTIFA}"
                        tmpTag = tmpTag & "{CFITIFA}" & tmpTifa & "{/CFITIFA}"
                        ArrFlight(iBar).Tag = tmpTag
                    End If
                End If
            End If
        Wend
        PaxTots = PaxTot1 + PaxTot2 + PaxTot3

        lblArrTotPax(iBar).Tag = CStr(PaxTots)
        If PaxTots > 0 Then
            lblArrTotPax(iBar).Caption = lblArrTotPax(iBar).Tag
            lblArrTotPax(iBar).MousePointer = 99
        Else
            lblArrTotPax(iBar).Caption = ""
            lblArrTotPax(iBar).MousePointer = 0
        End If

        lblArrTot1(iBar).Tag = CStr(PaxTot1)
        If PaxTot1 > 0 Then
            lblArrTot1(iBar).Caption = lblArrTot1(iBar).Tag
            lblArrTot1(iBar).MousePointer = 99
        Else
            lblArrTot1(iBar).Caption = ""
            lblArrTot1(iBar).MousePointer = 0
        End If
        lblArrTot2(iBar).Tag = CStr(PaxTot2)
        If PaxTot2 > 0 Then
            lblArrTot2(iBar).Caption = lblArrTot2(iBar).Tag
            lblArrTot2(iBar).MousePointer = 99
        Else
            lblArrTot2(iBar).Caption = ""
            lblArrTot2(iBar).MousePointer = 0
        End If
        lblArrTot3(iBar).Tag = CStr(PaxTot3)
        If PaxTot3 > 0 Then
            lblArrTot3(iBar).Caption = lblArrTot3(iBar).Tag
            lblArrTot3(iBar).MousePointer = 99
        Else
            lblArrTot3(iBar).Caption = ""
            lblArrTot3(iBar).MousePointer = 0
        End If

        lblArrTotBag(iBar).Tag = ""
        lblArrTotBag(iBar).Caption = lblArrTotBag(iBar).Tag
        lblArrTotBag(iBar).MousePointer = 0

        lblArrTotUld(iBar).Tag = ""
        lblArrTotUld(iBar).Caption = lblArrTotBag(iBar).Tag
        lblArrTotUld(iBar).MousePointer = 0

        lblArrCsct(iBar).BackColor = vbWhite
        lblArrCsct(iBar).ForeColor = vbBlack
        lblArrCsct(iBar).Caption = ""
        lblArrCsct(iBar).MousePointer = 0
        If MinCtim < 999999 Then
            lblArrCsct(iBar).Caption = CStr(MinCtim)
            If IsShort Then
                lblArrCsct(iBar).BackColor = vbRed
                lblArrCsct(iBar).ForeColor = vbWhite
                lblArrCsct(iBar).MousePointer = 99
            End If
            If IsCritical Then
                lblArrCsct(iBar).BackColor = vbCyan
                lblArrCsct(iBar).ForeColor = vbBlack
                lblArrCsct(iBar).MousePointer = 99
            End If
        End If
    'End If
End Sub

Private Sub GetDepFlightData(LineNo As Long, BarIdx As Integer)
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim BgnLine As Long
    Dim iBar As Integer
    Dim DepFields As String
    Dim FldName As String
    Dim FldValue As String
    Dim DepFldIdx As Integer
    Dim TimeOffs As Integer
    If chkUtc(1).Value = 1 Then
        TimeOffs = UtcTimeDiff
    Else
        TimeOffs = 0
    End If
    If LineNo >= 0 Then
        BgnLine = LineNo
        MaxLine = LineNo
        iBar = BarIdx
    Else
        If BarIdx >= 0 Then
            iBar = BarIdx
            'ClearDepFlight iBar
            BgnLine = 0
            MaxLine = -1
        Else
            BgnLine = 0
            MaxLine = TabDepFlightsTab.GetLineCount - 1
            iBar = 0
        End If
    End If
    DepFields = "FTYP,FLNO,FLTI,DES3,VIA3,STOD,ETDI,OFBL,PSTD,GTD1,REGN,ACT3,'C3'"
    For CurLine = BgnLine To MaxLine
        ClearDepFlight iBar
        DepFldIdx = 1
        FldName = "START"
        While FldName <> ""
            FldName = GetItem(DepFields, DepFldIdx, ",")
            If FldName <> "" Then
                FldValue = TabDepFlightsTab.GetFieldValue(CurLine, FldName)
                Select Case FldName
                    Case "FTYP"
                        'lblDepFtyp(iBar).Tag = FldValue
                        'lblDepFtyp(iBar).Caption = FldValue
                    Case "FLNO"
                        lblDepFlno(iBar).Tag = FldValue
                        lblDepFlno(iBar).Caption = FldValue
                    Case "FLTI"
                        lblDepFlti(iBar).Tag = FldValue
                        lblDepFlti(iBar).Caption = FldValue
                    Case "DES3"
                        lblDepDes3(iBar).Tag = FldValue
                        lblDepDes3(iBar).Caption = FldValue
                    Case "VIA3"
                        lblDepVia3(iBar).Tag = FldValue
                        lblDepVia3(iBar).Caption = FldValue
                    Case "STOD"
                        lblDepStod(iBar).Tag = FldValue
                        SetTimeFieldCaption lblDepStod(iBar), TimeOffs
                    Case "ETDI"
                        lblDepEtdi(iBar).Tag = FldValue
                        SetTimeFieldCaption lblDepEtdi(iBar), TimeOffs
                    Case "OFBL"
                        lblDepOfbl(iBar).Tag = FldValue
                        SetTimeFieldCaption lblDepOfbl(iBar), TimeOffs
                    Case "PSTD"
                        lblDepPstd(iBar).Tag = FldValue
                        lblDepPstd(iBar).Caption = FldValue
                    Case "GTD1"
                        'lblDepGtd1(iBar).Tag = FldValue
                        'lblDepGtd1(iBar).Caption = FldValue
                    Case "REGN"
                        lblDepRegn(iBar).Tag = FldValue
                        lblDepRegn(iBar).Caption = FldValue
                    Case "ACT3"
                        lblDepAct3(iBar).Tag = FldValue
                        lblDepAct3(iBar).Caption = FldValue
                    Case "'C3'"
                        If FldValue = "S" Then
                            picDepStatus(iBar).Picture = picMarker(1).Picture
                            picDepStatus(iBar).Tag = "1"
                        Else
                            picDepStatus(iBar).Picture = picMarker(0).Picture
                            picDepStatus(iBar).Tag = "0"
                        End If
                    Case Else
                End Select
            End If
            DepFldIdx = DepFldIdx + 1
        Wend
        DepFlight(iBar).Visible = True
        iBar = iBar + 1
    Next
End Sub
Private Sub GetDepConnexTrigger(DepLineNo As Long, iBar As Integer, HitLines As String)
    Dim tmpLineNo As String
    Dim tmpData As String
    Dim LineNo As Long
    Dim CurCTim As Long
    Dim MinCtim As Long
    Dim PaxTot1 As Integer
    Dim PaxTot2 As Integer
    Dim PaxTot3 As Integer
    Dim PaxTots As Integer
    Dim IsShort As Boolean
    Dim iIdx As Integer
    'If HitLines <> "" Then
    'Else
        IsShort = False
        MinCtim = 999999
        PaxTot1 = 0
        PaxTot2 = 0
        PaxTot3 = 0
        iIdx = 0
        tmpLineNo = "START"
        While tmpLineNo <> ""
            iIdx = iIdx + 1
            tmpLineNo = GetItem(HitLines, iIdx, ",")
            If tmpLineNo <> "" Then
                LineNo = Val(tmpLineNo)
                tmpData = TabDepCnxPaxTab.GetFieldValue(LineNo, "TCLF")
                PaxTot1 = PaxTot1 + Val(tmpData)
                tmpData = TabDepCnxPaxTab.GetFieldValue(LineNo, "TCLC")
                PaxTot2 = PaxTot2 + Val(tmpData)
                tmpData = TabDepCnxPaxTab.GetFieldValue(LineNo, "TCLY")
                PaxTot3 = PaxTot3 + Val(tmpData)
                tmpData = TabDepCnxPaxTab.GetFieldValue(LineNo, "SCID")
                If tmpData <> "" Then IsShort = True
                tmpData = TabDepCnxPaxTab.GetFieldValue(LineNo, "CTMS")
                If tmpData <> "" Then
                    CurCTim = Val(tmpData)
                    If (CurCTim > 0) And (CurCTim < MinCtim) Then MinCtim = CurCTim
                End If
                tmpData = TabDepCnxPaxTab.GetFieldValue(LineNo, "CTMT")
                If tmpData <> "" Then
                    CurCTim = Val(tmpData)
                    If (CurCTim > 0) And (CurCTim < MinCtim) Then MinCtim = CurCTim
                    If CurCTim <= 0 Then IsShort = True
                End If
            End If
        Wend
        PaxTots = PaxTot1 + PaxTot2 + PaxTot3
        If PaxTots > 0 Then
            lblDepTpax(iBar).Caption = CStr(PaxTots)
            lblDepTpax(iBar).BackColor = LightGreen
        Else
            lblDepTpax(iBar).Caption = ""
            lblDepTpax(iBar).BackColor = LightGrey
        End If
        If PaxTot1 > 0 Then
            lblDepTot1(iBar).Caption = CStr(PaxTot1)
        Else
            lblDepTot1(iBar).Caption = ""
        End If
        If PaxTot2 > 0 Then
            lblDepTot2(iBar).Caption = CStr(PaxTot2)
        Else
            lblDepTot2(iBar).Caption = ""
        End If
        If PaxTot3 > 0 Then
            lblDepTot3(iBar).Caption = CStr(PaxTot3)
        Else
            lblDepTot3(iBar).Caption = ""
        End If
        lblDepTotPax(iBar).Caption = lblDepTpax(iBar).Caption
        lblDepCsct(iBar).BackColor = vbWhite
        lblDepCsct(iBar).ForeColor = vbBlack
        If MinCtim < 999999 Then
            lblDepCsct(iBar).Caption = CStr(MinCtim)
            If IsShort Then
                lblDepCsct(iBar).BackColor = vbRed
                lblDepCsct(iBar).ForeColor = vbWhite
            End If
        Else
            lblDepCsct(iBar).Caption = ""
        End If
        lblDepCsct(iBar).Tag = "CNX"
    'End If
End Sub

Private Sub ClearArrFlight(iBar As Integer)
    Dim FldValue As String
    FldValue = ""
    'lblArrFtyp(iBar).Tag = FldValue
    'lblArrFtyp(iBar).Caption = FldValue
    lblArrFlno(iBar).Tag = FldValue
    lblArrFlno(iBar).Caption = FldValue
    lblArrFlti(iBar).Tag = FldValue
    lblArrFlti(iBar).Caption = FldValue
    lblArrOrg3(iBar).Tag = FldValue
    lblArrOrg3(iBar).Caption = FldValue
    lblArrVia3(iBar).Tag = FldValue
    lblArrVia3(iBar).Caption = FldValue
    lblArrStoa(iBar).Tag = FldValue
    lblArrStoa(iBar).Caption = FldValue
    lblArrEtai(iBar).Tag = FldValue
    lblArrEtai(iBar).Caption = FldValue
    lblArrOnbl(iBar).Tag = FldValue
    lblArrOnbl(iBar).Caption = FldValue
    lblArrPsta(iBar).Tag = FldValue
    lblArrPsta(iBar).Caption = FldValue
    'lblArrGta1(iBar).Tag = FldValue
    'lblArrGta1(iBar).Caption = FldValue
    lblArrRegn(iBar).Tag = FldValue
    lblArrRegn(iBar).Caption = FldValue
    lblArrAct3(iBar).Tag = FldValue
    lblArrAct3(iBar).Caption = FldValue
    lblArrTpax(iBar).Tag = FldValue
    lblArrTpax(iBar).Caption = FldValue
    lblArrTbag(iBar).Tag = FldValue
    lblArrTbag(iBar).Caption = FldValue
    lblArrTuld(iBar).Tag = FldValue
    lblArrTuld(iBar).Caption = FldValue
End Sub

Private Sub ClearDepFlight(iBar As Integer)
    Dim FldValue As String
    FldValue = ""
    'lblDepFtyp(iBar).Tag = FldValue
    'lblDepFtyp(iBar).Caption = FldValue
    lblDepFlno(iBar).Tag = FldValue
    lblDepFlno(iBar).Caption = FldValue
    lblDepFlti(iBar).Tag = FldValue
    lblDepFlti(iBar).Caption = FldValue
    lblDepDes3(iBar).Tag = FldValue
    lblDepDes3(iBar).Caption = FldValue
    lblDepVia3(iBar).Tag = FldValue
    lblDepVia3(iBar).Caption = FldValue
    lblDepStod(iBar).Tag = FldValue
    lblDepStod(iBar).Caption = FldValue
    lblDepEtdi(iBar).Tag = FldValue
    lblDepEtdi(iBar).Caption = FldValue
    lblDepOfbl(iBar).Tag = FldValue
    lblDepOfbl(iBar).Caption = FldValue
    lblDepPstd(iBar).Tag = FldValue
    lblDepPstd(iBar).Caption = FldValue
    'lblDepGtd1(iBar).Tag = FldValue
    'lblDepGtd1(iBar).Caption = FldValue
    lblDepRegn(iBar).Tag = FldValue
    lblDepRegn(iBar).Caption = FldValue
    lblDepAct3(iBar).Tag = FldValue
    lblDepAct3(iBar).Caption = FldValue
    lblDepTpax(iBar).Tag = FldValue
    lblDepTpax(iBar).Caption = FldValue
    lblDepTbag(iBar).Tag = FldValue
    lblDepTbag(iBar).Caption = FldValue
    lblDepTuld(iBar).Tag = FldValue
    lblDepTuld(iBar).Caption = FldValue
End Sub

Private Sub ShowArrConnexDetails(Index As Integer)
    Dim iArrBar As Integer
    Dim iDepBar As Integer
    Dim ArrUrno As String
    Dim DepUrno As String
    Dim CurConx As String
    Dim HitLine As String
    Dim ArrRotLine As Long
    Dim DepRotLine As Long
    Dim CfiLine As Long
    Dim LinColor As Long
    Static LastColor As Long
    Dim NewLeft As Long
    Dim NewWidth As Long
    Dim MaxWidth As Long
    Dim CurCTim As Long
    Dim IsShort As Boolean
    Dim tmpData As String
    Dim tmpArrTag As String
    
    Dim ArrTagTXml As String
    Dim ArrTagTPax As String
    
    Dim ArrTagTAll As String
    
    Dim tmpDepTag As String
    
    Dim tmpCnxTag As String
    
    'Dim CurValue As Integer
    
    'Counters FlightPanel ARR
    'Dim ArrTPax As Integer
    
    'Variables of the connection record of CFITAB
    Dim CfiPaxTot0 As Integer   'Total PAX
    Dim CfiPaxTot1 As Integer   'Class F
    Dim CfiPaxTot2 As Integer   'Class C
    Dim CfiPaxTot3 As Integer   'Class Y
    Dim CfiPaxTot4 As Integer   'Class W
    
    'Totals per connection ARR->DEP
    'We must collect totals because in CFITAB
    'might exist more than one record per connection
    'because of CodeShare PAX with different
    'Flight Numbers on the same AFTTAB departure
    Dim CnxPaxTot0 As Integer   'Total PAX
    Dim CnxPaxTot1 As Integer   'Class F
    Dim CnxPaxTot2 As Integer   'Class C
    Dim CnxPaxTot3 As Integer   'Class Y
    Dim CnxPaxTot4 As Integer   'Class W
    
    'Totals per ARR PAX connex
    Dim ArrPaxTot0 As Integer
    Dim ArrPaxTot1 As Integer
    Dim ArrPaxTot2 As Integer
    Dim ArrPaxTot3 As Integer
    Dim ArrPaxTot4 As Integer
    
    
    Dim ArrTBag As Integer
    Dim ArrTUld As Integer
    
    Dim CurTpax As Integer
    
    Dim ErrCase As Integer
    Dim ErrCnt1 As Integer
    Dim ErrCnt2 As Integer
    
    Dim X0 As Long
    Dim X1 As Long
    Dim X2 As Long
    Dim X3 As Long
    Dim X4 As Long
    Dim Y1A As Long
    Dim Y1B As Long
    Dim Y2A As Long
    Dim Y2B As Long
    Dim Y1Min As Long
    Dim Y1Max As Long
    
    Dim ShowDep As Boolean
    Dim ShowArr As Boolean

    If CurCnxLayoutHours < 1 Then CurCnxLayoutHours = 5
    ArrCnxIdx = -1
    ShowArr = False
    ShowDep = False
    
    If (CnxLayoutType >= 0) And (Index >= 0) Then
        If CnxLayoutType > 0 Then
            ClearConnexDetails
            CurCnxView = 1
        Else
            CurCnxView = 0
        End If
        ComposeChartTitle
        
        iArrBar = Index
        Y1A = ArrLblPanel(iArrBar).Top + (ArrLblPanel(iArrBar).Height \ 2)
        Y1B = Y1A - 15
        Y2A = Y1A
        Y2B = Y1B
        X3 = MidScConx(0).Width
        Y1Min = Y1A
        Y1Max = Y1A
        Select Case CnxLayoutType
            Case 0, 1
                'ArrLblPanel(iArrBar).Visible = True
                LeftTime(0).Left = ArrLblPanel(iArrBar).Left + ArrLblPanel(iArrBar).Width + 150
                X1 = LeftTime(0).Left
                X0 = X1
                picArrCnx(iArrBar).Left = X0 - 120
                'picArrCnx(iArrBar).Visible = True
                'MidScConx(0).Line (0, Y1A)-(X0, Y2A), vbBlack
                'MidScConx(0).Line (0, Y1B)-(X0, Y2B), vbBlack
                ArrCnxInfo(iArrBar).Left = LeftTime(0).Left + 180
                ArrCnxInfo(iArrBar).Top = Y1A - (ArrCnxInfo(iArrBar).Height \ 2)
                X2 = X3 - 600
                X4 = X3 - 600
            Case 2
                ArrLblPanel(iArrBar).Visible = True
                LeftTime(0).Left = ArrLblPanel(iArrBar).Left + ArrLblPanel(iArrBar).Width + 150
                X1 = LeftTime(0).Left
                X0 = X1
                'Calculation: 6Hours * 60Min * 15Twips
                CnxRangeMin = CurCnxLayoutHours * 60
                NewLeft = LeftTime(0).Left + (CnxRangeMin * 15)
                RightTime(0).Left = NewLeft
                picArrCnx(iArrBar).Left = X1 - 120
                picArrCnx(iArrBar).Visible = True
                MidScConx(0).Line (0, Y1A)-(X1, Y1A), vbBlack
                MidScConx(0).Line (0, Y1B)-(X1, Y1B), vbBlack
                X2 = LeftTime(0).Left - 120
                ArrCnxInfo(iArrBar).Left = LeftTime(0).Left + 180
                ArrCnxInfo(iArrBar).Top = Y1A - (ArrCnxInfo(iArrBar).Height \ 2)
                X4 = X3 - 600
            Case 3
                ArrLblPanel(iArrBar).Visible = True
                LeftTime(0).Left = ArrLblPanel(iArrBar).Left + ArrLblPanel(iArrBar).Width + 150
                X1 = LeftTime(0).Left
                X0 = X1
                'Calculation: 6Hours * 60Min * 15Twips
                CnxRangeMin = CurCnxLayoutHours * 60
                NewLeft = LeftTime(0).Left + (CnxRangeMin * 15)
                RightTime(0).Left = NewLeft
                picArrCnx(iArrBar).Left = X1 - 120
                picArrCnx(iArrBar).Visible = True
                MidScConx(0).Line (0, Y1A)-(X1, Y1A), vbBlack
                MidScConx(0).Line (0, Y1B)-(X1, Y1B), vbBlack
                X2 = LeftTime(0).Left - 120
                ArrCnxInfo(iArrBar).Left = LeftTime(0).Left + 180
                ArrCnxInfo(iArrBar).Top = Y1A - (ArrCnxInfo(iArrBar).Height \ 2)
                ArrCnxInfo(iArrBar).Visible = True
                MidScConx(0).Line (X1, Y1A)-(ArrCnxInfo(iArrBar).Left, Y1A), vbBlack
                MidScConx(0).Line (X1, Y1B)-(ArrCnxInfo(iArrBar).Left, Y1B), vbBlack
                X4 = X3 - 600
            Case 4
                ArrLblPanel(iArrBar).Visible = True
                LeftTime(0).Left = ArrLblPanel(iArrBar).Left + ArrLblPanel(iArrBar).Width + 150 + (1 * (60 * 15)) 'One Hour
                X1 = LeftTime(0).Left
                X0 = ArrLblPanel(iArrBar).Left + ArrLblPanel(iArrBar).Width + 150
                'Calculation: 5Hours * 60Min * 15Twips
                CnxRangeMin = CurCnxLayoutHours * 60
                NewLeft = LeftTime(0).Left + (CnxRangeMin * 15)
                RightTime(0).Left = NewLeft
                picArrCnx(iArrBar).Left = X0 - 120
                picArrCnx(iArrBar).Visible = True
                MidScConx(0).Line (0, Y1A)-(X0, Y1A), vbBlack
                MidScConx(0).Line (0, Y1B)-(X0, Y1B), vbBlack
                X2 = LeftTime(0).Left - 60
                ArrCnxInfo(iArrBar).Left = X0 + 180
                ArrCnxInfo(iArrBar).Top = Y1A - (ArrCnxInfo(iArrBar).Height \ 2)
                ArrCnxInfo(iArrBar).Visible = True
                MidScConx(0).Line (X0, Y1A)-(ArrCnxInfo(iArrBar).Left, Y1A), vbBlack
                MidScConx(0).Line (X0, Y1B)-(ArrCnxInfo(iArrBar).Left, Y1B), vbBlack
                X4 = X3 - 600
            Case Else
        End Select
        
        CurConx = Right("000000" & CStr(iArrBar), 6)
        HitLine = TabRotFlightsTab.GetLinesByIndexValue("CONX", CurConx, 0)
        If HitLine <> "" Then
            ArrCnxIdx = iArrBar
            If CnxLayoutType > 0 Then
                ArrFlight(iArrBar).BackColor = vbYellow
                CopySelArrFlight iArrBar
                HighlightCurrentBar Index, ChartName
                SetCnxArrTimeScale iArrBar
            End If
            picArrCnx(iArrBar).Picture = CnxArrIcon(0)
            picArrCnx(iArrBar).ToolTipText = ""
            
            ArrRotLine = Val(HitLine)
            ErrCnt1 = 0
            ErrCnt2 = 0
            ArrUrno = TabRotFlightsTab.GetFieldValue(ArrRotLine, "AURN")
            If ArrUrno <> "" Then
                
                TabArrCnxPaxTab.SetInternalLineBuffer True
                HitLine = TabArrCnxPaxTab.GetLinesByIndexValue("AURN", ArrUrno, 0)
                If Val(HitLine) > 0 Then
                    CfiLine = TabArrCnxPaxTab.GetNextResultLine
                    
'TEST ONLY
                                        If LastColor = vbWhite Then
                                            LinColor = vbBlack
                                        Else
                                            LinColor = vbWhite
                                        End If
                                        LastColor = LinColor
                    
                
                    While CfiLine >= 0
                        'Here we identified a record of CFITAB
                        
                        'First we must initialize all field values
                        'and check the filter condition
                        CfiPaxTot1 = 0
                        CfiPaxTot2 = 0
                        CfiPaxTot3 = 0
                        CfiPaxTot4 = 0
                        tmpCnxTag = ""
                        
                        'Assume that the connection record is valid ...
                        'and check if this record matches the current filter.
                        'First we check the status conditions
                        
                        ShowDep = True
                        If ShowDep = True Then
                            IsShort = False
                            tmpData = TabArrCnxPaxTab.GetFieldValue(CfiLine, "SCID")
                            If tmpData <> "" Then
                                If InStr(tmpCnxTag, "S") = 0 Then tmpCnxTag = tmpCnxTag & "S"
                            End If
                            tmpData = TabArrCnxPaxTab.GetFieldValue(CfiLine, "CTMT")
                            If tmpData <> "" Then
                                CurCTim = Val(tmpData)
                                If CurCTim <= 0 Then
                                    If InStr(tmpCnxTag, "C") = 0 Then tmpCnxTag = tmpCnxTag & "C"
                                End If
                                If InStr(tmpCnxTag, "S") > 0 Then
                                    If CurCTim = Val(lblArrCsct(iArrBar).Caption) Then IsShort = True
                                End If
                            End If
                            
                            If (CnxFilterCritical > 0) And (CnxFilterShort > 0) Then
                                If (InStr(tmpCnxTag, "C") = 0) Or (InStr(tmpCnxTag, "S") = 0) Then ShowDep = False
                            ElseIf CnxFilterCritical > 0 Then
                                If (InStr(tmpCnxTag, "C") = 0) Then ShowDep = False
                            ElseIf CnxFilterShort > 0 Then
                                If (InStr(tmpCnxTag, "S") = 0) Then ShowDep = False
                            End If
                        End If
                        
                        If ShowDep = True Then
                            CfiPaxTot1 = Val(TabArrCnxPaxTab.GetFieldValue(CfiLine, "TCLF"))
                            CfiPaxTot2 = Val(TabArrCnxPaxTab.GetFieldValue(CfiLine, "TCLC"))
                            CfiPaxTot3 = Val(TabArrCnxPaxTab.GetFieldValue(CfiLine, "TCLY"))
                            CfiPaxTot4 = Val(TabArrCnxPaxTab.GetFieldValue(CfiLine, "TCLW"))
                            Select Case CnxFilterClass
                                Case 1
                                    If CfiPaxTot1 <= 0 Then ShowDep = False
                                Case 2
                                    If CfiPaxTot2 <= 0 Then ShowDep = False
                                Case 3
                                    If CfiPaxTot3 <= 0 Then ShowDep = False
                                Case 4
                                    If CfiPaxTot4 <= 0 Then ShowDep = False
                                Case Else
                                    ShowDep = True
                            End Select
                        End If
                            
                        
                        If ShowDep = True Then
                            'We have valid data of the arrival connection
                            'so we activate the connex grahpic objects
                            ShowArr = True
                            
                            'After having all values we can care about
                            'the chart line of the departure flight
                            'where we must publish the results
                            ErrCase = 0
                            DepUrno = TabArrCnxPaxTab.GetFieldValue(CfiLine, "DURN")
                            If DepUrno <> "0" Then
                                HitLine = TabRotFlightsTab.GetLinesByIndexValue("DURN", DepUrno, 0)
                                If HitLine <> "" Then
                                    DepRotLine = Val(HitLine)
                                    HitLine = TabRotFlightsTab.GetFieldValue(DepRotLine, "CONX")
                                    If HitLine <> "------" Then
                                        'Got the departure flight
                                        iDepBar = Val(HitLine)
                If CnxLayoutType = 0 Then
                        picDepCnxCnt(0).Tag = CStr(Val(picDepCnxCnt(0).Tag) + 1)
                        picDepCnxCnt(0).Picture = imgLookUpB
                End If
                                        If CnxLayoutType > 0 Then DepFlight(iDepBar).BackColor = vbBlack
                                        
                                        'Get the current connex totals per class fields
                                        'of the ARR-DEP connection (Sum of CodeShares)
                                        CnxPaxTot1 = Val(DepCnxTot1(iDepBar).Tag)
                                        CnxPaxTot2 = Val(DepCnxTot2(iDepBar).Tag)
                                        CnxPaxTot3 = Val(DepCnxTot3(iDepBar).Tag)
                                        CnxPaxTot4 = Val(DepCnxTot4(iDepBar).Tag)
                                        
                                        'And add this record to the PAX connex totals
                                        CnxPaxTot1 = CnxPaxTot1 + CfiPaxTot1
                                        CnxPaxTot2 = CnxPaxTot2 + CfiPaxTot2
                                        CnxPaxTot3 = CnxPaxTot3 + CfiPaxTot3
                                        CnxPaxTot4 = CnxPaxTot4 + CfiPaxTot4
                                        DepCnxTot1(iDepBar).Tag = CStr(CnxPaxTot1)
                                        DepCnxTot2(iDepBar).Tag = CStr(CnxPaxTot2)
                                        DepCnxTot3(iDepBar).Tag = CStr(CnxPaxTot3)
                                        DepCnxTot4(iDepBar).Tag = CStr(CnxPaxTot4)
                                        If CnxPaxTot1 > 0 Then DepCnxTot1(iDepBar).Caption = CStr(CnxPaxTot1)
                                        If CnxPaxTot2 > 0 Then DepCnxTot2(iDepBar).Caption = CStr(CnxPaxTot2)
                                        If CnxPaxTot3 > 0 Then DepCnxTot3(iDepBar).Caption = CStr(CnxPaxTot3)
                                        If CnxPaxTot4 > 0 Then DepCnxTot4(iDepBar).Caption = CStr(CnxPaxTot4)
                                        
                                        'Calculate the PAX total of this connection
                                        CnxPaxTot0 = 0
                                        CnxPaxTot0 = CnxPaxTot0 + CnxPaxTot1
                                        CnxPaxTot0 = CnxPaxTot0 + CnxPaxTot2
                                        CnxPaxTot0 = CnxPaxTot0 + CnxPaxTot3
                                        CnxPaxTot0 = CnxPaxTot0 + CnxPaxTot4
                                        lblDepTpax(iDepBar).Tag = CStr(CnxPaxTot0)
                                        
                                        'Get the current connex totals per class fields
                                        'of the ARR connection (Sum of CodeShares)
                                        ArrPaxTot1 = Val(ArrCnxTot1(iArrBar).Tag)
                                        ArrPaxTot2 = Val(ArrCnxTot2(iArrBar).Tag)
                                        ArrPaxTot3 = Val(ArrCnxTot3(iArrBar).Tag)
                                        ArrPaxTot4 = Val(ArrCnxTot4(iArrBar).Tag)
                                        
                                        'And add this record to the ARR PAX connex totals
                                        ArrPaxTot1 = ArrPaxTot1 + CfiPaxTot1
                                        ArrPaxTot2 = ArrPaxTot2 + CfiPaxTot2
                                        ArrPaxTot3 = ArrPaxTot3 + CfiPaxTot3
                                        ArrPaxTot4 = ArrPaxTot4 + CfiPaxTot4
                                        ArrCnxTot1(iArrBar).Tag = CStr(ArrPaxTot1)
                                        ArrCnxTot2(iArrBar).Tag = CStr(ArrPaxTot2)
                                        ArrCnxTot3(iArrBar).Tag = CStr(ArrPaxTot3)
                                        ArrCnxTot4(iArrBar).Tag = CStr(ArrPaxTot4)
                                        If ArrPaxTot1 > 0 Then ArrCnxTot1(iArrBar).Caption = CStr(ArrPaxTot1)
                                        If ArrPaxTot2 > 0 Then ArrCnxTot2(iArrBar).Caption = CStr(ArrPaxTot2)
                                        If ArrPaxTot3 > 0 Then ArrCnxTot3(iArrBar).Caption = CStr(ArrPaxTot3)
                                        If ArrPaxTot4 > 0 Then ArrCnxTot4(iArrBar).Caption = CStr(ArrPaxTot4)
                                        
                                        'Calculate the PAX total of this ARR connection
                                        ArrPaxTot0 = 0
                                        ArrPaxTot0 = ArrPaxTot0 + ArrPaxTot1
                                        ArrPaxTot0 = ArrPaxTot0 + ArrPaxTot2
                                        ArrPaxTot0 = ArrPaxTot0 + ArrPaxTot3
                                        ArrPaxTot0 = ArrPaxTot0 + ArrPaxTot4
                                        lblArrTpax(iArrBar).Tag = CStr(ArrPaxTot0)
                                        
                                        If CnxPaxTot0 > 0 Then
                                            lblDepTpax(iDepBar).Caption = CStr(CnxPaxTot0)
                                            lblDepTpax(iDepBar).BackColor = LightGreen
                                        Else
                                            lblDepTpax(iDepBar).Caption = ""
                                            lblDepTpax(iDepBar).BackColor = LightGrey
                                        End If
                                        
                                        If ArrPaxTot0 > 0 Then
                                            lblArrTpax(iArrBar).Caption = CStr(ArrPaxTot0)
                                            lblArrTpax(iArrBar).BackColor = LightGreen
                                        Else
                                            lblArrTpax(iArrBar).Caption = ""
                                            lblArrTpax(iArrBar).BackColor = LightGrey
                                        
                                        End If
                                        
                                        lblArrTbag(iArrBar).Caption = ""
                                        lblArrTbag(iArrBar).BackColor = NormalGrey
                                        lblArrTuld(iArrBar).Caption = ""
                                        lblArrTuld(iArrBar).BackColor = NormalGrey
                                        
                                        
                                        lblDepTbag(iDepBar).Caption = ""
                                        lblDepTbag(iDepBar).BackColor = NormalGrey
                                        
                                        lblDepTuld(iDepBar).Caption = ""
                                        lblDepTuld(iDepBar).BackColor = NormalGrey
                                        
                                        lblCnxPaxBar(iDepBar).BackColor = lblDepTpax(iDepBar).BackColor
                                        lblCnxBagBar(iDepBar).BackColor = lblDepTbag(iDepBar).BackColor
                                        lblCnxUldBar(iDepBar).BackColor = lblDepTuld(iDepBar).BackColor
                                        
                                        'LinColor = LastColor
                                        'LinColor = vbYellow
                                        'LinColor = vbBlue
                                        'LinColor = vbGreen
                                        LinColor = vbBlack
                                        'LinColor = vbWhite
                                        
                                        'THIS PART TO BE MODIFIED!!
                                        tmpData = TabArrCnxPaxTab.GetFieldValue(CfiLine, "SCID")
                                        DepCnxCsct(iDepBar).BackColor = vbWhite
                                        DepCnxCsct(iDepBar).ForeColor = vbBlack
                                        picArrCnx(iDepBar).Picture = CnxArrIcon(0).Picture
                                        If tmpData <> "" Then
                                            LinColor = vbRed
                                            DepCnxCsct(iDepBar).BackColor = LinColor
                                            DepCnxCsct(iDepBar).ForeColor = vbWhite
                                            picArrCnx(iDepBar).Picture = CnxArrIcon(1).Picture
                                            If InStr(tmpCnxTag, "S") = 0 Then tmpCnxTag = tmpCnxTag & "S"
                                        End If
                                        tmpData = TabArrCnxPaxTab.GetFieldValue(CfiLine, "CTMT")
                                        DepCnxCsct(iDepBar).Caption = tmpData
                                        If tmpData <> "" Then
                                            CurCTim = Val(tmpData)
                                            If CurCTim <= 0 Then
                                                LinColor = vbCyan
                                                DepCnxCsct(iDepBar).BackColor = LinColor
                                                DepCnxCsct(iDepBar).ForeColor = vbBlack
                                                picArrCnx(iDepBar).Picture = CnxArrIcon(2).Picture
                                            End If
                                        End If
                                        'SEE ABOVE
                                        
                                        If ShowDep = True Then
                                            tmpDepTag = picBarCnx(iDepBar).Tag
                                            If InStr(tmpCnxTag, "C") > 0 Then
                                                picBarCnx(iDepBar).Picture = picSmallIcon(6).Picture
                                                If InStr(tmpDepTag, "S") = 0 Then tmpDepTag = tmpDepTag & "S"
                                            ElseIf InStr(tmpCnxTag, "S") > 0 Then
                                                picBarCnx(iDepBar).Picture = picSmallIcon(1).Picture
                                                If InStr(tmpDepTag, "C") = 0 Then tmpDepTag = tmpDepTag & "C"
                                            End If
                                            picBarCnx(iDepBar).Tag = tmpDepTag
                                            DepLblPanel(iDepBar).Left = X3 - DepLblPanel(iDepBar).Width - 60
                                            picDepCnx(iDepBar).Left = DepLblPanel(iDepBar).Left - picDepCnx(iDepBar).Width
                                            X4 = picDepCnx(iDepBar).Left - 120
                                            picBarCnx(iDepBar).Left = X4 - 120
                                            picBarCnx(iDepBar).Visible = True
                                            DepLblPanel(iDepBar).Visible = True
                                            MaxWidth = picDepCnx(iDepBar).Left - X1
                                            
                                            DepCnxInfo(iDepBar).Width = DepCnxCsct(iDepBar).Left + DepCnxCsct(iDepBar).Width + 30
                                            
                                            Y2A = DepLblPanel(iDepBar).Top + (DepLblPanel(iDepBar).Height \ 2)
                                            Y2B = Y2A - 15
                                            If Y2A < Y1Min Then Y1Min = Y2A
                                            If Y2A > Y1Max Then Y1Max = Y2A
                                            Select Case CnxLayoutType
                                                Case 0, 1
                                                    If LinColor = vbCyan Then MidScConx(0).Line (X1, Y1A)-(X4, Y2A), vbBlack
                                                    If IsShort Then
                                                        MidScConx(0).Line (X1, Y1A - 30)-(X4, Y2A - 30), vbYellow
                                                        MidScConx(0).Line (X1, Y1A)-(X4, Y2A), vbRed
                                                    End If
                                                    
                                                    MidScConx(0).Line (X1, Y1B)-(X4, Y2B), LinColor
                                                    MidScConx(0).Line (X4, Y2A)-(X3, Y2A), vbBlack
                                                    MidScConx(0).Line (X4, Y2B)-(X3, Y2B), LinColor
                                                    'If CnxLayoutType = 1 Then
                                                        DepLblPanel(iDepBar).Visible = True
                                                    'End If
                                                    If (CnxFilterShort = 0) And (CnxFilterCritical = 0) Then
                                                        DepCnxInfo(iDepBar).Width = DepCnxCsct(iDepBar).Left
                                                    End If
                                                    DepCnxInfo(iDepBar).Left = picBarCnx(iDepBar).Left - DepCnxInfo(iDepBar).Width + 30
                                                    DepCnxInfo(iDepBar).Top = Y2A - (DepCnxInfo(iDepBar).Height \ 2)
                                                    'DepCnxInfo(iDepBar).Visible = True
                                                Case 2
                                                    MidScConx(0).Line (X1, Y2A)-(X3, Y2A), vbBlack
                                                    MidScConx(0).Line (X1, Y2B)-(X3, Y2B), LinColor
                                                    picArrCnx(iDepBar).Left = picArrCnx(iArrBar).Left
                                                    picArrCnx(iDepBar).Visible = True
                                                    lblCnxPaxBar(iDepBar).Left = X1
                                                    lblCnxBagBar(iDepBar).Left = X1
                                                    lblCnxUldBar(iDepBar).Left = X1
                                                    NewWidth = CurCTim * 30
                                                    If NewWidth > MaxWidth Then NewWidth = MaxWidth
                                                    If NewWidth < 0 Then NewWidth = 0
                                                    lblCnxPaxBar(iDepBar).Width = NewWidth
                                                    lblCnxBagBar(iDepBar).Width = NewWidth
                                                    lblCnxUldBar(iDepBar).Width = NewWidth
                                                    lblCnxPaxBar(iDepBar).Visible = True
                                                    lblCnxBagBar(iDepBar).Visible = True
                                                    lblCnxUldBar(iDepBar).Visible = True
                                                    DepLblPanel(iDepBar).Visible = True
                                                Case 3
                                                    MidScConx(0).Line (X1, Y2A)-(X3, Y2A), vbBlack
                                                    MidScConx(0).Line (X1, Y2B)-(X3, Y2B), LinColor
                                                    picArrCnx(iDepBar).Left = picArrCnx(iArrBar).Left
                                                    picArrCnx(iDepBar).Visible = True
                                                    lblCnxPaxBar(iDepBar).Left = X1
                                                    lblCnxBagBar(iDepBar).Left = X1
                                                    lblCnxUldBar(iDepBar).Left = X1
                                                    NewWidth = CurCTim * 30
                                                    If NewWidth > MaxWidth Then NewWidth = MaxWidth
                                                    If NewWidth < 0 Then NewWidth = 0
                                                    lblCnxPaxBar(iDepBar).Width = NewWidth
                                                    lblCnxBagBar(iDepBar).Width = NewWidth
                                                    lblCnxUldBar(iDepBar).Width = NewWidth
                                                    lblCnxPaxBar(iDepBar).Visible = True
                                                    lblCnxBagBar(iDepBar).Visible = True
                                                    lblCnxUldBar(iDepBar).Visible = True
                                                    DepLblPanel(iDepBar).Visible = True
                                                    DepCnxInfo(iDepBar).Left = RightTime(0).Left - DepCnxInfo(iDepBar).Width + 690
                                                    DepCnxInfo(iDepBar).Top = Y2A - (DepCnxInfo(iDepBar).Height \ 2)
                                                    DepCnxInfo(iDepBar).Visible = True
                                                Case 4
                                                    If IsShort Then
                                                        'MidScConx(0).Line (X1, Y1A - 30)-(X4, Y2A - 30), vbYellow
                                                        'MidScConx(0).Line (X1, Y1A)-(X4, Y2A), vbRed
                                                    End If
                                                    MidScConx(0).Line (X0, Y1B)-(X2, Y2B), LinColor
                                                    MidScConx(0).Line (X1, Y2A)-(X3, Y2A), vbBlack
                                                    MidScConx(0).Line (X1, Y2B)-(X3, Y2B), LinColor
                                                    picArrCnx(iDepBar).Left = X1 - 120
                                                    picArrCnx(iDepBar).Visible = True
                                                    lblCnxPaxBar(iDepBar).Left = X1
                                                    lblCnxBagBar(iDepBar).Left = X1
                                                    lblCnxUldBar(iDepBar).Left = X1
                                                    NewWidth = CurCTim * 30
                                                    If NewWidth > MaxWidth Then NewWidth = MaxWidth
                                                    If NewWidth < 0 Then NewWidth = 0
                                                    lblCnxPaxBar(iDepBar).Width = NewWidth
                                                    lblCnxBagBar(iDepBar).Width = NewWidth
                                                    lblCnxUldBar(iDepBar).Width = NewWidth
                                                    lblCnxPaxBar(iDepBar).Visible = True
                                                    lblCnxBagBar(iDepBar).Visible = True
                                                    lblCnxUldBar(iDepBar).Visible = True
                                                    DepLblPanel(iDepBar).Visible = True
                                                    DepCnxInfo(iDepBar).Left = RightTime(0).Left - DepCnxInfo(iDepBar).Width + 690
                                                    DepCnxInfo(iDepBar).Top = Y2A - (DepCnxInfo(iDepBar).Height \ 2)
                                                    DepCnxInfo(iDepBar).Visible = True
                                                Case Else
                                            End Select
                                            picDepCnx(iDepBar).Visible = True
                                        End If
                                    Else
                                        ErrCase = 3
                                        ErrCnt1 = ErrCnt1 + 1
                                    End If
                                Else
                                    ErrCase = 2
                                    ErrCnt1 = ErrCnt1 + 1
                                End If
                            Else
                                ErrCase = 1
                                ErrCnt1 = ErrCnt1 + 1
                            End If
                        End If
                        If ErrCase > 0 Then
                            If CnxLayoutType > 0 Then CreateDepInfoRec CfiLine, ErrCase
                            ShowArr = True
                        End If
                        SetInfoPanelColors iArrBar, iDepBar
                        CfiLine = TabArrCnxPaxTab.GetNextResultLine
                    Wend
                End If
                TabArrCnxPaxTab.SetInternalLineBuffer False
                If (ShowArr = True) And (picArrCnx(iArrBar).Visible = False) Then
                    ArrLblPanel(iArrBar).Visible = True
                    picArrCnx(iArrBar).Visible = True
                    MidScConx(0).Line (0, Y1A)-(X0, Y1A), vbBlack
                    MidScConx(0).Line (0, Y1B)-(X0, Y1B), vbBlack
                End If
                Select Case CnxLayoutType
                    Case 0, 1
                    Case 2, 3
                        LeftTime(0).Top = 0
                        LeftTime(0).Height = MidScConx(0).Height
                        RightTime(0).Top = LeftTime(0).Top
                        RightTime(0).Height = LeftTime(0).Height
                        LeftTime(0).Visible = True
                    Case 4
                        LeftTime(0).Top = Y1Min - 120
                        LeftTime(0).Height = Y1Max - Y1Min + 240
                        RightTime(0).Top = LeftTime(0).Top
                        RightTime(0).Height = LeftTime(0).Height
                        LeftTime(0).Visible = True
                        
                        LeftLine(0).Y1 = 0
                        LeftLine(0).Y2 = LeftTime(0).Top
                        LeftLine(1).Y1 = LeftLine(0).Y1
                        LeftLine(1).Y2 = LeftLine(0).Y2
                        
                        RightLine(0).Y1 = 0
                        RightLine(0).Y2 = RightTime(0).Top
                        RightLine(1).Y1 = RightLine(0).Y1
                        RightLine(1).Y2 = RightLine(0).Y2
                    Case Else
                End Select
                LeftTime(1).Left = LeftTime(0).Left
                lblLeftTime(0).Left = LeftTime(1).Left + MidScCon1(0).Left + 60
                lblLeftDate(0).Left = lblLeftTime(0).Left - lblLeftDate(0).Width
                RightTime(1).Left = RightTime(0).Left
                lblRightTime(0).Left = RightTime(1).Left - lblRightTime(0).Width + MidScCon1(0).Left + 45
                lblRightDate(0).Left = RightTime(1).Left + MidScCon1(0).Left + 45
                LeftTime(1).Visible = LeftTime(0).Visible
                lblLeftDate(0).Visible = LeftTime(1).Visible
                lblLeftTime(0).Visible = LeftTime(1).Visible
                RightTime(0).Visible = LeftTime(0).Visible
                RightTime(1).Visible = LeftTime(0).Visible
                lblRightTime(0).Visible = RightTime(1).Visible
                lblRightDate(0).Visible = RightTime(1).Visible
                
                LeftLine(0).X1 = LeftTime(0).Left
                LeftLine(0).X2 = LeftLine(0).X1
                LeftLine(1).X1 = LeftLine(0).X1 + 15
                LeftLine(1).X2 = LeftLine(0).X2 + 15
                LeftLine(0).Visible = LeftTime(0).Visible
                LeftLine(1).Visible = LeftLine(0).Visible
            
                RightLine(0).X1 = RightTime(0).Left
                RightLine(0).X2 = RightLine(0).X1
                RightLine(1).X1 = RightLine(0).X1 + 15
                RightLine(1).X2 = RightLine(0).X2 + 15
                RightLine(0).Visible = RightTime(0).Visible
                RightLine(1).Visible = RightLine(0).Visible
                If (ErrCnt1 + ErrCnt2) > 0 Then
                    picArrCnx(iArrBar).Picture = picInfoPics(1)
                    If ErrCnt1 = 1 Then
                        tmpData = "--> " & CStr(ErrCnt1) & " Departure Flight Is Outside The Data View"
                    End If
                    If ErrCnt1 > 1 Then
                        tmpData = "--> " & CStr(ErrCnt1) & " Departure Flights Are Outside The Data View"
                    End If
                    picArrCnx(iArrBar).ToolTipText = tmpData
                End If
                If TabDepInfo(0).GetLineCount > 0 Then
                    TabDepInfo(0).AutoSizeColumns
                    TabDepInfo(0).Sort "2", True, True
                    TabDepInfo(0).Visible = True
                End If
                'If CurLayoutType > 0 Then
                    tmpData = ""
                    tmpData = tmpData & "{X3}" & CStr(X3) & "{/X3}"
                    tmpData = tmpData & "{H}" & CStr(CurCnxLayoutHours) & "{/H}"
                    MidScConx(0).Tag = tmpData
                'End If
                If CnxLayoutType = 0 Then
                    If ShowArr = True Then
                        picArrCnxCnt(0).Tag = CStr(Val(picArrCnxCnt(0).Tag) + 1)
                        picArrCnxCnt(0).Picture = imgLookUpA
                    End If
                    If ShowDep = True Then
                        picDepCnxCnt(0).Tag = CStr(Val(picDepCnxCnt(0).Tag) + 1)
                        picDepCnxCnt(0).Picture = imgLookUpB
                    End If
                End If
            End If
        End If
    End If
    MidScConx(0).ZOrder
End Sub
Private Sub SetInfoPanelColors(iArrBar As Integer, iDepBar As Integer)
    Dim SetBackColor As Long
    Dim SetTypeColor As Long
    If iArrBar >= 0 Then
        SetTypeColor = vbWhite
        If chkCnxType(0).Value = 1 Then SetTypeColor = LightGreen
        SetBackColor = SetTypeColor
        If CnxFilterClass > 0 Then SetBackColor = LightGrey
        ArrCnxTot1(iArrBar).BackColor = SetBackColor
        ArrCnxTot2(iArrBar).BackColor = SetBackColor
        ArrCnxTot3(iArrBar).BackColor = SetBackColor
        ArrCnxTot4(iArrBar).BackColor = SetBackColor
        Select Case CnxFilterClass
            Case 1
                ArrCnxTot1(iArrBar).BackColor = SetTypeColor
            Case 2
                ArrCnxTot2(iArrBar).BackColor = SetTypeColor
            Case 3
                ArrCnxTot3(iArrBar).BackColor = SetTypeColor
            Case 4
                ArrCnxTot4(iArrBar).BackColor = SetTypeColor
            Case Else
        End Select
    End If
    If iDepBar >= 0 Then
        SetTypeColor = vbWhite
        If chkCnxType(0).Value = 1 Then SetTypeColor = LightGreen
        SetBackColor = SetTypeColor
        If CnxFilterClass > 0 Then SetBackColor = LightGrey
        DepCnxTot1(iDepBar).BackColor = SetBackColor
        DepCnxTot2(iDepBar).BackColor = SetBackColor
        DepCnxTot3(iDepBar).BackColor = SetBackColor
        DepCnxTot4(iDepBar).BackColor = SetBackColor
        Select Case CnxFilterClass
            Case 1
                DepCnxTot1(iDepBar).BackColor = SetTypeColor
            Case 2
                DepCnxTot2(iDepBar).BackColor = SetTypeColor
            Case 3
                DepCnxTot3(iDepBar).BackColor = SetTypeColor
            Case 4
                DepCnxTot4(iDepBar).BackColor = SetTypeColor
            Case Else
        End Select
    End If
End Sub

Private Sub CreateDepInfoRec(CnxLine As Long, ForWhat As Integer)
    Dim tmpData As String
    Dim tmpRec As String
    'We must fetch each single field
    'because tab.ocx has a bug in GetFieldValues:
    'Creates a corrupt data list when a grid field is unknown
    tmpRec = ""
    'DepInfoFields = "AFSD,FLND,STOD,TIFD,DES3,PSTD,SCID,TPAX,TBAG,TULD,REMA,DURN"
    tmpRec = tmpRec & TabArrCnxPaxTab.GetFieldValue(CnxLine, "AFSD") & ","
    tmpRec = tmpRec & TabArrCnxPaxTab.GetFieldValue(CnxLine, "FLND") & ","
    tmpRec = tmpRec & TabArrCnxPaxTab.GetFieldValue(CnxLine, "STOD") & ","
    tmpRec = tmpRec & TabArrCnxPaxTab.GetFieldValue(CnxLine, "DES3") & ","
    tmpRec = tmpRec & TabArrCnxPaxTab.GetFieldValue(CnxLine, "PSTD") & ","
    tmpRec = tmpRec & TabArrCnxPaxTab.GetFieldValue(CnxLine, "SCID") & ","
    tmpRec = tmpRec & TabArrCnxPaxTab.GetFieldValue(CnxLine, "TALL") & ","
    tmpRec = tmpRec & "" & ","  'TBAG
    tmpRec = tmpRec & "" & ","  'TULD
    tmpRec = tmpRec & "" & ","  'REMA
    tmpRec = tmpRec & TabArrCnxPaxTab.GetFieldValue(CnxLine, "DURN")
    TabDepInfo(0).InsertTextLine tmpRec, False
End Sub
Private Sub ClearConnexDetails()
    Dim iBar As Integer
    MouseTimer(0).Enabled = False
    MouseTimer(1).Enabled = False
    MouseTimer(0).Tag = ""
    MouseTimer(1).Tag = ""
    
    For iBar = 0 To CurBarMax
        'If (lblArrCsct(iBar).Tag <> "") Or (lblDepCsct(iBar).Tag <> "") Then
    
            ArrFlight(iBar).BackColor = LightGrey
            DepFlight(iBar).BackColor = LightGrey
            ArrCnxInfo(iBar).Visible = False
            DepCnxInfo(iBar).Visible = False
            lblCnxPaxBar(iBar).Visible = False
            lblCnxBagBar(iBar).Visible = False
            lblCnxUldBar(iBar).Visible = False
            picArrCnx(iBar).Visible = False
            ArrLblPanel(iBar).Visible = False
            picBarCnx(iBar).Visible = False
            picDepCnx(iBar).Visible = False
            DepLblPanel(iBar).Visible = False
            If picBarCnx(iBar).Tag <> "" Then picBarCnx(iBar).Picture = picSmallIcon(0).Picture
            picBarCnx(iBar).Tag = ""
            
            ArrCnxTot1(iBar).Tag = "0"
            ArrCnxTot2(iBar).Tag = "0"
            ArrCnxTot3(iBar).Tag = "0"
            ArrCnxTot4(iBar).Tag = "0"
            ArrCnxTot1(iBar).Caption = ""
            ArrCnxTot2(iBar).Caption = ""
            ArrCnxTot3(iBar).Caption = ""
            ArrCnxTot4(iBar).Caption = ""
            
            DepCnxTot1(iBar).Tag = "0"
            DepCnxTot2(iBar).Tag = "0"
            DepCnxTot3(iBar).Tag = "0"
            DepCnxTot4(iBar).Tag = "0"
            DepCnxTot1(iBar).Caption = ""
            DepCnxTot2(iBar).Caption = ""
            DepCnxTot3(iBar).Caption = ""
            DepCnxTot4(iBar).Caption = ""
        'End If
    Next
    MidScConx(0).Cls
    LeftTime(0).Visible = False
    RightTime(0).Visible = False
    LeftTime(1).Visible = False
    lblLeftDate(0).Visible = False
    lblLeftTime(0).Visible = False
    lblRightTime(0).Visible = False
    lblRightDate(0).Visible = False
    RightTime(1).Visible = False
    SelArrPanel(0).Visible = False
    SelDepPanel(0).Visible = False
    LeftLine(0).Visible = False
    RightLine(0).Visible = False
    LeftLine(1).Visible = False
    RightLine(1).Visible = False
    TabDepInfo(0).Visible = False
    TabDepInfo(0).ResetContent
    TabDepInfo(0).Refresh
    picArrCnxCnt(0).Tag = "0"
    picArrCnxCnt(0).Picture = picNeutral(0).Picture
    picDepCnxCnt(0).Tag = "0"
    picDepCnxCnt(0).Picture = picNeutral(0).Picture
    picArrCnxCnt(0).Refresh
    picDepCnxCnt(0).Refresh
    
    CurCnxView = -1
End Sub

Private Sub CopySelArrFlight(iBar)
    CopyLabelObject lblSelArrFlno(0), lblArrFlno(iBar)
    CopyLabelObject lblSelArrFlti(0), lblArrFlti(iBar)
    CopyLabelObject lblSelArrRegn(0), lblArrRegn(iBar)
    CopyLabelObject lblSelArrAct3(0), lblArrAct3(iBar)
    CopyLabelObject lblSelArrStoa(0), lblArrStoa(iBar)
    CopyLabelObject lblSelArrEtai(0), lblArrEtai(iBar)
    CopyLabelObject lblSelArrOnbl(0), lblArrOnbl(iBar)
    CopyLabelObject lblSelArrPsta(0), lblArrPsta(iBar)
    CopyLabelObject lblSelArrOrg3(0), lblArrOrg3(iBar)
    CopyLabelObject lblSelArrVia3(0), lblArrVia3(iBar)
    CopyLabelObject lblSelArrTotPax(0), lblArrTotPax(iBar)
    CopyLabelObject lblSelArrTotBag(0), lblArrTotBag(iBar)
    CopyLabelObject lblSelArrTotUld(0), lblArrTotUld(iBar)
    CopyLabelObject lblSelArrTot1(0), lblArrTot1(iBar)
    CopyLabelObject lblSelArrTot2(0), lblArrTot2(iBar)
    CopyLabelObject lblSelArrTot3(0), lblArrTot3(iBar)
    CopyLabelObject lblSelArrTot4(0), lblArrTot4(iBar)
    CopyLabelObject lblSelArrCsct(0), lblArrCsct(iBar)
    
    picSelArrStatus(0).Picture = picArrStatus(iBar).Picture
    SelArrFlight(0).BackColor = ArrFlight(iBar).BackColor
    
End Sub

Private Sub CopyLabelObject(TgtLbl As Label, SrcLbl As Label)
    TgtLbl.Caption = SrcLbl.Caption
    TgtLbl.Tag = SrcLbl.Tag
    TgtLbl.BackColor = SrcLbl.BackColor
    TgtLbl.ForeColor = SrcLbl.ForeColor
    TgtLbl.MousePointer = SrcLbl.MousePointer
End Sub

Private Sub ComposeChartTitle()
    Dim tmpTitle As String
    Dim tmpText As String
    Dim tmpAdid As String
    Dim tmpType As String
    Dim tmpClass As String
    Dim tmpShort As String
    Dim tmpCritical As String
    tmpText = ""
    Select Case CnxFilterAdid
        Case 0
            tmpAdid = "Arrival"
        Case 1
            tmpAdid = "Departure"
        Case Else
            tmpAdid = ""
    End Select
    Select Case CnxFilterType
        Case 1
            tmpType = "PAX"
        Case 2
            tmpType = "BAG"
        Case 3
            tmpType = "ULD"
        Case Else
            tmpType = "Connections"
    End Select
    Select Case CnxFilterClass
        Case 1
            tmpClass = "First Class"
        Case 2
            tmpClass = "Business Class"
        Case 3
            tmpClass = "Economy Class"
        Case 4
            tmpClass = "Premium Economy"
        Case Else
            tmpClass = ""
    End Select
    tmpCritical = ""
    If (CnxFilterShort > 0) And (CnxFilterCritical > 0) Then
        tmpShort = "Critical Short Connections"
    ElseIf CnxFilterShort > 0 Then
        tmpShort = "Short Connections"
    ElseIf CnxFilterCritical > 0 Then
        tmpCritical = "Critical Connections"
    Else
        tmpShort = ""
    End If
    
    If CnxLayoutType < 1 Then
        'tmpText = "Transfer Connections"
    Else
        'tmpText = "Transfer"
    End If
    
    tmpText = "Transfer"
    Select Case CnxLayoutType
        'Case 0
        Case -2
            tmpText = tmpText & " (" & tmpAdid & ")"
        Case Else
            tmpText = tmpText & " " & tmpType
            tmpText = tmpText & " (" & tmpAdid & ")"
            If tmpClass <> "" Then tmpText = tmpText & " " & tmpClass
            If tmpShort <> "" Then tmpText = tmpText & " " & tmpShort
            If tmpCritical <> "" Then tmpText = tmpText & " " & tmpCritical
    End Select
    
    lblTabCaption(0).Caption = tmpText
    lblTabCaptionShadow(0).Caption = tmpText
    
    'If chkSelDeco(2).Value = 0 Then
        tmpTitle = Me.Caption
        tmpTitle = GetItem(tmpTitle, 1, "[")
        tmpTitle = tmpTitle & "  [" & tmpText & "]"
        Me.Caption = tmpTitle
    'End If
    
End Sub

Private Sub SetCnxArrTimeScale(iBar)
    Dim tmpTag As String
    Dim tmpCfiTifa As String
    Dim tmpAftTifa As String
    Dim tmpDate As String
    Dim tmpRange As String
    If iBar >= 0 Then
        tmpTag = ArrFlight(iBar).Tag
        GetKeyItem tmpCfiTifa, tmpTag, "{CFITIFA}", "{/CFITIFA}"
        GetKeyItem tmpAftTifa, tmpTag, "{AFTTIFA}", "{/AFTTIFA}"
        If tmpCfiTifa = "" Then tmpCfiTifa = tmpAftTifa
        lblLeftTime(0).Tag = tmpCfiTifa
        SetTimeFieldCaption lblLeftTime(0), CurUtcTimeDiff
        tmpDate = CedaDateTimeAdd(tmpCfiTifa, CLng(CurUtcTimeDiff))
        tmpDate = Left(tmpDate, 8)
        tmpDate = DecodeSsimDayFormat(tmpDate, "CEDA", "SSIM2")
        'tmpDate = Left(tmpDate, 5)
        lblLeftDate(0).Caption = tmpDate
        
        tmpRange = CedaDateTimeAdd(tmpCfiTifa, CnxRangeMin)
        lblRightTime(0).Tag = tmpRange
        SetTimeFieldCaption lblRightTime(0), CurUtcTimeDiff
        tmpDate = CedaDateTimeAdd(tmpRange, CLng(CurUtcTimeDiff))
        tmpDate = Left(tmpDate, 8)
        tmpDate = DecodeSsimDayFormat(tmpDate, "CEDA", "SSIM2")
        'tmpDate = Left(tmpDate, 5)
        lblRightDate(0).Caption = tmpDate
    End If
End Sub
