﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ufis.Data;
using Ufis.Entities;
using Ufis.Data.Ceda;

namespace GroundControlManager.DataAccess
{
    public class DlGroundControlManager
    {
        public static EntityCollectionBase<EntDbFlight> LoadFlights(string attributeList, string whereClause)
        {
            EntityCollectionBase<EntDbFlight> flights = null;

            EntityDataContextBase dataContext = DlUfisData.Current.DataContext;
            DataCommand command = new CedaEntitySelectCommand()
            {
                EntityType = typeof(EntDbFlight),
                AttributeList = attributeList,
                EntityWhereClause = (whereClause ?? string.Empty)
            };

            try
            {
                flights = dataContext.OpenEntityCollection<EntDbFlight>(command);
            }
            catch (Exception e)
            {
                throw e;
            }

            return flights;
        }

    }
}
