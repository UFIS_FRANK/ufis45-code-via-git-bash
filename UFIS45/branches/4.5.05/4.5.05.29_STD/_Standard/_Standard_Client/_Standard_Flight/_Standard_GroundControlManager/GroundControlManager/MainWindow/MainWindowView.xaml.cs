﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using DevExpress.Xpf.Bars;
using Ufis.Entities;
using System.Windows.Input;
using System.IO;
using Ufis.UserLayout;
using DevExpress.Xpf.Grid;

namespace GroundControlManager.MainWindow
{
    /// <summary>
    /// Interaction logic for MainWindowView.xaml
    /// </summary>
    public partial class MainWindowView : UserControl
    {
        public MainWindowView()
        {
            InitializeComponent();
        }

        private void barEditViewList_EditValueChanged(object sender, RoutedEventArgs e)
        {
            //BarEditItem barEditItem = sender as BarEditItem;
            //EntDbUserDefinedLayout userLayout = barEditItem.EditValue as EntDbUserDefinedLayout;
            //if (barEditItem == null || userLayout == null)
            //    return;

            ////ignore if userLayout is the same as current item
            //MainWindowViewModel mainWindowViewModel = DataContext as MainWindowViewModel;
            //if (mainWindowViewModel == null)
            //    return;
            //BasicDataViewModel basicDataViewModel = mainWindowViewModel.Workspace as BasicDataViewModel;
            //if (basicDataViewModel == null)
            //    return;
            //if (userLayout.Equals(basicDataViewModel.UserDefinedLayoutViewModel.FilteredUserDefinedLayouts.CurrentItem))
            //    return;

            //Mouse.OverrideCursor = Cursors.Wait;

            //string strLayout = userLayout.Value;
            //if (!string.IsNullOrEmpty(strLayout))
            //{
            //    using (Stream stream = UfisDxLayoutConverter.UfisToDxGridLayout(strLayout))
            //    {
            //        if (stream != null)
            //        {
            //            DevExpress.Xpf.Grid.GridControl grid = FlightList.Helpers.HpDxGrid.CurrentGrid;
            //            grid.RestoreLayoutFromStream(stream);
            //            //grid.ShowLoadingPanel = true;
            //            //using (BackgroundWorker worker = new BackgroundWorker())
            //            //{
            //            //    worker.DoWork += (o, d) =>
            //            //    {
            //            //        byte[] blob = (byte[])d.Argument;
            //            //        //System.Threading.Thread.Sleep(2000);
            //            //        this.Dispatcher.BeginInvoke(new Action(() =>
            //            //        {
            //            //            using (MemoryStream memStream = new MemoryStream(blob))
            //            //            {
            //            //                grid.RestoreLayoutFromStream(memStream);
            //            //                grid.ShowLoadingPanel = false;
            //            //            }
            //            //        }));
            //            //    };
            //            //    worker.RunWorkerAsync(((MemoryStream)stream).ToArray());
            //            //}
            //        }
            //    }
            //}

            //basicDataViewModel.LoadLayout(userLayout);

            //Mouse.OverrideCursor = null;
        }

        private void UserControl_LayoutUpdated(object sender, EventArgs e)
        {
            Mouse.OverrideCursor = null;
        }

        private void TableView_CellValueChanging(object sender, DevExpress.Xpf.Grid.CellValueChangedEventArgs e)
        {
            TableView view = sender as TableView;
            if (view != null)
            {
                view.PostEditor();

                MainWindowViewModel viewModel = DataContext as MainWindowViewModel;
                if (viewModel != null)
                {
                    viewModel.UpdateDb(e.Row as EntDbFlight);
                }
            }
        }
    }
}
