#if !defined(AFX_POSITIONMATRIXDLG_H__D898A153_D1E8_11D6_8218_00010215BFDE__INCLUDED_)
#define AFX_POSITIONMATRIXDLG_H__D898A153_D1E8_11D6_8218_00010215BFDE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// PositionMatrixDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// PositionMatrixDlg dialog
class CGridFenster;

class PositionMatrixDlg : public CDialog
{
// Construction
public:
	PositionMatrixDlg(const CString& ropFromPos,const CString& ropToPos,CWnd* pParent = NULL);   // standard constructor
	PositionMatrixDlg(const CString& ropFromPos,const CString& ropToPos, const CString& ropAlo, const CString& ropGrp,CWnd* pParent = NULL);   // standard constructor
	~PositionMatrixDlg();

// Dialog Data
	//{{AFX_DATA(PositionMatrixDlg)
	enum { IDD = IDD_POS_MATRIX };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(PositionMatrixDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(PositionMatrixDlg)
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg	LONG OnGridMessageCellClick(WPARAM wParam,LPARAM lParam);
	afx_msg	LONG OnGridMessageEndEditing(WPARAM wParam,LPARAM lParam);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	CGridFenster*	pomMatrix;
	CString			omFromPos;
	CString			omToPos;
	CString			omAlo;
	CString			omGrp;
	bool*			pomModified;
	long			lmUrnoFrom;
	long			lmUrnoTo;
private:
	void	UpdateGrid();
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_POSITIONMATRIXDLG_H__D898A153_D1E8_11D6_8218_00010215BFDE__INCLUDED_)
