// CedaHTYData.h

#ifndef __CEDAHTYDATA__
#define __CEDAHTYDATA__
 
//#include "stdafx.h"
#include <basicdata.h>

//---------------------------------------------------------------------------------------------------------
// Record structure declaration

//@See: CedaHTYData
//@Doc:	A structure for reading Duty data. We read all data from database and put them
//		into this struct and store the data in omData.  

//---------------------------------------------------------------------------------------------------------
struct HTYDATA 
{
	CTime	 Cdat; 		// Erstellungsdatum
	CTime	 Lstu; 		// Datum letzte �nderung
	char 	 Prfl[3]; 	// Protokollierungskennzeichen
	char 	 Hnam[32]; 	// Abfertigungsart Name
	char 	 Htyp[4]; 	// Abfertigungsart
	long 	 Urno; 		// Eindeutige Datensatz-Nr.
	char 	 Usec[34]; 	// Anwender (Ersteller)
	char 	 Useu[34]; 	// Anwender (letzte �nderung)
	CTime	 Vafr; 		// G�ltig von
	CTime	 Vato; 		// G�ltig bis

	//DataCreated by this class
	int		 IsChanged;

	HTYDATA(void)
	{ 
		memset(this,'\0',sizeof(*this));
		Cdat=-1;Lstu=-1;Vafr=-1;Vato=-1;
	}

}; // end HTYDataStrukt


//---------------------------------------------------------------------------------------------------------
// Class declaratino

//@Memo: Read and write HTY(duty) data
//@See: CedaData, CedaDutyData
/*@Doc:
  Reads and writes HTY(duty) data from and to database. Stores HTY data in memory and
  provides methods for searching and retrieving HTYs from its internal list. Does
  some data preparations.

  Data of this class has to be updated by CEDA through broadcasts. This procedure and the
  necessary methods will be described later with the description of broadcast handling.

  {\bf CedaHTYData} handles data from the server process. 
  See the specification for table descriptions.*/

class CedaHTYData: public CCSCedaData
{
// Attributes
public:
    //@ManMemo: A map, containing the Urno field of all loaded HTYs.
    CMapPtrToPtr omUrnoMap;
    CMapStringToPtr omHtypMap;

    //@ManMemo: HTYDATA records read by ReadAllHTY().
    CCSPtrArray<HTYDATA> omData;

	void SetTableName(CString opTableName)
	{
		strcpy(pcmTableName, opTableName.GetBuffer(0));
	}
// Operations
public:
    //@ManMemo: Default constructor
    /*@Doc:
      Initialize {\bf omRecInfo} for offsets and types for each fields in {\bf HTYDATA},
      the {\bf pcmTableName} with table name {\bf HTYLSG}, the data members {\bf pcmFieldList}
      contains a list of used fields of {\bf HTYDATA}.
    */
    CedaHTYData();
	~CedaHTYData();

	void Register(void);
	void ClearAll(void);

    //@ManMemo: Read all HTYs.
    /*@Doc:
      Read all HTYs. 

      This method will clear {\bf omData} and {\bf omKeyMap}. Convert each record
      to a HTYDATA and store it in {\bf omData} and the Urno field in {\bf omKeyMap}.
    */
    bool ReadAllHTYs();
    //@AreMemo: Read all HTY-Data corresponding to Where-condition
	bool ReadSpecial(CCSPtrArray<HTYDATA> &ropHty,char *pspWhere);
    //@ManMemo: Add a new HTY and store the new data to the Database.
	bool InsertHTY(HTYDATA *prpHTY,BOOL bpSendDdx = TRUE);
    //@ManMemo: Add data for a new HTY to the internal data.
	bool InsertHTYInternal(HTYDATA *prpHTY);
	//@ManMemo: Change the data of a special HTY
	bool UpdateHTY(HTYDATA *prpHTY,BOOL bpSendDdx = TRUE);
    //@AreMemo: Change the data of a special HTY in the internal data.
	bool UpdateHTYInternal(HTYDATA *prpHTY);
    //@ManMemo: Delete a special HTY, also in the Database.
	bool DeleteHTY(long lpUrno);
    //@ManMemo: Delete a special HTY only in the internal data.
	bool DeleteHTYInternal(HTYDATA *prpHTY);
    //@AreMemo: Selects all HTYs with a special Urno.
	HTYDATA  *GetHTYByUrno(long lpUrno);
    //@AreMemo: return Urno for Htyp
	long CedaHTYData::GetUrnoByHtyp(char *pspHtyp);
    //@ManMemo: Search a HTY with a special Key.
	CString HTYExists(HTYDATA *prpHTY);
    //@AreMemo: Search a Htyp
	bool ExistHtyp(char* popHtyp);
	//@ManMemo: Writes a special HTY to the Database.
	bool SaveHTY(HTYDATA *prpHTY);
    //@ManMemo: Contains the valid data for all existing HTYs.
	char pcmHTYFieldList[2048];
    //@ManMemo: Handle Broadcasts for HTYs.
	void ProcessHTYBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

	// Private methods
private:
    void PrepareHTYData(HTYDATA *prpHTYData);
};

extern CedaHTYData ogHtyData;

//---------------------------------------------------------------------------------------------------------
#endif //__CEDAHTYDATA__
