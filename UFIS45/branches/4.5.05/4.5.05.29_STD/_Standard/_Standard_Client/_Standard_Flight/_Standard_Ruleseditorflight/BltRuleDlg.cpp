// BltRuleDlg.cpp : implementation file
//

#include "stdafx.h"
#include "rules.h"
#include "BltRuleDlg.h"
#include "BasicDAta.h"
#include "CedaAltData.h"
#include "CedaAPTData.h"
#include "CedaBasicData.h"
#include "CedaNATData.h"
#include "AirCraftGroupSelDlg.h"
#include <CedaActData.h>
#include "GroupNamesPage.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


/////////////////////////////////////////////////////////////////////////////
// BltRuleDlg dialog
IMPLEMENT_SERIAL(BltRuleDlg,CDialog,0);

BltRuleDlg::BltRuleDlg(CWnd* pParent /*=NULL*/, BLTDATA *popBlt)
	: CDialog(BltRuleDlg::IDD, pParent)
{
	pomBlt = popBlt;


	//{{AFX_DATA_INIT(BltRuleDlg)
	m_Prio = 0;
	m_sequence = 1;
	m_Maxp = 0;
	m_Maxb = 0;
	m_Pral = _T("");
	m_Natr = _T("");
	m_STYP_List = _T("");
	m_PDest = _T("");
	m_Flti = _T("");
	m_ACParam = _T("");
	m_ACGroup = _T("");

	//}}AFX_DATA_INIT

	CStringArray olArray;
	
	CString olTmpBltr = pomBlt->Bltr;
	if(olTmpBltr.Replace(";","#") < 2)
	{
		olTmpBltr = pomBlt->Bltr;
		olTmpBltr.Replace("/",";");
		strcpy(pomBlt->Bltr, olTmpBltr.GetBuffer(0));
	}
	
	CString olStr = CString(pomBlt->Bltr);
	ExtractItemList(olStr, &olArray, ';');
	if(olArray.GetSize() >= 7)
	{
		m_Prio = atoi(olArray[0]);
		m_sequence = atoi(olArray[1]);
		if (olArray[1] == "0")
			m_sequence = 1;

		m_Maxp = atoi(olArray[2]);

		m_Pral = olArray[3];
		m_PDest = olArray[4];
		m_Natr = olArray[5];
		m_STYP_List = olArray[6];
	}

	if(olArray.GetSize() >= 8)
		m_Flti = olArray[7];

	if(olArray.GetSize() >= 10)
	{
		m_ACParam = olArray[8];
		m_ACGroup = olArray[9];
	}

	if(olArray.GetSize() >= 11)
	{
		m_Maxb = atoi(olArray[10]);
	}



/*
	if(olArray.GetSize() >= 10)
	{
		m_Flti = olArray[9];
	}
*/
}

BltRuleDlg ::BltRuleDlg()
{

}

void BltRuleDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(BltRuleDlg)
	DDX_Control(pDX, IDC_MAXP, m_MaxPCtrl);
	DDX_Control(pDX, IDC_MAXB, m_MaxBCtrl);
	DDX_Control(pDX, IDC_SEQU, m_SequenceCtrl);
	DDX_Control(pDX, IDC_PRIO, m_PrioCtrl);
	DDX_Text(pDX, IDC_PRIO, m_Prio);
	DDV_MinMaxInt(pDX, m_Prio, 0, 9);
	DDX_Text(pDX, IDC_SEQU, m_sequence);
	DDV_MinMaxInt(pDX, m_sequence, 1, 999);
	DDX_Text(pDX, IDC_MAXP, m_Maxp);
	DDX_Text(pDX, IDC_MAXB, m_Maxb);
	DDV_MinMaxInt(pDX, m_Maxp, 0, 999);
	DDV_MinMaxInt(pDX, m_Maxb, 0, 999);
	DDX_Text(pDX, IDC_PRAL, m_Pral);
	DDV_MaxChars(pDX, m_Pral, 150);
	DDX_Text(pDX, IDC_NATR, m_Natr);
	DDV_MaxChars(pDX, m_Natr, 150);
	DDX_Text(pDX, IDC_STYP_LIST, m_STYP_List);
	DDV_MaxChars(pDX, m_STYP_List, 150);
	DDX_Text(pDX, IDC_PDEST, m_PDest);
	DDV_MaxChars(pDX, m_PDest, 150);
	DDX_Text(pDX, IDC_FLTI, m_Flti);
	DDX_Text(pDX, IDC_ACTYPPARAM,m_ACParam);
	DDV_MaxChars(pDX, m_ACParam, 350);
	DDX_Text(pDX,IDC_ACGROUP,m_ACGroup);
	DDV_MaxChars(pDX,m_ACGroup,350);

	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(BltRuleDlg, CDialog)
	//{{AFX_MSG_MAP(BltRuleDlg)
	ON_BN_CLICKED(IDC_BUTTON1,OnACGroup)	
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// BltRuleDlg message handlers
BOOL BltRuleDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	UpdateData(FALSE);
	CString olCaption;
	olCaption.Format(GetString(IDS_STRING1476), pomBlt->Bnam, pomBlt->Term);
	SetWindowText(olCaption);


	m_MaxPCtrl.SetTypeToInt(0, 999);
	m_SequenceCtrl.SetTypeToInt(0, 999);

	if( ( (CRulesApp *)AfxGetApp() )->CanHandleBltFlti() == FALSE)
	{
		CWnd *polWnd = GetDlgItem(IDC_STATIC_FLTI);
		if (polWnd)
			polWnd->ShowWindow(SW_HIDE);

		polWnd = GetDlgItem(IDC_FLTI);
		if (polWnd)
			polWnd->ShowWindow(SW_HIDE);
	}


	if( ( (CRulesApp *)AfxGetApp() )->CanHandleBeltBag() == FALSE)
	{
		CWnd *polWnd = GetDlgItem(IDC_STATIC_BAG);
		if (polWnd)
			polWnd->ShowWindow(SW_HIDE);

		polWnd = GetDlgItem(IDC_MAXB);
		if (polWnd)
			polWnd->ShowWindow(SW_HIDE);
	}



	// TODO: Add extra initialization here
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void BltRuleDlg::OnOK() 
{

	CString olValues;
	CString olTmp;
	CString olStr;
	CString olMsg;
	CString olTmpTxt;
	m_MaxPCtrl.GetWindowText(olTmpTxt);
	
	int ilLength = olTmpTxt.GetLength();
	bool blIsValid = true;
	if(ilLength > 3 || ilLength == 0)
		blIsValid = false;
	for (int illc = 0;(illc < ilLength) && blIsValid;illc++)
	{
		CString olDummy = olTmpTxt[illc];
		int ilfsc = atoi(olDummy);
		if((atoi(olDummy) == 0) && (olDummy.Find("0") < 0))
		{
			blIsValid = false;
		}
	}

	if(	!blIsValid )
	{
		MessageBox(GetString(IDS_STRING1441), GetString(IDS_STRING117), MB_OK);
		m_MaxPCtrl.SetFocus();
		return;
	}


	m_MaxBCtrl.GetWindowText(olTmpTxt);
	
	ilLength = olTmpTxt.GetLength();
	blIsValid = true;
	if(ilLength > 3 || ilLength == 0)
		blIsValid = false;
	for ( illc = 0;(illc < ilLength) && blIsValid;illc++)
	{
		CString olDummy = olTmpTxt[illc];
		int ilfsc = atoi(olDummy);
		if((atoi(olDummy) == 0) && (olDummy.Find("0") < 0))
		{
			blIsValid = false;
		}
	}

	if(	!blIsValid )
	{
		MessageBox(GetString(IDS_STRING1441), GetString(IDS_STRING117), MB_OK);
		m_MaxPCtrl.SetFocus();
		return;
	}




	m_PrioCtrl.GetWindowText(olTmpTxt);

	ilLength = olTmpTxt.GetLength();
	blIsValid = true;
	if(ilLength > 1 || ilLength == 0)
		blIsValid = false;
	for (illc = 0;(illc < ilLength) && blIsValid;illc++)
	{
		CString olDummy = olTmpTxt[illc];
		int ilfsc = atoi(olDummy);
		if((atoi(olDummy) == 0) && (olDummy.Find("0") < 0))
		{
			blIsValid = false;
		}
	}

	if(	!blIsValid )
	{
		MessageBox(GetString(IDS_STRING1443), GetString(IDS_STRING117), MB_OK);
		m_PrioCtrl.SetFocus();
		return;
	}

	
	m_SequenceCtrl.GetWindowText(olTmpTxt);
	
	ilLength = olTmpTxt.GetLength();
	blIsValid = true;
	if(ilLength > 3 || ilLength == 0)
		blIsValid = false;

	int ilfsc = atoi(olTmpTxt);
	if(ilfsc < 1 || ilfsc > 999)
	{
		blIsValid = false;
	}

	if(	!blIsValid )
	{
		MessageBox(GetString(IDS_STRING1467), GetString(IDS_STRING117), MB_OK);
		m_SequenceCtrl.SetFocus();
		return;
	}



	UpdateData(TRUE);

	//prio
	olStr.Format("%d", m_Prio);
	olValues+=olStr+";";

	//sequence
	char bufferTmp[128];
	itoa(m_sequence, bufferTmp, 10);
	CString olStrSequ = CString(bufferTmp);
	olValues += olStrSequ+";";

	//pax
	olStr.Format("%d", m_Maxp);
	olValues+=olStr+";";

	//alc
	CStringArray olArray;
	ExtractItemList(m_Pral, &olArray, ' ');
	CString olTmp2;	
	for(int i = 0; i < olArray.GetSize(); i++)
	{
		olTmp2 = olArray[i].GetBuffer(0);
		if(olTmp2.GetLength() > 1)
		{
			if(olTmp2[0] == '!' || olTmp2[0] == '=')
				olTmp2 = olTmp2.Right(olTmp2.GetLength() - 1);


			ALTDATA *prlAlt = ogAltData.GetAltByAlc2(olTmp2.GetBuffer(0));
			if(prlAlt == NULL)
			{
				prlAlt = ogAltData.GetAltByAlc3(olTmp2.GetBuffer(0));
				if(prlAlt == NULL)
				{
					olTmp.Format(GetString(IDS_STRING1430), olTmp2);
					olMsg+=olTmp;
				}
			}
		}
	}
	olValues+=m_Pral+";";

	//org
	ExtractItemList(m_PDest, &olArray, ' ');
	for(i = 0; i < olArray.GetSize(); i++)
	{
		olTmp2 = olArray[i].GetBuffer(0);
		if(olTmp2.GetLength() > 1)
		{
			if(olTmp2[0] == '!' || olTmp2[0] == '=')
				olTmp2 = olTmp2.Right(olTmp2.GetLength() - 1);


			if(ogAptData.GetUrnoByApc(olTmp2.GetBuffer(0)) == 0)
			{
				olTmp.Format(GetString(IDS_STRING1456), olTmp2);
				olMsg+=olTmp;
			}
		}
	}
	olValues+=m_PDest+";";


	//nature
	ExtractItemList(m_Natr, &olArray, ' ');
	for(i = 0; i < olArray.GetSize(); i++)
	{
		olTmp2 = olArray[i].GetBuffer(0);
		if(olTmp2.GetLength() > 1)
		{
			if(olTmp2[0] == '!' || olTmp2[0] == '=')
				olTmp2 = olTmp2.Right(olTmp2.GetLength() - 1);


			if(ogNatData.GetUrnoByTtyp(olTmp2.GetBuffer(0)) == 0)
			{
				olTmp.Format(GetString(IDS_STRING1466), olTmp2);
				olMsg+=olTmp;
			}
		}
	}
	olValues+=m_Natr+";";

	//servicetypes
	ExtractItemList(m_STYP_List, &olArray, ' ');
	for(i = 0; i < olArray.GetSize(); i++)
	{
		olTmp2 = olArray[i].GetBuffer(0);
		if(olTmp2.GetLength() > 0)
		{
			if(olTmp2[0] == '!' || olTmp2[0] == '=')
				olTmp2 = olTmp2.Right(olTmp2.GetLength() - 1);
		
			CString olUrno;
			if(!ogBCD.GetField("STY", "STYP", olTmp2, "URNO", olUrno))
			{
				olTmp.Format(GetString(IDS_STRING1463), olTmp2);
				olMsg+=olTmp;
			}
		}
	}
	olValues += m_STYP_List+";";


	//flight id
	if( ( (CRulesApp *)AfxGetApp() )->CanHandleBltFlti() == FALSE)
	{
		m_Flti = "";
	}
	olValues += m_Flti + ";";


	//AC Parameter 
	ExtractItemList(m_ACParam, &olArray, ' ');
	for(i = 0; i < olArray.GetSize(); i++)
	{

		olTmp2 = olArray[i].GetBuffer(0);

		if(olTmp2.GetLength() > 1)
		{
			if(olTmp2[0] == '!' || olTmp2[0] == '=')
				olTmp2 = olTmp2.Right(olTmp2.GetLength() - 1);
		
		
			ACTDATA *prlAct = ogActData.GetActByAct3(olTmp2.GetBuffer(0));
			if(prlAct == NULL)
			{
				prlAct = ogActData.GetActByAct5(olTmp2.GetBuffer(0));
				if(prlAct == NULL)
				{
					olTmp.Format(GetString(IDS_STRING1431), olTmp2);
					olMsg+=olTmp;
				}
			}
		}
	}

	olValues += m_ACParam+";";

	//AC Group
	bool ol_GroupFound = FALSE;
	if(!m_ACGroup.IsEmpty())
	{
		CCSPtrArray<GRNDATA> olGrnList;
		char pclTableName[16] = "ACTTAB";
		ogGrnData.GetGrnsByTabn(olGrnList, pclTableName, "POPS");
		int ilCount = olGrnList.GetSize();

		CStringArray olArray;
		ExtractItemList(m_ACGroup, &olArray, ' ');
		CString olTmp2;	
		for(i = 0; i < olArray.GetSize(); i++)
		{
			olTmp2 = olArray[i].GetBuffer(0);

			if(olTmp2.GetLength() > 1)
			{
				if(olTmp2[0] == '!' || olTmp2[0] == '=')
				olTmp2 = olTmp2.Right(olTmp2.GetLength() - 1);
				
				for(int i = 0; i < ilCount; i++)
				{
					if(olGrnList[i].Grpn == olTmp2)
					ol_GroupFound = TRUE;
				}
				if(!ol_GroupFound)
				{
					olTmp.Format(GetString(IDS_GROUP_ERROR), olTmp2);
					olMsg+=olTmp;
				}

			}
			ol_GroupFound = FALSE;
		}
	}

	olValues += m_ACGroup;
/*
	//flight id
	if( ( (CRulesApp *)AfxGetApp() )->CanHandleBltFlti() == FALSE)
	{
		m_Flti = "";
	}
	olValues += m_Flti;
*/


	if( ( (CRulesApp *)AfxGetApp() )->CanHandleBeltBag() == TRUE)
	{
		olStr.Format("%d", m_Maxb);
		olValues+= ";" + olStr;
	}


	//checking
	if(olMsg.IsEmpty())
	{
		if (olStr.GetLength() > 2000)
		{
			MessageBox(GetString(IDS_STRING1468), GetString(IDS_STRING117), MB_OK);
			return;
		}

		//save data
		strncpy(pomBlt->Bltr, olValues.GetBuffer(0), 2000);
		pomBlt->IsChanged = DATA_CHANGED;
		ogBltData.SaveBLT( pomBlt);
		CDialog::OnOK();
	}
	else
	{
		MessageBox(olMsg, GetString(IDS_STRING117), MB_OK);
	}
}

void BltRuleDlg::OnACGroup()
{

	CAirCraftGroupSelDlg dlg(this, CString("ACTTAB"));
	//dlg.DoModal();
	if (dlg.DoModal() == IDOK)
	{
		m_ACGroup = dlg.omGrpStr;
		UpdateData(FALSE);
	}
}
