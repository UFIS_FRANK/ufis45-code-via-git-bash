// BlaRulesViewer.h: Schnittstelle f�r die Klasse BlaRulesViewer.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_BlaRulesViewer_H__7CC9F681_68A9_11D2_A4B5_0000B45A33F5__INCLUDED_)
#define AFX_BlaRulesViewer_H__7CC9F681_68A9_11D2_A4B5_0000B45A33F5__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

struct BLATABLE_LINEDATA
{
	int Urno;
	CString Ftyp;
	CString Agrp;
	CString Ogrp;
	int     Time;
};



#include "CVIEWER.H"
#include "CCSTable.H"
#include "CedaBlaData.h"


class BlaRulesViewer : public CViewer  
{
public:
	BlaRulesViewer();
	virtual ~BlaRulesViewer();

	void Attach(CCSTable *popAttachWnd);
    //@ManMemo: ChangeViewTo
    void ChangeViewTo(const char *pcpViewName);
    //@ManMemo: omLines
    CCSPtrArray<BLATABLE_LINEDATA> omLines;
	bool bmNoUpdatesNow;
private:
	
	
	int CompareBla(BLATABLE_LINEDATA *prpBla1, BLATABLE_LINEDATA *prpBla2);

    void MakeLines();

	void MakeBlaLineData(BLADATA *popBla, CStringArray &ropArray);
	int MakeLine(BLADATA *prpBla);
	int MakeLine();
	void MakeHeaderData();
	void MakeColumnData(CCSPtrArray<TABLE_COLUMN> &ropColList,BLATABLE_LINEDATA *prpLine);

	BOOL FindLine(char *pcpKeya, char *pcpKeyd, int &rilLineno);
	BOOL FindLine(long lpRtnr, int &rilLineno);
public:
    //@ManMemo: DeleteAll
	void DeleteAll();
    //@ManMemo: CreateLine
	//@ManMemo: SetStartEndTime
	int CreateLine(BLATABLE_LINEDATA *prpBla);
    //@ManMemo: DeleteLine
	void DeleteLine(int ipLineno);
	void ClearAll();


// Window refreshing routines
public:


    //@ManMemo: UpdateDisplay
	void UpdateDisplay();
private:
    CString omDate;
	// Attributes
private:
	CCSTable *pomTable;
	CCSPtrArray <TABLE_HEADER_COLUMN> omHeaderDataArray;
	int imTotalLines;

// Methods which handle changes (from Data Distributor)
public:
    //@ManMemo: omStartTime
    
    //@ManMemo: omEndTime
	
    //@ManMemo: omDay
	

};

#endif // !defined(AFX_BlaRulesViewer_H__7CC9F681_68A9_11D2_A4B5_0000B45A33F5__INCLUDED_)
