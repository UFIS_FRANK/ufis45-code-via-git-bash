// CedaALTData.h

#ifndef __CEDAALTDATA__
#define __CEDAALTDATA__
 
#include <stdafx.h>
#include <basicdata.h>

//---------------------------------------------------------------------------------------------------------
// Record structure declaration

//@See: CedaALTData
//@Doc:	A structure for reading Duty data. We read all data from database and put them
//		into this struct and store the data in omData.  

//---------------------------------------------------------------------------------------------------------
struct ALTDATA 
{
	long 	 Urno; 		// Eindeutige Datensatz-Nr.
	CTime	 Cdat;		// Erstellungsdatum
	char 	 Usec[34]; 	// Anwender (Ersteller)
	CTime	 Lstu;	 	// Datum letzte �nderung
	char 	 Prfl[3]; 	// Protokollierungskennzeichen
	char 	 Useu[34]; 	// Anwender (letzte �nderung)
	char 	 Alc2[4]; 	// Fluggesellschaft 2-Letter Code
	char 	 Alc3[5]; 	// Fluggesellschaft 3-Letter Code
	char 	 Alfn[34]; 	// Fluggesellschaft Name
	char 	 Cash[3]; 	// Barzahler Y/N
	CTime	 Vafr;	 	// G�ltig von
	CTime	 Vato;	 	// G�ltig bis

	//DataCreated by this class
	int		 IsChanged;

	ALTDATA(void)
	{ 
		memset(this,'\0',sizeof(*this));
		Cdat=-1;Lstu=-1;Vafr=-1;Vato=-1;
	}

}; // end ALTDataStrukt


//---------------------------------------------------------------------------------------------------------
// Class declaratino

//@Memo: Read and write ALT(duty) data
//@See: CedaData, CedaDutyData
/*@Doc:
  Reads and writes ALT(duty) data from and to database. Stores ALT data in memory and
  provides methods for searching and retrieving ALTs from its internal list. Does
  some data preparations.

  Data of this class has to be updated by CEDA through broadcasts. This procedure and the
  necessary methods will be described later with the description of broadcast handling.

  {\bf CedaALTData} handles data from the server process. 
  See the specification for table descriptions.*/

class CedaALTData: public CCSCedaData
{
// Attributes
public:
    //@ManMemo: A map, containing the Urno field of all loaded ALTs.
    CMapPtrToPtr omUrnoMap;
    CMapStringToPtr omAlc2Map;
    CMapStringToPtr omAlc3Map;

    //@ManMemo: ALTDATA records read by ReadAllALT().
    CCSPtrArray<ALTDATA> omData;

	void SetTableName(CString opTableName)
	{
		strcpy(pcmTableName, opTableName.GetBuffer(0));
	}
// Operations
public:
    //@ManMemo: Default constructor
    /*@Doc:
      Initialize {\bf omRecInfo} for offsets and types for each fields in {\bf ALTDATA},
      the {\bf pcmTableName} with table name {\bf ALTLSG}, the data members {\bf pcmFieldList}
      contains a list of used fields of {\bf ALTDATA}.
    */
    CedaALTData();
	~CedaALTData();

	void Register(void);
	void ClearAll(void);

    //@ManMemo: Read all ALTs.
    /*@Doc:
      Read all ALTs. 

      This method will clear {\bf omData} and {\bf omKeyMap}. Convert each record
      to a ALTDATA and store it in {\bf omData} and the Urno field in {\bf omKeyMap}.
    */
    bool ReadAllALTs();
    //@AreMemo: Read all ALT-Data corresponding to Where-condition
	bool ReadSpecial(CCSPtrArray<ALTDATA> &ropAlt,char *pspWhere);
    //@ManMemo: Add a new ALT and store the new data to the Database.
	bool InsertALT(ALTDATA *prpALT,BOOL bpSendDdx = TRUE);
    //@ManMemo: Add data for a new ALT to the internal data.
	bool InsertALTInternal(ALTDATA *prpALT);
	//@ManMemo: Change the data of a special ALT
	bool UpdateALT(ALTDATA *prpALT,BOOL bpSendDdx = TRUE);
    //@AreMemo: Change the data of a special ALT in the internal data.
	bool UpdateALTInternal(ALTDATA *prpALT);
    //@ManMemo: Delete a special ALT, also in the Database.
	bool DeleteALT(long lpUrno);
    //@ManMemo: Delete a special ALT only in the internal data.
	bool DeleteALTInternal(ALTDATA *prpALT);
    //@AreMemo: Selects all ALTs with a special Urno.
	ALTDATA  *GetALTByUrno(long lpUrno);
	ALTDATA  *GetAltByAlc2(char *pspAlc2);
	ALTDATA  *GetAltByAlc3(char *pspAlc3);
    //@ManMemo: Search a ALT with a special Key.
	CString ALTExists(ALTDATA *prpALT);
    //@AreMemo: Search a Alc3
	bool ExistAlc3(char* popAlc3);
	//@ManMemo: Writes a special ALT to the Database.
	bool SaveALT(ALTDATA *prpALT);
    //@ManMemo: Contains the valid data for all existing ALTs.
	char pcmALTFieldList[2048];
    //@ManMemo: Handle Broadcasts for ALTs.
	void ProcessALTBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
	long GetUrnoByAlc(char *pspAlc);
	// Private methods
private:
    void PrepareALTData(ALTDATA *prpALTData);
};

extern CedaALTData ogAltData;

//---------------------------------------------------------------------------------------------------------

#endif //__CEDAALTDATA__
