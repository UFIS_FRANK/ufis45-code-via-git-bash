// CedaPerData.h

#ifndef __CEDAPERDATA__
#define __CEDAPERDATA__
 
#include <stdafx.h>
#include <basicdata.h>

//---------------------------------------------------------------------------------------------------------

struct PERDATA 
{
	CTime	 Cdat; 		// Erstellungsdatum
	CTime	 Lstu;	 	// Datum letzte �nderung
	char 	 Prfl[3]; 	// Protokollierungskennzeichen
	char 	 Prmc[7]; 	// Code
	char 	 Prmn[42]; 	// Bezeichnung
	char 	 Rema[62]; 	// Bemerkung
	long 	 Urno;		// Eindeutige Datensatz-Nr.
	char 	 Usec[34]; 	// Anwender (Ersteller)
	char 	 Useu[34]; 	// Anwender (letzte �nderung)

	//DataCreated by this class
	int		 IsChanged;	// Check whether Data has Changed f�r Relaese

	PERDATA(void)
	{ 
		memset(this,'\0',sizeof(*this));
		IsChanged	=	DATA_UNCHANGED;
		Cdat		=	TIMENULL;
		Lstu		=	TIMENULL;
	}

}; // end PerDataStrukt

//---------------------------------------------------------------------------------------------------------
// Class declaratino

class CedaPerData: public CCSCedaData
{
// Attributes
public:
    CMapPtrToPtr omUrnoMap;

    CCSPtrArray<PERDATA> omData;

	char pcmListOfFields[2048];

	void SetTableName(CString opTableName)
	{
		strcpy(pcmTableName, opTableName.GetBuffer(0));
	}
// Operations
public:
    CedaPerData();
	~CedaPerData();
	virtual void GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType);
	void Register(void);
	void ClearAll(bool bpWithRegistration = true);
    bool Read(char *pspWhere = NULL);
	bool Insert(PERDATA *prpPer);
	bool InsertInternal(PERDATA *prpPer);
	bool Update(PERDATA *prpPer);
	bool UpdateInternal(PERDATA *prpPer);
	bool Delete(long lpUrno);
	bool DeleteInternal(PERDATA *prpPer);
	bool ReadSpecialData(CCSPtrArray<PERDATA> *popPer,char *pspWhere,char *pspFieldList,bool ipSYS=true);
	bool Save(PERDATA *prpPer);
	void ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
	PERDATA  *GetPerByUrno(long lpUrno);


	// Private methods
private:
    void PreparePerData(PERDATA *prpPerData);

};

//---------------------------------------------------------------------------------------------------------
extern CedaPerData ogPerData;

#endif //__CEDAPERDATA__
