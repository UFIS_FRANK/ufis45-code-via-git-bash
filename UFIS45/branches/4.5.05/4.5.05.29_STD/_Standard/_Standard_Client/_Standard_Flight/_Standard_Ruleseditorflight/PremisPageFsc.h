#if !defined(AFX_PREMISPAGE_H__CFE7A531_40B2_11D1_9579_0000B4392C49__INCLUDED_)
#define AFX_PREMISPAGE_H__CFE7A531_40B2_11D1_9579_0000B4392C49__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// PremisPage.h : header file
//

#include "PremisPageDetailViewer.h"
#include "PremisPageGantt.h"
#include "CedaGhpData.h"
#include "CCSDragDropCtrl.h"
#include "CedaGrnData.h"
#include "CedaPerData.h"
#include "CedaGegData.h"
#include "CedaPfcData.h"
#include "CedaPrcData.h"
#include "Table.h"
#include "CCSEdit.h"
#include "GhsList.h"
#include "CCSButtonCtrl.h"
#include "CCSTable.h"

/////////////////////////////////////////////////////////////////////////////
// PremisPage dialog
extern GhsList *pogGhsList;

struct LIST_DATA
{
	long Urno;
	char Code[20];
};


class PremisPage : public CPropertyPage
{
	DECLARE_DYNCREATE(PremisPage)

// Construction
public:
	PremisPage();
	~PremisPage();

	GHPDATA *pomCurrentGhp,
			*pomBkGhp,
			rmOldGhp;

	GPMDATA *pomCurrentGpm,
			rmOldGpm;
	int imMode;
	bool bmBarChanged;
	CCSPtrArray<GPMDATA>	omOldGpmList;
	CCSPtrArray<GPMDATA>	omGpmData;
	CCSPtrArray<GHPDATA>	omLines;
	CCSPtrArray<PERDATA>	omPerData;
	CCSPtrArray<GEGDATA>	omGegData;
	CCSPtrArray<PFCDATA>	omPfcData;
	CCSPtrArray<PRCDATA>	omPrcData;
	CUIntArray omPerUrnos;
	CUIntArray omGegUrnos;
	CUIntArray omPfcUrnos;
	CCSPtrArray<GRNDATA>	omTrraData;
	CCSPtrArray<GRNDATA>	omTrrdData;
	CCSPtrArray<GRNDATA>	omPcaaData;
	CCSPtrArray<GRNDATA>	omPcadData;
	CCSPtrArray<GRNDATA>	omAtmData;
	CCSPtrArray<GRNDATA>	omAlmaData;
	CCSPtrArray<GRNDATA>	omAlmdData;
	CCSPtrArray<GRNDATA>    omTtgaData;
	CCSPtrArray<GRNDATA>    omTtgdData;
	CCSPtrArray<GRNDATA>    omHtgaData;
	CCSPtrArray<GRNDATA>    omHtgdData;
	CCSPtrArray<GPMDATA>	omStdData;
	CCSPtrArray<GPMDATA>	omZusatzData;

	CCSPtrArray<GRNDATA>	omCicGrpData;

// Dialog Data
	//{{AFX_DATA(PremisPage)
	enum { IDD = IDD_PREMISSES_PAGE };
	CComboBox	m_Htgd;
	CComboBox	m_Htga;
	CComboBox	m_Ttgd;
	CComboBox	m_Ttga;
	CStatic	m_ScciFrame;
	CStatic	m_OsduFrame;
	CComboBox	m_Prco;
	CComboBox	m_PCAD;
	CComboBox	m_PCAA;
	CComboBox	m_TRRD;
	CComboBox	m_TRRA;
	CComboBox	m_ALMD;
	CComboBox	m_ALMA;
	CStatic	m_Resource;
	CListBox	m_Perm;
	CEdit	m_Chgr;
	CButton	m_BAwRegn;
	CCSButtonCtrl	*m_m_BAwPrco;
	CButton	m_m_BAwAct5;
	CButton	m_m_BAwActD;
	CButton	m_BAwActA;
	CStatic	m_Statusbar;
	CEdit	m_Rema;
	CStatic	m_Lstu;
	CListBox	m_LkcoPerm;
	CButton	m_Duch;
	CComboBox	m_Dtyp;
	CButton	m_Dopd7;
	CButton	m_Dopd6;
	CButton	m_Dopd5;
	CButton	m_Dopd4;
	CButton	m_Dopd3;
	CButton	m_Dopd2;
	CButton	m_Dopd1;
	CButton	m_Dopd_All;
	CButton	m_Dopa7;
	CButton	m_Dopa6;
	CButton	m_Dopa5;
	CButton	m_Dopa4;
	CButton	m_Dopa3;
	CButton	m_Dopa2;
	CButton	m_Dopa1;
	CButton	m_Dopa_All;
	CComboBox	m_Actm;
	CCSEdit	m_Act5;
	CCSEdit	m_Dest;
	int		m_CBDtyp_Idx;
	CCSEdit	m_Flca;
	CCSEdit	m_Flcd;
	CCSEdit	m_Flna;
	CCSEdit	m_Flnd;
	CCSEdit	m_Flsa;
	CCSEdit	m_Flsd;
	CCSEdit	m_Htpa;
	CCSEdit	m_Htpd;
	CCSEdit	m_Orig;
	CCSEdit	m_Prna;
	CCSEdit	m_Prsn;
	CCSEdit	m_Regn;
	CCSEdit	m_Scca;
	CCSEdit	m_Sccd;
	CCSEdit	m_Ttpa;
	CCSEdit	m_Ttpd;
	CCSEdit	m_Vi1a;
	CCSEdit	m_Vi1d;
	CCSEdit	m_Vpfr_Date;
	CCSEdit	m_Vpfr_Time;
	CCSEdit	m_Vpto_Date;
	CCSEdit	m_Vpto_Time;
	CCSEdit	m_Prta;
	CCSEdit	m_Tfdu;
	CCSEdit	m_Ttdu;
	CCSEdit	m_Dtim;
	CCSEdit	m_Pota;
	CCSEdit	m_Noma;
	CCSEdit	m_Etot;
	CCSEdit	m_Prio;
	CCSEdit	m_Didu;
	CCSEdit	m_Maxt;
	//}}AFX_DATA

/*	CEdit	m_Prta;
	CEdit	m_Tfdu;
	CEdit	m_Ttdu;
	CEdit	m_Dtim;
	CEdit	m_Pota;
	CEdit	m_Noma;
	CEdit	m_Etot;
	CEdit	m_Prio;
	CEdit	m_Didu;
*/

// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(PremisPage)
	public:
	virtual BOOL OnKillActive();
	virtual BOOL OnSetActive();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

public:
    CCSTimeScale omTimeScale;
	PremisPageDetailViewer *pomViewer;
	PremisPageGantt *pomGantt;
	CCSDragDropCtrl m_DragDropSource;
	CCSDragDropCtrl m_DragDropTarget;

	CCSTable *pomStdTable;
	CCSTable *pomSonderTable;
	CTable *pomTable;
	int imVerticalScaleIndent;
	int imLastSelectedUrno;

	void UpdateDisplay();
	CString Format(GHPDATA *prpGhp);
	void CreateLine(GHPDATA *prpGhp);

    LONG OnTableLButtonDblClk(UINT wParam, LONG lParam);
	void SetControls();
	void HideAllControls();
	void ShowAllControls();
	void ShowSpecialTables();
	LONG OnSaveButton(WPARAM wParam, LPARAM lParam);
	LONG OnNewButton(WPARAM wParam, LPARAM lParam);
	LONG OnUpdateButton(WPARAM wParam, LPARAM lParam);
	LONG OnDeleteButton(WPARAM wParam, LPARAM lParam);
	LONG OnCopyButton(WPARAM wParam, LPARAM lParam);
	LONG OnStandardButton(WPARAM wParam, LPARAM lParam);
	LONG OnAdditionalButton(WPARAM wParam, LPARAM lParam);
	LONG OnCCSEditKillFocus( UINT wParam, LPARAM lParam);
	bool CheckMussFelder();
	void SetDutyFields(GPMDATA *prpGpm);
	bool GetAllFields(bool bpWithMessageBox = true);
	void ClearMask();
	void MakeNewGantt();
	void ResetMask();
	void ShowCurrentData();
	void OnCancelPage();
	void OnBarChanged();
	void OnBarDeleted();
	void LoadPerData();
	void LoadPfcData();
	void LoadGegData();
	void MakeOsduList(char *pspVrgc);
	void MakePerUrnoList(char *pspVrgc);
	void MakePfcUrnoList(char *pspVrgc);
	void MakeGegUrnoList(char *pspVrgc);
	void UpdateAllTurnarrounds();
	void ShowPremisByUrno(long lpUrno);
	void CreateGpmFromGhsList(CWnd *popWnd, CDWordArray &ropGhsUrnos, GHPDATA *prpGhp,
								   CString opDisp = CString("D"));

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(PremisPage)
	virtual BOOL OnInitDialog();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg void OnClose();
	afx_msg void OnOK();
	afx_msg void OnPaint();
	afx_msg LONG OnUpdateDiagram(WPARAM wParam, LPARAM lParam);
	afx_msg void OnAwPrco();
	afx_msg void OnKillfocusPrco();
	afx_msg void OnKillfocusPrna();
	afx_msg void OnKillfocusPrsn();
	afx_msg void OnKillfocusVpfrDate();
	afx_msg void OnKillfocusVpfrTime();
	afx_msg void OnKillfocusVptoDate();
	afx_msg void OnKillfocusVptoTime();
	afx_msg void OnSelchangeDtyp();
	afx_msg void OnKillfocusDuch();
	afx_msg void OnKillfocusTest();
	afx_msg void OnAwRegn();
	afx_msg void OnAwAct5();
	afx_msg void OnDopaAll();
	afx_msg void OnDopdAll();
	afx_msg void OnKillfocusFlca();
	afx_msg void OnKillfocusFlna();
	afx_msg void OnKillfocusFlsa();
	afx_msg void OnKillfocusAlma();
	afx_msg void OnKillfocusOrig();
	afx_msg void OnKillfocusVi1a();
	afx_msg void OnKillfocusTrra();
	afx_msg void OnKillfocusScca();
	afx_msg void OnKillfocusPcaa();
	afx_msg void OnKillfocusTtpa();
	afx_msg void OnKillfocusHtpa();
	afx_msg void OnKillfocusFlcd();
	afx_msg void OnKillfocusFlnd();
	afx_msg void OnKillfocusFlsd();
	afx_msg void OnKillfocusAlmd();
	afx_msg void OnKillfocusDest();
	afx_msg void OnKillfocusVi1d();
	afx_msg void OnKillfocusTrrd();
	afx_msg void OnKillfocusSccd();
	afx_msg void OnKillfocusPcad();
	afx_msg void OnKillfocusTtpd();
	afx_msg void OnKillfocusHtpd();
	afx_msg void OnCancel();
	afx_msg void OnSelchangeLkcoPermVrgc();
	afx_msg void OnSelchangePerm();
	afx_msg void OnSelchangeAlma();
	afx_msg void OnSelchangeActm();
	afx_msg void OnSelchangeAlmd();
	afx_msg void OnSelchangeTrra();
	afx_msg void OnSelchangeTrrd();
	afx_msg void OnSelchangePcaa();
	afx_msg void OnSelchangePcad();
	afx_msg void OnDopa2();
	afx_msg void OnDopa1();
	afx_msg void OnDopa3();
	afx_msg void OnDopa4();
	afx_msg void OnDopa5();
	afx_msg void OnDopa6();
	afx_msg void OnDopa7();
	afx_msg void OnDopd1();
	afx_msg void OnDopd2();
	afx_msg void OnDopd3();
	afx_msg void OnDopd4();
	afx_msg void OnDopd5();
	afx_msg void OnDopd6();
	afx_msg void OnDopd7();
	afx_msg void OnChangeMaxt();
	afx_msg void OnChangePrsn();
	afx_msg void OnChangePrna();
	afx_msg void OnChangeVpfrDate();
	afx_msg void OnChangeVpfrTime();
	afx_msg void OnChangeVptoDate();
	afx_msg void OnChangeVptoTime();
	afx_msg void OnSelchangePrco();
    afx_msg LONG OnDragOver(UINT, LONG); 
    afx_msg LONG OnDrop(UINT, LONG); 
	afx_msg LONG OnTableLButtonDown(UINT wParam, LONG lParam);
	afx_msg LONG OnTableRButtonDown(UINT wParam, LONG lParam);
	afx_msg LONG OnTableDragBegin(UINT wParam, LONG lParam);
	afx_msg void OnMenuServiceDelete();
	afx_msg void OnSelchangeTtga();
	afx_msg void OnSelchangeTtgd();
	afx_msg void OnSelchangeHtga();
	afx_msg void OnSelchangeHtgd();
	afx_msg void OnChar(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);

	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PREMISPAGE_H__CFE7A531_40B2_11D1_9579_0000B4392C49__INCLUDED_)
