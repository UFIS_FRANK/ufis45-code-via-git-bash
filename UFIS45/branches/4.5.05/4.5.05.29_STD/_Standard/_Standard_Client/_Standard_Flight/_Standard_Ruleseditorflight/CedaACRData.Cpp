// CedaACRData.cpp

#include <stdafx.h>
#include <CedaACRData.h>


// Local function prototype
static void ProcessACRCf(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//--CEDADATA-----------------------------------------------------------------------------------------------


CedaACRData::CedaACRData() : CCSCedaData(&ogCommHandler)
{
    // Create an array of CEDARECINFO for ACRDATA
	//		CCS_FIELD_LONG		(<Fiel>,"<DBField>","<Description>", <Show in View 0 => NO 1 => YES>)

	BEGIN_CEDARECINFO(ACRDATA,ACRDataRecInfo)
		CCS_FIELD_LONG		(Urno,"URNO","Eindeutige Datensatz-Nr.", 1)
		CCS_FIELD_DATE		(Cdat,"CDAT","Erstellungsdatum", 1)
		CCS_FIELD_CHAR_TRIM	(Usec,"USEC","Anwender (Ersteller)", 1)
		CCS_FIELD_DATE		(Lstu,"LSTU","Datum letzte �nderung", 1)
		CCS_FIELD_CHAR_TRIM	(Prfl,"PRFL","Protokollierungskennzeichen", 1)
		CCS_FIELD_CHAR_TRIM	(Useu,"USEU","Anwender (letzte �nderung)", 1)
		CCS_FIELD_CHAR_TRIM	(Regn,"REGN","Flugzeug-Kennung", 1)
		CCS_FIELD_CHAR_TRIM	(Regi,"REGI","Flugzeug-Kennung mit Bindestrich", 1)
		CCS_FIELD_CHAR_TRIM	(Act3,"ACT3","Flugzeug-Typ 3-Letter Code (IATA)", 1)
		CCS_FIELD_CHAR_TRIM	(Act5,"ACT5","Flugzeug-Typ 5-Letter Code (ICAO)", 1)
		CCS_FIELD_CHAR_TRIM	(Owne,"OWNE","Flugzeug-Besitzer", 1)
		CCS_FIELD_CHAR_TRIM	(Selc,"SELC","SELCAL", 1)
		CCS_FIELD_CHAR_TRIM	(Mtow,"MTOW","Max. Startgewicht", 1)
		CCS_FIELD_CHAR_TRIM	(Mowe,"MOWE","Max. Eigengewicht", 1)
		CCS_FIELD_CHAR_TRIM	(Apui,"APUI","APU INOP", 1)
		CCS_FIELD_CHAR_TRIM	(Main,"MAIN","Maindeck-Flugzeug", 1)
		CCS_FIELD_CHAR_TRIM	(Enna,"ENNA","Triebwerksbezeichnung", 1)
		CCS_FIELD_CHAR_TRIM	(Annx,"ANNX","L�rm-Annex", 1)
		CCS_FIELD_CHAR_TRIM	(Nose,"NOSE","Anzahl Sitze", 1)
		CCS_FIELD_CHAR_TRIM	(Noga,"NOGA","Anzahl K�chen", 1)
		CCS_FIELD_CHAR_TRIM	(Noto,"NOTO","Anzahl Toiletten", 1)
		CCS_FIELD_CHAR_TRIM	(Debi,"DEBI","Debitoren Nummer", 1)
		CCS_FIELD_DATE		(Ladp,"LADP","Letzte Abflugzeit", 1)
		CCS_FIELD_CHAR_TRIM	(Rema,"REMA","Bemerkung", 1)
		CCS_FIELD_DATE		(Vafr,"VAFR","G�ltig von", 1)
		CCS_FIELD_DATE		(Vato,"VATO","G�ltig bis", 1)
	END_CEDARECINFO //(ACRDataAStruct)
/* Macro laesst kein CString zu MBR 11.01.1999
	BEGIN_CEDARECINFO(ACRDATA,ACRDataRecInfo)
		CCS_FIELD_LONG		(Urno,"URNO",GetString(IDS_STRING162), 1)
		CCS_FIELD_DATE		(Cdat,"CDAT",GetString(IDS_STRING163), 1)
		CCS_FIELD_CHAR_TRIM	(Usec,"USEC",GetString(IDS_STRING164), 1)
		CCS_FIELD_DATE		(Lstu,"LSTU",GetString(IDS_STRING165), 1)
		CCS_FIELD_CHAR_TRIM	(Prfl,"PRFL",GetString(IDS_STRING166), 1)
		CCS_FIELD_CHAR_TRIM	(Useu,"USEU",GetString(IDS_STRING167), 1)
		CCS_FIELD_CHAR_TRIM	(Regn,"REGN",GetString(IDS_STRING168), 1)
		CCS_FIELD_CHAR_TRIM	(Regi,"REGI",GetString(IDS_STRING169), 1)
		CCS_FIELD_CHAR_TRIM	(Act3,"ACT3",GetString(IDS_STRING170), 1)
		CCS_FIELD_CHAR_TRIM	(Act5,"ACT5",GetString(IDS_STRING171), 1)
		CCS_FIELD_CHAR_TRIM	(Owne,"OWNE",GetString(IDS_STRING172), 1)
		CCS_FIELD_CHAR_TRIM	(Selc,"SELC",GetString(IDS_STRING173), 1)
		CCS_FIELD_CHAR_TRIM	(Mtow,"MTOW",GetString(IDS_STRING174), 1)
		CCS_FIELD_CHAR_TRIM	(Mowe,"MOWE",GetString(IDS_STRING175), 1)
		CCS_FIELD_CHAR_TRIM	(Apui,"APUI",GetString(IDS_STRING176), 1)
		CCS_FIELD_CHAR_TRIM	(Main,"MAIN",GetString(IDS_STRING177), 1)
		CCS_FIELD_CHAR_TRIM	(Enna,"ENNA",GetString(IDS_STRING178), 1)
		CCS_FIELD_CHAR_TRIM	(Annx,"ANNX",GetString(IDS_STRING179), 1)
		CCS_FIELD_CHAR_TRIM	(Nose,"NOSE",GetString(IDS_STRING180), 1)
		CCS_FIELD_CHAR_TRIM	(Noga,"NOGA",GetString(IDS_STRING181), 1)
		CCS_FIELD_CHAR_TRIM	(Noto,"NOTO",GetString(IDS_STRING182), 1)
		CCS_FIELD_CHAR_TRIM	(Debi,"DEBI",GetString(IDS_STRING183), 1)
		CCS_FIELD_DATE		(Ladp,"LADP",GetString(IDS_STRING184), 1)
		CCS_FIELD_CHAR_TRIM	(Rema,"REMA",GetString(IDS_STRING185), 1)
		CCS_FIELD_DATE		(Vafr,"VAFR",GetString(IDS_STRING187), 1)
		CCS_FIELD_DATE		(Vato,"VATO",GetString(IDS_STRING186), 1)
	END_CEDARECINFO //(ACRDataAStruct)*/

	// Copy the record structure
	for (int i=0; i< sizeof(ACRDataRecInfo)/sizeof(ACRDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&ACRDataRecInfo[i],sizeof(CEDARECINFO));
		omRecInfo.Add(prpCedaRecInfo);
	} // end for

	// initialize field names
	strcpy(pcmTableName,"ACR");
	strcat(pcmTableName,pcgTableExt);
	strcpy(pcmACRFieldList,"URNO,CDAT,USEC,LSTU,PRFL,USEU,REGN,REGI,ACT3,ACT5,OWNE,SELC,MTOW,MOWE,APUI,MAIN,ENNA,ANNX,NOSE,NOGA,NOTO,DEBI,LADP,REMA,VAFR,VATO");
	pcmFieldList = pcmACRFieldList;
	omData.SetSize(0,1000);
	//omUrnoMap.InitHashTable(2001);
}

void CedaACRData::Register(void)
{
	ogDdx.Register((void *)this,BC_ACR_CHANGE,CString("ACRDATA"), CString("ACR-changed"),ProcessACRCf);
	ogDdx.Register((void *)this,BC_ACR_DELETE,CString("ACRDATA"), CString("ACR-deleted"),ProcessACRCf);
}

//--~CEDADATA----------------------------------------------------------------------------------------------

CedaACRData::~CedaACRData(void)
{
	omRecInfo.DeleteAll();
	ClearAll();
}

//--CLEAR-ALL----------------------------------------------------------------------------------------------

void CedaACRData::ClearAll(void)
{
    omUrnoMap.RemoveAll();
	omRegnMap.RemoveAll();
    omData.DeleteAll();
	ogDdx.UnRegister(this,NOTUSED);
}

//--READ-ALL-----------------------------------------------------------------------------------------------

bool CedaACRData::ReadAllACRs(char *pspWhere)
{
    // Select data from the database
	bool ilRc = true;
    omUrnoMap.RemoveAll();
	omRegnMap.RemoveAll();
    omData.DeleteAll();
	if(strcmp(pspWhere, "") != 0)
	{
		ilRc = CedaAction(ogBasicData.GetCedaCommand("RT"),pspWhere);
	}
	else
	{
		ilRc = CedaAction(ogBasicData.GetCedaCommand("RT"));
	}
	if (ilRc != true)
	{
		if(!omLastErrorMessage.IsEmpty())
		{
			if(omLastErrorMessage.Find("ORA") != -1)
			{
				char pclMsg[2048]="";
				sprintf(pclMsg, "%s", omLastErrorMessage.GetBuffer(0));
				::MessageBox(NULL,pclMsg,GetString(IDS_STRING188),MB_OK);
			}
		}
		return false;
	}
	// Load data from CedaData into the dynamic array of record
	for (int ilLc = 0; ilRc == true; ilLc++)
	{
		ACRDATA *prpACR = new ACRDATA;
		if ((ilRc = GetBufferRecord(ilLc,prpACR)) == true)
		{
			prpACR->IsChanged = DATA_UNCHANGED;
			omData.Add(prpACR);//Update omData
			omUrnoMap.SetAt((void *)prpACR->Urno,prpACR);
			omRegnMap.SetAt(prpACR->Regn,prpACR);
		}
		else
		{
			delete prpACR;
		}
	}
    return true;
}

//--INSERT-------------------------------------------------------------------------------------------------

bool CedaACRData::InsertACR(ACRDATA *prpACR,BOOL bpSendDdx)
{
	prpACR->IsChanged = DATA_NEW;
	if(SaveACR(prpACR) == false) return false; //Update Database
	InsertACRInternal(prpACR);
    return true;
}

//--INSERT-INTERNAL----------------------------------------------------------------------------------------

bool CedaACRData::InsertACRInternal(ACRDATA *prpACR)
{
	//PrepareACRData(prpACR);
	ogDdx.DataChanged((void *)this, ACR_CHANGE,(void *)prpACR ); //Update Viewer
	omData.Add(prpACR);//Update omData
	omUrnoMap.SetAt((void *)prpACR->Urno,prpACR);
	omRegnMap.SetAt(prpACR->Regn,prpACR);
	return true;
}

//--DELETE-------------------------------------------------------------------------------------------------

bool CedaACRData::DeleteACR(long lpUrno)
{
	ACRDATA *prlACR = GetACRByUrno(lpUrno);
	if (prlACR != NULL)
	{
		prlACR->IsChanged = DATA_DELETED;
		if(SaveACR(prlACR) == false) return false; //Update Database
		DeleteACRInternal(prlACR);
	}
    return true;
}

//--DELETE-INTERNAL----------------------------------------------------------------------------------------

bool CedaACRData::DeleteACRInternal(ACRDATA *prpACR)
{
	ogDdx.DataChanged((void *)this,ACR_DELETE,(void *)prpACR); //Update Viewer
	omUrnoMap.RemoveKey((void *)prpACR->Urno);
	omRegnMap.RemoveKey(prpACR->Regn);
	int ilACRCount = omData.GetSize();
	for (int ilLc = 0; ilLc < ilACRCount; ilLc++)
	{
		if (omData[ilLc].Urno == prpACR->Urno)
		{
			omData.DeleteAt(ilLc);//Update omData
			break;
		}
	}
    return true;
}

//--PREPARE-DATA-------------------------------------------------------------------------------------------

void CedaACRData::PrepareACRData(ACRDATA *prpACR)
{
	// TODO: add code here
}

//--UPDATE-------------------------------------------------------------------------------------------------

bool CedaACRData::UpdateACR(ACRDATA *prpACR,BOOL bpSendDdx)
{
	if (GetACRByUrno(prpACR->Urno) != NULL)
	{
		if (prpACR->IsChanged == DATA_UNCHANGED)
		{
			prpACR->IsChanged = DATA_CHANGED;
		}
		if(SaveACR(prpACR) == false) return false; //Update Database
		UpdateACRInternal(prpACR);
	}
    return true;
}

//--UPDATE-INTERNAL--------------------------------------------------------------------------------------------

bool CedaACRData::UpdateACRInternal(ACRDATA *prpACR)
{
	ACRDATA *prlACR = GetACRByUrno(prpACR->Urno);
	if (prlACR != NULL)
	{
		omRegnMap.RemoveKey(prlACR->Regn);
		*prlACR = *prpACR; //Update omData
		omRegnMap.SetAt(prlACR->Regn,prlACR);
		ogDdx.DataChanged((void *)this,ACR_CHANGE,(void *)prlACR); //Update Viewer
	}
    return true;
}

//--GET-BY-URNO--------------------------------------------------------------------------------------------

ACRDATA *CedaACRData::GetACRByUrno(long lpUrno)
{
	ACRDATA  *prlACR;
	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlACR) == TRUE)
	{
		return prlACR;
	}
	return NULL;
}

//--EXIST---------------------------------------------------------------------------------------------

bool CedaACRData::ACRExists(ACRDATA *prpACR)
{
	ACRDATA  *prlACR;
	if (omRegnMap.Lookup((LPCSTR)prpACR->Regn,(void *&)prlACR) == TRUE)
	{
		if(prlACR->Urno != prpACR->Urno) return true;
	}
	return false;
}

//--READSPECIALACRDATA-------------------------------------------------------------------------------------

bool CedaACRData::ReadSpecialAcrData(CCSPtrArray<ACRDATA> &ropAcr,char *pspWhere)
{
	bool ilRc = true;
	if (pspWhere == NULL)
	{
		ilRc = CedaAction("RT");
		if (ilRc != true)
		{
			if(!omLastErrorMessage.IsEmpty())
			{
				if(omLastErrorMessage.Find("ORA") != -1)
				{
					char pclMsg[2048]="";
					sprintf(pclMsg, "%s", omLastErrorMessage.GetBuffer(0));
					::MessageBox(NULL,pclMsg,GetString(IDS_STRING188),MB_OK);
					return false;
				}
			}
		}
	}
	else
	{
		ilRc = CedaAction("RT",pspWhere);
		if (ilRc != true)
		{
			if(!omLastErrorMessage.IsEmpty())
			{
				if(omLastErrorMessage.Find("ORA") != -1)
				{
					char pclMsg[2048]="";
					sprintf(pclMsg, "%s", omLastErrorMessage.GetBuffer(0));
					::MessageBox(NULL,pclMsg,GetString(IDS_STRING188),MB_OK);
					return false;
				}
			}
		}
	}
	for (int ilLc = 0; ilRc == true; ilLc++)
	{
		ACRDATA *prpAcr = new ACRDATA;
		if ((ilRc = GetBufferRecord(ilLc,prpAcr)) == true)
		{
			ropAcr.Add(prpAcr);
		}
		else
		{
			delete prpAcr;
		}
	}
    return true;
}

//--SAVE---------------------------------------------------------------------------------------------------

bool CedaACRData::SaveACR(ACRDATA *prpACR)
{
	bool ilRc = true;
	CString olListOfData;
	char pclSelection[124];
	char pclData[524];

	if (prpACR->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}

	switch(prpACR->IsChanged)
	{
	case DATA_NEW:
		MakeCedaData(&omRecInfo,olListOfData,prpACR);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("IRT","","",pclData);
		prpACR->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpACR->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpACR);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("URT",pclSelection,"",pclData);
		prpACR->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_DELETED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpACR->Urno);
		ilRc = CedaAction("DRT",pclSelection);
		break;
	}
	if(!omLastErrorMessage.IsEmpty())
	{
		if(omLastErrorMessage.Find("ORA") != -1)
		{
			char pclMsg[2048]="";
			sprintf(pclMsg, "%s", omLastErrorMessage.GetBuffer(0));
			::MessageBox(NULL,pclMsg,GetString(IDS_STRING188),MB_OK);
			return false;
		}
	}

    return ilRc;
}

//--PROCESS-CF---------------------------------------------------------------------------------------------

void  ProcessACRCf(void * popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	switch(ipDDXType)
	{
	case BC_ACR_CHANGE :
	case BC_ACR_DELETE :
		((CedaACRData *)popInstance)->ProcessACRBc(ipDDXType,vpDataPointer,ropInstanceName);
		break;
	}
}

//--PROCESS-BC---------------------------------------------------------------------------------------------

void  CedaACRData::ProcessACRBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlACRData;
	long llUrno;
	prlACRData = (struct BcStruct *) vpDataPointer;
	CString olSelection = (CString)prlACRData->Selection;
	if (olSelection.Find('\'') != -1)
	{
		llUrno = GetUrnoFromSelection(prlACRData->Selection);
	}
	else
	{
		int ilFirst = olSelection.Find("=")+2;
		int ilLast  = olSelection.GetLength();
		llUrno = atol(olSelection.Mid(ilFirst,ilLast-ilFirst));
	}

	ACRDATA *prlACR;
	prlACR = GetACRByUrno(llUrno);
	if(ipDDXType == BC_ACR_CHANGE)
	{
		if (prlACR != NULL)
		{
			GetRecordFromItemList(prlACR,prlACRData->Fields,prlACRData->Data);
			UpdateACRInternal(prlACR);
		}
		else
		{
			prlACR = new ACRDATA;
			GetRecordFromItemList(prlACR,prlACRData->Fields,prlACRData->Data);
			InsertACRInternal(prlACR);
		}
	}
	if(ipDDXType == BC_ACR_DELETE)
	{
		if (prlACR != NULL)
		{
			DeleteACRInternal(prlACR);
		}
	}
}

//---------------------------------------------------------------------------------------------------------
