#if !defined(AFX_DIALOGGRID_H__041C8F83_B1E0_11D2_AAEE_00001C018CF3__INCLUDED_)
#define AFX_DIALOGGRID_H__041C8F83_B1E0_11D2_AAEE_00001C018CF3__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

//////////////////////////////////////////////////////////////////////////////////
//
// GridFenster.h : header file
//
// Notes: The CGridFenster class is derived from the standard CGXGridWnd class . 
//         
//        The class offers the possibility to create Templates for the display in the main
//        FormView Window . The Dialog offers all possible fields from the TSR table in a gridform
//        display on the left hand side for the 3 areas (turnaround, arrival, departure).
//         
//        From these lists the apropriate fields can be choosen and will then appear in the corresponding 
//        grid on the right handside .
//
//        The selections can be sorted alphabeticly and lines in the grids can be moved in order to 
//        represent the priority by position .
//
//        The finiched selections are stored in the TPL table .
//
//
// Date : March 1999
//
// Author : EDE 
//
// Modification History:
//
//


#include "CCSGlobl.h"
#include <gxall.h>



typedef struct RowColPos
{
	int Row;
	int Col;

	RowColPos(void)
	{
		Row = -1;
		Col = -1;
	}

} CELLPOS;





class CGridFenster : public CGXGridWnd
{
private:
	int *pimSortIndex;
	bool *pbmDoubleSort;
	int imTopSelectedRow;
	CRowColArray omSelectedRows;

	bool bmAllowMultiSelect;
	//--- Construction / Destruction
public:
	CGridFenster();	// default constructor
	CGridFenster(CWnd *pParent);
	~CGridFenster();

//--- Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CGridFenster)
	//}}AFX_VIRTUAL


//--- Implementation
	//--- ‹berschreiben von CGXGridCore virtual Funktionen
	BOOL OnSelDragDrop(ROWCOL nStartRow, ROWCOL nStartCol, ROWCOL nDragRow, ROWCOL nDragCol);
	BOOL OnEndEditing(ROWCOL nRow, ROWCOL nCol);
	BOOL OnTrackColWidth(ROWCOL nCol);

    BOOL OnLButtonClickedRowCol(ROWCOL nRow, ROWCOL nCol, UINT nFlags, CPoint pt);
	BOOL OnLButtonDblClkRowCol(ROWCOL nRow, ROWCOL nCol, UINT nFlags, CPoint pt);
	void OnClickedButtonRowCol(ROWCOL nRow, ROWCOL nCol);
	void OnChangedSelection(const CGXRange* pRange, BOOL bIsDragging, BOOL bKey);
	void SetAllowMultiSelect(bool bpAllow = true);

	BOOL OnGridKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);

	void OnMovedCurrentCell(ROWCOL nRow, ROWCOL nCol);
	// Set
	void SetNumSort(bool b);
	void SetSortingEnabled(bool bpEnable);
	void SetColsAlwaysVisible(CPtrArray opCols);

	BOOL SetColCount(ROWCOL nCols, UINT flags = GX_UPDATENOW);
	void SetSortQuery(int ipClickedCol,int ipSortCol);

	void SetDoubleSort(int ipClickedCol,bool bpUseDoubleSort = true);

	// Get
	bool GetNumSort();
	int GetSortKey();
	int GetCurrentRow();
	int GetCurrentCol();
	int  GetColWidth(ROWCOL nCol);


//--- Generated message map functions

protected:
	//{{AFX_MSG(CGridFenster)
	afx_msg  void  OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg  void  OnLButtonDown(UINT nFlags, CPoint point);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()


//--- Attributes
	CWnd *pomParent;
	CPtrArray omVisibleCols;
	bool bmSortNumerical; //--- Ist numerisch sortiert ? 
	bool bmSortAscend;    //--- sort asdcending or descending ? 
	bool bmIsSorting;	  //--- Is sorting enabled ?
	int imSortKey; //--- Nach welchem Feld wird sortiert ?
	int imCurrentRow; 
	int imCurrentCol;	
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DIALOGGRID_H__041C8F83_B1E0_11D2_AAEE_00001C018CF3__INCLUDED_)
