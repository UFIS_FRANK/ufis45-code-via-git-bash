// FidsRemarkDlg.cpp : implementation file
//

#include "stdafx.h"
#include "rules.h"
#include "FidsRemarkDlg.h"
#include "CedaBasicData.h"
#include "CCSGLOBL.H"
#include "PremisPage.h"
#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CFidsRemarkDlg dialog


CFidsRemarkDlg::CFidsRemarkDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CFidsRemarkDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CFidsRemarkDlg)
		m_Remarks = _T("");
	//}}AFX_DATA_INIT
}


void CFidsRemarkDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CFidsRemarkDlg)
		DDX_Control(pDX, IDC_LIST1, m_CL_List);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CFidsRemarkDlg, CDialog)
	//{{AFX_MSG_MAP(CFidsRemarkDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CFidsRemarkDlg message handlers

BOOL CFidsRemarkDlg::OnInitDialog() 
{

	CDialog::OnInitDialog();
	//Sandesh//
	m_CL_List.SetExtendedStyle(LVS_EX_FULLROWSELECT);
	m_CL_List.InsertColumn(0,"Code",LVCFMT_LEFT,85,-1);
	m_CL_List.InsertColumn(1,"Description",LVCFMT_LEFT,500,-1);
	char pclFieldList[512];
	//sprintf(pclWhere, "WHERE STAT = 'O'");
	CCSCedaData* olCCSData = new CCSCedaData;
	strcpy(pclFieldList, "CODE,BEME");
	CCSPtrArray<RecordSet> pomData;
	if (!olCCSData->CedaAction("RT","FID",pclFieldList,"","","","BUF1",false,0,&pomData,2))
	{
		//CFPMSApp::MyTopmostMessageBox(this, GetString(IDS_NO_FOGTAB), GetString(ST_FEHLER), MB_OK | MB_ICONERROR);								
		return false;
	}

	CString  olRemarkCode;
	CString  olRemarkUser;
	CString olRemark;
	for(int i = 0; i < pomData.GetSize(); i++)
	{
		RecordSet rlRec = pomData.GetAt(i);
		olRemarkCode = rlRec.Values.GetAt(0) ;
		olRemarkUser = rlRec.Values.GetAt(1);	
		int nitem = m_CL_List.InsertItem(0, olRemarkCode);
		m_CL_List.SetItemText(nitem,1,olRemarkUser);
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CFidsRemarkDlg::OnOK() 
{
	char olStr[20];
	int li_index = m_CL_List.GetSelectionMark();
	m_CL_List.GetItemText(li_index,0,olStr,20);
	m_Remarks = olStr;

	CDialog::OnOK();
}
