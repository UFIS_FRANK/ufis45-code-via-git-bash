// BlaRuleDlg.cpp : implementation file
//

#include "stdafx.h"
#include "rules.h"
#include "BlaRuleDlg.h"
#include "BasicDAta.h"
#include "CedaAltData.h"
#include "CedaAPTData.h"
#include "CedaBasicData.h"
#include "CedaNATData.h"
#include "AirCraftGroupSelDlg.h"
#include "GroupSelDlg.h"
#include <CedaActData.h>
#include "GroupNamesPage.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


/////////////////////////////////////////////////////////////////////////////
// BlaRuleDlg dialog
IMPLEMENT_SERIAL(BlaRuleDlg,CDialog,0);

BlaRuleDlg::BlaRuleDlg(CWnd* pParent /*=NULL*/, BLADATA *popBla)
	: CDialog(BlaRuleDlg::IDD, pParent)
{
	pomBla = popBla;


	//{{AFX_DATA_INIT(BlaRuleDlg)
	m_Time = 0;
	m_Ftyp = _T("");
	m_Agrp = _T("");
	m_Ogrp = _T("");

	//}}AFX_DATA_INIT

	m_Ftyp = pomBla->Ftyp;
	m_Time = pomBla->Time;
	m_Agrp = pomBla->Agrp;
	m_Ogrp = pomBla->Ogrp;

}

BlaRuleDlg ::BlaRuleDlg()
{

}

void BlaRuleDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(BlaRuleDlg)
	DDX_Control(pDX, IDC_FTYP, m_FtypCtrl);
	DDX_Control(pDX, IDC_AGRP, m_AgrpCtrl);
	DDX_Control(pDX, IDC_OGRP, m_OgrpCtrl);
	DDX_Control(pDX, IDC_TIME, m_TimeCtrl);
	DDX_Text(pDX, IDC_FTYP, m_Ftyp);
	DDX_Text(pDX, IDC_AGRP, m_Agrp);
	DDX_Text(pDX, IDC_OGRP, m_Ogrp);
	DDX_Text(pDX, IDC_TIME, m_Time);
//	DDV_MinMaxInt(pDX, m_Time, 1, 999);


//	DDV_MaxChars(pDX,m_Ftyp,1);

	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(BlaRuleDlg, CDialog)
	//{{AFX_MSG_MAP(BlaRuleDlg)
	ON_BN_CLICKED(IDC_AGRP_LIST,OnACGroup)	
	ON_BN_CLICKED(IDC_OGRP_LIST,OnOriGroup)	
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// BlaRuleDlg message handlers
BOOL BlaRuleDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	UpdateData(FALSE);
	CString olCaption;
//	olCaption.Format(GetString(IDS_STRING1476), pomBla->Bnam, pomBla->Term);
	SetWindowText(olCaption);


	m_TimeCtrl.SetTypeToInt(0, 999);


	// TODO: Add extra initialization here
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void BlaRuleDlg::OnOK() 
{

	CString olValues;
	CString olTmp;
	CString olStr;
	CString olMsg;
	CString olTmpTxt;

	m_TimeCtrl.GetWindowText(olTmpTxt);
	
	int ilLength = olTmpTxt.GetLength();
	bool blIsValid = true;
	if(ilLength > 4 || ilLength == 0)
		blIsValid = false;
	for (int illc = 0;(illc < ilLength) && blIsValid;illc++)
	{
		CString olDummy = olTmpTxt[illc];
		int ilfsc = atoi(olDummy);
		if((atoi(olDummy) == 0) && (olDummy.Find("0") < 0))
		{
			blIsValid = false;
		}
	}

	if(	!blIsValid )
	{
		MessageBox(GetString(IDS_STRING1441), GetString(IDS_STRING117), MB_OK);
		m_TimeCtrl.SetFocus();
		return;
	}


	m_AgrpCtrl.GetWindowText(olTmpTxt);

	if ( !olTmpTxt.IsEmpty() && ogGrnData.GetUrnoByName((char *) LPCSTR(olTmpTxt), "ACTTAB", "POPS") <= 0)
	{
		MessageBox(GetString(IDS_STRING1494), GetString(IDS_STRING117), MB_OK);
		m_AgrpCtrl.SetFocus();
		return;
	}

	m_OgrpCtrl.GetWindowText(olTmpTxt);

	if (!olTmpTxt.IsEmpty() && ogGrnData.GetUrnoByName((char *) LPCSTR(olTmpTxt), "APTTAB", "POPS") <= 0)
	{
		MessageBox(GetString(IDS_STRING1494), GetString(IDS_STRING117), MB_OK);
		m_OgrpCtrl.SetFocus();
		return;
	}

	m_FtypCtrl.GetWindowText(olTmpTxt);
	if (olTmpTxt.GetLength() != 1) 
	{
		MessageBox(GetString(IDS_STRING1495), GetString(IDS_STRING117), MB_OK);
		m_FtypCtrl.SetFocus();
		return;

	}

	UpdateData(TRUE);

	CDialog::OnOK();
}

bool BlaRuleDlg::GetBlaData(BLADATA *popBla)
{
	if (pomBla != NULL)
	{
		popBla->Urno = pomBla->Urno;
	}
	popBla->Time = m_Time;
	strcpy(popBla->Ftyp,m_Ftyp);
	strcpy(popBla->Ogrp, m_Ogrp);
	strcpy(popBla->Agrp,m_Agrp);

	return true;
}

void BlaRuleDlg::OnACGroup()
{

	UpdateData(TRUE);
	CGroupSelDlg dlg(this, CString("ACTTAB"));
	//dlg.DoModal();
	if (dlg.DoModal() == IDOK)
	{
		m_Agrp = dlg.omGrpStr;
		UpdateData(FALSE);
	}
}

void BlaRuleDlg::OnOriGroup()
{
	UpdateData(TRUE);
	CGroupSelDlg dlg(this, CString("APTTAB"));
	//dlg.DoModal();
	if (dlg.DoModal() == IDOK)
	{
		m_Ogrp = dlg.omGrpStr;
		UpdateData(FALSE);
	}
}
