#if !defined(AFX_GATRULEDLG_H__FB4A7141_F271_11D2_A1A5_0000B4984BBE__INCLUDED_)
#define AFX_GATRULEDLG_H__FB4A7141_F271_11D2_A1A5_0000B4984BBE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// GatRuleDlg.h : header file
//

#include <CedaGatData.h>
/////////////////////////////////////////////////////////////////////////////
// GatRuleDlg dialog

class GatRuleDlg : public CDialog
{
// Construction
public:
	GatRuleDlg(CWnd* pParent, GATDATA *popGat);   // standard constructor
	GatRuleDlg();

// Dialog Data
	//{{AFX_DATA(GatRuleDlg)
	enum { IDD = IDD_GAT_RULE_DLG };
	CEdit	m_SequenceCtrl;
	CEdit	m_PrioCtrl;
	CEdit	m_MultCtrl;
	CEdit	m_MaxPCtrl;
	CString	m_Flti;
	BOOL	m_Inbo;
	int		m_Maxp;
	int		m_Mult;
	CString	m_Natr;
	BOOL	m_Outb;
	CString	m_Pral;
	int		m_Prio;
	CString	m_PDest;
	CString	m_FltiDep;
	int		m_sequence;
	CString	m_STYP_List;
	CString	m_Npos1;
	CString	m_Npos2;
	CString	m_Npos3;
	CString	m_Npos4;
	CString	m_Npos5;
	CString	m_Npos6;
	CString	m_Npos7;
	CString	m_Npos8;
	CString	m_Npos9;
	CString	m_Npos10;
	CString m_ACParam;
	CString m_ACGroup;
	BOOL	m_Restr_To1;
	BOOL	m_Restr_To2;
	BOOL	m_Restr_To3;
	BOOL	m_Restr_To4;
	BOOL	m_Restr_To5;
	BOOL	m_Restr_To6;
	BOOL	m_Restr_To7;
	BOOL	m_Restr_To8;
	BOOL	m_Restr_To9;
	BOOL	m_Restr_To10;
	//}}AFX_DATA

	GATDATA *pomGat;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(GatRuleDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(GatRuleDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	afx_msg void OnALMatrix1();
	afx_msg void OnALMatrix2();
	afx_msg void OnALMatrix3();
	afx_msg void OnALMatrix4();
	afx_msg void OnALMatrix5();
	afx_msg void OnALMatrix6();
	afx_msg void OnALMatrix7();
	afx_msg void OnALMatrix8();
	afx_msg void OnALMatrix9();
	afx_msg void OnALMatrix10();
	afx_msg void OnAPMatrix1();
	afx_msg void OnAPMatrix11();
	afx_msg void OnAPMatrix12();
	afx_msg void OnAPMatrix13();
	afx_msg void OnAPMatrix14();
	afx_msg void OnAPMatrix15();
	afx_msg void OnAPMatrix16();
	afx_msg void OnAPMatrix17();
	afx_msg void OnAPMatrix18();
	afx_msg void OnAPMatrix19();
	void OnACGroup();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
		DECLARE_SERIAL(GatRuleDlg)
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_GATRULEDLG_H__FB4A7141_F271_11D2_A1A5_0000B4984BBE__INCLUDED_)
