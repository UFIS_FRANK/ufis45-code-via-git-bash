#ifndef _CEDAALODATA_H_
#define _CEDAALODATA_H_

#include <CCSCedaData.h>
#include <CCSPtrArray.h>

/////////////////////////////////////////////////////////////////////////////
// Record structure declaration

 // Data fields from table ALOTAB - Allocation Types
struct AloDataStruct 
{
	long	Urno;		// Unique record number
	char	Aloc[11];	// Allocation Code eg "GAT" or "GATEAREA"
	char	Alod[33];	// Allocation Description eg "Gate" or "Gate Bareich"
	char	Alot[2];	// Allocation type eg "0"=Unit "1"=Group
	char	Reft[9];	// Reference Table eg "GAT.GNAM" of group owner
	char	Refm[9];	// Reference Table of group member	

	AloDataStruct(void)
	{
		memset(this,0,sizeof(struct AloDataStruct));
	}
};
typedef struct AloDataStruct ALODATA;

/////////////////////////////////////////////////////////////////////////////
// Class declaration

class CedaAloData: public CCSCedaData
{

public:
    CCSPtrArray<ALODATA> omData;
	CMapPtrToPtr omUrnoMap;
	CString GetTableName(void);

public:
    CedaAloData();
    ~CedaAloData();
	void ClearAll();
	BOOL ReadAloData();
	BOOL AddAloInternal(ALODATA *prpAlo);
	void GetAllGroupTypes(CCSPtrArray <ALODATA> &ropGroupTypes, bool bpReset=true);
	ALODATA *GetAloByUrno(long lpUrno);
	long GetAloUrnoByName(const char *pcpName);	
	ALODATA *GetAloByName(const char *pcpName);	
	CString GetAlocByUrno(long lpUrno);
};

extern CedaAloData ogAloData;
#endif
