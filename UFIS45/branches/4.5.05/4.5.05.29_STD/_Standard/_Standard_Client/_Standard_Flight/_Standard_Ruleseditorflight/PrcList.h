#if !defined(AFX_PRCLIST_H__60B207E1_83B4_11D1_B439_0000B45A33F5__INCLUDED_)
#define AFX_PRCLIST_H__60B207E1_83B4_11D1_B439_0000B45A33F5__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// PrcList.h : header file
//

#include "resource.h"
/////////////////////////////////////////////////////////////////////////////
// PrcList dialog

class PrcList : public CDialog
{
// Construction
public:
	PrcList(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(PrcList)
	enum { IDD = IDD_PRC_DLG };
	CListBox	m_List;
	//}}AFX_DATA

	CString omRedu;
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(PrcList)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(PrcList)
	afx_msg void OnDblclkList();
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PRCLIST_H__60B207E1_83B4_11D1_B439_0000B45A33F5__INCLUDED_)
