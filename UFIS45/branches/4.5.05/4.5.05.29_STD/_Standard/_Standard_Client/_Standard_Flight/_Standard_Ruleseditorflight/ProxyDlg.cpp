// DlgProxy.cpp : implementation file
//

#include "stdafx.h"
#include "Rules.h"
#include "ProxyDlg.h"
#include "ButtonListDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CAutoProxyDlg

IMPLEMENT_DYNCREATE(CAutoProxyDlg, CCmdTarget)

CAutoProxyDlg::CAutoProxyDlg()
{
	EnableAutomation();
	
	// To keep the application running as long as an OLE automation 
	//	object is active, the constructor calls AfxOleLockApp.
	AfxOleLockApp();

	// Get access to the dialog through the application's
	//  main window pointer.  Set the proxy's internal pointer
	//  to point to the dialog, and set the dialog's back pointer to
	//  this proxy.
	ASSERT (AfxGetApp()->m_pMainWnd != NULL);
	ASSERT_VALID (AfxGetApp()->m_pMainWnd);
	ASSERT_KINDOF(CButtonListDlg, AfxGetApp()->m_pMainWnd);
	m_pDialog = (CButtonListDlg*) AfxGetApp()->m_pMainWnd;
	m_pDialog->m_pAutoProxy = this;
}

CAutoProxyDlg::~CAutoProxyDlg()
{
	// To terminate the application when all objects created with
	// 	with OLE automation, the destructor calls AfxOleUnlockApp.
	//  Among other things, this will destroy the main dialog
	AfxOleUnlockApp();
}

void CAutoProxyDlg::OnFinalRelease()
{
	// When the last reference for an automation object is released
	// OnFinalRelease is called.  The base class will automatically
	// deletes the object.  Add additional cleanup required for your
	// object before calling the base class.

	CCmdTarget::OnFinalRelease();
}

BEGIN_MESSAGE_MAP(CAutoProxyDlg, CCmdTarget)
	//{{AFX_MSG_MAP(CAutoProxyDlg)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

BEGIN_DISPATCH_MAP(CAutoProxyDlg, CCmdTarget)
	//{{AFX_DISPATCH_MAP(CAutoProxyDlg)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_DISPATCH_MAP
END_DISPATCH_MAP()

// Note: we add support for IID_IRules to support typesafe binding
//  from VBA.  This IID must match the GUID that is attached to the 
//  dispinterface in the .ODL file.

// {FC3BA6F4-1AED-11D1-82C3-0080AD1DC701}
static const IID IID_IRules =
{ 0xfc3ba6f4, 0x1aed, 0x11d1, { 0x82, 0xc3, 0x0, 0x80, 0xad, 0x1d, 0xc7, 0x1 } };

BEGIN_INTERFACE_MAP(CAutoProxyDlg, CCmdTarget)
	INTERFACE_PART(CAutoProxyDlg, IID_IRules, Dispatch)
END_INTERFACE_MAP()

// The IMPLEMENT_OLECREATE2 macro is defined in StdAfx.h of this project
// {FC3BA6F2-1AED-11D1-82C3-0080AD1DC701}
IMPLEMENT_OLECREATE2(CAutoProxyDlg, "Rules.Application", 0xfc3ba6f2, 0x1aed, 0x11d1, 0x82, 0xc3, 0x0, 0x80, 0xad, 0x1d, 0xc7, 0x1)

/////////////////////////////////////////////////////////////////////////////
// CAutoProxyDlg message handlers
