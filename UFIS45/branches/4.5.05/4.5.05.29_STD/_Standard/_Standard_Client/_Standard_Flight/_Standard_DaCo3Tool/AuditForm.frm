VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "TAB.ocx"
Begin VB.Form AuditForm 
   Caption         =   "Audit Log"
   ClientHeight    =   6960
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   11565
   LinkTopic       =   "Form1"
   ScaleHeight     =   6960
   ScaleWidth      =   11565
   StartUpPosition =   1  'CenterOwner
   Begin MSComctlLib.ImageList imlAudit 
      Left            =   3840
      Top             =   2400
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   2
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "AuditForm.frx":0000
            Key             =   "refresh"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "AuditForm.frx":039A
            Key             =   "close"
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.Toolbar tbrAudit 
      Align           =   1  'Align Top
      Height          =   330
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   11565
      _ExtentX        =   20399
      _ExtentY        =   582
      ButtonWidth     =   1720
      ButtonHeight    =   582
      Style           =   1
      TextAlignment   =   1
      ImageList       =   "imlAudit"
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   3
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Refresh"
            Key             =   "refresh"
            Description     =   "Refresh data"
            ImageIndex      =   1
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Close"
            Key             =   "close"
            Description     =   "Close the dialog"
            ImageIndex      =   2
         EndProperty
      EndProperty
   End
   Begin TABLib.TAB tabAudit 
      Height          =   1485
      Left            =   600
      TabIndex        =   1
      Top             =   1320
      Width           =   2055
      _Version        =   65536
      _ExtentX        =   3625
      _ExtentY        =   2619
      _StockProps     =   64
      Columns         =   10
      FontName        =   "Courier New"
   End
End
Attribute VB_Name = "AuditForm"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private m_FlightUrno As String
Private m_IsOnTop As Boolean
Private m_DateTimeFormat As String
Private m_MainTab As TABLib.Tab
Private m_FormIcon As StdPicture

Public Sub SetStaticProperties(ByVal DateTimeFormat As String, MainTab As TABLib.Tab, Optional FormIcon As StdPicture = Nothing)
    m_DateTimeFormat = DateTimeFormat
    Set m_MainTab = MainTab
    Set m_FormIcon = FormIcon
End Sub

Public Sub SetDynamicProperties(ByVal FlightUrno As String, ByVal IsOnTop As Boolean)
    m_IsOnTop = IsOnTop
    SetFormOnTop Me, m_IsOnTop
    
    m_FlightUrno = FlightUrno
    Call RefreshTab
End Sub

Private Sub RefreshTab()
    Dim intDateTimeCols(1) As Integer, i As Integer

    tabAudit.ResetContent
    tabAudit.HeaderString = "Org,Modified On,Status,Status Timing,User"
    tabAudit.LogicalFieldList = "HSNM,CDAT,LSNM,CONA,PENO"
    tabAudit.HeaderAlignmentString = "L,L,L,L,L"
    tabAudit.HeaderLengthString = "50,130,350,130,130"
    
    intDateTimeCols(0) = 1
    intDateTimeCols(1) = 3
    For i = 0 To UBound(intDateTimeCols)
        tabAudit.DateTimeSetColumn intDateTimeCols(i)
        tabAudit.DateTimeSetInputFormatString intDateTimeCols(i), "YYYYMMDDhhmmss"
        tabAudit.DateTimeSetOutputFormatString intDateTimeCols(i), m_DateTimeFormat
    Next i
    
    tabAudit.CedaAction "RTA", "ITATAB", tabAudit.LogicalFieldList, "", _
        "WHERE FLNU = '" & m_FlightUrno & "' ORDER BY CDAT"
End Sub

Private Sub Form_Activate()
    If Not (m_FormIcon Is Nothing) Then
        Set Me.Icon = m_FormIcon
    End If
End Sub

Private Sub Form_Load()
    tabAudit.CedaServerName = UfisServer.aCeda.ServerName
    tabAudit.CedaPort = "3357"
    tabAudit.CedaHopo = UfisServer.HOPO
    tabAudit.CedaCurrentApplication = UfisServer.aCeda.Module
    tabAudit.CedaTabext = "TAB"
    tabAudit.CedaUser = UfisServer.aCeda.UserName
    tabAudit.CedaWorkstation = UfisServer.GetMyWorkStationName
    tabAudit.CedaSendTimeout = "3"
    tabAudit.CedaReceiveTimeout = "240"
    tabAudit.CedaRecordSeparator = Chr(10)
    tabAudit.CedaIdentifier = UfisServer.ModName
    
    'copy setting from maintab
    tabAudit.FontSize = m_MainTab.FontSize
    tabAudit.HeaderFontSize = m_MainTab.HeaderFontSize
    tabAudit.LineHeight = m_MainTab.LineHeight
    tabAudit.LeftTextOffset = m_MainTab.LeftTextOffset
    tabAudit.SetTabFontBold True
    tabAudit.LifeStyle = m_MainTab.LifeStyle
    tabAudit.CursorLifeStyle = m_MainTab.CursorLifeStyle
    tabAudit.SelectColumnBackColor = m_MainTab.SelectColumnBackColor
    tabAudit.SelectColumnTextColor = m_MainTab.SelectColumnTextColor
    tabAudit.ShowHorzScroller True
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    If UnloadMode = 0 Then
        Me.Hide
    End If
End Sub

Private Sub Form_Resize()
    On Error Resume Next
    
    tabAudit.Left = 0
    tabAudit.Top = tbrAudit.Height
    tabAudit.Width = Me.ScaleWidth
    tabAudit.Height = Me.ScaleHeight - tabAudit.Top
End Sub

Private Sub tbrAudit_ButtonClick(ByVal Button As MSComctlLib.Button)
    Select Case Button.Key
        Case "refresh"
            Call RefreshTab
        Case "close"
            Me.Hide
    End Select
End Sub
