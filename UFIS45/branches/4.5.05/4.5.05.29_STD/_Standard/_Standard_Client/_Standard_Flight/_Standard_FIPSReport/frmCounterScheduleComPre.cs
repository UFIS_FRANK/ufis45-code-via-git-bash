using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Text;
using Ufis.Utils;
using Ufis.Data;

namespace FIPS_Reports
{
	/// <summary>
	/// Summary description for frmCounterScheduleComPre.
	/// </summary>
	public class frmCounterScheduleComPre : System.Windows.Forms.Form
	{
		#region my_variables

		/// <summary>
		/// Single instance of database
		/// </summary>
		private IDatabase myDB;

		/// <summary>
		/// CCA table pointer
		/// </summary>
		private ITable myCCA;
		/// <summary>
		/// ALT table pointer
		/// </summary>
		private ITable myALT;
		/// <summary>
		/// Count of data displayed in the report
		/// </summary>
		private int iTotalCnt = 0;
		/// <summary>
		/// Selection condition for CCA table. The parameters required are FROM and TO. This checks takes care of
		/// the following conditions :
		/// 1. Ending inside
		/// 2. Starting inside
		/// 3. Within 
		/// 4. Covering range
		/// </summary>
		private string strCCAWhere = "where ((CKES >= '@@FROM' and CKES <= '@@TO') "+
									"or (CKBS >= '@@FROM' and CKBS <= '@@TO') "+
									"or (CKBS <= '@@FROM' and CKES >= '@@TO')) "+
									"and (CTYP='C' OR CTYP='P')";
		/// <summary>
		/// Selection condition for ALT table. Looks for the distinct URNO obtained from CCA.FLNU
		/// </summary>
		private string strALTWhere = "where URNO in (@@FLNU)";
		/// <summary>
		/// List headers
		/// </summary>
		private string strTabHeader = "Type, Name, Ter, Airline, Min, From, To, Remarks, Display";
		/// <summary>
		/// List logical headers
		/// </summary>
		private string strLogicalFields = "CTYP,CKIC,CKIT,AL,MIN,CKBS,CKES,REMA,DISP";
		/// <summary>
		/// List header lengths
		/// </summary>
		private string strTabHeaderLens = "100,50,50,50,50,50,50,100,100";
		#endregion

		private System.Windows.Forms.Panel panelBody;
		private System.Windows.Forms.Panel panelTab;
		private AxTABLib.AxTAB tabResult;
		private System.Windows.Forms.Label lblResults;
		private System.Windows.Forms.Panel panelTop;
		private System.Windows.Forms.Label lblProgress;
		private System.Windows.Forms.ProgressBar progressBar1;
		private System.Windows.Forms.Button btnLoadPrint;
		private System.Windows.Forms.Button btnPrintPreview;
		private System.Windows.Forms.Button btnCancel;
		private System.Windows.Forms.Button btnOK;
		private System.Windows.Forms.DateTimePicker dtTo;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.DateTimePicker dtFrom;
		private System.Windows.Forms.Label label1;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public frmCounterScheduleComPre()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(frmCounterScheduleComPre));
			this.panelBody = new System.Windows.Forms.Panel();
			this.panelTab = new System.Windows.Forms.Panel();
			this.tabResult = new AxTABLib.AxTAB();
			this.lblResults = new System.Windows.Forms.Label();
			this.panelTop = new System.Windows.Forms.Panel();
			this.lblProgress = new System.Windows.Forms.Label();
			this.progressBar1 = new System.Windows.Forms.ProgressBar();
			this.btnLoadPrint = new System.Windows.Forms.Button();
			this.btnPrintPreview = new System.Windows.Forms.Button();
			this.btnCancel = new System.Windows.Forms.Button();
			this.btnOK = new System.Windows.Forms.Button();
			this.dtTo = new System.Windows.Forms.DateTimePicker();
			this.label2 = new System.Windows.Forms.Label();
			this.dtFrom = new System.Windows.Forms.DateTimePicker();
			this.label1 = new System.Windows.Forms.Label();
			this.panelBody.SuspendLayout();
			this.panelTab.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.tabResult)).BeginInit();
			this.panelTop.SuspendLayout();
			this.SuspendLayout();
			// 
			// panelBody
			// 
			this.panelBody.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
			this.panelBody.Controls.Add(this.panelTab);
			this.panelBody.Controls.Add(this.lblResults);
			this.panelBody.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panelBody.Location = new System.Drawing.Point(0, 144);
			this.panelBody.Name = "panelBody";
			this.panelBody.Size = new System.Drawing.Size(744, 414);
			this.panelBody.TabIndex = 5;
			// 
			// panelTab
			// 
			this.panelTab.Controls.Add(this.tabResult);
			this.panelTab.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panelTab.Location = new System.Drawing.Point(0, 16);
			this.panelTab.Name = "panelTab";
			this.panelTab.Size = new System.Drawing.Size(744, 398);
			this.panelTab.TabIndex = 2;
			// 
			// tabResult
			// 
			this.tabResult.ContainingControl = this;
			this.tabResult.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tabResult.Location = new System.Drawing.Point(0, 0);
			this.tabResult.Name = "tabResult";
			this.tabResult.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("tabResult.OcxState")));
			this.tabResult.Size = new System.Drawing.Size(744, 398);
			this.tabResult.TabIndex = 0;
			// 
			// lblResults
			// 
			this.lblResults.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblResults.Dock = System.Windows.Forms.DockStyle.Top;
			this.lblResults.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.lblResults.Location = new System.Drawing.Point(0, 0);
			this.lblResults.Name = "lblResults";
			this.lblResults.Size = new System.Drawing.Size(744, 16);
			this.lblResults.TabIndex = 1;
			this.lblResults.Text = "Report Results";
			this.lblResults.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// panelTop
			// 
			this.panelTop.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
			this.panelTop.Controls.Add(this.lblProgress);
			this.panelTop.Controls.Add(this.progressBar1);
			this.panelTop.Controls.Add(this.btnLoadPrint);
			this.panelTop.Controls.Add(this.btnPrintPreview);
			this.panelTop.Controls.Add(this.btnCancel);
			this.panelTop.Controls.Add(this.btnOK);
			this.panelTop.Controls.Add(this.dtTo);
			this.panelTop.Controls.Add(this.label2);
			this.panelTop.Controls.Add(this.dtFrom);
			this.panelTop.Controls.Add(this.label1);
			this.panelTop.Dock = System.Windows.Forms.DockStyle.Top;
			this.panelTop.Location = new System.Drawing.Point(0, 0);
			this.panelTop.Name = "panelTop";
			this.panelTop.Size = new System.Drawing.Size(744, 144);
			this.panelTop.TabIndex = 4;
			// 
			// lblProgress
			// 
			this.lblProgress.Location = new System.Drawing.Point(412, 72);
			this.lblProgress.Name = "lblProgress";
			this.lblProgress.Size = new System.Drawing.Size(212, 16);
			this.lblProgress.TabIndex = 12;
			// 
			// progressBar1
			// 
			this.progressBar1.Location = new System.Drawing.Point(412, 88);
			this.progressBar1.Name = "progressBar1";
			this.progressBar1.Size = new System.Drawing.Size(216, 23);
			this.progressBar1.TabIndex = 13;
			this.progressBar1.Visible = false;
			// 
			// btnLoadPrint
			// 
			this.btnLoadPrint.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnLoadPrint.Location = new System.Drawing.Point(244, 88);
			this.btnLoadPrint.Name = "btnLoadPrint";
			this.btnLoadPrint.TabIndex = 10;
			this.btnLoadPrint.Text = "Loa&d + Print";
			this.btnLoadPrint.Click += new System.EventHandler(this.btnLoadPrint_Click);
			// 
			// btnPrintPreview
			// 
			this.btnPrintPreview.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnPrintPreview.Location = new System.Drawing.Point(168, 88);
			this.btnPrintPreview.Name = "btnPrintPreview";
			this.btnPrintPreview.TabIndex = 9;
			this.btnPrintPreview.Text = "&Print Preview";
			this.btnPrintPreview.Click += new System.EventHandler(this.btnPrintPreview_Click);
			// 
			// btnCancel
			// 
			this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnCancel.Location = new System.Drawing.Point(320, 88);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.TabIndex = 11;
			this.btnCancel.Text = "&Close";
			// 
			// btnOK
			// 
			this.btnOK.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnOK.Location = new System.Drawing.Point(92, 88);
			this.btnOK.Name = "btnOK";
			this.btnOK.TabIndex = 8;
			this.btnOK.Text = "&Load Data";
			this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
			// 
			// dtTo
			// 
			this.dtTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dtTo.Location = new System.Drawing.Point(268, 4);
			this.dtTo.Name = "dtTo";
			this.dtTo.Size = new System.Drawing.Size(128, 20);
			this.dtTo.TabIndex = 1;
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(232, 8);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(24, 16);
			this.label2.TabIndex = 2;
			this.label2.Text = "to:";
			// 
			// dtFrom
			// 
			this.dtFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dtFrom.Location = new System.Drawing.Point(92, 4);
			this.dtFrom.Name = "dtFrom";
			this.dtFrom.Size = new System.Drawing.Size(128, 20);
			this.dtFrom.TabIndex = 0;
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(12, 8);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(64, 16);
			this.label1.TabIndex = 0;
			this.label1.Text = "Date from:";
			// 
			// frmCounterScheduleComPre
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
			this.ClientSize = new System.Drawing.Size(744, 558);
			this.Controls.Add(this.panelBody);
			this.Controls.Add(this.panelTop);
			this.Name = "frmCounterScheduleComPre";
			this.Text = "Common/Pre Check In Counter Schedule";
			this.Load += new System.EventHandler(this.frmCounterScheduleComPre_load);
			this.panelBody.ResumeLayout(false);
			this.panelTab.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.tabResult)).EndInit();
			this.panelTop.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		private void frmCounterScheduleComPre_load(object sender, System.EventArgs e)
		{
			myDB = UT.GetMemDB();
			InitTimePickers();
			InitTab();
		}

		/// <summary>
		/// Initializes the From/to time control with the current day in
		/// the custom format "dd.MM.yyyy - HH:mm".
		/// </summary>
		private void InitTimePickers()
		{
			DateTime olFrom;
			DateTime olTo;
			
			olFrom = DateTime.Now;
			olFrom = new DateTime(olFrom.Year, olFrom.Month, olFrom.Day, 0,0,0);
			olTo   = new DateTime(olFrom.Year, olFrom.Month, olFrom.Day, 23,59,0);
			dtFrom.Value = olFrom;
			dtTo.Value = olTo;
			dtFrom.CustomFormat = "dd.MM.yyyy - HH:mm";
			dtTo.CustomFormat = "dd.MM.yyyy - HH:mm";
		}

		/// <summary>
		/// Intitialise the list section for the display of the result.
		/// </summary>
		private void InitTab()
		{
			tabResult.ResetContent();
			tabResult.ShowHorzScroller(true);
			tabResult.EnableHeaderSizing(true);
			tabResult.SetTabFontBold(true);
			tabResult.LifeStyle = true;
			tabResult.LineHeight = 16;
			tabResult.FontName = "Arial";
			tabResult.FontSize = 14;
			tabResult.HeaderFontSize = 14;
			tabResult.AutoSizeByHeader = true;
			tabResult.HeaderString = strTabHeader;
			tabResult.LogicalFieldList = strLogicalFields;
			tabResult.HeaderLengthString = strTabHeaderLens;
			PrepareReportData();
		}
		/// <summary>
		/// Formats the data received, to display in the list section. Sorts the 
		/// result wrt to 5th column (From).
		/// </summary>
		private void PrepareReportData() 
		{
			if (myCCA == null)
				return;

			StringBuilder strData = new StringBuilder(200);
			StringBuilder sb = new StringBuilder(100000);
			iTotalCnt = 0;
			IRow [] rows;
			TimeSpan diffTime;

			for (int i = 0; i<myCCA.Count ; i++ )
			{
				if (myCCA[i]["CTYP"].Equals("P"))
                    strData.Append("Pre Check-In").Append(",");
				else if (myCCA[i]["CTYP"].Equals("C"))
					strData.Append("Common").Append(",");
				else
					strData.Append(myCCA[i]["CTYP"]).Append(",");

				strData.Append(myCCA[i]["CKIC"]).Append(",");
				strData.Append(myCCA[i]["CKIT"]).Append(",");
				rows = myALT.RowsByIndexValue("URNO", myCCA[i]["FLNU"]);
				if (rows.Length > 0)
					strData.Append(rows[0]["ALC2"]).Append("/")
							.Append(rows[0]["ALC3"]).Append(",");
				else
					strData.Append("").Append(",");
				diffTime = UT.CedaFullDateToDateTime(myCCA[i]["CKES"])-UT.CedaFullDateToDateTime(myCCA[i]["CKBS"]);
				strData.Append(diffTime.TotalMinutes.ToString()).Append(",");
				strData.Append(Helper.DateString(myCCA[i]["CKBS"],"dd'/'HH:mm")).Append(",");
				strData.Append(Helper.DateString(myCCA[i]["CKES"],"dd'/'HH:mm")).Append(",");
				strData.Append(myCCA[i]["REMA"]).Append(",");
				strData.Append(myCCA[i]["DISP"]).Append(",");
				strData.Append("\n");
				sb.Append(strData.ToString());
				strData.Remove(0,strData.Length);
				iTotalCnt++;
			}
			tabResult.InsertBuffer(sb.ToString(),"\n");
			tabResult.Sort("5", true, true);
			tabResult.Refresh();
			lblResults.Text = "Report Results: (" + tabResult.GetLineCount().ToString() + ")";
		}

		/// <summary>
		/// Loads the data from the database. 
		/// 
		/// Retrieves the data for the CCA table corresponding to the specified range. 
		/// Gets all the unique FLNU's from this data and then loads the data from ALT 
		/// for those FLNU's.
		/// </summary>
		private void LoadReportData() 
		{
			tabResult.ResetContent();
			DateTime datFrom = dtFrom.Value;
			DateTime datTo = dtTo.Value;
			DateTime datReadTo;
			DateTime datReadFrom;
			string strDateFrom = "";
			string strDateTo = "";
			string strTmpQry = "";
			int ilTotal;
			int loopCnt = 1;
			int percent;
			TimeSpan tsDays;
			StringBuilder strFlnus = new StringBuilder(10000);

			if(UT.IsTimeInUtc == false)
			{
				datFrom = UT.LocalToUtc(dtFrom.Value);
				datTo   = UT.LocalToUtc(dtTo.Value);
				strDateFrom = UT.DateTimeToCeda( datFrom );
				strDateTo = UT.DateTimeToCeda( datTo );
			}
			else
			{
				strDateFrom = UT.DateTimeToCeda(dtFrom.Value);
				strDateTo = UT.DateTimeToCeda(dtTo.Value);
			}

			lblProgress.Text = "Loading Counter Data";
			progressBar1.Value = 0;
			lblProgress.Refresh();
			progressBar1.Show();

			myDB.Unbind("CCA");
			myCCA = myDB.Bind("CCA","CCA"
				,"CTYP,CKIC,CKIT,CKBS,CKES,REMA,DISP,URNO,FLNU"
				,"1,5,1,14,14,60,60"
				,"CTYP,CKIC,CKIT,CKBS,CKES,REMA,DISP,URNO,FLNU");
			myCCA.Clear();
			myCCA.TimeFields = "CKBS,CKES";
			myCCA.TimeFieldsInitiallyInUtc = true;
			tsDays = (datTo - datFrom);
			ilTotal = Convert.ToInt32(tsDays.TotalDays);
			if(ilTotal == 0) 
				ilTotal = 1;
			datReadFrom = datFrom;
			do
			{
				percent = Convert.ToInt32((loopCnt * 100)/ilTotal);
				if (percent > 100) percent = 100;
				progressBar1.Value = percent;
				datReadTo = datReadFrom + new TimeSpan(1, 0, 0, 0, 0);
				if( datReadTo > datTo) 
					datReadTo = datTo;
				strDateFrom = UT.DateTimeToCeda(datReadFrom);
				strDateTo = UT.DateTimeToCeda(datReadTo);
				strTmpQry = strCCAWhere;
				strTmpQry = strTmpQry.Replace("@@FROM",strDateFrom);
				strTmpQry = strTmpQry.Replace("@@TO",strDateTo);
				myCCA.Load(strTmpQry);
				datReadFrom += new TimeSpan(1, 0, 0, 0, 0);
				loopCnt++;
			} while(datReadFrom <= datReadTo);
			lblProgress.Text = "";
			progressBar1.Hide();

			lblProgress.Text = "Loading Airline Data";
			progressBar1.Value = 0;
			lblProgress.Refresh();
			progressBar1.Show();

			myDB.Unbind("ALT");
			myALT = myDB.Bind("ALT","ALT"
							, "URNO,ALC2,ALC3"
							, "10,2,3"
							, "URNO,ALC2,ALC3");
			myALT.Clear();
			ilTotal = (int)(myCCA.Count/300);
			if(ilTotal == 0) 
				ilTotal = 1;
			loopCnt = 0;

			for(int i = 0; i < myCCA.Count; i++)
			{
				if (strFlnus.ToString().IndexOf(myCCA[i]["FLNU"],0,strFlnus.Length) == -1 )
					strFlnus.Append(myCCA[i]["FLNU"]).Append(",");
				if((i % 300) == 0 && i > 0)
				{
					loopCnt++;
					percent = Convert.ToInt32((loopCnt * 100)/ilTotal);
					if (percent > 100) percent = 100;
					progressBar1.Value = percent;

					strFlnus = strFlnus.Remove(strFlnus.Length-1, 1);
					strTmpQry = strALTWhere;
					strTmpQry = strTmpQry.Replace("@@FLNU",strFlnus.ToString());
					myALT.Load(strTmpQry);
					strFlnus.Remove(0,strFlnus.Length);
				}
			}
			if(strFlnus.Length > 0)
			{
				strFlnus = strFlnus.Remove(strFlnus.Length-1, 1);
				strTmpQry = strALTWhere;
				strTmpQry = strTmpQry.Replace("@@FLNU",strFlnus.ToString());
				myALT.Load(strTmpQry);
				strFlnus.Remove(0,strFlnus.Length);
			}
			myALT.CreateIndex("URNO", "URNO");
			lblProgress.Text = "";
			progressBar1.Hide();
			myCCA.TimeFieldsCurrentlyInUtc = UT.IsTimeInUtc;
		}

		/// <summary>
		/// Generates the report in a pop up window.
		/// 
		/// Print layout - A4 Portrait
		/// </summary>
		private void RunReport() 
		{
			string strMainHeader = "Counter Schedule (Common/Pre Check-In)";
			StringBuilder strSubHeader = new StringBuilder();
			strSubHeader.Append("From: ")
				.Append(dtFrom.Value.ToString("dd.MM.yy'/'HH:mm"))
				.Append(" To: ")
				.Append(dtTo.Value.ToString("dd.MM.yy'/'HH:mm"));

			rptFIPS rpt = new rptFIPS(tabResult,strMainHeader,strSubHeader.ToString(),"",8);
			rpt.TextBoxCanGrow = false;
			frmPrintPreview frm = new frmPrintPreview(rpt);
			frm.Show();
		}

		/// <summary>
		/// Validates the user entry for the following fields:
		/// 1. FROM and TO fields
		/// 
		/// Displays the error message.
		/// </summary>
		/// <returns>TRUE - If entries are correct, else FALSE</returns>
		private bool validEntry() 
		{
			string strErr = "";
			if (dtFrom.Value > dtTo.Value)
				strErr = "Date From is later than Date To!\n";

			if (strErr.Equals(""))
				return true;
			else 
			{
				MessageBox.Show(this, strErr
					,"FIPS Reports" ,MessageBoxButtons.OK ,MessageBoxIcon.Error);
				return false;
			}
		}

		private void btnOK_Click(object sender, System.EventArgs e)
		{
			if (validEntry())
			{
				this.Cursor = Cursors.WaitCursor;
				LoadReportData();
				PrepareReportData();
				this.Cursor = Cursors.Arrow;
			}
		}

		private void btnPrintPreview_Click(object sender, System.EventArgs e)
		{
			RunReport();
		}

		private void btnLoadPrint_Click(object sender, System.EventArgs e)
		{
			if (validEntry())
			{
				this.Cursor = Cursors.WaitCursor;
				LoadReportData();
				PrepareReportData();
				RunReport();
				this.Cursor = Cursors.Arrow;
			}
		}
	}
}
