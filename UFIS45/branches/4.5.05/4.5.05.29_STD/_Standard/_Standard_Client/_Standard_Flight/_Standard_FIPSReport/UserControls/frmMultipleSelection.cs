using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace UserControls
{
	/// <summary>
	/// Summary description for frmMultipleSelection.
	/// </summary>
	public class frmMultipleSelection : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Button btnAddSingle;
		private System.Windows.Forms.Button btnRemoveAll;
		private System.Windows.Forms.Button btnRemoveSingle;
		private System.Windows.Forms.Button btnAddAll;
		private System.Windows.Forms.Button btnOK;
		private System.Windows.Forms.Button btnCancel;

		private System.Windows.Forms.CheckedListBox source = null;
		private System.Windows.Forms.ListBox lbSource;
		private System.Windows.Forms.ListBox lbTarget;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public frmMultipleSelection()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(frmMultipleSelection));
			this.lbSource = new System.Windows.Forms.ListBox();
			this.lbTarget = new System.Windows.Forms.ListBox();
			this.btnAddSingle = new System.Windows.Forms.Button();
			this.btnRemoveAll = new System.Windows.Forms.Button();
			this.btnRemoveSingle = new System.Windows.Forms.Button();
			this.btnAddAll = new System.Windows.Forms.Button();
			this.btnOK = new System.Windows.Forms.Button();
			this.btnCancel = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// lbSource
			// 
			this.lbSource.AccessibleDescription = resources.GetString("lbSource.AccessibleDescription");
			this.lbSource.AccessibleName = resources.GetString("lbSource.AccessibleName");
			this.lbSource.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("lbSource.Anchor")));
			this.lbSource.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("lbSource.BackgroundImage")));
			this.lbSource.ColumnWidth = ((int)(resources.GetObject("lbSource.ColumnWidth")));
			this.lbSource.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("lbSource.Dock")));
			this.lbSource.Enabled = ((bool)(resources.GetObject("lbSource.Enabled")));
			this.lbSource.Font = ((System.Drawing.Font)(resources.GetObject("lbSource.Font")));
			this.lbSource.HorizontalExtent = ((int)(resources.GetObject("lbSource.HorizontalExtent")));
			this.lbSource.HorizontalScrollbar = ((bool)(resources.GetObject("lbSource.HorizontalScrollbar")));
			this.lbSource.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("lbSource.ImeMode")));
			this.lbSource.IntegralHeight = ((bool)(resources.GetObject("lbSource.IntegralHeight")));
			this.lbSource.ItemHeight = ((int)(resources.GetObject("lbSource.ItemHeight")));
			this.lbSource.Location = ((System.Drawing.Point)(resources.GetObject("lbSource.Location")));
			this.lbSource.Name = "lbSource";
			this.lbSource.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("lbSource.RightToLeft")));
			this.lbSource.ScrollAlwaysVisible = ((bool)(resources.GetObject("lbSource.ScrollAlwaysVisible")));
			this.lbSource.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
			this.lbSource.Size = ((System.Drawing.Size)(resources.GetObject("lbSource.Size")));
			this.lbSource.Sorted = true;
			this.lbSource.TabIndex = ((int)(resources.GetObject("lbSource.TabIndex")));
			this.lbSource.Visible = ((bool)(resources.GetObject("lbSource.Visible")));
			// 
			// lbTarget
			// 
			this.lbTarget.AccessibleDescription = resources.GetString("lbTarget.AccessibleDescription");
			this.lbTarget.AccessibleName = resources.GetString("lbTarget.AccessibleName");
			this.lbTarget.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("lbTarget.Anchor")));
			this.lbTarget.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("lbTarget.BackgroundImage")));
			this.lbTarget.ColumnWidth = ((int)(resources.GetObject("lbTarget.ColumnWidth")));
			this.lbTarget.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("lbTarget.Dock")));
			this.lbTarget.Enabled = ((bool)(resources.GetObject("lbTarget.Enabled")));
			this.lbTarget.Font = ((System.Drawing.Font)(resources.GetObject("lbTarget.Font")));
			this.lbTarget.HorizontalExtent = ((int)(resources.GetObject("lbTarget.HorizontalExtent")));
			this.lbTarget.HorizontalScrollbar = ((bool)(resources.GetObject("lbTarget.HorizontalScrollbar")));
			this.lbTarget.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("lbTarget.ImeMode")));
			this.lbTarget.IntegralHeight = ((bool)(resources.GetObject("lbTarget.IntegralHeight")));
			this.lbTarget.ItemHeight = ((int)(resources.GetObject("lbTarget.ItemHeight")));
			this.lbTarget.Location = ((System.Drawing.Point)(resources.GetObject("lbTarget.Location")));
			this.lbTarget.Name = "lbTarget";
			this.lbTarget.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("lbTarget.RightToLeft")));
			this.lbTarget.ScrollAlwaysVisible = ((bool)(resources.GetObject("lbTarget.ScrollAlwaysVisible")));
			this.lbTarget.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
			this.lbTarget.Size = ((System.Drawing.Size)(resources.GetObject("lbTarget.Size")));
			this.lbTarget.Sorted = true;
			this.lbTarget.TabIndex = ((int)(resources.GetObject("lbTarget.TabIndex")));
			this.lbTarget.Visible = ((bool)(resources.GetObject("lbTarget.Visible")));
			// 
			// btnAddSingle
			// 
			this.btnAddSingle.AccessibleDescription = resources.GetString("btnAddSingle.AccessibleDescription");
			this.btnAddSingle.AccessibleName = resources.GetString("btnAddSingle.AccessibleName");
			this.btnAddSingle.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("btnAddSingle.Anchor")));
			this.btnAddSingle.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnAddSingle.BackgroundImage")));
			this.btnAddSingle.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("btnAddSingle.Dock")));
			this.btnAddSingle.Enabled = ((bool)(resources.GetObject("btnAddSingle.Enabled")));
			this.btnAddSingle.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("btnAddSingle.FlatStyle")));
			this.btnAddSingle.Font = ((System.Drawing.Font)(resources.GetObject("btnAddSingle.Font")));
			this.btnAddSingle.Image = ((System.Drawing.Image)(resources.GetObject("btnAddSingle.Image")));
			this.btnAddSingle.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnAddSingle.ImageAlign")));
			this.btnAddSingle.ImageIndex = ((int)(resources.GetObject("btnAddSingle.ImageIndex")));
			this.btnAddSingle.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("btnAddSingle.ImeMode")));
			this.btnAddSingle.Location = ((System.Drawing.Point)(resources.GetObject("btnAddSingle.Location")));
			this.btnAddSingle.Name = "btnAddSingle";
			this.btnAddSingle.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("btnAddSingle.RightToLeft")));
			this.btnAddSingle.Size = ((System.Drawing.Size)(resources.GetObject("btnAddSingle.Size")));
			this.btnAddSingle.TabIndex = ((int)(resources.GetObject("btnAddSingle.TabIndex")));
			this.btnAddSingle.Text = resources.GetString("btnAddSingle.Text");
			this.btnAddSingle.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnAddSingle.TextAlign")));
			this.btnAddSingle.Visible = ((bool)(resources.GetObject("btnAddSingle.Visible")));
			this.btnAddSingle.Click += new System.EventHandler(this.btnAddSingle_Click);
			// 
			// btnRemoveAll
			// 
			this.btnRemoveAll.AccessibleDescription = resources.GetString("btnRemoveAll.AccessibleDescription");
			this.btnRemoveAll.AccessibleName = resources.GetString("btnRemoveAll.AccessibleName");
			this.btnRemoveAll.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("btnRemoveAll.Anchor")));
			this.btnRemoveAll.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnRemoveAll.BackgroundImage")));
			this.btnRemoveAll.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("btnRemoveAll.Dock")));
			this.btnRemoveAll.Enabled = ((bool)(resources.GetObject("btnRemoveAll.Enabled")));
			this.btnRemoveAll.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("btnRemoveAll.FlatStyle")));
			this.btnRemoveAll.Font = ((System.Drawing.Font)(resources.GetObject("btnRemoveAll.Font")));
			this.btnRemoveAll.Image = ((System.Drawing.Image)(resources.GetObject("btnRemoveAll.Image")));
			this.btnRemoveAll.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnRemoveAll.ImageAlign")));
			this.btnRemoveAll.ImageIndex = ((int)(resources.GetObject("btnRemoveAll.ImageIndex")));
			this.btnRemoveAll.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("btnRemoveAll.ImeMode")));
			this.btnRemoveAll.Location = ((System.Drawing.Point)(resources.GetObject("btnRemoveAll.Location")));
			this.btnRemoveAll.Name = "btnRemoveAll";
			this.btnRemoveAll.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("btnRemoveAll.RightToLeft")));
			this.btnRemoveAll.Size = ((System.Drawing.Size)(resources.GetObject("btnRemoveAll.Size")));
			this.btnRemoveAll.TabIndex = ((int)(resources.GetObject("btnRemoveAll.TabIndex")));
			this.btnRemoveAll.Text = resources.GetString("btnRemoveAll.Text");
			this.btnRemoveAll.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnRemoveAll.TextAlign")));
			this.btnRemoveAll.Visible = ((bool)(resources.GetObject("btnRemoveAll.Visible")));
			this.btnRemoveAll.Click += new System.EventHandler(this.btnRemoveAll_Click);
			// 
			// btnRemoveSingle
			// 
			this.btnRemoveSingle.AccessibleDescription = resources.GetString("btnRemoveSingle.AccessibleDescription");
			this.btnRemoveSingle.AccessibleName = resources.GetString("btnRemoveSingle.AccessibleName");
			this.btnRemoveSingle.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("btnRemoveSingle.Anchor")));
			this.btnRemoveSingle.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnRemoveSingle.BackgroundImage")));
			this.btnRemoveSingle.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("btnRemoveSingle.Dock")));
			this.btnRemoveSingle.Enabled = ((bool)(resources.GetObject("btnRemoveSingle.Enabled")));
			this.btnRemoveSingle.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("btnRemoveSingle.FlatStyle")));
			this.btnRemoveSingle.Font = ((System.Drawing.Font)(resources.GetObject("btnRemoveSingle.Font")));
			this.btnRemoveSingle.Image = ((System.Drawing.Image)(resources.GetObject("btnRemoveSingle.Image")));
			this.btnRemoveSingle.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnRemoveSingle.ImageAlign")));
			this.btnRemoveSingle.ImageIndex = ((int)(resources.GetObject("btnRemoveSingle.ImageIndex")));
			this.btnRemoveSingle.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("btnRemoveSingle.ImeMode")));
			this.btnRemoveSingle.Location = ((System.Drawing.Point)(resources.GetObject("btnRemoveSingle.Location")));
			this.btnRemoveSingle.Name = "btnRemoveSingle";
			this.btnRemoveSingle.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("btnRemoveSingle.RightToLeft")));
			this.btnRemoveSingle.Size = ((System.Drawing.Size)(resources.GetObject("btnRemoveSingle.Size")));
			this.btnRemoveSingle.TabIndex = ((int)(resources.GetObject("btnRemoveSingle.TabIndex")));
			this.btnRemoveSingle.Text = resources.GetString("btnRemoveSingle.Text");
			this.btnRemoveSingle.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnRemoveSingle.TextAlign")));
			this.btnRemoveSingle.Visible = ((bool)(resources.GetObject("btnRemoveSingle.Visible")));
			this.btnRemoveSingle.Click += new System.EventHandler(this.btnRemoveSingle_Click);
			// 
			// btnAddAll
			// 
			this.btnAddAll.AccessibleDescription = resources.GetString("btnAddAll.AccessibleDescription");
			this.btnAddAll.AccessibleName = resources.GetString("btnAddAll.AccessibleName");
			this.btnAddAll.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("btnAddAll.Anchor")));
			this.btnAddAll.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnAddAll.BackgroundImage")));
			this.btnAddAll.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("btnAddAll.Dock")));
			this.btnAddAll.Enabled = ((bool)(resources.GetObject("btnAddAll.Enabled")));
			this.btnAddAll.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("btnAddAll.FlatStyle")));
			this.btnAddAll.Font = ((System.Drawing.Font)(resources.GetObject("btnAddAll.Font")));
			this.btnAddAll.Image = ((System.Drawing.Image)(resources.GetObject("btnAddAll.Image")));
			this.btnAddAll.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnAddAll.ImageAlign")));
			this.btnAddAll.ImageIndex = ((int)(resources.GetObject("btnAddAll.ImageIndex")));
			this.btnAddAll.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("btnAddAll.ImeMode")));
			this.btnAddAll.Location = ((System.Drawing.Point)(resources.GetObject("btnAddAll.Location")));
			this.btnAddAll.Name = "btnAddAll";
			this.btnAddAll.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("btnAddAll.RightToLeft")));
			this.btnAddAll.Size = ((System.Drawing.Size)(resources.GetObject("btnAddAll.Size")));
			this.btnAddAll.TabIndex = ((int)(resources.GetObject("btnAddAll.TabIndex")));
			this.btnAddAll.Text = resources.GetString("btnAddAll.Text");
			this.btnAddAll.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnAddAll.TextAlign")));
			this.btnAddAll.Visible = ((bool)(resources.GetObject("btnAddAll.Visible")));
			this.btnAddAll.Click += new System.EventHandler(this.btnAddAll_Click);
			// 
			// btnOK
			// 
			this.btnOK.AccessibleDescription = resources.GetString("btnOK.AccessibleDescription");
			this.btnOK.AccessibleName = resources.GetString("btnOK.AccessibleName");
			this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("btnOK.Anchor")));
			this.btnOK.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnOK.BackgroundImage")));
			this.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.btnOK.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("btnOK.Dock")));
			this.btnOK.Enabled = ((bool)(resources.GetObject("btnOK.Enabled")));
			this.btnOK.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("btnOK.FlatStyle")));
			this.btnOK.Font = ((System.Drawing.Font)(resources.GetObject("btnOK.Font")));
			this.btnOK.Image = ((System.Drawing.Image)(resources.GetObject("btnOK.Image")));
			this.btnOK.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnOK.ImageAlign")));
			this.btnOK.ImageIndex = ((int)(resources.GetObject("btnOK.ImageIndex")));
			this.btnOK.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("btnOK.ImeMode")));
			this.btnOK.Location = ((System.Drawing.Point)(resources.GetObject("btnOK.Location")));
			this.btnOK.Name = "btnOK";
			this.btnOK.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("btnOK.RightToLeft")));
			this.btnOK.Size = ((System.Drawing.Size)(resources.GetObject("btnOK.Size")));
			this.btnOK.TabIndex = ((int)(resources.GetObject("btnOK.TabIndex")));
			this.btnOK.Text = resources.GetString("btnOK.Text");
			this.btnOK.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnOK.TextAlign")));
			this.btnOK.Visible = ((bool)(resources.GetObject("btnOK.Visible")));
			this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
			// 
			// btnCancel
			// 
			this.btnCancel.AccessibleDescription = resources.GetString("btnCancel.AccessibleDescription");
			this.btnCancel.AccessibleName = resources.GetString("btnCancel.AccessibleName");
			this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("btnCancel.Anchor")));
			this.btnCancel.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnCancel.BackgroundImage")));
			this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnCancel.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("btnCancel.Dock")));
			this.btnCancel.Enabled = ((bool)(resources.GetObject("btnCancel.Enabled")));
			this.btnCancel.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("btnCancel.FlatStyle")));
			this.btnCancel.Font = ((System.Drawing.Font)(resources.GetObject("btnCancel.Font")));
			this.btnCancel.Image = ((System.Drawing.Image)(resources.GetObject("btnCancel.Image")));
			this.btnCancel.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnCancel.ImageAlign")));
			this.btnCancel.ImageIndex = ((int)(resources.GetObject("btnCancel.ImageIndex")));
			this.btnCancel.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("btnCancel.ImeMode")));
			this.btnCancel.Location = ((System.Drawing.Point)(resources.GetObject("btnCancel.Location")));
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("btnCancel.RightToLeft")));
			this.btnCancel.Size = ((System.Drawing.Size)(resources.GetObject("btnCancel.Size")));
			this.btnCancel.TabIndex = ((int)(resources.GetObject("btnCancel.TabIndex")));
			this.btnCancel.Text = resources.GetString("btnCancel.Text");
			this.btnCancel.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnCancel.TextAlign")));
			this.btnCancel.Visible = ((bool)(resources.GetObject("btnCancel.Visible")));
			// 
			// frmMultipleSelection
			// 
			this.AccessibleDescription = resources.GetString("$this.AccessibleDescription");
			this.AccessibleName = resources.GetString("$this.AccessibleName");
			this.AutoScaleBaseSize = ((System.Drawing.Size)(resources.GetObject("$this.AutoScaleBaseSize")));
			this.AutoScroll = ((bool)(resources.GetObject("$this.AutoScroll")));
			this.AutoScrollMargin = ((System.Drawing.Size)(resources.GetObject("$this.AutoScrollMargin")));
			this.AutoScrollMinSize = ((System.Drawing.Size)(resources.GetObject("$this.AutoScrollMinSize")));
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = ((System.Drawing.Size)(resources.GetObject("$this.ClientSize")));
			this.Controls.Add(this.btnCancel);
			this.Controls.Add(this.btnOK);
			this.Controls.Add(this.btnAddAll);
			this.Controls.Add(this.btnRemoveSingle);
			this.Controls.Add(this.btnRemoveAll);
			this.Controls.Add(this.btnAddSingle);
			this.Controls.Add(this.lbTarget);
			this.Controls.Add(this.lbSource);
			this.Enabled = ((bool)(resources.GetObject("$this.Enabled")));
			this.Font = ((System.Drawing.Font)(resources.GetObject("$this.Font")));
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("$this.ImeMode")));
			this.Location = ((System.Drawing.Point)(resources.GetObject("$this.Location")));
			this.MaximumSize = ((System.Drawing.Size)(resources.GetObject("$this.MaximumSize")));
			this.MinimumSize = ((System.Drawing.Size)(resources.GetObject("$this.MinimumSize")));
			this.Name = "frmMultipleSelection";
			this.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("$this.RightToLeft")));
			this.StartPosition = ((System.Windows.Forms.FormStartPosition)(resources.GetObject("$this.StartPosition")));
			this.Text = resources.GetString("$this.Text");
			this.Load += new System.EventHandler(this.frmMultipleSelection_Load);
			this.ResumeLayout(false);

		}
		#endregion

		public CheckedListBox	Source
		{
			get
			{
				return this.source;
			}
			set
			{
				this.source = value;
			}
		}

		private void frmMultipleSelection_Load(object sender, System.EventArgs e)
		{
			for (int i = 0; i < this.source.Items.Count; i++)
			{
				int ind = this.source.CheckedItems.IndexOf(this.source.Items[i]);
				if (ind >= 0)
				{
					this.lbTarget.Items.Add(this.source.Items[i]);
					this.source.SetItemChecked(ind,false);
				}
				else
				{
					this.lbSource.Items.Add(this.source.Items[i]);
				}
			}
		}

		private void btnOK_Click(object sender, System.EventArgs e)
		{
			for (int i = this.source.CheckedIndices.Count-1; i >= 0; i--)
			{
				this.source.SetItemChecked(this.source.CheckedIndices[i],false);
			}

			for (int i = 0; i < this.lbTarget.Items.Count; i++)
			{
				int ind = this.source.Items.IndexOf(this.lbTarget.Items[i]);
				this.source.SetItemChecked(ind,true);
			}
		}

		private void btnAddSingle_Click(object sender, System.EventArgs e)
		{
			for (int i = this.lbSource.SelectedIndices.Count-1; i >= 0;i--)
			{
				this.lbTarget.Items.Add(this.lbSource.Items[this.lbSource.SelectedIndices[i]]);
				this.lbSource.Items.RemoveAt(this.lbSource.SelectedIndices[i]);
			}
		}

		private void btnAddAll_Click(object sender, System.EventArgs e)
		{
			this.lbTarget.Items.AddRange(this.lbSource.Items);
			this.lbSource.Items.Clear();
		}

		private void btnRemoveSingle_Click(object sender, System.EventArgs e)
		{
			for (int i = this.lbTarget.SelectedIndices.Count-1; i >= 0;i--)
			{
				this.lbSource.Items.Add(this.lbTarget.Items[this.lbTarget.SelectedIndices[i]]);
				this.lbTarget.Items.RemoveAt(this.lbTarget.SelectedIndices[i]);
			}
		}

		private void btnRemoveAll_Click(object sender, System.EventArgs e)
		{
			this.lbSource.Items.AddRange(this.lbTarget.Items);
			this.lbTarget.Items.Clear();
		}
	}
}
