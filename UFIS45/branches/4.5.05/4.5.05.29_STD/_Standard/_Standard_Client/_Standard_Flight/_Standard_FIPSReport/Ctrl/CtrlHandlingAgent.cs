﻿using System;
using System.Collections.Generic;
using System.Text;

using Ufis.Utils;
using Ufis.Data;

namespace FIPS_Reports.Ctrl
{
    public class CtrlHandlingAgent
    {
        #region DB Related
        /// <summary>
        /// The one and only IDatabase obejct.
        /// </summary>
        private IDatabase _MyDB;
        /// <summary>
        /// ITable object for the HAI (Handling Agent)
        /// </summary>
        private ITable myHAI;

        private ITable myALT;//Airline Table
 
           private IDatabase MyDB
        {
            get
            {
                if (_MyDB == null)
                    _MyDB = UT.GetMemDB();

                return _MyDB;
            }
        }
        #endregion
    
        public List<string> GetAirLineCodesForHandlingAgent(string stHandlingAgent)
        {
            List<string> ls = new List<string>();
            foreach  (IRow haiRow in myHAI.RowsByIndexValue("HSNA", stHandlingAgent))
            {
                foreach (IRow altRow in myALT.RowsByIndexValue("URNO", haiRow["ALTU"]))
                {
                    if (!ls.Contains( altRow["ALC2"])) ls.Add( altRow["ALC2"]);
                }
            }
            return ls;
        }

        public string GetAirLineCodesForHandlingAgentInString(string stHandlingAgent, string separator, string quotationMark)
        {
            string st = "";
            List<string> ls = GetAirLineCodesForHandlingAgent(stHandlingAgent);
            if (ls.Count > 0)
            {
                //st = quotationMark + ls[0] + quotationMark;
                for (int i = 0; i < ls.Count; i++)
                {
                    st += separator + quotationMark + ls[i] + quotationMark;
                }
            }

            return st;
        }

        private static CtrlHandlingAgent _this = null;
        public static CtrlHandlingAgent GetInstance()
        {
            if (_this == null) _this = new CtrlHandlingAgent();
            return _this;
        }
    }
}
