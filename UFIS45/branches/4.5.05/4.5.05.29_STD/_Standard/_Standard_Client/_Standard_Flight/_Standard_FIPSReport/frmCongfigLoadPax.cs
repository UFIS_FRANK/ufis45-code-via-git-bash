using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using Ufis.Utils;
using Ufis.Data;
using System.Text;
using System.IO;
using System.Text.RegularExpressions;

namespace FIPS_Reports
{
	/// <summary>
	/// Summary description for frmCongfigLoadPax.
	/// </summary>
	public class frmCongfigLoadPax : System.Windows.Forms.Form
	{
		#region _My Members

		/// <summary>
		/// The where statements: all "@@xx" fields will be replaced
		/// with the true values according to the user's entry.
		/// </summary>
		private string strWhereRaw = "WHERE ((STOA BETWEEN '@@FROM' AND '@@TO' AND DES3='@@DEST' ) OR ( STOD BETWEEN '@@FROM' AND '@@TO' AND ORG3='@@ORIG')) AND (FTYP='O' OR FTYP='S')";
		/// <summary>
		/// The where statement for the dedicated chutes.
		/// </summary>
		private string strDedicatedCHAWhere = "WHERE RURN IN (@@RURN) AND LNAM<>' ' AND DES3 <> '@@@'";
		private string strCCAWhereRaw = "where FLNU in (@@FLNU)";
		private string strTabHeaderArrival = "Flight ,ORG ,STA ,ATA ,A/C ,REG ,NA ,ALC ,POS,Term ,Gate ,Belt";
		private string strLogicalFieldsArrival = "FLNO,ORG,STOA,ATA,AC_TYP,REG,NA,ALC,POS,TERM,GATE,BELT";//,PAX_F,PAX_C,PAX_Y,PAX_INF,P_TTL,ID,TR,TRF,BAG,CGO,MAIL,TTL";
		private string strTabHeaderLensArrival = "60,30,50,50,40,50,40,40,40,20,50,40"; //,40,40,40,40,40,40,40,40,40,40,40,40";
		private string strExcelTabHeaderLensArrival = "60,30,110,110,40,50,40,40,40,20,50,40";		
		private string strTabHeaderDeparture = "Flight ,DES ,STD ,ATD ,A/C ,REG ,NA ,ALC ,Cki ,Gate ,POS, Term,Chute From/To";// ,F ,C ,Y ,Inf ,Total ,ID ,TR ,TRF ,BAG ,CGO ,MAIL ,Total";
		private string strLogicalFieldsDeparture = "FLNO,DES,STOD,ATD,AC_TYP,REG,NA,ALC,CKIN,GATE,POS,TERM,CHUTE_FROM_TO";//,PAX_F,PAX_C,PAX_Y,PAX_INF,P_TTL,ID,TR,TRF,BAG,CGO,MAIL,TTL";
		private String strTabHeaderLensDeparture = "60,30,50,50,40,40,40,40,135,50,40,20,100";
		private string strExcelTabHeaderLensDeparture = "60,30,110,110,40,40,40,40,135,50,40,20,120";		
		private string strAPXWhereRaw = "where FLNU in (@@FLNU)";
		private string strLOAWhereRaw = "where FLNU in (@@FLNU) AND HOPO= '@@HOPO' AND APC3 = ' ' AND FLNO = ' '";
		private string strTerminalWhereRaw = " AND (STEV = '@@STEV')";
			//"AND DSSN IN ('USR','LDM','MVT','KRI') "+
			//"AND TYPE IN ('PAX','PAD','LOA') "+
			
		private IDatabase myDB;
		private ITable myAFT;
		private ITable myCCA;
		private ITable myLOA;
		private ITable myAPX;
		private ITable myCHA;
		private int iTotalFlights = 0;
		ArrayList objConfig = new ArrayList();

        /// <summary>
        /// Additional column for the STA/STD date
        /// </summary>
        private bool bmAdditionalDateColumn = false;
        /// <summary>
        /// Datetime format for excel export
        /// </summary>
        private string strExcelDateTimeFormat = "";
        /// <summary>
		/// Datetime format for report
		/// </summary>
		private string strReportDateTimeFormat = "dd'/'HH:mm";
		/// <summary>
		/// Datetime format for table
		/// </summary>
		private string strDateDisplayFormat = "";
		/// <summary>
		/// Boolean for mouse status with respect to tabResult control
		/// </summary>
		private bool bmMouseInsideTabControl = false;
		/// <summary>
		/// Report Header
		/// </summary>
		private string strReportHeader = "";
		/// <summary>
		/// Report Sub Header
		/// </summary>
		private string strReportSubHeader = "";
		/// <summary>
		/// Report Object
		/// </summary>
		private DataDynamics.ActiveReports.ActiveReport3 activeReport = null;
		/// <summary>
		/// Boolean to suggest if "Chutes from/to" column should be visible or not
		/// </summary>
		private bool bmChuteDisplay = false;
		/// <summary>
		/// Boolean for showing stev
		/// </summary>
		private bool bmShowStev = false;

		//public string[] strConfigReturn = new string[10000];
		#endregion _My Members
		
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.RadioButton radioButtonDeparture;
		private System.Windows.Forms.RadioButton radioButtonArrival;
		private System.Windows.Forms.DateTimePicker dtFrom;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label lblProgress;
		private System.Windows.Forms.ProgressBar progressBar1;
		private System.Windows.Forms.Button btnCancel;
		private System.Windows.Forms.Button btnLoadPrint;
		private System.Windows.Forms.Button buttonPrintPView;
		private System.Windows.Forms.Button btnLoad;
		private System.Windows.Forms.Panel panelBody;
		private System.Windows.Forms.Panel panelTab;
		private AxTABLib.AxTAB tabResult;
		private AxTABLib.AxTAB tabExcelResult;
		private System.Windows.Forms.Label lblResults;
		private System.Windows.Forms.Button buttonHelp;
		private System.Windows.Forms.Label lblTerminal;
		private System.Windows.Forms.TextBox txtTerminal;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.DateTimePicker dtTo;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public frmCongfigLoadPax()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeMouseEvents();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(frmCongfigLoadPax));
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.label3 = new System.Windows.Forms.Label();
			this.radioButtonDeparture = new System.Windows.Forms.RadioButton();
			this.radioButtonArrival = new System.Windows.Forms.RadioButton();
			this.dtFrom = new System.Windows.Forms.DateTimePicker();
			this.label1 = new System.Windows.Forms.Label();
			this.lblProgress = new System.Windows.Forms.Label();
			this.progressBar1 = new System.Windows.Forms.ProgressBar();
			this.btnCancel = new System.Windows.Forms.Button();
			this.btnLoadPrint = new System.Windows.Forms.Button();
			this.buttonPrintPView = new System.Windows.Forms.Button();
			this.btnLoad = new System.Windows.Forms.Button();
			this.panelBody = new System.Windows.Forms.Panel();
			this.panelTab = new System.Windows.Forms.Panel();
			this.tabResult = new AxTABLib.AxTAB();
			this.tabExcelResult = new AxTABLib.AxTAB();
			this.lblResults = new System.Windows.Forms.Label();
			this.buttonHelp = new System.Windows.Forms.Button();
			this.lblTerminal = new System.Windows.Forms.Label();
			this.txtTerminal = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.dtTo = new System.Windows.Forms.DateTimePicker();
			this.groupBox1.SuspendLayout();
			this.panelBody.SuspendLayout();
			this.panelTab.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.tabResult)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.tabExcelResult)).BeginInit();
			this.SuspendLayout();
			// 
			// groupBox1
			// 
			this.groupBox1.BackColor = System.Drawing.Color.Transparent;
			this.groupBox1.Controls.Add(this.label3);
			this.groupBox1.Controls.Add(this.radioButtonDeparture);
			this.groupBox1.Controls.Add(this.radioButtonArrival);
			this.groupBox1.Location = new System.Drawing.Point(16, 48);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(392, 64);
			this.groupBox1.TabIndex = 14;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Flights...";
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(64, 16);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(216, 16);
			this.label3.TabIndex = 0;
			this.label3.Text = "Which Flights should be shown";
			// 
			// radioButtonDeparture
			// 
			this.radioButtonDeparture.Location = new System.Drawing.Point(232, 32);
			this.radioButtonDeparture.Name = "radioButtonDeparture";
			this.radioButtonDeparture.TabIndex = 1;
			this.radioButtonDeparture.Text = "Departure";
			this.radioButtonDeparture.CheckedChanged += new System.EventHandler(this.radioButtonDeparture_CheckedChanged);
			// 
			// radioButtonArrival
			// 
			this.radioButtonArrival.Location = new System.Drawing.Point(64, 32);
			this.radioButtonArrival.Name = "radioButtonArrival";
			this.radioButtonArrival.Size = new System.Drawing.Size(92, 24);
			this.radioButtonArrival.TabIndex = 0;
			this.radioButtonArrival.Text = "Arrival";
			this.radioButtonArrival.CheckedChanged += new System.EventHandler(this.radioButtonArrival_CheckedChanged);
			// 
			// dtFrom
			// 
			this.dtFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dtFrom.Location = new System.Drawing.Point(96, 16);
			this.dtFrom.Name = "dtFrom";
			this.dtFrom.Size = new System.Drawing.Size(128, 20);
			this.dtFrom.TabIndex = 1;
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(24, 18);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(64, 16);
			this.label1.TabIndex = 0;
			this.label1.Text = "Date From :";
			// 
			// lblProgress
			// 
			this.lblProgress.Location = new System.Drawing.Point(442, 128);
			this.lblProgress.Name = "lblProgress";
			this.lblProgress.Size = new System.Drawing.Size(212, 16);
			this.lblProgress.TabIndex = 25;
			// 
			// progressBar1
			// 
			this.progressBar1.Location = new System.Drawing.Point(440, 152);
			this.progressBar1.Name = "progressBar1";
			this.progressBar1.Size = new System.Drawing.Size(216, 23);
			this.progressBar1.TabIndex = 24;
			this.progressBar1.Visible = false;
			// 
			// btnCancel
			// 
			this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnCancel.Location = new System.Drawing.Point(251, 152);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.TabIndex = 6;
			this.btnCancel.Text = "&Close";
			// 
			// btnLoadPrint
			// 
			this.btnLoadPrint.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnLoadPrint.Location = new System.Drawing.Point(174, 152);
			this.btnLoadPrint.Name = "btnLoadPrint";
			this.btnLoadPrint.TabIndex = 5;
			this.btnLoadPrint.Text = "Loa&d + Print";
			this.btnLoadPrint.Click += new System.EventHandler(this.btnLoadPrint_Click);
			// 
			// buttonPrintPView
			// 
			this.buttonPrintPView.BackColor = System.Drawing.SystemColors.Control;
			this.buttonPrintPView.Location = new System.Drawing.Point(92, 152);
			this.buttonPrintPView.Name = "buttonPrintPView";
			this.buttonPrintPView.Size = new System.Drawing.Size(80, 23);
			this.buttonPrintPView.TabIndex = 4;
			this.buttonPrintPView.Text = "&Print Preview";
			this.buttonPrintPView.Click += new System.EventHandler(this.buttonPrintPView_Click);
			// 
			// btnLoad
			// 
			this.btnLoad.BackColor = System.Drawing.SystemColors.Control;
			this.btnLoad.Location = new System.Drawing.Point(16, 152);
			this.btnLoad.Name = "btnLoad";
			this.btnLoad.TabIndex = 3;
			this.btnLoad.Text = "Load";
			this.btnLoad.Click += new System.EventHandler(this.btnLoad_Click);
			// 
			// panelBody
			// 
			this.panelBody.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.panelBody.Controls.Add(this.panelTab);
			this.panelBody.Controls.Add(this.lblResults);
			this.panelBody.Location = new System.Drawing.Point(0, 184);
			this.panelBody.Name = "panelBody";
			this.panelBody.Size = new System.Drawing.Size(752, 360);
			this.panelBody.TabIndex = 26;
			// 
			// panelTab
			// 
			this.panelTab.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.panelTab.Controls.Add(this.tabResult);
			this.panelTab.Controls.Add(this.tabExcelResult);
			this.panelTab.Location = new System.Drawing.Point(0, 16);
			this.panelTab.Name = "panelTab";
			this.panelTab.Size = new System.Drawing.Size(752, 328);
			this.panelTab.TabIndex = 3;
			// 
			// tabResult
			// 
			this.tabResult.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.tabResult.ContainingControl = this;
			this.tabResult.Location = new System.Drawing.Point(0, 0);
			this.tabResult.Name = "tabResult";
			this.tabResult.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("tabResult.OcxState")));
			this.tabResult.Size = new System.Drawing.Size(752, 328);
			this.tabResult.TabIndex = 0;
			this.tabResult.SendLButtonDblClick += new AxTABLib._DTABEvents_SendLButtonDblClickEventHandler(this.tabResult_SendLButtonDblClick);
			// 
			// tabExcelResult
			// 
			this.tabExcelResult.ContainingControl = this;
			this.tabExcelResult.Location = new System.Drawing.Point(0, 0);
			this.tabExcelResult.Name = "tabExcelResult";
			this.tabExcelResult.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("tabExcelResult.OcxState")));
			this.tabExcelResult.Size = new System.Drawing.Size(100, 50);
			this.tabExcelResult.TabIndex = 1;
			// 
			// lblResults
			// 
			this.lblResults.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblResults.Dock = System.Windows.Forms.DockStyle.Top;
			this.lblResults.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.lblResults.Location = new System.Drawing.Point(0, 0);
			this.lblResults.Name = "lblResults";
			this.lblResults.Size = new System.Drawing.Size(752, 16);
			this.lblResults.TabIndex = 2;
			this.lblResults.Text = "Report Results";
			this.lblResults.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// buttonHelp
			// 
			this.buttonHelp.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.buttonHelp.Location = new System.Drawing.Point(328, 152);
			this.buttonHelp.Name = "buttonHelp";
			this.buttonHelp.TabIndex = 7;
			this.buttonHelp.Text = "Help";
			this.buttonHelp.Click += new System.EventHandler(this.buttonHelp_Click);
			// 
			// lblTerminal
			// 
			this.lblTerminal.Location = new System.Drawing.Point(24, 122);
			this.lblTerminal.Name = "lblTerminal";
			this.lblTerminal.Size = new System.Drawing.Size(56, 14);
			this.lblTerminal.TabIndex = 31;
			this.lblTerminal.Text = "Terminal:";
			// 
			// txtTerminal
			// 
			this.txtTerminal.Location = new System.Drawing.Point(96, 120);
			this.txtTerminal.Name = "txtTerminal";
			this.txtTerminal.Size = new System.Drawing.Size(88, 20);
			this.txtTerminal.TabIndex = 2;
			this.txtTerminal.Text = "";
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(240, 19);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(24, 16);
			this.label2.TabIndex = 32;
			this.label2.Text = "to:";
			// 
			// dtTo
			// 
			this.dtTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dtTo.Location = new System.Drawing.Point(280, 16);
			this.dtTo.Name = "dtTo";
			this.dtTo.Size = new System.Drawing.Size(128, 20);
			this.dtTo.TabIndex = 33;
			// 
			// frmCongfigLoadPax
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
			this.ClientSize = new System.Drawing.Size(752, 526);
			this.Controls.Add(this.dtTo);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.txtTerminal);
			this.Controls.Add(this.lblTerminal);
			this.Controls.Add(this.buttonHelp);
			this.Controls.Add(this.panelBody);
			this.Controls.Add(this.lblProgress);
			this.Controls.Add(this.progressBar1);
			this.Controls.Add(this.btnCancel);
			this.Controls.Add(this.btnLoadPrint);
			this.Controls.Add(this.buttonPrintPView);
			this.Controls.Add(this.btnLoad);
			this.Controls.Add(this.groupBox1);
			this.Controls.Add(this.dtFrom);
			this.Controls.Add(this.label1);
			this.Name = "frmCongfigLoadPax";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Load and Pax";
			this.Load += new System.EventHandler(this.frmCongfigLoadPax_Load);
			this.HelpRequested += new System.Windows.Forms.HelpEventHandler(this.frmCongfigLoadPax_HelpRequested);
			this.groupBox1.ResumeLayout(false);
			this.panelBody.ResumeLayout(false);
			this.panelTab.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.tabResult)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.tabExcelResult)).EndInit();
			this.ResumeLayout(false);

            IniFile myIni = new IniFile("C:\\Ufis\\System\\Ceda.ini");

            string strTmp;
            strTmp = myIni.IniReadValue("FIPSREPORT", "PAXLOAD_DATECOLUMN");

            if (strTmp == "TRUE")
                bmAdditionalDateColumn = true;

            if (bmAdditionalDateColumn)
            {
                strTabHeaderArrival = "Flight ,ORG , DATE ,STA, ATA ,A/C ,REG ,NA ,ALC ,POS,Term ,Gate ,Belt";
                strLogicalFieldsArrival = "FLNO,ORG,DATE,STA,ATA,AC_TYP,REG,NA,ALC,POS,TERM,GATE,BELT";//,PAX_F,PAX_C,PAX_Y,PAX_INF,P_TTL,ID,TR,TRF,BAG,CGO,MAIL,TTL";
                strTabHeaderLensArrival = "60,30,70,40,40,40,50,40,40,40,20,50,40"; //,40,40,40,40,40,40,40,40,40,40,40,40";
                strExcelTabHeaderLensArrival = "60,30,80,50,50,40,50,40,40,40,20,50,40";
                strTabHeaderDeparture = "Flight ,DES , DATE, STD, ATD ,A/C ,REG ,NA ,ALC ,Cki ,Gate ,POS, Term,Chute From/To";// ,F ,C ,Y ,Inf ,Total ,ID ,TR ,TRF ,BAG ,CGO ,MAIL ,Total";
                strLogicalFieldsDeparture = "FLNO,DES,DATE,STD,ATD,AC_TYP,REG,NA,ALC,CKIN,GATE,POS,TERM,CHUTE_FROM_TO";//,PAX_F,PAX_C,PAX_Y,PAX_INF,P_TTL,ID,TR,TRF,BAG,CGO,MAIL,TTL";
                strTabHeaderLensDeparture = "60,30,70,40,40,40,40,40,40,135,50,40,20,100";
                strExcelTabHeaderLensDeparture = "60,30,80,50,50,40,40,40,40,135,50,40,20,120";
            }



		}
		#endregion

		private void btnLoad_Click(object sender, System.EventArgs e)
		{
			this.Cursor = Cursors.WaitCursor;
			LoadReportData();
			if(this.radioButtonArrival.Checked)
			{
				PrepareReportDataArrival("Report");
				PrepareReportDataArrival("Excel");
			}
			else
			{
				PrepareReportDataDeparture("Report");
				PrepareReportDataDeparture("Excel");
			}
			this.Cursor = Cursors.Arrow;
			lblResults.Text = "Report Results: (" + tabResult.GetLineCount().ToString() + ")";
		}

		private void frmCongfigLoadPax_Load(object sender, System.EventArgs e)
		{
			myDB = UT.GetMemDB();
			this.radioButtonArrival.Checked = true;
			InitTimePickers();
            ReadConfigFile();
            SetupReportTabHeader();
			
			FormHeaderString();
			InitTab();
		}

		private void InitTimePickers()
		{
			DateTime olFrom;			
			DateTime olTo;
			olFrom = DateTime.Now - new TimeSpan(1, 0, 0, 0, 0);
			olFrom = new DateTime(olFrom.Year, olFrom.Month, olFrom.Day, 0,0,0);
			olTo   = new DateTime(olFrom.Year, olFrom.Month, olFrom.Day, 23,59,0);
			dtFrom.Value = olFrom;
			dtTo.Value = olTo;
			dtFrom.CustomFormat = "dd.MM.yyyy - HH:mm";
			dtTo.CustomFormat = "dd.MM.yyyy - HH:mm";
		}

		/// <summary>
		///  This function initializes the table to be displayed 
		/// </summary>
		private void InitTab()
		{		
			tabExcelResult.ResetContent();
			tabResult.ResetContent();
			tabResult.ShowHorzScroller(true);
			tabResult.EnableHeaderSizing(true);
			tabResult.SetTabFontBold(true);
			tabResult.LifeStyle = true;
			tabResult.LineHeight = 16;
			tabResult.FontName = "Arial";
			tabResult.FontSize = 14;
			tabResult.HeaderFontSize = 14;
			tabResult.AutoSizeByHeader = true;
			
			IniFile myIni = new IniFile("C:\\Ufis\\System\\Ceda.ini");
			strDateDisplayFormat = myIni.IniReadValue("FIPSREPORT", "DATEDISPLAYFORMAT");
			strExcelDateTimeFormat = myIni.IniReadValue("FIPSREPORT", "DATEEXCELFORMAT");


			if(strDateDisplayFormat.Length == 0)
			{
				strDateDisplayFormat = strReportDateTimeFormat;
			}
			else
			{
				strReportDateTimeFormat = strDateDisplayFormat;
			}
			if(strExcelDateTimeFormat.Length == 0)
			{
				strExcelDateTimeFormat = strReportDateTimeFormat;
			}

			if(this.radioButtonArrival.Checked)
			{
				tabResult.HeaderString = strTabHeaderArrival;
				tabResult.LogicalFieldList = strLogicalFieldsArrival;
				tabResult.HeaderLengthString = strTabHeaderLensArrival;
				PrepareReportDataArrival("Report");
				
				tabExcelResult.HeaderString = strTabHeaderArrival;
				tabExcelResult.LogicalFieldList = strLogicalFieldsArrival;
				tabExcelResult.HeaderLengthString = strTabHeaderLensArrival;
				PrepareReportDataArrival("Excel");
			}
			else
			{
				tabResult.HeaderString = strTabHeaderDeparture;
				tabResult.LogicalFieldList = strLogicalFieldsDeparture;
				tabResult.HeaderLengthString = strTabHeaderLensDeparture;
				PrepareReportDataDeparture("Report");

				tabExcelResult.HeaderString = strTabHeaderDeparture;
				tabExcelResult.LogicalFieldList = strLogicalFieldsDeparture;
				tabExcelResult.HeaderLengthString = strTabHeaderLensDeparture;
				PrepareReportDataDeparture("Excel");
			}
		}

		/// <summary>
		/// Initializes the tab control header data.
		/// </summary>
		private void SetupReportTabHeader()
		{
			IniFile myIni = new IniFile("C:\\Ufis\\System\\Ceda.ini");
			if(myIni.IniReadValue("FIPS","CHUTE_DISPLAY").CompareTo("TRUE") == 0)
			{
				bmChuteDisplay = true;
			}
            

			string[] strLabelHeaderEntry = new string[2];
			string strTerminalLabel = "";
			string strTerminalHeader = "";
			if(Helper.IsSTEVConfigured(this,ref strLabelHeaderEntry,true,true) == true)
			{
				strTerminalLabel  = strLabelHeaderEntry[0];
				strTerminalHeader = strLabelHeaderEntry[1];
				bmShowStev = true;
				lblTerminal.Text = strTerminalLabel + ":";
                if (bmAdditionalDateColumn)
                {
                    Helper.RemoveOrReplaceString(10, ref strTabHeaderArrival, ',', strTerminalHeader, false);
                    Helper.RemoveOrReplaceString(12, ref strTabHeaderDeparture, ',', strTerminalHeader, false);
                }
                else
                {
                    Helper.RemoveOrReplaceString(9, ref strTabHeaderArrival, ',', strTerminalHeader, false);
                    Helper.RemoveOrReplaceString(11, ref strTabHeaderDeparture, ',', strTerminalHeader, false);
                }
			}
			else
			{
				lblTerminal.Hide();
				txtTerminal.Hide();
			}
			if(bmChuteDisplay == false)
			{
                if (bmAdditionalDateColumn)
                {
                    Helper.RemoveOrReplaceString(13, ref strTabHeaderDeparture, ',', "", true);
                    Helper.RemoveOrReplaceString(13, ref strLogicalFieldsDeparture, ',', "", true);
                    Helper.RemoveOrReplaceString(13, ref strTabHeaderLensDeparture, ',', "", true);
                    Helper.RemoveOrReplaceString(13, ref strExcelTabHeaderLensDeparture, ',', "", true);
                }
                else
                {
                    Helper.RemoveOrReplaceString(12, ref strTabHeaderDeparture, ',', "", true);
                    Helper.RemoveOrReplaceString(12, ref strLogicalFieldsDeparture, ',', "", true);
                    Helper.RemoveOrReplaceString(12, ref strTabHeaderLensDeparture, ',', "", true);
                    Helper.RemoveOrReplaceString(12, ref strExcelTabHeaderLensDeparture, ',', "", true);
                }
			}
			if(bmShowStev == false)
			{
                if (bmAdditionalDateColumn)
                {
                    Helper.RemoveOrReplaceString(10, ref strLogicalFieldsArrival, ',', "", true);
                    Helper.RemoveOrReplaceString(10, ref strTabHeaderArrival, ',', "", true);
                    Helper.RemoveOrReplaceString(10, ref strTabHeaderLensArrival, ',', "", true);
                    Helper.RemoveOrReplaceString(10, ref strExcelTabHeaderLensArrival, ',', "", true);

                    Helper.RemoveOrReplaceString(12, ref strLogicalFieldsDeparture, ',', "", true);
                    Helper.RemoveOrReplaceString(12, ref strTabHeaderDeparture, ',', "", true);
                    Helper.RemoveOrReplaceString(12, ref strTabHeaderLensDeparture, ',', "", true);
                    Helper.RemoveOrReplaceString(12, ref strExcelTabHeaderLensDeparture, ',', "", true);
                }
                else
                {
                    Helper.RemoveOrReplaceString(9, ref strLogicalFieldsArrival, ',', "", true);
                    Helper.RemoveOrReplaceString(9, ref strTabHeaderArrival, ',', "", true);
                    Helper.RemoveOrReplaceString(9, ref strTabHeaderLensArrival, ',', "", true);
                    Helper.RemoveOrReplaceString(9, ref strExcelTabHeaderLensArrival, ',', "", true);

                    Helper.RemoveOrReplaceString(11, ref strLogicalFieldsDeparture, ',', "", true);
                    Helper.RemoveOrReplaceString(11, ref strTabHeaderDeparture, ',', "", true);
                    Helper.RemoveOrReplaceString(11, ref strTabHeaderLensDeparture, ',', "", true);
                    Helper.RemoveOrReplaceString(11, ref strExcelTabHeaderLensDeparture, ',', "", true);
                }
			}
		}
		/// <summary>
		/// LoadReportData reads the respective data and stores it in the respective members.
		/// </summary>
		private void LoadReportData()
		{
			string strTmpWhere;
			strTmpWhere = strWhereRaw;
			
			tabResult.ResetContent();
			tabExcelResult.ResetContent();
			DateTime datFrom;
			DateTime datTo;

			string strDateFrom = "";
			string strDateTo = "";
			datFrom = dtFrom.Value;
			datTo   = dtTo.Value;
			
			if(UT.IsTimeInUtc == false)
			{
				datFrom = UT.LocalToUtc(dtFrom.Value);
				datTo =  UT.LocalToUtc(datTo);
				strDateFrom = UT.DateTimeToCeda( datFrom );
				strDateTo = UT.DateTimeToCeda( datTo );
			}
			else
			{
				strDateFrom = UT.DateTimeToCeda(dtFrom.Value);
				strDateTo = UT.DateTimeToCeda(datTo);
			}
			myDB.Unbind("AFT");
			myAFT = myDB.Bind("AFT", "AFT", "URNO,FLNO,ORG3,STOA,LAND,DES3,STOD,AIRB,ACT3,REGN,PSTD,PSTA,GTA1,GTA2,GTD1,GTD2,BLT1,BLT2,ADID,PAX1,PAX2,PAX3,PAXF,PAXI,PAXT,BAGN,BAGW,MAIL,CGOT,TTYP,ALC3,STEV,CKIF,CKIT"
				, "10,9,3,14,14,3,14,14,3,12,5,5,5,5,5,5,5,5,1,3,3,3,3,3,3,6,7,6,6,5,3,1,5,5"
				, "URNO,FLNO,ORG3,STOA,LAND,DES3,STOD,AIRB,ACT3,REGN,PSTD,PSTA,GTA1,GTA2,GTD1,GTD2,BLT1,BLT2,ADID,PAX1,PAX2,PAX3,PAXF,PAXI,PAXT,BAGN,BAGW,MAIL,CGOT,TTYP,ALC3,STEV,CKIF,CKIT");

			myAFT.Clear();
			myAFT.TimeFields = "STOA,STOD,TIFA,TIFD,ONBL,OFBL,LAND,AIRB";
			myAFT.Command("read",",GFR,");
			ArrayList myUniques = new ArrayList();
			myUniques.Add("URNO");
			myAFT.UniqueFields = myUniques;
			myAFT.TimeFieldsInitiallyInUtc = true;

            myAFT.TimeFieldsCurrentlyInUtc = UT.IsTimeInUtc;

			string strTerminal = "";
			if(bmShowStev == true)
			{
				strTerminal = txtTerminal.Text.Trim();
			}
			//Prepare loop reading day by day
			DateTime datReadFrom = datFrom;
			DateTime datReadTo ;
			TimeSpan tsDays = (datTo - datFrom);

			int ilTotal = Convert.ToInt32(tsDays.TotalDays);
			if(ilTotal == 0) ilTotal = 1;
			int loopCnt = 1;
			progressBar1.Value = 0;
			lblProgress.Text = "Loading Flight Data";
			lblProgress.Refresh();
			progressBar1.Show();
			do
			{
				int percent = Convert.ToInt32((loopCnt * 100)/ilTotal);
				if (percent > 100) 
					percent = 100;
				progressBar1.Value = percent;
				datReadTo = datReadFrom + new TimeSpan(1, 0, 0, 0, 0);
				if( datReadTo > datTo) datReadTo = datTo;

				strDateFrom = UT.DateTimeToCeda(datReadFrom);
				strDateTo = UT.DateTimeToCeda(datReadTo);
				strTmpWhere = strWhereRaw;
				if(strTerminal != "")
				{
					strTmpWhere += strTerminalWhereRaw;
				}
				strTmpWhere = strTmpWhere.Replace("@@FROM", strDateFrom);
				strTmpWhere = strTmpWhere.Replace("@@TO", strDateTo );
				strTmpWhere = strTmpWhere.Replace("@@ORIG",UT.Hopo);
				strTmpWhere = strTmpWhere.Replace("@@DEST",UT.Hopo);
				strTmpWhere = strTmpWhere.Replace("@@HOPO", UT.Hopo);
				strTmpWhere = strTmpWhere.Replace("@@STEV",strTerminal);
				strLOAWhereRaw = strLOAWhereRaw.Replace("@@HOPO", UT.Hopo);

				//strTmpWhere += "[ROTATIONS]";
				//Load the data from AFTTAB
				myAFT.Load(strTmpWhere);
				datReadFrom = datReadFrom.AddDays(1);//+=  new TimeSpan(1, 0, 0, 0, 0);
				loopCnt++;
			}while(datReadFrom <= datReadTo);
			
			for(int i = myAFT.Count-1; i >= 0; i--)
			{
				if(myAFT[i]["FTYP"] == "T" || myAFT[i]["FTYP"] == "G" || 
					myAFT[i]["FTYP"] == "N" || myAFT[i]["FTYP"] == "B" ||  myAFT[i]["FTYP"] == "Z" )
				{
					myAFT.Remove(i);
				}
			}

			LoadCheckInCounterData();
			LoadPaxAndLoadData();
			myAFT.TimeFieldsCurrentlyInUtc = UT.IsTimeInUtc;

			if(bmChuteDisplay == true)
			{
				//-------------------------------------------------------------------
				//Load chutes according to the AFT.URNO->CHA.RURN
				myDB.Unbind("DED_CHA");
				myCHA = myDB.Bind("DED_CHA", "CHA", "RURN,LNAM,STOB,STOE,TIFB,TIFE,LTYP,CHUF,CHUT", "10,5,14,14,14,14,3,5,5,3", "RURN,LNAM,STOB,STOE,TIFB,TIFE,LTYP");
				myCHA.Clear();
				lblProgress.Text = "Loading Chute Data";
				lblProgress.Refresh();
				int loops = 0;
				progressBar1.Value = 0;
				progressBar1.Refresh();
				ilTotal = (int)(myAFT.Count/300);
				if(ilTotal == 0) ilTotal = 1;
				string strRurns = "";
				string strTmpCHAWhere = strDedicatedCHAWhere;
				for(int i = 0; i < myAFT.Count; i++)
				{
					strRurns += myAFT[i]["URNO"] + ",";
					if((i % 300) == 0 && i > 0)
					{
						loops++;
						int percent = Convert.ToInt32((loops * 100)/ilTotal);
						if (percent > 100) 
							percent = 100;
						progressBar1.Value = percent;
						strRurns = strRurns.Remove(strRurns.Length-1, 1);
						strTmpCHAWhere = strDedicatedCHAWhere;
						strTmpCHAWhere = strTmpCHAWhere.Replace("@@RURN", strRurns);
						myCHA.Load(strTmpCHAWhere);
						strRurns = "";
					}
				}
				//Load the rest of the dedicated Chutes.
				if(strRurns.Length > 0)
				{
					strRurns = strRurns.Remove(strRurns.Length-1, 1);
					strTmpCHAWhere = strDedicatedCHAWhere;
					strTmpCHAWhere = strTmpCHAWhere.Replace("@@RURN", strRurns);
					myCHA.Load(strTmpCHAWhere);
					strRurns = "";
				}
				myCHA.Sort("STOB,TIFB,STOE,TIFE", true);
				myCHA.CreateIndex("RURN", "RURN");
			}
			lblProgress.Text = "";
			progressBar1.Hide();
		}

		/// <summary>
		/// Prepares the report to be displayed on the table(Arrival Report).
		/// </summary>
		private void PrepareReportDataArrival(string strReportOrExcel)
		{
			if (myAFT == null)
				return;
			StringBuilder strData = new StringBuilder(200);
			StringBuilder sb = new StringBuilder(100000);
			string strId = "";
			string strInf = "";
			iTotalFlights = 0;
			IRow [] rows;

			for (int i = 0; i<myAFT.Count ; i++ )
			{
				if( (myAFT[i]["ADID"] == "A") 
					 || (myAFT[i]["ADID"].Equals("B")) )
				{
					strData.Append(myAFT[i]["FLNO"]).Append(",");
					strData.Append(myAFT[i]["ORG3"]).Append(",");
					if(strReportOrExcel.Equals("Report") == true)
					{
                        if (bmAdditionalDateColumn)
                        {
                            strData.Append(Helper.DateString(myAFT[i]["STOA"], "dd.MM.yyyy")).Append(",");
                            strData.Append(Helper.DateString(myAFT[i]["STOA"], "HH:mm")).Append(",");
                            strData.Append(Helper.DateString(myAFT[i]["LAND"], "HH:mm")).Append(",");
                        }
                        else
                        {
                            strData.Append(Helper.DateString(myAFT[i]["STOA"], strDateDisplayFormat)).Append(",");
                            strData.Append(Helper.DateString(myAFT[i]["LAND"], strDateDisplayFormat)).Append(",");
                        }
					}
					else if(strReportOrExcel.Equals("Excel") == true)
					{
                        if (bmAdditionalDateColumn)
                        {
                            strData.Append(Helper.DateString(myAFT[i]["STOA"], "dd.MM.yyyy")).Append(",");
                            strData.Append(Helper.DateString(myAFT[i]["STOA"], "HH:mm")).Append(",");
                            strData.Append(Helper.DateString(myAFT[i]["LAND"], "HH:mm")).Append(",");
                        }
                        else
                        {
                            strData.Append(Helper.DateString(myAFT[i]["STOA"], strExcelDateTimeFormat)).Append(",");
                            strData.Append(Helper.DateString(myAFT[i]["LAND"], strExcelDateTimeFormat)).Append(",");
                        }
					}
					strData.Append(myAFT[i]["ACT3"]).Append(",");
					strData.Append(myAFT[i]["REGN"]).Append(",");
					strData.Append(myAFT[i]["TTYP"]).Append(",");
					strData.Append(myAFT[i]["ALC3"]).Append(",");
					strData.Append(myAFT[i]["PSTA"]).Append(",");
					if(bmShowStev == true)
					{
						strData.Append(myAFT[i]["STEV"]).Append(",");
					}
					if ( !myAFT[i]["GTA2"].Equals(""))
						strData.Append(myAFT[i]["GTA1"]).Append("/").Append(myAFT[i]["GTA2"]).Append(",");
					else
						strData.Append(myAFT[i]["GTA1"]).Append(",");
					if (!myAFT[i]["BLT2"].Equals(""))
						strData.Append(myAFT[i]["BLT1"]).Append("-").Append(myAFT[i]["BLT2"]).Append(",");
					else
						strData.Append(myAFT[i]["BLT1"]).Append(",");
					
					string[] strConfigReturn = new string[100];
					string strFinal = "";
					int mindex = 0;
					IEnumerator LoadPaxDataEnum = objConfig.GetEnumerator();
					while( LoadPaxDataEnum.MoveNext( ) )
					{
						LoadPaxData pl = (LoadPaxData)LoadPaxDataEnum.Current;
						if(pl.m_StrName != null)
						{
							strFinal = GetLoadData(pl,myAFT[i]["URNO"],i,strConfigReturn);
							strConfigReturn[mindex] = strFinal;	
							mindex++;
							strData.Append(strFinal).Append(",");
						}
					}
					LoadPaxDataEnum.Reset();
					sb.Append(strData.ToString());
					strData.Remove(0,strData.Length);
					strData.Append("\n");
					iTotalFlights++;
				}
			}
			if(strReportOrExcel.Equals("Report") == true)
			{
				tabResult.InsertBuffer(sb.ToString(),"\n");
				if(((TimeSpan)(dtTo.Value - dtFrom.Value)).TotalDays <= 1)
				{
					tabResult.Sort("2", true, true);
				}
				tabResult.Refresh();
				lblResults.Text = "Report Results: (" + tabResult.GetLineCount().ToString() + ")";
			}
			else if(strReportOrExcel.Equals("Excel") == true)
			{
				tabExcelResult.InsertBuffer(sb.ToString(),"\n");
				if(((TimeSpan)(dtTo.Value - dtFrom.Value)).TotalDays <= 1)
				{
					tabExcelResult.Sort("2", true, true);
				}
			}
		}

		/// <summary>
		/// PrepareReportDataDeparture prepares the display on the table.
		/// </summary>
		private void PrepareReportDataDeparture(string strReportOrExcel)
		{
			if (myAFT == null)
				return;
			StringBuilder strData = new StringBuilder(200);
			StringBuilder sb = new StringBuilder(100000);
			string strCknCtr;
			string strId = "";
			string strInf = "";
			iTotalFlights = 0;
			IRow [] rows;

			for (int i = 0; i<myAFT.Count ; i++ )
			{
				if ((myAFT[i]["ADID"] == "D") 
					|| (myAFT[i]["ADID"].Equals("B")) )
				{
					strData.Append(myAFT[i]["FLNO"]).Append(",");
					strData.Append(myAFT[i]["DES3"]).Append(",");
					if(strReportOrExcel.Equals("Report") == true)
					{
                        if (bmAdditionalDateColumn)
                        {
                            strData.Append(Helper.DateString(myAFT[i]["STOD"], "dd.MM.yyyy")).Append(",");
                            strData.Append(Helper.DateString(myAFT[i]["STOD"], "HH:mm")).Append(",");
                            strData.Append(Helper.DateString(myAFT[i]["AIRB"], "HH:mm")).Append(",");
                        }
                        else
                        {
                            strData.Append(Helper.DateString(myAFT[i]["STOD"], strDateDisplayFormat)).Append(",");
                            strData.Append(Helper.DateString(myAFT[i]["AIRB"], strDateDisplayFormat)).Append(",");
                        }
					}
					else if(strReportOrExcel.Equals("Excel") == true)
					{
                        if (bmAdditionalDateColumn)
                        {
                            strData.Append(Helper.DateString(myAFT[i]["STOD"], "dd.MM.yyyy")).Append(",");
                            strData.Append(Helper.DateString(myAFT[i]["STOD"], "HH:mm")).Append(",");
                            strData.Append(Helper.DateString(myAFT[i]["AIRB"], "HH:mm")).Append(",");
                        }
                        else
                        {
                            strData.Append(Helper.DateString(myAFT[i]["STOD"], strExcelDateTimeFormat)).Append(",");
                            strData.Append(Helper.DateString(myAFT[i]["AIRB"], strExcelDateTimeFormat)).Append(",");
                        }
					}
					strData.Append(myAFT[i]["ACT3"]).Append(",");
					strData.Append(myAFT[i]["REGN"]).Append(",");
					strData.Append(myAFT[i]["TTYP"]).Append(",");
					strData.Append(myAFT[i]["ALC3"]).Append(",");
					rows = myCCA.RowsByIndexValue("FLNU", myAFT[i]["URNO"]);
					strCknCtr = "";
					if(rows.Length == 0)
					{
						if(myAFT[i]["CKIF"] != "" || myAFT[i]["CKIT"] != "")
						{
							strCknCtr = myAFT[i]["CKIF"] + "/ " + myAFT[i]["CKIT"];
						}
						strData.Append(strCknCtr).Append(",");
					}
					else
					{
						for(int j = 0; j < rows.Length; j++)
						{
							strCknCtr += rows[j]["CKIC"] + "/ ";
						}
						strData.Append(strCknCtr.Substring(0,strCknCtr.Length-2)).Append(",");
					}
					if ( !myAFT[i]["GTD2"].Equals(""))
						strData.Append(myAFT[i]["GTD1"]).Append("/").Append(myAFT[i]["GTD2"]).Append(",");
					else
						strData.Append(myAFT[i]["GTD1"]).Append(",");
					strData.Append(myAFT[i]["PSTD"]).Append(",");
					if(bmShowStev == true)
					{
						strData.Append(myAFT[i]["STEV"]).Append(",");
					}
					bool blDataAppended = false;
					if(bmChuteDisplay == true)
					{
						rows = myCHA.RowsByIndexValue("RURN", myAFT[i]["URNO"]);
						if(rows.Length == 0)
						{
							strData.Append(",");
						}
						else
						{
							string[] chuteData = GetChuteData(rows);							
							for(int j = 0 ; j < chuteData.Length; j++)
							{
								string strRowValue = strData.ToString();
								strRowValue += chuteData[j] + ",";
								string[] strConfigReturn = new string[100];
								string strFinal = "";
								int mindex = 0;
								IEnumerator LoadPaxDataEnum = objConfig.GetEnumerator();
								while( LoadPaxDataEnum.MoveNext( ) )
								{
									LoadPaxData pl = (LoadPaxData)LoadPaxDataEnum.Current;
									if(pl.m_StrName != null)
									{
										strFinal = GetLoadData(pl,myAFT[i]["URNO"],i,strConfigReturn);
										strConfigReturn[mindex] = strFinal;	
										mindex++;
										strRowValue += strFinal + ",";
									}
								}
								strRowValue += "\n";
								LoadPaxDataEnum.Reset();
								sb.Append(strRowValue);
							}
							blDataAppended = true;
							//this.AppendChuteFromTo(ref strData,rows,sb);
						}
					}
					
					if(blDataAppended == false)
					{
						string[] strConfigReturn = new string[100];
						string strFinal = "";
						int mindex = 0;
						IEnumerator LoadPaxDataEnum = objConfig.GetEnumerator();
						while( LoadPaxDataEnum.MoveNext( ) )
						{
							LoadPaxData pl = (LoadPaxData)LoadPaxDataEnum.Current;
							if(pl.m_StrName != null)
							{
								strFinal = GetLoadData(pl,myAFT[i]["URNO"],i,strConfigReturn);
								strConfigReturn[mindex] = strFinal;	
								mindex++;
								strData.Append(strFinal).Append(",");
							}
						}
						LoadPaxDataEnum.Reset();						
						strData.Append("\n");
						sb.Append(strData.ToString());
					}
					
					strData.Remove(0,strData.Length);
					iTotalFlights++;
				}
			}
			if(strReportOrExcel.Equals("Report") == true)
			{
				tabResult.InsertBuffer(sb.ToString(), "\n");
				if(((TimeSpan)(dtTo.Value - dtFrom.Value)).TotalDays <= 1)
				{
					tabResult.Sort("2", true, true);
				}
				tabResult.Refresh();
			}
			else if(strReportOrExcel.Equals("Excel") == true)
			{
				tabExcelResult.InsertBuffer(sb.ToString(), "\n");
				if(((TimeSpan)(dtTo.Value - dtFrom.Value)).TotalDays <= 1)
				{
					tabExcelResult.Sort("2", true, true);
				}
			}
		}
		/// <summary>
		/// Loads the Check-In counter data corresponding to the flights 
		/// selected from the AFT table.
		/// </summary>
		private void LoadCheckInCounterData() 
		{
			int ilTotal = 0;
			int loopCnt = 0;
			int percent = 0;
			StringBuilder strFlnus = new StringBuilder();
			string strTmpQry;

			progressBar1.Value = 0;
			lblProgress.Text = "Loading Counter Data";
			lblProgress.Refresh();
			progressBar1.Show();
			myDB.Unbind("CCA");
			myCCA = myDB.Bind("CCA", "CCA"
				, "FLNU,CKIC"
				, "10,5"
				, "FLNU,CKIC");
			myCCA.Clear();

			ilTotal = (int)(myAFT.Count/300);
			if(ilTotal == 0) 
				ilTotal = 1;
			loopCnt = 0;
			for(int i = 0; i < myAFT.Count; i++)
			{
				strFlnus.Append(myAFT[i]["URNO"]).Append(",");
				if((i % 300) == 0 && i > 0)
				{
					loopCnt++;
					percent = Convert.ToInt32((loopCnt * 100)/ilTotal);
					if (percent > 100) 
						percent = 100;
					progressBar1.Value = percent;

					strFlnus = strFlnus.Remove(strFlnus.Length-1, 1);
					strTmpQry = strCCAWhereRaw;
					strTmpQry = strTmpQry.Replace("@@FLNU", strFlnus.ToString());
					myCCA.Load(strTmpQry);
					strFlnus.Remove(0,strFlnus.Length);
				}
			}
			if(strFlnus.Length > 0)
			{
				strFlnus = strFlnus.Remove(strFlnus.Length-1, 1);
				strTmpQry = strCCAWhereRaw;
				strTmpQry = strTmpQry.Replace("@@FLNU", strFlnus.ToString());
				myCCA.Load(strTmpQry);
				strFlnus.Remove(0,strFlnus.Length);
			}
			myCCA.CreateIndex("FLNU", "FLNU");
			lblProgress.Text = "";
			progressBar1.Hide();
		}

		private string[] GetChuteData(IRow[] rows)
		{
			// copy members to array list
			ArrayList myRows = new ArrayList(rows);
			// initialize members			
			for (int i = 0; i < myRows.Count; i++)
			{				
				((IRow)myRows[i])["CHUF"]=	((IRow)myRows[i])["LNAM"];
				((IRow)myRows[i])["CHUT"]=	((IRow)myRows[i])["LNAM"];
			}

			for (int i = 0; i < myRows.Count; i++)
			{				
				int ilCHUF = CalculateIntValueOfString(((IRow)myRows[i])["CHUF"]);
				int ilCHUT = CalculateIntValueOfString(((IRow)myRows[i])["CHUT"]);
				for (int j = 0; j < myRows.Count; j++)
				{					
					if (((IRow)myRows[i])["LTYP"] == ((IRow)myRows[j])["LTYP"] && 
						((IRow)myRows[i])["STOB"] == ((IRow)myRows[j])["STOB"] &&
						((IRow)myRows[i])["STOE"] == ((IRow)myRows[j])["STOE"])
					{
						if (CalculateIntValueOfString(((IRow)myRows[j])["CHUT"]) + 1 == ilCHUF ||
							CalculateIntValueOfString(((IRow)myRows[j])["CHUF"]) - 1 == ilCHUT )
						{
							if (CalculateIntValueOfString(((IRow)myRows[j])["CHUT"]) + 1 == ilCHUF )
							{								
								((IRow)myRows[i])["CHUF"] = ((IRow)myRows[j])["CHUF"];
							}
							else
							{								
								((IRow)myRows[i])["CHUT"] = ((IRow)myRows[j])["CHUT"];
							}
							
							myRows.RemoveAt(j);
							if ( j < i ) 
								i--;
							i--;
							break;
						}
					}
				}
			}
			
			string[] chuteData = new string[myRows.Count];
			for (int i = 0; i < myRows.Count; i++)
			{				
				chuteData[i] = ((IRow)myRows[i])["CHUF"] + "-" + ((IRow)myRows[i])["CHUT"];
			}
			return chuteData;
		}

		/// <summary>
		/// This function returns the int value of a string.
		/// </summary>
		/// <param name="strFlightValues"></param>
		/// <returns></returns>
		private int CalculateIntValueOfString(string strFlightValues)
		{
			string strNew = "";
			int total = 0;
			if(IsAlpha(strFlightValues))
			{
				char ab;
				IEnumerator strFltValEnum = strFlightValues.GetEnumerator();
				int CharCount = 0;
				while( strFltValEnum.MoveNext( ) )
				{
					CharCount++;
					ab = (char) strFltValEnum.Current;
					if(Char.IsNumber(ab))
						strNew += ab;
				}
				total = int.Parse(strNew);	
			}
			else
			{
				total = int.Parse(strFlightValues);
			}
			return total ;
		}
		/// <summary>
		/// Checks if a character is Alphabet or Numeric.
		/// </summary>
		/// <param name="strToCheck"></param>
		/// <returns></returns>
		public bool IsAlpha(string strToCheck)
		{
			Regex objAlphaPattern=new Regex("[A-Z]");

			return objAlphaPattern.IsMatch(strToCheck);
		}

		/// <summary>
		/// Read the config file .
		/// </summary>
		private void ReadConfigFile()
		{
			Ufis.Utils.IniFile myIni = new IniFile("C:\\Ufis\\system\\ceda.ini");
			string strLoadPaxConfigPath = myIni.IniReadValue("FIPSREPORT", "LOADPAX_CONFIG");
			if(strLoadPaxConfigPath.Length == 0)
			{
				MessageBox.Show("Configuration file path is not set in Ceda.ini!!");
				return;
			}
			FileStream fin;
			try
			{
				fin = new FileStream(strLoadPaxConfigPath, FileMode.OpenOrCreate,FileAccess.Read);
			}
			catch (IOException exc)
			{
				MessageBox.Show(this, strLoadPaxConfigPath, exc.Message + "Read Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
				return;
			}
			string txtLine = "";
			StreamReader fstr_in = new StreamReader(fin);
			LoadPaxData lp = new LoadPaxData();
			while ((txtLine = fstr_in.ReadLine()) != null)
			{
				if(txtLine.Length != 0)
				{
					string olStr = txtLine;//.Substring(.Substring(0);
					//if(System.String.CompareOrdinal(olStr,0,"NAMEe",0,5) != -1)
					if(olStr.IndexOf("NAME") >= 0)
					{
						int index = olStr.IndexOf("=");
						olStr = olStr.Remove(0,index+1);
						lp.m_StrName = olStr;
					}
					else if(olStr.IndexOf("TABLE") >= 0)
					{
						int index = olStr.IndexOf("=");
						olStr = olStr.Remove(0,index+1);
						lp.m_StrTable = olStr;
					}
					else if(olStr.IndexOf("PRIORITY") >= 0)
					{
						int index = olStr.IndexOf("=");
						olStr = olStr.Remove(0,index+1);
						lp.m_StrPriority = olStr;
					}
					else if(olStr.IndexOf("KEYS") >= 0)
					{
						int index = olStr.IndexOf("=");
						olStr = olStr.Remove(0,index+1);
						lp.m_StrKeys = olStr;
					}
					else if(olStr.IndexOf("SUMMARY") >= 0)
					{
						int index = olStr.IndexOf("=");
						olStr = olStr.Remove(0,index+1);
						lp.m_StrSummary = olStr;
					}
					else if(olStr.IndexOf("FIELD") >= 0)
					{
						int index = olStr.IndexOf("=");
						olStr = olStr.Remove(0,index+1);
						lp.m_StrField = olStr;
					}
				}
				else
				{
					if(lp.m_StrName != null)
					{
						objConfig.Add( lp);
						lp = new LoadPaxData();
					}
				}
			}
			if(lp != null)
			{
				if(lp.m_StrName != null)
				{
					objConfig.Add(lp);
				}
			}
		}
		/// <summary>
		/// Form the Header Information 
		/// </summary>
	
		public void FormHeaderString()
		{
			string strHeader = "";
			string strLogicalHeader = "";
			string strHeaderLength = "";
			string strLogicalField = "";
			string strFieldName = "";
			string strFieldNameTemp = "";
			string strFieldLen = "";

			IEnumerator LoadPaxDataEnum = objConfig.GetEnumerator();

			while(LoadPaxDataEnum.MoveNext( ) )
			{
				//objConfig.
				LoadPaxData pl = (LoadPaxData)LoadPaxDataEnum.Current;
				if(pl.m_StrName != null)
				{
					int index = pl.m_StrName.IndexOf(",");
					strLogicalField = pl.m_StrName.Remove(index,pl.m_StrName.Length - (index));
					strLogicalField = strLogicalField.Replace(" ","_");
					strFieldNameTemp = pl.m_StrName.Remove(0,index+1);
					index = strFieldNameTemp.IndexOf(",");
					if(index != -1)
					{
						strFieldName = strFieldNameTemp.Remove(index,strFieldNameTemp.Length-index );
						strFieldLen = strFieldNameTemp.Remove(0,index+1);
					}
					else
						strFieldName = strFieldNameTemp ;

					strLogicalField.Trim();
					strFieldName.Trim();
					strFieldLen.Trim();
					
					if(strFieldName.Length !=0 )
					{
						strHeader  += strFieldName + ",";
						strLogicalHeader +=  strLogicalField + ",";
						if(strFieldLen.Length == 0)
							strHeaderLength += "120" + ",";
						else
							strHeaderLength += strFieldLen + ",";
					}
					strFieldName = "";
					strLogicalField = "";
					strFieldLen = "";
				}
			} 

			if(strHeader.Length > 0)
			{
				strHeader = strHeader.Remove(strHeader.Length-1,1);
				strLogicalHeader = strLogicalHeader.Remove(strLogicalHeader.Length-1,1);
				strHeaderLength = strHeaderLength.Remove(strHeaderLength.Length-1,1);

				if(strHeader.Length > 0)
				{
					//Arrival Header Info
					strTabHeaderArrival += "," + strHeader ;
					strLogicalFieldsArrival += "," + strLogicalHeader ;
					strTabHeaderLensArrival += "," + strHeaderLength ;					
					strExcelTabHeaderLensArrival += "," + strHeaderLength ; 

					//Departure Header Info
					strTabHeaderDeparture += "," + strHeader;
					strLogicalFieldsDeparture += "," + strLogicalHeader;
					strTabHeaderLensDeparture += "," + strHeaderLength;
					strExcelTabHeaderLensDeparture+= "," + strHeaderLength;
				}
			}
		}
		/// <summary>
		/// Get the proper value according to the Config Data
		/// </summary>
		/// <param name="objLoadPaxData"></param>
		/// <param name="strAftUrno"></param>
		/// <param name="iMyAFT"></param>
		/// <param name="strConfigReturn"></param>
		/// <returns></returns>
		public string GetLoadData(LoadPaxData objLoadPaxData ,string strAftUrno,int iMyAFT,string[] strConfigReturn )
		{
			if(objLoadPaxData.m_StrTable.IndexOf("LOA") >= 0)
			{
				string strPriority ;
				string strKeys;
				string strTmp;
				strPriority = objLoadPaxData.m_StrPriority;
				strKeys = objLoadPaxData.m_StrKeys;
				string[] strArPrior  = strPriority.Split(new char[]{','});
				string[] strArKeys   = strKeys.Split(new char[]{','});
				string[] strLessPrio = new string[50];
								
				//Check with the keys
				int i = 0;
				IRow [] rows = myLOA.RowsByIndexValue("FLNU", strAftUrno);
				for(int k = 0; k < rows.Length; k++)
				{
					if(i < strArKeys.Length)
					{
						string TYPE = rows[k]["TYPE"];
						string ARKEY = strArKeys[i];

						if(rows[k]["TYPE"].Equals(strArKeys[i]))
						{
							if(i+3 < strArKeys.Length)
							{
								if( rows[k]["STYP"].Equals(strArKeys[i+1])
									&& rows[k]["SSTP"].Equals(strArKeys[i+2])
									&& rows[k]["SSST"].Equals(strArKeys[i+3]))
									{
										int j = 0;
										if(j < strArPrior.Length)
										{
											if(rows[k]["DSSN"].Equals(strArPrior[j]))
											{
												return rows[k]["VALU"];
											}
											j++;
										}
										int l = 0;	
										while(j < strArPrior.Length)
										{
											if(rows[k]["DSSN"].Equals(strArPrior[j]))
												strLessPrio[l] = rows[k]["VALU"];
											j++;
											l++;
										}	
								    }
							}
						}
					}
				}
				
				if(strLessPrio.Length > 0)
				{
					for(int p =0;p < strLessPrio.Length ; p++)
					{
						if(strLessPrio[p] != "" && strLessPrio[p] != null)
						{
							return strLessPrio[p];
						}
					}
				}
			}
			else if(objLoadPaxData.m_StrTable.IndexOf("COMPUTE") >= 0)
			{
				int firstColVal = 0;
				int secColVal = 0;
				int Total = 0;
				int ilindexSummary = 0;
				string[] strSummary = objLoadPaxData.m_StrSummary.Split(new char[]{','});
				int index = 0;
				while(strSummary.Length >= 2 )
				{
					while(index < strSummary.Length)
					{
						ilindexSummary = int.Parse(strSummary[index]);
						if(ilindexSummary-1 < strConfigReturn.Length && strConfigReturn[ilindexSummary-1] != null)
						{
							string strtmp =  strConfigReturn[ilindexSummary-1];
							if(strtmp.Length == 0)
								Total += 0;
							else
                                Total += int.Parse(strConfigReturn[ilindexSummary-1]);
						}
						index++;
					}
					return Total.ToString() ;
				}
			}
			else if(objLoadPaxData.m_StrTable.IndexOf("AFT") >= 0)
			{
				return myAFT[iMyAFT][objLoadPaxData.m_StrField];
			}
			return "";
		}
		/// <summary>
		/// Reads the Load and Pax data from the respective tables
		/// </summary>
		private void LoadPaxAndLoadData()
		{
			int ilTotal = 0;
			int loopCnt = 0;
			int percent = 0;
			string strTmpQry;
			StringBuilder strFlnus = new StringBuilder();

			progressBar1.Value = 0;
			lblProgress.Text = "Loading Load Data";
			lblProgress.Refresh();
			progressBar1.Show();
			myDB.Unbind("LOA");
			myLOA = myDB.Bind("LOA","LOA"
				,"FLNU,DSSN,TYPE,STYP,SSTP,SSST,VALU"
				,"10,10,14,40,1,3,3,3,3,6"
				,"FLNU,DSSN,TYPE,STYP,SSTP,SSST,VALU");
			myLOA.Clear();

			ilTotal = (int)(myAFT.Count/300);
			if(ilTotal == 0) 
				ilTotal = 1;
			loopCnt = 0;
			for(int i = 0; i < myAFT.Count; i++)
			{
				strFlnus.Append(myAFT[i]["URNO"]).Append(",");
				if((i % 300) == 0 && i > 0)
				{
					loopCnt++;
					percent = Convert.ToInt32((loopCnt * 100)/ilTotal);
					if (percent > 100) 
						percent = 100;
					progressBar1.Value = percent;

					strFlnus = strFlnus.Remove(strFlnus.Length-1, 1);
					strTmpQry = strLOAWhereRaw;
					strTmpQry = strTmpQry.Replace("@@FLNU", strFlnus.ToString());
					myLOA.Load(strTmpQry);
					strFlnus.Remove(0,strFlnus.Length);
				}
			}
			if(strFlnus.Length > 0)
			{
				strFlnus = strFlnus.Remove(strFlnus.Length-1, 1);
				strTmpQry = strLOAWhereRaw;
				strTmpQry = strTmpQry.Replace("@@FLNU", strFlnus.ToString());
				myLOA.Load(strTmpQry);
				strFlnus.Remove(0,strFlnus.Length);
			}
			myLOA.CreateIndex("FLNU", "FLNU");
			lblProgress.Text = "";
			progressBar1.Hide();
		}
		/// <summary>
		/// Arrival radio button change
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void radioButtonArrival_CheckedChanged(object sender, System.EventArgs e)
		{
			//InitTab();
			if(radioButtonArrival.Checked)
			{
				tabResult.HeaderString = strTabHeaderArrival;
				tabResult.LogicalFieldList = strLogicalFieldsArrival;
				tabResult.HeaderLengthString = strTabHeaderLensArrival;

				tabExcelResult.HeaderString = strTabHeaderArrival;
				tabExcelResult.LogicalFieldList = strLogicalFieldsArrival;
				tabExcelResult.HeaderLengthString = strTabHeaderLensArrival;
				if(tabResult.GetLineCount() != 0)
				{
					tabResult.ResetContent();
					PrepareReportDataArrival("Report");
					
					tabExcelResult.ResetContent();
					PrepareReportDataArrival("Excel");
				}
				lblResults.Text = "Report Results: (" + tabResult.GetLineCount().ToString() + ")";
			}
		}
		/// <summary>
		/// Departure radio button change
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void radioButtonDeparture_CheckedChanged(object sender, System.EventArgs e)
		{
			//InitTab();
			if(radioButtonDeparture.Checked)
			{
				tabResult.HeaderString = strTabHeaderDeparture;
				tabResult.LogicalFieldList = strLogicalFieldsDeparture;
				tabResult.HeaderLengthString = strTabHeaderLensDeparture;

				tabExcelResult.HeaderString = strTabHeaderDeparture;
				tabExcelResult.LogicalFieldList = strLogicalFieldsDeparture;
				tabExcelResult.HeaderLengthString = strTabHeaderLensDeparture;
				if(tabResult.GetLineCount() != 0)
				{
					tabResult.ResetContent();
					PrepareReportDataDeparture("Report");
					
					tabExcelResult.ResetContent();
					PrepareReportDataDeparture("Excel");
				}
				lblResults.Text = "Report Results: (" + tabResult.GetLineCount().ToString() + ")";
			}
		}

		private void buttonPrintPView_Click(object sender, System.EventArgs e)
		{
			RunReport();
		}

		private void btnLoadPrint_Click(object sender, System.EventArgs e)
		{
			this.Cursor = Cursors.WaitCursor;
			LoadReportData();
			if(this.radioButtonArrival.Checked == false)
			{
				PrepareReportDataDeparture("Report");
				PrepareReportDataDeparture("Excel");
			}
			else
			{
				PrepareReportDataArrival("Report");
				PrepareReportDataArrival("Excel");
			}
			RunReport();
			this.Cursor = Cursors.Arrow;
		}	

		/// <summary>
		/// Prepares the Print Preview page
		/// </summary>
		private void RunReport() 
		{
			StringBuilder strSubHeader = new StringBuilder();
			if (radioButtonArrival.Checked)
				strSubHeader.Append("Arrival ");
			else
				strSubHeader.Append("Departure ");
			strSubHeader.Append("From: ")
				.Append(dtFrom.Value.ToString("dd.MM.yy'/'HH:mm"))
				.Append(" To: ")
				.Append(dtTo.Value.ToString("dd.MM.yy'/'HH:mm"));
			strSubHeader.Append(" (Flights: ").Append(iTotalFlights.ToString());
			if (radioButtonArrival.Checked)
				strSubHeader.Append("/ARR:").Append(iTotalFlights.ToString())
					.Append(" /DEP:0)"); 
			else
				strSubHeader.Append("/ARR:0")
					.Append(" /DEP:").Append(iTotalFlights.ToString()).Append(")"); 

			strReportHeader = frmMain.strPrefixVal + "Load and Pax";
			strReportSubHeader = strSubHeader.ToString();
			rptA3_Landscape rpt = new rptA3_Landscape(tabResult,strReportHeader,strReportSubHeader,"",8);
			//prtFIPS_Landscape rptPrtFIPS = new prtFIPS_Landscape(tabResult,strReportHeader,strReportSubHeader,"",8);
						
			if(this.radioButtonArrival.Checked)
			{
				rpt.SetExcelExportHeaderLenghtString(strExcelTabHeaderLensArrival);
			}
			else
			{
				rpt.SetExcelExportHeaderLenghtString(strExcelTabHeaderLensDeparture);
			}
			rpt.SetExcelExportTabResult(tabExcelResult);
			//rpt.TextBoxCanGrow = false;
			
			frmPrintPreview frm = new frmPrintPreview(rpt);
			if(Helper.UseSpreadBuilder() == true)
			{
				activeReport = rpt;
				frm.UseSpreadBuilder(true);
				frm.GetBtnExcelExport().Click += new EventHandler(btnExcelExport_Click);
			}
			else
			{
				frm.UseSpreadBuilder(false);
			}
			frm.Show();	
		}

		private void tabResult_SendLButtonDblClick(object sender, AxTABLib._DTABEvents_SendLButtonDblClickEvent e)
		{
			if(e.lineNo == -1)
			{
				if( e.colNo == tabResult.CurrentSortColumn)
				{
					if( tabResult.SortOrderASC == true)
					{
						tabResult.Sort(e.colNo.ToString(), false, true);
						tabExcelResult.Sort(e.colNo.ToString(), false, true);
					}
					else
					{
						tabResult.Sort(e.colNo.ToString(), true, true);
						tabExcelResult.Sort(e.colNo.ToString(), true, true);
					}
				}
				else
				{
					tabResult.Sort( e.colNo.ToString(), true, true);
					tabExcelResult.Sort( e.colNo.ToString(), true, true);
				}
				tabResult.Refresh();
			}
		}

		private void InitializeMouseEvents()
		{
			((System.Windows.Forms.Control)tabResult).MouseWheel += new MouseEventHandler(frmCongfigLoadPax_MouseWheel);
			((System.Windows.Forms.Control)tabResult).MouseEnter += new EventHandler(frmCongfigLoadPax_MouseEnter);
			((System.Windows.Forms.Control)tabResult).MouseLeave += new EventHandler(frmCongfigLoadPax_MouseLeave);
		}

		private void frmCongfigLoadPax_MouseWheel(object sender, MouseEventArgs e)
		{
			if(bmMouseInsideTabControl == false || Helper.CheckScrollingStatus(tabResult) == false)
			{
				return;
			}
			if(e.Delta > 0 && tabResult.GetVScrollPos() > 0)
			{				
				tabResult.OnVScrollTo(tabResult.GetVScrollPos() - 1);
			}
			else if(e.Delta < 0 && (tabResult.GetVScrollPos() + 1) < tabResult.GetLineCount())
			{
				tabResult.OnVScrollTo(tabResult.GetVScrollPos() + 1);
			}
		}

		private void frmCongfigLoadPax_MouseEnter(object sender, EventArgs e)
		{			
			bmMouseInsideTabControl = true;
		}

		private void frmCongfigLoadPax_MouseLeave(object sender, EventArgs e)
		{
			bmMouseInsideTabControl = false;
		}

		private void btnExcelExport_Click(object sender, EventArgs e)
		{			
			if (radioButtonArrival.Checked)
			{
				Helper.ExportToExcel(tabExcelResult,strReportHeader,strReportSubHeader,strExcelTabHeaderLensArrival,8,0,activeReport);
			}
			else
			{
				Helper.ExportToExcel(tabExcelResult,strReportHeader,strReportSubHeader,strExcelTabHeaderLensDeparture,8,0,activeReport);
			}
		}

		private void buttonHelp_Click(object sender, System.EventArgs e)
		{
			Helper.ShowHelpFile(this);
		}

		private void frmCongfigLoadPax_HelpRequested(object sender, HelpEventArgs hlpevent)
		{
			Helper.ShowHelpFile(this);
		}
	}

	/// <summary>
	/// Class for storing the Configuration data of the columns
	/// </summary>
	
	public class LoadPaxData
	{
		public string m_StrName;
		public string m_StrTable;
		public string m_StrPriority;
		public string m_StrKeys;
		public string m_StrSummary;
		public string m_StrField;
	}
}
