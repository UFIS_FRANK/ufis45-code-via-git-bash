using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;
using Ufis.Data;
using Ufis.Utils;

namespace UserControls
{
	/// <summary>
	/// Summary description for ucSelectionPage.
	/// </summary>
	public class ucSelectionPage : System.Windows.Forms.UserControl
	{
		private System.Windows.Forms.Label lblAbsolutePeriod;
		private NullableDateTimePicker dtpPeriodFrom;
		private NullableDateTimePicker dtpPeriodTo;
		private System.Windows.Forms.Label lblDays;
		private System.Windows.Forms.Label lblCalculated;
		private System.Windows.Forms.NumericUpDown updBefore;
		private System.Windows.Forms.Label lblRelativePeriod;
		private System.Windows.Forms.Label lblBefore;
		private System.Windows.Forms.NumericUpDown updAfter;
		private System.Windows.Forms.Label lblAfter;
		private NullableDateTimePicker dtpChangesFrom;
		private System.Windows.Forms.Label lblChangesFrom;
		private NullableDateTimePicker dtpFlicoFrom;
		private System.Windows.Forms.Label lblFlicoFrom;
		private System.Windows.Forms.Label lblFlightNumber;
		private TextBoxRegex txtFlightNumber;
		private System.Windows.Forms.CheckBox cbCodeShare;
		private System.Windows.Forms.TextBox txtCallsign;
		private System.Windows.Forms.Label lblCallsign;
		private System.Windows.Forms.TextBox txtFlightId;
		private System.Windows.Forms.Label lblFlightId;
		private System.Windows.Forms.Label lblAircraftType;
		private System.Windows.Forms.Label lblRegistration;
		private System.Windows.Forms.Label lblVia;
		private System.Windows.Forms.Label lblKeycode;
		private System.Windows.Forms.Label lblNature;
		private System.Windows.Forms.Label lblHandlingType;
		private System.Windows.Forms.Label lblHandlingAgent;
		private System.Windows.Forms.CheckBox cbArrival;
		private System.Windows.Forms.CheckBox cbDeparture;
		private System.Windows.Forms.CheckBox cbRotation;
		private System.Windows.Forms.CheckBox cbCancelled;
		private System.Windows.Forms.CheckBox cbNoop;
		private System.Windows.Forms.CheckBox cbDiverted;
		private System.Windows.Forms.CheckBox cbRerouted;
		private System.Windows.Forms.CheckBox cbTowing;
		private System.Windows.Forms.CheckBox cbGroundMovement;
		private System.Windows.Forms.CheckBox cbPrognosis;
		private System.Windows.Forms.CheckBox cbPlanning;
		private System.Windows.Forms.CheckBox cbOperational;
		private System.Windows.Forms.GroupBox grpType;
		private System.Windows.Forms.GroupBox grpMode;
		private System.Windows.Forms.GroupBox grpStatus;
		private System.Windows.Forms.ComboBox cboHandlingAgent;
		private System.Windows.Forms.ComboBox cboHandlingType;
		private System.Windows.Forms.CheckedListBox clbNature;
		private System.Windows.Forms.TextBox txtRegistration;
		private System.Windows.Forms.CheckedListBox clbAirport;
		private System.Windows.Forms.CheckedListBox clbAircraftTypes;

		private	ViewFilter					filter	= null;
		private	ViewFilter.ViewSelectionRow	view	= null;

		private IDatabase myDB	= null;
		private	ITable	  myACT	= null;
		private	ITable	  myAPT	= null;
		private	ITable	  myNAT	= null;
		private	ITable	  myHTY = null;
		private ITable	  myHAG = null;
		private	bool	  initialized = false;
		private System.Windows.Forms.GroupBox grbOnGround;
		private System.Windows.Forms.TextBox txtOwner;
		private System.Windows.Forms.Label lblOwner;
		private UserControls.TextBoxRegex txtAirlineCode;
		private UserControls.TextBoxRegex txtFlightSuffix;
		private UserControls.TextBoxRegex txtDays;
		private System.Windows.Forms.TextBox txtStatisticId;
		private System.Windows.Forms.ToolTip toolTip1;
		private System.Windows.Forms.CheckedListBox clbVia;
		private System.Windows.Forms.Label lblOrigin;
		private System.Windows.Forms.CheckedListBox clbDestination;
		private System.Windows.Forms.Label lblDestination;
		private System.Windows.Forms.Button btnOrigin;
		private System.Windows.Forms.Button btnNature;
		private System.Windows.Forms.Button btnDestination;
		private System.Windows.Forms.Button btnAircraftType;
		private System.Windows.Forms.Button btnVia;
		private System.ComponentModel.IContainer components;

		public ucSelectionPage()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();

			// TODO: Add any initialization after the InitializeComponent call
			this.dtpPeriodFrom.CustomFormat = "dd.MM.yyyy - HH:mm";
			this.dtpPeriodTo.CustomFormat = "dd.MM.yyyy - HH:mm";
			this.dtpChangesFrom.CustomFormat = "dd.MM.yyyy - HH:mm";
			this.dtpFlicoFrom.CustomFormat = "dd.MM.yyyy - HH:mm";

			myDB = UT.GetMemDB();

			if (this.myACT == null)
			{
				this.myACT = myDB["ucSelectionPage.ACT"];
				if (this.myACT == null)
				{
					this.myACT = myDB.Bind("ucSelectionPage.ACT","ACT","ACT3,ACT5,ACFN","3,5,35","ACT3,ACT5,ACFN");
					this.myACT.Load("");
				}
			}

			if (this.myAPT == null)
			{
				this.myAPT = myDB["ucSelectionPage.APT"];
				if (this.myAPT == null)
				{
					this.myAPT = myDB.Bind("ucSelectionPage.APT","APT","APC3,APC4,APFN","3,4,32","APC3,APC4,APFN");
					this.myAPT.Load("");
				}
			}

			if (this.myNAT == null)
			{
				this.myNAT = myDB["ucSelectionPage.NAT"];
				if (this.myNAT == null)
				{
					this.myNAT = myDB.Bind("ucSelectionPage.NAT","NAT","TTYP,TNAM","5,30","TTYP,TNAM");
					myNAT.Load("ORDER BY TTYP");
				}
			}

			if (this.myHTY == null)
			{
				this.myHTY = myDB["ucSelectionPage.HTY"];
				if (this.myHTY == null)
				{
					this.myHTY = myDB.Bind("ucSelectionPage.HTY","HTY","HTYP,HNAM","5,40","HTYP,HNAM");
					this.myHTY.Load("");
				}
			}

			if (this.myHAG == null)
			{
				this.myHAG = myDB["ucSelectionPage.HAG"];
				if (this.myHAG == null)
				{
					this.myHAG = myDB.Bind("ucSelectionPage.HAG","HAG","HSNA,HNAM","5,30","HSNA,HNAM");
					this.myHAG.Load("");
				}
			}

			this.initialized = false;

			this.clbAircraftTypes.Items.Clear();
			string[] names = new string[this.myACT.Count];
			for(int i = 0; i < this.myACT.Count; i++)
			{
				names[i] = this.myACT[i]["ACT3"] + "," + this.myACT[i]["ACT5"];
			}
			this.clbAircraftTypes.Items.AddRange(names);

			this.clbAirport.Items.Clear();
			this.clbVia.Items.Clear();
			this.clbDestination.Items.Clear();
			names = new string[this.myAPT.Count];
			for(int i = 0; i < this.myAPT.Count; i++)
			{
				names[i] = this.myAPT[i]["APC3"] + "," + this.myAPT[i]["APC4"];
			}
			this.clbAirport.Items.AddRange(names);
			this.clbVia.Items.AddRange(names);
			this.clbDestination.Items.AddRange(names);

			this.clbNature.Items.Clear();
			names = new string[this.myNAT.Count];
			for(int i = 0; i < this.myNAT.Count; i++)
			{
				names[i] = this.myNAT[i]["TTYP"];
			}

			this.clbNature.Items.AddRange(names);

			this.cboHandlingAgent.Items.Clear();
			this.cboHandlingAgent.Items.Add("");
			for(int i = 0; i < this.myHAG.Count; i++)
			{
				this.cboHandlingAgent.Items.Add(this.myHAG[i]["HSNA"]);
			}

			this.cboHandlingType.Items.Clear();
			this.cboHandlingType.Items.Add("");
			for(int i = 0; i < this.myHTY.Count; i++)
			{
				this.cboHandlingType.Items.Add(this.myHTY[i]["HTYP"]);
			}

			this.initialized = true;

		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Component Designer generated code
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(ucSelectionPage));
			this.lblAbsolutePeriod = new System.Windows.Forms.Label();
			this.dtpPeriodFrom = new UserControls.NullableDateTimePicker();
			this.dtpPeriodTo = new UserControls.NullableDateTimePicker();
			this.lblDays = new System.Windows.Forms.Label();
			this.lblCalculated = new System.Windows.Forms.Label();
			this.updBefore = new System.Windows.Forms.NumericUpDown();
			this.lblRelativePeriod = new System.Windows.Forms.Label();
			this.lblBefore = new System.Windows.Forms.Label();
			this.updAfter = new System.Windows.Forms.NumericUpDown();
			this.lblAfter = new System.Windows.Forms.Label();
			this.dtpChangesFrom = new UserControls.NullableDateTimePicker();
			this.lblChangesFrom = new System.Windows.Forms.Label();
			this.dtpFlicoFrom = new UserControls.NullableDateTimePicker();
			this.lblFlicoFrom = new System.Windows.Forms.Label();
			this.lblFlightNumber = new System.Windows.Forms.Label();
			this.txtFlightNumber = new UserControls.TextBoxRegex();
			this.cbCodeShare = new System.Windows.Forms.CheckBox();
			this.txtCallsign = new System.Windows.Forms.TextBox();
			this.lblCallsign = new System.Windows.Forms.Label();
			this.txtFlightId = new System.Windows.Forms.TextBox();
			this.lblFlightId = new System.Windows.Forms.Label();
			this.lblAircraftType = new System.Windows.Forms.Label();
			this.txtRegistration = new System.Windows.Forms.TextBox();
			this.lblRegistration = new System.Windows.Forms.Label();
			this.lblVia = new System.Windows.Forms.Label();
			this.lblOrigin = new System.Windows.Forms.Label();
			this.txtStatisticId = new System.Windows.Forms.TextBox();
			this.lblKeycode = new System.Windows.Forms.Label();
			this.lblNature = new System.Windows.Forms.Label();
			this.lblHandlingType = new System.Windows.Forms.Label();
			this.lblHandlingAgent = new System.Windows.Forms.Label();
			this.grpType = new System.Windows.Forms.GroupBox();
			this.cbRotation = new System.Windows.Forms.CheckBox();
			this.cbDeparture = new System.Windows.Forms.CheckBox();
			this.cbArrival = new System.Windows.Forms.CheckBox();
			this.cbCancelled = new System.Windows.Forms.CheckBox();
			this.cbNoop = new System.Windows.Forms.CheckBox();
			this.cbDiverted = new System.Windows.Forms.CheckBox();
			this.cbRerouted = new System.Windows.Forms.CheckBox();
			this.grbOnGround = new System.Windows.Forms.GroupBox();
			this.cbTowing = new System.Windows.Forms.CheckBox();
			this.cbGroundMovement = new System.Windows.Forms.CheckBox();
			this.grpMode = new System.Windows.Forms.GroupBox();
			this.cbPrognosis = new System.Windows.Forms.CheckBox();
			this.cbPlanning = new System.Windows.Forms.CheckBox();
			this.cbOperational = new System.Windows.Forms.CheckBox();
			this.grpStatus = new System.Windows.Forms.GroupBox();
			this.cboHandlingAgent = new System.Windows.Forms.ComboBox();
			this.cboHandlingType = new System.Windows.Forms.ComboBox();
			this.clbNature = new System.Windows.Forms.CheckedListBox();
			this.clbAirport = new System.Windows.Forms.CheckedListBox();
			this.clbAircraftTypes = new System.Windows.Forms.CheckedListBox();
			this.txtOwner = new System.Windows.Forms.TextBox();
			this.lblOwner = new System.Windows.Forms.Label();
			this.txtAirlineCode = new UserControls.TextBoxRegex();
			this.txtFlightSuffix = new UserControls.TextBoxRegex();
			this.txtDays = new UserControls.TextBoxRegex();
			this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
			this.clbVia = new System.Windows.Forms.CheckedListBox();
			this.clbDestination = new System.Windows.Forms.CheckedListBox();
			this.lblDestination = new System.Windows.Forms.Label();
			this.btnOrigin = new System.Windows.Forms.Button();
			this.btnNature = new System.Windows.Forms.Button();
			this.btnDestination = new System.Windows.Forms.Button();
			this.btnAircraftType = new System.Windows.Forms.Button();
			this.btnVia = new System.Windows.Forms.Button();
			((System.ComponentModel.ISupportInitialize)(this.updBefore)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.updAfter)).BeginInit();
			this.grpType.SuspendLayout();
			this.grbOnGround.SuspendLayout();
			this.grpMode.SuspendLayout();
			this.grpStatus.SuspendLayout();
			this.SuspendLayout();
			// 
			// lblAbsolutePeriod
			// 
			this.lblAbsolutePeriod.AccessibleDescription = resources.GetString("lblAbsolutePeriod.AccessibleDescription");
			this.lblAbsolutePeriod.AccessibleName = resources.GetString("lblAbsolutePeriod.AccessibleName");
			this.lblAbsolutePeriod.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("lblAbsolutePeriod.Anchor")));
			this.lblAbsolutePeriod.AutoSize = ((bool)(resources.GetObject("lblAbsolutePeriod.AutoSize")));
			this.lblAbsolutePeriod.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("lblAbsolutePeriod.Dock")));
			this.lblAbsolutePeriod.Enabled = ((bool)(resources.GetObject("lblAbsolutePeriod.Enabled")));
			this.lblAbsolutePeriod.Font = ((System.Drawing.Font)(resources.GetObject("lblAbsolutePeriod.Font")));
			this.lblAbsolutePeriod.Image = ((System.Drawing.Image)(resources.GetObject("lblAbsolutePeriod.Image")));
			this.lblAbsolutePeriod.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("lblAbsolutePeriod.ImageAlign")));
			this.lblAbsolutePeriod.ImageIndex = ((int)(resources.GetObject("lblAbsolutePeriod.ImageIndex")));
			this.lblAbsolutePeriod.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("lblAbsolutePeriod.ImeMode")));
			this.lblAbsolutePeriod.Location = ((System.Drawing.Point)(resources.GetObject("lblAbsolutePeriod.Location")));
			this.lblAbsolutePeriod.Name = "lblAbsolutePeriod";
			this.lblAbsolutePeriod.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("lblAbsolutePeriod.RightToLeft")));
			this.lblAbsolutePeriod.Size = ((System.Drawing.Size)(resources.GetObject("lblAbsolutePeriod.Size")));
			this.lblAbsolutePeriod.TabIndex = ((int)(resources.GetObject("lblAbsolutePeriod.TabIndex")));
			this.lblAbsolutePeriod.Text = resources.GetString("lblAbsolutePeriod.Text");
			this.lblAbsolutePeriod.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("lblAbsolutePeriod.TextAlign")));
			this.toolTip1.SetToolTip(this.lblAbsolutePeriod, resources.GetString("lblAbsolutePeriod.ToolTip"));
			this.lblAbsolutePeriod.Visible = ((bool)(resources.GetObject("lblAbsolutePeriod.Visible")));
			this.lblAbsolutePeriod.Click += new System.EventHandler(this.label1_Click);
			// 
			// dtpPeriodFrom
			// 
			this.dtpPeriodFrom.AccessibleDescription = resources.GetString("dtpPeriodFrom.AccessibleDescription");
			this.dtpPeriodFrom.AccessibleName = resources.GetString("dtpPeriodFrom.AccessibleName");
			this.dtpPeriodFrom.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("dtpPeriodFrom.Anchor")));
			this.dtpPeriodFrom.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("dtpPeriodFrom.BackgroundImage")));
			this.dtpPeriodFrom.CalendarFont = ((System.Drawing.Font)(resources.GetObject("dtpPeriodFrom.CalendarFont")));
			this.dtpPeriodFrom.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("dtpPeriodFrom.Dock")));
			this.dtpPeriodFrom.DropDownAlign = ((System.Windows.Forms.LeftRightAlignment)(resources.GetObject("dtpPeriodFrom.DropDownAlign")));
			this.dtpPeriodFrom.Enabled = ((bool)(resources.GetObject("dtpPeriodFrom.Enabled")));
			this.dtpPeriodFrom.Font = ((System.Drawing.Font)(resources.GetObject("dtpPeriodFrom.Font")));
			this.dtpPeriodFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dtpPeriodFrom.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("dtpPeriodFrom.ImeMode")));
			this.dtpPeriodFrom.Location = ((System.Drawing.Point)(resources.GetObject("dtpPeriodFrom.Location")));
			this.dtpPeriodFrom.Name = "dtpPeriodFrom";
			this.dtpPeriodFrom.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("dtpPeriodFrom.RightToLeft")));
			this.dtpPeriodFrom.Size = ((System.Drawing.Size)(resources.GetObject("dtpPeriodFrom.Size")));
			this.dtpPeriodFrom.TabIndex = ((int)(resources.GetObject("dtpPeriodFrom.TabIndex")));
			this.toolTip1.SetToolTip(this.dtpPeriodFrom, resources.GetString("dtpPeriodFrom.ToolTip"));
			this.dtpPeriodFrom.Value = new System.DateTime(2005, 11, 17, 8, 59, 46, 207);
			this.dtpPeriodFrom.Visible = ((bool)(resources.GetObject("dtpPeriodFrom.Visible")));
			this.dtpPeriodFrom.ValueChanged += new System.EventHandler(this.dtpPeriodFrom_ValueChanged);
			// 
			// dtpPeriodTo
			// 
			this.dtpPeriodTo.AccessibleDescription = resources.GetString("dtpPeriodTo.AccessibleDescription");
			this.dtpPeriodTo.AccessibleName = resources.GetString("dtpPeriodTo.AccessibleName");
			this.dtpPeriodTo.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("dtpPeriodTo.Anchor")));
			this.dtpPeriodTo.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("dtpPeriodTo.BackgroundImage")));
			this.dtpPeriodTo.CalendarFont = ((System.Drawing.Font)(resources.GetObject("dtpPeriodTo.CalendarFont")));
			this.dtpPeriodTo.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("dtpPeriodTo.Dock")));
			this.dtpPeriodTo.DropDownAlign = ((System.Windows.Forms.LeftRightAlignment)(resources.GetObject("dtpPeriodTo.DropDownAlign")));
			this.dtpPeriodTo.Enabled = ((bool)(resources.GetObject("dtpPeriodTo.Enabled")));
			this.dtpPeriodTo.Font = ((System.Drawing.Font)(resources.GetObject("dtpPeriodTo.Font")));
			this.dtpPeriodTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dtpPeriodTo.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("dtpPeriodTo.ImeMode")));
			this.dtpPeriodTo.Location = ((System.Drawing.Point)(resources.GetObject("dtpPeriodTo.Location")));
			this.dtpPeriodTo.Name = "dtpPeriodTo";
			this.dtpPeriodTo.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("dtpPeriodTo.RightToLeft")));
			this.dtpPeriodTo.Size = ((System.Drawing.Size)(resources.GetObject("dtpPeriodTo.Size")));
			this.dtpPeriodTo.TabIndex = ((int)(resources.GetObject("dtpPeriodTo.TabIndex")));
			this.toolTip1.SetToolTip(this.dtpPeriodTo, resources.GetString("dtpPeriodTo.ToolTip"));
			this.dtpPeriodTo.Value = new System.DateTime(2005, 11, 17, 8, 59, 46, 223);
			this.dtpPeriodTo.Visible = ((bool)(resources.GetObject("dtpPeriodTo.Visible")));
			this.dtpPeriodTo.ValueChanged += new System.EventHandler(this.dtpPeriodTo_ValueChanged);
			// 
			// lblDays
			// 
			this.lblDays.AccessibleDescription = resources.GetString("lblDays.AccessibleDescription");
			this.lblDays.AccessibleName = resources.GetString("lblDays.AccessibleName");
			this.lblDays.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("lblDays.Anchor")));
			this.lblDays.AutoSize = ((bool)(resources.GetObject("lblDays.AutoSize")));
			this.lblDays.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("lblDays.Dock")));
			this.lblDays.Enabled = ((bool)(resources.GetObject("lblDays.Enabled")));
			this.lblDays.Font = ((System.Drawing.Font)(resources.GetObject("lblDays.Font")));
			this.lblDays.Image = ((System.Drawing.Image)(resources.GetObject("lblDays.Image")));
			this.lblDays.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("lblDays.ImageAlign")));
			this.lblDays.ImageIndex = ((int)(resources.GetObject("lblDays.ImageIndex")));
			this.lblDays.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("lblDays.ImeMode")));
			this.lblDays.Location = ((System.Drawing.Point)(resources.GetObject("lblDays.Location")));
			this.lblDays.Name = "lblDays";
			this.lblDays.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("lblDays.RightToLeft")));
			this.lblDays.Size = ((System.Drawing.Size)(resources.GetObject("lblDays.Size")));
			this.lblDays.TabIndex = ((int)(resources.GetObject("lblDays.TabIndex")));
			this.lblDays.Text = resources.GetString("lblDays.Text");
			this.lblDays.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("lblDays.TextAlign")));
			this.toolTip1.SetToolTip(this.lblDays, resources.GetString("lblDays.ToolTip"));
			this.lblDays.Visible = ((bool)(resources.GetObject("lblDays.Visible")));
			// 
			// lblCalculated
			// 
			this.lblCalculated.AccessibleDescription = resources.GetString("lblCalculated.AccessibleDescription");
			this.lblCalculated.AccessibleName = resources.GetString("lblCalculated.AccessibleName");
			this.lblCalculated.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("lblCalculated.Anchor")));
			this.lblCalculated.AutoSize = ((bool)(resources.GetObject("lblCalculated.AutoSize")));
			this.lblCalculated.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("lblCalculated.Dock")));
			this.lblCalculated.Enabled = ((bool)(resources.GetObject("lblCalculated.Enabled")));
			this.lblCalculated.Font = ((System.Drawing.Font)(resources.GetObject("lblCalculated.Font")));
			this.lblCalculated.Image = ((System.Drawing.Image)(resources.GetObject("lblCalculated.Image")));
			this.lblCalculated.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("lblCalculated.ImageAlign")));
			this.lblCalculated.ImageIndex = ((int)(resources.GetObject("lblCalculated.ImageIndex")));
			this.lblCalculated.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("lblCalculated.ImeMode")));
			this.lblCalculated.Location = ((System.Drawing.Point)(resources.GetObject("lblCalculated.Location")));
			this.lblCalculated.Name = "lblCalculated";
			this.lblCalculated.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("lblCalculated.RightToLeft")));
			this.lblCalculated.Size = ((System.Drawing.Size)(resources.GetObject("lblCalculated.Size")));
			this.lblCalculated.TabIndex = ((int)(resources.GetObject("lblCalculated.TabIndex")));
			this.lblCalculated.Text = resources.GetString("lblCalculated.Text");
			this.lblCalculated.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("lblCalculated.TextAlign")));
			this.toolTip1.SetToolTip(this.lblCalculated, resources.GetString("lblCalculated.ToolTip"));
			this.lblCalculated.Visible = ((bool)(resources.GetObject("lblCalculated.Visible")));
			// 
			// updBefore
			// 
			this.updBefore.AccessibleDescription = resources.GetString("updBefore.AccessibleDescription");
			this.updBefore.AccessibleName = resources.GetString("updBefore.AccessibleName");
			this.updBefore.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("updBefore.Anchor")));
			this.updBefore.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("updBefore.Dock")));
			this.updBefore.Enabled = ((bool)(resources.GetObject("updBefore.Enabled")));
			this.updBefore.Font = ((System.Drawing.Font)(resources.GetObject("updBefore.Font")));
			this.updBefore.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("updBefore.ImeMode")));
			this.updBefore.Location = ((System.Drawing.Point)(resources.GetObject("updBefore.Location")));
			this.updBefore.Name = "updBefore";
			this.updBefore.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("updBefore.RightToLeft")));
			this.updBefore.Size = ((System.Drawing.Size)(resources.GetObject("updBefore.Size")));
			this.updBefore.TabIndex = ((int)(resources.GetObject("updBefore.TabIndex")));
			this.updBefore.TextAlign = ((System.Windows.Forms.HorizontalAlignment)(resources.GetObject("updBefore.TextAlign")));
			this.updBefore.ThousandsSeparator = ((bool)(resources.GetObject("updBefore.ThousandsSeparator")));
			this.toolTip1.SetToolTip(this.updBefore, resources.GetString("updBefore.ToolTip"));
			this.updBefore.UpDownAlign = ((System.Windows.Forms.LeftRightAlignment)(resources.GetObject("updBefore.UpDownAlign")));
			this.updBefore.Visible = ((bool)(resources.GetObject("updBefore.Visible")));
			this.updBefore.ValueChanged += new System.EventHandler(this.updBefore_ValueChanged);
			this.updBefore.Leave += new System.EventHandler(this.updBefore_ValueChanged);
			// 
			// lblRelativePeriod
			// 
			this.lblRelativePeriod.AccessibleDescription = resources.GetString("lblRelativePeriod.AccessibleDescription");
			this.lblRelativePeriod.AccessibleName = resources.GetString("lblRelativePeriod.AccessibleName");
			this.lblRelativePeriod.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("lblRelativePeriod.Anchor")));
			this.lblRelativePeriod.AutoSize = ((bool)(resources.GetObject("lblRelativePeriod.AutoSize")));
			this.lblRelativePeriod.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("lblRelativePeriod.Dock")));
			this.lblRelativePeriod.Enabled = ((bool)(resources.GetObject("lblRelativePeriod.Enabled")));
			this.lblRelativePeriod.Font = ((System.Drawing.Font)(resources.GetObject("lblRelativePeriod.Font")));
			this.lblRelativePeriod.Image = ((System.Drawing.Image)(resources.GetObject("lblRelativePeriod.Image")));
			this.lblRelativePeriod.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("lblRelativePeriod.ImageAlign")));
			this.lblRelativePeriod.ImageIndex = ((int)(resources.GetObject("lblRelativePeriod.ImageIndex")));
			this.lblRelativePeriod.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("lblRelativePeriod.ImeMode")));
			this.lblRelativePeriod.Location = ((System.Drawing.Point)(resources.GetObject("lblRelativePeriod.Location")));
			this.lblRelativePeriod.Name = "lblRelativePeriod";
			this.lblRelativePeriod.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("lblRelativePeriod.RightToLeft")));
			this.lblRelativePeriod.Size = ((System.Drawing.Size)(resources.GetObject("lblRelativePeriod.Size")));
			this.lblRelativePeriod.TabIndex = ((int)(resources.GetObject("lblRelativePeriod.TabIndex")));
			this.lblRelativePeriod.Text = resources.GetString("lblRelativePeriod.Text");
			this.lblRelativePeriod.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("lblRelativePeriod.TextAlign")));
			this.toolTip1.SetToolTip(this.lblRelativePeriod, resources.GetString("lblRelativePeriod.ToolTip"));
			this.lblRelativePeriod.Visible = ((bool)(resources.GetObject("lblRelativePeriod.Visible")));
			this.lblRelativePeriod.Click += new System.EventHandler(this.label1_Click_1);
			// 
			// lblBefore
			// 
			this.lblBefore.AccessibleDescription = resources.GetString("lblBefore.AccessibleDescription");
			this.lblBefore.AccessibleName = resources.GetString("lblBefore.AccessibleName");
			this.lblBefore.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("lblBefore.Anchor")));
			this.lblBefore.AutoSize = ((bool)(resources.GetObject("lblBefore.AutoSize")));
			this.lblBefore.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("lblBefore.Dock")));
			this.lblBefore.Enabled = ((bool)(resources.GetObject("lblBefore.Enabled")));
			this.lblBefore.Font = ((System.Drawing.Font)(resources.GetObject("lblBefore.Font")));
			this.lblBefore.Image = ((System.Drawing.Image)(resources.GetObject("lblBefore.Image")));
			this.lblBefore.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("lblBefore.ImageAlign")));
			this.lblBefore.ImageIndex = ((int)(resources.GetObject("lblBefore.ImageIndex")));
			this.lblBefore.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("lblBefore.ImeMode")));
			this.lblBefore.Location = ((System.Drawing.Point)(resources.GetObject("lblBefore.Location")));
			this.lblBefore.Name = "lblBefore";
			this.lblBefore.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("lblBefore.RightToLeft")));
			this.lblBefore.Size = ((System.Drawing.Size)(resources.GetObject("lblBefore.Size")));
			this.lblBefore.TabIndex = ((int)(resources.GetObject("lblBefore.TabIndex")));
			this.lblBefore.Text = resources.GetString("lblBefore.Text");
			this.lblBefore.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("lblBefore.TextAlign")));
			this.toolTip1.SetToolTip(this.lblBefore, resources.GetString("lblBefore.ToolTip"));
			this.lblBefore.Visible = ((bool)(resources.GetObject("lblBefore.Visible")));
			// 
			// updAfter
			// 
			this.updAfter.AccessibleDescription = resources.GetString("updAfter.AccessibleDescription");
			this.updAfter.AccessibleName = resources.GetString("updAfter.AccessibleName");
			this.updAfter.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("updAfter.Anchor")));
			this.updAfter.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("updAfter.Dock")));
			this.updAfter.Enabled = ((bool)(resources.GetObject("updAfter.Enabled")));
			this.updAfter.Font = ((System.Drawing.Font)(resources.GetObject("updAfter.Font")));
			this.updAfter.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("updAfter.ImeMode")));
			this.updAfter.Location = ((System.Drawing.Point)(resources.GetObject("updAfter.Location")));
			this.updAfter.Name = "updAfter";
			this.updAfter.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("updAfter.RightToLeft")));
			this.updAfter.Size = ((System.Drawing.Size)(resources.GetObject("updAfter.Size")));
			this.updAfter.TabIndex = ((int)(resources.GetObject("updAfter.TabIndex")));
			this.updAfter.TextAlign = ((System.Windows.Forms.HorizontalAlignment)(resources.GetObject("updAfter.TextAlign")));
			this.updAfter.ThousandsSeparator = ((bool)(resources.GetObject("updAfter.ThousandsSeparator")));
			this.toolTip1.SetToolTip(this.updAfter, resources.GetString("updAfter.ToolTip"));
			this.updAfter.UpDownAlign = ((System.Windows.Forms.LeftRightAlignment)(resources.GetObject("updAfter.UpDownAlign")));
			this.updAfter.Visible = ((bool)(resources.GetObject("updAfter.Visible")));
			this.updAfter.ValueChanged += new System.EventHandler(this.updAfter_ValueChanged);
			this.updAfter.Leave += new System.EventHandler(this.updAfter_ValueChanged);
			// 
			// lblAfter
			// 
			this.lblAfter.AccessibleDescription = resources.GetString("lblAfter.AccessibleDescription");
			this.lblAfter.AccessibleName = resources.GetString("lblAfter.AccessibleName");
			this.lblAfter.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("lblAfter.Anchor")));
			this.lblAfter.AutoSize = ((bool)(resources.GetObject("lblAfter.AutoSize")));
			this.lblAfter.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("lblAfter.Dock")));
			this.lblAfter.Enabled = ((bool)(resources.GetObject("lblAfter.Enabled")));
			this.lblAfter.Font = ((System.Drawing.Font)(resources.GetObject("lblAfter.Font")));
			this.lblAfter.Image = ((System.Drawing.Image)(resources.GetObject("lblAfter.Image")));
			this.lblAfter.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("lblAfter.ImageAlign")));
			this.lblAfter.ImageIndex = ((int)(resources.GetObject("lblAfter.ImageIndex")));
			this.lblAfter.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("lblAfter.ImeMode")));
			this.lblAfter.Location = ((System.Drawing.Point)(resources.GetObject("lblAfter.Location")));
			this.lblAfter.Name = "lblAfter";
			this.lblAfter.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("lblAfter.RightToLeft")));
			this.lblAfter.Size = ((System.Drawing.Size)(resources.GetObject("lblAfter.Size")));
			this.lblAfter.TabIndex = ((int)(resources.GetObject("lblAfter.TabIndex")));
			this.lblAfter.Text = resources.GetString("lblAfter.Text");
			this.lblAfter.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("lblAfter.TextAlign")));
			this.toolTip1.SetToolTip(this.lblAfter, resources.GetString("lblAfter.ToolTip"));
			this.lblAfter.Visible = ((bool)(resources.GetObject("lblAfter.Visible")));
			// 
			// dtpChangesFrom
			// 
			this.dtpChangesFrom.AccessibleDescription = resources.GetString("dtpChangesFrom.AccessibleDescription");
			this.dtpChangesFrom.AccessibleName = resources.GetString("dtpChangesFrom.AccessibleName");
			this.dtpChangesFrom.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("dtpChangesFrom.Anchor")));
			this.dtpChangesFrom.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("dtpChangesFrom.BackgroundImage")));
			this.dtpChangesFrom.CalendarFont = ((System.Drawing.Font)(resources.GetObject("dtpChangesFrom.CalendarFont")));
			this.dtpChangesFrom.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("dtpChangesFrom.Dock")));
			this.dtpChangesFrom.DropDownAlign = ((System.Windows.Forms.LeftRightAlignment)(resources.GetObject("dtpChangesFrom.DropDownAlign")));
			this.dtpChangesFrom.Enabled = ((bool)(resources.GetObject("dtpChangesFrom.Enabled")));
			this.dtpChangesFrom.Font = ((System.Drawing.Font)(resources.GetObject("dtpChangesFrom.Font")));
			this.dtpChangesFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dtpChangesFrom.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("dtpChangesFrom.ImeMode")));
			this.dtpChangesFrom.Location = ((System.Drawing.Point)(resources.GetObject("dtpChangesFrom.Location")));
			this.dtpChangesFrom.Name = "dtpChangesFrom";
			this.dtpChangesFrom.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("dtpChangesFrom.RightToLeft")));
			this.dtpChangesFrom.Size = ((System.Drawing.Size)(resources.GetObject("dtpChangesFrom.Size")));
			this.dtpChangesFrom.TabIndex = ((int)(resources.GetObject("dtpChangesFrom.TabIndex")));
			this.toolTip1.SetToolTip(this.dtpChangesFrom, resources.GetString("dtpChangesFrom.ToolTip"));
			this.dtpChangesFrom.Value = new System.DateTime(2005, 11, 17, 8, 59, 46, 239);
			this.dtpChangesFrom.Visible = ((bool)(resources.GetObject("dtpChangesFrom.Visible")));
			this.dtpChangesFrom.CloseUp += new System.EventHandler(this.dtpChangesFrom_CloseUp);
			this.dtpChangesFrom.ValueChanged += new System.EventHandler(this.dtpChangesFrom_ValueChanged);
			// 
			// lblChangesFrom
			// 
			this.lblChangesFrom.AccessibleDescription = resources.GetString("lblChangesFrom.AccessibleDescription");
			this.lblChangesFrom.AccessibleName = resources.GetString("lblChangesFrom.AccessibleName");
			this.lblChangesFrom.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("lblChangesFrom.Anchor")));
			this.lblChangesFrom.AutoSize = ((bool)(resources.GetObject("lblChangesFrom.AutoSize")));
			this.lblChangesFrom.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("lblChangesFrom.Dock")));
			this.lblChangesFrom.Enabled = ((bool)(resources.GetObject("lblChangesFrom.Enabled")));
			this.lblChangesFrom.Font = ((System.Drawing.Font)(resources.GetObject("lblChangesFrom.Font")));
			this.lblChangesFrom.Image = ((System.Drawing.Image)(resources.GetObject("lblChangesFrom.Image")));
			this.lblChangesFrom.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("lblChangesFrom.ImageAlign")));
			this.lblChangesFrom.ImageIndex = ((int)(resources.GetObject("lblChangesFrom.ImageIndex")));
			this.lblChangesFrom.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("lblChangesFrom.ImeMode")));
			this.lblChangesFrom.Location = ((System.Drawing.Point)(resources.GetObject("lblChangesFrom.Location")));
			this.lblChangesFrom.Name = "lblChangesFrom";
			this.lblChangesFrom.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("lblChangesFrom.RightToLeft")));
			this.lblChangesFrom.Size = ((System.Drawing.Size)(resources.GetObject("lblChangesFrom.Size")));
			this.lblChangesFrom.TabIndex = ((int)(resources.GetObject("lblChangesFrom.TabIndex")));
			this.lblChangesFrom.Text = resources.GetString("lblChangesFrom.Text");
			this.lblChangesFrom.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("lblChangesFrom.TextAlign")));
			this.toolTip1.SetToolTip(this.lblChangesFrom, resources.GetString("lblChangesFrom.ToolTip"));
			this.lblChangesFrom.Visible = ((bool)(resources.GetObject("lblChangesFrom.Visible")));
			// 
			// dtpFlicoFrom
			// 
			this.dtpFlicoFrom.AccessibleDescription = resources.GetString("dtpFlicoFrom.AccessibleDescription");
			this.dtpFlicoFrom.AccessibleName = resources.GetString("dtpFlicoFrom.AccessibleName");
			this.dtpFlicoFrom.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("dtpFlicoFrom.Anchor")));
			this.dtpFlicoFrom.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("dtpFlicoFrom.BackgroundImage")));
			this.dtpFlicoFrom.CalendarFont = ((System.Drawing.Font)(resources.GetObject("dtpFlicoFrom.CalendarFont")));
			this.dtpFlicoFrom.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("dtpFlicoFrom.Dock")));
			this.dtpFlicoFrom.DropDownAlign = ((System.Windows.Forms.LeftRightAlignment)(resources.GetObject("dtpFlicoFrom.DropDownAlign")));
			this.dtpFlicoFrom.Enabled = ((bool)(resources.GetObject("dtpFlicoFrom.Enabled")));
			this.dtpFlicoFrom.Font = ((System.Drawing.Font)(resources.GetObject("dtpFlicoFrom.Font")));
			this.dtpFlicoFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dtpFlicoFrom.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("dtpFlicoFrom.ImeMode")));
			this.dtpFlicoFrom.Location = ((System.Drawing.Point)(resources.GetObject("dtpFlicoFrom.Location")));
			this.dtpFlicoFrom.Name = "dtpFlicoFrom";
			this.dtpFlicoFrom.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("dtpFlicoFrom.RightToLeft")));
			this.dtpFlicoFrom.Size = ((System.Drawing.Size)(resources.GetObject("dtpFlicoFrom.Size")));
			this.dtpFlicoFrom.TabIndex = ((int)(resources.GetObject("dtpFlicoFrom.TabIndex")));
			this.toolTip1.SetToolTip(this.dtpFlicoFrom, resources.GetString("dtpFlicoFrom.ToolTip"));
			this.dtpFlicoFrom.Value = new System.DateTime(2005, 11, 17, 8, 59, 46, 254);
			this.dtpFlicoFrom.Visible = ((bool)(resources.GetObject("dtpFlicoFrom.Visible")));
			this.dtpFlicoFrom.CloseUp += new System.EventHandler(this.dtpFlicoFrom_CloseUp);
			this.dtpFlicoFrom.ValueChanged += new System.EventHandler(this.dtpFlicoFrom_ValueChanged);
			// 
			// lblFlicoFrom
			// 
			this.lblFlicoFrom.AccessibleDescription = resources.GetString("lblFlicoFrom.AccessibleDescription");
			this.lblFlicoFrom.AccessibleName = resources.GetString("lblFlicoFrom.AccessibleName");
			this.lblFlicoFrom.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("lblFlicoFrom.Anchor")));
			this.lblFlicoFrom.AutoSize = ((bool)(resources.GetObject("lblFlicoFrom.AutoSize")));
			this.lblFlicoFrom.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("lblFlicoFrom.Dock")));
			this.lblFlicoFrom.Enabled = ((bool)(resources.GetObject("lblFlicoFrom.Enabled")));
			this.lblFlicoFrom.Font = ((System.Drawing.Font)(resources.GetObject("lblFlicoFrom.Font")));
			this.lblFlicoFrom.Image = ((System.Drawing.Image)(resources.GetObject("lblFlicoFrom.Image")));
			this.lblFlicoFrom.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("lblFlicoFrom.ImageAlign")));
			this.lblFlicoFrom.ImageIndex = ((int)(resources.GetObject("lblFlicoFrom.ImageIndex")));
			this.lblFlicoFrom.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("lblFlicoFrom.ImeMode")));
			this.lblFlicoFrom.Location = ((System.Drawing.Point)(resources.GetObject("lblFlicoFrom.Location")));
			this.lblFlicoFrom.Name = "lblFlicoFrom";
			this.lblFlicoFrom.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("lblFlicoFrom.RightToLeft")));
			this.lblFlicoFrom.Size = ((System.Drawing.Size)(resources.GetObject("lblFlicoFrom.Size")));
			this.lblFlicoFrom.TabIndex = ((int)(resources.GetObject("lblFlicoFrom.TabIndex")));
			this.lblFlicoFrom.Text = resources.GetString("lblFlicoFrom.Text");
			this.lblFlicoFrom.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("lblFlicoFrom.TextAlign")));
			this.toolTip1.SetToolTip(this.lblFlicoFrom, resources.GetString("lblFlicoFrom.ToolTip"));
			this.lblFlicoFrom.Visible = ((bool)(resources.GetObject("lblFlicoFrom.Visible")));
			// 
			// lblFlightNumber
			// 
			this.lblFlightNumber.AccessibleDescription = resources.GetString("lblFlightNumber.AccessibleDescription");
			this.lblFlightNumber.AccessibleName = resources.GetString("lblFlightNumber.AccessibleName");
			this.lblFlightNumber.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("lblFlightNumber.Anchor")));
			this.lblFlightNumber.AutoSize = ((bool)(resources.GetObject("lblFlightNumber.AutoSize")));
			this.lblFlightNumber.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("lblFlightNumber.Dock")));
			this.lblFlightNumber.Enabled = ((bool)(resources.GetObject("lblFlightNumber.Enabled")));
			this.lblFlightNumber.Font = ((System.Drawing.Font)(resources.GetObject("lblFlightNumber.Font")));
			this.lblFlightNumber.Image = ((System.Drawing.Image)(resources.GetObject("lblFlightNumber.Image")));
			this.lblFlightNumber.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("lblFlightNumber.ImageAlign")));
			this.lblFlightNumber.ImageIndex = ((int)(resources.GetObject("lblFlightNumber.ImageIndex")));
			this.lblFlightNumber.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("lblFlightNumber.ImeMode")));
			this.lblFlightNumber.Location = ((System.Drawing.Point)(resources.GetObject("lblFlightNumber.Location")));
			this.lblFlightNumber.Name = "lblFlightNumber";
			this.lblFlightNumber.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("lblFlightNumber.RightToLeft")));
			this.lblFlightNumber.Size = ((System.Drawing.Size)(resources.GetObject("lblFlightNumber.Size")));
			this.lblFlightNumber.TabIndex = ((int)(resources.GetObject("lblFlightNumber.TabIndex")));
			this.lblFlightNumber.Text = resources.GetString("lblFlightNumber.Text");
			this.lblFlightNumber.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("lblFlightNumber.TextAlign")));
			this.toolTip1.SetToolTip(this.lblFlightNumber, resources.GetString("lblFlightNumber.ToolTip"));
			this.lblFlightNumber.Visible = ((bool)(resources.GetObject("lblFlightNumber.Visible")));
			// 
			// txtFlightNumber
			// 
			this.txtFlightNumber.AccessibleDescription = resources.GetString("txtFlightNumber.AccessibleDescription");
			this.txtFlightNumber.AccessibleName = resources.GetString("txtFlightNumber.AccessibleName");
			this.txtFlightNumber.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("txtFlightNumber.Anchor")));
			this.txtFlightNumber.AutoSize = ((bool)(resources.GetObject("txtFlightNumber.AutoSize")));
			this.txtFlightNumber.BackColor = System.Drawing.SystemColors.Window;
			this.txtFlightNumber.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("txtFlightNumber.BackgroundImage")));
			this.txtFlightNumber.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("txtFlightNumber.Dock")));
			this.txtFlightNumber.Enabled = ((bool)(resources.GetObject("txtFlightNumber.Enabled")));
			this.txtFlightNumber.Font = ((System.Drawing.Font)(resources.GetObject("txtFlightNumber.Font")));
			this.txtFlightNumber.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("txtFlightNumber.ImeMode")));
			this.txtFlightNumber.InvalidTextBackColor = System.Drawing.Color.LightPink;
			this.txtFlightNumber.Location = ((System.Drawing.Point)(resources.GetObject("txtFlightNumber.Location")));
			this.txtFlightNumber.MaxLength = ((int)(resources.GetObject("txtFlightNumber.MaxLength")));
			this.txtFlightNumber.Multiline = ((bool)(resources.GetObject("txtFlightNumber.Multiline")));
			this.txtFlightNumber.Name = "txtFlightNumber";
			this.txtFlightNumber.PasswordChar = ((char)(resources.GetObject("txtFlightNumber.PasswordChar")));
			this.txtFlightNumber.Pattern = UserControls.TextBoxRegex.Patterns.RegexPattern;
			this.txtFlightNumber.PatternString = "[0-9]{1,5}";
			this.txtFlightNumber.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("txtFlightNumber.RightToLeft")));
			this.txtFlightNumber.ScrollBars = ((System.Windows.Forms.ScrollBars)(resources.GetObject("txtFlightNumber.ScrollBars")));
			this.txtFlightNumber.Size = ((System.Drawing.Size)(resources.GetObject("txtFlightNumber.Size")));
			this.txtFlightNumber.TabIndex = ((int)(resources.GetObject("txtFlightNumber.TabIndex")));
			this.txtFlightNumber.Text = resources.GetString("txtFlightNumber.Text");
			this.txtFlightNumber.TextAlign = ((System.Windows.Forms.HorizontalAlignment)(resources.GetObject("txtFlightNumber.TextAlign")));
			this.toolTip1.SetToolTip(this.txtFlightNumber, resources.GetString("txtFlightNumber.ToolTip"));
			this.txtFlightNumber.UseColors = true;
			this.txtFlightNumber.UseInvalidTextException = true;
			this.txtFlightNumber.UseNumbersOnly = true;
			this.txtFlightNumber.ValidTextBackColor = System.Drawing.Color.LightGreen;
			this.txtFlightNumber.Visible = ((bool)(resources.GetObject("txtFlightNumber.Visible")));
			this.txtFlightNumber.WordWrap = ((bool)(resources.GetObject("txtFlightNumber.WordWrap")));
			this.txtFlightNumber.TextChanged += new System.EventHandler(this.txtFlightNumber_TextChanged);
			// 
			// cbCodeShare
			// 
			this.cbCodeShare.AccessibleDescription = resources.GetString("cbCodeShare.AccessibleDescription");
			this.cbCodeShare.AccessibleName = resources.GetString("cbCodeShare.AccessibleName");
			this.cbCodeShare.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("cbCodeShare.Anchor")));
			this.cbCodeShare.Appearance = ((System.Windows.Forms.Appearance)(resources.GetObject("cbCodeShare.Appearance")));
			this.cbCodeShare.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("cbCodeShare.BackgroundImage")));
			this.cbCodeShare.CheckAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("cbCodeShare.CheckAlign")));
			this.cbCodeShare.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("cbCodeShare.Dock")));
			this.cbCodeShare.Enabled = ((bool)(resources.GetObject("cbCodeShare.Enabled")));
			this.cbCodeShare.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("cbCodeShare.FlatStyle")));
			this.cbCodeShare.Font = ((System.Drawing.Font)(resources.GetObject("cbCodeShare.Font")));
			this.cbCodeShare.Image = ((System.Drawing.Image)(resources.GetObject("cbCodeShare.Image")));
			this.cbCodeShare.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("cbCodeShare.ImageAlign")));
			this.cbCodeShare.ImageIndex = ((int)(resources.GetObject("cbCodeShare.ImageIndex")));
			this.cbCodeShare.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("cbCodeShare.ImeMode")));
			this.cbCodeShare.Location = ((System.Drawing.Point)(resources.GetObject("cbCodeShare.Location")));
			this.cbCodeShare.Name = "cbCodeShare";
			this.cbCodeShare.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("cbCodeShare.RightToLeft")));
			this.cbCodeShare.Size = ((System.Drawing.Size)(resources.GetObject("cbCodeShare.Size")));
			this.cbCodeShare.TabIndex = ((int)(resources.GetObject("cbCodeShare.TabIndex")));
			this.cbCodeShare.Text = resources.GetString("cbCodeShare.Text");
			this.cbCodeShare.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("cbCodeShare.TextAlign")));
			this.toolTip1.SetToolTip(this.cbCodeShare, resources.GetString("cbCodeShare.ToolTip"));
			this.cbCodeShare.Visible = ((bool)(resources.GetObject("cbCodeShare.Visible")));
			this.cbCodeShare.CheckedChanged += new System.EventHandler(this.cbCodeShare_CheckedChanged);
			// 
			// txtCallsign
			// 
			this.txtCallsign.AccessibleDescription = resources.GetString("txtCallsign.AccessibleDescription");
			this.txtCallsign.AccessibleName = resources.GetString("txtCallsign.AccessibleName");
			this.txtCallsign.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("txtCallsign.Anchor")));
			this.txtCallsign.AutoSize = ((bool)(resources.GetObject("txtCallsign.AutoSize")));
			this.txtCallsign.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("txtCallsign.BackgroundImage")));
			this.txtCallsign.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
			this.txtCallsign.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("txtCallsign.Dock")));
			this.txtCallsign.Enabled = ((bool)(resources.GetObject("txtCallsign.Enabled")));
			this.txtCallsign.Font = ((System.Drawing.Font)(resources.GetObject("txtCallsign.Font")));
			this.txtCallsign.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("txtCallsign.ImeMode")));
			this.txtCallsign.Location = ((System.Drawing.Point)(resources.GetObject("txtCallsign.Location")));
			this.txtCallsign.MaxLength = ((int)(resources.GetObject("txtCallsign.MaxLength")));
			this.txtCallsign.Multiline = ((bool)(resources.GetObject("txtCallsign.Multiline")));
			this.txtCallsign.Name = "txtCallsign";
			this.txtCallsign.PasswordChar = ((char)(resources.GetObject("txtCallsign.PasswordChar")));
			this.txtCallsign.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("txtCallsign.RightToLeft")));
			this.txtCallsign.ScrollBars = ((System.Windows.Forms.ScrollBars)(resources.GetObject("txtCallsign.ScrollBars")));
			this.txtCallsign.Size = ((System.Drawing.Size)(resources.GetObject("txtCallsign.Size")));
			this.txtCallsign.TabIndex = ((int)(resources.GetObject("txtCallsign.TabIndex")));
			this.txtCallsign.Text = resources.GetString("txtCallsign.Text");
			this.txtCallsign.TextAlign = ((System.Windows.Forms.HorizontalAlignment)(resources.GetObject("txtCallsign.TextAlign")));
			this.toolTip1.SetToolTip(this.txtCallsign, resources.GetString("txtCallsign.ToolTip"));
			this.txtCallsign.Visible = ((bool)(resources.GetObject("txtCallsign.Visible")));
			this.txtCallsign.WordWrap = ((bool)(resources.GetObject("txtCallsign.WordWrap")));
			this.txtCallsign.TextChanged += new System.EventHandler(this.txtCallsign_TextChanged);
			// 
			// lblCallsign
			// 
			this.lblCallsign.AccessibleDescription = resources.GetString("lblCallsign.AccessibleDescription");
			this.lblCallsign.AccessibleName = resources.GetString("lblCallsign.AccessibleName");
			this.lblCallsign.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("lblCallsign.Anchor")));
			this.lblCallsign.AutoSize = ((bool)(resources.GetObject("lblCallsign.AutoSize")));
			this.lblCallsign.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("lblCallsign.Dock")));
			this.lblCallsign.Enabled = ((bool)(resources.GetObject("lblCallsign.Enabled")));
			this.lblCallsign.Font = ((System.Drawing.Font)(resources.GetObject("lblCallsign.Font")));
			this.lblCallsign.Image = ((System.Drawing.Image)(resources.GetObject("lblCallsign.Image")));
			this.lblCallsign.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("lblCallsign.ImageAlign")));
			this.lblCallsign.ImageIndex = ((int)(resources.GetObject("lblCallsign.ImageIndex")));
			this.lblCallsign.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("lblCallsign.ImeMode")));
			this.lblCallsign.Location = ((System.Drawing.Point)(resources.GetObject("lblCallsign.Location")));
			this.lblCallsign.Name = "lblCallsign";
			this.lblCallsign.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("lblCallsign.RightToLeft")));
			this.lblCallsign.Size = ((System.Drawing.Size)(resources.GetObject("lblCallsign.Size")));
			this.lblCallsign.TabIndex = ((int)(resources.GetObject("lblCallsign.TabIndex")));
			this.lblCallsign.Text = resources.GetString("lblCallsign.Text");
			this.lblCallsign.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("lblCallsign.TextAlign")));
			this.toolTip1.SetToolTip(this.lblCallsign, resources.GetString("lblCallsign.ToolTip"));
			this.lblCallsign.Visible = ((bool)(resources.GetObject("lblCallsign.Visible")));
			// 
			// txtFlightId
			// 
			this.txtFlightId.AccessibleDescription = resources.GetString("txtFlightId.AccessibleDescription");
			this.txtFlightId.AccessibleName = resources.GetString("txtFlightId.AccessibleName");
			this.txtFlightId.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("txtFlightId.Anchor")));
			this.txtFlightId.AutoSize = ((bool)(resources.GetObject("txtFlightId.AutoSize")));
			this.txtFlightId.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("txtFlightId.BackgroundImage")));
			this.txtFlightId.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("txtFlightId.Dock")));
			this.txtFlightId.Enabled = ((bool)(resources.GetObject("txtFlightId.Enabled")));
			this.txtFlightId.Font = ((System.Drawing.Font)(resources.GetObject("txtFlightId.Font")));
			this.txtFlightId.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("txtFlightId.ImeMode")));
			this.txtFlightId.Location = ((System.Drawing.Point)(resources.GetObject("txtFlightId.Location")));
			this.txtFlightId.MaxLength = ((int)(resources.GetObject("txtFlightId.MaxLength")));
			this.txtFlightId.Multiline = ((bool)(resources.GetObject("txtFlightId.Multiline")));
			this.txtFlightId.Name = "txtFlightId";
			this.txtFlightId.PasswordChar = ((char)(resources.GetObject("txtFlightId.PasswordChar")));
			this.txtFlightId.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("txtFlightId.RightToLeft")));
			this.txtFlightId.ScrollBars = ((System.Windows.Forms.ScrollBars)(resources.GetObject("txtFlightId.ScrollBars")));
			this.txtFlightId.Size = ((System.Drawing.Size)(resources.GetObject("txtFlightId.Size")));
			this.txtFlightId.TabIndex = ((int)(resources.GetObject("txtFlightId.TabIndex")));
			this.txtFlightId.Text = resources.GetString("txtFlightId.Text");
			this.txtFlightId.TextAlign = ((System.Windows.Forms.HorizontalAlignment)(resources.GetObject("txtFlightId.TextAlign")));
			this.toolTip1.SetToolTip(this.txtFlightId, resources.GetString("txtFlightId.ToolTip"));
			this.txtFlightId.Visible = ((bool)(resources.GetObject("txtFlightId.Visible")));
			this.txtFlightId.WordWrap = ((bool)(resources.GetObject("txtFlightId.WordWrap")));
			this.txtFlightId.TextChanged += new System.EventHandler(this.txtFlightId_TextChanged);
			// 
			// lblFlightId
			// 
			this.lblFlightId.AccessibleDescription = resources.GetString("lblFlightId.AccessibleDescription");
			this.lblFlightId.AccessibleName = resources.GetString("lblFlightId.AccessibleName");
			this.lblFlightId.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("lblFlightId.Anchor")));
			this.lblFlightId.AutoSize = ((bool)(resources.GetObject("lblFlightId.AutoSize")));
			this.lblFlightId.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("lblFlightId.Dock")));
			this.lblFlightId.Enabled = ((bool)(resources.GetObject("lblFlightId.Enabled")));
			this.lblFlightId.Font = ((System.Drawing.Font)(resources.GetObject("lblFlightId.Font")));
			this.lblFlightId.Image = ((System.Drawing.Image)(resources.GetObject("lblFlightId.Image")));
			this.lblFlightId.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("lblFlightId.ImageAlign")));
			this.lblFlightId.ImageIndex = ((int)(resources.GetObject("lblFlightId.ImageIndex")));
			this.lblFlightId.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("lblFlightId.ImeMode")));
			this.lblFlightId.Location = ((System.Drawing.Point)(resources.GetObject("lblFlightId.Location")));
			this.lblFlightId.Name = "lblFlightId";
			this.lblFlightId.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("lblFlightId.RightToLeft")));
			this.lblFlightId.Size = ((System.Drawing.Size)(resources.GetObject("lblFlightId.Size")));
			this.lblFlightId.TabIndex = ((int)(resources.GetObject("lblFlightId.TabIndex")));
			this.lblFlightId.Text = resources.GetString("lblFlightId.Text");
			this.lblFlightId.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("lblFlightId.TextAlign")));
			this.toolTip1.SetToolTip(this.lblFlightId, resources.GetString("lblFlightId.ToolTip"));
			this.lblFlightId.Visible = ((bool)(resources.GetObject("lblFlightId.Visible")));
			// 
			// lblAircraftType
			// 
			this.lblAircraftType.AccessibleDescription = resources.GetString("lblAircraftType.AccessibleDescription");
			this.lblAircraftType.AccessibleName = resources.GetString("lblAircraftType.AccessibleName");
			this.lblAircraftType.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("lblAircraftType.Anchor")));
			this.lblAircraftType.AutoSize = ((bool)(resources.GetObject("lblAircraftType.AutoSize")));
			this.lblAircraftType.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("lblAircraftType.Dock")));
			this.lblAircraftType.Enabled = ((bool)(resources.GetObject("lblAircraftType.Enabled")));
			this.lblAircraftType.Font = ((System.Drawing.Font)(resources.GetObject("lblAircraftType.Font")));
			this.lblAircraftType.Image = ((System.Drawing.Image)(resources.GetObject("lblAircraftType.Image")));
			this.lblAircraftType.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("lblAircraftType.ImageAlign")));
			this.lblAircraftType.ImageIndex = ((int)(resources.GetObject("lblAircraftType.ImageIndex")));
			this.lblAircraftType.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("lblAircraftType.ImeMode")));
			this.lblAircraftType.Location = ((System.Drawing.Point)(resources.GetObject("lblAircraftType.Location")));
			this.lblAircraftType.Name = "lblAircraftType";
			this.lblAircraftType.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("lblAircraftType.RightToLeft")));
			this.lblAircraftType.Size = ((System.Drawing.Size)(resources.GetObject("lblAircraftType.Size")));
			this.lblAircraftType.TabIndex = ((int)(resources.GetObject("lblAircraftType.TabIndex")));
			this.lblAircraftType.Text = resources.GetString("lblAircraftType.Text");
			this.lblAircraftType.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("lblAircraftType.TextAlign")));
			this.toolTip1.SetToolTip(this.lblAircraftType, resources.GetString("lblAircraftType.ToolTip"));
			this.lblAircraftType.Visible = ((bool)(resources.GetObject("lblAircraftType.Visible")));
			// 
			// txtRegistration
			// 
			this.txtRegistration.AccessibleDescription = resources.GetString("txtRegistration.AccessibleDescription");
			this.txtRegistration.AccessibleName = resources.GetString("txtRegistration.AccessibleName");
			this.txtRegistration.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("txtRegistration.Anchor")));
			this.txtRegistration.AutoSize = ((bool)(resources.GetObject("txtRegistration.AutoSize")));
			this.txtRegistration.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("txtRegistration.BackgroundImage")));
			this.txtRegistration.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
			this.txtRegistration.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("txtRegistration.Dock")));
			this.txtRegistration.Enabled = ((bool)(resources.GetObject("txtRegistration.Enabled")));
			this.txtRegistration.Font = ((System.Drawing.Font)(resources.GetObject("txtRegistration.Font")));
			this.txtRegistration.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("txtRegistration.ImeMode")));
			this.txtRegistration.Location = ((System.Drawing.Point)(resources.GetObject("txtRegistration.Location")));
			this.txtRegistration.MaxLength = ((int)(resources.GetObject("txtRegistration.MaxLength")));
			this.txtRegistration.Multiline = ((bool)(resources.GetObject("txtRegistration.Multiline")));
			this.txtRegistration.Name = "txtRegistration";
			this.txtRegistration.PasswordChar = ((char)(resources.GetObject("txtRegistration.PasswordChar")));
			this.txtRegistration.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("txtRegistration.RightToLeft")));
			this.txtRegistration.ScrollBars = ((System.Windows.Forms.ScrollBars)(resources.GetObject("txtRegistration.ScrollBars")));
			this.txtRegistration.Size = ((System.Drawing.Size)(resources.GetObject("txtRegistration.Size")));
			this.txtRegistration.TabIndex = ((int)(resources.GetObject("txtRegistration.TabIndex")));
			this.txtRegistration.Text = resources.GetString("txtRegistration.Text");
			this.txtRegistration.TextAlign = ((System.Windows.Forms.HorizontalAlignment)(resources.GetObject("txtRegistration.TextAlign")));
			this.toolTip1.SetToolTip(this.txtRegistration, resources.GetString("txtRegistration.ToolTip"));
			this.txtRegistration.Visible = ((bool)(resources.GetObject("txtRegistration.Visible")));
			this.txtRegistration.WordWrap = ((bool)(resources.GetObject("txtRegistration.WordWrap")));
			this.txtRegistration.TextChanged += new System.EventHandler(this.txtRegistration_TextChanged);
			// 
			// lblRegistration
			// 
			this.lblRegistration.AccessibleDescription = resources.GetString("lblRegistration.AccessibleDescription");
			this.lblRegistration.AccessibleName = resources.GetString("lblRegistration.AccessibleName");
			this.lblRegistration.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("lblRegistration.Anchor")));
			this.lblRegistration.AutoSize = ((bool)(resources.GetObject("lblRegistration.AutoSize")));
			this.lblRegistration.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("lblRegistration.Dock")));
			this.lblRegistration.Enabled = ((bool)(resources.GetObject("lblRegistration.Enabled")));
			this.lblRegistration.Font = ((System.Drawing.Font)(resources.GetObject("lblRegistration.Font")));
			this.lblRegistration.Image = ((System.Drawing.Image)(resources.GetObject("lblRegistration.Image")));
			this.lblRegistration.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("lblRegistration.ImageAlign")));
			this.lblRegistration.ImageIndex = ((int)(resources.GetObject("lblRegistration.ImageIndex")));
			this.lblRegistration.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("lblRegistration.ImeMode")));
			this.lblRegistration.Location = ((System.Drawing.Point)(resources.GetObject("lblRegistration.Location")));
			this.lblRegistration.Name = "lblRegistration";
			this.lblRegistration.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("lblRegistration.RightToLeft")));
			this.lblRegistration.Size = ((System.Drawing.Size)(resources.GetObject("lblRegistration.Size")));
			this.lblRegistration.TabIndex = ((int)(resources.GetObject("lblRegistration.TabIndex")));
			this.lblRegistration.Text = resources.GetString("lblRegistration.Text");
			this.lblRegistration.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("lblRegistration.TextAlign")));
			this.toolTip1.SetToolTip(this.lblRegistration, resources.GetString("lblRegistration.ToolTip"));
			this.lblRegistration.Visible = ((bool)(resources.GetObject("lblRegistration.Visible")));
			// 
			// lblVia
			// 
			this.lblVia.AccessibleDescription = resources.GetString("lblVia.AccessibleDescription");
			this.lblVia.AccessibleName = resources.GetString("lblVia.AccessibleName");
			this.lblVia.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("lblVia.Anchor")));
			this.lblVia.AutoSize = ((bool)(resources.GetObject("lblVia.AutoSize")));
			this.lblVia.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("lblVia.Dock")));
			this.lblVia.Enabled = ((bool)(resources.GetObject("lblVia.Enabled")));
			this.lblVia.Font = ((System.Drawing.Font)(resources.GetObject("lblVia.Font")));
			this.lblVia.Image = ((System.Drawing.Image)(resources.GetObject("lblVia.Image")));
			this.lblVia.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("lblVia.ImageAlign")));
			this.lblVia.ImageIndex = ((int)(resources.GetObject("lblVia.ImageIndex")));
			this.lblVia.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("lblVia.ImeMode")));
			this.lblVia.Location = ((System.Drawing.Point)(resources.GetObject("lblVia.Location")));
			this.lblVia.Name = "lblVia";
			this.lblVia.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("lblVia.RightToLeft")));
			this.lblVia.Size = ((System.Drawing.Size)(resources.GetObject("lblVia.Size")));
			this.lblVia.TabIndex = ((int)(resources.GetObject("lblVia.TabIndex")));
			this.lblVia.Text = resources.GetString("lblVia.Text");
			this.lblVia.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("lblVia.TextAlign")));
			this.toolTip1.SetToolTip(this.lblVia, resources.GetString("lblVia.ToolTip"));
			this.lblVia.Visible = ((bool)(resources.GetObject("lblVia.Visible")));
			// 
			// lblOrigin
			// 
			this.lblOrigin.AccessibleDescription = resources.GetString("lblOrigin.AccessibleDescription");
			this.lblOrigin.AccessibleName = resources.GetString("lblOrigin.AccessibleName");
			this.lblOrigin.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("lblOrigin.Anchor")));
			this.lblOrigin.AutoSize = ((bool)(resources.GetObject("lblOrigin.AutoSize")));
			this.lblOrigin.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("lblOrigin.Dock")));
			this.lblOrigin.Enabled = ((bool)(resources.GetObject("lblOrigin.Enabled")));
			this.lblOrigin.Font = ((System.Drawing.Font)(resources.GetObject("lblOrigin.Font")));
			this.lblOrigin.Image = ((System.Drawing.Image)(resources.GetObject("lblOrigin.Image")));
			this.lblOrigin.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("lblOrigin.ImageAlign")));
			this.lblOrigin.ImageIndex = ((int)(resources.GetObject("lblOrigin.ImageIndex")));
			this.lblOrigin.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("lblOrigin.ImeMode")));
			this.lblOrigin.Location = ((System.Drawing.Point)(resources.GetObject("lblOrigin.Location")));
			this.lblOrigin.Name = "lblOrigin";
			this.lblOrigin.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("lblOrigin.RightToLeft")));
			this.lblOrigin.Size = ((System.Drawing.Size)(resources.GetObject("lblOrigin.Size")));
			this.lblOrigin.TabIndex = ((int)(resources.GetObject("lblOrigin.TabIndex")));
			this.lblOrigin.Text = resources.GetString("lblOrigin.Text");
			this.lblOrigin.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("lblOrigin.TextAlign")));
			this.toolTip1.SetToolTip(this.lblOrigin, resources.GetString("lblOrigin.ToolTip"));
			this.lblOrigin.Visible = ((bool)(resources.GetObject("lblOrigin.Visible")));
			// 
			// txtStatisticId
			// 
			this.txtStatisticId.AccessibleDescription = resources.GetString("txtStatisticId.AccessibleDescription");
			this.txtStatisticId.AccessibleName = resources.GetString("txtStatisticId.AccessibleName");
			this.txtStatisticId.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("txtStatisticId.Anchor")));
			this.txtStatisticId.AutoSize = ((bool)(resources.GetObject("txtStatisticId.AutoSize")));
			this.txtStatisticId.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("txtStatisticId.BackgroundImage")));
			this.txtStatisticId.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("txtStatisticId.Dock")));
			this.txtStatisticId.Enabled = ((bool)(resources.GetObject("txtStatisticId.Enabled")));
			this.txtStatisticId.Font = ((System.Drawing.Font)(resources.GetObject("txtStatisticId.Font")));
			this.txtStatisticId.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("txtStatisticId.ImeMode")));
			this.txtStatisticId.Location = ((System.Drawing.Point)(resources.GetObject("txtStatisticId.Location")));
			this.txtStatisticId.MaxLength = ((int)(resources.GetObject("txtStatisticId.MaxLength")));
			this.txtStatisticId.Multiline = ((bool)(resources.GetObject("txtStatisticId.Multiline")));
			this.txtStatisticId.Name = "txtStatisticId";
			this.txtStatisticId.PasswordChar = ((char)(resources.GetObject("txtStatisticId.PasswordChar")));
			this.txtStatisticId.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("txtStatisticId.RightToLeft")));
			this.txtStatisticId.ScrollBars = ((System.Windows.Forms.ScrollBars)(resources.GetObject("txtStatisticId.ScrollBars")));
			this.txtStatisticId.Size = ((System.Drawing.Size)(resources.GetObject("txtStatisticId.Size")));
			this.txtStatisticId.TabIndex = ((int)(resources.GetObject("txtStatisticId.TabIndex")));
			this.txtStatisticId.Text = resources.GetString("txtStatisticId.Text");
			this.txtStatisticId.TextAlign = ((System.Windows.Forms.HorizontalAlignment)(resources.GetObject("txtStatisticId.TextAlign")));
			this.toolTip1.SetToolTip(this.txtStatisticId, resources.GetString("txtStatisticId.ToolTip"));
			this.txtStatisticId.Visible = ((bool)(resources.GetObject("txtStatisticId.Visible")));
			this.txtStatisticId.WordWrap = ((bool)(resources.GetObject("txtStatisticId.WordWrap")));
			this.txtStatisticId.TextChanged += new System.EventHandler(this.textBox5_TextChanged);
			// 
			// lblKeycode
			// 
			this.lblKeycode.AccessibleDescription = resources.GetString("lblKeycode.AccessibleDescription");
			this.lblKeycode.AccessibleName = resources.GetString("lblKeycode.AccessibleName");
			this.lblKeycode.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("lblKeycode.Anchor")));
			this.lblKeycode.AutoSize = ((bool)(resources.GetObject("lblKeycode.AutoSize")));
			this.lblKeycode.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("lblKeycode.Dock")));
			this.lblKeycode.Enabled = ((bool)(resources.GetObject("lblKeycode.Enabled")));
			this.lblKeycode.Font = ((System.Drawing.Font)(resources.GetObject("lblKeycode.Font")));
			this.lblKeycode.Image = ((System.Drawing.Image)(resources.GetObject("lblKeycode.Image")));
			this.lblKeycode.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("lblKeycode.ImageAlign")));
			this.lblKeycode.ImageIndex = ((int)(resources.GetObject("lblKeycode.ImageIndex")));
			this.lblKeycode.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("lblKeycode.ImeMode")));
			this.lblKeycode.Location = ((System.Drawing.Point)(resources.GetObject("lblKeycode.Location")));
			this.lblKeycode.Name = "lblKeycode";
			this.lblKeycode.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("lblKeycode.RightToLeft")));
			this.lblKeycode.Size = ((System.Drawing.Size)(resources.GetObject("lblKeycode.Size")));
			this.lblKeycode.TabIndex = ((int)(resources.GetObject("lblKeycode.TabIndex")));
			this.lblKeycode.Text = resources.GetString("lblKeycode.Text");
			this.lblKeycode.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("lblKeycode.TextAlign")));
			this.toolTip1.SetToolTip(this.lblKeycode, resources.GetString("lblKeycode.ToolTip"));
			this.lblKeycode.Visible = ((bool)(resources.GetObject("lblKeycode.Visible")));
			// 
			// lblNature
			// 
			this.lblNature.AccessibleDescription = resources.GetString("lblNature.AccessibleDescription");
			this.lblNature.AccessibleName = resources.GetString("lblNature.AccessibleName");
			this.lblNature.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("lblNature.Anchor")));
			this.lblNature.AutoSize = ((bool)(resources.GetObject("lblNature.AutoSize")));
			this.lblNature.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("lblNature.Dock")));
			this.lblNature.Enabled = ((bool)(resources.GetObject("lblNature.Enabled")));
			this.lblNature.Font = ((System.Drawing.Font)(resources.GetObject("lblNature.Font")));
			this.lblNature.Image = ((System.Drawing.Image)(resources.GetObject("lblNature.Image")));
			this.lblNature.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("lblNature.ImageAlign")));
			this.lblNature.ImageIndex = ((int)(resources.GetObject("lblNature.ImageIndex")));
			this.lblNature.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("lblNature.ImeMode")));
			this.lblNature.Location = ((System.Drawing.Point)(resources.GetObject("lblNature.Location")));
			this.lblNature.Name = "lblNature";
			this.lblNature.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("lblNature.RightToLeft")));
			this.lblNature.Size = ((System.Drawing.Size)(resources.GetObject("lblNature.Size")));
			this.lblNature.TabIndex = ((int)(resources.GetObject("lblNature.TabIndex")));
			this.lblNature.Text = resources.GetString("lblNature.Text");
			this.lblNature.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("lblNature.TextAlign")));
			this.toolTip1.SetToolTip(this.lblNature, resources.GetString("lblNature.ToolTip"));
			this.lblNature.Visible = ((bool)(resources.GetObject("lblNature.Visible")));
			// 
			// lblHandlingType
			// 
			this.lblHandlingType.AccessibleDescription = resources.GetString("lblHandlingType.AccessibleDescription");
			this.lblHandlingType.AccessibleName = resources.GetString("lblHandlingType.AccessibleName");
			this.lblHandlingType.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("lblHandlingType.Anchor")));
			this.lblHandlingType.AutoSize = ((bool)(resources.GetObject("lblHandlingType.AutoSize")));
			this.lblHandlingType.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("lblHandlingType.Dock")));
			this.lblHandlingType.Enabled = ((bool)(resources.GetObject("lblHandlingType.Enabled")));
			this.lblHandlingType.Font = ((System.Drawing.Font)(resources.GetObject("lblHandlingType.Font")));
			this.lblHandlingType.Image = ((System.Drawing.Image)(resources.GetObject("lblHandlingType.Image")));
			this.lblHandlingType.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("lblHandlingType.ImageAlign")));
			this.lblHandlingType.ImageIndex = ((int)(resources.GetObject("lblHandlingType.ImageIndex")));
			this.lblHandlingType.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("lblHandlingType.ImeMode")));
			this.lblHandlingType.Location = ((System.Drawing.Point)(resources.GetObject("lblHandlingType.Location")));
			this.lblHandlingType.Name = "lblHandlingType";
			this.lblHandlingType.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("lblHandlingType.RightToLeft")));
			this.lblHandlingType.Size = ((System.Drawing.Size)(resources.GetObject("lblHandlingType.Size")));
			this.lblHandlingType.TabIndex = ((int)(resources.GetObject("lblHandlingType.TabIndex")));
			this.lblHandlingType.Text = resources.GetString("lblHandlingType.Text");
			this.lblHandlingType.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("lblHandlingType.TextAlign")));
			this.toolTip1.SetToolTip(this.lblHandlingType, resources.GetString("lblHandlingType.ToolTip"));
			this.lblHandlingType.Visible = ((bool)(resources.GetObject("lblHandlingType.Visible")));
			// 
			// lblHandlingAgent
			// 
			this.lblHandlingAgent.AccessibleDescription = resources.GetString("lblHandlingAgent.AccessibleDescription");
			this.lblHandlingAgent.AccessibleName = resources.GetString("lblHandlingAgent.AccessibleName");
			this.lblHandlingAgent.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("lblHandlingAgent.Anchor")));
			this.lblHandlingAgent.AutoSize = ((bool)(resources.GetObject("lblHandlingAgent.AutoSize")));
			this.lblHandlingAgent.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("lblHandlingAgent.Dock")));
			this.lblHandlingAgent.Enabled = ((bool)(resources.GetObject("lblHandlingAgent.Enabled")));
			this.lblHandlingAgent.Font = ((System.Drawing.Font)(resources.GetObject("lblHandlingAgent.Font")));
			this.lblHandlingAgent.Image = ((System.Drawing.Image)(resources.GetObject("lblHandlingAgent.Image")));
			this.lblHandlingAgent.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("lblHandlingAgent.ImageAlign")));
			this.lblHandlingAgent.ImageIndex = ((int)(resources.GetObject("lblHandlingAgent.ImageIndex")));
			this.lblHandlingAgent.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("lblHandlingAgent.ImeMode")));
			this.lblHandlingAgent.Location = ((System.Drawing.Point)(resources.GetObject("lblHandlingAgent.Location")));
			this.lblHandlingAgent.Name = "lblHandlingAgent";
			this.lblHandlingAgent.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("lblHandlingAgent.RightToLeft")));
			this.lblHandlingAgent.Size = ((System.Drawing.Size)(resources.GetObject("lblHandlingAgent.Size")));
			this.lblHandlingAgent.TabIndex = ((int)(resources.GetObject("lblHandlingAgent.TabIndex")));
			this.lblHandlingAgent.Text = resources.GetString("lblHandlingAgent.Text");
			this.lblHandlingAgent.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("lblHandlingAgent.TextAlign")));
			this.toolTip1.SetToolTip(this.lblHandlingAgent, resources.GetString("lblHandlingAgent.ToolTip"));
			this.lblHandlingAgent.Visible = ((bool)(resources.GetObject("lblHandlingAgent.Visible")));
			// 
			// grpType
			// 
			this.grpType.AccessibleDescription = resources.GetString("grpType.AccessibleDescription");
			this.grpType.AccessibleName = resources.GetString("grpType.AccessibleName");
			this.grpType.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("grpType.Anchor")));
			this.grpType.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("grpType.BackgroundImage")));
			this.grpType.Controls.Add(this.cbRotation);
			this.grpType.Controls.Add(this.cbDeparture);
			this.grpType.Controls.Add(this.cbArrival);
			this.grpType.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("grpType.Dock")));
			this.grpType.Enabled = ((bool)(resources.GetObject("grpType.Enabled")));
			this.grpType.Font = ((System.Drawing.Font)(resources.GetObject("grpType.Font")));
			this.grpType.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("grpType.ImeMode")));
			this.grpType.Location = ((System.Drawing.Point)(resources.GetObject("grpType.Location")));
			this.grpType.Name = "grpType";
			this.grpType.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("grpType.RightToLeft")));
			this.grpType.Size = ((System.Drawing.Size)(resources.GetObject("grpType.Size")));
			this.grpType.TabIndex = ((int)(resources.GetObject("grpType.TabIndex")));
			this.grpType.TabStop = false;
			this.grpType.Text = resources.GetString("grpType.Text");
			this.toolTip1.SetToolTip(this.grpType, resources.GetString("grpType.ToolTip"));
			this.grpType.Visible = ((bool)(resources.GetObject("grpType.Visible")));
			// 
			// cbRotation
			// 
			this.cbRotation.AccessibleDescription = resources.GetString("cbRotation.AccessibleDescription");
			this.cbRotation.AccessibleName = resources.GetString("cbRotation.AccessibleName");
			this.cbRotation.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("cbRotation.Anchor")));
			this.cbRotation.Appearance = ((System.Windows.Forms.Appearance)(resources.GetObject("cbRotation.Appearance")));
			this.cbRotation.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("cbRotation.BackgroundImage")));
			this.cbRotation.CheckAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("cbRotation.CheckAlign")));
			this.cbRotation.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("cbRotation.Dock")));
			this.cbRotation.Enabled = ((bool)(resources.GetObject("cbRotation.Enabled")));
			this.cbRotation.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("cbRotation.FlatStyle")));
			this.cbRotation.Font = ((System.Drawing.Font)(resources.GetObject("cbRotation.Font")));
			this.cbRotation.Image = ((System.Drawing.Image)(resources.GetObject("cbRotation.Image")));
			this.cbRotation.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("cbRotation.ImageAlign")));
			this.cbRotation.ImageIndex = ((int)(resources.GetObject("cbRotation.ImageIndex")));
			this.cbRotation.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("cbRotation.ImeMode")));
			this.cbRotation.Location = ((System.Drawing.Point)(resources.GetObject("cbRotation.Location")));
			this.cbRotation.Name = "cbRotation";
			this.cbRotation.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("cbRotation.RightToLeft")));
			this.cbRotation.Size = ((System.Drawing.Size)(resources.GetObject("cbRotation.Size")));
			this.cbRotation.TabIndex = ((int)(resources.GetObject("cbRotation.TabIndex")));
			this.cbRotation.Text = resources.GetString("cbRotation.Text");
			this.cbRotation.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("cbRotation.TextAlign")));
			this.toolTip1.SetToolTip(this.cbRotation, resources.GetString("cbRotation.ToolTip"));
			this.cbRotation.Visible = ((bool)(resources.GetObject("cbRotation.Visible")));
			this.cbRotation.CheckedChanged += new System.EventHandler(this.cbRotation_CheckedChanged);
			// 
			// cbDeparture
			// 
			this.cbDeparture.AccessibleDescription = resources.GetString("cbDeparture.AccessibleDescription");
			this.cbDeparture.AccessibleName = resources.GetString("cbDeparture.AccessibleName");
			this.cbDeparture.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("cbDeparture.Anchor")));
			this.cbDeparture.Appearance = ((System.Windows.Forms.Appearance)(resources.GetObject("cbDeparture.Appearance")));
			this.cbDeparture.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("cbDeparture.BackgroundImage")));
			this.cbDeparture.CheckAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("cbDeparture.CheckAlign")));
			this.cbDeparture.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("cbDeparture.Dock")));
			this.cbDeparture.Enabled = ((bool)(resources.GetObject("cbDeparture.Enabled")));
			this.cbDeparture.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("cbDeparture.FlatStyle")));
			this.cbDeparture.Font = ((System.Drawing.Font)(resources.GetObject("cbDeparture.Font")));
			this.cbDeparture.Image = ((System.Drawing.Image)(resources.GetObject("cbDeparture.Image")));
			this.cbDeparture.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("cbDeparture.ImageAlign")));
			this.cbDeparture.ImageIndex = ((int)(resources.GetObject("cbDeparture.ImageIndex")));
			this.cbDeparture.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("cbDeparture.ImeMode")));
			this.cbDeparture.Location = ((System.Drawing.Point)(resources.GetObject("cbDeparture.Location")));
			this.cbDeparture.Name = "cbDeparture";
			this.cbDeparture.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("cbDeparture.RightToLeft")));
			this.cbDeparture.Size = ((System.Drawing.Size)(resources.GetObject("cbDeparture.Size")));
			this.cbDeparture.TabIndex = ((int)(resources.GetObject("cbDeparture.TabIndex")));
			this.cbDeparture.Text = resources.GetString("cbDeparture.Text");
			this.cbDeparture.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("cbDeparture.TextAlign")));
			this.toolTip1.SetToolTip(this.cbDeparture, resources.GetString("cbDeparture.ToolTip"));
			this.cbDeparture.Visible = ((bool)(resources.GetObject("cbDeparture.Visible")));
			this.cbDeparture.CheckedChanged += new System.EventHandler(this.cbDeparture_CheckedChanged);
			// 
			// cbArrival
			// 
			this.cbArrival.AccessibleDescription = resources.GetString("cbArrival.AccessibleDescription");
			this.cbArrival.AccessibleName = resources.GetString("cbArrival.AccessibleName");
			this.cbArrival.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("cbArrival.Anchor")));
			this.cbArrival.Appearance = ((System.Windows.Forms.Appearance)(resources.GetObject("cbArrival.Appearance")));
			this.cbArrival.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("cbArrival.BackgroundImage")));
			this.cbArrival.CheckAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("cbArrival.CheckAlign")));
			this.cbArrival.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("cbArrival.Dock")));
			this.cbArrival.Enabled = ((bool)(resources.GetObject("cbArrival.Enabled")));
			this.cbArrival.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("cbArrival.FlatStyle")));
			this.cbArrival.Font = ((System.Drawing.Font)(resources.GetObject("cbArrival.Font")));
			this.cbArrival.Image = ((System.Drawing.Image)(resources.GetObject("cbArrival.Image")));
			this.cbArrival.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("cbArrival.ImageAlign")));
			this.cbArrival.ImageIndex = ((int)(resources.GetObject("cbArrival.ImageIndex")));
			this.cbArrival.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("cbArrival.ImeMode")));
			this.cbArrival.Location = ((System.Drawing.Point)(resources.GetObject("cbArrival.Location")));
			this.cbArrival.Name = "cbArrival";
			this.cbArrival.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("cbArrival.RightToLeft")));
			this.cbArrival.Size = ((System.Drawing.Size)(resources.GetObject("cbArrival.Size")));
			this.cbArrival.TabIndex = ((int)(resources.GetObject("cbArrival.TabIndex")));
			this.cbArrival.Text = resources.GetString("cbArrival.Text");
			this.cbArrival.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("cbArrival.TextAlign")));
			this.toolTip1.SetToolTip(this.cbArrival, resources.GetString("cbArrival.ToolTip"));
			this.cbArrival.Visible = ((bool)(resources.GetObject("cbArrival.Visible")));
			this.cbArrival.CheckedChanged += new System.EventHandler(this.cbArrival_CheckedChanged);
			// 
			// cbCancelled
			// 
			this.cbCancelled.AccessibleDescription = resources.GetString("cbCancelled.AccessibleDescription");
			this.cbCancelled.AccessibleName = resources.GetString("cbCancelled.AccessibleName");
			this.cbCancelled.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("cbCancelled.Anchor")));
			this.cbCancelled.Appearance = ((System.Windows.Forms.Appearance)(resources.GetObject("cbCancelled.Appearance")));
			this.cbCancelled.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("cbCancelled.BackgroundImage")));
			this.cbCancelled.CheckAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("cbCancelled.CheckAlign")));
			this.cbCancelled.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("cbCancelled.Dock")));
			this.cbCancelled.Enabled = ((bool)(resources.GetObject("cbCancelled.Enabled")));
			this.cbCancelled.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("cbCancelled.FlatStyle")));
			this.cbCancelled.Font = ((System.Drawing.Font)(resources.GetObject("cbCancelled.Font")));
			this.cbCancelled.Image = ((System.Drawing.Image)(resources.GetObject("cbCancelled.Image")));
			this.cbCancelled.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("cbCancelled.ImageAlign")));
			this.cbCancelled.ImageIndex = ((int)(resources.GetObject("cbCancelled.ImageIndex")));
			this.cbCancelled.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("cbCancelled.ImeMode")));
			this.cbCancelled.Location = ((System.Drawing.Point)(resources.GetObject("cbCancelled.Location")));
			this.cbCancelled.Name = "cbCancelled";
			this.cbCancelled.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("cbCancelled.RightToLeft")));
			this.cbCancelled.Size = ((System.Drawing.Size)(resources.GetObject("cbCancelled.Size")));
			this.cbCancelled.TabIndex = ((int)(resources.GetObject("cbCancelled.TabIndex")));
			this.cbCancelled.Text = resources.GetString("cbCancelled.Text");
			this.cbCancelled.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("cbCancelled.TextAlign")));
			this.toolTip1.SetToolTip(this.cbCancelled, resources.GetString("cbCancelled.ToolTip"));
			this.cbCancelled.Visible = ((bool)(resources.GetObject("cbCancelled.Visible")));
			this.cbCancelled.CheckedChanged += new System.EventHandler(this.cbCancelled_CheckedChanged);
			// 
			// cbNoop
			// 
			this.cbNoop.AccessibleDescription = resources.GetString("cbNoop.AccessibleDescription");
			this.cbNoop.AccessibleName = resources.GetString("cbNoop.AccessibleName");
			this.cbNoop.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("cbNoop.Anchor")));
			this.cbNoop.Appearance = ((System.Windows.Forms.Appearance)(resources.GetObject("cbNoop.Appearance")));
			this.cbNoop.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("cbNoop.BackgroundImage")));
			this.cbNoop.CheckAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("cbNoop.CheckAlign")));
			this.cbNoop.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("cbNoop.Dock")));
			this.cbNoop.Enabled = ((bool)(resources.GetObject("cbNoop.Enabled")));
			this.cbNoop.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("cbNoop.FlatStyle")));
			this.cbNoop.Font = ((System.Drawing.Font)(resources.GetObject("cbNoop.Font")));
			this.cbNoop.Image = ((System.Drawing.Image)(resources.GetObject("cbNoop.Image")));
			this.cbNoop.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("cbNoop.ImageAlign")));
			this.cbNoop.ImageIndex = ((int)(resources.GetObject("cbNoop.ImageIndex")));
			this.cbNoop.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("cbNoop.ImeMode")));
			this.cbNoop.Location = ((System.Drawing.Point)(resources.GetObject("cbNoop.Location")));
			this.cbNoop.Name = "cbNoop";
			this.cbNoop.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("cbNoop.RightToLeft")));
			this.cbNoop.Size = ((System.Drawing.Size)(resources.GetObject("cbNoop.Size")));
			this.cbNoop.TabIndex = ((int)(resources.GetObject("cbNoop.TabIndex")));
			this.cbNoop.Text = resources.GetString("cbNoop.Text");
			this.cbNoop.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("cbNoop.TextAlign")));
			this.toolTip1.SetToolTip(this.cbNoop, resources.GetString("cbNoop.ToolTip"));
			this.cbNoop.Visible = ((bool)(resources.GetObject("cbNoop.Visible")));
			this.cbNoop.CheckedChanged += new System.EventHandler(this.cbNoop_CheckedChanged);
			// 
			// cbDiverted
			// 
			this.cbDiverted.AccessibleDescription = resources.GetString("cbDiverted.AccessibleDescription");
			this.cbDiverted.AccessibleName = resources.GetString("cbDiverted.AccessibleName");
			this.cbDiverted.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("cbDiverted.Anchor")));
			this.cbDiverted.Appearance = ((System.Windows.Forms.Appearance)(resources.GetObject("cbDiverted.Appearance")));
			this.cbDiverted.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("cbDiverted.BackgroundImage")));
			this.cbDiverted.CheckAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("cbDiverted.CheckAlign")));
			this.cbDiverted.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("cbDiverted.Dock")));
			this.cbDiverted.Enabled = ((bool)(resources.GetObject("cbDiverted.Enabled")));
			this.cbDiverted.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("cbDiverted.FlatStyle")));
			this.cbDiverted.Font = ((System.Drawing.Font)(resources.GetObject("cbDiverted.Font")));
			this.cbDiverted.Image = ((System.Drawing.Image)(resources.GetObject("cbDiverted.Image")));
			this.cbDiverted.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("cbDiverted.ImageAlign")));
			this.cbDiverted.ImageIndex = ((int)(resources.GetObject("cbDiverted.ImageIndex")));
			this.cbDiverted.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("cbDiverted.ImeMode")));
			this.cbDiverted.Location = ((System.Drawing.Point)(resources.GetObject("cbDiverted.Location")));
			this.cbDiverted.Name = "cbDiverted";
			this.cbDiverted.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("cbDiverted.RightToLeft")));
			this.cbDiverted.Size = ((System.Drawing.Size)(resources.GetObject("cbDiverted.Size")));
			this.cbDiverted.TabIndex = ((int)(resources.GetObject("cbDiverted.TabIndex")));
			this.cbDiverted.Text = resources.GetString("cbDiverted.Text");
			this.cbDiverted.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("cbDiverted.TextAlign")));
			this.toolTip1.SetToolTip(this.cbDiverted, resources.GetString("cbDiverted.ToolTip"));
			this.cbDiverted.Visible = ((bool)(resources.GetObject("cbDiverted.Visible")));
			this.cbDiverted.CheckedChanged += new System.EventHandler(this.cbDiverted_CheckedChanged);
			// 
			// cbRerouted
			// 
			this.cbRerouted.AccessibleDescription = resources.GetString("cbRerouted.AccessibleDescription");
			this.cbRerouted.AccessibleName = resources.GetString("cbRerouted.AccessibleName");
			this.cbRerouted.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("cbRerouted.Anchor")));
			this.cbRerouted.Appearance = ((System.Windows.Forms.Appearance)(resources.GetObject("cbRerouted.Appearance")));
			this.cbRerouted.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("cbRerouted.BackgroundImage")));
			this.cbRerouted.CheckAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("cbRerouted.CheckAlign")));
			this.cbRerouted.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("cbRerouted.Dock")));
			this.cbRerouted.Enabled = ((bool)(resources.GetObject("cbRerouted.Enabled")));
			this.cbRerouted.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("cbRerouted.FlatStyle")));
			this.cbRerouted.Font = ((System.Drawing.Font)(resources.GetObject("cbRerouted.Font")));
			this.cbRerouted.Image = ((System.Drawing.Image)(resources.GetObject("cbRerouted.Image")));
			this.cbRerouted.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("cbRerouted.ImageAlign")));
			this.cbRerouted.ImageIndex = ((int)(resources.GetObject("cbRerouted.ImageIndex")));
			this.cbRerouted.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("cbRerouted.ImeMode")));
			this.cbRerouted.Location = ((System.Drawing.Point)(resources.GetObject("cbRerouted.Location")));
			this.cbRerouted.Name = "cbRerouted";
			this.cbRerouted.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("cbRerouted.RightToLeft")));
			this.cbRerouted.Size = ((System.Drawing.Size)(resources.GetObject("cbRerouted.Size")));
			this.cbRerouted.TabIndex = ((int)(resources.GetObject("cbRerouted.TabIndex")));
			this.cbRerouted.Text = resources.GetString("cbRerouted.Text");
			this.cbRerouted.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("cbRerouted.TextAlign")));
			this.toolTip1.SetToolTip(this.cbRerouted, resources.GetString("cbRerouted.ToolTip"));
			this.cbRerouted.Visible = ((bool)(resources.GetObject("cbRerouted.Visible")));
			this.cbRerouted.CheckedChanged += new System.EventHandler(this.cbRerouted_CheckedChanged);
			// 
			// grbOnGround
			// 
			this.grbOnGround.AccessibleDescription = resources.GetString("grbOnGround.AccessibleDescription");
			this.grbOnGround.AccessibleName = resources.GetString("grbOnGround.AccessibleName");
			this.grbOnGround.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("grbOnGround.Anchor")));
			this.grbOnGround.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("grbOnGround.BackgroundImage")));
			this.grbOnGround.Controls.Add(this.cbTowing);
			this.grbOnGround.Controls.Add(this.cbGroundMovement);
			this.grbOnGround.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("grbOnGround.Dock")));
			this.grbOnGround.Enabled = ((bool)(resources.GetObject("grbOnGround.Enabled")));
			this.grbOnGround.Font = ((System.Drawing.Font)(resources.GetObject("grbOnGround.Font")));
			this.grbOnGround.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("grbOnGround.ImeMode")));
			this.grbOnGround.Location = ((System.Drawing.Point)(resources.GetObject("grbOnGround.Location")));
			this.grbOnGround.Name = "grbOnGround";
			this.grbOnGround.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("grbOnGround.RightToLeft")));
			this.grbOnGround.Size = ((System.Drawing.Size)(resources.GetObject("grbOnGround.Size")));
			this.grbOnGround.TabIndex = ((int)(resources.GetObject("grbOnGround.TabIndex")));
			this.grbOnGround.TabStop = false;
			this.grbOnGround.Text = resources.GetString("grbOnGround.Text");
			this.toolTip1.SetToolTip(this.grbOnGround, resources.GetString("grbOnGround.ToolTip"));
			this.grbOnGround.Visible = ((bool)(resources.GetObject("grbOnGround.Visible")));
			// 
			// cbTowing
			// 
			this.cbTowing.AccessibleDescription = resources.GetString("cbTowing.AccessibleDescription");
			this.cbTowing.AccessibleName = resources.GetString("cbTowing.AccessibleName");
			this.cbTowing.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("cbTowing.Anchor")));
			this.cbTowing.Appearance = ((System.Windows.Forms.Appearance)(resources.GetObject("cbTowing.Appearance")));
			this.cbTowing.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("cbTowing.BackgroundImage")));
			this.cbTowing.CheckAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("cbTowing.CheckAlign")));
			this.cbTowing.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("cbTowing.Dock")));
			this.cbTowing.Enabled = ((bool)(resources.GetObject("cbTowing.Enabled")));
			this.cbTowing.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("cbTowing.FlatStyle")));
			this.cbTowing.Font = ((System.Drawing.Font)(resources.GetObject("cbTowing.Font")));
			this.cbTowing.Image = ((System.Drawing.Image)(resources.GetObject("cbTowing.Image")));
			this.cbTowing.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("cbTowing.ImageAlign")));
			this.cbTowing.ImageIndex = ((int)(resources.GetObject("cbTowing.ImageIndex")));
			this.cbTowing.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("cbTowing.ImeMode")));
			this.cbTowing.Location = ((System.Drawing.Point)(resources.GetObject("cbTowing.Location")));
			this.cbTowing.Name = "cbTowing";
			this.cbTowing.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("cbTowing.RightToLeft")));
			this.cbTowing.Size = ((System.Drawing.Size)(resources.GetObject("cbTowing.Size")));
			this.cbTowing.TabIndex = ((int)(resources.GetObject("cbTowing.TabIndex")));
			this.cbTowing.Text = resources.GetString("cbTowing.Text");
			this.cbTowing.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("cbTowing.TextAlign")));
			this.toolTip1.SetToolTip(this.cbTowing, resources.GetString("cbTowing.ToolTip"));
			this.cbTowing.Visible = ((bool)(resources.GetObject("cbTowing.Visible")));
			this.cbTowing.CheckedChanged += new System.EventHandler(this.cbTowing_CheckedChanged);
			// 
			// cbGroundMovement
			// 
			this.cbGroundMovement.AccessibleDescription = resources.GetString("cbGroundMovement.AccessibleDescription");
			this.cbGroundMovement.AccessibleName = resources.GetString("cbGroundMovement.AccessibleName");
			this.cbGroundMovement.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("cbGroundMovement.Anchor")));
			this.cbGroundMovement.Appearance = ((System.Windows.Forms.Appearance)(resources.GetObject("cbGroundMovement.Appearance")));
			this.cbGroundMovement.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("cbGroundMovement.BackgroundImage")));
			this.cbGroundMovement.CheckAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("cbGroundMovement.CheckAlign")));
			this.cbGroundMovement.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("cbGroundMovement.Dock")));
			this.cbGroundMovement.Enabled = ((bool)(resources.GetObject("cbGroundMovement.Enabled")));
			this.cbGroundMovement.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("cbGroundMovement.FlatStyle")));
			this.cbGroundMovement.Font = ((System.Drawing.Font)(resources.GetObject("cbGroundMovement.Font")));
			this.cbGroundMovement.Image = ((System.Drawing.Image)(resources.GetObject("cbGroundMovement.Image")));
			this.cbGroundMovement.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("cbGroundMovement.ImageAlign")));
			this.cbGroundMovement.ImageIndex = ((int)(resources.GetObject("cbGroundMovement.ImageIndex")));
			this.cbGroundMovement.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("cbGroundMovement.ImeMode")));
			this.cbGroundMovement.Location = ((System.Drawing.Point)(resources.GetObject("cbGroundMovement.Location")));
			this.cbGroundMovement.Name = "cbGroundMovement";
			this.cbGroundMovement.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("cbGroundMovement.RightToLeft")));
			this.cbGroundMovement.Size = ((System.Drawing.Size)(resources.GetObject("cbGroundMovement.Size")));
			this.cbGroundMovement.TabIndex = ((int)(resources.GetObject("cbGroundMovement.TabIndex")));
			this.cbGroundMovement.Text = resources.GetString("cbGroundMovement.Text");
			this.cbGroundMovement.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("cbGroundMovement.TextAlign")));
			this.toolTip1.SetToolTip(this.cbGroundMovement, resources.GetString("cbGroundMovement.ToolTip"));
			this.cbGroundMovement.Visible = ((bool)(resources.GetObject("cbGroundMovement.Visible")));
			this.cbGroundMovement.CheckedChanged += new System.EventHandler(this.cbGroundMovement_CheckedChanged);
			// 
			// grpMode
			// 
			this.grpMode.AccessibleDescription = resources.GetString("grpMode.AccessibleDescription");
			this.grpMode.AccessibleName = resources.GetString("grpMode.AccessibleName");
			this.grpMode.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("grpMode.Anchor")));
			this.grpMode.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("grpMode.BackgroundImage")));
			this.grpMode.Controls.Add(this.cbPrognosis);
			this.grpMode.Controls.Add(this.cbPlanning);
			this.grpMode.Controls.Add(this.cbOperational);
			this.grpMode.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("grpMode.Dock")));
			this.grpMode.Enabled = ((bool)(resources.GetObject("grpMode.Enabled")));
			this.grpMode.Font = ((System.Drawing.Font)(resources.GetObject("grpMode.Font")));
			this.grpMode.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("grpMode.ImeMode")));
			this.grpMode.Location = ((System.Drawing.Point)(resources.GetObject("grpMode.Location")));
			this.grpMode.Name = "grpMode";
			this.grpMode.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("grpMode.RightToLeft")));
			this.grpMode.Size = ((System.Drawing.Size)(resources.GetObject("grpMode.Size")));
			this.grpMode.TabIndex = ((int)(resources.GetObject("grpMode.TabIndex")));
			this.grpMode.TabStop = false;
			this.grpMode.Text = resources.GetString("grpMode.Text");
			this.toolTip1.SetToolTip(this.grpMode, resources.GetString("grpMode.ToolTip"));
			this.grpMode.Visible = ((bool)(resources.GetObject("grpMode.Visible")));
			// 
			// cbPrognosis
			// 
			this.cbPrognosis.AccessibleDescription = resources.GetString("cbPrognosis.AccessibleDescription");
			this.cbPrognosis.AccessibleName = resources.GetString("cbPrognosis.AccessibleName");
			this.cbPrognosis.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("cbPrognosis.Anchor")));
			this.cbPrognosis.Appearance = ((System.Windows.Forms.Appearance)(resources.GetObject("cbPrognosis.Appearance")));
			this.cbPrognosis.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("cbPrognosis.BackgroundImage")));
			this.cbPrognosis.CheckAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("cbPrognosis.CheckAlign")));
			this.cbPrognosis.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("cbPrognosis.Dock")));
			this.cbPrognosis.Enabled = ((bool)(resources.GetObject("cbPrognosis.Enabled")));
			this.cbPrognosis.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("cbPrognosis.FlatStyle")));
			this.cbPrognosis.Font = ((System.Drawing.Font)(resources.GetObject("cbPrognosis.Font")));
			this.cbPrognosis.Image = ((System.Drawing.Image)(resources.GetObject("cbPrognosis.Image")));
			this.cbPrognosis.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("cbPrognosis.ImageAlign")));
			this.cbPrognosis.ImageIndex = ((int)(resources.GetObject("cbPrognosis.ImageIndex")));
			this.cbPrognosis.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("cbPrognosis.ImeMode")));
			this.cbPrognosis.Location = ((System.Drawing.Point)(resources.GetObject("cbPrognosis.Location")));
			this.cbPrognosis.Name = "cbPrognosis";
			this.cbPrognosis.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("cbPrognosis.RightToLeft")));
			this.cbPrognosis.Size = ((System.Drawing.Size)(resources.GetObject("cbPrognosis.Size")));
			this.cbPrognosis.TabIndex = ((int)(resources.GetObject("cbPrognosis.TabIndex")));
			this.cbPrognosis.Text = resources.GetString("cbPrognosis.Text");
			this.cbPrognosis.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("cbPrognosis.TextAlign")));
			this.toolTip1.SetToolTip(this.cbPrognosis, resources.GetString("cbPrognosis.ToolTip"));
			this.cbPrognosis.Visible = ((bool)(resources.GetObject("cbPrognosis.Visible")));
			this.cbPrognosis.CheckedChanged += new System.EventHandler(this.cbPrognosis_CheckedChanged);
			// 
			// cbPlanning
			// 
			this.cbPlanning.AccessibleDescription = resources.GetString("cbPlanning.AccessibleDescription");
			this.cbPlanning.AccessibleName = resources.GetString("cbPlanning.AccessibleName");
			this.cbPlanning.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("cbPlanning.Anchor")));
			this.cbPlanning.Appearance = ((System.Windows.Forms.Appearance)(resources.GetObject("cbPlanning.Appearance")));
			this.cbPlanning.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("cbPlanning.BackgroundImage")));
			this.cbPlanning.CheckAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("cbPlanning.CheckAlign")));
			this.cbPlanning.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("cbPlanning.Dock")));
			this.cbPlanning.Enabled = ((bool)(resources.GetObject("cbPlanning.Enabled")));
			this.cbPlanning.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("cbPlanning.FlatStyle")));
			this.cbPlanning.Font = ((System.Drawing.Font)(resources.GetObject("cbPlanning.Font")));
			this.cbPlanning.Image = ((System.Drawing.Image)(resources.GetObject("cbPlanning.Image")));
			this.cbPlanning.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("cbPlanning.ImageAlign")));
			this.cbPlanning.ImageIndex = ((int)(resources.GetObject("cbPlanning.ImageIndex")));
			this.cbPlanning.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("cbPlanning.ImeMode")));
			this.cbPlanning.Location = ((System.Drawing.Point)(resources.GetObject("cbPlanning.Location")));
			this.cbPlanning.Name = "cbPlanning";
			this.cbPlanning.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("cbPlanning.RightToLeft")));
			this.cbPlanning.Size = ((System.Drawing.Size)(resources.GetObject("cbPlanning.Size")));
			this.cbPlanning.TabIndex = ((int)(resources.GetObject("cbPlanning.TabIndex")));
			this.cbPlanning.Text = resources.GetString("cbPlanning.Text");
			this.cbPlanning.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("cbPlanning.TextAlign")));
			this.toolTip1.SetToolTip(this.cbPlanning, resources.GetString("cbPlanning.ToolTip"));
			this.cbPlanning.Visible = ((bool)(resources.GetObject("cbPlanning.Visible")));
			this.cbPlanning.CheckedChanged += new System.EventHandler(this.cbPlanning_CheckedChanged);
			// 
			// cbOperational
			// 
			this.cbOperational.AccessibleDescription = resources.GetString("cbOperational.AccessibleDescription");
			this.cbOperational.AccessibleName = resources.GetString("cbOperational.AccessibleName");
			this.cbOperational.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("cbOperational.Anchor")));
			this.cbOperational.Appearance = ((System.Windows.Forms.Appearance)(resources.GetObject("cbOperational.Appearance")));
			this.cbOperational.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("cbOperational.BackgroundImage")));
			this.cbOperational.CheckAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("cbOperational.CheckAlign")));
			this.cbOperational.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("cbOperational.Dock")));
			this.cbOperational.Enabled = ((bool)(resources.GetObject("cbOperational.Enabled")));
			this.cbOperational.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("cbOperational.FlatStyle")));
			this.cbOperational.Font = ((System.Drawing.Font)(resources.GetObject("cbOperational.Font")));
			this.cbOperational.Image = ((System.Drawing.Image)(resources.GetObject("cbOperational.Image")));
			this.cbOperational.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("cbOperational.ImageAlign")));
			this.cbOperational.ImageIndex = ((int)(resources.GetObject("cbOperational.ImageIndex")));
			this.cbOperational.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("cbOperational.ImeMode")));
			this.cbOperational.Location = ((System.Drawing.Point)(resources.GetObject("cbOperational.Location")));
			this.cbOperational.Name = "cbOperational";
			this.cbOperational.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("cbOperational.RightToLeft")));
			this.cbOperational.Size = ((System.Drawing.Size)(resources.GetObject("cbOperational.Size")));
			this.cbOperational.TabIndex = ((int)(resources.GetObject("cbOperational.TabIndex")));
			this.cbOperational.Text = resources.GetString("cbOperational.Text");
			this.cbOperational.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("cbOperational.TextAlign")));
			this.toolTip1.SetToolTip(this.cbOperational, resources.GetString("cbOperational.ToolTip"));
			this.cbOperational.Visible = ((bool)(resources.GetObject("cbOperational.Visible")));
			this.cbOperational.CheckedChanged += new System.EventHandler(this.cbOperational_CheckedChanged);
			// 
			// grpStatus
			// 
			this.grpStatus.AccessibleDescription = resources.GetString("grpStatus.AccessibleDescription");
			this.grpStatus.AccessibleName = resources.GetString("grpStatus.AccessibleName");
			this.grpStatus.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("grpStatus.Anchor")));
			this.grpStatus.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("grpStatus.BackgroundImage")));
			this.grpStatus.Controls.Add(this.cbNoop);
			this.grpStatus.Controls.Add(this.cbRerouted);
			this.grpStatus.Controls.Add(this.cbCancelled);
			this.grpStatus.Controls.Add(this.cbDiverted);
			this.grpStatus.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("grpStatus.Dock")));
			this.grpStatus.Enabled = ((bool)(resources.GetObject("grpStatus.Enabled")));
			this.grpStatus.Font = ((System.Drawing.Font)(resources.GetObject("grpStatus.Font")));
			this.grpStatus.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("grpStatus.ImeMode")));
			this.grpStatus.Location = ((System.Drawing.Point)(resources.GetObject("grpStatus.Location")));
			this.grpStatus.Name = "grpStatus";
			this.grpStatus.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("grpStatus.RightToLeft")));
			this.grpStatus.Size = ((System.Drawing.Size)(resources.GetObject("grpStatus.Size")));
			this.grpStatus.TabIndex = ((int)(resources.GetObject("grpStatus.TabIndex")));
			this.grpStatus.TabStop = false;
			this.grpStatus.Text = resources.GetString("grpStatus.Text");
			this.toolTip1.SetToolTip(this.grpStatus, resources.GetString("grpStatus.ToolTip"));
			this.grpStatus.Visible = ((bool)(resources.GetObject("grpStatus.Visible")));
			// 
			// cboHandlingAgent
			// 
			this.cboHandlingAgent.AccessibleDescription = resources.GetString("cboHandlingAgent.AccessibleDescription");
			this.cboHandlingAgent.AccessibleName = resources.GetString("cboHandlingAgent.AccessibleName");
			this.cboHandlingAgent.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("cboHandlingAgent.Anchor")));
			this.cboHandlingAgent.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("cboHandlingAgent.BackgroundImage")));
			this.cboHandlingAgent.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("cboHandlingAgent.Dock")));
			this.cboHandlingAgent.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cboHandlingAgent.Enabled = ((bool)(resources.GetObject("cboHandlingAgent.Enabled")));
			this.cboHandlingAgent.Font = ((System.Drawing.Font)(resources.GetObject("cboHandlingAgent.Font")));
			this.cboHandlingAgent.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("cboHandlingAgent.ImeMode")));
			this.cboHandlingAgent.IntegralHeight = ((bool)(resources.GetObject("cboHandlingAgent.IntegralHeight")));
			this.cboHandlingAgent.ItemHeight = ((int)(resources.GetObject("cboHandlingAgent.ItemHeight")));
			this.cboHandlingAgent.Location = ((System.Drawing.Point)(resources.GetObject("cboHandlingAgent.Location")));
			this.cboHandlingAgent.MaxDropDownItems = ((int)(resources.GetObject("cboHandlingAgent.MaxDropDownItems")));
			this.cboHandlingAgent.MaxLength = ((int)(resources.GetObject("cboHandlingAgent.MaxLength")));
			this.cboHandlingAgent.Name = "cboHandlingAgent";
			this.cboHandlingAgent.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("cboHandlingAgent.RightToLeft")));
			this.cboHandlingAgent.Size = ((System.Drawing.Size)(resources.GetObject("cboHandlingAgent.Size")));
			this.cboHandlingAgent.Sorted = true;
			this.cboHandlingAgent.TabIndex = ((int)(resources.GetObject("cboHandlingAgent.TabIndex")));
			this.cboHandlingAgent.Text = resources.GetString("cboHandlingAgent.Text");
			this.toolTip1.SetToolTip(this.cboHandlingAgent, resources.GetString("cboHandlingAgent.ToolTip"));
			this.cboHandlingAgent.Visible = ((bool)(resources.GetObject("cboHandlingAgent.Visible")));
			this.cboHandlingAgent.TextChanged += new System.EventHandler(this.cboHandlingAgent_TextChanged);
			this.cboHandlingAgent.SelectedIndexChanged += new System.EventHandler(this.cboHandlingAgent_TextChanged);
			// 
			// cboHandlingType
			// 
			this.cboHandlingType.AccessibleDescription = resources.GetString("cboHandlingType.AccessibleDescription");
			this.cboHandlingType.AccessibleName = resources.GetString("cboHandlingType.AccessibleName");
			this.cboHandlingType.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("cboHandlingType.Anchor")));
			this.cboHandlingType.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("cboHandlingType.BackgroundImage")));
			this.cboHandlingType.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("cboHandlingType.Dock")));
			this.cboHandlingType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cboHandlingType.Enabled = ((bool)(resources.GetObject("cboHandlingType.Enabled")));
			this.cboHandlingType.Font = ((System.Drawing.Font)(resources.GetObject("cboHandlingType.Font")));
			this.cboHandlingType.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("cboHandlingType.ImeMode")));
			this.cboHandlingType.IntegralHeight = ((bool)(resources.GetObject("cboHandlingType.IntegralHeight")));
			this.cboHandlingType.ItemHeight = ((int)(resources.GetObject("cboHandlingType.ItemHeight")));
			this.cboHandlingType.Location = ((System.Drawing.Point)(resources.GetObject("cboHandlingType.Location")));
			this.cboHandlingType.MaxDropDownItems = ((int)(resources.GetObject("cboHandlingType.MaxDropDownItems")));
			this.cboHandlingType.MaxLength = ((int)(resources.GetObject("cboHandlingType.MaxLength")));
			this.cboHandlingType.Name = "cboHandlingType";
			this.cboHandlingType.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("cboHandlingType.RightToLeft")));
			this.cboHandlingType.Size = ((System.Drawing.Size)(resources.GetObject("cboHandlingType.Size")));
			this.cboHandlingType.Sorted = true;
			this.cboHandlingType.TabIndex = ((int)(resources.GetObject("cboHandlingType.TabIndex")));
			this.cboHandlingType.Text = resources.GetString("cboHandlingType.Text");
			this.toolTip1.SetToolTip(this.cboHandlingType, resources.GetString("cboHandlingType.ToolTip"));
			this.cboHandlingType.Visible = ((bool)(resources.GetObject("cboHandlingType.Visible")));
			this.cboHandlingType.TextChanged += new System.EventHandler(this.cboHandlingType_TextChanged);
			this.cboHandlingType.SelectedIndexChanged += new System.EventHandler(this.cboHandlingType_TextChanged);
			// 
			// clbNature
			// 
			this.clbNature.AccessibleDescription = resources.GetString("clbNature.AccessibleDescription");
			this.clbNature.AccessibleName = resources.GetString("clbNature.AccessibleName");
			this.clbNature.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("clbNature.Anchor")));
			this.clbNature.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("clbNature.BackgroundImage")));
			this.clbNature.CheckOnClick = true;
			this.clbNature.ColumnWidth = ((int)(resources.GetObject("clbNature.ColumnWidth")));
			this.clbNature.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("clbNature.Dock")));
			this.clbNature.Enabled = ((bool)(resources.GetObject("clbNature.Enabled")));
			this.clbNature.Font = ((System.Drawing.Font)(resources.GetObject("clbNature.Font")));
			this.clbNature.HorizontalExtent = ((int)(resources.GetObject("clbNature.HorizontalExtent")));
			this.clbNature.HorizontalScrollbar = ((bool)(resources.GetObject("clbNature.HorizontalScrollbar")));
			this.clbNature.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("clbNature.ImeMode")));
			this.clbNature.IntegralHeight = ((bool)(resources.GetObject("clbNature.IntegralHeight")));
			this.clbNature.Location = ((System.Drawing.Point)(resources.GetObject("clbNature.Location")));
			this.clbNature.Name = "clbNature";
			this.clbNature.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("clbNature.RightToLeft")));
			this.clbNature.ScrollAlwaysVisible = ((bool)(resources.GetObject("clbNature.ScrollAlwaysVisible")));
			this.clbNature.Size = ((System.Drawing.Size)(resources.GetObject("clbNature.Size")));
			this.clbNature.Sorted = true;
			this.clbNature.TabIndex = ((int)(resources.GetObject("clbNature.TabIndex")));
			this.toolTip1.SetToolTip(this.clbNature, resources.GetString("clbNature.ToolTip"));
			this.clbNature.Visible = ((bool)(resources.GetObject("clbNature.Visible")));
			this.clbNature.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.clbNature_ItemCheck);
			// 
			// clbAirport
			// 
			this.clbAirport.AccessibleDescription = resources.GetString("clbAirport.AccessibleDescription");
			this.clbAirport.AccessibleName = resources.GetString("clbAirport.AccessibleName");
			this.clbAirport.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("clbAirport.Anchor")));
			this.clbAirport.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("clbAirport.BackgroundImage")));
			this.clbAirport.CheckOnClick = true;
			this.clbAirport.ColumnWidth = ((int)(resources.GetObject("clbAirport.ColumnWidth")));
			this.clbAirport.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("clbAirport.Dock")));
			this.clbAirport.Enabled = ((bool)(resources.GetObject("clbAirport.Enabled")));
			this.clbAirport.Font = ((System.Drawing.Font)(resources.GetObject("clbAirport.Font")));
			this.clbAirport.HorizontalExtent = ((int)(resources.GetObject("clbAirport.HorizontalExtent")));
			this.clbAirport.HorizontalScrollbar = ((bool)(resources.GetObject("clbAirport.HorizontalScrollbar")));
			this.clbAirport.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("clbAirport.ImeMode")));
			this.clbAirport.IntegralHeight = ((bool)(resources.GetObject("clbAirport.IntegralHeight")));
			this.clbAirport.Location = ((System.Drawing.Point)(resources.GetObject("clbAirport.Location")));
			this.clbAirport.MultiColumn = true;
			this.clbAirport.Name = "clbAirport";
			this.clbAirport.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("clbAirport.RightToLeft")));
			this.clbAirport.ScrollAlwaysVisible = ((bool)(resources.GetObject("clbAirport.ScrollAlwaysVisible")));
			this.clbAirport.Size = ((System.Drawing.Size)(resources.GetObject("clbAirport.Size")));
			this.clbAirport.Sorted = true;
			this.clbAirport.TabIndex = ((int)(resources.GetObject("clbAirport.TabIndex")));
			this.toolTip1.SetToolTip(this.clbAirport, resources.GetString("clbAirport.ToolTip"));
			this.clbAirport.Visible = ((bool)(resources.GetObject("clbAirport.Visible")));
			this.clbAirport.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.clbAirport_ItemCheck);
			// 
			// clbAircraftTypes
			// 
			this.clbAircraftTypes.AccessibleDescription = resources.GetString("clbAircraftTypes.AccessibleDescription");
			this.clbAircraftTypes.AccessibleName = resources.GetString("clbAircraftTypes.AccessibleName");
			this.clbAircraftTypes.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("clbAircraftTypes.Anchor")));
			this.clbAircraftTypes.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("clbAircraftTypes.BackgroundImage")));
			this.clbAircraftTypes.CheckOnClick = true;
			this.clbAircraftTypes.ColumnWidth = ((int)(resources.GetObject("clbAircraftTypes.ColumnWidth")));
			this.clbAircraftTypes.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("clbAircraftTypes.Dock")));
			this.clbAircraftTypes.Enabled = ((bool)(resources.GetObject("clbAircraftTypes.Enabled")));
			this.clbAircraftTypes.Font = ((System.Drawing.Font)(resources.GetObject("clbAircraftTypes.Font")));
			this.clbAircraftTypes.HorizontalExtent = ((int)(resources.GetObject("clbAircraftTypes.HorizontalExtent")));
			this.clbAircraftTypes.HorizontalScrollbar = ((bool)(resources.GetObject("clbAircraftTypes.HorizontalScrollbar")));
			this.clbAircraftTypes.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("clbAircraftTypes.ImeMode")));
			this.clbAircraftTypes.IntegralHeight = ((bool)(resources.GetObject("clbAircraftTypes.IntegralHeight")));
			this.clbAircraftTypes.Location = ((System.Drawing.Point)(resources.GetObject("clbAircraftTypes.Location")));
			this.clbAircraftTypes.MultiColumn = true;
			this.clbAircraftTypes.Name = "clbAircraftTypes";
			this.clbAircraftTypes.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("clbAircraftTypes.RightToLeft")));
			this.clbAircraftTypes.ScrollAlwaysVisible = ((bool)(resources.GetObject("clbAircraftTypes.ScrollAlwaysVisible")));
			this.clbAircraftTypes.Size = ((System.Drawing.Size)(resources.GetObject("clbAircraftTypes.Size")));
			this.clbAircraftTypes.Sorted = true;
			this.clbAircraftTypes.TabIndex = ((int)(resources.GetObject("clbAircraftTypes.TabIndex")));
			this.toolTip1.SetToolTip(this.clbAircraftTypes, resources.GetString("clbAircraftTypes.ToolTip"));
			this.clbAircraftTypes.Visible = ((bool)(resources.GetObject("clbAircraftTypes.Visible")));
			this.clbAircraftTypes.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.clbAircraftTypes_ItemCheck);
			// 
			// txtOwner
			// 
			this.txtOwner.AccessibleDescription = resources.GetString("txtOwner.AccessibleDescription");
			this.txtOwner.AccessibleName = resources.GetString("txtOwner.AccessibleName");
			this.txtOwner.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("txtOwner.Anchor")));
			this.txtOwner.AutoSize = ((bool)(resources.GetObject("txtOwner.AutoSize")));
			this.txtOwner.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("txtOwner.BackgroundImage")));
			this.txtOwner.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
			this.txtOwner.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("txtOwner.Dock")));
			this.txtOwner.Enabled = ((bool)(resources.GetObject("txtOwner.Enabled")));
			this.txtOwner.Font = ((System.Drawing.Font)(resources.GetObject("txtOwner.Font")));
			this.txtOwner.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("txtOwner.ImeMode")));
			this.txtOwner.Location = ((System.Drawing.Point)(resources.GetObject("txtOwner.Location")));
			this.txtOwner.MaxLength = ((int)(resources.GetObject("txtOwner.MaxLength")));
			this.txtOwner.Multiline = ((bool)(resources.GetObject("txtOwner.Multiline")));
			this.txtOwner.Name = "txtOwner";
			this.txtOwner.PasswordChar = ((char)(resources.GetObject("txtOwner.PasswordChar")));
			this.txtOwner.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("txtOwner.RightToLeft")));
			this.txtOwner.ScrollBars = ((System.Windows.Forms.ScrollBars)(resources.GetObject("txtOwner.ScrollBars")));
			this.txtOwner.Size = ((System.Drawing.Size)(resources.GetObject("txtOwner.Size")));
			this.txtOwner.TabIndex = ((int)(resources.GetObject("txtOwner.TabIndex")));
			this.txtOwner.Text = resources.GetString("txtOwner.Text");
			this.txtOwner.TextAlign = ((System.Windows.Forms.HorizontalAlignment)(resources.GetObject("txtOwner.TextAlign")));
			this.toolTip1.SetToolTip(this.txtOwner, resources.GetString("txtOwner.ToolTip"));
			this.txtOwner.Visible = ((bool)(resources.GetObject("txtOwner.Visible")));
			this.txtOwner.WordWrap = ((bool)(resources.GetObject("txtOwner.WordWrap")));
			this.txtOwner.TextChanged += new System.EventHandler(this.txtOwner_TextChanged);
			// 
			// lblOwner
			// 
			this.lblOwner.AccessibleDescription = resources.GetString("lblOwner.AccessibleDescription");
			this.lblOwner.AccessibleName = resources.GetString("lblOwner.AccessibleName");
			this.lblOwner.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("lblOwner.Anchor")));
			this.lblOwner.AutoSize = ((bool)(resources.GetObject("lblOwner.AutoSize")));
			this.lblOwner.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("lblOwner.Dock")));
			this.lblOwner.Enabled = ((bool)(resources.GetObject("lblOwner.Enabled")));
			this.lblOwner.Font = ((System.Drawing.Font)(resources.GetObject("lblOwner.Font")));
			this.lblOwner.Image = ((System.Drawing.Image)(resources.GetObject("lblOwner.Image")));
			this.lblOwner.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("lblOwner.ImageAlign")));
			this.lblOwner.ImageIndex = ((int)(resources.GetObject("lblOwner.ImageIndex")));
			this.lblOwner.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("lblOwner.ImeMode")));
			this.lblOwner.Location = ((System.Drawing.Point)(resources.GetObject("lblOwner.Location")));
			this.lblOwner.Name = "lblOwner";
			this.lblOwner.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("lblOwner.RightToLeft")));
			this.lblOwner.Size = ((System.Drawing.Size)(resources.GetObject("lblOwner.Size")));
			this.lblOwner.TabIndex = ((int)(resources.GetObject("lblOwner.TabIndex")));
			this.lblOwner.Text = resources.GetString("lblOwner.Text");
			this.lblOwner.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("lblOwner.TextAlign")));
			this.toolTip1.SetToolTip(this.lblOwner, resources.GetString("lblOwner.ToolTip"));
			this.lblOwner.Visible = ((bool)(resources.GetObject("lblOwner.Visible")));
			// 
			// txtAirlineCode
			// 
			this.txtAirlineCode.AccessibleDescription = resources.GetString("txtAirlineCode.AccessibleDescription");
			this.txtAirlineCode.AccessibleName = resources.GetString("txtAirlineCode.AccessibleName");
			this.txtAirlineCode.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("txtAirlineCode.Anchor")));
			this.txtAirlineCode.AutoSize = ((bool)(resources.GetObject("txtAirlineCode.AutoSize")));
			this.txtAirlineCode.BackColor = System.Drawing.SystemColors.Window;
			this.txtAirlineCode.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("txtAirlineCode.BackgroundImage")));
			this.txtAirlineCode.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
			this.txtAirlineCode.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("txtAirlineCode.Dock")));
			this.txtAirlineCode.Enabled = ((bool)(resources.GetObject("txtAirlineCode.Enabled")));
			this.txtAirlineCode.Font = ((System.Drawing.Font)(resources.GetObject("txtAirlineCode.Font")));
			this.txtAirlineCode.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("txtAirlineCode.ImeMode")));
			this.txtAirlineCode.InvalidTextBackColor = System.Drawing.Color.LightPink;
			this.txtAirlineCode.Location = ((System.Drawing.Point)(resources.GetObject("txtAirlineCode.Location")));
			this.txtAirlineCode.MaxLength = ((int)(resources.GetObject("txtAirlineCode.MaxLength")));
			this.txtAirlineCode.Multiline = ((bool)(resources.GetObject("txtAirlineCode.Multiline")));
			this.txtAirlineCode.Name = "txtAirlineCode";
			this.txtAirlineCode.PasswordChar = ((char)(resources.GetObject("txtAirlineCode.PasswordChar")));
			this.txtAirlineCode.Pattern = UserControls.TextBoxRegex.Patterns.RegexPattern;
			this.txtAirlineCode.PatternString = "[A-Z]{2,3}";
			this.txtAirlineCode.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("txtAirlineCode.RightToLeft")));
			this.txtAirlineCode.ScrollBars = ((System.Windows.Forms.ScrollBars)(resources.GetObject("txtAirlineCode.ScrollBars")));
			this.txtAirlineCode.Size = ((System.Drawing.Size)(resources.GetObject("txtAirlineCode.Size")));
			this.txtAirlineCode.TabIndex = ((int)(resources.GetObject("txtAirlineCode.TabIndex")));
			this.txtAirlineCode.Text = resources.GetString("txtAirlineCode.Text");
			this.txtAirlineCode.TextAlign = ((System.Windows.Forms.HorizontalAlignment)(resources.GetObject("txtAirlineCode.TextAlign")));
			this.toolTip1.SetToolTip(this.txtAirlineCode, resources.GetString("txtAirlineCode.ToolTip"));
			this.txtAirlineCode.UseColors = true;
			this.txtAirlineCode.UseInvalidTextException = true;
			this.txtAirlineCode.UseNumbersOnly = false;
			this.txtAirlineCode.ValidTextBackColor = System.Drawing.Color.LightGreen;
			this.txtAirlineCode.Visible = ((bool)(resources.GetObject("txtAirlineCode.Visible")));
			this.txtAirlineCode.WordWrap = ((bool)(resources.GetObject("txtAirlineCode.WordWrap")));
			this.txtAirlineCode.TextChanged += new System.EventHandler(this.txtAirlineCode_TextChanged);
			// 
			// txtFlightSuffix
			// 
			this.txtFlightSuffix.AccessibleDescription = resources.GetString("txtFlightSuffix.AccessibleDescription");
			this.txtFlightSuffix.AccessibleName = resources.GetString("txtFlightSuffix.AccessibleName");
			this.txtFlightSuffix.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("txtFlightSuffix.Anchor")));
			this.txtFlightSuffix.AutoSize = ((bool)(resources.GetObject("txtFlightSuffix.AutoSize")));
			this.txtFlightSuffix.BackColor = System.Drawing.SystemColors.Window;
			this.txtFlightSuffix.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("txtFlightSuffix.BackgroundImage")));
			this.txtFlightSuffix.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
			this.txtFlightSuffix.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("txtFlightSuffix.Dock")));
			this.txtFlightSuffix.Enabled = ((bool)(resources.GetObject("txtFlightSuffix.Enabled")));
			this.txtFlightSuffix.Font = ((System.Drawing.Font)(resources.GetObject("txtFlightSuffix.Font")));
			this.txtFlightSuffix.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("txtFlightSuffix.ImeMode")));
			this.txtFlightSuffix.InvalidTextBackColor = System.Drawing.Color.LightPink;
			this.txtFlightSuffix.Location = ((System.Drawing.Point)(resources.GetObject("txtFlightSuffix.Location")));
			this.txtFlightSuffix.MaxLength = ((int)(resources.GetObject("txtFlightSuffix.MaxLength")));
			this.txtFlightSuffix.Multiline = ((bool)(resources.GetObject("txtFlightSuffix.Multiline")));
			this.txtFlightSuffix.Name = "txtFlightSuffix";
			this.txtFlightSuffix.PasswordChar = ((char)(resources.GetObject("txtFlightSuffix.PasswordChar")));
			this.txtFlightSuffix.Pattern = UserControls.TextBoxRegex.Patterns.RegexPattern;
			this.txtFlightSuffix.PatternString = "[A-Z]{0,1}";
			this.txtFlightSuffix.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("txtFlightSuffix.RightToLeft")));
			this.txtFlightSuffix.ScrollBars = ((System.Windows.Forms.ScrollBars)(resources.GetObject("txtFlightSuffix.ScrollBars")));
			this.txtFlightSuffix.Size = ((System.Drawing.Size)(resources.GetObject("txtFlightSuffix.Size")));
			this.txtFlightSuffix.TabIndex = ((int)(resources.GetObject("txtFlightSuffix.TabIndex")));
			this.txtFlightSuffix.Text = resources.GetString("txtFlightSuffix.Text");
			this.txtFlightSuffix.TextAlign = ((System.Windows.Forms.HorizontalAlignment)(resources.GetObject("txtFlightSuffix.TextAlign")));
			this.toolTip1.SetToolTip(this.txtFlightSuffix, resources.GetString("txtFlightSuffix.ToolTip"));
			this.txtFlightSuffix.UseColors = true;
			this.txtFlightSuffix.UseInvalidTextException = true;
			this.txtFlightSuffix.UseNumbersOnly = false;
			this.txtFlightSuffix.ValidTextBackColor = System.Drawing.Color.LightGreen;
			this.txtFlightSuffix.Visible = ((bool)(resources.GetObject("txtFlightSuffix.Visible")));
			this.txtFlightSuffix.WordWrap = ((bool)(resources.GetObject("txtFlightSuffix.WordWrap")));
			this.txtFlightSuffix.TextChanged += new System.EventHandler(this.txtFlightSuffix_TextChanged);
			// 
			// txtDays
			// 
			this.txtDays.AccessibleDescription = resources.GetString("txtDays.AccessibleDescription");
			this.txtDays.AccessibleName = resources.GetString("txtDays.AccessibleName");
			this.txtDays.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("txtDays.Anchor")));
			this.txtDays.AutoSize = ((bool)(resources.GetObject("txtDays.AutoSize")));
			this.txtDays.BackColor = System.Drawing.SystemColors.Window;
			this.txtDays.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("txtDays.BackgroundImage")));
			this.txtDays.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("txtDays.Dock")));
			this.txtDays.Enabled = ((bool)(resources.GetObject("txtDays.Enabled")));
			this.txtDays.Font = ((System.Drawing.Font)(resources.GetObject("txtDays.Font")));
			this.txtDays.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("txtDays.ImeMode")));
			this.txtDays.InvalidTextBackColor = System.Drawing.Color.LightPink;
			this.txtDays.Location = ((System.Drawing.Point)(resources.GetObject("txtDays.Location")));
			this.txtDays.MaxLength = ((int)(resources.GetObject("txtDays.MaxLength")));
			this.txtDays.Multiline = ((bool)(resources.GetObject("txtDays.Multiline")));
			this.txtDays.Name = "txtDays";
			this.txtDays.PasswordChar = ((char)(resources.GetObject("txtDays.PasswordChar")));
			this.txtDays.Pattern = UserControls.TextBoxRegex.Patterns.RegexPattern;
			this.txtDays.PatternString = "[0-9]{1,7}";
			this.txtDays.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("txtDays.RightToLeft")));
			this.txtDays.ScrollBars = ((System.Windows.Forms.ScrollBars)(resources.GetObject("txtDays.ScrollBars")));
			this.txtDays.Size = ((System.Drawing.Size)(resources.GetObject("txtDays.Size")));
			this.txtDays.TabIndex = ((int)(resources.GetObject("txtDays.TabIndex")));
			this.txtDays.Text = resources.GetString("txtDays.Text");
			this.txtDays.TextAlign = ((System.Windows.Forms.HorizontalAlignment)(resources.GetObject("txtDays.TextAlign")));
			this.toolTip1.SetToolTip(this.txtDays, resources.GetString("txtDays.ToolTip"));
			this.txtDays.UseColors = true;
			this.txtDays.UseInvalidTextException = true;
			this.txtDays.UseNumbersOnly = false;
			this.txtDays.ValidTextBackColor = System.Drawing.Color.LightGreen;
			this.txtDays.Visible = ((bool)(resources.GetObject("txtDays.Visible")));
			this.txtDays.WordWrap = ((bool)(resources.GetObject("txtDays.WordWrap")));
			this.txtDays.TextChanged += new System.EventHandler(this.txtDays_TextChanged);
			// 
			// clbVia
			// 
			this.clbVia.AccessibleDescription = resources.GetString("clbVia.AccessibleDescription");
			this.clbVia.AccessibleName = resources.GetString("clbVia.AccessibleName");
			this.clbVia.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("clbVia.Anchor")));
			this.clbVia.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("clbVia.BackgroundImage")));
			this.clbVia.CheckOnClick = true;
			this.clbVia.ColumnWidth = ((int)(resources.GetObject("clbVia.ColumnWidth")));
			this.clbVia.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("clbVia.Dock")));
			this.clbVia.Enabled = ((bool)(resources.GetObject("clbVia.Enabled")));
			this.clbVia.Font = ((System.Drawing.Font)(resources.GetObject("clbVia.Font")));
			this.clbVia.HorizontalExtent = ((int)(resources.GetObject("clbVia.HorizontalExtent")));
			this.clbVia.HorizontalScrollbar = ((bool)(resources.GetObject("clbVia.HorizontalScrollbar")));
			this.clbVia.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("clbVia.ImeMode")));
			this.clbVia.IntegralHeight = ((bool)(resources.GetObject("clbVia.IntegralHeight")));
			this.clbVia.Location = ((System.Drawing.Point)(resources.GetObject("clbVia.Location")));
			this.clbVia.MultiColumn = true;
			this.clbVia.Name = "clbVia";
			this.clbVia.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("clbVia.RightToLeft")));
			this.clbVia.ScrollAlwaysVisible = ((bool)(resources.GetObject("clbVia.ScrollAlwaysVisible")));
			this.clbVia.Size = ((System.Drawing.Size)(resources.GetObject("clbVia.Size")));
			this.clbVia.Sorted = true;
			this.clbVia.TabIndex = ((int)(resources.GetObject("clbVia.TabIndex")));
			this.toolTip1.SetToolTip(this.clbVia, resources.GetString("clbVia.ToolTip"));
			this.clbVia.Visible = ((bool)(resources.GetObject("clbVia.Visible")));
			this.clbVia.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.clbVia_ItemCheck);
			// 
			// clbDestination
			// 
			this.clbDestination.AccessibleDescription = resources.GetString("clbDestination.AccessibleDescription");
			this.clbDestination.AccessibleName = resources.GetString("clbDestination.AccessibleName");
			this.clbDestination.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("clbDestination.Anchor")));
			this.clbDestination.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("clbDestination.BackgroundImage")));
			this.clbDestination.CheckOnClick = true;
			this.clbDestination.ColumnWidth = ((int)(resources.GetObject("clbDestination.ColumnWidth")));
			this.clbDestination.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("clbDestination.Dock")));
			this.clbDestination.Enabled = ((bool)(resources.GetObject("clbDestination.Enabled")));
			this.clbDestination.Font = ((System.Drawing.Font)(resources.GetObject("clbDestination.Font")));
			this.clbDestination.HorizontalExtent = ((int)(resources.GetObject("clbDestination.HorizontalExtent")));
			this.clbDestination.HorizontalScrollbar = ((bool)(resources.GetObject("clbDestination.HorizontalScrollbar")));
			this.clbDestination.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("clbDestination.ImeMode")));
			this.clbDestination.IntegralHeight = ((bool)(resources.GetObject("clbDestination.IntegralHeight")));
			this.clbDestination.Location = ((System.Drawing.Point)(resources.GetObject("clbDestination.Location")));
			this.clbDestination.MultiColumn = true;
			this.clbDestination.Name = "clbDestination";
			this.clbDestination.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("clbDestination.RightToLeft")));
			this.clbDestination.ScrollAlwaysVisible = ((bool)(resources.GetObject("clbDestination.ScrollAlwaysVisible")));
			this.clbDestination.Size = ((System.Drawing.Size)(resources.GetObject("clbDestination.Size")));
			this.clbDestination.Sorted = true;
			this.clbDestination.TabIndex = ((int)(resources.GetObject("clbDestination.TabIndex")));
			this.toolTip1.SetToolTip(this.clbDestination, resources.GetString("clbDestination.ToolTip"));
			this.clbDestination.Visible = ((bool)(resources.GetObject("clbDestination.Visible")));
			this.clbDestination.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.clbDestination_ItemCheck);
			// 
			// lblDestination
			// 
			this.lblDestination.AccessibleDescription = resources.GetString("lblDestination.AccessibleDescription");
			this.lblDestination.AccessibleName = resources.GetString("lblDestination.AccessibleName");
			this.lblDestination.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("lblDestination.Anchor")));
			this.lblDestination.AutoSize = ((bool)(resources.GetObject("lblDestination.AutoSize")));
			this.lblDestination.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("lblDestination.Dock")));
			this.lblDestination.Enabled = ((bool)(resources.GetObject("lblDestination.Enabled")));
			this.lblDestination.Font = ((System.Drawing.Font)(resources.GetObject("lblDestination.Font")));
			this.lblDestination.Image = ((System.Drawing.Image)(resources.GetObject("lblDestination.Image")));
			this.lblDestination.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("lblDestination.ImageAlign")));
			this.lblDestination.ImageIndex = ((int)(resources.GetObject("lblDestination.ImageIndex")));
			this.lblDestination.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("lblDestination.ImeMode")));
			this.lblDestination.Location = ((System.Drawing.Point)(resources.GetObject("lblDestination.Location")));
			this.lblDestination.Name = "lblDestination";
			this.lblDestination.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("lblDestination.RightToLeft")));
			this.lblDestination.Size = ((System.Drawing.Size)(resources.GetObject("lblDestination.Size")));
			this.lblDestination.TabIndex = ((int)(resources.GetObject("lblDestination.TabIndex")));
			this.lblDestination.Text = resources.GetString("lblDestination.Text");
			this.lblDestination.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("lblDestination.TextAlign")));
			this.toolTip1.SetToolTip(this.lblDestination, resources.GetString("lblDestination.ToolTip"));
			this.lblDestination.Visible = ((bool)(resources.GetObject("lblDestination.Visible")));
			// 
			// btnOrigin
			// 
			this.btnOrigin.AccessibleDescription = resources.GetString("btnOrigin.AccessibleDescription");
			this.btnOrigin.AccessibleName = resources.GetString("btnOrigin.AccessibleName");
			this.btnOrigin.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("btnOrigin.Anchor")));
			this.btnOrigin.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnOrigin.BackgroundImage")));
			this.btnOrigin.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("btnOrigin.Dock")));
			this.btnOrigin.Enabled = ((bool)(resources.GetObject("btnOrigin.Enabled")));
			this.btnOrigin.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("btnOrigin.FlatStyle")));
			this.btnOrigin.Font = ((System.Drawing.Font)(resources.GetObject("btnOrigin.Font")));
			this.btnOrigin.Image = ((System.Drawing.Image)(resources.GetObject("btnOrigin.Image")));
			this.btnOrigin.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnOrigin.ImageAlign")));
			this.btnOrigin.ImageIndex = ((int)(resources.GetObject("btnOrigin.ImageIndex")));
			this.btnOrigin.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("btnOrigin.ImeMode")));
			this.btnOrigin.Location = ((System.Drawing.Point)(resources.GetObject("btnOrigin.Location")));
			this.btnOrigin.Name = "btnOrigin";
			this.btnOrigin.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("btnOrigin.RightToLeft")));
			this.btnOrigin.Size = ((System.Drawing.Size)(resources.GetObject("btnOrigin.Size")));
			this.btnOrigin.TabIndex = ((int)(resources.GetObject("btnOrigin.TabIndex")));
			this.btnOrigin.Text = resources.GetString("btnOrigin.Text");
			this.btnOrigin.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnOrigin.TextAlign")));
			this.toolTip1.SetToolTip(this.btnOrigin, resources.GetString("btnOrigin.ToolTip"));
			this.btnOrigin.Visible = ((bool)(resources.GetObject("btnOrigin.Visible")));
			this.btnOrigin.Click += new System.EventHandler(this.btnOrigin_Click);
			// 
			// btnNature
			// 
			this.btnNature.AccessibleDescription = resources.GetString("btnNature.AccessibleDescription");
			this.btnNature.AccessibleName = resources.GetString("btnNature.AccessibleName");
			this.btnNature.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("btnNature.Anchor")));
			this.btnNature.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnNature.BackgroundImage")));
			this.btnNature.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("btnNature.Dock")));
			this.btnNature.Enabled = ((bool)(resources.GetObject("btnNature.Enabled")));
			this.btnNature.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("btnNature.FlatStyle")));
			this.btnNature.Font = ((System.Drawing.Font)(resources.GetObject("btnNature.Font")));
			this.btnNature.Image = ((System.Drawing.Image)(resources.GetObject("btnNature.Image")));
			this.btnNature.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnNature.ImageAlign")));
			this.btnNature.ImageIndex = ((int)(resources.GetObject("btnNature.ImageIndex")));
			this.btnNature.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("btnNature.ImeMode")));
			this.btnNature.Location = ((System.Drawing.Point)(resources.GetObject("btnNature.Location")));
			this.btnNature.Name = "btnNature";
			this.btnNature.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("btnNature.RightToLeft")));
			this.btnNature.Size = ((System.Drawing.Size)(resources.GetObject("btnNature.Size")));
			this.btnNature.TabIndex = ((int)(resources.GetObject("btnNature.TabIndex")));
			this.btnNature.Text = resources.GetString("btnNature.Text");
			this.btnNature.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnNature.TextAlign")));
			this.toolTip1.SetToolTip(this.btnNature, resources.GetString("btnNature.ToolTip"));
			this.btnNature.Visible = ((bool)(resources.GetObject("btnNature.Visible")));
			this.btnNature.Click += new System.EventHandler(this.btnNature_Click);
			// 
			// btnDestination
			// 
			this.btnDestination.AccessibleDescription = resources.GetString("btnDestination.AccessibleDescription");
			this.btnDestination.AccessibleName = resources.GetString("btnDestination.AccessibleName");
			this.btnDestination.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("btnDestination.Anchor")));
			this.btnDestination.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnDestination.BackgroundImage")));
			this.btnDestination.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("btnDestination.Dock")));
			this.btnDestination.Enabled = ((bool)(resources.GetObject("btnDestination.Enabled")));
			this.btnDestination.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("btnDestination.FlatStyle")));
			this.btnDestination.Font = ((System.Drawing.Font)(resources.GetObject("btnDestination.Font")));
			this.btnDestination.Image = ((System.Drawing.Image)(resources.GetObject("btnDestination.Image")));
			this.btnDestination.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnDestination.ImageAlign")));
			this.btnDestination.ImageIndex = ((int)(resources.GetObject("btnDestination.ImageIndex")));
			this.btnDestination.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("btnDestination.ImeMode")));
			this.btnDestination.Location = ((System.Drawing.Point)(resources.GetObject("btnDestination.Location")));
			this.btnDestination.Name = "btnDestination";
			this.btnDestination.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("btnDestination.RightToLeft")));
			this.btnDestination.Size = ((System.Drawing.Size)(resources.GetObject("btnDestination.Size")));
			this.btnDestination.TabIndex = ((int)(resources.GetObject("btnDestination.TabIndex")));
			this.btnDestination.Text = resources.GetString("btnDestination.Text");
			this.btnDestination.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnDestination.TextAlign")));
			this.toolTip1.SetToolTip(this.btnDestination, resources.GetString("btnDestination.ToolTip"));
			this.btnDestination.Visible = ((bool)(resources.GetObject("btnDestination.Visible")));
			this.btnDestination.Click += new System.EventHandler(this.btnDestination_Click);
			// 
			// btnAircraftType
			// 
			this.btnAircraftType.AccessibleDescription = resources.GetString("btnAircraftType.AccessibleDescription");
			this.btnAircraftType.AccessibleName = resources.GetString("btnAircraftType.AccessibleName");
			this.btnAircraftType.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("btnAircraftType.Anchor")));
			this.btnAircraftType.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnAircraftType.BackgroundImage")));
			this.btnAircraftType.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("btnAircraftType.Dock")));
			this.btnAircraftType.Enabled = ((bool)(resources.GetObject("btnAircraftType.Enabled")));
			this.btnAircraftType.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("btnAircraftType.FlatStyle")));
			this.btnAircraftType.Font = ((System.Drawing.Font)(resources.GetObject("btnAircraftType.Font")));
			this.btnAircraftType.Image = ((System.Drawing.Image)(resources.GetObject("btnAircraftType.Image")));
			this.btnAircraftType.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnAircraftType.ImageAlign")));
			this.btnAircraftType.ImageIndex = ((int)(resources.GetObject("btnAircraftType.ImageIndex")));
			this.btnAircraftType.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("btnAircraftType.ImeMode")));
			this.btnAircraftType.Location = ((System.Drawing.Point)(resources.GetObject("btnAircraftType.Location")));
			this.btnAircraftType.Name = "btnAircraftType";
			this.btnAircraftType.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("btnAircraftType.RightToLeft")));
			this.btnAircraftType.Size = ((System.Drawing.Size)(resources.GetObject("btnAircraftType.Size")));
			this.btnAircraftType.TabIndex = ((int)(resources.GetObject("btnAircraftType.TabIndex")));
			this.btnAircraftType.Text = resources.GetString("btnAircraftType.Text");
			this.btnAircraftType.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnAircraftType.TextAlign")));
			this.toolTip1.SetToolTip(this.btnAircraftType, resources.GetString("btnAircraftType.ToolTip"));
			this.btnAircraftType.Visible = ((bool)(resources.GetObject("btnAircraftType.Visible")));
			this.btnAircraftType.Click += new System.EventHandler(this.btnAircraftType_Click);
			// 
			// btnVia
			// 
			this.btnVia.AccessibleDescription = resources.GetString("btnVia.AccessibleDescription");
			this.btnVia.AccessibleName = resources.GetString("btnVia.AccessibleName");
			this.btnVia.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("btnVia.Anchor")));
			this.btnVia.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnVia.BackgroundImage")));
			this.btnVia.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("btnVia.Dock")));
			this.btnVia.Enabled = ((bool)(resources.GetObject("btnVia.Enabled")));
			this.btnVia.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("btnVia.FlatStyle")));
			this.btnVia.Font = ((System.Drawing.Font)(resources.GetObject("btnVia.Font")));
			this.btnVia.Image = ((System.Drawing.Image)(resources.GetObject("btnVia.Image")));
			this.btnVia.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnVia.ImageAlign")));
			this.btnVia.ImageIndex = ((int)(resources.GetObject("btnVia.ImageIndex")));
			this.btnVia.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("btnVia.ImeMode")));
			this.btnVia.Location = ((System.Drawing.Point)(resources.GetObject("btnVia.Location")));
			this.btnVia.Name = "btnVia";
			this.btnVia.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("btnVia.RightToLeft")));
			this.btnVia.Size = ((System.Drawing.Size)(resources.GetObject("btnVia.Size")));
			this.btnVia.TabIndex = ((int)(resources.GetObject("btnVia.TabIndex")));
			this.btnVia.Text = resources.GetString("btnVia.Text");
			this.btnVia.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnVia.TextAlign")));
			this.toolTip1.SetToolTip(this.btnVia, resources.GetString("btnVia.ToolTip"));
			this.btnVia.Visible = ((bool)(resources.GetObject("btnVia.Visible")));
			this.btnVia.Click += new System.EventHandler(this.btnVia_Click);
			// 
			// ucSelectionPage
			// 
			this.AccessibleDescription = resources.GetString("$this.AccessibleDescription");
			this.AccessibleName = resources.GetString("$this.AccessibleName");
			this.AutoScroll = ((bool)(resources.GetObject("$this.AutoScroll")));
			this.AutoScrollMargin = ((System.Drawing.Size)(resources.GetObject("$this.AutoScrollMargin")));
			this.AutoScrollMinSize = ((System.Drawing.Size)(resources.GetObject("$this.AutoScrollMinSize")));
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.Controls.Add(this.btnVia);
			this.Controls.Add(this.btnAircraftType);
			this.Controls.Add(this.btnDestination);
			this.Controls.Add(this.btnNature);
			this.Controls.Add(this.btnOrigin);
			this.Controls.Add(this.clbDestination);
			this.Controls.Add(this.lblDestination);
			this.Controls.Add(this.clbVia);
			this.Controls.Add(this.txtDays);
			this.Controls.Add(this.txtFlightSuffix);
			this.Controls.Add(this.txtAirlineCode);
			this.Controls.Add(this.lblOwner);
			this.Controls.Add(this.txtOwner);
			this.Controls.Add(this.clbAircraftTypes);
			this.Controls.Add(this.clbAirport);
			this.Controls.Add(this.clbNature);
			this.Controls.Add(this.cboHandlingType);
			this.Controls.Add(this.cboHandlingAgent);
			this.Controls.Add(this.grpStatus);
			this.Controls.Add(this.grpType);
			this.Controls.Add(this.lblHandlingType);
			this.Controls.Add(this.lblHandlingAgent);
			this.Controls.Add(this.txtStatisticId);
			this.Controls.Add(this.lblKeycode);
			this.Controls.Add(this.lblNature);
			this.Controls.Add(this.lblVia);
			this.Controls.Add(this.lblOrigin);
			this.Controls.Add(this.lblAircraftType);
			this.Controls.Add(this.txtRegistration);
			this.Controls.Add(this.lblRegistration);
			this.Controls.Add(this.txtFlightId);
			this.Controls.Add(this.lblFlightId);
			this.Controls.Add(this.txtCallsign);
			this.Controls.Add(this.lblCallsign);
			this.Controls.Add(this.cbCodeShare);
			this.Controls.Add(this.txtFlightNumber);
			this.Controls.Add(this.lblFlightNumber);
			this.Controls.Add(this.dtpFlicoFrom);
			this.Controls.Add(this.lblFlicoFrom);
			this.Controls.Add(this.dtpChangesFrom);
			this.Controls.Add(this.lblChangesFrom);
			this.Controls.Add(this.lblAfter);
			this.Controls.Add(this.updAfter);
			this.Controls.Add(this.lblBefore);
			this.Controls.Add(this.updBefore);
			this.Controls.Add(this.lblRelativePeriod);
			this.Controls.Add(this.lblCalculated);
			this.Controls.Add(this.lblDays);
			this.Controls.Add(this.dtpPeriodTo);
			this.Controls.Add(this.dtpPeriodFrom);
			this.Controls.Add(this.lblAbsolutePeriod);
			this.Controls.Add(this.grbOnGround);
			this.Controls.Add(this.grpMode);
			this.Enabled = ((bool)(resources.GetObject("$this.Enabled")));
			this.Font = ((System.Drawing.Font)(resources.GetObject("$this.Font")));
			this.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("$this.ImeMode")));
			this.Location = ((System.Drawing.Point)(resources.GetObject("$this.Location")));
			this.Name = "ucSelectionPage";
			this.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("$this.RightToLeft")));
			this.Size = ((System.Drawing.Size)(resources.GetObject("$this.Size")));
			this.toolTip1.SetToolTip(this, resources.GetString("$this.ToolTip"));
			this.Load += new System.EventHandler(this.ucSelectionPage_Load);
			((System.ComponentModel.ISupportInitialize)(this.updBefore)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.updAfter)).EndInit();
			this.grpType.ResumeLayout(false);
			this.grbOnGround.ResumeLayout(false);
			this.grpMode.ResumeLayout(false);
			this.grpStatus.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		private void label1_Click(object sender, System.EventArgs e)
		{
		
		}

		private void label1_Click_1(object sender, System.EventArgs e)
		{
		
		}

		private void ucSelectionPage_Load(object sender, System.EventArgs e)
		{
			this.initialized = false;

			this.initialized = true;

			UpdateData();
		}

		#region	implementation

		public ViewFilter	Filter
		{
			get
			{
				return this.filter;
			}
			set
			{
				this.filter = value;
			}
		}

		public ViewFilter.ViewSelectionRow View
		{
			get
			{
				return this.view;
			}

			set
			{
				this.view = value;
				UpdateData();
			}
		}


		private void UpdateData()
		{
			DefaultData();
			if (this.filter == null || this.view == null)			
				return;

			if (!this.initialized)
				return;

			if (this.view["PeriodFrom"] != System.DBNull.Value)
				this.dtpPeriodFrom.Value = this.view.PeriodFrom;

			if (this.view["PeriodTo"] != System.DBNull.Value)
				this.dtpPeriodTo.Value = this.view.PeriodTo;

			if (this.view["Days"] != System.DBNull.Value)
				this.txtDays.Text = this.view.Days;

			if (this.view["RelativeAfter"] != System.DBNull.Value)
				this.updBefore.Value = this.view.RelativeBefore;
			else
				this.updBefore.Value = 0;

			if (this.view["RelativeBefore"] != System.DBNull.Value)
				this.updAfter.Value = this.view.RelativeAfter;
			else
				this.updAfter.Value = 0;

			if (this.view["FlicoFrom"] != System.DBNull.Value)
				this.dtpFlicoFrom.Value = this.view.FlicoFrom;

			if (this.view["ChangesFrom"] != System.DBNull.Value)
				this.dtpChangesFrom.Value = this.view.ChangesFrom;

			if (this.view["AirlineCode"] != System.DBNull.Value)
				this.txtAirlineCode.Text = this.view.AirlineCode;
			else
				this.txtAirlineCode.Text = "";

			this.txtAirlineCode.Refresh();

			if (this.view["FlightNumber"] != System.DBNull.Value)
				this.txtFlightNumber.Text = this.view.FlightNumber;
			else
				this.txtFlightNumber.Text = "";

			this.txtFlightNumber.Refresh();

			if (this.view["FlightSuffix"] != System.DBNull.Value)
				this.txtFlightSuffix.Text = this.view.FlightSuffix;
			else
				this.txtFlightSuffix.Text = "";

			this.txtFlightSuffix.Refresh();

			if (this.view["IncludeCodeShare"] != System.DBNull.Value)
				this.cbCodeShare.Checked = this.view.IncludeCodeShare;
			else
				this.cbCodeShare.Checked = false;

			if (this.view["Callsign"] != System.DBNull.Value)
				this.txtCallsign.Text = this.view.Callsign;
			else
				this.txtCallsign.Text = "";

			if (this.view["Registration"] != System.DBNull.Value)
				this.txtRegistration.Text = this.view.Registration;
			else
				this.txtRegistration.Text = "";

			if (this.view["Owner"] != System.DBNull.Value)
				this.txtOwner.Text = this.view.Owner;
			else
				this.txtOwner.Text = "";

			// get aircraft types
			string selection = string.Format("ViewSelection_Id = {0}",this.view["ViewSelection_Id"]);
			ViewFilter.AircraftTypeRow[] aircraftTypeRows = (ViewFilter.AircraftTypeRow[])this.filter.AircraftType.Select(selection);
			if (aircraftTypeRows != null && aircraftTypeRows.Length != 0)
			{
				foreach(ViewFilter.AircraftTypeRow row in aircraftTypeRows)
				{
					if (row["Name"] != System.DBNull.Value)
					{
						int ind = this.clbAircraftTypes.Items.IndexOf(row["Name"]);
						if (ind >= 0)
						{
							this.clbAircraftTypes.SetItemChecked(ind,true);
						}
						else
						{
							row.Delete();
						}
					}
				}
			}


			// get origin
			selection = string.Format("ViewSelection_Id = {0}",this.view["ViewSelection_Id"]);
			ViewFilter.AirportRow[] airportRows = (ViewFilter.AirportRow[])this.filter.Airport.Select(selection);
			if (airportRows != null && airportRows.Length != 0)
			{
				foreach(ViewFilter.AirportRow row in airportRows)
				{
					if (row["Name"] != System.DBNull.Value)
					{
						int ind = this.clbAirport.Items.IndexOf(row["Name"]);
						if (ind >= 0)
						{
							this.clbAirport.SetItemChecked(ind,true);
						}
						else
						{
							row.Delete();
						}
					}
				}
			}

			// get destination
			selection = string.Format("ViewSelection_Id = {0}",this.view["ViewSelection_Id"]);
			ViewFilter.DestinationRow[] destRows = (ViewFilter.DestinationRow[])this.filter.Destination.Select(selection);
			if (destRows != null && destRows.Length != 0)
			{
				foreach(ViewFilter.DestinationRow row in destRows)
				{
					if (row["Name"] != System.DBNull.Value)
					{
						int ind = this.clbDestination.Items.IndexOf(row["Name"]);
						if (ind >= 0)
						{
							this.clbDestination.SetItemChecked(ind,true);
						}
						else
						{
							row.Delete();
						}
					}
				}
			}

			// get vias
			selection = string.Format("ViewSelection_Id = {0}",this.view["ViewSelection_Id"]);
			ViewFilter.ViaRow[] viaRows = (ViewFilter.ViaRow[])this.filter.Via.Select(selection);
			if (viaRows != null && viaRows.Length != 0)
			{
				foreach(ViewFilter.ViaRow row in viaRows)
				{
					if (row["Name"] != System.DBNull.Value)
					{
						int ind = this.clbVia.Items.IndexOf(row["Name"]);
						if (ind >= 0)
						{
							this.clbVia.SetItemChecked(ind,true);
						}
						else
						{
							row.Delete();
						}
					}
				}
			}

			// get natures
			selection = string.Format("ViewSelection_Id = {0}",this.view["ViewSelection_Id"]);
			ViewFilter.NatureRow[] natureRows = (ViewFilter.NatureRow[])this.filter.Nature.Select(selection);
			if (natureRows != null && natureRows.Length != 0)
			{
				foreach(ViewFilter.NatureRow row in natureRows)
				{
					if (row["Name"] != System.DBNull.Value)
					{
						int ind = this.clbNature.Items.IndexOf(row["Name"]);
						if (ind >= 0)
						{
							this.clbNature.SetItemChecked(ind,true);
						}
						else
						{
							row.Delete();
						}
					}
				}
			}

			// get flight type
			ViewFilter.FlightTypeRow[] flightTypeRows = (ViewFilter.FlightTypeRow[])this.filter.FlightType.Select(selection);
			if (flightTypeRows != null && flightTypeRows.Length != 0)
			{
				if (flightTypeRows[0]["Arrival"] != System.DBNull.Value)
					this.cbArrival.Checked = flightTypeRows[0].Arrival;
				else
					this.cbArrival.Checked = false;

				if (flightTypeRows[0]["Departure"] != System.DBNull.Value)
					this.cbDeparture.Checked = flightTypeRows[0].Departure;
				else
					this.cbDeparture.Checked = false;

				if (flightTypeRows[0]["Rotation"] != System.DBNull.Value)
					this.cbRotation.Checked = flightTypeRows[0].Rotation;
				else
					this.cbRotation.Checked = false;
			}

			// get flight mode
			ViewFilter.FlightModeRow[] flightModeRows = (ViewFilter.FlightModeRow[])this.filter.FlightMode.Select(selection);
			if (flightModeRows != null && flightModeRows.Length != 0)
			{
				if (flightModeRows[0]["Operation"] != System.DBNull.Value)
					this.cbOperational.Checked = flightModeRows[0].Operation;
				else
					this.cbOperational.Checked = false;

				if (flightModeRows[0]["Planning"] != System.DBNull.Value)
					this.cbPlanning.Checked = flightModeRows[0].Planning;
				else
					this.cbPlanning.Checked = false;

				if (flightModeRows[0]["Prognosis"] != System.DBNull.Value)
					this.cbPrognosis.Checked = flightModeRows[0].Prognosis;
				else
					this.cbPrognosis.Checked = false;
			}

			// get flight status
			ViewFilter.FlightStatusRow[] flightStatusRows = (ViewFilter.FlightStatusRow[])this.filter.FlightStatus.Select(selection);
			if (flightStatusRows != null && flightStatusRows.Length != 0)
			{
				if (flightStatusRows[0]["Cancelled"] != System.DBNull.Value)
					this.cbCancelled.Checked = flightStatusRows[0].Cancelled;
				else
					this.cbCancelled.Checked = false;

				if (flightStatusRows[0]["Noop"] != System.DBNull.Value)
					this.cbNoop.Checked = flightStatusRows[0].Noop;
				else
					this.cbNoop.Checked = false;

				if (flightStatusRows[0]["Diverted"] != System.DBNull.Value)
					this.cbDiverted.Checked = flightStatusRows[0].Diverted;
				else
					this.cbDiverted.Checked = false;

				if (flightStatusRows[0]["Rerouted"] != System.DBNull.Value)
					this.cbRerouted.Checked = flightStatusRows[0].Rerouted;
				else
					this.cbRerouted.Checked = false;
			}

			// get ground handling
			ViewFilter.GroundHandlingRow[] groundHandlingRows = (ViewFilter.GroundHandlingRow[])this.filter.GroundHandling.Select(selection);
			if (groundHandlingRows != null && groundHandlingRows.Length != 0)
			{
				if (groundHandlingRows[0]["Movement"] != System.DBNull.Value)
					this.cbGroundMovement.Checked = groundHandlingRows[0].Movement;
				else
					this.cbGroundMovement.Checked = false;

				if (groundHandlingRows[0]["Towing"] != System.DBNull.Value)
					this.cbTowing.Checked = groundHandlingRows[0].Towing;
				else
					this.cbTowing.Checked = false;

			}

			// get handling agent
			if (this.view["HandlingAgent"] != System.DBNull.Value)
			{
				this.cboHandlingAgent.Text = this.view.HandlingAgent;
			}
			else
			{
				this.cboHandlingAgent.Text = "";
			}
			this.cboHandlingAgent.Refresh();

			// get handling type
			if (this.view["HandlingType"] != System.DBNull.Value)
			{
				this.cboHandlingType.Text = this.view.HandlingType;
			}
			else
			{
				this.cboHandlingType.Text = "";
			}
			this.cboHandlingType.Refresh();

			// flight id
			if (this.view["FlightId"] != System.DBNull.Value)
				this.txtFlightId.Text = this.view.FlightId;
			else
				this.txtFlightId.Text = "";

			// statistical id
			if (this.view["StatisticId"] != System.DBNull.Value)
				this.txtStatisticId.Text = this.view.StatisticId;
			else
				this.txtStatisticId.Text = "";


		}

		private void DefaultData()
		{
			bool initialized = this.initialized;
			this.initialized = false;

			this.dtpPeriodFrom.Value	= null;
			this.dtpPeriodTo.Value		= null;

			this.txtDays.Text			= "";
			this.updBefore.Value		= 0;
			this.updAfter.Value			= 0;

			this.dtpChangesFrom.Value   = null;
			this.dtpFlicoFrom.Value		= null;

			this.txtAirlineCode.Text	= "";
			this.txtAirlineCode.Refresh();

			this.txtFlightNumber.Text	= "";
			this.txtFlightNumber.Refresh();

			this.txtFlightSuffix.Text	= "";
			this.txtFlightSuffix.Refresh();

			this.cbCodeShare.Checked	= false;

			this.txtCallsign.Text		= "";
			this.txtFlightId.Text		= "";
			this.txtStatisticId.Text	= "";

			this.txtRegistration.Text	= "";
			this.txtOwner.Text			= "";

			foreach (int checkedIndex in this.clbAircraftTypes.CheckedIndices)
			{
				this.clbAircraftTypes.SetItemChecked(checkedIndex,false);
			}

			foreach (int checkedIndex in this.clbAirport.CheckedIndices)
			{
				this.clbAirport.SetItemChecked(checkedIndex,false);
			}

			foreach (int checkedIndex in this.clbDestination.CheckedIndices)
			{
				this.clbDestination.SetItemChecked(checkedIndex,false);
			}

			foreach (int checkedIndex in this.clbVia.CheckedIndices)
			{
				this.clbVia.SetItemChecked(checkedIndex,false);
			}

			foreach (int checkedIndex in this.clbNature.CheckedIndices)
			{
				this.clbNature.SetItemChecked(checkedIndex,false);
			}


			this.cbArrival.Checked = false;
			this.cbDeparture.Checked = false;
			this.cbRotation.Checked = false;
			
			this.cbOperational.Checked = false;
			this.cbPlanning.Checked = false;
			this.cbPrognosis.Checked = false;
			
			this.cbCancelled.Checked = false;
			this.cbNoop.Checked = false;
			this.cbDiverted.Checked = false;
			this.cbRerouted.Checked = false;
			
			this.cbGroundMovement.Checked = false;
			this.cbTowing.Checked = false;

			this.cboHandlingAgent.Text = "";
			this.cboHandlingType.Text = "";
			this.initialized = initialized;
		}

		#endregion

		private void dtpPeriodFrom_ValueChanged(object sender, System.EventArgs e)
		{
			if (!this.initialized)
				return;

			if (this.view != null)
			{
				if (this.dtpPeriodFrom.Value == null)
					this.view["PeriodFrom"] = System.DBNull.Value;
				else
					this.view.PeriodFrom = (DateTime)this.dtpPeriodFrom.Value;
			}
		}

		private void clbAircraftTypes_ItemCheck(object sender, System.Windows.Forms.ItemCheckEventArgs e)
		{
			if (!this.initialized)
				return;

			if (e.NewValue == CheckState.Checked)
			{
				string name = (string)this.clbAircraftTypes.Items[e.Index];
				string selection = string.Format("ViewSelection_Id = {0} AND Name = '{1}'",this.view["ViewSelection_Id"],name);
				ViewFilter.AircraftTypeRow[] aircraftTypeRows = (ViewFilter.AircraftTypeRow[])this.filter.AircraftType.Select(selection);
				if (aircraftTypeRows == null || aircraftTypeRows.Length == 0)
				{
					ViewFilter.AircraftTypeRow row = this.filter.AircraftType.NewAircraftTypeRow();
					row.ViewSelectionRow = this.view;
					row.Name = name;
					this.filter.AircraftType.AddAircraftTypeRow(row);
				}
			}
			else if (e.NewValue == CheckState.Unchecked)
			{
				string name = (string)this.clbAircraftTypes.Items[e.Index];
				string selection = string.Format("ViewSelection_Id = {0} AND Name = '{1}'",this.view["ViewSelection_Id"],name);
				ViewFilter.AircraftTypeRow[] aircraftTypeRows = (ViewFilter.AircraftTypeRow[])this.filter.AircraftType.Select(selection);
				if (aircraftTypeRows != null && aircraftTypeRows.Length == 1)
				{
					aircraftTypeRows[0].Delete();
				}
			}
		}

		private void dtpPeriodTo_ValueChanged(object sender, System.EventArgs e)
		{
			if (!this.initialized)
				return;

			if (this.view != null)
			{
				if (this.dtpPeriodTo.Value == null)
					this.view["PeriodTo"] = System.DBNull.Value;
				else
					this.view.PeriodTo = (DateTime)this.dtpPeriodTo.Value;
			}
		}


		private void updBefore_ValueChanged(object sender, System.EventArgs e)
		{
			if (!this.initialized)
				return;

			if (this.view != null)
			{
				if (this.updBefore.Value == 0)
					this.view["RelativeBefore"] = System.DBNull.Value;
				else
					this.view.RelativeBefore = (int)this.updBefore.Value;
			}
		}

		private void updAfter_ValueChanged(object sender, System.EventArgs e)
		{
			if (!this.initialized)
				return;

			if (this.view != null)
			{
				if (this.updAfter.Value == 0)
					this.view["RelativeAfter"] = System.DBNull.Value;
				else
					this.view.RelativeAfter = (int)this.updAfter.Value;
			}
		}

		private void clbAirport_ItemCheck(object sender, System.Windows.Forms.ItemCheckEventArgs e)
		{
			if (!this.initialized)
				return;

			if (e.NewValue == CheckState.Checked)
			{
				string name = (string)this.clbAirport.Items[e.Index];
				string selection = string.Format("ViewSelection_Id = {0} AND Name = '{1}'",this.view["ViewSelection_Id"],name);
				ViewFilter.AirportRow[] airportRows = (ViewFilter.AirportRow[])this.filter.Airport.Select(selection);
				if (airportRows == null || airportRows.Length == 0)
				{
					ViewFilter.AirportRow row = this.filter.Airport.NewAirportRow();
					row.ViewSelectionRow = this.view;
					row.Name = name;
					this.filter.Airport.AddAirportRow(row);
				}
			}
			else if (e.NewValue == CheckState.Unchecked)
			{
				string name = (string)this.clbAirport.Items[e.Index];
				string selection = string.Format("ViewSelection_Id = {0} AND Name = '{1}'",this.view["ViewSelection_Id"],name);
				ViewFilter.AirportRow[] airportRows = (ViewFilter.AirportRow[])this.filter.Airport.Select(selection);
				if (airportRows != null && airportRows.Length == 1)
				{
					airportRows[0].Delete();
				}
			}
		
		}

		private void txtFlightNumber_TextChanged(object sender, System.EventArgs e)
		{
			if (!this.initialized)
				return;

			if (this.view != null)
			{
				if (this.txtFlightNumber.Text == "")
				{
					this.view["FlightNumber"] = System.DBNull.Value;

				}
				else
					this.view.FlightNumber = this.txtFlightNumber.Text;
			}
		}

		private void dtpFlicoFrom_ValueChanged(object sender, System.EventArgs e)
		{
			if (!this.initialized)
				return;

			if (this.view != null)
			{
				if (this.dtpFlicoFrom.Value == null)
					this.view["FlicoFrom"] = System.DBNull.Value;
				else
					this.view.FlicoFrom = (DateTime)this.dtpFlicoFrom.Value;
			}
		
		}

		private void dtpChangesFrom_ValueChanged(object sender, System.EventArgs e)
		{
			if (!this.initialized)
				return;

			if (this.view != null)
			{
				if (this.dtpChangesFrom.Value == null)
					this.view["ChangesFrom"] = System.DBNull.Value;
				else
					this.view.ChangesFrom = (DateTime)this.dtpChangesFrom.Value;
			}
		
		}

		private void clbNature_ItemCheck(object sender, System.Windows.Forms.ItemCheckEventArgs e)
		{
			if (!this.initialized)
				return;

			if (e.NewValue == CheckState.Checked)
			{
				string name = (string)this.clbNature.Items[e.Index];
				string selection = string.Format("ViewSelection_Id = {0} AND Name = '{1}'",this.view["ViewSelection_Id"],name);
				ViewFilter.NatureRow[] natureRows = (ViewFilter.NatureRow[])this.filter.Nature.Select(selection);
				if (natureRows == null || natureRows.Length == 0)
				{
					ViewFilter.NatureRow row = this.filter.Nature.NewNatureRow();
					row.ViewSelectionRow = this.view;
					row.Name = name;
					this.filter.Nature.AddNatureRow(row);
				}
			}
			else if (e.NewValue == CheckState.Unchecked)
			{
				string name = (string)this.clbNature.Items[e.Index];
				string selection = string.Format("ViewSelection_Id = {0} AND Name = '{1}'",this.view["ViewSelection_Id"],name);
				ViewFilter.NatureRow[] natureRows = (ViewFilter.NatureRow[])this.filter.Nature.Select(selection);
				if (natureRows != null && natureRows.Length == 1)
				{
					natureRows[0].Delete();
				}
			}
		
		}

		private void cbCodeShare_CheckedChanged(object sender, System.EventArgs e)
		{
			if (!this.initialized)
				return;

			if (this.view != null)
			{
				if (this.cbCodeShare.Checked == false)
					this.view["IncludeCodeShare"] = System.DBNull.Value;
				else
					this.view.IncludeCodeShare = this.cbCodeShare.Checked;
			}
		
		}

		private void cbArrival_CheckedChanged(object sender, System.EventArgs e)
		{
			if (!this.initialized)
				return;

			if (this.view != null)
			{
				string selection = string.Format("ViewSelection_Id = {0}",this.view["ViewSelection_Id"]);
				ViewFilter.FlightTypeRow[] flightTypeRows = (ViewFilter.FlightTypeRow[])this.filter.FlightType.Select(selection);
				if (flightTypeRows != null && flightTypeRows.Length != 0)
				{
					flightTypeRows[0].Arrival = this.cbArrival.Checked;
				}
				else
				{
					if (this.cbArrival.Checked == true)
					{
						ViewFilter.FlightTypeRow row = this.filter.FlightType.NewFlightTypeRow();
						row.Arrival = this.cbArrival.Checked;
						row.ViewSelectionRow = this.view;
						this.filter.FlightType.AddFlightTypeRow(row);
					}
				}
			}
		}

		private void cbDeparture_CheckedChanged(object sender, System.EventArgs e)
		{
			if (!this.initialized)
				return;

			if (this.view != null)
			{
				string selection = string.Format("ViewSelection_Id = {0}",this.view["ViewSelection_Id"]);
				ViewFilter.FlightTypeRow[] flightTypeRows = (ViewFilter.FlightTypeRow[])this.filter.FlightType.Select(selection);
				if (flightTypeRows != null && flightTypeRows.Length != 0)
				{
					flightTypeRows[0].Departure = this.cbDeparture.Checked;
				}
				else
				{
					if (this.cbDeparture.Checked == true)
					{
						ViewFilter.FlightTypeRow row = this.filter.FlightType.NewFlightTypeRow();
						row.Departure = this.cbDeparture.Checked;
						row.ViewSelectionRow = this.view;
						this.filter.FlightType.AddFlightTypeRow(row);
					}
				}
			}
		
		}

		private void cbRotation_CheckedChanged(object sender, System.EventArgs e)
		{
			if (!this.initialized)
				return;

			if (this.view != null)
			{
				string selection = string.Format("ViewSelection_Id = {0}",this.view["ViewSelection_Id"]);
				ViewFilter.FlightTypeRow[] flightTypeRows = (ViewFilter.FlightTypeRow[])this.filter.FlightType.Select(selection);
				if (flightTypeRows != null && flightTypeRows.Length != 0)
				{
					flightTypeRows[0].Rotation = this.cbRotation.Checked;
				}
				else
				{
					if (this.cbRotation.Checked == true)
					{
						ViewFilter.FlightTypeRow row = this.filter.FlightType.NewFlightTypeRow();
						row.Rotation = this.cbRotation.Checked;
						row.ViewSelectionRow = this.view;
						this.filter.FlightType.AddFlightTypeRow(row);
					}
				}
			}
		
		}

		private void cbOperational_CheckedChanged(object sender, System.EventArgs e)
		{
			if (!this.initialized)
				return;

			if (this.view != null)
			{
				string selection = string.Format("ViewSelection_Id = {0}",this.view["ViewSelection_Id"]);
				// get flight mode
				ViewFilter.FlightModeRow[] flightModeRows = (ViewFilter.FlightModeRow[])this.filter.FlightMode.Select(selection);
				if (flightModeRows != null && flightModeRows.Length != 0)
				{
					flightModeRows[0].Operation = this.cbOperational.Checked;
				}
				else
				{
					if (this.cbOperational.Checked == true)
					{
						ViewFilter.FlightModeRow row = this.filter.FlightMode.NewFlightModeRow();
						row.Operation = this.cbOperational.Checked;
						row.ViewSelectionRow = this.view;
						this.filter.FlightMode.AddFlightModeRow(row);

					}
				}
			}
	
		}

		private void cbPlanning_CheckedChanged(object sender, System.EventArgs e)
		{
			if (!this.initialized)
				return;

			if (this.view != null)
			{
				string selection = string.Format("ViewSelection_Id = {0}",this.view["ViewSelection_Id"]);
				// get flight mode
				ViewFilter.FlightModeRow[] flightModeRows = (ViewFilter.FlightModeRow[])this.filter.FlightMode.Select(selection);
				if (flightModeRows != null && flightModeRows.Length != 0)
				{
					flightModeRows[0].Planning = this.cbPlanning.Checked;
				}
				else
				{
					if (this.cbPlanning.Checked == true)
					{
						ViewFilter.FlightModeRow row = this.filter.FlightMode.NewFlightModeRow();
						row.Planning = this.cbPlanning.Checked;
						row.ViewSelectionRow = this.view;
						this.filter.FlightMode.AddFlightModeRow(row);

					}
				}
			}
		
		}

		private void cbPrognosis_CheckedChanged(object sender, System.EventArgs e)
		{
			if (!this.initialized)
				return;

			if (this.view != null)
			{
				string selection = string.Format("ViewSelection_Id = {0}",this.view["ViewSelection_Id"]);
				// get flight mode
				ViewFilter.FlightModeRow[] flightModeRows = (ViewFilter.FlightModeRow[])this.filter.FlightMode.Select(selection);
				if (flightModeRows != null && flightModeRows.Length != 0)
				{
					flightModeRows[0].Prognosis = this.cbPrognosis.Checked;
				}
				else
				{
					if (this.cbPrognosis.Checked == true)
					{
						ViewFilter.FlightModeRow row = this.filter.FlightMode.NewFlightModeRow();
						row.Prognosis = this.cbPrognosis.Checked;
						row.ViewSelectionRow = this.view;
						this.filter.FlightMode.AddFlightModeRow(row);

					}
				}
			}
		
		}

		private void cbCancelled_CheckedChanged(object sender, System.EventArgs e)
		{
			if (!this.initialized)
				return;

			if (this.view != null)
			{
				string selection = string.Format("ViewSelection_Id = {0}",this.view["ViewSelection_Id"]);
				// get flight status
				ViewFilter.FlightStatusRow[] flightStatusRows = (ViewFilter.FlightStatusRow[])this.filter.FlightStatus.Select(selection);
				if (flightStatusRows != null && flightStatusRows.Length != 0)
				{
					flightStatusRows[0].Cancelled = this.cbCancelled.Checked;
				}
				else
				{
					if (this.cbCancelled.Checked == true)
					{
						ViewFilter.FlightStatusRow row = this.filter.FlightStatus.NewFlightStatusRow();
						row.Cancelled = this.cbCancelled.Checked;
						row.ViewSelectionRow = this.view;
						this.filter.FlightStatus.AddFlightStatusRow(row);

					}
				}
			}
		
		}

		private void cbNoop_CheckedChanged(object sender, System.EventArgs e)
		{
			if (!this.initialized)
				return;

			if (this.view != null)
			{
				string selection = string.Format("ViewSelection_Id = {0}",this.view["ViewSelection_Id"]);
				// get flight status
				ViewFilter.FlightStatusRow[] flightStatusRows = (ViewFilter.FlightStatusRow[])this.filter.FlightStatus.Select(selection);
				if (flightStatusRows != null && flightStatusRows.Length != 0)
				{
					flightStatusRows[0].Noop = this.cbNoop.Checked;
				}
				else
				{
					if (this.cbNoop.Checked == true)
					{
						ViewFilter.FlightStatusRow row = this.filter.FlightStatus.NewFlightStatusRow();
						row.Noop = this.cbNoop.Checked;
						row.ViewSelectionRow = this.view;
						this.filter.FlightStatus.AddFlightStatusRow(row);

					}
				}
			}
		
		}

		private void cbDiverted_CheckedChanged(object sender, System.EventArgs e)
		{
			if (!this.initialized)
				return;

			if (this.view != null)
			{
				string selection = string.Format("ViewSelection_Id = {0}",this.view["ViewSelection_Id"]);
				// get flight status
				ViewFilter.FlightStatusRow[] flightStatusRows = (ViewFilter.FlightStatusRow[])this.filter.FlightStatus.Select(selection);
				if (flightStatusRows != null && flightStatusRows.Length != 0)
				{
					flightStatusRows[0].Diverted = this.cbDiverted.Checked;
				}
				else
				{
					if (this.cbDiverted.Checked == true)
					{
						ViewFilter.FlightStatusRow row = this.filter.FlightStatus.NewFlightStatusRow();
						row.Diverted = this.cbDiverted.Checked;
						row.ViewSelectionRow = this.view;
						this.filter.FlightStatus.AddFlightStatusRow(row);

					}
				}
			}
		
		}

		private void cbRerouted_CheckedChanged(object sender, System.EventArgs e)
		{
			if (!this.initialized)
				return;

			if (this.view != null)
			{
				string selection = string.Format("ViewSelection_Id = {0}",this.view["ViewSelection_Id"]);
				// get flight status
				ViewFilter.FlightStatusRow[] flightStatusRows = (ViewFilter.FlightStatusRow[])this.filter.FlightStatus.Select(selection);
				if (flightStatusRows != null && flightStatusRows.Length != 0)
				{
					flightStatusRows[0].Rerouted = this.cbRerouted.Checked;
				}
				else
				{
					if (this.cbRerouted.Checked == true)
					{
						ViewFilter.FlightStatusRow row = this.filter.FlightStatus.NewFlightStatusRow();
						row.Rerouted = this.cbRerouted.Checked;
						row.ViewSelectionRow = this.view;
						this.filter.FlightStatus.AddFlightStatusRow(row);

					}
				}
			}
		
		}

		private void cbGroundMovement_CheckedChanged(object sender, System.EventArgs e)
		{
			if (!this.initialized)
				return;

			if (this.view != null)
			{
				string selection = string.Format("ViewSelection_Id = {0}",this.view["ViewSelection_Id"]);
				// get flight status
				ViewFilter.GroundHandlingRow[] groundHandlingRows = (ViewFilter.GroundHandlingRow[])this.filter.GroundHandling.Select(selection);
				if (groundHandlingRows != null && groundHandlingRows.Length != 0)
				{
					groundHandlingRows[0].Movement = this.cbGroundMovement.Checked;
				}
				else
				{
					if (this.cbGroundMovement.Checked == true)
					{
						ViewFilter.GroundHandlingRow row = this.filter.GroundHandling.NewGroundHandlingRow();
						row.Movement = this.cbGroundMovement.Checked;
						row.ViewSelectionRow = this.view;
						this.filter.GroundHandling.AddGroundHandlingRow(row);

					}
				}
			}
		
		}

		private void cbTowing_CheckedChanged(object sender, System.EventArgs e)
		{
			if (!this.initialized)
				return;

			if (this.view != null)
			{
				string selection = string.Format("ViewSelection_Id = {0}",this.view["ViewSelection_Id"]);
				// get flight status
				ViewFilter.GroundHandlingRow[] groundHandlingRows = (ViewFilter.GroundHandlingRow[])this.filter.GroundHandling.Select(selection);
				if (groundHandlingRows != null && groundHandlingRows.Length != 0)
				{
					groundHandlingRows[0].Towing = this.cbTowing.Checked;
				}
				else
				{
					if (this.cbTowing.Checked == true)
					{
						ViewFilter.GroundHandlingRow row = this.filter.GroundHandling.NewGroundHandlingRow();
						row.Towing = this.cbTowing.Checked;
						row.ViewSelectionRow = this.view;
						this.filter.GroundHandling.AddGroundHandlingRow(row);

					}
				}
			}
		
		}

		private void cboHandlingAgent_TextChanged(object sender, System.EventArgs e)
		{
			if (!this.initialized)
				return;

			if (this.view != null)
			{
				if (this.cboHandlingAgent.Text == "")
				{
					this.view["HandlingAgent"] = System.DBNull.Value;

				}
				else
					this.view.HandlingAgent = this.cboHandlingAgent.Text;
			}
		}

		private void cboHandlingType_TextChanged(object sender, System.EventArgs e)
		{
			if (!this.initialized)
				return;

			if (this.view != null)
			{
				if (this.cboHandlingType.Text == "")
				{
					this.view["HandlingType"] = System.DBNull.Value;

				}
				else
					this.view.HandlingType = this.cboHandlingType.Text;
			}
		
		}

		private void txtAirlineCode_TextChanged(object sender, System.EventArgs e)
		{
			if (!this.initialized)
				return;

			if (this.view != null)
			{
				if (this.txtAirlineCode.Text == "")
				{
					this.view["AirlineCode"] = System.DBNull.Value;

				}
				else
					this.view.AirlineCode = this.txtAirlineCode.Text;
			}
		
		}

		private void txtFlightSuffix_TextChanged(object sender, System.EventArgs e)
		{
			if (!this.initialized)
				return;

			if (this.view != null)
			{
				if (this.txtFlightSuffix.Text == "")
				{
					this.view["FlightSuffix"] = System.DBNull.Value;

				}
				else
					this.view.FlightSuffix = this.txtFlightSuffix.Text;
			}
		
		}

		private void txtFlightId_TextChanged(object sender, System.EventArgs e)
		{
			if (!this.initialized)
				return;

			if (this.view != null)
			{
				if (this.txtFlightId.Text == "")
				{
					this.view["FlightId"] = System.DBNull.Value;

				}
				else
					this.view.FlightId = this.txtFlightId.Text;
			}
		
		}

		private void txtDays_TextChanged(object sender, System.EventArgs e)
		{
			if (!this.initialized)
				return;

			if (this.view != null)
			{
				if (this.txtDays.Text.Length == 0)
					this.view["Days"] = System.DBNull.Value;
				else
					this.view.Days = this.txtDays.Text;
			}
		
		}

		private void txtOwner_TextChanged(object sender, System.EventArgs e)
		{
			if (!this.initialized)
				return;

			if (this.view != null)
			{
				if (this.txtOwner.Text.Length == 0)
					this.view["Owner"] = System.DBNull.Value;
				else
					this.view.Owner = this.txtOwner.Text;
			}
		
		}

		private void textBox5_TextChanged(object sender, System.EventArgs e)
		{
			if (!this.initialized)
				return;

			if (this.view != null)
			{
				if (this.txtStatisticId.Text == "")
				{
					this.view["StatisticId"] = System.DBNull.Value;

				}
				else
					this.view.StatisticId = this.txtStatisticId.Text;
			}
		
		}

		private void clbVia_ItemCheck(object sender, System.Windows.Forms.ItemCheckEventArgs e)
		{
			if (!this.initialized)
				return;

			if (e.NewValue == CheckState.Checked)
			{
				string name = (string)this.clbVia.Items[e.Index];
				string selection = string.Format("ViewSelection_Id = {0} AND Name = '{1}'",this.view["ViewSelection_Id"],name);
				ViewFilter.ViaRow[] viaRows = (ViewFilter.ViaRow[])this.filter.Via.Select(selection);
				if (viaRows == null || viaRows.Length == 0)
				{
					ViewFilter.ViaRow row = this.filter.Via.NewViaRow();
					row.ViewSelectionRow = this.view;
					row.Name = name;
					this.filter.Via.AddViaRow(row);
				}
			}
			else if (e.NewValue == CheckState.Unchecked)
			{
				string name = (string)this.clbVia.Items[e.Index];
				string selection = string.Format("ViewSelection_Id = {0} AND Name = '{1}'",this.view["ViewSelection_Id"],name);
				ViewFilter.ViaRow[] viaRows = (ViewFilter.ViaRow[])this.filter.Via.Select(selection);
				if (viaRows != null && viaRows.Length == 1)
				{
					viaRows[0].Delete();
				}
			}
		
		}

		private void clbDestination_ItemCheck(object sender, System.Windows.Forms.ItemCheckEventArgs e)
		{
			if (!this.initialized)
				return;

			if (e.NewValue == CheckState.Checked)
			{
				string name = (string)this.clbDestination.Items[e.Index];
				string selection = string.Format("ViewSelection_Id = {0} AND Name = '{1}'",this.view["ViewSelection_Id"],name);
				ViewFilter.DestinationRow[] destRows = (ViewFilter.DestinationRow[])this.filter.Destination.Select(selection);
				if (destRows == null || destRows.Length == 0)
				{
					ViewFilter.DestinationRow row = this.filter.Destination.NewDestinationRow();
					row.ViewSelectionRow = this.view;
					row.Name = name;
					this.filter.Destination.AddDestinationRow(row);
				}
			}
			else if (e.NewValue == CheckState.Unchecked)
			{
				string name = (string)this.clbDestination.Items[e.Index];
				string selection = string.Format("ViewSelection_Id = {0} AND Name = '{1}'",this.view["ViewSelection_Id"],name);
				ViewFilter.DestinationRow[] destRows = (ViewFilter.DestinationRow[])this.filter.Destination.Select(selection);
				if (destRows != null && destRows.Length == 1)
				{
					destRows[0].Delete();
				}
			}
		
		}

		private void dtpChangesFrom_CloseUp(object sender, System.EventArgs e)
		{
			if (this.dtpChangesFrom.Value != null)
			{
				DateTime olDate = (DateTime)this.dtpChangesFrom.Value;
				this.dtpChangesFrom.Value = new DateTime(olDate.Year,olDate.Month,olDate.Day,0,0,0);
			}
		}

		private void dtpFlicoFrom_CloseUp(object sender, System.EventArgs e)
		{
			if (this.dtpFlicoFrom.Value != null)
			{
				DateTime olDate = (DateTime)this.dtpFlicoFrom.Value;
				this.dtpFlicoFrom.Value = new DateTime(olDate.Year,olDate.Month,olDate.Day,0,0,0);
			}
		}

		private void txtCallsign_TextChanged(object sender, System.EventArgs e)
		{
			if (!this.initialized)
				return;

			if (this.view != null)
			{
				if (this.txtCallsign.Text == "")
				{
					this.view["Callsign"] = System.DBNull.Value;

				}
				else
					this.view.Callsign = this.txtCallsign.Text;
			}
		
		}

		private void txtRegistration_TextChanged(object sender, System.EventArgs e)
		{
			if (!this.initialized)
				return;

			if (this.view != null)
			{
				if (this.txtRegistration.Text == "")
				{
					this.view["Registration"] = System.DBNull.Value;

				}
				else
					this.view.Registration = this.txtRegistration.Text;
			}
		
		}

		private void btnOrigin_Click(object sender, System.EventArgs e)
		{
			frmMultipleSelection dlg = new frmMultipleSelection();
			dlg.Source = this.clbAirport;
			dlg.ShowDialog(this);
		}

		private void btnNature_Click(object sender, System.EventArgs e)
		{
			frmMultipleSelection dlg = new frmMultipleSelection();
			dlg.Source = this.clbNature;
			dlg.ShowDialog(this);
		
		}

		private void btnDestination_Click(object sender, System.EventArgs e)
		{
			frmMultipleSelection dlg = new frmMultipleSelection();
			dlg.Source = this.clbDestination;
			dlg.ShowDialog(this);
		
		}

		private void btnAircraftType_Click(object sender, System.EventArgs e)
		{
			frmMultipleSelection dlg = new frmMultipleSelection();
			dlg.Source = this.clbAircraftTypes;
			dlg.ShowDialog(this);
		
		}

		private void btnVia_Click(object sender, System.EventArgs e)
		{
			frmMultipleSelection dlg = new frmMultipleSelection();
			dlg.Source = this.clbVia;
			dlg.ShowDialog(this);
		
		}

	}
}
