using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Text;

using System.Collections.Generic;

using Ufis.Utils;
using Ufis.Data;

using FIPS_Reports.Ctrl; //igu on 22/03/2011

namespace FIPS_Reports
{
	/// <summary>
	/// Summary description for frmDelayed_Flights.
	/// </summary>
	public class frmDelayed_Flights : System.Windows.Forms.Form
	{
		#region _My Members

		/// <summary>
		/// The where statement: all "@@xx" fields will be replaced
		/// with the true values according to the user's entry.
		/// </summary>
		//private string strWhereRaw = "WHERE (((((STOA BETWEEN '@@FROM' AND '@@TO') OR (TIFA BETWEEN '@@FROM' AND '@@TO')) AND DES3 = '@@HOPO') OR (((STOD BETWEEN '@@FROM' AND '@@TO') OR (TIFD BETWEEN '@@FROM' AND '@@TO')) AND ORG3 = '@@HOPO')) AND (FTYP = 'O'))";

		private string strWhereRaw = "WHERE (((STOA BETWEEN '@@FROM' AND '@@TO') AND DES3 = '@@HOPO' AND FTYP = 'O') OR ((TIFA BETWEEN '@@FROM' AND '@@TO') AND DES3 = '@@HOPO' AND FTYP = 'O') OR ((STOD BETWEEN '@@FROM' AND '@@TO') AND ORG3 = '@@HOPO' AND FTYP = 'O') OR ((TIFD BETWEEN '@@FROM' AND '@@TO') AND ORG3 = '@@HOPO' AND FTYP = 'O'))";
		
		

		private string strWhereDelayPart = " AND (DCD1='@@DEN' OR DCD2='@@DEN')";
		private string strWhereDelayQuery = "AND ((DCD1='@@DECA' OR DCD2='@@DECA') OR (DCD1='@@DECN' OR DCD2='@@DECN'))";
		private string strWhereDelay = "";
        private string strWhereAirlinePart = " AND (ALC2='@@ALC2')";
        private string strWhereAirline3Part = " AND (ALC3='@@ALC2')";
        private string strTerminalWhere = " AND (STEV = '@@STEV')";
		/// <summary>
		/// The logical field names of the tab control for internal 
		/// development purposes
		/// </summary>
		private string strLogicalFields = "FLNO,ADID,ORGDES,SCHEDULE,ACTUAL,ONOFBL,ACTYPE,DEL1,MIN1,DEL2,MIN2,DIFF,GATE1,NA,POS,TERM,REG,GATE2";
		/// <summary>
		/// The header columns, which must be used for the report output
		/// </summary>
		private string strTabHeader =     "Flight  ,A/D  ,Org/Des  ,Schedule  ,Actual ,Onbl/Ofbl ,A/C  ,Del1 ,Min1 ,Del2 ,Min2 ,Diff ,Gate1 ,NA ,POS ,Term,REG ,Gate2";

        /// <summary>
        /// The header columns, which must be used for the report output
        /// </summary>
        private const string TAB_HEADER_WITHOUT_DELAY_COLUMNS = "Flight  ,A/D  ,Org/Des  ,Schedule  ,Actual ,Onbl/Ofbl ,A/C  ,{0}Diff ,Gate1 ,NA ,POS ,{1}REG ,Gate2";
		/// <summary>
		/// The lengths, which must be used for the report's column widths
		/// </summary>
		private string strTabHeaderLens = "70,30,60,70,70,70,70,40,40,40,40,50,40,40,40,20,70,40";

        /// <summary>
        /// The lengths, which must be used for the report's column widths
        /// </summary>
        private const string TAB_HEADER_LENS_WO_DELAY_COLUMNS = "70,30,60,70,70,70,70,{0}50,40,40,40,{1}70,40";
        //private const string TAB_HEADER_LENS_FOR_DELAY_COLUMN_PAIR = "40,40,"; //igu on 28/01/2011
        private const string TAB_HEADER_LENS_FOR_DELAY_COLUMN_PAIR = "50,40,"; //igu on 28/01/2011
        private const string TAB_HEADER_LENS_FOR_SUBDELAY_COLUMN = "60,"; //igu on 22/03/2011
        private const string TAB_COLUMN_WIDTH_WO_DELAY_COLUMNS = "12,1,3,8,8,8,3,{0}4";
        private const string TAB_COLUMN_WIDTH_FOR_DELAY_COLUMN_PAIR = "2,4,";
        private const string TAB_COLUMN_WIDTH_FOR_SUBDELAY_COLUMN = "2,";//igu on 22/03/2011
		private const string TAB_COLUMN_ALIGNMENT_WO_DELAY_COLUMNS = "L,L,L,L,L,L,L,{0}R";
        private const string TAB_COLUMN_ALIGNMENT_FOR_DELAY_COLUMN_PAIR = "L,R,";
        private const string TAB_COLUMN_ALIGNMENT_FOR_SUBDELAY_COLUMN = "L,";//igu on 22/03/2011
        private const string TAB_LOGICAL_FIELDS_WO_DELAY_COLUMNS = "FLNO,ADID,ORGDES,SCHEDULE,ACTUAL,ONOFBL,ACTYPE,{0} DIFF,GATE1,NA,POS,{1} REG,GATE2";
        private const string TAB_LOGICAL_FIELDS_FOR_DELAY_COLUMN_PAIR = "DEL{0},MIN{0},";
        private const string TAB_LOGICAL_FIELDS_FOR_SUBDELAY_COLUMN = "SC{0},"; //igu on 22/03/2011
		/// <summary>
		/// The lengths, which must be used for the report's excel export column widths
		/// </summary>
		private string strExcelTabHeaderLens = "70,30,60,135,135,135,70,40,40,40,40,50,40,40,40,20,70,40";
        private const string TAB_EXCEL_HEADER_LENS_WO_DELAY_COLUMNS = "70,30,60,135,135,135,70,{0}50,40,40,40,{1}70,40";
		/// <summary>
		/// String for empty line generation in the tab control.
		/// </summary>
		private string strEmptyLine = ",,,,,,,,,,,,";
		/// <summary>
		/// The one and only IDatabase obejct.
		/// </summary>
		private IDatabase myDB;
		/// <summary>
		/// ITable object for the AFTTAB
		/// </summary>
		private ITable myAFT;
        /// <summary>
        /// ITable object for DCFTAB
        /// </summary>
        private ITable myDCF;
		/// <summary>
		/// ITable object for the nature codes of DENTAB
		/// </summary>
		private ITable myDEN;
		/// <summary>
		/// The nature code as global valid member.
		/// </summary>
		private string strDecn = "";
		/// <summary>
		/// Airline if filter was set
		/// </summary>
		private string strAlc2 = "";
		/*/// <summary>
		/// Read from Ceda.ini [FIPS] ==> SHOW_DELAYCODE=ALPHABETICAL
		/// </summary>
		private bool DelayAlpha = true;*/ //PRF 8930
		/// <summary>
		/// Total of arrival flights in the report
		/// </summary>
		private int imArrivals = 0;
		/// <summary>
		/// Total of departure flights in the report
		/// </summary>
		private int imDepartures = 0;
		/// <summary>
		/// Total of rotationein the report
		/// </summary>
		private int imRotations = 0;
		/// <summary>
		/// Total of all flights in the report
		/// </summary>
		private int imTotalFlights = 0;
		/// <summary>
		/// Datetime format for excel export
		/// </summary>
		private string strExcelDateTimeFormat = "";
		/// <summary>
		/// Datetime format for report
		/// </summary>
		private string strReportDateTimeFormat = "dd'/'HH:mm";
		/// <summary>
		/// Datetime format for table
		/// </summary>
		private string strDateDisplayFormat = "";
		/// <summary>
		/// Boolean for mouse status with respect to tabResult control
		/// </summary>
		private bool bmMouseInsideTabControl = false;
		/// <summary>
		/// Report Header
		/// </summary>
		private string strReportHeader = "";
		/// <summary>
		/// Report Sub Header
		/// </summary>
		private string strReportSubHeader = "";
		/// <summary>
		/// Report Object
		/// </summary>
		private DataDynamics.ActiveReports.ActiveReport3 activeReport = null;
		/// <summary>
		/// Boolean for showing stev
		/// </summary>
		private bool bmShowStev = false;

        /// <summary>
        /// Boolean for claclulate Actual Delay
        /// </summary>
        private bool bmCalculateActualDelay = true;

		//private string strDelayReasonWhereRaw = "WHERE ((STOA BETWEEN '@@FROM' AND '@@TO' AND DES3='@@DEST' ) OR ( STOD BETWEEN '@@FROM' AND '@@TO' AND ORG3='@@ORIG')) AND (FTYP='O' OR FTYP='S'))";	
		private string strDelayReasonLogicalFields = "DELCODE,DELRES,ALC,NOARRFL,NODEPFL";
		private string strDelayReasonWhereRawDEN = "WHERE (DECA = '@@DELCODE' OR DECN = '@@DELCODE')";
		/// <summary>
		/// The length of each column
		/// </summary>
		private string strDelayReasonTabHeaderLens = "80,150,60,130,130"; 
		private string strDelayReasonTabHeader = "Delay Code,Delay Reason,Airlines,No. of Arriv Flights,No. of Dept Flights";

        //igu on 26/01/2011
        /// <summary>
        /// use delay subcode??
        /// </summary>
        private bool bmUseDecs = false;

        //igu on 23/03/2011
        /// <summary>
        /// Max Delay Count
        /// </summary>
        private DelayCount mMaxDelayCnt = new DelayCount();
        private int imMaxDelayCnt = 2;
        FlightDelaysCollection flightDelaysCollection = new FlightDelaysCollection();

        //igu on 23/05/2011
        bool delayCodeChangesFromCode = false;
        bool autoPopulateDelayCodes = true;

        //igu on 31/05/2011
        bool rememberLastSortSetting = true;
        int sortColumnIndex = -1;
        bool sortAsc = true;
                    
		private class DelayCodeRowData
		{
			public string strDelayCode = "";
			public string strDelayReason = "";
			public string strAirline = "";
			public string strFlightNo = "";
			public int imNoOfArrivalFlights = 0;
			public int imNoOfDepartureFlights = 0;

            public string GetKey()
            {
                return GenerateKey(strAirline, strDelayCode );
            }

            public static string GenerateKey(string strAirline, string strDelayCode)
            {
                return string.Format("{0}--{1}", strAirline, strDelayCode);
            }

            public string ToString()
            {
                return string.Format("{0},{1},{2},{3},{4}",
                    strDelayCode,
                    strDelayReason,
                    strAirline,
                    imNoOfArrivalFlights,
                    imNoOfDepartureFlights);
            }
		}
		#endregion _My Members

		private System.Windows.Forms.Panel panelTop;
		private System.Windows.Forms.Panel panelBody;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.DateTimePicker dtFrom;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.DateTimePicker dtTo;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Panel panelTab;
		private AxTABLib.AxTAB tabResult;
		private AxTABLib.AxTAB tabExcelResult;
		private System.Windows.Forms.Button btnOK;
		private System.Windows.Forms.Button btnCancel;
		private System.Windows.Forms.ComboBox cbDelayCodes;
		private System.Windows.Forms.Label lblResults;
		private System.Windows.Forms.Button btnPrintPreview;
		private System.Windows.Forms.Button btnLoadPrint;
		private System.Windows.Forms.Label lblProgress;
		private System.Windows.Forms.ProgressBar progressBar1;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.TextBox txtAirline;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.NumericUpDown txtDelay;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.RadioButton radioButtonDelayReasons;
		private System.Windows.Forms.RadioButton radioButtonDelayedFlights;
		private System.Windows.Forms.Button buttonHelp;
		private System.Windows.Forms.Label lblTerminal;
        private System.Windows.Forms.TextBox txtTerminal;
        private AxTABLib.AxTAB tabLookup;
        private Panel pnlDelay;
        private Button btnDelayLookup;
        private TextBox txtDENA;
        private TextBox txtDECS;
        private TextBox txtDECN;
        private TextBox txtDECA;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public frmDelayed_Flights()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeMouseEvents();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//

            bmUseDecs = Ctrl.CtrlDelayedFlight.GetInstance().UseDelayCodeDecs; //igu on 26/01/2011
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmDelayed_Flights));
            this.panelTop = new System.Windows.Forms.Panel();
            this.tabLookup = new AxTABLib.AxTAB();
            this.pnlDelay = new System.Windows.Forms.Panel();
            this.btnDelayLookup = new System.Windows.Forms.Button();
            this.txtDENA = new System.Windows.Forms.TextBox();
            this.txtDECS = new System.Windows.Forms.TextBox();
            this.txtDECN = new System.Windows.Forms.TextBox();
            this.txtDECA = new System.Windows.Forms.TextBox();
            this.txtTerminal = new System.Windows.Forms.TextBox();
            this.lblTerminal = new System.Windows.Forms.Label();
            this.buttonHelp = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.radioButtonDelayReasons = new System.Windows.Forms.RadioButton();
            this.radioButtonDelayedFlights = new System.Windows.Forms.RadioButton();
            this.txtDelay = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.txtAirline = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.lblProgress = new System.Windows.Forms.Label();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.btnLoadPrint = new System.Windows.Forms.Button();
            this.btnPrintPreview = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnOK = new System.Windows.Forms.Button();
            this.cbDelayCodes = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.dtTo = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.dtFrom = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.panelBody = new System.Windows.Forms.Panel();
            this.panelTab = new System.Windows.Forms.Panel();
            this.tabResult = new AxTABLib.AxTAB();
            this.tabExcelResult = new AxTABLib.AxTAB();
            this.lblResults = new System.Windows.Forms.Label();
            this.panelTop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tabLookup)).BeginInit();
            this.pnlDelay.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtDelay)).BeginInit();
            this.panelBody.SuspendLayout();
            this.panelTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tabResult)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabExcelResult)).BeginInit();
            this.SuspendLayout();
            // 
            // panelTop
            // 
            this.panelTop.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.panelTop.Controls.Add(this.tabLookup);
            this.panelTop.Controls.Add(this.pnlDelay);
            this.panelTop.Controls.Add(this.txtTerminal);
            this.panelTop.Controls.Add(this.lblTerminal);
            this.panelTop.Controls.Add(this.buttonHelp);
            this.panelTop.Controls.Add(this.groupBox1);
            this.panelTop.Controls.Add(this.txtDelay);
            this.panelTop.Controls.Add(this.label5);
            this.panelTop.Controls.Add(this.txtAirline);
            this.panelTop.Controls.Add(this.label4);
            this.panelTop.Controls.Add(this.lblProgress);
            this.panelTop.Controls.Add(this.progressBar1);
            this.panelTop.Controls.Add(this.btnLoadPrint);
            this.panelTop.Controls.Add(this.btnPrintPreview);
            this.panelTop.Controls.Add(this.btnCancel);
            this.panelTop.Controls.Add(this.btnOK);
            this.panelTop.Controls.Add(this.cbDelayCodes);
            this.panelTop.Controls.Add(this.label3);
            this.panelTop.Controls.Add(this.dtTo);
            this.panelTop.Controls.Add(this.label2);
            this.panelTop.Controls.Add(this.dtFrom);
            this.panelTop.Controls.Add(this.label1);
            this.panelTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelTop.Location = new System.Drawing.Point(0, 0);
            this.panelTop.Name = "panelTop";
            this.panelTop.Size = new System.Drawing.Size(720, 168);
            this.panelTop.TabIndex = 0;
            this.panelTop.MouseEnter += new System.EventHandler(this.panelTop_MouseEnter);
            // 
            // tabLookup
            // 
            this.tabLookup.Location = new System.Drawing.Point(420, 80);
            this.tabLookup.Name = "tabLookup";
            this.tabLookup.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("tabLookup.OcxState")));
            this.tabLookup.Size = new System.Drawing.Size(208, 25);
            this.tabLookup.TabIndex = 7;
            this.tabLookup.TabStop = false;
            this.tabLookup.Visible = false;
            this.tabLookup.SendLButtonDblClick += new AxTABLib._DTABEvents_SendLButtonDblClickEventHandler(this.tabLookup_SendLButtonDblClick);
            // 
            // pnlDelay
            // 
            this.pnlDelay.Controls.Add(this.btnDelayLookup);
            this.pnlDelay.Controls.Add(this.txtDENA);
            this.pnlDelay.Controls.Add(this.txtDECS);
            this.pnlDelay.Controls.Add(this.txtDECN);
            this.pnlDelay.Controls.Add(this.txtDECA);
            this.pnlDelay.Location = new System.Drawing.Point(92, 34);
            this.pnlDelay.Name = "pnlDelay";
            this.pnlDelay.Size = new System.Drawing.Size(304, 20);
            this.pnlDelay.TabIndex = 6;
            // 
            // btnDelayLookup
            // 
            this.btnDelayLookup.Location = new System.Drawing.Point(279, 0);
            this.btnDelayLookup.Name = "btnDelayLookup";
            this.btnDelayLookup.Size = new System.Drawing.Size(25, 20);
            this.btnDelayLookup.TabIndex = 4;
            this.btnDelayLookup.TabStop = false;
            this.btnDelayLookup.Text = "...";
            this.btnDelayLookup.UseVisualStyleBackColor = true;
            this.btnDelayLookup.Click += new System.EventHandler(this.btnDelayLookup_Click);
            // 
            // txtDENA
            // 
            this.txtDENA.BackColor = System.Drawing.SystemColors.Control;
            this.txtDENA.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txtDENA.Location = new System.Drawing.Point(81, 0);
            this.txtDENA.Name = "txtDENA";
            this.txtDENA.ReadOnly = true;
            this.txtDENA.Size = new System.Drawing.Size(198, 20);
            this.txtDENA.TabIndex = 3;
            this.txtDENA.TabStop = false;
            // 
            // txtDECS
            // 
            this.txtDECS.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDECS.Location = new System.Drawing.Point(54, 0);
            this.txtDECS.MaxLength = 1;
            this.txtDECS.Name = "txtDECS";
            this.txtDECS.Size = new System.Drawing.Size(27, 20);
            this.txtDECS.TabIndex = 2;
            this.txtDECS.TextChanged += new System.EventHandler(this.txtDelayCode_TextChanged);
            // 
            // txtDECN
            // 
            this.txtDECN.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDECN.Location = new System.Drawing.Point(27, 0);
            this.txtDECN.MaxLength = 2;
            this.txtDECN.Name = "txtDECN";
            this.txtDECN.Size = new System.Drawing.Size(27, 20);
            this.txtDECN.TabIndex = 1;
            this.txtDECN.TextChanged += new System.EventHandler(this.txtDelayCode_TextChanged);
            // 
            // txtDECA
            // 
            this.txtDECA.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDECA.Location = new System.Drawing.Point(0, 0);
            this.txtDECA.MaxLength = 2;
            this.txtDECA.Name = "txtDECA";
            this.txtDECA.Size = new System.Drawing.Size(27, 20);
            this.txtDECA.TabIndex = 0;
            this.txtDECA.TextChanged += new System.EventHandler(this.txtDelayCode_TextChanged);
            // 
            // txtTerminal
            // 
            this.txtTerminal.Location = new System.Drawing.Point(92, 96);
            this.txtTerminal.Name = "txtTerminal";
            this.txtTerminal.Size = new System.Drawing.Size(100, 20);
            this.txtTerminal.TabIndex = 13;
            // 
            // lblTerminal
            // 
            this.lblTerminal.Location = new System.Drawing.Point(12, 100);
            this.lblTerminal.Name = "lblTerminal";
            this.lblTerminal.Size = new System.Drawing.Size(68, 23);
            this.lblTerminal.TabIndex = 12;
            this.lblTerminal.Text = "Terminal:";
            // 
            // buttonHelp
            // 
            this.buttonHelp.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.buttonHelp.Location = new System.Drawing.Point(396, 124);
            this.buttonHelp.Name = "buttonHelp";
            this.buttonHelp.Size = new System.Drawing.Size(75, 23);
            this.buttonHelp.TabIndex = 19;
            this.buttonHelp.Text = "Help";
            this.buttonHelp.Click += new System.EventHandler(this.buttonHelp_Click);
            this.buttonHelp.MouseEnter += new System.EventHandler(this.buttonHelp_MouseEnter);
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.groupBox1.Controls.Add(this.radioButtonDelayReasons);
            this.groupBox1.Controls.Add(this.radioButtonDelayedFlights);
            this.groupBox1.Location = new System.Drawing.Point(420, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(208, 72);
            this.groupBox1.TabIndex = 14;
            this.groupBox1.TabStop = false;
            // 
            // radioButtonDelayReasons
            // 
            this.radioButtonDelayReasons.Location = new System.Drawing.Point(20, 40);
            this.radioButtonDelayReasons.Name = "radioButtonDelayReasons";
            this.radioButtonDelayReasons.Size = new System.Drawing.Size(180, 24);
            this.radioButtonDelayReasons.TabIndex = 1;
            this.radioButtonDelayReasons.Text = "Statistics of Delay Reasons";
            this.radioButtonDelayReasons.CheckedChanged += new System.EventHandler(this.radioButtonDelayReasons_CheckedChanged);
            // 
            // radioButtonDelayedFlights
            // 
            this.radioButtonDelayedFlights.Checked = true;
            this.radioButtonDelayedFlights.Location = new System.Drawing.Point(20, 12);
            this.radioButtonDelayedFlights.Name = "radioButtonDelayedFlights";
            this.radioButtonDelayedFlights.Size = new System.Drawing.Size(180, 20);
            this.radioButtonDelayedFlights.TabIndex = 0;
            this.radioButtonDelayedFlights.TabStop = true;
            this.radioButtonDelayedFlights.Text = "Statistics of Delayed Flights";
            this.radioButtonDelayedFlights.CheckedChanged += new System.EventHandler(this.radioButtonDelayedFlights_CheckedChanged);
            // 
            // txtDelay
            // 
            this.txtDelay.Location = new System.Drawing.Point(296, 64);
            this.txtDelay.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.txtDelay.Name = "txtDelay";
            this.txtDelay.Size = new System.Drawing.Size(100, 20);
            this.txtDelay.TabIndex = 11;
            this.txtDelay.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtDelay.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(212, 66);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(80, 16);
            this.label5.TabIndex = 10;
            this.label5.Text = "Delay Time >=";
            // 
            // txtAirline
            // 
            this.txtAirline.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtAirline.Location = new System.Drawing.Point(92, 64);
            this.txtAirline.Name = "txtAirline";
            this.txtAirline.Size = new System.Drawing.Size(100, 20);
            this.txtAirline.TabIndex = 9;
            this.txtAirline.Enter += new System.EventHandler(this.txtAirline_Enter);
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(12, 68);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(72, 16);
            this.label4.TabIndex = 8;
            this.label4.Text = "Airline:";
            // 
            // lblProgress
            // 
            this.lblProgress.Location = new System.Drawing.Point(492, 108);
            this.lblProgress.Name = "lblProgress";
            this.lblProgress.Size = new System.Drawing.Size(212, 16);
            this.lblProgress.TabIndex = 20;
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(488, 124);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(216, 23);
            this.progressBar1.TabIndex = 21;
            this.progressBar1.Visible = false;
            // 
            // btnLoadPrint
            // 
            this.btnLoadPrint.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnLoadPrint.Location = new System.Drawing.Point(244, 124);
            this.btnLoadPrint.Name = "btnLoadPrint";
            this.btnLoadPrint.Size = new System.Drawing.Size(75, 23);
            this.btnLoadPrint.TabIndex = 17;
            this.btnLoadPrint.Text = "Loa&d + Print";
            this.btnLoadPrint.Click += new System.EventHandler(this.btnLoadPrint_Click);
            // 
            // btnPrintPreview
            // 
            this.btnPrintPreview.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnPrintPreview.Location = new System.Drawing.Point(168, 124);
            this.btnPrintPreview.Name = "btnPrintPreview";
            this.btnPrintPreview.Size = new System.Drawing.Size(75, 23);
            this.btnPrintPreview.TabIndex = 16;
            this.btnPrintPreview.Text = "&Print Preview";
            this.btnPrintPreview.Click += new System.EventHandler(this.btnPrintPreview_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnCancel.Location = new System.Drawing.Point(320, 124);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 18;
            this.btnCancel.Text = "&Close";
            // 
            // btnOK
            // 
            this.btnOK.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnOK.Location = new System.Drawing.Point(92, 124);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 15;
            this.btnOK.Text = "&Load Data";
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // cbDelayCodes
            // 
            this.cbDelayCodes.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbDelayCodes.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbDelayCodes.Location = new System.Drawing.Point(92, 34);
            this.cbDelayCodes.MaxDropDownItems = 16;
            this.cbDelayCodes.Name = "cbDelayCodes";
            this.cbDelayCodes.Size = new System.Drawing.Size(304, 24);
            this.cbDelayCodes.TabIndex = 5;
            this.cbDelayCodes.Visible = false;
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(12, 36);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(72, 16);
            this.label3.TabIndex = 4;
            this.label3.Text = "Delay Code:";
            // 
            // dtTo
            // 
            this.dtTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtTo.Location = new System.Drawing.Point(268, 4);
            this.dtTo.Name = "dtTo";
            this.dtTo.Size = new System.Drawing.Size(128, 20);
            this.dtTo.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(232, 8);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(24, 16);
            this.label2.TabIndex = 2;
            this.label2.Text = "to:";
            // 
            // dtFrom
            // 
            this.dtFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtFrom.Location = new System.Drawing.Point(92, 4);
            this.dtFrom.Name = "dtFrom";
            this.dtFrom.Size = new System.Drawing.Size(128, 20);
            this.dtFrom.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(12, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(64, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "Date from:";
            // 
            // panelBody
            // 
            this.panelBody.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.panelBody.Controls.Add(this.panelTab);
            this.panelBody.Controls.Add(this.lblResults);
            this.panelBody.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelBody.Location = new System.Drawing.Point(0, 168);
            this.panelBody.Name = "panelBody";
            this.panelBody.Size = new System.Drawing.Size(720, 398);
            this.panelBody.TabIndex = 1;
            // 
            // panelTab
            // 
            this.panelTab.Controls.Add(this.tabResult);
            this.panelTab.Controls.Add(this.tabExcelResult);
            this.panelTab.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelTab.Location = new System.Drawing.Point(0, 16);
            this.panelTab.Name = "panelTab";
            this.panelTab.Size = new System.Drawing.Size(720, 382);
            this.panelTab.TabIndex = 2;
            // 
            // tabResult
            // 
            this.tabResult.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabResult.Location = new System.Drawing.Point(0, 0);
            this.tabResult.Name = "tabResult";
            this.tabResult.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("tabResult.OcxState")));
            this.tabResult.Size = new System.Drawing.Size(720, 382);
            this.tabResult.TabIndex = 0;
            this.tabResult.SendLButtonDblClick += new AxTABLib._DTABEvents_SendLButtonDblClickEventHandler(this.tabResult_SendLButtonDblClick);
            // 
            // tabExcelResult
            // 
            this.tabExcelResult.Location = new System.Drawing.Point(0, 0);
            this.tabExcelResult.Name = "tabExcelResult";
            this.tabExcelResult.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("tabExcelResult.OcxState")));
            this.tabExcelResult.Size = new System.Drawing.Size(100, 50);
            this.tabExcelResult.TabIndex = 1;
            // 
            // lblResults
            // 
            this.lblResults.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblResults.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblResults.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.lblResults.Location = new System.Drawing.Point(0, 0);
            this.lblResults.Name = "lblResults";
            this.lblResults.Size = new System.Drawing.Size(720, 16);
            this.lblResults.TabIndex = 1;
            this.lblResults.Text = "Report Results";
            this.lblResults.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // frmDelayed_Flights
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(720, 566);
            this.Controls.Add(this.panelBody);
            this.Controls.Add(this.panelTop);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmDelayed_Flights";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Delayed Flights";
            this.Load += new System.EventHandler(this.frmDelayed_Flights_Load);
            this.Closing += new System.ComponentModel.CancelEventHandler(this.frmDelayed_Flights_Closing);
            this.HelpRequested += new System.Windows.Forms.HelpEventHandler(this.frmDelayed_Flights_HelpRequested);
            this.panelTop.ResumeLayout(false);
            this.panelTop.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tabLookup)).EndInit();
            this.pnlDelay.ResumeLayout(false);
            this.pnlDelay.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtDelay)).EndInit();
            this.panelBody.ResumeLayout(false);
            this.panelTab.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tabResult)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabExcelResult)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion
		
		/// <summary>
		/// Load the form. Prepare and init all controls.
		/// </summary>
		/// <param name="sender">Form.</param>
		/// <param name="e">Event arguments.</param>
		private void frmDelayed_Flights_Load(object sender, System.EventArgs e)
		{
			//SHOW_DELAYCODE=ALPHABETICAL
			/*Ufis.Utils.IniFile myIni = new IniFile("C:\\Ufis\\system\\ceda.ini");
			string strAlpha = myIni.IniReadValue("FIPS", "SHOW_DELAYCODE");
			if(strAlpha == "ALPHABETICAL")
				DelayAlpha = true;
			else
				DelayAlpha = false;*/ //Commented PRF 8930
			myDB = UT.GetMemDB();
			this.radioButtonDelayedFlights.Checked = true;
			InitTimePickers();
			SetupReportTabHeader();
			InitTab();
            InitLookupTab(); //igu on 26/01/2011
			PrepareFilterData();

            //igu on 23/05/2011
            IniFile myIni = new IniFile("C:\\Ufis\\System\\Ceda.ini");
            string strConfig = myIni.IniReadValue("FIPSREPORT", "AUTO_POPULATE_DELAY_CODES");
            if (string.IsNullOrEmpty(strConfig)) strConfig = "TRUE";
            switch (strConfig.ToUpper())
            {
                case "YES":
                case "TRUE":
                case "Y":
                    autoPopulateDelayCodes = true;
                    break;
                default:
                    autoPopulateDelayCodes = false;
                    break;
            }

            //igu on 31/05/2011
            strConfig = myIni.IniReadValue("FIPSREPORT", "DELAY_REPORT_REMEMBER_SORT");
            if (string.IsNullOrEmpty(strConfig)) strConfig = "TRUE";
            switch (strConfig.ToUpper())
            {
                case "YES":
                case "TRUE":
                case "Y":
                    rememberLastSortSetting = true;
                    break;
                default:
                    rememberLastSortSetting = false;
                    break;
            }

            btnDelayLookup.Tag = ""; ///igu on 31/05/2011
		}
		/// <summary>
		/// initializes the From/to time control with the current day in
		/// the custom format "dd.MM.yyyy - HH:mm".
		/// </summary>
		private void InitTimePickers()
		{
			DateTime olFrom;
			DateTime olTo;
			
			olFrom = DateTime.Now;
			olFrom = new DateTime(olFrom.Year, olFrom.Month, olFrom.Day, 0,0,0);
			olTo   = new DateTime(olFrom.Year, olFrom.Month, olFrom.Day, 23,59,0);
			dtFrom.Value = olFrom;
			dtTo.Value = olTo;
			dtFrom.CustomFormat = "dd.MM.yyyy - HH:mm";
			dtTo.CustomFormat = "dd.MM.yyyy - HH:mm";
		}
		/// <summary>
		/// Initializes the tab controls.
		/// </summary>
		private void InitTab()
		{
			tabResult.ResetContent();
			tabResult.ShowHorzScroller(true);
			tabResult.EnableHeaderSizing(true);
			tabResult.SetTabFontBold(true);
			tabResult.LifeStyle = true;
			tabResult.LineHeight = 16;
			tabResult.FontName = "Arial";
			tabResult.FontSize = 14;
			tabResult.HeaderFontSize = 14;
			tabResult.AutoSizeByHeader = true;

			if(this.radioButtonDelayedFlights.Checked == true)
			{
				tabResult.HeaderString = strTabHeader;
				tabResult.LogicalFieldList = strLogicalFields;
				tabResult.HeaderLengthString = strTabHeaderLens;
				tabResult.ColumnWidthString = "12,1,3,8,8,8,3,2,4,2,4,4";
				tabResult.ColumnAlignmentString = "L,L,L,L,L,L,L,L,R,L,R,R";

				tabExcelResult.ResetContent();
				tabExcelResult.HeaderString = strTabHeader;
				tabExcelResult.LogicalFieldList = strLogicalFields;
				tabExcelResult.HeaderLengthString = strTabHeaderLens;
			}
			else if(this.radioButtonDelayReasons.Checked == true)
			{
				tabResult.ResetContent();
				tabResult.ShowHorzScroller(true);
				tabResult.EnableHeaderSizing(true);
				tabResult.SetTabFontBold(true);
				tabResult.LifeStyle = true;
				tabResult.LineHeight = 16;
				tabResult.FontName = "Arial";
				tabResult.FontSize = 14;
				tabResult.HeaderFontSize = 14;
				tabResult.AutoSizeByHeader = true;
				tabResult.HeaderString = strDelayReasonTabHeader;
				tabResult.LogicalFieldList = strDelayReasonLogicalFields;
				tabResult.HeaderLengthString = strDelayReasonTabHeaderLens;				
			}

			IniFile myIni = new IniFile("C:\\Ufis\\System\\Ceda.ini");
			strDateDisplayFormat = myIni.IniReadValue("FIPSREPORT", "DATEDISPLAYFORMAT");
			strExcelDateTimeFormat = myIni.IniReadValue("FIPSREPORT", "DATEEXCELFORMAT");
			if(strDateDisplayFormat.Length == 0)
			{
				strDateDisplayFormat = strReportDateTimeFormat;
			}
			else
			{
				strReportDateTimeFormat = strDateDisplayFormat;
			}
			if(strExcelDateTimeFormat.Length == 0)
			{
				strExcelDateTimeFormat = strReportDateTimeFormat;
			}
             
                 if (myIni.IniReadValue("FIPSREPORT", "CALCULATE_ACTUAL_DELAY").Length > 0)
            {
                if (myIni.IniReadValue("FIPSREPORT", "CALCULATE_ACTUAL_DELAY").CompareTo("FALSE") == 0)
                {
                    bmCalculateActualDelay = false;

                }
            }
		}

        /// <summary>
        /// Initializes the tab controll.
        /// </summary>
        private void InitTab(string columnheaders, string logicalFields, string headerlens,string columnwidths,string columnalignments)
        {
            tabResult.ResetContent();
            tabResult.ShowHorzScroller(true);
            tabResult.EnableHeaderSizing(true);
            tabResult.SetTabFontBold(true);
            tabResult.LifeStyle = true;
            tabResult.LineHeight = 16;
            tabResult.FontName = "Arial";
            tabResult.FontSize = 14;
            tabResult.HeaderFontSize = 14;
            tabResult.AutoSizeByHeader = true;

            if (this.radioButtonDelayedFlights.Checked == true)
            {
                tabExcelResult.ResetContent();
                tabExcelResult.HeaderString = columnheaders;
                tabExcelResult.LogicalFieldList = logicalFields;
                tabExcelResult.HeaderLengthString = headerlens;

                //igu on 19/05/2011
                columnheaders += ",+";
                logicalFields += ",PLUS";
                headerlens += ",0";
                columnwidths += ",1";
                columnalignments += ",C";
                //

                tabResult.HeaderString = columnheaders;
                tabResult.LogicalFieldList = logicalFields;
                tabResult.HeaderLengthString = headerlens;
                tabResult.ColumnWidthString = columnwidths;
                tabResult.ColumnAlignmentString = columnalignments;
            }
            else if (this.radioButtonDelayReasons.Checked == true)
            {
                tabResult.ResetContent();
                tabResult.ShowHorzScroller(true);
                tabResult.EnableHeaderSizing(true);
                tabResult.SetTabFontBold(true);
                tabResult.LifeStyle = true;
                tabResult.LineHeight = 16;
                tabResult.FontName = "Arial";
                tabResult.FontSize = 14;
                tabResult.HeaderFontSize = 14;
                tabResult.AutoSizeByHeader = true;
                tabResult.HeaderString = strDelayReasonTabHeader;
                tabResult.LogicalFieldList = strDelayReasonLogicalFields;
                tabResult.HeaderLengthString = strDelayReasonTabHeaderLens;
            }

            IniFile myIni = new IniFile("C:\\Ufis\\System\\Ceda.ini");
            strDateDisplayFormat = myIni.IniReadValue("FIPSREPORT", "DATEDISPLAYFORMAT");
            strExcelDateTimeFormat = myIni.IniReadValue("FIPSREPORT", "DATEEXCELFORMAT");
            if (strDateDisplayFormat.Length == 0)
            {
                strDateDisplayFormat = strReportDateTimeFormat;
            }
            else
            {
                strReportDateTimeFormat = strDateDisplayFormat;
            }
            if (strExcelDateTimeFormat.Length == 0)
            {
                strExcelDateTimeFormat = strReportDateTimeFormat;
            }
        }

        /// <summary>
        /// Initializes the tab control for lookup delay data.
        /// </summary>
        private void InitLookupTab()
        {
            tabLookup.ResetContent();
            tabLookup.ShowHorzScroller(false);            
            tabLookup.FontName = "Arial";
            tabLookup.FontSize = 14;
            tabLookup.HeaderFontSize = 14;
            tabLookup.AutoSizeByHeader = true;

            //igu on 31/05/2011
            tabLookup.Left = pnlDelay.Left;
            tabLookup.Top = pnlDelay.Top + pnlDelay.Height;
            tabLookup.Width = this.Width - pnlDelay.Left - 10;
            tabLookup.Width = Math.Max(tabLookup.Width, pnlDelay.Width);
            tabLookup.Height = panelTop.Height - tabLookup.Top - 10;

            int totalHeaderLen = tabLookup.Width * 288 / 304;
            int lastColLen;
            if (bmUseDecs)
            {
                lastColLen = totalHeaderLen - (3* 27);
                tabLookup.LogicalFieldList = "URNO,DECA,DECN,DECS,DENA";
                tabLookup.HeaderString = "URNO,Al,Nu,SC,Description";                
                tabLookup.HeaderLengthString = "0,27,27,27," + lastColLen.ToString();
                tabLookup.ColumnAlignmentString = "L,L,L,L,L";
            }
            else
            {
                lastColLen = totalHeaderLen - (2 * 27);
                tabLookup.LogicalFieldList = "URNO,DECA,DECN,DENA";
                tabLookup.HeaderString = "URNO,Al,Nu,Description";
                tabLookup.HeaderLengthString = "0,27,27," + lastColLen.ToString();
                tabLookup.ColumnAlignmentString = "L,L,L,L";
            }
            //
        }

		/// <summary>
		/// Initializes the tab control header data.
		/// </summary>
		private void SetupReportTabHeader()
		{
			string[] strLabelHeaderEntry = new string[2];
			string strTerminalLabel = "";
			string strTerminalHeader = "";
			if(Helper.IsSTEVConfigured(this,ref strLabelHeaderEntry,true,true) == true)
			{
				strTerminalLabel  = strLabelHeaderEntry[0];
				strTerminalHeader = strLabelHeaderEntry[1];
				bmShowStev = true;
				lblTerminal.Text = strTerminalLabel + ":";
				Helper.RemoveOrReplaceString(15,ref strTabHeader,',',strTerminalHeader,false);				
			}
			else
			{
				Helper.RemoveOrReplaceString(15,ref strLogicalFields,',',"",true);
				Helper.RemoveOrReplaceString(15,ref strTabHeader,',',"",true);
				Helper.RemoveOrReplaceString(15,ref strTabHeaderLens,',',"",true);
				Helper.RemoveOrReplaceString(15,ref strExcelTabHeaderLens,',',"",true);
				lblTerminal.Hide();
				txtTerminal.Hide();
			}
		}
		/// <summary>
		/// Loads and prepare the data necessary for the filter. 
		/// In this case loads the nature codes and puts the
        /// data into the combobox.
        /// ----------------------
        /// igu on 26/01/2011
        /// we do not use combobox anymore, instead we use 4 textboxes
        /// for DECA, DECN, DECS and [DENA (readonly)]
        /// ----------------------
		/// </summary>
		private void PrepareFilterData()
		{
			Ufis.Utils.IniFile myIni = new IniFile("C:\\Ufis\\system\\ceda.ini");
			string strAlpha = myIni.IniReadValue("FIPS", "SHOW_DELAYCODE");

            //bool useDecs = Ctrl.CtrlDelayedFlight.GetInstance().UseDelayCodeDecs; igu on 26/01/2011
			
			myDB.Unbind("DEN");
			myDEN = myDB["DEN"];


            //if (useDecs) igu on 26/01/2011
            if (bmUseDecs) //igu on 26/01/2011
            {
                myDEN = myDB.Bind("DEN", "DEN", "DECA,DECN,DECS,DENA,ALC3,URNO", "2,2,2,75,4,10", "DECA,DECN,DECS,DENA,ALC3,URNO");
            }
            else
            {
                myDEN = myDB.Bind("DEN", "DEN", "DECA,DECN,DENA,URNO", "2,2,75,10", "DECA,DECN,DENA,URNO");
            }

            string strOrder = "ORDER BY "; //igu on 31/05/2011
			if(myDEN == null)
			{
				//myDEN = myDB.Bind("DEN", "DEN", "DECA,DECN,DECS,DENA", "2,2,2,75", "DECA,DECN,DECS,DENA");
				if(strAlpha == "ALPHABETICAL")
				{
                    //myDEN.Load("ORDER BY DECA"); //igu on 31/05/2011
                    strOrder += "DECA,DECN"; //igu on 31/05/2011
				}
				else if(strAlpha == "NUMERIC")
				{
                    //myDEN.Load("ORDER BY DECN"); //igu on 31/05/2011
                    strOrder += "DECN,DECA"; //igu on 31/05/2011
				}
				else
				{
                    //myDEN.Load(""); //igu on 31/05/2011
                    strOrder += "URNO"; //igu on 31/05/2011
				}
			}
			else if(myDEN.Count == 0)
			{
				//myDEN = myDB.Bind("DEN", "DEN", "DECA,DECN,DECS,DENA", "2,2,2,75", "DECA,DECN,DECS,DENA");
				if(strAlpha == "ALPHABETICAL")
				{
                    //myDEN.Load("ORDER BY DECA"); //igu on 31/05/2011
                    strOrder += "DECA,DECN"; //igu on 31/05/2011
				}
				else if(strAlpha == "NUMERIC")
				{
                    //myDEN.Load("ORDER BY DECN"); //igu on 31/05/2011
                    strOrder += "DECN,DECA"; //igu on 31/05/2011
				}
				else
				{
					//myDEN.Load("ORDER BY TTYP");
                    //myDEN.Load("ORDER BY DECA,DECN"); //igu on 31/05/2011
                    strOrder += "URNO"; //igu on 31/05/2011
				}
			}

            if (bmUseDecs) strOrder += ",DECS"; //igu on 31/05/2011
            myDEN.Load(strOrder); //igu on 31/05/2011

            myDEN.CreateIndex("DECA", "DECA"); //igu on 26/01/2011
            myDEN.CreateIndex("DECN", "DECN"); //igu on 26/01/2011
            myDEN.CreateIndex("URNO", "URNO"); //igu on 03/03/2011
            //igu on 26/01/2011
            //cbDelayCodes.Items.Add("<None>");
            //for (int i = 0; i < myDEN.Count; i++)
            //{
            //    string strValue = myDEN[i]["DECA"] + " | " + myDEN[i]["DECN"] + " | " +
            //        (useDecs ? (myDEN[i]["DECS"] + " | ") : "") +
            //        myDEN[i]["DENA"];
            //    cbDelayCodes.Items.Add(strValue);
            //}
            //if (cbDelayCodes.Items.Count > 0)
            //    cbDelayCodes.SelectedIndex = 0;

            if (!bmUseDecs)
            {
                txtDECS.Visible = false;
                txtDECA.Left = txtDECS.Left;
                txtDECA.Width += txtDECS.Width;
            }

            //igu on 20/05/2011
            //insert blank row at first row
            tabLookup.InsertTextLine("", false);

            for (int i = 0; i < myDEN.Count; i++)
            {
                //string strValue = myDEN[i]["DECA"] + "," + myDEN[i]["DECN"] + "," + //igu on 31/05/2011
                string strValue = myDEN[i]["URNO"] + "," +  //igu on 31/05/2011
                    myDEN[i]["DECA"] + "," + myDEN[i]["DECN"] + "," + //igu on 31/05/2011          
                    (bmUseDecs ? (myDEN[i]["DECS"] + ",") : "") +
                    myDEN[i]["DENA"];
                tabLookup.InsertTextLine(strValue, false);
            }
            tabLookup.IndexCreate("URNO", 0);
            tabLookup.Refresh();
            //
		}

        private void LoadReportDataNew()
        {
            progressBar1.Value = 0;
            lblProgress.Text = "Loading Data";
            lblProgress.Refresh();
            progressBar1.Show();

            /*igu on 26/01/2011
            string strDelays = cbDelayCodes.Text;
            string deca, decn, decs;
            deca = decn = decs = "";
            if (strDelays != "<None>")
            {
                string[] delays = strDelays.Split('|');
                deca = delays[0];
                decn = delays[1];
                decs = delays[2];
            }
            */
            //igu on 26/01/2011
            string deca = txtDECA.Text.Trim();
            string decn = txtDECN.Text.Trim();
            string decs = txtDECS.Text.Trim();
            //

            //igu on 14/06/2011
            if (deca != "" || decn != "" || decs != "")
            {
                strDecn = (string.IsNullOrEmpty(deca) ? " " : deca) + "|" +
                    (string.IsNullOrEmpty(decn) ? " " : decn) + "|" +
                    (string.IsNullOrEmpty(decs) ? " " : decs);
            }
            else
            {
                strDecn = ""; 
            }
            strAlc2 = txtAirline.Text;
            //

            long llTxtDelay = 0;
            if (txtDelay.Text.Length > 0)
            {
                llTxtDelay = Convert.ToInt64(txtDelay.Text);
            }

            Ctrl.CtrlDelayedFlight ctrl = Ctrl.CtrlDelayedFlight.GetInstance();
            ctrl.ProgressEvent += new FIPS_Reports.Ctrl.CtrlDelayedFlight.ProgressHandler(DoProgressEvent);
            Ctrl.CtrlDelayedFlight.GetInstance().LoadData(dtFrom.Value, dtTo.Value,
                deca, decn, decs,
                txtTerminal.Text.Trim(), txtAirline.Text.Trim(), llTxtDelay, bmCalculateActualDelay);

            lblProgress.Text = "";
            myAFT = myDB["AFT"];
            myDCF = myDB["DCF"];
            progressBar1.Hide();
        }

        void DoProgressEvent(int pctCompleted)
        {
            progressBar1.Value = pctCompleted;
        }

        private void DisplayData(bool loadData)
        {
            if (this.radioButtonDelayedFlights.Checked == true)
            {
                //if (Ctrl.CtrlDelayedFlight.GetInstance().UseDelayCodeDecs) //igu on 26/01/2011
                if (bmUseDecs) //igu on 26/01/2011
                {
                    if (loadData)
                    {
                        LoadReportDataNew();
                    }
                    PrepareReportDataNew("Report");
                    //PrepareReportDataNew("Excel");
                }
                else
                {
                    InitTab();
                    LoadReportData();
                    PrepareReportData("Report");
                    PrepareReportData("Excel");
                }
            }
            else if (this.radioButtonDelayReasons.Checked == true)
            {
                InitTab();
                //if (Ctrl.CtrlDelayedFlight.GetInstance().UseDelayCodeDecs) //igu on 26/01/2011
                if (bmUseDecs) //igu on 26/01/2011
                {
                    if (loadData)
                    {
                        LoadReportDataNew();
                    }
                }
                else
                {
                    LoadReportData();
                }
                //PrepareDelayReasonReportData();
                PrepareDelayReasonReportDataNew();
            }
        }

		/// <summary>
		/// Runs the data queries according to the reports needs and puts
		///	Calls the data prepare method and puts the prepared data into
		///	the tab control. After this is finished it calls the reports
		///	by sending the tab and the headers to the generic report.
		/// </summary>
		/// <param name="sender">Originator object of the call</param>
		/// <param name="e">Event arguments</param>
		private void btnOK_Click(object sender, System.EventArgs e)
		{
			string strError = ValidateUserEntry();
			if(strError != "")
			{
				MessageBox.Show(this, strError);
				return; // Do nothing due to errors
			}
			this.Cursor = Cursors.WaitCursor;
            DisplayData(true);
			this.Cursor = Cursors.Arrow;
		}
		/// <summary>
		/// Validates the user entry for nonsense.
		/// </summary>
		/// <returns></returns>
		private string ValidateUserEntry()
		{
			string strRet = "";
			int ilErrorCount = 1;


			//			TimeSpan span = dtTo.Value - dtFrom.Value;
			//			if(span.TotalDays > 10)
			//			{
			//				strRet += ilErrorCount.ToString() +  ". Please do not load more the 10 days!\n";
			//				ilErrorCount++;
			//			}
			if(dtFrom.Value >= dtTo.Value)
			{
				strRet += ilErrorCount.ToString() +  ". Date From is later than Date To!\n";
				ilErrorCount++;
			}
            //if (cbDelayCodes.Text == "") igu on 26/01/2011
            if ((txtDECA.Text.Trim() == "" && txtDECN.Text.Trim() == "" && txtDECS.Text.Trim() != "")) //igu on 26/01/2011
            {
                //strRet += ilErrorCount.ToString() + ". Please select a nature code!\n"; igu on 26/01/2011
                strRet += ilErrorCount.ToString() + ". Invalid delay code!\n"; //igu on 26/01/2011
                ilErrorCount++;
            }
			return strRet;
		}
		/// <summary>
		/// Loads all necessary tables with filter criteria from the 
		/// database ==> to memoryDB.
		/// </summary>
		private void LoadReportData()
		{
			string strTmpWhere = strWhereRaw;
			//Clear the tab
			tabResult.ResetContent();
			tabExcelResult.ResetContent();

			// In the first step change the times to UTC if this is
			// necessary.
			DateTime datFrom;
			DateTime datTo;
			string strDateFrom = "";
			string strDateTo = "";
			datFrom = dtFrom.Value;
			datTo   = dtTo.Value;
			if(UT.IsTimeInUtc == false)
			{
				datFrom = UT.LocalToUtc(dtFrom.Value);
				datTo   = UT.LocalToUtc(dtTo.Value);
				strDateFrom = UT.DateTimeToCeda( datFrom );
				strDateTo = UT.DateTimeToCeda( datTo );
			}
			else
			{
				strDateFrom = UT.DateTimeToCeda(dtFrom.Value);
				strDateTo = UT.DateTimeToCeda(dtTo.Value);
			}
			// Extract the true Nature code from the selected entry of the combobox
			strDecn = "";
			strWhereDelay = "";
            //igu on 26/01/2011
            //if(cbDelayCodes.Text != "<None>") 
            //{
            //    string [] strArr = cbDelayCodes.Text.Split('|');
            //    /*if(DelayAlpha == true)
            //        strDecn = strArr[0].Trim();
            //    else
            //        strDecn = strArr[1].Trim();*/
            //    if(strArr[0].Trim().Length > 0 && strArr[1].Trim().Length > 0)
            //    {
            //        strWhereDelay = strWhereDelayQuery;
            //        strWhereDelay = strWhereDelay.Replace("@@DECA", strArr[0].Trim());
            //        strWhereDelay = strWhereDelay.Replace("@@DECN", strArr[1].Trim());	
            //        strDecn = strArr[0].Trim() + "<" + strArr[1].Trim()+">";
            //    }
            //    else
            //    {
            //        strWhereDelay = strWhereDelayPart;
            //        if(strArr[0].Trim().Length > 0)
            //        {
            //            strWhereDelay = strWhereDelay.Replace("@@DEN", strArr[0].Trim());
            //            strDecn = strArr[0].Trim();
            //        }
            //        else if(strArr[1].Trim().Length > 0)
            //        {
            //            strWhereDelay = strWhereDelay.Replace("@@DEN", strArr[1].Trim());
            //            strDecn = strArr[1].Trim();
            //        }
            //    }
            //}
        
            //igu on 26/01/2011
            string deca = txtDECA.Text.Trim();
            string decn = txtDECN.Text.Trim();
            string decs = txtDECS.Text.Trim();

            if (deca != "" || decn != "" || decs != "")
            {
                strDecn = (string.IsNullOrEmpty(deca) ? " " : deca) + "|" +
                    (string.IsNullOrEmpty(decn) ? " " : decn) + "|" +
                    (string.IsNullOrEmpty(decs) ? " " : decs); //igu on 14/06/2011
                if (deca.Length > 0 && decn.Length > 0)
                {
                    strWhereDelay = strWhereDelayQuery;
                    strWhereDelay = strWhereDelay.Replace("@@DECA", deca);
                    strWhereDelay = strWhereDelay.Replace("@@DECN", decn);
                    //strDecn = deca + "<" + decn + ">"; //igu on 14/06/2011
                }
                else
                {
                    strWhereDelay = strWhereDelayPart;
                    if (deca.Length > 0)
                    {
                        strWhereDelay = strWhereDelay.Replace("@@DEN", deca);
                        //strDecn = deca; //igu on 14/06/2011
                    }
                    else if (decn.Length > 0)
                    {
                        strWhereDelay = strWhereDelay.Replace("@@DEN", decn);
                        //strDecn = decn; //igu on 14/06/2011
                    }
                }
            }
            else
            {
                strDecn = ""; //igu on 14/06/2011
            }
            //
			strAlc2 = txtAirline.Text;

			//Load the data from AFTTAB
			myDB.Unbind("AFT");
			myAFT = myDB.Bind("AFT", "AFT", "FLNO,ADID,ORG3,DES3,STOA,STOD,TIFA,TIFD,ONBL,OFBL,ACT3,DCD1,DCD2,DTD1,DTD2,GTA1,GTA2,GTD1,GTD2,REGN,PSTA,PSTD,TTYP,LAND,AIRB,ALC2,ALC3,STEV", "12,2,3,3,14,14,14,14,14,14,3,2,2,4,4,5,5,5,5,12,5,5,5,14,14,2,3,1", "FLNO,ADID,ORG3,DES3,STOA,STOD,TIFA,TIFD,ONBL,OFBL,ACT3,DCD1,DCD2,DTD1,DTD2,GTA1,GTA2,GTD1,GTD2,REGN,PSTA,PSTD,TTYP,LAND,AIRB,ALC2,ALC3,STEV");
			myAFT.Clear();
			myAFT.TimeFields = "STOA,STOD,TIFA,TIFD,ONBL,OFBL,LAND,AIRB";			
			/*ArrayList myUniques = new ArrayList();
			myUniques.Add("URNO");
			myAFT.UniqueFields = myUniques;*/
			myAFT.TimeFieldsInitiallyInUtc = true;
			//Prepare loop reading day by day
			string strTerminal = "";
			if(bmShowStev == true)
			{
				strTerminal = txtTerminal.Text.Trim();
			}
			DateTime datReadFrom = datFrom;
			DateTime datReadTo;
			TimeSpan tsDays = (datTo - datFrom);
			progressBar1.Value = 0;
			int ilTotal = Convert.ToInt32(tsDays.TotalDays);
			if(ilTotal == 0) ilTotal = 1;
			lblProgress.Text = "Loading Data";
			lblProgress.Refresh();
			progressBar1.Show();
			int loopCnt = 1;
			do
			{
				int percent = Convert.ToInt32((loopCnt * 100)/ilTotal);
				if (percent > 100) 
					percent = 100;
				progressBar1.Value = percent;
				datReadTo = datReadFrom + new TimeSpan(1, 0, 0, 0, 0);
				if( datReadTo > datTo) datReadTo = datTo;
				strDateFrom = UT.DateTimeToCeda(datReadFrom);
				strDateTo = UT.DateTimeToCeda(datReadTo);
				//patch the where statement according to the user's entry
				strTmpWhere = strWhereRaw;

                if (strAlc2 != "" && strAlc2.Length == 2)
                    strTmpWhere += strWhereAirlinePart;
                else
                {
                    if (strAlc2 != "" && strAlc2.Length == 3)
                          strTmpWhere += strWhereAirline3Part;
                 }
				//if(strDecn != "") strTmpWhere += strWhereDelayPart;
				if(strWhereDelay.Length > 0) strTmpWhere += strWhereDelay; //PRF 8930
				if(strTerminal.Length > 0) strTmpWhere += strTerminalWhere;
				strTmpWhere = strTmpWhere.Replace("@@FROM", strDateFrom);
				strTmpWhere = strTmpWhere.Replace("@@TO", strDateTo );
				//strTmpWhere = strTmpWhere.Replace("@@DEN", strDecn  );
				strTmpWhere = strTmpWhere.Replace("@@ALC2", strAlc2 );
				strTmpWhere = strTmpWhere.Replace("@@HOPO", UT.Hopo);
				strTmpWhere = strTmpWhere.Replace("@@STEV",strTerminal);

				//Load the data from AFTTAB
				myAFT.Load(strTmpWhere);
				datReadFrom += new TimeSpan(1, 0, 0, 0, 0);
				loopCnt++;
			}while(datReadFrom <= datReadTo);
			myAFT.TimeFieldsCurrentlyInUtc = UT.IsTimeInUtc;
			lblProgress.Text = "";
			progressBar1.Hide();


		}
		/// <summary>
		/// Prepare the loaded data according to the report's needs.
		/// Prepare line by line the data a logical records and put
		/// the data into the Tab control.
		/// </summary>
		private void PrepareReportData(string strReportOrExcel)
		{
			//"FLNO,ADID,ORGDES,SCHEDULE,ACTUAL,ACTYPE"
			imArrivals = 0;
			imDepartures =0 ;
			imRotations = 0;
			imTotalFlights = 0;

			//FLNO,ADID,ORGDES,SCHEDULE,ACTUAL,ONOFBL,ACTYPE,DEL1,MIN1,DEL2,MIN2,DIFF
			StringBuilder sb = new StringBuilder(10000);	
				
			string strDateTimeFormat = strReportDateTimeFormat;
			if(strReportOrExcel.Equals("Excel") == true)
			{
				strDateTimeFormat = strExcelDateTimeFormat;
			}

			for(int i = 0; i < myAFT.Count; i++)
			{
				string strValues = "";
                IRow myAFTRow = myAFT[i];
				string strAdid = myAFTRow["ADID"];

                TimeSpan olDelayDTD;
                int ilHoursDTD;
                int ilMinutesDTD;
                
                if(strAdid == "A")
				{
					long llDelay = 0;
                    
                    //if((myAFTRow["DCD1"] != "" || myAFTRow["DCD2"] != "" || myAFTRow["DTD1"] != "" || myAFTRow["DTD2"] != "") &&// ||
					if(myAFTRow["STOA"] != "" && myAFTRow["ONBL"] != "" && myAFTRow["STOA"] != myAFTRow["ONBL"])// )
					{
						DateTime olStoa = UT.CedaFullDateToDateTime(myAFTRow["STOA"]);
						DateTime olOnbl = UT.CedaFullDateToDateTime(myAFTRow["ONBL"]);
						TimeSpan olDelay = olOnbl - olStoa;
                        int ilHours = olDelay.Hours;
                        int ilMinutes = olDelay.Minutes;

						if(txtDelay.Text.Length == 0)
							txtDelay.Text = "0";			
						long llTxtDelay = Convert.ToInt64(txtDelay.Text);
						llDelay = Convert.ToInt64(olDelay.TotalMinutes);
						if( llDelay >= llTxtDelay)
						{
							imArrivals++;
							imTotalFlights++;
							strValues += myAFTRow["FLNO"] + ",";
							strValues += myAFTRow["ADID"] + ",";
							strValues += myAFTRow["ORG3"] + ",";
							strValues += Helper.DateString(myAFTRow["STOA"], strDateTimeFormat) + ",";
							strValues += Helper.DateString(myAFTRow["LAND"], strDateTimeFormat) + ",";
							strValues += Helper.DateString(myAFTRow["ONBL"], strDateTimeFormat) + ",";
							strValues += myAFTRow["ACT3"] + ",";
							strValues += myAFTRow["DCD1"] + ",";
                            if (myAFTRow["DTD1"] == "")
                                olDelayDTD = new TimeSpan(0, 0, 0);
                            else
                                olDelayDTD      = new TimeSpan(0, Convert.ToInt32(myAFTRow["DTD1"]), 0);
                            ilHoursDTD = olDelayDTD.Hours;
                            ilMinutesDTD = olDelayDTD.Minutes;
                            strValues += ilHoursDTD.ToString("D2") + "." + ilMinutesDTD.ToString("D2") + ",";
                            //  strValues += myAFTRow["DTD1"] + ",";
							
                            strValues += myAFTRow["DCD2"] + ",";

                            if (myAFTRow["DTD2"] == "")
                                olDelayDTD = new TimeSpan(0, 0, 0);
                            else
                                olDelayDTD = new TimeSpan(0, Convert.ToInt32(myAFTRow["DTD2"]), 0);

                          

                            ilHoursDTD = olDelayDTD.Hours;
                            ilMinutesDTD = olDelayDTD.Minutes;
                            strValues += ilHoursDTD.ToString("D2") + "." + ilMinutesDTD.ToString("D2") + ",";
                            //strValues += myAFTRow["DTD2"] + ",";

                            strValues += ilHours.ToString("D2") + "." + ilMinutes.ToString("D2") + ",";
                            //strValues += llDelay.ToString() + ",";
                            strValues += myAFTRow["GTA1"] + ",";
							strValues += myAFTRow["TTYP"] + ",";
							strValues += myAFTRow["PSTA"] + ",";
							if(bmShowStev == true)
							{
								strValues += myAFTRow["STEV"] + ",";
							}
							strValues += myAFTRow["REGN"] + ",";
							strValues += myAFTRow["GTA2"] + "\n";
							sb.Append(strValues);
						}
					}
				}
				if(strAdid == "D")
				{
					long llDelay = 0;
					//if((myAFTRow["DCD1"] != "" || myAFTRow["DCD2"] != "" || myAFTRow["DCD2"] != "" || myAFTRow["DTD1"] != "" || myAFTRow["DTD2"] != "") &&// ||
					if(myAFTRow["STOD"] != "" && myAFTRow["OFBL"] != "" && myAFTRow["STOD"] != myAFTRow["OFBL"])// )
					{
						DateTime olStod = UT.CedaFullDateToDateTime(myAFTRow["STOD"]);
						DateTime olOfbl = UT.CedaFullDateToDateTime(myAFTRow["OFBL"]);
						TimeSpan olDelay = olOfbl - olStod;
                        int ilHours = olDelay.Hours;
                        int ilMinutes = olDelay.Minutes;
                        if (txtDelay.Text.Length == 0)
							txtDelay.Text = "0";
						long llTxtDelay = Convert.ToInt64(txtDelay.Text);
						llDelay = Convert.ToInt64(olDelay.TotalMinutes);
						if( llDelay >= llTxtDelay)
						{
							imDepartures++;
							imTotalFlights++;
							strValues += myAFTRow["FLNO"] + ",";
							strValues += myAFTRow["ADID"] + ",";
							strValues += myAFTRow["DES3"] + ",";
							strValues += Helper.DateString(myAFTRow["STOD"], strDateTimeFormat) + ",";
							strValues += Helper.DateString(myAFTRow["AIRB"], strDateTimeFormat) + ",";
							strValues += Helper.DateString(myAFTRow["OFBL"], strDateTimeFormat) + ",";
							strValues += myAFTRow["ACT3"] + ",";
							strValues += myAFTRow["DCD1"] + ",";

                            if(myAFTRow["DTD1"] == "")
                                olDelayDTD = new TimeSpan(0, 0, 0);
                            else
                                olDelayDTD = new TimeSpan(0, Convert.ToInt32(myAFTRow["DTD1"]), 0);
                            ilHoursDTD = olDelayDTD.Hours;
                            ilMinutesDTD = olDelayDTD.Minutes;
                            strValues += ilHoursDTD.ToString("D2") + "." + ilMinutesDTD.ToString("D2") + ",";
                            //strValues += myAFTRow["DTD1"] + ",";
							
                            strValues += myAFTRow["DCD2"] + ",";

                            if (myAFTRow["DTD2"] == "")
                                olDelayDTD = new TimeSpan(0, 0, 0);
                            else
                                olDelayDTD = new TimeSpan(0, Convert.ToInt32(myAFTRow["DTD2"]), 0);
                            ilHoursDTD = olDelayDTD.Hours;
                            ilMinutesDTD = olDelayDTD.Minutes;
                            strValues += ilHoursDTD.ToString("D2") + "." + ilMinutesDTD.ToString("D2") + ",";
                            //strValues += myAFTRow["DTD2"] + ",";



                            strValues += ilHours.ToString("D2") + "." + ilMinutes.ToString("D2") + ",";
                            //strValues += llDelay.ToString() + ",";
							strValues += myAFTRow["GTD1"] + ",";
							strValues += myAFTRow["TTYP"] + ",";
							strValues += myAFTRow["PSTD"] + ",";
							if(bmShowStev == true)
							{
								strValues += myAFTRow["STEV"] + ",";
							}
							strValues += myAFTRow["REGN"] + ",";
							strValues += myAFTRow["GTD2"] + "\n";
							sb.Append(strValues);
						}
					}
					//					tabResult.InsertTextLine(strEmptyLine, false);
//					ilLine = tabResult.GetLineCount() - 1;
//					tabResult.SetFieldValues(ilLine, "FLNO", myAFTRow["FLNO"]);
//					tabResult.SetFieldValues(ilLine, "ADID", strAdid);
//					tabResult.SetFieldValues(ilLine, "ORGDES", myAFTRow["DES3"]);
//					strSchedule = myAFTRow["STOD"];
//					tmpDate = UT.CedaFullDateToDateTime(strSchedule);
//					tabResult.SetFieldValues(ilLine, "SCHEDULE", tmpDate.ToString("dd'/'HH:mm"));
//					strActual = myAFTRow["TIFD"];
//					tmpDate = UT.CedaFullDateToDateTime(strActual);
//					tabResult.SetFieldValues(ilLine, "ACTUAL", tmpDate.ToString("dd'/'HH:mm"));
//					tabResult.SetFieldValues(ilLine, "ACTYPE", myAFTRow["ACT3"]);
				}
			}
			if(strReportOrExcel.Equals("Report") == true)
			{
				tabResult.InsertBuffer(sb.ToString(), "\n");
				//tabResult.Sort("4", true, true);
				//tabResult.AutoSizeColumns();
				tabResult.Refresh();
				lblResults.Text = "Report Results: (" + tabResult.GetLineCount().ToString() + ")";
			}
			else if(strReportOrExcel.Equals("Excel") == true)
			{
				tabExcelResult.InsertBuffer(sb.ToString(), "\n");
            }

            //igu on 31/05/2001
            //sort by current sort setting
            if (rememberLastSortSetting && sortColumnIndex != -1)
            {
                tabResult.Sort(sortColumnIndex.ToString(), sortAsc, true);
                tabResult.Refresh();
                tabExcelResult.Sort(sortColumnIndex.ToString(), sortAsc, true);
                tabExcelResult.Refresh();
            }
		}

        /// <summary>
        /// Prepare the loaded data according to the report's needs.
        /// Prepare line by line the data a logical records and put
        /// the data into the Tab control.
        /// </summary>
        private void PrepareTabData(string strReportOrExcel)
        {
            //"FLNO,ADID,ORGDES,SCHEDULE,ACTUAL,ACTYPE"
            imArrivals = 0;
            imDepartures = 0;
            imRotations = 0;
            imTotalFlights = 0;

            //FLNO,ADID,ORGDES,SCHEDULE,ACTUAL,ONOFBL,ACTYPE,DEL1,MIN1,DEL2,MIN2,DIFF
            StringBuilder sb = new StringBuilder(10000);

            string strDateTimeFormat = strReportDateTimeFormat;
            if (strReportOrExcel.Equals("Excel") == true)
            {
                strDateTimeFormat = strExcelDateTimeFormat;
            }

            for (int i = 0; i < myAFT.Count; i++)
            {
                string strValues = "";
                IRow myAFTRow = myAFT[i];
                string strAdid = myAFTRow["ADID"];

                TimeSpan olDelayDTD;
                int ilHoursDTD;
                int ilMinutesDTD;

                if (strAdid == "A")
                {
                    long llDelay = 0;

                    //if((myAFTRow["DCD1"] != "" || myAFTRow["DCD2"] != "" || myAFTRow["DTD1"] != "" || myAFTRow["DTD2"] != "") &&// ||
                    if (myAFTRow["STOA"] != "" && myAFTRow["ONBL"] != "" && myAFTRow["STOA"] != myAFTRow["ONBL"])// )
                    {
                        DateTime olStoa = UT.CedaFullDateToDateTime(myAFTRow["STOA"]);
                        DateTime olOnbl = UT.CedaFullDateToDateTime(myAFTRow["ONBL"]);
                        TimeSpan olDelay = olOnbl - olStoa;
                        int ilHours = olDelay.Hours;
                        int ilMinutes = olDelay.Minutes;

                        if (txtDelay.Text.Length == 0)
                            txtDelay.Text = "0";
                        long llTxtDelay = Convert.ToInt64(txtDelay.Text);
                        llDelay = Convert.ToInt64(olDelay.TotalMinutes);
                        if (llDelay >= llTxtDelay)
                        {
                            imArrivals++;
                            imTotalFlights++;
                            strValues += myAFTRow["FLNO"] + ",";
                            strValues += myAFTRow["ADID"] + ",";
                            strValues += myAFTRow["ORG3"] + ",";
                            strValues += Helper.DateString(myAFTRow["STOA"], strDateTimeFormat) + ",";
                            strValues += Helper.DateString(myAFTRow["LAND"], strDateTimeFormat) + ",";
                            strValues += Helper.DateString(myAFTRow["ONBL"], strDateTimeFormat) + ",";
                            strValues += myAFTRow["ACT3"] + ",";
                            strValues += myAFTRow["DCD1"] + ",";
                            if (myAFTRow["DTD1"] == "")
                                olDelayDTD = new TimeSpan(0, 0, 0);
                            else
                                olDelayDTD = new TimeSpan(0, Convert.ToInt32(myAFTRow["DTD1"]), 0);
                            ilHoursDTD = olDelayDTD.Hours;
                            ilMinutesDTD = olDelayDTD.Minutes;
                            strValues += ilHoursDTD.ToString("D2") + "." + ilMinutesDTD.ToString("D2") + ",";
                            //  strValues += myAFTRow["DTD1"] + ",";

                            strValues += myAFTRow["DCD2"] + ",";

                            if (myAFTRow["DTD2"] == "")
                                olDelayDTD = new TimeSpan(0, 0, 0);
                            else
                                olDelayDTD = new TimeSpan(0, Convert.ToInt32(myAFTRow["DTD2"]), 0);

                            //myDCF.Clear();
                            int cntDCF = 0;
                            string strDCF = GetDCFData(myAFTRow["FLNO"], out cntDCF);
                            strValues += strDCF;
                            

                            ilHoursDTD = olDelayDTD.Hours;
                            ilMinutesDTD = olDelayDTD.Minutes;
                            strValues += ilHoursDTD.ToString("D2") + "." + ilMinutesDTD.ToString("D2") + ",";
                            //strValues += myAFTRow["DTD2"] + ",";

                            strValues += ilHours.ToString("D2") + "." + ilMinutes.ToString("D2") + ",";
                            //strValues += llDelay.ToString() + ",";
                            strValues += myAFTRow["GTA1"] + ",";
                            strValues += myAFTRow["TTYP"] + ",";
                            strValues += myAFTRow["PSTA"] + ",";
                            if (bmShowStev == true)
                            {
                                strValues += myAFTRow["STEV"] + ",";
                            }
                            strValues += myAFTRow["REGN"] + ",";
                            strValues += myAFTRow["GTA2"] + "\n";
                            sb.Append(strValues);
                        }
                    }
                }
                if (strAdid == "D")
                {
                    long llDelay = 0;
                    //if((myAFTRow["DCD1"] != "" || myAFTRow["DCD2"] != "" || myAFTRow["DCD2"] != "" || myAFTRow["DTD1"] != "" || myAFTRow["DTD2"] != "") &&// ||
                    if (myAFTRow["STOD"] != "" && myAFTRow["OFBL"] != "" && myAFTRow["STOD"] != myAFTRow["OFBL"])// )
                    {
                        DateTime olStod = UT.CedaFullDateToDateTime(myAFTRow["STOD"]);
                        DateTime olOfbl = UT.CedaFullDateToDateTime(myAFTRow["OFBL"]);
                        TimeSpan olDelay = olOfbl - olStod;
                        int ilHours = olDelay.Hours;
                        int ilMinutes = olDelay.Minutes;
                        if (txtDelay.Text.Length == 0)
                            txtDelay.Text = "0";
                        long llTxtDelay = Convert.ToInt64(txtDelay.Text);
                        llDelay = Convert.ToInt64(olDelay.TotalMinutes);
                        if (llDelay >= llTxtDelay)
                        {
                            imDepartures++;
                            imTotalFlights++;
                            strValues += myAFTRow["FLNO"] + ",";
                            strValues += myAFTRow["ADID"] + ",";
                            strValues += myAFTRow["DES3"] + ",";
                            strValues += Helper.DateString(myAFTRow["STOD"], strDateTimeFormat) + ",";
                            strValues += Helper.DateString(myAFTRow["AIRB"], strDateTimeFormat) + ",";
                            strValues += Helper.DateString(myAFTRow["OFBL"], strDateTimeFormat) + ",";
                            strValues += myAFTRow["ACT3"] + ",";
                            strValues += myAFTRow["DCD1"] + ",";

                            if (myAFTRow["DTD1"] == "")
                                olDelayDTD = new TimeSpan(0, 0, 0);
                            else
                                olDelayDTD = new TimeSpan(0, Convert.ToInt32(myAFTRow["DTD1"]), 0);
                            ilHoursDTD = olDelayDTD.Hours;
                            ilMinutesDTD = olDelayDTD.Minutes;
                            strValues += ilHoursDTD.ToString("D2") + "." + ilMinutesDTD.ToString("D2") + ",";
                            //strValues += myAFTRow["DTD1"] + ",";

                            strValues += myAFTRow["DCD2"] + ",";

                            if (myAFTRow["DTD2"] == "")
                                olDelayDTD = new TimeSpan(0, 0, 0);
                            else
                                olDelayDTD = new TimeSpan(0, Convert.ToInt32(myAFTRow["DTD2"]), 0);
                            ilHoursDTD = olDelayDTD.Hours;
                            ilMinutesDTD = olDelayDTD.Minutes;
                            strValues += ilHoursDTD.ToString("D2") + "." + ilMinutesDTD.ToString("D2") + ",";
                            //strValues += myAFTRow["DTD2"] + ",";

                            int cntDCF = 0;
                            string strDCF = GetDCFData(myAFTRow["FLNO"], out cntDCF);
                            strValues += strDCF;

                            strValues += ilHours.ToString("D2") + "." + ilMinutes.ToString("D2") + ",";
                            //strValues += llDelay.ToString() + ",";
                            strValues += myAFTRow["GTD1"] + ",";
                            strValues += myAFTRow["TTYP"] + ",";
                            strValues += myAFTRow["PSTD"] + ",";
                            if (bmShowStev == true)
                            {
                                strValues += myAFTRow["STEV"] + ",";
                            }
                            strValues += myAFTRow["REGN"] + ",";
                            strValues += myAFTRow["GTD2"] + "\n";
                            sb.Append(strValues);
                        }
                    }
                    //					tabResult.InsertTextLine(strEmptyLine, false);
                    //					ilLine = tabResult.GetLineCount() - 1;
                    //					tabResult.SetFieldValues(ilLine, "FLNO", myAFTRow["FLNO"]);
                    //					tabResult.SetFieldValues(ilLine, "ADID", strAdid);
                    //					tabResult.SetFieldValues(ilLine, "ORGDES", myAFTRow["DES3"]);
                    //					strSchedule = myAFTRow["STOD"];
                    //					tmpDate = UT.CedaFullDateToDateTime(strSchedule);
                    //					tabResult.SetFieldValues(ilLine, "SCHEDULE", tmpDate.ToString("dd'/'HH:mm"));
                    //					strActual = myAFTRow["TIFD"];
                    //					tmpDate = UT.CedaFullDateToDateTime(strActual);
                    //					tabResult.SetFieldValues(ilLine, "ACTUAL", tmpDate.ToString("dd'/'HH:mm"));
                    //					tabResult.SetFieldValues(ilLine, "ACTYPE", myAFTRow["ACT3"]);
                }
            }
            if (strReportOrExcel.Equals("Report") == true)
            {
                tabResult.InsertBuffer(sb.ToString(), "\n");
                //tabResult.Sort("4", true, true);
                //tabResult.AutoSizeColumns();
                tabResult.Refresh();
                lblResults.Text = "Report Results: (" + tabResult.GetLineCount().ToString() + ")";
            }
            else if (strReportOrExcel.Equals("Excel") == true)
            {
                tabExcelResult.InsertBuffer(sb.ToString(), "\n");
            }
        }

        private string GetDCFData(string flurno,out int cntDCF)
        {
            myDB.Unbind("DCF");
            myDCF = myDB.Bind("DCF", "DCF", "URNO,DURN,DURA,FURN,DECN,DECA,DECS,DSEQ,USEQ,USEC",
                              "10,10,4,10,2,2,2,2,2,32", "URNO,DURN,DURA,FURN,DECN,DECA,DECS,DSEQ,USEQ,USEC");
            myDCF.Clear();
            string where = "WHERE FURN='" + flurno + "'";
            myDCF.Load(where);

            string strDCFCode = string.Empty;
            
            for (int i = 0; i < myDCF.Count; i++)
            {
                strDCFCode += "," + myDCF[i]["DURA"];
                
            }
            cntDCF = myDCF.Count;

            return strDCFCode;
        }
		/// <summary>
		/// Calls the generic report. The contructor must have the Tab 
		/// control and the reports headers as well as the types for
		/// each column to enable the reports to transform data, e.g.
		/// the data/time formatting. The lengths of the columns are
		/// dynamically read out of the tab control's header.
		/// </summary>
		private void RunReport()
		{
			string strSubHeader = "";
			string strHeader = "";
			strSubHeader = "From: " + dtFrom.Value.ToString("dd.MM.yy'/'HH:mm") + " to " + 
				dtTo.Value.ToString("dd.MM.yy'/'HH:mm");
			strSubHeader += "       (Flights: " + imTotalFlights.ToString();
			strSubHeader += " ARR: " + imArrivals.ToString();
			strSubHeader += " DEP: " + imDepartures.ToString() + ")";
            strHeader = frmMain.strPrefixVal;
            if (this.radioButtonDelayedFlights.Checked)
            {
                strHeader += this.radioButtonDelayedFlights.Text;
            }
            else if (this.radioButtonDelayReasons.Checked)
            {
                strHeader += this.radioButtonDelayReasons.Text;
            }
			if(strDecn != "")
				strHeader += " ==> Reason: " + strDecn;
			if(strAlc2 != "")
				strHeader += " Airline: " + strAlc2;
			//rptFIPS rpt = new rptFIPS(tabResult, strHeader, strSubHeader, "", 10);
			strReportHeader = strHeader;
			strReportSubHeader = strSubHeader;
			prtFIPS_Landscape rpt = null;

            //commented: igu on 19/05/2011
            //PrtMultiLineData multData = new PrtMultiLineData(1, 10);
            //int delayCol = 7;
            //List<Color> lsColor = new List<Color>();
            //lsColor.Add(Color.LightGray);
            //lsColor.Add( Color.Empty);

            //for (int i = 0; i < imMaxDelayCnt; i++)
            //{
            //    int colorIdx = i % 2;
            //    multData.AddForMultiLine(delayCol++, lsColor[colorIdx]);
            //    multData.AddForMultiLine(delayCol++, lsColor[colorIdx]);
            //}

            //for (int i = 0; i < imMaxDelayCnt; i++)
            //{
            //    int colorIdx = i % 2;
            //    multData.AddForMultiLine(delayCol++, lsColor[colorIdx]);
            //}

            //---
            //igu on 19/05/2011
            if (this.radioButtonDelayedFlights.Checked == true)
            {
                string logicalFields = string.Empty;
                string headerCaptions = string.Empty;
                string headerLengths = string.Empty;

                string[] arrLengths = tabResult.HeaderLengthString.Split(',');
                List<string> lengthList = new List<string>();
                foreach (string length in arrLengths)
                {
                    int intLength = Convert.ToInt32(length);
                    double dblLength = intLength * 4 / 5;
                    dblLength = Math.Floor(dblLength);
                    intLength = Convert.ToInt32(dblLength);

                    string strLength = intLength.ToString();

                    lengthList.Add(strLength);
                }

                if (imMaxDelayCnt <= 3)
                {
                    logicalFields = tabResult.LogicalFieldList;
                    headerCaptions = tabResult.HeaderString; 
                    arrLengths = lengthList.ToArray();
                    headerLengths = string.Join(",", arrLengths);
                }
                else
                {
                    string[] delayDetails = new string[] { "DEL{0}", "MIN{0}", "SC{0}" };
                    
                    string[] arrFields = tabResult.LogicalFieldList.Split(',');
                    List<string> fieldList = new List<string>(arrFields);
                    string[] arrCaptions = tabResult.HeaderString.Split(',');
                    List<string> captionList = new List<string>(arrCaptions);                

                    for (int i = 4; i <= imMaxDelayCnt; i++)
                    {
                        foreach (string delayDetail in delayDetails)
                        {
                            string field = string.Format(delayDetail, i);
                            int fieldIndex = fieldList.IndexOf(field);
                            if (fieldIndex > 0)
                            {
                                fieldList.RemoveAt(fieldIndex);
                                captionList.RemoveAt(fieldIndex);
                                lengthList.RemoveAt(fieldIndex);
                            }
                        }
                    }

                    int plusFieldIndex = fieldList.IndexOf("PLUS");
                    if (plusFieldIndex > 0)
                    {
                        lengthList[plusFieldIndex] = "15";
                    }

                    arrFields = fieldList.ToArray();
                    arrCaptions = captionList.ToArray();
                    arrLengths = lengthList.ToArray();

                    logicalFields = string.Join(",", arrFields);
                    headerCaptions = string.Join(",", arrCaptions);
                    headerLengths = string.Join(",", arrLengths);

                    //fill in plus sign
                    for (int i = 0; i < tabResult.GetLineCount(); i++)
                    {
                        tabResult.SetFieldValues(i, "PLUS",
                            (tabResult.GetFieldValue(i, "DEL4").Trim() == "" ? " " : "+"));
                    }
                }
                //---
			
                //rpt = new prtFIPS_Landscape(tabResult,strHeader.ToString(),strSubHeader.ToString(),"",10, multData);//igu on 19/05/2011				
                rpt = new prtFIPS_Landscape(tabResult, logicalFields, headerCaptions, headerLengths, 
                    strHeader, strSubHeader, "", 8, 8); //igu on 19/05/2011				

				rpt.SetExcelExportHeaderLenghtString(strExcelTabHeaderLens);				
				rpt.SetExcelExportTabResult(tabExcelResult);
			}
			else if(this.radioButtonDelayReasons.Checked == true)
			{
                rpt = new prtFIPS_Landscape(tabResult, strHeader, strSubHeader, "", 8);
				//rpt.TextBoxCanGrow = false;			
			}
			if(rpt != null)
			{
				frmPrintPreview frm = new frmPrintPreview(rpt);				
				if(Helper.UseSpreadBuilder() == true)
				{
					activeReport = rpt;
					frm.UseSpreadBuilder(true);
					frm.GetBtnExcelExport().Click += new EventHandler(frmDelayed_Flights_Click);
				}
				else
				{
					frm.UseSpreadBuilder(false);
				}
				frm.Show();
			}
		}
		/// <summary>
		/// Start the print preview without data selection.
		/// This can be used to adjust the column widths and
		/// the sorting of the tab before printing.
		/// </summary>
		/// <param name="sender">Originator is the form</param>
		/// <param name="e">Event arguments.</param>
		private void btnPrintPreview_Click(object sender, System.EventArgs e)
		{
			RunReport();
		}
		/// <summary>
		/// Loads data and dirctly calls the print preview.
		/// </summary>
		/// <param name="sender">Originator is the form</param>
		/// <param name="e">Event arguments.</param>
		private void btnLoadPrint_Click(object sender, System.EventArgs e)
		{
			string strError = ValidateUserEntry();
			if(strError != "")
			{
				MessageBox.Show(this, strError);
				return; // Do nothing due to errors
			}
			this.Cursor = Cursors.WaitCursor;
            //if(this.radioButtonDelayedFlights.Checked == true)
            //{
            //    LoadReportData();
            //    PrepareReportData("Report");
            //    PrepareReportData("Excel");
            //    RunReport();
            //}
            //else if(this.radioButtonDelayReasons.Checked == true)
            //{
            //    LoadReportData();
            //    PrepareDelayReasonReportData();								
            //}
            DisplayData(true);
            RunReport();
			this.Cursor = Cursors.Arrow;
		}
		/// <summary>
		/// Sent by the tab control when the user double click.
		/// This is necessary for sorting.
		/// </summary>
		/// <param name="sender">The Tab control</param>
		/// <param name="e">Double click event parameters.</param>
		private void tabResult_SendLButtonDblClick(object sender, AxTABLib._DTABEvents_SendLButtonDblClickEvent e)
		{
			if(e.lineNo == -1)
			{
				if( e.colNo == tabResult.CurrentSortColumn)
				{
					if( tabResult.SortOrderASC == true)
					{
						tabResult.Sort(e.colNo.ToString(), false, true);
						tabExcelResult.Sort(e.colNo.ToString(), false, true);
					}
					else
					{
						tabResult.Sort(e.colNo.ToString(), true, true);
						tabExcelResult.Sort(e.colNo.ToString(), true, true);
					}
				}
				else
				{
					tabResult.Sort( e.colNo.ToString(), true, true);
					tabExcelResult.Sort( e.colNo.ToString(), true, true);
				}
				tabResult.Refresh();

                //igu on 31/05/2011
                //save current sort setting
                if (rememberLastSortSetting)
                {
                    sortColumnIndex = tabResult.CurrentSortColumn;
                    sortAsc = tabResult.SortOrderASC;
                }
			}
		}

        private void frmDelayed_Flights_Closing(object sender, System.ComponentModel.CancelEventArgs e)
		{
			myDB.Unbind("DEN");
			myDB.Unbind("AFT");
		}

		/// <summary>
		/// PrepareDelayReasonReportData is used to prepare the data for the display.
		/// </summary>
		private void PrepareDelayReasonReportData()
		{
			if (myAFT == null)
				return;			
			System.Collections.Hashtable hashDelayCodes = new Hashtable();
			StringBuilder sb = new StringBuilder(10000);
			myAFT.Sort("ALC3",true);			
			
			for(int i = 0; i < myAFT.Count; i++)
			{	
				bool blIsDelayCodeOK = true;
				bool blIsDelayTimeOK = false;
                IRow myAFTRow = myAFT[i];
				if(myAFTRow["ADID"] == "A")
				{
					long llDelay = 0;
					//if((myAFTRow["DCD1"] != "" || myAFTRow["DCD2"] != "" || myAFTRow["DTD1"] != "" || myAFTRow["DTD2"] != "") ||
					if(myAFTRow["STOA"] != "" && myAFTRow["ONBL"] != "" && myAFTRow["STOA"] != myAFTRow["ONBL"])// )
					{
						DateTime olStoa = UT.CedaFullDateToDateTime(myAFTRow["STOA"]);
						DateTime olOnbl = UT.CedaFullDateToDateTime(myAFTRow["ONBL"]);
						TimeSpan olDelay = olOnbl - olStoa;
						if(txtDelay.Text.Length == 0)
							txtDelay.Text = "0";			
						long llTxtDelay = Convert.ToInt64(txtDelay.Text);
						llDelay = Convert.ToInt64(olDelay.TotalMinutes);
						if(llDelay >= llTxtDelay)
						{
							blIsDelayTimeOK = true;
						}
					}
				}

				if(myAFTRow["ADID"] == "D")
				{
					long llDelay = 0;
					//if((myAFTRow["DCD1"] != "" || myAFTRow["DCD2"] != "" || myAFTRow["DTD1"] != "" || myAFTRow["DTD2"] != "") ||
					if(myAFTRow["STOD"] != "" && myAFTRow["OFBL"] != "" && myAFTRow["STOD"] != myAFTRow["OFBL"])// )
					{
						DateTime olStod = UT.CedaFullDateToDateTime(myAFTRow["STOD"]);
						DateTime olOfbl = UT.CedaFullDateToDateTime(myAFTRow["OFBL"]);
						TimeSpan olDelay = olOfbl - olStod;
						if(txtDelay.Text.Length == 0)
							txtDelay.Text = "0";
						long llTxtDelay = Convert.ToInt64(txtDelay.Text);
						llDelay = Convert.ToInt64(olDelay.TotalMinutes);
						if(llDelay >= llTxtDelay)
						{
							blIsDelayTimeOK = true;
						}
					}
				}
				
                /* igu on 26/01/2011
				if(cbDelayCodes.Text != "<None>")
				{
					string [] strArr = cbDelayCodes.Text.Split('|');
					if(strArr[0].Trim().Length > 0 && strArr[1].Trim().Length > 0)
					{
						if((strArr[0].Trim() != myAFTRow["DCD1"].Trim()) 
							&& (strArr[1].Trim() != myAFTRow["DCD1"].Trim()))
						{
							blIsDelayCodeOK = false;
						}
					}
					else if(strArr[0].Trim().Length > 0)
					{
						if(strArr[0].Trim() != myAFTRow["DCD1"].Trim())
						{
							blIsDelayCodeOK = false;
						}
					}
					else if(strArr[1].Trim().Length > 0)
					{
						if(strArr[1].Trim() != myAFTRow["DCD1"].Trim())
						{
							blIsDelayCodeOK = false;
						}
					}
				}
                */

                //igu on 26/01/2011
                string deca = txtDECA.Text.Trim();
                string decn = txtDECN.Text.Trim();
                string decs = txtDECS.Text.Trim();

                if (deca != "" || decn != "" || decs != "") 
                {
                    if (deca.Length > 0 && decn.Length > 0)
                    {
                        if ((deca != myAFTRow["DCD1"].Trim())
                            && (decn != myAFTRow["DCD1"].Trim()))
                        {
                            blIsDelayCodeOK = false;
                        }
                    }
                    else if (deca.Length > 0)
                    {
                        if (deca != myAFTRow["DCD1"].Trim())
                        {
                            blIsDelayCodeOK = false;
                        }
                    }
                    else if (decn.Length > 0)
                    {
                        if (decn != myAFTRow["DCD1"].Trim())
                        {
                            blIsDelayCodeOK = false;
                        }
                    }
                }

				if(blIsDelayTimeOK == true && blIsDelayCodeOK == true)
				{
					string strDelayCode = myAFTRow["DCD1"] + myAFTRow["ALC3"];
					if(myAFTRow["DCD1"].Trim().Length == 0)
					{
						strDelayCode = "@EMPTYDELAYCODE@" + strDelayCode;
					}
					if(myAFTRow["DCD1"].Trim().Length != 0)
					{
						if(hashDelayCodes[strDelayCode] == null)
						{
							DelayCodeRowData delayCodeRowData = new DelayCodeRowData();
							delayCodeRowData.strAirline = myAFTRow["ALC3"];
							delayCodeRowData.strDelayCode = myAFTRow["DCD1"];
							
							string strTmpWhere = strDelayReasonWhereRawDEN;
							if(myAFTRow["DCD1"].Trim().Length != 0)
							{
								strTmpWhere = strTmpWhere.Replace("@@DELCODE",myAFTRow["DCD1"].Trim());
							}
							else
							{
								strTmpWhere = strTmpWhere.Replace("@@DELCODE"," ");
							}
							strTmpWhere += " AND ALC3 = '" + myAFTRow["ALC3"] + "'";							

							myDB.Unbind("DEN");
							myDEN = myDB.Bind("DEN","DEN","DECA,DECN,DENA","2,2,75","DECA,DECN,DENA");
							myDEN.Clear();
							myDEN.Load(strTmpWhere);

							if(myDEN.Count > 0)
							{
								delayCodeRowData.strDelayReason = myDEN[0]["DENA"];
							}

							if(myAFTRow["ADID"] == "A")
							{
								delayCodeRowData.imNoOfArrivalFlights++;
							}
							else
							{
								delayCodeRowData.imNoOfDepartureFlights++;
							}
							delayCodeRowData.strFlightNo += myAFTRow["FLNO"].ToString();
							hashDelayCodes.Add(strDelayCode,delayCodeRowData);
						}
						else
						{
							DelayCodeRowData delayCodeRowData = (DelayCodeRowData)hashDelayCodes[strDelayCode];
							if(delayCodeRowData.strFlightNo.IndexOf(myAFTRow["FLNO"]) == -1)
							{
								delayCodeRowData.strFlightNo = delayCodeRowData.strFlightNo + myAFTRow["FLNO"];
								if(myAFTRow["ADID"] == "A")
								{
									delayCodeRowData.imNoOfArrivalFlights++;
								}
								else
								{
									delayCodeRowData.imNoOfDepartureFlights++;
								}							
							}			
						}
					}
				}

				blIsDelayCodeOK = true;
				
                /* igu on 26/01/2011
				if(cbDelayCodes.Text != "<None>")
				{			
					string [] strArr = cbDelayCodes.Text.Split('|');
					if(strArr[0].Trim().Length > 0 && strArr[1].Trim().Length > 0)
					{
						if((strArr[0].Trim() != myAFTRow["DCD2"].Trim()) 
							&& (strArr[1].Trim() != myAFTRow["DCD2"].Trim()))
						{
							blIsDelayCodeOK = false;
						}
					}
					else if(strArr[0].Trim().Length > 0)
					{
						if(strArr[0].Trim() != myAFTRow["DCD2"].Trim())
						{
							blIsDelayCodeOK = false;
						}
					}
					else if(strArr[1].Trim().Length > 0)
					{
						if(strArr[1].Trim() != myAFTRow["DCD2"].Trim())
						{
							blIsDelayCodeOK = false;
						}
					}
				}
                */

                //igu on 26/01/2011
                if (deca != "" || decn != "" || decs != "")
                {
                    if (deca.Length > 0 && decn.Length > 0)
                    {
                        if ((deca != myAFTRow["DCD2"].Trim())
                            && (decn != myAFTRow["DCD2"].Trim()))
                        {
                            blIsDelayCodeOK = false;
                        }
                    }
                    else if (deca.Length > 0)
                    {
                        if (deca != myAFTRow["DCD2"].Trim())
                        {
                            blIsDelayCodeOK = false;
                        }
                    }
                    else if (decn.Length > 0)
                    {
                        if (decn != myAFTRow["DCD2"].Trim())
                        {
                            blIsDelayCodeOK = false;
                        }
                    }
                }
                //

				if(blIsDelayTimeOK == true && blIsDelayCodeOK == true)
				{
					string strDelayCode = myAFTRow["DCD2"] + myAFTRow["ALC3"];;
					if(myAFTRow["DCD2"].Trim().Length == 0)
					{
						strDelayCode = "@EMPTYDELAYCODE@" + strDelayCode;
					}
					if(myAFTRow["DCD2"].Trim().Length != 0 ||(myAFTRow["DCD1"].Trim().Length == 0 && myAFTRow["DCD2"].Trim().Length == 0))
					{						
						if(hashDelayCodes[strDelayCode] == null)
						{
							DelayCodeRowData delayCodeRowData = new DelayCodeRowData();
							delayCodeRowData.strAirline = myAFTRow["ALC3"];
							delayCodeRowData.strDelayCode = myAFTRow["DCD2"];
						
							string strTmpWhere = strDelayReasonWhereRawDEN;
							if(myAFTRow["DCD2"].Trim().Length != 0)
							{
								strTmpWhere = strTmpWhere.Replace("@@DELCODE",myAFTRow["DCD2"].Trim());
							}
							else
							{
								strTmpWhere = strTmpWhere.Replace("@@DELCODE"," ");
							}
							strTmpWhere += " AND ALC3 = '" + myAFTRow["ALC3"] + "'";							

							myDB.Unbind("DEN");
							myDEN = myDB.Bind("DEN","DEN","DECA,DECN,DENA","2,2,75","DECA,DECN,DENA");
							myDEN.Clear();
							myDEN.Load(strTmpWhere);

							if(myDEN.Count > 0)
							{
								delayCodeRowData.strDelayReason = myDEN[0]["DENA"];
							}

							if(myAFTRow["ADID"] == "A")
							{
								delayCodeRowData.imNoOfArrivalFlights++;
							}
							else
							{
								delayCodeRowData.imNoOfDepartureFlights++;
							}
							delayCodeRowData.strFlightNo += myAFTRow["FLNO"].ToString();
							hashDelayCodes.Add(strDelayCode,delayCodeRowData);
						}
						else
						{
							DelayCodeRowData delayCodeRowData = (DelayCodeRowData)hashDelayCodes[strDelayCode];
							if(delayCodeRowData.strFlightNo.IndexOf(myAFTRow["FLNO"]) == -1)
							{
								delayCodeRowData.strFlightNo = delayCodeRowData.strFlightNo + myAFTRow["FLNO"];
								if(myAFTRow["ADID"] == "A")
								{
									delayCodeRowData.imNoOfArrivalFlights++;
								}
								else
								{
									delayCodeRowData.imNoOfDepartureFlights++;
								}
							}
						}
					}
				}
			}

			IDictionaryEnumerator iDictEnumerator = hashDelayCodes.GetEnumerator();
			
			while(iDictEnumerator.MoveNext() == true)
			{
				string strValues = "";
				strValues += ((DelayCodeRowData)iDictEnumerator.Value).strDelayCode + ",";
				strValues += ((DelayCodeRowData)iDictEnumerator.Value).strDelayReason + ",";
				strValues += ((DelayCodeRowData)iDictEnumerator.Value).strAirline + ",";
				strValues += ((DelayCodeRowData)iDictEnumerator.Value).imNoOfArrivalFlights.ToString() + ",";
				strValues += ((DelayCodeRowData)iDictEnumerator.Value).imNoOfDepartureFlights.ToString() + "," + "\n";
				sb.Append(strValues);
			}			

			tabResult.InsertBuffer(sb.ToString(), "\n");
			tabResult.Sort("0",true,true);
			tabResult.Refresh();
			lblResults.Text = "Report Results: (" + tabResult.GetLineCount().ToString() + ")";
		}

		private void radioButtonDelayedFlights_CheckedChanged(object sender, System.EventArgs e)
		{
            if (this.radioButtonDelayedFlights.Checked == true)
			{
                DisplayData(false);
                /*
				if(tabResult.GetLineCount() > 0)
				{
                    
                    //InitTab();
                    //LoadReportData();
                    //PrepareReportData("Report");
                    //PrepareReportData("Excel");
				}
				else
				{
					InitTab();
				}
                 */
			}
		}

		private void radioButtonDelayReasons_CheckedChanged(object sender, System.EventArgs e)
		{
            if(this.radioButtonDelayReasons.Checked == true)
			{
                DisplayData(false);
                /*
				if(tabResult.GetLineCount() > 0)
				{
                    
                    //InitTab();
                    //LoadReportData();					
                    //PrepareDelayReasonReportData();
				}
				else
				{
					InitTab();
				}
                 */
			}
		}

		private void InitializeMouseEvents()
		{
			((System.Windows.Forms.Control)tabResult).MouseWheel += new MouseEventHandler(frmDelayed_Flights_MouseWheel);
			((System.Windows.Forms.Control)tabResult).MouseEnter += new EventHandler(frmDelayed_Flights_MouseEnter);
			((System.Windows.Forms.Control)tabResult).MouseLeave += new EventHandler(frmDelayed_Flights_MouseLeave);
		}

		private void frmDelayed_Flights_MouseWheel(object sender, MouseEventArgs e)
		{
			if(bmMouseInsideTabControl == false || Helper.CheckScrollingStatus(tabResult) == false)
			{
				return;
			}
			if(e.Delta > 0 && tabResult.GetVScrollPos() > 0)
			{				
				tabResult.OnVScrollTo(tabResult.GetVScrollPos() - 1);
			}
			else if(e.Delta < 0 && (tabResult.GetVScrollPos() + 1) < tabResult.GetLineCount())
			{
				tabResult.OnVScrollTo(tabResult.GetVScrollPos() + 1);
			}
		}

		private void frmDelayed_Flights_MouseEnter(object sender, EventArgs e)
		{			
			bmMouseInsideTabControl = true;
		}

		private void frmDelayed_Flights_MouseLeave(object sender, EventArgs e)
		{
			bmMouseInsideTabControl = false;
		}

		private void frmDelayed_Flights_Click(object sender, EventArgs e)
		{
			if(this.radioButtonDelayedFlights.Checked == true)
			{
				Helper.ExportToExcel(tabExcelResult,strReportHeader,strReportSubHeader,strExcelTabHeaderLens,10,0,activeReport);
			}
			else
			{
				Helper.ExportToExcel(tabResult,strReportHeader,strReportSubHeader,strTabHeaderLens,8,0,activeReport);
			}

		}

		private void buttonHelp_Click(object sender, System.EventArgs e)
		{
			Helper.ShowHelpFile(this);
		}

		private void frmDelayed_Flights_HelpRequested(object sender, HelpEventArgs hlpevent)
		{
			Helper.ShowHelpFile(this);
		}

        private void PrepareTabHeader(int maxDelayCnt)
        {
            string stDelayHdr = "";
            string stDelayHdrLen = "";
            string stDelayAlignment = "";
            string stDelayWidth = "";
            string stDelayField = "";

            if (maxDelayCnt > 0)
            {
                for (int i = 1; i <= maxDelayCnt; i++)
                {
                    stDelayHdr += string.Format("Del{0},Min{0},", i);
                    stDelayField += string.Format(TAB_LOGICAL_FIELDS_FOR_DELAY_COLUMN_PAIR, i);
                    stDelayHdrLen +=  TAB_HEADER_LENS_FOR_DELAY_COLUMN_PAIR;
                    stDelayAlignment += TAB_COLUMN_ALIGNMENT_FOR_DELAY_COLUMN_PAIR;
                    stDelayWidth += TAB_COLUMN_WIDTH_FOR_DELAY_COLUMN_PAIR;
                }

                for (int i = 1; i <= maxDelayCnt; i++)
                {
                    stDelayHdr += string.Format("SC{0},", i);
                    stDelayField += string.Format(TAB_LOGICAL_FIELDS_FOR_SUBDELAY_COLUMN, i);
                    stDelayHdrLen += TAB_HEADER_LENS_FOR_SUBDELAY_COLUMN;
                    stDelayAlignment += TAB_COLUMN_ALIGNMENT_FOR_SUBDELAY_COLUMN;
                    stDelayWidth += TAB_COLUMN_WIDTH_FOR_SUBDELAY_COLUMN;
                }
            }

            string stTermHdr = "";
            string stTermFld = "";
            string stTermHdrLen = "";

            if (bmShowStev)
            {
                stTermFld = "TERM,";
                stTermHdr = "Term,";
                stTermHdrLen = "20,";
            }

            strExcelTabHeaderLens = string.Format(TAB_EXCEL_HEADER_LENS_WO_DELAY_COLUMNS, stDelayHdrLen, stTermHdrLen);

            InitTab( 
                string.Format ( TAB_HEADER_WITHOUT_DELAY_COLUMNS, stDelayHdr, stTermHdr ),
                string.Format ( TAB_LOGICAL_FIELDS_WO_DELAY_COLUMNS, stDelayField, stTermFld ),
                string.Format ( TAB_HEADER_LENS_WO_DELAY_COLUMNS, stDelayHdrLen, stTermHdrLen ),
                string.Format ( TAB_COLUMN_WIDTH_WO_DELAY_COLUMNS, stDelayWidth ),
                string.Format ( TAB_COLUMN_ALIGNMENT_WO_DELAY_COLUMNS, stDelayAlignment ));
        }

        /// <summary>
        /// Prepare the loaded data according to the report's needs.
        /// Prepare line by line the data a logical records and put
        /// the data into the Tab control.
        /// </summary>
        private void PrepareReportDataNew(string strReportOrExcel)
        {
            //myAFT = myDB["AFT"];
            //myDCF = myDB["DCF"];
            //"FLNO,ADID,ORGDES,SCHEDULE,ACTUAL,ACTYPE"
            imArrivals = 0;
            imDepartures = 0;
            imRotations = 0;
            imTotalFlights = 0;
            mMaxDelayCnt = CtrlDelayedFlight.ComputeMaxDelayCnt(myAFT, myDCF, myDEN, bmUseDecs, out flightDelaysCollection);
            imMaxDelayCnt = Math.Max(mMaxDelayCnt.ArrDelayCodeCount, mMaxDelayCnt.DepDelayCodeCount);
            PrepareTabHeader(imMaxDelayCnt);

            //FLNO,ADID,ORGDES,SCHEDULE,ACTUAL,ONOFBL,ACTYPE,DEL1,MIN1,DEL2,MIN2,DIFF
            StringBuilder sb = new StringBuilder(10000);


            string strDateTimeFormat = strReportDateTimeFormat;
            if (strReportOrExcel.Equals("Excel") == true)
            {
                strDateTimeFormat = strExcelDateTimeFormat;
            }

            for (int i = 0; i < myAFT.Count; i++)
            {
                string strValues = "";
                IRow myAFTRow = myAFT[i];
                string strAdid = myAFTRow["ADID"];

                TimeSpan olDelayDTD;
                int ilHoursDTD;
                int ilMinutesDTD;

                if (strAdid == "A")
                {
                    long llDelay = 0;

                    //if((myAFTRow["DCD1"] != "" || myAFTRow["DCD2"] != "" || myAFTRow["DTD1"] != "" || myAFTRow["DTD2"] != "") &&// ||
                    if (myAFTRow["STOA"] != "" && myAFTRow["ONBL"] != "" && myAFTRow["STOA"] != myAFTRow["ONBL"])// )
                    {
                        DateTime olStoa = UT.CedaFullDateToDateTime(myAFTRow["STOA"]);
                        DateTime olOnbl = UT.CedaFullDateToDateTime(myAFTRow["ONBL"]);
                        TimeSpan olDelay = olOnbl - olStoa;
                        int ilHours = olDelay.Hours;
                        int ilMinutes = olDelay.Minutes;

                        if (txtDelay.Text.Length == 0)
                            txtDelay.Text = "0";
                        long llTxtDelay = Convert.ToInt64(txtDelay.Text);
                        llDelay = Convert.ToInt64(olDelay.TotalMinutes);
                        if (llDelay >= llTxtDelay)
                        {
                            imArrivals++;
                            imTotalFlights++;
                            strValues += myAFTRow["FLNO"] + ",";
                            strValues += myAFTRow["ADID"] + ",";
                            strValues += myAFTRow["ORG3"] + ",";
                            strValues += Helper.DateString(myAFTRow["STOA"], strDateTimeFormat) + ",";
                            strValues += Helper.DateString(myAFTRow["LAND"], strDateTimeFormat) + ",";
                            strValues += Helper.DateString(myAFTRow["ONBL"], strDateTimeFormat) + ",";
                            strValues += myAFTRow["ACT3"] + ",";

                            strValues += PrepareDelayData(imMaxDelayCnt, myAFTRow);
                            strValues += ilHours.ToString("D2") + "." + ilMinutes.ToString("D2") + ",";

                            strValues += myAFTRow["GTA1"] + ",";
                            strValues += myAFTRow["TTYP"] + ",";
                            strValues += myAFTRow["PSTA"] + ",";
                            if (bmShowStev == true)
                            {
                                strValues += myAFTRow["STEV"] + ",";
                            }
                            strValues += myAFTRow["REGN"] + ",";
                            strValues += myAFTRow["GTA2"] + "\n";
                            sb.Append(strValues);
                        }
                    }
                }
                if (strAdid == "D")
                {
                    long llDelay = 0;
                    //if((myAFTRow["DCD1"] != "" || myAFTRow["DCD2"] != "" || myAFTRow["DCD2"] != "" || myAFTRow["DTD1"] != "" || myAFTRow["DTD2"] != "") &&// ||
                    if (myAFTRow["STOD"] != "" && myAFTRow["OFBL"] != "" && myAFTRow["STOD"] != myAFTRow["OFBL"])// )
                    {
                        DateTime olStod = UT.CedaFullDateToDateTime(myAFTRow["STOD"]);
                        DateTime olOfbl = UT.CedaFullDateToDateTime(myAFTRow["OFBL"]);
                        TimeSpan olDelay = olOfbl - olStod;
                        int ilHours = olDelay.Hours;
                        int ilMinutes = olDelay.Minutes;
                        if (txtDelay.Text.Length == 0)
                            txtDelay.Text = "0";
                        long llTxtDelay = Convert.ToInt64(txtDelay.Text);
                        llDelay = Convert.ToInt64(olDelay.TotalMinutes);
                        if (llDelay >= llTxtDelay)
                        {
                            imDepartures++;
                            imTotalFlights++;
                            strValues += myAFTRow["FLNO"] + ",";
                            strValues += myAFTRow["ADID"] + ",";
                            strValues += myAFTRow["DES3"] + ",";
                            strValues += Helper.DateString(myAFTRow["STOD"], strDateTimeFormat) + ",";
                            strValues += Helper.DateString(myAFTRow["AIRB"], strDateTimeFormat) + ",";
                            strValues += Helper.DateString(myAFTRow["OFBL"], strDateTimeFormat) + ",";
                            strValues += myAFTRow["ACT3"] + ",";
                            
                            strValues += PrepareDelayData(imMaxDelayCnt, myAFTRow);
                            strValues += ilHours.ToString("D2") + "." + ilMinutes.ToString("D2") + ",";

                            strValues += myAFTRow["GTD1"] + ",";
                            strValues += myAFTRow["TTYP"] + ",";
                            strValues += myAFTRow["PSTD"] + ",";
                            if (bmShowStev == true)
                            {
                                strValues += myAFTRow["STEV"] + ",";
                            }
                            strValues += myAFTRow["REGN"] + ",";
                            strValues += myAFTRow["GTD2"] + "\n";
                            sb.Append(strValues);
                        }
                    }
                }
            }
            //if (strReportOrExcel.Equals("Report") == true)
            {
                tabResult.InsertBuffer(sb.ToString(), "\n");
                tabResult.Refresh();
                lblResults.Text = "Report Results: (" + tabResult.GetLineCount().ToString() + ")";
            }
            //else if (strReportOrExcel.Equals("Excel") == true)
            {
                tabExcelResult.InsertBuffer(sb.ToString(), "\n");
            }

            //igu on 31/05/2001
            //sort by current sort setting
            if (rememberLastSortSetting && sortColumnIndex != -1)
            {
                tabResult.Sort(sortColumnIndex.ToString(), sortAsc, true);
                tabResult.Refresh();
                tabExcelResult.Sort(sortColumnIndex.ToString(), sortAsc, true);
                tabExcelResult.Refresh();
            }
        }

        private string PrepareDelayData(int maxDelayCnt, IRow flightRow)
        {
            string strValues = "";
            int cnt = 0;

            //igu on 22/03/2011
            int delayCodeCount = maxDelayCnt;

            FlightDelays flightDelays = flightDelaysCollection.GetFlightDelays(flightRow["URNO"]);
            if (flightDelays == null)
            {
                strValues = new String(',', delayCodeCount * 3);
            }
            else
            {
                List<DelayData> lsDelays = flightDelays.DelayDataCollection.DelayDataList;
                for (int i = 0; i < delayCodeCount * 2; i++)
                {
                    DelayData delay = null;
                    if (cnt < lsDelays.Count)
                    {
                        delay = lsDelays[cnt];
                    }

                    if (i < delayCodeCount) //MainDelay
                    {
                        if (delay == null)
                        {
                            strValues += string.Format("{0},{1},", "", DelayData.DEFAULT_DURA);
                        }
                        else
                        {
                            if (string.IsNullOrEmpty(delay.Subcode))
                            {
                                strValues += string.Format("{0},{1},", delay.FullCode, delay.Duration);
                                cnt++;
                            }
                            else
                            {
                                strValues += string.Format("{0},{1},", "", DelayData.DEFAULT_DURA);
                            }
                        }
                    }
                    else //SubDelay
                    {
                        if (delay == null)
                        {
                            strValues += string.Format("{0},", "");
                        }
                        else
                        {
                            if (string.IsNullOrEmpty(delay.Subcode))
                            {
                                strValues += string.Format("{0},", "");
                            }
                            else
                            {
                                strValues += string.Format("{0},", delay.FullCode);
                                cnt++;
                            }
                        }
                    }
                }
            }

            return strValues;
        }


        class DelayReasonInfo
        {
            Dictionary<string, DelayCodeRowData> _mapDelayCodeInfo = new Dictionary<string, DelayCodeRowData>();

            public Dictionary<string, DelayCodeRowData> MapDelayCodeInfo
            {
                get { return _mapDelayCodeInfo; }
            }

            Dictionary<string, string> _mapDelayFilter = new Dictionary<string, string>();
            long _lDelayMin = 0;
            bool _useDecs = false;
            ITable _myDCF;
            IDatabase _myDB;

            public static string GetDelayCodeKey(string delayCode, string airLineCode)
            {
                string stKey = delayCode + airLineCode;
                if (delayCode.Trim() == "") stKey = "@EMPTYDELAYCODE@" + stKey;
                return stKey;
            }

            private bool IsDelayCodeOkForAFT(Dictionary<string, string> mapDelayFilter, string stDelayCode)
            {
                bool ok = true;
                stDelayCode = stDelayCode.Trim();
                if (mapDelayFilter.Count>0)
                {
                    if (!mapDelayFilter.ContainsKey(stDelayCode)) ok = false;
                }
                return ok;
            }

            private bool IsDelayCodeOkForDCF(Dictionary<string, string> mapDelayFilter, string stDelayCode)
            {
                bool ok = true;
                stDelayCode = stDelayCode.Trim();
                if (mapDelayFilter.Count > 0)
                {
                    if (!mapDelayFilter.ContainsKey(stDelayCode)) ok = false;
                }
                return ok;
            }

            public static string GetFlightInfo(IRow flightRow)
            {
                return flightRow["ADID"] == "A" ?
                    flightRow["FLNO"] + ":A:" + flightRow["STOA"] :
                    flightRow["FLNO"] + ":D:" + flightRow["STOD"];
            }

            /* igu on 26/01/2011
            internal DelayReasonInfo(string strDelays, long llDelayMinutes)
            {
                _lDelayMin = llDelayMinutes;
                int strArrCnt = 0;
                _useDecs = Ctrl.CtrlDelayedFlight.GetInstance().UseDelayCodeDecs;
                _myDB = UT.GetMemDB();
                _myDCF = _myDB["DCF"];
                _myDCF.CreateIndex("FURN", "FURN");
                _myDCF.Sort("FURN", true);

                if (strDelays != "<None>")
                {
                    string[] strArr = strDelays.Split('|');
                    strArrCnt = strArr.Length;
                    if (strArrCnt > 1)
                    {
                        if (_useDecs) { if (strArrCnt > 3) strArrCnt = 3; }
                        else { if (strArrCnt > 2) strArrCnt = 2; }
                    }
                    bool hasDelayFilter = false;
                    for (int i = 0; i < strArrCnt; i++)
                    {
                        strArr[i] = strArr[i].Trim();
                        if (strArr[i] != "") _mapDelayFilter.Add(strArr[i], strArr[i]);
                    }
                }
            }
            */

            //igu on 26/01/2011
            internal DelayReasonInfo(string delayAlpha, string delayNumeric, string delaySubcode, long llDelayMinutes, bool useDecs)
            {
                _lDelayMin = llDelayMinutes;
                _useDecs = useDecs;
                _myDB = UT.GetMemDB();
                _myDCF = _myDB["DCF"];
                _myDCF.CreateIndex("FURN", "FURN");
                _myDCF.Sort("FURN", true);

                if (delayAlpha != "" || delayNumeric != "" || delaySubcode != "")
                {
                    if (delayAlpha != "") _mapDelayFilter.Add(delayAlpha, delayAlpha);
                    if (delayNumeric != "") _mapDelayFilter.Add(delayNumeric, delayNumeric);

                    if (_useDecs) 
                    {
                        if (delaySubcode != "") _mapDelayFilter.Add(delaySubcode, delaySubcode);
                    }
                }
            }
            //

            internal void Add(IRow flightRow)
            {
                bool blIsDelayTimeOK = Ctrl.CtrlDelayedFlight.IsDelayTimeMeetFilterCriteria(flightRow, _lDelayMin);
                if (!blIsDelayTimeOK) return;

                string dcd1 = flightRow["DCD1"].Trim();
                string dcd2 = flightRow["DCD2"].Trim();
                string alc3 = flightRow["ALC3"];

                IRow[] lsDCFRows = _myDCF.RowsByIndexValue("FURN", flightRow["URNO"]);
                string flightInfo = GetFlightInfo(flightRow);
                bool isArrival = (flightRow["ADID"] == "A");

                if (lsDCFRows == null || lsDCFRows.Length < 1)
                {
                    if (dcd1 != "" && IsDelayCodeOkForAFT(_mapDelayFilter, dcd1))
                    {
                        SummarizeForAftDelay(dcd1, alc3, isArrival, flightInfo);
                    }

                    if ((dcd2 != "" || dcd1 == "" && dcd2 == "") && IsDelayCodeOkForAFT(_mapDelayFilter, dcd2))
                    {
                        SummarizeForAftDelay(dcd2, alc3, isArrival, flightInfo);
                    }
                }
                else
                {
                    foreach (IRow dcfRow in lsDCFRows)
                    {
                        if (IsDelayCodeOkForDCF(_mapDelayFilter, dcfRow["DECA"]) ||
                            IsDelayCodeOkForDCF(_mapDelayFilter, dcfRow["DECN"]) ||
                            (_useDecs && (IsDelayCodeOkForDCF(_mapDelayFilter, dcfRow["DECS"])))
                            )
                        {
                            //SummarizeForDCFDelay(dcfRow["DECA"], dcfRow["DECN"], dcfRow["DECS"], alc3, isArrival, flightInfo); //igu on 03/03/2010
                            SummarizeForDCFDelay(dcfRow["DECA"], dcfRow["DECN"], dcfRow["DECS"], 
                                (_useDecs ? dcfRow["MEAN"] : ""), dcfRow["DURN"], alc3, isArrival, flightInfo); //igu on 03/03/2010
                        }
                    }
                }

            }

            Dictionary<string, string> _mapDenDelayReasons = new Dictionary<string, string>();

            private string GetDelayReason(string strWhere)
            {
                return GetDelayReason(strWhere, false); //igu on 03/03/2010
            }

            //igu on 03/03/2010
            private string GetDelayReason(string strWhere, bool bIncludeDelayCodes)
            {
                string stReason = "";

                if (_mapDenDelayReasons.ContainsKey(strWhere)) stReason = _mapDenDelayReasons[strWhere];
                else
                {
                    string fieldList = "DECA,DECN,DENA";
                    string fieldLength = "2,2,75";
                    string fieldAlias = "DECA,DECN,DENA";

                    if (_useDecs)
                    {
                        fieldList += ",DECS";
                        fieldLength += ",2";
                        fieldAlias += ",DECS";
                    }

                    _myDB.Unbind("TEMP_DEN");
                    ITable tempDen = _myDB.Bind("TEMP_DEN", "DEN", fieldList, fieldLength, fieldAlias);
                    tempDen.Clear();
                    tempDen.Load(strWhere);

                    if (tempDen.Count > 0)
                    {
                        if (bIncludeDelayCodes)
                        {
                            stReason = tempDen[0]["DECA"] + " | " +
                                tempDen[0]["DECN"];
                            if (_useDecs)
                            {
                                stReason += " | " + tempDen[0]["DECS"];
                            }
                            stReason += "," + tempDen[0]["DENA"];
                        }
                        else
                        {
                            stReason = tempDen[0]["DENA"];
                        }
                    }
                    _mapDenDelayReasons.Add(strWhere, stReason);
                }
                return stReason;
            }

            //private void SummarizeForDCFDelay(string deca, string decn, string decs, string alc3, bool isArrival, string flightInfo) //igu on 03/03/2010
            private void SummarizeForDCFDelay(string deca, string decn, string decs, string mean, string durn, string alc3, bool isArrival, string flightInfo) //igu on 03/03/2010
            {
                string strDelayCode = deca + " | " + decn;
                string strDelayReason = "";
                //string strDenWhere = string.Format(
                //    "WHERE ALC3='{0}' AND DECA='{1}' AND DECN='{2}'",
                //    alc3, deca, decn); //igu on 03/03/2010
                string strDenWhere = string.Format(
                    "WHERE URNO={0}", durn); //igu on 03/03/2010
                if (_useDecs)
                {
                    strDelayCode += " | " + decs;
                    //strDenWhere += " AND DECS='" + decs + "'"; //igu on 03/03/2010
                }

                if (_useDecs && mean != "") //igu on 03/03/2010
                {
                    strDelayReason = mean; //igu on 03/03/2010
                }
                else //igu on 03/03/2010
                {
                    string delayData = GetDelayReason(strDenWhere, true);
                    if (delayData != "")
                    {
                        string[] delayArray = delayData.Split(',');
                        strDelayCode = delayArray[0];
                        strDelayReason = delayArray[1];
                    }
                }

                DelayCodeRowData delayCodeRowData;
                if (_mapDelayCodeInfo.ContainsKey(strDelayCode))
                {
                    delayCodeRowData = _mapDelayCodeInfo[strDelayCode];
                }
                else
                {
                    delayCodeRowData = new DelayCodeRowData();
                    delayCodeRowData.strAirline = alc3;
                    delayCodeRowData.strDelayCode = strDelayCode;
                    delayCodeRowData.strDelayReason = strDelayReason;
                
                    _mapDelayCodeInfo.Add(strDelayCode, delayCodeRowData);
                }

                if (delayCodeRowData.strFlightNo.IndexOf(flightInfo) == -1)
                {
                    delayCodeRowData.strFlightNo += flightInfo;
                    if (isArrival) delayCodeRowData.imNoOfArrivalFlights++;
                    else delayCodeRowData.imNoOfDepartureFlights++;
                }
            }

            private const string DELAY_REASON_WHERE_FOR_DENTAB = "WHERE (DECA = '@@DELCODE' OR DECN = '@@DELCODE')";

            private void SummarizeForAftDelay(string delayCode, string alc3, bool isArrival, string flightInfo)
            {
                if (string.IsNullOrEmpty(delayCode)) delayCode = "";
                else delayCode = delayCode.Trim();

                string strDelayCode = GetDelayCodeKey(delayCode, alc3);

                //if (delayCode.Length != 0)
                {
                    if (!_mapDelayCodeInfo.ContainsKey(strDelayCode))
                    {
                        DelayCodeRowData delayCodeRowData = new DelayCodeRowData();
                        delayCodeRowData.strAirline = alc3;
                        delayCodeRowData.strDelayCode = delayCode;

                        string strTmpWhere = DELAY_REASON_WHERE_FOR_DENTAB;
                        if (delayCode.Trim().Length != 0)
                        {
                            strTmpWhere = strTmpWhere.Replace("@@DELCODE", delayCode.Trim());
                        }
                        else
                        {
                            strTmpWhere = strTmpWhere.Replace("@@DELCODE", " ");
                        }
                        strTmpWhere += " AND ALC3 = '" + alc3 + "'";

                            delayCodeRowData.strDelayReason = GetDelayReason( strTmpWhere );


                        if (isArrival)
                        {
                            delayCodeRowData.imNoOfArrivalFlights++;
                        }
                        else
                        {
                            delayCodeRowData.imNoOfDepartureFlights++;
                        }
                        delayCodeRowData.strFlightNo += flightInfo;
                        _mapDelayCodeInfo.Add(strDelayCode, delayCodeRowData);
                    }
                    else
                    {
                        DelayCodeRowData delayCodeRowData = _mapDelayCodeInfo[strDelayCode];
                        if (delayCodeRowData.strFlightNo.IndexOf(flightInfo) == -1)
                        {
                            delayCodeRowData.strFlightNo += flightInfo;
                            if (isArrival)
                            {
                                delayCodeRowData.imNoOfArrivalFlights++;
                            }
                            else
                            {
                                delayCodeRowData.imNoOfDepartureFlights++;
                            }
                        }
                    }
                }
            }

        }


        /// <summary>
        /// PrepareDelayReasonReportData is used to prepare the data for the display.
        /// </summary>
        private void PrepareDelayReasonReportDataNew()
        {
            //myAFT = myDB["AFT"];
            //myDCF = myDB["DCF"];
            if (myAFT == null)
                return;

            StringBuilder sb = new StringBuilder(10000);

            if (txtDelay.Text.Length == 0)
                txtDelay.Text = "0";
            long llTxtDelay = Convert.ToInt64(txtDelay.Text);

            //DelayReasonInfo delaySummary = new DelayReasonInfo(cbDelayCodes.Text, llTxtDelay); igu on 26/01/2011

            //igu on 26/01/2011
            string deca = txtDECA.Text.Trim();
            string decn = txtDECN.Text.Trim();
            string decs = txtDECS.Text.Trim();

            DelayReasonInfo delaySummary = new DelayReasonInfo(deca, decn, decs, llTxtDelay, bmUseDecs);
            //
     
            for (int i = 0; i < myAFT.Count; i++)
            {
                delaySummary.Add(myAFT[i]);
            }

            foreach (DelayCodeRowData dRowData in delaySummary.MapDelayCodeInfo.Values)
            {
                sb.Append(dRowData.ToString() + ",\n");
            }

            tabResult.InsertBuffer(sb.ToString(), "\n");
            tabResult.Sort("0", true, true);
            tabResult.Refresh();
            lblResults.Text = "Report Results: (" + tabResult.GetLineCount().ToString() + ")";
        }

        private void txtDECN_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar))
                e.Handled = true; 
        }

        /*
        private void txtDelayCode_TextChanged(object sender, EventArgs e)
        {
            string delayName = "";
            string delayIndexName = "";
            string delayIndexValue = "";
            string delayAlpha = txtDECA.Text.Trim();
            string delayNumeric = txtDECN.Text.Trim();
            string delaySubcode = txtDECS.Text.Trim();
            string delayValue = "";
            string delayFields = "";
        
            if (delayAlpha != "")
            {
                delayValue += "," + delayAlpha;
                delayFields += ",DECA";

                delayIndexName = "DECA";
                delayIndexValue = delayAlpha;
            }
            if (delayNumeric != "")
            {
                delayValue += "," + delayNumeric;
                delayFields += ",DECN";
            }
            //if (Ctrl.CtrlDelayedFlight.GetInstance().UseDelayCodeDecs) //igu on 26/01/2011
            if (bmUseDecs) //igu on 26/01/2011
            {
                delayValue += "," + delaySubcode;
                delayFields += ",DECS";
            }
            if (delayValue.Length > 0) 
                delayValue = delayValue.Substring(1);
            if (delayFields.Length > 0) 
                delayFields = delayFields.Substring(1);

            if (delayAlpha == "" && delayNumeric != "")
            {
                delayIndexName = "DECN";
                delayIndexValue = delayNumeric;
            }
            
            if (delayIndexName != "")
            {
                string fullDelayFields = "DECA,DECN,DESC";
                string fullDelayValue = delayAlpha + "," + delayNumeric + "," + delaySubcode;
                
                IRow[] delayRows = myDEN.RowsByIndexValue(delayIndexName, delayIndexValue);
                List<int> foundRowIndexes = new List<int>();
                for (int i = 0; i < delayRows.Length; i++)
                {
                    //first priority, check every single delay code
                    string rowValue = delayRows[i].FieldValues(fullDelayFields);
                    if (fullDelayValue == rowValue)
                    {
                        foundRowIndexes.Clear();
                        foundRowIndexes.Add(i);
                        break;
                    }
                    else
                    {
                        if (fullDelayFields != delayFields)
                        {
                            rowValue = delayRows[i].FieldValues(delayFields);
                            if (delayValue == rowValue)
                            {
                                foundRowIndexes.Add(i);
                            }
                        }
                    }
                }

                if (foundRowIndexes.Count == 1)
                {
                    delayAlpha = delayRows[foundRowIndexes[0]]["DECA"];
                    delayNumeric = delayRows[foundRowIndexes[0]]["DECN"];
                    delayName = delayRows[foundRowIndexes[0]]["DENA"];
                }
                else
                {
                    delayAlpha = delayNumeric = string.Empty;
                }
            }

            if (string.IsNullOrEmpty(txtDECA.Text.Trim()) && (!string.IsNullOrEmpty(delayAlpha)))
                txtDECA.Text = delayAlpha;
            if (string.IsNullOrEmpty(txtDECN.Text.Trim()) && (!string.IsNullOrEmpty(delayNumeric)))
                txtDECN.Text = delayNumeric;
            txtDENA.Text = delayName;
        }
        */
        
        //igu on 23/05/2011
        private void txtDelayCode_TextChanged(object sender, EventArgs e)
        {
            if (!delayCodeChangesFromCode)
            {
                string delayAlpha = txtDECA.Text.Trim();
                string delayNumeric = txtDECN.Text.Trim();
                string delaySubcode = string.Empty;
                string delayName = string.Empty;
                string delayIndexName = "DECA";
                string delayIndexValue = delayAlpha;
                string fullDelayFields = "DECA,DECN";
                string fullDelayValue = delayAlpha + "," + delayNumeric;
                TextBox txtSender = (TextBox)sender;
                IRow[] delayRows = new IRow[] { };

                btnDelayLookup.Tag = ""; //igu on 31/05/2011

                if (bmUseDecs)
                {
                    delaySubcode = txtDECS.Text.Trim();
                    fullDelayFields += ",DECS";
                    fullDelayValue += "," + delaySubcode;
                }

                if (string.IsNullOrEmpty(delayAlpha) && string.IsNullOrEmpty(delayNumeric))
                {
                    if (!string.IsNullOrEmpty(delaySubcode))
                    {
                        delayName = "Delay code not found!";
                    }
                }
                else
                {
                    if (((txtSender == txtDECN) && (!string.IsNullOrEmpty(delayNumeric)))
                        || string.IsNullOrEmpty(delayAlpha))
                    {
                        delayIndexName = "DECN";
                        delayIndexValue = delayNumeric;
                    }

                    delayRows = myDEN.RowsByIndexValue(delayIndexName, delayIndexValue);
                    if (delayRows.Length > 0)
                    {
                        bool fullDelayCodeFound = false;
                        int mainDelayCodeCount = 0;
                        int delayCodePos = -1;
                        for (int i = 0; i < delayRows.Length; i++)
                        {
                            string rowValue = delayRows[i].FieldValues(fullDelayFields);
                            if (fullDelayValue == rowValue)
                            {
                                delayRows = new IRow[] { delayRows[i] };
                                fullDelayCodeFound = true;
                                break;
                            }

                            if (bmUseDecs)
                            {
                                if (string.IsNullOrEmpty(delayRows[i].FieldValues("DECS").Trim()))
                                {
                                    delayCodePos = i;
                                    mainDelayCodeCount++;
                                }
                            }
                        }

                        if (!fullDelayCodeFound)
                        {
                            if ((!autoPopulateDelayCodes) || (txtSender == txtDECS))
                            {
                                delayRows = new IRow[] { };
                            }
                            else
                            {
                                if (mainDelayCodeCount == 1)
                                {
                                    delayRows = new IRow[] { delayRows[delayCodePos] };
                                }
                            }
                        }
                    }

                    delayCodeChangesFromCode = true;

                    switch (delayRows.Length)
                    {
                        case 0:
                            delayName = "Delay code not found!";
                            break;
                        case 1:
                            if ((txtSender != txtDECA) || (string.IsNullOrEmpty(delayAlpha)))
                                txtDECA.Text = delayRows[0]["DECA"];
                            if ((txtSender != txtDECN) || (string.IsNullOrEmpty(delayNumeric)))
                                txtDECN.Text = delayRows[0]["DECN"];
                            if (bmUseDecs)
                                txtDECS.Text = delayRows[0]["DECS"];
                            delayName = delayRows[0]["DENA"];
                            btnDelayLookup.Tag = delayRows[0]["URNO"]; //igu on 31/05/2011
                            break;
                        default:
                            delayName = "Multiple entries found!";
                            break;
                            btnDelayLookup.Tag = delayRows[0]["URNO"]; //igu on 31/05/2011
                    }

                    delayCodeChangesFromCode = false;
                }

                txtDENA.Text = delayName;
                txtDENA.ForeColor = (delayRows.Length == 1 ? txtDECA.ForeColor : Color.Red);
                txtDENA.Refresh();

                if (tabLookup.Visible) FindInLookupTab(btnDelayLookup.Tag.ToString()); //igu on 31/05/2011
            }
        }

        //igu on 31/05/2011
        private void FindInLookupTab(string delayURNO)
        {
            int line = 0;

            if (!string.IsNullOrEmpty(delayURNO))
            {
                string lines = tabLookup.GetLinesByIndexValue("URNO", delayURNO, 0);
                if (!int.TryParse(lines, out line))
                    line = 0;
            }
            tabLookup.SetCurrentSelection(line);
            tabLookup.OnVScrollTo(line);
        }

        private void panelTop_MouseEnter(object sender, EventArgs e)
        {
            if (tabLookup.Visible) 
                tabLookup.Visible = false;
        }

        private void btnDelayLookup_Click(object sender, EventArgs e)
        {
            //igu on 31/05/2011            
            if (tabLookup.Visible)
            {
                tabLookup.Visible = false;
            }
            else
            {
                FindInLookupTab(btnDelayLookup.Tag.ToString());
                tabLookup.Visible = true;
            }
        }

        private void tabLookup_SendLButtonDblClick(object sender, AxTABLib._DTABEvents_SendLButtonDblClickEvent e)
        {
            delayCodeChangesFromCode = true;

            btnDelayLookup.Tag = tabLookup.GetFieldValue(e.lineNo, "URNO"); ; //igu on 31/05/2011
            txtDECA.Text = tabLookup.GetFieldValue(e.lineNo, "DECA");
            txtDECN.Text = tabLookup.GetFieldValue(e.lineNo, "DECN");
            txtDECS.Text = tabLookup.GetFieldValue(e.lineNo, "DECS");
            txtDENA.Text = tabLookup.GetFieldValue(e.lineNo, "DENA");

            txtDENA.ForeColor = txtDECA.ForeColor;

            delayCodeChangesFromCode = false;

            tabLookup.Visible = false;
        }

        private void txtAirline_Enter(object sender, EventArgs e)
        {
            if (tabLookup.Visible)
                tabLookup.Visible = false;
        }

        private void buttonHelp_MouseEnter(object sender, EventArgs e)
        {
            if (tabLookup.Visible)
                tabLookup.Visible = false;
        }
	}
}

