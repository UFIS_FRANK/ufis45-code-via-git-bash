
#include <stdafx.h>
#include <DataSet.h>
#include <BasicData.h>
//#include "CedaGhdData.h"
//#include "CedaDsrData.h"
#include <CedaFlightData.h>
#include <ConflictCheck.h>
#include <CedaPfcData.h>
#include <CedaPerData.h>

// General macros for testing a point against an interval
#define IsBetween(val, start, end)  ((start) <= (val) && (val) <= (end))
#define IsOverlapped(start1, end1, start2, end2)    ((start1) <= (end2) && (start2) <= (end1))
#define IsReallyOverlapped(start1, end1, start2, end2)	((start1) < (end2) && (start2) < (end1))
#define IsWithIn(start1, end1, start2, end2)	((start1) >= (start2) && (end1) <= (end2))


ConflictCheck::ConflictCheck()
{
}

ConflictCheck::~ConflictCheck()
{
	omPtrMap.RemoveAll();
	omData.DeleteAll();
}


void ConflictCheck::AddConflict(long ipErrorNo, long lpFlightUrno, long lpFkey, long lpGhdUrno, long lpDsrUrno, CString opText)
{
}

void ConflictCheck::QuickCheckAllConflicts()
{
}

void ConflictCheck::CheckAllConflicts()
{
}

void ConflictCheck::DeleteConflict(long lpFlightUrno, int ipCfiType)
{
	int ilCount = omData.GetSize();
 	for(int i = ilCount-1; i >= 0; i--)
	{
		if((omData[i].Type == ipCfiType) && (omData[i].FlightUrno == lpFlightUrno))
		{
			if(omData[i].Status == CFI_NOTACCEPTED)
			{
				omPtrMap.RemoveKey(&omData[i]);
				omData.DeleteAt(i);
			}
		}
	}
}
/******************
void ConflictCheck::DeleteConflict(GHDDATA *prpGhd, DSRDATA *prpDsr, int ipCfiType)
{

}

void ConflictCheck::AcceptConflict(GHDDATA *prpGhd, DSRDATA *prpDsr, int ipCfiType)
{

}
**********/

bool ConflictCheck::CheckArray()
{
	bool blRet = true;
	
	for(int ilCfls=omData.GetSize()-1;ilCfls>=0;ilCfls--)
	{
		void *prlPtr = (void *)&omData[ilCfls];
		if(_CrtIsValidHeapPointer(prlPtr)==FALSE)
		{
			blRet = false;
			break;
		}
	}
	return blRet;

}

CONFLICTENTRY *ConflictCheck::GetDataAt(int i)
{
	CONFLICTENTRY *prlConflictEntry = NULL;
	if(i<omData.GetSize())
	{
		prlConflictEntry = &omData[i];
	}
	return prlConflictEntry;
}

bool ConflictCheck::AddInternal(CONFLICTENTRY *prpConflict)
{
	bool blRet = true;
	CheckArray();
	long llConflict=0L;
	if(omPtrMap.Lookup((void *)prpConflict,(void *&)llConflict)==FALSE)
	{
		omPtrMap.SetAt((void *)prpConflict,NULL);
		omData.Add(prpConflict);
	}
	else
	{
		TRACE("ConflictCheck: This pointer already exists!\n");
		blRet = false;
		CONFLICTENTRY *prlConflict = new CONFLICTENTRY;
		if(prlConflict!=NULL)
		{
			*prlConflict = *prpConflict;
			if(omPtrMap.Lookup((void *)prlConflict,(void *&)llConflict)==FALSE)
			{
				omPtrMap.SetAt((void *)prlConflict,NULL);
				omData.Add(prlConflict);
				blRet = true;
			}
		}
	}
	return blRet;
}
