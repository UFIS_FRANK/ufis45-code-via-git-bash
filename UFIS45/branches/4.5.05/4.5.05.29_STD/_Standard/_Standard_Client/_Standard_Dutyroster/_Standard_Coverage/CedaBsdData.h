// CedaBsdData.h
 
#ifndef __CEDAPBSDDATA__
#define __CEDAPBSDDATA__
 
#include <stdafx.h>
#include <basicdata.h>

//---------------------------------------------------------------------------------------------------------

struct BSDDATA 
{
	char 	 Bewc[5]; 	// Bewertungsfaktor.code
	char 	 Bkd1[6]; 	// Pausenlänge
	char 	 Bkf1[6]; 	// Pausenlage von
	char 	 Bkr1[3]; 	// Pausenlage relativ zu Schichtbeginn oder absolut
	char 	 Bkt1[6]; 	// Pausenlage bis
	char 	 Bsdc[9]; 	// Code
	char 	 Bsdk[14]; 	// Kurzbezeichnung
	char 	 Bsdn[42]; 	// Bezeichnung
	char 	 Bsds[5]; 	// SAP-Code
	CTime 	 Cdat; 	// Erstellungsdatum
	char 	 Ctrc[6]; 	// Vertragsart.code
	char 	 Esbg[6]; 	// Frühester Schichtbeginn
	char 	 Lsen[6]; 	// Spätestes Schichtende
	CTime 	 Lstu; 	// Datum letzte Änderung
	char 	 Prfl[3]; 	// Protokollierungskennung
	char 	 Sdu1[6]; 	// Reguläre Schichtdauer
	char 	 Sex1[6]; 	// Mögliche Arbeitszeitver-längerung
	char 	 Ssh1[6]; 	// Mögliche Arbeitszeitverkürzung
	char 	 Type[3]; 	// Flag (statisch/dynamisch)
	long 	 Urno; 	// Eindeutige Datensatz-Nr.
	char     Dpt1[9] ; //Organisationseinheit
	char     Fctc[7] ; //Functionscode
	char     Fdel[2] ; //Abordnung existiert
	char	 Rgsb[6];  // Regulärer Schichtbeginn
	CTime    Vafr;
	CTime    Vato;

	//DataCreated by this class
	int      IsChanged;
	BOOL	 IsSelected;
	CTime    From;
	CTime	 To;
	CTime	 BreakFrom;
	CTime	 BreakTo;
	CTime	 BreakTimeFrameFrom;
	CTime	 BreakTimeFrameTo;
	//long, CTime
	BSDDATA(void)
	{
		memset(this,'\0',sizeof(*this));
		Vafr=-1;
		Vato=-1;
		IsSelected = FALSE;
	// Cdat=-1,Lstu=-1; //<zB.(FIELD_DATE Felder)
	}

}; // end BSDDataStruct

//---------------------------------------------------------------------------------------------------------
// Class declaratino

class CedaBsdData: public CCSCedaData
{
// Attributes
public:
    CMapPtrToPtr omUrnoMap;
    CMapStringToPtr omBsdcMap;
	CUIntArray omSelectedData;
    CCSPtrArray<BSDDATA> omData;

	char pcmListOfFields[2048];

	void SetTableName(CString opTableName)
	{
		strcpy(pcmTableName, opTableName.GetBuffer(0));
	}
// OBsdations
public:
    CedaBsdData();
	~CedaBsdData();
//	virtual void GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType);
	void Register(void);
	void ClearAll(bool bpWithRegistration = true);
    bool Read(char *pspWhere = NULL);
	bool Insert(BSDDATA *prpBsd);
	bool InsertInternal(BSDDATA *prpBsd);
	bool Update(BSDDATA *prpBsd);
	bool UpdateInternal(BSDDATA *prpBsd);
	bool Delete(long lpUrno);
	bool DeleteInternal(BSDDATA *prpBsd);
	bool ReadSpecialData(CCSPtrArray<BSDDATA> *popBsd,char *pspWhere,char *pspFieldList,bool ipSYS=true);
	bool Save(BSDDATA *prpBsd);
	void ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
	void MakeBsdSelected(BSDDATA *prpBsd, BOOL bpSelect);
	bool GetSelectedBsd(CCSPtrArray<BSDDATA> *popSelectedBsd);
	void DeSelectAll();
	BSDDATA  *GetBsdByUrno(long lpUrno);
	BSDDATA *GetBsdByBsdc(CString opBsdc);

	void GetAllBsds(CStringArray &ropPstFilter);
	void GetAllBsdsOrderedByTime(CCSPtrArray<BSDDATA>& ropBsdList);

	// Private methods
private:
    void PrepareBsdData(BSDDATA *prpBsdData);

};

//---------------------------------------------------------------------------------------------------------

extern CedaBsdData ogBsdData;
extern CedaBsdData ogAllBsdData;

#endif //__CEDAPBSDDATA__
