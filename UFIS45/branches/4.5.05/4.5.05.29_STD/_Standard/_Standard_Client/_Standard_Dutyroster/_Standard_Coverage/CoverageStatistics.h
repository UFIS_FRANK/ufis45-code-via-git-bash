#if !defined(AFX_COVERAGESTATISTICS_H__55CE01A3_AF72_11D2_993A_0000B43C16D8__INCLUDED_)
#define AFX_COVERAGESTATISTICS_H__55CE01A3_AF72_11D2_993A_0000B43C16D8__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// CoverageStatistics.h : header file
//
#include <CoverageDiaViewer.h>
/////////////////////////////////////////////////////////////////////////////
// CCoverageStatistics dialog


class CCoverageStatistics : public CDialog
{
// Construction
public:
	CCoverageStatistics(CWnd* pParent = NULL,CoverageDiagramViewer *popViewer=NULL);   // standard constructor
	//CCoverageStatistics(CoverageDiagramViewer *popViewer,CWnd* pParent = NULL /*=NULL*/);
	~CCoverageStatistics();

	void UpdateDisplay(CUIntArray &opShiftArray,CUIntArray &opFlightValues) ;
	void AttachViewer(CoverageDiagramViewer *popViewer);
	
	CWnd *pomParent;
	CoverageDiagramViewer *pomViewer;
	CCSTimeScale *pomTimeScale;
	CedaDemData *pomDemandData;
	CUIntArray omShiftArray,omFlightValues;
	int imExcess;
	int imDeficit;
	int imDemand;

	CTime omStartTime;
	CTime omEndTime;
	CTime omLoadStartTime;
	CTime omLoadEndTime;
	int imXMinutes;
	int imMaxYCount;
	int imCountPerUnit;
	double imOneMinute;
	int imOneShift;
	//bool bmAdditive;
// Dialog Data
	//{{AFX_DATA(CCoverageStatistics)
	enum { IDD = IDD_COVERAGE_STATISTICS };
	CEdit	m_Under;
	CEdit	m_Over;
	CEdit	m_ToTime;
	CEdit	m_ToDate;
	CEdit	m_FromTime;
	CEdit	m_FromDate;
	CEdit	m_Worktime;
	CEdit	m_Demandtime;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCoverageStatistics)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CCoverageStatistics)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	afx_msg void OnKillfocusToTime();
	afx_msg void OnKillfocusToDate();
	afx_msg void OnKillfocusFromTime();
	afx_msg void OnKillfocusFromDate();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:
	void Evaluate();
	int imOffSet;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_COVERAGESTATISTICS_H__55CE01A3_AF72_11D2_993A_0000B43C16D8__INCLUDED_)
