// MainFrm.cpp : implementation of the CMainFrame class
//

#include <stdafx.h>
#include <Coverage.h>

#include <MainFrm.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMainFrame


IMPLEMENT_DYNAMIC(CMainFrame, CovDiagram)

BEGIN_MESSAGE_MAP(CMainFrame, CovDiagram)
	//{{AFX_MSG_MAP(CMainFrame)
	ON_WM_CREATE()
    ON_WM_GETMINMAXINFO()
	ON_WM_SETFOCUS()
	ON_WM_SIZE()
	ON_WM_ERASEBKGND()
	ON_WM_HSCROLL()
	ON_WM_PAINT()
	ON_BN_CLICKED(IDC_PREV, OnNextChart)
    ON_BN_CLICKED(IDC_NEXT, OnPrevChart)
    ON_WM_TIMER()
    ON_MESSAGE(WM_DEACTIVEAUTOCOV, OnDeactivateAutoCov)
    ON_MESSAGE(WM_ACTIVEAUTOCOV, OnActivateAutoCov)
 	ON_MESSAGE(WM_POSITIONCHILD, OnPositionChild)
 	ON_MESSAGE(WM_UPDATE_COVERAGE, UpdateGraphic)
	ON_MESSAGE(WM_UPDATE_BASESHIFT, UpdateBaseShift)
	ON_MESSAGE(WM_UPDATEDEMANDGANTT, UpdateDemandGantt)
	ON_MESSAGE(WM_DEMAND_UPDATECOMBO, UpdateDemandCombo)
    ON_MESSAGE(WM_UPDATEDIAGRAM, OnUpdateDiagram)
	ON_CBN_SELCHANGE(IDC_VIEW, OnViewSelChange)
	ON_CBN_CLOSEUP(IDC_VIEW, OnCloseupView)
	ON_COMMAND(IDC_TOGGLEBUTTON, OnAnsicht)
	ON_COMMAND(IDP_PLACEHOLDER, OnCombobox)
	ON_COMMAND(ID_SIMULATION, OnSimulation)
	ON_COMMAND(ID_STATISTIC, OnStatistic)
	ON_COMMAND(ID_KONFLICT, OnKonflict)
	ON_COMMAND(ID_CREATEDEM, OnCreateDem)
	ON_COMMAND(ID_AUTOCOV, OnAutoCov)
	ON_COMMAND(ID_DRUCKEN, OnDrucken)
	ON_COMMAND(ID_BEWERTUNG, OnBewertung)
	ON_COMMAND(ID_FLIGHT_NO_RES, OnFlightNoRes)
	ON_WM_SIZING()
	ON_COMMAND(ID_LOADTIMEFRAME, OnLoadtimeframe)
	ON_COMMAND(ID_MASSTAB, OnMasstab)
	ON_COMMAND(ID_SEARCH, OnSearch)
	ON_MESSAGE(WM_SEARCH_EXIT, OnSearchExit)
	ON_WM_CLOSE()
	ON_MESSAGE(WM_BCADD,OnBcAdd)
	ON_WM_KEYDOWN()
	ON_WM_KEYUP()
	ON_MESSAGE(WM_SIMULATE, OnSimulate)
	ON_MESSAGE(WM_MARKTIME, OnSetMarkTime)
	ON_COMMAND(ID_VIEWBUTTON, OnViewbutton)
	ON_NOTIFY_EX_RANGE(TTN_NEEDTEXTW, 0, 0xFFFF, OnToolTipText)
	ON_NOTIFY_EX_RANGE(TTN_NEEDTEXTA, 0, 0xFFFF, OnToolTipText)
	ON_COMMAND(ID_HELP_FINDER, CFrameWnd::OnHelpFinder)
	ON_COMMAND(ID_HELP, CFrameWnd::OnHelp)
	ON_COMMAND(ID_CONTEXT_HELP, CFrameWnd::OnContextHelp)
	ON_COMMAND(ID_DEFAULT_HELP, CFrameWnd::OnHelpFinder)

	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

static UINT indicators[] =
{
	ID_SEPARATOR,           // status line indicator
	ID_INDICATOR_CAPS,
	ID_INDICATOR_NUM,
	ID_INDICATOR_SCRL,
};

/////////////////////////////////////////////////////////////////////////////
// CMainFrame construction/destruction

CMainFrame::CMainFrame()
{
	// TODO: add member initialization code here
	
}

CMainFrame::~CMainFrame()
{
}

int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CovDiagram::OnCreate(lpCreateStruct) == -1)
	return -1;
/*********************************************
	if (CFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;
	// create a view to occupy the client area of the frame
	if (!m_wndView.Create(NULL, NULL, AFX_WS_DEFAULT_VIEW,
		CRect(0, 0, 0, 0), this, AFX_IDW_PANE_FIRST, NULL))
	{
		TRACE0("Failed to create view window\n");
		return -1;
	}
	
	if (!m_wndToolBar.CreateEx(this, TBSTYLE_FLAT, WS_CHILD | WS_VISIBLE | CBRS_TOP
		| CBRS_GRIPPER | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC) ||
		!m_wndToolBar.LoadToolBar(IDR_MAINFRAME))
	{
		TRACE0("Failed to create toolbar\n");
		return -1;      // fail to create
	}

	if (!m_wndStatusBar.Create(this) ||
		!m_wndStatusBar.SetIndicators(indicators,
		  sizeof(indicators)/sizeof(UINT)))
	{
		TRACE0("Failed to create status bar\n");
		return -1;      // fail to create
	}

	// TODO: Delete these three lines if you don't want the toolbar to
	//  be dockable
	m_wndToolBar.EnableDocking(CBRS_ALIGN_ANY);
	EnableDocking(CBRS_ALIGN_ANY);
	DockControlBar(&m_wndToolBar);
***********************************/

	char pclConfigPath[142];
    if (getenv("CEDA") == NULL)
        strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
    else
        strcpy(pclConfigPath, getenv("CEDA"));


	char pclUseBcProxy[256];
	GetPrivateProfileString("GLOBAL", "BCEVENTTYPE", "BCSERV",pclUseBcProxy, sizeof pclUseBcProxy, pclConfigPath);

	//MWO/RRO 25.03.03
	if (stricmp(pclUseBcProxy,"BCPROXY") == 0 || stricmp(pclUseBcProxy,"BCCOM") == 0)
	{
		//MWO/RRO
		//ogCommHandler.RegisterBcWindow(this);
		ogBcHandle.StartBc(); 
		//END MWO/RRO
	}
	else
	{
		ogCommHandler.RegisterBcWindow(this);
	}
	return 0;
}

BOOL CMainFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	if( !CFrameWnd::PreCreateWindow(cs) )
		return FALSE;
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.lpszClass = AfxRegisterWndClass(0);
	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////
// CMainFrame diagnostics

#ifdef _DEBUG
void CMainFrame::AssertValid() const
{
	CFrameWnd::AssertValid();
}

void CMainFrame::Dump(CDumpContext& dc) const
{
	CFrameWnd::Dump(dc);
}

#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CMainFrame message handlers
void CMainFrame::OnSetFocus(CWnd* pOldWnd)
{
	// forward focus to the view window
	//m_wndView.SetFocus();
}

BOOL CMainFrame::OnCmdMsg(UINT nID, int nCode, void* pExtra, AFX_CMDHANDLERINFO* pHandlerInfo)
{
	// let the view have first crack at the command
	if (m_wndView.OnCmdMsg(nID, nCode, pExtra, pHandlerInfo))
		return TRUE;

	// otherwise, do default handling
	return CFrameWnd::OnCmdMsg(nID, nCode, pExtra, pHandlerInfo);
}


void CMainFrame::OnSize(UINT nType, int cx, int cy) 
{
	CovDiagram::OnSize(nType, cx, cy);
	
	// TODO: Add your message handler code here
	
}

BOOL CMainFrame::OnEraseBkgnd(CDC* pDC) 
{
	// TODO: Add your message handler code here and/or call default
	
	return CovDiagram::OnEraseBkgnd(pDC);
}

void CMainFrame::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar) 
{
	// TODO: Add your message handler code here and/or call default
	//MWO:P 01.04.03
	int ilPos;
	CTime olEndTime;
	if(pScrollBar != NULL)
	{
		switch (nSBCode)
		{
			case SB_THUMBTRACK /* pressed, any drag time */:
				
				olEndTime = omStartTime + omDuration;
				pogCoverageDiagram->omTSDuration = CTimeSpan(0,pogCoverageDiagram->omTimeSlider.GetPos() / ONE_HOUR_POINT,0,0);
				pogCoverageDiagram->omTSDuration = min(pogCoverageDiagram->omTSDuration,pogCoverageDiagram->omDuration);
				pogCoverageDiagram->omTSDuration = min(pogCoverageDiagram->omTSDuration,olEndTime - pogCoverageDiagram->omTSStartTime);

				ilPos = pogCoverageDiagram->omTSDuration.GetTotalHours();
				pogCoverageDiagram->omTimeSlider.SetPos(ilPos * ONE_HOUR_POINT);
				pogCoverageDiagram->omViewer.SetGeometryTimeSpan(omTSDuration);

				pogCoverageDiagram->omTimeScale.SetDisplayTimeFrame(pogCoverageDiagram->omTSStartTime, pogCoverageDiagram->omTSDuration, pogCoverageDiagram->omTSInterval);
				pogCoverageDiagram->omTimeScale.Invalidate(TRUE);
				break;
			default:
				CovDiagram::OnHScroll(nSBCode, nPos, pScrollBar);

		}
	}
	else
	{
		CovDiagram::OnHScroll(nSBCode, nPos, pScrollBar);
	}
	//END MWO:P 01.04.03
}

void CMainFrame::OnPaint() 
{
//	CPaintDC dc(this); // device context for painting

	CovDiagram::OnPaint();	
	
	// TODO: Add your message handler code here
	
	// Do not call CFrameWnd::OnPaint() for painting messages
}

LONG CMainFrame::OnPositionChild(WPARAM wParam, LPARAM lParam)
{
	CCS_TRY
	CovDiagram::OnPositionChild(wParam,  lParam);	

	CCS_CATCH_ALL
    return 0L;
}

LONG CMainFrame::OnActivateAutoCov(WPARAM wParam, LPARAM lParam)
{
	CCS_TRY
	CovDiagram::OnActivateAutoCov(wParam,  lParam);	

	CCS_CATCH_ALL
    return 0L;
}

LONG CMainFrame::OnDeactivateAutoCov(WPARAM wParam, LPARAM lParam)
{
	CCS_TRY
	CovDiagram::OnDeactivateAutoCov(wParam,  lParam);	

	CCS_CATCH_ALL
    return 0L;
}

void CMainFrame::OnAnsicht() 
{
	CovDiagram::OnAnsicht();
	
}

void CMainFrame::OnCombobox() 
{
	CovDiagram::OnViewSelChange();
	
}

void CMainFrame::OnSimulation() 
{
	CovDiagram::OnSimulation();
	
}

void CMainFrame::OnStatistic() 
{
	CovDiagram::OnStatistics();
	
}

void CMainFrame::OnKonflict() 
{
	CovDiagram::OnConflicts();
	
}
void CMainFrame::OnCreateDem() 
{
	CovDiagram::OnCreateDem();
	
}

void CMainFrame::OnAutoCov() 
{
	CovDiagram::OnAutoCov();
	
}

void CMainFrame::OnDrucken() 
{
	CovDiagram::OnPrintCoverage();	
}

void CMainFrame::OnBewertung() 
{
	CovDiagram::OnCheckDemands();	
	
}

void CMainFrame::OnFlightNoRes() 
{
	CovDiagram::OnNoResList();	
	
}
void CMainFrame::OnNextChart()
{
	CovDiagram::OnNextChart();	
}

void CMainFrame::OnPrevChart()
{
	CovDiagram::OnPrevChart();	
}


void CMainFrame::OnTimer(UINT nIDEvent)
{
	CovDiagram::OnTimer(nIDEvent);	
}

void CMainFrame::OnGetMinMaxInfo(MINMAXINFO FAR* lpMMI) 
{
	CovDiagram::OnGetMinMaxInfo(lpMMI) ;
}

LONG CMainFrame::OnUpdateDiagram(WPARAM wParam, LPARAM lParam)
{
	return CovDiagram::OnUpdateDiagram(wParam, lParam) ;

}

LONG CMainFrame::UpdateDemandGantt(WPARAM wParam, LPARAM lParam)
{
	return CovDiagram::UpdateDemandGantt(wParam, lParam) ;
}

LONG CMainFrame::UpdateDemandCombo(WPARAM wParam, LPARAM lParam)
{
	return CovDiagram::UpdateDemandCombo(wParam, lParam) ;
}

LONG CMainFrame::UpdateBaseShift(WPARAM wParam, LPARAM lParam)
{
	return CovDiagram::UpdateBaseShift(wParam, lParam) ;
}
LONG CMainFrame::UpdateGraphic(WPARAM wParam, LPARAM lParam)
{
	return CovDiagram::UpdateGraphic(wParam, lParam) ;
}

void CMainFrame::OnCloseupView()
{
	CovDiagram::OnCloseupView();	
}

void CMainFrame::OnViewSelChange()
{
	CovDiagram::OnViewSelChange();	
}



void CMainFrame::OnSizing(UINT fwSide, LPRECT pRect) 
{
	CFrameWnd::OnSizing(fwSide, pRect);
	
int i= 1;	
}

void CMainFrame::OnLoadtimeframe() 
{
	
	CovDiagram::OnLoadtimeframe() ;
}

void CMainFrame::OnMasstab() 
{
	CovDiagram::OnMasstab() ;	
}

void CMainFrame::OnSearch() 
{
	CovDiagram::OnSearch() ;	
	
}

void CMainFrame::OnSearchExit() 
{
	CovDiagram::OnSearchExit() ;	
	
}

LONG CMainFrame::OnBcAdd(UINT wParam, LONG lParam)
{
	return CovDiagram::OnBcAdd(wParam,lParam);	
	
}


void CMainFrame::OnDestroy() 
{
	CovDiagram::OnDestroy() ;
}


void CMainFrame::OnClose() 
{
	// TODO: Add your message handler code here and/or call default
	//uhi 23.4.01
	if (MessageBox(LoadStg(IDS_EXIT), LoadStg(IDR_MAINFRAME), MB_YESNO | MB_ICONQUESTION) == IDYES)

	CFrameWnd::OnClose();
}


void CMainFrame::OnViewbutton() 
{
	CovDiagram::OnAnsicht();
	
}


bool CMainFrame::OnToolTipText(UINT, NMHDR *pNMHDR, LRESULT *pResult)
{
	return CovDiagram::OnToolTipText(0, pNMHDR, pResult);
}

void CMainFrame::GetMessageString(UINT nID, CString &rMessage) const
{
	rMessage = LoadStg(nID);
	rMessage.Replace('\n','\0');
}

