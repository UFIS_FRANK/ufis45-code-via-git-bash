#if !defined(AFX_COPYSHIFTROSTERREQUIREMENTDLG_H__42669333_01F8_11D7_BFEF_00010215BFDE__INCLUDED_)
#define AFX_COPYSHIFTROSTERREQUIREMENTDLG_H__42669333_01F8_11D7_BFEF_00010215BFDE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// CopyShiftRosterRequirementDlg.h : header file
//
#include <CCSEdit.h>

/////////////////////////////////////////////////////////////////////////////
// CopyShiftRosterRequirementDlg dialog

class CopyShiftRosterRequirementDlg : public CDialog
{
// Construction
public:
	CopyShiftRosterRequirementDlg(CWnd* pParent = NULL);   // standard constructor

	CString	NewName();
	CString	ReplaceFunction();
	CString	WithFunction();
	UINT	CountOfCopies();	

protected:
// Dialog Data
	//{{AFX_DATA(CopyShiftRosterRequirementDlg)
	enum { IDD = IDD_COPY_SRR };
	CSpinButtonCtrl	m_SpinCopies;
	CComboBox		m_ComboWith;
	CComboBox		m_ComboReplace;
	UINT			m_CountOfCopies;
	CCSEdit			m_EditName;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CopyShiftRosterRequirementDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CopyShiftRosterRequirementDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSelchangeComboReplace();
	afx_msg void OnSelchangeComboWith();
	afx_msg void OnUpdateEditName();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	void FillComboBoxes();
private:
	CString	omNewName;
	CString	omReplaceFunction;
	CString	omWithFunction;
	UINT	imCountOfCopies;

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_COPYSHIFTROSTERREQUIREMENTDLG_H__42669333_01F8_11D7_BFEF_00010215BFDE__INCLUDED_)
