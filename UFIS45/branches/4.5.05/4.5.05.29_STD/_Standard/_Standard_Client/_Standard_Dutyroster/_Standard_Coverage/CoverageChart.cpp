// stafchrt.cpp : implementation file
//

#include <stdafx.h>
#include <CCSGlobl.h>
#include <resource.h>
#include <Coverage.h>
#include <CCSButtonCtrl.h>
#include <CCSClientWnd.h>
#include <CCSTimeScale.h>
#include <ccsdragdropctrl.h>
#include <CoverageDiaViewer.h>
//#include <StaffGantt.h>
#include <CovDiagram.h>
#include <DataSet.h>
#include <BewertungPropertySheet.h>
#include <CedaSdgData.h>
#include <MasterShiftDemand.h>
#include <CCSParam.h>

#include <ResultTable.h>
#include <BaseShiftsPropertySheet.h>
#include <CedaBasicData.h>
#include <Basicdata.h>
#include <DelSdgDlg.h>
#include <CedaDrrData.h>
#include <CedaPfcData.h>
//----------------------------------------------------------------------

#include <CoverageChart.h>
#include <process.h>

#ifdef _DEBUG
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CoverageChart

IMPLEMENT_DYNCREATE(CoverageChart, CFrameWnd)

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


static int OrderByEsbg(const SDTDATA **e1, const SDTDATA **e2)
{
	if ((**e1).Esbg < (**e2).Esbg)
		return -1;
	else if ((**e1).Esbg > (**e2).Esbg)
		return 1;
	else
	{
		if ((**e1).Lsen < (**e2).Lsen)
			return -1;
		else if ((**e1).Lsen > (**e2).Lsen)
			return 1;
		else
			return 0;
	}
}

CoverageChart::CoverageChart()
: omDemUpdate(TRUE)
{
	CCS_TRY
    pomTimeScale = NULL;
    pomStatusBar = NULL;
    pomViewer = NULL;
    pomResultTable = NULL;
    pomTopScaleText = NULL;
    pomPIdxText = NULL;
	pomCountText = NULL;
	imStartVerticalScalePos = 30;    
	imStartTopScaleTextPos = 120;
	lmBkColor = lgBkColor;
	bmAdditive = true;
	bmReleaseIsRed = false;
	bmNoReload = false;
	bmUpdateIsRed = false;
	bmOperEnabled = ogCCSParam.GetParamValue(ogGlobal,"ID_OPERATION_FL",CTime::GetCurrentTime().Format("%Y%m%d%H%M%S"),true) == "Y" ? true : false;
	CCS_CATCH_ALL
}

CoverageChart::CoverageChart(int ipChartType)
: omDemUpdate(TRUE)
{
	CCS_TRY
	//omGantt.SetViewer(GetViewer(), ipChartType);

	imChartType = ipChartType;
    pomTimeScale = NULL;
    pomStatusBar = NULL;
    pomViewer = NULL;
    pomResultTable = NULL;
    pomTopScaleText = NULL;
    pomPIdxText = NULL;
	pomCountText = NULL;
	imStartVerticalScalePos = 30;    
	imStartTopScaleTextPos = 120;
	lmBkColor = lgBkColor;
	bmAdditive = true;
	bmIsInit = true;
	bmNoReload = false;
	bmOptBreakIsActive = true;
	bmUpdateIsRed = false;
	bmOperEnabled = ogCCSParam.GetParamValue(ogGlobal,"ID_OPERATION_FL",CTime::GetCurrentTime().Format("%Y%m%d%H%M%S"),true) == "Y" ? true : false;
	CCS_CATCH_ALL
}

CoverageChart::~CoverageChart()
{
	CCS_TRY
		
		if (pomPIdxText != NULL)
			delete pomPIdxText;
		if (pomTopScaleText != NULL)
			delete pomTopScaleText;
		if (pomCountText != NULL)
			delete pomCountText;
		if(pomResultTable!=NULL)
			delete pomResultTable;
	CCS_CATCH_ALL
}

BEGIN_MESSAGE_MAP(CoverageChart, CFrameWnd)
    //{{AFX_MSG_MAP(CoverageChart)
    ON_WM_CREATE()
	ON_WM_DESTROY()
    ON_WM_SIZE()
    ON_WM_ERASEBKGND()
    ON_WM_PAINT()
    ON_BN_CLICKED(IDC_CHARTBUTTON, OnChartButton)
	ON_BN_CLICKED(IDC_CHARTBUTTON2, OnChartButton2)
	ON_BN_CLICKED(IDC_CHARTBUTTON4, OnChartButton4)
	ON_BN_CLICKED(IDC_CHARTBUTTON5, OnChartButton5)
	ON_BN_CLICKED(IDC_CHARTBUTTON3, OnChartButton3)
	ON_BN_CLICKED(IDC_CHARTBUTTON6, OnChartButton6)
	ON_BN_CLICKED(IDC_AUTOCOVRELEASE, OnAutoCovRelease)
	ON_BN_CLICKED(IDC_AUTOCOVDISCARD, OnAutoCovDiscard)
	ON_BN_CLICKED(IDC_CHARTBUTTON7, OnChartButton7)
    ON_MESSAGE(WM_CCSBUTTON_RBUTTONDOWN, OnChartButtonRButtonDown)
    ON_MESSAGE(WM_DRAGOVER, OnDragOver)
    ON_MESSAGE(WM_DROP, OnDrop)  
	ON_MESSAGE(WM_COV_RELOADCOMBO, OnReloadCombo)
	ON_MESSAGE(WM_COV_UPDATECOMBO, OnUpdateCombo)
	ON_COMMAND(31, OnMenuAssign)
	ON_CBN_SELCHANGE(IDC_CHARTCB, OnSelChangeCB)
	ON_BN_CLICKED(IDC_DEMUPDATE, OnDemUpdate)
	ON_WM_KEYDOWN()
	ON_WM_KEYUP()
	ON_BN_CLICKED(IDC_EXCELBUTTON, OnExcelButton)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


int CoverageChart::GetHeight()
{
	int ilHeight = 0;
	CCS_TRY
	switch(imChartType)
	{
	case COVERAGE_GRAPHIC:
		{
			if (imState == Minimized)
				ilHeight = imStartVerticalScalePos; // height of ChartButtton/TopScaleText
			else                                // (imState == Normal) || (imState == Maximized)
				ilHeight = imStartVerticalScalePos + 275 + 2;
				//ilHeight = imStartVerticalScalePos + 350 + 2;
		}
		break;

	case COVERAGE_SHIFTDEMANDS:
		{
			if (imState == Minimized)
				ilHeight = imStartVerticalScalePos; // height of ChartButtton/TopScaleText
			else                                // (imState == Normal) || (imState == Maximized)
				ilHeight = imStartVerticalScalePos + 120 + 2;
	
		}
		break;
	case COVERAGE_BASESHIFTS:
		{
			if (imState == Minimized)
				ilHeight = imStartVerticalScalePos; // height of ChartButtton/TopScaleText
			else                                // (imState == Normal) || (imState == Maximized)
				ilHeight = imStartVerticalScalePos + omGantt.GetGanttChartHeight() + 2;
		}
		break;
	}
	
	return ilHeight ;
	CCS_CATCH_ALL
	return ilHeight ;
        //return imStartVerticalScalePos + imHeight + 2;
}

/////////////////////////////////////////////////////////////////////////////
// CoverageChart message handlers


int CoverageChart::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
 	CCS_TRY
   if (CFrameWnd::OnCreate(lpCreateStruct) == -1)
        return -1;
    
    // TODO: Add your specialized creation code here
    CString olStr; GetWindowText(olStr);
    omButton.Create(olStr, BS_OWNERDRAW | WS_CHILD | WS_VISIBLE, CRect(5, 4, 110, 4 + 20), this, IDC_CHARTBUTTON);
    omButton.SetFont(&ogMSSansSerif_Bold_8, FALSE);
   
	CRect olRect; GetClientRect(&olRect);
	CRect olRectTopS(imStartTopScaleTextPos + 6 + 350, 2, (int)((olRect.right / 2) /* * 3*/) , 23);


	m_ChartWindowDragDrop.RegisterTarget(this, this);
    m_ChartButtonDragDrop.RegisterTarget(&omButton, this);

	switch(imChartType)
	{
	case COVERAGE_GRAPHIC:
		{
			CRect olWndRect = CRect(0, imStartVerticalScalePos, olRect.right - (  2), 270 + imStartVerticalScalePos);
			//CRect olWndRect = CRect(0, imStartVerticalScalePos, olRect.right - ( 2), 345 + imStartVerticalScalePos);
			omGraphic.SetTimeScale(GetTimeScale());
			//omGraphic.Create(0, CRect(olRect.left, imStartVerticalScalePos, olRect.right, olRect.bottom), this);
			omGraphic.Create( NULL, "Coverage-Graphic", WS_VISIBLE|WS_CHILD|WS_BORDER, olWndRect, 
				   this, 0);
			omGraphic.SetDimensions(imStartTopScaleTextPos);
			omGraphic.AttachViewer(GetViewer());
			omGraphic.Invalidate();
			omGraphic.ShowWindow(SW_NORMAL);
			omGraphic.SetAdditive(GetViewer()->GetAdditiv());
			GetViewer()->SetCoverage(&omGraphic);
		}
		break;
	case COVERAGE_SHIFTDEMANDS:
		{
			omGantt.SetTimeScale(GetTimeScale());
			omGantt.SetViewer(GetViewer(), SHIFT_DEMAND);
			omGantt.SetCoverageWindow(&omGraphic);
			omGantt.SetStatusBar(GetStatusBar());
			omGantt.SetDisplayWindow(GetStartTime(), GetStartTime() + GetInterval());
			omGantt.SetVerticalScaleWidth(imStartTopScaleTextPos);
			GetViewer()->MakeMasstab();
			omGantt.SetFonts(GetViewer()->GetGeometryFontIndex(), GetViewer()->GetGeometryFontIndex());
			omGantt.SetVerticalScaleColors(/*BLUE*/NAVY, SILVER, YELLOW, SILVER);
			omGantt.SetGanttChartColors(NAVY, SILVER, YELLOW, SILVER);
			omGantt.Create(0, CRect(olRect.left, imStartVerticalScalePos, olRect.right, olRect.bottom), this);
			omGantt.AttachChart(this);
			SetState(Maximized);
		}
		break;
	case COVERAGE_BASESHIFTS:
		{
			omGantt.SetTimeScale(GetTimeScale());
			omGantt.SetViewer(GetViewer(), BASIC_SHIFT);
			omGantt.SetStatusBar(GetStatusBar());
			omGantt.SetDisplayWindow(GetStartTime(), GetStartTime() + GetInterval());
			omGantt.SetVerticalScaleWidth(imStartTopScaleTextPos);
			GetViewer()->MakeMasstab();
			omGantt.SetFonts(GetViewer()->GetGeometryFontIndex(), GetViewer()->GetGeometryFontIndex());
			omGantt.SetVerticalScaleColors(/*BLUE*/NAVY, SILVER, YELLOW, SILVER);
			omGantt.SetGanttChartColors(NAVY, SILVER, YELLOW, SILVER);
			omGantt.Create(0, CRect(olRect.left, imStartVerticalScalePos, olRect.right, olRect.bottom), this);
			SetState(Maximized);
		}
		break;
	}

    olStr = GetViewer()->GetGroupText(GetGroupNo());
    omButton.SetWindowText(olStr);

#ifndef	PICHIT_FIXED_THE_COUNTER
	char clBuf[255];
	int ilCount = GetViewer()->GetLineCount(GetGroupNo());
	sprintf(clBuf, "%d", ilCount);
	//pomCountText->SetWindowText(clBuf);
#else
	char clBuf[255];
	int ilCount = GetViewer()->GetLineCount(GetGroupNo());
	//pomCountText->SetWindowText(clBuf);
#endif

    
    //pomTopScaleText->GetWindowText(clBuf, sizeof(clBuf));
	CString olButtonText;
	int i;
	CRect olRectTopScale(120 + 6, 2, (int)((olRect.right / 4) * 3) , 23);
	switch(imChartType)
	{
	
	case COVERAGE_GRAPHIC:
		{			
			CRect olRectButton2(5 + olRectTopScale.left, 2, 180 + olRectTopScale.left, 42);
			//MWO: 02.04.03 added CBS_SORT
			omComboBox.Create(CBS_DROPDOWNLIST|WS_VSCROLL|WS_TABSTOP|WS_CHILD|WS_VISIBLE|CBS_SORT, 
			    CRect(olRectButton2.left, olRectButton2.top, olRectButton2.right, olRectButton2.bottom+200), this, IDC_CHARTCB);
			//END MWO: 02.04.03
			omComboBox.SetFont(&ogMSSansSerif_Bold_8, FALSE);

			// Filter
			olRectTopScale.right = olRectButton2.right;
			olRectButton2 = CRect(5 + olRectTopScale.right, 2, 90 + olRectTopScale.right, 22);
			omButton2.Create(LoadStg(IDS_STRING967), BS_OWNERDRAW | WS_CHILD | WS_VISIBLE, olRectButton2, this, IDC_CHARTBUTTON2);
			omButton2.SetFont(&ogMSSansSerif_Bold_8, FALSE);

			// List
			olRectTopScale.right = olRectButton2.right;
			olRectButton2 = CRect(5 + olRectTopScale.right, 2, 90 + olRectTopScale.right, 22);
			omButton3.Create(LoadStg(IDS_STRING1727), BS_OWNERDRAW | WS_CHILD | WS_VISIBLE, olRectButton2, this, IDC_CHARTBUTTON3);
			omButton3.SetFont(&ogMSSansSerif_Bold_8, FALSE);

			// Cumulative
			olRectTopScale.right = olRectButton2.right;
			olRectButton2 = CRect(5 + olRectTopScale.right, 2, 100 + olRectTopScale.right, 22);
			CString olText;
			olText = LoadStg(COV_ADDITIVE);
			omAdditiveCB.Create( olText, WS_VISIBLE|WS_CHILD|BS_AUTOCHECKBOX, olRectButton2, this, IDC_CHARTBUTTON4 );
			if(GetViewer()->GetAdditiv() == true)
			{
				omAdditiveCB.SetCheck(1);
			}
			else
			{
				omAdditiveCB.SetCheck(0);
			}

			// Simulate
			olRectButton2 = CRect(5 + olRectButton2.right, 2, 90 + olRectButton2.right , 22);
			olText = LoadStg(IDS_STRING61248);
			omIgnoreBreakCB.Create( olText, WS_VISIBLE|WS_CHILD|BS_AUTOCHECKBOX, olRectButton2, this, IDC_CHARTBUTTON5 );
								
			ReloadComboView();

			olStr = GetViewer()->GetGroupText(GetGroupNo());
			pomTopScaleText = new CCS3DStatic(TRUE);
			
			olRectTopS = CRect(5 + olRectButton2.right, 2, 150 + olRectButton2.right , 22);
			pomTopScaleText->Create("", BS_OWNERDRAW |WS_CHILD | WS_VISIBLE,olRectTopS, this);

			// peak index
			olRectButton2 = CRect(5 + olRectTopS.right, 2, 100 + olRectTopS.right, 22);
			pomPIdxText = new CCS3DStatic(TRUE);
			pomPIdxText->Create("", WS_CHILD | WS_VISIBLE,olRectButton2, this);

			// Broadcast state
			olRectTopS.right = olRectButton2.right;
			olRectButton2 = CRect(5 + olRectTopS.right, 2, 16 + olRectTopS.right, 22);

			omBCStatus.Create("", BS_OWNERDRAW |WS_CHILD | WS_VISIBLE,olRectButton2, this,IDC_BC_STATUS);
			omBCStatus.SetColors(LIME, ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));

			// Demand update button
			olRectTopS.right = olRectButton2.right;
			olRectButton2 = CRect(5 + olRectTopS.right, 2, 90 + olRectTopS.right, 22);

			if (pomViewer->NoDemUpdates())
				olText = LoadStg(IDS_STRING1796);
			else
				olText = LoadStg(IDS_STRING1795);
			omDemUpdate.Recess(!pomViewer->NoDemUpdates());
			omDemUpdate.Create(olText, BS_OWNERDRAW | WS_CHILD | WS_VISIBLE, olRectButton2, this, IDC_DEMUPDATE);
			omDemUpdate.SetFont(&ogMSSansSerif_Bold_8, FALSE);

			olButtonText = LoadStg(COV_BTN_DEMANDS);
			omButton.SetWindowText(olButtonText);
			omGraphic.SetTimeFrame();
			SetStaticFieldText();
		}
		break;
	case COVERAGE_SHIFTDEMANDS:
		{
		CRect olRectButton2(5 + olRectTopScale.left, 2, 180 + olRectTopScale.left, 42);
		//MWO: 02.04.03 added CBS_SORT
		omComboBox.Create(CBS_DROPDOWNLIST|WS_VSCROLL|WS_TABSTOP|WS_CHILD|WS_VISIBLE|CBS_SORT, 
			              CRect(olRectButton2.left, olRectButton2.top, olRectButton2.right, olRectButton2.bottom+200), this, IDC_CHARTCB);
		//END MWO: 02.04.03 added CBS_SORT
		omComboBox.SetFont(&ogMSSansSerif_Bold_8, FALSE);
		omComboBox.SetDroppedWidth(170);
		
		if (GetViewer()->GetShiftRoster())
		{
			CString olSteps = ogCCSParam.GetParamValue("ROSTER","ID_SHOW_STEPS",CTime::GetCurrentTime().Format("%Y%m%d%H%M%S"),true);
			for(int i = 0; i <= 6; i++)
			{
				switch(i)
				{
				case 0:	// Shift roster
					if (olSteps.Find('1') >= 0)
					{
						int ind = omComboBox.AddString(LoadStg(IDS_STRING1586));
						omComboBox.SetItemData(ind,1);
					}
					break;
				case 1:	// Duty roster
					if (olSteps.Find('2') >= 0)
					{
						int ind = omComboBox.AddString(LoadStg(IDS_STRING1587));
						omComboBox.SetItemData(ind,2);
					}
					break;
				case 2:	// Daily list of employees
					if (olSteps.Find('3') >= 0)
					{
						int ind = omComboBox.AddString(LoadStg(IDS_STRING1588));
						omComboBox.SetItemData(ind,3);
					}
					break;
				case 3:	// Postprocessing of day
					if (olSteps.Find('4') >= 0)
					{
						int ind = omComboBox.AddString(LoadStg(IDS_STRING1589));
						omComboBox.SetItemData(ind,4);
					}
					break;
				case 4:	// Postprocessing
					if (olSteps.Find('5') >= 0)
					{
						int ind = omComboBox.AddString(LoadStg(IDS_STRING61256));
						omComboBox.SetItemData(ind,5);
					}
					break;
				case 5:	// Long term shift plan
					if (olSteps.Find('L') >= 0)
					{
						int ind = omComboBox.AddString(LoadStg(IDS_STRING61255));
						omComboBox.SetItemData(ind,6);
					}
					break;
				case 6:	// Current (Active) level
					{
						int ind = omComboBox.AddString(LoadStg(IDS_STRING1791));
						omComboBox.SetItemData(ind,7);
					}
					break;
				default:
					break;
				}
			}

			int ilWidth = CalculateMaxWidth();
			omComboBox.SetDroppedWidth(ilWidth);
			int ilDemLevel = GetViewer()->GetCurrentDemandLevel(SHIFT_DEMAND);
			omComboBox.SetCurSel(-1);
			for (i = 0; i < omComboBox.GetCount(); i++)
			{
				if (omComboBox.GetItemData(i) == ilDemLevel)
				{
					omComboBox.SetCurSel(i);
					break;
				}
			}
		}
		else 
		{
			CStringArray olDemandNames;

			GetViewer()->GetTimeFrameFromTo(omStartTime, omEndTime);
			ogSdgData.GetAllDnam(olDemandNames, omStartTime, omEndTime);

			omComboBox.AddString(LoadStg(IDS_STRING61249));//--Aktuell--
			omComboBox.AddString("");//To produce an empty line
			for(i = 0; i < olDemandNames.GetSize(); i++)
			{
				omComboBox.AddString(olDemandNames[i]);
				
			}
			int ilWidth = CalculateMaxWidth();
			omComboBox.SetDroppedWidth(ilWidth);
			CString olDemName = GetViewer()->GetCurrentDemandName(SHIFT_DEMAND);
			int ilIdx = omComboBox.FindStringExact( -1, olDemName);
			if(ilIdx != LB_ERR)
			{
				omComboBox.SetCurSel(ilIdx);
			}
			else
			{
				omComboBox.SetCurSel(-1);
				GetViewer()->SetDemandName(SHIFT_DEMAND,"");
			}
		}
		olRectTopScale.right = olRectButton2.right;
		olRectButton2 = CRect(5 + olRectTopScale.right, 2, 125 + olRectTopScale.right, 22);
		omButton2.Create(LoadStg(IDS_STRING530), BS_OWNERDRAW | WS_CHILD | WS_VISIBLE, olRectButton2, this, IDC_CHARTBUTTON2);
		omButton2.SetFont(&ogMSSansSerif_Bold_8, FALSE);
		
		olRectTopScale.right = olRectButton2.right;
		olRectButton2 = CRect(5 + olRectTopScale.right, 2, 110 + olRectTopScale.right, 22);
		omButton3.Create(LoadStg(IDS_STRING966), BS_OWNERDRAW | WS_CHILD | WS_VISIBLE, olRectButton2, this, IDC_CHARTBUTTON3);
		omButton3.SetFont(&ogMSSansSerif_Bold_8, FALSE);

		omAutoCovDis.Create(LoadStg(IDS_STRING61390), BS_OWNERDRAW | WS_CHILD | WS_VISIBLE, olRectButton2, this, IDC_AUTOCOVDISCARD);
		omAutoCovDis.SetFont(&ogMSSansSerif_Bold_8, FALSE);

		olRectTopScale.right = olRectButton2.right;
		olRectButton2 = CRect(5 + olRectTopScale.right, 2, 110 + olRectTopScale.right, 22);
		omButton6.Create(LoadStg(IDS_STRING1663), BS_OWNERDRAW | WS_CHILD | WS_VISIBLE, olRectButton2, this, IDC_CHARTBUTTON6);
		omButton6.SetFont(&ogMSSansSerif_Bold_8, FALSE);

		omAutoCovRel.Create(LoadStg(IDS_STRING1927), BS_OWNERDRAW | WS_CHILD | WS_VISIBLE, olRectButton2, this, IDC_AUTOCOVRELEASE);
		omAutoCovRel.SetFont(&ogMSSansSerif_Bold_8, FALSE);

		olRectTopScale.right = olRectButton2.right;
		olRectButton2 = CRect(5 + olRectTopScale.right, 2, 110 + olRectTopScale.right + 20, 22);
		CString olText;
		olText = LoadStg(COV_MAXVIEW);
		omAdditiveCB.Create( olText, WS_VISIBLE|WS_CHILD|BS_AUTOCHECKBOX, olRectButton2, this, IDC_CHARTBUTTON4 );
		if(GetViewer()->GetMaxView() == true)
		{
			omAdditiveCB.SetCheck(0);
		}
		else
		{
			omAdditiveCB.SetCheck(1);
		}
		olRectTopScale.right = olRectButton2.right;
		olRectButton2 = CRect(5 + olRectTopScale.right, 2, 110 + olRectTopScale.right + 40, 22);
		olText = LoadStg(COV_DEM_IGNORE_BREAK);
		omIgnoreBreakCB.Create( olText, WS_VISIBLE|WS_CHILD|BS_AUTOCHECKBOX, olRectButton2, this, IDC_CHARTBUTTON5 );
		if(GetViewer()->GetIgnoreBreak() == true)
		{
			omIgnoreBreakCB.SetCheck(1);
		}
		else
		{
			omIgnoreBreakCB.SetCheck(0);
		}

		olRectTopScale.right = olRectButton2.right;
		olRectButton2 = CRect(5 + olRectTopScale.right, 2, 110 + olRectTopScale.right + 40, 22);
		olText = LoadStg(IDS_STRING1671);
		omShiftRoster.Create( olText, WS_VISIBLE|WS_CHILD|BS_AUTOCHECKBOX, olRectButton2, this, IDC_CHARTBUTTON7 );
		if(GetViewer()->GetShiftRoster() == true)
		{
			omShiftRoster.SetCheck(1);
			omButton2.EnableWindow(FALSE);
			omButton3.EnableWindow(FALSE);
			omButton6.EnableWindow(FALSE);
		}
		else
		{
			omShiftRoster.SetCheck(0);
			omButton2.EnableWindow(TRUE);
			omButton3.EnableWindow(TRUE);
			omButton6.EnableWindow(TRUE);
		}
		omAutoCovRel.ShowWindow(SW_HIDE);
		omAutoCovDis.ShowWindow(SW_HIDE);

//		omButton4.Create("Maxim. Anzeige", BS_OWNERDRAW | WS_CHILD | WS_VISIBLE, CRect(olRectTopS.right - (olRectButton2.right - olRectButton2.left), 2, olRect.right - 6, 22), this, IDC_CHARTBUTTON3);
//		omButton4.SetFont(&ogMSSansSerif_Bold_8, FALSE);
		olStr = GetViewer()->GetGroupText(GetGroupNo());
		olButtonText = LoadStg(COV_BTN_SHIFTDEMANDS);
		omButton.SetWindowText(olButtonText);


		olRectTopScale.right = olRectButton2.right;
		olRectButton2 = CRect(5 + olRectTopScale.right, 2, 110 + olRectTopScale.right, 22);
		omExcel.Create(LoadStg(IDS_STRING1950), BS_OWNERDRAW | WS_CHILD | WS_VISIBLE, olRectButton2, this, IDC_EXCELBUTTON);
		omExcel.SetFont(&ogMSSansSerif_Bold_8, FALSE);
		}
		break;
	case COVERAGE_BASESHIFTS:
		{
			pomViewer->SetShiftView(BASIC_SHIFT,BASE_SHIFT_ALL); // default
			CRect olRectButton2(5 + olRectTopScale.left, 2, 180 + olRectTopScale.left, 42);
			omComboBox.Create(CBS_DROPDOWNLIST|WS_VSCROLL|WS_TABSTOP|WS_CHILD|WS_VISIBLE|CBS_SORT,
				CRect(olRectButton2.left, olRectButton2.top, olRectButton2.right, olRectButton2.bottom+200), this, IDC_CHARTCB);
			omComboBox.SetFont(&ogMSSansSerif_Bold_8, FALSE);
			omComboBox.SetDroppedWidth(170);
			
			CString olText;

			ReloadComboView();

/********************************			
			//-- Alle --
			olText.LoadStg(IDS_STRING1685);
			int ilIndex = omComboBox.AddString(olText);
			omComboBox.SetItemData(ilIndex,(DWORD)BASE_SHIFT_ALL);
			
			//Dummy shift
			olText.LoadStg(IDS_STRING1682);
			ilIndex = omComboBox.AddString(olText);
			omComboBox.SetItemData(ilIndex,(DWORD)BASE_SHIFT_SPECIAL);
			
			//Static shift
			olText.LoadStg(IDS_STRING1683);
			ilIndex = omComboBox.AddString(olText);
			omComboBox.SetItemData(ilIndex,(DWORD)BASE_SHIFT_STATIC);

			//Dynamic shift
			olText.LoadStg(IDS_STRING1684);
			ilIndex = omComboBox.AddString(olText);
			omComboBox.SetItemData(ilIndex,(DWORD)BASE_SHIFT_DYNAMIC);
			int ilWidth = CalculateMaxWidth();
			omComboBox.SetDroppedWidth(ilWidth);
			int ilView = GetViewer()->GetShiftView(BASIC_SHIFT);
			for(ilIndex = 0; ilIndex<omComboBox.GetCount();ilIndex++)
			{
				if(ilView==-1)
				{
					if(omComboBox.GetItemData(ilIndex)==BASE_SHIFT_ALL)
					{
						omComboBox.SetCurSel(ilIndex);
						GetViewer()->SetShiftView(BASIC_SHIFT,BASE_SHIFT_ALL);
						break;
					}
				}
				else if(omComboBox.GetItemData(ilIndex)==(DWORD)ilView)
				{
					omComboBox.SetCurSel(ilIndex);
					GetViewer()->SetShiftView(BASIC_SHIFT,ilView);
					break;
				}
			}
			*****************************/
			olRectTopScale.right = olRectButton2.right;
			olRectButton2 = CRect(5 + olRectTopScale.right, 2, 110 + olRectTopScale.right, 22);
			
			// View/Ansicht Button
			omButton2.Create(LoadStg(IDS_STRING967), BS_OWNERDRAW | WS_CHILD | WS_VISIBLE, olRectButton2, this, IDC_CHARTBUTTON2);
			omButton2.SetFont(&ogMSSansSerif_Bold_8, FALSE);
			
			olRectTopScale.right = olRectButton2.right;
			olRectButton2 = CRect(5 + olRectTopScale.right, 2, 110 + olRectTopScale.right, 22);

			/************************
			omButton3.Create(LoadStg(IDS_STRING968), BS_OWNERDRAW | WS_CHILD | WS_VISIBLE, olRectButton2, this, IDC_CHARTBUTTON3);
			omButton3.SetFont(&ogMSSansSerif_Bold_8, FALSE);
			***********************/
			olStr = GetViewer()->GetGroupText(GetGroupNo());
			olButtonText = LoadStg(COV_BTN_BASICSHIFTS);
			omButton.SetWindowText(olButtonText);

		}
		break;
	}
    return 0;
	CCS_CATCH_ALL
	return -1;
}

void CoverageChart::SetMarkTime(CTime opStartTime, CTime opEndTime)
{
CCS_TRY;
	omGantt.SetMarkTime(opStartTime, opEndTime);
CCS_CATCH_ALL
}

void CoverageChart::OnDestroy() 
{
	CCS_TRY
//	if (bgModal == TRUE)
//		return;
	// Id 30-Sep-96
	// This will remove a lot of warning message when the user change view.
	// If we just delete a staff chart, MFC will produce two warning message.
	// First, Revoke not called before the destructor.
	// Second, calling DestroyWindow() in CWnd::~CWnd.

	m_ChartWindowDragDrop.Revoke();
    m_ChartButtonDragDrop.Revoke();
    m_CountTextDragDrop.Revoke();
//    m_TopScaleTextDragDrop.Revoke();

	CFrameWnd::OnDestroy();
	CCS_CATCH_ALL
}

void CoverageChart::OnSize(UINT nType, int cx, int cy) 
{
	CCS_TRY
    // TODO: Add your message handler code here
    CFrameWnd::OnSize(nType, cx, cy);
    
    CRect olClientRect; GetClientRect(&olClientRect);
    //TRACE("CoverageChart OnSize: client rect [top=%d, bottom+%d]\n", olClientRect.top, olClientRect.bottom);
	switch(imChartType)
	{
	
		case COVERAGE_GRAPHIC:
			{
				CRect olWndRect = CRect(0, imStartVerticalScalePos, olClientRect.right - (  2), olClientRect.bottom);
				omGraphic.MoveWindow(&olWndRect, FALSE);
				//MWO:P 31.03.03 Performance 
				//omGraphic.SetTimeFrame();
				//END MWO:P 31.03.03 Performance 
			}
		break;
		case COVERAGE_SHIFTDEMANDS:
			{
				CRect olRect(imStartTopScaleTextPos + 6, 4 + 1 , olClientRect.right - 6, (4 + 20) - 1);
				olRect.SetRect (olClientRect.left, imStartVerticalScalePos, olClientRect.right, olClientRect.bottom);
				//TRACE("CoverageChart SetRect: client rect [top=%d, bottom+%d]\n", imStartVerticalScalePos, olClientRect.bottom);
				omGantt.MoveWindow(&olRect, FALSE);
			}
			break;
		case COVERAGE_BASESHIFTS:
			{
				CRect olRect(imStartTopScaleTextPos + 6, 4 + 1 , olClientRect.right - 6, (4 + 20) - 1);
				olRect.SetRect (olClientRect.left, imStartVerticalScalePos, olClientRect.right, olClientRect.bottom);
				//TRACE("CoverageChart SetRect: client rect [top=%d, bottom+%d]\n", imStartVerticalScalePos, olClientRect.bottom);
				omGantt.MoveWindow(&olRect, FALSE);
			}
		break;
	}

	CCS_CATCH_ALL
}

BOOL CoverageChart::OnEraseBkgnd(CDC* pDC) 
{
	CCS_TRY
    // TODO: Add your message handler code here and/or call default
    CRect olClipRect;
    pDC->GetClipBox(&olClipRect);
    //GetClientRect(&olClipRect);

    CBrush olBrush(lmBkColor);
    CBrush *polOldBrush = pDC->SelectObject(&olBrush);
    pDC->PatBlt(olClipRect.left, olClipRect.top,
        olClipRect.Width(), olClipRect.Height(),
        PATCOPY);
    pDC->SelectObject(polOldBrush);
    
    return TRUE;
	CCS_CATCH_ALL
	return FALSE;    
    //return CFrameWnd::OnEraseBkgnd(pDC);
}

void CoverageChart::OnPaint()
{
	CCS_TRY

	CPaintDC dc(this); // device context for painting
    
    // TODO: Add your message handler code here
    
    // Do not call CFrameWnd::OnPaint() for painting messages
    CPen *polOldPen = (CPen *) dc.SelectStockObject(BLACK_PEN);
    CRect olClientRect;
    GetClientRect(&olClientRect);
    
#define imHorizontalPos imStartVerticalScalePos
#define imVerticalPos imStartTopScaleTextPos

    // draw horizontal seperator
    dc.SelectStockObject(BLACK_PEN);
    dc.MoveTo(olClientRect.left, imHorizontalPos - 2);
    dc.LineTo(olClientRect.right, imHorizontalPos - 2);
    
    dc.SelectStockObject(WHITE_PEN);
    dc.MoveTo(olClientRect.left, imHorizontalPos - 1);
    dc.LineTo(olClientRect.right, imHorizontalPos - 1);
    //

    // draw vertical seperator
    dc.SelectStockObject(BLACK_PEN);
    dc.MoveTo(imVerticalPos - 2, olClientRect.top);
    //dc.LineTo(imVerticalPos - 2, olClientRect.bottom);
    dc.LineTo(imVerticalPos - 2, imHorizontalPos - 2);
    
    dc.SelectStockObject(WHITE_PEN);
    dc.MoveTo(imVerticalPos - 1, olClientRect.top);
    //dc.LineTo(imVerticalPos - 1, olClientRect.bottom);
    dc.LineTo(imVerticalPos - 1, imHorizontalPos - 2);
    //

    dc.SelectObject(polOldPen);
	CCS_CATCH_ALL
}

//@ManMemo:OnChartButton 
 /*@Doc:
		Toggles between Maximized and Minimized state of the chart
 */
void CoverageChart::OnChartButton()
{
	CCS_TRY
    if (imState == Minimized)
        SetState(Maximized);
    else
        SetState(Minimized);

	GetParent() -> GetParent() -> SendMessage(WM_POSITIONCHILD, 0, 0L);
	CCS_CATCH_ALL
}
//@ManMemo:OnChartButton5
 /*@Doc:
		for imChartType == COVERAGE_GRAPHIC
				Toggles the offset state
		for imChartType == COVERAGE_SHIFTDEMANDS
				Toggles the ignore break state
 */
void CoverageChart::OnChartButton5()
{
	CCS_TRY
	switch(imChartType)
	{
		/*
		case COVERAGE_ZEROLINE:
			{
			}
			break;
			*/
		case COVERAGE_GRAPHIC:
			{
				if(omIgnoreBreakCB.GetCheck() == 1)
				{
					GetViewer()->SetSimulationFlag(true);
				}
				else
				{
					GetViewer()->SetSimulationFlag(false);
				}
				pogCoverageDiagram->SendMessage(WM_SIMULATE, 0, 0);
			}
		break;
		case COVERAGE_SHIFTDEMANDS:
			{
				CWaitCursor olWait;
				if(omIgnoreBreakCB.GetCheck() == 1)
				{
					GetViewer()->SetIgnoreBreak(true);
				}
				else
				{
					GetViewer()->SetIgnoreBreak(false);
				}
				GetViewer()->LoadSdtWindow();
				pogCoverageDiagram->SendMessage(WM_UPDATEDEMANDGANTT, 1, 0);
				pogCoverageDiagram->SendMessage(WM_UPDATE_COVERAGE, 0, 0);
			}
			break;
		case COVERAGE_BASESHIFTS:
			{
			}
		break;
	}
	CCS_CATCH_ALL
}

//@ManMemo:OnChartButton5
 /*@Doc:
		for imChartType == COVERAGE_SHIFTDEMANDS
				Release Button saves the master shift demamd into the DB
 */
void CoverageChart::OnChartButton6()
{
	CCS_TRY
	switch(imChartType)
	{
		case COVERAGE_GRAPHIC:
			{
				;
			}
		break;
		case COVERAGE_SHIFTDEMANDS:
			{
				if (GetViewer()->GetShiftRoster()==false)
				{
					if (!GetViewer()->GetActuellState())
					{
						//uhi 4.8.00 Abfrage Release Ja/Nein
						CString olText, olDnam;
						CTime olBegi;

						olText = LoadStg(IDS_RELEASE);
						
						SDGDATA *prlSdg = ogSdgData.GetSdgByUrno(GetViewer()->CurrentMasterShiftRequirement());
						if (prlSdg != NULL)
						{
							olDnam = prlSdg->Dnam;
							olText = olText + CString(" '") + olDnam + CString("' ?");
							olBegi = prlSdg->Begi;
						}
						else 
							//uhi 17.01.01
							return;
							//olText = olText + CString(" ?");
						
						if (MessageBox(olText, LoadStg(ST_QUESTION), (MB_ICONQUESTION|MB_YESNO)) == IDNO)
							return;		
						
						//uhi 1.9.00 Freigabe erlaubt?
						CTime olTimeNow = CTime::GetCurrentTime();
						bool blCheckValidity = false;
						int ilYear, ilMonth;
						CString olStringNow = olTimeNow.Format("%Y%m%d%H%M%S");
						int iLastInput = atoi(ogCCSParam.GetParamValue(ogGlobal,"ID_LAST_INPUT",olStringNow));
						if (iLastInput >= 0)
						{
							blCheckValidity = true;
						}

						int ilNextMonth = atoi(ogCCSParam.GetParamValue(ogGlobal,"ID_NEXT_MONTH",olStringNow));
						if (ilNextMonth <= 0 || ilNextMonth > 12)
							ilNextMonth = 1;

						ilYear = olTimeNow.GetYear();
						// reload sdg in case of been deleted / updated during dialogue broadcasts
						prlSdg = ogSdgData.GetSdgByUrno(GetViewer()->CurrentMasterShiftRequirement());
						if (prlSdg == NULL)
							return;

						if (prlSdg->Operation[0] == 'O' && blCheckValidity == true)
						{
							if (olTimeNow.GetDay() <= iLastInput)
							{
								ilMonth = olTimeNow.GetMonth() + ilNextMonth;
							}
							else
							{
								if (olTimeNow.GetDay() > iLastInput)
								{
									ilMonth = olTimeNow.GetMonth() + ilNextMonth + 1;	
								}
							}

							if (ilMonth > 12)
							{
								ilMonth = ilMonth - 12;
								ilYear = ilYear + 1;
							}
						}
						else
						{
							ilMonth = olTimeNow.GetMonth();
						}

						
						CTime olValidInput = CTime(ilYear, ilMonth, 1, 0, 0, 0);
						
						if (olBegi < olValidInput && blCheckValidity)
						{
							SetReleaseBtnRed(false);
							MessageBox(LoadStg(IDS_STRING4),LoadStg(IDS_STRING1734), MB_OK);
							return;
						}

						CWaitCursor olWait;
						ogSdtData.ReleaseVirtuellSdts(GetViewer()->CurrentMasterShiftRequirement());
						//uhi 1.9.00 Vorg�nger abl�sen
						ogDataSet.TerminatePredecessor(GetViewer()->CurrentMasterShiftRequirement());
						ogDataSet.CreateVirtuellSdtsBySdgu(GetViewer()->CurrentMasterShiftRequirement());
						SetReleaseBtnRed(false);

						// reload sdg in case of been deleted / updated during dialogue broadcasts
						prlSdg = ogSdgData.GetSdgByUrno(GetViewer()->CurrentMasterShiftRequirement());
						if (prlSdg != NULL)
						{
							prlSdg->Expd = true;
							prlSdg->IsChanged = DATA_CHANGED;
							ogSdgData.UpdateSdg(prlSdg,TRUE);
						}

						pomViewer->LoadSdtWindow(true);
						pomViewer->ChangeViewToForHScroll(TIMENULL,TIMENULL);
					}
				}
			}
			break;
		case COVERAGE_BASESHIFTS:
			{
			}
		break;
	}
	CCS_CATCH_ALL
}

//@ManMemo:OnChartButton7
 /*@Doc:
		for imChartType == COVERAGE_SHIFTDEMANDS
				Toggles between Roster and Shiftdemand state
				if the Roster button is checked 'DRR','DRS','DRA' are being loaded
				and coverted into Sdt's and stores them locally in ogSdtRosterData
				and loads the shiftplan data converts it in Sdt's, stores them locally in 
				ogSdtShiftData.

 */


void CoverageChart::OnChartButton7() // OnShiftRoster
{
	CCS_TRY
	switch(imChartType)
	{
		/*
		case COVERAGE_ZEROLINE:
			{
			}
			break;
			*/
		case COVERAGE_GRAPHIC:
			{
			}
		break;
		case COVERAGE_SHIFTDEMANDS:
			{
				// Umschalten zwischen Master shift und Shift Roster Sichten
				CWaitCursor olWait;
	
				CTime olFrom,olTo;
				// Load all ROSDATA in spec. time frame
				pomViewer->GetTimeFrameFromTo(olFrom, olTo);
				if(omShiftRoster.GetCheck() == 1)
				{
					bmOptBreakIsActive = false;
					GetParent() -> GetParent() -> SendMessage(WM_DEACTIVEAUTOCOV, 0, 0L);
					GetViewer()->SetShiftRoster(true);
					CWnd *polWnd = GetDlgItem(IDC_CHARTBUTTON2);
					if(polWnd!=NULL)
					{
						polWnd->ShowWindow(SW_SHOW);
						polWnd->EnableWindow(TRUE);
						if(pomViewer->IsRealRoster())
						{
							polWnd->SetWindowText(LoadStg(IDS_STRING61360));
							
						}
						else
						{
							polWnd->SetWindowText(LoadStg(IDS_STRING61361));
						}

					}
					polWnd = GetDlgItem(IDC_CHARTBUTTON3);
					if(polWnd!=NULL)
					{
						polWnd->ShowWindow(SW_HIDE);
						polWnd->EnableWindow(FALSE);
					}
					polWnd = GetDlgItem(IDC_CHARTBUTTON6);
					if(polWnd!=NULL)
					{
						polWnd->ShowWindow(SW_HIDE);
						polWnd->EnableWindow(FALSE);
					}
					
					
					CTime olFrom,olTo;
					GetViewer()->GetTimeFrameFromTo(olFrom, olTo);
					CString olWhere = "";
					
					if(!bmNoReload)
					{

						olWhere.Format(" WHERE SDAY BETWEEN '%s' AND '%s'", olFrom.Format("%Y%m%d"), olTo.Format("%Y%m%d"));
						//MWO: 27.03.03
						ogBcHandle.SetFilterRange("DRSTAB", "SDAY", "SDAY", olFrom.Format("%Y%m%d"), olTo.Format("%Y%m%d"));
						ogBcHandle.SetFilterRange("DRDTAB", "SDAY", "SDAY", olFrom.Format("%Y%m%d"), olTo.Format("%Y%m%d"));
						ogBcHandle.SetFilterRange("DRATAB", "SDAY", "SDAY", olFrom.Format("%Y%m%d"), olTo.Format("%Y%m%d"));
						//END MWO: 27.03.03

						ogBCD.Read("DRS",olWhere);
							
						ogBCD.Read("DRD",olWhere);

						ogBCD.Read("DRA",olWhere);

						olWhere = "";
						olWhere.Format(" WHERE VAFR <= '%s' AND VATO >= '%s'", olTo.Format("%Y%m%d%H%M%S"), olFrom.Format("%Y%m%d%H%M%S"));
						//MWO: 27.03.03
						ogBcHandle.SetFilterRange("GPLTAB", "VAFR", "VATO", olFrom.Format("%Y%m%d%H%M%S"), olTo.Format("%Y%m%d%H%M%S"));
						//END MWO: 27.03.03

						ogBCD.Read("GPL",olWhere);

			
						ogSdtShiftData.ClearAll();
						pomViewer->PrepareShiftNameMap();
					}
					omComboBox.ResetContent();

					if (pomViewer->IsRealRoster())
					{
						CString olT;
						CString olSteps = ogCCSParam.GetParamValue("ROSTER","ID_SHOW_STEPS",CTime::GetCurrentTime().Format("%Y%m%d%H%M%S"),true);
						for(int i = 0; i <= 6; i++)
						{
							switch(i)
							{
							case 0:	// Shift roster
								if (olSteps.Find('1') >= 0)
								{
									olT = LoadStg(IDS_STRING1586);
									int ind = omComboBox.AddString(LoadStg(IDS_STRING1586));
									omComboBox.SetItemData(ind,1);
								}
								break;
							case 1:	// Duty roster
								if (olSteps.Find('2') >= 0)
								{
									olT = LoadStg(IDS_STRING1587);
									int ind = omComboBox.AddString(LoadStg(IDS_STRING1587));
									omComboBox.SetItemData(ind,2);
								}
								break;
							case 2:	// Daily list of employees
								if (olSteps.Find('3') >= 0)
								{
									olT = LoadStg(IDS_STRING1588);
									int ind = omComboBox.AddString(LoadStg(IDS_STRING1588));
									omComboBox.SetItemData(ind,3);
								}
								break;
							case 3:	// Postprocessing of day
								if (olSteps.Find('4') >= 0)
								{
									olT = LoadStg(IDS_STRING1589);
									int ind = omComboBox.AddString(LoadStg(IDS_STRING1589));
									omComboBox.SetItemData(ind,4);
								}
								break;
							case 4:	// Postprocessing
								if (olSteps.Find('5') >= 0)
								{
									olT = LoadStg(IDS_STRING61256);
									int ind = omComboBox.AddString(LoadStg(IDS_STRING61256));
									omComboBox.SetItemData(ind,5);
								}
								break;
							case 5:	// Long term shift plan
								if (olSteps.Find('L') >= 0)
								{
									olT = LoadStg(IDS_STRING61255);
									int ind = omComboBox.AddString(LoadStg(IDS_STRING61255));
									omComboBox.SetItemData(ind,6);
								}
								break;
							case 6:	// Current (Active) level
								{
									olT = LoadStg(IDS_STRING1791);
									int ind = omComboBox.AddString(LoadStg(IDS_STRING1791));
									omComboBox.SetItemData(ind,7);
								}
								break;
							default:
								break;
							}
						}
						int ilWidth = CalculateMaxWidth();
						omComboBox.SetDroppedWidth(ilWidth);
						int ilDemLevel = GetViewer()->GetCurrentDemandLevel(SHIFT_DEMAND);
						omComboBox.SetCurSel(-1);
						for (i = 0; i < omComboBox.GetCount(); i++)
						{
							if (omComboBox.GetItemData(i) == ilDemLevel)
							{
								omComboBox.SetCurSel(i);
								break;
							}
						}

						GetViewer()->SetMaxView(true);						
						OnSelChangeCB();
						omAdditiveCB.ShowWindow(SW_HIDE);
					}
					else
					{
						CStringArray olShiftNameArray;
						pomViewer->ShiftNameArray(olShiftNameArray);
						for(int ilLc = 0; ilLc < olShiftNameArray.GetSize();ilLc++)
						{
							omComboBox.AddString(olShiftNameArray[ilLc]);
						}
						pomViewer->CurrentShiftRosterName("");
						int ilWidth = CalculateMaxWidth();
						omComboBox.SetDroppedWidth(ilWidth);
					
						omComboBox.SetCurSel(-1);

						GetViewer()->SetMaxView(true);						
						omAdditiveCB.ShowWindow(SW_HIDE);

					}
					

					pomViewer->LoadSdtWindow(true);

				}
				else	// Master shift requirement
				{
					if (GetViewer()->GetMaxView() != (omAdditiveCB.GetCheck() == 0))
					{
						GetViewer()->SetMaxView(omAdditiveCB.GetCheck() == 0);
					}

					omAdditiveCB.ShowWindow(SW_SHOWNORMAL);
					GetViewer()->SetShiftRoster(false);
					CWnd *polWnd = GetDlgItem(IDC_CHARTBUTTON2);
					GetParent() -> GetParent() -> SendMessage(WM_ACTIVEAUTOCOV, 0, 0L);
					if(polWnd!=NULL)
					{
						polWnd->ShowWindow(SW_HIDE);
						polWnd->SetWindowText(LoadStg(IDS_STRING530));
					}

					if (!GetViewer()->GetActuellState())	// --Current-- ?
					{
						CWnd *polWnd = GetDlgItem(IDC_CHARTBUTTON2);
						if(polWnd!=NULL)
						{
							polWnd->ShowWindow(SW_SHOW);
							polWnd->EnableWindow(TRUE);
							polWnd->SetWindowText(LoadStg(IDS_STRING530));
						}
						polWnd = GetDlgItem(IDC_CHARTBUTTON3);
						if(polWnd!=NULL)
						{
							polWnd->ShowWindow(SW_SHOW);
							polWnd->EnableWindow(TRUE);
						}
						polWnd = GetDlgItem(IDC_CHARTBUTTON6);
						if(polWnd!=NULL)
						{
							if (bmOperEnabled)
							{
								if (GetViewer()->CurrentMasterShiftRequirement() != 0)
								{
									SDGDATA *polSdg = ogSdgData.GetSdgByUrno(GetViewer()->CurrentMasterShiftRequirement());
									if (polSdg != NULL && polSdg->Operation[0] == 'O')
									{
										polWnd->ShowWindow(SW_SHOW);
										polWnd->EnableWindow(TRUE);
									}
								}
							}
							else
							{
								polWnd->ShowWindow(SW_SHOW);
								polWnd->EnableWindow(TRUE);
							}
						}
					}
					CStringArray olDemandNames;
					GetViewer()->GetTimeFrameFromTo(omStartTime, omEndTime);
					ogSdgData.GetAllDnam(olDemandNames, omStartTime, omEndTime);
					omComboBox.ResetContent();
					omComboBox.AddString(LoadStg(IDS_STRING61249));//--Aktuell-
					omComboBox.AddString("");//To produce an empty line
					for(int i = 0; i < olDemandNames.GetSize(); i++)
					{
						omComboBox.AddString(olDemandNames[i]);
					}
					int ilWidth = CalculateMaxWidth();
					omComboBox.SetDroppedWidth(ilWidth);
					CString olDemName = GetViewer()->GetCurrentDemandName(SHIFT_DEMAND);

					if(olDemName.IsEmpty())
					{
						GetParent() -> GetParent() -> SendMessage(WM_DEACTIVEAUTOCOV, 0, 0L);
					}
					int ilIdx = omComboBox.FindStringExact( -1, olDemName);
					// gsa. (Re)load sdtdata anew!
					if(ilIdx != LB_ERR)
					{
						omComboBox.SetCurSel(ilIdx);
						omComboBox.GetLBText(ilIdx, olDemName);
						//GetViewer()->SetDemandName(SHIFT_DEMAND, olSelString);
						long llSdgUrno = ogSdgData.GetUrnoByDnam(olDemName);
						pomViewer->LoadSdtWindow();
					}	
				}
				pogCoverageDiagram->SendMessage(WM_UPDATEDEMANDGANTT, 0, 0);
			}
			break;
		case COVERAGE_BASESHIFTS:
			{
			}
		break;
	}
	CCS_CATCH_ALL
}

//@ManMemo:OnChartButton4
 /*@Doc:
		for imChartType == COVERAGE_GRAPHIC
				Toggles the additve state
		for imChartType == COVERAGE_SHIFTDEMANDS
				Toggles the maximise/minimize display of the shiftdemands
 */


void CoverageChart::OnChartButton4()
{
	CCS_TRY
	switch(imChartType)
	{
		/*
		case COVERAGE_ZEROLINE:
			{
			}
			break;
			*/
		case COVERAGE_GRAPHIC:
			{
				if(omAdditiveCB.GetCheck() == 1)
				{
					GetViewer()->SetAdditiv(true);
					omGraphic.SetAdditive(true);
				}
				else
				{
					GetViewer()->SetAdditiv(false);
					omGraphic.SetAdditive(false);
				}
				pogCoverageDiagram->SendMessage(WM_UPDATE_COVERAGE, 0, 0);
			}
		break;
		case COVERAGE_SHIFTDEMANDS:
			{
				CWaitCursor olWait;
				// Maximise/Minimise view
				if(omAdditiveCB.GetCheck() == 1)
				{
					GetViewer()->SetMaxView(false);
				}
				else
				{
					GetViewer()->SetMaxView(true);
				}
				pogCoverageDiagram->SendMessage(WM_UPDATEDEMANDGANTT, 1, 0);
			}
			break;
		case COVERAGE_BASESHIFTS:
			{
			}
		break;
	}
	CCS_CATCH_ALL
}

//@ManMemo:OnChartButton2
 /*@Doc:
		for imChartType == COVERAGE_GRAPHIC
				Opens the evalution filter property page
		for imChartType == COVERAGE_SHIFTDEMANDS
				Opens the master shift demand dialog
		for imChartType == COVERAGE_BASESHIFTS
				Opens the basic shift filter property page
				
 */


void CoverageChart::OnChartButton2()
{
	CCS_TRY
	switch(imChartType)
	{
	case COVERAGE_GRAPHIC:
		{
			CWaitCursor olWait;
			CString olOldBaseViewName = GetViewer()->GetBaseViewName();
			CString olOldViewName = GetViewer()->GetViewName();
			CString olViewName;
			int ilIdx;
			if((ilIdx = omComboBox.GetCurSel()) != LB_ERR)
			{
				omComboBox.GetLBText(ilIdx, olViewName);
			}
			GetViewer()->SetViewerKey("BEWERTUNG");
			GetViewer()->SelectView(olViewName);
			SetFocus();

	//		CTime olEndTime = omStartTime + omDuration;
			CString olCaption;
			
			CTime olFrom,olTo;
			GetViewer()->GetTimeFrameFromTo(olFrom, olTo);

			CString olFromTxt = olFrom.Format("%d.%m.%Y / %H:%M");
			CString olToTxt = olTo.Format("%d.%m.%Y / %H:%M");
			olCaption = LoadStg(IDS_STRING61243) + olFromTxt + " - " + olToTxt;
			BewertungPropertySheet olDlg("BEWERTUNG",olCaption,pomViewer->GetFilterIdx(), this, pomViewer);
			if(olDlg.DoModal() != IDCANCEL)
			{
				CString olNewViewName = GetViewer()->GetViewName();
				GetViewer()->PrepareEvaluationFilter(olNewViewName);

				GetViewer()->SetReCalcShifts(true);
				ReloadComboView();
				int ilPos = omComboBox.FindStringExact(-1, olNewViewName);

				omComboBox.SetCurSel(ilPos);

			}

			ReloadComboView();
			GetViewer()->SetViewerKey(olOldBaseViewName);
			GetViewer()->SelectView(olOldViewName);

			pogCoverageDiagram->SendMessage(WM_UPDATEDEMANDGANTT, 1, 0);

		}
		break;
	case COVERAGE_SHIFTDEMANDS:
		{
			CWaitCursor olWait;
			if(GetViewer()->GetShiftRoster()==false)
			{
				CString olDnam = GetViewer()->GetCurrentDemandName(SHIFT_DEMAND);

				SDGDATA *prlSdg = NULL;
				bool blCreateNew = false;
				if(!olDnam.IsEmpty())
				{
					prlSdg= ogSdgData.GetSdgByDnam(olDnam);
				}
				else
				{
					blCreateNew = true;
				}
				CString olSelectSdgName = olDnam;
				if(blCreateNew)
				{
					MasterShiftDemand *polDlg = new MasterShiftDemand(this,NULL,omStartTime,omEndTime);
					if(polDlg->DoModal() == IDOK)
					{
						SDGDATA *prlSdg1 = ogSdgData.GetSdgByDnam(polDlg->omDnam);
						olSelectSdgName = polDlg->omDnam;
						if(prlSdg1 != NULL)
						{
							//uhi 17.01.01 Ladezeitraum und Master stimmen nicht �berein
							bool blTreffer = true;
	
							CCSPtrArray<TIMEFRAMEDATA> olTimes;

							ogSdgData.GetTimeFrames(prlSdg1->Urno,  olTimes);
							if((omStartTime != TIMENULL) && (omEndTime != TIMENULL)){
								for(int j = olTimes.GetSize() -1; j >= 0; j--){
									blTreffer = false;
									if(IsOverlapped(omStartTime, omEndTime, olTimes[j].StartTime, olTimes[j].EndTime) == TRUE){
										blTreffer = true;
										break;
									}
								}

							}
							olTimes.DeleteAll();

							if(blTreffer){
								GetViewer()->CurrentMasterShiftRequirement(prlSdg1->Urno);
								GetViewer()->SetDemandName(SHIFT_DEMAND,prlSdg1->Dnam);
								//uhi 18.9.00
								//Nur noch Infofeld Display Begin
								if(polDlg->bmSdgChanged) //|| polDlg->bmDispBegChanged)
								{
									ogSdtData.DeleteVirtuellSdtsBySdg(prlSdg1->Urno);
									ogDataSet.CreateVirtuellSdtsBySdgu(prlSdg1->Urno);
									if(!prlSdg1->Expd)
									{
										SetReleaseBtnRed(true);
									}
									GetViewer()->LoadSdtWindow(true);
									pogCoverageDiagram->SendMessage(WM_UPDATE_COVERAGE, 0, 0);
									pogCoverageDiagram->SendMessage(WM_UPDATEDEMANDGANTT, 1, 0);
									GetParent() -> GetParent() -> SendMessage(WM_ACTIVEAUTOCOV, 0, 0L);
								}
							}
							else
								GetViewer()->SetDemandName(SHIFT_DEMAND, CString(""));
						}
					}
					delete polDlg;
				}
				else if(prlSdg != NULL)
				{
					MasterShiftDemand *polDlg = new MasterShiftDemand(this,prlSdg,omStartTime,omEndTime);
					if(polDlg->DoModal() == IDOK)
					{
						SDGDATA *prlSdg1 = ogSdgData.GetSdgByDnam(polDlg->omDnam);
						olSelectSdgName = polDlg->omDnam;

						if(prlSdg1 != NULL)
						{
							//uhi 17.01.01 Ladezeitraum und Master stimmen nicht �berein
							bool blTreffer = true;
	
							CCSPtrArray<TIMEFRAMEDATA> olTimes;

							ogSdgData.GetTimeFrames(prlSdg1->Urno,  olTimes);
							if((omStartTime != TIMENULL) && (omEndTime != TIMENULL)){
								for(int j = olTimes.GetSize() -1; j >= 0; j--){
									blTreffer = false;
									if(IsOverlapped(omStartTime, omEndTime, olTimes[j].StartTime, olTimes[j].EndTime) == TRUE){
										blTreffer = true;
										break;
									}
								}

							}
							olTimes.DeleteAll();

							if(blTreffer){
								GetViewer()->CurrentMasterShiftRequirement(prlSdg1->Urno);
								GetViewer()->SetDemandName(SHIFT_DEMAND,prlSdg1->Dnam);
								//uhi 18.9.00
								//Nur noch Infofeld Display Begin
								if(polDlg->bmSdgChanged) // || polDlg->bmDispBegChanged)
								{
									ogSdtData.DeleteVirtuellSdtsBySdg(prlSdg1->Urno);
									ogDataSet.CreateVirtuellSdtsBySdgu(prlSdg1->Urno);
									if(!prlSdg1->Expd)
									{
										SetReleaseBtnRed(true);
									}
									GetViewer()->LoadSdtWindow(true);
									pogCoverageDiagram->SendMessage(WM_UPDATE_COVERAGE, 0, 0);
									pogCoverageDiagram->SendMessage(WM_UPDATEDEMANDGANTT, 1, 0);
								}
							}
							else
								GetViewer()->SetDemandName(SHIFT_DEMAND, CString(""));
						}
					}
					delete polDlg;
				}
				CStringArray olDemandNames;
				GetViewer()->GetTimeFrameFromTo(omStartTime, omEndTime);
				ogSdgData.GetAllDnam(olDemandNames, omStartTime, omEndTime);
	
				omComboBox.ResetContent();
				omComboBox.AddString(LoadStg(IDS_STRING61249));//--Aktuell-
				omComboBox.AddString("");//To produce an empty line
				for(int i = 0; i < olDemandNames.GetSize(); i++)
				{
					omComboBox.AddString(olDemandNames[i]);
				}
				
				omComboBox.SelectString(-1,olSelectSdgName);
				olDemandNames.RemoveAll();
						

			}
			else
			{
				CWnd *polWnd = GetDlgItem(IDC_AUTOCOVRELEASE);
				if(polWnd!=NULL)
				{
					polWnd->ShowWindow(SW_HIDE);
					polWnd->EnableWindow(FALSE);
				}
				pomViewer->IsRealRoster(!pomViewer->IsRealRoster());
				bmNoReload = true;
				OnChartButton7();
				OnSelChangeCB();
				bmNoReload = false;
			}
		}
		break;
	case COVERAGE_BASESHIFTS:
		{
			CWaitCursor olWait;

			CString olOldBaseViewName = GetViewer()->GetBaseViewName();
			CString olOldViewName = GetViewer()->GetViewName();
			CString olViewName;
			int ilIdx;
			if((ilIdx = omComboBox.GetCurSel()) != LB_ERR)
			{
				omComboBox.GetLBText(ilIdx, olViewName);
			}
			GetViewer()->SetViewerKey("BASISSCHICHTEN");
			GetViewer()->SelectView(olViewName);
			SetFocus();

	//		CTime olEndTime = omStartTime + omDuration;
			CString olCaption;

			CTime olFrom,olTo;
			GetViewer()->GetTimeFrameFromTo(olFrom, olTo);

			CString olFromTxt = olFrom.Format("%d.%m.%Y / %H:%M");
			CString olToTxt = olTo.Format("%d.%m.%Y / %H:%M");
			olCaption = LoadStg(IDS_STRING61243) + olFromTxt + " - " + olToTxt;

			BaseShiftsPropertySheet olDlg("BASISSCHICHTEN",olCaption, this, pomViewer);
			if(olDlg.DoModal() != IDCANCEL)
			{
				olWait.Restore();
				CString olNewViewName = GetViewer()->GetViewName();
				GetViewer()->PrepareBaseShiftsFilter(olNewViewName);

				ReloadComboView();
				int ilPos = omComboBox.FindStringExact(-1, olNewViewName);

				omComboBox.SetCurSel(ilPos);

			}
			else
			{
				olWait.Restore();
			}
			ReloadComboView();
			GetViewer()->SetViewerKey(olOldBaseViewName);
			GetViewer()->SelectView(olOldViewName);

			pogCoverageDiagram->SendMessage(WM_UPDATEDEMANDGANTT, 1, 0);

		}
	}
	CCS_CATCH_ALL
}


LONG CoverageChart::OnChartButtonRButtonDown(UINT wParam, LONG lParam)
{
	CCS_TRY

	return 0L;
	CCS_CATCH_ALL
	return 0L;
}

void CoverageChart::OnMenuAssign()
{

}

void CoverageChart::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	CCS_TRY
	GetParent()->SendMessage(WM_KEYDOWN, nChar, MAKELONG(LOWORD(nRepCnt), LOWORD(nFlags)));
	CCS_CATCH_ALL
}

void CoverageChart::OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	CCS_TRY
	GetParent()->SendMessage(WM_KEYUP, nChar, MAKELONG(LOWORD(nRepCnt), LOWORD(nFlags)));
	CCS_CATCH_ALL
}


LONG CoverageChart::OnUpdateCombo(UINT wParam, LONG lParam)
{
	CCS_TRY
	char *pcpName;
	pcpName = (char*)lParam;

	CWaitCursor olWait;
	int ilIdx = omComboBox.FindStringExact( -1, pcpName);
	if(ilIdx != LB_ERR)
	{
		omComboBox.SetCurSel(ilIdx);
	}
	pogCoverageDiagram->SendMessage(WM_UPDATEDEMANDGANTT, 0, 0);
	//pogCoverageDiagram->SendMessage(WM_UPDATE_COVERAGE, 0, 0);
	return 0L;
	CCS_CATCH_ALL
	return 0L;
}

void CoverageChart::DeaktivateCombobox(bool blDeaktivate)
{
	omComboBox.EnableWindow((blDeaktivate)?FALSE:TRUE);
}




LONG CoverageChart::OnReloadCombo(UINT wParam, LONG lParam)
{
	CCS_TRY
    if(imChartType==COVERAGE_SHIFTDEMANDS)
	{
		omComboBox.ResetContent();
		if(GetViewer()->GetShiftRoster())
		{
			if(pomViewer->IsRealRoster())
			{
				CString olSteps = ogCCSParam.GetParamValue("ROSTER","ID_SHOW_STEPS",CTime::GetCurrentTime().Format("%Y%m%d%H%M%S"),true);
				for(int i = 0; i <= 6; i++)
				{
					switch(i)
					{
					case 0:	// Shift roster
						if (olSteps.Find('1') >= 0)
						{
							int ind = omComboBox.AddString(LoadStg(IDS_STRING1586));
							omComboBox.SetItemData(ind,1);
						}
						break;
					case 1:	// Duty roster
						if (olSteps.Find('2') >= 0)
						{
							int ind = omComboBox.AddString(LoadStg(IDS_STRING1587));
							omComboBox.SetItemData(ind,2);
						}
						break;
					case 2:	// Daily list of employees
						if (olSteps.Find('3') >= 0)
						{
							int ind = omComboBox.AddString(LoadStg(IDS_STRING1588));
							omComboBox.SetItemData(ind,3);
						}
						break;
					case 3:	// Postprocessing of day
						if (olSteps.Find('4') >= 0)
						{
							int ind = omComboBox.AddString(LoadStg(IDS_STRING1589));
							omComboBox.SetItemData(ind,4);
						}
						break;
					case 4:	// Postprocessing
						if (olSteps.Find('5') >= 0)
						{
							int ind = omComboBox.AddString(LoadStg(IDS_STRING61256));
							omComboBox.SetItemData(ind,5);
						}
						break;
					case 5:	// Long term shift plan
						if (olSteps.Find('L') >= 0)
						{
							int ind = omComboBox.AddString(LoadStg(IDS_STRING61255));
							omComboBox.SetItemData(ind,6);
						}
						break;
					case 6:	// Current (Active) level
						{
							int ind = omComboBox.AddString(LoadStg(IDS_STRING1791));
							omComboBox.SetItemData(ind,7);
						}
						break;
					default:
						break;
					}
				}
				int ilDemLevel = GetViewer()->GetCurrentDemandLevel(SHIFT_DEMAND);
				omComboBox.SetCurSel(-1);
				for (i = 0; i < omComboBox.GetCount(); i++)
				{
					if (omComboBox.GetItemData(i) == ilDemLevel)
					{
						omComboBox.SetCurSel(i);
						break;
					}
				}
			}
			else
			{
				CStringArray olShiftNameArray;
				pomViewer->ShiftNameArray(olShiftNameArray);
				for(int ilLc = 0; ilLc < olShiftNameArray.GetSize();ilLc++)
				{
					omComboBox.AddString(olShiftNameArray[ilLc]);
				}
				
				int ilWidth = CalculateMaxWidth();
				omComboBox.SetDroppedWidth(ilWidth);
			
				omComboBox.SelectString(-1,pomViewer->CurrentShiftRosterName());
			}	

			int ilWidth = CalculateMaxWidth();
			omComboBox.SetDroppedWidth(ilWidth);
		
		}
		else
		{
			CStringArray olDemandNames;

			GetViewer()->GetTimeFrameFromTo(omStartTime, omEndTime);
			ogSdgData.GetAllDnam(olDemandNames, omStartTime, omEndTime);
						
			omComboBox.AddString(LoadStg(IDS_STRING61249));//--Aktuell--

			omComboBox.AddString("");//To produce an empty line
			for(int i = 0; i < olDemandNames.GetSize(); i++)
			{
				omComboBox.AddString(olDemandNames[i]);
				ogSdgData.CheckSelected(ogSdgData.GetUrnoByDnam(olDemandNames[i]),false);

			}
			int ilWidth = CalculateMaxWidth();
			omComboBox.SetDroppedWidth(ilWidth);
			CString olDemName = GetViewer()->GetCurrentDemandName(SHIFT_DEMAND);
			int ilIdx = omComboBox.FindStringExact( -1, olDemName);
			if(ilIdx != LB_ERR)
			{
				omComboBox.SetCurSel(ilIdx);
				ogSdgData.CheckSelected(ogSdgData.GetUrnoByDnam(olDemName),true);
			}
		}
	}
	return 0L;
	CCS_CATCH_ALL
	return 0L;
}

LONG CoverageChart::OnDragOver(UINT wParam, LONG lParam)
{
	return -1L;	// cannot accept this object
//----------------------------------------------------------------------
}

LONG CoverageChart::OnDrop(UINT wParam, LONG lParam)
{
    return -1L;
//----------------------------------------------------------------------
}

LONG CoverageChart::ProcessDropShifts(CCSDragDropCtrl *popDragDropCtrl, DROPEFFECT lpDropEffect)
{
	return 0L;
}

//-DTT Jul.23-----------------------------------------------------------
LONG CoverageChart::ProcessDropDutyBar(CCSDragDropCtrl *popDragDropCtrl, DROPEFFECT lpDropEffect)
{
	return 0L;
}

LONG CoverageChart::ProcessDropFlightBar(CCSDragDropCtrl *popDragDropCtrl, DROPEFFECT lpDropEffect)
{
	return 0L;
}
//----------------------------------------------------------------------
void CoverageChart::OnSelChangeCB()
{
	CCS_TRY
		SYSTEMTIME rlPerf;
	switch(imChartType)
	{
	case COVERAGE_GRAPHIC:
		{
			int ilIdx;
			if((ilIdx = omComboBox.GetCurSel()) != LB_ERR)
			{
				CWaitCursor olWait;
				CString olSelString;
				omComboBox.GetLBText(ilIdx, olSelString);
				GetViewer()->PrepareEvaluationFilter(olSelString);
				GetViewer()->SetReCalcShifts(true);

				SetStaticFieldText();
				pogCoverageDiagram->SendMessage(WM_UPDATEDEMANDGANTT, 1, 0);

			}
		}
		break;
	case COVERAGE_SHIFTDEMANDS:
		{
			int ilIdx;
			pomViewer->ShowActiveDrrLevel(false);

			if((ilIdx = omComboBox.GetCurSel()) != LB_ERR)
			{
				if(GetViewer()->GetShiftRoster()==false)
				{
					CWaitCursor olWait;
					CString olSelString;
					omComboBox.GetLBText(ilIdx, olSelString);
					GetViewer()->SetDemandName(SHIFT_DEMAND, olSelString);

					if(olSelString.IsEmpty())	// empty line selected ?
					{
						GetParent() -> GetParent() -> SendMessage(WM_DEACTIVEAUTOCOV, 0, 0L);
					}
					else
					{
						GetParent() -> GetParent() -> SendMessage(WM_ACTIVEAUTOCOV, 0, 0L);
					}
					SetReleaseBtnRed(false);
					if(olSelString == LoadStg(IDS_STRING61249))	// 'current' selected ?
					{
						pomViewer->SetActuellState(true);
						GetViewer()->CurrentMasterShiftRequirement(0);
						GetViewer()->LoadSdtWindow(true);
						pogCoverageDiagram->SendMessage(WM_UPDATEDEMANDGANTT, 0, 0);
												
						CWnd *polWnd = GetDlgItem(IDC_CHARTBUTTON2);
						if(polWnd!=NULL)
						{
							polWnd->ShowWindow(SW_HIDE);
							polWnd->EnableWindow(FALSE);
						}
						polWnd = GetDlgItem(IDC_CHARTBUTTON3);
						if(polWnd!=NULL)
						{
							polWnd->ShowWindow(SW_HIDE);
							polWnd->EnableWindow(FALSE);
						}
						polWnd = GetDlgItem(IDC_CHARTBUTTON6);
						if(polWnd!=NULL)
						{
							polWnd->ShowWindow(SW_HIDE);
							polWnd->EnableWindow(FALSE);
						}

					}
					else
					{
						long llSdgUrno = ogSdgData.GetUrnoByDnam(olSelString);
						GetViewer()->CurrentMasterShiftRequirement(llSdgUrno);
						if(!ogSdgData.CheckSelected(llSdgUrno,true))
						{
							ogSdtData.DeleteVirtuellSdtsBySdg(llSdgUrno);	// create virtual SDT's from
							ogDataSet.CreateVirtuellSdtsBySdgu(llSdgUrno);
						}
						
						SDGDATA *prlSdg = ogSdgData.GetSdgByUrno(llSdgUrno);
						if(prlSdg != NULL)
						{
							if(prlSdg->Expd)
							{
								SetReleaseBtnRed(false);
							}
							else
							{
								SetReleaseBtnRed(true);
							}

						}
						CWnd *polWnd = GetDlgItem(IDC_CHARTBUTTON2);
						if(polWnd!=NULL)
						{
							polWnd->ShowWindow(SW_SHOW);
							polWnd->EnableWindow(TRUE);
						}
						polWnd = GetDlgItem(IDC_CHARTBUTTON3);
						if(polWnd!=NULL)
						{
							polWnd->ShowWindow(SW_SHOW);
							polWnd->EnableWindow(TRUE);
						}
						polWnd = GetDlgItem(IDC_CHARTBUTTON6);
						if(polWnd!=NULL)
						{
							if (bmOperEnabled)
							{
								if (prlSdg != NULL && prlSdg->Operation[0] == 'O')
								{
									polWnd->ShowWindow(SW_SHOW);
									polWnd->EnableWindow(TRUE);
								}
							}
							else
							{
								polWnd->ShowWindow(SW_SHOW);
								polWnd->EnableWindow(TRUE);
							}
						}

						pomViewer->SetActuellState(false);

						GetViewer()->LoadSdtWindow(true);
						GetSystemTime(&rlPerf);
						TRACE("OnSelChangeCB: ReadSDT END %d:%d:%d.%d\n",rlPerf.wHour,rlPerf.wMinute,rlPerf.wSecond,rlPerf.wMilliseconds);
						pogCoverageDiagram->SendMessage(WM_UPDATEDEMANDGANTT, 0, 0);
					}
				}
				else
				{
					CString olSelString;
					CWaitCursor olWait;
					omComboBox.GetLBText(ilIdx, olSelString);

					CWnd *polWnd = GetDlgItem(IDC_AUTOCOVRELEASE);
					if(polWnd!=NULL)
					{
						polWnd->ShowWindow(SW_HIDE);
						polWnd->EnableWindow(FALSE);
					}
					polWnd = GetDlgItem(IDC_AUTOCOVDISCARD);
					if(polWnd!=NULL)
					{
						polWnd->ShowWindow(SW_HIDE);
						polWnd->EnableWindow(FALSE);
					}

					if(pomViewer->IsRealRoster())
					{
						if (ilIdx >= 0)
						{	
							DWORD	llNow,llLast, llStart;
							double	flDiff;

							llLast = GetTickCount();
							llStart = llLast;

							CTime olFrom,olTo;
							GetViewer()->GetTimeFrameFromTo(olFrom, olTo);
							int ilRosl = omComboBox.GetItemData(ilIdx);
							GetViewer()->SetDemandLevel(SHIFT_DEMAND, ilRosl);

							CString olWhere = "";
							if (ilRosl == 7)
								olWhere.Format(" WHERE SDAY BETWEEN '%s' AND '%s' AND ROSS = 'A'",olFrom.Format("%Y%m%d"), olTo.Format("%Y%m%d"));
							else if (ilRosl == 6)
								olWhere.Format(" WHERE SDAY BETWEEN '%s' AND '%s' AND ROSL = 'L'",olFrom.Format("%Y%m%d"), olTo.Format("%Y%m%d"));
							else
								olWhere.Format(" WHERE SDAY BETWEEN '%s' AND '%s' AND ROSL = '%d'",olFrom.Format("%Y%m%d"), olTo.Format("%Y%m%d"),ilRosl);

							//MWO: 27.03.03
							ogBcHandle.SetFilterRange("DRRTAB", "SDAY", "SDAY", olFrom.Format("%Y%m%d"), olTo.Format("%Y%m%d"));
							//END MWO: 27.03.03
							ogDrrData.Read(olWhere);

							llNow  = GetTickCount();
							flDiff = llNow-llLast;
							llLast = llNow;
							TRACE("CoverageChart::OnSelChangeCB:ogDrrData.Read() %.3lf sec\n", flDiff/1000.0 );


							ogSdtRosterData.ClearAll();

							llNow  = GetTickCount();
							flDiff = llNow-llLast;
							llLast = llNow;
							TRACE("CoverageChart::OnSelChangeCB:ogSdtRosterData.ClearAll() %.3lf sec\n", flDiff/1000.0 );

							ogDataSet.CreateSdtFromDrr();

							llNow  = GetTickCount();
							flDiff = llNow-llLast;
							llLast = llNow;
							TRACE("CoverageChart::OnSelChangeCB:ogDataSet.CreateSdtFromDrr() %.3lf sec\n", flDiff/1000.0 );

							if(LoadStg(IDS_STRING1791) == olSelString)	// "active" level only 
							{
								
								pomViewer->ShowActiveDrrLevel(true);
							}
							else
							{
								if (LoadStg(IDS_STRING1587) == olSelString)	// duty roster level
								{
#ifdef	AUTOCOVERAGE
									CWnd *polWnd = GetDlgItem(IDC_AUTOCOVRELEASE);
									if(polWnd!=NULL)
									{
										
										polWnd->ShowWindow(SW_SHOW);
										polWnd->EnableWindow(TRUE);
										CString olText = LoadStg(IDS_STRING61391);	// optimize breaks
										polWnd->SetWindowText(olText);
									}
#endif
								}
							}

							GetViewer()->LoadSdtWindow(true);

							llNow  = GetTickCount();
							flDiff = llNow-llLast;
							llLast = llNow;
							TRACE("CoverageChart::OnSelChangeCB:GetViewer()->LoadSdtWindow() %.3lf sec\n", flDiff/1000.0 );

							pogCoverageDiagram->SendMessage(WM_UPDATEDEMANDGANTT, 0, 1);

							llNow  = GetTickCount();
							flDiff = llNow-llLast;
							llLast = llNow;
							TRACE("CoverageChart::OnSelChangeCB:pogCoverageDiagram->SendMessage() %.3lf sec\n", flDiff/1000.0 );

						}
						else	// ilIdx == -1 !
						{
							GetViewer()->SetDemandLevel(SHIFT_DEMAND, ilIdx);
						}
					}
					else
					{
						pomViewer->CurrentShiftRosterName(olSelString);
						CWaitCursor olWait;
						CTime olFrom,olTo;
						GetViewer()->GetTimeFrameFromTo(olFrom, olTo);
						ogSdtShiftData.ClearAll();
						ogDataSet.CreateSdtFromSpl(olSelString,olFrom,olTo);
						GetViewer()->LoadSdtWindow(true);
						pogCoverageDiagram->SendMessage(WM_UPDATEDEMANDGANTT, 0, 1);
					}
				}
			}
		}
		break;
	case COVERAGE_BASESHIFTS:
		{
			int ilIndex = omComboBox.GetCurSel();
			if(ilIndex!=CB_ERR)
			{
				CString olSelString;
				omComboBox.GetLBText(ilIndex, olSelString);
				GetViewer()->PrepareBaseShiftsFilter(olSelString);
				
				pogCoverageDiagram->SendMessage(WM_UPDATE_BASESHIFT, 0, 0);
			}
		}
	}
	CCS_CATCH_ALL
}


//@ManMemo:OnChartButton3 
 /*@Doc:
		for imChartType == COVERAGE_GRAPHIC
				Opens the result table
		for imChartType == COVERAGE_SHIFTDEMANDS
				deletes the selected mastershift demand
 */

void CoverageChart::OnChartButton3()
{
	CCS_TRY
	switch(imChartType)
	{
	case COVERAGE_GRAPHIC:
		{
			if (pomResultTable == NULL)
			{
				pomResultTable = new CResultTable(this,COV_TAB,omGraphic.omFilterType);
				if (pomResultTable == NULL || pomResultTable->Create(IDD_RESULT_TABLE,this) == FALSE)
				{
					MessageBox(LoadStg(IDS_STRING1731), "", MB_ICONSTOP);
					return;
				}
			}

			omGraphic.OnResultTable(pomResultTable);

		}
		break;
	case COVERAGE_SHIFTDEMANDS:
		{
			if (GetViewer()->GetShiftRoster() == false)
			{
				int ilIdx;
				CString olSelString;
				if((ilIdx = omComboBox.GetCurSel()) != LB_ERR)
				{
					omComboBox.GetLBText(ilIdx, olSelString);
				}

				CDelSdgDlg olDelSdgDlg(olSelString,this);
				if (olDelSdgDlg.DoModal() == IDOK)
				{
					CString olText;
					olText = LoadStg(COV_DEMANDS_REALLY_DEL_ALL);
					if (MessageBox(olText, "", (MB_ICONQUESTION|MB_YESNO)) == IDYES)
					{
						CWaitCursor olWait;

						CStringArray olSelList;
						olDelSdgDlg.GetSelection(olSelList);

						// rescan for current selection due to broadcast processing 
						if((ilIdx = omComboBox.GetCurSel()) != LB_ERR)
						{
							omComboBox.GetLBText(ilIdx, olSelString);
						}
						else
						{
							olSelString = "";
						}

						for (int i = 0; i < olSelList.GetSize(); i++)
						{
							if (olSelList[i] == olSelString)
							{
								GetViewer()->SetDemandName(SHIFT_DEMAND,"");
								
								long llSdgUrno = ogSdgData.GetUrnoByDnam(olSelString);

								SDGDATA *prlSdg =  ogSdgData.GetSdgByUrno(llSdgUrno);

								omComboBox.SetCurSel(-1);
								ilIdx = omComboBox.FindStringExact( -1, olSelString);
								if (ilIdx != CB_ERR)
								{
									int ilCnt = omComboBox.DeleteString(ilIdx);
								}

								//uhi 17.01.01
								SetReleaseBtnRed(false);

								if (prlSdg != NULL)
								{
									ogSdgData.DeleteSdg(prlSdg, TRUE);
									ogSdgData.DeleteSdgInternal(prlSdg);
									ogSdtData.DeleteBySdgu(llSdgUrno);
									ogMsdData.DeleteBySdgu(llSdgUrno);
									
									int ilWidth = CalculateMaxWidth();
									omComboBox.SetDroppedWidth(ilWidth);
									pogCoverageDiagram->SendMessage(WM_UPDATEDEMANDGANTT, 0, 0);
								}
							}
							else
							{
								long llSdgUrno = ogSdgData.GetUrnoByDnam(olSelList[i]);
								SDGDATA *prlSdg =  ogSdgData.GetSdgByUrno(llSdgUrno);

								ilIdx = omComboBox.FindStringExact( -1, olSelList[i]);
								if (ilIdx != CB_ERR)
								{
									int ilCnt = omComboBox.DeleteString(ilIdx);
								}
								
								if (prlSdg != NULL)
								{
									ogSdgData.DeleteSdg(prlSdg, TRUE);
									ogSdgData.DeleteSdgInternal(prlSdg);
									ogSdtData.DeleteBySdgu(llSdgUrno);
									ogMsdData.DeleteBySdgu(llSdgUrno);
				
									int ilWidth = CalculateMaxWidth();
									omComboBox.SetDroppedWidth(ilWidth);
								}
							}
						}
					}

					this->OnReloadCombo(0,0l);
						
				}
			}
		}
		break;
	case COVERAGE_BASESHIFTS:
		{
			
		}
	}
	CCS_CATCH_ALL
}


int CoverageChart::CalculateMaxWidth()
{
	int ilMaxWidth = 100;

	CRect rect;
	
	omComboBox.GetClientRect(rect);
	
	ilMaxWidth = rect.right - rect.left;

	CClientDC olCdc(this);
	
	CFont *polFont = omComboBox.GetFont();
	if(polFont!=NULL)
	{
		olCdc.SelectObject(polFont);
		for(int i=0;i<omComboBox.GetCount();i++)
		{
			CString olText;
			omComboBox.GetLBText(i,olText);
			CSize olSize = olCdc.GetTextExtent(olText);
			if(olSize.cx>=ilMaxWidth)
			{
				ilMaxWidth = olSize.cx + 5;
			}
		}
	}
	return ilMaxWidth;
}

void CoverageChart::ReloadComboView()
{
	CCS_TRY
	int ilIdx = omComboBox.GetCurSel();

	switch(imChartType)
	{
		case COVERAGE_GRAPHIC:
		{	
			omComboBox.ResetContent();
			CString olOldBaseViewName = GetViewer()->GetBaseViewName();
			CString olOldViewName = GetViewer()->GetViewName();
			CString olViewName;
			
			GetViewer()->SetViewerKey("BEWERTUNG");
			CStringArray olStrArr;
			GetViewer()->GetViews(olStrArr);
			for (int ilIndex = 0; ilIndex < olStrArr.GetSize(); ilIndex++)
			{
				omComboBox.AddString(olStrArr[ilIndex]);
			}
			if(ilIdx == -1)
			{
				ilIdx = omComboBox.FindStringExact(-1,"<Default>");
			}

			omComboBox.SetCurSel(ilIdx);
			GetViewer()->SetViewerKey(olOldBaseViewName);
			GetViewer()->SelectView(olOldViewName);

			SetStaticFieldText();
		}
	}

	if(imChartType!= COVERAGE_SHIFTDEMANDS || ilIdx==LB_ERR)
	{
		if(imChartType==COVERAGE_SHIFTDEMANDS)
		{
			CWaitCursor olWait;
			pogCoverageDiagram->SendMessage(WM_UPDATEDEMANDGANTT, 0, 0);
		}
	}
	else
	{
		if(GetViewer()->GetShiftRoster()==false)
		{
			CWaitCursor olWait;
			CString olSelString;
			omComboBox.GetLBText(ilIdx, olSelString);
			GetViewer()->SetDemandName(SHIFT_DEMAND, olSelString);
			long llSdgUrno = ogSdgData.GetUrnoByDnam(olSelString);
			GetViewer()->LoadSdtWindow(true);
		}
		else
		{
			CString olSelString;
			CWaitCursor olWait;
			omComboBox.GetLBText(ilIdx, olSelString);
			if (ilIdx >= 0)
			{	
				int ilRosl = omComboBox.GetItemData(ilIdx);
				GetViewer()->SetDemandLevel(SHIFT_DEMAND, ilRosl);
				GetViewer()->LoadSdtWindow(true);
			}
			else // ilIdx == 0 !
			{
				GetViewer()->SetDemandLevel(SHIFT_DEMAND, ilIdx);
			}
		}
		pogCoverageDiagram->SendMessage(WM_UPDATEDEMANDGANTT, 1, 0);
	}
	if(imChartType == COVERAGE_BASESHIFTS)
	{
		omComboBox.ResetContent();
		CString olOldBaseViewName = GetViewer()->GetBaseViewName();
		CString olOldViewName = GetViewer()->GetViewName();
		CString olViewName;
		
		GetViewer()->SetViewerKey("BASISSCHICHTEN");
		CStringArray olStrArr;
		GetViewer()->GetViews(olStrArr);
		for (int ilIndex = 0; ilIndex < olStrArr.GetSize(); ilIndex++)
		{
			omComboBox.AddString(olStrArr[ilIndex]);
		}
		if(ilIdx == -1)
		{
			ilIdx = omComboBox.FindStringExact(-1,"<Default>");
		}

		omComboBox.SetCurSel(ilIdx);
		GetViewer()->SetViewerKey(olOldBaseViewName);
		GetViewer()->SelectView(olOldViewName);
	}


	CCS_CATCH_ALL
}

void CoverageChart::SetStaticFieldText()
{
	CString olStaticText = "";
	if(GetViewer()->IsLocation())
	{
		olStaticText = LoadStg(IDS_STRING61245);
	}
	else if(GetViewer()->IsPersonal())
	{
		olStaticText = LoadStg(IDS_STRING61244);
	}
	else if(GetViewer()->IsEquipment())
	{
		olStaticText = LoadStg(IDS_STRING61246);
	}
	if(pomTopScaleText != NULL)
	{
		pomTopScaleText->SetWindowText(olStaticText);
	}
}

void CoverageChart::SetPIdxFieldText()
{
	CString olStaticText = LoadStg(IDS_STRING61247);
	CString olPIdxText; 
	if(omGraphic.imPIdx>= 0)
	{
		olPIdxText.Format("%d",omGraphic.imPIdx);
	}
	else
	{
		olPIdxText = "-";
	}

	olStaticText += olPIdxText;
	if(pomPIdxText != NULL)
	{
		pomPIdxText->SetWindowText(olStaticText);
	}
}

void CoverageChart::SetReleaseBtnRed(bool bpSetRed)
{
	bmReleaseIsRed = bpSetRed;
	pomViewer->SdgChanged(bpSetRed);

	if(imChartType == COVERAGE_SHIFTDEMANDS)
	{
		if(bmReleaseIsRed)
		{
			omButton6.SetColors(
				RED,
			::GetSysColor(COLOR_BTNSHADOW),
			::GetSysColor(COLOR_BTNHIGHLIGHT));
		}
		else
		{
			omButton6.SetColors(
			::GetSysColor(COLOR_BTNFACE),
			::GetSysColor(COLOR_BTNSHADOW),
			::GetSysColor(COLOR_BTNHIGHLIGHT));
		}

	}
}

void CoverageChart::SetUpdateBtnRed(bool bpSetRed)
{
	bmUpdateIsRed = bpSetRed;

	if (bmUpdateIsRed)
	{
		omDemUpdate.SetColors(RED,::GetSysColor(COLOR_BTNSHADOW),::GetSysColor(COLOR_BTNHIGHLIGHT));
	}
	else
	{
		omDemUpdate.SetColors(::GetSysColor(COLOR_BTNFACE),::GetSysColor(COLOR_BTNSHADOW),::GetSysColor(COLOR_BTNHIGHLIGHT));
	}
}

void CoverageChart::ChangeToAutoCov(bool bpAutoCov)
{
	pomViewer->AutoCovIsActive(bpAutoCov);

	int ilIdx = 0;
	CString olSelString;
	bool blIsCurrent = false;
	if(imChartType == COVERAGE_SHIFTDEMANDS)
	{
	
		if(omShiftRoster.GetCheck() != 1)
		{
			if(bpAutoCov)
			{
				omShiftRoster.EnableWindow(FALSE);
			}
			else
			{
				omShiftRoster.EnableWindow(TRUE);
			}

			DeaktivateCombobox(bpAutoCov);
			olSelString = GetViewer()->GetCurrentDemandName(SHIFT_DEMAND);

			if(olSelString == LoadStg(IDS_STRING61249))
			{
				blIsCurrent = true;
			}

			CWnd *polWnd = GetDlgItem(IDC_CHARTBUTTON2);
			if(polWnd!=NULL)
			{
				polWnd->ShowWindow((bpAutoCov || blIsCurrent)?SW_HIDE:SW_SHOW);
			}
			polWnd = GetDlgItem(IDC_CHARTBUTTON3);
			if(polWnd!=NULL)
			{
				polWnd->ShowWindow((bpAutoCov || blIsCurrent)?SW_HIDE:SW_SHOW);
			}
			polWnd = GetDlgItem(IDC_CHARTBUTTON6);
			if(polWnd!=NULL)
			{
				if (bpAutoCov || blIsCurrent)
				{
					polWnd->ShowWindow(SW_HIDE);
					polWnd->EnableWindow(FALSE);
				}
				else
				{
					if (bmOperEnabled)
					{
						if (GetViewer()->CurrentMasterShiftRequirement() != 0)
						{
							SDGDATA *polSdg = ogSdgData.GetSdgByUrno(GetViewer()->CurrentMasterShiftRequirement());
							if (polSdg != NULL && polSdg->Operation[0] == 'O')
							{
								polWnd->ShowWindow(SW_SHOW);
								polWnd->EnableWindow(TRUE);
							}
							else
							{
								polWnd->ShowWindow(SW_HIDE);
								polWnd->EnableWindow(FALSE);
							}
						}
					}
					else
					{
						polWnd->ShowWindow(SW_SHOW);
						polWnd->EnableWindow(TRUE);
					}
				}
			}
			polWnd = GetDlgItem(IDC_AUTOCOVRELEASE);
			if(polWnd!=NULL)
			{
				polWnd->ShowWindow((!bpAutoCov)?SW_HIDE:SW_SHOW);
				polWnd->EnableWindow((!bpAutoCov)?FALSE:TRUE);
				if (bpAutoCov)
				{
					CString olText = LoadStg(IDS_STRING1927);
					polWnd->SetWindowText(olText);
				}
				else
				{
					CString olText = LoadStg(IDS_STRING1663);
					polWnd->SetWindowText(olText);
				}
			}
			polWnd = GetDlgItem(IDC_AUTOCOVDISCARD);
			if(polWnd!=NULL)
			{
				polWnd->ShowWindow((!bpAutoCov)?SW_HIDE:SW_SHOW);
				polWnd->EnableWindow((!bpAutoCov)?FALSE:TRUE);
			}
		}
	}
}

void CoverageChart::OnAutoCovRelease()
{
#ifdef	AUTOCOVERAGE
	CCS_TRY
	switch(imChartType)
	{
		case COVERAGE_SHIFTDEMANDS:
		{
			CWaitCursor olWait;
			if(GetViewer()->GetShiftRoster())
			{
				if(!bmOptBreakIsActive)
				{

					pomViewer->BreakPostOptimization(omStartTime,omEndTime,false);
					omShiftRoster.EnableWindow(false);
					omComboBox.EnableWindow(false);
					CWnd *polWnd = GetDlgItem(IDC_AUTOCOVDISCARD);
					if(polWnd!=NULL)
					{
						polWnd->ShowWindow(SW_SHOW);
						polWnd->EnableWindow(TRUE);
					}				
					polWnd = GetDlgItem(IDC_AUTOCOVRELEASE);
					if(polWnd != NULL)
					{
						polWnd->SetWindowText(LoadStg(IDS_STRING1927));
					}

					polWnd = GetDlgItem(IDC_CHARTBUTTON2);	// hide "Roster-Level" button
					if(polWnd!=NULL)
					{
						polWnd->ShowWindow(SW_HIDE);
						polWnd->EnableWindow(FALSE);
					}

				}
				else
				{
					omShiftRoster.EnableWindow(true);
					omComboBox.EnableWindow(true);
					CWnd *polWnd = GetDlgItem(IDC_AUTOCOVDISCARD);
					if(polWnd!=NULL)
					{
						polWnd->ShowWindow(SW_HIDE);
						polWnd->EnableWindow(FALSE);
					}
					polWnd = GetDlgItem(IDC_AUTOCOVRELEASE);
					if(polWnd!=NULL)
					{
						polWnd->ShowWindow(SW_SHOW);
						polWnd->EnableWindow(TRUE);
						polWnd->SetWindowText(LoadStg(IDS_STRING61391));	// optimize breaks
					}

					polWnd = GetDlgItem(IDC_CHARTBUTTON2);	// show "Roster-Level" button
					if(polWnd!=NULL)
					{
						polWnd->ShowWindow(SW_SHOW);
						polWnd->EnableWindow(TRUE);
					}

					GetViewer()->SaveDRRBreakChanges();
				}
				bmOptBreakIsActive = !bmOptBreakIsActive;
			}
			else
			{
				pomViewer->ReleaseAutoCovChanges();
				GetParent() -> GetParent() -> SendMessage(WM_ACTIVEAUTOCOV, 0, 0L);
				ChangeToAutoCov(false);
			}
		}
	}
	CCS_CATCH_ALL
#endif
}

void CoverageChart::OnAutoCovDiscard()
{
#ifdef	AUTOCOVERAGE
	CCS_TRY
	switch(imChartType)
	{
		case COVERAGE_SHIFTDEMANDS:
		{
			if(GetViewer()->GetShiftRoster())
			{
				if(bmOptBreakIsActive)
				{

					omShiftRoster.EnableWindow(true);
					omComboBox.EnableWindow(true);
					CWnd *polWnd = GetDlgItem(IDC_AUTOCOVDISCARD);
					if(polWnd!=NULL)
					{
						polWnd->ShowWindow(SW_HIDE);
						polWnd->EnableWindow(FALSE);
					}
					polWnd = GetDlgItem(IDC_AUTOCOVRELEASE);
					if(polWnd!=NULL)
					{
						polWnd->ShowWindow(SW_SHOW);
						polWnd->EnableWindow(TRUE);
						polWnd->SetWindowText(LoadStg(IDS_STRING61391));
					}

					polWnd = GetDlgItem(IDC_CHARTBUTTON2);	// show "Roster-Level" button
					if(polWnd!=NULL)
					{
						polWnd->ShowWindow(SW_SHOW);
						polWnd->EnableWindow(TRUE);
					}

					GetViewer()->DiscardDRRBreakChanges();
					bmOptBreakIsActive = !bmOptBreakIsActive;
				}
			}
			else
			{
				pomViewer->DiscardAutoCovChanges();
				GetParent() -> GetParent() -> SendMessage(WM_ACTIVEAUTOCOV, 0, 0L);
				ChangeToAutoCov(false);
			}
		}
	}
	CCS_CATCH_ALL
#endif
}

void CoverageChart::OnDemUpdate()
{
	if (!omDemUpdate.Recess())	// status was NO_UPDATE
	{
		omDemUpdate.SetWindowText(LoadStg(IDS_STRING1795));
		pomViewer->NoDemUpdates(false);
		if (bmUpdateIsRed && pomViewer->DemUpdateAllowed())
		{
			pomViewer->ReloadAllDems();
			SetUpdateBtnRed(false);
		}
	}
	else	// status was UPDATE
	{
		omDemUpdate.SetWindowText(LoadStg(IDS_STRING1796));
		pomViewer->NoDemUpdates(true);
	}

	omDemUpdate.Recess(!omDemUpdate.Recess());
}

void CoverageChart::OnExcelButton()
{
	this->Export("Test");
}


void CoverageChart::Export(const CString& opExportName)
{
	char pclConfigPath[512];
	char pclExcelPath[512];
	char pclDelimiter[256];

	if (getenv("CEDA") == NULL)
		strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
	else
		strcpy(pclConfigPath, getenv("CEDA"));

	GetPrivateProfileString(ogAppName, "Excel", "DEFAULT",pclExcelPath, sizeof pclExcelPath, pclConfigPath);

	GetPrivateProfileString(ogAppName, "ExcelSeparator", ";",pclDelimiter, sizeof pclDelimiter, pclConfigPath);

	if (!strcmp(pclExcelPath, "DEFAULT"))
	{
		AfxMessageBox(LoadStg(IDS_STRING1951), MB_ICONERROR);
		return;
	}

	CWaitCursor olDummy;
	CString olExcelFileName = this->CreateFileName();

	CreateExcelFile(olExcelFileName,pclDelimiter);

	// testing error
	if (olExcelFileName.IsEmpty() || olExcelFileName.GetLength() > 255)
	{
		CString olMsg;
		if (olExcelFileName.IsEmpty())
		{
			CString olMsg = "Filename is empty !" + olExcelFileName;
			AfxMessageBox(olMsg, MB_ICONERROR);
			return;
		}
		if (olExcelFileName.GetLength() > 255 )
		{
			CString olMsg = "Length of the filename > 255: " + olExcelFileName;
			AfxMessageBox(olMsg,  MB_ICONERROR);
			return;
		}

		olMsg = "Filename invalid: " + olExcelFileName;
		AfxMessageBox(olMsg, MB_ICONERROR);
		return;
	}

	char pclTmp[1024];
	strcpy(pclTmp, olExcelFileName); 

	// Set up parameters to be sent:
	char *args[4];
	args[0] = "child";
	args[1] = pclTmp;
	args[2] = NULL;
	args[3] = NULL;

	_spawnv( _P_NOWAIT , pclExcelPath, args );
}

CString CoverageChart::CreateFileName()
{
	CString olRet;

	CString olExcelFileName;
	CString olViewName;

				
	olViewName.Format ("Coverage %s", this->GetActiveViewName()); // getting the title (name of the view)
	olExcelFileName.Format("%s %s", olViewName, (CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")) );

	olExcelFileName.Remove('*');
	olExcelFileName.Remove('.');
	olExcelFileName.Remove(':');
	olExcelFileName.Remove('/');
	olExcelFileName.Replace(" ", "_");

	char pHeader[256];
	strcpy (pHeader, CCSLog::GetTmpPath());
	CString path = pHeader;
	olRet =  path + "\\" + olExcelFileName + ".csv";
	return olRet;
}

bool CoverageChart::CreateExcelFile(const CString& ropExcelFileName,const CString& ropDelimiter)
{
	COleDateTime olDayTmp;
	CStringArray olArr;

	CString olTmp;
	CString olSday;
	CString olName;
	CString olFunction;


	CTime olLoadStart;
	CTime olLoadEnd;
	CString olViewName;
	CString olViewType;

	if (pomViewer == NULL)
		return false;

	pomViewer->GetTimeFrameFromTo(olLoadStart,olLoadEnd);

	olViewType = this->GetActiveViewType();
	olViewName = this->GetActiveViewName();


	// don't evaluate last day, if 00:00:00 o'clock		
	if (olLoadEnd.GetHour() == 0 && olLoadEnd.GetMinute() == 0 && olLoadEnd.GetSecond() == 0)
	{
		olLoadEnd -= CTimeSpan(0,0,0,1);
	}

	// check on MSR
	if (!pomViewer->GetShiftRoster())
	{
		
	}

	ofstream of;
	of.open(ropExcelFileName, ios::out);

	//
	// writing header captions into file
	//
	of << setw(0) << "View/Date/Time of Creation" << setw(0) << ropDelimiter << ropDelimiter
	   << setw(0) << "Report Name" << setw(0) << ropDelimiter << ropDelimiter
	   << setw(0) << "Program" << setw(0)
	   << endl;

	of << setw(0) << olViewName << CTime::GetCurrentTime().Format("/%d.%m.%Y/%H:%M") << setw(0) << ropDelimiter << ropDelimiter
	   << setw(0) << olViewType << setw(0) << ropDelimiter << ropDelimiter
	   << setw(0) << "Coverage" << setw(0) 
	   << endl
	   << endl;

	of << setw(0) << "Period requested: " << setw(0) << olLoadStart.Format("%d-%m-%Y") << setw(0)
	   << setw(0) << " to " << setw(0) << olLoadEnd.Format("%d-%m-%Y") << setw(0)
	   << ropDelimiter << ropDelimiter << ropDelimiter << ropDelimiter
	   << setw(0) << "UserID : " << setw(0) << pcgUser << setw(0)
	   << endl
	   << endl;


	of << setw(0) << "Shift" << setw(0) << ropDelimiter;

	for (CTime olDay = olLoadStart; olDay <= olLoadEnd; olDay += CTimeSpan(1,0,0,0))
	{
		of << setw(0) << olDay.Format("%a %d.%m") << setw(0) << ropDelimiter;
	}

	of << endl
	   << endl;


	//	get currently used functions
	CStringArray olPfcList;
	CStringArray olPfcUrnoList;
	pomViewer->AllDistinctSdtFunctions(olPfcList);

	//	calculating statistics
	CMapStringToPtr olSdtBsdcMap;

	pomViewer->GetSdtsBetweenTimesOrderedByBsdc(olLoadStart,olLoadEnd,olSdtBsdcMap);	

	for (POSITION pos = olSdtBsdcMap.GetStartPosition(); pos != NULL;)
	{
		CString olBsdc;
		CCSPtrArray<SDTDATA> *polArray = NULL;
		olSdtBsdcMap.GetNextAssoc(pos,olBsdc,(void *&)polArray);								

		polArray->Sort(OrderByEsbg);
	}
	
	CTime olCurrent = CTime::GetCurrentTime();

	// writing statistics
	CCSPtrArray<BSDDATA> olBasicShifts;
	ogBsdData.GetAllBsdsOrderedByTime(olBasicShifts);

	for (int i = 0; i < olBasicShifts.GetSize(); i++)
	{
		BSDDATA *prlBsd = &olBasicShifts[i];
		if (prlBsd == NULL)
			continue;

		CCSPtrArray<SDTDATA> *polArray = NULL;
		if (olSdtBsdcMap.Lookup(prlBsd->Bsdc,(void *&)polArray) == FALSE)
			continue;

		for (int ilPfc = 0; ilPfc < olPfcList.GetSize(); ilPfc++)
		{
			int ilCount = 0;
			for (int j = 0; j < polArray->GetSize(); j++)
			{
				SDTDATA *prlSdt = &polArray->GetAt(j);
				if (strcmp(prlSdt->Fctc,olPfcList[ilPfc]) == 0)
				{
					++ilCount;
				}
			}

			if (ilCount == 0)
				continue;

			CTime olFrom = HourStringToDate(CString(prlBsd->Esbg),olCurrent);
			CTime olTo   = HourStringToDate(CString(prlBsd->Lsen),olCurrent);

			of << setw(0) << prlBsd->Bsdc	<< "(" << olPfcList[ilPfc] << " " << olFrom.Format("%H:%M") << "-" << olTo.Format("%H:%M") << ")" << setw(0) << ropDelimiter;

			for (olDay = olLoadStart; olDay <= olLoadEnd; olDay += CTimeSpan(1,0,0,0))
			{
				// write MSD count for the specific day
				int ilCount = 0;
				for (int j = 0; j < polArray->GetSize(); j++)
				{
					SDTDATA *prlSdt = &polArray->GetAt(j);
					if (prlSdt->Esbg.GetDay() == olDay.GetDay() && prlSdt->Esbg.GetMonth() == olDay.GetMonth() && strcmp(prlSdt->Fctc,olPfcList[ilPfc]) == 0)
					{
						++ilCount;
					}
				}

				of << setw(0) << ilCount <<  setw(0) << ropDelimiter;
			}

			of << endl;
		}

	}

	of.close();

	// cleanup
	for (pos = olSdtBsdcMap.GetStartPosition(); pos != NULL;)
	{
		CString olBsdc;
		CCSPtrArray<SDTDATA> *polArray = NULL;
		olSdtBsdcMap.GetNextAssoc(pos,olBsdc,(void *&)polArray);
		polArray->RemoveAll();						
		delete polArray;
	}

	return true;

}


CString CoverageChart::GetActiveViewName()
{
	CString olViewName;
	if (pomViewer->GetShiftRoster())
	{
		if (pomViewer->IsRealRoster())
		{
			int ilIndex = this->omComboBox.GetCurSel();
			if (ilIndex == CB_ERR)
				return olViewName;

			this->omComboBox.GetLBText(ilIndex,olViewName);
			
		}
		else
		{
			olViewName = pomViewer->CurrentShiftRosterName();
		}
	}
	else
	{
		olViewName = pomViewer->GetCurrentDemandName(SHIFT_DEMAND);
	}

	return olViewName;

}


CString CoverageChart::GetActiveViewType()
{
	CString olViewType;
	if (pomViewer->GetShiftRoster())
	{
		if (pomViewer->IsRealRoster())
		{
			olViewType = "Dutyroster";
			
		}
		else
		{
			olViewType = "Shiftroster";
		}
	}
	else
	{
		olViewType = "Mastershiftrequirement";
	}

	return olViewType;

}