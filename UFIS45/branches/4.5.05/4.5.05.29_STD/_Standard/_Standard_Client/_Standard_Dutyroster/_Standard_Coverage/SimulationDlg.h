#if !defined(AFX_SIMULATIONDLG_H__E8A62C71_C18A_11D2_9955_0000B43C16D8__INCLUDED_)
#define AFX_SIMULATIONDLG_H__E8A62C71_C18A_11D2_9955_0000B43C16D8__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// SimulationDlg.h : header file
//

#include <CoverageDiaViewer.h>
#include <CCSEdit.h>
/////////////////////////////////////////////////////////////////////////////
// SimulationDlg dialog

class SimulationDlg : public CDialog
{
// Construction
public:
	SimulationDlg(CWnd* pParent = NULL,CoverageDiagramViewer *popViewer = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(SimulationDlg)
	enum { IDD = IDD_SIMULATIONDLG };
	CCSEdit	m_To;
	CCSEdit	m_From;
	CSpinButtonCtrl	m_SpinOffset;
	CEdit	m_Demand;
	CSpinButtonCtrl	m_SpinDemand;
	CButton m_Abs;
	CButton m_Percent;
	CButton m_Constant;
	CButton m_Mins;
	CButton m_Hours;
	CEdit	m_Offset;
	CString	m_DemandVal;
	//}}AFX_DATA

	CWnd *pomParent;
	CoverageDiagramViewer *pomViewer;
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(SimulationDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(SimulationDlg)
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	afx_msg void OnDeltaposSpinDemand(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnKillfocusDemand();
	afx_msg void OnAbs();
	afx_msg void OnPercent();
	afx_msg void OnConstant();
	afx_msg void OnDeltaposSpinOffset(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnHours();
	afx_msg void OnMins();
	afx_msg void OnKillfocusOffset();
	afx_msg void OnStop();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SIMULATIONDLG_H__E8A62C71_C18A_11D2_9955_0000B43C16D8__INCLUDED_)
