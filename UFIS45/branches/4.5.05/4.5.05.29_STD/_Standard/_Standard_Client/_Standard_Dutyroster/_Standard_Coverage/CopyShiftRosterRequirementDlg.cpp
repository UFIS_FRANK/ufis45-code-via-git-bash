// CopyShiftRosterRequirementDlg.cpp : implementation file
//

#include <stdafx.h>
#include <resource.h>
#include <CopyShiftRosterRequirementDlg.h>
#include <BasicData.h>
#include <CedaPfcData.h>
#include <CedaSdgData.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CopyShiftRosterRequirementDlg dialog


CopyShiftRosterRequirementDlg::CopyShiftRosterRequirementDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CopyShiftRosterRequirementDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CopyShiftRosterRequirementDlg)
	m_CountOfCopies = 1;
	//}}AFX_DATA_INIT
}


void CopyShiftRosterRequirementDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CopyShiftRosterRequirementDlg)
	DDX_Control(pDX, IDC_SPIN_COPIES, m_SpinCopies);
	DDX_Control(pDX, IDC_COMBO_WITH, m_ComboWith);
	DDX_Control(pDX, IDC_COMBO_REPLACE, m_ComboReplace);
	DDX_Text(pDX, IDC_EDIT_COPIES, m_CountOfCopies);
	DDV_MinMaxUInt(pDX, m_CountOfCopies, 1, 9999);
	DDX_Control(pDX, IDC_EDIT_NAME, m_EditName);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CopyShiftRosterRequirementDlg, CDialog)
	//{{AFX_MSG_MAP(CopyShiftRosterRequirementDlg)
	ON_CBN_SELCHANGE(IDC_COMBO_REPLACE, OnSelchangeComboReplace)
	ON_CBN_SELCHANGE(IDC_COMBO_WITH, OnSelchangeComboWith)
	ON_EN_UPDATE(IDC_EDIT_NAME, OnUpdateEditName)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CopyShiftRosterRequirementDlg message handlers

BOOL CopyShiftRosterRequirementDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	SetWindowText(LoadStg(IDS_STRING1896));

	CWnd *polWnd = GetDlgItem(IDC_STATIC_NAME);
	if (polWnd)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING1897));
	}


	polWnd = GetDlgItem(IDC_STATIC_REPLACE);
	if (polWnd)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING1898));
	}

	polWnd = GetDlgItem(IDC_STATIC_WITH);
	if (polWnd)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING1899));
	}

	polWnd = GetDlgItem(IDC_STATIC_COPIES);
	if (polWnd)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING1900));
	}


	m_EditName.SetTextLimit(1,32);
	m_EditName.SetBKColor(LTYELLOW);

	FillComboBoxes();

	m_SpinCopies.SetBuddy(GetDlgItem(IDC_EDIT_COPIES));
	m_SpinCopies.SetPos(1);
	m_SpinCopies.SetRange(1,9999);

	polWnd = this->GetDlgItem(IDOK);
	if (polWnd)
	{
		polWnd->EnableWindow(FALSE);
	}
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CopyShiftRosterRequirementDlg::FillComboBoxes()
{
	this->m_ComboReplace.ResetContent();
	this->m_ComboWith.ResetContent();

	int ind = this->m_ComboReplace.AddString(LoadStg(IDS_STRING1901));	// --ALL--
	this->m_ComboReplace.SetItemData(ind,-1);
	
	ind = this->m_ComboReplace.AddString(LoadStg(IDS_STRING1902));		// --NO REPLACE--
	this->m_ComboReplace.SetItemData(ind,-2);

/***
	ind = this->m_ComboWith.AddString(LoadStg(IDS_STRING1901));			// --ALL--
	this->m_ComboWith.SetItemData(ind,-1);
****/
	
	ind = this->m_ComboWith.AddString(LoadStg(IDS_STRING1902));			// --NO REPLACE--
	this->m_ComboWith.SetItemData(ind,-2);

	CString olLine;
	for (int i = 0; i < ogPfcData.omData.GetSize(); i++)
	{
		olLine.Format("%-5s - %-40s", ogPfcData.omData[i].Fctc, ogPfcData.omData[i].Fctn);
		ind = m_ComboReplace.AddString(olLine);
		m_ComboReplace.SetItemData(ind,i);

		ind = m_ComboWith.AddString(olLine);
		m_ComboWith.SetItemData(ind,i);
		
	}

	this->m_ComboReplace.SetCurSel(this->m_ComboReplace.FindStringExact(-1,LoadStg(IDS_STRING1902)));
	this->m_ComboWith.SetCurSel(this->m_ComboWith.FindStringExact(-1,LoadStg(IDS_STRING1902)));

}

void CopyShiftRosterRequirementDlg::OnSelchangeComboReplace() 
{
	// TODO: Add your control notification handler code here
	int ind = this->m_ComboReplace.GetCurSel();
	if (ind == CB_ERR)
		return;

	
	
}

void CopyShiftRosterRequirementDlg::OnSelchangeComboWith() 
{
	// TODO: Add your control notification handler code here
	int ind = this->m_ComboWith.GetCurSel();
	if (ind == CB_ERR)
		return;
	
}

void CopyShiftRosterRequirementDlg::OnUpdateEditName() 
{
	// TODO: If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialog::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.
	
	// TODO: Add your control notification handler code here
	CString olText;
	this->m_EditName.GetWindowText(olText);		
	if (olText.GetLength() == 0)
	{
		CWnd *pWnd = this->GetDlgItem(IDOK);
		if (pWnd)
		{
			pWnd->EnableWindow(FALSE);
		}
	}
	else
	{
		SDGDATA *polSdg = ogSdgData.GetSdgByDnam(olText);
		CWnd *pWnd = this->GetDlgItem(IDOK);
		if (polSdg)
		{
			if (pWnd)
			{
				pWnd->EnableWindow(FALSE);
			}
		}
		else
		{
			if (pWnd)
			{
				pWnd->EnableWindow(TRUE);
			}
		}
	}
}

CString CopyShiftRosterRequirementDlg::NewName()
{
	return this->omNewName;
}

CString	CopyShiftRosterRequirementDlg::ReplaceFunction()
{
	return this->omReplaceFunction;
}

CString	CopyShiftRosterRequirementDlg::WithFunction()
{
	return this->omWithFunction;
}

UINT	CopyShiftRosterRequirementDlg::CountOfCopies()
{
	return this->m_CountOfCopies;
}


void CopyShiftRosterRequirementDlg::OnOK() 
{
	// TODO: Add extra validation here
	if (!UpdateData(TRUE))
		return;

	this->m_EditName.GetWindowText(omNewName);

	int ind = this->m_ComboReplace.GetCurSel();
	if (ind == CB_ERR)
		omReplaceFunction = "";
	else
	{
		switch(this->m_ComboReplace.GetItemData(ind))
		{
		case -1:
			omReplaceFunction = LoadStg(IDS_STRING1901);
		break;
		case -2:
			omReplaceFunction = LoadStg(IDS_STRING1902);
		break;
		default:
			omReplaceFunction = ogPfcData.omData[this->m_ComboReplace.GetItemData(ind)].Fctc;
		}
	}

	ind = this->m_ComboWith.GetCurSel();
	if (ind == CB_ERR)
		omWithFunction = "";
	else
	{
		switch(this->m_ComboWith.GetItemData(ind))
		{
		case -1:
			omWithFunction = LoadStg(IDS_STRING1901);
		break;
		case -2:
			omWithFunction = LoadStg(IDS_STRING1902);
		break;
		default:
			omWithFunction = ogPfcData.omData[this->m_ComboWith.GetItemData(ind)].Fctc;
		}
	}
	
	CDialog::OnOK();
}
