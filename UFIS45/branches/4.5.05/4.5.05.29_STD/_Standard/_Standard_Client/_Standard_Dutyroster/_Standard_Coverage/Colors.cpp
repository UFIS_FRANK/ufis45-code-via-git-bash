// Colors.cpp : implementation file
//

#include <stdafx.h>
#include <coverage.h>
#include <Colors.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Colors

IMPLEMENT_DYNCREATE(Colors, CCmdTarget)

Colors::Colors()
{
}

Colors::~Colors()
{
}


BEGIN_MESSAGE_MAP(Colors, CCmdTarget)
	//{{AFX_MSG_MAP(Colors)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Colors message handlers
