// AutoCoverage.h: interface for the CAutoCoverage class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_AUTOCOVERAGE_H__62C35A94_CCFC_11D4_B227_204C4F4F5020__INCLUDED_)
#define AFX_AUTOCOVERAGE_H__62C35A94_CCFC_11D4_B227_204C4F4F5020__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
#include <InputData.h>
#include <BaseShifts.h>
#include <BaseShiftsGroups.h>

typedef struct EvaluatedShift
{
	BASESHIFT*	pomShift;
	double		dlDemDiff;
	int			ilOptStart;
	int			ilOptEnd;
	int			ilOptBreakStart;
	int			ilOptBreakEnd;
	int			ilCoveredMinWorkingHours;

	EvaluatedShift()
	{
		memset(this,0,sizeof(EvaluatedShift));
	}

}	EVALUATEDSHIFT;

typedef struct EvaluatedShiftPair
{
	EVALUATEDSHIFT				omEvaluated1;
	CCSPtrArray<EVALUATEDSHIFT>	omEvaluated2;

	EvaluatedShiftPair()
	{
				
	}

	~EvaluatedShiftPair()
	{
		omEvaluated2.DeleteAll();
	}

}	EVALUATEDSHIFTPAIR;


class CAutoCoverage  
{
public:
	void InitValues(CUIntArray &ropDemData,CUIntArray &ropShiftCovData,
						CCSPtrArray <BASESHIFT> &ropBaseShifts,
						CBaseShiftsGroups *pmpShiftGroups,CString opFctc);

	
	bool CalcCoverage(bool bpForward,bool bpToggle,bool bpOverCover,bool UnderCover,
					  long lpShiftDeviation,double dpFactor,int ilAdaption,int ipMinShiftCoverage,bool bpUseBaseCoverage);

	
	void GetAutoCovShifts(CStringArray &ropAutoCovShifts);

	CAutoCoverage();
	virtual ~CAutoCoverage();

public:	// Attributes
	void	IgnoreBreaks(bool bpIgnoreBreaks);
	bool	IgnoreBreaks();

private:
	bool CalcCoverage(CInputData *pmpInDemData,CInputData *pmpOutShiftCovData,
						CBaseShifts *pmpShiftData,CBaseShiftsGroups *pmpShiftGroups,
						bool bpForward,bool bpToggle,bool bpOverCover,bool UnderCover,
						long lpShiftDeviation,double dpFactor,int ipMinShiftCoverage);

	bool CalcCoverage2(CInputData *pmpInDemData,CInputData *pmpOutShiftCovData,
						CBaseShifts *pmpShiftData,CBaseShiftsGroups *pmpShiftGroups,
						bool bpForward,bool bpToggle,bool bpOverCover,bool bpUnderCover,
						long lpShiftDeviation,double dpFactor,int ipMinShiftCoverage);

	bool CalcBaseCoverage(CInputData *pmpInDemData,CInputData *pmpOutShiftCovData,
						CBaseShifts *pmpShiftData,CBaseShiftsGroups *pmpShiftGroups,
						bool bpForward,bool bpToggle,bool bpOverCover,bool UnderCover,
						long lpShiftDeviation,double dpFactor,int ipMinShiftCoverage);

	bool CalcBaseCoverage2(CInputData *pmpInDemData,CInputData *pmpOutShiftCovData,
						CBaseShifts *pmpShiftData,CBaseShiftsGroups *pmpShiftGroups,
						bool bpForward,bool bpToggle,bool bpOverCover,bool bpUnderCover,
						long lpShiftDeviation,double dpFactor,int ipMinShiftCoverage);

	bool CalcBaseCoverage3(CInputData *pmpInDemData,CInputData *pmpOutShiftCovData,
						CBaseShifts *pmpShiftData,CBaseShiftsGroups *pmpShiftGroups,
						bool bpForward,bool bpToggle,bool bpOverCover,bool bpUnderCover,
						long lpShiftDeviation,double dpFactor,int ipMinShiftCoverage);

	int CalcOptBreakOffSet(int ipBPStart, int ipBPEnd, int ipBDuration,int dpFactor,
							  CInputData *pmpInDemData,CInputData *pmpOutShiftCovData);

	int	SelectBaseShifts(CBaseShifts *pmpShiftData,bool bpForward,long lpShiftDeviation,int ipCalcTime);
	int	SelectBaseShifts2(CBaseShifts *pmpShiftData,bool bpForward,long lpShiftDeviation,int ipCalcTime);
	int	SelectBaseShifts3(CCSPtrArray<BASESHIFT>& ropShiftData,CMapStringToPtr& ropShiftMap,int ipCalcTime,CCSPtrArray<BASESHIFT>& olSelectedBaseShifts);

	int CalcDemDiff (CInputData *pmpInDemData,CInputData *pmpOutShiftCovData,bool bpUnderCover,double dpFactor,BASESHIFT *prpShift,int ipShiftOffSet,int& ipStartPos,int& ipEndPos,int ipBreakStart,int ipBreakOffSet,int ipBreakEnd,int& ipCoveredWorkingHours,int& ipEvaluationFactor,int ipCalcTime);
	double CalcDemDiff3(CInputData *pmpInDemData,CInputData *pmpOutShiftCovData,double dpFactor,BASESHIFT *prpShift,int ipShiftOffSet,int& ipStartPos,int& ipEndPos,int ipBreakStart,int ipBreakOffSet,int ipBreakEnd,int& ipCoveredWorkingHours,double& dpEvaluationFactor,int ipCalcTime);

	bool EvaluateBaseShift(CInputData *pmpInDemData,CInputData *pmpOutShiftCovData,CBaseShiftsGroups *pmpShiftGroups,BASESHIFT *prlShift,int ilShiftOffSet,int ilTime,double dpFactor,EVALUATEDSHIFT& ropEvaluatedShift);

private:
	CBaseShiftsGroups	*pomShiftGroups;
	CBaseShifts			omShiftData;
	CInputData			omInDemData;
	CInputData			omOutShiftCovData;
	CStringArray		omOptShifts;
	CStringArray		omActShifts;
	double				dmBestVal;
	bool				bmInit;
	bool				bmEvaluateSecondShift;
	bool				bmEvaluateBreaks;
	int					imEvaluateBreaks;
};

#endif // !defined(AFX_AUTOCOVERAGE_H__62C35A94_CCFC_11D4_B227_204C4F4F5020__INCLUDED_)
