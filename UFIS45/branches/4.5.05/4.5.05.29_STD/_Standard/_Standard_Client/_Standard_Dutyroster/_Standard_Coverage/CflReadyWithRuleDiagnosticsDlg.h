#if !defined(AFX_CFLREADYWITHRULEDIAGNOSTICSDLG_H__8E6B92B5_89B8_11D7_8016_00010215BFDE__INCLUDED_)
#define AFX_CFLREADYWITHRULEDIAGNOSTICSDLG_H__8E6B92B5_89B8_11D7_8016_00010215BFDE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// CflReadyWithRuleDiagnosticsDlg.h : header file
//
#include <CflReadyDlg.h>
#include <AfxINet.h>

/////////////////////////////////////////////////////////////////////////////
// CCflReadyWithRuleDiagnosticsDlg dialog

class CCflReadyWithRuleDiagnosticsDlg : public CCflReadyDlg
{
// Construction
public:
	CCflReadyWithRuleDiagnosticsDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CCflReadyWithRuleDiagnosticsDlg)
	enum { IDD = IDD_CFLREADY_RULE_DIAGNOSTICS_DLG };
	CEdit	m_RuleResults;
	CListBox	m_RuleFiles;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCflReadyWithRuleDiagnosticsDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CCflReadyWithRuleDiagnosticsDlg)
	afx_msg void OnDblclkRuleFiles();
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	void ScanServer();
	void FillFiles(const CString& path); 
	void LoadRuleFile(const CString& procName);
	void ConvertUnixFile(const CString& cFile,const CString& cOutFile);
	int	 ConvertLFToCRLF(CString& theString);
private:
	CFtpConnection *ftp_connection;
	CInternetSession inet_session;

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CFLREADYWITHRULEDIAGNOSTICSDLG_H__8E6B92B5_89B8_11D7_8016_00010215BFDE__INCLUDED_)
