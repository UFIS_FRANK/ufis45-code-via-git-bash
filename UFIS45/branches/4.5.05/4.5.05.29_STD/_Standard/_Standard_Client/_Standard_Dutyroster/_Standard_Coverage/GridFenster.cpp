// DialogGrid.cpp : implementation file
//

#include <stdafx.h>
#include <Coverage.h>
#include <GridFenster.h>
#include <BasicData.h>

#include <CedaBasicData.h>
#include <CCSGlobl.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

//---------------------------------------------------------------------------------------------------------------------
//					defines, global functions, etc.
//---------------------------------------------------------------------------------------------------------------------

#define MIN_COL_WIDTH		20		// minimum column width for all grid columns


int StringComp(const RecordSet** ppRecord1, const RecordSet** ppRecord2);



//---------------------------------------------------------------------------------------------------------------------
//					construction / destruction
//---------------------------------------------------------------------------------------------------------------------
CGridFenster::CGridFenster()
{
	pomParent = NULL;
	bmSortNumerical = false;
	bmAllowMultiSelect = true;
	bmSortAscend = true;
	bmIsSorting = true;
	bmDragDropRow = false;
	imSortKey = 1;
	imCurrentRow = -1;
	pimSortIndex = NULL;
	pimDependendCell = NULL;
	pbmDoubleSort = NULL;
	imTopSelectedRow = -1;

}

CGridFenster::CGridFenster(CWnd *pParent)
{
	pomParent = pParent;
	bmSortNumerical = false;
	bmAllowMultiSelect = true;
	bmSortAscend = true;
	bmIsSorting = true;
	bmDragDropRow = false;
	imSortKey = 1;
	imCurrentRow = -1;
	pimSortIndex = NULL;
	pimDependendCell = NULL;
	pbmDoubleSort = NULL;
	imTopSelectedRow = -1;

}
//---------------------------------------------------------------------------------------------------------------------

CGridFenster::~CGridFenster()
{
	if(pimSortIndex != NULL)
		delete pimSortIndex ;
	if(pimDependendCell != NULL)
		delete pimDependendCell ;

	if(pbmDoubleSort != NULL)
	{
		delete pbmDoubleSort;
	}

}

//---------------------------------------------------------------------------------------------------------------------
//					message map
//---------------------------------------------------------------------------------------------------------------------
BEGIN_MESSAGE_MAP(CGridFenster, CGXGridWnd)
	//{{AFX_MSG_MAP(CGridFenster)
	ON_WM_LBUTTONUP()
	ON_WM_LBUTTONDOWN()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


//---------------------------------------------------------------------------------------------------------------------
//					message handlers
//---------------------------------------------------------------------------------------------------------------------
void CGridFenster::OnLButtonDown(UINT nFlags, CPoint point) 
{
	//--
	// checks sorting
	//-- 


    ROWCOL  Row;
	ROWCOL  Col;
	CRowColArray olRows;

	//--- Betroffene Zelle ermitten
    HitTest(point, &Row,  &Col, NULL);

	//--- Check f�r StingRay Fehler
	if(Col > GetColCount())
		return;   


	CGXGridWnd ::OnLButtonDown(nFlags, point);

	if(Col <= 0)
		return;   

	bool blSelRange = false;
	
	if (bmAllowMultiSelect && (nFlags & MK_LBUTTON) && (nFlags & MK_SHIFT))
	{
		int ilSelCount = (int)GetSelectedRows( olRows);
		int ilLastSel = -1;
		if(ilSelCount > 0)
		{
			ilLastSel = (int) olRows[ilSelCount-1];
			SelectRange(CGXRange(Row, 0, imTopSelectedRow, GetColCount()), TRUE);
			blSelRange = true;
		}
	}
/***/
	if(Row > 0 && !blSelRange)
	{
		int ilSelCount = (int)GetSelectedRows( olRows);
		bool blFound = false;
		ROWCOL ilSel;
		for(int ilLc = 0; ilLc < ilSelCount && !blFound;ilLc++)
		{
			ilSel = olRows[ilLc];
			if(ilSel == Row)
			{
				blFound = true;
			}
		}
		if(!blFound)
		{
			SelectRange(CGXRange(Row, 1, Row, GetColCount()), TRUE);
			imTopSelectedRow = Row;
		}
		else
		{
		//	SelectRange(CGXRange(Row, 1, Row, GetColCount()), TRUE);
		}

	}
/**/
}

void CGridFenster::OnLButtonUp(UINT nFlags, CPoint point) 
{
	//--
	// checks sorting
	//-- 


	CGXGridWnd ::OnLButtonUp(nFlags, point);

    ROWCOL  Row;
	ROWCOL  Col;

	//--- Betroffene Zelle ermitten
    HitTest(point, &Row,  &Col, NULL);

	//--- Check f�r StingRay Fehler
	if(Col > GetColCount())
		return;

	//--- Sortieren je nachdem welcher Header gedr�ckt wurde
	if(Row == 0 && Col != 0 && bmIsSorting)
	{
		CGXSortInfoArray  sortInfo;
		CRowColArray olRows;

		bool blIsDoubleSort = false;
		int ilSortSize = 1;
		sortInfo .SetSize(ilSortSize);
		if(pbmDoubleSort != NULL)
		{
			if(pbmDoubleSort[Col])
			{
				blIsDoubleSort = true;
				ilSortSize = 2;
				sortInfo .SetSize(ilSortSize);
				sortInfo[0].sortOrder = CGXSortInfo::descending;
				sortInfo[0].nRC = Col;
				sortInfo[0].sortType;
				if(pimSortIndex != NULL)
				{
					sortInfo[0].nRC = pimSortIndex[Col];                       
				}
				else
				{
					sortInfo[0].nRC = Col;                       
				}

			}
		}
		
		int ilTopRow = (int)  GetTopRow();


		// switch between sorting in ascending / descending order with each click
		if (bmSortAscend == true)
		{
			sortInfo[ilSortSize -1].sortOrder = CGXSortInfo::descending;
			bmSortAscend = false;
		}
		else
		{
			sortInfo[ilSortSize -1].sortOrder = CGXSortInfo::ascending;
			bmSortAscend = true;
		}
		if(pimSortIndex != NULL && !blIsDoubleSort)
		{
			sortInfo[ilSortSize -1].nRC = pimSortIndex[Col];                       
		}
		else
		{
			sortInfo[ilSortSize -1].nRC = Col;                       
		}
		int ildummy = CGXSortInfo::autodetect;
		sortInfo[ilSortSize -1].sortType = CGXSortInfo::autodetect;  
		int ilRowCount = (int)GetRowCount() ;
	
		LockUpdate(TRUE);

		// Sort 
		SortRows(CGXRange().SetTable(), sortInfo);

		LockUpdate(FALSE);
		Redraw();
		//--- Merke welche Spalte f�r sorting verwendet wurde
		imSortKey  = Col;

		//--- Check ob numerisch sortiert wurde 
		CGXStyle  il_Cell_Style;
		GetStyleRowCol(1, Col, il_Cell_Style);


		if(il_Cell_Style.GetValueType() ==  GX_VT_NUMERIC )
		  bmSortNumerical = true;
		else
		  bmSortNumerical = false;

		SetTopRow(ilTopRow, GX_UPDATENOW ,TRUE);

	}
//	SelectRange(CGXRange(Row, 1, Row, GetColCount()), FALSE);
}
//---------------------------------------------------------------------------------------------------------------------

BOOL  CGridFenster::OnSelDragDrop (ROWCOL nStartRow, ROWCOL nStartCol, ROWCOL nDragRow, ROWCOL nDragCol)
{
	//--
	// check dragging of rows
	//--

	 //---  Verschieben der Zeilen nur wenn numerisch sortiert
	 if( bmDragDropRow)
	 {
	      CGXGridCore::OnSelDragDrop (nStartRow, nStartCol, nDragRow, nDragCol);

		  //--- Neu durchnumerieren 
		  /*
		  SetReadOnly(false);

	        int  il_ilSize = GetRowCount();

	        for(int  i = 1; i <= il_ilSize; i++) 
                 SetValueRange(CGXRange( i , 1), (WORD) i);  

		    Redraw();

		  SetReadOnly(true);
*/
		  return true;
     }
	 else   
	 {
		 return false;
	 }

}
//---------------------------------------------------------------------------------------------------------------------

BOOL CGridFenster::OnTrackColWidth(ROWCOL nCol)
{
	//------------------------------------
	// overwritten virtual of CGXGridCore
	//------------------------------------

	if (IsColHidden(nCol) == TRUE)
		return FALSE;
	else
		return TRUE;

}
//---------------------------------------------------------------------------------------------------------------------

int CGridFenster::GetColWidth(ROWCOL nCol)
{
	//----------------------------------
	// test if cols can be hidden or not
	//----------------------------------

	int nRet = CGXGridCore::GetColWidth(nCol);
	
	for (int i = 0; i < omVisibleCols.GetSize(); i++)
	{
		int ilColNo = (int) omVisibleCols.GetAt(i);
		if ((int)nCol == ilColNo)
		{
			if (nRet < MIN_COL_WIDTH)
			{
				nRet = MIN_COL_WIDTH;
			}
			else
			{
				nRet = 0;
			}
		}
	}

	//-- columns 6 to 8 stay hidden (width = 0)
	/*if (nCol <= 5)
	{
		//-- first 5 columns' width will be set to at least MIN_COL_WIDTH
		if (nRet < MIN_COL_WIDTH)
		{
			nRet = MIN_COL_WIDTH;
		}
	}
	else
	{
		nRet = 0;
	}*/

	return nRet;
}
//------------------------------------------------------------------------------------------------------------------------

void  CGridFenster::OnClickedButtonRowCol(ROWCOL nRow, ROWCOL nCol) 
{
	//--------------------------------------
	// Click onto a Pushbutton inside the grid
	// ==> Message to parent dialog
	//--------------------------------------


	CELLPOS rlPos;
	rlPos.Col = nCol;
	rlPos.Row = nRow;

	CDialog*  polDlg = NULL;
	
	if(pomParent == NULL)
	{
		polDlg = (CDialog*) GXGetParentWnd(this, RUNTIME_CLASS(CDialog), TRUE);
	}
	else 
	{
		polDlg = (CDialog*) pomParent;
	}

	pomParent->SendMessage(GRID_MESSAGE_BUTTONCLICK, (WPARAM)this, (LPARAM)&rlPos);
}
//------------------------------------------------------------------------------------------------------------------------

BOOL  CGridFenster::OnLButtonClickedRowCol(ROWCOL nRow, ROWCOL nCol, UINT nFlags, CPoint pt) 
{
	//---
	// Click into a cell of the grid
	// ==> Message to parent dialog
	//---

	CELLPOS rlPos;
	rlPos.Col = nCol;
	rlPos.Row = nRow;

	CDialog*  polDlg = NULL;

	if(pomParent == NULL)
	{
		polDlg = (CDialog*) GXGetParentWnd(this, RUNTIME_CLASS(CDialog), TRUE);
	}
	else 
	{
		polDlg = (CDialog*) pomParent;
	}
		
	polDlg->SendMessage(GRID_MESSAGE_CELLCLICK, (WPARAM)this, (LPARAM)&rlPos);

	return TRUE;
}
//------------------------------------------------------------------------------------------------------------------------

BOOL CGridFenster::OnLButtonDblClkRowCol(ROWCOL nRow, ROWCOL nCol, UINT nFlags, CPoint pt)
{
	//---
	// DoubleClick into a cell of the grid
	// ==> Message to parent dialog
	//---

	CELLPOS rlPos;
	rlPos.Col = nCol;
	rlPos.Row = nRow;

	CDialog*  polDlg = NULL;

	if(pomParent == NULL)
	{
		polDlg = (CDialog*) GXGetParentWnd(this, RUNTIME_CLASS(CDialog), TRUE);
	}
	else 
	{
		polDlg = (CDialog*) pomParent;
	}
		
	polDlg->SendMessage(GRID_MESSAGE_DOUBLECLICK, (WPARAM)this, (LPARAM)&rlPos);

	return TRUE;
}
//------------------------------------------------------------------------------------------------------------------------

BOOL  CGridFenster::OnEndEditing(ROWCOL nRow, ROWCOL nCol)
{
	//  Setzte Flag wenn Editing beendet 

      /*CString  olStr; 
	  GetCurrentCellControl()->GetCurrentText(olStr);

	  if(!olStr.IsEmpty())
	  {
	    int ilSize = min(GetColCount(), 6);

	    //--- Alle Eintr�ge l�schen
		for(int i = 2; i < ilSize; i++)
		{
			if (nRow > 0)
				SetValueRange(CGXRange(nRow, i), "" );
		}

	    //--- Alten Text wieder einsetzen
             SetValueRange(CGXRange(nRow, nCol), olStr );
	  }*/

	CELLPOS rlPos;
	rlPos.Col = nCol;
	rlPos.Row = nRow;

	CDialog*  polDlg = NULL;

	if(pomParent == NULL)
	{
		polDlg = (CDialog*) GXGetParentWnd(this, RUNTIME_CLASS(CDialog), TRUE);
	}
	else 
	{
		polDlg = (CDialog*) pomParent;
	}
		
	polDlg->SendMessage(GRID_MESSAGE_ENDEDITING, (WPARAM)this, (LPARAM)&rlPos);

	return TRUE;
}



//---------------------------------------------------------------------------------------------------------------------
//					set methods
//---------------------------------------------------------------------------------------------------------------------
void CGridFenster::SetNumSort( bool b )
{
	bmSortNumerical = b;
}
//---------------------------------------------------------------------------------------------------------------------

void CGridFenster::SetColsAlwaysVisible(CPtrArray opCols)
{
	omVisibleCols.RemoveAll();
	for (int i = 0; i < opCols.GetSize(); i++)
	{
		int	ilColNo = (int) opCols.GetAt(i);
		omVisibleCols.Add((void*)&ilColNo);
	}
}
//---------------------------------------------------------------------------------------------------------------------

void CGridFenster::SetSortingEnabled(bool bpEnable)
{
	if (bpEnable == true)
	{
		bmIsSorting = true;
	}
	else
	{
		bmIsSorting = false;
	}
}

void CGridFenster::SetDragDropRowEnabled(bool bpEnable)
{
	if (bpEnable == true)
	{
		bmDragDropRow = true;
	}
	else
	{
		bmDragDropRow = false;
	}
}

BOOL CGridFenster::SetColCount(ROWCOL nCols, UINT flags)
{
	if(pimSortIndex != NULL)
	{
		delete pimSortIndex;
	}
	if(pimDependendCell != NULL)
	{
		delete pimDependendCell;
	}
	
	int ilCols = (int)nCols;
	pimSortIndex = new int[ilCols +1];
	pimDependendCell = new int[ilCols +1];
	
	for(int illc = 0; illc <= ilCols; illc++)
	{
		pimSortIndex[illc] = illc;
	}

	for( illc = 0; illc <= ilCols; illc++)
	{
		pimDependendCell[illc] = illc;
	}
	if(pbmDoubleSort != NULL)
	{
		delete pbmDoubleSort;
	}
	pbmDoubleSort = new bool[ilCols +1];
	for( illc = 0; illc <= ilCols; illc++)
	{
		pbmDoubleSort[illc] = false;
	}

	omValueTypeList = "";
	for( illc = 0; illc <= ilCols; illc++)
	{
		omValueTypeList += "_";
	}
	
	omDependendType = "";
	for( illc = 0; illc <= ilCols; illc++)
	{
		omDependendType += "_";
	}

	return CGXGridCore::SetColCount(nCols,flags);
}

void  CGridFenster::SetValueType(int ipCol,char cpType)
{
	if(ipCol > (int)GetColCount())
		return;	
	
	omValueTypeList.SetAt(ipCol,cpType);

}

void  CGridFenster::SetCondition(int ipCol1,int ipCol2,char cpCondition)
{
	if(ipCol1 > (int)GetColCount() || ipCol2 > (int)GetColCount())
		return;	

	pimDependendCell[ipCol1] = ipCol2;
	omDependendType.SetAt(ipCol1,cpCondition);
}


void  CGridFenster::SetSortQuery(int ipClickedCol,int ipSortCol)
{
	if(ipClickedCol > (int)GetColCount())
		return;	
	if(ipSortCol > (int)GetColCount())
		return;	
	pimSortIndex[ipClickedCol] = ipSortCol;

}

void  CGridFenster::SetDoubleSort(int ipClickedCol,bool bpUseDoubleSort)
{
	if(ipClickedCol > (int)GetColCount())
		return;	
	pbmDoubleSort[ipClickedCol] = bpUseDoubleSort;

}


//---------------------------------------------------------------------------------------------------------------------
//						get methods
//---------------------------------------------------------------------------------------------------------------------
bool CGridFenster::GetNumSort()
{
	return bmSortNumerical;
}
//---------------------------------------------------------------------------------------------------------------------

int CGridFenster::GetSortKey()
{
	return imSortKey;
}
//---------------------------------------------------------------------------------------------------------------------

int  CGridFenster::GetCurrentRow()
{
	return imCurrentRow;
}
//---------------------------------------------------------------------------------------------------------------------

int  CGridFenster::GetCurrentCol()
{
	return imCurrentCol;
}
//---------------------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------------

void CGridFenster::SetAllowMultiSelect(bool bpAllow )
{
	bmAllowMultiSelect = bpAllow;
}

void CGridFenster::OnChangedSelection(const CGXRange* pRange, BOOL bIsDragging, BOOL bKey)
{

	CGXGridWnd::OnChangedSelection(pRange, bIsDragging, bKey);
		
}

BOOL CGridFenster::OnGridKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags)
{
	BOOL blReturn = TRUE;
	blReturn = CGXGridWnd::OnGridKeyDown(nChar, nRepCnt, nFlags);
	return blReturn;
}


void CGridFenster::OnMovedCurrentCell(ROWCOL nRow, ROWCOL nCol)
{
	
	CELLPOS rlPos;
	rlPos.Col = nCol;
	rlPos.Row = nRow;
	
	
	CGXGridWnd::OnMovedCurrentCell(nRow, nCol);
	if(pomParent != NULL)
	{
		 pomParent->SendMessage(GRID_ACTCELLMOVED, (WPARAM)this, (LPARAM)&rlPos);;
	}
}

bool CGridFenster::UIntCheck(CString opValue)
{
	char clTmp;
	CString olValue = opValue;
	bool blReturn = false;

	olValue.TrimRight();
	int ilOffSet = olValue.GetLength();

	for(int ilLc = 0; ilLc < olValue.GetLength(); ilLc++)
	{
		blReturn = true;
		clTmp = olValue[ilLc];
		
		if(!isdigit(clTmp))
		{	
			return false;
		}
	}
	return blReturn;
}

bool CGridFenster::IntCheck(CString opValue)
{
	char clTmp;
	CString olValue = opValue;
	bool blReturn = false;

	olValue.TrimRight();
	int ilOffSet = olValue.GetLength();

	for(int ilLc = 0; ilLc < olValue.GetLength(); ilLc++)
	{
		blReturn = true;
		clTmp = olValue[ilLc];
		
		if(!((isdigit(clTmp)) || ((clTmp == '-') && (ilLc == 0))))
		{
			return false;
		}
	}
	return blReturn;
}

bool CGridFenster::ConditionCheck(ROWCOL nRow, ROWCOL nCol)
{

	CString olValue1 = GetValueRowCol(nRow, nCol);
	bool blIsOk = true;
	int ilCol = (int)nCol;

	int ilDependendCell = pimDependendCell[ilCol];
	CString olValue2 = GetValueRowCol(nRow, (ROWCOL)ilDependendCell);


	switch(omValueTypeList[ilCol])
	{
	   case 'U': switch(omDependendType[ilCol])
				 {
					case '<': blIsOk = (atoi(olValue1) < atoi(olValue2));
						break;
					case '=': blIsOk = (atoi(olValue1) == atoi(olValue2));
						break;
					case '>': blIsOk = (atoi(olValue1) > atoi(olValue2));
						break;
					case '!': blIsOk = (atoi(olValue1) != atoi(olValue2));
						break;
				 }
		         break;
	   case 'I': switch(omDependendType[ilCol])
				 {
					case '<': blIsOk = (atoi(olValue1) < atoi(olValue2));
						break;
					case '=': blIsOk = (atoi(olValue1) == atoi(olValue2));
						break;
					case '>': blIsOk = (atoi(olValue1) > atoi(olValue2));
						break;
					case '!': blIsOk = (atoi(olValue1) != atoi(olValue2));
						break;
				 }
		         break;
	   default : switch(omDependendType[ilCol])
				 {
					case '<': blIsOk = (olValue1 < olValue2);
						break;
					case '=': blIsOk = (olValue1 == olValue2);
						break;
					case '>': blIsOk = (olValue1 > olValue2);
						break;
					case '!': blIsOk = (olValue1 != olValue2);
						break;
				 }
		         break;
	}
	return blIsOk;
}

bool CGridFenster::CheckCell(ROWCOL nRow, ROWCOL nCol)
{
	CString olValue = GetValueRowCol(nRow, nCol);
	bool blIsOk = true;

	int ilCol = (int)nCol;

	switch(omValueTypeList[ilCol])
	{
	   case 'U': blIsOk = UIntCheck(olValue);
		         break;
	   case 'I': blIsOk = IntCheck(olValue);
		         break;
	   default : blIsOk = true;
		         break;
	}

	if(blIsOk)
	{
		blIsOk = ConditionCheck(nRow, nCol);
	}
	return blIsOk;
}


