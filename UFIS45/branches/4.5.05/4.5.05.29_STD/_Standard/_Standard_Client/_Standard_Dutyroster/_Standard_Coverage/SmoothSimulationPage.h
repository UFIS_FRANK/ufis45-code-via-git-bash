#if !defined(AFX_SMOOTHSIMULATIONPAGE_H__DC322378_8D89_11D6_820A_00010215BFDE__INCLUDED_)
#define AFX_SMOOTHSIMULATIONPAGE_H__DC322378_8D89_11D6_820A_00010215BFDE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SmoothSimulationPage.h : header file
//
#include <CCSEdit.h>

/////////////////////////////////////////////////////////////////////////////
// SmoothSimulationPage dialog

class SmoothSimulationPage : public CPropertyPage
{
	DECLARE_DYNCREATE(SmoothSimulationPage)

// Construction
public:
	SmoothSimulationPage();
	~SmoothSimulationPage();

	bool bmSmooth;
	void SetCaption(const char *pcpCaption);

// Dialog Data
	//{{AFX_DATA(SmoothSimulationPage)
	enum { IDD = IDD_SMOOTH_SIMULATION_PAGE };
	CSpinButtonCtrl	m_SpinMaxWidth;
	CEdit	m_MaxWidth;
	CCSEdit	m_Iterations;
	CSpinButtonCtrl	m_SpinIterations;
	CCSEdit	m_PeakWidth;
	CSpinButtonCtrl	m_SpinWidth;
	CCSEdit	m_PeakHeight;
	CSpinButtonCtrl	m_SpinHeight;
	//}}AFX_DATA


// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(SmoothSimulationPage)
	public:
	virtual void OnOK();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(SmoothSimulationPage)
	virtual BOOL OnInitDialog();
	afx_msg void OnDeltaposSpinIterations(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDeltaposSpinMaxWidth(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDeltaposSpinWidth(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDeltaposSpinHeight(NMHDR* pNMHDR, LRESULT* pResult);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	char pcmCaption[256];

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SMOOTHSIMULATIONPAGE_H__DC322378_8D89_11D6_820A_00010215BFDE__INCLUDED_)
