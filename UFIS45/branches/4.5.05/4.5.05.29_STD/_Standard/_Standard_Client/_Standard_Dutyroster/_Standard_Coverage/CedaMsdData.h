#ifndef _CMSDSD_H_
#define _CMSDSD_H_
 
//#include <afxwin.h>
#include <ccsglobl.h>
//#include "ccsobj.h"
#include <CCSPtrArray.h>
#include <CCSCedaData.h>
#include <ccsddx.h>
/////////////////////////////////////////////////////////////////////////////



struct MSDDATA {
	char 	 Bewc[7]; 	// Bewertungsfaktor.code
	char 	 Bkr1[3]; 	// Pausenlage relativ zu Schichtbeginn oder absolut
	char 	 Bsdc[8+2];	// Code
	char 	 Bsdk[14]; 	// Kurzbezeichnung
	char 	 Bsdn[42]; 	// Bezeichnung
	char 	 Bsds[5]; 	// SAP-Code
	char 	 Ctrc[7]; 	// Vertragsart.code
	char 	 Days[9]; 	// Verkehrstage
	char 	 Dnam[34]; 	// Bedarfsname
	char 	 Type[3]; 	// Flag (statisch/dynamisch)
	char 	 Usec[34]; 	// Anwender (Ersteller)
	char 	 Useu[34]; 	// Anwender (letzte �nderung)
	int 	 Sdu1/*[6]*/; 	// Regul�re Schichtdauer
	int 	 Sex1/*[6]*/; 	// M�gliche Arbeitszeitver-l�ngerung
	int 	 Ssh1/*[6]*/; 	// M�gliche Arbeitszeitverk�rzung
	int		 Bkd1/*[6]*/; 	// Pausenl�nge
	long 	 Urno; 	// Eindeutige Datensatz-Nr.
	long	 Bsdu;
	CTime	 Brkf;	//Pause von
	CTime	 Brkt;	//Pause bis
	CTime 	 Bkf1/*[6]*/; 	// Pausenlage von			====> char 	 Bkf1[6]
	CTime 	 Bkt1/*[6]*/; 	// Pausenlage bis			====> char 	 Bkt1
	CTime	 Sbgi;			//Schichtbeginn
	CTime	 Seni;			//Schichtende
	CTime 	 Cdat; 	// Erstellungsdatum
	CTime 	 Esbg; 	// Fr�hester Schichtbeginn		===> char 	 Esbg[6]
	CTime 	 Lsen; 	// Sp�testes Schichtende		===> char 	 Lsen[6]
	CTime 	 Lstu; 	// Datum letzte �nderung
	long	 Sdgu;
	char     Fctc[64]; // Extra bytes for 'multi-talented' employees, used in displaying roster plans - gsa

	//Fehlt noch
	//Additionals
	int  BarBkColorIdx;        // Index in global Color Table for Backgroundcolor of the Bar
	int  BarTextColorIdx;      // Index in global Color Table for Textcolor of the Bar
	int  TriangelColorLeft;	   // Dreiecksfarbe im Balken links
	int  TriangelColorRight;	   // Dreiecksfarbe im Balken rechts
	int  TblTextColorIdx;		// Index in global Color Table for TextColor in NoAllocTable
	int  IsChanged;	// Check whether Data has Changed f�r Relaese
	BOOL IsSelected;
	BOOL IsConflictAccepted;
	bool IsDetailMarked; 		
	bool IsVirtuel; 		
	int  Planungsstufe;
	char StatPlSt[2];
	long RosterStfu;
	long GspUrno;
	long SplUrno;
	long MsdUrno;
	int  Faktor;
	int  Drrn;
	char Sday[18];
    char Enames[125];


	MSDDATA(void)
	{ /*memset(&Key,'\0',sizeof(*this)-sizeof(Break));*/
		memset(this,'\0',sizeof(*this));

		IsSelected = FALSE;IsConflictAccepted=FALSE;
		Faktor = 1;
		Bkf1 = TIMENULL;
		Bkt1 = TIMENULL;
		Cdat = TIMENULL;
		Esbg = TIMENULL;
		Lsen = TIMENULL;
		Lstu = TIMENULL;
		Brkf = TIMENULL;
		Brkt = TIMENULL;

		IsDetailMarked = false;
		BarBkColorIdx = SILVER_IDX;
		BarTextColorIdx = BLACK_IDX;
		TriangelColorLeft = -1;	   // Dreiecksfarbe im Balken links
		TriangelColorRight = -1;	   // Dreiecksfarbe im Balken rechts
	}

	
 };	
//typedef struct MsdDataStruct MSDDATA;


// the broadcast CallBack function, has to be outside the CedaMsdData class
void ProcessMsdCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName);


class CedaMsdData: public CCSCedaData
{
// Attributes
public:
    CCSPtrArray<MSDDATA> omData;
	CUIntArray			 omSelectedData;
	CCSPtrArray<MSDDATA> omRedrawDuties;

    CMapPtrToPtr		 omUrnoMap;
	CMapStringToPtr		 omDnamMap;

	char pcmListOfFields[2048];
	void SetTableName(CString opTableName)
	{
		strcpy(pcmTableName, opTableName.GetBuffer(0));
	}
	bool ChangeTime(long lpUrno,CTime opStartTime,CTime opEndTime,GanttDynType bpDynType,BOOL bpSave = TRUE);
// Operations
public:
    CedaMsdData();
	~CedaMsdData();
	void Register(void);
	bool Read(char *pspWhere = NULL);
	bool ReadRealMsd(char *pspWhere = NULL);
	bool ReadSpecialData(CCSPtrArray<MSDDATA> *popMsd, char *pspWhere, char *pspFieldList, bool ipSYS);
	bool ReadMsdWithSdgu(long lpSdgu);
	bool DeleteMsdWithRos(long lpRosu,CTime opStart,CTime opEnd,bool bpWithDdx);

	bool DeleteMsdWithSplu(long lpSplUrno);

	bool DeleteVirtuellMsdsBySdg(long lpSdgu);
	bool DeleteVirtuellMsdsByMsdu(long lpMsdu);
	//bool DeleteAllMsdsWithRosu(long lpRosu,bool bpWithDdx = true);
	//bool DeleteMsdWithRosuBetweenTimes(long lpRosu,CTime opStart,CTime opEnd,bool bpWithDdx = true);
	//void PrepareMsdData(MSDDATA *prpMsd);
    //bool InsertMsd(const MSDDATA *prpMsdData);    // used in PrePlanTable only
    //bool UpdateMsd(const MSDDATA *prpMsdData);    // used in PrePlanTable only
 	void ProcessMsdBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
	bool AddMsdInternal(MSDDATA *prpMsd, bool bpWithDDX = false);
	bool UpdateMsdInternal(MSDDATA *prpMsd);
	bool ClearAll();



	MSDDATA * GetMsdByUrno(long pcpUrno);
	bool GetMsdsBySdgu(long lpSdgu, CCSPtrArray <MSDDATA> &opMsdData,bool bpAdd = false);

	bool GetMsdByGplUrno(long lpGplu, CCSPtrArray <MSDDATA> &opMsdData);
	bool GetMsdByGspUrno(long lpGspu, CCSPtrArray <MSDDATA> &opMsdData);
	bool GetMsdsBetweenTimes(CTime opStart,CTime opEnd,CCSPtrArray<MSDDATA> &opMsdArray);
	bool GetMsdsBetweenTimes(CTime opStart,CTime opEnd,CMapPtrToPtr &opMsdUrnoMap);
	bool GetMsdsBetweenTimesAndSdgu(CTime opStart,CTime opEnd,long lpSdgu,CMapPtrToPtr &opMsdUrnoMap);
	bool GetMsdsBetweenTimesAndDrrFilter(CTime opStart,CTime opEnd,CMapPtrToPtr &opMsdUrnoMap,bool bpShowActualDsr,int ipLevel);
	bool GetMsdsByGplus(CString opGplus,CMapPtrToPtr &opMsdUrnoMap);
	bool ChangeMsdTime(long lpMsdUrno,CTime opNewStart,CTime opNewEnd, bool bpWithSave = false);
	bool MsdExist(long Urno);
	bool DnamExist(CString opDnam);
	bool InsertMsd(MSDDATA *prpMsdData, BOOL bpWithSave = FALSE,bool bpSendDdx = true);
	bool UpdateMsd(MSDDATA *prpMsdData, BOOL bpWithSave = FALSE ,bool bpSendDdx = true);
	bool DeleteMsd(MSDDATA *prpMsdData, BOOL bpWithSave = FALSE,bool bpSendDdx = true);
	bool SaveMsd(MSDDATA *prpMsd);
	bool DeleteMsdInternal(MSDDATA *prpMsd);
	//void Release(CCSPtrArray<MSDDATA> *popMSDList = NULL);
	void DeleteBySdgu(long lpSdgUrno);
	void GetMsdByBsdu(long lpBsdu,CCSPtrArray<MSDDATA> *popMsdData);
	bool DeleteMsdWithSdgu(MSDDATA *prpMsdData,bool bpWithSave = true);
	bool Release(long lpSdgu = 0);
	bool Synchronise(CString opUnknown, bool bpActive = false);
private:
	void PrepareMsdData(MSDDATA *prpMsd);

	


};

extern CedaMsdData ogMsdData;

#endif
