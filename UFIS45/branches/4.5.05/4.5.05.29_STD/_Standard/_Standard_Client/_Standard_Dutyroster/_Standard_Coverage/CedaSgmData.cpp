// CedaSgmData.cpp - Class for Airline Types
//

#include <stdafx.h>
#include <ccsglobl.h>
#include <CCSPtrArray.h>
#include <CCSCedaData.h>
#include <ccsddx.h>
#include <CCSBcHandle.h>
#include <ccsddx.h>
#include <CedaSgmData.h>
#include <BasicData.h>

void  ProcessSgmCf(void * popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

CedaSgmData::CedaSgmData()
{                  
    BEGIN_CEDARECINFO(SGMDATA, SgmDataRecInfo)
		FIELD_LONG(Urno,"URNO")
		FIELD_CHAR_TRIM(Tabn,"TABN")
		FIELD_LONG(Ugty,"UGTY")
        FIELD_LONG(Usgr,"USGR")
        FIELD_LONG(Uval,"UVAL")
		FIELD_CHAR_TRIM(Valu,"VALU")
    END_CEDARECINFO

    // Copy the record structure
    for (int i = 0; i < sizeof(SgmDataRecInfo)/sizeof(SgmDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&SgmDataRecInfo[i],sizeof(CEDARECINFO));
        omRecInfo.Add(prpCedaRecInfo);
	}

	ogDdx.Register((void *)this,BC_SGM_CHANGE,CString("SGMDATA"), CString("Sgm changed"),ProcessSgmCf);
	ogDdx.Register((void *)this,BC_SGM_DELETE,CString("SGMDATA"), CString("Sgm deleted"),ProcessSgmCf);

    // Initialize table names and field names
    strcpy(pcmTableName,"SGMTAB");
    pcmFieldList = "URNO,TABN,UGTY,USGR,UVAL,VALU";
}
 
CedaSgmData::~CedaSgmData()
{
	TRACE("CedaSgmData::~CedaSgmData called\n");
	ogDdx.UnRegister(this,NOTUSED);
	ClearAll();
}

void  ProcessSgmCf(void * popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	BC_TRY
		((CedaSgmData *)popInstance)->ProcessSgmBc(ipDDXType,vpDataPointer,ropInstanceName);
	BC_CATCH_ALL
}

void CedaSgmData::ProcessSgmBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlSgmData = (struct BcStruct *) vpDataPointer;

	long llUrno = GetUrnoFromSelection(prlSgmData->Selection);
	if(llUrno == 0L && ipDDXType == BC_SGM_CHANGE)
	{
		SGMDATA rlSgm;
		GetRecordFromItemList(&omRecInfo,&rlSgm,prlSgmData->Fields,prlSgmData->Data);
		llUrno = rlSgm.Urno;
	}
	
	SGMDATA *prlSgm = GetSgmByUrno(llUrno);
	SGMDATA rlOriginalSgm;
	if(ipDDXType == BC_SGM_CHANGE && prlSgm != NULL)
	{
		// update
		GetRecordFromItemList(prlSgm,prlSgmData->Fields,prlSgmData->Data);
		ogDdx.DataChanged((void *)this,SGM_CHANGE,(void *)prlSgm);
	}
	else if(ipDDXType == BC_SGM_CHANGE && prlSgm == NULL)
	{
		// insert
		SGMDATA rlSgm;
		GetRecordFromItemList(&rlSgm,prlSgmData->Fields,prlSgmData->Data);
		prlSgm = AddSgmInternal(rlSgm);
		ogDdx.DataChanged((void *)this,SGM_CHANGE,(void *)prlSgm);
	}
	else if(ipDDXType == BC_SGM_DELETE && prlSgm != NULL)
	{
		// delete
		rlOriginalSgm = *prlSgm;
		DeleteSgmInternal(prlSgm);
		ogDdx.DataChanged((void *)this,SGM_DELETE,(void *)rlOriginalSgm.Urno);
	}
}

void CedaSgmData::ClearAll()
{
	omUrnoMap.RemoveAll();
	POSITION pos = omUsgrMap.GetStartPosition();
	while(pos)
	{
		void *polKey;
		CPtrArray *polUsgrList = NULL;
		omUsgrMap.GetNextAssoc(pos,polKey,(void *&)polUsgrList);
		if (polUsgrList)
		{
			delete polUsgrList;
		}
	}
	omUsgrMap.RemoveAll();
	omData.DeleteAll();
}

BOOL CedaSgmData::ReadSgmData(const CTime& ropStartTime,const CTime& ropEndTime)
{
	BOOL ilRc = TRUE;
	ClearAll();

	char pclCom[10] = "RT";
    char pclWhere[512] = "";

    if((ilRc = CedaAction2(pclCom, pclWhere)) == TRUE)
	{
		SGMDATA rlSgmData;
		for (int ilLc = 0; ilRc == TRUE; ilLc++)
		{
			if ((ilRc = GetBufferRecord2(ilLc,&rlSgmData)) == TRUE)
			{
				AddSgmInternal(rlSgmData);
			}
		}
		ilRc = TRUE;
	}

    return ilRc;
}


SGMDATA *CedaSgmData::AddSgmInternal(SGMDATA &rrpSgm)
{
	SGMDATA *prlSgm = new SGMDATA;
	*prlSgm = rrpSgm;
	omData.Add(prlSgm);
	omUrnoMap.SetAt((void *)prlSgm->Urno,prlSgm);

	CPtrArray *polUsgrList = NULL;
	if (!omUsgrMap.Lookup((void *)prlSgm->Usgr,(void *&)polUsgrList) || !polUsgrList)
	{
		polUsgrList = new CPtrArray;
		omUsgrMap.SetAt((void *)prlSgm->Usgr,polUsgrList);
	}

	if (polUsgrList)
	{
		polUsgrList->Add(prlSgm);
	}

	return prlSgm;
}

void CedaSgmData::DeleteSgmInternal(SGMDATA *prpSgm)
{
	CPtrArray *polUsgrList;
	if(omUsgrMap.Lookup((void *)prpSgm->Usgr,(void *& )polUsgrList) && polUsgrList)
	{
		for (int i = 0; i < polUsgrList->GetSize(); i++)
		{
			SGMDATA *prlSgm = (SGMDATA *)polUsgrList->GetAt(i);
			if (prlSgm && prlSgm->Uval == prpSgm->Uval)
			{
				polUsgrList->RemoveAt(i);
				break;
			}
		}
		if (polUsgrList->GetSize() == 0)
		{
			delete polUsgrList;
			omUsgrMap.RemoveKey((void *)prpSgm->Usgr);
		}
	}

	long llUrno = prpSgm->Urno;
	int ilCount = omData.GetSize();
	for (int ilLc = ilCount-1; ilLc >= 0; ilLc--)
	{
		if (omData[ilLc].Urno == llUrno)
		{
			omUrnoMap.RemoveKey((void *)llUrno);
			omData.DeleteAt(ilLc);
		}
	}
}

SGMDATA* CedaSgmData::GetSgmByUrno(long lpUrno)
{
	SGMDATA *prlSgm = NULL;
	omUrnoMap.Lookup((void *)lpUrno, (void *&) prlSgm);
	return prlSgm;
}

int	CedaSgmData::GetSgmByUsgr(const CString& ropTable,long lpUrno,CCSPtrArray<SGMDATA>& ropSgm)
{
	CPtrArray *polUsgrList = NULL;
	if (omUsgrMap.Lookup((void *)lpUrno,(void *&)polUsgrList) && polUsgrList)
	{
		ropSgm.Append(*polUsgrList);
	}

	return ropSgm.GetSize();
}

CString CedaSgmData::GetTableName(void)
{
	return CString(pcmTableName);
}

