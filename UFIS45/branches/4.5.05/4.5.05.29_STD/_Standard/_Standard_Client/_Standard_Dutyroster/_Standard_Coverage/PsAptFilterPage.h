#ifndef AFX_PSAPTFILTERPAGE_H__F0BFF844_A2B6_11D1_BD3B_0000B4392C49__INCLUDED_
#define AFX_PSAPTFILTERPAGE_H__F0BFF844_A2B6_11D1_BD3B_0000B4392C49__INCLUDED_

// PsAptFilterPage.h : Header-Datei
//
#include <GridFenster.h>

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CPsAptFilterPage 

class CPsAptFilterPage : public CPropertyPage
{
	DECLARE_DYNCREATE(CPsAptFilterPage)

// Konstruktion
public:
	CPsAptFilterPage();
	~CPsAptFilterPage();

	void SetCaption(const char *pcpCaption);
	char pcmCaption[100];

// Dialogfelddaten
	//{{AFX_DATA(CPsAptFilterPage)
	enum { IDD = IDD_PSAPTFILTER_PAGE };
	CButton	m_RemoveButton;
	CButton	m_AddButton;
	CListBox	m_ContentList;
	CListBox	m_InsertList;
	BOOL	m_Destination;
	BOOL	m_Origin;
	//}}AFX_DATA

	int imColCount;
	int imHideColStart;

	bool blIsInit;

	CGridFenster *pomPossilbeList;
	CGridFenster *pomSelectedList;
// Überschreibungen
	// Der Klassen-Assistent generiert virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(CPsAptFilterPage)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	//}}AFX_VIRTUAL

// Implementierung
protected:
	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(CPsAptFilterPage)
	afx_msg void OnButtonAdd();
	afx_msg void OnButtonRemove();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()


public:
	CStringArray omPossibleItems;
	CStringArray omSelectedItems;
	CStringArray omButtonValues;

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio fügt zusätzliche Deklarationen unmittelbar vor der vorhergehenden Zeile ein.

#endif // AFX_PSAPTFILTERPAGE_H__F0BFF844_A2B6_11D1_BD3B_0000B4392C49__INCLUDED_
