// FlightPage.cpp : implementation file
//

#include <stdafx.h>
#include <Coverage.h>
#include <FlightPage.h>
#include <BasicData.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CFlightPage property page

IMPLEMENT_DYNCREATE(CFlightPage, CPropertyPage)

CFlightPage::CFlightPage() : CPropertyPage(CFlightPage::IDD)
{
	//{{AFX_DATA_INIT(CFlightPage)
	m_FlightSuffixVal = _T("");
	m_Act3Val = _T("");
	m_AirpVal = _T("");
	m_Airp2Val = _T("");
	m_ZeitraumVal = TRUE;
	m_ArrivalVal = TRUE;
	m_BetriebVal = TRUE;
	m_CancelledVal = FALSE;
	m_FAirlineVal = _T("");
	m_FlightNrVal = _T("");
	m_LstuDateVal = _T("");
	m_LstuTimeVal = _T("");
	m_NoopVal = FALSE;
	m_PlanungVal = TRUE;
	m_RegnVal = _T("");
	m_RemaVal = _T("");
	m_SFromDateVal = _T("");
	m_SFromTimeVal = _T("");
	m_SToDateVal = _T("");
	m_SToTimeVal = _T("");
	m_TrafficTypeVal = _T("");
	m_DepatureVal = TRUE;
	m_FlukoDateVal = _T("");
	m_FlukoTimeVal = _T("");
	m_DivertedVal = FALSE;
	m_FidVal = TRUE;
	m_FlightDepVal = TRUE;
	m_RuleDiagnosis= FALSE;

	//}}AFX_DATA_INIT

	omSearchStart = CTime::GetCurrentTime();
	omSearchEnd = CTime::GetCurrentTime();
	m_SFromDateVal = omSearchStart.Format("%d.%m.%Y");
	m_SFromTimeVal = omSearchStart.Format("%H:%M");
	m_SToDateVal = omSearchEnd.Format("%d.%m.%Y");
	m_SToTimeVal = omSearchEnd.Format("%H:%M");
	imPageType = SEARCH;
}

CFlightPage::CFlightPage(PageType ipPageType,CTime opDate1,CTime opDate2) : CPropertyPage(CFlightPage::IDD)
{
	//{{AFX_DATA_INIT(CFlightPage)
	m_FlightSuffixVal = _T("");
	m_Act3Val = _T("");
	m_AirpVal = _T("");
	m_Airp2Val = _T("");
	m_ZeitraumVal = TRUE;
	m_ArrivalVal = TRUE;
	m_BetriebVal = TRUE;
	m_CancelledVal = FALSE;
	m_FAirlineVal = _T("");
	m_FlightNrVal = _T("");
	m_LstuDateVal = _T("");
	m_LstuTimeVal = _T("");
	m_NoopVal = FALSE;
	m_PlanungVal = TRUE;
	m_RegnVal = _T("");
	m_RemaVal = _T("");
	m_SFromDateVal = _T("");
	m_SFromTimeVal = _T("");
	m_SToDateVal = _T("");
	m_SToTimeVal = _T("");
	m_TrafficTypeVal = _T("");
	m_DepatureVal = TRUE;
	m_FlukoDateVal = _T("");
	m_FlukoTimeVal = _T("");
	m_DivertedVal = FALSE;

	m_FidVal = TRUE;
	m_FlightDepVal = TRUE;
	m_RuleDiagnosis= FALSE;

	//}}AFX_DATA_INIT

	imPageType = ipPageType;
	omSearchStart = opDate1;
	omSearchEnd = opDate2;
	m_SFromDateVal = opDate1.Format("%d.%m.%Y");
	m_SFromTimeVal = opDate1.Format("%H:%M");
	m_SToDateVal = opDate2.Format("%d.%m.%Y");
	m_SToTimeVal = opDate2.Format("%H:%M");

}

CFlightPage::~CFlightPage()
{
}

void CFlightPage::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CFlightPage)
	DDX_Control(pDX, IDC_DIVERTED, m_Diverted);
	DDX_Control(pDX, IDC_FLUKOTIME, m_FlukoTime);
	DDX_Control(pDX, IDC_FLUKODATE, m_FlukoDate);
	DDX_Control(pDX, IDC_DEPATURE, m_Depature);
	DDX_Control(pDX, IDC_ZEITRAUMSTATIC, m_ZeitraumStatic);
	DDX_Control(pDX, IDC_VERKEHRSART, m_VerkehrsArt);
	DDX_Control(pDX, IDC_TRAFFICTYPE, m_TrafficType);
	DDX_Control(pDX, IDC_STOTIME, m_SToTime);
	DDX_Control(pDX, IDC_STODATE, m_SToDate);
	DDX_Control(pDX, IDC_STIMETO, m_STimeTo);
	DDX_Control(pDX, IDC_STIMEFROM, m_STimeFrom);
	DDX_Control(pDX, IDC_SFROMTIME, m_SFromTime);
	DDX_Control(pDX, IDC_SFROMDATE, m_SFromDate);
	DDX_Control(pDX, IDC_REMA, m_Rema);
	DDX_Control(pDX, IDC_REGN, m_Regn);
	DDX_Control(pDX, IDC_REGISTRATION, m_Registration);
	DDX_Control(pDX, IDC_PLANUNG, m_Planung);
	DDX_Control(pDX, IDC_NOOP, m_Noop);
	DDX_Control(pDX, IDC_LSTUTIME, m_LstuTime);
	DDX_Control(pDX, IDC_LSTUSTATIC, m_LstuStatic);
	DDX_Control(pDX, IDC_LSTUFROM, m_LstuFrom);
	DDX_Control(pDX, IDC_LSTUDATE, m_LstuDate);
	DDX_Control(pDX, IDC_FTYPSTATIC, m_FTypStatic);
	DDX_Control(pDX, IDC_FLUKOSTATIC, m_FlukoStatic);
	DDX_Control(pDX, IDC_FLUKOFROM, m_FlukoFrom);
	DDX_Control(pDX, IDC_FLIGHTNR, m_FlightNr);
	DDX_Control(pDX, IDC_FLIGHTCODE, m_FlightCode);
	DDX_Control(pDX, IDC_FLIGHTAIRLINE, m_FAirline);
	DDX_Control(pDX, IDC_CANCELLED, m_Cancelled);
	DDX_Control(pDX, IDC_BETRIEB, m_Betrieb);
	DDX_Control(pDX, IDC_BEMERKUNG, m_Bemerkung);
	DDX_Control(pDX, IDC_ARRIVALS, m_Arrival);
	DDX_Control(pDX, IDC_ZEITRAUMCHECK, m_Zeitraum);
	DDX_Control(pDX, IDC_AIRPORT, m_Airport);
	DDX_Control(pDX, IDC_AIRP, m_Airp);
	DDX_Control(pDX, IDC_AIRP2, m_Airp2);
	DDX_Control(pDX, IDC_ACTYPE, m_ActType);
	DDX_Control(pDX, IDC_ACT3, m_Act3);
	DDX_Control(pDX, IDC_FLIGHTSUFFIX, m_FlightSuffix);
	DDX_Control(pDX, IDC_FLUGSTATIC, m_FlugStatic);
	DDX_Text(pDX, IDC_FLIGHTSUFFIX, m_FlightSuffixVal);
	DDX_Control(pDX, IDC_FIDCHECK, m_Fid);
	DDX_Control(pDX, IDC_FLIGHTDEP, m_FlightDep);
	DDX_Text(pDX, IDC_ACT3, m_Act3Val);
	DDX_Text(pDX, IDC_AIRP, m_AirpVal);
	DDX_Text(pDX, IDC_AIRP2, m_Airp2Val);
	DDX_Check(pDX, IDC_ARRIVALS, m_ArrivalVal);
	DDX_Check(pDX, IDC_ZEITRAUMCHECK, m_ZeitraumVal);
	DDX_Check(pDX, IDC_BETRIEB, m_BetriebVal);
	DDX_Check(pDX, IDC_CANCELLED, m_CancelledVal);
	DDX_Text(pDX, IDC_FLIGHTAIRLINE, m_FAirlineVal);
	DDX_Text(pDX, IDC_FLIGHTNR, m_FlightNrVal);
	DDX_Text(pDX, IDC_LSTUDATE, m_LstuDateVal);
	DDX_Text(pDX, IDC_LSTUTIME, m_LstuTimeVal);
	DDX_Check(pDX, IDC_NOOP, m_NoopVal);
	DDX_Check(pDX, IDC_PLANUNG, m_PlanungVal);
	DDX_Text(pDX, IDC_REGN, m_RegnVal);
	DDX_Text(pDX, IDC_REMA, m_RemaVal);
	DDX_Text(pDX, IDC_SFROMDATE, m_SFromDateVal);
	DDX_Text(pDX, IDC_SFROMTIME, m_SFromTimeVal);
	DDX_Text(pDX, IDC_STODATE, m_SToDateVal);
	DDX_Text(pDX, IDC_STOTIME, m_SToTimeVal);
	DDX_Text(pDX, IDC_TRAFFICTYPE, m_TrafficTypeVal);
	DDX_Check(pDX, IDC_DEPATURE, m_DepatureVal);
	DDX_Text(pDX, IDC_FLUKODATE, m_FlukoDateVal);
	DDX_Text(pDX, IDC_FLUKOTIME, m_FlukoTimeVal);
	DDX_Check(pDX, IDC_DIVERTED, m_DivertedVal);
	DDX_Check(pDX, IDC_FIDCHECK, m_FidVal);
	DDX_Check(pDX, IDC_FLIGHTDEP, m_FlightDepVal);
	DDX_Check(pDX, IDC_RULE_DIAGNOSIS, m_RuleDiagnosis);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CFlightPage, CPropertyPage)
	//{{AFX_MSG_MAP(CFlightPage)
	ON_BN_CLICKED(IDC_ZEITRAUMCHECK, OnZeitraum)

	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CFlightPage message handlers

BOOL CFlightPage::OnInitDialog() 
{
	CPropertyPage::OnInitDialog();

	
		CWnd *polWnd = GetDlgItem(IDC_ZEITRAUMCHECK);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING61264));
	}
	polWnd = GetDlgItem(IDC_STIMEFROM);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING61265));
	}
	polWnd = GetDlgItem(IDC_STIMETO);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING61266));
	}
	polWnd = GetDlgItem(IDC_ARRIVALS);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING1531));
	}
	polWnd = GetDlgItem(IDC_DEPATURE);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING1533));
	}
	polWnd = GetDlgItem(IDC_LSTUSTATIC);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING61269));
	}
	polWnd = GetDlgItem(IDC_LSTUFROM);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING61270));
	}
	polWnd = GetDlgItem(IDC_FLIGHTCODE);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING61271));
	}
	polWnd = GetDlgItem(IDC_REGISTRATION);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING61272));
	}
	polWnd = GetDlgItem(IDC_ACTYPE);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING61273));
	}
	polWnd = GetDlgItem(IDC_AIRPORT);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING61274));
	}
	polWnd = GetDlgItem(IDC_AIRPORT2);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING61275));
	}
	polWnd = GetDlgItem(IDC_BEMERKUNG);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING704));
	}
	polWnd = GetDlgItem(IDC_BETRIEB);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING61276));
	}
	polWnd = GetDlgItem(IDC_PLANUNG);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING61277));
	}
	polWnd = GetDlgItem(IDC_CANCELLED);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING61278));
	}
	polWnd = GetDlgItem(IDC_NOOP);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING61279));
	}

	polWnd = GetDlgItem(IDC_RULE_DIAGNOSIS);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING1925));
	}

	SetWindowText(LoadStg(IDS_STRING61205));
	


// Liste aller Controls die "versteckt" werden k�nnen
/*****************************************
	m_FlukoTime.ShowWindow(SW_HIDE);
	m_FlukoDate.ShowWindow(SW_HIDE);
	m_Depature.ShowWindow(SW_HIDE);
	m_ZeitraumStatic.ShowWindow(SW_HIDE);
	m_VerkehrsArt.ShowWindow(SW_HIDE);
	m_TrafficType.ShowWindow(SW_HIDE);
	m_SToTime.ShowWindow(SW_HIDE);
	m_SToDate.ShowWindow(SW_HIDE);
	m_STimeTo.ShowWindow(SW_HIDE);
	m_STimeFrom.ShowWindow(SW_HIDE);
	m_SFromTime.ShowWindow(SW_HIDE);
	m_SFromDate.ShowWindow(SW_HIDE);
	m_Rema.ShowWindow(SW_HIDE);
	m_Regn.ShowWindow(SW_HIDE);
	m_Registration.ShowWindow(SW_HIDE);
	m_Planung.ShowWindow(SW_HIDE);
	m_Noop.ShowWindow(SW_HIDE);
	m_LstuTime.ShowWindow(SW_HIDE);
	m_LstuStatic.ShowWindow(SW_HIDE);
	m_LstuFrom.ShowWindow(SW_HIDE);
	m_LstuDate.ShowWindow(SW_HIDE);
	m_FTypStatic.ShowWindow(SW_HIDE);
	m_FlukoStatic.ShowWindow(SW_HIDE);
	m_FlukoFrom.ShowWindow(SW_HIDE);
	m_FlightNr.ShowWindow(SW_HIDE);
	m_FlightCode.ShowWindow(SW_HIDE);
	m_FAirline.ShowWindow(SW_HIDE);
	m_Cancelled.ShowWindow(SW_HIDE);
	m_Betrieb.ShowWindow(SW_HIDE);
	m_Bemerkung.ShowWindow(SW_HIDE);
	m_Arrival.ShowWindow(SW_HIDE);
	m_Zeitraum.ShowWindow(SW_HIDE);
	m_Airport.ShowWindow(SW_HIDE);
	m_Airp.ShowWindow(SW_HIDE);
	m_Airp2.ShowWindow(SW_HIDE);
	m_ActType.ShowWindow(SW_HIDE);
	m_Act3.ShowWindow(SW_HIDE);
	m_FlightSuffix.ShowWindow(SW_HIDE);
	m_FlugStatic.ShowWindow(SW_HIDE);
	m_Diverted.ShowWindow(SW_HIDE);
	m_Fid.ShowWindow(SW_HIDE);
	m_FlightDep.ShowWindow(SW_HIDE);
	m_RuleDiagnosis.ShowWindow(SW_HIDE);
************************************/

	if(imPageType == SEARCH)
	{
		m_Fid.ShowWindow(SW_HIDE);
		m_FlightDep.ShowWindow(SW_HIDE);

		CWnd *polWnd = GetDlgItem(IDC_RULE_DIAGNOSIS);
		if(polWnd != NULL)
		{
			polWnd->ShowWindow(SW_HIDE);
			polWnd->EnableWindow(FALSE);
		}
	}
	else if(imPageType == CREATEDEM)
	{
		m_Cancelled.ShowWindow(SW_HIDE);
		m_Noop.ShowWindow(SW_HIDE);
		m_Arrival.ShowWindow(SW_HIDE);
		m_Depature.ShowWindow(SW_HIDE);
		m_Zeitraum.ShowWindow(SW_HIDE);
		CWnd *polWnd = GetDlgItem(IDC_RULE_DIAGNOSIS);
		if(polWnd != NULL)
		{
			polWnd->ShowWindow(SW_SHOWNORMAL);
			polWnd->EnableWindow(ogBasicData.IsRuleDiagnosisEnabled());
		}
	}
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

bool CFlightPage::GetSearchText(CString &ropWhere,bool &bpIsSingleFlight) 
{
	CString olT1,olT2,olT3;
	CString olTmp; 
	CTime olDate = TIMENULL;
	CTime olTime = TIMENULL;

	bpIsSingleFlight = false;

	bmCreateFid = (m_FidVal == TRUE)?true:false;
	bmCreateFdep = (m_FlightDepVal == TRUE)?true:false;
	ropWhere = "";
	CString olTmpFtypWhere;
	if(m_ZeitraumVal == TRUE)
	{
		if(m_SFromDateVal != "" && m_SFromTimeVal != "")
		{
			olDate = DateStringToDate(m_SFromDateVal);
			if(olDate != TIMENULL)
				olT1 = olDate.Format("%Y%m%d");
			else
			{
				CString olText, olAdd;
				olText = LoadStg(IDS_NO_DATE_TIME);
				olAdd = LoadStg(IDS_WARNING);
				MessageBox(olText, olAdd, MB_OK);
				return false;
			}
			if(olDate != TIMENULL)
			{
				olTime = HourStringToDate(m_SFromTimeVal, olDate);
				if(olTime != TIMENULL)
					olT1 = olDate.Format("%Y%m%d") + olTime.Format("%H%M%S");
				else
				{
					CString olText, olAdd;
					olText = LoadStg(IDS_NO_DATE_TIME);
					olAdd = LoadStg(IDS_WARNING);
					MessageBox(olText, olAdd, MB_OK);
					return false;
				}
			}
		}
		else
		{
			CString olText, olAdd;
			olText = LoadStg(IDS_NO_DATE_TIME);
			olAdd = LoadStg(IDS_WARNING);
			MessageBox(olText, olAdd, MB_OK);
			return false;
		}
	}		
	if(m_SToDateVal != "" && m_SToTimeVal != "")
	{
		olDate = DateStringToDate(m_SToDateVal);
		if(olDate != TIMENULL)
			olT2 = olDate.Format("%Y%m%d");
		else
		{
			CString olText, olAdd;
			olText = LoadStg(IDS_NO_DATE_TIME);
			olAdd = LoadStg(IDS_WARNING);
			MessageBox(olText, olAdd, MB_OK);
			return false;
		}
		if(olDate != TIMENULL)
		{
			olTime = HourStringToDate(m_SToTimeVal, olDate);
			if(olTime != TIMENULL)
				olT2 = olDate.Format("%Y%m%d") + olTime.Format("%H%M%S");
			else
			{
				CString olText, olAdd;
				olText = LoadStg(IDS_NO_DATE_TIME);
				olAdd = LoadStg(IDS_WARNING);
				MessageBox(olText, olAdd, MB_OK);
				return false;
			}
		}
	}
	else
	{
		CString olText, olAdd;
		olText = LoadStg(IDS_NO_DATE_TIME);
		olAdd = LoadStg(IDS_WARNING);
		MessageBox(olText, olAdd, MB_OK);
		return false;
	}

	if(m_LstuDateVal != "" && m_LstuTimeVal != "")
	{
		olDate = DateStringToDate(m_LstuDateVal);
		if(olDate != TIMENULL)
			olT3 = olDate.Format("%Y%m%d");
		else
		{
			CString olText, olAdd;
			olText = LoadStg(IDS_NO_DATE_TIME);
			olAdd = LoadStg(IDS_WARNING);
			MessageBox(olText, olAdd, MB_OK);
			return false;
		}
		if(olDate != TIMENULL)
		{
			olTime = HourStringToDate(m_LstuTimeVal, olDate);
			if(olTime != TIMENULL)
				olT3 = olDate.Format("%Y%m%d") + olTime.Format("%H%M%S");
			else
			{
				CString olText, olAdd;
				olText = LoadStg(IDS_NO_DATE_TIME);
				olAdd = LoadStg(IDS_WARNING);
				MessageBox(olText, olAdd, MB_OK);
				return false;
			}
		}
	}
	else
	{
	  olT3 = "";
	}

					
	bool blOr = false;
	bool blAnd = false;

	CTime olTime1 = CTime::GetCurrentTime();
	CTime olTime2 = CTime::GetCurrentTime();
	//RST!
	if(imPageType == CREATEDEM)
	{
		olTime1 = DBStringToDateTime(olT1);
		olTime2 = DBStringToDateTime(olT2);
		ogBasicData.LocalToUtc(olTime1);
		ogBasicData.LocalToUtc(olTime2);

		omFidStart = CTimeToDBString(olTime1,olTime1);
		omFidEnd = CTimeToDBString(olTime2,olTime2);
		

		if(olTime1 > olTime2)
		{
			CString olText, olAdd;
			olText = LoadStg(IDS_NO_DATE_TIME);
			olAdd = LoadStg(IDS_WARNING);
			MessageBox(olText, olAdd, MB_OK);
			return false;
		}
		
		CString olOrg3 = "";
		olOrg3.Format("AND DES3 = '%s')",pcgHome);
		ropWhere += olTime1.Format("((TIFA >= '%Y%m%d%H%M00' AND TIFA <=") + olTime2.Format("'%Y%m%d%H%M00'") +olOrg3 ;

		ropWhere += " OR ";

		CString olDes3 = "";
		olDes3.Format(" AND ORG3 = '%s'))",pcgHome);
		ropWhere += olTime1.Format("(TIFD >= '%Y%m%d%H%M00' AND TIFD <=") + olTime2.Format("'%Y%m%d%H%M00'") + olDes3 ;
		blAnd = true;
		
	}
	else if(m_ZeitraumVal == TRUE)
	{
		olTime1 = DBStringToDateTime(olT1);
		olTime2 = DBStringToDateTime(olT2);
		ogBasicData.LocalToUtc(olTime1);
		ogBasicData.LocalToUtc(olTime2);

		if(olTime1 > olTime2) 
		{
			CString olText, olAdd;
			olText = LoadStg(IDS_NO_DATE_TIME);
			olAdd = LoadStg(IDS_WARNING);
			MessageBox(olText, olAdd, MB_OK);
			return false;
		}

		if (m_ArrivalVal == TRUE)
		{
			blOr = true;
			CString olOrg3 = "";
			olOrg3.Format(" AND DES3 = '%s')",pcgHome);
			ropWhere += olTime1.Format("(TIFA >= '%Y%m%d%H%M00' AND TIFA <=") + olTime2.Format("'%Y%m%d%H%M00'") +olOrg3 ;

			blAnd = true;
		}
		if (m_DepatureVal == TRUE)
		{
			if(blOr== true)
			{
				ropWhere += " OR ";
			}
			CString olDes3 = "";
			olDes3.Format(" AND ORG3 = '%s')",pcgHome);
			ropWhere += olTime1.Format("(TIFD >= '%Y%m%d%H%M00' AND TIFD <=") + olTime2.Format("'%Y%m%d%H%M00'") + olDes3 ;		

			blAnd = true;
		}
		if(!ropWhere.IsEmpty())
		{
			ropWhere = CString("(") + ropWhere + CString(")");
		}
	}
	if(!olT3.IsEmpty())
	{
		CTime olTime3 = DBStringToDateTime(olT3);
		ogBasicData.LocalToUtc(olTime3);
		if(	blAnd )
			ropWhere += " AND ";
		blAnd = true;
		ropWhere +=  olTime3.Format(" LSTU >= '%Y%m%d%H%M00' ");
	}

	if(m_FAirlineVal.IsEmpty() == FALSE)
	{
		bpIsSingleFlight = true;
		m_FAirlineVal.MakeUpper();
		if(	blAnd )
			ropWhere += " AND ";
		blAnd = true;
		olTmp = m_FAirlineVal;
		if(m_FAirlineVal.Find("*") > -1)
		{	
			bpIsSingleFlight = false;
			olTmp.Replace("*","%");
			olTmp.Replace("?","_");
			ropWhere += "(ALC2 LIKE '" ;
			ropWhere += olTmp +"' OR ALC3 LIKE '" ;
			ropWhere +=  olTmp + "')";
		}
		else if(m_FAirlineVal.Find("?") > -1)
		{
			bpIsSingleFlight = false;
			olTmp.Replace("?","_");
			if(olTmp.GetLength() > 2)
			{
				ropWhere += "ALC3 LIKE '" + olTmp +"'"; 
			}
			else
			{
				ropWhere += "ALC2 LIKE '" + olTmp +"'"; 
			}
		}
		else
		{
			if(olTmp.GetLength() > 2)
			{
				ropWhere += "ALC3 = '" + olTmp +"'"; 
			}
			else
			{
				ropWhere += "ALC2 = '" + olTmp +"'"; 
			}
		}
	}
	if(m_FlightNrVal.IsEmpty() == FALSE)
	{
		if(	blAnd )
			ropWhere += " AND ";
		blAnd = true;
		olTmp = m_FlightNrVal;
		if(olTmp.Find("*") > -1 || olTmp.Find("?") > -1)
		{	
			bpIsSingleFlight = false;

			olTmp.Replace("*","%");
			olTmp.Replace("?","_");
			ropWhere += "FLTN LIKE '" + olTmp + CString("' ");
		}
		else
		{
			ropWhere += "FLTN = '" + olTmp + CString("' ");
		}

	}
	else
	{
		bpIsSingleFlight = false;
	}

	if(m_FlightSuffixVal.IsEmpty() == FALSE)
	{
		if(	blAnd )
			ropWhere += " AND ";
		blAnd = true;
		olTmp = m_FlightSuffixVal;
		if(olTmp.Find("*") > -1 || olTmp.Find("?") > -1)
		{	
			olTmp.Replace("*","%");
			olTmp.Replace("?","_");
			ropWhere += "FLNS LIKE '" + olTmp + CString("' ");
		}
		else
		{
			ropWhere += "FLNS = '" + olTmp + CString("' ");
		}

	}

	if(m_Act3Val.IsEmpty() == FALSE)
	{
		if(	blAnd )
			ropWhere += " AND ";
		blAnd = true;
		olTmp = m_Act3Val;
		if(olTmp.Find("*") > -1)
		{	
			olTmp.Replace("*","%");
			olTmp.Replace("?","_");
			ropWhere += "(ACT3 LIKE '" ;
			ropWhere += olTmp +"' OR ACT5 LIKE '" ;
			ropWhere +=  olTmp + "')";
		}
		else if(olTmp.Find("?") > -1)
		{
			olTmp.Replace("?","_");
			if(olTmp.GetLength() > 3)
			{
				ropWhere += "ACT5 LIKE '" + olTmp +"'"; 
			}
			else
			{
				ropWhere += "ACT3 LIKE '" + olTmp +"'"; 
			}
		}
		else
		{
			if(olTmp.GetLength() > 3)
			{
				ropWhere += "ACT5 = '" + olTmp +"'"; 
			}
			else
			{
				ropWhere += "ACT3 = '" + olTmp +"'"; 
			}
		}
	}
	if(m_RegnVal.IsEmpty() == FALSE)
	{
		if(	blAnd )
			ropWhere += " AND ";
		blAnd = true;
		olTmp = m_RegnVal;
		if(olTmp.Find("*") > -1 || olTmp.Find("?") > -1)
		{	
			olTmp.Replace("*","%");
			olTmp.Replace("?","_");
			ropWhere += "REGN LIKE '" + olTmp + CString("' ");
		}
		else
		{
			ropWhere += "REGN = '" + olTmp + CString("' ");
		}

	}
	
	if(m_AirpVal.IsEmpty() == FALSE) // Origin
	{
		if(	blAnd )
			ropWhere += " AND ";
		blAnd = true;
		olTmp = m_AirpVal;
		if(olTmp.Find("*") > -1)
		{	
			olTmp.Replace("*","%");
			olTmp.Replace("?","_");
			ropWhere += "(ORG3 LIKE '" ;
			ropWhere += olTmp +"' OR ORG4 LIKE '" ;
			ropWhere +=  olTmp + "')";
		}
		else if(olTmp.Find("?") > -1)
		{
			olTmp.Replace("?","_");
			if(olTmp.GetLength() > 3)
			{
				ropWhere += "ORG4 LIKE '" + olTmp +"'"; 
			}
			else
			{
				ropWhere += "ORG3 LIKE '" + olTmp +"'"; 
			}
		}
		else
		{
			if(olTmp.GetLength() > 3)
			{
				ropWhere += "ORG4 = '" + olTmp +"'"; 
			}
			else
			{
				ropWhere += "ORG3 = '" + olTmp +"'"; 
			}
		}
	}
	if(m_Airp2Val.IsEmpty() == FALSE) // Dest
	{
		if(	blAnd )
			ropWhere += " AND ";
		blAnd = true;
		olTmp = m_Airp2Val;
		if(olTmp.Find("*") > -1)
		{	
			olTmp.Replace("*","%");
			olTmp.Replace("?","_");
			ropWhere += "(DES3 LIKE '" ;
			ropWhere += olTmp +"' OR DES4 LIKE '" ;
			ropWhere +=  olTmp + "')";
		}
		else if(olTmp.Find("?") > -1)
		{
			olTmp.Replace("?","_");
			if(olTmp.GetLength() > 3)
			{
				ropWhere += "DES4 LIKE '" + olTmp +"'"; 
			}
			else
			{
				ropWhere += "DES3 LIKE '" + olTmp +"'"; 
			}
		}
		else
		{
			if(olTmp.GetLength() > 3)
			{
				ropWhere += "DES4 = '" + olTmp +"'"; 
			}
			else
			{
				ropWhere += "DES3 = '" + olTmp +"'"; 
			}
		}
	}
	if(m_TrafficTypeVal.IsEmpty() == FALSE)
	{
		if(	blAnd )
			ropWhere += " AND ";
		blAnd = true;
		olTmp = m_TrafficTypeVal;
		if(olTmp.Find("*") > -1 || olTmp.Find("?") > -1)
		{	
			olTmp.Replace("*","%");
			olTmp.Replace("?","_");
			ropWhere += "TTYP LIKE '" + olTmp + CString("' ");
		}
		else
		{
			ropWhere += "TTYP = '" + olTmp + CString("' ");
		}

	}
	if(m_RemaVal.IsEmpty() == FALSE)
	{
		if(	blAnd )
			ropWhere += " AND ";
		blAnd = true;
		olTmp = m_RemaVal;
		if(olTmp.Find("*") > -1 || olTmp.Find("?") > -1)
		{	
			olTmp.Replace("*","%");
			olTmp.Replace("?","_");
			ropWhere += "REM1 LIKE '" + olTmp + CString("' ");
		}
		else
		{
			ropWhere += "REM1 = '" + olTmp + CString("' ");
		}

	}
	
	olTmpFtypWhere = "";
	if(m_BetriebVal == TRUE)
	{	
		if(olTmpFtypWhere.IsEmpty())					
			olTmpFtypWhere += CString("FTYP IN ('O','R','D'");
		else
			olTmpFtypWhere += CString(",'O','R','D'");
	}
	if(m_PlanungVal == TRUE)
	{	
		if(olTmpFtypWhere.IsEmpty())					
			olTmpFtypWhere += CString("FTYP IN ('S'");
		else
			olTmpFtypWhere += CString(",'S'");
	}
	if(m_CancelledVal == TRUE)
	{	
		if(olTmpFtypWhere.IsEmpty())					
			olTmpFtypWhere += CString("FTYP IN ('X'");
		else
			olTmpFtypWhere += CString(",'X'");
	}
	if(m_NoopVal == TRUE)
	{	
		if(olTmpFtypWhere.IsEmpty())					
			olTmpFtypWhere += CString("FTYP IN ('N'");
		else
			olTmpFtypWhere += CString(",'N'");
	}

	if(!ropWhere.IsEmpty())
	{
		CString olTmp;
		olTmp = CString("(") + ropWhere + CString(")");
		ropWhere = olTmp;
	}

	if(!olTmpFtypWhere.IsEmpty())
	{
		CString olTmp;
		olTmp = CString("(") + olTmpFtypWhere + CString("))");

		if(ropWhere.IsEmpty())
			ropWhere = olTmp;
		else
			ropWhere = "(" + ropWhere + CString(" AND ") + olTmp + ")";
	}

	return true;
}

void CFlightPage::ResetDisplay()
{
	m_FlightSuffixVal = "";
	m_Act3Val = "";
	m_AirpVal = "";
	m_Airp2Val = "";
	m_ArrivalVal = TRUE;
	m_BetriebVal = TRUE;
	m_CancelledVal = FALSE;
	m_FAirlineVal = "";
	m_FlightNrVal = "";
	m_LstuDateVal = "";
	m_LstuTimeVal = "";
	m_NoopVal = FALSE;
	m_PlanungVal = TRUE;
	m_RegnVal = "";
	m_RemaVal = "";
	m_TrafficTypeVal = "";
	m_DepatureVal = TRUE;
	m_FlukoDateVal = "";
	m_FlukoTimeVal = "";
	m_DivertedVal = FALSE;

	m_FidVal = TRUE;
	m_FlightDepVal = TRUE;

	m_SFromDateVal = omSearchStart.Format("%d.%m.%Y");
	m_SFromTimeVal = omSearchStart.Format("%H:%M");
	m_SToDateVal = omSearchEnd.Format("%d.%m.%Y");
	m_SToTimeVal = omSearchEnd.Format("%H:%M");

	UpdateData(FALSE);
	if(imPageType == SEARCH)
	{
		m_Zeitraum.SetCheck(1);
		OnZeitraum();	
	}
	if(imPageType == CREATEDEM)
	{

	}



}

void CFlightPage::SetSearchStartEnd(CTime opDate1, CTime opDate2,bool bpSearchIsOpen)
{
	omSearchStart = opDate1;
	omSearchEnd = opDate2;
	RedisplayTime(bpSearchIsOpen);
}

void CFlightPage::RedisplayTime(bool bpUpdateDate)
{
	m_SFromDateVal = omSearchStart.Format("%d.%m.%Y");
	m_SFromTimeVal = omSearchStart.Format("%H:%M");
	m_SToDateVal = omSearchEnd.Format("%d.%m.%Y");
	m_SToTimeVal = omSearchEnd.Format("%H:%M");

	if(bpUpdateDate && this->m_hWnd != NULL)
		UpdateData(FALSE);
}
void CFlightPage::OnZeitraum() 
{
	bool blCheck = (m_Zeitraum.GetCheck() == 1)?true:false ;
	if(blCheck)
	{
		m_Depature.ShowWindow(SW_SHOW);
		m_SToTime.ShowWindow(SW_SHOW);
		m_SToDate.ShowWindow(SW_SHOW);
		m_STimeTo.ShowWindow(SW_SHOW);
		m_STimeFrom.ShowWindow(SW_SHOW);
		m_SFromTime.ShowWindow(SW_SHOW);
		m_SFromDate.ShowWindow(SW_SHOW);
		m_Arrival.ShowWindow(SW_SHOW);
	}
	else
	{
		m_Depature.ShowWindow(SW_HIDE);
		m_SToTime.ShowWindow(SW_HIDE);
		m_SToDate.ShowWindow(SW_HIDE);
		m_STimeTo.ShowWindow(SW_HIDE);
		m_STimeFrom.ShowWindow(SW_HIDE);
		m_SFromTime.ShowWindow(SW_HIDE);
		m_SFromDate.ShowWindow(SW_HIDE);
		m_Arrival.ShowWindow(SW_HIDE);
	}

}

void CFlightPage::SetCaption(const char *pcpCaption)
{
	strcpy(pcmCaption,pcpCaption);
	m_psp.pszTitle = pcmCaption;
	m_psp.dwFlags |= PSP_USETITLE;
}
