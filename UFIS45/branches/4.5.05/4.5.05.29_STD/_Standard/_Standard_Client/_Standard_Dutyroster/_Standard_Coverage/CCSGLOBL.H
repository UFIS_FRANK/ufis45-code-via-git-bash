// CCSGlobl.h: interface for the CCSGlobl class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_CCSGLOBL_H__55D90F34_191F_11D1_82BE_0080AD1DC701__INCLUDED_)
#define AFX_CCSGLOBL_H__55D90F34_191F_11D1_82BE_0080AD1DC701__INCLUDED_

#ifndef __CCSGLOBL_H__
#define __CCSGLOBL_H__

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000


#include <CCSDefines.h>
#include <CCSTime.h>
#include <iomanip.h>
#include <iostream.h>
#include <fstream.h>

#include <resource.h>


/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
// Global Variable Section


class CCSBcHandle;
class CCSCedaCom;
class CCSDdx;
class CCSLog;
class CedaCfgData;
class CCSBcHandle;
class CBasicData;
class CedaCfgData;
class CedaDemData;
class CedaPerData;
class CedaGpmData;
class CedaGhpData;
class CedaGegData;
class CedaGATData;
class CedaNfmData;
class CedaNfpData;
class CedaCICData;
class CedaCfgData;
class CedaBLTData;
class CedaSysTabData;
class CedaACRData;
class CedaPfcData;
class CedaPrcData;
class CedaDsrData;
class CedaFlightData;
class PrivList;
class CBackGround;
class ConflictCheck;
class CACProgressBar;


// start bch
class CedaBLTData;
class CedaEXTData;
class CedaWROData;
class CedaGATData;
class CedaDENData;
class CedaFlightData;
class CFlightTableDlg;
class CJobDlg;
class CedaBasicData;
class DiaCedaFlightData;
class PosDiagram;
class CedaDrrData;

#define IDM_ROTATION		11L
#define IDM_SEASON			14L
#define STAFFGANTT_ID		10
#define JOBDLG_ID			20

// end bch
class DutyRosterDlg;
//start are
class ShiftRosterDlg;
//end are


extern CedaBasicData ogBCD;

extern CCSBcHandle ogBcHandle;
extern CCSCedaCom ogCommHandler;  // The one and only CedaCom object
extern CCSDdx ogDdx;
extern CCSLog ogLog;
extern CBasicData ogBasicData;
//extern CedaACRData ogAcrData;
extern CedaCfgData ogCfgData;
extern CedaPerData ogPerData;
extern CedaGegData ogGegData;
extern CedaGATData	ogGatData;
extern CedaCICData ogCicData;
extern CedaCfgData ogCfgData;
extern CedaBLTData ogBltData;
extern CedaPfcData	ogPfcData;
extern CedaDemData  ogDemData;

extern CedaFlightData ogFlightData;
extern CedaFlightData ogCovFlightData;
extern DiaCedaFlightData ogPosDiaFlightData;

extern PosDiagram	 *pogPosDiagram;
//MWO 01.08.03 
extern CACProgressBar *pogProgressBarDlg;
//END MWO 01.08.03 

extern CedaSysTabData ogSysTabData;
extern ConflictCheck ogConflictData;
extern ConflictCheck ogCoverageConflictData;

// start bch
extern CedaBLTData		ogBltData;
extern CedaEXTData		ogExtData;
extern CedaWROData		ogWroData;
extern CedaGATData		ogGatData;
extern CedaDENData		ogDenData;
extern CFlightTableDlg *pogFlightTableDlg;
extern CedaFlightData	ogCedaFlightData;
extern CedaFlightData	ogCedaFlightTableData;
extern CedaDrrData		ogDrrData;
extern bool bgIsModal;
extern CJobDlg *pogJobDlg;
extern CFlightTableDlg *pogFlightTableDlg;
extern bool bgKlebefunktion;
// end bch
//start are
extern ShiftRosterDlg *pogShiftRosterDlg;
extern DutyRosterDlg *pogDutyRosterDlg;
// end are

extern PrivList ogPrivList;
extern const char *pcgAppName;
extern CString ogCustomer;

extern char pcgUser[33];
extern char pcgPasswd[33];

extern bool bpUseGhdBc;

//uhi 11.10.00
extern CTime ogLoginTime;

extern bool bgCreateDemIsActive;
extern int	bgCreateDemCount;
extern int	bgReleaseDemCount;
extern bool bgRuleDiagnosticIsActive;
enum ChartState{ Minimized, Normal, Maximized };
enum GanttDynType{GANTT_NONE,GANTT_SHIFT,GANTT_BREAK,GANTT_BREAK_PERIOD};
extern ofstream of_catch;
extern CString	ogBackTrace;
extern WORD		ogBcRecursion;	
extern const CString ogAutoCoverageLog;

/**********************
#define  CCS_TRY try{
#define  CCS_CATCH_ALL }\
						catch(...)\
						{\
						    char pclExcText[512]="";\
						    char pclText[1024]="";\
						    sprintf(pclText, "Es ist ein interner Fehler im Modul: %s\n in der Zeile %d aufgetreten",\
							                 __FILE__, __LINE__);\
						    strcat(pclText, "\n Beim Fortsetzen kann es zu unerw�nschten Effekten kommen\nFortsetzen??");\
						    sprintf(pclExcText, "File: %s  ==> Source-Line: %d", __FILE__, __LINE__);\
						    of_catch << pclExcText << endl;\
							if(::MessageBox(NULL, pclText, "Error", (MB_YESNO)) == IDNO)\
							{\
						       ExitProcess(0);\
							}\
						}
*************************/
/**************/
#define  CCS_TRY 
#define  CCS_CATCH_ALL 
/*********/


#define  BC_TRY			try\
						{\
							++ogBcRecursion;\
							if (ogBackTrace.GetLength() > 0)\
								ogBackTrace = CString("[") + ogBackTrace + CString("]");

#define  BC_CATCH_ALL	ogBackTrace = "";\
						--ogBcRecursion;\
						}\
						catch(...)\
						{\
							struct BcStruct *prlBcData;\
							prlBcData = (struct BcStruct *) vpDataPointer;\
							if (prlBcData != NULL)\
							{\
								CString olMsg;\
								olMsg.Format(LoadStg(IDS_STRING1936),prlBcData->Cmd,prlBcData->Object,prlBcData->Seq,prlBcData->Tws,prlBcData->Twe,prlBcData->DdxType,prlBcData->Selection,prlBcData->Fields,prlBcData->Data,prlBcData->BcNum,ogBackTrace);\
								MessageBox(NULL,olMsg,"Fatal error",MB_OK);\
							}\
							else\
							{\
								MessageBox(NULL,"Unknown Broadcast exception occured","Fatal error",MB_OK);\
							}\
						}

//Fsc //Enum types f�r den Searchpage 
enum PageType{ SEARCH,CREATEDEM, DUMMY };

enum FilterType{ FUNCTION,QUALIFICATION, CHECKIN,POSITIONS,GATES ,EQUIPMENT,EQUIPMENTGROUP};

enum SearchType{PS_FLIGHTS, PS_EMPLOYEES, PS_HUBWAGEN};


enum enumBitmapIndexes
{
	BREAK_RED_IDX = 21,
	BREAK_GRAY_IDX = 22,
	BREAK_GREEN_IDX = 23,
	BREAK_SILVER_IDX = 24
};

enum SimValues
{
	SIM_CONSTANT,
	SIM_PERCENT,
	SIM_ABSOLUTE
};

//enum enumColorIndexes
//{
//	BLACK_IDX, WHITE_IDX, GREEN_IDX, MAROON_IDX, OLIVE_IDX, NAVY_IDX,
//	PURPLE_IDX, TEAL_IDX, GRAY_IDX, SILVER_IDX, RED_IDX, RED2_IDX,
//	YELLOW_IDX, GREEN2_IDX, LIME_IDX, YELLOW2_IDX, BLUE_IDX, FUCHSIA_IDX,
//	AQUA_IDX, WHITE2_IDX, GREEN3_IDX
//};

enum enumColorIndexes
{
	GRAY_IDX=2,GREEN_IDX,RED_IDX,BLUE_IDX,SILVER_IDX,MAROON_IDX,
	OLIVE_IDX,NAVY_IDX,PURPLE_IDX,TEAL_IDX,LIME_IDX,
	YELLOW_IDX,FUCHSIA_IDX,AQUA_IDX, WHITE_IDX,BLACK_IDX,ORANGE_IDX,
	PGREEN_IDX,PBLUE_IDX
};


//DDX-Types
enum 
{
	CLOSE_ALL_MODALS,
	BC_GHS_NEW, BC_GHS_DELETE, BC_GHS_CHANGE,
	GHS_NEW, GHS_DELETE, GHS_CHANGE,
	BC_GPM_NEW, BC_GPM_DELETE, BC_GPM_CHANGE,
	GPM_NEW, GPM_DELETE, GPM_CHANGE,
	BC_GHP_NEW, BC_GHP_DELETE, BC_GHP_CHANGE,
	//GSA - Non Flight Rules
	BC_NFP_NEW, BC_NFP_DELETE, BC_NFP_CHANGE,
	BC_NFM_NEW, BC_NFM_DELETE, BC_NFM_CHANGE,
	//END GSA
	BC_GRM_NEW, BC_GRM_DELETE, BC_GRM_CHANGE,
	GRM_NEW, GRM_DELETE, GRM_CHANGE,
	BC_GRN_NEW, BC_GRN_DELETE, BC_GRN_CHANGE,
	GRN_NEW, GRN_DELETE, GRN_CHANGE,
	GHP_NEW, GHP_CHANGE, GHP_DELETE,
	//GSA - Non Flight Rules
	NFP_NEW, NFP_CHANGE, NFP_DELETE,
	NFM_NEW, NFM_CHANGE, NFM_DELETE,
	//END GSA
	RUE_NEW, RUE_CHANGE, RUE_DELETE,
	RUD_NEW, RUD_CHANGE, RUD_DELETE,

	BC_DRR_NEW, BC_DRR_CHANGE, BC_DRR_DELETE,
	DRR_NEW, DRR_CHANGE, DRR_DELETE,
//uhi 29.8.00
	MSD_NEW, MSD_CHANGE, MSD_DELETE,
	MSD_NEW_SELF, MSD_CHANGE_SELF, MSD_DELETE_SELF,

	GSP_NEW, GSP_CHANGE, GSP_DELETE,
	GPL_NEW, GPL_CHANGE, GPL_DELETE,
	SPL_NEW, SPL_CHANGE, SPL_DELETE,

//----BDPS-SECTION
	BC_ALT_CHANGE,BC_ALT_DELETE,ALT_CHANGE,ALT_DELETE,    
	BC_ACT_CHANGE,BC_ACT_DELETE,ACT_CHANGE,ACT_DELETE,    
	BC_ACR_CHANGE,BC_ACR_DELETE,ACR_CHANGE,ACR_DELETE,
	BC_APT_CHANGE,BC_APT_DELETE,APT_CHANGE,APT_DELETE,    
	BC_RWY_CHANGE,BC_RWY_DELETE,RWY_CHANGE,RWY_DELETE,    
	BC_TWY_CHANGE,BC_TWY_DELETE,TWY_CHANGE,TWY_DELETE,   
	BC_PST_CHANGE,BC_PST_DELETE,PST_CHANGE,PST_DELETE, 
	BC_GAT_CHANGE,BC_GAT_DELETE,GAT_CHANGE,GAT_DELETE,
	BC_CIC_CHANGE,BC_CIC_DELETE,CIC_CHANGE,CIC_DELETE,   
	BC_BLT_CHANGE,BC_BLT_DELETE,BLT_CHANGE,BLT_DELETE,   
	BC_EXT_CHANGE,BC_EXT_DELETE,EXT_CHANGE,EXT_DELETE,   
	BC_DEN_CHANGE,BC_DEN_DELETE,DEN_CHANGE,DEN_DELETE,   
	BC_MVT_CHANGE,BC_MVT_DELETE,MVT_CHANGE,MVT_DELETE,   
	BC_HAG_CHANGE,BC_HAG_DELETE,HAG_CHANGE,HAG_DELETE,    
	BC_NAT_CHANGE,BC_NAT_DELETE,NAT_CHANGE,NAT_DELETE,   
	BC_WRO_CHANGE,BC_WRO_DELETE,WRO_CHANGE,WRO_DELETE,   
	BC_HTY_CHANGE,BC_HTY_DELETE,HTY_CHANGE,HTY_DELETE,   
	BC_STY_CHANGE,BC_STY_DELETE,STY_CHANGE,STY_DELETE,   
	BC_FID_CHANGE,BC_FID_DELETE,FID_CHANGE,FID_DELETE,
	BC_PER_CHANGE,BC_PER_NEW,BC_PER_DELETE,
	PER_NEW,PER_DELETE,PER_CHANGE,
	BC_GEG_CHANGE,BC_GEG_NEW,BC_GEG_DELETE,
	GEG_CHANGE,GEG_NEW,GEG_DELETE,
	BC_PFC_CHANGE,BC_PFC_DELETE,BC_PFC_NEW,PFC_CHANGE,PFC_DELETE,PFC_NEW,
	BC_CCA_CHANGE,BC_CCA_DELETE,BC_CCA_NEW,CCA_CHANGE,CCA_DELETE,CCA_NEW,
	BC_PRC_CHANGE,BC_PRC_DELETE,BC_PRC_NEW,PRC_CHANGE,PRC_DELETE,PRC_NEW,
	BC_SDT_CHANGE,BC_SDT_DELETE,BC_SDT_NEW,SDT_CHANGE,SDT_SELCHANGE,SDT_DELETE,SDT_NEW,
	BC_SDG_CHANGE,BC_SDG_DELETE,BC_SDG_NEW,SDG_CHANGE,SDG_DELETE,SDG_NEW,
	BC_ODA_CHANGE,BC_ODA_DELETE,BC_ODA_NEW,ODA_CHANGE,ODA_DELETE,ODA_NEW,
	BC_ORG_CHANGE,BC_ORG_DELETE,BC_ORG_NEW,ORG_CHANGE,ORG_DELETE,ORG_NEW,
//FPMS-Section
	S_FLIGHT_UPDATE, S_FLIGHT_DELETE, S_FLIGHT_INSERT,
	BC_GHD_CHANGE,BC_GHD_DELETE,BC_GHD_NEW,GHD_CHANGE,GHD_DELETE,GHD_NEW,
	GHD_STATUS_CHANGE,GHD_MARK_BAR,COV_MARKBAR,COV_NEWBAR,ENDDRR_RELEASE,
	BC_DSR_CHANGE,BC_DSR_DELETE,BC_DSR_NEW,DSR_CHANGE,DSR_MULTI_CHANGE,DSR_DELETE,DSR_NEW,DSR_DELETE_COV,DSR_NEW_COV,DSR_CHANGE_COV,DSR_MULTI_CHANGE_COV,
	FLIGHT_CHANGE, FLIGHT_DELETE, FLIGHT_UPDATE,
	BC_FLIGHT_CHANGE, BC_FLIGHT_DELETE,
//--COVERAGE Section 
	DEM_CHANGE,DEM_DELETE,DEM_NEW,
//--INTERNAL DDX-SECTION
    BC_CFG_INSERT, BC_CFG_CHANGE, CFG_CHANGE, CFG_INSERT,CFG_DELETE,
	GPM_PREMIS_DELETE,

	BC_SPE_CHANGE, BC_SPE_NEW, BC_SPE_DELETE, SPE_NEW, SPE_DELETE,SPE_CHANGE,
	BC_SCO_CHANGE, BC_SCO_NEW, BC_SCO_DELETE, SCO_NEW, SCO_DELETE,SCO_CHANGE,
	BC_SOR_CHANGE, BC_SOR_NEW, BC_SOR_DELETE, SOR_NEW, SOR_DELETE,SOR_CHANGE,
	BC_SPF_CHANGE, BC_SPF_NEW, BC_SPF_DELETE, SPF_NEW, SPF_DELETE,SPF_CHANGE,
	BC_STE_CHANGE, BC_STE_NEW, BC_STE_DELETE, STE_NEW, STE_DELETE,STE_CHANGE,
	BC_SWG_CHANGE, BC_SWG_NEW, BC_SWG_DELETE, SWG_NEW, SWG_DELETE,SWG_CHANGE,
	BC_MSD_CHANGE,BC_MSD_NEW,BC_MSD_DELETE,
//ARE
	BC_ACC_NEW,BC_ACC_DELETE,BC_ACC_CHANGE,ACC_NEW,ACC_DELETE,ACC_CHANGE,ACC_FIELD_CHANGE,
	BC_ENABLEACCSAVE,BC_DISABLEACCSAVE,BC_RELACC,RELACC,
	BC_INF_NEW,BC_INF_DELETE,BC_INF_CHANGE,INF_NEW,INF_DELETE,INF_CHANGE,
	BC_ESP_NEW,BC_ESP_DELETE,BC_ESP_CHANGE,ESP_NEW,ESP_DELETE,ESP_CHANGE,ESP_FIELD_CHANGE,
	BC_PGP_NEW,BC_PGP_DELETE,BC_PGP_CHANGE,PGP_NEW,PGP_DELETE,PGP_CHANGE,PGP_FIELD_CHANGE,
	BC_WGP_NEW,BC_WGP_DELETE,BC_WGP_CHANGE,WGP_NEW,WGP_DELETE,WGP_CHANGE,WGP_FIELD_CHANGE,
//END ARE 
// start bch
	BC_COT_CHANGE,BC_COT_DELETE,BC_COT_NEW,COT_CHANGE,COT_DELETE,COT_NEW,
	BC_ASF_CHANGE,BC_ASF_DELETE,BC_ASF_NEW,ASF_CHANGE,ASF_DELETE,ASF_NEW,
	S_DLG_FLIGHT_CHANGE,S_DLG_FLIGHT_DELETE,S_FLIGHT_CHANGE,
// end bch

	BC_RELOAD_FLIGHT_GHDS, BC_RELOAD_SINGLE_GHD, BC_RELOAD_ALL_GHDS,  
	BC_RELOAD_SINGLE_DSR,BC_RELOAD_MULTI_DSR,BC_DISABLEGHDSAVE,BC_ENABLEGHDSAVE,BC_RELOAD_MULTI_GHD,
	BC_RELDSR,RELDSR,
	BC_RELDRR,RELDRR,
	BC_RELPBO,RELPBO,
	BC_UPDDEM,UPDDEM,

	D_FLIGHT_CHANGE, D_FLIGHT_DELETE, D_FLIGHT_UPDATE,D_FLIGHT_INSERT,
	
//Coverage-Part
	COV_REDRAW,
//from HG
	BC_TEA_CHANGE,BC_TEA_NEW,BC_TEA_DELETE,
	TEA_CHANGE,TEA_NEW,TEA_DELETE,
	BC_BSD_CHANGE,BC_BSD_NEW,BC_BSD_DELETE,
	BSD_CHANGE,BSD_NEW,BSD_DELETE,
	BC_EQU_NEW, BC_EQU_DELETE, BC_EQU_CHANGE,
	EQU_NEW, EQU_DELETE, EQU_CHANGE,
	BC_STF_CHANGE,BC_STF_DELETE,BC_STF_NEW,STF_CHANGE,STF_DELETE,STF_NEW,
//end from HG

//Staffdiagram
	BC_SERVER_SHUTDOWN,
//Flight Changes with Conflict
	FLT_NEW_FLIGHT, FLT_AC_CHANGED, FLT_EQU_CHANGED, FLT_CXX_CHANGED,
	FLT_GAT_CHANGED, FLT_POS_CHANGED, FLT_BLT_CHANGED,
	UNDO_CHANGE,
//Collect Flights
    BC_CFL_READY,
// Demands
	BC_DEM_NEW,BC_DEM_CHANGE,BC_DEM_DELETE,DEM_CFL_READY,

// Release events
	BC_RELSDG,BC_RELMSD,RELSDG,RELMSD,BC_RELDEM,RELDEM,	

	SDG_NEW_SELF,SDG_CHANGE_SELF,SDG_DELETE_SELF,

//--INFO on Demand change, necessary to have viewer updated 
	INFO_DEM_CHANGE,INFO_DEM_DELETE,INFO_DEM_NEW
};

enum DLG_ACTION 
{ 
    DLG_INSERT,DLG_UPDATE
   
};  

//enum enumBarType{BAR_FLIGHT,BAR_SPECIAL,BKBAR,GEDBAR,BAR_BREAK,BAR_ABSENT,BAR_SHADOW};


////////////////////////////////////////////////////////////////////////////////////
// used by  xxxxCedaFlightData

struct JFNODATA
{
	long Urno;
	char Alc3[4];
	char Fltn[6];
	char Flns[2];

	JFNODATA(void)
	{
		memset(this,'\0',sizeof(*this));
		Urno = 0;
	}

};
struct VIADATA
{
	char	Fids[2];
	char	Apc3[4];
	char	Apc4[5];
	CTime	Stoa;
	CTime	Etoa;
	CTime	Land;
	CTime	Onbl;
	CTime	Stod;
	CTime	Etod;
	CTime	Ofbl;
	CTime	Airb;

	VIADATA(void)
	{ 
		sprintf(Fids, "");
		sprintf(Apc3, "");
		sprintf(Apc4, "");
		Stoa = -1;
		Etoa = -1;
		Land = -1;
		Onbl = -1;
		Stod = -1;
		Etod = -1;
		Ofbl = -1;
		Airb = -1;
	}
};




#define MAXCOLORS 64
#define FIRSTCONFLICTCOLOR 31

// Symbolic colors (helper constants for CGateDiagramDialog -- testing purpose only)
#define BLACK   RGB(  0,   0,   0)
#define MAROON  RGB(128,   0,   0)          // dark red
#define GREEN   RGB(  0, 128,   0)          // dark green
#define DKGREEN RGB(  0, 200,   0)          // dark green
#define OLIVE   RGB(128, 128,   0)          // dark yellow
#define NAVY    RGB(  0,   0, 128)          // dark blue
#define PURPLE  RGB(128,   0, 128)          // dark magenta
#define TEAL    RGB(  0, 128, 128)          // dark cyan
#define GRAY    RGB(128, 128, 128)          // dark gray
//PRF6932 #define SILVER  RGB(192, 192, 192)          // light gray
#define SILVER ::GetSysColor(COLOR_BTNFACE)
#define RED     RGB(255,   0,   0)
#define ORANGE  RGB(255, 132,   0)
#define LIME    RGB(  0, 255,   0)          // green
#define YELLOW  RGB(255, 255,   0)
#define LTYELLOW  RGB(255, 255,   160)
#define BLUE    RGB(  0,   0, 255)
#define FUCHSIA RGB(255,   0, 255)          // magenta
#define AQUA    RGB(  0, 255, 255)          // cyan
#define WHITE   RGB(255, 255, 255)
#define LTGRAY  RGB(170, 170, 170)
#define MEDIUMDGRAY  RGB(164, 164, 164)
#define	BROWN	RGB(128,  64,   0)
#define	PGREEN	RGB(190,255,190)
#define	PBLUE	RGB(157,255,255)

extern COLORREF ogColors[];
extern CBrush *ogBrushs[];
extern CString ogAppName;
extern CString ogAppl;
extern CString ogGlobal;

extern COLORREF lgBkColor;
extern COLORREF lgTextColor;
extern COLORREF lgHilightColor;

extern enum enumRecordState egRecordState;

extern BOOL bgIsInitialized;

// CFont : translate from logical point to physical pixel
#define PT_TO_PIXELS(dc, pt)    (- MulDiv( pt, (dc).GetDeviceCaps( LOGPIXELSY ), 72 ))


/////////////////////////////////////////////////////////////////////////////
// IDs


#define IDC_PREV            0x4008
#define IDC_NEXT            0x4009
#define IDC_INPLACEEDIT		0x400c


#define IDM_ROTATION		11L
#define IDM_SEASON			14L
#define IDM_IMPORT			17L
#define IDM_COMMONCCA		19L


/////////////////////////////////////////////////////////////////////////////
// Messages


// Message and constants which are used to handshake viewer and the attached diagram
#define WM_USERKEYDOWN					(WM_USER + 151)
#define WM_USERKEYUP					(WM_USER + 152)
#define WM_POSITIONCHILD				(WM_USER + 201)
#define WM_UPDATEDIAGRAM				(WM_USER + 202)
#define UD_UPDATEGROUP                  (WM_USER + 203)
#define UD_DELETELINE                   (WM_USER + 204)
#define UD_INSERTLINE                   (WM_USER + 206)
#define UD_UPDATELINEHEIGHT             (WM_USER + 208)
#define UD_UPDATELINE					(WM_USER + 209)
#define UD_UPDATEDLG					(WM_USER + 210)
#define WM_REASSIGNFINISHED				(WM_USER + 211)
#define WM_ATTENTIONSETCOLOR			(WM_USER + 212)
#define UD_REASSIGNFINISHED				(WM_USER + 213)
#define WM_SETRELEASE					(WM_USER + 216)
#define WM_RESETRELEASE					(WM_USER + 217)
#define UD_RESETCONTENT					(WM_USER + 220)
#define WM_SETREDRAWBUTTON				(WM_USER + 211)
#define UB_SETREDRAW					(WM_USER + 212)
#define UB_RESETREDRAW					(WM_USER + 213)
#define WM_SETATTENTIONBUTTON			(WM_USER + 214)
#define WM_SETTIMELINES					(WM_USER + 215)
#define WM_PRINT_COVERAGE				(WM_USER + 216)
#define WM_REPAINT_ALL					(WM_USER + 217)
#define WM_UNDO_EXIT					(WM_USER + 218)
#define WM_MARKTIME						(WM_USER + 219)
#define WM_SIMULATE						(WM_USER + 221)


#define GRID_MESSAGE_ENDEDITING			(WM_USER + 222)
#define GRID_MESSAGE_BUTTONCLICK		(WM_USER + 223)
#define GRID_MESSAGE_CELLCLICK			(WM_USER + 224)
#define GRID_MESSAGE_DOUBLECLICK		(WM_USER + 225)
#define GRID_ACTCELLMOVED		        (WM_USER + 226)

#define WM_DEACTIVEAUTOCOV				(WM_USER + 230)
#define WM_ACTIVEAUTOCOV				(WM_USER + 231)
#define	WM_UPDATEDEMANDBTN				(WM_USER + 232)

#define	WM_UPDATE_ALL_PAGES				(WM_USER + 233)  /* update all pages	*/
#define	WM_LOAD_TIMEFRAME				(WM_USER + 234)
#define	WM_UPDATETOOLBAR				(WM_USER + 235)

#define WM_CCSBUTTON_RBUTTONDOWN		(WM_USER + 350)
#define WM_COV_RELOADCOMBO				(WM_USER + 351)
#define WM_COV_UPDATECOMBO				(WM_USER + 352)

#define WM_DYNTABLE_HSCROLL				(WM_USER + 360)
#define WM_DYNTABLE_VSCROLL				(WM_USER + 361)
#define WM_DYNTABLE_HMOVE				(WM_USER + 362)
#define WM_DYNTABLE_VMOVE				(WM_USER + 363)
#define WM_DYNTABLE_INLINE_UPDATE		(WM_USER + 364)
#define WM_DYNTABLE_RBUTTONDOWN			(WM_USER + 365)
#define WM_DYNTABLE_LBUTTONDOWN			(WM_USER + 366)
#define WM_DYNTABLE_RBUTTONUP			(WM_USER + 367)
#define WM_DYNTABLE_LBUTTONUP			(WM_USER + 368)

#define BAR_CHANGED						(WM_USER + 11006)
#define GHD_BAR_DELETED					(WM_USER + 11009)

/////////////////////////////////////////////////////////////////////////////
// Font variable
enum{GANTT_S_FONT, GANTT_M_FONT, GANTT_L_FONT, GANTT_XL_FONT};

extern CFont ogSmallFonts_Regular_6;
extern CFont ogSmallFonts_Regular_7;
extern CFont ogSmallFonts_Regular_8;
extern CFont ogSmallFonts_Bold_7;
extern CFont ogMSSansSerif_Regular_8;
extern CFont ogMSSansSerif_Bold_8;
extern CFont ogCourier_Bold_10;
extern CFont ogCourier_Regular_10;
extern CFont ogCourier_Regular_8;
extern CFont ogCourier_Bold_8;
extern CFont ogCourier_Regular_9;

extern CFont ogTimesNewRoman_9;
extern CFont ogTimesNewRoman_12;
extern CFont ogTimesNewRoman_16;
extern CFont ogTimesNewRoman_30;

extern CFont ogScalingFonts[30];
extern int igFontIndex1;
extern int igFontIndex2;
extern int igDaysToRead;

void InitFont();
void DeleteBrushes();
void CreateBrushes();


enum{MS_SANS6, MS_SANS8, MS_SANS12, MS_SANS16, 
     MS_SANS6BOLD, MS_SANS8BOLD, MS_SANS12BOLD, MS_SANS16BOLD};

struct FONT_INDEXES
{
	int VerticalScale;
	int Chart;
	FONT_INDEXES(void)
	{VerticalScale=0;Chart=0;}
};


/////////////////////////////////////////////////////////////////////////////
// Color and brush variables

extern COLORREF ogColors[];
extern CBrush *ogBrushs[];

/////////////////////////////////////////////////////////////////////////////



struct TIMEFRAMEDATA
{
//	CCSTime StartTime;
//	CCSTime EndTime;
	CTime StartTime;
	CTime EndTime;
	TIMEFRAMEDATA(void)
	{StartTime=TIMENULL;EndTime=TIMENULL;}
};


class NoBroadcastSupport
{
public:
	NoBroadcastSupport();
	~NoBroadcastSupport();
private:
	void*	operator new(size_t);
	void	operator delete(void *);
};


/////////////////////////////////////////////////////////////////////////////
// application globals 
extern char pcgHome[4];
extern char pcgHelpPath[1024];
extern char pcgHome4[5];
extern char pcgTableExt[10];
extern bool bgNoScroll;
extern bool bgOnline;
extern bool bgIsButtonListMovable;
extern bool bgEinteilung;
class CInitialLoadDlg;
extern CInitialLoadDlg *pogInitialLoad;
extern CBackGround *pogBackGround;
extern bool bgEcChange;
extern bool bgAcChange;
extern bool bgNoAccept;
extern bool bgNoReturn;
/////////////////////////////////////////////////////////////////////////////
// IDs

//***** moved from "ccsglobl.h" by Damkerng 04/30/96 18:30
//***** moved from "tscall.cpp" by Pichate May 08,96 18:15
#define IDC_TIMESCALE       0x4001

//***** moved from "ccsglobl.h" by Damkerng 04/30/96 18:30
//***** moved from "vscale.cpp" by Pichate May 08,96 18:15
#define IDC_VERTICALSCALE   0x4002

//***** moved from "ccsglobl.h" by Damkerng 04/30/96 18:30
//***** moved from "gbar.cpp" by Pichate May 08,96 18:15
#define IDC_GANTTBAR        0x4003

//#define IDD_DIAGRAM         0x4004
#define IDD_CHART           0x4005
#define IDC_CHARTBUTTON     0x4006
#define IDD_GANTT           0x4007

#define IDC_PREV            0x4008
#define IDC_NEXT            0x4009
#define IDC_ARRIVAL         0x400a
#define IDC_DEPARTURE       0x400b
#define IDC_INPLACEEDIT		0x400c

#define IDC_CHARTBUTTON2     0x400d
#define IDC_CHARTBUTTON3     0x400e
#define IDC_CHARTCB		     0x400f
#define IDC_CHARTBUTTON4     0x4010
#define IDC_CHARTBUTTON5	 0x4011
#define IDC_CHARTBUTTON6	 0x4012
#define IDC_CHARTBUTTON7	 0x4013
#define IDC_AUTOCOVRELEASE	 0x4014
#define IDC_AUTOCOVDISCARD	 0x4015

#define IDD_GANTTCHART      0x4101


/////////////////////////////////////////////////////////////////////////////
// Messages

// Message sent from PrePlanTable to the parent window when closed
// Id 24-Sep-96
// Fix some bug here, since CCI diagram and Gate diagram shares the same message number.
// This surely will crash the machine or destroy our memory blocks if the user terminate
// the diagram with Alt-F4.
#define WM_STAFFTABLE_EXIT          (WM_APP + 20)	/* tables */
#define WM_GEQTABLE_EXIT            (WM_APP + 21)	/* tables */
#define WM_INFTABLE_EXIT            (WM_APP + 22)	/* tables */
#define WM_HWDIA_EXIT				(WM_APP + 23)	/* tables */
#define WM_FLIGHTDIA_EXIT           (WM_APP + 24)	/* tables */
#define WM_STAFFDIA_EXIT            (WM_APP + 25)	/* tables */
#define WM_CONFTABLE_EXIT           (WM_APP + 26)	/* tables */
#define WM_FLIGHTTABLE_EXIT         (WM_APP + 27)	/* tables */
#define WM_MAGNETICTABLE_EXIT       (WM_APP + 28)	/* tables */
#define WM_ATTENTIONTABLE_EXIT      (WM_APP + 29)	/* tables */
#define WM_CONFLICTTABLE_EXIT       (WM_APP + 30)	/* tables */
#define WM_LOCKWINDOWUPDATE         (WM_APP + 31)	/* tables */
#define WM_ZOLLTABLE_EXIT			(WM_APP + 32)	/* tables */
#define WM_SEARCH_EXIT				(WM_APP + 33)	/* tables */



#define SetWndStatAll(clStat, plWnd)\
	if(clStat=='1') {plWnd.ShowWindow(SW_SHOW);plWnd.EnableWindow(TRUE);}\
	else if(clStat=='0') {plWnd.ShowWindow(SW_SHOW);plWnd.EnableWindow(FALSE);}\
	else if(clStat=='-') plWnd.ShowWindow(SW_HIDE);

#define SetWndStatPrio_1(clStat1,clStat2, plWnd)\
	if(clStat1=='-'||clStat2=='-') plWnd.ShowWindow(SW_HIDE);\
	else if(clStat1=='0'||clStat2=='0') {plWnd.ShowWindow(SW_SHOW);plWnd.EnableWindow(FALSE);}\
	else if(clStat1=='1'||clStat2=='1') {plWnd.ShowWindow(SW_SHOW);plWnd.EnableWindow(TRUE);}

#define SetpWndStatAll(clStat, plWnd)\
	if(clStat=='1') {plWnd->ShowWindow(SW_SHOW);plWnd->EnableWindow(TRUE);}\
	else if(clStat=='0') {plWnd->ShowWindow(SW_SHOW);plWnd->EnableWindow(FALSE);}\
	else if(clStat=='-') plWnd->ShowWindow(SW_HIDE);


#define WIN_COORD(CHAR_COORD) ( (int) ( (CHAR_COORD) * 8.3 + 0.5 ) )


/////////////////////////////////////////////////////////////////////////////
// Drag Information Type

enum 
{
	DIT_FLIGHT,							// source: Flugdatenansicht
	DIT_AFLIGHT,						// source: Flugdatenansicht
	DIT_DFLIGHT,						// source: Flugdatenansicht
	DIT_GHSLIST,						// source: GhsList Target PremisPageGantt
	DIT_ANSICHT_GRP,					// source: Geometrie Gruppen Table targt dito
	DIT_DUTYFROMGANTT,					// Drop from this gantt to another point
	DIT_DUTYFROMTABLE,					// Drop from NoResourceList
	DIT_DUTYFROMDETAIL,					// Drop from Detail-Gantt of Rotatio-Diagram
	DIT_EXCHANGE_JOBS,					// Source: Gantt Destination: Gantt
	DIT_BASIC_SHIFT_FOR_DEMAND,			// Drag from Coverage Basic-Shift-Gantt
	DIT_POS_GANTT,						// ?
	DIT_SHIFTROSTER_BSD_TABLE,			// Drag from Shiftroster BSD Table
	DIT_DUTYROSTER_BSD_TABLE,			// Drag from Dutyroster BSD Table
	DIT_FROMGROUP_TODUTYROSTER,			// Drag from Group Table to Dutyroster
	DIT_FROMDUTYROSTER_TOGROUP			// Drag from Dutyroster to Group Table
};

// DIT 0 - 49



enum
{
	DLG_NEW,
	DLG_COPY,
	DLG_CHANGE
};


/*enum enumDDXTypes
{
    UNDO_CHANGE, 
    BC_CFG_INSERT, BC_CFG_CHANGE, CFG_CHANGE, CFG_INSERT,CFG_DELETE,

    // from here, there are defines for your project
	FLIGHT_CHANGE, FLIGHT_DELETE, FLIGHT_UPDATE,
	BC_FLIGHT_CHANGE, BC_FLIGHT_DELETE
};
*/
// end globals
/////////////////////////////////////////////////////////////////////////////


#endif //__CCSGLOBL_H__
#endif // !defined(AFX_CCSGLOBL_H__55D90F34_191F_11D1_82BE_0080AD1DC701__INCLUDED_)
