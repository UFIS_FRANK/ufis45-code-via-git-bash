#if !defined(AFX_RESULTTABLE_H__73AD2C01_0130_11D3_99C8_0000B43C16D8__INCLUDED_)
#define AFX_RESULTTABLE_H__73AD2C01_0130_11D3_99C8_0000B43C16D8__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// ResultTable.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CResultTable dialog

struct RESDATA
{
	CString Name;
	long Dura;
	long Dura2;
	CString OutName;

	RESDATA(void)
	{
		Name = "";
		Dura = 0L;
		Dura2 = 0L;
		OutName = "";
	}
};

enum {APLANE,WKGRP,COV_TAB};
class CResultTable : public CDialog
{
// Construction
public:
	CResultTable(CWnd* pParent = NULL,int ipType = APLANE,CString opOutHeader = "");   // standard constructor
	~CResultTable();
	
	CTable *pomTable;
	int imType;
	CCSPtrArray<RESDATA> omLines;
	CCSPtrArray<RESDATA> om2Lines;
	CMapStringToPtr omKeyMap;
	CMapStringToPtr omKey2Map;
// Dialog Data
	//{{AFX_DATA(CResultTable)
	enum { IDD = IDD_RESULT_TABLE };
	CButton	m_Frame;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CResultTable)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

	private:
		int imOutNameMaxSize;
		CString omOutHeader;
public:
	void ResetContent();
	void SetOutHeader(CString opOutHeader);
	void UpdateResults(int ipType,bool bpUseKey2Map = false);
	BOOL AddResults(char *pcpName,long ipDuration,int ipDura2=-1, CString opOutName = "",bool bpUseKey2Map = false);
	CString Format(RESDATA *prpRes,int ipType = -1);
	void ClearLines();
// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CResultTable)
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	afx_msg void OnSnapshot();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_RESULTTABLE_H__73AD2C01_0130_11D3_99C8_0000B43C16D8__INCLUDED_)
