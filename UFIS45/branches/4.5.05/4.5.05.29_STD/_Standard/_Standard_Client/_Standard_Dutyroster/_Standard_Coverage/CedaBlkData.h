// Class for Blocked Times
#ifndef _CEDABLKDATA_H_
#define _CEDABLKDATA_H_

#include <CCSCedaData.h>
#include <CCSPtrArray.h>

/////////////////////////////////////////////////////////////////////////////
// Record structure declaration

struct BlkDataStruct
{
	long	Urno;		// Unique Record Number
	char	Tabn[4];	// Blocked record table name
	char	Days[8];	// days of week
	long	Burn;		// blocked record URNO
	CTime	Nafr;		// not available from...
	CTime	Nato;		// not available to ...
	char	Tifr[5];	// from o'clock
	char	Tito[5];	// to o'clock	
	char	Resn[41];	// not available due to ...

	BlkDataStruct(void)
	{
		Urno = 0L;
		strcpy(Tabn,"");
		strcpy(Days,"");
		Burn = 0L;
		Nafr = TIMENULL;
		Nato = TIMENULL;
		strcpy(Tifr,"");
		strcpy(Tito,"");
		strcpy(Resn,"");
	}
};

typedef struct BlkDataStruct BLKDATA;

struct	BlkBlockedTimes
{
	CTime	Tifr;
	CTime	Tito;

	BlkBlockedTimes()
	{
		Tifr = TIMENULL;
		Tito = TIMENULL;
	}

	BlkBlockedTimes(const CTime& ropTifr,const CTime& ropTito)
	{
		Tifr = ropTifr;
		Tito = ropTito;
	}
};

typedef struct BlkBlockedTimes	BLKBLOCKEDTIMES;


/////////////////////////////////////////////////////////////////////////////
// Class declaration

class CedaBlkData: public CCSCedaData
{

// Attributes
public:
	CString GetTableName(void);

// Operations
public:
	CedaBlkData();
	~CedaBlkData();

	BOOL		ReadBlkData(const CTime& ropFrom,const CTime& ropTo);
	BLKDATA*	GetBlkByUrno(long lpUrno);
	int			GetBlocked(const CString& ropTable,long lpUrno,CCSPtrArray<BLKDATA>& ropBlocked);
	int			GetCountOfRecords();
	int			GetBlockedTimes(BLKDATA *prpBlocked,const CTime& ropStart,const CTime& ropEnd,CCSPtrArray<BLKBLOCKEDTIMES>& ropBlocked);
	int			GetBlockedTimes(const CString& ropTable,long lpUrno,const CTime& ropStart,const CTime& ropEnd,CCSPtrArray<BLKBLOCKEDTIMES>& ropBlocked);
	BOOL		IsBlockedAt(BLKDATA *prpBlocked,const CTime& ropStart,const CTime& ropEnd);
	bool		IsCompletelyBlocked(const CString& ropTable, long lpUrno, CTime opFrom, CTime opTo);
	void		ProcessBlkBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

private:
	BLKDATA*	AddBlkInternal(BLKDATA &rrpBlk);
	void		DeleteBlkInternal(BLKDATA *prpBlk);
	void		ConvertDatesToLocal(BLKDATA &rrpBlk);
	void		ConvertDatesToUtc(BLKDATA &rrpBlk);
	void		ClearAll();

private:
    CCSPtrArray<BLKDATA>	omData;
	CMapPtrToPtr			omUrnoMap;
	CMapStringToPtr			omTableMap;
};


extern CedaBlkData ogBlkData;
#endif _CEDABLKDATA_H_
