// Class for Validity Times
#ifndef _CEDAVALDATA_H_
#define _CEDAVALDATA_H_

#include <CCSCedaData.h>
#include <CCSPtrArray.h>

/////////////////////////////////////////////////////////////////////////////
// Record structure declaration

struct ValDataStruct
{
	long	Urno;		// Unique Record Number
	char	Tabn[4];	// valid record table name
	char	Freq[8];	// days of week
	long	Uval;		// valid record URNO
	CTime	Vafr;		// valid from...
	CTime	Vato;		// valid to ...
	char	Tifr[5];	// from o'clock
	char	Tito[5];	// to o'clock	

	ValDataStruct(void)
	{
		Urno = 0L;
		strcpy(Tabn,"");
		strcpy(Freq,"");
		Uval = 0L;
		Vafr = TIMENULL;
		Vato = TIMENULL;
		strcpy(Tifr,"");
		strcpy(Tito,"");
	}
};

typedef struct ValDataStruct VALDATA;

struct	ValidTimes
{
	CTime	Tifr;
	CTime	Tito;

	ValidTimes()
	{
		Tifr = TIMENULL;
		Tito = TIMENULL;
	}

	ValidTimes(const CTime& ropTifr,const CTime& ropTito)
	{
		Tifr = ropTifr;
		Tito = ropTito;
	}
};

typedef struct ValidTimes	VALIDTIMES;


/////////////////////////////////////////////////////////////////////////////
// Class declaration

class CedaValData: public CCSCedaData
{

// Attributes
public:
	CString GetTableName(void);

// Operations
public:
	CedaValData();
	~CedaValData();

	BOOL		ReadValData(const CTime& ropFrom,const CTime& ropTo);
	VALDATA*	GetValByUrno(long lpUrno);
	int			GetValid(const CString& ropTable,long lpUrno,CCSPtrArray<VALDATA>& ropValid);
	int			GetCountOfRecords();
	int			GetValidTimes(VALDATA *prpValid,const CTime& ropStart,const CTime& ropEnd,CCSPtrArray<VALIDTIMES>& ropValid);
	int			GetValidTimes(const CString& ropTable,long lpUrno,const CTime& ropStart,const CTime& ropEnd,CCSPtrArray<VALIDTIMES>& ropValid);
	BOOL		IsValidAt(VALDATA *prpValid,const CTime& ropStart,const CTime& ropEnd);
	bool		IsCompletelyValid(const CString& ropTable, long lpUrno, CTime opFrom, CTime opTo);
	void		ProcessValBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

private:
	VALDATA*	AddValInternal(VALDATA &rrpVal);
	void		DeleteValInternal(VALDATA *prpVal);
	void		ConvertDatesToLocal(VALDATA &rrpVal);
	void		ClearAll();

private:
    CCSPtrArray <VALDATA>	omData;
	CMapPtrToPtr			omUrnoMap;
	CMapStringToPtr			omTableMap;
};


extern CedaValData ogValData;
#endif _CEDAVALDATA_H_
