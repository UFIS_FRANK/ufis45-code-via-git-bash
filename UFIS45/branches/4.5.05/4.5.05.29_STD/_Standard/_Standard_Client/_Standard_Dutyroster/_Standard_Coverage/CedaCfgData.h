#ifndef _CCFGD_H_
#define _CCFGD_H_

#include <CCSCedadata.h>
#include <CCSGlobl.h>
#include <CCSPtrArray.h>


#define MON_STAFF_BOARD_STRING		"STAFFBOARD"
#define MON_NORESOURCE_STRING		"NORESOURCE"
#define MON_FLIGHTCHANGES_STRING	"FLIGHTCHANGES"
#define MON_FLIGHTSCHEDULE_STRING	"FLIGHTSCHEDULE"
#define MON_COVERAGE_STRING			"COVERAGE"
#define MON_RULES_STRING			"RULES"
#define MON_COUNT_STRING			"MONITORCOUNT"
#define MON_DUTYROSTER_STRING		"DUTYROSTER"
/////////////////////////////////////////////////////////////////////////////
// Record structure declaration
struct RAW_VIEWDATA
{
	char Ckey[40];
	char Name[100];
	char Type[40];
	char Page[40];
	char Values[2001];
	RAW_VIEWDATA()
	{ memset(this,'\0',sizeof(*this));}
};
struct VIEW_TEXTDATA
{
	CString Page;							// e.g. Rank, Pool, Shift
	CCSPtrArray <CString> omValues;					// e.g. Page=Shift: F1,F50,N1 etc.
};

struct VIEW_TYPEDATA
{
	CString Type;							// e.g. Filter, Group, Sort
	CCSPtrArray <VIEW_TEXTDATA> omTextData; // necessary only with filters
	CCSPtrArray<CString> omValues;					// values only relevant if Type != Filter
};
struct VIEW_VIEWNAMES
{
	CString ViewName;						// e.g. <Default>, Heute, Test etc.
	CCSPtrArray<VIEW_TYPEDATA> omTypeData;
};
struct VIEWDATA
{
	CString Ckey;							// e.g. Staffdia, CCI-Chart etc.
	CCSPtrArray<VIEW_VIEWNAMES> omNameData;
};
//struct CfgDataStruct {
struct CFGDATA {
    // Data fields from table CFGCKI for whatif-rows
    long    Urno;           // Unique Record Number of CFGCKI
    char    Appn[34];       // name of application
    char    Ctyp[34];       // Type of Row; in Whaif constant string "WHAT-IF"
	char    Ckey[34];		// Name of what-if row
    CTime	Vafr;           // Valid from
    CTime	Vato;           // Valid to
	char	Text[2001];		// Parameter string
	char	Pkno[34];		// Staff-/User-ID
    int	    IsChanged;		// Is changed flag

	CFGDATA(void) 
	{ memset(this,'\0',sizeof(*this));
	  sprintf(Appn,"CCS_%s", ogAppName);
	  //strcpy(Vafr, "19960101000000");
	  //strcpy(Vato, "20351231000000");
	  IsChanged=DATA_UNCHANGED;}

};	

struct USERSETUPDATA
{
	CString DET1;	// Demand timer 1 outside demand calculation:			5	minutes
	CString	DET2;	// Demand timer 2 during demand calculation :			1	minute
	CString	NAME;	// Name configuration for display
	CString	EVSO;	// Evaluation sort order (0 = By rotation, 1 = By Demand)
	CString	NDFC;	// No demand flights color
	CString	RESO;	// Zombie (for compilation only)

	USERSETUPDATA(void)
	{
		DET1 = "300";
		DET2 = "60";	
		EVSO = "0";
		NDFC.Format("%ld",GRAY);
		NAME = "";
	}
};

typedef struct CFGDATA SETUPDATA;
//typedef struct CfgDataStruct CFGDATA;

// the broadcast CallBack function, has to be outside the CedaCfgData class
void ProcessCfgCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

/////////////////////////////////////////////////////////////////////////////
// Class declaration

class CedaCfgData: public CCSCedaData
{
// Attributes
public:
    CCSPtrArray<CFGDATA>	omData;
    CCSPtrArray<CFGDATA>	omSetupData;
	CCSPtrArray<VIEWDATA>	omViews;
    CMapStringToPtr			omCkeyMap;
    CMapPtrToPtr			omUrnoMap;
    CMapPtrToPtr			omSetupUrnoMap;
	USERSETUPDATA			rmUserSetup;
	CFGDATA					rmMonitorSetup;

	// this is the name of a dummy user for default values
	static const char* pomDefaultName;

// Operations
public:
    CedaCfgData();
    //@ManMemo: Destructor, Unregisters the CedaCfgData object from DataDistribution
	~CedaCfgData();
	
	void SetTableName(CString opTableName)
	{
		strcpy(pcmTableName, opTableName.GetBuffer(0));
	}
	char pcmListOfFields[2048];
	//@ManMemo: Create the Request for What-If
	bool CreateCfgRequest(const CFGDATA *prpCfgData);
    //@ManMemo: Read all Cfg from database at program start
	bool ReadCfgData();
	int	 GetMonitorForWindow(CString opWindow);
	int	 GetMonitorCount();
	bool ReadMonitorSetup();
	void GetDefaultConflictSetup(CStringArray &opLines);

	//@ManMemo: Delete a Cfg
	bool ChangeCfgData(CFGDATA *prpCfg);
	//@ManMemo: Delete the Cfg
	bool DeleteCfg(long lpUrno);
	//@ManMemo: Adds a Cfg to omData and to the Maps
	bool AddCfg(CFGDATA *prpCfg);
	//@ManMemo: Makes Database-Actions Insert/Update/Delete
	bool SaveCfg(CFGDATA *prpCfg);	
    //@ManMemo: Prepare the data, not used for the moment
	void PrepareCfgData(CFGDATA *prpCfg);
	//@ManMemo: Insert staff data (const CFGDATA *prpCfgData);    // used in PrePlanTable only

	BOOL InterpretSetupString(CString popSetupString, USERSETUPDATA *prpSetupData);
    //@ManMemo: Update cfg data 
    bool UpdateCfg(const CFGDATA *prpCfgData);    // used in PrePlanTable only
	void ProcessCfgBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
	bool InsertCfg(const CFGDATA *prpCfgData);
	long  GetUrnoById(char *pclWiid);
	BOOL  GetIdByUrno(long lpUrno,char *pcpWiid);

	CFGDATA  * GetCfgByUrno(long lpUrno);
	void SetCfgData();
	void MakeCurrentUser();
	void ClearAllViews();
	void PrepareViewData(CFGDATA *prpCfg);
	VIEWDATA * FindViewData(CFGDATA *prlCfg);
	VIEW_VIEWNAMES * FindViewNameData(RAW_VIEWDATA *prpRawData);
	VIEW_TYPEDATA * FindViewTypeData(RAW_VIEWDATA *prpRawData);
	VIEW_TEXTDATA * FindViewTextData(RAW_VIEWDATA *prpRawData);
	BOOL MakeRawViewData(CFGDATA *prpCfg, RAW_VIEWDATA * prpRawData);
	void MakeViewValues(char *pspValues, CCSPtrArray<CString> *popValues);
	void UpdateViewForDiagram(CString opDiagram, VIEWDATA &prpView,CString opViewName);
	void DeleteViewFromDiagram(CString opDiagram, CString olView);
	bool ReadConflicts(CStringArray &opLines);
	bool SaveConflictData(CFGDATA *prpCfg);

	bool ReadColors(const CString& opUserName,CStringArray &opLines);
	void CfgRecordFromColor(const CString& popUsername,const CString& popRecord,CFGDATA& popCfgData);
	void GetDefaultColorSetup(CStringArray &opLines);
	bool ResetToDefault(void);
	bool SaveColorData(const CString& ropName,CFGDATA *prpCfg);

private:
    BOOL CfgExist(long lpUrno);
    bool InsertCfgRecord(const CFGDATA *prpCfgData);
    bool UpdateCfgRecord(const CFGDATA *prpCfgData);
};

extern CedaCfgData ogCfgData;
#endif
