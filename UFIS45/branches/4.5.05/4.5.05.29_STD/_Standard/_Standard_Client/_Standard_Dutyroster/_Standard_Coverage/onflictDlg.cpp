// onflictDlg.cpp : implementation file
//

#include <stdafx.h>
#include <CCSGlobl.h>
#include <CCSEdit.h>
#include <onflictDlg.h>
#include <CedaFlightData.h>
//#include "CedaDsrData.h"
#include <DataSet.h>
//#include "CedaGhdData.h"
#include <JobDlg.h>
#include <PrivList.h>
//#include "DsrGhds.h"
 
#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// ConflictDlg dialog

static int CompareConflictTimes(const CONFLICT_LINEDATA **p1, const CONFLICT_LINEDATA **p2);

ConflictDlg::ConflictDlg(CWnd* pParent, bool bpCxxAdd, CedaFlightData *popFlightData, ConflictCheck *popConflictData,CViewer *popCallerView)
	: CDialog(ConflictDlg::IDD, pParent)
{
	pomConflictData = popConflictData;
	pomFlightData = popFlightData;
//	pomGhdData = popGhdData;
	m_nDialogBarHeight;
	bmCxxAdd = bpCxxAdd;
	isCreated = FALSE;
    CDialog::Create(ConflictDlg::IDD,pParent);
	pomParent = pParent;
	pomCallerView = popCallerView;
	pomTable = new CTable;
	pomTable->tempFlag = 2;
	pomTable->SetSelectMode(0);
	CRect rect;
	GetClientRect(&rect);
	rect.OffsetRect(0, m_nDialogBarHeight);
   rect.InflateRect(1, 1);     // hiding the CTable window border
   pomTable->SetTableData(this, rect.left, rect.right, rect.top, rect.bottom, ::GetSysColor(COLOR_WINDOWTEXT), ::GetSysColor(COLOR_WINDOW), ::GetSysColor(COLOR_HIGHLIGHTTEXT), ::GetSysColor(COLOR_HIGHLIGHT), &ogMSSansSerif_Regular_8);

	isCreated = TRUE;
	UpdateDisplay();

	LoadList();
	ShowWindow(SW_SHOWNORMAL);
	//{{AFX_DATA_INIT(ConflictDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}

ConflictDlg::~ConflictDlg()
{
	if(pogJobDlg!=NULL)
	{
		CWnd *polWnd = pogJobDlg->GetOwner();
		if((CWnd *)this == polWnd)
		{
			pogJobDlg->SetOwner(NULL);
			pogJobDlg->SetCallerViewer(NULL);
		}		
	}
	delete pomTable;
	Clear();

}

void ConflictDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(ConflictDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(ConflictDlg, CDialog)
	//{{AFX_MSG_MAP(ConflictDlg)
	ON_BN_CLICKED(IDC_DELETEALL, OnDeleteall)
	ON_BN_CLICKED(IDC_VIEW, OnView)
	ON_WM_SIZE()
	ON_MESSAGE(WM_TABLE_LBUTTONDBLCLK, OnTableLButtonDblClk)
	ON_MESSAGE(WM_TABLE_RBUTTONDOWN, OnTableRButtonDown)
	ON_MESSAGE(WM_TABLE_DRAGBEGIN, OnTableDragBegin)
	ON_COMMAND(40, OnMenuConflictDelete)
	ON_WM_CREATE()
	ON_WM_DESTROY()
	//}}AFX_MSG_MAP
    ON_MESSAGE(WM_TABLE_LBUTTONDOWN, OnTableDragBegin)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// ConflictDlg message handlers

void ConflictDlg::OnDeleteall() 
{
	pomTable->ResetContent();
	pomConflictData->DeleteAllData();
	ogDataSet.CheckAttention();
	omLines.DeleteAll();
	//DestroyWindow();
}

void ConflictDlg::OnView() 
{
//	int ilitemID;
/*****************
	if((ilitemID = pomTable->GetCTableListBox()->GetCurSel()) != LB_ERR)
	{
		CONFLICT_LINEDATA rlLine;
		rlLine = omLines[ilitemID];

		if( pogJobDlg == NULL )
			pogJobDlg = new CJobDlg(NULL);
		else
			pogJobDlg->SetActiveWindow();

		GHDDATA *prlGhd = pomGhdData->GetGhdByUrno(rlLine.GhdUrno);
		if(prlGhd != NULL)
		{
			long UrnoA = 0, UrnoD = 0;
			FLIGHTDATA *prlFlightA = NULL;
			FLIGHTDATA *prlFlightD = NULL;
			if(strcmp(prlGhd->Drti, "A") == 0)
			{
				prlFlightA = pomFlightData->GetFlightByUrno(prlGhd->Fkey); 
				if(prlFlightA != NULL)
				{
					UrnoA = prlFlightA->Urno;
				}
			}
			if(strcmp(prlGhd->Drti, "D") == 0)
			{
				prlFlightA = pomFlightData->GetFlightByUrno(prlGhd->Fkey); 
				if(prlFlightA != NULL)
				{
					if(prlFlightA != NULL)
					{
						UrnoA = prlFlightA->Urno;
					}
					prlFlightD = pomFlightData->GetJoinFlight(prlFlightA);
					if(prlFlightD != NULL)
					{
						UrnoD = prlFlightD->Urno;
					}
				}
			}
			if(strcmp(prlGhd->Drti, "B") == 0)
			{
				prlFlightD = pomFlightData->GetFlightByUrno(prlGhd->Fkey); 
				if(prlFlightD != NULL)
				{
					UrnoD = prlFlightD->Urno;
				}
			}

//FSCX			pogJobDlg->InitFlightFields((CWnd*)this, pomFlightData, pomGhdData, UrnoA, UrnoD,pomCallerView);
		}
	}
	********************/
}
void ConflictDlg::LoadList()
{

}

void ConflictDlg::Clear()
{
	omLines.DeleteAll();
}

void ConflictDlg::UpdateDisplay()
{													 
	int ilCount = 0;
   pomTable->SetHeaderFields(LoadStg(IDS_STRING1490));
   pomTable->SetFormatList("10|5|3|100");


	Clear();

	if(bmCxxAdd == true)
	{
		CWnd *polWnd = GetDlgItem(IDC_DELETEALL);
		if(polWnd != NULL)
		{
			polWnd->ShowWindow(SW_HIDE);
		}
		int ilCflCount = 0;
		ilCount = pomConflictData->GetDataSize();
		SetWindowText(LoadStg(IDS_STRING1004));
		for(int i = 0; i < ilCount; i++)
		{
			CONFLICTENTRY *prlConfl = pomConflictData->GetDataAt(i); //&pomConflictData->omData[i];
			/*
			if(pomConflictData->omData[i].Type == CFI_F_ARR_NOMORE_CXX ||
			   pomConflictData->omData[i].Type == CFI_F_ARR_CXX ||
			   pomConflictData->omData[i].Type == CFI_F_DEP_NOMORE_CXX ||
			   pomConflictData->omData[i].Type == CFI_F_DEP_CXX)
			   */
			if(prlConfl->Type == CFI_F_ARR_NOMORE_CXX ||
			   prlConfl->Type == CFI_F_ARR_CXX ||
			   prlConfl->Type == CFI_F_DEP_NOMORE_CXX ||
			   prlConfl->Type == CFI_F_DEP_CXX)
			   
			{
				//CONFLICTENTRY *prlConfl = &pomConflictData->omData[i];// = new DSRDATA;
				CONFLICT_LINEDATA rlLine;
				rlLine.erroNo = prlConfl->erroNo;
				rlLine.FlightUrno = prlConfl->FlightUrno;
				rlLine.Fkey = prlConfl->Fkey;
				rlLine.GhdKey = prlConfl->GhdKey;
				rlLine.GhdUrno = prlConfl->GhdUrno;
				rlLine.DsrUrno = prlConfl->DsrUrno;
				rlLine.Time = prlConfl->Time;
				rlLine.Type = prlConfl->Type;
				strcpy(rlLine.Esnm, prlConfl->Esnm);
				strcpy(rlLine.Sfca, prlConfl->Sfca);
				strcpy(rlLine.Text, prlConfl->text);
				CreateLine(&rlLine);
				ilCflCount++;
			}
		}
		if(ilCflCount == 0)
		{
			SetWindowText(LoadStg(IDS_STRING1003));
		}
	}
	else
	{
		ilCount = pomConflictData->GetDataSize();
		for(int i = 0; i < ilCount; i++)
		{
			CONFLICTENTRY *prlConfl = pomConflictData->GetDataAt(i);
			CONFLICT_LINEDATA rlLine;
			rlLine.erroNo = prlConfl->erroNo;
			rlLine.FlightUrno = prlConfl->FlightUrno;
			rlLine.Fkey = prlConfl->Fkey;
			rlLine.GhdKey = prlConfl->GhdKey;
			rlLine.GhdUrno = prlConfl->GhdUrno;
			rlLine.DsrUrno = prlConfl->DsrUrno;
			rlLine.Time = prlConfl->Time;
			rlLine.Type = prlConfl->Type;
			strcpy(rlLine.Esnm, prlConfl->Esnm);
			strcpy(rlLine.Sfca, prlConfl->Sfca);
			strcpy(rlLine.Text, prlConfl->text);
			CreateLine(&rlLine);
		}
	}
	omLines.Sort(CompareConflictTimes);

	pomTable->ResetContent();
	ilCount = omLines.GetSize();
	for(int i = 0; i < ilCount; i++) 
	{
		pomTable->AddTextLine(Format(&omLines[i]), &omLines[i]);
		//pomTable->SetTextLineColor(i, RED, WHITE);
	}
	pomTable->DisplayTable();

}

CString ConflictDlg::Format(CONFLICT_LINEDATA *prpConfl)
{
   CString s;

   FLIGHTDATA *prlFlight = pomFlightData->GetFlightByUrno(prpConfl->Fkey);
   if(prlFlight != NULL)
   {
		s =  CString(prlFlight->Flno) + "|";
   }
	s+=  prpConfl->Time.Format("%H%M") + "|";
	s+=  CString(prpConfl->Esnm) + "|";
	s+=  CString(prpConfl->Text);

	return s;
}

void ConflictDlg::CreateLine(CONFLICT_LINEDATA *prpConfl)
{
	CONFLICT_LINEDATA rlConfl;
	rlConfl = *prpConfl;
/*
	long Rtnr;
	char Fkey[18];	
	long GhdKey;
	long GhdUrno;
	long DsrUrno;
	CTime Time;
	int  Type;
	char Esnm[4]; 
	char Sfca[4];
	char Text[1024];
*/
	if(rlConfl.DsrUrno != 0)
	{
		/**********
		DSRDATA *prlDsr = ogDsrData.GetDsrByUrno(rlConfl.DsrUrno);
		if(prlDsr != NULL)
		{
			strcpy(rlConfl.Esnm, prlDsr->Esnm);
		}
		***********/
	}
	if(rlConfl.GhdUrno != 0)
	{
		/*************
		GHDDATA *prlGhd = pomGhdData->GetGhdByUrno(rlConfl.GhdUrno);
		if(prlGhd != NULL)
		{
			if(strcmp(prlGhd->Dtyp, "B") != 0)
			{
				FLIGHTDATA *prlFlight = pomFlightData->GetFlightByUrno(prlGhd->Fkey);
				if(prlFlight != NULL)
				{
					CString olFkey = CString(prlFlight->Flno);
					rlConfl.Fkey = prlFlight->Urno;
				}
			}
		}
		************/
	}
	else if(rlConfl.FlightUrno != 0)
	{
		FLIGHTDATA *prlFlight = pomFlightData->GetFlightByUrno(rlConfl.FlightUrno);
		if(prlFlight != NULL)
		{
			if(prlFlight->Tifd != TIMENULL)
			{
				rlConfl.Fkey = prlFlight->Urno;
				rlConfl.Time = prlFlight->Tifd;
			}
			else if(prlFlight->Tifa != TIMENULL)
			{
				rlConfl.Fkey = prlFlight->Urno;
				rlConfl.Time = prlFlight->Tifa;
			}
		}
	}
	omLines.NewAt(omLines.GetSize(), rlConfl);
}

static int CompareConflictTimes(const CONFLICT_LINEDATA **p1, const CONFLICT_LINEDATA **p2)
{
	return ((**p1).Time == (**p2).Time)? 0:
		((**p1).Time > (**p2).Time)? 1: -1;
}

BOOL ConflictDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

    // calculate the window height
    CRect rect;
    GetClientRect(&rect);
    m_nDialogBarHeight = rect.bottom - rect.top;

    // extend dialog window to current screen width
    rect.left = 200;
    rect.top = 200;
    rect.right = rect.left + 650;
//    rect.right = ::GetSystemMetrics(SM_CXSCREEN) - 170;
    rect.bottom = ::GetSystemMetrics(SM_CYSCREEN) - 200;
    MoveWindow(&rect);
	HICON h_icon = AfxGetApp()->LoadIcon(IDI_UFIS);
	SetIcon(h_icon, FALSE);

	
	CWnd *polWnd = GetDlgItem(IDCANCEL);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING61214));
	}
	polWnd = GetDlgItem(IDC_DELETEALL);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING61260));
	}
	
	SetWindowText(LoadStg(IDS_STRING1474));

	// TODO: Add extra initialization here
	CUIntArray 	olCtrlIDs;
	olCtrlIDs.Add((UINT)IDC_DELETEALL);
	
	CString olSecIDs  = "CONFLICT_DLG_DeleteList";
	int ilNoOfFlds = olCtrlIDs.GetSize();

	for(int ilLc = 0;ilLc<ilNoOfFlds; ilLc++)
	{
		UINT ilCtrlID = olCtrlIDs.GetAt(ilLc);
		CString olSecID = GetListItem(olSecIDs,ilLc+1,FALSE,',');
		CWnd *polWnd = GetDlgItem(ilCtrlID);
		if(polWnd!=NULL)
		{
			switch(ogPrivList.GetStat(olSecID))
			{
			case '1':
				polWnd->EnableWindow(TRUE);
				polWnd->ShowWindow(SW_SHOW);
				break;
			case '0':
				polWnd->EnableWindow(FALSE);
				polWnd->ShowWindow(SW_SHOW);
				break;
			case '-':
				//polWnd->EnableWindow(FALSE);
				polWnd->ShowWindow(SW_HIDE);
				break;
			default:
				break;
			}
		}
		else
		{
			printf("Error! No such window object!");
			// Error no such window object!!
		}
	}
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

/*
BOOL ConflictDlg::DestroyWindow() 
{
	// TODO: Add your specialized code here and/or call the base class
	BOOL rc;
	rc = CDialog::DestroyWindow();
	return rc;
}
*/

void ConflictDlg::OnMenuConflictDelete()
{
/**************************
	CListBox *polListBox = pomTable->GetCTableListBox();
	if(polListBox == NULL)
	{
		return;
	}
	int ilItemID;
	if ((ilItemID = polListBox->GetCurSel()) == LB_ERR)
		return;

	CONFLICT_LINEDATA rlLine;
	rlLine = omLines[ilItemID];
	GHDDATA *prlGhd = pomGhdData->GetGhdByUrno(rlLine.GhdUrno);
//	DSRDATA *prlDsr = ogDsrData.GetDsrByUrno(rlLine.DsrUrno);
	if(prlGhd != NULL )
	{
		pomConflictData->DeleteConflict(prlGhd, prlDsr, rlLine.Type);
		pomTable->DeleteTextLine(ilItemID);
		omLines.DeleteAt(ilItemID);
	}
	else
	{
		pomConflictData->DeleteConflict(rlLine.Fkey, rlLine.Type);
		pomTable->DeleteTextLine(ilItemID);
		omLines.DeleteAt(ilItemID);
	}

	return;
******************************/
}

LONG ConflictDlg::OnTableRButtonDown(UINT wParam, LONG lParam)
{
	int ilitemID;

	
	CPoint point = (CPoint)lParam;
	ilitemID = (int)wParam;
	CONFLICT_LINEDATA rlLine;
	rlLine = omLines[ilitemID];
	if(ogPrivList.GetStat("CONFLICT_DLG_DELSERVICE")=='-')
	{
		return 0L;
	}
	CMenu menu;
	menu.CreatePopupMenu();
	for (int i = menu.GetMenuItemCount(); --i >= 0;)
	  menu.RemoveMenu(i, MF_BYPOSITION);
	menu.AppendMenu(MF_STRING,40, LoadStg(IDS_STRING1002));	// confirm job
	pomTable->ClientToScreen(&point);
	if(ogPrivList.GetStat("CONFLICT_DLG_DELSERVICE")=='0')
	{
		int ilMenuItemID = menu.GetMenuItemID(0);
		int ilState=menu.EnableMenuItem(ilMenuItemID,MF_GRAYED);	// disable menu
	}//ScreenToClient(&point);
	menu.TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, point.x, point.y, this, NULL);
	return 0L;
}

LONG ConflictDlg::OnTableLButtonDblClk(UINT wParam, LONG lParam)
{
//	int ilitemID;
/******************
	if(ogPrivList.GetStat("CONFLICT_DLG_DBL_CLICK")!='1')
	{
		return 0L;
	}

	ilitemID = (int)wParam;
	CONFLICT_LINEDATA rlLine;
	rlLine = omLines[ilitemID];

	GHDDATA *prlGhd = pomGhdData->GetGhdByUrno(rlLine.GhdUrno);
	if(prlGhd != NULL)
	{
		CONFLICT_LINEDATA rlLine;
		rlLine = omLines[ilitemID];

		if( pogJobDlg == NULL )
			pogJobDlg = new CJobDlg(NULL);
		else
			pogJobDlg->SetActiveWindow();

		long UrnoA = 0, UrnoD = 0;
		FLIGHTDATA *prlFlightA = NULL;
		FLIGHTDATA *prlFlightD = NULL;
		if(strcmp(prlGhd->Drti, "A") == 0)
		{
			prlFlightA = pomFlightData->GetFlightByUrno(prlGhd->Fkey); 
			if(prlFlightA != NULL)
			{
				UrnoA = prlFlightA->Urno;
			}
		}
		if(strcmp(prlGhd->Drti, "D") == 0)
		{
			prlFlightA = pomFlightData->GetFlightByUrno(prlGhd->Fkey); 
			if(prlFlightA != NULL)
			{
				if(prlFlightA != NULL)
				{
					UrnoA = prlFlightA->Urno;
				}
				prlFlightD = pomFlightData->GetJoinFlight(prlFlightA);
				if(prlFlightD != NULL)
				{
					UrnoD = prlFlightD->Urno;
				}
			}
		}
		if(strcmp(prlGhd->Drti, "B") == 0)
		{
			prlFlightD = pomFlightData->GetFlightByUrno(prlGhd->Fkey); 
			if(prlFlightD != NULL)
			{
				UrnoD = prlFlightD->Urno;
			}
		}

//FSCX		pogJobDlg->InitFlightFields((CWnd*)this, pomFlightData, pomGhdData, UrnoA, UrnoD,pomCallerView);
	}
	else
	{
		CONFLICT_LINEDATA rlLine;
		rlLine = omLines[ilitemID];

		if( pogJobDlg == NULL )
			pogJobDlg = new CJobDlg(NULL);
		else
			pogJobDlg->SetActiveWindow();

		long UrnoA = 0, UrnoD = 0;
		FLIGHTDATA *prlF = pomFlightData->GetFlightByUrno(rlLine.FlightUrno);
		if(prlF != 0)
		{
			if(strcmp(prlF->Adid, "A") == 0)
			{
				UrnoA = prlF->Urno;
				FLIGHTDATA *prlFlightD = pomFlightData->GetJoinFlight(prlF);
				if(prlFlightD != NULL)
				{
					UrnoD = prlFlightD->Urno;
				}
			}
			else if(strcmp(prlF->Adid, "D") == 0)
			{
				UrnoD = prlF->Urno;
				FLIGHTDATA *prlFlightA = pomFlightData->GetJoinFlight(prlF);
				if(prlFlightA != NULL)
				{
					UrnoA = prlFlightA->Urno;
				}
			}
		}
//FSCX		pogJobDlg->InitFlightFields((CWnd*)this, pomFlightData, pomGhdData, UrnoA, UrnoD,pomCallerView);
	}
	************************/
	return 0L;
}

LONG ConflictDlg::OnTableDragBegin(UINT wParam, LONG lParam)
{
	int ilitemID;

	ilitemID = (int)wParam;
	CONFLICT_LINEDATA rlLine;
	if(ilitemID < (omLines.GetSize()-1))
	{
		return 0L;
	}
	rlLine = omLines[ilitemID];

	return 0L;
}

void ConflictDlg::OnCancel() 
{
	//DestroyWindow();
	ShowWindow(SW_HIDE);
}

void ConflictDlg::OnSize(UINT nType, int cx, int cy) 
{
	if(isCreated != FALSE)
	{
		CDialog::OnSize(nType, cx, cy);
	
		if (nType != SIZE_MINIMIZED)
		{
			pomTable->SetPosition(1, cx+1, m_nDialogBarHeight, cy+1);
		}
	}
}

