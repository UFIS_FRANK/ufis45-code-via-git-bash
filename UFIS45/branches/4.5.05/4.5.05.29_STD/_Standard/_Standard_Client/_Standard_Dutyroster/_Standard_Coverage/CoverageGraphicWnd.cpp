// CoverageGraphicWnd.cpp: Implementierungsdatei
//
 
#include <stdafx.h>
#include <CCSEdit.h>
#include <CCSGlobl.h>
#include <Coverage.h>
#include <CCSPtrArray.h>
#include <CoverageGraphicWnd.h>
#include <CedaDemData.h> 
#include <CedaSdtData.h>
#include <CedaPfcData.h>
#include <CedaPerData.h>
#include <CedaValData.h>
#include <CedaBlkData.h>
#include <CedaSgmData.h>
#include <CovDiagram.h>
#include <CoverageStatistics.h>
#include <ResultTable.h>


#include <CedaBasicData.h> 

#include <WINSPOOL.H>
#include <process.h>
#include <iomanip.h>
#include <iostream.h>
#include <fstream.h>
#include <algorithm>


#include <math.h>
#define FACTOR 1
#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

int XXX;
#define INCH 0.284
#define GetX(time)	(pomTimeScale->GetXFromTime(time))// + imVerticalScaleWidth)
#define IsBetween(val, start, end)  ((start) <= (val) && (val) <= (end))
#define IsOverlapped(start1, end1, start2, end2)    ((start1) <= (end2) && (start2) <= (end1))

#define MMX(x)    ((int)(MulDiv((x),imLogPixelsX, 72)*INCH))
#define MMY(x)    ((int)(MulDiv((x),imLogPixelsY, 72)*INCH))

struct Peak
{
	int xStart;
	int xEnd;
	int xMax;
	int yMax;
	float relHeight;
};

static int CompareDemTimes(const DEMDATA **e1, const DEMDATA **e2)
{
	CCS_TRY
    int ilCompareResult = 0;

	if((**e1).Debe == (**e2).Debe)
	{
		ilCompareResult = 0;
	}
	else
	{
		if((**e1).Debe > (**e2).Debe)
		{
			ilCompareResult = 1;
		}
		else
		{
			ilCompareResult = -1;
		}
	}
	return ilCompareResult;
	CCS_CATCH_ALL
	return 0;
}

static BOOL IsFlightDependend(const DEMDATA *polDem)
{
	switch(polDem->Dety[0])
	{
	case '0' :
	case '1' :
	case '2' :
		return TRUE;
	break;
	default:
		return FALSE;
	}
}

static BOOL IsCommonCheckin(const DEMDATA *polDem)
{
	if (polDem->Dety[0] == '6')
		return TRUE;
	else
		return FALSE;
}

static int CompareDemTimesAndFltno(const DEMDATA **e1, const DEMDATA **e2)
{
	CCS_TRY
    int ilCompareResult = 0;

	if (!IsFlightDependend(*e1))	// FID or CCI
	{
		if (IsFlightDependend(*e2))	
		{
			return 1;
		}

		if (IsCommonCheckin(*e1))	
		{
			if (!IsCommonCheckin(*e2))
			{
				return 1;
			}
		}
		else	// FID
		{
			if (IsCommonCheckin(*e2))	// CCI
			{
				return -1;
			}
		}
	}
	else	// Flight dependend 
	{
		if (!IsFlightDependend(*e2))	// FID or CCI
		{
			return -1;
		}
	}

	if ((**e1).Ouri == (**e2).Ouri && (**e1).Ouro == (**e2).Ouro)
	{
		if((**e1).Debe == (**e2).Debe)
		{
			ilCompareResult = 0;
		}
		else
		{
			if((**e1).Debe > (**e2).Debe)
			{
				ilCompareResult = 1;
			}
			else
			{
				ilCompareResult = -1;
			}
		}
	}
	else if((**e1).Debe == (**e2).Debe)
	{
		ilCompareResult = 0;
	}
	else
	{
		if((**e1).Debe > (**e2).Debe)
		{
			ilCompareResult = 1;
		}
		else
		{
			ilCompareResult = -1;
		}
	}

	return ilCompareResult;
	CCS_CATCH_ALL
	return 0;
}

static int CompareSSdtTimes(const SPLITTEDSDT **e1, const SPLITTEDSDT **e2)
{
	CCS_TRY
    int ilCompareResult = 0;

	if((**e1).Start == (**e2).Start)
	{
		ilCompareResult = 0;
	}
	else
	{
		if((**e1).Start > (**e2).Start)
		{
			ilCompareResult = 1;
		}
		else
		{
			ilCompareResult = -1;
		}
	}
	return ilCompareResult;
	CCS_CATCH_ALL
	return 0;
}
/////////////////////////////////////////////////////////////////////////////
// CCoverageGraphicWnd


CCoverageGraphicWnd::CCoverageGraphicWnd() : CWnd()
{
	CCS_TRY
	imLeftScale = 0;
	imPageNo = 0;
	XXX = 112000;


	bmShowMainCurve = true;
	bmIsInitialized = FALSE;
	imCountPerUnit = 1;
	pomStatic = NULL;
	pomHorzCross = NULL;
	pomVertCross = NULL;
	pomViewer = NULL;
	omFrameStart =TIMENULL;
	omFrameEnd = TIMENULL;
	bmAdditive = true;
	pomFlightPoly = NULL;
	pomSimFlightPoly = NULL;
	pomSimFlight2Poly = NULL;
	pomRealFlightPoly = NULL;
	pomShiftPoly = NULL;
	pomSingleShiftPoly = NULL;
	pomSingleFlightPoly = NULL;

	omCurTime = CTime::GetCurrentTime();

	omBlackPen.CreatePen(PS_SOLID, 3, COLORREF(BLACK));
	omGrayPen.CreatePen(PS_DOT, 1, COLORREF(GRAY));
	omWhitePen.CreatePen(PS_SOLID, (int)1, COLORREF(WHITE));
	omRedPen.CreatePen(PS_SOLID, (int)1, COLORREF(RED));
	omBluePen.CreatePen(PS_SOLID, (int)1, COLORREF(BLUE));
	omGreenPen.CreatePen(PS_SOLID, (int)1, COLORREF(GREEN));
	omTimeLinePen.CreatePen(PS_SOLID, 1, COLORREF(RED));
	
	omGreenBrush.CreateSolidBrush(COLORREF(GREEN));
	omLightGreenBrush.CreateSolidBrush(COLORREF(LIME));
	omSilverBrush.CreateSolidBrush(COLORREF(SILVER));
	omBlackBrush.CreateSolidBrush(COLORREF(BLACK));
	imColorCount=1;

	ogDdx.Register(this, COV_REDRAW,CString("COVERAGEGRAPHIC"), CString("COV_REDRAW"), CoverageWndCf);
	ogDdx.Register(this, UPDATE_COLOR_SETUP,CString("UPDATE_COLOR_SETUP"), CString("UPDATE_COLOR_SETUP"), CoverageWndCf);

	pomList = new CGridFenster(this);
	imFilterIdx = FUNCTION;

	imActuelRow = -1;

	CCS_CATCH_ALL
}

void CCoverageGraphicWnd::Register()
{
	CCS_TRY
		ogDdx.Register(this, COV_REDRAW,CString("COVERAGEGRAPHIC"), CString("COV_REDRAW"), CoverageWndCf);
		ogDdx.Register(this, UPDATE_COLOR_SETUP,CString("UPDATE_COLOR_SETUP"), CString("UPDATE_COLOR_SETUP"), CoverageWndCf);
	CCS_CATCH_ALL
}
void CCoverageGraphicWnd::UnRegister()
{
	CCS_TRY
    ogDdx.UnRegister(this, NOTUSED);
	CCS_CATCH_ALL
}
CCoverageGraphicWnd::~CCoverageGraphicWnd()
{
	CCS_TRY
    ogDdx.UnRegister(this, NOTUSED);
	if(pomStatic != NULL)
	{
		delete pomStatic;
		pomStatic = NULL;
	}
	if(pomHorzCross != NULL)
	{
		delete pomHorzCross;
		pomHorzCross = NULL;
	}
	if(pomVertCross != NULL)
	{
		delete pomVertCross;
		pomVertCross = NULL;
	}
	if(pomList != NULL)
	{
		delete pomList;
		pomList = NULL;
	}
	for(int i = 0; i < omLines.GetSize(); i++)
	{
		omLines[i].Values.DeleteAll();
	}
	omLines.DeleteAll();
	
	for( i = 0; i < omShiftLines.GetSize(); i++)
	{
		omShiftLines[i].Values.DeleteAll();
	}

	omShiftLines.DeleteAll();

	if(pomFlightPoly!=NULL)
		delete pomFlightPoly;
	
	if(pomSimFlightPoly!=NULL)
		delete pomSimFlightPoly;

	if(pomSimFlight2Poly!=NULL)
		delete pomSimFlight2Poly;
	
	if(pomRealFlightPoly!=NULL)
		delete pomRealFlightPoly;
	if(pomShiftPoly!=NULL)
		delete pomShiftPoly;
	if(pomSingleShiftPoly!=NULL)
		delete pomSingleShiftPoly;
	if(pomSingleFlightPoly!=NULL)
		delete pomSingleFlightPoly;
	
	omBlackPen.DeleteObject();
	omGrayPen.DeleteObject();
	omGreenPen.DeleteObject();
	omBluePen.DeleteObject();
	omRedPen.DeleteObject();
	omTimeLinePen.DeleteObject();
	omCurrentPen.DeleteObject();
	omGreenBrush.DeleteObject();
	omLightGreenBrush.DeleteObject();
	omSilverBrush.DeleteObject();
	omBlackBrush.DeleteObject();
	
	pomPolyLines.DeleteAll();
	pomPolyRealLines.DeleteAll();
	omColors.DeleteAll();
	omGhsUrnos.RemoveAll();
	omPfcUrnos.RemoveAll();
//	omPenList.DeleteAll();
	ClearPolygonLines();
	CCS_CATCH_ALL
}

void CCoverageGraphicWnd::CoverageWndCf(void *popInstance, int ipDDXType,void *vpDataPointer, CString &ropInstanceName)
{
	CCS_TRY
    CCoverageGraphicWnd *polWnd = (CCoverageGraphicWnd *)popInstance;

	//Changes for employees
		if (ipDDXType == COV_REDRAW)
		{
			polWnd->SetTimeFrame();
			polWnd->UpdateWindow();

		}
		else if (ipDDXType == UPDATE_COLOR_SETUP)
		{
			if (::IsWindow(polWnd->m_hWnd))
			{
				polWnd->SetColorSetup();
				polWnd->SetTimeFrame();
				polWnd->FillList();
				polWnd->UpdateWindow();
			}
		}

	CCS_CATCH_ALL
}


BEGIN_MESSAGE_MAP(CCoverageGraphicWnd, CWnd)
	//{{AFX_MSG_MAP(CCoverageGraphicWnd)
	ON_WM_CREATE()
	ON_WM_DESTROY()
	ON_WM_MOUSEMOVE()
	ON_WM_PAINT()
	ON_WM_RBUTTONDOWN()
	ON_WM_SIZE()
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONDBLCLK()
	ON_WM_ERASEBKGND()
	ON_COMMAND(21, OnTimeEval)
	ON_MESSAGE(WM_UPDATE_COVERAGE, UpdateGraphic)
	ON_MESSAGE(GRID_ACTCELLMOVED, OnActuellCellMoved)
	ON_WM_KEYDOWN()
	ON_WM_KEYUP()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten CCoverageGraphicWnd 

int CCoverageGraphicWnd::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	CCS_TRY
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	SetColorSetup();
	
	pomStatic = new CStatic();
	CRect olRect(0, 0, 0, 0);
	pomStatic->Create( "", WS_VISIBLE|WS_CHILD|WS_BORDER|SS_CENTER, olRect, this);
	pomStatic->SetFont(&ogSmallFonts_Regular_8);
	pomHorzCross = new CStatic();
	pomHorzCross->Create( "", WS_VISIBLE|WS_CHILD|WS_BORDER|SS_CENTER, olRect, this);
	pomVertCross = new CStatic();
	pomVertCross->Create( "", WS_VISIBLE|WS_CHILD|WS_BORDER|SS_CENTER, olRect, this);

	CRect olRect2(0,50,90,200);
	pomList->Create(WS_BORDER | WS_VSCROLL | WS_HSCROLL | WS_TABSTOP,olRect2,this,IDC_GRIDLIST);
	pomList->Initialize();
	pomList->SetColCount(1);
	pomList->SetRowCount(10);
	pomList->GetParam()->EnableUndo(FALSE);
	pomList->GetParam()->EnableTrackColWidth(FALSE);
	pomList->GetParam()->EnableTrackRowHeight(FALSE);
	pomList->GetParam()->EnableSelection(GX_SELROW);
	pomList->GetParam()->SetNumberedColHeaders(FALSE);
	pomList->GetParam()->SetNumberedRowHeaders(FALSE);
	pomList->SetColWidth(0,0,0);
	pomList->SetColWidth(1,1,70);

	pomList->SetSortingEnabled(true);

	LOGFONT rlLf;
	memset(&rlLf, 0, sizeof(LOGFONT));

	//Courier New 8 
	rlLf.lfHeight = - MulDiv(8, imLogPixelsY, 72);
	rlLf.lfWeight = FW_NORMAL;
	rlLf.lfPitchAndFamily = VARIABLE_PITCH | FF_MODERN;
	lstrcpy(rlLf.lfFaceName, "Courier New");

	omCGXFont.SetLogFont(rlLf);
	pomList->GetParam()->EnableUndo(TRUE);

	pomList->ShowWindow(SW_SHOW);
	return 0;
	CCS_CATCH_ALL
	return -1;
}

LONG CCoverageGraphicWnd::UpdateGraphic(WPARAM wParam, LPARAM lParam)
{
	CCS_TRY
	CCS_CATCH_ALL
	return 0L;
}


LONG CCoverageGraphicWnd::InitializePrinter()
{
	CCS_TRY
	int ilRc;
	HDC hlHdc;

	char pclDevices[182];

	::GetProfileString("windows", "device","",pclDevices,180);
	CString olDevices = pclDevices;
	CString olDeviceName;
	int ilKomma = olDevices.Find(',');
	if (ilKomma > -1)
	{
		olDeviceName = olDevices.Left(ilKomma);
	}


	char pclDeviceName[182];
	strcpy(pclDeviceName,olDeviceName);

	HANDLE  hlPrinter = 0;
	static HANDLE hDevMode = 0;
	
	//if (hDevMode == 0)
	{
		if (OpenPrinter(pclDeviceName,&hlPrinter,NULL) == TRUE)
		{
		//	if (GetPrinter(hlPrinter,2,(unsigned char *)&rlPrinterInfo,sizeof(rlPrinterInfo),&llBytesReceived) == TRUE)
			{
				LONG llDevModeLen = DocumentProperties(NULL,hlPrinter,pclDeviceName,NULL,NULL,0);
				if (llDevModeLen > 0)
				{
					hDevMode = GlobalAlloc(GMEM_MOVEABLE,llDevModeLen);
					DEVMODE *prlDevMode = (DEVMODE *) GlobalLock(hDevMode);
					DocumentProperties(NULL,hlPrinter,pclDeviceName,prlDevMode,NULL,DM_OUT_BUFFER);
					DWORD dm_orientation =  DM_ORIENTATION;
					prlDevMode->dmFields = DM_ORIENTATION;
					
					prlDevMode->dmOrientation = DMORIENT_LANDSCAPE;
					
					DocumentProperties(NULL,hlPrinter,pclDeviceName,prlDevMode,prlDevMode,DM_IN_BUFFER|DM_OUT_BUFFER);
					GlobalUnlock(hDevMode);
				}
			}
			ClosePrinter(hlPrinter);
		}
	}
   
	
   
  
	imPageNo = 0;

	CPrintDialog *polPrintDialog = new CPrintDialog(
		  FALSE,PD_ALLPAGES|PD_NOPAGENUMS|PD_NOSELECTION|PD_USEDEVMODECOPIESANDCOLLATE,
		  this);

	if (hDevMode != 0)
	{
		polPrintDialog->m_pd.hDevMode = hDevMode;
	}
	LPDEVMODE prlOldDevMode = polPrintDialog->GetDevMode( );

	ilRc = polPrintDialog->DoModal();
	if (ilRc != IDCANCEL )
	{
		if(bmIsInitialized == FALSE)
		{
			LPDEVMODE prlDevMode = polPrintDialog->GetDevMode( );

			hlHdc = polPrintDialog->GetPrinterDC();
			if(hlHdc != NULL)
			{
				omCdc.Attach(hlHdc);
				omCdc.SetMapMode(MM_ANISOTROPIC);
			
				imLogPixelsY = omCdc.GetDeviceCaps(VERTRES/*LOGPIXELSY*/);
				imLogPixelsX = omCdc.GetDeviceCaps(HORZRES/*LOGPIXELSX*/);

				omRgn.CreateRectRgn(0,0,omCdc.GetDeviceCaps(HORZRES),omCdc.GetDeviceCaps(VERTRES));
				omCdc.SelectClipRgn(&omRgn);


				//TRACE("X = %d, Y = %d \n",imLogPixelsX,imLogPixelsY);
				LOGFONT rlLf;
				memset(&rlLf, 0, sizeof(LOGFONT));

				//Courier New 8 
				rlLf.lfCharSet= DEFAULT_CHARSET;
				rlLf.lfHeight = - MulDiv(8, imLogPixelsY, 72);
				rlLf.lfWeight = FW_NORMAL;
				rlLf.lfPitchAndFamily = VARIABLE_PITCH | FF_MODERN;
				lstrcpy(rlLf.lfFaceName, "Courier New");
				ilRc = ogCourierNew_Regular_8.CreateFontIndirect(&rlLf);
				//Courier New 8 Bold
				rlLf.lfWeight = FW_BOLD;
				ilRc = ogCourierNew_Bold_8.CreateFontIndirect(&rlLf);

				//Arial 8
				rlLf.lfCharSet= DEFAULT_CHARSET;
				rlLf.lfWeight = FW_NORMAL;
				rlLf.lfPitchAndFamily = VARIABLE_PITCH | FF_SWISS;
				lstrcpy(rlLf.lfFaceName, "Arial");
				ilRc = omSmallFont_Regular.CreateFontIndirect(&rlLf);

				//Arial 12
				rlLf.lfCharSet= DEFAULT_CHARSET;
				rlLf.lfHeight = - MulDiv(12, imLogPixelsY, 72); 
				ilRc = omMediumFont_Regular.CreateFontIndirect(&rlLf);

				//Arial 18
				rlLf.lfCharSet= DEFAULT_CHARSET;
				rlLf.lfHeight =  - MulDiv(18, imLogPixelsY, 72);   
				ilRc = omLargeFont_Regular.CreateFontIndirect(&rlLf);

				//Arial 8 Bold
				rlLf.lfCharSet= DEFAULT_CHARSET;
				rlLf.lfHeight = - MulDiv(8, imLogPixelsY, 72);
				rlLf.lfWeight = FW_BOLD;
				ilRc = omSmallFont_Bold.CreateFontIndirect(&rlLf);

				//Arial 12 Bold
				rlLf.lfCharSet= DEFAULT_CHARSET;
				rlLf.lfHeight = - MulDiv(12, imLogPixelsY, 72); 
				ilRc = omMediumFont_Bold.CreateFontIndirect(&rlLf);

				//Arial 18 Bold
				rlLf.lfCharSet= DEFAULT_CHARSET;
				rlLf.lfHeight =  - MulDiv(18, imLogPixelsY, 72);   
				ilRc = omLargeFont_Bold.CreateFontIndirect(&rlLf);
				///////////////

				rlLf.lfHeight = - MulDiv(10, imLogPixelsY, 72);
				rlLf.lfPitchAndFamily = 2;
				rlLf.lfCharSet = SYMBOL_CHARSET;
				rlLf.lfWeight = 400;
				lstrcpy(rlLf.lfFaceName, "ccs");
				ilRc = omCCSFont.CreateFontIndirect(&rlLf);

				bmIsInitialized = TRUE;

				TEXTMETRIC olTm;

				omCdc.GetTextMetrics(&olTm);
				int  ilHeight = olTm.tmHeight + olTm.tmExternalLeading;

				// calculate vertical ofset of text in a bar
				CFont *polOldFont = omCdc.SelectObject(&omSmallFont_Regular);
				CSize olSize = omCdc.GetTextExtent("LH 4711");
				omCdc.SelectObject(polOldFont);
			}
			else
			{
				MessageBox("Coverage detected problems with the printerdriver\nPlease check it!", "Error", (MB_ICONEXCLAMATION|MB_OK));
				return 0L;
			}
		}
	}
	else
	{
		delete polPrintDialog;
		return 0L;
	}
	delete polPrintDialog;

	//Minus 10 % for each size
	int il10PercentX = (int)((imLogPixelsX/100)*10);
	int il10PercentY = (int)((imLogPixelsY/100)*10);
	CRect olClientRect = CRect(il10PercentX, il10PercentY, imLogPixelsX-(int)(il10PercentX/2), imLogPixelsY-(int)(il10PercentY));	

	int i;
	long ilCy = 80000;
	CPen olBlackPen, 
		 olGrayPen,
		 olGreenPen,
		 olOlivePen,
		 olBluePen,
		 olYellowPen,
		 olTimeLinePen,
		 olCurrentPen,
		*polOldPen;

	CBrush olGreenBrush(COLORREF(GREEN));
	CBrush olLightGreenBrush(COLORREF(LIME));
	CBrush olSilverBrush(COLORREF(SILVER));
	CBrush olBlackBrush(COLORREF(BLACK));
	CBrush *polOldBrush = NULL;

	CTime		olEndTime;
	CTime		olStartTime = pomTimeScale->GetDisplayStartTime();
	CTimeSpan	olTimeSpan  = pomTimeScale->GetDisplayDuration();

	olEndTime = olStartTime + olTimeSpan;
	imXMinutes = olTimeSpan.GetTotalMinutes();

	DOCINFO	rlDocInfo;
	memset(&rlDocInfo, 0, sizeof(DOCINFO));
	rlDocInfo.cbSize = sizeof( DOCINFO );
	rlDocInfo.lpszDocName = "Coverage Toolbox";	
	int ilRet = omCdc.StartDoc( &rlDocInfo );

	int ilYCount = 0;
	ilYCount = imMaxYCount;
	int ilOneShift = (int)(10000);
	if (imMaxYCount > 0)
		ilOneShift = (int)(100000/(imMaxYCount+1));
	//int ilOneMinute;
	int ilLegendSize = (int)(100000/15);
	int ilLegendOffset = (int)(100000/100);

	////////////////////////////////////////////////////////////////////////////////////
	int ilXXXTmp = (int)(olClientRect.right - olClientRect.left - imLeftScale);
	int ilGraphWidth = MulDiv(ilXXXTmp,XXX,olClientRect.right);
	
	double ilOneMinute = (double)(ilGraphWidth/imXMinutes);
	
	ilOneMinute = imOneMinute;
	imOneShift = ilOneShift;

	omCdc.SetMapMode(MM_ANISOTROPIC);
	omCdc.SetWindowExt(XXX/*112000*/, 100000);
	omCdc.SetViewportExt(olClientRect.right, -(olClientRect.bottom-500));
	omCdc.SetViewportOrg(olClientRect.left + imLeftScale, olClientRect.bottom);

	olBlackPen.CreatePen(PS_SOLID, 3, COLORREF(BLACK));
	olGrayPen.CreatePen(PS_DOT, 1, COLORREF(GRAY));
	olBluePen.CreatePen(PS_SOLID, ilOneMinute, COLORREF(BLUE));
	olOlivePen.CreatePen(PS_SOLID, ilOneMinute, COLORREF(OLIVE));
	olTimeLinePen.CreatePen(PS_SOLID, 1, COLORREF(RED));
	olYellowPen.CreatePen(PS_SOLID, 1, COLORREF(YELLOW));
	
	polOldPen = omCdc.SelectObject(&omPrintPenList[COVERAGE_ALL]);


	CTime olTime = olStartTime;
	olTime -= CTimeSpan(0, 0, olTime.GetMinute(), 0);
	CString olTimeString = olTime.Format("%H:%M");
	int ilX;
	CTimeSpan olCurrent = olStartTime - olTime;
	olCurrent = CTimeSpan(0,1,0,0) - olCurrent;
	int ilCurrentMinute = 0;

	// -*- coverage
	for(i = 0; (i < imXMinutes && i < omShiftArray.GetSize()); i++)
	{
		ilCurrentMinute = (i+1)*ilOneMinute;
		CRect olFillRect = CRect((i)*ilOneMinute, omShiftArray[i]*ilOneShift, ilCurrentMinute, 0);
		omCdc.FillRect(olFillRect, &omPrintBrushList[COVERAGE_ALL]);
		
	}

	// single coverage
	if (pomSingleShiftPoly != NULL)
	{
		polOldBrush = omCdc.SelectObject(&omPrintBrushList[COVERAGE_SINGLE]);
		polOldPen   = omCdc.SelectObject(&omPrintPenList[COVERAGE_SINGLE]);
		omCdc.Polygon(pomSingleShiftPoly,imSingleShiftPolySize);
		omCdc.SelectObject(polOldBrush);
		omCdc.SelectObject(polOldPen);
	}

	CFont olNewFont;
	CFont olLegendFont;
	CFont olHeaderFont1;
	CFont olHeaderFont2;
	int ilLogPixelsY = omCdc.GetDeviceCaps(LOGPIXELSY);
	int ilLogPixelsX = omCdc.GetDeviceCaps(LOGPIXELSX);
	LOGFONT rlLf;

	memset(&rlLf, 0, sizeof(LOGFONT));
	//Courier New 8 
	rlLf.lfCharSet= DEFAULT_CHARSET;
	rlLf.lfHeight = - MulDiv(30, ilLogPixelsY*7, 72);//300
	rlLf.lfWeight = FW_BOLD;
	rlLf.lfPitchAndFamily = VARIABLE_PITCH | FF_MODERN;
	lstrcpy(rlLf.lfFaceName, "Courier New");
	ilRc = olNewFont.CreateFontIndirect(&rlLf);

	rlLf.lfCharSet= DEFAULT_CHARSET;
	rlLf.lfHeight = - MulDiv(30, ilLogPixelsY*7, 72);//400
	rlLf.lfWeight = FW_BOLD;
	rlLf.lfPitchAndFamily = VARIABLE_PITCH | FF_MODERN;
	lstrcpy(rlLf.lfFaceName, "Arial");
	ilRc = olLegendFont.CreateFontIndirect(&rlLf);

	rlLf.lfCharSet= DEFAULT_CHARSET;
	rlLf.lfHeight = - MulDiv(40, ilLogPixelsY*7, 72);//400
	rlLf.lfWeight = FW_BOLD;
	rlLf.lfPitchAndFamily = VARIABLE_PITCH | FF_MODERN;
	lstrcpy(rlLf.lfFaceName, "Arial");
	ilRc = olHeaderFont1.CreateFontIndirect(&rlLf);

	rlLf.lfCharSet= DEFAULT_CHARSET;
	rlLf.lfHeight = - MulDiv(20, ilLogPixelsY*7, 72);//400
	rlLf.lfWeight = FW_NORMAL;
	rlLf.lfPitchAndFamily = VARIABLE_PITCH | FF_MODERN;
	lstrcpy(rlLf.lfFaceName, "Arial");
	ilRc = olHeaderFont2.CreateFontIndirect(&rlLf);

	
    TEXTMETRIC tm;
    omCdc.GetTextMetrics(&tm); 
    int ilTextHeight = tm.tmHeight + tm.tmExternalLeading + 2;
//Header malen
	omCdc.SetTextColor( COLORREF(BLACK));
	omCdc.SetBkColor(COLORREF(WHITE));
	omCdc.SelectObject(&olHeaderFont1);

	CRect olCovRect = CRect(-11000, 110000, 100000, 110000-(int)(ilTextHeight*1.5));
	CString olStr;

	olStr.Format(" %s%s         ", olStartTime.Format("%d.%m.%y  %H:%M - "), olEndTime.Format("%d.%m.%y  %H:%M"));

	omCdc.TextOut(olCovRect.left, olCovRect.top, CString("Coverage :  ") + LoadStg(IDS_STRING61264) + olStr +"View: " + pomViewer->TopViewName() + "Filter: " + pomViewer->EvalViewName());

	int ilNextY = 110000-(int)(ilTextHeight*1.5);
	omCdc.MoveTo(-11000, ilNextY);
	omCdc.LineTo(100000, ilNextY);
	CStringArray olFilterValues;

	omCdc.SelectObject(&olHeaderFont1);
    omCdc.GetTextMetrics(&tm); 
    ilTextHeight = tm.tmHeight + tm.tmExternalLeading + 2;
	ilNextY -= ilTextHeight;
	olCovRect = CRect(-11000, ilNextY, 100000, ilNextY-(int)(ilTextHeight*1.5));
	


//Ende Header

	//Achsen malen
	omCdc.SelectObject(&olBluePen);
	omCdc.MoveTo(0, 0);
	omCdc.LineTo(0, 100000);
	omCdc.MoveTo(0, 0);
	omCdc.LineTo(100000, 0);
	omCdc.MoveTo(0, 100000);
	omCdc.LineTo(100000, 100000);
	omCdc.LineTo(100000, 0);

	CFont *polOldFont = omCdc.SelectObject( &olNewFont );
	omCdc.SelectObject(&olGrayPen);
	omCdc.SetBkMode(TRANSPARENT);
	//Vertikale Hilfslinien
	olTime = olStartTime;
	olTime -= CTimeSpan(0, 0, olTime.GetMinute(), 0);
	CTimeSpan olFTDiff = olEndTime - olTime;
	int ilHourFreq = (int)((olFTDiff.GetTotalMinutes()/60)/6);
	if(ilHourFreq == 0)
	{
		ilHourFreq = 1;
	}
	int ilFreqCount = ilHourFreq;

	for(int ili=0; olTime < olEndTime; ili++)
	{
		//CString olText;
		olTime += CTimeSpan(0,0,30,0);
		ilX = ilOneMinute * olCurrent.GetTotalMinutes();
		olCurrent = olCurrent + CTimeSpan(0, 0, 30, 0);
		if(ili%2 == 0)
		{
			omCdc.SelectObject(&olGrayPen);
			omCdc.SetBkMode(TRANSPARENT);
			CTime olTmpTime = olTime + CTimeSpan(0, 1, 0, 0);
			olTimeString = olTmpTime.Format("%H");
			omCdc.MoveTo(ilX, 100000);
			omCdc.LineTo(ilX, 0);
			CSize olSize = omCdc.GetTextExtent( olTimeString );
			omCdc.SetTextColor( COLORREF(BLACK));
			omCdc.SetBkColor(COLORREF(WHITE));

			ilFreqCount--;
			{
				CRect olTextRect = CRect(ilX-(int)(olSize.cx/2), 
										(-3*ilOneMinute),
										 ilX+(int)(olSize.cx/2), 
										 (-3*ilOneMinute)+ilTextHeight);
				omCdc.TextOut(olTextRect.left, olTextRect.top, olTimeString);
				olTextRect = CRect(ilX-(int)(olSize.cx/2), 
										(100000+ilOneMinute+ilTextHeight+500),
										 ilX+(int)(olSize.cx/2), 
										 (100000+ilOneMinute+500));
				omCdc.TextOut(olTextRect.left, olTextRect.top, olTimeString);
				ilFreqCount = ilHourFreq;
			}
		}//end if
		else
		{
			if(olTime < olEndTime)
			{
				omCdc.SelectObject(&olBlackPen);
				omCdc.MoveTo(ilX, 0);
				omCdc.LineTo(ilX, -800);
				omCdc.MoveTo(ilX, 100000);
				omCdc.LineTo(ilX, 100000+800);
			}
		}
	}
	//Horizontale Hilfslinien zeichnen
	omCdc.SelectObject(&olGrayPen);
	omCdc.SetBkMode(TRANSPARENT);
	int ilYAxixCount = 0;
	for(i = 0; i < (int)(ilYCount/imCountPerUnit); i++)
	{
		CString olText;
		ilYAxixCount = (i+1)*imCountPerUnit;
		olText.Format("%d", ilYAxixCount);
		int ilCurrY = (i+1)*ilOneShift*imCountPerUnit;
		omCdc.MoveTo(0, ilCurrY);
		omCdc.LineTo(100000, ilCurrY);
		CSize olSize = omCdc.GetTextExtent( olText );
		omCdc.SetTextColor( COLORREF(BLACK));
		omCdc.SetBkColor(COLORREF(WHITE));
		CRect olTextRect = CRect((-3*ilOneMinute)-olSize.cx, 
								ilCurrY+(int)(ilTextHeight/2),//+(int)((imCountPerUnit*ilOneShift)), 
								 (-3*ilOneMinute), ilCurrY-(int)(ilTextHeight/2));//+(int)((imCountPerUnit*ilOneShift)+olSize.cy));
		omCdc.TextOut(olTextRect.left, olTextRect.top, olText);
		olTextRect = CRect((100000+ilOneMinute), 
								ilCurrY+(int)(ilTextHeight/2),//+(int)((imCountPerUnit*ilOneShift)), 
								 (100000+ilOneMinute)+olSize.cx, ilCurrY-(int)(ilTextHeight/2));//+(int)((imCountPerUnit*ilOneShift)+olSize.cy));
		omCdc.TextOut(olTextRect.left, olTextRect.top, olText);
	}
	//Y-Axis-HelpLines per Unit
	omCdc.SelectObject(&olBlackPen);
	for(i = 0; i < ilYCount; i++)
	{
		if(i % imCountPerUnit != 0)
		{
			ilYAxixCount = i;
			int ilCurrY =  i*ilOneShift;
			omCdc.MoveTo(0, ilCurrY);
			omCdc.LineTo(-500, ilCurrY);
			omCdc.MoveTo(100000, ilCurrY);
			omCdc.LineTo(100000+500, ilCurrY);
		}
	}

	omCdc.SelectObject(&omPrintPenList[FUNCTION_ALL]);
	omCdc.SetBkMode(OPAQUE);
	omCdc.MoveTo(0,0);
	if(	bmShowMainCurve == true)
	{
		for(i = 0; i< imFlightPolySize; i++)
		{
			int ilYCoord = pomFlightPoly[i].y;
			int ilXAbs   = pomFlightPoly[i].x;
			if(ilYCoord>0)
			{
				printf("");
			}
			omCdc.LineTo(ilXAbs,ilYCoord);

		}

		if (imActuelRow >= 0)		
		{
			omCdc.SelectObject(&omPrintPenList[imActuelRow]);
		
			omCdc.MoveTo(0,0);
			for(i = 0; i< imSingleFligtPolySize; i++)
			{
				int ilYCoord = pomSingleFlightPoly[i].y;
				int ilXAbs   = pomSingleFlightPoly[i].x;
				if(ilYCoord>0)
				{
					printf("");
				}
				omCdc.LineTo(ilXAbs,ilYCoord);

			}
		}

		if (pomViewer->SimulationActive() == true && pomViewer->SimDisplay() == 0 && pomSimFlight2Poly != NULL)
		{
			omCdc.SelectObject(&omPrintPenList[SIMULATION_CALC]);
			omCdc.MoveTo(0,0);
			for(i = 0; i< imSimFlight2PolySize; i++)
			{
				int ilYCoord = pomSimFlight2Poly[i].y;
				int ilXAbs   = pomSimFlight2Poly[i].x;
				if(ilYCoord>0)
				{
					printf("");
				}
				omCdc.LineTo(ilXAbs,ilYCoord);

			}
		}

		if (pomViewer->SimulationActive() && pomViewer->SimDisplay() == 1 && pomSimFlightPoly != NULL)
		{
			omCdc.SelectObject(&omPrintPenList[SIMULATION_VISUAL]);
			
			omCdc.MoveTo(0,0);
			for(i = 0; i< imSimFlightPolySize; i++)
			{
				int ilYCoord = pomSimFlightPoly[i].y;
				int ilXAbs   = pomSimFlightPoly[i].x;
				if(ilYCoord>0)
				{
					printf("");
				}
				omCdc.LineTo(ilXAbs,ilYCoord);

			}
		}
	}
	CUIntArray omAdditCount;

	for(i = 0; i < omFlightValues.GetSize(); i++)
	{
		omAdditCount.Add(0);
	}

	omCdc.MoveTo(0,0);

//Legende malen
	CRect olTmpRect = olClientRect;
	olTmpRect.left += 10;
	olTmpRect.right = imLeftScale;
	omCdc.DPtoLP(&olTmpRect);

	CRect olLegRect = CRect(-11000, 100000, -2000, 100000-(int)(ilLegendSize/1.5)/*-ilLegendOffset*/);
	omCdc.SelectObject(&olBlackBrush);
	omCdc.SelectObject(&olLegendFont);
	omCdc.SetBkColor( COLORREF(WHITE) );
	omCdc.SetTextColor(COLORREF(BROWN));
	
	omCdc.FrameRect( olLegRect, &olBlackBrush );
	omCdc.DrawText( omFilterType, omFilterType.GetLength(), olLegRect, DT_LEFT );

	olLegRect.top = olLegRect.bottom - 100;
	olLegRect.bottom -= (int)(ilLegendSize/1.5);

	for(i = 0; i < omLines.GetSize(); i++)
	{
		omCdc.SetBkColor( COLORREF(WHITE) );

		omCdc.SetTextColor( COLORREF(BROWN));
	
		omCdc.FrameRect( olLegRect, &olBlackBrush );
		CString olComplText;
		if(imActuelRow == i)
		{
			olComplText = "->";
		}
		olComplText += omLines[i].Lkco;

		omCdc.DrawText( olComplText, olComplText.GetLength(), olLegRect, DT_LEFT );
		omCdc.MoveTo(0,0);
		omCdc.SelectObject(&omPrintPenList[i%imColorCount]);

		for(int j = 0; (j < imXMinutes && j < omLines[i].Values.GetSize()); j++)
		{
			if(bmAdditive == true)
			{
				omAdditCount[j] = omAdditCount[j] + omLines[i].Values[j];
			}
			else
			{
				omCdc.LineTo((j)*ilOneMinute, (omLines[i].Values[j]+omAdditCount[j])*ilOneShift);
			}
			
		}
		//Real Demand

		olLegRect.top = olLegRect.bottom - 100;
		olLegRect.bottom -= (int)(ilLegendSize/1.5);
	}

//ende legende
	omCdc.SelectObject(&olNewFont);
	omCdc.SelectObject(&olTimeLinePen);
	omCurTime = CTime::GetCurrentTime();
	if(IsBetween(omCurTime,olStartTime, olEndTime))
	{
		CTimeSpan olTS;
		if(omCurTime >= olStartTime)
		{
			olTS = omCurTime - olStartTime;
		}
		else if(omCurTime < olEndTime)
		{
			olTS = olEndTime - omCurTime;
		}
		ilX = olTS.GetTotalMinutes()*ilOneMinute;
		
	}

	omCdc.EndDoc();

	omCdc.SelectObject(polOldBrush);
	omCdc.SelectObject(polOldFont);
	omCdc.SelectObject(polOldPen);

	olBlackPen.DeleteObject();
	olGrayPen.DeleteObject();
	olGreenPen.DeleteObject();
	olBluePen.DeleteObject();
	olOlivePen.DeleteObject();
	olYellowPen.DeleteObject();
	olTimeLinePen.DeleteObject();
	olCurrentPen.DeleteObject();
	olGreenBrush.DeleteObject();
	olLightGreenBrush.DeleteObject();
	olSilverBrush.DeleteObject();
	olBlackBrush.DeleteObject();
	olNewFont.DeleteObject();
	olLegendFont.DeleteObject();

	return (LONG)ilRc;
	CCS_CATCH_ALL
	return 0L;
}

void CCoverageGraphicWnd::OnDestroy() 
{
	CCS_TRY

    ogDdx.UnRegister(this, NOTUSED);

	CWnd::OnDestroy();
	
	// TODO: Code f�r die Behandlungsroutine f�r Nachrichten hier einf�gen
	
	CCS_CATCH_ALL
}

void CCoverageGraphicWnd::OnMouseMove(UINT nFlags, CPoint point) 
{
	CPoint olPoint = point;
	CClientDC dc(this);
	CRect olClientRect;	
	GetClientRect(&olClientRect);

	int ilStartX = pomTimeScale->GetXFromTime(omStartTime);
	int ilEndX = pomTimeScale->GetXFromTime(omEndTime);
	int ilXDiff = ilEndX-ilStartX; 

	pomHorzCross->ShowWindow(SW_HIDE);
	pomVertCross->ShowWindow(SW_HIDE);

	CTime olClickTime = pomTimeScale->GetTimeFromX(point.x-120);
	char pclTmp[712]="";
	sprintf(pclTmp, "%s: %s %s %s", LoadStg(COV_BTN_SHIFTDEMANDS), olClickTime.Format("%d.%m.%Y - %H:%M"),pogCoverageDiagram->omCaptionText,pogCoverageDiagram->omCurrTimeText);
	pogCoverageDiagram->SetWindowText(pclTmp);

	dc.SetMapMode(MM_ANISOTROPIC);
	dc.SetWindowExt(XXX, 100000);
	dc.SetViewportExt(olClientRect.right, -olClientRect.bottom);
	dc.SetViewportOrg(olClientRect.right - ilXDiff/*olClientRect.left + imLeftScale*/, olClientRect.bottom);
	dc.DPtoLP(&olPoint);

	if (olPoint.x < 0)
	{
		printf("");
		return;
	}

	CFont olNewFont;
	int ilLogPixelsY = dc.GetDeviceCaps(LOGPIXELSY);
	int ilLogPixelsX = dc.GetDeviceCaps(LOGPIXELSX);

	CString olText;
	
	
	UINT ilPolyIdx = MulDiv(1,olPoint.x,imOneMinute*FACTOR);
	int ilPolySize = imXMinutes/FACTOR;
	CRect olStaticRect;

	if (imOneShift <= 0)
	{
		imOneShift = 1;
	}

	int ilRangeStart = 0;
	int ilRangeEnd = 0;
	bool blRangeFound = false; 
	int ilCurrentValue = 0;
	for(int ilShiftIdx = 1;ilShiftIdx < imShiftPolySize && !blRangeFound; ilShiftIdx++)
	{
		if(pomShiftPoly[ilShiftIdx -1].x <= olPoint.x &&  pomShiftPoly[ilShiftIdx].x >= olPoint.x)
		{
			ilRangeStart = pomShiftPoly[ilShiftIdx -1].x;
			ilRangeEnd = pomShiftPoly[ilShiftIdx].x;
			ilCurrentValue = pomShiftPoly[ilShiftIdx].y;
			if (ilCurrentValue >= olPoint.y)
			{
				blRangeFound = true;
			}
			else
			{
				ilCurrentValue = 0;
				break;
			}
		}

	}	
		
	int ilSingleRangeStart = 0;
	int ilSingleRangeEnd = 0;
	bool blSingleRangeFound = false; 
	int ilSingleCurrentValue = 0;

	if (pomSingleShiftPoly != NULL)
	{
		for(int ilShiftIdx = 1;ilShiftIdx < imShiftPolySize && !blSingleRangeFound; ilShiftIdx++)
		{
			if (pomSingleShiftPoly[ilShiftIdx -1].x <= olPoint.x &&  pomSingleShiftPoly[ilShiftIdx].x >= olPoint.x)
			{
				ilSingleRangeStart = pomSingleShiftPoly[ilShiftIdx -1].x;
				ilSingleRangeEnd = pomSingleShiftPoly[ilShiftIdx].x;
				ilSingleCurrentValue = pomSingleShiftPoly[ilShiftIdx].y;
				if (ilSingleCurrentValue >= olPoint.y)
				{
					blSingleRangeFound = true;
				}
				else
				{
					ilSingleCurrentValue = 0;
					break;
				}
			}

		}	
	}
	
	if (blRangeFound)
	{
		if (blSingleRangeFound)
		{
			ilCurrentValue	= ilSingleCurrentValue;
			ilRangeStart	= ilSingleRangeStart;
			ilRangeEnd		= ilSingleRangeEnd;
		}
	}
	else
	{
		if (blSingleRangeFound)
		{
			ilCurrentValue	= ilSingleCurrentValue;
			ilRangeStart	= ilSingleRangeStart;
			ilRangeEnd		= ilSingleRangeEnd;
		}
	}
		
	int ilRangeStart2	= 0;
	int ilRangeEnd2		= 0;
	bool blRangeFound2	= false; 
	int ilCurrentValue2 = 0;

	for(int ilFlightIdx = 1;ilFlightIdx < imFlightPolySize && !blRangeFound2; ilFlightIdx++)
	{
		if(pomFlightPoly[ilFlightIdx -1].x <= olPoint.x &&  pomFlightPoly[ilFlightIdx].x >= olPoint.x)
		{
			ilRangeStart2 = pomFlightPoly[ilFlightIdx -1].x;
			ilRangeEnd2 = pomFlightPoly[ilFlightIdx].x;
			ilCurrentValue2 = pomFlightPoly[ilFlightIdx].y;
			if (ilCurrentValue2 >= olPoint.y)
			{
				blRangeFound2 = true;
			}
			else
			{
				ilCurrentValue2 = 0;
				break;
			}
		}

	}	

	bool blSingleRangeFound2 = false;
	int	 ilSingleRangeStart2 = 0;
	int	 ilSingleRangeEnd2	 = 0;
	int	 ilSingleCurrentValue2 = 0;

	if (pomSingleFlightPoly != NULL)
	{
		for(int ilFlightIdx = 1;ilFlightIdx < imSingleFligtPolySize && !blSingleRangeFound2; ilFlightIdx++)
		{
			if(pomSingleFlightPoly[ilFlightIdx -1].x <= olPoint.x &&  pomSingleFlightPoly[ilFlightIdx].x >= olPoint.x)
			{
				ilSingleRangeStart2 = pomSingleFlightPoly[ilFlightIdx -1].x;
				ilSingleRangeEnd2 = pomSingleFlightPoly[ilFlightIdx].x;
				ilSingleCurrentValue2 = pomSingleFlightPoly[ilFlightIdx].y;
				if (ilSingleCurrentValue2 >= olPoint.y)
				{
					blSingleRangeFound2 = true;
				}
				else
				{
					ilSingleCurrentValue2 = 0;
					break;
				}
			}

		}	
	}

	bmEvaluateSingleFunctionOnly = true;

	if (blRangeFound2)
	{
		if (blSingleRangeFound2)
		{
			if (ilCurrentValue2 > ilSingleCurrentValue2)
			{
				ilRangeStart2	= ilSingleRangeStart2;
				ilRangeEnd2		= ilSingleRangeEnd2;
				ilCurrentValue2	= ilSingleCurrentValue2;
				bmEvaluateSingleFunctionOnly = false;
			}
		}
	}
	else if (blSingleRangeFound2)
	{
		ilRangeStart2	= ilSingleRangeStart2;
		ilRangeEnd2		= ilSingleRangeEnd2;
		ilCurrentValue2	= ilSingleCurrentValue2;
		bmEvaluateSingleFunctionOnly = false;
	}



	CTime olFrom2(olClickTime);
	CTime olTo2(olClickTime);

	if (ilRangeStart2 != 0 && ilRangeEnd2 != 0)
	{
		long llRangeStart2 = ilRangeStart2/(imOneMinute*FACTOR)+1;
		long llRangeEnd2 = ilRangeEnd2/(imOneMinute*FACTOR)+1;

		olFrom2 = omStartTime + CTimeSpan(llRangeStart2*60*FACTOR);
		olTo2 = omStartTime + CTimeSpan(llRangeEnd2*60*FACTOR);
	}

	ilCurrentValue2 = MulDiv(1,ilCurrentValue2,imOneShift);
	if (olTo2 > olFrom2)
		olTo2 -= CTimeSpan(0,0,1,0);
	olText.Format("%s-%s => %d ", olFrom2.Format("%H:%M"), olTo2.Format("%H:%M"), ilCurrentValue2);

	olText += LoadStg(COV_BTN_DEMANDS);
	pogCoverageDiagram->omStatusBar.SetPaneText(0, olText);
	pogCoverageDiagram->omStatusBar.UpdateWindow();
		

	if (ilCurrentValue < olPoint.y)
	{
		olText = "";
		olStaticRect = CRect(0,0,0,0);
	}
	else
	{
		if (point.x+100>olClientRect.right)
			olStaticRect = CRect(point.x-100, point.y-20, point.x, point.y + 15-20);
		else
			olStaticRect = CRect(point.x, point.y-20, point.x + 100, point.y + 15-20);
		
		CTime olFrom(olClickTime);
		CTime olTo(olClickTime);
		if (ilRangeStart != 0 && ilRangeEnd != 0)
		{
			long llRangeStart = ilRangeStart/(imOneMinute*FACTOR)+1;
			long llRangeEnd = ilRangeEnd/(imOneMinute*FACTOR)+1;

			olFrom = omStartTime + CTimeSpan(llRangeStart*60*FACTOR);
			olTo = omStartTime + CTimeSpan(llRangeEnd*60*FACTOR);
		}
		
		if (olTo > olFrom)
			olTo -= CTimeSpan(0,0,1,0);
		ilCurrentValue = MulDiv(1,ilCurrentValue,imOneShift);
		olText.Format("%s-%s => %d", olFrom.Format("%H:%M"), olTo.Format("%H:%M"), ilCurrentValue);
		
	}
	pomStatic->MoveWindow(olStaticRect);
	pomStatic->SetWindowText(olText);
}

void CCoverageGraphicWnd::OnPaint() 
{
	CPaintDC dc(this); // device context for painting
	CRect olClientRect;	
	GetClientRect(&olClientRect);

	int i;
	int ilLineSize = omLines.GetSize();

	CPen*	polOldPen = NULL;
	CBrush*	polOldBrush = NULL;

	CTime olEndTime;
	CTime	  olStartTime = pomTimeScale->GetDisplayStartTime();
	CTimeSpan olTimeSpan  = pomTimeScale->GetDisplayDuration();
	olEndTime = olStartTime + olTimeSpan;
	imXMinutes = olTimeSpan.GetTotalMinutes();
	int ilPolySize = imXMinutes/FACTOR;

	int ilYCount = 0;
	ilYCount = imMaxYCount;
	int ilOneShift = (int)(10000);
	if (imMaxYCount > 0)
		ilOneShift = (int)(100000/(imMaxYCount+1));
	int ilLegendSize = (int)(100000/15);
	int ilLegendOffset = (int)(100000/100);
	
	int ilXDiff = (int)(olClientRect.right - 120/*imLeftScale*/);

	imOneMinute = (double)(ilXDiff*XXX)/(olClientRect.right*imXMinutes);//(double)ilOneMinute;
	imOneShift = ilOneShift;
	dc.SetMapMode(MM_ANISOTROPIC);
	CSize olWndExt = dc.SetWindowExt(XXX/*112000*/, 100000);
	CSize olViewRect = dc.SetViewportExt(olClientRect.right, -olClientRect.bottom);
	

	dc.SetViewportOrg(120/*olClientRect.right-ilXDiff-2*/, olClientRect.bottom);

	
	polOldPen = dc.SelectObject(&omBlackPen);
	
	CTime olTime = olStartTime - CTimeSpan(0, 0, olStartTime.GetMinute(), 0);
	//olTimeString = olTime.Format("%H:%M");

	int ilX;
	CTimeSpan olCurrent = olStartTime - olTime;
	olCurrent = CTimeSpan(0,1,0,0) - olCurrent;
	CTime olShiftTime = olStartTime;
	float ilFactor = (1.0*XXX/olClientRect.right);
	
	// draw -*- coverage	
	if (pomShiftPoly!=NULL)
	{
		polOldBrush = dc.SelectObject(&omBrushList[COVERAGE_ALL]);
		polOldPen   = dc.SelectObject(&omPenList[COVERAGE_ALL]);
		dc.Polygon(pomShiftPoly,imShiftPolySize);
		dc.SelectObject(polOldBrush);
		dc.SelectObject(polOldPen);
	}

	// draw single function coverage
	if (pomSingleShiftPoly != NULL)
	{
		polOldBrush = dc.SelectObject(&omBrushList[COVERAGE_SINGLE]);
		polOldPen	= dc.SelectObject(&omPenList[COVERAGE_SINGLE]);
		dc.Polygon(pomSingleShiftPoly,imSingleShiftPolySize);
		dc.SelectObject(polOldBrush);
		dc.SelectObject(polOldPen);
	}

	dc.SelectObject(&omGrayPen);
	dc.SetBkMode(TRANSPARENT);
	CTimeSpan olTimeDiff = olEndTime - olStartTime;
	for(int ili=0; olTime <= olEndTime; ili++)
	{
		olTime += CTimeSpan(0,1,0,0);
		//olTimeString = olTime.Format("%Y.%m.%d - %H:%M:%S");
		ilX = pomTimeScale->GetXFromTime(olTime);//(int)(imOneMinute*olCurrent.GetTotalMinutes() + 0.5);
		ilX = (int)(0.5 + (1.0*XXX/olClientRect.right)*ilX);
		olCurrent = olCurrent + CTimeSpan(0, 1, 0, 0);
		dc.MoveTo(ilX, 100000);
		dc.LineTo(ilX, 0);
	}

	// Horizontale Hilfslinien zeichnen
	CFont olNewFont;
	CFont olLegendFont;

	int ilLogPixelsY = dc.GetDeviceCaps(LOGPIXELSY);
	int ilLogPixelsX = dc.GetDeviceCaps(LOGPIXELSX);
	LOGFONT rlLf;
	memset(&rlLf, 0, sizeof(LOGFONT));

	//Courier New 8 
	rlLf.lfCharSet= DEFAULT_CHARSET;
	rlLf.lfHeight = - MulDiv(500/*300*/, ilLogPixelsY*7, 72);
	rlLf.lfWeight = FW_NORMAL;
	rlLf.lfPitchAndFamily = VARIABLE_PITCH | FF_MODERN;
	lstrcpy(rlLf.lfFaceName, "Courier New");
	int ilRc = olNewFont.CreateFontIndirect(&rlLf);

	rlLf.lfCharSet= DEFAULT_CHARSET;
	rlLf.lfHeight = - MulDiv(400, ilLogPixelsY*7, 72);
	rlLf.lfWeight = FW_NORMAL;
	rlLf.lfPitchAndFamily = VARIABLE_PITCH | FF_MODERN;
	lstrcpy(rlLf.lfFaceName, "Arial");
	ilRc = olLegendFont.CreateFontIndirect(&rlLf);

    TEXTMETRIC tm;
    dc.GetTextMetrics(&tm); 
    int ilTextHeight = tm.tmHeight + tm.tmExternalLeading + 2;
    ilTextHeight = 6899;

	CFont *polOldFont = dc.SelectObject( &olNewFont );
	int ilYAxixCount = 0;
	for(i = 0; i < ilYCount-1; i++)
	{
		CString olText;
		ilYAxixCount = (i+1)*imCountPerUnit;
		olText.Format("%d", ilYAxixCount);
		int ilCurrY = (i+1)*ilOneShift*imCountPerUnit;
		dc.MoveTo(0, ilCurrY);
		dc.LineTo(/*100000*/112000, ilCurrY);
		CSize olSize = dc.GetTextExtent( olText );
		dc.SetTextColor( COLORREF(BLACK));
		dc.SetBkColor(COLORREF(WHITE));
		//CRect olTextRect = CRect((-10*ilOneMinute)-olSize.cx, ilCurrY+(int)((imCountPerUnit*ilOneShift)), 
		//						 (-10*ilOneMinute), ilCurrY-(int)((imCountPerUnit*ilOneShift)));
		CRect olTextRect = CRect((int)(-3*imOneMinute)-olSize.cx + (int) (0.3 * olSize.cx), 
								ilCurrY+(int)(ilTextHeight/2),//+(int)((imCountPerUnit*ilOneShift)), 
								 (int)(-3*imOneMinute) +(int) (0.3 * olSize.cx), ilCurrY-(int)(ilTextHeight/2));//+(int)((imCountPerUnit*ilOneShift)+olSize.cy));
		dc.TextOut(olTextRect.left, olTextRect.top, olText);
	}
	CUIntArray omAdditCount;
	CUIntArray omRealAdditCount;

	if(	bmShowMainCurve == true)
	{
		//	draw min/max lines	
		if(pomViewer->SimulationActive())	
		{
			CPen *pOldPen = dc.SelectObject(&omPenList[MIN_MAX_LINES]);
			int ilCurrY = pomViewer->GetMaxVal()*ilOneShift;
			
			dc.MoveTo(0,ilCurrY);
			dc.LineTo(120000,ilCurrY);

			ilCurrY = pomViewer->GetMinVal()*ilOneShift;
			
			dc.MoveTo(0,ilCurrY);
			dc.LineTo(120000,ilCurrY);
			dc.SelectObject(pOldPen);
		}

		dc.SelectObject(&omPenList[SIMULATION_CALC]);
		dc.SetBkMode(OPAQUE);
		dc.MoveTo(0,0);

		if (pomViewer->SimulationActive() == true && pomViewer->SimDisplay() == 0 && pomSimFlight2Poly != NULL)
		{
			dc.Polyline(pomSimFlight2Poly,imSimFlight2PolySize);
		}

		if (omLines.GetSize()==0)
		{
			dc.SelectObject(&omPenList[10]);
			dc.SetBkColor(COLORREF(SILVER));
			//dc.SetBkMode(OPAQUE);
			

			if(pomRealFlightPoly!=NULL)
				dc.Polyline(pomRealFlightPoly,imRealFlightPolySize);
		}
		//dc.MoveTo(0,0);

		dc.SelectObject(&omPenList[FUNCTION_ALL]);
		dc.SetBkMode(OPAQUE);
		dc.MoveTo(0,0);

		if(pomFlightPoly!=NULL && bmAdditive)
			dc.Polyline(pomFlightPoly,imFlightPolySize);		

	}
	
	int ilFligSize = omFlightValues.GetSize();
	//omAdditCount.SetSize(ilFligSize);
	omAdditCount.RemoveAll();
	omAdditCount.InsertAt(0,0,ilFligSize);
	//omRealAdditCount.SetSize(ilFligSize);
	omRealAdditCount.RemoveAll();
	omRealAdditCount.InsertAt(0,0,ilFligSize);

	
//Legende malen
	//CRect olLegRect = CRect(-11000, 100000, -2000, 100000-(int)(ilLegendSize/1.5)/*-ilLegendOffset*/);
	if(ilLineSize>0)
	{
		CRect olTmpRect = olClientRect;
		olTmpRect.left += 10;
		olTmpRect.right = imLeftScale;
		dc.DPtoLP(&olTmpRect);

		CRect olLegRect = CRect(olTmpRect.left, 100000, olTmpRect.right, 100000-(int)(ilLegendSize/1.5)/*-ilLegendOffset*/);

		dc.SelectObject(&omBlackBrush);
		dc.SelectObject(&olLegendFont);
		


	
		olLegRect.top = olLegRect.bottom - 100;
			olLegRect.bottom -=(int)(ilLegendSize/1.5);
		//int ilLineSize = omLines.GetSize();
		//int ilColorSize = omColors.GetSize();


		for(i = 0; i < ilLineSize; i++)
		{
			dc.SetBkColor( COLORREF(SILVER) );
			dc.SetTextColor(omColors[i%imColorCount].Color);
			dc.FrameRect( olLegRect, &omBlackBrush );
		
			dc.SelectObject(&omPenList[i%imColorCount]);

			if(bmAdditive == false && (imActuelRow == -1 || imActuelRow == i))	// nur f�r "alle"
			{
				CPoint *polLpPoints = &pomPolyLines[i];
				if(polLpPoints!=NULL)
					dc.Polyline(polLpPoints,ilPolySize);
			}
			olLegRect.top = olLegRect.bottom - 100;
			olLegRect.bottom -=(int)(ilLegendSize/1.5);
		}
		
		if(bmAdditive == true)
		{
			dc.SelectObject(&omPenList[SIMULATION_VISUAL]);
			dc.SetBkMode(OPAQUE);
			// Draw Simulation curve here!
			if (pomViewer->SimulationActive() == true && pomViewer->SimDisplay() == 1 && pomSimFlightPoly != NULL)
			{
				dc.Polyline(pomSimFlightPoly,imSimFlightPolySize);
			}

			if(bmAdditive)
			{
				if(pomPolyRealLines.GetSize()>0)
				{
					dc.SelectObject(&omPenList[10]);
					dc.Polyline(&pomPolyRealLines[0],ilPolySize);
				}
			}		
			
			if(bmAdditive)
			{
				if(pomPolyLines.GetSize()>0)
				{
					dc.SelectObject(&omPenList[3]);
					dc.Polyline(&pomPolyLines[0],ilPolySize);
				}

				if(pomSingleFlightPoly != NULL)
				{
					dc.SelectObject(&omPenList[imActuelRow%imColorCount]);

					dc.Polyline(&pomSingleFlightPoly[0],imSingleFligtPolySize);

				}
			}
		}
		//ende legende
		dc.SelectObject(&olNewFont);
		dc.SelectObject(&omTimeLinePen);
	}
	
	// Draw time marks
	CTime olMarkTimeStart,olMarkTimeEnd;
	pomViewer->GetMarkTimes(olMarkTimeStart,olMarkTimeEnd);
	if (olMarkTimeStart != TIMENULL && olMarkTimeEnd != TIMENULL)
	{
		CPen penYellow(PS_SOLID, 0, RGB(255, 255, 0));
		CPen *pOldPen = dc.SelectObject(&penYellow);
		if (IsBetween(olMarkTimeStart,olStartTime,olEndTime))
		{
			//CTimeSpan olTimeDiff = olEndTime - olStartTime;
			//CTimeSpan olCurrent = pomViewer->omMarkTimeStart-olStartTime;
			int ilX1 = pomTimeScale->GetXFromTime(olMarkTimeStart);
			ilX1 = (int)(0.5 + (XXX*1.0/olClientRect.right)*ilX1);
			//int ilX1 = (int)(imOneMinute*olCurrent.GetTotalMinutes());
			dc.MoveTo(ilX1, 100000);
			dc.LineTo(ilX1, 0);
		}
		
		if (IsBetween(olMarkTimeEnd,olStartTime,olEndTime))
		{
			//CTimeSpan olCurrent = pomViewer->omMarkTimeEnd-olStartTime;
			//int ilX = (int)(imOneMinute*olCurrent.GetTotalMinutes());
			int ilX = pomTimeScale->GetXFromTime(olMarkTimeEnd);
			ilX = (int)(0.5 + (XXX*1.0/olClientRect.right)*ilX);
			dc.MoveTo(ilX, 100000);
			dc.LineTo(ilX, 0);
		}
	}
	

	dc.SelectObject(polOldFont);
	dc.SelectObject(polOldPen);
	olNewFont.DeleteObject();
	olLegendFont.DeleteObject();

}

void CCoverageGraphicWnd::OnRButtonDown(UINT nFlags, CPoint point) 
{
	CCS_TRY


	if (point.x > imLeftScale )	// in VerticalScale?
	{


		CPoint olPoint = point;
		CRect olTimeRect,olWndRect;

		double ilLogMinutes = (double) olPoint.x/imOneMinute;
		omClickTime = pomTimeScale->GetTimeFromX(point.x-imLeftScale);
		

		CMenu menu;
		menu.CreatePopupMenu();
		for (int i = menu.GetMenuItemCount(); i > 0; i--)
			menu.RemoveMenu(i, MF_BYPOSITION);

		int ilFlag = MF_UNCHECKED;

		CString olText;
			olText = LoadStg(IDS_STRING61251);
			menu.AppendMenu(MF_STRING|ilFlag,21, olText);


		ClientToScreen(&point);
		menu.TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, point.x, point.y, this, NULL);

	}
	CWnd::OnRButtonDown(nFlags, point);
	CCS_CATCH_ALL
}

void CCoverageGraphicWnd::OnSize(UINT nType, int cx, int cy) 
{
	CCS_TRY
	CWnd::OnSize(nType, cx, cy);
	
	CRect olRect2(0,0,90,cy);

//	omList.MoveWindow(olRect2);
	pomList->MoveWindow(olRect2);
	// TODO: Code f�r die Behandlungsroutine f�r Nachrichten hier einf�gen
	
	CCS_CATCH_ALL
}

void CCoverageGraphicWnd::OnLButtonDown(UINT nFlags, CPoint point) 
{
	CCS_TRY
	// TODO: Code f�r die Behandlungsroutine f�r Nachrichten hier einf�gen und/oder Standard aufrufen
	
	CWnd::OnLButtonDown(nFlags, point);
	CCS_CATCH_ALL
}

void CCoverageGraphicWnd::OnLButtonDblClk(UINT nFlags, CPoint point) 
{
	CCS_TRY
	// TODO: Code f�r die Behandlungsroutine f�r Nachrichten hier einf�gen und/oder Standard aufrufen
	
	CWnd::OnLButtonDblClk(nFlags, point);
	CCS_CATCH_ALL
}

void CCoverageGraphicWnd::SetTimeScale(CCSTimeScale *popTimeScale)
{
	CCS_TRY
    pomTimeScale = popTimeScale;
	CCS_CATCH_ALL
}

void CCoverageGraphicWnd::SetDimensions(int ipLeftScale)
{
	CCS_TRY
	imLeftScale = ipLeftScale;
	CCS_CATCH_ALL
}

BOOL CCoverageGraphicWnd::OnEraseBkgnd(CDC* pDC) 
{
	CCS_TRY
    CRect olRect;
	CBrush olBrush(COLORREF(SILVER));;
	GetClientRect(&olRect);
    pDC->FillRect(&olRect, &olBrush);
	
	return TRUE;
	//return CWnd::OnEraseBkgnd(pDC);
	CCS_CATCH_ALL
	return FALSE;
}


void CCoverageGraphicWnd::SetTimeFrame(bool bpCalcFlightDemands)
{
	CCS_TRY
	DWORD	llNow,llLast, llStart;
	double	flDiff;

	llLast = GetTickCount();
	llStart = llLast;
	
	CTime olEndTime;
	CTime	  olStartTime = pomTimeScale->GetDisplayStartTime();
	CTimeSpan olTimeSpan  = pomTimeScale->GetDisplayDuration();
	
	int ilMin = olTimeSpan.GetTotalMinutes();
	olEndTime = olStartTime + olTimeSpan;
	omStartTime = olStartTime;
	
	omEndTime = olEndTime;
	imXMinutes = olTimeSpan.GetTotalMinutes();
	if(imXMinutes<=0)
		return;
	imMaxYCount = pomViewer->GetBorderVal();
	imCountPerUnit = 1;
	
	CString olEnd1 = omEndTime.Format("%Y%m%d%H%M");
	CString olStart1 = omStartTime.Format("%Y%m%d%H%M");
	omCurTime = CTime::GetCurrentTime();

	FilterType ilFilterIdx = pomViewer->GetFilterIdx();
	if(ilFilterIdx != imFilterIdx)
	{
		imFilterIdx = ilFilterIdx;
		imActuelRow = -1;
		pomViewer->SetReCalcShifts(true);
		pomViewer->SetSdtFlag(true);
		SetTimeFrame();
		pomViewer->SetSdtFlag(false);
		FillList();
	}


	llNow  = GetTickCount();
	flDiff = llNow-llLast;
	llLast = llNow;
	TRACE("CovGraphicWnd::SetTimeFrame(%d):Filter %.3lf sec\n",(int)bpCalcFlightDemands, flDiff/1000.0 );
	ogBackTrace += "11,";

	if (bpCalcFlightDemands)
	{
		ClearPolygonLines();
		CalculateFlightDemands();
	}

	llNow  = GetTickCount();
	flDiff = llNow-llLast;
	llLast = llNow;
	TRACE("CovGraphicWnd::SetTimeFrame(%d):CalcFlightDemands %.3lf sec\n",(int)bpCalcFlightDemands, flDiff/1000.0 );
	ogBackTrace += "12,";

	if(pomViewer->IsSdtChanged()==true)
	{
		if(pomViewer->EvaluationFilterChanged())
		{
			imActuelRow = -1;
		}
		CalculateShiftDemands();
	}

	llNow  = GetTickCount();
	flDiff = llNow-llLast;
	llLast = llNow;
	TRACE("CovGraphicWnd::SetTimeFrame(%d):CalcShiftDemands %.3lf sec\n",(int)bpCalcFlightDemands, flDiff/1000.0 );
	ogBackTrace += "13,";

	switch (pomViewer->SimVariant())
	{
	case 0 :
		CalculateSimOffsetFlightDemands();
	break;
	case 1 :
		CalculateSimCutFlightDemands();
	break;
	case 2 :
		CalculateSimSmoothFlightDemands();
	break;
	}

	llNow  = GetTickCount();
	flDiff = llNow-llLast;
	llLast = llNow;
	TRACE("CovGraphicWnd::SetTimeFrame(%d):CalcSimulation %.3lf sec\n",(int)bpCalcFlightDemands, flDiff/1000.0 );
	ogBackTrace += "14,";

	if(pomViewer->EvaluationFilterChanged())
	{
		FillList();
		pomViewer->EvaluationFilterChanged(false);
	}


	int ilLineSize =omLines.GetSize();
	int ilFCount = omFlightValues.GetSize();
	CUIntArray omAdditCount;
	CUIntArray omRealAdditCount;
	if(ilLineSize == 0)
	{
		bmShowMainCurve = true;
		for(int i = 0; i < ilFCount; i++)
		{
			if((UINT)imMaxYCount < omFlightValues[i])
			{
				imMaxYCount = omFlightValues[i];
			}
		}
	}
	else
	{
		bmShowMainCurve = false;
		if(bmAdditive == true)
		{
			bmShowMainCurve = true;
			for(int i = 0; i < ilFCount; i++)
			{
				omAdditCount.Add(0);
			}
			for(i = 0; i < ilLineSize; i++)
			{
				for(int j = 0; j < omLines[i].Values.GetSize(); j++)
				{
					omAdditCount[j] = omAdditCount[j] + omLines[i].Values[j];
					if(omLines[i].Values[j]>0)
						printf("");
					if(imMaxYCount < (int)omAdditCount[j])
					{
						imMaxYCount = omAdditCount[j];
					}
				}
			}
		}
		else
		{
			for(int i = 0; i < ilLineSize; i++)
			{
				for(int j = 0; j < omLines[i].Values.GetSize(); j++)
				{
					if(imMaxYCount < omLines[i].Values[j])
					{
						imMaxYCount = omLines[i].Values[j];
					}
				}
			}
		}
	}

//-------------
	ilFCount = omRealFlightValues.GetSize();

	int ilSCount = omShiftArray.GetSize();
	for(int i = 0; i < ilSCount; i++)
	{
		if((UINT)imMaxYCount < omShiftArray[i])
		{
			imMaxYCount = omShiftArray[i];
		}
	}
	if(pomViewer->SimulationActive())
	{
		int ilSimCount = omSimFlightValues.GetSize();
		int ilOneShift = (int)(10000);
		if (imMaxYCount > 0)
			ilOneShift = (int)(100000/(imMaxYCount+1));
		for(i = 0; i < ilSimCount; i+=FACTOR)
		{
			if((UINT)imMaxYCount < omSimFlightValues[i])
			{
				imMaxYCount = omSimFlightValues[i];
			}
		}
	}

	if(pomViewer->IsBorderFix())
		imMaxYCount = pomViewer->GetBorderVal();

	llNow  = GetTickCount();
	flDiff = llNow-llLast;
	llLast = llNow;
	TRACE("CovGraphicWnd::SetTimeFrame(%d):CalcMaxYCount %.3lf sec\n",(int)bpCalcFlightDemands, flDiff/1000.0 );
	ogBackTrace += "15,";

	int ilOneShift = (int)(10000);
	if (imMaxYCount > 0)
		ilOneShift = (int)(100000/(imMaxYCount+1));

	CRect olClientRect;	
	GetClientRect(&olClientRect);
	int ilXDiff = (int)(olClientRect.right - 120);
	imOneMinute = (double)(ilXDiff*XXX)/(olClientRect.right*imXMinutes);//(double)ilOneMinute;
	double ilScaledTimeUnit = (double)(ilXDiff*XXX*FACTOR)/(olClientRect.right*imXMinutes);
	int ilPolySize = imXMinutes/FACTOR;
		
	if (pomViewer->SimulationActive() == true && pomViewer->SimDisplay() == 1 && omSimFlightValues.GetSize() > 0)
	{
		bool blSim =false;
		if(omSimFlightValues.GetSize()>=imXMinutes)
		{
			blSim = true;
		}
		if(pomSimFlightPoly!=NULL)
			delete pomSimFlightPoly;
//		pomSimFlightPoly = new CPoint[ilPolySize];
		
		CPoint *polSimFlightPoly = new CPoint[(int)(2*ilPolySize)];

		bool blSimFlightInit = true;
		int ilActSimFlightVal = -1;
		int ilLastSimFlightVal = 0;
		imSimFlightPolySize = 0;
		for(int i=0;i<ilPolySize && blSim;i++)
		{
			ilActSimFlightVal = (int)omSimFlightValues[i*FACTOR]*ilOneShift;
			if(ilActSimFlightVal != ilLastSimFlightVal)
			{
				if(blSimFlightInit)
				{
					ilLastSimFlightVal  = ilActSimFlightVal;
					blSimFlightInit = false;
				}

				polSimFlightPoly[imSimFlightPolySize].x = int(i*ilScaledTimeUnit);
				polSimFlightPoly[imSimFlightPolySize++].y = ilLastSimFlightVal;
				polSimFlightPoly[imSimFlightPolySize].x = int(i*ilScaledTimeUnit);
				polSimFlightPoly[imSimFlightPolySize++].y = ilActSimFlightVal;
			}
			ilLastSimFlightVal = ilActSimFlightVal;
		}
		if(blSim)
		{
			polSimFlightPoly[imSimFlightPolySize].x = int(ilPolySize*ilScaledTimeUnit);
			polSimFlightPoly[imSimFlightPolySize++].y = ilActSimFlightVal;
		}
		pomSimFlightPoly = new CPoint[imSimFlightPolySize];
	
		for(i = 0; i< imSimFlightPolySize; i++)
		{
			pomSimFlightPoly[i] = polSimFlightPoly[i];
		}

		delete polSimFlightPoly;
	}

	llNow  = GetTickCount();
	flDiff = llNow-llLast;
	llLast = llNow;
	TRACE("CovGraphicWnd::SetTimeFrame(%d):CalcpolSimFlightPoly %.3lf sec\n",(int)bpCalcFlightDemands, flDiff/1000.0 );
	ogBackTrace += "16,";

	bool blSimFlight2Init = true;
	int ilActSimFlight2Val = -1;
	int ilLastSimFlight2Val = -1;

	imSimFlight2PolySize = 0;
	if (pomViewer->SimulationActive() == true && pomViewer->SimDisplay() == 0)
	{
		if(pomSimFlight2Poly!=NULL)
			delete pomSimFlight2Poly;
		int ilIndex = omSimFlightValues2.GetSize();
		CPoint *polSimFlight2Poly = new CPoint[(int)(2*ilIndex)];
		imSimFlight2PolySize = 0;
		for(int i=0;i<ilIndex;i++)
		{
			ilActSimFlight2Val = (int)omSimFlightValues2[i]*ilOneShift;
			if(ilActSimFlight2Val != ilLastSimFlight2Val)
			{
				if(blSimFlight2Init)
				{
					ilLastSimFlight2Val  = ilActSimFlight2Val;
					blSimFlight2Init = false;
				}

				polSimFlight2Poly[imSimFlight2PolySize].x = int(i*imOneMinute);
				polSimFlight2Poly[imSimFlight2PolySize++].y = ilLastSimFlight2Val;
				polSimFlight2Poly[imSimFlight2PolySize].x = int(i*imOneMinute);
				polSimFlight2Poly[imSimFlight2PolySize++].y = ilActSimFlight2Val;
				ilLastSimFlight2Val = ilActSimFlight2Val;
			}
			
		}
		polSimFlight2Poly[imSimFlight2PolySize].x = int(ilIndex*imOneMinute);
		polSimFlight2Poly[imSimFlight2PolySize++].y = ilActSimFlight2Val;

		pomSimFlight2Poly = new CPoint[imSimFlight2PolySize];
	
		for(i = 0; i< imSimFlight2PolySize; i++)
		{
			pomSimFlight2Poly[i] = polSimFlight2Poly[i];
		}

		delete polSimFlight2Poly;
	}

	llNow  = GetTickCount();
	flDiff = llNow-llLast;
	llLast = llNow;
	TRACE("CovGraphicWnd::SetTimeFrame(%d):CalcSimFlight2Poly %.3lf sec\n",(int)bpCalcFlightDemands, flDiff/1000.0 );
	ogBackTrace += "17,";

	if(true)//pomViewer->IsGhdChanged() || pomViewer->bmSimulate)
	{

		bool blFlight=false,blReal=false;
		if(omFlightValues.GetSize()>=imXMinutes)
		{
			blFlight = true;
		}
		
		if(omRealFlightValues.GetSize()>=imXMinutes)
		{
			blReal = true;
		}
		if(pomFlightPoly!=NULL)
			delete pomFlightPoly;
		if(pomRealFlightPoly!=NULL)
			delete pomRealFlightPoly;

		CPoint *polFlightPoly = new CPoint[(int)(2*ilPolySize)];
		CPoint *polRealFlightPoly = new CPoint[(int)(2*ilPolySize)];
		imRealFlightPolySize = 0;
		imFlightPolySize = 0;

		bool blFlightInit = true;
		bool blRealFlightInit = true;
		int ilActFlightVal = -1;
		int ilActRealFlightVal = -1;
		int ilLastFlightVal = -1;
		int ilLastRealFlightVal = -1;
		int ilFactor ;
	
		for(int i=0;i<ilPolySize && (blFlight || blReal);i++)
		{
			ilFactor =(int)i*FACTOR;
			if(blFlight)
			{ 
				ilActFlightVal = (int)omFlightValues[ilFactor]*ilOneShift;
				if(ilActFlightVal != ilLastFlightVal)
				{
					if(blFlightInit )
					{
						ilLastFlightVal= ilActFlightVal;
						blFlightInit = false;
					}
					polFlightPoly[imFlightPolySize].x = int(imOneMinute*ilFactor);
					polFlightPoly[imFlightPolySize++].y = ilLastFlightVal;
					polFlightPoly[imFlightPolySize].x = int(imOneMinute*ilFactor);
					polFlightPoly[imFlightPolySize++].y = ilActFlightVal;
					ilLastFlightVal = ilActFlightVal;
				}
				
			}
			if(blReal)
			{
				ilActRealFlightVal = (int)omRealFlightValues[ilFactor]*ilOneShift;
				if(ilActRealFlightVal != ilLastRealFlightVal)
				{
					if(blRealFlightInit )
					{
						ilLastRealFlightVal= ilActRealFlightVal;
						blRealFlightInit = false;
					}

					polRealFlightPoly[imRealFlightPolySize].x = int(imOneMinute*ilFactor);
					polRealFlightPoly[imRealFlightPolySize++].y = ilLastRealFlightVal;
					polRealFlightPoly[imRealFlightPolySize].x = int(imOneMinute*ilFactor);
					polRealFlightPoly[imRealFlightPolySize++].y = ilActRealFlightVal;
					ilLastRealFlightVal = ilActRealFlightVal;
				}
			}
		}
		if(blFlight)
		{ 
			polFlightPoly[imFlightPolySize].x = int(imOneMinute*ilFactor);
			polFlightPoly[imFlightPolySize++].y = ilActFlightVal;
		}
					
		if(blReal)
		{
			polRealFlightPoly[imRealFlightPolySize].x = int(imOneMinute*ilFactor);
			polRealFlightPoly[imRealFlightPolySize++].y = ilActRealFlightVal;
		}
		pomFlightPoly = new CPoint[imFlightPolySize];
		pomRealFlightPoly = new CPoint[imRealFlightPolySize];
		for(i = 0; i< imFlightPolySize; i++)
		{
			pomFlightPoly[i] = polFlightPoly[i];
		}
		for(i = 0; i< imRealFlightPolySize; i++)
		{
			pomRealFlightPoly[i] = polRealFlightPoly[i];
		}

		delete[] polRealFlightPoly;
		delete[] polFlightPoly;
	}

	llNow  = GetTickCount();
	flDiff = llNow-llLast;
	llLast = llNow;
	TRACE("CovGraphicWnd::SetTimeFrame(%d):CalcPolFlightPoly %.3lf sec\n",(int)bpCalcFlightDemands, flDiff/1000.0 );
	ogBackTrace += "18,";

	bool blShift=false;
	if(omShiftArray.GetSize()>=imXMinutes)
	{
		blShift = true;
	}
	if(pomShiftPoly!=NULL)
		delete[] pomShiftPoly;

	pomShiftPoly = NULL;
	CPoint *polShiftPoly = new CPoint[(int)(2*ilPolySize +2)];
	imShiftPolySize = 0;

	bool blShiftInit = true;
	int ilActShiftVal = -1;
	int ilLastShiftVal = -1;
	int ilFactor ;
	
	if(blShift)
	{
		polShiftPoly[imShiftPolySize].x = 0;
		polShiftPoly[imShiftPolySize++].y = 0;
		for(int i=0;i<ilPolySize ;i++)
		{
			ilFactor =(int)i*FACTOR;		
			ilActShiftVal = (int)omShiftArray[ilFactor]*ilOneShift;
			if(ilActShiftVal != ilLastShiftVal)
			{
				if(blShiftInit )
				{
					ilLastShiftVal= ilActShiftVal;
					blShiftInit = false;
				}
				polShiftPoly[imShiftPolySize].x = int(imOneMinute*ilFactor);
				polShiftPoly[imShiftPolySize++].y = ilLastShiftVal;
				polShiftPoly[imShiftPolySize].x = int(imOneMinute*ilFactor);
				polShiftPoly[imShiftPolySize++].y = ilActShiftVal;
				ilLastShiftVal = ilActShiftVal;
			}
		}
	}
	if(blShift)
	{ 
		polShiftPoly[imShiftPolySize].x = int(imOneMinute*ilFactor);
		polShiftPoly[imShiftPolySize++].y = ilActShiftVal;
		polShiftPoly[imShiftPolySize].x = int(imOneMinute*ilFactor);
		polShiftPoly[imShiftPolySize++].y = 0;
			
		pomShiftPoly = new CPoint[imShiftPolySize];
		
		for(i = 0; i< imShiftPolySize; i++)
		{
			pomShiftPoly[i] = polShiftPoly[i];
		}
		
	}
	delete polShiftPoly;
		
	llNow  = GetTickCount();
	flDiff = llNow-llLast;
	llLast = llNow;
	TRACE("CovGraphicWnd::SetTimeFrame(%d):CalcPolShiftPoly %.3lf sec\n",(int)bpCalcFlightDemands, flDiff/1000.0 );
	ogBackTrace += "19,";

	//int ilLineSize = omLines.GetSize();
	pomPolyLines.DeleteAll();
	pomPolyRealLines.DeleteAll();
	if(ilLineSize>0 && !bmAdditive)
	{

		pomPolyLines.SetSize(ilLineSize); // Stores the summation of selected services
		CPoint *polValues = NULL;
		for(i = 0; i < ilLineSize; i++)
		{
			bool blSet = false; // Flag indicates whether non-zero curve exists or not
			if(polValues==NULL)
				polValues = new CPoint[ilPolySize];
			int ilValuesSize = omLines[i].Values.GetSize();
			for(int k=0; k<ilPolySize; k++)
			{
				polValues[k].x = (int)(k*ilScaledTimeUnit);
				if((polValues[k].y = omLines[i].Values[k*FACTOR]*ilOneShift)>0)
					blSet=true;
			}
			if(blSet)
			{
				pomPolyLines.SetAt(i,polValues);
				polValues = NULL;
			}
			else
			{
				pomPolyLines.SetAt(i,NULL); // reuse polValues!!
			}
		}
		if(polValues!=NULL)
			delete polValues;
	}

	llNow  = GetTickCount();
	flDiff = llNow-llLast;
	llLast = llNow;
	TRACE("CovGraphicWnd::SetTimeFrame(%d):CalcPomPolyLines %.3lf sec\n",(int)bpCalcFlightDemands, flDiff/1000.0 );
	ogBackTrace += "20,";

	pomViewer->ResetFlags();	
//imHelpLineUnit einrichten

	PrepareSingleShiftPoly();	

	llNow  = GetTickCount();
	flDiff = llNow-llLast;
	llLast = llNow;
	TRACE("CovGraphicWnd::SetTimeFrame(%d):PrepareSingleShiftPoly %.3lf sec\n",(int)bpCalcFlightDemands, flDiff/1000.0 );
	ogBackTrace += "21,";


	imPIdx = CalcPIdx();
	
	llNow  = GetTickCount();
	flDiff = llNow-llLast;
	llLast = llNow;
	TRACE("CovGraphicWnd::SetTimeFrame(%d):CalcPIdx %.3lf sec\n",(int)bpCalcFlightDemands, flDiff/1000.0 );
	ogBackTrace += "22,";

	//bool blEnd = false;
	double dlBase;
	int ilMult;
	int ilUpperUnits;
	CString olLen;
	olLen.Format("%d", imMaxYCount);
	int ilLen = olLen.GetLength();
	dlBase = pow(10., (double)(ilLen-1));
	CString olMult = olLen.Left(1);
	ilMult = atoi(olMult.GetBuffer(0));
	ilUpperUnits = (int)(dlBase*(ilMult+1));
	imCountPerUnit = (int)(ilUpperUnits/10);



	Invalidate();
	llNow  = GetTickCount();
	flDiff = llNow-llLast;
	llLast = llNow;
	TRACE("CovGraphicWnd::SetTimeFrame(%d):Invalidate %.3lf sec\n",(int)bpCalcFlightDemands, flDiff/1000.0 );
	ogBackTrace += "23,";

	CCS_CATCH_ALL
}

void CCoverageGraphicWnd::UpdateForShiftDemandUpdate()
{
	CCS_TRY
	DWORD	llNow,llLast, llStart;
	double	flDiff;

	llLast = GetTickCount();
	llStart = llLast;
	
	CTime	  olEndTime;
	CTime	  olStartTime = pomTimeScale->GetDisplayStartTime();
	CTimeSpan olTimeSpan  = pomTimeScale->GetDisplayDuration();
	
	int ilMin = olTimeSpan.GetTotalMinutes();
	olEndTime = olStartTime + olTimeSpan;
	omStartTime = olStartTime;
	
	omEndTime = olEndTime;
	imXMinutes = olTimeSpan.GetTotalMinutes();
	if(imXMinutes<=0)
		return;

	imMaxYCount = pomViewer->GetBorderVal();
	imCountPerUnit = 1;
	
	CString olEnd1 = omEndTime.Format("%Y%m%d%H%M");
	CString olStart1 = omStartTime.Format("%Y%m%d%H%M");
	omCurTime = CTime::GetCurrentTime();

	CalculateShiftDemands();

	llNow  = GetTickCount();
	flDiff = llNow-llLast;
	llLast = llNow;
	TRACE("CovGraphicWnd::UpdateForShiftDemandUpdate:CalcShiftDemands %.3lf sec\n", flDiff/1000.0 );

//-------------
	int ilLineSize =omLines.GetSize();
	int ilFCount = omFlightValues.GetSize();
	CUIntArray omAdditCount;
	CUIntArray omRealAdditCount;
	if(ilLineSize == 0)
	{
		bmShowMainCurve = true;
		for(int i = 0; i < ilFCount; i++)
		{
			if((UINT)imMaxYCount < omFlightValues[i])
			{
				imMaxYCount = omFlightValues[i];
			}
		}
	}
	else
	{
		bmShowMainCurve = false;
		if(bmAdditive == true)
		{
			bmShowMainCurve = true;
			for(int i = 0; i < ilFCount; i++)
			{
				omAdditCount.Add(0);
			}
			for(i = 0; i < ilLineSize; i++)
			{
				for(int j = 0; j < omLines[i].Values.GetSize(); j++)
				{
					omAdditCount[j] = omAdditCount[j] + omLines[i].Values[j];
					if(omLines[i].Values[j]>0)
						printf("");
					if(imMaxYCount < (int)omAdditCount[j])
					{
						imMaxYCount = omAdditCount[j];
					}
				}
			}
		}
		else
		{
			for(int i = 0; i < ilLineSize; i++)
			{
				for(int j = 0; j < omLines[i].Values.GetSize(); j++)
				{
					if(imMaxYCount < omLines[i].Values[j])
					{
						imMaxYCount = omLines[i].Values[j];
					}
				}
			}
		}
	}

	int ilSCount = omShiftArray.GetSize();
	for(int i = 0; i < ilSCount; i++)
	{
		if((UINT)imMaxYCount < omShiftArray[i])
		{
			imMaxYCount = omShiftArray[i];
		}
	}

	if(pomViewer->SimulationActive())
	{
		int ilSimCount = omSimFlightValues.GetSize();
		int ilOneShift = (int)(10000);
		if (imMaxYCount > 0)
			ilOneShift = (int)(100000/(imMaxYCount+1));
		for(i = 0; i < ilSimCount; i+=FACTOR)
		{
			if((UINT)imMaxYCount < omSimFlightValues[i])
			{
				imMaxYCount = omSimFlightValues[i];
			}
		}
	}

	if (pomViewer->IsBorderFix())
		imMaxYCount = pomViewer->GetBorderVal();

	llNow  = GetTickCount();
	flDiff = llNow-llLast;
	llLast = llNow;
	TRACE("CovGraphicWnd::UpdateForShiftDemandUpdate:CalcMaxYCount %.3lf sec\n", flDiff/1000.0 );

	int ilOneShift = (int)(10000);
	if (imMaxYCount > 0)
		ilOneShift = (int)(100000/(imMaxYCount+1));

	CRect olClientRect;	
	GetClientRect(&olClientRect);
	int ilXDiff = (int)(olClientRect.right - 120);
	imOneMinute = (double)(ilXDiff*XXX)/(olClientRect.right*imXMinutes);//(double)ilOneMinute;
	double ilScaledTimeUnit = (double)(ilXDiff*XXX*FACTOR)/(olClientRect.right*imXMinutes);
	int ilPolySize = imXMinutes/FACTOR;
		
	if (pomViewer->SimulationActive() == true && pomViewer->SimDisplay() == 1 && omSimFlightValues.GetSize() > 0)
	{
		bool blSim =false;
		if(omSimFlightValues.GetSize()>=imXMinutes)
		{
			blSim = true;
		}
		if(pomSimFlightPoly!=NULL)
			delete pomSimFlightPoly;
//		pomSimFlightPoly = new CPoint[ilPolySize];
		
		CPoint *polSimFlightPoly = new CPoint[(int)(2*ilPolySize)];

		bool blSimFlightInit = true;
		int ilActSimFlightVal = -1;
		int ilLastSimFlightVal = 0;
		imSimFlightPolySize = 0;
		for(int i=0;i<ilPolySize && blSim;i++)
		{
			ilActSimFlightVal = (int)omSimFlightValues[i*FACTOR]*ilOneShift;
			if(ilActSimFlightVal != ilLastSimFlightVal)
			{
				if(blSimFlightInit)
				{
					ilLastSimFlightVal  = ilActSimFlightVal;
					blSimFlightInit = false;
				}

				polSimFlightPoly[imSimFlightPolySize].x = int(i*ilScaledTimeUnit);
				polSimFlightPoly[imSimFlightPolySize++].y = ilLastSimFlightVal;
				polSimFlightPoly[imSimFlightPolySize].x = int(i*ilScaledTimeUnit);
				polSimFlightPoly[imSimFlightPolySize++].y = ilActSimFlightVal;
			}
			ilLastSimFlightVal = ilActSimFlightVal;
		}
		if(blSim)
		{
			polSimFlightPoly[imSimFlightPolySize].x = int(ilPolySize*ilScaledTimeUnit);
			polSimFlightPoly[imSimFlightPolySize++].y = ilActSimFlightVal;
		}
		pomSimFlightPoly = new CPoint[imSimFlightPolySize];
	
		for(i = 0; i< imSimFlightPolySize; i++)
		{
			pomSimFlightPoly[i] = polSimFlightPoly[i];
		}

		delete polSimFlightPoly;
	}

	llNow  = GetTickCount();
	flDiff = llNow-llLast;
	llLast = llNow;
	TRACE("CovGraphicWnd::UpdateForShiftDemandUpdate:CalcpolSimFlightPoly %.3lf sec\n", flDiff/1000.0 );

	bool blSimFlight2Init = true;
	int ilActSimFlight2Val = -1;
	int ilLastSimFlight2Val = -1;

	imSimFlight2PolySize = 0;
	if (pomViewer->SimulationActive() == true && pomViewer->SimDisplay() == 0)
	{
		if(pomSimFlight2Poly!=NULL)
			delete pomSimFlight2Poly;
		int ilIndex = omSimFlightValues2.GetSize();
		CPoint *polSimFlight2Poly = new CPoint[(int)(2*ilIndex)];
		imSimFlight2PolySize = 0;
		for(int i=0;i<ilIndex;i++)
		{
			ilActSimFlight2Val = (int)omSimFlightValues2[i]*ilOneShift;
			if(ilActSimFlight2Val != ilLastSimFlight2Val)
			{
				if(blSimFlight2Init)
				{
					ilLastSimFlight2Val  = ilActSimFlight2Val;
					blSimFlight2Init = false;
				}

				polSimFlight2Poly[imSimFlight2PolySize].x = int(i*imOneMinute);
				polSimFlight2Poly[imSimFlight2PolySize++].y = ilLastSimFlight2Val;
				polSimFlight2Poly[imSimFlight2PolySize].x = int(i*imOneMinute);
				polSimFlight2Poly[imSimFlight2PolySize++].y = ilActSimFlight2Val;
				ilLastSimFlight2Val = ilActSimFlight2Val;
			}
			
		}
		polSimFlight2Poly[imSimFlight2PolySize].x = int(ilIndex*imOneMinute);
		polSimFlight2Poly[imSimFlight2PolySize++].y = ilActSimFlight2Val;

		pomSimFlight2Poly = new CPoint[imSimFlight2PolySize];
	
		for(i = 0; i< imSimFlight2PolySize; i++)
		{
			pomSimFlight2Poly[i] = polSimFlight2Poly[i];
		}

		delete polSimFlight2Poly;
	}

	llNow  = GetTickCount();
	flDiff = llNow-llLast;
	llLast = llNow;
	TRACE("CovGraphicWnd::UpdateForShiftDemandUpdate:CalcSimFlight2Poly %.3lf sec\n", flDiff/1000.0 );

	if(true)//pomViewer->IsGhdChanged() || pomViewer->bmSimulate)
	{

		bool blFlight=false,blReal=false;
		if(omFlightValues.GetSize()>=imXMinutes)
		{
			blFlight = true;
		}
		
		if(omRealFlightValues.GetSize()>=imXMinutes)
		{
			blReal = true;
		}
		if(pomFlightPoly!=NULL)
			delete pomFlightPoly;
		if(pomRealFlightPoly!=NULL)
			delete pomRealFlightPoly;

		CPoint *polFlightPoly = new CPoint[(int)(2*ilPolySize)];
		CPoint *polRealFlightPoly = new CPoint[(int)(2*ilPolySize)];
		imRealFlightPolySize = 0;
		imFlightPolySize = 0;

		bool blFlightInit = true;
		bool blRealFlightInit = true;
		int ilActFlightVal = -1;
		int ilActRealFlightVal = -1;
		int ilLastFlightVal = -1;
		int ilLastRealFlightVal = -1;
		int ilFactor ;
	
		for(int i=0;i<ilPolySize && (blFlight || blReal);i++)
		{
			ilFactor =(int)i*FACTOR;
			if(blFlight)
			{ 
				ilActFlightVal = (int)omFlightValues[ilFactor]*ilOneShift;
				if(ilActFlightVal != ilLastFlightVal)
				{
					if(blFlightInit )
					{
						ilLastFlightVal= ilActFlightVal;
						blFlightInit = false;
					}
					polFlightPoly[imFlightPolySize].x = int(imOneMinute*ilFactor);
					polFlightPoly[imFlightPolySize++].y = ilLastFlightVal;
					polFlightPoly[imFlightPolySize].x = int(imOneMinute*ilFactor);
					polFlightPoly[imFlightPolySize++].y = ilActFlightVal;
					ilLastFlightVal = ilActFlightVal;
				}
				
			}
			if(blReal)
			{
				ilActRealFlightVal = (int)omRealFlightValues[ilFactor]*ilOneShift;
				if(ilActRealFlightVal != ilLastRealFlightVal)
				{
					if(blRealFlightInit )
					{
						ilLastRealFlightVal= ilActRealFlightVal;
						blRealFlightInit = false;
					}

					polRealFlightPoly[imRealFlightPolySize].x = int(imOneMinute*ilFactor);
					polRealFlightPoly[imRealFlightPolySize++].y = ilLastRealFlightVal;
					polRealFlightPoly[imRealFlightPolySize].x = int(imOneMinute*ilFactor);
					polRealFlightPoly[imRealFlightPolySize++].y = ilActRealFlightVal;
					ilLastRealFlightVal = ilActRealFlightVal;
				}
			}
		}
		if(blFlight)
		{ 
			polFlightPoly[imFlightPolySize].x = int(imOneMinute*ilFactor);
			polFlightPoly[imFlightPolySize++].y = ilActFlightVal;
		}
					
		if(blReal)
		{
			polRealFlightPoly[imRealFlightPolySize].x = int(imOneMinute*ilFactor);
			polRealFlightPoly[imRealFlightPolySize++].y = ilActRealFlightVal;
		}
		pomFlightPoly = new CPoint[imFlightPolySize];
		pomRealFlightPoly = new CPoint[imRealFlightPolySize];
		for(i = 0; i< imFlightPolySize; i++)
		{
			pomFlightPoly[i] = polFlightPoly[i];
		}
		for(i = 0; i< imRealFlightPolySize; i++)
		{
			pomRealFlightPoly[i] = polRealFlightPoly[i];
		}

		delete[] polRealFlightPoly;
		delete[] polFlightPoly;
	}

	llNow  = GetTickCount();
	flDiff = llNow-llLast;
	llLast = llNow;
	TRACE("CovGraphicWnd::UpdateForShiftDemandUpdate:CalcPolFlightPoly %.3lf sec\n", flDiff/1000.0 );

	bool blShift=false;
	if(omShiftArray.GetSize()>=imXMinutes)
	{
		blShift = true;
	}
	if(pomShiftPoly!=NULL)
		delete[] pomShiftPoly;

	pomShiftPoly = NULL;
	CPoint *polShiftPoly = new CPoint[(int)(2*ilPolySize +2)];
	imShiftPolySize = 0;

	bool blShiftInit = true;
	int ilActShiftVal = -1;
	int ilLastShiftVal = -1;
	int ilFactor;
	
	if(blShift)
	{
		polShiftPoly[imShiftPolySize].x = 0;
		polShiftPoly[imShiftPolySize++].y = 0;
		for(int i=0;i<ilPolySize ;i++)
		{
			ilFactor =(int)i*FACTOR;		
			ilActShiftVal = (int)omShiftArray[ilFactor]*ilOneShift;
			if(ilActShiftVal != ilLastShiftVal)
			{
				if(blShiftInit )
				{
					ilLastShiftVal= ilActShiftVal;
					blShiftInit = false;
				}
				polShiftPoly[imShiftPolySize].x = int(imOneMinute*ilFactor);
				polShiftPoly[imShiftPolySize++].y = ilLastShiftVal;
				polShiftPoly[imShiftPolySize].x = int(imOneMinute*ilFactor);
				polShiftPoly[imShiftPolySize++].y = ilActShiftVal;
				ilLastShiftVal = ilActShiftVal;
			}
		}
	}

	if(blShift)
	{ 
		polShiftPoly[imShiftPolySize].x = int(imOneMinute*ilFactor);
		polShiftPoly[imShiftPolySize++].y = ilActShiftVal;
		polShiftPoly[imShiftPolySize].x = int(imOneMinute*ilFactor);
		polShiftPoly[imShiftPolySize++].y = 0;
			
		pomShiftPoly = new CPoint[imShiftPolySize];
		
		for(i = 0; i< imShiftPolySize; i++)
		{
			pomShiftPoly[i] = polShiftPoly[i];
		}
		
	}
	delete polShiftPoly;
		
	llNow  = GetTickCount();
	flDiff = llNow-llLast;
	llLast = llNow;
	TRACE("CovGraphicWnd::UpdateForShiftDemandUpdate:CalcPolShiftPoly %.3lf sec\n", flDiff/1000.0 );

	pomViewer->ResetFlags();	

	PrepareSingleShiftPoly();	

	llNow  = GetTickCount();
	flDiff = llNow-llLast;
	llLast = llNow;
	TRACE("CovGraphicWnd::UpdateForShiftDemandUpdate:PrepareSingleShiftPoly %.3lf sec\n", flDiff/1000.0 );


	imPIdx = CalcPIdx();
	
	llNow  = GetTickCount();
	flDiff = llNow-llLast;
	llLast = llNow;
	TRACE("CovGraphicWnd::UpdateForShiftDemandUpdate:CalcPIdx %.3lf sec\n", flDiff/1000.0 );

	//bool blEnd = false;
	double dlBase;
	int ilMult;
	int ilUpperUnits;
	CString olLen;
	olLen.Format("%d", imMaxYCount);
	int ilLen = olLen.GetLength();
	dlBase = pow(10., (double)(ilLen-1));
	CString olMult = olLen.Left(1);
	ilMult = atoi(olMult.GetBuffer(0));
	ilUpperUnits = (int)(dlBase*(ilMult+1));
	imCountPerUnit = (int)(ilUpperUnits/10);



	Invalidate();
	llNow  = GetTickCount();
	flDiff = llNow-llLast;
	llLast = llNow;
	TRACE("CovGraphicWnd::UpdateForShiftDemandUpdate:Invalidate %.3lf sec\n", flDiff/1000.0 );

	CCS_CATCH_ALL
}

void CCoverageGraphicWnd::CalculateFlightDemands()
{
//	CCS_TRY
	int ilCount = 0;
	int ilValidCount = 0;
	int i;

	omFlightValues.RemoveAll();
	omFlightValues.InsertAt(0,0,imXMinutes);

	CStringArray olDisplayUrnos;
	bool blUseAll = false;
	int ilPolyCount = 0;
	switch(imFilterIdx)
	{
	case FUNCTION:
		{
			blUseAll = pomViewer->PfcMapForGraphic(olDisplayUrnos);
			break;
		}
	case QUALIFICATION:
		{
			blUseAll = pomViewer->GhsMapForGraphic(olDisplayUrnos);
			break;
		}
	case CHECKIN:
		{
			blUseAll = pomViewer->CicMapForGraphic(olDisplayUrnos);
			break;
		}
	case POSITIONS:
		{
			blUseAll = pomViewer->PstMapForGraphic(olDisplayUrnos);
			break;
		}
	case GATES:  
		{
			blUseAll = pomViewer->GatMapForGraphic(olDisplayUrnos);
			break;
		}
	case EQUIPMENT:
		{
			blUseAll = pomViewer->EquMapForGraphic(olDisplayUrnos);
			break;
		}
	}

	int ilC = olDisplayUrnos.GetSize();
	omTypeList = "";
	omTypeUrnoList = "";
	if(blUseAll)
	{
		ilC = 1;
		omLines.SetSize(ilC);
		POLYGON_LINE *prlPolyLine = new POLYGON_LINE;
		prlPolyLine->Urno = 0L;
		prlPolyLine->UrnoText = "0L";
		prlPolyLine->Lkco = LoadStg(IDS_STRING61216);
		omTypeList += LoadStg(IDS_STRING61216);
		omTypeUrnoList += "0L";
		prlPolyLine->CurrentOffset = 0;
		prlPolyLine->Color = omColors[0 % imColorCount].Color;
		omLines.SetAt(0,prlPolyLine);
	}
	else
	{
		CString olTmpText;
		CString olTmp;
		CStringArray olItemList;
		omLines.SetSize(ilC);
		for(i = 0; i < ilC; i++)
		{
			POLYGON_LINE *prlPolyLine = new POLYGON_LINE;
			olTmpText = olDisplayUrnos[i];				
			ExtractItemList(olTmpText, &olItemList, '�');
			if(olItemList.GetSize() > 1)
			{
				olTmp = olItemList[0];
				if(olTmp.Find("S") || olTmp.Find("G"))
				{
					olTmp = olTmp.Left(olTmp.GetLength() -1 );
				}
				prlPolyLine->UrnoText = olTmp + ",";
				omTypeUrnoList += prlPolyLine->UrnoText;
				olTmpText = olItemList[1];
				ExtractItemList(olTmpText, &olItemList, '%');
				if(olItemList.GetSize() > 0)
				{
					olTmp = olItemList[0];
					if(olTmp.IsEmpty() && olItemList.GetSize() > 1)
					{
						olTmp = olItemList[1];
					}
				}
				prlPolyLine->Lkco = olTmp;
				omTypeList += olTmp;
				omTypeList += ",";
				


			}
			prlPolyLine->Urno = atol(prlPolyLine->UrnoText);
			prlPolyLine->Color = omColors[i % imColorCount].Color;
			omLines.SetAt(i,prlPolyLine);

		}
	}

	ilC = omLines.GetSize();
	for(i = 0; i < ilC; i++)
	{
		for(int j = 0; j < imXMinutes; j++)
		{
			int ilValue = 0;
			omLines[i].Values.New(ilValue);
		}
	}
	
	POSITION rlPos;
	int ilStartIdx = 0;
	CString olStartTime = omStartTime.Format("%Y.%m.%d - %H:%M:%S");
	CString olEndTime	= omEndTime.Format("%Y.%m.%d - %H:%M:%S");
	CTime olCurTime = CTime::GetCurrentTime();


	for ( rlPos = pomViewer->GetDemUrnoMap()->GetStartPosition(); rlPos != NULL; )
	{
		long llUrno;
		DEMDATA *prlDem = NULL; //&pomViewer->omDemGhds[i];
		pomViewer->GetDemUrnoMap()->GetNextAssoc(rlPos, (void *&)llUrno, (void *&)prlDem);
		
		// HAJ Change: If displaying demands in the past use only despatched DEMs

		if( IsOverlapped(omStartTime, omEndTime, prlDem->DispDebe, prlDem->DispDeen)) 
		{
			ilStartIdx = 0;
			CTimeSpan olDemandDuration;// = prlDem->Duen - omStartTime;
			if(prlDem->DispDebe<=omStartTime)
			{
				ilStartIdx = 0;
			}
			else
			{
				CTimeSpan TS = prlDem->DispDebe - omStartTime;
				ilStartIdx = TS.GetTotalMinutes(); 
			}
			if(prlDem->DispDeen<omEndTime)
			{
				olDemandDuration = (ilStartIdx==0) ? prlDem->DispDeen - omStartTime : prlDem->DispDeen - prlDem->DispDebe;
			}
			else
			{
				olDemandDuration = (ilStartIdx==0) ? omEndTime - omStartTime : omEndTime - prlDem->DispDebe;
			}
			prlDem->MinuteStart = ilStartIdx;
			int ilCount = olDemandDuration.GetTotalMinutes();
			int ilValueCount = omFlightValues.GetSize();
			CTime olTime = omStartTime + CTimeSpan(0,0,ilStartIdx,0) ;
			int ilCnt = 0;
			for(int j = 0; j < ilCount; j++,olTime+=CTimeSpan(0,0,1,0))
			{
				static long llUrno = -1;
				if(j+ilStartIdx < ilValueCount)
				{					
//					if(olTime>=omCurTime)
					{
						omFlightValues[j+ilStartIdx]++;						
					}
					
				}
			}
			//Die einzelnen Ghs, falls die Bewertung definiert ist setzen
			ilCount = omLines.GetSize();
			olTime = omStartTime + CTimeSpan(0,0,ilStartIdx,0) ;
			CString olPosUrnos = "";
			switch(imFilterIdx)
			{
			case FUNCTION:
				{
					olPosUrnos = ogDemData.GetPfcSingeUrnos(prlDem->Urud);
					break;
				}
			case QUALIFICATION:
				{
					olPosUrnos = ogDemData.GetPerSingeUrnos(prlDem->Urud);
					break;
				}
			case CHECKIN:
				{
					olPosUrnos = ogDemData.GetCicSingeUrnos(prlDem->Urud);
					break;
				}
			case POSITIONS:
				{
					olPosUrnos = ogDemData.GetPstSingeUrnos(prlDem->Urud);
					break;
				}
			case GATES:  
				{
					olPosUrnos = ogDemData.GetGatSingeUrnos(prlDem->Urud);					
					break;
				}
			case EQUIPMENT:  
				{
					olPosUrnos = ogDemData.GetReqSingeUrnos(prlDem->Urud);					
					break;
				}
			}
			for(j = 0; j < ilCount; j++)
			{
				CString olUrnoTxt = omLines[j].UrnoText;
				if (imFilterIdx == QUALIFICATION)
				{
					if(olPosUrnos.Find(omLines[j].UrnoText) > -1 || omLines[j].Lkco == LoadStg(IDS_STRING61216))
					{
						int ilC2 = olDemandDuration.GetTotalMinutes();
						int ilValues = omLines[j].Values.GetSize();
						for(int k = 0; k < ilC2; k++)
						{
							if(k+ilStartIdx < ilValues)
							{
	//							if( olTime>omCurTime)
								{
									omLines[j].Values[k+ilStartIdx]++;
								}
							}
						}
					}
				}
				else if(olPosUrnos.Find(omLines[j].UrnoText) == 0 || omLines[j].Lkco == LoadStg(IDS_STRING61216))
				{
					int ilC2 = olDemandDuration.GetTotalMinutes();
					int ilValues = omLines[j].Values.GetSize();
					for(int k = 0; k < ilC2; k++)
					{
						if(k+ilStartIdx < ilValues)
						{
//							if( olTime>omCurTime)
							{
								omLines[j].Values[k+ilStartIdx]++;
							}
						}
					}
				}
			}
		}
	}



	CTime olLoadStart,olLoadEnd;
	pomViewer->GetTimeFrameFromTo(olLoadStart,olLoadEnd);


	omCompleteFlightValues.RemoveAll();

	int ilTotalMinuteCount = (olLoadEnd - olLoadStart).GetTotalMinutes();
	omCompleteFlightValues.InsertAt(0,0,ilTotalMinuteCount);

	for ( rlPos = pomViewer->GetDemUrnoMap()->GetStartPosition(); rlPos != NULL; )
	{
		long llUrno;
		DEMDATA *prlDem = NULL; //&pomViewer->omDemGhds[i];
		pomViewer->GetDemUrnoMap()->GetNextAssoc(rlPos, (void *&)llUrno, (void *&)prlDem);
		
		ilStartIdx = 0;
		CTimeSpan olDemandDuration;// = prlDem->Duen - omStartTime;
		if(prlDem->DispDebe<=olLoadStart)
		{
			ilStartIdx = 0;
		}
		else
		{
			CTimeSpan TS = prlDem->DispDebe - olLoadStart;
			ilStartIdx = TS.GetTotalMinutes(); 
		}
		if(prlDem->DispDeen<olLoadEnd)
		{
			olDemandDuration = (ilStartIdx==0) ? prlDem->DispDeen - olLoadStart : prlDem->DispDeen - prlDem->DispDebe;
		}
		else
		{
			olDemandDuration = (ilStartIdx==0) ? olLoadEnd - olLoadStart : olLoadEnd - prlDem->DispDebe;
		}
		int ilCount = olDemandDuration.GetTotalMinutes();
		int ilValueCount = omCompleteFlightValues.GetSize();
		CTime olTime = olLoadStart + CTimeSpan(0,0,ilStartIdx,0) ;
		int ilCnt = 0;
		for(int j = 0; j < ilCount; j++,olTime+=CTimeSpan(0,0,1,0))
		{
			static long llUrno = -1;
			if(j+ilStartIdx < ilValueCount)
			{					
				{
					omCompleteFlightValues[j+ilStartIdx]++;						
				}
			}
		}
	}


//	CCS_CATCH_ALL
}

/****
void CCoverageGraphicWnd::CalculateSimOffsetFlightDemands()
{
	CCS_TRY
	CUIntArray *pomSimFlightValues = NULL;

	if (pomViewer->bmSimulate2)
	{
		pomSimFlightValues = &omSimFlightValues2;
	}
	else if (pomViewer->bmSimulate)
	{
		pomSimFlightValues = &omSimFlightValues;
	}
	else
	{
		return;
	}

	int ilCount = 0;
	int ilValidCount = 0;
	int i;
	//Das UIntArray vorher kalkulieren
	//ClearPolygonLines();
	int ilSimSize = imXMinutes+100; //omSimFlightValues.GetSize();
	pomSimFlightValues->RemoveAll();
	pomSimFlightValues->InsertAt(0,0,ilSimSize);
	
	
//	Polygone erst mal allokieren
	CString olGhsUrnos = "";
	bool blGhs = false;
	int ilC = omGhsUrnos.GetSize();
	for(i =0;i<ilC;i++)
	{
		CString olGhsUrno;
		olGhsUrno.Format("|%ld| ",omGhsUrnos.GetAt(i));
		olGhsUrnos += olGhsUrno;
		blGhs = true;
	}

	if (pomViewer->imSimType==SIM_CONSTANT)
	{
		for(int ilIndex = 0;ilIndex < ilSimSize;ilIndex+=FACTOR)
		{
			(*pomSimFlightValues)[ilIndex] = pomViewer->imSimValue;
		}
	}
	else
	{
		CTime omSimStartTime = omStartTime;
		CTime omSimEndTime = omEndTime;
		

		if(pomViewer->imSimOffset>0)
		{
			omSimStartTime -= CTimeSpan(0,0,pomViewer->imSimOffset,0);		
			omSimEndTime -= CTimeSpan(0,0,pomViewer->imSimOffset,0);
		}
		else if (pomViewer->imSimOffset<0)
		{
			omSimStartTime += CTimeSpan(0,0,abs(pomViewer->imSimOffset),0);		
			omSimEndTime += CTimeSpan(0,0,abs(pomViewer->imSimOffset),0);
		}
		
		POSITION rlPos;

		for ( rlPos = pomViewer->omDemUrnoMap.GetStartPosition(); rlPos != NULL; )
		{
			long llUrno;
			DEMDATA *prlDem;
			pomViewer->omDemUrnoMap.GetNextAssoc(rlPos, (void *&)llUrno, (void *&)prlDem);
			if(blGhs==true)
			{
				char pclStr[16];
				sprintf(pclStr,"|%ld|",prlDem->Ghsu);
				if(olGhsUrnos.Find(pclStr)==-1)
				{
					continue;
				}
			}

			if(IsOverlapped(omSimStartTime, omSimEndTime, prlDem->DispDebe, prlDem->DispDeen)) 
			{
				//if(IsOverlapped(omSimStartTime, omSimEndTime, prlDem->Dube, prlDem->Duen)) 
				int ilStartIdx = 0;//pomViewer->imSimOffset;
				CTimeSpan olDemandDuration;// = prlDem->Duen - omStartTime;
				if(prlDem->DispDebe<=omSimStartTime)
				{
					ilStartIdx = 0;
				}
				else
				{
					CTimeSpan TS = prlDem->DispDebe - omSimStartTime;
					ilStartIdx = TS.GetTotalMinutes(); 
				}
				if(prlDem->DispDeen<omSimEndTime)
				{
					olDemandDuration = (ilStartIdx==0) ? prlDem->DispDeen - omSimStartTime : prlDem->DispDeen - prlDem->DispDebe;
				}
				else
				{
					olDemandDuration = (ilStartIdx==0) ? omSimEndTime - omSimStartTime : omSimEndTime - prlDem->DispDebe;
				}
				// Adjust ilStartIdx, if necessary!
				int ilMod = ilStartIdx%FACTOR;
				if(ilMod !=0)
				{
					ilStartIdx+=(FACTOR-ilMod);
				}
				int ilCount = olDemandDuration.GetTotalMinutes();
				
				CTime olTime = omSimStartTime+CTimeSpan(0,0,ilStartIdx,0);
				for(int j = 0; j < ilCount; j+=FACTOR,olTime+=CTimeSpan(0,0,FACTOR,0))
				{
					if((j+ilStartIdx)>=0 && j+ilStartIdx < ilSimSize)
					{
						(*pomSimFlightValues)[j+ilStartIdx]++;
					}
				}
			}
		}
		int ilC = 0,ilSum=0;
		for(ilC=0;ilC<ilSimSize;ilC+=FACTOR)
		{
			
			switch(pomViewer->imSimType)
			{
			case SIM_ABSOLUTE:
				ilSum = (*pomSimFlightValues)[ilC] + pomViewer->imSimValue;
				break;
			case SIM_PERCENT:
				ilSum = (*pomSimFlightValues)[ilC] + MulDiv(omSimFlightValues[ilC],pomViewer->imSimValue,100);
				break;
			default:
				ilSum = 0;
				break;
			}
		
			if(ilSum<0)
			{
				(*pomSimFlightValues)[ilC] = 0;
			}
			else
			{
				(*pomSimFlightValues)[ilC] = ilSum;
			}	
		}
	}	
	CCS_CATCH_ALL
}
****/

void CCoverageGraphicWnd::CalculateSimOffsetFlightDemands()
{
	CUIntArray *pomSimFlightValues = NULL;

	if (pomViewer->SimulationActive() == true && pomViewer->SimDisplay() == 0)
	{
		pomSimFlightValues = &omSimFlightValues2;
	}
	else if (pomViewer->SimulationActive() == true && pomViewer->SimDisplay() == 1)
	{
		pomSimFlightValues = &omSimFlightValues;
	}
	else
	{
		return;
	}

	int ilSimSize = omFlightValues.GetSize();
	VERIFY(ilSimSize == imXMinutes);

	pomSimFlightValues->RemoveAll();
	pomSimFlightValues->InsertAt(0,0,ilSimSize);

	if (this->imActuelRow >= 0 && this->imActuelRow < this->omLines.GetSize())
	{
		CUIntArray olFlightValues;
		olFlightValues.InsertAt(0,0,this->omLines[this->imActuelRow].Values.GetSize());
		for (int ilIndex = 0; ilIndex < this->omLines[this->imActuelRow].Values.GetSize(); ilIndex++)
		{
			olFlightValues[ilIndex] = this->omLines[this->imActuelRow].Values[ilIndex];
		}
		CalculateSimOffsetFlightDemands(olFlightValues,*pomSimFlightValues);
		CalculateSimOffsetFlightDemands(omCompleteFlightValues,omCompleteSimFlightValues2);
	}
	else
	{
		CalculateSimOffsetFlightDemands(omCompleteFlightValues,omCompleteSimFlightValues2);

		int ilComplFlightSize	  = omCompleteFlightValues.GetSize();
		CTime olLoadTimeStart,olLoadTimeEnd;
		pomViewer->GetTimeFrameFromTo(olLoadTimeStart,olLoadTimeEnd);
		CTimeSpan	olTotalOffset = omStartTime - olLoadTimeStart;
		int			ilTotalMinOffset = olTotalOffset.GetTotalMinutes();

		for (int ilIndex = 0; ilIndex < ilSimSize; ilIndex++)
		{
			if (ilIndex + ilTotalMinOffset < ilComplFlightSize)
				(*pomSimFlightValues)[ilIndex] = omCompleteSimFlightValues2[ilIndex+ilTotalMinOffset];
		}
	}	
}

void CCoverageGraphicWnd::CalculateSimOffsetFlightDemands(const CUIntArray& ropFlightValues,CUIntArray& ropSimFlightValues)
{
	CCS_TRY

	int ilCount = 0;
	int ilValidCount = 0;
	int ilComplFlightSize = ropFlightValues.GetSize();

	ropSimFlightValues.RemoveAll();
	ropSimFlightValues.InsertAt(0,0,ilComplFlightSize);

	if (pomViewer->SimType() == SIM_CONSTANT)
	{
		for(int ilIndex = 0;ilIndex < ilComplFlightSize - 1;ilIndex+=FACTOR)
		{
			ropSimFlightValues[ilIndex] = pomViewer->SimValue();
		}
	}
	else
	{
		// calculate time moved curve
		int ilSimIndex;
		for(int ilC = 0;ilC < ilComplFlightSize - 1;ilC += FACTOR)
		{
			ilSimIndex = ilC + pomViewer->SimOffset();
			if (ilSimIndex >= 0 && ilSimIndex < ilComplFlightSize)
				ropSimFlightValues[ilSimIndex] = ropFlightValues[ilC];
		}
		
		// calculate demand moved curve
		int ilSum = 0;
		for(ilC = 0;ilC < ilComplFlightSize - 1;ilC += FACTOR)
		{
			switch(pomViewer->SimType())
			{
			case SIM_ABSOLUTE:
				ilSum = ropSimFlightValues[ilC] + pomViewer->SimValue();
				break;
			case SIM_PERCENT:
				ilSum = ropSimFlightValues[ilC] + MulDiv(ropSimFlightValues[ilC],pomViewer->SimValue(),100);
				break;
			default:
				ilSum = 0;
				break;
			}
		
			if(ilSum<0)
			{
				ropSimFlightValues[ilC] = 0;
			}
			else
			{
				ropSimFlightValues[ilC] = ilSum;
			}	
		}

	}	

	CCS_CATCH_ALL
}

/*******
void CCoverageGraphicWnd::CalculateSimOffsetFlightDemands()
{
	CCS_TRY
	CUIntArray *pomSimFlightValues = NULL;

	if (pomViewer->bmSimulate2)
	{
		pomSimFlightValues = &omSimFlightValues2;
	}
	else if (pomViewer->bmSimulate)
	{
		pomSimFlightValues = &omSimFlightValues;
	}
	else
	{
		return;
	}

	int ilCount = 0;
	int ilValidCount = 0;
	int i;
	int ilSimSize = omFlightValues.GetSize();
	VERIFY(ilSimSize == imXMinutes+100);
	int ilComplFlightSize = omCompleteFlightValues.GetSize();

	pomSimFlightValues->RemoveAll();
	pomSimFlightValues->InsertAt(0,0,ilSimSize);
	
	omCompleteSimFlightValues2.RemoveAll();
	omCompleteSimFlightValues2.InsertAt(0,0,ilComplFlightSize);

//	Polygone erst mal allokieren
	CString olGhsUrnos = "";
	bool blGhs = false;
	int ilC = omGhsUrnos.GetSize();
	for(i =0;i<ilC;i++)
	{
		CString olGhsUrno;
		olGhsUrno.Format("|%ld| ",omGhsUrnos.GetAt(i));
		olGhsUrnos += olGhsUrno;
		blGhs = true;
	}

	CTimeSpan	olTotalOffset = omStartTime - pomViewer->omLoadTimeStart;
	int			ilTotalMinOffset = olTotalOffset.GetTotalMinutes();

	if (pomViewer->imSimType==SIM_CONSTANT)
	{
		for(int ilIndex = 0;ilIndex < ilComplFlightSize - 1;ilIndex+=FACTOR)
		{
			omCompleteSimFlightValues2[ilIndex] = pomViewer->imSimValue;
		}
	}
	else
	{
		CTime omSimStartTime = pomViewer->omLoadTimeStart;
		CTime omSimEndTime	 = pomViewer->omLoadTimeEnd;
		
		if (pomViewer->imSimOffset>0)
		{
			omSimStartTime -= CTimeSpan(0,0,pomViewer->imSimOffset,0);		
			omSimEndTime -= CTimeSpan(0,0,pomViewer->imSimOffset,0);
		}
		else if (pomViewer->imSimOffset<0)
		{
			omSimStartTime += CTimeSpan(0,0,abs(pomViewer->imSimOffset),0);		
			omSimEndTime += CTimeSpan(0,0,abs(pomViewer->imSimOffset),0);
		}
		
		POSITION rlPos;

		for ( rlPos = pomViewer->omDemUrnoMap.GetStartPosition(); rlPos != NULL; )
		{
			long llUrno;
			DEMDATA *prlDem;
			pomViewer->omDemUrnoMap.GetNextAssoc(rlPos, (void *&)llUrno, (void *&)prlDem);
			if(blGhs==true)
			{
				char pclStr[16];
				sprintf(pclStr,"|%ld|",prlDem->Ghsu);
				if(olGhsUrnos.Find(pclStr)==-1)
				{
					continue;
				}
			}

			if (IsOverlapped(omSimStartTime, omSimEndTime, prlDem->DispDebe, prlDem->DispDeen)) 
			{
				int ilStartIdx = 0;//pomViewer->imSimOffset;
				CTimeSpan olDemandDuration;// = prlDem->Duen - omStartTime;
				if(prlDem->DispDebe<=omSimStartTime)
				{
					ilStartIdx = 0;
				}
				else
				{
					CTimeSpan TS = prlDem->DispDebe - omSimStartTime;
					ilStartIdx = TS.GetTotalMinutes(); 
				}
				if(prlDem->DispDeen<omSimEndTime)
				{
					olDemandDuration = (ilStartIdx==0) ? prlDem->DispDeen - omSimStartTime : prlDem->DispDeen - prlDem->DispDebe;
				}
				else
				{
					olDemandDuration = (ilStartIdx==0) ? omSimEndTime - omSimStartTime : omSimEndTime - prlDem->DispDebe;
				}
				// Adjust ilStartIdx, if necessary!
				int ilMod = ilStartIdx%FACTOR;
				if(ilMod !=0)
				{
					ilStartIdx+=(FACTOR-ilMod);
				}
				int ilCount = olDemandDuration.GetTotalMinutes();
				
				CTime olTime = omSimStartTime+CTimeSpan(0,0,ilStartIdx,0);
				for(int j = 0; j < ilCount; j+=FACTOR,olTime+=CTimeSpan(0,0,FACTOR,0))
				{
					if ((j+ilStartIdx)>=0 && j+ilStartIdx < ilComplFlightSize)
					{
						omCompleteSimFlightValues2[j+ilStartIdx]++;
					}
				}
			}
		}

		int ilSum = 0;
		for(int ilC = 0;ilC < ilComplFlightSize - 1;ilC += FACTOR)
		{
			switch(pomViewer->imSimType)
			{
			case SIM_ABSOLUTE:
				ilSum = omCompleteSimFlightValues2[ilC] + pomViewer->imSimValue;
				break;
			case SIM_PERCENT:
				ilSum = omCompleteSimFlightValues2[ilC] + MulDiv(omSimFlightValues[ilC],pomViewer->imSimValue,100);
				break;
			default:
				ilSum = 0;
				break;
			}
		
			if(ilSum<0)
			{
				omCompleteSimFlightValues2[ilC] = 0;
			}
			else
			{
				omCompleteSimFlightValues2[ilC] = ilSum;
			}	
		}

	}	

	for (int ilIndex = 0; ilIndex < ilSimSize; ilIndex++)
	{
		if (ilIndex + ilTotalMinOffset < ilComplFlightSize)
			(*pomSimFlightValues)[ilIndex] = omCompleteSimFlightValues2[ilIndex+ilTotalMinOffset];
	}
	CCS_CATCH_ALL
}

*****/

void CCoverageGraphicWnd::CalculateSimCutFlightDemands()
{
	CUIntArray *pomSimFlightValues = NULL;

	if (pomViewer->SimulationActive() == true && pomViewer->SimDisplay() == 0)
	{
		pomSimFlightValues = &omSimFlightValues2;
	}
	else if (pomViewer->SimulationActive() == true && pomViewer->SimDisplay() == 1)
	{
		pomSimFlightValues = &omSimFlightValues;
	}
	else
	{
		return;
	}

	int ilSimSize = omFlightValues.GetSize();

	pomSimFlightValues->RemoveAll();
	pomSimFlightValues->InsertAt(0,0,ilSimSize);

	if (this->imActuelRow >= 0 && this->imActuelRow < this->omLines.GetSize())
	{
		CUIntArray olFlightValues;
		olFlightValues.InsertAt(0,0,this->omLines[this->imActuelRow].Values.GetSize());
		for (int ilIndex = 0; ilIndex < this->omLines[this->imActuelRow].Values.GetSize(); ilIndex++)
		{
			olFlightValues[ilIndex] = this->omLines[this->imActuelRow].Values[ilIndex];
		}
		CalculateSimCutFlightDemands(olFlightValues,*pomSimFlightValues);
		CalculateSimCutFlightDemands(omCompleteFlightValues,omCompleteSimFlightValues2);
	}
	else
	{

		CalculateSimCutFlightDemands(omCompleteFlightValues,omCompleteSimFlightValues2);

		int ilComplFlightSize = omCompleteFlightValues.GetSize();
		CTime olLoadTimeStart,olLoadTimeEnd;
		pomViewer->GetTimeFrameFromTo(olLoadTimeStart,olLoadTimeEnd);
		CTimeSpan olTotalOffset = omStartTime - olLoadTimeStart;
		int ilTotalMinOffset = olTotalOffset.GetTotalMinutes();

		for(int ilIndex = 0;ilIndex < ilSimSize ;ilIndex++)
		{
			if (ilIndex + ilTotalMinOffset < ilComplFlightSize)
			{
				(*pomSimFlightValues)[ilIndex] = omCompleteSimFlightValues2[ilIndex + ilTotalMinOffset];
			}
		}
	}
}

void CCoverageGraphicWnd::CalculateSimCutFlightDemands(const CUIntArray& ropFlightValues,CUIntArray& ropSimFlightValues)
{
	CCS_TRY

	int ilCount = 0;
	int ilValidCount = 0;
	int ilComplFlightSize = ropFlightValues.GetSize();
	DWORD llNow, llStart;
	double flDiff;

	llStart = GetTickCount();

	// initialize parameter in any case to prevent program abort
	ropSimFlightValues.RemoveAll();
	ropSimFlightValues.InsertAt(0,0,ilComplFlightSize);

	int ilMaxVal = pomViewer->SimHeight();
	if (ilMaxVal > 0)
	{
		int ilDiff = 0;
		for (int ilIndex = 0;ilIndex < ilComplFlightSize - 1;ilIndex++)
		{
			ropSimFlightValues[ilIndex] += ropFlightValues[ilIndex];
			ilDiff = ropSimFlightValues[ilIndex] - ilMaxVal;
			if (ilDiff > 0)
			{
				ropSimFlightValues[ilIndex] = ilMaxVal;
				ropSimFlightValues[ilIndex+1] += ilDiff;
			}
		}
	}

	/* must add last values	*/
	ropSimFlightValues[ilComplFlightSize - 1] += ropFlightValues[ilComplFlightSize - 1];

	llNow = GetTickCount();
	flDiff = llNow-llStart;
	TRACE( "CalculateSimFlightDemands2 %.3lf sec\n", flDiff/1000.0 );

	CCS_CATCH_ALL

}


/*****
void CCoverageGraphicWnd::CalculateSimCutFlightDemands()
{
	CCS_TRY

	CUIntArray *pomSimFlightValues = NULL;

	if (pomViewer->bmSimulate2)
	{
		pomSimFlightValues = &omSimFlightValues2;
	}
	else if (pomViewer->bmSimulate)
	{
		pomSimFlightValues = &omSimFlightValues;
	}
	else
	{
		return;
	}

	int ilCount = 0;
	int ilValidCount = 0;
	int ilSimSize = omFlightValues.GetSize();
	int ilComplFlightSize = omCompleteFlightValues.GetSize();
	DWORD llNow, llStart;
	double flDiff;

	llStart = GetTickCount();

	(*pomSimFlightValues).RemoveAll();
	(*pomSimFlightValues).InsertAt(0,0,ilSimSize);
		
	int ilMaxVal = pomViewer->imSimHeight;
	if(ilMaxVal > 0)
	{
		omCompleteSimFlightValues2.RemoveAll();
		omCompleteSimFlightValues2.InsertAt(0,0,ilComplFlightSize);
		
		CTimeSpan olTotalOffset = omStartTime - pomViewer->omLoadTimeStart;

		int ilDiff = 0;

		int ilTotalMinOffset = olTotalOffset.GetTotalMinutes();
		if(ilTotalMinOffset >=0)
		{
			for(int ilIndex = 0;ilIndex < ilComplFlightSize-1;ilIndex++)
			{
				omCompleteSimFlightValues2[ilIndex] += omCompleteFlightValues[ilIndex];
				ilDiff = omCompleteSimFlightValues2[ilIndex] - ilMaxVal;
				if(ilDiff > 0)
				{
					omCompleteSimFlightValues2[ilIndex] = ilMaxVal;
					omCompleteSimFlightValues2[ilIndex +1] = ilDiff;
				}
			}
		}
		else
		{
			for(int ilIndex = 0;ilIndex < ilSimSize;ilIndex++)
			{
				omCompleteSimFlightValues2[ilIndex] += omFlightValues[ilIndex];
				ilDiff = omCompleteSimFlightValues2[ilIndex] - ilMaxVal;
				if(ilDiff > 0)
				{
					omCompleteSimFlightValues2[ilIndex] = ilMaxVal;
					omCompleteSimFlightValues2[ilIndex +1] = ilDiff;
				}
			}
			ilTotalMinOffset = 0;
		}
		for(int ilIndex = 0;ilIndex < ilSimSize ;ilIndex++)
		{
			if(ilIndex + ilTotalMinOffset < ilComplFlightSize)
			{
				(*pomSimFlightValues)[ilIndex] = omCompleteSimFlightValues2[ilIndex + ilTotalMinOffset];
			}
		}
	}

	llNow = GetTickCount();
	flDiff = llNow-llStart;
	TRACE( "CalculateSimFlightDemands2 %.3lf sec\n", flDiff/1000.0 );

	CCS_CATCH_ALL
}
*****/


void CCoverageGraphicWnd::ClearPolygonLines()
{
	CCS_TRY
	int ilCount = omLines.GetSize();
	for(int i = 0; i < ilCount; i++)
	{
		omLines[i].Values.DeleteAll();
	}
	omLines.DeleteAll();
/*************	
	********/
	/*
	ilCount = omSimLines.GetSize();
	for(i = 0; i < ilCount; i++)
	{
		omSimLines[i].Values.DeleteAll();
	}
	omSimLines.DeleteAll();
	*/

	ilCount = omRealLines.GetSize();
	for(i = 0; i < ilCount; i++)
	{
		omRealLines[i].Values.DeleteAll();
	}
	omRealLines.DeleteAll();
	CCS_CATCH_ALL
}


void CCoverageGraphicWnd::CalculateShiftDemands()
{
//	CCS_TRY
	DWORD	llNow,llLast, llStart;
	double	flDiff;
	double flDiff2 = 0;

	llLast = GetTickCount();
	llStart = llLast;


	long	llDiff=0;
	int		ilCount = 0;
	int		ilValidCount = 0;
	
	int		ilSdtCount = 0;

	CTime	olLoadStart,olLoadEnd;
	pomViewer->GetTimeFrameFromTo(olLoadStart,olLoadEnd);


	int ilOffSet = (omStartTime - olLoadStart).GetTotalMinutes();
	int ilBorder = ilOffSet + (omEndTime - omStartTime).GetTotalMinutes();

	if(pomViewer->ReCalcShifts())
	{

		CStringArray olDisplayUrnos;
		bool blUseAll = false;
		int ilPolyCount = 0;

		int ilTotalMinuteCount = (olLoadEnd - olLoadStart).GetTotalMinutes();

		switch(imFilterIdx)
		{
		case FUNCTION:
			{
				blUseAll = pomViewer->PfcMapForGraphic(olDisplayUrnos);
				break;
			}
		case QUALIFICATION:
			{
				blUseAll = pomViewer->GhsMapForGraphic(olDisplayUrnos);
				break;
			}
		case CHECKIN:
			{
				blUseAll = pomViewer->CicMapForGraphic(olDisplayUrnos);
				break;
			}
		case POSITIONS:
			{
				blUseAll = pomViewer->PstMapForGraphic(olDisplayUrnos);
				break;
			}
		case GATES:  
			{
				blUseAll = pomViewer->GatMapForGraphic(olDisplayUrnos);
				break;
			}
		case EQUIPMENT:  
			{
				blUseAll = pomViewer->EquMapForGraphic(olDisplayUrnos);
				break;
			}
		}

		llNow  = GetTickCount();
		flDiff = llNow-llLast;
		llLast = llNow;
		TRACE("CovGraphicWnd::CalcShiftDemands:GetUrnos %.3lf sec\n",flDiff/1000.0 );

		// delete old lines
		ilCount = omShiftLines.GetSize();
		for(int i = 0; i < ilCount; i++)
		{
			omShiftLines[i].Values.DeleteAll();
		}
		omShiftLines.DeleteAll();

		llNow  = GetTickCount();
		flDiff = llNow-llLast;
		llLast = llNow;
		TRACE("CovGraphicWnd::CalcShiftDemands:omShiftLines.DeleteAll %.3lf sec\n",flDiff/1000.0 );

		//	Polygone erst mal allokieren
		int ilC = olDisplayUrnos.GetSize();
		if(blUseAll)
		{
			ilC = 1;
			omShiftLines.SetSize(ilC);
			POLYGON_LINE *prlPolyLine = new POLYGON_LINE;
			prlPolyLine->Urno = 0L;
			prlPolyLine->UrnoText = "0L";
			prlPolyLine->Lkco = LoadStg(IDS_STRING61216);
			prlPolyLine->CurrentOffset = 0;
			prlPolyLine->Color = omColors[0 % imColorCount].Color;
			omShiftLines.SetAt(0,prlPolyLine);
		}
		else
		{
			CString olTmpText;
			CString olTmp;
			CStringArray olItemList;
			omShiftLines.SetSize(ilC);
			for(int i = 0; i < ilC; i++)
			{
				POLYGON_LINE *prlPolyLine = new POLYGON_LINE;
				olTmpText = olDisplayUrnos[i];				
				ExtractItemList(olTmpText, &olItemList, '�');
				if(olItemList.GetSize() > 1)
				{
					olTmp = olItemList[0];
					if(olTmp.Find("S") >= 0 || olTmp.Find("G") >= 0)
					{
						if (olTmp.Find("G") >= 0)
							prlPolyLine->IsGroup = true;
						else
							prlPolyLine->IsGroup = false;

						olTmp = olTmp.Left(olTmp.GetLength() -1 );
					}
					prlPolyLine->UrnoText = olTmp + ",";
					olTmpText = olItemList[1];
					ExtractItemList(olTmpText, &olItemList, '%');
					if(olItemList.GetSize() > 0)
					{
						olTmp = olItemList[0];
						if(olTmp.IsEmpty() && olItemList.GetSize() > 1)
						{
							olTmp = olItemList[1];
						}
					}
					prlPolyLine->Lkco = olTmp;

				}
				prlPolyLine->Urno = atol(prlPolyLine->UrnoText);
				prlPolyLine->Color = omColors[i % imColorCount].Color;
				omShiftLines.SetAt(i,prlPolyLine);
			}
		}

		ilC = omShiftLines.GetSize();
		for( i = 0; i < ilC; i++)
		{
			for(int j = 0; j < ilTotalMinuteCount +50; j++)
			{
				int ilValue = 0;
				omShiftLines[i].Values.New(ilValue);
			}
		}
		
		llNow  = GetTickCount();
		flDiff = llNow-llLast;
		llLast = llNow;
		TRACE("CovGraphicWnd::CalcShiftDemands:Allocate polylines %.3lf sec\n",flDiff/1000.0 );

		olDisplayUrnos.RemoveAll();
		omCompleteShiftArray.RemoveAll();

		// initialize array with 0 from [0 ... ilTotalMinuteCount]		
		omCompleteShiftArray.InsertAt(0,0,ilTotalMinuteCount);

		if (imFilterIdx == EQUIPMENT || (imFilterIdx >= CHECKIN && imFilterIdx <= GATES))
		{
			CString olTable;
			if (imFilterIdx == EQUIPMENT)
			{
				olTable = "EQU";
			}
			else if (imFilterIdx == CHECKIN)
			{
				olTable = "CIC";
			}
			else if (imFilterIdx == POSITIONS)
			{
				olTable = "PST";
			}
			else if (imFilterIdx == GATES)
			{
				olTable = "GAT";
			}

			CTime olStartTime(olLoadStart);
			CTime olEndTime(olLoadEnd);

			CMapPtrToPtr olLocUrnosCompleted;
			// loop over all equipment urnos to be displayed (single or group) 
			for (int j = 0; j < ilC; j++)
			{
				CUIntArray olLocUrnos;
				// use all					
				if (omShiftLines[j].Lkco == LoadStg(IDS_STRING61216))
				{
					int ilCount = ogBCD.GetDataCount(olTable);
					for (int i = 0; i < ilCount; i++)
					{
						if (imFilterIdx == EQUIPMENT)
						{
							if (ogBCD.GetField("EQU",i,"GKEY") == pomViewer->GetFilterEquipmentType())
							{
								olLocUrnos.Add(atol(ogBCD.GetField(olTable,i,"URNO")));	
							}
						}
						else
						{
							olLocUrnos.Add(atol(ogBCD.GetField(olTable,i,"URNO")));	
						}
					}
				}
				else if (omShiftLines[j].IsGroup)	// static group
				{
					CCSPtrArray<SGMDATA> olSgm;
					int ilRecordCount = ogSgmData.GetSgmByUsgr(olTable,omShiftLines[j].Urno,olSgm);
					for(int k = 0; k < ilRecordCount; k++)
					{
 						 olLocUrnos.Add(olSgm[k].Uval);
					}
				}
				else	// single
				{
					olLocUrnos.Add(omShiftLines[j].Urno);
				}

				// loop over all locations available for the current line
				for (i = 0; i < olLocUrnos.GetSize(); i++)
				{
					void *pDummy;
					bool blAdd = !olLocUrnosCompleted.Lookup((void *)olLocUrnos[i],pDummy);
					if (blAdd)
					{
						olLocUrnosCompleted.SetAt((void *)olLocUrnos[i],pDummy);
					}

					// get available times for this location
					CCSPtrArray<VALIDTIMES> olValid;
					ogValData.GetValidTimes(olTable,olLocUrnos[i],olLoadStart,olLoadEnd,olValid);	
					if (olValid.GetSize() > 0)
					{
						for (int k = 0; k < olValid.GetSize(); k++)
						{
							int ilStartIdx = 0;
							CTimeSpan olDemandDuration = CTimeSpan(0,0,0,0);
							CTime olStartTime,olEndTime;

							olStartTime = olValid[k].Tifr;
							olEndTime   = olValid[k].Tito;

							if (IsOverlapped(olStartTime,olEndTime,olLoadStart,olLoadEnd))
							{
								if (olStartTime < olLoadStart)
								{
									olStartTime = olLoadStart;									
								}

								if (olEndTime > olLoadEnd)
								{
									olEndTime = olLoadEnd;
								}


								CTimeSpan TS = olStartTime - olLoadStart;
								ilStartIdx	 = TS.GetTotalMinutes();
								olDemandDuration = olEndTime - olStartTime;								
							}

							int ilCount = olDemandDuration.GetTotalMinutes();
							int ilValueCount = omShiftLines[j].Values.GetSize();
							for (int l = 0; l < ilCount; l++)
							{
								omShiftLines[j].Values[ilStartIdx+l] += 1;
								if (blAdd)
								{
									omCompleteShiftArray[ilStartIdx+l] += 1;
								}
							}
						}

						olValid.DeleteAll();

						// get blocked times for this equipment
						CCSPtrArray<BLKBLOCKEDTIMES> olBlocked;
						ogBlkData.GetBlockedTimes(olTable,olLocUrnos[i],olLoadStart,olLoadEnd,olBlocked);					
						for (k = 0; k < olBlocked.GetSize(); k++)
						{
							int ilStartIdx = 0;
							CTimeSpan olDemandDuration = CTimeSpan(0,0,0,0);
							CTime olStartTime,olEndTime;

							olStartTime = olBlocked[k].Tifr;
							olEndTime   = olBlocked[k].Tito;

							if (IsOverlapped(olStartTime,olEndTime,olLoadStart,olLoadEnd))
							{
								if (olStartTime < olLoadStart)
								{
									olStartTime = olLoadStart;									
								}

								if (olEndTime > olLoadEnd)
								{
									olEndTime = olLoadEnd;
								}


								CTimeSpan TS = olStartTime - olLoadStart;
								ilStartIdx	 = TS.GetTotalMinutes();
								olDemandDuration = olEndTime - olStartTime;								
							}

							int ilCount = olDemandDuration.GetTotalMinutes();
							int ilValueCount = omShiftLines[j].Values.GetSize();
							for (int l = 0; l < ilCount; l++)
							{
								omShiftLines[j].Values[ilStartIdx + l] -= 1;
								if (blAdd)
								{
									omCompleteShiftArray[ilStartIdx + l] -= 1;
								}
							}
						}

						olBlocked.DeleteAll();
					}
				}
			}
		}
		else	// PERSONNEL
		{								
			CCSPtrArray <SPLITTEDSDT> olSplittedSdt;
			POSITION rlPos;

			llNow  = GetTickCount();
			flDiff = llNow-llLast;
			llStart= llNow;
			llLast = llNow;
			TRACE("CovGraphicWnd::CalcShiftDemands:Start Recalculation %.3lf sec\n",flDiff/1000.0 );

			for(rlPos = pomViewer->GetSdtUrnoMap()->GetStartPosition();rlPos!=NULL;)
			{
				SDTDATA *prlDem = NULL; 
				long llUrno;

				pomViewer->GetSdtUrnoMap()->GetNextAssoc(rlPos,(void *&)llUrno,(void *&)prlDem);

				llNow  = GetTickCount();
				TransformSdtData(olSplittedSdt,prlDem);
				llLast = llNow;
				llNow  = GetTickCount();
				flDiff2 += llNow-llLast;
				for (int illc = 0; illc < olSplittedSdt.GetSize(); illc++)
				{
					SPLITTEDSDT * prlSSdt = &olSplittedSdt[illc];
					if(IsPassSSdtFxFilter(prlSSdt)) 
					{
						ilSdtCount++;
						int ilStartIdx = 0;
						int ilBreakIdx = 0;
						CTimeSpan olDemandDuration = CTimeSpan(0,0,0,0);// = prlDem->Lsen - omStartTime;
						CTimeSpan olBreakDuration = CTimeSpan(0,0,0,0);
						CTime olStartTime,olEndTime;

				//Teil 1: Die L�nge der Schicht an sich
						olStartTime = prlSSdt->Start;
						olEndTime   = prlSSdt->End;

						CString olStrtStr,olEndStr;
						CString olBrkfStr,olBrktStr;
						CString omStrtStr,omEndStr;

						if(olStartTime <= olLoadStart && olEndTime < olLoadEnd) //ragt in sichbaren Bereich
						{
							if(olStartTime < olLoadStart)
							{
								olDemandDuration = olEndTime - olLoadStart;
							}
							else
							{
								olDemandDuration = olEndTime - olLoadStart;
							}
							ilStartIdx = 0;
						}
						else if(olStartTime > olLoadStart && olEndTime <= olLoadEnd) // voll in sichbaren Bereich
						{
							olDemandDuration = olEndTime - olStartTime;
							CTimeSpan TS = olStartTime - olLoadStart;
							ilStartIdx = TS.GetTotalMinutes(); 
						}
						else if(olStartTime > olLoadStart && olEndTime > olLoadEnd) // ragt aus sichbaren Bereich
						{
							olDemandDuration = olLoadEnd - olStartTime;
							CTimeSpan TS = olStartTime - olLoadStart;
							ilStartIdx = TS.GetTotalMinutes(); 
						}
						else if(olStartTime < olLoadStart && olEndTime > olLoadEnd) // links/rechts aus Sichtbereich
						{
							olDemandDuration = olLoadEnd - olLoadStart;
							CTimeSpan TS = olLoadEnd - olLoadStart;
							ilStartIdx = 0;//TS.GetTotalMinutes(); 
						}
				//Teil 2: Die Pause und deren Lage
						if(pomViewer->GetIgnoreBreak() == false)
						{
							olBrkfStr = prlSSdt->Brkf.Format("%Y.%m.%d - %H:%M");
							olBrktStr = prlSSdt->Brkt.Format("%Y.%m.%d - %H:%M");
							if(prlSSdt->Brkf <= olLoadStart && prlSSdt->Brkt > olLoadStart && prlSSdt->Brkt < omEndTime) //ragt in sichbaren Bereich
							{
								if(prlSSdt->Brkf < olLoadStart)
								{
									olBreakDuration = prlSSdt->Brkt - olLoadStart;
								}
							
								ilBreakIdx = 0;
							}
							else if(prlSSdt->Brkf > olLoadStart && prlSSdt->Brkt <= olLoadEnd) // voll in sichbaren Bereich
							{
								olBreakDuration = prlSSdt->Brkt - prlSSdt->Brkf;
								CTimeSpan TS = prlSSdt->Brkf - olLoadStart;
								ilBreakIdx = TS.GetTotalMinutes(); 
							}
							else if(prlSSdt->Brkf > olLoadStart && prlSSdt->Brkf < olLoadEnd  && prlSSdt->Brkt > olLoadEnd) // ragt aus sichbaren Bereich
							{
								olBreakDuration = olLoadEnd - prlSSdt->Brkf;
								CTimeSpan TS = prlSSdt->Brkf - olLoadStart;
								ilBreakIdx = TS.GetTotalMinutes(); 
							}
							else if(prlSSdt->Brkf < olLoadStart && prlSSdt->Brkt > olLoadEnd) // links/rechts aus Sichtbereich
							{
								olBreakDuration = olLoadEnd - olLoadStart;
								CTimeSpan TS = olLoadEnd - olLoadStart;
								ilBreakIdx = 0;//TS.GetTotalMinutes(); 
							}
						}

						//Und die Berechnung der Werte f�r Schicht
						int ilCount = olDemandDuration.GetTotalMinutes();
						int ilValueCount = omCompleteShiftArray.GetSize();
						for(int j = 0; j < ilCount; j++)
						{
							if(j+ilStartIdx < ilValueCount)
							{
								omCompleteShiftArray[j+ilStartIdx]+=prlSSdt->Factor;
							}
						}
						if(pomViewer->GetIgnoreBreak() == false)
						{
							ilCount = olBreakDuration.GetTotalMinutes();
							ilValueCount = omCompleteShiftArray.GetSize();
							for(j = 0; j < ilCount; j++)
							{
								if(j+ilBreakIdx < ilValueCount)
								{
									omCompleteShiftArray[j+ilBreakIdx]-=prlSSdt->Factor;
								}
							}
						}
						for(j = 0; j < ilC; j++)
						{
							CString olFctcTxt = omShiftLines[j].Lkco;
							CString olFctc = prlSSdt->Fctc;
							bool blIsOk = false;
							switch(imFilterIdx)
							{
							case FUNCTION:
								{
									if(olFctc == olFctcTxt || omShiftLines[j].Lkco == LoadStg(IDS_STRING61216))
									{
										blIsOk = true;
									}
									break;
								}	
							case QUALIFICATION:
								{
									CString olQuali;
									if(prlSSdt->Urno > 0)
									{
										CStringArray olList;
										olQuali = ogSdtRosterData.GetTotalQuali(prlSSdt->Urno);
										if(!olQuali.IsEmpty())
										{
											CString olUt = omShiftLines[j].UrnoText;
											if(olQuali.Find(omShiftLines[j].Lkco) > -1 || omShiftLines[j].Lkco == LoadStg(IDS_STRING61216))
											{
												blIsOk = true;						
											}
							
										}
									}
									break;
								}	
							}

								
							if(blIsOk)
							{
								int ilC2 = olDemandDuration.GetTotalMinutes();
								int ilValues = omShiftLines[j].Values.GetSize();
								for(int k = 0; k < ilC2; k++)
								{
									if(k+ilStartIdx < ilValues)
									{
			//							if( olTime>omCurTime)
										{
											omShiftLines[j].Values[k+ilStartIdx]+= prlSSdt->Factor;
										}
									}
								}

								if(pomViewer->GetIgnoreBreak() == false)
								{
									ilCount = olBreakDuration.GetTotalMinutes();
									ilValueCount = omShiftLines[j].Values.GetSize();
									for(k = 0; k < ilCount; k++)
									{
										if(k+ilBreakIdx < ilValueCount)
										{
											omShiftLines[j].Values[k+ilBreakIdx]-=prlSSdt->Factor;
										}
									}
								}
							}
						}
					}
				}
				olSplittedSdt.DeleteAll();
			}
		}
	}

	TRACE("CovGraphicWnd::CalcShiftDemands:TransformSdtData(ALL) %.3lf sec\n",flDiff2/1000.0 );

	llNow  = GetTickCount();
	flDiff = llNow-llStart;
	llLast = llNow;
	TRACE("CovGraphicWnd::CalcShiftDemands:Recalculation %.3lf sec\n",flDiff/1000.0 );

	if (ilBorder <= omCompleteShiftArray.GetSize())
	{
		omShiftArray.RemoveAll();
		// initialize with 0 from [0 ... ilBorder-ilOffSet]
		omShiftArray.InsertAt(0,0,ilBorder - ilOffSet);

		for (int ilLc = 0; ilLc < ilBorder - ilOffSet;ilLc++)
		{
			omShiftArray[ilLc] = omCompleteShiftArray[ilLc + ilOffSet];
		}
	}

	pomViewer->SetReCalcShifts(false);
	//TRACE("CalculateShiftDemand: Total SDT Count =%d Effective SDT Count= %d\n",ogSdtData.omData.GetSize(),ilSdtCount);//
	//llTotalDiff=llEffectivDiff=0;
	llNow  = GetTickCount();
	flDiff = llNow-llLast;
	llLast = llNow;
	TRACE("CovGraphicWnd::CalcShiftDemands:omShiftArray %.3lf sec\n",flDiff/1000.0 );

//	CCS_CATCH_ALL
}


bool CCoverageGraphicWnd::IsPassSdtFilter(SDTDATA *&prpSdt)
{
	CCS_TRY

	CCS_CATCH_ALL
	return false;
}

bool CCoverageGraphicWnd::IsPassSdtFxFilter(SDTDATA *&prpSdt)
{
	CCS_TRY
		
	bool blRet = true;

	{
		char pclCodes[64];
		char pclCode[32];
		strcpy(pclCodes,prpSdt->Fctc);
		char *pclPtr = strstr(pclCodes," - ");
		if(pclPtr!=NULL)
			*pclPtr = '\0';
		blRet = false;
		pclPtr = strtok(pclCodes,"\\");
		while(pclPtr!=NULL)
		{
			sprintf(pclCode,",%s,",pclPtr);
			if(strstr(pomViewer->GetPfcCodeList().GetBuffer(0),pclCode)!=NULL)
			{
				blRet = true;
				break;
			}
			pclPtr = strtok(NULL,"\\");
		}
	
	}
	return blRet;
	CCS_CATCH_ALL
	return false;
}
bool CCoverageGraphicWnd::IsPassSSdtFxFilter(SPLITTEDSDT *&prpSdt)
{
	CCS_TRY
		
	bool blRet = false;

	switch(imFilterIdx)
	{
	case FUNCTION:
		{
			blRet = false;
			if(pomViewer->GetShiftRoster() && !pomViewer->IsRealRoster())
			{
				blRet = true;
			}
			else
			{
				char pclCodes[64];
				char pclCode[32];
				strcpy(pclCodes,(LPCTSTR)prpSdt->Fctc);
				char *pclPtr = strstr(pclCodes," - ");
				if(pclPtr!=NULL)
					*pclPtr = '\0';
				pclPtr = strtok(pclCodes,"\\");
				while(pclPtr!=NULL)
				{
					sprintf(pclCode,",%s,",pclPtr);
					if(strstr(pomViewer->GetPfcCodeList().GetBuffer(0),pclCode)!=NULL)
					{
						blRet = true;
						break;
					}
					pclPtr = strtok(NULL,"\\");
				}
			}
			break;
		}	
	case QUALIFICATION:
		{
			CString olQuali;
			if(prpSdt->Urno > 0)
			{
				CStringArray olList;
				olQuali = ogSdtRosterData.GetTotalQuali(prpSdt->Urno);
				if(!olQuali.IsEmpty())
				{
					if (omTypeList != LoadStg(IDS_STRING61216))
					{
						CStringArray olTypeList;
						ExtractItemList(omTypeList,&olTypeList);
						ExtractItemList(olQuali,&olList);

						for(int ilCount = 0; ilCount < olList.GetSize() - 1 && !blRet;ilCount++)
						{
							for (int ilTypes = 0; ilTypes < olTypeList.GetSize() - 1 && !blRet; ilTypes++)
							{
								if (olTypeList[ilTypes] == olList[ilCount])
								{
									blRet = true;
								}
							}
						}
					}
					else
					{
						blRet = true;
					}
				}
				else
				{
					int i= 1;
				}
			}
			break;
		}	
	}

	return blRet;
	CCS_CATCH_ALL
	return false;
}

void CCoverageGraphicWnd::SetMarkTime(CPaintDC *pDC,CTime opMarkTimeStart, CTime opMarkTimeEnd)
{
CCS_TRY
	if(opMarkTimeStart != TIMENULL && opMarkTimeEnd != TIMENULL)
	{
		CRect rcPaint;
		CTime  olStartTime = pomTimeScale->GetDisplayStartTime();
		CTimeSpan olTimeSpan  = pomTimeScale->GetDisplayDuration();
		CTime olEndTime = olStartTime + olTimeSpan;		
		GetClientRect(&rcPaint);
		if (opMarkTimeStart <= olStartTime)
			rcPaint.left = 0;
		else
			rcPaint.left = (int)GetX(opMarkTimeStart);

		// Use the client width if user want to repaint to the end of time
		if(opMarkTimeEnd!= TIMENULL && opMarkTimeEnd<= olEndTime)
			rcPaint.right = (int)GetX(opMarkTimeEnd) + 1;
		else
			rcPaint.right = 0;
		DrawTimeLines(pDC,rcPaint);
	}
CCS_CATCH_ALL
}

void CCoverageGraphicWnd::DrawTimeLines(CPaintDC *pDC,CRect rcPaint)
{
CCS_TRY;
    // Draw marked time start
	 if(rcPaint.left>0)
	 {
		  CPen penYellow(PS_SOLID, 0, RGB(255, 255, 0));
		  CPen *pOldPen = pDC->SelectObject(&penYellow);
		  pDC->MoveTo(rcPaint.left,rcPaint.top);
		  pDC->LineTo(rcPaint.left,rcPaint.bottom);
		  pDC->SelectObject(pOldPen);
	 }

	 // Draw marked time end
	 if(rcPaint.right>0)
	 {
		  CPen penYellow(PS_SOLID, 0, RGB(255, 255, 0));
		  CPen *pOldPen = pDC->SelectObject(&penYellow);
		  pDC->MoveTo(rcPaint.right,rcPaint.top);
		  pDC->LineTo(rcPaint.right,rcPaint.bottom);
		  pDC->SelectObject(pOldPen);
	 }
CCS_CATCH_ALL
}



int CCoverageGraphicWnd::CalcPIdx()
{
	double ilFlightSum = 0;
	double ilShiftSum = 0;
	int ilPIdx = -1;
	int ilRangeStart = 0;
	int	ilRangeEnd = 0;


	for (int ilFlight = 0; ilFlight < omCompleteFlightValues.GetSize(); ilFlight++)
	{
		if (pomViewer->SimulationActive() && pomViewer->SimDisplay() == 0)
			ilFlightSum += omCompleteSimFlightValues2[ilFlight];
		else
			ilFlightSum += omCompleteFlightValues[ilFlight];
	}

	for (int ilShift = 0; ilShift < omCompleteShiftArray.GetSize(); ilShift++)
	{
		ilShiftSum += omCompleteShiftArray[ilShift];
	}

	if (ilShiftSum > 0)
	{
		ilPIdx = (int) floor((ilFlightSum/ilShiftSum)*100);
	}

	if (pogCoverageStatistics != NULL)
	{
		if (pomViewer->SimulationActive() && pomViewer->SimDisplay() == 0)
			pogCoverageStatistics->UpdateDisplay(omCompleteShiftArray,omCompleteSimFlightValues2);
		else
			pogCoverageStatistics->UpdateDisplay(omCompleteShiftArray,omCompleteFlightValues);
	}
	return ilPIdx;
}

void CCoverageGraphicWnd::TransformSdtData(CCSPtrArray <SPLITTEDSDT> &ropSplittedSdt,SDTDATA *prpSdt)
{
	int ilFactor = prpSdt->Faktor;
	ropSplittedSdt.DeleteAll();
	if (prpSdt != NULL)
	{
		CCSPtrArray <RecordSet> olDelList;
		CString olBsdUrno;
		CTime olDate ;
		olBsdUrno.Format("%ld",prpSdt->Bsdu);

		ogBCD.GetRecords("DEL", "BSDU", olBsdUrno, &olDelList );

		// delegations from basic shift
		if (olDelList.GetSize() == 0)
		{

			SPLITTEDSDT rlSSdt;
			if (pomViewer->GetShiftRoster() == false && strcmp(prpSdt->Type,"D") == 0)
			{
				rlSSdt.Start = prpSdt->Sbgi;
				rlSSdt.End   = prpSdt->Seni;
			}
			else
			{
				rlSSdt.Start = prpSdt->Esbg;
				rlSSdt.End   = prpSdt->Lsen;
			}

			rlSSdt.Fctc = prpSdt->Fctc;
			rlSSdt.Brkf = prpSdt->Brkf;
			rlSSdt.Brkt = prpSdt->Brkt;
			rlSSdt.Urno = prpSdt->Urno;
			rlSSdt.Factor = ilFactor;
			ropSplittedSdt.New(rlSSdt);

		}
		else
		{
			static int ilStartIdx = -1;
			static int ilEndIdx	  = -1;
			static int ilFctcIdx  = -1;

			if (ilStartIdx == -1)
			{
				ilStartIdx = ogBCD.GetFieldIndex("DEL", "DELF");
				ilEndIdx   = ogBCD.GetFieldIndex("DEL", "DELT");
				ilFctcIdx  = ogBCD.GetFieldIndex("DEL", "FCTC");
			}

			CTime olSdtStart = prpSdt->Esbg;
			CTime olSdtEnd = prpSdt->Lsen;

			if (pomViewer->GetShiftRoster() == false && strcmp(prpSdt->Type,"D") == 0)
			{
				olSdtStart = prpSdt->Sbgi;
				olSdtEnd   = prpSdt->Seni;
			}

			for ( int illc = 0; illc < olDelList.GetSize(); illc++)
			{
				SPLITTEDSDT rlSSdt;

				olDate = olSdtStart;
				rlSSdt.Urno = prpSdt->Urno;
				rlSSdt.Factor = ilFactor;

				CString olStartTime = olDelList[illc].Values[ilStartIdx];
				CString olEndTime	= olDelList[illc].Values[ilEndIdx];
				CString olFctc		= olDelList[illc].Values[ilFctcIdx];
				
				CTime olTime = HourStringToDate(olStartTime, olDate);
				if(olTime < olDate)
				{
					olDate = olSdtEnd;
					olTime = HourStringToDate(olStartTime, olDate);
				}
				rlSSdt.Start = olTime;
				olTime = HourStringToDate(olEndTime, olDate);
				rlSSdt.End = olTime;
				rlSSdt.Brkf = rlSSdt.Start;
				rlSSdt.Brkt = rlSSdt.Start;
				rlSSdt.Fctc = olFctc;
				rlSSdt.Factor = ilFactor;

				ropSplittedSdt.New(rlSSdt);
			}

			ropSplittedSdt.Sort(CompareSSdtTimes);

			CCSPtrArray <SPLITTEDSDT> olNewSplittedSdt;
			int ilSSdtSize = ropSplittedSdt.GetSize();

			SPLITTEDSDT rlSSdt;

			for(  illc = 0 ; illc < ilSSdtSize; illc++)
			{
				if(olSdtStart <= ropSplittedSdt[illc].Start)
				{
					rlSSdt.Start = olSdtStart;
					rlSSdt.End = ropSplittedSdt[illc].Start;
					rlSSdt.Urno = prpSdt->Urno;
					rlSSdt.Brkf = rlSSdt.Start;
					rlSSdt.Brkt = rlSSdt.Start;
					rlSSdt.Fctc = prpSdt->Fctc;
					rlSSdt.Factor = ilFactor;

					olNewSplittedSdt.New(rlSSdt);
					olSdtStart = ropSplittedSdt[illc].End;
				}
			}

			if (ilSSdtSize > 0)
			{
				if(olSdtEnd >= ropSplittedSdt[ilSSdtSize-1].End)
				{
					rlSSdt.Start = ropSplittedSdt[ilSSdtSize-1].End;
					rlSSdt.End = olSdtEnd;
					rlSSdt.Brkf = rlSSdt.Start;
					rlSSdt.Brkt = rlSSdt.Start;
					rlSSdt.Urno = prpSdt->Urno;
		
					rlSSdt.Fctc = prpSdt->Fctc;
					rlSSdt.Factor = ilFactor;

					olNewSplittedSdt.New(rlSSdt);
				}
			}

			for(illc = 0; illc < olNewSplittedSdt.GetSize(); illc++)
			{
				ropSplittedSdt.New(olNewSplittedSdt[illc]);
			}

			olNewSplittedSdt.DeleteAll();
			ropSplittedSdt.Sort(CompareSSdtTimes);
		}

		// check delegations and absences from DRR record
		CCSPtrArray <SPLITTEDSDT> olDrdSplittedSdt;
		CCSPtrArray <SPLITTEDSDT> olDraSplittedSdt;

		if (pomViewer->GetShiftRoster() == true)
		{
			SPLITTEDSDT rlSplitted2;
			SPLITTEDSDT rlSplitted3;

			CCSPtrArray <SDTDATA> olSdtList;
			
			ogSdtRosterData.GetSdtsSdguAndType(prpSdt->Urno, 'D',olSdtList);
		
			for(int ilDrd = 0; ilDrd < olSdtList.GetSize(); ilDrd++)
			{
				rlSplitted2.Start = olSdtList[ilDrd].Esbg;
				rlSplitted2.End = olSdtList[ilDrd].Lsen;
				rlSplitted2.Brkf = rlSplitted2.Start;
				rlSplitted2.Brkt = rlSplitted2.Start;
				rlSplitted2.Fctc = olSdtList[ilDrd].Fctc;
			
				rlSplitted2.Factor = ilFactor;

				olDrdSplittedSdt.New(rlSplitted2);
			}

			olSdtList.RemoveAll();
			ogSdtRosterData.GetSdtsSdguAndType(prpSdt->Urno, 'A',olSdtList);
		
			for(int ilDra = 0; ilDra < olSdtList.GetSize(); ilDra++)
			{
				rlSplitted3.Start = olSdtList[ilDra].Esbg;
				rlSplitted3.End = olSdtList[ilDra].Lsen;
				rlSplitted3.Brkf = rlSplitted3.Start;
				rlSplitted3.Brkt = rlSplitted3.Start;
				rlSplitted3.Fctc = "";

				rlSplitted3.Factor = ilFactor;
				olDraSplittedSdt.New(rlSplitted3);
			}

			olSdtList.RemoveAll();

			olDrdSplittedSdt.Sort(CompareSSdtTimes);

			int ilSSdtSize = ropSplittedSdt.GetSize();

			SPLITTEDSDT rlSSdt;
			int ilDrdSSdtSize = olDrdSplittedSdt.GetSize();
			for(int ilDrdCount = 0; ilDrdCount < ilDrdSSdtSize; ilDrdCount++)
			{
				SPLITTEDSDT *prlDrdSSdt = &olDrdSplittedSdt[ilDrdCount];
				for(int illc = 0 ; illc < ilSSdtSize; illc++)
				{
					SPLITTEDSDT *prlSSdt = &ropSplittedSdt[illc];
					if( IsOverlapped(prlSSdt->Start, prlSSdt->End, prlDrdSSdt->Start, prlDrdSSdt->End))
					{
						if(prlDrdSSdt->Start <= prlSSdt->Start && prlDrdSSdt->End >= prlSSdt->Start)
						{
							prlSSdt->Start = prlDrdSSdt->End;
							prlSSdt->Brkf = prlSSdt->Start;
							prlSSdt->Brkt = prlSSdt->Start;
						}
						else if(prlDrdSSdt->End >= prlSSdt->End && prlDrdSSdt->Start <= prlSSdt->End)
						{
							prlSSdt->End = prlDrdSSdt->Start;
						}
						else if(prlDrdSSdt->End >= prlSSdt->End && prlDrdSSdt->Start <= prlSSdt->Start) 
						{
							prlSSdt->End = prlSSdt->Start;
						}
						else if(prlDrdSSdt->End < prlSSdt->End && prlDrdSSdt->Start > prlSSdt->Start) 
						{
							rlSSdt.Start = prlSSdt->Start;
							rlSSdt.End = prlDrdSSdt->Start;
							rlSSdt.Brkf = rlSSdt.Start;
							rlSSdt.Brkt = rlSSdt.Start;
							rlSSdt.Urno = prpSdt->Urno;
							rlSSdt.Fctc = prlSSdt->Fctc;
							rlSSdt.Factor = prlSSdt->Factor;

							ropSplittedSdt.NewAt(ilSSdtSize,rlSSdt);
							ilSSdtSize = ropSplittedSdt.GetSize();
							prlSSdt->Start = prlDrdSSdt->End;
							prlSSdt->Brkf = prlSSdt->Start;
							prlSSdt->Brkt = prlSSdt->Start;
						}

					}
				}
			}

			for(int illc = 0; illc < olDrdSplittedSdt.GetSize(); illc++)
			{
				ropSplittedSdt.New(olDrdSplittedSdt[illc]);
			}

			olDrdSplittedSdt.DeleteAll();

			ropSplittedSdt.Sort(CompareSSdtTimes);
			
			ilSSdtSize = ropSplittedSdt.GetSize();
			int ilDraSSdtSize = olDraSplittedSdt.GetSize();
			for(int ilDraCount = 0; ilDraCount < ilDraSSdtSize; ilDraCount++)
			{
				SPLITTEDSDT *prlDraSSdt = &olDraSplittedSdt[ilDraCount];
				for(illc = 0 ; illc < ilSSdtSize; illc++)
				{
					SPLITTEDSDT *prlSSdt = &ropSplittedSdt[illc];
					if(IsOverlapped(prlSSdt->Start, prlSSdt->End, prlDraSSdt->Start, prlDraSSdt->End))
					{
						if(prlDraSSdt->Start <= prlSSdt->Start && prlDraSSdt->End >= prlSSdt->Start)
						{
							prlSSdt->Start = prlDraSSdt->End;
							prlSSdt->Brkf = prlSSdt->Start;
							prlSSdt->Brkt = prlSSdt->Start;
						}
						else if(prlDraSSdt->End >= prlSSdt->End && prlDraSSdt->Start <= prlSSdt->End)
						{
							prlSSdt->End = prlDraSSdt->Start;
						}
						else if(prlDraSSdt->End >= prlSSdt->End && prlDraSSdt->Start <= prlSSdt->Start) 
						{
							prlSSdt->End = prlSSdt->Start;
						}
						else if(prlDraSSdt->End < prlSSdt->End && prlDraSSdt->Start > prlSSdt->Start) 
						{
							rlSSdt.Start = prlSSdt->Start;
							rlSSdt.End = prlDraSSdt->Start;
							rlSSdt.Brkf = rlSSdt.Start;
							rlSSdt.Brkt = rlSSdt.Start;
							rlSSdt.Urno = 0L;
							rlSSdt.Fctc = prlSSdt->Fctc;
							rlSSdt.Factor = prlSSdt->Factor;

							ropSplittedSdt.NewAt(ilSSdtSize,rlSSdt);
							ilSSdtSize = ropSplittedSdt.GetSize();
							prlSSdt->Start = prlDraSSdt->End;
							prlSSdt->Brkf = prlSSdt->Start;
							prlSSdt->Brkt = prlSSdt->Start;
						}

					}				
				}
			}

			olDraSplittedSdt.DeleteAll();
		}

		CTime olSdtStart = prpSdt->Brkf;
		CTime olSdtEnd = prpSdt->Brkt;

		int ilSSdtSize = ropSplittedSdt.GetSize();

		if(olSdtStart != TIMENULL && olSdtEnd != TIMENULL)
		{
			for( int illc = 0 ; illc < ilSSdtSize; illc++)
			{
				SPLITTEDSDT *prlSSdt = &ropSplittedSdt[illc];
				if(olSdtStart <= prlSSdt->End)
				{
					prlSSdt->Brkf = max(olSdtStart,prlSSdt->Start);
				}
				if((olSdtEnd >= prlSSdt->Start) && (olSdtStart <= prlSSdt->End))
				{
					prlSSdt->Brkt = min(olSdtEnd,prlSSdt->End);
				}
			}
		}

		olDelList.DeleteAll();
	}
}

void CCoverageGraphicWnd::OnTimeEval()
{
		POSITION rlPos;
		CCSPtrArray<DEMDATA> olTimeDems;
		for ( rlPos = pomViewer->GetDemUrnoMap()->GetStartPosition(); rlPos != NULL; )
		{
			long llUrno;
			DEMDATA *prlDem;
			pomViewer->GetDemUrnoMap()->GetNextAssoc(rlPos, (void *&)llUrno, (void *&)prlDem);
			if((prlDem->DispDeen > omClickTime) && IsBetween(omClickTime, prlDem->DispDebe, prlDem->DispDeen)) 
			{
				// --ALL-- ?
				if (this->bmEvaluateSingleFunctionOnly || this->imActuelRow == -1 || omLines[this->imActuelRow].Lkco == LoadStg(IDS_STRING61216))	
				{
					olTimeDems.Add(prlDem);
				}
				else
				{
					CString olPosUrnos;
					switch(imFilterIdx)
					{
					case FUNCTION:
						{
							olPosUrnos = ogDemData.GetPfcSingeUrnos(prlDem->Urud);
							break;
						}
					case QUALIFICATION:
						{
							olPosUrnos = ogDemData.GetPerSingeUrnos(prlDem->Urud);
							break;
						}
					case CHECKIN:
						{
							olPosUrnos = ogDemData.GetCicSingeUrnos(prlDem->Urud);
							break;
						}
					case POSITIONS:
						{
							olPosUrnos = ogDemData.GetPstSingeUrnos(prlDem->Urud);
							break;
						}
					case GATES:  
						{
							olPosUrnos = ogDemData.GetGatSingeUrnos(prlDem->Urud);					
							break;
						}
					case EQUIPMENT:  
						{
							olPosUrnos = ogDemData.GetReqSingeUrnos(prlDem->Urud);					
							break;
						}
					}


					CString olUrnoTxt = omLines[this->imActuelRow].UrnoText;
					if (imFilterIdx == QUALIFICATION)
					{
						if (olPosUrnos.Find(omLines[this->imActuelRow].UrnoText) > -1)
						{
							olTimeDems.Add(prlDem);
						}
					}
					else if (olPosUrnos.Find(omLines[this->imActuelRow].UrnoText) == 0)
					{
						olTimeDems.Add(prlDem);
					}
				}

			}
		}
		CString olHeader = LoadStg(IDS_STRING61257) + omClickTime.Format("%d.%m.%Y-%H:%M") ;
		CString olHeaderFID = LoadStg(IDS_STRING1959) + omClickTime.Format("%d.%m.%Y-%H:%M") ;
		CString olHeaderCCI = LoadStg(IDS_STRING1955) + omClickTime.Format("%d.%m.%Y-%H:%M") ;
		CheckDemands(olTimeDems,ogBasicData.GetBewTimePath(), olHeader,olHeaderFID,olHeaderCCI);
		olTimeDems.RemoveAll();
}

void CCoverageGraphicWnd::OnCompleteCheckDemands()
{
//	check how much functions do we have
	int ilRowSize = pomList->GetRowCount();

//	check, if there is "-*-" and "*All" only
	if ((this->imActuelRow == -1 || omLines[this->imActuelRow].Lkco == LoadStg(IDS_STRING61216)) || ilRowSize == 2)
	{
		POSITION rlPos;
		CCSPtrArray<DEMDATA> olTimeDems;
		for ( rlPos = pomViewer->GetDemUrnoMap()->GetStartPosition(); rlPos != NULL; )
		{
			long llUrno;
			DEMDATA *prlDem;
			pomViewer->GetDemUrnoMap()->GetNextAssoc(rlPos, (void *&)llUrno, (void *&)prlDem);

			olTimeDems.Add(prlDem);

		}
		CString olHeader = LoadStg(IDS_STRING61258) + pogCoverageDiagram->omCaptionText;
		CString olHeaderFID = LoadStg(IDS_STRING1960) + pogCoverageDiagram->omCaptionText;
		CString olHeaderCCI = LoadStg(IDS_STRING1956) + pogCoverageDiagram->omCaptionText;
		CheckDemands(olTimeDems,ogBasicData.GetBewTotalPath(), olHeader,olHeaderFID,olHeaderCCI);
		olTimeDems.RemoveAll();

	}
	else	// check Fct + "-*-" 
	{
		POSITION rlPos;
		CCSPtrArray<DEMDATA> olTimeDems;

		// Fct at first
		for ( rlPos = pomViewer->GetDemUrnoMap()->GetStartPosition(); rlPos != NULL; )
		{
			long llUrno;
			DEMDATA *prlDem;
			pomViewer->GetDemUrnoMap()->GetNextAssoc(rlPos, (void *&)llUrno, (void *&)prlDem);

			CString olPosUrnos;
			switch(imFilterIdx)
			{
			case FUNCTION:
				{
					olPosUrnos = ogDemData.GetPfcSingeUrnos(prlDem->Urud);
					break;
				}
			case QUALIFICATION:
				{
					olPosUrnos = ogDemData.GetPerSingeUrnos(prlDem->Urud);
					break;
				}
			case CHECKIN:
				{
					olPosUrnos = ogDemData.GetCicSingeUrnos(prlDem->Urud);
					break;
				}
			case POSITIONS:
				{
					olPosUrnos = ogDemData.GetPstSingeUrnos(prlDem->Urud);
					break;
				}
			case GATES:  
				{
					olPosUrnos = ogDemData.GetGatSingeUrnos(prlDem->Urud);					
					break;
				}
			case EQUIPMENT:  
				{
					olPosUrnos = ogDemData.GetReqSingeUrnos(prlDem->Urud);					
					break;
				}
			}


			CString olUrnoTxt = omLines[this->imActuelRow].UrnoText;
			if (imFilterIdx == QUALIFICATION)
			{
				if (olPosUrnos.Find(omLines[this->imActuelRow].UrnoText) > -1)
				{
					olTimeDems.Add(prlDem);
				}
			}
			else if (olPosUrnos.Find(omLines[this->imActuelRow].UrnoText) == 0)
			{
				olTimeDems.Add(prlDem);
			}
		}

		CString olHeader = LoadStg(IDS_STRING61258) + pogCoverageDiagram->omCaptionText;
		CString olHeaderFID = LoadStg(IDS_STRING1960) + pogCoverageDiagram->omCaptionText;
		CString olHeaderCCI = LoadStg(IDS_STRING1956) + pogCoverageDiagram->omCaptionText;
		CheckDemands(olTimeDems,ogBasicData.GetBewTotalPath(), olHeader,olHeaderFID,olHeaderCCI,false,false);
		olTimeDems.RemoveAll();

		// -*-
		for ( rlPos = pomViewer->GetDemUrnoMap()->GetStartPosition(); rlPos != NULL; )
		{
			long llUrno;
			DEMDATA *prlDem;
			pomViewer->GetDemUrnoMap()->GetNextAssoc(rlPos, (void *&)llUrno, (void *&)prlDem);

			olTimeDems.Add(prlDem);

		}
		olHeader = LoadStg(IDS_STRING61258) + pogCoverageDiagram->omCaptionText;
		olHeaderFID = LoadStg(IDS_STRING1960) + pogCoverageDiagram->omCaptionText;
		olHeaderCCI = LoadStg(IDS_STRING1956) + pogCoverageDiagram->omCaptionText;
		CheckDemands(olTimeDems,ogBasicData.GetBewTotalPath(), olHeader,olHeaderFID,olHeaderCCI,true,true);
		olTimeDems.RemoveAll();

	}

}

void CCoverageGraphicWnd::CheckDemands(CCSPtrArray <DEMDATA> &ropDemList,CString opFileName,CString& ropHeader,CString& ropHeaderFID,CString& ropHeaderCCI,bool bpAppend,bool bpDisplay)
{
	char pclFile[512]="";
	CMapStringToPtr	olKeyMap;
	CMapPtrToPtr	olIgnoreMap;
	CMapPtrToPtr	olCCIMap;
	
	ofstream of;
	int ilCount = 0;
	sprintf(pclFile,"%s", opFileName);

	bool blSortByRotation = ogCfgData.rmUserSetup.EVSO == CString("0");

	if (bpAppend)
	{
		of.open(pclFile, ios::out|ios::app);
		of << endl;
		of << endl;
		of << "--------------------------------------------------------------------------------------------------" << endl;

	}
	else
	{
		of.open(pclFile, ios::out);
	}

	// sort items by time and type
	ropDemList.Sort(CompareDemTimesAndFltno); 


	//	get all CCI demands
	CDWordArray olCCIDemands;
	for (int i = 0; i < ropDemList.GetSize(); i++)
	{
		if (IsCommonCheckin(&ropDemList[i]))
		{
			olCCIDemands.Add(ropDemList[i].Urno);			
		}
	}

	// get all flights according to the CCI demands
	if (olCCIDemands.GetSize() > 0)
	{
		ogDemData.GetFlightsFromCciDemand(olCCIDemands,olCCIMap);
	}

	// check on missing flights
	CMapPtrToPtr olMissingFlights;
	CMapPtrToPtr olSumCCIFlights;

	for (POSITION pos = olCCIMap.GetStartPosition(); pos != NULL;)
	{
		void *polKey;
		CStringArray *polFlights = NULL;
		olCCIMap.GetNextAssoc(pos,polKey,(void *&)polFlights);
		if (polFlights != NULL)
		{
			void *polValue;
			for (int i = 0; i < polFlights->GetSize(); i++)
			{
				long llUrno = atol(polFlights->GetAt(i));
				if (ogCovFlightData.GetFlightByUrno(llUrno) == NULL)
				{
					if (olMissingFlights.Lookup((void *)llUrno,polValue) == FALSE)
					{
						olMissingFlights.SetAt((void *)llUrno,(void *)llUrno);												
					}
				}

				if (olSumCCIFlights.Lookup((void *)llUrno,polValue) == FALSE)
				{
					olSumCCIFlights.SetAt((void *)llUrno,(void *)llUrno);												
				}

			}
		}
	}

	ogCovFlightData.ReadMissingFlights(olMissingFlights);

	bool blHeader = false;
	bool blFID	  = false;
	bool blCCI	  = false;

	bool blHeaderSum = false;
	bool blFIDSum    = false;
	bool blCCISum    = false;

	int  ilSum    = 0;
	int	 ilSumFID = 0;
	int	 ilSumCCI = 0;

	if (blSortByRotation)
	{
		// link all depending demands together 
		for (int i = 0; i < ropDemList.GetSize(); i++)
		{
			CString olKey;
			CCSPtrArray<DEMDATA>* polDemands;
			olKey.Format("%s,%d,%d",ropDemList[i].Obty,ropDemList[i].Ouri,ropDemList[i].Ouro);
			if (olKeyMap.Lookup(olKey,(void *&)polDemands) == FALSE)
			{
				polDemands = new CCSPtrArray<DEMDATA>();
				olKeyMap.SetAt(olKey,polDemands);
			}
			else
			{
				olIgnoreMap.SetAt(&ropDemList[i],&ropDemList[i]);
			}
			polDemands->Add(&ropDemList[i]);
				
		}

		for (i = 0; i < ropDemList.GetSize(); i++)
		{
			void *polValue;
			if (olIgnoreMap.Lookup(&ropDemList[i],polValue))
			{
				continue;
			}

			CString olKey;
			CCSPtrArray<DEMDATA> *polDemands = NULL;
			olKey.Format("%s,%d,%d",ropDemList[i].Obty,ropDemList[i].Ouri,ropDemList[i].Ouro);
			if (olKeyMap.Lookup(olKey,(void *&)polDemands) == FALSE || polDemands == NULL)
			{
				continue;
			}


			for (int j = 0; j < polDemands->GetSize(); j++)
			{
				if (IsCommonCheckin(&(*polDemands)[j]))
				{
					if (blCCI == false)
					{
						if (blHeader == true && blHeaderSum == false)
						{
							blHeaderSum = true;

							int ilHours = ilSum / 60;
							int ilMinutes = ilSum - ilHours * 60;

							of << "--------------------------------------------------------------------------------------------------" << endl;
							of << (const char *)LoadStg(IDS_STRING1963);
							of << endl;
							of << "                                                                                  " << setw(5) << ilHours << " h:" << setw(2) << ilMinutes << " min" << endl;
							of << endl;
							of << endl;
						}

						if (blFID == true && blFIDSum == false)
						{
							blFIDSum = true;

							int ilHours = ilSumFID / 60;
							int ilMinutes = ilSumFID - ilHours * 60;

							of << "--------------------------------------------------------------------------------------------------" << endl;
							of << (const char *)LoadStg(IDS_STRING1962);
							of << endl;
							of << "                                  " << setw(5) << ilHours << " h:" << setw(2) << ilMinutes << " min" << endl;
							of << endl;
							of << endl;
						}

						blCCI = true;
						of << ropHeaderCCI << endl;
						of << "--------------------------------------------------------------------------------------------------" << endl;

						of << (const char *)LoadStg(IDS_STRING1957);
						of << endl;	
					}
				}
				else if (!IsFlightDependend(&(*polDemands)[j]))
				{
					if (blFID == false)
					{
						if (blHeader == true && blHeaderSum == false)
						{
							blHeaderSum = true;

							int ilHours = ilSum / 60;
							int ilMinutes = ilSum - ilHours * 60;

							of << "--------------------------------------------------------------------------------------------------" << endl;
							of << (const char *)LoadStg(IDS_STRING1963);
							of << endl;
							of << "                                                                                  " << setw(5) << ilHours << " h:" << setw(2) << ilMinutes << " min" << endl;
							of << endl;
							of << endl;
						}

						blFID = true;
						of << ropHeaderFID << endl;
						of << "--------------------------------------------------------------------------------------------------" << endl;

						of << (const char *)LoadStg(IDS_STRING1958);
						of << endl;	
					}
				}
				else
				{
					if (blHeader == false)
					{
						blHeader = true;
									
						of << ropHeader << endl;
						of << "--------------------------------------------------------------------------------------------------" << endl;

						of << (const char *)LoadStg(IDS_STRING1803);
						of << endl;	
					}
				}

				CheckDemand(of,++ilCount,(*polDemands)[j],olCCIMap,ilSum,ilSumFID,ilSumCCI);
			}

			delete polDemands;

			of << '\n';
		}		
	}
	else	// sort by demand time
	{
		for (int i = 0; i < ropDemList.GetSize(); i++)
		{
			if (IsCommonCheckin(&ropDemList[i]))
			{
				if (blCCI == false)
				{

					if (blHeader == true && blHeaderSum == false)
					{
						blHeaderSum = true;

						int ilHours = ilSumFID / 60;
						int ilMinutes = ilSumFID - ilHours * 60;

						of << "--------------------------------------------------------------------------------------------------" << endl;
						of << (const char *)LoadStg(IDS_STRING1963);
						of << endl;
						of << "                                                                                  " << setw(5) << ilHours << " h:" << setw(2) << ilMinutes << " min" << endl;
						of << endl;
						of << endl;
					}

					if (blFID == true && blFIDSum == false)
					{
						blFIDSum = true;

						int ilHours = ilSumFID / 60;
						int ilMinutes = ilSumFID - ilHours * 60;

						of << "--------------------------------------------------------------------------------------------------" << endl;
						of << (const char *)LoadStg(IDS_STRING1962);
						of << endl;
						of << "                                  " << setw(5) << ilHours << " h:" << setw(2) << ilMinutes << " min" << endl;
						of << endl;
						of << endl;
					}

					blCCI = true;
					of << ropHeaderCCI << endl;
					of << "--------------------------------------------------------------------------------------------------" << endl;
					of << (const char *)LoadStg(IDS_STRING1957);
					of << endl;	
				}
			}
			else if (!IsFlightDependend(&ropDemList[i]))
			{
				if (blFID == false)
				{

					if (blHeader == true && blHeaderSum == false)
					{
						blHeaderSum = true;

						int ilHours = ilSum / 60;
						int ilMinutes = ilSum - ilHours * 60;

						of << "--------------------------------------------------------------------------------------------------" << endl;
						of << (const char *)LoadStg(IDS_STRING1963);
						of << endl;
						of << "                                                                                  " << setw(5) << ilHours << " h:" << setw(2) << ilMinutes << " min" << endl;
						of << endl;
						of << endl;
					}

					blFID = true;
					of << ropHeaderFID << endl;
					of << "--------------------------------------------------------------------------------------------------" << endl;
					of << (const char *)LoadStg(IDS_STRING1958);
					of << endl;	
				}
			}
			else
			{
				if (blHeader == false)
				{
					blHeader = true;
									
					of << ropHeader << endl;
					of << "--------------------------------------------------------------------------------------------------" << endl;

					of << (const char *)LoadStg(IDS_STRING1803);
					of << endl;	
				}
			}

			CheckDemand(of,++ilCount,ropDemList[i],olCCIMap,ilSum,ilSumFID,ilSumCCI);
		}
	}


	if (blHeader == true && blHeaderSum == false)
	{
		blHeaderSum = true;

		int ilHours = ilSum / 60;
		int ilMinutes = ilSum - ilHours * 60;

		of << "--------------------------------------------------------------------------------------------------" << endl;
		of << (const char *)LoadStg(IDS_STRING1963);
		of << endl;
		of << "                                                                                  " << setw(5) << ilHours << " h:" << setw(2) << ilMinutes << " min" << endl;
		of << endl;
		of << endl;
	}

	if (blFID == true && blFIDSum == false)
	{
		blFIDSum = true;

		int ilHours = ilSumFID / 60;
		int ilMinutes = ilSumFID - ilHours * 60;

		of << "--------------------------------------------------------------------------------------------------" << endl;
		of << (const char *)LoadStg(IDS_STRING1962);
		of << endl;
		of << "                                  " << setw(5) << ilHours << " h:" << setw(2) << ilMinutes << " min" << endl;
		of << endl;
		of << endl;
	}

	if (blCCI == true && blCCISum == false)
	{
		blCCISum = true;

		int ilHours = ilSumCCI / 60;
		int ilMinutes = ilSumCCI - ilHours * 60;

		of << "--------------------------------------------------------------------------------------------------" << endl;
		of << (const char *)LoadStg(IDS_STRING1961);
		of << endl;
//		of << "                                  " << setw(5) << ilHours << " h:" << setw(2) << ilMinutes << " min" << endl;
		of << "         " << setw(5) << olSumCCIFlights.GetCount() << (const char *)LoadStg(IDS_STRING1966) << setw(5) << ilHours << " h:" << setw(2) << ilMinutes << " min" << endl;
		of << endl;
		of << endl;
	}

	// release CCI map
	for (pos = olCCIMap.GetStartPosition(); pos != NULL; )
	{
		void *polKey;
		CStringArray *polValue = NULL;
		olCCIMap.GetNextAssoc(pos,polKey,(void *&)polValue);
		delete polValue;
	}

	of.close(); 
	char pclArgStr[256];
	

	if (bpDisplay)
	{
		CString olArgString = ogBasicData.GetEditor() + CString(" ") + opFileName;
		// initialize STARTUPINFO struct
		STARTUPINFO Info;
		ZeroMemory(&Info, sizeof(Info));
		Info.cb = sizeof(Info);
		Info.dwX = 50;
		Info.dwY = 50;
		Info.dwXSize = 1200;
		Info.dwYSize = 600;
		Info.dwFlags = STARTF_FORCEONFEEDBACK | STARTF_USEPOSITION | STARTF_USESIZE;
		strcpy(pclArgStr,(LPCSTR) olArgString ); 

		// initialize PROCESS_INFORMATION struct
		PROCESS_INFORMATION *pProcInfo = new PROCESS_INFORMATION;
		ZeroMemory(pProcInfo, sizeof(pProcInfo));
		
		BOOL ilRet = CreateProcess(NULL,pclArgStr,NULL,NULL,FALSE,CREATE_DEFAULT_ERROR_MODE,NULL,NULL,&Info,pProcInfo);
		delete pProcInfo;
	}
}


void CCoverageGraphicWnd::CheckDemand(ofstream& of,int ilCount,DEMDATA& ropDemand,CMapPtrToPtr& ropCCIMap,int& ropSum,int& ropSumFID,int& ropSumCCI)	
{
			CString olTyp = ropDemand.Obty;

			CString olFlightInfo;
			if(olTyp == "AFT" )
			{
				FLIGHTDATA *prlFlight = NULL;
				if (ropDemand.Dety[0] == '0')
				{
					FLIGHTDATA *prlFlight1 = ogCovFlightData.GetFlightByUrno(ropDemand.Ouri);
					FLIGHTDATA *prlFlight2 = ogCovFlightData.GetFlightByUrno(ropDemand.Ouro);
						
					CString olTmp;
					CString olTmp1;
					CString olFltn1;
					CString olFltn2;
					CString olAlc1;
					CString olAlc2;
					CString olFlns1;
					CString olFlns2;
					CString olTtyp;
						
					if(prlFlight1 != NULL)
					{
						olAlc1 = prlFlight1->Alc2;
						olAlc1.TrimLeft();
						if(olAlc1.IsEmpty())
						{
							olAlc1.Format("%5s", prlFlight1->Alc3);
						}
						else
						{
							olAlc1.Format("%5s", prlFlight1->Alc2);
						}

						olTmp.Format("%5s", prlFlight1->Act5);
				
						olFltn1.Format("%6s", prlFlight1->Fltn);
						olFlns1.Format("%3s", prlFlight1->Flns);
						olTmp.Format  ("%5s", prlFlight1->Act5);
						olTmp1.Format ("%3s",  prlFlight1->Act3);
						olTtyp.Format ("%5s",  prlFlight1->Ttyp);

					}

					if(prlFlight2 != NULL)
					{
						olAlc2 = prlFlight2->Alc2;
						olAlc2.TrimLeft();
						if(olAlc2.IsEmpty())
						{
							olAlc2.Format("%5s", prlFlight2->Alc3);
						}
						else
						{
							olAlc2.Format("%5s", prlFlight2->Alc2);
						}

						
						olFltn2.Format("%6s", prlFlight2->Fltn);
						olFlns2.Format("%3s", prlFlight2->Flns);
						olTmp.Format  ("%5s", prlFlight2->Act5);
						olTmp1.Format ("%3s", prlFlight2->Act3);
						olTtyp.Format ("%5s", prlFlight2->Ttyp);
					}

						
					olFlightInfo+= CString(" ")		// %1s
								+ olAlc1			// %5s 
								+ olFltn1			// %6s
								+ olFlns1			// %3s
								+ "/"				// %1s
								+ olAlc2			// %5s	
								+ olFltn2			// %6s
								+ olFlns2			// %3s
								+ CString("   ")	// %3s	
								+ olTmp1			// %3s 	
								+ CString("/")		// %1s	
								+ olTmp				// %5s	
								+ CString("  ")		// %2s		
								+ olTtyp;			// %5s


				}
				else
				{
					if (ropDemand.Dety[0] == '1') // Inbound
					{
						prlFlight = ogCovFlightData.GetFlightByUrno(ropDemand.Ouri);
					}
					else
					{
						prlFlight = ogCovFlightData.GetFlightByUrno(ropDemand.Ouro);
					}
					
					if(prlFlight != NULL)
					{
						CString olTmp;
						CString olFltn;
						CString olFlns;
						CString olTtyp;
						CString olTmp1;
						CString olAlc = prlFlight->Alc2;
						olAlc.TrimLeft();
						if(olAlc.IsEmpty())
						{
							olAlc.Format("%5s", prlFlight->Alc3);
						}
						else
						{
							olAlc.Format("%5s", prlFlight->Alc2);
						}

						
						olFltn.Format ("%6s", prlFlight->Fltn);
						olFlns.Format ("%3s", prlFlight->Flns);
						olTmp.Format  ("%5s", prlFlight->Act5);
						olTmp1.Format ("%3s", prlFlight->Act3);
						olTtyp.Format ("%5s", prlFlight->Ttyp);
						olTmp.Format  ("%5s", prlFlight->Act5);

						if (ropDemand.Dety[0] == '1') // Inbound
						{
							olFlightInfo+= CString(" ") 
										+ olAlc 
										+ olFltn 
										+ olFlns 
										+ CString("                  ") // %18s
										+ olTmp1 
										+ CString("/") 
										+ olTmp 
										+ CString("  ") 
										+ olTtyp;
						}
						else
						{
							olFlightInfo+= CString("                ")	// %16s
										+ olAlc 
										+ olFltn 
										+ olFlns 
										+ CString("   ") 
										+ olTmp1 
										+ CString("/") 
										+ olTmp 
										+ CString("  ") 
										+ olTtyp;
						}
					}
					else
					{
						olFlightInfo += "                                                 ";
					}
				}
			}
			else
			{
				olFlightInfo += "                                                 ";
			}

			CString olFkt;
			CString olFctText = "Fkt:";

					 
			switch(imFilterIdx)
			{
			case FUNCTION:
				{
					CStringArray olItemList;
					
					CString olSingleUrnos = ogDemData.GetPfcSingeUrnos(ropDemand.Urud);

					olFctText = LoadStg(IDS_STRING61218);
					if(!olSingleUrnos.IsEmpty())
					{
						ExtractItemList(olSingleUrnos, &olItemList, ',');
						for(int ilCount = 0;ilCount < olItemList.GetSize(); ilCount++)
						{
							long llUrno = atol(olItemList[ilCount]);
							CString olTmpFkt = ogPfcData.GetFctcByUrno(llUrno) ;
							if(olTmpFkt.IsEmpty())
							{
								olTmpFkt = ogBCD.GetFieldExt("SGR", "URNO", "TABN", olItemList[ilCount], "PFC", "GRPN");
							}
							if(!olTmpFkt.IsEmpty())
							{
								olTmpFkt += "  ";
								olFkt += olTmpFkt;
							}
						}
					}
					olItemList.RemoveAll();
					break;
				}
			case QUALIFICATION:
				{
					CStringArray olItemList;
					
					CString olSingleUrnos = ogDemData.GetPerSingeUrnos(ropDemand.Urud);
					olFctText = LoadStg(IDS_STRING61217);
					if(!olSingleUrnos.IsEmpty())
					{
						ExtractItemList(olSingleUrnos, &olItemList, ',');
						for(int ilCount = 0;ilCount < olItemList.GetSize(); ilCount++)
						{
							long llUrno = atol(olItemList[ilCount]);
							CString olTmpFkt = ogPerData.GetPrmcByUrno(llUrno) ;
							if(olTmpFkt.IsEmpty())
							{
								olTmpFkt = ogBCD.GetFieldExt("SGR", "URNO", "TABN", olItemList[ilCount], "PER", "GRPN");
							}
							if(!olTmpFkt.IsEmpty())
							{
								olTmpFkt += "  ";
								olFkt += olTmpFkt;
							}
						}
					}
					olItemList.RemoveAll();
					break;
				}
			case CHECKIN:
				{
					CStringArray olItemList;
					
					CString olSingleUrnos = ogDemData.GetCicSingeUrnos(ropDemand.Urud);
					olFctText = LoadStg(IDS_STRING61236);
					if(!olSingleUrnos.IsEmpty())
					{
						ExtractItemList(olSingleUrnos, &olItemList, ',');
						for(int ilCount = 0;ilCount < olItemList.GetSize(); ilCount++)
						{
							CString olTmpFkt = ogBCD.GetField("CIC", "URNO", olItemList[ilCount], "CNAM");
							if(olTmpFkt.IsEmpty())
							{
								olTmpFkt = ogBCD.GetFieldExt("SGR", "URNO", "TABN", olItemList[ilCount], "CIC", "GRPN");
							}
							if(!olTmpFkt.IsEmpty())
							{
								olTmpFkt += "  ";
								olFkt += olTmpFkt;
							}
						}
					}
					olItemList.RemoveAll();
					break;
				}
			case POSITIONS:
				{
					CStringArray olItemList;
					
					CString olSingleUrnos = ogDemData.GetPstSingeUrnos(ropDemand.Urud);
					olFctText = LoadStg(IDS_STRING61237);
					if(!olSingleUrnos.IsEmpty())
					{
						ExtractItemList(olSingleUrnos, &olItemList, ',');
						for(int ilCount = 0;ilCount < olItemList.GetSize(); ilCount++)
						{
							CString olTmpFkt = ogBCD.GetField("PST", "URNO", olItemList[ilCount], "PNAM");
							if(olTmpFkt.IsEmpty())
							{
								olTmpFkt = ogBCD.GetFieldExt("SGR", "URNO", "TABN", olItemList[ilCount], "PST", "GRPN");
							}
							if(!olTmpFkt.IsEmpty())
							{
								olTmpFkt += "  ";
								olFkt += olTmpFkt;
							}
						}
					}
					olItemList.RemoveAll();
					break;
				}
			case GATES:  
				{
					CStringArray olItemList;
						
					CString olSingleUrnos = ogDemData.GetGatSingeUrnos(ropDemand.Urud);
					olFctText = LoadStg(IDS_STRING61238);

					if(!olSingleUrnos.IsEmpty())
					{
						ExtractItemList(olSingleUrnos, &olItemList, ',');
						for(int ilCount = 0;ilCount < olItemList.GetSize(); ilCount++)
						{
							CString olTmpFkt = ogBCD.GetField("GAT", "URNO", olItemList[ilCount], "GNAM");
							if(olTmpFkt.IsEmpty())
							{
								olTmpFkt = ogBCD.GetFieldExt("SGR", "URNO", "TABN", olItemList[ilCount], "GAT", "GRPN");
							}
							if(!olTmpFkt.IsEmpty())
							{
								olTmpFkt += "  ";
								olFkt += olTmpFkt;
							}
						}
					}
					olItemList.RemoveAll();
					break;
				}
			case EQUIPMENT:  
				{
					CStringArray olItemList;
						
					CString olSingleUrnos = ogDemData.GetReqSingeUrnos(ropDemand.Urud);
					olFctText = LoadStg(IDS_STRING61239);
					if(!olSingleUrnos.IsEmpty())
					{
						CString olUrud;
						olUrud.Format("%d",ropDemand.Urud);
						CString olEqco = ogBCD.GetField("REQ","URUD",olUrud,"EQCO");
						ExtractItemList(olSingleUrnos, &olItemList, ',');
						for(int ilCount = 0;ilCount < olItemList.GetSize(); ilCount++)
						{
							if (!olEqco.IsEmpty())
							{
								CString olEqt = ogBCD.GetField("EQU","URNO",olItemList[ilCount],"GCDE");
								if(!olEqt.IsEmpty())
								{
									olEqt += "  ";
									olFkt += olEqt;
								}
							}
							else
							{
								CString olEqt = ogBCD.GetFieldExt("SGR", "URNO", "TABN", olItemList[ilCount], "EQU", "GRPN");
								if(!olEqt.IsEmpty())
								{
									olEqt += "  ";
									olFkt += olEqt;
								}
							}
						}
					}
					olItemList.RemoveAll();
					break;
				}
			}	
			

			CString olRuleName;
			CString olRuleText = LoadStg(IDS_STRING61259);
			olRuleName = ropDemand.Rusn;
			if(ropDemand.Rust == 2)
			{
				olRuleText = LoadStg(IDS_STRING61363) + " " + LoadStg(IDS_STRING61259);
			}

			CString olDuration;
			olDuration.Format("%4ld",(ropDemand.DispDeen-ropDemand.DispDebe).GetTotalMinutes());

			if (IsCommonCheckin(&ropDemand))
			{
				CString olFlights;
				CStringArray *polFlightList;
				if (ropCCIMap.Lookup(reinterpret_cast<void *>(ropDemand.Urno),(void *&)polFlightList))
				{
					for (int i = 1; i < polFlightList->GetSize(); i++)
					{

						FLIGHTDATA *prlFlight = ogCovFlightData.GetFlightByUrno(atol(polFlightList->GetAt(i)));
						if (prlFlight != NULL)
						{
							if (!olFlights.IsEmpty())
							{
								olFlights += ',';
							}

							olFlights += ogCovFlightData.CreateFlno(prlFlight->Alc2,prlFlight->Fltn,prlFlight->Flns);
						}
					}
				}

				ropSumCCI += atoi(olDuration);

				of << setw(3) << ilCount << setw(23)
				   << setw(17) << (ropDemand.DispDebe).Format("%d/%H:%M-") << (ropDemand.DispDeen).Format("%d/%H:%M")
				   << "    "
				   << olDuration
				   << "         " 
				   << setw(6) << olFctText  << setw(15) << olFkt.GetBuffer(0) 
				   << setw(4) << olRuleText << olRuleName << " " << olFlights << endl;
			}
			else if (!IsFlightDependend(&ropDemand))
			{
				ropSumFID += atoi(olDuration);

				of << setw(3) << ilCount << setw(23)
				   << setw(17) << (ropDemand.DispDebe).Format("%d/%H:%M-") << (ropDemand.DispDeen).Format("%d/%H:%M")
				   << "    "
				   << olDuration
				   << "         " 
				   << setw(6) << olFctText  << setw(15) << olFkt.GetBuffer(0) 
				   << setw(4) << olRuleText << olRuleName << endl;
			}
			else
			{
				ropSum += atoi(olDuration);

				of << setw(3) << ilCount << setw(23) << olFlightInfo 
				   << setw(17) << (ropDemand.DispDebe).Format("%d/%H:%M-") << (ropDemand.DispDeen).Format("%d/%H:%M")
				   << "    "
				   << olDuration
				   << "         " 
				   << setw(6) << olFctText  << setw(15) << olFkt.GetBuffer(0) 
				   << setw(4) << olRuleText << olRuleName << endl;
			}
}

int	 CCoverageGraphicWnd::FindListRow(int ipIndex)
{
	CGXStyle olStyle;
	olStyle.SetDefault();

	int ilRowSize = pomList->GetRowCount();
	for (int ilRow = 1; ilRow <= ilRowSize; ilRow++)
	{
		pomList->GetStyleRowCol(ilRow,1,olStyle,gxOverride);
		if (olStyle.GetItemDataPtr() == (void *)ipIndex)
			return ilRow;
	}

	return -1;
}

void CCoverageGraphicWnd::OnActuellCellMoved(WPARAM wParam, LPARAM lParam)
{
	CCS_TRY

	if (bmSorting)
		return;

	CWaitCursor olDummy;

	CGXStyle olStyle;
	CGXStyle olStyle2;

	olStyle.SetDefault();

	int ilRow = ((CELLPOS*)lParam)->Row;

	int ilRowSize = pomList->GetRowCount();
	if(ilRow < 1 || ilRow > ilRowSize)
		return;

	int ilOldRow = FindListRow(imActuelRow);
	bool blResetOldRow = false;

	pomList->GetStyleRowCol(ilRow,1,olStyle,gxOverride);
	imActuelRow = (int)olStyle.GetItemDataPtr();

	if(ilOldRow > 0 && ilOldRow <= ilRowSize)
	{
		pomList->GetStyleRowCol(ilOldRow,1,olStyle2,gxOverride);
		blResetOldRow = true;

	}


	olStyle.SetInterior(ogColors[GRAY_IDX]);
	olStyle2.SetInterior(ogColors[SILVER_IDX]);
	
	pomList->GetParam()->EnableUndo(FALSE);

	pomList->GetParam()->SetLockReadOnly(FALSE);
	
	pomList->SetStyleRange(CGXRange(ilRow, 1, ilRow, 1), olStyle);

	if(blResetOldRow)
	{
		pomList->SetStyleRange(CGXRange(ilOldRow, 1, ilOldRow, 1), olStyle2);
	}


	pomList->GetParam()->SetLockReadOnly(TRUE);

	pomList->GetParam()->EnableUndo(TRUE);
#if	0
	PrepareSingleShiftPoly();

	switch (pomViewer->imSimVariant)
	{
	case 0 :
		CalculateSimOffsetFlightDemands();
	break;
	case 1 :
		CalculateSimCutFlightDemands();
	break;
	case 2 :
		CalculateSimSmoothFlightDemands();
	break;
	}
	
	Invalidate();
#else
	SetTimeFrame(false);
#endif

	CCS_CATCH_ALL

}


void CCoverageGraphicWnd::FillList()
{
	int ilRowCount = omLines.GetSize();

	CGXStyle olStyle;


	switch(imFilterIdx)
	{
	case FUNCTION:
		{
			omFilterType = LoadStg(IDS_STRING61218);
			break;
		}
	case QUALIFICATION:
		{
			omFilterType = LoadStg(IDS_STRING61217);
			break;
		}
	case CHECKIN:
		{
			omFilterType = LoadStg(IDS_STRING61236);
			break;
		}
	case POSITIONS:
		{
			omFilterType = LoadStg(IDS_STRING61237);
			break;
		}
	case GATES:  
		{
			omFilterType = LoadStg(IDS_STRING61238);
			break;
		}
	case EQUIPMENT:
		{
			
			omFilterType = ogBCD.GetField("EQT","URNO",pomViewer->GetFilterEquipmentType(),"NAME");
			break;
		}
	}
//	olStyle.SetFont(omCGXFont);
	pomList->LockUpdate(TRUE);

	olStyle.SetDefault();

	LONG llFonSize = (olStyle.GetFont()).GetSize();
	
	pomList->GetParam()->EnableUndo(FALSE);


	CString olNames;

	int ilMaxWidth = omFilterType.GetLength();
	for(int ilLc = 0;ilLc < ilRowCount;ilLc++)
	{
		olNames = omLines[ilLc].Lkco;
		ilMaxWidth = max(ilMaxWidth,olNames.GetLength());
	}
	
	pomList->RemoveRows(1,  pomList->GetRowCount());
	pomList->SetRowCount(ilRowCount +1);
	
	pomList->SetColWidth(1,1,(llFonSize*ilMaxWidth));

//	olStyle.SetEnabled(FALSE);
	olStyle.SetReadOnly(TRUE);

	pomList->SetValueRange(CGXRange(0,1),omFilterType);


	ilRowCount += 1;
	olStyle.SetTextColor(ogColors[BLACK_IDX]);

	if (imActuelRow == -1)
		olStyle.SetInterior(ogColors[GRAY_IDX]);
	else
		olStyle.SetInterior(ogColors[SILVER_IDX]);

	pomList->SetStyleRange(CGXRange(1, 1, 1, 1), olStyle.SetValue("-*-").SetItemDataPtr((void *) -1));


	for(ilLc = 1; ilLc < ilRowCount; ilLc++)
	{
		if (imActuelRow == ilLc - 1)
			olStyle.SetInterior(ogColors[GRAY_IDX]);
		else
			olStyle.SetInterior(ogColors[SILVER_IDX]);

		olStyle.SetTextColor(omLines[ilLc -1].Color);

		pomList->SetStyleRange(CGXRange(ilLc+1, 1, ilLc+1, 1), olStyle.SetValue(omLines[ilLc -1].Lkco).SetItemDataPtr((void *)(ilLc-1)));
	}

	CGXSortInfoArray rlSortInfo;
	rlSortInfo.SetSize(1);
	rlSortInfo[0].nRC		= 1;
	rlSortInfo[0].bCase		= false;
	rlSortInfo[0].sortType	= CGXSortInfo::autodetect;
	rlSortInfo[0].sortOrder = CGXSortInfo::ascending;

	if (ilRowCount > 2)
	{
		bmSorting = true;
		pomList->SortRows(CGXRange().SetTable(),rlSortInfo);
		bmSorting = false;
	}
	else
	{
		bmSorting = false;
	}

	pomList->GetParam()->EnableUndo(TRUE);

	pomList->LockUpdate(FALSE);


}

void CCoverageGraphicWnd::PrepareSingleShiftPoly()
{
	CTime olLoadStart,olLoadEnd;
	pomViewer->GetTimeFrameFromTo(olLoadStart,olLoadEnd);

	CUIntArray olSingleShiftArray;


	int ilOffSet = (omStartTime - olLoadStart).GetTotalMinutes();
	int ilBorder = ilOffSet + (omEndTime - omStartTime).GetTotalMinutes();

	int ilSize = omShiftLines.GetSize();

	if(imActuelRow >= 0 && imActuelRow < ilSize)
	{
		if (ilBorder <= omShiftLines[imActuelRow].Values.GetSize())
		{
			olSingleShiftArray.RemoveAll();
			olSingleShiftArray.InsertAt(0,0,ilBorder - ilOffSet);

			for (int ilLc = 0; ilLc < ilBorder - ilOffSet;ilLc++)
			{
				olSingleShiftArray[ilLc] = omShiftLines[imActuelRow].Values[ilLc + ilOffSet];
			}
		}
	}

	int ilPolySize = imXMinutes/FACTOR;

	bool blShift = false;
	if(olSingleShiftArray.GetSize()>=imXMinutes)
	{
		blShift = true;
	}
	if(pomSingleShiftPoly!=NULL)
		delete pomSingleShiftPoly;

	pomSingleShiftPoly = NULL;
	CPoint *polShiftPoly = new CPoint[(int)(2*ilPolySize +2)];
	imSingleShiftPolySize = 0;

	bool blShiftInit = true;
	int ilActShiftVal = -1;
	int ilLastShiftVal = -1;
	int ilFactor ;

	int ilOneShift = (int)(10000);
	if (imMaxYCount > 0)
		ilOneShift = (int)(100000/(imMaxYCount+1));

	if(blShift)
	{
		polShiftPoly[imSingleShiftPolySize].x = 0;
		polShiftPoly[imSingleShiftPolySize++].y = 0;
		for(int i=0;i<ilPolySize ;i++)
		{
			ilFactor =(int)i*FACTOR;		
			ilActShiftVal = (int)olSingleShiftArray[ilFactor]*ilOneShift;
			if(ilActShiftVal != ilLastShiftVal)
			{
				if(blShiftInit )
				{
					ilLastShiftVal= ilActShiftVal;
					blShiftInit = false;
				}
				polShiftPoly[imSingleShiftPolySize].x = int(imOneMinute*ilFactor);
				polShiftPoly[imSingleShiftPolySize++].y = ilLastShiftVal;
				polShiftPoly[imSingleShiftPolySize].x = int(imOneMinute*ilFactor);
				polShiftPoly[imSingleShiftPolySize++].y = ilActShiftVal;
				ilLastShiftVal = ilActShiftVal;
			}
		}
	}
	if(blShift)
	{ 
		polShiftPoly[imSingleShiftPolySize].x = int(imOneMinute*ilFactor);
		polShiftPoly[imSingleShiftPolySize++].y = ilActShiftVal;
		polShiftPoly[imSingleShiftPolySize].x = int(imOneMinute*ilFactor);
		polShiftPoly[imSingleShiftPolySize++].y = 0;
		
		pomSingleShiftPoly = new CPoint[imSingleShiftPolySize];
	
		for(int i = 0; i< imSingleShiftPolySize; i++)
		{
			pomSingleShiftPoly[i] = polShiftPoly[i];
		}
	
	}
	delete polShiftPoly;

	PrepareSingleFlightPoly();
}

void CCoverageGraphicWnd::PrepareSingleFlightPoly()
{

	int ilSize = omLines.GetSize();

	if(pomSingleFlightPoly!=NULL)
		delete pomSingleFlightPoly;

	pomSingleFlightPoly = NULL;
	imSingleFligtPolySize = 0;
	if(imActuelRow >= 0 && imActuelRow < ilSize)
	{
		int ilOrgSize = omLines[imActuelRow].Values.GetSize();

		int ilPolySize = imXMinutes/FACTOR;

		CPoint *polShiftPoly = new CPoint[(int)(2*ilPolySize +2)];

		bool blShiftInit = true;
		int ilActShiftVal = -1;
		int ilLastShiftVal = -1;
		int ilFactor ;

		int ilOneShift = (int)(10000);
		if (imMaxYCount > 0)
			ilOneShift = (int)(100000/(imMaxYCount+1));

//		polShiftPoly[imSingleFligtPolySize].x = 0;
//		polShiftPoly[imSingleFligtPolySize++].y = 0;
		for(int i=0;i<ilPolySize ;i++)
		{
			ilFactor =(int)i*FACTOR;
			ilActShiftVal = (int)omLines[imActuelRow].Values[i*FACTOR]*ilOneShift;
			if(ilActShiftVal != ilLastShiftVal)
			{
				if(blShiftInit )
				{
					ilLastShiftVal= ilActShiftVal;
					blShiftInit = false;
				}
				polShiftPoly[imSingleFligtPolySize].x = int(imOneMinute*ilFactor);
				polShiftPoly[imSingleFligtPolySize++].y = ilLastShiftVal;
				polShiftPoly[imSingleFligtPolySize].x = int(imOneMinute*ilFactor);
				polShiftPoly[imSingleFligtPolySize++].y = ilActShiftVal;
				ilLastShiftVal = ilActShiftVal;
			}
		}
	
		polShiftPoly[imSingleFligtPolySize].x = int(imOneMinute*ilFactor);
		polShiftPoly[imSingleFligtPolySize++].y = ilActShiftVal;
//		polShiftPoly[imSingleFligtPolySize].x = int(imOneMinute*ilFactor);
//		polShiftPoly[imSingleFligtPolySize++].y = 0;
		
		pomSingleFlightPoly = new CPoint[imSingleFligtPolySize];
		
		for( i = 0; i< imSingleFligtPolySize; i++)
		{
			pomSingleFlightPoly[i] = polShiftPoly[i];
		}
			
		delete polShiftPoly;
	}	

}

void CCoverageGraphicWnd::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	CCS_TRY
	CELLPOS rlPos;
	switch (nChar)
	{
		case VK_UP:		
			{
				if(imActuelRow >= 0)
				{				
					rlPos.Col = 1;
					rlPos.Row = imActuelRow +1;
					
					OnActuellCellMoved(0L, (LPARAM)&rlPos);
				}
				break;
			}
		case VK_DOWN:
			{
				int ilRowCount = pomList->GetRowCount();
				if(imActuelRow < ilRowCount -2)
				{				
					rlPos.Col = 1;
					rlPos.Row = imActuelRow +3;

					OnActuellCellMoved(0L, (LPARAM)&rlPos);
				}
				break;
			}
	}

	CCS_CATCH_ALL
}

void CCoverageGraphicWnd::OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	CCS_TRY
//	GetParent()->SendMessage(WM_KEYUP, nChar, MAKELONG(LOWORD(nRepCnt), LOWORD(nFlags)));
	CCS_CATCH_ALL
}

static int ComparePeak(const Peak **prpPeak1, const Peak **prpPeak2)
{
	if ( (*prpPeak1)->relHeight > (*prpPeak2)->relHeight )
		return 1;
	if ( (*prpPeak1)->relHeight < (*prpPeak2)->relHeight )
		return -1;
	return ((*prpPeak1)->yMax - (*prpPeak2)->yMax );
}

void CCoverageGraphicWnd::CalculateSimSmoothFlightDemands()
{
	CCS_TRY

	CUIntArray *pomSimFlightValues = NULL;

	if (pomViewer->SimulationActive() == true && pomViewer->SimDisplay() == 0)
	{
		pomSimFlightValues = &omSimFlightValues2;
	}
	else if (pomViewer->SimulationActive() == true && pomViewer->SimDisplay() == 1)
	{
		pomSimFlightValues = &omSimFlightValues;
	}
	else
	{
		return;
	}

	int ilSimSize = omFlightValues.GetSize();
	pomSimFlightValues->RemoveAll();
	pomSimFlightValues->InsertAt(0,0,ilSimSize);

	if (this->imActuelRow >= 0 && this->imActuelRow < this->omLines.GetSize())
	{
		CUIntArray olFlightValues;
		olFlightValues.InsertAt(0,0,this->omLines[this->imActuelRow].Values.GetSize());
		for (int ilIndex = 0; ilIndex < this->omLines[this->imActuelRow].Values.GetSize(); ilIndex++)
		{
			olFlightValues[ilIndex] = this->omLines[this->imActuelRow].Values[ilIndex];
		}
		CalculateSimSmoothFlightDemands(olFlightValues,*pomSimFlightValues);
		CalculateSimSmoothFlightDemands(omCompleteFlightValues,omCompleteSimFlightValues2);
	}
	else
	{

		CalculateSimSmoothFlightDemands(omCompleteFlightValues,omCompleteSimFlightValues2);

		int ilComplFlightSize = omCompleteFlightValues.GetSize();
		CTime olLoadTimeStart,olLoadTimeEnd;
		pomViewer->GetTimeFrameFromTo(olLoadTimeStart,olLoadTimeEnd);
		CTimeSpan olTotalOffset = omStartTime - olLoadTimeStart;
		int ilTotalMinOffset = olTotalOffset.GetTotalMinutes();
		for(int ilIndex = 0;ilIndex < ilSimSize ;ilIndex++)
		{
			if (ilIndex + ilTotalMinOffset < ilComplFlightSize)
			{
				(*pomSimFlightValues)[ilIndex] = omCompleteSimFlightValues2[ilIndex + ilTotalMinOffset];
			}
		}
	}
}

void CCoverageGraphicWnd::CalculateSimSmoothFlightDemands(const CUIntArray& ropFlightValues,CUIntArray& ropSimFlightValues)
{
	CCS_TRY

	int ilPeakWidth = 20;
	float flPeakHight = 10.0, flMinFakt;
	int ilLastVal = 0;
	//int ilLastMaxVal = 0;
	int ilIndex, i, j, ilCount, ilMaximum;
	int ilAktVal, ilNextVal, ilWidth, ilLeft, ilRight ;
	bool blSuccess = true;
	int  ilToReduce, ilToEnlarge;
	CCSPtrArray<Peak> olPeaks;
	Peak	rlPeak;
	DWORD llNow, llStart;
	double flDiff;

	CUIntArray	olCompleteFlightValues;
	olCompleteFlightValues.Copy(ropFlightValues);

	llStart = GetTickCount();

	int ilComplFlightSize = ropFlightValues.GetSize();

	bool blUsePercent	= pomViewer->SimHeightType() == SIM_PERCENT;
	int ilMaxIterations = pomViewer->SimIterations();
	int	ilMaxWidth		= pomViewer->SimWidth();
	ilPeakWidth			= pomViewer->SimPeakWidth();
	flPeakHight			= pomViewer->SimPeakHeight();

	memset ( &rlPeak, 0, sizeof(Peak) );

	CTime olLoadTimeStart,olLoadTimeEnd;
	pomViewer->GetTimeFrameFromTo(olLoadTimeStart,olLoadTimeEnd);

	for (int ilIteration = 0; ilIteration < ilMaxIterations; ilIteration++)
	{
		if( (ilPeakWidth>0) && (flPeakHight>0.0) )
		{
			ropSimFlightValues.RemoveAll();
			ropSimFlightValues.InsertAt(0,0,ilComplFlightSize);

			//  find maxima
			CTimeSpan olTotalOffset = omStartTime - olLoadTimeStart;

			int ilTotalMinOffset = olTotalOffset.GetTotalMinutes();
			if(ilTotalMinOffset >=0)
			{
				ilLastVal = olCompleteFlightValues[0];
				for(ilIndex = 1;ilIndex < ilComplFlightSize-2;ilIndex++)
				{
					ilAktVal = olCompleteFlightValues[ilIndex];
					ilNextVal = olCompleteFlightValues[ilIndex+1];
					if ( ( ilAktVal >= ilLastVal ) && ( ilAktVal > ilNextVal ) )
					{
						rlPeak.xStart = 
							rlPeak.xMax = 
							rlPeak.xEnd = ilIndex;
						rlPeak.yMax = ilAktVal;
						olPeaks.New ( rlPeak );
					}
					if ( ilAktVal != ilNextVal )
						ilLastVal = ilAktVal;
				}
				TRACE ( "Found %d Maxima\n", olPeaks.GetSize () );

				//  Calculate peaks
				ilCount = olPeaks.GetSize ();
				for ( i=ilCount-1; i>=0; i-- )
				{
					ilWidth = 0;
					while ( ilWidth <= ilPeakWidth )
					{
						ilLeft = ilRight = -1;
						if ( olPeaks[i].xEnd + 1 < ilComplFlightSize )
							ilRight = olCompleteFlightValues[olPeaks[i].xEnd+1];
						if ( olPeaks[i].xStart - 1 >= 0 )
							ilLeft = olCompleteFlightValues[olPeaks[i].xStart-1];
						if ( ilLeft < 0 ) 
						{	// es geht nur nach rechts
							if ( ( ilRight > olCompleteFlightValues[olPeaks[i].xEnd] ) || ( ilRight<0) )
								break;
							else
								olPeaks[i].xEnd++;
						}
						else 
							if (ilRight <0)
							{	// es geht nur nach links
								if ( ( ilLeft > olCompleteFlightValues[olPeaks[i].xStart] ) || ( ilLeft<0) )
									break;
								else
									olPeaks[i].xStart--;
							}
							else // es geht in beide Richtungen
							{
								if ( ( ilLeft > olCompleteFlightValues[olPeaks[i].xStart] ) &&
									 ( ilRight > olCompleteFlightValues[olPeaks[i].xEnd] ) )
									break;	// keine Verbesserung mehr m�glich
								else
									if ( ilLeft > olCompleteFlightValues[olPeaks[i].xStart] )
										olPeaks[i].xEnd++;	// Verbesserung nach rechts
									else
										if ( ilRight > olCompleteFlightValues[olPeaks[i].xEnd] )
											olPeaks[i].xStart--;
										else
											if ( olCompleteFlightValues[olPeaks[i].xStart] >
												 olCompleteFlightValues[olPeaks[i].xEnd] )
												olPeaks[i].xStart--;
											else
												olPeaks[i].xEnd++;


							}
						ilWidth++;
					}
					TRACE ( "%d. Peak: Start <%d> Max. <%d> at <%d> End <%d>\n", i,
							olPeaks[i].xStart, olPeaks[i].yMax, olPeaks[i].xMax, olPeaks[i].xEnd );
				}
				//  zu niedrige Peak rauswerfen
				ilCount = olPeaks.GetSize ();

				if (blUsePercent)
					flMinFakt = (100.0 + flPeakHight)/100.0;
				else
					flMinFakt = flPeakHight;
				
				for ( i=ilCount-1; i>=0; i-- )
				{
					ilLeft = olCompleteFlightValues[olPeaks[i].xStart];
					ilRight = olCompleteFlightValues[olPeaks[i].xEnd];
					ilLastVal = max (ilLeft,ilRight );
					ilAktVal = olPeaks[i].yMax;
					if (blUsePercent)
					{
						if (ilLastVal != 0)
							olPeaks[i].relHeight = (float)ilAktVal / (float)ilLastVal;
						else
							olPeaks[i].relHeight = 0;
					}
					else
					{
						olPeaks[i].relHeight = (float)ilAktVal;
					}

					if ( olPeaks[i].relHeight < flMinFakt )
					{
						TRACE ( "Peak Top <%d> at <%d> Bottom <%d> RelHeight <%.2f> deleted\n", 
								ilAktVal, olPeaks[i].xMax, ilLastVal, olPeaks[i].relHeight );
						olPeaks.DeleteAt(i);
					}
				}
				ilCount = olPeaks.GetSize ();
				TRACE ( "%d Peaks remaining\n", ilCount );
				olPeaks.Sort ( ComparePeak );
				for( ilIndex = 0;ilIndex < ilComplFlightSize;ilIndex++)
				{
					ropSimFlightValues[ilIndex] = olCompleteFlightValues[ilIndex];
				}
				for ( ilIndex = 0; ilIndex<ilCount; ilIndex++ )
				{
					ilMaximum = olPeaks[ilIndex].yMax;
					ilLastVal = min ( olPeaks[ilIndex].xEnd+ilMaxWidth, ilComplFlightSize-1 );
					//ilLastMaxVal = min ( olPeaks[ilIndex].xEnd+ilMaxWidth, ilComplFlightSize-1 );
					blSuccess = true;
					while ( blSuccess && (ilMaximum>0) )
					{
						ilToReduce = 0; // zu reduzierende
						ilToEnlarge = 0; // zu reduzierende
						int ilFillEndIndex= olPeaks[ilIndex].xStart;
						for ( i=olPeaks[ilIndex].xStart; blSuccess && (i<=ilLastVal); i++ )
						{
							if ( ropSimFlightValues[i]>= ilMaximum /*&& i <= olPeaks[ilIndex].xEnd*/)
							{
								ilToReduce ++;
							}
							else
								if ( (ropSimFlightValues[i]+1 < ilMaximum) && (ilToReduce>ilToEnlarge) )
								{		
									int h1 = ilMaximum - ropSimFlightValues[i]-1;
									int h2 = ilToReduce - ilToEnlarge; 
									ilToEnlarge += min (h1,h2);
									ilFillEndIndex = i;
								}
						}

						blSuccess = (ilToReduce<=ilToEnlarge);
						for ( i=olPeaks[ilIndex].xStart; blSuccess && (i<=ilFillEndIndex); i++ )
						{
							if ( ropSimFlightValues[i] >= ilMaximum )
							{	// aktuelles Maximum gefunden
								blSuccess = false;
								for ( j=i+1; !blSuccess && j<=ilLastVal; j ++ )
								{
									if ( ropSimFlightValues[i] > ropSimFlightValues[j]+1 )
									{
										ropSimFlightValues[i]--;
										ropSimFlightValues[j]++;
										ilAktVal = ropSimFlightValues[i];
										ilNextVal = ropSimFlightValues[j];
										blSuccess = true;
									}
								}
							}
						}
						ilMaximum--;
					}

				}

			}
			
		}

		olPeaks.RemoveAll ();

		llNow = GetTickCount();
		flDiff = llNow-llStart;
		TRACE( "CalculateSmoothDemands %.3lf sec\n", flDiff/1000.0 );

		if (ilCount > 0)
		{
			olCompleteFlightValues.RemoveAll();
			olCompleteFlightValues.Copy(ropSimFlightValues);
		}
		else
		{
			break;
		}
	}

	CCS_CATCH_ALL
}

/*******
void CCoverageGraphicWnd::CalculateSimSmoothFlightDemands()
{
	CCS_TRY

	CUIntArray *pomSimFlightValues = NULL;

	if (pomViewer->bmSimulate2)
	{
		pomSimFlightValues = &omSimFlightValues2;
	}
	else if (pomViewer->bmSimulate)
	{
		pomSimFlightValues = &omSimFlightValues;
	}
	else
	{
		return;
	}
	
	int ilPeakWidth = 20;
	float flPeakHight = 10.0, flMinFakt;
	int ilLastVal = 0;
	//int ilLastMaxVal = 0;
	int ilIndex, i, j, ilCount, ilMaximum;
	int ilAktVal, ilNextVal, ilWidth, ilLeft, ilRight ;
	bool blSuccess = true;
	int  ilToReduce, ilToEnlarge;
	CCSPtrArray<Peak> olPeaks;
	Peak	rlPeak;
	DWORD llNow, llStart;
	double flDiff;

	CUIntArray	olCompleteFlightValues;
	olCompleteFlightValues.Copy(omCompleteFlightValues);

	llStart = GetTickCount();

	int ilSimSize = omFlightValues.GetSize();
	int ilComplFlightSize = olCompleteFlightValues.GetSize();

	int ilMaxIterations = pomViewer->imSimIterations;
	int	ilMaxWidth		= pomViewer->imSimWidth;
	ilPeakWidth			= pomViewer->imSimPeakWidth;
	flPeakHight			= pomViewer->imSimPeakHeight;
	
	memset ( &rlPeak, 0, sizeof(Peak) );


	for (int ilIteration = 0; ilIteration < ilMaxIterations; ilIteration++)
	{
		(*pomSimFlightValues).RemoveAll();
		(*pomSimFlightValues).InsertAt(0,0,ilSimSize);

		if( (ilPeakWidth>0) && (flPeakHight>0.0) )
		{
			omCompleteSimFlightValues2.RemoveAll();
			omCompleteSimFlightValues2.InsertAt(0,0,ilComplFlightSize);

			//  find maxima
			CTimeSpan olTotalOffset = omStartTime - pomViewer->omLoadTimeStart;

			int ilTotalMinOffset = olTotalOffset.GetTotalMinutes();
			if(ilTotalMinOffset >=0)
			{
				ilLastVal = olCompleteFlightValues[0];
				for(ilIndex = 1;ilIndex < ilComplFlightSize-2;ilIndex++)
				{
					ilAktVal = olCompleteFlightValues[ilIndex];
					ilNextVal = olCompleteFlightValues[ilIndex+1];
					if ( ( ilAktVal >= ilLastVal ) && ( ilAktVal > ilNextVal ) )
					{
						rlPeak.xStart = 
							rlPeak.xMax = 
							rlPeak.xEnd = ilIndex;
						rlPeak.yMax = ilAktVal;
						olPeaks.New ( rlPeak );
					}
					if ( ilAktVal != ilNextVal )
						ilLastVal = ilAktVal;
				}
				TRACE ( "Found %d Maxima\n", olPeaks.GetSize () );

				//  Calculate peaks
				ilCount = olPeaks.GetSize ();
				for ( i=ilCount-1; i>=0; i-- )
				{
					ilWidth = 0;
					while ( ilWidth <= ilPeakWidth )
					{
						ilLeft = ilRight = -1;
						if ( olPeaks[i].xEnd + 1 < ilComplFlightSize )
							ilRight = olCompleteFlightValues[olPeaks[i].xEnd+1];
						if ( olPeaks[i].xStart - 1 >= 0 )
							ilLeft = olCompleteFlightValues[olPeaks[i].xStart-1];
						if ( ilLeft < 0 ) 
						{	// es geht nur nach rechts
							if ( ( ilRight > olCompleteFlightValues[olPeaks[i].xEnd] ) || ( ilRight<0) )
								break;
							else
								olPeaks[i].xEnd++;
						}
						else 
							if (ilRight <0)
							{	// es geht nur nach links
								if ( ( ilLeft > olCompleteFlightValues[olPeaks[i].xStart] ) || ( ilLeft<0) )
									break;
								else
									olPeaks[i].xStart--;
							}
							else // es geht in beide Richtungen
							{
								if ( ( ilLeft > olCompleteFlightValues[olPeaks[i].xStart] ) &&
									 ( ilRight > olCompleteFlightValues[olPeaks[i].xEnd] ) )
									break;	// keine Verbesserung mehr m�glich
								else
									if ( ilLeft > olCompleteFlightValues[olPeaks[i].xStart] )
										olPeaks[i].xEnd++;	// Verbesserung nach rechts
									else
										if ( ilRight > olCompleteFlightValues[olPeaks[i].xEnd] )
											olPeaks[i].xStart--;
										else
											if ( olCompleteFlightValues[olPeaks[i].xStart] >
												 olCompleteFlightValues[olPeaks[i].xEnd] )
												olPeaks[i].xStart--;
											else
												olPeaks[i].xEnd++;


							}
						ilWidth++;
					}
					TRACE ( "%d. Peak: Start <%d> Max. <%d> at <%d> End <%d>\n", i,
							olPeaks[i].xStart, olPeaks[i].yMax, olPeaks[i].xMax, olPeaks[i].xEnd );
				}
				//  zu niedrige Peak rauswerfen
				ilCount = olPeaks.GetSize ();
				flMinFakt = (100.0 + flPeakHight)/100.0;
				for ( i=ilCount-1; i>=0; i-- )
				{
					ilLeft = olCompleteFlightValues[olPeaks[i].xStart];
					ilRight = olCompleteFlightValues[olPeaks[i].xEnd];
					ilLastVal = max (ilLeft,ilRight );
					ilAktVal = olPeaks[i].yMax;
					olPeaks[i].relHeight = (float)ilAktVal / (float)ilLastVal;
					if ( olPeaks[i].relHeight < flMinFakt )
					{
						TRACE ( "Peak Top <%d> at <%d> Bottom <%d> RelHeight <%.2f> deleted\n", 
								ilAktVal, olPeaks[i].xMax, ilLastVal, olPeaks[i].relHeight );
						olPeaks.DeleteAt(i);
					}
				}
				ilCount = olPeaks.GetSize ();
				TRACE ( "%d Peaks remaining\n", ilCount );
				olPeaks.Sort ( ComparePeak );
				for( ilIndex = 0;ilIndex < ilComplFlightSize;ilIndex++)
				{
					omCompleteSimFlightValues2[ilIndex] = olCompleteFlightValues[ilIndex];
				}
				for ( ilIndex = 0; ilIndex<ilCount; ilIndex++ )
				{
					ilMaximum = olPeaks[ilIndex].yMax;
					ilLastVal = min ( olPeaks[ilIndex].xEnd+ilMaxWidth, ilComplFlightSize-1 );
					//ilLastMaxVal = min ( olPeaks[ilIndex].xEnd+ilMaxWidth, ilComplFlightSize-1 );
					blSuccess = true;
					while ( blSuccess && (ilMaximum>0) )
					{
						ilToReduce = 0; // zu reduzierende
						ilToEnlarge = 0; // zu reduzierende
						for ( i=olPeaks[ilIndex].xStart; blSuccess && (i<=ilLastVal); i++ )
						{
							if ( omCompleteSimFlightValues2[i]>= ilMaximum )
								ilToReduce ++;
							else
								if ( omCompleteSimFlightValues2[i]+1 < ilMaximum )
									ilToEnlarge ++;
						}

						blSuccess = (ilToReduce<=ilToEnlarge);
						for ( i=olPeaks[ilIndex].xStart; blSuccess && (i<=ilLastVal); i++ )
						{
							if ( omCompleteSimFlightValues2[i] >= ilMaximum )
							{	// aktuelles Maximum gefunden
								blSuccess = false;
								for ( j=i+1; !blSuccess && j<=ilLastVal; j ++ )
								{
									if ( omCompleteSimFlightValues2[i] > omCompleteSimFlightValues2[j]+1 )
									{
										omCompleteSimFlightValues2[i]--;
										omCompleteSimFlightValues2[j]++;
										ilAktVal = omCompleteSimFlightValues2[i];
										ilNextVal = omCompleteSimFlightValues2[j];
										blSuccess = true;
									}
								}
							}
						}
						ilMaximum--;
					}

				}
				for( ilIndex = 0;ilIndex < ilSimSize ;ilIndex++)
				{
					if(ilIndex + ilTotalMinOffset < ilComplFlightSize)
					{
						(*pomSimFlightValues)[ilIndex] = omCompleteSimFlightValues2[ilIndex + ilTotalMinOffset];
					}
				}

			}
			
		}

		olPeaks.RemoveAll ();

		llNow = GetTickCount();
		flDiff = llNow-llStart;
		TRACE( "CalculateSmoothDemands %.3lf sec\n", flDiff/1000.0 );

		if (ilCount > 0)
		{
			olCompleteFlightValues.RemoveAll();
			olCompleteFlightValues.Copy(omCompleteSimFlightValues2);
		}
		else
		{
			break;
		}
	}

	CCS_CATCH_ALL
}
*****/

double CCoverageGraphicWnd::CalculateTotalWorkingHours(CTime opAssignStart,CTime opAssignEnd)
{
	CTime olLoadTimeStart,olLoadTimeEnd;
	pomViewer->GetTimeFrameFromTo(olLoadTimeStart,olLoadTimeEnd);
	CTimeSpan olOffset	= opAssignStart - olLoadTimeStart;
	int ilOffset		= olOffset.GetTotalMinutes();

	CTimeSpan olDuration= opAssignEnd - opAssignStart;
	int ilDuration		= olDuration.GetTotalMinutes();

	int	ilComplete		= omCompleteFlightValues.GetSize();

	double dlWorkingHours = 0;
	for (int i = ilOffset; i < ilOffset + ilDuration; i++)
	{
		if (i < ilComplete)
		{
			if (pomViewer->SimulationActive() == true && pomViewer->SimDisplay() == 0)
			{
				dlWorkingHours += omCompleteSimFlightValues2.GetAt(i);
				dlWorkingHours -= omCompleteShiftArray.GetAt(i);
			}
			else
			{
				dlWorkingHours += omCompleteFlightValues.GetAt(i);
				dlWorkingHours -= omCompleteShiftArray.GetAt(i);
			}
		}
	}

	return dlWorkingHours;
}

void CCoverageGraphicWnd::SetColorSetup()
{
	// delete current colors and pens
	omColors.DeleteAll();

	for (int i = 0; i < NUMCONFCOLORS; i++)
	{
		omPenList[i].DeleteObject();
		omPrintPenList[i].DeleteObject();
		omBrushList[i].DeleteObject();
		omPrintBrushList[i].DeleteObject();
	}

	ogBasicData.GetColorConfData(omColors);

	imColorCount = omColors.GetSize() - 6;
	for (i = 0; i < omColors.GetSize(); i++)
	{
		if (i == 10)
		{
			omPrintPenList[i].CreatePen(omColors[i].Type, 100, omColors[i].Color);
			omPenList[i].CreatePen(omColors[i].Type, 5, omColors[i].Color);
		}
		else
		{
			omPenList[i].CreatePen(omColors[i].Type, 3, omColors[i].Color);
			omPrintPenList[i].CreatePen(omColors[i].Type, 100, omColors[i].Color);
		}

		omBrushList[i].CreateSolidBrush(omColors[i].Color);
		omPrintBrushList[i].CreateSolidBrush(omColors[i].Color);

	}
}

void CCoverageGraphicWnd::OnResultTable(CResultTable *popResultTable)
{
	if (popResultTable == NULL)
		return;

	popResultTable->ClearLines();
	popResultTable->SetOutHeader(this->omFilterType);
	popResultTable->ShowWindow(SW_SHOW);

//	check, if there is "-*-" and "*All" only
	int ilRowSize = this->pomList->GetRowCount();
	if ((this->imActuelRow == -1 || this->omLines[this->imActuelRow].Lkco == LoadStg(IDS_STRING61216)) || ilRowSize == 2)
	{
		CUIntArray olAdditCount;
		CUIntArray olRealAdditCount;
		for(int i = 0; i < this->omFlightValues.GetSize(); i++)
		{
			olAdditCount.Add(0);
		}

		if (this->omLines.GetSize() > 0)
		{
			for(i = 0; i < this->omLines.GetSize(); i++)
			{
				for(int j = 0; (j < this->imXMinutes && j < this->omLines[i].Values.GetSize()); j++)
				{
					olAdditCount[j] += this->omLines[i].Values[j];
				}
			}
		}
		else
		{
			for(i = 0; i < this->omFlightValues.GetSize() ; i++)
			{
				if (i < this->omFlightValues.GetSize())
					olAdditCount[i] = this->omFlightValues[i];
			}
		}

		for (i = 0; i < this->omShiftArray.GetSize() ; i++)
		{
			olRealAdditCount.Add(0);
		}

		for(i = 0; i < this->omShiftArray.GetSize() ; i++)
		{
			olRealAdditCount[i] = this->omShiftArray[i];
		}

		int ilActual = olAdditCount.GetSize();
		int ilPlanned = olRealAdditCount.GetSize();
		CTime olStartTime = this->omStartTime;

		for(int ilCnt = 0;ilCnt < ilActual || ilCnt < ilPlanned;ilCnt++)
		{
			int ilDura=0,ilDura2=0;
			CString olStart = olStartTime.Format("%y.%m.%d - %H:%M");
			if (ilCnt < ilActual)
			{
				ilDura = olAdditCount[ilCnt];
			}
			if (ilCnt < ilPlanned)
			{
				ilDura2 = olRealAdditCount[ilCnt];
			}
			popResultTable->AddResults(olStart.GetBuffer(0),ilDura,ilDura2,this->omTypeList);
			olStartTime += CTimeSpan(0,0,1,0);
		}

		popResultTable->UpdateResults(COV_TAB);
	}
	else	// Fct specific behavior + "-*-" behavior 
	{
		CUIntArray olAdditCount;
		CUIntArray olRealAdditCount;
		for(int i = 0; i < this->omFlightValues.GetSize(); i++)
		{
			olAdditCount.Add(0);
		}

		for(int j = 0; (j < this->imXMinutes && j < this->omLines[this->imActuelRow].Values.GetSize()); j++)
		{
			olAdditCount[j] += this->omLines[this->imActuelRow].Values[j];
		}

		for (i = 0; i < this->omShiftArray.GetSize() ; i++)
		{
			olRealAdditCount.Add(0);
		}

		for(j = 0; (j < this->imXMinutes && j < this->omShiftLines[this->imActuelRow].Values.GetSize()); j++)
		{
			olRealAdditCount[j] += this->omShiftLines[this->imActuelRow].Values[j];
		}

		int ilActual = olAdditCount.GetSize();
		int ilPlanned = olRealAdditCount.GetSize();
		CTime olStartTime = this->omStartTime;

		for(int ilCnt = 0;ilCnt < ilActual || ilCnt < ilPlanned;ilCnt++)
		{
			int ilDura=0,ilDura2=0;
			CString olStart = olStartTime.Format("%y.%m.%d - %H:%M");
			if (ilCnt < ilActual)
			{
				ilDura = olAdditCount[ilCnt];
			}
			if (ilCnt < ilPlanned)
			{
				ilDura2 = olRealAdditCount[ilCnt];
			}

			popResultTable->AddResults(olStart.GetBuffer(0),ilDura,ilDura2,this->omLines[this->imActuelRow].Lkco);

			olStartTime += CTimeSpan(0,0,1,0);
		}


		olAdditCount.RemoveAll();
		olRealAdditCount.RemoveAll();

		for(i = 0; i < this->omFlightValues.GetSize(); i++)
		{
			olAdditCount.Add(0);
		}

		if (this->omLines.GetSize() > 0)
		{
			for(i = 0; i < this->omLines.GetSize(); i++)
			{
				for(int j = 0; (j < this->imXMinutes && j < this->omLines[i].Values.GetSize()); j++)
				{
					olAdditCount[j] += this->omLines[i].Values[j];
				}
			}
		}
		else
		{
			for(i = 0; i < this->omFlightValues.GetSize() ; i++)
			{
				if (i < this->omFlightValues.GetSize())
					olAdditCount[i] = this->omFlightValues[i];
			}
		}

		for (i = 0; i < this->omShiftArray.GetSize() ; i++)
		{
			olRealAdditCount.Add(0);
		}

		for(i = 0; i < this->omShiftArray.GetSize() ; i++)
		{
			olRealAdditCount[i] = this->omShiftArray[i];
		}

		ilActual = olAdditCount.GetSize();
		ilPlanned = olRealAdditCount.GetSize();
		olStartTime = this->omStartTime;

		for(ilCnt = 0;ilCnt < ilActual || ilCnt < ilPlanned;ilCnt++)
		{
			int ilDura=0,ilDura2=0;
			CString olStart = olStartTime.Format("%y.%m.%d - %H:%M");
			if (ilCnt < ilActual)
			{
				ilDura = olAdditCount[ilCnt];
			}
			if (ilCnt < ilPlanned)
			{
				ilDura2 = olRealAdditCount[ilCnt];
			}
			popResultTable->AddResults(olStart.GetBuffer(0),ilDura,ilDura2,this->omTypeList,true);
			olStartTime += CTimeSpan(0,0,1,0);
		}


		popResultTable->UpdateResults(COV_TAB,true);

	}
}

int	 CCoverageGraphicWnd::CalculateMaxFlightDemandPeak()
{
	int ilPeak = 0;
	for (int i = 0; i < this->omCompleteFlightValues.GetSize(); i++)
	{
		if (this->omCompleteFlightValues[i] > ilPeak)
			ilPeak = this->omCompleteFlightValues[i];
	}

	return ilPeak;
}

int	 CCoverageGraphicWnd::CalculateMinFlightDemandCut()
{
	return CalculateMinFlightDemandCut(this->omCompleteFlightValues);
}

int	 CCoverageGraphicWnd::CalculateMinFlightDemandCut(const CUIntArray& ropCompleteFlightValues)
{
	int ilPeakLevel = 0;
	int ilArea = 0;
	for (int i = 0; i < ropCompleteFlightValues.GetSize(); i++)
	{
		if (this->omCompleteFlightValues[i] > ilPeakLevel)
			ilPeakLevel = ropCompleteFlightValues[i];

		ilArea += ropCompleteFlightValues[i];
	}

	int ilMinLevel = ilArea / ropCompleteFlightValues.GetSize();

	int ilSavLevel = pomViewer->SimHeight();
	CUIntArray olCompleteSimFlightValues;

	for (int ilLevel = ilMinLevel; ilLevel < ilPeakLevel;ilLevel++)
	{
		pomViewer->SimHeight(ilLevel);
		CalculateSimCutFlightDemands(ropCompleteFlightValues,olCompleteSimFlightValues);
		if (olCompleteSimFlightValues[ropCompleteFlightValues.GetSize()-1] <= ilLevel)
			break;
	}

	pomViewer->SimHeight(ilSavLevel);
	return ilLevel;
}
