// CedaValData.cpp - Class for Airline Types
//

#include <stdafx.h>
#include <ccsglobl.h>
#include <CCSPtrArray.h>
#include <CCSCedaData.h>
#include <ccsddx.h>
#include <CCSBcHandle.h>
#include <ccsddx.h>
#include <CedaValData.h>
#include <BasicData.h>
#include <CedaBasicData.h>

// General macros for testing a point against an interval
#define IsBetween(val, start, end)  ((start) <= (val) && (val) <= (end))
#define IsOverlapped(start1, end1, start2, end2)    ((start1) <= (end2) && (start2) <= (end1))
#define IsReallyOverlapped(start1, end1, start2, end2)	((start1) < (end2) && (start2) < (end1))
#define IsWithIn(start1, end1, start2, end2)	((start1) >= (start2) && (end1) <= (end2))

static int ByStartTime(const VALIDTIMES **pppValTime1, const VALIDTIMES **pppValTime2);
static int ByStartTime(const VALIDTIMES **pppValTime1, const VALIDTIMES **pppValTime2)
{
	return (int)((**pppValTime1).Tifr.GetTime() - (**pppValTime2).Tito.GetTime());
}

void  ProcessValCf(void * popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);


CedaValData::CedaValData()
{                  
    BEGIN_CEDARECINFO(VALDATA, ValDataRecInfo)
		FIELD_LONG(Urno,"URNO")
		FIELD_CHAR_TRIM(Tabn,"TABN")
		FIELD_CHAR_TRIM(Freq,"FREQ")
		FIELD_LONG(Uval,"UVAL")
        FIELD_DATE(Vafr,"VAFR")
        FIELD_DATE(Vato,"VATO")
        FIELD_CHAR_TRIM(Tifr,"TIMF")
        FIELD_CHAR_TRIM(Tito,"TIMT")
    END_CEDARECINFO

    // Copy the record structure
    for (int i = 0; i < sizeof(ValDataRecInfo)/sizeof(ValDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&ValDataRecInfo[i],sizeof(CEDARECINFO));
        omRecInfo.Add(prpCedaRecInfo);
	}

	ogDdx.Register((void *)this,BC_VAL_CHANGE,CString("VALDATA"), CString("Val changed"),ProcessValCf);
	ogDdx.Register((void *)this,BC_VAL_DELETE,CString("VALDATA"), CString("Val deleted"),ProcessValCf);

    // Initialize table names and field names
    strcpy(pcmTableName,"VALTAB");
    pcmFieldList = "URNO,TABN,FREQ,UVAL,VAFR,VATO,TIMF,TIMT";
}
 
CedaValData::~CedaValData()
{
	TRACE("CedaValData::~CedaValData called\n");
	ogDdx.UnRegister(this,NOTUSED);
	ClearAll();
}

void  ProcessValCf(void * popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	BC_TRY
		((CedaValData *)popInstance)->ProcessValBc(ipDDXType,vpDataPointer,ropInstanceName);
	BC_CATCH_ALL
}

void CedaValData::ProcessValBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlValData = (struct BcStruct *) vpDataPointer;

	long llUrno = GetUrnoFromSelection(prlValData->Selection);
	if(llUrno == 0L && ipDDXType == BC_VAL_CHANGE)
	{
		VALDATA rlVal;
		GetRecordFromItemList(&omRecInfo,&rlVal,prlValData->Fields,prlValData->Data);
		llUrno = rlVal.Urno;
	}
	
	VALDATA *prlVal = GetValByUrno(llUrno);
	VALDATA rlOriginalVal;
	if(ipDDXType == BC_VAL_CHANGE && prlVal != NULL)
	{
		// update
		GetRecordFromItemList(prlVal,prlValData->Fields,prlValData->Data);
		ogDdx.DataChanged((void *)this,VAL_CHANGE,(void *)prlVal);
	}
	else if(ipDDXType == BC_VAL_CHANGE && prlVal == NULL)
	{
		// insert
		VALDATA rlVal;
		GetRecordFromItemList(&rlVal,prlValData->Fields,prlValData->Data);
		prlVal = AddValInternal(rlVal);
		ogDdx.DataChanged((void *)this,VAL_CHANGE,(void *)prlVal);
	}
	else if(ipDDXType == BC_VAL_DELETE && prlVal != NULL)
	{
		// delete
		rlOriginalVal = *prlVal;
		DeleteValInternal(prlVal);
		ogDdx.DataChanged((void *)this,VAL_DELETE,(void *)rlOriginalVal.Urno);
	}
}

void CedaValData::ClearAll()
{
	omUrnoMap.RemoveAll();
	POSITION pos = omTableMap.GetStartPosition();
	while(pos)
	{
		CString olKey;
		CMapPtrToPtr *polTableMap = NULL;
		omTableMap.GetNextAssoc(pos,olKey,(void *&)polTableMap);
		if (polTableMap)
		{
			POSITION pos2 = polTableMap->GetStartPosition();
			while(pos2)
			{
				void *polKey;
				CCSPtrArray<VALDATA> *polValid = NULL;
				polTableMap->GetNextAssoc(pos2,polKey,(void *&)polValid);
				delete polValid;
			}
			polTableMap->RemoveAll();
			delete polTableMap;
		}
	}
	omTableMap.RemoveAll();
	omData.DeleteAll();
}

BOOL CedaValData::ReadValData(const CTime& ropStartTime,const CTime& ropEndTime)
{
	BOOL ilRc = TRUE;
	ClearAll();

	char pclCom[10] = "RT";
    char pclWhere[512];

	sprintf(pclWhere, "WHERE VAFR <= '%s'",ropEndTime.Format("%Y%m%d%H%M%S"));
	//MWO: 27.03.03
	ogBcHandle.SetFilterRange("VALTAB", "VAFR", "VATO", ropStartTime.Format("%Y%m%d%H%M%S"), ropEndTime.Format("%Y%m%d%H%M%S"));
	//END MWO: 27.03.03

    if((ilRc = CedaAction(pclCom, pclWhere)) == TRUE)
	{
		VALDATA rlValData;
		for (int ilLc = 0; ilRc == TRUE; ilLc++)
		{
			if ((ilRc = GetBufferRecord(ilLc,&rlValData)) == TRUE)
			{
				if (rlValData.Vato == TIMENULL || ropStartTime <= rlValData.Vato)
					AddValInternal(rlValData);
			}
		}
	}

    return ilRc;
}


VALDATA *CedaValData::AddValInternal(VALDATA &rrpVal)
{
	ConvertDatesToLocal(rrpVal);

	VALDATA *prlVal = new VALDATA;
	*prlVal = rrpVal;
	if (strlen(prlVal->Freq) == 0)
	{
		strcpy(prlVal->Freq,"1111111");
	}

	omData.Add(prlVal);
	omUrnoMap.SetAt((void *)prlVal->Urno,prlVal);

	CMapPtrToPtr *polTableMap = NULL;
	if (!omTableMap.Lookup(prlVal->Tabn,(void *&)polTableMap) || !polTableMap)
	{
		polTableMap = new CMapPtrToPtr;
		omTableMap.SetAt(prlVal->Tabn,polTableMap);
	}

	if (polTableMap)
	{
		CCSPtrArray<VALDATA> *polValid = NULL;
		if (!polTableMap->Lookup((void *)prlVal->Uval,(void *&)polValid) || !polValid)
		{
			polValid = new CCSPtrArray<VALDATA>;
			polTableMap->SetAt((void *)prlVal->Uval,polValid);
		}

		if (polValid)
		{
			polValid->Add(prlVal);
		}
	}

	return prlVal;
}

void CedaValData::DeleteValInternal(VALDATA *prpVal)
{
	CMapPtrToPtr *polTableMap;
	if(omTableMap.Lookup(prpVal->Tabn,(void *& )polTableMap))
	{
		CCSPtrArray <VALDATA> *polValid = NULL;
		if (polTableMap->Lookup((void *)prpVal->Uval,(void *&)polValid) && polValid)
		{
			int ilNumRecs = polValid->GetSize();
			for(int ilR = (ilNumRecs-1); ilR >= 0; ilR--)
			{
				VALDATA *prlVal = &((*polValid)[ilR]);
				if(prlVal->Urno == prpVal->Urno)
				{
					polValid->RemoveAt(ilR);
				}
			}
		}
	}

	long llUrno = prpVal->Urno;
	int ilCount = omData.GetSize();
	for (int ilLc = ilCount-1; ilLc >= 0; ilLc--)
	{
		if (omData[ilLc].Urno == llUrno)
		{
			omUrnoMap.RemoveKey((void *)llUrno);
			omData.DeleteAt(ilLc);
		}
	}
}

VALDATA* CedaValData::GetValByUrno(long lpUrno)
{
	VALDATA *prlVal = NULL;
	omUrnoMap.Lookup((void *)lpUrno, (void *&) prlVal);
	return prlVal;
}

int		CedaValData::GetValid(const CString& ropTable,long lpUrno,CCSPtrArray<VALDATA>& ropValid)
{
	CMapPtrToPtr *polTableMap = NULL;
	if (omTableMap.Lookup(ropTable,(void *&)polTableMap) && polTableMap)
	{
		CCSPtrArray<VALDATA> *polValid = NULL;
		if (polTableMap->Lookup((void *)lpUrno,(void *&)polValid) && polValid)
		{
			ropValid = *polValid;
			return ropValid.GetSize();
		}
		else
			return 0;
	}
	else
		return 0;
}

CString CedaValData::GetTableName(void)
{
	return CString(pcmTableName);
}

void CedaValData::ConvertDatesToLocal(VALDATA &rrpVal)
{
	ogBasicData.UtcToLocal(rrpVal.Vafr);
	ogBasicData.UtcToLocal(rrpVal.Vato);
}

int	CedaValData::GetValidTimes(VALDATA *prpVal,const CTime& ropStartTime,const CTime& ropEndTime,CCSPtrArray<VALIDTIMES>& ropValidTimes)
{
	if (!prpVal)
		return 0;

	CTime olTimeframeStart,olTimeframeEnd;
	ogBasicData.GetDiagramStartTime(olTimeframeStart,olTimeframeEnd);

	CString	olFreq(prpVal->Freq);

	CTime olStartTime = ropStartTime;		
	if (prpVal->Vafr > olStartTime)
		olStartTime = prpVal->Vafr;
				
	CTime olEndTime   = ropEndTime;		
	if (prpVal->Vato != TIMENULL && prpVal->Vato < olEndTime)
		olEndTime = prpVal->Vato;

	// convert Tifr and Tito from UTC to local
	CString olLocalTifr, olLocalTito;
	CTime olNow = CTime::GetCurrentTime(); // any day is OK
	if(strlen(prpVal->Tifr) > 0)
	{
		CTime olTmpTifr = HourStringToDate(CString(prpVal->Tifr),olNow);
		ogBasicData.UtcToLocal(olTmpTifr);
		olLocalTifr = olTmpTifr.Format("%H%M");
	}
	if(strlen(prpVal->Tito) > 0)
	{
		CTime olTmpTito = HourStringToDate(CString(prpVal->Tito),olNow);
		ogBasicData.UtcToLocal(olTmpTito);
		olLocalTito = olTmpTito.Format("%H%M");
	}


	olStartTime = CTime(olStartTime.GetYear(),olStartTime.GetMonth(),olStartTime.GetDay(),0,0,0);
	olEndTime   = CTime(olEndTime.GetYear(),olEndTime.GetMonth(),olEndTime.GetDay(),23,59,59);
	for (CTime olCurrent = olStartTime; olCurrent <= olEndTime; olCurrent += CTimeSpan(1,0,0,0))
	{
		int ilDayOfWeek = olCurrent.GetDayOfWeek() - 1;

		if (olFreq[ilDayOfWeek] == '1')
		{
			// if Tifr is empty or before Vafr then use Vafr
			CTime olTifr = prpVal->Vafr;
			if (!olLocalTifr.IsEmpty())
			{
				CTime olTmpTifr = HourStringToDate(olLocalTifr,olCurrent);
				if (olTmpTifr > olTifr)
				{
					olTifr = olTmpTifr;
				}
			}

			// if Tito is empty or after Vato the use Vato
			CTime olTito = (prpVal->Vato != TIMENULL) ? prpVal->Vato : olTimeframeEnd;
			if (!olLocalTito.IsEmpty())
			{
				CTime olTmpTito = HourStringToDate(olLocalTito,olCurrent);
				if (olTmpTito < olTifr)
				{
					// if timeTo is before timeFrom then add one day to timeTo - ie the blocking time is before and after midnight
					olTmpTito = HourStringToDate(olLocalTito,olCurrent+CTimeSpan(1,0,0,0));
				}

				if(olTmpTito < olTito)
				{
					olTito = olTmpTito;
				}
			}

			// make sure that the bar times are within the specified timeframe
			if(olTifr < ropStartTime)
				olTifr = ropStartTime;

			if(olTito > ropEndTime)
				olTito = ropEndTime;

			// check if we can merge this bar with existing overlapping bars (they are already ordered by time)
			bool blNotMerged = true;
			int ilNumBarsSoFar = ropValidTimes.GetSize();
			for(int ilB = 0; ilB < ilNumBarsSoFar; ilB++)
			{
				VALIDTIMES *prlBar = &ropValidTimes[ilB];
				if(IsOverlapped(prlBar->Tifr, prlBar->Tito, olTifr, olTito))
				{
					if(olTifr < prlBar->Tifr)
						prlBar->Tifr = olTifr;
					if(olTito > prlBar->Tito)
						prlBar->Tito = olTito;
					blNotMerged = false;
				}
			}

			if(blNotMerged && olTifr < olTito)
				ropValidTimes.New(VALIDTIMES(olTifr,olTito));
		}
	}
	
	return ropValidTimes.GetSize();
}

BOOL CedaValData::IsValidAt(VALDATA *prpVal,const CTime& ropStart,const CTime& ropEnd)
{
	BOOL blOverlapped = FALSE;

	if (!prpVal)
		return blOverlapped;

	CCSPtrArray<VALIDTIMES> olValidTimes;
	if (GetValidTimes(prpVal,ropStart,ropEnd,olValidTimes))	
	{
		for (int ilC = 0; ilC < olValidTimes.GetSize(); ilC++)
		{
			if (IsReallyOverlapped(olValidTimes[ilC].Tifr,olValidTimes[ilC].Tito,ropStart,ropEnd))
			{
				blOverlapped = TRUE;
				break;
			}
		}

		olValidTimes.DeleteAll();
	}

	return blOverlapped;
}

// return true if the object is completely blocked within the period opFrom/opTo
bool CedaValData::IsCompletelyValid(const CString& ropTable, long lpUrno, CTime opFrom, CTime opTo)
{
	bool blIsCompletelyValid = false;

	int ilNumValidTimes = 0, ilBT = 0;

	CCSPtrArray<VALIDTIMES> olValidTimes;

	CCSPtrArray <VALDATA> olValid;
	if(ogValData.GetValid(ropTable, lpUrno, olValid) > 0)
	{
		int ilNumValid = olValid.GetSize();
		for (int ilC = 0; ilC < ilNumValid; ilC ++)
		{
			VALDATA *prlVal = &olValid[ilC];
			CCSPtrArray <VALIDTIMES> olTmpValidTimes;
			if(ogValData.GetValidTimes(prlVal,opFrom,opTo,olTmpValidTimes) > 0)
			{
				ilNumValidTimes = olTmpValidTimes.GetSize();
				for(ilBT = 0; ilBT < ilNumValidTimes; ilBT++)
				{
					olValidTimes.Add(&olTmpValidTimes[ilBT]);
				}
			}
		}
	}

	olValidTimes.Sort(ByStartTime);
	ilNumValidTimes = olValidTimes.GetSize();
	CTime olStart = TIMENULL, olEnd = TIMENULL;

	for(ilBT = 0; ilBT < ilNumValidTimes; ilBT++)
	{
		VALIDTIMES *prlBt = &olValidTimes[ilBT];
		if(olStart == TIMENULL || olEnd == TIMENULL)
		{
			olStart = prlBt->Tifr;
			olEnd = prlBt->Tito;
		}
		else if(IsOverlapped(olStart, olEnd, prlBt->Tifr, prlBt->Tito))
		{
			if(prlBt->Tifr < olStart)
			{
				olStart = prlBt->Tifr;
			}
			if(prlBt->Tito > olEnd)
			{
				olEnd = prlBt->Tito;
			}
		}
	}

	if(IsWithIn(opFrom, opTo, olStart, olEnd))
	{
		blIsCompletelyValid = true;
	}

	return blIsCompletelyValid;
}

int	CedaValData::GetValidTimes(const CString& ropTable,long lpUrno,const CTime& ropStart,const CTime& ropEnd,CCSPtrArray<VALIDTIMES>& ropValidTimes)
{
	CCSPtrArray<VALDATA> olValid;
	
	int ilc = GetValid(ropTable,lpUrno,olValid);
	if (ilc == 0 && ropTable != "EQU")
	{
		CString olUrno;
		olUrno.Format("%ld",lpUrno);
		CTime olValidFrom = DBStringToDateTime(ogBCD.GetField(ropTable,"URNO",olUrno,"VAFR"));
		if (olValidFrom != TIMENULL)
		{
			ogBasicData.UtcToLocal(olValidFrom);
			CTime olValidTo	  = DBStringToDateTime(ogBCD.GetField(ropTable,"URNO",olUrno,"VATO"));
			if (olValidTo == TIMENULL)
			{
				olValidTo = ropEnd;
			}
			else
			{
				ogBasicData.UtcToLocal(olValidTo);
			}

			ropValidTimes.New(VALIDTIMES(olValidFrom,olValidTo));						
		}
	}
	else
	{
		for (int i = 0; i < olValid.GetSize(); i++)
		{
			ilc = GetValidTimes(&olValid[i],ropStart,ropEnd,ropValidTimes);
		}
	}

	return ropValidTimes.GetSize();
}
