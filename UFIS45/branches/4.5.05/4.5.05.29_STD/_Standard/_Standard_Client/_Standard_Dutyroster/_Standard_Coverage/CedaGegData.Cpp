// CedaGegData.cpp
 
#include <stdafx.h>
#include <CedaGegData.h>


void ProcessGegCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//--CEDADATA-----------------------------------------------------------------------------------------------

CedaGegData::CedaGegData() : CCSCedaData(&ogCommHandler)
{
    BEGIN_CEDARECINFO(GEGDATA, GegDataRecInfo)
		CCS_FIELD_DATE		(Cdat,"CDAT","",0)
		CCS_FIELD_CHAR_TRIM	(Gcat,"GCAT","",0)
		CCS_FIELD_CHAR_TRIM	(Gcde,"GCDE","",0)
		CCS_FIELD_CHAR_TRIM	(Gnam,"GNAM","",0)
		CCS_FIELD_CHAR_TRIM	(Gsnm,"GSNM","",0)
		CCS_FIELD_DATE		(Lstu,"LSTU","",0)
		CCS_FIELD_CHAR_TRIM	(Prfl,"PRFL","",0)
		CCS_FIELD_CHAR_TRIM	(Rema,"REMA","",0)
		CCS_FIELD_LONG		(Urno,"URNO","",0)
		CCS_FIELD_CHAR_TRIM	(Usec,"USEC","",0)
		CCS_FIELD_CHAR_TRIM	(Useu,"USEU","",0)
    END_CEDARECINFO

    // Copy the record structure
    for (int i = 0; i < sizeof(GegDataRecInfo)/sizeof(GegDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&GegDataRecInfo[i],sizeof(CEDARECINFO));
        omRecInfo.Add(prpCedaRecInfo);
	}
    // Initialize table names and field names
	strcpy(pcmTableName,"GEG");
	strcat(pcmTableName,pcgTableExt);
    sprintf(pcmListOfFields, "CDAT,GCAT,GCDE,GNAM,GSNM,LSTU,PRFL,REMA,URNO,USEC,USEU");

	pcmFieldList = pcmListOfFields; //because in CedaData pcmFieldList is just a pointer

	omData.RemoveAll();
}

//-------------------------------------------------------------------------------------------------------

//--REGISTER----------------------------------------------------------------------------------------------

void CedaGegData::Register(void)
{
	ogDdx.Register((void *)this,BC_GEG_CHANGE,	CString("GEGDATA"), CString("Geg-changed"),	ProcessGegCf);
	ogDdx.Register((void *)this,BC_GEG_NEW,		CString("GEGDATA"), CString("Geg-new"),		ProcessGegCf);
	ogDdx.Register((void *)this,BC_GEG_DELETE,	CString("GEGDATA"), CString("Geg-deleted"),	ProcessGegCf);
}

//--~CEDADATA----------------------------------------------------------------------------------------------

CedaGegData::~CedaGegData(void)
{
	ClearAll();
	omRecInfo.DeleteAll();
}

//--CLEAR-ALL----------------------------------------------------------------------------------------------

void CedaGegData::ClearAll(bool bpWithRegistration)
{
	if(bpWithRegistration)
	{
		ogDdx.UnRegister(this,NOTUSED);
	}

    omUrnoMap.RemoveAll();
    omData.DeleteAll();
}

//--READ-ALL-----------------------------------------------------------------------------------------------

bool CedaGegData::Read(char *pspWhere /*NULL*/)
{
    // Select data from the database
	bool ilRc = true;
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(pspWhere == NULL)
	{	
		ilRc = CedaAction("RT");
	}
	else
	{
		ilRc = CedaAction("RT", pspWhere);
	}
	if (ilRc != true)
	{
		return ilRc;
	}
	// Load data from CedaData into the dynamic array of record
	for (int ilLc = 0; ilRc == true; ilLc++)
	{
		GEGDATA *prlGeg = new GEGDATA;
		if ((ilRc = GetFirstBufferRecord(prlGeg)) == true)
		{
			omData.Add(prlGeg);//Update omData
			omUrnoMap.SetAt((void *)prlGeg->Urno,prlGeg);
		}
		else
		{
			delete prlGeg;
		}
	}
    return true;

}

//--INSERT-------------------------------------------------------------------------------------------------

bool CedaGegData::Insert(GEGDATA *prpGeg)
{
	prpGeg->IsChanged = DATA_NEW;
	if(Save(prpGeg) == false) return false; //Update Database
	InsertInternal(prpGeg);
    return true;
}

//--INSERT-INTERNAL--------------------------------------------------------------------------------------

bool CedaGegData::InsertInternal(GEGDATA *prpGeg)
{
	ogDdx.DataChanged((void *)this, GEG_NEW,(void *)prpGeg ); //Update Viewer
	omData.Add(prpGeg);//Update omData
	omUrnoMap.SetAt((void *)prpGeg->Urno,prpGeg);
    return true;
}

//--DELETE-------------------------------------------------------------------------------------------------

bool CedaGegData::Delete(long lpUrno)
{
	GEGDATA *prlGeg = GetGegByUrno(lpUrno);
	if (prlGeg != NULL)
	{
		prlGeg->IsChanged = DATA_DELETED;
		if(Save(prlGeg) == false) return false; //Update Database
		DeleteInternal(prlGeg);
	}
    return true;
}

//--DELETE-INTERNAL----------------------------------------------------------------------------------------

bool CedaGegData::DeleteInternal(GEGDATA *prpGeg)
{
	ogDdx.DataChanged((void *)this,GEG_DELETE,(void *)prpGeg); //Update Viewer
	omUrnoMap.RemoveKey((void *)prpGeg->Urno);
	int ilGegCount = omData.GetSize();
	for (int ilLc = 0; ilLc < ilGegCount; ilLc++)
	{
		if (omData[ilLc].Urno == prpGeg->Urno)
		{
			omData.DeleteAt(ilLc);//Update omData
			break;
		}
	}
    return true;
}

//--UPDATE-------------------------------------------------------------------------------------------------

bool CedaGegData::Update(GEGDATA *prpGeg)
{
	if (GetGegByUrno(prpGeg->Urno) != NULL)
	{
		if (prpGeg->IsChanged == DATA_UNCHANGED)
		{
			prpGeg->IsChanged = DATA_CHANGED;
		}
		if(Save(prpGeg) == false) return false; //Update Database
		UpdateInternal(prpGeg);
	}
    return true;
}

//--UPDATE-INTERNAL--------------------------------------------------------------------------------------------

bool CedaGegData::UpdateInternal(GEGDATA *prpGeg)
{
	GEGDATA *prlGeg = GetGegByUrno(prpGeg->Urno);
	if (prlGeg != NULL)
	{
		*prlGeg = *prpGeg; //Update omData
		ogDdx.DataChanged((void *)this,GEG_CHANGE,(void *)prlGeg); //Update Viewer
	}
    return true;
}

//--GET-BY-URNO--------------------------------------------------------------------------------------------

GEGDATA *CedaGegData::GetGegByUrno(long lpUrno)
{
	GEGDATA  *prlGeg;
	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlGeg) == TRUE)
	{
		return prlGeg;
	}
	return NULL;
}

//--READSPECIALDATA-------------------------------------------------------------------------------------

bool CedaGegData::ReadSpecialData(CCSPtrArray<GEGDATA> *popGeg,char *pspWhere,char *pspFieldList,bool ipSYS/*=true*/)
{
	bool ilRc = true;
	char pclFieldList[256] = " ";

	if(strlen(pspFieldList) > 0) 
	{
		strcpy(pclFieldList, pspFieldList);
	}
	if(ipSYS == true) 
	{
		if (CedaAction("SYS",pcmTableName,pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	else
	{
		if (CedaAction("RT",pcmTableName,pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	if(popGeg != NULL)
	{
		for (int ilLc = 0; ilRc == true; ilLc++)
		{
			GEGDATA *prpGeg = new GEGDATA;
			if ((ilRc = GetBufferRecord(ilLc,prpGeg,CString(pclFieldList))) == true)
			{
				popGeg->Add(prpGeg);
			}
			else
			{
				delete prpGeg;
			}
		}
		if(popGeg->GetSize() == 0) return false;
	}
    return true;
}

//--SAVE---------------------------------------------------------------------------------------------------

bool CedaGegData::Save(GEGDATA *prpGeg)
{
	bool ilRc = true;
	CString olListOfData;
	char pclSelection[124];
	char pclData[524];

	if (prpGeg->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}
	switch(prpGeg->IsChanged)
	{
	case DATA_NEW:
		MakeCedaData(&omRecInfo,olListOfData,prpGeg);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("IRT","","",pclData);
		prpGeg->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpGeg->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpGeg);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("URT",pclSelection,"",pclData);
		prpGeg->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_DELETED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpGeg->Urno);
		ilRc = CedaAction("DRT",pclSelection);
		break;
	}
    return ilRc;
}

//--PROCESS-CF---------------------------------------------------------------------------------------------

void  ProcessGegCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	BC_TRY
		ogGegData.ProcessBc(ipDDXType,vpDataPointer,ropInstanceName);
	BC_CATCH_ALL
}

//--PROCESS-BC---------------------------------------------------------------------------------------------

void  CedaGegData::ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlGegData;
	prlGegData = (struct BcStruct *) vpDataPointer;
	GEGDATA *prlGeg;
	if(ipDDXType == BC_GEG_NEW)
	{
		prlGeg = new GEGDATA;
		GetRecordFromItemList(prlGeg,prlGegData->Fields,prlGegData->Data);
		InsertInternal(prlGeg);
	}
	if(ipDDXType == BC_GEG_CHANGE)
	{
		long llUrno = GetUrnoFromSelection(prlGegData->Selection);
		prlGeg = GetGegByUrno(llUrno);
		if(prlGeg != NULL)
		{
			GetRecordFromItemList(prlGeg,prlGegData->Fields,prlGegData->Data);
			UpdateInternal(prlGeg);
		}
	}
	if(ipDDXType == BC_GEG_DELETE)
	{
		long llUrno;
		CString olSelection = (CString)prlGegData->Selection;
		if (olSelection.Find('\'') != -1)
		{
			llUrno = GetUrnoFromSelection(prlGegData->Selection);
		}
		else
		{
			int ilFirst = olSelection.Find("=")+2;
			int ilLast  = olSelection.GetLength();
			llUrno = atol(olSelection.Mid(ilFirst,ilLast-ilFirst));
		}
		prlGeg = GetGegByUrno(llUrno);
		if (prlGeg != NULL)
		{
			DeleteInternal(prlGeg);
		}
	}
}

//---------------------------------------------------------------------------------------------------------
