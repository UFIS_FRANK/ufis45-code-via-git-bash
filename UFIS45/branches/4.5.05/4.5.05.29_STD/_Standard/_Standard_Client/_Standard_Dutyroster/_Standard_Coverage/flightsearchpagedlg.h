// FlightSearchPageDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// FlightSearchPageDlg dialog
#ifndef _FLIGHTSEARCH_PAGE_DLG_
#define _FLIGHTSEARCH_PAGE_DLG_

/////////////////////////////////////////////////////////////////////////////
// Class declaration of FlightSearchPageDlg

//@Man:
//@Memo: FlightSearchPageDlg dialog
/*@Doc:
  No comment on this up to now.
*/
class FlightSearchPageDlg : public CPropertyPage
{
	DECLARE_DYNCREATE(FlightSearchPageDlg)

// Construction
public:
    //@ManMemo: Default constructor
	FlightSearchPageDlg();
    //@ManMemo: Default destructor
	~FlightSearchPageDlg();
	void Attach(CWnd *popParent);
// Dialog Data
	//{{AFX_DATA(FlightSearchPageDlg)
	enum { IDD = IDD_FLIGHT_SEARCH_PAGE };
	CEdit	m_FlightNrCtrl;
	CString	m_Airline;
	CTime	m_Date;
	CString	m_Flightno;
	CString	m_Regn;
	//}}AFX_DATA

	CWnd *pomParent;
	bool bmFirst;

// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(FlightSearchPageDlg)
	public:
	virtual BOOL OnSetActive();
	virtual BOOL OnKillActive();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(FlightSearchPageDlg)
	afx_msg void OnKillfocusFlightnr();
	virtual BOOL OnInitDialog();
	afx_msg void OnSetfocusAirline();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

};


#endif // _FLIGHTSEARCH_PAGE_DLG_
