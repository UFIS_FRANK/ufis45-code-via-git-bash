#if !defined(AFX_OMMONSETUPPAGE_H__84CA2503_A2C7_11D6_820F_00010215BFDE__INCLUDED_)
#define AFX_OMMONSETUPPAGE_H__84CA2503_A2C7_11D6_820F_00010215BFDE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ommonSetupPage.h : header file
//
#include <CCSEdit.h>
#include <CCSButtonCtrl.h>
#include <CedaCfgData.h>

/////////////////////////////////////////////////////////////////////////////
// CommonSetupPage dialog

class CommonSetupPage : public CPropertyPage
{
	DECLARE_DYNCREATE(CommonSetupPage)

// Construction
public:
	CommonSetupPage();
	~CommonSetupPage();
	void SetCaption(const char *pcpCaption);

// Dialog Data
	//{{AFX_DATA(CommonSetupPage)
	enum { IDD = IDD_COMMON_SETUP_PAGE };
	CCSEdit			m_DemandTimer2;
	CCSEdit			m_DemandTimer1;
	int				m_EvaluationSortOrder;
	CCSButtonCtrl	m_NoDemFlightsColor;
	//}}AFX_DATA


// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(CommonSetupPage)
	public:
	virtual void OnOK();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(CommonSetupPage)
	virtual BOOL OnInitDialog();
	afx_msg void OnNameConfig();
	afx_msg void OnNoDemFlightsColor();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
protected:
	void	InitializeUserPreferences(); 
	void	MakeSetupStruct();
	int		ResetDialog();
	void	EvaluateUpdateMode();
	int		MakeParameters();
	void	MakeCedaData();
protected:
	char			pcmCaption[256];
	CString			omMode;
	CString			omParameters1;
	USERSETUPDATA	rmUserSetup;
	COLORREF		cmColor;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_OMMONSETUPPAGE_H__84CA2503_A2C7_11D6_820F_00010215BFDE__INCLUDED_)
