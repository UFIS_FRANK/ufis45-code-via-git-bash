// LoadTimeSpanDlg.cpp : implementation file
//

#include <stdafx.h>
#include <Coverage.h>
#include <BasicData.h>
#include <LoadTimeSpanDlg.h>
#include <CedaBasicData.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CLoadTimeSpanDlg dialog


CLoadTimeSpanDlg::CLoadTimeSpanDlg(CWnd* pParent ,CStringArray &ropValues,int ipAbsFlag)
	: CDialog(CLoadTimeSpanDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CLoadTimeSpanDlg)
	m_AbsTimeVal = -1;
	//}}AFX_DATA_INIT

	omValues.RemoveAll();

	for(int i = 0; i < ropValues.GetSize(); i++)
	{
		omValues.Add(ropValues[i]);
	}

	m_AbsTimeVal = ipAbsFlag;
	omStartTime = TIMENULL;
	omEndTime = TIMENULL;
	CMapStringToPtr olItemMap;
	void *polValue = NULL;
	int  ilRecordCount = ogBCD.GetDataCount("TPL");
	for( i = 0; i < ilRecordCount; i++)
	{
 		 CString olName = ogBCD.GetField("TPL", i, "TNAM");
		 if (!olItemMap.Lookup(olName,polValue))
		 {
			olItemMap.SetAt(olName,NULL);
		 }    
	}

	omPossibleItems.RemoveAll();
	for(POSITION olPos = olItemMap.GetStartPosition(); olPos != NULL;)
	{
		CString olName;
		olItemMap.GetNextAssoc(olPos,olName,polValue);

		if(!olName.IsEmpty())
		{
			bool blTplFound = false;
			CString olTplValues = ogBCD.GetValueList("TPL", "TNAM", olName, "URNO") ;
			for (int illc = 0; illc < ogBasicData.omValidTemplates.GetSize() && !blTplFound; illc++)
			{
				if(olTplValues.Find(ogBasicData.omValidTemplates.GetAt(illc)) >= 0)
				{
					blTplFound = true;
				}
			}
			if(blTplFound)
			{
				omSelectedItems.Add(olName);
			}
			else
			{
				omPossibleItems.Add(olName);
			}

		}
	}

	imHideColStart = 1;
	
	CStringArray olItemList;
	if(omPossibleItems.GetSize() > 0)
	{
		ExtractItemList(omPossibleItems[0], &olItemList, ';');
	}
	else if (omSelectedItems.GetSize() > 0)
	{
		ExtractItemList(omSelectedItems[0], &olItemList, ';');
	}

	
	imHideColStart = olItemList.GetSize() +1;
	imColCount = imHideColStart +2;

	pomPossilbeList = new CGridFenster(this);
	pomSelectedList = new CGridFenster(this);


	blIsInit = false;

}

CLoadTimeSpanDlg::~CLoadTimeSpanDlg()
{	
	omValues.RemoveAll();
	delete pomPossilbeList;
	delete pomSelectedList;

}

void CLoadTimeSpanDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CLoadTimeSpanDlg)
	DDX_Control(pDX, IDC_INSERTLIST, m_InsertList);
	DDX_Control(pDX, IDC_CONTENTLIST, m_ContentList);
	DDX_Control(pDX, IDC_TOTIME, m_ToTime);
	DDX_Control(pDX, IDC_TODATE, m_ToDate);
	DDX_Control(pDX, IDC_RELTO, m_RelTo);
	DDX_Control(pDX, IDC_FROMTIME, m_FromTime);
	DDX_Control(pDX, IDC_RELFROM, m_RelFrom);
	DDX_Control(pDX, IDC_FROMDATE, m_FromDate);
	DDX_Radio(pDX, IDC_ABSTIME, m_AbsTimeVal);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CLoadTimeSpanDlg, CDialog)
	//{{AFX_MSG_MAP(CLoadTimeSpanDlg)
	ON_BN_CLICKED(IDC_ABSTIME, OnAbstime)
	ON_BN_CLICKED(IDC_RELTIME, OnReltime)
	ON_BN_CLICKED(IDC_REMOVE, OnRemove)
	ON_BN_CLICKED(IDC_ADD, OnAdd)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CLoadTimeSpanDlg message handlers
BOOL CLoadTimeSpanDlg::GetData()
{
	omValues.RemoveAll();
	CStringArray olTmpValues;
	if(GetData(olTmpValues) == FALSE)
	{
		return FALSE;
	}
	for(int i = 0; i < olTmpValues.GetSize(); i++)
	{
		CString olTmp = olTmpValues[i];
		omValues.Add(olTmpValues[i]);
	}
	for(i = 0; i < m_ContentList.GetCount(); i++)
	{

		CString olTplName;
		m_ContentList.GetText(i,olTplName);
		CString olTplValues = ogBCD.GetValueList("TPL", "TNAM", olTplName, "URNO") ;
		CStringArray olItemList;
		ExtractItemList(olTplValues, &olItemList, ',');

		for(int ilTmp = 0; ilTmp < olItemList.GetSize(); ilTmp++)
		{
			omTplUrnos.Add(olItemList[ilTmp]);
		}
	}
	return TRUE;
}
BOOL CLoadTimeSpanDlg::GetData(CStringArray &ropValues)
{
	CString olT1, olT2, olT3, olT4;
	CString olResult;
	CTime olDate, olTime;
	m_FromDate.GetWindowText(olT1);// TIFA From || TIFD From; Date
	m_FromTime.GetWindowText(olT2);
	m_ToDate.GetWindowText(olT3);
	m_ToTime.GetWindowText(olT4);
	//Erst mal einen precheck und was leer ist wird 
	//entsprechend gesetzt

	if(olT1.IsEmpty() && olT2.IsEmpty() && olT3.IsEmpty() && olT4.IsEmpty())
	{
		;
	}
	else
	{
		if(olT2.IsEmpty())
		{
			olT2 = CString("00:00");
			m_FromTime.SetWindowText(olT2);
		}
		if(olT4.IsEmpty())
		{
			olT4 = CString("00:00");
			m_ToTime.SetWindowText(olT4);
		}
		if(olT1.IsEmpty() && olT3.IsEmpty())
		{
			olT1 = CTime::GetCurrentTime().Format("%d.%m.%Y");
			olT3 = olT1;
			m_FromDate.SetWindowText(olT1);
			m_ToDate.SetWindowText(olT3);
		}
		if(!olT1.IsEmpty() && olT3.IsEmpty())
		{
			olT3 = olT1;
			m_ToDate.SetWindowText(olT3);
		}
		if(olT1.IsEmpty() && !olT3.IsEmpty())
		{
			olT1 = olT3;
			m_FromDate.SetWindowText(olT1);
		}
	}
	if(olT1 != "" && olT2 != "")
	{
		olDate = DateStringToDate(olT1);
		if(olDate != TIMENULL)
			olT1 = olDate.Format("%Y%m%d");
		else
		{
			CString olText, olAdd;
			olText = LoadStg(IDS_NO_DATE_TIME);
			olAdd = LoadStg(IDS_WARNING);
			MessageBox(olText, olAdd, MB_OK);
			return FALSE;//olT1 = " ";
		}
		if(olDate != TIMENULL)
		{
			olTime = HourStringToDate(olT2, olDate);
			if(olTime != TIMENULL)
				olT1 = olDate.Format("%Y%m%d") + olTime.Format("%H%M%S");
			else
			{
				CString olText, olAdd;
				olText = LoadStg(IDS_NO_DATE_TIME);
				olAdd = LoadStg(IDS_WARNING);
				MessageBox(olText, olAdd, MB_OK);
				return FALSE;//olT1 = " ";
			}
		}
	}
	else
	{
		olT1 = " ";
		olDate = TIMENULL;
	}
	ropValues.Add(olT1);
	olResult += olT1 + CString("|");

    m_ToDate.GetWindowText(olT1);// TIFA To || TIFD To; Date
	m_ToTime.GetWindowText(olT2);
	if(olT1 != "" && olT2 != "")
	{
		olDate = DateStringToDate(olT1);
		if(olDate != TIMENULL)
			olT1 = olDate.Format("%Y%m%d");
		else
		{
			CString olText, olAdd;
			olText = LoadStg(IDS_NO_DATE_TIME);
			olAdd = LoadStg(IDS_WARNING);
			MessageBox(olText, olAdd, MB_OK);
			return FALSE;//olT1 = " ";
		}
		if(olDate != TIMENULL)
		{
			olTime = HourStringToDate(olT2, olDate);
			if(olTime != TIMENULL)
				olT1 = olDate.Format("%Y%m%d") + olTime.Format("%H%M%S");
			else
			{
				CString olText, olAdd;
				olText = LoadStg(IDS_NO_DATE_TIME);
				olAdd = LoadStg(IDS_WARNING);
				MessageBox(olText, olAdd, MB_OK);
				return FALSE;//olT1 = " ";
			}
		}
	}
	else
	{
		olT1 = " ";
		olDate = TIMENULL;
	}
	ropValues.Add(olT1);
	olResult += olT1 + CString("|");
    m_RelFrom.GetWindowText(olT1);// RelHBefore
	if(olT1 == "")
		olT1 = " ";
	ropValues.Add(olT1);
	olResult += olT1 + CString("|");
    m_RelTo.GetWindowText(olT1);// RelHAfter
	if(olT1 == "")
		olT1 = " ";
	ropValues.Add(olT1);
	olResult += olT1 + CString("|");

	return true;
}

void CLoadTimeSpanDlg::SetData()
{
	SetData(omValues);
}

void CLoadTimeSpanDlg::SetData(CStringArray &ropValues)
{
	m_FromDate.SetWindowText("");// TIFA From || TIFD From; Date
    m_FromTime.SetWindowText("");// TIFA From || TIFD From; Time
    m_ToDate.SetWindowText("");// TIFA To || TIFD To; Date
    m_ToTime.SetWindowText("");// TIFA To || TIFD To; Time
    m_RelFrom.SetWindowText("");// RelHBefore
    m_RelTo.SetWindowText("");// RelHAfter
 	bool blFlag = false;
	for(int i = 0; i < ropValues.GetSize(); i++)
	{
		CTime olDate = TIMENULL;
		CString olV = ropValues[i];
		if(!olV.IsEmpty())
			blFlag = true;
		switch(i)
		{
		case 0:
			olDate = DBStringToDateTime(ropValues[i]);
			if(olDate != TIMENULL)
			{
				m_FromDate.SetWindowText(olDate.Format("%d.%m.%Y"));
				m_FromTime.SetWindowText(olDate.Format("%H:%M"));
			}
			else
			{
				m_FromDate.SetWindowText(CString(""));
				m_FromTime.SetWindowText(CString(""));
			}
			break;
		case 1:
			olDate = DBStringToDateTime(ropValues[i]);
			if(olDate != TIMENULL)
			{
				m_ToDate.SetWindowText(olDate.Format("%d.%m.%Y"));
				m_ToTime.SetWindowText(olDate.Format("%H:%M"));
			}
			else
			{
				m_ToDate.SetWindowText(CString(""));
				m_ToTime.SetWindowText(CString(""));
			}
			break;
		case 2:
			if(ropValues[i] != CString(" "))
				m_RelFrom.SetWindowText(ropValues[i]);
			break;
		case 3:
			if(ropValues[i] != CString(" "))
				m_RelTo.SetWindowText(ropValues[i]);
			break;

		}
	}
}

BOOL CLoadTimeSpanDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	CWnd *polWnd = GetDlgItem(IDCANCEL);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING999));
	}
	polWnd = GetDlgItem(IDOK);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING998));
	}
	polWnd = GetDlgItem(IDC_ZEITRAUMVON);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING61281));
	}
	polWnd = GetDlgItem(IDC_VON);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING61265));
	}
	polWnd = GetDlgItem(IDC_BIS);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING61266));
	}
	polWnd = GetDlgItem(IDC_ZEITRAUMREL);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING61282));
	}
	polWnd = GetDlgItem(IDC_STDVOR);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING61283));
	}
	polWnd = GetDlgItem(IDC_SDTNACH);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING61284));
	}

	SetWindowText(LoadStg(IDS_STRING61285));


	SetData();
	
	if(m_AbsTimeVal == 0)
	{
		OnAbstime();
	}
	else
	{
		OnReltime();
	}

	UpdateData(FALSE);


	m_InsertList.ResetContent();
	m_ContentList.ResetContent();
	for (int ilLc = 0; ilLc < omPossibleItems.GetSize(); ilLc++)
	{
		CString olTest = omPossibleItems[ilLc];
		m_InsertList.AddString(omPossibleItems[ilLc]);
	}

	for (ilLc = 0; ilLc < omSelectedItems.GetSize(); ilLc++)
	{
		CString olTest = omSelectedItems[ilLc];
		m_ContentList.AddString(omSelectedItems[ilLc]);
	}


	ilLc = omPossibleItems.GetSize() + 2;
	int ilIndex = -1;

	pomSelectedList->SubclassDlgItem(IDC_SELECTED, this);
	pomPossilbeList->SubclassDlgItem(IDC_POSSIBLE, this);
	pomSelectedList->Initialize();
	pomPossilbeList->Initialize();

	CGXStyle olStyle;

	pomPossilbeList->LockUpdate(TRUE);
	pomPossilbeList->GetParam()->EnableUndo(FALSE);
	pomPossilbeList->GetParam()->EnableTrackColWidth(FALSE);
	pomPossilbeList->GetParam()->EnableTrackRowHeight(FALSE);
	pomPossilbeList->GetParam()->SetNumberedColHeaders(FALSE);

	pomSelectedList->LockUpdate(TRUE);
	pomSelectedList->GetParam()->EnableUndo(FALSE);
	pomSelectedList->GetParam()->EnableTrackColWidth(FALSE);
	pomSelectedList->GetParam()->EnableTrackRowHeight(FALSE);
	pomSelectedList->GetParam()->SetNumberedColHeaders(FALSE);

	
	pomPossilbeList->SetColCount(imColCount);

	for(int illc = 0; illc < imColCount; illc++)
		pomPossilbeList->SetColWidth(0,illc,40);

	pomPossilbeList->SetColWidth(0,1,105);

	pomPossilbeList->SetColWidth(0,0,30);


	pomSelectedList->SetColCount(imColCount);

	for( illc = 0; illc < imColCount; illc++)
		pomSelectedList->SetColWidth(0,illc,40);
	
	pomSelectedList->SetColWidth(0,1,105);

	pomSelectedList->SetColWidth(0,0,30);

	pomPossilbeList->SetRowHeight(0, 0, 12);
		
	pomSelectedList->SetRowHeight(0, 0, 12);
		
		
	olStyle.SetEnabled(FALSE);
	olStyle.SetReadOnly(TRUE);

	pomPossilbeList->LockUpdate(FALSE);
	pomSelectedList->LockUpdate(FALSE);
	

	pomSelectedList->HideCols(imHideColStart, imColCount); 
//	pomSelectedList->SetSortQuery(3, 16); 
//	pomSelectedList->SetSortQuery(9, 15); 
	

	pomPossilbeList->HideCols(imHideColStart, imColCount); 


	CString olText;
	int ilCount = m_InsertList.GetCount() + 1;

	CStringArray olItemList;
	pomPossilbeList->SetRowCount(max(7,ilCount));
	for ( ilLc = 0; ilLc < ilCount-1; ilLc++)
	{
		m_InsertList.GetText(ilLc,olText);
		ExtractItemList(olText, &olItemList, ';');
		for(int ilItem = 0 ; ilItem < olItemList.GetSize(); ilItem++)
			pomPossilbeList->SetStyleRange(CGXRange(ilLc+1, ilItem+1, ilLc+1, ilItem+1), olStyle.SetValue(olItemList[ilItem]));
		pomPossilbeList->SetStyleRange(CGXRange(ilLc+1, imHideColStart, ilLc+1, imHideColStart), olStyle.SetValue(olText));

	}
	ilCount = m_ContentList.GetCount()+ 1;
		
	pomSelectedList->SetRowCount(max(7,ilCount));

	for (ilLc = 0; ilLc < ilCount-1; ilLc++)
	{
		m_ContentList.GetText(ilLc,olText);
		ExtractItemList(olText, &olItemList, ';');
		for(int ilItem = 0 ; ilItem < olItemList.GetSize(); ilItem++)
			pomSelectedList->SetStyleRange(CGXRange(ilLc+1, ilItem+1, ilLc+1, ilItem+1), olStyle.SetValue(olItemList[ilItem]));
		pomSelectedList->SetStyleRange(CGXRange(ilLc+1, imHideColStart, ilLc+1, imHideColStart), olStyle.SetValue(olText));

	}
	pomSelectedList->Redraw();
	pomPossilbeList->Redraw();

	if(m_ContentList.GetCount() == 0)
	{
		polWnd = GetDlgItem(IDOK);
		if(polWnd != NULL)
		{
			polWnd->EnableWindow(FALSE);
		}
	}
	else
	{
		polWnd = GetDlgItem(IDOK);
		if(polWnd != NULL)
		{
			polWnd->EnableWindow(TRUE);
		}
	}

	blIsInit = true;

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CLoadTimeSpanDlg::OnOK() 
{
	if(GetData())
	{
		UpdateData(TRUE);
		omStartTime = CTime::GetCurrentTime() - CTimeSpan(0,1,0,0);
		omEndTime = CTime::GetCurrentTime() + CTimeSpan(0,1,0,0);
		for( int i = 0; i < omValues.GetSize(); i++)
		{
			CString olTmpVal = omValues[i];
			if(omValues[i] != CString("") && omValues[i] != CString(" "))
			{
				switch (i)
				{
				case 0: // Zeitraum absolut von
					{
						if(m_AbsTimeVal == 0)
						{
							omStartTime = DBStringToDateTime(omValues[i]);
							omEndTime = DBStringToDateTime(omValues[i+1]);
							if(omEndTime <=  omStartTime)
							{
								CString olText, olAdd;
								olText = LoadStg(IDS_NO_DATE_TIME);
								olAdd = LoadStg(IDS_WARNING);
								MessageBox(olText, olAdd, MB_OK);
								return;
							}					
						}
					}
					break;
				case 2: //Rel. Stunden vor akt. Zeit ==> ist eigentlich tifa und tifd
					{
						if(m_AbsTimeVal == 1)
						{
							CTime olTime = CTime::GetCurrentTime();
							char pclHours[10]="";
							sprintf(pclHours, "%s", omValues[i]);

							olTime -= CTimeSpan((atoi(pclHours)*60*60));
							omStartTime = olTime;
						}
					}
					break;
				case 3: //Rel. Stunden nach akt. Zeit
					{
						if(m_AbsTimeVal == 1)
						{
							CTime olTime = CTime::GetCurrentTime();
							char pclHours[10]="";
							sprintf(pclHours, "%s", omValues[i]);

							olTime += CTimeSpan((atoi(pclHours)*60*60));
							omEndTime = olTime;
						}
					}
					break;
				}
			}
		}
		CDialog::OnOK();
	}
}

bool CLoadTimeSpanDlg::GetDispoZeitraumWhereString(CString &ropWhere)
{
	CString olWhere;
	CString olTmpTifaWhere;
	CString olTmpTifdWhere;

	bool blIsTifSet = false;

	CTime olTime1 = omStartTime;
	CTime olTime2 = omEndTime;
	//RST!
	
	CString T1 = olTime1.Format("%d.%m.%Y-%H:%M");
	CString T2 = olTime2.Format("%d.%m.%Y-%H:%M");
	ogBasicData.LocalToUtc(olTime1);
	ogBasicData.LocalToUtc(olTime2);
	T1 = olTime1.Format("%d.%m.%Y-%H:%M");
	T2 = olTime2.Format("%d.%m.%Y-%H:%M");
	CString olOrg3 = "";
	olOrg3.Format(" AND DES3 = '%s')",pcgHome);
	ropWhere += olTime1.Format("((TIFA BETWEEN '%Y%m%d%H%M00' AND ") + olTime2.Format("'%Y%m%d%H%M00'") +olOrg3  +
				olTime1.Format(" OR (STOA BETWEEN '%Y%m%d%H%M00' AND ") + olTime2.Format("'%Y%m%d%H%M00'") + olOrg3 + CString(")");
	
	ropWhere += " OR ";

	CString olDes3 = "";
	olDes3.Format(" AND ORG3 = '%s')",pcgHome);
	ropWhere += olTime1.Format("((TIFD BETWEEN '%Y%m%d%H%M00' AND ") + olTime2.Format("'%Y%m%d%H%M00'") + olDes3  + 
				olTime1.Format(" OR (STOD BETWEEN '%Y%m%d%H%M00' AND ") + olTime2.Format("'%Y%m%d%H%M00'") + olDes3 +CString(")");
	if(!ropWhere.IsEmpty())
	{
		ropWhere = CString("(") + ropWhere + CString(")");
	}

	return true;	
}

void CLoadTimeSpanDlg::OnAbstime() 
{
	m_ToTime.EnableWindow(TRUE);	
	m_ToDate.EnableWindow(TRUE);	
	m_FromDate.EnableWindow(TRUE);	
	m_FromTime.EnableWindow(TRUE);	
	m_RelFrom.EnableWindow(FALSE);	
	m_RelTo.EnableWindow(FALSE);	
}

void CLoadTimeSpanDlg::OnReltime() 
{
	m_ToTime.EnableWindow(FALSE);	
	m_ToDate.EnableWindow(FALSE);	
	m_FromDate.EnableWindow(FALSE);	
	m_FromTime.EnableWindow(FALSE);	
	m_RelFrom.EnableWindow(TRUE);	
	m_RelTo.EnableWindow(TRUE);	
	
}


void CLoadTimeSpanDlg::OnRemove() 
{
	CString olText;
	CGXStyle olStyle;
	CRowColArray olRows;
	bool blFoundAlle = false;

	// Move selected items from right list box to left list box
	int ilSelCount = (int)pomSelectedList->GetSelectedRows( olRows);
	
	if(ilSelCount > 0)
	{

	    pomPossilbeList->SetReadOnly(FALSE);
	    pomSelectedList->SetReadOnly(FALSE);

		int ilContCount = m_ContentList.GetCount();
		m_ContentList.ResetContent();
		for (int ilLc = 1 ; ilLc < min(ilContCount+1,(int)pomSelectedList->GetRowCount()); ilLc++)
		{
			olText = pomSelectedList->GetValueRowCol(ilLc, imHideColStart);
			m_ContentList.AddString(olText);
		}


		CString olComplText;

	// Move selected items from left list box to right list box
		for (ilLc = olRows.GetSize()-1; 
		ilLc >= 0 && blFoundAlle == false; ilLc--)
		{
			olText = pomSelectedList->GetValueRowCol(olRows[ilLc], 1);
			olComplText = pomSelectedList->GetValueRowCol(olRows[ilLc], imHideColStart);

			int ilDummy = (int)olRows[ilLc];
			if(ilDummy <= 0 || olText.IsEmpty())
				continue;
			
			int iltest = m_ContentList.FindStringExact(-1, olComplText);
			m_InsertList.AddString(olComplText);	// move string from right to left box
			m_ContentList.DeleteString(m_ContentList.FindStringExact(-1, olComplText));
		}
	
		olStyle.SetEnabled(FALSE);
		olStyle.SetReadOnly(TRUE);
		
		pomPossilbeList->RemoveRows(1,  pomPossilbeList->GetRowCount());

		CStringArray olItemList;

		int ilCount = m_InsertList.GetCount() + 1;

		int ilRealCount = 0;
		pomPossilbeList->SetRowCount(max(7,ilCount));
		for ( ilLc = 0; ilLc < ilCount-1; ilLc++)
		{
			m_InsertList.GetText(ilLc,olText);
			if(!olText.IsEmpty())
			{
				ilRealCount++;

				ExtractItemList(olText, &olItemList, ';');
				for(int ilItem = 0 ; ilItem < olItemList.GetSize(); ilItem++)
					pomPossilbeList->SetStyleRange(CGXRange(ilRealCount, ilItem+1, ilRealCount, ilItem+1), olStyle.SetValue(olItemList[ilItem]));
				pomPossilbeList->SetStyleRange(CGXRange(ilRealCount, imHideColStart, ilRealCount, imHideColStart), olStyle.SetValue(olText));
			}
		}

		
		pomSelectedList->RemoveRows(1,  pomSelectedList->GetRowCount());
		ilCount = m_ContentList.GetCount()+ 1;

		ilRealCount = 0;
		pomSelectedList->SetRowCount(max(7,ilCount));
		for (ilLc = 0; ilLc < ilCount-1; ilLc++)
		{
			m_ContentList.GetText(ilLc,olText);
			if(!olText.IsEmpty())
			{
				ilRealCount++;
				ExtractItemList(olText, &olItemList, ';');
				for(int ilItem = 0 ; ilItem < olItemList.GetSize(); ilItem++)
					pomSelectedList->SetStyleRange(CGXRange(ilRealCount, ilItem+1, ilRealCount, ilItem+1), olStyle.SetValue(olItemList[ilItem]));
				pomSelectedList->SetStyleRange(CGXRange(ilRealCount, imHideColStart, ilRealCount, imHideColStart), olStyle.SetValue(olText));
			}
		}
	
		CWnd *polWnd = NULL;
		if(m_ContentList.GetCount() == 0)
		{
			polWnd = GetDlgItem(IDOK);
			if(polWnd != NULL)
			{
				polWnd->EnableWindow(FALSE);
			}
		}
		else
		{
			polWnd = GetDlgItem(IDOK);
			if(polWnd != NULL)
			{
				polWnd->EnableWindow(TRUE);
			}
		}

		pomPossilbeList->SetReadOnly(TRUE);
	    pomSelectedList->SetReadOnly(TRUE);

	}
	
}

void CLoadTimeSpanDlg::OnAdd() 
{
	CString olText;
	bool blFoundAlle = false;
	CGXStyle olStyle;
	CRowColArray olRows;
	int ilSelCount = (int)pomPossilbeList->GetSelectedRows( olRows);

	
	if(ilSelCount > 0)
	{
		pomPossilbeList->SetReadOnly(FALSE);
	    pomSelectedList->SetReadOnly(FALSE);

		int ilContCount = m_InsertList.GetCount() + 1;
		m_InsertList.ResetContent();
		for (int ilLc = 1 ; ilLc < min(ilContCount+1,(int)pomPossilbeList->GetRowCount()); ilLc++)
		{
			olText = pomPossilbeList->GetValueRowCol(ilLc, imHideColStart);
			m_InsertList.AddString(olText);
		}

		

		CString olComplText;
	// Move selected items from left list box to right list box
		for ( ilLc = olRows.GetSize()-1; 
		ilLc >= 0 && blFoundAlle == false; ilLc--)
		{
			int ilDummy = (int)olRows[ilLc];
			olText = pomPossilbeList->GetValueRowCol(olRows[ilLc], 1);
			olComplText = pomPossilbeList->GetValueRowCol(olRows[ilLc], imHideColStart);
		
			if(ilDummy <= 0 || olText.IsEmpty())
				continue;

			int iltest = m_InsertList.FindStringExact(-1, olComplText);
			m_ContentList.AddString(olComplText);	// move string from left to right box
			m_InsertList.DeleteString(m_InsertList.FindStringExact(-1, olComplText));
	
		}
//		pomPossilbeList->Clear(FALSE);


		olStyle.SetEnabled(FALSE);
		olStyle.SetReadOnly(TRUE);
		
		pomPossilbeList->RemoveRows(1,  pomPossilbeList->GetRowCount());

		CStringArray olItemList;

		int ilCount = m_InsertList.GetCount() + 1;

		pomPossilbeList->SetRowCount(max(7,ilCount));
		int ilRealCount = 0;
		for ( ilLc = 0; ilLc < ilCount-1; ilLc++)
		{
			m_InsertList.GetText(ilLc,olText);
			if(!olText.IsEmpty())
			{
				ilRealCount++;
				ExtractItemList(olText, &olItemList, ';');
				for(int ilItem = 0 ; ilItem < olItemList.GetSize(); ilItem++)
					pomPossilbeList->SetStyleRange(CGXRange(ilRealCount, ilItem+1, ilRealCount, ilItem+1), olStyle.SetValue(olItemList[ilItem]));
				pomPossilbeList->SetStyleRange(CGXRange(ilRealCount, imHideColStart, ilRealCount, imHideColStart), olStyle.SetValue(olText));
			}
		}

		
		pomSelectedList->RemoveRows(1,  pomSelectedList->GetRowCount());
		ilCount = m_ContentList.GetCount()+ 1;
		pomSelectedList->SetRowCount(max(7,ilCount));
		ilRealCount = 0;
		for (ilLc = 0; ilLc < ilCount-1; ilLc++)
		{
			m_ContentList.GetText(ilLc,olText);
			if(!olText.IsEmpty())
			{
				ilRealCount++;
				ExtractItemList(olText, &olItemList, ';');
				for(int ilItem = 0 ; ilItem < olItemList.GetSize(); ilItem++)
					pomSelectedList->SetStyleRange(CGXRange(ilRealCount, ilItem+1, ilRealCount, ilItem+1), olStyle.SetValue(olItemList[ilItem]));
				pomSelectedList->SetStyleRange(CGXRange(ilRealCount, imHideColStart, ilRealCount, imHideColStart), olStyle.SetValue(olText));
			}
		}
		
		CWnd *polWnd = NULL;
		if(m_ContentList.GetCount() == 0)
		{
			polWnd = GetDlgItem(IDOK);
			if(polWnd != NULL)
			{
				polWnd->EnableWindow(FALSE);
			}
		}
		else
		{
			polWnd = GetDlgItem(IDOK);
			if(polWnd != NULL)
			{
				polWnd->EnableWindow(TRUE);
			}
		}


	    pomPossilbeList->SetReadOnly(TRUE);
	    pomSelectedList->SetReadOnly(TRUE);
	}
	
}
