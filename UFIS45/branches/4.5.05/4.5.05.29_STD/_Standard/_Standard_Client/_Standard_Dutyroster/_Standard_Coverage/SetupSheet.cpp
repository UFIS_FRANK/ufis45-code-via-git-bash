// SetupSheet.cpp : implementation file
//

#include <stdafx.h>
#include <coverage.h>
#include <SetupSheet.h>
#include <BasicData.h>
	
#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// SetupSheet

IMPLEMENT_DYNAMIC(SetupSheet, CPropertySheet)

SetupSheet::SetupSheet(UINT nIDCaption, CWnd* pParentWnd, UINT iSelectPage)
	:CPropertySheet(nIDCaption, pParentWnd, iSelectPage)
{
	AddPage(&omColorSetupPage);
	AddPage(&omCommonSetupPage);
}

SetupSheet::SetupSheet(LPCTSTR pszCaption, CWnd* pParentWnd, UINT iSelectPage)
	:CPropertySheet(pszCaption, pParentWnd, iSelectPage)
{
	AddPage(&omColorSetupPage);
	AddPage(&omCommonSetupPage);
}

SetupSheet::~SetupSheet()
{
}


BEGIN_MESSAGE_MAP(SetupSheet, CPropertySheet)
	//{{AFX_MSG_MAP(SetupSheet)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// SetupSheet message handlers

BOOL SetupSheet::OnInitDialog() 
{
	BOOL bResult = CPropertySheet::OnInitDialog();
	
	// TODO: Add your specialized code here
	this->SetWindowText(LoadStg(IDS_STRING1834));
	
	omColorSetupPage.SetCaption(LoadStg(IDS_STRING1835));
	omCommonSetupPage.SetCaption(LoadStg(IDS_STRING1855));

	return bResult;
}
