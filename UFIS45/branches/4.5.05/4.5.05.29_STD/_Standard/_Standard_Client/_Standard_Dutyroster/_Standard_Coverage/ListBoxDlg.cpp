// ListBoxDlg.cpp: Implementierungsdatei
//

#include <stdafx.h>
#include <ListBoxDlg.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CListBoxDlg 


CListBoxDlg::CListBoxDlg(CStringList* popStringList,CString opCaption,CString opInfoText,CWnd* pParent /* NULL*/)
	: CDialog(CListBoxDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CListBoxDlg)
		// HINWEIS: Der Klassen-Assistent f�gt hier Elementinitialisierung ein
	//}}AFX_DATA_INIT

	pomStringList = popStringList;
	omCaption = opCaption;
	omInfoText = opInfoText;
}


void CListBoxDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CListBoxDlg)
		// HINWEIS: Der Klassen-Assistent f�gt hier DDX- und DDV-Aufrufe ein
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CListBoxDlg, CDialog)
	//{{AFX_MSG_MAP(CListBoxDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten CListBoxDlg 

BOOL CListBoxDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	//Titel setzten	
	SetWindowText(omCaption);

	// Listbox f�llen
	POSITION olPos;
	CListBox* olListBox = (CListBox*) GetDlgItem(IDC_LIST);

	for (olPos = pomStringList->GetHeadPosition(); olPos != NULL;)
	{
		olListBox->AddString(pomStringList->GetNext(olPos));
	}

	// Infofeld Text setzen
	SetDlgItemText(IDC_INFO,omInfoText);

	return TRUE;
}
