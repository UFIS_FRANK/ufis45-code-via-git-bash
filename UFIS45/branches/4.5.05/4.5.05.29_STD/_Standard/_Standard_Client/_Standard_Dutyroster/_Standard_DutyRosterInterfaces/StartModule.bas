Attribute VB_Name = "StartModule"
Option Explicit
Public MyDateFormat As String
Public MyDateTimeFormat As String
Public newDrrTabFields As String
Sub Main()
    ApplicationIsStarted = False
    newDrrTabFields = "SDAY,STFU,AVFR,AVTO,BSDU,SCOD,SCOO,SBFR,SBTO,DRRN,SBLU,ROSS,ROSL,DRS2,URNO"
    UtcTimeDiff = 180
    myIniFile = "c:\ufis\system\RmsImportTool.ini"
    MyDateFormat = GetIniEntry(myIniFile, "MAIN", "", "DATE_FORMAT", "DDMMMYY")
    MyDateTimeFormat = GetIniEntry(myIniFile, "MAIN", "", "DATE_TIME_FORMAT", "DDMMMYY'/'hh':'mm")
    
    Load UfisServer
    UfisServer.ModName.Text = "RmsImport"
    frmMainDialog.Show
    If UfisServer.HostName <> "LOCAL" Then
        UfisServer.ConnectToCeda
    End If
    UfisServer.ConnectToBcProxy
    'Due to problems in bcserv32 (ignoring first broadcasts)
    '-------------------------------------------------------
    If UfisServer.HostName <> "LOCAL" Then
        If CedaIsConnected Then
            Screen.MousePointer = 11
            UfisServer.CallCeda UserAnswer, "SBC", "GDLTAB", "LOGIN", "RmsImport", "1/6", "", 0, True, False
            UfisServer.CallCeda UserAnswer, "SBC", "GDLTAB", "LOGIN", "RmsImport", "2/6", "", 0, True, False
            UfisServer.CallCeda UserAnswer, "SBC", "GDLTAB", "LOGIN", "RmsImport", "3/6", "", 0, True, False
            UfisServer.CallCeda UserAnswer, "SBC", "GDLTAB", "LOGIN", "RmsImport", "4/6", "", 0, True, False
            UfisServer.CallCeda UserAnswer, "SBC", "GDLTAB", "LOGIN", "RmsImport", "5/6", "", 0, True, False
            UfisServer.CallCeda UserAnswer, "SBC", "GDLTAB", "LOGIN", "RmsImport", "6/6", "", 0, True, False
            Screen.MousePointer = 0
        End If
    End If
    '-------------------------------------------------------
End Sub

Public Sub SetAllFormsOnTop(SetValue As Boolean)
'we do nothing here
End Sub

Public Sub HandleBroadCast(ReqId As String, BcUsr As String, BcWks As String, BcCmd As String, BcTbl As String, Seq As String, BcTws As String, BcTwe As String, BcSel As String, BcFld As String, BcDat As String, BcNum As String)
    frmMainDialog.EvaluateBroadCast BcCmd, BcTbl, BcSel, BcFld, BcDat, BcTws, BcTwe, BcUsr, BcWks
End Sub

Public Sub MsgFromUFISAppManager(Orig As Long, Data As String)
Dim tmpFldLst As String
Dim tmpDatLst As String
Dim nlPos As Integer
    If ApplicationIsStarted = True Then
        nlPos = InStr(Data, vbLf)
        If nlPos > 1 Then
            tmpFldLst = Left(Data, nlPos - 1)
            tmpDatLst = Mid(Data, nlPos + 1)
            'TelexPoolHead.SearchFlightTelexes tmpFldLst, tmpDatLst
        Else
            'If MyMsgBox.CallAskUser(0, 0, 0, "Communication Control", "Undefined message received from FIPS", "hand", "", UserAnswer) > 0 Then DoNothing
        End If
    End If
End Sub

Public Sub ShutDownApplication()
Dim i As Integer
Dim cnt As Integer
    On Error GoTo ErrorHandler
    If MyMsgBox.CallAskUser(0, 0, 0, "Application Control", "Do you want to exit ?", "ask", "Yes,No;F", UserAnswer) = 1 Then
        ShutDownRequested = True
        UfisServer.aCeda.CleanupCom
        'cnt = Forms.Count - 1
        'For i = cnt To 0 Step -1
        '    Unload Forms(i)
        'Next
        'Here we leave
        End
    End If
    'If not, we come back
    Exit Sub
ErrorHandler:
    Resume Next
End Sub

Public Sub RefreshMain()
    'do nothing
End Sub

Public Function DrawBackGround(MyPanel As PictureBox, MyColor As Integer, DrawHoriz As Boolean, DrawDown As Boolean) As Long
    Const intBLUESTART% = 255
    Const intBLUEEND% = 0
    Const intBANDHEIGHT% = 15
    Const intSHADOWSTART% = 64
    Const intSHADOWCOLOR% = 0
    Const intTEXTSTART% = 0
    Const intTEXTCOLOR% = 15
    Const intRed% = 1
    Const intGreen% = 2
    Const intBlue% = 4
    Const intBackRed% = 8
    Const intBackGreen% = 16
    Const intBackBlue% = 32
    Dim sngBlueCur As Single
    Dim sngBlueStep As Single
    Dim intFormHeight As Single
    Dim intFormWidth As Single
    Dim intX As Single
    Dim intY As Single
    Dim iColor As Integer
    Dim iRed As Single, iBlue As Single, iGreen As Single
    Dim ReturnColor As Long
    ReturnColor = vbWhite
    If MyColor >= 0 Then
        intFormHeight = MyPanel.ScaleHeight
        intFormWidth = MyPanel.ScaleWidth
    
        iColor = MyColor
        sngBlueCur = intBLUESTART
    
        If DrawDown Then
            If DrawHoriz Then
                sngBlueStep = intBANDHEIGHT * (intBLUEEND - intBLUESTART) / intFormHeight
                For intY = 0 To intFormHeight Step intBANDHEIGHT
                    If iColor And intBlue Then iBlue = sngBlueCur
                    If iColor And intRed Then iRed = sngBlueCur
                    If iColor And intGreen Then iGreen = sngBlueCur
                    If iColor And intBackBlue Then iBlue = 255 - sngBlueCur
                    If iColor And intBackRed Then iRed = 255 - sngBlueCur
                    If iColor And intBackGreen Then iGreen = 255 - sngBlueCur
                    MyPanel.Line (-1, intY - 1)-(intFormWidth, intY + intBANDHEIGHT), RGB(iRed, iGreen, iBlue), BF
                    sngBlueCur = sngBlueCur + sngBlueStep
                    If intY = 0 Then ReturnColor = RGB(iRed, iGreen, iBlue)
                Next intY
            Else
                sngBlueStep = intBANDHEIGHT * (intBLUEEND - intBLUESTART) / intFormWidth
                For intX = 0 To intFormWidth Step intBANDHEIGHT
                    If iColor And intBlue Then iBlue = sngBlueCur
                    If iColor And intRed Then iRed = sngBlueCur
                    If iColor And intGreen Then iGreen = sngBlueCur
                    If iColor And intBackBlue Then iBlue = 255 - sngBlueCur
                    If iColor And intBackRed Then iRed = 255 - sngBlueCur
                    If iColor And intBackGreen Then iGreen = 255 - sngBlueCur
                    MyPanel.Line (intX - 1, -1)-(intX + intBANDHEIGHT, intFormHeight), RGB(iRed, iGreen, iBlue), BF
                    sngBlueCur = sngBlueCur + sngBlueStep
                Next intX
            End If
        Else
            If DrawHoriz Then
                sngBlueStep = intBANDHEIGHT * (intBLUEEND - intBLUESTART) / intFormHeight
                For intY = intFormHeight To 0 Step -intBANDHEIGHT
                    If iColor And intBlue Then iBlue = sngBlueCur
                    If iColor And intRed Then iRed = sngBlueCur
                    If iColor And intGreen Then iGreen = sngBlueCur
                    If iColor And intBackBlue Then iBlue = 255 - sngBlueCur
                    If iColor And intBackRed Then iRed = 255 - sngBlueCur
                    If iColor And intBackGreen Then iGreen = 255 - sngBlueCur
                    MyPanel.Line (-1, intY - 1)-(intFormWidth, intY + intBANDHEIGHT), RGB(iRed, iGreen, iBlue), BF
                    sngBlueCur = sngBlueCur + sngBlueStep
                Next intY
            Else
                sngBlueStep = intBANDHEIGHT * (intBLUEEND - intBLUESTART) / intFormWidth
                For intX = intFormWidth To 0 Step -intBANDHEIGHT
                    If iColor And intBlue Then iBlue = sngBlueCur
                    If iColor And intRed Then iRed = sngBlueCur
                    If iColor And intGreen Then iGreen = sngBlueCur
                    If iColor And intBackBlue Then iBlue = 255 - sngBlueCur
                    If iColor And intBackRed Then iRed = 255 - sngBlueCur
                    If iColor And intBackGreen Then iGreen = 255 - sngBlueCur
                    MyPanel.Line (intX - 1, -1)-(intX + intBANDHEIGHT, intFormHeight), RGB(iRed, iGreen, iBlue), BF
                    sngBlueCur = sngBlueCur + sngBlueStep
                Next intX
            End If
        End If
    
    End If
    DrawBackGround = ReturnColor
End Function

Public Function GetDrawColor(DrawHeight As Long, MaxHeight As Long, MyColor As Integer) As Long
    Const intBLUESTART% = 255
    Const intBLUEEND% = 0
    Const intBANDHEIGHT% = 15
    Const intSHADOWSTART% = 64
    Const intSHADOWCOLOR% = 0
    Const intTEXTSTART% = 0
    Const intTEXTCOLOR% = 15
    Const intRed% = 1
    Const intGreen% = 2
    Const intBlue% = 4
    Const intBackRed% = 8
    Const intBackGreen% = 16
    Const intBackBlue% = 32
    Dim sngBlueCur As Single
    Dim sngBlueStep As Single
    Dim intFormHeight As Single
    Dim intX As Single
    Dim intY As Single
    Dim iColor As Integer
    Dim iRed As Single, iBlue As Single, iGreen As Single
    Dim ReturnColor As Long
    ReturnColor = -1
    If (MyColor >= 0) And (DrawHeight > 1) Then
        intFormHeight = DrawHeight
        iColor = MyColor
        sngBlueCur = intBLUESTART

        sngBlueStep = intBANDHEIGHT * (intBLUEEND - intBLUESTART) / intFormHeight
        For intY = 0 To intFormHeight Step intBANDHEIGHT
            If iColor And intBlue Then iBlue = sngBlueCur
            If iColor And intRed Then iRed = sngBlueCur
            If iColor And intGreen Then iGreen = sngBlueCur
            If iColor And intBackBlue Then iBlue = 255 - sngBlueCur
            If iColor And intBackRed Then iRed = 255 - sngBlueCur
            If iColor And intBackGreen Then iGreen = 255 - sngBlueCur
            sngBlueCur = sngBlueCur + sngBlueStep
            If intY >= MaxHeight Then
                ReturnColor = RGB(iRed, iGreen, iBlue)
                Exit For
            End If
        Next intY
    End If
    If ReturnColor < 0 Then ReturnColor = vbWhite
    GetDrawColor = ReturnColor
End Function

Public Sub PrintBackGroundText(MyPanel As PictureBox, CurFontSize As Integer, CurX As Long, CurY As Long, myText As String)
    Const intSHADOWCOLOR% = 0
    Const intTEXTCOLOR% = 15
    MyPanel.FontSize = CurFontSize
    MyPanel.CurrentX = CurX + 30
    MyPanel.CurrentY = CurY + 30
    MyPanel.ForeColor = QBColor(intSHADOWCOLOR)
    MyPanel.Print myText
    MyPanel.CurrentX = CurX
    MyPanel.CurrentY = CurY
    MyPanel.ForeColor = QBColor(intTEXTCOLOR)
    MyPanel.Print myText
    DoEvents
End Sub

Public Function InsertEmptyTabLine(CurTab As TABLib.TAB, UseLineNo As Long) As Long
    Dim tmpData As String
    Dim LineNo As Long
    If UseLineNo >= 0 Then LineNo = UseLineNo Else LineNo = CurTab.GetLineCount
    tmpData = CreateEmptyLine(CurTab.LogicalFieldList)
    CurTab.InsertTextLineAt LineNo, tmpData, False
    InsertEmptyTabLine = LineNo
End Function

Private Function CreateEmptyLine(FieldList As String) As String
    Dim Result As String
    Dim ItemNo As Long
    Dim FldNam As String
    Result = ""
    ItemNo = 1
    FldNam = GetRealItem(FieldList, ItemNo, ",")
    While FldNam <> ""
        Result = Result & ","
        ItemNo = ItemNo + 1
        FldNam = GetRealItem(FieldList, ItemNo, ",")
    Wend
    CreateEmptyLine = Result
End Function

Public Sub SetTabCellObjects(CurTab As TABLib.TAB, LineNo As Long, ObjType As String, CellObjName As String, FieldList As String, ClearBefore As Boolean)
    Dim tmpField As String
    Dim ColNo As Integer
    Dim i As Integer
    i = 0
    tmpField = "START"
    While tmpField <> ""
        i = i + 1
        tmpField = GetItem(FieldList, i, ",")
        If tmpField <> "" Then
            ColNo = GetRealItemNo(CurTab.LogicalFieldList, tmpField)
            If ColNo >= 0 Then
                Select Case ObjType
                    Case "DECO"
                        CurTab.SetDecorationObject LineNo, ColNo, CellObjName
                    Case "MARKER"
                        CurTab.SetCellProperty LineNo, ColNo, CellObjName
                    Case Else
                End Select
            End If
        End If
    Wend
End Sub


Public Sub SetTabIndexes(CurTab As TABLib.TAB, IndexList As String, FieldList As String, CreateIndex As Boolean)
    Dim tmpName As String
    Dim tmpField As String
    Dim ColNo As Integer
    Dim i As Integer
    i = 0
    tmpField = "START"
    While tmpField <> ""
        i = i + 1
        tmpField = GetItem(FieldList, i, ",")
        If tmpField <> "" Then
            tmpName = GetItem(IndexList, i, ",")
            ColNo = GetRealItemNo(CurTab.LogicalFieldList, tmpField)
            CurTab.IndexDestroy tmpName
            If (CreateIndex = True) And (CurTab.GetLineCount > 0) Then CurTab.IndexCreate tmpName, ColNo
        End If
    Wend
End Sub

Public Sub SetTabSortCols(CurTab As TABLib.TAB, FieldList As String)
    Dim tmpField As String
    Dim ColNo As Integer
    Dim i As Integer
    i = 0
    tmpField = "START"
    While tmpField <> ""
        i = i + 1
        tmpField = GetItem(FieldList, i, ",")
        If tmpField <> "" Then
            ColNo = GetRealItemNo(CurTab.LogicalFieldList, tmpField)
            If ColNo >= 0 Then CurTab.Sort ColNo, True, True
        End If
    Wend
    CurTab.AutoSizeColumns
End Sub


