#ifndef _CedaHdcData_H_
#define _CedaHdcData_H_
 
#include <CCSPtrArray.h>
#include <CCSCedaData.h>
#include <ccsddx.h>
#include <ccsglobl.h>
//#include "CedaDrrData.h" // f�r G�ltigkeitspr�fungen

/////////////////////////////////////////////////////////////////////////////

// COMMANDS FOR MEMBER-FUNCTIONS
#define HDC_SEND_DDX	(true)
#define HDC_NO_SEND_DDX	(false)

// Felddimensionen
#define HDC_DPT1_LEN	(8)
#define HDC_YMDY_LEN	(8)
#define HDC_REMA_LEN	(60)
#define HDC_HOPO_LEN	(3)
#define HDC_USEC_LEN	(32)
#define HDC_CDAT_LEN	(14)

// Struktur eines Hdc-Datensatzes
struct HDCDATA {
	long			Ubud;					//Urno BUDTAB
	char			Dpt1[HDC_DPT1_LEN+2];	//Organisationseinheit
	char			Ymdy[HDC_YMDY_LEN+2];	//Jahr/Monat/Stichtag
	//char			Rema[HDC_REMA_LEN+2];	//Remarks
	char			Hopo[HDC_HOPO_LEN+2];	//Hopo
	COleDateTime	Cdat; //[HDC_CDAT_LEN+2];	//Erstellungsdatum
	long			Urno;					//Urno
	char			Usec[HDC_USEC_LEN+2];	//Ersteller
	char			Useu[HDC_USEC_LEN+2];	//Anwender letzte �nderung
	COleDateTime	Lstu; //[HDC_CDAT_LEN+2];	//Datum letzte �nderung

	//DataCreated by this class
	int			IsChanged;	// Check whether Data has Changed f�r Relaese

	// Initialisation
	HDCDATA(void)
	{ 
		memset(this,'\0',sizeof(*this));
		IsChanged = DATA_UNCHANGED;
		//Abfr.SetStatus(COleDateTime::invalid);		// Abweichung von
		//Abto.SetStatus(COleDateTime::invalid);		// Abweichung bis
	}
};	

// the broadcast CallBack function, has to be outside the CedaHdcData class
void ProcessHdcCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//************************************************************************************************************************
//************************************************************************************************************************
// Deklaration CedaHdcData: kapselt den Zugriff auf die Tabelle Hdc (Daily Roster
//	Absences - untert�gige Abwesenheit)
//************************************************************************************************************************
//************************************************************************************************************************

class CedaHdcData: public CCSCedaData
{
// Funktionen
public:
	bool ReadHdcData();
    // Konstruktor/Destruktor
	CedaHdcData(CString opTableName = "Hdc", CString opExtName = "TAB");
	~CedaHdcData();

	void SetTableName(CString opTableName)
	{
		strcpy(pcmTableName, opTableName.GetBuffer(0));
	}

	// die geladenen Datens�tze
	CCSPtrArray<HDCDATA> omData;

	// allgemeine Funktionen
	// Feldliste lesen
	CString GetFieldList(void) {return CString(pcmListOfFields);}

	// Broadcasts empfangen und bearbeiten
	// behandelt die Broadcasts BC_HDC_CHANGE,BC_HDC_DELETE und BC_HDC_NEW
	void ProcessHdcBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

	// Datens�tze einlesen
	bool Read(char *pspWhere = NULL, CMapPtrToPtr *popLoadStfUrnoMap = NULL,
			  bool bpRegisterBC = true, bool bpClearData = true);
	// Datensatz mit bestimmter Urno einlesen
	bool ReadHdcByUrno(long lpUrno, HDCDATA *prpHdc);
	
	// einen Datensatz aus der Datenbank l�schen
	bool Delete(HDCDATA *prpHdc, bool bpWithSave = true);
	// einen neuen Datensatz in der Datenbank speichern
	bool Insert(HDCDATA *prpHdc, bool bpSave = true);
	// einen ge�nderten Datensatz speichern
	bool Update(HDCDATA *prpHdc, bool bpSave = true);

	// Datens�tze suchen
	// Hdc nach Urno suchen
	HDCDATA* GetHdcByUrno(long lpUrno);

	// kopiert die Feldwerte eines Hdc-Datensatzes ohne die Schl�sselfelder (Urno, etc.)
	void CopyHdc(HDCDATA* popHdcDataSource,HDCDATA* popHdcDataTarget);

protected:	
	// Funktionen
	// allgemeine Funktionen
	// Tabellenname einstellen
	bool SetTableNameAndExt(CString opTableAndExtName);
	// Feldliste schreiben
	void SetFieldList(CString opFieldList) {strcpy(pcmListOfFields,opFieldList.GetBuffer(0));}
	// alle internen Arrays zur Datenhaltung leeren und beim BC-Handler abmelden
	bool ClearAll(bool bpUnregister = true);

	// Broadcasts empfangen und bearbeiten
	// beim Broadcast-Handler anmelden
	void Register(void);

	// Kommunikation mit CEDA
	// Kommandos an CEDA senden (die Funktionen der Basisklasse werden �berschrieben
	// wegen des Feldlisten Bugs)
	bool CedaAction(char *pcpAction, char *pcpTableName, char *pcpFieldList,
					char *pcpSelection, char *pcpSort, char *pcpData, 
					char *pcpDest = "BUF1",bool bpIsWriteAction = false, 
					long lpID = 0, CCSPtrArray<RecordSet> *pomData = NULL, 
					int ipFieldCount = 0);
    bool CedaAction(char *pcpAction, char *pcpSelection = NULL, char *pcpSort = NULL,
					char *pcpData = pcgDataBuf, char *pcpDest = "BUF1", 
					bool bpIsWriteAction = false, long lpID = 0);
	
	// Datens�tze bearbeiten/speichern
	// speichert einen einzelnen Datensatz
	bool Save(HDCDATA *prpHdc);
	// einen Broadcast BUD_NEW abschicken und den Datensatz in die interne Datenhaltung aufnehmen
	bool InsertInternal(HDCDATA *prpHdc, bool bpSendDdx);
	// einen Broadcast BUD_CHANGE versenden
	bool UpdateInternal(HDCDATA *prpHdc, bool bpSendDdx = true);
	// einen Datensatz aus der internen Datenhaltung l�schen und einen Broadcast senden
	void DeleteInternal(HDCDATA *prpHdc, bool bpSendDdx = true);

	// Daten
    // Map mit den Datensatz-Urnos der geladenen Datens�tze
	CMapPtrToPtr omUrnoMap;
	// speichert die Feldnamen der Tabelle in sequentieller Form mit Trennzeichen Komma
	char pcmListOfFields[1024];
	// beim Broadcast-Handler angemeldet?
	bool bmIsBCRegistered;

	// ToDo: Minimale und maximales G�ltigkeitsdatum einbauen, damit nicht
	// unn�tig viele Datens�tze intern gehalten werden. Die min.-max.-Daten
	// m�ssen bei jeder Leseaktion (ReadXXX) gepr�ft und wenn n�tig erweitert
	// werden. Wenn dann BCs einlaufen (REL,NEW), kann gepr�ft werden ob
	// die Datens�tze in den BCs �berhaupt relevant sind.

	// min. und max. Tag der Ansichts-relevanten Hdcs
	//	COleDateTime omMinDay;
	//	COleDateTime omMaxDay;
};

#endif	// _CedaHdcData_H_
