// cedadatahelper.cpp - Klasse zum Konvertieren von Datenformaten
//
 
#include <stdafx.h>
#include <CedaDataHelper.h>

//************************************************************************************************************************************************
//************************************************************************************************************************************************
// Implementation der Klasse CedaDataHelper
//************************************************************************************************************************************************
//************************************************************************************************************************************************

//************************************************************************************************************************************************
// DateStringToOleDateTime: erzeugt aus einem String im Format YYYYMMDD ein 
//	COleDateTime-Obkjekt.
// R�ckgabe:	true	-> <opDate> g�ltig, <opTime> initialisiert
//				false	-> Fehler
//************************************************************************************************************************************************

bool CedaDataHelper::DateStringToOleDateTime(CString opDate, COleDateTime &opTime)
{
CCS_TRY
	if ((opDate == "") || (opDate.GetLength() < 8) || (opDate.SpanExcluding("0123456789") != "")){
		// <opDate> ung�ltig -> terminieren
		return false;
	}
	
	// COleDateTime-Objekt aus <opDate> erzeugen...
	COleDateTime olTime(atoi(opDate.Left(4).GetBuffer(0)), // Jahr (YYYY)
						atoi(opDate.Mid(4,2).GetBuffer(0)), // Monat (MM)
						atoi(opDate.Mid(6,2).GetBuffer(0)), // Tag (DD)
						0,0,0);	// Stunde, Minute, Sekunde
	// ...und kopieren
	opTime = olTime;
	return true;
CCS_CATCH_ALL
return false;
}

//************************************************************************************************************************************************
// IsWeekend: erzeugt aus einem String im Format YYYYMMDD ein 
//	COleDateTime-Obkjekt und pr�ft, ob der Wochentag Sonntag oder Sonnabend ist.
// R�ckgabe:	true	-> <opDate> g�ltig, Wochentag ist Wochenende
//				false	-> Fehler
//************************************************************************************************************************************************

bool CedaDataHelper::IsWeekend(CString opDateString)
{
CCS_TRY
	// f�r Datumskonvertierung
	COleDateTime opDate;
	if (!DateStringToOleDateTime(opDateString,opDate)) return false; // ung�ltiger String
	return IsWeekend(opDate);
CCS_CATCH_ALL
return false;
}

//************************************************************************************************************************************************
// IsWeekend: pr�ft, ob der Wochentag Sonntag oder Sonnabend ist.
// R�ckgabe:	true	-> Wochentag ist Wochenende
//				false	-> Fehler
//************************************************************************************************************************************************

bool CedaDataHelper::IsWeekend(COleDateTime opDate)
{
CCS_TRY
	// Wochentag pr�fen
	switch(opDate.GetDayOfWeek())
	{
	case 1: // Sonntag
	case 7: // Sonnabend
		return true;
	default: // anderer Tag
		return false;
	}
CCS_CATCH_ALL
return false;
}

//************************************************************************************************************************************************
// HourMinStringToOleDateTimeSpan: erzeugt aus einem String im Format 'HHMM' oder
//	'HHMMSS' ein COleDateTimeSpan-Obkjekt.
// R�ckgabe:	true	-> String g�ltig, <opTimeSpan> wurde initialisiert
//				false	-> Fehler
//************************************************************************************************************************************************

bool CedaDataHelper::HourMinStringToOleDateTimeSpan(CString opHourMin, COleDateTimeSpan &opTimeSpan)
{
CCS_TRY
	// Stirng pr�fen
	if ((opHourMin == "") || (opHourMin.SpanExcluding("0123456789") != "") ||
		((opHourMin.GetLength() != 4) && (opHourMin.GetLength() != 6))){
		// <opHourMin> ung�ltig -> terminieren
		return false;
	}
	
	// gibt es Sekunden?
	int ilSec = 0;
	if (opHourMin.GetLength() == 6) atoi(opHourMin.Right(2).GetBuffer(0)); // ja -> ermitteln

	// COleDateTimeSpan-Objekt aus Stirng erzeugen...
	COleDateTimeSpan olTimeSpan(0, // Tage = 0
								atoi(opHourMin.Left(2).GetBuffer(0)), // Stunde (HH)
								atoi(opHourMin.Mid(2,2).GetBuffer(0)), // Minuten (MM)
								ilSec);	// Sekunden
	// ...und kopieren
	opTimeSpan = olTimeSpan;
	return true;
CCS_CATCH_ALL
return false;
}

//************************************************************************************************************************************************
// DateTimeStringToOleDateTime: erzeugt aus einem Zeit-String im Format 'YYYYMMDDHHMMSS' 
//	ein Objekt vom Typ COleDateTime.
// R�ckgabe:	true	-> String g�ltig, <opTime> wurde initialisiert
//				false	-> Fehler
//************************************************************************************************************************************************

bool CedaDataHelper::DateTimeStringToOleDateTime(CString opDateTime, COleDateTime &opTime)
{

CCS_TRY
	// pr�fen, ob die L�nge stimmt
	if (opDateTime.GetLength() < 12) return false; // falsche L�nge

	// Datum und Zeit ermitteln
	COleDateTime olDate;
	COleDateTimeSpan olTimeSpan;
	if (!DateStringToOleDateTime(opDateTime.Left(8),olDate) ||
		!HourMinStringToOleDateTimeSpan(opDateTime.Mid(8),olTimeSpan))
	{
		// Fehler beim Konvertieren -> terminieren
		return false;
	}

	// Datum und Zeit addieren und kopieren
	opTime = olDate + olTimeSpan;
	return true;
CCS_CATCH_ALL
return false;

}

//************************************************************************************************************************************************
// DeleteExtraChars: L�scht alle ":" und "." aus einen String
// R�ckgabe:	gereinigter String
//************************************************************************************************************************************************

void CedaDataHelper::DeleteExtraChars(CString& opString)
{
CCS_TRY
	opString.Replace(":","");
	opString.Replace(".","");
CCS_CATCH_ALL
}

//*******************************************************************************************************
// GetDaysOfMonth(): ermittelt, wieviel Tage der Monat im Datum <opDays> hat
//	(28 min. z.B. Februar bis 31 max.).
// R�ckgabe:	die Anzahl der Tage des Monats.
//*******************************************************************************************************

int CedaDataHelper::GetDaysOfMonth(COleDateTime opDay)
{
CCS_TRY
	if(opDay.GetStatus() != COleDateTime::valid)
		return 0;

	COleDateTime olMonth = COleDateTime(opDay.GetYear(), opDay.GetMonth(),1,0,0,0);
	bool blNextMonth = false;
	int ilDays = 28;
	while(!blNextMonth)
	{
		COleDateTime olTmpDay = olMonth + COleDateTimeSpan(ilDays,0,0,0);
		if(olTmpDay.GetMonth() != opDay.GetMonth())
			blNextMonth = true;
		else
			ilDays++;
	}
	if(ilDays < 0 || ilDays > 31)
		ilDays = 0;
	return ilDays;
CCS_CATCH_ALL
return 0;
}

//*******************************************************************************************************
// GetMaxPeriod(): vergleicht zwei Zetr�ume miteinander und ermittelt 
//	die maximal resultierende Zeitspanne, wenn gew�nscht (<opMaxPeriodFrom> und
//	<opMaxPeriodTo> ungleich NULL).
// R�ckgabe:	
//	CHECK_P2_INNER_P1				->	die zweite Zeitspanne liegt innerhalb der ersten 
//										Zeitspanne
//	CHECK_P2_OUTER_P1				->	die erste Zeitspanne liegt innerhalb der zweiten 
//										Zeitspanne
//	CHECK_P2_INNER_ABOVE_P1			->	die zweite Zeitspanne beginnt innerhalb der
//										der ersten Zeitspanne, �berschreitet diese aber
//	CHECK_P2_ABOVE_P1				->	die zweite Zeitspanne liegt ausserhalb und �ber
//										der ersten Zeitspanne										
//	CHECK_P2_INNER_UNDERNEATH_P1	->	die zweite Zeitspanne beginnt innerhalb der
//										der ersten Zeitspanne, unterschreitet diese aber
//	CHECK_P2_UNDERNEATH_P1			->	die zweite Zeitspanne liegt ausserhalb und unter
//										der ersten Zeitspanne	
//	CHECK_INVALID_DATE				->	einer der Parameter ist ung�ltig oder Exception									
//*******************************************************************************************************

int CedaDataHelper::GetMaxPeriod(COleDateTime opP1From, COleDateTime opP1To, 
								 COleDateTime opP2From, COleDateTime opP2To, 
								 COleDateTime *popMaxPeriodFrom /*= NULL*/, 
								 COleDateTime *popMaxPeriodTo /*= NULL*/)
{
CCS_TRY
	if((opP1From.GetStatus() != COleDateTime::valid) || (opP1To.GetStatus() != COleDateTime::valid) || 
	   (opP2From.GetStatus() != COleDateTime::valid) || (opP2To.GetStatus() != COleDateTime::valid) ||
	   (opP1From > opP1To) || (opP2From > opP2To))
		return CHECK_INVALID_DATE;
	
	// max. Zeitraum
	COleDateTime olMaxFrom = opP2From;
	COleDateTime olMaxTo = opP2To;

	// R�ckgabewert
	int ilReturnCode = 0;

	// Zeitr�ume miteinander vergleichen, Ergebnisse bitweise kodieren,
	// max. Ladezeitraum ermitteln, wenn m�glich
	// Startzeitpunkt
	if (opP1From <= opP2From)
	{
		ilReturnCode += 1;
		olMaxFrom = opP1From;
		if (opP2From >= opP1To) 
			ilReturnCode += 4;
	}

	if (opP1To >= opP2To)
	{
		ilReturnCode += 2;
		olMaxTo = opP1To;
		if (opP2To <= opP1From)
			ilReturnCode += 8;
	}

	// max. Zeitraum speichern, wenn m�glich
	if (popMaxPeriodFrom != NULL) *popMaxPeriodFrom = olMaxFrom;
	if (popMaxPeriodTo != NULL) *popMaxPeriodTo = olMaxTo;

	// jetzt kann <ilReturnCode> einen der in CedaDataHelper.h definierten Werte haben
	return ilReturnCode;
CCS_CATCH_ALL
return CHECK_INVALID_DATE;
}

