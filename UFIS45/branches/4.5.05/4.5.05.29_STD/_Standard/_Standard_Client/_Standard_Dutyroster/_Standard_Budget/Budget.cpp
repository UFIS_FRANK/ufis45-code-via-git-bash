// Budget.cpp : Defines the class behaviors for the application.
#ifndef _DEF_mks_version
  #define _DEF_mks_version
  #include <ufisvers.h> /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version[] = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Client/_Standard_Dutyroster/_Standard_Budget/Budget.cpp 1.3 2003/10/09 17:51:06SGT fei Exp  $";
#endif /* _DEF_mks_version *///

#include <stdafx.h>
#include <Budget.h>
#include <BudgetDlg.h>


#include <CCSParam.h>		// Parameterklasse
#include <ListBoxDlg.h>		// Ausgabe neuer Parameter in diesem Dielog
#include <InitialLoadDlg.h>
#include <LoginDlg.h>
#include <BasicData.h>
#include <CCSGlobl.h>
#include <CViewer.h>
#include <PrivList.h>
#include <Ufis.h>
#include <RegisterDlg.h>
#include <CedaBasicData.h>
#include <CedaSystabData.h>
#include <GUILng.h>
#include <CedaBudData.h>
#include <CedaUbuData.h>
#include <CedaOrgData.h>
#include <CedaHdcData.h>
#include <CedaScoData.h>
#include <CedaSorData.h>
#include <CedaStfData.h>
#include <CedaCotData.h>
#include <CedaMutData.h>
#include <CedaUmuData.h>
#include <CedaUhdData.h>
#include <CedaForData.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

CStringArray  ogCmdLineStghArray;

/////////////////////////////////////////////////////////////////////////////
// CBudgetApp

BEGIN_MESSAGE_MAP(CBudgetApp, CWinApp)
	//{{AFX_MSG_MAP(CBudgetApp)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG
	ON_COMMAND(ID_HELP, CWinApp::OnHelp)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CBudgetApp construction

CBudgetApp::CBudgetApp()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}

/////////////////////////////////////////////////////////////////////////////
// The one and only CBudgetApp object

CBudgetApp theApp;

/////////////////////////////////////////////////////////////////////////////
// CBudgetApp initialization

BOOL CBudgetApp::InitInstance()
{
	AfxEnableControlContainer();

	GXInit();

	// Standard initialization
	// If you are not using these features and wish to reduce the size
	//  of your final executable, you should remove from the following
	//  the specific initialization routines you do not need.

#ifdef _AFXDLL
	Enable3dControls();			// Call this when using MFC in a shared DLL
#else
	Enable3dControlsStatic();	// Call this when linking to MFC statically
#endif

	//***************************************************************************
	// Parameter�bergabe durch Aufruf aus einem anderen Programm
	//***************************************************************************
	ogCmdLineStghArray.RemoveAll();
	CString olCmdLine =  LPCTSTR(m_lpCmdLine);
	//m_lpCmdLine => "AppName,UserID,Password"
	if(olCmdLine.GetLength() == 0)
	{
		ogCmdLineStghArray.Add(ogAppName);
		ogCmdLineStghArray.Add("");
		ogCmdLineStghArray.Add("");
		ogCmdLineStghArray.Add("");
	}
	else
	{
		if(ExtractItemList(olCmdLine,&ogCmdLineStghArray) != 4)
		{
			MessageBox(NULL,LoadStg(IDS_STRING31),"Budget",MB_ICONERROR);
			return FALSE;
		}
	}
	//***************************************************************************
	// Parameter�bergabe Ende
	//***************************************************************************
	
	// Change the registry key under which our settings are stored.
	// TODO: You should modify this string to be something appropriate
	// such as the name of your company or organization.
	SetRegistryKey(_T("Ufis"));

	//LoadStdProfileSettings();  // Load standard INI file options (including MRU)


	char pclConfigPath[256];

    if (getenv("CEDA") == NULL)
        strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
    else
        strcpy(pclConfigPath, getenv("CEDA"));

    GetPrivateProfileString("GLOBAL", "HOMEAIRPORT", "TAB", pcgHome, sizeof pcgHome, pclConfigPath);
    GetPrivateProfileString("GLOBAL", "TABLEEXTENSION", "TAB", pcgTableExt, sizeof pcgTableExt, pclConfigPath);

	// CedaBasicData Objekt initialisieren
	ogBCD.SetTableExtension(CString(pcgTableExt));
	ogBCD.SetHomeAirport(CString(pcgHome));

	strcpy(CCSCedaData::pcmTableExt, pcgTableExt);
	strcpy(CCSCedaData::pcmApplName, pcgAppName);
	strcpy(CCSCedaData::pcmHomeAirport, pcgHome);

	// Standard Fonts and Brushes initialization
    InitFont();
	CreateBrushes();

	// INIT Tablenames and Homeairport
	strcpy(CCSCedaData::pcmTableExt, pcgTableExt);
	strcpy(CCSCedaData::pcmApplName, ogAppName);

	ogCommHandler.SetAppName(ogAppName);

    if(ogCommHandler.Initialize() != true)  // connection error?
    {
        AfxMessageBox(CString("CedaCom ") + ogCommHandler.LastError());
        return FALSE;
    }

	//***********************************************************************************************
	// Initialisierung der GUI-Sprache
	//***********************************************************************************************
	// in ceda.ini einf�gen !!!!!!!!
	// ;'DE','US','IT' oder 'DE,Test' etc.
	// LANGUAGE=DE
	char lng[128];
	char dbparam[128];

	CGUILng* ogGUILng = CGUILng::TheOne();

    GetPrivateProfileString("GLOBAL", "LANGUAGE", "", lng, sizeof lng, pclConfigPath);

    int ret = GetPrivateProfileString(ogAppName, "DBParam", "", dbparam, sizeof dbparam, pclConfigPath);
	CString Param = dbparam;
	if (Param.IsEmpty())
	{
		Param = "0";
	}

	CString Error	= "";
	CStringArray olApplArray;
	olApplArray.Add(ogAppl);
	bool rdr = ogGUILng->MemberInit(&Error, CString(pcgUser), &olApplArray, CString(pcgHome), CString(lng), Param);
	if (!rdr)
	{
		CString tmp = "Languagesupport failed!\n"+Error+"parameter empty!\n";
        AfxMessageBox(tmp);
		TRACE("\nMultiLng-Support failed!\n %sparameter empty!!!\n",Error);
	}
	//***********************************************************************************************
	// ENDE der Initialisierung der GUI-Sprache
	//***********************************************************************************************

	//---------------------------------------------------------------------------
	// Register your broadcasts here
	CString olTableName;
	CString olTableExt = CString( pcgTableExt);
	// RT : Read Table
	// IRT: Insert Record Table
	// URT: Update Record Table
	// DRT: Delete Record Table

	
	// DEMO
	//olTableName = CString("STF") + olTableExt;
	//ogBcHandle.AddTableCommand( olTableName, CString("IRT"),BC_STF_NEW,    true);
	//ogBcHandle.AddTableCommand( olTableName, CString("URT"),BC_STF_CHANGE, true);
	//ogBcHandle.AddTableCommand( olTableName, CString("DRT"),BC_STF_DELETE, true);
	//---------------------------------------------------------------------------
	
	// Login Dialog aufrufen
	// pcgHome,ogAppName und ogAppl sind global definiert und hardcodiert.
	CLoginDialog olLoginDlg(pcgHome,ogAppName,NULL);
	if(ogCmdLineStghArray.GetAt(0) != ogAppName)
	{
		if(olLoginDlg.Login(ogCmdLineStghArray.GetAt(1),ogCmdLineStghArray.GetAt(2)) == false)
		{
			return FALSE;
		}
	}
	else
	{
		if( olLoginDlg.DoModal() != IDCANCEL )
		{
			int ilStartApp = IDOK; 
			if(ogPrivList.GetStat("InitModu") == '1')
			{
				RegisterDlg olRegisterDlg;
				ilStartApp = olRegisterDlg.DoModal();
			}
			if(ilStartApp != IDOK)
			{
				return FALSE;
			}
		}
		else
		{
			return FALSE;
		} 
	}
	//---------------------------------------------------------------------------

	// Login Zeit setzen
	ogLoginTime = CTime::GetCurrentTime();
	// Ruft offensichtlich den "hochlade" Dialog auf
    InitialLoad();
	

	ogBasicData.SetLocalDiff();
	CCSCedaData::omLocalDiff1 = ogBasicData.GetLocalDiff1();
	CCSCedaData::omLocalDiff2 = ogBasicData.GetLocalDiff2();
	CCSCedaData::omTich = ogBasicData.GetTich(); 

	CBudgetDlg *dlg = new CBudgetDlg;
	m_pMainWnd = dlg;
	int nResponse = dlg->DoModal();
	if (nResponse == IDOK)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with OK
	}
	else if (nResponse == IDCANCEL)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with Cancel
	}

	// Since the dialog has been closed, return FALSE so that we exit the
	//  application, rather than start the application's message pump.
	
	return FALSE;
}


//****************************************************************************
// Hier werden die Daten von der Datenbank in die Application geladen
//****************************************************************************

void CBudgetApp::InitialLoad()
{
	pogInitialLoad = new CInitialLoadDlg();
	if (pogInitialLoad != NULL)
	{
		pogInitialLoad->SetMessage(LoadStg(IDS_STRING1362));
	}
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));

	bgIsInitialized = false;

	/////////////////////////////////////////////////////////////////////////////////////
	// add here your cedaXXXdata read methods
	/////////////////////////////////////////////////////////////////////////////////////
	//------------------------------------------------------------------------------------
	ogCfgData.SetTableName(CString(CString("VCD") + CString(pcgTableExt)));
    ogBudData.SetTableName(CString(CString("BUD") + CString(pcgTableExt)));
	ogUbuData.SetTableName(CString(CString("UBU") + CString(pcgTableExt)));
	ogOrgData.SetTableName(CString(CString("ORG") + CString(pcgTableExt)));
	ogHdcData.SetTableName(CString(CString("HDC") + CString(pcgTableExt)));
	ogUhdData.SetTableName(CString(CString("UHD") + CString(pcgTableExt)));
	ogScoData.SetTableName(CString(CString("SCO") + CString(pcgTableExt)));
	ogSorData.SetTableName(CString(CString("SOR") + CString(pcgTableExt)));
	ogStfData.SetTableName(CString(CString("STF") + CString(pcgTableExt)));
	ogCotData.SetTableName(CString(CString("COT") + CString(pcgTableExt)));
	ogMutData.SetTableName(CString(CString("MUT") + CString(pcgTableExt)));
	ogUmuData.SetTableName(CString(CString("UMU") + CString(pcgTableExt)));
	ogForData.SetTableName(CString(CString("FOR") + CString(pcgTableExt)));

	//--------------------------------------------------------------------------------------
	//Update Sync because a crashed apllication could have set this state
	CTime olCurrentTime; 
	CTime olToTime;
	olCurrentTime = CTime::GetCurrentTime();
	olToTime = olCurrentTime + CTimeSpan(0, 0, 2, 0);
	CString olStrFrom = CTimeToDBString(olCurrentTime,olCurrentTime);
	CString olStrTo = CTimeToDBString(olToTime,olToTime);
	char pclData[1024]="";

	//SYNC END------------------------------------------------------------------------

	ogCfgData.ReadCfgData();
	ogCfgData.ReadMonitorSetup();

	//---------------------------------------------------------------------------------
	// Start Reading
	
	CString olSizeText;
	int ilTmpLoadSize = 0;
	int ilPercent = 100 / 8; //Teiler ist gleich Anzahl der zu lesenden Tabellen
	

	// Parameter mittels CCSParam Klasse einlesen
	pogInitialLoad->SetMessage(LoadStg(IDS_STRING1844));
	olSizeText.Format(" ........ %d",ogCCSParam.BufferParams(ogAppl));
	// Parameter testen
	LoadParameters();
	pogInitialLoad->SetMessage(olSizeText, false);
	pogInitialLoad->SetProgress(ilPercent);
	
	// ORGTAB lesen
	pogInitialLoad->SetMessage(LoadStg(IDS_STRING1029));
	ogOrgData.Read(); //OrgData();
	olSizeText.Format(" ........ %d",ogOrgData.omData.GetSize());
	pogInitialLoad->SetMessage(olSizeText, false);
	pogInitialLoad->SetProgress(ilPercent);
	
	// STFTAB lesen
	pogInitialLoad->SetMessage(LoadStg(IDS_STRING1032));
	ogStfData.Read(); //StfData();
	olSizeText.Format(" ........ %d",ogStfData.omData.GetSize());
	pogInitialLoad->SetMessage(olSizeText, false);
	pogInitialLoad->SetProgress(ilPercent);

	// SCOTAB lesen
	//pogInitialLoad->SetMessage(LoadStg(IDS_STRING1029));
	ogScoData.Read(); //ScoData();
	//olSizeText.Format(" ........ %d",ogScoData.omData.GetSize());
	//pogInitialLoad->SetMessage(olSizeText, false);
	pogInitialLoad->SetProgress(ilPercent);

	// SORTAB lesen
	//pogInitialLoad->SetMessage(LoadStg(IDS_STRING1029));
	ogSorData.Read(); //SorData();
	//olSizeText.Format(" ........ %d",ogSorData.omData.GetSize());
	//pogInitialLoad->SetMessage(olSizeText, false);
	pogInitialLoad->SetProgress(ilPercent);
	
	// COTTAB lesen
	pogInitialLoad->SetMessage(LoadStg(IDS_STRING1033));
	ogCotData.Read(); //CotData();
	olSizeText.Format(" ........ %d",ogCotData.omData.GetSize());
	pogInitialLoad->SetMessage(olSizeText, false);
	pogInitialLoad->SetProgress(ilPercent);

	// BUDTAB lesen
	pogInitialLoad->SetMessage(LoadStg(IDS_STRING1028));
	ogBudData.Read(); //BudData();
	olSizeText.Format(" ........ %d",ogBudData.omData.GetSize());
	pogInitialLoad->SetMessage(olSizeText, false);
	pogInitialLoad->SetProgress(ilPercent);

	// UBUTAB lesen
	//pogInitialLoad->SetMessage(LoadStg(IDS_STRING1029));
	ogUbuData.Read(); //UbuData();
	//olSizeText.Format(" ........ %d",ogUbuData.omData.GetSize());
	//pogInitialLoad->SetMessage(olSizeText, false);
	pogInitialLoad->SetProgress(ilPercent);

	// HDCTAB lesen
	pogInitialLoad->SetMessage(LoadStg(IDS_STRING1031));
	ogHdcData.Read(); //HdcData();
	olSizeText.Format(" ........ %d",ogHdcData.omData.GetSize());
	pogInitialLoad->SetMessage(olSizeText, false);
	pogInitialLoad->SetProgress(ilPercent);

	// UHDTAB lesen
	//pogInitialLoad->SetMessage(LoadStg(IDS_STRING1029));
	ogUhdData.Read(); //UhdData();
	//olSizeText.Format(" ........ %d",ogUhdData.omData.GetSize());
	//pogInitialLoad->SetMessage(olSizeText, false);
	pogInitialLoad->SetProgress(ilPercent);
	
	// MUTTAB lesen
	pogInitialLoad->SetMessage(LoadStg(IDS_STRING1034));
	ogMutData.Read(); //MutData();
	olSizeText.Format(" ........ %d",ogMutData.omData.GetSize());
	pogInitialLoad->SetMessage(olSizeText, false);
	pogInitialLoad->SetProgress(ilPercent);

	// UMUTAB lesen
	//pogInitialLoad->SetMessage(LoadStg(IDS_STRING1029));
	ogUmuData.Read(); //UmuData();
	//olSizeText.Format(" ........ %d",ogUmuData.omData.GetSize());
	//pogInitialLoad->SetMessage(olSizeText, false);
	pogInitialLoad->SetProgress(ilPercent);
	
	// FORTAB lesen
	pogInitialLoad->SetMessage(LoadStg(IDS_STRING1035));
	ogForData.Read();
	olSizeText.Format(" ........ %d",ogForData.omData.GetSize());
	pogInitialLoad->SetMessage(olSizeText, false);
	pogInitialLoad->SetProgress(ilPercent);

	// Demo CedaXxxData
	//pogInitialLoad->SetMessage(LoadStg(IDS_STRING1381));
	//ogStfData.Read();
	//olSizeText.Format(" ........ %d",ogStfData.omData.GetSize());
	//pogInitialLoad->SetMessage(olSizeText, false);
	//pogInitialLoad->SetProgress(ilPercent);

	// Demo ogBCD
	//pogInitialLoad->SetMessage("Test");
	//ogBCD.SetObject("GAT");
	//ogBCD.SetObjectDesc("GAT", LoadStg(IDS_STRING1796));
	//ogBCD.AddKeyMap("GAT", "GNAM");
	//ogBCD.Read(CString("GAT"));
	//olSizeText.Format(" ........ %d",ogBCD.GetDataCount("GAT"));
	//pogInitialLoad->SetMessage(olSizeText, false);
	//pogInitialLoad->SetProgress(ilPercent);


	Sleep(1000);
	//-- End Reading
	pogInitialLoad->SetProgress(100);
	Sleep(1500);
	// globale Funktion ?

	// Hochlade Dialog beenden
	if (pogInitialLoad != NULL)
	{
		pogInitialLoad->DestroyWindow();
		pogInitialLoad = NULL;
	}
}


//****************************************************************************
// Parameter Werte pr�fen und ggf. Defaultwerte setzen
//****************************************************************************

void CBudgetApp::LoadParameters()
{


	CString olStringNow = ("19501010101010");

	CString olDefaultValidFrom	= olStringNow;
	CString olDefaultValidTo	= "";
	
	// Falls Parameter nicht exitiert. anlegen und Defaultwert setzen.

	// Demo
	//ogCCSParam.GetParam(ogAppl,"ID_BLANK_CHAR",olStringNow,"-","Default Zeichen","General","","TRIM",olDefaultValidFrom,olDefaultValidTo,"","","",true);

}




//****************************************************************************
// CAboutDlg dialog used for App About
//****************************************************************************

// App command to run the dialog
void CBudgetApp::OnAppAbout()
{
	CAboutDlg aboutDlg;
	aboutDlg.DoModal();
}

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	DDX_Control(pDX, IDC_STATIC_USER, m_StcUser);
	DDX_Control(pDX, IDC_STATIC_SERVER, m_StcServer);
	DDX_Control(pDX, IDC_STATIC_LOGINTIME, m_StcLogintime);
	DDX_Control(pDX, IDC_COPYRIGHT4, m_Copyright4);
	DDX_Control(pDX, IDC_COPYRIGHT3, m_Copyright3);
	DDX_Control(pDX, IDC_COPYRIGHT2, m_Copyright2);
	DDX_Control(pDX, IDC_COPYRIGHT1, m_Copyright1);
	DDX_Control(pDX, IDC_SERVER,	 m_Server);
	DDX_Control(pDX, IDC_USER,		 m_User);
	DDX_Control(pDX, IDC_LOGINTIME,  m_Logintime);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

//****************************************************************************
// Daten aufbereiten und darstellen
//****************************************************************************

BOOL CAboutDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// Statics einstellen
	m_Copyright1.SetWindowText(LoadStg(IDS_COPYRIGHT1));
	m_Copyright2.SetWindowText(LoadStg(IDS_COPYRIGHT2));
	m_Copyright3.SetWindowText(LoadStg(IDS_COPYRIGHT3));
	m_Copyright4.SetWindowText(LoadStg(IDS_COPYRIGHT4));
	m_StcServer.SetWindowText(LoadStg(IDS_STATIC_SERVER));
	m_StcUser.SetWindowText(LoadStg(IDS_STATIC_USER));
	m_StcLogintime.SetWindowText(LoadStg(IDS_STATIC_LOGINTIME));

	CString olServer = ogCommHandler.pcmRealHostName;
	olServer  += " / ";
	olServer  += ogCommHandler.pcmRealHostType;
	m_Server.SetWindowText(olServer);

	m_User.SetWindowText(CString(pcgUser));
	m_Logintime.SetWindowText(ogLoginTime.Format("%d.%m.%Y  %H:%M"));

	SetWindowText(LoadStg(IDS_APPL_ABOUTBOX));
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

