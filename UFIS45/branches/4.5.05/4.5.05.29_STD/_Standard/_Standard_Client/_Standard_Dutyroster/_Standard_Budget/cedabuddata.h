#ifndef _CedaBudData_H_
#define _CedaBudData_H_
 
#include <CCSPtrArray.h>
#include <CCSCedaData.h>
#include <ccsddx.h>
#include <ccsglobl.h>
//#include "CedaDrrData.h" // f�r G�ltigkeitspr�fungen

/////////////////////////////////////////////////////////////////////////////

// COMMANDS FOR MEMBER-FUNCTIONS
#define BUD_SEND_DDX	(true)
#define BUD_NO_SEND_DDX	(false)

// Felddimensionen
#define BUD_DAYX_LEN	(2)
#define BUD_FRMO_LEN	(6)
#define BUD_HDCO_LEN	(1)
#define BUD_REMA_LEN	(60)
#define BUD_USEC_LEN	(32)
#define BUD_CDAT_LEN	(14)
#define BUD_HOPO_LEN	(3)

// Struktur eines Bud-Datensatzes
struct BUDDATA {
	char			Dayx[BUD_DAYX_LEN+2];	//Stichtag
	char			Frmo[BUD_FRMO_LEN+2];	//Jahr/Monat von
	char			Tomo[BUD_FRMO_LEN+2];	//Jahr/Monat bis
	char			Hdco[BUD_HDCO_LEN+2];	//Headcount
	char			Muta[BUD_HDCO_LEN+2];	//Mutation
	char			Foca[BUD_HDCO_LEN+2];	//Forecast
	char			Budg[BUD_HDCO_LEN+2];	//Budget
	char			Rema[BUD_REMA_LEN+2];	//Remarks
	char			Hopo[BUD_HOPO_LEN+2];	//Hopo
	COleDateTime	Cdat; //[BUD_CDAT_LEN+2];	//Erstellungsdatum
	long			Urno;					//Urno
	char			Usec[BUD_USEC_LEN+2];	//Ersteller
	char			Useu[BUD_USEC_LEN+2];	//Anwender letzte �nderung
	COleDateTime	Lstu; //[BUD_CDAT_LEN+2];	//Datum letzte �nderung

	//DataCreated by this class
	int			IsChanged;	// Check whether Data has Changed f�r Relaese

	// Initialisation
	BUDDATA(void)
	{ 
		memset(this,'\0',sizeof(*this));
		IsChanged = DATA_UNCHANGED;
		//Abfr.SetStatus(COleDateTime::invalid);		// Abweichung von
		//Abto.SetStatus(COleDateTime::invalid);		// Abweichung bis
	}
};	

// the broadcast CallBack function, has to be outside the CedaBudData class
void ProcessBudCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//************************************************************************************************************************
//************************************************************************************************************************
// Deklaration CedaBudData: kapselt den Zugriff auf die Tabelle Bud (Daily Roster
//	Absences - untert�gige Abwesenheit)
//************************************************************************************************************************
//************************************************************************************************************************

class CedaBudData: public CCSCedaData
{
// Funktionen
public:
	bool ReadBudData();
    // Konstruktor/Destruktor
	CedaBudData(CString opTableName = "Bud", CString opExtName = "TAB");
	~CedaBudData();

	void SetTableName(CString opTableName)
	{
		strcpy(pcmTableName, opTableName.GetBuffer(0));
	}

	// die geladenen Datens�tze
	CCSPtrArray<BUDDATA> omData;

	// allgemeine Funktionen
	// Feldliste lesen
	CString GetFieldList(void) {return CString(pcmListOfFields);}

	// Broadcasts empfangen und bearbeiten
	// behandelt die Broadcasts BC_BUD_CHANGE,BC_BUD_DELETE und BC_BUD_NEW
	void ProcessBudBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

	// Datens�tze einlesen
	bool Read(char *pspWhere = NULL, CMapPtrToPtr *popLoadStfUrnoMap = NULL,
			  bool bpRegisterBC = true, bool bpClearData = true);
	// Datensatz mit bestimmter Urno einlesen
	bool ReadBudByUrno(long lpUrno, BUDDATA *prpBud);
	
	// einen Datensatz aus der Datenbank l�schen
	bool Delete(BUDDATA *prpBud, bool bpWithSave = true);
	// einen neuen Datensatz in der Datenbank speichern
	bool Insert(BUDDATA *prpBud, bool bpSave = true);
	// einen ge�nderten Datensatz speichern
	bool Update(BUDDATA *prpBud, bool bpSave = true);

	
	// Datens�tze suchen
	// Bud nach Urno suchen
	BUDDATA* GetBudByUrno(long lpUrno);

	// kopiert die Feldwerte eines Bud-Datensatzes ohne die Schl�sselfelder (Urno, etc.)
	void CopyBud(BUDDATA* popBUDDATASource,BUDDATA* popBUDDATATarget);

protected:	
	// Funktionen
	// allgemeine Funktionen
	// Tabellenname einstellen
	bool SetTableNameAndExt(CString opTableAndExtName);
	// Feldliste schreiben
	void SetFieldList(CString opFieldList) {strcpy(pcmListOfFields,opFieldList.GetBuffer(0));}
	// alle internen Arrays zur Datenhaltung leeren und beim BC-Handler abmelden
	bool ClearAll(bool bpUnregister = true);

	// Broadcasts empfangen und bearbeiten
	// beim Broadcast-Handler anmelden
	void Register(void);

	// Kommunikation mit CEDA
	// Kommandos an CEDA senden (die Funktionen der Basisklasse werden �berschrieben
	// wegen des Feldlisten Bugs)
	bool CedaAction(char *pcpAction, char *pcpTableName, char *pcpFieldList,
					char *pcpSelection, char *pcpSort, char *pcpData, 
					char *pcpDest = "BUF1",bool bpIsWriteAction = false, 
					long lpID = 0, CCSPtrArray<RecordSet> *pomData = NULL, 
					int ipFieldCount = 0);
    bool CedaAction(char *pcpAction, char *pcpSelection = NULL, char *pcpSort = NULL,
					char *pcpData = pcgDataBuf, char *pcpDest = "BUF1", 
					bool bpIsWriteAction = false, long lpID = 0);
	
	// Datens�tze bearbeiten/speichern
	// speichert einen einzelnen Datensatz
	bool Save(BUDDATA *prpBud);
	// einen Broadcast BUD_NEW abschicken und den Datensatz in die interne Datenhaltung aufnehmen
	bool InsertInternal(BUDDATA *prpBud, bool bpSendDdx);
	// einen Broadcast BUD_CHANGE versenden
	bool UpdateInternal(BUDDATA *prpBud, bool bpSendDdx = true);
	// einen Datensatz aus der internen Datenhaltung l�schen und einen Broadcast senden
	void DeleteInternal(BUDDATA *prpBud, bool bpSendDdx = true);

	// Daten
    // Map mit den Datensatz-Urnos der geladenen Datens�tze
	CMapPtrToPtr omUrnoMap;
	// speichert die Feldnamen der Tabelle in sequentieller Form mit Trennzeichen Komma
	char pcmListOfFields[1024];
	// beim Broadcast-Handler angemeldet?
	bool bmIsBCRegistered;

	// ToDo: Minimale und maximales G�ltigkeitsdatum einbauen, damit nicht
	// unn�tig viele Datens�tze intern gehalten werden. Die min.-max.-Daten
	// m�ssen bei jeder Leseaktion (ReadXXX) gepr�ft und wenn n�tig erweitert
	// werden. Wenn dann BCs einlaufen (REL,NEW), kann gepr�ft werden ob
	// die Datens�tze in den BCs �berhaupt relevant sind.

	// min. und max. Tag der Ansichts-relevanten Buds
	//	COleDateTime omMinDay;
	//	COleDateTime omMaxDay;
};

#endif	// _CedaBudData_H_
