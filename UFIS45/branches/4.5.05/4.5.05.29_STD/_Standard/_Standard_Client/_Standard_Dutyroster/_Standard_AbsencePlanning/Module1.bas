Attribute VB_Name = "Module1"
Option Explicit

Public fMainForm As frmMain
Public strRetLogin As String

Declare Sub InitCommonControls Lib "comctl32.dll" ()
' The NMHDR structure contains information about a notification message. The pointer
' to this structure is specified as the lParam member of the WM_NOTIFY message.
Public Type NMHDR
    hwndFrom As Long    ' Window handle of control sending message
    idFrom As Long      ' Identifier of control sending message
    code  As Long       ' Specifies the notification code
End Type

Public Type POINTAPI    ' pt
    x As Long
    y As Long
End Type

Public Type RECT        ' rct
    Left As Long
    Top As Long
    Right As Long
    Bottom As Long
End Type

Declare Function SendMessage Lib "user32" Alias "SendMessageA" _
                            (ByVal hWnd As Long, _
                            ByVal wMsg As Long, _
                            ByVal wParam As Long, _
                            lParam As Any) As Long

Public Const WM_USER = &H400

Declare Function CreateWindowEx Lib "user32" Alias "CreateWindowExA" _
                            (ByVal dwExStyle As Long, ByVal lpClassName As String, _
                             ByVal lpWindowName As String, ByVal dwStyle As Long, _
                             ByVal x As Long, ByVal y As Long, _
                             ByVal nWidth As Long, ByVal nHeight As Long, _
                             ByVal hwndParent As Long, ByVal hMenu As Long, _
                             ByVal hInstance As Long, lpParam As Any) As Long

Declare Function DestroyWindow Lib "user32" (ByVal hWnd As Long) As Long

Declare Sub MoveMemory Lib "kernel32" Alias "RtlMoveMemory" (pDest As Any, pSource As Any, ByVal dwLength As Long)

' Returns the low-order word from the given 32-bit value.
Public Function LOWORD(dwValue As Long) As Integer
    MoveMemory LOWORD, dwValue, 2
End Function

' Returns the larger of the two passed params
Public Function Max(param1 As Long, param2 As Long) As Long
    If param1 > param2 Then Max = param1 Else Max = param2
End Function

Public Function GetStrFromBufferA(szA As String) As String
    If InStr(szA, vbNullChar) Then
        GetStrFromBufferA = Left$(szA, InStr(szA, vbNullChar) - 1)
    Else
        ' If sz had no null char, the Left$ function. Above would rtn a zero length string ("").
        GetStrFromBufferA = szA
    End If
End Function

Sub Main()
    On Error GoTo ErrHdl
    ogCommandLine = Command()

    Set frmSplash.AATLoginControl1.UfisComCtrl = frmSplash.UfisCom1
    frmSplash.AATLoginControl1.VersionString = "4.5.0.6"
    frmSplash.AATLoginControl1.InfoCaption = "Info about AbsencePlanning"
    frmSplash.AATLoginControl1.InfoButtonVisible = True
    frmSplash.AATLoginControl1.InfoUfisVersion = "UFIS Version 4.5"
    frmSplash.AATLoginControl1.InfoAppVersion = CStr("AbsencePlanning 4.5.0.6")
    frmSplash.AATLoginControl1.InfoCopyright = "� 2001-2005 UFIS Airport Solutions GmbH"
    frmSplash.AATLoginControl1.InfoAAT = "UFIS Airport Solutions GmbH"
    frmSplash.AATLoginControl1.UserNameLCase = True

    If ogCommandLine = "" Then
        'we don't start the absence-planning from another application
        'init the login-control
        frmSplash.AATLoginControl1.ApplicationName = "AbsPlan"
        frmSplash.AATLoginControl1.LoginAttempts = 3
        frmSplash.AATLoginControl1.RegisterApplicationString = "AbsPlan,InitModu,InitModu,Initialisieren (InitModu),B,-" + _
                ",OnlyOwnDetailedWindow,m_OnlyOwnDetailedWindow,Action,A,1" + _
                ",OnlyOwnGroup,m_OnlyOwnGroup,Action,A,1"

        'do the login
        strRetLogin = frmSplash.AATLoginControl1.ShowLoginDialog

        'look after the login result
        If strRetLogin = "CANCEL" Or strRetLogin = "ERROR" Then
            End
        ElseIf strRetLogin = "OK" Then
            ogActualUser = frmSplash.AATLoginControl1.GetUserName
        End If
    Else
        ' if we start from another application, we have to do a silent login
        ' without showing the login-dialog
        ogActualUser = GetItem(ogCommandLine, 2, "@")
        ogActualUserURNO = GetItem(ogCommandLine, 4, "@")
        ogActualUserPassword = GetItem(ogCommandLine, 3, "@")

'        ogActualUser = "AbsPlanDefault" '"test" '
'        ogActualUserPassword = "Passwort"
'        ogActualUserURNO = 29458316
'        ogActualUserURNO = 29460225
'        ogActualUser = "rro_test"
'        ogActualUserURNO = 29156513   'Heidi Albrecht,      PENO = 106056
'        ogActualUserPassword = "Password"
'        ogActualUserURNO = "29459141"
'        ogActualUserURNO = "209560604" 'Mark Zenger
'        ogActualUserURNO = "29463309" 'Moeri Ursula

        strRetLogin = frmSplash.AATLoginControl1.DoLoginSilentMode(ogActualUser, ogActualUserPassword)
        If strRetLogin <> "OK" Then
            MsgBox "Login from " & GetItem(ogCommandLine, 1, "@") & " for user " & _
                ogActualUser & " failed." & vbCrLf & "Last error message: " & strRetLogin, _
                vbCritical, "Login failed"
            End
        End If
    End If
    Globals.ogStartTime = Now()

    DoEvents
    ogSystemMetrics.GetMetrics

    frmSplash.Show
    frmSplash.Refresh

    Set fMainForm = New frmMain
    Load fMainForm

    frmSplash.Visible = False
    frmSplash.Hide

    fMainForm.Show
    
    'init the combo with the names of the loaded views
    If frmSplash.AATLoginControl1.GetPrivileges("m_OnlyOwnGroup") <> 1 Then
        fMainForm.mnuFileNew.Enabled = False
        fMainForm.mnuFileClose.Enabled = False
        fMainForm.mnuFilePrint.Enabled = False
        fMainForm.mnuViewEditor.Enabled = False
        fMainForm.mnuWindow.Enabled = False
        fMainForm.tbToolBar.Buttons(1).Enabled = False
        fMainForm.tbToolBar.Buttons(5).Enabled = False
        fMainForm.tbToolBar.Buttons(7).Enabled = False
        fMainForm.cmbViews.Enabled = False

        Dim olEmployee As clsEmployee
        If DoesKeyExist(gcEmployee, ogActualUserURNO) = True Then
            Set olEmployee = gcEmployee.Item(ogActualUserURNO)
            ogActualEmployeeNameFirst = olEmployee.EmployeeFirstName
            ogActualEmployeeNameSecond = olEmployee.EmployeeSecondName
            ogActualEmployeeURNO = ogActualUserURNO
            ogActualEmployeePENO = olEmployee.EmployeePENO
            frmHiddenServerConnection.LoadDRR (Year(Now))
            frmDetailWindow.Show 'vbModal
        End If
    End If

    Exit Sub
ErrHdl:
    'WriteErrToLog "c:\tmp\AbsPlanErr.txt", "Module1.Main", Err
    WriteErrToLog UFIS_TMP & "\AbsPlanErr.txt", "Module1.Main", Err
    Err.Clear
    Resume Next
End Sub

Sub LoadResStrings(frm As Form)
    On Error Resume Next

    Dim ctl As Control
    Dim obj As Object

    Dim sCtlType As String
    Dim nVal As Integer

    'set the form's caption
    frm.Caption = LoadResString(CInt(frm.Tag))

    For Each ctl In frm.Controls
        sCtlType = TypeName(ctl)
        If sCtlType = "Label" Then
            ctl.Caption = LoadResString(CInt(ctl.Tag))
        ElseIf sCtlType = "Menu" Then
            ctl.Caption = LoadResString(CInt(ctl.Caption))
        ElseIf sCtlType = "TabStrip" Then
            For Each obj In ctl.Tabs
                obj.Caption = LoadResString(CInt(obj.Tag))
                obj.ToolTipText = LoadResString(CInt(obj.ToolTipText))
            Next
        ElseIf sCtlType = "Toolbar" Then
            For Each obj In ctl.Buttons
                obj.ToolTipText = LoadResString(CInt(obj.ToolTipText))
            Next
        ElseIf sCtlType = "ListView" Then
            For Each obj In ctl.ColumnHeaders
                obj.Text = LoadResString(CInt(obj.Tag))
            Next
        Else
            nVal = 0
            nVal = Val(ctl.Tag)
            If nVal > 0 Then ctl.Caption = LoadResString(nVal)
            nVal = 0
            nVal = Val(ctl.ToolTipText)
            If nVal > 0 Then ctl.ToolTipText = LoadResString(nVal)
        End If
    Next
End Sub

Sub Sleep(Sekunden%)
    Dim AktuelleZeit As Variant
    AktuelleZeit = Timer
    Do
        DoEvents
    Loop Until Timer - AktuelleZeit > Sekunden%
End Sub
