VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{A2F31E92-C74F-11D3-A251-00500437F607}#1.0#0"; "ufiscom.ocx"
Object = "{7BDE1363-DF9A-4D96-A2A0-4E85E0191F0F}#1.0#0"; "AatLogin.ocx"
Begin VB.Form frmSplash 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Initializing application"
   ClientHeight    =   4650
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6270
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   310
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   418
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Visible         =   0   'False
   Begin AATLOGINLib.AatLogin AATLoginControl1 
      Height          =   1305
      Left            =   4230
      TabIndex        =   2
      Top             =   645
      Visible         =   0   'False
      Width           =   1380
      _Version        =   65536
      _ExtentX        =   2434
      _ExtentY        =   2302
      _StockProps     =   0
   End
   Begin UFISCOMLib.UfisCom UfisCom1 
      Left            =   3480
      Top             =   1680
      _Version        =   65536
      _ExtentX        =   873
      _ExtentY        =   1085
      _StockProps     =   0
   End
   Begin VB.ListBox List1 
      Height          =   3375
      Left            =   120
      TabIndex        =   1
      Top             =   120
      Width           =   6015
   End
   Begin MSComctlLib.ProgressBar ProgressBar1 
      Height          =   615
      Left            =   120
      TabIndex        =   0
      Top             =   3840
      Width           =   6015
      _ExtentX        =   10610
      _ExtentY        =   1085
      _Version        =   393216
      Appearance      =   1
      Max             =   21
   End
End
Attribute VB_Name = "frmSplash"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'Private Sub AATLoginControl1_LoginResult(strLoginResult As String)
'    Module1.strRetLogin = strLoginResult
'End Sub

Private Sub Form_Load()
    LoadResStrings Me
    'lblVersion.Caption = "Version " & App.Major & "." & App.Minor & "." & App.Revision
    'lblProductName.Caption = App.Title
End Sub
