// ChoiceDlg.cpp : implementation file
//

#include <stdafx.h>
#include <StaffBalance.h>
#include <ChoiceDlg.h>
#include <StaffBalanceDlg.h>
#include <CCSGlobl.h>
#include <Ufis.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

//***************************************************************************
//* apo, 22.03.2001															*
//* defines for sending broadcasts (see SendBC)								*
//***************************************************************************
#define MAX_BC_SELECTION_SIZE	2048
#define MAX_BC_FIELDS			2048
#define MAX_BC_DATA				2048
#define MAX_BC_TW_SIZE			36
//************************************************************************************
// end of change (apo 22.03.2001)
//************************************************************************************


/////////////////////////////////////////////////////////////////////////////
// CChoiceDlg dialog


CChoiceDlg::CChoiceDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CChoiceDlg::IDD, pParent)
{
CCS_TRY
	//{{AFX_DATA_INIT(CChoiceDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	pomEmplList = NULL;
CCS_CATCH_ALL
}

CChoiceDlg::~CChoiceDlg()
{
CCS_TRY
	if (pomEmplList != NULL) delete pomEmplList;
	pomEmplList = NULL;
	
	omStfData.DeleteAll();

CCS_CATCH_ALL
}


void CChoiceDlg::DoDataExchange(CDataExchange* pDX)
{
CCS_TRY
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CChoiceDlg)
	DDX_Control(pDX, IDC_SHOW_EMPLOYEE, m_B_Filter);
	DDX_Control(pDX, IDC_DATETIMEPICKER, m_DateTimePicker);
	DDX_Control(pDX, IDC_COMBO_ORG, m_C_Orgeinheit);
	//}}AFX_DATA_MAP
CCS_CATCH_ALL
}


BEGIN_MESSAGE_MAP(CChoiceDlg, CDialog)
	//{{AFX_MSG_MAP(CChoiceDlg)
	ON_BN_CLICKED(IDC_SELECT_ALL_EMPLOYEES, OnSelectAllEmployees)
	ON_BN_CLICKED(IDC_SHOW_EMPLOYEE, OnShowEmployee)
	ON_BN_CLICKED(IDC_FIND_EMPLOYEE, OnFindEmployee)
	ON_BN_CLICKED(IDC_DESELECT_ALL_EMPLOYEES, OnDeselectAllEmployees)
	ON_CBN_SELCHANGE(IDC_COMBO_ORG, OnSelchangeComboOrg)
	ON_NOTIFY(DTN_DATETIMECHANGE, IDC_DATETIMEPICKER, OnDatetimechangeDatetimepicker)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CChoiceDlg message handlers

void CChoiceDlg::OnOK() 
{
CCS_TRY
	COleDateTime olDate;
	// TODO: Add extra validation here
	LONG ilUrno = GetUrno();
	if (!ilUrno)
		return;

	m_DateTimePicker.GetTime(olDate);
	if(olDate == COleDateTime::invalid)
		return;

	CStaffBalanceDlg dlg;
	dlg.m_Urno = ilUrno;
	dlg.m_Date = olDate;
	dlg.m_Name = GetName();

//************************************************************************************
// apo 22.03.2001
// ToDo UHI: 
// 1.) ersten Parameter von SendBC() dynamisch (aus CEDA.INI) machen
// 2.) Parameter UREF in <olParam> einbauen
// 3.) Start- und Endzeit in Parameter String einbauen (<olDate> ist zu Testzwecken beides)
// 4.) sicherstellen, dass SendBC() nur mit g�ltigen Parametern aufgerufen wird (z.B. olDate)
// 5.) Antwort von SendBC() auswerten (wenn gew�nscht)
//************************************************************************************
	CString olUrno,olParam,olCmd,olURef,strError;
	long lRefUrno;
	COleDateTime olStart, olEnd;
	olEnd = olDate;
	olStart.SetDate(olDate.GetYear(), olDate.GetMonth(), 1);
	olCmd = pcgScript;
	lRefUrno = ogBasicData.GetNextUrno();
	olURef.Format("%i", lRefUrno);
	dlg.m_RefUrno = olURef;
	if ((ilUrno > 0) && (lRefUrno > 0)  && (olCmd != "DEFAULT") && (olStart.GetStatus() == COleDateTime::valid) && (olEnd.GetStatus() == COleDateTime::valid))
	{
		// attention: last char in urno list must be delimiter (it's a bug)
		olUrno.Format("%d,",ilUrno);
		// make field value string
		olParam.Format("%s,%s,%s,%s",olStart.Format("%Y%m%d"),olEnd.Format("%Y%m%d"),olURef,olCmd);
		SendBC(olCmd,olUrno,"FROM,TO,UREF,CMD",olParam);
	}
	else{
		strError.Format(LoadStg(IDS_STRING2923), pcgScript);
		MessageBox(strError,LoadStg(IDS_STRING2918),MB_ICONEXCLAMATION);
	}

//************************************************************************************
// end of change (apo 22.03.2001)
//************************************************************************************

	int nResponse = dlg.DoModal();
	if (nResponse == IDOK)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with OK
	}
	else if (nResponse == IDCANCEL)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with Cancel
	}
	//CDialog::OnOK();
CCS_CATCH_ALL
}

BOOL CChoiceDlg::OnInitDialog() 
{
CCS_TRY
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here

	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu = LoadStg(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	
	SetDlgItemText(IDC_STATIC_ORG,LoadStg(IDS_ORG));
	SetDlgItemText(IDC_STATIC_DATE,LoadStg(IDS_DATE));
	SetDlgItemText(IDC_STATIC_EMPLOYEE,	LoadStg(IDS_MITARBEITER));
	SetDlgItemText(IDC_SHOW_EMPLOYEE,		LoadStg(IDS_FILTER));
	SetDlgItemText(IDC_SELECT_ALL_EMPLOYEES,CString("&") + LoadStg(IDS_ALLE));
	SetDlgItemText(IDC_DESELECT_ALL_EMPLOYEES,	LoadStg(IDS_KEINE));
	SetDlgItemText(IDC_FIND_EMPLOYEE,		LoadStg(IDS_SUCHEN));
	SetDlgItemText(IDOK,LoadStg(IDS_OK));
	SetDlgItemText(IDCANCEL,	LoadStg(IDS_ABBRECHEN));

	SetWindowText(LoadStg(IDS_STAFFBALANCE));

	IniGrid();
	// initialize Organisation unit
	m_C_Orgeinheit.SetFont(&ogCourier_Regular_10);
	FillOrgCombo();

	GetEmployee();

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
CCS_CATCH_ALL
	return TRUE;
}

void CChoiceDlg::IniGrid()
{
CCS_TRY
	// create the grid
	pomEmplList = new CGridControl (this, IDC_EMPL_GRID,EMPL_COLCOUNT,0);
	pomEmplList->Initialize();

	// allways show vertical scroll bar 
	pomEmplList->SetScrollBarMode(SB_BOTH,gxnAutomatic);

	// set the headlines
	pomEmplList->SetValueRange(CGXRange(0,1), LoadStg(SHIFT_STF_PERC));
	pomEmplList->SetValueRange(CGXRange(0,2), LoadStg(SHIFT_STF_NAME));
	pomEmplList->SetValueRange(CGXRange(0,3), LoadStg(SHIFT_STF_PENO));

	// setup the column widths
	InitEmployeeColWidths ();
		
	pomEmplList->Initialize();
	pomEmplList->GetParam()->EnableUndo(FALSE);
	pomEmplList->LockUpdate(TRUE);
	pomEmplList->LockUpdate(FALSE);
	pomEmplList->GetParam()->EnableUndo(TRUE);

	//Spaltenbreite und Zeilenh�he k�nnen nicht ver�ndert werden
	pomEmplList->GetParam()->EnableTrackColWidth(FALSE);
	pomEmplList->GetParam()->EnableTrackRowHeight(FALSE);
    pomEmplList->GetParam()->EnableSelection(GX_SELROW);
	
	/*// don't allow the user to change column widths
	pomEmplList->GetParam()->EnableTrackColWidth(FALSE);
	// prevent single cell selection, only whole lines can be selected
	pomEmplList->GetParam()->EnableSelection(GX_SELNONE);*/

	pomEmplList->SetStyleRange( CGXRange().SetTable(), CGXStyle()
				.SetVerticalAlignment(DT_VCENTER)
				.SetReadOnly(TRUE)
				.SetControl(GX_IDS_CTRL_STATIC));

	/*pomEmplList->SetStyleRange(CGXRange().SetTable(),CGXStyle()
				.SetVerticalAlignment(DT_VCENTER)
				.SetReadOnly(TRUE));*/

	pomEmplList->EnableAutoGrow ( FALSE );
CCS_CATCH_ALL
}

void CChoiceDlg::FillOrgCombo()
{
CCS_TRY
	CString olLine;
	// Eine leeren Zeile einf�gen, f�r alle MA's
	olLine.Format("           %-40s     URNO=ALL_EMPLOYEE", LoadStg(IDS_ALLE));
	m_C_Orgeinheit.AddString(olLine);
	// Eine Zeile einf�gen, f�r alle MA's die keine Funktion haben
	olLine.Format("--------   %-40s     URNO=NO_ORGCODE", LoadStg(IDS_OHNEORGCODE));
	m_C_Orgeinheit.AddString(olLine);


	RecordSet *polRecord;
	polRecord = new RecordSet(ogBCD.GetFieldCount("ORG"));

	int ilDpt1Idx = ogBCD.GetFieldIndex("ORG","DPT1");
	int ilDptnIdx = ogBCD.GetFieldIndex("ORG","DPTN");
	int ilUrnoIdx = ogBCD.GetFieldIndex("ORG","URNO");

	for(int i = 0; i < ogBCD.GetDataCount("ORG"); i++)
	{
		ogBCD.GetRecord("ORG",i, *polRecord);
		olLine.Format("%-8s   %-40s     URNO=%s", polRecord->Values[ilDpt1Idx], polRecord->Values[ilDptnIdx], polRecord->Values[ilUrnoIdx]);
		m_C_Orgeinheit.AddString(olLine);
	}
	delete polRecord;

	// default selection: all employees
	m_C_Orgeinheit.SetCurSel(0);
CCS_CATCH_ALL	
}

void CChoiceDlg::InitEmployeeColWidths()
{
CCS_TRY
	CString csTest;
	int ilFactor = 13;

	pomEmplList->SetColWidth ( 0, 0, 27);

	csTest = LoadStg(SHIFT_STF_PERC);
	pomEmplList->SetColWidth ( 1, 1, 80);

	csTest = LoadStg(SHIFT_STF_NAME);
	pomEmplList->SetColWidth ( 2, 2, 178);

	csTest = LoadStg(SHIFT_STF_PENO);
	pomEmplList->SetColWidth ( 3, 3, 143);
CCS_CATCH_ALL
}

void CChoiceDlg::FillGrid()
{
CCS_TRY
	CString		csFullName,csUrno;
	long		ilUrno;
	
	int iRows = pomEmplList->GetRowCount();
	if (iRows >= 1)
		pomEmplList->RemoveRows(1, iRows);


	pomEmplList->SetRowCount(omStfData.GetSize());
	pomEmplList->GetParam()->SetLockReadOnly(false);


	// read all staff member from the global object, which is initialized in 
	// CInitializeAccountApp::InitialLoad()
	for ( int i=0; i<omStfData.GetSize(); i++ )
	{
		// read the urno
		ilUrno = omStfData[i].Urno;
		csUrno.Format("%ld",omStfData[i].Urno);
		// connect urno with first cell of each line
		pomEmplList->SetStyleRange ( CGXRange(i+1,1), CGXStyle().SetItemDataPtr( (void*)ilUrno ) );

		pomEmplList->SetValueRange(CGXRange(i+1,1), omStfData[i].Perc );
		// get surname and lastname
		csFullName = omStfData[i].Lanm;
		csFullName += ",";
		csFullName += omStfData[i].Finm;
		pomEmplList->SetValueRange(CGXRange(i+1,2), csFullName);
		// get the staff number
		pomEmplList->SetValueRange(CGXRange(i+1,3), omStfData[i].Peno);
	}
	pomEmplList->GetParam()->SetLockReadOnly(false);
	pomEmplList->Redraw();
	SortGrid(2);
	pomEmplList->SetScrollBarMode(SB_VERT,gxnAutomatic);

CCS_CATCH_ALL
}

bool CChoiceDlg::SortGrid(int ipRow)
{
CCS_TRY
	CGXSortInfoArray  sortInfo;
	sortInfo.SetSize( 1 );
	sortInfo[0].sortOrder = CGXSortInfo::ascending;
	sortInfo[0].nRC = ipRow;                       
	sortInfo[0].sortType = CGXSortInfo::autodetect;  
	pomEmplList->SortRows( CGXRange().SetRows(1, omStfData.GetSize()), sortInfo); 

	return true;
CCS_CATCH_ALL
	return true;
}

void CChoiceDlg::OnSelectAllEmployees() 
{
CCS_TRY
	POSITION area = pomEmplList->GetParam( )->GetRangeList( )->AddTail(new CGXRange);
	pomEmplList->SetSelection(area,1, 1, omStfData.GetSize(),EMPL_COLCOUNT);
CCS_CATCH_ALL
}

void CChoiceDlg::GetEmployee()
{
CCS_TRY
	CString olSelection, olOrgCode, olTmp, olStfs;
	m_C_Orgeinheit.GetWindowText(olSelection);
	COleDateTime olStart; //, olEnd;
	// employees to show in grid
	CMapStringToPtr olStfUrnoMap;	
	void  *prlVoid = NULL;
	// array of org-codes needed by GetSorByOrgWithTime() (see later on)
	// holds only one org-code at a time here
	CStringArray olOrgCodes; 
	
	//m_DateTimePicker.GetTime(olEnd);
	//olStart.SetDate(olEnd.GetYear(), 1, 1);

	//uhi 2.5.01
	m_DateTimePicker.GetTime(olStart);

	TRACE("Start: %s\n",olStart.Format("%d.%m.%Y"));
	//TRACE("Ende: %s\n",olEnd.Format("%d.%m.%Y"));

	if(olSelection.GetLength() <= 0)
	{
		MessageBeep(MB_OK);
		return;
	}

	// show wait cursor
	AfxGetApp()->DoWaitCursor(1);

	// extract code from ORG-combo selection
	olOrgCode = olSelection.Left(8);
	olOrgCode.TrimRight();
	olOrgCodes.Add(olOrgCode);

	if(olOrgCode == "--------")
	{
		// show all employees without org code (no entry in SORTAB)
		CCSPtrArray<STFDATA> olStfData; 
		// alle Mitarbeiter ohne Organisation im angegebenen Zeitraum ermitteln
		olStfs = ogSorData.GetStfWithoutOrgWithTime(olStart, olStart, &olStfData);
		// Anzahl der Datens�tze
		int ilStfSize = olStfData.GetSize();
		// alle Datens�tze durchgehen
		for(int i=0; i<ilStfSize; i++)
		{
			// Mitarbeiter-Urno ermitteln...
			olTmp.Format("%ld",olStfData[i].Urno);
			// ...und in Array speichern
			olStfUrnoMap.SetAt(olTmp,NULL); //Alle MA's ohne Organisation aus den Stammdaten STF
		}
		olStfData.RemoveAll();
	}
	else if(olOrgCode.GetLength() > 0)
	{
		// show only employees with the selected ORG-code
		CCSPtrArray<SORDATA> olSorData;
		// alle Mitarbeiter ermitteln, die die passenden Organisationen haben
		olStfs = ogSorData.GetSorByOrgWithTime(&olOrgCodes, olStart, olStart, &olSorData);
		// Anzahl der Datens�tze
		int ilSorSize = olSorData.GetSize();
		// alle Datens�tze durchgehen
		for(int i=0; i<ilSorSize; i++)
		{
			// Mitarbeiter-Urno ermitteln...
			olTmp.Format("%ld",olSorData[i].Surn);
			// ...und in Array speichern
			olStfUrnoMap.SetAt(olTmp,NULL); //Alle MA's mit der richtigen Organisation aus den Stammdaten SOR
		}
		olSorData.RemoveAll();

	}
	else
	{
		// show all employees
		int ilStfSize = ogStfData.omData.GetSize();
		// alle Datens�tze durchgehen
		for(int i=0; i<ilStfSize; i++)
		{
			// Mitarbeiter-Urno ermitteln...
			olTmp.Format("%ld",ogStfData.omData[i].Urno);
			// ...und in Array speichern
			olStfUrnoMap.SetAt(olTmp,NULL); //Alle MA's ohne Organisation aus den Stammdaten STF
		}
	}

	CString	olUrno;
	int ilUrno = 0;
	omStfData.DeleteAll();
	for (int i=0; i<ogStfData.omData.GetSize(); i++ )
	{
		ilUrno = ogStfData.omData[i].Urno;
		olUrno.Format("%ld",ogStfData.omData[i].Urno);
		// Mitarbeiter vorhanden?
		if (olStfUrnoMap.Lookup(olUrno,(void *&)prlVoid))
		{
			STFDATA *prlStf = new STFDATA;
			*prlStf = ogStfData.omData[i];
			omStfData.Add(prlStf);
		}
	}
	olStfUrnoMap.RemoveAll();
	
	// re-fill employee grid
	FillGrid();
	// reset button color
	m_B_Filter.ChangeColor();
	// restore normal cursor
	AfxGetApp()->DoWaitCursor(-1);
CCS_CATCH_ALL
}


void CChoiceDlg::OnShowEmployee() 
{
CCS_TRY
	GetEmployee();
CCS_CATCH_ALL
}

void CChoiceDlg::OnFindEmployee() 
{
CCS_TRY
	if (pomEmplList	!= NULL)
		pomEmplList->OnShowFindReplaceDialog(TRUE);
CCS_CATCH_ALL
}

void CChoiceDlg::OnDeselectAllEmployees() 
{
CCS_TRY
	pomEmplList->SetSelection(NULL,0, 0, 0,0);
CCS_CATCH_ALL
}

void CChoiceDlg::OnSelchangeComboOrg() 
{
CCS_TRY
	// TODO: Add your control notification handler code here
	m_B_Filter.ChangeColor(RED,BLACK,MAROON,RED); 
CCS_CATCH_ALL
}

long CChoiceDlg::GetUrno()
{
CCS_TRY
	long ilUrno;
	CGXStyle style;
	ROWCOL nRow, nCol;
	if (pomEmplList->GetCurrentCell(nRow, nCol)){
		pomEmplList->ComposeStyleRowCol(nRow, 1, &style );
		// Urno erhalten
		ilUrno = (long)style.GetItemDataPtr();
		if ( ilUrno )
			return ilUrno;	
		else
			return 0;

	}
	else
		return 0;
CCS_CATCH_ALL
	return 0;
}


CString CChoiceDlg::GetName()
{
CCS_TRY
	CString olName;
	ROWCOL nRow, nCol;
	if (pomEmplList->GetCurrentCell(nRow, nCol)){
		olName = pomEmplList->GetValueRowCol(nRow,2);
		return olName;
	}
	else
		return CString("");
CCS_CATCH_ALL
	return CString("");

}

//*********************************************************************************************************************
// apo, 22.03.2001															
// SendBC: send a broadcast with cmd = XBS (eXecute Basic Script).
// Input:	CString <opCmd>		-> the command which is sended as bc's twstart-param
//			CString <opData>	-> the urno list, sended as bc's selection-param
//			CString <opAdditionalInfoFields>
//								-> list of field names of additional inf parameters which
//								   is sended as bc's fields-param
//			CString <opAdditionalInfoData>
//								-> list of field values of <opAdditionalInfoFields> which
//								   is sended as bc's data-param
// Return:	TRUE	-> everything is ok
//			FALSE	-> error
//*********************************************************************************************************************

bool CChoiceDlg::SendBC(CString opCmd, CString opData, CString opAdditionalInfoFields,
						CString opAdditionalInfoData)
{
    static char cReq_id[22];     
    static char cUser[12];					// normally CEDA
    static char cWks[12] = "StaffBal";		// workstation, here appl. name
    static char cAction[12] = "XBS"; 
    static char cTable[512] = "";			// table Name (multiple table are allowed)
    static char cSortClause[512] = "";		// sort Clause    
    static char cTws[MAX_BC_TW_SIZE] = "0";	// time Window Start 
    static char cTwe[MAX_BC_TW_SIZE];		// time Window end
	// selection criterium, normally a WHERE clause
    static char cSelection[MAX_BC_SELECTION_SIZE];
	static char cFields[MAX_BC_FIELDS] = "";// DB field, normally database fields
	static char cData[MAX_BC_DATA] = "";	// response buffer 
    static char cDataDest[12] = "RETURN";	// RETURN or BUF1 .. BUF4
	int iLastReturnCode;
	CString strError;

	// user specific info
	strcpy(cUser,pcgUser);
	strcpy(cTwe,pcgAppName);
	
	// check command length
	if (opCmd.GetLength() >= MAX_BC_TW_SIZE)
	{
		// command is too long -> do error message and terminate
		MessageBox(LoadStg(IDS_STRING2921),LoadStg(IDS_STRING2920),MB_ICONEXCLAMATION);
		return false;
	}
	// copy command
	strcpy(cTws,opCmd);

	// check length of data string
	ASSERT(opData.GetLength() <= MAX_BC_SELECTION_SIZE);
	// copy the data
	strcpy(cSelection,opData.GetBuffer(0));

	// check length of additional info field list
	ASSERT(opAdditionalInfoFields.GetLength() <= MAX_BC_FIELDS);
	// copy additional info to the broadcasts fieldlist
	strcpy(cFields,opAdditionalInfoFields.GetBuffer(0));
	// check length of additional info data
	ASSERT(opAdditionalInfoFields.GetLength() <= MAX_BC_DATA);
	// copy additional info to the broadcasts data buffer
	strcpy(cData,opAdditionalInfoData.GetBuffer(0));

	// in case the response takes time
	AfxGetApp()->DoWaitCursor(1);
	if ((iLastReturnCode = ::CallCeda(cReq_id,cUser,cWks,cAction,cTable,cSortClause,
									  cTws,cTwe,cSelection,cFields,cData,cDataDest)) != 0)
	{
		MessageBeep((UINT)-1);
		strError.Format(LoadStg(IDS_STRING2919),cData);
		MessageBox(strError,LoadStg(IDS_STRING2918),MB_ICONEXCLAMATION);
		return false;
	}
	// switch off the wait cursor
	AfxGetApp()->DoWaitCursor(-1);

	// check the return buffer for errors
	if ((*cData != '\0') && (strcmp(cData,"OK")))
	{
		// an error occurred -> show message
		MessageBeep((UINT)-1);
		strError.Format(LoadStg(IDS_STRING2922),cData);
		MessageBox(strError,LoadStg(IDS_STRING2918),MB_ICONEXCLAMATION);
	}
	return true;
}
//************************************************************************************
// end of change (apo 22.03.2001)
//************************************************************************************


void CChoiceDlg::OnDatetimechangeDatetimepicker(NMHDR* pNMHDR, LRESULT* pResult) 
{
CCS_TRY
	// TODO: Add your control notification handler code here
	m_B_Filter.ChangeColor(RED,BLACK,MAROON,RED); 
CCS_CATCH_ALL
}
