// GridControl.cpp: implementation of the CGridControl class.
//
//////////////////////////////////////////////////////////////////////

#include <stdafx.h>

#include <ccsglobl.h>
//#include "utilities.h"
#include <GridControl.h>
#include <BasicData.h>
//#include "BudgetDataDlg.h"
//#include "CedaBudData.h"
//#include "CedaUbuData.h"
//#include "CedaOrgData.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
//	Klasse f�r ein Grid innerhalb eines Dialogs, 
//  Baseclass:	BCGXGridWnd
//////////////////////////////////////////////////////////////////////


BEGIN_MESSAGE_MAP(CGridControl, CGXGridWnd)
	//{{AFX_MSG_MAP(CGridControl)
	ON_WM_LBUTTONUP()
	ON_WM_LBUTTONDOWN()
//	ON_WM_KILLFOCUS()
	//}}AFX_MSG_MAP
	// Standard printing commands
END_MESSAGE_MAP()


CGridControl::CGridControl()
{
	bmAutoGrow = true;
	bmSortEnabled = true;
	bmSortAscend = true;
	memset ( &smLButtonDblClick, 0, sizeof(smLButtonDblClick) );
	bmIsDirty = false;
	bmIsDnDEnabled = false;
	pomToolTipArray = NULL;
}

CGridControl::CGridControl ( CWnd *popParent, UINT nID, ROWCOL nCols, ROWCOL nRows )
{
	umID = nID;
	pomWnd = popParent;
	SubclassDlgItem ( umID, pomWnd );
	Initialize();
	SetParent ( pomWnd );
	GetParam()->EnableUndo(FALSE);
	LockUpdate(TRUE);
	SetRowCount(nRows);
	SetColCount(nCols);
	LockUpdate(FALSE);
	GetParam()->EnableUndo(TRUE);
	bmAutoGrow = true;
	bmSortEnabled = true;
	bmSortAscend = true;
	GetParam()->EnableTrackRowHeight(FALSE);
	memset ( &smLButtonDblClick, 0, sizeof(smLButtonDblClick) );
	bmIsDirty = false;
	bmIsDnDEnabled = false;
	pomToolTipArray = NULL;
}

CGridControl::~CGridControl()
{

}

BOOL CGridControl::SetValue ( ROWCOL nRow, ROWCOL nCol, CString &cStr ) 
{
	return SetValueRange( CGXRange(nRow, nCol), cStr );
}


const CString& CGridControl::GetValue ( ROWCOL nRow, ROWCOL nCol ) 
{
	return GetValueRowCol ( nRow, nCol );
}

BOOL CGridControl::EnableGrid ( BOOL enable )
{
	BOOL blRet;

	int ilRowCount = GetRowCount();
	int ilColCount = GetColCount();

	if (ilRowCount >= 1 && ilColCount >= 1)
	{
		if ( enable )
			blRet = SetStyleRange(CGXRange(1, 1, ilRowCount, ilColCount), CGXStyle().SetInterior(ogColors[WHITE_IDX]).SetEnabled(TRUE));
		else
			blRet = SetStyleRange(CGXRange(1, 1, ilRowCount, ilColCount), CGXStyle().SetInterior(ogColors[SILVER_IDX]).SetEnabled(FALSE));
	}
	Redraw();
	return blRet;
}

void CGridControl::EnableAutoGrow ( bool enable /*=true*/ )
{
	bmAutoGrow = enable;
}

BOOL CGridControl::OnEndEditing( ROWCOL nRow, ROWCOL nCol) 
{
	BOOL blRet = CGXGridWnd::OnEndEditing( nRow, nCol) ;
	DoAutoGrow ( nRow, nCol) ;
	CGXControl* polControl;
	if ( polControl = GetControl( nRow, nCol) )
		bmIsDirty |= ( polControl->GetModify () == TRUE );
	return blRet;
}

void CGridControl::DoAutoGrow ( ROWCOL nRow, ROWCOL nCol) 
{
	ROWCOL ilRowCount = GetRowCount ();
	if ( bmAutoGrow &&( nRow >= ilRowCount ) )
	{	//  es wird bereits in letzter Zeile editiert, d.h. event. Grid verl�ngern
		if ( ! GetValueRowCol ( nRow, nCol).IsEmpty () )
			InsertBottomRow ();
	}
}

BOOL CGridControl::InsertBottomRow ()
{
	ROWCOL ilRowCount = GetRowCount ();
	//LockUpdate(TRUE);
	BOOL blRet = InsertRows ( ilRowCount+1, 1 );
	//CopyStyleLastLineFromPrev ();
	//LockUpdate(FALSE);
	return blRet;
}

void CGridControl::CopyStyleOfLine ( ROWCOL ipSourceLine, ROWCOL ipDestLine,
									 bool bpResetValues/*=true*/ )
{
	ROWCOL ilColCount = GetColCount ();
	CGXStyle olStyle;

	for ( ROWCOL i=0; i<=ilColCount; i++ )
	{
		ComposeStyleRowCol( ipSourceLine, i, &olStyle );
		if ( bpResetValues )
		{
			olStyle.SetIncludeItemDataPtr ( FALSE );
			if ( i>0 )
				olStyle.SetInterior(WHITE);
		}
		SetStyleRange  ( CGXRange ( ipDestLine, i), olStyle );
		if ( bpResetValues )
		{
			SetValueRange ( CGXRange ( ipDestLine, i), "" );
		}
	}
}

//**************************************************************************************************
// 
//**************************************************************************************************

void CGridControl::CopyStyleLastLineFromPrev ( bool bpResetValues/*=true*/ )
{
	ROWCOL ilRowCount = GetRowCount ();
	CopyStyleOfLine ( ilRowCount-1, ilRowCount, bpResetValues );
}

//**************************************************************************************************
// 
//**************************************************************************************************

BOOL CGridControl::OnRButtonDblClkRowCol(ROWCOL nRow, ROWCOL nCol, UINT nFlags, CPoint pt)
{
	BOOL ok = CGXGridWnd::OnRButtonDblClkRowCol(nRow, nCol, nFlags, pt);
	
	int		*ilWidths;
	ROWCOL  ilRowAnz = GetColCount ();
	
	if ( ilWidths = new int[ilRowAnz+1] )
	{
		for ( ROWCOL i=0; i<=ilRowAnz; i++ )
			ilWidths[i] = GetColWidth(i);

 		delete ilWidths;
	}
	return ok;
}

//**************************************************************************************************
// 
//**************************************************************************************************

void CGridControl::OnLButtonUp (UINT nFlags, CPoint point) 
{
	CGXGridWnd::OnLButtonUp(nFlags, point);

    ROWCOL  Row;
	ROWCOL  Col;

	if ( !bmSortEnabled )
		return;
	//--- Betroffene Zelle ermitten
    if ( HitTest ( point, &Row,  &Col, NULL ) != GX_HEADERHIT )
		return;

	//--- Check, ob ung�ltige Spaltennummer
	if ( ( Col > GetColCount( ) ) || ( Col== 0 ) )
		return;
	SortTable ( Row, Col );
}

//**************************************************************************************************
// Such die Datenpointer der ersten Zeile nach der �bergebenden Urno ab.
// R�ckgabe: Row in der die Urno gefunden wurde
//**************************************************************************************************

int CGridControl::FindRowByUrnoDataPointer(long lpUrno)
{
	CGXStyle		olStyle;
	// Alle Rows durchgehen
	for (ROWCOL i=0; i < GetRowCount();i++)
	{
		// Style und damit Datenpointer einer Zelle erhalten
		ComposeStyleRowCol(i+1,1, &olStyle );

		if (lpUrno == (long) olStyle.GetItemDataPtr())
			return i+1;
	}

	// Nicht gefunden.
	return -1;
}

//**************************************************************************************************
// Von ADO: Sendet ATM ein Drag Begin Event wenn DnD eingeschaltet ist !
//**************************************************************************************************

void CGridControl::OnLButtonDown (UINT nFlags, CPoint point) 
{
	ROWCOL Row;
	ROWCOL Col;

	// Betroffene Zelle ermitten
    int ilHitState = HitTest ( point, &Row,  &Col, NULL );

	// Check, ob ung�ltige Spaltennummer
	if ((Col>GetColCount()) || (Row>GetRowCount()))
		return;

	// Info Struktur f�llen
	GRIDNOTIFY rlNotify;
	rlNotify.idc = umID;
	rlNotify.row = Row;
	rlNotify.col = Col;
	rlNotify.headerrows = GetHeaderRows();
	rlNotify.headercols = GetHeaderCols();
	rlNotify.point = point;
	rlNotify.source = this;

	if (bmIsDnDEnabled)
	{
		// DnD ist nur aus Datenzeilen erlaubt
		if ( (Col==0) || (Row==0) )
			return;

		// "Nutzwert" in die Info Struktur einf�gen
		if (GetRowCount() >= 1)
			rlNotify.value1 = GetValue(Row,1);

		CGXStyle		olStyle;
		// Style und damit Datenpointer einer Zelle erhalten
		ComposeStyleRowCol(Row,1, &olStyle );
		// Urno erhalten
		rlNotify.value2 = olStyle.GetItemDataPtr();
		// -> DnD, andere Message senden
		if (ilHitState==GX_CELLHIT)
		{
			// Message senden
			if (pomWnd )
			{
				// Set Selection
				// Bisherige Auswahl l�schen.
				SetSelection(NULL);
				// neue Auswahl markieren
				POSITION area = GetParam( )->GetRangeList( )->AddTail(new CGXRange);
				SetSelection(area,Row,1,Row,GetColCount());
				pomWnd->SendMessage(WM_GRID_DRAGBEGIN,nFlags,(LPARAM)&rlNotify );
			}
		}
	}
	else
	{
		// -> Kein DnD, normale Funktion
		CGXGridWnd::OnLButtonDown(nFlags, point);
	
		// CheckBox-Event?
		const CGXStyle& olStyle = LookupStyleRowCol(Row,Col);
		if (olStyle.GetControl() == GX_IDS_CTRL_CHECKBOX3D)
		{
			// yo -> toggle checkbox
			if (GetValueRowCol(Row,Col) == "0")
				SetValueRange(CGXRange(Row,Col), "1");
			else
				SetValueRange(CGXRange(Row,Col), "0");
		}
	
		pomWnd->SendMessage ( WM_GRID_LBUTTONDOWN, nFlags, (LPARAM)&rlNotify );
	}
}

//**************************************************************************************************
// 
//**************************************************************************************************

bool CGridControl::SortTable ( ROWCOL ipRowClicked, ROWCOL ipColClicked )
{
	ROWCOL ilRowCount = GetRowCount ();
	ROWCOL ilColCount = GetColCount ();

	if ( ipRowClicked != 0 )
		return false;

	// Warte Mouse
	AfxGetApp()->DoWaitCursor(1);
	
	//--- Sortieren je nachdem welcher Header gedr�ckt wurde
	CGXSortInfoArray  sortInfo;
	sortInfo.SetSize( 1 );
		
	// toggle between sorting in ascending / descending order with each click
	bmSortAscend = !bmSortAscend;
	sortInfo[0].sortOrder = bmSortAscend ? CGXSortInfo::ascending : 
										   CGXSortInfo::descending;
	sortInfo[0].nRC = ipColClicked;                       
	sortInfo[0].sortType = CGXSortInfo::autodetect;  
	if ( bmAutoGrow && GetValueRowCol ( ilRowCount, 1 ).IsEmpty () )
		ilRowCount--;
	SortRows( CGXRange().SetRows(1, ilRowCount), sortInfo); 

	// Warte Mouse
	AfxGetApp()->DoWaitCursor(-1);
	
	return true;
}

//**************************************************************************************************
// 
//**************************************************************************************************

void CGridControl::EnableSorting ( bool enable/*=true*/ )
{
	bmSortEnabled = enable;
}

//**************************************************************************************************
// 
//**************************************************************************************************

void CGridControl::SetLbDblClickAction ( UINT ipMsg, WPARAM ipWparam, bool bpOnlyInside/*=true*/ )
{
	smLButtonDblClick.bOnlyInside = bpOnlyInside;
	smLButtonDblClick.iMsg = ipMsg;
	smLButtonDblClick.iWparam = ipWparam;
}

//**************************************************************************************************
// 
//**************************************************************************************************

BOOL CGridControl::OnLButtonDblClkRowCol(ROWCOL nRow, ROWCOL nCol, UINT nFlags, CPoint pt)
{
	/*if ( smLButtonDblClick.iMsg )
	{
		pomWnd->PostMessage ( smLButtonDblClick.iMsg, smLButtonDblClick.iWparam, 0L );
		//return TRUE;
	}
	else
	{
		GRIDNOTIFY rlNotify;
		rlNotify.idc = umID;
		rlNotify.row = nRow;
		rlNotify.col = nCol;
		rlNotify.headerrows = GetHeaderRows();
		rlNotify.headercols = GetHeaderCols();
		rlNotify.point = pt;
		pomWnd->SendMessage ( WM_GRID_LBUTTONDBLCLK, nFlags, (LPARAM)&rlNotify );
		//return TRUE;
	}*/
	
	/*long ilUrno;
	CGXStyle style;
	ROWCOL row;
	CString		csUrno, csMonth, csYear, csOrgUnits, csStar;

	if (umID == IDC_DATA_GRID)
		return true;

	ComposeStyleRowCol(nRow, 1, &style );
	// Urno erhalten
	ilUrno = (long)style.GetItemDataPtr();
	if ( ilUrno ){
		CBudgetDataDlg dlg(ilUrno);
		if(dlg.DoModal() == IDOK){
			row = FindRowByUrnoDataPointer(ilUrno);
			if (row != -1){
				GetParam()->SetLockReadOnly(false);

				BUDDATA *pBud = ogBudData.GetBudByUrno(ilUrno);
				//Stichtag
				SetValueRange(CGXRange(row,1), pBud->Dayx);
				//von Jahr/Monat
				csMonth = pBud->Frmo;
				csMonth = csMonth.Right(2);
				csYear = pBud->Frmo;
				csYear = csYear.Left(4);
				if (csMonth.GetLength())
					SetValueRange(CGXRange(row,2), csMonth + csYear); //"/" + csYear);
				//bis Jahr/Monat
				csMonth = pBud->Tomo;
				csMonth = csMonth.Right(2);
				csYear = pBud->Tomo;
				csYear = csYear.Left(4);
				if (csMonth.GetLength())
					SetValueRange(CGXRange(row,3), csMonth + csYear); //"/" + csYear);
				//Headcount
				SetValueRange(CGXRange(row,4), pBud->Hdco);
				//Mutation
				SetValueRange(CGXRange(row,5), pBud->Muta);
				//Forecast
				SetValueRange(CGXRange(row,6), pBud->Foca);
				//Budget
				SetValueRange(CGXRange(row,7), pBud->Budg);
	
				csOrgUnits = "";
				//Org. Units
				for (int m=0; m<ogUbuData.omData.GetSize(); m++){
					if (ogUbuData.omData[m].Ubud == ilUrno){
						csOrgUnits = csOrgUnits + ogUbuData.omData[m].Dpt1;
						csStar = ogUbuData.omData[m].Star;
						if ( csStar == '1')
							csOrgUnits = csOrgUnits + "*";
						csOrgUnits = csOrgUnits + ",";
					}
				}
		
				if (csOrgUnits.Right(1) == ',')
					csOrgUnits = csOrgUnits.Left(csOrgUnits.GetLength() - 1);

				SetValueRange(CGXRange(row,8), csOrgUnits);
		
				//Remarks
				SetValueRange(CGXRange(row,9), pBud->Rema);
				
				GetParam()->SetLockReadOnly(true);
			}
		}

	}

	//pomWnd->SendMessage (WM_LBUTTONDBLCLK);*/
	
	return true;

}

//**************************************************************************************************
// 
//**************************************************************************************************

BOOL CGridControl::OnLButtonClickedRowCol(ROWCOL nRow, ROWCOL nCol, UINT nFlags, CPoint pt)
{
	CGXGridWnd::OnLButtonClickedRowCol(nRow,nCol,nFlags,pt);

	//SelectRange(CGXRange( ).SetTable(), FALSE);
	//SelectRange(CGXRange( ).SetRows(nRow), TRUE);

	GRIDNOTIFY rlNotify;
	rlNotify.idc = umID;
	rlNotify.row = nRow;
	rlNotify.col = nCol;
	rlNotify.headerrows = GetHeaderRows();
	rlNotify.headercols = GetHeaderCols();
	rlNotify.point = pt;
	pomWnd->SendMessage ( WM_GRID_LBUTTONCLK, nFlags, (LPARAM)&rlNotify );
	return TRUE;
}

//**************************************************************************************************
// 
//**************************************************************************************************

void CGridControl::OnTextNotFound(LPCTSTR)
{
	ROWCOL		ilRow, ilCol, ilColAnz;
	BOOL		blFound = FALSE;
	ilColAnz = GetColCount();
	GX_FR_STATE	*pslState = GXGetLastFRState();

	if ( GetCurrentCell( &ilRow, &ilCol ) && pslState )
	{
		if ( pslState->bNext )		//  d.h Abw�rts suchen
		{
			while ( !blFound && ( ilCol < ilColAnz ) ) //  und noch nicht in letzter Spalte
			{
				ilCol ++;
				ilRow = GetHeaderRows ()+1;
				blFound = FindText( ilRow, ilCol, true ) ;
			}
		}
		else 
		{	//  d.h Aufw�rts suchen
			while ( !blFound &&  ( ilCol > GetHeaderCols () + 1 ) )	
			{
				ilCol --;
				ilRow = GetRowCount ();
				blFound = FindText( ilRow, ilCol, true );
			}
		}
		if ( blFound )
			return;

	}
	MessageBeep(0);
}

//**************************************************************************************************
// 
//**************************************************************************************************

void CGridControl::SetDirtyFlag ( bool dirty/*=true*/ )
{
	bmIsDirty = dirty;

}

//**************************************************************************************************
// 
//**************************************************************************************************

bool CGridControl::IsGridDirty ()
{
	return bmIsDirty ;
}

//**************************************************************************************************
// 
//**************************************************************************************************

BOOL CGridControl::OnDeleteCell(ROWCOL nRow, ROWCOL nCol)
{
	CGXControl	*polControl;
	CString		olWert;
	BOOL		ilRet = FALSE;
	ROWCOL		ilFrozenRows = GetFrozenRows();
	ROWCOL		ilFrozenCols = GetFrozenCols(); 

	bool blfrozen = ( nRow <= ilFrozenRows ) || ( nCol <= ilFrozenCols ) ;

	//  event. Selection auf Row- und Columnheadern entfernen
	SelectRange( CGXRange ().SetRows(0,ilFrozenRows), FALSE );
	SelectRange( CGXRange ().SetCols(0,ilFrozenCols), FALSE );

	//  nur f�r Zellen innerhalb des Grid, alten Wert merken
	if ( !blfrozen )
	{
		polControl = GetControl(nRow, nCol);
		if ( !polControl  || !polControl->GetValue(olWert) )
			olWert = "XXX";
		ilRet = CGXGridWnd::OnDeleteCell(nRow, nCol);
	}
	bmIsDirty |= ( olWert.IsEmpty()==FALSE) ;
	return ilRet&&!blfrozen;
}

// ein Bug in CGXGridCore - m_SelRectId bleibt leer nach SetSelection() - das knallt irgendwann!
void CGridControl::SetSelection(POSITION pos, ROWCOL nTop, ROWCOL nLeft, ROWCOL nBottom, ROWCOL nRight)
{
	CGXGridWnd::SetSelection(pos, nTop, nLeft, nBottom, nRight);
	m_SelRectId = pos;
}

//////////////////////////////////////////////////////////////////////
//	Klasse f�r ein Grid mit �berschrift
//  Baseclass:	CGridControl
//////////////////////////////////////////////////////////////////////

CTitleGridControl::CTitleGridControl()
{
}


CTitleGridControl::CTitleGridControl(CWnd *popParent, UINT nID, ROWCOL nCols, ROWCOL nRows )
	:CGridControl ( popParent, nID, nCols, nRows+1 )
{
	
	// Assign a combo box to a cell
	BOOL ok ;
	CGXStyle style;
	
	SetStyleRange(CGXRange(0,0), 
				  CGXStyle().SetControl(GX_IDS_CTRL_HEADER)  );
	ok = SetFrozenRows ( 1,1 );
	ChangeRowHeaderStyle ( CGXStyle().SetValue("")  );
	ok = SetCoveredCellsRowCol( 0, 0, 0, nCols );
	SetRowHeadersText ();
}

CTitleGridControl::~CTitleGridControl()
{
}


void CTitleGridControl::SetTitle ( CString &opTitle )
{	
	omTitle = opTitle;
	SetValueRange( CGXRange(0,0), omTitle );
}

CString CTitleGridControl::GetTitle ()
{
	return omTitle ;
}

const CString &CTitleGridControl::GetValue ( ROWCOL nRow, ROWCOL nCol ) 
{
	return GetValue ( nRow+1, nCol );	
}

BOOL CTitleGridControl::SetValue ( ROWCOL nRow, ROWCOL nCol, CString &cStr ) 
{
	return SetValueRange( CGXRange(nRow+1, nCol), cStr );
}	


BOOL CTitleGridControl::EnableGrid ( BOOL enable )
{
	BOOL blRet;
	
	int ilRowCount = GetRowCount();
	int ilColCount = GetColCount();
	
	if (ilRowCount >= 2 && ilColCount >= 1)
	{
		if ( enable )
			blRet = SetStyleRange(CGXRange(2, 1, ilRowCount, ilColCount), CGXStyle().SetInterior(ogColors[WHITE_IDX]).SetEnabled(enable));
		else
		{
			SetCurrentCell( 1, 0  ) ;
			blRet = SetStyleRange(CGXRange(2, 1, ilRowCount, ilColCount), CGXStyle().SetInterior(ogColors[SILVER_IDX]).SetEnabled(enable));
		}
		//blRet &= SetStyleRange(CGXRange().SetTable(), CGXStyle().SetEnabled(enable));
			
	}
	Redraw();
	return blRet;
}


BOOL CTitleGridControl::InsertBottomRow ()
{
	BOOL blRet = CGridControl::InsertBottomRow ();
	char pclString[11];
	ROWCOL ilRowCount = GetRowCount ();

	SetValue ( ilRowCount-1, 0, (CString)itoa(ilRowCount-1,pclString,10) ) ;

	return blRet;
}


bool CTitleGridControl::SortTable ( ROWCOL ipRowClicked, ROWCOL ipColClicked )
{
	ROWCOL ilRowCount = GetRowCount ();

	if ( ipRowClicked != 1 )
		return false;

	//--- Sortieren je nachdem welcher Header gedr�ckt wurde
	CGXSortInfoArray  sortInfo;
	sortInfo.SetSize( 1 );
		
	// toggle between sorting in ascending / descending order with each click
	bmSortAscend = !bmSortAscend;
	sortInfo[0].sortOrder = bmSortAscend ? CGXSortInfo::ascending : 
										   CGXSortInfo::descending;
	sortInfo[0].nRC = ipColClicked;                       
	sortInfo[0].sortType = CGXSortInfo::autodetect;  
	if ( bmAutoGrow && GetValueRowCol ( ilRowCount, 1 ).IsEmpty () )
		ilRowCount--;
	if ( ilRowCount > 2 )
		SortRows( CGXRange().SetRows(2, ilRowCount), sortInfo); 
	SetRowHeadersText ();
	return true;
}

//  Zeilek�pfe neu beschriften
void CTitleGridControl::SetRowHeadersText ()
{
	char pclString[11];
	for ( UINT i=1; i<GetRowCount(); i++ )
		SetValue ( i, 0, (CString)itoa(i,pclString,10) ) ;
}

void CTitleGridControl::RemoveOneRow ( ROWCOL nRow )
{
	RemoveRows( nRow, nRow );
	SetRowHeadersText ();
}

BOOL CTitleGridControl::OnStartEditing(ROWCOL nRow, ROWCOL nCol)
{
	if ( (nRow<2) || (nCol<1) )
	{
		TRACE("User tried to edit in Row=%u, Col=%u\n", nRow, nCol );
		return FALSE;
	}
	else
		return CGridControl::OnStartEditing(nRow, nCol);
}


void CGridControl::SetColCheckbox(ROWCOL nCol)
{
	SetStyleRange(CGXRange( ).SetCols(nCol), CGXStyle()
		.SetControl(GX_IDS_CTRL_CHECKBOX3D));
}

int CGridControl::GetColWidth(ROWCOL nCol)
{
	int nWidth = CGXGridCore::GetColWidth(nCol);
   // Make the last column fill the rest of the window so that
   // no empty space is visible.
	if (!IsPrinting() && nCol == GetColCount()){
		CRect rect = GetGridRect();
		nCol = GetClientCol(nCol);
        if (nCol > 0)
			nWidth = max(nWidth, rect.Width() - CalcSumOfClientColWidths(0, nCol-1) - 1);
    }
	return nWidth;
}

BOOL CGridControl::OnRButtonClickedRowCol(ROWCOL nRow, ROWCOL nCol, UINT nFlags, CPoint pt)
{	
	CGXGridWnd::OnRButtonClickedRowCol(nRow, nCol, nFlags, pt);
	
	//Zeile markieren
	//SelectRange(CGXRange( ).SetTable( ), FALSE);
	//SelectRange(CGXRange( ).SetRows(nRow, nRow), TRUE);
	
	//pomWnd->SendMessage (WM_RBUTTONDOWN);
	
	/*GRIDNOTIFY rlNotify;
	rlNotify.idc = umID;
	rlNotify.row = 0;
	rlNotify.col = 0;
	rlNotify.headerrows = GetHeaderRows();
	rlNotify.headercols = GetHeaderCols();
	rlNotify.point = pt;
	rlNotify.source = this;
	pomWnd->SendMessage ( WM_RBUTTONDOWN, nFlags, (LPARAM)&rlNotify );*/
	
	//Popup-Men� anzeigen
/*	CMenu menu;
	long ilUrno;	
	CGXStyle style;

	if (umID == IDC_ORG_GRID){
		VERIFY(menu.LoadMenu(IDR_MENU1));
		CMenu* pPopup = menu.GetSubMenu( 0 );
		ASSERT( pPopup != NULL );
    
		CWnd* pWndPopupOwner = AfxGetMainWnd( ); // display the menuClientToScreen(&pt);

		if (pPopup != NULL)
		{
		
			pPopup->ModifyMenu(0,MF_BYPOSITION|MF_STRING,ID_ROW_NEW,LoadStg(IDS_STRING1011));
			pPopup->ModifyMenu(1,MF_BYPOSITION|MF_STRING,ID_ROW_EDIT,LoadStg(IDS_STRING1012));
			pPopup->ModifyMenu(2,MF_BYPOSITION|MF_STRING,ID_ROW_COPY,LoadStg(IDS_STRING1030));
			pPopup->ModifyMenu(3,MF_BYPOSITION|MF_STRING,ID_ROW_DELETE,LoadStg(IDS_STRING1013));
			pPopup->ModifyMenu(4,MF_BYPOSITION|MF_STRING,ID_ROW_CALCULATE,LoadStg(IDS_STRING1014));
			ComposeStyleRowCol(nRow, 1, &style );
			// Urno erhalten
			ilUrno = (long)style.GetItemDataPtr();
			if ( ilUrno ){
				pPopup->EnableMenuItem(ID_ROW_EDIT, MF_ENABLED);
				pPopup->EnableMenuItem(ID_ROW_COPY, MF_ENABLED);
				pPopup->EnableMenuItem(ID_ROW_DELETE, MF_ENABLED);
				pPopup->EnableMenuItem(ID_ROW_CALCULATE, MF_ENABLED);
			}	
			else {
				pPopup->EnableMenuItem(ID_ROW_EDIT, MF_DISABLED||MF_GRAYED);
				pPopup->EnableMenuItem(ID_ROW_COPY, MF_DISABLED||MF_GRAYED);
				pPopup->EnableMenuItem(ID_ROW_DELETE, MF_DISABLED||MF_GRAYED);
				pPopup->EnableMenuItem(ID_ROW_CALCULATE, MF_DISABLED||MF_GRAYED);
			}
			pPopup->TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON,pt.x + 125, pt.y + 125,
								   pWndPopupOwner);
		}
	}
	else{
		if (umID == IDC_DATA_GRID){
			VERIFY(menu.LoadMenu(IDR_MENU2));
			CMenu* pPopup = menu.GetSubMenu( 0 );
			ASSERT( pPopup != NULL );
    
			//CWnd* pWndPopupOwner = AfxGetMainWnd( ); // display the menuClientToScreen(&pt);
			CWnd* pWndPopupOwner = GetActiveWindow();

			if (pPopup != NULL)
			{
				pPopup->ModifyMenu(0,MF_BYPOSITION|MF_STRING,ID_ORG_DELETE,LoadStg(IDS_STRING1013));
				ComposeStyleRowCol(nRow, 1, &style );
				// Urno erhalten
				//ilUrno = (long)style.GetItemDataPtr();
				//if ( ilUrno )
					pPopup->EnableMenuItem(ID_ORG_DELETE, MF_ENABLED);
				//else
				//	pPopup->EnableMenuItem(ID_ORG_DELETE, MF_DISABLED||MF_GRAYED);
				
				pPopup->TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON,pt.x+220, pt.y+300,
					                   pWndPopupOwner);	
			}
		}
	}*/
	return true;
}

