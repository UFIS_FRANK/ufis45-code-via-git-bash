#if !defined(_DUTYSTATTABVIEWER_H_INCLUDED_)
#define _DUTYSTATTABVIEWER_H_INCLUDED_

#include <stdafx.h>
#include <CViewer.h>
#include <CCSDynTable.h>
//#include "CCSPtrArray.h"
#include <DutyRoster_View.h>
#include <Accounts.h>
#include <afxtempl.h>
#include <Groups.h>

/////////////////////////////////////////////////////////////////////////////
// DutyStatTabViewer



/////////////////////////////////////////////////////////////////////////////
class DutyStatTabViewer : public CViewer
{
// Constructions
public:
    DutyStatTabViewer(CWnd* pParent, CAccounts *popAccounts);
    ~DutyStatTabViewer();
	DutyRoster_View *pomParent;

    void Attach(CCSDynTable *popAttachWnd);
    virtual void ChangeView(bool bpSetHNull = true, bool bpSetVNull = true);

	// Funktionen zur Behandlung von Broadcasts
	// DRR_NEW und DRR_DELETE
	void ProcessDrrChange(DRRDATA *popDataPointer, int ipType);
	void ProcessAccChange(ACCDATA *popDataPointer, int ipType);
	void ProcessRelaccBc();

// Internal data processing routines
private:
	void InitTableArrays(int ipColumnOffset = 0);
	void MakeTableData(int ipColumnOffset,int ipRowOffset);
	void AddRow(int ipRow, int ipColumns, int ipColumnOffset, int ipRowOffset);
	void DelRow(int ipRow);
	void AddColumn(int ipColumn, int ipColumnOffset, int ipRowOffset);
	void DelColumn(int ipColumn);
	void GenerateFieldValues(int ipStfNr, CString opPType, int ipPTypeNr, int ipColumn, int ipRow, FIELDDATA *popFieldData);
	bool GetElementAndPType(int ipRowNr, int &ipRow, CString &opPType, int &ipPTypNr);

// Operations
public:

// Window refreshing routines
public:
	void HorzTableScroll(MODIFYTAB *prpModiTab);
	void VertTableScroll(MODIFYTAB *prpModiTab);
	void TableSizing(/*UINT nSide, */LPRECT lpRect);
	void MoveTable(MODIFYTAB *prpModiTab);
	void InlineUpdate(INLINEDATA *prpInlineUpdate);


// Attributes used for filtering condition
protected:
	// Objekt zur Berechnung der Arbeitszeitkonten der Mitarbeiter
	CAccounts *pomAccounts;
	// Liste mit den Konten aus denen sich Tempor�re-Konten zusammensetzen.
	CStringList omExtraAccounts;

// Attributes
private:
    CCSDynTable *pomDynTab;

	int imAccounts;
	
	VIEWINFO *prmViewInfo;
	CCSPtrArray<STFDATA> *pomStfData;
	//CCSPtrArray<GROUPSTRUCT> *pomGroups;
	CGroups *pomGroups;

	CStringArray omPTypes;
	CMapPtrToPtr omKontoMap;
// Methods which handle changes (from Data Distributor)
public:
	TABLEDATA *prmTableData;

private:
	bool IsOleBetween(COleDateTime opDate,COleDateTime opFrom,COleDateTime opTo);
};

inline bool DutyStatTabViewer::IsOleBetween(COleDateTime opDate,COleDateTime opFrom,COleDateTime opTo) 
{
	bool blRet = false;
	
	switch(opFrom.GetStatus())
	{
	case COleDateTime::valid:
		if(opTo.GetStatus()==COleDateTime::valid)
		{
			if(opDate>=opFrom && opDate<=opTo)
			{
				blRet = true;
			}
			else
			{
				blRet =  false;
			}
		}
		else if(opTo.GetStatus()==COleDateTime::invalid)
		{
			blRet = false;
		}
		else
			blRet = true;
		break;
	case COleDateTime::invalid:
		blRet = false;
		break;
	default:
		blRet = false;
		break;
	}
	return blRet;
}

#endif //_DUTYSTATTABVIEWER_H_INCLUDED_
