#ifndef AFX_DUTYPLANABSENCEPP_H__E3451C54_8E79_11D2_8E3E_0000C002916B__INCLUDED_
#define AFX_DUTYPLANABSENCEPP_H__E3451C54_8E79_11D2_8E3E_0000C002916B__INCLUDED_

// DutyPlanAbsencePp.h : Header-Datei
//
#include <stdafx.h>

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld DutyPlanAbsencePp 

class DutyPlanAbsencePp : public CPropertyPage
{
	DECLARE_DYNCREATE(DutyPlanAbsencePp)

// Konstruktion
public:
	DutyPlanAbsencePp();
	~DutyPlanAbsencePp();

	void InitValues(long lpStfUrno, CWnd* pParentWnd);

	DutyRoster_View *pomParent;


	bool IsRMSCodeOk(CString olCode);

	long lmStfUrno;

// Dialogfelddaten
	//{{AFX_DATA(DutyPlanAbsencePp)
	enum { IDD = IDD_DUTY_PLANABSENCE_PP };
	CButton	omDelUPType;
	CButton	omDelActivePType;
	CButton	omRBRegFree;
	CButton	omRBWeekendFree;
	CButton	omCheckActivePType;
	CButton	omCheckUPType;
	CButton	m_B_RmsViewH;
	CButton	m_B_Insert;
	CCSEdit	m_From;
	CCSEdit	m_To;
	CCSEdit	m_RMSCodeH;
	CString	m_EfromDate;
	CString	m_Reas;
	CString	m_Arc1H;
	CString	m_EtoDate;
	CString	m_Atfr;
	CString	m_Atto;
	//}}AFX_DATA

// Überschreibungen
	// Der Klassen-Assistent generiert virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(DutyPlanAbsencePp)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	//}}AFX_VIRTUAL

// Implementierung
protected:
	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(DutyPlanAbsencePp)
	virtual BOOL OnInitDialog();
	virtual void OnCancel();
	virtual void OnOK();
	afx_msg void OnRFausgleich();
	afx_msg void OnBRmsviewH();
	afx_msg void OnBStart();
	afx_msg void OnDelUpType();
	afx_msg void OnDelActivePType();
	afx_msg void OnCheckActivePType();
	afx_msg void OnCheckUpType();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:
	//setting all labels
	void SetLabels();

	// Oberflächenelemente aktivieren / deaktivieren
	void EnableAbsenceControls(bool bpActivate);
	void EnableOtherAbsencesControls(bool bpActivate);

	// ruft den Dialog zur Auswahl einer Abwesenheit auf
	void DoSelectOda(CCSEdit *popCCSEdit);

	// prüfen, ob der Tag <opDay> dem Mitarbeiter als Urlaubstag anzurechnen ist
	bool IsRegularFreeOrHoliday(COleDateTime opDay, long lpStfUrno, ODADATA *popOda);
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio fügt zusätzliche Deklarationen unmittelbar vor der vorhergehenden Zeile ein.

#endif // AFX_DUTYPLANABSENCEPP_H__E3451C54_8E79_11D2_8E3E_0000C002916B__INCLUDED_
