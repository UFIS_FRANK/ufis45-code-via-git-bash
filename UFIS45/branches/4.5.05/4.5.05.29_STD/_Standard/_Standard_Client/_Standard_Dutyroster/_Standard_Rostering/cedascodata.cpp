// CedaScoData.cpp
 
#include <stdafx.h>

CedaScoData ogScoData;

void ProcessScoCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//static int CompareTimes(const SCODATA **e1, const SCODATA **e2);
static int CompareSurnAndTimes(const SCODATA **e1, const SCODATA **e2);

//--CEDADATA-----------------------------------------------------------------------------------------------

CedaScoData::CedaScoData() : CedaData(&ogCommHandler)
{
	// Create an array of CEDARECINFO for SCODataStruct
	BEGIN_CEDARECINFO(SCODATA,SCODataRecInfo)
		FIELD_LONG		(Urno,"URNO")
		FIELD_LONG		(Surn,"SURN")
		FIELD_CHAR_TRIM	(Code,"CODE")
		FIELD_OLEDATE	(Vpfr,"VPFR")
		FIELD_OLEDATE	(Vpto,"VPTO")
		FIELD_CHAR_TRIM	(Cweh,"CWEH")
	END_CEDARECINFO //(SCODataStruct)

	// FIELD_LONG, FIELD_DATE 
	// Copy the record structure
	for (int i=0; i< sizeof(SCODataRecInfo)/sizeof(SCODataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&SCODataRecInfo[i],sizeof(CEDARECINFO));
		omRecInfo.Add(prpCedaRecInfo);
	} // end for

	// initialize field names
	strcpy(pcmTableName,"SCO");
	strcat(pcmTableName,pcgTableExt);
	strcpy(pcmListOfFields,"URNO,SURN,CODE,VPFR,VPTO,CWEH");
	pcmFieldList = pcmListOfFields; //because in CedaData pcmFieldList is just a pointer

	omData.RemoveAll();
}

//---------------------------------------------------------------------------------------------------------

void CedaScoData::GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType)
{

}

//--REGISTER----------------------------------------------------------------------------------------------

void CedaScoData::Register(void)
{
	DdxRegister((void *)this,BC_SCO_CHANGE,	"SCODATA", "Sco-changed",	ProcessScoCf);
	DdxRegister((void *)this,BC_SCO_NEW,	"SCODATA", "Sco-new",		ProcessScoCf);
	DdxRegister((void *)this,BC_SCO_DELETE,	"SCODATA", "Sco-deleted",	ProcessScoCf);
}

//--~CEDADATA----------------------------------------------------------------------------------------------

CedaScoData::~CedaScoData(void)
{
	TRACE("CedaScoData::~CedaScoData called\n");
	omRecInfo.DeleteAll();
	ClearAll();
}

//--CLEAR-ALL----------------------------------------------------------------------------------------------

void CedaScoData::ClearAll(bool bpWithRegistration)
{
	TRACE("CedaScoData::ClearAll called\n");
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(bpWithRegistration)
	{
		ogDdx.UnRegister(this,NOTUSED);
	}
}

//--READ-ALL-----------------------------------------------------------------------------------------------

bool CedaScoData::Read(char *pspWhere /*NULL*/)
{
    // Select data from the database
	bool ilRc = true;
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
		
	if(IsFullLoggingEnabled())
	{
		ogRosteringLogText.Format("CedaAction:\n  Cmd <RT>\n  Tbl <%s>\n  Sel <%s>",pcmTableName, pspWhere);
		WriteInRosteringLog();
	}

	if(pspWhere == NULL)
	{	
		ilRc = CedaAction2("RT");
	}
	else
	{
		ilRc = CedaAction2("RT", pspWhere);
	}
	if (ilRc != true)
	{
		TRACE("Read-Sco: Ceda-Error %d \n",ilRc);
		return ilRc;
	}
	// Load data from CedaData into the dynamic array of record
	for (int ilCountRecord = 0; ilRc == true; ilCountRecord++)
	{
		SCODATA *prlSco = new SCODATA;
		if ((ilRc = GetFirstBufferRecord2(prlSco)) == true)
		{
			// ARE: Vpto kann auch mal null sein, es ist OK! aber auf keinen Fall ung�ltig
			if(IsValidSco(prlSco))
			{
				omData.Add(prlSco);//Update omData
				omUrnoMap.SetAt((void *)prlSco->Urno,prlSco);
#ifdef TRACE_FULL_DATA
				// Datensatz OK, loggen if FULL
				ogRosteringLogText.Format(" read %8.8lu %s OK:  ", ilCountRecord, pcmTableName);
				GetDataFormatted(ogRosteringLogText, prlSco);
				WriteLogFull("");
#endif TRACE_FULL_DATA
			}
			else
			{
				delete prlSco;
			}
		}
		else
		{
			delete prlSco;
		}
	}
	TRACE("Read-Sco: %d gelesen\n",ilCountRecord-1);
	ClearFastSocketBuffer();	
		
	if(IsFullLoggingEnabled())
	{
		ogRosteringLogText.Format("CedaAction:\n  read %d records from table %s",ilCountRecord-1, pcmTableName);
		WriteInRosteringLog();
	}

	// wir sortieren alles nach Surn & Time - das beschleunigt das Lesen zigfach
	omData.Sort(CompareSurnAndTimes);

    return true;
}

//--INSERT-------------------------------------------------------------------------------------------------

bool CedaScoData::Insert(SCODATA *prpSco)
{
	prpSco->IsChanged = DATA_NEW;
	if(Save(prpSco) == false) return false; //Update Database
	InsertInternal(prpSco);
    return true;
}

//--INSERT-INTERNAL--------------------------------------------------------------------------------------

bool CedaScoData::InsertInternal(SCODATA *prpSco)
{
	ogDdx.DataChanged((void *)this, SCO_NEW,(void *)prpSco ); //Update Viewer
	omData.Add(prpSco);//Update omData
	// wir sortieren alles nach Surn & Time - das beschleunigt das Lesen zigfach
	omData.Sort(CompareSurnAndTimes);

	omUrnoMap.SetAt((void *)prpSco->Urno,prpSco);
    return true;
}

//--DELETE-------------------------------------------------------------------------------------------------

bool CedaScoData::Delete(long lpUrno)
{
	SCODATA *prlSco = GetScoByUrno(lpUrno);
	if (prlSco != NULL)
	{
		prlSco->IsChanged = DATA_DELETED;
		if(Save(prlSco) == false) return false; //Update Database
		DeleteInternal(prlSco);
	}
    return true;
}

//--DELETE-INTERNAL----------------------------------------------------------------------------------------

bool CedaScoData::DeleteInternal(SCODATA *prpSco)
{
	ogDdx.DataChanged((void *)this,SCO_DELETE,(void *)prpSco); //Update Viewer
	omUrnoMap.RemoveKey((void *)prpSco->Urno);
	int ilScoCount = omData.GetSize();
	for (int ilCountRecord = 0; ilCountRecord < ilScoCount; ilCountRecord++)
	{
		if (omData[ilCountRecord].Urno == prpSco->Urno)
		{
			omData.DeleteAt(ilCountRecord);//Update omData
			break;
		}
	}
    return true;
}

//--UPDATE-------------------------------------------------------------------------------------------------

bool CedaScoData::Update(SCODATA *prpSco)
{
	if (GetScoByUrno(prpSco->Urno) != NULL)
	{
		if (prpSco->IsChanged == DATA_UNCHANGED)
		{
			prpSco->IsChanged = DATA_CHANGED;
		}
		if(Save(prpSco) == false) return false; //Update Database
		UpdateInternal(prpSco);
	}
    return true;
}

//--UPDATE-INTERNAL--------------------------------------------------------------------------------------------

bool CedaScoData::UpdateInternal(SCODATA *prpSco)
{
	SCODATA *prlSco = GetScoByUrno(prpSco->Urno);
	if (prlSco != NULL)
	{
		*prlSco = *prpSco; //Update omData
		ogDdx.DataChanged((void *)this,SCO_CHANGE,(void *)prlSco); //Update Viewer
	}
    return true;
}

//--GET-BY-URNO--------------------------------------------------------------------------------------------

SCODATA *CedaScoData::GetScoByUrno(long lpUrno)
{
	SCODATA  *prlSco;
	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlSco) == TRUE)
	{
		return prlSco;
	}
	return NULL;
}

int CedaScoData::GetScoListBySurnWithTime(long lpSurn,COleDateTime opTime,CCSPtrArray<SCODATA> *popScoData)
{
	return GetScoListBySurnWithTime(lpSurn,opTime,opTime,popScoData);
}

/*******************************************************************************************
erstellt eine Liste mit allen SCO f�r angegebenen lpSurn innerhalb angegebener Zeit opStart-opEnd

*******************************************************************************************/
int CedaScoData::GetScoListBySurnWithTime(long lpSurn,COleDateTime opStart,COleDateTime opEnd,CCSPtrArray<SCODATA> *popScoData)
{
	if(opStart.GetStatus() != COleDateTime::valid || opEnd.GetStatus() != COleDateTime::valid) return 0;

	SCODATA* prlSco = 0;

	int ilCount = FindFirstOfSurn(lpSurn);
	if(ilCount == -1) return 0;	// lpSurn nicht gefunden

	COleDateTime olStart,olEnd;

	for(; ilCount < omData.GetSize(); ilCount++)
	{
		prlSco = (SCODATA*)omData.CPtrArray::GetAt(ilCount);

		if(prlSco->Surn > lpSurn) 
			break;		// omData ist ja nach Surn-Vpfr sortiert
		
		if(prlSco->Vpfr.GetStatus() != COleDateTime::valid) 
			continue;	// 1. Defekter Datensatz
		
		if(prlSco->Vpfr > opEnd) 
			break;	// 2. Anfang der G�ltigkeitszeit liegt nach dem opEnd, kein Datensatz mehr kann uns interessieren
		
		olStart = prlSco->Vpfr < opStart ? opStart : prlSco->Vpfr;

		switch(prlSco->Vpto.GetStatus())
		{
		default:
			// case COleDateTime::invalid:
			continue;	// gibts nicht
		case COleDateTime::valid:
			if(prlSco->Vpto < opStart) 
				continue;	// 3. Ende der G�ltigkeitszeit liegt vor dem opStart
			olEnd = prlSco->Vpto > opEnd ? opEnd : prlSco->Vpto;
			break;
		case COleDateTime::null:
			olEnd = opEnd;
			break;	// keine Endzeit, keine Beschr�nkung
		}
		
		// zur Liste addieren
		//++++++++++++++++++++++++++++ 
		// noch ein Filter:
		// Wenn eine SCODATA zeitlich komplett durch die andere �berlappt wird, soll sie nicht gespeichert werden
		// - nur innerhalb opStart - opEnd pr�fen

		COleDateTime olStart2,olEnd2;
		SCODATA *prlSco2;
		for (int ilC = popScoData->GetSize() - 1; ilC >= 0; ilC--)
		{
			prlSco2  = &popScoData->GetAt(ilC);
			olStart2 = prlSco2->Vpfr < opStart ? opStart : prlSco2->Vpfr;													
			olEnd2   = prlSco2->Vpto.GetStatus() == COleDateTime::null ? opEnd : prlSco2->Vpto > opEnd ? opEnd : prlSco2->Vpto;

			// neuer Contract deckt den vorherigen Contract komplett ab ?
			if (olStart <= olStart2 && olEnd >= olEnd2)
			{
				popScoData->RemoveAt(ilC);
			}
		}
			
		popScoData->Add(prlSco);
	}

	
	// opScoData.Sort(CompareTimes); - �berfl�ssig - wir haben aus einer sortierten Liste rausgeholt
	return popScoData->GetSize();
}

/**********************************************************************************
direkt von GetScoListBySurnWithTime abgeleitet
**********************************************************************************/

CString CedaScoData::GetCotAndCWEHBySurnWithTime(long lpSurn,COleDateTime opDate, CString* popWeeklyHour/*=0*/)
{
	CString olCotCode("");

	if(opDate.GetStatus() != COleDateTime::valid) return olCotCode;

	SCODATA  *prlSco = 0;

	int ilCount = FindFirstOfSurn(lpSurn);
	if(ilCount == -1) return olCotCode;	// lpSurn nicht gefunden

	for(; ilCount < omData.GetSize(); ilCount++)
	{
		prlSco = &omData[ilCount];

		if(prlSco->Surn > lpSurn) 
			break;		// omData ist ja nach Surn-Vpfr sortiert
		
		if(prlSco->Vpfr.GetStatus() != COleDateTime::valid) 
			continue;	// 1. Defekter Datensatz
		
		if(prlSco->Vpfr <= opDate)
		{
			switch(prlSco->Vpto.GetStatus())
			{
			default:
				// case COleDateTime::invalid:
				continue;	// gibts nicht
			case COleDateTime::valid:
				if(prlSco->Vpto < opDate) 
					continue;	// 3. Ende der G�ltigkeitszeit liegt vor dem opStart
				break;
			case COleDateTime::null:
				break;	// keine Endzeit, keine Beschr�nkung
			}
			olCotCode = prlSco->Code;	// gefunden!
			if(popWeeklyHour != 0)
				*popWeeklyHour = prlSco->Cweh;
			// wir pr�fen weiter, ob da noch passende Datens�tze vorliegen
		}
		else
			break;	// 2. Anfang der G�ltigkeitszeit liegt nach dem opDate, kein Datensatz mehr kann uns interessieren
	}
	return olCotCode;
}

//---------------------------------------------------------------------------------------

long CedaScoData::GetScoUrnoWithTime(COleDateTime opDate, CCSPtrArray<SCODATA> *popScoData)
{
	long llUrno = NULL;

	int ilScoSize = popScoData->GetSize();

	for(int i=0; i<ilScoSize; i++)
	{
		if((*popScoData)[i].Vpfr.GetStatus() == COleDateTime::valid)
		{
			if((*popScoData)[i].Vpfr <= opDate)
			{
				switch((*popScoData)[i].Vpto.GetStatus())
				{
				case COleDateTime::valid:
					if((*popScoData)[i].Vpto > opDate)
					{
						llUrno = (*popScoData)[i].Urno;
					}
					else
					{
						llUrno = NULL;
					}
					break;
				default:
						llUrno = (*popScoData)[i].Urno;
					break;
				}
			}
		}
	}
	return llUrno;
}

//--READSPECIALDATA-------------------------------------------------------------------------------------

bool CedaScoData::ReadSpecialData(CCSPtrArray<SCODATA> *popSco,char *pspWhere,char *pspFieldList,bool ipSYS/*=true*/)
{
	bool ilRc = true;
	char pclFieldList[256] = " ";

	if(strlen(pspFieldList) > 0) 
	{
		strcpy(pclFieldList, pspFieldList);
	}

	if(IsFullLoggingEnabled())
	{
		ogRosteringLogText.Format("CedaAction:\n  Cmd <RT>\n  Tbl <%s>\n  Sel <%s>",pcmTableName, pspWhere);
		WriteInRosteringLog();
	}

	if(ipSYS == true) 
	{
		if (CedaAction("SYS",pcmTableName,pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	else
	{
		if (CedaAction2("RT",pcmTableName,pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	if(popSco != NULL)
	{
		for (int ilCountRecord = 0; ilRc == true; ilCountRecord++)
		{
			SCODATA *prpSco = new SCODATA;
			if ((ilRc = GetBufferRecord2(ilCountRecord,prpSco,CString(pclFieldList))) == true)
			{
				// BDA: Vpfr & Vpto k�nnen auch mal null sein, es ist falsch!
				// ARE: Vpto kann auch mal null sein, es ist OK! aber auf keinen Fall ung�ltig
				if(prpSco->Vpfr.GetStatus() != COleDateTime::valid || prpSco->Vpto.GetStatus() == COleDateTime::invalid)
				{
					TRACE("Read-Sco: %d gelesen, FEHLER IM DATENSATZ, Surn=%ld\n",ilCountRecord-1,prpSco->Surn);
					delete prpSco;
					continue;
				}
				//else
					//TRACE("Read-Sco: %d gelesen, KEIN FEHLER\n",ilCountRecord-1);
				
				popSco->Add(prpSco);
			}
			else
			{
				delete prpSco;
			}
		}
		if(popSco->GetSize() == 0) return false;
	}
	ClearFastSocketBuffer();	
    return true;
}

//--SAVE---------------------------------------------------------------------------------------------------

bool CedaScoData::Save(SCODATA *prpSco)
{
	bool ilRc = true;
	CString olListOfData;
	char pclSelection[124];
	char pclData[524];

	if (prpSco->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}
	switch(prpSco->IsChanged)
	{
	case DATA_NEW:
		MakeCedaData(&omRecInfo,olListOfData,prpSco);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("IRT","","",pclData);
		prpSco->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpSco->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpSco);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("URT",pclSelection,"",pclData);
		prpSco->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_DELETED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpSco->Urno);
		ilRc = CedaAction("DRT",pclSelection);
		break;
	}
	TRACE("Sco-IRT/URT/DRT: Ceda-Return %d \n",ilRc);
    return ilRc;
}

//--PROCESS-CF---------------------------------------------------------------------------------------------

void  ProcessScoCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	ogScoData.ProcessBc(ipDDXType,vpDataPointer,ropInstanceName);
}

//--PROCESS-BC---------------------------------------------------------------------------------------------

void  CedaScoData::ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlScoData;
	prlScoData = (struct BcStruct *) vpDataPointer;
	SCODATA *prlSco;
	
	if(IsFullLoggingEnabled())
	{
		ogRosteringLogText.Format("ProcessBC:\n  Cmd <%s>\n  Tbl <%s>\n  Twe <%s>\n  Sel <%s>\n  Fld <%s>\n  Dat <%s>\n  Bcn <%s>",prlScoData->Cmd, prlScoData->Object, prlScoData->Twe, prlScoData->Selection, prlScoData->Fields, prlScoData->Data,prlScoData->BcNum);
		WriteInRosteringLog();
	}

	switch(ipDDXType)
	{
	default:
		break;
	case BC_SCO_CHANGE:
		prlSco = GetScoByUrno(GetUrnoFromSelection(prlScoData->Selection));
		if(prlSco != NULL)
		{
			GetRecordFromItemList(prlSco,prlScoData->Fields,prlScoData->Data);
			UpdateInternal(prlSco);
			break;
		}
	case BC_SCO_NEW:
		prlSco = new SCODATA;
		GetRecordFromItemList(prlSco,prlScoData->Fields,prlScoData->Data);
		InsertInternal(prlSco);
		break;
	case BC_SCO_DELETE:
		{
			long llUrno;
			CString olSelection = (CString)prlScoData->Selection;
			if (olSelection.Find('\'') != -1)
			{
				llUrno = GetUrnoFromSelection(prlScoData->Selection);
			}
			else
			{
				int ilFirst = olSelection.Find("=")+2;
				int ilLast  = olSelection.GetLength();
				llUrno = atol(olSelection.Mid(ilFirst,ilLast-ilFirst));
			}
			prlSco = GetScoByUrno(llUrno);
			if (prlSco != NULL)
			{
				DeleteInternal(prlSco);
			}
		}
		break;
	}
}

//---------------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------

/*static int CompareTimes(const SCODATA **e1, const SCODATA **e2)
{
	if((**e1).Vpfr>(**e2).Vpfr)
		return 1;
	else
		return -1;
}*/

//---------------------------------------------------------------------------------------------------------

static int CompareSurnAndTimes(const SCODATA **e1, const SCODATA **e2)
{
	if((**e1).Surn>(**e2).Surn) 
		return 1;
	else if((**e1).Surn<(**e2).Surn) 
		return -1;
	
    if((**e1).Vpfr>(**e2).Vpfr)
		return 1;
	else if((**e1).Vpfr<(**e2).Vpfr)
		return -1;

	return 0;
}

/*****************************************************************************
Sehr schnelles Suchen vom ersten Datensatz mit der angegebenen Surn
! wir nutzen die Tatsache, das omData nach Surn & Time sortiert ist !
*****************************************************************************/
int CedaScoData::FindFirstOfSurn(long lpSurn)
{
	int ilSize = omData.GetSize();
	if(!ilSize) 
		return -1;
	
	int ilSearch;

	if(omData[ilSize-1].Surn != omData[0].Surn)
	{
		if(ilSize > 16)
		{
			// rekursives Suchen
			ilSearch = ilSize >> 1;
			int ilUp, ilDown;
			
			for(ilUp = ilSize-1, ilDown = 0;;)
			{
				if(lpSurn == omData[ilSearch].Surn)
				{
					if(!ilSearch || lpSurn != omData[ilSearch-1].Surn)
					{
						// gefunden!
						return ilSearch;
					}

					// lineares Suchen nach unten
					for(ilSearch--; ilSearch>=0;ilSearch--)
					{
						if(!ilSearch || lpSurn != omData[ilSearch-1].Surn)
						{
							// gefunden!
							return ilSearch;
						}
					}
				}
				
				if(ilUp-ilDown <= 1)
				{
					if(ilUp == ilDown)
					{
						// kein Surn in der Liste vorhanden!
						return -1;
					}

					if(lpSurn == omData[ilUp].Surn)
						return ilUp;
					else if(lpSurn == omData[ilDown].Surn)
						return ilDown;
					else 
						return -1;
				}

				if(lpSurn < omData[ilSearch].Surn)
				{
					ilUp = ilSearch;
					ilSearch = (ilUp + ilDown) >> 1;	// == (ilUp + ilDown) / 2
				}
				else //  if(lpSurn > omData[ilSearch].Surn)
				{
					ilDown = ilSearch;
					ilSearch = (ilUp + ilDown) >> 1;	// == (ilUp + ilDown) / 2
				}
			}
		}
		else
		{
			// lineares Suchen
			for(int ilSearch=0; ilSearch<ilSize; ilSearch++)
			{
				if(omData[ilSearch].Surn == lpSurn)
					return ilSearch;
			}
		}
	}
	else
	{
		return 0;	// alle gleich
	}
	
	return -1;	// kein gefunden!
}

/***********************************************************************

***********************************************************************/
bool CedaScoData::IsValidSco(SCODATA* popSco)
{
	// long			Surn;			// Mitarbeiter-Urno
	if(!ogStfData.GetStfByUrno(popSco->Surn))
	{
		CString olErr;
		olErr.Format("Contract Connection Table (SCOTAB) URNO '%d' is defect, Employee with URNO '%d' in STFTAB not found\n",
			popSco->Urno, popSco->Surn);
		ogErrlog += olErr;
		
		if(IsTraceLoggingEnabled())
		{
			GetDefectDataString(ogRosteringLogText, (void*)popSco, "SCOTAB.SURN in STFTAB not found");
			WriteInRosteringLog(LOGFILE_TRACE);
		}
		return false;	// 1. Defekter Datensatz
	}
	
	// char			Code[7];		// Contract Code
	if(!ogCotData.GetCotByCtrc(CString(popSco->Code)))
	{
		STFDATA* prlStf = ogStfData.GetStfByUrno(popSco->Surn);

		CString olErr;
		olErr.Format("Contract code '%s' for %s,%s in Contract Types Table not found\n",
			popSco->Code, prlStf->Lanm, prlStf->Finm);
		ogErrlog += olErr;
		
		if(IsTraceLoggingEnabled())
		{
			GetDefectDataString(ogRosteringLogText, (void*)popSco, "SCOTAB.CODE in COTTAB not found");
			WriteInRosteringLog(LOGFILE_TRACE);
		}
		return false;	// 1. Defekter Datensatz
	}

	// COleDateTime	Vpfr;
	// COleDateTime	Vpto;
	if(popSco->Vpfr.GetStatus() != COleDateTime::valid || popSco->Vpto.GetStatus() == COleDateTime::invalid)
	{
		STFDATA* prlStf = ogStfData.GetStfByUrno(popSco->Surn);

		CString olErr;
		olErr.Format("Contract time for %s,%s is not valid\n",
			prlStf->Lanm, prlStf->Finm);
		ogErrlog += olErr;
		
		if(IsTraceLoggingEnabled())
		{
			GetDefectDataString(ogRosteringLogText, (void*)popSco, "SCOTAB.VPFR or SCOTAB.VPTO is not valid");
			WriteInRosteringLog(LOGFILE_TRACE);
		}
		return false;	// 1. Defekter Datensatz
	}
	
	// char			Cweh[6];
	return true;
}

