#ifndef AFX_PSDISPORULESPAGE_H__1E8FA911_B278_11D1_8CF5_0000C002916B__INCLUDED_
#define AFX_PSDISPORULESPAGE_H__1E8FA911_B278_11D1_8CF5_0000C002916B__INCLUDED_
#endif


#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#ifndef	SHIFTCHECK_H
#define	SHIFTCHECK_H

#include <stdafx.h>
//#include "CCSCedadata.h"
#include <CedaDrrdata.H>
#include <CedaBsddata.H>
#include <CedaStfData.H>
#include <CedaOdaData.H>
#include <CCSGLOBL.H>
//#include "CCSPtrArray.h"
#include <resource.h>
#include <ShiftRoster_view.h>

//#include "CedaBasicData.h"
//#include "RecordSet.h"
#include <CedaWorkTime.h>


// Detailierte TRACE-Ausgabe einschalten
#ifdef _DEBUG
//#define TRACE_ALL_CHECKFUNC
#endif


#ifdef TRACE_ALL_CHECKFUNC
//#define SHOW_MA_URNO 0//29158187
#endif


// imOutput			Kombinieren!
#define OUT_NONE						0
#define OUT_WARNING_AS_WARNING			1
#define OUT_WARNING_AS_ERROR			2
#define OUT_ERROR_AS_ERROR				4
#define OUT_ERROR_AS_WARNING			8


void ProcessShiftCheckCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

/***************************************************************************
CedaShiftCheck 
***************************************************************************/

class	CedaShiftCheck : public CedaData
{
public:
	CedaShiftCheck();
	~CedaShiftCheck();
	void Initialize(void);
	
	// FillWorkTimeData() f�llt den Datenarray mit Arbeitszeiten
	void FillWorkTimeData(void);

	// SetCheckDate f�llt omCheckDateFrom und omCheckDateTo aus
	void SetCheckDate(COleDateTime opCheckDateFrom, COleDateTime opCheckDateTo);
	// die Testfunktion f�r DutyRoster
	bool DutyRosterCheckIt(DRRDATA* prpDrr, bool bDoConflictCheck, int ipDDXType, bool bpEnableTeilzeitplusQuestion=true);
	void DutyRosterCheckIt(COleDateTime &opTestDay);
	
	// die Testfunktion f�r ShiftRoster
	int ShiftRosterCheckIt(int ipGPLPeri,CCSPtrArray<GSPTABLEDATA> &opGSPTableData);

	//	UpdateData() speichert �nderungen der Arbeitszeit im Datenarray
	void UpdateData(DRRDATA* prpDrr,int ipDDXType);
	void UpdateData(DRADATA* prpDra,int ipDDXType);

	// ProcessDrrMultiChange: bearbeitet den Broadcast DRR_MULTI_CHANGE 
	void ProcessDrrMultiChange(void *vpDataPointer, CString &ropInstanceName);
	// true, wenn Warnungen vorliegen
	bool IsWarning();
	// Alle Warnungen werden zu einer Liste (Array) gesammelt und aus der Quelle gel�scht
	CStringArray* GetWarningList();
	// true = Die neue Schicht soll als Teilzeit Plus eingef�gt werden
	bool IsTeilzeitplusConfirmed() {return bmSaveAsTeilzeitplus;}
	
	void SetOutputAsWarning(bool bpAsWarning=true);

private:

	//	Liest alle Schichten und speichert in dem Datenarray
	int	ReadShifts(int ipGPLPeri,CCSPtrArray<GSPTABLEDATA> &opGSPTableData);
	//	Liest alle Schichten einer Woche und speichert im Datenarray
	int	FillAWeek(DRRDATA* prpDrr, CedaWorkTime* popWorkTime, int ipWeek,COleDateTime opBegin,CCSPtrArray<GSPTABLEDATA> &opGSPTableData);

	// Broadcast Funktionen
	void Register(void);
	// Aufr�umen
	bool ClearAll(bool bpUnregister);
	void ClearWorkTimeMap();

public:	
	void SetViewEName(int ipEName);
	// alle Volumeangaben, als Pointer ist ID_WOR_SPACE bis ID_WOR_MAXIMUMVALUE
	// wobei ID_WOR_SPACE ein Defaultparameter ist (kein Value)
	CDWordArray omRules;
	// Sie OUT_-defines oben - die Art, wie die Meldungen ausgegeben werden sollen
	int imOutput;

	// true - ShiftRosterCheckIt(), false - DutyRosterCheckIt()-Aufruf
	bool bmShiftRosterCheck;

	// aus rmViewInfo.bDoConflictCheck ob man einen Check machen soll
	bool bmDoConfictCheck;	

	// Statt einfacher Fehlermeldung fragen, ob man die Schicht als Teilzeit+ speischern soll
	bool bmEnableTeilzeitplusQuestion;

	bool bmSaveAsTeilzeitplus;			// true, wenn die Frage nach Teilzeit+ best�tigt wurde

	// Aktiver Zeitraum
	COleDateTime omCheckDateFrom;
	COleDateTime omCheckDateTo;
private:	
	int imEName;

	// lokale Urno f�r ShiftCheck-Iteration
	long	lmUrno;

	// Map mit Schicht/Pause-Angaben der einzelnen Mitarbeiter
	// Key ist lStfu - MA-Urno
	CMap<long,long,CedaWorkTime*,CedaWorkTime*&> omMapStaffDutyData;
};

#endif AFX_PSDISPORULESPAGE_H__1E8FA911_B278_11D1_8CF5_0000C002916B__INCLUDED_

