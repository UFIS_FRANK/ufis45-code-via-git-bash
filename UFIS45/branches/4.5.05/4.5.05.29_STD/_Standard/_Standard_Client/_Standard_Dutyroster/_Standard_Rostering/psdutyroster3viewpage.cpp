// PSDutyRoster3ViewPage.cpp: Implementierungsdatei
//

#include <stdafx.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Eigenschaftenseite PSDutyRoster3ViewPage 
//---------------------------------------------------------------------------

IMPLEMENT_DYNCREATE(PSDutyRoster3ViewPage, RosterViewPage)

PSDutyRoster3ViewPage::PSDutyRoster3ViewPage() : RosterViewPage(PSDutyRoster3ViewPage::IDD)
{
	//{{AFX_DATA_INIT(PSDutyRoster3ViewPage)
	//}}AFX_DATA_INIT
	bmPgpLoaded = false;
	omTitleStrg = LoadStg(IDS_W_Statistik);
	m_psp.pszTitle = omTitleStrg;
	m_psp.dwFlags |= PSP_USETITLE;
}

PSDutyRoster3ViewPage::~PSDutyRoster3ViewPage()
{
}

void PSDutyRoster3ViewPage::DoDataExchange(CDataExchange* pDX)
{
	RosterViewPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(PSDutyRoster3ViewPage)
	DDX_Control(pDX, IDC_LB_FUNCTIONS,		m_LB_Functions);
	DDX_Control(pDX, IDC_LB_ORGANISATIONS,	m_LB_Organisations);
	DDX_Control(pDX, IDC_C_EXTRA,			m_C_Extra);
	DDX_Control(pDX, IDC_C_GRUPP,			m_C_Grupp);
	DDX_Control(pDX, IDC_C_DETAIL,			m_C_Detail);
	DDX_Control(pDX, IDC_R_DIFF,			m_R_Diff);
	DDX_Control(pDX, IDC_R_ISTSOLL,			m_R_IstSoll);
	DDX_Control(pDX, IDC_C_ADD,				m_C_Additiv);
	DDX_Control(pDX, IDC_C_ALLCODES,		m_C_AllCodes);
	DDX_Control(pDX, IDC_R_PLAN,			m_R_Plan);
	DDX_Control(pDX, IDC_R_LANGZEIT,		m_R_Langzeit);
	DDX_Control(pDX, IDC_R_DIENSTPLAN,		m_R_Dienstplan);
	DDX_Control(pDX, IDC_R_ALLE_STUFEN,		m_R_Alle_Stufen);
	//}}AFX_DATA_MAP

	if (pDX->m_bSaveAndValidate == FALSE)
	{
		SetData();
	}
	if (pDX->m_bSaveAndValidate == TRUE)
	{
		GetData();
	}

}


BEGIN_MESSAGE_MAP(PSDutyRoster3ViewPage, RosterViewPage)
	//{{AFX_MSG_MAP(PSDutyRoster3ViewPage)
	ON_BN_CLICKED(IDC_ALL_FUNCS,OnAllFuncs)
	ON_BN_CLICKED(IDC_NO_FUNCS,	OnNoFuncs)
	ON_BN_CLICKED(IDC_ALL_ORGS,	OnAllOrgs)
	ON_BN_CLICKED(IDC_NO_ORGS,	OnNoOrgs)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten PSDutyRoster3ViewPage 
//---------------------------------------------------------------------------

void PSDutyRoster3ViewPage::SetCalledFrom(CString opCalledFrom)
{
	omCalledFrom = opCalledFrom;
}

//---------------------------------------------------------------------------

BOOL PSDutyRoster3ViewPage::OnInitDialog() 
{
	//Nur auf der Ersten Seite wird OnInitDialog vor SetData aufgerufen !!!!!!!!!
	RosterViewPage::OnInitDialog();

	//m_C_Grupp.EnableWindow(TRUE);
	SetStatic();
	SetButtonText();


	bool blShow1=false;
	bool blShow2=false;
	bool blShowL=false;

	// Werte aus der Parameterklasse einlesen
	CString olStepList= ogCCSParam.GetParamValue(ogAppl,"ID_SHOW_STEPS");
	// In Array �berf�hren
	CStringArray olStrArray;
	int ilArray = ExtractItemList(olStepList,&olStrArray, '|');

	for(int i=0;i<ilArray;i++)
	{
		CString olTmp = olStrArray.GetAt(i);
		if (olTmp == "1")
			blShow1 = true;
		else if (olTmp == "2")
			blShow2 = true;
		else if (olTmp == "L")
			blShowL = true;
	}
	olStrArray.RemoveAll();
	// Oberfl�chen Elemente entsprechend der Auswertung setzen
	if (!blShow1) m_R_Plan.EnableWindow(FALSE);
	if (!blShow2) m_R_Dienstplan.EnableWindow(FALSE);
	if (!blShowL) m_R_Langzeit.EnableWindow(FALSE);

	// ADO: Alle durch die Parameter nicht sichtbaren Stufen unchecken.
	if (!blShow1)	m_R_Plan.SetCheck(0);
	if (!blShow2)	m_R_Dienstplan.SetCheck(0);
	if (!blShowL)	m_R_Langzeit.SetCheck(0);

	return TRUE;
}

//---------------------------------------------------------------------------

BOOL PSDutyRoster3ViewPage::GetData()
{
	omValues.RemoveAll();
	CStringArray olTmpValues;
	if(GetData(olTmpValues) == FALSE)
	{
		return FALSE;
	}
	for(int i = 0; i < olTmpValues.GetSize(); i++)
	{
		CString olTmp = olTmpValues[i];
		omValues.Add(olTmpValues[i]);
	}
	return TRUE;
}

//---------------------------------------------------------------------------

BOOL PSDutyRoster3ViewPage::GetData(CStringArray &opValues)
{
	//----------------------------
	if(m_C_Grupp.GetCheck() == 1)
		opValues.Add("J");
	else
		opValues.Add("N");
	//----------------------------
	if(m_C_Detail.GetCheck() == 1)
		opValues.Add("J");
	else
		opValues.Add("N");
	//----------------------------
	if(m_C_Extra.GetCheck() == 1)
		opValues.Add("J");
	else
		opValues.Add("N");
	//----------------------------
	if(m_R_Diff.GetCheck() == 1)
		opValues.Add("DIFF");
	else
		opValues.Add("ISDEBIT");
	//----------------------------

	//*** Get all selected Funktions ****************************************
	CString olTmpTxt, olUrno, olUrnos;
	int ilMaxItems;
	ilMaxItems = m_LB_Functions.GetSelCount();
	int *pilIndex = new int [ilMaxItems];
	m_LB_Functions.GetSelItems(ilMaxItems, pilIndex );
	for(int i=0; i<ilMaxItems; i++)
	{
		m_LB_Functions.GetText(pilIndex[i],olTmpTxt);
		olUrno = olTmpTxt.Mid(olTmpTxt.Find("URNO=")+5);
		olUrnos += olUrno;
		if(i < ilMaxItems-1)
			olUrnos += CString("|");
	}
	delete pilIndex;
	if (olUrnos.GetLength() > 0)
	{
		opValues.Add(olUrnos);
	}
	else
	{
		// NULL gibt an, dass keine Funktionen ausgew�hlt sind. Das ist erforderlich, 
		// damit die Default-Auswahl aller Funktionen beim Initialisieren der
		// Proppage verhindert werden kann (bei Abfrage des opValues mit IsEmpty()
		// w�re nicht klar, ob der Wert leer ist, weil keine Funktionen ausgew�hlt 
		// wurden oder weil die Ansicht noch nicht gespeichert wurde, also neu ist.)
		opValues.Add("NULL");
	}
	//***end> Get all selected Funktions ************************************

	//*** Get all selected Organisations ****************************************
	olTmpTxt.Empty();
	olUrno.Empty();
	olUrnos.Empty();
	ilMaxItems = m_LB_Organisations.GetSelCount();
	pilIndex = new int [ilMaxItems];
	m_LB_Organisations.GetSelItems(ilMaxItems, pilIndex );
	for(i=0; i<ilMaxItems; i++)
	{
		m_LB_Organisations.GetText(pilIndex[i],olTmpTxt);
		olUrno = olTmpTxt.Mid(olTmpTxt.Find("URNO=")+5);
		olUrnos += olUrno;
		if(i < ilMaxItems-1)
			olUrnos += CString("|");
	}
	delete pilIndex;
	if (olUrnos.GetLength() > 0)
	{
		opValues.Add(olUrnos);
	}
	else
	{
		// NULL f�r keine Organisationen (siehe oben, wie bei Funktionen)
		opValues.Add("NULL");
	}
	//***end> Get all selected Organisations ************************************

	if(m_R_Plan.GetCheck() == 1)
		opValues.Add("PLAN");
	else if(m_R_Langzeit.GetCheck() == 1)
		opValues.Add("LANGZEIT");
	else if(m_R_Dienstplan.GetCheck() == 1)
		opValues.Add("DIENSTPLAN");
	else
		opValues.Add("ALLE");

	//----------------------------
	if(m_C_Additiv.GetCheck() == 1)
		opValues.Add("J");
	else
		opValues.Add("N");
	//----------------------------

	//----------------------------
	if(m_C_AllCodes.GetCheck() == 1)
		opValues.Add("J");
	else
		opValues.Add("N");
	//----------------------------

	return TRUE;
}

//---------------------------------------------------------------------------

void PSDutyRoster3ViewPage::SetData()
{
	SetData(omValues);
}

//---------------------------------------------------------------------------

void PSDutyRoster3ViewPage::SetData(CStringArray &opValues)
{
	if(!bmPgpLoaded)
	{	
		CString olNoCode5 = "-----";
		CString olNoCode8 = "--------";
		//*** Insert all Functions in ListBox ******************************
		CString olLine;
		m_LB_Functions.ResetContent();
		m_LB_Functions.SetFont(&ogCourier_Regular_8);
		int ilPfcCount =  ogPfcData.omData.GetSize();
		for(int iPfc=0; iPfc<ilPfcCount; iPfc++)
		{
			olLine.Format("%-8s   %-40s     URNO=%d",ogPfcData.omData[iPfc].Fctc, ogPfcData.omData[iPfc].Fctn, ogPfcData.omData[iPfc].Urno);
			m_LB_Functions.AddString(olLine);
		}
		// Eine leeren Zeile einf�gen, f�r alle MA's die keine Funktion haben
		olLine.Format("%-8s   %-40s     URNO=NO_PFCCODE",olNoCode5, LoadStg(IDS_STRING1878));
		m_LB_Functions.AddString(olLine);
		//***end> Insert all Functions in ListBox ***************************
		//*** Insert all Organisations in ListBox ******************************
		m_LB_Organisations.ResetContent();
		m_LB_Organisations.SetFont(&ogCourier_Regular_8);
		int ilOrgCount =  ogOrgData.omData.GetSize();
		for(int iOrg=0; iOrg<ilOrgCount; iOrg++)
		{
			olLine.Format("%-8s   %-40s     URNO=%d",ogOrgData.omData[iOrg].Dpt1, ogOrgData.omData[iOrg].Dptn, ogOrgData.omData[iOrg].Urno);
			m_LB_Organisations.AddString(olLine);
		}
		// Eine leeren Zeile einf�gen, f�r alle MA's die keine Organisationseinheit haben
		olLine.Format("%-8s   %-40s     URNO=NO_ORGCODE",olNoCode8, LoadStg(IDS_STRING1879));
		m_LB_Organisations.AddString(olLine);
		//***end> Insert all Organisations in ListBox ***************************
		bmPgpLoaded = true;
	}
	//*** Set all adjustments to default ************************************
	//m_C_Grupp.SetCheck(1);
	m_C_Detail.SetCheck(1);
	m_C_Extra.SetCheck(0);
	m_R_Diff.SetCheck(0);
	m_R_IstSoll.SetCheck(1);
	m_R_Plan.SetCheck(0);
	m_R_Langzeit.SetCheck(0);
	m_R_Dienstplan.SetCheck(0);
	m_R_Alle_Stufen.SetCheck(1);
	m_C_Additiv.SetCheck(0);
	m_C_AllCodes.SetCheck(1);
	m_LB_Functions.SetSel(-1,false);
	m_LB_Organisations.SetSel(-1,false);

	//*** end> Set all adjustments to default *******************************

	for(int ilValue = 0; ilValue < opValues.GetSize(); ilValue++)
	{
		switch(ilValue)
		{
			case 0:
			{
				/*
				if(opValues[ilValue] == "J")
					m_C_Grupp.SetCheck(1);
				else
					m_C_Grupp.SetCheck(0);
				*/
			}
			break;
			case 1:
			{
				if(opValues[ilValue] == "J")
					m_C_Detail.SetCheck(1);
				else
					m_C_Detail.SetCheck(0);
			}
			break;
			case 2:
			{
				if(opValues[ilValue] == "J")
					m_C_Extra.SetCheck(1);
				else
					m_C_Extra.SetCheck(0);
			}
			break;
			case 3:
			{
				if(opValues[ilValue] == "DIFF")
				{
					m_R_Diff.SetCheck(1);
					m_R_IstSoll.SetCheck(0);
				}
 				else
				{
					m_R_Diff.SetCheck(0);
					m_R_IstSoll.SetCheck(1);
				}
 			}
			break;
			case 4:
			{
				if(opValues[ilValue].IsEmpty() == FALSE)
				{
					// Default-Auswahl aufheben
					m_LB_Functions.SetSel(-1,false);
					if (opValues[ilValue] != "NULL")
					{
						//*** Select all set Funktions ****************************************
						CString olTmpTxt, olUrno;
						int ilCount = m_LB_Functions.GetCount();
						for(int i = 0;i<ilCount;i++)
						{
							m_LB_Functions.GetText(i,olTmpTxt);
							olUrno = olTmpTxt.Mid(olTmpTxt.Find("URNO=")+5);
							if(opValues[ilValue].Find(olUrno) != -1)
								m_LB_Functions.SetSel(i,TRUE);
						}
						//*** end> Select all set Funktions ***********************************
					}
				}
 			}
			break;
			case 5:
			{
				if(opValues[ilValue].IsEmpty() == FALSE)
				{
					// Default-Auswahl aufheben
					m_LB_Organisations.SetSel(-1,false);
					if (opValues[ilValue] != "NULL")
					{
						//*** Select all set Organisations ****************************************
						CString olTmpTxt, olUrno;
						int ilCount = m_LB_Organisations.GetCount();
						for(int i = 0;i<ilCount;i++)
						{
							m_LB_Organisations.GetText(i,olTmpTxt);
							olUrno = olTmpTxt.Mid(olTmpTxt.Find("URNO=")+5);
							if(opValues[ilValue].Find(olUrno) != -1)
								m_LB_Organisations.SetSel(i,TRUE);
						}
						//*** end> Select all set Organisations ***********************************
					}
				}
 			}
			break;
			case 6:
			{
				if(opValues[ilValue] == "CURRENT")
				{
					m_R_Plan.SetCheck(0);
					m_R_Langzeit.SetCheck(0);
					m_R_Dienstplan.SetCheck(0);
					m_R_Alle_Stufen.SetCheck(0);
				}
 				else if(opValues[ilValue] == "PLAN")
				{
					m_R_Langzeit.SetCheck(0);
					m_R_Dienstplan.SetCheck(0);
					m_R_Alle_Stufen.SetCheck(0);
					m_R_Plan.SetCheck(1);
				}
 				else if(opValues[ilValue] == "LANGZEIT")
				{
					m_R_Plan.SetCheck(0);
					m_R_Dienstplan.SetCheck(0);
					m_R_Alle_Stufen.SetCheck(0);
					m_R_Langzeit.SetCheck(1);
				}
 				else if(opValues[ilValue] == "DIENSTPLAN")
				{
					m_R_Plan.SetCheck(0);
					m_R_Langzeit.SetCheck(0);
					m_R_Alle_Stufen.SetCheck(0);
					m_R_Dienstplan.SetCheck(1);
				}
 				else
				{
					m_R_Plan.SetCheck(0);
					m_R_Langzeit.SetCheck(0);
					m_R_Dienstplan.SetCheck(0);
					m_R_Alle_Stufen.SetCheck(1);
				}
 			}
			break;
			case 7:
			{
				if(opValues[ilValue] == "J")
				{
					m_C_Additiv.SetCheck(1);
				}
 				else
				{
					m_C_Additiv.SetCheck(0);
				}
 			}
			break;
			case 8:
			{
				if(opValues[ilValue] == "J")
				{
					m_C_AllCodes.SetCheck(1);
				}
 				else
				{
					m_C_AllCodes.SetCheck(0);
				}
 			}
			break;
		}
	}
}

//---------------------------------------------------------------------------

void PSDutyRoster3ViewPage::OnAllFuncs() 
{
	m_LB_Functions.SetSel(-1,true);
}

//---------------------------------------------------------------------------

void PSDutyRoster3ViewPage::OnNoFuncs() 
{
	m_LB_Functions.SetSel(-1,false);
}

//---------------------------------------------------------------------------

void PSDutyRoster3ViewPage::OnAllOrgs() 
{
	m_LB_Organisations.SetSel(-1,true);
}

//---------------------------------------------------------------------------

void PSDutyRoster3ViewPage::OnNoOrgs() 
{
	m_LB_Organisations.SetSel(-1,false);
}

//---------------------------------------------------------------------------


void PSDutyRoster3ViewPage::SetButtonText()
{
	SetDlgItemText(IDC_ALL_ORGS,LoadStg(IDS_S_All));
	SetDlgItemText(IDC_NO_ORGS,LoadStg(IDS_S_NoOne));
	SetDlgItemText(IDC_ALL_FUNCS,LoadStg(IDS_S_All));
	SetDlgItemText(IDC_NO_FUNCS,LoadStg(IDS_S_NoOne));

	SetDlgItemText(IDC_R_PLAN,LoadStg(IDS_STRING1586));
	SetDlgItemText(IDC_R_LANGZEIT,LoadStg(IDS_STRING1876));
	SetDlgItemText(IDC_R_DIENSTPLAN,LoadStg(IDS_STRING1587));
	SetDlgItemText(IDC_R_ALLE_STUFEN,LoadStg(IDS_S_AlleStufen));

	SetDlgItemText(IDC_C_GRUPP,LoadStg(SHIFT_SHEET_C_GRUPP));
	SetDlgItemText(IDC_C_DETAIL,LoadStg(SHIFT_SHEET_C_DETAIL));
	SetDlgItemText(IDC_C_EXTRA,LoadStg(IDS_S_ExtraShift));

	SetDlgItemText(IDC_R_ISTSOLL,LoadStg(SHIFT_SHEET_R_ISTSOLL));
	SetDlgItemText(IDC_R_DIFF,LoadStg(SHIFT_SHEET_R_DIFF));
	SetDlgItemText(IDC_C_ADD,LoadStg(IDS_S_Additiv));
	SetDlgItemText(IDC_C_ALLCODES,LoadStg(IDS_S_AllCodes));
 }

void PSDutyRoster3ViewPage::SetStatic()
{
	SetDlgItemText(IDC_S_Stat,LoadStg(IDS_S_Stat));
	SetDlgItemText(IDC_S_Ist,LoadStg(IDS_S_Ist));
	SetDlgItemText(IDC_S_Fct,LoadStg(SHIFT_STF_FUNCTION));
	SetDlgItemText(IDC_S_Org,LoadStg(IDS_S_Org2));
}
