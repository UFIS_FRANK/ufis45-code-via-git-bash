# Microsoft Developer Studio Project File - Name="Rostering" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Application" 0x0101

CFG=Rostering - Win32 Release
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "Rostering.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "Rostering.mak" CFG="Rostering - Win32 Release"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "Rostering - Win32 Release" (based on "Win32 (x86) Application")
!MESSAGE "Rostering - Win32 Debug" (based on "Win32 (x86) Application")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "Rostering - Win32 Release"

# PROP BASE Use_MFC 5
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 5
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "c:\Ufis_Bin\Release"
# PROP Intermediate_Dir "c:\Ufis_Intermediate\Rostering\Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MT /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /MT /W3 /GX /O2 /I ".\\" /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_MBCS" /FR /Yu"stdafx.h" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x407 /d "NDEBUG"
# ADD RSC /l 0x409 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /machine:I386
# ADD LINK32 c:\Ufis_Bin\Release\Ufis32.lib c:\Ufis_Bin\Classlib\Release\CCSClass.lib Wsock32.lib /nologo /subsystem:windows /machine:I386

!ELSEIF  "$(CFG)" == "Rostering - Win32 Debug"

# PROP BASE Use_MFC 5
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 5
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "c:\Ufis_Bin\Debug"
# PROP Intermediate_Dir "c:\Ufis_Intermediate\Rostering\Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MTd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /Yu"stdafx.h" /FD /GZ /c
# ADD CPP /nologo /MTd /W3 /Gm /GX /ZI /Od /I ".\\" /I "..\..\..\..\_Standard\_Standard_Client\_Standard_Share\_Standard_Wrapper" /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /FR /Yu"stdafx.h" /FD /GZ /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x407 /d "_DEBUG"
# ADD RSC /l 0x409 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept
# ADD LINK32 c:\Ufis_Bin\Debug\Ufis32.lib c:\Ufis_Bin\Classlib\Debug\CCSClass.lib Wsock32.lib /nologo /subsystem:windows /debug /machine:I386
# SUBTRACT LINK32 /profile /map

!ENDIF 

# Begin Target

# Name "Rostering - Win32 Release"
# Name "Rostering - Win32 Debug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=..\..\..\..\_Standard\_Standard_Client\_Standard_Share\_Standard_Wrapper\aatlogin.cpp
# End Source File
# Begin Source File

SOURCE=.\Accounts.cpp
# End Source File
# Begin Source File

SOURCE=.\AutoRosteringDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\BasePropertySheet.CPP
# End Source File
# Begin Source File

SOURCE=.\BasicData.cpp
# End Source File
# Begin Source File

SOURCE=.\CCSDynTable.cpp
# End Source File
# Begin Source File

SOURCE=.\CCSGlobl.cpp
# End Source File
# Begin Source File

SOURCE=.\CCSParam.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaAccData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaBsdData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaCfgData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaCotData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaDataHelper.cpp
# End Source File
# Begin Source File

SOURCE=.\cedadeldata.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaDraData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaDrdData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaDrgData.cpp
# End Source File
# Begin Source File

SOURCE=.\cedadrrdata.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaDrsData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaInitModuData.Cpp
# End Source File
# Begin Source File

SOURCE=.\cedamsddata.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaOacData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaOdaData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaOrgData.Cpp
# End Source File
# Begin Source File

SOURCE=.\CedaPerData.Cpp
# End Source File
# Begin Source File

SOURCE=.\CedaPfcData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaPgpData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaRelData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaScoData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaSdgData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaSdtData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaSgmData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaSgrData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaSorData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaSpeData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaSpfData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaSprData.cpp
# End Source File
# Begin Source File

SOURCE=.\cedasredata.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaSteData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaStfData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaSwgData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaTeaData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaWgpData.cpp
# End Source File
# Begin Source File

SOURCE=.\cedawisdata.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaWorkTime.cpp
# End Source File
# Begin Source File

SOURCE=.\ChangeShiftDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\CharacteristicValueDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\ChildFrm.cpp
# End Source File
# Begin Source File

SOURCE=.\CodeBigDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\CodeDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\ColorControls.cpp
# End Source File
# Begin Source File

SOURCE=.\COMBOBAR.CPP
# End Source File
# Begin Source File

SOURCE=.\CommonGridDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\CViewer.cpp
# End Source File
# Begin Source File

SOURCE=.\DoubleTourDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\DraDialog.cpp
# End Source File
# Begin Source File

SOURCE=.\DrdDialog.cpp
# End Source File
# Begin Source File

SOURCE=.\DutyAbsenceOverviewDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\DutyAccountDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\DutyAttentionDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\DutyBsdDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\dutychangegroupdlg.cpp
# End Source File
# Begin Source File

SOURCE=.\DutyDebitTabViewer.Cpp
# End Source File
# Begin Source File

SOURCE=.\DutyDragDropDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\DutyDragDropTabViewer.Cpp
# End Source File
# Begin Source File

SOURCE=.\DutyIsTabViewer.Cpp
# End Source File
# Begin Source File

SOURCE=.\DutyPlanAbsencePp.cpp
# End Source File
# Begin Source File

SOURCE=.\DutyPlanPs.cpp
# End Source File
# Begin Source File

SOURCE=.\dutyprintdlg.cpp
# End Source File
# Begin Source File

SOURCE=.\DutyReleaseDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\DutyRoster_Frm.cpp
# End Source File
# Begin Source File

SOURCE=.\DutyRoster_View.cpp
# End Source File
# Begin Source File

SOURCE=.\DutyRosterPropertySheet.CPP
# End Source File
# Begin Source File

SOURCE=.\DutyShowJobsDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\DutySolidTabViewer.Cpp
# End Source File
# Begin Source File

SOURCE=.\DutyStatTabViewer.Cpp
# End Source File
# Begin Source File

SOURCE=.\Empldlg.cpp
# End Source File
# Begin Source File

SOURCE=.\ExcelExport.cpp
# End Source File
# Begin Source File

SOURCE=.\ExcelExportDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\gridcontrol.cpp
# End Source File
# Begin Source File

SOURCE=.\GridEmplDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\Groups.cpp
# End Source File
# Begin Source File

SOURCE=.\GUILng.cpp
# End Source File
# Begin Source File

SOURCE=.\ImportShifts.cpp
# End Source File
# Begin Source File

SOURCE=.\InfoBoxDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\InfoDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\InitialLoadDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\ListBoxDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\LoginDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\MainFrm.cpp
# End Source File
# Begin Source File

SOURCE=.\PfcSelectDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\Plan_Info.cpp
# End Source File
# Begin Source File

SOURCE=.\PrintDailyRoster.cpp
# End Source File
# Begin Source File

SOURCE=.\PrintDailyRosterB.cpp
# End Source File
# Begin Source File

SOURCE=.\PrivList.cpp
# End Source File
# Begin Source File

SOURCE=.\PSDutyRoster1ViewPage.cpp
# End Source File
# Begin Source File

SOURCE=.\PSDutyRoster2ViewPage.cpp
# End Source File
# Begin Source File

SOURCE=.\PSDutyRoster3ViewPage.cpp
# End Source File
# Begin Source File

SOURCE=.\PSDutyRoster4ViewPage.cpp
# End Source File
# Begin Source File

SOURCE=.\PSDutyRoster5ViewPage.cpp
# End Source File
# Begin Source File

SOURCE=.\PSShiftRoster1ViewPage.cpp
# End Source File
# Begin Source File

SOURCE=.\PSShiftRoster2ViewPage.cpp
# End Source File
# Begin Source File

SOURCE=.\psshiftroster3viewpage.cpp
# End Source File
# Begin Source File

SOURCE=.\RegisterDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\ReportCompensation.cpp
# End Source File
# Begin Source File

SOURCE=.\Rostering.cpp
# End Source File
# Begin Source File

SOURCE=.\Rostering.rc
# End Source File
# Begin Source File

SOURCE=.\RosteringDoc.cpp
# End Source File
# Begin Source File

SOURCE=.\RosteringView.cpp
# End Source File
# Begin Source File

SOURCE=.\RosterViewPage.cpp
# End Source File
# Begin Source File

SOURCE=.\SelectDateDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\SelectShiftDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\SendToSapDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\SetupDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\shiftawdlg.cpp
# End Source File
# Begin Source File

SOURCE=.\shiftawdlgbonus.cpp
# End Source File
# Begin Source File

SOURCE=.\ShiftCheck.CPP
# End Source File
# Begin Source File

SOURCE=.\ShiftCodeNumberDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\ShiftKonsistDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\ShiftNewGSPDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\ShiftPlanDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\ShiftPrintDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\ShiftReleaseDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\ShiftRoster_Frm.cpp
# End Source File
# Begin Source File

SOURCE=.\ShiftRoster_View.cpp
# End Source File
# Begin Source File

SOURCE=.\ShiftRosterPropertySheet.CPP
# End Source File
# Begin Source File

SOURCE=.\StdAfx.cpp
# ADD CPP /Yc"stdafx.h"
# End Source File
# Begin Source File

SOURCE=.\TABLE.CPP
# End Source File
# Begin Source File

SOURCE=.\TageslisteDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\UDlgStatusBar.cpp
# End Source File
# Begin Source File

SOURCE=.\WaitDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\WishDialog.cpp
# End Source File
# Begin Source File

SOURCE=.\XStatusBar.cpp
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=..\..\..\..\..\Ufis4.5\Ufis\_Standard\_Standard_Client\_Standard_Share\_Standard_Wrapper\aatlogin.h
# End Source File
# Begin Source File

SOURCE=.\Accounts.h
# End Source File
# Begin Source File

SOURCE=.\Ansicht.h
# End Source File
# Begin Source File

SOURCE=.\AutoRosteringDlg.h
# End Source File
# Begin Source File

SOURCE=.\BasePropertySheet.H
# End Source File
# Begin Source File

SOURCE=.\BasicData.h
# End Source File
# Begin Source File

SOURCE=.\CCSDynTable.h
# End Source File
# Begin Source File

SOURCE=.\CCSGLOBL.H
# End Source File
# Begin Source File

SOURCE=.\CCSParam.h
# End Source File
# Begin Source File

SOURCE=.\CedaAccData.h
# End Source File
# Begin Source File

SOURCE=.\CedaBsdData.h
# End Source File
# Begin Source File

SOURCE=.\CedaCfgData.h
# End Source File
# Begin Source File

SOURCE=.\CedaCotData.h
# End Source File
# Begin Source File

SOURCE=.\CedaData.h
# End Source File
# Begin Source File

SOURCE=.\CedaDataHelper.h
# End Source File
# Begin Source File

SOURCE=.\cedadeldata.h
# End Source File
# Begin Source File

SOURCE=.\CedaDraData.h
# End Source File
# Begin Source File

SOURCE=.\CedaDrdData.h
# End Source File
# Begin Source File

SOURCE=.\CedaDrgData.h
# End Source File
# Begin Source File

SOURCE=.\cedadrrdata.h
# End Source File
# Begin Source File

SOURCE=.\CedaDrsData.h
# End Source File
# Begin Source File

SOURCE=.\CedaInitModuData.h
# End Source File
# Begin Source File

SOURCE=.\cedamsddata.h
# End Source File
# Begin Source File

SOURCE=.\CedaOacData.h
# End Source File
# Begin Source File

SOURCE=.\CedaOdaData.h
# End Source File
# Begin Source File

SOURCE=.\cedaorgdata.h
# End Source File
# Begin Source File

SOURCE=.\CedaPerData.h
# End Source File
# Begin Source File

SOURCE=.\CedaPfcData.h
# End Source File
# Begin Source File

SOURCE=.\CedaPgpData.h
# End Source File
# Begin Source File

SOURCE=.\CedaRelData.h
# End Source File
# Begin Source File

SOURCE=.\CedaScoData.h
# End Source File
# Begin Source File

SOURCE=.\CedaSdgData.h
# End Source File
# Begin Source File

SOURCE=.\CedaSdtData.h
# End Source File
# Begin Source File

SOURCE=.\CedaSgmData.h
# End Source File
# Begin Source File

SOURCE=.\CedaSgrData.h
# End Source File
# Begin Source File

SOURCE=.\CedaSorData.h
# End Source File
# Begin Source File

SOURCE=.\CedaSpedata.h
# End Source File
# Begin Source File

SOURCE=.\CedaSpfData.h
# End Source File
# Begin Source File

SOURCE=.\CedaSprData.h
# End Source File
# Begin Source File

SOURCE=.\cedasredata.h
# End Source File
# Begin Source File

SOURCE=.\CedaSteData.h
# End Source File
# Begin Source File

SOURCE=.\CedaStfData.h
# End Source File
# Begin Source File

SOURCE=.\CedaSwgData.h
# End Source File
# Begin Source File

SOURCE=.\CedaTeaData.h
# End Source File
# Begin Source File

SOURCE=.\CedaWgpData.h
# End Source File
# Begin Source File

SOURCE=.\cedawisdata.h
# End Source File
# Begin Source File

SOURCE=.\CedaWorkTime.h
# End Source File
# Begin Source File

SOURCE=.\ChangeShiftDlg.h
# End Source File
# Begin Source File

SOURCE=.\CharacteristicValueDlg.h
# End Source File
# Begin Source File

SOURCE=.\ChildFrm.h
# End Source File
# Begin Source File

SOURCE=.\CodeBigDlg.h
# End Source File
# Begin Source File

SOURCE=.\CodeDlg.h
# End Source File
# Begin Source File

SOURCE=.\ColorControls.h
# End Source File
# Begin Source File

SOURCE=.\COMBOBAR.H
# End Source File
# Begin Source File

SOURCE=.\CommonGridDlg.h
# End Source File
# Begin Source File

SOURCE=.\CViewer.h
# End Source File
# Begin Source File

SOURCE=.\DoubleTourDlg.h
# End Source File
# Begin Source File

SOURCE=.\DraDialog.h
# End Source File
# Begin Source File

SOURCE=.\DrdDialog.h
# End Source File
# Begin Source File

SOURCE=.\DutyAbsenceOverviewDlg.h
# End Source File
# Begin Source File

SOURCE=.\DutyAccountDlg.h
# End Source File
# Begin Source File

SOURCE=.\DutyAttentionDlg.h
# End Source File
# Begin Source File

SOURCE=.\DutyBsdDlg.h
# End Source File
# Begin Source File

SOURCE=.\dutychangegroupdlg.h
# End Source File
# Begin Source File

SOURCE=.\DutyDebitTabViewer.h
# End Source File
# Begin Source File

SOURCE=.\DutyDragDropDlg.h
# End Source File
# Begin Source File

SOURCE=.\DutyDragDropTabViewer.h
# End Source File
# Begin Source File

SOURCE=.\DutyIsTabViewer.h
# End Source File
# Begin Source File

SOURCE=.\DutyPlanAbsencePp.h
# End Source File
# Begin Source File

SOURCE=.\DutyPlanPs.h
# End Source File
# Begin Source File

SOURCE=.\dutyprintdlg.h
# End Source File
# Begin Source File

SOURCE=.\DutyReleaseDlg.h
# End Source File
# Begin Source File

SOURCE=.\DutyRoster_Frm.h
# End Source File
# Begin Source File

SOURCE=.\DutyRoster_View.h
# End Source File
# Begin Source File

SOURCE=.\DutyRosterPropertySheet.H
# End Source File
# Begin Source File

SOURCE=.\DutyShowJobsDlg.h
# End Source File
# Begin Source File

SOURCE=.\DutySolidTabViewer.h
# End Source File
# Begin Source File

SOURCE=.\DutyStatTabViewer.h
# End Source File
# Begin Source File

SOURCE=.\Empldlg.h
# End Source File
# Begin Source File

SOURCE=.\ExcelExport.h
# End Source File
# Begin Source File

SOURCE=.\ExcelExportDlg.h
# End Source File
# Begin Source File

SOURCE=.\gridcontrol.h
# End Source File
# Begin Source File

SOURCE=.\GridEmplDlg.h
# End Source File
# Begin Source File

SOURCE=.\Groups.h
# End Source File
# Begin Source File

SOURCE=.\GUILng.h
# End Source File
# Begin Source File

SOURCE=.\ImportShifts.h
# End Source File
# Begin Source File

SOURCE=.\InfoBoxDlg.h
# End Source File
# Begin Source File

SOURCE=.\InfoDlg.h
# End Source File
# Begin Source File

SOURCE=.\InitialLoadDlg.h
# End Source File
# Begin Source File

SOURCE=.\ListBoxDlg.h
# End Source File
# Begin Source File

SOURCE=.\LoginDlg.h
# End Source File
# Begin Source File

SOURCE=.\MainFrm.h
# End Source File
# Begin Source File

SOURCE=.\PfcSelectDlg.h
# End Source File
# Begin Source File

SOURCE=.\Plan_Info.h
# End Source File
# Begin Source File

SOURCE=.\PrintDailyRoster.h
# End Source File
# Begin Source File

SOURCE=.\PrintDailyRosterB.h
# End Source File
# Begin Source File

SOURCE=.\PrivList.h
# End Source File
# Begin Source File

SOURCE=.\PSDutyRoster1ViewPage.h
# End Source File
# Begin Source File

SOURCE=.\PSDutyRoster2ViewPage.h
# End Source File
# Begin Source File

SOURCE=.\PSDutyRoster3ViewPage.h
# End Source File
# Begin Source File

SOURCE=.\PSDutyRoster4ViewPage.h
# End Source File
# Begin Source File

SOURCE=.\PSDutyRoster5ViewPage.h
# End Source File
# Begin Source File

SOURCE=.\PSShiftRoster1ViewPage.h
# End Source File
# Begin Source File

SOURCE=.\PSShiftRoster2ViewPage.h
# End Source File
# Begin Source File

SOURCE=.\psshiftroster3viewpage.h
# End Source File
# Begin Source File

SOURCE=.\RegisterDlg.h
# End Source File
# Begin Source File

SOURCE=.\ReportCompensation.h
# End Source File
# Begin Source File

SOURCE=.\Resource.h
# PROP Ignore_Default_Tool 1
# End Source File
# Begin Source File

SOURCE=.\Rostering.h
# End Source File
# Begin Source File

SOURCE=.\RosteringDoc.h
# End Source File
# Begin Source File

SOURCE=.\RosteringView.h
# End Source File
# Begin Source File

SOURCE=.\RosterViewPage.h
# End Source File
# Begin Source File

SOURCE=.\SelectDateDlg.h
# End Source File
# Begin Source File

SOURCE=.\SelectShiftDlg.h
# End Source File
# Begin Source File

SOURCE=.\SendToSapDlg.h
# End Source File
# Begin Source File

SOURCE=.\SetupDlg.h
# End Source File
# Begin Source File

SOURCE=.\shiftawdlg.h
# End Source File
# Begin Source File

SOURCE=.\shiftawdlgbonus.h
# End Source File
# Begin Source File

SOURCE=.\ShiftCheck.H
# End Source File
# Begin Source File

SOURCE=.\ShiftCodeNumberDlg.h
# End Source File
# Begin Source File

SOURCE=.\ShiftKonsistDlg.h
# End Source File
# Begin Source File

SOURCE=.\ShiftNewGSPDlg.h
# End Source File
# Begin Source File

SOURCE=.\ShiftPlanDlg.h
# End Source File
# Begin Source File

SOURCE=.\ShiftPrintDlg.h
# End Source File
# Begin Source File

SOURCE=.\ShiftReleaseDlg.h
# End Source File
# Begin Source File

SOURCE=.\ShiftRoster_Frm.h
# End Source File
# Begin Source File

SOURCE=.\ShiftRoster_View.h
# End Source File
# Begin Source File

SOURCE=.\ShiftRosterPropertySheet.H
# End Source File
# Begin Source File

SOURCE=.\StdAfx.h
# End Source File
# Begin Source File

SOURCE=.\StringConst.h
# End Source File
# Begin Source File

SOURCE=.\TABLE.H
# End Source File
# Begin Source File

SOURCE=.\TageslisteDlg.h
# End Source File
# Begin Source File

SOURCE=.\UDlgStatusBar.h
# End Source File
# Begin Source File

SOURCE=.\WaitDlg.h
# End Source File
# Begin Source File

SOURCE=.\WishDialog.h
# End Source File
# Begin Source File

SOURCE=.\XPaneText.h
# End Source File
# Begin Source File

SOURCE=.\XStatusBar.h
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe"
# Begin Source File

SOURCE=.\res\abb.bmp
# End Source File
# Begin Source File

SOURCE=.\res\Abb.ico
# End Source File
# Begin Source File

SOURCE=.\res\bmp00001.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00002.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00003.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00006.bmp
# End Source File
# Begin Source File

SOURCE=.\res\disa_m.bmp
# End Source File
# Begin Source File

SOURCE=.\res\dutyfram.bmp
# End Source File
# Begin Source File

SOURCE=.\res\exclamat.bmp
# End Source File
# Begin Source File

SOURCE=.\res\exclamation.ico
# End Source File
# Begin Source File

SOURCE=.\res\go_m.bmp
# End Source File
# Begin Source File

SOURCE=.\res\legende.bmp
# End Source File
# Begin Source File

SOURCE=.\res\Rostering.ico
# End Source File
# Begin Source File

SOURCE=.\res\Rostering.rc2
# End Source File
# Begin Source File

SOURCE=.\res\RosteringDoc.ico
# End Source File
# Begin Source File

SOURCE=.\res\shiftfra.bmp
# End Source File
# Begin Source File

SOURCE=.\res\Toolbar.bmp
# End Source File
# Begin Source File

SOURCE=.\res\toolbar1.bmp
# End Source File
# Begin Source File

SOURCE=.\res\wait_m.bmp
# End Source File
# Begin Source File

SOURCE=.\res\wait_m1.bmp
# End Source File
# End Group
# Begin Group "Help Files"

# PROP Default_Filter "cnt;rtf"
# End Group
# Begin Source File

SOURCE=.\DataComp.avi
# End Source File
# Begin Source File

SOURCE=.\filecopy.avi
# End Source File
# Begin Source File

SOURCE=.\ReadMe.txt
# End Source File
# End Target
# End Project
