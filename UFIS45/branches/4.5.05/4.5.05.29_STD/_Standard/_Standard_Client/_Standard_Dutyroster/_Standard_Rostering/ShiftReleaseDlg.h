#if !defined(AFX_SHIFTRELEASEDLG_H__2B268853_304B_11D7_80B3_0001022205E4__INCLUDED_)
#define AFX_SHIFTRELEASEDLG_H__2B268853_304B_11D7_80B3_0001022205E4__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ShiftReleaseDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// ShiftReleaseDlg dialog

class ShiftReleaseDlg : public CDialog
{
// Construction
public:
	void InitStrings();
	ShiftReleaseDlg(CWnd* pParent = NULL, bool bpWithWorkGroup = false);   // standard constructor
	~ShiftReleaseDlg();

// Dialog Data
	//{{AFX_DATA(ShiftReleaseDlg)
	enum { IDD = IDD_SHIFT_RELEASE_DLG };
	CCSEdit	m_DayTo;
	CCSEdit	m_DayFrom;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(ShiftReleaseDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(ShiftReleaseDlg)
	afx_msg void OnAll();
	afx_msg void OnNone();
	afx_msg void OnBFind();
	afx_msg void OnClose();
	afx_msg void OnRelease();
	afx_msg void OnDelete();
	afx_msg void OnBMax();
	afx_msg void OnBMin();
	afx_msg	void OnActiveGPL();	
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

protected:
	friend class ShiftRoster_View;

private:
	CMapStringToPtr pomStfMap;
	void FillGrid();
	void InitGrid();
	CGridControl* pomGrid;
	ShiftRoster_View *pomParent;
	CString omSplu;
	bool bmWithWorkGroup;
	CString	omGplu;

	void DoRelease (bool bpDelete);
	void GetSelectedItems(CMapStringToPtr *ropStaffMap);
	bool CheckDateInput(COleDateTime *popFrom, COleDateTime *popTo);

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SHIFTRELEASEDLG_H__2B268853_304B_11D7_80B3_0001022205E4__INCLUDED_)
