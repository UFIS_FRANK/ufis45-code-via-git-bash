#if !defined(AFX_SHIFTPRINTDLG_H__4DB83B8C_6585_48E1_BB2E_D5023AD20CB4__INCLUDED_)
#define AFX_SHIFTPRINTDLG_H__4DB83B8C_6585_48E1_BB2E_D5023AD20CB4__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ShiftPrintDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// ShiftPrintDlg dialog

class ShiftPrintDlg : public CDialog
{
// Construction
public:
	ShiftPrintDlg(CWnd* pParent = NULL,bool bpPrintBSR = true);   // standard constructor

// Dialog Data
	//{{AFX_DATA(ShiftPrintDlg)
	enum { IDD = IDD_SHIFT_PRINT };
	int			m_PrintType;
	int			m_PrintBSRType;
	int		m_PrintBSRSHIFTType;
	int			m_PrintOverviewType;
	CCSEdit		m_PrintOverviewFrom;
	CCSEdit		m_PrintOverviewTo;
	int			m_PrintOrientation;
	CCSEdit		m_PrintWorkGroupsAt;
	CListBox	m_PrintWorkGroupsList;
	CCSEdit		m_PrintWorkGroupsComment;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(ShiftPrintDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(ShiftPrintDlg)
	afx_msg void OnRBsr();
	afx_msg void OnROverview();
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	afx_msg void OnButtonTo();
	afx_msg void OnButtonFrom();
	afx_msg void OnButtonAt();
	afx_msg void OnRWorkGroups();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	void	PrintBasicShiftRoster();
	void	PrintShiftRosterOverview();
	void	PrintWorkGroups();
	void	ActivatePrintType(int ipPrintType);
private:
	ShiftRoster_View*	pomParent;
	COleDateTime		omFrom;
	COleDateTime		omTo;
	int					imOleDateWeekDay;
	int					imCalCtrlWeekDay;
	COleDateTime		omStart;
	COleDateTime		omEnd;
	COleDateTime		omAt;
	bool				bmPrintBSR;

	static	CString		omLastComment;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SHIFTPRINTDLG_H__4DB83B8C_6585_48E1_BB2E_D5023AD20CB4__INCLUDED_)
