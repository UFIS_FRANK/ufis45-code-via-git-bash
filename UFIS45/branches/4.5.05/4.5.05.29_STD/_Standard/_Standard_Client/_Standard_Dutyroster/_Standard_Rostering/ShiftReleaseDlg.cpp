// ShiftReleaseDlg.cpp : implementation file
//

#include "stdafx.h"
#include "rostering.h"
#include "ShiftReleaseDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// ShiftReleaseDlg dialog
#define WORKGROUP_WIDTH 82

ShiftReleaseDlg::ShiftReleaseDlg(CWnd* pParent /*=NULL*/, bool bpWithWorkGroup /*= false*/)
	: CDialog(ShiftReleaseDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(ShiftReleaseDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	CCS_TRY;
	pomParent = (ShiftRoster_View*)pParent;
	pomStfMap.RemoveAll();
	omSplu = pomParent->GetSelectedShiftplanUrno();
	omGplu = pomParent->GetActiveBasicShiftRoster();
	bmWithWorkGroup = bpWithWorkGroup;
	CCS_CATCH_ALL;
}

ShiftReleaseDlg::~ShiftReleaseDlg()
{
CCS_TRY;
	pomStfMap.RemoveAll();

	// Grid l�schen
	if (pomGrid) 
	{
		delete pomGrid;
		pomGrid = 0;
	}
CCS_CATCH_ALL;
}

void ShiftReleaseDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(ShiftReleaseDlg)
	DDX_Control(pDX, IDC_DAYTO, m_DayTo);
	DDX_Control(pDX, IDC_DAYFROM, m_DayFrom);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(ShiftReleaseDlg, CDialog)
	//{{AFX_MSG_MAP(ShiftReleaseDlg)
	ON_BN_CLICKED(IDC_ALL, OnAll)
	ON_BN_CLICKED(IDC_NONE, OnNone)
	ON_BN_CLICKED(IDC_B_FIND, OnBFind)
	ON_BN_CLICKED(IDC_CLOSE, OnClose)
	ON_BN_CLICKED(IDC_RELEASE, OnRelease)
	ON_BN_CLICKED(IDC_DELETE, OnDelete)
	ON_BN_CLICKED(IDC_B_MAX, OnBMax)
	ON_BN_CLICKED(IDC_B_MIN, OnBMin)
	ON_BN_CLICKED(IDC_ACTIVE_GPL,OnActiveGPL)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// ShiftReleaseDlg message handlers

void ShiftReleaseDlg::OnAll() 
{
CCS_TRY;
	POSITION area = pomGrid->GetParam( )->GetRangeList( )->AddTail(new CGXRange);
	pomGrid->SetSelection(area,1, 1, pomGrid->GetRowCount(),RELEASECOLCOUNT);
CCS_CATCH_ALL;
}

void ShiftReleaseDlg::OnNone() 
{
CCS_TRY;
	pomGrid->SetSelection(NULL,0,0,0,0);
CCS_CATCH_ALL;
}

void ShiftReleaseDlg::OnBFind() 
{
	if (pomGrid)
	{
		pomGrid->OnShowFindReplaceDialog(TRUE);
	}
}

void ShiftReleaseDlg::OnClose() 
{
CCS_TRY;
	// Grid beenden
	pomGrid->DestroyWindow() ;
	CDialog::OnCancel();
CCS_CATCH_ALL;	
}

void ShiftReleaseDlg::OnRelease() 
{
	DoRelease(false);
}

void ShiftReleaseDlg::OnDelete() 
{
	DoRelease(true);
	
}

void ShiftReleaseDlg::OnBMax() 
{
	COleDateTime olFrom;
	COleDateTime olTo;
	if (pomParent->GetMinMaxGplTimesBySplu(omSplu, &olFrom, &olTo) == true)
	{
		m_DayTo.SetInitText(olTo.Format("%d.%m.%Y"));
	}
}

void ShiftReleaseDlg::OnBMin() 
{
	COleDateTime olFrom;
	COleDateTime olTo;
	if (pomParent->GetMinMaxGplTimesBySplu(omSplu, &olFrom, &olTo) == true)
	{
		m_DayTo.SetInitText(olFrom.Format("%d.%m.%Y"));
	}
}

BOOL ShiftReleaseDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	if (bmWithWorkGroup == true)
	{
		// --- resizing the form
		CRect olRect;
		GetWindowRect (olRect);
		ClientToScreen (olRect);
		MoveWindow(olRect.left, olRect.top, olRect.Width()+WORKGROUP_WIDTH, olRect.Height());
		CenterWindow();

		// --- resizing the grid
		GetDlgItem(IDC_GRID)->GetWindowRect(olRect);
		ScreenToClient (olRect);
		GetDlgItem(IDC_GRID)->MoveWindow(olRect.left, olRect.top, olRect.Width()+WORKGROUP_WIDTH, olRect.Height());
	}

	InitStrings();

	InitGrid();

	if (this->omGplu == "0")
	{
		GetDlgItem(IDC_ACTIVE_GPL)->EnableWindow(FALSE);
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void ShiftReleaseDlg::InitStrings()
{
	CCS_TRY;

	SetWindowText(LoadStg(IDS_STRING59394));

	SetDlgItemText(IDC_STATIC_TIME,LoadStg(IDS_STRING59395));
	SetDlgItemText(IDC_STATIC_EMPLOYEE,LoadStg(IDS_STRING59396));
	SetDlgItemText(IDC_ALL,LoadStg(IDS_STRING59397));
	SetDlgItemText(IDC_NONE,LoadStg(IDS_STRING59398));
	SetDlgItemText(IDC_B_FIND,LoadStg(IDS_STRING59399));
	SetDlgItemText(IDC_STATIC_ACTION,LoadStg(IDS_STRING59400));
	SetDlgItemText(IDC_RELEASE,LoadStg(IDS_STRING59401));
	SetDlgItemText(IDC_DELETE,LoadStg(IDS_STRING59402));
	SetDlgItemText(IDC_CLOSE,LoadStg(IDS_STRING59403));
	SetDlgItemText(IDC_ACTIVE_GPL,LoadStg(IDS_STRING63505));

	COleDateTime olFrom;
	COleDateTime olTo;
	if (pomParent->GetMinMaxGplTimesBySplu(omSplu, &olFrom, &olTo) == true)
	{
		if (olFrom.GetStatus() == COleDateTime::valid && olTo.GetStatus() == COleDateTime::valid)
		{
			// check if current day is between from and to
			COleDateTime olCurrent = COleDateTime::GetCurrentTime();
			if (olCurrent > olFrom && olCurrent < olTo)
			{
				m_DayFrom.SetInitText(olCurrent.Format("%d.%m.%Y"));
			}
			else
			{
				m_DayFrom.SetInitText(olFrom.Format("%d.%m.%Y"));
				olCurrent = olFrom;
			}

			// check if calculated release duration exceed a single month
			COleDateTimeSpan dura = olTo - olCurrent;
			if (dura.GetTotalDays() > 30)
			{
				olTo = olCurrent + COleDateTimeSpan(30,0,0,0);
			}

			m_DayTo.SetInitText(olTo.Format("%d.%m.%Y"));
		}
	}
	CCS_CATCH_ALL;
}

void ShiftReleaseDlg::InitGrid()
{
	CCS_TRY;
	int ilCountStaff;
	ilCountStaff = pomParent->GetStaffUrnosBySplUrno(pomParent->GetSelectedShiftplanUrno(), &pomStfMap);

	// Grid erzeugen
	if (bmWithWorkGroup == false)
	{
		pomGrid = new CGridControl (this, IDC_GRID,RELEASECOLCOUNT-1, ilCountStaff);
	}
	else
	{
		pomGrid = new CGridControl (this, IDC_GRID,RELEASECOLCOUNT, ilCountStaff);
	}

	// �berschriften setzen
	pomGrid->SetValueRange(CGXRange(0,1), LoadStg(SHIFT_STF_PENO));
	pomGrid->SetValueRange(CGXRange(0,2), LoadStg(SHIFT_S_STAFF));
	pomGrid->SetValueRange(CGXRange(0,3), LoadStg(SHIFT_STF_FUNCTION));

	// Breite der Spalten einstellen
	pomGrid->SetColWidth (0, 0, 35);
	pomGrid->SetColWidth (1, 1, 105);
	pomGrid->SetColWidth (2, 2, 220);
	pomGrid->SetColWidth (3, 3, 235);

	if (bmWithWorkGroup == false)
	{
		pomGrid->SetValueRange(CGXRange(0,4), LoadStg(IDS_STRING139));
		pomGrid->SetValueRange(CGXRange(0,5), LoadStg(IDS_STRING140));

		pomGrid->SetColWidth (4, 4, 70);
		pomGrid->SetColWidth (5, 5, 70);
	}
	else
	{
		pomGrid->SetValueRange(CGXRange(0,4), LoadStg(SHIFT_STF_WGRP/*IDS_STRING117*/));
		pomGrid->SetValueRange(CGXRange(0,5), LoadStg(IDS_STRING139));
		pomGrid->SetValueRange(CGXRange(0,6), LoadStg(IDS_STRING140));

		pomGrid->SetColWidth (4, 4, pomGrid->Width_LPtoDP(14 * GX_NXAVGWIDTH));	//working group
		//pomGrid->SetColWidth (4, 4, WORKGROUP_WIDTH);
		pomGrid->SetColWidth (5, 5, 70);
		pomGrid->SetColWidth (6, 6, 70);
	}

	// Grid mit Daten f�llen.
	if (ilCountStaff > 0)
	{
		FillGrid();
	}

	pomGrid->Initialize();
	pomGrid->GetParam()->EnableUndo(FALSE);
	pomGrid->LockUpdate(TRUE);
	pomGrid->LockUpdate(FALSE);
	pomGrid->GetParam()->EnableUndo(TRUE);

	// Verhindert, das die Spaltenbreite ver�ndert wird
	pomGrid->GetParam()->EnableTrackColWidth(FALSE);

	// Es k�nnen nur ganze Zeilen markiert werden.
	pomGrid->GetParam()->EnableSelection(GX_SELROW | GX_SELMULTIPLE | GX_SELSHIFT);

	pomGrid->SetStyleRange( CGXRange().SetTable(), CGXStyle()
				.SetVerticalAlignment(DT_VCENTER)
				.SetReadOnly(TRUE));
	pomGrid->EnableAutoGrow (FALSE);
	CCS_CATCH_ALL;
}

void ShiftReleaseDlg::FillGrid()
{
	long llStfUrno;
	void  *prlVoid = NULL;
	POSITION rlPos;
	CString olTmp;
	STFDATA *polStfData;
	CString olSurn;
	CString olFullName;
	CString *prlResult = NULL;
	int ilGPLvafrIdx = ogBCD.GetFieldIndex("GPL","VAFR");
	int ilGplRecordNumber;

	//alle Funktionen in 'ne Map packen
	CMapStringToPtr olSpfMap;
	COleDateTime olFrom;
	COleDateTime olReferenceDate;
	CTime olCTimeFrom = -1;

	SPFDATA *polSpfData = NULL;
	RecordSet *polGPLRecord = new RecordSet(ogBCD.GetFieldCount("GPL"));

	int ilSpfCount =  ogSpfData.omData.GetSize();
	for (int ilSpf = 0; ilSpf < ilSpfCount; ilSpf++)
	{
		polSpfData = &ogSpfData.omData[ilSpf];
		olSurn.Format("%ld",polSpfData->Surn);

		if (pomStfMap.Lookup(olSurn,(void *&)ilGplRecordNumber) == TRUE)
		{
			ogBCD.GetRecord("GPL",ilGplRecordNumber, *polGPLRecord);

			// get the reference date (first day of the basic shift roster the employee is assigned to)
			olTmp = polGPLRecord->Values[ilGPLvafrIdx];
			olCTimeFrom = DBStringToDateTime(olTmp);
			if (olCTimeFrom != -1)
			{
				olReferenceDate.SetDateTime(olCTimeFrom.GetYear(), olCTimeFrom.GetMonth(), olCTimeFrom.GetDay(), olCTimeFrom.GetHour(), olCTimeFrom.GetMinute(), olCTimeFrom.GetSecond());

				// check valid from and valid to
				if (polSpfData->Vpfr <= olReferenceDate && 
				   (polSpfData->Vpto.GetStatus() == COleDateTime::null || 
					polSpfData->Vpto.GetStatus() == COleDateTime::invalid || 
					polSpfData->Vpto >= olReferenceDate))
				{
					if (olSpfMap.Lookup(olSurn, (void *&)prlResult) == TRUE)
					{
						*prlResult +=  "," + (CString)polSpfData->Fctc;
					}
					else
					{
						CString *polCode = new CString;
						*polCode = (CString)polSpfData->Fctc;
						olSpfMap.SetAt(olSurn, polCode);
					}
				}
			}
		}
	}

	CMapStringToPtr olSwgMap;
	if (bmWithWorkGroup == true)
	{
		int ilSwgCount = ogSwgData.omData.GetSize();
		SWGDATA *prlSwgData = NULL;

		for (int iSwg = 0; iSwg < ilSwgCount; iSwg++)
		{
			prlSwgData = &ogSwgData.omData[iSwg];
			olSurn.Format("%ld",prlSwgData->Surn);

			// check valid from and valid to
			if (prlSwgData->Vpfr <= olReferenceDate && 
			   (prlSwgData->Vpto.GetStatus() == COleDateTime::null || 
				prlSwgData->Vpto.GetStatus() == COleDateTime::invalid || 
				prlSwgData->Vpto >= olReferenceDate))
			{
				if(olSwgMap.Lookup(olSurn,(void *&)prlResult) == TRUE)
				{
					*prlResult +=  "," + (CString)ogSwgData.omData[iSwg].Code;
				}
				else
				{
					CString *polCode = new CString;
					*polCode = (CString)ogSwgData.omData[iSwg].Code;
					olSwgMap.SetAt(olSurn,polCode);
				}
			}
		}
	}

	// Zeilen f�r alle MAs einf�gen
	int i = 0;
	for (rlPos = pomStfMap.GetStartPosition(); rlPos != NULL; )
	{
		pomStfMap.GetNextAssoc(rlPos, olTmp, (void *&)prlVoid);
		llStfUrno = atol(olTmp.GetBuffer(0));

		polStfData = ogStfData.GetStfByUrno(llStfUrno);
		if (polStfData != NULL)
		{
			// Urno mit erstem Feld koppeln.						
			pomGrid->SetStyleRange (CGXRange(i+1,1), CGXStyle().SetItemDataPtr((void*)llStfUrno));
			pomGrid->SetValueRange(CGXRange(i+1,1), polStfData->Peno);
			// Name und Vorname
			olFullName = pomParent->GetFormatedEmployeeName(polStfData);
			olSurn.Format("%ld", llStfUrno);

			pomGrid->SetValueRange(CGXRange(i+1,2), olFullName);
			if (bmWithWorkGroup == false)
			{
				pomGrid->SetValueRange(CGXRange(i+1,4), polStfData->Doem.Format("%d.%m.%Y"));
				pomGrid->SetValueRange(CGXRange(i+1,5), polStfData->Dodm.Format("%d.%m.%Y"));
			}
			else
			{
				//Working groups
				if(olSwgMap.Lookup(olSurn,(void *&)prlResult) == TRUE)
				{
					pomGrid->SetValueRange(CGXRange(i+1,4), *prlResult);
				}

				pomGrid->SetValueRange(CGXRange(i+1,5), polStfData->Doem.Format("%d.%m.%Y"));
				pomGrid->SetValueRange(CGXRange(i+1,6), polStfData->Dodm.Format("%d.%m.%Y"));
			}

			// Funktion(en)
			if(olSpfMap.Lookup(olSurn,(void *&)prlResult) == TRUE)
			{
				// Liste der Functions eintragen;
				pomGrid->SetValueRange(CGXRange(i+1,3), *prlResult);
			}
		}

		i++; //counter of the rows
	}

	//Delete Maps
	CString *prlString;
	CString olText;
	POSITION olPos;
	for(olPos = olSpfMap.GetStartPosition(); olPos != NULL; )
	{
		olSpfMap.GetNextAssoc(olPos,olText,(void*&)prlString);
		delete prlString;
	}
	olSpfMap.RemoveAll();
	for(olPos = olSwgMap.GetStartPosition(); olPos != NULL; )
	{
		olSwgMap.GetNextAssoc(olPos,olText,(void*&)prlString);
		delete prlString;
	}
	olSwgMap.RemoveAll();
}

void ShiftReleaseDlg::GetSelectedItems(CMapStringToPtr *ropStaffMap)
{
CCS_TRY;

	CGXStyle		olStyle;
	CString			olUrno;
	long			llUrno;
	CRowColArray	olRowColArray;

	ropStaffMap->RemoveAll();

	// Selektion ausw�hlen
	pomGrid->GetSelectedRows (olRowColArray,false,false);

	// Wurden Zeilen ausgew�hlt
	if (olRowColArray.GetSize() == 0)
	{
		TRACE("Keine Zeilen ausgew�hlt.\n");
		return;
	}

	// F�r jede ausgew�hlte Zeile abarbeiten
	for (int i = 0; i < olRowColArray.GetSize(); i++)
	{
		// Style und damit Datenpointer einer Zelle erhalten
		pomGrid->ComposeStyleRowCol(olRowColArray[i],1, &olStyle );
		// Urno erhalten
		llUrno = (long)olStyle.GetItemDataPtr();
		olUrno.Format("%ld", llUrno);
		if (olUrno != "0")
		{
			ropStaffMap->SetAt(olUrno, NULL);
		}
		else
		{
			TRACE ("Urno=0 in ShiftReleaseDlg! (Zeile%d)\n",olRowColArray[i]);
		}
	}
CCS_CATCH_ALL;
}

void ShiftReleaseDlg::DoRelease (bool bpDelete)
{
CCS_TRY;
	COleDateTime olFromDay;
	COleDateTime olToDay;
	CString olErrorText;
	CString olMsg;
	bool blStatus = true;

	if (CheckDateInput(&olFromDay, &olToDay) == false)
	{
		return;
	}

	// selektierten MAs aus Grid holen
	CMapStringToPtr olStaffMap;
	GetSelectedItems(&olStaffMap);

	if (olStaffMap.GetCount() < 1)
	{
		olErrorText += LoadStg(IDS_STRING1573);
		blStatus = false;
	}

	if (blStatus != true)
	{
		// Fehlermeldung ausgeben
		MessageBeep((UINT)-1);
		MessageBox (olErrorText, LoadStg(ST_FEHLER), MB_ICONEXCLAMATION);
		return;
	}

	if (bpDelete)
	{
		//Do you want to delete Shift Roster?*INTXT*
		olMsg = LoadStg(IDS_STRING1668);
	}
	else
	{
		//Do you want to release Shift Roster?*INTXT*
		olMsg = LoadStg(IDS_STRING1666);
	}

	int ilMBoxReturn = MessageBox(olMsg,LoadStg(ST_FRAGE),(MB_ICONQUESTION | MB_OKCANCEL | MB_DEFBUTTON1));
	if (ilMBoxReturn == IDCANCEL)
	{
		return;
	}

	pomParent->Release(omSplu, olFromDay, olToDay, &olStaffMap, bpDelete);

CCS_CATCH_ALL;
}

bool ShiftReleaseDlg::CheckDateInput(COleDateTime *popFrom, COleDateTime *popTo)
{
	CCS_TRY;
	bool blStatus = true;
	CString olTmp;
	CString	olErrorText;
	CString olFrom;
	CString olTo;
	CTime olTmpFrom;
	CTime olTmpTo;

	//-----------------------------------------------------------------------------------------
	//Check all User input
	//-----------------------------------------------------------------------------------------
	m_DayFrom.GetWindowText(olFrom);
	m_DayTo.GetWindowText(olTo);

	if(m_DayFrom.GetStatus() == false || m_DayTo.GetStatus() == false)
	{
		blStatus = false;
		olErrorText += LoadStg(IDS_STRING1571);
	}
	else
	{
		if (olFrom.IsEmpty() && olTo.IsEmpty())
		{
			blStatus = false;
			olErrorText += LoadStg(IDS_STRING1571);
		}
		else
		{
			if (olTo.IsEmpty())
			{
				olTo = olFrom;
			}
			olTmp = "00:00";
			olTmpFrom = DateHourStringToDate(olFrom,olTmp);
			olTmpTo   = DateHourStringToDate(olTo,olTmp);
			if (olTmpTo < olTmpFrom || (olTmpFrom == -1 || olTmpTo == -1))
			{
				blStatus = false;
				olErrorText += LoadStg(IDS_STRING1571);
			}
			else
			{
				popFrom->SetDate(olTmpFrom.GetYear(),olTmpFrom.GetMonth(),olTmpFrom.GetDay());
				popTo->SetDate(olTmpTo.GetYear(),olTmpTo.GetMonth(),olTmpTo.GetDay());
			}
		}
	}

	if (blStatus != true)
	{
		// Fehlermeldung ausgeben
		MessageBeep((UINT)-1);
		olErrorText += "\n" + olFrom + " - " + olTo;
		MessageBox (olErrorText, LoadStg(ST_FEHLER), MB_ICONEXCLAMATION);
	}
	return blStatus;
CCS_CATCH_ALL;
	return false;
}


void ShiftReleaseDlg::OnActiveGPL()
{
	if (pomGrid == NULL)
		return;

	CMapStringToPtr olStfMap;
	int ilCountStaff = pomParent->GetStaffUrnosByGplUrno(pomParent->GetActiveBasicShiftRoster(), &olStfMap);
	if (ilCountStaff == 0)
		return;

	CGXStyle		olStyle;
	CString			olUrno;
	long			llUrno;
	void*			olValue;

	pomGrid->SetSelection(NULL,0,0,0,0);


	for (int i = 1; i <= pomGrid->GetRowCount(); i++)
	{
		// Style und damit Datenpointer einer Zelle erhalten
		pomGrid->ComposeStyleRowCol(i,1, &olStyle);
		// Urno erhalten
		llUrno = (long)olStyle.GetItemDataPtr();
		olUrno.Format("%ld", llUrno);
		if (olStfMap.Lookup(olUrno,olValue))
		{
			POSITION area = pomGrid->GetParam( )->GetRangeList( )->AddTail(new CGXRange);
			pomGrid->SetSelection(area,i, 1,i,RELEASECOLCOUNT);
		}
	}


}
