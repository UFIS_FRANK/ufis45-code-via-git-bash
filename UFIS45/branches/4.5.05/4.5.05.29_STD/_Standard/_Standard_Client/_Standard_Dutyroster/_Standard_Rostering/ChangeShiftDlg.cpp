// ChangeShiftDlg.cpp: Implementierungsdatei
//

#include <stdafx.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define	WITH_UNIQUE_INDEX	1

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CChangeShiftDlg 

CChangeShiftDlg::CChangeShiftDlg(CWnd* pParent, COleDateTime opDate, long lpStfUrno, CString opRosl)
	: CDialog(CChangeShiftDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CChangeShiftDlg)
	m_DatePicker1 = opDate;
	m_DatePicker2 = opDate;
	m_EditEmpl1 = _T("");
	m_EditEmpl2 = _T("");
	//}}AFX_DATA_INIT

	lmEmpl1 = lpStfUrno;
	lmEmpl2 = 0;

	lmOldBsdu1 = lmOldBsdu2 = 0;
	lmNewBsdu1 = lmNewBsdu2 = 0;

	pomDrr1 = pomDrr2 = 0;

	omOldScod1.Empty();
	omOldScod2.Empty();

	omNewScod1.Empty();
	omNewScod2.Empty();

	omFctc1.Empty();
	omFctc2.Empty();

	omRosl		= opRosl;
	pomParent	= (DutyRoster_View*)pParent;
}

//********************************************************************************
// 
//********************************************************************************

BOOL CChangeShiftDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	CRect olRect;

	// the working group, function and SUB1/SUB2 is only available for SIN, else we have to hide it.
	if (ogCCSParam.GetParamValue(ogAppl,"ID_PROD_CUSTOMER") != "SIN")
	{
		GetDlgItem (IDC_S_SWAP_SUB1SUB2)->ShowWindow(SW_HIDE);
		GetDlgItem (IDC_C_SWAP_SUB_1)->ShowWindow(SW_HIDE);
		GetDlgItem (IDC_C_SWAP_SUB_2)->ShowWindow(SW_HIDE);
		GetDlgItem (IDC_S_WGPC)->ShowWindow(SW_HIDE);
		GetDlgItem (IDC_C_WGPC1)->ShowWindow(SW_HIDE);
		GetDlgItem (IDC_C_WGPC2)->ShowWindow(SW_HIDE);
		GetDlgItem (IDC_S_SWAP_FCTC)->ShowWindow(SW_HIDE);
		GetDlgItem (IDC_C_FCTC1)->ShowWindow(SW_HIDE);
		GetDlgItem (IDC_C_FCTC2)->ShowWindow(SW_HIDE);

		// resizing the form
		GetWindowRect (olRect);
		ClientToScreen (olRect);
		MoveWindow (olRect.left, olRect.top, 443, olRect.Height());
		CenterWindow();
	}
	else
	{
		// set combos
		SetCombos();
	}

	if (ogCCSParam.GetParamValue(ogAppl,"ID_PROD_CUSTOMER") != "ATH")
	{
		// hiding the second DateTime control
		GetDlgItem (IDC_STATIC_DATE2)->ShowWindow(SW_HIDE);
		GetDlgItem (IDC_DATETIMEPICKER2)->ShowWindow(SW_HIDE);

		// resizing the form
		GetWindowRect (olRect);
		ClientToScreen (olRect);
		MoveWindow (olRect.left, olRect.top, olRect.Width(), olRect.Height() - 30);
		CenterWindow();
	}


	STFDATA *prlStf = ogStfData.GetStfByUrno(lmEmpl1);
	if (prlStf != NULL)
	{
		// set PENO
		SetDlgItemText(IDC_EMPL1_NUMBER,CString(prlStf->Peno));

		// set formated name
		CString olName = CBasicData::GetFormatedEmployeeName(pomParent->rmViewInfo.iEName, prlStf, " ", "");;
		SetDlgItemText(IDC_EMPL1_NAME,olName);

		// set shift
		SetShift(1,prlStf->Urno);
	}

	// set statics
	SetStatics();

	// set titel
	SetWindowText(LoadStg(IDS_STRING84));

	return TRUE;  
}

void CChangeShiftDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CChangeShiftDlg)
	DDX_Control(pDX, IDC_C_WGPC2, m_C_WGPC2);
	DDX_Control(pDX, IDC_C_FCTC2, m_C_FCTC2);
	DDX_Control(pDX, IDC_C_WGPC1, m_C_WGPC1);
	DDX_Control(pDX, IDC_C_FCTC1, m_C_FCTC1);
	DDX_Control(pDX, IDC_C_SWAP_SUB_2, m_C_SUB1SUB2_2);
	DDX_Control(pDX, IDC_C_SWAP_SUB_1, m_C_SUB1SUB2_1);
	DDX_DateTimeCtrl(pDX, IDC_DATETIMEPICKER1, m_DatePicker1);
	DDX_DateTimeCtrl(pDX, IDC_DATETIMEPICKER2, m_DatePicker2);
	DDX_Text(pDX, IDC_EMPL1_NUMBER, m_EditEmpl1);
	DDX_Text(pDX, IDC_EMPL2_NUMBER, m_EditEmpl2);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CChangeShiftDlg, CDialog)
	//{{AFX_MSG_MAP(CChangeShiftDlg)
	ON_BN_CLICKED(IDC_SELECT_EMPL1, OnSelectEmpl1)
	ON_BN_CLICKED(IDC_SELECT_EMPL2, OnSelectEmpl2)
	ON_NOTIFY(DTN_DATETIMECHANGE, IDC_DATETIMEPICKER1, OnDatetimechangeDatetimepicker1)
	ON_NOTIFY(DTN_DATETIMECHANGE, IDC_DATETIMEPICKER2, OnDatetimechangeDatetimepicker2)
	ON_BN_CLICKED(IDC_BUTTONAPPLY, OnButtonapply)
	ON_EN_KILLFOCUS(IDC_EMPL1_NUMBER, OnKillfocusEmpl1Number)
	ON_EN_KILLFOCUS(IDC_EMPL2_NUMBER, OnKillfocusEmpl2Number)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten CChangeShiftDlg 


void CChangeShiftDlg::OnSelectEmpl1() 
{
CCS_TRY;
	CStringArray olCol1,olCol2,olCol3;
	CString olUrno;
	long llStfUrno;
	STFDATA *prlStfData = NULL;

	for (int i = 0; i < pomParent->rmViewInfo.oStfUrnos.GetSize(); i++)
	{
		llStfUrno = atol(pomParent->rmViewInfo.oStfUrnos[i]);

		if (llStfUrno != lmEmpl2)
		{
			prlStfData = ogStfData.GetStfByUrno(llStfUrno);
			if (prlStfData != NULL)
			{
				// Es d�rfen nicht die gleichen Mitarbeiter wie in den anderen Zeilen ausgew�hlt sein
				olUrno = pomParent->rmViewInfo.oStfUrnos[i];
				olCol1.Add(CString(prlStfData->Peno));
				olCol2.Add(CBasicData::GetFormatedEmployeeName(pomParent->rmViewInfo.iEName, prlStfData, "", ""));
				olCol3.Add(olUrno);
			}
		}
	}

	CCommonGridDlg olDlg(LoadStg(SHIFT_S_STAFF),&olCol1, &olCol2, &olCol3, LoadStg(SHIFT_STF_PENO), LoadStg(SHIFT_S_STAFF), this);
	if(olDlg.DoModal() == IDOK)
	{
		// Ergeniss auswerten
		STFDATA *prlStf = ogStfData.GetStfByUrno(atol(olDlg.GetReturnUrno()));
		if(prlStf != NULL)
		{
			SetDlgItemText(IDC_EMPL1_NUMBER,CString(prlStf->Peno));
			OnKillfocusEmpl1Number();
		}
	}
CCS_CATCH_ALL;		
}

//********************************************************************************
// Schicht setzen
//********************************************************************************

void CChangeShiftDlg::OnSelectEmpl2() 
{
CCS_TRY;
	CStringArray olCol1,olCol2,olCol3;
	CString olUrno;
	long llStfUrno;
	STFDATA *prlStfData = NULL;

	for (int i = 0; i < pomParent->rmViewInfo.oStfUrnos.GetSize(); i++)
	{
		llStfUrno = atol(pomParent->rmViewInfo.oStfUrnos[i]);

		if (llStfUrno != lmEmpl1)
		{
			prlStfData = ogStfData.GetStfByUrno(llStfUrno);
			if (prlStfData != NULL)
			{
				// Es d�rfen nicht die gleichen Mitarbeiter wie in den anderen Zeilen ausgew�hlt sein
				olUrno = pomParent->rmViewInfo.oStfUrnos[i];
				olCol1.Add(CString(prlStfData->Peno));
				olCol2.Add(CBasicData::GetFormatedEmployeeName(pomParent->rmViewInfo.iEName, prlStfData, "", ""));
				olCol3.Add(olUrno);
			}
		}
	}

	CCommonGridDlg olDlg(LoadStg(SHIFT_S_STAFF),&olCol1, &olCol2, &olCol3, LoadStg(SHIFT_STF_PENO), LoadStg(SHIFT_S_STAFF), this, true);
	if(olDlg.DoModal() == IDOK)
	{
		// Ergeniss auswerten
		STFDATA *prlStf = ogStfData.GetStfByUrno(atol(olDlg.GetReturnUrno()));
		if(prlStf != NULL)
		{
			SetDlgItemText(IDC_EMPL2_NUMBER,CString(prlStf->Peno));
			OnKillfocusEmpl2Number();
		}
	}
CCS_CATCH_ALL;
}


void CChangeShiftDlg::SetShift(int npEmpl,long lpStfUrno)
{
CCS_TRY;

	long llBsdu;
	CString olScod;
	CString olUses;
	CString olFctc;
	CString olTmp;
	CString olWgpCode;
	int i;

	// Es soll eine Schicht gel�scht werden
	if (lpStfUrno == 0)
	{
		switch (npEmpl)
		{
			case 1:
					pomDrr1 = NULL;
					lmOldBsdu1 = 0;
					omOldScod1 = "";
					SetDlgItemText(IDC_OLD_SHIFT1,"");
					SetDlgItemText(IDC_NEW_NEWSHIFT1,"");
					SetDlgItemText(IDC_NEW_ACCOUNT1,"");
					SetDlgItemText(IDC_OLD_ACCOUNT1,"");
					SetDlgItemText(IDC_SOURCE1,"");
					SetDlgItemText(IDC_C_WGPC1,"");
					SetDlgItemText(IDC_C_FCTC1,"");
					SetDlgItemText(IDC_C_SWAP_SUB_1,"");
					break;
			case 2:
					pomDrr2 = NULL;
					lmOldBsdu2 = 0;
					omOldScod2 = "";
					SetDlgItemText(IDC_OLD_SHIFT2,"");
					SetDlgItemText(IDC_NEW_NEWSHIFT2,"");
					SetDlgItemText(IDC_NEW_ACCOUNT2,"");
					SetDlgItemText(IDC_OLD_ACCOUNT2,"");
					SetDlgItemText(IDC_SOURCE2,"");
					SetDlgItemText(IDC_C_WGPC2,"");
					SetDlgItemText(IDC_C_FCTC2,"");
					SetDlgItemText(IDC_C_SWAP_SUB_2,"");
					break;
		}
		return;
	}

	UpdateData(true);

	// Daten neu laden !
	LoadMonthAndEmpl(m_DatePicker1);

	// Datum berechnen als Sday
	CString olSday = m_DatePicker1.Format("%Y%m%d");
	// DRR finden um Schicht zu erhalten (immer Tagesbearbeitung)
	DRRDATA* popDrr = ogDrrData.GetDrrByKey (olSday, lpStfUrno, "1", omRosl);

	olUses = "";

	if (popDrr == NULL)
	{
		llBsdu = 0; 
		olScod.Empty();
		olFctc.Empty();
	}
	else
	{
		llBsdu = popDrr->Bsdu; 
		olScod = popDrr->Scod;
		olFctc = popDrr->Fctc;
	}

	switch (npEmpl)
	{
		case 1:
				pomDrr1 = popDrr;
				lmOldBsdu1 = llBsdu;
				omOldScod1 = olScod;
				SetDlgItemText(IDC_OLD_SHIFT1,olScod);
				SetDlgItemText(IDC_SOURCE1,olUses);
				if (ogCCSParam.GetParamValue(ogAppl,"ID_PROD_CUSTOMER") == "SIN" && pomDrr1 != NULL)
				{
					// setting SUB1/SUB2
					if (strcmp(pomDrr1->Drs2, "2") == 0)
					{
						m_C_SUB1SUB2_1.SetCurSel(1);
					}
					else
					{
						m_C_SUB1SUB2_1.SetCurSel(0);
					}

					// if there is already a second employee we set his values as default
					// because of the change
					if (lmEmpl2)
					{
						pomDrr1 = ogDrrData.GetDrrByKey (olSday, lmEmpl2, "1", omRosl);
					}
					if (pomDrr1 != NULL)
					{
						// setting function
						for (i = 0; i < m_C_FCTC1.GetCount(); i++)
						{
							m_C_FCTC1.GetLBText(i, olTmp);
							olTmp = olTmp.Left(5);
							olTmp.TrimRight();
							if (strcmp(olTmp, pomDrr1->Fctc) == NULL)
							{
								m_C_FCTC1.SetCurSel(i);
								break;
							}
						}

						// setting the working group
						DRGDATA* polDrg = ogDrgData.GetDrgByKey(pomDrr1->Sday, pomDrr1->Stfu, "1");
						if (polDrg != 0)
						{
							olWgpCode	= polDrg->Wgpc;
						}
						else
						{
							COleDateTime olTime;
							CedaDataHelper::DateStringToOleDateTime(pomDrr1->Sday,olTime);
							olWgpCode = ogSwgData.GetWgpBySurnWithTime(pomDrr1->Stfu,olTime);
						}
						if (olWgpCode.GetLength() > 0)
						{
							for (i = 0; i < m_C_WGPC1.GetCount(); i++)
							{
								m_C_WGPC1.GetLBText(i, olTmp);
								olTmp = olTmp.Left(5);
								olTmp.TrimRight();
								if (strcmp(olTmp,olWgpCode) == NULL)
								{
									m_C_WGPC1.SetCurSel(i);
									break;
								}
							}
						}
						else
						{
							m_C_WGPC1.SetCurSel(0);
						}
					}
					pomDrr1 = popDrr;
				}
				break;
		case 2:
				pomDrr2 = popDrr;
				lmOldBsdu2 = llBsdu;
				omOldScod2 = olScod;
				SetDlgItemText(IDC_OLD_SHIFT2,olScod);
				SetDlgItemText(IDC_SOURCE2,olUses);
				if (ogCCSParam.GetParamValue(ogAppl,"ID_PROD_CUSTOMER") == "SIN" && pomDrr2 != NULL)
				{
					// setting SUB1/SUB2
					if (strcmp(pomDrr2->Drs2, "2") == 0)
					{
						m_C_SUB1SUB2_2.SetCurSel(1);
					}
					else
					{
						m_C_SUB1SUB2_2.SetCurSel(0);
					}
					// if there is already a second employee we set his values as default
					// because of the change
					if (lmEmpl1)
					{
						pomDrr2 = ogDrrData.GetDrrByKey (olSday, lmEmpl1, "1", omRosl);
					}
					if (pomDrr2 != NULL)
					{
						// setting function
						for (i = 0; i < m_C_FCTC2.GetCount(); i++)
						{
							m_C_FCTC2.GetLBText(i, olTmp);
							olTmp = olTmp.Left(5);
							olTmp.TrimRight();
							if (strcmp(olTmp, pomDrr2->Fctc) == NULL)
							{
								m_C_FCTC2.SetCurSel(i);
								break;
							}
						}

						// setting the working group
						DRGDATA* polDrg = ogDrgData.GetDrgByKey(pomDrr2->Sday, pomDrr2->Stfu, "1");
						if (polDrg != 0)
						{
							olWgpCode	= polDrg->Wgpc;
						}
						else
						{
							COleDateTime olTime;
							CedaDataHelper::DateStringToOleDateTime(pomDrr2->Sday,olTime);
							olWgpCode = ogSwgData.GetWgpBySurnWithTime(pomDrr2->Stfu,olTime);
						}
						if (olWgpCode.GetLength() > 0)
						{
							for (i = 0; i < m_C_WGPC2.GetCount(); i++)
							{
								m_C_WGPC2.GetLBText(i, olTmp);
								olTmp = olTmp.Left(5);
								olTmp.TrimRight();
								if (strcmp(olTmp,olWgpCode) == NULL)
								{
									m_C_WGPC2.SetCurSel(i);
									break;
								}
							}
						}
						else
						{
							m_C_WGPC2.SetCurSel(0);
						}
					}
					pomDrr2 = popDrr;
				}
				break;
	}

	// Alle Accounts berechnen
	SetAccounts(true);

CCS_CATCH_ALL;
}

//********************************************************************************
// Datumsauswahl 1 hat sich ver�ndert
//********************************************************************************
void CChangeShiftDlg::OnDatetimechangeDatetimepicker1(NMHDR* pNMHDR, LRESULT* pResult) 
{
	UpdateData(true);

	// Schichten setzen
	SetShift(1,lmEmpl1);
	SetShift(2,lmEmpl2);

	// Alle Eingaben l�schen
	ClearResults();

	LoadMonthAndEmpl(m_DatePicker1);

	// the date of the second DateTimePicker can't be earlier than of the first one
	if (m_DatePicker1.m_dt > m_DatePicker2.m_dt)
	{
		CDateTimeCtrl* polDateCtrl2 = (CDateTimeCtrl*) GetDlgItem(IDC_DATETIMEPICKER2);
		COleDateTime olStartDate(m_DatePicker1.GetYear(),m_DatePicker1.GetMonth(),1,0,0,0);
		COleDateTime olEndDate(m_DatePicker1.GetYear(),m_DatePicker1.GetMonth(),CedaDataHelper::GetDaysOfMonth(m_DatePicker1),0,0,0);
		polDateCtrl2->SetRange(&olStartDate,&olEndDate);
		polDateCtrl2->SetTime(m_DatePicker1);
	}

	// Alle neuen schon getauschten Schichten l�schen
	*pResult = 0;
}

//********************************************************************************
// Eingaben pr�fen
//********************************************************************************

bool CChangeShiftDlg::CheckInput()
{
	CString olMessageString;
	CString olTmp;

	UpdateData(true);

	//---------------------------------------------
	// Pr�fung der Schichten 
	//---------------------------------------------
	if (lmOldBsdu1 == 0)
	{
		// -> In Position 1 steht keine Schicht, Fehlermeldung und abbrechen.
		olMessageString =  m_DatePicker1.Format("%d.%m.%y: ") +LoadStg(IDS_STRING1927);
	}

	if (lmOldBsdu2 == 0)
	{
		// -> In Position 2 steht keine Schicht, Fehlermeldung und abbrechen.
		olMessageString +=  "\n" +m_DatePicker1.Format("%d.%m.%y: ")+LoadStg(IDS_STRING1928);
	}
	
	if (!olMessageString.IsEmpty())
	{
		MessageBox(olMessageString + "\n" + LoadStg(IDS_STRING1199) ,LoadStg(IDC_S_Meldung),MB_ICONWARNING);
		return false;
	}

	// ----------------------------------------------------------------------
	// Test auf Abwesenheiten ohne regul�r frei
	// ----------------------------------------------------------------------
	int ilTbsdType;
	bool blWork;
	bool blIsSleepDay;
	
	if (ogOdaData.IsODAAndIsRegularFree(omOldScod1, &ilTbsdType, &blWork, &blIsSleepDay) == CODE_IS_ODA_FREE)
	{
		if (blIsSleepDay == false)
		{
			// absence without regular free and without sleep day
			olMessageString =  m_DatePicker1.Format("%d.%m.%y: ") +LoadStg(IDS_STRING1672);
		}
	}

	if (ogOdaData.IsODAAndIsRegularFree(omOldScod2, &ilTbsdType, &blWork, &blIsSleepDay) == CODE_IS_ODA_FREE)
	{
		if (blIsSleepDay == false)
		{
			// absence without regular free and without sleep day
			olMessageString =  m_DatePicker1.Format("%d.%m.%y: ") +LoadStg(IDS_STRING1672);
		}
	}

	if (!olMessageString.IsEmpty())
	{
		MessageBox(olMessageString,LoadStg(IDC_S_Meldung),MB_ICONWARNING);
		return false;
	}
	
	// ----------------------------------------------------------------------
	// Test auf Doppeltouren
	// ----------------------------------------------------------------------

	// Schichten die Doppeltouren zugeodnet sind d�rfen nicht umgesetzt werden
	if (ogDrrData.HasDoubleTour(pomDrr1))
		olMessageString =  m_DatePicker1.Format("%d.%m.%y: ") + LoadStg(IDS_STRING1183);

	// Schichten die Doppeltouren zugeodnet sind d�rfen nicht umgesetzt werden
	if (ogDrrData.HasDoubleTour(pomDrr2))
		olMessageString +=  "\n" + m_DatePicker1.Format("%d.%m.%y: ") + LoadStg(IDS_STRING1186);

	if (!olMessageString.IsEmpty())
	{
		MessageBox(olMessageString + "\n" + LoadStg(IDS_STRING1199),LoadStg(IDC_S_Meldung),MB_ICONWARNING);
		return false;
	}

	if (ogCCSParam.GetParamValue(ogAppl,"ID_PROD_CUSTOMER") == "SIN")
	{
		PFCDATA *prlPfcData = NULL;
		// check function, Sub1/Sub2 and working group
		int ilSelItem;
		// get selected functions
		ilSelItem = m_C_FCTC1.GetCurSel();
		omFctc1 = " ";
		if (ilSelItem != LB_ERR)
		{
			m_C_FCTC1.GetLBText(ilSelItem, omFctc1);
			if (omFctc1.GetLength() > 4)
			{
				omFctc1 = omFctc1.Left(5);
			}
			omFctc1.TrimRight();
			prlPfcData = ogPfcData.GetPfcByFctc(omFctc1);
			if (prlPfcData == NULL)
			{
				olTmp.Format (LoadStg(IDS_STRING118), 1);
				olMessageString +=  "\n" + olTmp;
			}
		}
		else
		{
			olTmp.Format (LoadStg(IDS_STRING118), 1);
			olMessageString +=  "\n" + olTmp;
		}

		ilSelItem = m_C_FCTC2.GetCurSel();
		omFctc2 = " ";
		if (ilSelItem != LB_ERR)
		{
			m_C_FCTC2.GetLBText(ilSelItem, omFctc2);
			if (omFctc2.GetLength() > 4)
			{
				omFctc2 = omFctc2.Left(5);
			}
			omFctc2.TrimRight();
			prlPfcData = ogPfcData.GetPfcByFctc(omFctc2);
			if (prlPfcData == NULL)
			{
				olTmp.Format (LoadStg(IDS_STRING118), 2);
				olMessageString +=  "\n" + olTmp;
			}
		}
		else
		{
			olTmp.Format (LoadStg(IDS_STRING118), 2);
			olMessageString +=  "\n" + olTmp;
		}

		// get selected SUB1/SUB2
		ilSelItem = m_C_SUB1SUB2_1.GetCurSel();
		omDrs2_1 = " ";
		if (ilSelItem != LB_ERR)
		{
			omDrs2_1.Format("%d",ilSelItem+1);
		}
		else
		{
			olTmp.Format (LoadStg(IDS_STRING120), 1);
			olMessageString +=  "\n" + olTmp;
		}

		ilSelItem = m_C_SUB1SUB2_2.GetCurSel();
		omDrs2_2 = " ";
		if (ilSelItem != LB_ERR)
		{
			omDrs2_2.Format("%d",ilSelItem+1);
		}
		else
		{
			olTmp.Format (LoadStg(IDS_STRING120), 2);
			olMessageString +=  "\n" + olTmp;
		}

		// get selected working groups
		WGPDATA *prlWgpData = NULL;
		ilSelItem = m_C_WGPC1.GetCurSel();
		omWGPC1 = " ";
		if (ilSelItem != LB_ERR)
		{
			m_C_WGPC1.GetLBText(ilSelItem, omWGPC1);
			if (omWGPC1.GetLength() > 4)
				omWGPC1 = omWGPC1.Left(5);
			omWGPC1.TrimRight();
			prlWgpData = ogWgpData.GetWgpByWgpc(omWGPC1);
			if (prlWgpData == NULL && omWGPC1.GetLength())
			{
				olTmp.Format (LoadStg(IDS_STRING124), 1);
				olMessageString +=  "\n" + olTmp;
			}
		}
		else
		{
			olTmp.Format (LoadStg(IDS_STRING124), 1);
			olMessageString +=  "\n" + olTmp;
		}

		ilSelItem = m_C_WGPC2.GetCurSel();
		omWGPC2 = " ";
		if (ilSelItem != LB_ERR)
		{
			m_C_WGPC2.GetLBText(ilSelItem, omWGPC2);
			if (omWGPC2.GetLength() > 4)
				omWGPC2 = omWGPC2.Left(5);
			omWGPC2.TrimRight();
			prlWgpData = ogWgpData.GetWgpByWgpc(omWGPC2);
			if (prlWgpData == NULL && omWGPC2.GetLength())
			{
				olTmp.Format (LoadStg(IDS_STRING124), 2);
				olMessageString +=  "\n" + olTmp;
			}
		}
		else
		{
			olTmp.Format (LoadStg(IDS_STRING124), 2);
			olMessageString +=  "\n" + olTmp;
		}

		if (!olMessageString.IsEmpty())
		{
			MessageBox(olMessageString + "\n" + LoadStg(IDS_STRING1199),LoadStg(IDC_S_Meldung),MB_ICONWARNING);
			return false;
		}
	}

	// ----------------------------------------------------------------------
	// Test auf DRD DRA
	// ----------------------------------------------------------------------

	if (ogDrrData.HasDrdData(pomDrr1))
		olMessageString =  m_DatePicker1.Format("%d.%m.%y: ") + LoadStg(IDS_STRING1188);

	// Pr�ft ob der DRR DRAs hat
	if (ogDrrData.HasDraData(pomDrr1))
		olMessageString +=  "\n" + m_DatePicker1.Format("%d.%m.%y: ") + LoadStg(IDS_STRING1189);

	if (ogDrrData.HasDrdData(pomDrr2))
		olMessageString +=  "\n" + m_DatePicker1.Format("%d.%m.%y: ") + LoadStg(IDS_STRING1190);

	// Pr�ft ob der DRR DRAs hat
	if (ogDrrData.HasDraData(pomDrr2))
		olMessageString +=  "\n" + m_DatePicker1.Format("%d.%m.%y: ") + LoadStg(IDS_STRING1191);

	// Resultat ausgeben
	if (!olMessageString.IsEmpty())
		MessageBox(olMessageString,LoadStg(IDC_S_Meldung),MB_ICONWARNING);

	// ----------------------------------------------------------------------
	// Test auf Einhaltung der Betriebsregeln
	// ----------------------------------------------------------------------


	//For Function code swapping  if there is no user input
	if(omFctc1 == "" && omFctc2 == "" )
	{
		omFctc1 = pomDrr2->Fctc;
		omFctc2 = pomDrr1->Fctc;
	}

	//For Workgroup swapping  if there is no user input
	DRGDATA* prlDrg;
	if(omWGPC1 == "" && omWGPC2 == "")
	{
		prlDrg = ogDrgData.GetDrgByKey(pomDrr2->Sday, pomDrr2->Stfu, pomDrr2->Drrn);
		if (prlDrg != NULL)
		{
			omWGPC1= prlDrg->Wgpc;
		}
		else 
		{
			//Check in basic data.
			COleDateTime olTime;
			CedaDataHelper::DateStringToOleDateTime(pomDrr2->Sday,olTime);
			omWGPC1 = ogSwgData.GetWgpBySurnWithTime(pomDrr2->Stfu,olTime);
			
		}		
		prlDrg = NULL;
	
		prlDrg = ogDrgData.GetDrgByKey(pomDrr1->Sday, pomDrr1->Stfu, pomDrr1->Drrn);
		if (prlDrg != NULL)
		{
			omWGPC2= prlDrg->Wgpc;
		}
		else 
		{
			//Check in basic data.
			COleDateTime olTime;
			CedaDataHelper::DateStringToOleDateTime(pomDrr1->Sday,olTime);
			omWGPC2 = ogSwgData.GetWgpBySurnWithTime(pomDrr1->Stfu,olTime);
			
		}

		if(omWGPC1 == "")
		{
			omWGPC1 = CString(" ");
		}

		if(omWGPC2 == "")
		{
			omWGPC2 = CString(" ");
		}

	}

	

	// Bsdu,Scod,Avfr und Avto tauschen
	Swap(&pomDrr1->Bsdu, &pomDrr2->Bsdu);
	Swap(pomDrr1->Scod, pomDrr2->Scod);
	Swap(&pomDrr1->Avfr, &pomDrr2->Avfr);
	Swap(&pomDrr1->Avto, &pomDrr2->Avto);
	Swap(pomDrr1->Fctc, pomDrr2->Fctc);
	

	// ShiftCheck durchf�hren
	bool blShiftEnabled = ogShiftCheck.DutyRosterCheckIt(pomDrr1, ((DutyRoster_View*)m_pParentWnd)->rmViewInfo.bDoConflictCheck, DRR_CHANGE, false);
	if (blShiftEnabled)
	{
		blShiftEnabled = ogShiftCheck.DutyRosterCheckIt(pomDrr2, ((DutyRoster_View*)m_pParentWnd)->rmViewInfo.bDoConflictCheck, DRR_CHANGE, false);
	}

	// Bsdu,Scod,Avfr und Avto zur�ck tauschen
	Swap(&pomDrr1->Bsdu, &pomDrr2->Bsdu);
	Swap(pomDrr1->Scod, pomDrr2->Scod);
	Swap(&pomDrr1->Avfr, &pomDrr2->Avfr);
	Swap(&pomDrr1->Avto, &pomDrr2->Avto);
	Swap(pomDrr1->Fctc, pomDrr2->Fctc);

	// Fehler ausgeben, wenn vorhanden
	if (ogShiftCheck.IsWarning())
	{
		((DutyRoster_View*)m_pParentWnd)->ForwardWarnings();
	}

	if (!blShiftEnabled)
		return false;

	return true;
}

/*****************************************************************************
Tauscht den Inhalt von Parameter miteinander
*****************************************************************************/
void CChangeShiftDlg::Swap(long* lp1, long* lp2)
{
	long llTmp = *lp1;

	*lp1 = *lp2;
	*lp2 = llTmp;
}

/*****************************************************************************
Tauscht den Inhalt von Parameter miteinander
*****************************************************************************/
void CChangeShiftDlg::Swap(COleDateTime* lp1, COleDateTime* lp2)
{
	COleDateTime llTmp = *lp1;

	*lp1 = *lp2;
	*lp2 = llTmp;
}

/*****************************************************************************
Tauscht den Inhalt von Parameter miteinander
*****************************************************************************/
void CChangeShiftDlg::Swap(char* lp1, char* lp2)
{
	CString llTmp = lp1;

	strcpy(lp1,lp2);
	strcpy(lp2,llTmp);
}

//********************************************************************************
// Eingaben pr�fen
//********************************************************************************

void CChangeShiftDlg::OnButtonapply() 
{
	// G�ltigkeit der Eingabe pr�fen und Meldungen ausgeben
	if (!CheckInput())
		return;

	ChangeShifts();
}

//********************************************************************************
// Schichten tauschen
//********************************************************************************

void CChangeShiftDlg::ChangeShifts() 
{
	lmNewBsdu1 = lmOldBsdu2;
	lmNewBsdu2 = lmOldBsdu1;

	omNewScod1 = omOldScod2; 
	omNewScod2 = omOldScod1; 

	// Und anzeigen
	SetDlgItemText (IDC_NEW_NEWSHIFT1, omNewScod1);
	SetDlgItemText (IDC_NEW_NEWSHIFT2, omNewScod2);

	// Accountwerte (vorher/ nachher) berechnen und setzen
	SetAccounts(false);
}

//********************************************************************************
// Eingaben speichern und ausf�hren
//********************************************************************************

void CChangeShiftDlg::OnOK() 
{
	CWnd *polWnd = CWnd::GetFocus();
	if (polWnd != NULL)
	{
		CWnd *polOk = this->GetDlgItem(IDOK);
		if (polOk != NULL && polOk->m_hWnd != polWnd->m_hWnd)
		{
			polOk->SetFocus();
		}
	}

	if (ogCCSParam.GetParamValue(ogAppl,"ID_PROD_CUSTOMER") != "ATH")
	{

		bool blCheckMainFuncOnly;
		bool blIsBsdCode;

		DRRDATA olDrrSave1;
		DRRDATA olDrrSave2;

		ogDrrData.CopyDrr(&olDrrSave1,pomDrr1);
		ogDrrData.CopyDrr(&olDrrSave2,pomDrr2);

		blCheckMainFuncOnly = ((DutyRoster_View*)m_pParentWnd)->rmViewInfo.bCheckMainFuncOnly;

		// G�ltigkeit der Eingabe pr�fen und Meldungen ausgeben
		if (!CheckInput())
			return;

		// �nderungen anwenden
		ChangeShifts();

		// L�scht alle DRD und DRA Daten und pr�ft die Urnos
		if (ogDrrData.CheckBSDCode(pomDrr1,lmNewBsdu1,lmOldBsdu1) == lmOldBsdu1)
			return;

		if (ogDrrData.CheckBSDCode(pomDrr2,lmNewBsdu2,lmOldBsdu2) == lmOldBsdu2)
			return;

		// change the shifts
		blIsBsdCode = ogBsdData.GetBsdByUrno(lmNewBsdu1) != 0;
		if(!ogDrrData.SetDrrDataScod(pomDrr1,blIsBsdCode,lmNewBsdu1,omFctc1,true,blCheckMainFuncOnly))
		{
			ogDrrData.CopyDrr(pomDrr1, &olDrrSave1);
			ogDrrData.CopyDrr(pomDrr2, &olDrrSave2);
			return;
		}

		blIsBsdCode = ogBsdData.GetBsdByUrno(lmNewBsdu2) != 0;
		if(!ogDrrData.SetDrrDataScod(pomDrr2,blIsBsdCode,lmNewBsdu2,omFctc2,true,blCheckMainFuncOnly))
		{
			ogDrrData.CopyDrr(pomDrr1, &olDrrSave1);
			ogDrrData.CopyDrr(pomDrr2, &olDrrSave2);
			return;
		}

		// Datensatz �ndern (BC senden)
		strcpy(pomDrr2->Drs1," ");
		strcpy(pomDrr2->Drs3," ");
		strcpy(pomDrr2->Drs4," ");
		strcpy(pomDrr1->Drs1," ");
		strcpy(pomDrr1->Drs3," ");
		strcpy(pomDrr1->Drs4," ");

		if (ogCCSParam.GetParamValue(ogAppl,"ID_PROD_CUSTOMER") == "SIN" ||
			ogCCSParam.GetParamValue(ogAppl,"ID_SWAPSHIFT_WGP") == "Y" )
		{
			// take the settings of the combo boxes

			DRGDATA* prlDrg;
			CString olOldFctc;
			CString olOldWgpc;

			olOldFctc = olDrrSave1.Fctc;
			prlDrg = ogDrgData.GetDrgByKey(pomDrr1->Sday, pomDrr1->Stfu, pomDrr1->Drrn);
			if (prlDrg != NULL)
			{
				olOldWgpc = prlDrg->Wgpc;
			}
			//else
			//{
			//	COleDateTime olTime;
			//	CedaDataHelper::DateStringToOleDateTime(pomDrr1->Sday,olTime);
			//	olOldWgpc = ogSwgData.GetWgpBySurnWithTime(pomDrr1->Stfu,olTime);
			//}

			if (omWGPC1 != olOldWgpc || omFctc1 != olOldFctc)
			{
				//Commented the checking for workgroup to show the empty assignment.
				//WGPDATA *prlWgpData = ogWgpData.GetWgpByWgpc(omWGPC1);
				//if (prlWgpData != NULL)
				//{
					bool blInsert = false;
					if (prlDrg == NULL)
					{
						//create new DRG-record
						prlDrg = ogDrgData.CreateDrgData(pomDrr1->Stfu, pomDrr1->Sday, pomDrr1->Drrn);
						blInsert = true;
					}
					strcpy(prlDrg->Wgpc, omWGPC1);
					strcpy(prlDrg->Fctc, omFctc1);
					strcpy(prlDrg->Drrn, pomDrr1->Drrn);

					if (blInsert == true)
					{
						ogDrgData.Insert(prlDrg);
					}
					else
					{
						ogDrgData.Update(prlDrg);
					}
				//}
				//else
				//{
					// no working group - we have to delete existing DRGs
					//if (prlDrg != NULL)
					//{
					//	ogDrgData.Delete(prlDrg,true);
					//}
				//}
			}

			olOldWgpc = "";
			olOldFctc = olDrrSave2.Fctc;
			prlDrg = ogDrgData.GetDrgByKey(pomDrr2->Sday, pomDrr2->Stfu, pomDrr2->Drrn);
			if (prlDrg != NULL)
			{
				olOldWgpc = prlDrg->Wgpc;
			}
			//else
			//{
			//	COleDateTime olTime;
			//	CedaDataHelper::DateStringToOleDateTime(pomDrr2->Sday,olTime);
			//	olOldWgpc = ogSwgData.GetWgpBySurnWithTime(pomDrr2->Stfu,olTime);
			//}

			if (omWGPC2 != olOldWgpc || omFctc2 != olOldFctc)
			{
				//Commented the checking for workgroup to show the empty assignment.
				//WGPDATA *prlWgpData = ogWgpData.GetWgpByWgpc(omWGPC2);
				//if (prlWgpData != NULL)
				//{
					bool blInsert = false;
					if (prlDrg == NULL)
					{
						//create new DRG-record
						prlDrg = ogDrgData.CreateDrgData(pomDrr2->Stfu, pomDrr2->Sday, pomDrr2->Drrn);
						blInsert = true;
					}
					strcpy(prlDrg->Wgpc, omWGPC2);
					strcpy(prlDrg->Fctc, omFctc2);
					strcpy(prlDrg->Drrn, pomDrr2->Drrn);

					if (blInsert == true)
					{
						ogDrgData.Insert(prlDrg);
					}
					else
					{
						ogDrgData.Update(prlDrg);
					}
				//}
				//else
				//{
					// no working group - we have to delete existing DRGs
					//if (prlDrg != NULL)
					//{
					//	ogDrgData.Delete(prlDrg,true);
					//}
				//}
			}

			// we copy the information back, then we exchange only the STFU
			ogDrrData.CopyDrr(pomDrr1, &olDrrSave1);
			ogDrrData.CopyDrr(pomDrr2, &olDrrSave2);

			DRRDATA *prlDrrSave1 = NULL;
			DRRDATA *prlDrrSave2 = NULL;
			DRRDATA *prlDrrSave1_Drrn2 = NULL;
			DRRDATA *prlDrrSave2_Drrn2 = NULL;

			DRRDATA *prlDrrData1_Drrn2 = NULL;
			DRRDATA *prlDrrData2_Drrn2 = NULL;

			if (pomDrr1 != NULL && pomDrr2 != NULL)
			{
				long llEmpl1 = pomDrr1->Stfu;
				long llEmpl2 = pomDrr2->Stfu;

				// handling the first shifts
				prlDrrSave1 = new DRRDATA;
				prlDrrSave2 = new DRRDATA;

				ogDrrData.CopyDrr(prlDrrSave1,pomDrr1);
				ogDrrData.CopyDrr(prlDrrSave2,pomDrr2);

				// we have to take care that the internal maps of CedaDrrData are up to date
				// and we are doing the change

				// 0. step: store 2nd shifts, if any
				prlDrrData1_Drrn2 = ogDrrData.GetDrrByKey(pomDrr1->Sday, lmEmpl1, "2",pomDrr1->Rosl);
				prlDrrData2_Drrn2 = ogDrrData.GetDrrByKey(pomDrr2->Sday, lmEmpl2, "2",pomDrr2->Rosl);

				if (prlDrrData1_Drrn2 != NULL)
				{
					prlDrrSave1_Drrn2 = new DRRDATA;
					ogDrrData.CopyDrr(prlDrrSave1_Drrn2,prlDrrData1_Drrn2);
				}

				if (prlDrrData2_Drrn2 != NULL)
				{
					prlDrrSave2_Drrn2 = new DRRDATA;
					ogDrrData.CopyDrr(prlDrrSave2_Drrn2,prlDrrData2_Drrn2);
				}


				// 1. step: deleting old shifts internal
				if (prlDrrData1_Drrn2 != NULL)
					ogDrrData.Delete(prlDrrData1_Drrn2,FALSE);

				if (prlDrrData2_Drrn2 != NULL)
					ogDrrData.Delete(prlDrrData2_Drrn2,FALSE);

				ogDrrData.Delete(pomDrr1,FALSE);
				ogDrrData.Delete(pomDrr2,FALSE);
				pomDrr1 = NULL;
				pomDrr2 = NULL;

				// 2. step: changing the STFUs
				prlDrrSave1->Stfu = llEmpl2;
				prlDrrSave2->Stfu = llEmpl1;

				// 3. step: inserting the records internal
				ogDrrData.Insert(prlDrrSave1,false);
				ogDrrData.Insert(prlDrrSave2,false);

				if (prlDrrSave1_Drrn2 != NULL)
					ogDrrData.Insert(prlDrrSave1_Drrn2,false);

				if (prlDrrSave2_Drrn2 != NULL)
					ogDrrData.Insert(prlDrrSave2_Drrn2,false);
				
				// 4. step: changing the status, because it is an update, no insert
				prlDrrSave1->IsChanged = DATA_UNCHANGED;
				prlDrrSave2->IsChanged = DATA_UNCHANGED;

				// 5. step: setting the update information
				ogDrrData.SetDrrDataLastUpdate(prlDrrSave1);
				ogDrrData.SetDrrDataLastUpdate(prlDrrSave2);

				// 6. restoring pointer
				pomDrr1 = prlDrrSave1;
				pomDrr2 = prlDrrSave2;
			}

			// after that we copy the new selected function
			strcpy(pomDrr1->Fctc, omFctc2);
			strcpy(pomDrr2->Fctc, omFctc1);

			// create DRR.REMA
			STFDATA *prlStf = ogStfData.GetStfByUrno(lmEmpl1);
			CString olTmp;
			if (prlStf != NULL)
			{
				olTmp.Format ("%s %s", LoadStg(IDS_STRING126), CBasicData::GetFormatedEmployeeName(pomParent->rmViewInfo.iEName, prlStf, " ", ""));
				strncpy(pomDrr1->Rema,olTmp,ogDrrData.GetMaxFieldLength("REMA"));
				pomDrr1->Rema[ogDrrData.GetMaxFieldLength("REMA")-1] = '\0';
			}
			prlStf = ogStfData.GetStfByUrno(lmEmpl2);
			if (prlStf != NULL)
			{
				olTmp.Format ("%s %s", LoadStg(IDS_STRING126), CBasicData::GetFormatedEmployeeName(pomParent->rmViewInfo.iEName, prlStf, " ", ""));
				strncpy(pomDrr2->Rema,olTmp,ogDrrData.GetMaxFieldLength("REMA"));
				pomDrr2->Rema[ogDrrData.GetMaxFieldLength("REMA")-1] = '\0';
			}
		}
		else
		{
			// change the functions
			strcpy(pomDrr1->Fctc, olDrrSave2.Fctc);
			strcpy(pomDrr2->Fctc, olDrrSave1.Fctc);
		}

		// change sub1 / sub2
		if (this->omDrs2_2 == "2")
			strcpy(pomDrr2->Drs2,this->omDrs2_2);
		else
			strcpy(pomDrr2->Drs2,"1");

		if (this->omDrs2_1 == "2")
			strcpy(pomDrr1->Drs2,this->omDrs2_1);
		else
			strcpy(pomDrr1->Drs2,"1");

		// set scoo to reflect data changes
		strcpy(pomDrr1->Scoo,pomDrr2->Scod);
		strcpy(pomDrr2->Scoo,pomDrr1->Scod);
		

		CCSPtrArray<DRRDATA> olArr;
		long llOrigStfu = pomDrr1->Stfu;
		pomDrr1->Stfu = ogBasicData.GetNextUrno();
		olArr.Add (pomDrr1);

		ogDrrData.Synchronise("DUTYROSTER",true);
		ogDrrData.ReleaseRecords(&olArr, "LATE,NOBC,NOACTION,NOLOG");

		olArr.RemoveAll();
		pomDrr1->Stfu = llOrigStfu;
		pomDrr1->IsChanged = DATA_CHANGED;
		olArr.Add (pomDrr2);
		olArr.Add (pomDrr1);
		ogDrrData.ReleaseRecords(&olArr);
		ogDrrData.Synchronise("DUTYROSTER");
	}
	else
	{
		if (lmEmpl1 != 0 && lmEmpl2 != 0)
		{
			COleDateTime olStart = m_DatePicker1;
			CString olSday;
			CString olDrrn;

			DRRDATA *prlDrrData1_Drrn1 = NULL;
			DRRDATA *prlDrrData1_Drrn2 = NULL;
			DRRDATA *prlDrrData2_Drrn1 = NULL;
			DRRDATA *prlDrrData2_Drrn2 = NULL;

			DRRDATA *prlDrrSave1_Drrn1 = NULL;
			DRRDATA *prlDrrSave1_Drrn2 = NULL;
			DRRDATA *prlDrrSave2_Drrn1 = NULL;
			DRRDATA *prlDrrSave2_Drrn2 = NULL;

			for (COleDateTime olDate = m_DatePicker1; olDate <= m_DatePicker2; olDate += COleDateTimeSpan(1,0,0,0))
			{
				m_DatePicker1.SetDateTime(olDate.GetYear(),olDate.GetMonth(),olDate.GetDay(),0,0,0);
				olSday = olDate.Format("%Y%m%d");

				prlDrrData1_Drrn1 = ogDrrData.GetDrrByKey(olSday, lmEmpl1, "1", omRosl);
				prlDrrData2_Drrn1 = ogDrrData.GetDrrByKey(olSday, lmEmpl2, "1", omRosl);

				prlDrrData1_Drrn2 = ogDrrData.GetDrrByKey(olSday, lmEmpl1, "2", omRosl);
				prlDrrData2_Drrn2 = ogDrrData.GetDrrByKey(olSday, lmEmpl2, "2", omRosl);

				if (prlDrrData1_Drrn1 != NULL && prlDrrData2_Drrn1 != NULL)
				{
					// handling the first shifts
					prlDrrSave1_Drrn1 = new DRRDATA;
					prlDrrSave2_Drrn1 = new DRRDATA;

					ogDrrData.CopyDrr(prlDrrSave1_Drrn1,prlDrrData1_Drrn1);
					ogDrrData.CopyDrr(prlDrrSave2_Drrn1,prlDrrData2_Drrn1);

					// handling the second shifts
					if (prlDrrData1_Drrn2 != NULL)
					{
						prlDrrSave1_Drrn2 = new DRRDATA;
						ogDrrData.CopyDrr(prlDrrSave1_Drrn2,prlDrrData1_Drrn2);
					}
					else
					{
						prlDrrSave1_Drrn2 = NULL;
					}
					if (prlDrrData2_Drrn2 != NULL)
					{
						prlDrrSave2_Drrn2 = new DRRDATA;
						ogDrrData.CopyDrr(prlDrrSave2_Drrn2,prlDrrData2_Drrn2);
					}
					else
					{
						prlDrrSave2_Drrn2 = NULL;
					}

					// we have to take care that the internal maps of CedaDrrData are up to date
					// and we are doing the change

					// 1. step: deleting old shifts internal
					ogDrrData.Delete(prlDrrData1_Drrn1,FALSE);
					ogDrrData.Delete(prlDrrData2_Drrn1,FALSE);
					if (prlDrrData1_Drrn2 != NULL)
						ogDrrData.Delete(prlDrrData1_Drrn2,FALSE);
					if (prlDrrData2_Drrn2 != NULL)
						ogDrrData.Delete(prlDrrData2_Drrn2,FALSE);

					// 2. step: changing the STFUs
					prlDrrSave1_Drrn1->Stfu = lmEmpl2;
					prlDrrSave2_Drrn1->Stfu = lmEmpl1;
					if (prlDrrSave1_Drrn2 != NULL)
						prlDrrSave1_Drrn2->Stfu = lmEmpl2;
					if (prlDrrSave2_Drrn2 != NULL)
						prlDrrSave2_Drrn2->Stfu = lmEmpl1;

					// 3. step: inserting the records internal
					ogDrrData.Insert(prlDrrSave1_Drrn1,false);
					ogDrrData.Insert(prlDrrSave2_Drrn1,false);
					if (prlDrrSave1_Drrn2 != NULL)
						ogDrrData.Insert(prlDrrSave1_Drrn2,false);
					if (prlDrrSave2_Drrn2 != NULL)
						ogDrrData.Insert(prlDrrSave2_Drrn2,false);

					// 4. step: changing the status, because it is an update, no insert
					prlDrrSave1_Drrn1->IsChanged = DATA_UNCHANGED;
					prlDrrSave2_Drrn1->IsChanged = DATA_UNCHANGED;
					if (prlDrrSave1_Drrn2 != NULL)
						prlDrrSave1_Drrn2->IsChanged = DATA_UNCHANGED;
					if (prlDrrSave2_Drrn2 != NULL)
						prlDrrSave2_Drrn2->IsChanged = DATA_UNCHANGED;

					// 5. step: setting the update information
					ogDrrData.SetDrrDataLastUpdate(prlDrrSave1_Drrn1);
					ogDrrData.SetDrrDataLastUpdate(prlDrrSave2_Drrn1);
					if (prlDrrSave1_Drrn2 != NULL)
						ogDrrData.SetDrrDataLastUpdate(prlDrrSave1_Drrn2);
					if (prlDrrSave2_Drrn2 != NULL)
						ogDrrData.SetDrrDataLastUpdate(prlDrrSave2_Drrn2);


					// set scoo to reflect data changes
					strcpy(prlDrrSave1_Drrn1->Scoo,prlDrrSave2_Drrn1->Scod);
					strcpy(prlDrrSave2_Drrn1->Scoo,prlDrrSave1_Drrn1->Scod);

					// 6. step: writing the changes to DB
#ifdef	WITH_UNIQUE_INDEX
					CCSPtrArray<DRRDATA> olArr;

					ogDrrData.Synchronise("DUTYROSTER",true);

					long llOrigStfu = prlDrrSave1_Drrn1->Stfu;
					prlDrrSave1_Drrn1->Stfu = ogBasicData.GetNextUrno();
					olArr.Add (prlDrrSave1_Drrn1);

					ogDrrData.ReleaseRecords(&olArr, "LATE,NOBC,NOACTION,NOLOG");

					olArr.RemoveAll();
					prlDrrSave1_Drrn1->Stfu = llOrigStfu;
					prlDrrSave1_Drrn1->IsChanged = DATA_CHANGED;
					olArr.Add (prlDrrSave2_Drrn1);
					olArr.Add (prlDrrSave1_Drrn1);
					ogDrrData.ReleaseRecords(&olArr);


					olArr.RemoveAll();

					if (prlDrrSave1_Drrn2 != NULL && prlDrrSave2_Drrn2 != NULL)
					{
						llOrigStfu = prlDrrSave1_Drrn2->Stfu;
						prlDrrSave1_Drrn2->Stfu = ogBasicData.GetNextUrno();
						olArr.Add (prlDrrSave1_Drrn2);

						ogDrrData.ReleaseRecords(&olArr, "LATE,NOBC,NOACTION,NOLOG");

						olArr.RemoveAll();

						prlDrrSave1_Drrn2->Stfu = llOrigStfu;
						prlDrrSave1_Drrn2->IsChanged = DATA_CHANGED;

						olArr.Add (prlDrrSave2_Drrn2);
						olArr.Add (prlDrrSave1_Drrn2);

						ogDrrData.ReleaseRecords(&olArr);
					}
					else if (prlDrrSave1_Drrn2 != NULL)
					{
						ogDrrData.Update(prlDrrSave1_Drrn2);						
					}
					else if (prlDrrSave2_Drrn2 != NULL)
					{
						ogDrrData.Update(prlDrrSave2_Drrn2);						
					}

					ogDrrData.Synchronise("DUTYROSTER");


#else
					ogDrrData.Update(prlDrrSave1_Drrn1);
					ogDrrData.Update(prlDrrSave2_Drrn1);
					if (prlDrrSave1_Drrn2 != NULL)
						ogDrrData.Update(prlDrrSave1_Drrn2);
					if (prlDrrSave2_Drrn2 != NULL)
						ogDrrData.Update(prlDrrSave2_Drrn2);
#endif
				}
			}

			// setting the original value
			m_DatePicker1.SetDateTime(olStart.GetYear(),olStart.GetMonth(),olStart.GetDay(),0,0,0);
		}
	}

	CDialog::OnOK();
}

//********************************************************************************
// Setzen und berechnen der Konto Werte
//********************************************************************************

void CChangeShiftDlg::SetAccounts(bool bpOnlyBefore) 
{
	CString olValueBefore1,olValueBefore2;
	double llValueBefore1=0, llValueBefore2=0;
	CString olFormat;
	
	CString olYear;
	int ilMonth;

	ilMonth = m_DatePicker1.GetMonth();
	olYear.Format("%d",m_DatePicker1.GetYear());

	ACCOUNTRETURNSTRUCT olValue1, olValue2;
	COleDateTime olDate(m_DatePicker1.GetYear(), m_DatePicker1.GetMonth(), m_DatePicker1.GetDay(), 0, 0, 0);

	// Account Klasse initiaisieren
	CAccounts olMyAccount(	&ogDrrData, &ogAccData, &ogStfData, &ogBsdData,
							&ogOdaData, &ogScoData, &ogCotData);

	//uhi 16.7.01 PRF 247
	CString olText;
	int ilFormat;

	// Das Konto 27 ermitteln f�r 1 Mitarbeiter
	if (lmEmpl1 != 0 && lmOldBsdu1 != 0)
	{
		// Ermittlung des Schlu�kontostandes (1)
		olValue1 = olMyAccount.GetAccountByStaffAndMonth(27, lmEmpl1, olDate, olDate);
		ilFormat = atoi(olValue1.oFormat);

		// Plausibilit�tspr�fung des Formatstrings
		if ( ilFormat < 0 || ilFormat > 10)
		{
			// -> Fehler
			olFormat = "2";
		}
		else
		{
			// -> OK
			olFormat.Format("%d",ilFormat);
		}
		if(olValue1.oValueDefinition == "STRINGorSINGEL")
		{
			olText = olValue1.oString.Left(6);
		}
		else
		{
			olText.Format("%0."+olFormat+"f", olValue1.dDouble);
		}

		olValueBefore1 = olText;
		SetDlgItemText(IDC_OLD_ACCOUNT1,olText);
	}

	// Das Konto 27 ermitteln f�r 2 Mitarbeiter
	if (lmEmpl2 != 0 && lmOldBsdu2 != 0)
	{
		// Ermittlung des Schlu�kontostandes (2)
		olValue2 = olMyAccount.GetAccountByStaffAndMonth(27, lmEmpl2, olDate, olDate);
		ilFormat = atoi(olValue2.oFormat);

		// Plausibilit�tspr�fung des Formatstrings
		if ( ilFormat < 0 || ilFormat > 10)
		{
			// -> Fehler
			olFormat = "2";
		}
		else
		{
			// -> OK
			olFormat.Format("%d",ilFormat);
		}
		if(olValue2.oValueDefinition == "STRINGorSINGEL")
		{
			olText = olValue2.oString.Left(6);
		}
		else
		{
			olText.Format("%0."+olFormat+"f", olValue2.dDouble);
		}

		olValueBefore2 = olText;
		SetDlgItemText(IDC_OLD_ACCOUNT2,olText);
	}
	

	// --------------------------------------------------------------
	// Berechnung des Kontos nach dem Abgleich
	// L�nge der aktuellen Schicht abziehen und L�nge der getauschen Schicht
	// addieren 
	// --------------------------------------------------------------

	if (!bpOnlyBefore)
	{
		CString olValueAfter1,olValueAfter2;
		double llValueAfter1,llValueAfter2;

		double llDrrMin,llBreakMin;
		llBreakMin = llDrrMin = 0;

		//-----------------------------------------------------------------------------
		// Mitarbeiter 1
		// Urspr�ngliche Schicht rausrechnen
		olMyAccount.GetDailyTotalAZKMinutesByStaff(lmEmpl1,m_DatePicker1,llDrrMin,llBreakMin);

		llValueBefore1 = atof(olValueBefore1) * 60;
		llValueAfter1 = llValueBefore1 - (llDrrMin + llBreakMin);

		// Arbeitszeit nach Tausch ermitteln (DRR vom Tauschpartner an diesem Tag)
		olMyAccount.GetDailyTotalAZKMinutesByStaff(lmEmpl2,m_DatePicker1,llDrrMin,llBreakMin);

		llValueAfter1 = llValueAfter1 + llDrrMin + llBreakMin;

		olValueAfter1.Format("%0.2f", llValueAfter1/60);
		SetDlgItemText(IDC_NEW_ACCOUNT1,olValueAfter1);

		//-----------------------------------------------------------------------------
		// Mitarbeiter 2
		// Arbeitszeit vor Tausch ermitteln
		olMyAccount.GetDailyTotalAZKMinutesByStaff(lmEmpl2,m_DatePicker1,llDrrMin,llBreakMin);

		llValueBefore2 = atof(olValueBefore2) * 60;
		llValueAfter2 = llValueBefore2 - (llDrrMin + llBreakMin);

		olMyAccount.GetDailyTotalAZKMinutesByStaff(lmEmpl1,m_DatePicker1,llDrrMin,llBreakMin);

		llValueAfter2 = llValueAfter2 + llDrrMin + llBreakMin;

		olValueAfter2.Format("%0.2f", llValueAfter2/60);
		SetDlgItemText(IDC_NEW_ACCOUNT2,olValueAfter2);
	}
}

//********************************************************************************
// l�scht alle Berechneten Werte f�r Datum 1
//********************************************************************************
void CChangeShiftDlg::ClearResults()
{
	// Ziel Schicht (neue Schichten) l�schen
	lmNewBsdu1 = lmNewBsdu2 =0;

	omNewScod1.Empty();
	omNewScod2.Empty();

	SetDlgItemText(IDC_NEW_NEWSHIFT1,omNewScod1);
	SetDlgItemText(IDC_NEW_NEWSHIFT2,omNewScod2);

	SetDlgItemText(IDC_NEW_ACCOUNT1,"");
	SetDlgItemText(IDC_NEW_ACCOUNT2,"");

	SetDlgItemText(IDC_OLD_ACCOUNT1,"");
	SetDlgItemText(IDC_OLD_ACCOUNT2,"");
}


//********************************************************************************
// L�dt alle relevanten Daten aus der Datenbank f�r einen Monat
//********************************************************************************
bool CChangeShiftDlg::LoadMonthAndEmpl(COleDateTime opDate)
{
CCS_TRY;

	if (opDate < pomParent->rmViewInfo.oDateFrom || opDate > pomParent->rmViewInfo.oDateTo)
	{
		CWaitCursor olDummy;

		CString olWhere,olTmpUrnos;

		// collect StfUrnos
		if (lmEmpl1 != 0 && lmEmpl2 != 0)
		{
			olTmpUrnos.Format("'%ld','%ld'",lmEmpl1,lmEmpl2);
		}
		else
		{
			if(lmEmpl1 == 0 )
			{
				olTmpUrnos.Format("'%ld'",lmEmpl2);
			}
			else //lmEmpl2 == 0 
			{
				olTmpUrnos.Format("'%ld'",lmEmpl1);
			}
		}

		// Zeitraum berechnen
		COleDateTime olDateFrom,olDateTo;

		olDateFrom.SetDate(opDate.GetYear(),opDate.GetMonth(),1);
		olDateTo.SetDate(opDate.GetYear(),opDate.GetMonth(),CedaDataHelper::GetDaysOfMonth(opDate));

		//------------------------------------------------------------------------------------------------------
		// DRR Daten laden
		olWhere.Format("WHERE STFU IN (%s) AND SDAY BETWEEN '%s' AND '%s' AND ROSL = '%s'",olTmpUrnos,olDateFrom.Format("%Y%m%d"),olDateTo.Format("%Y%m%d"),omRosl);
		if(ogDrrData.Read(olWhere.GetBuffer(0),NULL,false,false) == false)
		{
		}

		// ACC Daten laden
		olWhere.Format("WHERE STFU IN (%s) AND YEAR = '%s' AND TYPE = '27'",olTmpUrnos,olDateFrom.Format("%Y"));
		if(ogAccData.Read(olWhere.GetBuffer(0),false) == false)
		{
		}

		// DRD Daten laden
		olWhere.Format("WHERE STFU IN (%s) AND SDAY BETWEEN '%s' AND '%s'" ,olTmpUrnos,olDateFrom.Format("%Y%m%d"),olDateTo.Format("%Y%m%d"));
		if(ogDrdData.Read(olWhere.GetBuffer(0),NULL,false,false) == false)
		{
		}

		// DRA Daten laden
		olWhere.Format("WHERE STFU IN (%s) AND SDAY BETWEEN '%s' AND '%s'" ,olTmpUrnos,olDateFrom.Format("%Y%m%d"),olDateTo.Format("%Y%m%d"));
		if(ogDraData.Read(olWhere.GetBuffer(0),NULL,false,false) == false)
		{
		}

		// DRS Daten laden
		olWhere.Format("WHERE STFU IN (%s) AND SDAY BETWEEN '%s' AND '%s'" ,olTmpUrnos,olDateFrom.Format("%Y%m%d"),olDateTo.Format("%Y%m%d"));
		if(ogDrsData.Read(olWhere.GetBuffer(0),NULL,false,false) == false)
		{
		}
	}
	else
	{
		//don't load anything - all needed data is already loaded!
	}

	return true;

CCS_CATCH_ALL;
return false;
}


//********************************************************************************
// Statische Elemente setzen
//********************************************************************************

void CChangeShiftDlg::SetStatics() 
{
	SetDlgItemText( IDC_STATIC_EMPLNR, LoadStg(IDS_STRING1930));
	SetDlgItemText( IDC_EMPLNAME, LoadStg(IDS_STRING60));
	SetDlgItemText( IDC_OLDTOUR, LoadStg(IDS_STRING1774));
	SetDlgItemText( IDC_OLDTOUR2, LoadStg(IDS_STRING1774));
	SetDlgItemText( IDC_NEWTOUR, LoadStg(IDS_STRING1775));
	SetDlgItemText( IDC_NEWTOUR2, LoadStg(IDS_STRING1775));
	SetDlgItemText( IDC_BEFORE, LoadStg(IDS_STRING1776));
	SetDlgItemText( IDC_AFTER, LoadStg(IDS_STRING1777));
	SetDlgItemText( IDC_BY, LoadStg(HD_DSR_AVFA_TIME));
	SetDlgItemText( IDC_BY2, LoadStg(HD_DSR_AVFA_TIME));

	if (ogCCSParam.GetParamValue(ogAppl,"ID_PROD_CUSTOMER") != "ATH")
	{
		SetDlgItemText( IDC_STATIC_DATE1, LoadStg(IDC_DAY));
	}
	else
	{
		SetDlgItemText( IDC_STATIC_DATE1, LoadStg(HD_DSR_AVFA_TIME)); //from
		SetDlgItemText( IDC_STATIC_DATE2, LoadStg(HD_DSR_AVTA_TIME)); //to
	}

	SetDlgItemText( IDC_ADJHRS, LoadStg(IDS_STRING1340));
	SetDlgItemText( IDC_CHECK1, LoadStg(IDS_STRING1341));
	SetDlgItemText( IDOK, LoadStg(IDS_STRING1343));
	SetDlgItemText( IDCANCEL, LoadStg(ID_CANCEL));
	SetDlgItemText( IDC_BUTTONAPPLY, LoadStg(IDS_STRING1344));
}

//********************************************************************************
// Manuelle Eingabe der Mitarbeiternummer
//********************************************************************************

void CChangeShiftDlg::OnKillfocusEmpl1Number() 
{
	UpdateData(true);

	STFDATA *prlStf = ogStfData.GetStfByPeno(m_EditEmpl1);

	if (prlStf != NULL)
	{
		lmEmpl1 = prlStf->Urno;

		// Name und Code setzen
		SetDlgItemText(IDC_EMPL1_NUMBER,CString(prlStf->Peno));
		CString olName;
		olName = CBasicData::GetFormatedEmployeeName(pomParent->rmViewInfo.iEName, prlStf,"","");
		SetDlgItemText(IDC_EMPL1_NAME,olName);

		// Schicht setzen
		SetShift(1,prlStf->Urno);

		prlStf = ogStfData.GetStfByPeno(m_EditEmpl2);
		if (prlStf != NULL)
		{
			lmEmpl2 = prlStf->Urno;
		}
		else
		{
			lmEmpl2 = 0;
		}
		SetShift(2,lmEmpl2);
	}
	else
	{
		// Name und Code setzen
		SetDlgItemText(IDC_EMPL1_NUMBER,"");
		SetDlgItemText(IDC_EMPL1_NAME,"");
		// Schicht setzen
		lmEmpl1 = 0;
		SetShift(1,lmEmpl1);

		prlStf = ogStfData.GetStfByPeno(m_EditEmpl2);
		if (prlStf != NULL)
		{
			lmEmpl2 = prlStf->Urno;
		}
		else
		{
			lmEmpl2 = 0;
		}
		SetShift(2,lmEmpl2);

	}
}

//********************************************************************************
// Manuelle Eingabe der Mitarbeiternummer
//********************************************************************************

void CChangeShiftDlg::OnKillfocusEmpl2Number() 
{
	UpdateData(true);

	STFDATA *prlStf = ogStfData.GetStfByPeno(m_EditEmpl2);

	if (prlStf != NULL)
	{
		lmEmpl2 = prlStf->Urno;

		// Name und Code setzen
		SetDlgItemText(IDC_EMPL2_NUMBER,CString(prlStf->Peno));
		CString olName;
		olName = CBasicData::GetFormatedEmployeeName(pomParent->rmViewInfo.iEName, prlStf, "", "");
		SetDlgItemText(IDC_EMPL2_NAME,olName);

		// Schicht setzen
		SetShift(2,prlStf->Urno);
		prlStf = ogStfData.GetStfByPeno(m_EditEmpl1);
		if (prlStf != NULL)
		{
			lmEmpl1 = prlStf->Urno;
		}
		else
		{
			lmEmpl1 = 0;
		}
		SetShift(1,lmEmpl1);
	}
	else
	{
		// Name und Code setzen
		SetDlgItemText(IDC_EMPL2_NUMBER,"");
		SetDlgItemText(IDC_EMPL2_NAME,"");
		// Schicht setzen
		lmEmpl2 = 0;
		SetShift(2,lmEmpl2);
		prlStf = ogStfData.GetStfByPeno(m_EditEmpl1);
		if (prlStf != NULL)
		{
			lmEmpl1 = prlStf->Urno;
		}
		else
		{
			lmEmpl1 = 0;
		}
		SetShift(1,lmEmpl1);
	}
}

//********************************************************************************
// DRS Daten setzten. (Wer hat den Tausch durchgef�hrt
//********************************************************************************

void CChangeShiftDlg::SaveDrsData()
{
	DRSDATA* polDrs;

	//-------------------------------------------------------------------------
	// Mitarbeiter 1 / Schicht 1
	//-------------------------------------------------------------------------
	// aktuellen DRS finden
	polDrs = ogDrsData.GetDrsByDrru(pomDrr1->Urno);

	// Kein DRS gefunden
	if (polDrs == NULL)
	{
		//MessageBox("No DRS found, creating new DRS");
		polDrs =  ogDrsData.CreateDrsData(lmEmpl1, pomDrr1->Sday, pomDrr1->Urno);
	}

	if (polDrs != NULL)
	{
		strcpy(polDrs->Uses,ogBasicData.omUserID);

		// DRS sichern
		if (polDrs->IsChanged == DATA_NEW)	
			ogDrsData.Insert(polDrs);
		else
			ogDrsData.Update(polDrs);
	}
	//-------------------------------------------------------------------------
	// Mitarbeiter 2 / Schicht 1
	//-------------------------------------------------------------------------
	// aktuellen DRS finden
	polDrs = ogDrsData.GetDrsByDrru(pomDrr2->Urno);

	// Kein DRS gefunden
	if (polDrs == NULL)
	{
		//MessageBox("No DRS found, creating new DRS");
		polDrs =  ogDrsData.CreateDrsData(lmEmpl2, pomDrr2->Sday, pomDrr2->Urno);
	}

	if (polDrs != NULL)
	{
		strcpy(polDrs->Uses,ogBasicData.omUserID);

		// DRS sichern
		if (polDrs->IsChanged == DATA_NEW)
		{
			ogDrsData.Insert(polDrs);
		}
		else
		{
			ogDrsData.Update(polDrs);
		}
	}
}


void CChangeShiftDlg::SetCombos()
{
	// initializing the SUB1SUB2-combos
	m_C_SUB1SUB2_1.SetFont(&ogCourier_Regular_8);
	m_C_SUB1SUB2_2.SetFont(&ogCourier_Regular_8);

	CDC *pdc = m_C_SUB1SUB2_1.GetDC();
	CFont olFont;
	CFont *pOldFont	= pdc->SelectObject(&olFont);
	CSize olSizeSub1;
	CSize olSizeSub2;
	long ilWidth;

	CString olInsertSub1;
	CString olInsertSub2;

	olInsertSub1 = LoadStg(IDS_STRING202) + "  " +  LoadStg(IDS_STRING206);
	olInsertSub2 = LoadStg(IDS_STRING203) + "  " +  LoadStg(IDS_STRING214);

	m_C_SUB1SUB2_1.ResetContent();
	m_C_SUB1SUB2_1.InsertString(-1, olInsertSub1);
	m_C_SUB1SUB2_1.InsertString(-1, olInsertSub2);

	m_C_SUB1SUB2_2.ResetContent();
	m_C_SUB1SUB2_2.InsertString(-1, olInsertSub1);
	m_C_SUB1SUB2_2.InsertString(-1, olInsertSub2);

	olSizeSub1 = pdc->GetTextExtent(olInsertSub1);
	olSizeSub2 = pdc->GetTextExtent(olInsertSub2);
	ilWidth = max(olSizeSub1.cx, olSizeSub2.cx) + 20;
	m_C_SUB1SUB2_1.SetDroppedWidth((UINT)ilWidth);
	m_C_SUB1SUB2_2.SetDroppedWidth((UINT)ilWidth);
	pdc->SelectObject(&olFont);

	// init function combos
	CSize olFunctionSize;
	CString olLine;
	CString olTmp;
	ilWidth = 0;
	int i;

	m_C_FCTC1.ResetContent();
	m_C_FCTC2.ResetContent();
	m_C_FCTC1.SetFont(&ogCourier_Regular_8);
	m_C_FCTC2.SetFont(&ogCourier_Regular_8);

	for (i = 0; i < ogPfcData.omData.GetSize(); i++)
	{
		olTmp.Format("'%s'", ogPfcData.omData[i].Fctc);
		if (pomParent->rmViewInfo.oPfcStatFctcs.Find (olTmp) > -1)
		{
			olLine.Format("%-8s - %-40s", ogPfcData.omData[i].Fctc, ogPfcData.omData[i].Fctn);
			olFunctionSize = pdc->GetTextExtent(olLine);
			ilWidth = max(ilWidth, olFunctionSize.cx);
			m_C_FCTC1.AddString(olLine);
			m_C_FCTC2.AddString(olLine);
		}
	}
	m_C_FCTC1.SetDroppedWidth((UINT)(ilWidth));
	m_C_FCTC2.SetDroppedWidth((UINT)(ilWidth));

	// setting the group combos
	olLine = "";
	m_C_WGPC1.ResetContent();
	m_C_WGPC2.ResetContent();
	m_C_WGPC1.SetFont(&ogCourier_Regular_8);
	m_C_WGPC2.SetFont(&ogCourier_Regular_8);
	m_C_WGPC1.AddString(olLine);
	m_C_WGPC2.AddString(olLine);
	PGPDATA *prlPgpData = NULL;
	WGPDATA *prlWgpData = NULL;
	CSize olGroupSize;
	ilWidth = 0;

	for (i = 0; i < ogWgpData.omData.GetSize(); i++)
	{
		prlWgpData = &ogWgpData.omData[i];
		prlPgpData = ogPgpData.GetPgpByUrno (prlWgpData->Pgpu);

		if (prlPgpData != NULL)
		{
			// extract the functions of the group. Look, if in the group is one function of the view.
			CStringArray olArr;
			olTmp = prlPgpData->Pgpm;
			CedaData::ExtractTextLineFast (olArr, olTmp.GetBuffer(0), "|");

			for (int j = 0; j < olArr.GetSize(); j++)
			{
				olTmp.Format("'%s'", olArr[j]);
				if (pomParent->rmViewInfo.oPfcStatFctcs.Find (olTmp) > -1)
				{
					olLine.Format("%-5.5s %-40.40s", prlWgpData->Wgpc, prlWgpData->Wgpn);
					m_C_WGPC1.AddString(olLine);
					m_C_WGPC2.AddString(olLine);
					olGroupSize = pdc->GetTextExtent(olLine);
					ilWidth = max(ilWidth, olGroupSize.cx);
					break;
				}
			}
		}
	}
	m_C_WGPC1.SetDroppedWidth((UINT)(ilWidth-40));
	m_C_WGPC2.SetDroppedWidth((UINT)(ilWidth-40));
}

void CChangeShiftDlg::OnDatetimechangeDatetimepicker2(NMHDR* pNMHDR, LRESULT* pResult) 
{
	UpdateData(true);

	if (m_DatePicker1.m_dt > m_DatePicker2.m_dt)
	{
		CDateTimeCtrl* polDateCtrl1 = (CDateTimeCtrl*) GetDlgItem(IDC_DATETIMEPICKER1);
		COleDateTime olStartDate(m_DatePicker2.GetYear(),m_DatePicker2.GetMonth(),1,0,0,0);
		COleDateTime olEndDate(m_DatePicker2.GetYear(),m_DatePicker2.GetMonth(),CedaDataHelper::GetDaysOfMonth(m_DatePicker2),0,0,0);
		polDateCtrl1->SetRange(&olStartDate,&olEndDate);
		polDateCtrl1->SetTime(m_DatePicker2);
	}
	
	// Schichten setzen
	SetShift(1,lmEmpl1);
	SetShift(2,lmEmpl2);

	// Alle Eingaben l�schen
	ClearResults();

	LoadMonthAndEmpl(m_DatePicker1);
	
	*pResult = 0;
}
