/////////////////////////////////////////////////////////////////////////////
// CInitialLoadDlg dialog
#include <stdafx.h>

CInitialLoadDlg::CInitialLoadDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CInitialLoadDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CInitialLoadDlg)
	//}}AFX_DATA_INIT
	Create(IDD,pParent);
	ShowWindow(SW_SHOWNORMAL);

	SetWindowText(LoadStg(IDS_S_InitialLoad));

	m_Progress.SetRange(0,130);

}

CInitialLoadDlg::~CInitialLoadDlg(void)
{
	pogInitialLoad = NULL;
}

void CInitialLoadDlg::SetProgress(int ipProgress)
{
	m_Progress.OffsetPos(ipProgress);
}

BOOL CInitialLoadDlg::OnInitDialog()
{
	CDialog::OnInitDialog();
	
	//HICON m_hIcon = AfxGetApp()->LoadIcon(IDR_ROSTERTYPE);
	//SetIcon(m_hIcon, FALSE);

	int ilY = ::GetSystemMetrics(SM_CYSCREEN);
	CRect olRect, olNewRect;
	GetWindowRect(&olRect);
	int ilHeight = olRect.bottom - olRect.top;
	int ilWidth = olRect.right - olRect.left;
	olNewRect.top = (int)((int)(ilY/2) - (int)(ilHeight/2));
	olNewRect.left = 400;
	olNewRect.bottom = olNewRect.top + ilHeight;
	olNewRect.right = olNewRect.left + ilWidth;

	MoveWindow(&olNewRect);

	return FALSE;
}


void CInitialLoadDlg::SetMessage(CString opMessage, bool bpAdd)
{
	if(bpAdd)
	{
		// Text in neue Zeile Schreiben
		m_MsgList.AddString(opMessage);
	}
	else
	{
		// Text an letzte Zeile anh�mgen
		int ilCount = m_MsgList.GetCount();
		if (ilCount == 0)
		{
			// Wenn es keine Zeile gibt, neue Zeile schreiben
			m_MsgList.AddString(opMessage);
		}
		else
		{
			CString olOldText;
			// lese alten Text aus letzter Zeile
			m_MsgList.GetText(ilCount-1, olOldText);
			// l�sche letzte Zeile 
			m_MsgList.DeleteString(ilCount-1);
			// schreibe alten und neuen Text in neue Zeile
			m_MsgList.AddString(olOldText+opMessage);
		}
	}
	if ((m_MsgList.GetCount()-12) > 0)
		m_MsgList.SetTopIndex(m_MsgList.GetCount()-12);
	pogInitialLoad->UpdateWindow();
}

BOOL CInitialLoadDlg::DestroyWindow() 
{
	BOOL blRc = CDialog::DestroyWindow();
	delete this;
	return blRc;
}

void CInitialLoadDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CInitialLoadDlg)
	DDX_Control(pDX, IDC_MSGLIST, m_MsgList);
	DDX_Control(pDX, IDC_PROGRESS1, m_Progress);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CInitialLoadDlg, CDialog)
	//{{AFX_MSG_MAP(CInitialLoadDlg)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CInitialLoadDlg message handlers
