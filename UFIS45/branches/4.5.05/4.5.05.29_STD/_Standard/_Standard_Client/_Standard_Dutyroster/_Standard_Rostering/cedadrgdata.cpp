// CedaDrgData.cpp: - Klasse f�r die Handhabung von DRG-Daten (Daily Roster Groups - 
//  Tagesdienstplan Arbeitsgruppen)
//

// BDA 16.02.2000 erstellt aus CedaDrdData.cpp

#include <stdafx.h>

//************************************************************************************************************************************************
//************************************************************************************************************************************************
// Globale Funktionen
//************************************************************************************************************************************************
//************************************************************************************************************************************************

//************************************************************************************************************************************************
// ProcessDrgCf: Callback-Funktion f�r den Broadcast-Handler und die Broadcasts
//	BC_DRG_CHANGE, BC_DRG_NEW und BC_DRG_DELETE. Ruft zur eingentlichen Bearbeitung
//	der Nachrichten die Funktion CedaDrgData::ProcessDrgBc() der entsprechenden 
//	Instanz von CedaDrgData auf.	
// R�ckgabe: keine
//************************************************************************************************************************************************

void  ProcessDrgCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	// Empf�nger ermitteln
	CedaDrgData *polDrgData = (CedaDrgData *)vpInstance;
	// Bearbeitungsfunktion des Empf�ngers aufrufen
	polDrgData->ProcessDrgBc(ipDDXType,vpDataPointer,ropInstanceName);
}

//************************************************************************************************************************************************
//************************************************************************************************************************************************
// Implementation der Klasse CedaDrgData
//************************************************************************************************************************************************
//************************************************************************************************************************************************

//************************************************************************************************************************************************
// Konstruktor
//************************************************************************************************************************************************

CedaDrgData::CedaDrgData(CString opTableName, CString opExtName) : CedaData(&ogCommHandler)
{
    // Infostruktur vom Typ CEDARECINFO f�r DRG-Datens�tze
	// Wird von Vaterklasse CCSCedaData f�r verschiedene Funktionen
	// (CCSCedaData::GetBufferRecord(), CCSCedaData::MakeCedaData())
	// benutzt. Die Datenstruktur beschreibt die Tabellenstruktur.
    BEGIN_CEDARECINFO(DRGDATA, DrgDataRecInfo)
		FIELD_CHAR_TRIM	(Drrn,"DRRN")	// Schichtnummer (1-n pro Tag)
		FIELD_CHAR_TRIM	(Fctc,"FCTC")	// Funktion des Mitarbeiters (Code aus PFC.FCTC)
		FIELD_CHAR_TRIM	(Hopo,"HOPO")	// Homeairport
		FIELD_CHAR_TRIM	(Sday,"SDAY")	// Schichttag
		FIELD_LONG		(Stfu,"STFU")	// MA-Urno
		FIELD_LONG		(Urno,"URNO")	// DS-Urno
		FIELD_CHAR_TRIM	(Wgpc,"WGPC")	// Arbeitsgruppe (Code aus WGP.WGPC)
    END_CEDARECINFO

	// Infostruktur kopieren
    for (int i = 0; i < sizeof(DrgDataRecInfo)/sizeof(DrgDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&DrgDataRecInfo[i],sizeof(CEDARECINFO));
        omRecInfo.Add(prpCedaRecInfo);
	}

	// Tabellenname per Default auf DRG setzen
	opExtName = pcgTableExt;
	SetTableNameAndExt(opTableName+opExtName);

    // Feldnamen initialisieren
	strcpy(pcmListOfFields,	"DRRN,FCTC,HOPO,SDAY,STFU,URNO,WGPC");
	// Zeiger der Vaterklasse auf Tabellenstruktur setzen
	CCSCedaData::pcmFieldList = pcmListOfFields; //because in CedaData pcmFieldList is just a pointer

    // Datenarrays initialisieren
	omUrnoMap.InitHashTable(256);
}

//************************************************************************************************************************************************
// Destruktor
//************************************************************************************************************************************************

CedaDrgData::~CedaDrgData()
{
	TRACE("CedaDrgData::~CedaDrgData called\n");
	ClearAll(true);
	omRecInfo.DeleteAll();
}

//************************************************************************************************************************************************
// SetTableNameAndExt: stellt den Namen und die Extension ein.
// R�ckgabe:	false	->	Tabellenname mit Extension gr��er als
//							Klassenmember
//				true	->	alles OK
//************************************************************************************************************************************************

bool CedaDrgData::SetTableNameAndExt(CString opTableAndExtName)
{
CCS_TRY;
	if(opTableAndExtName.GetLength() >= sizeof(pcmTableName)) return false;
	strcpy(pcmTableName,opTableAndExtName.GetBuffer(0));
	return true;
CCS_CATCH_ALL;
return false;
}

//************************************************************************************************************************************************
// Register: beim Broadcasthandler anmelden. Um Broadcasts (Meldungen vom 
//	Datenbankhandler) zu empfangen, muss diese Funktion ausgef�hrt werden.
//	Der letzte Parameter ist ein Zeiger auf eine Callback-Funktion, welche
//	dann die entsprechende Nachricht bearbeitet. �ber den nach void 
//	konvertierten Zeiger auf dieses Objekt ((void *)this) kann das 
//	Objekt ermittelt werden, das der eigentliche Empf�nger der Nachricht ist
//	und an das die Callback-Funktion die Nachricht weiterleitet (per Aufruf einer
//	entsprechenden Member-Funktion).
// R�ckgabe: keine
//************************************************************************************************************************************************

void CedaDrgData::Register(void)
{
CCS_TRY;
	// bereits angemeldet?
	if (bmIsBCRegistered) return;	// ja -> gibt nichts zu tun, terminieren

	// DRG-Datensatz hat sich ge�ndert
	DdxRegister((void *)this,BC_DRG_CHANGE, "CedaDrgData", "BC_DRG_CHANGE",        ProcessDrgCf);
	DdxRegister((void *)this,BC_DRG_NEW,    "CedaDrgData", "BC_DRG_NEW",		   ProcessDrgCf);
	DdxRegister((void *)this,BC_DRG_DELETE, "CedaDrgData", "BC_DRG_DELETE",        ProcessDrgCf);
	// jetzt ist die Instanz angemeldet
	bmIsBCRegistered = true;
CCS_CATCH_ALL;
}


//************************************************************************************************************************************************
// ClearAll: aufr�umen. Alle Arrays und PointerMaps werden geleert.
// R�ckgabe:	immer true
//************************************************************************************************************************************************

bool CedaDrgData::ClearAll(bool bpUnregister)
{
CCS_TRY;
    // alle Arrays leeren
	omUrnoMap.RemoveAll();
    omKeyMap.RemoveAll();
	omData.DeleteAll();
	
	// beim BC-Handler abmelden
	if(bpUnregister && bmIsBCRegistered)
	{
		ogDdx.UnRegister(this,NOTUSED);
		// jetzt ist die Instanz abgemeldet
		bmIsBCRegistered = false;
	}
    return true;
CCS_CATCH_ALL;
return false;
}

//************************************************************************************************************************************************
// ReadDrgByUrno: liest den DRG-Datensatz mit der  Urno <lpUrno> aus der 
//	Datenbank.
// R�ckgabe:	true, wenn Datensatz erfolgreich gelesen, sonst false
//************************************************************************************************************************************************

bool CedaDrgData::ReadDrgByUrno(long lpUrno, DRGDATA *prpDrg)
{
CCS_TRY;
	CString olWhere;
	olWhere.Format("'%ld'", lpUrno);
	// Datensatz aus Datenbank lesen
	if(IsFullLoggingEnabled())
	{
		ogRosteringLogText.Format("CedaAction:\n  Cmd <RT>\n  Tbl <%s>\n  Sel <%s>",pcmTableName, olWhere);
		WriteInRosteringLog();
	}

	if (CedaAction2("RT", (char*)(LPCTSTR)olWhere) == false)
	{
		return false;	// Fehler -> abbrechen
	}
	// Datensatz-Objekt instanziieren...
	DRGDATA *prlDrg = new DRGDATA;
	// und initialisieren
	if (GetBufferRecord2(0,prpDrg) == true)
	{  
		TRACE("ReadDrgByUrno: Datensatz gefunden,\nDrrn: %s\nFctc: %s\nHopo: %s\nSday: %s\nStfu: %d\nUrno: %d\nWgpc: %s\n",
			prlDrg->Drrn, prlDrg->Fctc, prlDrg->Hopo, prlDrg->Sday, prlDrg->Stfu, prlDrg->Urno, prlDrg->Wgpc);
		ClearFastSocketBuffer();	
		return true;	// Aktion erfolgreich
	}
	else
	{
		// Kein DRG gefunden - aufr�umen und terminieren
		delete prlDrg;
		ClearFastSocketBuffer();	
		return false;
	}
        
	if(IsFullLoggingEnabled())
	{
		ogRosteringLogText.Format("CedaAction:\n  read 1 records from table %s", pcmTableName);
		WriteInRosteringLog();
	}
	
	return false;
CCS_CATCH_ALL;
return false;
}

//************************************************************************************************************************************************
// ReadFilteredByDateAndStaff: liest Datens�tze ein, deren Feld SDAY (g�ltig am)
//	innerhalb des Zeitraums <opStart> bis <opEnd> liegen. Die Parameter werden auf 
//	G�ltigkeit gepr�ft. Wenn <popLoadStfUrnoMap> nicht NULL ist, werden nur die 
//	Datens�tze geladen, deren Feld STFU (Mitarbeiter-Urno) im Array enthalten ist.
// R�ckgabe:	true	->	Aktion erfolgreich
//				false	->	Fehler beim Einlesen der Datens�tze

// ToDo: die Funktion ist direkt von CedaDrdData �bernommen, ob 'ne �nderung notwendig ist?
//************************************************************************************************************************************************

bool CedaDrgData::ReadFilteredByDateAndStaff(COleDateTime opStart, COleDateTime opEnd,
											 CMapPtrToPtr *popLoadStfUrnoMap /*= NULL*/,
											 bool bpRegisterBC /*= true*/,
											 bool bpClearData /*= true*/)
{
CCS_TRY;
	// G�ltigkeit der Datumsangaben pr�fen
	if ((opStart.GetStatus() != COleDateTime::valid) ||
		(opEnd.GetStatus() != COleDateTime::valid) || 
		(opStart >= opEnd))
	{
		// Fehler -> terminieren
		return false;
	}

	// Puffer f�r WHERE-Statement
	CString olWhere;

	// Anzahl der Urnos
	int ilCountStfUrnos = -1;
	if ((popLoadStfUrnoMap != NULL) && ((ilCountStfUrnos = popLoadStfUrnoMap->GetCount()) == 0))
	{
		// es gibt nichts zu tun, kein Fehler
		return true;	
	}
	
	// keine Urno-Liste oder Urno-Liste �bergeben aber Anzahl gr��er als max. in WHERE-Clause erlaubte MA-Urnos?
	if ((popLoadStfUrnoMap == NULL) || (ilCountStfUrnos > MAX_WHERE_STAFFURNO_IN))
	{
		// mehr Urnos als in WHERE-Clause passen -> alles laden und lokal filtern
		olWhere.Format("WHERE SDAY BETWEEN '%s' AND '%s'",opStart.Format("%Y%m%d"), opEnd.Format("%Y%m%d"),opStart.Format("%Y%m%d"), opEnd.Format("%Y%m%d"));
		// Datens�tze einlesen und Ergebnis zur�ckgeben
		return Read(olWhere.GetBuffer(0),popLoadStfUrnoMap,bpRegisterBC,bpClearData);
	}
	else
	{
		// MA-Urnos mit in WHERE-Clause aufnehmen
		POSITION pos;			// zum Iterieren
		void *plDummy;			// das Objekt, immer NULL und daher obsolet
		long llStfUrno;			// die Urno als long
		CString	olBuf;			// um die Urno in einen String zu konvertieren
		CString olStfUrnoList;	// die Urno-Liste
		// alle Urnos ermitteln
		for(pos = popLoadStfUrnoMap->GetStartPosition(); pos != NULL;)   
		{
			// n�chste Urno
			popLoadStfUrnoMap->GetNextAssoc(pos,(void*&)llStfUrno,plDummy);
			// umwandeln in String
			olBuf.Format("%ld",llStfUrno);
			// ab in die Liste
			olStfUrnoList += ",'" + olBuf + "'";
		}
		// f�hrendes Komma aus Urno-Liste entfernen, WHERE-Statement generieren
		olWhere.Format("WHERE STFU IN (%s) AND SDAY BETWEEN '%s' AND '%s'",olStfUrnoList.Mid(1),opStart.Format("%Y%m%d"), opEnd.Format("%Y%m%d"),opStart.Format("%Y%m%d"), opEnd.Format("%Y%m%d"));
		// Datens�tze einlesen und Ergebnis zur�ckgeben
		return Read(olWhere.GetBuffer(0),NULL,bpRegisterBC,bpClearData);
	}
CCS_CATCH_ALL;
return false;
}

//************************************************************************************************************************************************
// Read: liest Datens�tze ein.
// R�ckgabe:	true	->	Aktion erfolgreich
//				false	->	Fehler beim Einlesen der Datens�tze
//************************************************************************************************************************************************

bool CedaDrgData::Read(char *pspWhere /*=NULL*/, CMapPtrToPtr *popLoadStfUrnoMap /*= NULL*/,
					   bool bpRegisterBC /*= true*/, bool bpClearData /*= true*/)
{
CCS_TRY;
	// Return-Code f�r Funktionsaufrufe
	//bool ilRc = true;

	// wenn gew�nscht, aufr�umen (und beim Broadcast-Handler abmelden, wenn
	// nach Ende angemeldet werden soll)
	if (bpClearData) ClearAll(bpRegisterBC);
	
    // Datens�tze lesen
	if(IsFullLoggingEnabled())
	{
		ogRosteringLogText.Format("CedaAction:\n  Cmd <RT>\n  Tbl <%s>\n  Sel <%s>",pcmTableName, pspWhere);
		WriteInRosteringLog();
	}

	if(pspWhere == NULL)
	{
		if (!CedaData::CedaAction2("RT", ""))
			return false;
	}
	else
	{
		if (!CedaData::CedaAction2("RT", pspWhere))
			return false;
	}

    bool blMoreRecords = false;
	// Datensatzz�hler f�r TRACE
	int ilCountRecord = 0;
	// void-Pointer f�r MA-Urno-Lookup
	void  *prlVoid = NULL;
	// solange es Datens�tze gibt
	do
	{
		// neuer Datensatz
		DRGDATA *prlDrg = new DRGDATA;
		// Datensatz lesen
		blMoreRecords = GetBufferRecord2(ilCountRecord,prlDrg);
		if(!blMoreRecords)
		{
			// kein weiterer Datensatz
			delete prlDrg;
			break;
		}
		// Datensatz gelesen, Z�hler inkrementieren
		ilCountRecord++;
		// wurde ein Datensatz gelesen und ist (wenn vorhanden) die
		// Mitarbeiter-Urno dieses Datensatzes im MA-Urno-Array enthalten? 
		if((popLoadStfUrnoMap == NULL) || 
			(popLoadStfUrnoMap->Lookup((void *)prlDrg->Stfu, (void *&)prlVoid) == TRUE))
		{ 
			// Datensatz hinzuf�gen
			if (!InsertInternal(prlDrg, DRG_NO_SEND_DDX)){
				// Zuerst mu� im Server gel�scht werden - Reinigung
				//Delete(prlDrg, true);

				// Pointer l�schen
				delete prlDrg;

			}
#ifdef TRACE_FULL_DATA
			else
			{
				// Datensatz OK, loggen if FULL
				ogRosteringLogText.Format(" read %8.8lu %s OK:  ", ilCountRecord, pcmTableName);
				GetDataFormatted(ogRosteringLogText, prlDrg);
				WriteLogFull("");
			}
#endif TRACE_FULL_DATA
		}
		else
		{
			if(popLoadStfUrnoMap != NULL)
			{
				TRACE("Read-Drg: Stfu im UrnoMap nicht gefunden, Drrn: %s, Fctc: %s, Hopo: %s, Sday: %s, Stfu: %d, Urno: %d, Wgpc: %s\n",
						prlDrg->Drrn, prlDrg->Fctc, prlDrg->Hopo, prlDrg->Sday, prlDrg->Stfu, prlDrg->Urno, prlDrg->Wgpc);
			}
			
			delete prlDrg;
		}
	} while (blMoreRecords);
	ClearFastSocketBuffer();	
	
	// beim Broadcast-Handler anmelden, wenn gew�nscht
	if (bpRegisterBC) Register();

	// Test: Anzahl der gelesenen DRG
	TRACE("Read-Drg: %d gelesen\n",ilCountRecord);
		
	if(IsFullLoggingEnabled())
	{
		ogRosteringLogText.Format("CedaAction:\n  read %d records from table %s",ilCountRecord, pcmTableName);
		WriteInRosteringLog();
	}


    return true;
CCS_CATCH_ALL;
return false;
}

//*******************************************************************************************************************
// ReadSpecialData: liest Datens�tze mit Hilfe eines �bergebenen SQL-Strings ein.
// R�ckgabe:	bool Erfolg?
//*******************************************************************************************************************

bool CedaDrgData::ReadSpecialData(CCSPtrArray<DRGDATA> *popDrgArray, char *pspWhere,
								  char *pspFieldList, char *pcpSort, bool ipSYS/*=true*/)
{
CCS_TRY;
	bool ilRc = true;

	if(IsFullLoggingEnabled())
	{
		ogRosteringLogText.Format("CedaAction:\n  Cmd <RT>\n  Tbl <%s>\n  Sel <%s ORDER BY %s>",pcmTableName, pspWhere, pcpSort);
		WriteInRosteringLog();
	}

	if(ipSYS == true) 
	{
		if (CedaAction("SYS",pcmTableName,"",pspWhere,pcpSort,pcgDataBuf) == false) return false;
	}
	else
	{
		if (CedaAction2("RT",pcmTableName,"",pspWhere,pcpSort,pcgDataBuf) == false) return false;
	}
	int ilCountRecord = 0;
	if(popDrgArray != NULL)	// Datens�tze in den �bergebenen Array
	{
		CString olFieldList(pspFieldList);
		if(olFieldList.IsEmpty())
			olFieldList = pcmListOfFields;	// GetBufferRecord kann leider mit "" nichts anfangen
			
		for (ilCountRecord = 0; ilRc == true; ilCountRecord++)
		{
			DRGDATA *prlDrg = new DRGDATA;
			if ((ilRc = GetBufferRecord2(ilCountRecord,prlDrg,olFieldList)) == true)
			{
				TRACE("CedaDrgData::ReadSpecialData(): DRG gelesen, Drrn: %s, Fctc: %s, Hopo: %s, Sday: %s, Stfu: %d, Urno: %d, Wgpc: %s\n",
					prlDrg->Drrn, prlDrg->Fctc, prlDrg->Hopo, prlDrg->Sday, prlDrg->Stfu, prlDrg->Urno, prlDrg->Wgpc);
				popDrgArray->Add(prlDrg);
			}
			else
			{
				delete prlDrg;
			}
		}
		if(popDrgArray->GetSize() == 0) return false;
	}
        
	if(IsFullLoggingEnabled())
	{
		ogRosteringLogText.Format("CedaAction:\n  read %d records from table %s",ilCountRecord-1, pcmTableName);
		WriteInRosteringLog();
	}
	ClearFastSocketBuffer();	

	return true;
CCS_CATCH_ALL;
return false;
}

//*********************************************************************************************
// Insert: speichert den Datensatz <prpDrg> in der Datenbank und schickt einen
//	Broadcast ab.
// R�ckgabe:	false	->	Fehler
//				true	->	alles OK
//*********************************************************************************************

bool CedaDrgData::Insert(DRGDATA *prpDrg, bool bpSave /*= true*/)
{
CCS_TRY;
	// �nderungs-Flag setzen
	prpDrg->IsChanged = DATA_NEW;
	// in Datenbank speichern, wenn erw�nscht
	if(bpSave && Save(prpDrg) == false) return false;
	// Broadcast DRG_NEW abschicken
	InsertInternal(prpDrg,true);
    return true;
CCS_CATCH_ALL;
return false;
}

/*********************************************************************************************
InsertInternal: 1. pr�fft, ob der neue Datensatz korrekt ist, u.a. auch ob schon ein Datensatz
mit gleichem prim�ren Schl�ssel "SDAY-DRRN-STFU" gespeichert ist, wenn ja - return FALSE 
(es kann nur einen geben), wenn nein
2. f�gt den internen Arrays einen neuen Datensatz hinzu. 
Der Datensatz muss fr�her oder sp�ter explizit durch Aufruf von Save() oder Release() in der Datenbank 
gespeichert werden. Wenn <bpSendDdx> true ist, wird ein Broadcast gesendet.
R�ckgabe:	false	->	Fehler
			true	->	alles OK
*********************************************************************************************/

bool CedaDrgData::InsertInternal(DRGDATA *prpDrg, bool bpSendDdx)
{
CCS_TRY;
	if(!IsValidDrg(prpDrg))
	{
		if(IsTraceLoggingEnabled())
		{
			GetDefectDataString(ogRosteringLogText, (void*)prpDrg);
			WriteInRosteringLog(LOGFILE_TRACE);
		}
		return false;
	}

	// Datensatz intern anf�gen
	omData.Add(prpDrg);

	// Primary-Key dieses Datensatzes als String
	CString olPrimaryKey;
	olPrimaryKey.Format("%s-%s-%ld",prpDrg->Sday,prpDrg->Drrn,prpDrg->Stfu);
//	TRACE("Speichere DRG mit Schl�ssel = %s\n",olPrimaryKey);
	omKeyMap.SetAt(olPrimaryKey,prpDrg);
	// Datensatz der Urno-Map hinzuf�gen
	omUrnoMap.SetAt((void *)prpDrg->Urno,prpDrg);
		
	// Broadcast senden?
	if( bpSendDdx == true )
	{
		// ja -> go!
		ogDdx.DataChanged((void *)this,DRG_NEW,(void *)prpDrg);
	}
	return true;
CCS_CATCH_ALL;
return false;
}

//*********************************************************************************************
// Update: sucht den Datensatz mit der Urno <prpDrg->Urno> und speichert ihn
//	in der Datenbank.
// R�ckgabe:	false	->	Datensatz wurde nicht gefunden
//				true	->	alles OK
//*********************************************************************************************

bool CedaDrgData::Update(DRGDATA *prpDrg)
{
CCS_TRY;
	// Datensatz raussuchen
	if (GetDrgByUrno(prpDrg->Urno) != NULL)
	{
		// �nderungsflag setzen
		if (prpDrg->IsChanged == DATA_UNCHANGED)
		{
			prpDrg->IsChanged = DATA_CHANGED;
		}
		// in die Datenbank speichern
		if(Save(prpDrg) == false) return false; 
		// Broadcast DRG_CHANGE versenden
		UpdateInternal(prpDrg);
	}
    return true;
CCS_CATCH_ALL;
return false;
}

//*********************************************************************************************
// UpdateInternal: �ndert einen Datensatz, der in der internen Datenhaltung
//	enthalten ist.
// R�ckgabe:	false	->	Datensatz wurde nicht gefunden
//				true	->	alles OK
//*********************************************************************************************

bool CedaDrgData::UpdateInternal(DRGDATA *prpDrg, bool bpSendDdx /*true*/)
{
CCS_TRY;
	// Broadcast senden, wenn gew�nscht
	if( bpSendDdx == true )
	{
		ogDdx.DataChanged((void *)this,DRG_CHANGE,(void *)prpDrg);
	}
	return true;
CCS_CATCH_ALL;
return false;
}

//*********************************************************************************************
// DeleteInternal: l�scht einen Datensatz aus den internen Arrays.
// R�ckgabe:	keine
//*********************************************************************************************

void CedaDrgData::DeleteInternal(DRGDATA *prpDrg, bool bpSendDdx /*true*/)
{
CCS_TRY;
	// Urno-Schl�ssel l�schen
	omUrnoMap.RemoveKey((void *)prpDrg->Urno);

	CString olKey;
	// Prim�rschl�ssel generieren
	olKey.Format("%s-%s-%ld", prpDrg->Sday, prpDrg->Drrn, prpDrg->Stfu);

	omKeyMap.RemoveKey(olKey);

	// Datensatz l�schen
	int ilCount = omData.GetSize();
	for (int i = 0; i < ilCount; i++)
	{
		if (omData[i].Urno == prpDrg->Urno)
		{
			omData.DeleteAt(i);//Update omData
			break;
		}
	}

	// Broadcast senden
	// Wir brauchen keinen DRG-Pointer mitzuschicken, viel sauberer und konsequenter ist, wenn
	// die Daten direkt aus dem Bestand gelesen werden. Gel�schter Datensatz wird damit nicht 
	// auftauchen.
	if(bpSendDdx == true)
	{
		ogDdx.DataChanged((void *)this,DRG_DELETE,(void *)0);
	}

CCS_CATCH_ALL;
}

//*********************************************************************************************
// RemoveInternalByStaffUrno: entfernt alle DRGs aus der internen Datenhaltung,
//	die dem Mitarbeiter <lpStfUrno> zugeordnet sind. Die Datens�tze werden NICHT 
//	aus der Datenbank gel�scht.
// R�ckgabe:	false	->	Fehler
//				true	->	alles OK
//*********************************************************************************************

bool CedaDrgData::RemoveInternalByStaffUrno(long lpStfUrno, bool bpSendBC /*= false*/)
{
CCS_TRY;
	DRGDATA *prlDrg;
	// alle DRGs durchgehen
	for(int i=0; i<omData.GetSize(); i++)
	{
		// DRG dieses MAs entfernen?
		if(lpStfUrno == omData[i].Stfu)
		{
			// ja
			if ((prlDrg =  GetDrgByUrno(omData[i].Urno)) == NULL) return false;
			// DRG entfernen
			DeleteInternal(prlDrg, bpSendBC);
			// neue Datensatz-Anzahl anpassen
			i--;
		}
	}
	return true;
CCS_CATCH_ALL;
return false;
}

//*********************************************************************************************
// Delete: markiert den Datensatz <prpDrg> als gel�scht und l�scht den Datensatz
//	aus der internen Datenhaltung und der Datenbank. 
// R�ckgabe:	false	->	Fehler
//				true	->	alles OK
//*********************************************************************************************

bool CedaDrgData::Delete(DRGDATA *prpDrg, BOOL bpWithSave)
{
CCS_TRY;
	// Flag setzen
	prpDrg->IsChanged = DATA_DELETED;
	
	// speichern
	if(bpWithSave == TRUE)
	{
		if (!Save(prpDrg)) return false;
	}

	// aus der internen Datenhaltung l�schen
	DeleteInternal(prpDrg,true);

	return true;
CCS_CATCH_ALL;
return false;
}

//*********************************************************************************************
// ProcessDrgBc: behandelt die einlaufenden Broadcasts.
// R�ckgabe:	keine
//*********************************************************************************************

void  CedaDrgData::ProcessDrgBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
CCS_TRY;
#ifdef _DEBUG
	// Performance-Trace
	SYSTEMTIME rlPerf;
	GetSystemTime(&rlPerf);
	TRACE("CedaDrgData::ProcessDrgBc: START %d:%d:%d.%d\n",rlPerf.wHour,rlPerf.wMinute,rlPerf.wSecond,rlPerf.wMilliseconds);
#endif // _DEBUG

	// Datensatz-Urno
	long llUrno;
	// Daten und Info
	struct BcStruct *prlBcStruct = NULL;
	prlBcStruct = (struct BcStruct *) vpDataPointer;
	DRGDATA *prlDrg = NULL;

		
	if(IsFullLoggingEnabled())
	{
		ogRosteringLogText.Format("ProcessBC:\n  Cmd <%s>\n  Tbl <%s>\n  Twe <%s>\n  Sel <%s>\n  Fld <%s>\n  Dat <%s>\n  Bcn <%s>",prlBcStruct->Cmd, prlBcStruct->Object, prlBcStruct->Twe, prlBcStruct->Selection, prlBcStruct->Fields, prlBcStruct->Data,prlBcStruct->BcNum);
		WriteInRosteringLog();
	}

	switch(ipDDXType)
	{
	case BC_DRG_CHANGE:	// Datensatz �ndern
		{
			// Datenatz-Urno ermitteln
			CString olSelection = (CString)prlBcStruct->Selection;
			if (olSelection.Find('\'') != -1)
			{
				llUrno = GetUrnoFromSelection(prlBcStruct->Selection);
			}
			else
			{
				int ilFirst = olSelection.Find("=")+2;
				int ilLast  = olSelection.GetLength();
				llUrno = atol(olSelection.Mid(ilFirst,ilLast-ilFirst));
			}
			// pr�fen, ob der Datensatz bereits in der internen Datenhaltung enthalten ist
			prlDrg = GetDrgByUrno(llUrno);
			if(prlDrg != NULL)
			{
				// ja -> Datensatz aktualisieren
				GetRecordFromItemList(prlDrg,prlBcStruct->Fields,prlBcStruct->Data);
				UpdateInternal(prlDrg);
				break;
			}
			// nein -> gehe zum Insert, !!! weil es kann sein, dass diese �nderung den Mitarbeiter in View laden soll
		}
	case BC_DRG_NEW: // neuen Datensatz einf�gen
		{
			// einzuf�gender Datensatz
			prlDrg = new DRGDATA;
			GetRecordFromItemList(prlDrg,prlBcStruct->Fields,prlBcStruct->Data);
			InsertInternal(prlDrg, DRG_SEND_DDX);
		}
		break;
	case BC_DRG_DELETE:	// Datensatz l�schen
		{
			// Datenatz-Urno ermitteln
			CString olSelection = (CString)prlBcStruct->Selection;
			if (olSelection.Find('\'') != -1)
			{
				llUrno = GetUrnoFromSelection(prlBcStruct->Selection);
			}
			else
			{
				int ilFirst = olSelection.Find("=")+2;
				int ilLast  = olSelection.GetLength();
				llUrno = atol(olSelection.Mid(ilFirst,ilLast-ilFirst));
			}
			// pr�fen, ob der Datensatz bereits in der internen Datenhaltung enthalten ist
			prlDrg = GetDrgByUrno(llUrno);
			if (prlDrg != NULL)
			{
				// ja -> Datensatz l�schen
				DeleteInternal(prlDrg);
			}
		}
		break;
	default:
		break;
	}
#ifdef _DEBUG
	// Performance-Test
	GetSystemTime(&rlPerf);
	TRACE("CedaDrgData::ProcessDrgBc: END %d:%d:%d.%d\n",rlPerf.wHour,rlPerf.wMinute,rlPerf.wSecond,rlPerf.wMilliseconds);
#endif // _DEBUG
CCS_CATCH_ALL;
}

//*********************************************************************************************
// GetDrgByUrno: sucht den Datensatz mit der Urno <lpUrno>.
// R�ckgabe:	ein Zeiger auf den Datensatz oder NULL, wenn der
//				Datensatz nicht gefunden wurde
//*********************************************************************************************

DRGDATA *CedaDrgData::GetDrgByUrno(long lpUrno)
{
CCS_TRY;
	// der Datensatz
	DRGDATA *prpDrg;
	// Datensatz in Urno-Map suchen
	if (omUrnoMap.Lookup((void*)lpUrno,(void *& )prpDrg) == TRUE)
	{
		return prpDrg;	// gefunden -> Zeiger zur�ckgeben
	}
	// nicht gefunden -> R�ckgabe NULL
	return NULL;
CCS_CATCH_ALL;
return NULL;
}

//*********************************************************************************************
// GetDrgByKey: sucht den Datensatz mit dem Prim�rschl�ssel aus 
//	Schichttag (<opSday>), Schichtnummer (<lpDrrn>) und Mitarbeiter-
//	Urno (<lpStfu>).
// R�ckgabe:	ein Zeiger auf den Datensatz oder NULL, wenn der
//				Datensatz nicht gefunden wurde
//*********************************************************************************************

DRGDATA *CedaDrgData::GetDrgByKey(CString opSday, long lpStfu, CString opDrrn)
{
	// Puffer f�r den Prim�rschl�ssel
	CString olKey;
	// Prim�rschl�ssel generieren
	olKey.Format("%s-%s-%ld", opSday, opDrrn, lpStfu);
	//	TRACE("CedaDrgData::GetDrgByKey: Suche DRG mit Key = %s:\n",olKey);
	// der Datensatz
	DRGDATA *prlDrg = NULL;
	// Datensatz suchen
	if (omKeyMap.Lookup(olKey,(void *&)prlDrg) == TRUE)
	{
		// Datensatz gefunden -> Zeiger darauf zur�ckgeben
//		TRACE("CedaDrgData::GetDrgByKey: DRG gefunden\n",olKey);
		return prlDrg;
	}
	// Datensatz nicht gefunden -> R�ckgabe NULL
	return NULL;
}

/*********************************************************************************************
GetDrgBySdayAndStfu: sucht den Datensatz mit dem Prim�rschl�ssel aus 
Schichttag (<opSday>) und Mitarbeiter-Urno (<lpStfu>).
Es gibt nur ein DRG pro MA/Tag/Schicht, und z.Zt. wird angenommen, da� es nur eine Schicht 
pro Tag in Gruppen bearbeitet wird (Darstellungsproblem) doch die Drrn kann auch != 1 sein
R�ckgabe:	ein Zeiger auf den Datensatz oder NULL, wenn der Datensatz nicht gefunden wurde
*********************************************************************************************/

DRGDATA *CedaDrgData::GetDrgBySdayAndStfu(CString opSday, long lpStfu)
{
CCS_TRY;
	//	TRACE("CedaDrgData::GetDrgBySdayAndStfu: Suche DRG mit Key = %s-%ld:\n",opSday,lpStfu);
	
	CString olKey;
	BOOL bFound = false;
	DRGDATA *prlDrg = NULL;

	// Datensatz suchen
	if(omKeyMap.GetCount())
	{
		for(POSITION olPos = omKeyMap.GetStartPosition(); olPos != NULL; )
		{
			omKeyMap.GetNextAssoc( olPos, olKey, (void*&)prlDrg);
			if(prlDrg != 0 && prlDrg->Stfu == lpStfu &&
			   prlDrg->Sday == opSday)
			{
				// Datensatz gefunden -> Zeiger darauf zur�ckgeben
				// TRACE("CedaDrgData::GetDrgBySdayAndStfu: DRG gefunden\n",olKey);
				bFound = true;
				break;
			}
		}
	}
	if(!bFound) 
		return 0;	// nichts gefunden
	return prlDrg;
CCS_CATCH_ALL;
	return 0;
}

/*********************************************************************************************
GetDrgByPfcWithTime: sucht alle Datens�tze im angegebenen Zeitraum opEnd - opStart raus
opSortString bestimmt, wie die R�ckgabedaten sortiert werden sollen
R�ckgabe:	gef�llter popDrgData und int Anzahl der gefundenen Datens�tze
*********************************************************************************************/
int CedaDrgData::GetDrgArrayByTime(COleDateTime opStart, COleDateTime opEnd, char* opSortString, CCSPtrArray<DRGDATA> *popDrgData)
{
CCS_TRY;

	if(!popDrgData || opStart.GetStatus() != COleDateTime::valid|| opEnd.GetStatus() != COleDateTime::valid || opStart > opEnd)
		// Parameter ung�ltig
		return 0;

	popDrgData->DeleteAll();

	// Where-Statement f�r DRG-Abfrage: alle DRGs, die f�r den 
	// ausgew�hlten Tag gelten sortiert nach Mitarbeiter-Funktion, 
	// Arbeitsgruppe und Schl�ssel
	
	CString olWhere;	// Puffer f�r WHERE-Clause
	if(opStart == opEnd)
	{
		olWhere.Format("WHERE SDAY = '%s'", opStart.Format("%Y%m%d"));
	}
	else
	{
		olWhere.Format("WHERE SDAY BETWEEN '%s' AND '%s'",opStart.Format("%Y%m%d"), opEnd.Format("%Y%m%d"),opStart.Format("%Y%m%d"), opEnd.Format("%Y%m%d"));
	}

	ReadSpecialData(popDrgData,(char*)(LPCTSTR)olWhere,"","STFU,SDAY",false);
	SortBySurnAndTime(popDrgData);
	// Anzahl der gefundenen Datens�tze
	return popDrgData->GetSize();

CCS_CATCH_ALL;
	return 0;
}

/*********************************************************************************************
IsValidDrg: pr�ft alles, was man pr�fen kann, inklusive g�ltigen Codes und duplizierten DRGs
0. Die L�nge der Datenfelder pr�fen
1. Datenfelder pr�fen:
Drrn	mu� eine String mit Nummer > 0 sein
Fctc	ein von PFC-Codes
Hopo	mu� dem pcmHomeAirport entsprechen
Sday	Datumsangabe in der Form YYYYMMDD
Stfu	!= 0
Urno	!= 0
Wgpc	ein von WGP-Codes
2. pr�fen, da� es nur ein DRG pro MA & DRRN & SDAY gibt
  R�ckgabe:	true	->	DRG ist OK
			false	->	DRG ist nicht OK
*********************************************************************************************/

bool CedaDrgData::IsValidDrg(DRGDATA *popDrg)
{
CCS_TRY;
	if(!popDrg)
	{
		return false;
	}

	/*
	0. Die L�nge der Datenfelder pr�fen
	1. Datenfelder pr�fen:
	Drrn	mu� eine String mit Nummer > 0 sein
	Fctc	ein von PFC-Codes oder leer
	Sday	Datumsangabe in der Form YYYYMMDD
	Stfu	!= 0
	Urno	!= 0
	Wgpc	ein von WGP-Codes oder leer (im Falle, wenn der MA aus der Gruppe rausfliegt)
	*/
	if(	!strlen(popDrg->Drrn) || 
		!strlen(popDrg->Hopo) ||
		!strlen(popDrg->Sday) || 
		strlen(popDrg->Drrn) > DRRN_LEN ||
		strlen(popDrg->Fctc) > FCTC_LEN ||
		strlen(popDrg->Hopo) > HOPO_LEN ||
		strlen(popDrg->Sday) > SDAY_LEN ||
		strlen(popDrg->Wgpc) > WGPC_LEN ||
		!popDrg->Stfu	      || 
		!popDrg->Urno)
	{
		return false;
	}

	if(atoi(popDrg->Drrn) <= 0) 
	{
		return false;
	}

	if(	strlen(popDrg->Fctc) &&
		!ogPfcData.GetPfcByFctc(CString(popDrg->Fctc)))
	{
		return false;
	}

	if(strcmp(popDrg->Hopo,pcmHomeAirport))
	{
		return false;
	}

	COleDateTime olDTime;
	if(!YYYYMMDDToOleDateTime(CString(popDrg->Sday), olDTime)) 
	{
		return false;
	}

	//Commented to save the empty workgroup assignment to internal
	//if(strlen(popDrg->Wgpc) && !ogWgpData.GetWgpByWgpc(CString(popDrg->Wgpc)))
	//{
	//	return false;
	//}
	
	// 2. pr�fen, da� es nur ein DRG pro MA & DRRN & SDAY gibt
	if(0 != GetDrgByKey(CString(popDrg->Sday), popDrg->Stfu, CString(popDrg->Drrn)))
	{
#ifdef _DEBUG
		DRGDATA* prlDrg = GetDrgByKey(CString(popDrg->Sday), popDrg->Stfu, CString(popDrg->Drrn));
		TRACE("IsValidDrg(): Ein DRG-Datensatz mit gleichen Daten ist schon vorhanden:\n\
			Drrn: %s\nFctc: %s\nHopo: %s\nSday: %s\nStfu: %d\nUrno: %d\nWgpc: %s\n",
				prlDrg->Drrn, prlDrg->Fctc, prlDrg->Hopo, prlDrg->Sday, prlDrg->Stfu, prlDrg->Urno, prlDrg->Wgpc);
#endif
		return false;	// es gibt schon einen!
	}


	// DRG ist g�ltig
	return true;
CCS_CATCH_ALL;
return false;
}

//*******************************************************************************
// Save: speichert den Datensatz <prpDrg>.
// R�ckgabe:	bool Erfolg?
//*******************************************************************************

bool CedaDrgData::Save(DRGDATA *prpDrg)
{
CCS_TRY;
	bool olRc = true;
	CString olListOfData;
	char pclSelection[1024];
	char pclData[2048];

	if (prpDrg->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}

	switch(prpDrg->IsChanged)   // New job, insert into database
	{
	case DATA_NEW:
		MakeCedaData(&omRecInfo,olListOfData,prpDrg);
		strcpy(pclData,olListOfData);
		olRc = CedaAction("IRT","","",pclData);
		prpDrg->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = '%ld'", prpDrg->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpDrg);
		strcpy(pclData,olListOfData);
		olRc = CedaAction("URT",pclSelection,"",pclData);
		prpDrg->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_DELETED:
		sprintf(pclSelection, "WHERE URNO = '%ld'", prpDrg->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpDrg);
		strcpy(pclData,olListOfData);
		olRc = CedaAction("DRT",pclSelection,"",pclData);
		break;
	}
	// R�ckgabe: Ergebnis der CedaAction
	return olRc;
CCS_CATCH_ALL;
return false;
}

//*************************************************************************************
// CedaAction: ruft die Funktion CedaAction der Basisklasse CCSCedaData auf.
//	Alle Parameter werden durchgereicht. Aufgrund eines internen CEDA-Bugs,
//	der daf�r sorgen kann, dass die Tabellenbeschreibung <pcmListOfFields>
//	zerst�rt wird, muss sicherheitshalber <pcmListOfFields> vor jedem Aufruf
//	von CedaData::CedaAction() gerettet und danach wiederhergestellt werden.
// R�ckgabe:	der R�ckgabewert von CedaData::CedaAction()
//*************************************************************************************

bool CedaDrgData::CedaAction(char *pcpAction, char *pcpTableName, char *pcpFieldList,
							 char *pcpSelection, char *pcpSort, char *pcpData, 
							 char *pcpDest /*= "BUF1"*/,bool bpIsWriteAction /*= false*/, 
							 long lpID /*= 0*/, CCSPtrArray<RecordSet> *pomData /*= NULL*/, 
							 int ipFieldCount /*= 0*/)
{
CCS_TRY;
	// R�ckgabepuffer
	bool blRet;
	// Feldliste retten
	CString olOrigFieldList = GetFieldList();
	// CedaAction ausf�hren
	blRet = CedaData::CedaAction(pcpAction,pcpSelection,pcpSort,pcpData,pcpDest,bpIsWriteAction,lpID);
	// Feldliste wiederherstellen
	ClearFastSocketBuffer();	
	SetFieldList(olOrigFieldList);
	// R�ckgabecode von CedaAction durchreichen
	return blRet;
CCS_CATCH_ALL;
return false;
}

//*************************************************************************************
// CedaAction: kurze Version, Beschreibung siehe oben.
// R�ckgabe:	der R�ckgabewert von CedaData::CedaAction()
//*************************************************************************************

bool CedaDrgData::CedaAction(char *pcpAction, char *pcpSelection, char *pcpSort,
							 char *pcpData, char *pcpDest /*"BUF1"*/, 
							 bool bpIsWriteAction/* false */, long lpID /* = imBaseID*/)
{
CCS_TRY;
	// R�ckgabepuffer
	bool blRet;
	// Feldliste retten
	CString olOrigFieldList = GetFieldList();
	// CedaAction ausf�hren
	blRet = CedaData::CedaAction(pcpAction,CCSCedaData::pcmTableName, CCSCedaData::pcmFieldList,(pcpSelection != NULL)? pcpSelection: CCSCedaData::pcmSelection,(pcpSort != NULL)? pcpSort: CCSCedaData::pcmSort,pcpData,pcpDest,bpIsWriteAction, lpID);
	// Feldliste wiederherstellen
	SetFieldList(olOrigFieldList);
	// R�ckgabecode von CedaAction durchreichen
	return blRet;
CCS_CATCH_ALL;
return false;
}

//************************************************************************************************************************
//************************************************************************************************************************
// die folgenden Funktionen sind Hilfsfunktionen zum Manipulieren von 
// Objekten des Typs DRGDATA. Die Funktionen wurden als Member der Klasse 
// CedaDrgData angelegt, um den Zugriff und die Manipulation von DRG-Datens�tzen
// zu zentralisieren und in einer Klasse zu kapseln.
//************************************************************************************************************************
//************************************************************************************************************************

//************************************************************************************************************************************************
// CreateDrgData: initialisiert eine Struktur vom Typ DRGDATA.
// R�ckgabe:	ein Zeiger auf den ge�nderten / erzeugten Datensatz oder
//				NULL, wenn ein Fehler auftrat.
//************************************************************************************************************************************************

DRGDATA* CedaDrgData::CreateDrgData(long lpStfUrno, CString opDay, CString opDrrn)
{
CCS_TRY;
	// Objekt erzeugen
	DRGDATA *prlDrgData = new DRGDATA;
	// �nderungsflag setzen
	prlDrgData->IsChanged = DATA_NEW;
	// Schichttag einstellen
	strcpy(prlDrgData->Sday,opDay.GetBuffer(0));
	// Mitarbeiter-Urno
	prlDrgData->Stfu = lpStfUrno;
	// n�chste freie Datensatz-Urno ermitteln und speichern
	prlDrgData->Urno = ogBasicData.GetNextUrno();
	// DRRN setzen (Nummer des DRG bei mehreren Schichten an einem Tag)
	strcpy(prlDrgData->Drrn,opDrrn.GetBuffer(0));
	// Hopo
	strcpy(prlDrgData->Hopo,pcmHomeAirport);
	// Zeiger auf Datensatz zur�ck
	return prlDrgData;
CCS_CATCH_ALL;
return NULL;
}

/**********************************************************************************************************
// DRGDATA erzeugen und initialisieren mit angegebenen Parametern ohne Aufnahme in die interne Datenhaltung
beliebige Pointer k�nnen NULL sein
**********************************************************************************************************/
DRGDATA* CedaDrgData::CreateDrgData(char* ppDrrn, char* ppFctc, char* ppHopo, char* ppSday, long lpStfu, char* ppWgpc)
{
	CCS_TRY;
	//Parameter testen
	if(	ppDrrn != 0 && strlen(ppDrrn) > DRRN_LEN ||
		ppFctc != 0 && strlen(ppFctc) > FCTC_LEN ||
		ppHopo != 0 && strlen(ppHopo) > HOPO_LEN ||
		ppSday != 0 && strlen(ppSday) > SDAY_LEN ||
		ppWgpc != 0 && strlen(ppWgpc) > WGPC_LEN) return 0;
	
	// Objekt erzeugen
	DRGDATA *prlDrgData = new DRGDATA;
	if(!prlDrgData) return 0;
	// �nderungsflag setzen
	prlDrgData->IsChanged = DATA_NEW;
	if(	ppDrrn != 0 )
		strcpy(prlDrgData->Drrn,ppDrrn);
	if(	ppFctc != 0 )
		strcpy(prlDrgData->Fctc,ppFctc);
	if(	ppHopo != 0 )
		strcpy(prlDrgData->Hopo,ppHopo);
	if(	ppSday != 0 )
		strcpy(prlDrgData->Sday,ppSday);
	
	prlDrgData->Stfu = lpStfu;
	if(	ppWgpc != 0 )
		strcpy(prlDrgData->Wgpc,ppWgpc);
	// Zeiger auf Datensatz zur�ck
	return prlDrgData;
	CCS_CATCH_ALL;
	return NULL;
}


//************************************************************************************************************************************************
// CompareDrgToDrg: zwei DRGs miteinander vergleichen. Wenn <bpCompareKey> gesetzt ist,
//	werden auch die Felder, die den Schl�ssel ergeben verglichen. 
// R�ckgabe:	false	->	die Werte der Felder beider DRGs sind gleich
//				true	->	die Werte der Felder beider DRGs sind unterschiedlich
//************************************************************************************************************************************************

bool CedaDrgData::CompareDrgToDrg(DRGDATA *popDrg1, DRGDATA *popDrg2,bool bpCompareKey /*= false*/)
{
CCS_TRY;
	// Schl�sselfelder vergleichen, wenn gew�nscht
	if (bpCompareKey && ((popDrg1->Stfu != popDrg2->Stfu) || 
		(strcmp(popDrg1->Sday,popDrg2->Sday) != 0) || (strcmp(popDrg1->Drrn,popDrg2->Drrn) != 0))) 
	{
		return true;
	}
	// andere Felder vergleichen
	if ((strcmp(popDrg1->Fctc,popDrg2->Fctc) != 0) || 
		(strcmp(popDrg1->Hopo,popDrg2->Hopo) != 0) || 
		(strcmp(popDrg1->Wgpc,popDrg2->Wgpc) != 0))
	{
		return true;
	}
CCS_CATCH_ALL;
return false;
}

//************************************************************************************************************************************************
// CopyDrg: kopiert alles, ausser Urno
// R�ckgabe:	keine
//************************************************************************************************************************************************

void CedaDrgData::CopyDrg(DRGDATA* popDrgDataSource,DRGDATA* popDrgDataTarget)
{
CCS_TRY;
	// Daten kopieren
	strcpy(popDrgDataTarget->Drrn,popDrgDataSource->Drrn);
	strcpy(popDrgDataTarget->Fctc,popDrgDataSource->Fctc);
	strcpy(popDrgDataTarget->Hopo,popDrgDataSource->Hopo);
	strcpy(popDrgDataTarget->Sday,popDrgDataSource->Sday);
	popDrgDataTarget->Stfu = popDrgDataSource->Stfu;
	strcpy(popDrgDataTarget->Wgpc,popDrgDataSource->Wgpc);
CCS_CATCH_ALL;
}

/************************************************************************************************************************************************
Sortieren 
************************************************************************************************************************************************/

static int CompareDrgBySurnAndTime( const DRGDATA **e1, const DRGDATA **e2);

/*****************************************************************************
*****************************************************************************/
void CedaDrgData::SortBySurnAndTime(CCSPtrArray<DRGDATA> *popDrgArray)
{
	popDrgArray->Sort(CompareDrgBySurnAndTime);
}

//****************************************************************************
// CompareGroupByTimeFunc: vergleicht zwei Arbeitsgruppen nach Zeit, bei 
//	Gleichheit der Zeit nach Code.
// R�ckgabe:	<0	->	e1 < e2
//				==0	->	e1 == e2
//				>0	->	e1 > e2
//****************************************************************************

static int CompareDrgBySurnAndTime( const DRGDATA **e1, const DRGDATA **e2)
{
	if((**e1).Stfu > (**e2).Stfu)
		return 1;
	else if((**e1).Stfu < (**e2).Stfu)
		return -1;
	return strcmp((**e1).Sday, (**e2).Sday);
}

