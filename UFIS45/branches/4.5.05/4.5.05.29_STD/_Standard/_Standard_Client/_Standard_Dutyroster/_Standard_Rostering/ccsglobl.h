// CCSGlobl.h: interface for the CCSGlobl class.
//
//////////////////////////////////////////////////////////////////////
 
#if !defined(AFX_CCSGLOBL_H__55D90F34_191F_11D1_82BE_0080AD1DC701__INCLUDED_)
#define AFX_CCSGLOBL_H__55D90F34_191F_11D1_82BE_0080AD1DC701__INCLUDED_

#ifndef __CCSGLOBL_H__
#define __CCSGLOBL_H__

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000


#include <CCSDefines.h>
#include <CCSTime.h>
#include <iomanip.h>
#include <iostream.h>
#include <fstream.h>


/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
// Global Variable Section


class CCSBcHandle;
class CCSCedaCom;
class CCSDdx;
class CCSLog;
class CBasicData;

class CedaPerData;
class CedaCfgData;
class CedaSysTabData;
class CedaCotData;
class CedaPfcData;
class CedaOdaData;
class CedaOacData;
class CedaDrrData;
class CedaDrdData;
class CedaDrgData;
class CedaDraData;
class CedaDrsData;
class CedaOrgData;
class CedaSprData;
class PrivList;
class ConflictCheck;
class CedaShiftCheck;
class CedaSdtData;
class CedaSgrData;
class CedaSgmData;
class CedaBasicData;

extern CedaBasicData	ogBCD;
extern CCSBcHandle		ogBcHandle;
extern CCSCedaCom		ogCommHandler;  // The one and only CedaCom object
extern CCSDdx			ogDdx;
extern CCSLog			ogLog;
extern CBasicData		ogBasicData;
extern CedaCfgData		ogCfgData;
extern CedaCotData		ogCotData;
extern CedaOdaData		ogOdaData;
extern CedaOacData		ogOacData;
extern CedaPfcData		ogPfcData;
extern CedaOrgData		ogOrgData;
extern CedaDrrData		ogDrrData;
extern CedaDrdData		ogDrdData;
extern CedaDrgData		ogDrgData;
extern CedaDraData		ogDraData;
extern CedaDrsData		ogDrsData;
extern CedaSprData		ogSprData;
extern CedaSdtData		ogSdtData;
extern CedaSgrData		ogSgrData;
extern CedaSgmData		ogSgmData;
extern CedaSysTabData	ogSysTabData;
extern CedaShiftCheck	ogShiftCheck;
extern NameConfigurationDlg* pogNameConfigurationDlg;

extern bool bgIsModal;
extern bool bgFirstLog;
extern bool bgFirstErrLog;

extern PrivList ogPrivList;

extern const char *pcgAppName; // Name of *.exe file of this
extern const char *pcgReportName;
extern const char *pcgAppl;    // f�r die 8 Stelligen APPL und APPN Felder in der DB

extern CString ogAppName;  // Name of *.exe file of this
extern CString ogAppl;     // f�r die 8 Stelligen APPL und APPN Felder in der DB
extern CString ogCustomer;

extern char pcgUser[33];
extern char pcgPasswd[33];

extern ofstream of_catch;
extern CString ogBackTrace;

// CCS_TRY and CCS_CATCH_ALL ///////////////////////////////////////////////
#ifndef _DEBUG
#define  CCS_TRY try{
#define  CCS_CATCH_ALL }\
						catch(...)\
						{\
						    char pclExcText[512]="";\
						    char pclText[1024]="";\
						    sprintf(pclText, "An internal error happened in the modul: %s\n in the line %d.",\
							                 __FILE__, __LINE__);\
						    strcat(pclText, "\nContinuing the operation can cause unwanted effects.\nResume??");\
						    sprintf(pclExcText, "File: %s  ==> Source-Line: %d", __FILE__, __LINE__);\
						    of_catch << pclExcText << endl;\
							CString olErr;\
							olErr.Format(pclText);\
							ogErrlog += olErr;\
							WriteInErrlogFile();\
						}
#else	// _DEBUG
#define  CCS_TRY
#define  CCS_CATCH_ALL
#endif	// #ifndef _DEBUG

// BC_TRY and BC_CATCH_ALL ///////////////////////////////////////////////
#ifndef _DEBUG
#define  BC_TRY try{	ogBackTrace = "";
#define  BC_CATCH_ALL }\
						catch(...)\
						{\
							struct BcStruct *prlBcData;\
							prlBcData = (struct BcStruct *) vpDataPointer;\
							if (prlBcData != NULL)\
							{\
								CString olMsg;\
								olMsg.Format(LoadStg(IDS_STRING1944),prlBcData->Cmd,prlBcData->Object,prlBcData->Seq,prlBcData->Tws,prlBcData->Twe,prlBcData->DdxType,prlBcData->Selection,prlBcData->Fields,prlBcData->Data,prlBcData->BcNum,ogBackTrace);\
								MessageBox(NULL,olMsg,"Fatal error",MB_OK);\
							}\
							else\
							{\
								MessageBox(NULL,"Unknown Broadcast exception occured","Fatal error",MB_OK);\
							}\
						}
#else	// _DEBUG
#define  BC_TRY
#define  BC_CATCH_ALL
#endif	// #ifndef _DEBUG


// Schicht, Regular Free und Free - Konstanten
#define CODE_UNDEFINED				0	// keine Ahnung
#define CODE_IS_ODA_REGULARFREE		1	// Code ist ODA, Flag ist gesetzt
#define CODE_IS_ODA_FREE			2	// <opCode> ist ODA-Datensatz, Flag ist NICHT gesetzt
#define CODE_IS_BSD					4	// keine Abwesenheit - BSD
#define CODE_IS_ODA_TIMEBASED		8	// TBSD-Feld ist gesetzt
#define CODE_IS_ODA_NOT_TIMEBASED	16	// TBSD-Feld ist nicht gesetzt
#define CODE_IS_ODA			(CODE_IS_ODA_REGULARFREE | CODE_IS_ODA_FREE)		// ein von ODA
#define CODE_IS_WIS					32

enum enumBitmapIndexes
{
	BREAK_RED_IDX = 21,
	BREAK_GRAY_IDX = 22,
	BREAK_GREEN_IDX = 23,
	BREAK_SILVER_IDX = 24
};

enum SimValues
{
	SIM_CONSTANT,
	SIM_PERCENT,
	SIM_ABSOLUTE
};

enum enumColorIndexes
{
	GRAY_IDX=2,GREEN_IDX,RED_IDX,BLUE_IDX,SILVER_IDX,MAROON_IDX,
	OLIVE_IDX,NAVY_IDX,PURPLE_IDX,TEAL_IDX,LIME_IDX,
	YELLOW_IDX,FUCHSIA_IDX,AQUA_IDX, WHITE_IDX,BLACK_IDX,ORANGE_IDX
};


//DDX-Types
enum 
{
	CLOSE_ALL_MODALS,
	BC_SGR_NEW, BC_SGR_DELETE, BC_SGR_CHANGE,	SGR_NEW, SGR_DELETE, SGR_CHANGE,
	BC_SGM_NEW, BC_SGM_DELETE, BC_SGM_CHANGE,	SGM_NEW, SGM_DELETE, SGM_CHANGE,
	BC_PER_CHANGE,BC_PER_NEW,BC_PER_DELETE,PER_NEW,PER_DELETE,PER_CHANGE,
	BC_PFC_CHANGE,BC_PFC_DELETE,BC_PFC_NEW,PFC_CHANGE,PFC_DELETE,PFC_NEW,
	BC_SDT_CHANGE,BC_SDT_DELETE,BC_SDT_NEW,SDT_CHANGE,SDT_SELCHANGE,SDT_DELETE,SDT_NEW,
	BC_MSD_CHANGE,BC_MSD_DELETE,BC_MSD_NEW,BC_RELMSD,MSD_CHANGE,MSD_SELCHANGE,MSD_DELETE,MSD_NEW,RELMSD,
	BC_DEL_CHANGE,BC_DEL_DELETE,BC_DEL_NEW,DEL_CHANGE,DEL_SELCHANGE,DEL_DELETE,DEL_NEW,
	BC_SDG_CHANGE,BC_SDG_DELETE,BC_SDG_NEW,BC_RELSDG,SDG_CHANGE,SDG_DELETE,SDG_NEW,RELSDG,
	BC_ODA_CHANGE,BC_ODA_DELETE,BC_ODA_NEW,ODA_CHANGE,ODA_DELETE,ODA_NEW,
	BC_ORG_CHANGE,BC_ORG_DELETE,BC_ORG_NEW,ORG_CHANGE,ORG_DELETE,ORG_NEW,
    BC_CFG_INSERT, BC_CFG_CHANGE, CFG_CHANGE, CFG_INSERT,CFG_DELETE,
	BC_SPE_CHANGE, BC_SPE_NEW, BC_SPE_DELETE, SPE_NEW, SPE_DELETE,SPE_CHANGE,
	BC_SCO_CHANGE, BC_SCO_NEW, BC_SCO_DELETE, SCO_NEW, SCO_DELETE,SCO_CHANGE,
	BC_SOR_CHANGE, BC_SOR_NEW, BC_SOR_DELETE, SOR_NEW, SOR_DELETE,SOR_CHANGE,
	BC_SPF_CHANGE, BC_SPF_NEW, BC_SPF_DELETE, SPF_NEW, SPF_DELETE,SPF_CHANGE,
	BC_STE_CHANGE, BC_STE_NEW, BC_STE_DELETE, STE_NEW, STE_DELETE,STE_CHANGE,
	BC_SWG_CHANGE, BC_SWG_NEW, BC_SWG_DELETE, SWG_NEW, SWG_DELETE,SWG_CHANGE,
	BC_SRE_CHANGE, BC_SRE_NEW, BC_SRE_DELETE, SRE_NEW, SRE_DELETE,SRE_CHANGE,
	BC_ACC_NEW,BC_ACC_DELETE,BC_ACC_CHANGE,ACC_NEW,ACC_DELETE,ACC_CHANGE,ACC_FIELD_CHANGE,
	BC_ENABLEACCSAVE,BC_DISABLEACCSAVE,BC_RELACC,RELACC,
	BC_PGP_NEW,BC_PGP_DELETE,BC_PGP_CHANGE,PGP_NEW,PGP_DELETE,PGP_CHANGE,PGP_FIELD_CHANGE,
	BC_WGP_NEW,BC_WGP_DELETE,BC_WGP_CHANGE,WGP_NEW,WGP_DELETE,WGP_CHANGE,WGP_FIELD_CHANGE,
	BC_DRW_CHANGE,BC_DRW_NEW,BC_DRW_DELETE,       
	DRW_CHANGE,DRW_NEW,DRW_DELETE,       
	BC_ADE_CHANGE,BC_ADE_NEW,BC_ADE_DELETE,       
	ADE_CHANGE,ADE_NEW,ADE_DELETE,       
	BC_DRR_CHANGE,BC_DRR_DELETE,BC_DRR_NEW,DRR_CHANGE,DRR_MULTI_CHANGE,
	DRR_DELETE,DRR_NEW,DRR_CHECK,
	BC_RELDRR,RELDRR,ENDRLR,BC_ENDRLR,SNGDRR,BC_RELOAD_SINGLE_DRR,
	MULDRR,BC_RELOAD_MULTI_DRR,STARTRLR,
	BC_SPR_CHANGE,BC_SPR_DELETE,BC_SPR_NEW,
	SPR_CHANGE,SPR_DELETE,SPR_NEW,
	BC_DRD_CHANGE,BC_DRD_DELETE,BC_DRD_NEW,DRD_CHANGE,DRD_DELETE,DRD_NEW,
	BC_DRA_CHANGE,BC_DRA_DELETE,BC_DRA_NEW,DRA_CHANGE,DRA_DELETE,DRA_NEW,
	BC_DRS_CHANGE,BC_DRS_DELETE,BC_DRS_NEW,DRS_CHANGE,DRS_DELETE,DRS_NEW,
	BC_DRG_CHANGE,BC_DRG_DELETE,BC_DRG_NEW,DRG_CHANGE,DRG_DELETE,DRG_NEW,
	BC_REL_CHANGE,BC_REL_DELETE,BC_REL_NEW,REL_CHANGE,REL_DELETE,REL_NEW,
	BC_OAC_CHANGE,BC_OAC_DELETE,BC_OAC_NEW,OAC_CHANGE,OAC_DELETE,OAC_NEW,
	BC_COT_CHANGE,BC_COT_DELETE,BC_COT_NEW,COT_CHANGE,COT_DELETE,COT_NEW,
	OFFDAY_CHANGE,
	BC_TEA_CHANGE,BC_TEA_NEW,BC_TEA_DELETE,	TEA_CHANGE,TEA_NEW,TEA_DELETE,
	BC_BSD_CHANGE,BC_BSD_NEW,BC_BSD_DELETE,	BSD_CHANGE,BSD_NEW,BSD_DELETE,
	BC_STF_CHANGE,BC_STF_DELETE,BC_STF_NEW,STF_CHANGE,STF_DELETE,STF_NEW,
	DROP_STAFF,
	BC_PAR_NEW,BC_PAR_CHANGE,BC_PAR_DELETE,
	BROADCAST_CHECK
};


// Konstanten f�r Kommunikation
#define MAX_WHERE_STAFFURNO_IN		120	// max. zul�ssige Anzahl f�r MA-Urnos in 
										// WHERE-Clause; 2K max. WHERE-Clause, um
										// auf der sicheren Seite zu stehen ->
										// pro MA-Urno ca. 13 Zeichen ('<Urno>',)
										// -> 120 Urnos insgesamt, Rest (von
										// den 2K) f�r sonstige Filterkriterien.

// Max. Anzahl von DRRs (DRRN)
#define MAX_DRR_DRRN				9	// max. Anzahl von DRRs pro MA/Tag/Planungsstufe

// Farben
#define MAXCOLORS 64
#define FIRSTCONFLICTCOLOR 31
// Symbolic colors (helper constants for CGateDiagramDialog -- testing purpose only)
#define BLACK		RGB(  0,   0,   0)
#define MAROON		RGB(128,   0,   0)  // dark red
#define GREEN		RGB(  0, 128,   0)  // dark green
#define DKGREEN		RGB(  0, 200,   0)  // dark green
#define OLIVE		RGB(128, 128,   0)  // dark yellow
#define NAVY		RGB(  0,   0, 128)  // dark blue
#define PURPLE		RGB(128,   0, 128)  // dark magenta
#define TEAL		RGB(  0, 128, 128)  // dark cyan
#define GRAY		RGB(128, 128, 128)  // dark gray
//PRF6932 #define SILVER		RGB(192, 192, 192)  // light gray
#define SILVER				 ::GetSysColor(COLOR_BTNFACE)
#define RED			RGB(255,   0,   0)
#define ORANGE		RGB(255, 135,   0)
#define LORANGE		RGB(255, 205,  70)
#define LIME		RGB(  0, 255,   0)  // green
#define YELLOW		RGB(255, 255,   0)
#define LTYELLOW	RGB(255, 255, 160)
#define BLUE		RGB(  0,   0, 255)
#define FUCHSIA		RGB(255,   0, 255)  // magenta
#define AQUA		RGB(  0, 255, 255)  // cyan
#define WHITE		RGB(255, 255, 255)
#define LTGRAY		RGB(170, 170, 170)
#define MEDIUMDGRAY	RGB(164, 164, 164)
#define LIGHTSILVER   RGB(235, 235, 235)  
#define LIGHTSILVER2  RGB(217, 217, 217)  
#define LIGHTSILVER3  RGB(205, 205, 205)  

extern COLORREF ogColors[];
extern CBrush *ogBrushs[];

extern COLORREF lgBkColor;
extern COLORREF lgTextColor;
extern COLORREF lgHilightColor;

////////////////////////////////////////////////////////////////////////////
// Brush & Pen Number Color Definitions
// for color numbers see CCSDynTable::CCSDynTable(CWnd* pParent /*=NULL*/, UINT ipID/*=0*/)

#define BPN_NOCOLOR				COLORREF(-1)		// = white by default
#define BPN_BLACK				0
#define BPN_WHITE				1
#define BPN_SILVER				2
#define BPN_LIGHTSILVER1		3
#define BPN_LIGHTSILVER2		4
#define BPN_SELECTBLUE			5
#define BPN_SILVERRED			6
#define BPN_SILVERGREEN			7
#define BPN_SILVERREDLIGHT		8
#define BPN_SILVERGREENLIGHT	9
#define BPN_RED					10
#define BPN_BLUE				11
#define BPN_SILVERREDLIGHT2		12
#define BPN_SILVERBLUELIGHT		13
#define BPN_SILVERORANGELIGHT	14
#define BPN_SILVERREDLIGHT3  	15
#define BPN_GREEN				16
#define BPN_LIGHTBLUE			17
#define BPN_YELLOW				18
	
extern enum enumRecordState egRecordState;

extern BOOL bgIsInitialized;

// CFont : translate from logical point to physical pixel
#define PT_TO_PIXELS(dc, pt)    (- MulDiv( pt, (dc).GetDeviceCaps( LOGPIXELSY ), 72 ))

#define WM_DYNTABLE_HSCROLL				(WM_USER + 360)
#define WM_DYNTABLE_VSCROLL				(WM_USER + 361)
#define WM_DYNTABLE_HMOVE				(WM_USER + 362)
#define WM_DYNTABLE_VMOVE				(WM_USER + 363)
#define WM_DYNTABLE_INLINE_UPDATE		(WM_USER + 364)
#define WM_DYNTABLE_RBUTTONDOWN			(WM_USER + 365)
#define WM_DYNTABLE_LBUTTONDOWN			(WM_USER + 366)
#define WM_DYNTABLE_RBUTTONUP			(WM_USER + 367)
#define WM_DYNTABLE_LBUTTONUP			(WM_USER + 368)
#define WM_DYNTABLE_LBUTTONDBLCLK		(WM_USER + 369)
#define WM_DYNTABLE_MOUSEMOVE			(WM_USER + 370)

#define WM_USER_FIND					(WM_USER + 804)
#define WM_USER_CHANGECODEDLG			(WM_USER + 805)

/////////////////////////////////////////////////////////////////////////////
// Size of field in data structures 
#define FCTC_LEN	(8)
#define BUFU_LEN	(15)
#define DRRN_LEN	(1)
#define EXPF_LEN	(1)
#define PRIO_LEN	(2)		// Priorit�t der Funktion, = 1 - Stammfunktion
#define REMA_LEN	(256) 	// Bemerkung
#define ROSL_LEN	(1) 	// Planungsstufe
#define ROSS_LEN	(1)	 	// Status der Planungsstufe ('L'=last/vorherige, 'A'=active/aktuelle, 'N'=next/n�chste)
#define SBLP_LEN	(4)	 	// Pausenl�nge bezahlt
#define SBLU_LEN	(4) 	// Pausenl�nge unbezahlt in Minuten, z.B. = "30"
#define SCOD_LEN	(8)	 	// Schichtcode (Bezeichner)
#define SCOO_LEN	(8) 	// urspr�ngliche BSD-Schichtcode (Bezeichner)
#define SDAY_LEN	(8) 	// Tages-Schl�ssel YYYYMMDD
#define DRSF_LEN	(1)	 	// gibt es einen DRS diesem DRR? 0=Nein, 1=Ja
#define USEC_LEN	(32) 	// Anwender (Ersteller)
#define USEU_LEN	(32) 	// Anwender (letzte �nderung)
#define BKDP_LEN	(1)	 	// 1. Pause bezahlt Ja = x
#define DRS1_LEN	(1) 	// Zusatzinfo zur Schicht / nach Schicht		ACHTUNG! bei �nderung DRA nicht vergessen
#define DRS2_LEN	(8)	 	// Prozent der Arbeitsunf�higkeit (z.B. 050Z)	ACHTUNG! bei �nderung DRA nicht vergessen
#define DRS3_LEN	(1)	 	// Zusatzinfo zur Schicht / vor Schicht			ACHTUNG! bei �nderung DRA nicht vergessen
#define DRS4_LEN	(1)	 	// Zusatzinfo zur Schicht / gesamt				ACHTUNG! bei �nderung DRA nicht vergessen
#define HOPO_LEN	(3) 	// Homeairport
#define WGPC_LEN	(5)		// WGPC - Arbeitsgruppencode aus WGP
#define SDAC_LEN	(8) 	// Abwesenheitscode (Code aus ODA)
#define PRFL_LEN	(1)		// Protocol flag ( '1' or ' ')

/////////////////////////////////////////////////////////////////////////////
// Logfile status defines 
#define	LOGFILE_OFF		0
#define	LOGFILE_TRACE	1
#define	LOGFILE_FULL	2


/////////////////////////////////////////////////////////////////////////////
// Font variable
extern CFont ogSmallFonts_Regular_6;
extern CFont ogSmallFonts_Regular_7;
extern CFont ogSmallFonts_Regular_8;
extern CFont ogSmallFonts_Bold_7;
extern CFont ogMSSansSerif_Regular_8;
extern CFont ogMSSansSerif_Italic_8;
extern CFont ogMSSansSerif_Bold_8;
extern CFont ogCourier_Bold_10;
extern CFont ogCourier_Regular_10;
extern CFont ogCourier_Regular_8;
extern CFont ogCourier_Bold_8;
extern CFont ogCourier_Regular_9;

extern CFont ogTimesNewRoman_9;
extern CFont ogTimesNewRoman_12;
extern CFont ogTimesNewRoman_16;
extern CFont ogTimesNewRoman_30;

extern CFont ogScalingFonts[30];

void InitFont();
void DeleteBrushes();
void CreateBrushes();

/////////////////////////////////////////////////////////////////////////////
// Color and brush variables

extern COLORREF ogColors[];
extern CBrush *ogBrushs[];

/////////////////////////////////////////////////////////////////////////////

struct TIMEFRAMEDATA
{
	CTime StartTime;
	CTime EndTime;
	TIMEFRAMEDATA(void)
	{StartTime=TIMENULL;EndTime=TIMENULL;}
};

/////////////////////////////////////////////////////////////////////////////
// application globals 
extern char pcgHome[4];
extern char pcgConfigPath[256];
extern char pcgStammdatenPath[256];
extern char pcgGroupingPath[256];
extern char pcgRosteringPrintPath[256];
extern char pcgPayrollRosteringPrintPath[256];
extern char pcgStaffBalancePath[256];
extern char pcgInitAccountPath[256];
extern char pcgAbsencePlanningPath[256];
extern char pcgDefaultNameConfig_Duty[256];
extern char pcgDefaultNameConfig_Shift[256];

extern char pcgLogFilePath[256];
extern char pcgErrlogFilePath[256];

// logfile section
extern long lgLogFileStatus;		// one of LOGFILE_OFF = 0, LOGFILE_TRACE = 1, LOGFILE_FULL = 2
extern CString ogRosteringLogText;	// actual temporary text for the logfile
extern CString ogErrlog;			// Error logging for user, corrupt data etc.

extern char pcgHelpPath[1024];
extern char pcgHome4[5];
extern char pcgTableExt[10];
extern bool bgNoScroll;
extern bool bgOnline;
extern bool bgEnableMonthlyAllowance;
extern bool bgEnableScenario;
extern bool bgScrollToNewShiftLine;
extern bool bgPrintShiftrosterShiftcode;
extern bool bgPrintDutyrosterShiftcode;
extern CTime ogLoginTime;
class CInitialLoadDlg;
extern CInitialLoadDlg *pogInitialLoad;

enum
{
	IDSAVE	= IDCANCEL + 1,
	IDAPPLY			// IDSAVE + 1
};

#define SetWndStatAll(clStat, plWnd)\
	if(clStat=='1') {plWnd.ShowWindow(SW_SHOW);plWnd.EnableWindow(TRUE);}\
	else if(clStat=='0') {plWnd.SetCheck(0);plWnd.ShowWindow(SW_SHOW);plWnd.EnableWindow(FALSE);}\
	else if(clStat=='-') {plWnd.SetCheck(0);plWnd.ShowWindow(SW_HIDE);};

/////////////////////////////////////////////////////////////////////////////
// Drag Information Type

enum 
{
	DIT_SHIFTROSTER_BSD_TABLE,			// Drag from Shiftroster BSD Table
	DIT_DUTYROSTER_BSD_TABLE,			// Drag from Dutyroster BSD Table
	DIT_FROMGROUP_TODUTYROSTER,			// Drag from Group Table to Dutyroster
	DIT_FROMDUTYROSTER_TOGROUP,			// Drag from Dutyroster to Group Table
	DIT_SHIFTROSTER_STAFF_TABLE,		// Drag from Shiftroster Staff Table to GSP-table
	DIT_SHIFTROSTER_NSR_TABLE			// Drag from Shiftroster NettoShiftRequirement to GSP-table
};

// Sortierungs ENUM
enum
{
	SRT_NOTSORT,
	SRT_UP,
	SRT_DOWN,
	SRT_SNAME_U,
	SRT_LNAME_U,
	SRT_FUNC_U,
	SRT_WGRP_U,
	SRT_CPOOL_U,
	SRT_PNUM_U,
	SRT_BEGINN_U,
	SRT_END_U,
	SRT_SNAME_D,
	SRT_LNAME_D,
	SRT_FUNC_D,
	SRT_WGRP_D,
	SRT_CPOOL_D,
	SRT_PNUM_D,
	SRT_BEGINN_D,
	SRT_END_D,
	SRT_GRUPP_U,
	SRT_MEMBERS_U,
	SRT_GRUPP_D,
	SRT_MEMBERS_D,
	SRT_NEW,
	SRT_CHANGE,
	SRT_OK,
	SRT_NOTOK,
	SRT_NOTGENERATE,
	SRT_ISGENERATED,
	SRT_ISRELEASED
};


enum
{
	DLG_NEW,
	DLG_COPY,
	DLG_CHANGE
};

enum
{
	UNKNOWN_VIEW,
	SHIFT_VIEW,
	DUTY_VIEW
};

// Ado: OFF Day Berechnung
struct OffDayData			
{
	int imValue;				
	COleDateTime omDate;
};

#define TIMER_REFRESH 0x2

bool RemoveWaitCursor(bool bpRet=false);
int GetAbsDay(COleDateTime& opDateTime);

// Tagesdialoge
#define		BASIC_SHIFT		1
#define		IS_SHIFT		2
#define		BSD_DATA		4
#define		ODA_DATA		8
#define		DRR_DATA		16
#define		LONGTIME_SHIFT	32
#define		AZE_DATA		64


// end globals
/////////////////////////////////////////////////////////////////////////////


#endif //__CCSGLOBL_H__
#endif // !defined(AFX_CCSGLOBL_H__55D90F34_191F_11D1_82BE_0080AD1DC701__INCLUDED_)
