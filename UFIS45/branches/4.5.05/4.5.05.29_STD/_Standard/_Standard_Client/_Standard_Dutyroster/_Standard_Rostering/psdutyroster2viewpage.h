#ifndef AFX_PSDUTYROSTER2VIEWPAGE_H__DF496401_6D7D_11D2_8054_004095434A85__INCLUDED_
#define AFX_PSDUTYROSTER2VIEWPAGE_H__DF496401_6D7D_11D2_8054_004095434A85__INCLUDED_

// PSDutyRoster2ViewPage.h : Header-Datei
//
#include <Ansicht.h>
#include <RosterViewPage.h>
#include <DutyRoster_View.h>

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld PSDutyRoster2ViewPage 

class PSDutyRoster2ViewPage : public RosterViewPage
{
	DECLARE_DYNCREATE(PSDutyRoster2ViewPage)

// Konstruktion
public:
	PSDutyRoster2ViewPage();
	~PSDutyRoster2ViewPage();

// Dialogfelddaten
	//{{AFX_DATA(PSDutyRoster2ViewPage)
	enum { IDD = IDD_PSDUTYROSTER2 };
	CStatic	m_SW;
	CStatic	m_SU;
	CStatic	m_SL;
	CStatic	m_S5;
	CStatic	m_S4;
	CStatic	m_S3;
	CStatic	m_S2;
	CStatic	m_S1;
	CButton	m_MAAll;
	CButton	m_MAPresent;
	CButton	m_MANotPresent;
	CButton	m_PlanW;
	CButton	m_PlanU;
	CButton	m_PlanL;
	CButton	m_Plan5;
	CButton	m_Plan4;
	CButton	m_Plan3;
	CButton	m_Plan2;
	CButton	m_Plan1;
	//}}AFX_DATA

	BOOL GetData();
	BOOL GetData(CStringArray &opValues);
	void SetData();
	void SetData(CStringArray &opValues);

// Überschreibungen
	// Der Klassen-Assistent generiert virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(PSDutyRoster2ViewPage)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	//}}AFX_VIRTUAL

// Implementierung
protected:
	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(PSDutyRoster2ViewPage)
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
	CString omCalledFrom;
	void SetCalledFrom(CString opCalledFrom);
private:
	CString omTitleStrg;
	void SetStatic();
	void SetButtonText();
	// Oberfläschen Elemente setzen nach Partab Einträgen
	void ShowSteps();

	// Anzeige der Planungsstufen
	bool bmShow1,bmShow2,bmShow3,bmShow4,bmShow5,bmShowU,bmShowL,bmShowW;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio fügt zusätzliche Deklarationen unmittelbar vor der vorhergehenden Zeile ein.

#endif // AFX_PSDUTYROSTER2VIEWPAGE_H__DF496401_6D7D_11D2_8054_004095434A85__INCLUDED_
