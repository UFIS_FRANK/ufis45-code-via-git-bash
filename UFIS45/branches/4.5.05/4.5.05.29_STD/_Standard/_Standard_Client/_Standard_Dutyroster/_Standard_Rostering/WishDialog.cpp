// WishDialog.cpp: Implementierungsdatei
//

#include <stdafx.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CWishDialog 


CWishDialog::CWishDialog(CWnd* pParent ,CString olUrno,long lpStfuUrno, CString opSday,bool bpReadOnly)
	: CDialog(CWishDialog::IDD, pParent)
{
	//{{AFX_DATA_INIT(CWishDialog)
	m_BIsLocked = FALSE;
	m_csRems = _T("");
	m_csReme = _T("");
	m_csRemi = _T("");
	m_csWisc = _T("");
	//}}AFX_DATA_INIT

	pomParent = (DutyRoster_View*)pParent;
	omUrno = olUrno;
	lmStfuUrno = lpStfuUrno;

	omSday = opSday;
	
	//uhi 19.4.01
	COleDateTime opTime;
	CedaDataHelper::DateStringToOleDateTime(omSday,opTime);
	omDpt1 = ogSorData.GetOrgBySurnWithTime(lmStfuUrno,opTime);
	
	bmReadOnly = bpReadOnly;

	// Soll ein neuer Datensatz angelegt werden ?
	if (olUrno == "")
		bmMakeNew = true;
	else
		bmMakeNew = false;
}


void CWishDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CWishDialog)
	DDX_Check(pDX, IDC_CHECK1, m_BIsLocked);
	DDX_Text(pDX, IDC_REMA2, m_csRems);
	DDX_Text(pDX, IDC_REMA3, m_csReme);
	DDX_Text(pDX, IDC_REMA4, m_csRemi);
	DDX_Text(pDX, IDC_WISC, m_csWisc);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CWishDialog, CDialog)
	//{{AFX_MSG_MAP(CWishDialog)
	ON_BN_CLICKED(IDC_RADIOP1, OnRadiop1)
	ON_BN_CLICKED(IDC_RADIOP2, OnRadiop2)
	ON_BN_CLICKED(IDC_RADIOP3, OnRadiop3)
	ON_BN_CLICKED(IDC_RADIOPN, OnRadiopn)
	ON_BN_CLICKED(IDC_DELETE, OnDelete)
	ON_BN_CLICKED(IDC_EDITCODE, OnEditcode)
	ON_EN_KILLFOCUS(IDC_WISC, OnKillfocusWisc)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten CWishDialog 

BOOL CWishDialog::OnInitDialog() 
{
	CDialog::OnInitDialog();

	// Static und Buttontexte setzen
	SetText();

	// Mitarbeiter
	STFDATA* olStfData;
	// Mitarbeiter finden
	CString olName;
	olStfData = ogStfData.GetStfByUrno(lmStfuUrno);
	if (olStfData != NULL)
	{
		// Vorname und Nachname
		olName = CBasicData::GetFormatedEmployeeName(pomParent->rmViewInfo.iEName, olStfData, "", "");
		SetDlgItemText(IDC_CDAT_D2,olName);
		SetDlgItemText(IDC_CDAT_D3,olStfData->Peno);
	}

	if (!bmMakeNew)
	{
		// Daten einlesen
		RecordSet olRecord(ogBCD.GetFieldCount("DRW"));
		ogBCD.GetRecord("DRW","URNO",omUrno,olRecord);

		// Statics setzen
		SetDlgItemText(IDC_USEC,olRecord.Values[ogBCD.GetFieldIndex("DRW","USEC")]);
		SetDlgItemText(IDC_USEU,olRecord.Values[ogBCD.GetFieldIndex("DRW","USEU")]);

		CTime olTime; 
		olTime = DBStringToDateTime(olRecord.Values[ogBCD.GetFieldIndex("DRW","CDAT")]);
		SetDlgItemText(IDC_CDAT_T,olTime.Format("%H:%M"));
		SetDlgItemText(IDC_CDAT_D,olTime.Format("%d.%m.%Y"));
	
		olTime = DBStringToDateTime(olRecord.Values[ogBCD.GetFieldIndex("DRW","LSTU")]);
		SetDlgItemText(IDC_LSTU_D,olTime.Format("%d.%m.%Y"));
		SetDlgItemText(IDC_LSTU_T,olTime.Format("%H:%M"));

		// Bemerkungen
		m_csRems = olRecord.Values[ogBCD.GetFieldIndex("DRW","REMS")];
		m_csReme = olRecord.Values[ogBCD.GetFieldIndex("DRW","REME")];
		m_csRemi = olRecord.Values[ogBCD.GetFieldIndex("DRW","REMI")];
	
		// Mitarbeiter Urno
		CString olStfu = olRecord.Values[ogBCD.GetFieldIndex("DRW","STFU")];

		// Wunschcode
		m_csWisc = olRecord.Values[ogBCD.GetFieldIndex("DRW","WISC")];
		m_StartWisc = m_csWisc;

		// Gesperrt
		if (olRecord.Values[ogBCD.GetFieldIndex("DRW","CLOS")] == "B")
			m_BIsLocked = true;
		else
			m_BIsLocked = false;

		// Priorit�t
		SetRadioButtons(olRecord.Values[ogBCD.GetFieldIndex("DRW","PRIO")]);
	}
	else
	{
		OnRadiopn();
	}

	// Testen ob readonly Aufruf
	if (bmReadOnly)
		DisableControls() ;

	UpdateData(FALSE);
	return TRUE;  
}

//************************************************************************************
// Setzt die Radiobuttons
//************************************************************************************

void CWishDialog::SetRadioButtons(CString opPrio)
{
	// Alles RB l�schen
	((CButton*) GetDlgItem(IDC_RADIOPN))->SetCheck(0);
	((CButton*) GetDlgItem(IDC_RADIOP3))->SetCheck(0);	
	((CButton*) GetDlgItem(IDC_RADIOP2))->SetCheck(0);	
	((CButton*) GetDlgItem(IDC_RADIOP1))->SetCheck(0);			

	if (opPrio == "1")
		OnRadiop1();
	if (opPrio == "2")
		OnRadiop2();
	if (opPrio == "3")
		OnRadiop3();
	// Keine Priorit�t
	if (opPrio == "" || opPrio == " " || opPrio == "0")
		OnRadiopn();
}

//************************************************************************************
//
//************************************************************************************

void CWishDialog::OnRadiop1() 
{
	((CButton*) GetDlgItem(IDC_RADIOP1))->SetCheck(1);			
}

//************************************************************************************
//
//************************************************************************************

void CWishDialog::OnRadiop2() 
{
	((CButton*) GetDlgItem(IDC_RADIOP2))->SetCheck(1);		
}

//************************************************************************************
//
//************************************************************************************

void CWishDialog::OnRadiop3() 
{
	((CButton*) GetDlgItem(IDC_RADIOP3))->SetCheck(1);	
}

//************************************************************************************
//
//************************************************************************************

void CWishDialog::OnRadiopn() 
{
	((CButton*) GetDlgItem(IDC_RADIOPN))->SetCheck(1);
}

//************************************************************************************
// Mitarbeiter eingaben l�schen
//************************************************************************************

void CWishDialog::OnDelete() 
{
	// Alles RB l�schen
	((CButton*) GetDlgItem(IDC_RADIOPN))->SetCheck(0);
	((CButton*) GetDlgItem(IDC_RADIOP3))->SetCheck(0);	
	((CButton*) GetDlgItem(IDC_RADIOP2))->SetCheck(0);	
	((CButton*) GetDlgItem(IDC_RADIOP1))->SetCheck(0);			
	// keine Priorit�t
	OnRadiopn();
	// Bemerkungen
	SetDlgItemText(IDC_REMA2,"");
}

//************************************************************************************
// Wunschcode ver�ndern
//************************************************************************************

void CWishDialog::OnEditcode() 
{
CCS_TRY;
	CStringArray olCol1,olCol2,olCol3;
	CString olUrno;
	CCSPtrArray<WISDATA> olWisData;
	
	//uhi 19.4.01
	//CString olWisUrnos = ogWisData.GetWisByOrg(omDpt1, &olWisData); - olWisUrnos ist umsonst
	
	int ilWisSize = ogWisData.GetWisArrayByOrg(omDpt1, &olWisData);

	for(int i=0; i<ilWisSize; i++)
	{
		olUrno.Format("%ld", olWisData[i].Urno);
		olCol1.Add(CString(olWisData[i].Wisc));
		olCol2.Add(CString(olWisData[i].Wisd));
		olCol3.Add(olUrno);
	}
	
	/*for(int i = 0; i < ogWisData.omData.GetSize(); i++)
	{
		olUrno.Format("%ld", ogWisData.omData[i].Urno);
		olCol1.Add(CString(ogWisData.omData[i].Wisc));
		olCol2.Add(CString(ogWisData.omData[i].Wisd));
		olCol3.Add(olUrno);
	}*/
	
	CCommonGridDlg olDlg(LoadStg(IDS_STRING1838),&olCol1, &olCol2, &olCol3, LoadStg(IDS_STRING1839), LoadStg(IDS_STRING1840), this);
	if(olDlg.DoModal() == IDOK)
	{
		WISDATA *prlWis = ogWisData.GetWisByUrno(atol(olDlg.GetReturnUrno()));
		if(prlWis != NULL)
		{
			// Wunschcode
			SetDlgItemText(IDC_BSDC,prlWis->Wisc);
		}
	}
CCS_CATCH_ALL;
}

//************************************************************************************
// Daten ver�ndern und abspeichern.
//************************************************************************************

void CWishDialog::OnOK() 
{
	bool		blResult;
	CString		olBuffer;

	UpdateData(true);
	
	//Pr�fen ob ein g�ltiger Wunsch-Code eingegeben wurde
	if(m_csWisc.IsEmpty() || ogWisData.GetWisByKey(m_csWisc, omDpt1) == NULL)
	{
		// -> ung�ltig
		OnCancel();
		return;
	}

	RecordSet olRecord(ogBCD.GetFieldCount("DRW"));

	// Sollte der Datensatz ver�ndert werden, erst die alten Daten holen
	if (!bmMakeNew)
	{
		// Datensatz erhalten
		ogBCD.GetRecord("DRW","URNO",omUrno,olRecord);
	}
	else
	{
		CString olStfuUrno;
		olStfuUrno.Format("%ld",lmStfuUrno);

		// SDAY und STFU setzen
		olRecord.Values[ogBCD.GetFieldIndex("DRW","STFU")] = olStfuUrno;
		olRecord.Values[ogBCD.GetFieldIndex("DRW","SDAY")] = omSday;
	}
	
	UpdateData(true);

	// Bemerkungen (ACHTUNG!!! Alle Kommata in Strings m�ssen eleminiert
	// werden, sonst gibt es einen Crash in der UfisClass-Lib: 
	// ExtractItemList() benutzt per Default das Komma als Trennzeichen.)
	m_csRems.Replace(","," ");
	m_csReme.Replace(","," ");
	m_csRemi.Replace(","," ");
	olRecord.Values[ogBCD.GetFieldIndex("DRW","REMS")] = m_csRems;
	olRecord.Values[ogBCD.GetFieldIndex("DRW","REME")] = m_csReme;
	olRecord.Values[ogBCD.GetFieldIndex("DRW","REMI")] = m_csRemi;

	// Wunsch Code
	olRecord.Values[ogBCD.GetFieldIndex("DRW","WISC")] = m_csWisc;

	// Datensatz gesperrt
	if (m_BIsLocked)
		olRecord.Values[ogBCD.GetFieldIndex("DRW","CLOS")] = "B";
	else
		olRecord.Values[ogBCD.GetFieldIndex("DRW","CLOS")] = " ";

	// Priorit�t
	 if (((CButton*) GetDlgItem(IDC_RADIOP1))->GetCheck() == 1)	
		 olRecord.Values[ogBCD.GetFieldIndex("DRW","PRIO")] = "1";
	 if (((CButton*) GetDlgItem(IDC_RADIOP2))->GetCheck() == 1)			
		 olRecord.Values[ogBCD.GetFieldIndex("DRW","PRIO")] = "2";
	 if (((CButton*) GetDlgItem(IDC_RADIOP3))->GetCheck() == 1)			
		 olRecord.Values[ogBCD.GetFieldIndex("DRW","PRIO")] = "3";
	 if (((CButton*) GetDlgItem(IDC_RADIOPN))->GetCheck() == 1)			
		 olRecord.Values[ogBCD.GetFieldIndex("DRW","PRIO")] = "";

	 // Abh�ngig, ob ein DRW Datensatz neu erzeugt werden soll, oder ob
	 // er ver�ndert werden soll.
	if (!bmMakeNew)
	{
		// Ersetzen
		blResult = ogBCD.SetRecord("DRW","URNO",omUrno,olRecord.Values,true);
	}
	else
	{
		// Anh�ngen
		blResult = ogBCD.InsertRecord("DRW",olRecord,true);
	}
	
	// Resultat pr�fen
	if(!blResult)
	{
		CString olErrorTxt;
		//olErrorTxt.Format("%s %d\n%s",LoadStg(ST_UPDATEERR), ogDrwData.imLastReturnCode, ogDrwData.omLastErrorMessage);
		::MessageBox(NULL, olErrorTxt,LoadStg(ST_FEHLER),MB_ICONEXCLAMATION);
		// Dialog mit Cancel beenden
		OnCancel();
	}	
	
	CDialog::OnOK();
}

//********************************************************************************
// Testen ob ein g�ltiger "Wunschcode" eingegeben wurde
//********************************************************************************

void CWishDialog::OnKillfocusWisc() 
{
	UpdateData(true);

	if (m_csWisc != "")
	{

		//Pr�fen ob ein g�ltiger Wunsch-Code eingegeben wurde
		if(!ogWisData.GetWisByKey(m_csWisc, omDpt1))
		{
			// Auf Anfangswert zur�cksetzen
			m_csWisc = m_StartWisc;
			UpdateData(false);
		}
	}
}

//********************************************************************************
// Static und Buttuntexte setzen
//********************************************************************************

void CWishDialog::SetText() 
{
	// Caption
	CString olWText;
	CString olDay;

	if(omSday.GetLength() >= 8)
		olDay = omSday.Mid(6,2) + CString(".") + omSday.Mid(4,2) + CString(".") + omSday.Mid(0,4);

	olWText = CString("  ") + olDay + CString("   ") + LoadStg(IDS_STRING1822);
	SetWindowText(olWText);

	SetDlgItemText(IDC_1,LoadStg(IDS_STRING1823));
	SetDlgItemText(IDC_2,LoadStg(IDS_STRING1824));
	SetDlgItemText(IDC_3,LoadStg(IDS_STRING1825));
	SetDlgItemText(IDC_4,LoadStg(IDS_STRING1826));
	SetDlgItemText(IDC_5,LoadStg(IDS_STRING1827));
	SetDlgItemText(IDC_6,LoadStg(IDS_STRING60)+CString(":"));
	SetDlgItemText(IDC_7,LoadStg(SHIFT_STF_PENO)+CString(":"));
	SetDlgItemText(IDC_8,LoadStg(IDS_STRING1830));
	SetDlgItemText(IDC_9,LoadStg(IDS_STRING1831));
	SetDlgItemText(IDC_10,LoadStg(IDS_STRING1832));
	SetDlgItemText(IDC_11,LoadStg(IDS_STRING1833));
	SetDlgItemText(IDC_12,LoadStg(IDS_STRING1831));
	SetDlgItemText(IDC_13,LoadStg(IDS_STRING1834));
	SetDlgItemText(IDC_14,LoadStg(IDS_STRING1831));
	SetDlgItemText(IDC_15,LoadStg(IDS_STRING1835));
	SetDlgItemText(IDC_CHECK1,LoadStg(IDS_STRING1836));
	SetDlgItemText(IDC_DELETE,LoadStg(IDS_STRING1837));

	(GetDlgItem(IDOK))->SetWindowText(LoadStg(ID_OK));
	(GetDlgItem(IDCANCEL))->SetWindowText(LoadStg(ID_CANCEL));
}

//********************************************************************************
// Elemente auf "grau" schalten
//********************************************************************************

void CWishDialog::DisableControls() 
{
	((CButton*) GetDlgItem(IDC_RADIOPN))->EnableWindow(false);
	((CButton*) GetDlgItem(IDC_RADIOP3))->EnableWindow(false);
	((CButton*) GetDlgItem(IDC_RADIOP2))->EnableWindow(false);
	((CButton*) GetDlgItem(IDC_RADIOP1))->EnableWindow(false);

	((CButton*) GetDlgItem(IDC_EDITCODE))->EnableWindow(false);
	((CButton*) GetDlgItem(IDC_CHECK1))->EnableWindow(false);
	//((CButton*) GetDlgItem(IDC_DELETE))->EnableWindow(false);
	((CButton*) GetDlgItem(IDOK))->EnableWindow(false);

	((CEdit*) GetDlgItem(IDC_WISC))->SetReadOnly(true);
	((CEdit*) GetDlgItem(IDC_REMA2))->SetReadOnly(true);
	((CEdit*) GetDlgItem(IDC_REMA3))->SetReadOnly(true);
	((CEdit*) GetDlgItem(IDC_REMA4))->SetReadOnly(true);
}
