#if !defined(AFX_ROSTERVIEWPAGE_H__371698F2_F596_11D3_ABCE_00E0981D21D7__INCLUDED_)
#define AFX_ROSTERVIEWPAGE_H__371698F2_F596_11D3_ABCE_00E0981D21D7__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// RosterViewPage.h : header file
//

// Die Klasse RosterViewPage ist eine Basisklasse f�r PSDutyRosterXXXViewPage-Klassen
/////////////////////////////////////////////////////////////////////////////
// RosterViewPage dialog

class RosterViewPage : public CPropertyPage
{
	DECLARE_DYNCREATE(RosterViewPage)

// Construction
public:
	RosterViewPage();
	RosterViewPage(UINT nIDTemplate, UINT nIDCaption = 0);
	RosterViewPage(LPCTSTR lpszTemplateName, UINT nIDCaption = 0);
	~RosterViewPage();

	// Dialog Data
	//{{AFX_DATA(RosterViewPage)
	enum { IDD = IDD_BSD_VIEW };
		// NOTE - ClassWizard will add data members here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_DATA


	// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(RosterViewPage)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

protected:
	bool GetSafeStringFromArray(CStringArray &opValues, int ipIndex, CString &opStrParam);

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(RosterViewPage)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
	// Friends
protected:
	friend class DutyRosterPropertySheet;
	friend class ShiftRosterPropertySheet;
	// Daten
protected:
	// Puffer f�r eingestellte Parameter
	CStringArray omValues;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ROSTERVIEWPAGE_H__371698F2_F596_11D3_ABCE_00E0981D21D7__INCLUDED_)
