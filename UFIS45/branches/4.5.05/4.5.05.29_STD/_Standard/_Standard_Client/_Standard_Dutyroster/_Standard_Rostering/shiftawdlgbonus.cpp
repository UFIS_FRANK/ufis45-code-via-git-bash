// ShiftAWDlgBonus.cpp: Implementierungsdatei
//

#include <stdafx.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld ShiftAWDlgBonus 
//---------------------------------------------------------------------------

ShiftAWDlgBonus::ShiftAWDlgBonus(CString *popUrnoList, CString opOtherSelectUrnoList, UINT ipAWType,bool bpSelect,CString opVpfr,CString opVpto): CDialog(ShiftAWDlgBonus::IDD/*, pParent*/)
{
	//{{AFX_DATA_INIT(ShiftAWDlgBonus)
	//}}AFX_DATA_INIT
	//pomParent = (ShiftRoster_View*)pParent;
	bmSelect = bpSelect;
	pomUrnoList = popUrnoList;
	imAWType = ipAWType;
	omOtherSelectUrnoList = opOtherSelectUrnoList;
	pgrid = NULL;
}

ShiftAWDlgBonus::~ShiftAWDlgBonus(void)
{
	if (pgrid != NULL)
		delete pgrid;

	omDboLines.DeleteAll();
}


void ShiftAWDlgBonus::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(ShiftAWDlgBonus)
	DDX_Control(pDX, IDC_CHECK_SHOWALL, m_Check_ShowAll);
	DDX_Control(pDX, ID_INFO, m_B_Info);
	DDX_Control(pDX, IDOK,				m_B_Ok);
	DDX_Control(pDX, IDCANCEL,			m_B_Cancel);
	DDX_Control(pDX, IDC_B_DELETEALL,	m_B_DeleteAll);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(ShiftAWDlgBonus, CDialog)
	//{{AFX_MSG_MAP(ShiftAWDlgBonus)
	ON_BN_CLICKED(IDC_B_DELETEALL,	OnBDeleteall)
	ON_BN_CLICKED(ID_INFO, OnInfo)
	ON_MESSAGE(WM_GRID_LBUTTONDOWN, OnGridLButton)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

#define SHIFTAWD_MINROWS 1 //26

/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten ShiftAWDlgBonus 
//---------------------------------------------------------------------------

BOOL ShiftAWDlgBonus::OnInitDialog() 
{
	CDialog::OnInitDialog();

	CString olLine, olTmpTxt;

	if (bmSelect == false)
	{
		m_B_Ok.EnableWindow(false);
		m_B_DeleteAll.EnableWindow(false);
		m_Check_ShowAll.EnableWindow(false);
	}

	m_B_Ok.SetWindowText(LoadStg(SHIFT_STF_B_OK));
	m_B_Cancel.SetWindowText(LoadStg(SHIFT_STF_B_CANCEL));
	m_B_DeleteAll.SetWindowText(LoadStg(SHIFT_STF_B_DELETEALL));
	m_Check_ShowAll.SetWindowText(LoadStg(IDS_SHOW_ALL));

	SetDlgItemText(ID_INFO,LoadStg(SHIFT_B_INFO));

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
		pSysMenu->DeleteMenu(SC_CLOSE, MF_BYCOMMAND);

	// BONUS 
	if(imAWType == BONUS)
	{
		m_Check_ShowAll.ShowWindow(SW_HIDE);
		m_B_Info.ShowWindow(SW_HIDE);

		SetWindowText(LoadStg(SHIFT_SHEET_C_BONUS));

// was soll der shit, die gesuchten daten stehen nach der instanziierung in pomUrnoList !!!!
		char clTabn[100];
		CString olSgrUrnos;
		sprintf(clTabn,"BSD");
		for(int i = ogSgrData.omData.GetSize()-1; i >= 0; i--)
		{
			CString clTabn2(ogSgrData.omData[i].Tabn);
			if(strcmp(clTabn2,clTabn) == 0)
			{
				AWDBOVIEWDATA_SHFT_BONUS *prlDboView = new AWDBOVIEWDATA_SHFT_BONUS;

				prlDboView->Grpn = ogSgrData.omData[i].Grpn;
				prlDboView->Urno.Format("%ld",ogSgrData.omData[i].Urno);
				omDboLines.Add(prlDboView);
				olTmpTxt.Format("%ld",ogSgrData.omData[i].Urno);
				olSgrUrnos += olTmpTxt + "|";
			}
		}
		CString olUsgr;
		for(i = ogSgmData.omData.GetSize()-1; i >= 0; i--)
		{
			olUsgr.Format("%ld",ogSgmData.omData[i].Usgr);
			if(olSgrUrnos.Find(olUsgr) != -1)
			{
				BSDDATA *prlBsd = ogBsdData.GetBsdByUrno(atol(ogSgmData.omData[i].Uval));
				if(prlBsd != NULL)
				{
					for(int k = omDboLines.GetSize()-1; k >= 0; k--)
					{
						if(omDboLines[k].Urno == olUsgr)
						{
							olTmpTxt = prlBsd->Bsdc;
							omDboLines[k].Valu += (CString)prlBsd->Bsdc + ",";
						}
					}
				}
			}
		}
		
		
		for(i=0;i<omDboLines.GetSize(); i++)
		{
			omDboLines[i].Valu = SortItemList(omDboLines[i].Valu.Left(omDboLines[i].Valu.GetLength()-1),',');
		}

		//Grid initialisieren
		long ilCount = omDboLines.GetSize();
		ilCount = ilCount + 1;
		pgrid = new CGridControl(this, IDC_AW_GRID, 2, __max(SHIFTAWD_MINROWS, ilCount));
		IniBonusGrid();
	}
	return TRUE; 
}

//---------------------------------------------------------------------------

void ShiftAWDlgBonus::OnOK() 
{
	pomUrnoList->Empty();
	ROWCOL ilMaxItems, ilSelRow;
	CRowColArray awRows;
	CGXStyle style;
	LONG ilUrno;

	ilMaxItems = pgrid->GetSelectedRows(awRows, true, false);

	if (ilMaxItems<90)
	{
		CString olTmpTxt,olUrno;
		for(ROWCOL i=0; i<ilMaxItems; i++)
		{

			ilSelRow = awRows[i];
			pgrid->ComposeStyleRowCol(ilSelRow, 1, &style );
			//Urno erhalten
			ilUrno = (long)style.GetItemDataPtr();
			if (ilUrno){
				olUrno.Format("%d", ilUrno);
				if(omOtherSelectUrnoList.Find(olUrno) == -1)
				{
					*pomUrnoList += olUrno;
				}
			}
			if(i < ilMaxItems-1)
			{
				*pomUrnoList += CString("|");
			}

		}
		CDialog::OnOK();
	}
	else
	{
		CString olTxt;
		olTxt.Format(LoadStg(IDS_STRING1773), 90);
		Beep(440,70);
		MessageBox(olTxt, LoadStg(ST_FEHLER), MB_ICONEXCLAMATION);
	}
}

//---------------------------------------------------------------------------

void ShiftAWDlgBonus::OnBDeleteall() 
{
	pgrid->SelectRange(CGXRange().SetTable(), false, true);
}

//---------------------------------------------------------------------------

void ShiftAWDlgBonus::OnInfo() 
{
	MessageBox(LoadStg(IDS_STRING1794), LoadStg(IDS_INFO), NULL);
}


void ShiftAWDlgBonus::IniBonusGrid()
{
	CString olLine,olTmpTxt,olUrno;
	int ilCount, ilUrno;
	CGXStyle style;

	if(imAWType == BONUS)	//+ BONUS 
	{

		//�berschriften setzten
		pgrid->SetValueRange(CGXRange(0,1), LoadStg(SHIFT_DBO_GRUPP));
		pgrid->SetValueRange(CGXRange(0,2), LoadStg(SHIFT_DBO_MEMBERS));

		//Spaltenbreite und Zeilenh�he k�nnen nicht ver�ndert werden
		pgrid->GetParam()->EnableTrackColWidth(FALSE);
		pgrid->GetParam()->EnableTrackRowHeight(FALSE);
		pgrid->GetParam()->EnableSelection(GX_SELROW | GX_SELMULTIPLE | GX_SELSHIFT);

		//Scrollbars anzeigen wenn n�tig
		pgrid->SetScrollBarMode(SB_VERT, gxnAutomatic, TRUE);

		pgrid->SetStyleRange( CGXRange().SetTable(), CGXStyle()
				.SetVerticalAlignment(DT_VCENTER)
				.SetReadOnly(TRUE));

		//Keine Zeilennummerierung
		pgrid->SetColWidth(0, 0, pgrid->Width_LPtoDP(4 * GX_NXAVGWIDTH));
		pgrid->SetColWidth(1, 1, pgrid->Width_LPtoDP(20 * GX_NXAVGWIDTH));
		pgrid->SetColWidth(2, 2, pgrid->Width_LPtoDP(116 * GX_NXAVGWIDTH));
		pgrid->EnableAutoGrow(false);
		pgrid->EnableSorting(true);

		// Grid mit Daten f�llen.
		pgrid->GetParam()->SetLockReadOnly(false);

		//Alle Schichten
		ilUrno = 4711;
		pgrid->SetStyleRange (CGXRange(1,1), CGXStyle().SetItemDataPtr( (void*)ilUrno ) );
		pgrid->SetValueRange(CGXRange(1,0), CString(""));
		pgrid->SetValueRange(CGXRange(1,1),CString("*.*"));
		pgrid->SetValueRange(CGXRange(1,2), LoadStg(IDC_MA_ALL));

		ilCount = omDboLines.GetSize();
		ilCount = ilCount + 1;

		for (int i = 1; i < ilCount; i++)
		{
			// Urno auslesen
			ilUrno = atol(omDboLines[i-1].Urno);
			// Urno mit erstem Feld koppeln.						
			pgrid->SetStyleRange (CGXRange(i+1,1), CGXStyle().SetItemDataPtr( (void*)ilUrno ) );
			
			pgrid->SetValueRange(CGXRange(i+1,0), CString(""));
			pgrid->SetValueRange(CGXRange(i+1,1), omDboLines[i-1].Grpn);
			pgrid->SetValueRange(CGXRange(i+1,2), omDboLines[i-1].Valu);
		}

		pgrid->GetParam()->SetLockReadOnly(true);

		CGXSortInfoArray SortInfo;
		SortInfo.SetSize(1);
		SortInfo[0].nRC = 1;
		SortInfo[0].sortType = CGXSortInfo::autodetect;
		SortInfo[0].sortOrder = CGXSortInfo::ascending;
		pgrid->SortRows(CGXRange().SetTable(), SortInfo);

		int ColCount = pgrid->GetColCount();
		if (bmSelect == true)
		{
			ilCount = pgrid->GetRowCount();
			for (int i = ilCount; i > 0; i--)
			{
				pgrid->ComposeStyleRowCol(i, 1, &style );
				// Urno erhalten
				ilUrno = (long)style.GetItemDataPtr();
				if (ilUrno)
				{
					olUrno.Format("%d", ilUrno);
					if(pomUrnoList->Find(olUrno) != -1)
					{
						POSITION area = pgrid->GetParam( )->GetRangeList( )->AddTail(new CGXRange);
						pgrid->SetSelection(area,i,0,i,ColCount);
					}
				}
			}
		}
	}
}

void ShiftAWDlgBonus::OnCancel() 
{
	// TODO: Add extra cleanup here
	CDialog::OnCancel();
}

LONG ShiftAWDlgBonus::OnGridLButton(WPARAM wParam, LPARAM lParam)
{
	GRIDNOTIFY* rlNotify = (GRIDNOTIFY*) lParam;

	ROWCOL ColCount = pgrid->GetColCount();
	POSITION area = pgrid->GetParam( )->GetRangeList( )->AddTail(new CGXRange);
	pgrid->SetSelection(area,rlNotify->row,0,rlNotify->row,ColCount);

	return 0L;
}
