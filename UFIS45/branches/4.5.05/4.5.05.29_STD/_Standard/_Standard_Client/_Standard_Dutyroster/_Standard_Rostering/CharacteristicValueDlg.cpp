// CharacteristicValueDlg.cpp: Implementierungsdatei
//

#include <stdafx.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


//*******************************************************************************************************
// Broadcast Behandlung
//*******************************************************************************************************

static void CCharacteristicValueDlgCf(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	CCharacteristicValueDlg* polDlg = (CCharacteristicValueDlg*)popInstance;

	//OffDayData* polOffDayData = (OffDayData*) vpDataPointer;

	// Weitergabe an Dialog
	polDlg->HandleBc(ipDDXType,vpDataPointer);
}

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CCharacteristicValueDlg 

CCharacteristicValueDlg::CCharacteristicValueDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CCharacteristicValueDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CCharacteristicValueDlg)
	//}}AFX_DATA_INIT
	pomParent = pParent;
	pomAccountGrid = NULL;
	pomStatisticGrid = NULL;
	//uhi 8.5.01
	pomStfData = NULL;
	pomViewInfo = NULL;
	pomAccounts = NULL;

	omStatValue = "";

    DdxRegister(this, OFFDAY_CHANGE,"CCharacteristicValueDlg","OFFDAY_CHANGE",	    CCharacteristicValueDlgCf);
}

//*****************************************************************************************
// Behandelt die BCs
//*****************************************************************************************

void CCharacteristicValueDlg::HandleBc(int ipDDXType, void* vpDataPointer)
{
	OffDayData* polOffDayData = (OffDayData*) vpDataPointer;

	if(ipDDXType == OFFDAY_CHANGE) 
	{
		// Wert �bernehmen
		int ilValue = polOffDayData->imValue;
		omStatValue.Format("%d",ilValue);
		// Grid neu zeichnen lassen
		//ClearGrids();
		FillStatisticGrid();
		FillAccountGrid();
	}
}

//*****************************************************************************************
// 
//*****************************************************************************************

CCharacteristicValueDlg::~CCharacteristicValueDlg()
{
	// Grids l�schen
	if (pomAccountGrid != NULL) delete pomAccountGrid;
	if (pomStatisticGrid != NULL) delete pomStatisticGrid;

	//uhi 8.5.01
	pomStfData = NULL;
	pomViewInfo = NULL;
	pomAccounts = NULL;

	// Beim BC Handler abmelden
	ogDdx.UnRegister(this, NOTUSED);
}

void CCharacteristicValueDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCharacteristicValueDlg)
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CCharacteristicValueDlg, CDialog)
	//{{AFX_MSG_MAP(CCharacteristicValueDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten CCharacteristicValueDlg 

BOOL CCharacteristicValueDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// alle Resourcen aus Datenbank laden
	SetWindowText(LoadStg(IDS_STRING1918));

	// Grid initialisieren und f�llen
	InitAccountGrid();
	InitStatisticGrid();

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX-Eigenschaftenseiten sollten FALSE zur�ckgeben
}

//**********************************************************************************
// InitAccountGrid: initialisiert das Konto-Grid
// R�ckgabe: keine
//**********************************************************************************

void CCharacteristicValueDlg::InitAccountGrid ()
{
	// Grid erzeugen
	pomAccountGrid = new CGridControl(this,IDC_GRID_ACCOUNTS,2,GRID_MINROWS);

	// Header-Zeile einstellen
	pomAccountGrid->SetValueRange(CGXRange(0,1),LoadStg(IDS_STRING1916));
	pomAccountGrid->SetValueRange(CGXRange(0,2),LoadStg(IDS_STRING1919));

	// Breite der Spalten einstellen
	InitAccountColWidths ();
	// initialisieren
	pomAccountGrid->Initialize();
	// Verhindert, das die Spaltenbreite ver�ndert wird
	pomAccountGrid->GetParam()->EnableTrackColWidth(FALSE);
	// keine Markierung erlauben
	pomAccountGrid->GetParam()->EnableSelection(GX_SELNONE);
	// Styles einstellen	
	pomAccountGrid->SetStyleRange( CGXRange().SetTable(), CGXStyle()
				.SetVerticalAlignment(DT_VCENTER)
				.SetReadOnly(TRUE)
				//.SetEnabled(FALSE)
				.SetControl(GX_IDS_CTRL_STATIC)
				);
	// Grid vergr�ssert sich nicht automatisch
	pomAccountGrid->EnableAutoGrow(FALSE);
}

//**********************************************************************************
// InitAccountColWidths: Breite der Spalten setzen.
// R�ckgabe: keine
//**********************************************************************************

void CCharacteristicValueDlg::InitAccountColWidths()
{
	pomAccountGrid->SetColWidth ( 0, 0, 0);
	pomAccountGrid->SetColWidth ( 1, 1, 112);
	pomAccountGrid->SetColWidth ( 2, 2, 50);
}

//**********************************************************************************
// FillAccountGrid: Grid mit Daten f�llen.
// R�ckgabe: keine
//**********************************************************************************

void CCharacteristicValueDlg::FillAccountGrid()
{
CCS_TRY;
	// Readonly ausschalten
	pomAccountGrid->GetParam()->SetLockReadOnly(false);
	// Anzeige austellen
	pomAccountGrid->LockUpdate(TRUE);

	// Anzahl der Rows ermitteln
	int ilRowCount = pomAccountGrid->GetRowCount();
	if (ilRowCount > 0)
		// Grid l�schen			
		pomAccountGrid->RemoveRows(1,ilRowCount);

		// Rows auff�llen, damit die "optik stimmt"
	for (int i=0; i<GRID_MINROWS;i++)
	{
		// Neue Row einf�gen
		ilRowCount = pomAccountGrid->GetRowCount ();
		pomAccountGrid->InsertRows(ilRowCount+1,1);
		// Urno mit erstem Feld koppeln.						
		pomAccountGrid->SetStyleRange (CGXRange(i+1,1), CGXStyle().SetItemDataPtr(NULL));
		// Datenfelder f�llen
		pomAccountGrid->SetValueRange(CGXRange(i+1,1),"");
		pomAccountGrid->SetValueRange(CGXRange(i+1,2),"");
	}
	
	//uhi 4.5.01 Accounts laden
	ilRowCount = pomAccountGrid->GetRowCount ();
	pomAccountGrid->InsertRows(ilRowCount+1,1);
	CString olTmp = LoadStg(IDS_STRING1920);
	CString olStatName = olTmp.Left(olTmp.Find("\n"));
	CString olTooltip = olTmp.Mid(olTmp.Find("\n")+1);

	CString olAccount6, olAccount5, olAccount4, olYear, olMonth, olType;
	ACCDATA *pomAcc;
	long ilUrno;
	double ilAccount4, ilAccount5, ilAccount6;
	
	ilAccount4 = ilAccount5 = ilAccount6 = 0.0;
	
	if(pomViewInfo != NULL){
		if(pomViewInfo->oSelectDate.GetStatus() == COleDateTime::valid){
			olYear = pomViewInfo->oSelectDate.Format("%Y");
			olMonth = pomViewInfo->oSelectDate.Format("%m");
		}
	}

	if(pomStfData != NULL){
		for(int ilCount = 0; ilCount < pomStfData->GetSize(); ilCount++){
			ilUrno = (*pomStfData)[ilCount].Urno;
			
			//Konto 4
			pomAcc = NULL;
			olType = CString("4");
			pomAcc = ogAccData.GetAccByKey(ilUrno, olYear, olType);
			if(pomAcc != NULL){
				switch(atoi(olMonth)){
					case 1:
						ilAccount4 = ilAccount4 + atol(pomAcc->Co01);
						break;
					case 2:
						ilAccount4 = ilAccount4 + atol(pomAcc->Co02);
						break;
					case 3:
						ilAccount4 = ilAccount4 + atol(pomAcc->Co03);
						break;
					case 4:
						ilAccount4 = ilAccount4 + atol(pomAcc->Co04);
						break;
					case 5:
						ilAccount4 = ilAccount4 + atol(pomAcc->Co05);
						break;
					case 6:
						ilAccount4 = ilAccount4 + atol(pomAcc->Co06);
						break;
					case 7:
						ilAccount4 = ilAccount4 + atol(pomAcc->Co07);
						break;
					case 8:
						ilAccount4 = ilAccount4 + atol(pomAcc->Co08);
						break;
					case 9:
						ilAccount4 = ilAccount4 + atol(pomAcc->Co09);
						break;
					case 10:
						ilAccount4 = ilAccount4 + atol(pomAcc->Co10);
						break;
					case 11:
						ilAccount4 = ilAccount4 + atol(pomAcc->Co11);
						break;
					case 12:
						ilAccount4 = ilAccount4 + atol(pomAcc->Co12);
						break;
					default:
						break;
				}
		
			}
			//Komto 5
			pomAcc = NULL;
			olType = CString("5");
			pomAcc = ogAccData.GetAccByKey(ilUrno, olYear, olType);
			if(pomAcc != NULL){
				switch(atoi(olMonth)){
					case 1:
						ilAccount5= ilAccount5 + atol(pomAcc->Cl01);
						break;
					case 2:
						ilAccount5= ilAccount5 + atol(pomAcc->Cl02);
						break;
					case 3:
						ilAccount5= ilAccount5 + atol(pomAcc->Cl03);
						break;
					case 4:
						ilAccount5= ilAccount5 + atol(pomAcc->Cl04);
						break;
					case 5:
						ilAccount5= ilAccount5 + atol(pomAcc->Cl05);
						break;
					case 6:
						ilAccount5= ilAccount5 + atol(pomAcc->Cl06);
						break;
					case 7:
						ilAccount5= ilAccount5 + atol(pomAcc->Cl07);
						break;
					case 8:
						ilAccount5= ilAccount5 + atol(pomAcc->Cl08);
						break;
					case 9:
						ilAccount5= ilAccount5 + atol(pomAcc->Cl09);
						break;
					case 10:
						ilAccount5= ilAccount5 + atol(pomAcc->Cl10);
						break;
					case 11:
						ilAccount5= ilAccount5 + atol(pomAcc->Cl11);
						break;
					case 12:
						ilAccount5= ilAccount5 + atol(pomAcc->Cl12);
						break;
					default:
						break;
				}
			}
		}
		
		//Konto 6
		ilAccount6 = ilAccount5 - ilAccount4;

		olAccount5.Format("%.0f", ilAccount5);
		olAccount6.Format("%.0f", ilAccount6);

		//uhi 9.5.01 Beschriftung
		CString olAcc5Snam, olAcc6Snam;
		olAcc5Snam = CString("OFFMIst");
		olAcc6Snam = CString("OFFMUngp");
		ACCOUNTTYPEINFO *pAcc5, *pAcc6;
		if(pomAccounts != NULL){
			pAcc5 = pomAccounts->GetAccountTypeInfoByAccountType(5);
			if(pAcc5 != NULL) //ARE 23.5.01
				olAcc5Snam = pAcc5->oShortName;
			pAcc6 = pomAccounts->GetAccountTypeInfoByAccountType(6);
			if(pAcc6 != NULL) //ARE 23.5.01
				olAcc6Snam = pAcc6->oShortName;
		}

		//Konto 6 = Konto 5 - Konto 4
		pomAccountGrid->SetValueRange(CGXRange(1,1),olAcc5Snam);
		pomAccountGrid->SetValueRange(CGXRange(1,2), olAccount5);

		//Konto 5
		pomAccountGrid->SetValueRange(CGXRange(2,1),olAcc6Snam);
		pomAccountGrid->SetValueRange(CGXRange(2,2), olAccount6);
	}


	// Readonly ausschalten
	pomAccountGrid->GetParam()->SetLockReadOnly(true);
	// Anzeige austellen
	pomAccountGrid->LockUpdate(false);
	// neu zeichnen
	pomAccountGrid->Redraw();

CCS_CATCH_ALL;
}

//**********************************************************************************
// InitStatisticGrid: initialisiert das Statistik-Grid
// R�ckgabe: keine
//**********************************************************************************

void CCharacteristicValueDlg::InitStatisticGrid ()
{
	// Grid erzeugen
	pomStatisticGrid = new CGridControl(this,IDC_GRID_STATISTIC,2,GRID_MINROWS);

	// Header-Zeile
	pomStatisticGrid->SetValueRange(CGXRange(0,1),LoadStg(IDS_STRING1917));
	pomStatisticGrid->SetValueRange(CGXRange(0,2),LoadStg(IDS_STRING1919));
	
	// Breite der Spalten einstellen
	InitStatisticColWidths ();
	// Initialisieren
	pomStatisticGrid->Initialize();
	// Tooltips aktivieren
	pomStatisticGrid->EnableGridToolTips();
	CGXStylesMap *olStylesmap1 = pomStatisticGrid->GetParam()->GetStylesMap();
	olStylesmap1->AddUserAttribute(GX_IDS_UA_TOOLTIPTEXT, CGXStyle().SetWrapText(TRUE).SetAutoSize(TRUE));
	// Verhindert, das die Spaltenbreite ver�ndert wird
	pomStatisticGrid->GetParam()->EnableTrackColWidth(FALSE);
	// Markierung verhindern
	pomStatisticGrid->GetParam()->EnableSelection(GX_SELNONE);
	// sonstige Styles	
	pomStatisticGrid->SetStyleRange( CGXRange().SetTable(), CGXStyle()
				.SetVerticalAlignment(DT_VCENTER)
				.SetReadOnly(TRUE)
				//.SetEnabled(FALSE)
				.SetControl(GX_IDS_CTRL_STATIC)
				);
	// keine automatische Vergr�sserung
	pomStatisticGrid->EnableAutoGrow(FALSE);
}

//**********************************************************************************
// InitStatisticColWidths: Breite der Spalten setzen.
// R�ckgabe: keine
//**********************************************************************************

void CCharacteristicValueDlg::InitStatisticColWidths()
{
	pomStatisticGrid->SetColWidth ( 0, 0, 0);
	pomStatisticGrid->SetColWidth ( 1, 1, 112);
	pomStatisticGrid->SetColWidth ( 2, 2, 50);
}

//**********************************************************************************
// FillStatisticGrid: Grid mit Daten f�llen.
// R�ckgabe: keine
//**********************************************************************************

void CCharacteristicValueDlg::FillStatisticGrid()
{
CCS_TRY;
	// Readonly ausschalten
	pomStatisticGrid->GetParam()->SetLockReadOnly(false);
	// Anzeige austellen
	pomStatisticGrid->LockUpdate(TRUE);

	// Anzahl der Rows ermitteln
	int ilRowCount = pomStatisticGrid->GetRowCount();
	if (ilRowCount > 0)
		// Grid l�schen			
		pomStatisticGrid->RemoveRows(1,ilRowCount);
	
	//-----------------------------------------------------------------------------------------------------
	// hardcodiert: bisher nur ein Eintrag in Statistik-Grid.
	// Diesen Eintrag aus Stringtable laden, Tooltip-Text steht hinter
	// '\n'.
	//-----------------------------------------------------------------------------------------------------
	ilRowCount = pomStatisticGrid->GetRowCount ();
	pomStatisticGrid->InsertRows(ilRowCount+1,1);
	CString olTmp = LoadStg(IDS_STRING1920);
	CString olStatName = olTmp.Left(olTmp.Find("\n"));
	CString olTooltip = olTmp.Mid(olTmp.Find("\n")+1);
	// Statistik-Bezeichner und Tooltip einstellen
	pomStatisticGrid->SetValueRange(CGXRange(ilRowCount+1,1),olStatName);
	pomStatisticGrid->SetStyleRange(CGXRange(ilRowCount+1,1), CGXStyle().SetUserAttribute(GX_IDS_UA_TOOLTIPTEXT,olTooltip));	
	pomStatisticGrid->SetValueRange(CGXRange(ilRowCount+1,2), omStatValue);

	//-----------------------------------------------------------------------------------------------------

	// Rows auff�llen, damit die "optik stimmt"
	for (int i=1; i<GRID_MINROWS;i++)
	{
		// Neue Row einf�gen
		ilRowCount = pomStatisticGrid->GetRowCount ();
		pomStatisticGrid->InsertRows(ilRowCount+1,1);
		// Urno mit erstem Feld koppeln.						
		pomStatisticGrid->SetStyleRange (CGXRange(i+1,1), CGXStyle().SetItemDataPtr(NULL));
		// Datenfelder f�llen
		pomStatisticGrid->SetValueRange(CGXRange(i+1,1),"");
		pomStatisticGrid->SetValueRange(CGXRange(i+1,2),"");
	}

	// Readonly ausschalten
	pomStatisticGrid->GetParam()->SetLockReadOnly(true);
	// Anzeige austellen
	pomStatisticGrid->LockUpdate(false);
	// neu zeichnen
	pomStatisticGrid->Redraw();
CCS_CATCH_ALL;
}

//**********************************************************************************
// FillGrids: f�llt die Grids mit Daten.
// R�ckgabe: keine
//**********************************************************************************

void CCharacteristicValueDlg::FillGrids()
{
CCS_TRY;
	FillStatisticGrid();
	FillAccountGrid();
CCS_CATCH_ALL;
}

//**********************************************************************************
// ClearGrids: leert die Grids und f�llt sie mit Leerzeilen auf, wenn 
//	<bpFillBlank> true ist.
// R�ckgabe: keine
//**********************************************************************************

void CCharacteristicValueDlg::ClearGrids(bool bpFillBlank /*= true*/)
{
CCS_TRY;
	//------------------------------------------------------------------------------
	// Statistik-Grid leeren
	//------------------------------------------------------------------------------
	// Readonly ausschalten
	pomStatisticGrid->GetParam()->SetLockReadOnly(false);
	// Anzeige austellen
	pomStatisticGrid->LockUpdate(TRUE);

	// Anzahl der Rows ermitteln
	int ilRowCount = pomStatisticGrid->GetRowCount();
	if (ilRowCount > 0)
		// Grid l�schen			
		pomStatisticGrid->RemoveRows(1,ilRowCount);
	
	// ggbf. mit Leerzeilen auff�llen
	if (bpFillBlank)
	{
		for (int i=0; i<GRID_MINROWS;i++)
		{
			// Neue Row einf�gen
			ilRowCount = pomStatisticGrid->GetRowCount ();
			pomStatisticGrid->InsertRows(ilRowCount+1,1);
			// Urno mit erstem Feld koppeln.						
			pomStatisticGrid->SetStyleRange (CGXRange(i+1,1), CGXStyle().SetItemDataPtr(NULL));
			// Datenfelder f�llen
			pomStatisticGrid->SetValueRange(CGXRange(i+1,1),"");
			pomStatisticGrid->SetValueRange(CGXRange(i+1,2),"");
		}
	}

	// Readonly ausschalten
	pomStatisticGrid->GetParam()->SetLockReadOnly(true);
	// Anzeige austellen
	pomStatisticGrid->LockUpdate(false);
	// neu zeichnen
	pomStatisticGrid->Redraw();
	
	//------------------------------------------------------------------------------
	// Konten-Grid leeren
	//------------------------------------------------------------------------------
	// Readonly ausschalten
	pomAccountGrid->GetParam()->SetLockReadOnly(false);
	// Anzeige austellen
	pomAccountGrid->LockUpdate(TRUE);

	// Anzahl der Rows ermitteln
	ilRowCount = pomAccountGrid->GetRowCount();
	if (ilRowCount > 0)
		// Grid l�schen			
		pomAccountGrid->RemoveRows(1,ilRowCount);
	
	// ggbf. mit Leerzeilen auff�llen
	if (bpFillBlank)
	{
		for (int i=0; i<GRID_MINROWS;i++)
		{
			// Neue Row einf�gen
			ilRowCount = pomAccountGrid->GetRowCount ();
			pomAccountGrid->InsertRows(ilRowCount+1,1);
			// Urno mit erstem Feld koppeln.						
			pomAccountGrid->SetStyleRange (CGXRange(i+1,1), CGXStyle().SetItemDataPtr(NULL));
			// Datenfelder f�llen
			pomAccountGrid->SetValueRange(CGXRange(i+1,1),"");
			pomAccountGrid->SetValueRange(CGXRange(i+1,2),"");
		}
	}

	// Readonly ausschalten
	pomAccountGrid->GetParam()->SetLockReadOnly(true);
	// Anzeige austellen
	pomAccountGrid->LockUpdate(false);
	// neu zeichnen
	pomAccountGrid->Redraw();

	//uhi 8.5.01
	//pomStfData = NULL;
	//pomViewInfo = NULL;

CCS_CATCH_ALL;
}
