// GridControl.h: interface for the CGridControl class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_GRIDCONTROL_H__775E51E1_38ED_11D3_933B_00001C033B5D__INCLUDED_)
#define AFX_GRIDCONTROL_H__775E51E1_38ED_11D3_933B_00001C033B5D__INCLUDED_

#include <gxall.h>
#include <basicdata.h>

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

struct DOONMOUSECLICK
{
	UINT	iMsg;
	WPARAM  iWparam;
	bool	bOnlyInside; 	
};

#define WM_GRID_LBUTTONDBLCLK  WM_USER+800
#define WM_GRID_LBUTTONCLK  WM_USER+801
#define WM_GRID_LBUTTONDOWN  WM_USER+802
#define WM_GRID_DRAGBEGIN  WM_USER+803

class CGridControl : public CGXGridWnd  
{
public:
	//uhi 18.7.00 Abspeichern Sortierung
	CGXSortInfoArray omSortInfo;
	CGridControl ();
	CGridControl ( CWnd *popParent, UINT nID, ROWCOL nCols, ROWCOL nRows, bool bpDeleteSelection = true );
	virtual ~CGridControl();
    //int GetColWidth(ROWCOL nCol);

//Implementation
	BOOL SetValue ( ROWCOL nRow, ROWCOL nCol, CString &cStr ) ;
	const CString& GetValue ( ROWCOL nRow, ROWCOL nCol ) ;
	virtual BOOL EnableGrid ( ROWCOL iRow, BOOL enable );
	virtual BOOL IsGridEnabled ( ROWCOL iRow, ROWCOL iCol );
	void CopyStyleOfLine ( ROWCOL ipSourceLine, ROWCOL ipDestLine,
						   bool bpResetValues=true );
	void CopyStyleLastLineFromPrev ( bool bpResetValues=true );
	void EnableAutoGrow ( bool enable=true );
	void EnableSorting ( bool enable=true );
	void SetLbDblClickAction ( UINT ipMsg, WPARAM ipWparam, bool bpOnlyInside=true ); 
	void SetDirtyFlag ( bool dirty=true );
	bool IsGridDirty ();
	void SetParentWnd(CWnd *popWnd) {pomWnd = popWnd;}
	void SetEnableDnD(bool bpEnableDnD) {bmIsDnDEnabled = bpEnableDnD;}
	void SetEnableExtraToolTip(bool bpShowExtraToolTip) {bmShowExtraToolTip = bpShowExtraToolTip;}
	int FindRowByUrnoDataPointer(long lpUrno);
	void SetSelection(POSITION SelRectId, ROWCOL nTop = 0, ROWCOL nLeft = 0, ROWCOL nBottom = 0, ROWCOL nRight = 0);

protected:
	friend class ShiftAWDlg;

	BOOL GetStyleRowCol(ROWCOL nRow, ROWCOL nCol, CGXStyle& style, GXModifyType mt = gxCopy, int nType = 0);
	BOOL OnEndEditing( ROWCOL nRow, ROWCOL nCol) ;
	virtual void DoAutoGrow ( ROWCOL nRow, ROWCOL nCol);
	virtual BOOL InsertBottomRow ();
	BOOL OnRButtonDblClkRowCol(ROWCOL nRow, ROWCOL nCol, UINT nFlags, CPoint pt);
	BOOL OnLButtonDblClkRowCol(ROWCOL nRow, ROWCOL nCol, UINT nFlags, CPoint pt);
	BOOL OnLButtonClickedRowCol(ROWCOL nRow, ROWCOL nCol, UINT nFlags, CPoint pt);
	void OnTextNotFound(LPCTSTR);
	BOOL OnDeleteCell(ROWCOL nRow, ROWCOL nCol);
	virtual bool SortTable ( ROWCOL ipRowClicked, ROWCOL ipColClicked );
	
private:
	bool bmDeleteSelection;


//{{AFX_MSG(CGridControl)
	afx_msg void OnLButtonUp(UINT nFlags, CPoint pt);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint pt);
//	afx_msg void OnKillFocus ( CWnd* pNewWnd );
	//}}AFX_MSG
DECLARE_MESSAGE_MAP()

	
protected:
	UINT	umID;
	CWnd	*pomWnd;
	bool    bmAutoGrow;
	bool    bmSortEnabled;
	bool    bmSortAscend;
	DOONMOUSECLICK smLButtonDblClick;	
	bool	bmIsDirty ;
	bool	bmIsDnDEnabled;
	bool	bmShowExtraToolTip;
};

struct GRIDNOTIFY
{
	UINT	idc;
	ROWCOL	row;
	ROWCOL	col;
	ROWCOL	headerrows;
	ROWCOL	headercols;
	POINT	point;
	CString value1;
	void* value2;
	CGridControl* source;
};


class CTitleGridControl : public CGridControl
{
public:
	CTitleGridControl ();
	CTitleGridControl ( CWnd *popParent, UINT nID, ROWCOL nCols, ROWCOL nRows );
	virtual ~CTitleGridControl();
//Implementation
	void SetTitle ( CString &opTitle );
	CString GetTitle ();
	const CString &GetValue ( ROWCOL nRow, ROWCOL nCol ) ;
	BOOL SetValue ( ROWCOL nRow, ROWCOL nCol, CString &cStr ) ;
	BOOL EnableGrid ( BOOL enable );
	void RemoveOneRow ( ROWCOL nRow );
	bool SortTable ( ROWCOL ipRowClicked, ROWCOL ipColClicked );
protected:
	BOOL InsertBottomRow ();
	void SetRowHeadersText ();
	BOOL OnStartEditing(ROWCOL nRow, ROWCOL nCol);
private:
	CString omTitle;
};

class CDutyAbsenceOverviewGridControl : public CGridControl
{
public:
	CDutyAbsenceOverviewGridControl ();
	CDutyAbsenceOverviewGridControl (CWnd *popParent, UINT nID, ROWCOL nCols, ROWCOL nRows );
	virtual ~CDutyAbsenceOverviewGridControl();
protected:
	virtual	BOOL OnStartSelection(ROWCOL nRow, ROWCOL nCol, UINT flags, CPoint point);
	virtual BOOL CanChangeSelection(CGXRange* pRange, BOOL bIsDragging, BOOL bKey);
	virtual BOOL OnLButtonHitRowCol(ROWCOL nHitRow, ROWCOL nHitCol, ROWCOL nDragRow, ROWCOL nDragCol, CPoint point, UINT flags, WORD nHitState);

	virtual	BOOL GetStyleRowCol(ROWCOL nRow,ROWCOL nCol,CGXStyle& style,GXModifyType mt=gxCopy,int nType=0);
	virtual	void DrawInvertCell(CDC *pDC,ROWCOL nRow,ROWCOL nCol,CRect rectItem);

};

#endif // !defined(AFX_GRIDCONTROL_H__775E51E1_38ED_11D3_933B_00001C033B5D__INCLUDED_)
