#if !defined(_DUTYDEBITTABVIEWER_H_INCLUDED_)
#define _DUTYDEBITTABVIEWER_H_INCLUDED_

#include <stdafx.h>

#define S_SCROLL_UP   true;
#define S_SCROLL_DOWN false;

//*** contains shift info == line ***
struct DDTV_SHIFT
{
	CString Bsdc;				//ShiftCode
	CString Fctc;				//FunctionCode
	CString omShiftTime;		// Schichtbeginn+Schichtende - f�rs Sortieren, z.B. 05301545
	CString ShiftText;			//Group text to show in grid
	CString Key;

	CString DataType;			//0 = Total amount -> header
								//1 = geplante Tage
								//2 = single Shifts	
								//3 = groups
								
	CPtrArray omDayDebitShift;	//Ptr to DayDebitShift
};

struct DayDebitShift			//*** contains dayly info == column ***
{
	int DebitCount;				// Soll-Seite = Bedarf
	int ShiftCount;				// Ist-Seite = vergeben
};

/////////////////////////////////////////////////////////////////////////////
// DutyDebitTabViewer
/////////////////////////////////////////////////////////////////////////////
class DutyDebitTabViewer : public CViewer
{
// Constructions
public:
    DutyDebitTabViewer(CWnd* pParent = NULL);
    ~DutyDebitTabViewer();
	void HandleBc(int ipDDXType, void *vpDataPointer);
	void Attach(CCSDebitDynTable *popAttachWnd);
	virtual void ChangeView(bool bpSetHNull = true, bool bpSetVNull = true);
	// Berechnet die Anzahl der noch m�glichen Abwesenheiten
	int GetFreeShiftValue(int ipOdaCount,int ipDayNo);	
// Internal data processing routines
private:
	void ProcessDrrNew(DRRDATA* popDrr);
	void ProcessDrrChange(DRRDATA* popDrr);
	void ProcessDrrDelete(DRRDATA* popDrr);
	void ProcessSdtNew(SDTDATA* popSdt);
	void ProcessSdtDelete(SDTDATA* popSdt);
	void ProcessAccBc();

	DutyRoster_View *pomParent;
	void InitTableArrays(int ipColumnOffset = 0);
	void MakeTableData(int ipColumnOffset,int ipRowOffset);
	void AddRow(int ipRow, int ipColumns, int ipColumnOffset, int ipRowOffset);
	void DelRow(int ipRow);
	void AddColumn(int ipColumn, int ipColumnOffset, int ipRowNr);
	void DelColumn(int ipColumn);

	int ReadDebitData();
	int ReadAllShiftFromDrr();
	bool ReadSingleShiftFromDrr(DRRDATA* popDrr,bool bpSendBC=true,CString* popKeyDrrChanged=0);
	CString ReadShiftFormated(int ilLineNo, int ilDayNo, COLORREF &ShiftCol);

	int DeleteAllShifts();
	int DeleteShiftByLineNo(int ilNo);
	int GenerateShift(DDTV_SHIFT &rlShiftData, CString olKey);

	int AddShiftDebit(int ilLineNo, int ilDayNo, BOOL bAddTotal);
	int AddShiftCount(int ilLineNo, int ilDayNo, BOOL bAddTotal);

	int DecreaseShiftCount(int ilLineNo, int ilDayNo,CString opKey, BOOL bAddTotal);
	int DecreaseShiftDebit(int ilLineNo, int ilDayNo, BOOL bAddTotal);

	int SortShifts();
	int GenerateTotalShift();

	CString GetShiftDescription(int ilNo);
	// Pr�ft, ob dieser DRR bearbeitet werden soll.
	bool IsDrrValid(DRRDATA* popDrr,bool bpIsDelete, bool* pbpIsInStat=0);
	bool IsCodeOda(long lpUrno);
	bool IsCodeOda(CString opCode);

	// Berechnet die Summe der m�glichen Abwesenheiten
	int GetFreeShiftValueSum();

	void DeleteSingleShiftData(DRRDATA* popDrr, CString* popKeyDrrChanged=0);
	// Einlesen der geplanten Tage/Schichten
	void PreparePlanedDays();
	// sortieren und neuzeichnen.
	void RefreshView();
	// Sendet BC bez�glich �nderung der zu OFF Tage
	void SendOffDayChangedBC();
	// Aktualisiert die Map mit den Stati eines DRR in der Doppeltour
	void SetStatMap(DRRDATA* popDrr);
	int LookForSdt(SDTDATA* popSdt, CString& opKey);
	int LookForDrr(DRRDATA* popDrr, CString& opKey);
	int LookForGroup(int ipGroupLineNo);
	void InsertDrrToCodeMap(DRRDATA* popDrr);

// Window refreshing routines
public:
	void HorzTableScroll(MODIFYTAB *prpModiTab);
	void VertTableScroll(MODIFYTAB *prpModiTab);
	void TableSizing(/*UINT nSide, */LPRECT lpRect);
	void MoveTable(MODIFYTAB *prpModiTab);
	// Hier sind die Schichten zeilenweise gespeichet. [0] - Header, [1] - Possible Days off
	CCSPtrArray<DDTV_SHIFT>	omDDTV_Shifts;

// Attributes
private:
	bool HasShiftDemand(DRRDATA *prpDrrData);
	bool HadShiftDemand(DRRDATA *prpDrrData);
	void HandleOffDays(DRRDATA *prpDrrData);
	void IncreaseAbsenceCount(CString opSday);
	void DecreaseAbsenceCount(CString opSday);
	bool IsStfuInStatUrnos(long lpStfu);
	bool IsStfuInExtraUrnos(long lpStfu);
	bool UseDrrRecord(CString opRosl, CString opRoss, bool bpIsDelete);

	bool Scroll2ActLine(DRRDATA* popDrr, CString* popKeyDrrChanged=0);
    CCSDebitDynTable *pomDynTab;
	TABLEDATA *prmTableData;
	
	VIEWINFO *prmViewInfo;

	COleDateTime omFrom;
	COleDateTime omTo;
	COleDateTime omShowFrom;
	COleDateTime omShowTo;
	int imDays;
	bool blLastActionIsScrollUpOrDown;
	// Sync bei Release
	bool bmReleaseInProgress;

	int					imDebitDays;
	// Anzahl der zus�tzlichen geladenen Tage
	int					imBonusLoadDays,imBonusLoadDaysTo,imBonusLoadDaysFrom;

	CMapStringToPtr		omDates;
	CStringArray		omDatesString;

	CMapPtrToPtr		omGroupMap;
	CMapStringToPtr		omGroupMember;
	CStringArray		omGroupName;


	// Key: BSDC-ORG | BSDC-ORG-FCTC | omGroupName[i] | OFFDAYS | omDDTV_Shifts[ilD].Key
	CMapStringToPtr		omShiftMap;

	// Verwaltet die DRR Urnos in bezug auf den Schichtcode
	CMapStringToString		omDrrToCodeMap;
	// Gibt an, welchen Status bei Doppeltouren ein DRR vor der �nderung hatte
	CMapStringToString		omDrrToStatusMap;
	CMapStringToPtr			omDrrToStatusMapExtra;
	CMapStringToPtr			omDrrOdaUrnos;
	CMapStringToPtr			omStatShiftsWithoutDemand;
	CMapStringToPtr			omSecondShifts;

	CString				omDateFrom;
	CString				omDateTo;

	// Datenstruct f�r die Broadcasts
	OffDayData			omOffDayData;

	enum mModes{SHIFTONLY,SHIFTWITHFCT} mStatMode;
	enum mCalcModes{ISTSOLL,DIFFERENZ} mCalcMode;

	int imStatView;

	BOOL mGroupMode;
	BOOL mStatistic;	//Calculate Statistic YES/NO

	// Anzahl der feststehenden Lines, die nicht gescrollt werden d�rfen
	int imFixLines;			// ist z.Zt. auf 1 festgelegt
};

#endif //_DUTYDEBITTABVIEWER_H_INCLUDED_
