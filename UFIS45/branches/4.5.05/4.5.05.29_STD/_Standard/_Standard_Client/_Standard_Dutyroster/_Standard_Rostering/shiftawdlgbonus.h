#ifndef AFX_SHIFTAWDLGBONUS_H__7891F411_CEC0_11D1_BFCE_004095434A85__INCLUDED_
#define AFX_SHIFTAWDLGBONUS_H__7891F411_CEC0_11D1_BFCE_004095434A85__INCLUDED_

// ShiftAWDlgBonus.h : Header-Datei
//

#include <stdafx.h>
#include <basicdata.h>
#include <ShiftRoster_View.h>
#include <GridControl.h>


// Auszug aus DBO
struct AWDBOVIEWDATA_SHFT_BONUS
{
	CString Grpn; 	// Beaufschlagungs-Gruppenname (aus GRN)
	CString Valu; 	// Werte der Grupierung (BSD-Urnos aus GRM)
	CString	Urno;  	// Eindeutige Datensatz-Nr. (GRN)
};
// end AWDBOVIEWDATA_SHFT

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld ShiftAWDlgBonus 
class ShiftAWDlgBonus : public CDialog
{
// Konstruktion
public:
	ShiftAWDlgBonus(CString *popUrnoList, CString opOtherSelectUrnoList, UINT ipAWType,bool bpSelect,CString opVpfr,CString opVpto);
	~ShiftAWDlgBonus(void);
// Dialogfelddaten
	//{{AFX_DATA(ShiftAWDlgBonus)
	enum { IDD = IDD_SHIFTROSTER_AW_DLG };
	CButton	m_Check_ShowAll;
	CButton	m_B_Info;
	CButton	m_B_Ok;
	CButton	m_B_Cancel;
	CButton	m_B_DeleteAll;
	//}}AFX_DATA


// Überschreibungen
	// Vom Klassen-Assistenten generierte virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(ShiftAWDlgBonus)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	//}}AFX_VIRTUAL

	//void Sort(int ipSort);

// Implementierung
protected:

	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(ShiftAWDlgBonus)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	afx_msg void OnBDeleteall();
	afx_msg void OnInfo();
	virtual void OnCancel();
	afx_msg LONG OnGridLButton(WPARAM wParam, LPARAM lParam);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

public:
	CString *pomUrnoList;
	CCSPtrArray<AWDBOVIEWDATA_SHFT_BONUS>  omDboLines;
	bool bmSelect;
	UINT imAWType;

protected:
	void IniBonusGrid();
	CString omOtherSelectUrnoList;

private:
	CGridControl *pgrid;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio fügt zusätzliche Deklarationen unmittelbar vor der vorhergehenden Zeile ein.

#endif // AFX_SHIFTAWDLG_H__7891F411_CEC0_11D1_BFCE_004095434A85__INCLUDED_
