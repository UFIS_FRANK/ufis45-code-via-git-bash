#if !defined(AFX_SENDTOSAPDLG_H__982CD501_BD88_11D1_944F_0000B44A2C90__INCLUDED_)
#define AFX_SENDTOSAPDLG_H__982CD501_BD88_11D1_944F_0000B44A2C90__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// SendToSapDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CSendToSapDlg dialog

class CSendToSapDlg : public CDialog
{
// Construction
public:
	CSendToSapDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CSendToSapDlg)
	enum { IDD = IDD_SEND_TO_SAP };
	CComboBox	m_ComboOrgUnit;
	CButton	m_OK;
	CButton	m_Cancel;
	CButton	m_CR_LangzeitDienstplan;
	CCSEdit	m_CE_DateFrom;
	CCSEdit	m_CE_DateTo;
	CCSEdit	m_CE_TimeFrom;
	CCSEdit	m_CE_TimeTo;
	int		m_VI_LangzeitDienstplan;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSendToSapDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CSendToSapDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	virtual void OnCancel();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()


public:
	void InternalCancel();

private:
	void FillComboOrgUnit();
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SENDTOSAPDLG_H__982CD501_BD88_11D1_944F_0000B44A2C90__INCLUDED_)
