// PSDutyRoster4ViewPage.cpp: Implementierungsdatei
//

#include <stdafx.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Eigenschaftenseite PSDutyRoster4ViewPage 
//---------------------------------------------------------------------------

IMPLEMENT_DYNCREATE(PSDutyRoster4ViewPage, RosterViewPage)

PSDutyRoster4ViewPage::PSDutyRoster4ViewPage() : RosterViewPage(PSDutyRoster4ViewPage::IDD)
{
	//{{AFX_DATA_INIT(PSDutyRoster4ViewPage)
		// HINWEIS: Der Klassen-Assistent f�gt hier Elementinitialisierung ein
	//}}AFX_DATA_INIT

	omTitleStrg = LoadStg(IDS_W_SortGrupp);
	m_psp.pszTitle = omTitleStrg;
	m_psp.dwFlags |= PSP_USETITLE;

	bmPgpLoaded = false;
}

PSDutyRoster4ViewPage::~PSDutyRoster4ViewPage()
{
}

void PSDutyRoster4ViewPage::DoDataExchange(CDataExchange* pDX)
{
	RosterViewPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(PSDutyRoster4ViewPage)
	DDX_Control(pDX, IDC_S_4,				m_S4);
	DDX_Control(pDX, IDC_S_3,				m_S3);
	DDX_Control(pDX, IDC_S_2,				m_S2);
	DDX_Control(pDX, IDC_PLAN4,				m_Plan4);
	DDX_Control(pDX, IDC_PLAN3,				m_Plan3);
	DDX_Control(pDX, IDC_PLAN2,				m_Plan2);
	DDX_Control(pDX, IDC_R_SORT_PERIODE,	m_R_SortPeriode);
	DDX_Control(pDX, IDC_R_SORT_DAY,		m_R_SortDay);
	DDX_Control(pDX, IDC_C_GRUPP,			m_C_Grupp);
	DDX_Control(pDX, IDC_NOGRUPP,			m_NoGrupp);
	DDX_Control(pDX, IDC_GRUPP,				m_Grupp );
	DDX_Control(pDX, IDC_R_PERC,			m_R_Perc);
	DDX_Control(pDX, IDC_R_SHNM,			m_R_Shnm);
	DDX_Control(pDX, IDC_R_FLNAME,			m_R_FLName);
	DDX_Control(pDX, IDC_R_FREE,			m_R_Free);
	DDX_Control(pDX, IDC_CHECK_SORT,		m_Check_Sort);
	//}}AFX_DATA_MAP

	if (pDX->m_bSaveAndValidate == FALSE)
	{
		SetData();
	}
	if (pDX->m_bSaveAndValidate == TRUE)
	{
		GetData();
	}

}


BEGIN_MESSAGE_MAP(PSDutyRoster4ViewPage, RosterViewPage)
	//{{AFX_MSG_MAP(PSDutyRoster4ViewPage)
	ON_WM_PAINT()
	ON_BN_CLICKED(IDC_GRUPP, OnGrupp)
	ON_BN_CLICKED(IDC_NOGRUPP, OnNogrupp)
	ON_BN_CLICKED(IDC_B_START_CONFIG, OnBStartConfig)
	ON_BN_CLICKED(IDC_R_FLNAME, OnRFlname)
	ON_BN_CLICKED(IDC_R_FREE, OnRFree)
	ON_BN_CLICKED(IDC_R_PERC, OnRPerc)
	ON_BN_CLICKED(IDC_R_SHNM, OnRShnm)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten PSDutyRoster4ViewPage 
//---------------------------------------------------------------------------

void PSDutyRoster4ViewPage::SetCalledFrom(CString opCalledFrom)
{
	omCalledFrom = opCalledFrom;
}

//---------------------------------------------------------------------------

void PSDutyRoster4ViewPage::OnPaint() 
{
	CPaintDC dc(this); // device context for painting

	CRect olRect;
	CPen olPen;
	olPen.CreatePen(PS_SOLID, 1, COLORREF(BLACK)); 
	dc.SelectObject(&olPen);
	CBrush olBrush;
	olBrush.CreateSolidBrush(COLORREF(BLACK));
	for(int i=0; i<3; i++ )
	{
		switch (i)
		{
			case 0:
			{
				m_S2.GetWindowRect(&olRect);
				olBrush.DeleteObject();
				olBrush.CreateSolidBrush(COLORREF(BLACK));
				break;
			}
			case 1:
			{
				m_S3.GetWindowRect(&olRect);
				olBrush.DeleteObject();
				olBrush.CreateSolidBrush(COLORREF(BLUE));
				break;
			}
			case 2:
			{
				m_S4.GetWindowRect(&olRect);
				olBrush.DeleteObject();
				olBrush.CreateSolidBrush(COLORREF(PURPLE));
				break;
			}
		}
		ScreenToClient(&olRect);
		dc.SelectObject(&olBrush);
		dc.Rectangle(olRect); //Ellipse,Rectangle
	}
}

//---------------------------------------------------------------------------

BOOL PSDutyRoster4ViewPage::OnInitDialog() 
{
	//Nur auf der Ersten Seite wird OnInitDialog vor SetData aufgerufen !!!!!!!!!
	RosterViewPage::OnInitDialog();

	m_S2.ShowWindow(SW_HIDE);
	m_S3.ShowWindow(SW_HIDE);
	m_S4.ShowWindow(SW_HIDE);

	m_C_Grupp.SetFont(&ogCourier_Regular_10);

	SetStatic();
	SetButtonText();
	HandleNameRadioButtons();

	return TRUE;
}

//---------------------------------------------------------------------------

BOOL PSDutyRoster4ViewPage::GetData()
{
	omValues.RemoveAll();
	CStringArray olTmpValues;
	if(GetData(olTmpValues) == FALSE)
	{
		return FALSE;
	}
	for(int i = 0; i < olTmpValues.GetSize(); i++)
	{
		CString olTmp = olTmpValues[i];
		omValues.Add(olTmpValues[i]);
	}
	return TRUE;
}

//---------------------------------------------------------------------------

BOOL PSDutyRoster4ViewPage::GetData(CStringArray &opValues)
{
	if(ogPrivList.GetStat("DUTYROSTER2PS_IDC_PLAN4") != '1') m_Plan4.SetCheck(0);
	if(ogPrivList.GetStat("DUTYROSTER2PS_IDC_PLAN3") != '1') m_Plan3.SetCheck(0);
	if(ogPrivList.GetStat("DUTYROSTER2PS_IDC_PLAN2") != '1') m_Plan2.SetCheck(0);

	//----------------------------0000
	if(m_R_Perc.GetCheck() == 1)
	{
		opValues.Add("CODE");
	}
	else if (m_R_FLName.GetCheck() == 1)
	{
		opValues.Add("FLNAME");
	}
	else if (m_R_Shnm.GetCheck() == 1)
	{
		opValues.Add("SHNM");
	}
	else if (m_R_Free.GetCheck() == 1)
	{
		opValues.Add("FREE");
	}

	//----------------------------1111
	if (m_Grupp.GetCheck() == 1)
	{
		opValues.Add("GRUPP");
	}
	else
	{
		opValues.Add("NOGRUPP");
	}

	//----------------------------2222
	if(m_Grupp.GetCheck() == 1)
	{
		CString olGrupp;
		CString olUrno = "0";
		m_C_Grupp.GetWindowText(olGrupp);
		if(olGrupp.GetLength()>olGrupp.Find("URNO=")+5)
			olUrno = olGrupp.Mid(olGrupp.Find("URNO=")+5);
		opValues.Add(olUrno); //Urno der Planungsgruppe
	}
	else
	{
		opValues.Add("0"); //Urno der Planungsgruppe
	}
	//----------------------------3333
	if(m_Grupp.GetCheck() == 1)
	{
		if(m_Plan2.GetCheck() == 1)
			opValues.Add("2");
		else if(m_Plan3.GetCheck() == 1)
			opValues.Add("3");
		else if(m_Plan4.GetCheck() == 1)
			opValues.Add("4");
		else
			opValues.Add(" ");
	}
	else
	{
		opValues.Add(" ");
	}
	//----------------------------4444
	if(m_R_SortPeriode.GetCheck() == 1)
		opValues.Add("SORT_PERIODE");
	else
		opValues.Add("SORT_DAY");
	//----------------------------5555
	if(m_Check_Sort.GetCheck() == 1)
		opValues.Add("SORT_FVN");
	else
		opValues.Add("SORT_N");

	//----------------------------6666
	opValues.Add(omConfigString);
	return TRUE;
}

//---------------------------------------------------------------------------

void PSDutyRoster4ViewPage::SetData()
{
	SetData(omValues);
	HandleNameRadioButtons();
}

//---------------------------------------------------------------------------

void PSDutyRoster4ViewPage::SetData(CStringArray &opValues)
{
	if(!bmPgpLoaded)
	{	
		POSITION rlPos;
		PGPDATA *prlPgp = NULL;
		CString  olText = "";
		for(rlPos = ogPgpData.omUrnoMap.GetStartPosition(); rlPos != NULL; )
		{
			long llUrno;
			ogPgpData.omUrnoMap.GetNextAssoc(rlPos, (void *&)llUrno, (void *&)prlPgp);
			if(CString(prlPgp->Type) == CString("PFC")) //Zur Zeit nur Funktionen (PFC), sp�ter evtl. l�schen!!!!!!!!!!!
			{
				olText.Format("%-5s %-40s        URNO=%ld",prlPgp->Pgpc, prlPgp->Pgpn, prlPgp->Urno);
				m_C_Grupp.AddString(olText);
			}
		}
		bmPgpLoaded = true;
	}
	bool blGrupp = false;
	//*** Set all adjustments to default ************************************
	m_R_Perc.SetCheck(0);
	m_R_FLName.SetCheck(1);
	m_R_Shnm.SetCheck(0);
	//uhi 12.9.00
	m_Check_Sort.SetCheck(0);

	m_NoGrupp.SetCheck(1);
	m_Grupp.SetCheck(0);
	m_C_Grupp.EnableWindow(FALSE);

	m_R_SortPeriode.SetCheck(1);
	m_R_SortDay.SetCheck(0);

	m_Plan2.SetCheck(0);
	m_Plan3.SetCheck(0);
	m_Plan4.SetCheck(0);

	m_Plan2.EnableWindow(FALSE);
	m_Plan3.EnableWindow(FALSE);
	m_Plan4.EnableWindow(FALSE);
	//*** end> Set all adjustments to default *******************************

	for (int i = 0; i < opValues.GetSize(); i++)
	{
		switch(i)
		{
			case 0:
			{
				if(opValues[i] == "CODE")
				{
					m_R_Perc.SetCheck(1);
					m_R_FLName.SetCheck(0);
					m_R_Shnm.SetCheck(0);
					m_R_Free.SetCheck(0);
				}
 				else if (opValues[i] == "FLNAME")
				{
					m_R_Perc.SetCheck(0);
					m_R_FLName.SetCheck(1);
					m_R_Shnm.SetCheck(0);
					m_R_Free.SetCheck(0);
				}
 				else if (opValues[i] == "SHNM")
				{
					m_R_Perc.SetCheck(0);
					m_R_FLName.SetCheck(0);
					m_R_Shnm.SetCheck(1);
					m_R_Free.SetCheck(0);
				}
 				else if (opValues[i] == "FREE")
				{
					m_R_Perc.SetCheck(0);
					m_R_FLName.SetCheck(0);
					m_R_Shnm.SetCheck(0);
					m_R_Free.SetCheck(1);
				}
 			}
			break;

			case 1:
			{
				if(opValues[i] == "GRUPP")
				{
					m_Grupp.SetCheck(1);
					m_NoGrupp.SetCheck(0);

					blGrupp = true;

					m_C_Grupp.EnableWindow(TRUE);	

					if(ogPrivList.GetStat("DUTYROSTER2PS_IDC_PLAN2") == '1') m_Plan2.EnableWindow(TRUE);
					if(ogPrivList.GetStat("DUTYROSTER2PS_IDC_PLAN3") == '1') m_Plan3.EnableWindow(TRUE);
					if(ogPrivList.GetStat("DUTYROSTER2PS_IDC_PLAN4") == '1') m_Plan4.EnableWindow(TRUE);
				}
 				else
				{
					m_NoGrupp.SetCheck(1);
					m_Grupp.SetCheck(0);
					m_C_Grupp.EnableWindow(FALSE);	
				}
			}
 			break;

			case 2:
			{
				CString olPgpUrno = opValues[i];
				if(olPgpUrno.IsEmpty() == FALSE && blGrupp == true)
				{
					CString olGrupp,olTmp;
					olGrupp.Format("URNO=%s",olPgpUrno);
					int ilCount = m_C_Grupp.GetCount();
					for(int i=0; i<ilCount; i++)
					{
						m_C_Grupp.GetLBText(i,olTmp);
						if((olTmp.Find(olGrupp))!=-1)
						{
							m_C_Grupp.SetCurSel(i);
							break;
						}
					}
				}
				else
				{
					m_C_Grupp.SetCurSel(-1);
				}
			}
			break;

			case 3:
			{
				if(opValues[i] == "2")
				{
					if(ogPrivList.GetStat("DUTYROSTER2PS_IDC_PLAN2") == '1') m_Plan2.SetCheck(1);
					m_Plan3.SetCheck(0);
					m_Plan4.SetCheck(0);
				}
				else if(opValues[i] == "3")
				{
					m_Plan2.SetCheck(0);
					if(ogPrivList.GetStat("DUTYROSTER2PS_IDC_PLAN3") == '1') m_Plan3.SetCheck(1);
					m_Plan4.SetCheck(0);
				}
 				else if(opValues[i] == "4")
				{
					m_Plan2.SetCheck(0);
					m_Plan3.SetCheck(0);
					if(ogPrivList.GetStat("DUTYROSTER2PS_IDC_PLAN4") == '1') m_Plan4.SetCheck(1);
				}
			}
			break;

			case 4:
			{
				if(opValues[i] == "SORT_PERIODE")
				{
					m_R_SortPeriode.SetCheck(1);
					m_R_SortDay.SetCheck(0);
				}
 				else
				{
					m_R_SortPeriode.SetCheck(0);
					m_R_SortDay.SetCheck(1);
				}
 			}
			break;
			
			case 5:
			{
				if(opValues[i] == "SORT_FVN")
					m_Check_Sort.SetCheck(1);
				else
					m_Check_Sort.SetCheck(0);
			}
			break;

			// the name config string
			case 6:
			{
				bool blConfigString = false;
				if (strlen(opValues[i]) && opValues[0] == "FREE")
				{
					omConfigString = opValues[i];
					blConfigString = true;
				}
				else if (strlen(pcgDefaultNameConfig_Duty))
				{
					omConfigString = CString(pcgDefaultNameConfig_Duty);
					blConfigString = true;
				}

				if (blConfigString == true)
				{
					m_R_Perc.SetCheck(0);
					m_R_FLName.SetCheck(0);
					m_R_Shnm.SetCheck(0);
					m_R_Free.SetCheck(1);
				}
			}
			break;			
			
		}
	}
}

//---------------------------------------------------------------------------

void PSDutyRoster4ViewPage::OnGrupp() 
{
	m_C_Grupp.EnableWindow(TRUE);	
	// BDA 8.03.2000 
	// Gruppenansicht selektiert: 
	// um die Situation zu vermeiden, wenn Gruppenansicht aktiviert ist, doch keine
	// Planungsgruppe selektiert ist, mu� die Combobox m_C_Grupp in diesem Moment
	// aufgeklappt und der erste Item selektiert werden
	if(m_C_Grupp.GetCurSel() == CB_ERR && m_C_Grupp.GetCount())
	{
		// Kein Item ist selektiert und die Box ist nicht leer
		m_C_Grupp.SetCurSel(0);
		m_C_Grupp.ShowDropDown(true);
	}
	//m_R_SortPeriode.EnableWindow(FALSE);	
	//m_R_SortPeriode.SetCheck(0);
	//m_R_SortDay.SetCheck(1);

	if(ogPrivList.GetStat("DUTYROSTER2PS_IDC_PLAN2") == '1') m_Plan2.SetCheck(1);
	m_Plan3.SetCheck(0);
	m_Plan4.SetCheck(0);

	if(ogPrivList.GetStat("DUTYROSTER2PS_IDC_PLAN2") == '1') m_Plan2.EnableWindow(TRUE);
	if(ogPrivList.GetStat("DUTYROSTER2PS_IDC_PLAN3") == '1') m_Plan3.EnableWindow(TRUE);
	if(ogPrivList.GetStat("DUTYROSTER2PS_IDC_PLAN4") == '1') m_Plan4.EnableWindow(TRUE);
}

//---------------------------------------------------------------------------

void PSDutyRoster4ViewPage::OnNogrupp() 
{
	m_C_Grupp.EnableWindow(FALSE);
	//m_R_SortPeriode.EnableWindow(TRUE);

	m_Plan2.SetCheck(0);
	m_Plan3.SetCheck(0);
	m_Plan4.SetCheck(0);

	m_Plan2.EnableWindow(FALSE);
	m_Plan3.EnableWindow(FALSE);
	m_Plan4.EnableWindow(FALSE);

}
//---------------------------------------------------------------------------

void PSDutyRoster4ViewPage::SetStatic()
{
	SetDlgItemText(IDC_S_MAName,LoadStg(IDS_S_MAName));
	SetDlgItemText(IDC_S_Sort,LoadStg(IDS_S_SortAnz));

	if("FRA" == ogCCSParam.GetParamValue(ogAppl,"ID_PROD_CUSTOMER"))
	{
		GetDlgItem(IDC_S_Grupp)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_NOGRUPP)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_GRUPP)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_C_GRUPP)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_S_2)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_PLAN2)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_S_3)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_PLAN3)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_S_4)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_PLAN4)->ShowWindow(SW_HIDE);
	}
	else
	{
		SetDlgItemText(IDC_S_Grupp,LoadStg(IDS_S_Grp));
	}
}

void PSDutyRoster4ViewPage::SetButtonText()
{
	SetDlgItemText(IDC_R_FLNAME,LoadStg(SHIFT_SHEET_C_FLNAME));
	SetDlgItemText(IDC_R_SHNM,LoadStg(IDS_STRING1807));
	SetDlgItemText(IDC_R_PERC,LoadStg(SHIFT_SHEET_C_CODE));
	SetDlgItemText(IDC_R_FREE, LoadStg(IDS_STRING1808));
	SetDlgItemText(IDC_NOGRUPP,LoadStg(IDS_S_Keine));
	SetDlgItemText(IDC_GRUPP,LoadStg(IDS_STRING117));
	SetDlgItemText(IDC_PLAN2,LoadStg(IDS_STRING412));
	SetDlgItemText(IDC_PLAN3,LoadStg(IDS_STRING421));
	SetDlgItemText(IDC_PLAN4,LoadStg(IDS_STRING422));
	SetDlgItemText(IDC_R_SORT_PERIODE,LoadStg(IDS_S_angegZeit));
	SetDlgItemText(IDC_R_SORT_DAY,LoadStg(IDS_S_ausgZeit));
	SetDlgItemText(IDC_CHECK_SORT,LoadStg(IDS_CHECK_SORT));
	SetDlgItemText(IDC_B_START_CONFIG, LoadStg(IDS_STRING1795));
}

	// The strings in the CStringArray must be ordered:
	// 1) IDD_NAME_CONFIGURATION
	// 2) IDC_S_FINM
	// 3) IDC_S_LANM
	// 4) IDC_S_FIELD_NAME
	// 5) IDC_S_PERC
	// 6) IDC_S_SHNM
	// 7) IDC_S_NO_CHARS
	// 8) IDC_S_SUFFIX
	// 9) IDC_S_PREVIEW
	//10) IDC_S_PENO
	//NameConfigurationDlg(CStringArray* prpStrings, CString opConfigString, CWnd* pParent = NULL);

void PSDutyRoster4ViewPage::OnBStartConfig() 
{
	pogNameConfigurationDlg->SetConfigString(omConfigString);

	int ilRet = pogNameConfigurationDlg->DoModal();

	if (ilRet == IDOK)
	{
		omConfigString = pogNameConfigurationDlg->GetConfigString();
	}
}

void PSDutyRoster4ViewPage::OnRFlname() 
{
	HandleNameRadioButtons();	
}

void PSDutyRoster4ViewPage::OnRFree() 
{
	HandleNameRadioButtons();
}

void PSDutyRoster4ViewPage::OnRPerc() 
{
	HandleNameRadioButtons();
}

void PSDutyRoster4ViewPage::OnRShnm() 
{
	HandleNameRadioButtons();
}

void PSDutyRoster4ViewPage::HandleNameRadioButtons()
{
	if (m_R_Free.GetCheck() == 1)
	{
		GetDlgItem(IDC_B_START_CONFIG)->EnableWindow(true);
	}
	else
	{
		GetDlgItem(IDC_B_START_CONFIG)->EnableWindow(false);
	}
}
