// BDA 16.02.2000 erstellt aus CedaDrdData.h

#ifndef _CEDARELDATA_H_
#define _CEDARELDATA_H_

#include <stdafx.h>
/*#include "CCSPtrArray.h"
#include <CCSCedaData.h>
#include <ccsddx.h>
#include <ccsglobl.h>*/

/////////////////////////////////////////////////////////////////////////////

// COMMANDS FOR MEMBER-FUNCTIONS
#define REL_SEND_DDX	(true)
#define REL_NO_SEND_DDX	(false)

// Felddimensionen
#define REL_DORS_LEN	(1)
#define REL_SJOB_LEN	(1)


// Struktur eines REL-Datensatzes
struct RELDATA {
	COleDateTime	Cdat 				;	//	Datum der Erstellung
	COleDateTime	Lstu				; 	//	Datum der letzten �nderung
	char			Dors[REL_DORS_LEN+2];	//	Dutyroster or Shiftroster  ('D' / 'S', default = ' ')
	char 			Hopo[HOPO_LEN+2]; 	//	Home Airport
	COleDateTime	Rlat				;	//	Datum der erfolgten Freigabe durch den RELDPL
	COleDateTime	Rlfr 				;	//	Freigabedatum von YYYYMMDD
	COleDateTime	Rlto				;	//	Freigabedatum bis YYYYMMDD
	char 			Rosl[ROSL_LEN+2]; 	//	Freizugebende Planungsstufe
	char			Sjob[REL_SJOB_LEN+2]; 	//	Start Job  ('N' = now / 'L' = later (default))
	long 			Splu 				;	//	Datensatz-Nr. des Schichtplanes (aus SPL)
	long 			Stfu				;	//	Datensatz-Nr. des Mitarbeiters (aus STF)
	long 			Urno 				;	//	Eindeutige Datensatz-Nr.
	char			Usec[USEC_LEN+2]; 	//	Anwender (Ersteller)
	char 			Useu[USEU_LEN+2]; 	//	Anwender (letzte �nderung)

	//DataCreated by this class
	int			IsChanged;	// Check whether Data has Changed f�r Relaese

	// Initialisation
	RELDATA(void)
	{ 
		memset(this,'\0',sizeof(*this));
		Cdat.SetStatus(COleDateTime::invalid);		
		Lstu.SetStatus(COleDateTime::invalid);		
		Rlat.SetStatus(COleDateTime::invalid);		
		Rlfr.SetStatus(COleDateTime::invalid);		
		Rlto.SetStatus(COleDateTime::invalid);		
		IsChanged = DATA_UNCHANGED;
	}
};	





/************************************************************************************************************************
************************************************************************************************************************

  Deklaration CedaRelData: kapselt den Zugriff auf die Tabelle RELTAB

************************************************************************************************************************
************************************************************************************************************************/

class CedaRelData : public CedaData  
{
// Funktionen
public:
    // Konstruktor/Destruktor
	CedaRelData(CString opTableName = "REL", CString opExtName = "TAB");
	~CedaRelData();

// allgemeine Funktionen
	void SetTableName(CString opTableName)
	{
		strcpy(pcmTableName, opTableName.GetBuffer(0));
	}
	// Feldliste lesen
	CString GetFieldList(void) {return CString(pcmListOfFields);}

// Broadcasts empfangen und bearbeiten
	// behandelt die Broadcasts BC_REL_CHANGE,BC_REL_DELETE und BC_REL_NEW
	void ProcessRelBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

// Lesen/Schreiben von Datens�tzen
	// Datens�tze einlesen
	bool Read(char *pspWhere, CMapPtrToPtr *popLoadStfUrnoMap = NULL, bool bpRegisterBC = true, bool bpClearData = true);
	// Datens�tze mit Hilfe selbst-formatierter SQL-String einlesen
	bool ReadSpecialData(CCSPtrArray<RELDATA> *popRelArray,char *pspWhere,char *pspFieldList, char *pcpSort=0,bool ipSYS=true);
	// Datensatz mit bestimmter Urno einlesen
	bool ReadRelByUrno(long lpUrno, RELDATA *prpRel);
	
	// alle RELs eines Mitarbeiters aus der internen Datenhaltung entfernen (ohne aus der DB zu l�schen!!!)
	bool RemoveInternalByStaffUrno(long lpStfUrno, bool bpSendBC /*= false*/);

	// einen Datensatz aus der Datenbank l�schen
	bool Delete(RELDATA *prpRel, BOOL bpWithSave = true);
	// einen neuen Datensatz in der Datenbank speichern
	bool Insert(RELDATA *prpRel, bool bpSave = true);
	// einen ge�nderten Datensatz speichern
	bool Update(RELDATA *prpRel);

// Datens�tze suchen
	// REL mit Urno <lpUrno> suchen
	RELDATA* GetRelByUrno(long llUrno);
	// REL mit dem Prim�rschl�ssel aus Mitarbeiter-Urno (<lpStfu>) suchen
	RELDATA* GetRelByKey(long lpStfu);

	// sucht alle Datens�tze im angegebenen Zeitraum opEnd - opStart raus
	int	GetRelArrayByTime(COleDateTime opStart, COleDateTime opEnd, char* opSortString, CCSPtrArray<RELDATA> *popRelData);
// Zugriff auf interne Daten
	// liefert den Zeiger auf die interne Datenhaltung <omData>
	RELDATA		GetInternalData (int ipIndex) {return omData[ipIndex];}
	// liefert die Anzahl der Datens�tze in der internen Datenhaltung <omData>
	int			GetInternalSize() {return omData.GetSize();}

	// IsValidRel: pr�ft alles, was man pr�fen kann, inklusive g�ltigen Codes und duplizierten RELs
	bool IsValidRel(RELDATA *popRel);
	// Trace Funktion
	void TraceRelData(RELDATA* prpRel);

	// zwei RELs miteinander vergleichen: Ergebnis false -> RELs sind gleich
	bool CompareRelToRel(RELDATA *popRel1, RELDATA *popRel2,bool bpCompareKey = false);
	// kopiert alles, ausser Urno
	void CopyRel(RELDATA* popRelDataSource,RELDATA* popRelDataTarget);

// Manipulation von Datens�tzen
	// RELDATA erzeugen, initialisieren und in die Datenhaltung mit aufnehmen
	RELDATA* CreateRelData(long lpStfUrno);
	// RELDATA erzeugen und initialisieren mit angegebenen Parametern ohne Aufnahme in die interne Datenhaltung
	RELDATA* CreateRelData(char* ppHopo, long lpStfu);

// Daten
	// alle internen Arrays zur Datenhaltung leeren und beim BC-Handler abmelden
	bool ClearAll(bool bpUnregister);
 
protected:	
// Funktionen
// Broadcasts empfangen und bearbeiten
	// beim Broadcast-Handler anmelden
	void Register(void);

// allgemeine Funktionen
	// Tabellenname einstellen
	bool SetTableNameAndExt(CString opTableAndExtName);
	// Feldliste schreiben
	void SetFieldList(CString opFieldList) {strcpy(pcmListOfFields,opFieldList.GetBuffer(0));}

// Datens�tze speichern/bearbeiten

	// speichert einen einzelnen Datensatz
	bool Save(RELDATA *prpRel);
	// wenn es ein neuer Datensatz ist, einen Broadcast REL_NEW abschicken und den Datensatz in die interne 
	// Datenhaltung aufnehmen, sonst return false
	bool InsertInternal(RELDATA *prpRel, bool bpSendDdx);
	// einen Broadcast REL_CHANGE versenden
	bool UpdateInternal(RELDATA *prpRel, bool bpSendDdx = true);
	// einen Datensatz aus der internen Datenhaltung l�schen und einen Broadcast senden
	void DeleteInternal(RELDATA *prpRel, bool bpSendDdx=true);
	// Sortieren 
	void SortBySurnAndTime(CCSPtrArray<RELDATA> *popRelArray);

public:
// Kommunikation mit CEDA
	// Kommandos an CEDA senden (die Funktionen der Basisklasse werden �berschrieben
	// wegen des Feldlisten Bugs)
	bool CedaAction(char *pcpAction, char *pcpTableName, char *pcpFieldList,
					char *pcpSelection, char *pcpSort, char *pcpData, 
					char *pcpDest = "BUF1",bool bpIsWriteAction = false, 
					long lpID = 0, CCSPtrArray<RecordSet> *pomData = NULL, 
					int ipFieldCount = 0);
    bool CedaAction(char *pcpAction, char *pcpSelection = NULL, char *pcpSort = NULL,
					char *pcpData = pcgDataBuf, char *pcpDest = "BUF1", 
					bool bpIsWriteAction = false, long lpID = 0);
protected:
// Daten
	// die geladenen Datens�tze
	CCSPtrArray<RELDATA> omData;
    // Map mit den Datensatz-Urnos der geladenen Datens�tze
	CMapPtrToPtr omUrnoMap;
    // Map mit dem Prim�rschl�ssel (aus STFU) der Datens�tze
	CMapStringToPtr omKeyMap;
	// beim Broadcast-Handler angemeldet?
	bool bmIsBCRegistered;
};

#endif // !defined _CEDARELDATA_H_















