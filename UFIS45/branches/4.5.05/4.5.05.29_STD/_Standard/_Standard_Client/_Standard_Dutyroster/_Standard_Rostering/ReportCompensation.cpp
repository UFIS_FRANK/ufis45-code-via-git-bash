// ReportCompensation.cpp: implementation of the ReportCompensation class.
//
//////////////////////////////////////////////////////////////////////

#include <stdafx.h>
#include <ReportCompensation.h>

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

ReportCompensation::ReportCompensation()
{
	pomView = NULL;
	pomDutyrosterView = NULL;
}

ReportCompensation::ReportCompensation(DutyRoster_View &ropDutyrosterView)
{
	// general initialization
	pomDutyrosterView = &ropDutyrosterView;
	pomView = &ropDutyrosterView.rmViewInfo;

	// getting the timeframe of the night working hours
	CStringArray olArr;
	CString olTmp;
	imNightHoursStart = 1200; /* 20:00 */
	imNightHoursEnd = 360; /* 06:00 */

	olTmp = ogCCSParam.GetParamValue("INITACC","ID_NIGHT_START");
	olArr.RemoveAll();
	if (CedaData::ExtractTextLineFast (olArr, olTmp.GetBuffer (0), ":") == 2)
	{
		imNightHoursStart = atoi(olArr[0].GetBuffer(0))*60 + atoi(olArr[1].GetBuffer(0));
	}

	olArr.RemoveAll();
	olTmp = ogCCSParam.GetParamValue("INITACC","ID_NIGHT_END");
	if (CedaData::ExtractTextLineFast (olArr, olTmp.GetBuffer (0), ":") == 2)
	{
		imNightHoursEnd = atoi(olArr[0].GetBuffer(0))*60 + atoi(olArr[1].GetBuffer(0));
	}
}

ReportCompensation::~ReportCompensation()
{

}

BOOL ReportCompensation::GeneratePrintFile(const CString& ropFileName,CString RosterLevel)
{
	if (pomView == NULL)
		return FALSE;

	if (ropFileName.GetLength() == 0)
		return FALSE;

	std::ofstream of(ropFileName);

	GenerateCommon(of);

	GenerateData(of,RosterLevel);

	of.close();
	return TRUE;
}

void ReportCompensation::GenerateCommon(std::ofstream& of)
{
	CStringArray olArr;
	CString olTmp;
	ORGDATA *prlOrgData = NULL;
	PFCDATA *prlPfcData = NULL;
	int i;

	//--- writing the name of the report to print
	olTmp = "RPT_COMPENSATION";
	of << "<=NAME=>" << (const char *)olTmp << "<=\\=>" << '\n';

	//--- getting valid from, e.g. "01.04.2002"
	olTmp = pomView->oDateFrom.Format("%d.%m.%Y");
	of << "<=FROM=>" << (const char *)olTmp << "<=\\=>" << '\n';

	//--- getting valid to, e.g. "28.04.2002"
	olTmp = pomView->oDateTo.Format("%d.%m.%Y");
	of << "<=TO=>" << (const char *)olTmp << "<=\\=>" << '\n';

	//--- writing the caption of the report to print
	olTmp = LoadStg(IDS_STRING156);
	of << "<=TITLE=>" << (const char *)olTmp << "<=\\=>" << '\n';

	//--- writing the OrgUnits
	olTmp = pomView->oOrgStatUrnos;
	CedaData::ExtractTextLineFast (olArr, olTmp.GetBuffer (0), ",");
	olTmp = "";
	for (i = 0; i < olArr.GetSize(); i++)
	{
		prlOrgData = ogOrgData.GetOrgByUrno(atol(olArr[i]));
		if (prlOrgData != NULL)
		{
			if (olTmp.GetLength())
			{
				olTmp += ",";
			}
			olTmp += CString(prlOrgData->Dpt1);
		}
	}
	of << "<=ORG_CODES=>" << (const char *)olTmp << "<=\\=>" << '\n';


	//--- writing the OrgUnits
	olTmp = pomView->oPfcStatUrnos;
	CedaData::ExtractTextLineFast (olArr, olTmp.GetBuffer (0), ",");
	olTmp = "";
	for (i = 0; i < olArr.GetSize(); i++)
	{
		prlPfcData = ogPfcData.GetPfcByUrno(atol(olArr[i]));
		if (prlPfcData != NULL)
		{
			if (olTmp.GetLength())
			{
				olTmp += ",";
			}
			olTmp += CString(prlPfcData->Fctc);
		}
	}
	of << "<=PFC_CODES=>" << (const char *)olTmp << "<=\\=>" << '\n';

	//--- getting only the holidays, not weekend
	COleDateTime olLookupDate = pomView->oDateFrom;
	CString olLookupDateString;
	void  *prlVoid = NULL;
	olTmp = "";
	while (olLookupDate <= pomView->oDateTo)
	{
		olLookupDateString = olLookupDate.Format("%Y%m%d000000");
		if (pomDutyrosterView->pomIsTabViewer->omHolidayKeyMap.Lookup(olLookupDateString, (void *&)prlVoid) == TRUE)
		{
			olTmp += "|" + olLookupDate.Format("%d.%m.%Y");
		}
		olLookupDate += COleDateTimeSpan(1, 0, 0, 0);
	}
	if (olTmp.GetLength() > 0)
	{
		olTmp = olTmp.Mid(1);
	}
	of << "<=HOLDAYS=>" << (const char *)olTmp << "<=\\=>" << '\n';
}

void ReportCompensation::GenerateData(std::ofstream& of ,CString RosterLevel)
{
	CStringArray olArr;
	CString olTmp;
	CString olTmpLine1; // containing the night-shift hours
	CString olTmpLine2; // containing the working hours on Sundays/Holidays
	CString olSday;
	double dlTotalSum1 = 0.0;
	double dlTotalSum2 = 0.0;
	double dlValue1 = 0.0;
	double dlValue2 = 0.0;
	COleDateTime olFrom;
	COleDateTime olTo;
	bool blNextDay1 = false;
	bool blNextDay2 = false;

	int i;
	int ilSblu = 0;

	STFDATA *prlStfData = NULL;
	DRRDATA *prlDrrData1 = NULL;
	DRRDATA *prlDrrData2 = NULL;

	for (i = 0; i < pomDutyrosterView->omStfData.GetSize(); i++)
	{
		dlTotalSum1 = 0.0;
		dlTotalSum2 = 0.0;
		dlValue1 = 0.0;
		dlValue2 = 0.0;
		blNextDay1 = false;
		blNextDay2 = false;

		// getting the general data of the employee
		prlStfData = &pomDutyrosterView->omStfData[i];
		olTmp.Format("%d", i+1);
		olTmpLine1 = "<=" + olTmp + "_1=>" + prlStfData->Peno + "|" + prlStfData->Lanm + ", " + prlStfData->Finm + "|";
		olTmpLine2 = "<=" + olTmp + "_2=>" + prlStfData->Peno + "|" + prlStfData->Lanm + ", " + prlStfData->Finm + "|";
		olTmpLine1 = olTmpLine1 + "N" + "|";
		olTmpLine2 = olTmpLine2 + "A" + "|";

		// getting the data from the shifts starting on the day before!
		COleDateTime olDayBefore = pomView->oDateFrom -  COleDateTimeSpan(1,0,0,0);
		olSday = olDayBefore.Format("%Y%m%d");
		//prlDrrData1 = ogDrrData.GetActiveDrr(olSday, prlStfData->Urno); // delivers always ROSS=A,DRRN=1 !
		prlDrrData1 = ogDrrData.GetDrrByKey(olSday, prlStfData->Urno,"1",RosterLevel);
		if (prlDrrData1 != NULL )
		{
			prlDrrData2 = ogDrrData.GetDrrByKey(olSday, prlStfData->Urno,"2",prlDrrData1->Rosl);
			if (UseShift(prlDrrData1) == true)
			{
				olFrom = prlDrrData1->Avfr;
				olTo = prlDrrData1->Avto;
				if (olFrom.GetDay() != olTo.GetDay())
				{
					blNextDay1 = true;
				}
			}

			if (prlDrrData2 != NULL && UseShift(prlDrrData2) == true)
			{
				olFrom = prlDrrData2->Avfr;
				olTo = prlDrrData2->Avto;
				if (olFrom.GetDay() != olTo.GetDay())
				{
					blNextDay2 = true;
				}
			}

			// init values for the next loop on the next day
			if (blNextDay1 == true)
			{
				ilSblu = atoi(prlDrrData1->Sblu);
				olTo = prlDrrData1->Avto - COleDateTimeSpan(0,0,ilSblu,0);
				olFrom = olTo;
				olFrom.SetDateTime(olTo.GetYear(),olTo.GetMonth(),olTo.GetDay(),0,0,0);
				if ((olTo - olFrom).GetTotalMinutes() > 0)
				{
					dlValue1 += GetNightWorkingHours(&olFrom, &olTo);
					dlValue2 += GetSundayHolidayWorkingHours(&olFrom, &olTo);
				}
			}
			if (blNextDay2 == true)
			{
				ilSblu = atoi(prlDrrData2->Sblu);
				olTo = prlDrrData2->Avto - COleDateTimeSpan(0,0,ilSblu,0);
				olFrom = olTo;
				olFrom.SetDateTime(olTo.GetYear(),olTo.GetMonth(),olTo.GetDay(),0,0,0);
				if ((olTo - olFrom).GetTotalMinutes() > 0)
				{
					dlValue1 += GetNightWorkingHours(&olFrom, &olTo);
					dlValue2 += GetSundayHolidayWorkingHours(&olFrom, &olTo);
				}
			}
		}

		// getting the shifts of all days of this employee
		for (COleDateTime olDate = pomView->oDateFrom; olDate <= pomView->oDateTo; olDate += COleDateTimeSpan(1,0,0,0))
		{
			blNextDay1 = false;
			blNextDay2 = false;
			olSday = olDate.Format("%Y%m%d");

			//prlDrrData1 = ogDrrData.GetActiveDrr(olSday, prlStfData->Urno); // delivers always ROSS=A,DRRN=1 !
			prlDrrData1 = ogDrrData.GetDrrByKey(olSday, prlStfData->Urno,"1",RosterLevel);
			if (prlDrrData1 != NULL )
			{
				prlDrrData2 = ogDrrData.GetDrrByKey(olSday, prlStfData->Urno,"2",prlDrrData1->Rosl);

				if (UseShift(prlDrrData1) == true)
				{
					ilSblu = atoi(prlDrrData1->Sblu);
					olFrom = prlDrrData1->Avfr;
					olTo = prlDrrData1->Avto - COleDateTimeSpan(0,0,ilSblu,0);

					if (olFrom.GetDay() != olTo.GetDay())
					{
						olTo.SetDateTime(olTo.GetYear(),olTo.GetMonth(),olTo.GetDay(),0,0,0);
						blNextDay1 = true;
					}
					// values of the first shift
					dlValue1 += GetNightWorkingHours(&olFrom, &olTo);
					dlValue2 += GetSundayHolidayWorkingHours(&olFrom, &olTo);
				}

				if (prlDrrData2 != NULL && UseShift(prlDrrData2) == true)
				{
					ilSblu = atoi(prlDrrData2->Sblu);
					olFrom = prlDrrData2->Avfr;
					olTo = prlDrrData2->Avto - COleDateTimeSpan(0,0,ilSblu,0);

					if (olFrom.GetDay() != olTo.GetDay())
					{
						olTo.SetDateTime(olTo.GetYear(),olTo.GetMonth(),olTo.GetDay(),0,0,0);
						blNextDay2 = true;
					}
					// values of the first shift
					dlValue1 += GetNightWorkingHours(&olFrom, &olTo);
					dlValue2 += GetSundayHolidayWorkingHours(&olFrom, &olTo);
				}


				// add the values to the lines
				if (dlValue1 > 0.0)
				{
					olTmp.Format("%.2f", dlValue1);
				}
				else
				{
					olTmp = "";
				}
				olTmpLine1 = olTmpLine1 + olTmp + "|";
				if (dlValue2 > 0.0)
				{
					olTmp.Format("%.2f", dlValue2);
				}
				else
				{
					olTmp = "";
				}
				olTmpLine2 = olTmpLine2 + olTmp + "|";

				// calculate the total sum
				dlTotalSum1 += dlValue1;
				dlTotalSum2 += dlValue2;

				// init values for the next loop on the next day
				dlValue1 = 0.0;
				dlValue2 = 0.0;
				if (blNextDay1 == true)
				{
					ilSblu = atoi(prlDrrData1->Sblu);
					olTo = prlDrrData1->Avto - COleDateTimeSpan(0,0,ilSblu,0);
					olFrom = olTo;
					olFrom.SetDateTime(olTo.GetYear(),olTo.GetMonth(),olTo.GetDay(),0,0,0);

					if ((olTo - olFrom).GetTotalMinutes() > 0)
					{
						dlValue1 += GetNightWorkingHours(&olFrom, &olTo);
						dlValue2 += GetSundayHolidayWorkingHours(&olFrom, &olTo);
					}
				}
				if (blNextDay2 == true)
				{
					ilSblu = atoi(prlDrrData2->Sblu);
					olTo = prlDrrData2->Avto - COleDateTimeSpan(0,0,ilSblu,0);
					olFrom = olTo;
					olFrom.SetDateTime(olTo.GetYear(),olTo.GetMonth(),olTo.GetDay(),0,0,0);
					if ((olTo - olFrom).GetTotalMinutes() > 0)
					{
						dlValue1 += GetNightWorkingHours(&olFrom, &olTo);
						dlValue2 += GetSundayHolidayWorkingHours(&olFrom, &olTo);
					}
				}
			}
			else
			{
				olTmpLine1 = olTmpLine1 + "|";
				olTmpLine2 = olTmpLine2 + "|";
			}
		}

		// adding the total sums
		olTmp.Format("%.2f", dlTotalSum1);
		olTmpLine1 += olTmp;
		olTmp.Format("%.2f", dlTotalSum2);
		olTmpLine2 += olTmp;

		// writing the lines into the file
		of << (const char *)olTmpLine1 << "<=\\=>" << '\n';
		of << (const char *)olTmpLine2 << "<=\\=>" << '\n';
	}
}

double ReportCompensation::GetNightWorkingHours(COleDateTime *popFrom,COleDateTime *popTo)
{
	double dlRet = 0.0;

	int ilStartMin = popFrom->GetHour()*60 + popFrom->GetMinute();
	int ilEndMin = popTo->GetHour()*60 + popTo->GetMinute();
	if (ilEndMin == 0)
	{
		ilEndMin = 1440; // it's 24:00
	}
	int ilDiffTime = 0;

	if (ilStartMin < ilEndMin) // start and end time are on the same day
	{
		if (ilStartMin < imNightHoursEnd)
		{
			if (ilEndMin <= imNightHoursEnd)
			{
				// complete shift between 00:00 and 06:00
				return ((double)(ilEndMin - ilStartMin) / 60);
			}
			else
			{
				// calculate night-minutes in the morning
				ilDiffTime += imNightHoursEnd - ilStartMin;
			}
		}
		else if (ilStartMin >= imNightHoursStart)
		{
			// Complete shift between 22:00 and 24:00
			return  ((double)(ilEndMin - ilStartMin) / 60);
		}

		if (ilEndMin > imNightHoursStart)
		{
			// there is a part of the shift between 22:00 and 24:00
			ilDiffTime += ilEndMin - imNightHoursStart;//imNightHoursEnd - ilStartMin;
		}

		return ((double)ilDiffTime / 60);
	}

	return dlRet;
}

double ReportCompensation::GetSundayHolidayWorkingHours(COleDateTime *popFrom,COleDateTime *popTo)
{
	int ilStartMin = popFrom->GetHour()*60 + popFrom->GetMinute();
	int ilEndMin = popTo->GetHour()*60 + popTo->GetMinute();
	if (ilEndMin == 0)
	{
		ilEndMin = 1440; // it's 24:00
	}

	// is it on a holiday?
	void *prlVoid = NULL;
	CString olTmp = popFrom->Format("%Y%m%d000000");
	if (pomDutyrosterView->pomIsTabViewer->omHolidayKeyMap.Lookup(olTmp, (void *&)prlVoid) == TRUE)
	{
		return ((double)(ilEndMin-ilStartMin) / 60);
	}

	// is it on a Sunday?
	if (popFrom->GetDayOfWeek() == 1)
	{
		return ((double)(ilEndMin-ilStartMin) / 60);
	}

	return 0.0;
}

bool ReportCompensation::UseShift(DRRDATA *popDrrData)
{
	if (strlen(popDrrData->Scod) == 0)
	{
		// it is a "deleted" record: no shift code -> no hours
		return false;
	}

	ODADATA *prlOdaData = NULL;
	prlOdaData = ogOdaData.GetOdaBySdac (popDrrData->Scod);
	if (prlOdaData != NULL)
	{
		if (strcmp(prlOdaData->Work,"1") == NULL || strcmp(prlOdaData->Tbsd,"1") == NULL)
		{
			return  true;
		}
		else
		{
			// it is an absence and not treated like a shift
			return false;
		}
	}

	return true;
}