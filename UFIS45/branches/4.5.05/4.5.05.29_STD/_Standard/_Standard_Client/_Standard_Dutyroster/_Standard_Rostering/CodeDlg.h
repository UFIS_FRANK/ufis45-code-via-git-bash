#if !defined(AFX_CODE_H__435D8F21_784E_11D1_B434_0000B45A33F5__INCLUDED_)
#define AFX_CODE_H__435D8F21_784E_11D1_B434_0000B45A33F5__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER >= 1000
// GridDlg.h : header file
//
#define COLCOUNT_CODEDLG 2
#define COLCOUNT_BSD 10
#define COLCOUNT_ODA 2
#define COLCOUNT_WIS 2

// DnD
#include <CCSDragDropCtrl.h>
//Schichten
#include <cedabsddata.h>

class CGridControl;

/////////////////////////////////////////////////////////////////////////////
// CCodeDlg dialog

class CCodeDlg : public CDialog
{
// Construction
public:
	CCodeDlg( UINT nIDTemplate, CWnd* pParent = NULL);   // standard constructor
	~CCodeDlg();

	void ChangeValidDates(COleDateTime opFrom,COleDateTime opTo);
	void SetOrgCodes(CString opOrgList);
	void SetPfcCodes(CString opPfcList);
	void ReloadAndShow(bool bpAllCodes);
	void SetCaptionInfo(CString opInfo);
	void SetShowWish(bool bpShowWish);
	int  GetIDD() {return m_IDD;};

// Dialog Data
	//{{AFX_DATA(CCodeDlg)
	CButton	m_OK;
	CButton	m_B_FindODA;
	CButton	m_B_FindBSD;
	CButton	m_B_FindWIS;
	//}}AFX_DATA

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCodeDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Grid initialisieren
	void IniGrid ();
	void IniColWidths();
	void FillWishGrid(bool bpAllCodes=false);
	void FillBsdGrid(bool bpAllCodes=false);
	bool SortGrid(CGridControl* popGrid,int ipRow);
	void FillOdaGrid();

	int m_IDD;

	// Das Grid
	CGridControl	*pomBsdGrid;
	CGridControl	*pomOdaGrid;
	CGridControl	*pomWisGrid;

	// Mu� das Grid neu gef�llt werden
	bool bmIsDirty;

	// Alle Codes m�ssen gezeigt werden
	bool bmAllCodes;

	// Gibt an ob das Grid mit den W�nschen angezeigt werden soll
	bool bmShowWish;

	// String mit allen Org-Codes die angezeigt werden sollen
	CString omOrgList;

	// String mit allen Pfc-Codes die angezeigt werden sollen
	CString omPfcList;

	// Map f�r die Schichtcodes
	CMapStringToString omBsdMap;

	// Objekt f�r D'n'D-Funktionalit�t
	CCSDragDropCtrl omDragDropObject;

	// Von / Bis G�ltigkeit der BSD Schichten
	COleDateTime omFromTime,omToTime;

	// DnD starten
	void OnDragBegin(CString opUrno,CString opCode,CString opSource);

	// Pr�ft ob eine Schicht angezeigt werden soll
	bool CheckBsd(BSDDATA* popBsd, COleDateTime opFrom,COleDateTime opTo, bool bpAllCodes);
	// Pr�fen ob der Extrabutton in der Titelleiste getroffen wurde
	bool IsButtonHit(CPoint point);
	
	// Kopieren der BSD Codes in eine StringMap
	void PrepareBsdCodes(bool bpAllCodes);


	// Generated message map functions
	//{{AFX_MSG(CCodeDlg)
	virtual void OnOK();
	virtual void OnCancel();
	virtual BOOL OnInitDialog();
    afx_msg LONG OnGridDragBegin(WPARAM wParam, LPARAM lParam);
	afx_msg void On_B_FindBSD();
	afx_msg void On_B_FindODA();
	afx_msg void On_B_FindWIS();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnNcLButtonDown( UINT nHitTest, CPoint point );
	afx_msg void OnNcPaint();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CODE_H__435D8F21_784E_11D1_B434_0000B45A33F5__INCLUDED_)
