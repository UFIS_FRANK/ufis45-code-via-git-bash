// TageslisteDlg.cpp : implementation file
// 

#include <stdafx.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CTageslisteDlg dialog

CTageslisteDlg::CTageslisteDlg(bool bpReadOnly,DRRDATA *popDrr,CCSPtrArray<STFDATA>* popStfData, CWnd* pParent /*=NULL*/)
: CDialog(CTageslisteDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CTageslisteDlg)
	//}}AFX_DATA_INIT
	CCS_TRY;
	// Daten initialisieren
	pomStfData = popStfData;
	pomDrr = popDrr;
	bmReadOnly = bpReadOnly;
	bmSetDrsActive = false;
	imTbsdType = CODE_UNDEFINED;
	bmWork = false;

	bmChangedDrg = false;

	pomViewInfo = &((DutyRoster_View*)m_pParentWnd)->rmViewInfo;

	if (pomDrr!=NULL)
	{
		lmUrno = pomDrr->Urno;
		lmShiftUrno = pomDrr->Bsdu;
		omFctc = pomDrr->Fctc;
		imScodType = ogOdaData.IsODAAndIsRegularFree(CString(pomDrr->Scod), &imTbsdType, &bmWork);
	}
	else
	{
		lmUrno = -1;
		imScodType = CODE_UNDEFINED;
		lmShiftUrno = 0;
		omFctc = "";
	}

	// beim BC-Handler anmelden
	Register(true);

	// DRD's m�ssen nicht gespeichert werden
	bmDrdIsDirty = false;

	CCS_CATCH_ALL;
}

//*********************************************************************************
// Destruktor
//*********************************************************************************

CTageslisteDlg::~CTageslisteDlg()
{
	CCS_TRY;
	// DRD-Map leeren
	omRefDrdMap.RemoveAll();
	
	//------------------------------------------------------------------------
	// tempor�re Drdmap l�schen
	// Es d�rfen nur die Drd gel�scht werden, deren Zeiger nicht in die
	// interne Datenhaltung �bergeben wurden
	POSITION rlPos; 
	long llTempUrno; 
	DRDDATA *prlTempDrd; 

	for (rlPos = omTempDrdMap.GetStartPosition(); rlPos != NULL;)
	{
		// DRD ermitteln
		omTempDrdMap.GetNextAssoc(rlPos,(void *&)llTempUrno, (void *&)prlTempDrd);
		
		// Drd markieren, damit er sp�ter NICHT gel�scht wird
		if (prlTempDrd->IsChanged != DATA_NEW)
		{
			delete prlTempDrd;
		}
	}
	// DRD-Map leeren
	omTempDrdMap.RemoveAll();
	//------------------------------------------------------------------------
	
	// DRA-Map leeren
	omRefDraMap.RemoveAll();
	//------------------------------------------------------------------------
	// tempor�re Drdmap l�schen
	// Es d�rfen nur die Drd gel�scht werden, deren Zeiger nicht in die
	// interne Datenhaltung �bergeben wurden
	DRADATA *prlTempDra; 
	for (rlPos = omTempDraMap.GetStartPosition(); rlPos != NULL;)
	{
		// DRA ermitteln
		omTempDraMap.GetNextAssoc(rlPos,(void *&)llTempUrno, (void *&)prlTempDra);

		// Drd markieren, damit er sp�ter NICHT gel�scht wird
		if (prlTempDra->IsChanged != DATA_NEW)
		{
			delete prlTempDra;
		}
	}
	// DRA-Map leeren
	omTempDraMap.RemoveAll();
	//------------------------------------------------------------------------

	// Dev Grid l�schen
	if (pomDevGrid != NULL)
	{
		delete pomDevGrid;
	}

	// Absence Grid l�schen
	if (pomAbsenceGrid != NULL)
	{
		delete pomAbsenceGrid;
	}

	// beim BC-Handler abmelden
	Register(false);
	CCS_CATCH_ALL;	
}

//*********************************************************************************
//
//*********************************************************************************

void CTageslisteDlg::DoDataExchange(CDataExchange* pDX)
{
	CCS_TRY;
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CTageslisteDlg)
	DDX_Control(pDX, IDC_PERC_LISTBOX,			m_Perc_Listbox);
	DDX_Control(pDX, IDC_SHIFT_COMBOBOX,		m_CB_ShiftNumber);
	DDX_Control(pDX, IDC_C_ACTUAL_SHIFT_FUNC,	m_C_ActualShiftFunc);
	DDX_Control(pDX, IDC_BUFU,					m_Bufu);
	DDX_Control(pDX, IDC_E_SCOD,				m_Scod);
	DDX_Control(pDX, IDC_AVFR_DATE,				m_Avfr_Date);
	DDX_Control(pDX, IDC_AVFR_TIME,				m_Avfr_Time);
	DDX_Control(pDX, IDC_AVTO_DATE,				m_Avto_Date);
	DDX_Control(pDX, IDC_AVTO_TIME,				m_Avto_Time);
	DDX_Control(pDX, IDC_SBFR_TIME,				m_Sbfr_Time);
	DDX_Control(pDX, IDC_SBTO_TIME,				m_Sbto_Time);
	DDX_Control(pDX, IDC_SBLU,					m_Sblu);
	DDX_Control(pDX, IDC_REMARK,				m_Remark);
	DDX_Control(pDX, IDC_E_ZCODE,				m_E_ZCode1);
	DDX_Control(pDX, IDC_E_ZCODE2,				m_E_ZCode3);
	DDX_Control(pDX, IDC_E_ZCODE3,				m_E_ZCode4);
	DDX_Control(pDX, IDC_BUTTONINFO,			m_B_Info);
	DDX_Control(pDX, IDC_C_SAP,					m_C_SAP);
	DDX_Control(pDX, IDC_C_SUB1SUB2,			m_C_SUB1SUB2);
	DDX_Control(pDX, IDC_C_GROUP,				m_C_Group);
	DDX_Control(pDX, IDC_BUTTON_REPAIRED,		m_B_Repaired);
	//}}AFX_DATA_MAP
	CCS_CATCH_ALL;
}

//*********************************************************************************
//
//*********************************************************************************

BEGIN_MESSAGE_MAP(CTageslisteDlg, CDialog)
//{{AFX_MSG_MAP(CTageslisteDlg)
ON_WM_CTLCOLOR()
ON_BN_CLICKED(IDC_B_SHIFTIS, OnSetShiftIs)
ON_BN_CLICKED(IDC_D_INSERTDRD, On_Insert_Dev)
ON_BN_CLICKED(IDC_D_DELETEDRD, On_Delete_Dev)
ON_BN_CLICKED(IDC_D_EDITDRD, On_Edit_Dev)
ON_BN_CLICKED(IDC_D_DELETEDRA, On_Absence_Delete)
ON_BN_CLICKED(IDC_D_EDITDRA, On_Absence_Edit)
ON_BN_CLICKED(IDC_D_INSERTDRA, On_Absence_Insert)
ON_BN_CLICKED(IDC_BUTTONINFO, OnButtoninfo)
ON_BN_CLICKED(IDC_B_CREATEDTOUR, OnInsertDoubleTour)
ON_BN_CLICKED(IDC_B_CANCELDOUBLE, OnCancelDoubleTour)
ON_CBN_SELCHANGE(IDC_SHIFT_COMBOBOX, OnSelchangeShiftCombobox)
ON_EN_UPDATE(IDC_E_ZCODE2, OnUpdateEZcode1)
ON_EN_UPDATE(IDC_E_ZCODE, OnUpdateEZcode3)
ON_EN_UPDATE(IDC_E_ZCODE3, OnUpdateEZcode4)
ON_MESSAGE(WM_EDIT_KILLFOCUS, OnKillfocus)
ON_MESSAGE(WM_EDIT_CHANGED, OnEditChanged)
ON_MESSAGE(WM_GRID_LBUTTONDOWN,	OnGridLButton)
ON_CBN_SELCHANGE(IDC_C_SUB1SUB2, OnSelchangeSub1Sub2)
ON_BN_CLICKED(IDC_BUTTON_REPAIRED, OnButtonRepaired)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CTageslisteDlg message handlers

//*********************************************************************************
//
//*********************************************************************************

BOOL CTageslisteDlg::OnInitDialog() 
{
	CCS_TRY;
	CDialog::OnInitDialog();

	bmChangedSub1Sub2 = false;

	// Readonly-> alles disablen.
	if (bmReadOnly)
	{
		ShowControls(false);
	}
	else
	{
		ShowControlsSelective();
	}

	omReadErrTxt   = LoadStg(ST_READERR);
	omInsertErrTxt = LoadStg(ST_INSERTERR);
	omUpdateErrTxt = LoadStg(ST_UPDATEERR);
	omDeleteErrTxt = LoadStg(ST_DELETEERR);
	omHinweis      = LoadStg( ST_HINWEIS );
	omFehler       = LoadStg( ST_FEHLER );
	
	//-------------------------------------------------------------------
	// Titel "zusammenbasteln"
	omCaption = LoadStg(IDS_STRING1852);

	CString olSday = pomDrr->Sday;
	if (olSday.GetLength() > 7)
	{
		omCaption += ", ";

		COleDateTime olShiftDay(atoi(olSday.Left(4)), atoi(olSday.Mid(4,2)), atoi(olSday.Mid(6,2)),0,0,0);

		omCaption += olShiftDay.Format("%d.%m.%Y");
	}

	omCaption += ", ";
	omCaption += ogDrrData.GetTypeName(pomDrr->Rosl);

	omCaption += " ";
	if (strcmp(pomDrr->Ross,"A") == 0)
	{
		omCaption += LoadStg(IDS_STRING1853);
	}
	else
	{
		omCaption += LoadStg(IDS_STRING1854);
	}
	SetWindowText(omCaption);
	//-------------------------------------------------------------------
	
	// Static Elemente setzen (Ohne Bsd Felder)
	SetStatic();

	// Editfelder initialisieren (Ohne Bsd Felder)
	SetEdit();

	// Alle Daten im Zusammenhang mit den DRS setzen
	SetDrsItems();
	
	// Buttons "beschriften"
	SetButtonText();

	int ilScooType = ogOdaData.IsODAAndIsRegularFree(CString(pomDrr->Scoo));
	SetShiftFields(BASIC_SHIFT | (ilScooType == CODE_IS_BSD ? BSD_DATA : ODA_DATA), pomDrr->Scoo);

	SetShiftFields(IS_SHIFT | DRR_DATA, pomDrr->Scod);

	// for ZRH,GVA and BSL we have not to show the times of the attendance recorder
	CString olCustomer = ogCCSParam.GetParamValue(ogAppl,"ID_PROD_CUSTOMER");
	CString olHide = CString("ZRH,GVA,BSL");
	int ilTbsd  = 0;
	bool blWork = false;
	if (olHide.Find (olCustomer) > -1)
	{
		HideSpr();
	}
	else if (ogOdaData.IsODAAndIsRegularFree(pomDrr->Scod,&ilTbsd,&blWork) == CODE_IS_BSD || (ilTbsd == CODE_IS_ODA_TIMEBASED || blWork == true))
	{
		SetShiftFields(AZE_DATA, pomDrr->Scod);
	}

	//----------------------------------------------------------------------------------

	// Freigabe an SAP setzen
	if (strcmp(pomDrr->Expf, "A") == 0)
	{
		m_C_SAP.SetCheck(1);
	}
	else
	{
		m_C_SAP.SetCheck(0);
	}

	// Combo-Box mit den DRR-Nummern initialisieren
	InitDrrnComboBox();
	InitFctcComboBox();
	InitSub1Sub2ComboBox();

	// Abweichungs Grid initialisieren
	InitDevGrid ();

	// Abwesenheits Grid initialisieren
	InitAbsenceGrid ();

	// Listbox Qualifikationen f�llen
	m_Perc_Listbox.SetBkColor(COLORREF(RGB(192,192,192)));
	m_Perc_Listbox.SetTextBkColor(COLORREF(RGB(192,192,192)));
	m_Perc_Listbox.SetFont(&ogCourier_Regular_8);

	PERDATA* polPerData;

	int ilSpe = ogSpeData.omData.GetSize();
	for (int i = 0; i < ilSpe; i++)
	{
		SPEDATA *prlSpe = &ogSpeData.omData[i];
		// Geh�rt diese Quali zum aktuellen Mitarbeiter 
		if (prlSpe->Surn == pomDrr->Stfu)
		{
			polPerData = ogPerData.GetPerByCode(prlSpe->Code);
			if (polPerData != NULL)
			{
				char pclTmp[50];
				// Eintrag muss mit anderem als Leerzeichen enden, da sonst automatisch
				// die letzten Zeichen durch '...' abgeschnitten werden
				sprintf( pclTmp, "%-5.5s %-43.43s.", polPerData->Prmc, polPerData->Prmn);
				m_Perc_Listbox.InsertItem(0,pclTmp);
			}
		}
	}

	if (!bmReadOnly)
	{
		// Doppeltour-Trennen Button nur bei vorhandener Doppeltour aktivieren
		if (ogDrrData.HasDoubleTour(pomDrr))
		{
			((CButton*) GetDlgItem(IDC_B_CREATEDTOUR))->EnableWindow(false);
			((CButton*) GetDlgItem(IDC_B_CANCELDOUBLE))->EnableWindow(true);
		}
		else
		{
			// Nur Anwesenheitschichten sind erlaubt.
			if (ogBsdData.IsBsdCode(pomDrr->Scod))
			{
				((CButton*) GetDlgItem(IDC_B_CREATEDTOUR))->EnableWindow(true);
			}
			else
			{
				((CButton*) GetDlgItem(IDC_B_CREATEDTOUR))->EnableWindow(false);
			}
			((CButton*) GetDlgItem(IDC_B_CANCELDOUBLE))->EnableWindow(false);
		}
	}
	
	// Arbeitsgruppenliste und Funktionenliste f�llen
	// DRG pr�fen, wenn vorhanden, die Arbeitsgruppe und Funktion selektieren, sonst leer
	FillGroupAndFunctionLists();

	// Schichtcode retten
	lmOldShiftUrno = pomDrr->Bsdu;

	// Funktion retten
	omOldFctc = pomDrr->Fctc;

	//-------------------------------------------------------------------------
	// alle relevanten DRDs ermitteln die bisher in der Datenbank sind.
	// F�r jeden bisher vorhandenen DRD wird ein neuer DRD erzeugt
	CreateTempDrdMap();
	// Grid neu f�llen
	FillDevGrid();
	//-------------------------------------------------------------------------
	// alle relevanten DRAs ermitteln die bisher in der Datenbank sind.
	// F�r jeden bisher vorhandenen DRA wird ein neuer DRA erzeugt
	CreateTempDraMap(false);

	// Grid neu f�llen
	FillAbsenceGrid();

	// Check if Repaired button must be enabled
	if (this->pomDrr != NULL && strlen(this->pomDrr->Repaired) > 0)
	{
		this->m_B_Repaired.ShowWindow(SW_SHOW);
		this->m_B_Repaired.EnableWindow(TRUE);
		this->m_B_Repaired.SetColors(RED,::GetSysColor(COLOR_BTNSHADOW),::GetSysColor(COLOR_BTNHIGHLIGHT));
	}

	return TRUE;  
	CCS_CATCH_ALL;
	return FALSE;  
}

//************************************************************************************
// Register: beim Broadcast-Handler anmelden (<bpUnregister> = false)
//	oder anmelden (<bpUnregister> = true).
// R�ckgabe: keine
//************************************************************************************

void CTageslisteDlg::Register(bool bpRegister /*=true*/)
{
	CCS_TRY;
	if (!bpRegister)
	{
		// abmelden
		ogDdx.UnRegister(this,NOTUSED);
	}
	CCS_CATCH_ALL;
}

/*************************************************************************************
Nur wenn die Basisschicht keine regul�re Funktion hat kann die Funktion der Schicht 
ge�ndert werden, egal woher die zugewiesene Funktion kommt.
Auch wenn ODA, ist keine Funktion m�glich
*************************************************************************************/
void CTageslisteDlg::SetFctcAndFields()
{
	CString olText;
	BSDDATA* polBsd = 0;
	if (imScodType != CODE_IS_BSD)
	{
		omFctc = "";
	}
	else
	{
		if (pomDrr != 0)
		{
			polBsd = ogBsdData.GetBsdByUrno(pomDrr->Bsdu);
		}
	}

	if (polBsd == NULL || strlen(polBsd->Fctc) > 0)
	{
		// Das ist ein ODA-Datensatz oder die Basischicht hat eine regul�re Funktion, die aktuelle Funktion darf nicht ge�ndert werden
		((CComboBox*) GetDlgItem(IDC_C_ACTUAL_SHIFT_FUNC))->EnableWindow(false);
	}

	if (omFctc.GetLength() > 0)
	{
		PFCDATA* polPfcData = ogPfcData.GetPfcByFctc(omFctc);
		if (polPfcData != 0)
		{
			char pclLine[250] = "";
			sprintf(pclLine, "%-8s  %s", polPfcData->Fctc, polPfcData->Fctn);
			m_C_ActualShiftFunc.InsertString(0, pclLine);
		}
	}
}

//************************************************************************************
// Editfelder f�llen
//************************************************************************************

void CTageslisteDlg::SetEdit() 
{
	CCS_TRY;
	char clFormat[256];
	//--------------------------------------------------------
	sprintf( clFormat, "X(%d)", BUFU_LEN );
	m_Bufu.SetTypeToString(clFormat,BUFU_LEN,0);
	m_Bufu.SetTextErrColor(RED);
	m_Bufu.SetInitText(pomDrr->Bufu);
	//--------------------------------------------------------
	sprintf( clFormat, "X(%d)", SCOD_LEN );
	m_Scod.SetTypeToString(clFormat,SCOD_LEN,1);
	m_Scod.SetTextErrColor(RED);
	m_Scod.SetInitText(pomDrr->Scod);
	//--------------------------------------------------------

	SetFctcAndFields();
	//--------------------------------------------------------
	
	CString olTmp;
	int ilSblu = 0;
	int ilHour = 0;
	int ilMin  = 0;

	//Formatstring bauen
	sprintf( clFormat, "##|[':'{0-5}{0-9}]#|[':'{0-5}{0-9}]#");

	m_Sblu.SetTypeToString(clFormat,SBLU_LEN+1,1);

	ilSblu = atoi(pomDrr->Sblu);	//Sblu in Minuten!
	ilHour = (int)(ilSblu / 60);
	ilMin  = (int)(ilSblu % 60);	
	olTmp.Format ("%02.2ld:%02.2ld",ilHour,ilMin);
	m_Sblu.SetInitText (olTmp);

	// Bemerkungen:
	sprintf( clFormat, "X(%d)",ogDrrData.GetMaxFieldLength("REMA"));
	m_Remark.SetTypeToString(clFormat,ogDrrData.GetMaxFieldLength("REMA"),0);
	m_Remark.SetTextErrColor(RED);
	m_Remark.SetInitText(pomDrr->Rema);
	
	// Z_Code Initialisierung
	bmSetDrsActive = true;
	m_E_ZCode1.SetTypeToString(clFormat,DRS1_LEN,1);
	m_E_ZCode1.SetTextErrColor(RED);
	m_E_ZCode1.SetInitText(pomDrr->Drs1);
	
	m_E_ZCode3.SetTypeToString(clFormat,DRS3_LEN,1);
	m_E_ZCode3.SetTextErrColor(RED);
	m_E_ZCode3.SetInitText(pomDrr->Drs3);
	
	m_E_ZCode4.SetTypeToString(clFormat,DRS4_LEN,1);
	m_E_ZCode4.SetTextErrColor(RED);
	m_E_ZCode4.SetInitText(pomDrr->Drs4);
	bmSetDrsActive = false;
	
	UpdateData(false);
	CCS_CATCH_ALL;	
}

//************************************************************************************
// Statische Elemente setzen
//************************************************************************************

void CTageslisteDlg::SetButtonText()
{
	CCS_TRY;
	SetDlgItemText(IDC_D_DELETEDRD,LoadStg(IDC_D_DELETEDRD));
	SetDlgItemText(IDC_D_EDITDRD,LoadStg(IDC_D_EDITDRD));
	SetDlgItemText(IDC_D_INSERTDRD,LoadStg(IDC_D_INSERTDRD));
	SetDlgItemText(IDC_B_SHIFTIS,LoadStg(IDC_B_SHIFTIS));
	SetDlgItemText(IDC_D_INSERTDRA,LoadStg(IDC_D_INSERTDRA));
	SetDlgItemText(IDC_D_EDITDRA,LoadStg(IDC_D_EDITDRA));
	SetDlgItemText(IDC_D_DELETEDRA,LoadStg(IDC_D_DELETEDRA));
	SetDlgItemText(IDC_B_CREATEDTOUR,LoadStg(IDC_B_CREATEDTOUR));
	SetDlgItemText(IDC_B_CANCELDOUBLE,LoadStg(IDC_B_CANCELDOUBLE));
	SetDlgItemText(IDC_BUTTONINFO,LoadStg(IDC_BUTTONINFO));
	SetDlgItemText(IDOK,LoadStg(ID_OK));
	SetDlgItemText(IDCANCEL,LoadStg(ID_CANCEL));
	SetDlgItemText(IDC_BUTTON_REPAIRED,LoadStg(IDC_BUTTON_REPAIRED));
	CCS_CATCH_ALL;	
}

//************************************************************************************
// 
//************************************************************************************

void CTageslisteDlg::SetStatic() 
{
	CCS_TRY;
	SetDlgItemText(IDC_S_1147,LoadStg(IDS_STRING188));		// produktiver Mitarbeiter:*INTXT*

	SetDlgItemText(IDC_S_Kuerzel,LoadStg(IDS_S_Kuerzel));
	SetDlgItemText(IDC_S_Kurzname,LoadStg(IDC_S_Kurzname));

	SetDlgItemText(IDC_MA_NAME,LoadStg(IDS_STRING60)+CString(":"));
	SetDlgItemText(IDC_PERSONAL_NR,LoadStg(IDS_STRING905));
	SetDlgItemText(IDC_C_SAP,LoadStg(IDC_S_SAP));
	SetDlgItemText(IDC_BUFU_NR,LoadStg(IDC_BUFU_NR));
	SetDlgItemText(IDC_S_Arbeitsgruppe2,LoadStg(IDC_S_Arbeitsgruppe2));
	SetDlgItemText(IDC_S_AktuelleTagesdaten,LoadStg(IDC_S_AktuelleTagesdaten));
	SetDlgItemText(IDC_S_Gruppe,LoadStg(PR_DSR_GROUP));
	SetDlgItemText(IDC_C_PRIO,LoadStg(IDC_S_PRIO));
	SetDlgItemText(IDC_S_Bemerkungen,LoadStg(IDS_STRING395));
	SetDlgItemText(IDC_S_Abweichung2,LoadStg(IDC_S_Abweichung2));
	SetDlgItemText(IDC_S_Funktion,LoadStg(IDC_S_Funktion));
	SetDlgItemText(IDC_S_OrgEinheit,LoadStg(IDC_S_OrgEinheit));
	SetDlgItemText(IDC_S_Arbeitsgruppe,LoadStg(IDC_S_Arbeitsgruppe));
	SetDlgItemText(IDC_S_Personaldaten,LoadStg(IDC_S_Personaldaten));
	SetDlgItemText(IDC_S_AllgemeinDaten,LoadStg(IDC_S_AllgemeinDaten));
	SetDlgItemText(IDC_S_SchichtNr,LoadStg(IDC_S_SchichtNr));
	SetDlgItemText(IDC_S_SHIFT_FUNC,LoadStg(IDS_STRING179));		// Schichtfunktion:*INTXT*
	SetDlgItemText(IDC_S_TagesdatenlautStammdaten,LoadStg(IDC_S_TagesdatenlautStammdaten));		// Tagesdaten*INTXT*
	SetDlgItemText(IDC_S_Anwesenheit,LoadStg(IDC_S_Anwesenheit));
	SetDlgItemText(IDC_S_Schichtcode,LoadStg(IDC_S_Schichtcode));
	SetDlgItemText(IDC_S_Dienstzeitvonbis,LoadStg(IDC_S_Dienstzeitvonbis));
	SetDlgItemText(IDC_S_Pausenlagevonbis,LoadStg(IDC_S_Pausenlagevonbis));
	SetDlgItemText(IDC_S_ShiftDuration,LoadStg(IDC_S_ShiftDuration));
	SetDlgItemText(IDC_S_Pausendauer,LoadStg(IDC_S_Pausendauer));
	SetDlgItemText(IDC_S_ZCode,LoadStg(IDC_S_ZCode)+CString(":"));
	SetDlgItemText(IDC_S_Vor,LoadStg(IDS_S_Vor));
	SetDlgItemText(IDC_S_Nach,LoadStg(IDS_S_Nach));
	SetDlgItemText(IDC_S_Komp,LoadStg(IDS_STRING94));
	SetDlgItemText(IDC_S_AZE,LoadStg(IDC_S_AZE));
	SetDlgItemText(IDC_S_Ist,LoadStg(IDS_STRING1384));
	SetDlgItemText(IDC_S_Abwesenheit,LoadStg(IDC_S_Abwesenheit));
	SetDlgItemText(IDC_S_Doppeltouren,LoadStg(IDC_S_Doppeltouren));
	SetDlgItemText(IDC_S_1145,LoadStg(IDS_STRING189));		// nicht produktiver Mitarbeiter:*INTXT*
	SetDlgItemText(IDC_S_Qualifikationen,LoadStg(IDC_S_Qualifikationen));
	SetDlgItemText(IDC_STATIC_3,pomDrr->Usec);
	SetDlgItemText(IDC_STATIC_6,pomDrr->Useu);
	SetDlgItemText(IDC_S_SUB1SUB2,LoadStg(IDS_STRING204));

	// Personaldaten
	STFDATA *prlStf = ogStfData.GetStfByUrno( pomDrr->Stfu);
	if( prlStf != NULL)
	{
		CString olFullName = prlStf->Lanm;
		olFullName += ", ";
		olFullName += prlStf->Finm;
		SetDlgItemText(IDC_STATIC_7,prlStf->Perc);
		SetDlgItemText(IDC_STATIC_9,prlStf->Shnm);
		SetDlgItemText(IDC_STATIC_8,olFullName);
		SetDlgItemText(IDC_STATIC_10,prlStf->Peno);
	}

	// Tagesdaten
	// Funktions Code und Bezeichnung
	COleDateTime olTime;
	CedaDataHelper::DateStringToOleDateTime(pomDrr->Sday,olTime);

	//-------------------------------------------------
	CString olCode =  ogSpfData.GetMainPfcBySurnWithTime(pomDrr->Stfu,olTime);
	SetDlgItemText(IDC_STATIC_11,olCode);
	PFCDATA* polPfcData = ogPfcData.GetPfcByFctc(olCode);
	if (polPfcData != NULL)
		SetDlgItemText(IDC_STATIC_14,polPfcData->Fctn);
	//-------------------------------------------------
	//Arbeitsgruppe ermitteln
	CString olWgpc = ogSwgData.GetWgpBySurnWithTime(pomDrr->Stfu,olTime);
	SetDlgItemText(IDC_STATIC_13,olWgpc);
	WGPDATA* polWgpData = ogWgpData.GetWgpByWgpc(olWgpc);
	if (polWgpData != NULL)
		SetDlgItemText(IDC_STATIC_16,polWgpData->Wgpn);
	//-------------------------------------------------
	//Organisationseinheiten ermitteln
	CString olDpt1 = ogSorData.GetOrgBySurnWithTime(pomDrr->Stfu,olTime);
	SetDlgItemText(IDC_STATIC_12,olDpt1);
	ORGDATA* polOrgData = ogOrgData.GetOrgByDpt1(olDpt1);
	if (polOrgData != NULL)
		SetDlgItemText(IDC_STATIC_15,polOrgData->Dptn);
	//-------------------------------------------------
	CCS_CATCH_ALL;	
}

//************************************************************************************
// Behandelt alle �nderungen eines DRD Datensatzes durch BC`s
//************************************************************************************

void CTageslisteDlg::ProcessDrdChange(DRDDATA *prpDrd,int ipDDXType)
{
	CCS_TRY;
	CCS_CATCH_ALL;
}

//************************************************************************************
// Behandelt alle �nderungen eines DRA Datensatzes durch BC`s
//************************************************************************************

void CTageslisteDlg::ProcessDraChange(DRADATA *prpDra,int ipDDXType)
{
}


//************************************************************************************
// Behandelt alle �nderungen eines DRG Datensatzes durch BC`s
// DIES IST NUR EIN PLATZHALTER, DIE FUNKTION SELBST IST Z.ZT. NICHT N�TIG
//************************************************************************************

void CTageslisteDlg::ProcessDrgChange(DRGDATA *prpDrg,int ipDDXType)
{
}


//************************************************************************************
// Eine Editbox hat den Focus verloren
//************************************************************************************

LONG CTageslisteDlg::OnKillfocus(UINT lParam, LONG wParam) 
{
	CCS_TRY;
	CCSEDITNOTIFY *prlNotify = (CCSEDITNOTIFY*) wParam;
	
	if ((int)lParam == m_Scod.imID && prlNotify->IsChanged) 
	{
		// Schichtcode-Editfeld (IDC_E_SCOD) ist Absender und es wurde eine Ver�nderung vorgenommen
		CString olText;
		m_Scod.GetWindowText(olText);
		if (!olText.IsEmpty())
		{
			long llShiftUrno	= 0;
			int  ilCodeType		= 0;
			bool blIsFreeType	= false;

			//Datum ermitteln
			COleDateTime olDay;
			CedaDataHelper::DateStringToOleDateTime (pomDrr->Sday, olDay);

			if (olDay.GetStatus () == COleDateTime::valid)
			{
				if (CedaDataHelper::GetCodeType (olText, ilCodeType, blIsFreeType))
				{
					switch (ilCodeType)
					{
						case CODE_IS_BSD:
						{
							BSDDATA* polBsdData = ogBsdData.GetBsdByBsdcAndDate(olText,olDay);
							if(polBsdData != NULL)
							{
								llShiftUrno = polBsdData->Urno;
								if (CedaDataHelper::CheckFuncOrgContract(pomDrr->Stfu, pomDrr->Sday,llShiftUrno,omFctc,pomViewInfo->bCheckMainFuncOnly))
								{
									// Pr�fen ob ge�ndert werden darf.
									llShiftUrno = ogDrrData.CheckBSDCode(pomDrr,llShiftUrno,lmOldShiftUrno);

									if(llShiftUrno != lmShiftUrno)
									{
										// Z_Code-s neu initialisieren
										m_E_ZCode1.SetInitText(_T(""));
										m_E_ZCode3.SetInitText(_T(""));
										m_E_ZCode4.SetInitText(_T(""));

										// ja -> Schichtcode retten.
										lmOldShiftUrno = lmShiftUrno;

										// Urno der Schicht setzen
										lmShiftUrno = llShiftUrno;
										// Datentyp festlegen
										imScodType = CODE_IS_BSD;
										imTbsdType = CODE_UNDEFINED;
										bmWork = false;

										SetShiftFields(IS_SHIFT | BSD_DATA, olText);
										
										// DRD Map neu erzeugen. Alle nicht gespeicherten �nderungen gehen jetzt verloren
										CreateTempDrdMap();

										// Grid neu darstellen
										FillDevGrid();

										// DRA Map neu erzeugen. Alle nicht gespeicherten �nderungen gehen jetzt verloren
										CreateTempDraMap(false);

										// Grid neu darstellen
										FillAbsenceGrid();
									}
								}
								else
								{
									// -> keine �nderung
									llShiftUrno = lmOldShiftUrno;
								}
							}
							break;
						}
						case CODE_IS_ODA:
						{
							ODADATA* polOdaData = ogOdaData.GetOdaBySdac (olText);
							if (polOdaData != NULL)
							{
								llShiftUrno = polOdaData->Urno;
								if (CedaDataHelper::CheckFuncOrgContract(pomDrr->Stfu, pomDrr->Sday,llShiftUrno,omFctc,pomViewInfo->bCheckMainFuncOnly))
								{
									// Pr�fen ob ge�ndert werden darf.
									llShiftUrno = ogDrrData.CheckBSDCode(pomDrr,llShiftUrno,lmOldShiftUrno);

									if(llShiftUrno != lmShiftUrno)
									{
										// Z_Code-s neu initialisieren
										m_E_ZCode1.SetInitText(_T(""));
										m_E_ZCode3.SetInitText(_T(""));
										m_E_ZCode4.SetInitText(_T(""));

										// ja -> Schichtcode retten.
										lmOldShiftUrno = lmShiftUrno;

										// Urno der Schicht setzen
										lmShiftUrno = llShiftUrno;
										
										SetShiftFields (IS_SHIFT | ODA_DATA, olText);
										
										// DRD Map neu erzeugen. Alle nicht gespeicherten �nderungen gehen jetzt verloren
										CreateTempDrdMap();

										// Grid neu darstellen
										FillDevGrid();

										// DRA Map neu erzeugen. Alle nicht gespeicherten �nderungen gehen jetzt verloren
										CreateTempDraMap(false);

										// Grid neu darstellen
										FillAbsenceGrid();

										imScodType = ogOdaData.IsODAAndIsRegularFree(olText, &imTbsdType, &bmWork);
									}
								}
								else
								{
									// -> keine �nderung
									llShiftUrno = lmOldShiftUrno;
								}
							}
							break;
						}
						default:
						{
							llShiftUrno = lmOldShiftUrno;
							break;
						}
					}

					if (llShiftUrno == lmOldShiftUrno)
					{
						// Static Anzeige reset
						olText = CedaDataHelper::GetShiftCodeByUrno (lmShiftUrno);
						m_Scod.SetWindowText(olText);
					}
				}
			}
		}	
	}
	ShowControlsSelective();
	/*else if ((int)lParam == m_E_ActualFctc.imID)// && prlNotify->IsChanged) 
	{
		// Aktuelle Funktion hat sich ge�ndert
		// 1. hat der MA diese Funktion in seiner Stamm-Liste?
		CString olFctc;
		m_E_ActualFctc.GetWindowText(olFctc);
		olFctc.MakeUpper();
		m_E_ActualFctc.SetWindowText(olFctc);
		
		if (!olFctc.IsEmpty())
		{
			PFCDATA* polPfcData = ogPfcData.GetPfcByFctc(olFctc);
			if (polPfcData == NULL)
			{
				// Funktion %s existiert nicht*INTXT*
				CString olMessage;
				olMessage.Format(LoadStg(IDS_STRING180),olFctc);
				AfxMessageBox(olMessage,MB_ICONSTOP);
				
				// -> keine �nderung
				olFctc = omOldFctc;
			}
			else
			{
				if (!ogDrrData.CheckFuncOrgContract(pomDrr,lmShiftUrno,olFctc,pomViewInfo->bCheckMainFuncOnly))
				{
					// -> keine �nderung
					olFctc = omOldFctc;
				}
				else
				{
					// alles OK, umsetzen
					// Funktionscode retten.
					omOldFctc = omFctc;
					
					omFctc = olFctc;
					
					// Infofeld setzen
					SetDlgItemText(IDC_STATIC_26,polPfcData->Fctn);
				}
			}
		}	
		else
		{
			//Bitte geben Sie eine Funktion ein!*INTXT*
			CString olMessage = LoadStg(IDS_STRING63483);
			AfxMessageBox(olMessage,MB_ICONSTOP);

			// -> keine �nderung
			olFctc = omOldFctc;
		}

		if(olFctc == omOldFctc)
		{
			// zur�cksetzen
			m_E_ActualFctc.SetWindowText(olFctc);
		}
	}*/
	CCS_CATCH_ALL;
	return 0;
}

/*************************************************************************************
Initialisiert Schichtcode und Zeitangaben aus BSD oder ODA, abh�ngig vom ipShift
*************************************************************************************/
void CTageslisteDlg::SetShiftFields(int ipShift, CString opScode, bool bpDurationOnly/*=false*/)
{
	CCS_TRY;
	// temp�r�re Texte entsprechen Zeitfelden der Dialogmaske: 0 - Dienstzeit von, 1 - Dienstzeit bis, ...
	// sie werden nachfolgend mit BSD oder ODA-Daten gef�llt
	CString olText[8];
	for (int i = 0; i < 8; i++)
	{
		olText[i].Empty();
	}

	BSDDATA* polBsd = 0;

	if (ipShift & DRR_DATA)
	{
		InitTimesByDrr(olText, pomDrr);
	}
	else if(ipShift & ODA_DATA)
	{
		InitTimesByOda(olText, opScode);
	}
	else if(ipShift & BSD_DATA)
	{
		polBsd = ogBsdData.GetBsdByBsdcAndDate(opScode,CString(pomDrr->Sday));
		if(polBsd != 0)
		{
			InitTimesByBsd(olText, polBsd);
		}
		else
		{
			return;
		}
	}
	else if(ipShift & AZE_DATA)
	{
		InitTimesBySpr(olText);
	}
	
	if(!bpDurationOnly)
	{
		SetFctcAndFields();
	}

	if(ipShift & BASIC_SHIFT)
	{
		SetDlgItemText(IDC_STATIC_25,opScode);		// 
		SetDlgItemText(IDC_STATIC_17,olText[0]);	// Ist - Von Datum
		SetDlgItemText(IDC_STATIC_18,olText[1]);	// Ist - Von Zeit
		SetDlgItemText(IDC_STATIC_19,olText[2]);	// Ist - Bis Datum
		SetDlgItemText(IDC_STATIC_20,olText[3]);	// Ist - Bis Zeit
		SetDlgItemText(IDC_STATIC_21,olText[4]);
		SetDlgItemText(IDC_STATIC_22,olText[5]);
		SetDlgItemText(IDC_STATIC_23,olText[6]);

		SetDlgItemText(IDC_STATIC_24,olText[7]);	// Dauer
	}
	else if(ipShift & IS_SHIFT)
	{
		if(!bpDurationOnly)
		{
			m_Scod.SetInitText(opScode);
			m_Avfr_Date.SetInitText(olText[0]);
			m_Avfr_Time.SetInitText(olText[1]);
			m_Avto_Date.SetInitText(olText[2]);
			m_Avto_Time.SetInitText(olText[3]);
			m_Sbfr_Time.SetInitText(olText[4]);
			m_Sbto_Time.SetInitText(olText[5]);
			m_Sblu.SetInitText(olText[6]);
		}
		SetDlgItemText(IDC_STATIC_1497,olText[7]);	// Dauer
	}
	else if(ipShift & AZE_DATA)
	{
		SetDlgItemText(IDC_STATIC_33,olText[0]);	// - Von SchichtDatum
		SetDlgItemText(IDC_STATIC_34,olText[1]);	// - Von SchichtZeit
		SetDlgItemText(IDC_STATIC_42,olText[2]);	// - Bis SchichtDatum
		SetDlgItemText(IDC_STATIC_43,olText[3]);	// - Bis SchichtZeit
		SetDlgItemText(IDC_STATIC_44,olText[4]);	// - von PausenZeit
		SetDlgItemText(IDC_STATIC_45,olText[5]);	// - bis PausenZeit
		SetDlgItemText(IDC_STATIC_46,olText[6]);	// - PausenDauer
		SetDlgItemText(IDC_STATIC_1498,olText[7]);	// - SchichtDauer
	}
	CCS_CATCH_ALL;
}

/***************************************************************************************
F�llt 8 CString-s aus popText mit Datum und Zeitangaben
***************************************************************************************/
void CTageslisteDlg::InitTimesByBsd(CString* popText, BSDDATA* popBsd)
{
	if( pomDrr->Avfr.GetStatus() != COleDateTime::valid)
		return;

	// Startdatum setzen
	CString olSday = pomDrr->Sday;
	COleDateTime olShiftDay(atoi(olSday.Left(4)), atoi(olSday.Mid(4,2)), atoi(olSday.Mid(6,2)),0,0,0);
	popText[0] = olShiftDay.Format("%d.%m.%Y");

	// Startzeit korrekt darstellen und setzen
	popText[1] = popBsd->Esbg;
	popText[1] = popText[1].Left(2) + ":" + popText[1].Right(2);
	
	// Endzeit setzen
	popText[3] = popBsd->Lsen;
	popText[3] = popText[3].Left(2) + ":" + popText[3].Right(2);
	// EndDatum bei Bedarf auf 24 Std. vergr��ern und setzen
	
	// Testen ob die Schicht �ber einen Tag hinaus geht !!
	COleDateTimeSpan olStartTime;
	COleDateTimeSpan olEndTime;
	
	CedaDataHelper::HourMinStringToOleDateTimeSpan(popBsd->Esbg,olStartTime);
	CedaDataHelper::HourMinStringToOleDateTimeSpan(popBsd->Lsen,olEndTime);
	
	if (olStartTime.GetStatus() == COleDateTime::valid && olEndTime.GetStatus() == COleDateTime::valid)
	{
		// Pr�fen Zeit �ber 24 Uhr hinausgeht
		if (olStartTime > olEndTime)
		{
			// -> Ja
			// Anfangsdatum der Schicht plus einen Tag
			olShiftDay += COleDateTimeSpan(1,0,0,0);
			popText[2] = olShiftDay.Format("%d.%m.%Y");
		}
		else
		{
			popText[2] = popText[0];
		}
		
	}

	// Pausenanfang setzen
	popText[4] = popBsd->Bkf1;
	if (!popText[4].IsEmpty())
	{
		popText[4] = popText[4].Left(2) + ":" + popText[4].Right(2);
	}
	else
	{
		popText[4].Empty();
	}
	
	// Pausenende setzen
	popText[5] = popBsd->Bkt1;
	if (!popText[5].IsEmpty())
	{
		popText[5] = popText[5].Left(2) + ":" + popText[5].Right(2);
	}
	else
	{
		popText[5].Empty();
	}
	
	// Pausenl�nge setzen (setzen der SBLU)
	CString olTmp;
	int ilSblu = atoi (popBsd->Bkd1);	// in Minuten
	int ilHour = 0;
	int ilMin  = 0;
	
	ilHour = (int)(ilSblu / 60);
	ilMin  = (int)(ilSblu % 60);	
	olTmp.Format ("%02.2ld:%02.2ld",ilHour,ilMin);

	popText[6] = olTmp;

	// Dauer der Schicht
	COleDateTime olDuration;
	if (olEndTime < olStartTime)
	{
		olEndTime += COleDateTimeSpan(1,0,0,0);
	}
	olDuration = olEndTime - olStartTime;
	olDuration -= COleDateTimeSpan(0,0,ilSblu,0);
	popText[7] = olDuration.Format("%H:%M");
}

/***************************************************************************************
F�llt 8 CString-s aus popText mit Datum und Zeitangaben
***************************************************************************************/
void CTageslisteDlg::InitTimesByOda(CString* popText, CString opScode)
{
	if( pomDrr->Avfr.GetStatus() != COleDateTime::valid)
		return;
	
	// wenn ODATAB.ABFR & ODATAB.ABTO g�ltige Zeiten haben, �berschreiben sie alles andere,
	// TBSD wird in diesem Fall ignoriert
	ODADATA* polOda = ogOdaData.GetOdaBySdac(opScode);
	if(!polOda) return;
	DRRDATA olDrr;
	ogDrrData.CopyDrr(&olDrr,pomDrr);

	if(!ogDrrData.SetDrrDataScodByODA(&olDrr, polOda->Urno, false))
		return;

	olDrr.Sbfr.SetStatus(COleDateTime::invalid);
	olDrr.Sbto.SetStatus(COleDateTime::invalid);

	InitTimesByDrr(popText, &olDrr);	
}

/***************************************************************************************
F�llt 8 CString-s aus popText mit Datum und Zeitangaben
***************************************************************************************/
void CTageslisteDlg::InitTimesByDrr(CString* popText, DRRDATA* popDrr)
{
	COleDateTime olAvto;
	olAvto.SetStatus(COleDateTime::invalid);
	if( popDrr->Avfr.GetStatus() == COleDateTime::valid)
	{
		// Startdatum setzen
		popText[0] = popDrr->Avfr.Format("%d.%m.%Y");
		
		if(popDrr->Avto.GetStatus() == COleDateTime::valid)
		{
			olAvto = popDrr->Avto;
			if(popDrr->Avto < popDrr->Avfr)
			{
				olAvto += COleDateTimeSpan(1,0,0,0);
			}
		}
		else
		{
			olAvto = popDrr->Avfr; // + COleDateTimeSpan(1,0,0,0);
		}
		
		popText[2] = olAvto.Format("%d.%m.%Y");
		
		popText[1] = popDrr->Avfr.Format("%H:%M");
		popText[3] = olAvto.Format("%H:%M");
	}
	
	if(popDrr->Sbfr.GetStatus() == COleDateTime::valid && popDrr->Sbto.GetStatus() == COleDateTime::valid)
	{
		popText[4] = popDrr->Sbfr.Format("%H:%M");
		popText[5] = popDrr->Sbto.Format("%H:%M");
	}
	else
	{
		popText[4] = popText[5] = "";
	}

	int ilSblu = 0;
	if(strlen(popDrr->Sblu) > 0)
	{
		//setzen der SBLU
		CString olTmp;
		ilSblu = atoi (popDrr->Sblu);
		int ilHour = 0;
		int ilMin  = 0;
		
		ilHour = (int)(ilSblu / 60);
		ilMin  = (int)(ilSblu % 60);	
		olTmp.Format ("%02.2ld:%02.2ld",ilHour,ilMin);
		popText[6] = olTmp;
	}
	else
		popText[6].Empty();

	if(olAvto.GetStatus() == COleDateTime::valid)
	{
		// Dauer der Schicht
		COleDateTimeSpan olDuration;
		olDuration = olAvto - popDrr->Avfr;
		
		olDuration -= COleDateTimeSpan(0,0,ilSblu,0);
		popText[7] = olDuration.Format("%H:%M");
	}
	else
		popText[7].Empty();
}

//*************************************************************************************************************
// Dialog soll beendet werden
//*************************************************************************************************************

void CTageslisteDlg::OnCancel() 
{
	// Sollten Daten ver�ndert worden sein Sicherheitsabfrage vor beenden
	if (bmDrdIsDirty || bmDraIsDirty)
	{
		if (MessageBox(LoadStg(IDS_STRING1141), LoadStg(IDS_STRING1178), MB_ICONQUESTION | MB_YESNO ) == IDYES)
		{
			OnOK();
			return;
		}
	}
	
	CDialog::OnCancel();
}

//**********************************************************************************
// Grid initialisieren
//**********************************************************************************

void CTageslisteDlg::InitDevGrid ()
{
	CCS_TRY;
	// Grid erzeugen
	pomDevGrid = new CGridControl ( this, IDC_DEV_GRID,DEVCOLCOUNT,0);
	// �berschriften setzen
	pomDevGrid->SetValueRange(CGXRange(0,1), LoadStg(HD_DSR_AVFA_TIME));
	pomDevGrid->SetValueRange(CGXRange(0,2), LoadStg(HD_DSR_AVTA_TIME));
	pomDevGrid->SetValueRange(CGXRange(0,3), LoadStg(HD_DSR_EFCT));
	pomDevGrid->SetValueRange(CGXRange(0,4), LoadStg(HD_DSR_ORG));

	// Breite der Spalten einstellen
	InitDevColWidths ();
	// Grid mit Daten f�llen.
	FillDevGrid();

	pomDevGrid->Initialize();
	pomDevGrid->GetParam()->EnableUndo(FALSE);
	pomDevGrid->LockUpdate(TRUE);
	pomDevGrid->LockUpdate(FALSE);
	pomDevGrid->GetParam()->EnableUndo(TRUE);
	pomDevGrid->SetScrollBarMode(SB_VERT,gxnAutomatic);	

	// Verhindert, das die Spaltenbreite ver�ndert wird
	pomDevGrid->GetParam()->EnableTrackColWidth(FALSE);
	pomDevGrid->GetParam()->EnableSelection(GX_SELROW);

	pomDevGrid->SetStyleRange( CGXRange().SetTable(), CGXStyle()
		.SetVerticalAlignment(DT_VCENTER)
		.SetReadOnly(TRUE)
		);
	pomDevGrid->EnableAutoGrow ( false );
	pomDevGrid->SetLbDblClickAction ( WM_COMMAND, IDC_D_EDITDRD);
	CCS_CATCH_ALL;	
}

//**********************************************************************************
// Grid initialisieren, 
//**********************************************************************************

void CTageslisteDlg::InitDevColWidths ()
{
	pomDevGrid->SetColWidth ( 0, 0, 27);
	pomDevGrid->SetColWidth ( 1, 1, 50);
	pomDevGrid->SetColWidth ( 2, 2, 50);
	pomDevGrid->SetColWidth ( 3, 3, 50);
	pomDevGrid->SetColWidth ( 4, 4, 66);
}

//**********************************************************************************
// Grid mit Daten f�llen
//**********************************************************************************

void CTageslisteDlg::FillDevGrid()
{
	CCS_TRY;
	ROWCOL ilRowCount;
	BOOL blRet;
	
	// Readonly ausschalten
	pomDevGrid->GetParam()->SetLockReadOnly(false);
	
	// Anzahl der Rows ermitteln
	ilRowCount = pomDevGrid->GetRowCount();
	if (ilRowCount > 0)
		// Grid l�schen			
		blRet =	pomDevGrid->RemoveRows(1,ilRowCount);
	
	// BSD-Delegations: alle DELs durchgehen

	// verbundene DELs bekommen
	CCSPtrArray<DELDATA> olDelList;
	int ilDelNum = ogDelData.GetDelListByBsdu(pomDrr->Bsdu, false, &olDelList);
	CString olStrTime;
	for(int ilCount=0; ilCount<ilDelNum; ilCount++)
	{
		DELDATA* polDel = (DELDATA*)&olDelList[ilCount];
		// Neue Row einf�gen
		ilRowCount = pomDevGrid->GetRowCount();
		blRet = pomDevGrid->InsertRows(ilRowCount+1,1);
		
		pomDevGrid->SetStyleRange ( CGXRange(ilCount+1,1), CGXStyle().SetItemDataPtr( (void*)polDel->Urno) );
		// Datum pr�fen und formatieren
		if(strlen(polDel->Delf) > 0)
		{
			olStrTime = polDel->Delf;
			olStrTime.Insert(2,':');
			pomDevGrid->SetValueRange(CGXRange(ilCount+1,1),olStrTime);
		}
		if(strlen(polDel->Delt) > 0)
		{
			olStrTime = polDel->Delt;
			olStrTime.Insert(2,':');
			pomDevGrid->SetValueRange(CGXRange(ilCount+1,2),olStrTime);
		}
		
		pomDevGrid->SetValueRange(CGXRange(ilCount+1,3),polDel->Fctc);
		pomDevGrid->SetValueRange(CGXRange(ilCount+1,4),polDel->Dpt1);
		pomDevGrid->EnableGrid(ilCount+1,false);
	}

	POSITION rlPos; 
	long llUrno; 
	DRDDATA *prlDrd; 

	// alle DRDs in <omDrdMap> durchgehen
	for(rlPos = omTempDrdMap.GetStartPosition(); rlPos != NULL; )
	{
		// Neue Row einf�gen
		ilRowCount = pomDevGrid->GetRowCount ();
		blRet = pomDevGrid->InsertRows ( ilRowCount+1,1);
		
		// DRD ermitteln
		omTempDrdMap.GetNextAssoc(rlPos,(void *&)llUrno, (void *&)prlDrd);
		// Urno mit erstem Feld koppeln.						
		TRACE("CTageslisteDlg::FillDevGrid(): Adding DRD with Urno %ld\n",llUrno);
		pomDevGrid->SetStyleRange ( CGXRange(ilCount+1,1), CGXStyle().SetItemDataPtr( (void*)llUrno ) );
		// Datum pr�fen und formatieren
		if( prlDrd->Drdf.GetStatus() == COleDateTime::valid)
			pomDevGrid->SetValueRange(CGXRange(ilCount+1,1),(prlDrd->Drdf.Format("%H:%M")));
		if( prlDrd->Drdt.GetStatus() == COleDateTime::valid)
			pomDevGrid->SetValueRange(CGXRange(ilCount+1,2),(prlDrd->Drdt.Format("%H:%M")));
		
		pomDevGrid->SetValueRange(CGXRange(ilCount+1,3),prlDrd->Fctc);
		pomDevGrid->SetValueRange(CGXRange(ilCount+1,4),prlDrd->Dpt1);
		pomDevGrid->EnableGrid (ilCount+1,true);
		
		ilCount++;
	}
	// Readonly einschalten
	pomDevGrid->GetParam()->SetLockReadOnly(true);
	CCS_CATCH_ALL;	
}

//**********************************************************************************
// Schichtcode ausw�hlen
//**********************************************************************************

void CTageslisteDlg::OnSetShiftIs() 
{
	CCS_TRY;
	long llShiftUrno;
	
	// Dialog zur Auswahl einer Basisschicht anzeigen
	CSelectShiftDlg olSelectShiftDlg (this, pomDrr);
	if (olSelectShiftDlg.DoModal() == IDCANCEL)	return;	// Dialog wurde mit abbrechen beendet
	
	// Wurde eine Schicht oder eine Abwesenheit ausgew�hlt
	switch(olSelectShiftDlg.GetCodeType())		// gibt an, ob intern gespeicherte Code Bsd ist
	{
	case CODE_IS_BSD:
		{
			// Z_Code-s neu initialisieren
			m_E_ZCode1.SetInitText(_T(""));
			m_E_ZCode3.SetInitText(_T(""));
			m_E_ZCode4.SetInitText(_T(""));
			
			BSDDATA* polBsd;
			// -> Schicht ausgew�hlt
			if ((polBsd = olSelectShiftDlg.GetBsdPtr()) != NULL)
			{
				TRACE("DutyRoster_View::OnMenuAddDrr(): Selection is %s\n",CString(polBsd->Bsdc));
				// Vorher wird der Code auf G�ltigkeit gepr�ft.
				if (CedaDataHelper::CheckFuncOrgContract (pomDrr->Stfu, pomDrr->Sday, polBsd->Urno, omFctc, pomViewInfo->bCheckMainFuncOnly))
				{
					llShiftUrno = ogDrrData.CheckBSDCode(pomDrr,polBsd->Urno,lmOldShiftUrno);
					
					// DRD Map neu erzeugen. Alle nicht gespeicherten �nderungen gehen jetzt 
					// verloren
					CreateTempDrdMap();
					// Grid neu darstellen
					FillDevGrid();
					// DRA Map neu erzeugen. Alle nicht gespeicherten �nderungen gehen jetzt 
					// verloren
					CreateTempDraMap(true, polBsd->Urno);
					// Grid neu darstellen
					FillAbsenceGrid();
					// Urno der Schicht setzen
					lmShiftUrno = llShiftUrno;
					// Datentyp festlegen
					imScodType = CODE_IS_BSD;
					imTbsdType = CODE_UNDEFINED;
					bmWork = false;
					// Editfelder aktualisieren
					SetShiftFields(IS_SHIFT | BSD_DATA, polBsd->Bsdc);
				}
			}
		}
		break;
	case CODE_IS_ODA:
		{
			// Z_Code-s neu initialisieren
			m_E_ZCode1.SetInitText(_T(""));
			m_E_ZCode3.SetInitText(_T(""));
			m_E_ZCode4.SetInitText(_T(""));

			ODADATA* polOda;
			// -> Abwesenheit ausgew�hlt
			if ((polOda = olSelectShiftDlg.GetOdaPtr()) != NULL)
			{
				// Schichtcode setzen
				// Vorher wird der Code auf G�ltigkeit gepr�ft.
				if (CedaDataHelper::CheckFuncOrgContract (pomDrr->Stfu, pomDrr->Sday, polOda->Urno, omFctc, pomViewInfo->bCheckMainFuncOnly))
				{
					llShiftUrno = ogDrrData.CheckBSDCode(pomDrr,polOda->Urno,lmOldShiftUrno);
					
					// DRD Map neu erzeugen. Alle nicht gespeicherten �nderungen gehen jetzt verloren
					CreateTempDrdMap();
					// Grid neu darstellen
					FillDevGrid();
					// DRA Map neu erzeugen. Alle nicht gespeicherten �nderungen gehen jetzt verloren
					CreateTempDraMap(false);
					// Grid neu darstellen
					FillAbsenceGrid();
					// Urno der Schicht setzen
					lmShiftUrno = llShiftUrno;
					// Datentyp feststellen
					imScodType = ogOdaData.IsODAAndIsRegularFree(CString(polOda->Sdac), &imTbsdType, &bmWork);

					// Editfelder aktualisieren
					SetShiftFields(IS_SHIFT | ODA_DATA, polOda->Sdac);
				}
			}
		}
		break;
	default:
		return;
	}
	
	// Abh�ngig vom Datentyp Felder ein- oder ausschalten
	ShowControlsSelective();
	
	CCS_CATCH_ALL;
}

//**********************************************************************************
// Abwesenheit einf�gen
//**********************************************************************************

void CTageslisteDlg::On_Insert_Dev() 
{
	CCS_TRY;
	// Alle Eingaben auf G�ltigkeit 
	if (!IsInputValid())
		return;
	
	//----------------------------------------------------------------------------------
	// Schichtzeiten
	//----------------------------------------------------------------------------------
	COleDateTime olTimeTo,olTimeFrom;
	// Start und Ende der Schicht aus der Oberfl�che einlesen
	ReadShiftTimes(IS_SHIFT,olTimeFrom,olTimeTo);
	//----------------------------------------------------------------------------------

	// Neuen leeren DRD Datensatz generieren
	DRDDATA* polDrdData = ogDrdData.CreateDrdData(pomDrr->Stfu,pomDrr->Sday,pomDrr->Drrn);

	if (polDrdData == NULL)
		return;

	//-----------------------------------------------------------------------
	// DRD Initialisieren
	//-----------------------------------------------------------------------
	// Abwesenheitszeiten auf Schichdauer setzen
	polDrdData->Drdf = olTimeFrom;
	polDrdData->Drdt = olTimeTo;

	//-----------------------------------------------------------------------
	// Aktuellen Werte f�r den DRR einlesen (Schicht Von - Bis)

	DRRDATA* polDummyDrr = new DRRDATA;
	// Nutzinformationen kopieren (Schichtcode usw.) 
	ogDrrData.CopyDrrValues(pomDrr,polDummyDrr);
	// Werte setzen
	polDummyDrr->Avfr = olTimeFrom;
	polDummyDrr->Avto = olTimeTo;

	// Dialog starten
	CDrdDialog olDlg(this,polDrdData,polDummyDrr,&omTempDrdMap,pomViewInfo->bCheckMainFuncOnly);
	if (olDlg.DoModal() == IDOK)
	{
		bmDrdIsDirty = true;
		omTempDrdMap.SetAt((void *)polDrdData->Urno,polDrdData);
		FillDevGrid();
	}
	
	delete polDummyDrr;

	OnGridLButton(NULL, NULL);
	CCS_CATCH_ALL;	
}

//**********************************************************************************
// Abwesenheit l�schen
//**********************************************************************************

void CTageslisteDlg::On_Delete_Dev() 
{
	CCS_TRY;
	CRowColArray	olRowColArray;
	CGXStyle		olStyle;
	long			ilUrno;
	
	// Ausgew�hlt DRD URNO erhalten, Selektion aus Grid 
	pomDevGrid->GetSelectedRows (olRowColArray);
	
	// Wurden Zeilen ausgew�hlt
	if (olRowColArray.GetSize() == 0)
	{
		TRACE ("Keine Zeilen ausgew�hlt.\n");
		return;
	}
	
	if(!pomDevGrid->IsGridEnabled(olRowColArray[0],1))
	{
		AfxMessageBox(LoadStg(IDS_STRING193),MB_ICONINFORMATION);	// Diese Abweichung aus Basisschicht kann nicht bearbeitet werden*INTXT*
		return;
	}

	// Style und damit Datenpointer einer Zelle erhalten
	pomDevGrid->ComposeStyleRowCol(olRowColArray[0],1, &olStyle );
	// Urno erhalten
	ilUrno = (long)olStyle.GetItemDataPtr();
	if (!ilUrno)
	{
		// Keine g�ltige URNO gefunden, L�schen abbrechen
		TRACE ("Urno=0 in TageslisteDlg f�r Zeile%d\n",olRowColArray[0]);
		return;
	}
	
	//--------------------------------------------------------------------
	// DRD aus der Temp Map l�schen
	bmDrdIsDirty = true;
	// DRD aus der Map l�schen
	omTempDrdMap.RemoveKey((void *&)ilUrno);
	
	// Grid neu f�llen
	FillDevGrid();

	OnGridLButton(NULL, NULL);
	CCS_CATCH_ALL;	
}

//**********************************************************************************
// Abwesenheit bearbeiten
//**********************************************************************************

void CTageslisteDlg::On_Edit_Dev() 
{
	CCS_TRY;
	if (((CButton*) GetDlgItem(IDC_D_EDITDRD))->IsWindowEnabled())
	{
		CRowColArray	olRowColArray;
		CGXStyle		olStyle;
		long			ilUrno;
		
		// Ausgew�hlt DRD URNO erhalten
		// Selektion aus Grid 
		pomDevGrid->GetSelectedRows (olRowColArray);
		
		// Wurden Zeilen ausgew�hlt
		if (olRowColArray.GetSize() == 0)
		{
			TRACE ("Keine Zeilen ausgew�hlt.\n");
			return;
		}

		if(!pomDevGrid->IsGridEnabled(olRowColArray[0],1))
		{
			AfxMessageBox(LoadStg(IDS_STRING193),MB_ICONINFORMATION);	// Diese Abweichung aus Basisschicht kann nicht bearbeitet werden*INTXT*
			return;
		}
		
		// Style und damit Datenpointer einer Zelle erhalten
		pomDevGrid->ComposeStyleRowCol(olRowColArray[0],1, &olStyle );
		// Urno erhalten
		ilUrno = (long)olStyle.GetItemDataPtr();
		if (!ilUrno)
		{
			// Keine g�ltige URNO gefunden, Editieren abbrechen
			TRACE ("Urno=0 in TageslisteDlg f�r Zeile%d\n",olRowColArray[0]);
			return;
		}
		
		//--------------------------------------------------------------------
		// DRD aus der Temp Map holen
		POSITION rlPos; 
		long llMapUrno; 
		DRDDATA *prlDrd = 0; 
		bool blFound = false;
		
		// alle DRDs in <omDrdMap> durchgehen
		for(rlPos = omTempDrdMap.GetStartPosition(); rlPos != NULL && !blFound ; )
		{
			// DRD ermitteln
			omTempDrdMap.GetNextAssoc(rlPos,(void *&)llMapUrno, (void *&)prlDrd);
			if (llMapUrno == ilUrno)
				blFound = true;
		}
		
		// Pr�fung
		if (prlDrd == NULL)
		{
			// Keine g�ltige URNO gefunden, L�schen abbrechen
			TRACE ("Urno ung�ltig in TageslisteDlg\n");
			return;
		}
		
		// Dialog starten
		CDrdDialog olDlg(this,prlDrd,pomDrr,&omTempDrdMap,pomViewInfo->bCheckMainFuncOnly);
		if(olDlg.DoModal() == IDOK)
		{
			bmDrdIsDirty = true;
			// Grid neu zeichnen.
			FillDevGrid();
		}

		OnGridLButton(NULL, NULL);
	}
	CCS_CATCH_ALL;	
}


//**********************************************************************************
// Grid initialisieren
//**********************************************************************************

void CTageslisteDlg::InitAbsenceGrid ()
{
	CCS_TRY;
	// Grid erzeugen
	pomAbsenceGrid = new CGridControl ( this, IDC_ABS_GRID,ABSCOLCOUNT,0);
	// �berschriften setzen
	pomAbsenceGrid->SetValueRange(CGXRange(0,1), LoadStg(IDS_STRING1861));
	pomAbsenceGrid->SetValueRange(CGXRange(0,2), LoadStg(HD_DSR_AVFA_TIME));
	pomAbsenceGrid->SetValueRange(CGXRange(0,3), LoadStg(HD_DSR_AVTA_TIME));
	pomAbsenceGrid->SetValueRange(CGXRange(0,4), LoadStg(IDS_STRING1862));
	
	// Breite der Spalten einstellen
	InitAbsenceColWidths ();
	// Grid mit Daten f�llen.
	FillAbsenceGrid();
	
	pomAbsenceGrid->Initialize();
	pomAbsenceGrid->GetParam()->EnableUndo(FALSE);
	pomAbsenceGrid->LockUpdate(TRUE);
	pomAbsenceGrid->LockUpdate(FALSE);
	pomAbsenceGrid->GetParam()->EnableUndo(TRUE);
	pomAbsenceGrid->SetScrollBarMode(SB_VERT,gxnAutomatic);	
		
	// Verhindert, das die Spaltenbreite ver�ndert wird
	pomAbsenceGrid->GetParam()->EnableTrackColWidth(FALSE);
	// Es k�nnen nur ganze Zeilen markiert werden.
	pomAbsenceGrid->GetParam()->EnableSelection(GX_SELROW);
	
	pomAbsenceGrid->SetStyleRange( CGXRange().SetTable(), CGXStyle()
		.SetVerticalAlignment(DT_VCENTER)
		.SetReadOnly(TRUE)
		);
	pomAbsenceGrid->EnableAutoGrow ( false );
	pomAbsenceGrid->SetLbDblClickAction ( WM_COMMAND, IDC_D_EDITDRA);
	CCS_CATCH_ALL;	
}

//**********************************************************************************
// Grid initialisieren, 
//**********************************************************************************

void CTageslisteDlg::InitAbsenceColWidths ()
{
	pomAbsenceGrid->SetColWidth ( 0, 0, 27);
	pomAbsenceGrid->SetColWidth ( 1, 1, 60);
	pomAbsenceGrid->SetColWidth ( 2, 2, 70);
	pomAbsenceGrid->SetColWidth ( 3, 3, 70);
	pomAbsenceGrid->SetColWidth ( 4, 4, 298);
}


//**********************************************************************************
// Grid mit Daten f�llen
//**********************************************************************************

void CTageslisteDlg::FillAbsenceGrid()
{
	CCS_TRY;
	
	ROWCOL ilRowCount;
	BOOL blRet;

	// Readonly ausschalten
	pomAbsenceGrid->GetParam()->SetLockReadOnly(false);

	// Anzahl der Rows ermitteln
	ilRowCount = pomAbsenceGrid->GetRowCount();
	if (ilRowCount > 0)
	{
		// Grid l�schen			
		blRet =	pomAbsenceGrid->RemoveRows(1,ilRowCount);
	}

	// BSD-Delegations: alle DELs durchgehen

	// verbundene absence DELs bekommen
	CCSPtrArray<DELDATA> olDelList;
	int ilDelNum = ogDelData.GetDelListByBsdu(pomDrr->Bsdu,true, &olDelList);
	CString olStrTime;
	for(int ilCount=0; ilCount<ilDelNum; ilCount++)
	{
		DELDATA* polDel = (DELDATA*)&olDelList[ilCount];
		// Neue Row einf�gen
		ilRowCount = pomAbsenceGrid->GetRowCount();
		blRet = pomAbsenceGrid->InsertRows(ilRowCount+1,1);

		pomAbsenceGrid->SetStyleRange ( CGXRange(ilCount+1,1), CGXStyle().SetItemDataPtr( (void*)polDel->Urno) );
		pomAbsenceGrid->SetValueRange(CGXRange(ilCount+1,1),polDel->Sdac);

		// Datum pr�fen und formatieren
		if(strlen(polDel->Delf) > 0)
		{
			olStrTime = polDel->Delf;
			olStrTime.Insert(2,':');
			pomAbsenceGrid->SetValueRange(CGXRange(ilCount+1,2),olStrTime);
		}
		if(strlen(polDel->Delt) > 0)
		{
			olStrTime = polDel->Delt;
			olStrTime.Insert(2,':');
			pomAbsenceGrid->SetValueRange(CGXRange(ilCount+1,3),olStrTime);
		}
		
		pomAbsenceGrid->EnableGrid(ilCount+1,false);
	}
	
	POSITION rlPos; 
	long llUrno; 
	DRADATA *prlDra; 
	// alle DRDs in <omDrdMap> durchgehen
	
	for(rlPos = omTempDraMap.GetStartPosition(); rlPos != NULL; )
	{
		// Neue Row einf�gen
		ilRowCount = pomAbsenceGrid->GetRowCount ();
		blRet = pomAbsenceGrid->InsertRows ( ilRowCount+1,1);
		
		// DRD ermitteln
		omTempDraMap.GetNextAssoc(rlPos,(void *&)llUrno, (void *&)prlDra);
		// Urno mit erstem Feld koppeln.						
		TRACE("CTageslisteDlg::FillAbsGrid(): Adding DRA with Urno %ld\n",llUrno);
		pomAbsenceGrid->SetStyleRange ( CGXRange(ilCount+1,1), CGXStyle().SetItemDataPtr( (void*)llUrno ) );
		
		pomAbsenceGrid->SetValueRange(CGXRange(ilCount+1,1),prlDra->Sdac);
		
		// Datum pr�fen und formatieren
		if( prlDra->Abfr.GetStatus() == COleDateTime::valid)
			pomAbsenceGrid->SetValueRange(CGXRange(ilCount+1,2),(prlDra->Abfr.Format("%H:%M")));
		if( prlDra->Abto.GetStatus() == COleDateTime::valid)
			pomAbsenceGrid->SetValueRange(CGXRange(ilCount+1,3),(prlDra->Abto.Format("%H:%M")));
		
		pomAbsenceGrid->SetValueRange(CGXRange(ilCount+1,4),prlDra->Rema);
		
		ilCount++;
	}
	// Readonly einschalten
	pomAbsenceGrid->GetParam()->SetLockReadOnly(true);
	CCS_CATCH_ALL;	
}


//**********************************************************************************
// Abwesenheiten l�schen
//**********************************************************************************

void CTageslisteDlg::On_Absence_Delete() 
{
	CCS_TRY;
	CRowColArray	olRowColArray;
	CGXStyle		olStyle;
	long			ilUrno;
	
	// Ausgew�hlt DRD URNO erhalten
	// Selektion aus Grid 
	pomAbsenceGrid->GetSelectedRows (olRowColArray);
	
	// Wurden Zeilen ausgew�hlt
	if (olRowColArray.GetSize() == 0)
	{
		TRACE ("Keine Zeilen ausgew�hlt.\n");
		return;
	}

	if(!pomAbsenceGrid->IsGridEnabled(olRowColArray[0],1))
	{
		AfxMessageBox(LoadStg(IDS_STRING193),MB_ICONINFORMATION);	// Diese Abwesenheit aus Basisschicht kann nicht bearbeitet werden*INTXT*
		return;
	}
	
	// Style und damit Datenpointer einer Zelle erhalten
	pomAbsenceGrid->ComposeStyleRowCol(olRowColArray[0],1, &olStyle );
	// Urno erhalten
	ilUrno = (long)olStyle.GetItemDataPtr();
	if (!ilUrno)
	{
		// Keine g�ltige URNO gefunden, L�schen abbrechen
		TRACE ("Urno=0 in TageslisteDlg f�r Zeile%d\n",olRowColArray[0]);
		return;
	}
	
	//--------------------------------------------------------------------
	// DRA aus der Temp Map l�schen
	bmDraIsDirty = true;
	// DRD aus der Map l�schen
	omTempDraMap.RemoveKey((void *&)ilUrno);
	
	// Grid neu f�llen
	FillAbsenceGrid();

	OnGridLButton(NULL, NULL);
	CCS_CATCH_ALL;	
}

//**********************************************************************************
// Abwesenheiten ver�ndern
//**********************************************************************************

void CTageslisteDlg::On_Absence_Edit() 
{
	CCS_TRY;
	if (((CButton*) GetDlgItem(IDC_D_EDITDRA))->IsWindowEnabled())
	{
		CRowColArray	olRowColArray;
		CGXStyle		olStyle;
		long			ilUrno;
		
		// Ausgew�hlt DRA URNO erhalten
		// Selektion aus Grid 
		pomAbsenceGrid->GetSelectedRows (olRowColArray);
		
		// Wurden Zeilen ausgew�hlt
		if (olRowColArray.GetSize() == 0)
		{
			TRACE ("Keine Zeilen ausgew�hlt.\n");
			return;
		}

		if(!pomAbsenceGrid->IsGridEnabled(olRowColArray[0],1))
		{
			AfxMessageBox(LoadStg(IDS_STRING193),MB_ICONINFORMATION);	// Diese Abweichung aus Basisschicht kann nicht bearbeitet werden*INTXT*
			return;
		}
		
		// Style und damit Datenpointer einer Zelle erhalten
		pomAbsenceGrid->ComposeStyleRowCol(olRowColArray[0],1, &olStyle );
		// Urno erhalten
		ilUrno = (long)olStyle.GetItemDataPtr();
		if (!ilUrno)
		{
			// Keine g�ltige URNO gefunden, L�schen abbrechen
			TRACE ("Urno=0 in TageslisteDlg f�r Zeile%d\n",olRowColArray[0]);
			return;
		}
		
		
		//--------------------------------------------------------------------
		// DRA aus der Temp Map holen
		POSITION rlPos; 
		long llMapUrno; 
		DRADATA *prlDra = 0; 
		bool blFound = false;
		
		// alle DRAs in <omDraMap> durchgehen
		for(rlPos = omTempDraMap.GetStartPosition(); rlPos != NULL && !blFound ; )
		{
			// DRA ermitteln
			omTempDraMap.GetNextAssoc(rlPos,(void *&)llMapUrno, (void *&)prlDra);
			if (llMapUrno == ilUrno)
				blFound = true;
		}
		
		// Pr�fung
		if (prlDra == NULL)
		{
			// Keine g�ltige URNO gefunden, L�schen abbrechen
			TRACE ("Urno ung�ltig in TageslisteDlg\n");
			return;
		}
		
		// Dialog starten
		CDraDialog olDlg(this,prlDra,pomDrr,&omTempDraMap);
		if(olDlg.DoModal() == IDOK)
		{
			bmDraIsDirty = true;
			// Grid neu zeichnen.
			FillAbsenceGrid();
		}

		OnGridLButton(NULL, NULL);
	}
	CCS_CATCH_ALL;	
}

//**********************************************************************************
// Abwesenheiten einf�gen
//**********************************************************************************

void CTageslisteDlg::On_Absence_Insert() 
{
	CCS_TRY;
	// Alle Eingaben auf G�ltigkeit 
	if (!IsInputValid())
		return;
	
	COleDateTime olTimeTo,olTimeFrom;
	// Start und Ende der Schicht aus Ist-Schicht-Zeit einlesen
	ReadShiftTimes(IS_SHIFT,olTimeFrom,olTimeTo);
	if(olTimeFrom.GetStatus() != COleDateTime::valid ||	olTimeTo.GetStatus() != COleDateTime::valid) 
	{
		_ASSERTE(false);
		return;		// da stimmt was nicht
	}
	
	// Neuen leeren DRA Datensatz generieren
	DRADATA* polDraData = ogDraData.CreateDraData(pomDrr);
	
	if (polDraData == NULL)
		return;
	
	//-----------------------------------------------------------------------
	// DRA Initialisieren
	//-----------------------------------------------------------------------
	
	// Abwesenheitszeiten auf Schichdauer setzen
	polDraData->Abfr = olTimeFrom;
	polDraData->Abto = olTimeTo;
	
	//-----------------------------------------------------------------------
	// Aktuellen Werte f�r den DRR einlesen (Schicht Von - Bis)
	
	DRRDATA* polDummyDrr = new DRRDATA;
	// Nutzinformationen kopieren (Schichtcode usw.) 
	ogDrrData.CopyDrrValues(pomDrr,polDummyDrr);
	// Werte setzen
	polDummyDrr->Avfr = olTimeFrom;
	polDummyDrr->Avto = olTimeTo;
	
	// Dialog starten
	CDraDialog olDlg(this,polDraData,polDummyDrr,&omTempDraMap);
	if(olDlg.DoModal() == IDOK)
	{
		bmDraIsDirty = true;
		// Aufnahme in die Map, um Ver�nderungen sp�ter zu verwerfen
		// Zeiger auf Datensatz in die Map
		omTempDraMap.SetAt((void *)polDraData->Urno,polDraData);
		// Grid neu f�llen
		FillAbsenceGrid();
	}
	delete polDummyDrr;

	OnGridLButton(NULL, NULL);
	CCS_CATCH_ALL;
}

//************************************************************************************
// Pr�ft, ob die Eingaben g�ltig sind
//************************************************************************************

bool CTageslisteDlg::IsInputValid() 
{
	CCS_TRY;
	bool blResult = true;
	CString olErrorString;
	// Schichtzeiten pr�fen
	if(	!m_Avfr_Date.GetStatus()	|| 
		!m_Avfr_Time.GetStatus()	||
		!m_Avto_Date.GetStatus()	|| 
		!m_Avto_Time.GetStatus())
	{
		olErrorString += LoadStg(IDS_STRING1865);
		blResult = false;
	}
	
	CString olFrom,olTo,olLength;
	
	m_Sbfr_Time.GetWindowText(olFrom);
	m_Sbto_Time.GetWindowText(olTo);
	
	// Beide Felder m�ssen gef�llt sein
	if (!olFrom.IsEmpty() && !olTo.IsEmpty())
	{
		// Pausenlage pr�fen
		if(	!m_Sbfr_Time.GetStatus() || !m_Sbto_Time.GetStatus())
		{
			olErrorString += LoadStg(IDS_STRING1866);
			blResult = false;
		}
	}
	
	m_Sblu.GetWindowText(olLength);
	// Feld mu� gef�llt sein
	if (!olLength.IsEmpty())
	{
		// Pausenl�nge pr�fen
		if(	!m_Sblu.GetStatus())
		{
			olErrorString += LoadStg(IDS_STRING1867);
			blResult = false;
		}
	}
	
	// Fehlermeldung ausgeben
	if (!blResult)
	{
		MessageBox(olErrorString,"Rostering",MB_ICONWARNING);
	}
	return blResult;
	CCS_CATCH_ALL;	
	return false;
}

//********************************************************************************
// Elemente auf "grau" schalten wenn bpShow = false
//********************************************************************************

void CTageslisteDlg::ShowControls(bool bpShow) 
{
	CCS_TRY;
	// Buttons
	((CButton*) GetDlgItem(IDC_D_DELETEDRD))->EnableWindow(false);
	((CButton*) GetDlgItem(IDC_D_INSERTDRD))->EnableWindow(bpShow);
	((CButton*) GetDlgItem(IDC_D_EDITDRD))->EnableWindow(false);
	((CButton*) GetDlgItem(IDC_D_DELETEDRA))->EnableWindow(false);
	((CButton*) GetDlgItem(IDC_D_INSERTDRA))->EnableWindow(bpShow);
	((CButton*) GetDlgItem(IDC_D_EDITDRA))->EnableWindow(false);
	((CButton*) GetDlgItem(IDC_C_SAP))->EnableWindow(bpShow);
	((CButton*) GetDlgItem(IDC_B_CREATEDTOUR))->EnableWindow(bpShow);
	((CButton*) GetDlgItem(IDC_B_CANCELDOUBLE))->EnableWindow(bpShow);
	((CComboBox*) GetDlgItem(IDC_SHIFT_COMBOBOX))->EnableWindow(bpShow);
	((CComboBox*) GetDlgItem(IDC_C_ACTUAL_SHIFT_FUNC))->EnableWindow(bpShow);
	((CEdit*) GetDlgItem(IDC_E_ZCODE))->EnableWindow(bpShow);
	((CEdit*) GetDlgItem(IDC_E_ZCODE2))->EnableWindow(bpShow);
	((CEdit*) GetDlgItem(IDC_E_ZCODE3))->EnableWindow(bpShow);
	((CCSEdit*) GetDlgItem(IDC_BUFU))->SetReadOnly(!bpShow);

	((CButton*) GetDlgItem(IDC_B_SHIFTIS))->EnableWindow(bpShow);
	((CButton*) GetDlgItem(IDOK))->EnableWindow(bpShow);

	SetReadOnlyColored(IDC_E_SCOD,!bpShow);
	SetReadOnlyColored(IDC_AVFR_DATE,!bpShow);
	SetReadOnlyColored(IDC_AVFR_TIME,!bpShow);
	SetReadOnlyColored(IDC_AVTO_DATE,!bpShow);
	SetReadOnlyColored(IDC_AVTO_TIME,!bpShow);

	((CCSEdit*) GetDlgItem(IDC_SBFR_TIME))->SetReadOnly(!bpShow);
	((CCSEdit*) GetDlgItem(IDC_SBTO_TIME))->SetReadOnly(!bpShow);
	((CCSEdit*) GetDlgItem(IDC_SBLU))->SetReadOnly(!bpShow);
	((CCSEdit*) GetDlgItem(IDC_REMARK))->SetReadOnly(!bpShow);
	
	CCS_CATCH_ALL;	
}

//********************************************************************************
// Controls selektiv auf grau setzen abh�ngig vom imScodType
// mit ShowControls(true/false) kann alles zur�ckgesetzt werden
//********************************************************************************

void CTageslisteDlg::ShowControlsSelective()
{
	CCS_TRY;
	bool blShift = imScodType == CODE_IS_BSD;

	// Buttons
	((CButton*) GetDlgItem(IDC_D_DELETEDRD))->EnableWindow(false);
	((CButton*) GetDlgItem(IDC_D_INSERTDRD))->EnableWindow(blShift);
	((CButton*) GetDlgItem(IDC_D_EDITDRD))->EnableWindow(false);
	((CButton*) GetDlgItem(IDC_D_DELETEDRA))->EnableWindow(false);
	((CButton*) GetDlgItem(IDC_D_INSERTDRA))->EnableWindow(blShift);
	((CButton*) GetDlgItem(IDC_D_EDITDRA))->EnableWindow(false);
	((CButton*) GetDlgItem(IDC_C_SAP))->EnableWindow(blShift);
	((CButton*) GetDlgItem(IDC_B_CREATEDTOUR))->EnableWindow(blShift);
	((CButton*) GetDlgItem(IDC_B_CANCELDOUBLE))->EnableWindow(blShift);

	((CComboBox*) GetDlgItem(IDC_SHIFT_COMBOBOX))->EnableWindow(true);
	((CComboBox*) GetDlgItem(IDC_C_ACTUAL_SHIFT_FUNC))->EnableWindow(blShift);

	// bda: diese Stelle schaltet besondere Abwesenheiten aus!!!
	// zum Einschalten mu� diese Zeile aktiviert werden!!!
	bool blEnableZcode = blShift || bmWork;

	((CEdit*) GetDlgItem(IDC_E_ZCODE))->EnableWindow(blShift);
	((CEdit*) GetDlgItem(IDC_E_ZCODE2))->EnableWindow(blShift);
	((CEdit*) GetDlgItem(IDC_E_ZCODE3))->EnableWindow(blEnableZcode);
	((CCSEdit*) GetDlgItem(IDC_BUFU))->SetReadOnly(!blShift);
	((CButton*) GetDlgItem(IDC_BUTTONINFO))->EnableWindow(true);
	((CButton*) GetDlgItem(IDC_B_SHIFTIS))->EnableWindow(true);
	((CButton*) GetDlgItem(IDOK))->EnableWindow(true);
	((CButton*) GetDlgItem(IDCANCEL))->EnableWindow(true);

	SetReadOnlyColored(IDC_E_SCOD,false);
	SetReadOnlyColored(IDC_AVFR_DATE,false);
	SetReadOnlyColored(IDC_AVFR_TIME,false);
	SetReadOnlyColored(IDC_AVTO_DATE,false);
	SetReadOnlyColored(IDC_AVTO_TIME,false);
	((CCSEdit*) GetDlgItem(IDC_SBFR_TIME))->SetReadOnly(false);
	((CCSEdit*) GetDlgItem(IDC_SBTO_TIME))->SetReadOnly(false);
	((CCSEdit*) GetDlgItem(IDC_SBLU))->SetReadOnly(false);
	((CCSEdit*) GetDlgItem(IDC_REMARK))->SetReadOnly(false);

	CCS_CATCH_ALL;	
}

/**************************************************************************************
Editfeld enablen/disablen, bkColor auf yellow/gray
**************************************************************************************/
void CTageslisteDlg::SetReadOnlyColored(int ipItem, bool bpReadOnly)
{
	((CCSEdit*) GetDlgItem(ipItem))->SetReadOnly(bpReadOnly);
	((CCSEdit*) GetDlgItem(ipItem))->SetBKColor(bpReadOnly ? LTGRAY : LTYELLOW);
}

//************************************************************************************
// OK -> Daten nach Pr�fung in die DRR Struktur kopieren
//************************************************************************************

void CTageslisteDlg::OnOK() 
{
	// ACHTUNG!!! Der ganze interne Funktionalit�t soll in ReadAndSaveData() stehen, 
	// hier d�rfen nur diese zwei Aufrufe sein (wegen OnCancelDoubleTour())
	if (ReadAndSaveData() == true)
	{
		CDialog::OnOK();
	}
	else
	{
		return;
	}
}

/*************************************************************************************
Liest alles und speichert
*************************************************************************************/
bool CTageslisteDlg::ReadAndSaveData(bool bpCancelDoubleTour/*= false*/)
{
	CCS_TRY;
	// dies killt bei Bedarf Fokus bei Children, so das sie ihre Daten updaten k�nnen	
	GetDlgItem(IDOK)->SetFocus();	
	
	UpdateData(true);
	
	// Alle Eingaben auf G�ltigkeit 
	if (!IsInputValid())
		return false;
	
	AfxGetApp()->DoWaitCursor(1);
	
	COleDateTime olDateFrom,olDateTo,olTimeFrom,olTimeTo,olOleBreakFrom, olOleBreakTo;
	// Zeiten formatieren
	CString olFormatStringFrom,olFormatStringTo,olBreakFrom,olBreakTo,olBreakLength;
	
	// Start und Ende der Schicht aus der Oberfl�che einlesen
	ReadShiftTimes(IS_SHIFT,olTimeFrom,olTimeTo);
	
	// wir erstellen eine Kopie des aktuellen pomDrr, um im Fehlerfall alles wiederherzustellen
	DRRDATA olDrrSave;
	ogDrrData.CopyDrr(&olDrrSave, pomDrr);
	
	// Startzeit
	if( olTimeFrom.GetStatus() == COleDateTime::valid)
	{
		pomDrr->Avfr = olTimeFrom;
	}
	else
	{
		RemoveWaitCursor();
		AfxMessageBox(LoadStg(IDS_NO_DATE_TIME),MB_ICONSTOP);
		ogDrrData.CopyDrr(pomDrr, &olDrrSave);
		RemoveWaitCursor();
		return false;
	}
	
	// Endzeit
	if( olTimeTo.GetStatus() == COleDateTime::valid)
	{
		pomDrr->Avto = olTimeTo;
	}
	else
	{
		RemoveWaitCursor();
		AfxMessageBox(LoadStg(IDS_NO_DATE_TIME),MB_ICONSTOP);
		ogDrrData.CopyDrr(pomDrr, &olDrrSave);
		return false;
	}
	
	CString olText;
	// Bemerkung
	m_Remark.GetWindowText(olText);
	strcpy (pomDrr->Rema,olText);
	
	// B�ndelfunk Nummer
	m_Bufu.GetWindowText(olText);
	strcpy (pomDrr->Bufu,olText);
	
	// �bergabe an SAP
	if (m_C_SAP.GetCheck() == 1)
		strcpy(pomDrr->Expf, "A");
	else
		strcpy(pomDrr->Expf, " ");
	
	// Schichtcode
	m_Scod.GetWindowText(olText);
	
	imScodType = ogOdaData.IsODAAndIsRegularFree(olText);
	if (imScodType == CODE_IS_BSD)
	{

		CString olFctc;
		int ilSelItem;
		ilSelItem = m_C_ActualShiftFunc.GetCurSel();
		if (ilSelItem != LB_ERR)
		{
			m_C_ActualShiftFunc.GetLBText(ilSelItem, olFctc);
			if (olFctc.GetLength() > 4)
				olFctc = olFctc.Left(5);
			olFctc.TrimRight();
		}
		
		if (!olFctc.IsEmpty())
		{
			PFCDATA* polPfcData = ogPfcData.GetPfcByFctc(olFctc);
			if (polPfcData == NULL)
			{
				// Funktion %s existiert nicht*INTXT*
				CString olMessage;
				olMessage.Format(LoadStg(IDS_STRING180),olFctc);
				AfxMessageBox(olMessage,MB_ICONSTOP);
				ogDrrData.CopyDrr(pomDrr, &olDrrSave);
				return false;
			}
		}
		else
		{
			//Bitte geben Sie eine Funktion ein!*INTXT*
			CString olMessage = LoadStg(IDS_STRING63483);
			AfxMessageBox(olMessage,MB_ICONSTOP);
			ogDrrData.CopyDrr(pomDrr, &olDrrSave);
			return false;
		}
	}

	if(imScodType != CODE_IS_ODA_FREE && imScodType != CODE_IS_ODA_REGULARFREE)
	{
		if(!ogBsdData.IsBsdCode(olText))
		{
			RemoveWaitCursor();
			AfxMessageBox(LoadStg(IDS_STRING1870),MB_ICONSTOP);
			ogDrrData.CopyDrr(pomDrr, &olDrrSave);
			
			return false;
		}
	}
	
	if (strcmp(pomDrr->Scod,olText) != 0)
	{
		strcpy (pomDrr->Scoo,pomDrr->Scod);
		strcpy (pomDrr->Scod,olText);
	}

	// Urno der Schicht setzen
	pomDrr->Bsdu = lmShiftUrno;
	
	// Fctc der Schicht setzen
	CString olFctc;
	int ilSelItem;
	ilSelItem = m_C_ActualShiftFunc.GetCurSel();
	if (ilSelItem != LB_ERR)
	{
		m_C_ActualShiftFunc.GetLBText(ilSelItem, olFctc);
		if (olFctc.GetLength() > 4)
			olFctc = olFctc.Left(5);
		olFctc.TrimRight();
	}
	omFctc = olFctc;
	strcpy(pomDrr->Fctc,(LPCTSTR)omFctc);
	
	//----------------------------------------------------------------------------------
	// Pausenlage
	//----------------------------------------------------------------------------------
	m_Sbfr_Time.GetWindowText(olBreakFrom);
	m_Sbto_Time.GetWindowText(olBreakTo);
	int ilHour,ilMin, ilDay;
	CString olBreakError = "";
	
	// Sollte keine Pausenlage eingegeben worden sein, Status auf null setzten
	CedaDataHelper::DeleteExtraChars(olBreakFrom);
	CedaDataHelper::DeleteExtraChars(olBreakTo);
	
	bool blNoBreakEntered = olBreakFrom.IsEmpty() || olBreakTo.IsEmpty() || (olBreakFrom == olBreakTo);
	
	if(blNoBreakEntered)
	{
		// Kein Wert vorhanden,, auf null setzen
		pomDrr->Sbfr.SetStatus(COleDateTime::null);
		pomDrr->Sbto.SetStatus(COleDateTime::null);
	}
	else
	{

		int ilMonth;
		int ilYear;
		// Uhrzeit aus String ermitteln
		ilHour = atoi(olBreakFrom.Left(2));
		ilMin = atoi(olBreakFrom.Right(2));
		ilDay = (ilHour >= olTimeFrom.GetHour()) ? olTimeFrom.GetDay() : olTimeTo.GetDay();
		ilDay = (ilHour > olTimeFrom.GetHour()) ? olTimeFrom.GetDay() : olTimeTo.GetDay();
		ilMonth = (ilHour > olTimeFrom.GetHour()) ? olTimeFrom.GetMonth() : olTimeTo.GetMonth();
		ilYear = (ilHour > olTimeFrom.GetHour()) ? olTimeFrom.GetYear() : olTimeTo.GetYear();

		olOleBreakFrom.SetDateTime(ilYear,ilMonth,ilDay,ilHour,ilMin,0);
		// Zeit und Datum pr�fen
		if ((olOleBreakFrom < olTimeFrom) || (olOleBreakFrom > olTimeTo))
		{
			// ausserhalb des g�ltigen Bereichs -> Fehlermeldung formatieren
			RemoveWaitCursor();
			AfxMessageBox(LoadStg(IDS_STRING1015),MB_ICONSTOP);
			m_Sbfr_Time.SetSel(0,-1);
			m_Sbfr_Time.SetFocus();
			ogDrrData.CopyDrr(pomDrr, &olDrrSave);
			return false;
		}
		
		// Uhrzeit aus String ermitteln
		ilHour = atoi(olBreakTo.Left(2));
		ilMin = atoi(olBreakTo.Right(2));
		ilDay = (ilHour > olTimeFrom.GetHour()) ? olTimeFrom.GetDay() : olTimeTo.GetDay();
		ilMonth = (ilHour > olTimeFrom.GetHour()) ? olTimeFrom.GetMonth() : olTimeTo.GetMonth();
		ilYear = (ilHour > olTimeFrom.GetHour()) ? olTimeFrom.GetYear() : olTimeTo.GetYear();
		
		olOleBreakTo.SetDateTime(ilYear,ilMonth,ilDay,ilHour,ilMin,0);
		// Zeit und Datum pr�fen
		if ((olOleBreakTo < olTimeFrom) || (olOleBreakTo > olTimeTo))
		{
			// ausserhalb des g�ltigen Bereichs -> Fehlermeldung formatieren
			RemoveWaitCursor();
			AfxMessageBox(LoadStg(IDS_STRING1016),MB_ICONSTOP);
			m_Sbto_Time.SetSel(0,-1);
			m_Sbto_Time.SetFocus();
			ogDrrData.CopyDrr(pomDrr, &olDrrSave);
			return false;
		}
		
		// sichergehen, dass Ende nicht vor Start liegt
		if (olOleBreakFrom > olOleBreakTo)
		{
			// ung�ltige Pausenlage
			RemoveWaitCursor();
			AfxMessageBox(LoadStg(IDS_STRING1017),MB_ICONSTOP);
			m_Sbfr_Time.SetSel(0,-1);
			m_Sbfr_Time.SetFocus();
			ogDrrData.CopyDrr(pomDrr, &olDrrSave);
			return false;
		}
		
		// alles OK -> Werte speichern
		pomDrr->Sbfr = olOleBreakFrom;
		pomDrr->Sbto = olOleBreakTo;
	}

	// check if there was a change of the work group
	if (ogCCSParam.GetParamValue(ogAppl,"ID_PROD_CUSTOMER") == "SIN")
	{
		CString olOldWgpc;
		CString olNewWgpc;
		CString olOldFctc;
		CString olNewFctc;
		CString olSwgCode;

		COleDateTime olTime;
		CedaDataHelper::DateStringToOleDateTime(pomDrr->Sday,olTime);
		olSwgCode = ogSwgData.GetWgpBySurnWithTime(pomDrr->Stfu,olTime);

		// get the OLD values of working group and function
		DRGDATA* prlDrg = ogDrgData.GetDrgByKey(pomDrr->Sday, pomDrr->Stfu, pomDrr->Drrn);
		if (prlDrg != 0)
		{
			olOldWgpc = prlDrg->Wgpc;
		}
		else
		{
			olOldWgpc = olSwgCode;
		}
		olOldFctc = olDrrSave.Fctc;

		// get the NEW values of working group and function
		m_C_Group.GetWindowText(olNewWgpc);
		olNewWgpc = olNewWgpc.Left(5);
		olNewWgpc.TrimRight();
		olNewFctc = pomDrr->Fctc;

		// if the new code is a shift (not an absence), the working group or the function (within this group)
		// changed and the employee was assigned to a group before, the user has to select a working group code.
		if ((imScodType == CODE_IS_BSD) &&
			(olNewWgpc != olOldWgpc || olNewFctc != olOldFctc) /*&& olOldWgpc.GetLength()*/)
		{
			WGPDATA *prlWgpData = ogWgpData.GetWgpByWgpc(olNewWgpc);
			if (prlWgpData != NULL || olNewWgpc.GetLength() == 0) //if olNewWgpc is empty, it is like a delete
			{
				if (olNewWgpc == olSwgCode)
				{
					// there is no difference to the basic data group assignment, so we have to delete the drg-record
					if (prlDrg != NULL)
					{
						ogDrgData.Delete(prlDrg);
						bmChangedDrg = true;
					}
				}
				else
				{
					// there is a difference to the basic data group assignment, so we have to update/insert drg-record
					bool blInsert = false;
					if (prlDrg == NULL)
					{
						//create new DRG-record
						prlDrg = ogDrgData.CreateDrgData(pomDrr->Stfu, pomDrr->Sday, pomDrr->Drrn);
						blInsert = true;
					}
					strcpy(prlDrg->Wgpc, olNewWgpc);
					strcpy(prlDrg->Fctc, pomDrr->Fctc);
					strcpy(prlDrg->Drrn, pomDrr->Drrn);

					if (blInsert == true)
					{
						ogDrgData.Insert(prlDrg);
					}
					else
					{
						ogDrgData.Update(prlDrg);
					}
					bmChangedDrg = true;
				}
			}
			else
			{
				//IDS_STRING114: The new working group '%s' could not be found!
				CString olMsg;
				olMsg.Format (LoadStg(IDS_STRING114), olNewWgpc);
				AfxMessageBox(olMsg,MB_ICONSTOP);
				ogDrrData.CopyDrr(pomDrr, &olDrrSave);
				return false;
			}
		}
	}

	//ZCODE
	m_E_ZCode1.GetWindowText(pomDrr->Drs1,DRS1_LEN+2);
	m_E_ZCode3.GetWindowText(pomDrr->Drs3,DRS3_LEN+2);
	m_E_ZCode4.GetWindowText(pomDrr->Drs4,DRS4_LEN+2);

	ilSelItem = m_C_SUB1SUB2.GetCurSel();
	if (ilSelItem != LB_ERR)
	{
		if (ilSelItem == 0)
		{
			strcpy(pomDrr->Drs2,"1");
		}
		else
		{
			strcpy(pomDrr->Drs2,"2");
		}
	}
	else
	{
		strcpy(pomDrr->Drs2," ");
	}
	
	//--------------------------------------------------------
	// Pausenl�nge
	//--------------------------------------------------------
	m_Sblu.GetWindowText(olBreakLength);	// 5 15 115 1:15 1:

	int ilInd = olBreakLength.Find(':');
	ilHour = 0;
	ilMin = 0;
	int ilStrLength = olBreakLength.GetLength();

	if(ilInd != -1)
	{
		ilHour = atoi(olBreakLength.Left(ilInd));
		if(ilInd < ilStrLength-1)
		{
			ilMin = atoi(olBreakLength.Mid(ilInd+1,ilStrLength-ilInd-1)) % 60;
		}
	}
	else
	{
		ilHour = atoi(olBreakLength.Left(ilStrLength-2));
		ilMin = atoi(olBreakLength.Right(2)) % 60;
	}
	
	// DRRTAB.SBLU nimmt nur Minuten auf
	sprintf(pomDrr->Sblu,"%d",ilMin + ilHour*60);

	// Arbeitsregeln kontrollieren
	if(!ogShiftCheck.DutyRosterCheckIt(pomDrr, pomViewInfo->bDoConflictCheck, DRR_CHANGE, false))
	{
		ogDrrData.CopyDrr(pomDrr, &olDrrSave);
		RemoveWaitCursor();
		return false;
	}

	if(ogShiftCheck.IsWarning())
	{
		((DutyRoster_View*)m_pParentWnd)->ForwardWarnings();
	}

	// Pr�fen, ob Doppeltouren vorliegen und ggf. ver�ndern
	if (!ogDrrData.ChangeConnectedScod(pomDrr,pomDrr->Bsdu,omFctc, pomViewInfo->bCheckMainFuncOnly))
	{
		RemoveWaitCursor();
		ogDrrData.CopyDrr(pomDrr, &olDrrSave);
		return false;
	}

	if (bpCancelDoubleTour == true)
	{
		ogDrrData.CancelDoubleTour(pomDrr);
	}

	// Alles fertig, genehmigt, kann gespeichert werden
	SaveDraInformation();
	SaveDrdInformation();
	if (bmDraIsDirty || bmDrdIsDirty || bmChangedDrg || ogDrrData.IsDrrChanged(pomDrr, &olDrrSave) == true)
	{
		ogDrrData.SetDrrDataLastUpdate(pomDrr);
		ogDrrData.Update(pomDrr,pomDrr->Drs2);
	}

	// �nderung der Schichtnummer bearbeiten
	// welche DRR-Nummer wurde ausgew�hlt
	CString olSelDrrn;
	int ilSelIndex = m_CB_ShiftNumber.GetCurSel();
	if (ilSelIndex != CB_ERR) m_CB_ShiftNumber.GetLBText(ilSelIndex,olSelDrrn);

	// gibt es was zu tun?
	if (olSelDrrn != CString(pomDrr->Drrn)) 
	{
		// beim BC-Handler vor�bergehend abmelden, da sonst Abwesenheits- und 
		// Abweichungs-BCs zuhauf bearbeitet werden
		Register(false);
		// DRR-Nummern tauschen
		ogDrrData.SwapDrrn(pomDrr,olSelDrrn);
		// wieder beim BC-Handler anmelden
		Register(true);
	}

	RemoveWaitCursor();
	return true;
	CCS_CATCH_ALL;
	return false;
}
//************************************************************************************
// Extra Informationen anzeigen
//************************************************************************************

void CTageslisteDlg::OnButtoninfo() 
{
	CCS_TRY;
	CStringArray olStringArray;
	COleDateTime olTmp;

	if (pomDrr->Cdat.GetStatus() == COleDateTime::valid)
	{
		olTmp = pomDrr->Cdat;
		ogBasicData.ConvertDateToLocal(olTmp);
		olStringArray.Add(olTmp.Format("%d.%m.%Y"));
		olStringArray.Add(olTmp.Format("%H:%M"));
	}
	else
	{
		olStringArray.Add("");
		olStringArray.Add("");
	}

	if (pomDrr->Lstu.GetStatus() == COleDateTime::valid)
	{
		olTmp = pomDrr->Lstu;
		ogBasicData.ConvertDateToLocal(olTmp);
		olStringArray.Add(olTmp.Format("%d.%m.%Y"));
		olStringArray.Add(olTmp.Format("%H:%M"));
	}
	else
	{
		olStringArray.Add("");
		olStringArray.Add("");
	}

	olStringArray.Add(pomDrr->Usec);
	olStringArray.Add(pomDrr->Useu);

	CInfoDlg olDlg(this,&olStringArray);
	olDlg.DoModal();
	CCS_CATCH_ALL;	
}

//************************************************************************************
// Doppeltour anlegen
//************************************************************************************

void CTageslisteDlg::OnInsertDoubleTour()
{
	CCS_TRY;
	DRSDATA* polDrs;
	
	// DRS suchen, der �ber die DRR-Urno <lpDrru> mit dem entsprechenden DRR verkn�pft ist
	polDrs = ogDrsData.GetDrsByDrru(pomDrr->Urno);
	
	// G�ltigkeitstest, falls Drs nicht vorhanden, neuen Drs erstellen
	if (polDrs == NULL)
	{
		polDrs = ogDrsData.CreateDrsData(pomDrr->Stfu, pomDrr->Sday, pomDrr->Urno);
		if (polDrs == NULL) return;
	}
	
	CDoubleTourDlg	plDlg(this,pomDrr,polDrs,pomStfData,(DutyRoster_View*)m_pParentWnd);
	plDlg.DoModal();
	SetDrsItems();
	CCS_CATCH_ALL;	
}

//************************************************************************************
// Doppeltour trennen
//************************************************************************************

void CTageslisteDlg::OnCancelDoubleTour()
{
	// Alle Berechnungen und Meldungen �bernimmt ogDrrData.
	
	ReadAndSaveData(true);
	//CDialog::OnOK();
}

//************************************************************************************
// DRS Daten setzen (Z_CODE und Doppeltouren)
//************************************************************************************

void CTageslisteDlg::SetDrsItems()
{
	CCS_TRY;
	// DRS Daten setzen
	DRSDATA * polDrs = ogDrsData.GetDrsByDrru(pomDrr->Urno);
	if (polDrs != NULL)
	{
		CString olFullName1 = "", olFullName2 = "";
		// Namen f�r die Doppeltouren setzen
		STFDATA *prlStf = ogStfData.GetStfByUrno( pomDrr->Stfu);
		if( prlStf != NULL)
		{
			olFullName1 = CBasicData::GetFormatedEmployeeName(pomViewInfo->iEName, prlStf, " ", "");
		}
		
		// der 2. MA
		prlStf = ogStfData.GetStfByUrno(polDrs->Ats1);
		if( prlStf != NULL)
		{
			olFullName2 = CBasicData::GetFormatedEmployeeName(pomViewInfo->iEName, prlStf, " ", "");
		}		
		
		if (CString(polDrs->Stat) == "T")
		{
			SetDlgItemText(IDC_STATIC_1087,olFullName1);
			SetDlgItemText(IDC_STATIC_1088,olFullName2);
		}
		if (CString(polDrs->Stat) == "S")
		{
			SetDlgItemText(IDC_STATIC_1087,olFullName2);
			SetDlgItemText(IDC_STATIC_1088,olFullName1);
		}
		((CButton*) GetDlgItem(IDC_B_CREATEDTOUR))->EnableWindow(false);
		((CButton*) GetDlgItem(IDC_B_CANCELDOUBLE))->EnableWindow(true);
	}
	
	UpdateData(false);
	CCS_CATCH_ALL;
}

//************************************************************************************
// InitDrrnComboBox: f�llt die ComboBox mit den DRR-Nummern mit Zahlen von 1
//	bis max. DRR-Nummer und selektiert die des bearbeiteten DRRs
// R�ckgabe:	keine
//************************************************************************************

void CTageslisteDlg::InitDrrnComboBox()
{
	CCS_TRY;
	// alle vorhandenen Eintr�ge l�schen
	while (m_CB_ShiftNumber.GetCount() > 0) m_CB_ShiftNumber.DeleteString(0);
	
	// DRR-Liste lesen
	int ilMaxDrrn = ogDrrData.GetDrrListByKeyWithoutDrrn(CString(pomDrr->Sday),pomDrr->Stfu,CString(pomDrr->Rosl),&omDrrList);
	CString olMaxDrrn;
	
	// Zahlen bis <ilMaxDrrn> in die Combobox eintragen
	for (int ilCount=1; ilCount<=ilMaxDrrn; ilCount++)
	{
		olMaxDrrn.Format("%d",ilCount);
		m_CB_ShiftNumber.InsertString(ilCount-1,olMaxDrrn);
	}
	// die DRR-Nummer dieses Drr ausw�hlen
	m_CB_ShiftNumber.SelectString(-1,pomDrr->Drrn);
	CCS_CATCH_ALL;
}

void CTageslisteDlg::InitSub1Sub2ComboBox()
{
	CString olPrivStat = ogPrivList.GetStat("DUTYDAILYLIST_SUB1SUB2");

	if (olPrivStat != "1")
	{
		m_C_SUB1SUB2.ShowWindow(SW_HIDE);
		GetDlgItem (IDC_S_SUB1SUB2)->ShowWindow(SW_HIDE);
	}
	else
	{
		// initializing the combo
		CDC *pdc = m_C_SUB1SUB2.GetDC();
		CFont olFont;
		CFont *pOldFont	= pdc->SelectObject(&olFont);
		CSize olSizeSub1;
		CSize olSizeSub2;
		long ilWidth;

		CString olInsertSub1;
		CString olInsertSub2;

		olInsertSub1 = LoadStg(IDS_STRING202) + "  " +  LoadStg(IDS_STRING206);
		olInsertSub2 = LoadStg(IDS_STRING203) + "  " +  LoadStg(IDS_STRING214);

		m_C_SUB1SUB2.ResetContent();
		m_C_SUB1SUB2.InsertString(-1, olInsertSub1);
		m_C_SUB1SUB2.InsertString(-1, olInsertSub2);

		olSizeSub1 = pdc->GetTextExtent(olInsertSub1);
		olSizeSub2 = pdc->GetTextExtent(olInsertSub2);
		ilWidth = max(olSizeSub1.cx, olSizeSub2.cx) - 40;
		m_C_SUB1SUB2.SetDroppedWidth((UINT)ilWidth);
		pdc->SelectObject(&olFont);

		//setting the initial selection
		if (strcmp(pomDrr->Drs2, "2") == 0)
		{
			m_C_SUB1SUB2.SetCurSel(1);
		}
		else
		{
			m_C_SUB1SUB2.SetCurSel(0);
		}
	}
}

void CTageslisteDlg::OnSelchangeSub1Sub2()
{
	bmChangedSub1Sub2 = true;
}

void CTageslisteDlg::ChangeSub1Sub2ToDefault()
{
	if (bmChangedSub1Sub2 != true)
	{
		m_C_SUB1SUB2.SetCurSel(0);
	}
}

void CTageslisteDlg::InitFctcComboBox()
{
	CCS_TRY;
	CString olLine;
	CString olTmp;
	int i;

	m_C_ActualShiftFunc.ResetContent();
	m_C_ActualShiftFunc.SetFont(&ogCourier_Regular_8);

	for (i = 0; i < ogPfcData.omData.GetSize(); i++)
	{
		olTmp.Format("'%s'", ogPfcData.omData[i].Fctc);
		if (pomViewInfo->oPfcStatFctcs.Find (olTmp) > -1 || strcmp(ogPfcData.omData[i].Fctc, pomDrr->Fctc) == NULL)
		{
			olLine.Format("%-8s - %-40s", ogPfcData.omData[i].Fctc, ogPfcData.omData[i].Fctn);
			m_C_ActualShiftFunc.AddString(olLine);
		}
	}
	for (i = 0; i < m_C_ActualShiftFunc.GetCount(); i++)
	{
		m_C_ActualShiftFunc.GetLBText(i, olTmp);
		olTmp = olTmp.Left(5);
		olTmp.TrimRight();

		if (olTmp == omFctc)
		{
			m_C_ActualShiftFunc.SetCurSel(i);
			break;
		}
	}
	CCS_CATCH_ALL;
}

//************************************************************************************
// Setzt die Farbe der Gruppenrahmen
//************************************************************************************

HBRUSH CTageslisteDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor) 
{
	CCS_TRY;
	HBRUSH hbr = 0;
	long llRc;
	CWnd *polWnd;
	
	polWnd = GetDlgItem(IDC_S_TagesdatenlautStammdaten);
	if (polWnd->m_hWnd == pWnd->m_hWnd)
	{
		llRc = pDC->SetBkColor(SILVER);
		llRc = pDC->SetTextColor(PURPLE);//BLUE
		omBrush.DeleteObject();
		omBrush.CreateSolidBrush(SILVER);
		hbr = omBrush;	
		return hbr;
	}
	polWnd = GetDlgItem(IDC_S_Personaldaten);
	if (polWnd->m_hWnd == pWnd->m_hWnd)
	{
		llRc = pDC->SetBkColor(SILVER);
		llRc = pDC->SetTextColor(PURPLE);
		omBrush.DeleteObject();
		omBrush.CreateSolidBrush(SILVER);
		hbr = omBrush;	
		return hbr;
	}
	polWnd = GetDlgItem(IDC_S_AllgemeinDaten);
	if (polWnd->m_hWnd == pWnd->m_hWnd)
	{
		llRc = pDC->SetBkColor(SILVER);
		llRc = pDC->SetTextColor(PURPLE);
		omBrush.DeleteObject();
		omBrush.CreateSolidBrush(SILVER);
		hbr = omBrush;	
		return hbr;
	}
	polWnd = GetDlgItem(IDC_S_Abweichung2);
	if (polWnd->m_hWnd == pWnd->m_hWnd)
	{
		llRc = pDC->SetBkColor(SILVER);
		llRc = pDC->SetTextColor(PURPLE);
		omBrush.DeleteObject();
		omBrush.CreateSolidBrush(SILVER);
		hbr = omBrush;	
		return hbr;
	}
	polWnd = GetDlgItem(IDC_S_Anwesenheit);
	if (polWnd->m_hWnd == pWnd->m_hWnd)
	{
		llRc = pDC->SetBkColor(SILVER);
		llRc = pDC->SetTextColor(PURPLE);
		omBrush.DeleteObject();
		omBrush.CreateSolidBrush(SILVER);
		hbr = omBrush;	
		return hbr;
	}
	polWnd = GetDlgItem(IDC_S_Abwesenheit);
	if (polWnd->m_hWnd == pWnd->m_hWnd)
	{
		llRc = pDC->SetBkColor(SILVER);
		llRc = pDC->SetTextColor(PURPLE);
		omBrush.DeleteObject();
		omBrush.CreateSolidBrush(SILVER);
		hbr = omBrush;	
		return hbr;
	}
	polWnd = GetDlgItem(IDC_S_Doppeltouren);
	if (polWnd->m_hWnd == pWnd->m_hWnd)
	{
		llRc = pDC->SetBkColor(SILVER);
		llRc = pDC->SetTextColor(PURPLE);
		omBrush.DeleteObject();
		omBrush.CreateSolidBrush(SILVER);
		hbr = omBrush;	
		return hbr;
	}
	polWnd = GetDlgItem(IDC_S_Bemerkungen);
	if (polWnd->m_hWnd == pWnd->m_hWnd)
	{
		llRc = pDC->SetBkColor(SILVER);
		llRc = pDC->SetTextColor(PURPLE);
		omBrush.DeleteObject();
		omBrush.CreateSolidBrush(SILVER);
		hbr = omBrush;	
		return hbr;
	}
	polWnd = GetDlgItem(IDC_S_Arbeitsgruppe2);
	if (polWnd->m_hWnd == pWnd->m_hWnd)
	{
		llRc = pDC->SetBkColor(SILVER);
		llRc = pDC->SetTextColor(PURPLE);
		omBrush.DeleteObject();
		omBrush.CreateSolidBrush(SILVER);
		hbr = omBrush;	
		return hbr;
	}
	return CDialog::OnCtlColor(pDC,pWnd,nCtlColor);
	CCS_CATCH_ALL;	
	return (HBRUSH)0;
}

//******************************************************************************************************************
//
//******************************************************************************************************************

void CTageslisteDlg::OnSelchangeShiftCombobox() 
{
}

//******************************************************************************************************************
// Sichert die DRD Informationen die lokal gehalten werden in die Datenbank
//******************************************************************************************************************

void CTageslisteDlg::SaveDrdInformation()
{
	if (!bmDrdIsDirty)
		return;
	CCS_TRY;
	//-------------------------------------------------------------------------
	// alle relevanten DRDs ermitteln die bisher in der Datenbank sind.
	// Wichtig hier neu ermitteln, da sich die Daten ge�ndert haben k�nnen.
	// (Andere User per BC)
	ogDrdData.GetDrdMapOfDrr(&omRefDrdMap,pomDrr);
	
	// DRD Informationen speichern
	POSITION rlPos; 
	long llTempUrno,llRefUrno; 
	DRDDATA *prlTempDrd = NULL,*prlRefDrd = NULL; 
	// alle DRDs in <omTempDrdMap> durchgehen
	for(rlPos = omTempDrdMap.GetStartPosition(); rlPos != NULL;)
	{
		prlTempDrd = NULL;
		prlRefDrd = NULL; 
		
		// DRD ermitteln
		omTempDrdMap.GetNextAssoc(rlPos,(void *&)llTempUrno, (void *&)prlTempDrd);
		// Pr�fen ob dieser DRR in der Referenzliste vorhanden ist
		omRefDrdMap.Lookup((void *&)llTempUrno,(void *&)prlRefDrd);
		
		if (prlRefDrd == NULL)
		{
			//----------------------------------------------------------------
			// DRD ist neu
			if (!ogDrdData.Insert(prlTempDrd))
			{
				// Fehler ausgeben
				CString olErrorTxt;
				olErrorTxt.Format("%s %d\n%s",LoadStg(ST_WRITEERR), ogDrdData.imLastReturnCode, ogDrdData.omLastErrorMessage);
				MessageBox(olErrorTxt, LoadStg(ST_FEHLER), MB_ICONEXCLAMATION);
			}
			// Drd markieren, damit er sp�ter NICHT gel�scht wird
			prlTempDrd->IsChanged = DATA_NEW;
		}
		else
		{
			//---------------------------------------------------------------
			// DRD wurde ge�ndert
			ogDrdData.CopyDrd(prlTempDrd,prlRefDrd);
			// Damit auch tats�chlich ein Update gemacht wird
			prlRefDrd->IsChanged = DATA_CHANGED;
			// Update in der Datenbank und Broadcast senden
			if (!ogDrdData.Update(prlRefDrd))
			{
				// Fehler ausgeben
				CString olErrorTxt;
				olErrorTxt.Format("%s %d\n%s",LoadStg(ST_WRITEERR), ogDrdData.imLastReturnCode, ogDrdData.omLastErrorMessage);
				MessageBox(olErrorTxt, LoadStg(ST_FEHLER), MB_ICONEXCLAMATION);
			}
			// Drd markieren, damit er sp�ter gel�scht wird
			prlTempDrd->IsChanged = DATA_CHANGED;
		}
	}
	//-------------------------------------------------------------------------
	// L�schen von DRD's
	// Alle DRD aus der Ref Map durchgehen und pr�fen ob sie in der Tempmap noch 
	// vorhanden sind. Wenn nicht dann wurde der DRD gel�scht
	for(rlPos = omRefDrdMap.GetStartPosition(); rlPos != NULL;)
	{
		prlTempDrd = NULL;
		prlRefDrd = NULL; 
		
		// Ref DRD ermitteln
		omRefDrdMap.GetNextAssoc(rlPos,(void *&)llRefUrno, (void *&)prlRefDrd);
		// Pr�fen ob dieser DRR in der Refernzliste vorhanden ist
		omTempDrdMap.Lookup((void *&)llRefUrno,(void *&)prlTempDrd);
		
		// Es wurde ein DRD gel�scht
		if (prlTempDrd == NULL)
		{
			// DRD auch in der Datenhaltung l�schen
			if (!ogDrdData.Delete(prlRefDrd))
			{
				// Fehler ausgeben
				CString olErrorTxt;
				olErrorTxt.Format("%s %d\n%s",LoadStg(ST_WRITEERR), ogDrdData.imLastReturnCode, ogDrdData.omLastErrorMessage);
				MessageBox(olErrorTxt, LoadStg(ST_FEHLER), MB_ICONEXCLAMATION);
			}
		}
	}
	CCS_CATCH_ALL;	
}

//*************************************************************************************************************
// alle relevanten DRDs ermitteln die bisher in der Datenbank sind.
// F�r jeden bisher vorhandenen DRD wird ein neuer DRD erzeugt
//*************************************************************************************************************

void CTageslisteDlg::CreateTempDrdMap()
{
	CCS_TRY;
	//------------------------------------------------------------------------
	// tempor�re Drdmap l�schen
	// Es d�rfen nur die Drd gel�scht werden, deren Zeiger nicht in die
	// interne Datenhaltung �bergeben wurden
	POSITION rlPos; 
	long llTempUrno; 
	DRDDATA *prlTempDrd; 
	
	for(rlPos = omTempDrdMap.GetStartPosition(); rlPos != NULL;)
	{
		// DRD ermitteln
		omTempDrdMap.GetNextAssoc(rlPos,(void *&)llTempUrno, (void *&)prlTempDrd);
		
		// Drd markieren, damit er sp�ter NICHT gel�scht wird
		if (prlTempDrd->IsChanged != DATA_NEW)
			delete prlTempDrd;
	}
	// DRD-Map leeren
	omTempDrdMap.RemoveAll();
	bmDrdIsDirty = false;
	//------------------------------------------------------------------------
	
	
	ogDrdData.GetDrdMapOfDrr(&omRefDrdMap,pomDrr);
	
	//	POSITION rlPos; 
	long llUrno; 
	DRDDATA *prlDrd; 
	// alle DRDs in <omDrdMap> durchgehen
	for(rlPos = omRefDrdMap.GetStartPosition(); rlPos != NULL; )
	{
		// DRD ermitteln
		omRefDrdMap.GetNextAssoc(rlPos,(void *&)llUrno, (void *&)prlDrd);
		
		// Neuen DRD Datensatz generieren, in welchem nur die Grunddaten gesetzt werden
		DRDDATA* polTempDrdData = ogDrdData.CreateDrdData(pomDrr->Stfu,pomDrr->Sday,pomDrr->Drrn);
		// Urno umsetzen
		polTempDrdData->Urno = llUrno;
		// DRD Daten in neuen DRD kopieren
		ogDrdData.CopyDrd(prlDrd,polTempDrdData);
		// Aufnahme in die TempMap, 
		omTempDrdMap.SetAt((void *)polTempDrdData->Urno,polTempDrdData);
	}
	CCS_CATCH_ALL;	
}

/*************************************************************************************************************
Schichtzeiten von der Oberfl�che einlesen und in COleDateTime umwandeln
ipShift == BASIC_SHIFT - es wird von Basisschicht gelesen
ipShift == IS_SHIFT	- es wird von "Schicht Ist" gelesen
*************************************************************************************************************/

void CTageslisteDlg::ReadShiftTimes(int ipShift, COleDateTime& opTimeFrom,COleDateTime& opTimeTo)
{
	CCS_TRY;
	//----------------------------------------------------------------------------------
	// Schichtzeiten
	//----------------------------------------------------------------------------------
	// Zeiten formatieren
	CString olDateFrom,olDateTo,olTimeFrom,olTimeTo,olFormatStringFrom,olFormatStringTo,
		olBreakFrom,olBreakTo,olBreakLength;
	
	// Schichtzeiten
	if(ipShift == BASIC_SHIFT)
	{
		GetDlgItemText(IDC_STATIC_17,olDateFrom);	// Ist - Von Datum	
		GetDlgItemText(IDC_STATIC_18,olDateTo);		// Ist - Von Zeit	
		GetDlgItemText(IDC_STATIC_19,olTimeFrom);	// Ist - Bis Datum	
		GetDlgItemText(IDC_STATIC_20,olTimeTo);		// Ist - Bis Zeit	
	}
	else if(ipShift == IS_SHIFT)
	{
		m_Avfr_Date.GetWindowText(olDateFrom);
		m_Avto_Date.GetWindowText(olDateTo);
		m_Avfr_Time.GetWindowText(olTimeFrom);
		m_Avto_Time.GetWindowText(olTimeTo);

	}
	
	
	// Alle Extra Zeichen (.:) l�schen.
	CedaDataHelper::DeleteExtraChars(olDateFrom);
	CedaDataHelper::DeleteExtraChars(olTimeFrom);
	CedaDataHelper::DeleteExtraChars(olDateTo);
	CedaDataHelper::DeleteExtraChars(olTimeTo);
	
	if(olDateFrom.GetLength() == 8 && olTimeFrom.GetLength() == 4)
	{
		// Datum und Zeit "von" formatieren
		olFormatStringFrom = olDateFrom.Right(4) + olDateFrom.Mid(2,2) + olDateFrom.Left(2) + olTimeFrom.Left(2) + olTimeFrom.Right(2) +"00";
		// String in OleDateTime umwandeln, und bei Erfolg in die Struktur kopieren
		CedaDataHelper::DateTimeStringToOleDateTime(olFormatStringFrom,opTimeFrom);
	}
	else
	{
		opTimeFrom.SetStatus(COleDateTime::invalid);
	}
	
	if(olDateTo.GetLength() == 8 && olTimeTo.GetLength() == 4)
	{
		
		// Datum und Zeit "bis" formatieren
		olFormatStringTo = olDateTo.Right(4) + olDateTo.Mid(2,2) + olDateTo.Left(2) + olTimeTo.Left(2) + olTimeTo.Right(2) +"00";
		
		CedaDataHelper::DateTimeStringToOleDateTime(olFormatStringTo,opTimeTo);
	}
	else
	{
		opTimeTo.SetStatus(COleDateTime::invalid) ;
	}
	
	CCS_CATCH_ALL;
}

/*************************************************************************************************************
alle relevanten DRAs ermitteln die bisher in der Datenbank sind.
F�r jeden bisher vorhandenen DRA wird ein neuer DRA erzeugt
bool bpCheckDel	wenn true, DELDATA muss gecheckt werden, ob f�r diese Schicht DRAs erstellt werden m�ssten
wenn bpCheckDel == true, , long lpBsdUrno mu� initialisiert werden
*************************************************************************************************************/

void CTageslisteDlg::CreateTempDraMap(bool bpCheckDel, long lpBsdUrno/*=0*/)
{
	CCS_TRY;
	//------------------------------------------------------------------------
	// tempor�re Dramap l�schen
	// Es d�rfen nur die Dra gel�scht werden, deren Zeiger nicht in die
	// interne Datenhaltung �bergeben wurden
	POSITION rlPos; 
	long llTempUrno; 
	DRADATA *prlTempDra; 
	
	for(rlPos = omTempDraMap.GetStartPosition(); rlPos != NULL;)
	{
		// DRA ermitteln
		omTempDraMap.GetNextAssoc(rlPos,(void *&)llTempUrno, (void *&)prlTempDra);
		
		// Pr�fen ob gel�scht werden darf
		if (prlTempDra->IsChanged != DATA_NEW)
			delete prlTempDra;
	}
	// DRD-Map leeren
	omTempDraMap.RemoveAll();
	bmDraIsDirty = false;
	//------------------------------------------------------------------------
	
	if(!bpCheckDel)
	{
		ogDraData.GetDraMapOfDrr(&omRefDraMap,pomDrr);
		
		//	POSITION rlPos; 
		long llUrno; 
		DRADATA *prlDra; 
		// alle DRAs in <omDraMap> durchgehen
		for(rlPos = omRefDraMap.GetStartPosition(); rlPos != NULL; )
		{
			// DRA ermitteln
			omRefDraMap.GetNextAssoc(rlPos,(void *&)llUrno, (void *&)prlDra);
			
			// Neuen DRA Datensatz generieren, in welchem nur die Grunddaten gesetzt werden
			DRADATA* polTempDraData = ogDraData.CreateDraData(pomDrr);
			// Urno umsetzen
			polTempDraData->Urno = llUrno;
			// DRA Daten in neuen DRA kopieren
			ogDraData.CopyDra(prlDra,polTempDraData);
			// Aufnahme in die TempMap, 
			omTempDraMap.SetAt((void *)polTempDraData->Urno,polTempDraData);
		}
	}
	else
	{
		if(ogDrrData.CheckAndSetDraIfDrrBsdDelConnected(pomDrr, lpBsdUrno, &omTempDraMap) == true)
		{
			if(!omTempDraMap.IsEmpty())
				bmDraIsDirty = true;
		}
	}
	
	CCS_CATCH_ALL;
}

//******************************************************************************************************************
// Sichert die DRA Informationen die lokal gehalten werden in die Datenbank
//******************************************************************************************************************

void CTageslisteDlg::SaveDraInformation()
{
	if (!bmDraIsDirty)
		return;
	CCS_TRY;
	//-------------------------------------------------------------------------
	// alle relevanten DRDs ermitteln die bisher in der Datenbank sind.
	// Wichtig hier neu ermitteln, da sich die Daten ge�ndert haben k�nnen.
	// (Andere User per BC)
	ogDraData.GetDraMapOfDrr(&omRefDraMap,pomDrr);
	
	// DRA Informationen speichern
	POSITION rlPos; 
	long llTempUrno,llRefUrno; 
	DRADATA *prlTempDra = NULL,*prlRefDra = NULL; 
	// alle DRAs in <omTempDraMap> durchgehen
	for(rlPos = omTempDraMap.GetStartPosition(); rlPos != NULL;)
	{
		prlTempDra = NULL;
		prlRefDra = NULL; 
		
		// DRA ermitteln
		omTempDraMap.GetNextAssoc(rlPos,(void *&)llTempUrno, (void *&)prlTempDra);
		// Pr�fen ob dieser DRA in der Referenzliste vorhanden ist
		omRefDraMap.Lookup((void *&)llTempUrno,(void *&)prlRefDra);
		
		if (prlRefDra == NULL)
		{
			//----------------------------------------------------------------
			// DRA ist neu
			if (!ogDraData.Insert(prlTempDra))
			{
				// Fehler ausgeben
				CString olErrorTxt;
				olErrorTxt.Format("%s %d\n%s",LoadStg(ST_WRITEERR), ogDraData.imLastReturnCode, ogDraData.omLastErrorMessage);
				MessageBox(olErrorTxt, LoadStg(ST_FEHLER), MB_ICONEXCLAMATION);
			}
			// Dra markieren, damit er sp�ter NICHT gel�scht wird
			prlTempDra->IsChanged = DATA_NEW;
		}
		else
		{
			//---------------------------------------------------------------
			// DRA wurde ge�ndert
			ogDraData.CopyDra(prlTempDra,prlRefDra);
			// Damit auch tats�chlich ein Update gemacht wird
			prlRefDra->IsChanged = DATA_CHANGED;
			// Update in der Datenbank und Broadcast senden
			if (!ogDraData.Update(prlRefDra))
			{
				// Fehler ausgeben
				CString olErrorTxt;
				olErrorTxt.Format("%s %d\n%s",LoadStg(ST_WRITEERR), ogDraData.imLastReturnCode, ogDraData.omLastErrorMessage);
				MessageBox(olErrorTxt, LoadStg(ST_FEHLER), MB_ICONEXCLAMATION);
			}
			// Dra markieren, damit er sp�ter gel�scht wird
			prlTempDra->IsChanged = DATA_CHANGED;
		}
	}
	//-------------------------------------------------------------------------
	// L�schen von DRA's
	// Alle DRA aus der Ref Map durchgehen und pr�fen ob sie in der Tempmap noch 
	// vorhanden sind. Wenn nicht dann wurde der DRA gel�scht
	for(rlPos = omRefDraMap.GetStartPosition(); rlPos != NULL;)
	{
		prlTempDra = NULL;
		prlRefDra = NULL; 
		
		// Ref DRA ermitteln
		omRefDraMap.GetNextAssoc(rlPos,(void *&)llRefUrno, (void *&)prlRefDra);
		// Pr�fen ob dieser DRA in der Refernzliste vorhanden ist
		omTempDraMap.Lookup((void *&)llRefUrno,(void *&)prlTempDra);
		
		// Es wurde ein DRA gel�scht
		if (prlTempDra == NULL)
		{
			// DRA auch in der Datenhaltung l�schen
			if (!ogDraData.Delete(prlRefDra))
			{
				// Fehler ausgeben
				CString olErrorTxt;
				olErrorTxt.Format("%s %d\n%s",LoadStg(ST_WRITEERR), ogDraData.imLastReturnCode, ogDraData.omLastErrorMessage);
				MessageBox(olErrorTxt, LoadStg(ST_FEHLER), MB_ICONEXCLAMATION);
			}
			
			// Drd markieren, damit er sp�ter gel�scht wird
			// prlTempDra->IsChanged = DATA_DELETED;
		}
	}
	CCS_CATCH_ALL;	
}

/******************************************************************************************
BDA 14.3.00 
Arbeitsgruppenliste und Funktionenliste f�llen
DRG pr�fen, wenn vorhanden, die Arbeitsgruppe und Funktion selektieren, sonst leer
******************************************************************************************/
void CTageslisteDlg::FillGroupAndFunctionLists()
{
	CCS_TRY;

	if (!pomDrr)
		return;

	CString olWgpCode;
	CString olFormat;
	CString olTmp;

	// wenn ein DRG vorhanden ist, werden diese Daten benutzt, sonst Stammdaten
	DRGDATA* polDrg = ogDrgData.GetDrgByKey(pomDrr->Sday, pomDrr->Stfu, "1");

	if(polDrg != 0)
	{
		// ein DRG ist vorhanden, Daten einsetzen
		olWgpCode	= polDrg->Wgpc;
	}
	else
	{
		// aus Stammdaten
		COleDateTime olTime;
		CedaDataHelper::DateStringToOleDateTime(pomDrr->Sday,olTime);
		olWgpCode = ogSwgData.GetWgpBySurnWithTime(pomDrr->Stfu,olTime);
	}

	if (ogCCSParam.GetParamValue(ogAppl,"ID_PROD_CUSTOMER") == "SIN")
	{
		// hide static field and set item & text of the Combo
		GetDlgItem(IDC_S_GROUP)->ShowWindow(SW_HIDE);
		m_C_Group.ResetContent();
		m_C_Group.SetFont(&ogCourier_Regular_8);
		PGPDATA *prlPgpData = NULL;
		WGPDATA *prlWgpData = NULL;

		// add empty string
		olFormat = "";
		m_C_Group.AddString(olFormat);

		for (int i = 0; i < ogWgpData.omData.GetSize(); i++)
		{
			prlWgpData = &ogWgpData.omData[i];
			prlPgpData = ogPgpData.GetPgpByUrno (prlWgpData->Pgpu);

			if (prlPgpData != NULL)
			{
				// extract the functions of the group. Look, if in the group is one function of the view.
				CStringArray olArr;
				olTmp = prlPgpData->Pgpm;
				CedaData::ExtractTextLineFast (olArr, olTmp.GetBuffer(0), "|");

				for (int j = 0; j < olArr.GetSize(); j++)
				{
					olTmp.Format("'%s'", olArr[j]);
					if (pomViewInfo->oPfcStatFctcs.Find (olTmp) > -1 || strcmp(olWgpCode, prlWgpData->Wgpc) == NULL)
					{
						olFormat.Format("%-5.5s %-40.40s", prlWgpData->Wgpc, prlWgpData->Wgpn);
						m_C_Group.AddString(olFormat);
						break;
					}
				}
			}
		}

		for (i = 0; i < m_C_Group.GetCount(); i++)
		{
			m_C_Group.GetLBText(i, olTmp);
			olTmp = olTmp.Left(5);
			olTmp.TrimRight();

			if (olTmp == olWgpCode)
			{
				m_C_Group.SetCurSel(i);
				break;
			}
		}
	}
	else
	{
		// hide Combo and set text of statc field
		GetDlgItem(IDC_C_GROUP)->ShowWindow(SW_HIDE);
		SetDlgItemText(IDC_S_GROUP,olWgpCode);
	}

	CCS_CATCH_ALL;	
}


/****************************************************************
Pr�fft ob in der ersten ComboBox leerer String selektiert ist,
wenn ja, selektiert leeren String in der zweiten,
wenn in der ersten ComboBox ein nicht leerer String selektiert ist,
pr�fft, ob die zweite leer ist, wenn ja - selektiert n�chsten nichtleeren 
Eintrag in der ListBox und macht DropDown auf.
****************************************************************/
void CTageslisteDlg::CheckEqualSelChangeInComboBoxes(CComboBox& opBox1, CComboBox& opBox2)
{
	CCS_TRY;
	CString olTmp;
	
	opBox1.GetWindowText(olTmp);
	if(olTmp.IsEmpty())
	{
		// Der Text der 1.Box ist leer
		// ist die 2.Box leer?
		opBox2.GetWindowText(olTmp);
		if(olTmp.IsEmpty())
			return;	// alles OK
		// 2.Box leeren
		for(int iCount=0;iCount<opBox2.GetCount();iCount++)
		{
			opBox2.SetCurSel(iCount);
			opBox2.GetWindowText(olTmp);
			if(olTmp.IsEmpty())
				return;	// alles OK
		}
		// gar kein leeres String in der Box, setzen wir einen ein
		opBox2.SetWindowText("");
	}
	else
	{
		// Der Text der 1.Box ist nicht leer
		// ist die 2.Box nicht leer?
		opBox2.GetWindowText(olTmp);
		if(olTmp.GetLength())
			return;	// alles OK
		// 2.Box f�llen
		for(int iCount=0;iCount<opBox2.GetCount();iCount++)
		{
			opBox2.SetCurSel(iCount);
			opBox2.GetWindowText(olTmp);
			if(olTmp.GetLength())
			{
				opBox2.ShowDropDown(true);
				return;	// alles OK
			}
		}
	}
	CCS_CATCH_ALL;
}

void CTageslisteDlg::OnUpdateEZcode1() 
{
	if (!bmSetDrsActive)
	{
		bmSetDrsActive = true;
		m_E_ZCode4.SetWindowText(_T(""));			
		bmSetDrsActive = false;
	}
}

void CTageslisteDlg::OnUpdateEZcode3() 
{
	if (!bmSetDrsActive)
	{
		bmSetDrsActive = true;
		m_E_ZCode4.SetWindowText(_T(""));			
		bmSetDrsActive = false;
	}
}

void CTageslisteDlg::OnUpdateEZcode4() 
{
	if (!bmSetDrsActive)
	{
		bmSetDrsActive = true;
		m_E_ZCode1.SetWindowText(_T(""));			
		m_E_ZCode3.SetWindowText(_T(""));			
		bmSetDrsActive = false;
	}
}

long CTageslisteDlg::OnEditChanged(UINT lParam, LONG wParam) 
{
	CCS_TRY;
	CString olValidFormat(" 12");
	if("GVA" != ogCCSParam.GetParamValue(ogAppl,"ID_PROD_CUSTOMER"))
	{
			olValidFormat += '8';
	}

	if (lParam == m_E_ZCode1.imID)
	{
		CString olValue;
		m_E_ZCode1.GetWindowText(olValue);
		if (!olValue.IsEmpty() && olValue.FindOneOf(olValidFormat) < 0)
		{
			bmSetDrsActive = true;
			m_E_ZCode1.SetWindowText("");
			bmSetDrsActive = false;
		}
	}
	else if (lParam == m_E_ZCode3.imID)
	{
		CString olValue;
		m_E_ZCode3.GetWindowText(olValue);
		if (!olValue.IsEmpty() && olValue.FindOneOf(olValidFormat) < 0)
		{
			bmSetDrsActive = true;
			m_E_ZCode3.SetWindowText("");
			bmSetDrsActive = false;
		}
	}
	else if (lParam == m_E_ZCode4.imID)
	{
		CString olValue;
		m_E_ZCode4.GetWindowText(olValue);
		if (!olValue.IsEmpty() && olValue.FindOneOf(olValidFormat) < 0)
		{
			bmSetDrsActive = true;
			m_E_ZCode4.SetWindowText("");
			bmSetDrsActive = false;
		}
	}

	ChangeSub1Sub2ToDefault();

	CCS_CATCH_ALL;
	return 0;
}

/***************************************************************************************
F�llt 8 CString-s aus popText mit Datum und Zeitangaben
***************************************************************************************/
void CTageslisteDlg::InitTimesBySpr(CString* popText)
{
	COleDateTime olFrom, olTo, olSbfr, olSbto;
	ogSprData.GetShiftTimes(olFrom, olTo, olSbfr, olSbto, pomDrr->Avfr, pomDrr->Avto, pomDrr->Stfu);
	
	COleDateTime olAvto;
	olAvto.SetStatus(COleDateTime::invalid);

	if(olFrom.GetStatus() == COleDateTime::valid)
	{
		// Startdatum setzen
		popText[0] = olFrom.Format("%d.%m.%Y");
		popText[1] = olFrom.Format("%H:%M");
	}
	if(olTo.GetStatus() == COleDateTime::valid)
	{
		popText[2] = olTo.Format("%d.%m.%Y");
		popText[3] = olTo.Format("%H:%M");
	}
	
	if(olSbfr.GetStatus() == COleDateTime::valid)
	{
		popText[4] = olSbfr.Format("%H:%M");
	}
		
	if(olSbto.GetStatus() == COleDateTime::valid)
	{
		popText[5] = olSbto.Format("%H:%M");
	}

	COleDateTimeSpan olSblu(0,0,0,0);
	if(olSbfr.GetStatus() == COleDateTime::valid && olSbto.GetStatus() == COleDateTime::valid)
	{
		olSblu = olSbto - olSbfr;
		popText[6] = olSblu.Format("%H:%M");
	}

	if(olFrom.GetStatus() == COleDateTime::valid && olTo.GetStatus() == COleDateTime::valid)
	{
		// Dauer der Schicht
		COleDateTimeSpan olDuration;
		olDuration = olTo - olFrom;
		
		olDuration -= olSblu;
		popText[7] = olDuration.Format("%H:%M");
	}
}

void CTageslisteDlg::HideSpr()
{
	CRect olRect;
	int ilMoveUp = 28;

	// --- hiding fields of the SPR-data, removing other controls dynamically
	GetDlgItem (IDC_S_AZE)->ShowWindow(SW_HIDE);
	GetDlgItem (IDC_STATIC_33)->ShowWindow(SW_HIDE);
	GetDlgItem (IDC_STATIC_34)->ShowWindow(SW_HIDE);
	GetDlgItem (IDC_STATIC_42)->ShowWindow(SW_HIDE);
	GetDlgItem (IDC_STATIC_43)->ShowWindow(SW_HIDE);
	GetDlgItem (IDC_STATIC_1498)->ShowWindow(SW_HIDE);
	GetDlgItem (IDC_STATIC_44)->ShowWindow(SW_HIDE);
	GetDlgItem (IDC_STATIC_45)->ShowWindow(SW_HIDE);
	GetDlgItem (IDC_STATIC_46)->ShowWindow(SW_HIDE);

	// --- resizing the form
	GetWindowRect (olRect);
	ClientToScreen (olRect);
	MoveWindow(olRect.left, olRect.top, olRect.Width(), olRect.Height() - ilMoveUp);
	CenterWindow();
	
	// --- resizing frame 'Anwesenheit' (IDC_S_Anwesenheit)
	GetDlgItem (IDC_S_Anwesenheit)->GetWindowRect(olRect);
	ScreenToClient (olRect);
	GetDlgItem (IDC_S_Anwesenheit)->MoveWindow(olRect.left, olRect.top, olRect.Width(), olRect.Height() - ilMoveUp);

	// --- moving frame 'untert�gige Abwesenheit' (IDC_S_Abwesenheit) and controls
	GetDlgItem (IDC_S_Abwesenheit)->GetWindowRect(olRect);
	ScreenToClient (olRect);
	GetDlgItem (IDC_S_Abwesenheit)->MoveWindow(olRect.left, olRect.top - ilMoveUp, olRect.Width(), olRect.Height());

	GetDlgItem (IDC_ABS_GRID)->GetWindowRect(olRect);
	ScreenToClient (olRect);
	GetDlgItem (IDC_ABS_GRID)->MoveWindow(olRect.left, olRect.top - ilMoveUp, olRect.Width(), olRect.Height());

	GetDlgItem (IDC_D_INSERTDRA)->GetWindowRect(olRect);
	ScreenToClient (olRect);
	GetDlgItem (IDC_D_INSERTDRA)->MoveWindow(olRect.left, olRect.top - ilMoveUp, olRect.Width(), olRect.Height());

	GetDlgItem (IDC_D_EDITDRA)->GetWindowRect(olRect);
	ScreenToClient (olRect);
	GetDlgItem (IDC_D_EDITDRA)->MoveWindow(olRect.left, olRect.top - ilMoveUp, olRect.Width(), olRect.Height());

	GetDlgItem (IDC_D_DELETEDRA)->GetWindowRect(olRect);
	ScreenToClient (olRect);
	GetDlgItem (IDC_D_DELETEDRA)->MoveWindow(olRect.left, olRect.top - ilMoveUp, olRect.Width(), olRect.Height());

	// --- moving frame 'Doppeltouren' (IDC_S_Doppeltouren) and controls
	GetDlgItem (IDC_S_Doppeltouren)->GetWindowRect(olRect);
	ScreenToClient (olRect);
	GetDlgItem (IDC_S_Doppeltouren)->MoveWindow(olRect.left, olRect.top - ilMoveUp, olRect.Width(), olRect.Height());

	GetDlgItem (IDC_S_1147)->GetWindowRect(olRect);
	ScreenToClient (olRect);
	GetDlgItem (IDC_S_1147)->MoveWindow(olRect.left, olRect.top - ilMoveUp, olRect.Width(), olRect.Height());

	GetDlgItem (IDC_S_1145)->GetWindowRect(olRect);
	ScreenToClient (olRect);
	GetDlgItem (IDC_S_1145)->MoveWindow(olRect.left, olRect.top - ilMoveUp, olRect.Width(), olRect.Height());

	GetDlgItem (IDC_STATIC_1087)->GetWindowRect(olRect);
	ScreenToClient (olRect);
	GetDlgItem (IDC_STATIC_1087)->MoveWindow(olRect.left, olRect.top - ilMoveUp, olRect.Width(), olRect.Height());

	GetDlgItem (IDC_STATIC_1088)->GetWindowRect(olRect);
	ScreenToClient (olRect);
	GetDlgItem (IDC_STATIC_1088)->MoveWindow(olRect.left, olRect.top - ilMoveUp, olRect.Width(), olRect.Height());

	GetDlgItem (IDC_B_CREATEDTOUR)->GetWindowRect(olRect);
	ScreenToClient (olRect);
	GetDlgItem (IDC_B_CREATEDTOUR)->MoveWindow(olRect.left, olRect.top - ilMoveUp, olRect.Width(), olRect.Height());

	GetDlgItem (IDC_B_CANCELDOUBLE)->GetWindowRect(olRect);
	ScreenToClient (olRect);
	GetDlgItem (IDC_B_CANCELDOUBLE)->MoveWindow(olRect.left, olRect.top - ilMoveUp, olRect.Width(), olRect.Height());

	// --- moving frame 'Bemerkungen' (IDC_S_Bemerkungen) and control
	GetDlgItem (IDC_S_Bemerkungen)->GetWindowRect(olRect);
	ScreenToClient (olRect);
	GetDlgItem (IDC_S_Bemerkungen)->MoveWindow(olRect.left, olRect.top - ilMoveUp, olRect.Width(), olRect.Height());

	GetDlgItem (IDC_REMARK)->GetWindowRect(olRect);
	ScreenToClient (olRect);
	GetDlgItem (IDC_REMARK)->MoveWindow(olRect.left, olRect.top - ilMoveUp, olRect.Width(), olRect.Height());

	// --- moving bottom-buttons (Info, OK, Cancel)
	GetDlgItem (IDC_BUTTONINFO)->GetWindowRect(olRect);
	ScreenToClient (olRect);
	GetDlgItem (IDC_BUTTONINFO)->MoveWindow(olRect.left, olRect.top - ilMoveUp, olRect.Width(), olRect.Height());

	GetDlgItem (IDOK)->GetWindowRect(olRect);
	ScreenToClient (olRect);
	GetDlgItem (IDOK)->MoveWindow(olRect.left, olRect.top - ilMoveUp, olRect.Width(), olRect.Height());

	GetDlgItem (IDCANCEL)->GetWindowRect(olRect);
	ScreenToClient (olRect);
	GetDlgItem (IDCANCEL)->MoveWindow(olRect.left, olRect.top - ilMoveUp, olRect.Width(), olRect.Height());
}

LONG CTageslisteDlg::OnGridLButton(WPARAM wParam, LPARAM lParam)
{
	bool blShift = imScodType == CODE_IS_BSD;
	blShift &= !bmReadOnly;
	if (blShift)
	{
		CRowColArray olRowArray;
		ROWCOL ilCount = pomDevGrid->GetSelectedRows (olRowArray, FALSE, FALSE);

		if (ilCount > 0)
		{
			((CButton*) GetDlgItem(IDC_D_DELETEDRD))->EnableWindow(true);
			((CButton*) GetDlgItem(IDC_D_EDITDRD))->EnableWindow(true);		}
		else
		{
			((CButton*) GetDlgItem(IDC_D_DELETEDRD))->EnableWindow(false);
			((CButton*) GetDlgItem(IDC_D_EDITDRD))->EnableWindow(false);
		}

		ilCount = pomAbsenceGrid->GetSelectedRows (olRowArray, FALSE, FALSE);
		if (ilCount > 0)
		{
			((CButton*) GetDlgItem(IDC_D_DELETEDRA))->EnableWindow(true);
			((CButton*) GetDlgItem(IDC_D_EDITDRA))->EnableWindow(true);
		}
		else
		{
			((CButton*) GetDlgItem(IDC_D_DELETEDRA))->EnableWindow(false);
			((CButton*) GetDlgItem(IDC_D_EDITDRA))->EnableWindow(false);
		}
	}
	return 0L;
}

void CTageslisteDlg::OnButtonRepaired()
{
	if (this->pomDrr != NULL && strlen(this->pomDrr->Repaired) > 0)
	{
		CString olField(this->pomDrr->Repaired);
		AfxMessageBox(olField,MB_OK|MB_ICONEXCLAMATION);
	}
}
