// SelectDateDlg.cpp : implementation file
//

#include "stdafx.h"
#include "rostering.h"
#include "SelectDateDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSelectDateDlg dialog


CSelectDateDlg::CSelectDateDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CSelectDateDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CSelectDateDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	this->omDate.SetStatus(COleDateTime::invalid);
	this->omFrom.SetStatus(COleDateTime::invalid);
	this->omTo.SetStatus(COleDateTime::invalid);
	this->imOleDateWeekDay = -1;
	this->imCalCtrlWeekDay = -1;
}


void CSelectDateDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSelectDateDlg)
	DDX_Control(pDX, IDC_MONTHCALENDAR1, m_CalCtrl);
	//}}AFX_DATA_MAP
}

void CSelectDateDlg::SetDate(const COleDateTime& ropDate)
{
	this->omDate = ropDate;
}

COleDateTime CSelectDateDlg::GetDate() const
{
	return this->omDate;
}


void CSelectDateDlg::SetRange(const COleDateTime& ropFrom,const COleDateTime ropTo)
{
	this->omFrom = ropFrom;
	this->omTo	 = ropTo;
}

void CSelectDateDlg::SetValidWeekDay(int ipWeekDay)
{
	this->imOleDateWeekDay = ipWeekDay;
	this->imCalCtrlWeekDay = ipWeekDay - 1;
}

BEGIN_MESSAGE_MAP(CSelectDateDlg, CDialog)
	//{{AFX_MSG_MAP(CSelectDateDlg)
	ON_NOTIFY(MCN_GETDAYSTATE, IDC_MONTHCALENDAR1, OnGetdaystateMonthcalendar1)
	ON_NOTIFY(MCN_SELCHANGE, IDC_MONTHCALENDAR1, OnSelchangeMonthcalendar1)
	ON_NOTIFY(MCN_SELECT, IDC_MONTHCALENDAR1, OnSelectMonthcalendar1)
	ON_NOTIFY(NM_RELEASEDCAPTURE, IDC_MONTHCALENDAR1, OnReleasedcaptureMonthcalendar1)
	ON_WM_CREATE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSelectDateDlg message handlers

void CSelectDateDlg::OnGetdaystateMonthcalendar1(NMHDR* pNMHDR, LRESULT* pResult) 
{
	// TODO: Add your control notification handler code here
	NMDAYSTATE *lpNMDayState = (NMDAYSTATE *)pNMHDR;

	if (lpNMDayState->prgDayState == NULL)
	{
		*pResult = 0;
		return;
	}

	if (this->imOleDateWeekDay != -1 && this->omTo.GetStatus() == COleDateTime::valid)
	{
		int nMonths = lpNMDayState->cDayState;

		int ilMonth = -1;
		int i = -1;

		for (COleDateTime olDay = lpNMDayState->stStart; i < nMonths; olDay += COleDateTimeSpan(1,0,0,0))
		{
			if (olDay.GetDayOfWeek() == this->imOleDateWeekDay)
			{
				if (ilMonth == -1 || ilMonth != olDay.GetMonth())
				{
					ilMonth = olDay.GetMonth();
					++i;
					lpNMDayState->prgDayState[i] = 0;
				}
			lpNMDayState->prgDayState[i] |= 1 << (olDay.GetDay() - 1);
			}
		}
	}

	*pResult = 0;
} 

void CSelectDateDlg::OnSelchangeMonthcalendar1(NMHDR* pNMHDR, LRESULT* pResult) 
{
	// TODO: Add your control notification handler code here
	NMSELCHANGE *lpNMSelChange = (NMSELCHANGE *) pNMHDR;
	COleDateTime olDay = lpNMSelChange->stSelStart;
	if (this->imOleDateWeekDay != -1 && olDay.GetDayOfWeek() != this->imOleDateWeekDay)
	{
		*pResult = 1;
	}
	else
	{
		this->omDate = lpNMSelChange->stSelStart;
		*pResult = 0;
	}
}

void CSelectDateDlg::OnSelectMonthcalendar1(NMHDR* pNMHDR, LRESULT* pResult) 
{
	// TODO: Add your control notification handler code here
	NMSELCHANGE *lpNMSelChange = (NMSELCHANGE *) pNMHDR;
	COleDateTime olDay = lpNMSelChange->stSelStart;
	if (this->imOleDateWeekDay != -1 && olDay.GetDayOfWeek() != this->imOleDateWeekDay)
	{
		this->GetDlgItem(IDOK)->EnableWindow(FALSE);
		*pResult = 1;
	}
	else
	{
		this->omDate = lpNMSelChange->stSelStart;
		this->GetDlgItem(IDOK)->EnableWindow(TRUE);
		*pResult = 0;
	}
}

void CSelectDateDlg::OnReleasedcaptureMonthcalendar1(NMHDR* pNMHDR, LRESULT* pResult) 
{
	// TODO: Add your control notification handler code here
	
	*pResult = 0;
}

void CSelectDateDlg::OnOK() 
{
	// TODO: Add extra validation here
	CDialog::OnOK();
}

BOOL CSelectDateDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	if (this->omDate.GetStatus() == COleDateTime::valid)
		this->m_CalCtrl.SetCurSel(this->omDate);

	if (this->omFrom.GetStatus() == COleDateTime::valid && this->omTo.GetStatus() == COleDateTime::valid)
	{
		this->m_CalCtrl.SetRange(&this->omFrom,&this->omTo);
	}
	
	if (this->imOleDateWeekDay != -1)
	{
		this->GetDlgItem(IDOK)->EnableWindow(FALSE);
	}
	else
	{
		long llOldValue = GetWindowLong(this->m_CalCtrl.m_hWnd,GWL_STYLE);
		long llNewValue = llOldValue - MCS_DAYSTATE;
		if (SetWindowLong(this->m_CalCtrl.m_hWnd,GWL_STYLE,llNewValue) == 0)
		{
			DWORD dwError = GetLastError();
		}
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

int CSelectDateDlg::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CDialog::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	// TODO: Add your specialized creation code here
	return 0;
}

BOOL CSelectDateDlg::Create(LPCTSTR lpszClassName, LPCTSTR lpszWindowName, DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID, CCreateContext* pContext) 
{
	// TODO: Add your specialized code here and/or call the base class
	
	return CDialog::Create(IDD, pParentWnd);
}

BOOL CSelectDateDlg::PreCreateWindow(CREATESTRUCT& cs) 
{
	// TODO: Add your specialized code here and/or call the base class
	
	return CDialog::PreCreateWindow(cs);
}
