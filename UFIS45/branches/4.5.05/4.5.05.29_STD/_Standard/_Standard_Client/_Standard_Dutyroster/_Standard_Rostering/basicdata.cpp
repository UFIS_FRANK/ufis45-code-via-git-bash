
// basicdat.cpp CBasicData class for providing general used methods

#include <stdafx.h>

#define N_URNOS_AT_ONCE 500

static FILE *rgOutFile = NULL;
static FILE *rgErrFile = NULL;

static int CompareArray( const CString **e1, const CString **e2);

//	used to get an GUI-String from DB or Res
CString LoadStg(UINT nID)
{
	CGUILng* ogGUILng = CGUILng::TheOne(); //!!!Singelton !!! es gibt nur ein Objekt !!!
	return ogGUILng->GetString(nID);
}

void DdxRegister(void *vpInstance, int ipDDXType, LPCTSTR DDXName, LPCTSTR InstanceName, DDXCALLBACK pfpCallBack)
{
	CString olDDXName(DDXName);
	CString olInstanceName(InstanceName);
	ogDdx.Register(vpInstance, ipDDXType, olDDXName, olInstanceName, pfpCallBack);
}

void WriteLogFull(LPTSTR fmt,...)
{
	if (lgLogFileStatus != LOGFILE_FULL)
	{
		return;
	}

	if (strlen(fmt))
	{
		TCHAR szBuf[4096];
		va_list marker;
		
		va_start(marker, fmt);
		wvsprintf(szBuf, fmt, marker);
		va_end(marker);
		WriteInRosteringLog(LOGFILE_FULL, szBuf);
	}
	else
	{
		WriteInRosteringLog(LOGFILE_FULL, 0);
	}
}


/*************************************************************************************
einzig g�ltige Logfile Ausgabem�glichkeit
ogRosteringLogText kann daf�r benutzt werden (wird ausgelesen und gel�scht
*************************************************************************************/
void WriteInRosteringLog(long lpStatus/*=LOGFILE_FULL*/, LPCTSTR opLogText /*=0*/)
{
	CCS_TRY;
	// LOGSTATUS in Ceda.Ini = FULL, TRACE or OFF (default)
	// opSatus = FULL   alles (jeder gew�nschte mist wenn gew�nscht)
	// opSatus = TRACE  nur wichtige Infos, die nicht das Logfile zum�llen
	// opSatus = OFF    Prio 1 Infos die IMMER ausgegeben werden sollen 
	//                  solange ein Logfile erzeugt werden kann
	
	// ogRosteringLogText steht immer als CString f�r formatierungen zur verf�gung
	
	switch(lpStatus)
	{
	case LOGFILE_FULL:
		if(lgLogFileStatus != LOGFILE_FULL) 
		{
			ogRosteringLogText.Empty();
			return;
		}
		break;
	case LOGFILE_TRACE:
		if(	lgLogFileStatus != LOGFILE_FULL &&
			lgLogFileStatus != LOGFILE_TRACE) 
		{
			ogRosteringLogText.Empty();
			return;
		}
		break;
	case LOGFILE_OFF:
		break;
	}
	
	if(opLogText == 0)
		opLogText = (LPCTSTR)ogRosteringLogText;	// d.h. f�r den Text wurde ogRosteringLogText benutzt

	if(!strlen(opLogText))
		return;		// leer, warum auch immer
	
	if(strcmp(pcgLogFilePath, "DEFAULT") != 0)
	{
		if(bgFirstLog)
		{
			rgOutFile = fopen(pcgLogFilePath,"w");
			bgFirstLog = false;
		}
		else
		{
			rgOutFile = fopen(pcgLogFilePath,"a");
		}
		if (rgOutFile != NULL)
		{
			CTime olTime = CTime::GetCurrentTime();
			fprintf(rgOutFile,"rostering %s %s\n",olTime.Format("%Y%m%d %H%M%S"),opLogText);
			fclose(rgOutFile);
			rgOutFile = NULL;
		}
	}
	ogRosteringLogText.Empty();
	
	CCS_CATCH_ALL;
}

/******************************************************************************
******************************************************************************/
void WriteInErrlogFile()
{
	if(ogErrlog.IsEmpty()) return;
	if(bgFirstErrLog)
	{
		rgErrFile = fopen(pcgErrlogFilePath,"w");
		bgFirstErrLog = false;
		fprintf(rgErrFile,"List of corrupted data\n\n");
	}
	else
	{
		rgErrFile = fopen(pcgErrlogFilePath,"a");
	}
	
	if (!rgErrFile) return;

	CTime olTime = CTime::GetCurrentTime();
	fprintf(rgErrFile,"%s\n%s\n",olTime.Format("%Y.%m.%d %H:%M:%S"),ogErrlog);
	fclose(rgErrFile);
	ogErrlog.Empty();

	rgErrFile = 0;
}

/******************************************************************************
******************************************************************************/
bool IsFullLoggingEnabled()
{
	return lgLogFileStatus == LOGFILE_FULL;
}

/******************************************************************************
******************************************************************************/
bool IsTraceLoggingEnabled()
{
	return lgLogFileStatus == LOGFILE_TRACE || lgLogFileStatus == LOGFILE_FULL;
}

/******************************************************************************
******************************************************************************/
bool IsOffLoggingEnabled()
{
	return lgLogFileStatus == LOGFILE_OFF || lgLogFileStatus == LOGFILE_TRACE || lgLogFileStatus == LOGFILE_FULL;
}

/**************************************************************************
opDst bekommt auch alle Strings aus opSrc, die in opDst zuvor nicht vorhanden waren
**************************************************************************/
void MergeStringArrays(CStringArray& opDst, CStringArray& opSrc)
{
	// ein in anderem suchen	
	int ilDstSize = opDst.GetSize();
	int ilSrcSize = opSrc.GetSize();
	CStringArray olDif;

	for(int iSrc=0; iSrc<ilSrcSize; iSrc++)
	{
		bool bFound = false;
		for(int iDst=0; iDst<ilDstSize; iDst++)
		{
			if(!bFound && opSrc[iSrc] == opDst[iDst])
				bFound = true;
		}
		if(!bFound)
			olDif.Add(opSrc[iSrc]);

	}
	opDst.Append(olDif);
}

/**************************************************************************
in opDst werden alle Strings gel�scht, die in opSrc gefunden werden
**************************************************************************/
void SubstStringArrays(CStringArray& opDst, CStringArray& opSrc, CMapStringToOb* popExtraUrnoMap/*=0*/)
{
	// ein in anderem suchen	
	int ilDstSize = opDst.GetSize();
	int ilSrcSize = opSrc.GetSize();

	for(int iSrc=0; iSrc<ilSrcSize; iSrc++)
	{
		for(int iDst=0; iDst<ilDstSize; iDst++)
		{
			if(opSrc[iSrc] == opDst[iDst])
			{
				// gefunden, aus Src l�schen
				opDst.RemoveAt(iDst);
				if(popExtraUrnoMap != 0)
				{
					popExtraUrnoMap->RemoveKey(opSrc[iSrc]);
				}
				ilDstSize--;
				break;	// n�chsten suchen, dieser kommt nicht mehr vor
			}
		}
	}
}

//------------------------------------------------------------------------------------

CTime COleDateTimeToCTime(COleDateTime opTime)
{
	CTime olTime = -1;
	bool blYear = false;
	int ilDay = 1,ilMonth =1, ilYear = 0;
	if(opTime.GetStatus() == COleDateTime::valid)
	{
		if(opTime.GetYear()>=2038 || opTime.GetYear()<1970)
		{
			blYear = true;
			if(opTime.GetYear()>=2038)
			{
				ilYear = 2038;
				ilMonth = 1;
			}
			else
			{
				ilYear = 1970;
				ilMonth = opTime.GetMonth();
			}
		}
		if(blYear && (ilDay = opTime.GetDay())>18)
		{
			if(ilYear ==2038)
				ilDay = 18;
		}
		if(blYear)
		{
			olTime = CTime(ilYear, ilMonth, ilDay, 1, 0, 0);
		}
		else
		{
			olTime = CTime(opTime.GetYear(), opTime.GetMonth(), opTime.GetDay(), opTime.GetHour(), opTime.GetMinute(), opTime.GetSecond());
		}
	}
	return olTime;
}

//************************************************************************************************************************************************
// YYYYMMDDToOleDateTime: erzeugt aus einem Datumsstring im Format 'JJJJMMTT'
//	ein COleDateTime-Obkjekt.
// R�ckgabe:	true	-> String g�ltig, <opTime> initialisiert
//				false	-> Fehler
//************************************************************************************************************************************************

bool YYYYMMDDToOleDateTime(CString opTimeString, COleDateTime &opTime)
{
CCS_TRY;
	// String-Param pr�fen	
	if ((opTimeString == "") || (opTimeString.GetLength() < 8) || (opTimeString.SpanExcluding("0123456789") != "")){
		// <opTimeString> ung�ltig -> terminieren
		return false;
	}
	
	// COleDateTime-Objekt aus SDAY erzeugen...
	COleDateTime olTime(atoi(opTimeString.Left(4).GetBuffer(0)), // Jahr (YYYY)
						atoi(opTimeString.Mid(4,2).GetBuffer(0)), // Monat (MM)
						atoi(opTimeString.Mid(6,2).GetBuffer(0)), // Tag (DD)
						0,0,0);	// Stunde, Minute, Sekunde
	// ...und kopieren
	opTime = olTime;
	return true;
CCS_CATCH_ALL;
return false;
}

COleDateTime CTimeToCOleDateTime(CTime opTime)
{
	COleDateTime olTime;
	olTime.SetStatus(COleDateTime::invalid);
	if(opTime != -1)
	{
		olTime = COleDateTime(opTime.GetYear(), opTime.GetMonth(), opTime.GetDay(), opTime.GetHour(), opTime.GetMinute(), opTime.GetSecond());
	}
	return olTime;
}

//-- ItemList-Functions --------------------------------------------------------------

int GetItemCount(CString olList, char cpTrenner  )
{
	CStringArray olStrArray;
	return ExtractItemList(olList,&olStrArray,cpTrenner);

}

bool GetListItemByField(CString &opDataList, CString &opFieldList, CString &opField, CString &opData)
{
	CStringArray olFields;
	CStringArray olData;
	int ilFields = ExtractItemList(opFieldList, &olFields);
	int ilData   = ExtractItemList(opDataList, &olData);

	for( int i = 0; i < ilFields; i++)
	{
		if(olFields[i] == opField)
		{
			if(ilData > i)
				opData = olData[i];
				return true; 
		}
	}
	return false;
}

bool GetListItemByField(CString &opDataList, CString &opFieldList, CString &opField, CTime &opData)
{
	CStringArray olFields;
	CStringArray olData;
	int ilFields = ExtractItemList(opFieldList, &olFields);
	int ilData   = ExtractItemList(opDataList, &olData);

	for( int i = 0; i < ilFields; i++)
	{
		if(olFields[i] == opField)
		{
			if(ilData > i)
				opData = DBStringToDateTime(olData[i]);
				return true; 
		}
	}
	return false;
}

int ExtractItemList(CString opSubString, CStringArray *popStrArray, char cpTrenner)
{

	CString olText;

	popStrArray->RemoveAll();

	BOOL blEnd = FALSE;
	
	if(!opSubString.IsEmpty())
	{
		int pos;
		//int olPos = 0;
		while(blEnd == FALSE)
		{
			pos = opSubString.Find(cpTrenner);
			if(pos == -1)
			{
				blEnd = TRUE;
				olText = opSubString;
			}
			else
			{
				olText = opSubString.Mid(0, opSubString.Find(cpTrenner));
				opSubString = opSubString.Mid(opSubString.Find(cpTrenner)+1, opSubString.GetLength( )-opSubString.Find(cpTrenner)+1);
			}
			popStrArray->Add(olText);
		}
	}
	return	popStrArray->GetSize();

}

CString GetListItem(CString &opList, int ipPos, bool bpCut, char cpTrenner )
{
	CStringArray olStrArray;
	int ilAnz = ExtractItemList(opList, &olStrArray, cpTrenner);
	CString olReturn;

	if (ipPos == -1)
		ipPos = ilAnz;

	if ((ipPos <= ilAnz) && (ipPos > 0))
		olReturn = olStrArray[ipPos - 1];
	if (bpCut)
	{
		opList = "";
		for (int ilLc = 0; ilLc < ilAnz; ilLc++)
		{
			if(ilLc != (ipPos - 1))
			{
				opList = opList + olStrArray[ilLc] + cpTrenner;
			}
		}
	}
	if (bpCut)
	{
		opList = opList.Left(opList.GetLength() - 1);
	}
	return olReturn;
}

CString DeleteListItem(CString &opList, CString olItem)
{
	CStringArray olStrArray;
	int ilAnz = ExtractItemList(opList, &olStrArray);
	opList = "";
	for(int ilLc = 0; ilLc < ilAnz; ilLc++)
	{
			if(olStrArray[ilLc] != olItem)
				opList = opList + olStrArray[ilLc] + ",";
	}
	opList = opList.Left(opList.GetLength() - 1);
	return opList;
}

CString SortItemList(CString opSubString, char cpTrenner)
{
	CString *polText;
	CCSPtrArray<CString> olArray;

	BOOL blEnd = FALSE;
	
	if(!opSubString.IsEmpty())
	{
		int pos;
		//int olPos = 0;
		while(blEnd == FALSE)
		{
			polText = new CString;
			pos = opSubString.Find(cpTrenner);
			if(pos == -1)
			{
				blEnd = TRUE;
				*polText = opSubString;
			}
			else
			{
				*polText = opSubString.Mid(0, opSubString.Find(cpTrenner));
				opSubString = opSubString.Mid(opSubString.Find(cpTrenner)+1, opSubString.GetLength( )-opSubString.Find(cpTrenner)+1);
			}
			olArray.Add(polText);
		}
	}

	CString olSortedString;
	olArray.Sort(CompareArray);
	for(int i=0; i<olArray.GetSize(); i++)
	{
		olSortedString += olArray[i] + cpTrenner;
	}
	
	olArray.DeleteAll();
	return olSortedString.Left(olSortedString.GetLength()-1);
}

int SplitItemList(CString opString, CStringArray *popStrArray, int ipMaxItem, char cpTrenner)
{

	CString olSubStr;

	popStrArray->RemoveAll();
	
	CStringArray olStrArray;

	int ilCount = ExtractItemList(opString, &olStrArray);
	int ilSubCount = 0;

	for(int i = 0; i < ilCount; i++)
	{
		if(ilSubCount >= ipMaxItem)
		{
			if(!olSubStr.IsEmpty())
				olSubStr = olSubStr.Right(olSubStr.GetLength() - 1);
			popStrArray->Add(olSubStr);
			ilSubCount = 0;
			olSubStr = "";
		}
		ilSubCount++;
		olSubStr = olSubStr + cpTrenner + olStrArray[i];
	}
	if(!olSubStr.IsEmpty())
	{
		olSubStr = olSubStr.Right(olSubStr.GetLength() - 1);
		popStrArray->Add(olSubStr);
	}
	
	return	popStrArray->GetSize();
}

/***************************************************************************************
Erweiterung f�r CedaBasicData ogBCD:
Read/ReadInternal ist auf CedaData::CedaAction() wegen Segmentierung umgeleitet
***************************************************************************************/
int Read(CString opObject, CString opWhere/* = ""*/, CCSPtrArray<RecordSet> *pomBuffer/* = NULL*/)
{
	CedaObject *prlObject; 
	char* pclSelection = 0;
	char pclTable[64];
	char pclFieldList[2048];
	char pclData[256] = "";
	char pclCmd[16] = "";
	CString olDataList;
	int ilCount;
	bool blRet; 
	
	pclSelection = (char*)malloc(opWhere.GetLength()+1);
	if(!pclSelection) return 0;
	strcpy(pclSelection, opWhere);

	CString olTable;

	if(ogBCD.omObjectMap.Lookup((LPCSTR)opObject,(void *& )prlObject) == TRUE)
	{
		strcpy(pclFieldList, prlObject->GetDBFieldList());
		
		olTable = prlObject->GetTableName();

		strcpy(pclTable, olTable);
		strcat(pclTable, ogBCD.omTableExt);


		if(pomBuffer == NULL)
			prlObject->ClearAll();
		else
			pomBuffer->DeleteAll();

		if(opWhere.IsEmpty())
			prlObject->SetReadAll(true);
		else
			prlObject->SetReadAll(false);
		
		prlObject->SetSelection(opWhere);
		
		strcpy(pclCmd, "RT");
		
		if(prlObject->SharedMem())
		{
			if(ogBCD.CheckSMWhere(opWhere))
				strcpy(pclCmd, "SYS");
		}

	    CStringArray *polDataBuf = ogBCD.GetDataBuff();
		polDataBuf->RemoveAll();
		int ilLineNo;

		CedaData olCedaData;
		if(pomBuffer == NULL)
		{
			if(ogBCD.omAccessMethod == "ODBC" || ogBCD.omAccessMethod == "READ_ODBC")
			{
				ogBCD.pomOdbc->CallDB("Select", CString(pclTable), CString(pclFieldList), CString(pclSelection), "",  prlObject->GetDataBuffer());
				ogBCD.pomOdbc->RemoveInternalData();

			}
			else
			{
				blRet = olCedaData.CedaAction(pclCmd, pclTable, pclFieldList, pclSelection, "", pclData, "BUF1", false, 0, prlObject->GetDataBuffer(), prlObject->GetFieldCount());
			}
			prlObject->CreateKeyMap();
		}
		else
		{
			if(ogBCD.omAccessMethod == "ODBC" || ogBCD.omAccessMethod == "READ_ODBC")
				;
			else
				blRet = olCedaData.CedaAction(pclCmd, pclTable, pclFieldList, pclSelection, "", pclData, "BUF1");
		}

		if(ogBCD.omAccessMethod == "ODBC" || ogBCD.omAccessMethod == "READ_ODBC")
		{
			ilCount = ogBCD.pomOdbc->GetLineCount();
		}
		else
		{
			ilCount = ogBCD.GetBufferSize();
			if(pomBuffer == NULL)
			{
				;
			}
			else
			{
				for(ilLineNo = 0;ilLineNo < ilCount; ilLineNo++)
				{
					ogBCD.MakeClientString((*polDataBuf)[ilLineNo]);
					prlObject->AddRecord(pomBuffer, (*polDataBuf)[ilLineNo]);
				}
			}
		}
	}

	if(pclSelection != 0)
	{
		free((void*)pclSelection);
	}
	return ilCount;
}
//-- CBasicDat -------------------------------------------------------------------------

CBasicData::CBasicData(void)
{
}

CBasicData::~CBasicData(void)
{
}

long CBasicData::GetNextUrno(void)
{
	bool	olRc = true;
	long	llNextUrno = 0L;
	

	if (omUrnos.GetSize() == 0)
	{
		olRc = GetNurnos(500);
	}

	if (omUrnos.GetSize() > 0)
	{
		llNextUrno = omUrnos[0];
		omUrnos.RemoveAt(0);

		if ( (llNextUrno != 0L) && (olRc == true) )
		{
			return(llNextUrno);
		}
	}
	::MessageBox(NULL,LoadStg(IDS_STRING444),LoadStg(IDS_STRING445),MB_OK);
	return -1;	
}


bool CBasicData::GetNurnos(int ipNrOfUrnos)
{
	bool	ilRc = false;
	char 	pclTmpDataBuf[12*N_URNOS_AT_ONCE];

	sprintf(pclTmpDataBuf, "%d", ipNrOfUrnos);

	ilRc = CedaAction("GMU", "", "", pclTmpDataBuf);

	if (ilRc == true)
	{
		for ( int ilItemNo=1; (ilItemNo <= N_URNOS_AT_ONCE) ; ilItemNo++ )
		{
			char pclTmpBuf[64];

			GetItem(ilItemNo, pclTmpDataBuf, pclTmpBuf);

			long llNewUrno = atol(pclTmpBuf);

			omUrnos.Add(llNewUrno);
		}
	}

	return ilRc;
}

void CBasicData::SetWorkstationName(CString opWsName)
{
	omWorkstationName = CString(opWsName);//("WKS234");//opWsName;
}

CString CBasicData::GetWorkstationName()
{
	return omWorkstationName;
}

static int CompareArray( const CString **e1, const CString **e2)
{
	return (strcmp((**e1),(**e2)));
}

void CBasicData::SetUtcDifferenceFromApttab(void)
{
	bool blRc = false;
	char pclWhere[100], pclFields[100] = "TICH,TDI1,TDI2";
	char pclTable[100];
	sprintf(pclWhere,"WHERE APC3='%s'",pcgHome);
	sprintf(pclTable,"APT%s",pcgTableExt);

	if(!CedaAction("RT", pclTable, pclFields, pclWhere, "", "BUF1"))
	{
		TRACE("Error reading UTC/local difference from APTTAB %s\nomLastErrorMessage = <%s>\n",pclWhere,omLastErrorMessage);
	}
	else
	{
		CTime olLocalTime = CTime::GetCurrentTime();
		CStringArray *polDataBuf = GetDataBuff();
		if(polDataBuf->GetSize() <= 0)
		{
			TRACE("Error reading UTC/local difference from APTTAB %s\nDataBuff is empty\n",pclWhere);
		}
		else
		{
			CString olText;
			char pclData[100];
			memset(pclData,0,100);
			CString olData = polDataBuf->GetAt(0);
			strncpy(pclData,olData,99);

			// TICH == 31/3
			// TDI1 = -60   TDI2 = -120
			//
			// TICH == 31/10
			// TDI1 = -120   TDI2 = -60

			char pclTich[100],pclTdi1[100],pclTdi2[100];
			GetItem(1, pclData, pclTich);
			GetItem(2, pclData, pclTdi1);
			GetItem(3, pclData, pclTdi2);
			if(strlen(pclTich) > 0 && strlen(pclTdi1) > 0 && strlen(pclTdi2) > 0)
			{
				CString olLocalStr = olLocalTime.Format("%Y%m%d%H%M");
				imUtcDifference = (((strncmp(olLocalStr,pclTich,12)<0) ? atoi(pclTdi1) : atoi(pclTdi2)) * 60);

				olText.Format("Difference between UTC and local read from APTTAB is %d minutes APT.TICH=<%s> APT.TDI1=<%s> APT.TDI2=<%s>\n",imUtcDifference/60,pclTich,pclTdi1,pclTdi2);
				TRACE("%s\n",olText);
			}
			else
			{
				olText.Format("Error reading UTC/local difference from APTTAB %s",pclWhere);
				TRACE("%s\n",olText);

				olText.Format("DataBuff contains <%s>",olData);
				TRACE("%s\n",olText);
			}
			// new method replacing the one above - although the one above will be retained for now
			int ilTdi1 = atoi(pclTdi1);
			omLocalDiff1 = COleDateTimeSpan (0,(ilTdi1 / 60) ,(ilTdi1 % 60),0);
			int ilTdi2 = atoi(pclTdi2);
			omLocalDiff2 = COleDateTimeSpan (0,(ilTdi2 / 60) ,(ilTdi2 % 60),0);
			CString olTich = pclTich;
			CTime olTmpTime = DBStringToDateTime(olTich);
			omTich = CTimeToCOleDateTime (olTmpTime);
			// end new method

			polDataBuf->RemoveAll();
		}
	}
}

void CBasicData::ConvertDateToLocal(COleDateTime &ropDate)
{
	ropDate = GetLocalFromUtc(ropDate);
}

COleDateTime CBasicData::GetLocalFromUtc(COleDateTime opDate)
{
	if((opDate != COleDateTime::invalid) && (omTich != COleDateTime::invalid))
	{
		if(opDate < omTich)
		{
			opDate += omLocalDiff1;
		}
		else
		{
			opDate += omLocalDiff2;
		}
	}
	return opDate;
}

void CBasicData::ConvertDateToUtc(COleDateTime &ropDate)
{
	ropDate = GetUtcFromLocal(ropDate);
}

COleDateTime CBasicData::GetUtcFromLocal(COleDateTime opDate)
{
	if((opDate != COleDateTime::invalid) && (omTich != COleDateTime::invalid))
	{
		if(opDate < omTich)
		{
			opDate -= omLocalDiff1;
		}
		else
		{
			opDate -= omLocalDiff2;
		}
	}
	return opDate;
}

CString CBasicData::GetFormatedEmployeeName(int ipEName, STFDATA *popStfData, char *pcpPrefix, char *pcpSuffix, NameConfigurationDlg* prpNameConfigDlg)
{
	CString olRet;
	CString olTmp;

	switch (ipEName)
	{
		case 1: //Code/Initials
		{
			olTmp.Format("%s",popStfData->Perc);
			break;
		}
		case 3: //Shortname/Nickname
		{
			olTmp.Format("%s",popStfData->Shnm);
			break;
		}
		case 4: // free configurated
		{
			if (prpNameConfigDlg != NULL)
			{
				olTmp = prpNameConfigDlg->GetNameString(
					(char*)popStfData->Finm,
					(char*)popStfData->Lanm,
					(char*)popStfData->Shnm,
					(char*)popStfData->Perc,
					(char*)popStfData->Peno);
			}
			else
			{
				olTmp = pogNameConfigurationDlg->GetNameString(
					(char*)popStfData->Finm,
					(char*)popStfData->Lanm,
					(char*)popStfData->Shnm,
					(char*)popStfData->Perc,
					(char*)popStfData->Peno);
			}
			break;
		}
		default: //normally == case 2: Name
		{
			CString olFINM,olLANM;
			olFINM = popStfData->Finm;
			olLANM = popStfData->Lanm;
			olTmp.Format("%s.%s", olFINM.Left(1), olLANM);
			break;
		}

	}
	olRet.Format("%s%s%s", pcpPrefix, olTmp, pcpSuffix);
	return olRet;
}

CString CBasicData::GetFormatedEmployeeName(int ipEName, const STFDATA *popStfData, char *pcpPrefix, char *pcpSuffix)
{
	CString olRet;
	CString olTmp;

	switch (ipEName)
	{
		case 1: //Code/Initials
		{
			olTmp.Format("%s",popStfData->Perc);
			break;
		}
		case 2: //Name
		{
			CString olFINM,olLANM;
			olFINM = popStfData->Finm;
			olLANM = popStfData->Lanm;
			olTmp.Format("%s.%s", olFINM.Left(1), olLANM);
			break;
		}
		case 3: //Shortname/Nickname
		{
			olTmp.Format("%s",popStfData->Shnm);
		}
		case 4: // free configurated
		{
			olTmp = pogNameConfigurationDlg->GetNameString(
				(char*)popStfData->Finm,
				(char*)popStfData->Lanm,
				(char*)popStfData->Shnm,
				(char*)popStfData->Perc,
				(char*)popStfData->Peno);
		}
	}
	olRet.Format("%s%s%s", pcpPrefix, olTmp, pcpSuffix);
	return olRet;
}

int CBasicData::GetHugeCountOfWeeksForShiftRoster()
{
	static int ilRc = -1;
	static int ilWeeks = 99;
	if (ilRc == -1)
	{
		char pclWhere[100], pclFields[100] = "data_length";
		char pclTable[100];
		sprintf(pclWhere,"where table_name = 'GSPTAB' and column_name = 'WEEK'");
		sprintf(pclTable,"user_tab_columns");

		ilRc = CedaAction("RT", pclTable, pclFields, pclWhere, "", "BUF1");
		if (ilRc && this->GetBufferSize() == 1)
		{
			CString olBuffer;
			if (GetBufferLine(0,olBuffer))
			{
				if (atoi(olBuffer) == 3)
					ilWeeks = 142;
				else
					ilWeeks = 99;
			}
		}
	}

	return ilWeeks;
}
