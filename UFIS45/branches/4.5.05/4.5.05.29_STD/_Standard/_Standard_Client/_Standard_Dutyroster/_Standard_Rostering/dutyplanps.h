#ifndef AFX_DUTYPLANPS_H__E3451C51_8E79_11D2_8E3E_0000C002916B__INCLUDED_
#define AFX_DUTYPLANPS_H__E3451C51_8E79_11D2_8E3E_0000C002916B__INCLUDED_

// DutyPlanPs.h : Header-Datei
//
#include <stdafx.h>
#include <DutyPlanAbsencePp.h>
#include <DutyRoster_View.h>

#define ID_DRR_GRID			(WM_USER+101)

class CGridControl;

/////////////////////////////////////////////////////////////////////////////
// DutyInsert
// Hilfsklasse, die zum �ndern von DRRs (vorher ROS/DSR) dient (Men�punkt
// 'Dienstplan bearbeiten' aus Kontextmen� in Dienstplanung -> Registerkarten
// 'Urlaub/Abwesenheit' und 'Arbeitsunf�higkeit' -> Button 'Einf�gen')
class DutyInsert
{
public:
	DutyInsert();
	// aktiven und/oder Urlaubs-DRR �ndern/erzeugen
	void WriteInDB(long lpStfUrno, COleDateTime opFrom, COleDateTime opTo, 
				   CString opCode, CString opPType,CWnd* pParentWnd, bool bpUpdateActiveDRR,
				   bool bpUpdateVacancyDRR, bool bpOverwriteWeekend,
				   bool bpOverwriteRegFree, bool bpInsert=true);
};

/////////////////////////////////////////////////////////////////////////////
// DutyPlanPs
class DutyPlanPs : public CPropertySheet
{
	DECLARE_DYNAMIC(DutyPlanPs)

// Konstruktion
public:
	DutyPlanPs(UINT nIDCaption, CWnd* pParentWnd = NULL, UINT iSelectPage = 0);
	DutyPlanPs(LPCTSTR pszCaption, long Eurn,CString opCurShift,COleDateTime ActDat,const CString& ropPType,CWnd* pParentWnd = NULL, UINT iSelectPage = 0);

// Attribute
public:
	DutyPlanAbsencePp	omDutyPlanAbsencePp;

	DutyRoster_View *pomParent;
	
	CString omDay;
	CString	omPType;	
	// der Planungstag, f�r den der Dialog aufgerufen wurde
	COleDateTime omCurrentDate;
	// die Schichtnummer, f�r den der Dialog aufgerufen wurde
	CString omCurrentShift;
	// die Mitarbeiter-Urno, f�r den der Dialog aufgerufen wurde
	long lmStfUrno;

	long lmUrno;
	CString omEName;
	CString omETel;
	CString omEDat;
	CString omEPersNr;
	CString omESollT;
	CString omECarP;
	CString omEQuali;
	CString omERem;

	CString omERemark1;
	CString omEDienstVonD;
	CString omEDienstBisD;
	CString omEDienstVonT;
	CString omEDienstBisT;
	CString omEPauseVon;
	CString omEPauseBis;
	CString omEDauer;
	CString omERemark2;
	
	CButton	*pomCancelButton,
			*pomOKButton;

	CStatic *pomSName;
	CStatic *pomSPersNr;

	CStatic *pomSRemark1;
	CStatic *pomSDienstVon;
	CStatic *pomSDienstBis;
	CStatic *pomSPauseVon;
	CStatic *pomSPauseBis;
	CStatic *pomSDauer;
	CStatic *pomSRemark2;

	CCSEdit *pomEName;
	CCSEdit *pomEPersNr;

	CCSEdit *pomERemark1;
	CCSEdit *pomEDienstVonD;
	CCSEdit *pomEDienstBisD;
	CCSEdit *pomEDienstVonT;
	CCSEdit *pomEDienstBisT;
	CCSEdit *pomEPauseVon;
	CCSEdit *pomEPauseBis;
	CCSEdit *pomEDauer;
	CCSEdit *pomERemark2;

	CPtrArray omWndPtrArray;

// Operationen
public:
	void InternalCancel();

// �berschreibungen
	// Vom Klassen-Assistenten generierte virtuelle Funktions�berschreibungen
	//{{AFX_VIRTUAL(DutyPlanPs)
	public:
	virtual BOOL OnInitDialog();
	//}}AFX_VIRTUAL

// Implementierung
public:
	virtual ~DutyPlanPs();
	void DrawLine(CDC *opDC, long left, long top, long right, long bottom, COLORREF color);

	// Generierte Nachrichtenzuordnungsfunktionen
protected:
	// Das Grid
	CGridControl *pomDrrGrid;
	
	// Grid initialisieren
	void InitDrrGrid(CRect opRect);
	// Spaltenbreite des Grids einstellen
	void InitDrrColWidths();
	// Grid mit DRR-Eintr�gen f�llen
	void FillDrrGrid();
	// Eintr�ge im Grid sortieren
	bool SortDrrGrid(int ipRow);

	//{{AFX_MSG(DutyPlanPs)
	virtual void OnOK();
	afx_msg void OnPaint();
	afx_msg void OnAppExit();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:

};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio f�gt zus�tzliche Deklarationen unmittelbar vor der vorhergehenden Zeile ein.

#endif // AFX_DUTYPLANPS_H__E3451C51_8E79_11D2_8E3E_0000C002916B__INCLUDED_
