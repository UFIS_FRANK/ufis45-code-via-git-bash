#ifndef AFX_SHIFTAWDLG_H__7891F411_CEC0_11D1_BFCE_004095434A85__INCLUDED_
#define AFX_SHIFTAWDLG_H__7891F411_CEC0_11D1_BFCE_004095434A85__INCLUDED_

// ShiftAWDlg.h : Header-Datei
//

#include <stdafx.h>
#include <basicdata.h>
#include <ShiftRoster_View.h>
#include <GridControl.h>

// Auszug aus STF
struct AWSTFVIEWDATA_SHFT
{
	CString	Urno;  	// Eindeutige Datensatz-Nr.
	CString Finm; 	// Vorname
	CString Lanm;	// Name
	CString Makr;	// Mitarbeiterkreis
	CString Perc; 	// Kürzel
	CString Prmc; 	// Funktion
	CString Qual; 	// Qualifikation
	CString Wgpc; 	// Arbeitsgruppen
	CString Peno;	// Personalnummer
	CString Code;	// Contract code
	COleDateTime Dodm; 	// Austrittsdatum
	COleDateTime Doem;	// Eintrittsdatum
	bool Select;	// MA ist in GSP enthalten

	AWSTFVIEWDATA_SHFT(void)
	{
		Select = false;
	}
};
// end AWSTFVIEWDATA_SHFT

// Auszug aus DBO
struct AWDBOVIEWDATA_SHFT
{
	CString Grpn; 	// Beaufschlagungs-Gruppenname (aus GRN)
	CString Valu; 	// Werte der Grupierung (BSD-Urnos aus GRM)
	CString	Urno;  	// Eindeutige Datensatz-Nr. (GRN)
};
// end AWDBOVIEWDATA_SHFT

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld ShiftAWDlg 

class ShiftAWDlg : public CDialog
{
// Konstruktion
public:
	//ShiftAWDlg::ShiftAWDlg (UINT nIDTemplate, CWnd* pParent);
	ShiftAWDlg(CString *popUrnoList, UINT ipAWType, bool bpSelect, CString opVpfr, CString opVpto, CWnd* pParent = NULL, CCSButtonCtrl* pButton = NULL);
	~ShiftAWDlg(void);
	void ProcessDropStaff(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
	void ProcessViewChanged();
// Dialogfelddaten
	//{{AFX_DATA(ShiftAWDlg)
	enum { IDD = IDD_SHIFTROSTER_AW_DLG };
	CButton	m_Check_ShowAll;
	CButton	m_B_Info;
	CButton	m_B_Ok;
	CButton	m_B_Cancel;
	CButton	m_B_DeleteAll;
	//}}AFX_DATA

// Überschreibungen
	// Vom Klassen-Assistenten generierte virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(ShiftAWDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	//}}AFX_VIRTUAL

// Implementierung
protected:

	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(ShiftAWDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	afx_msg void OnBDeleteall();
	afx_msg void OnInfo();
	afx_msg void OnCheckShowAll();
	virtual void OnCancel();
	afx_msg LONG OnGridLButton(WPARAM wParam, LPARAM lParam);
	afx_msg LONG OnGridDragBegin(WPARAM wParam, LPARAM lParam);
	afx_msg void OnDestroy();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
//
public:
	void IniEmplGrid();
	CString *pomUrnoList;
	CCSPtrArray<AWSTFVIEWDATA_SHFT>  omStfLines;
	// Objekt für D'n'D-Funktionalität
	CCSDragDropCtrl omDragDropObject;
	bool bmSelect;
	UINT imAWType;
	CString omVpfr;
	CString omVpto;

protected:
	bool FilterFunction(AWSTFVIEWDATA_SHFT *popStf);
	void FillEmplGrid();
	ShiftRoster_View *pomParent;

private:
	bool IsEmployeeGreyed(long lpStfUrno);
	CGridControl	*pgrid;
	CCSButtonCtrl	*pomButton;
	long			lmLastClickedRow;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio fügt zusätzliche Deklarationen unmittelbar vor der vorhergehenden Zeile ein.

#endif // AFX_SHIFTAWDLG_H__7891F411_CEC0_11D1_BFCE_004095434A85__INCLUDED_
