#ifndef _CSDTSD_H_
#define _CSDTSD_H_

#define SDT_SEND_DDX	(true)
#define SDT_NO_SEND_DDX	(false)
 
#include <stdafx.h>

/////////////////////////////////////////////////////////////////////////////

struct SDTDATA {
	char 	 Bsdc[8+2]; 	// Code
	char 	 Dnam[34]; 		// Bedarfsname
	char 	 Type[3]; 		// Flag (statisch/dynamisch)
	long 	 Urno; 			// Eindeutige Datensatz-Nr.
	long	 Bsdu;			// BSDTAB.URNO
	CTime	 Brkf;			//Pause von
	CTime	 Brkt;			//Pause bis
	CTime 	 Bkf1/*[6]*/; 	// Pausenlage von			====> char 	 Bkf1[6]
	CTime 	 Bkt1/*[6]*/; 	// Pausenlage bis			====> char 	 Bkt1
	CTime	 Sbgi;			//Schichtbeginn
	CTime	 Seni;			//Schichtende
	CTime 	 Esbg; 			// Fr�hester Schichtbeginn		===> char 	 Esbg[6]
	CTime 	 Lsen; 			// Sp�testes Schichtende		===> char 	 Lsen[6]
	long	 Sdgu;
	char     Fctc[FCTC_LEN+2];	// Extra bytes for 'multi-talented' employees, used in displaying roster plans - gsa
	int  bmIsChanged;			// Check whether Data has Changed f�r Relaese
	BOOL bmIsSelected;

	SDTDATA(void)
	{
		memset(this,'\0',sizeof(*this));

		bmIsSelected = FALSE;
		Bkf1 = TIMENULL;
		Bkt1 = TIMENULL;
		Esbg = TIMENULL;
		Lsen = TIMENULL;
		Brkf = TIMENULL;
		Brkt = TIMENULL;
	}
};	
//typedef struct SdtDataStruct SDTDATA;


// the broadcast CallBack function, has to be outside the CedaSdtData class
void ProcessSdtCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

class CedaSdtData: public CedaData
{
// Attributes
public:
    CCSPtrArray<SDTDATA> omData;
    CMapPtrToPtr omUrnoMap;

	void SetTableName(CString opTableName)
	{
		strcpy(pcmTableName, opTableName.GetBuffer(0));
	}

// Operations
public:
    CedaSdtData();
	~CedaSdtData();
	void Register(void);
	bool Read(char *pspWhere = NULL, bool bpRegisterBC = true, bool bpClearAll = true);
	bool ReadSpecialData(CCSPtrArray<SDTDATA> *popSdt, char *pspWhere, char *pspFieldList, bool ipSYS, bool bpRegisterBC = true);

 	void ProcessSdtBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
	bool AddSdtInternal(SDTDATA *prpSdt, bool bpWithDDX = false);
	bool UpdateSdtInternal(SDTDATA *prpSdt);
	bool ClearAll();

	SDTDATA * GetSdtByUrno(long pcpUrno);
	bool SdtExist(long Urno);
	bool InsertSdt(SDTDATA *prpSdtData, BOOL bpWithSave = FALSE);
	bool UpdateSdt(SDTDATA *prpSdtData, BOOL bpWithSave = FALSE);
	bool DeleteSdt(SDTDATA *prpSdtData, BOOL bpWithSave = FALSE);
	bool SaveSdt(SDTDATA *prpSdt);
	bool DeleteSdtInternal(SDTDATA *prpSdt);
	void DeleteSdtBySdgu(long lpSdgUrno);
	void GetSdtByBsdu(long lpBsdu,CCSPtrArray<SDTDATA> *popSdtData);
	void GetSdtBySdgu(long lpSdgu,CCSPtrArray<SDTDATA> *popSdtData);
	bool ReloadSdtBySdgu(long lpSdgu);

protected:
	bool bmIsBCRegistered;

private:

};

#endif
