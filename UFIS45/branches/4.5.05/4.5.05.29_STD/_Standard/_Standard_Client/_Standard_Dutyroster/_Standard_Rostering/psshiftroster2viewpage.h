#ifndef AFX_PSSHIFTROSTER2VIEWPAGE_H__C77E7EB1_9677_11D2_8E47_0000C002916B__INCLUDED_
#define AFX_PSSHIFTROSTER2VIEWPAGE_H__C77E7EB1_9677_11D2_8E47_0000C002916B__INCLUDED_

// PSShiftRoster2ViewPage.h : Header-Datei
//
#include <Ansicht.h>
#include <RosterViewPage.h>

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld PSShiftRoster2ViewPage 

class PSShiftRoster2ViewPage : public RosterViewPage
{
	DECLARE_DYNCREATE(PSShiftRoster2ViewPage)

// Konstruktion
public:
	PSShiftRoster2ViewPage();
	~PSShiftRoster2ViewPage();

// Dialogfelddaten
	//{{AFX_DATA(PSShiftRoster2ViewPage)
	enum { IDD = IDD_PSSHIFTROSTER2 };
	CListBox	m_LB_Functions;
	//}}AFX_DATA

	BOOL GetData();
	BOOL GetData(CStringArray &opValues);
	void SetData();
	void SetData(CStringArray &opValues);

// Überschreibungen
	// Der Klassen-Assistent generiert virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(PSShiftRoster2ViewPage)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	//}}AFX_VIRTUAL

// Implementierung
protected:
	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(PSShiftRoster2ViewPage)
	virtual BOOL OnInitDialog();
	afx_msg void OnAll();
	afx_msg void OnNothing();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

public:
	CString omCalledFrom;
	void SetCalledFrom(CString opCalledFrom);

private:
	CString omTitleStrg;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio fügt zusätzliche Deklarationen unmittelbar vor der vorhergehenden Zeile ein.

#endif // AFX_PSSHIFTROSTER2VIEWPAGE_H__C77E7EB1_9677_11D2_8E47_0000C002916B__INCLUDED_
