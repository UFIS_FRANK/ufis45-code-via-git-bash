// BDA 16.02.2000 erstellt aus CedaDrdData.h

#ifndef _CEDADRGDATA_H_
#define _CEDADRGDATA_H_

#include <stdafx.h>
/*#include "CCSPtrArray.h"
#include <CCSCedaData.h>
#include <ccsddx.h>
#include <ccsglobl.h>
#include <CedaDrrData.h> // f�r IsValidDrg()*/

/////////////////////////////////////////////////////////////////////////////

// COMMANDS FOR MEMBER-FUNCTIONS
#define DRG_SEND_DDX	(true)
#define DRG_NO_SEND_DDX	(false)

// Struktur eines DRG-Datensatzes
struct DRGDATA {
	char			Drrn[DRRN_LEN+2]; 	// Schichtnummer (1-n pro Tag)
	char 			Fctc[FCTC_LEN+2]; 	// Funktion des Mitarbeiters (Code aus PFC.FCTC)
	char			Hopo[HOPO_LEN+2];	// Homeairport
	char 			Sday[SDAY_LEN+2]; 	// Tages-Schl�ssel YYYYMMDD
	long 			Stfu;					// Mitarbeiter-Urno
	long 			Urno;					// Datensatz-Urno
	char 			Wgpc[WGPC_LEN+2]; 	// Arbeitsgruppe (Code aus WGP)

	//DataCreated by this class
	int			IsChanged;	// Check whether Data has Changed f�r Relaese

	// Initialisation
	DRGDATA(void)
	{ 
		memset(this,'\0',sizeof(*this));
		IsChanged = DATA_UNCHANGED;
	}
};	





/************************************************************************************************************************
************************************************************************************************************************
Deklaration CedaDrgData: kapselt den Zugriff auf die Tabelle DRG: Daily Roster Groups - Tagesdienstplan Arbeitsgruppen

  DRG ersetzt bei einem bestimmten Mitarbeiter seine Arbeitsgruppe und/oder seine Funktion gegen�ber Stammdaten
************************************************************************************************************************
************************************************************************************************************************/

class CedaDrgData : public CedaData  
{
// Funktionen
public:
    // Konstruktor/Destruktor
	CedaDrgData(CString opTableName = "DRG", CString opExtName = "TAB");
	~CedaDrgData();

// allgemeine Funktionen
	void SetTableName(CString opTableName)
	{
		strcpy(pcmTableName, opTableName.GetBuffer(0));
	}
	// Feldliste lesen
	CString GetFieldList(void) {return CString(pcmListOfFields);}

// Broadcasts empfangen und bearbeiten
	// behandelt die Broadcasts BC_DRG_CHANGE,BC_DRG_DELETE und BC_DRG_NEW
	void ProcessDrgBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

// Lesen/Schreiben von Datens�tzen
	// Datens�tze nach Datum gefiltert lesen
	bool ReadFilteredByDateAndStaff(COleDateTime opStart, COleDateTime opEnd,
									CMapPtrToPtr *popLoadStfUrnoMap = NULL,
									bool bpRegisterBC = true, bool bpClearData = true);
	// Datens�tze einlesen
	bool Read(char *pspWhere = NULL, CMapPtrToPtr *popLoadStfUrnoMap = NULL,
			  bool bpRegisterBC = true, bool bpClearData = true);
	// Datens�tze mit Hilfe selbst-formatierter SQL-String einlesen
	bool ReadSpecialData(CCSPtrArray<DRGDATA> *popDrgArray,char *pspWhere,char *pspFieldList, char *pcpSort=0,bool ipSYS=true);
	// Datensatz mit bestimmter Urno einlesen
	bool ReadDrgByUrno(long lpUrno, DRGDATA *prpDrg);
	
	// alle DRGs eines Mitarbeiters aus der internen Datenhaltung entfernen (ohne aus der DB zu l�schen!!!)
	bool RemoveInternalByStaffUrno(long lpStfUrno, bool bpSendBC = false);

	// einen Datensatz aus der Datenbank l�schen
	bool Delete(DRGDATA *prpDrg, BOOL bpWithSave = true);
	// einen neuen Datensatz in der Datenbank speichern
	bool Insert(DRGDATA *prpDrg, bool bpSave = true);
	// einen ge�nderten Datensatz speichern
	bool Update(DRGDATA *prpDrg);

// Datens�tze suchen
	// DRG mit Urno <lpUrno> suchen
	DRGDATA* GetDrgByUrno(long llUrno);
	// DRG mit dem Prim�rschl�ssel aus Schichttag (<opSday>), 
	// Mitarbeiter-Urno (<lpStfu>) und Schichtnummer (<opDrrn>) suchen
	DRGDATA* GetDrgByKey(CString opSday, long lpStfu, CString opDrrn);
	// DRG mit dem Prim�rschl�ssel aus Schichttag (<opSday>) und 
	// Mitarbeiter-Urno (<lpStfu>) suchen
	// Drrn liegt im Bereich von 1 bis 3
	DRGDATA* GetDrgBySdayAndStfu(CString opSday, long lpStfu);

	// sucht alle Datens�tze im angegebenen Zeitraum opEnd - opStart raus
	int	GetDrgArrayByTime(COleDateTime opStart, COleDateTime opEnd, char* opSortString, CCSPtrArray<DRGDATA> *popDrgData);
// Zugriff auf interne Daten
	// liefert den Zeiger auf die interne Datenhaltung <omData>
	DRGDATA		GetInternalData (int ipIndex) {return omData[ipIndex];}
	// liefert die Anzahl der Datens�tze in der internen Datenhaltung <omData>
	int			GetInternalSize() {return omData.GetSize();}

	// IsValidDrg: pr�ft alles, was man pr�fen kann, inklusive g�ltigen Codes und duplizierten DRGs
	bool IsValidDrg(DRGDATA *popDrg);
	
	// zwei DRGs miteinander vergleichen: Ergebnis false -> DRGs sind gleich
	bool CompareDrgToDrg(DRGDATA *popDrg1, DRGDATA *popDrg2,bool bpCompareKey = false);
	// kopiert alles, ausser Urno
	void CopyDrg(DRGDATA* popDrgDataSource,DRGDATA* popDrgDataTarget);

// Manipulation von Datens�tzen
	// DRGDATA erzeugen, initialisieren und in die Datenhaltung mit aufnehmen
	DRGDATA* CreateDrgData(long lpStfUrno, CString opDay, CString opDrrn);
	// DRGDATA erzeugen und initialisieren mit angegebenen Parametern ohne Aufnahme in die interne Datenhaltung
	DRGDATA* CreateDrgData(char* ppDrrn, char* ppFctc, char* ppHopo, char* ppSday, long lpStfu, char* ppWgpc);

// Daten
 
protected:	
// Funktionen
// Broadcasts empfangen und bearbeiten
	// beim Broadcast-Handler anmelden
	void Register(void);

// allgemeine Funktionen
	// Tabellenname einstellen
	bool SetTableNameAndExt(CString opTableAndExtName);
	// Feldliste schreiben
	void SetFieldList(CString opFieldList) {strcpy(pcmListOfFields,opFieldList.GetBuffer(0));}
	// alle internen Arrays zur Datenhaltung leeren und beim BC-Handler abmelden
	bool ClearAll(bool bpUnregister = true);

// Datens�tze speichern/bearbeiten

	// speichert einen einzelnen Datensatz
	bool Save(DRGDATA *prpDrg);
	// wenn es ein neuer Datensatz ist, einen Broadcast DRG_NEW abschicken und den Datensatz in die interne 
	// Datenhaltung aufnehmen, sonst return false
	bool InsertInternal(DRGDATA *prpDrg, bool bpSendDdx = true);
	// einen Broadcast DRG_CHANGE versenden
	bool UpdateInternal(DRGDATA *prpDrg, bool bpSendDdx = true);
	// einen Datensatz aus der internen Datenhaltung l�schen und einen Broadcast senden
	void DeleteInternal(DRGDATA *prpDrg, bool bpSendDdx = true);
	// Sortieren 
	void SortBySurnAndTime(CCSPtrArray<DRGDATA> *popDrgArray);

// Kommunikation mit CEDA
	// Kommandos an CEDA senden (die Funktionen der Basisklasse werden �berschrieben
	// wegen des Feldlisten Bugs)
	bool CedaAction(char *pcpAction, char *pcpTableName, char *pcpFieldList,
					char *pcpSelection, char *pcpSort, char *pcpData, 
					char *pcpDest = "BUF1",bool bpIsWriteAction = false, 
					long lpID = 0, CCSPtrArray<RecordSet> *pomData = NULL, 
					int ipFieldCount = 0);
    bool CedaAction(char *pcpAction, char *pcpSelection = NULL, char *pcpSort = NULL,
					char *pcpData = pcgDataBuf, char *pcpDest = "BUF1", 
					bool bpIsWriteAction = false, long lpID = 0);

// Daten
	// die geladenen Datens�tze
	CCSPtrArray<DRGDATA> omData;
    // Map mit den Datensatz-Urnos der geladenen Datens�tze
	CMapPtrToPtr omUrnoMap;
    // Map mit den Prim�rschl�sseln (aus SDAY, STFU und DRRN) der Datens�tze
	CMapStringToPtr omKeyMap;
	// beim Broadcast-Handler angemeldet?
	bool bmIsBCRegistered;
};

#endif // !defined _CEDADRGDATA_H_
