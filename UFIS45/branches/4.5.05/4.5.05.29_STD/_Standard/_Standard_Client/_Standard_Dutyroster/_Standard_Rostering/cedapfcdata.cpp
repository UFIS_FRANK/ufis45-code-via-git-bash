// CedaPfcData.cpp
 
#include <stdafx.h>


void ProcessPfcCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//--CEDADATA-----------------------------------------------------------------------------------------------

CedaPfcData::CedaPfcData() : CedaData(&ogCommHandler)
{
	// Create an array of CEDARECINFO for PFCDATA
	BEGIN_CEDARECINFO(PFCDATA,PfcDataRecInfo)
		FIELD_DATE(Cdat,"CDAT")
		FIELD_CHAR_TRIM(Dptc,"DPTC")
		FIELD_CHAR_TRIM(Fctc,"FCTC")
		FIELD_CHAR_TRIM(Fctn,"FCTN")
		FIELD_DATE(Lstu,"LSTU")
		FIELD_CHAR_TRIM(Prfl,"PRFL")
		FIELD_CHAR_TRIM(Rema,"REMA")
		FIELD_LONG(Urno,"URNO")
		FIELD_CHAR_TRIM(Usec,"USEC")
		FIELD_CHAR_TRIM(Useu,"USEU")

	END_CEDARECINFO //(PFCDATA)

	// Copy the record structure
	for (int i=0; i< sizeof(PfcDataRecInfo)/sizeof(PfcDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&PfcDataRecInfo[i],sizeof(CEDARECINFO));
		omRecInfo.Add(prpCedaRecInfo);
	} // end for

	// initialize field names
	strcpy(pcmTableName,"PFC");
	strcat(pcmTableName,pcgTableExt);
	strcpy(pcmListOfFields,"CDAT,DPTC,FCTC,FCTN,LSTU,PRFL,REMA,URNO,USEC,USEU");
	pcmFieldList = pcmListOfFields; //because in CedaData pcmFieldList is just a pointer

	Register();
	omData.RemoveAll();
}

//---------------------------------------------------------------------------------------------------------

void CedaPfcData::GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType)
{

}

//--REGISTER----------------------------------------------------------------------------------------------

void CedaPfcData::Register(void)
{
	DdxRegister((void *)this,BC_PFC_CHANGE,	"PFCDATA", "Pfc-changed",	ProcessPfcCf);
	DdxRegister((void *)this,BC_PFC_NEW,	"PFCDATA", "Pfc-new",		ProcessPfcCf);
	DdxRegister((void *)this,BC_PFC_DELETE,	"PFCDATA", "Pfc-deleted",	ProcessPfcCf);
}

//--~CEDADATA----------------------------------------------------------------------------------------------

CedaPfcData::~CedaPfcData(void)
{
	omRecInfo.DeleteAll();
	ogDdx.UnRegister(this,NOTUSED);
	ClearAll();
}

//--CLEAR-ALL----------------------------------------------------------------------------------------------

void CedaPfcData::ClearAll(bool bpWithRegistration)
{
    omUrnoMap.RemoveAll();
    omFctcMap.RemoveAll();
    omData.DeleteAll();
	if(bpWithRegistration)
	{
		ogDdx.UnRegister(this,NOTUSED);
	}
}

//--READ-ALL-----------------------------------------------------------------------------------------------

bool CedaPfcData::Read(char *pspWhere /*NULL*/)
{
    // Select data from the database
	bool ilRc = true;
    omUrnoMap.RemoveAll();
    omFctcMap.RemoveAll();
    omData.DeleteAll();
		
	if(IsFullLoggingEnabled())
	{
		ogRosteringLogText.Format("CedaAction:\n  Cmd <RT>\n  Tbl <%s>\n  Sel <%s>",pcmTableName, pspWhere);
		WriteInRosteringLog();
	}

	if(pspWhere == NULL)
	{	
		ilRc = CedaAction2("RT");
	}
	else
	{
		ilRc = CedaAction2("RT", pspWhere);
	}
	if (ilRc != true)
	{
		return ilRc;
	}
	// Load data from CedaData into the dynamic array of record
	for (int ilCountRecord = 0; ilRc == true; ilCountRecord++)
	{
		PFCDATA *prlPfc = new PFCDATA;
		if ((ilRc = GetFirstBufferRecord2(prlPfc)) == true)
		{
			omData.Add(prlPfc);//Update omData
			omUrnoMap.SetAt((void *)prlPfc->Urno,prlPfc);

			CString olTmp;
			olTmp.Format("%s",prlPfc->Fctc);
			omFctcMap.SetAt(olTmp,prlPfc);
#ifdef TRACE_FULL_DATA
			// Datensatz OK, loggen if FULL
			ogRosteringLogText.Format(" read %8.8lu %s OK:  ", ilCountRecord, pcmTableName);
			GetDataFormatted(ogRosteringLogText, prlPfc);
			WriteLogFull("");
#endif TRACE_FULL_DATA
		}
		else
		{
			delete prlPfc;
		}
	}

	if(IsFullLoggingEnabled())
	{
		ogRosteringLogText.Format("CedaAction:\n  read %d records from table %s",ilCountRecord-1, pcmTableName);
		WriteInRosteringLog();
	}

    return true;
}

//--INSERT-------------------------------------------------------------------------------------------------

bool CedaPfcData::Insert(PFCDATA *prpPfc)
{
	prpPfc->IsChanged = DATA_NEW;
	if(Save(prpPfc) == false) return false; //Update Database
	InsertInternal(prpPfc);
    return true;
}

//--INSERT-INTERNAL--------------------------------------------------------------------------------------

bool CedaPfcData::InsertInternal(PFCDATA *prpPfc)
{
	ogDdx.DataChanged((void *)this, PFC_NEW,(void *)prpPfc ); //Update Viewer
	omData.Add(prpPfc);//Update omData
	omUrnoMap.SetAt((void *)prpPfc->Urno,prpPfc);

	CString olTmp;
	olTmp.Format("%s",prpPfc->Fctc);
	omFctcMap.SetAt(olTmp,prpPfc);

    return true;
}

//--DELETE-------------------------------------------------------------------------------------------------

bool CedaPfcData::Delete(long lpUrno)
{
	PFCDATA *prlPfc = GetPfcByUrno(lpUrno);
	if (prlPfc != NULL)
	{
		prlPfc->IsChanged = DATA_DELETED;
		if(Save(prlPfc) == false) return false; //Update Database
		DeleteInternal(prlPfc);
	}
    return true;
}

//--DELETE-INTERNAL----------------------------------------------------------------------------------------

bool CedaPfcData::DeleteInternal(PFCDATA *prpPfc)
{
	ogDdx.DataChanged((void *)this,PFC_DELETE,(void *)prpPfc); //Update Viewer
	omUrnoMap.RemoveKey((void *)prpPfc->Urno);

	CString olTmp;
	olTmp.Format("%s",prpPfc->Fctc);
	omFctcMap.RemoveKey(olTmp);

	int ilPfcCount = omData.GetSize();
	for (int ilCountRecord = 0; ilCountRecord < ilPfcCount; ilCountRecord++)
	{
		if (omData[ilCountRecord].Urno == prpPfc->Urno)
		{
			omData.DeleteAt(ilCountRecord);//Update omData
			break;
		}
	}
    return true;
}

//--UPDATE-------------------------------------------------------------------------------------------------

bool CedaPfcData::Update(PFCDATA *prpPfc)
{
	if (GetPfcByUrno(prpPfc->Urno) != NULL)
	{
		if (prpPfc->IsChanged == DATA_UNCHANGED)
		{
			prpPfc->IsChanged = DATA_CHANGED;
		}
		if(Save(prpPfc) == false) return false; //Update Database
		UpdateInternal(prpPfc);
	}
    return true;
}

//--UPDATE-INTERNAL--------------------------------------------------------------------------------------------

bool CedaPfcData::UpdateInternal(PFCDATA *prpPfc)
{
	PFCDATA *prlPfc = GetPfcByUrno(prpPfc->Urno);
	if (prlPfc != NULL)
	{
		*prlPfc = *prpPfc; //Update omData
		ogDdx.DataChanged((void *)this,PFC_CHANGE,(void *)prlPfc); //Update Viewer
	}
    return true;
}

//--GET-BY-URNO--------------------------------------------------------------------------------------------

PFCDATA *CedaPfcData::GetPfcByUrno(long lpUrno)
{
	PFCDATA  *prlPfc;
	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlPfc) == TRUE)
	{
		return prlPfc;
	}
	return NULL;
}

//--GET-BY-FCTC--------------------------------------------------------------------------------------------

PFCDATA *CedaPfcData::GetPfcByFctc(CString opFctc)
{
	PFCDATA  *prlPfc;
	if(omFctcMap.Lookup(opFctc,(void *&)prlPfc) == TRUE)
	{
		return prlPfc;
	}
	return NULL;
}

//--READSPECIALDATA-------------------------------------------------------------------------------------

bool CedaPfcData::ReadSpecialData(CCSPtrArray<PFCDATA> *popPfc,char *pspWhere,char *pspFieldList,bool ipSYS/*=true*/)
{
	bool ilRc = true;
	char pclFieldList[256] = " ";

	if(strlen(pspFieldList) > 0) 
	{
		strcpy(pclFieldList, pspFieldList);
	}
		
	if(IsFullLoggingEnabled())
	{
		ogRosteringLogText.Format("CedaAction:\n  Cmd <RT>\n  Tbl <%s>\n  Sel <%s>",pcmTableName, pspWhere);
		WriteInRosteringLog();
	}

	if(ipSYS == true) 
	{
		if (CedaAction("SYS",pcmTableName,pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	else
	{
		if (CedaAction2("RT",pcmTableName,pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	if(popPfc != NULL)
	{
		for (int ilCountRecord = 0; ilRc == true; ilCountRecord++)
		{
			PFCDATA *prpPfc = new PFCDATA;
			if ((ilRc = GetBufferRecord2(ilCountRecord,prpPfc,CString(pclFieldList))) == true)
			{
				popPfc->Add(prpPfc);
			}
			else
			{
				delete prpPfc;
			}
		}
		if(popPfc->GetSize() == 0) return false;
	}
    return true;
}

//--SAVE---------------------------------------------------------------------------------------------------

bool CedaPfcData::Save(PFCDATA *prpPfc)
{
	bool ilRc = true;
	CString olListOfData;
	char pclSelection[124];
	char pclData[524];

	if (prpPfc->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}
	switch(prpPfc->IsChanged)
	{
	case DATA_NEW:
		MakeCedaData(&omRecInfo,olListOfData,prpPfc);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("IRT","","",pclData);
		prpPfc->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpPfc->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpPfc);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("URT",pclSelection,"",pclData);
		prpPfc->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_DELETED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpPfc->Urno);
		ilRc = CedaAction("DRT",pclSelection);
		break;
	}
    return ilRc;
}

//--PROCESS-CF---------------------------------------------------------------------------------------------

void  ProcessPfcCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	ogPfcData.ProcessBc(ipDDXType,vpDataPointer,ropInstanceName);
}

//--PROCESS-BC---------------------------------------------------------------------------------------------

void  CedaPfcData::ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlPfcData;
	prlPfcData = (struct BcStruct *) vpDataPointer;
	CString olSelection = (CString)prlPfcData->Selection;
	PFCDATA *prlPfc;
	long llUrno;

		
	if(IsFullLoggingEnabled())
	{
		ogRosteringLogText.Format("ProcessBC:\n  Cmd <%s>\n  Tbl <%s>\n  Twe <%s>\n  Sel <%s>\n  Fld <%s>\n  Dat <%s>\n  Bcn <%s>",prlPfcData->Cmd, prlPfcData->Object, prlPfcData->Twe, prlPfcData->Selection, prlPfcData->Fields, prlPfcData->Data,prlPfcData->BcNum);
		WriteInRosteringLog();
	}

	switch(ipDDXType)
	{
	default:
		break;
	case BC_PFC_CHANGE:
		if (olSelection.Find('\'') != -1)
		{
			llUrno = GetUrnoFromSelection(prlPfcData->Selection);
		}
		else
		{
			int ilFirst = olSelection.Find("=")+2;
			int ilLast  = olSelection.GetLength();
			llUrno = atol(olSelection.Mid(ilFirst,ilLast-ilFirst));
		}
//		long llUrno = GetUrnoFromSelection(prlPfcData->Selection);
		prlPfc = GetPfcByUrno(llUrno);
		if(prlPfc != NULL)
		{
			GetRecordFromItemList(prlPfc,prlPfcData->Fields,prlPfcData->Data);
			UpdateInternal(prlPfc);
			break;
		}
	case BC_PFC_NEW:
		prlPfc = new PFCDATA;
		GetRecordFromItemList(prlPfc,prlPfcData->Fields,prlPfcData->Data);
		InsertInternal(prlPfc);
		break;
	case BC_PFC_DELETE:
		if (olSelection.Find('\'') != -1)
		{
			llUrno = GetUrnoFromSelection(prlPfcData->Selection);
		}
		else
		{
			int ilFirst = olSelection.Find("=")+2;
			int ilLast  = olSelection.GetLength();
			llUrno = atol(olSelection.Mid(ilFirst,ilLast-ilFirst));
		}
		prlPfc = GetPfcByUrno(llUrno);
		if (prlPfc != NULL)
		{
			DeleteInternal(prlPfc);
		}
		break;
	}
}

//---------------------------------------------------------------------------------------------------------
