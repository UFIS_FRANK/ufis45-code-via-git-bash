// DutyAbsenceOverview.cpp : implementation file
//

#include <stdafx.h>
#include <rostering.h>
#include <DutyAbsenceOverviewDlg.h>
#include <DutyPlanPs.h>
#include <gxall.h> 
#include <resource.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// DutyAbsenceOverview dialog defines

#define	DAOGRID_USE_ENABLED	
#undef	DAOGRID_USE_ENABLED

////////////////////////////////////////////////////////////////////////
// DutyAbsenceOverview dialog constants

const int DAOGRID_MINROWS = 31;


/////////////////////////////////////////////////////////////////////////////
// DutyAbsenceOverview dialog class members

void DutyAbsenceOverviewDlg::ProcessDdx(void *popInstance, int ipDDXType,void *vpDataPointer, CString &ropInstanceName)
{
    DutyAbsenceOverviewDlg *polDlg = (DutyAbsenceOverviewDlg *)popInstance;
	ASSERT(polDlg);

	DRRDATA *polDrr = static_cast<DRRDATA *>(vpDataPointer);
	ASSERT(polDrr);

	// unser Mitarbeiter ?
	if (polDrr->Stfu != polDlg->lmStfUrno)
		return;

	// innerhalb des betrachteten Zeitrahmens ?
	if (polDrr->Sday < polDlg->omTimeFrom.Format("%Y%m%d") || polDrr->Sday > polDlg->omTimeTo.Format("%Y%m%d"))
		return;

	switch(ipDDXType)
	{
	case DRR_NEW	:
		polDlg->OnNewDrr(polDrr);
	break;
	case DRR_CHANGE :
		polDlg->OnChangeDrr(polDrr);
	break;
	}
}

/////////////////////////////////////////////////////////////////////////////
// DutyAbsenceOverview dialog


DutyAbsenceOverviewDlg::DutyAbsenceOverviewDlg(long lpStfUrno,const COleDateTime& ropTimeFrom,const COleDateTime& ropTimeTo,const CString& ropCode,CWnd* pParent,bool bpUpdateActiveDRR,bool bpUpdateVacancyDRR)
	: CDialog(DutyAbsenceOverviewDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(DutyAbsenceOverview)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	lmStfUrno	= lpStfUrno;
	omTimeFrom	= ropTimeFrom;
	omTimeTo	= ropTimeTo;
	omCode		= ropCode;
	omTimeSpan	= omTimeTo - omTimeFrom;
	bmUpdateActiveDRR	= bpUpdateActiveDRR;
	bmUpdateVacancyDRR  = bpUpdateVacancyDRR;
	pomGrid		= NULL;
	ogDdx.Register(this,DRR_CHANGE,	CString("DutyAbsenceOverviewDlg"),CString("DRR changed"),	ProcessDdx);
	ogDdx.Register(this,DRR_NEW,	CString("DutyAbsenceOverviewDlg"),CString("DRR new"),		ProcessDdx);
}

DutyAbsenceOverviewDlg::~DutyAbsenceOverviewDlg()
{
	ogDdx.UnRegister(this,NOTUSED);
	delete pomGrid;
}

void DutyAbsenceOverviewDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(DutyAbsenceOverviewDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(DutyAbsenceOverviewDlg, CDialog)
	//{{AFX_MSG_MAP(DutyAbsenceOverviewDlg)
	ON_WM_CLOSE()
	ON_BN_CLICKED(ID_APPLY, OnApply)
	ON_BN_CLICKED(IDSELECTALL, OnSelectall)
	ON_BN_CLICKED(IDUNSELECTALL, OnUnselectall)
	ON_COMMAND(ID_DAO_RBTN_SELECT,OnRBtnSelect)
	ON_COMMAND(ID_DAO_RBTN_UNSELECT,OnRBtnUnselect)
	ON_COMMAND(ID_DAO_RBTN_DELETE,OnRBtnDelete)
	ON_COMMAND(ID_DAO_RBTN_APPLY,OnRBtnApply)
	ON_MESSAGE(WM_GRID_LBUTTONDBLCLK,OnLBtnDblClk)
//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// DutyAbsenceOverview message handlers

BOOL DutyAbsenceOverviewDlg::OnInitDialog() 
{
	if (!CDialog::OnInitDialog())
		return FALSE;
	
	// Pr�fen, ob der Abwesenheitscode existiert
	ODADATA *prlOda = NULL;
	if (this->omCode.GetLength() > 0)
	{
		prlOda = ogOdaData.GetOdaBySdac(this->omCode);
		if (!prlOda)
			return FALSE;
	}

	// window caption setzen
	CString olText;
	olText.Format(LoadStg(IDS_STRING1938),this->omCode); //++++ LoadStg benutzen !
	this->SetWindowText(olText);

	// Pr�fen, ob der darzustellende Zeitraum mit dem geladenenen Zeitraum �bereinstimmt
	CWaitCursor olWait;

	DutyPlanPs *polParent = DYNAMIC_DOWNCAST(DutyPlanPs,this->GetParent());
	if (polParent == NULL || polParent->pomParent == NULL)
		return FALSE;

	COleDateTime	olLoadDateFrom	= polParent->pomParent->rmViewInfo.oLoadDateFrom;
	COleDateTime	olLoadDateTo	= polParent->pomParent->rmViewInfo.oLoadDateTo;
	COleDateTime	olDeleteFrom	= this->omTimeFrom;
	COleDateTime	olDeleteTo		= this->omTimeTo;

	int ilCheckVal = CedaDataHelper::GetMaxPeriod(olLoadDateFrom,olLoadDateTo,olDeleteFrom,olDeleteTo,&olLoadDateFrom,&olLoadDateTo);
	if (ilCheckVal == CHECK_INVALID_DATE)
	{
		return FALSE;
	}
	else if (ilCheckVal != CHECK_P2_INNER_P1)
	{
		CString olSQL;	
		bool blLoadFailed = false;
	

		// SQL-Statement f�r DRR/DRS/DRA/DRD (benutzen alle dasselbe)
		olSQL.Format("WHERE STFU='%ld' AND SDAY BETWEEN '%s' AND '%s'",lmStfUrno,olDeleteFrom.Format("%Y%m%d"),olDeleteTo.Format("%Y%m%d"));
	
		// DRR-Daten nachladen
		if(ogDrrData.Read(olSQL.GetBuffer(0),NULL,true,false) == false)
		{
			ogDrrData.omLastErrorMessage.MakeLower();
			if(ogDrrData.omLastErrorMessage.Find("no data found") == -1)
			{
				CString olErrorTxt;
				olErrorTxt.Format("%s %d\n%s",LoadStg(ST_READERR), ogDrrData.imLastReturnCode, ogDrrData.omLastErrorMessage);
				MessageBox(olErrorTxt, LoadStg(ST_FEHLER), MB_ICONEXCLAMATION);
				blLoadFailed = true;
			}
		}
		olSQL.ReleaseBuffer();
	}

	//Initialisierungsparameter f�r's Grid setzen
	imRowCount = __max(omTimeSpan.GetTotalDays()+1,DAOGRID_MINROWS);
	imColCount = 6;

	BOOL blResult = InitGrid();

	return blResult;	// return TRUE unless you set the focus to a control
						// EXCEPTION: OCX Property Pages should return FALSE
}

BOOL DutyAbsenceOverviewDlg::InitGrid()
{
	CCS_TRY;

	BOOL blResult = FALSE;
	// Warte Cursor einblenden
	CWaitCursor olDummy;

	// Grid erzeugen
	pomGrid = new CDutyAbsenceOverviewGridControl (this, IDC_ABSENCE_GRID,imColCount,imRowCount);

	// Header setzen
	CString olHeader1 = LoadStg(IDS_STRING1942);
	pomGrid->SetValueRange(CGXRange(0,1), olHeader1);
	pomGrid->SetValueRange(CGXRange(0,2), "");
	pomGrid->SetValueRange(CGXRange(0,3), "");
	pomGrid->SetValueRange(CGXRange(0,4), "");
	pomGrid->SetValueRange(CGXRange(0,5), "");
	pomGrid->SetValueRange(CGXRange(0,6), "");


	// Header style setzen
	pomGrid->SetStyleRange(CGXRange(0,2),CGXStyle().SetInterior(ogColors[RED_IDX]));	// Urlaub
	pomGrid->SetStyleRange(CGXRange(0,3),CGXStyle().SetInterior(ogColors[BLACK_IDX]));	// Dienstplan
	pomGrid->SetStyleRange(CGXRange(0,4),CGXStyle().SetInterior(ogColors[BLUE_IDX]));	// MA - Tageliste
	pomGrid->SetStyleRange(CGXRange(0,5),CGXStyle().SetInterior(ogColors[PURPLE_IDX]));	// Tagesendbearbeitung
	pomGrid->SetStyleRange(CGXRange(0,6),CGXStyle().SetInterior(ogColors[ORANGE_IDX]));	// Nachbearbeitung

	// Breite der Columns setzen

	pomGrid->SetColWidth ( 0, 0, 34);
	pomGrid->SetColWidth ( 1, 1, 60);
	pomGrid->SetColWidth ( 2, 2, 30);
	pomGrid->SetColWidth ( 3, 3, 30);
	pomGrid->SetColWidth ( 4, 4, 30);
	pomGrid->SetColWidth ( 5, 5, 30);
	pomGrid->SetColWidth ( 6, 6, 30);
	
	// Grid initialisieren
	pomGrid->Initialize();

	// vertikale Scrollbar aktivieren
	pomGrid->SetScrollBarMode(SB_VERT,gxnAutomatic);
	
	// Grid mit Daten f�llen.
	blResult = FillGrid();

	// Verhindert, das die Spaltenbreite ver�ndert wird
	pomGrid->GetParam()->EnableTrackColWidth(FALSE);

	// Es k�nnen Zellen selektiert werden
	pomGrid->GetParam()->EnableSelection(GX_SELCELL|GX_SELMULTIPLE);

	pomGrid->SetStyleRange( CGXRange().SetTable(), CGXStyle()
				.SetVerticalAlignment(DT_VCENTER)
				.SetReadOnly(TRUE)
				.SetControl(GX_IDS_CTRL_STATIC)
				);
	pomGrid->EnableAutoGrow ( false );

	return blResult;
	CCS_CATCH_ALL;
	return FALSE;
}

BOOL DutyAbsenceOverviewDlg::FillGrid()
{
	CCS_TRY;
		COleDateTime olCurrent = omTimeFrom;

		DutyRoster_View *polParent = (DutyRoster_View *)this->GetParent();
		ASSERT(polParent);

		pomGrid->LockUpdate(TRUE);
		pomGrid->GetParam()->SetLockReadOnly(false);

		pomGrid->SetStyleRange(CGXRange().SetTable(),CGXStyle().SetReadOnly(TRUE));
#ifdef	DAOGRID_USE_ENABLED
		pomGrid->SetStyleRange(CGXRange().SetTable(),CGXStyle().SetEnabled(FALSE));
#else
		pomGrid->SetStyleRange(CGXRange().SetTable(),CGXStyle().SetEnabled(FALSE));
#endif
//		pomGrid->SetStyleRange(CGXRange().SetTable(),CGXStyle().SetIncludeItemDataPtr(FALSE));

		for (int i = 0; i < omTimeSpan.GetTotalDays()+1; i++)
		{
			if (!pomGrid->SetValueRange(CGXRange(i+1,1),olCurrent.Format("%d.%m.%Y")))
				return FALSE;

			if (CedaDataHelper::IsWeekendOrHoliday(olCurrent, polParent->pomIsTabViewer->omHolidayKeyMap, lmStfUrno))
			{
				pomGrid->SetStyleRange(CGXRange(i+1,1),CGXStyle().SetInterior(DT_SILVERRED));
			}
			else
			{
				pomGrid->SetStyleRange(CGXRange(i+1,1),CGXStyle().SetInterior(DT_LIGHTSILVER1));
			}

			CString olCurrentDate = olCurrent.Format("%Y%m%d");

			const char *polLevels = "U2345";
			for (int j = 0; j < 5; j++)
			{
				CString olLevel = polLevels[j];
				DRRDATA *prlDrr = ogDrrData.GetDrrByKey(olCurrentDate,this->lmStfUrno,"1",olLevel);
				if (prlDrr)
				{
					TRACE("prlDrr found for level %s at %s\n",olLevel,olCurrentDate); 
					if (prlDrr->Scod == this->omCode)
					{
						pomGrid->SetValueRange(CGXRange(i+1,j+2),this->omCode);	
//						pomGrid->SetStyleRange(CGXRange(i+1,j+2),CGXStyle().SetItemDataPtr(prlDrr));

						// Urlaubsstufe ?
						if (strcmp(prlDrr->Rosl,"U") == 0)
						{
							if (this->bmUpdateVacancyDRR)
							{
#ifdef	DAOGRID_USE_ENABLED
								pomGrid->SetStyleRange(CGXRange(i+1,j+2),CGXStyle().SetEnabled(TRUE));
#endif
								pomGrid->SetStyleRange(CGXRange(i+1,j+2),CGXStyle().SetItemDataPtr((void *)prlDrr->Urno));
							}
							else
							{
								pomGrid->SetStyleRange(CGXRange(i+1,j+2),CGXStyle().SetInterior(DT_LIGHTSILVER2));
							}
						}

						// Aktuelle Stufe ?
						if (strcmp(prlDrr->Ross,"A") == 0)
						{
							if (this->bmUpdateActiveDRR)
							{
#ifdef	DAOGRID_USE_ENABLED
								pomGrid->SetStyleRange(CGXRange(i+1,j+2),CGXStyle().SetEnabled(TRUE));
#endif
								pomGrid->SetStyleRange(CGXRange(i+1,j+2),CGXStyle().SetItemDataPtr((void *)prlDrr->Urno));
							}
							else
							{
								pomGrid->SetStyleRange(CGXRange(i+1,j+2),CGXStyle().SetInterior(DT_LIGHTSILVER2));
							}

							for (int k = j + 1; k < 5; k++)
							{
								pomGrid->SetStyleRange(CGXRange(i+1,k+2),CGXStyle().SetInterior(DT_LIGHTYELLOW1));
							}
						}
					}
					else	// check on "OFF" shifts
					{
						int	 ilTbsd;
						bool blWork;
						int ilCodeType = ogOdaData.IsODAAndIsRegularFree(prlDrr->Scod,&ilTbsd,&blWork);
						if (ilCodeType == CODE_IS_ODA_REGULARFREE)
						{
							pomGrid->SetValueRange(CGXRange(i+1,j+2),prlDrr->Scod);	
//							pomGrid->SetStyleRange(CGXRange(i+1,j+2),CGXStyle().SetItemDataPtr(prlDrr));

							// Urlaubsstufe ?
							if (strcmp(prlDrr->Rosl,"U") == 0)
							{
								if (this->bmUpdateVacancyDRR)
								{
#ifdef	DAOGRID_USE_ENABLED
									pomGrid->SetStyleRange(CGXRange(i+1,j+2),CGXStyle().SetEnabled(TRUE));
#endif
									pomGrid->SetStyleRange(CGXRange(i+1,j+2),CGXStyle().SetItemDataPtr((void *)prlDrr->Urno));
								}
								else
								{
									pomGrid->SetStyleRange(CGXRange(i+1,j+2),CGXStyle().SetInterior(DT_LIGHTSILVER2));
								}
							}

							// Aktuelle Stufe ?
							if (strcmp(prlDrr->Ross,"A") == 0)
							{
								if (this->bmUpdateActiveDRR)
								{
#ifdef	DAOGRID_USE_ENABLED
									pomGrid->SetStyleRange(CGXRange(i+1,j+2),CGXStyle().SetEnabled(TRUE));
#endif
									pomGrid->SetStyleRange(CGXRange(i+1,j+2),CGXStyle().SetItemDataPtr((void *)prlDrr->Urno));
								}
								else
								{
									pomGrid->SetStyleRange(CGXRange(i+1,j+2),CGXStyle().SetInterior(DT_LIGHTSILVER2));
								}

								for (int k = j + 1; k < 5; k++)
								{
									pomGrid->SetStyleRange(CGXRange(i+1,k+2),CGXStyle().SetInterior(DT_LIGHTYELLOW1));
								}
							}
						}
					}
				}
			}

			olCurrent += COleDateTimeSpan(1,0,0,0);
		}

		// Rows auff�llen, damit die "optik stimmt"
		for (i; i < DAOGRID_MINROWS;i++)
		{
			// Datenfelder f�llen
			pomGrid->SetValueRange(CGXRange(i+1,1),"");
			pomGrid->SetValueRange(CGXRange(i+1,2),"");
			pomGrid->SetValueRange(CGXRange(i+1,3),"");
			pomGrid->SetValueRange(CGXRange(i+1,4),"");
			pomGrid->SetValueRange(CGXRange(i+1,5),"");
			pomGrid->SetValueRange(CGXRange(i+1,6),"");
		}

		pomGrid->LockUpdate(FALSE);
		pomGrid->GetParam()->SetLockReadOnly(true);
		pomGrid->Redraw();
		return TRUE;
	CCS_CATCH_ALL;
	return FALSE;
}

void DutyAbsenceOverviewDlg::OnClose() 
{
	// TODO: Add your message handler code here and/or call default
	DutyPlanPs *polParent = DYNAMIC_DOWNCAST(DutyPlanPs,this->GetParent());
	if (polParent == NULL || polParent->pomParent == NULL)
		return;

	COleDateTime	olLoadDateFrom	= polParent->pomParent->rmViewInfo.oLoadDateFrom;
	COleDateTime	olLoadDateTo	= polParent->pomParent->rmViewInfo.oLoadDateTo;
	COleDateTime	olDeleteFrom	= this->omTimeFrom;
	COleDateTime	olDeleteTo		= this->omTimeTo;
	COleDateTime	olDummyDateFrom;
	COleDateTime	olDummyDateTo;

	int ilCheckVal = CedaDataHelper::GetMaxPeriod(olLoadDateFrom,olLoadDateTo,olDeleteFrom,olDeleteTo,&olDummyDateFrom,&olDummyDateTo);
	if ((ilCheckVal != CHECK_P2_INNER_P1) && (ilCheckVal != CHECK_INVALID_DATE))
	{
		CString olSQL;	
		bool blLoadFailed = false;
	

		// SQL-Statement f�r DRR/DRS/DRA/DRD (benutzen alle dasselbe)
		olSQL.Format("WHERE STFU='%ld' AND SDAY BETWEEN '%s' AND '%s'",lmStfUrno,olLoadDateFrom.Format("%Y%m%d"),olLoadDateTo.Format("%Y%m%d"));
	
		// DRR-Daten nachladen
		if(ogDrrData.Read(olSQL.GetBuffer(0),NULL,true,false) == false)
		{
			ogDrrData.omLastErrorMessage.MakeLower();
			if(ogDrrData.omLastErrorMessage.Find("no data found") == -1)
			{
				CString olErrorTxt;
				olErrorTxt.Format("%s %d\n%s",LoadStg(ST_READERR), ogDrrData.imLastReturnCode, ogDrrData.omLastErrorMessage);
				MessageBox(olErrorTxt, LoadStg(ST_FEHLER), MB_ICONEXCLAMATION);
				blLoadFailed = true;
			}
		}
		olSQL.ReleaseBuffer();
	}
	
	CDialog::OnClose();
}

void DutyAbsenceOverviewDlg::OnOK() 
{
	// TODO: Add extra validation here
	OnApply();
	
	CDialog::OnOK();
}

void DutyAbsenceOverviewDlg::OnCancel() 
{
	// TODO: Add extra cleanup here
	
	CDialog::OnCancel();
}

void DutyAbsenceOverviewDlg::OnApply() 
{
	// TODO: Add your control notification handler code here
	CGXRangeList selList;
	if (!pomGrid->CopyRangeList(selList, TRUE))
	   return;

	pomGrid->GetParam()->SetLockReadOnly(false);

	POSITION pos = selList.GetHeadPosition( );
	while (pos)
	{
		CGXRange olRange = selList.GetNext(pos);
		ROWCOL ilRow,ilCol;
		BOOL blOK = olRange.GetFirstCell(ilRow,ilCol);
		while (blOK)
		{	
			CGXStyle olStyle;
			pomGrid->ComposeStyleRowCol(ilRow,ilCol,&olStyle);
			if (olStyle.GetIncludeItemDataPtr())
			{
				long ilUrno = reinterpret_cast<long>(olStyle.GetItemDataPtr());
				if (ilUrno)
				{
					DRRDATA *prlDrr = ogDrrData.GetDrrByUrno(ilUrno);
					ASSERT(prlDrr);
					ogDrrData.ClearDrr(prlDrr,TRUE,FALSE);
				}
			}

			blOK = olRange.GetNextCell(ilRow,ilCol);
		}
	}   	

	pomGrid->GetParam()->SetLockReadOnly(true);
}

void DutyAbsenceOverviewDlg::OnSelectall() 
{
	// TODO: Add your control notification handler code here
#if	0
	pomGrid->SelectRange(CGXRange().SetTable(),TRUE,TRUE);
#else
	// betreffende Zellen suchen
	pomGrid->GetParam()->SetLockReadOnly(false);

	CGXStyle olStyle;
	for (int ilRow = 1; ilRow < pomGrid->GetRowCount();ilRow++)
	{
		for (int ilCol = 2; ilCol < pomGrid->GetColCount(); ilCol++)
		{
			pomGrid->ComposeStyleRowCol(ilRow,ilCol, &olStyle );

			if (!olStyle.GetIncludeItemDataPtr() || !olStyle.GetItemDataPtr())
				continue;
			pomGrid->SelectRange(CGXRange(ilRow,ilCol),TRUE,TRUE);
		}
	}
	pomGrid->GetParam()->SetLockReadOnly(true);
#endif
}

void DutyAbsenceOverviewDlg::OnUnselectall() 
{
	// TODO: Add your control notification handler code here
	pomGrid->SelectRange(CGXRange().SetTable(),FALSE,TRUE);
	
}


void DutyAbsenceOverviewDlg::OnNewDrr(DRRDATA *popDrr)
{
	ASSERT(popDrr);
	ASSERT(popDrr);

	bool blNewDrr = false;
	if (popDrr->Scod == this->omCode)
		blNewDrr = true;

	int	 ilTbsd;
	bool blWork;
	int ilCodeType = ogOdaData.IsODAAndIsRegularFree(popDrr->Scod,&ilTbsd,&blWork);
	if (ilCodeType == CODE_IS_ODA_REGULARFREE)
		blNewDrr = true;

	if (!blNewDrr)
		return;

	COleDateTime olCurrent;
	CedaDataHelper::DateStringToOleDateTime(popDrr->Sday,olCurrent);

	COleDateTimeSpan olTimeSpan = olCurrent - this->omTimeFrom;
	ROWCOL ilRow = olTimeSpan.GetTotalDays() + 1;
	
	pomGrid->GetParam()->SetLockReadOnly(false);

	CString olCurrentDate = popDrr->Sday;

	const char *polLevels = "U2345";
	for (int j = 0; j < 5; j++)
	{
		CString olLevel = polLevels[j];
		pomGrid->SetValueRange(CGXRange(ilRow,j+2),popDrr->Scod);	
//		pomGrid->SetStyleRange(CGXRange(ilRow,j+2),CGXStyle().SetItemDataPtr(prlDrr));

		// Urlaubsstufe ?
		if (strcmp(popDrr->Rosl,"U") == 0)
		{
			if (this->bmUpdateVacancyDRR)
			{
#ifdef	DAOGRID_USE_ENABLED
				pomGrid->SetStyleRange(CGXRange(ilRow,j+2),CGXStyle().SetEnabled(TRUE));
#endif
				pomGrid->SetStyleRange(CGXRange(ilRow,j+2),CGXStyle().SetItemDataPtr((void *)popDrr->Urno));
			}
			else
			{
				pomGrid->SetStyleRange(CGXRange(ilRow,j+2),CGXStyle().SetInterior(DT_LIGHTSILVER2));
			}
		}

		// Aktuelle Stufe ?
		if (strcmp(popDrr->Ross,"A") == 0)
		{
			if (this->bmUpdateActiveDRR)
			{
#ifdef	DAOGRID_USE_ENABLED
				pomGrid->SetStyleRange(CGXRange(ilRow,j+2),CGXStyle().SetEnabled(TRUE));
#endif
				pomGrid->SetStyleRange(CGXRange(ilRow,j+2),CGXStyle().SetItemDataPtr((void *)popDrr->Urno));
			}
			else
			{
				pomGrid->SetStyleRange(CGXRange(ilRow,j+2),CGXStyle().SetInterior(DT_LIGHTSILVER2));
			}

			for (int k = j + 1; k < 5; k++)
			{
				pomGrid->SetStyleRange(CGXRange(ilRow,k+2),CGXStyle().SetInterior(DT_LIGHTYELLOW1));
			}
		}

		pomGrid->RedrawRowCol(ilRow,j+2);
	}

	pomGrid->GetParam()->SetLockReadOnly(true);
	
}

void DutyAbsenceOverviewDlg::OnChangeDrr(DRRDATA *popDrr)
{
	ASSERT(popDrr);
	ROWCOL ilRow,ilCol;
	if (FindCellByUrnoDataPtr(popDrr->Urno,ilRow,ilCol))
	{
		// Pr�fen, ob Datensatz als gel�scht markiert worden ist
		if (popDrr->Scod != this->omCode)
		{
			pomGrid->GetParam()->SetLockReadOnly(false);
			pomGrid->SetValue(ilRow,ilCol,CString(""));
#ifdef	DAOGRID_USE_ENABLED
			pomGrid->SetStyleRange(CGXRange(ilRow,ilCol),CGXStyle().SetEnabled(FALSE));
#endif
			pomGrid->SetStyleRange(CGXRange(ilRow,ilCol),CGXStyle().SetIncludeItemDataPtr(FALSE));
			pomGrid->SetStyleRange(CGXRange(ilRow,ilCol),CGXStyle().SetItemDataPtr(NULL));
			pomGrid->SelectRange(CGXRange(ilRow,ilCol),FALSE,FALSE);
			pomGrid->RedrawRowCol(ilRow,ilCol);
			pomGrid->GetParam()->SetLockReadOnly(true);
		}
	}
	
}


BOOL DutyAbsenceOverviewDlg::FindCellByUrnoDataPtr(long lpUrno,ROWCOL& ipRow,ROWCOL& ipCol)
{
	// betreffende Zelle suchen
	CGXStyle olStyle;
	for (int ilRow = 1; ilRow < pomGrid->GetRowCount();ilRow++)
	{
		for (int ilCol = 2; ilCol < pomGrid->GetColCount(); ilCol++)
		{
			pomGrid->ComposeStyleRowCol(ilRow,ilCol, &olStyle );

			if (!olStyle.GetIncludeItemDataPtr())
				continue;

			if (lpUrno == reinterpret_cast<long>(olStyle.GetItemDataPtr()))
			{
				ipRow = ilRow;
				ipCol = ilCol;
				return TRUE;
			}
		}
	}
	
	return FALSE;
}

void DutyAbsenceOverviewDlg::OnRBtnSelect()
{
	ROWCOL ilRow,ilCol;
	if (GetCurrentCell(&ilRow,&ilCol))
	{
		pomGrid->SelectRange(CGXRange(ilRow,ilCol),TRUE,TRUE);
	}
}

void DutyAbsenceOverviewDlg::OnRBtnUnselect()
{
	ROWCOL ilRow,ilCol;
	if (GetCurrentCell(&ilRow,&ilCol))
	{
		pomGrid->SelectRange(CGXRange(ilRow,ilCol),FALSE,TRUE);
	}
}

void DutyAbsenceOverviewDlg::OnRBtnDelete()
{
	ROWCOL ilRow,ilCol;
	if (GetCurrentCell(&ilRow,&ilCol))
	{
		pomGrid->GetParam()->SetLockReadOnly(false);

		CGXStyle olStyle;
		pomGrid->ComposeStyleRowCol(ilRow,ilCol,&olStyle);
		if (olStyle.GetIncludeItemDataPtr())
		{
			long ilUrno = reinterpret_cast<long>(olStyle.GetItemDataPtr());
			if (ilUrno)
			{
				DRRDATA *prlDrr = ogDrrData.GetDrrByUrno(ilUrno);
				ASSERT(prlDrr);
				ogDrrData.ClearDrr(prlDrr,TRUE,FALSE);
			}
		}

		pomGrid->GetParam()->SetLockReadOnly(true);
	}	
}

void DutyAbsenceOverviewDlg::OnRBtnApply()
{
	ROWCOL ilRow,ilCol;
	if (GetCurrentCell(&ilRow,&ilCol))
	{
		pomGrid->SelectRange(CGXRange(ilRow,ilCol),TRUE,TRUE);
		OnApply();
	}
}


BOOL DutyAbsenceOverviewDlg::GetCurrentCell(ROWCOL *pipRow,ROWCOL *pipCol)
{
	CPoint pt;
    ::GetCursorPos(&pt);
	this->ScreenToClient(&pt);

	if (pomGrid->GetCurrentCell(pipRow,pipCol))
		return TRUE;
	else
	{
		int ilCode = pomGrid->HitTest(pt,pipRow,pipCol,NULL);
		if (ilCode == GX_CELLHIT)
		{
			return TRUE;
		}
	}

	return FALSE;
}


long DutyAbsenceOverviewDlg::OnLBtnDblClk(WPARAM wParam,LPARAM lParam)
{
	GRIDNOTIFY *prlNotify = (GRIDNOTIFY *)lParam;
	if (prlNotify == NULL)
		return 0;

	pomGrid->GetParam()->SetLockReadOnly(false);

	CGXStyle olStyle;
	pomGrid->ComposeStyleRowCol(prlNotify->row,prlNotify->col,&olStyle);
	if (olStyle.GetIncludeItemDataPtr())
	{
		long ilUrno = reinterpret_cast<long>(olStyle.GetItemDataPtr());
		if (ilUrno)
		{
			DRRDATA *prlDrr = ogDrrData.GetDrrByUrno(ilUrno);
			ASSERT(prlDrr);
			ogDrrData.ClearDrr(prlDrr,TRUE,FALSE);
		}
	}

	pomGrid->GetParam()->SetLockReadOnly(true);

	return 0;
}