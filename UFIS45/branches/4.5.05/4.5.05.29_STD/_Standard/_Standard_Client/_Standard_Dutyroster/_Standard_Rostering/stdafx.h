// stdafx.h : include file for standard system include files,
//  or project specific include files that are used frequently, but
//      are changed infrequently
//

#if !defined(AFX_STDAFX_H__F66E6B66_5BB3_11D3_8EF6_00001C034EA0__INCLUDED_)
#define AFX_STDAFX_H__F66E6B66_5BB3_11D3_8EF6_00001C034EA0__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

//#define VC_EXTRALEAN		// Exclude rarely-used stuff from Windows headers

#include <afxwin.h>         // MFC core and standard components
#include <afxext.h>         // MFC extensions
#include <afxdisp.h>        // MFC Automation classes
#include <afxdtctl.h>		// MFC support for Internet Explorer 4 Common Controls
#include <gxall.h>			// Stingray Grid
#include <afxmt.h>			// MFC Multithreaded Extensions (Syncronization Objects)
//#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>			// MFC support for Windows Common Controls
//#endif // _AFX_NO_AFXCMN_SUPPORT
#include <afxcoll.h>
#include <algorithm>
#include <fstream.h>
#include <iomanip.h>
#include <iostream.h>
#include <math.h>
#include <process.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/timeb.h>
#include <time.h>
#include <winreg.h>
#include <afxsock.h>		// MFC socket extensions
#include <afxdtctl.h>		// MFC date / time controls

/* Disable ridiculous warnings so that the code */
/* compiles cleanly using warning level 4.      */

/* nonstandard extension 'single line comment' was used */
#pragma warning(disable: 4001)

// nonstandard extension used : nameless struct/union
#pragma warning(disable: 4201)

// nonstandard extension used : bit field types other than int
#pragma warning(disable: 4214)

// Note: Creating precompiled header 
#pragma warning(disable: 4699)

// unreferenced inline function has been removed
#pragma warning(disable: 4514)

// unreferenced formal parameter
#pragma warning(disable: 4100)

// indirection to slightly different base types
//#pragma warning(disable: 4057)

// named type definition in parentheses
#pragma warning(disable: 4115)

// warning C4663: C++ language change
#pragma warning(disable: 4663)

// warning C4018: '<' : signed/unsigned mismatch
#pragma warning(disable: 4018)

// warning C4097: typedef-name 'alloc_inst_type' used as synonym for class-name
#pragma warning(disable: 4097)

// warning C4244: 'argument' : conversion from 
#pragma warning(disable: 4244)

// warning C4245: 'return' : conversion from
#pragma warning(disable: 4245)

// warning C4239: nonstandard extension used : 'argument' : conversion
#pragma warning(disable: 4239)


///////////////////////////// STRICT Build Option /////////////////////////////


// Force all EXEs/DLLs to use STRICT type checking.
#ifndef STRICT
#define STRICT
#endif


// Defines f�r abweichende �nderungen
#ifdef _DEBUG
//#define ACCOUNTS_SPEC_SORTING

//#define MANUAL_RELEASE
// alle gelesenen Daten loggen (nur im FULL-Modus)
#define TRACE_FULL_DATA


#endif _DEBUG


#include <ccsddx.h>
#include <CCSbar.h>
#include <CCSBasic.h>
#include <CCSBcHandle.h>
#include <CCSCedaCom.h>
#include <CCSCedaData.h>
#include <CCSClientWnd.h>
#include <CCSDragDropCtrl.h>
#include <CCSEdit.h>
#include <CCSLog.h>
#include <ccsptrarray.h>
#include <CCSTIME.H>
#include <CCSButtonctrl.h>
#include <CedaBasicData.h>
#include <CedaSystabData.h>
#include <UFIS.h>
#include <NameConfigurationDlg.h>

#include <CCSParam.h>
#include <CCSGlobl.h>
#include <CCSDynTable.h>
#include <CedaData.h>
#include <Accounts.h>
#include <BasePropertySheet.h>
#include <BasicData.h>
#include <BSD_Frame.h>
#include <BSD_View.h>
#include <CedaAccData.h>
#include <cedabsddata.h>
#include <CedaCfgData.h>
#include <CedaCotData.h> // Arbeitsvertragsarten
#include <CedaDataHelper.h> // f�r Abweichungs-Check
#include <CedaDelData.h>
#include <cedadradata.h>
#include <CedaDrdData.h> // Abweichungen
#include <CedaDrgData.h> // Tagesdienstplan Arbeitsgruppen
#include <CedadrrData.h>
#include <CedaDrsData.h> // Erg�nzungen
#include <CedaInitModuData.h>
#include <CedamsdData.h>
#include <CedaOacData.h>
#include <CedaOdaData.h> // Basisabwesenheiten
#include <CedaOrgData.h> // Organisationen
#include <cedaperdata.h>
#include <CedaPfcData.h> // Funktionen
#include <CedaPgpData.h>
#include <CedaRelData.h>
#include <CedaScoData.h> // Zuordnungen Arbeitsvertr�ge-Mitarbeiter
#include <CedaSdgData.h>
#include <CedaSdtData.h>
#include <CedaSgmData.h>
#include <CedaSgrData.h>
#include <CedaSorData.h> // Allocate Organisationseinheiten
#include <CedaSpeData.h>
#include <CedaSpfData.h> 
#include <CedaSpfData.h> // Zuordnungen Funktionen-Mitarbeiter
#include <CedaSprData.h>
#include <CedaSreData.h> // Regelm�ssigkeitszuordnungen Mitarbeiter
#include <CedaSteData.h> // Zuordnungen Organisation-Mitarbeiter
#include <CedaStfData.h> // Mitarbeiter
#include <CedaSwgData.h> // f�r Abweichungs-Check
#include <CedaSwgData.h> // Zuordnungen Arbeitsgruppen-Mitarbeiter
#include <CedaTeaData.h> // Fahrgemeinschaften
#include <CedaWgpData.h> // Arbeitsgruppen
#include <cedawisdata.h>
#include <CedaWorkTime.h>
#include <ChangeShiftDlg.h>
#include <CharacteristicValueDlg.h>
#include <ChildFrm.h>
#include <CodeBigDlg.h>
#include <CodeDlg.h>
#include <ColorControls.h>
#include <combobar.h>
#include <CommonGridDlg.h>
#include <cviewer.h>
#include <cviewer.h> 
#include <DoubleTourDlg.h>
#include <DraDialog.h>
#include <DrdDialog.h>
#include <DutyAccountDlg.h>
#include <DutyAttentionDlg.h>
#include <DutyBsdDlg.h>
#include <DutyChangeGroupDlg.h>
#include <DutyDebitTabViewer.h>
#include <DutyDragDropDlg.h>
#include <DutyDragDropTabViewer.h>
#include <dutyistabviewer.h>
#include <DutyPlanAbsencePp.h>
#include <DutyPlanPs.h>
#include <dutyprintdlg.h>
#include <DutyReleaseDlg.h>
#include <DutyRoster_Frm.h>
#include <DutyRoster_View.h>
#include <DutyRosterPropertySheet.h>
#include <DutyShowJobsDlg.h>
#include <DutySolidTabViewer.h>
#include <DutyStatTabViewer.h>
#include <EmplDlg.h>
#include <ExcelExport.h>
#include <ExcelExportDlg.h>
#include <fstream.h>
#include <GridControl.h>
#include <GridEmplDlg.h>
#include <Groups.h>
#include <GUILng.h>
#include <GUILng.h> 
#include <ImportShifts.h>
#include <InfoBoxDlg.h>
#include <InfoDlg.h>
#include <InitialLoadDlg.h>
#include <iomanip.h>
#include <iostream.h>
#include <ListBoxDlg.h>
#include <LoginDlg.h>
#include <MainFrm.h >
#include <mainfrm.h>
#include <PfcSelectDlg.h>
#include <Plan_Info.h>
#include <PrivList.h>
#include <PSDutyRoster1ViewPage.h>
#include <PSDutyRoster2ViewPage.h>
#include <PSDutyRoster3ViewPage.h>
#include <PSDutyRoster4ViewPage.h>
#include <PSDutyRoster5ViewPage.h>
#include <PSShiftRoster1ViewPage.h>
#include <PSShiftRoster2ViewPage.h>
#include <PSShiftRoster3ViewPage.h>
#include <RegisterDlg.h>
#include <resource.h>
#include <rostering.h>
#include <RosteringDoc.h>
#include <RosteringView.h>
#include <RosterViewPage.h>
#include <SelectShiftDlg.h>
#include <SendToSapDlg.h>
#include <SetupDlg.h>
#include <ShiftAWDlg.h>
#include <ShiftCheck.h>
#include <ShiftCodeNumberDlg.h>
#include <ShiftKonsistDlg.h>
#include <ShiftNewGSPDlg.h>
#include <ShiftPlanDlg.h>
#include <ShiftReleaseDlg.h>
#include <ShiftRoster_Frm.h>
#include <ShiftRoster_View.h>
#include <ShiftRosterPropertySheet.h>
#include <ShiftPrintDlg.h>
#include <stdafx.h>
#include <StringConst.h>
#include <table.h>
#include <TageslisteDlg.h>
#include <UDlgStatusBar.h>
#include <VersionInfo.h>
#include <WaitDlg.h>
#include <WINSPOOL.H>
#include <WishDialog.h>
#include <XStatusBar.h>
#include <SelectDateDlg.h>


//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_STDAFX_H__F66E6B66_5BB3_11D3_8EF6_00001C034EA0__INCLUDED_)
