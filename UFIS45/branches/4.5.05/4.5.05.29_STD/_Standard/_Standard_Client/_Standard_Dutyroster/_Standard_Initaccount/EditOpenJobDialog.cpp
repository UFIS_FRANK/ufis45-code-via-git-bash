// EditOpenJobDialog.cpp: Implementierungsdatei
//

#include <stdafx.h>
#include <initializeaccount.h>
#include <EditOpenJobDialog.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CEditOpenJobDialog 


CEditOpenJobDialog::CEditOpenJobDialog(CString opInfo, CWnd* pParent /*=NULL*/, bool bpEditMode /* = false*/)
	: CDialog(CEditOpenJobDialog::IDD, pParent)
{
	//{{AFX_DATA_INIT(CEditOpenJobDialog)
	//}}AFX_DATA_INIT
	bmEditMode = bpEditMode;
	omInfo = opInfo;
}


void CEditOpenJobDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CEditOpenJobDialog)
	DDX_Control(pDX, IDC_COMBO_MODE, m_cb_Mode);
	DDX_Control(pDX, IDOK, m_but_Ok);
	DDX_Control(pDX, IDC_STATIC_TO, m_stc_To);
	DDX_Control(pDX, IDC_STATIC_TIME, m_stc_Time);
	DDX_Control(pDX, IDC_STATIC_OFFSET, m_stc_Offset);
	DDX_Control(pDX, IDC_STATIC_NEXT_EXEC, m_stc_NextExec);
	DDX_Control(pDX, IDC_STATIC_MODE, m_stc_Mode);
	DDX_Control(pDX, IDC_STATIC_FROM, m_stc_From);
	DDX_Control(pDX, IDC_STATIC_DAY, m_stc_Day);
	DDX_Control(pDX, IDC_EDIT_TO, m_ed_To);
	DDX_Control(pDX, IDC_EDIT_TIME, m_ed_Time);
	DDX_Control(pDX, IDC_EDIT_OFFSET, m_ed_Offset);
	DDX_Control(pDX, IDC_EDIT_NEXT_EXEC, m_ed_NextExec);
	DDX_Control(pDX, IDC_EDIT_FROM, m_ed_From);
	DDX_Control(pDX, IDC_EDIT_DAY, m_ed_Day);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CEditOpenJobDialog, CDialog)
	//{{AFX_MSG_MAP(CEditOpenJobDialog)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten CEditOpenJobDialog 

BOOL CEditOpenJobDialog::OnInitDialog() 
{
	CString olError, olParam, olInfoFormat, olFormattedParam;
	int ilMode;

	CDialog::OnInitDialog();
	
	olInfoFormat = pcgInfoStringFormat;
	// analyze info string: get parameters, check parameters and set edit field if parameter is ok
	if (omInfo.GetLength() != olInfoFormat.GetLength())
	{
		if (omInfo.GetLength() > olInfoFormat.GetLength())
		{
			omInfo = omInfo.Left(olInfoFormat.GetLength());
		}
		else
		{
			olError.Format(LoadStg(IDS_STRING1934),pcgInfoStringFormat);
			MessageBox(olError,LoadStg(IDS_STRING1935), MB_ICONSTOP|MB_OK);
			EndDialog(-1);
			return true;
		}
	}
	
	// get FROM from info string
	olParam = omInfo.Mid(0,8);
	if (olParam.SpanIncluding("1234567890") != olParam)
	{
		olError.Format(LoadStg(IDS_STRING1936),"FROM",olInfoFormat.Mid(0,8));
		MessageBox(olError,LoadStg(IDS_STRING1935), MB_ICONSTOP|MB_OK);
		EndDialog(-1);
		return true;
	}
	olFormattedParam.Format("%s.%s.%s",olParam.Right(2),olParam.Mid(4,2),olParam.Left(4));
	m_ed_From.SetWindowText(olFormattedParam);
	
	// get TO from info string
	olParam = omInfo.Mid(9,8);
	if (olParam.SpanIncluding("1234567890") != olParam)
	{
		olError.Format(LoadStg(IDS_STRING1936),"TO",olInfoFormat.Mid(9,8));
		MessageBox(olError,LoadStg(IDS_STRING1935), MB_ICONSTOP|MB_OK);
		EndDialog(-1);
		return true;
	}
	olFormattedParam.Format("%s.%s.%s",olParam.Right(2),olParam.Mid(4,2),olParam.Left(4));
	m_ed_To.SetWindowText(olFormattedParam);

	// get MODE from info string
	olParam = omInfo.Mid(18,1);
	if (olParam.SpanIncluding("123") != olParam)
	{
		olError.Format(LoadStg(IDS_STRING1936),"MODE",olInfoFormat.Mid(18,1));
		MessageBox(olError,LoadStg(IDS_STRING1935), MB_ICONSTOP|MB_OK);
		EndDialog(-1);
		return true;
	}
	ilMode = atoi(LPCTSTR(olParam));
	m_cb_Mode.InsertString(0,LoadStg(IDS_STRING1940));
	m_cb_Mode.InsertString(1,LoadStg(IDS_STRING1941));
	m_cb_Mode.InsertString(2,LoadStg(IDS_STRING1942));
	m_cb_Mode.SetCurSel(ilMode-1);

	// get TIME from info string
	olParam = omInfo.Mid(20,4);
	if (olParam.SpanIncluding("1234567890") != olParam)
	{
		olError.Format(LoadStg(IDS_STRING1936),"TIME",olInfoFormat.Mid(20,4));
		MessageBox(olError,LoadStg(IDS_STRING1935), MB_ICONSTOP|MB_OK);
		EndDialog(-1);
		return true;
	}
	olFormattedParam.Format("%s:%s",olParam.Left(2),olParam.Right(2));
	m_ed_Time.SetWindowText(olFormattedParam);
	
	// get DAY from info string
	olParam = omInfo.Mid(25,2);
	if (olParam.SpanIncluding("1234567890") != olParam)
	{
		olError.Format(LoadStg(IDS_STRING1936),"DAY",olInfoFormat.Mid(25,2));
		MessageBox(olError,LoadStg(IDS_STRING1935), MB_ICONSTOP|MB_OK);
		EndDialog(-1);
		return true;
	}
	if (atoi(LPCTSTR(olParam)) > 28)
		m_ed_Day.SetWindowText(LoadStg(IDS_STRING1903));
	else
		m_ed_Day.SetWindowText(olParam);

	// get OFFSET from info string
	olParam = omInfo.Mid(28,3);
	if ((olParam.Right(2).SpanIncluding("1234567890") != olParam.Right(2)) ||
		(olParam.Left(1).SpanExcluding("+-") != ""))
	{
		olError.Format(LoadStg(IDS_STRING1936),"OFFSET",olInfoFormat.Mid(28,3));
		MessageBox(olError,LoadStg(IDS_STRING1935), MB_ICONSTOP|MB_OK);
		EndDialog(-1);
		return true;
	}
	m_ed_Offset.SetWindowText(olParam);
	
	// get FIRSTEXEC from info string
	olParam = omInfo.Mid(32,12);
	if (olParam.SpanIncluding("1234567890") != olParam)
	{
		olError.Format(LoadStg(IDS_STRING1936),"FIRSTEXEC",olInfoFormat.Mid(32,12));
		MessageBox(olError,LoadStg(IDS_STRING1935), MB_ICONSTOP|MB_OK);
		EndDialog(-1);
		return true;
	}
	olFormattedParam.Format("%s.%s.%s, %s:%s",olParam.Mid(6,2),olParam.Mid(4,2),olParam.Left(4),olParam.Mid(8,2),olParam.Mid(10,2));
	m_ed_NextExec.SetWindowText(olFormattedParam);

	LoadStringResources();

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX-Eigenschaftenseiten sollten FALSE zur�ckgeben
}

//**********************************************************************************
// LoadStringResources: load language specific resources.
// Input:	none
// Return:	none
//**********************************************************************************

void CEditOpenJobDialog::LoadStringResources()
{
CCS_TRY
	if (bmEditMode)
		SetWindowText(LoadStg(IDS_STRING1926));
	else
		SetWindowText(LoadStg(IDS_STRING1925));
	m_but_Ok.SetWindowText(LoadStg(IDS_STRING1912));
	m_stc_To.SetWindowText(LoadStg(IDS_STRING1928));
	m_stc_Time.SetWindowText(LoadStg(IDS_STRING1930));
	m_stc_Offset.SetWindowText(LoadStg(IDS_STRING1932));
	m_stc_NextExec.SetWindowText(LoadStg(IDS_STRING1933));
	m_stc_Mode.SetWindowText(LoadStg(IDS_STRING1929));
	m_stc_From.SetWindowText(LoadStg(IDS_STRING1927));
	m_stc_Day.SetWindowText(LoadStg(IDS_STRING1931));
	// disable edit fields if mode is view-only
	m_ed_To.SetReadOnly(!bmEditMode);
	m_ed_Time.SetReadOnly(!bmEditMode);
	m_ed_Offset.SetReadOnly(!bmEditMode);
	m_ed_NextExec.SetReadOnly(!bmEditMode);
	m_ed_From.SetReadOnly(!bmEditMode);
	m_ed_Day.SetReadOnly(!bmEditMode);
	m_cb_Mode.EnableWindow(bmEditMode);
CCS_CATCH_ALL
}

