// OpenJobControlDialog.cpp: Implementierungsdatei
//

#include <stdafx.h>
#include <initializeaccount.h>
#include <OpenJobControlDialog.h>
#include <GridControl.h>
#include <Ufis.h>
#include <EditOpenJobDialog.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define MAX_OPENJOB_TRIALS	1

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld COpenJobControlDialog 


COpenJobControlDialog::COpenJobControlDialog(CWnd* pParent /*=NULL*/)
	: CDialog(COpenJobControlDialog::IDD, pParent)
{
	//{{AFX_DATA_INIT(COpenJobControlDialog)
		// HINWEIS: Der Klassen-Assistent f�gt hier Elementinitialisierung ein
	//}}AFX_DATA_INIT
	pomOpenJobGrid = NULL;
	imNoOfOpenJobs = 0;
	bmInitOk = false;
}

COpenJobControlDialog::~COpenJobControlDialog()
{
CCS_TRY
	// delete grid
	if (pomOpenJobGrid != NULL) delete pomOpenJobGrid;
	pomOpenJobGrid = NULL;
CCS_CATCH_ALL
}

void COpenJobControlDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(COpenJobControlDialog)
	DDX_Control(pDX, IDC_BUTTON_VIEW_INFO, m_but_ViewInfo);
	DDX_Control(pDX, IDC_BUTTON_REFRESH, m_but_Refresh);
	DDX_Control(pDX, IDC_BUTTON_REMOVE_ALL, m_but_RemoveAll);
	DDX_Control(pDX, IDC_LIST_RESULTS, m_list_Results);
	DDX_Control(pDX, IDOK, m_but_OK);
	DDX_Control(pDX, IDC_BUTTON_DELETE_JOB, m_but_DeleteJob);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(COpenJobControlDialog, CDialog)
	//{{AFX_MSG_MAP(COpenJobControlDialog)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_BUTTON_REFRESH, OnButtonRefresh)
	ON_BN_CLICKED(IDC_BUTTON_REMOVE_ALL, OnButtonRemoveAll)
	ON_BN_CLICKED(IDC_BUTTON_DELETE_JOB, OnButtonDeleteJob)
	ON_BN_CLICKED(IDC_BUTTON_VIEW_INFO, OnButtonViewInfo)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten COpenJobControlDialog 

BOOL COpenJobControlDialog::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Zus�tzliche Initialisierung hier einf�gen
	// initialize the grid
	InitOpenJobGrid();

	// add column to list box
	CRect r;
	m_list_Results.GetClientRect(&r);
	/* size = complete client window size - size of vertical scroll bar, 
	   to avoid a horizontal scroll bar appears when the vertical shows up */
	m_list_Results.InsertColumn(0,"",LVCFMT_LEFT,r.right-20);

	// disable buttons until initialization is complete
	EnableButtons(false);

	// load language specific resources
	LoadStringResources();
	
	// now the Dialog is initialized
	bmInitOk = true;
	// set the timer
	SetTimer(1,500,NULL);

	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX-Eigenschaftenseiten sollten FALSE zur�ckgeben
}

//**********************************************************************************
// LoadStringResources: load language specific resources.
// Input:	none
// Return:	none
//**********************************************************************************

void COpenJobControlDialog::LoadStringResources()
{
CCS_TRY
	SetWindowText(LoadStg(IDS_STRING1910));
	m_but_DeleteJob.SetWindowText(LoadStg(IDS_STRING1911));
	m_but_OK.SetWindowText(LoadStg(IDS_STRING1912));
	m_but_RemoveAll.SetWindowText(LoadStg(IDS_STRING1919));
	m_but_Refresh.SetWindowText(LoadStg(IDS_STRING1920));
	m_but_ViewInfo.SetWindowText(LoadStg(IDS_STRING1943));
CCS_CATCH_ALL
}

//**********************************************************************************
// ClearGrid: remove all entries from the grid
// Input:	none
// Return:	none
//**********************************************************************************

void COpenJobControlDialog::ClearGrid()
{
CCS_TRY
	// enable update
	pomOpenJobGrid->GetParam()->SetLockReadOnly(false);

	// clear grid
	if (pomOpenJobGrid->GetRowCount() > 0) 
	{
		// remove all rows
		pomOpenJobGrid->RemoveRows(1,pomOpenJobGrid->GetRowCount());
	}

	// disable update
	pomOpenJobGrid->GetParam()->SetLockReadOnly(true);
	pomOpenJobGrid->Redraw();
CCS_CATCH_ALL
}

//**********************************************************************************
// InitOpenJobGrid: initializes the grid which shows all OpenJobs.
// Input:	none
// Return:	none
//**********************************************************************************

void COpenJobControlDialog::InitOpenJobGrid()
{
CCS_TRY
	// create the grid
	pomOpenJobGrid = new CGridControl (this, IDC_OPENJOB_GRID,JOB_COLCOUNT,imNoOfOpenJobs);
	pomOpenJobGrid->Initialize();

	// allways show vertical scroll bar 
	pomOpenJobGrid->SetScrollBarMode(SB_VERT,gxnAutomatic);
	
	// set the headlines
	pomOpenJobGrid->SetValueRange(CGXRange(0,1),LoadStg(IDS_STRING1913));

	// setup the column widths
	pomOpenJobGrid->SetColWidth ( 0, 0, 27);
	pomOpenJobGrid->SetColWidth ( 1, 1, 455);

	pomOpenJobGrid->GetParam()->EnableUndo(FALSE);
	pomOpenJobGrid->LockUpdate(TRUE);
	pomOpenJobGrid->LockUpdate(FALSE);
	pomOpenJobGrid->GetParam()->EnableUndo(TRUE);

	
	// don't allow the user to change column widths
	pomOpenJobGrid->GetParam()->EnableTrackColWidth(FALSE);
	// prevent single cell selection, only whole lines can be selected
	pomOpenJobGrid->GetParam()->EnableSelection(GX_SELROW | GX_SELMULTIPLE | GX_SELSHIFT);

	pomOpenJobGrid->SetStyleRange( CGXRange().SetTable(), CGXStyle()
				.SetVerticalAlignment(DT_VCENTER)
				.SetReadOnly(TRUE)
				);
	pomOpenJobGrid->EnableAutoGrow ( FALSE );
CCS_CATCH_ALL
}

//**********************************************************************************
// FillOpenJobGrid. Fill the grid with OpenJob data.
// Input:	none
// Return:	success?
//**********************************************************************************

bool COpenJobControlDialog::FillOpenJobGrid(bool bpClearMessageList /* = true */)
{
CCS_TRY
	CString olError;
	CString olData;
	CString olMsg;

	// disable user buttons
	EnableButtons(false);

	// clear message list
	if (bpClearMessageList) m_list_Results.DeleteAllItems();
	
	// reset OpenJob counter
	imNoOfOpenJobs = 0;

	// clear grid
	ClearGrid();

	// enable update
	pomOpenJobGrid->GetParam()->SetLockReadOnly(false);

	// set first job index to "gimme all"
	strcpy(omBc.cSelection,"-1");

	// scan basic dir
	do
	{
		if (SendBC("OJC","",omBc.cSelection,"","GJI"))
		{
			// check if there are open jobs at all
			if (CString(omBc.cTws) == "-0001")
			{
				// no open jobs, print message
				m_list_Results.InsertItem(m_list_Results.GetItemCount(),LoadStg(IDS_STRING1916));
				m_list_Results.UpdateWindow();
				ClearGrid();
				EnableButtons(true);
				return true;
			}
			// insert Row
			pomOpenJobGrid->InsertRows(imNoOfOpenJobs+1,1);
			// connect index in array with first cell of each line
			pomOpenJobGrid->SetStyleRange ( CGXRange(imNoOfOpenJobs+1,1), CGXStyle().SetItemDataPtr((void*)atoi(omBc.cTws)) );
			// insert file name
			pomOpenJobGrid->SetValueRange(CGXRange(imNoOfOpenJobs+1,1), omBc.cData);
			// one more element added
			imNoOfOpenJobs++;
		}
		else
		{	
			// error sending bc -> terminate
			ClearGrid();
			EnableButtons(true);
			return false; 
		}
	} while (atoi(omBc.cSelection) >= 0);

	// print number of open jobs to output
	olMsg.Format(LoadStg(IDS_STRING1915),imNoOfOpenJobs);
	m_list_Results.InsertItem(m_list_Results.GetItemCount(),olMsg);
	m_list_Results.UpdateWindow();

	// disable update
	pomOpenJobGrid->GetParam()->SetLockReadOnly(true);
	pomOpenJobGrid->Redraw();

	// enable user buttons
	EnableButtons(true);
	return true;

CCS_CATCH_ALL
return false;
}

//**********************************************************************************
// EnableButtons: enable or disable all user buttons (e.g. while sending bc)
// Input:	enable buttons?
// Return:	none
//**********************************************************************************

void COpenJobControlDialog::EnableButtons(bool bpEnable) 
{
CCS_TRY
	m_but_DeleteJob.EnableWindow(bpEnable);
	m_but_RemoveAll.EnableWindow(bpEnable);
	m_but_Refresh.EnableWindow(bpEnable);
CCS_CATCH_ALL
}

//*********************************************************************************************************************
// SendBC: send a broadcast.
// Input: CString <opData>: the data string
// Return:	TRUE	-> everything is ok
//			FALSE	-> error
//*********************************************************************************************************************

bool COpenJobControlDialog::SendBC(CString opAction, CString opData, CString opSelection, 
								   CString opFields, CString opTable, CString opTwStart, 
								   CString opTwEnd)
{
	int iLastReturnCode;
	CString strError, strMsg;

	/* initialize bc struct */
	strcpy(omBc.cUser,pcgUser);
	strcpy(omBc.cWks,"InitAcc");
	strcpy(omBc.cSortClause,"");
	strcpy(omBc.cDataDest,"RETURN");
	
	// check command length
	if ((opAction.GetLength() >= MAX_BC_TW_SIZE) ||
		(opData.GetLength() >= MAX_BC_DATA_SIZE) ||
		(opSelection.GetLength() >= MAX_BC_SELECTION_SIZE) ||
		(opFields.GetLength() >= MAX_BC_FIELDS_SIZE) ||
		(opTable.GetLength() >= MAX_BC_TABLE_SIZE) ||
		(opTwStart.GetLength() >= MAX_BC_TW_SIZE) ||
		(opTwEnd.GetLength() >= MAX_BC_TW_SIZE))
	{
		// command is too long -> do error message and terminate
		m_list_Results.InsertItem(m_list_Results.GetItemCount(),LoadStg(IDS_STRING1918));
		m_list_Results.UpdateWindow();
		return false;
	}

	/* copy info */
	strcpy(omBc.cAction,opAction.GetBuffer(0));
	strcpy(omBc.cData,opData.GetBuffer(0));
	strcpy(omBc.cSelection,opSelection.GetBuffer(0));
	strcpy(omBc.cFields,opFields.GetBuffer(0));
	strcpy(omBc.cTable,opTable.GetBuffer(0));
	strcpy(omBc.cTws,opTwStart.GetBuffer(0));
	strcpy(omBc.cTwe,opTwEnd.GetBuffer(0));
	
	// in case the response takes time
	AfxGetApp()->DoWaitCursor(1);
//	strMsg.Format("Sende Kommando '%s', warte auf Antwort...",omBc.cAction);
//	m_list_Results.InsertItem(m_list_Results.GetItemCount(),strMsg);
//	m_list_Results.EnsureVisible(m_list_Results.GetItemCount(),false);
//	m_list_Results.UpdateWindow();
	if ((iLastReturnCode = ::CallCeda(omBc.cReq_id,omBc.cUser,omBc.cWks,omBc.cAction,omBc.cTable,
									  omBc.cSortClause,omBc.cTws,omBc.cTwe,omBc.cSelection,
									  omBc.cFields,omBc.cData,omBc.cDataDest)) != 0)
	{
		MessageBeep((UINT)-1);
		strError.Format(LoadStg(IDS_STRING1917),omBc.cData);
		m_list_Results.InsertItem(m_list_Results.GetItemCount(),strError);
		m_list_Results.EnsureVisible(m_list_Results.GetItemCount(),false);
//		m_list_Results.UpdateWindow();
		return false;
	}
	// switch off the wait cursor
	AfxGetApp()->DoWaitCursor(-1);
	return true;
}


//**********************************************************************************
// OnTimer: handle timer events for this Dialog.
// Input:	id of timer (can only be 1)
// Return:	none
//**********************************************************************************

void COpenJobControlDialog::OnTimer(UINT nIDEvent) 
{
CCS_TRY
	if (bmInitOk && (nIDEvent == 1))
	{
		CString strMsg;
		bool blSuccess = false;
		// try three times 
		for (int x=1; (x<=MAX_OPENJOB_TRIALS) && !blSuccess; x++)
		{
			strMsg.Format(LoadStg(IDS_STRING1914),x);
			m_list_Results.InsertItem(m_list_Results.GetItemCount(),strMsg);
			m_list_Results.UpdateWindow();
			blSuccess = FillOpenJobGrid(false);
		}
		KillTimer(nIDEvent);
	}
CCS_CATCH_ALL
	CDialog::OnTimer(nIDEvent);
}

//**********************************************************************************
// OnButtonRefresh: reload open jobs.
// Input:	none
// Return:	none
//**********************************************************************************

void COpenJobControlDialog::OnButtonRefresh() 
{
CCS_TRY
	// reread job list
	FillOpenJobGrid();
CCS_CATCH_ALL
}

//**********************************************************************************
// OnButtonRemoveAll: remove all jobs from handlers jobe queue
// Input:	none
// Return:	none
//**********************************************************************************

void COpenJobControlDialog::OnButtonRemoveAll() 
{
CCS_TRY
	// make sure this happens not accidently
	if (MessageBox(LoadStg(IDS_STRING1948),LoadStg(IDS_STRING1949),MB_ICONQUESTION|MB_YESNO) == IDNO)
		return;

	// hide action buttons
	EnableButtons(false);
	// send bc 'OJC' (='Open Job Control') - 'RAJ' (='Remove All Jobs') to handler
	SendBC("OJC","","","","RAJ");
	// clear grid
	ClearGrid();
	// show action buttons
	EnableButtons(true);
CCS_CATCH_ALL
}

//**********************************************************************************
// OnButtonDeleteJob: remove a single job from handlers job queue
// Input:	none
// Return:	none
//**********************************************************************************

void COpenJobControlDialog::OnButtonDeleteJob() 
{
CCS_TRY
	CStringArray olSelJobs;
	CString olNextJob, olInfoMsg;
	int ilLastPos = 0; // for '\n' search in error buffer

	// get selection
	if (!GetSelectedJobs(&olSelJobs)) 
		return; // error -> terminate

	// make sure this happens not accidently
	if (MessageBox(LoadStg(IDS_STRING1947),LoadStg(IDS_STRING1949),MB_ICONQUESTION|MB_YESNO) == IDNO)
		return;

	// disable user buttons
	EnableButtons(false);

	// clear message list
	m_list_Results.DeleteAllItems();
	
	// send one bc per job
	for (int x=0; x<olSelJobs.GetSize(); x++)
	{
		olNextJob = olSelJobs.GetAt(x);
		olInfoMsg.Format(LoadStg(IDS_STRING1922),olNextJob);
		m_list_Results.InsertItem(m_list_Results.GetItemCount(),olInfoMsg);
		m_list_Results.EnsureVisible(m_list_Results.GetItemCount()-1,false);
		m_list_Results.UpdateWindow();
		if (!SendBC("OJC","",LPCTSTR(olNextJob),"","RSJ"))
		{	
			// error sending bc -> terminate
			break;
		}
		// success -> check and print result
		if (CString(omBc.cData) != "ok")
		{
			olInfoMsg.Format(LoadStg(IDS_STRING1923),omBc.cData);
			m_list_Results.InsertItem(m_list_Results.GetItemCount(),olInfoMsg);
			m_list_Results.EnsureVisible(m_list_Results.GetItemCount()-1,false);
			m_list_Results.UpdateWindow();
		}
		else
		{
			// everything is OK, no error
			m_list_Results.InsertItem(m_list_Results.GetItemCount(),LoadStg(IDS_STRING1924));
			m_list_Results.EnsureVisible(m_list_Results.GetItemCount()-1,false);
			m_list_Results.UpdateWindow();
		}
	}
	
	// reread job list
	FillOpenJobGrid(false);

	// enable user buttons
	EnableButtons(true);
CCS_CATCH_ALL
}

//*********************************************************************************************************************
// GetSelectedJobs: fill list with selected open jobs from grid.
// Input:	a CStringArray to store the open jobs indexes in
// Return:	TRUE	-> everything is ok
//			FALSE	-> no opne job is selected
//*********************************************************************************************************************

bool COpenJobControlDialog::GetSelectedJobs(CStringArray *popSelectedOpenJobs)
{
CCS_TRY
	CGXStyle		olStyle;
	CRowColArray	olRowColArray;
	CString			olIndex;

	// remove all entrys from list
	popSelectedOpenJobs->RemoveAll();
		
	// read selection from grid
	pomOpenJobGrid->GetSelectedRows(olRowColArray,false,false);

	// are there any selected items?
	if (olRowColArray.GetSize() == 0)
	{
		// no -> do error message and terminate
		TRACE ("COpenJobControlDialog::GetSelectedJobs(): no OpenJobs selected.\n");
		MessageBeep((UINT)-1);
		MessageBox(LoadStg(IDS_STRING1921),LoadStg(IDS_STRING1876),MB_ICONEXCLAMATION);
		return false;
	}
	
	// get indexes from grid
	for (int i=0;i<olRowColArray.GetSize();i++)
	{
		// get style to get data pointer
		pomOpenJobGrid->ComposeStyleRowCol(olRowColArray[i],1, &olStyle );
		// get item
		olIndex.Format("%d",(int)olStyle.GetItemDataPtr());
		TRACE ("COpenJobControlDialog::GetSelectedJobs(): adding '%s'.\n",olIndex);
		popSelectedOpenJobs->Add(olIndex);
	}
	return true;
CCS_CATCH_ALL
return false;
}

//**********************************************************************************
// OnButtonViewInfo: view detailled job info.
// Input:	none
// Return:	none
//**********************************************************************************

void COpenJobControlDialog::OnButtonViewInfo() 
{
CCS_TRY
	CGXStyle		olStyle;
	CRowColArray	olRowColArray;
	CString			olIndex;
	CString			olInfoString;

	// read selection from grid
	pomOpenJobGrid->GetSelectedRows(olRowColArray,false,false);

	// are there any selected items?
	if (olRowColArray.GetSize() == 0)
	{
		// no -> do error message and terminate
		TRACE ("COpenJobControlDialog::GetSelectedJobs(): no OpenJobs selected.\n");
		MessageBeep((UINT)-1);
		MessageBox(LoadStg(IDS_STRING1921),LoadStg(IDS_STRING1876),MB_ICONEXCLAMATION);
		return;
	}
	
	// are there more than one selected job?
	if (olRowColArray.GetSize() > 1)
	{
		// yes -> do error message and terminate
		TRACE ("COpenJobControlDialog::GetSelectedJobs(): more than one OpenJobs selected.\n");
		MessageBeep((UINT)-1);
		MessageBox(LoadStg(IDS_STRING1944),LoadStg(IDS_STRING1876),MB_ICONEXCLAMATION);
		return;
	}
	

	// get indexes from grid
	pomOpenJobGrid->ComposeStyleRowCol(olRowColArray[0],1, &olStyle );
	// get item
	olInfoString = 	olStyle.GetValue();
	// do dialog
	CEditOpenJobDialog olEdDlg(olInfoString);
	olEdDlg.DoModal();

CCS_CATCH_ALL
}
