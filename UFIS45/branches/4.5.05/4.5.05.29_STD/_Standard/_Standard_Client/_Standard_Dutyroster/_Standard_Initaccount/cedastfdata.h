// CedaStfData.h

#ifndef __CEDAPSTFDATA__
#define __CEDAPSTFDATA__
 
#include <stdafx.h>
#include <basicdata.h>
#include <afxdisp.h>

//---------------------------------------------------------------------------------------------------------
 
struct STFDATA 
{
	COleDateTime 	 Dodm; 	// Austrittsdatum
	COleDateTime 	 Doem; 	// Eintrittsdatum
	char 	 Finm[42]; 	// Vorname
	char 	 Lanm[42]; 	// Name
	char 	 Peno[22]; 	// Personalnummer
	char 	 Perc[7]; 	// K�rzel
	char 	 Shnm[42]; 	// Kurzname
	long 	 Urno; 	// Eindeutige Datensatz-Nr.

	//DataCreated by this class
	int      IsChanged;

	//long, CTime
	STFDATA(void)
	{
		memset(this,'\0',sizeof(*this));
		Dodm.SetStatus(COleDateTime::invalid);
		Doem.SetStatus(COleDateTime::invalid);
	}

}; // end STFDataStruct

//---------------------------------------------------------------------------------------------------------
// Class declaratino

class CedaStfData: public CCSCedaData
{
// Attributes
public:
    CMapPtrToPtr omUrnoMap;
    // Map mit den Mitarbeiternummern
	CMapStringToPtr omPenoMap;

    CCSPtrArray<STFDATA> omData;

	char pcmListOfFields[2048];

	void SetTableName(CString opTableName)
	{
		strcpy(pcmTableName, opTableName.GetBuffer(0));
	}
// OStfations
public:
    CedaStfData();
	~CedaStfData();
	virtual void GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType);
	void Register(void);
	void ClearAll(bool bpWithRegistration = true);
    bool Read(char *pspWhere = NULL);
	bool Insert(STFDATA *prpStf);
	bool InsertInternal(STFDATA *prpStf);
	bool Update(STFDATA *prpStf);
	bool UpdateInternal(STFDATA *prpStf);
	bool Delete(long lpUrno);
	bool DeleteInternal(STFDATA *prpStf);
	bool Save(STFDATA *prpStf);
	void ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
	STFDATA  *GetStfByUrno(long lpUrno);
	STFDATA  *GetStfByPeno(CString opPeno);
	// Pr�ft ob MA zu diesem Zeitpunkt im Unternehmen ist
	bool IsStfNowValid(long lpUrno, COleDateTime opDay);
	bool Initialize(CString opServerName);

	// Private methods
private:
    void PrepareStfData(STFDATA *prpStfData);

};

extern CedaStfData ogStfData;

//---------------------------------------------------------------------------------------------------------


#endif //__CEDAPSTFDATA__
