// InitializeAccountDlg.h : header file
//

#if !defined(AFX_INITIALIZEACCOUNTDLG_H__EA4946E7_1ACF_11D4_8FA9_00010204A53B__INCLUDED_)
#define AFX_INITIALIZEACCOUNTDLG_H__EA4946E7_1ACF_11D4_8FA9_00010204A53B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <UDlgStatusBar.h>	// integrated status line and progress bar
#include <CCSEdit.h>		// CCSEdit fields for date / year input
#include <CedaBasicData.h>
#include <ColorControls.h>
#include <CedaStfData.h>

// modes of execution for handler 
#define EXECUTION_NOW		1
#define EXECUTION_LATER		2
#define EXECUTION_MONTHLY	3

// number of clomuns in grids
#define EMPL_COLCOUNT 5	// APO 28.06.2001: additional column with org code
#define ACCOUNT_COLCOUNT 2

static char *pcgInfoStringFormat = "YYYYMMDD,YYYYMMDD,X,HHMM,DD,VXY,YYYYMMDDHHMM";
// FROM,TO,MODE,TIME,DAY,OFFSET,FIRSTEXEC,BCNUM

class CGridControl;

// ACCOUNTTYPEINFO describes accounts
struct ACCOUNTTYPEINFO 
{
	UINT	iInternNumber;	// internal code
	CString	oExternKey;		// equals the define without 'K_' from enum accounttype
	CString	oName;			// name of account
	CString	oShortName;		// short name of account
	CString	oFormat;		// format tag for account e.g. "%01.2f "
	bool	bShowAccount;	// show this account in grid anyway?
	CString	oScbi;			// the command to send to XBSHDL for this account in INIT-mode
	CString	oScbr;			// the command to send to XBSHDL for this account in RECALC-mode
	CString	oScbs;			// the command to send to XBSHDL for this account in SALDO-mode

	ACCOUNTTYPEINFO(void)
	{
		iInternNumber	= 0;
		oExternKey		= "";
		oName			= "";
		oShortName		= "";
		oScbi			= "";
		oScbr			= "";
		oScbs			= "";
		oFormat			= "%f ";
		bShowAccount	= true;
	}
};

/////////////////////////////////////////////////////////////////////////////
// CInitializeAccountDlg dialog

class CInitializeAccountDlg : public CDialog
{
// Construction
public:
	CInitializeAccountDlg(CWnd* pParent, CString opStartNightJobAt,
						  CString opCmdLohnbestandteile, 
						  CString omCmdRecalculation,
						  CString opStartMonthlyTimeAndDate,
						  int ipMaxEmplUrnosPerBC);	// standard constructor

	~CInitializeAccountDlg();
	
	// process key input
	void ProcessChar(UINT nChar);

	void FillContractMap();

protected:
// Dialog Data
	CMapStringToString omVertragStunden;
	// the employee grid
	CGridControl *pomEmplList;
	// the account grid
	CGridControl *pomAccountList;
	// status and progress bar at dialog bottom
	UDlgStatusBar *pomStatusBar;
	// id of output static in <pomStatusBar>
	int imIDStatic;
	// id of progress bar in <pomStatusBar>
	int imIDProgress;
	// structures to hold the account type information
    CCSPtrArray<ACCOUNTTYPEINFO> omAccountTypeInfoList;
	CMapPtrToPtr omAccountTypeInfoPtrMap;
	// trigger for calculation abort
	bool bmUserAbort;
	// start time for night jobs
	CString omStartNightJobAt;
	// start time and date for monthly action ('Lohnbestandteile'), format: 'DDHHMM'
	// -> day of month, hour and minute of time
	CString omStartMonthlyTimeAndDate;
	// command for 'Lohnbestandteile'
	CString omCmdLohnbestandteile;
	// command for recalculation
	CString omCmdRecalculation;
	// array for selected employees
	CCSPtrArray<STFDATA> omStfData;
	// when to execute the job
	int imExecutionMode;
	// maximum number of employee urnos sended per bc
	int imMaxEmplUrnosPerBC;

	//{{AFX_DATA(CInitializeAccountDlg)
	enum { IDD = IDD_INITIALIZEACCOUNT_DIALOG };
	CButton	m_cb_AllEmployees;
	CButton	m_but_OpenJobControl;
	CButton	m_stc_NowOrLater;
	CStatic	m_S_Org;
	CColorButton	m_B_Filter;
	CComboBox	m_C_Orgeinheit;
	CButton	m_b_Abort;
	CButton	m_b_Actualize;
	CButton	m_b_Ok;
	CStatic	m_stc_Year;
	CButton	m_stc_TimeSpan;
	CButton	m_stc_Employees;
	CButton	m_stc_Accounts;
	CButton	m_b_SelectAll;
	CButton	m_b_DeselectAll;
	CButton	m_b_FindEmployee;
	CButton	m_R_Day;
	CButton	m_R_Month01;
	CButton	m_R_Month02;
	CButton	m_R_Month03;
	CButton	m_R_Month04;
	CButton	m_R_Month05;
	CButton	m_R_Month06;
	CButton	m_R_Month07;
	CButton	m_R_Month08;
	CButton	m_R_Month09;
	CButton	m_R_Month10;
	CButton	m_R_Month11;
	CButton	m_R_Month12;
	CButton	m_R_Now;
	CButton	m_R_Later;
	CButton	m_R_AccFilterInit;
	CButton	m_R_AccFilterRecalc;
	CButton	m_R_AccFilterSaldo;
	CButton	m_R_Lohnbestandteile;
	CCSEdit	m_E_Date;
	CCSEdit	m_E_Year;
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CInitializeAccountDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	HICON m_hIcon;

	// initialize grids
	void InitGrids();
	void InitAccountGrid(void);
	void InitEmployeeGrid(void);
	// set up cloumn widths
	void InitAccountColWidths();
	void InitEmployeeColWidths();
	// fill grids
	int FillAccountGrid();
	void FillEmployeeGrid();
	// sort the employee grid
	bool SortEmployeeGrid(int ipRow);
	// get selections from grids
	bool GetSelectedEmployees(CStringArray *popEmployeeUrnos);
	bool GetSelectedAccountCommands(CStringArray *popCommands);
	// get and check time span from controls
	bool GetAndCheckTimeSpan(COleDateTime &opFrom, COleDateTime &opTo);
	// calculate and check the first execution date
	bool CheckInfoForMonthlyJob(COleDateTime opStart, COleDateTime &opFirstExec,
								int ipMonthOffset);
	// send broadcast to XBSHDL
	bool SendBC(CString opCmd, CString opData,
				CString opAdditionalInfoFields,CString opAdditionalInfoData);
	// rerference function for UHI, can be ignored by anyone
	bool Send_XSCR_BC_and_get_result(CString opScriptName,CString opData,CString opFieldValues);
	// load string resources
	void LoadStringResources(void);
	// load account data
	int LoadAccountData(void);
	// Fill Organisation unit Combo
	void FillOrgCombo(void);
	// get an employee's organisation unit code
	CString GetEmployeeOrgCode(long lpUrno, COleDateTime opStart, COleDateTime opEnd);

	// get an employee's contract type
	CString GetEmployeeContract(long lpUrno, COleDateTime opStart, COleDateTime opEnd);
	
	// process account specific broadcasts from xbshdl
	static void  ProcessAccountCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

	void SendCommand(CString olCommand, CString olAction);

	// Generated message map functions
	//{{AFX_MSG(CInitializeAccountDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnDestroy();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnSelectAllEmployees();
	afx_msg void OnDeselectAllEmployees();
	afx_msg void OnFindEmployee();
	afx_msg void OnRbDate();
	afx_msg void OnMonth();
	afx_msg void OnAccFilter();
	virtual void OnOK();
	afx_msg void OnBActualize();
	afx_msg void OnButtonAbort();
	afx_msg void OnSelchange_C_Org();
	afx_msg void OnUpdateDate();
	afx_msg void OnUpdateYear();
	afx_msg void OnRadioNow();
	afx_msg void OnRadioLater();
	afx_msg void OnRbLohnbestandteile();
	afx_msg void OnRadioMonthly();
	afx_msg void OnButtonOpenjobcontrol();
	afx_msg void OnShowFilteredEmployee();
	afx_msg void OnRbRecalculation();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

public:

	// Hier gehen alle BC f�r diese Anwendung rein
	LONG OnBcAdd(UINT /*wParam*/, LONG /*lParam*/); 
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_INITIALIZEACCOUNTDLG_H__EA4946E7_1ACF_11D4_8FA9_00010204A53B__INCLUDED_)
