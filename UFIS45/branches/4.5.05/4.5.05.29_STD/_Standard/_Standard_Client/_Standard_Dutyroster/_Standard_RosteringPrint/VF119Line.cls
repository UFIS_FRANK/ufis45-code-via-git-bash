VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "VF119Line"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Public key As String

Public strOrga As String
Public strGroup As String
Public strSepa As String
Public strTime As String
Public strShiftCode As String
Public strFunction As String
Public strName As String
Public strDuration As String
Public strUser As String
'-------------------------
'Type possible values:
'"Line" or "Sepa"
Public LineType As String

