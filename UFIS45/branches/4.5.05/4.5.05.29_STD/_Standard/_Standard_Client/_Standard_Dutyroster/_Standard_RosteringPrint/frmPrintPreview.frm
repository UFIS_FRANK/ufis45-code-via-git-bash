VERSION 5.00
Object = "{A45D986F-3AAF-4A3B-A003-A6C53E8715A2}#1.0#0"; "ARVIEW2.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmPrintPreview 
   Caption         =   "Print Preview"
   ClientHeight    =   9660
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   13620
   LinkTopic       =   "Form2"
   ScaleHeight     =   9660
   ScaleWidth      =   13620
   StartUpPosition =   3  'Windows Default
   Begin DDActiveReportsViewer2Ctl.ARViewer2 viewer1 
      Height          =   8895
      Left            =   0
      TabIndex        =   1
      Top             =   720
      Width           =   13575
      _ExtentX        =   23945
      _ExtentY        =   15690
      SectionData     =   "frmPrintPreview.frx":0000
   End
   Begin MSComctlLib.Toolbar picToolBar 
      Align           =   1  'Align Top
      Height          =   675
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   13620
      _ExtentX        =   24024
      _ExtentY        =   1191
      ButtonWidth     =   1773
      ButtonHeight    =   1032
      Appearance      =   1
      ImageList       =   "ImageList1"
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   2
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Excel Export"
            Key             =   "Excel"
            Object.ToolTipText     =   "Export to Excel"
            Object.Tag             =   "Excel"
            ImageIndex      =   1
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Pdf Export"
            Key             =   "Pdf"
            Object.ToolTipText     =   "Export to Pdf"
            ImageIndex      =   2
         EndProperty
      EndProperty
      Begin MSComDlg.CommonDialog CommonDialog1 
         Left            =   11160
         Top             =   120
         _ExtentX        =   847
         _ExtentY        =   847
         _Version        =   393216
      End
      Begin MSComctlLib.ImageList ImageList1 
         Left            =   12480
         Top             =   0
         _ExtentX        =   1005
         _ExtentY        =   1005
         BackColor       =   -2147483643
         ImageWidth      =   19
         ImageHeight     =   19
         MaskColor       =   12632256
         _Version        =   393216
         BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
            NumListImages   =   2
            BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmPrintPreview.frx":003C
               Key             =   "Excel"
            EndProperty
            BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmPrintPreview.frx":0502
               Key             =   "Pdf"
            EndProperty
         EndProperty
      End
   End
End
Attribute VB_Name = "frmPrintPreview"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private oEXL As ActiveReportsExcelExport.ARExportExcel
Private oPDF As ActiveReportsPDFExport.ARExportPDF
Private oTXT As ActiveReportsTextExport.ARExportText
Private oHTML As ActiveReportsHTMLExport.HTMLexport


Private Sub Form_Resize()
    On Error Resume Next
    If Me.WindowState <> vbMinimized Then
        viewer1.Move 0, picToolBar.Top + picToolBar.Height, Me.width - 200, Me.Height - picToolBar.Height - 200
    End If
End Sub


Private Sub Form_Unload(Cancel As Integer)
    If ExistFile(Form1.omFile) = True Then
        Kill Form1.omFile
    End If
    
    Form1.ExitApp
End Sub

Private Sub picToolBar_ButtonClick(ByVal Button As MSComctlLib.Button)
    Dim strPath As String
    CommonDialog1.DialogTitle = "Save as..."
    On Error Resume Next

    ' setting the file filter
    If Button.Index = 1 Then
        CommonDialog1.Filter = "Excel-files (*.xls)|*.xls|All files (*.*)|*.*"
    ElseIf Button.Index = 2 Then
        CommonDialog1.Filter = "PDF-files (*.pdf)|*.pdf|All files (*.*)|*.*"
    ElseIf Button.Index = 3 Then
        CommonDialog1.Filter = "Text-files (*.txt)|*.txt|All files (*.*)|*.*"
    Else
        CommonDialog1.Filter = "HTML-files (*.html)|*.html|All files (*.*)|*.*"
    End If

    ' show the dialog
    CommonDialog1.ShowSave
    If CommonDialog1.CancelError = False Then
        strPath = CommonDialog1.filename
        ' do the export
        If Button.Index = 1 Then
            Set oEXL = New ActiveReportsExcelExport.ARExportExcel
            oEXL.filename = strPath
            oEXL.Export Me.viewer1.Pages
        ElseIf Button.Index = 2 Then
            Set oPDF = New ActiveReportsPDFExport.ARExportPDF
            oPDF.filename = strPath
            oPDF.AcrobatVersion = 2
            oPDF.SemiDelimitedNeverEmbedFonts = ""
            oPDF.Export Me.viewer1.Pages
        ElseIf Button.Index = 3 Then
            Set oTXT = New ActiveReportsTextExport.ARExportText
            oTXT.filename = strPath
            oTXT.PageDelimiter = ";"
            oTXT.TextDelimiter = ","
            oTXT.SuppressEmptyLines = True
            oTXT.Export Me.viewer1.Pages
        Else
            Set oHTML = New ActiveReportsHTMLExport.HTMLexport
            oHTML.filename = strPath
            oHTML.Export Me.viewer1.Pages
        End If
    End If
End Sub
