// ChoiceDlg.cpp : implementation file
//

#include <stdafx.h>
#include <staffterm.h>
#include <ChoiceDlg.h>
#include <StaffTermDlg.h>
#include <DPLDlg.h>
#include <CedaStfData.h>
#include <CedaScoData.h>
#include <CCSParam.h>

//uhi 21.6.01 f�r _spawnv
#include <stdio.h>
#include <process.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CChoiceDlg dialog


CChoiceDlg::CChoiceDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CChoiceDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CChoiceDlg)
	m_radio_wish = -1;
	//}}AFX_DATA_INIT
	
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);

	m_radio_wish = 0;

	StfUrno = 0;
	Name = CString("");

	//uhi 20.9.00 Parameter lesen
	CTime	olTimeNow = CTime::GetCurrentTime();
	CString olStringNow = olTimeNow.Format("%Y%m%d%H%M%S");
	CString olVertragFest, olVertragStunden;

	olVertragFest = CString(ogCCSParam.GetParamValue(ogAppl,"ID_VERTRAG_FEST",olStringNow));
	olVertragStunden = CString(ogCCSParam.GetParamValue(ogAppl,"ID_VERTRAG_STD",olStringNow));
	
	FillContractMap(olVertragFest, CString("FEST"));
	FillContractMap(olVertragStunden, CString("STUNDEN"));
}



void CChoiceDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CChoiceDlg)
	DDX_Radio(pDX, IDC_RADIO_WISH, m_radio_wish);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CChoiceDlg, CDialog)
	//{{AFX_MSG_MAP(CChoiceDlg)
	ON_BN_CLICKED(IDC_BUTTON_SHOW, OnButtonShow)
	ON_BN_CLICKED(IDC_RADIO_WISH, OnRadioWish)
	ON_BN_CLICKED(IDC_RADIO_DAYSCHEDULE, OnRadioDaySchedule)
	ON_BN_CLICKED(IDC_RADIO_SCHEDULEORG, OnRadioScheduleOrg)
	ON_BN_CLICKED(ID_BUTTON_INFO, OnButtonInfo)
	ON_BN_CLICKED(IDC_RADIO_ABSPLAN, OnRadioAbsplan)
	//}}AFX_MSG_MAP
	ON_MESSAGE(WM_BCADD,OnBcAdd)

END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CChoiceDlg message handlers

BOOL CChoiceDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	ogCommHandler.RegisterBcWindow(this);
	ogBcHandle.GetBc();

	UINT ilBcadd = WM_BCADD;
	UINT ilUser  = WM_USER;

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu = LoadStg(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
		pSysMenu->DeleteMenu(SC_CLOSE, MF_BYCOMMAND);
		pSysMenu->DeleteMenu(SC_MAXIMIZE, MF_BYCOMMAND);
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	SetDlgItemText(IDOK, LoadStg(ID_OK));
	SetDlgItemText(IDCANCEL, LoadStg(IDS_EXIT));
	SetDlgItemText(IDC_RADIO_WISH, LoadStg(IDS_STRING1850));
	SetDlgItemText(IDC_RADIO_ABSPLAN, LoadStg(IDS_STRING1881));
	SetDlgItemText(IDC_RADIO_SCHEDULEORG, LoadStg(IDS_STRING1852));
	SetDlgItemText(IDC_RADIO_DAYSCHEDULE, LoadStg(IDS_STRING1853));
	SetDlgItemText(IDC_BUTTON_SHOW, LoadStg(IDS_STRING1854));
	SetDlgItemText(IDC_STATIC_CHOICE, LoadStg(IDS_STRING1855));

	m_radio_wish = 1;

	CString csTitle = LoadStg(IDS_STAFFTERM);
	CString csLanm, csFinm, csName;
	if(StfUrno != 0){
		STFDATA *pStf = ogStfData.GetStfByUrno(StfUrno);
		if(pStf!=NULL){
			csLanm = pStf->Lanm;
			csFinm = pStf->Finm;
			if(!csLanm.IsEmpty() && !csFinm.IsEmpty()){
				csName = csFinm + CString(" ") + csLanm;
				Name = csName;
				csTitle = csTitle + CString(" - ") + csName;
				AfxGetApp()->m_pMainWnd->SetWindowText(csTitle);
			}
			else{
				csTitle = csTitle + CString(" - ") + LoadStg(IDS_STRING1013);
				AfxGetApp()->m_pMainWnd->SetWindowText(csTitle);
			}
		}
		else{
			csTitle = csTitle + CString(" - ") + LoadStg(IDS_STRING1013);
			AfxGetApp()->m_pMainWnd->SetWindowText(csTitle);
		}
	}
	else{
		csTitle = csTitle + CString(" - ") + LoadStg(IDS_STRING1013);
		AfxGetApp()->m_pMainWnd->SetWindowText(csTitle);
	}

	m_radio_wish = 0;
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

//***************************************************************************
// Sollte ein BC kommen f�r die sich diese Applikation angemeldet hat,
// wird diese Funktion aufgerufen und alle BC werden an den ogBcHandle 
// weitergegeben.
//***************************************************************************

LONG CChoiceDlg::OnBcAdd(UINT wParam, LONG /*lParam*/)
{
	ogBcHandle.GetBc(wParam);
	return TRUE;
}


void CChoiceDlg::OnButtonShow() 
{
	int nResponse, ilReturn;
	//CDPLDlg dlgDPL;	
	CStaffTermDlg dlgStaffTerm;
	CCSPtrArray<SCODATA> opScoData;
	SCODATA olSco;
	CString csCot, olValue, slExcelPath, csStfu;
	COleDateTime ctToday = COleDateTime::GetCurrentTime();

	switch(m_radio_wish){
	case 0:	
		//uhi 19.9.00
		//Vertragsart ermitteln
		ogScoData.GetScoBySurnWithTime(StfUrno, ctToday, ctToday, &opScoData);
		if(opScoData.GetSize()>0){
			olSco = opScoData.GetAt(0);
			csCot = olSco.Code;
		}
		else{
			CString MESSAGE_BOX_CAPTION	= LoadStg(IDS_STAFFTERM);
			MessageBox(LoadStg(IDS_NOTALLOWED),MESSAGE_BOX_CAPTION,MB_ICONINFORMATION);
			break;
		}

		//uhi 20.9.00
		if(omVertragFest.Lookup(csCot, olValue))
			dlgStaffTerm.bGav = true;
		else
			if (omVertragStunden.Lookup(csCot, olValue))
				dlgStaffTerm.bGav = false;
			else{
				CString MESSAGE_BOX_CAPTION	= LoadStg(IDS_STAFFTERM);
				MessageBox(LoadStg(IDS_NOTALLOWED),MESSAGE_BOX_CAPTION,MB_ICONINFORMATION);
				break;
		}

		dlgStaffTerm.StfUrno = StfUrno;
		dlgStaffTerm.Name = Name;
		nResponse = dlgStaffTerm.DoModal();
		break;

	//uhi 21.6.01 Aufruf Absence Planning
	case 1:
		if(!strcmp(pcgAbsPlan, "DEFAULT")){ 
			MessageBox(LoadStg(IDS_STRING22), LoadStg(IDS_ERROR),MB_ICONERROR);
			break;
		}

		char *args[4];
		char slRunTxt[256];
		args[0] = "child";
		csStfu.Format("%d", StfUrno);
		//sprintf(slRunTxt,"%s@%s@%s@%s",ogAppl,CString("AbsPlanDefault"), CString("Passwort"), csStfu);
		
		//uhi 8.8.01 User + Passwort durchreichen
		sprintf(slRunTxt,"%s@%s@%s@%s",ogAppl,pcgUser, pcgPasswd, csStfu);

		args[1] = slRunTxt;
		args[2] = NULL;
		args[3] = NULL;

		// Ausf�hren der Application
		ilReturn = _spawnv(_P_NOWAIT,pcgAbsPlan, args); //pclExcelPath,args);

		// R�ckgabewert bearbeiten
		if  (ilReturn == -1)
		{
			// Fehlermeldung ausgeben
			slExcelPath = pcgAbsPlan;
			MessageBox(slExcelPath + LoadStg(IDS_STRING22),LoadStg(IDS_ERROR),MB_OK | MB_ICONSTOP);
		}

		break;

	/*case 1:
		dlgDPL.StfUrno = StfUrno;
		dlgDPL.Name = Name;
		nResponse = dlgDPL.DoModal();
		break;
	case 2:
		break;
	case 3:
		break;*/
	default:
		CString MESSAGE_BOX_CAPTION	= LoadStg(IDS_STAFFTERM);
		MessageBox(LoadStg(IDS_COGNOS),MESSAGE_BOX_CAPTION,MB_ICONINFORMATION);
		break;
	}
}

void CChoiceDlg::OnRadioWish() 
{
	// TODO: Add your control notification handler code here
	m_radio_wish = 0;
}

void CChoiceDlg::OnRadioDaySchedule() 
{
	// TODO: Add your control notification handler code here
	m_radio_wish = 3;
}

void CChoiceDlg::OnRadioScheduleOrg() 
{
	// TODO: Add your control notification handler code here
	m_radio_wish = 2;
}

void CChoiceDlg::FillContractMap(CString omString, CString omContract)
{
	CString olString;
	olString = omString;
	
	CString polText;
	char cpTrenner = ',';

	BOOL blEnd = FALSE;

	if(!olString.IsEmpty())
	{
		int pos;
		int olPos = 0;
		while(blEnd == FALSE)
		{
			pos = olString.Find(cpTrenner);
			if(pos == -1)
			{
				blEnd = TRUE;
				polText = olString;
			}
			else
			{
				polText = olString.Mid(0, olString.Find(cpTrenner));
				olString = olString.Mid(olString.Find(cpTrenner)+1, olString.GetLength( )-olString.Find(cpTrenner)+1);
			}
			if(omContract == CString("FEST"))
				omVertragFest.SetAt(polText.GetBuffer(0), NULL);
			else
				if(omContract == CString("STUNDEN"))
					omVertragStunden.SetAt(polText.GetBuffer(0), NULL);
		}
	}

}

void CChoiceDlg::OnButtonInfo() 
{
	// TODO: Add your control notification handler code here
	CAboutDlg olAboutDlg;
	olAboutDlg.DoModal();
}

void CChoiceDlg::OnRadioAbsplan() 
{
	// TODO: Add your control notification handler code here
	m_radio_wish = 1;
}
