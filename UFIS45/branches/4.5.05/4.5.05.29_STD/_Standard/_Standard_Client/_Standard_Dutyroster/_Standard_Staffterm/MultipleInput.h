#if !defined(AFX_MULTIPLEINPUT1_H__498CE401_E6D1_11D4_906E_0050DADD7302__INCLUDED_)
#define AFX_MULTIPLEINPUT1_H__498CE401_E6D1_11D4_906E_0050DADD7302__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// MultipleInput1.h : header file
//

#include <CCSEdit.h>

/////////////////////////////////////////////////////////////////////////////
// CMultipleInput dialog

class CMultipleInput : public CDialog
{
// Construction
public:
	bool bGav;
	long StfUrno;
	COleDateTime ctValidInput;
	CMultipleInput(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CMultipleInput)
	enum { IDD = IDD_MULTIPLE_INPUT };
	CStatic	m_Static_Prio;
	CCSEdit	m_Edit_Remarks;
	CButton	m_Check_Wednesday;
	CButton	m_Check_Tuesday;
	CButton	m_Check_Thursday;
	CButton	m_Check_Sunday;
	CButton	m_Check_Saturday;
	CButton	m_Check_Monday;
	CButton	m_Check_Friday;
	CButton	m_Check_EveryDay;
	CComboBox	m_Combo_Prio;
	CComboBox	m_Combo_Code;
	CCSEdit	m_Edit_To;
	CCSEdit	m_Edit_From;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMultipleInput)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	void FillCodeCombo();

	// Generated message map functions
	//{{AFX_MSG(CMultipleInput)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	afx_msg void OnCheckEveryday();
	afx_msg void OnCheckFriday();
	afx_msg void OnCheckMonday();
	afx_msg void OnCheckSaturday();
	afx_msg void OnCheckSunday();
	afx_msg void OnCheckThursday();
	afx_msg void OnCheckTuesday();
	afx_msg void OnCheckWednesday();
	afx_msg void OnSelchangeComboCode();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MULTIPLEINPUT1_H__498CE401_E6D1_11D4_906E_0050DADD7302__INCLUDED_)
