// WGRToolDlg.cpp : implementation file
//

#include <stdafx.h>
#include <WGRTool.h>
#include <WGRToolDlg.h>
#include <CedaAloData.h>
#include <FilterPropertySheet.h>
#include <BasicData.h>
#include <CedaBasicData.h>
#include <Gridfenster.h>
#include <NewGroupDlg.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CWGRToolDlg dialog

CWGRToolDlg::CWGRToolDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CWGRToolDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CWGRToolDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);

	// create possible items grid
	pomPossilbeList = new CGridFenster(this);
	// create selected items grid
	pomSelectedList = new CGridFenster(this);

	omViewer.SetViewerKey("WGRTool");
	omViewer.SelectView("<Default>");

	bmIsInitialized = false;

	imPosALID = ogBCD.GetFieldIndex("WGR","ALID");
	imPosALGR = ogBCD.GetFieldIndex("WGR","ALGR");
	imPosALOC = ogBCD.GetFieldIndex("WGR","ALOC");
	imPosHOPO = ogBCD.GetFieldIndex("WGR","HOPO");

	imWGNPosALGR = ogBCD.GetFieldIndex("WGN","ALGR");
	imWGNPosHOPO = ogBCD.GetFieldIndex("WGN","HOPO");
}

CWGRToolDlg::~CWGRToolDlg()
{
	delete pomPossilbeList;
	delete pomSelectedList;
}

void CWGRToolDlg::DoDataExchange(CDataExchange* pDX)
{
	CString olText;
	CGXStyle olStyle;
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CWGRToolDlg)
	DDX_Control(pDX, IDC_LIST_GROUPED, m_GroupedList);
	DDX_Control(pDX, IDOK, m_OK);
	DDX_Control(pDX, IDC_CONTENTLIST, m_ContentList);
	DDX_Control(pDX, IDC_INSERTLIST, m_InsertList);
	DDX_Control(pDX, IDC_LIST_ALL, m_AllList);
	DDX_Control(pDX, IDC_LOCREMOVE, m_RemoveButton);
	DDX_Control(pDX, IDC_LOCADD, m_AddButton);
	DDX_Control(pDX, IDC_SAVE, m_SaveGroup);
	DDX_Control(pDX, IDC_NEW, m_NewGroup);
	DDX_Control(pDX, IDC_Delete, m_DeleteGroup);
	DDX_Control(pDX, IDC_GROUPBOX, m_GroupBox);
	DDX_Control(pDX, IDC_VIEWBOX, m_ViewBox);
	DDX_Control(pDX, IDC_HELPBUTTON, m_HelpButton);
	//}}AFX_DATA_MAP

	if (pDX->m_bSaveAndValidate)
	{
		CString olValue1;
		CString olValue2;
		omSelectedItems.RemoveAll();
		omSelectedItems.SetSize(pomSelectedList->GetRowCount());
		for (int ilLc = 0; ilLc < (int)pomSelectedList->GetRowCount(); ilLc++)
		{
			// get the hiden value
			olValue1 = pomSelectedList->GetValueRowCol(ilLc, imHideColStart );
			olValue2 = pomSelectedList->GetValueRowCol(ilLc, imHideColStart + 1 );
			if(!olValue1.IsEmpty())
			{
				omSelectedItems.SetAtGrow(ilLc,olValue1);
			}

		}

		int ilIndex = m_ViewBox.GetCurSel();
		if (ilIndex >= 0)
		{
			CString olView;
			m_ViewBox.GetLBText(ilIndex,olView);
			ChangeViewTo(olView);
		}
	}
	else
	{
		m_ViewBox.ResetContent();
		CStringArray olStrArr;
		omViewer.GetViews(olStrArr);
		for (int ilIndex = 0; ilIndex < olStrArr.GetSize(); ilIndex++)
		{
			m_ViewBox.AddString(olStrArr[ilIndex]);
		}
		CString olViewName = omViewer.GetViewName();
		if (olViewName.IsEmpty() == TRUE)
		{

			olViewName = omViewer.SelectView();//ogCfgData.rmUserSetup.STCV;
		}

		ilIndex = m_ViewBox.FindString(-1,olViewName);
			
		if (ilIndex != CB_ERR)
		{
			m_ViewBox.SetCurSel(ilIndex);
		}

		int ilLc;
		
		// Do not update window while processing new data
		m_InsertList.SetRedraw(FALSE);
		m_ContentList.SetRedraw(FALSE);

		// Initialize all possible items into the left list box
		m_InsertList.ResetContent();

		for (ilLc = 0; ilLc < omPossibleItems.GetSize(); ilLc++)
		{
			CString olTest = omPossibleItems[ilLc];
			m_InsertList.AddString(omPossibleItems[ilLc]);
		}

		// Initialize the selected items into the right list box,
		// also remove the corresponding items from the left list box.
		m_ContentList.ResetContent();
		for (ilLc = 0; ilLc < omSelectedItems.GetSize(); ilLc++)
		{
			CString olTmpText = omSelectedItems[ilLc];
			if(!olTmpText.IsEmpty())
			{
				int ilPos = m_InsertList.FindStringExact(-1, olTmpText);
				if(ilPos > -1)
				{
					// add to contents
					m_ContentList.AddString(olTmpText);
					// and remove from insertable
					m_InsertList.DeleteString(ilPos);
				}
				else	
				{
					// add to contents
					m_ContentList.AddString(olTmpText);
				}
			}
		}

		// enable window update 
		m_InsertList.SetRedraw(TRUE);
		m_ContentList.SetRedraw(TRUE);

		if(bmIsInitialized)
		{
			int ilGroupIndex = m_GroupBox.GetCurSel();
			if (ilGroupIndex >= 0)
			{
				m_AddButton.EnableWindow(TRUE);
			}
			else
			{
				m_AddButton.EnableWindow(FALSE);
			}

			
			CString olText;
			int ilCount = m_InsertList.GetCount();
				
			olStyle.SetEnabled(FALSE);
			olStyle.SetReadOnly(TRUE);
			
			pomPossilbeList->SetReadOnly(FALSE);
			pomSelectedList->SetReadOnly(FALSE);

			// save current sort information and top row
			int ilTopRow = pomPossilbeList->GetTopRow();

			// delete all
			pomPossilbeList->RemoveRows(1,  pomPossilbeList->GetRowCount());

			if (ilTopRow > ilCount)
				ilTopRow = ilCount;

			// display 16 rows at once only
			pomPossilbeList->SetRowCount(max(16,ilCount));

			CStringArray olItemList;
			CString olOrgText;

			for (int ilLc = 0; ilLc < ilCount; ilLc++)
			{
				m_InsertList.GetText(ilLc,olText);
				olOrgText = olText;
				ExtractItemList(olText, &olItemList, '#');
				if(olItemList.GetSize() > 1)
				{
					olText = olItemList[1];
					pomPossilbeList->SetStyleRange(CGXRange(ilLc+1, imHideColStart+1, ilLc+1, imHideColStart+1), olStyle.SetValue(olItemList[0]));
				}

				ExtractItemList(olText, &olItemList, ';');
				for(int ilItem = 0 ; ilItem < olItemList.GetSize(); ilItem++)
					pomPossilbeList->SetStyleRange(CGXRange(ilLc+1, ilItem+1, ilLc+1, ilItem+1), olStyle.SetValue(olItemList[ilItem]));
				pomPossilbeList->SetStyleRange(CGXRange(ilLc+1, imHideColStart, ilLc+1, imHideColStart), olStyle.SetValue(olOrgText));

			}

			pomPossilbeList->SortTable();

			pomPossilbeList->SetTopRow(ilTopRow);

			// save current sort information and top row
			ilTopRow = pomSelectedList->GetTopRow();

			// delete all
			pomSelectedList->RemoveRows(1,  pomSelectedList->GetRowCount());

			ilCount = m_ContentList.GetCount();
			if (ilTopRow > ilCount)
				ilTopRow = ilCount;
					
			pomSelectedList->SetRowCount(max(16,ilCount));

			for (ilLc = 0; ilLc < ilCount; ilLc++)
			{
				m_ContentList.GetText(ilLc,olText);
				olOrgText = olText;
				ExtractItemList(olText, &olItemList, '#');
				if(olItemList.GetSize() > 1)
				{
					olText = olItemList[1];
					pomSelectedList->SetStyleRange(CGXRange(ilLc+1, imHideColStart+1, ilLc+1, imHideColStart+1), olStyle.SetValue(olItemList[0]));

				}
				ExtractItemList(olText, &olItemList, ';');
				for(int ilItem = 0 ; ilItem < olItemList.GetSize(); ilItem++)
					pomSelectedList->SetStyleRange(CGXRange(ilLc+1, ilItem+1, ilLc+1, ilItem+1), olStyle.SetValue(olItemList[ilItem]));
				pomSelectedList->SetStyleRange(CGXRange(ilLc+1, imHideColStart, ilLc+1, imHideColStart), olStyle.SetValue(olOrgText));

			}


			pomSelectedList->SortTable();

			pomSelectedList->SetTopRow(ilTopRow);

			// grid is now no longer editable
			pomPossilbeList->SetReadOnly(TRUE);
			pomSelectedList->SetReadOnly(TRUE);

			// force grid update
			pomSelectedList->Redraw();
			pomPossilbeList->Redraw();
		}
	}
}

BEGIN_MESSAGE_MAP(CWGRToolDlg, CDialog)
	//{{AFX_MSG_MAP(CWGRToolDlg)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_Delete, OnDelete)
	ON_CBN_SELCHANGE(IDC_GROUPBOX, OnSelchangeGroupbox)
	ON_BN_CLICKED(IDC_LOCADD, OnLocadd)
	ON_BN_CLICKED(IDC_LOCREMOVE, OnLocremove)
	ON_BN_CLICKED(IDC_NEW, OnNew)
	ON_BN_CLICKED(IDC_SAVE, OnSave)
	ON_BN_CLICKED(IDC_VIEW, OnView)
	ON_CBN_SELCHANGE(IDC_VIEWBOX, OnSelchangeViewbox)
	ON_BN_CLICKED(IDC_HELPBUTTON, OnHelp)
	ON_BN_CLICKED(IDC_ABOUT, OnAbout)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CWGRToolDlg message handlers

BOOL CWGRToolDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	
	// TODO: Add extra initialization here
	// language specific initialization
	m_NewGroup.SetWindowText(LoadStg(IDS_STRING425));
	m_SaveGroup.SetWindowText(LoadStg(IDS_STRING419));
	m_DeleteGroup.SetWindowText(LoadStg(IDS_STRING420));
	m_OK.SetWindowText(LoadStg(IDS_STRING426));
	m_HelpButton.SetWindowText(LoadStg(IDS_STRING432));

	// disable save && delete group button
	m_SaveGroup.EnableWindow(FALSE);
	m_DeleteGroup.EnableWindow(FALSE);

	// disable add && remove location button
	m_AddButton.EnableWindow(FALSE);
	m_RemoveButton.EnableWindow(FALSE);

	// replace existing window procedure with our handler
	pomSelectedList->SubclassDlgItem(IDC_SELLIST, this);
	pomPossilbeList->SubclassDlgItem(IDC_POSLIST, this);

	// initialize selection grid
	pomSelectedList->Initialize();

	// disable immediate update
	pomSelectedList->LockUpdate(TRUE);
	pomSelectedList->GetParam()->EnableUndo(FALSE);
	// column width is static
	pomSelectedList->GetParam()->EnableTrackColWidth(FALSE);
	// row height is static
	pomSelectedList->GetParam()->EnableTrackRowHeight(FALSE);
	// column headers aren't numbered
	pomSelectedList->GetParam()->SetNumberedColHeaders(FALSE);

	// initialize possible grid
	pomPossilbeList->Initialize();
	pomPossilbeList->LockUpdate(TRUE);
	pomPossilbeList->GetParam()->EnableUndo(FALSE);
	// column width is static
	pomPossilbeList->GetParam()->EnableTrackColWidth(FALSE);
	// row height is static
	pomPossilbeList->GetParam()->EnableTrackRowHeight(FALSE);
	// column headers aren't numbered
	pomPossilbeList->GetParam()->SetNumberedColHeaders(FALSE);

	pomPossilbeList->SetRowHeight(0, 0, 12);


	for(int illc = 0; illc < imColCount; illc++)
		pomPossilbeList->SetColWidth(0,illc,40);

	// set the size of the allocation type
	pomPossilbeList->SetColWidth(2,2,100);
	// set the size of the allocation id
	pomPossilbeList->SetColWidth(1,1,100);
	// set the size of the row number
	pomPossilbeList->SetColWidth(0,0,30);


	pomSelectedList->SetRowHeight(0, 0, 12);

	for( illc = 0; illc < imColCount; illc++)
		pomSelectedList->SetColWidth(0,illc,40);
	
	// set the size of the allocation type
	pomSelectedList->SetColWidth(2,2,100);
	// set the size of the allocation id
	pomSelectedList->SetColWidth(1,1,100);
	// set the size of the row number
	pomSelectedList->SetColWidth(0,0,30);

	// create the list of already grouped locations
	CreateGroupedList();
	// create the list of all possible locations to group
	CreateAllList();

	// change view to display this view
	ChangeViewTo(omViewer.GetViewName());

	UpdateData(FALSE);

	imHideColStart = 1;
	CStringArray olItemList;
	if(omPossibleItems.GetSize() > 0)
	{
		ExtractItemList(omPossibleItems[0], &olItemList, ';');
	}

	// we start the hiding columns at least
	imHideColStart = olItemList.GetSize() +1;
	imColCount = imHideColStart + 2;

	CGXStyle olStyle;
	olStyle.SetEnabled(FALSE);
	olStyle.SetReadOnly(TRUE);

	// enable immediate update
	pomPossilbeList->LockUpdate(FALSE);
	pomSelectedList->LockUpdate(FALSE);

	// set estimated size
	pomPossilbeList->SetColCount(imColCount);
	pomSelectedList->SetColCount(imColCount);
	
	// hide following columns
	pomSelectedList->HideCols(imHideColStart, imColCount); 
	pomPossilbeList->HideCols(imHideColStart, imColCount); 

	CString olText;
	CString olOrgText;
	int ilCount = m_InsertList.GetCount();

	pomPossilbeList->SetRowCount(max(16,ilCount));
	for (int ilLc = 0; ilLc < ilCount; ilLc++)
	{
		m_InsertList.GetText(ilLc,olText);
		olOrgText = olText;
		ExtractItemList(olText, &olItemList, '#');
		if(olItemList.GetSize() > 1)
		{
			olText = olItemList[1];
			pomPossilbeList->SetStyleRange(CGXRange(ilLc+1, imHideColStart+1, ilLc+1, imHideColStart+1), olStyle.SetValue(olItemList[0]));

		}

		ExtractItemList(olText, &olItemList, ';');
		for(int ilItem = 0 ; ilItem < olItemList.GetSize(); ilItem++)
			pomPossilbeList->SetStyleRange(CGXRange(ilLc+1, ilItem+1, ilLc+1, ilItem+1), olStyle.SetValue(olItemList[ilItem]));
		pomPossilbeList->SetStyleRange(CGXRange(ilLc+1, imHideColStart, ilLc+1, imHideColStart), olStyle.SetValue(olOrgText));

	}

	ilCount = m_ContentList.GetCount();
		
	pomSelectedList->SetRowCount(max(16,ilCount));

	for (ilLc = 0; ilLc < ilCount; ilLc++)
	{
		m_ContentList.GetText(ilLc,olText);
		olOrgText = olText;
		ExtractItemList(olText, &olItemList, '#');
		if(olItemList.GetSize() > 1)
		{
			olText = olItemList[1];
			pomSelectedList->SetStyleRange(CGXRange(ilLc+1, imHideColStart+1, ilLc+1, imHideColStart+1), olStyle.SetValue(olItemList[0]));

		}
		ExtractItemList(olText, &olItemList, ';');
		for(int ilItem = 0 ; ilItem < olItemList.GetSize(); ilItem++)
			pomSelectedList->SetStyleRange(CGXRange(ilLc+1, ilItem+1, ilLc+1, ilItem+1), olStyle.SetValue(olItemList[ilItem]));
		pomSelectedList->SetStyleRange(CGXRange(ilLc+1, imHideColStart, ilLc+1, imHideColStart), olStyle.SetValue(olOrgText));

	}

	pomSelectedList->Redraw();
	pomPossilbeList->Redraw();

	bmIsInitialized = true;

	return TRUE;  // return TRUE  unless you set the focus to a control
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CWGRToolDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CWGRToolDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

void CWGRToolDlg::OnDelete() 
{
	// TODO: Add your control notification handler code here
	int ilIndex = m_GroupBox.GetCurSel();
	if (ilIndex < 0)
		return;

	if (AfxMessageBox(LoadStg(IDS_STRING428),MB_YESNO) == IDYES)
	{
		CString olGroup;
		m_GroupBox.GetLBText(ilIndex,olGroup);

		if (DeleteGroup(olGroup))
		{
			m_GroupBox.DeleteString(ilIndex);
			UpdateData(FALSE);
			m_GroupBox.SetCurSel(-1);
			m_DeleteGroup.EnableWindow(FALSE);
			m_SaveGroup.EnableWindow(FALSE);
			m_AddButton.EnableWindow(FALSE);
			m_RemoveButton.EnableWindow(FALSE);
		}
	}
}

void CWGRToolDlg::OnSelchangeGroupbox() 
{
	// TODO: Add your control notification handler code here
	int ilIndex = m_GroupBox.GetCurSel();
	if (ilIndex < 0)
		return;
	CString olALGR;
	m_GroupBox.GetLBText(ilIndex,olALGR);
	if (olALGR != omCurrentGroup)
	{
		UpdateData(TRUE);
		// check, if we must save current changes
		if (m_SaveGroup.IsWindowEnabled())
		{
			if (AfxMessageBox(LoadStg(IDS_STRING429),MB_YESNO) == IDYES)
			{
				SaveGroup(omCurrentGroup);	
			}
			else
			{
				ilIndex = m_GroupBox.FindStringExact(-1,omCurrentGroup);
				ASSERT(ilIndex >= 0);
				DeleteGroup(omCurrentGroup);
				m_GroupBox.DeleteString(ilIndex);
			}
		}

		// change to new group
		ChangeGroupTo(olALGR);
		UpdateData(FALSE);
		m_DeleteGroup.EnableWindow(TRUE);
		m_SaveGroup.EnableWindow(FALSE);
		m_AddButton.EnableWindow(TRUE);
		m_RemoveButton.EnableWindow(TRUE);
	}
}

void CWGRToolDlg::OnLocadd() 
{
	// TODO: Add your control notification handler code here
	CString olText;
	CGXStyle olStyle;
	CRowColArray olRows;

	// get count of selected rows
	int ilSelCount = (int)pomPossilbeList->GetSelectedRows( olRows);

	// anything selected ?
	if(ilSelCount > 0)
	{
		// enable modification
		pomPossilbeList->SetReadOnly(FALSE);
	    pomSelectedList->SetReadOnly(FALSE);

		int ilContCount = m_InsertList.GetCount();
		m_InsertList.ResetContent();
		for (int ilLc = 1 ; ilLc < max(ilContCount+1,(int)pomPossilbeList->GetRowCount()); ilLc++)
		{
			olText = pomPossilbeList->GetValueRowCol(ilLc, imHideColStart);
			m_InsertList.AddString(olText);
		}

		int ilDummy = m_InsertList.GetCount();
		

		CString olComplText;
		// Move selected items from left list box to right list box
		for ( ilLc = olRows.GetSize()-1;ilLc >= 0; ilLc--)
		{
			int ilDummy = (int)olRows[ilLc];
			olText = pomPossilbeList->GetValueRowCol(olRows[ilLc], 1);
			olComplText = pomPossilbeList->GetValueRowCol(olRows[ilLc], imHideColStart);
		
			if(ilDummy <= 0 || olComplText.IsEmpty())
				continue;
			int ilTest = m_InsertList.FindStringExact(-1, olComplText);
			if(ilTest >= 0)
			{
				m_ContentList.AddString(olComplText);	// move string from left to right box
				m_InsertList.DeleteString(ilTest);
				ilTest = m_AllList.FindStringExact(-1,olComplText);
				VERIFY(ilTest >= 0);
				m_AllList.DeleteString(ilTest);
			}
		}


		olStyle.SetEnabled(FALSE);
		olStyle.SetReadOnly(TRUE);
		
		int ilTopRow = pomPossilbeList->GetTopRow();

		pomPossilbeList->RemoveRows(1,  pomPossilbeList->GetRowCount());

		CStringArray olItemList;

		int ilCount = m_InsertList.GetCount() + 1;
		if (ilTopRow > ilCount)
			ilTopRow = ilCount;

		CString olOrgText;

		int ilRealCount = 0;
		pomPossilbeList->SetRowCount(max(16,ilCount));
		for ( ilLc = 0; ilLc < ilCount-1; ilLc++)
		{
			m_InsertList.GetText(ilLc,olText);
			olOrgText = olText;
			if(!olText.IsEmpty())
			{
				ilRealCount++;
				ExtractItemList(olText, &olItemList, '#');
				if(olItemList.GetSize() > 1)
				{
					olText = olItemList[1];
					pomPossilbeList->SetStyleRange(CGXRange(ilRealCount, imHideColStart+1, ilRealCount, imHideColStart+1), olStyle.SetValue(olItemList[0]));

				}
				ExtractItemList(olText, &olItemList, ';');
				for(int ilItem = 0 ; ilItem < olItemList.GetSize(); ilItem++)
					pomPossilbeList->SetStyleRange(CGXRange(ilRealCount, ilItem+1, ilRealCount, ilItem+1), olStyle.SetValue(olItemList[ilItem]));
				pomPossilbeList->SetStyleRange(CGXRange(ilRealCount, imHideColStart, ilRealCount, imHideColStart), olStyle.SetValue(olOrgText));
			}
		}

		pomPossilbeList->SortTable();
		pomPossilbeList->SetTopRow(ilTopRow);

		ilTopRow = pomSelectedList->GetTopRow();
		
		pomSelectedList->RemoveRows(1,  pomSelectedList->GetRowCount());
		ilCount = m_ContentList.GetCount()+ 1;
		if (ilTopRow > ilCount)
			ilTopRow = ilCount;

		ilRealCount = 0;
		pomSelectedList->SetRowCount(max(16,ilCount));
		for (ilLc = 0; ilLc < ilCount-1; ilLc++)
		{
			m_ContentList.GetText(ilLc,olText);
			olOrgText = olText;
			if(!olText.IsEmpty())
			{
				ilRealCount++;
				ExtractItemList(olText, &olItemList, '#');
				if(olItemList.GetSize() > 1)
				{
					olText = olItemList[1];
					pomSelectedList->SetStyleRange(CGXRange(ilRealCount, imHideColStart+1, ilRealCount, imHideColStart+1), olStyle.SetValue(olItemList[0]));

				}
				ExtractItemList(olText, &olItemList, ';');
				for(int ilItem = 0 ; ilItem < olItemList.GetSize(); ilItem++)
					pomSelectedList->SetStyleRange(CGXRange(ilRealCount, ilItem+1, ilRealCount, ilItem+1), olStyle.SetValue(olItemList[ilItem]));
				pomSelectedList->SetStyleRange(CGXRange(ilRealCount, imHideColStart, ilRealCount, imHideColStart), olStyle.SetValue(olOrgText));
			}
		}

		pomSelectedList->SortTable();
		pomSelectedList->SetTopRow(ilTopRow);

	    pomPossilbeList->SetReadOnly(TRUE);
	    pomSelectedList->SetReadOnly(TRUE);

		UpdateData(TRUE);
		m_SaveGroup.EnableWindow(SaveNeccessary());
	}
}

void CWGRToolDlg::OnLocremove() 
{
	// TODO: Add your control notification handler code here
	CString olText;
	CGXStyle olStyle;
	CRowColArray olRows;
	bool blFoundAlle = false;

	// Move selected items from right list box to left list box
	int ilSelCount = (int)pomSelectedList->GetSelectedRows( olRows);
	
	// Anything to do ?
	if(ilSelCount > 0)
	{
		// enable grid modification
	    pomPossilbeList->SetReadOnly(FALSE);
	    pomSelectedList->SetReadOnly(FALSE);

#pragma message ("The following line corrected due to prevent generating empty lines")
		int ilContCount = m_ContentList.GetCount();
		m_ContentList.ResetContent();
		for (int ilLc = 1; ilLc < max(ilContCount+1,(int)pomSelectedList->GetRowCount()); ilLc++)
		{
			olText = pomSelectedList->GetValueRowCol(ilLc, imHideColStart);
			m_ContentList.AddString(olText);
		}


		// Move selected items from left list box to right list box
		CString olComplText;
		bool blCheckFilter = false;
		for (ilLc = olRows.GetSize()-1;ilLc >= 0; ilLc--)
		{
			olText = pomSelectedList->GetValueRowCol(olRows[ilLc], 1);
			olComplText = pomSelectedList->GetValueRowCol(olRows[ilLc], imHideColStart);

			int ilDummy = (int)olRows[ilLc];
			if(ilDummy <= 0 || olComplText.IsEmpty())
				continue;

			int ilTest = m_ContentList.FindStringExact(-1, olComplText);
			if (ilTest >= 0)
			{
				blCheckFilter = true;
				m_ContentList.DeleteString(ilTest);
				ilTest = m_AllList.AddString(olComplText);
				VERIFY(ilTest >= 0);
			}
		}
	
		olStyle.SetEnabled(FALSE);
		olStyle.SetReadOnly(TRUE);
		
		pomSelectedList->RemoveRows(1,  pomSelectedList->GetRowCount());
		int ilCount = m_ContentList.GetCount();
		pomSelectedList->SetRowCount(max(16,ilCount));
		CString olOrgText;
		CStringArray olItemList;
		for (ilLc = 0; ilLc < ilCount; ilLc++)
		{
			m_ContentList.GetText(ilLc,olText);
			olOrgText = olText;
			ExtractItemList(olText, &olItemList, '#');
			if(olItemList.GetSize() > 1)
			{
				olText = olItemList[1];
				pomSelectedList->SetStyleRange(CGXRange(ilLc+1, imHideColStart+1, ilLc+1, imHideColStart+1), olStyle.SetValue(olItemList[0]));

			}
			ExtractItemList(olText, &olItemList, ';');
			for(int ilItem = 0 ; ilItem < olItemList.GetSize(); ilItem++)
				pomSelectedList->SetStyleRange(CGXRange(ilLc+1, ilItem+1, ilLc+1, ilItem+1), olStyle.SetValue(olItemList[ilItem]));
			pomSelectedList->SetStyleRange(CGXRange(ilLc+1, imHideColStart, ilLc+1, imHideColStart), olStyle.SetValue(olOrgText));

		}

		pomPossilbeList->SetReadOnly(TRUE);
	    pomSelectedList->SetReadOnly(TRUE);
		
		// store "selected" item list to internal member variables
		UpdateData(TRUE);
		// update "possible" item list
		CString olCurrentView = omViewer.SelectView();
		ChangeViewTo(olCurrentView);
		UpdateData(FALSE);
		// save to database enabled if neccessary
		m_SaveGroup.EnableWindow(SaveNeccessary());
	}
	
}

void CWGRToolDlg::OnNew() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
	// check, if we must save current changes
	if (m_SaveGroup.IsWindowEnabled())
	{
		if (AfxMessageBox(LoadStg(IDS_STRING429),MB_YESNO) == IDYES)
		{
			SaveGroup(omCurrentGroup);	
		}
		else
			DeleteGroup(omCurrentGroup);
	}

	CNewGroupDlg olDlg(this);
	if (olDlg.DoModal() == IDOK)
	{
		if (CreateGroup(olDlg.m_GroupDescription))
		{
			UpdateData(FALSE);
			m_SaveGroup.EnableWindow(FALSE);
			m_DeleteGroup.EnableWindow(TRUE);
			m_AddButton.EnableWindow(TRUE);
			m_RemoveButton.EnableWindow(TRUE);
		}
	}
}

void CWGRToolDlg::OnSave() 
{
	// TODO: Add your control notification handler code here
	int ilIndex = m_GroupBox.GetCurSel();
	if (ilIndex < 0)
		return;

	CString olGroup;
	m_GroupBox.GetLBText(ilIndex,olGroup);
	UpdateData(TRUE);
	if (SaveGroup(olGroup))
	{
		UpdateData(FALSE);
		m_SaveGroup.EnableWindow(FALSE);
	}
}

void CWGRToolDlg::OnView() 
{
	// TODO: Add your control notification handler code here
	CString olCaption = LoadStg(IDS_STRING424);
	FilterPropertySheet olDlg("WGRTool",olCaption,this,&omViewer);
	int ilState = olDlg.DoModal();
	if (ilState == IDOK)
	{
		CString olViewName = omViewer.SelectView();
		// save currently selected values
		UpdateData(TRUE);
		// change to new current view
		ChangeViewTo(olViewName);
		// update possible values controls
		UpdateData(FALSE);
	}
}

void CWGRToolDlg::OnSelchangeViewbox() 
{
	// TODO: Add your control notification handler code here
	CString olViewName;
    m_ViewBox.GetLBText(m_ViewBox.GetCurSel(),olViewName);

	// save currently selected values
	UpdateData(TRUE);
	// change to new current view
	ChangeViewTo(olViewName);
	// update possible values controls
	UpdateData(FALSE);
}

void CWGRToolDlg::OnOK() 
{
	// TODO: Add extra validation here
	UpdateData(TRUE);
	// check, if we must save current changes
	if (m_SaveGroup.IsWindowEnabled())
	{
		if (AfxMessageBox(LoadStg(IDS_STRING429),MB_YESNO) == IDYES)
		{
			SaveGroup(omCurrentGroup);	
		}
	}

	CDialog::OnOK();
}

bool CWGRToolDlg::ChangeViewTo(const CString& opViewName)
{
	if (!omViewer.SelectView(opViewName))
		return false;

	// get '*ALL' filter
	CString olAllFilter = LoadStg(IDS_STRING423);

	// evaluate current filters
	CStringArray olFilterArray;
	omViewer.GetFilter("ALO",olFilterArray);

	// check on initial '*ALL' filter
	for (int i = 0; i < olFilterArray.GetSize();i++)
	{
		if (olFilterArray[i].GetLength() && olFilterArray[i] == olAllFilter)
		{
			olFilterArray.RemoveAll();
			CAloFilterPage::GetAllFilters(olFilterArray);
			break;
		}
	}

	// initialize possible item count
	omPossibleItems.RemoveAll();

	// loop over all filters
	for (i = 0; i < olFilterArray.GetSize(); i++)
	{
		CString olFilter = olFilterArray[i];
		if (!olFilter.GetLength())
			continue;
		else
		{
			// extract table name && column name from this filter
			int ilIndex = olFilter.Find('.');
			if (ilIndex <= 0)	// should not occur	-> ignore
				continue;
			CString olTabName = olFilter.Left(ilIndex);
			CString olColName = olFilter.Right(olFilter.GetLength()-ilIndex-1);
			
			CString olFindName = ';';
			olFindName += olTabName;

			// loop over all items
			CString olCompleteName;
			for (int j = 0; j < m_AllList.GetCount();j++)
			{
				m_AllList.GetText(j,olCompleteName); 
				if (olCompleteName.Find(olFindName) >= 0)
					omPossibleItems.Add(olCompleteName);
			}

		}
	}
	return true;
	
}

bool CWGRToolDlg::CreateAllList()
{
	ASSERT(::IsWindow(m_AllList.m_hWnd));

	// get '*ALL' filter only once
	static CString olAllFilter = LoadStg(IDS_STRING423);

	// evaluate current filters
	CStringArray olFilterArray;
	omViewer.GetFilter("ALO",olFilterArray);

	// check on initial '*ALL' filter
	if (olFilterArray.GetSize() == 1 && olFilterArray[0] == olAllFilter)
	{
		olFilterArray.RemoveAll();
		CAloFilterPage::GetAllFilters(olFilterArray);
	}

	// initialize possible item count
	m_AllList.ResetContent();

	// loop over all filters
	for (int i = 0; i < olFilterArray.GetSize(); i++)
	{
		CString olFilter = olFilterArray[i];
		if (!olFilter.GetLength())
			continue;
		else
		{
			// extract table name && column name from this filter
			int ilIndex = olFilter.Find('.');
			if (ilIndex <= 0)	// should not occur	-> ignore
				continue;
			CString olTabName = olFilter.Left(ilIndex);
			CString olColName = olFilter.Right(olFilter.GetLength()-ilIndex-1);
			bool blOK = ogBCD.SetObject(olTabName,olColName);
			if (!blOK)
				continue;
			ogBCD.Read(olTabName);
			int ilSize = ogBCD.GetDataCount(olTabName);

			CString olALID;
			CString olURNO;
			CString olTmpText;
			for (int j = 0; j < ilSize; j++)
			{
				olALID = ogBCD.GetField(olTabName,j,olColName);
				if(!olALID.IsEmpty())
				{
					// check, if location has already been grouped previously
					olTmpText = olALID;
					olTmpText += ';';
					olTmpText +=olTabName;
					olTmpText += ';';

					int ilIndex = m_GroupedList.FindString(-1,olTmpText);
					if (ilIndex < 0)
					{
						olTmpText = olALID;
						olTmpText += "#";		
						olTmpText += olALID;
						olTmpText += ';';
						olTmpText += olTabName;
						int ilIndex = m_AllList.AddString(olTmpText);
						VERIFY(ilIndex >= 0);
					}
				}

			}
		}
	}
	return true;
}

bool CWGRToolDlg::CreateGroupedList()
{
	// necessary requirements
	ASSERT(::IsWindow(m_GroupBox.m_hWnd));
	ASSERT(::IsWindow(m_GroupedList.m_hWnd));

#ifdef	WGNTAB_SUPPORT

	// read logical group names from WGNTAB
	CString olALGR;
	int ilSize = ogBCD.GetDataCount("WGN");
	for (int ilC = 0; ilC < ilSize; ilC++)
	{
		olALGR = ogBCD.GetField("WGN",ilC,"ALGR");
		if (!olALGR.IsEmpty())
		{
			int ilIndex = m_GroupBox.AddString(olALGR);
			VERIFY(ilIndex >= 0);
		}
	}

	// read location assignments to logical groups from WGRTAB
	ilSize = ogBCD.GetDataCount("WGR");

	CString olALID;
	CString olALOC;
	CString olTmpText;
	for (ilC = 0; ilC < ilSize; ilC++)
	{
		olALID = ogBCD.GetField("WGR",ilC,"ALID");
		olALGR = ogBCD.GetField("WGR",ilC,"ALGR");
		olALOC = ogBCD.GetField("WGR",ilC,"ALOC");

		if(!olALID.IsEmpty() && !olALGR.IsEmpty() && !olALOC.IsEmpty())
		{
			olTmpText = olALID;
			olTmpText += ';';
			olTmpText += olALOC;
			olTmpText += ";";		
			olTmpText += olALGR;
			int ilIndex = m_GroupedList.AddString(olTmpText);
			VERIFY(ilIndex >= 0);
		}

		// check database integrity
		if (m_GroupBox.FindStringExact(-1,olALGR) < 0)
		{
			ASSERT(false);	// database integrity failure
			return false;
		}
	}
#else

	// read location assignments to logical groups from WGRTAB
	int ilSize = ogBCD.GetDataCount("WGR");

	CString olALID;
	CString olALGR;
	CString olALOC;
	CString olTmpText;
	for (int ilC = 0; ilC < ilSize; ilC++)
	{
		olALID = ogBCD.GetField("WGR",ilC,"ALID");
		olALGR = ogBCD.GetField("WGR",ilC,"ALGR");
		olALOC = ogBCD.GetField("WGR",ilC,"ALOC");

		if(!olALID.IsEmpty() && !olALGR.IsEmpty() && !olALOC.IsEmpty())
		{
			olTmpText = olALID;
			olTmpText += ';';
			olTmpText += olALOC;
			olTmpText += ";";		
			olTmpText += olALGR;
			int ilIndex = m_GroupedList.AddString(olTmpText);
			VERIFY(ilIndex >= 0);
		}

		// add group to group box, if not already done
		if (m_GroupBox.FindStringExact(-1,olALGR) < 0)
		{
			int ilIndex = m_GroupBox.AddString(olALGR);
			VERIFY(ilIndex >= 0);
		}
	}

#endif
	return true;
}

bool CWGRToolDlg::ChangeGroupTo(const CString& opGroupName)
{
	// initialize current selection list
	omSelectedItems.RemoveAll();

	CString		 olText;								
	CStringArray olItemList;

	for (int ilC = 0; ilC < m_GroupedList.GetCount(); ilC++)
	{
		m_GroupedList.GetText(ilC,olText);
		ExtractItemList(olText, &olItemList, ';');
		// compare group name
		if (olItemList[olItemList.GetSize()-1] == opGroupName)
		{
			olText = olItemList[0];
			olText+= '#';
			olText+= olItemList[0];
			olText+= ';';
			olText+= olItemList[1];
			omSelectedItems.Add(olText);
		}
	}

	omCurrentGroup = opGroupName;
	return true;
}

bool CWGRToolDlg::GetRecordsToInsert(const CString& opGroupName,CStringArray& opRecordsToInsert)
{
	CString		 olText;								
	CStringArray olItemList;
	CString		 olALID;
	CString		 olALOC;

	opRecordsToInsert.RemoveAll();
	// check on newly added items to this group
	for (int ilC = 0; ilC < omSelectedItems.GetSize(); ilC++)
	{
		olText = omSelectedItems[ilC];
		if (olText.IsEmpty())
			continue;

		ExtractItemList(olText, &olItemList, '#');
		olALID = olItemList[0];
		ExtractItemList(olText, &olItemList, ';');
		olALOC = olItemList[1];

		// check, if we must add a new item
		olText = olALID;
		olText+= ';';
		olText+= olALOC;
		olText+= ';';
		olText+= opGroupName;
		if (m_GroupedList.FindStringExact(-1,olText) < 0)														
		{
			int ilIndex = opRecordsToInsert.Add(olText);
		}
	}

	return opRecordsToInsert.GetSize() ? true : false;
}

bool CWGRToolDlg::GetRecordsToDelete(const CString& opGroupName,CStringArray& opRecordsToDelete)
{
	CString		 olText;								
	CStringArray olItemList;
	CString		 olALID;
	CString		 olALOC;

	opRecordsToDelete.RemoveAll();

	// check on items currently removed from this group
	for (int ilC = m_GroupedList.GetCount()-1; ilC >= 0 ; ilC--)
	{
		m_GroupedList.GetText(ilC,olText);				
		ExtractItemList(olText, &olItemList, ';');
		VERIFY(olItemList.GetSize() == 3);
		olALID = olItemList[0];
		olALOC = olItemList[1];

		// check, if we must remove an item
		if (opGroupName == olItemList[2])
		{
			olText = olALID;
			olText+= '#';
			olText+= olALID;
			olText+= ';';
			olText+= olALOC;
			if (m_ContentList.FindStringExact(-1,olText) < 0)
			{
				opRecordsToDelete.Add(olText);
			}
		}				
	}
	
	return opRecordsToDelete.GetSize() ? true:false;
}

bool CWGRToolDlg::SaveNeccessary()
{
	CStringArray olRecordsToInsert;
	CStringArray olRecordsToDelete;
	if (GetRecordsToInsert(omCurrentGroup,olRecordsToInsert) || GetRecordsToDelete(omCurrentGroup,olRecordsToDelete))
		return true;
	else
		return false;
}

// create group in database
bool CWGRToolDlg::CreateGroup(const CString& opGroupName)
{
#ifdef	WGNTAB_SUPPORT
	RecordSet rlRecord(ogBCD.GetFieldCount("WGN"));
	rlRecord.Values[imWGNPosALGR] = opGroupName;
	rlRecord.Values[imWGNPosHOPO] = ogHome;

	if (ogBCD.InsertRecord("WGN",rlRecord,true))
	{
		int ilIndex = m_GroupBox.AddString(opGroupName);
		VERIFY (ilIndex >= 0);
		ChangeGroupTo(opGroupName);
		m_GroupBox.SetCurSel(ilIndex);
		return true;
	}
	else
		return false;
#else
	int ilIndex = m_GroupBox.AddString(opGroupName);
	VERIFY (ilIndex >= 0);
	ChangeGroupTo(opGroupName);
	m_GroupBox.SetCurSel(ilIndex);
	return true;
#endif
}

// save group members in database
bool CWGRToolDlg::SaveGroup(const CString& opGroupName)
{
	CWaitCursor	 olWaitCursor;
	CString		 olText;								
	CStringArray olItemList;
	CString		 olALID;
	CString		 olALOC;

	// check on newly added items to this group
	for (int ilC = 0; ilC < omSelectedItems.GetSize(); ilC++)
	{
		olText = omSelectedItems[ilC];
		if (olText.IsEmpty())
			continue;

		ExtractItemList(olText, &olItemList, '#');
		olALID = olItemList[0];
		ExtractItemList(olText, &olItemList, ';');
		olALOC = olItemList[1];

		// check, if we must add a new item
		olText = olALID;
		olText+= ';';
		olText+= olALOC;
		olText+= ';';
		olText+= opGroupName;
		if (m_GroupedList.FindStringExact(-1,olText) < 0)														
		{
			RecordSet rlRecord(ogBCD.GetFieldCount("WGR"));
			rlRecord.Values[imPosALID] = olALID;
			rlRecord.Values[imPosALGR] = opGroupName;
			rlRecord.Values[imPosALOC] = olALOC;
			rlRecord.Values[imPosHOPO] = ogHome;

			if (ogBCD.InsertRecord("WGR",rlRecord))
			{
				int ilIndex = m_GroupedList.AddString(olText);
				VERIFY(ilIndex != LB_ERR);
			}
		}
	}

	// check on items currently removed from this group
	for (ilC = m_GroupedList.GetCount()-1; ilC >= 0 ; ilC--)
	{
		m_GroupedList.GetText(ilC,olText);				
		ExtractItemList(olText, &olItemList, ';');
		VERIFY(olItemList.GetSize() == 3);
		olALID = olItemList[0];
		olALOC = olItemList[1];

		// check, if we must remove an item
		if (opGroupName == olItemList[2])
		{
			olText = olALID;
			olText+= '#';
			olText+= olALID;
			olText+= ';';
			olText+= olALOC;
			if (m_ContentList.FindStringExact(-1,olText) < 0)
			{
				CString olURNO = ogBCD.GetFieldExt("WGR","ALID","ALOC",olALID,olALOC,"URNO");
				VERIFY(!olURNO.IsEmpty());
				if (ogBCD.DeleteRecord("WGR","URNO",olURNO))
				{
					 m_GroupedList.DeleteString(ilC);
				}
			}
		}				
	}

	return ogBCD.Save("WGR");
}

bool CWGRToolDlg::DeleteGroup(const CString& opGroupName)
{
	CWaitCursor	 olWaitCursor;
	CString		 olText;								
	CStringArray olItemList;
	CString		 olALID;
	CString		 olALOC;

	// check on items currently removed from this group
	bool blRecordsDeleted = false;
	for (int ilC = m_GroupedList.GetCount()-1; ilC >= 0 ; ilC--)
	{
		m_GroupedList.GetText(ilC,olText);				
		ExtractItemList(olText, &olItemList, ';');
		VERIFY(olItemList.GetSize() == 3);
		olALID = olItemList[0];
		olALOC = olItemList[1];

		// check, if we must remove an item
		if (opGroupName == olItemList[2])
		{
			CString olURNO = ogBCD.GetFieldExt("WGR","ALID","ALOC",olALID,olALOC,"URNO");
			VERIFY(!olURNO.IsEmpty());
			if (ogBCD.DeleteRecord("WGR","URNO",olURNO))
			{
				blRecordsDeleted = true;		
				m_GroupedList.DeleteString(ilC);
			}
			else
				return false;
		}
	}

#ifdef	WGNTAB_SUPPORT
	CString olURNO = ogBCD.GetField("WGN","ALGR",opGroupName,"URNO");
	VERIFY(!olURNO.IsEmpty());
	if (!ogBCD.DeleteRecord("WGN","URNO",olURNO,true))
		return false;
#endif

	// add selected items to all list
	CString olValue1;
	CString olValue2;
	for (int ilLc = 0; ilLc < (int)pomSelectedList->GetRowCount(); ilLc++)
	{
		// get the hiden value
		olValue1 = pomSelectedList->GetValueRowCol(ilLc, imHideColStart );
		olValue2 = pomSelectedList->GetValueRowCol(ilLc, imHideColStart + 1 );
		if(!olValue1.IsEmpty())
		{
			int ilTest = m_AllList.AddString(olValue1);
			VERIFY(ilTest >= 0);
		}

	}
	
	pomSelectedList->RemoveRows(1,pomSelectedList->GetRowCount());
	omSelectedItems.RemoveAll();

	int ilIndex = m_ViewBox.GetCurSel();
	if (ilIndex >= 0)
	{
		CString olView;
		m_ViewBox.GetLBText(ilIndex,olView);
		ChangeViewTo(olView);
	}

	return blRecordsDeleted ? ogBCD.Save("WGR") : true;
}

void CWGRToolDlg::OnHelp() 
{
	AfxGetApp()->WinHelp(0,HELP_CONTENTS);
}
void CWGRToolDlg::OnAbout() 
{
	// TODO: Add your control notification handler code here
	CAboutDlg olAboutDlg;
	olAboutDlg.DoModal();
	
}
