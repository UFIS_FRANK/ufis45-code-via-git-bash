#ifndef _FILTERPROPERTYSHEET_H_
#define _FILTERPROPERTYSHEET_H_


#include <BasePropertySheet.h>
#include <AloFilterPage.h>


/////////////////////////////////////////////////////////////////////////////
// CFilterPropertySheet

class FilterPropertySheet : public BasePropertySheet
{
// Construction
public:
	FilterPropertySheet(CString opCalledFrom,CString opCaption, CWnd* pParentWnd = NULL,
						 CViewer *popViewer = NULL, UINT iSelectPage = 0);

// Attributes
public:
	CAloFilterPage	m_PsFilterPage;

// Operations
public:
	virtual void LoadDataFromViewer();
	virtual void SaveDataToViewer(CString opViewName,BOOL bpSaveToDb = TRUE);
};

/////////////////////////////////////////////////////////////////////////////

#endif // _FILTERPROPERTYSHEET_H_
