#include <stdafx.h>
#include <CedaAloData.h>

CedaAloData::CedaAloData()
{                  
    // Create an array of CEDARECINFO for STAFFDATA
    BEGIN_CEDARECINFO(ALODATA, AloDataRecInfo)
		FIELD_CHAR_TRIM(Aloc, "ALOC")
		FIELD_CHAR_TRIM(Alod, "ALOD")	
    END_CEDARECINFO

    // Copy the record structure
    for (int i = 0; i < sizeof(AloDataRecInfo)/sizeof(AloDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&AloDataRecInfo[i],sizeof(CEDARECINFO));
        omRecInfo.Add(prpCedaRecInfo);
	}
    // Initialize table names and field names
	strcpy(pcmTableName,"ALO");
	strcat(pcmTableName,pcgTableExt);

	strcpy(pcmListOfFields,"ALOC,ALOD");

	pcmFieldList = pcmListOfFields; //because in CedaData pcmFieldList is just a pointer

}

CedaAloData::~CedaAloData()
{
}


bool CedaAloData::Read(char *pspWhere /*NULL*/)
{
	// Select data from the database
	if(pspWhere == NULL)
	{	
		if (CedaAction("RTA", "") == false)
		{
			return false;
		}
	}
	else
	{
		if (CedaAction("RT", pspWhere) == false)
		{
			return false;
		}
	}

    // Load data from CedaData into the dynamic array of record
	bool ilRc = true;
	for (int ilLc = 0; ilRc == true; ilLc++)
    {
		ALODATA *prlAlo = new ALODATA;
		if ((ilRc = GetFirstBufferRecord(prlAlo))==true) //GetBufferRecord(ilLc,prlSdt)) == true)
		{
			omData.Add(prlAlo);
		}
		else
		{
			delete prlAlo;
		}
	}
    return true;
}
