#ifndef	_CEDAALODATA_H_INCLUDED
#define	_CEDAALODATA_H_INCLUDED

#include <ccsglobl.h>
#include <CCSPtrArray.h>
#include <CCSCedaData.h>
#include <ccsddx.h>

struct ALODATA	{
	char	Aloc[12];
	char	Alod[34];

	ALODATA(void)
	{ 
		memset(this,'\0',sizeof(*this));
	}

};


class CedaAloData: public CCSCedaData
{
// Attributes
public:
    CCSPtrArray<ALODATA> omData;

	char pcmListOfFields[2048];

	void SetTableName(CString opTableName)
	{
		strcpy(pcmTableName, opTableName.GetBuffer(0));
	}

// Operations
public:
    CedaAloData();
	~CedaAloData();
	void Register(void);
	bool Read(char *pspWhere = NULL);
private:


};

#endif