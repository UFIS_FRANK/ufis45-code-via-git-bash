// CedaNatData.cpp - Class for Flight nature types
//

#include <afxwin.h>
#include <ccsglobl.h>
#include <CCSPtrArray.h>
#include <CCSCedaData.h>
#include <ccsddx.h>
#include <CCSBcHandle.h>
#include <ccsddx.h>
#include <CedaNatData.h>
#include <BasicData.h>

CedaNatData::CedaNatData()
{                  
    BEGIN_CEDARECINFO(NATDATA, NatDataRecInfo)
		FIELD_LONG(Urno,"URNO")
		FIELD_CHAR_TRIM(Ttyp,"TTYP")
		FIELD_CHAR_TRIM(Tnam,"TNAM")
    END_CEDARECINFO

    // Copy the record structure
    for (int i = 0; i < sizeof(NatDataRecInfo)/sizeof(NatDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&NatDataRecInfo[i],sizeof(CEDARECINFO));
        omRecInfo.Add(prpCedaRecInfo);
	}

    // Initialize table names and field names
    strcpy(pcmTableName,"NATTAB");
    pcmFieldList = "URNO,TTYP,TNAM";
}
 
CedaNatData::~CedaNatData()
{
	TRACE("CedaNatData::~CedaNatData called\n");
	ClearAll();
}

void CedaNatData::ClearAll()
{
	omUrnoMap.RemoveAll();
	omTtypMap.RemoveAll();
	omTnamMap.RemoveAll();
	omData.DeleteAll();
}

CCSReturnCode CedaNatData::ReadNatData()
{
	CCSReturnCode ilRc = RCSuccess;
	ClearAll();

	char pclCom[10] = "RT";
    char pclWhere[512];
    strcpy(pclWhere,"");
    if((ilRc = CedaAction2(pclCom, pclWhere)) != RCSuccess)
	{
		ogBasicData.LogCedaError("CedaNatData Error",omLastErrorMessage,pclCom,pcmFieldList,pcmTableName,pclWhere);
	}
	else
	{
		NATDATA rlNatData;
		for (int ilLc = 0; ilRc == RCSuccess; ilLc++)
		{
			if ((ilRc = GetBufferRecord2(ilLc,&rlNatData)) == RCSuccess)
			{
				AddNatInternal(rlNatData);
			}
		}
		ilRc = RCSuccess;
	}

	ogBasicData.Trace("CedaNatData Read %d Records (Rec Size %d) %s",omData.GetSize(), sizeof(NATDATA), pclWhere);
    return ilRc;
}


void CedaNatData::AddNatInternal(NATDATA &rrpNat)
{
	NATDATA *prlNat = new NATDATA;
	*prlNat = rrpNat;
	omData.Add(prlNat);
	omUrnoMap.SetAt((void *)prlNat->Urno,prlNat);
	omTtypMap.SetAt(prlNat->Ttyp,prlNat);

	omTnamMap.SetAt(prlNat->Tnam,prlNat);
}


NATDATA* CedaNatData::GetNatByUrno(long lpUrno)
{
	NATDATA *prlNat = NULL;
	omUrnoMap.Lookup((void *)lpUrno, (void *&) prlNat);
	return prlNat;
}

NATDATA* CedaNatData::GetNatByTtyp(const char *pcpTtyp)
{
	NATDATA *prlNat = NULL;
	if(strlen(pcpTtyp) > 0 && strcmp(pcpTtyp," "))
	{
		omTtypMap.Lookup(pcpTtyp, (void *&) prlNat);
	}
	return prlNat;
}

NATDATA* CedaNatData::GetNatByTnam(const char *pcpTnam)
{
	NATDATA *prlNat = NULL;
	if(strlen(pcpTnam) > 0 && strcmp(pcpTnam," "))
	{
		omTnamMap.Lookup(pcpTnam, (void *&) prlNat);
	}
	return prlNat;
}


CString CedaNatData::GetTableName(void)
{
	return CString(pcmTableName);
}

void CedaNatData::GetAllNatures(CStringArray& ropTypes)
{
	for (int i = 0; i < omData.GetSize(); i++)
	{
		ropTypes.Add(omData[i].Ttyp);
	}
}