// Class for Ccaes
#ifndef _CEDACCADATA_H_
#define _CEDACCADATA_H_

#include <CCSCedaData.h>
#include <CCSPtrArray.h>
#include <CedaDemandData.h>

/////////////////////////////////////////////////////////////////////////////
// Record structure declaration

struct CcaDataStruct
{
	long Urno;
	char Cnam[6];	// Checkin Counter Name
	char Ckit[2];	// Checkin terminal
	long Uaft;		// Urno in AFTTAB mapped from CCA.FLNU
	long Ualt;		// Urno in ALTTAB mapped from CCA.FLNU, when common checkin		
	long Udem;		// Urno in DEMTAB mapped from CCA.GHPU
	long Urue;		// Urno in RUETAB mapped from CCA.GHSU
	char Ctyp[2];	// flag for dedicated or common checkin 		
	CTime Ckbs;		// Begin Scheduled
	CTime Ckes;		// End Scheduled
	CTime Ckba;		// Begin Actual
	CTime Ckea;		// End Actual
    char  Usec[33];	// Creator
    CTime Cdat;		// Creation Date
    char  Useu[33];	// Updater
    CTime Lstu;		// Update Date

	// non DB data
	enum State
	{
		NEW,
		CHANGED,
		DELETED,
		UNCHANGED
	};

	State emState;	

	CcaDataStruct(void)
	{
		Urno	= 0L;
		Uaft	= 0L;
		Ualt	= 0L;
		Udem	= 0L;
		Urue	= 0L;
		memset(Cnam,0,sizeof(Cnam));
		memset(Ckit,0,sizeof(Ckit));
		memset(Ctyp,0,sizeof(Ctyp));
		Ckba	= TIMENULL;
		Ckea	= TIMENULL;
		Ckbs	= TIMENULL;
		Ckes	= TIMENULL;
		memset(Usec,0,sizeof(Usec));
		Cdat	= TIMENULL;
		memset(Useu,0,sizeof(Useu));
		Lstu	= TIMENULL;
		emState = NEW;
	}

	BOOL IsCommonCheckin() const
	{
		return stricmp(Ctyp,"C") == 0;
	}

	BOOL IsDedicatedCheckin() const
	{
		return stricmp(Ctyp,"C") != 0;
	}
};

typedef struct CcaDataStruct CCADATA;


/////////////////////////////////////////////////////////////////////////////
// Class declaration

class CedaCcaData: public CCSCedaData
{

// Attributes
public:
    CCSPtrArray <CCADATA> omData;
	CMapPtrToPtr omUrnoMap;
	CMapPtrToPtr omUaftMap;
	CMapPtrToPtr omCciMap;	
	CMapStringToPtr omCnamMap;
	CString GetTableName(void);
// Operations
public:
	CedaCcaData();
	~CedaCcaData();

	void ProcessCcaBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
	CCSReturnCode ReadCcaData(CTime,CTime);
	CCADATA* GetCcaByUrno(long lpUrno);
	void PrepareData(CCADATA *prpCca);
	void GetCcaListByCheckinCounter(CString opCnam, CCSPtrArray <CCADATA> &ropCcaList, bool bpReset=true);
	CCADATA *GetBestCcaForDemand(DEMANDDATA *prpDemand);
	bool CheckinCounterIsFree(CCADATA *prpCca);

	BOOL InitNewCcaRecord(const DEMANDDATA *prpDem,CCADATA& rrpData);
	BOOL AddCcaRecord(const CCADATA& rpData,BOOL bpSendDDX = TRUE);
	BOOL ChangeTime(long lpCca,const CTime& opNewStart,const CTime& opNewEnd);
	BOOL DeleteCcaRecord(long lpCca,BOOL IsFromBC = FALSE);

	BOOL FindCcaRecords(const DEMANDDATA *prpDem,CCSPtrArray<CCADATA>& ropCcaList);
	BOOL CreateCcaRecords(const DEMANDDATA *prpDem,CCSPtrArray<CCADATA>& ropCcaList);
	BOOL CcaCreatedByOpssPm(const CCADATA *prpCca);

	int GetCcaListByUaft(long lpUaft, CCSPtrArray <CCADATA> &ropCcas, bool bpReset = true);
	CString GetCounterRangeForFlight(long lpFlightUrno);
	CString Dump(long lpUrno);

private:
	CCADATA *AddCcaInternal(const CCADATA &rrpCca);
	void DeleteCcaInternal(long lpUrno);
	void AddCcaToCnamMap(CCADATA *prpCca);
	void DeleteCcaFromCnamMap(const char *pcpCnam, long lpCcaUrno);
	void AddCcaToUaftMap(CCADATA *prpCca);
	void AddCcaToCciMap(CCADATA *prpCca);
	void DeleteCcaFromUaftMap(long lpUaft, long lpCcaUrno);
	void DeleteCcaFromCciMap(long lpUaft);
	BOOL SaveCcaRecord(CCADATA *prpCca,CCADATA *prpOldCca = NULL);

	void ClearAll();
	void ConvertDatesToUtc(CCADATA *prpCca);
	void ConvertDatesToLocal(CCADATA *prpCca);
};

extern CedaCcaData ogCcaData;

#endif _CEDACCADATA_H_
