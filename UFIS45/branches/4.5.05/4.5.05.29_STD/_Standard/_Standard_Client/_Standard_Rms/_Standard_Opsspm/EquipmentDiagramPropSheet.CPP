// gatdiaps.cpp : implementation file
//

// These following include files are project dependent
#include <stdafx.h>
#include <OpssPm.h>
#include <CCSGlobl.h>
#include <CCSCedaData.h>
#include <AllocData.h>
#include <CedaEqtData.h>

// These following include files are necessary
#include <cviewer.h>
#include <BasicData.h>
#include <FilterPage.h>
#include <BasePropertySheet.h>
#include <EquipmentDiagramPropSheet.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


/////////////////////////////////////////////////////////////////////////////
// EquipmentDiagramPropertySheet
//

//EquipmentDiagramPropertySheet::EquipmentDiagramPropertySheet(CWnd* pParentWnd,
//	CViewer *popViewer, UINT iSelectPage) : BasePropertySheet
//	(ID_SHEET_GATE_DIAGRAM, pParentWnd, popViewer, iSelectPage)
EquipmentDiagramPropertySheet::EquipmentDiagramPropertySheet(CWnd* pParentWnd,
	CViewer *popViewer, UINT iSelectPage) : BasePropertySheet
	(GetString(IDS_EQ_CHARTVIEW), pParentWnd, popViewer, iSelectPage)
{
	AddPage(&m_pageGroup);
	AddPage(&m_pageEquipmentType);
	m_pageEquipmentType.bmSelectAllEnabled = true;
	AddPage(&m_pageEquipmentGroup);
	m_pageEquipmentGroup.bmSelectAllEnabled = true;
	AddPage(&m_pageEquipmentName);
	m_pageEquipmentName.bmSelectAllEnabled = true;

	// get all equipment types
	m_pageEquipmentType.SetCaption(GetString(IDS_EQTYPE));
	ogEqtData.GetEquipmentTypeNames(m_pageEquipmentType.omPossibleItems);

	// get all equipment names
	m_pageEquipmentName.SetCaption(GetString(IDS_EQUIPMENT_NAMES));
	ogEquData.GetEquipmentNames(m_pageEquipmentName.omPossibleItems);

	// get equipment static groups
	m_pageEquipmentGroup.SetCaption(GetString(IDS_EQSTATICGROUP));
	CCSPtrArray <ALLOCUNIT> olEquipmentGroups;
	ogAllocData.GetGroupsByGroupType(ALLOCUNITTYPE_EQUIPMENTGROUP,olEquipmentGroups);
	int ilNumEquipmentGroups = olEquipmentGroups.GetSize();
	for(int ilEquipmentGroup = 0; ilEquipmentGroup < ilNumEquipmentGroups; ilEquipmentGroup++)
	{
		ALLOCUNIT *prlEquipmentGroup = &olEquipmentGroups[ilEquipmentGroup];
		m_pageEquipmentGroup.omPossibleItems.Add(prlEquipmentGroup->Name);
	}
}

void EquipmentDiagramPropertySheet::LoadDataFromViewer()
{
	pomViewer->GetFilter("EquipmentGroup", m_pageEquipmentGroup.omSelectedItems);
	pomViewer->GetFilter("EquipmentType", m_pageEquipmentType.omSelectedItems);
	pomViewer->GetFilter("EquipmentName", m_pageEquipmentName.omSelectedItems);
	m_pageGroup.omGroupBy = pomViewer->GetGroup();
	CString olNotDisplayUnavailable = pomViewer->GetUserData("NOUNAVAILABLE");
	if (olNotDisplayUnavailable.IsEmpty())
		m_pageGroup.m_NotDisplayUnavailable = false;
	else
		m_pageGroup.m_NotDisplayUnavailable = olNotDisplayUnavailable == "YES" ? true : false;
}

void EquipmentDiagramPropertySheet::SaveDataToViewer(CString opViewName,BOOL bpSaveToDb)
{
	pomViewer->SetFilter("EquipmentGroup", m_pageEquipmentGroup.omSelectedItems);
	pomViewer->SetFilter("EquipmentType", m_pageEquipmentType.omSelectedItems);
	pomViewer->SetFilter("EquipmentName", m_pageEquipmentName.omSelectedItems);
	pomViewer->SetGroup(m_pageGroup.omGroupBy);
	pomViewer->SetUserData("NOUNAVAILABLE",m_pageGroup.m_NotDisplayUnavailable? "YES" : "NO");

	if (bpSaveToDb == TRUE)
	{
		pomViewer->SafeDataToDB(opViewName);
	}
}

int EquipmentDiagramPropertySheet::QueryForDiscardChanges()
{
	CStringArray olSelectedEquipmentGroups;
	pomViewer->GetFilter("EquipmentGroup", olSelectedEquipmentGroups);

	CStringArray olSelectedEquipmentTypes;
	pomViewer->GetFilter("EquipmentType", olSelectedEquipmentTypes);

	CStringArray olSelectedEquipmentNames;
	pomViewer->GetFilter("EquipmentName", olSelectedEquipmentNames);

	CString olGroupBy = pomViewer->GetGroup();

	if(!IsIdentical(olSelectedEquipmentGroups, m_pageEquipmentGroup.omSelectedItems) ||
		!IsIdentical(olSelectedEquipmentTypes, m_pageEquipmentType.omSelectedItems) ||
		!IsIdentical(olSelectedEquipmentNames, m_pageEquipmentName.omSelectedItems) ||
		olGroupBy != CString(m_pageGroup.omGroupBy))
	{
		// Discard changes ?
		return MessageBox(GetString(IDS_STRING61583), NULL, MB_ICONQUESTION | MB_OKCANCEL);
	}

	return IDOK;
}
