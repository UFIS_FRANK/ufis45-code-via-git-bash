// AllocData.cpp - Class Group Members (META)
//Test

#include <afxwin.h>
#include <ccsglobl.h>
#include <CCSPtrArray.h>
#include <ccsddx.h>
#include <AllocData.h>
#include <CedaAloData.h>
#include <CedaSgmData.h>
#include <CedaSgrData.h>
#include <CedaGatData.h>
#include <CedaPstData.h>
#include <CedaCicData.h>
#include <CedaValData.h>
#include <CedaAcrData.h>
#include <CedaPfcData.h>
#include <CedaEquData.h>


AllocData::AllocData()
{
}
 
AllocData::~AllocData()
{
	TRACE("AllocData::~AllocData called\n");
	ClearAll();
}

void AllocData::ClearAll()
{
	ClearAllocData(omAllocData);
	omGroupTypesMap.RemoveAll();
}

void AllocData::ClearAllocData(CCSPtrArray <ALLOCUNIT> &ropMembers)
{
	int ilNumMembers = ropMembers.GetSize();
	for(int ilMember = 0; ilMember < ilNumMembers; ilMember++)
	{
		ALLOCUNIT *prlMember = &ropMembers[ilMember];
		if(prlMember->Members.GetSize() > 0)
		{
			ClearAllocData(prlMember->Members);
		}
	}
	ropMembers.DeleteAll();
}



void AllocData::CreateAllocMap()
{
	ClearAll();

	// get alo records with ALOT='1' (1=Group/0=Unit)
	CCSPtrArray <ALODATA> olGroupTypes;
	ogAloData.GetAllGroupTypes(olGroupTypes);
	int ilNumAloTypes = olGroupTypes.GetSize();
	for(int ilAloType = 0; ilAloType < ilNumAloTypes; ilAloType++)
	{
		// create groups types eg "GATEAREA"
		ALLOCUNIT *prlGroupType = new ALLOCUNIT;

		ALODATA *prlAlo = &olGroupTypes[ilAloType];
		prlGroupType->Urno = prlAlo->Urno;
		strcpy(prlGroupType->Name,prlAlo->Aloc); // eg "GATEAREA"
		strcpy(prlGroupType->Desc,prlAlo->Alod); // eg "Gate Bereich"
		strcpy(prlGroupType->Tabn,"SGMTAB");
		prlGroupType->HasSubGroups = true;

		CCSPtrArray <SGRDATA> olSgrs;
		ogSgrData.GetSgrByUgty(prlAlo->Urno, olSgrs);
		int ilNumSgrs = olSgrs.GetSize();
		for(int ilSgr = 0; ilSgr < ilNumSgrs; ilSgr++)
		{
			CreateGroup(prlGroupType,&olSgrs[ilSgr],prlGroupType->Members);
		}

		omAllocData.Add(prlGroupType);
		omGroupTypesMap.SetAt(prlGroupType->Name,prlGroupType);
	}
}

// create groups eg. "A01-A03"
// the group will not be created if it has no members in SGMTAB, this
// can happen when the entries in SGRTAB are themselves units eg Pools ie. "POOLHALLE"
void AllocData::CreateGroup(ALLOCUNIT *prpGroupType,SGRDATA *prpSgr, CCSPtrArray <ALLOCUNIT> &ropMembers)
{
	ALLOCUNIT *prlGroup = new ALLOCUNIT;
	ALODATA *prlAloType = NULL;

	prlGroup->Urno = prpSgr->Urno;
	strcpy(prlGroup->Name,prpSgr->Grpn);
	strcpy(prlGroup->Desc,prpSgr->Grds);
	strcpy(prlGroup->Tabn,"SGRTAB");
	if((prlAloType = ogAloData.GetAloByUrno(prpSgr->Ugty)) != NULL)
	{
		strcpy(prlGroup->Type,prlAloType->Aloc);
	}

	ropMembers.Add(prlGroup);
	prpGroupType->GroupsMap.SetAt(prlGroup->Name,prlGroup);

	CCSPtrArray <SGMDATA> olUnits;
	ogSgmData.GetSgmListByUsgr(prpSgr->Urno, olUnits);
	int ilNumUnits = olUnits.GetSize();
	for(int ilUnit = 0; ilUnit < ilNumUnits; ilUnit++)
	{
		// members eg. "A01" or group of groups
		SGMDATA *prlSgm = &olUnits[ilUnit];
		bool blIsUnit = true;
		if(!strncmp(prlSgm->Tabn,"SGR",3))
		{
			// check for group of groups
			SGRDATA *prlSgr = ogSgrData.GetSgrByUrno(prlSgm->Uval);
			if(prlSgr != NULL)
			{
				prlAloType = ogAloData.GetAloByUrno(prlSgr->Ugty);
				if(prlAloType != NULL && *prlAloType->Alot == '1')
				{
					blIsUnit = false;
					prlGroup->HasSubGroups = true;
					CreateGroup(prpGroupType,prlSgr,prlGroup->Members);
				}
			}
		}
		
		if(blIsUnit)
		{
			// record created from GATTAB or CICTAB etc
			CreateUnit(prlSgm->Tabn,prlSgm->Uval,prlGroup->Members);
		}
	}
}

// create an allocation unit eg "A01"
ALLOCUNIT *AllocData::CreateUnit(const char *pcpTabn, long lpFurn, CCSPtrArray <ALLOCUNIT> &ropMembers)
{
	ALLOCUNIT *prlUnit = NULL;

	CString olUnitName;

	// currently a hardcoded list - future dynamic
	if(!strncmp(pcpTabn,ALLOCUNITTYPE_GATE,3))
	{
		GATDATA *prlGat = ogGatData.GetGatByUrno(lpFurn);
		if(prlGat != NULL)
		{
			prlUnit = new ALLOCUNIT;
			prlUnit->Urno = prlGat->Urno;
			strcpy(prlUnit->Name,prlGat->Gnam);
			prlUnit->Vafr = prlGat->Vafr;
			prlUnit->Vato = prlGat->Vato;
			prlUnit->Nafr = prlGat->Nafr;
			prlUnit->Nato = prlGat->Nato;
			strcpy(prlUnit->Type,ALLOCUNITTYPE_GATE);
		}
	}
	else if(!strncmp(pcpTabn,ALLOCUNITTYPE_PST,3))
	{
		PSTDATA *prlPst = ogPstData.GetPstByUrno(lpFurn);
		if(prlPst != NULL)
		{
			prlUnit = new ALLOCUNIT;
			prlUnit->Urno = prlPst->Urno;
			strcpy(prlUnit->Name,prlPst->Pnam);
			prlUnit->Vafr = prlPst->Vafr;
			prlUnit->Vato = prlPst->Vato;
			prlUnit->Nafr = prlPst->Nafr;
			prlUnit->Nato = prlPst->Nato;
			strcpy(prlUnit->Type,ALLOCUNITTYPE_PST);
		}
	}
	else if(!strncmp(pcpTabn,ALLOCUNITTYPE_EQUIPMENT,3))
	{
		EQUDATA *prlEqu = ogEquData.GetEquByUrno(lpFurn);
		if(prlEqu != NULL)
		{
			prlUnit = new ALLOCUNIT;
			prlUnit->Urno = prlEqu->Urno;
			strcpy(prlUnit->Name,prlEqu->Enam);
			prlUnit->Vafr = TIMENULL; // comes from VALTAB
			prlUnit->Vato = TIMENULL; // comes from VALTAB
			prlUnit->Nafr = TIMENULL; // comes from BLKTAB
			prlUnit->Nato = TIMENULL; // come from BLKTAB
			strcpy(prlUnit->Type,ALLOCUNITTYPE_EQUIPMENT);
		}
	}
	else if(!strncmp(pcpTabn,ALLOCUNITTYPE_CIC,3))
	{
		CICDATA *prlCic = ogCicData.GetCicByUrno(lpFurn);
		if(prlCic != NULL)
		{
			prlUnit = new ALLOCUNIT;
			prlUnit->Urno = prlCic->Urno;
			strcpy(prlUnit->Name,prlCic->Cnam);
			prlUnit->Vafr = prlCic->Vafr;
			prlUnit->Vato = prlCic->Vato;
			prlUnit->Nafr = prlCic->Nafr;
			prlUnit->Nato = prlCic->Nato;
			strcpy(prlUnit->Type,ALLOCUNITTYPE_CIC);
		}
	}
	else if(!strncmp(pcpTabn,"SGR",3))
	{
		// an example of a unit defined in SGRTAB is "POOLHALLE"
		SGRDATA *prlSgr = ogSgrData.GetSgrByUrno(lpFurn);
		if(prlSgr != NULL)
		{
			prlUnit = new ALLOCUNIT;
			prlUnit->Urno = prlSgr->Urno;
			strcpy(prlUnit->Name,prlSgr->Grpn);
//			prlUnit->Vafr = prlSgr->Vafr;
//			prlUnit->Vato = prlSgr->Vato;
//			prlUnit->Nafr = prlSgr->Nafr;
//			prlUnit->Nato = prlSgr->Nato;
		}
	}
	else if(!strncmp(pcpTabn,"ACR",3))
	{
		ACRDATA *prlAcr = ogAcrData.GetAcrByUrno(lpFurn);
		if(prlAcr != NULL)
		{
			prlUnit = new ALLOCUNIT;
			prlUnit->Urno = prlAcr->Urno;
			strcpy(prlUnit->Name,prlAcr->Regn);
			prlUnit->Vafr = prlAcr->Vafr;
			prlUnit->Vato = prlAcr->Vato;
			strcpy(prlUnit->Type,ALLOCUNITTYPE_REGN);
		}
	}
	else if(!strncmp(pcpTabn,ALLOCUNITTYPE_FUNCTION,3))
	{
		PFCDATA *prlPfc = ogPfcData.GetPfcByUrno(lpFurn);
		if(prlPfc != NULL)
		{
			prlUnit = new ALLOCUNIT;
			prlUnit->Urno = prlPfc->Urno;
			strcpy(prlUnit->Name,prlPfc->Fctc);
			prlUnit->Vafr = TIMENULL;
			prlUnit->Vato = TIMENULL;
			strcpy(prlUnit->Type,ALLOCUNITTYPE_FUNCTION);
		}
	}
	else
	{
//		Trace("**ERROR** AllocData::CreateUnit(): Invalid Resource Type Received: \"%s\"",pcpTabn);
	}

	if(prlUnit != NULL)
	{
		ropMembers.Add(prlUnit); // eg "A01"
	}

	return prlUnit;
}

//
// public GetGroupTypeByName()
// return a group type by its name eg:
// AllocData *prlGroupType = GetGroupTypeByName("GATEAREA");
//
ALLOCUNIT *AllocData::GetGroupTypeByName(const char *pcpGroupTypeName)
{
	ALLOCUNIT *prlGroupType = NULL;
	if(omGroupTypesMap.Lookup(pcpGroupTypeName,(void *&) prlGroupType))
	{
		if(!ogValData.IsCurrentlyValid(prlGroupType->Urno))
		{
			prlGroupType = NULL;
		}
	}

	return prlGroupType;
}

//
// public GetGroupByName()
// return a group by its name eg:
// ALLOCUNIT *prlGroup = GetGroupByName("GATEAREA","A01-A03");
//
ALLOCUNIT *AllocData::GetGroupByName(const char *pcpGroupTypeName, const char *pcpGroupName)
{
	ALLOCUNIT *prlGroup = NULL;
	ALLOCUNIT *prlGroupType = GetGroupTypeByName(pcpGroupTypeName);
	if(prlGroupType != NULL)
	{
		if(prlGroupType->GroupsMap.Lookup(pcpGroupName,(void *&) prlGroup))
		{
			if(!ogValData.IsCurrentlyValid(prlGroup->Urno))
			{
				prlGroup = NULL;
			}
		}
	}
	return prlGroup;
}


//
// public GetGroupsByGroupType()
// return a list of groups by their group type eg.
// GetGroupsByGroupType("GATEAREA",olGroupList);
// NOTE if bpGetSubUnits is true then search groups recursively (group of groups)
//
void AllocData::GetGroupsByGroupType(const char *pcpGroupTypeName,
									 CCSPtrArray <ALLOCUNIT> &ropGroupList,
									 bool bpReset /*true*/,bool bpGetSubGroups /*false*/)
{
	if(bpReset)
	{
		ropGroupList.RemoveAll();
	}
	ALLOCUNIT *prlGroupType = GetGroupTypeByName(pcpGroupTypeName);
	GetGroupsByGroupRecord(prlGroupType,ropGroupList,bpGetSubGroups);
}


void AllocData::GetGroupNamesByGroupType(const char *pcpGroupTypeName,CStringArray &ropGroupNames,bool bpReset /*true*/)
{
	if(bpReset)
	{
		ropGroupNames.RemoveAll();
	}
	CCSPtrArray <ALLOCUNIT> olGroupNames;
	ogAllocData.GetGroupsByGroupType(pcpGroupTypeName,olGroupNames);
	int ilNumGroups = olGroupNames.GetSize();
	for(int ilGroup = 0; ilGroup < ilNumGroups; ilGroup++)
	{
		ropGroupNames.Add(olGroupNames[ilGroup].Name);
	}
}

//
// private GetGroupsByGroupRecord()
// get a list of groups (recursively if bpGetSubGroups is true) for a given group of groups
//
void AllocData::GetGroupsByGroupRecord(ALLOCUNIT *prpGroupOfGroups,CCSPtrArray <ALLOCUNIT> &ropGroupList,bool bpGetSubGroups)
{
	if(prpGroupOfGroups != NULL)
	{
		int ilNumGroups = prpGroupOfGroups->Members.GetSize();
		for(int ilGroup = 0; ilGroup < ilNumGroups; ilGroup++)
		{
			ALLOCUNIT *prlGroup = &prpGroupOfGroups->Members[ilGroup];
			if(prlGroup->HasSubGroups && bpGetSubGroups)
			{
				GetGroupsByGroupRecord(prlGroup,ropGroupList,bpGetSubGroups); // search recursively
			}
			else
			{
				if(ogValData.IsCurrentlyValid(prlGroup->Urno))
				{
					ropGroupList.Add(prlGroup);
				}
			}
		}
	}
}


//
// GetGroupByUnitName()
// get the group for a given unit name eg.
// ALLOCUNIT *prlGroup = GetGroupByUnitName("GATEAREA","A01"); // returns the record for "A01-A03"
//
ALLOCUNIT *AllocData::GetGroupByUnitName(const char *pcpGroupTypeName, const char *pcpUnitName)
{
	ALLOCUNIT *prlGroup = NULL;
	ALLOCUNIT *prlGroupType = GetGroupTypeByName(pcpGroupTypeName);
	if(prlGroupType != NULL)
	{
		int ilNumGroups = prlGroupType->Members.GetSize();
		for(int ilGroup = 0; prlGroup == NULL && ilGroup < ilNumGroups; ilGroup++)
		{
			prlGroup = GetGroupByUnitName(&prlGroupType->Members[ilGroup],pcpUnitName);
		}
	}
	return prlGroup;
}

//
// GetGroupByUnitName() 2nd Method
// check if the given unit name is in the given group, if so return the group
//
ALLOCUNIT *AllocData::GetGroupByUnitName(ALLOCUNIT *prpGroup, const char *pcpUnitName)
{
	ALLOCUNIT *prlGroup = NULL;
	int ilNumUnits = prpGroup->Members.GetSize();
	for(int ilUnit = 0; prlGroup == NULL && ilUnit < ilNumUnits; ilUnit++)
	{
		ALLOCUNIT *prlUnit = &prpGroup->Members[ilUnit];
		if(prlUnit->HasSubGroups)
		{
			prlGroup = GetGroupByUnitName(prlUnit,pcpUnitName); // search recursively
		}
		else if(!strcmp(prlUnit->Name,pcpUnitName))
		{
			if(ogValData.IsCurrentlyValid(prpGroup->Urno))
			{
				prlGroup = prpGroup;
			}
		}
	}

	return prlGroup;
}


//
// public GetUnitsByGroup()
// get a list of allocation units for a givern group eg.
// GetUnitsByGroup("GATEAREA","A01-A03",olUnitList)
// NOTE if bpGetSubUnits is true then search groups recursively (group of groups)
//
void AllocData::GetUnitsByGroup(const char *pcpGroupTypeName,const char *pcpGroupName,
								CCSPtrArray <ALLOCUNIT> &ropUnitList,
								bool bpReset /*true*/,bool bpGetSubUnits /*false*/)
{
	if(bpReset)
	{
		ropUnitList.RemoveAll();
	}
	ALLOCUNIT *prlGroup = GetGroupByName(pcpGroupTypeName,pcpGroupName);
	if(prlGroup != NULL)
	{
		GetUnitsByGroupRecord(prlGroup,ropUnitList,bpGetSubUnits);
	}
}

//
// private GetUnitsByGroupRecord()
// get a list of groups units (recursively if bpGetSubGroups is true) for a given group
//
void AllocData::GetUnitsByGroupRecord(ALLOCUNIT *prpGroup,CCSPtrArray <ALLOCUNIT> &ropUnitList,bool bpGetSubUnits)
{
	if(prpGroup != NULL)
	{
		int ilNumUnits = prpGroup->Members.GetSize();
		for(int ilUnit = 0; ilUnit < ilNumUnits; ilUnit++)
		{
			ALLOCUNIT *prlUnit = &prpGroup->Members[ilUnit];
			if(prlUnit->HasSubGroups && bpGetSubUnits)
			{
				GetUnitsByGroupRecord(prlUnit,ropUnitList,bpGetSubUnits); // search recursively
			}
			else
			{
				if(ogValData.IsCurrentlyValid(prlUnit->Urno))
				{
					ropUnitList.Add(prlUnit);
				}
			}
		}
	}
}

//
// public GetUnitsByGroupType()
// return a list of all units for a given group type eg.
// GetUnitsByGroupType("GATEAREA",olUnitList);
// if bpGetSubUnits is true then search recursively group of groups
//
void AllocData::GetUnitsByGroupType(const char *pcpGroupTypeName,
									CCSPtrArray <ALLOCUNIT> &ropUnitList,
									bool bpReset /*true*/,bool bpGetSubUnits /*true*/)
{
	if(bpReset)
	{
		ropUnitList.RemoveAll();
	}

	ALLOCUNIT *prlGroupType = GetGroupTypeByName(pcpGroupTypeName);
	if(prlGroupType != NULL)
	{
		int ilNumGroups = prlGroupType->Members.GetSize();
		for(int ilGroup = 0; ilGroup < ilNumGroups; ilGroup++)
		{
			GetUnitsByGroupRecord(&prlGroupType->Members[ilGroup],ropUnitList,bpGetSubUnits);
		}
	}
}

