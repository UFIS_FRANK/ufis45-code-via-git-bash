// PstDetailViewer.cpp : implementation file
//

#include <stdafx.h>
#include <CCSGlobl.h>
#include <CedaEmpData.h>
#include <CedaShiftData.h>
#include <BasicData.h>
#include <ccsddx.h>
#include <PstDetailViewer.h>
#include <PrintControl.h>


#ifdef _DEBUG
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif

// Local function prototype
static void PstDetailCf(void *popInstance, int ipDDXType,void *vpDataPointer, CString &ropInstanceName);

/////////////////////////////////////////////////////////////////////////////
// PstDetailViewer
//

PstDetailViewer::PstDetailViewer()
{
    pomTable = NULL;
}

PstDetailViewer::~PstDetailViewer()
{
	ogCCSDdx.UnRegister(this, NOTUSED);
    DeleteAll();
}


void PstDetailViewer::Attach(CTable *popTable,CTime opStartTime,CTime opEndTime,CString opAlid)
{
    pomTable = popTable;
	omAlid = opAlid;
	omStartTime = opStartTime;
	omEndTime = opEndTime;
    ogCCSDdx.Register(this, FLIGHT_CHANGE, CString("PDVIEWER"), CString("Shift Change"), PstDetailCf);

}

void PstDetailViewer::ChangeView()
{

    DeleteAll();    
    MakeLines();

	UpdateDisplay();
}



BOOL PstDetailViewer::IsPassFilter(CTime opAcfr)
{
	BOOL blIsTimeOk = (omStartTime <= opAcfr && opAcfr <= omEndTime);

    return (blIsTimeOk);
}

/////////////////////////////////////////////////////////////////////////////
// PstDetailViewer -- code specific to this class

void PstDetailViewer::MakeLines()
{
	CCSPtrArray<FLIGHTDATA> olFlights;
	ogFlightData.GetFlightsByPosition(olFlights, omAlid);
    int ilFlightCount = olFlights.GetSize();
    for (int ilLc = 0; ilLc < ilFlightCount; ilLc++)
    {
        FLIGHTDATA *prlFlight = &olFlights[ilLc];
        MakeLine(prlFlight);
    }
}


void PstDetailViewer::MakeAssignments(long lpFlightUrno,PSTDETAIL_LINEDATA &rrpFlightLine)
{
	CCSPtrArray <JOBDATA> olJobs;
	ogJobData.GetJobsByFlur(olJobs,lpFlightUrno,FALSE, ogFlightData.GetPosition(lpFlightUrno),ALLOCUNITTYPE_PST); // don't like JOBFM);

    for ( int ilLc = 0; ilLc < olJobs.GetSize(); ilLc++ )
    {
        JOBDATA *prlJob = &olJobs[ilLc];
		
		EMPDATA *prlEmp = ogEmpData.GetEmpByPeno(prlJob->Peno);

		CString olAssignmentText = ogEmpData.GetEmpName(prlEmp);

	    // Create a new assignment
        PSTDETAIL_ASSIGNMENT rlAssignment;
        rlAssignment.Name = olAssignmentText;
		rlAssignment.Urno = prlJob->Urno;
        rlAssignment.Acfr = prlJob->Acfr;
        rlAssignment.Acto = prlJob->Acto;
        CreateAssignment(rrpFlightLine, &rlAssignment);
    }
}

void PstDetailViewer::MakeLine(FLIGHTDATA *prpFlight)
{

    // Update viewer data for this flight record
    PSTDETAIL_LINEDATA rlFlightLine;
	rlFlightLine.FlightTime = (ogFlightData.IsArrival(prpFlight) ? prpFlight->Stoa : prpFlight->Stod);
	
	if (IsPassFilter(rlFlightLine.FlightTime) == TRUE)
	{
		rlFlightLine.FlightUrno = prpFlight->Urno;
		rlFlightLine.Fnum = prpFlight->Fnum;
		rlFlightLine.Rou1 = (ogFlightData.IsArrival(prpFlight) ? prpFlight->Org3 : prpFlight->Des3);
		rlFlightLine.Iofl = prpFlight->Adid;

		MakeAssignments(prpFlight->Urno,rlFlightLine);
		CreateLine(&rlFlightLine);
	}
}

BOOL PstDetailViewer:: FindFlight(long lpFlightUrno, int &rilLineno)
{
    for (rilLineno = 0; rilLineno < omLines.GetSize(); rilLineno++)
	{
		long x = omLines[rilLineno].FlightUrno;
        if (omLines[rilLineno].FlightUrno == lpFlightUrno)
            return TRUE;
	}
    return FALSE;
}

int PstDetailViewer::CreateAssignment(PSTDETAIL_LINEDATA &rrpLine, PSTDETAIL_ASSIGNMENT *prpAssignment)
{
    int ilAssignmentCount = rrpLine.Assignment.GetSize();

// Generally, raw data will be sorted from left to right.
// So, scanning for the place of insertion backward is a little bit faster
//
    // Search for the position which we want to insert this new bar
    for (int ilAssignmentno = ilAssignmentCount; ilAssignmentno > 0; ilAssignmentno--)
        if (prpAssignment->Acfr >= rrpLine.Assignment[ilAssignmentno-1].Acfr)
            break;  // should be inserted after Lines[ilAssignmentno-1]

    rrpLine.Assignment.NewAt(ilAssignmentno, *prpAssignment);

    return ilAssignmentno;
}


/////////////////////////////////////////////////////////////////////////////
// PstDetailViewer - PSTDETAIL_LINEDATA array maintenance

void PstDetailViewer::DeleteAll()
{
    while (omLines.GetSize() > 0)
        DeleteLine(0);
}

int PstDetailViewer::CreateLine(PSTDETAIL_LINEDATA *prpFlightLine)
{
    int ilLineCount = omLines.GetSize();

    for (int ilLineno = 0; ilLineno < ilLineCount; ilLineno++)
    if (prpFlightLine->FlightTime <= omLines[ilLineno].FlightTime)
        break;  // should be inserted after Lines[ilLineno-1]

	PSTDETAIL_LINEDATA rlFlightLine;
	rlFlightLine = *prpFlightLine;
    omLines.NewAt(ilLineno, rlFlightLine);

    return ilLineno;
}

void PstDetailViewer::DeleteLine(int ipLineno)
{
	omLines.DeleteAt(ipLineno);
}


/////////////////////////////////////////////////////////////////////////////
// PstDetailViewer - display drawing routine

// UpdateDisplay: Load data selected by filter conditions to the display by using "omTable"
void PstDetailViewer::UpdateDisplay()
{
    //pomTable->SetHeaderFields("Flugnummer|Apt||Sta/d|Name|Name|Name|Name|Name|Name");
    pomTable->SetHeaderFields(GetString(IDS_STRING61568));
    pomTable->SetFormatList("10|4|1|4|22|22|22|22|22|22");


    // Load filtered and sorted data into the table content
	pomTable->ResetContent();
    for (int ilLc = 0; ilLc < omLines.GetSize(); ilLc++)
	{
		pomTable->AddTextLine(Format(&omLines[ilLc]), &omLines[ilLc]);
    }                                           

    // Update the table content in the display
    pomTable->DisplayTable();
}

CString PstDetailViewer::Format(PSTDETAIL_LINEDATA *prpLine)
{

	CString olText;
	olText = prpLine->Fnum;
	olText += "|" + prpLine->Rou1;
	olText += "|" + prpLine->Iofl;
	olText += "|" + prpLine->FlightTime.Format("%H%M");

    for (int ilLc = 0; ilLc < prpLine->Assignment.GetSize(); ilLc++)
    {
        PSTDETAIL_ASSIGNMENT *prlAssignment = &prpLine->Assignment[ilLc];
        olText += "|" + prlAssignment->Name;
    }
    return olText;
}


static void PstDetailCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName)
{
    PstDetailViewer *polViewer = (PstDetailViewer *)popInstance;

    if (ipDDXType == FLIGHT_CHANGE)
        polViewer->ProcessFlightChange((FLIGHTDATA *)vpDataPointer);

}


void PstDetailViewer::ProcessFlightChange(FLIGHTDATA *prpFlight)
{

	int ilItem;

	if (FindFlight(prpFlight->Urno,ilItem))
	{
        PSTDETAIL_LINEDATA *prlLine = &omLines[ilItem];

		CCSPtrArray <JOBDATA> olJobs;
		ogJobData.GetJobsByFlur(olJobs,prpFlight->Urno,FALSE, ogFlightData.GetPosition(prpFlight),ALLOCUNITTYPE_PST); // don't like JOBFM);

		// Update viewer data for this shift record
		prlLine->FlightUrno = prpFlight->Urno;
		prlLine->Fnum = prpFlight->Fnum;
		prlLine->Rou1 = (ogFlightData.IsArrival(prpFlight) ? prpFlight->Org3 : prpFlight->Des3);
		prlLine->Iofl = prpFlight->Adid;
		prlLine->FlightTime = (ogFlightData.IsArrival(prpFlight) ? prpFlight->Stoa : prpFlight->Stod);

		prlLine->Assignment.RemoveAll();
		MakeAssignments(prpFlight->Urno,*prlLine);

        pomTable->ChangeTextLine(ilItem, Format(&omLines[ilItem]), &omLines[ilItem]);
		pomTable->DisplayTable();
	}
	else
	{
		if (omAlid == ogFlightData.GetPosition(prpFlight))
		{
			MakeLine(prpFlight);
			if (FindFlight(prpFlight->Urno,ilItem))
			{
				pomTable->InsertTextLine(ilItem,Format(&omLines[ilItem]), &omLines[ilItem]);
				pomTable->DisplayTable();
			}
		}
	}
}

PSTDETAIL_LINEDATA *PstDetailViewer::GetLine(int ipLine)
{
	if (ipLine < 0 || ipLine > omLines.GetSize())
		return NULL;

	return &omLines[ipLine];
}

int PstDetailViewer::Lines() const
{
	return omLines.GetSize();
}





