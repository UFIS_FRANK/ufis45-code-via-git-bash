// CciDV.cpp : implementation file
//  

#include <stdafx.h>
#include <CCSGlobl.h>
#include <resource.h>
#include <CCSPtrArray.h>
#include <CCSCedaData.h>
#include <CCSDragDropCtrl.h>
#include <CedaJobData.h>
#include <CedaEmpData.h>
#include <CedaFlightData.h>
#include <CedaShiftData.h>
#include <gantt.h>
#include <gbar.h>
#include <CedaDemandData.h>
#include <CedaCcaData.h>
#include <conflict.h>
#include <ccsddx.h>
#include <cviewer.h>
#include <CciDetailWindow.h>
#include <AllocData.h>
#include <BasicData.h>
#include <StaffViewer.h>
#include <CciDetailViewer.h>
#include <CedaJodData.h>
#include <CedaAltData.h>
#include <DataSet.h>

#ifdef _DEBUG
#undef THIS_FILE
#define new DEBUG_NEW
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif

// General macros for testing a point against an interval
#define IsBetween(val, start, end)  ((start) <= (val) && (val) <= (end))
#define IsOverlapped(start1, end1, start2, end2)    ((start1) <= (end2) && (start2) <= (end1))
static int ByDebe(const DEMANDDATA **pppDem1, const DEMANDDATA **pppDem2);
static int ByDebe(const DEMANDDATA **pppDem1, const DEMANDDATA **pppDem2)
{
	return (int)((**pppDem1).Debe.GetTime() - (**pppDem2).Debe.GetTime());
}

static int ByCkba(const CCADATA **pppCca1, const CCADATA **pppCca2);
static int ByCkba(const CCADATA **pppCca1, const CCADATA **pppCca2)
{
	return (int)((**pppCca1).Ckba.GetTime() - (**pppCca2).Ckba.GetTime());
}


CciDetailViewer::CciDetailViewer()
{
	pomAttachWnd = NULL;
	bmIsFirstTime = TRUE;
	ogCCSDdx.Register((void *)this,JOB_CHANGE,CString("CciDETAIL"), CString("Job changed"),CciDetailCf);
	ogCCSDdx.Register((void *)this,JOB_DELETE,CString("CciDETAIL"), CString("Job new"),CciDetailCf);
	ogCCSDdx.Register((void *)this,JOB_NEW,CString("CciDETAIL"), CString("Fob delete"),CciDetailCf);

	omDedicatedCcaBkBrush.CreateSolidBrush(RGB(173,160,156));
	omCommonCcaBkBrush.CreateSolidBrush(GREEN);
	omDemandBrush.CreateSolidBrush(AQUA);

	CBitmap olBitmap;
	olBitmap.LoadBitmap(IDB_NOTAVAIL);
	omBlockedBrush.CreatePatternBrush(&olBitmap);
}

CciDetailViewer::~CciDetailViewer()
{
	RemoveAll();
	ogCCSDdx.UnRegister(this, NOTUSED);
}

void CciDetailViewer::RemoveAll()
{
	if (::IsWindow(pomAttachWnd->GetSafeHwnd()))
	{
		CciDetailGantt *polGantt = &((CciDetailWindow *)pomAttachWnd)->omGantt;
		if (::IsWindow(polGantt->GetSafeHwnd()))
		{
			polGantt->ResetContent();
		}
	}

	CCIDETAIL_LINEDATA *prlFL;
	for (int i = 0; i < omCciLines.GetSize(); i++)
	{
		prlFL = (CCIDETAIL_LINEDATA *)omCciLines[i];
		for (int j = 0; j < prlFL->Bars.GetSize(); j++)
		{
			CCIDETAIL_BARDATA *prlBar = (CCIDETAIL_BARDATA *)prlFL->Bars[j];
			delete prlBar;
		}
		prlFL->Bars.RemoveAll();
		for (j = 0; j < prlFL->BkBars.GetSize(); j++)
		{
			CCIDETAIL_BKBARDATA *prlBkBar = &prlFL->BkBars[j];
			delete prlBkBar;
		}
		prlFL->BkBars.RemoveAll();
		delete prlFL;
	}
	omCciLines.RemoveAll();
}

void CciDetailViewer::Attach(CWnd *popAttachWnd)
{
	pomAttachWnd = popAttachWnd;
}

static int CompareJobStartTime(const JOBDATA **e1, const JOBDATA **e2)
{
	return (int)((**e1).Acfr.GetTime() - (**e2).Acfr.GetTime());
}

void CciDetailViewer::Init(char *pcpAlocAlid, BOOL bpIsFirstTime)
{
	if (bpIsFirstTime)
	{
		bmIsFirstTime = FALSE;
		strcpy(cmAlocAlid, pcpAlocAlid);
	}

	RemoveAll();

	CCIDETAIL_LINEDATA *prlFL;

	omTimescaleStart = TIMENULL;
	omTimescaleEnd   = TIMENULL;

	// create background bars representing location demands ie checkin-counter demand
	if (ogCfgData.rmUserSetup.OPTIONS.CCA == USERSETUPDATA::NONE)
	{
		CCSPtrArray<DEMANDDATA> olCounterDemands;
		ogDemandData.GetDemandsByAlid(olCounterDemands,cmAlocAlid,ALLOCUNITTYPE_CIC,PERSONNELDEMANDS);
		olCounterDemands.Sort(ByDebe);
		for (int ilCD = 0; ilCD < olCounterDemands.GetSize(); ilCD++)
		{
			DEMANDDATA *prlDem = &olCounterDemands[ilCD];
			if (prlDem)
			{
				CCIDETAIL_BKBARDATA *prlDemBkBar = MakeDemBkBar(prlDem);
				if (prlDemBkBar)
				{
					CCIDETAIL_LINEDATA *prlLine = FindFreeSpaceOnLine(prlDem->Debe, prlDem->Deen);
					if(prlLine == NULL)
					{
						prlLine = new CCIDETAIL_LINEDATA;
						prlLine->Text = "";
						prlLine->MaxOverlapLevel = 0;
						omCciLines.Add(prlLine);
					}
					prlLine->BkBars.Add(prlDemBkBar);

					omTimescaleStart = (omTimescaleStart == TIMENULL || omTimescaleStart > prlDem->Debe) ? prlDem->Debe : omTimescaleStart;
					omTimescaleEnd   = (omTimescaleEnd == TIMENULL || omTimescaleEnd < prlDem->Deen) ? prlDem->Deen : omTimescaleEnd;
				}				
			}
		}
	}
	else
	{
		CCSPtrArray <CCADATA> olCounterJobs;
		ogCcaData.GetCcaListByCheckinCounter(cmAlocAlid, olCounterJobs);
		olCounterJobs.Sort(ByCkba);
		int ilNumCounterJobs = olCounterJobs.GetSize();
		for(int ilCJ = 0; ilCJ < ilNumCounterJobs; ilCJ++)
		{
			CCADATA *prlCca = &olCounterJobs[ilCJ];

			CCIDETAIL_BKBARDATA *prlCcaBkBar = MakeCcaBkBar(prlCca);
			if(prlCcaBkBar != NULL)
			{
				omTimescaleStart = (omTimescaleStart == TIMENULL || omTimescaleStart > prlCca->Ckba) ? prlCca->Ckba : omTimescaleStart;
				omTimescaleEnd = (omTimescaleEnd == TIMENULL || omTimescaleEnd < prlCca->Ckea) ? prlCca->Ckea : omTimescaleEnd;

				CCIDETAIL_LINEDATA *prlLine = FindFreeSpaceOnLine(prlCca->Ckba, prlCca->Ckea);
				if(prlLine == NULL)
				{
					prlLine = new CCIDETAIL_LINEDATA;
					prlLine->Text = "";
					prlLine->MaxOverlapLevel = 0;
					omCciLines.Add(prlLine);
				}
				prlLine->BkBars.Add(prlCcaBkBar);
			}
		}

		CCSPtrArray<DEMANDDATA> olCounterDemands;
		ogDemandData.GetDemandsByAlid(olCounterDemands,cmAlocAlid,ALLOCUNITTYPE_CIC,PERSONNELDEMANDS);
		olCounterDemands.Sort(ByDebe);
		for (int ilCD = 0; ilCD < olCounterDemands.GetSize(); ilCD++)
		{
			DEMANDDATA *prlDem = &olCounterDemands[ilCD];
			if (prlDem)
			{
				CCIDETAIL_BKBARDATA *prlDemBkBar = MakeDemBkBar(prlDem);
				if (prlDemBkBar)
				{
					CCIDETAIL_LINEDATA *prlLine = FindFreeSpaceOnLine(prlDem->Debe, prlDem->Deen);
					if(prlLine == NULL)
					{
						prlLine = new CCIDETAIL_LINEDATA;
						prlLine->Text = "";
						prlLine->MaxOverlapLevel = 0;
						omCciLines.Add(prlLine);
					}
					prlLine->BkBars.Add(prlDemBkBar);
					omTimescaleStart = (omTimescaleStart == TIMENULL || omTimescaleStart > prlDem->Debe) ? prlDem->Debe : omTimescaleStart;
					omTimescaleEnd   = (omTimescaleEnd == TIMENULL || omTimescaleEnd < prlDem->Deen) ? prlDem->Deen : omTimescaleEnd;
				}				
			}
		}
	}

	ogJobData.GetJobsByAlid(omJobs, cmAlocAlid, ALLOCUNITTYPE_CIC);
	omJobs.Sort(CompareJobStartTime);

	char pclText[255];
	CCIDETAIL_BARDATA *prlBar;
	
	for (int j = 0; j < omJobs.GetSize(); j++)
	{
		SHIFTDATA *prlShift = NULL;

		JOBDATA olJob = omJobs[j];

		omTimescaleStart = (omTimescaleStart == TIMENULL || omTimescaleStart > olJob.Acfr) ? olJob.Acfr : omTimescaleStart;
		omTimescaleEnd = (omTimescaleEnd == TIMENULL || omTimescaleEnd < olJob.Acto) ? olJob.Acto : omTimescaleEnd;


		EMPDATA *prlEmp = ogEmpData.GetEmpByPeno(olJob.Peno);
		JOBDATA *prlPoolJob = ogJobData.GetJobByUrno(olJob.Jour);
		if (prlPoolJob != NULL)
			prlShift = ogShiftData.GetShiftByUrno(prlPoolJob->Shur);
		sprintf(pclText, "%s,%s,%s",
				prlShift != NULL ? prlShift->Egrp : "",
				prlShift != NULL ? prlShift->Sfca : "",
				prlEmp != NULL ? ogEmpData.GetEmpName(prlEmp) : GetString(IDS_STRING61335));

		// Pichate CreateBar
		prlBar = new CCIDETAIL_BARDATA;
		prlBar->CciUrno = ogBasicData.GetFlightUrnoForJob(&olJob);
		prlBar->Urno = olJob.Urno;
		CString Acfr = olJob.Acfr.Format("%H%M");
		CString Acto = olJob.Acto.Format("%H%M");
		prlBar->Text = pclText;
		sprintf(pclText, "%s - %s %s", Acfr, Acto, prlBar->Text);
		prlBar->StatusText = pclText;
		prlBar->StartTime = olJob.Acfr;
		prlBar->EndTime = olJob.Acto;
		prlBar->FrameType = FRAMERECT;
		prlBar->MarkerType = (*olJob.Stat == 'P')? MARKLEFT: MARKFULL;
		//prlBar->MarkerBrush = ogBrushs[olJob.ColorIndex];
		prlBar->MarkerBrush = ogBrushs[ogConflicts.GetJobConflictColor(&olJob)];
		prlBar->OverlapLevel = 0;			// zero for the top-most level, each level lowered by 4 pixels
		//prlBar->Indicators.RemoveAll();

		if((prlFL = FindDemandLineForJob(olJob.Urno)) != NULL)
		{
			prlFL->Bars.Add(prlBar);
		}
		else
		{
			prlFL = new CCIDETAIL_LINEDATA;
			prlFL->Text = "";
			prlFL->MaxOverlapLevel = 0;
			prlFL->Bars.Add(prlBar);
			omCciLines.Add(prlFL);
			if (::IsWindow(pomAttachWnd->GetSafeHwnd()))
			{
				CciDetailGantt *polGantt = &((CciDetailWindow *)pomAttachWnd)->omGantt;
				if (::IsWindow(polGantt->GetSafeHwnd()))
				{
					polGantt->AddString("");
					polGantt->SetItemHeight(j, polGantt->GetLineHeight(j));
				}
			}
		}
	}

	for (;j < 8; j++)
	{
		prlFL = new CCIDETAIL_LINEDATA;
		prlFL->Text = "";
		prlFL->MaxOverlapLevel = 0;
		
		omCciLines.Add(prlFL);
		if (::IsWindow(pomAttachWnd->GetSafeHwnd()))
		{
			CciDetailGantt *polGantt = &((CciDetailWindow *)pomAttachWnd)->omGantt;
			if (::IsWindow(polGantt->GetSafeHwnd()))
			{
				polGantt->AddString("");
		        polGantt->SetItemHeight(j, polGantt->GetLineHeight(j));
			}
		}
	}	


	if(omTimescaleStart == TIMENULL)
	{
		omTimescaleStart = ogBasicData.GetTime() - CTimeSpan(0, 1, 0, 0);
	}
	else
	{
		omTimescaleStart = omTimescaleStart - CTimeSpan(0, 0, 30, 0);
	}
	if(omTimescaleEnd == TIMENULL)
	{
		omTimescaleEnd = omTimescaleStart + CTimeSpan(0, 4, 0, 0);
	}
	else
	{
		omTimescaleEnd = omTimescaleEnd + CTimeSpan(0, 0, 30, 0);
	}

}

CCIDETAIL_LINEDATA *CciDetailViewer::FindDemandLineForJob(long lpJobUrno)
{
	DEMANDDATA *prlDemand = ogJodData.GetDemandForJob(lpJobUrno);
	if(prlDemand != NULL)
	{
		int ilNumLines = omCciLines.GetSize();
		for(int ilLine = 0; ilLine < ilNumLines; ilLine++)
		{
			CCIDETAIL_LINEDATA *prlLine = (CCIDETAIL_LINEDATA *)(omCciLines.GetAt(ilLine));

			int ilNumBkBars = prlLine->BkBars.GetSize();
			for(int ilBkBar = 0; ilBkBar < ilNumBkBars; ilBkBar++)
			{
				if(prlLine->BkBars[ilBkBar].Urno == prlDemand->Urno)
				{
					return prlLine;
				}
			}
		}
	}

	return NULL;
}

CCIDETAIL_LINEDATA *CciDetailViewer::FindFreeSpaceOnLine(CTime opStart, CTime opEnd)
{
	CCIDETAIL_LINEDATA *prlLine = NULL;

	int ilNumLines = omCciLines.GetSize();
	for(int ilLine = 0; ilLine < ilNumLines; ilLine++)
	{
		prlLine = (CCIDETAIL_LINEDATA *)(omCciLines.GetAt(ilLine));
		bool blOverlap = false;

		int ilNumBkBars = prlLine->BkBars.GetSize();
		for(int ilBkBar = 0; ilBkBar < ilNumBkBars; ilBkBar++)
		{
			if(IsNearlyOverlapped(prlLine->BkBars[ilBkBar].StartTime, prlLine->BkBars[ilBkBar].EndTime, opStart, opEnd))
			{
				blOverlap = true;
			}
		}
		if(!blOverlap)
		{
			return prlLine;
		}
	}
	return NULL;
}

bool CciDetailViewer::IsNearlyOverlapped(CTime opStart1, CTime opEnd1, CTime opStart2, CTime opEnd2)
{
	CTimeSpan ol2Mins(0, 0, 2, 0);
	return ((opStart1-ol2Mins) <= opEnd2 && opStart2 <= (opEnd1+ol2Mins)) ? true : false;
}

void CciDetailViewer::MakeDemBkBarsForCcaBkBar(CCIDETAIL_BKBARDATA *prpCcaBkBar)
{
	CCIDETAIL_BKBARDATA *prlBkBar = NULL;
	if(prpCcaBkBar != NULL)
	{
		// create each personnel demand - link them to the location demand
		CCSPtrArray <DEMANDDATA> olPersonnelDemands;
		ogDemandData.GetDemandsByFlurAndUrue(olPersonnelDemands,prpCcaBkBar->Uaft,prpCcaBkBar->Urue,"",ALLOCUNITTYPE_CIC,PERSONNELDEMANDS);
		int ilNumDems = olPersonnelDemands.GetSize(), ilDem;

		// search for a personnel demand with the same counter as the CCA
		// (there can be several personnel demands for a singe counter)
		for(ilDem = 0; ilDem < ilNumDems; ilDem++)
		{
			DEMANDDATA *prlDem = &olPersonnelDemands[ilDem];
			if((prlDem->Ulnk != 0L && prpCcaBkBar->Ulnk == prlDem->Ulnk) || !strcmp(cmAlocAlid,prlDem->Alid))
			{
				omTimescaleStart = (omTimescaleStart == TIMENULL || omTimescaleStart > prlDem->Debe) ? prlDem->Debe : omTimescaleStart;
				omTimescaleEnd = (omTimescaleEnd == TIMENULL || omTimescaleEnd < prlDem->Deen) ? prlDem->Deen: omTimescaleEnd;

				// direct link
				prlBkBar = new CCIDETAIL_BKBARDATA;

				CString olText, olStatusText;
				FLIGHTDATA *prlFlight = ogDataSet.GetFlightByDemand(prlDem);
				if(prlFlight != NULL)
				{
					olText = CString(prlFlight->Fnum);
				}
				else
				{
					olText = GetString(IDS_STRING61387); // Flight not found
				}

				olText += CString(" ") + ogBasicData.GetDemandBarText(prlDem);
				olStatusText.Format("%s - %s  %s",prlDem->Debe.Format("%d/%H%M"),prlDem->Deen.Format("%d/%H%M"),olText);
				
				prlBkBar->Text = olText;
				prlBkBar->StatusText  = olStatusText;
				prlBkBar->Uaft = prpCcaBkBar->Uaft;
				prlBkBar->Urno = prlDem->Urno;
				prlBkBar->Urue = prpCcaBkBar->Urue;
				prlBkBar->Ulnk = prpCcaBkBar->Ulnk;
				prlBkBar->StartTime = prlDem->Debe;
				prlBkBar->EndTime = prlDem->Deen;
				prlBkBar->MarkerBrush = &omDemandBrush;

				CCIDETAIL_LINEDATA *prlLine;
				prlLine = new CCIDETAIL_LINEDATA;
				prlLine->Text = "";
				prlLine->MaxOverlapLevel = 0;
				prlLine->BkBars.Add(prlBkBar);
				omCciLines.Add(prlLine);
			}
		}
	}
}

CCIDETAIL_BKBARDATA *CciDetailViewer::MakeCcaBkBar(CCADATA *prlCca)
{
	CCIDETAIL_BKBARDATA *prlBkBar = NULL;
	if (prlCca->IsDedicatedCheckin())
	{
		FLIGHTDATA *prlFlight = ogFlightData.GetFlightByUrno(prlCca->Uaft);
		if(prlFlight != NULL)
		{
			prlBkBar = new CCIDETAIL_BKBARDATA;
			//prlBkBar->Text.Format("%s",prlFlight->Fnum);
			prlBkBar->Text.Format("%s",prlFlight->Fnum);
			prlBkBar->StatusText.Format("%s -%s  %s CCA.URNO <%ld> RUE.URNO <%ld>",prlCca->Ckba.Format("%d/%H%M"),prlCca->Ckea.Format("%d/%H%M"),prlFlight->Fnum,prlCca->Urno,prlCca->Urue);
			prlBkBar->Uaft = prlCca->Uaft;
			prlBkBar->Urno = prlCca->Urno;
			prlBkBar->Urue = prlCca->Urue;
			DEMANDDATA *prlLocationDemand = ogDemandData.GetDemandByUrno(prlCca->Udem);
			if(prlLocationDemand == NULL)
			{
				//ogBasicData.Trace("CciDiagramViewer::MakeCcaBkBar() CCA record has invalid pointer to a demand! COUNTER <%s> FROM <%s> TO <%s> CCA.URNO <%ld> DEM.URNO <%ld>\n",prlCca->Cnam,prlCca->Ckbs.Format("%H:%M/%d"),prlCca->Ckes.Format("%H:%M/%d"),prlCca->Urno,prlCca->Udem);
				prlBkBar->Ulnk = 0;
			}
			else
			{
				prlBkBar->Ulnk = prlLocationDemand->Ulnk;
			}
			prlBkBar->StartTime = prlCca->Ckba;
			prlBkBar->EndTime = prlCca->Ckea;
			prlBkBar->MarkerBrush = &omDedicatedCcaBkBrush;
		}
	}
	else
	{
		prlBkBar = new CCIDETAIL_BKBARDATA;
		prlBkBar->Text.Format("CCI");
		if (prlCca->Uaft)
		{
			ALTDATA *prlAlt = ogAltData.GetAltByUrno(prlCca->Ualt);
			if (prlAlt)
				prlBkBar->Text.Format("CCI %s",prlAlt->Alc2);
		}
		prlBkBar->StatusText.Format("%s -%s  CCA.URNO <%ld> RUE.URNO <%ld>",prlCca->Ckba.Format("%d/%H%M"),prlCca->Ckea.Format("%d/%H%M"),prlCca->Urno,prlCca->Urue);
		prlBkBar->Uaft = prlCca->Uaft;
		prlBkBar->Urno = prlCca->Urno;
		prlBkBar->Urue = prlCca->Urue;
		prlBkBar->Ulnk = 0;
		prlBkBar->StartTime = prlCca->Ckba;
		prlBkBar->EndTime = prlCca->Ckea;
		prlBkBar->MarkerBrush = &omCommonCcaBkBrush;
	}

	return prlBkBar;
}

CCIDETAIL_BKBARDATA *CciDetailViewer::MakeDemBkBar(DEMANDDATA *prpDem)
{
	CCIDETAIL_BKBARDATA *prlBkBar = NULL;

	if (!prpDem)
		return prlBkBar;

	CString olText, olStatusText;
	long llFlightUrno = 0L;

	// common checkin demand ?
	if (prpDem->Dety[0] == '6')
	{

	}
	else
	{
		FLIGHTDATA *prlFlight = ogDataSet.GetFlightByDemand(prpDem);
		if(prlFlight != NULL)
		{
			olText = CString(prlFlight->Fnum);
			llFlightUrno = prlFlight->Urno;
		}
		else
		{
			olText = ogDemandData.GetStringForDemandType(prpDem->Dety);
		}
	}

	olText += CString(" ") + ogBasicData.GetDemandBarText(prpDem);
	olStatusText.Format("%s - %s  %s DEM.URNO <%ld> RUD.URNO <%ld>",prpDem->Debe.Format("%d/%H%M"),prpDem->Deen.Format("%d/%H%M"),olText,prpDem->Urno,prpDem->Urud);

	prlBkBar = new CCIDETAIL_BKBARDATA;
	prlBkBar->Type = CCIDETAIL_BKBARDATA::PERSONNEL;
	strcpy(prlBkBar->Dety,prpDem->Dety);
	prlBkBar->Text = olText;
	prlBkBar->StatusText = olStatusText;
	prlBkBar->Uaft = llFlightUrno;
	prlBkBar->Urno = prpDem->Urno;
	prlBkBar->Urue = 0L;
	prlBkBar->StartTime = prpDem->Debe;
	prlBkBar->EndTime = prpDem->Deen;
	prlBkBar->MarkerBrush = &omDemandBrush;

	return prlBkBar;
}


int CciDetailViewer::GetLineCount(int ipGroupno)
{
	return omCciLines.GetSize();
}

CCIDETAIL_LINEDATA *CciDetailViewer::GetLine(int ipGroupno, int ipLineno)
{
	CCIDETAIL_LINEDATA *prlFL;
	prlFL = (CCIDETAIL_LINEDATA *) (omCciLines.GetAt(ipLineno));
	return prlFL;
}

CString CciDetailViewer::GetLineText(int ipGroupno, int ipLineno)
{
	static char clBuf[255];
	sprintf(clBuf, "[%d:%d]", ipGroupno, ipLineno);
	
	return CString(clBuf);
}

int CciDetailViewer::CreateBar(int ipGroupno, int ipLineno, CCIDETAIL_BARDATA *prpBar, BOOL bpFrontBar)
{
	CCIDETAIL_LINEDATA *prlFL;
	prlFL = (CCIDETAIL_LINEDATA *) (omCciLines.GetAt(ipLineno));
	CCIDETAIL_BARDATA *prlBar;

	int ilBarCount = prlFL->Bars.GetSize();

    // Search for the position which we want to insert this new bar
    for (int ilBarno = ilBarCount; ilBarno > 0; ilBarno--)
        if (prpBar->StartTime >= GetBar(ipGroupno,ipLineno,ilBarno-1)->StartTime)
            break;  // should be inserted after Bars[ilBarno-1]

	// Pichate CreateBar
	prlBar = new CCIDETAIL_BARDATA;
	prlBar->CciUrno = prpBar->CciUrno;
	prlBar->Urno = prpBar->Urno;
	prlBar->Text = prpBar->Text;
	prlBar->StatusText = prpBar->StatusText;
	prlBar->StartTime = prpBar->StartTime;
	prlBar->EndTime = prpBar->EndTime;
	prlBar->FrameType = prpBar->FrameType;
	prlBar->MarkerType = prpBar->MarkerType;
	prlBar->MarkerBrush = prpBar->MarkerBrush;
	prlBar->OverlapLevel = prpBar->OverlapLevel;			// zero for the top-most level, each level lowered by 4 pixels
	//prlBar->Indicators = prpBar->Indicators;				// CPtrArray
	
	prlFL->Bars.InsertAt(ilBarno, prlBar);
	return ilBarno;
}

void CciDetailViewer::DeleteBar(int ipGroupno, int ipLineno, int ipBarno)
{
	CCIDETAIL_LINEDATA *prlFL;
	prlFL = (CCIDETAIL_LINEDATA *)(omCciLines.GetAt(ipLineno));

	CCIDETAIL_BARDATA *prlBar;
	prlBar = (CCIDETAIL_BARDATA *)(prlFL->Bars.GetAt(ipBarno));
	delete prlBar;

	prlFL->Bars.RemoveAt(ipBarno);
}

int CciDetailViewer::GetBarCount(int ipGroupno, int ipLineno)
{
	return ((CCIDETAIL_LINEDATA *) (omCciLines.GetAt(ipLineno))) -> Bars.GetSize();
}

CCIDETAIL_BARDATA *CciDetailViewer::GetBar(int ipGroupno, int ipLineno, int ipBarno)
{
	CCIDETAIL_LINEDATA *prlFL;
	prlFL = (CCIDETAIL_LINEDATA *)(omCciLines.GetAt(ipLineno));

	CCIDETAIL_BARDATA *prlBar;
	prlBar = (CCIDETAIL_BARDATA *)(prlFL->Bars.GetAt(ipBarno));
	
	return prlBar;
}

int CciDetailViewer::GetBarnoFromTime(int ipGroupno, int ipLineno, CTime opTime1, CTime opTime2, int ipOverlapLevel1, int ipOverlapLevel2)
{
	CCIDETAIL_LINEDATA *prlFL;
	prlFL = (CCIDETAIL_LINEDATA *)(omCciLines.GetAt(ipLineno));
	CCIDETAIL_BARDATA *prlBar;
	int ilUB = prlFL->Bars.GetUpperBound();
	for (int i = ilUB; i >= 0; i--)
	{
		prlBar = (CCIDETAIL_BARDATA *)(prlFL->Bars.GetAt(i));
		if (IsOverlapped(prlBar->StartTime, prlBar->EndTime, opTime1, opTime2) == TRUE)
			break;
	}

	return i;
}

int CciDetailViewer::GetBkBarCount(int ipGroupno, int ipLineno)
{
	return ((CCIDETAIL_LINEDATA *) (omCciLines.GetAt(ipLineno))) -> BkBars.GetSize();
}

CCIDETAIL_BKBARDATA *CciDetailViewer::GetBkBar(int ipGroupno, int ipLineno, int ipBarno)
{
	CCIDETAIL_LINEDATA *prlFL;
	prlFL = (CCIDETAIL_LINEDATA *)(omCciLines.GetAt(ipLineno));

	CCIDETAIL_BKBARDATA *prlBar = &prlFL->BkBars[ipBarno];
	
	return prlBar;
}

int CciDetailViewer::GetBkBarnoFromTime(int ipGroupno, int ipLineno, CTime opTime1, CTime opTime2, int ipOverlapLevel1, int ipOverlapLevel2)
{
	CCIDETAIL_LINEDATA *prlFL;
	prlFL = (CCIDETAIL_LINEDATA *)(omCciLines.GetAt(ipLineno));
	CCIDETAIL_BKBARDATA *prlBar;
	int ilUB = prlFL->BkBars.GetUpperBound();
	for (int i = ilUB; i >= 0; i--)
	{
		prlBar = &prlFL->BkBars[i];
		if (IsOverlapped(prlBar->StartTime, prlBar->EndTime, opTime1, opTime2) == TRUE)
			break;
	}

	return i;
}
	
int CciDetailViewer::CreateIndicator(int ipGroupno, int ipLineno, int ipBarno, CCIDETAIL_INDICATORDATA *prpIndicator)
{
	return 0;
}

int CciDetailViewer::GetIndicatorCount(int ipGroupno, int ipLineno, int ipBarno)
{
	return 0;
}

CCIDETAIL_INDICATORDATA *CciDetailViewer::GetIndicator(int ipGroupno, int ipLineno, int ipBarno, int ipIndicatorno)
{
	return NULL;
}

void CciDetailViewer::CciDetailCf(void *popInstance, int ipDDXType,void *vpDataPointer, CString &ropInstanceName)
{
    CciDetailViewer *polViewer = (CciDetailViewer *)popInstance;

	if ((ipDDXType == JOB_CHANGE) || (ipDDXType == JOB_NEW) || (ipDDXType == JOB_DELETE))
		polViewer->ProcessJobChange((JOBDATA *)vpDataPointer);
}

void CciDetailViewer::ProcessCciChange(CCIDATA *prpCci)
{
	if (!::IsWindow(pomAttachWnd->GetSafeHwnd()))
		return;
}

void CciDetailViewer::ProcessJobChange(JOBDATA *prpJob)
{
	if (!::IsWindow(pomAttachWnd->GetSafeHwnd()))
		return;

	CCSPtrArray<JOBDATA> olJobs;
	ogJobData.GetJobsByAlid(olJobs, cmAlocAlid, ALLOCUNITTYPE_CIC);
	omJobs.Sort(CompareJobStartTime);
	for (int i = 0; i < olJobs.GetSize(); i++)
		if (olJobs[i].Urno == prpJob->Urno)
			break;

	Init(cmAlocAlid, FALSE);
}