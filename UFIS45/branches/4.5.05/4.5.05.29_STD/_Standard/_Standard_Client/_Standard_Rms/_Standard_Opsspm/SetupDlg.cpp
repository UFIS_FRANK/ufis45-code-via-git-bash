// SetupDlg.cpp : implementation file
//

#include <stdafx.h>
#include <Opsspm.h>
#include <ccsglobl.h>
#include <CCSCedaData.h>
#include <SetupDlg.h>
#include <BasicData.h>
#include <Cviewer.h>
#include <cedacfgdata.h>
#include <ConflictConfigTable.h>
#include <Conflict.h>
#include <NameConfig.h>
#include <FieldConfigDlg.h>
#include <TelexTypeDlg.h>
#include <FunctionColoursDlg.h>
#include <OtherColoursDlg.h>
#include <OtherSetupDlg.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// SetupDlg dialog
extern CCSBasicData ogBasicData;

SetupDlg::SetupDlg(CWnd* pParent /*=NULL*/)
	: CDialog(SetupDlg::IDD, pParent)
{
	imCurrMonitors = 0;
	//{{AFX_DATA_INIT(SetupDlg)
	m_Attention_RB = -1;
	m_CCIChrt_RB = -1;
	m_CCITbl_RB = -1;
	m_Coverage_RB = -1;
	m_Flightplan_RB = -1;
	m_FlightJobsTable_RB = -1;
	m_Flights_CK = FALSE;
	m_Gatechrt_RB = -1;
	m_Gatetbl_RB = -1;
	m_Info_RB = -1;
	m_Conflict_RB = -1;
	m_EqChart_RB = -1;
	m_EqTable_RB = -1;
	m_Monitors_RB = -1;
	m_Preplant_RB = -1;
	m_Requesttbl_RB = -1;
	m_Resolution_RB = -1;
	m_Staffchrt_RB = -1;
	m_Stafflst_RB = -1;
	m_Turnaround_CK = FALSE;
	m_Shadows_CK = FALSE;
	m_Monitors_RB1 = -1;
	m_RegnChrt_RB = -1;
	m_Posichrt_RB = -1;
	m_Attentiontbl = _T("");
	m_CCIBel = _T("");
	m_CCIChrt = _T("");
	m_Conflikttbl = _T("");
	m_Flightplan = _T("");
	m_Gatebel = _T("");
	m_FlightJobsTable = _T("");
	m_Gatechrt = _T("");
	m_EquipmentChart = _T("");
	m_Poschrt = _T("");
	m_Regnchrt = _T("");
	m_Preplanttbl = _T("");
	m_Requests = _T("");
	m_Staffchart = _T("");
	m_Stafflist = _T("");
	m_BreakPrintOffset = 0;
	m_PrintBreaks = FALSE;
	m_Demandlist = _T("");
	m_Demandlist_RB = -1;
	m_FlIndDemandlist = _T("");
	m_FlIndDemandlist_RB = -1;
	m_CheckinDemandlist = _T("");
	m_CheckinDemandlist_RB = -1;
	m_PrmList = _T("");
	//}}AFX_DATA_INIT

	omUser = ogUsername;
	omMode = CString("INSERT_MODE");
	omParameters2 =  CString("ATTV=<Default>;")
					+ CString("CCBV=<Default>;")
					+ CString("CCCV=<Default>;")
					+ CString("CONV=<Default>;")
					+ CString("FPLV=<Default>;")
					+ CString("GBLV=<Default>;")
					+ CString("FJTV=<Default>;")
					+ CString("GACV=<Default>;")
					+ CString("EQCV=<Default>;")
					+ CString("RECV=<Default>;")
					+ CString("PEAV=<Default>;")
					+ CString("VPLV=<Default>;")
					+ CString("RQSV=<Default>;")
					+ CString("STCV=<Default>;")
					+ CString("STLV=<Default>;")
					+ CString("PSCV=<Default>;")
					+ CString("DETV=<Default>;")
					+ CString("FITV=<Default>;")
					+ CString("CCDV=<Default>;")
					;

}

SetupDlg::~SetupDlg(void)
{
	TRACE("SetupDlg::~SetupDlg\n");
}


void SetupDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(SetupDlg)
	DDX_Radio(pDX, IDC_ATTENTION_RB, m_Attention_RB);
	DDX_Radio(pDX, IDC_CCICHRT_RB, m_CCIChrt_RB);
	DDX_Radio(pDX, IDC_CCITBL_RB, m_CCITbl_RB);
	DDX_Radio(pDX, IDC_COVERAGE_RB, m_Coverage_RB);
	DDX_Radio(pDX, IDC_FLIGHTPLAN_RB, m_Flightplan_RB);
	DDX_Radio(pDX, IDC_FLIGHTJOBSTABLE_RB, m_FlightJobsTable_RB);
	DDX_Check(pDX, IDC_FLIGHTS_CK, m_Flights_CK);
	DDX_Radio(pDX, IDC_GATECHRT_RB, m_Gatechrt_RB);
	DDX_Radio(pDX, IDC_GATETBL_RB, m_Gatetbl_RB);
	DDX_Radio(pDX, IDC_INFO_RB, m_Info_RB);
	DDX_Radio(pDX, IDC_KONFLICT_RB, m_Conflict_RB);
	DDX_Radio(pDX, IDC_EQUIPMENTCHART_RB, m_EqChart_RB);
	DDX_Radio(pDX, IDC_EQUIPMENTLIST_RB, m_EqTable_RB);
	DDX_Radio(pDX, IDC_PREPLANT_RB, m_Preplant_RB);
	DDX_Radio(pDX, IDC_REQUESTTBL_RB, m_Requesttbl_RB);
	DDX_Radio(pDX, IDC_RESULUTION_RB, m_Resolution_RB);
	DDX_Radio(pDX, IDC_STAFFCHRT_RB, m_Staffchrt_RB);
	DDX_Radio(pDX, IDC_STAFFLST_RB, m_Stafflst_RB);
	DDX_Check(pDX, IDC_TURNAROUND_CK, m_Turnaround_CK);
	DDX_Check(pDX, IDC_SHADOWS_CK, m_Shadows_CK);
	DDX_Radio(pDX, IDC_MONITORS_RB1, m_Monitors_RB1);
	DDX_Radio(pDX, IDC_REGNCHRT_RB, m_RegnChrt_RB);
	DDX_Radio(pDX, IDC_POSICHRT_RB, m_Posichrt_RB);
	DDX_Control(pDX, IDC_FLIGHTJOBSTABLEVIEW, c_FlightJobsTable);
	DDX_Control(pDX, IDC_ATTENTIONTBL, c_Attentiontbl);
	DDX_Control(pDX, IDC_STAFFLIST, c_Stafflist);
	DDX_Control(pDX, IDC_STAFFCHART, c_Staffchart);
	DDX_Control(pDX, IDC_REQUESTS, c_Requests);
	DDX_Control(pDX, IDC_PREPLANTTBL, c_Preplanttbl);
	DDX_Control(pDX, IDC_GATECHART, c_Gatechrt);
	DDX_Control(pDX, IDC_EQUIPMENTCHART, c_EquipmentChart);
	DDX_Control(pDX, IDC_REGNCHART, c_Regnchrt);
	DDX_Control(pDX, IDC_POSCHART, c_Poschrt);
	DDX_Control(pDX, IDC_GATEBEL, c_Gatebel);
	DDX_Control(pDX, IDC_FLIGHTPLAN, c_Flightplan);
	DDX_Control(pDX, IDC_CONFLICTTBL, c_conflicttbl);
	DDX_Control(pDX, IDC_CCICHART, c_Ccichart);
	DDX_Control(pDX, IDC_CCIBEL, c_CciBel);
	DDX_Control(pDX, IDC_PRMREQUESTLISTVIEW, c_PrmList);
	DDX_CBString(pDX, IDC_ATTENTIONTBL, m_Attentiontbl);
	DDX_CBString(pDX, IDC_CCIBEL, m_CCIBel);
	DDX_CBString(pDX, IDC_CCICHART, m_CCIChrt);
	DDX_CBString(pDX, IDC_CONFLICTTBL, m_Conflikttbl);
	DDX_CBString(pDX, IDC_FLIGHTPLAN, m_Flightplan);
	DDX_CBString(pDX, IDC_FLIGHTJOBSTABLEVIEW, m_FlightJobsTable);
	DDX_CBString(pDX, IDC_GATEBEL, m_Gatebel);
	DDX_CBString(pDX, IDC_GATECHART, m_Gatechrt);
	DDX_CBString(pDX, IDC_EQUIPMENTCHART, m_EquipmentChart);
	DDX_CBString(pDX, IDC_POSCHART, m_Poschrt);
	DDX_CBString(pDX, IDC_REGNCHART, m_Regnchrt);
	DDX_CBString(pDX, IDC_PREPLANTTBL, m_Preplanttbl);
	DDX_CBString(pDX, IDC_REQUESTS, m_Requests);
	DDX_CBString(pDX, IDC_STAFFCHART, m_Staffchart);
	DDX_CBString(pDX, IDC_STAFFLIST, m_Stafflist);
	DDX_CBString(pDX, IDC_PRMREQUESTLISTVIEW, m_PrmList);
	DDX_Text(pDX, IDC_EDIT_PRINT_BREAK_OFFSET, m_BreakPrintOffset);
	DDV_MinMaxInt(pDX, m_BreakPrintOffset, 0, 60);
	DDX_Check(pDX, IDC_CHECK_PRINT_BREAKS, m_PrintBreaks);
	DDX_Control (pDX, IDC_DEMANDLIST, c_Demandlist);
	//DDX_Control (pDX, IDC_PRM, c_Prmtbl);
	DDX_CBString(pDX, IDC_DEMANDLIST, m_Demandlist);
	DDX_Radio(pDX, IDC_DEMANDLIST_RB, m_Demandlist_RB);
	DDX_Control(pDX, IDC_FLINDDEMANDLIST, c_FlIndDemandlist);
	DDX_CBString(pDX, IDC_FLINDDEMANDLIST, m_FlIndDemandlist);
	DDX_Radio(pDX, IDC_FLINDDEMANDLIST_RB, m_FlIndDemandlist_RB);
	DDX_Control(pDX, IDC_CHECKINDEMANDLIST, c_CheckinDemandlist);
	DDX_CBString(pDX, IDC_CHECKINDEMANDLIST, m_CheckinDemandlist);
	DDX_Radio(pDX, IDC_CHECKINDEMANDLIST_RB, m_CheckinDemandlist_RB);

	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(SetupDlg, CDialog)
	//{{AFX_MSG_MAP(SetupDlg)
	ON_WM_MOVE() //PRF 8712
	ON_BN_CLICKED(IDC_MONITORS_RB1, On1Monitors)
	ON_BN_CLICKED(IDC_MONITORS_RB2, On2MonitorsRb)
	ON_BN_CLICKED(IDC_MONITORS_RB3, On3MonitorsRb)
	ON_BN_CLICKED(IDOK, OnSave)
	ON_BN_CLICKED(IDC_Conflict_B, OnConflictB)
	ON_BN_CLICKED(IDC_NAME_CONFIG, OnNameConfig)
	ON_BN_CLICKED(IDC_FUNCCOLOUR_CONFIG, OnFunctionColourConfig)
	ON_BN_CLICKED(IDC_OTHERCOLOUR_CONFIG, OnOtherColourConfig)
	ON_BN_CLICKED(IDC_OTHERSETUP, OnOtherSetup)
	ON_BN_CLICKED(IDC_TELEXTYPE_CONFIG, OnTelexTypeConfig)
	ON_BN_CLICKED(IDC_FIELD_CONFIG, OnFieldConfig)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// SetupDlg message handlers

BOOL SetupDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	bmInit = TRUE;


	if ( ogBasicData.imScreenResolutionX == 800 )	//hag20000613
		rmUserSetup.RESO = "800x600" ;
	else if ( ogBasicData.imScreenResolutionX == 1024 )
		rmUserSetup.RESO = "1024x768" ;
	else if ( ogBasicData.imScreenResolutionX == 1280 )
		rmUserSetup.RESO = "1280x1024" ;

	SetWindowText(GetString(IDS_STRING61835));
	CWnd *polWnd = GetDlgItem(IDC_SETUPUSER); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING33027));
	}
	polWnd = GetDlgItem(IDC_Conflict_B); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING33028));
	}
	polWnd = GetDlgItem(IDC_SETUPMONITOR); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING33029));
	}
	polWnd = GetDlgItem(IDC_SETUPSCREEN); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING33030));
	}
	polWnd = GetDlgItem(IDC_SETUPWHICH); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING33031));
	}
	polWnd = GetDlgItem(IDC_SETUPLEFT); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING33032));
	}
	polWnd = GetDlgItem(IDC_SETUPMIDDLE); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING33033));
	}
	polWnd = GetDlgItem(IDC_SETUPRIGHT); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING33034));
	}
	polWnd = GetDlgItem(IDC_STAFFCHRT_RB); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING33035));
	}
	polWnd = GetDlgItem(IDC_STAFFLST_RB); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING33036));
	}
	polWnd = GetDlgItem(IDC_GATECHRT_RB); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING33037));
	}
	polWnd = GetDlgItem(IDC_GATETBL_RB); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING33038));
	}
	polWnd = GetDlgItem(IDC_FLIGHTJOBSTABLE_RB); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_FLIGHTJOBSTABLE));
	}
	polWnd = GetDlgItem(IDC_EQUIPMENTCHART_RB); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_EQUIPMENTCHART));
	}
	polWnd = GetDlgItem(IDC_EQUIPMENTLIST_RB); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_EQUIPMENTLIST));
	}
	polWnd = GetDlgItem(IDC_CCICHRT_RB); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING33039));
	}
	polWnd = GetDlgItem(IDC_CCITBL_RB); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING33040));
	}
	polWnd = GetDlgItem(IDC_REGNCHRT_RB); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING33041));
	}
	polWnd = GetDlgItem(IDC_FLIGHTPLAN_RB); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING33042));
	}
	polWnd = GetDlgItem(IDC_POSICHRT_RB); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING33043));
	}
	polWnd = GetDlgItem(IDC_REQUESTTBL_RB); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING33044));
	}
	polWnd = GetDlgItem(IDC_INFO_RB); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING33045));
	}
	polWnd = GetDlgItem(IDC_COVERAGE_RB); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING33046));
	}
	polWnd = GetDlgItem(IDC_PREPLANT_RB); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING33047));
	}
	polWnd = GetDlgItem(IDC_ATTENTION_RB); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING33048));
	}
	polWnd = GetDlgItem(IDC_KONFLICT_RB); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING33049));
	}
	polWnd = GetDlgItem(IDC_SETUPVIEWS); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING33050));
	}
	polWnd = GetDlgItem(IDC_SETUP_EMPLOYEECHART); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING33035));
	}
	polWnd = GetDlgItem(IDC_SETUP_EMPLOYEELIST); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING33036));
	}
	polWnd = GetDlgItem(IDC_SETUP_GATECHART); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING33037));
	}
	polWnd = GetDlgItem(IDC_SETUP_GATELIST); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING33038));
	}
	polWnd = GetDlgItem(IDC_SETUP_FLIGHTJOBSTABLE); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_FLIGHTJOBSTABLE));
	}
	polWnd = GetDlgItem(IDC_SETUP_EQUIPMENTCHART); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_EQUIPMENTCHART));
	}
//	polWnd = GetDlgItem(IDC_SETUP_EQUIPMENTLIST); 
//	if(polWnd != NULL)
//	{
//		polWnd->SetWindowText(GetString(IDS_EQUIPMENTLIST));
//	}
	polWnd = GetDlgItem(IDC_SETUP_CCICHART); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING33039));
	}
	polWnd = GetDlgItem(IDC_SETUP_CCILIST); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING33040));
	}
	polWnd = GetDlgItem(IDC_SETUPREGN); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING33051));
	}
	polWnd = GetDlgItem(IDC_SETUP_FLIGHTPLAN); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING33042));
	}
	polWnd = GetDlgItem(IDC_SETUPPOSITION); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING33043));
	}
	polWnd = GetDlgItem(IDC_SETUPREQUEST); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING33044));
	}
	polWnd = GetDlgItem(IDC_SETUPPREPLAN); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING33047));
	}
	polWnd = GetDlgItem(IDC_SETUPATTENTION); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING33048));
	}

	polWnd = GetDlgItem(IDC_SETUPCONFLICT); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING33049));
		ogBasicData.SetWindowStat("SETUPDLG IDC_CONFLICT_CONFIG",polWnd);
	}
	polWnd = GetDlgItem(IDC_TELEXTYPE_CONFIG); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_TELEXTYPE_CONFIG));
		ogBasicData.SetWindowStat("SETUPDLG IDC_TELEXTYPE_CONFIG",polWnd);
	}
	polWnd = GetDlgItem(IDC_NAME_CONFIG); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_NAME_CONFIG));
		ogBasicData.SetWindowStat("SETUPDLG IDC_NAME_CONFIG",polWnd);
	}
	polWnd = GetDlgItem(IDC_FUNCCOLOUR_CONFIG); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_FUNCCOLOUR_CONFIG));
		ogBasicData.SetWindowStat("SETUPDLG IDC_FUNCCOLOUR_CONFIG",polWnd);
	}
	
	if(IsPrivateProfileOn("SETUP_OTHER_COLOURS_BTN", "NO"))
	{
		polWnd = GetDlgItem(IDC_OTHERCOLOUR_CONFIG); 
		if(polWnd != NULL)
		{
			polWnd->SetWindowText(GetString(IDS_OTHERCOLOUR_CONFIG));
			ogBasicData.SetWindowStat("SETUPDLG IDC_OTHERCOLOUR_CONFIG",polWnd);
		}
	}
	

	polWnd = GetDlgItem(IDC_OTHERSETUP); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_OTHERSETUPBUTTON));
		//ogBasicData.SetWindowStat("SETUPDLG IDC_OTHERSETUP",polWnd);
	}

	polWnd = GetDlgItem(IDC_FIELD_CONFIG); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_FIELD_CONFIG));
		ogBasicData.SetWindowStat("SETUPDLG IDC_FIELD_CONFIG",polWnd);
	}

	polWnd = GetDlgItem(IDC_TURNAROUND_CK); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING33052));
	}
	polWnd = GetDlgItem(IDC_FLIGHTS_CK); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING33053));
	}
	polWnd = GetDlgItem(IDC_SHADOWS_CK); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING33054));
	}
	polWnd = GetDlgItem(IDOK); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING33055));
	}
	polWnd = GetDlgItem(IDCANCEL); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING33017));
	}

	polWnd = GetDlgItem(IDC_DEMANDLIST_RB); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING62512));
	}

	polWnd = GetDlgItem(IDC_SETUPDEMAND); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61892));
	}

	polWnd = GetDlgItem(IDC_CHECKINDEMANDLIST_RB); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING62513));
	}

	polWnd = GetDlgItem(IDC_SETUPCHECKINDEMAND); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61969));
	}



	CWnd *polGATECHRT_RB = GetDlgItem(IDC_GATECHRT_RB);
	polGATECHRT_RB->EnableWindow(ogBasicData.bmDisplayGates);

	CWnd *polGATETBL_RB = GetDlgItem(IDC_GATETBL_RB);
	polGATETBL_RB->EnableWindow(ogBasicData.bmDisplayGates);

	CWnd *polFlightJobsTable_RB = GetDlgItem(IDC_FLIGHTJOBSTABLE_RB);
	polFlightJobsTable_RB->EnableWindow(ogBasicData.bmDisplayFlightJobsTable);

	CWnd *polEQUIPMENTCHART_RB = GetDlgItem(IDC_EQUIPMENTCHART_RB);
	polEQUIPMENTCHART_RB->EnableWindow(ogBasicData.bmDisplayEquipment);
	CWnd *polEQUIPMENTLIST_RB = GetDlgItem(IDC_EQUIPMENTLIST_RB);
	polEQUIPMENTLIST_RB->EnableWindow(ogBasicData.bmDisplayEquipment);

	CWnd *polPREPLANT_RB = GetDlgItem(IDC_PREPLANT_RB);
	polPREPLANT_RB->EnableWindow(ogBasicData.bmDisplayPrePlanning);
	CWnd *polCCICHRT_RB = GetDlgItem(IDC_CCICHRT_RB);
	polCCICHRT_RB->EnableWindow(ogBasicData.bmDisplayCheckins);
	CWnd *polCCITBL_RB = GetDlgItem(IDC_CCITBL_RB);
	polCCITBL_RB->EnableWindow(ogBasicData.bmDisplayCheckins);
	CWnd *polREGNCHRT_RB = GetDlgItem(IDC_REGNCHRT_RB);
	polREGNCHRT_RB->EnableWindow(ogBasicData.bmDisplayRegistrations);

	CWnd *polPOSICHRT = GetDlgItem(IDC_POSICHRT_RB);
	polPOSICHRT->EnableWindow(ogBasicData.bmDisplayPositions);

	CWnd *polDEMANDLIST_RB = GetDlgItem(IDC_DEMANDLIST_RB);
	polDEMANDLIST_RB->EnableWindow(ogBasicData.bmDisplayDemands);

	CWnd *polCHECKINDEMANDLIST_RB = GetDlgItem(IDC_CHECKINDEMANDLIST_RB);
	polCHECKINDEMANDLIST_RB->EnableWindow(ogBasicData.bmDisplayCheckins);

	
	// initialize user access
	CWaitCursor olWait;
	InitializeUserPreferences();

	c_Preplanttbl.EnableWindow(ogBasicData.bmDisplayPrePlanning);
	c_Gatechrt.EnableWindow(ogBasicData.bmDisplayGates);
	c_Ccichart.EnableWindow(ogBasicData.bmDisplayCheckins);
	//c_Prmtbl.EnableWindow(ogBasicData.bmDisplayPrm);
	c_FlightJobsTable.EnableWindow(ogBasicData.bmDisplayFlightJobsTable);
	c_Gatebel.EnableWindow(ogBasicData.bmDisplayGates);
	c_CciBel.EnableWindow(ogBasicData.bmDisplayCheckins);
	c_Regnchrt.EnableWindow(ogBasicData.bmDisplayRegistrations);
	c_Poschrt.EnableWindow(ogBasicData.bmDisplayPositions);
	c_Demandlist.EnableWindow(ogBasicData.bmDisplayDemands);
	c_CheckinDemandlist.EnableWindow(ogBasicData.bmDisplayCheckins);
	c_EquipmentChart.EnableWindow(ogBasicData.bmDisplayEquipment);
//	c_EquipmentList.EnableWindow(ogBasicData.bmDisplayEquipment);

	UpdateComboBoxes();
	bmInit = FALSE;

	SetUpResolution ();	//hag20000612

	if (strcmp(pcgReportingPath,"NONE") == 0 || strcmp(pcgMacroPrintBreak,"NONE") == 0)
	{
		CWnd *pWnd = GetDlgItem(IDC_CHECK_PRINT_BREAKS);
		if (pWnd && pWnd->IsWindowVisible())
			pWnd->ShowWindow(SW_HIDE);
		pWnd = GetDlgItem(IDC_EDIT_PRINT_BREAK_OFFSET);
		if (pWnd && pWnd->IsWindowVisible())
			pWnd->ShowWindow(SW_HIDE);
	}

	// lli: load PRM-SAVEPRMVIEW setting from ceda.ini, default is don't show Prm Request List view setting
	char pclTmpText[512];
	char pclConfigPath[512];
	if (getenv("CEDA") == NULL)
		strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
	else
		strcpy(pclConfigPath, getenv("CEDA"));

	GetPrivateProfileString(pcgAppName, "PRM-SAVEPRMVIEW",  "NO",pclTmpText, sizeof pclTmpText, pclConfigPath);
	if (!strcmp(pclTmpText,"YES")) {
		polWnd = GetDlgItem(IDC_PRMREQUESTLISTVIEW);
		polWnd->ShowWindow(SW_SHOW);
		polWnd = GetDlgItem(IDC_SETUP_PRMREQUESTLIST);
		polWnd->ShowWindow(SW_SHOW);
	}
	else {
		polWnd = GetDlgItem(IDC_PRMREQUESTLISTVIEW);
		polWnd->ShowWindow(SW_HIDE);
		polWnd = GetDlgItem(IDC_SETUP_PRMREQUESTLIST);
		polWnd->ShowWindow(SW_HIDE);
	}
	CRect olRect;
	GetWindowRect(&olRect);
	BOOL blMinimized = FALSE;
	ogBasicData.GetDialogFromReg(olRect, COpssPmApp::SETUP_DIALOG_WINDOWPOS_REG_KEY,blMinimized);

	//Multiple monitor setup
	ogBasicData.GetWindowPositionCorrect(olRect);

	MoveWindow(&olRect);

	return TRUE;  
}

void SetupDlg::UpdateComboBoxes()
{
	int iV = 0;
	int ilIndexV = 0;
	int ilCountV=0;
	CString olS;

	CViewer *polViewer = new CViewer();
	CStringArray olData;
	polViewer->SetViewerKey("CciDia");
	polViewer->GetViews(olData);
	ilCountV = olData.GetSize();
	for(iV = 0; iV < ilCountV; iV++)
	{
		olS = olData.GetAt(iV);
		c_Ccichart.AddString(olS);
	}
	if(ilCountV == 0)
	{
		c_Ccichart.AddString(GetString(IDS_STRINGDEFAULT));
	}

	CStringArray olData2;
	polViewer->SetViewerKey("CciTab");
	polViewer->GetViews(olData2);
	ilCountV = olData2.GetSize();
	for(iV = 0; iV < ilCountV; iV++)
	{
		olS = olData2.GetAt(iV);
		c_CciBel.AddString(olS);
	}
	if(ilCountV == 0)
	{
		c_CciBel.AddString(GetString(IDS_STRINGDEFAULT));
	}

	CStringArray olData3;
	polViewer->SetViewerKey("ConfTab");
	polViewer->GetViews(olData3);
	ilCountV = olData3.GetSize();
	for(iV = 0; iV < ilCountV; iV++)
	{
		olS = olData3.GetAt(iV);
		c_conflicttbl.AddString(olS);
	}
	if(ilCountV == 0)
	{
		c_conflicttbl.AddString(GetString(IDS_STRINGDEFAULT));
	}
	CStringArray olData4;
	polViewer->SetViewerKey("FltSched");
	polViewer->GetViews(olData4);
	ilCountV = olData4.GetSize();
	for(iV = 0; iV < ilCountV; iV++)
	{
		olS = olData4.GetAt(iV);
		c_Flightplan.AddString(olS);
	}
	if(ilCountV == 0)
	{
		c_Flightplan.AddString(GetString(IDS_STRINGDEFAULT));
	}

	CStringArray olData5;
	polViewer->SetViewerKey("GateDia");
	polViewer->GetViews(olData5);
	ilCountV = olData5.GetSize();
	for(iV = 0; iV < ilCountV; iV++)
	{
		olS = olData5.GetAt(iV);
		c_Gatechrt.AddString(olS);
	}
	if(ilCountV == 0)
	{
		c_Gatechrt.AddString(GetString(IDS_STRINGDEFAULT));
	}

	CStringArray olData18;
	polViewer->SetViewerKey("EquipmentDiagram");
	polViewer->GetViews(olData18);
	ilCountV = olData18.GetSize();
	for(iV = 0; iV < ilCountV; iV++)
	{
		olS = olData18.GetAt(iV);
		c_EquipmentChart.AddString(olS);
	}
	if(ilCountV == 0)
	{
		c_EquipmentChart.AddString(GetString(IDS_STRINGDEFAULT));
	}

	CStringArray olData14;
	polViewer->SetViewerKey("RegnDia");
	polViewer->GetViews(olData14);
	ilCountV = olData14.GetSize();
	for(iV = 0; iV < ilCountV; iV++)
	{
		olS = olData14.GetAt(iV);
		c_Regnchrt.AddString(olS);
	}
	if(ilCountV == 0)
	{
		c_Regnchrt.AddString(GetString(IDS_STRINGDEFAULT));
	}

	CStringArray olData6;
	polViewer->SetViewerKey("GateTab");
	polViewer->GetViews(olData6);
	ilCountV = olData6.GetSize();
	for(iV = 0; iV < ilCountV; iV++)
	{
		olS = olData6.GetAt(iV);
		c_Gatebel.AddString(olS);
	}
	if(ilCountV == 0)
	{
		c_Gatebel.AddString(GetString(IDS_STRINGDEFAULT));
	}


	CStringArray olData8;
	polViewer->SetViewerKey("PPTab");
	polViewer->GetViews(olData8);
	ilCountV = olData8.GetSize();
	for(iV = 0; iV < ilCountV; iV++)
	{
		olS = olData8.GetAt(iV);
		c_Preplanttbl.AddString(olS);
	}
	if(ilCountV == 0)
	{
		c_Preplanttbl.AddString(GetString(IDS_STRINGDEFAULT));
	}

	CStringArray olData9;
	polViewer->SetViewerKey("ReqTab");
	polViewer->GetViews(olData9);
	ilCountV = olData9.GetSize();
	for(iV = 0; iV < ilCountV; iV++)
	{
		olS = olData9.GetAt(iV);
		c_Requests.AddString(olS);
	}
	if(ilCountV == 0)
	{
		c_Requests.AddString(GetString(IDS_STRINGDEFAULT));
	}

	CStringArray olData10;
	polViewer->SetViewerKey("StaffDia");
	polViewer->GetViews(olData10);
	ilCountV = olData10.GetSize();
	for(iV = 0; iV < ilCountV; iV++)
	{
		olS = olData10.GetAt(iV);
		c_Staffchart.AddString(olS);
	}
	if(ilCountV == 0)
	{
		c_Staffchart.AddString(GetString(IDS_STRINGDEFAULT));
	}

	CStringArray olData11;
	polViewer->SetViewerKey("StaffTab");
	polViewer->GetViews(olData11);
	ilCountV = olData11.GetSize();
	for(iV = 0; iV < ilCountV; iV++)
	{
		olS = olData11.GetAt(iV);
		c_Stafflist.AddString(olS);
	}
	if(ilCountV == 0)
	{
		c_Stafflist.AddString(GetString(IDS_STRINGDEFAULT));
	}

	CStringArray olData12;
	polViewer->SetViewerKey("ConfTab");
	polViewer->GetViews(olData12);
	ilCountV = olData12.GetSize();
	for(iV = 0; iV < ilCountV; iV++)
	{
		olS = olData12.GetAt(iV);
		c_Attentiontbl.AddString(olS);
	}
	if(ilCountV == 0)
	{
		c_Attentiontbl.AddString(GetString(IDS_STRINGDEFAULT));
	}

	CStringArray olData13;
	polViewer->SetViewerKey("PstDia");
	polViewer->GetViews(olData13);
	ilCountV = olData13.GetSize();
	for(iV = 0; iV < ilCountV; iV++)
	{
		olS = olData13.GetAt(iV);
		c_Poschrt.AddString(olS);
	}
	if(ilCountV == 0)
	{
		c_Poschrt.AddString(GetString(IDS_STRINGDEFAULT));
	}

	CStringArray olData15;
	polViewer->SetViewerKey("DemandTab");
	polViewer->GetViews(olData15);
	ilCountV = olData15.GetSize();
	for(iV = 0; iV < ilCountV; iV++)
	{
		olS = olData15.GetAt(iV);
		c_Demandlist.AddString(olS);
	}
	if(ilCountV == 0)
	{
		c_Demandlist.AddString(GetString(IDS_STRINGDEFAULT));
	}

	CStringArray olData16;
	polViewer->SetViewerKey("FidTab");
	polViewer->GetViews(olData16);
	ilCountV = olData16.GetSize();
	for(iV = 0; iV < ilCountV; iV++)
	{
		olS = olData16.GetAt(iV);
		c_FlIndDemandlist.AddString(olS);
	}
	if(ilCountV == 0)
	{
		c_FlIndDemandlist.AddString(GetString(IDS_STRINGDEFAULT));
	}

	CStringArray olData17;
	polViewer->SetViewerKey("CicDemTab");
	polViewer->GetViews(olData17);
	ilCountV = olData17.GetSize();
	for(iV = 0; iV < ilCountV; iV++)
	{
		olS = olData17.GetAt(iV);
		c_CheckinDemandlist.AddString(olS);
	}
	if(ilCountV == 0)
	{
		c_CheckinDemandlist.AddString(GetString(IDS_STRINGDEFAULT));
	}

	/***
	CStringArray olData28;
	polViewer->SetViewerKey("PrmTab");
	polViewer->GetViews(olData28);
	ilCountV = olData28.GetSize();
	for(iV = 0; iV < ilCountV; iV++)
	{
		olS = olData28.GetAt(iV);
		c_Prmtbl.AddString(olS);
	}
	if(ilCountV == 0)
	{
		c_Prmtbl.AddString(GetString(IDS_STRINGDEFAULT));
	}
     ***/



	CStringArray olData19;
	polViewer->SetViewerKey("FlightJobsTable");
	polViewer->GetViews(olData19);
	ilCountV = olData19.GetSize();
	for(iV = 0; iV < ilCountV; iV++)
	{
		olS = olData19.GetAt(iV);
		c_FlightJobsTable.AddString(olS);
	}
	if(ilCountV == 0)
	{
		c_FlightJobsTable.AddString(GetString(IDS_STRINGDEFAULT));
	}

	CStringArray olData20;
	polViewer->SetViewerKey("PrmTab");
	polViewer->GetViews(olData20);
	ilCountV = olData20.GetSize();
	for(iV = 0; iV < ilCountV; iV++)
	{
		olS = olData20.GetAt(iV);
		c_PrmList.AddString(olS);
	}
	if(ilCountV == 0)
	{
		c_PrmList.AddString(GetString(IDS_STRINGDEFAULT));
	}

	if(omMode == CString("UPDATE_MODE"))
	{
		ilIndexV = c_Stafflist.FindString(0, rmUserSetup.STLV);
		c_Stafflist.SetCurSel(max(ilIndexV,0));
		ilIndexV = c_Staffchart.FindString(0, rmUserSetup.STCV);
		c_Staffchart.SetCurSel(max(ilIndexV,0));
		ilIndexV = c_Requests.FindString(0, rmUserSetup.RQSV);
		c_Requests.SetCurSel(max(ilIndexV,0));
		ilIndexV = c_Preplanttbl.FindString(0, rmUserSetup.VPLV);
		c_Preplanttbl.SetCurSel(max(ilIndexV,0));
		ilIndexV = c_Gatechrt.FindString(0, rmUserSetup.GACV);
		c_Gatechrt.SetCurSel(max(ilIndexV,0));

		ilIndexV = c_EquipmentChart.FindString(0, rmUserSetup.EQCV);
		c_EquipmentChart.SetCurSel(max(ilIndexV,0));
//		ilIndexV = c_EquipmentList.FindString(0, rmUserSetup.EQLV);
//		c_EquipmentList.SetCurSel(max(ilIndexV,0));

		ilIndexV = c_Regnchrt.FindString(0, rmUserSetup.RECV);
		c_Regnchrt.SetCurSel(max(ilIndexV,0));
		ilIndexV = c_Gatebel.FindString(0, rmUserSetup.GBLV);
		c_Gatebel.SetCurSel(max(ilIndexV,0));
		ilIndexV = c_FlightJobsTable.FindString(0, rmUserSetup.FJTV);
		c_FlightJobsTable.SetCurSel(max(ilIndexV,0));
		ilIndexV = c_Flightplan.FindString(0, rmUserSetup.FPLV);
		c_Flightplan.SetCurSel(max(ilIndexV,0));
		ilIndexV = c_conflicttbl.FindString(0, rmUserSetup.CONV);
		c_conflicttbl.SetCurSel(max(ilIndexV,0));
		ilIndexV = c_Ccichart.FindString(0, rmUserSetup.CCCV);
		c_Ccichart.SetCurSel(max(ilIndexV,0));
		//ilIndexV = c_Ccichart.FindString(0, rmUserSetup.PRMV);
		//c_Prmtbl.SetCurSel(max(ilIndexV,0));
		ilIndexV = c_CciBel.FindString(0, rmUserSetup.CCBV);
		c_CciBel.SetCurSel(max(ilIndexV,0));
		ilIndexV = c_Attentiontbl.FindString(0, rmUserSetup.ATTV);
		c_Attentiontbl.SetCurSel(max(ilIndexV,0));
		ilIndexV = c_Poschrt.FindString(0, rmUserSetup.PSCV);
		c_Poschrt.SetCurSel(max(ilIndexV,0));
		ilIndexV = c_Demandlist.FindString(0, rmUserSetup.DETV);
		c_Demandlist.SetCurSel(max(ilIndexV,0));
		ilIndexV = c_FlIndDemandlist.FindString(0, rmUserSetup.FITV);
		c_FlIndDemandlist.SetCurSel(max(ilIndexV,0));
		ilIndexV = c_CheckinDemandlist.FindString(0, rmUserSetup.CCDV);
		c_CheckinDemandlist.SetCurSel(max(ilIndexV,0));
		ilIndexV = c_PrmList.FindString(0, rmUserSetup.PRMV);
		c_PrmList.SetCurSel(max(ilIndexV,0));
	}
	delete polViewer;
	olData.RemoveAll();
	olData2.RemoveAll();
	olData3.RemoveAll();
	olData4.RemoveAll();
	olData5.RemoveAll();
	olData6.RemoveAll();
	olData8.RemoveAll();
	olData9.RemoveAll();
	olData10.RemoveAll();
	olData11.RemoveAll();
	olData12.RemoveAll();
	olData13.RemoveAll();
	olData14.RemoveAll();
	olData15.RemoveAll();
	olData16.RemoveAll();
	olData17.RemoveAll();
	olData18.RemoveAll();
	olData19.RemoveAll();
	olData20.RemoveAll();
}

int SetupDlg::ResetDialog()
{
	m_Monitors_RB1 = imCurrMonitors;
	m_Attention_RB = 0;
	m_CCIChrt_RB = 0;
	m_RegnChrt_RB = 0;
	m_CCITbl_RB = 0;
	m_Coverage_RB = 0;
	m_Flightplan_RB = 0;
	m_FlightJobsTable_RB = 0;
	m_Flights_CK = TRUE;
	m_Gatechrt_RB = 0;
	m_Gatetbl_RB = 0;
	m_Info_RB = 0;
	m_Conflict_RB = 0;
	m_EqChart_RB = 0;
	m_EqTable_RB = 0;
	m_Preplant_RB = 0;
	m_Requesttbl_RB = 0;
	m_Resolution_RB = 1;
	m_Staffchrt_RB = 0;
	m_Stafflst_RB = 0;
	m_Posichrt_RB = 0;
	m_Turnaround_CK = TRUE;
	m_Shadows_CK = TRUE;
	m_Demandlist_RB = 0;
	m_FlIndDemandlist_RB = 0;
	m_CheckinDemandlist_RB = 0;

	UpdateData(FALSE);
	return 0;
}

int SetupDlg::MakeParameters()
{
	omParameters1 = "";

	if(m_Attention_RB == 0)
		omParameters1 = "ATTB=L;";
	else if(m_Attention_RB == 1)
		omParameters1 = "ATTB=M;";
	else if(m_Attention_RB == 2)
		omParameters1 = "ATTB=R;";

	if(m_CCIChrt_RB == 0)
		omParameters1 += "CCCH=L;";
	else if(m_CCIChrt_RB == 1)
		omParameters1 += "CCCH=M;";
	else if(m_CCIChrt_RB == 2)
		omParameters1 += "CCCH=R;";

	if(m_RegnChrt_RB == 0)
		omParameters1 += "RECH=L;";
	else if(m_RegnChrt_RB == 1)
		omParameters1 += "RECH=M;";
	else if(m_RegnChrt_RB == 2)
		omParameters1 += "RECH=R;";

	if(m_CCITbl_RB == 0)
		omParameters1 += "CCTB=L;";
	else if(m_CCITbl_RB == 1)
		omParameters1 += "CCTB=M;";
	else if(m_CCITbl_RB == 2)
		omParameters1 += "CCTB=R;";

	if(m_Coverage_RB == 0)
		omParameters1 += "CVTB=L;";
	else if(m_Coverage_RB == 1)
		omParameters1 += "CVTB=M;";
	else if(m_Coverage_RB == 2)
		omParameters1 += "CVTB=R;";

	if(m_Flightplan_RB == 0)
		omParameters1 += "FPTB=L;";
	else if(m_Flightplan_RB == 1)
		omParameters1 += "FPTB=M;";
	else if(m_Flightplan_RB == 2)
		omParameters1 += "FPTB=R;";

	if(m_FlightJobsTable_RB == 0)
		omParameters1 += "FJTB=L;";
	else if(m_FlightJobsTable_RB == 1)
		omParameters1 += "FJTB=M;";
	else if(m_FlightJobsTable_RB == 2)
		omParameters1 += "FJTB=R;";

	if(m_Flights_CK == TRUE)
		omParameters1 += "FBCK=J;";
	else
		omParameters1 += "FBCK=N;";

	if(m_Gatechrt_RB == 0)
		omParameters1 += "GACH=L;";
	else if(m_Gatechrt_RB == 1)
		omParameters1 += "GACH=M;";
	else if(m_Gatechrt_RB == 2)
		omParameters1 += "GACH=R;";

	if(m_Gatetbl_RB == 0)
		omParameters1 += "GATB=L;";
	if(m_Gatetbl_RB == 1)
		omParameters1 += "GATB=M;";
	if(m_Gatetbl_RB == 2)
		omParameters1 += "GATB=R;";

	if(m_Info_RB == 0)
		omParameters1 += "INFO=L;";
	else if(m_Info_RB == 1)
		omParameters1 += "INFO=M;";
	else if(m_Info_RB == 2)
		omParameters1 += "INFO=R;";

	if(m_Conflict_RB == 0)
		omParameters1 += "CFTB=L;";
	else if(m_Conflict_RB == 1)
		omParameters1 += "CFTB=M;";
	else if(m_Conflict_RB == 2)
		omParameters1 += "CFTB=R;";

	if(m_EqChart_RB == 0)
		omParameters1 += "EQCH=L;";
	else if(m_EqChart_RB == 1)
		omParameters1 += "EQCH=M;";
	else if(m_EqChart_RB == 2)
		omParameters1 += "EQCH=R;";

	if(m_EqTable_RB == 0)
		omParameters1 += "EQTB=L;";
	else if(m_EqTable_RB == 1)
		omParameters1 += "EQTB=M;";
	else if(m_EqTable_RB == 2)
		omParameters1 += "EQTB=R;";

	if(m_Monitors_RB1 == 0)
		omParameters1 += "MONS=1;";
	else if(m_Monitors_RB1 == 1)
		omParameters1 += "MONS=2;";
	else if(m_Monitors_RB1 == 2)
		omParameters1 += "MONS=3;";

	if(m_Preplant_RB == 0)
		omParameters1 += "VPTB=L;";
	else if(m_Preplant_RB == 1)
		omParameters1 += "VPTB=M;";
	else if(m_Preplant_RB == 2)
		omParameters1 += "VPTB=R;";

	if(m_Requesttbl_RB == 0)
		omParameters1 += "RQTB=L;";
	else if(m_Requesttbl_RB == 1)
		omParameters1 += "RQTB=M;";
	else if(m_Requesttbl_RB == 2)
		omParameters1 += "RQTB=R;";

	if(m_Resolution_RB == 0)
		omParameters1 += "RESO=800x600;";
	else if(m_Resolution_RB == 1)
		omParameters1 += "RESO=1024x768;";
	else if(m_Resolution_RB == 2)
		omParameters1 += "RESO=1280x1024;";

	if(m_Staffchrt_RB == 0)
		omParameters1 += "STCH=L;";
	else if(m_Staffchrt_RB == 1)
		omParameters1 += "STCH=M;";
	else if(m_Staffchrt_RB == 2)
		omParameters1 += "STCH=R;";

	if(m_Stafflst_RB == 0)
		omParameters1 += "STTB=L;";
	else if(m_Stafflst_RB == 1)
		omParameters1 += "STTB=M;";
	else if(m_Stafflst_RB == 2)
		omParameters1 += "STTB=R;";

	if(m_Posichrt_RB == 0)
		omParameters1 += "POCH=L;";
	else if(m_Posichrt_RB == 1)
		omParameters1 += "POCH=M;";
	else if(m_Posichrt_RB == 2)
		omParameters1 += "POCH=R;";

	if(m_Turnaround_CK == TRUE)
		omParameters1 += "TACK=J;";
	else 
		omParameters1 += "TACK=N;";

	if(m_Shadows_CK == TRUE)
		omParameters1 += "SBCK=J;";
	else 
		omParameters1 += "SBCK=N;";

	if (m_PrintBreaks == TRUE)
		omParameters1 += "BKPR=J;";
	else
		omParameters1 += "BKPR=N;";

	CString olValue;
	olValue.Format("BKPO=%d;",m_BreakPrintOffset);
	omParameters1 += olValue;

	if(m_Demandlist_RB == 0)
		omParameters1 += "DETB=L;";
	else if(m_Demandlist_RB == 1)
		omParameters1 += "DETB=M;";
	else if(m_Demandlist_RB == 2)
		omParameters1 += "DETB=R;";

	if(m_FlIndDemandlist_RB == 0)
		omParameters1 += "FITB=L;";
	else if(m_FlIndDemandlist_RB == 1)
		omParameters1 += "FITB=M;";
	else if(m_FlIndDemandlist_RB == 2)
		omParameters1 += "FITB=R;";

	if(m_CheckinDemandlist_RB == 0)
		omParameters1 += "CCDB=L;";
	else if(m_CheckinDemandlist_RB == 1)
		omParameters1 += "CCDB=M;";
	else if(m_CheckinDemandlist_RB == 2)
		omParameters1 += "CCDB=R;";

	return 0;
}

void SetupDlg::On1Monitors() 
{
	imCurrMonitors = 0;
	ResetDialog();
	CWnd *polGATECHRT_RB2 = GetDlgItem(IDC_GATECHRT_RB2);
	if(polGATECHRT_RB2 != NULL)
		polGATECHRT_RB2->EnableWindow(FALSE);
	CWnd *polGATECHRT_RB3 = GetDlgItem(IDC_GATECHRT_RB3);
	if(polGATECHRT_RB3 != NULL)
		polGATECHRT_RB3->EnableWindow(FALSE);
	CWnd *polSTAFFCHRT_RB2 = GetDlgItem(IDC_STAFFCHRT_RB2);
	if(polSTAFFCHRT_RB2 != NULL)
		polSTAFFCHRT_RB2->EnableWindow(FALSE);
	CWnd *polIDC_STAFFCHRT_RB3 = GetDlgItem(IDC_STAFFCHRT_RB3);
	if(polIDC_STAFFCHRT_RB3 != NULL)
		polIDC_STAFFCHRT_RB3->EnableWindow(FALSE);
	CWnd *polCCICHRT_RB2 = GetDlgItem(IDC_CCICHRT_RB2);
	if(polCCICHRT_RB2 != NULL)
		polCCICHRT_RB2->EnableWindow(FALSE);
	CWnd *polCCICHRT_RB3 = GetDlgItem(IDC_CCICHRT_RB3);
	if(polCCICHRT_RB3 != NULL)
		polCCICHRT_RB3->EnableWindow(FALSE);
	CWnd *polREGNCHRT_RB2 = GetDlgItem(IDC_REGNCHRT_RB2);
	if(polREGNCHRT_RB2 != NULL)
		polREGNCHRT_RB2->EnableWindow(FALSE);
	CWnd *polREGNCHRT_RB3 = GetDlgItem(IDC_REGNCHRT_RB3);
	if(polREGNCHRT_RB3 != NULL)
		polREGNCHRT_RB3->EnableWindow(FALSE);
	CWnd *polFLIGHTPLAN_RB2 = GetDlgItem(IDC_FLIGHTPLAN_RB2);
	if(polFLIGHTPLAN_RB2 != NULL)
		polFLIGHTPLAN_RB2->EnableWindow(FALSE);
	CWnd *polFLIGHTPLAN_RB3 = GetDlgItem(IDC_FLIGHTPLAN_RB3);
	if(polFLIGHTPLAN_RB3 != NULL)
		polFLIGHTPLAN_RB3->EnableWindow(FALSE);
	CWnd *polSTAFFLST_RB2 = GetDlgItem(IDC_STAFFLST_RB2);
	if(polSTAFFLST_RB2 != NULL)
		polSTAFFLST_RB2->EnableWindow(FALSE);
	CWnd *polSTAFFLST_RB3 = GetDlgItem(IDC_STAFFLST_RB3);
	if(polSTAFFLST_RB3 != NULL)
		polSTAFFLST_RB3->EnableWindow(FALSE);
	CWnd *polGATETBL_RB2 = GetDlgItem(IDC_GATETBL_RB2);
	if(polGATETBL_RB2 != NULL)
		polGATETBL_RB2->EnableWindow(FALSE);
	CWnd *polGATETBL_RB3 = GetDlgItem(IDC_GATETBL_RB3);
	if(polGATETBL_RB3 != NULL)
		polGATETBL_RB3->EnableWindow(FALSE);

	CWnd *polFLIGHTJOBSTABLE_RB2 = GetDlgItem(IDC_FLIGHTJOBSTABLE_RB2);
	if(polFLIGHTJOBSTABLE_RB2 != NULL)
		polFLIGHTJOBSTABLE_RB2->EnableWindow(FALSE);
	CWnd *polFLIGHTJOBSTABLE_RB3 = GetDlgItem(IDC_FLIGHTJOBSTABLE_RB3);
	if(polFLIGHTJOBSTABLE_RB3 != NULL)
		polFLIGHTJOBSTABLE_RB3->EnableWindow(FALSE);
	
	CWnd *polCCITBL_RB2 = GetDlgItem(IDC_CCITBL_RB2);
	if(polCCITBL_RB2 != NULL)
		polCCITBL_RB2->EnableWindow(FALSE);
	CWnd *polCCITBL_RB3 = GetDlgItem(IDC_CCITBL_RB3);
	if(polCCITBL_RB3 != NULL)
		polCCITBL_RB3->EnableWindow(FALSE);
	CWnd *polREQUESTTBL_RB2 = GetDlgItem(IDC_REQUESTTBL_RB2);
	if(polREQUESTTBL_RB2 != NULL)
		polREQUESTTBL_RB2->EnableWindow(FALSE);
	CWnd *polREQUESTTBL_RB3 = GetDlgItem(IDC_REQUESTTBL_RB3);
	if(polREQUESTTBL_RB3 != NULL)
		polREQUESTTBL_RB3->EnableWindow(FALSE);
	CWnd *polCOVERAGE_RB2 = GetDlgItem(IDC_COVERAGE_RB2);
	if(polCOVERAGE_RB2 != NULL)
		polCOVERAGE_RB2->EnableWindow(FALSE);
	CWnd *polCOVERAGE_RB3 = GetDlgItem(IDC_COVERAGE_RB3);
	if(polCOVERAGE_RB3 != NULL)
		polCOVERAGE_RB3->EnableWindow(FALSE);
	CWnd *polPREPLANT_RB2 = GetDlgItem(IDC_PREPLANT_RB2);
	if(polPREPLANT_RB2 != NULL)
		polPREPLANT_RB2->EnableWindow(FALSE);
	CWnd *polPREPLANT_RB3 = GetDlgItem(IDC_PREPLANT_RB3);
	if(polPREPLANT_RB3 != NULL)
		polPREPLANT_RB3->EnableWindow(FALSE);
	CWnd *polKONFLICT_RB2 = GetDlgItem(IDC_KONFLICT_RB2);
	if(polKONFLICT_RB2 != NULL)
		polKONFLICT_RB2->EnableWindow(FALSE);
	CWnd *polKONFLICT_RB3 = GetDlgItem(IDC_KONFLICT_RB3);
	if(polKONFLICT_RB3 != NULL)
		polKONFLICT_RB3->EnableWindow(FALSE);
	CWnd *polATTENTION_RB2 = GetDlgItem(IDC_ATTENTION_RB2);
	if(polATTENTION_RB2 != NULL)
		polATTENTION_RB2->EnableWindow(FALSE);
	CWnd *polATTENTION_RB3 = GetDlgItem(IDC_ATTENTION_RB3);
	if(polATTENTION_RB3 != NULL)
		polATTENTION_RB3->EnableWindow(FALSE);
	CWnd *polINFO_RB2 = GetDlgItem(IDC_INFO_RB2);
	if(polINFO_RB2 != NULL)
		polINFO_RB2->EnableWindow(FALSE);
	CWnd *polINFO_RB3 = GetDlgItem(IDC_INFO_RB3);
	if(polINFO_RB3 != NULL)
		polINFO_RB3->EnableWindow(FALSE);
//	CWnd *polLHHOST_RB2 = GetDlgItem(IDC_LHHOST_RB2);
//	if(polLHHOST_RB2 != NULL)
//		polLHHOST_RB2->EnableWindow(FALSE);
//	CWnd *polLHHOST_RB3 = GetDlgItem(IDC_LHHOST_RB3);
//	if(polLHHOST_RB3 != NULL)
//		polLHHOST_RB3->EnableWindow(FALSE);
	CWnd *polPOSICHRT_RB2 = GetDlgItem(IDC_POSICHRT_RB2);
	if(polPOSICHRT_RB2 != NULL)
		polPOSICHRT_RB2->EnableWindow(FALSE);
	CWnd *polIDC_POSICHRT_RB3 = GetDlgItem(IDC_POSICHRT_RB3);
	if(polIDC_POSICHRT_RB3 != NULL)
		polIDC_POSICHRT_RB3->EnableWindow(FALSE);

	CWnd *polDEMANDLIST_RB2 = GetDlgItem(IDC_DEMANDLIST_RB2);
	if(polDEMANDLIST_RB2 != NULL)
		polDEMANDLIST_RB2->EnableWindow(FALSE);
	CWnd *polDEMANDLIST_RB3 = GetDlgItem(IDC_DEMANDLIST_RB3);
	if(polDEMANDLIST_RB3 != NULL)
		polDEMANDLIST_RB3->EnableWindow(FALSE);
	
	CWnd *polFLINDDEMANDLIST_RB2 = GetDlgItem(IDC_FLINDDEMANDLIST_RB2);
	if(polFLINDDEMANDLIST_RB2 != NULL)
		polFLINDDEMANDLIST_RB2->EnableWindow(FALSE);
	CWnd *polFLINDDEMANDLIST_RB3 = GetDlgItem(IDC_FLINDDEMANDLIST_RB3);
	if(polFLINDDEMANDLIST_RB3 != NULL)
		polFLINDDEMANDLIST_RB3->EnableWindow(FALSE);
	
	CWnd *polCHECKINDEMANDLIST_RB2 = GetDlgItem(IDC_CHECKINDEMANDLIST_RB2);
	if(polCHECKINDEMANDLIST_RB2 != NULL)
		polCHECKINDEMANDLIST_RB2->EnableWindow(FALSE);
	CWnd *polCHECKINDEMANDLIST_RB3 = GetDlgItem(IDC_CHECKINDEMANDLIST_RB3);
	if(polCHECKINDEMANDLIST_RB3 != NULL)
		polCHECKINDEMANDLIST_RB3->EnableWindow(FALSE);
	

	CWnd *polEQUIPMENTCHART_RB2 = GetDlgItem(IDC_EQUIPMENTCHART_RB2);
	if(polEQUIPMENTCHART_RB2 != NULL)
		polEQUIPMENTCHART_RB2->EnableWindow(FALSE);
	CWnd *polEQUIPMENTCHART_RB3 = GetDlgItem(IDC_EQUIPMENTCHART_RB3);
	if(polEQUIPMENTCHART_RB3 != NULL)
		polEQUIPMENTCHART_RB3->EnableWindow(FALSE);

	CWnd *polEQUIPMENTLIST_RB2 = GetDlgItem(IDC_EQUIPMENTLIST_RB2);
	if(polEQUIPMENTLIST_RB2 != NULL)
		polEQUIPMENTLIST_RB2->EnableWindow(FALSE);
	CWnd *polEQUIPMENTLIST_RB3 = GetDlgItem(IDC_EQUIPMENTLIST_RB3);
	if(polEQUIPMENTLIST_RB3 != NULL)
		polEQUIPMENTLIST_RB3->EnableWindow(FALSE);
}

void SetupDlg::On2MonitorsRb() 
{
	imCurrMonitors = 1;
	ResetDialog();
	CWnd *polGATECHRT_RB2 = GetDlgItem(IDC_GATECHRT_RB2);
	if(polGATECHRT_RB2 != NULL)
		polGATECHRT_RB2->EnableWindow(FALSE);
	CWnd *polGATECHRT_RB3 = GetDlgItem(IDC_GATECHRT_RB3);
	if(polGATECHRT_RB3 != NULL)
		polGATECHRT_RB3->EnableWindow(ogBasicData.bmDisplayGates);

	CWnd *polEQUIPMENTCHART_RB2 = GetDlgItem(IDC_EQUIPMENTCHART_RB2);
	if(polEQUIPMENTCHART_RB2 != NULL)
		polEQUIPMENTCHART_RB2->EnableWindow(FALSE);
	CWnd *polEQUIPMENTCHART_RB3 = GetDlgItem(IDC_EQUIPMENTCHART_RB3);
	if(polEQUIPMENTCHART_RB3 != NULL)
		polEQUIPMENTCHART_RB3->EnableWindow(ogBasicData.bmDisplayEquipment);

	CWnd *polSTAFFCHRT_RB2 = GetDlgItem(IDC_STAFFCHRT_RB2);
	if(polSTAFFCHRT_RB2 != NULL)
		polSTAFFCHRT_RB2->EnableWindow(FALSE);
	CWnd *polIDC_STAFFCHRT_RB3 = GetDlgItem(IDC_STAFFCHRT_RB3);
	if(polIDC_STAFFCHRT_RB3 != NULL)
		polIDC_STAFFCHRT_RB3->EnableWindow(TRUE);
	CWnd *polCCICHRT_RB2 = GetDlgItem(IDC_CCICHRT_RB2);
	if(polCCICHRT_RB2 != NULL)
		polCCICHRT_RB2->EnableWindow(FALSE);
	CWnd *polCCICHRT_RB3 = GetDlgItem(IDC_CCICHRT_RB3);
	if(polCCICHRT_RB3 != NULL)
		polCCICHRT_RB3->EnableWindow(ogBasicData.bmDisplayCheckins);
	CWnd *polREGNCHRT_RB2 = GetDlgItem(IDC_REGNCHRT_RB2);
	if(polREGNCHRT_RB2 != NULL)
		polREGNCHRT_RB2->EnableWindow(FALSE);
	CWnd *polREGNCHRT_RB3 = GetDlgItem(IDC_REGNCHRT_RB3);
	if(polREGNCHRT_RB3 != NULL)
		polREGNCHRT_RB3->EnableWindow(ogBasicData.bmDisplayRegistrations);
	CWnd *polFLIGHTPLAN_RB2 = GetDlgItem(IDC_FLIGHTPLAN_RB2);
	if(polFLIGHTPLAN_RB2 != NULL)
		polFLIGHTPLAN_RB2->EnableWindow(FALSE);
	CWnd *polFLIGHTPLAN_RB3 = GetDlgItem(IDC_FLIGHTPLAN_RB3);
	if(polFLIGHTPLAN_RB3 != NULL)
		polFLIGHTPLAN_RB3->EnableWindow(TRUE);
	CWnd *polSTAFFLST_RB2 = GetDlgItem(IDC_STAFFLST_RB2);
	if(polSTAFFLST_RB2 != NULL)
		polSTAFFLST_RB2->EnableWindow(FALSE);
	CWnd *polSTAFFLST_RB3 = GetDlgItem(IDC_STAFFLST_RB3);
	if(polSTAFFLST_RB3 != NULL)
		polSTAFFLST_RB3->EnableWindow(TRUE);

	CWnd *polGATETBL_RB2 = GetDlgItem(IDC_GATETBL_RB2);
	if(polGATETBL_RB2 != NULL)
		polGATETBL_RB2->EnableWindow(FALSE);
	CWnd *polGATETBL_RB3 = GetDlgItem(IDC_GATETBL_RB3);
	if(polGATETBL_RB3 != NULL)
		polGATETBL_RB3->EnableWindow(ogBasicData.bmDisplayGates);

	CWnd *polFLIGHTJOBSTABLE_RB2 = GetDlgItem(IDC_FLIGHTJOBSTABLE_RB2);
	if(polFLIGHTJOBSTABLE_RB2 != NULL)
		polFLIGHTJOBSTABLE_RB2->EnableWindow(FALSE);
	CWnd *polFLIGHTJOBSTABLE_RB3 = GetDlgItem(IDC_FLIGHTJOBSTABLE_RB3);
	if(polFLIGHTJOBSTABLE_RB3 != NULL)
		polFLIGHTJOBSTABLE_RB3->EnableWindow(ogBasicData.bmDisplayFlightJobsTable);

	CWnd *polEQUIPMENTLIST_RB2 = GetDlgItem(IDC_EQUIPMENTLIST_RB2);
	if(polEQUIPMENTLIST_RB2 != NULL)
		polEQUIPMENTLIST_RB2->EnableWindow(FALSE);
	CWnd *polEQUIPMENTLIST_RB3 = GetDlgItem(IDC_EQUIPMENTLIST_RB3);
	if(polEQUIPMENTLIST_RB3 != NULL)
		polEQUIPMENTLIST_RB3->EnableWindow(ogBasicData.bmDisplayEquipment);

	CWnd *polCCITBL_RB2 = GetDlgItem(IDC_CCITBL_RB2);
	if(polCCITBL_RB2 != NULL)
		polCCITBL_RB2->EnableWindow(FALSE);
	CWnd *polCCITBL_RB3 = GetDlgItem(IDC_CCITBL_RB3);
	if(polCCITBL_RB3 != NULL)
		polCCITBL_RB3->EnableWindow(ogBasicData.bmDisplayCheckins);
	CWnd *polREQUESTTBL_RB2 = GetDlgItem(IDC_REQUESTTBL_RB2);
	if(polREQUESTTBL_RB2 != NULL)
		polREQUESTTBL_RB2->EnableWindow(FALSE);
	CWnd *polREQUESTTBL_RB3 = GetDlgItem(IDC_REQUESTTBL_RB3);
	if(polREQUESTTBL_RB3 != NULL)
		polREQUESTTBL_RB3->EnableWindow(TRUE);
	CWnd *polCOVERAGE_RB2 = GetDlgItem(IDC_COVERAGE_RB2);
	if(polCOVERAGE_RB2 != NULL)
		polCOVERAGE_RB2->EnableWindow(FALSE);
	CWnd *polCOVERAGE_RB3 = GetDlgItem(IDC_COVERAGE_RB3);
	if(polCOVERAGE_RB3 != NULL)
		polCOVERAGE_RB3->EnableWindow(TRUE);
	CWnd *polPREPLANT_RB2 = GetDlgItem(IDC_PREPLANT_RB2);
	if(polPREPLANT_RB2 != NULL)
		polPREPLANT_RB2->EnableWindow(FALSE);
	CWnd *polPREPLANT_RB3 = GetDlgItem(IDC_PREPLANT_RB3);
	if(polPREPLANT_RB3 != NULL)
		polPREPLANT_RB3->EnableWindow(ogBasicData.bmDisplayPrePlanning);
	CWnd *polKONFLICT_RB2 = GetDlgItem(IDC_KONFLICT_RB2);
	if(polKONFLICT_RB2 != NULL)
		polKONFLICT_RB2->EnableWindow(FALSE);
	CWnd *polKONFLICT_RB3 = GetDlgItem(IDC_KONFLICT_RB3);
	if(polKONFLICT_RB3 != NULL)
		polKONFLICT_RB3->EnableWindow(TRUE);
	CWnd *polATTENTION_RB2 = GetDlgItem(IDC_ATTENTION_RB2);
	if(polATTENTION_RB2 != NULL)
		polATTENTION_RB2->EnableWindow(FALSE);
	CWnd *polATTENTION_RB3 = GetDlgItem(IDC_ATTENTION_RB3);
	if(polATTENTION_RB3 != NULL)
		polATTENTION_RB3->EnableWindow(TRUE);
	CWnd *polINFO_RB2 = GetDlgItem(IDC_INFO_RB2);
	if(polINFO_RB2 != NULL)
		polINFO_RB2->EnableWindow(FALSE);
	CWnd *polINFO_RB3 = GetDlgItem(IDC_INFO_RB3);
	if(polINFO_RB3 != NULL)
		polINFO_RB3->EnableWindow(TRUE);
//	CWnd *polLHHOST_RB2 = GetDlgItem(IDC_LHHOST_RB2);
//	if(polLHHOST_RB2 != NULL)
//		polLHHOST_RB2->EnableWindow(FALSE);
//	CWnd *polLHHOST_RB3 = GetDlgItem(IDC_LHHOST_RB3);
//	if(polLHHOST_RB3 != NULL)
//		polLHHOST_RB3->EnableWindow(TRUE);
	CWnd *polPOSICHRT_RB2 = GetDlgItem(IDC_POSICHRT_RB2);
	if(polPOSICHRT_RB2 != NULL)
		polPOSICHRT_RB2->EnableWindow(FALSE);
	CWnd *polIDC_POSICHRT_RB3 = GetDlgItem(IDC_POSICHRT_RB3);
	if(polIDC_POSICHRT_RB3 != NULL)
		polIDC_POSICHRT_RB3->EnableWindow(ogBasicData.bmDisplayPositions);

	CWnd *polDEMANDLIST_RB2 = GetDlgItem(IDC_DEMANDLIST_RB2);
	if(polDEMANDLIST_RB2 != NULL)
		polDEMANDLIST_RB2->EnableWindow(FALSE);
	CWnd *polDEMANDLIST_RB3 = GetDlgItem(IDC_DEMANDLIST_RB3);
	if(polDEMANDLIST_RB3 != NULL)
		polDEMANDLIST_RB3->EnableWindow(ogBasicData.bmDisplayDemands);

	CWnd *polFLINDDEMANDLIST_RB2 = GetDlgItem(IDC_FLINDDEMANDLIST_RB2);
	if(polFLINDDEMANDLIST_RB2 != NULL)
		polFLINDDEMANDLIST_RB2->EnableWindow(FALSE);
	CWnd *polFLINDDEMANDLIST_RB3 = GetDlgItem(IDC_FLINDDEMANDLIST_RB3);
	if(polFLINDDEMANDLIST_RB3 != NULL)
		polFLINDDEMANDLIST_RB3->EnableWindow(TRUE);

	CWnd *polCHECKINDEMANDLIST_RB2 = GetDlgItem(IDC_CHECKINDEMANDLIST_RB2);
	if(polCHECKINDEMANDLIST_RB2 != NULL)
		polCHECKINDEMANDLIST_RB2->EnableWindow(FALSE);
	CWnd *polCHECKINDEMANDLIST_RB3 = GetDlgItem(IDC_CHECKINDEMANDLIST_RB3);
	if(polCHECKINDEMANDLIST_RB3 != NULL)
		polCHECKINDEMANDLIST_RB3->EnableWindow(ogBasicData.bmDisplayCheckins);

}

void SetupDlg::On3MonitorsRb() 
{
	imCurrMonitors = 2;
	ResetDialog();
	CWnd *polGATECHRT_RB2 = GetDlgItem(IDC_GATECHRT_RB2);
	if(polGATECHRT_RB2 != NULL)
		polGATECHRT_RB2->EnableWindow(ogBasicData.bmDisplayGates);
	CWnd *polGATECHRT_RB3 = GetDlgItem(IDC_GATECHRT_RB3);
	if(polGATECHRT_RB3 != NULL)
		polGATECHRT_RB3->EnableWindow(ogBasicData.bmDisplayGates);

	CWnd *polEQUIPMENTCHART_RB2 = GetDlgItem(IDC_EQUIPMENTCHART_RB2);
	if(polEQUIPMENTCHART_RB2 != NULL)
		polEQUIPMENTCHART_RB2->EnableWindow(ogBasicData.bmDisplayEquipment);
	CWnd *polEQUIPMENTCHART_RB3 = GetDlgItem(IDC_EQUIPMENTCHART_RB3);
	if(polEQUIPMENTCHART_RB3 != NULL)
		polEQUIPMENTCHART_RB3->EnableWindow(ogBasicData.bmDisplayEquipment);
	
	CWnd *polSTAFFCHRT_RB2 = GetDlgItem(IDC_STAFFCHRT_RB2);
	if(polSTAFFCHRT_RB2 != NULL)
		polSTAFFCHRT_RB2->EnableWindow(TRUE);
	CWnd *polIDC_STAFFCHRT_RB3 = GetDlgItem(IDC_STAFFCHRT_RB3);
	if(polIDC_STAFFCHRT_RB3 != NULL)
		polIDC_STAFFCHRT_RB3->EnableWindow(TRUE);
	CWnd *polCCICHRT_RB2 = GetDlgItem(IDC_CCICHRT_RB2);
	if(polCCICHRT_RB2 != NULL)
		polCCICHRT_RB2->EnableWindow(ogBasicData.bmDisplayCheckins);
	CWnd *polCCICHRT_RB3 = GetDlgItem(IDC_CCICHRT_RB3);
	if(polCCICHRT_RB3 != NULL)
		polCCICHRT_RB3->EnableWindow(ogBasicData.bmDisplayCheckins);
	CWnd *polREGNCHRT_RB2 = GetDlgItem(IDC_REGNCHRT_RB2);
	if(polREGNCHRT_RB2 != NULL)
		polREGNCHRT_RB2->EnableWindow(ogBasicData.bmDisplayRegistrations);
	CWnd *polREGNCHRT_RB3 = GetDlgItem(IDC_REGNCHRT_RB3);
	if(polREGNCHRT_RB3 != NULL)
		polREGNCHRT_RB3->EnableWindow(ogBasicData.bmDisplayRegistrations);
	CWnd *polFLIGHTPLAN_RB2 = GetDlgItem(IDC_FLIGHTPLAN_RB2);
	if(polFLIGHTPLAN_RB2 != NULL)
		polFLIGHTPLAN_RB2->EnableWindow(TRUE);
	CWnd *polFLIGHTPLAN_RB3 = GetDlgItem(IDC_FLIGHTPLAN_RB3);
	if(polFLIGHTPLAN_RB3 != NULL)
		polFLIGHTPLAN_RB3->EnableWindow(TRUE);
	CWnd *polSTAFFLST_RB2 = GetDlgItem(IDC_STAFFLST_RB2);
	if(polSTAFFLST_RB2 != NULL)
		polSTAFFLST_RB2->EnableWindow(TRUE);
	CWnd *polSTAFFLST_RB3 = GetDlgItem(IDC_STAFFLST_RB3);
	if(polSTAFFLST_RB3 != NULL)
		polSTAFFLST_RB3->EnableWindow(TRUE);

	CWnd *polGATETBL_RB2 = GetDlgItem(IDC_GATETBL_RB2);
	if(polGATETBL_RB2 != NULL)
		polGATETBL_RB2->EnableWindow(ogBasicData.bmDisplayGates);
	CWnd *polGATETBL_RB3 = GetDlgItem(IDC_GATETBL_RB3);
	if(polGATETBL_RB3 != NULL)
		polGATETBL_RB3->EnableWindow(ogBasicData.bmDisplayGates);

	CWnd *polFLIGHTJOBSTABLE_RB2 = GetDlgItem(IDC_FLIGHTJOBSTABLE_RB2);
	if(polFLIGHTJOBSTABLE_RB2 != NULL)
		polFLIGHTJOBSTABLE_RB2->EnableWindow(ogBasicData.bmDisplayFlightJobsTable);
	CWnd *polFLIGHTJOBSTABLE_RB3 = GetDlgItem(IDC_FLIGHTJOBSTABLE_RB3);
	if(polFLIGHTJOBSTABLE_RB3 != NULL)
		polFLIGHTJOBSTABLE_RB3->EnableWindow(ogBasicData.bmDisplayFlightJobsTable);

	CWnd *polEQUIPMENTLIST_RB2 = GetDlgItem(IDC_EQUIPMENTLIST_RB2);
	if(polEQUIPMENTLIST_RB2 != NULL)
		polEQUIPMENTLIST_RB2->EnableWindow(ogBasicData.bmDisplayEquipment);
	CWnd *polEQUIPMENTLIST_RB3 = GetDlgItem(IDC_EQUIPMENTLIST_RB3);
	if(polEQUIPMENTLIST_RB3 != NULL)
		polEQUIPMENTLIST_RB3->EnableWindow(ogBasicData.bmDisplayEquipment);

	CWnd *polCCITBL_RB2 = GetDlgItem(IDC_CCITBL_RB2);
	if(polCCITBL_RB2 != NULL)
		polCCITBL_RB2->EnableWindow(ogBasicData.bmDisplayCheckins);
	CWnd *polCCITBL_RB3 = GetDlgItem(IDC_CCITBL_RB3);
	if(polCCITBL_RB3 != NULL)
		polCCITBL_RB3->EnableWindow(ogBasicData.bmDisplayCheckins);
	CWnd *polREQUESTTBL_RB2 = GetDlgItem(IDC_REQUESTTBL_RB2);
	if(polREQUESTTBL_RB2 != NULL)
		polREQUESTTBL_RB2->EnableWindow(TRUE);
	CWnd *polREQUESTTBL_RB3 = GetDlgItem(IDC_REQUESTTBL_RB3);
	if(polREQUESTTBL_RB3 != NULL)
		polREQUESTTBL_RB3->EnableWindow(TRUE);
	CWnd *polCOVERAGE_RB2 = GetDlgItem(IDC_COVERAGE_RB2);
	if(polCOVERAGE_RB2 != NULL)
		polCOVERAGE_RB2->EnableWindow(TRUE);
	CWnd *polCOVERAGE_RB3 = GetDlgItem(IDC_COVERAGE_RB3);
	if(polCOVERAGE_RB3 != NULL)
		polCOVERAGE_RB3->EnableWindow(TRUE);
	CWnd *polPREPLANT_RB2 = GetDlgItem(IDC_PREPLANT_RB2);
	if(polPREPLANT_RB2 != NULL)
		polPREPLANT_RB2->EnableWindow(ogBasicData.bmDisplayPrePlanning);
	CWnd *polPREPLANT_RB3 = GetDlgItem(IDC_PREPLANT_RB3);
	if(polPREPLANT_RB3 != NULL)
		polPREPLANT_RB3->EnableWindow(ogBasicData.bmDisplayPrePlanning);
	CWnd *polKONFLICT_RB2 = GetDlgItem(IDC_KONFLICT_RB2);
	if(polKONFLICT_RB2 != NULL)
		polKONFLICT_RB2->EnableWindow(TRUE);
	CWnd *polKONFLICT_RB3 = GetDlgItem(IDC_KONFLICT_RB3);
	if(polKONFLICT_RB3 != NULL)
		polKONFLICT_RB3->EnableWindow(TRUE);
	CWnd *polATTENTION_RB2 = GetDlgItem(IDC_ATTENTION_RB2);
	if(polATTENTION_RB2 != NULL)
		polATTENTION_RB2->EnableWindow(TRUE);
	CWnd *polATTENTION_RB3 = GetDlgItem(IDC_ATTENTION_RB3);
	if(polATTENTION_RB3 != NULL)
		polATTENTION_RB3->EnableWindow(TRUE);
	CWnd *polINFO_RB2 = GetDlgItem(IDC_INFO_RB2);
	if(polINFO_RB2 != NULL)
		polINFO_RB2->EnableWindow(TRUE);
	CWnd *polINFO_RB3 = GetDlgItem(IDC_INFO_RB3);
	if(polINFO_RB3 != NULL)
		polINFO_RB3->EnableWindow(TRUE);
//	CWnd *polLHHOST_RB2 = GetDlgItem(IDC_LHHOST_RB2);
//	if(polLHHOST_RB2 != NULL)
//		polLHHOST_RB2->EnableWindow(TRUE);
//	CWnd *polLHHOST_RB3 = GetDlgItem(IDC_LHHOST_RB3);
//	if(polLHHOST_RB3 != NULL)
//		polLHHOST_RB3->EnableWindow(TRUE);
	CWnd *polPOSICHRT_RB2 = GetDlgItem(IDC_POSICHRT_RB2);
	if(polPOSICHRT_RB2 != NULL)
		polPOSICHRT_RB2->EnableWindow(ogBasicData.bmDisplayPositions);
	CWnd *polIDC_POSICHRT_RB3 = GetDlgItem(IDC_POSICHRT_RB3);
	if(polIDC_POSICHRT_RB3 != NULL)
		polIDC_POSICHRT_RB3->EnableWindow(ogBasicData.bmDisplayPositions);

	CWnd *polDEMANDLIST_RB2 = GetDlgItem(IDC_DEMANDLIST_RB2);
	if(polDEMANDLIST_RB2 != NULL)
		polDEMANDLIST_RB2->EnableWindow(ogBasicData.bmDisplayDemands);
	CWnd *polDEMANDLIST_RB3 = GetDlgItem(IDC_DEMANDLIST_RB3);
	if(polDEMANDLIST_RB3 != NULL)
		polDEMANDLIST_RB3->EnableWindow(ogBasicData.bmDisplayDemands);

	CWnd *polFLINDDEMANDLIST_RB2 = GetDlgItem(IDC_FLINDDEMANDLIST_RB2);
	if(polFLINDDEMANDLIST_RB2 != NULL)
		polFLINDDEMANDLIST_RB2->EnableWindow(TRUE);
	CWnd *polFLINDDEMANDLIST_RB3 = GetDlgItem(IDC_FLINDDEMANDLIST_RB3);
	if(polFLINDDEMANDLIST_RB3 != NULL)
		polFLINDDEMANDLIST_RB3->EnableWindow(TRUE);

	CWnd *polCHECKINDEMANDLIST_RB2 = GetDlgItem(IDC_CHECKINDEMANDLIST_RB2);
	if(polCHECKINDEMANDLIST_RB2 != NULL)
		polCHECKINDEMANDLIST_RB2->EnableWindow(ogBasicData.bmDisplayCheckins);
	CWnd *polCHECKINDEMANDLIST_RB3 = GetDlgItem(IDC_CHECKINDEMANDLIST_RB3);
	if(polCHECKINDEMANDLIST_RB3 != NULL)
		polCHECKINDEMANDLIST_RB3->EnableWindow(ogBasicData.bmDisplayCheckins);

}

void SetupDlg::InitializeUserPreferences() 
{
	if(bmInit == FALSE)
	{	
		c_Attentiontbl.ResetContent();
		c_Attentiontbl.AddString(GetString(IDS_STRINGDEFAULT));
		c_Stafflist.ResetContent();
		c_Stafflist.AddString(GetString(IDS_STRINGDEFAULT));
		c_Staffchart.ResetContent();
		c_Staffchart.AddString(GetString(IDS_STRINGDEFAULT));
		c_Requests.ResetContent();
		c_Requests.AddString(GetString(IDS_STRINGDEFAULT));
		c_Preplanttbl.ResetContent();
		c_Preplanttbl.AddString(GetString(IDS_STRINGDEFAULT));
		c_Gatechrt.ResetContent();
		c_Gatechrt.AddString(GetString(IDS_STRINGDEFAULT));
		c_EquipmentChart.ResetContent();
		c_EquipmentChart.AddString(GetString(IDS_STRINGDEFAULT));
		c_Regnchrt.ResetContent();
		c_Regnchrt.AddString(GetString(IDS_STRINGDEFAULT));
		c_Poschrt.ResetContent();
		c_Poschrt.AddString(GetString(IDS_STRINGDEFAULT));
		c_Gatebel.ResetContent();
		c_Gatebel.AddString(GetString(IDS_STRINGDEFAULT));
		c_FlightJobsTable.ResetContent();
		c_FlightJobsTable.AddString(GetString(IDS_STRINGDEFAULT));
		c_Flightplan.ResetContent();
		c_Flightplan.AddString(GetString(IDS_STRINGDEFAULT));
		c_conflicttbl.ResetContent();
		c_conflicttbl.AddString(GetString(IDS_STRINGDEFAULT));
		c_Ccichart.ResetContent();
		c_Ccichart.AddString(GetString(IDS_STRINGDEFAULT));
		c_CciBel.ResetContent();
		c_CciBel.AddString(GetString(IDS_STRINGDEFAULT));
		//c_Prmtbl.ResetContent();
		//c_Prmtbl.AddString(GetString(IDS_STRINGDEFAULT));
		c_Demandlist.ResetContent();
		c_Demandlist.AddString(GetString(IDS_STRINGDEFAULT));
		c_FlIndDemandlist.ResetContent();
		c_FlIndDemandlist.AddString(GetString(IDS_STRINGDEFAULT));
		c_CheckinDemandlist.ResetContent();
		c_CheckinDemandlist.AddString(GetString(IDS_STRINGDEFAULT));
	}

	CString olParameters;
	CCSPtrArray<CFGDATA> olData;
	int ilCountSets = 0;
	int ilCount = ogCfgData.omData.GetSize();
	for(int i = 0; i < ilCount; i++)
	{
		CFGDATA rlC = ogCfgData.omData[i];
		if((strcmp(rlC.Pkno, omUser) == 0) && (strcmp(rlC.Ctyp, "CKI-SETUP") == 0))
		{
			olData.NewAt(0, rlC);
			ilCountSets++;
		}
	}
	
	if(ilCountSets == 2)
	{
		char *psp = NULL;
		CString olTmp1, olTmp2;
		CFGDATA rlC1 = olData.GetAt(0);
		CFGDATA rlC2 = olData.GetAt(1);
		olTmp1 = rlC1.Text;
		psp = strstr(rlC1.Text, "MONS");
		if(psp != NULL)
		{
			omParameters1 = olTmp1;
		}
		else
		{
			psp = strstr(rlC1.Text, "STCV");
			if(psp != NULL)
			{
				omParameters2 = olTmp1;
			}
		}
		olTmp2 = rlC2.Text;
		psp = strstr(rlC2.Text, "STCV");
		if(psp != NULL)
		{
			omParameters2 = olTmp2;
		}
		else
		{
			psp = strstr(rlC2.Text, "MONS");
			if(psp != NULL)
			{
				omParameters1 = olTmp2;
			}
		}
		olParameters = omParameters1 + omParameters2; //olTmp1 + olTmp2;
	}
	else
	{
		ResetDialog();
		On1Monitors(); 
		return;
	}

	olData.DeleteAll();

	if(!olParameters.IsEmpty())
	{
		MakeSetupStuct();

		if(rmUserSetup.MONS == "1")
		{
			On1Monitors();
			m_Monitors_RB1 = 0;
		}
		else if(rmUserSetup.MONS == "2")
		{
			m_Monitors_RB1 = 1;
			On2MonitorsRb();
		}
		else if(rmUserSetup.MONS == "3")
		{
			m_Monitors_RB1 = 2;
			On3MonitorsRb();
		}

		SetUpResolution ();		//hag20000612

		if(rmUserSetup.GACH == "L")
			m_Gatechrt_RB = 0;
		else if(rmUserSetup.GACH == "M")
			m_Gatechrt_RB = 1;
		else if(rmUserSetup.GACH == "R")
			m_Gatechrt_RB = 2;

		if(rmUserSetup.STCH == "L")
			m_Staffchrt_RB = 0;
		else if(rmUserSetup.STCH == "M")
			m_Staffchrt_RB = 1;
		else if(rmUserSetup.STCH == "R")
			m_Staffchrt_RB = 2;

		if(rmUserSetup.CCCH == "L")
			m_CCIChrt_RB = 0;
		else if(rmUserSetup.CCCH == "M")
			m_CCIChrt_RB = 1;
		else if(rmUserSetup.CCCH == "R")
			m_CCIChrt_RB = 2;

		if(rmUserSetup.RECH == "L")
			m_RegnChrt_RB = 0;
		else if(rmUserSetup.RECH == "M")
			m_RegnChrt_RB = 1;
		else if(rmUserSetup.RECH == "R")
			m_RegnChrt_RB = 2;

		if(rmUserSetup.FPTB == "L")
			m_Flightplan_RB = 0;
		else if(rmUserSetup.FPTB == "M")
			m_Flightplan_RB = 1;
		else if(rmUserSetup.FPTB == "R")
			m_Flightplan_RB = 2;

		if(rmUserSetup.FJTB == "L")
			m_FlightJobsTable_RB = 0;
		else if(rmUserSetup.FJTB == "M")
			m_FlightJobsTable_RB = 1;
		else if(rmUserSetup.FJTB == "R")
			m_FlightJobsTable_RB = 2;

		if(rmUserSetup.STTB == "L")
			m_Stafflst_RB = 0;
		else if(rmUserSetup.STTB == "M")
			m_Stafflst_RB = 1;
		else if(rmUserSetup.STTB == "R")
			m_Stafflst_RB = 2;

		if(rmUserSetup.GATB == "L")
			m_Gatetbl_RB = 0;
		else if(rmUserSetup.GATB == "M")
			m_Gatetbl_RB = 1;
		else if(rmUserSetup.GATB == "R")
			m_Gatetbl_RB= 2;

		if(rmUserSetup.CCTB == "L")
			m_CCITbl_RB = 0;
		else if(rmUserSetup.CCTB == "M")
			m_CCITbl_RB = 1;
		else if(rmUserSetup.CCTB == "R")
			m_CCITbl_RB = 2;
		
		if(rmUserSetup.RQTB == "L")
			m_Requesttbl_RB = 0;
		else if(rmUserSetup.RQTB == "M")
			m_Requesttbl_RB = 1;
		else if(rmUserSetup.RQTB == "R")
			m_Requesttbl_RB = 2;

		if(rmUserSetup.CVTB == "L")
			m_Coverage_RB = 0;
		else if(rmUserSetup.CVTB == "M")
			m_Coverage_RB = 1;
		else if(rmUserSetup.CVTB == "R")
			m_Coverage_RB = 2;

		if(rmUserSetup.VPTB == "L")
			m_Preplant_RB = 0;
		else if(rmUserSetup.VPTB == "M")
			m_Preplant_RB = 1;
		else if(rmUserSetup.VPTB == "R")
			m_Preplant_RB = 2;

		if(rmUserSetup.CFTB == "L")
			m_Conflict_RB = 0;
		else if(rmUserSetup.CFTB == "M")
			m_Conflict_RB = 1;
		else if(rmUserSetup.CFTB == "R")
			m_Conflict_RB = 2;

		if(rmUserSetup.EQCH == "L")
			m_EqChart_RB = 0;
		else if(rmUserSetup.EQCH == "M")
			m_EqChart_RB = 1;
		else if(rmUserSetup.EQCH == "R")
			m_EqChart_RB = 2;

		if(rmUserSetup.EQTB == "L")
			m_EqTable_RB = 0;
		else if(rmUserSetup.EQTB == "M")
			m_EqTable_RB = 1;
		else if(rmUserSetup.EQTB == "R")
			m_EqTable_RB = 2;

		if(rmUserSetup.ATTB == "L")
			m_Attention_RB = 0;
		else if(rmUserSetup.ATTB == "M")
			m_Attention_RB = 1;
		else if(rmUserSetup.ATTB == "R")
			m_Attention_RB = 2;

		if(rmUserSetup.INFO == "L")
			m_Info_RB = 0;
		else if(rmUserSetup.INFO == "M")
			m_Info_RB = 1;
		else if(rmUserSetup.INFO == "R")
			m_Info_RB = 2;

		if(rmUserSetup.POCH == "L")
			m_Posichrt_RB = 0;
		else if(rmUserSetup.POCH == "M")
			m_Posichrt_RB = 1;
		else if(rmUserSetup.POCH == "R")
			m_Posichrt_RB = 2;

		//
		if(rmUserSetup.TACK == "J")
			m_Turnaround_CK = TRUE;
		else if(rmUserSetup.TACK == "N")
			m_Turnaround_CK = FALSE;

		if(rmUserSetup.FBCK == "J")
			m_Flights_CK = TRUE;
		else if(rmUserSetup.FBCK == "N")
			m_Flights_CK = FALSE;

		if(rmUserSetup.SBCK == "J")
			m_Shadows_CK = TRUE;
		else if(rmUserSetup.SBCK == "N")
			m_Shadows_CK = FALSE;

		if (rmUserSetup.BKPR == "J")
			m_PrintBreaks = TRUE;
		else if (rmUserSetup.BKPR == "N")
			m_PrintBreaks = FALSE;

		if (sscanf(rmUserSetup.BKPO,"%d",&m_BreakPrintOffset) != 1)
			m_BreakPrintOffset = 30;

		if(rmUserSetup.DETB == "L")
			m_Demandlist_RB = 0;
		else if(rmUserSetup.DETB == "M")
			m_Demandlist_RB = 1;
		else if(rmUserSetup.DETB == "R")
			m_Demandlist_RB = 2;

		if(rmUserSetup.FITB == "L")
			m_FlIndDemandlist_RB = 0;
		else if(rmUserSetup.FITB == "M")
			m_FlIndDemandlist_RB = 1;
		else if(rmUserSetup.FITB == "R")
			m_FlIndDemandlist_RB = 2;

		if(rmUserSetup.CCDB == "L")
			m_CheckinDemandlist_RB = 0;
		else if(rmUserSetup.CCDB == "M")
			m_CheckinDemandlist_RB = 1;
		else if(rmUserSetup.CCDB == "R")
			m_CheckinDemandlist_RB = 2;


		if(bmInit == FALSE)
			UpdateComboBoxes();

		UpdateData(FALSE);
		omMode = CString("UPDATE_MODE");
	}
}

void SetupDlg::OnSave() 
{
	CWaitCursor olWait;

	UpdateData(TRUE);

	MakeParameters();

	FillCombosWithDefault();
	EvaluateUpdateMode();
	MakeCedaData();
	RestoreViews();
	ogCCSDdx.DataChanged(this,UPDATE_SETUP,NULL);

	CDialog::OnOK();
}

void SetupDlg::OnCancel()
{
	ogBasicData.WriteDialogToReg(this,COpssPmApp::SETUP_DIALOG_WINDOWPOS_REG_KEY,omWindowRect,this->IsIconic());
	CDialog::OnCancel();
}

void SetupDlg::RestoreViews()
{
	COpssPmApp::InitDefaultViews();
}


void SetupDlg::MakeCedaData()
{
	//omParameters1 
	//omParameters2;
	if(omMode == CString("INSERT_MODE"))
	{
		CString olParameters;

		//Part 1 the basic-parameters
		CFGDATA olCfg1;
		strcpy(olCfg1.Ctyp, "CKI-SETUP");
		strcpy(olCfg1.Pkno, omUser);
		strcpy(olCfg1.Text, omParameters1);
		olCfg1.Urno = ogBasicData.GetNextUrno();
		olCfg1.IsChanged = DATA_NEW;

		ogCfgData.AddCfg(&olCfg1);

		//Part 2 the views
		CFGDATA olCfg2;
		strcpy(olCfg2.Ctyp, "CKI-SETUP");
		strcpy(olCfg2.Pkno, omUser);
		strcpy(olCfg2.Text, omParameters2);
		olCfg2.Urno = ogBasicData.GetNextUrno();
		olCfg2.IsChanged = DATA_NEW;

		ogCfgData.AddCfg(&olCfg2);

	}
	else if(omMode == CString("UPDATE_MODE"))
	{
		int ilCount = 0;
		int ilCountSet = 0;
		CCSPtrArray<CFGDATA> olData;
		ilCount = ogCfgData.omData.GetSize();
		for(int i = 0; i < ilCount; i++)
		{
			CFGDATA rlC = ogCfgData.omData[i];
			if((strcmp(rlC.Pkno, omUser) == 0) && (strcmp(rlC.Ctyp, "CKI-SETUP") == 0))
			{
				olData.NewAt(0, rlC);
				ilCountSet++;
			}
		}
		
		if(ilCountSet > 0)
		{
			CFGDATA rlCfgData1 = olData.GetAt(0);

			CFGDATA *prlCfg1 = ogCfgData.GetCfgByUrno(rlCfgData1.Urno);
			strcpy(prlCfg1->Text, omParameters1);

			strcpy(rlCfgData1.Text, omParameters1);
			rlCfgData1.IsChanged = DATA_CHANGED;
			ogCfgData.ChangeCfgData(&rlCfgData1);
		
		}
		else
		{
			//Part 1 the basic-parameters
			CFGDATA olCfg1;
			strcpy(olCfg1.Ctyp, "CKI-SETUP");
			strcpy(olCfg1.Pkno, omUser);
			strcpy(olCfg1.Text, omParameters1);
			olCfg1.Urno = ogBasicData.GetNextUrno();
			olCfg1.IsChanged = DATA_NEW;

			ogCfgData.AddCfg(&olCfg1);

		}
		if(ilCountSet > 1)
		{
			CFGDATA rlCfgData2 = olData.GetAt(1);

			CFGDATA *prlCfg2 = ogCfgData.GetCfgByUrno(rlCfgData2.Urno);
			strcpy(prlCfg2->Text, omParameters2);

			strcpy(rlCfgData2.Text, omParameters2);
			rlCfgData2.IsChanged = DATA_CHANGED;
			ogCfgData.ChangeCfgData(&rlCfgData2);
		}
		else
		{
			CString olUser;
			CString olParameters;

			//Part 2 the views
			CFGDATA olCfg2;
			strcpy(olCfg2.Ctyp, "CKI-SETUP");
			strcpy(olCfg2.Pkno, omUser);
			strcpy(olCfg2.Text, omParameters2);
			olCfg2.Urno = ogBasicData.GetNextUrno();
			olCfg2.IsChanged = DATA_NEW;

			ogCfgData.AddCfg(&olCfg2);
		}
		olData.DeleteAll();
	}
}


void SetupDlg::MakeSetupStuct()
{
		CString olParameters;
		olParameters = omParameters1 + omParameters2;
		USERSETUPDATA rlUserSetup;
		ogCfgData.InterpretSetupString(olParameters, &rlUserSetup);
		//rmUserSetup = rlUserSetup;

		//We have to do the copy in this way because memcpy does not
		//work with CStrings in a struct
		rmUserSetup.MONS = rlUserSetup.MONS;
		rmUserSetup.RESO = rlUserSetup.RESO; 
		if ( ogBasicData.imScreenResolutionX == 800 )	//hag20000613
			rmUserSetup.RESO = "800x600" ;
		else if ( ogBasicData.imScreenResolutionX == 1024 )
			rmUserSetup.RESO = "1024x768" ;
		else if ( ogBasicData.imScreenResolutionX == 1280 )
			rmUserSetup.RESO = "1280x1024" ;

		rmUserSetup.GACH = rlUserSetup.GACH; 
		rmUserSetup.EQCH = rlUserSetup.EQCH; 
		rmUserSetup.EQTB = rlUserSetup.EQTB; 
		rmUserSetup.STCH = rlUserSetup.STCH; 
		rmUserSetup.CCCH = rlUserSetup.CCCH; 
		rmUserSetup.RECH = rlUserSetup.RECH; 
		rmUserSetup.FPTB = rlUserSetup.FPTB; 
		rmUserSetup.FJTB = rlUserSetup.FJTB; 
		rmUserSetup.STTB = rlUserSetup.STTB; 
		rmUserSetup.GATB = rlUserSetup.GATB; 
		rmUserSetup.CCTB = rlUserSetup.CCTB; 
		rmUserSetup.RQTB = rlUserSetup.RQTB; 
		rmUserSetup.PKTB = rlUserSetup.PKTB; 
		rmUserSetup.CVTB = rlUserSetup.CVTB; 
		rmUserSetup.VPTB = rlUserSetup.VPTB; 
		rmUserSetup.CFTB = rlUserSetup.CFTB; 
		rmUserSetup.ATTB = rlUserSetup.ATTB; 
		rmUserSetup.INFO = rlUserSetup.INFO; 
		rmUserSetup.LHHS = rlUserSetup.LHHS; 
		rmUserSetup.TACK = rlUserSetup.TACK; 
		rmUserSetup.FBCK = rlUserSetup.FBCK; 
		rmUserSetup.SBCK = rlUserSetup.SBCK; 
		rmUserSetup.STCV = rlUserSetup.STCV; 
		rmUserSetup.GACV = rlUserSetup.GACV; 
		rmUserSetup.EQCV = rlUserSetup.EQCV; 
		rmUserSetup.RECV = rlUserSetup.RECV; 
		rmUserSetup.CCCV = rlUserSetup.CCCV; 
		rmUserSetup.STLV = rlUserSetup.STLV; 
		rmUserSetup.FPLV = rlUserSetup.FPLV; 
		rmUserSetup.GBLV = rlUserSetup.GBLV; 
		rmUserSetup.FJTV = rlUserSetup.FJTV; 
		rmUserSetup.CCBV = rlUserSetup.CCBV; 
		rmUserSetup.RQSV = rlUserSetup.RQSV; 
		rmUserSetup.PEAV = rlUserSetup.PEAV; 
		rmUserSetup.VPLV = rlUserSetup.VPLV; 
		rmUserSetup.CONV = rlUserSetup.CONV; 
		rmUserSetup.ATTV = rlUserSetup.ATTV; 
		rmUserSetup.POCH = rlUserSetup.POCH; 
		rmUserSetup.PSCV = rlUserSetup.PSCV; 
		rmUserSetup.BKPR = rlUserSetup.BKPR;
		rmUserSetup.BKPO = rlUserSetup.BKPO;
		rmUserSetup.DETB = rlUserSetup.DETB;
		rmUserSetup.DETV = rlUserSetup.DETV;
		rmUserSetup.FITB = rlUserSetup.FITB;
		rmUserSetup.FITV = rlUserSetup.FITV;
		rmUserSetup.CCDB = rlUserSetup.CCDB;
		rmUserSetup.CCDV = rlUserSetup.CCDV;
		rmUserSetup.PRMV = rlUserSetup.PRMV;
}

void SetupDlg::OnConflictB() 
{
	ConfConflicts olDlg(this);
	// restore saved values on cancel
	if (olDlg.DoModal() == IDOK)
	{
		// inform all dependends
		CWaitCursor olWait;
		ogConflicts.CheckAllConflicts(TRUE);
		ogCCSDdx.DataChanged(this,UPDATE_CONFLICT_SETUP,NULL);
	}
}

void SetupDlg::FillCombosWithDefault()
{
	CString pomString;

	if(m_Attentiontbl == CString(""))
		m_Attentiontbl = GetString(IDS_STRINGDEFAULT);
	if(m_CCIBel == CString(""))
		m_CCIBel = GetString(IDS_STRINGDEFAULT);
	if(m_CCIChrt == CString(""))
		m_CCIChrt = GetString(IDS_STRINGDEFAULT);
	if(m_Conflikttbl == CString(""))
		m_Conflikttbl = GetString(IDS_STRINGDEFAULT);
	if(m_Flightplan == CString(""))
		m_Flightplan = GetString(IDS_STRINGDEFAULT);
	if(m_Gatebel == CString(""))
		m_Gatebel = GetString(IDS_STRINGDEFAULT);
	if(m_Gatechrt == CString(""))
		m_Gatechrt = GetString(IDS_STRINGDEFAULT);
	if(m_FlightJobsTable == CString(""))
		m_FlightJobsTable = GetString(IDS_STRINGDEFAULT);
	if(m_Regnchrt == CString(""))
		m_Regnchrt = GetString(IDS_STRINGDEFAULT);
	if(m_Preplanttbl == CString(""))
		m_Preplanttbl = GetString(IDS_STRINGDEFAULT);
	if(m_Requests == CString(""))
		m_Requests = GetString(IDS_STRINGDEFAULT);
	if(m_Staffchart == CString(""))
		m_Staffchart = GetString(IDS_STRINGDEFAULT);
	if(m_Stafflist == CString(""))
		m_Stafflist = GetString(IDS_STRINGDEFAULT);
	if(m_Poschrt == CString(""))
		m_Poschrt = GetString(IDS_STRINGDEFAULT);
	if(m_Demandlist == CString(""))
		m_Demandlist = GetString(IDS_STRINGDEFAULT);
	if(m_FlIndDemandlist == CString(""))
		m_FlIndDemandlist = GetString(IDS_STRINGDEFAULT);
	if(m_CheckinDemandlist == CString(""))
		m_CheckinDemandlist = GetString(IDS_STRINGDEFAULT);
	if(m_PrmList == CString(""))
		m_PrmList = GetString(IDS_STRINGDEFAULT);
	
	pomString = CString("ATTV=") + m_Attentiontbl + CString(";")
	+ CString("CCBV=") + m_CCIBel + CString(";")
	+ CString("CCCV=") + m_CCIChrt + CString(";")
	+ CString("CONV=") + m_Conflikttbl + CString(";")
	+ CString("FPLV=") + m_Flightplan + CString(";")
	+ CString("GBLV=") + m_Gatebel + CString(";")
	+ CString("FJTV=") + m_FlightJobsTable + CString(";")
	+ CString("GACV=") + m_Gatechrt + CString(";")
	+ CString("EQCV=") + m_EquipmentChart + CString(";")
	+ CString("RECV=") + m_Regnchrt + CString(";")
	+ CString("VPLV=") + m_Preplanttbl + CString(";")
	+ CString("RQSV=") + m_Requests + CString(";")	
	+ CString("STCV=") + m_Staffchart + CString(";") 
	+ CString("STLV=") + m_Stafflist + CString(";")
	+ CString("PSCV=") + m_Poschrt + CString(";")
	+ CString("DETV=") + m_Demandlist + CString(";")
	+ CString("FITV=") + m_FlIndDemandlist + CString(";")
	+ CString("CCDV=") + m_CheckinDemandlist + CString(";")
	+ CString("PRMV=") + m_PrmList + CString(";") 
	;

	omParameters2 = pomString;
}

void SetupDlg::EvaluateUpdateMode()
{
	BOOL blFound = FALSE;
	int ilCount = ogCfgData.omData.GetSize();
	for(int i = 0; i < ilCount; i++)
	{
		CFGDATA rlC = ogCfgData.omData[i];
		if((strcmp(rlC.Pkno, omUser) == 0) && (strcmp(rlC.Ctyp, "CKI-SETUP") == 0))
		{
			blFound = TRUE;
			break;
		}
	}

	if(blFound == TRUE)
		omMode = CString("UPDATE_MODE");
	else
		omMode = CString("INSERT_MODE");
}


void SetupDlg::SetUpResolution ()			//hag20000612
{
	CWnd *polReso800;
	CWnd *polReso1024;
	CWnd *polReso1280;
	polReso800 = GetDlgItem ( IDC_RESULUTION_RB );
	polReso1024 = GetDlgItem ( IDC_RESULUTION2 );
	polReso1280 = GetDlgItem ( IDC_RESULUTION3 );
	if ( !polReso800 || !polReso1024 || !polReso1280 )
		return ;
	polReso800->EnableWindow ( TRUE );
	polReso1024->EnableWindow ( TRUE );
	polReso1280->EnableWindow ( TRUE );
	if(rmUserSetup.RESO == "800x600")
		m_Resolution_RB = 0;
	else if(rmUserSetup.RESO == "1024x768")
		m_Resolution_RB = 1;
	else if(rmUserSetup.RESO == "1280x1024")
		m_Resolution_RB = 2;
	UpdateData ( FALSE );
	polReso800->EnableWindow ( m_Resolution_RB==0 );
	polReso1024->EnableWindow ( m_Resolution_RB==1 );
	polReso1280->EnableWindow ( m_Resolution_RB==2 );
}


void SetupDlg::OnNameConfig() 
{
	if(ogNameConfig.SetNameConfig())
	{
		ogCCSDdx.DataChanged(this,REDISPLAY_ALL,NULL);
	}
}

void SetupDlg::OnFunctionColourConfig() 
{
	CFunctionColoursDlg olDlg;
	if(olDlg.DoModal() == IDOK)
	{
		ogCCSDdx.DataChanged(this,REDISPLAY_ALL,NULL);
	}
}

void SetupDlg::OnOtherColourConfig() 
{
	COtherColoursDlg olDlg;
	if(olDlg.DoModal() == IDOK)
	{
		ogCCSDdx.DataChanged(this,REDISPLAY_ALL,NULL);
	}
}

void SetupDlg::OnOtherSetup() 
{
	COtherSetupDlg olDlg;
	if(olDlg.DoModal() == IDOK)
	{
		ogCCSDdx.DataChanged(this,REDISPLAY_ALL,NULL);
	}
}

void SetupDlg::OnTelexTypeConfig() 
{
	CTelexTypeDlg olDlg;
	olDlg.DoModal();
}

void SetupDlg::OnFieldConfig() 
{
	CFieldConfigDlg olFieldConfigDlg;
	CString olWhere;
	
	BOOL blIsGateCfgUsed;
	BOOL blIsPstFlidUsed;

	char pclTmpText[512];
	char pclConfigPath[512];
	if (getenv("CEDA") == NULL)
		strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
	else
		strcpy(pclConfigPath, 	getenv("CEDA"));

	GetPrivateProfileString(pcgAppName, "GT_BAR_STATUS_TEXT",  "NO",pclTmpText, sizeof pclTmpText, pclConfigPath);
	if (strcmp(pclTmpText,"NO")==0) 
	{
		blIsGateCfgUsed = FALSE;			
	}
	else if(strcmp(pclTmpText,"YES")==0)
	{
		blIsGateCfgUsed = 	TRUE;
	}

	GetPrivateProfileString(pcgAppName, "PST_FLID_TEXT",  "NO",pclTmpText, sizeof pclTmpText, pclConfigPath);
	if (strcmp(pclTmpText,"NO")==0) 
	{
		blIsPstFlidUsed = FALSE;			
	}
	else if(strcmp(pclTmpText,"YES")==0)
	{
		blIsPstFlidUsed = 	TRUE;
	}

	// notes on the keys (the running numbers - 2nd param in AddField())
	// never change these values as they are used as an index into an array
	// to remove a line - just remove it, don't change the numbering
	// to add a line - increment the key and insert the line in the order that it should be displayed in the list

	// in the functions ogFieldConfig.ConfigureField(), never remove a value, even when a field is deleted from
	// the function calls below, always add new values to the end of the list

	// "Staff Gantt Chart Tool Tip";
	olFieldConfigDlg.AddField(IDS_FIELDCONFIG_STAFFTOOLTIP, 0, IDS_STAFFTOOLTIP_EMPLOYEE, "John Smith", 30); // "Employee Name"
	olFieldConfigDlg.AddField(IDS_FIELDCONFIG_STAFFTOOLTIP, 1, IDS_STAFFTOOLTIP_SHIFTCODE, "SC0070", 10); // "Shift Code"
	olFieldConfigDlg.AddField(IDS_FIELDCONFIG_STAFFTOOLTIP, 2, IDS_STAFFTOOLTIP_FUNCTION, "FUNC1", 10); // "Function"
	olFieldConfigDlg.AddField(IDS_FIELDCONFIG_STAFFTOOLTIP, 3, IDS_STAFFTOOLTIP_WORKGROUP, "WRK GRP 1", 10); // "Work Group"
	olFieldConfigDlg.AddField(IDS_FIELDCONFIG_STAFFTOOLTIP, 4, IDS_STAFFTOOLTIP_SHIFTIND, "Shift 007", 40); // "Basic Shift Indicator"
	olFieldConfigDlg.AddField(IDS_FIELDCONFIG_STAFFTOOLTIP, 5, IDS_STAFFTOOLTIP_MULTIFUNC, "1: ABCEF,2: VWXYZ", 40); // "Basic Shift Indicator"

	//"Position Gantt Chart - Arrival Flight Bar";
	olFieldConfigDlg.AddField(IDS_FIELDCONFIG_PSTBARARR, 0, IDS_PSTBAR_ARRFLIGHTNUM, "ZX12345S", 8); // "Flight Number"
	olFieldConfigDlg.AddField(IDS_FIELDCONFIG_PSTBARARR, 1, IDS_PSTBAR_REGN, "ABC01", 5); // "A/C Registration"
	olFieldConfigDlg.AddField(IDS_FIELDCONFIG_PSTBARARR, 2, IDS_PSTBAR_ACT3, "747", 3); // "A/C Type"
	olFieldConfigDlg.AddField(IDS_FIELDCONFIG_PSTBARARR, 3, IDS_PSTBAR_ORIG, "FCO", 3); // "Origin"
	olFieldConfigDlg.AddField(IDS_FIELDCONFIG_PSTBARARR, 4, IDS_PSTBAR_STA, "1200", 4); // "STA"
	olFieldConfigDlg.AddField(IDS_FIELDCONFIG_PSTBARARR, 5, IDS_PSTBAR_ETA, "1201", 4); // "ETA"
	olFieldConfigDlg.AddField(IDS_FIELDCONFIG_PSTBARARR, 6, IDS_PSTBAR_ARRGATE, "A01", 5); // "Gate"
	olFieldConfigDlg.AddField(IDS_FIELDCONFIG_PSTBARARR, 7, IDS_PSTBAR_ARRPOS, "A01", 5); // "Position"
	olFieldConfigDlg.AddField(IDS_FIELDCONFIG_PSTBARARR, 8, IDS_PSTBAR_ARRPAX, "999", 3); // "Arr Passengers"
	if(blIsPstFlidUsed)
	{
		olFieldConfigDlg.AddField(IDS_FIELDCONFIG_PSTBARARR, 9, IDS_PSTBAR_ARRFLIGHTID, "D", 1 ); // "Arr. Flight ID"
	}
	//"Position Gantt Chart - Turnaround Flight Bar";
	olFieldConfigDlg.AddField(IDS_FIELDCONFIG_PSTBARTURN, 0, IDS_PSTBAR_REGN, "ABC01", 5); // "A/C Registration"
	olFieldConfigDlg.AddField(IDS_FIELDCONFIG_PSTBARTURN, 1, IDS_PSTBAR_ACT3, "747", 3); // "A/C Type"
	olFieldConfigDlg.AddField(IDS_FIELDCONFIG_PSTBARTURN, 2, IDS_PSTBAR_ARRFLIGHTNUM, "ZX12345S", 8); // "Arr. Flight Number"
	olFieldConfigDlg.AddField(IDS_FIELDCONFIG_PSTBARTURN, 3, IDS_PSTBAR_ORIG, "FCO", 3); // "Origin"
	olFieldConfigDlg.AddField(IDS_FIELDCONFIG_PSTBARTURN, 4, IDS_PSTBAR_STA, "1200", 4); // "STA"
	olFieldConfigDlg.AddField(IDS_FIELDCONFIG_PSTBARTURN, 5, IDS_PSTBAR_ETA, "1201", 4); // "ETA"
	olFieldConfigDlg.AddField(IDS_FIELDCONFIG_PSTBARTURN, 6, IDS_PSTBAR_ARRGATE, "A01", 5); // "Arr. Gate"
	olFieldConfigDlg.AddField(IDS_FIELDCONFIG_PSTBARTURN, 7, IDS_PSTBAR_ARRPOS, "A01", 5); // "Arr. Position"
	olFieldConfigDlg.AddField(IDS_FIELDCONFIG_PSTBARTURN, 8, IDS_PSTBAR_ARRPAX, "999", 3); // "Arr Passengers"
	olFieldConfigDlg.AddField(IDS_FIELDCONFIG_PSTBARTURN, 9, IDS_PSTBAR_DEPFLIGHTNUM, "ZX12345S", 8); // "Dep. Flight Number"
	olFieldConfigDlg.AddField(IDS_FIELDCONFIG_PSTBARTURN, 10, IDS_PSTBAR_DEST, "FRA", 3); // "Destination"
	olFieldConfigDlg.AddField(IDS_FIELDCONFIG_PSTBARTURN, 11, IDS_PSTBAR_STD, "1200", 4); // "STD"
	olFieldConfigDlg.AddField(IDS_FIELDCONFIG_PSTBARTURN, 12, IDS_PSTBAR_ETD, "1201", 4); // "ETD"
	olFieldConfigDlg.AddField(IDS_FIELDCONFIG_PSTBARTURN, 13, IDS_PSTBAR_DEPGATE, "A01", 5); // "Dep. Gate"
	olFieldConfigDlg.AddField(IDS_FIELDCONFIG_PSTBARTURN, 14, IDS_PSTBAR_DEPPOS, "A01", 5); // "Dep. Position"
	olFieldConfigDlg.AddField(IDS_FIELDCONFIG_PSTBARTURN, 15, IDS_PSTBAR_DEPPAX, "999", 3); // "Dep Passengers"
	if(blIsPstFlidUsed)
	{
		olFieldConfigDlg.AddField(IDS_FIELDCONFIG_PSTBARTURN, 16, IDS_PSTBAR_ARRFLIGHTID, "D", 1 ); // "Arr. Flight ID"
		olFieldConfigDlg.AddField(IDS_FIELDCONFIG_PSTBARTURN, 17, IDS_PSTBAR_DEPFLIGHTID, "D", 1 ); // "Dep. Flight ID"
	}

	//"Position Gantt Chart - Departure Flight Bar";
	olFieldConfigDlg.AddField(IDS_FIELDCONFIG_PSTBARDEP, 0, IDS_PSTBAR_DEPFLIGHTNUM, "ZX12345S", 8); // "Flight Number"
	olFieldConfigDlg.AddField(IDS_FIELDCONFIG_PSTBARDEP, 1, IDS_PSTBAR_REGN, "ABC01", 5); // "A/C Registration"
	olFieldConfigDlg.AddField(IDS_FIELDCONFIG_PSTBARDEP, 2, IDS_PSTBAR_ACT3, "747", 3); // "A/C Type"
	olFieldConfigDlg.AddField(IDS_FIELDCONFIG_PSTBARDEP, 3, IDS_PSTBAR_DEST, "FRA", 3); // "Destination"
	olFieldConfigDlg.AddField(IDS_FIELDCONFIG_PSTBARDEP, 4, IDS_PSTBAR_STD, "1200", 4); // "STD"
	olFieldConfigDlg.AddField(IDS_FIELDCONFIG_PSTBARDEP, 5, IDS_PSTBAR_ETD, "1201", 4); // "ETD"
	olFieldConfigDlg.AddField(IDS_FIELDCONFIG_PSTBARDEP, 6, IDS_PSTBAR_DEPGATE, "A01", 5); // "Gate"
	olFieldConfigDlg.AddField(IDS_FIELDCONFIG_PSTBARDEP, 7, IDS_PSTBAR_DEPPOS, "A01", 5); // "Position"
	olFieldConfigDlg.AddField(IDS_FIELDCONFIG_PSTBARDEP, 8, IDS_PSTBAR_DEPPAX, "999", 3); // "Dep Passengers"
	if(blIsPstFlidUsed)
	{
		olFieldConfigDlg.AddField(IDS_FIELDCONFIG_PSTBARDEP, 9, IDS_PSTBAR_DEPFLIGHTID, "D", 1 ); // "Dep. Flight ID"
	}

	//"Position Gantt Chart - Arrival Flight Status Bar";
	olFieldConfigDlg.AddField(IDS_FIELDCONFIG_PSTSTATUSARR, 0, IDS_PSTBAR_ARRFLIGHTNUM, "ZX12345S", 8); // "Flight Number"
	olFieldConfigDlg.AddField(IDS_FIELDCONFIG_PSTSTATUSARR, 1, IDS_PSTBAR_REGN, "ABC01", 5); // "A/C Registration"
	olFieldConfigDlg.AddField(IDS_FIELDCONFIG_PSTSTATUSARR, 2, IDS_PSTBAR_ACT3, "747", 3); // "A/C Type"
	olFieldConfigDlg.AddField(IDS_FIELDCONFIG_PSTSTATUSARR, 3, IDS_PSTBAR_ORIG, "FCO", 3); // "Origin"
	olFieldConfigDlg.AddField(IDS_FIELDCONFIG_PSTSTATUSARR, 4, IDS_PSTBAR_STA, "1200", 4); // "STA"
	olFieldConfigDlg.AddField(IDS_FIELDCONFIG_PSTSTATUSARR, 5, IDS_PSTBAR_ETA, "1201", 4); // "ETA"
	olFieldConfigDlg.AddField(IDS_FIELDCONFIG_PSTSTATUSARR, 6, IDS_PSTBAR_ARRGATE, "A01", 5); // "Gate"
	olFieldConfigDlg.AddField(IDS_FIELDCONFIG_PSTSTATUSARR, 7, IDS_PSTBAR_ARRPOS, "A01", 5); // "Position"
	olFieldConfigDlg.AddField(IDS_FIELDCONFIG_PSTSTATUSARR, 8, IDS_PSTBAR_ARRPAX, "999", 3); // "Arr Passengers"
	if(blIsPstFlidUsed)
	{
		olFieldConfigDlg.AddField(IDS_FIELDCONFIG_PSTSTATUSARR, 9, IDS_PSTBAR_ARRFLIGHTID, "D", 1 ); // "Arr. Flight ID"
	}

	//"Position Gantt Chart - Turnaround Flight Status Bar";
	olFieldConfigDlg.AddField(IDS_FIELDCONFIG_PSTSTATUSTURN, 0, IDS_PSTBAR_REGN, "ABC01", 5); // "A/C Registration"
	olFieldConfigDlg.AddField(IDS_FIELDCONFIG_PSTSTATUSTURN, 1, IDS_PSTBAR_ACT3, "747", 3); // "A/C Type"
	olFieldConfigDlg.AddField(IDS_FIELDCONFIG_PSTSTATUSTURN, 2, IDS_PSTBAR_ARRFLIGHTNUM, "ZX12345S", 8); // "Arr. Flight Number"
	olFieldConfigDlg.AddField(IDS_FIELDCONFIG_PSTSTATUSTURN, 3, IDS_PSTBAR_ORIG, "FCO", 3); // "Origin"
	olFieldConfigDlg.AddField(IDS_FIELDCONFIG_PSTSTATUSTURN, 4, IDS_PSTBAR_STA, "1200", 4); // "STA"
	olFieldConfigDlg.AddField(IDS_FIELDCONFIG_PSTSTATUSTURN, 5, IDS_PSTBAR_ETA, "1201", 4); // "ETA"
	olFieldConfigDlg.AddField(IDS_FIELDCONFIG_PSTSTATUSTURN, 6, IDS_PSTBAR_ARRGATE, "A01", 5); // "Arr. Gate"
	olFieldConfigDlg.AddField(IDS_FIELDCONFIG_PSTSTATUSTURN, 7, IDS_PSTBAR_ARRPOS, "A01", 5); // "Arr. Position"
	olFieldConfigDlg.AddField(IDS_FIELDCONFIG_PSTSTATUSTURN, 8, IDS_PSTBAR_ARRPAX, "999", 3); // "Arr Passengers"
	olFieldConfigDlg.AddField(IDS_FIELDCONFIG_PSTSTATUSTURN, 9, IDS_PSTBAR_DEPFLIGHTNUM, "ZX12345S", 8); // "Dep. Flight Number"
	olFieldConfigDlg.AddField(IDS_FIELDCONFIG_PSTSTATUSTURN, 10, IDS_PSTBAR_DEST, "FRA", 3); // "Destination"
	olFieldConfigDlg.AddField(IDS_FIELDCONFIG_PSTSTATUSTURN, 11, IDS_PSTBAR_STD, "1200", 4); // "STD"
	olFieldConfigDlg.AddField(IDS_FIELDCONFIG_PSTSTATUSTURN, 12, IDS_PSTBAR_ETD, "1201", 4); // "ETD"
	olFieldConfigDlg.AddField(IDS_FIELDCONFIG_PSTSTATUSTURN, 13, IDS_PSTBAR_DEPGATE, "A01", 5); // "Dep. Gate"
	olFieldConfigDlg.AddField(IDS_FIELDCONFIG_PSTSTATUSTURN, 14, IDS_PSTBAR_DEPPOS, "A01", 5); // "Dep. Position"
	olFieldConfigDlg.AddField(IDS_FIELDCONFIG_PSTSTATUSTURN, 15, IDS_PSTBAR_DEPPAX, "999", 3); // "Dep Passengers"
	if(blIsPstFlidUsed)
	{	
		olFieldConfigDlg.AddField(IDS_FIELDCONFIG_PSTSTATUSTURN, 16, IDS_PSTBAR_ARRFLIGHTID, "D", 1 ); // "Arr. Flight ID"
		olFieldConfigDlg.AddField(IDS_FIELDCONFIG_PSTSTATUSTURN, 17, IDS_PSTBAR_DEPFLIGHTID, "D", 1 ); // "Dep. Flight ID"
	}

	//"Position Gantt Chart - Departure Flight Status Bar";
	olFieldConfigDlg.AddField(IDS_FIELDCONFIG_PSTSTATUSDEP, 0, IDS_PSTBAR_DEPFLIGHTNUM, "ZX12345S", 8); // "Flight Number"
	olFieldConfigDlg.AddField(IDS_FIELDCONFIG_PSTSTATUSDEP, 1, IDS_PSTBAR_REGN, "ABC01", 5); // "A/C Registration"
	olFieldConfigDlg.AddField(IDS_FIELDCONFIG_PSTSTATUSDEP, 2, IDS_PSTBAR_ACT3, "747", 3); // "A/C Type"
	olFieldConfigDlg.AddField(IDS_FIELDCONFIG_PSTSTATUSDEP, 3, IDS_PSTBAR_DEST, "FRA", 3); // "Destination"
	olFieldConfigDlg.AddField(IDS_FIELDCONFIG_PSTSTATUSDEP, 4, IDS_PSTBAR_STD, "1200", 4); // "STD"
	olFieldConfigDlg.AddField(IDS_FIELDCONFIG_PSTSTATUSDEP, 5, IDS_PSTBAR_ETD, "1201", 4); // "ETD"
	olFieldConfigDlg.AddField(IDS_FIELDCONFIG_PSTSTATUSDEP, 6, IDS_PSTBAR_DEPGATE, "A01", 5); // "Gate"
	olFieldConfigDlg.AddField(IDS_FIELDCONFIG_PSTSTATUSDEP, 7, IDS_PSTBAR_DEPPOS, "A01", 5); // "Position"
	olFieldConfigDlg.AddField(IDS_FIELDCONFIG_PSTSTATUSDEP, 8, IDS_PSTBAR_DEPPAX, "999", 3); // "Dep Passengers"
	if(blIsPstFlidUsed)
	{
		olFieldConfigDlg.AddField(IDS_FIELDCONFIG_PSTSTATUSDEP, 9, IDS_PSTBAR_DEPFLIGHTID, "D", 1 ); // "Dep. Flight ID"
	}


	if(blIsGateCfgUsed)
	{
		//"Gate Gantt Chart - Arrival Flight Bar";
		olFieldConfigDlg.AddField(IDS_FIELDCONFIG_GTBARARR, 0, IDS_GTBAR_ARRFLIGHTNUM, "ZX12345S", 8); // "Flight Number"
		olFieldConfigDlg.AddField(IDS_FIELDCONFIG_GTBARARR, 1, IDS_GTBAR_REGN, "ABC01", 5); // "A/C Registration"
		olFieldConfigDlg.AddField(IDS_FIELDCONFIG_GTBARARR, 2, IDS_GTBAR_ACT3, "747", 3); // "A/C Type"
		olFieldConfigDlg.AddField(IDS_FIELDCONFIG_GTBARARR, 3, IDS_GTBAR_ORIG, "FCO", 3); // "Origin"
		olFieldConfigDlg.AddField(IDS_FIELDCONFIG_GTBARARR, 4, IDS_GTBAR_STA, "1200", 4); // "STA"
		olFieldConfigDlg.AddField(IDS_FIELDCONFIG_GTBARARR, 5, IDS_GTBAR_ETA, "1201", 4); // "ETA"
		olFieldConfigDlg.AddField(IDS_FIELDCONFIG_GTBARARR, 6, IDS_GTBAR_ARRGATE, "A01", 5); // "Gate"
		olFieldConfigDlg.AddField(IDS_FIELDCONFIG_GTBARARR, 7, IDS_GTBAR_ARRPOS, "A01", 5); // "Position"
		olFieldConfigDlg.AddField(IDS_FIELDCONFIG_GTBARARR, 8, IDS_GTBAR_ARRPAX, "999", 3); // "Arr Passengers"
		olFieldConfigDlg.AddField(IDS_FIELDCONFIG_GTBARARR, 9, IDS_GTBAR_ARRFLIGHTID, "D", 1 ); // "Arr. Flight ID"

		//"Gate Gantt Chart - Turnaround Flight Bar";
		olFieldConfigDlg.AddField(IDS_FIELDCONFIG_GTBARTURN, 0, IDS_GTBAR_REGN, "ABC01", 5); // "A/C Registration"
		olFieldConfigDlg.AddField(IDS_FIELDCONFIG_GTBARTURN, 1, IDS_GTBAR_ACT3, "747", 3); // "A/C Type"
		olFieldConfigDlg.AddField(IDS_FIELDCONFIG_GTBARTURN, 2, IDS_GTBAR_ARRFLIGHTNUM, "ZX12345S", 8); // "Arr. Flight Number"
		olFieldConfigDlg.AddField(IDS_FIELDCONFIG_GTBARTURN, 3, IDS_GTBAR_ORIG, "FCO", 3); // "Origin"
		olFieldConfigDlg.AddField(IDS_FIELDCONFIG_GTBARTURN, 4, IDS_GTBAR_STA, "1200", 4); // "STA"
		olFieldConfigDlg.AddField(IDS_FIELDCONFIG_GTBARTURN, 5, IDS_GTBAR_ETA, "1201", 4); // "ETA"
		olFieldConfigDlg.AddField(IDS_FIELDCONFIG_GTBARTURN, 6, IDS_GTBAR_ARRGATE, "A01", 5); // "Arr. Gate"
		olFieldConfigDlg.AddField(IDS_FIELDCONFIG_GTBARTURN, 7, IDS_GTBAR_ARRPOS, "A01", 5); // "Arr. Position"
		olFieldConfigDlg.AddField(IDS_FIELDCONFIG_GTBARTURN, 8, IDS_GTBAR_ARRPAX, "999", 3); // "Arr Passengers"
		olFieldConfigDlg.AddField(IDS_FIELDCONFIG_GTBARTURN, 9, IDS_GTBAR_DEPFLIGHTNUM, "ZX12345S", 8); // "Dep. Flight Number"
		olFieldConfigDlg.AddField(IDS_FIELDCONFIG_GTBARTURN, 10, IDS_GTBAR_DEST, "FRA", 3); // "Destination"
		olFieldConfigDlg.AddField(IDS_FIELDCONFIG_GTBARTURN, 11, IDS_GTBAR_STD, "1200", 4); // "STD"
		olFieldConfigDlg.AddField(IDS_FIELDCONFIG_GTBARTURN, 12, IDS_GTBAR_ETD, "1201", 4); // "ETD"
		olFieldConfigDlg.AddField(IDS_FIELDCONFIG_GTBARTURN, 13, IDS_GTBAR_DEPGATE, "A01", 5); // "Dep. Gate"
		olFieldConfigDlg.AddField(IDS_FIELDCONFIG_GTBARTURN, 14, IDS_GTBAR_DEPPOS, "A01", 5); // "Dep. Position"
		olFieldConfigDlg.AddField(IDS_FIELDCONFIG_GTBARTURN, 15, IDS_GTBAR_DEPPAX, "999", 3); // "Dep Passengers"
		olFieldConfigDlg.AddField(IDS_FIELDCONFIG_GTBARTURN, 16, IDS_GTBAR_ARRFLIGHTID, "D", 1 ); // "Arr. Flight ID"
		olFieldConfigDlg.AddField(IDS_FIELDCONFIG_GTBARTURN, 17, IDS_GTBAR_DEPFLIGHTID, "D", 1 ); // "Dep. Flight ID"

		//"Gate Gantt Chart - Departure Flight Bar";
		olFieldConfigDlg.AddField(IDS_FIELDCONFIG_GTBARDEP, 0, IDS_GTBAR_DEPFLIGHTNUM, "ZX12345S", 8); // "Flight Number"
		olFieldConfigDlg.AddField(IDS_FIELDCONFIG_GTBARDEP, 1, IDS_GTBAR_REGN, "ABC01", 5); // "A/C Registration"
		olFieldConfigDlg.AddField(IDS_FIELDCONFIG_GTBARDEP, 2, IDS_GTBAR_ACT3, "747", 3); // "A/C Type"
		olFieldConfigDlg.AddField(IDS_FIELDCONFIG_GTBARDEP, 3, IDS_GTBAR_DEST, "FRA", 3); // "Destination"
		olFieldConfigDlg.AddField(IDS_FIELDCONFIG_GTBARDEP, 4, IDS_GTBAR_STD, "1200", 4); // "STD"
		olFieldConfigDlg.AddField(IDS_FIELDCONFIG_GTBARDEP, 5, IDS_GTBAR_ETD, "1201", 4); // "ETD"
		olFieldConfigDlg.AddField(IDS_FIELDCONFIG_GTBARDEP, 6, IDS_GTBAR_DEPGATE, "A01", 5); // "Gate"
		olFieldConfigDlg.AddField(IDS_FIELDCONFIG_GTBARDEP, 7, IDS_GTBAR_DEPPOS, "A01", 5); // "Position"
		olFieldConfigDlg.AddField(IDS_FIELDCONFIG_GTBARDEP, 8, IDS_GTBAR_DEPPAX, "999", 3); // "Dep Passengers"
		olFieldConfigDlg.AddField(IDS_FIELDCONFIG_GTBARDEP, 9, IDS_GTBAR_DEPFLIGHTID, "D", 1 ); // "Dep. Flight ID"


		//"Gate Gantt Chart - Arrival Flight Status Bar";
		olFieldConfigDlg.AddField(IDS_FIELDCONFIG_GTSTATUSARR, 0, IDS_GTBAR_ARRFLIGHTNUM, "ZX12345S", 8); // "Flight Number"
		olFieldConfigDlg.AddField(IDS_FIELDCONFIG_GTSTATUSARR, 1, IDS_GTBAR_REGN, "ABC01", 5); // "A/C Registration"
		olFieldConfigDlg.AddField(IDS_FIELDCONFIG_GTSTATUSARR, 2, IDS_GTBAR_ACT3, "747", 3); // "A/C Type"
		olFieldConfigDlg.AddField(IDS_FIELDCONFIG_GTSTATUSARR, 3, IDS_GTBAR_ORIG, "FCO", 3); // "Origin"
		olFieldConfigDlg.AddField(IDS_FIELDCONFIG_GTSTATUSARR, 4, IDS_GTBAR_STA, "1200", 4); // "STA"
		olFieldConfigDlg.AddField(IDS_FIELDCONFIG_GTSTATUSARR, 5, IDS_GTBAR_ETA, "1201", 4); // "ETA"
		olFieldConfigDlg.AddField(IDS_FIELDCONFIG_GTSTATUSARR, 6, IDS_GTBAR_ARRGATE, "A01", 5); // "Gate"
		olFieldConfigDlg.AddField(IDS_FIELDCONFIG_GTSTATUSARR, 7, IDS_GTBAR_ARRPOS, "A01", 5); // "Position"
		olFieldConfigDlg.AddField(IDS_FIELDCONFIG_GTSTATUSARR, 8, IDS_GTBAR_ARRPAX, "999", 3); // "Arr Passengers"
		olFieldConfigDlg.AddField(IDS_FIELDCONFIG_GTSTATUSARR, 9, IDS_GTBAR_ARRFLIGHTID, "D", 1 ); // "Arr. Flight ID"


		//"Gate Gantt Chart - Turnaround Flight Status Bar";
		olFieldConfigDlg.AddField(IDS_FIELDCONFIG_GTSTATUSTURN, 0, IDS_GTBAR_REGN, "ABC01", 5); // "A/C Registration"
		olFieldConfigDlg.AddField(IDS_FIELDCONFIG_GTSTATUSTURN, 1, IDS_GTBAR_ACT3, "747", 3); // "A/C Type"
		olFieldConfigDlg.AddField(IDS_FIELDCONFIG_GTSTATUSTURN, 2, IDS_GTBAR_ARRFLIGHTNUM, "ZX12345S", 8); // "Arr. Flight Number"
		olFieldConfigDlg.AddField(IDS_FIELDCONFIG_GTSTATUSTURN, 3, IDS_GTBAR_ORIG, "FCO", 3); // "Origin"
		olFieldConfigDlg.AddField(IDS_FIELDCONFIG_GTSTATUSTURN, 4, IDS_GTBAR_STA, "1200", 4); // "STA"
		olFieldConfigDlg.AddField(IDS_FIELDCONFIG_GTSTATUSTURN, 5, IDS_GTBAR_ETA, "1201", 4); // "ETA"
		olFieldConfigDlg.AddField(IDS_FIELDCONFIG_GTSTATUSTURN, 6, IDS_GTBAR_ARRGATE, "A01", 5); // "Arr. Gate"
		olFieldConfigDlg.AddField(IDS_FIELDCONFIG_GTSTATUSTURN, 7, IDS_GTBAR_ARRPOS, "A01", 5); // "Arr. Position"
		olFieldConfigDlg.AddField(IDS_FIELDCONFIG_GTSTATUSTURN, 8, IDS_GTBAR_ARRPAX, "999", 3); // "Arr Passengers"
		olFieldConfigDlg.AddField(IDS_FIELDCONFIG_GTSTATUSTURN, 9, IDS_GTBAR_DEPFLIGHTNUM, "ZX12345S", 8); // "Dep. Flight Number"
		olFieldConfigDlg.AddField(IDS_FIELDCONFIG_GTSTATUSTURN, 10, IDS_GTBAR_DEST, "FRA", 3); // "Destination"
		olFieldConfigDlg.AddField(IDS_FIELDCONFIG_GTSTATUSTURN, 11, IDS_GTBAR_STD, "1200", 4); // "STD"
		olFieldConfigDlg.AddField(IDS_FIELDCONFIG_GTSTATUSTURN, 12, IDS_GTBAR_ETD, "1201", 4); // "ETD"
		olFieldConfigDlg.AddField(IDS_FIELDCONFIG_GTSTATUSTURN, 13, IDS_GTBAR_DEPGATE, "A01", 5); // "Dep. Gate"
		olFieldConfigDlg.AddField(IDS_FIELDCONFIG_GTSTATUSTURN, 14, IDS_GTBAR_DEPPOS, "A01", 5); // "Dep. Position"
		olFieldConfigDlg.AddField(IDS_FIELDCONFIG_GTSTATUSTURN, 15, IDS_GTBAR_DEPPAX, "999", 3); // "Dep Passengers"
		olFieldConfigDlg.AddField(IDS_FIELDCONFIG_GTSTATUSTURN, 16, IDS_GTBAR_ARRFLIGHTID, "D", 1 ); // "Arr. Flight ID"
		olFieldConfigDlg.AddField(IDS_FIELDCONFIG_GTSTATUSTURN, 17, IDS_GTBAR_DEPFLIGHTID, "D", 1 ); // "Dep. Flight ID"

		//"Gate Gantt Chart - Departure Flight Status Bar";
		olFieldConfigDlg.AddField(IDS_FIELDCONFIG_GTSTATUSDEP, 0, IDS_GTBAR_DEPFLIGHTNUM, "ZX12345S", 8); // "Flight Number"
		olFieldConfigDlg.AddField(IDS_FIELDCONFIG_GTSTATUSDEP, 1, IDS_GTBAR_REGN, "ABC01", 5); // "A/C Registration"
		olFieldConfigDlg.AddField(IDS_FIELDCONFIG_GTSTATUSDEP, 2, IDS_GTBAR_ACT3, "747", 3); // "A/C Type"
		olFieldConfigDlg.AddField(IDS_FIELDCONFIG_GTSTATUSDEP, 3, IDS_GTBAR_DEST, "FRA", 3); // "Destination"
		olFieldConfigDlg.AddField(IDS_FIELDCONFIG_GTSTATUSDEP, 4, IDS_GTBAR_STD, "1200", 4); // "STD"
		olFieldConfigDlg.AddField(IDS_FIELDCONFIG_GTSTATUSDEP, 5, IDS_GTBAR_ETD, "1201", 4); // "ETD"
		olFieldConfigDlg.AddField(IDS_FIELDCONFIG_GTSTATUSDEP, 6, IDS_GTBAR_DEPGATE, "A01", 5); // "Gate"
		olFieldConfigDlg.AddField(IDS_FIELDCONFIG_GTSTATUSDEP, 7, IDS_GTBAR_DEPPOS, "A01", 5); // "Position"
		olFieldConfigDlg.AddField(IDS_FIELDCONFIG_GTSTATUSDEP, 8, IDS_GTBAR_DEPPAX, "999", 3); // "Dep Passengers"
		olFieldConfigDlg.AddField(IDS_FIELDCONFIG_GTSTATUSDEP, 9, IDS_GTBAR_DEPFLIGHTID, "D", 1 ); // "Dep. Flight ID"

	}
	//"Print Job Card Columns";
	olFieldConfigDlg.AddField(IDS_FIELDCONFIG_JOBCARD, 0, IDS_JOBCARD_SERVICE, "A Service Name", 30);
	olFieldConfigDlg.AddField(IDS_FIELDCONFIG_JOBCARD, 1, IDS_JOBCARD_TEAM, "TEAM01", 10);
	olFieldConfigDlg.AddField(IDS_FIELDCONFIG_JOBCARD, 2, IDS_JOBCARD_DEMFUNCTION, "FUNC01", 10);
	olFieldConfigDlg.AddField(IDS_FIELDCONFIG_JOBCARD, 3, IDS_JOBCARD_EMPLOYEE, "Geoff Hurst", 30);
	olFieldConfigDlg.AddField(IDS_FIELDCONFIG_JOBCARD, 4, IDS_JOBCARD_JOBTIME, "1200-1240", 9);
	olFieldConfigDlg.AddField(IDS_FIELDCONFIG_JOBCARD, 5, IDS_JOBCARD_OPSSPMREM, "My OPSS-PM remark", 20);

	//"Print Staff List";
	olFieldConfigDlg.AddField(IDS_FIELDCONFIG_STAFFTABLEPRINT1, 0, IDS_STAFFTABLEPRINT_MFCT, "M-Fct", 7, 1);
	olFieldConfigDlg.AddField(IDS_FIELDCONFIG_STAFFTABLEPRINT1, 1, IDS_STAFFTABLEPRINT_NAME, "Stan Bowles", 30, 1);
	olFieldConfigDlg.AddField(IDS_FIELDCONFIG_STAFFTABLEPRINT1, 2, IDS_STAFFTABLEPRINT_TEAM, "Team1", 8, 1);
	olFieldConfigDlg.AddField(IDS_FIELDCONFIG_STAFFTABLEPRINT1, 3, IDS_STAFFTABLEPRINT_SHIFT, "Shift1", 9, 1);
	olFieldConfigDlg.AddField(IDS_FIELDCONFIG_STAFFTABLEPRINT1, 4, IDS_STAFFTABLEPRINT_SHIFTTIME, "0900-1730", 9, 1);
	olFieldConfigDlg.AddField(IDS_FIELDCONFIG_STAFFTABLEPRINT1, 5, IDS_STAFFTABLEPRINT_POOL, "Pool1", 11, 1);
	olFieldConfigDlg.AddField(IDS_FIELDCONFIG_STAFFTABLEPRINT1, 6, IDS_STAFFTABLEPRINT_POOLTIME, "0900-1730", 9, 1);
	olFieldConfigDlg.AddField(IDS_FIELDCONFIG_STAFFTABLEPRINT1, 7, IDS_STAFFTABLEPRINT_JOB, "Job1", 11, 2);
	olFieldConfigDlg.AddField(IDS_FIELDCONFIG_STAFFTABLEPRINT1, 8, IDS_STAFFTABLEPRINT_JOBTIME, "1320-1400", 9, 2);
	olFieldConfigDlg.AddField(IDS_FIELDCONFIG_STAFFTABLEPRINT1, 9, IDS_STAFFTABLEPRINT_REGN, "DAIRY", 6, 2);
	olFieldConfigDlg.AddField(IDS_FIELDCONFIG_STAFFTABLEPRINT1, 10, IDS_STAFFTABLEPRINT_STAETA, "1320/1400", 9, 2);
	olFieldConfigDlg.AddField(IDS_FIELDCONFIG_STAFFTABLEPRINT1, 11, IDS_STAFFTABLEPRINT_STDETD, "1520/1600", 9, 2);
	olFieldConfigDlg.AddField(IDS_FIELDCONFIG_STAFFTABLEPRINT1, 12, IDS_STAFFTABLEPRINT_SERVICE, "Service", 20, 2);
	olFieldConfigDlg.AddField(IDS_FIELDCONFIG_STAFFTABLEPRINT1, 13, IDS_STAFFTABLEPRINT_GATE, "A12", 4, 2);
	olFieldConfigDlg.AddField(IDS_FIELDCONFIG_STAFFTABLEPRINT1, 14, IDS_STAFFTABLEPRINT_POSITION, "A23", 4, 2);
	olFieldConfigDlg.AddField(IDS_FIELDCONFIG_STAFFTABLEPRINT1, 15, IDS_STAFFTABLEPRINT_JFCT, "J-Fct", 7, 2);
	olFieldConfigDlg.AddField(IDS_FIELDCONFIG_STAFFTABLEPRINT1, 16, IDS_STAFFTABLEPRINT_TLTEAM, "TL-Team", 14, 2);

	//"Check-in Chart - Flight Independent Bar Text";
	olFieldConfigDlg.AddField(IDS_FIELDCONFIG_CHECKINFIDBAR, 0, IDS_CHECKINFIDBAR_DETY, "FID", 5);
	olFieldConfigDlg.AddField(IDS_FIELDCONFIG_CHECKINFIDBAR, 1, IDS_CHECKINFIDBAR_SERVICE, "XXXX12345", 30);
	olFieldConfigDlg.AddField(IDS_FIELDCONFIG_CHECKINFIDBAR, 2, IDS_CHECKINFIDBAR_FUNCTION, "FunctionName", 30);

	// Restricted flight job text;
	olFieldConfigDlg.AddField(IDS_FIELDCONFIG_RFJ, 0, IDS_FIELDCONFIG_PREFIX, "", 10);
	olFieldConfigDlg.AddField(IDS_FIELDCONFIG_RFJ, 1, IDS_FIELDCONFIG_FLIGHTNUM, "SQ 1234", 7);

	// Delegated flight job text;
	olFieldConfigDlg.AddField(IDS_FIELDCONFIG_DFJ, 0, IDS_FIELDCONFIG_PREFIX, "", 10);
	olFieldConfigDlg.AddField(IDS_FIELDCONFIG_DFJ, 1, IDS_FIELDCONFIG_FLIGHTNUM, "SQ 1234", 7);

	if(olFieldConfigDlg.DoModal() == IDOK)
		ogCCSDdx.DataChanged(this,REDISPLAY_ALL,NULL);
}

//PRF 8712
void SetupDlg::OnMove(int x, int y)
{
	CDialog::OnMove(x,y);
	if(this->IsIconic() == FALSE)
	{
		GetWindowRect(&omWindowRect);
	}
}
