//
// CedaSwgData.h - links emps (STFTAB) to their groups, only one of which is currently valid
// 
//
#ifndef _CEDASWGDATA_H_
#define _CEDASWGDATA_H_

#include <CCSCedaData.h>
#include <CCSPtrArray.h>

/////////////////////////////////////////////////////////////////////////////
// Record structure declaration

struct SwgDataStruct
{
	long	Urno;
	long	Surn;		// Emp URNO (STFTAB.URNO)
	char	Code[11];	// Function Code
	CTime	Vpfr;		// Valid From
	CTime	Vpto;		// Valid To
};

typedef struct SwgDataStruct SWGDATA;


/////////////////////////////////////////////////////////////////////////////
// Class declaration

class CedaSwgData: public CCSCedaData
{

// Attributes
public:
    CCSPtrArray <SWGDATA> omData;
	CMapPtrToPtr omUrnoMap;
	CMapPtrToPtr omSurnMap;
	CString GetTableName(void);

// Operations
public:
	CedaSwgData();
	~CedaSwgData();

	bool ReadSwgData();
	SWGDATA* GetSwgByUrno(long lpUrno);
	CString GetGroupBySurn(long lpSurn, CTime opDate = TIMENULL);
	void GetSwgsBySurn(long lpSurn, CCSPtrArray <SWGDATA> &ropSwgs, CTime opDate = TIMENULL, bool bpReset=true);
	void ProcessSwgBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
	CCSReturnCode UpdateSwgCode(long lpSwgUrno, const char *pcpCode);
	CCSReturnCode UpdateSwgCodeForSurn(long lpSurn, const char *pcpCode, CTime opDate = TIMENULL);
	CString DumpForUstf(long lpUstf, CTime opDate);
	CString Dump(long lpUrno);
	SWGDATA *InsertSwg(long lpSurn, const char *pcpCode, CTime opDate);

private:
	SWGDATA *AddSwgInternal(SWGDATA &rrpSwg);
	void DeleteSwgInternal(long lpUrno);
	void ClearAll();
};


extern CedaSwgData ogSwgData;
#endif _CEDASWGDATA_H_
