// FlightPlan.h : header file
//
#ifndef __FLPLAN_H__
#define __FLPLAN_H__

#include <FlightScheduleViewer.h>

/////////////////////////////////////////////////////////////////////////////
// FlightPlan dialog

extern CedaFlightData ogFlightData;

class FlightPlan : public CDialog
{
// Construction
public:
	FlightPlan(CWnd* pParent = NULL, BOOL bpIsFromSearch = FALSE,BOOL bpDetailDisplay = TRUE);	// standard constructor
    FlightPlan(CWnd* pParent, CTime StartTime, CTime EndTime, BOOL bpIsFromSearch = FALSE,BOOL bpDetailDisplay = TRUE); 
	~FlightPlan();
	void SetDate(CTime opDate);


private :
	CTime m_StartDate;
	BOOL bmIsViewOpen;
	int m_nDialogBarHeight;
	BOOL bmIsFromSearch;
	BOOL bmNoUpdatesNow;
	BOOL bmDetailDisplay;
	CRect omWindowRect; //PRF 8712

    CTable *pomFlightTable; // visual object, the table content
	CSingleDocTemplate* pomTemplate; //Singapore
	CPrintPreviewView* pomPrintPreviewView; //Singapore
	FlightScheduleViewer omViewer;

   	BOOL bmContextItem;
	long omContextDepUrno;
	long omContextArrUrno;

	static void FlightScheduleCf(void *popInstance, int ipDDXType,void *vpDataPointer, CString &ropInstanceName);
	void UpdateComboBox();
    void UpdateDisplay();
    CString Format(FLIGHTDATA *prpFlightArr,FLIGHTDATA *prpFlightDep);
	void HandleGlobalDateUpdate();
	void SetViewerDate();
	void UpdateView();
	void SetCaptionText(void);

// Dialog Data
	//{{AFX_DATA(FlightPlan)
	enum { IDD = IDD_FLIGHTTABLE };
	CComboBox	m_Date;
	CButton m_bSetTime;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(FlightPlan)
	public:
	virtual BOOL DestroyWindow();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();
	virtual void OnCancel();
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(FlightPlan)
	afx_msg void OnDestroy();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnMove(int x, int y); //PRF 8712
	afx_msg void OnView();
	afx_msg void OnSelchangeCombo2();
	afx_msg void OnCloseupCombo2();
	afx_msg void OnPrint();
    afx_msg LONG OnTableLButtonDblclk(UINT wParam, LONG lParam);
    afx_msg LONG OnTableRButtonDown(UINT wParam, LONG lParam);
	afx_msg void OnMenuWorkOn();
    afx_msg LONG OnDragOver(UINT, LONG); 
    afx_msg LONG OnDrop(UINT, LONG); 
	afx_msg void OnClose();
	afx_msg void OnSelchangeDate();
	afx_msg LONG OnTableDragBegin(UINT wParam, LONG lParam);
	afx_msg void OnTableUpdateDataCount(); //Singapore
	afx_msg void OnSetTime(); //Singapore
        afx_msg void OnPrintPreview(); //Singapore
	afx_msg void OnBeginPrinting(WPARAM wParam, LPARAM lParam); //Singapore
	afx_msg void PrintPreview(WPARAM wParam, LPARAM lParam); //Singapore
	afx_msg void OnEndPrinting(WPARAM wParam, LPARAM lParam); //Singapore
	afx_msg void OnPreviewPrint(WPARAM wParam, LPARAM lParam); //Singapore

	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

// Drag-and-drop implementation
private:
	CCSDragDropCtrl m_DragDropTarget;
	int imDropLineno;
	long imDropFlightUrno;
	CString omDropFlightAlid;
};

#endif //__FLPLAN_H__
