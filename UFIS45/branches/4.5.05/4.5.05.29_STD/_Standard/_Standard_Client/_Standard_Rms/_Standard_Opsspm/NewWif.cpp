// NewWif.cpp : implementation file
//

#include <stdafx.h> 
#include <OpssPm.h>
#include <NewWif.h>
#include <CCSTime.H>
#include <CcsGlobl.h>

/*
#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif
*/

/////////////////////////////////////////////////////////////////////////////
// CNewWif dialog


CNewWif::CNewWif(CWnd* pParent, char *pcpNewWif)
	: CDialog(CNewWif::IDD, pParent)
{
	//{{AFX_DATA_INIT(CNewWif)
	//}}AFX_DATA_INIT

	pcmNewWif = pcpNewWif;
}


void CNewWif::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CNewWif)
	DDX_Control(pDX, IDC_NEW_WIF, m_NewWif);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CNewWif, CDialog)
	//{{AFX_MSG_MAP(CNewWif)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CNewWif message handlers


BOOL CNewWif::OnInitDialog() 
{
	CDialog::OnInitDialog();
	SetWindowText(GetString(IDS_STRING33004));
	CWnd *polWnd = GetDlgItem(IDC_WHATNAME); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING32923));
	}
	polWnd = GetDlgItem(IDOK); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61697));
	}
	polWnd = GetDlgItem(IDCANCEL); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61698));
	}


	UpdateData(FALSE);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}


void CNewWif::OnOK() 
{
	CString olText;
	UpdateData(TRUE);
	
	char pcmNewTmpWif[82];
	GetDlgItemText(IDC_NEW_WIF, pcmNewTmpWif, 80);
	if (strcmp(pcmNewTmpWif, "") == 0)
	{
		MessageBox(GetString(IDS_STRING61268), GetString(IDS_STRING61269), MB_OK);
		m_NewWif.SetFocus();
		return;
	}

	sprintf(pcmNewWif, "%s", pcmNewTmpWif);

	CDialog::OnOK();
}
