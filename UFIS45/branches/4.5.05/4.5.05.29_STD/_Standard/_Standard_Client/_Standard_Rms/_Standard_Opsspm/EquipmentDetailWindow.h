// EquipmentDetailWindow.h : header file
//


#ifndef _EQUIPMENTDW_
#define _EQUIPMENTDW_

#include <EquipmentDetailDlg.h>
#include <EquipmentDetailViewer.h>
#include <EquipmentDetailGantt.h>
#include <CedaEquData.h>
#include <CedaEqtData.h>
#include <CedaEqaData.h>
#include <CedaJobData.h>

#define EquipmentData  EQUIPMENTDATA

/////////////////////////////////////////////////////////////////////////////
// EquipmentDetailWindow frame

class EquipmentDetailWindow : public CFrameWnd
{
	DECLARE_DYNCREATE(EquipmentDetailWindow)

protected:
    EquipmentDetailWindow();

// Attributes
public:

// Operations
public:
	EquipmentDetailWindow(CWnd *popParent, long lpJobPrimaryKey, CTime opClickedTime = TIMENULL, int ipDurationHours = 24);
	virtual ~EquipmentDetailWindow();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(EquipmentDetailWindow)
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(EquipmentDetailWindow)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnDestroy();
	afx_msg void OnGetMinMaxInfo(MINMAXINFO FAR* lpMMI);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

public:
	CCSPtrArray <JOBDATA> omJobList;

protected:
	EQUDATA *prmEqu;
	EQTDATA *prmEqt;
	CCSPtrArray <EQADATA> omEqaList;

    EquipmentDetailDialog omEquipmentDetail;
    CTimeScale omTimeScale;
    CStatusBar omStatusBar;

	BOOL GetMaxEndTime(CCSPtrArray<JOBDATA> *popJobs, CTime &ropMaxEndTime);
	BOOL GetMinStartTime(CCSPtrArray<JOBDATA> *popJobs, CTime &ropMinStartTime);
    
    int imStartTimeScalePos;
    int imBottomPos;

    CTime omTSStartTime;
    CTimeSpan omTSDuration;
    CTimeSpan omTSInterval;

	CTime omStartTime;
	CTimeSpan omDuration;
	CTime omClickedTime; // point (in time) which was clicked on the equipment chart - used to centre the display time
	int imDurationSecs; // duration of the displayed data in seconds

	
	EquipmentDetailViewer omViewer;
public:
	EquipmentDetailGantt omGantt;
	void OnHorizScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	long CalcTotalMinutes(void);
	void SetTSStartTime(CTime opNewTsStartTime);

private:      
    static CPoint omMaxTrackSize;
    static CPoint omMinTrackSize;
};

/////////////////////////////////////////////////////////////////////////////

#endif // _EQUIPMENTDW_

