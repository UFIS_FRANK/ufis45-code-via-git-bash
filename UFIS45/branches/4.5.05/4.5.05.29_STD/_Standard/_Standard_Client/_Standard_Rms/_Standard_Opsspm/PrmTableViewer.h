#ifndef __PRMTBLVW_H__
#define __PRMTBLVW_H__

#include <CCSCedaData.h>
#include <ccsobj.h>
#include <CCSPtrArray.h>
#include <AllocData.h>
#include <CedaDpxData.h>

#include <cviewer.h>
#include <table.h>
#include <ccsprint.h>

#include <FieldConfigDlg.h>

struct PRMDATA_LINE
{

	DPXDATA *dpx;
	CString Flno;  // Formatted flight Number
	CString Tfln;  // Transit flight Number
	CTime	Etai;  // Time of arrival flight
	CTime	Etdi;  // Time of departure flight
	CString HasJobs;
	CString	Service;	// Service Code
	CStringArray	Services;	// Service Code for a demand or job
	CStringArray	Staffs;	// Staff Name
	CStringArray	FromTos;	// Job From-To
	CStringArray	Functions;	// Job Function

	PRMDATA_LINE(void)
	{
		dpx				= NULL;
		Flno			= "";
		Tfln			= "";
		Etai			= TIMENULL;
		Etdi			= TIMENULL;
		HasJobs			= "";
		Service			= "";
	}

	PRMDATA_LINE& operator=(const PRMDATA_LINE& rhs)
	{
		if (&rhs != this)
		{
			this->dpx				= rhs.dpx;
			this->Flno			= rhs.Flno;
			this->Tfln			= rhs.Tfln;
			this->Etai			= rhs.Etai;
			this->Etdi			= rhs.Etdi;
			this->HasJobs			= rhs.HasJobs;
			this->Service			= rhs.Service;

			this->Services.RemoveAll();
			this->Services.Append(rhs.Services);

			this->Staffs.RemoveAll();
			this->Staffs.Append(rhs.Staffs);

			this->FromTos.RemoveAll();
			this->FromTos.Append(rhs.FromTos);

			this->Functions.RemoveAll();
			this->Functions.Append(rhs.Functions);
		}

		return *this;
	}

	
};

struct DPXDATA_FIELD
{
	CString	Field;
	CString	Name;
	int		Length;
	int		PrintLength;

	DPXDATA_FIELD(const char *pcpField,const char *pcpName,int ipLength,int ipPrintLength)
	{
		Field = pcpField;
		Name  = pcpName;
		Length= ipLength;
		PrintLength = ipPrintLength; 
	};

	DPXDATA_FIELD()
	{
		Length		= 0;
		PrintLength = 0;
	};
};

class	CCSTable;
struct	TABLE_COLUMN;

class PrmTableViewer: public CViewer
{
// Constructions
public:
			PrmTableViewer();
    ~		PrmTableViewer();

    void	Attach(CCSTable *popAttachWnd);
	void	ChangeViewTo(const char *pcpViewName, CString opDate);
    void	ChangeViewTo(const char *pcpViewName);
	CTime	StartTime() const;
	CTime	EndTime() const;
	int		Lines() const;
	PRMDATA_LINE*	GetLine(int ipLine);
	void	SetStartTime(CTime opStartTime);
	void	SetEndTime(CTime opStartTime);
	void	AddLine(DPXDATA *prpDpx);
	static void		GetAllViewColumns(CStringArray& ropColumnFields);
	static void		GetDefaultViewColumns(CStringArray& ropColumnFields);
	static CString	GetViewColumnName(const CString& ropColumnField);
	static CString	GetViewColumnField(const CString& ropColumnName);

	BOOL IsPassCurrentFilter(DPXDATA *rpDpx); //igu
	void UpdateCurrentDisplay(); //igu

private:
	void	PrepareGrouping();
    void	PrepareFilter();
	void	PrepareSorter();
	int		ComparePrm(PRMDATA_LINE *prpPrm1, PRMDATA_LINE *prpPrm2);
	BOOL	IsPassFilter(DPXDATA *rpDpx);
	BOOL	IsSameGroup(PRMDATA_LINE *prpLine1, PRMDATA_LINE *prpLine2);
	CString	GetServicesForDpx(long lpUrno);

	void	MakeLines();
	void	MakeLines(const CString& ropGroup);
	void	MakeLinesWhenGroupByTerminal(const CString& ropGroupName);
	void	MakeLinesWhenGroupByHall(const CString& ropGroupName);
	void	MakeLinesWhenGroupByRegion(const CString& ropGroupName);
	void	MakeLinesWhenGroupByLine(const CString& ropGroupName);

	void	MakeLine(DPXDATA *bppDpxData);
	void	MakeLine(const CString& ropCicName,const CString& ropGroupName,const CString& ropDeskType);
	BOOL	FindDesk(const CString &ropDesk, int &rilLineno);
	BOOL	FindAssignment(long lpJobUrno, int &ripLineno, int &ripAssignmentno);
	void	DeleteAll();
	int		CreateLine(PRMDATA_LINE *prpPrm);

	bool	EvaluateTableFields();
	int		TableFieldIndex(const CString& ropField);

	void	UpdateDisplay(BOOL bpEraseAll = TRUE);
	void    makeLineColor(COLORREF& textColor,COLORREF& backGroundColor);
	void    getHighLightColor(COLORREF& textColor,COLORREF& backGroundColor);
	void	Format(PRMDATA_LINE *prpLine,CCSPtrArray<TABLE_COLUMN>& rrpColumns);

// Attributes used for filtering condition
private:
    CWordArray		omSortOrder;
	CMapStringToPtr omCMapForName;
	CMapStringToPtr omCMapForType;
	CMapStringToPtr omCMapForAlcd;
	CMapStringToPtr omCMapForServices;
	CMapStringToPtr omCMapForPrmts;
	//added by MAX
	CMapStringToPtr omCMapForPrmLAs;

	CMapStringToPtr omCMapForAdids;
	CMapStringToPtr omCMapForTerms;
	
	bool			bmUseAllAlcs;
	bool			bmUseAllServices;
	bool			bmUseAllAdids;
	bool			bmUseAllPrmts;
	//added by MAX
	bool            bmUseAllPrmLAs;

	int				imMaxJobs;
	bool			bmFilterTransitFlight;
	bool			bmUseAllTerms;//Singapore

	bool			bmEmptyTermSelected; //Singapore

// Attributes
private:
//    CTable*						pomTable;
	CCSTable*						pomTable;
    CCSPtrArray<PRMDATA_LINE>	omLines;
    CString						omDate;
	CTime						omStartTime;
	CTime						omEndTime;
	CCSPrint*					pomPrint;
	CCSPtrArray <DPXDATA_FIELD> omTableFields;
	CCSPtrArray <TABLE_COLUMN> omColumns;
	void MakeColumns(void);

// Methods which handle changes (from Data Distributor)
public:
	static void PrmTableCf(void *popInstance, int ipDDXType,void *vpDataPointer, CString &ropInstanceName);
	BOOL ProcessPrmNew(DPXDATA *prpDpx);
	BOOL ProcessPrmDelete(DPXDATA *prpDpx);
	BOOL ProcessPrmUpdate(DPXDATA *prpDpx); // lli: create the update function to remove the chance of accessing null pointer
	BOOL ProcessFlightChange(FLIGHTDATA *prpFlight); // lli: for updating STD and STA column
	BOOL ProcessJobNew(JOBDATA *prpJob);	// lli: for updating Jobs and Job Assignments columns
	BOOL ProcessJobDelete(JOBDATA *prpJob); // lli: for updating Jobs and Job Assignments columns
	BOOL ProcessJobChange(JOBDATA *prpJob); // lli: for updating Jobs and Job Assignments columns

////////////////////////////////////////////////////////////////////////////////////////////
// Printing functions

private:
	BOOL	PrintPRMLine(PRMDATA_LINE *prpLine,BOOL bpIsLastLine);
	BOOL	PrintPRMHeader(CCSPrint *pomPrint);
public:
	void	PrintView();
	void	CreateExport(const char *pclFileName, const char *pclSeperator, CString opTitle); //Print to excel file


};

#endif //__PRMTBLVW_H__
