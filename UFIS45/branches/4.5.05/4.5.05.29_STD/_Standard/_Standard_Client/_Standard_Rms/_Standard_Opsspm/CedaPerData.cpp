//
// CedaPerData.cpp - Permits Stammdaten (aka Qualifications)
//
#include <afxwin.h>
#include <ccsglobl.h>
#include <CCSPtrArray.h>
#include <CCSCedaData.h>
#include <CedaPerData.h>
#include <BasicData.h>

void ProcessPerCf(void * popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

CedaPerData::CedaPerData()
{                  
    BEGIN_CEDARECINFO(PERDATA, PerDataRecInfo)
		FIELD_LONG(Urno,"URNO")
		FIELD_CHAR_TRIM(Prmc,"PRMC")
		FIELD_CHAR_TRIM(Prmn,"PRMN")
    END_CEDARECINFO

    // Copy the record structure
    for (int i = 0; i < sizeof(PerDataRecInfo)/sizeof(PerDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&PerDataRecInfo[i],sizeof(CEDARECINFO));
        omRecInfo.Add(prpCedaRecInfo);
	}

	ogCCSDdx.Register((void *)this, BC_PER_INSERT, CString("PERDATA"), CString("Per changed"),ProcessPerCf);
	ogCCSDdx.Register((void *)this, BC_PER_UPDATE, CString("PERDATA"), CString("Per changed"),ProcessPerCf);
	ogCCSDdx.Register((void *)this, BC_PER_DELETE, CString("PERDATA"), CString("Per deleted"),ProcessPerCf);

    // Initialize table names and field names
    strcpy(pcmTableName,"PERTAB");
    pcmFieldList = "URNO,PRMC,PRMN";
}

void ProcessPerCf(void * popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	((CedaPerData *)popInstance)->ProcessPerBc(ipDDXType,vpDataPointer,ropInstanceName);
}

void CedaPerData::ProcessPerBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlPerData = (struct BcStruct *) vpDataPointer;
	ogBasicData.LogBroadcast(prlPerData);
	PERDATA rlPer, *prlPer = NULL;
	long llUrno = GetUrnoFromSelection(prlPerData->Selection);
	GetRecordFromItemList(&rlPer,prlPerData->Fields,prlPerData->Data);
	if(llUrno == 0L) llUrno = rlPer.Urno;

	switch(ipDDXType)
	{
		case BC_PER_INSERT:
		{
			if((prlPer = AddPerInternal(rlPer)) != NULL)
			{
				ogCCSDdx.DataChanged((void *)this, PER_INSERT, (void *)prlPer);
			}
			break;
		}
		case BC_PER_UPDATE:
		{
			if((prlPer = GetPerByUrno(llUrno)) != NULL)
			{
				GetRecordFromItemList(prlPer,prlPerData->Fields,prlPerData->Data);
				//PrepareDataAfterRead(prlPer);
				ogCCSDdx.DataChanged((void *)this, PER_UPDATE, (void *)prlPer);
			}
			break;
		}
		case BC_PER_DELETE:
		{
			if((prlPer = GetPerByUrno(llUrno)) != NULL)
			{
				DeletePerInternal(prlPer->Urno);
				ogCCSDdx.DataChanged((void *)this, PER_DELETE, (void *)prlPer);
			}
			break;
		}
	}
}
 
CedaPerData::~CedaPerData()
{
	TRACE("CedaPerData::~CedaPerData called\n");
	ClearAll();
}

void CedaPerData::ClearAll()
{
	omUrnoMap.RemoveAll();
	omData.DeleteAll();
}

bool CedaPerData::ReadPerData()
{
	CCSReturnCode ilRc = RCSuccess;
	ClearAll();

	char pclCom[10] = "RT";
    char pclWhere[512] = "";
	if ((ilRc = CedaAction2(pclCom, pclWhere)) != RCSuccess)
	{
		ogBasicData.LogCedaError("CedaPerData Error",omLastErrorMessage,pclCom,pcmFieldList,pcmTableName,pclWhere);
	}
	else
	{
		PERDATA rlPerData;
		for (int ilLc = 0; ilRc == RCSuccess; ilLc++)
		{
			if ((ilRc = GetBufferRecord2(ilLc,&rlPerData)) == RCSuccess)
			{
				AddPerInternal(rlPerData);
			}
		}
		ilRc = RCSuccess;
	}

	ogBasicData.Trace("CedaPerData Read %d Records (Rec Size %d) %s",omData.GetSize(), sizeof(PERDATA), pclWhere);
    return ilRc;
}

PERDATA *CedaPerData::AddPerInternal(PERDATA &rrpPer)
{
	PERDATA *prlPer = new PERDATA;
	*prlPer = rrpPer;
	omData.Add(prlPer);
	omUrnoMap.SetAt((void *)prlPer->Urno,prlPer);

	return prlPer;
}

void CedaPerData::DeletePerInternal(long lpUrno)
{
	int ilNumPers = omData.GetSize();
	for(int ilPer = (ilNumPers-1); ilPer >= 0; ilPer--)
	{
		if(omData[ilPer].Urno == lpUrno)
		{
			omData.DeleteAt(ilPer);
		}
	}
	omUrnoMap.RemoveKey((void *)lpUrno);
}

PERDATA *CedaPerData::GetPerByUrno(long lpUrno)
{
	PERDATA *prlPer = NULL;
	omUrnoMap.Lookup((void *)lpUrno, (void *&) prlPer);
	return prlPer;
}

void CedaPerData::GetAllPermits(CStringArray &ropPermits)
{
	int ilNumPers = omData.GetSize();
	for(int ilPer = 0; ilPer < ilNumPers; ilPer++)
	{
		ropPermits.Add(omData[ilPer].Prmc);
	}
}

CString CedaPerData::GetTableName(void)
{
	return CString(pcmTableName);
}