// DJobChange.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// DJobChange dialog

#ifndef _DJOBCHANGE_H_
#define _DJOBCHANGE_H_

#include <CedaJobData.h>

class DJobChange : public CDialog
{
// Construction
public:
	DJobChange(CWnd* pParent,CString opEmpNames,CTime opAcfr,CTime opActo,bool bpAcfrReadOnly = false,bool bpActoReadOnly = false);
	DJobChange(CWnd* pParent,CStringArray &ropEmpNames,CTime opAcfr,CTime opActo,bool bpAcfrReadOnly = false,bool bpActoReadOnly = false);
	DJobChange::DJobChange(CWnd* pParent, JOBDATA *prpJob,bool bpAcfrReadOnly = false,bool bpActoReadOnly = false);
	void Init(CWnd* pParent,CStringArray &ropEmpNames,CTime opAcfr,CTime opActo,bool bpAcfrReadOnly,bool bpActoReadOnly);

	CTime		m_Acfr;
	CTime		m_Acto;
	JOBDATA		*prmJob;

// Dialog Data
protected:
	//{{AFX_DATA(DJobChange)
	enum { IDD = IDD_CHANGEJOB };
	CString		m_EmpName;
	CTime		m_FromDate;
	CTime		m_FromTime;
	CTime		m_ToDate;
	CTime		m_ToTime;
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA

	bool		bmAcfrReadOnly;
	bool		bmActoReadOnly;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(DJobChange)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(DJobChange)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#endif

