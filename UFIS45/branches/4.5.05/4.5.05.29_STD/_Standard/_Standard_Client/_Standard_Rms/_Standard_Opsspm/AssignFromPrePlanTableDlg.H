// AssignFromPrePlanTableDlg.h : header file
//
#ifndef _ASSIGNFROMPREPLANDIALOG_
#define _ASSIGNFROMPREPLANDIALOG_

/////////////////////////////////////////////////////////////////////////////
// CAssignmentFromPrePlanTableDialog dialog

class CAssignmentFromPrePlanTableDialog : public CAssignmentDialog
{
// Construction
public:
	CAssignmentFromPrePlanTableDialog(CWnd* pParent, CCSDragDropCtrl *popDragDropCtrl);

// Dialog Data
	CCSDragDropCtrl *pomDragDropCtrl;
	//{{AFX_DATA(CAssignmentFromPrePlanTableDialog)
	enum { IDD = IDD_ASSIGNMENT };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAssignmentFromPrePlanTableDialog)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	CDWordArray omStartTimeAllowed;	// possible time period for each staff
	CDWordArray omEndTimeAllowed;

	// Generated message map functions
	//{{AFX_MSG(CAssignmentFromPrePlanTableDialog)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////
// CAssignmentFromPrePlanTableToCciAreaDialog dialog

class CAssignmentFromPrePlanTableToCciAreaDialog : public CAssignmentFromPrePlanTableDialog
{
// Construction
public:
	CAssignmentFromPrePlanTableToCciAreaDialog(CWnd* pParent,
		CCSDragDropCtrl *popDragDropCtrl,
		const char *pcpPoolId, const char *pcpCciAreaId, const char *pcpCciAreaName);

// Dialog Data
	CString omPoolId;
	CString omCciAreaId;
	//{{AFX_DATA(CAssignmentFromPrePlanTableToCciAreaDialog)
	enum { IDD = IDD_ASSIGNMENT };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAssignmentFromPrePlanTableToCciAreaDialog)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void OnOK();
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CAssignmentFromPrePlanTableToCciAreaDialog)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////
// CAssignmentFromPrePlanTableToGateAreaDialog dialog

class CAssignmentFromPrePlanTableToGateAreaDialog : public CAssignmentFromPrePlanTableDialog
{
// Construction
public:
	CAssignmentFromPrePlanTableToGateAreaDialog(CWnd* pParent,
		CCSDragDropCtrl *popDragDropCtrl,
		const char *pcpPoolId, const char *pcpGateAreaId, const char *pcpGateAreaName);

// Dialog Data
	CString omPoolId;
	CString omGateAreaId;
	CString omGateAreaName;
	//{{AFX_DATA(CAssignmentFromPrePlanTableToGateAreaDialog)
	enum { IDD = IDD_ASSIGNMENT };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAssignmentFromPrePlanTableToGateAreaDialog)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void OnOK();
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CAssignmentFromPrePlanTableToGateAreaDialog)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////
// CAssignmentFromPrePlanTableToPoolDialog dialog

class CAssignmentFromPrePlanTableToPoolDialog : public CAssignmentFromPrePlanTableDialog
{
// Construction
public:
	CAssignmentFromPrePlanTableToPoolDialog(CWnd* pParent,
		CCSDragDropCtrl *popDragDropCtrl,
		const char *pcpPoolId, const char *pcpPoolName);

// Dialog Data
	CString omPoolId;
	CString omPoolName;
	//{{AFX_DATA(CAssignmentFromPrePlanTableToPoolDialog)
	enum { IDD = IDD_ASSIGNMENT };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAssignmentFromPrePlanTableToPoolDialog)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnCommand(WPARAM wParam, LPARAM lParam);
	virtual void OnOK();
	//}}AFX_VIRTUAL

// Implementation
protected:
	BOOL bmIsJobPool;	// co-working with "m_szOldDescription" -- see SetRadioJobType()
	CString m_szOldDescription;

	// Generated message map functions
	//{{AFX_MSG(CAssignmentFromPrePlanTableToPoolDialog)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

// Helper routines
private:
	void SetRadioJobType();
};
#endif // _ASSIGNFROMPREPLANDIALOG_