// CedaDrsData.cpp - Doppeltour class containing student-teacher combinations
//

#include <afxwin.h>
#include <ccsglobl.h>
#include <CCSPtrArray.h>
#include <CCSCedaData.h>
#include <ccsddx.h>
#include <CCSBcHandle.h>
#include <ccsddx.h>
#include <CedaDrsData.h>
#include <CedaShiftData.h>
#include <BasicData.h>
#include <Resource.h>

extern CCSDdx ogCCSDdx;
void  ProcessDrsCf(void * popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

CedaDrsData::CedaDrsData()
{                  
    BEGIN_CEDARECINFO(DRSDATA, DrsDataRecInfo)
		FIELD_LONG(Urno,"URNO")
		FIELD_LONG(Drru,"DRRU")
		FIELD_LONG(Ats1,"ATS1")
		FIELD_CHAR_TRIM(Stat,"STAT")
    END_CEDARECINFO

    // Copy the record structure
    for (int i = 0; i < sizeof(DrsDataRecInfo)/sizeof(DrsDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&DrsDataRecInfo[i],sizeof(CEDARECINFO));
        omRecInfo.Add(prpCedaRecInfo);
	}

    // Initialize table names and field names
    strcpy(pcmTableName,"DRSTAB");
    pcmFieldList = "URNO,DRRU,ATS1,STAT";

	ogCCSDdx.Register((void *)this,BC_DRS_CHANGE,CString("DRSDATA"), CString("Drs changed"),ProcessDrsCf);
	ogCCSDdx.Register((void *)this,BC_DRS_DELETE,CString("DRSDATA"), CString("Drs deleted"),ProcessDrsCf);
}
 
CedaDrsData::~CedaDrsData()
{
	TRACE("CedaDrsData::~CedaDrsData called\n");
	ClearAll();
	ogCCSDdx.UnRegister(this,NOTUSED);
}


void ProcessDrsCf(void * popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	((CedaDrsData *)popInstance)->ProcessDrsBc(ipDDXType,vpDataPointer,ropInstanceName);
}

void CedaDrsData::ProcessDrsBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlDrsData = (struct BcStruct *) vpDataPointer;
	ogBasicData.LogBroadcast(prlDrsData);
	long llUrno = GetUrnoFromSelection(prlDrsData->Selection);
	DRSDATA *prlDrs = GetDrsByUrno(llUrno);
	//ogBasicData.Trace("DRSTAB BC Cmd <%s>\nFields <%s>\nData <%s>\nSelection <%s>\n",prlDrsData->Cmd,prlDrsData->Fields,prlDrsData->Data,prlDrsData->Selection);

	if(ipDDXType == BC_DRS_CHANGE && prlDrs != NULL)
	{
		// update
		//ogBasicData.Trace("BC_DRS_CHANGE - UPDATE DRSTAB URNO %ld\n",prlDrs->Urno); 
		omDrruMap.RemoveKey((void *)prlDrs->Drru);
		GetRecordFromItemList(prlDrs,prlDrsData->Fields,prlDrsData->Data);
		omDrruMap.SetAt((void *)prlDrs->Drru,prlDrs);
		ogCCSDdx.DataChanged(this,DRS_CHANGE,prlDrs);
	}
	else if(ipDDXType == BC_DRS_CHANGE && prlDrs == NULL)
	{
		// insert
		DRSDATA rlDrs;
		GetRecordFromItemList(&rlDrs,prlDrsData->Fields,prlDrsData->Data);
		//ogBasicData.Trace("BC_DRS_CHANGE - INSERT DRSTAB URNO %ld\n",rlDrs.Urno);
		AddDrsInternal(rlDrs);
		ogCCSDdx.DataChanged(this,DRS_CHANGE,&rlDrs);
	}
	else if(ipDDXType == BC_DRS_DELETE && prlDrs != NULL)
	{
		// delete
		//ogBasicData.Trace("BC_DRS_DELETE - DELETE DRSTAB URNO %ld\n",prlDrs->Urno);
		long llDrru = prlDrs->Drru;
		DeleteDrsInternal(prlDrs->Urno);
		ogCCSDdx.DataChanged(this,DRS_DELETE,(void *)llDrru);
	}

}

void CedaDrsData::SendShiftUpdate(long lpDrru)
{
	// send shift update for the teacher
	SHIFTDATA *prlShift = ogShiftData.GetShiftByUrno(lpDrru);
	if(prlShift != NULL)
	{
		ogCCSDdx.DataChanged((void *)this,SHIFT_CHANGE,(void *)prlShift);
	}
/*	EMPDATA *prlEmp = ogEmpData.GetEmpByUrno(lpAts1);
	if(prlEmp != NULL && (prlShift = ogShiftData.GetShiftByPeno(prlEmp->Peno)) != NULL)
	{
		ogCCSDdx.DataChanged((void *)this,SHIFT_CHANGE,(void *)prlShift);
	}*/
}

void CedaDrsData::ClearAll()
{
	omUrnoMap.RemoveAll();
	omDrruMap.RemoveAll();
	omData.DeleteAll();
}

bool CedaDrsData::ReadDrsData()
{
	CCSReturnCode ilRc = RCSuccess;
	ClearAll();

	char pclCom[10] = "RT";
    char pclWhere[512] = "";
    sprintf(pclWhere,"WHERE SDAY BETWEEN '%s' AND '%s'", ogBasicData.omShiftFirstSDAY.Format("%Y%m%d"), 
														 ogBasicData.omShiftLastSDAY.Format("%Y%m%d"));
	if ((ilRc = CedaAction2(pclCom, pclWhere)) != RCSuccess)
	{
		ogBasicData.LogCedaError("CedaDrsData Error",omLastErrorMessage,pclCom,pcmFieldList,pcmTableName,pclWhere);
	}
	else
	{
		DRSDATA rlDrsData;
		for (int ilLc = 0; ilRc == RCSuccess; ilLc++)
		{
			if ((ilRc = GetBufferRecord2(ilLc,&rlDrsData)) == RCSuccess)
			{
				AddDrsInternal(rlDrsData);
			}
		}
		ilRc = RCSuccess;
	}

	ogBasicData.Trace("CedaDrsData Read %d Records (Rec Size %d) %s",omData.GetSize(), sizeof(DRSDATA), pclWhere);
    return ilRc;
}


void CedaDrsData::AddDrsInternal(DRSDATA &rrpDrs)
{
	DRSDATA *prlDrs = new DRSDATA;
	*prlDrs = rrpDrs;
	omData.Add(prlDrs);
	omUrnoMap.SetAt((void *)prlDrs->Urno,prlDrs);
	omDrruMap.SetAt((void *)prlDrs->Drru,prlDrs);
}

void CedaDrsData::DeleteDrsInternal(long lpUrno)
{
	int ilNumDrss = omData.GetSize();
	long llDrru = 0L;
	for(int ilDel = (ilNumDrss-1); ilDel >= 0; ilDel--)
	{
		if(omData[ilDel].Urno == lpUrno)
		{
			llDrru = omData[ilDel].Drru;
			omData.DeleteAt(ilDel);
		}
	}
	omUrnoMap.RemoveKey((void *)lpUrno);
	if(llDrru != 0L)
	{
		omDrruMap.RemoveKey((void *)llDrru);
	}
}

DRSDATA* CedaDrsData::GetDrsByUrno(long lpUrno)
{
	DRSDATA *prlDrs = NULL;
	omUrnoMap.Lookup((void *)lpUrno, (void *&) prlDrs);
	return prlDrs;
}

DRSDATA* CedaDrsData::GetDrsByDrru(long lpDrru)
{
	DRSDATA *prlDrs = NULL;
	omDrruMap.Lookup((void *)lpDrru, (void *&) prlDrs);
	return prlDrs;
}

bool CedaDrsData::IsStudent(long lpDrru)
{
	bool blIsStudent = false;
	DRSDATA *prlDrs = GetDrsByDrru(lpDrru);
	if(prlDrs != NULL && *prlDrs->Stat == CEDADRSDATA_STUDENT)
	{
		blIsStudent = true;
	}

	return blIsStudent;
}

// given a DRR.URNO (shift URNO) check if the emp is a teacher and if so return 
// the STF.URNO (Staff URNO) for the corresponding student else return 0L
long CedaDrsData::GetStudentForTeacher(long lpDrru)
{
	long llStfu = 0L;
	DRSDATA *prlDrs = GetDrsByDrru(lpDrru);
	if(prlDrs != NULL && *prlDrs->Stat == CEDADRSDATA_TEACHER)
	{
		llStfu = prlDrs->Ats1;
	}

	return llStfu;
}

CString CedaDrsData::GetTableName(void)
{
	return CString(pcmTableName);
}