// pptable.cpp - Class for performing the pre-planning functionality for one team
//
//
// Written by:
// MWO: 18.09.1996
//
// Modification History:
// 

#include <time.h>
#include <stdlib.h>
#include <stdafx.h>
#include <ccsglobl.h>
#include <ccsobj.h>
#include <resource.h>
#include <ufis.h>
#include <CCSCedaCom.h>
#include <CCSCedaData.h>
#include <table.h>
#include <StaffFilterDlg.h>
#include <StaffSortDlg.h>
#include <CedaEmpData.h>
#include <CedaJobData.h>
#include <CedaShiftData.h>
#include <CedaEmpData.h>
#include <EmpDialog.h>
#include <CCSDragDropCtrl.h>

#include <cviewer.h>
#include <FilterPage.h>
#include <ZeitPage.h>
#include <PrePlanTableSortPage.h>
#include <PrePlanTable.h>
#include <PrePlanTeamTable.h>
#include <BasePropertySheet.h>
#include <PrePlanTablePropertySheet.h>
#include <ccsddx.h>
#include <BasicData.h> 
#include <PrePlanTableViewer.h>
#include <CedaOdaData.h>


/////////////////////////////////////////////////////////////////////////////
// PrePlanTeamTable dialog

PrePlanTeamTable::PrePlanTeamTable(CWnd* pParent,CString opTeamID, 
	CCSPtrArray<PREPLANT_SHIFTDATA> *popTeamMember, int ipItemHeight)
	: CDialog(PrePlanTeamTable::IDD, pParent)
{
    //{{AFX_DATA_INIT(PrePlanTeamTable) 
    //}}AFX_DATA_INIT

	pomParent = pParent;
	imItemHeight = ipItemHeight;
	if(popTeamMember != NULL)
	{
		pomTeamMember = popTeamMember;
	}
	else
	{
		pomTeamMember = NULL;
	}
	omTeamID = opTeamID;

    CDialog::Create(PrePlanTeamTable::IDD);
}

void PrePlanTeamTable::SelectNewTeam(CString opTeamID, CCSPtrArray<PREPLANT_SHIFTDATA> *popTeamMember, int ipItemHeight)
{
	imItemHeight = ipItemHeight;
	if(popTeamMember != NULL)
	{
		pomTeamMember = popTeamMember;
	}
	else
	{
		pomTeamMember = NULL;
	}
	omTeamID = opTeamID;

	UpdateView();
	omDate = omViewer.StartTime().Format("%Y%m%d");
	omViewer.UpdateDisplay(pomTeamMember);
	omTable.DisplayTable();
}

PrePlanTeamTable::~PrePlanTeamTable()
{
	TRACE("PrePlanTeamTable::~PrePlanTeamTable()\n");
}

CTime PrePlanTeamTable::GetPrePlanTime(void)
{
	if (CString(omViewer.StartTime().Format("%H%M")) == "0000")
	{
		// if pre planning from 00:00 add six hours to preplan mode time
		return omViewer.StartTime() + CTimeSpan(0,6,0,0); 
	}
	else
	{
		return omViewer.StartTime();
	}
}

void PrePlanTeamTable::UpdateView()
{
	AfxGetApp()->DoWaitCursor(1);
	AfxGetApp()->DoWaitCursor(-1);

    // Update dependent push buttons
    //CDialog::SetWindowText(CString("Verf�gbare Mitarbeiter im Team: " + omTeamID));
    CDialog::SetWindowText(CString(GetString(IDS_STRING61323) + omTeamID));
}

void PrePlanTeamTable::DoDataExchange(CDataExchange* pDX)
{
    CDialog::DoDataExchange(pDX);
    //{{AFX_DATA_MAP(PrePlanTeamTable)
    //}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(PrePlanTeamTable, CDialog)
    //{{AFX_MSG_MAP(PrePlanTeamTable)
	ON_WM_DESTROY()
	ON_WM_ERASEBKGND()
    ON_WM_PAINT()
    ON_WM_SIZE()
    ON_MESSAGE(WM_TABLE_DRAGBEGIN, OnTableDragBegin)
    ON_MESSAGE(WM_TABLE_LBUTTONDBLCLK, OnTableLButtonDblClk)
    ON_MESSAGE(WM_TABLE_RBUTTONDOWN, OnTableRButtonDown)
	ON_COMMAND_RANGE(11, 40,OnMenuDelete)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// PrePlanTeamTable message handlers

BOOL PrePlanTeamTable::OnInitDialog()
{
    CDialog::OnInitDialog();

	SetWindowText(GetString(IDS_STRING61340));
	CWnd *polWnd = GetDlgItem(IDCANCEL); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING33017));
	}

	SetWindowPos(&wndTopMost,0,0,0,0,SWP_NOSIZE|SWP_NOMOVE);

   	omViewer.Attach(&omTable);
	omViewer.SetTeamID(omTeamID);
    
	int ilTmp =0;
	CRect rect;
    GetClientRect(&rect);
	ilTmp = rect.bottom - rect.top;
    rect.OffsetRect(0, m_nDialogBarHeight);
    rect.InflateRect(1, 1);     // hiding the CTable window border

    omTable.SetTableData(this, rect.left, rect.right, rect.top, rect.bottom);
	omContextItem = -1;

	UpdateView();
	omDate = omViewer.StartTime().Format("%Y%m%d");
	omViewer.UpdateDisplay(pomTeamMember);
	omTable.DisplayTable();

	int ilCount = omViewer.Lines();
	if(ilCount > 20)
	{
		ilCount = 20; // force the count of visible items not more than 20
	}

    GetClientRect(&rect);
    m_nDialogBarHeight = rect.bottom - rect.top;

	GetWindowRect(&rect);
	if (pomParent != NULL)
	{
		pomParent->GetWindowRect(&rect);
	}
	int ilHeight = (imItemHeight * ilCount) + (4*imItemHeight) + 6 + ilTmp;
	rect.top = rect.bottom - ilHeight;
	MoveWindow(&rect);
	SetWindowPos(&wndTopMost, 0, 0, 0, 0, SWP_SHOWWINDOW | SWP_NOMOVE | SWP_NOSIZE);

	// Register DDX call back function
	TRACE("PrePlanTeamTable: DDX Registration\n");
	ogCCSDdx.Register(this, REDISPLAY_ALL, CString("PPTEAMTABLE"),CString("Redisplay all"), PrePlanTeamTableCf);

    return TRUE;  // return TRUE  unless you set the focus to a control
}

void PrePlanTeamTable::OnDestroy() 
{
	if (bgModal == TRUE)
		return;
	// Unregister DDX call back function
	TRACE("PrePlanTeamTable: DDX Unregistration %s\n",
		::IsWindow(GetSafeHwnd())? "": "(window is already destroyed)");
    ogCCSDdx.UnRegister(this, NOTUSED);

	CDialog::OnDestroy();
}

void PrePlanTeamTable::OnCancel()
{
	// Normally, we should just call CDialog::OnCancel().
	// However, the DestroyWindow() is required since this table can be destroyed in two ways.
	// First is by pressing ESC key which will call this routine.
	// Second, it may be destroyed directly by calling Manfred's DestroyWindow() in this class
	// which is not so compatible with MFC.
	DestroyWindow();
}

BOOL PrePlanTeamTable::OnEraseBkgnd(CDC* pDC) 
{
	return CDialog::OnEraseBkgnd(pDC);
}

void PrePlanTeamTable::OnPaint()
{
    CPaintDC dc(this); // device context for painting
    
    CRect rect;
    GetUpdateRect(&rect);
    if (omTable.m_hWnd != NULL)
    {
        MapWindowPoints(&omTable, &rect);
        omTable.InvalidateRect(&rect, FALSE);
    }
}

void PrePlanTeamTable::OnSize(UINT nType, int cx, int cy)
{
    CDialog::OnSize(nType, cx, cy);
    
    if (nType != SIZE_MINIMIZED)
        omTable.SetPosition(-1, cx+1, m_nDialogBarHeight-1, cy+1);
}

LONG PrePlanTeamTable::OnTableLButtonDblClk(UINT wParam, LONG lParam)
{
	return 0L;
}

LONG PrePlanTeamTable::OnTableRButtonDown(UINT ipItem, LONG lpLParam) 
{
	CMenu menu;
	CPoint olPoint(LOWORD(lpLParam),HIWORD(lpLParam));

	omContextItem = -1;
	omAssignUrnos.RemoveAll();

	PREPLANT_LINEDATA *prlLine = (PREPLANT_LINEDATA *)omTable.GetTextLineData(ipItem);
	if (prlLine != NULL)
	{
		int ilLc;

		menu.CreatePopupMenu();
		for (ilLc = menu.GetMenuItemCount(); --ilLc >= 0;)
			menu.RemoveMenu(ilLc, MF_BYPOSITION);
		for ( ilLc = 0; ilLc < prlLine->Line.Assignment.GetSize(); ilLc++)
		{
			//CString olMenuText = "L�schen: Einsatz " + 
			CString olMenuText = GetString(IDS_STRING61380) + 
				prlLine->Line.Assignment[ilLc].Alid + " " +
				prlLine->Line.Assignment[ilLc].Acfr.Format("%H%M") + " - " +
				prlLine->Line.Assignment[ilLc].Acto.Format("%H%M");
				menu.AppendMenu(MF_STRING,11 + ilLc, olMenuText);	
			omAssignUrnos.Add(prlLine->Line.Assignment[ilLc].Urno);
		}
		if ( prlLine->Line.Assignment.GetSize() > 0)
		{
			ClientToScreen( &olPoint);
			menu.TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, olPoint.x, olPoint.y + 55, this, NULL);
			omContextItem = prlLine->Line.ShiftUrno;
		}
	}
	return 0L;
}

void PrePlanTeamTable::OnMenuDelete(UINT nID)
{
	DeleteAssignment(nID-11);
}

void PrePlanTeamTable::DeleteAssignment(int ipIndex)
{
	if (omContextItem != -1)
	{
		int ilItem = -1;
		if (omViewer.FindShift(omContextItem,ilItem))
		{
			PREPLANT_LINEDATA *prlLine = (PREPLANT_LINEDATA *)omTable.GetTextLineData(ilItem);
			if (prlLine && ipIndex < prlLine->Line.Assignment.GetSize() && prlLine->Line.Assignment[ipIndex].Urno == omAssignUrnos[ipIndex])												
				omViewer.DeleteAssignmentJob(ilItem,ipIndex);
		}
	}

	omContextItem = -1;
}

BOOL PrePlanTeamTable::DestroyWindow() 
{
	BOOL blRc = CDialog::DestroyWindow();
	pomParent->SendMessage(WM_PREPLANTEAMTABLE_EXIT);
	delete this;
	return blRc;
}

////////////////////////////////////////////////////////////////////////
// PrePlanTable -- implementation of DDX call back function

void PrePlanTeamTable::PrePlanTeamTableCf(void *popInstance, int ipDDXType,void *vpDataPointer, CString &ropInstanceName)
{
	PrePlanTeamTable *polTable = (PrePlanTeamTable *)popInstance;

	if (ipDDXType == REDISPLAY_ALL)
	{
		polTable->UpdateView();
	}
}

/////////////////////////////////////////////////////////////////////////////
// PrePlanTeamTable -- drag-and-drop functionalities
//
// The user may drag from:
//	- any non-empty line for create a DutyBar in the StaffDiagram.
//
LONG PrePlanTeamTable::OnTableDragBegin(UINT wParam, LONG lParam)
{
	CListBox *pListBox = (CListBox *)lParam;
	int nMaxItems;
	if ((nMaxItems = pListBox->GetSelCount()) == LB_ERR)
		return 0L;

/////////////////////////////////////////////////////////////////////////////
// DTT - Jul.18
// Change the algorithm for creating data structure on dragging from
// PrePlanTeamTable. We put only still available staff from the PrePlanTeamTable into
// the DragDropObject data structure.
//
	int *rgIndex = new int [nMaxItems];
	pListBox->GetSelItems(nMaxItems, rgIndex);
	int i, ilItemCount = 0;
	for (i = 0; i < nMaxItems; i++)
	{
		PREPLANT_LINEDATA *prlLine = (PREPLANT_LINEDATA *)omTable.GetTextLineData(rgIndex[i]);
		if ((prlLine->Line.AssignmentStatus != FULLY_ASSIGNED) &&
			(ogOdaData.GetOdaByType(prlLine->Line.Sfca) == NULL))
			/*
			(strcmp(prlLine->Line.Sfca,"K") != 0) &&
			(strcmp(prlLine->Line.Sfca,"S") != 0) &&
			(strcmp(prlLine->Line.Sfca,"U") != 0))
			*/
			ilItemCount++;
	}
	if (ilItemCount > 0)
		{
		omDragDropObject.CreateDWordData(DIT_SHIFT, ilItemCount);
		for (i = 0; i < nMaxItems; i++)
		{
			PREPLANT_LINEDATA *prlLine = (PREPLANT_LINEDATA *)omTable.GetTextLineData(rgIndex[i]);
			if ((prlLine->Line.AssignmentStatus != FULLY_ASSIGNED) &&
				(ogOdaData.GetOdaByType(prlLine->Line.Sfca) == NULL))
				/*
				(strcmp(prlLine->Line.Sfca,"K") != 0) &&
				(strcmp(prlLine->Line.Sfca,"S") != 0) &&
				(strcmp(prlLine->Line.Sfca,"U") != 0))
				*/
				omDragDropObject.AddDWord(prlLine->Line.ShiftUrno);
		}
		omDragDropObject.BeginDrag();
		pListBox->SetSel(-1,FALSE);
	}
/////////////////////////////////////////////////////////////////////////////
	delete rgIndex;

    return 0L;
}
