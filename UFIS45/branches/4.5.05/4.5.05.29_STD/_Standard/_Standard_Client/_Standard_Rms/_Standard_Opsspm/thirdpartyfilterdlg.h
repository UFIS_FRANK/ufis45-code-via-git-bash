#if !defined(AFX_THIRDPARTFILTERDLG_H__D025C4C1_114F_11D5_B0D1_0010A4C227B8__INCLUDED_)
#define AFX_THIRDPARTFILTERDLG_H__D025C4C1_114F_11D5_B0D1_0010A4C227B8__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ThirdPartFilterDlg.h : header file
//
#include <ccsglobl.h>

/////////////////////////////////////////////////////////////////////////////
// CThirdPartyFilterDlg dialog

class CThirdPartyFilterDlg : public CDialog
{
// Construction
public:
	CThirdPartyFilterDlg(CWnd* pParent = NULL);   // standard constructor
	void SetSelectedFlights(CMapPtrToPtr &ropSelectedFlights);
	void GetSelectedFlights(CMapPtrToPtr &ropSelectedFlights);
	CMapPtrToPtr omSelectedFlightUrnos;
	void *pvmDummy;

// Dialog Data
	//{{AFX_DATA(CThirdPartyFilterDlg)
	enum { IDD = IDD_THIRDPARTYFILTERDLG };
	CListBox	m_SelectedFlights;
	CListBox	m_PossibleFlights;
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CThirdPartyFilterDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CThirdPartyFilterDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnRemoveFlight();
	afx_msg void OnAddFlight();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_THIRDPARTFILTERDLG_H__D025C4C1_114F_11D5_B0D1_0010A4C227B8__INCLUDED_)
