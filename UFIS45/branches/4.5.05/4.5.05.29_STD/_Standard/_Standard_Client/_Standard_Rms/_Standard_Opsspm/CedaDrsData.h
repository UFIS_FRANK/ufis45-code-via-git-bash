// CedaDrsData.h - Doppeltour class containing student-teacher combinations
//
#ifndef _CEDADRSDATA_H_
#define _CEDADRSDATA_H_

#include <CCSCedaData.h>
#include <CCSPtrArray.h>

#define CEDADRSDATA_STUDENT 'S'
#define CEDADRSDATA_TEACHER 'T'

/////////////////////////////////////////////////////////////////////////////
// Record structure declaration

struct DrsDataStruct
{
	long	Urno;		// Unique Record Number
	long	Drru;		// Shift URNO in DRRTAB
	long	Ats1;		// Staff URNO in STFTAB of the student (for a teacher) or a teacher (for a student)
	char	Stat[2];	// Status field where 'T' = teacher and 'S' = student
};

typedef struct DrsDataStruct DRSDATA;


/////////////////////////////////////////////////////////////////////////////
// Class declaration

class CedaDrsData: public CCSCedaData
{

// Attributes
public:
    CCSPtrArray <DRSDATA> omData;
	CMapPtrToPtr omUrnoMap;
	CMapPtrToPtr omDrruMap;
	CString GetTableName(void);
	void SendShiftUpdate(long lpDrru);

// Operations
public:
	CedaDrsData();
	~CedaDrsData();

	bool ReadDrsData();
	DRSDATA* GetDrsByUrno(long lpUrno);
	DRSDATA* GetDrsByDrru(long lpDrru);
	bool IsStudent(long lpDrru);
	long GetStudentForTeacher(long lpDrru);
	void ProcessDrsBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

private:
	void AddDrsInternal(DRSDATA &rrpDrs);
	void DeleteDrsInternal(long lpUrno);
	void ClearAll();
};


extern CedaDrsData ogDrsData;
#endif _CEDADRSDATA_H_
