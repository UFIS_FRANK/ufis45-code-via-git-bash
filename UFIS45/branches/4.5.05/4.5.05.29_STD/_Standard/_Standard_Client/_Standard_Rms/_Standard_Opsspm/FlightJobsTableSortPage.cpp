// FlightJobsTableSortPage.cpp : implementation file
//

#include <stdafx.h>
#include <OpssPm.h>
#include <CCSGlobl.h>
#include <FlightJobsTableSortPage.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

// Change this key identifiers for name used in the registry database
#define NUMBER_OF_SORTKEYS	6
static CString ogSortKeys[NUMBER_OF_SORTKEYS] =
	{ "Flight", "Act3", "Adid", "Time1", "Time2", "none" };
#define NOSORT	(NUMBER_OF_SORTKEYS - 1)	// must be "none"

/////////////////////////////////////////////////////////////////////////////
// FlightJobsTableSortPage property page

IMPLEMENT_DYNCREATE(FlightJobsTableSortPage, CPropertyPage)

FlightJobsTableSortPage::FlightJobsTableSortPage() : CPropertyPage(FlightJobsTableSortPage::IDD)
{
	//{{AFX_DATA_INIT(FlightJobsTableSortPage)
	m_SortOrder0 = -1;
	m_SortOrder1 = -1;
	m_SortOrder2 = -1;
	m_DisplayArr = FALSE;
	m_DisplayDep = FALSE;
	//}}AFX_DATA_INIT
	omTitle = GetString(IDS_STRING32900);
	m_psp.pszTitle = omTitle;
	m_psp.dwFlags |= PSP_USETITLE;

	omUpArrow.LoadBitmap(IDB_UPARROW);
	omDownArrow.LoadBitmap(IDB_DOWNARROW);
	bmTime1Ascending = true;
	bmTime2Ascending = true;
}

FlightJobsTableSortPage::~FlightJobsTableSortPage()
{
}

void FlightJobsTableSortPage::DoDataExchange(CDataExchange* pDX)
{
	// Extended data exchange -- for member variables with Value type
	if (pDX->m_bSaveAndValidate == FALSE)
	{
		omSortOrders.SetSize(3);
		m_SortOrder0 = GetSortOrder(omSortOrders[0]);
		m_SortOrder1 = GetSortOrder(omSortOrders[1]);
		m_SortOrder2 = GetSortOrder(omSortOrders[2]);
		SetDirectionButton(IDC_TIME2DIRECTION, bmTime2Ascending);
		SetDirectionButton(IDC_TIME1DIRECTION, bmTime1Ascending);
	}

	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(FlightJobsTableSortPage)
	DDX_Radio(pDX, IDC_SORTRADIO_FLIGHT1, m_SortOrder0);
	DDX_Radio(pDX, IDC_SORTRADIO_FLIGHT2, m_SortOrder1);
	DDX_Radio(pDX, IDC_SORTRADIO_FLIGHT3, m_SortOrder2);
	DDX_Check(pDX, IDC_DISPLAYARR, m_DisplayArr);
	DDX_Check(pDX, IDC_DISPLAYDEP, m_DisplayDep);
	//}}AFX_DATA_MAP

	// Extended data validation
	if (pDX->m_bSaveAndValidate == TRUE)
	{
		omSortOrders.RemoveAll();
		omSortOrders.Add(GetSortKey(m_SortOrder0));
		omSortOrders.Add(GetSortKey(m_SortOrder1));
		omSortOrders.Add(GetSortKey(m_SortOrder2));
	}
}


BEGIN_MESSAGE_MAP(FlightJobsTableSortPage, CPropertyPage)
	//{{AFX_MSG_MAP(FlightJobsTableSortPage)
	ON_BN_CLICKED(IDC_TIME2DIRECTION, OnTime2Direction)
	ON_BN_CLICKED(IDC_TIME1DIRECTION, OnTime1Direction)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// FlightJobsTableSortPage message handlers

BOOL FlightJobsTableSortPage::OnCommand(WPARAM wParam, LPARAM lParam) 
{
	BOOL bResult = CPropertyPage::OnCommand(wParam, lParam);

	if (HIWORD(wParam) == BN_CLICKED)
	{
		UpdateData();	// caution: this assume there is always no error

		// Disallow using the same sorting order more than once
		if (m_SortOrder1 == m_SortOrder0)
			omSortOrders[1] = "none";
		if (m_SortOrder2 == m_SortOrder0 || m_SortOrder2 == m_SortOrder1)
			omSortOrders[2] = "none";
		UpdateData(FALSE);
	}
	return bResult;
}

/////////////////////////////////////////////////////////////////////////////
// FlightJobsTableSortPage -- helper routines

int FlightJobsTableSortPage::GetSortOrder(const char *pcpSortKey)
{
	for (int i = 0; i < NUMBER_OF_SORTKEYS; i++)
		if (ogSortKeys[i] == pcpSortKey)
			return i;

	// If there is no sorting order matched, assume "none" for no sorting
	return NOSORT;
}

CString FlightJobsTableSortPage::GetSortKey(int ipSortOrder)
{
	if (0 <= ipSortOrder && ipSortOrder <= NUMBER_OF_SORTKEYS-1)
		return ogSortKeys[ipSortOrder];

	return "";	// invalid sort order
}

BOOL FlightJobsTableSortPage::OnInitDialog() 
{
	CPropertyPage::OnInitDialog();

	SetTitle(IDC_SORTTITLE_FLIGHT, IDS_SORTTITLE_FLIGHT);
	SetTitle(IDC_SORTTITLE_ACT3, IDS_SORTTITLE_ACT3);
	SetTitle(IDC_SORTTITLE_TIME1, IDS_SORTTITLE_TIME1);
	SetTitle(IDC_SORTTITLE_TIME2, IDS_SORTTITLE_TIME2);
	SetTitle(IDC_SORTTITLE_ACT3, IDS_SORTTITLE_ACT3);
	SetTitle(IDC_SORTTITLE_NONE, IDS_SORTTITLE_NONE);
	SetTitle(IDC_DISPLAYARR, IDS_DISPLAYARR);
	SetTitle(IDC_DISPLAYDEP, IDS_DISPLAYDEP);
	
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void FlightJobsTableSortPage::SetTitle(UINT ipControlId, UINT ipTextId)
{
	CWnd *polWnd = GetDlgItem(ipControlId); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(ipTextId));
	}
}

void FlightJobsTableSortPage::OnTime2Direction() 
{
	bmTime2Ascending = !bmTime2Ascending;
	SetDirectionButton(IDC_TIME2DIRECTION, bmTime2Ascending);
	SetFocus();
}

void FlightJobsTableSortPage::OnTime1Direction() 
{
	bmTime1Ascending = !bmTime1Ascending;
	SetDirectionButton(IDC_TIME1DIRECTION, bmTime1Ascending);
	SetFocus();
}

bool FlightJobsTableSortPage::SetDirectionButton(int ipButtonId, bool bpAscending)
{
	bool blRc = false;
	CButton *prlButton = (CButton *) GetDlgItem(ipButtonId);
	if(prlButton != NULL)
	{
		blRc = true;
		if(bpAscending)
		{
			prlButton->SetBitmap(omDownArrow);
		}
		else
		{
			prlButton->SetBitmap(omUpArrow);
		}
	}

	return blRc;
}
