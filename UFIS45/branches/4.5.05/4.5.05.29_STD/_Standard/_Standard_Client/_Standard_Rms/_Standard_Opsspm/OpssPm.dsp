# Microsoft Developer Studio Project File - Name="OpssPm" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Application" 0x0101

CFG=OpssPm - Win32 WatsonDebug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "OpssPm.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "OpssPm.mak" CFG="OpssPm - Win32 WatsonDebug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "OpssPm - Win32 Debug" (based on "Win32 (x86) Application")
!MESSAGE "OpssPm - Win32 Release" (based on "Win32 (x86) Application")
!MESSAGE "OpssPm - Win32 WatsonDebug" (based on "Win32 (x86) Application")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "OpssPm - Win32 Debug"

# PROP BASE Use_MFC 5
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir ".\Debug"
# PROP BASE Intermediate_Dir ".\Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 5
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "C:\Ufis_Bin\debug"
# PROP Intermediate_Dir "C:\Ufis_Intermediate\Opsspm\debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MTd /W3 /Gm /GX /Zi /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /Yu"stdafx.h" /c
# ADD CPP /nologo /MTd /W3 /Gm /GX /ZI /Od /I ".\\" /D "_DEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /Fr /YX /FD /c
# ADD BASE MTL /nologo /D "_DEBUG" /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x41e /d "_DEBUG"
# ADD RSC /l 0x809 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /debug /machine:I386
# ADD LINK32 C:\Ufis_Bin\Debug\ufis32.lib C:\Ufis_Bin\ClassLib\debug\ccsclass.lib Wsock32.lib C:\Ufis_Bin\ForaignLibs\htmlhelp.lib /nologo /subsystem:windows /debug /machine:I386
# SUBTRACT LINK32 /incremental:no

!ELSEIF  "$(CFG)" == "OpssPm - Win32 Release"

# PROP BASE Use_MFC 5
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "OpssPm___Win32_Release"
# PROP BASE Intermediate_Dir "OpssPm___Win32_Release"
# PROP BASE Ignore_Export_Lib 0
# PROP BASE Target_Dir ""
# PROP Use_MFC 5
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "C:\Ufis_Bin\Release"
# PROP Intermediate_Dir "C:\Ufis_Intermediate\Opsspm\debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MTd /W3 /Gm /GX /ZI /Od /I "Q:\SWR\Ufis\Client\Share\Classlib" /D "_DEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /Fr /YX /FD /c
# ADD CPP /nologo /MTd /W3 /Gm /GX /ZI /Od /I ".\\" /D "_DEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /Fr /YX /FD /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x407 /d "_DEBUG"
# ADD RSC /l 0x809 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 C:\Ufis_Bin\Debug\ufis32.lib C:\Ufis_Bin\ClassLib\debug\ccsclass.lib /nologo /subsystem:windows /debug /machine:I386 /out:"C:\Ufis_Bin\Debug/OpssPm.exe" /libpath:"Q:\SWR\Ufis\Client\Share\Classlib\Debug"
# SUBTRACT BASE LINK32 /incremental:no
# ADD LINK32 C:\Ufis_Bin\Release\ufis32.lib C:\Ufis_Bin\ClassLib\debug\ccsclass.lib WSock32.lib C:\Ufis_Bin\ForaignLibs\htmlhelp.lib /nologo /subsystem:windows /debug /machine:I386
# SUBTRACT LINK32 /incremental:no

!ELSEIF  "$(CFG)" == "OpssPm - Win32 WatsonDebug"

# PROP BASE Use_MFC 5
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "OpssPm___Win32_WatsonDebug"
# PROP BASE Intermediate_Dir "OpssPm___Win32_WatsonDebug"
# PROP BASE Ignore_Export_Lib 0
# PROP BASE Target_Dir ""
# PROP Use_MFC 5
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "C:\Ufis_Bin\Debug"
# PROP Intermediate_Dir "C:\Ufis_Intermediate\Opsspm\Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MTd /W3 /Gm /GX /ZI /Od /I ".\\" /D "_DEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /Fr /YX /FD /c
# ADD CPP /nologo /MTd /W3 /GX /Z7 /Od /I ".\\" /D "_DEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /YX /FD /c
# SUBTRACT CPP /Fr
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x407 /d "_DEBUG"
# ADD RSC /l 0x809 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 C:\Ufis_Bin\Debug\ufis32.lib C:\Ufis_Bin\ClassLib\debug\ccsclass.lib Wsock32.lib C:\Ufis_Bin\ForaignLibs\htmlhelp.lib /nologo /subsystem:windows /debug /machine:I386
# SUBTRACT BASE LINK32 /incremental:no
# ADD LINK32 C:\Ufis_Bin\Debug\ufis32.lib C:\Ufis_Bin\ClassLib\debug\ccsclass.lib Wsock32.lib C:\Ufis_Bin\ForaignLibs\htmlhelp.lib /nologo /subsystem:windows /pdb:none /debug /machine:I386

!ENDIF 

# Begin Target

# Name "OpssPm - Win32 Debug"
# Name "OpssPm - Win32 Release"
# Name "OpssPm - Win32 WatsonDebug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;hpj;bat;for;f90"
# Begin Source File

SOURCE=.\3DSTATIC.CPP
# End Source File
# Begin Source File

SOURCE=..\..\_Standard_Share\_Standard_Wrapper\aatlogin.cpp
# End Source File
# Begin Source File

SOURCE=.\AbsenceDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\AllocData.cpp
# End Source File
# Begin Source File

SOURCE=.\AllocDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\AssignConflictDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\AssignDlg.CPP
# End Source File
# Begin Source File

SOURCE=.\AssignFromPrePlanTableDlg.CPP
# End Source File
# Begin Source File

SOURCE=.\AssignJobDetachDlg.CPP
# End Source File
# Begin Source File

SOURCE=.\AutoAssignAz.cpp
# End Source File
# Begin Source File

SOURCE=.\AutoAssignAzViewer.cpp
# End Source File
# Begin Source File

SOURCE=.\AutoAssignDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\AvailableEmpsDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\AvailableEmpsViewer.cpp
# End Source File
# Begin Source File

SOURCE=.\AvailableEquipmentDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\AvailableEquipmentViewer.cpp
# End Source File
# Begin Source File

SOURCE=.\BasePropertySheet.cpp
# End Source File
# Begin Source File

SOURCE=.\BasicData.cpp
# End Source File
# Begin Source File

SOURCE=.\BCDialog2.cpp
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=.\BreakTable.cpp
# End Source File
# Begin Source File

SOURCE=.\BreakTableViewer.cpp
# End Source File
# Begin Source File

SOURCE=.\Buttonct.cpp
# End Source File
# Begin Source File

SOURCE=.\ButtonList.cpp
# End Source File
# Begin Source File

SOURCE=.\CciChart.cpp
# End Source File
# Begin Source File

SOURCE=.\CciDemandDetailDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\CciDeskDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\CciDetailDlg.CPP
# End Source File
# Begin Source File

SOURCE=.\CciDetailGantt.CPP
# End Source File
# Begin Source File

SOURCE=.\CciDetailViewer.CPP
# End Source File
# Begin Source File

SOURCE=.\CciDetailWindow.CPP
# End Source File
# Begin Source File

SOURCE=.\CciDiagram.cpp
# End Source File
# Begin Source File

SOURCE=.\CciDiagramFilterPage.CPP
# End Source File
# Begin Source File

SOURCE=.\CciDiagramGroupPage.CPP
# End Source File
# Begin Source File

SOURCE=.\CciDiagramPropertySheet.CPP
# End Source File
# Begin Source File

SOURCE=.\CCIGANTT.CPP
# End Source File
# Begin Source File

SOURCE=.\CCITable.cpp
# End Source File
# Begin Source File

SOURCE=.\CciTablePropertySheet.cpp
# End Source File
# Begin Source File

SOURCE=.\CciTableSortPage.cpp
# End Source File
# Begin Source File

SOURCE=.\CciTableViewer.cpp
# End Source File
# Begin Source File

SOURCE=.\CCIVIEWER.CPP
# End Source File
# Begin Source File

SOURCE=.\CCSGlobl.cpp
# End Source File
# Begin Source File

SOURCE=.\Ccsobj.cpp
# End Source File
# Begin Source File

SOURCE=.\CCSPrint.cpp
# End Source File
# Begin Source File

SOURCE=.\Ccsstr.cpp
# End Source File
# Begin Source File

SOURCE=.\CCSTree.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaAcrData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaActData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaAloData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaAltData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaAptData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaAzaData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaBlkData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaBltData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaBsdData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaCcaData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaCCCData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaCfgData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaCicData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaCnfData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaDelData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaDemandData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaDlgData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaDprData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaDpxData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaDraData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaDrdData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaDrgData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaDrsData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaEmpData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaEqaData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaEqtData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaEquData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaFlightData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaGatData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaHagData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaHaiData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaInitModuData.Cpp
# End Source File
# Begin Source File

SOURCE=.\CedaJobData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaJodData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaJtyData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaLoaData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaNatData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaOdaData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaPaxData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaPdaData.Cpp
# End Source File
# Begin Source File

SOURCE=.\CedaPeakData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaPerData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaPfcData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaPgpData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaPolData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaPrmData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaPstData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaRemData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaReqData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaRequestData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaRloData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaRnkData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaRpfData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaRpqData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaRudData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaRueData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaSerData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaSgmData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaSgrData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaShiftData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaShiftTypeData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaSpeData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaSpfData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaSprData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaSwgData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaTlxData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaTplData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaValData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaWgpData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaWroData.cpp
# End Source File
# Begin Source File

SOURCE=.\Cformat.cpp
# End Source File
# Begin Source File

SOURCE=.\CheckDataConsistencyDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\cicAutoAssignDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\CiCDemandTable.cpp
# End Source File
# Begin Source File

SOURCE=.\CiCDemandTablePropertySheet.cpp
# End Source File
# Begin Source File

SOURCE=.\CiCDemandTableSortPage.cpp
# End Source File
# Begin Source File

SOURCE=.\CiCDemandTableViewer.cpp
# End Source File
# Begin Source File

SOURCE=.\CicPrintDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\CLIENTWN.CPP
# End Source File
# Begin Source File

SOURCE=.\ConfigTextDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\conflict.cpp
# End Source File
# Begin Source File

SOURCE=.\ConflictConfigTable.cpp
# End Source File
# Begin Source File

SOURCE=.\ConflictTable.cpp
# End Source File
# Begin Source File

SOURCE=.\ConflictTablePropSheet.CPP
# End Source File
# Begin Source File

SOURCE=.\ConflictTableSortPage.CPP
# End Source File
# Begin Source File

SOURCE=.\ConflictViewer.cpp
# End Source File
# Begin Source File

SOURCE=.\CopyDemandDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\CPrintPageDialog.cpp
# End Source File
# Begin Source File

SOURCE=.\CPrintPreviewView.cpp
# End Source File
# Begin Source File

SOURCE=.\CreateDemand.cpp
# End Source File
# Begin Source File

SOURCE=.\Cviewer.cpp
# End Source File
# Begin Source File

SOURCE=.\Cxbutton.cpp
# End Source File
# Begin Source File

SOURCE=.\dataset.cpp
# End Source File
# Begin Source File

SOURCE=.\dbreak.cpp
# End Source File
# Begin Source File

SOURCE=.\DChange.cpp
# End Source File
# Begin Source File

SOURCE=.\DebugCountDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\DebugInfoLogDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\DebugInfoMsgBox.cpp
# End Source File
# Begin Source File

SOURCE=.\DemandDetailDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\DemandFilterPage.cpp
# End Source File
# Begin Source File

SOURCE=.\DemandTable.cpp
# End Source File
# Begin Source File

SOURCE=.\DemandTablePropertySheet.cpp
# End Source File
# Begin Source File

SOURCE=.\DemandTableSortPage.cpp
# End Source File
# Begin Source File

SOURCE=.\DemandTableViewer.cpp
# End Source File
# Begin Source File

SOURCE=.\DGateJob.cpp
# End Source File
# Begin Source File

SOURCE=.\DInfo.cpp
# End Source File
# Begin Source File

SOURCE=.\DJobChange.cpp
# End Source File
# Begin Source File

SOURCE=.\DlgSettings.cpp
# End Source File
# Begin Source File

SOURCE=.\DMreqTable.cpp
# End Source File
# Begin Source File

SOURCE=.\DruckAuswahl.cpp
# End Source File
# Begin Source File

SOURCE=.\EmpDialog.cpp
# End Source File
# Begin Source File

SOURCE=.\EmpSearchPage.cpp
# End Source File
# Begin Source File

SOURCE=.\EquipmentChart.cpp
# End Source File
# Begin Source File

SOURCE=.\EquipmentDetailAttributesViewer.cpp
# End Source File
# Begin Source File

SOURCE=.\EquipmentDetailDlg.CPP
# End Source File
# Begin Source File

SOURCE=.\EquipmentDetailGantt.CPP
# End Source File
# Begin Source File

SOURCE=.\EquipmentDetailViewer.CPP
# End Source File
# Begin Source File

SOURCE=.\EquipmentDetailWindow.cpp
# End Source File
# Begin Source File

SOURCE=.\EquipmentDiagram.cpp
# End Source File
# Begin Source File

SOURCE=.\EquipmentDiagramGroupPage.CPP
# End Source File
# Begin Source File

SOURCE=.\EquipmentDiagramPropSheet.CPP
# End Source File
# Begin Source File

SOURCE=.\EquipmentGantt.cpp
# End Source File
# Begin Source File

SOURCE=.\EquipmentViewer.cpp
# End Source File
# Begin Source File

SOURCE=..\..\_Standard_Share\_Standard_Classlib\excel9.cpp
# End Source File
# Begin Source File

SOURCE=.\ExtendShiftDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\FieldConfigDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\FilterPage.cpp
# End Source File
# Begin Source File

SOURCE=.\FlightDetailDlg.CPP
# End Source File
# Begin Source File

SOURCE=.\FlightDetailGantt.CPP
# End Source File
# Begin Source File

SOURCE=.\FlightDetailViewer.CPP
# End Source File
# Begin Source File

SOURCE=.\FlightDetailWindow.cpp
# End Source File
# Begin Source File

SOURCE=.\FlightJobsTable.cpp
# End Source File
# Begin Source File

SOURCE=.\FlightJobsTablePropertySheet.cpp
# End Source File
# Begin Source File

SOURCE=.\FlightJobsTableSortPage.cpp
# End Source File
# Begin Source File

SOURCE=.\FlightJobsTableViewer.cpp
# End Source File
# Begin Source File

SOURCE=.\FlightMgrDetailViewer.cpp
# End Source File
# Begin Source File

SOURCE=.\FlightMgrDetailWindow.CPP
# End Source File
# Begin Source File

SOURCE=.\FlightPlan.cpp
# End Source File
# Begin Source File

SOURCE=.\FlightScheduleViewer.cpp
# End Source File
# Begin Source File

SOURCE=.\FlightSearchPage.cpp
# End Source File
# Begin Source File

SOURCE=.\FlightTableBoundFilterPage.cpp
# End Source File
# Begin Source File

SOURCE=.\FlightTablePropertySheet.cpp
# End Source File
# Begin Source File

SOURCE=.\FlightTableSortPage.cpp
# End Source File
# Begin Source File

SOURCE=.\FlIndDemandTable.cpp
# End Source File
# Begin Source File

SOURCE=.\FlIndDemandTablePropertySheet.cpp
# End Source File
# Begin Source File

SOURCE=.\FlIndDemandTableSortPage.cpp
# End Source File
# Begin Source File

SOURCE=.\FlIndDemandTableViewer.cpp
# End Source File
# Begin Source File

SOURCE=.\FreeEmps.cpp
# End Source File
# Begin Source File

SOURCE=.\FreeEmpViewer.cpp
# End Source File
# Begin Source File

SOURCE=.\FunctionCodeSelectionDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\FunctionColoursDetailDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\FunctionColoursDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\FunctionColoursViewer.cpp
# End Source File
# Begin Source File

SOURCE=.\GateChart.cpp
# End Source File
# Begin Source File

SOURCE=.\GateDetailViewer.CPP
# End Source File
# Begin Source File

SOURCE=.\GateDetailWindow.CPP
# End Source File
# Begin Source File

SOURCE=.\GateDiagram.cpp
# End Source File
# Begin Source File

SOURCE=.\GateDiagramArrDepPage.CPP
# End Source File
# Begin Source File

SOURCE=.\GateDiagramPropSheet.CPP
# End Source File
# Begin Source File

SOURCE=.\GateGantt.cpp
# End Source File
# Begin Source File

SOURCE=.\GateTable.cpp
# End Source File
# Begin Source File

SOURCE=.\GateTablePropertySheet.cpp
# End Source File
# Begin Source File

SOURCE=.\GateTableSortPage.cpp
# End Source File
# Begin Source File

SOURCE=.\GateTableViewer.cpp
# End Source File
# Begin Source File

SOURCE=.\GateViewer.cpp
# End Source File
# Begin Source File

SOURCE=.\GBAR.CPP
# End Source File
# Begin Source File

SOURCE=.\GUILng.cpp
# End Source File
# Begin Source File

SOURCE=.\JobTimeHandler.cpp
# End Source File
# Begin Source File

SOURCE=.\JobWithoutDemDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\LastJobInfo.cpp
# End Source File
# Begin Source File

SOURCE=.\LoadTimeSpanDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\LoginDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\MainFrm.cpp
# End Source File
# Begin Source File

SOURCE=.\MReqTable.cpp
# End Source File
# Begin Source File

SOURCE=.\MREQTAPS.CPP
# End Source File
# Begin Source File

SOURCE=.\MREQTASO.CPP
# End Source File
# Begin Source File

SOURCE=.\mreqtblv.cpp
# End Source File
# Begin Source File

SOURCE=.\NameConfig.cpp
# End Source File
# Begin Source File

SOURCE=.\NewAssignConflictDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\NewAssignConflictGantt.CPP
# End Source File
# Begin Source File

SOURCE=.\NewAssignConflictViewer.CPP
# End Source File
# Begin Source File

SOURCE=.\NewParaSet.cpp
# End Source File
# Begin Source File

SOURCE=.\NewWif.cpp
# End Source File
# Begin Source File

SOURCE=".\OpssPm.cpp"
# End Source File
# Begin Source File

SOURCE=".\OpssPm.rc"
# End Source File
# Begin Source File

SOURCE=.\OtherColoursDetailDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\OtherColoursDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\OtherColoursViewer.cpp
# End Source File
# Begin Source File

SOURCE=.\OtherSetupDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\peaktable.cpp
# End Source File
# Begin Source File

SOURCE=.\PoolJobDemandAssignments.cpp
# End Source File
# Begin Source File

SOURCE=.\PrePlanTable.cpp
# End Source File
# Begin Source File

SOURCE=.\PrePlanTablePropertySheet.cpp
# End Source File
# Begin Source File

SOURCE=.\PrePlanTableSortPage.cpp
# End Source File
# Begin Source File

SOURCE=.\PrePlanTableTeamViewer.cpp
# End Source File
# Begin Source File

SOURCE=.\PrePlanTableViewer.cpp
# End Source File
# Begin Source File

SOURCE=.\PrePlanTeamPrintDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\PrePlanTeamTable.cpp
# End Source File
# Begin Source File

SOURCE=.\PrintCard.cpp
# End Source File
# Begin Source File

SOURCE=.\PrintControl.cpp
# End Source File
# Begin Source File

SOURCE=.\PrintFlightSelection.cpp
# End Source File
# Begin Source File

SOURCE=.\PrintOrExportToExcel.cpp
# End Source File
# Begin Source File

SOURCE=.\PrintTerminalSelection.cpp
# End Source File
# Begin Source File

SOURCE=.\PrivList.cpp
# End Source File
# Begin Source File

SOURCE=.\PrmAssignPdaDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\PrmConfig.cpp
# End Source File
# Begin Source File

SOURCE=.\PrmDiagramFilterPage.CPP
# End Source File
# Begin Source File

SOURCE=.\PrmDiagramGroupPage.CPP
# End Source File
# Begin Source File

SOURCE=.\PrmDialog.cpp
# End Source File
# Begin Source File

SOURCE=.\PRMFlightSearch.cpp
# End Source File
# Begin Source File

SOURCE=.\PrmJobWithoutDemDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\PRMTABLE.CPP
# End Source File
# Begin Source File

SOURCE=.\PrmTablePropertySheet.cpp
# End Source File
# Begin Source File

SOURCE=.\PrmTableSortPage.cpp
# End Source File
# Begin Source File

SOURCE=.\PrmTableViewer.cpp
# End Source File
# Begin Source File

SOURCE=.\PstChart.cpp
# End Source File
# Begin Source File

SOURCE=.\PstDetailViewer.cpp
# End Source File
# Begin Source File

SOURCE=.\PstDetailWindow.cpp
# End Source File
# Begin Source File

SOURCE=.\PstDiagram.cpp
# End Source File
# Begin Source File

SOURCE=.\PstDiagramArrDepPage.CPP
# End Source File
# Begin Source File

SOURCE=.\PstDiagramPrintSelection.cpp
# End Source File
# Begin Source File

SOURCE=.\PstDiagramPropSheet.CPP
# End Source File
# Begin Source File

SOURCE=.\PstDiagramSetupPage.cpp
# End Source File
# Begin Source File

SOURCE=.\PstGantt.cpp
# End Source File
# Begin Source File

SOURCE=.\PstViewer.cpp
# End Source File
# Begin Source File

SOURCE=.\ReassignBreak.cpp
# End Source File
# Begin Source File

SOURCE=.\RegisterDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\RegnChart.cpp
# End Source File
# Begin Source File

SOURCE=.\RegnDetailViewer.CPP
# End Source File
# Begin Source File

SOURCE=.\RegnDetailWindow.CPP
# End Source File
# Begin Source File

SOURCE=.\RegnDiagram.cpp
# End Source File
# Begin Source File

SOURCE=.\RegnDiagramPropSheet.CPP
# End Source File
# Begin Source File

SOURCE=.\RegnGantt.cpp
# End Source File
# Begin Source File

SOURCE=.\RegnViewer.cpp
# End Source File
# Begin Source File

SOURCE=.\ReleaseDialog.cpp
# End Source File
# Begin Source File

SOURCE=.\search.cpp
# End Source File
# Begin Source File

SOURCE=.\SearchDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\SearchPropertySheet.cpp
# End Source File
# Begin Source File

SOURCE=.\SearchResults.cpp
# End Source File
# Begin Source File

SOURCE=.\SelectPoolStaffDlg.CPP
# End Source File
# Begin Source File

SOURCE=.\SelectPoolStaffForJobFlightDlg.CPP
# End Source File
# Begin Source File

SOURCE=.\SelectShiftDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\SelPoolDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\SelReportDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\SetDisplayTimeframeDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\Settings.cpp
# End Source File
# Begin Source File

SOURCE=.\SetupDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\ShiftCodeSelectionDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\SplitJobDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\StaffBreakViewer.cpp
# End Source File
# Begin Source File

SOURCE=.\StaffChart.cpp
# End Source File
# Begin Source File

SOURCE=.\StaffDetailDlg.CPP
# End Source File
# Begin Source File

SOURCE=.\StaffDetailGantt.CPP
# End Source File
# Begin Source File

SOURCE=.\StaffDetailViewer.CPP
# End Source File
# Begin Source File

SOURCE=.\StaffDetailWindow.cpp
# End Source File
# Begin Source File

SOURCE=.\StaffDiagram.cpp
# End Source File
# Begin Source File

SOURCE=.\StaffDiagramArrDepPage.cpp
# End Source File
# Begin Source File

SOURCE=.\StaffDiagramGroupPage.CPP
# End Source File
# Begin Source File

SOURCE=.\StaffDiagramPropSheet.CPP
# End Source File
# Begin Source File

SOURCE=.\StaffDiagramSortPage.CPP
# End Source File
# Begin Source File

SOURCE=.\StaffFilterDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\StaffGantt.cpp
# End Source File
# Begin Source File

SOURCE=.\StaffSortDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\StaffTable.cpp
# End Source File
# Begin Source File

SOURCE=.\StaffTableFilterPage.cpp
# End Source File
# Begin Source File

SOURCE=.\StaffTablePropertySheet.cpp
# End Source File
# Begin Source File

SOURCE=.\StaffTableSortPage.cpp
# End Source File
# Begin Source File

SOURCE=.\StaffTableViewer.cpp
# End Source File
# Begin Source File

SOURCE=.\StaffViewer.cpp
# End Source File
# Begin Source File

SOURCE=.\StartDate.cpp
# End Source File
# Begin Source File

SOURCE=.\StdAfx.cpp
# ADD CPP /Yc"stdafx.h"
# End Source File
# Begin Source File

SOURCE=.\SUViewsDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\SwapShiftDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\Table.cpp
# End Source File
# Begin Source File

SOURCE=.\TeamDelegationDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\TelexTypeDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\thirdpartyfilterdlg.cpp
# End Source File
# Begin Source File

SOURCE=.\TimeFrameList.cpp
# End Source File
# Begin Source File

SOURCE=.\TimeScaleDlg.CPP
# End Source File
# Begin Source File

SOURCE=.\TrackSheet.cpp
# End Source File
# Begin Source File

SOURCE=.\TransactionManager.cpp
# End Source File
# Begin Source File

SOURCE=.\TSCALE.CPP
# End Source File
# Begin Source File

SOURCE=.\Ufis.cpp
# End Source File
# Begin Source File

SOURCE=.\UndoClasses.cpp
# End Source File
# Begin Source File

SOURCE=.\UndoManager.cpp
# End Source File
# Begin Source File

SOURCE=.\VersionInfo.cpp
# End Source File
# Begin Source File

SOURCE=.\WaitAssignDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\WaitDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\WorkGroupBackgroundBars.cpp
# End Source File
# Begin Source File

SOURCE=.\ZeitPage.cpp
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl;fi;fd"
# Begin Source File

SOURCE=.\3DSTATIC.H
# End Source File
# Begin Source File

SOURCE=..\..\..\..\_Standard\_Standard_Client\_Standard_Share\_Standard_Wrapper\aatlogin.h
# End Source File
# Begin Source File

SOURCE=.\AbsenceDlg.h
# End Source File
# Begin Source File

SOURCE=.\AllocData.h
# End Source File
# Begin Source File

SOURCE=.\AllocDlg.h
# End Source File
# Begin Source File

SOURCE=.\AssignConflictDlg.h
# End Source File
# Begin Source File

SOURCE=.\AssignDlg.H
# End Source File
# Begin Source File

SOURCE=.\AssignFromPrePlanTableDlg.H
# End Source File
# Begin Source File

SOURCE=.\AssignJobDetachDlg.H
# End Source File
# Begin Source File

SOURCE=.\AutoAssignAz.h
# End Source File
# Begin Source File

SOURCE=.\AutoAssignAzViewer.h
# End Source File
# Begin Source File

SOURCE=.\AutoAssignDlg.h
# End Source File
# Begin Source File

SOURCE=.\AvailableEmpsDlg.h
# End Source File
# Begin Source File

SOURCE=.\AvailableEmpsViewer.h
# End Source File
# Begin Source File

SOURCE=.\AvailableEquipmentDlg.h
# End Source File
# Begin Source File

SOURCE=.\AvailableEquipmentViewer.h
# End Source File
# Begin Source File

SOURCE=.\BasePropertySheet.h
# End Source File
# Begin Source File

SOURCE=.\BasicData.h
# End Source File
# Begin Source File

SOURCE=.\BCDialog2.h
# End Source File
# Begin Source File

SOURCE=.\BchandleX.h
# End Source File
# Begin Source File

SOURCE=.\BreakTable.h
# End Source File
# Begin Source File

SOURCE=.\BreakTableViewer.h
# End Source File
# Begin Source File

SOURCE=.\BUTTONCT.H
# End Source File
# Begin Source File

SOURCE=.\ButtonList.h
# End Source File
# Begin Source File

SOURCE=.\CciChart.h
# End Source File
# Begin Source File

SOURCE=.\CciDemandDetailDlg.h
# End Source File
# Begin Source File

SOURCE=.\CciDeskDlg.h
# End Source File
# Begin Source File

SOURCE=.\CciDetailDlg.H
# End Source File
# Begin Source File

SOURCE=.\CciDetailGantt.H
# End Source File
# Begin Source File

SOURCE=.\CciDetailViewer.H
# End Source File
# Begin Source File

SOURCE=.\CciDetailWindow.H
# End Source File
# Begin Source File

SOURCE=.\CciDiagram.h
# End Source File
# Begin Source File

SOURCE=.\CciDiagramFilterPage.H
# End Source File
# Begin Source File

SOURCE=.\CciDiagramGroupPage.H
# End Source File
# Begin Source File

SOURCE=.\CciDiagramPropertySheet.H
# End Source File
# Begin Source File

SOURCE=.\CCIGANTT.H
# End Source File
# Begin Source File

SOURCE=.\CCITABLE.H
# End Source File
# Begin Source File

SOURCE=.\CciTablePropertySheet.h
# End Source File
# Begin Source File

SOURCE=.\CciTableSortPage.h
# End Source File
# Begin Source File

SOURCE=.\CciTableViewer.h
# End Source File
# Begin Source File

SOURCE=.\cciviewer.h
# End Source File
# Begin Source File

SOURCE=.\CCSDEFS.H
# End Source File
# Begin Source File

SOURCE=.\CCSERR.H
# End Source File
# Begin Source File

SOURCE=.\CCSGLOBL.H
# End Source File
# Begin Source File

SOURCE=.\CCSOBJ.H
# End Source File
# Begin Source File

SOURCE=.\CCSPrint.h
# End Source File
# Begin Source File

SOURCE=.\CCSSTR.H
# End Source File
# Begin Source File

SOURCE=.\CCSTree.h
# End Source File
# Begin Source File

SOURCE=.\CedaAcrData.h
# End Source File
# Begin Source File

SOURCE=.\CedaActData.h
# End Source File
# Begin Source File

SOURCE=.\CedaAloData.h
# End Source File
# Begin Source File

SOURCE=.\CedaAltData.h
# End Source File
# Begin Source File

SOURCE=.\CedaAptData.h
# End Source File
# Begin Source File

SOURCE=.\CedaAzaData.h
# End Source File
# Begin Source File

SOURCE=.\CedaBlkData.h
# End Source File
# Begin Source File

SOURCE=.\CedaBltData.h
# End Source File
# Begin Source File

SOURCE=.\CedaBsdData.h
# End Source File
# Begin Source File

SOURCE=.\CedaCcaData.h
# End Source File
# Begin Source File

SOURCE=.\CedaCCCData.h
# End Source File
# Begin Source File

SOURCE=.\CedaCfgData.h
# End Source File
# Begin Source File

SOURCE=.\CedaCicData.h
# End Source File
# Begin Source File

SOURCE=.\CedaCnfData.h
# End Source File
# Begin Source File

SOURCE=.\CedaDelData.h
# End Source File
# Begin Source File

SOURCE=.\CedaDemandData.h
# End Source File
# Begin Source File

SOURCE=.\CedaDlgData.h
# End Source File
# Begin Source File

SOURCE=.\CedaDprData.h
# End Source File
# Begin Source File

SOURCE=.\CedaDpxData.h
# End Source File
# Begin Source File

SOURCE=.\CedaDraData.h
# End Source File
# Begin Source File

SOURCE=.\CedaDrdData.h
# End Source File
# Begin Source File

SOURCE=.\CedaDrgData.h
# End Source File
# Begin Source File

SOURCE=.\CedaDrsData.h
# End Source File
# Begin Source File

SOURCE=.\CedaEmpData.h
# End Source File
# Begin Source File

SOURCE=.\CedaEqaData.h
# End Source File
# Begin Source File

SOURCE=.\CedaEqtData.h
# End Source File
# Begin Source File

SOURCE=.\CedaEquData.h
# End Source File
# Begin Source File

SOURCE=.\CedaFlightData.h
# End Source File
# Begin Source File

SOURCE=.\CedaGatData.h
# End Source File
# Begin Source File

SOURCE=.\CedaHagData.h
# End Source File
# Begin Source File

SOURCE=.\CedaInitModuData.h
# End Source File
# Begin Source File

SOURCE=.\CedaJobData.h
# End Source File
# Begin Source File

SOURCE=.\CedaJodData.h
# End Source File
# Begin Source File

SOURCE=.\CedaJtyData.h
# End Source File
# Begin Source File

SOURCE=.\CedaLoaData.h
# End Source File
# Begin Source File

SOURCE=.\CedaNatData.h
# End Source File
# Begin Source File

SOURCE=.\CedaOdaData.h
# End Source File
# Begin Source File

SOURCE=.\CedaPaxData.h
# End Source File
# Begin Source File

SOURCE=.\CedaPdaData.h
# End Source File
# Begin Source File

SOURCE=.\CedaPeakData.h
# End Source File
# Begin Source File

SOURCE=.\CedaPerData.h
# End Source File
# Begin Source File

SOURCE=.\CedaPfcData.h
# End Source File
# Begin Source File

SOURCE=.\CedaPgpData.h
# End Source File
# Begin Source File

SOURCE=.\CedaPolData.h
# End Source File
# Begin Source File

SOURCE=.\CedaPrmData.h
# End Source File
# Begin Source File

SOURCE=.\CedaPstData.h
# End Source File
# Begin Source File

SOURCE=.\CedaRemData.h
# End Source File
# Begin Source File

SOURCE=.\CedaReqData.h
# End Source File
# Begin Source File

SOURCE=.\CedaRequestData.h
# End Source File
# Begin Source File

SOURCE=.\CedaRloData.h
# End Source File
# Begin Source File

SOURCE=.\CedaRnkData.h
# End Source File
# Begin Source File

SOURCE=.\CedaRpfData.h
# End Source File
# Begin Source File

SOURCE=.\CedaRpqData.h
# End Source File
# Begin Source File

SOURCE=.\CedaRudData.h
# End Source File
# Begin Source File

SOURCE=.\CedaRueData.h
# End Source File
# Begin Source File

SOURCE=.\CedaSerData.h
# End Source File
# Begin Source File

SOURCE=.\CedaSgmData.h
# End Source File
# Begin Source File

SOURCE=.\CedaSgrData.h
# End Source File
# Begin Source File

SOURCE=.\CedaShiftData.h
# End Source File
# Begin Source File

SOURCE=.\CedaShiftTypeData.h
# End Source File
# Begin Source File

SOURCE=.\CedaSpeData.h
# End Source File
# Begin Source File

SOURCE=.\CedaSpfData.h
# End Source File
# Begin Source File

SOURCE=.\CedaSprData.h
# End Source File
# Begin Source File

SOURCE=.\CedaSwgData.h
# End Source File
# Begin Source File

SOURCE=.\CedaTlxData.H
# End Source File
# Begin Source File

SOURCE=.\CedaTplData.h
# End Source File
# Begin Source File

SOURCE=.\CedaValData.h
# End Source File
# Begin Source File

SOURCE=.\CedaWgpData.h
# End Source File
# Begin Source File

SOURCE=.\CedaWroData.h
# End Source File
# Begin Source File

SOURCE=.\cformat.h
# End Source File
# Begin Source File

SOURCE=.\CheckDataConsistencyDlg.h
# End Source File
# Begin Source File

SOURCE=.\cicAutoAssignDlg.h
# End Source File
# Begin Source File

SOURCE=.\CiCDemandTable.h
# End Source File
# Begin Source File

SOURCE=.\CiCDemandTablePropertySheet.h
# End Source File
# Begin Source File

SOURCE=.\CiCDemandTableSortPage.h
# End Source File
# Begin Source File

SOURCE=.\CiCDemandTableViewer.h
# End Source File
# Begin Source File

SOURCE=.\CicPrintDlg.h
# End Source File
# Begin Source File

SOURCE=.\CLIENTWN.H
# End Source File
# Begin Source File

SOURCE=.\COLOR.H
# End Source File
# Begin Source File

SOURCE=.\ConfigTextDlg.h
# End Source File
# Begin Source File

SOURCE=.\CONFLICT.H
# End Source File
# Begin Source File

SOURCE=.\ConflictConfigTable.h
# End Source File
# Begin Source File

SOURCE=.\ConflictTable.h
# End Source File
# Begin Source File

SOURCE=.\ConflictTablePropSheet.H
# End Source File
# Begin Source File

SOURCE=.\ConflictTableSortPage.H
# End Source File
# Begin Source File

SOURCE=.\ConflictViewer.h
# End Source File
# Begin Source File

SOURCE=.\CopyDemandDlg.h
# End Source File
# Begin Source File

SOURCE=.\CPrintPageDialog.h
# End Source File
# Begin Source File

SOURCE=.\CPrintPreviewView.h
# End Source File
# Begin Source File

SOURCE=.\CreateDemand.h
# End Source File
# Begin Source File

SOURCE=.\CVIEWER.H
# End Source File
# Begin Source File

SOURCE=.\CXBUTTON.H
# End Source File
# Begin Source File

SOURCE=.\DATASET.H
# End Source File
# Begin Source File

SOURCE=.\DBREAK.H
# End Source File
# Begin Source File

SOURCE=.\DChange.h
# End Source File
# Begin Source File

SOURCE=.\DebugCountDlg.h
# End Source File
# Begin Source File

SOURCE=.\DebugInfoLogDlg.h
# End Source File
# Begin Source File

SOURCE=.\DebugInfoMsgBox.h
# End Source File
# Begin Source File

SOURCE=.\DemandDetailDlg.h
# End Source File
# Begin Source File

SOURCE=.\DemandFilterPage.h
# End Source File
# Begin Source File

SOURCE=.\DemandTable.h
# End Source File
# Begin Source File

SOURCE=.\DemandTablePropertySheet.h
# End Source File
# Begin Source File

SOURCE=.\DemandTableSortPage.h
# End Source File
# Begin Source File

SOURCE=.\DemandTableViewer.h
# End Source File
# Begin Source File

SOURCE=.\DemandViewer.h
# End Source File
# Begin Source File

SOURCE=.\DGateJob.h
# End Source File
# Begin Source File

SOURCE=.\DINFO.H
# End Source File
# Begin Source File

SOURCE=.\DJobChange.h
# End Source File
# Begin Source File

SOURCE=.\DlgSettings.h
# End Source File
# Begin Source File

SOURCE=.\DMreqTable.h
# End Source File
# Begin Source File

SOURCE=.\DruckAuswahl.h
# End Source File
# Begin Source File

SOURCE=.\empdialog.h
# End Source File
# Begin Source File

SOURCE=.\EmpSearchPage.h
# End Source File
# Begin Source File

SOURCE=.\EquipmentChart.h
# End Source File
# Begin Source File

SOURCE=.\EquipmentDetailAttributesViewer.h
# End Source File
# Begin Source File

SOURCE=.\EquipmentDetailDlg.H
# End Source File
# Begin Source File

SOURCE=.\EquipmentDetailGantt.H
# End Source File
# Begin Source File

SOURCE=.\EquipmentDetailViewer.H
# End Source File
# Begin Source File

SOURCE=.\EquipmentDetailWindow.h
# End Source File
# Begin Source File

SOURCE=.\EquipmentDiagram.h
# End Source File
# Begin Source File

SOURCE=.\EquipmentDiagramGroupPage.H
# End Source File
# Begin Source File

SOURCE=.\EquipmentDiagramPropSheet.H
# End Source File
# Begin Source File

SOURCE=.\EquipmentGantt.h
# End Source File
# Begin Source File

SOURCE=.\EquipmentViewer.h
# End Source File
# Begin Source File

SOURCE=..\..\_Standard_Share\_Standard_Classlib\excel9.h
# End Source File
# Begin Source File

SOURCE=.\ExtendShiftDlg.h
# End Source File
# Begin Source File

SOURCE=.\FieldConfigDlg.h
# End Source File
# Begin Source File

SOURCE=.\FilterData.h
# End Source File
# Begin Source File

SOURCE=.\FilterPage.h
# End Source File
# Begin Source File

SOURCE=.\FlightDetailDlg.H
# End Source File
# Begin Source File

SOURCE=.\FlightDetailGantt.H
# End Source File
# Begin Source File

SOURCE=.\FlightDetailViewer.H
# End Source File
# Begin Source File

SOURCE=.\FlightDetailWindow.h
# End Source File
# Begin Source File

SOURCE=.\FlightJobsTable.h
# End Source File
# Begin Source File

SOURCE=.\FlightJobsTablePropertySheet.h
# End Source File
# Begin Source File

SOURCE=.\FlightJobsTableSortPage.h
# End Source File
# Begin Source File

SOURCE=.\FlightJobsTableViewer.h
# End Source File
# Begin Source File

SOURCE=.\FlightMgrDetailViewer.h
# End Source File
# Begin Source File

SOURCE=.\FlightMgrDetailWindow.H
# End Source File
# Begin Source File

SOURCE=.\FlightPlan.h
# End Source File
# Begin Source File

SOURCE=.\FlightScheduleViewer.h
# End Source File
# Begin Source File

SOURCE=.\flightsearchpage.h
# End Source File
# Begin Source File

SOURCE=.\FlightTableBoundFilterPage.h
# End Source File
# Begin Source File

SOURCE=.\FlightTablePropertySheet.h
# End Source File
# Begin Source File

SOURCE=.\FlightTableSortPage.h
# End Source File
# Begin Source File

SOURCE=.\FlIndDemandTable.h
# End Source File
# Begin Source File

SOURCE=.\FlIndDemandTablePropertySheet.h
# End Source File
# Begin Source File

SOURCE=.\FlIndDemandTableSortPage.h
# End Source File
# Begin Source File

SOURCE=.\FlIndDemandTableViewer.h
# End Source File
# Begin Source File

SOURCE=.\FreeEmps.h
# End Source File
# Begin Source File

SOURCE=.\FreeEmpViewer.h
# End Source File
# Begin Source File

SOURCE=.\FunctionCodeSelectionDlg.h
# End Source File
# Begin Source File

SOURCE=.\FunctionColoursDetailDlg.h
# End Source File
# Begin Source File

SOURCE=.\FunctionColoursDlg.h
# End Source File
# Begin Source File

SOURCE=.\FunctionColoursViewer.h
# End Source File
# Begin Source File

SOURCE=.\GANTT.H
# End Source File
# Begin Source File

SOURCE=.\GateChart.h
# End Source File
# Begin Source File

SOURCE=.\GateDetailViewer.H
# End Source File
# Begin Source File

SOURCE=.\GateDetailWindow.H
# End Source File
# Begin Source File

SOURCE=.\GateDiagram.h
# End Source File
# Begin Source File

SOURCE=.\GateDiagramArrDepPage.h
# End Source File
# Begin Source File

SOURCE=.\GateDiagramPropSheet.H
# End Source File
# Begin Source File

SOURCE=.\GateGantt.h
# End Source File
# Begin Source File

SOURCE=.\gatetable.h
# End Source File
# Begin Source File

SOURCE=.\GateTablePropertySheet.h
# End Source File
# Begin Source File

SOURCE=.\GateTableSortPage.h
# End Source File
# Begin Source File

SOURCE=.\GateTableViewer.h
# End Source File
# Begin Source File

SOURCE=.\GateViewer.h
# End Source File
# Begin Source File

SOURCE=.\GBAR.H
# End Source File
# Begin Source File

SOURCE=.\GUILng.h
# End Source File
# Begin Source File

SOURCE=.\JobTimeHandler.h
# End Source File
# Begin Source File

SOURCE=.\JobWithoutDemDlg.h
# End Source File
# Begin Source File

SOURCE=.\LastJobInfo.h
# End Source File
# Begin Source File

SOURCE=.\LoadTimeSpanDlg.h
# End Source File
# Begin Source File

SOURCE=.\LoginDlg.h
# End Source File
# Begin Source File

SOURCE=.\MainFrm.h
# End Source File
# Begin Source File

SOURCE=.\mreqtable.h
# End Source File
# Begin Source File

SOURCE=.\MREQTAPS.H
# End Source File
# Begin Source File

SOURCE=.\MREQTASO.H
# End Source File
# Begin Source File

SOURCE=.\mreqtblv.h
# End Source File
# Begin Source File

SOURCE=.\NameConfig.h
# End Source File
# Begin Source File

SOURCE=.\NewAssignConflictDlg.h
# End Source File
# Begin Source File

SOURCE=.\NewAssignConflictGantt.H
# End Source File
# Begin Source File

SOURCE=.\NewAssignConflictViewer.H
# End Source File
# Begin Source File

SOURCE=.\NewParaSet.h
# End Source File
# Begin Source File

SOURCE=.\NewWif.h
# End Source File
# Begin Source File

SOURCE=".\OpssPm.h"
# End Source File
# Begin Source File

SOURCE=.\OtherColoursDetailDlg.h
# End Source File
# Begin Source File

SOURCE=.\OtherColoursDlg.h
# End Source File
# Begin Source File

SOURCE=.\OtherColoursViewer.h
# End Source File
# Begin Source File

SOURCE=.\OtherSetupDlg.h
# End Source File
# Begin Source File

SOURCE=.\peaktable.h
# End Source File
# Begin Source File

SOURCE=.\PoolJobDemandAssignments.h
# End Source File
# Begin Source File

SOURCE=.\PrePlanTable.h
# End Source File
# Begin Source File

SOURCE=.\PrePlanTablePropertySheet.h
# End Source File
# Begin Source File

SOURCE=.\PrePlanTableSortPage.h
# End Source File
# Begin Source File

SOURCE=.\PrePlanTableTeamViewer.h
# End Source File
# Begin Source File

SOURCE=.\PrePlanTableViewer.h
# End Source File
# Begin Source File

SOURCE=.\PrePlanTeamPrintDlg.h
# End Source File
# Begin Source File

SOURCE=.\PrePlanTeamTable.h
# End Source File
# Begin Source File

SOURCE=.\PrintCard.h
# End Source File
# Begin Source File

SOURCE=.\PrintControl.h
# End Source File
# Begin Source File

SOURCE=.\PrintFlightSelection.h
# End Source File
# Begin Source File

SOURCE=.\PrintOrExportToExcel.h
# End Source File
# Begin Source File

SOURCE=.\PrintTerminalSelection.h
# End Source File
# Begin Source File

SOURCE=.\PrivList.h
# End Source File
# Begin Source File

SOURCE=.\PrmAssignPdaDlg.h
# End Source File
# Begin Source File

SOURCE=.\PrmConfig.h
# End Source File
# Begin Source File

SOURCE=.\PrmDiagramFilterPage.H
# End Source File
# Begin Source File

SOURCE=.\PrmDiagramGroupPage.H
# End Source File
# Begin Source File

SOURCE=.\PrmDialog.h
# End Source File
# Begin Source File

SOURCE=.\PRMFlightSearch.h
# End Source File
# Begin Source File

SOURCE=.\PrmJobWithoutDemDlg.h
# End Source File
# Begin Source File

SOURCE=.\PRMTABLE.H
# End Source File
# Begin Source File

SOURCE=.\PrmTablePropertySheet.h
# End Source File
# Begin Source File

SOURCE=.\PrmTableSortPage.h
# End Source File
# Begin Source File

SOURCE=.\PrmTableViewer.h
# End Source File
# Begin Source File

SOURCE=.\PstChart.h
# End Source File
# Begin Source File

SOURCE=.\PstDetailViewer.h
# End Source File
# Begin Source File

SOURCE=.\PstDetailWindow.h
# End Source File
# Begin Source File

SOURCE=.\PstDiagram.h
# End Source File
# Begin Source File

SOURCE=.\PstDiagramArrDepPage.h
# End Source File
# Begin Source File

SOURCE=.\PstDiagramPrintSelection.h
# End Source File
# Begin Source File

SOURCE=.\PstDiagramPropSheet.H
# End Source File
# Begin Source File

SOURCE=.\PstDiagramSetupPage.h
# End Source File
# Begin Source File

SOURCE=.\PstGantt.h
# End Source File
# Begin Source File

SOURCE=.\PstViewer.h
# End Source File
# Begin Source File

SOURCE=.\ReassignBreak.h
# End Source File
# Begin Source File

SOURCE=.\RegisterDlg.h
# End Source File
# Begin Source File

SOURCE=.\RegnChart.h
# End Source File
# Begin Source File

SOURCE=.\RegnDetailViewer.H
# End Source File
# Begin Source File

SOURCE=.\RegnDetailWindow.H
# End Source File
# Begin Source File

SOURCE=.\RegnDiagram.h
# End Source File
# Begin Source File

SOURCE=.\RegnDiagramPropSheet.H
# End Source File
# Begin Source File

SOURCE=.\RegnGantt.h
# End Source File
# Begin Source File

SOURCE=.\RegnViewer.h
# End Source File
# Begin Source File

SOURCE=.\ReleaseDialog.h
# End Source File
# Begin Source File

SOURCE=.\resource.h
# End Source File
# Begin Source File

SOURCE=.\Search.h
# End Source File
# Begin Source File

SOURCE=.\SearchDlg.h
# End Source File
# Begin Source File

SOURCE=.\SearchPropertySheet.h
# End Source File
# Begin Source File

SOURCE=.\SearchResults.h
# End Source File
# Begin Source File

SOURCE=.\SelectPoolStaffDlg.H
# End Source File
# Begin Source File

SOURCE=.\SelectPoolStaffForJobFlightDlg.H
# End Source File
# Begin Source File

SOURCE=.\SelectShiftDlg.h
# End Source File
# Begin Source File

SOURCE=.\SelPoolDlg.h
# End Source File
# Begin Source File

SOURCE=.\SelReportDlg.h
# End Source File
# Begin Source File

SOURCE=.\SetDisplayTimeframeDlg.h
# End Source File
# Begin Source File

SOURCE=.\Settings.h
# End Source File
# Begin Source File

SOURCE=.\SetupDlg.h
# End Source File
# Begin Source File

SOURCE=.\ShiftCodeSelectionDlg.h
# End Source File
# Begin Source File

SOURCE=.\SplitJobDlg.h
# End Source File
# Begin Source File

SOURCE=.\StaffBreakViewer.h
# End Source File
# Begin Source File

SOURCE=.\StaffChart.h
# End Source File
# Begin Source File

SOURCE=.\StaffDetailDlg.H
# End Source File
# Begin Source File

SOURCE=.\StaffDetailGantt.H
# End Source File
# Begin Source File

SOURCE=.\StaffDetailViewer.H
# End Source File
# Begin Source File

SOURCE=.\StaffDetailWindow.h
# End Source File
# Begin Source File

SOURCE=.\StaffDiagram.h
# End Source File
# Begin Source File

SOURCE=.\StaffDiagramArrDepPage.h
# End Source File
# Begin Source File

SOURCE=.\StaffDiagramGroupPage.H
# End Source File
# Begin Source File

SOURCE=.\StaffDiagramPropSheet.H
# End Source File
# Begin Source File

SOURCE=.\StaffDiagramSortPage.H
# End Source File
# Begin Source File

SOURCE=.\StaffFilterDlg.h
# End Source File
# Begin Source File

SOURCE=.\StaffGantt.h
# End Source File
# Begin Source File

SOURCE=.\StaffSortDlg.h
# End Source File
# Begin Source File

SOURCE=.\StaffTable.h
# End Source File
# Begin Source File

SOURCE=.\StaffTableFilterPage.h
# End Source File
# Begin Source File

SOURCE=.\StaffTablePropertySheet.h
# End Source File
# Begin Source File

SOURCE=.\StaffTableSortPage.h
# End Source File
# Begin Source File

SOURCE=.\StaffTableViewer.h
# End Source File
# Begin Source File

SOURCE=.\StaffViewer.h
# End Source File
# Begin Source File

SOURCE=.\StartDate.h
# End Source File
# Begin Source File

SOURCE=.\STDAFX.H
# End Source File
# Begin Source File

SOURCE=.\SUViewsDlg.h
# End Source File
# Begin Source File

SOURCE=.\SwapShiftDlg.h
# End Source File
# Begin Source File

SOURCE=.\TABLE.H
# End Source File
# Begin Source File

SOURCE=.\TeamDelegationDlg.h
# End Source File
# Begin Source File

SOURCE=.\TelexTypeDlg.h
# End Source File
# Begin Source File

SOURCE=.\thirdpartyfilterdlg.h
# End Source File
# Begin Source File

SOURCE=.\TimeFrameList.h
# End Source File
# Begin Source File

SOURCE=.\TimePacket.h
# End Source File
# Begin Source File

SOURCE=.\TimeScaleDlg.H
# End Source File
# Begin Source File

SOURCE=.\TrackSheet.h
# End Source File
# Begin Source File

SOURCE=.\TransactionManager.h
# End Source File
# Begin Source File

SOURCE=.\TSCALE.H
# End Source File
# Begin Source File

SOURCE=.\UFIS.H
# End Source File
# Begin Source File

SOURCE=.\UndoClasses.h
# End Source File
# Begin Source File

SOURCE=.\UndoManager.h
# End Source File
# Begin Source File

SOURCE=.\VersionInfo.h
# End Source File
# Begin Source File

SOURCE=.\WaitAssignDlg.h
# End Source File
# Begin Source File

SOURCE=.\WaitDlg.h
# End Source File
# Begin Source File

SOURCE=.\WorkGroupBackgroundBars.h
# End Source File
# Begin Source File

SOURCE=.\ZEITPAGE.H
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;cnt;rtf;gif;jpg;jpeg;jpe"
# Begin Source File

SOURCE=.\res\3dcheck.bmp
# End Source File
# Begin Source File

SOURCE=.\res\95check.bmp
# End Source File
# Begin Source File

SOURCE=.\RES\bitmap1.bmp
# End Source File
# Begin Source File

SOURCE=.\RES\bitmap10.bmp
# End Source File
# Begin Source File

SOURCE=.\RES\bitmap11.bmp
# End Source File
# Begin Source File

SOURCE=.\Res\bitmap17.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bitmap2.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bitmap4.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bitmap5.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bitmap6.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bitmap7.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bitmap8.bmp
# End Source File
# Begin Source File

SOURCE=.\RES\bitmap9.bmp
# End Source File
# Begin Source File

SOURCE=.\RES\bmp00001.bmp
# End Source File
# Begin Source File

SOURCE=.\RES\bmp00002.bmp
# End Source File
# Begin Source File

SOURCE=.\RES\bmp00003.bmp
# End Source File
# Begin Source File

SOURCE=.\RES\bmp00004.bmp
# End Source File
# Begin Source File

SOURCE=.\RES\bmp00005.bmp
# End Source File
# Begin Source File

SOURCE=.\res\break.bmp
# End Source File
# Begin Source File

SOURCE=.\Res\break1.bmp
# End Source File
# Begin Source File

SOURCE=.\RES\Bw_airp.bmp
# End Source File
# Begin Source File

SOURCE=.\RES\Bw_area.bmp
# End Source File
# Begin Source File

SOURCE=.\RES\Bw_gate.bmp
# End Source File
# Begin Source File

SOURCE=.\RES\Bw_pool.bmp
# End Source File
# Begin Source File

SOURCE=.\RES\Bw_term.bmp
# End Source File
# Begin Source File

SOURCE=.\RES\CCS_SW.BMP
# End Source File
# Begin Source File

SOURCE=.\RES\colaero.bmp
# End Source File
# Begin Source File

SOURCE=.\res\cursor1.cur
# End Source File
# Begin Source File

SOURCE=.\res\cursor2.cur
# End Source File
# Begin Source File

SOURCE=.\res\cursor3.cur
# End Source File
# Begin Source File

SOURCE=.\res\cursor4.cur
# End Source File
# Begin Source File

SOURCE=.\RES\exclamat.bmp
# End Source File
# Begin Source File

SOURCE=.\res\help.cur
# End Source File
# Begin Source File

SOURCE=.\Res\ico00001.ico
# End Source File
# Begin Source File

SOURCE=.\Res\ico00004.ico
# End Source File
# Begin Source File

SOURCE=.\res\icon1.ico
# End Source File
# Begin Source File

SOURCE=.\Res\icon2.ico
# End Source File
# Begin Source File

SOURCE=.\res\Login.bmp
# End Source File
# Begin Source File

SOURCE=.\RES\maeneken.bmp
# End Source File
# Begin Source File

SOURCE=.\RES\mannblau.bmp
# End Source File
# Begin Source File

SOURCE=.\res\minifwnd.bmp
# End Source File
# Begin Source File

SOURCE=.\res\move4way.cur
# End Source File
# Begin Source File

SOURCE=.\res\Nextd.bmp
# End Source File
# Begin Source File

SOURCE=.\res\Nextu.bmp
# End Source File
# Begin Source File

SOURCE=.\res\nextx.bmp
# End Source File
# Begin Source File

SOURCE=.\res\nodrop.cur
# End Source File
# Begin Source File

SOURCE=.\Res\notvalid.bmp
# End Source File
# Begin Source File

SOURCE=.\res\ntcheck.bmp
# End Source File
# Begin Source File

SOURCE=.\RES\OpssPm.bmp
# End Source File
# Begin Source File

SOURCE=.\res\Pepper.rc2
# End Source File
# Begin Source File

SOURCE=.\res\Prevd.bmp
# End Source File
# Begin Source File

SOURCE=.\res\Prevu.bmp
# End Source File
# Begin Source File

SOURCE=.\res\prevx.bmp
# End Source File
# Begin Source File

SOURCE=.\res\sarrows.cur
# End Source File
# Begin Source File

SOURCE=.\res\splith.cur
# End Source File
# Begin Source File

SOURCE=.\res\splitv.cur
# End Source File
# Begin Source File

SOURCE=.\RES\Terminal.bmp
# End Source File
# Begin Source File

SOURCE=.\res\Toolbar.bmp
# End Source File
# Begin Source File

SOURCE=.\res\trck4way.cur
# End Source File
# Begin Source File

SOURCE=.\res\trcknesw.cur
# End Source File
# Begin Source File

SOURCE=.\res\trckns.cur
# End Source File
# Begin Source File

SOURCE=.\res\trcknwse.cur
# End Source File
# Begin Source File

SOURCE=.\res\trckwe.cur
# End Source File
# Begin Source File

SOURCE=.\RES\UFIS.ICO
# End Source File
# Begin Source File

SOURCE=.\Res\ufis1.ico
# End Source File
# Begin Source File

SOURCE=.\Res\uparrow.bmp
# End Source File
# End Group
# Begin Source File

SOURCE=.\DataComp.avi
# End Source File
# End Target
# End Project
