// RegnGantt.cpp : implementation file
//

#include <stdafx.h>
#include <ccsglobl.h>
#include <OpssPm.h>
#include <CCSPtrArray.h>
#include <CLIENTWN.H>
#include <CCSCedaData.h>
#include <CedaEmpData.h>
#include <CedaShiftData.h>
#include <CedaJobData.h>
#include <CedaDemandData.h>
#include <CedaJodData.h>
#include <CedaFlightData.h>
#include <tscale.h>
#include <cviewer.h>
#include <CCSDragDropCtrl.h>
#include <gbar.h>
#include <cxbutton.h>
#include <PrePlanTable.h>
#include <CCITable.h>
//#include "Regntable.h"
#include <FlightPlan.h>
#include <StaffTable.h>
#include <StaffDiagram.h>
#include <RegnDiagram.h>
#include <CciDiagram.h>
#include <StartDate.h>
#include <ConflictConfigTable.h>
#include <conflict.h>
#include <dataset.h>
#include <ufis.h>
#include <CCSBcHandle.h>
#include <BasicData.h>
#include <ConflictTable.h>
#include <GateDiagram.h>
#include <GateTable.h>
#include <ButtonList.h>
#include <FlightDetailWindow.h>
#include <StaffDetailWindow.h>
#include <ccsddx.h>
#include <RegnViewer.h>
#include <RegnDetailWindow.h>
#include <CreateDemand.h>
//#include "dRegnjob.h"
#include <RegnGantt.h>
#include <CedaPolData.h>
#include <AvailableEmpsDlg.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

// General macros for testing a point against an interval
#define IsBetween(val, start, end)  ((start) <= (val) && (val) <= (end))
#define IsOverlapped(start1, end1, start2, end2)    ((start1) <= (end2) && (start2) <= (end1))
#define IsReallyOverlapped(start1, end1, start2, end2)	((start1) < (end2) && (start2) < (end1))
#define IsTotallyInside(start1, end1, start2, end2)	((start1) >= (start2) && (end1) <= (end2))

//static CString pclRanks(":TR:5F:4F:3F:2F:1F:MM:AFM:AM:FM:");

////////////////////////////////////////////////////////////////////////
// Damkerng 06/27/96:
// This block of code is used for substituing the scale calculation
// since the TimeScale has been implemented with a different pixel
// calculation algorithm (Pichet used the floating point calculation,
// which introduce +1 or -1 rounding effect. Furthermore, he still has
// some adjustments may be 2 or 3 pixels for makeing the TimeScale
// display nicely :-). Besides, there are still more few pixels which
// display inappropriately which introduced from the border of the
// RegnChart or the other classes of the same kind). To fix all of these
// things, the only fastest way is just adopt the scaling algorithm
// written in the TimeScale and come back to clean up this sometime later.
//
// After a few though, just replace the macro GetX(time) and GetCTime(x)
// should be enough, still we had to update the "omDisplayStart" and
// "omDisplayEnd" everytime these value has been changed.
#define	FIX_TIMESCALE_ROUNDING_ERROR
#ifdef	FIX_TIMESCALE_ROUNDING_ERROR
#define GetX(time)	(pomTimeScale->GetXFromTime(time) + imVerticalScaleWidth)
#define GetCTime(x)	(pomTimeScale->GetTimeFromX((x) - imVerticalScaleWidth))
#else
////////////////////////////////////////////////////////////////////////

// Macro definition for get X-coordinate from the given time
#define GetX(time)  (omDisplayStart == omDisplayEnd? -1: \
            (imVerticalScaleWidth + \
            (((time) - omDisplayStart).GetTotalSeconds() * \
            (imWindowWidth - imVerticalScaleWidth) / \
            (omDisplayEnd - omDisplayStart).GetTotalSeconds())))

// Macro definition for get time from the given X-coordinate
#define GetCTime(x)  ((imWindowWidth - imVerticalScaleWidth) == 0? TIMENULL: \
            (omDisplayStart + \
            (time_t)(((x) - imVerticalScaleWidth) * \
            (omDisplayEnd - omDisplayStart).GetTotalSeconds() / \
            (imWindowWidth - imVerticalScaleWidth))))

////////////////////////////////////////////////////////////////////////
// Damkerng 06/27/96:
#endif
////////////////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////////////////////
// RegnGantt

RegnGantt::RegnGantt(RegnDiagramViewer *popViewer, int ipGroupno,
    int ipVerticalScaleWidth, int ipVerticalScaleIndent,
    CFont *popVerticalScaleFont, CFont *popGanttChartFont,
    int ipGutterHeight, int ipOverlapHeight,
    COLORREF lpVerticalScaleTextColor, COLORREF lpVerticalScaleBackgroundColor,
    COLORREF lpHighlightVerticalScaleTextColor, COLORREF lpHighlightVerticalScaleBackgroundColor,
    COLORREF lpGanttChartTextColor, COLORREF lpGanttChartBackgroundColor,
    COLORREF lpHighlightGanttChartTextColor, COLORREF lpHighlightGanttChartBackgroundColor)
{
    SetViewer(popViewer, ipGroupno);
    SetVerticalScaleWidth(ipVerticalScaleWidth);
    SetVerticalScaleIndent(ipVerticalScaleIndent);
	SetFonts(ogRegnIndexes.VerticalScale, ogRegnIndexes.Chart);
    SetGutters(ipGutterHeight, ipOverlapHeight);
    SetVerticalScaleColors(lpVerticalScaleTextColor, lpVerticalScaleBackgroundColor,
        lpHighlightVerticalScaleTextColor, lpHighlightVerticalScaleBackgroundColor);
    SetGanttChartColors(lpGanttChartTextColor, lpGanttChartBackgroundColor,
        lpHighlightGanttChartTextColor, lpHighlightGanttChartBackgroundColor);

    // Initialize default values
    omDisplayStart = TIMENULL;
    omDisplayEnd = TIMENULL;
    imWindowWidth = 0;          // unessential -- WM_SIZE will initialize this
    bmIsFixedScaling = TRUE;    // unessential -- will be set in method SetDisplayWindow()
    omCurrentTime = TIMENULL;
    omMarkTimeStart = TIMENULL;
    omMarkTimeEnd = TIMENULL;
    pomStatusBar = NULL;
	prmActualBkBar = NULL;
	pomTimeScale = NULL;
    bmIsMouseInWindow = FALSE;
    bmIsControlKeyDown = FALSE;
    imHighlightLine = -1;
    imCurrentBar = -1;
	imCurrentBkBar = -1;
	imContextItem = -1;
	bmContextBarSet = FALSE;
	imContextBarNo = -1;
	bmActiveLineSet = FALSE;  // There is a selected line ?
	lmActiveUrno = -1;	// Urno of last selected line

	// Id 1-Oct-96
	// This will help us create a virtual double click on a double click after SetCursorPos().
	omLastClickedPosition = CPoint(-1, -1);

    // Required only if you allow moving/resizing
    SetBorderPrecision(5);
    umResizeMode = HTNOWHERE;
}

RegnGantt::~RegnGantt()
{
	pogButtonList->UnRegisterTimer(this);    // install the timer
}

void RegnGantt::SetViewer(RegnDiagramViewer *popViewer, int ipGroupno)
{
    pomViewer = popViewer;
    imGroupno = ipGroupno;
}

void RegnGantt::SetVerticalScaleWidth(int ipWidth)
{
    imVerticalScaleWidth = ipWidth;
}

void RegnGantt::SetVerticalScaleIndent(int ipIndent)
{
    imVerticalScaleIndent = ipIndent;
}

void RegnGantt::SetFonts(int ipIndex1, int ipIndex2)
{
	LOGFONT rlLogFont;
    pomVerticalScaleFont = &ogScalingFonts[ipIndex1];//popVerticalScaleFont;
    pomGanttChartFont = &ogScalingFonts[ipIndex2];//&ogSmallFonts_Regular_4;//popGanttChartFont;
	pomGanttChartFont->GetLogFont(&rlLogFont);

	switch(ipIndex2)
	{
	case 0 : SetGutters(imGutterHeight, 2);
		break;
	case 1 : SetGutters(imGutterHeight, 4);
		break;
	case 2 : SetGutters(imGutterHeight, 8);
		break;
	}
    // Calculate the normal height of a bar
    CDC dc;
    dc.CreateCompatibleDC(NULL);
    if (pomGanttChartFont)
        dc.SelectObject(pomGanttChartFont);
    TEXTMETRIC tm;
    dc.GetTextMetrics(&tm);
    imBarHeight = tm.tmHeight + tm.tmExternalLeading + 2;
    imLeadingHeight = (tm.tmExternalLeading + 2) / 2;
    dc.DeleteDC();
}

void RegnGantt::SetGutters(int ipGutterHeight, int ipOverlapHeight)
{
    imGutterHeight = ipGutterHeight;
    imOverlapHeight = ipOverlapHeight;
}

void RegnGantt::SetVerticalScaleColors(COLORREF lpTextColor, COLORREF lpBackgroundColor,
    COLORREF lpHighlightTextColor, COLORREF lpHighlightBackgroundColor)
{
    lmVerticalScaleTextColor = lpTextColor;
    lmVerticalScaleBackgroundColor = lpBackgroundColor;
    lmHighlightVerticalScaleTextColor = lpHighlightTextColor;
    lmHighlightVerticalScaleBackgroundColor = lpHighlightBackgroundColor;
}

void RegnGantt::SetGanttChartColors(COLORREF lpTextColor, COLORREF lpBackgroundColor,
    COLORREF lpHighlightTextColor, COLORREF lpHighlightBackgroundColor)
{
    lmGanttChartTextColor = lpTextColor;
    lmGanttChartBackgroundColor = lpBackgroundColor;
    lmHighlightGanttChartTextColor = lpHighlightTextColor;
    lmHighlightGanttChartBackgroundColor = lpHighlightBackgroundColor;
}

// Attentions:
// Return the height that it's expected for being displayed
// Be careful, this one may be called before the CListBox was created.
//
int RegnGantt::GetGanttChartHeight()
{
    int ilHeight = 0;
    for (int ilLineno = 0; ilLineno < pomViewer->GetLineCount(imGroupno); ilLineno++)
        ilHeight += GetLineHeight(ilLineno);

    return ilHeight;
}

int RegnGantt::GetLineHeight(int ilLineno)
{
	int ilVisualMaxOverlapLevel = pomViewer->GetVisualMaxOverlapLevel(imGroupno, ilLineno);
	return (2 * imGutterHeight) + imBarHeight + (ilVisualMaxOverlapLevel * imOverlapHeight);
}

BOOL RegnGantt::Create(DWORD dwStyle, const RECT &rect, CWnd *pParentWnd, UINT nID)
{
    // Make sure that the viewer is already given
    if (pomViewer == NULL)
        return FALSE;

    // Make sure that all essential style is defined
    // WS_CLIPSIBLINGS is very important for the ListBox so it does not paint outside its window
    dwStyle |= WS_CHILD | WS_VISIBLE | WS_VSCROLL | WS_CLIPSIBLINGS;
    dwStyle |= LBS_NOINTEGRALHEIGHT | LBS_OWNERDRAWVARIABLE;
    dwStyle |= LBS_MULTIPLESEL;

    // Create the window
    if (CListBox::Create(dwStyle, rect, pParentWnd, nID) == FALSE)
        return FALSE;

    // Create items according to what's in the Viewer
    ResetContent();
    for (int ilLineno = 0; ilLineno < pomViewer->GetLineCount(imGroupno); ilLineno++)
    {
        AddString("");
        SetItemHeight(ilLineno, GetLineHeight(ilLineno));
    }

    return TRUE;
}

void RegnGantt::SetStatusBar(CStatusBar *popStatusBar)
{
    pomStatusBar = popStatusBar;
}

void RegnGantt::SetTimeScale(CTimeScale *popTimeScale)
{
    pomTimeScale = popTimeScale;
}

void RegnGantt::SetBorderPrecision(int ipBorderPrecision)
{
    imBorderPreLeft = ipBorderPrecision / 2;
    imBorderPreRight = (ipBorderPrecision+1) / 2;
}

void RegnGantt::SetDisplayWindow(CTime opDisplayStart, CTime opDisplayEnd, BOOL bpFixedScaling)
{
    omDisplayStart = opDisplayStart;
    omDisplayEnd = opDisplayEnd;
    bmIsFixedScaling = bpFixedScaling;

    if (m_hWnd == NULL)
        imWindowWidth = 0;  // window is still not opened
    else
    {
        CRect rect;
        GetWindowRect(&rect);   // can't use client rect (vertical scroll may less its width)
        imWindowWidth = rect.Width();   // however, this is correct only if window has no border
        RepaintGanttChart();    // redraw the entire screen
    }
}

void RegnGantt::SetDisplayStart(CTime opDisplayStart)
{
    if (bmIsFixedScaling)   // must shift both start/end time while keeping the old time span
        omDisplayEnd = opDisplayStart + (omDisplayEnd - omDisplayStart).GetTotalSeconds();
    omDisplayStart = opDisplayStart;
    RepaintGanttChart();    // redraw the entire screen
}

void RegnGantt::SetCurrentTime(CTime opCurrentTime)
{                               
    RepaintGanttChart(-1, omCurrentTime, opCurrentTime);
    omCurrentTime = opCurrentTime;
}

void RegnGantt::SetMarkTime(CTime opMarkTimeStart, CTime opMarkTimeEnd)
{
	// There are two reasons for these min/max.
	// First, we have to make sure that the start time parameter for RepaintGanttChart()
	// should not be greater than the end time parameter.
	// Second, we should not refresh markers in the vertical scale, since this will
	// disturb the repainting of the vertical scale when we are drawing these marker lines
	// together with the focus rectangle when moving/resizing a bar.
    RepaintGanttChart(-1,
		max(omDisplayStart, min(omMarkTimeStart, opMarkTimeStart)),
		max(omDisplayStart, max(omMarkTimeStart, opMarkTimeStart)));
    omMarkTimeStart = opMarkTimeStart;
    RepaintGanttChart(-1,
		max(omDisplayStart, min(omMarkTimeEnd, opMarkTimeEnd)),
		max(omDisplayStart, max(omMarkTimeEnd, opMarkTimeEnd)));
    omMarkTimeEnd = opMarkTimeEnd;
}

void RegnGantt::RepaintVerticalScale(int ipLineno, BOOL bpErase)
{
    if (m_hWnd == NULL) // window hasn't been opened?
        return;

    CRect rcPaint;
    if (GetItemRect(ipLineno, &rcPaint) != LB_ERR)  // valid item in list box?
    {
        rcPaint.right = imVerticalScaleWidth - 1;
        InvalidateRect(&rcPaint, bpErase);
    }
}

void RegnGantt::RepaintGanttChart(int ipLineno, CTime opStartTime, CTime opEndTime, BOOL bpErase)
{
    if (m_hWnd == NULL) // window hasn't been opened?
        return;

    CRect rcPaint;
    if (ipLineno == -1) // repaint the whole chart?
        GetClientRect(&rcPaint);
    else
        GetItemRect(ipLineno, &rcPaint);

    // Update the GanttChart body
    if (opStartTime <= omDisplayStart)
        rcPaint.left = imVerticalScaleWidth;
    else
        rcPaint.left = (int)GetX(opStartTime);

    // Use the client width if user want to repaint to the end of time
    if (opEndTime != TIMENULL && opEndTime <= omDisplayEnd)
        rcPaint.right = (int)GetX(opEndTime) + 1;

    InvalidateRect(&rcPaint, bpErase);
}

void RegnGantt::RepaintItemHeight(int ipLineno)
{
    if (m_hWnd == NULL) // window hasn't been opened?
        return;

    // This repaint method will recalculate the item height
    SetItemHeight(ipLineno, GetLineHeight(ipLineno));

    CRect rcItem, rcPaint;
    GetClientRect(&rcPaint);
    GetItemRect(ipLineno, &rcItem);
    rcPaint.top = rcItem.top;
    InvalidateRect(&rcPaint, TRUE);
}


/////////////////////////////////////////////////////////////////////////////
// RegnGantt implementation

void RegnGantt::MeasureItem(LPMEASUREITEMSTRUCT lpMeasureItemStruct)
{
    // We have to define MeasureItem() here since MFC places an ASSERT inside
    // the default MeasureItem() of an owner-drawn CListBox.
}

void RegnGantt::DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct)
{
////////////////////////////////////////////////////////////////////////
// Damkerng 06/27/96:
// To let the GanttChart know automatically those changes in TimeScale.
// We had to reset the value of omDisplayStart and omDisplayEnd everytime.
	omDisplayStart = GetCTime(imVerticalScaleWidth);
	omDisplayEnd = GetCTime(imWindowWidth);
////////////////////////////////////////////////////////////////////////
    
	int itemID = lpDrawItemStruct->itemID;

    // Attention:
    // This optimization will the speed of displaying GanttChart very a lot.
    // However, this works because we hadn't display any selected or focus items.
    // (We use the "imCurrentItem" variable instead.)
    // So, be careful since this assumption may be changed in the future.
    //
    if (itemID == -1)
        return;
    if (!(lpDrawItemStruct->itemAction & ODA_DRAWENTIRE))
        return; // nothing more to do if listbox just change its selection and/or focus item

    // Drawing routines start here
    CDC dc;
    dc.Attach(lpDrawItemStruct->hDC);
    CRect rcItem(&lpDrawItemStruct->rcItem);
    CRect rcClip;
    dc.GetClipBox(&rcClip);

    // Draw vertical scale if necessary
    if (rcClip.left < imVerticalScaleWidth)
        DrawVerticalScale(&dc, itemID, rcItem);

	DrawDottedLines(&dc, itemID, rcItem);

    // Draw time lines and bars if necessary
    if (imVerticalScaleWidth <= rcClip.right)
    {
        CRgn rgn;
        CRect rect(imVerticalScaleWidth, rcItem.top, rcItem.right, rcItem.bottom);
        rgn.CreateRectRgnIndirect(&rect);
        dc.SelectClipRgn(&rgn);
		DrawBackgroundBars(&dc, itemID, rcItem, rcClip);
        DrawTimeLines(&dc, rcItem.top, rcItem.bottom);
        DrawGanttChart(&dc, itemID, rcItem, rcClip);
        if (umResizeMode != HTNOWHERE)
            DrawFocusRect(&dc, &omRectResize);
        dc.SelectClipRgn(NULL);
    }

    dc.Detach();
}


/////////////////////////////////////////////////////////////////////////////
// RegnGantt implementation helper functions

void RegnGantt::DrawVerticalScale(CDC *pDC, int itemID, const CRect &rcItem)
{
    CPoint point;
    ::GetCursorPos(&point);
    ScreenToClient(&point);

    // Check if we need to change the highlight text in VerticalScale
    BOOL blHighlight;
    if (!bmIsMouseInWindow || !rcItem.PtInRect(point))
        blHighlight = FALSE;
    else
    {
        blHighlight = TRUE;
        if (imHighlightLine != itemID)
            RepaintVerticalScale(imHighlightLine, FALSE);
        imHighlightLine = itemID;
    }

    // Drawing routines start here
    CString s = pomViewer->GetLineText(imGroupno, itemID);
	s += pomViewer->GetTmo(imGroupno, itemID);
	s += pomViewer->GetApuInOp(imGroupno, itemID);
    CFont *pOldFont = pDC->SelectObject(pomVerticalScaleFont);
    COLORREF nOldTextColor = pDC->SetTextColor(blHighlight?
        lmHighlightVerticalScaleTextColor: lmVerticalScaleTextColor);
    COLORREF nOldBackgroundColor = pDC->SetBkColor(blHighlight?
        lmHighlightVerticalScaleBackgroundColor: lmVerticalScaleBackgroundColor);
    CRect rect(rcItem);
    int left = rect.left + imVerticalScaleIndent;
	rect.right = imVerticalScaleWidth - 2;
    pDC->ExtTextOut(left, rect.top, ETO_CLIPPED | ETO_OPAQUE, &rect, s, lstrlen(s), NULL);
    pDC->SetBkColor(nOldBackgroundColor);
    pDC->SetTextColor(nOldTextColor);
    pDC->SelectObject(pOldFont);
}

void RegnGantt::DrawBackgroundBars(CDC *pDC, int itemID, const CRect &rcItem, const CRect &rcClip)
{
	int ilPixel = 1;
    if (omDisplayStart >= omDisplayEnd) // no space to display?
        return;

    // Draw each bar if it in the range of the clipped box
    for (int i = 0; i < pomViewer->GetBkBarCount(imGroupno, itemID); i++)
    {
        REGN_BKBARDATA *prlBkBar = pomViewer->GetBkBar(imGroupno, itemID, i);

        if (!IsOverlapped(prlBkBar->StartTime, prlBkBar->EndTime, omDisplayStart, omDisplayEnd))
            continue;   // skip bar which is out of time range

        int left = (int)GetX(prlBkBar->StartTime);
        int right = (int)GetX(prlBkBar->EndTime);
        int top = rcItem.top;
        int bottom = rcItem.bottom;

        if (!IsOverlapped(left, right, rcClip.left, rcClip.right))
            continue;   // skip bar which is out of clipping region

		// Temp
		if (pomViewer->GetLineText(imGroupno, itemID) == "I310C")
			ASSERT(1);

        GanttBar paintbar(pDC, CRect(left, top, right, bottom),
            FRAMERECT, MARKFULL, prlBkBar->MarkerBrush,
            prlBkBar->TextArr, pomGanttChartFont, WHITE /*lmGanttChartTextColor*/, ilPixel,
			FALSE, CRect(0,0,0,0), NULL, NULL, TA_LEFT, prlBkBar->TextDep, TA_RIGHT);
    }
}

void RegnGantt::DrawGanttChart(CDC *pDC, int itemID, const CRect &rcItem, const CRect &rcClip)
{
	int ilPixel = 1;

    if (omDisplayStart >= omDisplayEnd) // no space to display?
        return;

    // Draw each bar if it in the range of the clipped box
    for (int i = 0; i < pomViewer->GetBarCount(imGroupno, itemID); i++)
    {
        REGN_BARDATA *prlBar = pomViewer->GetBar(imGroupno, itemID, i);

   //    if (!IsOverlapped(prlBar->StartTime, prlBar->EndTime, omDisplayStart, omDisplayEnd))
   //         continue;   // skip bar which is out of time range

		if(ogRegnIndexes.Chart == MS_SANS12)
		{
		    ilPixel = 2;
		}
		else
		{
			ilPixel = 1;
		}

		CTime olEnd = prlBar->EndTime;

        int left = (int)GetX(prlBar->StartTime);
        int right = (int)GetX(olEnd);
        int top = rcItem.top + imGutterHeight + (prlBar->OverlapLevel * imOverlapHeight);
        int bottom = top + imBarHeight;

        if (!IsOverlapped(left, right, rcClip.left, rcClip.right))
            continue;   // skip bar which is out of clipping region

//		if (prlBar->IsDelayed != CFST_NOTSET)
//		{
//			olEnd -= prlBar->Delay;
//
//			int DelayLeft = (int)GetX(olEnd);
//			int ilBrushIndex = prlBar->IsDelayed == CFST_CONFIRMED ? FIRSTCONFLICTCOLOR+(CFI_DELAYED*2)+1 : FIRSTCONFLICTCOLOR+(CFI_DELAYED*2);
//			GanttBar paintbar(pDC, CRect(left, top, right, bottom),
//				prlBar->FrameType, prlBar->MarkerType, prlBar->MarkerBrush,
//				prlBar->Text, pomGanttChartFont, lmGanttChartTextColor, ilPixel,
//				TRUE, CRect(DelayLeft, top, right, bottom),ogBrushs[ilBrushIndex]);
//		}
//		else
		{
			GanttBar paintbar(pDC, CRect(left, top, right, bottom),
				prlBar->FrameType, prlBar->MarkerType, prlBar->MarkerBrush,
				prlBar->Text, pomGanttChartFont, lmGanttChartTextColor, ilPixel);
		}
    }
}

void RegnGantt::DrawTimeLines(CDC *pDC, int top, int bottom)
{
    // Draw current time
    if (omCurrentTime != TIMENULL && IsBetween(omCurrentTime, omDisplayStart, omDisplayEnd))
    {
        CPen penRed(PS_SOLID, 0, RGB(255, 0, 0));
        CPen *pOldPen = pDC->SelectObject(&penRed);
        int x = (int)GetX(omCurrentTime);
        pDC->MoveTo(x, top);
        pDC->LineTo(x, bottom);
        pDC->SelectObject(pOldPen);
    }

    // Draw marked time start
    if (omMarkTimeStart != TIMENULL && IsBetween(omMarkTimeStart, omDisplayStart, omDisplayEnd))
    {
        CPen penYellow(PS_SOLID, 0, RGB(255, 255, 0));
        CPen *pOldPen = pDC->SelectObject(&penYellow);
        int x = (int)GetX(omMarkTimeStart);
        pDC->MoveTo(x, top);
        pDC->LineTo(x, bottom);
        pDC->SelectObject(pOldPen);
    }

    // Draw marked time end
    if (omMarkTimeEnd != TIMENULL && IsBetween(omMarkTimeEnd, omDisplayStart, omDisplayEnd))
    {
        CPen penYellow(PS_SOLID, 0, RGB(255, 255, 0));
        CPen *pOldPen = pDC->SelectObject(&penYellow);
        int x = (int)GetX(omMarkTimeEnd);
        pDC->MoveTo(x, top);
        pDC->LineTo(x, bottom);
        pDC->SelectObject(pOldPen);
    }
}

void RegnGantt::DrawFocusRect(CDC *pDC, const CRect &rect)
{
	CRect rcFocus = rect;
	rcFocus.right++;	// extra pixel to help DrawFocusRect() work correctly
	pDC->DrawFocusRect(&rcFocus);

	// Before finish: please insert the correct code for status bar while moving/resizing
    if (pomStatusBar != NULL)
	{
		CString s = GetCTime(rcFocus.left).Format("%H%M") + " - "
			+ GetCTime(rcFocus.right - 1).Format("%H%M");
		pomStatusBar->SetPaneText(0, s);
	}
}

BEGIN_MESSAGE_MAP(RegnGantt, CWnd)
	//{{AFX_MSG_MAP(RegnGantt)
	ON_WM_CREATE()
	ON_WM_DESTROY()
    ON_WM_SIZE()
    ON_WM_ERASEBKGND()
	ON_WM_SETCURSOR()
    ON_WM_MOUSEMOVE()
    ON_WM_TIMER()
    ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_WM_LBUTTONDBLCLK()
	ON_WM_RBUTTONDOWN()
	ON_WM_KEYDOWN()
	ON_WM_KEYUP()
    ON_MESSAGE(WM_USERKEYDOWN, OnUserKeyDown)
    ON_MESSAGE(WM_USERKEYUP, OnUserKeyUp)
	ON_COMMAND(11, OnMenuRegnWorkOn)
	ON_COMMAND(12, OnMenuFreeEmployees)
	ON_COMMAND(21, OnMenuFlightConfirm)
    ON_COMMAND(22, OnMenuFlightWorkOn)
	ON_COMMAND_RANGE(23, 40, OnMenuFlightAcceptConflict)
	ON_COMMAND(44, OnMenuCreateDemand)
	ON_COMMAND(45, OnMenuCheckConflict)
	ON_COMMAND(46, OnMenuDisplayDebugInfo)
    ON_MESSAGE(WM_DRAGOVER, OnDragOver)
    ON_MESSAGE(WM_DROP, OnDrop)  
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// RegnGantt message handlers
int RegnGantt::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;
	
    m_DragDropTarget.RegisterTarget(this, this);
	
	return 0;
}

void RegnGantt::OnDestroy() 
{
	if (bgModal == TRUE)
		return;
	m_DragDropTarget.Revoke();
	CWnd::OnDestroy();
}

void RegnGantt::OnSize(UINT nType, int cx, int cy)
{
    CListBox::OnSize(nType, cx, cy);

    CRect rect;
    GetWindowRect(&rect);   // don't use cx (vertical scroll may less the width of client rect)

    if (bmIsFixedScaling)   // fixed-scaling mode?
    {
        if (imWindowWidth == 0) // first time initialization?
            imWindowWidth = rect.Width();
        omDisplayEnd = GetCTime(rect.Width());   // has to recalculate the right most boundary?
    }
    else    // must be automatic variable-scaling mode
    {
        if (rect.Width() != imWindowWidth) // display window changed?
            InvalidateRect(NULL);   // repaint whole GanttChart
    }
    imWindowWidth = rect.Width();
}

// Attention:
// Be careful, this may not work if you try to develop horizontal-scrolling in GanttChart.
// Generally, if the ListBox need to be repainted, it will send the message WM_ERASEBKGND.
// But if you allow horizontal-scrolling, WM_ERASEBKGND will be called before the new
// horizontal position could be detected by dc.GetWindowOrg().x in DrawItem().
// However, this version work fine since there's no such scrolling.
// Then, we can assume that the beginning offset will be 0 all the time.
//
BOOL RegnGantt::OnEraseBkgnd(CDC* pDC)
{
    CRect rectErase;
    pDC->GetClipBox(&rectErase);

    // Draw the VerticalScale if the user specify it to be displayed
    if (imVerticalScaleWidth > 0)
    {
        CBrush brush(lmVerticalScaleBackgroundColor);
        CRect rect(0, rectErase.top, imVerticalScaleWidth, rectErase.bottom);
        pDC->FillRect(&rect, &brush);

        // Draw seperator line between VerticalScale and the body of GanttChart
        CPen penBlack(PS_SOLID, 0, ::GetSysColor(COLOR_WINDOWFRAME));
        CPen *pOldPen = pDC->SelectObject(&penBlack);
        pDC->MoveTo(imVerticalScaleWidth-2, rectErase.top);
        pDC->LineTo(imVerticalScaleWidth-2, rectErase.bottom);
        CPen penWhite(PS_SOLID, 0, ::GetSysColor(COLOR_BTNHIGHLIGHT));
        pDC->SelectObject(&penWhite);
        pDC->MoveTo(imVerticalScaleWidth-1, rectErase.top);
        pDC->LineTo(imVerticalScaleWidth-1, rectErase.bottom);
        pDC->SelectObject(pOldPen);
    }

    // Draw the background of the body of GanttChart
    CBrush brush(lmGanttChartBackgroundColor);
    CRect rect(imVerticalScaleWidth, rectErase.top, rectErase.right, rectErase.bottom);
    pDC->FillRect(&rect, &brush);

    // Draw the vertical current time and marked time lines
    DrawTimeLines(pDC, rectErase.top, rectErase.bottom);

    // Tell Windows there's nothing more to do
    return TRUE;
}

BOOL RegnGantt::OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message) 
{
	// Id 2-Oct-96
	// This override cursor setting will remove cursor flickering when the user press
	// control key but still not begin the moving/resizing yet.
	//
    if (bmIsControlKeyDown && imHighlightLine != -1 && imCurrentBar != -1)
		return TRUE;	// override the default processing

	return CWnd::OnSetCursor(pWnd, nHitTest, message);
}

void RegnGantt::OnMouseMove(UINT nFlags, CPoint point)
{
    if (umResizeMode != HTNOWHERE)  // in SetCapture()?
    {
        OnMovingOrResizing(point, nFlags & MK_LBUTTON);
        return;
    }

    CListBox::OnMouseMove(nFlags, point);
    UpdateGanttChart(point, nFlags & MK_CONTROL);
}

void RegnGantt::OnTimer(UINT nIDEvent)
{
	if(::IsWindow(this->GetSafeHwnd()))
	{
		if (umResizeMode != HTNOWHERE)  // in SetCapture()?
			return;

		if (nIDEvent == 0)  // the last timer message, clear everything
		{
			bmIsMouseInWindow = FALSE;
			RepaintVerticalScale(imHighlightLine, FALSE);
			imHighlightLine = -1;			// no more selected line
			UpdateBarStatus(-1, -1, -1);	// no more status if mouse is outside of GanttChart
			return;
		}

		CPoint point;
		::GetCursorPos(&point);
		ScreenToClient(&point);    
		UpdateGanttChart(point, ::GetKeyState(VK_CONTROL) & 0x8080);
			// This statement has to be fixed for using both in Windows 3.11 and NT.
			// In Windows 3.11 the bit 0x80 will be turned on if control key is pressed.
			// In Windows NT, the bit 0x8000 will be turned on if control key is pressed.
	}
	else
	{
		ASSERT(0);
	}
}

void RegnGantt::OnLButtonDown(UINT nFlags, CPoint point)
{
	// Id 1-Oct-96
	// This will help us create a virtual double click on a double click after SetCursorPos().
	CPoint olLastClickedPosition = omLastClickedPosition;
	omLastClickedPosition = point;
	if (olLastClickedPosition == point)
	{
		OnLButtonDblClk(nFlags, point);
		omLastClickedPosition = CPoint(-1, -1);
		return;
	}

    // Check if the user starts moving/resizing mouse action
    if ((nFlags & MK_LBUTTON) && (nFlags & MK_CONTROL) &&
        BeginMovingOrResizing(point))
        return; // not call the default after SetCapture()

	// Check dragging from the vertical scale (dragging DutyBar)
    imHighlightLine = GetItemFromPoint(point);
	if (IsBetween(point.x, 0, imVerticalScaleWidth-2) && (imHighlightLine != -1))	// from VerticalScale?
	{
		DragRegnBegin(imHighlightLine);
		return;	// not call the default after dragging;
	}

    // Bring bar to front / Send bar to back
    imCurrentBar = GetBarnoFromPoint(imHighlightLine, point);
    if (imCurrentBar != -1)
    {
		// Update the time band (two vertical yellow lines)
		REGN_BARDATA *prlBar = pomViewer->GetBar(imGroupno, imHighlightLine, imCurrentBar);
		TIMEPACKET olTimePacket;
		olTimePacket.StartTime = prlBar->StartTime;
		olTimePacket.EndTime = prlBar->EndTime;
		//GetTimeOfLongestUnresolvedDemand(*prlBar,olTimePacket.StartTime,olTimePacket.EndTime);

		ogCCSDdx.DataChanged((void *)this, STAFFDIAGRAM_UPDATETIMEBAND, &olTimePacket);

		// Bring bar to front / Send bar to back
		int ilOldOverlapLevel = prlBar->OverlapLevel;
		int ilOldLineHeight = GetLineHeight(imHighlightLine);
        REGN_BARDATA rlBar = *prlBar;	// copy all data of this bar
		rlBar.Indicators.RemoveAll();	// remove indicators, will be dealloc in DeleteBar()
		for (int i = 0; i < prlBar->Indicators.GetSize(); i++)
			rlBar.Indicators.NewAt(i, prlBar->Indicators[i]);
        pomViewer->DeleteBar(imGroupno, imHighlightLine, imCurrentBar);
		BOOL blBringBarToFront = !(nFlags & MK_SHIFT);
        imCurrentBar = pomViewer->CreateBar(imGroupno, imHighlightLine, &rlBar, blBringBarToFront);
			// unnecessary to delete the NewAt() bars since create bar automatic copy it

		// Update the display, also check if line height was changed
		if (GetLineHeight(imHighlightLine) == ilOldLineHeight)
		{
			RepaintGanttChart(imHighlightLine, TIMENULL, TIMENULL, TRUE);
		}
		else
		{
			//RepaintItemHeight(imHighlightLine);
			LONG lParam = MAKELONG(imHighlightLine, imGroupno);
			pomViewer->pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM, UD_UPDATELINEHEIGHT, lParam);
		}

		// Id 22-Sep-96
		// To make drag-and-drop operation works properly, we have to change the position
		// of the mouse cursor also, since the drag-and-drop will use the current position
		// as a basis for selecting a bar.
		prlBar = pomViewer->GetBar(imGroupno, imHighlightLine, imCurrentBar);
			// reload the bar again (it may be already moved to some other position)
		int ilNewOverlapLevel = prlBar->OverlapLevel;
		point.Offset(0, (ilNewOverlapLevel - ilOldOverlapLevel) * imOverlapHeight);
	}

    CListBox::OnLButtonDown(nFlags, point);

	// Update the cursor position to make sure that the cursor will always be over
	// the bar we just click on.
	CPoint olMousePosition = point;
	ClientToScreen(&olMousePosition);
	::SetCursorPos(olMousePosition.x, olMousePosition.y);
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));

	// Id 1-Oct-96
	// This will help us create a virtual double click on a double click after SetCursorPos().
	omLastClickedPosition = point;

	// Checking for dragging a job bar?
	// Notes: Here's the best place for checking for dragging from job bar.
	// We cannot simply regard OnLButtonDown() as the beginning of dragging,
	// since we defined OnLButtonDown() for bringing bar to front or to back.
	//
	// Id 25-Sep-96
	// All of the above are wrong. Try to move it back and you will see.
	// If we place this in OnMouseMove(), we can initiate the drag operation by
	// holding on the left mouse from some place which does not allow dragging,
	// then move the mouse over some bars, the drag operation will start when the
	// cursor is on some bar. This is weird and should not happened.
	if ((nFlags & MK_LBUTTON) &&
		imHighlightLine != -1 && imCurrentBar != -1)
	{
		DragFlightBarBegin(imHighlightLine, imCurrentBar);
		return;
	}
}

void RegnGantt::OnLButtonUp(UINT nFlags, CPoint point) 
{
    if (umResizeMode != HTNOWHERE)  // in SetCapture()?
    {
		// This allow a fast way for terminating moving or resizing mode
        OnMovingOrResizing(point, nFlags & MK_LBUTTON);
        return;
    }

	CListBox::OnLButtonUp(nFlags, point);
}

void RegnGantt::OnLButtonDblClk(UINT nFlags, CPoint point) 
{
    // Check if the user starts moving/resizing mouse action
    if ((nFlags & MK_LBUTTON) && (nFlags & MK_CONTROL) &&
        BeginMovingOrResizing(point))
        return; // not call the default after SetCapture()

    // Pop-up detailed window
    if ((imHighlightLine != -1) && (point.x < imVerticalScaleWidth))	// vertical scale?
	{
//		CString s = pomViewer->GetLineText(imGroupno, imHighlightLine);
//		char clBuf[255]; strcpy(clBuf, s);
//		BOOL blArrival=true, blDeparture=true;
//		CWnd *polWnd = GetParent()->GetParent()->GetParent();
//		new FlightDetailWindow(polWnd,(const char *) clBuf, blArrival, blDeparture, omDisplayStart, omDisplayEnd);
    }
	if ((imHighlightLine != -1) && (imCurrentBar != -1) && (point.x >= imVerticalScaleWidth))	// bar?
    {
		// on a (aggregate) demand bar
		REGN_BARDATA *prlBar = pomViewer->GetBar(imGroupno, imHighlightLine, imCurrentBar);
		if(prlBar != NULL)
		{
			if (prlBar->FlightUrno > 0 || prlBar->DepartureUrno > 0)
			{
				new FlightDetailWindow(this,prlBar->FlightUrno,prlBar->DepartureUrno,prlBar->IsShadowBar,ALLOCUNITTYPE_REGN);
			}
			else if(prlBar->DemandUrno > 0)
			{
				// registration dependant demand without flight due to aircraft change
				DEMANDDATA *prlDemand = ogDemandData.GetDemandByUrno(prlBar->DemandUrno);
				if(prlDemand != NULL)
				{
					ACRDATA *prlAcr = ogAcrData.GetAcrByUrno(prlDemand->Uref);
					if(prlAcr != NULL)
					{
						// open the flight det dlg for the registration
						new FlightDetailWindow(this,0L,0L,prlBar->IsShadowBar,ALLOCUNITTYPE_REGN,prlAcr);
					}
				}
			}
		}
    }
	else if ((imHighlightLine != -1) && (imCurrentBkBar != -1))
	{
		// on a background bar only
		REGN_BKBARDATA *prlBkBar = pomViewer->GetBkBar(imGroupno, imHighlightLine, imCurrentBkBar);
		if(prlBkBar != NULL)
		{
			new FlightDetailWindow(this,prlBkBar->FlightUrno,prlBkBar->FlightRotationUrno,true,ALLOCUNITTYPE_REGN);
		}

	}
}

void RegnGantt::OnRButtonDown(UINT nFlags, CPoint point) 
{
    if ((nFlags & MK_LBUTTON) && (nFlags & MK_CONTROL) &&
        BeginMovingOrResizing(point))
        return; // not call the default after SetCapture()

	int itemID, ilBarno;
	if ((itemID = GetItemFromPoint(point)) == -1)
		return;	// not click in the Gantt window

	if (point.x < imVerticalScaleWidth - 2)	// in VerticalScale?
	{
/*		CMenu menu;
		menu.CreatePopupMenu();
		for (int i = menu.GetMenuItemCount(); i > 0; i--)
	        menu.RemoveMenu(i, MF_BYPOSITION);
	    //menu.AppendMenu(MF_STRING,11, "Bearbeiten");	// work on
	    menu.AppendMenu(MF_STRING,11, GetString(IDS_STRING61352));	// work on

		ClientToScreen(&point);
		imContextItem = itemID;
        menu.TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, point.x, point.y, this, NULL);*/
		return;
	}


	if ((ilBarno = GetBarnoFromPoint(itemID, point)) != -1)	// on a bar
	{
        REGN_BARDATA *prlBar = pomViewer->GetBar(imGroupno, itemID, ilBarno);
		bmContextBarSet = TRUE;	// Save bar data for handling after context menu closed
		rmContextBar = *prlBar;	// copy all data of this bar
		imContextItem = itemID;
		imContextBarNo = ilBarno;

		CMenu menu;
		menu.CreatePopupMenu();
		for (int i = menu.GetMenuItemCount(); i > 0; i--)
	        menu.RemoveMenu(i, MF_BYPOSITION);
		if (prlBar->IsShadowBar == TRUE)
		{
			//menu.AppendMenu(MF_STRING,41, "Mitarbeiter folgen");// jobs follow to new Regn
			menu.AppendMenu(MF_STRING,41, GetString(IDS_STRING61371));// jobs follow to new Regn
			//menu.AppendMenu(MF_STRING,42, "Eins�tze l�schen");	// delete jobs on shadow bar
			menu.AppendMenu(MF_STRING,42, GetString(IDS_STRING61372));	// delete jobs on shadow bar
			//menu.AppendMenu(MF_STRING,43, "L�schen");			// delete shadow bar
			menu.AppendMenu(MF_STRING,43, GetString(IDS_STRING61210));			// delete shadow bar

			CCSPtrArray<JOBDATA>  olJobs;

			FLIGHTDATA *prlFlight = ogFlightData.GetFlightByUrno(prlBar->FlightUrno);
			if (prlFlight != NULL)
			{
				if (olJobs.GetSize() == 0)
				{
					menu.EnableMenuItem(41,MF_BYCOMMAND || MF_GRAYED);
					menu.EnableMenuItem(42,MF_BYCOMMAND || MF_GRAYED);
				}
				else
				{
					menu.EnableMenuItem(43,MF_BYCOMMAND || MF_GRAYED);
				}
			}
		}
		else
		{
			
//			menu.AppendMenu(MF_STRING,21, GetString(IDS_STRING61353));	// confirm job
//			menu.AppendMenu(MF_STRING,22, GetString(IDS_STRING61352));	// work on
			menu.AppendMenu(MF_STRING,12, GetString(IDS_STRING61376));	// to find another free employee

			if (prlBar->Type == BAR_REGNFLIGHT)
			{
				CCSPtrArray<JOBDATA>  olJobs;
				ogJobData.GetJobsByFlur(olJobs,prlBar->FlightUrno);
				if (olJobs.GetSize() == 0)
				{
					FLIGHTDATA *prlFlight;
					if ((prlFlight = ogFlightData.GetFlightByUrno(prlBar->FlightUrno)) != NULL)
					{
						FLIGHTDATA *prlRotation = ogFlightData.GetRotationFlight(prlFlight);
						if (prlRotation != NULL)
						{
							ogJobData.GetJobsByFlur(olJobs,prlRotation->Urno,FALSE); // don't like JOBFM
						}
					}
				}
				CString olText;
				CCSPtrArray <CONFLICTDATA> olConflicts;
				CCSPtrArray <CONFLICTDATA> olRotationConflicts;
				long llUaft1 = prlBar->FlightUrno, llUaft2 = 0L;
				FLIGHTDATA *prlFlight = ogFlightData.GetFlightByUrno(llUaft1);
				FLIGHTDATA *prlRotation = NULL;
				if(prlFlight != NULL)
				{
					prlRotation = ogFlightData.GetRotationFlight(prlFlight);
					if(prlRotation != NULL)
					{
						llUaft2 = prlRotation->Urno;
					}
				}

				
				int ilConflNum = 24;
				int ilNumUnacceptedConflicts = 0;
				omAcceptConflictsMap.RemoveAll();
				omAcceptRotationConflictsMap.RemoveAll();
				if(prlFlight != NULL)
				{
					ogConflicts.GetFlightConflicts(olConflicts,llUaft1,ALLOCUNITTYPE_REGN,PERSONNELDEMANDS);
					if(prlRotation != NULL)
					{
						ogConflicts.GetFlightConflicts(olRotationConflicts,llUaft2,ALLOCUNITTYPE_REGN,PERSONNELDEMANDS);
						int ilCnt = olRotationConflicts.GetSize();
						for(int ilC = ilCnt-1; ilC >= 0; ilC--)
						{
							CONFLICTDATA *prlConflict2 = &olRotationConflicts[ilC];
//							if(ogConflicts.ConflictIsForRotation(prlConflict2->Type))
//							{
//								olRotationConflicts.RemoveAt(ilC);
//							}
						}
					}
				}
				else
				{
					// there is a conflict for demand without a flight
					ogConflicts.GetDemandConflicts(olConflicts,prlBar->DemandUrno);
				}

				int ilNumConflicts = olConflicts.GetSize();
				CString olFlightNum;
				if(ilNumConflicts > 0)
				{
					for	(int ilConfl = 0; ilConfl < ilNumConflicts; ilConfl++)
					{
						CONFLICTDATA *prlConflict = &olConflicts[ilConfl];
						//menu.AppendMenu(MF_STRING,25, "Konflikt akzeptieren");	// accept conflict
						ilConflNum++;
						olFlightNum.Empty();
//						if(!ogConflicts.ConflictIsForRotation(prlConflict->Type) && prlFlight != NULL)
//						{
//							// conflict is not for the rotation so print the flight num
						if(prlFlight != NULL)
						{
							olFlightNum = prlFlight->Fnum;
						}
//						}
						if(olFlightNum.IsEmpty())
						{
							olText.Format("%s: %s",GetString(IDS_STRING61357),ogConflicts.GetConflictTextByType(prlConflict->Type));
						}
						else
						{
							olText.Format("%s: %s %s",GetString(IDS_STRING61357),olFlightNum,ogConflicts.GetConflictTextByType(prlConflict->Type));
						}
						olText += CString(" ") + ogConflicts.GetAdditionalConflictText(olConflicts[ilConfl].Type,prlFlight);
						if(prlConflict->Confirmed)
						{
							menu.AppendMenu(MF_STRING|MF_GRAYED, ilConflNum, olText);
						}
						else
						{
							menu.AppendMenu(MF_STRING, ilConflNum, olText);
							ilNumUnacceptedConflicts++;
						}
						omAcceptConflictsMap.SetAt((void *) ilConflNum, (void *) prlConflict);
					}
					olConflicts.RemoveAll();
				}
				ilNumConflicts = olRotationConflicts.GetSize();
				if(ilNumConflicts > 0 && prlRotation != NULL)
				{
					for	(int ilConfl = 0; ilConfl < ilNumConflicts; ilConfl++)
					{
						CONFLICTDATA *prlConflict = &olRotationConflicts[ilConfl];
						//menu.AppendMenu(MF_STRING,25, "Konflikt akzeptieren");	// accept conflict
						ilConflNum++;
						olFlightNum = prlRotation->Fnum;
						olText.Format("%s: %s %s",GetString(IDS_STRING61357),olFlightNum,ogConflicts.GetConflictTextByType(prlConflict->Type));
						olText += CString(" ") + ogConflicts.GetAdditionalConflictText(olRotationConflicts[ilConfl].Type,prlRotation);
						if(prlConflict->Confirmed)
						{
							menu.AppendMenu(MF_STRING|MF_GRAYED, ilConflNum, olText);
						}
						else
						{
							menu.AppendMenu(MF_STRING, ilConflNum, olText);
							ilNumUnacceptedConflicts++;
						}
						omAcceptRotationConflictsMap.SetAt((void *) ilConflNum, (void *) prlConflict);
					}
					olRotationConflicts.RemoveAll();
				}

				if(ilNumUnacceptedConflicts > 1)
				{
					menu.AppendMenu(MF_STRING, 23, GetString(IDS_STRING61378));
				}

				if(ogBasicData.DisplayDebugInfo())
				{
					menu.AppendMenu(MF_STRING, 46, "Display Debug Info");
					menu.AppendMenu(MF_STRING, 45, "Check Conflict");
				}

			}
		}
		ClientToScreen(&point);
        menu.TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, point.x, point.y, this, NULL);
		return;
	}

	// Menu per creare Demand a mano
	if ((imHighlightLine != -1) && (imCurrentBkBar != -1))
	{
		// far vedere il volo
		REGN_BKBARDATA *prlBkBar = pomViewer->GetBkBar(imGroupno, imHighlightLine, imCurrentBkBar);
		if(prlBkBar != NULL)
		{
			prmActualBkBar = prlBkBar;
			CMenu menu;
			menu.CreatePopupMenu();
			for (int i = menu.GetMenuItemCount(); i > 0; i--)
				menu.RemoveMenu(i, MF_BYPOSITION);
			menu.AppendMenu(MF_STRING,44, GetString(IDS_CREATEDEMAND));
			ClientToScreen(&point);
			menu.TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, point.x, point.y, this, NULL);
			return;
		}

	}
	
	CWnd::OnRButtonDown(nFlags, point);
}

void RegnGantt::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	GetParent()->SendMessage(WM_KEYDOWN, nChar, MAKELONG(LOWORD(nRepCnt), LOWORD(nFlags)));
}

void RegnGantt::OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	GetParent()->SendMessage(WM_KEYUP, nChar, MAKELONG(LOWORD(nRepCnt), LOWORD(nFlags)));
}

LONG RegnGantt::OnUserKeyDown(UINT wParam, LONG lParam)
{
	int nIndex = GetTopIndex();

	switch (wParam) {
	case VK_UP:
		SetTopIndex(nIndex - 1);
		break;
	case VK_DOWN:
		SetTopIndex(nIndex + 1);
		break;
	}

	return 0L;
}

LONG RegnGantt::OnUserKeyUp(UINT wParam, LONG lParam)
{
	return 0L;
}

/////////////////////////////////////////////////////////////////////////////
// RegnGantt context menus helper functions

void RegnGantt::OnMenuRegnWorkOn()
{
	if (imContextItem > -1)
	{
		CString olAlid = pomViewer->GetLineText(imGroupno,imContextItem);

		RegnDetailWindow *polRegnDW = new RegnDetailWindow(this,(const char *) olAlid,TRUE, TRUE, 
											omDisplayStart, omDisplayEnd);
	}
}


void RegnGantt::GetTimeOfLongestUnresolvedDemand(REGN_BARDATA &rrmContextBar,CTime &ropStartTime,CTime &ropEndTime)
{
/*		// we are interested only in the time for the longest unresolved demand
		ropStartTime = rrmContextBar.EndTime;
		ropEndTime = rrmContextBar.StartTime;
		FLIGHTDATA *prlFlight;
		if ((prlFlight = ogFlightData.GetFlightByUrno(rrmContextBar.FlightUrno)) == NULL)
			return;	// error: no such flight
		CCSPtrArray<DEMANDDATA> olDemands;

		// We will loop this twice (for arrival-departure pair) if it's a turnaround
		long n = (ogFlightData.IsArrival(prlFlight) && ogFlightData.IsTurnaround(prlFlight))? 2: 1;
		for (long lpFlur = prlFlight->Urno, i = 0; i < n; lpFlur = prlFlight->Rkey, i++)
		{
			// quick fix by Brian because Rkey is no longer the URNO of the other half of the rotation !
			if(i == 1)
			{
				lpFlur = 0L;
				FLIGHTDATA *prlRotation = ogFlightData.GetRotationFlight(prlFlight);
				if(prlRotation != NULL)
				{
					lpFlur = prlRotation->Urno;
				}
			}
			ogDataSet.GetDemandsByFlur(olDemands, lpFlur, ogFlightData.GetRegn(prlFlight), PERSONNEL_DEMAND);
			while (olDemands.GetSize() > 0)
			{
				long llNextDemandUrno = olDemands[0].Urno;
				// Check if this demand has no job associated with yet
				CCSPtrArray<JODDATA> olJods;
				ogJodData.GetJodsByDemand(olJods, llNextDemandUrno);
				while (olJods.GetSize() > 0)
				{
					if (ogJobData.GetJobByUrno(olJods[0].Ujob) == NULL)
						olJods.RemoveAt(0);
					else
						break;
				}
				if (olJods.GetSize() == 0)	  // this demand has no job assigned
				{
					DEMANDDATA *prlDemand = ogDemandData.GetDemandByUrno(llNextDemandUrno);
					if (prlDemand != NULL)
					{
						if (prlDemand->Debe < ropStartTime)
							ropStartTime = prlDemand->Debe;
						if (prlDemand->Deen > ropEndTime)
							ropEndTime = prlDemand->Deen;
					}
				}
				olJods.RemoveAll();
				olDemands.RemoveAt(0);
			}
		}
*/
}

void RegnGantt::OnMenuFreeEmployees()
{
	if (bmContextBarSet && rmContextBar.Type == BAR_REGNFLIGHT)
	{
		FLIGHTDATA *prlFlight = ogFlightData.GetFlightByUrno(rmContextBar.FlightUrno);
		if(prlFlight != NULL)
		{
			bool blFirstAttempt = true;
			bool blMultiAssign = true;
			while(blMultiAssign)
			{
				blMultiAssign = false;

				CString olAloc = ALLOCUNITTYPE_REGN;
				CString olAlid = prlFlight->Regn;

				bool blIsThirdParty = (pomViewer->imThirdPartyGroup != -1 && imGroupno == pomViewer->imThirdPartyGroup) ? true : false;

				CCSPtrArray<DEMANDDATA> olDemands;
				if(blIsThirdParty)
				{
					FLIGHTDATA *prlRotation = ogFlightData.GetRotationFlight(prlFlight);
					if(prlRotation != NULL)
					{
						ogDataSet.GetOpenDemandsForRotation(prlFlight, prlRotation, olDemands, TIMENULL, TIMENULL, olAlid, olAloc, false);
					}
					else
					{
						ogDataSet.GetOpenDemandsForFlight(prlFlight, olDemands, TIMENULL, TIMENULL, olAlid, olAloc);
					}
				}
				else
				{
					ogDataSet.GetOpenDemandsForRegn(prlFlight, prlFlight->Regn, olDemands, TIMENULL, TIMENULL);
				}

				int ilNumDemands = olDemands.GetSize();
				if(ilNumDemands <= 0)
				{
					if(blFirstAttempt)
					{
						// no open demands
						MessageBox(GetString(IDS_NOOPENDEMANDS), GetString(IDS_AVAILEMPSCAPTION), MB_ICONINFORMATION);
					}
				}
				else
				{
					int ilNumEmps = 0;
					CAvailableEmpsDlg olAvailableEmpsDlg;
					olAvailableEmpsDlg.SetAllocUnit(olAloc, olAlid);
					olAvailableEmpsDlg.SetCaptionObject(prlFlight->Fnum);
					for(int ilD = 0; ilD < ilNumDemands; ilD++)
					{
						ilNumEmps += olAvailableEmpsDlg.GetAvailableEmployeesForDemand(&olDemands[ilD], true);
						olAvailableEmpsDlg.AddDemand(&olDemands[ilD]);
					}
					if(ilNumEmps <= 0)
					{
						// no employees available for the demands
						MessageBox(GetString(IDS_STRING61266), GetString(IDS_AVAILEMPSCAPTION), MB_ICONEXCLAMATION);
					}
					else
					{
						if(olAvailableEmpsDlg.DoModal() == IDOK && olAvailableEmpsDlg.omSelectedPoolJobUrnos.GetSize() > 0)
						{
							if(!strcmp(olAvailableEmpsDlg.prmSelectedDemand->Aloc,ALLOCUNITTYPE_REGN) && !blIsThirdParty)
							{
								// registration demand
								ogDataSet.CreateJobRegnFromDuty(this,olAvailableEmpsDlg.omSelectedPoolJobUrnos,NULL,NULL,olAvailableEmpsDlg.prmSelectedDemand);
							}
							else
							{
								CCSPtrArray <DEMANDDATA> olDemands;
								olDemands.Add(olAvailableEmpsDlg.prmSelectedDemand);
								ogDataSet.CreateJobFlightFromDuty(this, olAvailableEmpsDlg.omSelectedPoolJobUrnos, prlFlight->Urno, olAloc,olAlid, false, &olDemands);
							}

							blMultiAssign = ogBasicData.bmAvailEmpsMultiAssign;
						}
					}
				}
				blFirstAttempt = false;
			}
		}
	}
}


void RegnGantt::OnMenuFlightConfirm()
{
	if (bmContextBarSet)
	{
		if (rmContextBar.Type == BAR_REGNFLIGHT)
		{
			FLIGHTDATA *prlFlight;
			if ((prlFlight = ogFlightData.GetFlightByUrno(rmContextBar.FlightUrno)) != NULL)
			{
				BOOL blIsChanged = TRUE; 
				CCSPtrArray<JOBDATA> olJobs;
				ogJobData.GetJobsByFlur(olJobs,prlFlight->Urno,FALSE); // don't like JOBFM
				for ( int ilJobNo = 0; ilJobNo < olJobs.GetSize(); ilJobNo++)
				{
					if (DataSet::ConfirmJob(this,&olJobs[ilJobNo]) == TRUE)
						blIsChanged = TRUE;
				}
				if(ogFlightData.IsTurnaround(prlFlight))
				{
					FLIGHTDATA *prlRotationFlight = ogFlightData.GetRotationFlight(prlFlight);
					if (prlRotationFlight != NULL)
					{
						olJobs.RemoveAll();
						ogJobData.GetJobsByFlur(olJobs,prlRotationFlight->Urno,FALSE); // don't like JOBFM
						for ( int ilJobNo = 0; ilJobNo < olJobs.GetSize(); ilJobNo++)
						{
							if (DataSet::ConfirmJob(this,&olJobs[ilJobNo]) == TRUE)
								blIsChanged = TRUE;
						}
					}
				}
				if (blIsChanged)
				{
					ogConflicts.CheckConflicts(*prlFlight);
					ogCCSDdx.DataChanged((void *)this, FLIGHT_CHANGE, (void *)prlFlight);
				}
			}
		}
	}
	bmContextBarSet = FALSE;

}

void RegnGantt::OnMenuFlightWorkOn()
{
   	if (bmContextBarSet)
	{
		if (rmContextBar.Type == BAR_REGNFLIGHT)
		{
			new FlightDetailWindow(this,rmContextBar.FlightUrno,rmContextBar.DepartureUrno,rmContextBar.IsShadowBar,ALLOCUNITTYPE_REGN);
		}
		else
		{
			if (rmContextBar.Type == BAR_REGNSPECIAL)
			{
				JOBDATA *prlJob = ogJobData.GetJobByUrno(rmContextBar.FlightUrno);
				if (prlJob != NULL)
				{
					new StaffDetailWindow(this,prlJob->Jour);
				}
			}
		}
	}

	bmContextBarSet = FALSE;
}

void RegnGantt::OnMenuFlightAcceptConflict(UINT ipId)
{
	if (bmContextBarSet)
	{
		if(ipId == 23)
		{
			// accept all conflicts
			ogConflicts.AcceptFlightConflicts(rmContextBar.FlightUrno,ALLOCUNITTYPE_REGN);
			FLIGHTDATA *prlFlight;
			if ((prlFlight = ogFlightData.GetFlightByUrno(rmContextBar.FlightUrno)) != NULL)
			{
				FLIGHTDATA *prlRotation = ogFlightData.GetRotationFlight(prlFlight);
				if (prlRotation != NULL)
				{
					ogConflicts.AcceptFlightConflicts(prlRotation->Urno,ALLOCUNITTYPE_REGN);
				}
			}
		}
		else
		{
			// accept a single conflict
			CONFLICTDATA *prlConflict;
			if(omAcceptConflictsMap.Lookup((void *) ipId, (void *&) prlConflict))
			{
				FLIGHTDATA *prlFlight;
				if ((prlFlight = ogFlightData.GetFlightByUrno(rmContextBar.FlightUrno)) != NULL)
				{
					ogConflicts.AcceptOneFlightConflict(rmContextBar.FlightUrno, prlConflict->Type,ALLOCUNITTYPE_REGN);
					if(ogConflicts.ConflictIsForRotation(prlConflict->Type))
					{
						FLIGHTDATA *prlRotation = ogFlightData.GetRotationFlight(prlFlight);
						if (prlRotation != NULL)
						{
							ogConflicts.AcceptOneFlightConflict(prlRotation->Urno, prlConflict->Type,ALLOCUNITTYPE_REGN);
						}
					}
				}
				else
				{
					// conflict is possible for a single demand - DemandWithoutFlight
					ogConflicts.AcceptOneDemandConflict(rmContextBar.DemandUrno, prlConflict->Type);
				}
			}
			else if(omAcceptRotationConflictsMap.Lookup((void *) ipId, (void *&) prlConflict))
			{
				FLIGHTDATA *prlRotation, *prlFlight;
				if ((prlFlight = ogFlightData.GetFlightByUrno(rmContextBar.FlightUrno)) != NULL &&
					(prlRotation = ogFlightData.GetRotationFlight(rmContextBar.FlightUrno)) != NULL)
				{
					ogConflicts.AcceptOneFlightConflict(prlRotation->Urno, prlConflict->Type,ALLOCUNITTYPE_REGN);
					if(ogConflicts.ConflictIsForRotation(prlConflict->Type))
					{
						ogConflicts.AcceptOneFlightConflict(prlFlight->Urno, prlConflict->Type,ALLOCUNITTYPE_REGN);
					}
				}
			}
		}
	}
	bmContextBarSet = FALSE;

}


void RegnGantt::OnMenuCheckConflict()
{
	if (bmContextBarSet)
	{
		if (rmContextBar.Type == BAR_REGNFLIGHT)
		{
			FLIGHTDATA *prlFlight;
			if ((prlFlight = ogFlightData.GetFlightByUrno(rmContextBar.FlightUrno)) != NULL)
			{
				ogConflicts.CheckConflicts(*prlFlight,FALSE,FALSE,FALSE);
			}
			if ((prlFlight = ogFlightData.GetFlightByUrno(rmContextBar.DepartureUrno)) != NULL)
			{
				ogConflicts.CheckConflicts(*prlFlight,FALSE,FALSE,FALSE);
			}
		}
	}
}

void RegnGantt::OnMenuDisplayDebugInfo()
{
	if (bmContextBarSet)
	{
		if (rmContextBar.Type == BAR_REGNFLIGHT)
		{
			CString olDebugInfo, olTmp, olNewLine("\n");
			bool blFlightFound = false, blDemandFound = false;
			FLIGHTDATA *prlFlight;
			if ((prlFlight = ogFlightData.GetFlightByUrno(rmContextBar.FlightUrno)) != NULL)
			{
				olTmp.Format("rmContextBar.FlightUrno <%d>:\n",rmContextBar.FlightUrno);
				olDebugInfo += olTmp + ogFlightData.Dump(rmContextBar.FlightUrno) + olNewLine;
				blFlightFound = true;
			}
			else
			{
				olTmp.Format("rmContextBar.FlightUrno <%d> not found.\n",rmContextBar.FlightUrno);
				olDebugInfo += olTmp;
			}
			if ((prlFlight = ogFlightData.GetFlightByUrno(rmContextBar.DepartureUrno)) != NULL)
			{
				olTmp.Format("rmContextBar.DepartureUrno <%d>:\n",rmContextBar.DepartureUrno);
				olDebugInfo += olTmp + ogFlightData.Dump(rmContextBar.DepartureUrno) + olNewLine;
				blFlightFound = true;
			}
			else
			{
				olTmp.Format("rmContextBar.DepartureUrno <%d> not found.\n",rmContextBar.DepartureUrno);
				olDebugInfo += olTmp;
			}
			CCSPtrArray <DEMANDDATA> olDemands;
			if(!pomViewer->GetDemandsForBar(rmContextBar.DemandUrno, olDemands))
			{
				olTmp.Format("No demands found for rmContextBar.DemandUrno <%d>\n",rmContextBar.DemandUrno);
				olDebugInfo += olTmp;
			}
			else
			{
				int ilNumDemands = olDemands.GetSize();
				olTmp.Format("%d demands found for rmContextBar.DemandUrno <%d>:\n",ilNumDemands,rmContextBar.DemandUrno);
				olDebugInfo += olTmp;
				for(int ilDemand = 0; ilDemand < ilNumDemands; ilDemand++)
				{
					olDebugInfo += ogDemandData.Dump(olDemands[ilDemand].Urno) + olNewLine;
				}
			}

			ogBasicData.DebugInfoMsgBox(olDebugInfo);
		}
	}
}


static int CompareJobStartTime(const JOBDATA **e1, const JOBDATA **e2)
{
	return (int)((**e1).Acfr.GetTime() - (**e2).Acfr.GetTime());
}

static int CompareDemandStartTime(const DEMANDDATA **e1, const DEMANDDATA **e2)
{
	return (int)((**e1).Debe.GetTime() - (**e2).Debe.GetTime());
}

void RegnGantt::OnMenuCreateDemand()
{
	if (prmActualBkBar)
	{
		if(imGroupno != -1 && imGroupno == pomViewer->imThirdPartyGroup)
		{
			new CreateDemand(prmActualBkBar, this, true);
		}
		else
		{
			new CreateDemand(prmActualBkBar, this);
		}
	}
}




/////////////////////////////////////////////////////////////////////////////
// RegnGantt message handlers helper functions

// Update every necessary GUI elements for each mouse/timer event
/*void RegnGantt::UpdateGanttChart(CPoint point, BOOL bpIsControlKeyDown)
{
    int itemID = GetItemFromPoint(point);

    // Check if we have to change the highlight line in VerticalScale
	CPoint ptScreen = point;
	ClientToScreen(&ptScreen);
	if (WindowFromPoint(ptScreen) == this)	// moving inside GanttChart?
    {
        if (!bmIsMouseInWindow) // first time?
        {
            bmIsMouseInWindow = TRUE;
            pogButtonList->RegisterTimer(this);    // install the timer
        }

        if (itemID != imHighlightLine)  // move to new bar?
            RepaintVerticalScale(itemID, FALSE);
        if (IsBetween(point.x, 0, imVerticalScaleWidth-1))
		{
			;
            //UpdateBarStatus(itemID, -1, -1);    // moving on vertical scale, not a bar
		}
        else
        {
            int ilBarno;
			int ilBkBarno;
            if (!bpIsControlKeyDown)
			{
                ilBarno = GetBarnoFromPoint(itemID, point);
				ilBkBarno = GetBkBarnoFromPoint(itemID, point);
			}
            else
			{
                ilBarno = GetBarnoFromPoint(itemID, point, imBorderPreLeft, imBorderPreRight);
				ilBkBarno = GetBkBarnoFromPoint(itemID, point, imBorderPreLeft, imBorderPreRight);
			}
            UpdateBarStatus(itemID, ilBarno, ilBkBarno);
        }
    }
    else if (bmIsMouseInWindow) // moving out of GanttChart? (first time only)
    {
        bmIsMouseInWindow = FALSE;
        RepaintVerticalScale(imHighlightLine, FALSE);
        imHighlightLine = -1;       // no more selected line
        UpdateBarStatus(-1, -1, -1);    // no more status if mouse is outside of GanttChart
    }

    // Check if we have to change the icon (when user want to move/resize a bar)
	if (GetCapture() != NULL)
		;	// don't set cursor on drag-and-drop
    else if (bmIsControlKeyDown && !bpIsControlKeyDown)              // no more Ctrl-key?
        SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
    else if (!bmIsMouseInWindow || !bpIsControlKeyDown || imCurrentBar == -1)
        ;   // do noting
    else if (HitTest(itemID, imCurrentBar, point) == HTCAPTION) // body?
        SetCursor(AfxGetApp()->LoadStandardCursor(IDC_SIZEALL));
//
//    else                                                        // left/right border?
//        SetCursor(AfxGetApp()->LoadStandardCursor(IDC_SIZEWE));
//

    // Update control key state
    bmIsControlKeyDown = bpIsControlKeyDown;
}
*/
void RegnGantt::UpdateGanttChart(CPoint point, BOOL bpIsControlKeyDown)
{
    int itemID = GetItemFromPoint(point);

    // Check if we have to change the highlight line in VerticalScale
	CPoint ptScreen = point;
	ClientToScreen(&ptScreen);
	if (WindowFromPoint(ptScreen) == this)	// moving inside GanttChart?
    {
        if (!bmIsMouseInWindow) // first time?
        {
            bmIsMouseInWindow = TRUE;
            pogButtonList->RegisterTimer(this);    // install the timer
        }

        if (itemID != imHighlightLine)  // move to new bar?
            RepaintVerticalScale(itemID, FALSE);
        if (IsBetween(point.x, 0, imVerticalScaleWidth-1))
            UpdateBarStatus(itemID, -1, -1);    // moving on vertical scale, not a bar
        else
        {
            int ilBarno, ilBkBarno;
            if (!bpIsControlKeyDown)
			{
                ilBarno = GetBarnoFromPoint(itemID, point);
				ilBkBarno = GetBkBarnoFromPoint(itemID, point);
			}
            else
			{
                ilBarno = GetBarnoFromPoint(itemID, point, imBorderPreLeft, imBorderPreRight);
				ilBkBarno = GetBkBarnoFromPoint(itemID, point, imBorderPreLeft, imBorderPreRight);
			}
            UpdateBarStatus(itemID, ilBarno, ilBkBarno);
        }
    }
    else if (bmIsMouseInWindow) // moving out of GanttChart? (first time only)
    {
        bmIsMouseInWindow = FALSE;
        RepaintVerticalScale(imHighlightLine, FALSE);
        imHighlightLine = -1;       // no more selected line
        UpdateBarStatus(-1, -1, -1);    // no more status if mouse is outside of GanttChart
    }

    // Check if we have to change the icon (when user want to move/resize a bar)
	if (GetCapture() != NULL)
		;	// don't set cursor on drag-and-drop
    else if (bmIsControlKeyDown && !bpIsControlKeyDown)              // no more Ctrl-key?
        SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
    else if (!bmIsMouseInWindow || !bpIsControlKeyDown || imCurrentBar == -1)
        ;   // do noting
    else if (HitTest(itemID, imCurrentBar, point) == HTCAPTION) // body?
        SetCursor(AfxGetApp()->LoadStandardCursor(IDC_SIZEALL));	//hag20000613
/*
    else                                                        // left/right border?
        SetCursor(AfxGetApp()->LoadStandardCursor(IDC_SIZEWE));
*/

    // Update control key state
    bmIsControlKeyDown = bpIsControlKeyDown;
}

// Update the frame window's status bar
void RegnGantt::UpdateBarStatus(int ipLineno, int ipBarno, int ipBkBarno)
{
    CString s;

    if (ipLineno == imHighlightLine 
		&& ipBarno == imCurrentBar 
		&& ipBkBarno == imCurrentBkBar)		// user still be on the old bar?
		return;

    if (ipLineno == -1) // there is no more bar status?
        s = "";
    else if (ipBarno == -1) // user move outside the bar?
		if (ipBkBarno == -1) 
			s = "";
		else
			s = pomViewer->GetBkBar(imGroupno, ipLineno, ipBkBarno)->StatusText;
	else
        s = pomViewer->GetBar(imGroupno, ipLineno, ipBarno)->StatusText;

	// display status bar
    if (pomStatusBar != NULL)
        pomStatusBar->SetPaneText(0, (LPCSTR)s);

	// display top scale indicator
	if (pomTimeScale != NULL)
	{
		if (ipLineno == -1 ||	// mouse moved out of every lines?
			ipBarno == -1 && imCurrentBar != -1)	// mouse just leave a bar?
			pomTimeScale->RemoveAllTopScaleIndicator();
		else if (ipBarno != imCurrentBar)
		{
			pomTimeScale->RemoveAllTopScaleIndicator();
			int ilIndicatorCount = pomViewer->GetIndicatorCount(imGroupno,
				ipLineno, ipBarno);
			for (int ilIndicatorno = 0; ilIndicatorno < ilIndicatorCount; ilIndicatorno++)
			{
				REGN_INDICATORDATA *prlIndicator = pomViewer->GetIndicator(imGroupno,
					ipLineno, ipBarno, ilIndicatorno);
				pomTimeScale->AddTopScaleIndicator(prlIndicator->StartTime,
					prlIndicator->EndTime, prlIndicator->Color);
			}
			pomTimeScale->DisplayTopScaleIndicator();
		}
	}

    // remember the bar and update the status bar (if exist)
    imCurrentBar = ipBarno;
	imCurrentBkBar = ipBkBarno;
}

// Start moving/resizing
BOOL RegnGantt::BeginMovingOrResizing(CPoint point)
{
    int itemID = GetItemFromPoint(point);
    int ilBarno = GetBarnoFromPoint(itemID, point, imBorderPreLeft, imBorderPreRight);
	int ilBkBarno = GetBkBarnoFromPoint(itemID, point, imBorderPreLeft, imBorderPreRight);
    UpdateBarStatus(itemID, ilBarno, ilBkBarno);

	umResizeMode = HTNOWHERE;
    if (ilBarno != -1)
        umResizeMode = HitTest(itemID, ilBarno, point);

    if (umResizeMode != HTNOWHERE)
    {
        SetCapture();
        if (umResizeMode == HTCAPTION)
            SetCursor(AfxGetApp()->LoadStandardCursor(IDC_SIZEALL));
        else
            SetCursor(AfxGetApp()->LoadStandardCursor(IDC_SIZEWE));

        // Calculate size of current bar
        REGN_BARDATA *prlBar = pomViewer->GetBar(imGroupno, itemID, ilBarno);
        CRect rcItem;
        GetItemRect(itemID, &rcItem);
        int left = (int)GetX(prlBar->StartTime);
        int right = (int)GetX(prlBar->EndTime);
        int top = rcItem.top + imGutterHeight + (prlBar->OverlapLevel * imOverlapHeight);
        int bottom = top + imBarHeight;

        omPointResize = point;
        omRectResize = CRect(left, top, right, bottom);
        CClientDC dc(this);
        DrawFocusRect(&dc, &omRectResize);	// first time focus rectangle
    }

    return (umResizeMode != HTNOWHERE);
}

// Update the resizing/moving rectangle
BOOL RegnGantt::OnMovingOrResizing(CPoint point, BOOL bpIsLButtonDown)
{
    CClientDC dc(this);

    if (bpIsLButtonDown)    // still resizing/moving?
    {
        DrawFocusRect(&dc, &omRectResize);	// delete previous focus rectangle
        switch (umResizeMode)
        {
        case HTCAPTION: // moving mode
            omRectResize.OffsetRect(point.x - omPointResize.x, 0);
            break;
        case HTLEFT:    // resize left border
            omRectResize.left = min(point.x, omRectResize.right);
            break;
        case HTRIGHT:   // resize right border
            omRectResize.right = max(point.x, omRectResize.left);
            break;
        }
        omPointResize = point;
    }
    else
    {
        umResizeMode = HTNOWHERE;   // no more moving/resizing
        ReleaseCapture();
        SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
    }

    DrawFocusRect(&dc, &omRectResize);
    return (umResizeMode != HTNOWHERE);
}

// return the item ID of the list box based on the given "point"
int RegnGantt::GetItemFromPoint(CPoint point)
{
    CRect rcItem;
    for (int itemID = GetTopIndex(); itemID < GetCount(); itemID++)
    {
        GetItemRect(itemID, &rcItem);
        if (rcItem.PtInRect(point))
            return itemID;
    }
    return -1;
}

// Return the bar number of the list box based on the given point
int RegnGantt::GetBarnoFromPoint(int ipLineno, CPoint point, int ipPreLeft, int ipPreRight)
{
    if (ipLineno == -1) // this checking was added after some bugs found
        return -1;

    // Calculate possible levels for vertical position
    CRect rcItem;
    GetItemRect(ipLineno, &rcItem);
    point.y -= rcItem.top + imGutterHeight;
    int level1 = (point.y < imBarHeight)? 0: ((point.y - imBarHeight) / imOverlapHeight) + 1;
    int level2 = (point.y < 0)? -1: point.y / imOverlapHeight;

    // Calculate possible time for horizontal position
    CTime time1 = GetCTime(point.x-ipPreLeft);
    CTime time2 = GetCTime(point.x+ipPreRight);

    return pomViewer->GetBarnoFromTime(imGroupno, ipLineno, time1, time2, level1, level2);
}


// Return the background bar number of the list box based on the given point
int RegnGantt::GetBkBarnoFromPoint(int ipLineno, CPoint point, int ipPreLeft, int ipPreRight)
{
    if (ipLineno == -1) // this checking was added after some bugs found
        return -1;

    // Calculate possible levels for vertical position
    CRect rcItem;
    GetItemRect(ipLineno, &rcItem);
    point.y -= rcItem.top + imGutterHeight;
    //int level1 = (point.y < imBarHeight)? 0: ((point.y - imBarHeight) / imOverlapHeight) + 1;
    //int level2 = (point.y < 0)? -1: point.y / imOverlapHeight;

    // Calculate possible time for horizontal position
    CTime time1 = GetCTime(point.x-ipPreLeft);
    CTime time2 = GetCTime(point.x+ipPreRight);

    return pomViewer->GetBkBarnoFromTime(imGroupno, ipLineno, time1, time2);
}

// Return the hit test area code (determine the location of the cursor).
// Possible codes are HTLEFT, HTRIGHT, HTNOWHERE (out-of-bar), HTCAPTION (bar body)
// 
UINT RegnGantt::HitTest(int ipLineno, int ipBarno, CPoint point)
{
	return HTNOWHERE;

    if (ipLineno == -1 || ipBarno == -1)    // this checking was added after some bugs found
        return HTNOWHERE;

    // Calculate possible time for horizontal position
    CTime time1 = GetCTime(point.x-imBorderPreLeft);
    CTime time2 = GetCTime(point.x+imBorderPreRight);

    // Check against each possible case of hit test areas.
    // Since most people like to drag things to the right, we check right border before left.
    // Be careful, if the bar is very small, the user may not be able to get HTCAPTION or HTLEFT.
    //
    REGN_BARDATA *prlBar = pomViewer->GetBar(imGroupno, ipLineno, ipBarno);
    if (IsBetween(prlBar->EndTime, time1, time2))
        return HTRIGHT;
    else if (IsBetween(prlBar->StartTime, time1, time2))
        return HTLEFT;
    else if (IsOverlapped(time1, time2, prlBar->StartTime, prlBar->EndTime))
        return HTCAPTION;

    return HTNOWHERE;
}


/////////////////////////////////////////////////////////////////////////////
// RegnGantt -- drag-and-drop functionalities
//
// The user may drag from:
//	- Vertical Scale for doing a Regn job assignment action (create flight jobs).
//	- the Flight Bar for create normal flight jobs.
//
// and the user will have opportunities to:
//	-- drop from StaffTable (single or multiple staff shift) onto the vertical scale.
//		We will ask the user for the period of time and create many flight jobs
//		for this employees for demands in that Regn.
//	-- drop from StaffTable (single or multiple staff shift) onto FlightBar.
//		We will create normal flight jobs for those staffs.
//	-- drop from DutyBar onto the vertical scale (Regn).
//		We will ask the user for the period of time and create many flight jobs
//		for this employees for demands in that Regn.
//	-- drop from DutyBar onto FlightBar.
//		We will create a normal flight job for that staff.
//
void RegnGantt::DragRegnBegin(int ipLineno)
{
	m_DragDropSource.CreateDWordData(DIT_REGN, 1);
	m_DragDropSource.AddString(pomViewer->GetLineText(imGroupno, ipLineno));
	m_DragDropSource.BeginDrag();

}

void RegnGantt::DragFlightBarBegin(int ipLineno, int ipBarno)
{
	REGN_BARDATA *prlBar = pomViewer->GetBar(imGroupno, ipLineno, ipBarno);
	if(prlBar != NULL)
	{
		FLIGHTDATA *prlFlight = ogFlightData.GetFlightByUrno(prlBar->FlightUrno);
		if (prlFlight != NULL)
		{
			m_DragDropSource.CreateDWordData(DIT_FLIGHTBAR, 4);
			m_DragDropSource.AddDWord(prlBar->FlightUrno);
			m_DragDropSource.AddDWord(0L); // no rotation URNO
			m_DragDropSource.AddString(ALLOCUNITTYPE_REGN); // Aloc
			m_DragDropSource.AddString(""); // Alid
			m_DragDropSource.BeginDrag();
		}
		else if(prlBar->DemandUrno != 0L)
		{
			// demand without flight
			m_DragDropSource.CreateDWordData(DIT_DEMANDBAR, 1);
			m_DragDropSource.AddDWord(prlBar->DemandUrno);
			m_DragDropSource.BeginDrag();
		}
	}
}

LONG RegnGantt::OnDragOver(UINT i, LONG l)
{
	CPoint point;
    ::GetCursorPos(&point);
    ScreenToClient(&point);    
    UpdateGanttChart(point, FALSE);

    int ilClass = m_DragDropTarget.GetDataClass(); 
	if ((ilClass != DIT_DUTYBAR) && (ilClass != DIT_FMDETAIL))
		return -1L;	// cannot interpret this object

	JOBDATA *prlPoolJob = ogJobData.GetJobByUrno(m_DragDropTarget.GetDataDWord(0));
	if (prlPoolJob == NULL)
		return -1L;


	int itemID;

	if ((itemID = GetItemFromPoint(point)) == -1)
		return -1L;	// cannot find a line of this diagram

	CString olRegn = pomViewer->GetLineText(imGroupno, itemID);
	
	if (point.x < imVerticalScaleWidth - 2)
	{/*
		// check if the pool/emp can be dropped on this gate
		if(!ogBasicData.IsInPool(ALLOCUNITTYPE_REGN,olRegn,prlPoolJob->Alid))
			return -1L;
		else
			return 0L;	// drop on Gate
	*/}

	int ilBarno;
	if ((ilBarno = GetBarnoFromPoint(itemID, point)) == -1)
		return -1L;	// not on the bar (on the background of chart)

	REGN_BARDATA *prlBar = pomViewer->GetBar(imGroupno, itemID, ilBarno);	
	if (prlBar == NULL)
		return -1L;
	if (prlBar->Type == BAR_REGNSPECIAL)
		return -1L;
	if (prlBar->IsShadowBar)
		return -1L; // no drop on shadow bar
	if (prlBar->NoDropOnThisBar)
		return -1L; // no drop after gate change

//	int ilBkBarno = GetBkBarnoFromPoint(itemID, point);
//	if(ilBkBarno == -1)
//		return -1L;

	if(!ogBasicData.IsInPool(ALLOCUNITTYPE_REGN,olRegn,prlPoolJob->Alid))
		return -1L;

	return 0L;	// the objects are welcome

}

LONG RegnGantt::OnDrop(UINT i, LONG l)
{
    int ilClass = m_DragDropTarget.GetDataClass(); 
	if ((ilClass != DIT_DUTYBAR) && (ilClass != DIT_FMDETAIL))
		return -1L;	// cannot interpret this object

	CPoint point;
    ::GetCursorPos(&point);
    ScreenToClient(&point);    

	int itemID, ilBarno;
	if ((itemID = GetItemFromPoint(point)) == -1)
		return -1L;	// cannot find a line of this diagram
	if (point.x < imVerticalScaleWidth - 2)
	{
		/*
		if (ilClass != DIT_DUTYBAR)
			return -1L;	// cannot interpret this object

		// drop on Regn
		REGN_LINEDATA *prlLine = pomViewer->GetLine(imGroupno, itemID);
		if (prlLine != NULL)
		{
			CString olRegn = prlLine->Text;
			long llDutyJobUrno = m_DragDropTarget.GetDataDWord(0);
			JOBDATA *prlJob = ogJobData.GetJobByUrno(llDutyJobUrno);

			CWnd *polWnd = GetParent()->GetParent()->GetParent();
			EMPDATA *prlEmp = ogEmpData.GetEmpByPeno(prlJob->Peno);
			if (prlEmp != NULL) 
			{
				CString olEmpNam = ogEmpData.GetEmpName(prlEmp);
				CTime olStartTime = ogDataSet.GetTimeForNextJob(llDutyJobUrno);
				DRegnJob olRegnJobDialog(polWnd,olRegn,olEmpNam,olStartTime);
				int ilDuration;
				bgModal++;
				if (olRegnJobDialog.DoModal(olStartTime,ilDuration) == IDOK)
				{
					olStartTime = olRegnJobDialog.m_Hour;
					ilDuration = olRegnJobDialog.m_Duration;
					if (prlLine->IsStairCaseRegn)
					{
						ogDataSet.CreateJobStairCaseRegnFromDuty(this,olRegn,llDutyJobUrno,olStartTime,ilDuration);
					}
					else
					{ // create normal flight jobs for all flights in timeframe
						CDWordArray olDutyUrnos;
						olDutyUrnos.Add(llDutyJobUrno);
						ogDataSet.CreateRegnJob(this,olDutyUrnos,olStartTime,olStartTime+(ilDuration*60),olRegn);
					}
				}
				bgModal--;
			}
			else
			{
//				MessageBox("Internal Error: Pool-Einsatz ohne PK-Nummer!");
				MessageBox(GetString(IDS_STRING61267));
			}
		}
		*/
	}
	else
	{
		JOBDATA *prlPoolJob = ogJobData.GetJobByUrno(m_DragDropTarget.GetDataDWord(0));
		if (prlPoolJob == NULL)
			return -1L;

		if ((ilBarno = GetBarnoFromPoint(itemID, point)) == -1)
			return -1L;	// not on the bar (on the background of chart)
		CString s = pomViewer->GetLineText(imGroupno, itemID);
		if(!ogBasicData.IsInPool(ALLOCUNITTYPE_REGN,s,prlPoolJob->Alid))
			return -1L;
		REGN_BARDATA *prlBar = pomViewer->GetBar(imGroupno, itemID, ilBarno);	
		if (prlBar == NULL)
			return -1L;
		if (prlBar->IsShadowBar)
			return -1L; // no drop on shadow bar
		if (prlBar->Type == BAR_REGNSPECIAL)
			return -1L;

		long llDutyJobUrno = m_DragDropTarget.GetDataDWord(0);

		if (ilClass == DIT_DUTYBAR)
		{
			CDWordArray olDutyUrnos;
			//olDutyUrnos.Add(llDutyJobUrno);
			for (int ilIndex = 0; ilIndex < m_DragDropTarget.GetDataCount(); ilIndex++)
				olDutyUrnos.Add(m_DragDropTarget.GetDataDWord(ilIndex));

			FLIGHTDATA *prlFlight = ogFlightData.GetFlightByUrno(prlBar->FlightUrno);
			if(prlFlight != NULL)
			{
				if(ogFlightData.IsThirdPartyFlight(prlFlight))
				{
					ogDataSet.CreateJobFlightFromDuty(this, olDutyUrnos, prlFlight->Urno, ALLOCUNITTYPE_REGN,prlFlight->Regn);
				}
				else
				{
					ogDataSet.CreateJobRegnFromDuty(this,olDutyUrnos,prlFlight);
				}
			}
			else
			{
				// demand without flight so assign directly to the demand
				ogDataSet.CreateJobRegnFromDuty(this,olDutyUrnos,NULL,NULL,ogDemandData.GetDemandByUrno(prlBar->DemandUrno));
			}
		}
		else
		{
			ogDataSet.CreateSpecialFlightManagerJob(this,llDutyJobUrno,prlBar->FlightUrno);
		}
	}

    return 0L;
}


void RegnGantt::DrawDottedLines(CDC *pDC, int itemID, const CRect &rcItem)
{
    CBitmap Bitmap;
	CTime olTime, olTmpTime;

	// select dotted pen
	CPen SepPen(PS_DOT, 1, BLACK);
	COLORREF olOldBkColor = pDC->SetBkColor(SILVER);
	CPen *pOldPen = pDC->SelectObject(&SepPen);
	
	// dotted horizontal lines
	CRect olRect(rcItem);
	int ilHeight = 0;
	CRect olTmpRect = olRect;
	{
		olRect.bottom = olRect.top + GetLineHeight(itemID);
        pDC->MoveTo(olRect.left + imVerticalScaleWidth + imVerticalScaleIndent, olRect.bottom - 1);
        pDC->LineTo(olRect.right, olRect.bottom - 1);
		olRect.top += GetLineHeight(itemID);
	}    
                     
	// vertical lines
//	int ilX;
//	olRect = olTmpRect;
//	CPen BlackPen(PS_DOT, 1, GRAY);
//	pDC->SetBkColor(GRAY);
//	pDC->SelectObject(&BlackPen);
//	olTime = omDisplayStart;
//
//	// calculate correct X-coordinates for first line
//	olTime += CTimeSpan(0, 0, 30, 0);
//	ilX = pomTimeScale->GetXFromTime(olTime) + imVerticalScaleWidth + imVerticalScaleIndent;
//
//	// draw lines
//	static int Offset = 0;
//	for(int i = 0; i < 10; i++)
//	{
//		olTime += CTimeSpan(0, 1, 0, 0);
//		//if (i == 5)
//		{
//			ilX = pomTimeScale->GetXFromTime(olTime) + imVerticalScaleWidth + imVerticalScaleIndent;
//			pDC->MoveTo(ilX, olRect.top);
//			pDC->LineTo(ilX, olRect.bottom);
//		}
//	}

	pDC->SelectObject(pOldPen);	
	pDC->SetBkColor(olOldBkColor);
}
