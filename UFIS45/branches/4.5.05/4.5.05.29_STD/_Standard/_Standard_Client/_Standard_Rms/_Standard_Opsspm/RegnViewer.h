// RegnViewer.h : header file
//

#ifndef _REGNVIEWER_H_
#define _REGNVIEWER_H_

#include <cviewer.h>
#include <ccsprint.h>
#include <CedaShiftData.h>
#include <CedaEmpData.h>

struct REGN_INDICATORDATA
{
	long DemandUrno;
	long JobUrno;
	// standard data for indicators in general GanttBar
	CTime StartTime;
	CTime EndTime;
	COLORREF Color;
	BOOL IsArrival;
};
struct REGN_BARDATA 
{
	int  Type;
	long FlightUrno;		// from FLTFRA.URNO
	long DepartureUrno;
	long DemandUrno;		// for demand bars that don't currently have a flight
	CString StatusText;		// long text used to be displayed at the status bar

	// standard data for bars in general GanttLine
	CString Text;
	CTime StartTime;
	CTime EndTime;
	CTimeSpan Delay;
	int IsDelayed;
	int FrameType;
	int MarkerType;
	CBrush *MarkerBrush;
	BOOL IsShadowBar;
	BOOL IsTmo;
	BOOL NoDropOnThisBar;
	int OverlapLevel;       // zero for the top-most level, each level lowered by 4 pixels
	CCSPtrArray<REGN_INDICATORDATA> Indicators;
};

struct REGN_BKBARDATA
{
	long FlightUrno;
	long FlightRotationUrno;
	CString TextArr;
	CString TextDep;
	CString StatusText;
	CTime StartTime;
	CTime EndTime;
	CBrush *MarkerBrush;	// background bar will be only displayed in FRAMENONE, MARKFULL
};
struct REGN_LINEDATA 
{
	// standard data for lines in general Diagram's chart
    CString Text;
    int MaxOverlapLevel;    // maximum number of overlapped level of bars in this line
	BOOL ApuInOp;
	BOOL IsStairCaseRegn;
	long ThirdPartyFlightUrno;
    CCSPtrArray<REGN_BARDATA> Bars;		// in order of painting (leftmost is the bottommost)
	CCSPtrArray<REGN_BKBARDATA> BkBars;	// background bar
    CCSPtrArray<int> TimeOrder;			// maintain overlapped bars, see below
};
struct REGN_MANAGERDATA
{
	long JobUrno;			// from JOBCKI.URNO (of the job for FM/AM)
	// these data are for displaying the TopScaleText for each group in RegnDiagram
	CString ShiftStid;
	CString ShiftTmid;
	CString EmpLnam;
};
struct REGN_GROUPDATA 
{
	CString RegnAreaId;			// Regn area's ID associated with this group
    CCSPtrArray<REGN_MANAGERDATA> Managers;
	// standard data for groups in general Diagram
    CString Text;
    CCSPtrArray<REGN_LINEDATA> Lines;
};

struct REGN_SELECTION
{
	int imGroupno;
	int imLineno;
	int imBarno;

	REGN_SELECTION(void)
	{
		imGroupno = -1;
		imLineno  = -1;
		imBarno	  = -1;
	}
};

enum enumRegnBarType {BAR_REGNFLIGHT, BAR_REGNSPECIAL};

// Attention:
// Element "TimeOrder" in the struct "REGN_LINEDATA".
//
// This array will maintain bars sorted by StartTime of each bar.
// You can see it the same way you see an array of index in the array REGN_BARDATA[].
// For example, if there are five bars in the first overlapped group, and their
// overlap level are [3, 0, 4, 1, 2]. So the indexes to the REGN_BARDATA[] sorted by
// the beginning of time should be [1, 3, 4, 0, 2]. You can see that the indexes
// in these two arrays are cross-linked together. Please notice that we define
// the overlap level to be the same number as the order of the bar when sorted
// by StartTime.
//
// However, I keep the relative offset in this array, not the absolute offset.
// The relative offset helps us separate each group of bar more efficiently.
// For example, using the same previous example, the relative offset of index
// to the array REGN_BARDATA[] kept in this array would be [+1, +2, +3, -3, -2].
//
// For a given "i", the bar in the same overlap group will always have the
// same value of "i - Bars[i + TimeOrder[i]].OverlapLevel". This value will be the
// index of the first member in that group.
//
// Notes: This bar order array should be implemented with CCSArray <int>.


/////////////////////////////////////////////////////////////////////////////
// RegnDiagramViewer window

class RegnGantt;

class RegnDiagramViewer: public CViewer
{
	friend RegnGantt;

// Constructions
public:
    RegnDiagramViewer();
    ~RegnDiagramViewer();

	void Attach(CWnd *popAttachWnd);
	void ChangeViewTo(const char *pcpViewName,
		BOOL bpIncludeArrivalFlights, BOOL bpIncludeDepartureFlights,
		CTime opStartTime, CTime opEndTime);
	void UpdateManagers(CTime opStartTime, CTime opEndTime);

	void UpdateThirdPartyGroup();
	int imThirdPartyGroup;
	BOOL IsPassFilter(const char *pcpRegnArea);

	CMapPtrToPtr omBarMap; // map of demands used to make up a bar
	void AddToBarMap(long lpBarUrno, long lpDemandUrno);
	bool BarContainsDemand(long lpBarUrno, long lpDemandUrno);
	bool GetDemandsForBar(long lpBarUrno, CCSPtrArray <DEMANDDATA> &ropDemands);
	void DeleteDemandFromBarMap(long lpBarUrno, long lpDemandUrno);
	void DeleteBarFromBarMap(long lpBarUrno);

// Internal data processing routines
private:
	static void RegnDiagramCf(void *popInstance, int ipDDXType,void *vpDataPointer, CString &ropInstanceName);
	void PrepareFilter();
	BOOL IsPassFilterTemplate(const char *pcpTemplate);
	int CompareGroup(REGN_GROUPDATA *prpGroup1, REGN_GROUPDATA *prpGroup2);
	int CompareLine(REGN_LINEDATA *prpLine1, REGN_LINEDATA *prpLine2);
	int CompareManager(REGN_MANAGERDATA *prpManager1, REGN_MANAGERDATA *prpManager2);

	void MakeGroupsAndLines();
	void MakeManagers();
	void MakeBars();
	void MakeBarsByLine(int ipGroupno, int ipLineno);
	void MakeBkBars();
	void MakeManager(int ipGroupno, JOBDATA *prpJob);
	//void MakeBar(int ipGroupno, int ipLineno, FLIGHTDATA *prpFlight);
	bool MakeBkBar(int ipGroupno, int ipLineno, FLIGHTDATA *prpFlight);
	void MakeBarWithoutDemand(int ipGroupno, int ipLineno, FLIGHTDATA *prpFlight);
	void MakeBar(int ilGroupno, int ilLineno, DEMANDDATA *prpDemand);
	//void MakeBar(int ilGroupno, int ilLineno, CCSPtrArray<DEMANDDATA> &ropDemands);
	void MakeSpecialBar(int ipGroupno, int ipLineno, JOBDATA *prpJob);
	int CreateThirdPartyLine(FLIGHTDATA *prpFlight);
	void DeleteThirdPartyFlight(int ipLine);
	int AddThirdPartyFlight(FLIGHTDATA *prpFlight);
	void UpdateThirdPartyFlight(FLIGHTDATA *prpFlight, int ipLine);
	void ProcessThirdPartyFlightChange(FLIGHTDATA *prpFlight);
	int GetLineFlightInThirdPartyList(long lpFlightUrno);
	bool IsPassThirdPartyFilter(FLIGHTDATA *prpFlight);
	bool RotationAlreadyDisplayed(FLIGHTDATA *prpFlight);
	/*
	void MakeIndicators(int ipGroupno, int ipLineno, int ipBarno, 
		CCSPtrArray<DEMANDDATA> &ropArrivalDemands,
		CCSPtrArray<DEMANDDATA> &ropDepartureDemands,
		FLIGHTDATA *prpArrivalFlight,FLIGHTDATA *prpDepartureFlight);
	*/
	void MakeIndicators(int ipGroupno, int ipLineno, int ipBarno, CCSPtrArray<DEMANDDATA> &ropDemands);
	int GetDemands(long lpUrno, char *pcpAlid, CCSPtrArray<DEMANDDATA> &ropDemands,
		CTime &rolStartTime, CTime &rolEndTime);
	int GetDemandsByRegn(CCSPtrArray<DEMANDDATA> &ropDemands, const char*pcpRegn);
	int GetTemplateNameByDemand(DEMANDDATA *pDemand, CString &TplName);

	CString ArrivalFlightStatus(FLIGHTDATA *prpFlight, FLIGHTDATA *prpRotationFlight);
	CString DepartureFlightStatus(FLIGHTDATA *prpFlight, FLIGHTDATA *prpRotationFlight);
	CString TurnAroundFlightStatus(FLIGHTDATA *prpFlight, FLIGHTDATA *prpRotationFlight);

	BOOL FindGroup(const char *pcpRegnAreaId, int &ripGroupno);
	BOOL FindGroupAndLine(const char *pcpRegnId, int &ripGroupno, int &ripLineno);
	BOOL FindManager(long lpUrno, int &ripGroupno, int &ripManagerno);
	BOOL FindFlightBar(long lpUrno, int &ripGroupno, int &ripLineno, int &ripBarno);

// Operations
public:
    int GetGroupCount();
    REGN_GROUPDATA *GetGroup(int ipGroupno);
    CString GetGroupText(int ipGroupno);
    CString GetGroupTopScaleText(int ipGroupno);
	int GetManagerCount(int ipGroupno);
    REGN_MANAGERDATA *GetManager(int ipGroupno, int ipManagerno);
    int GetLineCount(int ipGroupno);
    REGN_LINEDATA *GetLine(int ipGroupno, int ipLineno);
    CString GetLineText(int ipGroupno, int ipLineno);
    int GetMaxOverlapLevel(int ipGroupno, int ipLineno);
    int GetVisualMaxOverlapLevel(int ipGroupno, int ipLineno);
    int GetBarCount(int ipGroupno, int ipLineno);
    int GetBkBarCount(int ipGroupno, int ipLineno);
    REGN_BKBARDATA *GetBkBar(int ipGroupno, int ipLineno, int ipBkBarno);
    CString GetBkBarTextArr(int ipGroupno, int ipLineno, int ipBkBarno);
	CString GetBkBarTextDep(int ipGroupno, int ipLineno, int ipBkBarno);
    REGN_BARDATA *GetBar(int ipGroupno, int ipLineno, int ipBarno);
    CString GetBarText(int ipGroupno, int ipLineno, int ipBarno);
	int GetIndicatorCount(int ipGroupno, int ipLineno, int ipBarno);
	REGN_INDICATORDATA *GetIndicator(int ipGroupno, int ipLineno, int ipBarno,
		int ipIndicatorno);
	CString GetTmo(int ipGroupno, int ipLineno);
	CString GetApuInOp(int ipGroupno, int ipLineno);
	int GetGroupColorIndex(int ipGroupno,CTime opStart,CTime opEnd);
	void AllowUpdates(BOOL bpNoUpdatesNow);
	BOOL bmNoUpdatesNow;
    void DeleteAll();
    int CreateGroup(REGN_GROUPDATA *prpGroup);
    void DeleteGroup(int ipGroupno);
	CCSReturnCode CreateManagersForGroup(int ipGroupNo);
	int CreateManager(int ipGroupno, REGN_MANAGERDATA *prpManager);
	void DeleteManager(int ipGroupno, int ipManagerno);
    int CreateLine(int ipGroupno, REGN_LINEDATA *prpLine);
    void DeleteLine(int ipGroupno, int ipLineno);
    int CreateBkBar(int ipGroupno, int ipLineno, REGN_BKBARDATA *prpBkBar);
    void DeleteBkBar(int ipGroupno, int ipLineno, int ipBkBarno);
    int CreateBar(int ipGroupno, int ipLineno, REGN_BARDATA *prpBar, BOOL bpFrontBar = TRUE);
    void DeleteBar(int ipGroupno, int ipLineno, int ipBarno);
    int CreateIndicator(int ipGroupno, int ipLineno, int ipBarno,
    	REGN_INDICATORDATA *prpIndicator);
	void DeleteIndicator(int ipGroupno, int ipLineno, int ipBarno, int ipIndicator);

    int GetBarnoFromTime(int ipGroupno, int ipLineno, CTime opTime1, CTime opTime2,
        int ipOverlapLevel1, int ipOverlapLevel2);
    int GetBkBarnoFromTime(int ipGroupno, int ipLineno, CTime opTime1, CTime opTime2);

	void DeleteBarsForRegn(CString opRegn);

	CWnd *pomAttachWnd;

	CMapPtrToPtr omThirdPartyFlights;
	CMapPtrToPtr omThirdPartyRkeyMap;

// Private helper routines
private:
	void GetOverlappedBarsFromTime(int ipGroupno, int ipLineno,
		CTime opTime1, CTime opTime2, CUIntArray &ropBarnoList);

// Attributes used for filtering condition
private:
	CMapStringToPtr omCMapForRegnArea;
	CMapStringToPtr omCMapForTemplate;
	BOOL bmIncludeArrivalFlights;
	BOOL bmIncludeDepartureFlights;
	CTime omStartTime;
	CTime omEndTime;

// Attributes
private:
	CCSPtrArray<REGN_GROUPDATA> omGroups;
	CBrush omBkBrush;
	CBrush omArrBkBrush;
	CBrush omDepBkBrush;
	CBrush omBkBreakBrush;

// Methods which handle changes (from Data Distributor)
private:
	void ProcessFlightChange(FLIGHTDATA *prpFlight);
	void ProcessFlightSelect(FLIGHTDATA *prpFlight);
	void ProcessSpecialJobNew(JOBDATA * prpJob);
	void ProcessSpecialJobChange(JOBDATA * prpJob);
	void ProcessSpecialJobDelete(JOBDATA * prpJob);
	void ProcessFmgJobNew(JOBDATA *prpJob);
	void ProcessFmgJobDelete(JOBDATA *prpJob);
	void ProcessDemandChange(DEMANDDATA *prpDem);
	void ProcessDemandDelete(DEMANDDATA *prpDemand);
	void UpdateBarsForDemand(long lpDemandUrno);
	// printing
public:
	void PrintDiagram(CPtrArray &opPtrArray);
	void PrintFm(CPtrArray &opPtrArray);

private:
	void PrintRegnDiagramHeader(int ipGroupno);
	CCSPrint *pomPrint;
	int	igFirstGroupOnPage;
	void PrintRegnArea(CPtrArray &opPtrArray,int ipGroupNo);
	void PrintPrepareLineData(int ipGroupNo,int ipLineno,CCSPtrArray<PRINTBARDATA> &ropPrintLine);
	void PrintManagers(int ipGroupno,int ipYOffset1,int ipYOffset2);

	void PrintFmHeader(int ipGroupNo);
	void PrintOneFm(SHIFTDATA *prpShift,EMPDATA *prpEmp,JOBDATA *prpJob,BOOL bpIsFirstLine);
	void PrintFmForRegnArea(CPtrArray &opPtrArray,int ipGroupNo);


};

/////////////////////////////////////////////////////////////////////////////

#endif
