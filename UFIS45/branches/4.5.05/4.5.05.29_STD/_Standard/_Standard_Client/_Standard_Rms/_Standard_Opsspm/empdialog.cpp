// EmpDialog.cpp : implementation file
//

#include <stdafx.h>
#include <CCSGlobl.h>
#include <OpssPm.h>
#include <CedaShiftData.h>
#include <CedaEmpData.h>
#include <CCSTime.H>
#include <CedaShiftTypeData.h>
#include <EmpDialog.h>
#include <CCSPtrArray.h>
#include <cviewer.h>
#include <table.h>
#include <CedaJobData.h>

#include <ccsddx.h>
#include <CedaAloData.h>
#include <BasicData.h>
#include <PrePlanTableViewer.h>
#include <CedaOdaData.h>
#include <CedaWgpData.h>
#include <CedaRnkData.h>
#include <CedaSprData.h>
#include <CedaSwgData.h>
#include <PrivList.h>

#include <ShiftCodeSelectionDlg.h>

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


/////////////////////////////////////////////////////////////////////////////
// CEmpDialog dialog
#define IsBetween(val, start, end)  ((start) <= (val) && (val) <= (end))


CEmpDialog::CEmpDialog(CWnd* pParent  /*=NULL*/)
	: CDialog(CEmpDialog::IDD, pParent)
{

	pomParent = pParent;
	bmEmpIsAbsent = false;
	bmChanged = false;
	bmOnlyBaseFunctions = false;

	//{{AFX_DATA_INIT(CEmpDialog)
	m_Rank = _T("");
	m_WorkGroup = _T("");
	m_Sub1Sub2 = _T("");
	m_Ddat = _T("");
	m_Lnam = _T("");
	m_Pkno = _T("");
	m_Sxid = _T("");
	m_Tel1 = _T("");
	m_Tel2 = _T("");
	m_Fnam = _T("");
	m_Sfca = _T("");
	m_Acfr1 = CTime(time(NULL));
	m_Acfr2 = CTime(time(NULL));
	m_Acfr3 = CTime(time(NULL));
	m_Acfr4 = CTime(time(NULL));
	m_Acto1 = CTime(time(NULL));
	m_Acto2 = CTime(time(NULL));
	m_Acto3 = CTime(time(NULL));
	m_Acto4 = CTime(time(NULL));
	m_Sfcs = _T("");
	m_BreakStart = CTime(time(NULL));
	m_BreakEnd = CTime(time(NULL));
	m_BreakDuration = CTime(time(NULL));
	m_Remark = _T("");
	m_Useu = _T("");
	m_Lstu = CTime(time(NULL));
	m_Lnam = _T("");
	m_Before = _T("");
	m_After = _T("");
	m_Complete = _T("");
	//}}AFX_DATA_INIT
}


void CEmpDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CEmpDialog)
	DDX_Control(pDX, IDC_E_ZCODE3, m_CompleteCtrl);
	DDX_Control(pDX, IDC_E_ZCODE2, m_AfterCtrl);
	DDX_Control(pDX, IDC_E_ZCODE, m_BeforeCtrl);
	DDX_Control(pDX, IDC_SFCA, m_SfcaCtrl);
	DDX_Control(pDX, IDC_SPTO, m_SbtoCtrl);
	DDX_Control(pDX, IDC_SPFR, m_SbfrCtrl);
	DDX_Control(pDX, IDC_RANK, m_RankCtrl);
	DDX_Control(pDX, IDC_WORKGROUP, m_WorkGroupCtrl);
	DDX_Control(pDX, IDC_BRDUR, m_BreakDurationCtrl);
	DDX_Control(pDX, IDC_ACTOTIME2, m_ActoTimeCtrl);
	DDX_Control(pDX, IDC_ACTODATE2, m_ActoDateCtrl);
	DDX_Control(pDX, IDC_ACFRTIME2, m_AcfrTimeCtrl);
	DDX_Control(pDX, IDC_ACFRDATE2, m_AcfrDateCtrl);
	DDX_Control(pDX, IDC_SUB1SUB2, m_Sub1Sub2Ctrl);
	DDX_CBString(pDX, IDC_RANK, m_Rank);
	DDX_CBString(pDX, IDC_WORKGROUP, m_WorkGroup);
	DDX_CBString(pDX, IDC_SUB1SUB2, m_Sub1Sub2);
	DDX_Text(pDX, IDC_LNAM, m_Lnam);
	DDX_Text(pDX, IDC_PKNO, m_Pkno);
	DDX_Text(pDX, IDC_PHONE1, m_Tel1);
	DDX_Text(pDX, IDC_PHONE2, m_Tel2);
	DDX_Text(pDX, IDC_FNAM, m_Fnam);
	DDV_MaxChars(pDX, m_Fnam, 26);
	DDX_CCSddmmyy(pDX, IDC_ACFRDATE2, m_Acfr2);
	DDX_CCSTime(pDX, IDC_ACFRTIME2, m_Acfr4);
	DDX_CCSddmmyy(pDX, IDC_ACTODATE2, m_Acto2);
	DDX_CCSTime(pDX, IDC_ACTOTIME2, m_Acto4);
	DDX_Text(pDX, IDC_REMA, m_Remark);
//	DDV_MaxChars(pDX, m_Remark, ogShiftData.GetMaxFieldLength("REMA"));
	DDX_Text(pDX, IDC_USEC, m_Useu);
	DDX_Text(pDX, IDC_E_ZCODE, m_Before);
	DDV_MaxChars(pDX, m_Before, 1);
	DDX_Text(pDX, IDC_E_ZCODE2, m_After);
	DDV_MaxChars(pDX, m_After, 1);
	DDX_Text(pDX, IDC_E_ZCODE3, m_Complete);
	DDV_MaxChars(pDX, m_Complete, 1);
	CEmpDialog_DDX_CCSTime(pDX, IDC_SPFR, m_BreakStart);
	CEmpDialog_DDX_CCSTime(pDX, IDC_SPTO, m_BreakEnd);
	CEmpDialog_DDX_CCSTime(pDX, IDC_BRDUR, m_BreakDuration);
	//}}AFX_DATA_MAP

	if(!pDX->m_bSaveAndValidate)
	{
		pDX->m_pDlgWnd->GetDlgItem(IDC_LSTU)->SetWindowText(m_Lstu.Format("%H:%M %d.%m.%Y"));
		DDX_Text(pDX, IDC_SFCA, m_Sfca);
		DDX_Text(pDX, IDC_SFCS, m_Sfcs);
		DDX_CCSddmmyy(pDX, IDC_ACFRDATE, m_Acfr1);
		DDX_CCSTime(pDX, IDC_ACFRTIME, m_Acfr3);
		DDX_CCSddmmyy(pDX, IDC_ACTODATE, m_Acto1);
		DDX_CCSTime(pDX, IDC_ACTOTIME, m_Acto3);
		DDX_Text(pDX, IDC_CLOCKON, m_ClockOn);
		DDX_Text(pDX, IDC_CLOCKOFF, m_ClockOff);
		DDX_CCSTime(pDX, IDC_SPFR, m_BreakStart);
		DDX_CCSTime(pDX, IDC_SPTO, m_BreakEnd);
		DDX_CCSTime(pDX, IDC_BRDUR, m_BreakDuration);
	}
}

void CEmpDialog::CEmpDialog_DDX_CCSTime( CDataExchange *pDX, int nIDC, CTime& tm )
{
    CString val;

    pDX->PrepareEditCtrl(nIDC);
    if (pDX->m_bSaveAndValidate)
    {
		pDX->m_pDlgWnd->GetDlgItem(nIDC)->GetWindowText(val);
        if ( !CheckHHMMValid( val ) )
		{
			tm = TIMENULL;
		}
		else
		{
			int hour = atoi( val.Left(2) );
			int minute = atoi( val.Right(2) );

			tm = CTime( CTime::GetCurrentTime().GetYear(),
						CTime::GetCurrentTime().GetMonth(),
						CTime::GetCurrentTime().GetDay(),
						hour, minute, 0);
		}
    }
    else
    {
        val = tm.Format( "%H:%M" );
        pDX->m_pDlgWnd->GetDlgItem( nIDC )->SetWindowText( val );
    }
}


BEGIN_MESSAGE_MAP(CEmpDialog, CDialog)
	//{{AFX_MSG_MAP(CEmpDialog)
	ON_WM_CREATE()
	ON_WM_CTLCOLOR()
	ON_BN_CLICKED(IDC_SELECTCODE_BUTTON, OnSelectCodeButton)
	ON_EN_KILLFOCUS(IDC_ACFRDATE2, OnKillfocusAcfrdate2)
	ON_EN_KILLFOCUS(IDC_ACFRTIME2, OnKillfocusAcfrtime2)
	ON_EN_KILLFOCUS(IDC_ACTODATE2, OnKillfocusActodate2)
	ON_EN_KILLFOCUS(IDC_ACTOTIME2, OnKillfocusActotime2)
	ON_EN_KILLFOCUS(IDC_BRDUR, OnKillfocusBrdur)
	ON_EN_KILLFOCUS(IDC_SPFR, OnKillfocusSpfr)
	ON_EN_KILLFOCUS(IDC_SPTO, OnKillfocusSpto)
	ON_CBN_SELCHANGE(IDC_RANK, OnSelchangeRank)
	ON_CBN_SELCHANGE(IDC_WORKGROUP, OnSelchangeWorkGroup)
	ON_EN_CHANGE(IDC_E_ZCODE, OnChangeEZcode)
	ON_EN_CHANGE(IDC_E_ZCODE2, OnChangeEZcode2)
	ON_EN_CHANGE(IDC_E_ZCODE3, OnChangeEZcode3)
	ON_EN_KILLFOCUS(IDC_E_ZCODE, OnKillfocusEZcode)
	ON_EN_KILLFOCUS(IDC_E_ZCODE2, OnKillfocusEZcode2)
	ON_EN_KILLFOCUS(IDC_E_ZCODE3, OnKillfocusEZcode3)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CEmpDialog message handlers

void CEmpDialog::OnCancel() 
{
	// TODO: Add extra cleanup here
	
	CDialog::OnCancel();
}

void CEmpDialog::SetTitle(UINT ipControlId, UINT ipStringId)
{
	CWnd *polWnd = GetDlgItem(ipControlId); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(ipStringId));
	}
}

void CEmpDialog::EnableWindow(UINT ipControlId, bool bpEnable)
{
	CWnd *polWnd = GetDlgItem(ipControlId); 
	if(polWnd != NULL)
	{
		polWnd->EnableWindow(bpEnable ? TRUE : FALSE);
	}
}

BOOL CEmpDialog::OnInitDialog() 
{
	CDialog::OnInitDialog();
	SetFieldsForShift();

	CWnd *polWnd = NULL;
	int ilCount;
	int ilLc;

	omStartDate = omShift.Avfa;

	SetTitle(IDC_INSFIRSTNAME,IDS_STRING32930);
	SetTitle(IDC_INSLASTNAME,IDS_STRING32931);
	SetTitle(IDC_INSTEAM,IDS_STRING32927);
	SetTitle(IDC_INSCODE,IDS_STRING32929);
	SetTitle(IDC_INSSHIFTPLAN,IDS_STRING32932);
	SetTitle(IDC_INSSHIFTPLAN_FROM,IDS_STRING32845);
	SetTitle(IDC_INSSHIFTPLANTO,IDS_STRING32846);
	SetTitle(IDC_INSSHIFTCUR,IDS_STRING32933);
	SetTitle(IDC_INSSHIFTCUR_FROM,IDS_STRING32845);
	SetTitle(IDC_INSSHIFTCUR_TO,IDS_STRING32846);
	SetTitle(IDC_BREAKPERIOD1,IDS_STRING32934);
	SetTitle(IDC_INSFROM,IDS_INSFROM);
	SetTitle(IDC_INSTO,IDS_STRING32846);
	SetTitle(IDC_BREAKDURATION,IDS_BREAKDURATION);
	SetTitle(IDC_CLOCKONTITLE,IDS_CLOCKONTITLE);
	SetTitle(IDC_CLOCKOFFTITLE,IDS_CLOCKOFFTITLE);
	SetTitle(IDC_SELECTCODE_BUTTON,IDS_EMPDLG_SELECT);
	SetTitle(IDC_INSREMARK,IDS_STAFFDD_TEXTTITLE2);
	SetTitle(IDC_PHONEGRP,IDS_PHONEGRP);
	SetTitle(IDC_PHONETITLE1,IDS_PHONETITLE1);
	SetTitle(IDC_PHONETITLE2,IDS_PHONETITLE2);
	SetTitle(IDC_CHANGEDBYTITLE,IDS_CHANGEDBYTITLE);
	SetTitle(IDOK,IDS_STRING61697);
	SetTitle(IDCANCEL,IDS_STRING61698);
	SetTitle(IDC_S_Vor,IDS_OVERTIME_BEFORE);
	SetTitle(IDC_S_Nach,IDS_OVERTIME_AFTER);
	SetTitle(IDC_S_Komp,IDS_OVERTIME_COMPLETE);
	SetTitle(IDC_S_ZCode,IDS_OVERTIME_TITLE);

	CString olTitle;
	olTitle = GetString(IDS_STRING32926);

	SetWindowText(olTitle);

	ogSpfData.GetSpfsBySurn(omShift.Stfu, omSpfs);

 	if(ogPrivList.GetStat("ONLY BASE FUNCTIONS") == '1')
	{
		if(m_Rank)
		{
			SendDlgItemMessage(IDC_RANK,CB_ADDSTRING,0,(LPARAM)(LPCTSTR)m_Rank);
		}

		ilCount = omSpfs.GetSize();

		for(ilLc = 0; ilLc < ilCount; ilLc++)
		{
			SPFDATA *prlSpf = &omSpfs[ilLc];

			if ( prlSpf && 
				 (m_RankCtrl.FindStringExact ( -1, prlSpf->Code ) == CB_ERR ) )
			{
				SendDlgItemMessage(IDC_RANK,CB_ADDSTRING,0,(LPARAM)prlSpf->Code);
			}
		}
		bmOnlyBaseFunctions = true;
	}
	else
	{
		CStringArray olRanks;
		ogRnkData.GetAllRanks(olRanks);
		ilCount = olRanks.GetSize();
		for(ilLc = 0; ilLc < ilCount; ilLc++)
		{
			SendDlgItemMessage(IDC_RANK,CB_ADDSTRING,0,(LPARAM)(LPCTSTR)olRanks[ilLc]);
		}
		bmOnlyBaseFunctions = false;
	}
	CString olRank = CString("");
	SendDlgItemMessage(IDC_RANK,CB_ADDSTRING,0,(LPARAM)(LPCTSTR)olRank); // add an empty function code


	CStringArray olTeams;
	ogWgpData.GetAllTeams(olTeams);
	ilCount = olTeams.GetSize();
	for(ilLc = 0; ilLc < ilCount; ilLc++)
	{
		SendDlgItemMessage(IDC_WORKGROUP,CB_ADDSTRING,0,(LPARAM)(LPCTSTR)olTeams[ilLc]);
	}
//	if(m_WorkGroup.IsEmpty())
	{
		CString olTeam = CString("");
		SendDlgItemMessage(IDC_WORKGROUP,CB_ADDSTRING,0,(LPARAM)(LPCTSTR)olTeam); // add an empty workgroup
	}
	SetWindowText(GetString(IDS_EMPDIALOGTITLE));

	UpdateData(FALSE);

	m_AcfrDateCtrl.GetWindowText(omOldAcfrDate);
	m_AcfrTimeCtrl.GetWindowText(omOldAcfrTime);
	m_ActoDateCtrl.GetWindowText(omOldActoDate);
	m_ActoTimeCtrl.GetWindowText(omOldActoTime);
	m_BreakDurationCtrl.GetWindowText(omOldBreakDuration);
	m_SbfrCtrl.GetWindowText(omOldSbfr);
	m_SbtoCtrl.GetWindowText(omOldSbto);
	m_SfcaCtrl.GetWindowText(omOldSfca);
	omOldRank = m_Rank;
	omOldWorkGroup = m_WorkGroup;
	m_BeforeCtrl.GetWindowText(omOldBefore);
	m_AfterCtrl.GetWindowText(omOldAfter);
	m_CompleteCtrl.GetWindowText(omOldComplete);

	InitSub1Sub2ComboBox();
	
	if((ogSprData.bmNoChangesAllowedAfterClockout && ogSprData.ClockedOut(&omShift)) || strcmp(omShift.Ross,"A") || !bgOnline) // cannot edit a shift unless it is ACTIVE and the operator is working online
	{
		EnableWindow(IDC_ACFRTIME2, false);
		EnableWindow(IDC_ACFRDATE2, false);
		EnableWindow(IDC_ACTOTIME2, false);
		EnableWindow(IDC_ACTODATE2, false);
		EnableWindow(IDC_SPFR, false);
		EnableWindow(IDC_SPTO, false);
		EnableWindow(IDC_BRDUR, false);
		EnableWindow(IDC_REMA, false);
		EnableWindow(IDOK, false);
		EnableWindow(IDC_RANK, false);
		EnableWindow(IDC_WORKGROUP, false);
		EnableWindow(IDC_SUB1SUB2, false);
		EnableWindow(IDC_E_ZCODE, false);
		EnableWindow(IDC_E_ZCODE2, false);
		EnableWindow(IDC_E_ZCODE3, false);

		AfxMessageBox(GetString(IDS_SHIFT_INACTIVE), MB_ICONINFORMATION);
	}
	else if(bmEmpIsAbsent)
	{
		EnableWindow(IDC_ACFRTIME2, false);
		EnableWindow(IDC_ACFRDATE2, false);
		EnableWindow(IDC_ACTOTIME2, false);
		EnableWindow(IDC_ACTODATE2, false);
		EnableWindow(IDC_SPFR, false);
		EnableWindow(IDC_SPTO, false);
		EnableWindow(IDC_BRDUR, false);
		EnableWindow(IDC_RANK, false);
		EnableWindow(IDC_WORKGROUP, false);
		EnableWindow(IDC_SUB1SUB2, false);
		EnableWindow(IDC_E_ZCODE, false);
		EnableWindow(IDC_E_ZCODE2, false);
		EnableWindow(IDC_E_ZCODE3, false);
	}

	return TRUE;  // return TRUE unless you set the focus to a control
}

void CEmpDialog::SetFieldsForShift()
{
	bmEmpIsAbsent = ogShiftData.IsAbsent(&omShift);
	CTime olClockOn = ogSprData.GetTimeForUstf(omShift.Stfu, SPR_STARTSHIFT, omShift.Avfa, omShift.Avta);
	CTime olClockOff = ogSprData.GetTimeForUstf(omShift.Stfu, SPR_ENDSHIFT, omShift.Avfa, omShift.Avta);

	if(olClockOn != TIMENULL)
	{
		m_ClockOn = olClockOn.Format("%H:%M %d.%m.%Y");
	}
	if(olClockOff != TIMENULL)
	{
		m_ClockOff = olClockOff.Format("%H:%M %d.%m.%Y");
	}

	EMPDATA *prlEmp = ogEmpData.GetEmpByUrno(omShift.Stfu);
	if(prlEmp != NULL)
	{
		m_Lnam = prlEmp->Lanm;
		m_Fnam = prlEmp->Finm;
		m_Pkno = prlEmp->Peno;
		m_Tel1 = prlEmp->Telp;
		m_Tel2 = prlEmp->Telh;
	}

	m_Acfr1 = omShift.Avfs;
	m_Acfr2 = omShift.Avfa;
	m_Acfr3 = omShift.Avfs;
	m_Acfr4 = omShift.Avfa;

	m_Acto1 = omShift.Avts;
	m_Acto2 = omShift.Avta;
	m_Acto3 = omShift.Avts;
	m_Acto4 = omShift.Avta;

	m_Sfca = omShift.Sfca;
	m_Sfcs = omShift.Sfco;
	m_Remark = omShift.Rema;
	m_Useu = omShift.Useu;
	m_Lstu = omShift.Lstu;
	m_Rank = ogBasicData.GetFunctionForShift(&omShift);
	m_WorkGroup = ogBasicData.GetTeamForShift(&omShift);
	m_Before = omShift.Drs1;
	m_After = omShift.Drs3;
	m_Complete = omShift.Drs4;

	m_BreakStart = omShift.Sbfr;
	m_BreakEnd = omShift.Sbto;
	if(m_BreakStart != TIMENULL && strlen(omShift.Sblu) > 0)
	{
		m_BreakDuration = CTime(m_BreakStart.GetYear(),m_BreakStart.GetMonth(),m_BreakStart.GetDay(),(atoi(omShift.Sblu) / 60),(atoi(omShift.Sblu) % 60),0);
	}
	else
	{
		m_BreakDuration = TIMENULL;
	}
}

void CEmpDialog::OnOK() 
{
	bool blRc = UpdateData(TRUE) ? true : false;
	CString olErrorMsg;

	if(bmEmpIsAbsent)
	{
		strcpy(omShift.Rema,m_Remark);
		if(m_Sub1Sub2 == GetString(IDS_EMPDLG_SUB1))
			strcpy(omShift.Drs2, "1");
		else if(m_Sub1Sub2 == GetString(IDS_EMPDLG_SUB2))
			strcpy(omShift.Drs2, "2");
		else
			strcpy(omShift.Drs2, "");
	}
	else
	{
		while(blRc)
		{
			// not absent
			if(m_Rank.IsEmpty())
			{
				olErrorMsg = GetString(IDS_ERREMPDLGFUNC);
				blRc = false;
				break;
			}
			if( !bmOnlyBaseFunctions )
			{
				if ( m_Rank != omShift.Fctc ) // Function modified
				{
					bool blFctcFound = false;
					int	ilCount = omSpfs.GetSize();
					for(int ilLc = 0; (ilLc < ilCount) && !blFctcFound; ilLc++)
					{
						SPFDATA *prlSpf = &omSpfs[ilLc];
						if ( prlSpf && (m_Rank==prlSpf->Code) )
							blFctcFound = true;
					}
					if ( !blFctcFound )
					{
						CString olWndCaption;
						int ilRc;
						GetWindowText(olWndCaption);
						olErrorMsg = GetString(IDS_WARN_FUNCTION_CHANGE);
						ilRc = MessageBox(olErrorMsg, olWndCaption, MB_ICONQUESTION|MB_OKCANCEL);
						if ( ilRc != IDOK )
						{
							olErrorMsg.Empty();
							blRc = false;
							break;
						}

					}
				}

			}

			if(!strcmp(omShift.Ctyp,"A"))
			{
				// was previously set to absent
				strcpy(omShift.Ctyp,"S");
				strcpy(omShift.Sfca,omShift.Sfcs);
				if(omShift.Avfa == m_Acfr4)
				{
					omShift.Avfa = omShift.Avfs;
				}
				if(omShift.Avta == m_Acto4)
				{
					omShift.Avta = omShift.Avts;
				}
			}

			if(!CheckDDMMYYValid(m_Acfr2.Format("%d%m%y")))
			{
				//olErrorMsg = "Invalid date in Shift (Current) From.\nEnter dd.mm.yyyy";
				olErrorMsg = GetString(IDS_ERREMPDLG01);
				blRc = false;
				break;
			}

			if(!CheckHHMMValid(m_Acfr4.Format("%H%M")))
			{
				//olErrorMsg = "Invalid time in Shift (Current) From.\nEnter hh:mm";
				olErrorMsg = GetString(IDS_ERREMPDLG02);
				blRc = false;
				break;
			}

			if(!CheckDDMMYYValid(m_Acto2.Format("%d%m%y")))
			{
				//olErrorMsg = "Invalid date in Shift (Current) To.\nEnter dd.mm.yyyy";
				olErrorMsg = GetString(IDS_ERREMPDLG03);
				blRc = false;
				break;
			}

			if(!CheckHHMMValid(m_Acto4.Format("%H%M")))
			{
				//olErrorMsg = "Invalid time in Shift (Current) To.\nEnter hh:mm";
				olErrorMsg = GetString(IDS_ERREMPDLG04);
				blRc = false;
				break;
			}

			CTime olAvfa(m_Acfr2.GetYear(), m_Acfr2.GetMonth(),m_Acfr2.GetDay(), m_Acfr4.GetHour(), m_Acfr4.GetMinute(), m_Acfr4.GetSecond());
			CTime olAvta(m_Acto2.GetYear(), m_Acto2.GetMonth(), m_Acto2.GetDay(), m_Acto4.GetHour(), m_Acto4.GetMinute(), m_Acto4.GetSecond());

			if(olAvfa >= olAvta)
			{
				//olErrorMsg = "Shift (Current) From must be an earlier date than Shift (Current) To";
				olErrorMsg = GetString(IDS_ERREMPDLG05);
				blRc = false;
				break;
			}


			CTime olBreakStart = TIMENULL, olBreakEnd = TIMENULL;
			CString olBreakDuration("");
			if(m_BreakStart != TIMENULL ||	m_BreakEnd != TIMENULL || m_BreakDuration != TIMENULL)
			{
				if(!CheckHHMMValid(m_BreakStart.Format("%H%M")))
				{
					//olErrorMsg = "Invalid time in Break Period From.\nEnter hh:mm";
					olErrorMsg = GetString(IDS_ERREMPDLG06);
					blRc = false;
					break;
				}

				if(!CheckHHMMValid(m_BreakEnd.Format("%H%M")))
				{
					//olErrorMsg = "Invalid time in Break Period To.\nEnter hh:mm";
					olErrorMsg = GetString(IDS_ERREMPDLG07);
					blRc = false;
					break;
				}

				if(!CheckHHMMValid(m_BreakDuration.Format("%H%M")))
				{
					//olErrorMsg = "Invalid time in Break Duration.\nEnter hh:mm";
					olErrorMsg = GetString(IDS_ERREMPDLG11);
					blRc = false;
					break;
				}


				olBreakStart = CTime(olAvfa.GetYear(), olAvfa.GetMonth(), olAvfa.GetDay(), m_BreakStart.GetHour(),m_BreakStart.GetMinute(),0);
				if(olBreakStart < olAvfa)
				{
					if(olAvfa.GetYear() != olAvta.GetYear() || olAvfa.GetMonth() != olAvta.GetMonth() || olAvfa.GetDay() != olAvta.GetDay())
					{
						olBreakStart = CTime(olAvta.GetYear(), olAvta.GetMonth(), olAvta.GetDay(), m_BreakStart.GetHour(),m_BreakStart.GetMinute(),0);
					}
				}
				if(!IsBetween(olBreakStart,olAvfa,olAvta))
				{
					//olErrorMsg = "The Break Period must be within the current shift";
					olErrorMsg = GetString(IDS_ERREMPDLG08);
					blRc = false;
					break;
				}

				olBreakEnd = CTime(olAvfa.GetYear(), olAvfa.GetMonth(), olAvfa.GetDay(), m_BreakEnd.GetHour(),m_BreakEnd.GetMinute(),0);
				if(olBreakEnd < olAvfa)
				{
					if(olAvfa.GetYear() != olAvta.GetYear() || olAvfa.GetMonth() != olAvta.GetMonth() || olAvfa.GetDay() != olAvta.GetDay())
					{
						olBreakEnd = CTime(olAvta.GetYear(), olAvta.GetMonth(), olAvta.GetDay(), m_BreakEnd.GetHour(),m_BreakEnd.GetMinute(),0);
					}
				}
				if(!IsBetween(olBreakEnd,olAvfa,olAvta))
				{
					//olErrorMsg = "The Break Period must be within the current shift";
					olErrorMsg = GetString(IDS_ERREMPDLG08);
					blRc = false;
					break;
				}


				if(olBreakStart > olBreakEnd)
				{
					//olErrorMsg = "Break Period From must be an earlier date than Break Period To";
					olErrorMsg = GetString(IDS_ERREMPDLG10);
					blRc = false;
					break;
				}

				CTime olBreakDurationTime(olBreakStart.GetYear(), olBreakStart.GetMonth(), olBreakStart.GetDay(), m_BreakDuration.GetHour(),m_BreakDuration.GetMinute(),0);
				olBreakDuration.Format("%d", (olBreakDurationTime.GetHour() * 60) + olBreakDurationTime.GetMinute());
			}

			if(blRc)
			{
				omShift.Avfa = olAvfa;
				omShift.Avta = olAvta;
				omShift.Sbfr = olBreakStart;
				omShift.Sbto = olBreakEnd;
				sprintf(omShift.Sblu,"%d",atoi(olBreakDuration));
				strcpy(omShift.Rema,m_Remark);
				//strcpy(omShift.Fctc,m_Rank);
				if(m_Sub1Sub2 == GetString(IDS_EMPDLG_SUB1))
					strcpy(omShift.Drs2, "1");
				else if(m_Sub1Sub2 == GetString(IDS_EMPDLG_SUB2))
					strcpy(omShift.Drs2, "2");
				else
					strcpy(omShift.Drs2, "");
				strcpy(omShift.Drs1,m_Before);
				strcpy(omShift.Drs3,m_After);
				strcpy(omShift.Drs4,m_Complete);
				strcpy(omShift.Egrp,m_WorkGroup);
			}
			break;
		}
	}

	if(blRc)
	{
		EndDialog(IDOK);
	}
	else
	{
		if(!olErrorMsg.IsEmpty())
		{
			CString olWndCaption;
			GetWindowText(olWndCaption);
			MessageBox(olErrorMsg, olWndCaption, MB_ICONERROR);
		}
	}
}

int CEmpDialog::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CDialog::OnCreate(lpCreateStruct) == -1)
		return -1;
	return 0;
}

HBRUSH CEmpDialog::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor) 
{
	HBRUSH hbr = 0;
	long llRc;

	CWnd *polWnd = GetDlgItem(IDC_SFCA);
	if (polWnd->m_hWnd == pWnd->m_hWnd)
	{
		llRc = pDC->SetBkColor(SILVER);
		llRc = pDC->SetTextColor((bmEmpIsAbsent) ? RED : BLACK);
		hbr = CreateSolidBrush(SILVER);


		return hbr;
	}

	return CDialog::OnCtlColor(pDC,pWnd,nCtlColor);
}

void CEmpDialog::OnSelectCodeButton() 
{
	bool blOldEmpIsAbsent = bmEmpIsAbsent;
	if(UpdateData(TRUE))
	{
		CShiftCodeSelectionDlg olDlg;
		olDlg.bmEnableBothCodes = true;
		if(bmEmpIsAbsent)
		{
			olDlg.bmSelectShiftCodeFirst = false;
		}
		else
		{
			olDlg.bmSelectShiftCodeFirst = true;
		}
		olDlg.omSelectedCode = omShift.Sfca;

		if(olDlg.DoModal() == IDOK)
		{
			if(olDlg.bmIsShiftCode)
			{
				SHIFTTYPEDATA *prlShiftType = ogShiftTypes.GetShiftTypeByUrno(olDlg.lmSelectedCodeUrno);
				if(prlShiftType != NULL)
				{
					bmEmpIsAbsent = false;
					UpdateShiftForShiftCode(prlShiftType);
				}
			}
			else
			{
				ODADATA *prlOda = ogOdaData.GetOdaByUrno(olDlg.lmSelectedCodeUrno);
				if(prlOda != NULL)
				{
					bmEmpIsAbsent = true;
					UpdateShiftForAbsenceCode(prlOda);
				}
			}
			CWnd *polWnd = GetDlgItem(IDC_SFCA);
			if(polWnd != NULL)
			{
				RECT rcRect;
				polWnd->GetClientRect(&rcRect);
				InvalidateRect(&rcRect, TRUE);
			}
			SetFieldsForShift();
			UpdateData(FALSE);
			SetSub1Sub2();
		}
	}

	if(blOldEmpIsAbsent != bmEmpIsAbsent)
	{
		bool blEnable = !bmEmpIsAbsent;
		EnableWindow(IDC_ACFRTIME2, blEnable);
		EnableWindow(IDC_ACFRDATE2, blEnable);
		EnableWindow(IDC_ACTOTIME2, blEnable);
		EnableWindow(IDC_ACTODATE2, blEnable);
		EnableWindow(IDC_SPFR, blEnable);
		EnableWindow(IDC_SPTO, blEnable);
		EnableWindow(IDC_BRDUR, blEnable);
		EnableWindow(IDC_RANK, blEnable);
		EnableWindow(IDC_WORKGROUP, blEnable);
		EnableWindow(IDC_SUB1SUB2, blEnable);
		EnableWindow(IDC_E_ZCODE, blEnable);
		EnableWindow(IDC_E_ZCODE2, blEnable);
		EnableWindow(IDC_E_ZCODE3, blEnable);
	}
}

void CEmpDialog::UpdateShiftForShiftCode(SHIFTTYPEDATA *prpShiftType)
{
	if(prpShiftType != NULL)
	{
		CTimeSpan olOneDay(1, 0, 0, 0);
		CTime olTestDay;
		CString olHHMM;

		// shift start
		olHHMM = prpShiftType->Esbg;
		if(olHHMM.GetLength() == 4)
		{
			omShift.Avfa = CTime(omStartDate.GetYear(), omStartDate.GetMonth(), omStartDate.GetDay(), atoi(olHHMM.Left(2)), atoi(olHHMM.Mid(2,2)), 0);
		}

		// shift end
		olHHMM = prpShiftType->Lsen;
		if(olHHMM.GetLength() == 4)
		{
			omShift.Avta = CTime(omStartDate.GetYear(), omStartDate.GetMonth(), omStartDate.GetDay(), atoi(olHHMM.Left(2)), atoi(olHHMM.Mid(2,2)), 0);
		}

		if(omShift.Avfa > omShift.Avta)
		{
			omShift.Avta += olOneDay;
		}

		// pausenlage begin
		olHHMM = prpShiftType->Bkf1;
		if(olHHMM.GetLength() == 4)
		{
			omShift.Sbfr = CTime(omStartDate.GetYear(), omStartDate.GetMonth(), omStartDate.GetDay(), atoi(olHHMM.Left(2)), atoi(olHHMM.Mid(2,2)), 0);
			olTestDay = omShift.Sbfr + olOneDay;
			if(!IsBetween(omShift.Sbfr,omShift.Avfa,omShift.Avta) && IsBetween(olTestDay,omShift.Avfa,omShift.Avta))
			{
				omShift.Sbfr = olTestDay;
			}
		}
		else
		{
			omShift.Sbfr = TIMENULL;
		}

		// pausenlage end
		olHHMM = prpShiftType->Bkt1;
		if(olHHMM.GetLength() == 4)
		{
			omShift.Sbto = CTime(omStartDate.GetYear(), omStartDate.GetMonth(), omStartDate.GetDay(), atoi(olHHMM.Left(2)), atoi(olHHMM.Mid(2,2)), 0);
			olTestDay = omShift.Sbto + olOneDay;
			if(!IsBetween(omShift.Sbto,omShift.Avfa,omShift.Avta) && IsBetween(olTestDay,omShift.Avfa,omShift.Avta))
			{
				omShift.Sbto = olTestDay;
			}
		}
		else
		{
			omShift.Sbto = TIMENULL;
		}

		// break duration
		strcpy(omShift.Sblu,prpShiftType->Bkd1);

		strcpy(omShift.Sfco, omShift.Sfca); //previous shift code
		strcpy(omShift.Sfca, prpShiftType->Bsdc);
		//strcpy(omShift.Fctc, prpShiftType->Fctc);
		strcpy(omShift.Bkdp, prpShiftType->Bkdp);
		strcpy(omShift.Ctyp,"S");
	}
}

void CEmpDialog::UpdateShiftForAbsenceCode(ODADATA *prpOda)
{
	if(prpOda != NULL)
	{
		strcpy(omShift.Sfco, omShift.Sfca); //previous shift code
		strcpy(omShift.Sfca, prpOda->Sdac);
		if(*prpOda->Work != '1')
		{
			strcpy(omShift.Ctyp,"A");
			strcpy(omShift.Drs1,"");
			strcpy(omShift.Drs3,"");
			strcpy(omShift.Drs4,"");
			CString olSday = omShift.Sday;
			omShift.Avfa = CTime(atoi(olSday.Left(4)),atoi(olSday.Mid(4,2)),atoi(olSday.Right(2)),8,30,0);
			omShift.Avta = omShift.Avfa;
		}
	}
}


void CEmpDialog::OnKillfocusAcfrdate2() 
{
	SetSub1Sub2();
}

void CEmpDialog::OnKillfocusAcfrtime2() 
{
	SetSub1Sub2();
}

void CEmpDialog::OnKillfocusActodate2() 
{
	SetSub1Sub2();
}

void CEmpDialog::OnKillfocusActotime2() 
{
	SetSub1Sub2();
}

void CEmpDialog::OnKillfocusBrdur() 
{
	SetSub1Sub2();
}

void CEmpDialog::OnKillfocusSpfr() 
{
	SetSub1Sub2();
}

void CEmpDialog::OnKillfocusSpto() 
{
	SetSub1Sub2();
}

void CEmpDialog::OnSelchangeRank() 
{
	SetSub1Sub2();
}

void CEmpDialog::OnSelchangeWorkGroup() 
{
	SetSub1Sub2();
}

void CEmpDialog::SetSub1Sub2()
{
	if(!bmChanged)
	{
		CString olAcfrDate, olAcfrTime, olActoDate, olActoTime, olBreakDuration, olSbfr, olSbto, olRank, olWorkGroup, olSfca, olBefore, olAfter, olComplete;
		m_AcfrDateCtrl.GetWindowText(olAcfrDate);
		m_AcfrTimeCtrl.GetWindowText(olAcfrTime);
		m_ActoDateCtrl.GetWindowText(olActoDate);
		m_ActoTimeCtrl.GetWindowText(olActoTime);
		m_BreakDurationCtrl.GetWindowText(olBreakDuration);
		m_SbfrCtrl.GetWindowText(olSbfr);
		m_SbtoCtrl.GetWindowText(olSbto);
		m_SfcaCtrl.GetWindowText(olSfca);
		m_BeforeCtrl.GetWindowText(olBefore);
		m_AfterCtrl.GetWindowText(olAfter);
		m_CompleteCtrl.GetWindowText(olComplete);
		int ilIdx = m_RankCtrl.GetCurSel();
		if(ilIdx != CB_ERR)
		{
			m_RankCtrl.GetLBText(ilIdx, olRank);
		}
		ilIdx = m_WorkGroupCtrl.GetCurSel();
		if(ilIdx != CB_ERR)
		{
			m_WorkGroupCtrl.GetLBText(ilIdx, olWorkGroup);
		}
		
		if(olAcfrDate != omOldAcfrDate || olAcfrTime != omOldAcfrTime || olActoDate != omOldActoDate ||
			olActoTime != omOldActoTime || olBreakDuration != omOldBreakDuration ||
			olSbfr != omOldSbfr || olSbto != omOldSbto || olRank != omOldRank || olWorkGroup != omOldWorkGroup || olSfca != omOldSfca ||
			olBefore != omOldBefore || olAfter != omOldAfter || olComplete != omOldComplete)
		{
			m_Sub1Sub2Ctrl.SelectString(-1,GetString(IDS_EMPDLG_SUB2));
			bmChanged = true;
		}
	}
}

void CEmpDialog::InitSub1Sub2ComboBox()
{
	ogBasicData.SetWindowStat("GENERAL SUB1SUB2", GetDlgItem(IDC_SUB1SUB2));
	CWnd *polWnd = GetDlgItem(IDC_SUB1SUB2TITLE);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_SUB1SUB2));
		ogBasicData.SetWindowStat("GENERAL SUB1SUB2", polWnd);
	}
	
	if(ogPrivList.GetStat("GENERAL SUB1SUB2") != '-')
	{
		// initializing the combo
		CDC *pdc = m_Sub1Sub2Ctrl.GetDC();
		CFont olFont;
		CFont *pOldFont	= pdc->SelectObject(&olFont);
		CSize olSizeSub1;
		CSize olSizeSub2;
		long ilWidth;

		CString olInsertSub1 = GetString(IDS_EMPDLG_SUB1);
		CString olInsertSub2 = GetString(IDS_EMPDLG_SUB2);

		m_Sub1Sub2Ctrl.ResetContent();
		m_Sub1Sub2Ctrl.InsertString(-1, olInsertSub1);
		m_Sub1Sub2Ctrl.InsertString(-1, olInsertSub2);

		olSizeSub1 = pdc->GetTextExtent(olInsertSub1);
		olSizeSub2 = pdc->GetTextExtent(olInsertSub2);
		ilWidth = max(olSizeSub1.cx, olSizeSub2.cx) - 40;
		m_Sub1Sub2Ctrl.SetDroppedWidth((UINT)ilWidth);
		pdc->SelectObject(&olFont);

		//setting the initial selection
		if(!strcmp(omShift.Drs2, "2"))
		{
			m_Sub1Sub2Ctrl.SetCurSel(1);
		}
		else
		{
			m_Sub1Sub2Ctrl.SetCurSel(0);
		}
	}
}

void CEmpDialog::CheckOvertimeValue(UINT ipId)
{
	CWnd *polWnd = GetDlgItem(ipId); 
	if(polWnd != NULL)
	{
		CString olValue;
		polWnd->GetWindowText(olValue);
		if(!olValue.IsEmpty() && olValue.FindOneOf(" 12") < 0)
		{
			polWnd->SetWindowText("");
		}
	}
}

void CEmpDialog::OnChangeEZcode() 
{
	CheckOvertimeValue(IDC_E_ZCODE);
}

void CEmpDialog::OnChangeEZcode2() 
{
	CheckOvertimeValue(IDC_E_ZCODE2);
}

void CEmpDialog::OnChangeEZcode3() 
{
	CheckOvertimeValue(IDC_E_ZCODE3);
}

void CEmpDialog::OnKillfocusEZcode() 
{
	SetSub1Sub2();
}

void CEmpDialog::OnKillfocusEZcode2() 
{
	SetSub1Sub2();
}

void CEmpDialog::OnKillfocusEZcode3() 
{
	SetSub1Sub2();
}
