#if !defined(AFX_SHIFTCODESELECTIONDLG_H__8D438333_BB99_11D4_8020_00010215BFE5__INCLUDED_)
#define AFX_SHIFTCODESELECTIONDLG_H__8D438333_BB99_11D4_8020_00010215BFE5__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// FunctionCodeSelectionDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CShiftCodeSelectionDlg dialog

class CShiftCodeSelectionDlg : public CDialog
{
// Construction
public:
	CShiftCodeSelectionDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CShiftCodeSelectionDlg)
	enum { IDD = IDD_SEL_SHIFTCODES_DLG };
	CButton	m_ShiftCodesCtrl;
	CButton	m_AbsenceCodesCtrl;
	CListBox	m_CodesList;
	int		m_ShiftOrAbsence;
	CEdit   m_EditShiftStartTime; //Singapore
	CEdit   m_EditShiftEndTime;   //Singapore
	//}}AFX_DATA

	CString omSelectedCode;			// the selected code - both initially and on OK
	long	lmSelectedCodeUrno;		// the URNO of the code selected when the user presses OK
	bool	bmIsShiftCode;			// false if the returned value is an absence code
	bool	bmEnableBothCodes;		// display both shift code and absence list
	bool	bmSelectShiftCodeFirst;	// which of the two codes types is initially selected

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CShiftCodeSelectionDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CShiftCodeSelectionDlg)
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	afx_msg void OnDblclkCodesList();
	afx_msg void OnChangeCodeType();
	afx_msg void OnCheckT2(); //Singapore
	afx_msg void OnCheckT3(); //Singapore
	afx_msg void OnChangeEditShiftStartTime(); //Singapore
	afx_msg void OnChangeEditShiftEndTime();   //Singapore
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	void DisplayShiftCodes(void);
	void DisplayAbsenceCodes(void);
	void DisplayCheckTerminal(int ipShowT2 = SW_HIDE, int ipShowT3 = SW_HIDE); //Singapore
	void UpdateDisplayList(); //Singapore
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SHIFTCODESELECTIONDLG_H__8D438333_BB99_11D4_8020_00010215BFE5__INCLUDED_)
