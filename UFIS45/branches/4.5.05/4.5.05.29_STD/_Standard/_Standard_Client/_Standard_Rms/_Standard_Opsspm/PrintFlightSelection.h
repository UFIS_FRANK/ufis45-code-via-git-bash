#if !defined(AFX_PRINTFLIGHTSELECTION_H__BEE379D3_8844_11D6_8181_00010215BFE5__INCLUDED_)
#define AFX_PRINTFLIGHTSELECTION_H__BEE379D3_8844_11D6_8181_00010215BFE5__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// PrintFlightSelection.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CPrintFlightSelection dialog

class CPrintFlightSelection : public CDialog
{
// Construction
public:
	CPrintFlightSelection(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CPrintFlightSelection)
	enum { IDD = IDD_PRINTFLIGHT };
	int		m_SelectedFlight;
	//}}AFX_DATA

	long lmFlightArrUrno, lmFlightDepUrno;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPrintFlightSelection)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CPrintFlightSelection)
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PRINTFLIGHTSELECTION_H__BEE379D3_8844_11D6_8181_00010215BFE5__INCLUDED_)
