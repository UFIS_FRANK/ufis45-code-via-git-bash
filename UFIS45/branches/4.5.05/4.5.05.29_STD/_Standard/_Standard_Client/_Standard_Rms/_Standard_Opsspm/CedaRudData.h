//
// CedaRudData.h - rule demands - contains extra info about demands
//
#ifndef _CEDARUDDATA_H_
#define _CEDARUDDATA_H_

#include <CCSCedaData.h>
#include <CCSPtrArray.h>
#include <CedaDemandData.h>

/////////////////////////////////////////////////////////////////////////////
// Record structure declaration

struct RudDataStruct
{
	long	Urno;		// Rule demand URNO (RUDTAB.URNO)
	char	Rety[4];	// Type: where: "100"=Personnel "010"=Equipment "001"=Position
	char	Aloc[11];	// "GAT"=Gate "CIC"=CheckInCounter
	long	Urue;		// URNO of the Rule events table (links several demands together)
	long	Udgr;		// links a team of RUDs together
	long	Ulnk;		// links two (or more) RUDs together eg a personnel demand might be linked to a certain location demand
	long	Ughs;		// Urno of a SerTab's Record
	char	Reft[9];	// table and field of referenced table
	long	Vref;		// Urno of referenced table
	long	Ttgf;
	long	Ttgt;
	char	Disp[4];	// Demand sort values, where 0 = Top , 1 = 2nd ... n = last. (Order that demands are displayed on the FlightDetailDlg)
	char	Rede[33];	// Use same employee "0#1" - next / "1#0" - prev / "1#1" next and prev
	long	Unde;		// urno of next demand in dependency chain
	long	Upde;		// urno of prev demand in dependency chain
	char	Drty[2];	// Duty requirenment type 0 = turnaround, 1 = inbound, 2 = outbound
	long	Dedu;		// Duration (in sec)
	char	Rtdb[6];	// Starting Reference field
	char	Rtde[6];	// Ending Reference field
	long	Tsdb;		// Starting reference time (in sec)
	long	Tsde;		// Ending reference time (in sec)
	long	Sdti;		// Setdown time (in sec)
	long	Suti;		// Setup time (in sec)

	RudDataStruct(void)
	{
		Urno = 0L;
		memset(Rety,0,sizeof(Rety));
		memset(Aloc,0,sizeof(Aloc));
		Urue = 0L;
		Udgr = 0L;
		Ulnk = 0L;
		Ughs = 0L;
		memset(Reft,0,sizeof(Reft));
		Vref = 0L;
		Ttgf = 0L;
		Ttgt = 0L;
		memset(Disp,0,sizeof(Disp));
		memset(Rede,0,sizeof(Rede));
		Unde = 0L;
		Upde = 0L;
		memset(Drty,0,sizeof(Drty));
		Dedu = 0L;
		memset(Rtdb,0,sizeof(Rtdb));
		memset(Rtde,0,sizeof(Rtde));
		Tsdb = 0L;
		Tsde = 0L;
		Sdti = 0L;
		Suti = 0L;
	}

	RudDataStruct& RudDataStruct::operator=(const RudDataStruct& rhs)
	{
		if (&rhs != this)
		{
			Urno = rhs.Urno;
			strcpy(Rety,rhs.Rety);
			strcpy(Aloc,rhs.Aloc);
			Urue = rhs.Urue;
			Udgr = rhs.Udgr;
			Ulnk = rhs.Ulnk;
			Ughs = rhs.Ughs;
			strcpy(Reft,rhs.Reft);
			Vref = rhs.Vref;
			Ttgf = rhs.Ttgf;
			Ttgt = rhs.Ttgt;
			strcpy(Disp,rhs.Disp);
			strcpy(Rede,rhs.Rede);
			Unde = rhs.Unde;
			Upde = rhs.Upde;
			strcpy(Drty,rhs.Drty);
			Dedu = rhs.Dedu;
			strcpy(Rtdb,rhs.Rtdb);
			strcpy(Rtde,rhs.Rtde);
			Tsdb = rhs.Tsdb;
			Tsde = rhs.Tsde;
			Sdti = rhs.Sdti;
			Suti = rhs.Suti;
		}
		return *this;
	}
};

typedef struct RudDataStruct RUDDATA;

/////////////////////////////////////////////////////////////////////////////
// Class declaration

class CedaRudData: public CCSCedaData
{

// Attributes
public:
    CCSPtrArray <RUDDATA> omData;
	CMapPtrToPtr omUrnoMap;
	CMapPtrToPtr omUlnkMap;
	CMapPtrToPtr omUrueMap;
	CString GetTableName(void);
	bool bmUtplExists;

// Operations
public:
	CedaRudData();
	~CedaRudData();

	bool ReadRudData(CDWordArray &ropTplUrnos);
	RUDDATA *GetRudByUrno(long lpUrno);
	bool DemandIsOfType(long lpRudUrno, int ipDemandType);
	bool DemandIsOfType(const char *pcpRety, int ipDemandType);
	long GetUlnkByUrno(long lpUrno);
	CString Dump(long lpUrno);
	int GetRudsByUlnk(long lpUlnk,const char *pcpDemandType,CCSPtrArray<RUDDATA>& ropRuds);
	void ProcessRudBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
	int GetDemandType(const char *pcpRety);
	bool GetSameEmpDemandUrnos(long lpUrud, long lpOuri, long lpOuro, CUIntArray &ropDemUrnos, bool bpReset = false);
	bool LoadRudsForExtraDemands(CCSPtrArray <DEMANDDATA> &ropDemands, CCSPtrArray <RUDDATA> &ropRuds);
	int GetRudsByUrue(long lpUrue,CCSPtrArray<RUDDATA>& ropRuds);

private:
	RUDDATA *AddRudInternal(RUDDATA &rrpRud);
	void ClearAll();
};


extern CedaRudData ogRudData;
#endif _CEDARUDDATA_H_
