// CedaCicData.cpp - Class for common check in
//

#include <afxwin.h>
#include <ccsglobl.h>
#include <CCSPtrArray.h>
#include <CCSCedaData.h>
#include <ccsddx.h>
#include <CCSBcHandle.h>
#include <ccsddx.h>
#include <CedaCicData.h>
#include <BasicData.h>

CedaCicData::CedaCicData()
{                  
    BEGIN_CEDARECINFO(CICDATA, CicDataRecInfo)
		FIELD_LONG(Urno,"URNO")
		FIELD_CHAR_TRIM(Cnam,"CNAM")
		FIELD_DATE(Vafr,"VAFR")
		FIELD_DATE(Vato,"VATO")
		FIELD_DATE(Nafr,"NAFR")
		FIELD_DATE(Nato,"NATO")
		FIELD_CHAR_TRIM(Tele,"TELE")
		FIELD_CHAR_TRIM(Term,"TERM")
		FIELD_CHAR_TRIM(Hall,"HALL")
		FIELD_CHAR_TRIM(Region,"CICR")
		FIELD_CHAR_TRIM(Line,"CICL")
    END_CEDARECINFO

    // Copy the record structure
    for (int i = 0; i < sizeof(CicDataRecInfo)/sizeof(CicDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&CicDataRecInfo[i],sizeof(CEDARECINFO));
        omRecInfo.Add(prpCedaRecInfo);
	}

    // Initialize table names and field names
    strcpy(pcmTableName,"CICTAB");
    pcmFieldList = "URNO,CNAM,VAFR,VATO,NAFR,NATO,TELE,TERM,HALL,CICR,CICL";
}
 
CedaCicData::~CedaCicData()
{
	TRACE("CedaCicData::~CedaCicData called\n");
	ClearAll();
}

void CedaCicData::ClearAll()
{
	omUrnoMap.RemoveAll();
	omNameMap.RemoveAll();
	omData.DeleteAll();

	CMapPtrToPtr *polSingleMap;
	CString olHall;
	for(POSITION rlPos =  omHallMap.GetStartPosition(); rlPos != NULL; )
	{
		omHallMap.GetNextAssoc(rlPos,olHall,(void *& )polSingleMap);
		polSingleMap->RemoveAll();
		delete polSingleMap;
	}
	omHallMap.RemoveAll();

	CString olRegion;
	for (rlPos = omRegionMap.GetStartPosition(); rlPos != NULL;)
	{
		omRegionMap.GetNextAssoc(rlPos,olRegion,(void *& )polSingleMap);
		polSingleMap->RemoveAll();
		delete polSingleMap;
	}

	omRegionMap.RemoveAll();

	CString olLine;
	for (rlPos = omLineMap.GetStartPosition(); rlPos != NULL;)
	{
		omLineMap.GetNextAssoc(rlPos,olLine,(void *& )polSingleMap);
		polSingleMap->RemoveAll();
		delete polSingleMap;
	}

	omLineMap.RemoveAll();
}

CCSReturnCode CedaCicData::ReadCicData()
{
	CCSReturnCode ilRc = RCSuccess;
	ClearAll();

	char pclCom[10] = "RT";
    char pclWhere[512];
    strcpy(pclWhere,"");
    if ((ilRc = CedaAction2(pclCom, pclWhere)) != RCSuccess)
	{
		ogBasicData.LogCedaError("CedaCicData Error",omLastErrorMessage,pclCom,pcmFieldList,pcmTableName,pclWhere);
	}
	else
	{
		CICDATA rlCicData;
		for (int ilLc = 0; ilRc == RCSuccess; ilLc++)
		{
			if ((ilRc = GetBufferRecord2(ilLc,&rlCicData)) == RCSuccess)
			{
				AddCicInternal(rlCicData);
			}
		}
		ilRc = RCSuccess;
	}

	ogBasicData.Trace("CedaCicData Read %d Records (Rec Size %d) %s",omData.GetSize(),sizeof(CICDATA), pclWhere);
    return ilRc;
}


void CedaCicData::AddCicInternal(CICDATA &rrpCic)
{
	CICDATA *prlCic = new CICDATA;
	*prlCic = rrpCic;
	omData.Add(prlCic);
	omUrnoMap.SetAt((void *)prlCic->Urno,prlCic);
	omNameMap.SetAt(prlCic->Cnam,prlCic);
	AddCicToHallMap(prlCic);
	AddCicToRegionMap(prlCic);
	AddCicToLineMap(prlCic);
}

void CedaCicData::AddCicToHallMap(CICDATA *prpCic)
{
	CMapPtrToPtr *polSingleMap;
	if(omHallMap.Lookup(prpCic->Hall, (void *&) polSingleMap))
	{
		polSingleMap->SetAt((void *)prpCic->Urno,prpCic);
	}
	else
	{
		polSingleMap = new CMapPtrToPtr;
		polSingleMap->SetAt((void *)prpCic->Urno,prpCic);
		omHallMap.SetAt(prpCic->Hall,polSingleMap);
	}
}

void CedaCicData::AddCicToRegionMap(CICDATA *prpCic)
{
	CMapPtrToPtr *polSingleMap;
	if(omRegionMap.Lookup(prpCic->Region, (void *&) polSingleMap))
	{
		polSingleMap->SetAt((void *)prpCic->Urno,prpCic);
	}
	else
	{
		polSingleMap = new CMapPtrToPtr;
		polSingleMap->SetAt((void *)prpCic->Urno,prpCic);
		omRegionMap.SetAt(prpCic->Region,polSingleMap);
	}
}

void CedaCicData::AddCicToLineMap(CICDATA *prpCic)
{
	CMapPtrToPtr *polSingleMap;
	if(omLineMap.Lookup(prpCic->Line, (void *&) polSingleMap))
	{
		polSingleMap->SetAt((void *)prpCic->Urno,prpCic);
	}
	else
	{
		polSingleMap = new CMapPtrToPtr;
		polSingleMap->SetAt((void *)prpCic->Urno,prpCic);
		omLineMap.SetAt(prpCic->Line,polSingleMap);
	}
}

CICDATA* CedaCicData::GetCicByUrno(long lpUrno)
{
	CICDATA *prlCic = NULL;
	omUrnoMap.Lookup((void *)lpUrno, (void *&) prlCic);
	return prlCic;
}

CICDATA* CedaCicData::GetCicByName(const char *pcpName)
{
	CICDATA *prlCic = NULL;
	omNameMap.Lookup(pcpName, (void *&) prlCic);
	return prlCic;
}


long CedaCicData::GetCicUrnoByName(const char *pcpName)
{
	long llUrno = 0L;
	CICDATA *prlCic = GetCicByName(pcpName);
	if(prlCic != NULL)
	{
		llUrno = prlCic->Urno;
	}
	return llUrno;
}

CString CedaCicData::GetTableName(void)
{
	return CString(pcmTableName);
}

void CedaCicData::GetHalls(CStringArray &ropHalls)
{
	CMapPtrToPtr *polSingleMap;
	CString olHall;
	for(POSITION rlPos = omHallMap.GetStartPosition(); rlPos != NULL; )
	{
		omHallMap.GetNextAssoc(rlPos,olHall,(void *& )polSingleMap);
		ropHalls.Add(olHall);
	}
}

void CedaCicData::GetLines(CStringArray &ropLines)
{
	CMapPtrToPtr *polSingleMap;
	CString olLine;
	for(POSITION rlPos = omLineMap.GetStartPosition(); rlPos != NULL; )
	{
		omLineMap.GetNextAssoc(rlPos,olLine,(void *& )polSingleMap);
		ropLines.Add(olLine);
	}
}

void CedaCicData::GetRegions(CStringArray &ropRegions)
{
	CMapPtrToPtr *polSingleMap;
	CString olRegion;
	for(POSITION rlPos = omRegionMap.GetStartPosition(); rlPos != NULL; )
	{
		omRegionMap.GetNextAssoc(rlPos,olRegion,(void *& )polSingleMap);
		ropRegions.Add(olRegion);
	}
}

void CedaCicData::GetCicsByHall(CCSPtrArray <CICDATA> &ropCics, CString opHall, bool bpReset /* = true */)
{
	if(bpReset)
	{
	    ropCics.RemoveAll();
	}

	CMapPtrToPtr *polSingleMap;
	long llUrno;
	CICDATA *prlCic;

	if(omHallMap.Lookup(opHall,(void *& )polSingleMap))
	{
		for(POSITION rlPos = polSingleMap->GetStartPosition(); rlPos != NULL; )
		{
			polSingleMap->GetNextAssoc(rlPos,(void *& ) llUrno,(void *& )prlCic);
			ropCics.Add(prlCic);
		}
	}
}

void CedaCicData::GetCicsByLine(CCSPtrArray <CICDATA> &ropCics, CString opLine, bool bpReset /* = true */)
{
	if(bpReset)
	{
	    ropCics.RemoveAll();
	}

	CMapPtrToPtr *polSingleMap;
	long llUrno;
	CICDATA *prlCic;

	if(omLineMap.Lookup(opLine,(void *& )polSingleMap))
	{
		for(POSITION rlPos = polSingleMap->GetStartPosition(); rlPos != NULL; )
		{
			polSingleMap->GetNextAssoc(rlPos,(void *& ) llUrno,(void *& )prlCic);
			ropCics.Add(prlCic);
		}
	}
}

void CedaCicData::GetCicsByRegion(CCSPtrArray <CICDATA> &ropCics, CString opRegion, bool bpReset /* = true */)
{
	if(bpReset)
	{
	    ropCics.RemoveAll();
	}

	CMapPtrToPtr *polSingleMap;
	long llUrno;
	CICDATA *prlCic;

	if(omRegionMap.Lookup(opRegion,(void *& )polSingleMap))
	{
		for(POSITION rlPos = polSingleMap->GetStartPosition(); rlPos != NULL; )
		{
			polSingleMap->GetNextAssoc(rlPos,(void *& ) llUrno,(void *& )prlCic);
			ropCics.Add(prlCic);
		}
	}
}

void CedaCicData::GetAllCics(CStringArray& ropCics, bool bpReset /*=true*/)
{
	if(bpReset)
	{
		ropCics.RemoveAll();
	}

	int ilNumCics = omData.GetSize();
	for(int ilLc = 0; ilLc < ilNumCics; ilLc++)
	{
		ropCics.Add(CString(omData[ilLc].Cnam));
	}
}
