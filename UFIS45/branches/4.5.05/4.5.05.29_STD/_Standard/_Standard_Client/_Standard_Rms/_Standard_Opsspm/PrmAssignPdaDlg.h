#if !defined(AFX_PRMASSIGNPDADLG_H__D70F1283_AC51_11D7_8259_00010215BFE5__INCLUDED_)
#define AFX_PRMASSIGNPDADLG_H__D70F1283_AC51_11D7_8259_00010215BFE5__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// PrmAssignPdaDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// PrmAssignPdaDlg dialog

class PrmAssignPdaDlg : public CDialog
{
// Construction
public:
	PrmAssignPdaDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(PrmAssignPdaDlg)
	enum { IDD = IDD_PRMASSIGNPDA_DLG };
	BOOL	bmShowAllCheckBox;
	CTime		m_FromDate;
	CTime		m_FromTime;
	CTime		m_ToDate;
	CTime		m_ToTime;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(PrmAssignPdaDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

public:
	
	void InitDefaultValues(long lpStfUrno);

	long lmUstf;

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(PrmAssignPdaDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	afx_msg void OnShowAll();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	bool m_ShowAllPdas;


};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PRMASSIGNPDADLG_H__D70F1283_AC51_11D7_8259_00010215BFE5__INCLUDED_)
