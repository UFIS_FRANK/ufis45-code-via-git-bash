// FlPlan.cpp : implementation file
//

#include <stdafx.h>
#include <ccsglobl.h>
#include <CCSPtrArray.h>
#include <OpssPm.h>
#include <CCSCedaCom.h>
#include <AllocData.h>
#include <CedaEmpData.h>
#include <CedaJobData.h>
#include <CedaDemandData.h>
#include <CedaFlightData.h>
#include <table.h>
#include <FlightPlan.h>

#include <cviewer.h>
#include <FilterPage.h>
#include <ZeitPage.h>
#include <FlightTableBoundFilterPage.h>
#include <FlightTableSortPage.h>
#include <BasePropertySheet.h>
#include <FlightTablePropertySheet.h>
#include <BasicData.h>

#include <CCSDragDropCtrl.h>
#include <FlightDetailWindow.h>
#include <dataset.h>
#include <ccsddx.h>
#include <CedaCfgData.h>

#include <cxbutton.h>
#include <CedaShiftData.h>
#include <CCIDiagram.h>
#include <CCITable.h>
#include <ConflictTable.h>
#include <ConflictConfigTable.h>
#include <StaffDiagram.h>
#include <GateDiagram.h>
#include <RegnDiagram.h>
#include <StaffTable.h>
#include <GateTable.h>
#include <PrePlanTable.h>
#include <ButtonList.h>
#include <SetDisplayTimeframeDlg.h> // Singapore

extern CStringArray ogFlightAlcd;


/////////////////////////////////////////////////////////////////////////////
// FlightPlan dialog

FlightPlan::FlightPlan(CWnd* pParent /*=NULL*/, BOOL bpIsFromSearch,BOOL bpDetailDisplay)
: CDialog(FlightPlan::IDD, pParent)
{
	m_StartDate = TIMENULL;
	bmNoUpdatesNow = TRUE;
	omViewer.AllowUpdates(bmNoUpdatesNow);


	//{{AFX_DATA_INIT(FlightPlan)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	bmIsViewOpen = FALSE;
	bmContextItem = FALSE;
}

FlightPlan::FlightPlan(CWnd* pParent, CTime StartTime, CTime EndTime,BOOL bpIsFromSearch,BOOL bpDetailDisplay)
: CDialog(FlightPlan::IDD, pParent)
{
	bmNoUpdatesNow = TRUE;
	omViewer.AllowUpdates(bmNoUpdatesNow);
	omViewer.omStartTime = StartTime;
	omViewer.omEndTime = EndTime;
	m_StartDate = StartTime;

	pomFlightTable = new CTable;
    pomFlightTable->tempFlag = 2;
	pomFlightTable->SetSelectMode(0);

    CDialog::Create(FlightPlan::IDD);  
    CRect olRect;
	ogBasicData.GetWindowPosition(olRect,ogCfgData.rmUserSetup.FPTB,ogCfgData.rmUserSetup.MONS);
	olRect.top += 40;
	BOOL blMinimized = FALSE;
	ogBasicData.GetDialogFromReg(olRect, COpssPmApp::FLIGHTPLAN_TABLE_WINDOWPOS_REG_KEY,blMinimized);	

	//Multiple monitor setup
	ogBasicData.GetWindowPositionCorrect(olRect);

    MoveWindow(&olRect);
	bmIsViewOpen = FALSE;
	bmContextItem = FALSE;
	

    CRect rect;
    GetClientRect(&rect);
    rect.OffsetRect(0, m_nDialogBarHeight);
    rect.InflateRect(1, 1);     // hiding the CTable window border
    pomFlightTable->SetTableData(this, rect.left, rect.right, rect.top, rect.bottom);

	omViewer.Attach(pomFlightTable);
	OnSelchangeDate();
	if(blMinimized == TRUE)
	{
		ShowWindow(SW_MINIMIZE);
	}

	pomTemplate = NULL; //Singpaore
	pomPrintPreviewView = NULL; //Singapore

}

FlightPlan::~FlightPlan()
{
	TRACE("FlightPlan::~FlightPlan()\n");
	delete pomFlightTable;	
}

void FlightPlan::SetCaptionText(void)
{
	CString olCaptionText;
	olCaptionText.Format(GetString(IDS_STRING61308),omViewer.omStartTime.Format("%d/%H%M"),omViewer.omEndTime.Format("%H%M "));
	SetWindowText(olCaptionText);
}

void FlightPlan::UpdateView()
{
	CString  olViewName = omViewer.GetViewName();
	
	if (olViewName.IsEmpty() == TRUE)
	{	
		olViewName = ogCfgData.rmUserSetup.FPLV;
	}


	AfxGetApp()->DoWaitCursor(1);
    omViewer.ChangeViewTo(olViewName);
	AfxGetApp()->DoWaitCursor(-1);
	SetCaptionText();

	//Singapore
	OnTableUpdateDataCount();
	// Id 24-Sep-96
	// This will set the keyboard focus back to the list-box after this button was clicked.
	//pomFlightTable->GetCTableListBox()->SetFocus();
}

void FlightPlan::UpdateComboBox()
{
	CComboBox *polCB = (CComboBox *)GetDlgItem(IDC_COMBO2);
	polCB->ResetContent();
	CStringArray olStrArr;
	omViewer.GetViews(olStrArr);
	for (int ilIndex = 0; ilIndex < olStrArr.GetSize(); ilIndex++)
	{
		polCB->AddString(olStrArr[ilIndex]);
	}
	CString olViewName = omViewer.GetViewName();
	if (olViewName.IsEmpty() == TRUE)
	{
		olViewName = ogCfgData.rmUserSetup.FPLV;
	}
	ilIndex = polCB->FindString(-1,olViewName);
		
	if (ilIndex != CB_ERR)
	{
		polCB->SetCurSel(ilIndex);
	}
}

void FlightPlan::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(FlightPlan)
	DDX_Control(pDX, IDC_DATE, m_Date);
	DDX_Control(pDX, IDC_FLP_SETTIME ,m_bSetTime);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(FlightPlan, CDialog)
	//{{AFX_MSG_MAP(FlightPlan)
	ON_WM_DESTROY()
	ON_WM_SIZE()
	ON_WM_MOVE() //PRF 8712
	ON_BN_CLICKED(IDC_VIEW, OnView)
	ON_CBN_SELCHANGE(IDC_COMBO2, OnSelchangeCombo2)
	ON_CBN_CLOSEUP(IDC_COMBO2, OnCloseupCombo2)
	ON_BN_CLICKED(IDC_PRINT, OnPrint)
	ON_BN_CLICKED(IDC_BUTTON_PRINT_PREVIEW,OnPrintPreview) //Singapore
	ON_MESSAGE(WM_BEGIN_PRINTING, OnBeginPrinting) //Singapore
	ON_MESSAGE(WM_END_PRINTING, OnEndPrinting) //Singapore
	ON_MESSAGE(WM_PRINT_PREVIEW, PrintPreview) //Singapore
	ON_MESSAGE(WM_PREVIEW_PRINT, OnPreviewPrint) //Singapore
    ON_MESSAGE(WM_TABLE_LBUTTONDBLCLK, OnTableLButtonDblclk)
    ON_MESSAGE(WM_TABLE_RBUTTONDOWN, OnTableRButtonDown)
	ON_COMMAND(11, OnMenuWorkOn)
    ON_MESSAGE(WM_DRAGOVER, OnDragOver)
    ON_MESSAGE(WM_DROP, OnDrop)  
	ON_WM_CLOSE()
	ON_CBN_SELCHANGE(IDC_DATE, OnSelchangeDate)
    ON_MESSAGE(WM_TABLE_DRAGBEGIN, OnTableDragBegin)
	ON_MESSAGE(WM_TABLE_UPDATE_DATACOUNT,OnTableUpdateDataCount) //Singapore
	ON_BN_CLICKED(IDC_FLP_SETTIME, OnSetTime) //Singapore
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// FlightPlan message handlers

BOOL FlightPlan::OnInitDialog() 
{
	CDialog::OnInitDialog();

	CWnd *polWnd = GetDlgItem(IDC_VIEW); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61840));
	}
	polWnd = GetDlgItem(IDC_PRINT); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61844));
	}
	polWnd = GetDlgItem(IDC_TAG_FLIGHTTABLE); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING32976));
	}

	//Singapore
	if(ogBasicData.IsPaxServiceT2() == false)
	{
		CWnd* polWnd = GetDlgItem(IDC_DATACOUNT);
		if(polWnd != NULL)
		{
			polWnd->ShowWindow(SW_HIDE);
		}
	}

	char pclTmpText[512];
	char pclConfigPath[512];
	if (getenv("CEDA") == NULL)
	{	
		strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
	}
	else
	{
		strcpy(pclConfigPath, getenv("CEDA"));
	}

	GetPrivateProfileString(pcgAppName, "FLIGHT_PLAN_SETTIME_BTN",  "NO",pclTmpText, sizeof pclTmpText, pclConfigPath);
	if (stricmp(pclTmpText,"YES")==0)
	{		
		m_bSetTime.ShowWindow(SW_SHOW);
	}
	else
	{
		m_bSetTime.ShowWindow(SW_HIDE);
	}

	//check config file, set the visible of print preview button
	polWnd = GetDlgItem(IDC_BUTTON_PRINT_PREVIEW);
	polWnd->ShowWindow(FALSE);
	if(IsPrivateProfileOn("FLIGHT_PLAN_PRINTPREVIW_BTN","NO"))
	{		
		if(polWnd != NULL)
		{
			polWnd->ShowWindow(TRUE);
		}
	}

	UpdateComboBox();

    CRect rect;
    GetClientRect(&rect);
    m_nDialogBarHeight = rect.bottom - rect.top;

    // extend dialog window to current screen width
    rect.left = 10;
    rect.top = 56;
    rect.right = ::GetSystemMetrics(SM_CXSCREEN) - 10;
    rect.bottom = ::GetSystemMetrics(SM_CYSCREEN) - 10;
    //MoveWindow(&rect);  //Commented - Singapore PRF 8712

	m_DragDropTarget.RegisterTarget(this, this);

	TRACE("FlightPlan: DDX Registration\n");
	ogCCSDdx.Register(this, REDISPLAY_ALL, CString("FLPLAN"),CString("Redisplay all"), FlightScheduleCf);
	ogCCSDdx.Register(this, GLOBAL_DATE_UPDATE, CString("FLPLAN"),CString("Global Date Change"), FlightScheduleCf);


	ogBasicData.SetWindowStat("FLIGHTPLAN IDC_COMBO1",GetDlgItem(IDC_COMBO1));
	ogBasicData.SetWindowStat("FLIGHTPLAN IDC_VIEW",GetDlgItem(IDC_VIEW));
	ogBasicData.SetWindowStat("FLIGHTPLAN IDC_TAG_FLIGHTTABLE",GetDlgItem(IDC_TAG_FLIGHTTABLE));
	ogBasicData.SetWindowStat("FLIGHTPLAN IDC_PRINT",GetDlgItem(IDC_PRINT));


	CStringArray olTimeframeList;
	ogBasicData.GetTimeframeList(olTimeframeList);
	int ilNumDays = olTimeframeList.GetSize();
	for(int ilDay = 0; ilDay < ilNumDays; ilDay++)
	{
		m_Date.AddString(olTimeframeList[ilDay]);
	}
	if(ilNumDays > 0)
	{
		if(m_StartDate == TIMENULL)
		{
			m_Date.SetCurSel(ogBasicData.GetDisplayDateOffset());
		}
		else
		{
			m_Date.SetCurSel(ogBasicData.GetDisplayDateOffsetByDate(m_StartDate));
		}
		SetViewerDate();
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void FlightPlan::OnDestroy() 
{
	if (bgModal == TRUE)
		return;
	ogBasicData.WriteDialogToReg(this,COpssPmApp::FLIGHTPLAN_TABLE_WINDOWPOS_REG_KEY,omWindowRect,this->IsIconic());
	bmNoUpdatesNow = TRUE;
	omViewer.AllowUpdates(bmNoUpdatesNow);

	// Unregister DDX call back function
	TRACE("FlightPlan: DDX Unregistration %s\n",
		::IsWindow(GetSafeHwnd())? "": "(window is already destroyed)");
    ogCCSDdx.UnRegister(this, NOTUSED);

	CDialog::OnDestroy();
}

void FlightPlan::OnCancel() 
{
	// Normally, we should just call CDialog::OnCancel().
	// However, the DestroyWindow() is required since this table can be destroyed in two ways.
	// First is by pressing ESC key which will call this routine.
	// Second, it may be destroyed directly by calling Manfred's DestroyWindow() in this class
	// which is not so compatible with MFC.
	//hag000413 AfxGetMainWnd()->SendMessage(WM_FLIGHTTABLE_EXIT);

	for ( int ilFP = 0; ilFP < pogButtonList->omFlightPlanArray.GetSize(); ilFP++)
	{
		if(pogButtonList->omFlightPlanArray[ilFP] == (void *)this)
		{
			pogButtonList->omFlightPlanArray.RemoveAt(ilFP);
			break;
		}
	}	

	pogButtonList->SendMessage(WM_FLIGHTTABLE_EXIT);	//hag000413
	DestroyWindow();
}

void FlightPlan::OnSize(UINT nType, int cx, int cy) 
{
	CDialog::OnSize(nType, cx, cy);
	
	// TODO: Add your message handler code here
   if (nType != SIZE_MINIMIZED)
   {
        pomFlightTable->SetPosition(-1, cx+1, m_nDialogBarHeight-1, cy+1);
        GetWindowRect(&omWindowRect); //PRF 8712
   }
}

void FlightPlan::OnView() 
{
	// Id 24-Sep-96
	// This will set the keyboard focus back to the list-box after this button was clicked.
	//pomFlightTable->GetCTableListBox()->SetFocus();

	FlightTablePropertySheet dialog(this, &omViewer);
	bmIsViewOpen = TRUE;
	if (dialog.DoModal() != IDCANCEL)
		UpdateView();
	bmIsViewOpen = FALSE;
	UpdateComboBox();
}

void FlightPlan::OnSelchangeCombo2() 
{
    char clText[64];
    CComboBox *polCB = (CComboBox *)GetDlgItem(IDC_COMBO2);
    polCB->GetLBText(polCB->GetCurSel(), clText);
    TRACE("PrePlanTable::OnComboBox() [%s]", clText);
	omViewer.SelectView(clText);
	UpdateView();	
}

void FlightPlan::OnCloseupCombo2() 
{
	// Id 24-Sep-96
	// This will set the keyboard focus back to the list-box when the combo-box is closed.
    //pomFlightTable->GetCTableListBox()->SetFocus();
}

void FlightPlan::OnPrint() 
{
	// Id 24-Sep-96
	// This will set the keyboard focus back to the list-box after this button was clicked.
	//pomFlightTable->GetCTableListBox()->SetFocus();

	omViewer.PrintView();	
}

//Nyan OnSetTime()
void FlightPlan::OnSetTime() 
{
	CSetDisplayTimeframeDlg olDlg;
	olDlg.omFrom = omViewer.omStartTime;
	olDlg.omTo = omViewer.omEndTime;
	olDlg.omFromReadOnly = false;
	olDlg.omToReadOnly = false;
	if(olDlg.DoModal() == IDOK)
	{
		omViewer.omStartTime = olDlg.omFrom;
		omViewer.omEndTime = olDlg.omTo	;
		UpdateView();
	}
}
void FlightPlan::OnPrintPreview()
{
	

		if (!pomTemplate)
		{
			pomTemplate = new CSingleDocTemplate(
				IDR_MENU_PRINT_PREVIEW,
				NULL,
				RUNTIME_CLASS(CFrameWnd),
				RUNTIME_CLASS(CPrintPreviewView));
			AfxGetApp()->AddDocTemplate(pomTemplate);
		}

		CFrameWnd * pFrameWnd = pomTemplate->CreateNewFrame( NULL,NULL);	
		pomTemplate->InitialUpdateFrame( pFrameWnd, NULL, FALSE);
		pomPrintPreviewView =(CPrintPreviewView*)pFrameWnd->GetActiveView();
		pomPrintPreviewView->pomCallMsgWindow=this;	
		pomPrintPreviewView->SetFrameWindow(pFrameWnd);
		pFrameWnd->SetWindowText(_T("Preplan Print Preview"));	
		pomPrintPreviewView->OnFilePrintPreview();	
		return;
		
		//PrintData();
}
void FlightPlan::OnBeginPrinting(WPARAM wParam, LPARAM lParam)
{
	PRINT_PREVIEW_DCINFO* polPrintPreviewDcInfo = (PRINT_PREVIEW_DCINFO*)lParam;
	if (!polPrintPreviewDcInfo->pomDC|| !polPrintPreviewDcInfo->pomPrintInfo) return;

	CString omTarget;
	CTime olPrintStart = (ogBasicData.GetTimeframeStart()  > omViewer.omStartTime) ? ogBasicData.GetTimeframeStart() : omViewer.omStartTime;
	CTime olPrintEnd = (ogBasicData.GetTimeframeEnd()  < omViewer.omEndTime) ? ogBasicData.GetTimeframeEnd() : omViewer.omEndTime;
	omTarget.Format(GetString(IDS_STRING61285),"",olPrintStart.Format("%d.%m.%Y %H:%M"),olPrintEnd.Format("%d.%m.%Y %H:%M"));
	omTarget += GetString(IDS_STRING61292) + ogCfgData.rmUserSetup.FPLV;		
	CString olPrintDate = ogBasicData.GetTime().Format("%d.%m.%Y");

	omViewer.GetCCSPrinter() = 
		new CCSPrint(this,PRINT_LANDSCAPE,60,450,50,GetString(IDS_STRING61364),olPrintDate,omTarget);
	//}
	omViewer.GetCCSPrinter()->pomCdc = polPrintPreviewDcInfo->pomDC;
	omViewer.GetCCSPrinter()->InitializePrintSetup(CCSPrint::PRINT_PREVIEW);

	//reduce one line of table name
	omViewer.GetCCSPrinter()->imMaxLines=omViewer.GetCCSPrinter()->imMaxLines -1;
	//set max page
	int ilNoOfPages = (
		omViewer.GetLines().GetSize() % omViewer.GetCCSPrinter()->imMaxLines) == 0 
		? 
		(omViewer.GetLines().GetSize() / omViewer.GetCCSPrinter()->imMaxLines) 
		: 
	(omViewer.GetLines().GetSize() / omViewer.GetCCSPrinter()->imMaxLines) + 1;
	//add 1, as need count line of table name
	polPrintPreviewDcInfo->pomPrintInfo->SetMaxPage(ilNoOfPages);
	polPrintPreviewDcInfo->pomPrintInfo->m_nCurPage = 1;
	bgPrintPreviewMode = TRUE;
	pogPreviewPrintWnd = this;
	pomPrintPreviewView->GetFrameWindow()->BeginModalState();
}

void FlightPlan::PrintPreview(WPARAM wParam, LPARAM lParam)
{
	//CPrintPreviewView::OnPrint send msg , include parameters of print preview device info
	//set param to current viewer's printer
	PRINT_PREVIEW_DCINFO* polPrintPreviewDcInfo = (PRINT_PREVIEW_DCINFO*)lParam;
	if (!polPrintPreviewDcInfo->pomDC|| !polPrintPreviewDcInfo->pomPrintInfo) return;

	int aa=polPrintPreviewDcInfo->pomDC->GetDeviceCaps(HORZSIZE);
	omViewer.GetCCSPrinter()->pomCdc = polPrintPreviewDcInfo->pomDC;
	omViewer.GetCCSPrinter()->InitializePrintSetup(CCSPrint::PRINT_PREVIEW);
	//omViewer.GetCCSPrinter()->pomCdc->SetMapMode(MM_ANISOTROPIC);	
	omViewer.GetCCSPrinter()->pomCdc->SetWindowExt(
			omViewer.GetCCSPrinter()->pomCdc->GetDeviceCaps(HORZRES)*1.1,
			omViewer.GetCCSPrinter()->pomCdc->GetDeviceCaps(VERTRES)*1.1);
	omViewer.GetCCSPrinter()->pomCdc->SetViewportExt(
			omViewer.GetCCSPrinter()->pomCdc->GetDeviceCaps(HORZRES),
			omViewer.GetCCSPrinter()->pomCdc->GetDeviceCaps(VERTRES));	

	omViewer.PrintPreview(polPrintPreviewDcInfo->pomPrintInfo->m_nCurPage);
}

void FlightPlan::OnPreviewPrint(WPARAM wParam, LPARAM lParam)
{
	omViewer.PrintView();
}

void FlightPlan::OnEndPrinting(WPARAM wParam, LPARAM lParam)
{	
	pomPrintPreviewView->GetFrameWindow()->EndModalState();
	//if(myFlightScheduleViewer->GetCCSPrinter() != NULL)
	{
	//	delete myFlightScheduleViewer->GetCCSPrinter();
	}
//	omBothViewer.Attach(NULL);
	pomPrintPreviewView = NULL;
	//myFlightScheduleViewer = NULL;
	bgPrintPreviewMode = FALSE;
}


LONG FlightPlan::OnTableLButtonDblclk(UINT ipItem, LONG lpLParam)
{
	FLIGHTSCHEDULE_LINEDATA *prlLine = (FLIGHTSCHEDULE_LINEDATA *)pomFlightTable->GetTextLineData(ipItem);
	if (prlLine != NULL)
	{
		new FlightDetailWindow(this,prlLine->ArrUrno,prlLine->DepUrno);
	}
	return 0L;
}

LONG FlightPlan::OnTableRButtonDown(UINT ipItem, LONG lpLParam)
{
	CMenu menu;
	CPoint olPoint(LOWORD(lpLParam),HIWORD(lpLParam));

	FLIGHTSCHEDULE_LINEDATA *prlLine = (FLIGHTSCHEDULE_LINEDATA *)pomFlightTable->GetTextLineData(ipItem);
	if (prlLine != NULL)
	{
		omContextArrUrno = prlLine->ArrUrno;
		omContextDepUrno = prlLine->DepUrno;
		bmContextItem = TRUE;

		int ilLc;
		
		menu.CreatePopupMenu();
		for (ilLc = menu.GetMenuItemCount(); --ilLc >= 0;)
			menu.RemoveMenu(ilLc, MF_BYPOSITION);
		//menu.AppendMenu(MF_STRING,11,"Bearbeiten");	
		menu.AppendMenu(MF_STRING,11,GetString(IDS_STRING61352));	

		ClientToScreen( &olPoint);
		menu.TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, olPoint.x, olPoint.y + 55, this, NULL);
	}
	else
		bmContextItem = FALSE;

	return 0L;
}

void FlightPlan::OnMenuWorkOn()
{
	if (bmContextItem)
	{
		new FlightDetailWindow(this, omContextArrUrno, omContextDepUrno);
	}

	bmContextItem = FALSE;
}

BOOL FlightPlan::DestroyWindow() 
{
	if ((bmIsViewOpen) || (bgModal == TRUE))
	{
		MessageBeep((UINT)-1);
		return FALSE;    // don't destroy window while view property sheet is still open
	}

	bmNoUpdatesNow = TRUE;
	omViewer.AllowUpdates(bmNoUpdatesNow);

	BOOL blRc = CDialog::DestroyWindow();
	delete this;
	return blRc;
}

////////////////////////////////////////////////////////////////////////
// FlightPlan -- implementation of DDX call back function

void FlightPlan::FlightScheduleCf(void *popInstance, int ipDDXType,void *vpDataPointer, CString &ropInstanceName)
{
	FlightPlan *polTable = (FlightPlan *)popInstance;

	if (ipDDXType == REDISPLAY_ALL)
	{
		polTable->UpdateView();
		polTable->SetCaptionText();
	}
	else if(ipDDXType == GLOBAL_DATE_UPDATE)
	{
		polTable->HandleGlobalDateUpdate();
	}
}

//*****
/////////////////////////////////////////////////////////////////////////////
// FlightPlan -- drag-and-drop functionalities
//
// The user may drag from:
//	- any non-empty line for create a JobFlight.
//
// and the user will have opportunities to:
//	-- drop from StaffTable (single or multiple staff shift) onto any non-empty line.
//		We will create a normal flight job for that staff.
//	-- drop from DutyBar onto any non-empty lines.
//		We will create a normal flight job for that staff.
//
LONG FlightPlan::OnDragOver(UINT wParam, LONG lParam)
{
    int ilClass = m_DragDropTarget.GetDataClass(); 
	if (ilClass != DIT_DUTYBAR)
		return -1L;	// cannot interpret this object

	JOBDATA *prlPoolJob = ogJobData.GetJobByUrno(m_DragDropTarget.GetDataDWord(0));
	if (prlPoolJob == NULL)
		return -1L;

	CPoint olDropPosition;
    ::GetCursorPos(&olDropPosition);
	imDropLineno = pomFlightTable->GetLinenoFromPoint(olDropPosition);
	if (!(0 <= imDropLineno && imDropLineno <= omViewer.omLines.GetSize()-1))
		return -1L;	// cannot drop on a blank line

	// check if the user drop into a blank part of the line
	int ilDropColumnno = pomFlightTable->GetColumnnoFromPoint(olDropPosition);
	if (ilDropColumnno == -1L)	// drop on an invalid column?
		return -1L;
	imDropFlightUrno = (ilDropColumnno <= 10)?	// drop into an arrival flight?
		omViewer.omLines[imDropLineno].ArrUrno:
		omViewer.omLines[imDropLineno].DepUrno;
	if (imDropFlightUrno == -1L)
		return -1L;
	omDropFlightAlid = (ilDropColumnno <= 10)?	// drop into an arrival flight?
		omViewer.omLines[imDropLineno].Alida:
		omViewer.omLines[imDropLineno].Alidd;

	if(!ogBasicData.IsInPool(ALLOCUNITTYPE_GATE,omDropFlightAlid,prlPoolJob->Alid))
		return -1L;

	return 0L;
}

LONG FlightPlan::OnDrop(UINT wParam, LONG lParam)
{
	// create job flight from duty without
//	CDWordArray olDutyUrnos;
//	olDutyUrnos.Add(m_DragDropTarget.GetDataDWord(0));
//
//	JOBDATA *prlPoolJob = ogJobData.GetJobByUrno(m_DragDropTarget.GetDataDWord(0));
//	if (prlPoolJob == NULL)
//		return -1L;

	if (imDropFlightUrno == -1L)	// invalid flight number
		return -1L;

	if (!(0 <= imDropLineno && imDropLineno <= omViewer.omLines.GetSize()-1))
		return -1L;	// cannot drop on a blank line
	//ogDataSet.CreateJobFlightFromDuty(this, olDutyUrnos, imDropFlightUrno, ALLOCUNITTYPE_GATE, ogFlightData.GetGate(imDropFlightUrno),FALSE);

	CDWordArray olPoolJobUrnos;
	int ilDataCount = m_DragDropTarget.GetDataCount() - 1;
	if(ilDataCount > 0)
	{
		for(int ilPoolJob = 0; ilPoolJob < ilDataCount; ilPoolJob++)
		{
			long llPoolJobUrno = m_DragDropTarget.GetDataDWord(ilPoolJob);
			olPoolJobUrnos.Add(llPoolJobUrno);
		}

		int ilReturnFlightType = ID_NOT_RETURNFLIGHT;
		if(omViewer.omLines[imDropLineno].ArrUrno == imDropFlightUrno)
			ilReturnFlightType = omViewer.omLines[imDropLineno].ArrReturnFlightType;
		else
			ilReturnFlightType = omViewer.omLines[imDropLineno].DepReturnFlightType;

		bool blTeamAlloc = m_DragDropTarget.GetDataDWord(ilDataCount) == 1 ? true : false; // assigning a whole team ?
		ogDataSet.CreateJobFlightFromDuty(this, olPoolJobUrnos, imDropFlightUrno, ALLOCUNITTYPE_GATE, ogFlightData.GetGate(imDropFlightUrno), 
											blTeamAlloc,  NULL, TIMENULL, TIMENULL, true, 0L, false, false, 0L, ilReturnFlightType);
	}

	return 0L;
}

void FlightPlan::OnClose() 
{
/*	for ( int ilFP = 0; ilFP < pogButtonList->omFlightPlanArray.GetSize(); ilFP++)
	{
		if(pogButtonList->omFlightPlanArray[ilFP] == (void *)this)
		{
			//CDialog::OnClose();	hag000413
			pogButtonList->omFlightPlanArray.RemoveAt(ilFP);
			break;
		}
	}	*/
	CDialog::OnClose();	//hag000413
   	/*pogButtonList->m_wndFlightPlan = NULL;
	pogButtonList->m_FlightPlanButton.Recess(FALSE);*/
	
}

void FlightPlan::OnSelchangeDate() 
{
	SetViewerDate();
	UpdateView();	
}

// called in response to a global date update - new day selected ie in preplanning so syschronise the date of this display
void FlightPlan::HandleGlobalDateUpdate()
{
	m_Date.SetCurSel(ogBasicData.GetDisplayDateOffset());
	OnSelchangeDate();
}

void FlightPlan::SetViewerDate()
{
	CTime olStart = ogBasicData.GetTimeframeStart();
	CTime olDay = CTime(olStart.GetYear(),olStart.GetMonth(),olStart.GetDay(),0,0,0)  + CTimeSpan(m_Date.GetCurSel(),0,0,0);

	omViewer.omStartTime = CTime(olDay.GetYear(),olDay.GetMonth(),olDay.GetDay(),0,0,0);
	omViewer.omEndTime = CTime(olDay.GetYear(),olDay.GetMonth(),olDay.GetDay(),23,59,59);
}

void FlightPlan::SetDate(CTime opDate)
{
	m_Date.SetCurSel(ogBasicData.GetDisplayDateOffsetByDate(opDate));
	OnSelchangeDate();
}

LONG FlightPlan::OnTableDragBegin(UINT wParam, LONG lParam)
{
	CPoint olDragPosition;
	::GetCursorPos(&olDragPosition);

	int imDragLineno = pomFlightTable->GetLinenoFromPoint(olDragPosition);
	if (!(0 <= imDragLineno && imDragLineno <= omViewer.omLines.GetSize()-1))
		return -1L;	// cannot drag from a blank line

	// check if the user drags from a blank part of the line
	int ilDragColumnno = pomFlightTable->GetColumnnoFromPoint(olDragPosition);
	if (ilDragColumnno == -1L)	// drag from an invalid column?
		return -1L;

	long ilDragFlightUrno = (ilDragColumnno <= 10)?	// drag an arrival flight?
		omViewer.omLines[imDragLineno].ArrUrno:
		omViewer.omLines[imDragLineno].DepUrno;
	if (ilDragFlightUrno == -1L)
		return -1L;

	CString olDragFlightAloc;
	CString olDragFlightAlid;

	switch(ilDragColumnno)
	{
	case 7 :	//	arrival position
		olDragFlightAloc = ALLOCUNITTYPE_PST;
		olDragFlightAlid = omViewer.omLines[imDragLineno].Posia;
	break;
	case 8 :	//	arrival gate
		olDragFlightAloc = ALLOCUNITTYPE_GATE;
		olDragFlightAlid = omViewer.omLines[imDragLineno].Alida;
	break;
	case 10 :	//	registration
		olDragFlightAloc = ALLOCUNITTYPE_REGN;
		olDragFlightAlid = omViewer.omLines[imDragLineno].Regn;
	break;
	case 18 :	//	departure position
		olDragFlightAloc = ALLOCUNITTYPE_PST;
		olDragFlightAlid = omViewer.omLines[imDragLineno].Posid;
	break;
	case 19:	// departure gate
		olDragFlightAloc = ALLOCUNITTYPE_GATE;
		olDragFlightAlid = omViewer.omLines[imDragLineno].Alidd;
	break;
	default :
		if (ilDragColumnno <= 10)
		{
			olDragFlightAloc = ALLOCUNITTYPE_GATE;
			olDragFlightAlid = omViewer.omLines[imDragLineno].Alida;
		}
		else
		{
			olDragFlightAloc = ALLOCUNITTYPE_GATE;
			olDragFlightAlid = omViewer.omLines[imDragLineno].Alidd;
		}
	}

	if (!olDragFlightAloc.GetLength())
		return -1L;

	int ilReturnFlightType = ID_NOT_RETURNFLIGHT;
	if(omViewer.omLines[imDragLineno].ArrUrno == imDropFlightUrno)
		ilReturnFlightType = omViewer.omLines[imDragLineno].ArrReturnFlightType;
	else
		ilReturnFlightType = omViewer.omLines[imDragLineno].DepReturnFlightType;

	m_DragDropTarget.CreateDWordData(DIT_FLIGHTBAR, 6);
	m_DragDropTarget.AddDWord(ilDragFlightUrno);
	m_DragDropTarget.AddDWord(0L); // no rotation URNO
	m_DragDropTarget.AddString(olDragFlightAloc);
	m_DragDropTarget.AddString(olDragFlightAlid);
	m_DragDropTarget.AddDWord(0L);
	m_DragDropTarget.AddDWord(ilReturnFlightType);
	m_DragDropTarget.BeginDrag();
		
//	pomFlightTable->GetCTableListBox()->SetCurSel(-1);

	return 0L;
}

//Singapore
void FlightPlan::OnTableUpdateDataCount()
{
	if(ogBasicData.IsPaxServiceT2() == true)
	{
		CWnd* polWnd = GetDlgItem(IDC_DATACOUNT);
		if(polWnd != NULL)
		{	
			char olDataCount[10];
			polWnd->EnableWindow();			
			polWnd->SetWindowText(itoa(omViewer.omLines.GetSize(),olDataCount,10));
		}
	}
}

//PRF 8712
void FlightPlan::OnMove(int x, int y)
{	
	CDialog::OnMove(x,y);
	if(this->IsIconic() == FALSE)
	{
		GetWindowRect(&omWindowRect);
	}
}