// CCITableSortPage.h : header file
//
#ifndef _CCITBLSO_H_
#define _CCITBLSO_H_
/////////////////////////////////////////////////////////////////////////////
// CCITableSortPage dialog

class CCITableSortPage : public CPropertyPage
{
	DECLARE_DYNCREATE(CCITableSortPage)

// Construction
public:
	CCITableSortPage();
	~CCITableSortPage();

// Dialog Data
	CStringArray omSortOrders;

	//{{AFX_DATA(CCITableSortPage)
	enum { IDD = IDD_CCITABLE_SORT_PAGE };
	BOOL	m_Group;
	int		m_SortOrder0;
	int		m_SortOrder1;
	//}}AFX_DATA
	CString omTitle;

// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(CCITableSortPage)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnCommand(WPARAM wParam, LPARAM lParam);
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(CCITableSortPage)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

// Helper routines
private:
	int GetSortOrder(const char *pcpSortKey);
	CString GetSortKey(int ipSortOrder);
};

#endif // _CCITBLSO_H_
