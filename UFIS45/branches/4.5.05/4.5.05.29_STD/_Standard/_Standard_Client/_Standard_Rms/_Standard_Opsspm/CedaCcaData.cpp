// CedaCcaData.cpp - Class for gates
//

#include <afxwin.h>
#include <ccsglobl.h>
#include <OpssPm.h>
#include <CCSPtrArray.h>
#include <CCSCedaData.h>
#include <CCSBcHandle.h>
#include <ccsddx.h>
#include <CedaCcaData.h>
#include <CedaRudData.h>
#include <CedaCicData.h>
#include <BasicData.h>
#include <CedaDemandData.h>
#include <AllocData.h>
#include <TimeFrameList.h>

// General macros for testing a point against an interval
#define IsBetween(val, start, end)  ((start) <= (val) && (val) <= (end))
#define IsOverlapped(start1, end1, start2, end2)    ((start1) <= (end2) && (start2) <= (end1))
#define IsReallyOverlapped(start1, end1, start2, end2)	((start1) < (end2) && (start2) < (end1))
#define IsWithIn(start1, end1, start2, end2)	((start1) >= (start2) && (end1) <= (end2))

extern CCSDdx ogCCSDdx;
void  ProcessCcaCf(void * popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

static int ByName(const CCADATA **ppp1, const CCADATA **ppp2)
{
	return (int)(strcmp((*ppp1)->Cnam,(*ppp2)->Cnam));
}

CedaCcaData::CedaCcaData()
{                  
    BEGIN_CEDARECINFO(CCADATA, CCADATARecInfo)
		FIELD_LONG(Urno,"URNO")
		FIELD_CHAR_TRIM(Cnam,"CKIC")
		FIELD_CHAR_TRIM(Ckit,"CKIT")
		FIELD_LONG(Uaft,"FLNU")
		FIELD_LONG(Udem,"GHPU")
		FIELD_LONG(Urue,"GHSU")
		FIELD_CHAR_TRIM(Ctyp,"CTYP")
		FIELD_DATE(Ckbs,"CKBS")
		FIELD_DATE(Ckes,"CKES")
		FIELD_DATE(Ckba,"CKBA")
		FIELD_DATE(Ckea,"CKEA")
        FIELD_CHAR_TRIM(Usec,"USEC")
        FIELD_DATE(Cdat,"CDAT")
        FIELD_CHAR_TRIM(Useu,"USEU")
        FIELD_DATE(Lstu,"LSTU")
    END_CEDARECINFO

    // Copy the record structure
    for (int i = 0; i < sizeof(CCADATARecInfo)/sizeof(CCADATARecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&CCADATARecInfo[i],sizeof(CEDARECINFO));
        omRecInfo.Add(prpCedaRecInfo);
	}

    // Initialize table names and field names
    strcpy(pcmTableName,"CCATAB");
    pcmFieldList = "URNO,CKIC,CKIT,FLNU,GHPU,GHSU,CTYP,CKBS,CKES,CKBA,CKEA,USEC,CDAT,USEU,LSTU";

	ogCCSDdx.Register((void *)this,BC_CCA_CHANGE,CString("CCADATA"), CString("Cca changed"),ProcessCcaCf);
	ogCCSDdx.Register((void *)this,BC_CCA_DELETE,CString("CCADATA"), CString("Cca deleted"),ProcessCcaCf);
}
 
CedaCcaData::~CedaCcaData()
{
	TRACE("CedaCcaData::~CedaCcaData called\n");
	ClearAll();
	ogCCSDdx.UnRegister(this,NOTUSED);
}

void CedaCcaData::ClearAll()
{
	CMapPtrToPtr *polSingleMap;
	CString olCnam;
	POSITION rlPos;
	for(rlPos =  omCnamMap.GetStartPosition(); rlPos != NULL; )
	{
		omCnamMap.GetNextAssoc(rlPos,olCnam,(void *& )polSingleMap);
		polSingleMap->RemoveAll();
		delete polSingleMap;
	}
	omCnamMap.RemoveAll();

	long llUaft;
	for(rlPos =  omUaftMap.GetStartPosition(); rlPos != NULL; )
	{
		omUaftMap.GetNextAssoc(rlPos, (void *&) llUaft,(void *&)polSingleMap);
		polSingleMap->RemoveAll();
		delete polSingleMap;
	}
	omUaftMap.RemoveAll();

	omUrnoMap.RemoveAll();
	omCciMap.RemoveAll();

	omData.DeleteAll();
}

void  ProcessCcaCf(void * popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	((CedaCcaData *)popInstance)->ProcessCcaBc(ipDDXType,vpDataPointer,ropInstanceName);
}

void  CedaCcaData::ProcessCcaBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlCcaData = (struct BcStruct *) vpDataPointer;
	ogBasicData.LogBroadcast(prlCcaData);
	long llUrno = GetUrnoFromSelection(prlCcaData->Selection);
	if(llUrno == 0L)
	{
		CCADATA rlCca;
		GetRecordFromItemList(&rlCca,prlCcaData->Fields,prlCcaData->Data);
		llUrno = rlCca.Urno;
	}
	CCADATA *prlCca = GetCcaByUrno(llUrno);
	//ogBasicData.Trace("CCATAB BC Cmd <%s>\nFields <%s>\nData <%s>\nSelection <%s>\n",prlCcaData->Cmd,prlCcaData->Fields,prlCcaData->Data,prlCcaData->Selection);

	if(ipDDXType == BC_CCA_CHANGE && prlCca != NULL)
	{
		// update
		//ogBasicData.Trace("BC_CCA_CHANGE - UPDATE CCATAB URNO %ld\n",prlCca->Urno);
		CCADATA rlOldCca = *prlCca;

		ConvertDatesToUtc(prlCca);
		GetRecordFromItemList(prlCca,prlCcaData->Fields,prlCcaData->Data);
		PrepareData(prlCca);

		if(strcmp(rlOldCca.Cnam,prlCca->Cnam) != 0)
		{
			DeleteCcaFromCnamMap(rlOldCca.Cnam, rlOldCca.Urno);
			AddCcaToCnamMap(prlCca);
		}

		if(rlOldCca.Uaft,prlCca->Uaft)
		{
			DeleteCcaFromUaftMap(rlOldCca.Uaft, rlOldCca.Uaft);
			AddCcaToUaftMap(prlCca);
		}

		ogCCSDdx.DataChanged((void *)this,CCA_CHANGE,(void *)prlCca);
	}
	else if(ipDDXType == BC_CCA_CHANGE && prlCca == NULL)
	{
		// insert
		CCADATA rlCca;
		GetRecordFromItemList(&rlCca,prlCcaData->Fields,prlCcaData->Data);
		//ogBasicData.Trace("BC_CCA_CHANGE - INSERT CCATAB URNO %ld\n",rlCca.Urno);
		if((prlCca = AddCcaInternal(rlCca)) != NULL)
		{
			ogCCSDdx.DataChanged((void *)this,CCA_CHANGE,(void *)prlCca);
		}
	}
	else if(ipDDXType == BC_CCA_DELETE && prlCca != NULL)
	{
		// delete
		long llCcaUrno = prlCca->Urno;
		//ogBasicData.Trace("BC_CCA_DELETE - DELETE CCATAB URNO %ld\n",llCcaUrno);
		DeleteCcaInternal(llCcaUrno);
		ogCCSDdx.DataChanged((void *)this,CCA_DELETE,(void *)llCcaUrno);
	}
}

CCSReturnCode CedaCcaData::ReadCcaData(CTime opFrom, CTime opTo)
{
	CCSReturnCode ilRc = RCSuccess;
	ClearAll();

	char pclCom[10] = "RT";
    char pclWhere[512];
//    sprintf(pclWhere,"WHERE (CKBS >= '%s' AND CKES <= '%s') OR (CKEA <> ' ' AND CKBA <> ' ' AND CKBA >= '%s' AND CKEA <= '%s')",opFrom.Format("%Y%m%d%H%M%S"),opTo.Format("%Y%m%d%H%M%S"),opFrom.Format("%Y%m%d%H%M%S"),opTo.Format("%Y%m%d%H%M%S"));
	sprintf(pclWhere,"WHERE (ckbs between '%s' AND '%s')or(ckes between '%s' AND '%s') or (ckba between '%s' AND '%s' ) or (ckea between '%s' AND '%s')",
						opFrom.Format("%Y%m%d%H%M%S"),opTo.Format("%Y%m%d%H%M%S"),
						opFrom.Format("%Y%m%d%H%M%S"),opTo.Format("%Y%m%d%H%M%S"),
						opFrom.Format("%Y%m%d%H%M%S"),opTo.Format("%Y%m%d%H%M%S"),
						opFrom.Format("%Y%m%d%H%M%S"),opTo.Format("%Y%m%d%H%M%S")
						);

    if((ilRc = CedaAction2(pclCom, pclWhere)) != RCSuccess)
	{
		ogBasicData.LogCedaError("CedaCcaData Error",omLastErrorMessage,pclCom,pcmFieldList,pcmTableName,pclWhere);
	}
	else
	{
		CCADATA rlCcaData;
		for (int ilLc = 0; ilRc == RCSuccess; ilLc++)
		{
			if ((ilRc = GetBufferRecord2(ilLc,&rlCcaData)) == RCSuccess)
			{
				rlCcaData.emState = CCADATA::UNCHANGED;
				AddCcaInternal(rlCcaData);
			}
		}
		ilRc = RCSuccess;
	}

	ogBasicData.Trace("CedaCcaData Read %d Records (Rec Size %d) %s",omData.GetSize(), sizeof(CCADATA), pclWhere);
    return ilRc;
}


CCADATA *CedaCcaData::AddCcaInternal(const CCADATA &rrpCca)
{
	CCADATA *prlCca = NULL;

	// Ctyp = 'P' -> Pre-checkin (can check-in the night before, for early morning flights), should not be displayed
	if(rrpCca.Ckbs != TIMENULL && rrpCca.Ckes != TIMENULL && rrpCca.Ckbs < rrpCca.Ckes && rrpCca.Ctyp[0] != 'P')
	{
		prlCca = new CCADATA;
		*prlCca = rrpCca;
		PrepareData(prlCca);
		omData.Add(prlCca);
		omUrnoMap.SetAt((void *)prlCca->Urno,prlCca);
		AddCcaToCnamMap(prlCca);
		if (prlCca->IsDedicatedCheckin())
			AddCcaToUaftMap(prlCca);
		else
			AddCcaToCciMap(prlCca);
	}
	else
	{
		if(rrpCca.Ckbs == TIMENULL || rrpCca.Ckes == TIMENULL)
		{
			TRACE("Invalid CCA record CKBA or CKES are empty CCATAB.URNO <%ld>\n", rrpCca.Urno);
		}
		else
		{
			TRACE("Invalid CCA record CKBA is later than CKES CCATAB.URNO <%ld>\n", rrpCca.Urno);
		}
	}

	return prlCca;
}

void CedaCcaData::DeleteCcaInternal(long lpUrno)
{
	int ilNumCcas = omData.GetSize();
	for(int ilDel = (ilNumCcas-1); ilDel >= 0; ilDel--)
	{
		CCADATA *prlCca = &omData[ilDel];
		if(prlCca->Urno == lpUrno)
		{
			DeleteCcaFromCnamMap(prlCca->Cnam, prlCca->Urno);
			if (prlCca->IsDedicatedCheckin())
				DeleteCcaFromUaftMap(prlCca->Uaft, prlCca->Urno);
			else
				DeleteCcaFromCciMap(lpUrno);
			omData.DeleteAt(ilDel);
		}
	}
	omUrnoMap.RemoveKey((void *)lpUrno);
}

void CedaCcaData::AddCcaToCnamMap(CCADATA *prpCca)
{
	CMapPtrToPtr *polSingleMap;
	if(omCnamMap.Lookup(prpCca->Cnam, (void *&) polSingleMap))
	{
		polSingleMap->SetAt((void *)prpCca->Urno,prpCca);
	}
	else
	{
		polSingleMap = new CMapPtrToPtr;
		polSingleMap->SetAt((void *)prpCca->Urno,prpCca);
		omCnamMap.SetAt(prpCca->Cnam,polSingleMap);
	}
}

void CedaCcaData::DeleteCcaFromCnamMap(const char *pcpCnam, long lpCcaUrno)
{
	CMapPtrToPtr *polSingleMap;
	if(omCnamMap.Lookup(pcpCnam,(void *& )polSingleMap))
	{
		polSingleMap->RemoveKey((void *)lpCcaUrno);
	}
}

void CedaCcaData::AddCcaToUaftMap(CCADATA *prpCca)
{
	ASSERT(prpCca && prpCca->IsDedicatedCheckin());

	CMapPtrToPtr *polSingleMap;
	if(omUaftMap.Lookup((void *)prpCca->Uaft, (void *&) polSingleMap))
	{
		polSingleMap->SetAt((void *)prpCca->Urno,prpCca);
	}
	else
	{
		polSingleMap = new CMapPtrToPtr;
		polSingleMap->SetAt((void *)prpCca->Urno,prpCca);
		omUaftMap.SetAt((void *)prpCca->Uaft,polSingleMap);
	}
}

void CedaCcaData::DeleteCcaFromUaftMap(long lpUaft, long lpCcaUrno)
{
	CMapPtrToPtr *polSingleMap;
	if(omUaftMap.Lookup((void *)lpUaft,(void *&)polSingleMap))
	{
		polSingleMap->RemoveKey((void *)lpCcaUrno);
	}
}

void CedaCcaData::AddCcaToCciMap(CCADATA *prpCca)
{
	ASSERT(prpCca && prpCca->IsCommonCheckin());

	omCciMap.SetAt((void *)prpCca->Urno,prpCca);
}

void CedaCcaData::DeleteCcaFromCciMap(long lpCcaUrno)
{
	omCciMap.RemoveKey((void *)lpCcaUrno);
}

void CedaCcaData::PrepareData(CCADATA *prpCca)
{
	ConvertDatesToLocal(prpCca);

	if(prpCca->Ckba == TIMENULL)
	{
		prpCca->Ckba = prpCca->Ckbs; // if actual is blank then actual = scheduled
	}
	if(prpCca->Ckea == TIMENULL)
	{
		prpCca->Ckea = prpCca->Ckes; // if actual is blank then actual = scheduled
	}

	if (prpCca->IsCommonCheckin())
	{
		prpCca->Ualt = prpCca->Uaft;
		prpCca->Uaft = 0;
	}
	else
	{
		prpCca->Ualt = 0;
	}
}

CCADATA* CedaCcaData::GetCcaByUrno(long lpUrno)
{
	CCADATA *prlCca = NULL;
	omUrnoMap.Lookup((void *)lpUrno, (void *&) prlCca);
	return prlCca;
}

void CedaCcaData::GetCcaListByCheckinCounter(CString opCnam, CCSPtrArray <CCADATA> &ropCcaList, bool bpReset /*true*/)
{
	if(bpReset)
	{
	    ropCcaList.RemoveAll();
	}

	CMapPtrToPtr *polSingleMap;
	long llUrno;
	CCADATA *prlCca;

	if(omCnamMap.Lookup(opCnam,(void *& )polSingleMap))
	{
		for(POSITION rlPos = polSingleMap->GetStartPosition(); rlPos != NULL; )
		{
			polSingleMap->GetNextAssoc(rlPos,(void *& ) llUrno,(void *& )prlCca);
			ropCcaList.Add(prlCca);
		}
	}
}

// prpDemand - personnel checkin demand that requires a link to a checkin counter
// get a list of checkin counters that have the same rule-event and flight as this
// demand and return the one with the time closest to the demand
CCADATA *CedaCcaData::GetBestCcaForDemand(DEMANDDATA *prpDemand)
{
	CCADATA *prlCca, *prlBestCca = NULL, *prl2ndBestCca = NULL;
	if(prpDemand != NULL)
	{
		if (prpDemand->Dety[0] == CCI_DEMAND)
		{
			long llUrno;
			int ilDiff, ilBestDiff, il2ndBestDiff;
			for (POSITION rlPos = omCciMap.GetStartPosition(); rlPos != NULL;)
			{
				omCciMap.GetNextAssoc(rlPos,(void *&)llUrno,(void *&)prlCca);
				if (prlCca->Urue == prpDemand->Urue)
				{
					if(CheckinCounterIsFree(prlCca))
					{
						ilDiff = abs((prlCca->Ckba - prpDemand->Debe).GetTotalMinutes()) + abs((prlCca->Ckea - prpDemand->Deen).GetTotalMinutes());
						if(prlBestCca == NULL || ilDiff < ilBestDiff)
						{
							prlBestCca = prlCca;
							ilBestDiff = ilDiff;
						}
					}
					else
					{
						ilDiff = abs((prlCca->Ckba - prpDemand->Debe).GetTotalMinutes()) + abs((prlCca->Ckea - prpDemand->Deen).GetTotalMinutes());
						if(prl2ndBestCca == NULL || ilDiff < il2ndBestDiff)
						{
							prl2ndBestCca = prlCca;
							il2ndBestDiff = ilDiff;
						}
					}
				}
			}
		}
		else
		{
			CMapPtrToPtr *polSingleMap;
			long llUrno;
			int ilDiff, ilBestDiff, il2ndBestDiff;
			long lpUaft = ogDemandData.GetFlightUrno(prpDemand);

			if(omUaftMap.Lookup((void *&)lpUaft,(void *&)polSingleMap))
			{
				for(POSITION rlPos = polSingleMap->GetStartPosition(); rlPos != NULL; )
				{
					polSingleMap->GetNextAssoc(rlPos,(void *& ) llUrno,(void *& )prlCca);
					if(prlCca->Urue == prpDemand->Urue)
					{
						if(CheckinCounterIsFree(prlCca))
						{
							ilDiff = abs((prlCca->Ckba - prpDemand->Debe).GetTotalMinutes()) + abs((prlCca->Ckea - prpDemand->Deen).GetTotalMinutes());
							if(prlBestCca == NULL || ilDiff < ilBestDiff)
							{
								prlBestCca = prlCca;
								ilBestDiff = ilDiff;
							}
						}
						else
						{
							ilDiff = abs((prlCca->Ckba - prpDemand->Debe).GetTotalMinutes()) + abs((prlCca->Ckea - prpDemand->Deen).GetTotalMinutes());
							if(prl2ndBestCca == NULL || ilDiff < il2ndBestDiff)
							{
								prl2ndBestCca = prlCca;
								il2ndBestDiff = ilDiff;
							}
						}
					}
				}
			}
		}
	}
	return prlBestCca != NULL ? prlBestCca : prl2ndBestCca;
}

bool CedaCcaData::CheckinCounterIsFree(CCADATA *prpCca)
{
	bool blCheckinCounterIsFree = false;
	if(prpCca != NULL)
	{
		CCSPtrArray <DEMANDDATA> olDemands;
		ogDemandData.GetDemandsByAlid(olDemands,prpCca->Cnam,ALLOCUNITTYPE_CIC, PERSONNELDEMANDS);
		if(olDemands.GetSize() <= 0)
		{
			blCheckinCounterIsFree = true;
		}
	}
	return blCheckinCounterIsFree;
}

CString CedaCcaData::GetTableName(void)
{
	return CString(pcmTableName);
}

void CedaCcaData::ConvertDatesToUtc(CCADATA *prpCca)
{
	ogBasicData.ConvertDateToUtc(prpCca->Ckbs);
	ogBasicData.ConvertDateToUtc(prpCca->Ckes);
	ogBasicData.ConvertDateToUtc(prpCca->Ckba);
	ogBasicData.ConvertDateToUtc(prpCca->Ckea);
	ogBasicData.ConvertDateToUtc(prpCca->Cdat);
	ogBasicData.ConvertDateToUtc(prpCca->Lstu);
}

void CedaCcaData::ConvertDatesToLocal(CCADATA *prpCca)
{
	ogBasicData.ConvertDateToLocal(prpCca->Ckbs);
	ogBasicData.ConvertDateToLocal(prpCca->Ckes);
	ogBasicData.ConvertDateToLocal(prpCca->Ckba);
	ogBasicData.ConvertDateToLocal(prpCca->Ckea);
	ogBasicData.ConvertDateToLocal(prpCca->Cdat);
	ogBasicData.ConvertDateToLocal(prpCca->Lstu);
}

BOOL CedaCcaData::InitNewCcaRecord(const DEMANDDATA *prpDem,CCADATA& rpData)
{
	ASSERT(prpDem);

	rpData.Urno = ogBasicData.GetNextUrno();

	if (!ogDemandData.IsFlightDependingDemand(prpDem->Dety))
	{
		rpData.Ctyp[0] = 'C';			
		rpData.Urue	   = prpDem->Urue;	 
		rpData.Udem	   = 0;	
	}
	else
	{
		rpData.Ctyp[0] = ' ';			
		rpData.Uaft	   = ogDemandData.GetFlightUrno(const_cast<DEMANDDATA *>(prpDem));
		rpData.Urue	   = prpDem->Urue;	 
		rpData.Udem	   = 0;	
	}

	strncpy(rpData.Cnam,prpDem->Alid,sizeof(rpData.Cnam));
	CICDATA *prlCic = ogCicData.GetCicByName(rpData.Cnam);
	if(prlCic != NULL)
	{
		strcpy(rpData.Cnam, prlCic->Cnam);
		rpData.Ckit[0] = prlCic->Term[0];
		rpData.Ckit[1] = '\0';
	}

	rpData.Cnam[sizeof(rpData.Cnam)-1] = '\0';

	rpData.Ckbs = prpDem->Debe;
	rpData.Ckes = prpDem->Deen;

	rpData.Ckba = prpDem->Debe;
	rpData.Ckea = prpDem->Deen;

	return TRUE;
}

BOOL CedaCcaData::AddCcaRecord(const CCADATA& rpData,BOOL bpSendDDX)
{
	BOOL blResult = FALSE;

	CCADATA rlData(rpData);
	ConvertDatesToUtc(&rlData);
	rlData.emState = CCADATA::NEW;
	CCADATA *prlData = AddCcaInternal(rlData);
	if(prlData != NULL)
	{
		blResult = SaveCcaRecord(prlData);

		if (blResult && bpSendDDX)
		{
			ogCCSDdx.DataChanged((void *)this,CCA_CHANGE,(void *)prlData);
		}
	}
	
	return blResult;
}


BOOL CedaCcaData::SaveCcaRecord(CCADATA *prpCca,CCADATA *prpOldCca)
{
	ASSERT(prpCca);

	if (prpCca->emState == CCADATA::UNCHANGED)
		return TRUE;

	// set lstu/cdat etc
	CTime olCurrTime = ogBasicData.GetTime();
	CString olUserText;
	olUserText.Format("%s %s %s",ogUsername,"CedaCcaData",ogBasicData.GetVersion()); // username + version number and function number (which function did the change)

	prpCca->Lstu = olCurrTime;
	strcpy(prpCca->Useu,olUserText.Left(31));

	if(prpCca->Cdat == TIMENULL || strlen(prpCca->Usec) <= 0)
	{
		prpCca->Cdat = olCurrTime;
		strcpy(prpCca->Usec,olUserText.Left(31));
	}

	ConvertDatesToUtc(prpCca);
	if (prpOldCca)
		ConvertDatesToUtc(prpOldCca);

	CString olListOfData;
	CCADATA rlCca = *prpCca;
	char pclData[2048]		= "";
	char pclSelection[256]	= "";
	char pclCmd[128]		= "";
	int	 olRc = 0;

	switch(prpCca->emState)
	{
	case CCADATA::NEW :
		MakeCedaData(&omRecInfo,olListOfData,&rlCca);
		strcpy(pclData,olListOfData);
		strcpy(pclCmd,"IRT");
		ogBasicData.LogCedaAction(pcmTableName,"IRT","", pcmFieldList, pclData);

		if(!(olRc = CedaAction("IRT","","",pclData)))
		{
			ogBasicData.LogCedaError("CedaCcaData Error",omLastErrorMessage,"IRT",pcmFieldList,pcmTableName,"");
		}
		else
			prpCca->emState = CCADATA::UNCHANGED;
	break;
	case CCADATA::CHANGED :
		sprintf(pclSelection,"WHERE URNO = '%ld'",prpCca->Urno);
		strcpy(pclCmd,"URT");

		// update all fields in JOBTAB for this record
		MakeCedaData(&omRecInfo,olListOfData,&rlCca);
		strcpy(pclData,olListOfData);
		ogBasicData.LogCedaAction(pcmTableName, pclCmd, pclSelection, pcmFieldList, pclData);
		olRc = CedaAction(pclCmd,pclSelection,"",pclData);
	
		if(!olRc)
		{
			ogBasicData.LogCedaError("CedaCcaData Error",omLastErrorMessage,pclCmd,pcmFieldList,pcmTableName,pclSelection);
		}
		else
			prpCca->emState = CCADATA::UNCHANGED;
	break;
	case CCADATA::DELETED :
		sprintf(pclSelection,"WHERE URNO = '%ld'",prpCca->Urno);
		strcpy(pclCmd,"DRT");
		ogBasicData.LogCedaAction(pcmTableName, pclCmd, pclSelection, pcmFieldList, pclData);
		olRc = CedaAction(pclCmd,pclSelection);
		if (!olRc)
		{
			ogBasicData.LogCedaError("CedaCcaData Error",omLastErrorMessage,pclCmd,pcmFieldList,pcmTableName,pclSelection);
		}
	break;
	}

	ConvertDatesToLocal(prpCca);
	if (prpOldCca)
		ConvertDatesToLocal(prpOldCca);

	return olRc != 0;
}

BOOL CedaCcaData::ChangeTime(long lpCca,const CTime& opNewStart,const CTime& opNewEnd)
{
	CCADATA *prlCca = GetCcaByUrno(lpCca);
	if (!prlCca)
		return FALSE;
	
	CCADATA rlOldCca(*prlCca);

	prlCca->Ckba = opNewStart;
	prlCca->Ckea = opNewEnd;
	ogCCSDdx.DataChanged((void *)this,CCA_CHANGE,(void *)prlCca);
	prlCca->emState = CCADATA::CHANGED;
	return SaveCcaRecord(prlCca,&rlOldCca);
}

BOOL CedaCcaData::DeleteCcaRecord(long lpCca,BOOL IsFromBC)
{
	CCADATA *prlCca = GetCcaByUrno(lpCca);
	if (!prlCca)
		return FALSE;
	CCADATA rlCca(*prlCca);

	ogCCSDdx.DataChanged((void *)this,CCA_DELETE,(void *)lpCca);
	DeleteCcaInternal(lpCca);

	rlCca.emState = CCADATA::DELETED;	
	return SaveCcaRecord(&rlCca);
}

BOOL CedaCcaData::FindCcaRecords(const DEMANDDATA *prpDem,CCSPtrArray<CCADATA>& ropCcaList)
{
	ASSERT(prpDem);
	if (!strlen(prpDem->Alid))
		return NULL;

	CTime olStart = prpDem->Debe;
	CTime olEnd   = prpDem->Deen;

	if (ogDemandData.IsFlightDependingDemand(prpDem->Dety))	// dedicated only
	{
		RUDDATA *prlRud = ogRudData.GetRudByUrno(prpDem->Urud);
		if (prlRud)
		{
			olStart += CTimeSpan(0L,0L,0L,prlRud->Ttgt);
			olEnd   -= CTimeSpan(0L,0L,0L,prlRud->Ttgf);
		}
	}

	CMapPtrToPtr *polSingleMap;
	if (omCnamMap.Lookup(prpDem->Alid, (void *&) polSingleMap))
	{
		ASSERT(polSingleMap);

		CCADATA *polKey;
		CCADATA *polCca;
		POSITION pos = polSingleMap->GetStartPosition();
		while(pos)
		{
			polSingleMap->GetNextAssoc(pos,(void *&)polKey,(void *&)polCca);
			ASSERT(polCca);
			if (!ogDemandData.IsFlightDependingDemand(prpDem->Dety))
			{
				if (polCca->IsCommonCheckin())
				{
					if (IsReallyOverlapped(polCca->Ckba,polCca->Ckea,olStart,olEnd))
					{
						ropCcaList.Add(polCca);
					}
				}
			}
			else
			{
				if (polCca->IsDedicatedCheckin())
				{
					if (IsReallyOverlapped(polCca->Ckba,polCca->Ckea,olStart,olEnd))
					{
						ropCcaList.Add(polCca);
					}
				}
			}
		}
	}
	
	return ropCcaList.GetSize() > 0;
}	

BOOL CedaCcaData::CcaCreatedByOpssPm(const CCADATA *prpCca)
{
	ASSERT(prpCca);
	if (prpCca->Urue && !prpCca->Udem)
		return TRUE;
	else
		return FALSE;
}

BOOL CedaCcaData::CreateCcaRecords(const DEMANDDATA *prpDem,CCSPtrArray<CCADATA>& ropCcaList)
{
	ASSERT(prpDem);
	if (!strlen(prpDem->Alid))
		return FALSE;

	CTime olStart = prpDem->Debe;
	CTime olEnd   = prpDem->Deen;

	if (ogDemandData.IsFlightDependingDemand(prpDem->Dety))	// dedicated only
	{
		RUDDATA *prlRud = ogRudData.GetRudByUrno(prpDem->Urud);
		if (prlRud)
		{
			olStart += CTimeSpan(0L,0L,0L,prlRud->Ttgt);
			olEnd   -= CTimeSpan(0L,0L,0L,prlRud->Ttgf);
		}
	}

	TimeFrameList olFrameList (olStart,olEnd);

	CMapPtrToPtr *polSingleMap;
	if (omCnamMap.Lookup(prpDem->Alid, (void *&) polSingleMap))
	{
		ASSERT(polSingleMap);

		CCADATA *polKey;
		CCADATA *polCca;
		POSITION pos = polSingleMap->GetStartPosition();
		while(pos)
		{
			polSingleMap->GetNextAssoc(pos,(void *&)polKey,(void *&)polCca);
			ASSERT(polCca);
			if(polCca->Ckba != TIMENULL && polCca->Ckea != TIMENULL)
			{
				if (!ogDemandData.IsFlightDependingDemand(prpDem->Dety))
				{
					if (polCca->IsCommonCheckin())
					{
						olFrameList.Subtract(polCca->Ckba,polCca->Ckea);
					}
				}
				else
				{
					if (polCca->IsDedicatedCheckin())
					{
						olFrameList.Subtract(polCca->Ckba,polCca->Ckea);
					}
				}
			}
		}
	}

	if (olFrameList.TimeFrames() == 0)
		return FALSE;

	for (int i = 0; i < olFrameList.TimeFrames(); i++)
	{
		CTime olStart,olEnd;
		olFrameList.GetTimeFrame(i,olStart,olEnd);

		ASSERT(olStart >= prpDem->Debe && olEnd <= prpDem->Deen);

		DEMANDDATA olDemand(*prpDem);
		olDemand.Debe = olStart;
		olDemand.Deen = olEnd;
		CCADATA olCcaData;
		if (!ogCcaData.InitNewCcaRecord(&olDemand,olCcaData))
			return FALSE;

		if (!ogCcaData.AddCcaRecord(olCcaData))
			return FALSE;

		ropCcaList.Add(ogCcaData.GetCcaByUrno(olCcaData.Urno));
	}

	return TRUE;
	
}

int CedaCcaData::GetCcaListByUaft(long lpUaft, CCSPtrArray <CCADATA> &ropCcaList, bool bpReset /*= true*/)
{
	if(bpReset)
	{
		ropCcaList.RemoveAll();
	}

	CMapPtrToPtr *polSingleMap;
	if(omUaftMap.Lookup((void *)lpUaft, (void *&) polSingleMap))
	{
		CCADATA *prlCca;
		long llUrno;
		for(POSITION rlPos = polSingleMap->GetStartPosition(); rlPos != NULL; )
		{
			polSingleMap->GetNextAssoc(rlPos,(void *& ) llUrno,(void *& )prlCca);
			ropCcaList.Add(prlCca);
		}
	}

	ropCcaList.Sort(ByName);

	return ropCcaList.GetSize();
}

// return range og checkin counters for the flight eg. "321-323"
CString CedaCcaData::GetCounterRangeForFlight(long lpFlightUrno)
{
	CString olCounters;
	CCSPtrArray <CCADATA> olCcaList;

	int ilNumCounters = ogCcaData.GetCcaListByUaft(lpFlightUrno, olCcaList);

	if(ilNumCounters == 1)
	{
		olCounters = olCcaList[0].Cnam;
	}
	else if(ilNumCounters > 1)
	{
		olCounters.Format("%s - %s",olCcaList[0].Cnam, olCcaList[ilNumCounters-1].Cnam);
	}

	return olCounters;
}

CString CedaCcaData::Dump(long lpUrno)
{
	CString olDumpStr;
	CCADATA *prlCca = GetCcaByUrno(lpUrno);
	if(prlCca == NULL)
	{
		olDumpStr.Format("No CCADATA Found for CCA.URNO <%ld>",lpUrno);
	}
	else
	{
		CString olData;
		MakeCedaData(&omRecInfo,olData,prlCca);
		CStringArray olFieldArray, olDataArray;
		ogBasicData.ExtractItemList(pcmFieldList, &olFieldArray);
		ogBasicData.ExtractItemList(olData, &olDataArray);
		int ilNumFields = olFieldArray.GetSize();
		int ilNumData = olDataArray.GetSize();
		if(ilNumFields != ilNumData)
		{
			olDumpStr.Format("<%s>\n<%s>\n",pcmFieldList,olData);
		}
		else
		{
			for(int ilD = 0; ilD < ilNumData; ilD++)
			{
				olDumpStr += olFieldArray[ilD] + " = <" + olDataArray[ilD] + ">    ";
			}
		}
	}
	return olDumpStr;
}
