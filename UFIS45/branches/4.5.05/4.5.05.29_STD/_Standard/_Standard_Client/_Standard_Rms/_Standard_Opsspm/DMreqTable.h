// DMreqTable.h : header file
//
#ifndef _DMREQTABLE_H_
#define _DMREQTABLE_H_

/////////////////////////////////////////////////////////////////////////////
// DMreqTable dialog

class DMreqTable : public CDialog
{
// Construction
public:
	// DMreqTable(CWnd* pParent = NULL);   // standard constructor
	DMreqTable(CWnd* pParent,REQUESTDATA *prpRequest,int ipDlgAction);

// Dialog Data
	//{{AFX_DATA(DMreqTable)
	enum { IDD = IDD_MREQDIALOG };
	CTime	m_Rdat;
	CString	m_NameFrom;
	CTime	m_Rqda;
	CString m_NameTo;
	CString m_Text;

		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA
 

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(DMreqTable)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected: 

	CString omCusr;
	CString omPeno;
	REQUESTDATA *prmRequest;
	int imDlgAction;

	// Generated message map functions
	//{{AFX_MSG(DMreqTable)
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	afx_msg void OnEditchangeNamfrom();
	afx_msg void OnEditchangeNamto();
	afx_msg void OnKillfocusNamfrom();
	afx_msg void OnKillfocusNamto();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#endif // _DMREQTABLE_H_