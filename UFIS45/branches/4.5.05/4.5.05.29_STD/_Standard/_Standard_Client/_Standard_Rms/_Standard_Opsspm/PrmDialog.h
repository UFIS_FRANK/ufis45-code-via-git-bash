#if !defined(AFX_PRMDIALOG_H__5E926A91_3E61_11D3_94EF_00001C018ACE__INCLUDED_)
#define AFX_PRMDIALOG_H__5E926A91_3E61_11D3_94EF_00001C018ACE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// PrmDialog.h : header file
//
#include <CCSCedaData.h>
#include <ccsobj.h>
#include <CCSPtrArray.h>
#include <CCSEdit.h>
#include <CedaDpxData.h>

enum PridFormat { X50, N12};
enum FlightType { FLIGHT, TRANSIT};
/////////////////////////////////////////////////////////////////////////////
// PrmDialog dialog

class   CPrmDialog :public CDialog
{
// Construction
public:
	CPrmDialog();
		
	CPrmDialog(CWnd* pParent,DPXDATA *prpDpx);  

	CPrmDialog(CWnd* pParent,DPXDATA *prpDpx, BOOL CanEditFlight);  

	CPrmDialog::~CPrmDialog();

// Dialog Data
	//{{AFX_DATA(PrmDialog)
	enum { IDD = IDD_PRMDIALOG };
	CCSEdit	m_Prid;
	CCSEdit	m_Name;
	CEdit	m_Mail;
	CCSEdit	m_Rema;
	CEdit	m_TransitFlight;
	CCSEdit	m_Seat;
	CEdit   m_BookingDate;
	CEdit   m_Stoa;   
	CEdit   m_Atoa;
	CEdit   m_Booking;
	CEdit	m_FlnuAlc;
	CEdit   m_FlnuNumber;
	CEdit   m_FlnuSuffix;
	CEdit   m_TfluAlc;
	CEdit   m_TfluNumber;
	CEdit   m_TfluSuffix;
	int		m_Owhc1;
	int		m_Owhc2;
	int		m_Owhc3;
	int		m_FlnuAdid;
	int		m_TfluAdid;
	BOOL    m_IsReadOnly;
	//CButton	m_Owhc2;
	CEdit   m_TimeStoa;
	CEdit   m_TimeAtoa;
	CEdit	m_Clos;
	CEdit	m_TimeClos;
	CEdit   m_TimeBooking;
	CComboBox m_PrmType;
	CComboBox m_BookingChannel;
	CComboBox m_ArrivalLocation;
	CComboBox m_Gender;
	CComboBox m_Lang;
	CComboBox m_Natl;
	CCSEdit m_Status;
	BOOL	m_CheckFlnuPrms;
	BOOL	m_CheckTfluPrms;
	CEdit   m_FlnuTime;
	CEdit   m_TfluTime;
	CEdit   m_FCsgn;
	CEdit   m_TCsgn;
	//}}AFX_DATA

	CStringArray omValues;

	CTime omStoa;
	CTime omAtoa;
	BOOL  bmAllowFreeLocation;

protected:
	DPXDATA *pomDpx;
	PridFormat imFormat;

	BOOL GetData();
	void SetData();
	void SetData(DPXDATA *popDpx);
	bool checkDate(CString date,CString time, CTime &value);
	CTime getDate(CString date,CString time);
	FLIGHTDATA* GetFlightByAlcFltnFlns(const char *pcpAlc,const char *pcpFltn, 
							const char *pcpFlns,const char *pcpFdat,char cpAdid,bool& rbpCancelled, bool& rbpAddOneday);
	
	FLIGHTDATA* GetFlightByCallsign(const char *pcpCsgn,const char *pcpFdate,char cpAdid,bool& rbpCancelled, bool& rbpAddOneday);

	CString	GetTifName(const char *polTis);
	void OnKillfocusGetTime(FlightType type);

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(PrmDialog)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(PrmDialog)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	afx_msg void OnGetFlnuTime();
	afx_msg void OnGetTfluTime();
	afx_msg void OnKillfocusFlnuTime();
	afx_msg void OnKillfocusTfluTime();
	afx_msg void OnSelchangePrmtcombo();
    afx_msg void OnKillfocusFCsgnTime();
	afx_msg void OnKillfocusTCsgnTime();
	//virtual void OnCancel();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_LOADTIMESPANDLG_H__5E926A91_3E61_11D3_94EF_00001C018ACE__INCLUDED_)
