#ifndef _ALLOCDATA_H_
#define _ALLOCDATA_H_

#include <CCSPtrArray.h>
#include <CedaSgrData.h>


#define ALLOCUNITTYPE_EQUIPMENTGROUP "EQUGROUP"
#define ALLOCUNITTYPE_PSTGROUP "PSTAREA"
#define ALLOCUNITTYPE_GATEGROUP "GATEAREA"
#define ALLOCUNITTYPE_CICGROUP "CICAREA"
#define ALLOCUNITTYPE_REGNGROUP "REGNAREA"
#define ALLOCUNITTYPE_POOLGROUP "POOL"
#define ALLOCUNITTYPE_CICDESKTYPE "CICDESKTYP"
#define ALLOCUNITTYPE_FUNCGROUP "FUNCGROUP"
#define ALLOCUNITTYPE_TEAMLEADERS "Team Leaders"
#define ALLOCUNITTYPE_REGN "A/C REG"
#define ALLOCUNITTYPE_CIC "CIC"
#define ALLOCUNITTYPE_GATE "GAT"
#define ALLOCUNITTYPE_PST "PST"
#define ALLOCUNITTYPE_EQUIPMENT "EQU"
#define ALLOCUNITTYPE_FUNCTION "PFC"
#define ALLOCUNITTYPE_FID "FID"
#define ALLOCUNITTYPE_WRO "WRO"


/////////////////////////////////////////////////////////////////////////////
// Record structure declaration

//
//	CCSPtrArray <AllocUnit> omAllocData is in the following format
//
//	omAllocData -> GroupTypes (ALOTAB) eg "GATEAREA"
//	Urno -> ALO.URNO
//	Name -> ALO.ALOC eg "GATEAREA"
//	Desc -> ALO.ALOD eg "Gate Bereich"
//	Possibly other values from zusatz table
//
//	Members -> List of Groups (SGRTAB) eg "A01-A03"
//		Urno -> SGR.URNO
//		Name -> SGR.GRPN eg "A01-A03"
//		Desc -> SGR.GRDS
//		Possibly other values from zusatz table
//
//		Members -> either a list of sub-groups (recursive) or
//				   a list of Units {GATTAB,CICTAB,etc) eg "A01"
//			Name -> GAT.GNAM | CIC.CNAM | etc
//
//
//
//	Summary: this results in a structure of the format
//
//	GATEAREAS
//	|		|->A01-A03
//	|		|		|->A01
//	|		|		|->A02
//	|		|		|->A03
//	|		|
//	|		|->A03-A07
//	|				|etc
//	|
//	CICAREAS
//			|etc


struct AllocUnitStruct {

	long	Urno;
	char	Tabn[10];
	char	Name[33];
	char	Desc[81];
	CTime	Vafr;
	CTime	Vato;
	CTime	Nafr;
	CTime	Nato;
	char	Type[11]; // type of allocation unit (ALOTAB.ALOC) eg "GATEAREA","GATE","A/C REG"

	bool	HasSubGroups;

	CCSPtrArray <AllocUnitStruct> Members; // eg "A01-A03" for groups  or "A03" for units
	CMapStringToPtr GroupsMap; // list of all groups for a group type (required only be group types)

	AllocUnitStruct(void)
	{
		Urno = 0L;
		memset(Tabn,'\0',sizeof Tabn);
		memset(Name,'\0',sizeof Name);
		memset(Desc,'\0',sizeof Desc);
		Vafr = TIMENULL;
		Vato = TIMENULL;
		Nafr = TIMENULL;
		Nato = TIMENULL;
		HasSubGroups = false;
	}
};

typedef struct AllocUnitStruct ALLOCUNIT;


/////////////////////////////////////////////////////////////////////////////
// Class declaration

class AllocData
{

// Attributes
public:
	CCSPtrArray <ALLOCUNIT> omAllocData;
	CMapStringToPtr omGroupTypesMap;

// Operations
public:
	AllocData();
	~AllocData();
	void ClearAll();
	void ClearAllocData(CCSPtrArray <ALLOCUNIT> &ropMembers);
	void CreateAllocMap();
	void CreateGroup(ALLOCUNIT *prpGroupType,SGRDATA *prpSgr, CCSPtrArray <ALLOCUNIT> &ropMembers);
	ALLOCUNIT *CreateUnit(const char *pcpTabn, long lpFurn, CCSPtrArray <ALLOCUNIT> &ropMembers);
	ALLOCUNIT *GetGroupTypeByName(const char *pcpGroupTypeName);
	ALLOCUNIT *GetGroupByName(const char *pcpGroupTypeName, const char *pcpGroupName);
	void GetGroupsByGroupType(const char *pcpGroupTypeName,CCSPtrArray <ALLOCUNIT> &ropGroupList,bool bpReset=true,bool bpGetSubGroups=false);
	void GetGroupNamesByGroupType(const char *pcpGroupTypeName,CStringArray &ropGroupNames,bool bpReset=true);
	void GetGroupsByGroupRecord(ALLOCUNIT *prpGroupOfGroups,CCSPtrArray <ALLOCUNIT> &ropGroupList,bool bpGetSubGroups);
	void GetUnitsByGroup(const char *pcpGroupTypeName,const char *pcpGroupName,CCSPtrArray <ALLOCUNIT> &ropUnitList,bool bpReset=true,bool bpGetSubUnits=false);
	void GetUnitsByGroupRecord(ALLOCUNIT *prpGroup,CCSPtrArray <ALLOCUNIT> &ropUnitList,bool bpGetSubUnits);
	ALLOCUNIT *GetGroupByUnitName(const char *pcpGroupTypeName, const char *pcpUnitName);
	ALLOCUNIT *GetGroupByUnitName(ALLOCUNIT *prpGroup, const char *pcpUnitName);
	void GetUnitsByGroupType(const char *pcpGroupTypeName,CCSPtrArray <ALLOCUNIT> &ropUnitList,bool bpReset=true,bool bpGetSubUnits=true);
};

extern AllocData ogAllocData;
#endif _ALLOCDATA_H_
