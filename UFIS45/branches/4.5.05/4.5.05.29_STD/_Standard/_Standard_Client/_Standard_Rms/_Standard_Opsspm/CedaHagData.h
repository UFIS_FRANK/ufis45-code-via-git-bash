// Class for Handling Agents
#ifndef _CEDAHAGDATA_H_
#define _CEDAHAGDATA_H_

#include <CCSCedaData.h>
#include <CCSPtrArray.h>

/////////////////////////////////////////////////////////////////////////////
// Record structure declaration

struct HagDataStruct
{
	long	Urno;		// Unique Record Number
	char	Faxn[11];	// fax number
	char	Hnam[41];	// name of handling agent
	char	Hsna[6];	// short name
	char	Prfl[2];	// 
	char	Tele[11];	// phone number
	
						

	HagDataStruct(void)
	{
		Urno = 0L;
		strcpy(Faxn,"");
		strcpy(Hnam,"");
		strcpy(Hsna,"");
		strcpy(Prfl,"");
		strcpy(Tele,"");
	}
};

typedef struct HagDataStruct HAGDATA;


/////////////////////////////////////////////////////////////////////////////
// Class declaration

class CedaHagData: public CCSCedaData
{

// Construction && destruction
public:
					CedaHagData();
	~				CedaHagData();
// Attributes
public:
	CString			GetTableName(void);
	int				GetCountOfRecords();

// Operations
public:
	CCSReturnCode	ReadHagData();
	HAGDATA*		GetHagByUrno(long lpUrno);
	HAGDATA*		GetHagByName(const CString& ropName);	
	HAGDATA*		GetHagByHsna(const CString& ropName);
	int				GetNameList(CStringArray& ropNames);
	int				GetShortNameList(CStringArray& ropNames);
	bool			GetNameList(const CStringArray& ropShortNames,CStringArray& ropNames);
	bool			GetShortNameList(const CStringArray& ropNames,CStringArray& ropShortNames);
// helpers
private:
	void AddHagInternal(HAGDATA &rrpAlt);
	void ClearAll();

// implementation data
private:
    CCSPtrArray <HAGDATA>	omData;
	CMapPtrToPtr			omUrnoMap;
	CMapStringToPtr			omHnamMap;
	CMapStringToPtr			omHsnaMap;
	char clFieldList[1024];

};


extern CedaHagData ogHagData;
#endif _CEDAHAGDATA_H_
