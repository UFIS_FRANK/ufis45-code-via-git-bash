#ifndef _CSHIFTDT_H_
#define _CSHIFTDT_H_

#include <CCSCedaData.h>
#include <CCSPtrArray.h>
#include <ccsglobl.h>

/////////////////////////////////////////////////////////////////////////////
// Record structure declaration

struct ShiftTypeDataStruct
{
	long	Urno;		// Unique record number
	char	Bsdc[9];	// Shift Code
	char	Fctc[6];	// Function Code
	char	Esbg[5];	// shift start HHMM
	char	Lsen[5];	// shift end HHMM
	char	Bkf1[5];	// Pausenlage start HHMM
	char	Bkt1[5];	// Pausenlage end HHMM
	char	Bkd1[5];	// Break Duration minutes
	char	Bkdp[2];	// Break paid flag 1=paid or blank
	char	Bsdn[41];	// shift type description
	char    Bsds[5];    // Sap Code //Singapore


	ShiftTypeDataStruct(void)
	{
		Urno = 0L;
		strcpy(Bsdc,"");
		strcpy(Fctc,"");
		strcpy(Esbg,"");
		strcpy(Lsen,"");
		strcpy(Bkf1,"");
		strcpy(Bkt1,"");
		strcpy(Bkd1,"");
		strcpy(Bkdp,"");
		strcpy(Bsdn,"");
		strcpy(Bsds,""); //Singapore
	}
};

typedef struct ShiftTypeDataStruct SHIFTTYPEDATA;


/////////////////////////////////////////////////////////////////////////////
// Class declaration

class CedaShiftTypeData: public CCSCedaData
{
// Attributes
public:
    CCSPtrArray<SHIFTTYPEDATA> omData;
    CMapStringToPtr omBsdcMap;
    CMapPtrToPtr    omUrnoMap;
	CString GetTableName(void);

// Operations
public:
    CedaShiftTypeData();
	~CedaShiftTypeData();

	void ClearAll();
	CCSReturnCode ReadShiftTypeData();
	SHIFTTYPEDATA  *GetShiftTypeById(const char *pcpShiftTypeId);
	SHIFTTYPEDATA  *GetShiftTypeByCode(const char *pcpShiftTypeId);
	SHIFTTYPEDATA  *CedaShiftTypeData::GetShiftTypeByUrno(long lpUrno);
	void GetAllShiftTypes(CStringArray& ropShiftTypes,bool bpReset=true);

private:

};


extern CedaShiftTypeData ogShiftTypes;
#endif
