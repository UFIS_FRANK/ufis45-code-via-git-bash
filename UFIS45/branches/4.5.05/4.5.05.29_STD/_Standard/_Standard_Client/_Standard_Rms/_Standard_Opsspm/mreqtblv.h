// FlightMgrDetailViewer.h : header file
//

#ifndef _MREQTBLV_H_
#define _MREQTBLV_H_

#include <CedaRequestData.h>


struct REQUEST_LINEDATA
{
	long	Urno;        // Unique record number
	CString	Stat;        // Status I=Info
	CTime	Rdat;		 // Request Date
	CString	Cusr;        // Userid of Creator (PKNO)
	CString UserFrom;
	CTime	Adat;		 // Date of Alteration
	CTime	Rqda;		 // Date concerning the request
	CString	Peno;        // Peno of user concerning the request
	CString UserTo;
	CString	Text;
};

/////////////////////////////////////////////////////////////////////////////
// RequestTableViewer

class RequestTableViewer: public CViewer
{
// Constructions
public:
    RequestTableViewer();
    ~RequestTableViewer();

  void Attach(CTable *popTable,CString opTableType);

 // Internal data processing routines
private:
    BOOL IsPassFilter(CTime opAcfr,CTime opActo);

    void MakeLines();
    void MakeAssignments();
    void MakeLine(REQUESTDATA *prpRequest);
	int CompareLine(REQUEST_LINEDATA &rrpLine1,REQUEST_LINEDATA &rrpLine2);


// Operations
public:
    void DeleteAll();
    int CreateLine(REQUEST_LINEDATA &rrpLine);
    void DeleteLine(int ipLineno);

	// Window refreshing routines
public:
    void UpdateDisplay();
    CString Format(REQUEST_LINEDATA *prpLine);
	BOOL FindItemByUrno(REQUESTDATA *prpRequest,int& ripItem);
	BOOL FindLine(long Urno, int &rilLineno);
    // Attributes used for filtering condition
private:
	CString omTableType;
	int omContextItem;

public:
	CTime omStartTime;
	CTime omEndTime;

// Attributes
public:
    CTable *pomTable;
    CCSPtrArray<REQUEST_LINEDATA> omLines;

// Methods which handle changes (from Data Distributor)
public:
    void ProcessRequestChange(REQUESTDATA *prpRequest);
	void ProcessRequestDelete(REQUESTDATA *prpRequest);

	void UpdateView();
	void UpdateComboBox();
	void ChangeViewTo(const char *pcpViewName);
    CWordArray omSortOrder;     // array of enumerated value -- defined in "mreqtblv.cpp"
	int omGroupBy;

	void PrepareSorter(void);
	void PrepareGrouping(void);
};

#endif  // _MREQTBLV_H_
