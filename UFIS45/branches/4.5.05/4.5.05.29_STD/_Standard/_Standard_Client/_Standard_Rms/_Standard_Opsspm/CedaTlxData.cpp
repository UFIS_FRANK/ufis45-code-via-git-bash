// CedaTlxData.cpp - Read/update SECTAB
//

#include <stdafx.h>
#include <CedaTlxData.h>
#include <BasicData.h> //von MZi
#include <resource.h> //von MZi

CedaTlxData ogTlxData;
//
// CedaTlxData()
//
// Constructor
//
CedaTlxData::CedaTlxData()
{

    
	// Create an array of CEDARECINFO for TLXDATA
    BEGIN_CEDARECINFO(TLXDATA, CedaTlxData)
        FIELD_LONG(URNO,"URNO")
        FIELD_CHAR_TRIM(USEC,"USEC")
        FIELD_DATE(CDAT,"CDAT")
        FIELD_CHAR_TRIM(USEU,"USEU")
        FIELD_DATE(LSTU,"LSTU")
        FIELD_CHAR_TRIM(PRFL,"PRFL")
        FIELD_CHAR_TRIM(TTYP,"TTYP")
        FIELD_CHAR_TRIM(TXT1,"TXT1")
        FIELD_CHAR_TRIM(TXT2,"TXT2")
        FIELD_DATE(TIME,"TIME")
        FIELD_CHAR_TRIM(SERE,"SERE")
        FIELD_CHAR_TRIM(STAT,"STAT")
        FIELD_CHAR_TRIM(FKEY,"FKEY")
        FIELD_CHAR_TRIM(ALC3,"ALC3")
        FIELD_CHAR_TRIM(FLTN,"FLTN")
        FIELD_CHAR_TRIM(FLNS,"FLNS")
        FIELD_CHAR_TRIM(BEME,"BEME")
        FIELD_CHAR_TRIM(MSNO,"MSNO")
        FIELD_CHAR_TRIM(MSTX,"MSTX")
    END_CEDARECINFO

    // Copy the record structure
    for (int i = 0; i < sizeof(CedaTlxData)/sizeof(CedaTlxData[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&CedaTlxData[i],sizeof(CEDARECINFO));
        omRecInfo.Add(prpCedaRecInfo);
	}


    // Initialize table name and field names
	strcpy(pcmTableName,"TLX");
	strcat(pcmTableName,pcgTableExt);
    pcmFieldList = "URNO,USEC,CDAT,USEU,LSTU,PRFL,TTYP,TXT1,TXT2,TIME,SERE,STAT,FKEY,ALC3,FLTN,FLNS,BEME,MSNO,MSTX";

	imCurrTelex = 0; // telex in omData which is currently displayed

} // end CedaTlxData()


//
// ~CedaTlxData()
//
// Destructor
//
CedaTlxData::~CedaTlxData(void)
{
	omRecInfo.DeleteAll();
	ClearAll();

} // end ~CedaTlxData()


//
// ClearAll()
//
// Deletes omData
//
void CedaTlxData::ClearAll(void)
{
    omData.DeleteAll();

} // end ClearAll()




//
// Read()
//
// Reads all records from SECTAB loading them into omData
//
bool CedaTlxData::Read(const char *pcpWhere)
{
	CString olReadText;
	
//	ogDdx.Register((void *)this, TLX_INSERT, CString("TLXTABLE"),CString("TLXTAB INSERT"), CedaTlxDataCf);

	imCurrTelex = 0; // telex in omData which is currently displayed
	omData.DeleteAll();


    // Select data from the database
	char pclOrderBy[100] = {"ORDER BY TIME DESC"};
	char *pclWhere = new char[strlen(pcpWhere) + strlen(pclOrderBy) + 2];
	sprintf(pclWhere,"%s %s",pcpWhere,pclOrderBy);
	bool blRc = true;

	CWaitCursor olDummy;
	blRc = CedaAction2("RT", pclWhere);
	if (! blRc)
	{
		ogLog.Trace("LOADTLX","Read: Ceda-Error %d \n",blRc);

		// it's not an error if no data were found
		if(omLastErrorMessage.Find(ORA_NOT_FOUND) != -1)
			blRc = true;
	}
	else
	{
		// Load data from CedaData into the dynamic array of records
		for (int ilLc = 0; blRc; ilLc++)
		{
			TLXDATA *prpTlx = new TLXDATA;
			if ((blRc = GetBufferRecord2(ilLc,prpTlx)) == true)
			{
				olReadText.Format("reading %d %s.",ilLc+1,(ilLc==0)?"Telex":"Telexes");
//				olReadText.Format(GetString(IDS_STRING556),ilLc+1,(ilLc==0)?GetString(IDS_STRING557):GetString(IDS_STRING558));
				pomStaticFreeText->SetWindowText(olReadText);
				Add(prpTlx);
			}
			else
			{
				delete prpTlx;
			}
		}

		blRc = true;
	}

	int ilSize = omData.GetSize();
	ogLog.Trace("LOADSEC"," %d users loaded from %s",ilSize,pcmTableName);

	delete [] pclWhere;
	return blRc;


} // Read()

// ----------------------------------------------------------------------------------------------
// GetNextUrno()
//
// Read an URNO from the DB
//
//
long CedaTlxData::GetNextUrno( void )
{
	long llNextUrno = 0;
	if(CedaAction("GNU"))
	{
		CString olBuf;
		if(GetBufferLine(0, olBuf))
			llNextUrno = atol(olBuf);
	}

	return llNextUrno;
}


// ----------------------------------------------------------------------------------------------
// SendSBC()
//
// Send Broadcast
//
//
bool CedaTlxData::SendSBC(const char *pcpCommand, const char *pcpFields, const char *pcpData)
{
	bool blRc = true;
/*	char pclData[500];
	char pclFields[500];
	char pclWhere[50];


	strcpy(pclWhere,pcpCommand);

	if(strlen(pcpFields) > 0)
		sprintf(pclFields,"WKST,%s",ogWorkstation,pcpFields);
	else
		strcpy(pclFields,"WKST");

	if(strlen(pcpData) > 0)
		sprintf(pclData,"%s,%s",ogWorkstation,pcpData);
	else
		sprintf(pclData,"%s",ogWorkstation);

	// Action,TableName,FieldList,Selection,Sort,Data,Dest (eg "RETURN")
	blRc = CedaAction("SBC","SBCCOM",pclFields,pclWhere,"",pclData);

*/
	return blRc;

} // end SendSBC()



// ----------------------------------------------------------------------------------------------
// NewTelex()
//
// Insert a telex record into TLXTAB
//
//
bool CedaTlxData::NewTelex( TLXDATA *prpTlx )
{
	bool blRc = true;
	
	// get a new URNO
	prpTlx->URNO = GetNextUrno();

	if( prpTlx->URNO <= 0 )
	{
		blRc = false;
	}

	if( blRc )
	{
		prpTlx->CDAT = CTime::GetCurrentTime();
		prpTlx->TIME = prpTlx->CDAT;
		strcpy(prpTlx->USEC,ogUsername);

		CString olData;
		MakeCedaData(&omRecInfo,olData,prpTlx);

		char *pclData = new char[max(500,olData.GetLength())];
		strcpy(pclData,olData);
		blRc = CedaAction("IRT","","",pclData);


		delete [] pclData;
	}

	return blRc;

} // end NewTelex()



// ----------------------------------------------------------------------------------------------
// ResendTelex()
//
// Update a new telex record in TLXTAB
//
//
bool CedaTlxData::ResendTelex( TLXDATA *prpTlx )
{
	bool blRc = true;
	
	prpTlx->LSTU = CTime::GetCurrentTime();
	prpTlx->TIME = prpTlx->LSTU;
	strcpy(prpTlx->USEU,ogUsername);

	CString olData;
	MakeCedaData(&omRecInfo,olData,prpTlx);

	char pclWhere[500];
	char *pclData = new char[max(500,olData.GetLength())];
	strcpy(pclData,olData);

	sprintf(pclWhere, "WHERE URNO = %ld", prpTlx->URNO);
	blRc = CedaAction("URT",pclWhere,"",pclData);

	delete [] pclData;

	return blRc;

} // end ResendTelex()



// ----------------------------------------------------------------------------------------------
// ForwardTelex()
//
// Update a new telex record in TLXTAB
//
//
bool CedaTlxData::ForwardTelex( TLXDATA *prpTlx , const char *pcpNewText )
{
	bool blRc = true;


	// create a new telex record
	TLXDATA *prlNewTlx = new TLXDATA;
	strcpy(prlNewTlx->SERE,"S");
	strcpy(prlNewTlx->STAT,"S");
	strcpy(prlNewTlx->TTYP,prpTlx->TTYP);

	// copy the text to prpTlx->TXT1/2 (DB format)
	TranslateText(prlNewTlx,CString(pcpNewText),false);

	// insert the new telex
	blRc = NewTelex(prlNewTlx);

	if( !blRc )
	{
	}
	else
	{
		// set the original telex status to "G" to indicate that it has been forwarded
		blRc = SetStat("G");
	}

	if( !blRc )
	{
	}
	else
	{
		// if the new telex is within the select dates then add it to the list of currently displayed telexes
	}


	return blRc;

} // end ForwardTelex()



// ----------------------------------------------------------------------------------------------
// SetStat()
//
// Update the STAT field for the currently displayed record
//
// pcpStat		- status.
//
bool CedaTlxData::SetStat( const char *pcpStat )
{
	bool blRc = true;
	char pclData[500];
	char pclFields[500];
	char pclWhere[50];


	strcpy(pclFields,"STAT,LSTU,USEU");
	sprintf(pclWhere,"WHERE URNO=%d",omData[imCurrTelex].URNO);
	CTime olCurrTime = CTime::GetCurrentTime();
	sprintf(pclData,"%s,%s,%s",pcpStat,olCurrTime.Format("%Y%m%d%H%M%S"),ogUsername);

	// Action,TableName,FieldList,Selection,Sort,Data,Dest (eg "RETURN")
	blRc = CedaAction("URT",pcmTableName,pclFields,pclWhere,"",pclData);


	if( ! blRc )
	{
	}
	else
	{
		// update local data
		strcpy(omData[imCurrTelex].STAT,pcpStat);
		omData[imCurrTelex].LSTU = olCurrTime;
		strcpy(omData[imCurrTelex].USEU,ogUsername);
	}


	return blRc;

} // end SetStat()


// ----------------------------------------------------------------------------------------------
// SetFlightNum()
//
// Update ALC3,FLTN,FLNS fields for the currently displayed record
//
bool CedaTlxData::SetFlightNum( const char *pcpAlc3, const char *pcpFltn, const char *pcpFlns )
{
	bool blRc = true;
	char pclData[500];
	char pclFields[500];
	char pclWhere[50];

	strcpy(pclFields,"ALC3,FLTN,FLNS,LSTU,USEU");
	sprintf(pclWhere,"WHERE URNO=%d",omData[imCurrTelex].URNO);
	CTime olCurrTime = CTime::GetCurrentTime();
	sprintf(pclData,"%s,%s,%s,%s,%s",pcpAlc3,pcpFltn,pcpFlns,olCurrTime.Format("%Y%m%d%H%M%S"),ogUsername);

	// Action,TableName,FieldList,Selection,Sort,Data,Dest (eg "RETURN")
	blRc = CedaAction("URT",pcmTableName,pclFields,pclWhere,"",pclData);

	if( ! blRc )
	{
	}
	else
	{
		// update local data
		strcpy(omData[imCurrTelex].ALC3,pcpAlc3);
		strcpy(omData[imCurrTelex].FLTN,pcpFltn);
		strcpy(omData[imCurrTelex].FLNS,pcpFlns);
		omData[imCurrTelex].LSTU = olCurrTime;
		strcpy(omData[imCurrTelex].USEU,ogUsername);
	}

	return blRc;

} // end SetStat()



// ----------------------------------------------------------------------------------------------
// CheckTelex()
//
// Check the format of the telex using CTXHDL
//
bool CedaTlxData::CheckTelex( const char *pcpTelexText )
{
	bool blRc = true;
	char *pclData = new char[max(500,strlen(pcpTelexText)+1)];
	char pclFields[500];
	char pclWhere[50];

	strcpy(pclFields,"");
	strcpy(pclWhere,"");
	strcpy(pclData,pcpTelexText);

	// Action,TableName,FieldList,Selection,Sort,Data,Dest (eg "RETURN")
	blRc = CedaAction("CTC","",pclFields,pclWhere,"",pclData);

	if( ! blRc )
	{
	}
	else
	{
		// update local data
	}

	delete [] pclData;

	return blRc;

} // end CheckTelex()


// ----------------------------------------------------------------------------------------------
// Add()

bool CedaTlxData::Add(TLXDATA *prpTlx)
{

	omData.Add(prpTlx);

    return true;

} // end Add()


static void CedaTlxDataCf(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
    CedaTlxData *polThis = (CedaTlxData *)popInstance;
/*
	switch(ipDDXType) {

	case TLX_INSERT:
		polThis->Insert((TLXDATA *)vpDataPointer);
		break;
	case TLX_UPDATE:
        polThis->Update((TLXDATA *)vpDataPointer);
		break;
	case TLX_DELETE:
		// delete a TLX rec
        polThis->DeleteByFsec((char *)vpDataPointer);
		break;
	case SEC_DELETE:
		// SEC record deleted
        polThis->DeleteByFsec((char *)vpDataPointer);
		break;
	case SEC_DELAPP:
		// SEC application record deleted
        polThis->DeleteByFsec((char *)vpDataPointer);
		break;
	default:
		break;
	}
*/
}


// create a new telex record in local memory
void CedaTlxData::CreateNewTelex(const char *pcpTtyp)
{
	// delete existing telexes
	omData.DeleteAll();

	// current telex count
	imCurrTelex = 0;

	// create the new record
	TLXDATA *prpTlx = new TLXDATA;
	strcpy(prpTlx->TTYP,pcpTtyp);
	strcpy(prpTlx->SERE,"S");
	strcpy(prpTlx->STAT,"S");
	Add(prpTlx);

}

//
// Insert()
//
// Add a record to omData
//
// prpTlx - TLXDATA record to add
//
bool CedaTlxData::Insert(TLXDATA *prpTlx)
{

	Add(prpTlx);

    return true;

} // end Insert()


//
// Update()
//
// Update a record in omData
//
// pcpText - text for TXLTAB TXT1/TXT2
//
// SERE='S' and URNO undefined --> new telex (insert rec)
// SERE='S' and URNO defined --> resend of a new telex (update rec)
// SERE='R' --> forward of telex (insert new telex and update the existing)
//
bool CedaTlxData::Update(const char *pcpText)
{
	int ilTextLen = strlen(pcpText);
	bool blRc = true;
	TLXDATA *prlTlx = GetTlxRec(THIS_TELEX);

	if( prlTlx == NULL )
	{
		blRc = false;
	}

	if( blRc )
	{
		if( !strcmp(prlTlx->SERE,"S") ) // Sender (new) telex
		{
			// copy the text to prpTlx->TXT1/2 (DB format)
			TranslateText(prlTlx,CString(pcpText),false);

			if( prlTlx->URNO <= 0 ) // New telex
			{
				blRc = NewTelex(prlTlx);
			}
			else // Resend New telex
			{
				blRc = ResendTelex(prlTlx);
			}
		}
		else if( !strcmp(prlTlx->SERE,"R") ) // Receiver - forward the telex
		{
			blRc = ForwardTelex(prlTlx,pcpText);
		}
		else
		{
			blRc = false;
		}
	}


    return blRc;

} // end Update()

//-----TranslateText()--------------------------------------------------------------------------------------------

// Copies content of pclOldText to pclNewText, translating special chars and returns a pointer to pclNewText
// If blFromDb is true, translates from DB chars to display chars, else translates from display chars to DB chars
//
// DB chars		Display Chars
//   176    -->    34        
//   177    -->    39        
//   178    -->    44        
//   179    -->    10        
//
LPCTSTR CedaTlxData::TranslateText(TLXDATA *prpTlxRec, CString &ropText, bool blFromDb)
{
	int ilTextLen;
	char *pclOldText; // current text
	char *pclNewText; // translated text

	if(blFromDb)
	{
		ilTextLen = strlen(prpTlxRec->TXT1)+strlen(prpTlxRec->TXT2);

		if(ilTextLen <= 0)
		{
			strcpy(prpTlxRec->TXT1," ");
			ilTextLen = strlen(prpTlxRec->TXT1)+strlen(prpTlxRec->TXT2);
		}

		pclOldText = new char[ilTextLen+1]; // current text
		sprintf(pclOldText,"%s%s",prpTlxRec->TXT1,prpTlxRec->TXT2);

		pclNewText = new char[ilTextLen*2]; // translated text
		memset(pclNewText,0,(ilTextLen*2));
	}
	else
	{
		ilTextLen = ropText.GetLength();

		if(ilTextLen <= 0)
		{
			ropText = " ";
			ilTextLen = ropText.GetLength();
		}

		pclOldText = new char[ilTextLen+1]; // current text
		strcpy(pclOldText,ropText);

		pclNewText = new char[ilTextLen*2]; // translated text
		memset(pclNewText,0,(ilTextLen*2));
	}
	

	const short NUMCHARS = 5;
	char pclDbChars[NUMCHARS],pclDisplayChars[NUMCHARS];

//	pclDbChars[0] = (char) 176; pclDbChars[1] = (char) 177; pclDbChars[2] = (char) 178; pclDbChars[3] = (char) 179; 
	pclDbChars[0] = (char) 23; pclDbChars[1] = (char) 24; pclDbChars[2] = (char) 25; pclDbChars[3] = (char) 28;; pclDbChars[4] = (char) 29; 
	pclDisplayChars[0] = (char) 34; pclDisplayChars[1] = (char) 39; pclDisplayChars[2] = (char) 44; pclDisplayChars[3] = (char) 10; pclDisplayChars[4] = (char) 13;


	char *pclFromChars, *pclToChars;
	int ilLen = strlen(pclOldText), ilNew = 0, ilOld = 0;

	if( blFromDb )
	{
		// copy the text adding linefeeds to carriage returns, when required
		for( ; ilOld < ilLen; ilOld++ )
		{
			pclNewText[ilNew++] = pclOldText[ilOld];
			if( (int) pclOldText[ilOld] == 13  )
				pclNewText[ilNew++] = (char) 10;
		}
		
		// translate from DB format to display format
		pclFromChars = pclDbChars;
		pclToChars = pclDisplayChars;
	}
	else
	{
		// copy the text removing linefeeds
		for( ; ilOld < ilLen; ilOld++ )
			if( (int) pclOldText[ilOld] != 10 )
				pclNewText[ilNew++] = pclOldText[ilOld];

		// translate from display format to DB format
		pclFromChars = pclDisplayChars;
		pclToChars = pclDbChars;
	}

	char *pclChar;

	// loop through the list of chars to search for (pclFromChars), changing them to pclToChars in pclNewText
	for( int ilChar = 0; ilChar < NUMCHARS; ilChar++ )
		while((pclChar = strchr(pclNewText,pclFromChars[ilChar])) != NULL)
			*pclChar = pclToChars[ilChar];


	if(blFromDb)
	{
		// copy the new text to the return string
		ropText.Empty();
		ropText = CString(pclNewText);
	}
	else
	{
		// copy new text to the text fields
		memset(prpTlxRec->TXT1,0,TEXTLEN);
		memset(prpTlxRec->TXT2,0,TEXTLEN);
		CString olFormat;
		olFormat.Format("%%.%ds",TEXTLEN-1);
		sprintf(prpTlxRec->TXT1,olFormat,pclNewText); // copy the first TEXTLEN chars
		if(ilTextLen > TEXTLEN)
			strcpy(prpTlxRec->TXT2,&pclNewText[TEXTLEN-1]);
		else
			strcpy(prpTlxRec->TXT2," ");
	}

	delete [] pclOldText;
	delete [] pclNewText;

	return (LPCTSTR) ropText;

} // end TranslateText()

//-------------------------------------------------------------------------------------------------

//
// GetRec()
//
// return the FIRST,PREV,THIS,NEXT or LAST record text in a displayable format
//
TLXDATA *CedaTlxData::GetTlxRec( const int ipWhich )
{
	TLXDATA *polRec = NULL;

	int ilNumRecs = omData.GetSize();

	if( ilNumRecs > 0 )
	{
		switch(ipWhich) {
		
			case FIRST_TELEX:
				imCurrTelex = 0;
				break;
			case PREV_TELEX:
				imCurrTelex = max(imCurrTelex-1,0);
				break;
			case NEXT_TELEX:
				imCurrTelex = min(imCurrTelex+1,ilNumRecs-1);
				break;
			case LAST_TELEX:
				imCurrTelex = ilNumRecs-1;
				break;
			case THIS_TELEX:
			default:
				break;
		}
		polRec = &omData[imCurrTelex];
	}

	return polRec;


} // end GetRec()

//--ReadSpecialData-------------------------------------------------------------------------------------

bool CedaTlxData::ReadSpecialData(CCSPtrArray<TLXDATA> *popTlx,char *pspWhere,char *pspFieldList,bool ipSYS/*=true*/)
{
	bool ilRc = true;
	char pclFieldList[256] = " ";

	if(strlen(pspFieldList) > 0) 
	{
		strcpy(pclFieldList, pspFieldList);
	}
	if(ipSYS == true) 
	{
		if (CedaAction("SYS",pcmTableName,pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	else
	{
		if (CedaAction("RT",pcmTableName,pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	if(popTlx != NULL)
	{
		for (int ilLc = 0; ilRc == true; ilLc++)
		{
			TLXDATA *prpTlx = new TLXDATA;
			if ((ilRc = GetBufferRecord(ilLc,prpTlx,CString(pclFieldList))) == true)
			{
				popTlx->Add(prpTlx);
			}
			else
			{
				delete prpTlx;
			}
		}
		if(popTlx->GetSize() == 0) return false;
	}
    return true;
}

//-------------------------------------------------------------------------------------------------
