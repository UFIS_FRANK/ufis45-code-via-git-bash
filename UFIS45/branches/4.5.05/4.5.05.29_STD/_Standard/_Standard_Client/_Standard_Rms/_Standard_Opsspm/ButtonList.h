// ButtonList.h : header file
//
#ifndef _DBTNLIST_H
#define _DBTNLIST_H

//#include "peaktable.h"
#include <cxbutton.h>
#include <SetupDlg.h>
#include <CCSButtonctrl.h>
#include <RegnDiagram.h>
#include <GateDiagram.h>
#include <EquipmentDiagram.h>
#include <CciDiagram.h>
#include <GateTable.h>
#include <FlightJobsTable.h>
//#include "EquipmentTable.h"
#include <CciTable.h>
#include <PrmTable.h>
#include <PstDiagram.h>
#include <FlIndDemandTableViewer.h>
#include <FlIndDemandTable.h>
#include <DemandTable.h>
                   
class MReqTable;
class PrePlanTable;
class StaffTable;
class FlightPlan;
class StaffDiagram;
class ConfConflicts;
class ConflictTable;
class StaffBreakDiagramViewer;
class DemandTableViewer;
class ConflictTableViewer;

/////////////////////////////////////////////////////////////////////////////
// CButtonListDialog dialog
#define TRAFFIC_LIGHT_RED RGB(255,   0,   0)
#define TRAFFIC_LIGHT_GREEN RGB(0,   240,   0)
#define TRAFFIC_LIGHT_YELLOW RGB(255,   255,   0)

class CButtonListDialog : public CDialog
{
// Construction
public:
    CButtonListDialog(CWnd* pParent = NULL);    // standard constructor
	~CButtonListDialog(void);

	LONG OnBcAdd(UINT /*wParam*/, LONG /*lParam*/); 
	LONG OnCcsButton(UINT /*wParam*/, LONG /*lParam*/);

	void SetRequestColor(COLORREF lpColor);
	void SetInfoColor(COLORREF lpColor);
	void SetConflictColor(COLORREF lpColor);
	void SetAttentionColor(COLORREF lpColor);
	void SetDemandColor(COLORREF lpColor);
	void InitializeViewer();
	BOOL ExitCommitted();
	
	void UpdateTrafficLight(int ipState);

// Timer registration service
public:
	void RegisterTimer(CWnd *popWnd);
	void UnRegisterTimer(CWnd *popWnd);


private:
	CWnd *pomWndRequireTimer;	// the window which require the timer service
	void SetLoadMessage(const char *pcpMessage, int ipProgress=0);
	static void ProcessCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
	void HandleJobChange(JOBDATA *vpDataPointer);
	void HandleGlobalDateUpdate();
	void HandleSetupChange();
	void PrintBreakJobs();
	BOOL PrintBreakJob(JOBDATA *prlJobData);

	bool bmBroadcastReceived;
	int	 imBroadcastState;		// the broadcast state of the watch dog
								// 0 = initialized, 1 = data send, 2 = data received
// Dialog Data
public:
    int				m_nDialogBarHeight;
    PrePlanTable	*m_wndPrePlanTable;
    PrePlanTable	*m_wndPrePlanSearchTable;
    StaffTable		*m_wndStaffTable;
    FlightPlan		*m_wndFlightPlan;
	FlIndDemandTable *m_wndFlIndDemandTable;
	CPtrArray		omFlightPlanArray;
    
    StaffDiagram	*m_wndStaffDiagram;
    CCIDiagram		*m_wndCciDiagram;
	PRMTable	    *m_wndPrmTable;
    GateDiagram		*m_wndGateDiagram;
    EquipmentDiagram *m_wndEquipmentDiagram;
    PstDiagram		*m_wndPstDiagram;
	RegnDiagram		*m_wndRegnDiagram;
	ConfConflicts	*m_wndConfConflictTable;
	ConflictTable	*m_wndAttentionTable;
	ConflictTable	*m_wndConflictTable;
//	CCoverageDlg	*m_wndCoverage;
	CCITable		*m_wndCCITable;
	GateTable		*m_wndGateTable;
	FlightJobsTable	*m_wndFlightJobsTable;
//	EquipmentTable		*m_wndEquipmentTable;
	MReqTable		*m_wndMReqTable;
	MReqTable		*m_wndInfoTable;
//	PeakTable		*m_wndPeakTable;
	SetupDlg		*m_wndSetupDlg;
	DemandTable		*m_wndDemandTable;

    BOOL			m_flagOnLine;

    CTime			GetPrePlanTime();

	BOOL			omPrePlanMode;

	BOOL			bmPrintBreaks;
	CTimeSpan		omPrintBreakOffset;
	StaffBreakDiagramViewer* pomBreakViewer;
	DemandTableViewer*		 pomDemandViewer;
	ConflictTableViewer*	 pomAttentionViewer;
	CMapStringToOb *         pomMapDiagramToScaleDlg; //Singapore

    //{{AFX_DATA(CButtonListDialog)
	enum { IDD = IDD_BUTTONLIST };
	CComboBox	m_Date;
	CButton	m_ReleaseButton;
	CCSButtonCtrl	m_OnlineButton;
	C2StateButton	m_RegnDiagramButton;
    C2StateButton	m_StaffDiagramButton;
    C2StateButton	m_GateDiagramButton;
    C2StateButton	m_EquipmentDiagramButton;
    C2StateButton	m_PstDiagramButton;
    C2StateButton	m_CciDiagramButton;
    C2StateButton	m_StaffTableButton;
    CButton			m_FlightPlanButton;
	C2StateButton	m_FlIndDemandButton;
	C2StateButton	m_GateTableButton;
	C2StateButton	m_FlightJobsTableButton;
	C2StateButton	m_EquipmentTableButton;
	C2StateButton	m_CCITableButton;
	CCSButtonCtrl	m_RequestButton;
    C2StateButton	m_PeakTableButton;
    C2StateButton	m_CoverageButton;
    C2StateButton	m_PrePlanningButton;
	CCSButtonCtrl	m_AttentionTableButton;
	CCSButtonCtrl	m_ConflictTableButton;
	C2StateButton	m_SetupButton;
	CCSButtonCtrl	m_InfoButton;
    C2StateButton	m_HelpButton;
	C2StateButton	m_PrintButton;
	CCSButtonCtrl	*pomDemandTableButton;
	CCSButtonCtrl	m_CB_BCStatus;
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDialogAppDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	HICON	m_hIcon;
	bool	bmBroadcastCheckMsg;

    // Generated message map functions
    //{{AFX_MSG(CButtonListDialog)
    virtual void OnCancel();
    virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnDestroy();
    afx_msg BOOL OnEraseBkgnd(CDC* pDC);
    afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnReload(void);
    afx_msg void OnStaffdiagram();
    afx_msg void OnGatediagram();
    afx_msg void OnEquipmentDiagram();
    afx_msg void OnCcidiagram();
    afx_msg void OnPstdiagram();
    afx_msg void OnStafftable();
    afx_msg LONG OnStaffTableExit(UINT /*wParam*/, LONG /*lParam*/);
    afx_msg void OnFlightplan();
    afx_msg LONG OnFlightplanExit(UINT /*wParam*/, LONG /*lParam*/);
	afx_msg void OnFlIndDemand();
	afx_msg void OnGatebelegung();
    afx_msg LONG OnGateTableExit(UINT wParam, LONG lParam);
	afx_msg void OnFlightJobsTable();
    afx_msg LONG OnFlightJobsTableExit(UINT wParam, LONG lParam);
	afx_msg void OnEquipmentTable();
    afx_msg LONG OnEquipmentTableExit(UINT wParam, LONG lParam);
	afx_msg void OnCcibelegung();
	afx_msg LONG OnCCITableExit(UINT wParam, LONG lParam);
	afx_msg void OnRequest();
	afx_msg LONG OnRequestTableExit(UINT wParam, LONG lParam);
	afx_msg void OnPeaktable();
    afx_msg LONG OnPeaktableExit(UINT /*wParam*/, LONG /*lParam*/);
	afx_msg void OnCoverage();
    afx_msg LONG OnCoverageExit(UINT wParam, LONG lParam);
    afx_msg void OnPreplaning();
    afx_msg LONG OnPrePlanTableExit(UINT wParam, LONG lParam);
	afx_msg LONG OnPrePlanSearchTableExit(UINT /*wParam*/, LONG /*lParam*/);
	afx_msg void OnAttention();
	afx_msg LONG OnAttentionTableExit(UINT wParam, LONG lParam);
	afx_msg void OnConflict();
	afx_msg LONG OnConflictTableExit(UINT /*wParam*/, LONG /*lParam*/);
	afx_msg void OnUndo();
	afx_msg void OnSearch();
	afx_msg void OnRelease();
	afx_msg void OnInfo();
	afx_msg LONG OnInfoTableExit(UINT wParam, LONG lParam);
	afx_msg void OnSetup();
	afx_msg LONG OnSetupTableExit(UINT wParam, LONG lParam);
    afx_msg void OnHelp();
	afx_msg void OnLhHost();
    afx_msg void OnExit();
	afx_msg void OnOnline();
	afx_msg void OnClose();
	virtual void OnOK();
	afx_msg void OnRegndiagram();
	afx_msg void OnCloseAllFlightPlans();
	afx_msg void OnSelchangeDate();
	afx_msg void OnPrint();
    afx_msg void OnDemandTable();
	afx_msg LONG OnDemandTableExit(UINT wParam, LONG lParam);
	//}}AFX_MSG
    DECLARE_MESSAGE_MAP()


public:
	void OnClickMenu(UINT upMenuItem);
	void CleanupOnPrePlanExit();
	bool HandleOnline(void);

	void StartBroadcastReceived(void);
	void EndBroadcastReceived(void);

};

extern CButtonListDialog *pogButtonList;

#endif 
