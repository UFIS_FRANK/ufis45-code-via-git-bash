#ifndef PRINTTERMINALSELECTION_H_F0103BDA_9DEA_4296_9B9F_F2162E85E406_INCLUDED
#define PRINTTERMINALSELECTION_H_F0103BDA_9DEA_4296_9B9F_F2162E85E406_INCLUDED

#include "resource.h"
// PrintTerminalSelection.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CPrintTerminalSelection dialog

class CPrintTerminalSelection : public CDialog
{
// Construction
public:
	CPrintTerminalSelection(CWnd* pParent = NULL);   // standard constructor
	BOOL IsTerminal2Selected();
	BOOL IsTerminal3Selected();
	BOOL IsBothTerminalSelected();
	void SetWindowHeaderText(const CString& ropWindowHeaderText){omWindowHeaderText = ropWindowHeaderText;}

	enum { IDD = IDD_PRINTTERMINAL };
	int m_SelectedTerminal;

	static const int TERMINAL2_SELECTED;
	static const int TERMINAL3_SELECTED;
	static const int BOTH_TERMINAL_SELECTED;

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

private:
	CString omWindowHeaderText;

// Implementation
protected:

	virtual void OnOK();
	virtual BOOL OnInitDialog();

	DECLARE_MESSAGE_MAP()
};

#endif
