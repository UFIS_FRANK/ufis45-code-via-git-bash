// CedaCfgData.cpp - config data ie "Views"
//

#include <afxwin.h>
#include <resource.h>
#include <ccsglobl.h>
#include <CCSPtrArray.h>
#include <CCSCedaCom.h>
#include <CCSCedaData.h>
#include <ccsddx.h>
#include <CCSBcHandle.h>
#include <ccsddx.h>
#include <BasicData.h>
#include <CedaCfgData.h>
#include <cviewer.h>
#include <CedaJobData.h>


const char* CedaCfgData::pomDefaultName = "#__DEFAULT__#";

CedaCfgData::CedaCfgData()
{                  
    // Create an array of CEDARECINFO for CFGDATA
    BEGIN_CEDARECINFO(CFGDATA, CfgDataRecInfo)
        FIELD_LONG(Urno,"URNO")
        FIELD_CHAR_TRIM(Appn,"APPN")
        FIELD_CHAR_TRIM(Ctyp,"CTYP")
        FIELD_CHAR_TRIM(Ckey,"CKEY")
        FIELD_CHAR_TRIM(Vafr,"VAFR")
        FIELD_CHAR_TRIM(Vato,"VATO")
        FIELD_CHAR_TRIM(Pkno,"PKNO")
        FIELD_CHAR_TRIM(Text,"TEXT")
    END_CEDARECINFO

    // Copy the record structure
    for (int i = 0; i < sizeof(CfgDataRecInfo)/sizeof(CfgDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&CfgDataRecInfo[i],sizeof(CEDARECINFO));
        omRecInfo.Add(prpCedaRecInfo);
	}

    // Initialize table names and field names
    strcpy(pcmTableName,"VCDTAB");
    pcmFieldList = "URNO,APPN,CTYP,CKEY,VAFR,VATO,PKNO,TEXT";

	ogCCSDdx.Register((void *)this,BC_CFG_CHANGE,CString("BC_CfgChange"), CString("CfgDataChange"),ProcessCfgCf);
	ogCCSDdx.Register((void *)this,BC_CFG_INSERT,CString("BC_CfgInsert"), CString("CfgDataChange"),ProcessCfgCf);

	char pclTmpText[512];
	char pclConfigPath[512];

    if (getenv("CEDA") == NULL)
        strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
    else
        strcpy(pclConfigPath, getenv("CEDA"));

	GetPrivateProfileString(pcgAppName, "CCATAB", "NONE", pclTmpText, sizeof pclTmpText, pclConfigPath);
	if (stricmp(pclTmpText,"READONLY") == 0)
	{
		rmUserSetup.OPTIONS.CCA = USERSETUPDATA::READONLY;		
	}
	else if (stricmp(pclTmpText,"READWRITE") == 0)
	{
		rmUserSetup.OPTIONS.CCA = USERSETUPDATA::READWRITE;		
	}
	else
	{
		rmUserSetup.OPTIONS.CCA = USERSETUPDATA::NONE;		
	}

	GetPrivateProfileString(pcgAppName, "CIC_CHECK_LINK", "YES", pclTmpText, sizeof pclTmpText, pclConfigPath);
	if (stricmp(pclTmpText,"YES") == 0)
	{
		rmUserSetup.OPTIONS.CIC_CHECK_LINK = TRUE;		
	}
	else if (stricmp(pclTmpText,"NO") == 0)
	{
		rmUserSetup.OPTIONS.CIC_CHECK_LINK = FALSE;		
	}
	else
	{
		rmUserSetup.OPTIONS.CIC_CHECK_LINK = TRUE;		
	}
}

CedaCfgData::~CedaCfgData()
{
	TRACE("CedaCfgData::~CedaCfgData called\n");
	ogCCSDdx.UnRegister(this,NOTUSED);
	omData.DeleteAll();
	//omViews.DeleteAll();
	omUrnoMap.RemoveAll();
	omCkeyMap.RemoveAll();
	omRecInfo.DeleteAll();
	ClearAllViews();
}

// read general config data for the Views
CCSReturnCode CedaCfgData::ReadCfgData()
{
    char pclWhere[1024];
	CCSReturnCode ilRc = RCSuccess;

	// WHERE conditions are not mapped so a user such as UFIS$ADMIN is not found because '$' in the field PKNO has been mapped
	CString olUserName = ogUsername;
	MakeCedaString(olUserName);
    
    sprintf(pclWhere,"WHERE APPN='%s' AND PKNO='%s' AND CTYP != 'VIEW-DATA' AND CTYP NOT LIKE 'CONFLICT%%'",pcgAppName,olUserName);
	ilRc = CedaAction2("RT", pclWhere);

    // Load data from CCSCedaData into the dynamic array of record
    omCkeyMap.RemoveAll();
	omUrnoMap.RemoveAll();
    omData.DeleteAll();

    for (int ilLc = 0; ilRc == RCSuccess; ilLc++)
    {
		CFGDATA *prlCfg = new CFGDATA;
		if ((ilRc = GetBufferRecord2(ilLc,prlCfg)) == RCSuccess)
		{
			PrepareCfgData(prlCfg);
			omData.Add(prlCfg);
			omCkeyMap.SetAt(prlCfg->Ckey,prlCfg);
			omUrnoMap.SetAt((void *)prlCfg->Urno,prlCfg);
		}
		else
		{
			delete prlCfg;
		}
	}

	MakeCurrentUser(); 

// this section is responsible for the VIEWS in charts and lists
	ilRc = RCSuccess;


    sprintf(pclWhere, "WHERE APPN='%s' AND CTYP='VIEW-DATA' AND PKNO='%s'", pcgAppName, olUserName);
    if (CedaAction2("RT", pclWhere) == RCFailure)
        return RCFailure;

	//Initialize the datastructure of for the views
	ClearAllViews();
    for (ilLc = 0; ilRc == RCSuccess; ilLc++)
    {
		CFGDATA rlCfg;
		if ((ilRc = GetBufferRecord2(ilLc,&rlCfg)) == RCSuccess)
		{
			PrepareViewData(&rlCfg);
		}
	}

	ogBasicData.Trace("CedaCfgData Read %d Records (Rec Size %d) %s",omData.GetSize(),sizeof(CFGDATA), pclWhere);
    return RCSuccess;
}


void CedaCfgData::CfgRecordFromConflict(const CString& popUsername,const CString& popRecord,CFGDATA& popCfgData)
{
	int ilMaxFields = 11;
	int ilMaxBytes  = popRecord.GetLength();

	int ilIndex;
	long llType;
	CString olName;
	int ilWeight;
	int ilTime;
	int ilNSymbol;
	int ilCSymbol;
	COLORREF olNCol;
	COLORREF olCCol;
	CString	 olDscr;
	bool	 blEnabled;


	// the following are for debugging only
	BYTE bRed, bBlue, bGreen;

	for (int ilFieldCount = 0, ilByteCount = 0; ilFieldCount < ilMaxFields; ilFieldCount++)
   	{
        // Extract next field in the specified text-line
		for (int ilFirstByteInField = ilByteCount; ilByteCount < ilMaxBytes && popRecord[ilByteCount] != ';'; ilByteCount++)
			;
		if(ilByteCount <= ilMaxBytes)
		{
			CString olField = popRecord.Mid(ilFirstByteInField,ilByteCount-ilFirstByteInField);
			ilByteCount++;
			switch(ilFieldCount)
			{
			case 0 : ilIndex = atol((LPCSTR)olField);
				break;
			case 1 : llType  = atol((LPCSTR)olField);
				break;
			case 2 : olName  = olField;
				break;
			case 3 : ilWeight= atoi((LPCSTR)olField);
				break;
			case 4 : ilTime  = atoi((LPCSTR)olField);
				break;
			case 5 : ilNSymbol = (int)olField[0];
				break;
			case 6 : ilCSymbol = (int)olField[0];
				break;
			case 7 : olNCol = (COLORREF) atol((LPCSTR)olField);
				bRed = GetRValue(olNCol);
				bGreen = GetGValue(olNCol);
				bBlue = GetBValue(olNCol);
				break;
			case 8 : olCCol = (COLORREF) atol((LPCSTR)olField);
				bRed = GetRValue(olCCol);
				bGreen = GetGValue(olCCol);
				bBlue = GetBValue(olCCol);
				break;
			case 9 : olDscr = olField;
				break;
			case 10: blEnabled = (olField == "1") ? true : false;
				break;
			}
		}
	}

	popCfgData.Urno = ogBasicData.GetNextUrno();
	sprintf(popCfgData.Ctyp, "CONFLICT%02d", ilIndex);
	strncpy(popCfgData.Pkno,(LPCSTR)popUsername,sizeof(popCfgData.Pkno));
	popCfgData.Pkno[sizeof(popCfgData.Pkno)-1] = '\0';
	strncpy(popCfgData.Text,(LPCSTR)popRecord,sizeof(popCfgData.Text));
	popCfgData.Text[sizeof(popCfgData.Text)-1] = '\0';

}

// read conflict config data
CCSReturnCode CedaCfgData::ReadConflicts(const CString& opUserName,CStringArray &opLines)
{
	char pclWhere[1024]="";
	int ilLc = 0;

	opLines.RemoveAll();
	CCSReturnCode ilRc = RCSuccess;
	CCSReturnCode olRc = RCSuccess;
	ilRc = RCSuccess;

	// WHERE conditions are not mapped so a user such as UFIS$ADMIN is not found because '$' in the field PKNO has been mapped
	CString olUserName = opUserName;
	MakeCedaString(olUserName);

    
    //added by MAX
	int maxNum=NUMCONFLICTS;//max conflict is 56
	if (ogBasicData.myNewConflict==false)
	{
		maxNum--; //max conflict is 55
		sprintf(pclWhere, "WHERE APPN='%s' AND CTYP LIKE 'CONFLICT%%' AND PKNO = '%s' AND CTYP <> 'CONFLICT55' ORDER BY CTYP", pcgAppName,olUserName);
	}
	else
		sprintf(pclWhere, "WHERE APPN='%s' AND CTYP LIKE 'CONFLICT%%' AND PKNO = '%s' ORDER BY CTYP", pcgAppName,olUserName);

	olRc = CedaAction2("RT", pclWhere);

	
	if(olRc == RCFailure)
	{
		if(omLastErrorMessage == "No Data Found")
		{
			olRc = RCSuccess;
		}
	}
	else
	{
		ilLc = 0;
		while(ilRc == RCSuccess)
		{
			CFGDATA rlCfg;
			CString olTxt;
			if ((ilRc = GetBufferRecord2(ilLc,&rlCfg)) == RCSuccess)
			{
				
				if(ilLc < maxNum)
				{
					olTxt = CString(rlCfg.Text);
					opLines.Add(olTxt);
				}
				else
				{
					olTxt.Format("Conflict <%d> ignored because exceeds NUMCONFLICTS <%d>\n%s",ilLc,NUMCONFLICTS,rlCfg.Text);
				}
				ogBasicData.Trace("%s\n",olTxt);
				ilLc++;
			}
		}
	}

	return olRc;
}

void CedaCfgData::GetDefaultConflictSetup(CStringArray &opLines)
{
	opLines.RemoveAll();
	CString olConflictColour; // Colours for conflict & confirmed-conflict

	// Kein Konflikt
	olConflictColour.Format("%ld;%ld;",RGB(0,255,0),RGB(0,255,0));
	opLines.Add(CString("0;0;" + GetString(IDS_STRING32100) + ";0;0;!;?;" + olConflictColour + GetString(IDS_STRING32101) + CString(";1")));
	// Delay
	olConflictColour.Format("%ld;%ld;",RGB(255,255,0),RGB(128,128,255));
	opLines.Add(CString("1;1;" + GetString(IDS_STRING32102) + ";70;9;6;6;" + olConflictColour + GetString(IDS_STRING32103) + CString(";1")));
	// Cancellation
	olConflictColour.Format("%ld;%ld;",RGB(0,0,255),RGB(0,0,255));
	opLines.Add(CString("2;2;" + GetString(IDS_STRING32104) + ";97;0;!;?;" + olConflictColour + GetString(IDS_STRING32105) + CString(";1")));
	// Diversion
	olConflictColour.Format("%ld;%ld;",RGB(0,0,255),RGB(0,0,255));
	opLines.Add(CString("3;3;" + GetString(IDS_STRING32106) + ";96;0;!;?;" + olConflictColour + GetString(IDS_STRING32107) + CString(";1")));
	// Einsatzueberlappung
	olConflictColour.Format("%ld;%ld;",RGB(255,0,0),RGB(255,128,64));
	opLines.Add(CString("4;4;" + GetString(IDS_STRING32108) + ";65;9;!;?;" + olConflictColour + GetString(IDS_STRING32109) + CString(";1")));
	// Bestätigung
	olConflictColour.Format("%ld;%ld;",RGB(255,0,0),RGB(255,128,64));
	opLines.Add(CString("5;5;" + GetString(IDS_STRING32110) + ";60;0;!;?;" + olConflictColour + GetString(IDS_STRING32111) + CString(";1")));
	// Gate Change
	olConflictColour.Format("%ld;%ld;",RGB(128,128,0),RGB(0,255,0));
	opLines.Add(CString("6;6;" + GetString(IDS_STRING32112) + ";30;90;!;?;" + olConflictColour + GetString(IDS_STRING32113) + CString(";0")));
	// Equipment Change
	olConflictColour.Format("%ld;%ld;",RGB(0,128,128),RGB(0,255,0));
	opLines.Add(CString("7;7;" + GetString(IDS_STRING32114) + ";29;0;!;?;" + olConflictColour + GetString(IDS_STRING32115) + CString(";1")));
	// Overbooking
	olConflictColour.Format("%ld;%ld;",RGB(255,128,255),RGB(0,255,0));
	opLines.Add(CString("8;8;" + GetString(IDS_STRING32116) + ";35;0;!;?;" + olConflictColour + GetString(IDS_STRING32117) + CString(";0")));
	// Keine Rückmeldung
	olConflictColour.Format("%ld;%ld;",RGB(255,0,0),RGB(255,128,0));
	opLines.Add(CString("9;9;" + GetString(IDS_STRING32118) + ";59;0;!;?;" + olConflictColour + GetString(IDS_STRING32119) + CString(";1")));
	// Outside shift
	olConflictColour.Format("%ld;%ld;",RGB(255,0,0),RGB(255,128,64));
	opLines.Add(CString("10;10;" + GetString(IDS_STRING32120) + ";62;0;!;?;" + olConflictColour + GetString(IDS_STRING32121) + CString(";1")));
	// Positionswechsel
	olConflictColour.Format("%ld;%ld;",RGB(128,128,0),RGB(0,255,0));
	opLines.Add(CString("11;11;" + GetString(IDS_STRING32122) + ";30;0;!;?;" + olConflictColour + GetString(IDS_STRING32123) + CString(";1")));
	// Aircraft Change
	olConflictColour.Format("%ld;%ld;",RGB(0,128,128),RGB(0,255,0));
	opLines.Add(CString("12;12;" + GetString(IDS_STRING32124) + ";31;0;!;?;" + olConflictColour + GetString(IDS_STRING32125) + CString(";1")));

	// Kein Gate Bedarf
	olConflictColour.Format("%ld;%ld;",RGB(128,255,255),RGB(128,255,255));
	opLines.Add(CString("13;13;" + GetString(IDS_STRING32126) + ";99;0;!;?;" + olConflictColour + GetString(IDS_STRING32127) + CString(";0")));
	// Kein Gate Einsatz
	olConflictColour.Format("%ld;%ld;",RGB(255,0,0),RGB(255,0,0));
	opLines.Add(CString("14;14;" + GetString(IDS_STRING32128) + ";80;60;!;?;" + olConflictColour + GetString(IDS_STRING32129) + CString(";0")));
	// Gate Unterdeckung
	olConflictColour.Format("%ld;%ld;",RGB(255,0,0),RGB(255,128,0));
	opLines.Add(CString("15;15;" + GetString(IDS_STRING32130) + ";79;60;!;?;" + olConflictColour + GetString(IDS_STRING32131) + CString(";0")));
	// Gate Unterdeckung
	olConflictColour.Format("%ld;%ld;",RGB(255,128,0),RGB(255,255,128));
	opLines.Add(CString("16;16;" + GetString(IDS_STRING32132) + ";78;60;!;?;" + olConflictColour + GetString(IDS_STRING32133) + CString(";0")));
	// Gate Bedarfsüberdeckung
	olConflictColour.Format("%ld;%ld;",RGB(0,128,0),RGB(0,128,0));
	opLines.Add(CString("17;17;" + GetString(IDS_STRING32134) + ";55;60;!;?;" + olConflictColour + GetString(IDS_STRING32135) + CString(";0")));

	// Kein Checkin Bedarf
	olConflictColour.Format("%ld;%ld;",RGB(128,255,255),RGB(128,255,255));
	opLines.Add(CString("18;18;" + GetString(IDS_STRING32136) + ";99;0;!;?;" + olConflictColour + GetString(IDS_STRING32137) + CString(";0")));
	// Kein Checkin Einsatz
	olConflictColour.Format("%ld;%ld;",RGB(255,0,0),RGB(255,0,0));
	opLines.Add(CString("19;19;" + GetString(IDS_STRING32138) + ";80;240;!;?;" + olConflictColour + GetString(IDS_STRING32139) + CString(";0")));
	// Checkin Unterdeckung
	olConflictColour.Format("%ld;%ld;",RGB(255,0,0),RGB(255,128,0));
	opLines.Add(CString("20;20;" + GetString(IDS_STRING32140) + ";79;240;!;?;" + olConflictColour + GetString(IDS_STRING32141) + CString(";0")));
	// Checkin Unterdeckung
	olConflictColour.Format("%ld;%ld;",RGB(255,128,0),RGB(255,255,128));
	opLines.Add(CString("21;21;" + GetString(IDS_STRING32142) + ";78;240;!;?;" + olConflictColour + GetString(IDS_STRING32143) + CString(";0")));
	// Checkin Bedarfsüberdeckung
	olConflictColour.Format("%ld;%ld;",RGB(0,128,0),RGB(0,128,0));
	opLines.Add(CString("22;22;" + GetString(IDS_STRING32144) + ";55;240;!;?;" + olConflictColour + GetString(IDS_STRING32145) + CString(";0")));

	// Kein Position Bedarf
	olConflictColour.Format("%ld;%ld;",RGB(128,255,255),RGB(128,255,255));
	opLines.Add(CString("23;23;" + GetString(IDS_STRING32146) + ";99;0;!;?;" + olConflictColour + GetString(IDS_STRING32147) + CString(";1")));
	// Kein Position Einsatz
	olConflictColour.Format("%ld;%ld;",RGB(255,0,0),RGB(255,0,0));
	opLines.Add(CString("24;24;" + GetString(IDS_STRING32148) + ";80;60;!;?;" + olConflictColour + GetString(IDS_STRING32149) + CString(";1")));
	// Position Unterdeckung
	olConflictColour.Format("%ld;%ld;",RGB(255,0,0),RGB(255,128,0));
	opLines.Add(CString("25;25;" + GetString(IDS_STRING32150) + ";79;60;!;?;" + olConflictColour + GetString(IDS_STRING32151) + CString(";1")));
	// Position Unterdeckung
	olConflictColour.Format("%ld;%ld;",RGB(255,128,0),RGB(255,255,128));
	opLines.Add(CString("26;26;" + GetString(IDS_STRING32152) + ";78;60;!;?;" + olConflictColour + GetString(IDS_STRING32153) + CString(";1")));
	// Position Bedarfsüberdeckung
	olConflictColour.Format("%ld;%ld;",RGB(0,128,0),RGB(0,128,0));
	opLines.Add(CString("27;27;" + GetString(IDS_STRING32154) + ";60;60;!;?;" + olConflictColour + GetString(IDS_STRING32155) + CString(";1")));

	// Kein Registration Bedarf
	olConflictColour.Format("%ld;%ld;",RGB(128,255,255),RGB(128,255,255));
	opLines.Add(CString("28;28;" + GetString(IDS_NOREGNDEMAND1) + ";99;0;!;?;" + olConflictColour + GetString(IDS_NOREGNDEMAND2) + CString(";0")));
	// Kein Registration Einsatz
	olConflictColour.Format("%ld;%ld;",RGB(255,0,0),RGB(255,0,0));
	opLines.Add(CString("29;29;" + GetString(IDS_NOREGNJOBS1) + ";80;60;!;?;" + olConflictColour + GetString(IDS_NOREGNJOBS2) + CString(";0")));
	// Registration Unterdeckung
	olConflictColour.Format("%ld;%ld;",RGB(255,0,0),RGB(255,128,0));
	opLines.Add(CString("30;30;" + GetString(IDS_LOWREGN1) + ";79;60;!;?;" + olConflictColour + GetString(IDS_LOWREGN2) + CString(";0")));
	// Registration Unterdeckung
	olConflictColour.Format("%ld;%ld;",RGB(255,128,0),RGB(0,255,0));
	opLines.Add(CString("31;31;" + GetString(IDS_PARTIALREGN1) + ";78;60;!;?;" + olConflictColour + GetString(IDS_PARTIALREGN2) + CString(";0")));
	// Registration Bedarfsüberdeckung
	olConflictColour.Format("%ld;%ld;",RGB(0,128,0),RGB(0,128,0));
	opLines.Add(CString("32;32;" + GetString(IDS_EXCESSREGN1) + ";55;60;!;?;" + olConflictColour + GetString(IDS_EXCESSREGN2) + CString(";0")));

	// First Class Overbooked
	olConflictColour.Format("%ld;%ld;",RGB(255,128,255),RGB(0,255,0));
	opLines.Add(CString("33;33;" + GetString(IDS_FIRSTOVERBOOKED1) + ";34;0;!;?;" + olConflictColour + GetString(IDS_FIRSTOVERBOOKED2) + CString(";0")));
	// Business Class Overbooked
	olConflictColour.Format("%ld;%ld;",RGB(255,128,255),RGB(0,255,0));
	opLines.Add(CString("34;34;" + GetString(IDS_BUSINESSOVERBOOKED1) + ";33;0;!;?;" + olConflictColour + GetString(IDS_BUSINESSOVERBOOKED2) + CString(";0")));
	// Economy Class Overbooked
	olConflictColour.Format("%ld;%ld;",RGB(255,128,255),RGB(0,255,0));
	opLines.Add(CString("35;35;" + GetString(IDS_ECONOMYOVERBOOKED1) + ";32;0;!;?;" + olConflictColour + GetString(IDS_ECONOMYOVERBOOKED2) + CString(";0")));

	// Advice time
	olConflictColour.Format("%ld;%ld;",RGB(255,255,0),RGB(128,128,255));
	opLines.Add(CString("36;36;" + GetString(IDS_ADVICETIMECFL1) + ";98;0;!;?;" + olConflictColour + GetString(IDS_ADVICETIMECFL2) + CString(";1")));

	// Invalid Job-Demand Combination
	olConflictColour.Format("%ld;%ld;",RGB(255,128,128),RGB(255,128,128));
	opLines.Add(CString("37;37;" + GetString(IDS_JOBDEMANDCFL1) + ";34;60;!;?;" + olConflictColour + GetString(IDS_JOBDEMANDCFL2) + CString(";1")));

	// Demand without flight - Registration demands that currently have no flight
	olConflictColour.Format("%ld;%ld;",RGB(255,128,0),RGB(255,128,64));
	opLines.Add(CString("38;38;" + GetString(IDS_DEMWITHOUTFLIGHT1) + ";75;0;!;?;" + olConflictColour + GetString(IDS_DEMWITHOUTFLIGHT2) + CString(";0")));

	// Demand - currently for demands with AZATAB records where a default time was given
	olConflictColour.Format("%ld;%ld;",RGB(255,0,0),RGB(255,128,0));
	opLines.Add(CString("39;39;" + GetString(IDS_DEMDEFAULTVALUES1) + ";85;0;!;?;" + olConflictColour + GetString(IDS_DEMDEFAULTVALUES2) + CString(";0")));

	// Kein Equipment Bedarf
	olConflictColour.Format("%ld;%ld;",RGB(128,255,255),RGB(128,255,255));
	opLines.Add(CString("40;40;" + GetString(IDS_NOEQUDEMAND1) + ";99;0;!;?;" + olConflictColour + GetString(IDS_NOEQUDEMAND2) + CString(";0")));

	// Kein Equipment Einsatz
	olConflictColour.Format("%ld;%ld;",RGB(255,0,0),RGB(255,0,0));
	opLines.Add(CString("41;41;" + GetString(IDS_NOEQUJOBS1) + ";80;0;!;?;" + olConflictColour + GetString(IDS_NOEQUJOBS2) + CString(";0")));

	// Equipment Unterdeckung
	olConflictColour.Format("%ld;%ld;",RGB(255,0,0),RGB(255,128,0));
	opLines.Add(CString("42;42;" + GetString(IDS_LOWEQU1) + ";79;0;!;?;" + olConflictColour + GetString(IDS_LOWEQU2) + CString(";0")));

	// Equipment Unterdeckung
	olConflictColour.Format("%ld;%ld;",RGB(255,128,0),RGB(255,255,128));
	opLines.Add(CString("43;43;" + GetString(IDS_PARTIALEQU1) + ";78;0;!;?;" + olConflictColour + GetString(IDS_PARTIALEQU2) + CString(";0")));

	// Equipment Bedarfsüberdeckung
	olConflictColour.Format("%ld;%ld;",RGB(0,128,0),RGB(0,128,0));
	opLines.Add(CString("44;44;" + GetString(IDS_EXCESSEQU1) + ";55;0;!;?;" + olConflictColour + GetString(IDS_EXCESSEQU2) + CString(";0")));

	// emp not clocked on
	olConflictColour.Format("%ld;%ld;",RGB(255,0,0),RGB(255,128,0));
	opLines.Add(CString("45;45;" + GetString(IDS_SHIFTNOTSTARTED1) + ";66;10;!;?;" + olConflictColour + GetString(IDS_SHIFTNOTSTARTED2) + CString(";1")));

	// employee's pool job set to absent - so all jobs assigned are in conflict
	olConflictColour.Format("%ld;%ld;",RGB(255,0,0),RGB(255,128,0));
	opLines.Add(CString("46;46;" + GetString(IDS_EMPLOYEEABSENT1) + ";67;60;!;?;" + olConflictColour + GetString(IDS_EMPLOYEEABSENT2) + CString(";1")));

	// equ job overlaps with equ not available time
	olConflictColour.Format("%ld;%ld;",RGB(255,0,0),RGB(255,128,0));
	opLines.Add(CString("47;47;" + GetString(IDS_EQU_UNAVAILABLE1) + ";67;60;!;?;" + olConflictColour + GetString(IDS_EQU_UNAVAILABLE2) + CString(";1")));

	// return flight
	olConflictColour.Format("%ld;%ld;",RGB(255,0,0),RGB(255,128,0));
	opLines.Add(CString("48;48;" + GetString(IDS_RETURNFLIGHT1) + ";67;60;!;?;" + olConflictColour + GetString(IDS_RETURNFLIGHT2) + CString(";1")));

	// return from taxi
	olConflictColour.Format("%ld;%ld;",RGB(255,0,0),RGB(255,128,0));
	opLines.Add(CString("49;49;" + GetString(IDS_RETURNFROMTAXI1) + ";67;60;!;?;" + olConflictColour + GetString(IDS_RETURNFROMTAXI2) + CString(";1")));

	// Employee with shift code for terminal T2 is assigned to job under terminal T3
	olConflictColour.Format("%ld;%ld;",RGB(255,0,0),RGB(255,128,0));
	opLines.Add(CString("50;50;" + GetString(IDS_JOBTERMINALCONFLICT1) + ";67;60;!;?;" + olConflictColour + GetString(IDS_JOBTERMINALCONFLICT1) + CString(";1")));

	// Employee with shift code for terminal T3 is assigned to job under terminal T2
	olConflictColour.Format("%ld;%ld;",RGB(255,0,0),RGB(255,128,0));
	opLines.Add(CString("51;51;" + GetString(IDS_JOBTERMINALCONFLICT2) + ";67;60;!;?;" + olConflictColour + GetString(IDS_JOBTERMINALCONFLICT2) + CString(";1")));
	
	// Flight meant to depart from terminal T2 will depart from terminal T3
	olConflictColour.Format("%ld;%ld;",RGB(255,0,0),RGB(255,128,0));
	opLines.Add(CString("52;52;" + GetString(IDS_FLIGHTTERMINALCONFLICT1) + ";67;60;!;?;" + olConflictColour + GetString(IDS_FLIGHTTERMINALCONFLICT1) + CString(";1")));
	
	// Flight meant to depart from terminal T3 will depart from terminal T2
	olConflictColour.Format("%ld;%ld;",RGB(255,0,0),RGB(255,128,0));
	opLines.Add(CString("53;53;" + GetString(IDS_FLIGHTTERMINALCONFLICT2) + ";67;60;!;?;" + olConflictColour + GetString(IDS_FLIGHTTERMINALCONFLICT2) + CString(";1")));

	// Flight with No-opearation status
	olConflictColour.Format("%ld;%ld;",RGB(255,0,0),RGB(255,128,0));
	opLines.Add(CString("54;54;" + GetString(IDS_FLIGHT_NO_OPERATION) + ";67;60;!;?;" + olConflictColour + GetString(IDS_FLIGHT_NO_OPERATION_DESC) + CString(";1")));

	// Job Coverage
	if (ogBasicData.myNewConflict==true)
	{
		olConflictColour.Format("%ld;%ld;",RGB(255,0,0),RGB(255,128,0));
		opLines.Add(CString("55;55;" + GetString(ID_61928) + ";67;60;!;?;" + olConflictColour + GetString(ID_61929) + CString(";1;70")));
	}
}


bool CedaCfgData::ResetToDefault(void)
{

	bool blResetToDefault = false;
	char pclTmpText[512];
	char pclConfigPath[512];
	char pclKeyword[521];

    if (getenv("CEDA") == NULL)
        strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
    else
        strcpy(pclConfigPath, getenv("CEDA"));

	strcpy(pclKeyword,"RESET_CONFLICTS_TO_DEFAULT");
    GetPrivateProfileString(pcgAppName, pclKeyword, "NO",pclTmpText, sizeof pclTmpText, pclConfigPath);
	if(!stricmp(pclTmpText,"YES"))
	{
		blResetToDefault = true;
	}
	return blResetToDefault;
}

CCSReturnCode CedaCfgData::SaveConflictData(const CString& opUserName,CFGDATA *prpCfg)
{
	CString olListOfData;
	CCSReturnCode olRc = RCSuccess;
	char pclData[824];
	char pclSelection[1024]="";

	// WHERE conditions are not mapped so a user such as UFIS$ADMIN is not found because '$' in the field PKNO has been mapped
	CString olUserName = opUserName;
	MakeCedaString(olUserName);
	
	//First we delete the entry
	sprintf(pclSelection,"WHERE APPN='%s' AND (PKNO = '%s' AND CTYP = '%s')", pcgAppName, olUserName, prpCfg->Ctyp);
	olRc = CedaAction("DRT",pclSelection);

	//and create it anew
	MakeCedaData(olListOfData,prpCfg);
	strcpy(pclData,olListOfData);
	if((olRc = CedaAction("IRT","","",pclData))  == RCFailure)
	{
		ogBasicData.LogCedaError("CedaCfgData Error",omLastErrorMessage,"IRT",pcmFieldList,pcmTableName,"");
	}

	return olRc;
}

void CedaCfgData::MakeCurrentUser()
{
	// Fills the setup struct for the user with current login-USERID
	CCSPtrArray<CFGDATA> olData;
	int ilCountSets = 0;
	int ilCount = omData.GetSize();
	for(int i = 0; i < ilCount; i++)
	{
		CFGDATA rlC = omData[i];
		if((strcmp(rlC.Pkno, ogUsername) == 0) && (strcmp(rlC.Ctyp, "CKI-SETUP") == 0))
		{
			olData.NewAt(0, rlC);
			ilCountSets++;
		}
	}
	
	if(ilCountSets == 2)
	{
		CString olParameters;
		CString olTmp1, olTmp2;
		CFGDATA rlC1 = olData.GetAt(0);
		CFGDATA rlC2 = olData.GetAt(1);
		olTmp1 = rlC1.Text;
		olTmp2 = rlC2.Text;
		olParameters = olTmp1 + olTmp2;
		InterpretSetupString(olParameters, &rmUserSetup);
	}
	//  Wenn Resolution im Ini-File mit gültigen Wert eingetragen, überschreibt
	//  dies die Einstellung in CFG-Data				//hag20000612
	if ( ogBasicData.imScreenResolutionX == 800 )
		rmUserSetup.RESO = "800x600" ;
	else if ( ogBasicData.imScreenResolutionX == 1024 )
		rmUserSetup.RESO = "1024x768" ;
	else if ( ogBasicData.imScreenResolutionX == 1280 )
		rmUserSetup.RESO = "1280x1024" ;

	olData.DeleteAll();
}

void CedaCfgData::DeleteViewFromDiagram(CString opDiagram, CString olView)
{
	CCSReturnCode olRc = RCSuccess;
	char pclSelection[1024]="";
	char pclDiagram[100]="";
	char pclViewName[100]="";

	strcpy(pclDiagram, opDiagram);
	strcpy(pclViewName, olView);

	// WHERE conditions are not mapped so a user such as UFIS$ADMIN is not found because '$' in the field PKNO has been mapped
	CString olUserName = ogUsername;
	MakeCedaString(olUserName);

	CString olViewName = pclViewName;
	MakeCedaString(olViewName);
	
	sprintf(pclSelection,"WHERE APPN='%s' AND (PKNO = '%s' AND CKEY = '%s' AND CTYP = 'VIEW-DATA' AND TEXT LIKE '%%VIEW=%s#%%')", pcgAppName, olUserName, pclDiagram, olViewName);
	olRc = CedaAction("DRT",pclSelection);
}
CCSReturnCode CedaCfgData::UpdateViewForDiagram(CString opDiagram, VIEWDATA &prpView,CString opViewName)
{
	CCSReturnCode olRc = RCSuccess;
	CString olListOfData;
	char pclSelection[1024]="";
	char pclData[824];
	char pclDiagram[100]="";
	char pclText[401]="";
	char pclTmp[512]="";
	//


	strcpy(pclDiagram, opDiagram);

	// to make it easier we delete all rows of the user and his configuration for the
	// specified diagram
	/*************** View is already deleted in "DeleteView" 
	sprintf(pclSelection,"WHERE APPN='%s' AND (PKNO = '%s' AND "
		"CKEY = '%s' AND CTYP = 'VIEW-DATA' AND TEXT LIKE 'VIEW=%s#%%')", 
		pcgAppName, ogUsername, pclDiagram,opViewName);
	olRc = CedaAction("DRT",pclSelection);
	********************/
	// after removing we create a completely new configuration
	int ilC1 = prpView.omNameData.GetSize();

	for(int i = 0; olRc == RCSuccess && i < ilC1; i++)
	{
		if (opViewName == prpView.omNameData[i].ViewName) 
		{
			int ilC2 = prpView.omNameData[i].omTypeData.GetSize();
			for(int j = 0; olRc == RCSuccess && j < ilC2; j++)
			{
				if(prpView.omNameData[i].omTypeData[j].Type == "FILTER")
				{
					if(prpView.omNameData[i].ViewName != GetString(IDS_STRINGDEFAULT))
					{
						CFGDATA rlCfg;
						strcpy(rlCfg.Ckey, pclDiagram);
						strcpy(rlCfg.Pkno, ogUsername);
						strcpy(rlCfg.Ctyp, "VIEW-DATA");
						int ilC3 = prpView.omNameData[i].omTypeData[j].omTextData.GetSize();
						for(int k = 0; olRc == RCSuccess && k < ilC3; k++)
						{
							sprintf(pclText, "VIEW=%s#;", prpView.omNameData[i].ViewName);
							sprintf(pclTmp, "TYPE=%s;", prpView.omNameData[i].omTypeData[j].Type );
							strcat(pclText, pclTmp);
							sprintf(pclTmp, "PAGE=%s;TEXT=", prpView.omNameData[i].omTypeData[j].omTextData[k].Page);
							strcat(pclText, pclTmp);
							pclTmp[0]='\0';
							int ilC4 = prpView.omNameData[i].omTypeData[j].omTextData[k].omValues.GetSize();
							for(int l = 0; olRc == RCSuccess && l < ilC4; l++)
							{
								char pclS[100]="";
								sprintf(pclS, "%s|", prpView.omNameData[i].omTypeData[j].omTextData[k].omValues[l]);
								strcat(pclTmp, pclS);
								if(strlen(pclTmp)>300)
								{
									//we have to devide the row into two parts
									CFGDATA rlCfg2;
									if((rlCfg2.Urno = ogBasicData.GetNextUrno()) == -1)
									{
										olRc = RCFailure;
									}
									else
									{
										strcpy(rlCfg2.Ckey, rlCfg.Ckey);
										strcpy(rlCfg2.Pkno, rlCfg.Pkno);
										strcpy(rlCfg2.Ctyp, rlCfg.Ctyp);
										strcpy(rlCfg2.Text, pclText);
										strcat(pclTmp, ";");
										strcat(rlCfg2.Text, pclTmp);
										pclTmp[0]='\0';
										//And save the record
										MakeCedaData(olListOfData,&rlCfg2);
										strcpy(pclData,olListOfData);
										olRc = CedaAction("IRT","","",pclData);
									}
								}
							}

							if(olRc == RCSuccess)
							{
								if((rlCfg.Urno = ogBasicData.GetNextUrno()) == -1)
								{
									olRc = RCFailure;
								}
								else
								{
									strcat(pclTmp, ";");
									strcat(pclText, pclTmp);
									strcpy(rlCfg.Text, pclText);
									//and save to database
									MakeCedaData(olListOfData,&rlCfg);
									strcpy(pclData,olListOfData);
									olRc = CedaAction("IRT","","",pclData);
									rlCfg.IsChanged = DATA_UNCHANGED;
									pclText[0]='\0';
								}
							}
						}
					}
				}
				else //not filter but ==> Group, Sort etc.
				{
					if(prpView.omNameData[i].ViewName != GetString(IDS_STRINGDEFAULT))
					{
						CFGDATA rlCfg;
						if((rlCfg.Urno = ogBasicData.GetNextUrno()) == -1)
						{
							olRc = RCFailure;
						}
						else
						{
							strcpy(rlCfg.Ckey, pclDiagram);
							strcpy(rlCfg.Pkno, ogUsername);
							strcpy(rlCfg.Ctyp, "VIEW-DATA");
							sprintf(pclText, "VIEW=%s#;", prpView.omNameData[i].ViewName);
							sprintf(pclTmp, "TYPE=%s;TEXT=", prpView.omNameData[i].omTypeData[j].Type );
							strcat(pclText, pclTmp);
							pclTmp[0]='\0';
							int ilC3 = prpView.omNameData[i].omTypeData[j].omValues.GetSize();
							for(int k = 0; k < ilC3; k++)
							{
								char pclS[100]="";
								sprintf(pclS, "%s|", prpView.omNameData[i].omTypeData[j].omValues[k]);
								strcat(pclTmp, pclS);
							}
							strcat(pclTmp, ";");
							strcat(pclText, pclTmp);
							strcpy(rlCfg.Text, pclText);
							//and save to database
							MakeCedaData(olListOfData,&rlCfg);
							strcpy(pclData,olListOfData);
							olRc = CedaAction("IRT","","",pclData);
							rlCfg.IsChanged = DATA_UNCHANGED;
							pclText[0]='\0';
						}
					}
				}
			}
		i = ilC1;
		}
	}

	return olRc;
}

////////////////////////////////////////////////////////////////
// Interprets the raw data for one propertypage
BOOL CedaCfgData::MakeRawViewData(CFGDATA *prpCfg, RAW_VIEWDATA *prpRawData)
{
	strcpy(prpRawData->Ckey, prpCfg->Ckey);
	char *psp = NULL;
	char pclOriginal[1024]="";
	char pclVIEW[100]="";
	char pclTYPE[100]="";
	char pclPAGE[100]="";
	char pclTEXT[1024]="";

	strcpy(pclOriginal, prpCfg->Text);
	psp = strstr(pclOriginal, ";");
	while(psp != NULL)
	{
		char *pclTmp;
		char pclPart[2048];
		char pclRest[2048];
		strcpy(pclRest, psp+1);
		*psp = '\0';
		strcpy(pclPart, pclOriginal);
		strcpy(pclOriginal, pclRest);
		if((pclTmp = strstr(pclPart, "VIEW=")) != NULL)
			strcpy(pclVIEW, pclPart);
		else if((pclTmp = strstr(pclPart, "TYPE=")) != NULL)
			strcpy(pclTYPE, pclPart);
		else if((pclTmp = strstr(pclPart, "PAGE=")) != NULL)
			strcpy(pclPAGE, pclPart);
		else if((pclTmp = strstr(pclPart, "TEXT=")) != NULL)
			strcpy(pclTEXT, pclPart);

		psp = strstr(pclOriginal, ";");
	}

	psp = strstr(pclTYPE, "=");
	if(psp == NULL)
	{
		return FALSE;
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal, psp);
		if(strlen(pclOriginal) == 0)
		{
			return FALSE;
		}
		else
		{
			strcpy(prpRawData->Type, pclOriginal);
		}
	}
	psp = NULL;
	psp = strstr(pclVIEW, "=");
	if(psp == NULL)
	{
		return FALSE;
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal, psp);
		if(strlen(pclOriginal) == 0)
		{
			return FALSE;
		}
		else
		{
			strcpy(prpRawData->Name, pclOriginal);
		}
	}
	psp = NULL;
	psp = strstr(pclPAGE, "=");
	if(psp == NULL)
	{
		if(prpRawData->Type == "FILTER")
		{
			return FALSE;
		}
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal, psp);
		if(strlen(pclOriginal) == 0)
		{
			return FALSE;
		}
		else
		{
			strcpy(prpRawData->Page, pclOriginal);
		}
	}
	psp = NULL;
	psp = strstr(pclTEXT, "=");
	if(psp == NULL)
	{
		return FALSE;
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal, psp);
		if(strlen(pclOriginal) == 0)
		{
			return FALSE;
		}
		else
		{
			strcpy(prpRawData->Values, pclOriginal);
		}
	}
	psp = NULL;

	return TRUE;
}

////////////////////////////////////////////////////////////////
//MWO: extracts the values which are comma-separated and copies them
//     into CStringArray of VIEW_TEXTDATA
void CedaCfgData::MakeViewValues(char *pspValues, CCSPtrArray<CString> *popValues)
{
	char *psp = NULL;
	char pclOriginal[1024]="";
	char pclValue[100]="";

	strcpy(pclOriginal, pspValues);
	psp = strstr(pclOriginal, "|");
	while(psp != NULL)
	{
		char pclPart[2048];
		char pclRest[2048];
		strcpy(pclRest, psp+1);
		*psp = '\0';
		strcpy(pclPart, pclOriginal);
		strcpy(pclOriginal, pclRest);
		if(strcmp(pclPart, "") != 0)
			 popValues->NewAt(popValues->GetSize(), pclPart);

		psp = strstr(pclOriginal, "|");
	}
}


////////////////////////////////////////////////////////////////
// MWO: prepares the nested arrays for the CFGDATA
void CedaCfgData::PrepareViewData(CFGDATA *prpCfg)
{
	VIEWDATA *prlFoundViewData;
	VIEW_VIEWNAMES *prlFoundViewName;
	VIEW_TYPEDATA *prlFoundTypeData;
	VIEW_TEXTDATA *prlFoundTextData;
	RAW_VIEWDATA rlRawData;

	if(MakeRawViewData(prpCfg, &rlRawData) != TRUE)
	{
		return; //then the row is corrupt
	}
	if (rlRawData.Name[strlen(rlRawData.Name)-1] == '#')
	{
		rlRawData.Name[strlen(rlRawData.Name)-1] = '\0';
	}
	prlFoundViewData = FindViewData(prpCfg);
	if(prlFoundViewData == NULL) 
	{
		//if the key does not exist we have generate an new on one from the root
		VIEW_TEXTDATA rlTextData;
		VIEW_TYPEDATA rlTypeData;
		VIEW_VIEWNAMES rlNameData;
		VIEWDATA rlViewData;
		rlViewData.Ckey = CString(rlRawData.Ckey);
		rlNameData.ViewName = CString(rlRawData.Name);
		rlTypeData.Type = CString(rlRawData.Type);
		if(strcmp(rlRawData.Type, "FILTER") == 0)
		{
			rlTextData.Page = CString(rlRawData.Page);
			MakeViewValues(rlRawData.Values, &rlTextData.omValues);
			rlTypeData.omTextData.NewAt(rlTypeData.omTextData.GetSize(), rlTextData);
		}
		else
		{
			MakeViewValues(rlRawData.Values, &rlTypeData.omValues);
		}
		rlNameData.omTypeData.NewAt(rlNameData.omTypeData.GetSize(), rlTypeData);
		rlViewData.omNameData.NewAt(rlViewData.omNameData.GetSize(), rlNameData);
		omViews.NewAt(omViews.GetSize(), rlViewData);

		/*************
		for ( int ilViewCount = rlViewData.omNameData.GetSize() -1; ilViewCount >= 0; ilViewCount--)
		{
			VIEW_VIEWNAMES *prlNameData = &rlViewData.omNameData[ilViewCount];
			for ( int ilNameCount = prlNameData->omTypeData.GetSize() -1; ilNameCount >= 0; ilNameCount--)
			{
				VIEW_TYPEDATA *prlTypeData = &prlNameData->omTypeData[ilViewCount];

				for ( int ilLc = prlTypeData->omTextData.GetSize() -1; ilLc >= 0; ilLc --)
				{
					prlTypeData->omTextData[ilLc].omValues.DeleteAll();
				}
				prlTypeData->omTextData.DeleteAll();
				prlTypeData->omValues.DeleteAll();
			}
			prlNameData->omTypeData.DeleteAll();
		}
		rlViewData.omNameData.DeleteAll();
		*****/
	}
	else
	{

		//the key exists, we have to look for the rest of the tree and
		//evaluate, whether parts of it do exist and merge them together
		//Now first let's look for existing of the view-name
		prlFoundViewName = FindViewNameData(&rlRawData);
		if(prlFoundViewName == NULL)
		{
			//the view itself exists but there are no entries

			VIEW_TEXTDATA rlTextData;
			VIEW_TYPEDATA rlTypeData;
			VIEW_VIEWNAMES rlNameData;
			rlNameData.ViewName = CString(rlRawData.Name);
			rlTypeData.Type = CString(rlRawData.Type);
			if(strcmp(rlRawData.Type, "FILTER") == 0)
			{
				rlTextData.Page = CString(rlRawData.Page);
				MakeViewValues(rlRawData.Values, &rlTextData.omValues);
				rlTypeData.omTextData.NewAt(rlTypeData.omTextData.GetSize(), rlTextData);
			}
			else
			{
				MakeViewValues(rlRawData.Values, &rlTypeData.omValues);
			}
			rlNameData.omTypeData.NewAt(rlNameData.omTypeData.GetSize(), rlTypeData);
			prlFoundViewData->omNameData.NewAt(prlFoundViewData->omNameData.GetSize(), rlNameData);
		}
		else
		{
			//there are name entries => let's have a look at typs and their data
			prlFoundTypeData = FindViewTypeData(&rlRawData);
			if(prlFoundTypeData == NULL) //***** OK
			{
				//View + name exist but no Type entries
				VIEW_TYPEDATA rlTypeData;
				VIEW_TEXTDATA rlTextData;
				rlTypeData.Type = CString(rlRawData.Type);
				if(strcmp(rlRawData.Type, "FILTER") == 0)
				{
					rlTextData.Page = CString(rlRawData.Page);
					MakeViewValues(rlRawData.Values, &rlTextData.omValues);
					rlTypeData.omTextData.NewAt(rlTypeData.omTextData.GetSize(), rlTextData);
				}
				else
				{
					MakeViewValues(rlRawData.Values, &rlTypeData.omValues);
				}
				prlFoundViewName->omTypeData.NewAt(prlFoundViewName->omTypeData.GetSize(), rlTypeData);
			}
			else
			{
				if(rlRawData.Type == CString("FILTER"))//***
				{   
					prlFoundTextData = FindViewTextData(&rlRawData);
					if(prlFoundTextData == NULL) //*** OK
					{
						//View + Name + Type exist but no values for Filter
						VIEW_TEXTDATA rlTextData;
						rlTextData.Page = CString(rlRawData.Page);
						MakeViewValues(rlRawData.Values, &rlTextData.omValues);
						prlFoundTypeData->omTextData.NewAt(prlFoundTypeData->omTextData.GetSize(), rlTextData);
					}
					else
					{ 
						MakeViewValues(rlRawData.Values, &prlFoundTextData->omValues);
					}
				}
				else
				{

				}
			}
		}
	}
			
}

/////////////////////////////////////////////////////////////////////
// MWO: Evaluates the existing of a view, e.g. Staffdia
VIEWDATA * CedaCfgData::FindViewData(CFGDATA *prlCfg)
{
	int ilCount = omViews.GetSize();

	for(int i = 0; i < ilCount; i++)
	{
		if(omViews[i].Ckey == prlCfg->Ckey)
		{
			return &omViews[i];
		}
	}
	return NULL;
}

//////////////////////////////////////////////////////////////////////////////
//MWO: Evaluates the existing of a viewname, e.g. Heute, Morgen
VIEW_VIEWNAMES * CedaCfgData::FindViewNameData(RAW_VIEWDATA *prpRawData)
{
	int ilCount = omViews.GetSize();
	for(int i = 0; i < ilCount; i++)
	{
		if(omViews[i].Ckey == prpRawData->Ckey)
		{
			int ilC2 = omViews[i].omNameData.GetSize();
			for(int j = 0; j < ilC2; j++)
			{
				if(omViews[i].omNameData[j].ViewName == prpRawData->Name)
				{	
					return &omViews[i].omNameData[j];
				}
			}
		}
	}
	return NULL;
}

/////////////////////////////////////////////////////////////////////////////////
// MWO: Evaluates the existing of a viewtyp, e.g. FILTER, GROUP
VIEW_TYPEDATA * CedaCfgData::FindViewTypeData(RAW_VIEWDATA *prpRawData)
{
	int ilCount = omViews.GetSize();
	for(int i = 0; i < ilCount; i++)
	{
		if(omViews[i].Ckey == prpRawData->Ckey)
		{
			int ilC2 = omViews[i].omNameData.GetSize();
			for(int j = 0; j < ilC2; j++)
			{
				if(omViews[i].omNameData[j].ViewName == CString(prpRawData->Name))
				{	
					int ilC3 = omViews[i].omNameData[j].omTypeData.GetSize();
					for(int k = 0; k < ilC3; k++)
					{
						if(omViews[i].omNameData[j].omTypeData[k].Type == CString(prpRawData->Type))
						{
							return  &omViews[i].omNameData[j].omTypeData[k];
						}
					}
				}
			}
		}
	}
	return NULL;
}
////////////////////////////////////////////////////////////////////////////////////
// MWO: Evaluates the existing of values for FILTER
VIEW_TEXTDATA * CedaCfgData::FindViewTextData(RAW_VIEWDATA *prpRawData)
{
	int ilCount = omViews.GetSize();
	for(int i = 0; i < ilCount; i++)
	{
		if(omViews[i].Ckey == prpRawData->Ckey)
		{
			int ilC2 = omViews[i].omNameData.GetSize();
			for(int j = 0; j < ilC2; j++)
			{
				if(omViews[i].omNameData[j].ViewName == CString(prpRawData->Name))
				{	
					int ilC3 = omViews[i].omNameData[j].omTypeData.GetSize();
					for(int k = 0; k < ilC3; k++)
					{
						if(omViews[i].omNameData[j].omTypeData[k].Type == CString(prpRawData->Type))
						{
							int ilC4 = omViews[i].omNameData[j].omTypeData[k].omTextData.GetSize();
							for(int l = 0; l < ilC4; l++)
							{
								if(omViews[i].omNameData[j].omTypeData[k].omTextData[l].Page == CString(prpRawData->Page))
								{
									return &omViews[i].omNameData[j].omTypeData[k].omTextData[l];
								}
							}
						}
					}
				}
			}
		}
	}
	return NULL;
}

/////////////////////////////////////////////////////////////////////////
//MWO: Clear all buffers allocated in omViews
void CedaCfgData::ClearAllViews()
{
	while(omViews.GetSize() > 0)
	{
	//	VIEWDATA *prlView = &omViews[0];
		while(omViews[0].omNameData.GetSize() > 0)
		{
	//		VIEW_VIEWNAMES *prlViewName = &omViews[0].omNameData[0];
			while(omViews[0].omNameData[0].omTypeData.GetSize() > 0)
			{
	//			VIEW_TYPEDATA *prlViewType = &omViews[0].omNameData[0].omTypeData[0];
				while(omViews[0].omNameData[0].omTypeData[0].omTextData.GetSize() > 0)
				{
	//				VIEW_TEXTDATA *prlViewText = &omViews[0].omNameData[0].omTypeData[0].omTextData[0];
					while(omViews[0].omNameData[0].omTypeData[0].omTextData[0].omValues.GetSize() > 0)
					{
						CString *prlText = &omViews[0].omNameData[0].omTypeData[0].omTextData[0].omValues[0];
						omViews[0].omNameData[0].omTypeData[0].omTextData[0].omValues.DeleteAt(0);
					}
					omViews[0].omNameData[0].omTypeData[0].omTextData.DeleteAt(0);
				}
			//	omViews[0].omNameData[0].omTypeData[0].omValues.DeleteAll();
			
				while(omViews[0].omNameData[0].omTypeData[0].omValues.GetSize() > 0)
				{
					omViews[0].omNameData[0].omTypeData[0].omValues.DeleteAt(0);
				}
			
				omViews[0].omNameData[0].omTypeData.DeleteAt(0);
			}
			omViews[0].omNameData.DeleteAt(0);
		}
		omViews.DeleteAt(0);
	}
}

void CedaCfgData::PrepareCfgData(CFGDATA *prpCfg)
{
}

/////////////////////////////////////////////////////////////////////////////
// Update data methods (called from PrePlanTable class and StaffTable class)

CCSReturnCode CedaCfgData::InsertCfg(const CFGDATA *prpCfgData)
{

    if (CfgExist(prpCfgData->Urno))
	{
        return (UpdateCfgRecord(prpCfgData));
	}
    else
	{
        return (InsertCfgRecord(prpCfgData));
	}
}

CCSReturnCode CedaCfgData::UpdateCfg(const CFGDATA *prpCfgData)
{
    return(UpdateCfgRecord(prpCfgData));
}


BOOL CedaCfgData::CfgExist(long lpUrno)
{
	// don't read from database anymore, just check internal array
	CFGDATA *prpData;

	return(omUrnoMap.Lookup((void *) &lpUrno,(void *&)prpData) );
}

CCSReturnCode CedaCfgData::InsertCfgRecord(const CFGDATA *prpCfgData)
{
	CCSReturnCode olRc = RCSuccess;
	// insert new Cfg record into table "SHFCKI"
	char *CfgFields = "URNO,CTYP,CKEY,VAFR,VATO,TEXT";
	char CfgData[512]; // CFGCKI ~ 430 Byte
	sprintf(CfgData, "%ld,%s,%s,%s,%s,%s,%s,%s",
		prpCfgData->Urno, 
		prpCfgData->Appn,
		prpCfgData->Ctyp,
		prpCfgData->Ckey,
		prpCfgData->Vafr,
		prpCfgData->Vato,
		prpCfgData->Pkno,
		prpCfgData->Text);

	if((olRc = CedaAction("IRT", "CFGCKI", CfgFields, "", "", CfgData)) ==  RCFailure)
	{
		ogBasicData.LogCedaError("CedaCfgData Error",omLastErrorMessage,"IRT",pcmFieldList,pcmTableName,"");
	}

	return olRc;
}

CCSReturnCode CedaCfgData::UpdateCfgRecord(const CFGDATA *prpCfgData)
{
    // update Cfgl data in table "SHFCKI"
    // insert new Cfg record into table "SHFCKI"
    char *CfgFields = "CTYP,CKEY,VAFR,VATO,TEXT";
    char CfgData[512]; // SHFCKI ~ 140 byte
	char pclSelection[64];
    sprintf(CfgData, "%s,%s,%s,%s,%s,%s,%s",
		prpCfgData->Appn,
		prpCfgData->Ctyp,
		prpCfgData->Ckey,
		prpCfgData->Vafr,
		prpCfgData->Vato,
		prpCfgData->Pkno,
		prpCfgData->Text);
	sprintf(pclSelection," where URNO = '%ld%'",prpCfgData->Urno);
	CCSReturnCode olRc = CedaAction("URT", "CFGCKI", CfgFields, pclSelection, "", CfgData);
	if(olRc == RCFailure)
	{
		ogBasicData.LogCedaError("CedaCfgData Error",omLastErrorMessage,"IRT",pcmFieldList,pcmTableName,"");
	}
	return olRc;
}


CCSReturnCode CedaCfgData::CreateCfgRequest(const CFGDATA *prpCfgData)
{
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
    // update Cfgl data in table "SHFCKI"
    // insert new Cfg record into table "SHFCKI"
    char *CfgFields = "CTYP,CKEY,VAFR,VATO,TEXT";
    char CfgData[512]; // SHFCKI ~ 140 byte
	char pclSelection[64];
    sprintf(CfgData, "%s,%s,%s,%s,%s,%s",
		prpCfgData->Ctyp,
		prpCfgData->Ckey,
		prpCfgData->Vafr,
		prpCfgData->Vato,
		prpCfgData->Text,
		prpCfgData->Pkno);
	sprintf(pclSelection,"'%s'",prpCfgData->Ckey);
	CCSReturnCode olRc = CedaAction("CKO", "CFGCKI", CfgFields, pclSelection, "", CfgData);
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	if (olRc == RCSuccess)
	{
//		MessageBox(NULL,CfgData,"Optimiereraufruf",MB_OK);
	}
	else
	{
		//MessageBox(NULL,"Keine Verbindung zu Optimierer möglich","Optimiereraufruf",MB_OK);
		MessageBox(NULL,GetString(IDS_STRING61227),GetString(IDS_STRING61228),MB_OK);
	}
//	ogCCSDdx.DataChanged((void *)this,CFG_CHANGE,(void *)prpCfgData);
	return olRc;
}


// the callback function for storing broadcasted fligthdata in FLIGHTDATA array

void  ProcessCfgCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	 ogCfgData.ProcessCfgBc(ipDDXType,vpDataPointer,ropInstanceName);
}


void  CedaCfgData::ProcessCfgBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{

	if ((ipDDXType == BC_CFG_CHANGE) || (ipDDXType == BC_CFG_INSERT))
	{
		CFGDATA *prpCfg;
		struct BcStruct *prlCfgData;

		prlCfgData = (struct BcStruct *) vpDataPointer;
		long llUrno = GetUrnoFromSelection(prlCfgData->Selection);
		ogLog.Trace("BCSHIFT","Cfg-Urno: <%ld>",llUrno);

		if (omUrnoMap.Lookup((void *)llUrno,(void *& )prpCfg) == TRUE)
		{
			GetRecordFromItemList(prpCfg,
				prlCfgData->Fields,prlCfgData->Data);
			TRACE("ProcessCfgBc %s %s\n",prpCfg->Ckey,(LPCSTR)ropInstanceName);
			ogCCSDdx.DataChanged((void *)this,CFG_CHANGE,(void *)prpCfg);
		}
		else
		{
			prpCfg = new CFGDATA;
			GetRecordFromItemList(prpCfg,
				prlCfgData->Fields,prlCfgData->Data);
			PrepareCfgData(prpCfg);
			omData.Add(prpCfg);
			omCkeyMap.SetAt(prpCfg->Ckey,prpCfg);
			omUrnoMap.SetAt((void *)prpCfg->Urno,prpCfg);
			ogCCSDdx.DataChanged((void *)this,CFG_INSERT,(void *)prpCfg);
			TRACE("Process new Cfg %s %s\n",prpCfg->Ckey,(LPCSTR)ropInstanceName);
		}
	}
}

CFGDATA  *CedaCfgData::GetCfgByUrno(long lpUrno)
{
	CFGDATA  *prlCfg;

	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlCfg) == TRUE)
	{
		//TRACE("GetCfgByUrno %ld \n",prlCfg->Urno);
		return prlCfg;
	}
	return NULL;
}

CCSReturnCode CedaCfgData::AddCfg(CFGDATA *prpCfg)
{
	CFGDATA *prlCfg = new CFGDATA;
	memcpy(prlCfg,prpCfg,sizeof(CFGDATA));
	prlCfg->IsChanged = DATA_NEW;

	omData.Add(prlCfg);
	omUrnoMap.SetAt((void *)prlCfg->Urno, prlCfg);
	omCkeyMap.SetAt(prlCfg->Ckey, prlCfg); 

	ogCCSDdx.DataChanged((void *)this,CFG_INSERT,(void *)prlCfg);
	SaveCfg(prlCfg);
//    RC(RCSuccess);
    return RCSuccess;
}

CCSReturnCode CedaCfgData::ChangeCfgData(CFGDATA *prpCfg)
{
//	int ilLc;

	if (prpCfg->IsChanged == DATA_UNCHANGED)
	{
		prpCfg->IsChanged = DATA_CHANGED;
	}
	SaveCfg(prpCfg);

//    RC(RCSuccess);
    return RCSuccess;
}

CCSReturnCode CedaCfgData::DeleteCfg(long lpUrno)
{

	CFGDATA *prpCfg = GetCfgByUrno(lpUrno);
	if (prpCfg != NULL)
	{
		prpCfg->IsChanged = DATA_DELETED;

		omUrnoMap.RemoveKey((void *)lpUrno);
		omCkeyMap.RemoveKey(prpCfg->Ckey);

		SaveCfg(prpCfg);
	}
//    RC(RCSuccess);
    return RCSuccess;
}

CCSReturnCode CedaCfgData::SaveCfg(CFGDATA *prpCfg)
{

	CCSReturnCode olRc = RCSuccess;
	CString olListOfData;
	char pclSelection[124];
	char pclData[2200];

	if ((prpCfg->IsChanged == DATA_UNCHANGED) || (! bgOnline))
	{
		return RCSuccess; // no change, nothing to do
	}
	switch(prpCfg->IsChanged)   // New job, insert into database
	{
	case DATA_NEW:
		MakeCedaData(olListOfData,prpCfg);
		strcpy(pclData,olListOfData);
		if((olRc = CedaAction("IRT","","",pclData)) != RCSuccess)
		{
			ogBasicData.LogCedaError("CedaCfgData Error",omLastErrorMessage,"IRT",pcmFieldList,pcmTableName,"");
		}
		prpCfg->IsChanged = DATA_UNCHANGED;
		if(strcmp(prpCfg->Pkno, ogUsername)==0)
		{
			MakeCurrentUser();
		}
		break;
	case DATA_CHANGED:
		sprintf(pclSelection,"WHERE URNO = '%ld'",prpCfg->Urno);
		MakeCedaData(olListOfData,prpCfg);
		strcpy(pclData,olListOfData);
		olRc = CedaAction("URT", pclSelection, "", pclData);
		if (olRc != RCSuccess)
		{
			prpCfg->IsChanged = DATA_NEW;
			SaveCfg(prpCfg);
		}
		else
		{
			prpCfg->IsChanged = DATA_UNCHANGED;
			//ogCCSDdx.DataChanged((void *)this,CFG_CHANGE,(void *)prpCfg);
			if(strcmp(prpCfg->Pkno, ogUsername)==0)
			{
				MakeCurrentUser();
			}
		}
		break;
	case DATA_DELETED:
		sprintf(pclSelection,"WHERE URNO = '%ld'",prpCfg->Urno);
		olRc = CedaAction("DRT",pclSelection);
		ogCCSDdx.DataChanged((void *)this,CFG_DELETE,(void *)prpCfg);
		if (olRc == RCSuccess)
		{
			for (int ilLc = 0; ilLc < omData.GetSize(); ilLc++)
			{
				if (omData[ilLc].Urno == prpCfg->Urno)
				{
					omData.DeleteAt(ilLc);
					break;
				}
			}
		}
		MakeCurrentUser();
		break;
	}

	
//	RC(RCSuccess);
    return RCSuccess;

}

BOOL CedaCfgData::InterpretSetupString(CString popSetupString, 
									   USERSETUPDATA *prpSetupData)
{
	char *psp = NULL;
	char pclOriginal[1024]="";
	char pclStr[1024]="";
	char pclResult[256]="";

	char pclMONS[300]="";
	char pclRESO[300]="";
	char pclGACH[300]="";
	char pclEQCH[300]="";
	char pclSTCH[300]="";
	char pclCCCH[300]="";
	char pclRECH[300]="";
	char pclFPTB[300]="";
	char pclFJTB[300]="";
	char pclSTTB[300]="";
	char pclGATB[300]="";
	char pclEQTB[300]="";
	char pclCCTB[300]="";
	char pclRQTB[300]="";
	char pclPKTB[300]="";
	char pclCVTB[300]="";
	char pclVPTB[300]="";
	char pclCFTB[300]="";
	char pclATTB[300]="";
	char pclINFO[300]="";
	char pclLHHS[300]="";
	char pclTACK[300]="";
	char pclFBCK[300]="";
	char pclSBCK[300]="";
	char pclSTCV[300]="";
	char pclGACV[300]="";
	char pclEQCV[300]="";
	char pclPSCV[300]="";
	char pclRECV[300]="";
	char pclCCCV[300]="";
	char pclPRMV[300]="";
	char pclSTLV[300]="";
	char pclFPLV[300]="";
	char pclGBLV[300]="";
	char pclCCBV[300]="";
	char pclRQSV[300]="";
	char pclPEAV[300]="";
	char pclVPLV[300]="";
	char pclCONV[300]="";
	char pclATTV[300]="";
	char pclPOCH[300]="";
	char pclPOSV[300]="";
	char pclBKPR[300]="";
	char pclBKPO[300]="";
	char pclDETB[300]="";
	char pclDETV[300]="";
	char pclFITB[300]="";
	char pclFITV[300]="";
	char pclFJTV[300]="";
	char pclCCDB[300]="";
	char pclCCDV[300]="";

	strcpy(pclOriginal, popSetupString);

	// First we have to devide the incomming string into valid Commands

	// the parts of incomming string
	

	psp = strstr(pclOriginal, ";");
	while(psp != NULL)
	{
		char *pclTmp;
		char pclPart[2048];
		char pclRest[2048];
		strcpy(pclRest, psp+1);
		*psp = '\0';
		strcpy(pclPart, pclOriginal);
		strcpy(pclOriginal, pclRest);
		if((pclTmp = strstr(pclPart, "MONS=")) != NULL)
			strcpy(pclMONS, pclPart);
		else if((pclTmp = strstr(pclPart, "RESO=")) != NULL)
			strcpy(pclRESO, pclPart);
		else if((pclTmp = strstr(pclPart, "GACH=")) != NULL)
			strcpy(pclGACH, pclPart);
		else if((pclTmp = strstr(pclPart, "EQCH=")) != NULL)
			strcpy(pclEQCH, pclPart);
		else if((pclTmp = strstr(pclPart, "STCH=")) != NULL)
			strcpy(pclSTCH, pclPart);
		else if((pclTmp = strstr(pclPart, "CCCH=")) != NULL)
			strcpy(pclCCCH, pclPart);
		else if((pclTmp = strstr(pclPart, "RECH=")) != NULL)
			strcpy(pclRECH, pclPart);
		else if((pclTmp = strstr(pclPart, "FPTB=")) != NULL)
			strcpy(pclFPTB, pclPart);
		else if((pclTmp = strstr(pclPart, "FJTB=")) != NULL)
			strcpy(pclFJTB, pclPart);
		else if((pclTmp = strstr(pclPart, "STTB=")) != NULL)
			strcpy(pclSTTB, pclPart);
		else if((pclTmp = strstr(pclPart, "GATB=")) != NULL)
			strcpy(pclGATB, pclPart);
		else if((pclTmp = strstr(pclPart, "EQTB=")) != NULL)
			strcpy(pclEQTB, pclPart);
		else if((pclTmp = strstr(pclPart, "CCTB=")) != NULL)
			strcpy(pclCCTB, pclPart);
		else if((pclTmp = strstr(pclPart, "RQTB=")) != NULL)
			strcpy(pclRQTB, pclPart);
		else if((pclTmp = strstr(pclPart, "PKTB=")) != NULL)
			strcpy(pclPKTB, pclPart);
		else if((pclTmp = strstr(pclPart, "CVTB=")) != NULL)
			strcpy(pclCVTB, pclPart);
		else if((pclTmp = strstr(pclPart, "VPTB=")) != NULL)
			strcpy(pclVPTB, pclPart);
		else if((pclTmp = strstr(pclPart, "CFTB=")) != NULL)
			strcpy(pclCFTB, pclPart);
		else if((pclTmp = strstr(pclPart, "ATTB=")) != NULL)
			strcpy(pclATTB, pclPart);
		else if((pclTmp = strstr(pclPart, "INFO=")) != NULL)
			strcpy(pclINFO, pclPart);
		else if((pclTmp = strstr(pclPart, "LHHS=")) != NULL)
			strcpy(pclLHHS, pclPart);
		else if((pclTmp = strstr(pclPart, "TACK=")) != NULL)
			strcpy(pclTACK, pclPart);
		else if((pclTmp = strstr(pclPart, "FBCK=")) != NULL)
			strcpy(pclFBCK, pclPart);
		else if((pclTmp = strstr(pclPart, "SBCK=")) != NULL)
			strcpy(pclSBCK, pclPart);
		else if((pclTmp = strstr(pclPart, "STCV=")) != NULL)
			strcpy(pclSTCV, pclPart);
		else if((pclTmp = strstr(pclPart, "GACV=")) != NULL)
			strcpy(pclGACV, pclPart);
		else if((pclTmp = strstr(pclPart, "EQCV=")) != NULL)
			strcpy(pclEQCV, pclPart);
		else if((pclTmp = strstr(pclPart, "PSCV=")) != NULL)
			strcpy(pclPSCV, pclPart);
		else if((pclTmp = strstr(pclPart, "RECV=")) != NULL)
			strcpy(pclRECV, pclPart);
		else if((pclTmp = strstr(pclPart, "CCCV=")) != NULL)
			strcpy(pclCCCV, pclPart);
		else if((pclTmp = strstr(pclPart, "PRMV=")) != NULL)
			strcpy(pclPRMV, pclPart);
		else if((pclTmp = strstr(pclPart, "STLV=")) != NULL)
			strcpy(pclSTLV, pclPart);
		else if((pclTmp = strstr(pclPart, "FPLV=")) != NULL)
			strcpy(pclFPLV, pclPart);
		else if((pclTmp = strstr(pclPart, "GBLV=")) != NULL)
			strcpy(pclGBLV, pclPart);
		else if((pclTmp = strstr(pclPart, "FJTV=")) != NULL)
			strcpy(pclFJTV, pclPart);
		else if((pclTmp = strstr(pclPart, "CCBV=")) != NULL)
			strcpy(pclCCBV, pclPart);
		else if((pclTmp = strstr(pclPart, "RQSV=")) != NULL)
			strcpy(pclRQSV, pclPart);
		else if((pclTmp = strstr(pclPart, "PEAV=")) != NULL)
			strcpy(pclPEAV, pclPart);
		else if((pclTmp = strstr(pclPart, "VPLV=")) != NULL)
			strcpy(pclVPLV, pclPart);
		else if((pclTmp = strstr(pclPart, "CONV=")) != NULL)
			strcpy(pclCONV, pclPart);
		else if((pclTmp = strstr(pclPart, "ATTV=")) != NULL)
			strcpy(pclATTV, pclPart);
		else if((pclTmp = strstr(pclPart, "POCH=")) != NULL)
			strcpy(pclPOCH, pclPart);
		else if((pclTmp = strstr(pclPart, "BKPR=")) != NULL)
			strcpy(pclBKPR, pclPart);
		else if((pclTmp = strstr(pclPart, "BKPO=")) != NULL)
			strcpy(pclBKPO, pclPart);
		else if((pclTmp = strstr(pclPart, "DETB=")) != NULL)
			strcpy(pclDETB, pclPart);
		else if((pclTmp = strstr(pclPart, "DETV=")) != NULL)
			strcpy(pclDETV, pclPart);
		else if((pclTmp = strstr(pclPart, "FITB=")) != NULL)
			strcpy(pclFITB, pclPart);
		else if((pclTmp = strstr(pclPart, "FITV=")) != NULL)
			strcpy(pclFITV, pclPart);
		else if((pclTmp = strstr(pclPart, "CCDB=")) != NULL)
			strcpy(pclCCDB, pclPart);
		else if((pclTmp = strstr(pclPart, "CCDV=")) != NULL)
			strcpy(pclCCDV, pclPart);

		psp = strstr(pclOriginal, ";");

	}


	
	psp = strstr(pclMONS, "=");
	if(psp == NULL)
	{
			prpSetupData->MONS = CString("1");
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal, psp);
		if(strlen(pclOriginal) == 0)
		{
			prpSetupData->MONS = CString("1");
		}
		else
		{
			prpSetupData->MONS =  CString(pclOriginal);
		}
	}
	psp = NULL;

	
	psp = strstr(pclRESO, "=");
	if(psp == NULL)
	{
			prpSetupData->RESO = CString("1024x768");
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal, psp);
		if(strlen(pclOriginal) == 0)
		{
			prpSetupData->RESO = CString("1024x768");
		}
		else
		{
			prpSetupData->RESO =  CString(pclOriginal);
		}
	}
	psp = NULL;

	
	psp = strstr(pclGACH, "=");
	if(psp == NULL)
	{
			prpSetupData->GACH  = CString("L");
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal, psp);
		if(strlen(pclOriginal) == 0)
		{
			prpSetupData->GACH  = CString("L");
		}
		else
		{
			prpSetupData->GACH =  CString(pclOriginal);
		}
	}
	psp = NULL;
	
	psp = strstr(pclEQCH, "=");
	if(psp == NULL)
	{
			prpSetupData->EQCH  = CString("L");
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal, psp);
		if(strlen(pclOriginal) == 0)
		{
			prpSetupData->EQCH  = CString("L");
		}
		else
		{
			prpSetupData->EQCH =  CString(pclOriginal);
		}
	}
	psp = NULL;

	
	psp = strstr(pclSTCH, "=");
	if(psp == NULL)
	{
			prpSetupData->STCH = CString("L");
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal, psp);
		if(strlen(pclOriginal) == 0)
		{
			prpSetupData->STCH = CString("L");
		}
		else
		{
			prpSetupData->STCH =  CString(pclOriginal);
		}
	}
	psp = NULL;

	
	psp = strstr(pclCCCH, "=");
	if(psp == NULL)
	{
			prpSetupData->CCCH = CString("L");
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal, psp);
		if(strlen(pclOriginal) == 0)
		{
			prpSetupData->CCCH = CString("L");
		}
		else
		{
			prpSetupData->CCCH =  CString(pclOriginal);
		}
	}
	psp = NULL;

	
	psp = strstr(pclRECH, "=");
	if(psp == NULL)
	{
			prpSetupData->RECH = CString("L");
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal, psp);
		if(strlen(pclOriginal) == 0)
		{
			prpSetupData->RECH = CString("L");
		}
		else
		{
			prpSetupData->RECH =  CString(pclOriginal);
		}
	}
	psp = NULL;

	
	psp = strstr(pclFPTB, "=");
	if(psp == NULL)
	{
			prpSetupData->FPTB = CString("L");
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal, psp);
		if(strlen(pclOriginal) == 0)
		{
			prpSetupData->FPTB = CString("L");
		}
		else
		{
			prpSetupData->FPTB =  CString(pclOriginal);
		}
	}
	psp = NULL;
	
	psp = strstr(pclFJTB, "=");
	if(psp == NULL)
	{
			prpSetupData->FJTB = CString("L");
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal, psp);
		if(strlen(pclOriginal) == 0)
		{
			prpSetupData->FJTB = CString("L");
		}
		else
		{
			prpSetupData->FJTB =  CString(pclOriginal);
		}
	}
	psp = NULL;

	
	psp = strstr(pclSTTB, "=");
	if(psp == NULL)
	{
			prpSetupData->STTB = CString("L");
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal, psp);
		if(strlen(pclOriginal) == 0)
		{
			prpSetupData->STTB = CString("L");
		}
		else
		{
			prpSetupData->STTB =  CString(pclOriginal);
		}
	}
	psp = NULL;

	
	psp = strstr(pclGATB, "=");
	if(psp == NULL)
	{
			prpSetupData->GATB = CString("L");
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal, psp);
		if(strlen(pclOriginal) == 0)
		{
			prpSetupData->GATB = CString("L");
		}
		else
		{
			prpSetupData->GATB =  CString(pclOriginal);
		}
	}
	psp = NULL;
	
	psp = strstr(pclEQTB, "=");
	if(psp == NULL)
	{
			prpSetupData->EQTB = CString("L");
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal, psp);
		if(strlen(pclOriginal) == 0)
		{
			prpSetupData->EQTB = CString("L");
		}
		else
		{
			prpSetupData->EQTB =  CString(pclOriginal);
		}
	}
	psp = NULL;

	
	psp = strstr(pclCCTB, "=");
	if(psp == NULL)
	{
			prpSetupData->CCTB = CString("L");
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal, psp);
		if(strlen(pclOriginal) == 0)
		{
			prpSetupData->CCTB = CString("L");
		}
		else
		{
			prpSetupData->CCTB =  CString(pclOriginal);
		}
	}
	psp = NULL;

	
	psp = strstr(pclRQTB, "=");
	if(psp == NULL)
	{
			prpSetupData->RQTB = CString("L");
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal, psp);
		if(strlen(pclOriginal) == 0)
		{
			prpSetupData->RQTB = CString("L");
		}
		else
		{
			prpSetupData->RQTB =  CString(pclOriginal);
		}
	}
	psp = NULL;

	
	psp = strstr(pclPKTB, "=");
	if(psp == NULL)
	{
			prpSetupData->PKTB = CString("L");
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal, psp);
		if(strlen(pclOriginal) == 0)
		{
			prpSetupData->PKTB = CString("L");
		}
		else
		{
			prpSetupData->PKTB =  CString(pclOriginal);
		}
	}
	psp = NULL;

	
	psp = strstr(pclCVTB, "=");
	if(psp == NULL)
	{
			prpSetupData->CVTB = CString("L");
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal, psp);
		if(strlen(pclOriginal) == 0)
		{
			prpSetupData->CVTB = CString("L");
		}
		else
		{
			prpSetupData->CVTB =  CString(pclOriginal);
		}
	}
	psp = NULL;

	
	psp = strstr(pclVPTB, "=");
	if(psp == NULL)
	{
			prpSetupData->VPTB = CString("L");
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal, psp);
		if(strlen(pclOriginal) == 0)
		{
			prpSetupData->VPTB = CString("L");
		}
		else
		{
			prpSetupData->VPTB =  CString(pclOriginal);
		}
	}
	psp = NULL;

	
	psp = strstr(pclCFTB, "=");
	if(psp == NULL)
	{
			prpSetupData->CFTB = CString("L");
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal, psp);
		if(strlen(pclOriginal) == 0)
		{
			prpSetupData->CFTB = CString("L");
		}
		else
		{
			prpSetupData->CFTB =  CString(pclOriginal);
		}
	}
	psp = NULL;

	
	psp = strstr(pclATTB, "=");
	if(psp == NULL)
	{
			prpSetupData->ATTB = CString("L");
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal, psp);
		if(strlen(pclOriginal) == 0)
		{
			prpSetupData->ATTB = CString("L");
		}
		else
		{
			prpSetupData->ATTB =  CString(pclOriginal);
		}
	}
	psp = NULL;

	
	psp = strstr(pclINFO, "=");
	if(psp == NULL)
	{
			prpSetupData->INFO = CString("L");
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal, psp);
		if(strlen(pclOriginal) == 0)
		{
			prpSetupData->INFO = CString("L");
		}
		else
		{
			prpSetupData->INFO =  CString(pclOriginal);
		}
	}
	psp = NULL;

	
	psp = strstr(pclLHHS, "=");
	if(psp == NULL)
	{
			prpSetupData->LHHS = CString("L");
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal, psp);
		if(strlen(pclOriginal) == 0)
		{
			prpSetupData->LHHS = CString("L");
		}
		else
		{
			prpSetupData->LHHS =  CString(pclOriginal);
		}
	}
	psp = NULL;

	
	psp = strstr(pclTACK, "=");
	if(psp == NULL)
	{
			prpSetupData->TACK = CString("J");
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal, psp);
		if(strlen(pclOriginal) == 0)
		{
			prpSetupData->TACK = CString("J");
		}
		else
		{
			prpSetupData->TACK =  CString(pclOriginal);
		}
	}
	psp = NULL;

	
	psp = strstr(pclFBCK, "=");
	if(psp == NULL)
	{
			prpSetupData->FBCK = CString("J");
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal, psp);
		if(strlen(pclOriginal) == 0)
		{
			prpSetupData->FBCK =  CString(pclOriginal);
		}
		else
		{
			prpSetupData->FBCK =  CString(pclOriginal);
		}
	}
	psp = NULL;

	
	psp = strstr(pclSBCK, "=");
	if(psp == NULL)
	{
			prpSetupData->SBCK = CString("J");
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal, psp);
		if(strlen(pclOriginal) == 0)
		{
			prpSetupData->SBCK = CString("J");
		}
		else
		{
			prpSetupData->SBCK =  CString(pclOriginal);
		}
	}
	psp = NULL;

	
	psp = strstr(pclSTCV, "=");
	if(psp == NULL)
	{
			prpSetupData->STCV = GetString(IDS_STRINGDEFAULT);
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal, psp);
		if(strlen(pclOriginal) == 0)
		{
			prpSetupData->STCV = GetString(IDS_STRINGDEFAULT);
		}
		else
		{
			prpSetupData->STCV =  CString(pclOriginal);
		}
	}
	psp = NULL;

	
	psp = strstr(pclGACV, "=");
	if(psp == NULL)
	{
			prpSetupData->GACV = GetString(IDS_STRINGDEFAULT);
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal, psp);
		if(strlen(pclOriginal) == 0)
		{
			prpSetupData->GACV = GetString(IDS_STRINGDEFAULT);
		}
		else
		{
			prpSetupData->GACV =  CString(pclOriginal);
		}
	}
	psp = NULL;

	
	psp = strstr(pclEQCV, "=");
	if(psp == NULL)
	{
			prpSetupData->EQCV = GetString(IDS_STRINGDEFAULT);
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal, psp);
		if(strlen(pclOriginal) == 0)
		{
			prpSetupData->EQCV = GetString(IDS_STRINGDEFAULT);
		}
		else
		{
			prpSetupData->EQCV =  CString(pclOriginal);
		}
	}
	psp = NULL;


	psp = strstr(pclPSCV, "=");
	if(psp == NULL)
	{
			prpSetupData->PSCV = GetString(IDS_STRINGDEFAULT);
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal, psp);
		if(strlen(pclOriginal) == 0)
		{
			prpSetupData->PSCV = GetString(IDS_STRINGDEFAULT);
		}
		else
		{
			prpSetupData->PSCV =  CString(pclOriginal);
		}
	}
	psp = NULL;


	psp = strstr(pclRECV, "=");
	if(psp == NULL)
	{
			prpSetupData->RECV = GetString(IDS_STRINGDEFAULT);
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal, psp);
		if(strlen(pclOriginal) == 0)
		{
			prpSetupData->RECV = GetString(IDS_STRINGDEFAULT);
		}
		else
		{
			prpSetupData->RECV =  CString(pclOriginal);
		}
	}
	psp = NULL;
	

	psp = strstr(pclCCCV, "=");
	if(psp == NULL)
	{
			prpSetupData->CCCV = GetString(IDS_STRINGDEFAULT);
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal, psp);
		if(strlen(pclOriginal) == 0)
		{
			prpSetupData->CCCV = GetString(IDS_STRINGDEFAULT);
		}
		else
		{
			prpSetupData->CCCV =  CString(pclOriginal);
		}
	}
	psp = NULL;

	psp = strstr(pclPRMV, "=");
	if(psp == NULL)
	{
			prpSetupData->PRMV = GetString(IDS_STRINGDEFAULT);
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal, psp);
		if(strlen(pclOriginal) == 0)
		{
			prpSetupData->PRMV = GetString(IDS_STRINGDEFAULT);
		}
		else
		{
			prpSetupData->PRMV =  CString(pclOriginal);
		}
	}
	psp = NULL;

	
	psp = strstr(pclSTLV, "=");
	if(psp == NULL)
	{
			prpSetupData->STLV = GetString(IDS_STRINGDEFAULT);
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal, psp);
		if(strlen(pclOriginal) == 0)
		{
			prpSetupData->STLV = GetString(IDS_STRINGDEFAULT);
		}
		else
		{
			prpSetupData->STLV =  CString(pclOriginal);
		}
	}
	psp = NULL;

	
	psp = strstr(pclFPLV, "=");
	if(psp == NULL)
	{
			prpSetupData->FPLV = GetString(IDS_STRINGDEFAULT);
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal, psp);
		if(strlen(pclOriginal) == 0)
		{
			prpSetupData->FPLV = GetString(IDS_STRINGDEFAULT);
		}
		else
		{
			prpSetupData->FPLV =  CString(pclOriginal);
		}
	}
	psp = NULL;

	
	psp = strstr(pclGBLV, "=");
	if(psp == NULL)
	{
			prpSetupData->GBLV = GetString(IDS_STRINGDEFAULT);
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal, psp);
		if(strlen(pclOriginal) == 0)
		{
			prpSetupData->GBLV = GetString(IDS_STRINGDEFAULT);
		}
		else
		{
			prpSetupData->GBLV =  CString(pclOriginal);
		}
	}
	psp = NULL;
	

	psp = strstr(pclFJTV, "=");
	if(psp == NULL)
	{
			prpSetupData->FJTV = GetString(IDS_STRINGDEFAULT);
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal, psp);
		if(strlen(pclOriginal) == 0)
		{
			prpSetupData->FJTV = GetString(IDS_STRINGDEFAULT);
		}
		else
		{
			prpSetupData->FJTV =  CString(pclOriginal);
		}
	}
	psp = NULL;

	
	psp = strstr(pclCCBV, "=");
	if(psp == NULL)
	{
			prpSetupData->CCBV = GetString(IDS_STRINGDEFAULT);
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal, psp);
		if(strlen(pclOriginal) == 0)
		{
			prpSetupData->CCBV = GetString(IDS_STRINGDEFAULT);
		}
		else
		{
			prpSetupData->CCBV =  CString(pclOriginal);
		}
	}
	psp = NULL;

	
	psp = strstr(pclRQSV, "=");
	if(psp == NULL)
	{
			prpSetupData->RQSV = GetString(IDS_STRINGDEFAULT);
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal, psp);
		if(strlen(pclOriginal) == 0)
		{
			prpSetupData->RQSV = GetString(IDS_STRINGDEFAULT);
		}
		else
		{
			prpSetupData->RQSV =  CString(pclOriginal);
		}
	}
	psp = NULL;

	
	psp = strstr(pclPEAV, "=");
	if(psp == NULL)
	{
			prpSetupData->PEAV = GetString(IDS_STRINGDEFAULT);
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal, psp);
		if(strlen(pclOriginal) == 0)
		{
			prpSetupData->PEAV = GetString(IDS_STRINGDEFAULT);
		}
		else
		{
			prpSetupData->PEAV =  CString(pclOriginal);
		}
	}
	psp = NULL;

	
	psp = strstr(pclVPLV, "=");
	if(psp == NULL)
	{
			prpSetupData->VPLV = GetString(IDS_STRINGDEFAULT);
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal, psp);
		if(strlen(pclOriginal) == 0)
		{
			prpSetupData->VPLV = GetString(IDS_STRINGDEFAULT);
		}
		else
		{
			prpSetupData->VPLV =  CString(pclOriginal);
		}
	}
	psp = NULL;

	
	psp = strstr(pclCONV, "=");
	if(psp == NULL)
	{
			prpSetupData->CONV = GetString(IDS_STRINGDEFAULT);
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal, psp);
		if(strlen(pclOriginal) == 0)
		{
			prpSetupData->CONV = GetString(IDS_STRINGDEFAULT);
		}
		else
		{
			prpSetupData->CONV =  CString(pclOriginal);
		}
	}
	psp = NULL;

	
	psp = strstr(pclATTV, "=");
	if(psp == NULL)
	{
			prpSetupData->ATTV = GetString(IDS_STRINGDEFAULT);
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal, psp);
		if(strlen(pclOriginal) == 0)
		{
			prpSetupData->ATTV = GetString(IDS_STRINGDEFAULT);
		}
		else
		{
			prpSetupData->ATTV =  CString(pclOriginal);
		}
	}
	psp = NULL;

	psp = strstr(pclPOCH, "=");
	if(psp == NULL)
	{
			prpSetupData->POCH = CString("L");
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal, psp);
		if(strlen(pclOriginal) == 0)
		{
			prpSetupData->POCH = CString("L");
		}
		else
		{
			prpSetupData->POCH =  CString(pclOriginal);
		}
	}
	psp = NULL;

	psp = strstr(pclBKPR, "=");
	if(psp == NULL)
	{
			prpSetupData->BKPR = CString("N");
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal, psp);
		if(strlen(pclOriginal) == 0)
		{
			prpSetupData->BKPR = CString("N");
		}
		else
		{
			prpSetupData->BKPR =  CString(pclOriginal);
		}
	}
	psp = NULL;

	psp = strstr(pclBKPO, "=");
	if(psp == NULL)
	{
			prpSetupData->BKPO = CString("30");
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal, psp);
		if(strlen(pclOriginal) == 0)
		{
			prpSetupData->BKPO = CString("30");
		}
		else
		{
			prpSetupData->BKPO =  CString(pclOriginal);
		}
	}
	psp = NULL;

	psp = strstr(pclDETB, "=");
	if(psp == NULL)
	{
			prpSetupData->DETB = CString("L");
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal, psp);
		if(strlen(pclOriginal) == 0)
		{
			prpSetupData->DETB = CString("L");
		}
		else
		{
			prpSetupData->DETB =  CString(pclOriginal);
		}
	}
	psp = NULL;

	psp = strstr(pclDETV, "=");
	if(psp == NULL)
	{
			prpSetupData->DETV = GetString(IDS_STRINGDEFAULT);
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal, psp);
		if(strlen(pclOriginal) == 0)
		{
			prpSetupData->DETV = GetString(IDS_STRINGDEFAULT);
		}
		else
		{
			prpSetupData->DETV =  CString(pclOriginal);
		}
	}
	psp = NULL;

	psp = strstr(pclFITB, "=");
	if(psp == NULL)
	{
			prpSetupData->FITB = CString("L");
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal, psp);
		if(strlen(pclOriginal) == 0)
		{
			prpSetupData->FITB = CString("L");
		}
		else
		{
			prpSetupData->FITB =  CString(pclOriginal);
		}
	}
	psp = NULL;

	psp = strstr(pclFITV, "=");
	if(psp == NULL)
	{
			prpSetupData->FITV = GetString(IDS_STRINGDEFAULT);
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal, psp);
		if(strlen(pclOriginal) == 0)
		{
			prpSetupData->FITV = GetString(IDS_STRINGDEFAULT);
		}
		else
		{
			prpSetupData->FITV =  CString(pclOriginal);
		}
	}
	psp = NULL;

	psp = strstr(pclCCDB, "=");
	if(psp == NULL)
	{
			prpSetupData->CCDB = CString("L");
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal, psp);
		if(strlen(pclOriginal) == 0)
		{
			prpSetupData->CCDB = CString("L");
		}
		else
		{
			prpSetupData->CCDB =  CString(pclOriginal);
		}
	}
	psp = NULL;

	psp = strstr(pclCCDV, "=");
	if(psp == NULL)
	{
			prpSetupData->CCDV = GetString(IDS_STRINGDEFAULT);
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal, psp);
		if(strlen(pclOriginal) == 0)
		{
			prpSetupData->CCDV = GetString(IDS_STRINGDEFAULT);
		}
		else
		{
			prpSetupData->CCDV =  CString(pclOriginal);
		}
	}
	psp = NULL;

	return TRUE;
}

void CedaCfgData::SetCfgData(void)
{
	ogBasicData.bmTurnaroundBars = ogCfgData.rmUserSetup.TACK[0] == 'J';
	ogBasicData.bmTurnaroundBars = ogCfgData.rmUserSetup.TACK[0] == 'J';
//	ogBasicData.bmDemandsOnly = ogCfgData.rmUserSetup.FBCK[0] == 'N';		//hag20000412 Tommy's Wunsch nur aus Ini-File
	ogBasicData.bmShowShadowBars = ogCfgData.rmUserSetup.SBCK[0] == 'J';
	
	CViewer rlViewer;

	rlViewer.SetViewerKey("GateDia");
	rlViewer.SelectView(ogCfgData.rmUserSetup.GACV);

	rlViewer.SetViewerKey("EquipmentDiagram");
	rlViewer.SelectView(ogCfgData.rmUserSetup.EQCV);

	rlViewer.SetViewerKey("PstDia");
	rlViewer.SelectView(ogCfgData.rmUserSetup.PSCV);

	rlViewer.SetViewerKey("RegnDia");
	rlViewer.SelectView(ogCfgData.rmUserSetup.RECV);

	rlViewer.SetViewerKey("StaffDia");
	rlViewer.SelectView(ogCfgData.rmUserSetup.STCV);

	rlViewer.SetViewerKey("CciDia");
	rlViewer.SelectView(ogCfgData.rmUserSetup.CCCV);
	rlViewer.SetViewerKey("StaffTab");
	rlViewer.SelectView(ogCfgData.rmUserSetup.STLV);
	rlViewer.SetViewerKey("FltSched");
	rlViewer.SelectView(ogCfgData.rmUserSetup.FPLV);
	rlViewer.SetViewerKey("GateTab");
	rlViewer.SelectView(ogCfgData.rmUserSetup.GBLV);
	rlViewer.SetViewerKey("FlightJobsTab");
	rlViewer.SelectView(ogCfgData.rmUserSetup.FJTV);
	rlViewer.SetViewerKey("CciTab");
	rlViewer.SelectView(ogCfgData.rmUserSetup.CCBV);
	rlViewer.SetViewerKey("ReqTab");
	rlViewer.SelectView(ogCfgData.rmUserSetup.RQSV);
	rlViewer.SetViewerKey("ConfTab");
	rlViewer.SelectView(ogCfgData.rmUserSetup.CONV);
	rlViewer.SetViewerKey("PPTab");
	rlViewer.SelectView(ogCfgData.rmUserSetup.VPLV);
	rlViewer.SetViewerKey("DemandTab");
	rlViewer.SelectView(ogCfgData.rmUserSetup.DETV);
	rlViewer.SetViewerKey("FidTab");
	rlViewer.SelectView(ogCfgData.rmUserSetup.FITV);
	rlViewer.SetViewerKey("CicDemTab");
	rlViewer.SelectView(ogCfgData.rmUserSetup.CCDV);
	rlViewer.SetViewerKey("PrmTab");
	rlViewer.SelectView(ogCfgData.rmUserSetup.PRMV);

}


CString CedaCfgData::GetTableName(void)
{
	return CString(pcmTableName);
}


CFGDATA *CedaCfgData::GetCfgByCtypAndCkey(const char *pcpCtyp,  const char *pcpCkey)
{
	CFGDATA *prlCfg = NULL;

	CCSPtrArray <CFGDATA> olCfgs;
	int ilNumCfgs = GetCfgsByCtyp(pcpCtyp, olCfgs);
	for(int ilCfg = 0; ilCfg < ilNumCfgs; ilCfg++)
	{
		CFGDATA *prlTmpCfg = &olCfgs[ilCfg];
		if(!strcmp(prlTmpCfg->Ckey, pcpCkey))
		{
			prlCfg = prlTmpCfg;
			break;
		}
	}

	return prlCfg;
}

CString CedaCfgData::GetValueByCtypAndCkey(const char *pcpCtyp,  const char *pcpCkey, CString opDefaultValue /* = "" */)
{
	CString olValue = opDefaultValue;

	CFGDATA *prlCfg = GetCfgByCtypAndCkey(pcpCtyp, pcpCkey);
	if(prlCfg != NULL)
	{
		olValue = prlCfg->Text;
	}

	return olValue;
}

int CedaCfgData::GetCfgsByCtyp(const char *pcpCtyp, CCSPtrArray <CFGDATA> &ropCfgs)
{
	int ilNumCfgs = omData.GetSize();
	for(int ilC = 0; ilC < ilNumCfgs; ilC++)
	{
		CFGDATA *prlCfg = &omData[ilC];
		if(!strcmp(prlCfg->Ctyp, pcpCtyp))
		{
			ropCfgs.Add(prlCfg);
		}
	}

	return ropCfgs.GetSize();
}

void CedaCfgData::DeleteByCtyp(const char *pcpCtyp)
{
	CCSPtrArray <CFGDATA> olCfgs;
	int ilNumCfgs = ogCfgData.GetCfgsByCtyp(pcpCtyp, olCfgs);
	for(int ilCfg = 0; ilCfg < ilNumCfgs; ilCfg++)
	{
		DeleteCfg(olCfgs[ilCfg].Urno);
	}
}

void CedaCfgData::DeleteByCtypAndCkey(const char *pcpCtyp, const char *pcpCkey)
{
	CCSPtrArray <CFGDATA> olCfgs;
	int ilNumCfgs = ogCfgData.GetCfgsByCtyp(pcpCtyp, olCfgs);
	for(int ilCfg = 0; ilCfg < ilNumCfgs; ilCfg++)
	{
		CFGDATA *prlCfg = &olCfgs[ilCfg];
		if(!strcmp(prlCfg->Ckey, pcpCkey))
		{
			DeleteCfg(prlCfg->Urno);
		}
	}
}

void CedaCfgData::CreateCfg(const char *pcpCtyp, const char *pcpCkey, const char *pcpText)
{
	CFGDATA rlCfg;

	strcpy(rlCfg.Appn, pcgAppName);
	strcpy(rlCfg.Ckey, pcpCkey);
	strcpy(rlCfg.Ctyp, pcpCtyp);
	rlCfg.IsChanged = DATA_NEW;
	strcpy(rlCfg.Pkno, ogUsername);
	strcpy(rlCfg.Text, pcpText);
	rlCfg.Urno = ogBasicData.GetNextUrno();;
	strcpy(rlCfg.Vafr, "");
	strcpy(rlCfg.Vato, "");
	
	AddCfg(&rlCfg);
}
