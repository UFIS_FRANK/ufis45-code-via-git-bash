// ZeitPage.h : header file
//
#ifndef _ZEITPAGE_
#define _ZEITPAGE_

/////////////////////////////////////////////////////////////////////////////
// ZeitPage dialog

class ZeitPage : public CPropertyPage
{
	DECLARE_DYNCREATE(ZeitPage)

// Construction
public:
	ZeitPage();
	~ZeitPage();

	void SetCaption(const char *pcpCaption);

// Dialog Data
	CStringArray omFilterValues;
	CString		 omTitle ;

	//{{AFX_DATA(ZeitPage)
	enum { IDD = IDD_ZEIT_PAGE };
	CTime	m_Time1;
	CTime	m_Time2;
	int		m_RelativeDate;
	//}}AFX_DATA


// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(ZeitPage)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnCommand(WPARAM wParam, LPARAM lParam);
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(ZeitPage)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

// Help routines
private:
	CTime GetTimeFromHHMM(const char *pcpHHMM);
};
#endif // _ZEITPAGE_