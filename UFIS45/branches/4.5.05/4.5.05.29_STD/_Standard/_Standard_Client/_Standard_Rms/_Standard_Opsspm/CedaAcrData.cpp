// CedaAcrData.cpp - Class for gates
//

#include <afxwin.h>
#include <ccsglobl.h>
#include <CCSPtrArray.h>
#include <CCSCedaData.h>
#include <ccsddx.h>
#include <CCSBcHandle.h>
#include <ccsddx.h>
#include <CedaAcrData.h>
#include <BasicData.h>

CedaAcrData::CedaAcrData()
{                  
    BEGIN_CEDARECINFO(ACRDATA, AcrDataRecInfo)
		FIELD_LONG(Urno,"URNO")
		FIELD_CHAR_TRIM(Regn,"REGN")
		FIELD_CHAR_TRIM(Apui,"APUI")
		FIELD_DATE(Vafr,"VAFR")
		FIELD_DATE(Vato,"VATO")
		FIELD_CHAR_TRIM(Act3,"ACT3")
    END_CEDARECINFO

    // Copy the record structure
    for (int i = 0; i < sizeof(AcrDataRecInfo)/sizeof(AcrDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&AcrDataRecInfo[i],sizeof(CEDARECINFO));
        omRecInfo.Add(prpCedaRecInfo);
	}

    // Initialize table names and field names
    strcpy(pcmTableName,"ACRTAB");
    pcmFieldList = "URNO,REGN,APUI,VAFR,VATO,ACT3";
}
 
CedaAcrData::~CedaAcrData()
{
	TRACE("CedaAcrData::~CedaAcrData called\n");
	ClearAll();
}

void CedaAcrData::ClearAll()
{
	omUrnoMap.RemoveAll();
	omNameMap.RemoveAll();
	omData.DeleteAll();
}

CCSReturnCode CedaAcrData::ReadAcrData()
{
	CCSReturnCode ilRc = RCSuccess;
	ClearAll();

	char pclCom[10] = "RT";
    char pclWhere[512];
    strcpy(pclWhere,"");
    if((ilRc = CedaAction2(pclCom, pclWhere)) != RCSuccess)
	{
		ogBasicData.LogCedaError("CedaAcrData Error",omLastErrorMessage,pclCom,pcmFieldList,pcmTableName,pclWhere);
	}
	else
	{
		ACRDATA rlAcrData;
		for (int ilLc = 0; ilRc == RCSuccess; ilLc++)
		{
			if ((ilRc = GetBufferRecord2(ilLc,&rlAcrData)) == RCSuccess)
			{
				AddAcrInternal(rlAcrData);
			}
		}
		ilRc = RCSuccess;
	}

	ogBasicData.Trace("CedaAcrData Read %d Records (Rec Size %d) %s",omData.GetSize(),sizeof(ACRDATA), pclWhere);
    return ilRc;
}


void CedaAcrData::AddAcrInternal(ACRDATA &rrpAcr)
{
	ACRDATA *prlAcr = new ACRDATA;
	*prlAcr = rrpAcr;
	omData.Add(prlAcr);
	omUrnoMap.SetAt((void *)prlAcr->Urno,prlAcr);
	omNameMap.SetAt(prlAcr->Regn,prlAcr);
}


ACRDATA* CedaAcrData::GetAcrByUrno(long lpUrno)
{
	ACRDATA *prlAcr = NULL;
	omUrnoMap.Lookup((void *)lpUrno, (void *&) prlAcr);
	return prlAcr;
}

ACRDATA* CedaAcrData::GetAcrByName(const char *pcpName)
{
	ACRDATA *prlAcr = NULL;
	omNameMap.Lookup(pcpName, (void *&) prlAcr);
	return prlAcr;
}

long CedaAcrData::GetAcrUrnoByName(const char *pcpName)
{
	long llUrno = 0L;
	ACRDATA *prlAcr = GetAcrByName(pcpName);
	if(prlAcr != NULL)
	{
		llUrno = prlAcr->Urno;
	}
	return llUrno;
}

void CedaAcrData::GetAcrNames(CStringArray &ropAcrNames)
{
	int ilNumAcrs = omData.GetSize();
	for(int ilAcr = 0; ilAcr < ilNumAcrs; ilAcr++)
	{
		ropAcrNames.Add(omData[ilAcr].Regn);
	}
}



CString CedaAcrData::GetTableName(void)
{
	return CString(pcmTableName);
}