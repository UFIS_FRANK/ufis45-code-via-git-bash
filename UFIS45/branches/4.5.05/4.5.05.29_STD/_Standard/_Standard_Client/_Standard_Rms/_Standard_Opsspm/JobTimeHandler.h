#ifndef	_JOBTIMEHANDLER_H_INCLUDED_
#define	_JOBTIMEHANDLER_H_INCLUDED_

#include <CCSPtrArray.h>
#include <CedaJobData.h>

class JobTimeHandler : public CObject
{
public: 
	JobTimeHandler(JOBDATA *prpJob, CMenu *popMenu, UINT ipPosConfirm, UINT ipPosEnd);
	JobTimeHandler(CCSPtrArray <JOBDATA> &ropJobs, CMenu *popMenu, UINT ipPosConfirm, UINT ipPosEnd);
	void Init(CCSPtrArray <JOBDATA> &ropJobs, CMenu *popMenu, UINT ipPosConfirm, UINT ipPosEnd);
	~JobTimeHandler();
	void GetEmpNamesForJobs(CCSPtrArray <JOBDATA> &ropJobs, CStringArray &ropEmpNames, const char cpStat);
	bool GetJobs(CCSPtrArray <JOBDATA> &ropJobs, CTime &ropStart, CTime &ropEnd, const char cpStat, bool bpReset = true);

	bool TrackPopupMenu(const CPoint& ropIp,CWnd *popParent);
		
private: // helpers
	bool InstallHandler();

private: // implementation data
			CMenu*		pomMenu;						
			UINT		imPosConfirm;
			UINT		imPosEnd;
			CMenu*		pomConfirmMenu;
			CMenu*		pomEndMenu;
			CDWordArray omJobUrnos;

			UINT		umCmdConfirmed;			// the 
			UINT		umCmdEnded;

	static	UINT		simDefaultConfirmation;	// the default confirmation type
	static	UINT		simDefaultEnded;		// the default ended type

	static	int			simInstall;				// need to install job time handler ?
};

#endif	// _JOBTIMEHANDLER_H_INCLUDED_