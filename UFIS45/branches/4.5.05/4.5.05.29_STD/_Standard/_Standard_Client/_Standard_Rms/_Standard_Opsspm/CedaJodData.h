#ifndef _CJOBDEMAND_H_
#define _CJOBDEMAND_H_

#include <CCSCedaData.h>

struct JodDataStruct {

	long	Urno;           // Unique Record Number
	long	Ujob;           // Unique Record Number in JOBCKI
	long	Udem;           // Unique Record Number in DEMCKI

    char    Usec[33];        // Creator
    CTime   Cdat;           // Creation Date
    char    Useu[33];        // Updater
    CTime   Lstu;           // Update Date

	JodDataStruct(void)
	{
		Urno = 0L;
		Ujob = 0L;
		Udem = 0L;
		strcpy(Usec,"");
		Cdat = TIMENULL;
		strcpy(Useu,"");
		Lstu = TIMENULL;
	}

	JodDataStruct& JodDataStruct::operator=(const JodDataStruct& rhs)
	{
		if (&rhs != this)
		{
			Urno = rhs.Urno;
			Ujob = rhs.Ujob;
			Udem = rhs.Udem;
			strcpy(Usec,rhs.Usec);
			Cdat = rhs.Cdat;
			strcpy(Useu,rhs.Useu);
			Lstu = rhs.Lstu;
		}		
		return *this;
	}

	friend bool operator==(const JodDataStruct& rrp1,const JodDataStruct& rrp2)
	{
		return rrp1.Ujob == rrp2.Ujob && rrp1.Udem == rrp2.Udem;
	}

	friend bool operator!=(const JodDataStruct& rrp1,const JodDataStruct& r2)
	{
		return !operator==(rrp1,r2);
	}
};
typedef struct JodDataStruct JODDATA;

class CedaJodData: public CCSCedaData
{
public:
	bool bmUseJodtab;

    CMapPtrToPtr omUrnoMap;
	CMapPtrToPtr omJobuMap;
	CMapPtrToPtr omDemuMap;
	CCSPtrArray <JODDATA> omData;   

	CedaJodData();
	~CedaJodData();
	void ClearAll();
	bool ReadAllJods(CTime opStartTime, CTime opEndTime);
	bool ReadJods(char *pcpSelection, bool bpReset = false);
	void ReReadJods(CUIntArray &ropAddedJobUrnos);
	JODDATA *AllocNewJod(JODDATA *prpJod);
	bool AddIncomingJod(JODDATA *prpJod);
	void AddJodInternal(JODDATA *prpJod);
	void AddJodToJodDemMap(JODDATA *prpJod);
	void AddJodToDemJodMap(JODDATA *prpJod);
	void RemoveJodFromJodDemMap(JODDATA *prpJod);
	void RemoveJodFromDemJodMap(JODDATA *prpJod);
	JODDATA* AddJod(long lpJobUrno, long lpDemandUrno);
	JODDATA* AddJod(JODDATA *prpJod);
	JODDATA* ChangeDemu(JODDATA *prpJod, long lpNewDemu);
	bool GetJodsByDemand(CCSPtrArray<JODDATA> &ropJods,long lpDemu);
	bool GetJobsByDemand(CCSPtrArray<JOBDATA> &ropJobs,long lpDemu, bool bpNoDuplicates = false);
	bool DemandHasJobs(long lpDemu);
	bool GetDemandsByJob(CCSPtrArray <DEMANDDATA> &ropDemands, long lpJobu);
	long GetDemandUrnoForJob(JOBDATA *prpJob);
	DEMANDDATA *GetDemandForJob(long lpJobu);
	DEMANDDATA *GetDemandForJob(JOBDATA *prpJob);
	bool JobHasDemands(long lpJobu);
	bool JobHasDemands(JOBDATA *prpJob);
	bool GetJodsByJob(CCSPtrArray<JODDATA> &ropJods,long lpUrno);
	JODDATA *GetJodByUrno(long lpUrno);
	long GetFirstJobUrnoByDemand(long lpDemu);
	bool DeleteJod(long lpJodUrno);
	bool DeleteJodByJobu(long lpJobUrno);
	void DeleteJodInternal(long lpJodUrno);
	void PrepareDataForWrite(JODDATA *prpJod);
	CString GetTableName(void);
	bool DbInsertJod(JODDATA *prpJod);
	bool DbInsertJod2(JODDATA *prpJod);
	bool DbUpdateJod(JODDATA *prpJod, JODDATA *prpOldJod = NULL);
	bool DbUpdateJod2(JODDATA *prpJod, JODDATA *prpOldJod = NULL);
	bool DbDeleteJod(long lpJodUrno);
	bool DbDeleteJod2(long lpJodUrno);
	bool GetChangedFields(JODDATA *prpJod, JODDATA *prpOldJod, char *pcpFieldList, char *pcpData);
	void ProcessJodUpdate(void *vpDataPointer);
	bool ProcessJodUpdate(long lpJodUrno, char *pcpFields, char *pcpData);
	void ProcessJodDelete(void *vpDataPointer);
	void ProcessJodDelete(long lpUrno);
	void ProcessJodInsert(char *pcpFields, char *pcpData);
	void ProcessJodsUpdate(void *vpDataPointer);
	void ProcessAttachment(CString &ropAttachment);
	void ReadJodsForDelegatedFlight();
	void LoadJodsForExtraDemands(CCSPtrArray <DEMANDDATA> &ropDemands);

    CMapPtrToPtr omOnlineUrnoMap;
	CCSPtrArray <JODDATA> omOnlineData;
	CMapPtrToPtr omDeletedOfflineJods; // points to jods in omOnlineData that have been deleted in omData whilst offline
	bool bmIsOffline, bmNoOfflineChangesYet;

	bool WasDeletedOffline(long lpJodUrno);
	JODDATA *GetOnlineJodByUrno(long lpJodUrno);
	void DeleteFromOnlineData(long lpJodUrno);
	void UpdateOnlineData(JODDATA *prpJod);
	void SetOffline();
	void SetOnline(bool bpRelease);
	void SetReleaseButton(void);
	bool JodsChangedOffline(void);
	bool JodChangedOffline(JODDATA *prpOfflineJod);
	bool JodChangedOffline(long lpJobUrno);
	void ReleaseJodsChangedOffline(void);
	bool Release(CString opReleaseString);
	void CreateBackupData();
	void RollbackJodsChangedOffline(void);
	void GetJodsChangedOffline(CUIntArray &ropJobUrnos);

	bool bmTransactionActivated;
	CMapPtrToPtr omIrtJods, omUrtJods, omDrtJods;
	void StartTransaction(void);
	void CommitTransaction(void);
	bool AddToIrtTransaction(JODDATA *prpJod);
	bool AddToUrtTransaction(JODDATA *prpJod);
	bool AddToDrtTransaction(long lpJodUrno);
	void CheckForMissingFlightJods(CDWordArray &ropJobUrnos);
};

#endif
