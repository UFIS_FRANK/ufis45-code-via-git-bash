// CicPrintDlg.cpp : implementation file
//

#include <stdafx.h>
#include <opsspm.h>
#include <CicPrintDlg.h>
#include <DlgSettings.h>
#include <BasicData.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCicPrintDlg dialog


CCicPrintDlg::CCicPrintDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CCicPrintDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CCicPrintDlg)
	m_STA = FALSE;
	m_STD = FALSE;
	m_ETA = FALSE;
	m_ETD = FALSE;
	m_Gate = FALSE;
	m_Arr = FALSE;
	m_EmpSort = 0;
	m_FlightSort = 0;
	//}}AFX_DATA_INIT
}


void CCicPrintDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCicPrintDlg)
	DDX_Check(pDX, IDC_STA, m_STA);
	DDX_Check(pDX, IDC_STD, m_STD);
	DDX_Check(pDX, IDC_ETA, m_ETA);
	DDX_Check(pDX, IDC_ETD, m_ETD);
	DDX_Check(pDX, IDC_GATE, m_Gate);
	DDX_Check(pDX, IDC_ARR, m_Arr);
	DDX_Radio(pDX, IDC_SORTBYSHIFT, m_EmpSort);
	DDX_Radio(pDX, IDC_SORTBYSTD, m_FlightSort);
	DDX_CCSddmmyy(pDX, IDC_FROMDATE, m_FromDate);
	DDX_CCSTime(pDX, IDC_FROMTIME, m_FromTime);
	DDX_CCSddmmyy(pDX, IDC_TODATE, m_ToDate);
	DDX_CCSTime(pDX, IDC_TOTIME, m_ToTime);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CCicPrintDlg, CDialog)
	//{{AFX_MSG_MAP(CCicPrintDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCicPrintDlg message handlers

BOOL CCicPrintDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	SetWindowText(GetString(IDS_CICPRINTDLG));
	SetControlText(IDC_STA, IDS_CICPRINTDLG_STA);
	SetControlText(IDC_ETA, IDS_CICPRINTDLG_ETA);
	SetControlText(IDC_STD, IDS_CICPRINTDLG_STD);
	SetControlText(IDC_ETD, IDS_CICPRINTDLG_ETD);
	SetControlText(IDC_GATE, IDS_CICPRINTDLG_GATE);
	SetControlText(IDC_ARR, IDS_CICPRINTDLG_ARR);
	SetControlText(IDC_EMPSORTTITLE, IDS_CICPRINTDLG_EMPSORTTITLE);
	SetControlText(IDC_SORTBYSHIFT, IDS_CICPRINTDLG_SORTBYSHIFT);
	SetControlText(IDC_SORTBYFUNC, IDS_CICPRINTDLG_SORTBYFUNC);
	SetControlText(IDC_FLIGHTSORTTITLE, IDS_CICPRINTDLG_FLIGHTSORTTITLE);
	SetControlText(IDC_SORTBYSTD, IDS_CICPRINTDLG_SORTBYSTD);
	SetControlText(IDC_SORTBYCOUNTER, IDS_CICPRINTDLG_SORTBYCOUNTER);
	SetControlText(IDC_FROMTITLE, IDS_CICPRINTDLG_FROMTITLE);
	SetControlText(IDC_TOTITLE, IDS_CICPRINTDLG_TOTITLE);
	SetControlText(IDC_TIMETITLE, IDS_CICPRINTDLG_TIMETITLE);

	m_FromDate = omFrom;
	m_FromTime = omFrom;
	m_ToDate   = omTo;
	m_ToTime   = omTo;

	ogDlgSettings.EnableSaveToDb("CICPRINTDLG"); // causes settings to be read/written to and from the DB
	m_STA = ogDlgSettings.GetCheckBoxValue("CICPRINTDLG", IDC_STA);
	m_ETA = ogDlgSettings.GetCheckBoxValue("CICPRINTDLG", IDC_ETA);
	m_STD = ogDlgSettings.GetCheckBoxValue("CICPRINTDLG", IDC_STD);
	m_ETD = ogDlgSettings.GetCheckBoxValue("CICPRINTDLG", IDC_ETD);
	m_Gate = ogDlgSettings.GetCheckBoxValue("CICPRINTDLG", IDC_GATE);
	m_Arr = ogDlgSettings.GetCheckBoxValue("CICPRINTDLG", IDC_ARR);
	m_EmpSort = ogDlgSettings.GetRadioButtonValue("CICPRINTDLG", IDC_SORTBYSHIFT);
	m_FlightSort = ogDlgSettings.GetRadioButtonValue("CICPRINTDLG", IDC_SORTBYSTD);

	UpdateData(FALSE);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CCicPrintDlg::SetControlText(int ipControlId, int ipStringId)
{
	CWnd *polWnd = GetDlgItem(ipControlId); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(ipStringId));
	}
}

void CCicPrintDlg::SetTime(CTime opFrom, CTime opTo)
{
	omFrom = opFrom;
	omTo = opTo;
}

void CCicPrintDlg::OnOK() 
{
	if(UpdateData(TRUE))
	{
		omFrom = CTime (m_FromDate.GetYear(),m_FromDate.GetMonth(),m_FromDate.GetDay(),
						m_FromTime.GetHour(),m_FromTime.GetMinute(),m_FromTime.GetSecond());

		omTo = CTime (m_ToDate.GetYear(),m_ToDate.GetMonth(),m_ToDate.GetDay(),
						m_ToTime.GetHour(),m_ToTime.GetMinute(),m_ToTime.GetSecond());

		if(omFrom >= omTo)
		{
			MessageBox(GetString(IDS_STRING61262));
		}
		else
		{
			ogDlgSettings.SaveCheckboxValue(this, "CICPRINTDLG", IDC_STA);
			ogDlgSettings.SaveCheckboxValue(this, "CICPRINTDLG", IDC_ETA);
			ogDlgSettings.SaveCheckboxValue(this, "CICPRINTDLG", IDC_STD);
			ogDlgSettings.SaveCheckboxValue(this, "CICPRINTDLG", IDC_ETD);
			ogDlgSettings.SaveCheckboxValue(this, "CICPRINTDLG", IDC_GATE);
			ogDlgSettings.SaveCheckboxValue(this, "CICPRINTDLG", IDC_ARR);
			ogDlgSettings.SaveRadioButtonValue("CICPRINTDLG", IDC_SORTBYSHIFT, m_EmpSort);
			ogDlgSettings.SaveRadioButtonValue("CICPRINTDLG", IDC_SORTBYSTD, m_FlightSort);
			CDialog::OnOK();
		}
	}
}

void CCicPrintDlg::GetSettings(CTime &ropFrom, CTime &ropTo, bool &rbpSortByShift, bool &rbpSortByStd, bool &rbpDisplaySta, bool &rbpDisplayEta, bool &rbpDisplayStd, bool &rbpDisplayEtd, bool &rbpDisplayGate, bool &rbpDisplayArr)
{
	ropFrom = omFrom;
	ropTo = omTo;
	rbpSortByShift = (m_EmpSort == 0) ? true : false;
	rbpSortByStd = (m_FlightSort == 0) ? true : false;
	rbpDisplaySta = m_STA ? true : false;
	rbpDisplayEta = m_ETA ? true : false;
	rbpDisplayStd = m_STD ? true : false;
	rbpDisplayEtd = m_ETD ? true : false;
	rbpDisplayGate = m_Gate ? true : false;
	rbpDisplayArr = m_Arr ? true : false;
}