// PRMTable.h : header file
//
#ifndef __PRMTABLE_H__
#define __PRMTABLE_H__

/////////////////////////////////////////////////////////////////////////////
// PRMTable dialog
  
#include <PrmTableViewer.h>
#include <CCSPrint.h>

class CPrintPreviewView;

class PRMTable : public CDialog
{
// Construction
public:
	PRMTable(CWnd* pParent = NULL);   // standard constructor
	~PRMTable();
	void UpdateView();
	void SetCaptionText();
	void OnExport(); // lli: export data to excel

	BOOL IsPassFilter(DPXDATA *rpDpx); //igu
	void UpdateCurrentView(); //igu
	BOOL IsViewLocked(); //igu
	void SetDpx(DPXDATA *rpDpx); //igu
	DPXDATA* GetDpx(); //igu
	void SetSelfInsert(BOOL bSelfInsert); //igu
	BOOL IsSelfInsert(); //igu

private :
    CCSTable *pomTable; // visual object, the table content
	BOOL bmIsViewOpen;
	int m_nDialogBarHeight;
	PrmTableViewer omViewer;
	CString omViewName;
	CRect omWindowRect; //PRF 8712
	BOOL m_PrmEditFlight; // lli: flag to control if user can edit PRM request's flight info
	CString omSelectedReport; // lli: report name to print (excel or normal print)
	CPrintPreviewView* pomPrintPreviewView; //Singapore
	CCSPrint *pomPrint;
	int imPrintMaxItems;
	int imPrintItems;
	CSingleDocTemplate* pomTemplate;
	CCSPtrArray<PRMDATA_LINE>	omPrintLines;

	BOOL bmIsViewLocked; //igu
	DPXDATA *m_DpxData; //igu
	BOOL bmIsSelfInsert; //igu

private:
	void SetViewerDate();
	void UpdateComboBox();
	CString Format(PRMDATA_LINE *prpLine);
	static void PRMTableCf(void *popInstance, int ipDDXType,void *vpDataPointer, CString &ropInstanceName);
	void HandleGlobalDateUpdate();
	BOOL PrintPrmItem(CCSPrint *prpPrint,PRMDATA_LINE *prpLine);
	void PrintData(CCSPrint *prpPrint,const int& ripPageNo); //Singapore
	void PrintData();
 

// Dialog Data
	//{{AFX_DATA(PRMTable)
	enum { IDD = IDD_PRMTABLE };
	CComboBox	m_Date;
	CButton m_bSetTime;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(PRMTable)
	public:
	virtual BOOL DestroyWindow();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();
	virtual void OnCancel();
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(PRMTable)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnDestroy();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnMove(int x, int y); //PRF 8712
	afx_msg void OnView();
	afx_msg void OnInsert();
	afx_msg void OnEdit();
	afx_msg void OnDelete();
	afx_msg void OnCopy();
	afx_msg void OnSelchangeViewcombo();
	afx_msg void OnCloseupViewcombo();
	afx_msg void OnPrint();
    afx_msg LONG OnDragOver(UINT, LONG); 
    afx_msg LONG OnDrop(UINT, LONG); 
	afx_msg void OnClose();
	afx_msg void OnSelchangeDate();
    afx_msg LONG OnTableLButtonDblclk(UINT wParam, LONG lParam);
	afx_msg void OnTableUpdateDataCount(); //Singapore
	afx_msg void OnPrintPreview(); //Singapore
	afx_msg void OnBeginPrinting(WPARAM wParam, LPARAM lParam); //Singapore
	afx_msg void PrintPreview(WPARAM wParam, LPARAM lParam); //Singapore
	afx_msg void OnEndPrinting(WPARAM wParam, LPARAM lParam); //Singapore
	afx_msg void OnPreviewPrint(WPARAM wParam, LPARAM lParam); //Singapore
	afx_msg void OnLock(); //igu
	afx_msg void OnSetTime();//Nyan 
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

// Drag-and-drop implementation
private:
	CCSDragDropCtrl m_DragDropTarget;
	int				imDropLineno;
};

#endif //__PRMTABLE_H__
