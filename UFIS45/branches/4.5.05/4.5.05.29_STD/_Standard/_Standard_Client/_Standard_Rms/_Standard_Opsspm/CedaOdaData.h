#ifndef _CEDAODADATA_H_
#define _CEDAODADATA_H_

#include <CCSCedaData.h>
#include <CCSPtrArray.h>

/////////////////////////////////////////////////////////////////////////////
// Record structure declaration

 struct OdaDataStruct
{
	long	Urno;
	char	Sdac[9];	// Odaence Code - corresponds to DSR.SFAC/S in CedaShiftData
	char	Sdan[41];	// Description
	char	Work[2];	// if set to '1' then the absence code acts as a shift

	OdaDataStruct(void)
	{
		Urno = 0L;
		strcpy(Sdac,"");
		strcpy(Sdan,"");
		strcpy(Work,"0");
	}
};

typedef struct OdaDataStruct ODADATA;


/////////////////////////////////////////////////////////////////////////////
// Class declaration

class CedaOdaData: public CCSCedaData
{
// Attributes
public:
    CCSPtrArray<ODADATA> omData;
    CMapStringToPtr omOdaTypeMap;
	CString GetTableName(void);
	CMapPtrToPtr omUrnoMap;
// Operations
public:
	 void GetAllOdaTypes(CCSPtrArray <ODADATA> &rrpOdaTypes);
    CedaOdaData();
	~CedaOdaData();

	bool ReadOdaData();
	ODADATA *GetOdaByType(CString opOdaType);
	void AddOdaInternal(ODADATA &rrpOdaData);
	ODADATA *GetOdaByUrno(long lpUrno);
private:

};


extern CedaOdaData ogOdaData;
#endif 
