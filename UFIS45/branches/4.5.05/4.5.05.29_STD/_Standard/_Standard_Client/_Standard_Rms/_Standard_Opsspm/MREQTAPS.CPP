// RequestTablePropertySheet.cpp : implementation file
//

// These following include files are project dependent
// These following include files are project dependent
#include <stdafx.h>
#include <OpssPm.h>
#include <CCSGlobl.h>
#include <CCSCedaData.h>
#include <CedaAloData.h>
#include <CedaJobData.h>
#include <CedaDemandData.h>
#include <CedaJodData.h>
#include <CedaEmpData.h>
#include <CedaShiftData.h>

// These following include files are necessary
#include <cviewer.h>
#include <BasicData.h>
#include <MreqTaSo.h>
#include <BasePropertySheet.h>
#include <MreqTaPS.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// RequestTablePropertySheet
//

//RequestTablePropertySheet::RequestTablePropertySheet(CWnd* pParentWnd,
//	CViewer *popViewer, UINT iSelectPage) : BasePropertySheet
//	(ID_SHEET_REQUEST_TABLE, pParentWnd, popViewer, iSelectPage)
RequestTablePropertySheet::RequestTablePropertySheet(CWnd* pParentWnd,
	CViewer *popViewer, UINT iSelectPage) : BasePropertySheet
	(GetString(IDS_STRING61594), pParentWnd, popViewer, iSelectPage)
{

	AddPage(&m_pageSort);
//	AddPage(&m_pageZeitVald);
//	AddPage(&m_pageZeitCrat);

	// Change the caption in tab control of the PropertySheet
//	m_pageZeitVald.SetCaption(ID_PAGE_ZEIT_REQUESTVALD);
//	m_pageZeitCrat.SetCaption(ID_PAGE_ZEIT_REQUESTCRAT);

	// Prepare possible values for each PropertyPage

}

void RequestTablePropertySheet::LoadDataFromViewer()
{
	
	pomViewer->GetSort(m_pageSort.omSortOrders);
	m_pageSort.m_Group = pomViewer->GetGroup()[0] - '0';
//	pomViewer->GetFilter("ValdTime", m_pageZeitVald.omFilterValues);
//	pomViewer->GetFilter("CratTime", m_pageZeitCrat.omFilterValues);
}

void RequestTablePropertySheet::SaveDataToViewer(CString opViewName,BOOL bpSaveToDb)
{
	pomViewer->SetSort(m_pageSort.omSortOrders);
	pomViewer->SetGroup(CString(m_pageSort.m_Group + '0'));
//	pomViewer->SetFilter("ValdTime", m_pageZeitVald.omFilterValues);
//	pomViewer->SetFilter("CratTime", m_pageZeitCrat.omFilterValues);
	if (bpSaveToDb == TRUE)
	{
		pomViewer->SafeDataToDB(opViewName);
	}
}

int RequestTablePropertySheet::QueryForDiscardChanges()
{
	CStringArray olSortOrders;
	CString olGroupBy;
	CStringArray olSelectedTimeVald;
	CStringArray olSelectedTimeCrat;
	pomViewer->GetSort(olSortOrders);
	olGroupBy = pomViewer->GetGroup();
	pomViewer->GetFilter("TimeVald", olSelectedTimeVald);
	pomViewer->GetFilter("TimeCrat", olSelectedTimeCrat);

	if (!IsIdentical(olSortOrders, m_pageSort.omSortOrders) ||
		olGroupBy != CString(m_pageSort.m_Group + '0') )
	{
		// Discard changes ?
		return MessageBox(GetString(IDS_STRING61583), NULL, MB_ICONQUESTION | MB_OKCANCEL);
	}

	return IDOK;
}
