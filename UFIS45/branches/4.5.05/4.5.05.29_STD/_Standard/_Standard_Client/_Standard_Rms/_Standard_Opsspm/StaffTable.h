#ifndef __STAFFLST_H__
#define __STAFFLST_H__


#include <StaffTableViewer.h>
#include <afxtempl.h>
#include <CCSButtonctrl.h>

class CPrintPreviewView;
// StaffTable.h : header file
//
/////////////////////////////////////////////////////////////////////////////
// StaffTable dialog

class StaffTable : public CDialog
{
// Construction
public:
	StaffTable(CTime opDate = TIMENULL, BOOL bpIsFromSearch = FALSE,BOOL bpIsDetailDisplay = TRUE);   // standard constructor
	~StaffTable();
	void UpdateView();
	void SetCaptionText(void);
	void SetCaptionText(CString opDate);
	void DeleteAssignment(int ipIndex);
	void SetViewerDate();
	void SetDate(CTime opDate);
	void OnExport();
	BOOL bmIsFromSearch;	
	
	static const CString Terminal2;
	static const CString Terminal3;

private:
	CTable *pomStaffTable;
	CTable *pomActiveStaffTable; //Singapore
	CTable *pomStaffTableT3; //Singapore	
	CSingleDocTemplate* pomTemplate; //Singapore
	CPrintPreviewView* pomPrintPreviewView; //Singapore
	CRect omWindowRect; //PRF 8712
	
	int imSortCrit[3];
	CTime lmDate;
	CTime omDate; // initial date for which the table is loaded
	BOOL bmIsViewOpen;
	BOOL bmIsDetailDisplay;

	int m_nDialogBarHeight;

	// Definition of drag-and-drop object
	CCSDragDropCtrl omDragDropObject;

	StaffTableViewer	omViewer;
	StaffTableViewer omViewerT3; //Singapore
	StaffTableViewer* pomActiveViewer; //Singapore
	StaffTableViewer* pomStaffTableViewer; //Singapore
	StaffTableViewer omBothViewer;
	long				omContextItem;	// Shift urno of selected item
	CArray<long,long>	omAssignUrnos;	// Job urnos assigned to selected item

	CString omActiveTerminal; //Singapore
	BOOL bmIsT2Visible; //Singapore
	BOOL bmIsT3Visible; //Singapore
	CString omSelectedReport; //Singapore
	int imSelectedTerminal; //Singapore
	bool bmIsPaxServiceT2; //Singapore
// Dialog Data
	//{{AFX_DATA(StaffTable)
	enum { IDD = IDD_STAFFTABLE };
	CComboBox	m_Date;
	CCSButtonCtrl m_bTerminal2; //Singapore
	CCSButtonCtrl m_bTerminal3; //Singapore
	//}}AFX_DATA

private:
	static void StaffTableCf(void *popInstance, int ipDDXType,void *vpDataPointer, CString &ropInstanceName);
	void UpdateComboBox();
	void UpdateDisplay();
	CString Format(SHIFTDATA *prpShiftData);
	BOOL DeleteAssignments(int ipItem);
	void HandleGlobalDateUpdate();
	void PrepareBothTerminalPrintData(StaffTableViewer& ropStaffTableViewer /*OUT*/) const; //Singapore
	void DeleteViewerLineData(StaffTableViewer& ropStaffTableViewer/*IN*/); //Singapore
	void PrintData(); //Singapore

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(StaffTable)
	public:
	virtual BOOL DestroyWindow();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(StaffTable)
	virtual BOOL OnInitDialog();
	afx_msg void OnDestroy();
	virtual void OnCancel();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnMove(int x, int y); //PRF 8712
	afx_msg void OnSelchangeCombo1();
	afx_msg void OnView();
	afx_msg void OnTagStafftable();
	afx_msg void OnPrint();
	afx_msg void OnSetTime();
	afx_msg void OnMenuWorkOn();
	afx_msg void OnMenuAbsent();
	afx_msg void OnMenuDelete(UINT nID);
    afx_msg LONG OnTableLButtonDblclk(UINT wParam, LONG lParam);
    afx_msg LONG OnTableRButtonDown(UINT wParam, LONG lParam);
	afx_msg void OnCloseupCombo1();
	afx_msg LONG OnTableDragBegin(UINT wParam, LONG lParam);
	afx_msg void OnClose();
	afx_msg void OnSelchangeDate();
	afx_msg void OnTableSetTableWindow(WPARAM wParam, LPARAM lParam); //Singapore
	afx_msg void OnTableUpdateDataCount(); //Singapore
	afx_msg void OnButtonT2(); //Singapore
	afx_msg void OnButtonT3(); //Singapore
	afx_msg void OnPrintPreview(); //Singapore
	afx_msg void OnBeginPrinting(WPARAM wParam, LPARAM lParam); //Singapore
	afx_msg void PrintPreview(WPARAM wParam, LPARAM lParam); //Singapore
	afx_msg void OnEndPrinting(WPARAM wParam, LPARAM lParam); //Singapore
	afx_msg void OnPreviewPrint(WPARAM wParam, LPARAM lParam); //Singapore
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#endif //__STAFFLST_H__
