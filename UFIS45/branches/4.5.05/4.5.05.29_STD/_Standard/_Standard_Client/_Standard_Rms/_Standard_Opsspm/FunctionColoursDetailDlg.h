#if !defined(AFX_FUNCTIONCOLOURSDETAILDLG_H__4CCF4E00_F83A_465A_B6D8_E2536CF28FC2__INCLUDED_)
#define AFX_FUNCTIONCOLOURSDETAILDLG_H__4CCF4E00_F83A_465A_B6D8_E2536CF28FC2__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// FunctionColoursDetailDlg.h : header file
//
#include <CCSButtonctrl.h>

/////////////////////////////////////////////////////////////////////////////
// CFunctionColoursDetailDlg dialog

class CFunctionColoursDetailDlg : public CDialog
{
// Construction
public:
	CFunctionColoursDetailDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CFunctionColoursDetailDlg)
	enum { IDD = IDD_FUNCTIONCOLOURSDETAILDLG };
	CString	m_ColourTitle;
	CString	m_FctnTitle;
	BOOL	m_Enabled;
	CString	m_FctcTitle;
	CString	m_Fctn;
	CString	m_Fctc;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CFunctionColoursDetailDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CFunctionColoursDetailDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	afx_msg void OnColour();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:
	COLORREF omColour;
	CCSButtonCtrl omColourButton;

public:
	void SetData(CString opFctc, CString opFctn, bool bpEnabled, COLORREF opColour);
	void GetData(CString &ropFctc, CString &ropFctn, bool &rbpEnabled, COLORREF &ropColour);
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_FUNCTIONCOLOURSDETAILDLG_H__4CCF4E00_F83A_465A_B6D8_E2536CF28FC2__INCLUDED_)
