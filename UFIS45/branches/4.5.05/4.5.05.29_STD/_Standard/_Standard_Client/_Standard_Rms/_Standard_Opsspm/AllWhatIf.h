// AllWhatIf.h : header file
//
#ifndef _ALLWHATIF_
#define _ALLWHATIF_


#include <CedaOptData.h>


/////////////////////////////////////////////////////////////////////////////
// CAllWhatIf dialog

class CAllWhatIf : public CDialog
{
// Construction
public:
	CAllWhatIf(CWnd* pParent = NULL);   // standard constructor
	void UpdateView();
	void WhatIfInsert(OptDataStruct *prpOpt);
	void WhatIfDelete(OptDataStruct *prpOpt);

// Dialog Data
	//{{AFX_DATA(CAllWhatIf)
	enum { IDD = IDD_ALLWHATIF };
	CListBox	m_AllWhatIfList;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAllWhatIf)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CAllWhatIf)
	afx_msg void OnNew();
	afx_msg void OnUse();
	virtual BOOL OnInitDialog();
	virtual void OnCancel();
	afx_msg void OnKill();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	int   getNthItemOfField(char *Pstr, char *Ptrenn, int n, char *Presult);
	int   flt_get_nth_field(char *Pstr, char *Ptrenn, int n, char **PPresult);

};
#endif // _ALLWHATIF_