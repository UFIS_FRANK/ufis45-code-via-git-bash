#ifndef _CXBUTTON_H_
#define _CXBUTTON_H_

#include <afxwin.h>

class C2StateButton : public CButton
{
public:
// Constructor
    C2StateButton();

// Attributes
    // This member makes a C2StateButton different from a plain CButton.
    // Its value controls the appearance of the push button (recessed or protuded).
    //
    BOOL bmIsRecessed;

// Operations
    // Recess(TRUE) makes the button pressed (let's say recessed). Recess(FALSE)
    // makes a pressed button released (let's say protuded). Calling Recess() with
    // no parameters returns current state of the 2-state push button.
    //
    void Recess(BOOL bpIsRecessed);
    BOOL Recess();

protected:
    // Generated message map functions
    //{{AFX_MSG(C2StateButton)
    afx_msg UINT OnGetDlgCode();
    afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnKillFocus(CWnd* pNewWnd);
	//}}AFX_MSG
    DECLARE_MESSAGE_MAP()
};

#endif
