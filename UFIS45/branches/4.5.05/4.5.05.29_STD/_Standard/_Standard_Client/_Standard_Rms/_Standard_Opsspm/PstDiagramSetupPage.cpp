// PstDiagramSetupPage.cpp : implementation file
//

#include "stdafx.h"
#include "opsspm.h"
#include "PstDiagramSetupPage.h"
#include <resource.h>
#include <CCSGlobl.h>
#include <BasicData.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// PstDiagramSetupPage dialog
IMPLEMENT_DYNCREATE(PstDiagramSetupPage, CPropertyPage)


PstDiagramSetupPage::PstDiagramSetupPage()
	: CPropertyPage(PstDiagramSetupPage::IDD)
{
	//{{AFX_DATA_INIT(PstDiagramSetupPage)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void PstDiagramSetupPage::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(PstDiagramSetupPage)
	DDX_Control(pDX, IDC_CONFLICTS_CHECKBOX, m_ConflictsCheckboxCtrl);
	//}}AFX_DATA_MAP

	bmConflictsCheckbox = false;
}


BEGIN_MESSAGE_MAP(PstDiagramSetupPage, CDialog)
	//{{AFX_MSG_MAP(PstDiagramSetupPage)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// PstDiagramSetupPage message handlers
void PstDiagramSetupPage::SetConflictsCheckboxState(bool bpConflictsCheckbox)
{
	bmConflictsCheckbox = bpConflictsCheckbox;
}

bool PstDiagramSetupPage::GetConflictsCheckboxState()
{
	int ilState = m_ConflictsCheckboxCtrl.GetState();
	return (ilState == 1) ? true : false;
}

BOOL PstDiagramSetupPage::OnInitDialog() 
{
	CPropertyPage::OnInitDialog();
	
	m_ConflictsCheckboxCtrl.SetWindowText(GetString(IDS_PDSP_JOBCONFLICTS));
	m_ConflictsCheckboxCtrl.SetCheck(bmConflictsCheckbox ? 1 : 0);
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}
