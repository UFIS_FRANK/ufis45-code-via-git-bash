// FlightSearchPage.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// FlightSearchPage dialog
#ifndef _FLIGHTSEARCH_PAGE_
#define _FLIGHTSEARCH_PAGE_
  
class FlightSearchPage : public CPropertyPage
{
	DECLARE_DYNCREATE(FlightSearchPage)

// Construction
public:
	FlightSearchPage();
	~FlightSearchPage();

// Dialog Data
	//{{AFX_DATA(FlightSearchPage)
	enum { IDD = IDD_FLIGHT_SORT_PAGE };
	CString		m_Airline;
//	CTime		m_Date;
	CComboBox	m_Date;
	CString		m_Flightno;
	int			m_SingleFlight;
	//}}AFX_DATA
	CString omTitle; 

// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(FlightSearchPage)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(FlightSearchPage)
	afx_msg void OnKillfocusFlightnr();
	virtual BOOL OnInitDialog();
	afx_msg void OnRadioGanttSelect();
	afx_msg void OnRadioDetailDisplay();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	static int imLastState;
	int FlightDetailDisplayAsDefault() const;
};


#endif // _FLIGHTSEARCH_PAGE_
