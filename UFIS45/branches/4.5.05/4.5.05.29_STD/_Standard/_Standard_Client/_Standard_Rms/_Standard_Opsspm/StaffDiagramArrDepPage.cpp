// StaffDiagramArrDepPage.cpp : implementation file
//

#include <stdafx.h>
#include <resource.h>
#include <CCSGlobl.h>
#include <StaffDiagramArrDepPage.h>
#include <BasicData.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// StaffDiagramArrDepPage property page

IMPLEMENT_DYNCREATE(StaffDiagramArrDepPage, CPropertyPage)

StaffDiagramArrDepPage::StaffDiagramArrDepPage() : CPropertyPage(StaffDiagramArrDepPage::IDD)
{
	//{{AFX_DATA_INIT(StaffDiagramArrDepPage)
	bmArrCheckBox = FALSE;
	bmDepCheckBox = FALSE;
	bmTurnaroundCheckBox = FALSE;
	//}}AFX_DATA_INIT

	lmStoaColour = SILVER;
	lmEtoaColour = WHITE;
	lmTmoaColour = YELLOW;
	lmLandColour = GREEN;
	lmOnblColour = LIME;
	lmStodColour = SILVER;
	lmEtodColour = WHITE;
	lmSlotColour = YELLOW;
	lmOfblColour = LIME;
	lmAirbColour = GREEN;

	omTitle = GetString(IDS_STAFFARRDEP_TITLE);

	m_psp.pszTitle = omTitle;
	m_psp.dwFlags |= PSP_USETITLE;
}

StaffDiagramArrDepPage::~StaffDiagramArrDepPage()
{
}

void StaffDiagramArrDepPage::DoDataExchange(CDataExchange* pDX)
{
	// Extended data exchange -- for member variables with Value type
	if (pDX->m_bSaveAndValidate == FALSE)
	{
	}

	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(StaffDiagramArrDepPage)
	DDX_Check(pDX, IDC_ARRIVAL_CHECKBOX, bmArrCheckBox);
	DDX_Check(pDX, IDC_DEPARTURE_CHECKBOX, bmDepCheckBox);
	DDX_Check(pDX, IDC_TURNAROUND_CHECKBOX, bmTurnaroundCheckBox);
	DDX_Control(pDX, IDC_STACOLOUR, omStoaButton);
	DDX_Control(pDX, IDC_ETACOLOUR, omEtoaButton);
	DDX_Control(pDX, IDC_TMOCOLOUR, omTmoaButton);
	DDX_Control(pDX, IDC_LANDCOLOUR, omLandButton);
	DDX_Control(pDX, IDC_ONBLCOLOUR, omOnblButton);
	DDX_Control(pDX, IDC_STDCOLOUR, omStodButton);
	DDX_Control(pDX, IDC_ETDCOLOUR, omEtodButton);
	DDX_Control(pDX, IDC_SLOTCOLOUR, omSlotButton);
	DDX_Control(pDX, IDC_OFBLCOLOUR, omOfblButton);
	DDX_Control(pDX, IDC_AIRBCOLOUR, omAirbButton);
	//}}AFX_DATA_MAP

	// Extended data validation
	if (pDX->m_bSaveAndValidate == TRUE)
	{
	}
}


BEGIN_MESSAGE_MAP(StaffDiagramArrDepPage, CPropertyPage)
	//{{AFX_MSG_MAP(StaffDiagramArrDepPage)
	ON_BN_CLICKED(IDC_STACOLOUR, OnStoaColour)
	ON_BN_CLICKED(IDC_ETACOLOUR, OnEtoaColour)
	ON_BN_CLICKED(IDC_TMOCOLOUR, OnTmoaColour)
	ON_BN_CLICKED(IDC_LANDCOLOUR, OnLandColour)
	ON_BN_CLICKED(IDC_ONBLCOLOUR, OnOnblColour)
	ON_BN_CLICKED(IDC_STDCOLOUR, OnStodColour)
	ON_BN_CLICKED(IDC_ETDCOLOUR, OnEtodColour)
	ON_BN_CLICKED(IDC_SLOTCOLOUR, OnSlotColour)
	ON_BN_CLICKED(IDC_OFBLCOLOUR, OnOfblColour)
	ON_BN_CLICKED(IDC_AIRBCOLOUR, OnAirbColour)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// StaffDiagramArrDepPage message handlers

BOOL StaffDiagramArrDepPage::OnCommand(WPARAM wParam, LPARAM lParam) 
{
	if (HIWORD(wParam) == BN_CLICKED)
		CancelToClose();
	return CPropertyPage::OnCommand(wParam, lParam);
}

BOOL StaffDiagramArrDepPage::OnInitDialog() 
{
	CPropertyPage::OnInitDialog();
	
	CWnd *polWnd = GetDlgItem(IDC_STOATITLE); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STAFFARRDEP_STOA));
	}
	polWnd = GetDlgItem(IDC_ETOATITLE); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STAFFARRDEP_ETOA));
	}
	polWnd = GetDlgItem(IDC_TMOATITLE); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STAFFARRDEP_TMOA));
	}
	polWnd = GetDlgItem(IDC_LANDTITLE); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STAFFARRDEP_LAND));
	}
	polWnd = GetDlgItem(IDC_ONBLTITLE); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STAFFARRDEP_ONBL));
	}
	polWnd = GetDlgItem(IDC_STODTITLE); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STAFFARRDEP_STOD));
	}
	polWnd = GetDlgItem(IDC_ETODTITLE); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STAFFARRDEP_ETOD));
	}

	polWnd = GetDlgItem(IDC_SLOTTITLE); 
    if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STAFFARRDEP_SLOT));
	}

	polWnd = GetDlgItem(IDC_OFBLTITLE); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STAFFARRDEP_OFBL));
	}
	polWnd = GetDlgItem(IDC_AIRBTITLE); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STAFFARRDEP_AIRB));
	}

	polWnd = GetDlgItem(IDC_ARRIVAL_CHECKBOX); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STAFFARRDEP_ARRIVAL));
	}
	polWnd = GetDlgItem(IDC_DEPARTURE_CHECKBOX); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STAFFARRDEP_DEPARTURE));
	}
	polWnd = GetDlgItem(IDC_TURNAROUND_CHECKBOX); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STAFFARRDEP_TURNAROUND));
	}
	SetColours();

	polWnd = GetDlgItem(IDC_TITLE); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STAFFARRDEP_INTRO));
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void StaffDiagramArrDepPage::SetColours()
{
	SetColour(omStoaButton, lmStoaColour);
	SetColour(omEtoaButton, lmEtoaColour);
	SetColour(omTmoaButton, lmTmoaColour);
	SetColour(omLandButton, lmLandColour);
	SetColour(omOnblButton, lmOnblColour);
	SetColour(omStodButton, lmStodColour);
	SetColour(omEtodButton, lmEtodColour);
	SetColour(omSlotButton, lmSlotColour);
	SetColour(omOfblButton, lmOfblColour);
	SetColour(omAirbButton, lmAirbColour);
}

void StaffDiagramArrDepPage::SetColour(CCSButtonCtrl &ropButton, COLORREF lpColour)
{
	if (m_hWnd != NULL)
	{
		ropButton.SetColors(lpColour,lpColour,lpColour);
	}
}

void StaffDiagramArrDepPage::OnStoaColour() 
{
	ChangeColour(omStoaButton, lmStoaColour);
}

void StaffDiagramArrDepPage::OnEtoaColour() 
{
	ChangeColour(omEtoaButton, lmEtoaColour);
}

void StaffDiagramArrDepPage::OnTmoaColour() 
{
	ChangeColour(omTmoaButton, lmTmoaColour);
}

void StaffDiagramArrDepPage::OnOnblColour() 
{
	ChangeColour(omOnblButton, lmOnblColour);
}

void StaffDiagramArrDepPage::OnLandColour() 
{
	ChangeColour(omLandButton, lmLandColour);
}

void StaffDiagramArrDepPage::OnStodColour() 
{
	ChangeColour(omStodButton, lmStodColour);
}

void StaffDiagramArrDepPage::OnEtodColour() 
{
	ChangeColour(omEtodButton, lmEtodColour);
}

void StaffDiagramArrDepPage::OnSlotColour() 
{
	ChangeColour(omSlotButton, lmSlotColour);
}

void StaffDiagramArrDepPage::OnOfblColour() 
{
	ChangeColour(omOfblButton, lmOfblColour);
}

void StaffDiagramArrDepPage::OnAirbColour() 
{
	ChangeColour(omAirbButton, lmAirbColour);
}

void StaffDiagramArrDepPage::ChangeColour(CCSButtonCtrl &ropButton, COLORREF &ropColour)
{
	CColorDialog olColorDlg(ropColour, 0 , this);

	if (olColorDlg.DoModal() == IDOK)
	{
		ropColour = olColorDlg.GetColor();
		SetColour(ropButton, ropColour);
		ropButton.Invalidate(TRUE);
	}
}
