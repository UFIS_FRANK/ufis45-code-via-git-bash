
#ifndef __CVIEWER_H__
#define __CVIEWER_H__

#include <CedaCfgData.h>
                                 
#ifndef HKEY_CLASSES_ROOT
    #define HKEY_CLASSES_ROOT           (( HKEY ) 0x80000000 )
#endif
#ifndef HKEY_CURRENT_USER
	#define HKEY_CURRENT_USER           (( HKEY ) 0x80000001 )
#endif
#ifndef HKEY_LOCAL_MACHINE
	#define HKEY_LOCAL_MACHINE          (( HKEY ) 0x80000002 )
#endif
#ifndef HKEY_USERS
	#define HKEY_USERS                  (( HKEY ) 0x80000003 )
#endif


//////////////////////////////////////////////////////////////////////////////////
// MWO: 07.10.1996 
// now we have to read, create and set all views from database. The registry
// will not be used any more. We keep the method-call devices in the same manner.
// For calls there will be no changes and it is transparent like nothing has
// changed. the release, which uses the registry is saved as backup for
// possible problems.
//

class CViewer: public CObject
{
public:
    CViewer(); 
    ~CViewer();
    
public:
	void	SetViewerKey(CString strKey);
	BOOL    CreateView(CString strView, const CStringArray &possibleFilters,BOOL bpSave);
	void	GetViews(CStringArray &strArray);
    BOOL    SelectView(CString strView);
	CString SelectView();
    BOOL    DeleteView(CString strView,BOOL bpSave);
    
	void	GetFilterPage(CStringArray &strArray);
    void    SetFilter(CString strFilter, const CStringArray &opFilter);
    bool	GetFilter(CString strFilter, CStringArray &opFilter);
	bool	SetFilterMap(const char *pcpFilterName, CMapStringToPtr &ropFilterMap, const char *pcpAllFilter);
	bool	CheckForEmptyValue(CMapStringToPtr &ropFilterMap, const char *pcpEmptyValue);
    
    void    SetSort(const CStringArray &opSort);
    void	GetSort(CStringArray &opSort);
    
    void    SetGroup(CString strGroup);
	CString GetGroup();

	void SetUserData(CString opKey, CString opUserData);
	CString GetUserData(CString opKey);
	
	VIEW_VIEWNAMES * GetActiveView();
	VIEW_TEXTDATA * GetActiveFilter(CString opFilter);
	VIEW_TEXTDATA * AddToActiveFilter(CString opFilter);
	void SafeDataToDB(CString opViewName);
	CString GetBaseViewName();
	CString GetViewName();

private:
	CString	m_BaseViewName;
	CString	m_ViewName;

private:
    BOOL    CheckKey(char* pStrKey);
	BOOL	CreateKey(char* pStrKey);
	BOOL	DeleteKey(char* pStrKey);
	BOOL	SetValue(char* pStrKey, char* pStrValue);
	BOOL	GetValue(char* pStrKey, char* pStrValue);
	BOOL	DeleteFilter(CString strView);
};

#endif //__CVIEWER_H__
