// CedaWgpData.cpp - Class for employee groups (teams)
//

#include <afxwin.h>
#include <ccsglobl.h>
#include <CCSPtrArray.h>
#include <CCSCedaData.h>
#include <ccsddx.h>
#include <CCSBcHandle.h>
#include <ccsddx.h>
#include <CedaWgpData.h>
#include <BasicData.h>

CedaWgpData::CedaWgpData()
{                  
    BEGIN_CEDARECINFO(WGPDATA, WgpDataRecInfo)
		FIELD_LONG(Urno,"URNO")
		FIELD_CHAR_TRIM(Wgpc,"WGPC")
		FIELD_CHAR_TRIM(Wgpn,"WGPN")
		FIELD_LONG(Pgpu,"PGPU")
    END_CEDARECINFO

    // Copy the record structure
    for (int i = 0; i < sizeof(WgpDataRecInfo)/sizeof(WgpDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&WgpDataRecInfo[i],sizeof(CEDARECINFO));
        omRecInfo.Add(prpCedaRecInfo);
	}

    // Initialize table names and field names
    strcpy(pcmTableName,"WGPTAB");
    pcmFieldList = "URNO,WGPC,WGPN,PGPU";
}
 
CedaWgpData::~CedaWgpData()
{
	TRACE("CedaWgpData::~CedaWgpData called\n");
	ClearAll();
}

void CedaWgpData::ClearAll()
{
	omWgpcMap.RemoveAll();
	omData.DeleteAll();
}

CCSReturnCode CedaWgpData::ReadWgpData()
{
	CCSReturnCode ilRc = RCSuccess;
	ClearAll();

	char pclCom[10] = "RT";
    char pclWhere[512];
    strcpy(pclWhere,"");
    if((ilRc = CedaAction2(pclCom, pclWhere)) != RCSuccess)
	{
		ogBasicData.LogCedaError("CedaWgpData Error",omLastErrorMessage,pclCom,pcmFieldList,pcmTableName,pclWhere);
	}
	else
	{
		WGPDATA rlWgpData;
		for (int ilLc = 0; ilRc == RCSuccess; ilLc++)
		{
			if ((ilRc = GetBufferRecord2(ilLc,&rlWgpData)) == RCSuccess)
			{
				AddWgpInternal(rlWgpData);
			}
		}
		ilRc = RCSuccess;
	}

	ogBasicData.Trace("CedaWgpData Read %d Records (Rec Size %d) %s",omData.GetSize(), sizeof(WGPDATA), pclWhere);
    return ilRc;
}


void CedaWgpData::AddWgpInternal(WGPDATA &rrpWgp)
{
	WGPDATA *prlWgp = new WGPDATA;
	*prlWgp = rrpWgp;
	omData.Add(prlWgp);
	omWgpcMap.SetAt(prlWgp->Wgpc,prlWgp);
}

// get list of teams
// return count of records found
void CedaWgpData::GetAllTeams(CStringArray &ropTeams)
{
	int ilNumTeams = omData.GetSize();
	for(int ilTeam = 0; ilTeam < ilNumTeams; ilTeam++)
	{
		ropTeams.Add(omData[ilTeam].Wgpc);
	}
}


WGPDATA* CedaWgpData::GetWgpByWgpc(const char *pcpWgpc)
{
	WGPDATA *prlWgp = NULL;
	omWgpcMap.Lookup(pcpWgpc, (void *&) prlWgp);

	return prlWgp;
}

CString CedaWgpData::GetTableName(void)
{
	return CString(pcmTableName);
}