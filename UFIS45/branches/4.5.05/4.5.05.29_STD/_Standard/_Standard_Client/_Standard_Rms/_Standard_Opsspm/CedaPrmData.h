// CedaPrmData.h: Auto assignment parameters for line maintenence (Reduktionstufen)
//
//////////////////////////////////////////////////////////////////////

#ifndef _CEDAPRMDATA_H_
#define _CEDAPRMDATA_H_

#include <CCSCedaData.h>
#include <CCSPtrArray.h>

struct PrmDataStruct
{
	long	Urno;		// Unique Record Number
	char	Dscr[126];	// Prm description

	PrmDataStruct(void)
	{
		Urno = 0L;
		strcpy(Dscr,"");
	}
};

typedef struct PrmDataStruct PRMDATA;

class CedaPrmData: public CCSCedaData
{

// Attributes
public:
    CCSPtrArray <PRMDATA> omData;
	CMapPtrToPtr omUrnoMap;
	CMapStringToPtr omNameMap;
	CString GetTableName(void);

// Operations
public:
	CedaPrmData();
	~CedaPrmData();

	CCSReturnCode ReadPrmData();
	PRMDATA* GetPrmByUrno(long lpUrno);
	PRMDATA* GetPrmByName(const char *pcpName);
	long GetPrmUrnoByName(const char *pcpName);
	void GetAllPrmNames(CStringArray &ropPrmNames);

private:
	void AddPrmInternal(PRMDATA &rrpPrm);
	void ClearAll();
};


extern CedaPrmData ogPrmData;

#endif _CEDAPRMDATA_H_
