// OtherColoursDlg.cpp : implementation file
//

#include <stdafx.h>
#include <opsspm.h>
#include <BasicData.h>
#include <OtherColoursDlg.h>
#include <OtherColoursDetailDlg.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// COtherColoursDlg dialog


COtherColoursDlg::COtherColoursDlg(CWnd* pParent /*=NULL*/)
	: CDialog(COtherColoursDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(COtherColoursDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void COtherColoursDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(COtherColoursDlg)
	DDX_Control(pDX, IDC_DISPLAYDEFINEDONLY, m_DisplayDefinedOnlyCheckboxCtrl);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(COtherColoursDlg, CDialog)
	//{{AFX_MSG_MAP(COtherColoursDlg)
	ON_BN_CLICKED(IDC_DISPLAYDEFINEDONLY, OnDisplayDefinedOnlyClicked)
    ON_MESSAGE(WM_TABLE_LBUTTONDBLCLK, OnTableLButtonDblclk)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// COtherColoursDlg message handlers

BOOL COtherColoursDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	SetWindowText(GetString(IDS_OTHCOLOUR_TITLE));
	m_DisplayDefinedOnlyCheckboxCtrl.SetWindowText(GetString(IDS_OTHCOLOUR_DEFONLY));

	pomTable = new CCSTable;
	pomTable->SetSelectMode(0);
    CRect olRect;
    GetClientRect(&olRect);
	olRect.left += 10; olRect.right -= 10; olRect.top += 40; olRect.bottom -= 60;
    pomTable->SetTableData(this, olRect.left, olRect.right, olRect.top, olRect.bottom);

	pomViewer = new OtherColoursViewer();
	pomViewer->Attach(pomTable);
	pomViewer->ChangeViewTo();
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

COtherColoursDlg::~COtherColoursDlg()
{
	pomViewer->Attach(NULL);
	delete pomViewer;
	delete pomTable;	
}

// view selected (or initializing)
void COtherColoursDlg::UpdateDisplay() 
{
	AfxGetApp()->DoWaitCursor(1);
	pomViewer->UpdateDisplay();
	AfxGetApp()->DoWaitCursor(-1);
}

void COtherColoursDlg::OnOK() 
{
	pomViewer->SaveLineData();
	
	CDialog::OnOK();
}


void COtherColoursDlg::OnDisplayDefinedOnlyClicked() 
{
	pomViewer->bmDisplayDefinedOnly = (m_DisplayDefinedOnlyCheckboxCtrl.GetCheck() == 1) ? true : false;
	UpdateDisplay();
}

LONG COtherColoursDlg::OnTableLButtonDblclk(UINT ipItem, LONG lpLParam)
{
	UINT ilNumLines = pomViewer->LineCount();
	if (ipItem >= 0 && ipItem < ilNumLines)
	{
		OTHERCOLOURS_LINE *prlLine = pomViewer->GetLine(ipItem);
		if (prlLine != NULL)
		{
			COtherColoursDetailDlg olDlg;
			olDlg.SetData(prlLine->Name, prlLine->Enabled, prlLine->Colour);
			if(olDlg.DoModal() == IDOK)
			{
				olDlg.GetData(prlLine->Name, prlLine->Enabled, prlLine->Colour);
				pomViewer->UpdateTableLine(ipItem);
			}
		}
	}

	return 0L;
}
