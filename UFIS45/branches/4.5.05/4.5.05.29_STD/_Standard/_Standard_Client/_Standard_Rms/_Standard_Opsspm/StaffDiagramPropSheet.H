// StaffDiagramPropSheet.h : header file
//

#ifndef _STFDIAPS_H_
#define _STFDIAPS_H_

/////////////////////////////////////////////////////////////////////////////
// StaffDiagramPropertySheet
#include <DemandFilterPage.h>
#include <StaffDiagramArrDepPage.h>


class StaffDiagramPropertySheet : public BasePropertySheet
{
// Construction
public:
	StaffDiagramPropertySheet(CWnd* pParentWnd = NULL,
		CViewer *popViewer = NULL, UINT iSelectPage = 0);

// Attributes
public:
	DemandFilterPage m_pagePool;
	FilterPage m_pageRank;	
	FilterPage m_pagePermits;	
	FilterPage m_pageShiftCode;	
	FilterPage m_pageTeam;
	StaffDiagramSortPage m_pageSort;
	StaffDiagramGroupPage m_pageGroup;
	BOOL bmOldHideAbsentEmps, bmOldHideStandbyEmps, bmOldHideFidEmps;
	StaffDiagramArrDepPage m_pageArrDep;

// Operations
public:
	virtual void LoadDataFromViewer();
	virtual void SaveDataToViewer(CString opViewName,BOOL bpSaveToDb = TRUE);
	virtual int QueryForDiscardChanges();
};

/////////////////////////////////////////////////////////////////////////////

#endif // _STFDIAPS_H_
