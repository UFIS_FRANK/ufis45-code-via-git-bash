#if !defined(AFX_DEMANDDETAILDLG_H__1D08B2B4_6743_40A1_B78D_2581C1412C27__INCLUDED_)
#define AFX_DEMANDDETAILDLG_H__1D08B2B4_6743_40A1_B78D_2581C1412C27__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DemandDetailDlg.h : header file
//


/////////////////////////////////////////////////////////////////////////////
// DemandDetailDlg dialog

class DemandDetailDlg : public CDialog
{
// Construction
public:
	DemandDetailDlg(CWnd* pParent = NULL);   // standard constructor
	DemandDetailDlg(FlightDetailViewer* pViewer, CWnd* pParent = NULL);   // standard constructor

// Dialog Data

// Implementation
public:
	void InitData(FlightDetailViewer* pViewer);
	void SetRules();
	void SetServices();
	void SetControls();
	void SetIOTRadioButtons();
	void EnableIOTRadioButtons(CString opEvty);
	void SetTimeValues();
	CTime GetTimeFromFlight(FLIGHTDATA *opAF, CString opField);

	CString GetCurrentRud();

// attributes
public:
	CWnd *pomParent;
	FlightDetailViewer *pomFltViewer;
	RUDDATA* omRud;
	RUEDATA* omRue;
	long lmRudUrno;
	long lmCurrRueUrno;
	long lmTplUrno;
	CTime omDebe;
	CTime omDeen;
	long lmDedu;


	//{{AFX_DATA(DemandDetailDlg)
	enum { IDD = IDD_DEMAND_DETAIL_DLG };
	CButton	m_Btn_OK;
	CButton	m_Btn_Cancel;
	CComboBox	m_Cob_Template;
	CComboBox	m_Cob_Rule;
	CComboBox	m_Cob_Service;
	CEdit	m_Edt_ShortName;
	CEdit	m_Edt_Name;
	CEdit	m_Edt_Time1;
	CEdit	m_Edt_Time2;
	CEdit	m_Edt_Time3;
	CEdit	m_Edt_Time4;
	CEdit	m_Edt_Time5;
	CEdit	m_Edt_Func;
	CEdit	m_Edt_Qua;
	CEdit	m_Edt_Ref1;
	CEdit	m_Edt_Ref2;
	CEdit	m_Edt_Ref3;
	CEdit	m_Edt_Ref4;
	CEdit	m_Edt_Alo;
	CEdit	m_Edt_Ref;
	CButton m_RBt_Inbound;
	CButton m_RBt_Outbound;
	CButton m_RBt_Turnaround;
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(DemandDetailDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void OnOK();
	//}}AFX_VIRTUAL


	// Generated message map functions
	//{{AFX_MSG(DemandDetailDlg)
	afx_msg void OnEditchangeComboRule();
	afx_msg void OnEditchangeComboService();
	afx_msg void OnDestroy();
	afx_msg void OnInbound();
	afx_msg void OnOutbound();
	afx_msg void OnTurnaround();
	virtual BOOL OnInitDialog();
	afx_msg void OnEditchangeCobTemplate();
	afx_msg void OnSelchangeCobTemplate();
	afx_msg void OnSelchangeCobRule();
	afx_msg void OnSelchangeCobService();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DEMANDDETAILDLG_H__1D08B2B4_6743_40A1_B78D_2581C1412C27__INCLUDED_)
