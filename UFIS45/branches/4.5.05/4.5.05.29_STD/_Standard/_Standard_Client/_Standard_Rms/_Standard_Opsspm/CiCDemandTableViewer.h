#ifndef __CICDEMTABLEVIEWER_H__
#define __CICDEMTABLEVIEWER_H__

#include <CCSCedaData.h>
#include <ccsobj.h>
#include <CCSPtrArray.h>
#include <CCSCedaData.h>
#include <CedaDemandData.h>
#include <CedaJodData.h>

#include <cviewer.h>
#include <ccsprint.h>

struct CICDEMDATA_LINE	// each line represents one CIC demand
{
	long		DemUrno;
	char		Obty[11];	// demand type, eg. 'FID', 'AFT', ...	
	CTime		Debe;		// demand begin
	CTime		Deen;		// demand end
	CTimeSpan	Dedu;		// demand duration
	char		Alc2[15];	// airline code format "ALC2/ALC3"
	char		DFnum[15];	// flight number departure
	char		Dety[2];	// duty type 0 = Turnaround, 1 = Arrival, 2 = Departure
	long		Ulnk;		// links a group of demands together
	bool		Expanded;	// checks, whether this group has been expanded or not
	CString		Fcco;		// Function code
	CStringArray Functions; // list of all functions for filtering
	CString		Permits;	// Permits
	CString		Class;		// Counter class
	CString		CountersRules;	// Counters defined in the rule
	CString		CountersFips;	// Counters defined in FIPS
	int			Staff;		// # of employees needed for this demand
	bool		IsTeamLine;	// represents whole team 
	CCSPtrArray<CICDEMDATA_LINE>	TeamLines;	// the team lines
	CTime		OriDebe;
	CTime		OriDeen;
	CTimeSpan	OriDedu;

	CICDEMDATA_LINE()
	{
		DemUrno = 0;
		Obty[0] = '\0';
		Alc2[0] = '\0';
		DFnum[0]= '\0';
		Dety[0] = '\0';
		Ulnk    = 0;
		Staff	= 0;
		Expanded= true;
		IsTeamLine = false;
	}

	CICDEMDATA_LINE(const CICDEMDATA_LINE& rhs)
	{
		*this = rhs;		
	}

	CICDEMDATA_LINE& CICDEMDATA_LINE::operator=(const CICDEMDATA_LINE& rhs)
	{
		if (&rhs != this)
		{
			DemUrno = rhs.DemUrno;
			strcpy(Obty,rhs.Obty);
			Debe = rhs.Debe;		// demand begin
			Deen = rhs.Deen;		// demand end
			Dedu = rhs.Dedu;		// demand duration
			strcpy(Alc2,rhs.Alc2);
			strcpy(DFnum,rhs.DFnum);
			strcpy(Dety,rhs.Dety);
			Ulnk = rhs.Ulnk;		// links a group of demands together
			Expanded = rhs.Expanded;	// checks, whether this group has been expanded or not
			Fcco = rhs.Fcco;		// Function code
			Permits = rhs.Permits;	// Permits
			Class = rhs.Class;		// Counter class
			CountersRules = rhs.CountersRules;// Counters
			CountersFips = rhs.CountersFips;// Counters
			Staff = rhs.Staff;		// # of employees needed for this demand
			IsTeamLine = rhs.IsTeamLine;	// represents whole team 

			this->TeamLines.DeleteAll();
			for (int i = 0; i < rhs.TeamLines.GetSize(); i++)
			{
				TeamLines.NewAt(i,rhs.TeamLines[i]);
			}

			this->Functions.RemoveAll();
			this->Functions.Append(rhs.Functions);

			OriDebe = rhs.OriDebe;
			OriDeen = rhs.OriDeen;
			OriDedu = rhs.OriDedu;
		}		
		return *this;
	}

	~CICDEMDATA_LINE()
	{
		TeamLines.DeleteAll(); 
	}
};

struct CICDEMDATA_FIELD
{
	CString	Field;
	CString	Name;
	int		Length;
	int		PrintLength;

	CICDEMDATA_FIELD(const char *pcpField,const char *pcpName,int ipLength,int ipPrintLength)
	{
		Field = pcpField;
		Name  = pcpName;
		Length= ipLength;
		PrintLength = ipPrintLength; 
	};

	CICDEMDATA_FIELD()
	{
		Length		= 0;
		PrintLength = 0;
	};
};

class	CCSTable;
class	CciDiagramViewer;
struct	TABLE_COLUMN;

class CicDemandTableViewer: public CViewer
{
// Constructions
public:
					CicDemandTableViewer(CciDiagramViewer*	pomParentViewer);
    ~				CicDemandTableViewer();

    void			Attach(CCSTable *popAttachWnd);
	void			ChangeViewTo(const char *pcpViewName, CString opDate);
    void			ChangeViewTo(const char *pcpViewName);
	CTime			StartTime() const;
	CTime			EndTime() const;
	int				Lines() const;
	CICDEMDATA_LINE*GetLine(int ipLineNo);
	void			SetStartTime(CTime opStartTime);	
	void			SetEndTime(CTime opEndTime);	
	void			CompressGroups(bool bpCompress);
	CICDEMDATA_LINE*GetLine(DEMANDDATA *prpDem);
	static void		GetAllViewColumns(CStringArray& ropColumnFields);
	static void		GetDefaultViewColumns(CStringArray& ropColumnFields);
	static CString	GetViewColumnName(const CString& ropColumnField);
	static CString	GetViewColumnField(const CString& ropColumnName);
	static const char*	omAllocGroupTypes[];
private:
	static void		CicDemandTableCf(void *popInstance, int ipDDXType,void *vpDataPointer, CString &ropInstanceName);
    void			PrepareFilter();
	void			PrepareSorter();
	void			PrepareGrouping();
	BOOL			IsSameGroup(CICDEMDATA_LINE *prpDem1,CICDEMDATA_LINE *prpDem2);

	BOOL			IsPassFilter(CICDEMDATA_LINE *prpDemand);
	int				CompareDemand(CICDEMDATA_LINE *prpDem1, CICDEMDATA_LINE *prpDem2);

	void			MakeLines();
	void			MakeLine(DEMANDDATA *prpDem);
	void			MakeLine(DEMANDDATA *prpDem,CICDEMDATA_LINE& opLine);
	void			MakeLine(CCSPtrArray<DEMANDDATA>& ropTeamDemands);
	BOOL			FindTeamLeaderOf(DEMANDDATA *prpDem,int &ripLineno);
	BOOL			FindTeamLeaderOf(long lpDemUrno,int &ripLineno);
	BOOL			FindTeamOf(DEMANDDATA *prpDem,int &ripLineno);

	void			DeleteAll();
	int				CreateLine(CICDEMDATA_LINE *prpGate);
	void			DeleteLine(int ipLineno);

	void			UpdateDisplay(BOOL bpEraseAll = TRUE);
	void			Format(CICDEMDATA_LINE *prpLine,CCSPtrArray<TABLE_COLUMN>& rrpColumns);
	BOOL			EvaluateFlight(FLIGHTDATA *prpFlight,CICDEMDATA_LINE& rrpLine);
	CString			GetCounterClass(DEMANDDATA *prpDem);
	CString			GetCountersRules(DEMANDDATA *prpDem);

// Attributes used for filtering condition
private:
	int				imGroupBy;
    CWordArray		omSortOrder;
	CMapStringToPtr omCMapForDety;
	bool			bmUseAllDetys;
	CMapStringToPtr	omCMapForAlcd;
	bool			bmUseAllAirlines;
	CMapStringToPtr omCMapForClass;
	bool			bmUseAllClasses;
	CMapPtrToPtr	omCMapForGroups;
	bool			bmUseAllGroups;
	CMapStringToPtr	omCMapForFunctions;
	bool			bmUseAllFunctions;

	bool			bmCompressGroups;

// Attributes
private:
    CCSTable*		pomTable;
	CciDiagramViewer*	pomParentViewer;
    CCSPtrArray<CICDEMDATA_LINE> omLines;
	CMapPtrToPtr	omMapUrnoToLine;
	CTime			omStartTime;
	CTime			omEndTime;
    CString			omDate;
	CCSPtrArray<CICDEMDATA_FIELD>omTableFields;
	CString			omTisa;	
	CString			omTisd;

// DDX callback function processing
private:
	int  ProcessJodNew(JODDATA *prlJod);
	int  ProcessJodDelete(JODDATA *prlJod);
	void ProcessJodChange(JODDATA *prpJod);
	void ProcessJodChanges(int ipDDXType,JODDATA *prpJod);
	void ProcessDemandChange(DEMANDDATA *prpDemand);
	void ProcessDemandDelete(DEMANDDATA *prpDemand);
	void ProcessFlightChanges(int ipDDXType,FLIGHTDATA *prpFlight);

	int  DeleteLine(CICDEMDATA_LINE *prpLine);
	bool EvaluateTableFields();
	int  TableFieldIndex(const CString& ropField);
	int  RemoveLine(CICDEMDATA_LINE *prlLine);
	int  ChangeLine(CICDEMDATA_LINE *prlLine,BOOL bpResort = TRUE);
	int  InsertLine(CICDEMDATA_LINE *prpDem);
	void ExpandGroup(CICDEMDATA_LINE *prpGroup);
	void CompressGroup(CICDEMDATA_LINE *prpGroup);
	BOOL RecalculateTeamLine(CICDEMDATA_LINE *prlTeamLine);

// Printing functions
private:
	BOOL PrintDemandLine(CICDEMDATA_LINE *prpLine,BOOL bpIsLastLine);
	BOOL PrintDemandHeader(CCSPrint *pomPrint);
	CCSPrint *pomPrint;
public:
	void PrintView();
};

#endif //__CICDEMANDTABLEVIEWER_H__