
#ifndef _CNFCONFL_H_
#define _CNFCONFL_H_


#include <table.h>

// ConflictConfigTable.h : header file
//

//@Man: CONFLICTCONFDATA
/*@Doc:
  A struct to hold configuration data for conflict checking, for
  the moment, this struct is initialized here, later the data
  will be read from database.
  Database fields: TYPE,NAME,TIME,NCOL,CCOL,DSCR

   
*/
typedef struct ConflictConfDataStruct 
{
    int			Index;				// Index of conflict type 
	long		Type;				// Type of conflict
	char		Name[64];			// Name of conflict type
	int			Time;				// Minutes for calculation conflicts
	int			NSymbol;			// Symbol for unconfirmed conflict
	int			CSymbol;			// Symbol for confirmed conflict
	int			Weight;				// High = most important, low 0 less important
	COLORREF	NCol;				// Color for unconfirmed conflict
	COLORREF	CCol;				// Color for confirmed conflict
	char		Dscr[82];			// Description of conflict type
	bool		Enabled;
	int         Coverage;           // Only for Type 56

	ConflictConfDataStruct(void)
	{
		Enabled = true;
	}

	friend bool operator==(const ConflictConfDataStruct& rrp1,const ConflictConfDataStruct& rrp2)
	{
		return rrp1.Index == rrp2.Index 
			&& rrp1.Type  == rrp2.Type
			&& strcmp(rrp1.Name,rrp2.Name) == 0
			&& rrp1.Time  == rrp2.Time
			&& rrp1.NSymbol == rrp2.NSymbol
			&& rrp1.CSymbol == rrp2.CSymbol
			&& rrp1.Weight == rrp2.Weight
			&& rrp1.NCol == rrp2.NCol
			&& rrp1.CCol == rrp2.CCol
			&& strcmp(rrp1.Dscr,rrp2.Dscr) == 0
			&& rrp1.Enabled == rrp2.Enabled
			&& rrp1.Coverage == rrp2.Coverage
			;
	}

	friend bool operator!=(const ConflictConfDataStruct& rrp1,const ConflictConfDataStruct& r2)
	{
		return !operator==(rrp1,r2);
	}
} CONFLICTCONFDATA;

extern CCSPtrArray <CONFLICTCONFDATA>  ogConflictConfData;


//@Man:
//@Memo: Configuring Conflict checking
//@See: 
/*@Doc: 
	A Table which contains one entry for each conflict-type. The 
	conditions for these conflicts can be changed in the 
	{\bf ConflictConfigDlg }


  The ConfConflict checking uses a struct with the following components:
  \begin{itemize}
  \item {\bf INT ConflictType}:        Type of conflict
  \item {\bf INT ConflictTypeName}:    Name of Type of conflict
  \item {\bf INT MinimumTime}:         Time for conflict checking
  \item {\bf COLORREF ConflictColor}:  Color to display this conflict
  \item {\bf COLORREF ConfirmedColor}: Color to display this conflict if 
                                       it is already confirmed by PDI
  \item {\bf CSTRING Description}:     Description of conflict type
  \end{itemize}

  Possible conflict types:
  Flight related conflicts
  NOCONFLICT     No conflict at all
  NOTCOVERED     No demand is covered by a job and the start of 
                 the earliest demand is not later than (Time) in future..
  VLOWCOVERED    Only one demand is covered by a job and there are more than 
                 one demands and the start of the earliest demand is not later 
				 than (Time) in future.
  LOWCOVERED     More than one demand is covered by a job and there are still 
                 uncovered demands and the start of the earliest demand is not 
				 later than (Time) in future
  DELAYED        The difference between STO and ETO is more than (time) and there 
                 are already demands covered by jobs
  CANCELED       The flight is cancelled (CXXI == �X�) and the start of the earliest
                 demand is not later than (Time) in future.
  DIVERTED       The flight is diverted (DIVI is set) and the start of the earliest 
                 demand is not later than (Time) in future. 
Job related conflicts:
  OVERLAPPING    A job is overlapped by a previous job for more than (Time).
	Exception:	 The overlapping jobs are on the same allocation unit.
  NOTCONFIRMED   A job is not confirmed after (Time before job begin).
*/



/////////////////////////////////////////////////////////////////////////////
// ConfConflicts dialog

class ConfConflicts : public CDialog
{
// Construction
public:
	ConfConflicts(CWnd* pParent = NULL);   // standard constructor
	~ConfConflicts(void);
    void UpdateDisplay();

    CTable omTable; // visual object, the table content
    int m_nDialogBarHeight;
	int m_RecCount;

	

	CString							m_szTitle;  // window title (displayed by OnInitDialog()
	BOOL							m_bContinue;

// Dialog Data
	//{{AFX_DATA(ConfConflicts)
	enum { IDD = IDD_CONFCONFLICTS };
	BOOL	m_DisplayEnabledFirst;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(ConfConflicts)
	public:
	virtual void OnCancel();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(ConfConflicts)
	virtual BOOL OnInitDialog();
	afx_msg void OnUpdate();
	afx_msg void OnSave();
	afx_msg LONG OnLButtonDblClk(UINT wParam, LONG lParam);
	afx_msg void OnPrint();
	afx_msg void OnLoadFromDefault();
	afx_msg void OnSaveAsDefault();
	afx_msg void OnDisplayEnabledFirst();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
			void	ReadAllConfigData(void);
	static	BOOL	UseConflictTime(int ipType);
	static	BOOL	UseOverlappedTime(int ipType);
	CString Format(CONFLICTCONFDATA *prpConflictConfData);
	friend	class	ConflictConfigDlg;
private:
	CCSPtrArray<CONFLICTCONFDATA>	omConflictConfData;
	CMapPtrToPtr omLinesChangedMap;
};


/////////////////////////////////////////////////////////////////////////////
// ConflictConfigDlg dialog

class ConflictConfigDlg : public CDialog
{
// Construction
	
public:
	ConflictConfigDlg(CWnd* pParent = NULL);   // standard constructor
	CString m_szTitle;
	CONFLICTCONFDATA *m_prpData;
	BOOL m_bContinue;

	COLORREF m_CCol;
	COLORREF m_NCol;

	// Dialog Data
	//{{AFX_DATA(ConflictConfigDlg)
	enum { IDD = IDD_CONFCONFLICTDIALOG };
	CString	m_Dscr;
	int		m_OverlappedTime;
	int		m_ConflictTime;
	int		m_Weight;
	BOOL	m_Enabled;
	int     m_Coverage;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(ConflictConfigDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(ConflictConfigDlg)
	virtual BOOL OnInitDialog();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg void OnCcol();
	afx_msg void OnNcol();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#endif
