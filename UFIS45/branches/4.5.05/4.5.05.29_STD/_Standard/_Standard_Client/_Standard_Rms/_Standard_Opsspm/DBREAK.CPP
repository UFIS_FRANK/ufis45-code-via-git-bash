// dbreak.cpp : implementation file
//

#include <stdafx.h>
#include <OpssPm.h>
#include <ccsglobl.h>
#include <dbreak.h>
#include <CCSTime.h>
#include <BasicData.h>
#include <CedaCfgData.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CBreakJobDialog dialog


CBreakJobDialog::CBreakJobDialog(CWnd* pParent /*=NULL*/,CTime opDate)
	: CDialog(CBreakJobDialog::IDD, pParent)
{
	//{{AFX_DATA_INIT(CBreakJobDialog)
	m_Name = ("");
	m_StartDate = CTime(opDate.GetYear(),opDate.GetMonth(),opDate.GetDay(),0,0,0);
	m_Duration = atoi(ogCfgData.GetValueByCtypAndCkey("BREAKDLG", "DEFAULTBREAKTIME", "45"));
	m_PauseType = 1;
	m_CoffeeBreak = FALSE;
	//}}AFX_DATA_INIT

	if (opDate != TIMENULL)
	{
		m_StartTime = opDate;
	}
	else
	{
		m_StartTime = ogBasicData.GetTime();
	}

	imPrevDuration = m_Duration;
	switch(m_Duration)
	{
	case 25:
  		m_PauseType = 0;
		break;
	case 45:
  		m_PauseType = 1;
		break;
    case 60:
  		m_PauseType = 2;
		break;
	default:
  		m_PauseType = 3;
		break;
	}

	m_CoffeeBreak = !strcmp(ogCfgData.GetValueByCtypAndCkey("BREAKDLG", "COFFEEBREAK", "FALSE"),"TRUE") ? TRUE : FALSE;
	bmPrevCoffeeBreak = m_CoffeeBreak;
}

void CBreakJobDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CBreakJobDialog)
	DDX_Control(pDX, IDC_MIN, m_DurationCtrl);
	DDX_Control(pDX, IDC_COFFEEBREAK, m_CoffeeBreakCtrl);
	DDX_Text(pDX, IDC_NAME, m_Name);
	DDX_CCSTime(pDX, IDC_HOUR, m_StartTime);
	DDX_CCSddmmyy(pDX, IDC_DATE, m_StartDate);
	DDX_Text(pDX, IDC_MIN, m_Duration);
	DDV_MinMaxInt(pDX, m_Duration, 0, 1440);
	DDX_Radio(pDX, IDC_MIN25, m_PauseType);
	DDX_Check(pDX, IDC_COFFEEBREAK, m_CoffeeBreak);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CBreakJobDialog, CDialog)
	//{{AFX_MSG_MAP(CBreakJobDialog)
	ON_EN_KILLFOCUS(IDC_MIN, OnKillfocusMin)
	ON_BN_CLICKED(IDC_MIN25, OnMin25)
	ON_BN_CLICKED(IDC_MIN45, OnMin45)
	ON_BN_CLICKED(IDC_MIN60, OnMin60)
	ON_BN_CLICKED(IDC_COFFEEBREAK, OnCoffeebreak)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CBreakJobDialog message handlers

BOOL CBreakJobDialog::OnInitDialog() 
{
	CDialog::OnInitDialog();

	SetWindowText(GetString(IDS_STRING32848)); 

	CWnd *polWnd = GetDlgItem(IDC_EMPLOYEE); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING32849));
	}
	polWnd = GetDlgItem(IDC_BREAK); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING32850));
	}
	polWnd = GetDlgItem(IDC_MIN25); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING32851));
	}
	polWnd = GetDlgItem(IDC_MIN45); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING32852));
	}
	polWnd = GetDlgItem(IDC_MIN60); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING32853));
	}
	polWnd = GetDlgItem(IDC_OTHER); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING32854));
	}
	polWnd = GetDlgItem(IDC_DATUM); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING32855));
	}
	polWnd = GetDlgItem(IDC_TIME); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING32856));
	}
	polWnd = GetDlgItem(IDC_DURATION); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING32857));
	}
	polWnd = GetDlgItem(IDC_COFFEEBREAK); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING32858));
	}

	polWnd = GetDlgItem(IDOK); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61697));
	}
	polWnd = GetDlgItem(IDCANCEL); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61698));
	}

	
	// TODO: Add extra initialization here
	CenterWindow();

   	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CBreakJobDialog::OnKillfocusMin() 
{
	if (UpdateData() == FALSE)
		return;

	switch (m_Duration)
	{
	case 25:
  		m_PauseType = 0;
		break;
	case 45:
  		m_PauseType = 1;
		break;
    case 60:
  		m_PauseType = 2;
		break;
	default:
  		m_PauseType = 3;
		break;
	}
	UpdateData(FALSE);
}


void CBreakJobDialog::OnMin25() 
{
	if (UpdateData() == FALSE)
		return;

	m_Duration = 25;
	UpdateData(FALSE);
}


void CBreakJobDialog::OnMin45() 
{
	if (UpdateData() == FALSE)
		return;

	m_Duration = 45;
	UpdateData(FALSE);
}

void CBreakJobDialog::OnMin60() 
{
	if (UpdateData() == FALSE)
		return;

	m_Duration = 60;
	UpdateData(FALSE);
}

void CBreakJobDialog::OnOK() 
{
	CDialog::OnOK();

	m_StartTime = CTime(m_StartDate.GetYear(), m_StartDate.GetMonth(),
		m_StartDate.GetDay(), m_StartTime.GetHour(), m_StartTime.GetMinute(),
		m_StartTime.GetSecond());

	if(imPrevDuration != m_Duration)
	{
		CString olDefaultBreakTime;
		olDefaultBreakTime.Format("%d", m_Duration);
		ogCfgData.DeleteByCtypAndCkey("BREAKDLG", "DEFAULTBREAKTIME");
		ogCfgData.CreateCfg("BREAKDLG", "DEFAULTBREAKTIME", olDefaultBreakTime);
	}
	if(bmPrevCoffeeBreak != m_CoffeeBreak)
	{
		CString olCoffeeBreak;
		olCoffeeBreak.Format("%s", m_CoffeeBreak ? "TRUE" : "FALSE");
		ogCfgData.DeleteByCtypAndCkey("BREAKDLG", "COFFEEBREAK");
		ogCfgData.CreateCfg("BREAKDLG", "COFFEEBREAK", olCoffeeBreak);
	}
}

void CBreakJobDialog::OnCoffeebreak() 
{
	if(m_CoffeeBreakCtrl.GetCheck())
	{
		if (UpdateData() == FALSE)
			return;

  		m_PauseType = 3;
		m_Duration = 20;
		UpdateData(FALSE);
	}
}
