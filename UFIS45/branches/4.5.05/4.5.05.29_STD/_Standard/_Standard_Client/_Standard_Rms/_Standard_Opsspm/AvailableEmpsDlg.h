#if !defined(AFX_AVAILABLEEMPSDLG_H__5C4418B4_45DF_11D5_808B_00010215BFE5__INCLUDED_)
#define AFX_AVAILABLEEMPSDLG_H__5C4418B4_45DF_11D5_808B_00010215BFE5__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// AvailableEmpsDlg.h : header file
//
#include <TScale.h>

#include <CedaDemandData.h>
#include <CedaShiftData.h>
#include <CedaEmpData.h>
#include <CedaJobData.h>
#include <AvailableEmpsViewer.h>
#include <NewAssignConflictViewer.h>
#include <NewAssignConflictGantt.h>

/////////////////////////////////////////////////////////////////////////////
// CAvailableEmpsDlg dialog


class CAvailableEmpsDlg : public CDialog
{
// Construction
public:
	CAvailableEmpsDlg(CWnd* pParent = NULL);   // standard constructor
	~CAvailableEmpsDlg();

// Dialog Data
	//{{AFX_DATA(CAvailableEmpsDlg)
	enum { IDD = IDD_AVAILABLEEMPSDLG };
	CButton	m_IgnoreBreaksCtrl;
	CButton	m_IgnoreOverlapCtrl;
	CButton	m_IgnoreOutsideShiftCtrl;
	CButton	m_TeamAllocCtrl;
	CButton	m_DisplayOnlyEmpsWithCorrectFunctionsAndPermitsCtrl;
	CStatic	m_StatusCtrl;
	BOOL	m_DisplayOnlyEmpsWithCorrectFunctionsAndPermits;
	BOOL	m_IgnoreBreaks;
	BOOL	m_IgnoreOverlap;
	BOOL	m_IgnoreOutsideShift;
	//}}AFX_DATA

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAvailableEmpsDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CAvailableEmpsDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	afx_msg void UpdateView();
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnFilterfuncpermits();
    afx_msg LONG OnTableLButtonDblclk(UINT wParam, LONG lParam);
    afx_msg LONG OnTableSelectionChanged(UINT wParam, LONG lParam);
	afx_msg void OnIgnoreBreaksClicked();
	afx_msg void OnIgnoreOverlapClicked();
	afx_msg void OnIgnoreOutsideShiftClicked();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
	DEMANDDATA *prmSelectedDemand;
	CCSPtrArray <DEMANDDATA> omDemands; // demands indicated by the calling environment
	CDWordArray omSelectedPoolJobUrnos; // the resulting employees to be assigned to the demand
	bool bmTeamAlloc;

private:
	// don't need these but are created because they are required in the Gantt/Viewer
	CCSPtrArray <JOBDATA> omJobs;
	CCSPtrArray <JODDATA> omJods;
	CCSPtrArray <JOBDATA> omNoDemandJobs;

	CCSPtrArray <AVAILABLE_EMPS_LINEDATA> omEmployees; // created dynamically as single demands are selected
	void AddEmployee(JOBDATA *prpPoolJob, EMPDATA *prpEmp, SHIFTDATA *prpShift);

	// The allocation unit is required when selecting pool jobs for pools with restrictions e.g. certain gates may only be 
	// assigned to certain pools. One or both of these are optional e.g. Allocation Unit may be empty when the flight has no gate.
	CString omAllocUnitType, omAllocUnit;

	CTable *pomEmpTable;
	AvailableEmpViewer *pomEmpViewer;

	CString omCaptionObject; // caption requires an object type e.g. flight name, registration or single demand
	void UpdateComboBox();

    CTimeScale omTimeScale;
    int imStartTimeScalePos;
    int imBottomPos;
    CTime omTSStartTime;
    CTimeSpan omTSDuration;
    CTimeSpan omTSInterval;
	NewAssignConflictViewer omDemandsViewer;
	NewAssignConflictGantt omDemandsGantt;

	void InitDemandsGantt(void);
	void InitEmployeesTable(void);
	void InitSelectViewComboBox(void);
	bool HasOverlappingBreakJob(EMPDATA *prpEmp,DEMANDDATA *prpSelectedDemand);
	bool HasOverlapConflict(EMPDATA *prpEmp, DEMANDDATA *prpSelectedDemand);
	bool HasOutsideShiftConflict(EMPDATA *prpEmp, DEMANDDATA *prpSelectedDemand);
	void RemoveBreakJobLine();
	bool IgnoreJobType(const char *pcpJobType);
	void RemoveShiftLines();

public:
	void ProcessSelectDemand(DEMANDDATA *prpDemand);
	void AddDemand(DEMANDDATA *prpDemand);
	void SetAllocUnit(const char *pcpAllocUnitType, const char *pcpAllocUnit);
	void SetCaptionObject(CString opCaptionObject);
	bool SelectDemand(DEMANDDATA *prpDemand);
	int  GetAvailableEmployeesForDemand(DEMANDDATA *prpDemand, bool bpOnlyTestHowManyAvailable = false);

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_AVAILABLEEMPSDLG_H__5C4418B4_45DF_11D5_808B_00010215BFE5__INCLUDED_)
