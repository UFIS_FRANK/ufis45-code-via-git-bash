//
// CedaPfcData.h - Function Stammdaten
//
#ifndef _CEDAPFCDATA_H_
#define _CEDAPFCDATA_H_

#include <CCSCedaData.h>
#include <CCSPtrArray.h>

/////////////////////////////////////////////////////////////////////////////
// Record structure declaration

struct PfcDataStruct
{
	long	Urno;
	char	Fctc[6];	// Function Code
	char	Fctn[41];	// Function Code Description
};

typedef struct PfcDataStruct PFCDATA;


/////////////////////////////////////////////////////////////////////////////
// Class declaration

class CedaPfcData: public CCSCedaData
{
public:
    CCSPtrArray <PFCDATA> omData;
	CMapPtrToPtr omUrnoMap;
	CMapStringToPtr omFctcMap;
	CString GetTableName(void);
	void ProcessPfcBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
	CedaPfcData();
	~CedaPfcData();

	bool ReadPfcData();
	PFCDATA *GetPfcByUrno(long lpUrno);
	PFCDATA *GetPfcByFctc(const char *prpFctc);
	void GetAllFunctions(CStringArray& ropTypes); //PRF 8999

private:
	PFCDATA *AddPfcInternal(PFCDATA &rrpPfc);
	void DeletePfcInternal(long lpUrno);
	void ClearAll();
};


extern CedaPfcData ogPfcData;
#endif _CEDAPFCDATA_H_
