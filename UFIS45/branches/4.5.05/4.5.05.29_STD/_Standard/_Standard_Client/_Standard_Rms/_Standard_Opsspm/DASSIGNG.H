// dassigng.h : header file
//
#ifndef _CASSIGNMENTJOBGATEDIALOG_H_
#define _CASSIGNMENTJOBGATEDIALOG_H_

/////////////////////////////////////////////////////////////////////////////
// CAssignmentJobGateDialog dialog

class CAssignmentJobGateDialog : public CAssignmentDialog
{
// Construction
public:
	CAssignmentJobGateDialog(CWnd* pParent,
		long lpDutyUrno, const char *pcpGateId, const char *pcpGateName);

// Dialog Data
	long lmDutyUrno;
	CString omGateId;
	CString omGateName;
	//{{AFX_DATA(CAssignmentJobGateDialog)
	enum { IDD = IDD_ASSIGNMENT };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAssignmentJobGateDialog)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CAssignmentJobGateDialog)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};
#endif // _CASSIGNMENTJOBGATEDIALOG_H_