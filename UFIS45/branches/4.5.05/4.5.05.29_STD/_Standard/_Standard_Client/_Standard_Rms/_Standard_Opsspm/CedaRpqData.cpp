// DEMAND PERMITS
// CedaRpqData.cpp - table containing one or more permits for each demand
//

#include <afxwin.h>
#include <ccsglobl.h>
#include <CCSPtrArray.h>
#include <CCSCedaData.h>
#include <CedaRpqData.h>
#include <BasicData.h>
#include <CedaRudData.h>

void ProcessRpqCf(void * popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

CedaRpqData::CedaRpqData()
{                  
    BEGIN_CEDARECINFO(RPQDATA, RpqDataRecInfo)
		FIELD_LONG(Urno,"URNO")
		FIELD_LONG(Urud,"URUD")
		FIELD_LONG(Uper,"UPER")
		FIELD_LONG(Udgr,"UDGR")
		FIELD_CHAR_TRIM(Quco,"QUCO")
    END_CEDARECINFO

    // Copy the record structure
    for (int i = 0; i < sizeof(RpqDataRecInfo)/sizeof(RpqDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&RpqDataRecInfo[i],sizeof(CEDARECINFO));
        omRecInfo.Add(prpCedaRecInfo);
	}

	ogCCSDdx.Register((void *)this, BC_RPQ_INSERT, CString("RPQDATA"), CString("Rpq changed"),ProcessRpqCf);
	ogCCSDdx.Register((void *)this, BC_RPQ_UPDATE, CString("RPQDATA"), CString("Rpq changed"),ProcessRpqCf);
	ogCCSDdx.Register((void *)this, BC_RPQ_DELETE, CString("RPQDATA"), CString("Rpq deleted"),ProcessRpqCf);

    // Initialize table names and field names
    strcpy(pcmTableName,"RPQTAB");
    pcmFieldList = "URNO,URUD,UPER,UDGR,QUCO";
}

void ProcessRpqCf(void * popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	((CedaRpqData *)popInstance)->ProcessRpqBc(ipDDXType,vpDataPointer,ropInstanceName);
}

void CedaRpqData::ProcessRpqBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlRpqData = (struct BcStruct *) vpDataPointer;
	ogBasicData.LogBroadcast(prlRpqData);
	RPQDATA rlRpq, *prlRpq = NULL;
	long llUrno = GetUrnoFromSelection(prlRpqData->Selection);
	GetRecordFromItemList(&rlRpq,prlRpqData->Fields,prlRpqData->Data);
	if(llUrno == 0L) llUrno = rlRpq.Urno;

	switch(ipDDXType)
	{
		case BC_RPQ_INSERT:
		{
			if((prlRpq = AddRpqInternal(rlRpq)) != NULL)
			{
				ogCCSDdx.DataChanged((void *)this, RPQ_INSERT, (void *)prlRpq);
			}
			break;
		}
		case BC_RPQ_UPDATE:
		{
			if((prlRpq = GetRpqByUrno(llUrno)) != NULL)
			{
				GetRecordFromItemList(prlRpq,prlRpqData->Fields,prlRpqData->Data);
				//PrepareDataAfterRead(prlRpq);
				ogCCSDdx.DataChanged((void *)this, RPQ_UPDATE, (void *)prlRpq);
			}
			break;
		}
		case BC_RPQ_DELETE:
		{
			if((prlRpq = GetRpqByUrno(llUrno)) != NULL)
			{
				DeleteRpqInternal(prlRpq->Urno);
				ogCCSDdx.DataChanged((void *)this, RPQ_DELETE, (void *)prlRpq);
			}
			break;
		}
	}
}
 
CedaRpqData::~CedaRpqData()
{
	TRACE("CedaRpqData::~CedaRpqData called\n");
	ClearAll();
}

void CedaRpqData::ClearAll()
{
	CMapPtrToPtr *polSingleMap;
	long llUrno;
	POSITION rlPos;
	for(rlPos =  omUrudMap.GetStartPosition(); rlPos != NULL; )
	{
		omUrudMap.GetNextAssoc(rlPos,(void *&) llUrno,(void *& )polSingleMap);
		polSingleMap->RemoveAll();
		delete polSingleMap;
	}
	for(rlPos =  omUdgrMap.GetStartPosition(); rlPos != NULL; )
	{
		omUdgrMap.GetNextAssoc(rlPos,(void *&) llUrno,(void *& )polSingleMap);
		polSingleMap->RemoveAll();
		delete polSingleMap;
	}
	omUrudMap.RemoveAll();
	omUdgrMap.RemoveAll();
	omUrnoMap.RemoveAll();
	omData.DeleteAll();
}

CCSReturnCode CedaRpqData::ReadRpqData(CDWordArray &ropTplUrnos )
{
	CCSReturnCode ilRc = RCSuccess;
	ClearAll();

	char pclCom[10] = "RT";
    char pclWhere[5000] = "";

	if ( ogRudData.bmUtplExists && !ogBasicData.bmDelegateFlights)
	{
		CString olTplUrnos, olTmp;
		int ilNumTplUrnos = ropTplUrnos.GetSize();
		for(int ilTpl = 0; ilTpl < ilNumTplUrnos; ilTpl++)
		{
			if(ilTpl != 0)
				olTplUrnos += ",";
			olTmp.Format("'%d'", ropTplUrnos[ilTpl]);
			olTplUrnos += olTmp;
		}
		sprintf(pclWhere, "WHERE URUD in (select urno from rudtab where UTPL IN (%s)) OR UDGR in (select udgr from rudtab where UTPL IN (%s))", olTplUrnos, olTplUrnos);
	}
    if((ilRc = CedaAction2(pclCom, pclWhere)) != RCSuccess)
	{
		ogBasicData.LogCedaError("CedaRpqData Error",omLastErrorMessage,pclCom,pcmFieldList,pcmTableName,pclWhere);
	}
	else
	{
		RPQDATA rlRpqData;
		for (int ilLc = 0; ilRc == RCSuccess; ilLc++)
		{
			if ((ilRc = GetBufferRecord2(ilLc,&rlRpqData)) == RCSuccess)
			{
				AddRpqInternal(rlRpqData);
			}
		}
		ilRc = RCSuccess;
	}

	ogBasicData.Trace("CedaRpqData Read %d Records (Rec Size %d) %s",omData.GetSize(), sizeof(RPQDATA), pclWhere);
    return ilRc;
}

RPQDATA *CedaRpqData::AddRpqInternal(RPQDATA &rrpRpq)
{
	RPQDATA *prlRpq = new RPQDATA;
	*prlRpq = rrpRpq;
	omData.Add(prlRpq);
	omUrnoMap.SetAt((void *)prlRpq->Urno,prlRpq);
	CMapPtrToPtr *polSingleMap;
	if(omUrudMap.Lookup((void *)prlRpq->Urud, (void *&) polSingleMap))
	{
		polSingleMap->SetAt((void *)prlRpq->Urno,prlRpq);
	}
	else
	{
		polSingleMap = new CMapPtrToPtr;
		polSingleMap->SetAt((void *)prlRpq->Urno,prlRpq);
		omUrudMap.SetAt((void *)prlRpq->Urud,polSingleMap);
	}
	if(omUdgrMap.Lookup((void *)prlRpq->Udgr, (void *&) polSingleMap))
	{
		polSingleMap->SetAt((void *)prlRpq->Urno,prlRpq);
	}
	else
	{
		polSingleMap = new CMapPtrToPtr;
		polSingleMap->SetAt((void *)prlRpq->Urno,prlRpq);
		omUdgrMap.SetAt((void *)prlRpq->Udgr,polSingleMap);
	}

	return prlRpq;
}

void CedaRpqData::DeleteRpqInternal(long lpUrno)
{
	int ilNumRpqs = omData.GetSize();
	for(int ilRpq = (ilNumRpqs-1); ilRpq >= 0; ilRpq--)
	{
		if(omData[ilRpq].Urno == lpUrno)
		{
			CMapPtrToPtr *polSingleMap;
			if (omUrudMap.Lookup((void *) omData[ilRpq].Urud, (void *&) polSingleMap) == TRUE)
			{
				polSingleMap->RemoveKey((void * ) lpUrno);
			}

			omData.DeleteAt(ilRpq);
		}
	}
	omUrnoMap.RemoveKey((void *)lpUrno);
}

RPQDATA *CedaRpqData::GetRpqByUrno(long lpUrno)
{
	RPQDATA *prlRpq = NULL;
	omUrnoMap.Lookup((void *)lpUrno, (void *&) prlRpq);
	return prlRpq;
}

void CedaRpqData::GetRpqsByUrud(long lpUrud, CCSPtrArray <RPQDATA> &ropRpqs, bool bpReset /*true*/)
{
	if(bpReset)
	{
	    ropRpqs.RemoveAll();
	}

	CMapPtrToPtr *polSingleMap;
	long llRpqUrno;
	RPQDATA *prlRpq;

	if(omUrudMap.Lookup((void *)lpUrud,(void *& )polSingleMap))
	{
		for(POSITION rlPos = polSingleMap->GetStartPosition(); rlPos != NULL; )
		{
			polSingleMap->GetNextAssoc(rlPos,(void *&)llRpqUrno,(void *& )prlRpq);
			ropRpqs.Add(prlRpq);
		}
	}
}


void CedaRpqData::GetRpqsByUdgr(long lpUdgr, CCSPtrArray <RPQDATA> &ropRpqs, bool bpReset /*true*/)
{
	if(bpReset)
	{
	    ropRpqs.RemoveAll();
	}

	CMapPtrToPtr *polSingleMap;
	long llRpqUrno;
	RPQDATA *prlRpq;

	if(omUdgrMap.Lookup((void *)lpUdgr,(void *& )polSingleMap))
	{
		for(POSITION rlPos = polSingleMap->GetStartPosition(); rlPos != NULL; )
		{
			polSingleMap->GetNextAssoc(rlPos,(void *&)llRpqUrno,(void *& )prlRpq);
			ropRpqs.Add(prlRpq);
		}
	}
}


CString CedaRpqData::GetTableName(void)
{
	return CString(pcmTableName);
}