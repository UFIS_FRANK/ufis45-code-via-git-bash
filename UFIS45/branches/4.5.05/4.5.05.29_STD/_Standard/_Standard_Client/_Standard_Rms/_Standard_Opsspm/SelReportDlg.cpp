// SelReportDlg.cpp : implementation file
//

#include <stdafx.h>
#include <opsspm.h>
#include <SelReportDlg.h>
#include <BasicData.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSelReportDlg dialog


CSelReportDlg::CSelReportDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CSelReportDlg::IDD, pParent)
{
	omCaption = GetString(IDS_SELREPORTDLGTITLE);
	//{{AFX_DATA_INIT(CSelReportDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}

CSelReportDlg::CSelReportDlg(CString opCaption, CWnd* pParent /*=NULL*/)
	: CDialog(CSelReportDlg::IDD, pParent)
{
	omCaption = opCaption;
	//{{AFX_DATA_INIT(CSelReportDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CSelReportDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSelReportDlg)
	DDX_Control(pDX, IDC_SELREPORTLIST, m_SelReportListCtrl);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CSelReportDlg, CDialog)
	//{{AFX_MSG_MAP(CSelReportDlg)
	ON_LBN_DBLCLK(IDC_SELREPORTLIST, OnDoubleClickSelReportList)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSelReportDlg message handlers

BOOL CSelReportDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	SetWindowText(omCaption);

	for(int ilR = 0; ilR < omReports.GetSize(); ilR++)
	{
		m_SelReportListCtrl.AddString(omReports[ilR]);
	}
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CSelReportDlg::OnOK() 
{
	int ilSel = m_SelReportListCtrl.GetCurSel();
	if(ilSel != CB_ERR)
		m_SelReportListCtrl.GetText(ilSel,omSelectedReport);
	CDialog::OnOK();
}

void CSelReportDlg::SetReports(CString &ropReport)
{
	omReports.Add(ropReport);
}

void CSelReportDlg::OnDoubleClickSelReportList() 
{
	OnOK();
}
