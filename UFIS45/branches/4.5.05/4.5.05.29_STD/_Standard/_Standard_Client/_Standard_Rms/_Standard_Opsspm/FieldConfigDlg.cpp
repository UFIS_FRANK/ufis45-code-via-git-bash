// FieldConfigDlg.cpp : implementation file
//


//HOW TO USE THIS DIALOG:
//
//in the setup dialog add the following lines to define which fields are displayed in the field selection list:
//
//	// "Staff Gantt Chart Tool Tip";
//	olFieldConfigDlg.AddField(IDS_FIELDCONFIG_STAFFTOOLTIP, 0, IDS_STAFFTOOLTIP_EMPLOYEE, "John Smith", 30); // "Employee Name"
//	olFieldConfigDlg.AddField(IDS_FIELDCONFIG_STAFFTOOLTIP, 1, IDS_STAFFTOOLTIP_SHIFTCODE, "SC0070", 10); // "Shift Code"
//	olFieldConfigDlg.AddField(IDS_FIELDCONFIG_STAFFTOOLTIP, 2, IDS_STAFFTOOLTIP_FUNCTION, "FUNC1", 10); // "Function"
//	olFieldConfigDlg.AddField(IDS_FIELDCONFIG_STAFFTOOLTIP, 3, IDS_STAFFTOOLTIP_WORKGROUP, "WRK GRP 1", 10); // "Work Group"
//	olFieldConfigDlg.AddField(IDS_FIELDCONFIG_STAFFTOOLTIP, 4, IDS_STAFFTOOLTIP_SHIFTIND, "Shift 007", 40); // "Basic Shift Indicator"
//	olFieldConfigDlg.AddField(IDS_FIELDCONFIG_STAFFTOOLTIP, 5, IDS_STAFFTOOLTIP_MULTIFUNC, "1: ABCEF,2: VWXYZ", 40); // "Basic Shift Indicator"
//
//where:	IDS_FIELDCONFIG_STAFFTOOLTIP - string ID - where the configured field is displayed 
//		0 - integer key - running number indexing each field IMPORTANT see "notes on the keys" below
//		IDS_STAFFTOOLTIP_EMPLOYEE - string ID - name of the field
//		"John Smith" - string constant - sample text
//		30 - integer - default field length
//
// notes on the keys (the running numbers - 2nd param in AddField())
// never change these values as they are used as an index into an array
// to remove a line - just remove it, don't change the numbering
// to add a line - increment the key and insert the line in the order that it should be displayed in the list
//
//in the viewer (or wherever) add the following lines
//
//	CString olBarText = ogFieldConfig.ConfigureField(IDS_FIELDCONFIG_PSTBARTURN, "%s%s%s%s%s%s%s%s%d%s%s%s%s%s%s%d", 
//											prlArr->Regn, prlArr->Act3, prlArr->Fnum, prlArr->Org3, prlArr->Stoa.Format("%H%M"), 
//											prlArr->Etoa.Format("%H%M"), GetGateText(prlArr), GetPositionText(prlArr), prlArr->Pax1, 
//											prlDep->Fnum, prlDep->Des3, prlDep->Stod.Format("%H%M"), prlDep->Etod.Format("%H%M"), 
//											GetGateText(prlDep), GetPositionText(prlDep),prlDep->Pax1);
//
// in the functions ogFieldConfig.ConfigureField(), never remove a value, even when a field is deleted from
// the function calls olFieldConfigDlg.AddField(), always add new values to the end of the list


#include <stdafx.h>
#include <opsspm.h>
#include <FieldConfigDlg.h>
#include <GUILng.h>
#include <CCSGlobl.h>
#include <BasicData.h>
#include <CedaCfgData.h>
#include <DlgSettings.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CFieldConfigDlg dialog


CFieldConfigDlg::CFieldConfigDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CFieldConfigDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CFieldConfigDlg)
	m_NumChars = 0;
	m_Preview = _T("");
	m_SampleText = _T("");
	m_Suffix = _T("");
	//}}AFX_DATA_INIT

	pomSelectedFieldType = NULL;
}

CFieldConfigDlg::~CFieldConfigDlg()
{
	int ilNumFieldTypes = omFieldTypes.GetSize();
	for(int ilFT = 0; ilFT < ilNumFieldTypes; ilFT++)
	{
		FIELDTYPE *prlFieldType = &omFieldTypes[ilFT];
		prlFieldType->PossibleFields.DeleteAll();
		prlFieldType->SelectedFields.DeleteAll();
	}
	omFieldTypes.DeleteAll();
}

void CFieldConfigDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CFieldConfigDlg)
	DDX_Control(pDX, IDC_FIELDTYPE, m_FieldTypeCtrl);
	DDX_Control(pDX, IDC_LIST2, m_List2Ctrl);
	DDX_Control(pDX, IDC_LIST1, m_List1Ctrl);
	DDX_Text(pDX, IDC_NUMCHARS, m_NumChars);
	DDX_Text(pDX, IDC_PREVIEW, m_Preview);
	DDX_Text(pDX, IDC_SAMPLE, m_SampleText);
	DDX_Text(pDX, IDC_SUFFIX, m_Suffix);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CFieldConfigDlg, CDialog)
	//{{AFX_MSG_MAP(CFieldConfigDlg)
	ON_BN_CLICKED(IDC_ADD, OnAdd)
	ON_BN_CLICKED(IDC_REMOVE, OnRemove)
	ON_LBN_SELCHANGE(IDC_LIST2, OnSelchangeList2)
	ON_EN_KILLFOCUS(IDC_NUMCHARS, OnKillfocusNumchars)
	ON_EN_KILLFOCUS(IDC_SUFFIX, OnKillfocusSuffix)
	ON_CBN_SELCHANGE(IDC_FIELDTYPE, OnSelchangeFieldtype)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CFieldConfigDlg message handlers

BOOL CFieldConfigDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	ogDlgSettings.EnableSaveToDb("FieldConfigDlg"); // causes settings to be read/written to and from the DB

	SetWindowText(GetString(IDS_FCD_TITLE));
	SetControlText(IDC_FIELDTYPETITLE,IDS_FCD_FIELDTYPETITLE);
	SetControlText(IDC_NUMCHARSTITLE,IDS_FCD_NUMCHARSTITLE);
	SetControlText(IDC_SUFFIXTITLE,IDS_FCD_SUFFIXTITLE);
	SetControlText(IDC_SAMPLETITLE,IDS_FCD_SAMPLETITLE);
	SetControlText(IDC_PREVIEWTITLE,IDS_FCD_PREVIEWTITLE);
	SetControlText(IDOK,IDS_FCD_OK);
	SetControlText(IDCANCEL,IDS_FCD_CANCEL);

	int ilNumFieldTypes = omFieldTypes.GetSize();
	for(int ilFT = 0; ilFT < ilNumFieldTypes; ilFT++)
	{
		m_FieldTypeCtrl.AddString(omFieldTypes[ilFT].FieldTypeText);
	}

	LoadSettings(); // load the settings as they were last saved in the DB

	m_FieldTypeCtrl.SetCurSel(0);
	OnSelchangeFieldtype();

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CFieldConfigDlg::SetControlText(int ipControlId, int ipStringId)
{
	CWnd *polWnd = GetDlgItem(ipControlId); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(ipStringId));
	}
}

void CFieldConfigDlg::AddField(int ipFieldType, int ipKey, int ipFieldId, CString opSampleText, int ipDefaultNumChars, int ipSortOrder /*= -1*/)
{
	CString olFieldType; 
	olFieldType.Format("%d", ipFieldType);

	FIELDTYPE *prlFieldType = NULL;
	int ilNumFieldTypes = omFieldTypes.GetSize();
	for(int ilFT = 0; ilFT < ilNumFieldTypes; ilFT++)
	{
		if(omFieldTypes[ilFT].FieldType == olFieldType)
		{
			prlFieldType = &omFieldTypes[ilFT];
			break;
		}
	}
	if(prlFieldType == NULL)
	{
		prlFieldType = new FIELDTYPE;
		prlFieldType->FieldType = olFieldType;
		prlFieldType->FieldTypeText = GetString(ipFieldType);
		omFieldTypes.Add(prlFieldType);
	}

	FIELDDATA *prlField = new FIELDDATA;
	prlField->Field = GetString(ipFieldId);
	prlField->Text = opSampleText;
	prlField->NumChars = ipDefaultNumChars;
	prlField->Key = ipKey;
	prlField->SortOrder = ipSortOrder;
	prlFieldType->PossibleFields.Add(prlField);
}

void CFieldConfigDlg::OnOK() 
{
	SaveSettings();
	CDialog::OnOK();
}

void CFieldConfigDlg::SaveSettings(void)
{
	ogDlgSettings.DeleteSettingsByDialog("FieldConfigDlg");

	CUIntArray olKeys, olNumChars, olSortOrders;
	CStringArray olSuffixes, olFields;
	CString olWhere;

	int ilNumFields = omFieldTypes.GetSize();
	for(int ilF = 0; ilF < ilNumFields; ilF++)
	{
		FIELDTYPE *prlFieldType = &omFieldTypes[ilF];

		olKeys.RemoveAll();
		olNumChars.RemoveAll();
		olSuffixes.RemoveAll();
		olFields.RemoveAll();
		olSortOrders.RemoveAll();
		olWhere = prlFieldType->FieldType;

		CString olValue, olTmp;
		int ilNumSF = prlFieldType->SelectedFields.GetSize();
		for(int ilSF = 0; ilSF < ilNumSF; ilSF++)
		{
			FIELDDATA *prlData = &prlFieldType->SelectedFields[ilSF];
			olTmp.Format("<%s><%d><%d+%d><%d><%s>", prlData->Field, prlData->NumChars, prlData->Key, prlData->SortOrder, prlData->Suffix.GetLength(), prlData->Suffix);
			olValue += olTmp;

			olFields.Add(prlData->Field);
			olKeys.Add(prlData->Key);
			olNumChars.Add(prlData->NumChars);
			olSuffixes.Add(prlData->Suffix);
			olSortOrders.Add(prlData->SortOrder);
		}

		ogFieldConfig.AddConfiguredField(olWhere, olFields, olKeys, olNumChars, olSuffixes, olSortOrders);

		if(olValue.GetLength() > 0)
		{
			ogDlgSettings.AddValue("FieldConfigDlg", prlFieldType->FieldType, olValue);
		}
	}
}

void CFieldConfigDlg::LoadSettings(void)
{
	int ilNumFields = omFieldTypes.GetSize();
	for(int ilF = 0; ilF < ilNumFields; ilF++)
	{
		FIELDTYPE *prlFieldType = &omFieldTypes[ilF];

		CString olValue;
		ogDlgSettings.GetValue("FieldConfigDlg", prlFieldType->FieldType, olValue);

		CStringArray olFields, olSuffixes;
		CUIntArray olNumChars, olKeys, olSortOrders;
		ogFieldConfig.ExtractFieldsFromValue(olValue, olFields, olKeys, olNumChars, olSuffixes, olSortOrders);

		int ilNumDbVals = olFields.GetSize();
		for(int ilDbVal = 0; ilDbVal < ilNumDbVals; ilDbVal++)
		{
			int ilNumPF = prlFieldType->PossibleFields.GetSize();
			for(int ilPF = 0; ilPF < ilNumPF; ilPF++)
			{
				FIELDDATA *prlData = &prlFieldType->PossibleFields[ilPF];
				if(prlData->Field == olFields[ilDbVal])
				{
					prlFieldType->PossibleFields.RemoveAt(ilPF);
					prlData->NumChars = olNumChars[ilDbVal];
					prlData->Key = olKeys[ilDbVal];
					prlData->Suffix = olSuffixes[ilDbVal];
					prlData->SortOrder = olSortOrders[ilDbVal];
					prlFieldType->SelectedFields.Add(prlData);
					break;
				}
			}
		}
	}
}

void CFieldConfigDlg::GetFieldFormat(int ipFieldType, CUIntArray &ropKeys, CUIntArray &ropNumChars, CStringArray &ropSuffixes)
{
	CString olFieldType;
	olFieldType.Format("%d", ipFieldType);

	ropKeys.RemoveAll();
	ropNumChars.RemoveAll();
	ropSuffixes.RemoveAll();

	int ilNumFT = omFieldTypes.GetSize();
	for(int ilFT = 0; ilFT < ilNumFT; ilFT++)
	{
		FIELDTYPE *prlFieldType = &omFieldTypes[ilFT];
		if(prlFieldType->FieldType == olFieldType)
		{
			CString olTmp;
			int ilNumF = prlFieldType->SelectedFields.GetSize();
			for(int ilF = 0; ilF < ilNumF; ilF++)
			{
				FIELDDATA *prlField = &prlFieldType->SelectedFields[ilF];
				ropKeys.Add(prlField->Key);
				ropNumChars.Add(prlField->NumChars);
				ropSuffixes.Add(prlField->Suffix);
			}
			break;
		}
	}
}

void CFieldConfigDlg::OnAdd() 
{
	int ilSelLine = m_List1Ctrl.GetCurSel();
	if(ilSelLine != LB_ERR && ilSelLine >= 0 && ilSelLine < pomSelectedFieldType->PossibleFields.GetSize())
	{
		FIELDDATA *prlSelectedField = &pomSelectedFieldType->PossibleFields[ilSelLine];

		bool blCorrectPosition = true;
		int ilNumSelectedFields = pomSelectedFieldType->SelectedFields.GetSize();
		if(prlSelectedField->SortOrder != -1 && ilNumSelectedFields > 0 &&
			pomSelectedFieldType->SelectedFields[ilNumSelectedFields-1].SortOrder > prlSelectedField->SortOrder)
		{
			blCorrectPosition = false;
		}

		int ilPos = ilNumSelectedFields;
		if(!blCorrectPosition)
		{
			if(MessageBox(GetString(IDS_FCD_REPOSITION), GetString(IDS_FCD_TITLE), MB_OKCANCEL|MB_ICONWARNING) != IDOK)
				return;

			ilPos = 0;
			for(int ilSF = (ilNumSelectedFields-1); ilSF >= 0; ilSF--)
			{
				if(pomSelectedFieldType->SelectedFields[ilSF].SortOrder <= prlSelectedField->SortOrder)
				{
					ilPos = ilSF+1;
					break;
				}
			}
		}
		
		pomSelectedFieldType->PossibleFields.RemoveAt(ilSelLine);
		m_List1Ctrl.DeleteString(ilSelLine);
		
		//pomSelectedFieldType->SelectedFields.Add(prlSelectedField);
		pomSelectedFieldType->SelectedFields.InsertAt(ilPos, prlSelectedField);
		int ilNewLine = m_List2Ctrl.InsertString(ilPos, FormatField(prlSelectedField));

		m_List2Ctrl.SetCurSel(ilNewLine);
		OnSelchangeList2();
	}
}

void CFieldConfigDlg::OnRemove() 
{
	int ilSelLine = m_List2Ctrl.GetCurSel();
	if(ilSelLine != LB_ERR && ilSelLine >= 0 && ilSelLine < pomSelectedFieldType->SelectedFields.GetSize())
	{
		FIELDDATA *prlSelectedField = &pomSelectedFieldType->SelectedFields[ilSelLine];

		pomSelectedFieldType->SelectedFields.RemoveAt(ilSelLine);
		m_List2Ctrl.DeleteString(ilSelLine);
		
		pomSelectedFieldType->PossibleFields.Add(prlSelectedField);
		m_List1Ctrl.AddString(FormatField(prlSelectedField));

		m_List2Ctrl.SetCurSel(0);
		OnSelchangeList2();
	}
}

void CFieldConfigDlg::OnSelchangeList2() 
{
	int ilSelLine = m_List2Ctrl.GetCurSel();
	if(ilSelLine != LB_ERR && ilSelLine >= 0 && ilSelLine < pomSelectedFieldType->SelectedFields.GetSize())
	{
		FIELDDATA *prlSelectedField = &pomSelectedFieldType->SelectedFields[ilSelLine];
		m_NumChars = prlSelectedField->NumChars;
		m_Suffix = prlSelectedField->Suffix;
		m_SampleText = prlSelectedField->Text;
	}
	else
	{
		m_NumChars = 0;
		m_Suffix = "";
		m_SampleText = "";
	}
	UpdatePreview();
	UpdateData(FALSE);
}

void CFieldConfigDlg::OnKillfocusNumchars() 
{
	UpdateFieldData();
}

void CFieldConfigDlg::OnKillfocusSuffix() 
{
	UpdateFieldData();
}

void CFieldConfigDlg::UpdateFieldData()
{
	UpdateData();
	int ilSelLine = m_List2Ctrl.GetCurSel();
	if(ilSelLine != LB_ERR && ilSelLine >= 0 && ilSelLine < pomSelectedFieldType->SelectedFields.GetSize())
	{
		FIELDDATA *prlSelectedField = &pomSelectedFieldType->SelectedFields[ilSelLine];
		prlSelectedField->NumChars = m_NumChars;
		prlSelectedField->Suffix = m_Suffix;
	}
	UpdatePreview();
}

void CFieldConfigDlg::OnSelchangeFieldtype() 
{
	int ilSelLine = m_FieldTypeCtrl.GetCurSel();
	if(ilSelLine != CB_ERR && ilSelLine >= 0 && ilSelLine < omFieldTypes.GetSize())
	{
		pomSelectedFieldType = &omFieldTypes[ilSelLine];
		m_List1Ctrl.ResetContent();
		m_List2Ctrl.ResetContent();
		m_NumChars = 0;
		m_Suffix = "";
		m_SampleText = "";
		m_Preview = "";

		int ilNumPossibleFields = pomSelectedFieldType->PossibleFields.GetSize();
		for(int ilPF = 0; ilPF < ilNumPossibleFields; ilPF++)
		{
			m_List1Ctrl.AddString(FormatField(&pomSelectedFieldType->PossibleFields[ilPF]));
		}

		int ilNumSelectedFields = pomSelectedFieldType->SelectedFields.GetSize();
		for(int ilSF = 0; ilSF < ilNumSelectedFields; ilSF++)
		{
			m_List2Ctrl.AddString(FormatField(&pomSelectedFieldType->SelectedFields[ilSF]));
		}
		if(ilNumSelectedFields)
			m_List2Ctrl.SetCurSel(0);
		OnSelchangeList2();

		UpdateFieldData();
	}
}

void CFieldConfigDlg::UpdatePreview()
{
	CString olPreview;
	int ilNumSelectedFields = pomSelectedFieldType->SelectedFields.GetSize();
	for(int ilSF = 0; ilSF < ilNumSelectedFields; ilSF++)
	{
		olPreview += CString(pomSelectedFieldType->SelectedFields[ilSF].Text.Left(pomSelectedFieldType->SelectedFields[ilSF].NumChars)) +
						CString(pomSelectedFieldType->SelectedFields[ilSF].Suffix);
	}
	m_Preview = olPreview;
	UpdateData(FALSE);
}

CString CFieldConfigDlg::FormatField(FIELDDATA *prpField)
{
	if(prpField->SortOrder != -1)
	{
		CString olFieldText;
		olFieldText.Format("%d. %s", prpField->SortOrder, prpField->Field);
		return olFieldText;
	}

	return prpField->Field;
}


////////////////////////////////////////////////////////////////////////////////////////////////////////////
CFieldDataArray::CFieldDataArray()
{
}

CFieldDataArray::~CFieldDataArray()
{
	RemoveAll();
}

CFieldDataArray::RemoveAll()
{
	omData.DeleteAll();
}

void CFieldDataArray::Add(FIELDDATA *prpFieldData)
{
	FIELDDATA *prlNewField = new FIELDDATA;
	*prlNewField = *prpFieldData;
	omData.Add(prlNewField);
}

void CFieldDataArray::Add(CString opField, CString opText, int ipNumChars, CString opSuffix, int ipKey /* = -1 */, int ipSortOrder /* = -1*/)
{
	FIELDDATA olField;
	olField.Field = opField;
	olField.Text = opText;
	olField.NumChars = ipNumChars;
	olField.Suffix = opSuffix;
	olField.Key = ipKey;
	olField.SortOrder = ipSortOrder;
	Add(&olField);
}

int CFieldDataArray::GetSize(void)
{
	return omData.GetSize();
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////
CFieldConfig::CFieldConfig()
{
}

CFieldConfig::~CFieldConfig()
{
	CONFIGUREDFIELDDATA *prlFieldFormat = NULL;
	CString olFieldType;
	for(POSITION rlPos = omFieldFormats.GetStartPosition(); rlPos != NULL; )
	{
		omFieldFormats.GetNextAssoc(rlPos, olFieldType, (void *& )prlFieldFormat);
		delete prlFieldFormat;
	}
	omFieldFormats.RemoveAll();
}

void CFieldConfig::Initialize()
{
	ogDlgSettings.LoadSettingsFromDb("FieldConfigDlg");

	CStringArray olFieldTypes, olValues;
	ogDlgSettings.GetAllValuesForDialog("FieldConfigDlg", olFieldTypes, olValues);
	int ilCnt = olFieldTypes.GetSize();
	for(int i = 0; i < ilCnt; i++)
	{
		CStringArray olFields, olSuffixes;
		CUIntArray olNumChars, olKeys, olSortOrders;
		ExtractFieldsFromValue(olValues[i], olFields, olKeys, olNumChars, olSuffixes, olSortOrders);
		AddConfiguredField(olFieldTypes[i], olFields, olKeys, olNumChars, olSuffixes, olSortOrders);
	}
}

void CFieldConfig::ExtractFieldsFromValue(CString &ropValue, CStringArray &ropFields, CUIntArray &ropKeys, CUIntArray &ropNumChars, CStringArray &ropSuffixes, CUIntArray &ropSortOrders)
{
	CString olTmp;
	int ilSep = -1;
	int ilValueLen = ropValue.GetLength();
	for(int i = 0; i < ilValueLen; i++)
	{
		// Field name
		olTmp.Empty();
		while(i < ilValueLen && ropValue[i++] != '<');
		while(i < ilValueLen && ropValue[i] != '>') olTmp += ropValue[i++];
		ropFields.Add(olTmp);

		// Num Chars
		olTmp.Empty();
		while(i < ilValueLen && ropValue[i++] != '<');
		while(i < ilValueLen && ropValue[i] != '>') olTmp += ropValue[i++];
		ropNumChars.Add(atoi(olTmp));

		// Key (and sort order if present)
		olTmp.Empty();
		while(i < ilValueLen && ropValue[i++] != '<');
		while(i < ilValueLen && ropValue[i] != '>') olTmp += ropValue[i++];
		ropKeys.Add(atoi(olTmp));
		if((ilSep = olTmp.Find('+') + 1) != 0)
			ropSortOrders.Add(atoi(olTmp.Mid(ilSep,(olTmp.GetLength()-ilSep))));
		else
			ropSortOrders.Add(-1);

		// Suffix len
		olTmp.Empty();
		while(i < ilValueLen && ropValue[i++] != '<');
		while(i < ilValueLen && ropValue[i] != '>') olTmp += ropValue[i++];
		int ilSuffixLen = atoi(olTmp);

		// Suffix
		olTmp.Empty();
		i += 2;
		for(int ilS = 0; i < ilValueLen && ilS < ilSuffixLen; ilS++) olTmp += ropValue[i++];
		ropSuffixes.Add(olTmp);
	}
}

void CFieldConfig::AddConfiguredField(CString opFieldType, CStringArray &ropFields, CUIntArray &ropKeys, CUIntArray &ropNumChars, CStringArray &ropSuffixes, CUIntArray &ropSortOrders)
{
	// remove old entry for this field type
	CONFIGUREDFIELDDATA *prlFieldFormat = NULL;
	if(omFieldFormats.Lookup(opFieldType, (void *&) prlFieldFormat))
		delete prlFieldFormat;

	// add new entry
	prlFieldFormat = new CONFIGUREDFIELDDATA;
	prlFieldFormat->Fields.Append(ropFields);
	prlFieldFormat->Keys.Append(ropKeys);
	prlFieldFormat->NumChars.Append(ropNumChars);
	prlFieldFormat->Suffixes.Append(ropSuffixes);
	prlFieldFormat->SortOrders.Append(ropSortOrders);

	omFieldFormats.SetAt(opFieldType, (void *) prlFieldFormat);
}

CONFIGUREDFIELDDATA *CFieldConfig::GetFieldFormat(CString opFieldType)
{
	CONFIGUREDFIELDDATA *prlFieldFormat = NULL;
	omFieldFormats.Lookup(opFieldType, (void *&) prlFieldFormat);
	return prlFieldFormat;
}

CString CFieldConfig::ConfigureField(CONFIGUREDFIELDDATA *prpFieldFormat, CStringArray &ropValues)
{
	CString olFormattedString = "";
	if(prpFieldFormat != NULL)
	{
		for(int i = 0; i < prpFieldFormat->Keys.GetSize(); i++)
		{
			if(prpFieldFormat->Keys[i] >= 0 && (int) prpFieldFormat->Keys[i] < ropValues.GetSize())
				olFormattedString += ropValues[prpFieldFormat->Keys[i]].Left(prpFieldFormat->NumChars[i]) + prpFieldFormat->Suffixes[i];
		}
	}
	return olFormattedString;
}

void CFieldConfig::ConfigureField(CONFIGUREDFIELDDATA *prpFieldFormat, CStringArray &ropValues, CFieldDataArray &ropFields)
{
	if(prpFieldFormat != NULL)
	{
		int ilKey, ilNumChars;
		for(int i = 0; i < prpFieldFormat->Keys.GetSize(); i++)
		{
			ilKey = prpFieldFormat->Keys[i];
			ilNumChars = prpFieldFormat->NumChars[i];
			if(ilKey >= 0 && ilKey < ropValues.GetSize())
			{
				CString olText = ropValues[ilKey].Left(ilNumChars) + prpFieldFormat->Suffixes[i];
				ropFields.Add(prpFieldFormat->Fields[i], olText, ilNumChars, prpFieldFormat->Suffixes[i], ilKey, prpFieldFormat->SortOrders[i]);
			}
		}
	}
}

CString CFieldConfig::ConfigureField(int ipFieldType, char *fmt, ...)
{
	CString olFieldType; olFieldType.Format("%d", ipFieldType);
	CONFIGUREDFIELDDATA *prlFieldFormat = GetFieldFormat(olFieldType);
	if(prlFieldFormat == NULL || prlFieldFormat->Keys.GetSize() <= 0)
		return CString("");

	CStringArray olValues;

	va_list ap;
	char *p, *sval;
	CString olTmp;

	va_start(ap, fmt);
	for(p = fmt; *p; p++)
	{
		if (*p != '%')
		   continue;

		olTmp.Empty();

		switch (*++p) 
		{
		case 'd': 
			olTmp.Format("%d",va_arg(ap, int));
			break;
		case 'f': 
			olTmp.Format("%f",va_arg(ap, double));
			break;
		case 'l': 
			olTmp.Format("%ld",va_arg(ap, long));
			break;
		case 's': 
			for(sval = va_arg(ap, char *); *sval; sval++) olTmp += *sval;
			break;
		default:
			ASSERT(0);
			break;
		}

		olValues.Add(olTmp);
	}

	va_end(ap);

	return ConfigureField(prlFieldFormat, olValues);
}

void CFieldConfig::ConfigureField(int ipFieldType, CFieldDataArray &ropFields, char *fmt, ...)
{
	CString olFieldType; olFieldType.Format("%d", ipFieldType);
	CONFIGUREDFIELDDATA *prlFieldFormat = GetFieldFormat(olFieldType);
	if(prlFieldFormat != NULL && prlFieldFormat->Keys.GetSize() >= 0)
	{
		CStringArray olValues;

		va_list ap;
		char *p, *sval;
		CString olTmp;

		va_start(ap, fmt);
		for(p = fmt; *p; p++)
		{
			if (*p != '%')
			   continue;

			olTmp.Empty();

			switch (*++p) 
			{
			case 'd': 
				olTmp.Format("%d",va_arg(ap, int));
				break;
			case 'f': 
				olTmp.Format("%f",va_arg(ap, double));
				break;
			case 'l': 
				olTmp.Format("%ld",va_arg(ap, long));
				break;
			case 's': 
				for(sval = va_arg(ap, char *); *sval; sval++) olTmp += *sval;
				break;
			default:
				ASSERT(0);
				break;
			}

			olValues.Add(olTmp);
		}
		va_end(ap);

		ConfigureField(prlFieldFormat, olValues, ropFields);
	}
}

