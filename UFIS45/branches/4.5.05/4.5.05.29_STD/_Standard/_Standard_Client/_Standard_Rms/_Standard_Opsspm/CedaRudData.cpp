// RULE DEMANDS - needed because it contains extra info about demands
// CedaRudData.cpp
//

#include <afxwin.h>
#include <ccsglobl.h>
#include <CCSPtrArray.h>
#include <CCSCedaData.h>
#include <CedaRudData.h>
#include <BasicData.h>
#include <CedaRudData.h>
#include <CedaTplData.h>

void  ProcessRudCf(void * popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

CedaRudData::CedaRudData()
{                  
    BEGIN_CEDARECINFO(RUDDATA, RudDataRecInfo)
		FIELD_LONG(Urno,"URNO")
		FIELD_CHAR_TRIM(Rety,"RETY") 
		FIELD_CHAR_TRIM(Aloc,"ALOC")
		FIELD_LONG(Urue,"URUE") 
		FIELD_LONG(Udgr,"UDGR")
		FIELD_LONG(Ulnk,"ULNK")
		FIELD_LONG(Ughs,"UGHS")
		FIELD_CHAR_TRIM(Reft,"REFT")
		FIELD_LONG(Vref,"VREF")
		FIELD_LONG(Ttgf,"TTGF")
		FIELD_LONG(Ttgt,"TTGT")
		FIELD_CHAR_TRIM(Disp,"DISP")
		FIELD_CHAR_TRIM(Rede,"REDE")
		FIELD_LONG(Unde,"UNDE")
		FIELD_LONG(Upde,"UPDE")
		FIELD_CHAR_TRIM(Drty,"DRTY")
		FIELD_LONG(Dedu,"DEDU")
		FIELD_CHAR_TRIM(Rtdb,"RTDB") 
		FIELD_CHAR_TRIM(Rtde,"RTDE")
		FIELD_LONG(Tsdb,"TSDB") 
		FIELD_LONG(Tsde,"TSDE")
		FIELD_LONG(Sdti,"SDTI")
		FIELD_LONG(Suti,"SUTI")
    END_CEDARECINFO

    // Copy the record structure
    for (int i = 0; i < sizeof(RudDataRecInfo)/sizeof(RudDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&RudDataRecInfo[i],sizeof(CEDARECINFO));
        omRecInfo.Add(prpCedaRecInfo);
	}
	bmUtplExists = false; 
    // Initialize table names and field names
    strcpy(pcmTableName,"RUDTAB");
    pcmFieldList = "URNO,RETY,ALOC,URUE,UDGR,ULNK,UGHS,REFT,VREF,TTGF,TTGT,DISP,REDE,UNDE,UPDE,DRTY,DEDU,RTDB,RTDE,TSDB,TSDE,SDTI,SUTI";
	ogCCSDdx.Register((void *)this,BC_RUD_CHANGE,CString("RUDDATA"), CString("Rud changed"),ProcessRudCf);
}
 
CedaRudData::~CedaRudData()
{
	TRACE("CedaRudData::~CedaRudData called\n");
	ClearAll();
	ogCCSDdx.UnRegister(this,NOTUSED);
}

void  ProcessRudCf(void * popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	((CedaRudData *)popInstance)->ProcessRudBc(ipDDXType,vpDataPointer,ropInstanceName);
}

void CedaRudData::ProcessRudBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlRudData = (struct BcStruct *) vpDataPointer;
	ogBasicData.LogBroadcast(prlRudData);
	long llUrno = GetUrnoFromSelection(prlRudData->Selection);
	if(llUrno == 0L)
	{
		RUDDATA rlRud;
		GetRecordFromItemList(&rlRud,prlRudData->Fields,prlRudData->Data);
		llUrno = rlRud.Urno;
	}

	if(llUrno != 0L)
	{
		RUDDATA *prlRud = GetRudByUrno(llUrno);

		if(ipDDXType == BC_RUD_CHANGE && prlRud != NULL)
		{
			// update
			GetRecordFromItemList(prlRud,prlRudData->Fields,prlRudData->Data);
		}
		else if(ipDDXType == BC_RUD_CHANGE && prlRud == NULL)
		{
			// insert
			RUDDATA rlRud;
			GetRecordFromItemList(&rlRud,prlRudData->Fields,prlRudData->Data);
			AddRudInternal(rlRud);
		}
	}
}

void CedaRudData::ClearAll()
{
	omUrnoMap.RemoveAll();
	POSITION pos = omUlnkMap.GetStartPosition();
	while(pos)
	{
		long olUlnk;
		CCSPtrArray<RUDDATA> *polArray = NULL;
		omUlnkMap.GetNextAssoc(pos,(void *&)olUlnk,(void *&)polArray);
		delete polArray;
	}
	omUlnkMap.RemoveAll();

	pos = omUrueMap.GetStartPosition();
	while(pos)
	{
		long olUrue;
		CCSPtrArray<RUDDATA> *polArray2 = NULL;
		omUrueMap.GetNextAssoc(pos,(void *&)olUrue,(void *&)polArray2);
		delete polArray2;
	}
	omUrueMap.RemoveAll();

	omData.DeleteAll();
}

CCSReturnCode CedaRudData::ReadRudData(CDWordArray &ropTplUrnos)
{
	CCSReturnCode ilRc = RCSuccess;
	ClearAll();

	CString olTplUrnos, olTmp;
	int ilNumTplUrnos = ropTplUrnos.GetSize();
	for(int ilTpl = 0; ilTpl < ilNumTplUrnos; ilTpl++)
	{
		if(ilTpl != 0)
			olTplUrnos += ",";
		olTmp.Format("'%d'", ropTplUrnos[ilTpl]);
		olTplUrnos += olTmp;
	}

	///
	if(ogBasicData.IsSinApron1() == true)
	{
		TPLDATA* polTplData;
		for(int i = 0; i< ogTplData.omData.GetSize(); i++)
		{
			polTplData = &ogTplData.omData.GetAt(i);
			if(strcmp(polTplData->Tnam,CCSBasicData::TPL_APRON2) == 0)
			{
				olTmp.Format(",'%d'",polTplData->Urno);
				olTplUrnos += olTmp;
			}
		}
	}
	else if(ogBasicData.IsSinApron2() == true)
	{
		TPLDATA* polTplData;
		for(int i = 0; i< ogTplData.omData.GetSize(); i++)
		{
			polTplData = &ogTplData.omData.GetAt(i);
			if(strcmp(polTplData->Tnam,CCSBasicData::TPL_APRON1) == 0)
			{
				olTmp.Format(",'%d'",polTplData->Urno);
				olTplUrnos += olTmp;
			}
		}
	}
	///

	char pclCom[10] = "RT";
    char pclWhere[5000] = "";
	if ( bmUtplExists )
		sprintf(pclWhere, "WHERE UTPL IN (%s)", olTplUrnos);
	else
		sprintf(pclWhere, "WHERE URUE IN (SELECT URNO FROM RUETAB WHERE UTPL IN (%s))", olTplUrnos);
    if((ilRc = CedaAction2(pclCom, pclWhere)) != RCSuccess)
	{
		ogBasicData.LogCedaError("CedaRudData Error",omLastErrorMessage,pclCom,pcmFieldList,pcmTableName,pclWhere);
	}
	else
	{
		RUDDATA rlRudData;
		for (int ilLc = 0; ilRc == RCSuccess; ilLc++)
		{
			if ((ilRc = GetBufferRecord2(ilLc,&rlRudData)) == RCSuccess)
			{
				AddRudInternal(rlRudData);
			}
		}
		ilRc = RCSuccess;
	}

	ogBasicData.Trace("CedaRudData Read %d Records (Rec Size %d) %s",omData.GetSize(), sizeof(RUDDATA), pclWhere);
    return ilRc;
}


RUDDATA *CedaRudData::AddRudInternal(RUDDATA &rrpRud)
{
	RUDDATA *prlRud = new RUDDATA;
	*prlRud = rrpRud;
	omData.Add(prlRud);
	omUrnoMap.SetAt((void *)prlRud->Urno,prlRud);

	// change DISP value '2' to '002' and '3' to '003' etc so that alphabetically
	// '2' comes before '10'
	CString olDisp;
	olDisp.Format("%03d", atoi(prlRud->Disp));
	strcpy(prlRud->Disp, olDisp);

	CCSPtrArray<RUDDATA> *polArray = NULL;
	if (!omUlnkMap.Lookup((void *)prlRud->Ulnk,(void *&)polArray) || !polArray)
	{
		polArray = new CCSPtrArray<RUDDATA>;
		omUlnkMap.SetAt((void *)prlRud->Ulnk,polArray);
	}
	
	polArray->Add(prlRud);

	CCSPtrArray<RUDDATA> *polArray2 = NULL;
	if (!omUrueMap.Lookup((void *)prlRud->Urue,(void *&)polArray2) || !polArray2)
	{
		polArray2 = new CCSPtrArray<RUDDATA>;
		omUrueMap.SetAt((void *)prlRud->Urue,polArray2);
	}
	
	polArray2->Add(prlRud);

	return prlRud;
}


RUDDATA *CedaRudData::GetRudByUrno(long lpUrno)
{
	RUDDATA *prlRud = NULL;
	omUrnoMap.Lookup((void *)lpUrno,(void *& )prlRud);
	return prlRud;
}

// ipDemandType - { ALLDEMANDS | PERSONNELDEMANDS | EQUIPMENTDEMANDS | LOCATIONDEMANDS }
bool CedaRudData::DemandIsOfType(long lpRudUrno, int ipDemandType)
{
	bool blDemandIsOfType = false;
	RUDDATA *prlRud = GetRudByUrno(lpRudUrno);
	if(prlRud != NULL)
	{
		blDemandIsOfType = DemandIsOfType(prlRud->Rety, ipDemandType);
	}
	return blDemandIsOfType;
}

// ipDemandType - { ALLDEMANDS | PERSONNELDEMANDS | EQUIPMENTDEMANDS | LOCATIONDEMANDS }
bool CedaRudData::DemandIsOfType(const char *pcpRety, int ipDemandType)
{
	bool blDemandIsOfType = false;
	int ilMask = 0x01;

	int ilLen = strlen(pcpRety);
	for(int i = 0; !blDemandIsOfType && i < ilLen; i++)
	{
		if(pcpRety[i] == '1' && ilMask & ipDemandType)
		{
			blDemandIsOfType = true;
		}
		ilMask <<= 1;
	}

	return blDemandIsOfType;
}

// given pcpRety return a bitmap
int CedaRudData::GetDemandType(const char *pcpRety)
{
	int ilDemandType = NODEMANDS;
	int ilMask = 0x01;

	int ilLen = strlen(pcpRety);
	for(int i = 0; i < ilLen; i++)
	{
		if(pcpRety[i] == '1')
		{
			ilDemandType |= ilMask;
		}
		ilMask <<= 1;
	}

	return ilDemandType;
}


long CedaRudData::GetUlnkByUrno(long lpUrno)
{
	long lpUlnk = 0L;
	RUDDATA *prlRud = GetRudByUrno(lpUrno);
	if(prlRud != NULL)
	{
		lpUlnk = prlRud->Ulnk;
	}
	return lpUlnk;
}

CString CedaRudData::GetTableName(void)
{
	return CString(pcmTableName);
}


CString CedaRudData::Dump(long lpUrno)
{
	CString olDumpStr;
	RUDDATA *prlRud = GetRudByUrno(lpUrno);
	if(prlRud == NULL)
	{
		olDumpStr.Format("No RUDDATA Found for RUD.URNO <%ld>",lpUrno);
	}
	else
	{
		CString olData;
		MakeCedaData(&omRecInfo,olData,prlRud);
		CStringArray olFieldArray, olDataArray;
		ogBasicData.ExtractItemList(pcmFieldList, &olFieldArray);
		ogBasicData.ExtractItemList(olData, &olDataArray);
		int ilNumFields = olFieldArray.GetSize();
		int ilNumData = olDataArray.GetSize();
		if(ilNumFields != ilNumData)
		{
			olDumpStr.Format("<%s>\n<%s>\n",pcmFieldList,olData);
		}
		else
		{
			for(int ilD = 0; ilD < ilNumData; ilD++)
			{
				olDumpStr += olFieldArray[ilD] + " = <" + olDataArray[ilD] + ">    ";
			}
		}
	}
	return olDumpStr;
}

int CedaRudData::GetRudsByUlnk(long lpUlnk,const char *pcpDemandType,CCSPtrArray<RUDDATA>& ropRuds)
{
	if (!pcpDemandType)
		return 0;

	CCSPtrArray<RUDDATA> *polArray = NULL;
	if (omUlnkMap.Lookup((void *)lpUlnk,(void *&)polArray) && polArray)
	{
		for (int ilC = 0; ilC < polArray->GetSize(); ilC++)
		{
			if (strcmp((*polArray)[ilC].Rety,pcpDemandType) == 0)			
				ropRuds.Add(&(*polArray)[ilC]);
		}
		return ropRuds.GetSize();
	}
	else
		return 0;
}

// it's possible in rule editor to define RUDs which are linked
// via UPDE (prev dependency) and UNDE (next dependency) in addition REDE 
// indicates which demands require the same employee and thus the emp should 
// be allocated to all demands simultaneously by the auto assigner (jobhdl)
//
// URNO=1 UPDE=0 UNDE=2 REDE=0#1 dependnt URNO 2, same res as 2 
// URNO=2 UPDE=1 UNDE=3 REDE=1#0 dependnt URNO 1+3, same res as 1 
// URNO=3 UPDE=2 UNDE=0 REDE=0#0 dependnt URNO 2, NO same resource 
// DEM1 has URUD=1 
// DEM2 is found because it has URUD=2 and same OURI/OURO as DEM1 
//
bool CedaRudData::GetSameEmpDemandUrnos(long lpUrud, long lpOuri, long lpOuro, CUIntArray &ropDemUrnos, bool bpReset /*=false*/)
{
	bool blSameEmpDemandsFound = false;

	if(bpReset)
	{
		ropDemUrnos.RemoveAll();
	}

	RUDDATA *prlRud = GetRudByUrno(lpUrud), *prlTmpRud = NULL;
	bool blFirstInTheChain = false;
	while(prlRud && !blFirstInTheChain)
	{
		if(prlRud->Upde != 0L && prlRud->Rede[0] == '1' && (prlTmpRud = GetRudByUrno(prlRud->Upde)) != NULL)
		{
			prlRud = prlTmpRud;
		}
		else
		{
			blFirstInTheChain = true;
		}
	}

	DEMANDDATA *prlDemand = NULL;
	bool blEndOfTheChain = false;
	while(prlRud && !blEndOfTheChain)
	{
		if((prlDemand = ogDemandData.GetDemandByFlurAndUrud(lpOuri, lpOuro, prlRud->Urno)) != NULL)
		{
			ropDemUrnos.Add((UINT) prlDemand->Urno);
			blSameEmpDemandsFound = true;
		}

		if(prlRud->Unde != 0L && prlRud->Rede[2] == '1')
			prlRud = GetRudByUrno(prlRud->Unde);
		else
			blEndOfTheChain = true;
	}

	return blSameEmpDemandsFound;
}

bool CedaRudData::LoadRudsForExtraDemands(CCSPtrArray <DEMANDDATA> &ropDemands, CCSPtrArray <RUDDATA> &ropRuds)
{
	CString olUrnosAdded, olUrno;
	int ilNumDems = ropDemands.GetSize();
	if(ilNumDems <= 0)
		return false;

	for(int i = 0; i < ilNumDems; i++)
	{
		DEMANDDATA *prlDemand = &ropDemands[i];
		if(GetRudByUrno(prlDemand->Urud) == NULL)
		{
			olUrno.Format("'%ld'", prlDemand->Urud);
			if(olUrnosAdded.Find(olUrno) == -1)
			{
				if(!olUrnosAdded.IsEmpty())
					olUrnosAdded += ",";
				olUrnosAdded += olUrno;
			}
		}
	}


	if(!olUrnosAdded.IsEmpty())
	{
		char pclCom[10] = "RT";
		char clWhere[2000];
		sprintf(clWhere, "WHERE URNO IN (%s)", olUrnosAdded);
		int ilCount = 0;
		bool ilRc = true;
		if((ilRc = CedaAction2(pclCom, clWhere)) != RCSuccess)
		{
			ogBasicData.LogCedaError("CedaRudData Error",omLastErrorMessage,pclCom,pcmFieldList,pcmTableName,clWhere);
		}
		else
		{
			RUDDATA rlRudData;
			for (int ilLc = 0; ilRc == RCSuccess; ilLc++)
			{
				if ((ilRc = GetBufferRecord2(ilLc,&rlRudData)) == RCSuccess)
				{
					ropRuds.Add(AddRudInternal(rlRudData));
				}
			}
			ilRc = RCSuccess;
		}
	}
	return true;
}

int CedaRudData::GetRudsByUrue(long lpUrue,CCSPtrArray<RUDDATA>& ropRuds)
{
	CCSPtrArray<RUDDATA> *polArray = NULL;
	if (omUrueMap.Lookup((void *)lpUrue,(void *&)polArray) && polArray)
	{
		for (int ilC = 0; ilC < polArray->GetSize(); ilC++)
		{
			ropRuds.Add(&(*polArray)[ilC]);
		}
		return ropRuds.GetSize();
	}
	else
		return 0;
}

