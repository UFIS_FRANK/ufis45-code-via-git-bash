// Class for Ruees
#ifndef _CEDARUEDATA_H_
#define _CEDARUEDATA_H_

#include <CCSCedaData.h>
#include <CCSPtrArray.h>
#include <CedaRudData.h>

/////////////////////////////////////////////////////////////////////////////
// Record structure declaration

struct RueDataStruct
{
	long	Urno;		// Unique Record Number
	char	Runa[65];
	char	Rusn[15];
	long	Utpl;		// Urno to TPLTAB
	char	Evrm[2001];
	char	Rust[3];	// Rule status

	RueDataStruct(void)
	{
		Urno = 0L;
		strcpy(Runa,"");
		strcpy(Rusn,"");
		Utpl = 0L;
		strcpy(Evrm,"");
		strcpy(Rust,"");
	}

	RueDataStruct& RueDataStruct::operator=(const RueDataStruct& rhs)
	{
		if (&rhs != this)
		{
			Urno = rhs.Urno;
			strcpy(Runa,rhs.Runa);
			strcpy(Rusn,rhs.Rusn);
			Utpl = rhs.Utpl;
			strcpy(Evrm,rhs.Evrm);
			strcpy(Rust,rhs.Rust);
		}		
		return *this;
	}
};

typedef struct RueDataStruct RUEDATA;


/////////////////////////////////////////////////////////////////////////////
// Class declaration

class CedaRueData: public CCSCedaData
{

// Attributes
public:
    CCSPtrArray <RUEDATA> omData;
	CMapPtrToPtr omUrnoMap;
	CString GetTableName(void);
	CString Dump(long lpUrno);

// Operations
public:
	CedaRueData();
	~CedaRueData();

	CCSReturnCode ReadRueData(CDWordArray &ropTplUrnos);
	RUEDATA* GetRueByUrno(long lpUrno);
	void ProcessRueBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
	bool LoadRuesForExtraDemands(CCSPtrArray <RUDDATA> &ropRuds);
    bool GetRuesByTemplate(CCSPtrArray <RUEDATA> &ropRues, long lpTplUrno);
    bool GetRuesByTemplate(CCSPtrArray <RUEDATA> &ropRues, long lpTplUrno, bool bpStatus);

private:
	void AddRueInternal(RUEDATA &rrpRue);
	void ClearAll();
};


extern CedaRueData ogRueData;
#endif _CEDARUEDATA_H_
