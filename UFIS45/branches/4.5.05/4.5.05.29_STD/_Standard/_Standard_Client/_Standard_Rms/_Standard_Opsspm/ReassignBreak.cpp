// ReassignBreak.cpp : implementation file
//
// object can reassign breaks for one or more newly inserted jobs:
// the breaks are only reassigned if they iverlap the new job and if they still fit in the Pausenlage
// Example:
//
// CReassignBreak olReassignedBreaks;  // declaration
// olReassignedBreaks.ReassignBreakForNewJob(&rlJob); // attempt to reassign the break job
// // either
//		olReassignedBreaks.Commit(); // saves the reassigned breaks to the DB
// // or
//		olReassignedBreaks.Rollback(); // undo the changes

#include <stdafx.h>
#include <opsspm.h>
#include <ReassignBreak.h>
#include <CedaShiftData.h>
#include <BasicData.h>
#include <DataSet.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

static int ByJobStartTime(const JOBDATA **pppJob1, const JOBDATA **pppJob2);
static int ByJobStartTime(const JOBDATA **pppJob1, const JOBDATA **pppJob2)
{
	return (int)((**pppJob1).Acfr.GetTime() - (**pppJob2).Acfr.GetTime());
}
/////////////////////////////////////////////////////////////////////////////
// CReassignBreak

CReassignBreak::CReassignBreak()
{
	bmBreakReassignActivated = ogBasicData.bmReassignOverlappingBreaks;
}

CReassignBreak::~CReassignBreak()
{
	omReassignedBreaks.DeleteAll();
}

bool CReassignBreak::ReassignBreakForNewJob(JOBDATA *prpNewJob)
{
	bool blBreakReassigned = false;
	if(bmBreakReassignActivated)
	{
		CTime olNewBreakBegin, olNewBreakEnd;
		JOBDATA *prlBreak = CanReassignBreak(prpNewJob, olNewBreakBegin, olNewBreakEnd);
		if(prlBreak != NULL)
		{
			REASSIGNEDBREAK *prlReassignedBreak = new REASSIGNEDBREAK;
			if(prlReassignedBreak != NULL)
			{
				// remember the old break position in case we need to rollback
				prlReassignedBreak->OldBreakBegin = prlBreak->Acfr;
				prlReassignedBreak->OldBreakEnd = prlBreak->Acto;

				prlReassignedBreak->JobUrno = prpNewJob->Urno;

				// set the new break position
				prlBreak->Acfr = olNewBreakBegin;
				prlBreak->Acto = olNewBreakEnd;

				prlReassignedBreak->BreakPtr = prlBreak;

				omReassignedBreaks.Add(prlReassignedBreak);

				blBreakReassigned = true;
			}
		}
	}
	return blBreakReassigned;
}

// update the new break time - can happen if the time for the job is changed in CNewAssignConflictDlg
// i.e. by dragging and dropping the job from one demand to another
void CReassignBreak::UpdateBreakTime(JOBDATA *prpJob)
{
	bool blUpdate = false;
	int ilNumReassignedBreaks = omReassignedBreaks.GetSize();
	for(int ilRB = (ilNumReassignedBreaks-1); ilRB >= 0; ilRB--)
	{
		REASSIGNEDBREAK *prlReassignedBreak = &omReassignedBreaks[ilRB];
		if(prlReassignedBreak->JobUrno == prpJob->Urno && 
			(prlReassignedBreak->BreakPtr->Acfr != prpJob->Acfr || 
				prlReassignedBreak->BreakPtr->Acto != prpJob->Acto))
		{
			Rollback(prlReassignedBreak);
			omReassignedBreaks.DeleteAt(ilRB);
			blUpdate = true;
		}
	}

	if(blUpdate)
	{
		ReassignBreakForNewJob(prpJob);
	}
}

//
// CanReassignBreak()
// attempts to reassign a break within the pausenlage to make way for a newly inserted job
//
// prpNewJob - the newly inserted job that requires the break to be reassigned
// returns a break job if there is one that needs reassigning else NULL
//
JOBDATA *CReassignBreak::CanReassignBreak(JOBDATA *prpNewJob, CTime &ropNewBegin, CTime &ropNewEnd)
{
	JOBDATA *prlReassignedBreak = NULL;
	ropNewBegin = TIMENULL;
	ropNewEnd = TIMENULL;

	SHIFTDATA *prlShift = ogShiftData.GetShiftByUrno(prpNewJob->Shur);
	if(prlShift != NULL && prlShift->Sbfr != TIMENULL && prlShift->Sbto != TIMENULL)
	{
		CTime olCurrTime = ogBasicData.GetLocalTime();
		CTime olBreakBufferStart = (prlShift->Sbfr < olCurrTime) ? olCurrTime : prlShift->Sbfr;
		CTime olBreakBufferEnd = (prlShift->Sbto < olCurrTime) ? olCurrTime : prlShift->Sbto;
		JOBDATA *prlOverlappingBreak = NULL, *prlJob = NULL;
		CTimeSpan  olBrkBuffer(0, 0, ogBasicData.imDefaultBreakBuffer, 0);
		CTime olLatestJobEnd;


		CCSPtrArray <JOBDATA> olJobs;
		ogJobData.GetJobsByShur(olJobs, prpNewJob->Shur, false);
		bool blThisJobNotFound = true;

		int ilNumJobs = olJobs.GetSize();
		for(int ilJ = (ilNumJobs-1); ilJ >= 0; ilJ--)
		{
			prlJob = &olJobs[ilJ];

			if(prlJob->Urno == prpNewJob->Urno)
			{
				blThisJobNotFound = false;
			}

			if(!strcmp(prlJob->Jtco,JOBBREAK) && prlOverlappingBreak == NULL && prlJob->Stat[0] == 'P' )
			{
				//PRF5802: Job must be finished at least olBrkBuffer before break starts
				olLatestJobEnd = prlJob->Acfr - olBrkBuffer;
				if ( IsOverlapped(prpNewJob->Acfr, prpNewJob->Acto, olLatestJobEnd, prlJob->Acto))
				{
					prlOverlappingBreak = prlJob;
					olJobs.RemoveAt(ilJ);
				}
			}
			else if(!strcmp(prlJob->Jtco,JOBPOOL) || !strcmp(prlJob->Jtco,JOBEQUIPMENTFASTLINK) ||
					!strcmp(prlJob->Jtco,JOBEQUIPMENT) ||
					!IsOverlapped(prlJob->Acfr, prlJob->Acto, olBreakBufferStart, olBreakBufferEnd))
			{
				// only check non-pool jobs and those jobs which overlap the Pausenlage
				olJobs.RemoveAt(ilJ);
			}
		}
		if(blThisJobNotFound && IsOverlapped(prpNewJob->Acfr, prpNewJob->Acto, olBreakBufferStart, olBreakBufferEnd))
		{
			olJobs.Add(prpNewJob);
		}
		olJobs.Sort(ByJobStartTime);

		if(prlOverlappingBreak != NULL)
		{
			// a break overlapping the new job has been found so see if it can be moved within the pausenlage
			CTimeSpan olDiff = prlOverlappingBreak->Acfr - olBreakBufferStart;
			CTime olBreakBegin = prlOverlappingBreak->Acfr - olDiff;
			CTime olBreakEnd = prlOverlappingBreak->Acto - olDiff;

			ilNumJobs = olJobs.GetSize();
			for(int ilJ = 0; ilJ < ilNumJobs; ilJ++)
			{
				prlJob = &olJobs[ilJ];
				olLatestJobEnd = prlJob->Acto+olBrkBuffer;
				if(!IsOverlapped(olBreakBegin, olBreakEnd, prlJob->Acfr, olLatestJobEnd))
				{
					// space found for the break - check that it is still in the Pausenlage
					if(olBreakBegin >= olBreakBufferStart && olBreakEnd <= olBreakBufferEnd)
					{
						prlReassignedBreak = prlOverlappingBreak;
						ropNewBegin = olBreakBegin;
						ropNewEnd = olBreakEnd;
					}
				}
				else
				{
					olDiff = olLatestJobEnd - olBreakBegin;
					olBreakBegin += olDiff;
					olBreakEnd += olDiff;

					prlReassignedBreak = NULL;
					ropNewBegin = TIMENULL;
					ropNewEnd = TIMENULL;
				}
			}

			if(prlReassignedBreak == NULL && olBreakBegin >= olBreakBufferStart && olBreakEnd <= olBreakBufferEnd)
			{
				prlReassignedBreak = prlOverlappingBreak;
				ropNewBegin = olBreakBegin;
				ropNewEnd = olBreakEnd;
			}
		}
	}

	return prlReassignedBreak;
}

// rollback reassigned break jobs
void CReassignBreak::Rollback(void)
{
	int ilNumReassignedBreaks = omReassignedBreaks.GetSize();
	for(int ilRB = 0; ilRB < ilNumReassignedBreaks; ilRB++)
	{
		Rollback(&omReassignedBreaks[ilRB]);
	}
	omReassignedBreaks.DeleteAll();
}

void CReassignBreak::Rollback(REASSIGNEDBREAK *prpReassignedBreak)
{
	prpReassignedBreak->BreakPtr->Acfr = prpReassignedBreak->OldBreakBegin;
	prpReassignedBreak->BreakPtr->Acto = prpReassignedBreak->OldBreakEnd;
}

// commit reassigned break jobs
void CReassignBreak::Commit(void)
{
	int ilNumReassignedBreaks = omReassignedBreaks.GetSize();
	for(int ilRB = 0; ilRB < ilNumReassignedBreaks; ilRB++)
	{
		REASSIGNEDBREAK *prlReassignedBreak = &omReassignedBreaks[ilRB];

		JOBDATA *prlNewBreak = prlReassignedBreak->BreakPtr;

		if(prlNewBreak->Acfr != prlReassignedBreak->OldBreakBegin || 
			prlNewBreak->Acto != prlReassignedBreak->OldBreakEnd)
		{
			JOBDATA rlOldBreak = *prlNewBreak;

			rlOldBreak.Acfr = prlReassignedBreak->OldBreakBegin;
			rlOldBreak.Acto = prlReassignedBreak->OldBreakEnd;

			ogDataSet.ChangeTimeOfJob(prlNewBreak, &rlOldBreak);
		}
	}
}

