// EquipmentViewer.cpp : implementation file
//  

#include <stdafx.h>
#include <CCSGlobl.h>
#include <OpssPm.h>
#include <CCSCedaData.h>
#include <CCSCedaCom.h>
#include <CedaJobData.h>
#include <CedaDemandData.h>
#include <CedaJodData.h>
#include <gantt.h>
#include <gbar.h>
#include <CedaFlightData.h>
#include <CCSPtrArray.h>
#include <AllocData.h>
#include <cviewer.h>
#include <ccsddx.h>
#include <conflict.h>
#include <EquipmentViewer.h>
#include <conflict.h>
#include <ConflictConfigTable.h>
#include <BasicData.h>
#include <DataSet.h>
#include <CedaEqtData.h>
#include <CedaJtyData.h>
#include <CedaValData.h>
#include <CedaEqaData.h>

#ifdef _DEBUG
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// General macros for testing a point against an interval
#define IsBetween(val, start, end)  ((start) <= (val) && (val) <= (end))
#define IsOverlapped(start1, end1, start2, end2)    ((start1) <= (end2) && (start2) <= (end1))
#define IsReallyOverlapped(start1, end1, start2, end2)	((start1) < (end2) && (start2) < (end1))
#define IsWithIn(start1, end1, start2, end2)	((start1) >= (start2) && (end1) <= (end2))


static int ByTime(const EQUIPMENT_BKBARDATA **pppBkBar1, const EQUIPMENT_BKBARDATA **pppBkBar2);
static int ByTime(const EQUIPMENT_BKBARDATA **pppBkBar1, const EQUIPMENT_BKBARDATA **pppBkBar2)
{
	return (int)((**pppBkBar1).StartTime.GetTime() - (**pppBkBar2).StartTime.GetTime());
}


/////////////////////////////////////////////////////////////////////////////
// EquipmentDiagramViewer
//
EquipmentDiagramViewer::EquipmentDiagramViewer()
{
	pomAttachWnd = NULL;
	omStartTime = TIMENULL;
	omEndTime = TIMENULL;

	bmUseAllEquipmentTypes = false;
	bmUseAllEquipmentGroups = false;
	bmUseAllEquipmentNames = false;

	ogCCSDdx.Register(this, EQU_INSERT,CString("EQUIPMENTVIEWER"), CString("Equ Insert"), EquipmentDiagramViewerCf);
	ogCCSDdx.Register(this, EQU_UPDATE,CString("EQUIPMENTVIEWER"), CString("Equ Update"), EquipmentDiagramViewerCf);
	ogCCSDdx.Register(this, EQU_DELETE,CString("EQUIPMENTVIEWER"), CString("Equ Delete"), EquipmentDiagramViewerCf);
	ogCCSDdx.Register(this, JOB_NEW,CString("EQUIPMENTVIEWER"), CString("Job New"), EquipmentDiagramViewerCf);
	ogCCSDdx.Register(this, JOB_CHANGE,CString("EQUIPMENTVIEWER"), CString("Job Changed"), EquipmentDiagramViewerCf);
	ogCCSDdx.Register(this, JOB_DELETE,CString("EQUIPMENTVIEWER"), CString("Job Delete"), EquipmentDiagramViewerCf);
	ogCCSDdx.Register(this, REDISPLAY_ALL,CString("EQUIPMENTVIEWER"), CString("Update Conflict Setup"), EquipmentDiagramViewerCf);
	ogCCSDdx.Register(this, UPDATE_CONFLICT_SETUP,CString("EQUIPMENTVIEWER"), CString("Refresh"), EquipmentDiagramViewerCf);
	ogCCSDdx.Register(this, EQUIPMENT_SELECT,CString("EQUIPMENTVIEWER"),CString("Equipment Select"), EquipmentDiagramViewerCf);
	ogCCSDdx.Register(this, BLK_CHANGE,CString("EQUIPMENTVIEWER"), CString("Blk Changed"), EquipmentDiagramViewerCf);
	ogCCSDdx.Register(this, BLK_DELETE,CString("EQUIPMENTVIEWER"), CString("Blk Deleted"), EquipmentDiagramViewerCf);
	ogCCSDdx.Register(this, VAL_INSERT,CString("EQUIPMENTVIEWER"), CString("Val Insert"), EquipmentDiagramViewerCf);
	ogCCSDdx.Register(this, VAL_UPDATE,CString("EQUIPMENTVIEWER"), CString("Val Update"), EquipmentDiagramViewerCf);
	ogCCSDdx.Register(this, VAL_DELETE,CString("EQUIPMENTVIEWER"), CString("Val Delete"), EquipmentDiagramViewerCf);

	CBitmap olBitmap;
	olBitmap.LoadBitmap(IDB_NOTAVAIL);
	omBlockedBrush.CreatePatternBrush(&olBitmap);
	omFastLinkBrush.CreateSolidBrush(OLIVE);
	omBkTextBrush.CreateSolidBrush(WHITE);

	bmNotDisplayUnavailable = false;
}

EquipmentDiagramViewer::~EquipmentDiagramViewer()
{
	ogCCSDdx.UnRegister(this, NOTUSED);
	DeleteAll();
}

void EquipmentDiagramViewer::Attach(CWnd *popAttachWnd)
{
	pomAttachWnd = popAttachWnd;
}


void EquipmentDiagramViewer::EquipmentDiagramViewerCf(void *popInstance, int ipDDXType,void *vpDataPointer, CString &ropInstanceName)
{
	EquipmentDiagramViewer *polViewer = (EquipmentDiagramViewer *)popInstance;

	if (polViewer->bmNoUpdatesNow == FALSE)
	{
		switch(ipDDXType)
		{
		case EQU_INSERT:
			polViewer->ProcessEquInsert((EQUDATA *) vpDataPointer);
			break;
		case EQU_UPDATE:
			polViewer->ProcessEquUpdate((EQUDATA *) vpDataPointer);
			break;
		case EQU_DELETE:
			polViewer->ProcessEquDelete((long) vpDataPointer);
			break;
		case JOB_NEW:
			polViewer->ProcessJobChange((JOBDATA *) vpDataPointer);
			break;
		case JOB_CHANGE:
			polViewer->ProcessJobChange((JOBDATA *) vpDataPointer);
			break;
		case JOB_DELETE:
			polViewer->ProcessJobDelete((JOBDATA *) vpDataPointer);
			break;
		case EQUIPMENT_SELECT:
			polViewer->ProcessEquSelect((EQUDATA *) vpDataPointer);
			break;
		case REDISPLAY_ALL:
		case UPDATE_CONFLICT_SETUP:
			polViewer->DeleteAll();
			polViewer->ChangeViewTo();
			polViewer->pomAttachWnd->Invalidate();
			break;
		case BLK_CHANGE:
		case BLK_DELETE:
			polViewer->ProcessBlkChange((BLKDATA *) vpDataPointer);
			break;
		case VAL_INSERT:
		case VAL_UPDATE:
		case VAL_DELETE:
			polViewer->ProcessValChange((VALDATA *) vpDataPointer);
			break;
		default:
			break;
		}
	}
	else
	{
		// should never reach this point
	//	ASSERT( 0 );
	}
}

void EquipmentDiagramViewer::ProcessEquSelect(EQUDATA *prpEqu)
{
	if(::IsWindow(pomAttachWnd->GetSafeHwnd()) && prpEqu != NULL)
	{
		EQUIPMENT_SELECTION rolSelection;
		rolSelection.imGroupno = rolSelection.imLineno = -1;
		if(FindGroupAndLine(prpEqu->Urno,rolSelection.imGroupno,rolSelection.imLineno))
		{
			LONG lParam = reinterpret_cast<LONG>(&rolSelection);
			pomAttachWnd->SendMessage(WM_SELECTDIAGRAM,UD_SELECTLINE, lParam);
		}
		else
		{
			pomAttachWnd->MessageBox(GetString(IDS_EV_NOTFOUND),GetString(IDS_EV_NOTFOUNDTITLE),MB_ICONEXCLAMATION );
		}
	}
}

void EquipmentDiagramViewer::ProcessValChange(VALDATA *prpVal)
{
	if(prpVal != NULL)
	{
		UpdateEquLine(prpVal->Uval);
	}
}

void EquipmentDiagramViewer::ProcessBlkChange(BLKDATA *prpBlk)
{
	if(prpBlk != NULL)
	{
		UpdateEquLine(prpBlk->Burn);
	}
}

void EquipmentDiagramViewer::UpdateEquLine(long lpEquUrno)
{
	UpdateEquLine(ogEquData.GetEquByUrno(lpEquUrno));
}

void EquipmentDiagramViewer::UpdateEquLine(EQUDATA *prpEqu)
{
	if(prpEqu != NULL)
	{
		DeleteEquLine(prpEqu->Urno);
		InsertEquLine(prpEqu);
	}
}

void EquipmentDiagramViewer::DeleteEquLine(long lpEquUrno)
{
	int ilGroupno = -1, ilLineno = -1;
	while(FindGroupAndLine(lpEquUrno,ilGroupno,ilLineno))
	{
		DeleteLine(ilGroupno, ilLineno);
		if (::IsWindow(pomAttachWnd->GetSafeHwnd()))
		{
			LONG lParam = MAKELONG(ilLineno, ilGroupno);
			pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM, UD_DELETELINE, lParam);
		}
		ilGroupno = -1;
		ilLineno = -1;
	}
}

void EquipmentDiagramViewer::InsertEquLine(EQUDATA *prpEqu)
{
	if(prpEqu != NULL && IsPassFilter(prpEqu->Urno,prpEqu->Enam))
	{
		int ilGroupno = -1, ilLineno = -1;
		while(FindGroup(prpEqu->Urno, ilGroupno))
		{
			if((ilLineno = MakeLine(ilGroupno, prpEqu)) != -1)
			{
				MakeBkBars(ilGroupno, ilLineno);
				MakeBars(ilGroupno, ilLineno, prpEqu->Urno);
				if (::IsWindow(pomAttachWnd->GetSafeHwnd()))
				{
					LONG lParam = MAKELONG(ilLineno, ilGroupno);
					pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM, UD_INSERTLINE, lParam);
				}
			}
		}
	}
}

void EquipmentDiagramViewer::ProcessEquInsert(EQUDATA *prpEqu)
{
	InsertEquLine(prpEqu);
}

void EquipmentDiagramViewer::ProcessEquUpdate(EQUDATA *prpEqu)
{
	UpdateEquLine(prpEqu);
}

void EquipmentDiagramViewer::ProcessEquDelete(long lpEquUrno)
{
	DeleteEquLine(lpEquUrno);
}

void EquipmentDiagramViewer::ProcessJobChange(JOBDATA *prpJob)
{
	if(prpJob != NULL)
	{
		if(!strcmp(prpJob->Jtco,JOBEQUIPMENTFASTLINK))
		{
			UpdateEquLine(prpJob->Uequ);
		}
		else
		{
			ProcessJobDelete(prpJob);

			int ilGroupno, ilLineno;
			ilGroupno = -1;
			ilLineno = -1;
			while(FindGroupAndLine(prpJob->Uequ,ilGroupno,ilLineno))
			{
				int ilOldMaxOverlapLevel = GetVisualMaxOverlapLevel(ilGroupno, ilLineno);
				MakeBar(ilGroupno, ilLineno, prpJob);
				if (::IsWindow(pomAttachWnd->GetSafeHwnd()))
				{
					LONG lParam = MAKELONG(ilLineno, ilGroupno);
					if (GetVisualMaxOverlapLevel(ilGroupno, ilLineno) == ilOldMaxOverlapLevel)
						pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM, UD_UPDATELINE, lParam);
					else
						pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM, UD_UPDATELINEHEIGHT, lParam);
				}
			}
		}
	}
}

void EquipmentDiagramViewer::ProcessJobDelete(JOBDATA *prpJob)
{
	if(prpJob != NULL)
	{
		if(!strcmp(prpJob->Jtco,JOBEQUIPMENTFASTLINK))
		{
			UpdateEquLine(prpJob->Uequ);
		}
		else
		{
			int ilGroupno, ilLineno, ilBarno;

			while(FindBar(prpJob->Urno, ilGroupno, ilLineno, ilBarno))
			{
				int ilOldMaxOverlapLevel = GetVisualMaxOverlapLevel(ilGroupno, ilLineno);
				DeleteBar(ilGroupno, ilLineno, ilBarno);
				if (::IsWindow(pomAttachWnd->GetSafeHwnd()))
				{
					LONG lParam = MAKELONG(ilLineno, ilGroupno);
					if (GetVisualMaxOverlapLevel(ilGroupno, ilLineno) == ilOldMaxOverlapLevel)
						pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM,	UD_UPDATELINE, lParam);
					else
						pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM,	UD_UPDATELINEHEIGHT, lParam);
				}
				ilGroupno = -1;
				ilLineno = -1;
			}
		}
	}
}

void EquipmentDiagramViewer::ChangeViewTo(const char *pcpViewName, CTime opStartTime, CTime opEndTime)
{
	DeleteAll();	// remove everything

	ogCfgData.rmUserSetup.EQCV = pcpViewName;
	SelectView(pcpViewName);
	PrepareGrouping();
	PrepareFilter();
	omStartTime = opStartTime;
	omEndTime = opEndTime;
	ChangeViewTo();
}

void EquipmentDiagramViewer::ChangeViewTo(void)
{
	MakeGroupsAndLines();
	MakeBackgroundBars();
	MakeBars();
}

CString EquipmentDiagramViewer::GetGroup()
{
	return CViewer::GetGroup();
}

void EquipmentDiagramViewer::PrepareGrouping()
{
	omGroupBy = CViewer::GetGroup();
	CString olNotDisplayUnavailable = GetUserData("NOUNAVAILABLE");
	if (olNotDisplayUnavailable.IsEmpty())
		bmNotDisplayUnavailable = false;
	else
		bmNotDisplayUnavailable = olNotDisplayUnavailable == "YES";
}

void EquipmentDiagramViewer::PrepareFilter()
{
	CStringArray olFilterValues;
	int i, ilNumFilterValues;

	bmUseAllEquipmentGroups = false;
	omEquipmentGroupMap.RemoveAll();
	GetFilter("EquipmentGroup", olFilterValues);
	ilNumFilterValues = olFilterValues.GetSize();
	if (ilNumFilterValues > 0)
	{
		if (olFilterValues[0] == GetString(IDS_ALLSTRING))
		{
			bmUseAllEquipmentGroups = true;
		}
		else
		{
			for (i = 0; i < ilNumFilterValues; i++)
			{
				omEquipmentGroupMap[olFilterValues[i]] = NULL;
			}
		}
	}

	bmUseAllEquipmentTypes = false;
	omEquipmentTypeMap.RemoveAll();
	GetFilter("EquipmentType", olFilterValues);
	ilNumFilterValues = olFilterValues.GetSize();
	if (ilNumFilterValues > 0)
	{
		if (olFilterValues[0] == GetString(IDS_ALLSTRING))
		{
			bmUseAllEquipmentTypes = true;
		}
		else
		{
			for (i = 0; i < ilNumFilterValues; i++)
			{
				omEquipmentTypeMap[olFilterValues[i]] = NULL;
			}
		}
	}

	bmUseAllEquipmentNames = false;
	omEquipmentNamesMap.RemoveAll();
	GetFilter("EquipmentName", olFilterValues);
	ilNumFilterValues = olFilterValues.GetSize();
	if (ilNumFilterValues > 0)
	{
		if (olFilterValues[0] == GetString(IDS_ALLSTRING))
		{
			bmUseAllEquipmentNames = true;
		}
		else
		{
			for (i = 0; i < ilNumFilterValues; i++)
			{
				omEquipmentNamesMap[olFilterValues[i]] = NULL;
			}
		}
	}
}

BOOL EquipmentDiagramViewer::IsPassFilter(const char *pcpEquipmentArea)
{
	void *p;
	BOOL blIsGroupOk = FALSE;

	if(omGroupBy == "EquipmentGroup")
	{
		blIsGroupOk = bmUseAllEquipmentGroups || omEquipmentGroupMap.Lookup(CString(pcpEquipmentArea), p);
	}
	else if(omGroupBy == "EquipmentType")
	{
		blIsGroupOk = bmUseAllEquipmentTypes || omEquipmentTypeMap.Lookup(CString(pcpEquipmentArea), p);
	}

	return blIsGroupOk;
}

BOOL EquipmentDiagramViewer::IsPassFilter(long lpEquUrno, const CString& ropEquName)
{
	bool blEquOk = true;

	if(bmNotDisplayUnavailable &&
		(ogBlkData.IsCompletelyBlocked("EQU", lpEquUrno, ogBasicData.GetTimeframeStart(),ogBasicData.GetTimeframeEnd()) ||
		 ogValData.IsCompletelyBlocked(lpEquUrno, ogBasicData.GetTimeframeStart(),ogBasicData.GetTimeframeEnd())))
	{
		blEquOk = false;
	}
	
	if(!ropEquName.IsEmpty())
	{
		void* p;
		blEquOk = bmUseAllEquipmentNames || omEquipmentNamesMap.Lookup(ropEquName,p);
	}

	return blEquOk;
}

int EquipmentDiagramViewer::CompareGroup(EQUIPMENT_GROUPDATA *prpGroup1, EQUIPMENT_GROUPDATA *prpGroup2)
{
	CString &s1 = prpGroup1->Text;
	CString &s2 = prpGroup2->Text;
	return (s1 == s2)? 0: (s1 > s2)? 1: -1;
}

int EquipmentDiagramViewer::CompareLine(EQUIPMENT_LINEDATA *prpLine1, EQUIPMENT_LINEDATA *prpLine2)
{
	CString &s1 = prpLine1->Text;
	CString &s2 = prpLine2->Text;
	return (s1 == s2)? 0: (s1 > s2)? 1: -1;
}

void EquipmentDiagramViewer::MakeGroupsAndLines()
{
	CString olGroup = CViewer::GetGroup(); // either "EquipGroup" or "EquipType"
	CStringArray olGroupNames;
//	GetFilter(olGroup, olGroupNames);
	if(olGroup == "EquipmentType") // group by equipment type
	{
		ogEqtData.GetEquipmentTypeNames(olGroupNames);
	}
	else if(olGroup == "EquipmentGroup") // group by static groups
	{
		ogAllocData.GetGroupNamesByGroupType(ALLOCUNITTYPE_EQUIPMENTGROUP,olGroupNames);
	}

	// Create a group for each value found in the corresponding filter
	for (int i = 0; i < olGroupNames.GetSize(); i++)
	{
		CString olGroupName = olGroupNames[i];
		if(IsPassFilter(olGroupName))
		{
			EQUIPMENT_GROUPDATA rlGroup;
			rlGroup.EquipmentAreaId = olGroupName;
			rlGroup.Text = olGroupName;
			int ilGroupno = CreateGroup(&rlGroup);

			if(olGroup == "EquipmentType") // group by equipment type
			{
				EQTDATA *prlEqt = ogEqtData.GetEqtByName(olGroupName);
				if(prlEqt != NULL)
				{
					CCSPtrArray <EQUDATA> olEquList;
					ogEquData.GetEquipmentByType(prlEqt->Urno, olEquList);
					int ilNumEqu = olEquList.GetSize();
					for(int ilE = 0; ilE < ilNumEqu; ilE++)
					{
						EQUDATA *prlEqu = &olEquList[ilE];
						if(IsPassFilter(prlEqu->Urno,prlEqu->Enam)) // check if equipment is not completely blocked
						{
							MakeLine(ilGroupno, prlEqu);
						}
					}
				}
			}
			else if(olGroup == "EquipmentGroup") // group by static groups
			{
				CCSPtrArray <ALLOCUNIT> olEquipment;
				ogAllocData.GetUnitsByGroup(ALLOCUNITTYPE_EQUIPMENTGROUP,olGroupName,olEquipment);
				int ilNumEquipments = olEquipment.GetSize();
				for(int ilEquipment = 0; ilEquipment < ilNumEquipments; ilEquipment++)
				{
					EQUDATA *prlEqu = ogEquData.GetEquByUrno(olEquipment[ilEquipment].Urno);
					if(prlEqu != NULL && IsPassFilter(prlEqu->Urno,prlEqu->Enam)) // check if equipment is not completely blocked
					{
						MakeLine(ilGroupno, prlEqu);
					}
				}
			}
		}
	}
}

int EquipmentDiagramViewer::MakeLine(int ipGroupno, EQUDATA *prpEqu)
{
	int ilLineno = -1;
	if(prpEqu != NULL)
	{
		EQUIPMENT_LINEDATA rlLine;
		rlLine.Text = prpEqu->Enam;

		if(IsPrivateProfileOn("ONE_GLANCE_DEMO","NO"))
		{
			CCSPtrArray<EQADATA> olEqaList;
			ogEqaData.GetEqasByUequ(prpEqu->Urno, olEqaList);

			int ilNumLines = olEqaList.GetSize();
			CString olAttrName;
			for (int ilLc = 0; ilLc < ilNumLines; ilLc++)
			{
				EQADATA *prlEqa = new EQADATA;
				if(prlEqa != NULL)
				{
					
					*prlEqa = olEqaList[ilLc];
					olAttrName = prlEqa->Name;

					if(olAttrName == "Bay")
					{
						rlLine.Text  = rlLine.Text + "     " + prlEqa->Valu;
					}
				}
			}
		}
		rlLine.EquUrno = prpEqu->Urno;
		rlLine.StatusText.Format("%s %s %s %s %s %s",prpEqu->Enam, prpEqu->Gcde, prpEqu->Etyp, prpEqu->Eqps, prpEqu->Ivnr, prpEqu->Crqu);
		ilLineno = CreateLine(ipGroupno, &rlLine);
	}

	return ilLineno;
}

void EquipmentDiagramViewer::MakeBars()
{
	int ilGroupCount = GetGroupCount();
	for(int ilGroupno = 0; ilGroupno < ilGroupCount; ilGroupno++)
	{
		EQUIPMENT_GROUPDATA *prlGroup = GetGroup(ilGroupno);
		int ilLineCount = GetLineCount(ilGroupno);
		for(int ilLineno = 0; ilLineno < ilLineCount; ilLineno++)
		{
			MakeBars(ilGroupno, ilLineno, GetLine(ilGroupno, ilLineno)->EquUrno);
		}
	}
}

void EquipmentDiagramViewer::MakeBars(int ipGroupno, int ipLineno, long lpEquUrno)
{
	CCSPtrArray<JOBDATA> olJobs;
	ogJobData.GetJobsByUequ(olJobs, lpEquUrno);

	int ilNumJobs = olJobs.GetSize();
	for(int ilJob = 0; ilJob < ilNumJobs; ilJob++)
	{
		JOBDATA *prlJob = &olJobs[ilJob];
		if(strcmp(prlJob->Jtco,JOBEQUIPMENTFASTLINK))
		{
			MakeBar(ipGroupno, ipLineno, prlJob);
		}
	}
}

void EquipmentDiagramViewer::MakeBar(int ipGroupno, int ipLineno, JOBDATA *prpJob)
{
	EQUIPMENT_BARDATA rlBar;

	int ilColorIndex = ogConflicts.GetJobConflictColor(prpJob);

	rlBar.Text = ogDataSet.GetFlightJobBarText(prpJob);
	FLIGHTDATA *prlFlight = ogFlightData.GetFlightByUrno(prpJob->Flur);
	if(prlFlight != NULL)
		rlBar.StatusText = FlightStatus(prlFlight);
	else
		rlBar.StatusText.Format("%s  %s-%s",ogDataSet.JobBarText(prpJob),prpJob->Acfr.Format("%H%M/%d"),prpJob->Acto.Format("%H%M/%d"));
	rlBar.StartTime = prpJob->Acfr;
	rlBar.EndTime = prpJob->Acto;
	rlBar.FrameType = FRAMERECT;
	rlBar.MarkerType = MARKFULL;
	rlBar.MarkerBrush = ogBrushs[ilColorIndex];
	rlBar.JobUrno = prpJob->Urno;
	rlBar.OfflineBar = !bgOnline && (ogJobData.JobChangedOffline(prpJob) || ogJodData.JodChangedOffline(prpJob->Urno));
	rlBar.DifferentFunction = ogBasicData.JobAndDemHaveDiffFuncs(prpJob);
	rlBar.FlightUrno = prpJob->Flur;
	switch(*prpJob->Dety)
	{
		case TURNAROUND_DEMAND:
			rlBar.FlightType = EQUIPMENTBAR_TURNAROUND;
			break;
		case INBOUND_DEMAND:
			rlBar.FlightType = EQUIPMENTBAR_ARRIVAL;
			break;
		case OUTBOUND_DEMAND:
			rlBar.FlightType = EQUIPMENTBAR_DEPARTURE;
			break;
		default:
			break;
	}


	int ilBarno = CreateBar(ipGroupno, ipLineno, &rlBar);

	EQUIPMENT_INDICATORDATA rlIndicator;
	rlIndicator.StartTime = rlBar.StartTime;
	rlIndicator.EndTime = rlBar.EndTime;
	rlIndicator.Color = ogColors[ilColorIndex];
	CreateIndicator(ipGroupno, ipLineno, ilBarno, &rlIndicator);

	return;
}

void EquipmentDiagramViewer::MakeBackgroundBars()
{
	CString olUdemAlidList, olTmp;

	int ilNumGroups = omGroups.GetSize();
	for(int ilGroup = 0; ilGroup < ilNumGroups; ilGroup++)
	{
		int ilNumLines = omGroups[ilGroup].Lines.GetSize();
		for(int ilLine = 0; ilLine < ilNumLines; ilLine++)
		{
			MakeBkBars(ilGroup, ilLine);
		}
	}
}

void EquipmentDiagramViewer::MakeBkBars(int ipGroupno, int ipLineno)
{
	EQUIPMENT_LINEDATA *prlLine = &omGroups[ipGroupno].Lines[ipLineno];
	MakeBlkBkBars(prlLine);
	MakeFastLinkBkBars(prlLine);
}

void EquipmentDiagramViewer::MakeBlkBkBars(EQUIPMENT_LINEDATA *prpLine)
{
	if(prpLine != NULL)
	{
		CCSPtrArray <BLKDATA> olBlocked;
		if(ogBlkData.GetBlocked("EQU", prpLine->EquUrno, olBlocked) > 0)
		{
			int ilNumBlocked = olBlocked.GetSize();
			for (int ilB = 0; ilB < ilNumBlocked; ilB ++)
			{
				BLKDATA *prlBlk = &olBlocked[ilB];

				CCSPtrArray<BLKBLOCKEDTIMES> olBlockedTimes;
				if(ogBlkData.GetBlockedTimes(prlBlk, ogBasicData.GetTimeframeStart(), ogBasicData.GetTimeframeEnd(), olBlockedTimes) > 0)
				{
					for(int ilC = 0; ilC < olBlockedTimes.GetSize(); ilC++)
					{
						MakeBlkBkBar(prpLine,olBlockedTimes[ilC].Tifr,olBlockedTimes[ilC].Tito,prlBlk->Resn, prlBlk->Urno);
					}
					olBlockedTimes.DeleteAll();
				}
			}
		}

		CCSPtrArray<BLKBLOCKEDTIMES> olBlockedTimes;
		if(ogValData.GetBlockedTimes(prpLine->EquUrno, omStartTime, omEndTime, olBlockedTimes) > 0)
		{
			for(int ilC = 0; ilC < olBlockedTimes.GetSize(); ilC++)
			{
				MakeBlkBkBar(prpLine,olBlockedTimes[ilC].Tifr, olBlockedTimes[ilC].Tito, GetString(IDS_NOTVALID), prpLine->EquUrno);
			}
			olBlockedTimes.DeleteAll();
		}

		
		prpLine->BkBars.Sort(ByTime);
	}
}

EQUIPMENT_BKBARDATA* EquipmentDiagramViewer::MakeBlkBkBar(EQUIPMENT_LINEDATA *prpLine,const CTime& ropFrom,const CTime& ropTo,const CString& ropText, long lpBlkUrno)
{
	EQUIPMENT_BKBARDATA *prlBkBar = new EQUIPMENT_BKBARDATA;
	prlBkBar->Type = EQUIPMENT_BKBARDATA::BLOCKED;
	prlBkBar->Text = (ropFrom < ropTo) ? ropText : "Error From > To in BLKTAB";
	prlBkBar->StatusText.Format("%s - %s  %s",ropFrom.Format("%d/%H%M"),ropTo.Format("%d/%H%M"),ropText);
	prlBkBar->StartTime = ropFrom;
	prlBkBar->EndTime	= ropTo;
	prlBkBar->MarkerBrush = &omBlockedBrush;
	prlBkBar->FrameType = FRAMERECT;
	prlBkBar->JobUrno = lpBlkUrno;
	prpLine->BkBars.Add(prlBkBar);

	return prlBkBar;
}

void EquipmentDiagramViewer::MakeFastLinkBkBars(EQUIPMENT_LINEDATA *prpLine)
{
	if(prpLine != NULL)
	{
		CCSPtrArray <JOBDATA> olFastLinkJobs;
		ogJobData.GetJobsByUequ(olFastLinkJobs, prpLine->EquUrno);
		JOBDATA *prlJob, *prlPrevJob = NULL;
		CCSPtrArray <JOBDATA> olOverlappingJobs;
		int ilNumJobs = olFastLinkJobs.GetSize();
		for(int ilFLJ = 0; ilFLJ < ilNumJobs; ilFLJ++)
		{
			prlJob = &olFastLinkJobs[ilFLJ];
			if(!strcmp(prlJob->Jtco, JOBEQUIPMENTFASTLINK))
			{
				if(prlPrevJob == NULL || IsOverlapped(prlJob->Acfr, prlJob->Acto, prlPrevJob->Acfr, prlPrevJob->Acto))
				{
					olOverlappingJobs.Add(prlJob);
					prlPrevJob = prlJob;
				}
				else
				{
					MakeFastLinkBkBar(prpLine, olOverlappingJobs);
					prlPrevJob = NULL;
					olOverlappingJobs.RemoveAll();

					olOverlappingJobs.Add(prlJob);
					prlPrevJob = prlJob;
				}
			}
		}
		if(olOverlappingJobs.GetSize() > 0)
		{
			MakeFastLinkBkBar(prpLine, olOverlappingJobs);
		}
	}
}

EQUIPMENT_BKBARDATA* EquipmentDiagramViewer::MakeFastLinkBkBar(EQUIPMENT_LINEDATA *prpLine, CCSPtrArray <JOBDATA> &ropOverlappingJobs)
{
	EQUIPMENT_BKBARDATA *prlBkBar = NULL;
	int ilNumOverlappingJobs = ropOverlappingJobs.GetSize();

	if(prpLine != NULL && ilNumOverlappingJobs > 0)
	{
		prlBkBar = new EQUIPMENT_BKBARDATA;
		prlBkBar->Type = EQUIPMENT_BKBARDATA::FASTLINK;
		prlBkBar->TextColor = WHITE;
		prlBkBar->MarkerBrush = &omFastLinkBrush;
		prlBkBar->FrameType = FRAMERECT;
		prlBkBar->JobUrno = 0L;

		CString olText, olStatusText;
		prlBkBar->StartTime = TIMENULL;
		prlBkBar->EndTime = TIMENULL;

		for(int ilJ = 0; ilJ < ilNumOverlappingJobs; ilJ++)
		{
			JOBDATA *prlJob = &ropOverlappingJobs[ilJ];

			if(!bgOnline && (ogJobData.JobChangedOffline(prlJob) || ogJodData.JodChangedOffline(prlJob->Urno)))
			{
				prlBkBar->OfflineBar = true;
			}

			EMPDATA *prlEmp = ogEmpData.GetEmpByUrno(prlJob->Ustf);
			if(prlEmp != NULL)
			{
				if(ilNumOverlappingJobs == 1)
				{
					prlBkBar->JobUrno = prlJob->Urno;
					olText = ogEmpData.GetEmpName(prlEmp);
					olStatusText.Format("%s ", olText);
					prlBkBar->JobUrnos.Add(prlJob->Urno);
				}
				else
				{
					prlBkBar->JobUrnos.Add(prlJob->Urno);

					CString olTmpText = ogEmpData.GetEmpName(prlEmp);
					if(!olText.IsEmpty())
					{
						olText += CString(" / ");
					}
					olText += olTmpText;

					if(!olStatusText.IsEmpty())
					{
						olStatusText += CString(" / ");
					}
					olTmpText.Format("%s %s-%s ",ogEmpData.GetEmpName(prlEmp), prlJob->Acfr.Format("%H%M"),prlJob->Acto.Format("%H%M"));
					olStatusText += olTmpText;
				}
				if(prlBkBar->StartTime == TIMENULL || prlBkBar->StartTime > prlJob->Acfr)
				{
					prlBkBar->StartTime = prlJob->Acfr;
				}
				if(prlBkBar->EndTime == TIMENULL || prlBkBar->EndTime < prlJob->Acto)
				{
					prlBkBar->EndTime = prlJob->Acto;
				}
			}
		}

		prlBkBar->Text = olText;
		prlBkBar->StatusText.Format("%s-%s %s:  %s",prlBkBar->StartTime.Format("%H%M"),prlBkBar->EndTime.Format("%H%M"),GetString(IDS_EQUIPMENT_FASTLINK),olStatusText);
		prpLine->BkBars.Add(prlBkBar);
	}

	return prlBkBar;
}

char EquipmentDiagramViewer::GetInitial(const char *pcpName)
{
	char clInitial = ' ';
	if(strlen(pcpName) > 0)
	{
		clInitial = toupper(pcpName[0]);
	}
	return clInitial;
}

CString EquipmentDiagramViewer::FlightStatus(FLIGHTDATA *prpFlight)
{
	CString olFlightStatus;
	if(prpFlight != NULL)
	{
		FLIGHTDATA *prlArrFlight, *prlDepFlight;
		if(ogFlightData.IsArrival(prpFlight))
		{
			prlArrFlight = prpFlight;
			prlDepFlight = ogFlightData.GetRotationFlight(prpFlight);
		}
		else
		{
			prlDepFlight = prpFlight;
			prlArrFlight = ogFlightData.GetRotationFlight(prpFlight);
		}

		int ilCnt = 0;
		CString olArrText;
		if(prlArrFlight != NULL)
		{
			CString olEtoa = prlArrFlight->Etoa != TIMENULL ? CString(" ETA:") + prlArrFlight->Etoa.Format("%H%M") : "";
			CString olOnbl = prlArrFlight->Onbl != TIMENULL ? CString(" ONBL:") + prlArrFlight->Onbl.Format("%H%M") : "";
			olArrText.Format("%s %s %s STA:%s%s%s P:%s G:%s PAX:%d",prlArrFlight->Regn, prlArrFlight->Fnum,
						  prlArrFlight->Org3,prlArrFlight->Stoa.Format("%H%M"),olEtoa,olOnbl,
						  ogFlightData.GetPosition(prlArrFlight), ogFlightData.GetGate(prlArrFlight), prlArrFlight->Pax1);
			ilCnt++;
		}
	
		CString olDepText;
		if(prlDepFlight != NULL)
		{
			CString olEtod = prlDepFlight->Etod != TIMENULL ? CString(" ETD:") + prlDepFlight->Etod.Format("%H%M") : "";
			CString olOfbl = prlDepFlight->Ofbl != TIMENULL ? CString(" OFBL:") + prlDepFlight->Ofbl.Format("%H%M") : "";
			olDepText.Format("%s STD:%s%s%s P:%s G:%s",
							prlDepFlight->Fnum,prlDepFlight->Stod.Format("%H%M"),olEtod,olOfbl,
							ogFlightData.GetPosition(prlDepFlight), ogFlightData.GetGate(prlDepFlight));
			ilCnt++;
		}

		olFlightStatus = olArrText;
		if(ilCnt == 2)
		{
			olFlightStatus += (" -> ");
		}
		olFlightStatus += olDepText;
	}

	return olFlightStatus;
}

BOOL EquipmentDiagramViewer::FindGroup(const char *pcpEquipmentAreaId, int &ripGroupno)
{
	for (ripGroupno = 0; ripGroupno < GetGroupCount(); ripGroupno++)
		if (GetGroup(ripGroupno)->EquipmentAreaId == pcpEquipmentAreaId)
			return TRUE;

	return FALSE;
}

// this new version of FindGroupAndLine allows iteration because it is now
// possible to have a flight displayed more than once ie if equipment appears
// in more than one group For this function to
// iterate properly, ripGroupno and ropLineno need to be initialized to -1
// - subsequent calls will then increment ripLineno.
BOOL EquipmentDiagramViewer::FindGroup(long lpEquUrno, int &ripGroupno)
{
	if(ripGroupno == -1)
		ripGroupno = 0;
	else
		ripGroupno++;

	CString olGroup = CViewer::GetGroup(); // either "EquipGroup" or "EquipType"

	for (ripGroupno; ripGroupno < GetGroupCount(); ripGroupno++)
	{
		CString olGroupName = GetGroup(ripGroupno)->EquipmentAreaId;
		if(olGroup == "EquipmentType") // group by equipment type
		{
			EQTDATA *prlEqt = ogEqtData.GetEqtByName(olGroupName);
			if(prlEqt != NULL)
			{
				CCSPtrArray <EQUDATA> olEquList;
				ogEquData.GetEquipmentByType(prlEqt->Urno, olEquList);
				int ilNumEqu = olEquList.GetSize();
				for(int ilE = 0; ilE < ilNumEqu; ilE++)
				{
					if(olEquList[ilE].Urno == lpEquUrno)
						return TRUE;
				}
			}
		}
		else if(olGroup == "EquipmentGroup") // group by static groups
		{
			CCSPtrArray <ALLOCUNIT> olEquipment;
			ogAllocData.GetUnitsByGroup(ALLOCUNITTYPE_EQUIPMENTGROUP,olGroupName,olEquipment);
			int ilNumEquipments = olEquipment.GetSize();
			for(int ilEquipment = 0; ilEquipment < ilNumEquipments; ilEquipment++)
			{
				EQUDATA *prlEqu = ogEquData.GetEquByUrno(olEquipment[ilEquipment].Urno);
				if(prlEqu != NULL && prlEqu->Urno == lpEquUrno)
						return TRUE;
			}
		}
	}

	return FALSE;
}

// this new version of FindGroupAndLine allows iteration because it is now
// possible to have a flight displayed more than once ie if equipment appears
// in more than one group For this function to
// iterate properly, ripGroupno and ropLineno need to be initialized to -1
// - subsequent calls will then increment ripLineno.
BOOL EquipmentDiagramViewer::FindGroupAndLine(long lpEquUrno, int &ripGroupno, int &ripLineno)
{
	if(ripGroupno == -1)
		ripGroupno = 0;
	if(ripLineno == -1)
		ripLineno = 0;
	else
		ripLineno++;

	int ilNumGroups = GetGroupCount();
	for(; ripGroupno < ilNumGroups; ripGroupno++)
	{
		int ilNumLines = GetLineCount(ripGroupno);
		for(; ripLineno < ilNumLines; ripLineno++)
		{
			EQUIPMENT_LINEDATA *prlLine = GetLine(ripGroupno, ripLineno);
			if(prlLine != NULL && prlLine->EquUrno == lpEquUrno)
			{
				return TRUE;
			}
		}
		ripLineno = 0;
	}

    return FALSE;
}

BOOL EquipmentDiagramViewer::FindBar(long lpUrno, int &ripGroupno, int &ripLineno, int &ripBarno)
{
	for (ripGroupno = 0; ripGroupno < GetGroupCount(); ripGroupno++)
		for (ripLineno = 0; ripLineno < GetLineCount(ripGroupno); ripLineno++)
            for (ripBarno = 0; ripBarno < GetBarCount(ripGroupno, ripLineno); ripBarno++)
                if (GetBar(ripGroupno, ripLineno, ripBarno)->JobUrno == lpUrno)
                    return TRUE;

    return FALSE;
}

BOOL EquipmentDiagramViewer::FindBkBar(long lpUrno, int &ripGroupno, int &ripLineno, int &ripBkBarno)
{
	for (ripGroupno = 0; ripGroupno < GetGroupCount(); ripGroupno++)
		for (ripLineno = 0; ripLineno < GetLineCount(ripGroupno); ripLineno++)
            for (ripBkBarno = 0; ripBkBarno < GetBkBarCount(ripGroupno, ripLineno); ripBkBarno++)
                if (GetBkBar(ripGroupno, ripLineno, ripBkBarno)->JobUrno == lpUrno)
                    return TRUE;

    return FALSE;
}


/////////////////////////////////////////////////////////////////////////////
// EquipmentDiagramViewer -- Viewer basic operations

int EquipmentDiagramViewer::GetGroupCount()
{
    return omGroups.GetSize();
}

EQUIPMENT_GROUPDATA *EquipmentDiagramViewer::GetGroup(int ipGroupno)
{
    return &omGroups[ipGroupno];
}

CString EquipmentDiagramViewer::GetGroupText(int ipGroupno)
{
    return omGroups[ipGroupno].Text;
}

CString EquipmentDiagramViewer::GetGroupTopScaleText(int ipGroupno)
{
	CString s;
	return s;
}

int EquipmentDiagramViewer::GetGroupColorIndex(int ipGroupno,CTime opStart,CTime opEnd)
{
	int ilColorIndex = FIRSTCONFLICTCOLOR+(CFI_NOCONFLICT*2);
	CBrush *prlBrush = ogBrushs[ilColorIndex];
	int ilLineCount = GetLineCount(ipGroupno);
	int ilColour, ilType;
	bool blConfirmed;
	for (int ilLineno = 0; ilLineno < ilLineCount; ilLineno++)
	{
		for (int ilBarno = 0; ilBarno < GetBarCount(ipGroupno, ilLineno); ilBarno++)
		{
			EQUIPMENT_BARDATA *prlBar;
			if ((prlBar = GetBar(ipGroupno, ilLineno, ilBarno)) != NULL)
			{
				if (IsOverlapped(opStart, opEnd, prlBar->StartTime, prlBar->EndTime))
				{
					JOBDATA *prlJob = ogJobData.GetJobByUrno(prlBar->JobUrno);
					if (prlJob != NULL)
					{
//						ogConflicts.GetJobConflict(prlJob, ilColour, ilType, blConfirmed, ALLOCUNITTYPE_EQUIPMENT);
						ogConflicts.GetJobConflict(prlJob, ilColour, ilType, blConfirmed);
						if(ilType != CFI_NOCONFLICT && !blConfirmed)
						{
							// valid conflict found
							return FIRSTCONFLICTCOLOR+(CFI_GAT_NOTCOVERED*2);
						}
					}
				}
			}
		}
	}
	return ilColorIndex;
}

int EquipmentDiagramViewer::GetLineCount(int ipGroupno)
{
    return omGroups[ipGroupno].Lines.GetSize();
}

EQUIPMENT_LINEDATA *EquipmentDiagramViewer::GetLine(int ipGroupno, int ipLineno)
{
    return &omGroups[ipGroupno].Lines[ipLineno];
}

CString EquipmentDiagramViewer::GetLineText(int ipGroupno, int ipLineno)
{
    return omGroups[ipGroupno].Lines[ipLineno].Text;
}

CString EquipmentDiagramViewer::GetLineStatusText(int ipGroupno, int ipLineno)
{
    return omGroups[ipGroupno].Lines[ipLineno].StatusText;
}

int EquipmentDiagramViewer::GetMaxOverlapLevel(int ipGroupno, int ipLineno)
{
    return omGroups[ipGroupno].Lines[ipLineno].MaxOverlapLevel;
}

int EquipmentDiagramViewer::GetVisualMaxOverlapLevel(int ipGroupno, int ipLineno)
{
	// For max level 0,1,2 we will use 2 levels, 3,4,5 we will use 5 levels, 6,7,8 we use 8 levels, and so on ...
	return GetMaxOverlapLevel(ipGroupno, ipLineno) / 3 * 3 + 2;
}

int EquipmentDiagramViewer::GetBarCount(int ipGroupno, int ipLineno)
{
    return omGroups[ipGroupno].Lines[ipLineno].Bars.GetSize();
}

int EquipmentDiagramViewer::GetBkBarCount(int ipGroupno, int ipLineno)
{
    return omGroups[ipGroupno].Lines[ipLineno].BkBars.GetSize();
}

EQUIPMENT_BARDATA *EquipmentDiagramViewer::GetBar(int ipGroupno, int ipLineno, int ipBarno)
{
    return &omGroups[ipGroupno].Lines[ipLineno].Bars[ipBarno];
}

EQUIPMENT_BKBARDATA *EquipmentDiagramViewer::GetBkBar(int ipGroupno, int ipLineno, int ipBarno)
{
    return &omGroups[ipGroupno].Lines[ipLineno].BkBars[ipBarno];
}

CString EquipmentDiagramViewer::GetBarText(int ipGroupno, int ipLineno, int ipBarno)
{
    return omGroups[ipGroupno].Lines[ipLineno].Bars[ipBarno].Text;
}

CString EquipmentDiagramViewer::GetBarStatusText(int ipGroupno, int ipLineno, int ipBarno)
{
//	return 	FlightStatus(ogFlightData.GetFlightByUrno(omGroups[ipGroupno].Lines[ipLineno].Bars[ipBarno].FlightUrno));
    return omGroups[ipGroupno].Lines[ipLineno].Bars[ipBarno].StatusText;
}

CString EquipmentDiagramViewer::GetBkBarText(int ipGroupno, int ipLineno, int ipBkBarno)
{
    return omGroups[ipGroupno].Lines[ipLineno].BkBars[ipBkBarno].Text;
}

CString EquipmentDiagramViewer::GetBkBarStatusText(int ipGroupno, int ipLineno, int ipBkBarno)
{
    return omGroups[ipGroupno].Lines[ipLineno].BkBars[ipBkBarno].StatusText;
}

int EquipmentDiagramViewer::GetIndicatorCount(int ipGroupno, int ipLineno, int ipBarno)
{
    return omGroups[ipGroupno].Lines[ipLineno].Bars[ipBarno].Indicators.GetSize();
}

EQUIPMENT_INDICATORDATA *EquipmentDiagramViewer::GetIndicator(int ipGroupno, int ipLineno, int ipBarno, int ipIndicatorno)
{
    return &omGroups[ipGroupno].Lines[ipLineno].Bars[ipBarno].Indicators[ipIndicatorno];
}

/////////////////////////////////////////////////////////////////////////////
// EquipmentDiagramViewer - EQUIPMENT_BARDATA array maintenance (overlapping version)

void EquipmentDiagramViewer::DeleteAll()
{
    while (GetGroupCount() > 0)
        DeleteGroup(0);
}

int EquipmentDiagramViewer::CreateGroup(EQUIPMENT_GROUPDATA *prpGroup)
{
    int ilGroupCount = omGroups.GetSize();

// Generally, raw data will be sorted from left to right.
// So, scanning for the place of insertion backward is a little bit faster
//
#ifndef SCANBACKWARD
    // Search for the position which we want to insert this new bar
    for (int ilGroupno = 0; ilGroupno < ilGroupCount; ilGroupno++)
		if (CompareGroup(prpGroup, GetGroup(ilGroupno)) <= 0)
            break;  // should be inserted before Groups[ilGroupno]
#else
    // Search for the position which we want to insert this new bar
    for (int ilGroupno = ilGroupCount; ilGroupno > 0; ilGroupno--)
		if (CompareGroup(prpGroup, GetGroup(ilGroupno-1)) >= 0)
            break;  // should be inserted after Groups[ilGroupno-1]
#endif
    
    omGroups.NewAt(ilGroupno, *prpGroup);
    return ilGroupno;
}

void EquipmentDiagramViewer::DeleteGroup(int ipGroupno)
{
    while (GetLineCount(ipGroupno) > 0)
        DeleteLine(ipGroupno, 0);

    omGroups.DeleteAt(ipGroupno);
}

int EquipmentDiagramViewer::CreateLine(int ipGroupno, EQUIPMENT_LINEDATA *prpLine)
{
    int ilLineCount = omGroups[ipGroupno].Lines.GetSize();

// Generally, raw data will be sorted from left to right.
// So, scanning for the place of insertion backward is a little bit faster
//
#ifndef SCANBACKWARD
    // Search for the position which we want to insert this new bar
    for (int ilLineno = 0; ilLineno < ilLineCount; ilLineno++)
		if (CompareLine(prpLine, GetLine(ipGroupno, ilLineno)) <= 0)
            break;  // should be inserted before Lines[ilLineno]
#else
    // Search for the position which we want to insert this new bar
    for (int ilLineno = ilLineCount; ilLineno > 0; ilLineno--)
		if (CompareLine(prpLine, GetLine(ipGroupno, ilLineno-1)) >= 0)
            break;  // should be inserted after Lines[ilLineno-1]
#endif

    omGroups[ipGroupno].Lines.NewAt(ilLineno, *prpLine);
    EQUIPMENT_LINEDATA *prlLine = &omGroups[ipGroupno].Lines[ilLineno];
    prlLine->MaxOverlapLevel = 0;   // there is no bar in this line right now
    return ilLineno;
}

void EquipmentDiagramViewer::DeleteLine(int ipGroupno, int ipLineno)
{
	// Id 14-Sep-1996
	// Don't simply call DeleteBar() for sake of efficiency
	EQUIPMENT_LINEDATA *prlLine = &omGroups[ipGroupno].Lines[ipLineno];
	while (prlLine->Bars.GetSize() > 0)
	{
		prlLine->Bars[0].Indicators.DeleteAll();	// delete associated indicators first
		prlLine->Bars.DeleteAt(0);
	}
	while (prlLine->BkBars.GetSize() > 0)
	{
		prlLine->BkBars[0].Indicators.DeleteAll();	// delete associated indicators first
		prlLine->BkBars.DeleteAt(0);
	}

    omGroups[ipGroupno].Lines.DeleteAt(ipLineno);
}

int EquipmentDiagramViewer::CreateBar(int ipGroupno, int ipLineno, EQUIPMENT_BARDATA *prpBar, BOOL bpTopBar)
{
    EQUIPMENT_LINEDATA *prlLine = GetLine(ipGroupno, ipLineno);
    int ilBarCount = prlLine->Bars.GetSize();

	CUIntArray olBarnoList;
	GetOverlappedBarsFromTime(ipGroupno, ipLineno,
		prpBar->StartTime, prpBar->EndTime, olBarnoList);
	int ilListCount = olBarnoList.GetSize();

	if (bpTopBar)
	{
		for (int ilLc = 0; ilLc < ilListCount; ilLc++)
		{
			int ilBarno = olBarnoList[ilLc];
			EQUIPMENT_BARDATA *prlBar = &prlLine->Bars[ilBarno];
			int ilNewOverlapLevel = ++prlBar->OverlapLevel;
			prlLine->MaxOverlapLevel = max(prlLine->MaxOverlapLevel, ilNewOverlapLevel);
		}
		prlLine->Bars.NewAt(ilBarCount, *prpBar);	// to be the last bar in painting order
		prlLine->Bars[ilBarCount].OverlapLevel = 0;
		return ilBarCount;
	}
	else
	{
		int ilNewBarLevel = -1;
		for (int ilLc = 0; ilLc < ilListCount; ilLc++)
		{
			int ilBarno = olBarnoList[ilLc];
			EQUIPMENT_BARDATA *prlBar = &prlLine->Bars[ilBarno];
			ilNewBarLevel = max(ilNewBarLevel, prlBar->OverlapLevel + 1);
				// the +1 means that we have to insert the new bar below this bar
		}
		ilNewBarLevel = ilNewBarLevel == -1 ? 0 : ilNewBarLevel;
		prlLine->Bars.NewAt(0, *prpBar);	// to be the first bar in painting order
		prlLine->Bars[0].OverlapLevel = ilNewBarLevel;
		prlLine->MaxOverlapLevel = max(prlLine->MaxOverlapLevel, ilNewBarLevel);
		return 0;
	}
}

void EquipmentDiagramViewer::DeleteBar(int ipGroupno, int ipLineno, int ipBarno)
{
    EQUIPMENT_LINEDATA *prlLine = GetLine(ipGroupno, ipLineno);
	EQUIPMENT_BARDATA *prlBar = &prlLine->Bars[ipBarno];

	// Delete all indicators of this bar
	while (GetIndicatorCount(ipGroupno, ipLineno, ipBarno) > 0)
		DeleteIndicator(ipGroupno, ipLineno, ipBarno, 0);

	CUIntArray olBarnoList;
	GetOverlappedBarsFromTime(ipGroupno, ipLineno,
		prlBar->StartTime, prlBar->EndTime, olBarnoList);
	int ilListCount = olBarnoList.GetSize();

	// Save the bar in this overlapping group to a temporary array
	CCSPtrArray<EQUIPMENT_BARDATA> olSavedBars;
	BOOL blMustRecomputeMaxOverlapLevel = FALSE;
	for (int ilLc = 0; ilLc < ilListCount; ilLc++)
	{
		int ilBarno = olBarnoList[ilLc];
		EQUIPMENT_BARDATA *prlBar = &prlLine->Bars[ilBarno];
		blMustRecomputeMaxOverlapLevel |= (prlBar->OverlapLevel == prlLine->MaxOverlapLevel);
			// check if deletion of this bar may change the line height
		if (ilBarno != ipBarno)	// must we save this bar?
			olSavedBars.NewAt(olSavedBars.GetSize(), prlLine->Bars[ilBarno]);
	}
	// Delete all bars (which are already backed up in a temporary array) from the viewer
	while (ilListCount-- > 0)
	{
		int ilBarno = olBarnoList[ilListCount];
		prlLine->Bars.DeleteAt(ilBarno);
	}
	// Then, we insert those bars back to the Gantt chart, except the specified one
	int ilSavedBarCount = olSavedBars.GetSize();
	for (int ilSavedBarno = 0; ilSavedBarno < ilSavedBarCount; ilSavedBarno++)
	{
		CreateBar(ipGroupno, ipLineno, &olSavedBars[ilSavedBarno]);
	}
	olSavedBars.DeleteAll();
	// Then recompute the MaxOverlapLevel if it's necessary
	if (blMustRecomputeMaxOverlapLevel)
	{
		prlLine->MaxOverlapLevel = 0;
	    int ilBarCount = prlLine->Bars.GetSize();
		for (int ilBarno = 0; ilBarno < ilBarCount; ilBarno++)
		{
			EQUIPMENT_BARDATA *prlBar = &prlLine->Bars[ilBarno];
			prlLine->MaxOverlapLevel = max(prlLine->MaxOverlapLevel, prlBar->OverlapLevel);
		}
	}
}

void EquipmentDiagramViewer::DeleteBkBar(int ipGroupno, int ipLineno, int ipBkBarno)
{
    omGroups[ipGroupno].Lines[ipLineno].BkBars.DeleteAt(ipBkBarno);
}

void EquipmentDiagramViewer::DeleteBkBarsByJob(int ipGroupno, int ipLineno, long lpJobUrno)
{
	int ilNumBkBars = omGroups[ipGroupno].Lines[ipLineno].BkBars.GetSize();
	for(int ilBkBar = (ilNumBkBars-1); ilBkBar >= 0; ilBkBar--)
	{
		if(omGroups[ipGroupno].Lines[ipLineno].BkBars[ilBkBar].JobUrno == lpJobUrno)
		{
			omGroups[ipGroupno].Lines[ipLineno].BkBars.DeleteAt(ilBkBar);
		}
	}
}

int EquipmentDiagramViewer::CreateIndicator(int ipGroupno, int ipLineno, int ipBarno, EQUIPMENT_INDICATORDATA *prpIndicator)
{
    int ilIndicatorCount = omGroups[ipGroupno].Lines[ipLineno].Bars[ipBarno].Indicators.GetSize();

    // Search for the position which we want to insert this new indicator
    for (int ilIndicatorno = 0; ilIndicatorno < ilIndicatorCount; ilIndicatorno++)
	{
		EQUIPMENT_INDICATORDATA *prlTmpIndicator = GetIndicator(ipGroupno,ipLineno,ipBarno,ilIndicatorno);
		if (prlTmpIndicator == NULL)
			break; // should not happen

		if (prpIndicator->DemandUrno != 0)	// new indicator has a demand
		{
			if (prlTmpIndicator->DemandUrno == 0)
				break;	// should be inseted before a job without demand

			// the job in the buffer has some demand, so we use the old algorithm
			if (prpIndicator->IsArrival)	// new indicator is for arrival demand
			{
				if (!prlTmpIndicator->IsArrival)
					break;	// should be inserted before indicator for departure demand
				if (prlTmpIndicator->StartTime < prpIndicator->StartTime)
					break;	// should place indicator in time order
				if (prlTmpIndicator->StartTime == prpIndicator->StartTime)
				{
					if (prlTmpIndicator->DemandUrno < prpIndicator->DemandUrno)
						break;	// if the time is equal, we will use the Urno instead
				}
			}
			else	// new indicator is for departure demand
			{
				if (prlTmpIndicator->IsArrival)
					continue;	// bar in buffer is for arrival demand, step to the next one
				if (prlTmpIndicator->StartTime < prpIndicator->StartTime)
					break;	// should place indicator in time order
				if (prlTmpIndicator->StartTime == prpIndicator->StartTime)
				{
					if (prlTmpIndicator->DemandUrno < prpIndicator->DemandUrno)
						break;	// should place indicator in demand URNO order
				}
			}
		}
		else	// new indicator has no demand
		{
			if (prlTmpIndicator->DemandUrno != 0)
				continue;	// bar in buffer has demand, step to the next one
			if (prlTmpIndicator->StartTime < prpIndicator->StartTime)
				break;	// should place indicator in time order
				if (prlTmpIndicator->StartTime == prpIndicator->StartTime)
				{
					if (prlTmpIndicator->JobUrno < prpIndicator->JobUrno)
						break;	// if the time is equal, we will use the Urno instead
				}
		}
	}

    omGroups[ipGroupno].Lines[ipLineno].Bars[ipBarno].Indicators.NewAt(ilIndicatorno, *prpIndicator);
    return ilIndicatorno;
}

void EquipmentDiagramViewer::DeleteIndicator(int ipGroupno, int ipLineno, int ipBarno, int ipIndicatorno)
{
    omGroups[ipGroupno].Lines[ipLineno].Bars[ipBarno].Indicators.DeleteAt(ipIndicatorno);
}


/////////////////////////////////////////////////////////////////////////////
// EquipmentDiagramViewer - EQUIPMENT_BARDATA calculation and searching routines

// Return the bar number of the list box based on the given period [time1, time2]
int EquipmentDiagramViewer::GetBarnoFromTime(int ipGroupno, int ipLineno, CTime opTime1, CTime opTime2,
    int ipOverlapLevel1, int ipOverlapLevel2)
{
    for (int i = GetBarCount(ipGroupno, ipLineno)-1; i >= 0; i--)
    {
        EQUIPMENT_BARDATA *prlBar = GetBar(ipGroupno, ipLineno, i);
        if (opTime1 <= prlBar->EndTime && prlBar->StartTime <= opTime2 &&
            ipOverlapLevel1 <= prlBar->OverlapLevel && prlBar->OverlapLevel <= ipOverlapLevel2)
            return i;
    }
    return -1;
}


// Return the background bar number of the list box based on the given period [time1, time2]
int EquipmentDiagramViewer::GetBkBarnoFromTime(int ipGroupno, int ipLineno, CTime opTime1, CTime opTime2)
{
    for (int i = GetBkBarCount(ipGroupno, ipLineno)-1; i >= 0; i--)
    {
        EQUIPMENT_BKBARDATA *prlBkBar = GetBkBar(ipGroupno, ipLineno, i);
        if (prlBkBar != NULL && opTime1 <= prlBkBar->EndTime && prlBkBar->StartTime <= opTime2)
            return i;
    }
    return -1;
}

/////////////////////////////////////////////////////////////////////////////
// EquipmentDiagramViewer private helper methods

void EquipmentDiagramViewer::GetOverlappedBarsFromTime(int ipGroupno, int ipLineno,
	CTime opTime1, CTime opTime2, CUIntArray &ropBarnoList)
{
    EQUIPMENT_LINEDATA *prlLine = GetLine(ipGroupno, ipLineno);
    int ilBarCount = prlLine->Bars.GetSize();

	// Prepare the empty array of the involvement flag for each bar
	CCSPtrArray<BOOL> IsInvolvedBar;
	for (int i = 0; i < ilBarCount; i++)
		IsInvolvedBar.NewAt(i, FALSE);

	// We will loop again and again until there is no more bar in this overlapping found.
	// The overlapping group is at first determine by the given "opTime1" and "opTime2".
	// But if the bar we find is exceed this period of time, we will extend searching
	// to both left side and right side in the next round.
	//
	int nBarsFoundThisRound = -1;
	while (nBarsFoundThisRound != 0)
	{
		nBarsFoundThisRound = 0;
		for (int ilBarno = 0; ilBarno < ilBarCount; ilBarno++)
		{
			CTime olStartTime = prlLine->Bars[ilBarno].StartTime;
			CTime olEndTime = prlLine->Bars[ilBarno].EndTime;
			BOOL blIsOverlapped = IsReallyOverlapped(olStartTime, olEndTime, opTime1, opTime2);
			BOOL blIsWithIn = IsWithIn(olStartTime, olEndTime, opTime1, opTime2);
				// this IsWithIn() fix the real thin bar bug
			BOOL blIsInvolved = (blIsOverlapped || blIsWithIn);

			if (!IsInvolvedBar[ilBarno] && blIsInvolved)	// some more new involved bars?
			{
				IsInvolvedBar[ilBarno] = TRUE;
				nBarsFoundThisRound++;
				opTime1 = min(opTime1, prlLine->Bars[ilBarno].StartTime);
				opTime2 = max(opTime2, prlLine->Bars[ilBarno].EndTime);
			}
		}
	}

	// Create the list of involved bars, then store them to "ropBarnoList"
	ropBarnoList.RemoveAll();
	for (int ilBarno = 0; ilBarno < ilBarCount; ilBarno++)
	{
		if (IsInvolvedBar[ilBarno])
		{
			ropBarnoList.Add(ilBarno);
		}
	}
	IsInvolvedBar.DeleteAll();
}

void EquipmentDiagramViewer::AllowUpdates(BOOL bpNoUpdatesNow)
{
	bmNoUpdatesNow = bpNoUpdatesNow;
}



///////////////////////////////////////////////////////////////////////////////////////////
// Printing routines

void EquipmentDiagramViewer::PrintEquipmentDiagramHeader(int ipGroupno)
{
	CString olEquipmentAreaName = GetGroupText(ipGroupno);
	CString omTarget;
//	omTarget.Format("%s  von: %s  bis: %s",olGateAreaName,omStartTime.Format("%d.%m.%Y %H:%M"),omEndTime.Format("%d.%m.%Y  %H:%M"));
	omTarget.Format(GetString(IDS_STRING61285),olEquipmentAreaName,omStartTime.Format("%d.%m.%Y %H:%M"),omEndTime.Format("%d.%m.%Y  %H:%M"));
	CString olPrintDate = ogBasicData.GetTime().Format("%d.%m.%Y");

	pomPrint->omCdc.StartPage();
	pomPrint->PrintHeader(GetString(IDS_EQ_CHARTVIEW),olPrintDate,omTarget);
	pomPrint->PrintTimeScale(400,2800,omStartTime,omEndTime,olEquipmentAreaName);
}

void EquipmentDiagramViewer::PrintPrepareLineData(int ipGroupNo,int ipLineno,CCSPtrArray<PRINTBARDATA> &ropPrintLine,
												  CCSPtrArray<PRINTBARDATA> &ropBkBars)
{
	EQUIPMENT_LINEDATA *prlLine = &omGroups[ipGroupNo].Lines[ipLineno];
	ropPrintLine.DeleteAll();
	int ilBarCount = prlLine->Bars.GetSize();
	for( int i = 0; i < ilBarCount; i++)
	{
		if (IsOverlapped(prlLine->Bars[i].StartTime,prlLine->Bars[i].EndTime,
			omStartTime,omEndTime))
		{
			PRINTBARDATA rlPrintBar;
			rlPrintBar.Text = prlLine->Bars[i].Text;
			rlPrintBar.StartTime = prlLine->Bars[i].StartTime;
			rlPrintBar.EndTime = prlLine->Bars[i].EndTime;
			rlPrintBar.FrameType = prlLine->Bars[i].FrameType;
			rlPrintBar.MarkerType = prlLine->Bars[i].MarkerType;
			rlPrintBar.OverlapLevel = 0;
			rlPrintBar.IsBackGroundBar = FALSE;
			ropPrintLine.NewAt(ropPrintLine.GetSize(),rlPrintBar);
		}
	}
	ilBarCount = prlLine->BkBars.GetSize();
	for( i = 0; i < ilBarCount; i++)
	{
		if (IsOverlapped(prlLine->BkBars[i].StartTime,prlLine->BkBars[i].EndTime,
			omStartTime,omEndTime))
		{
			PRINTBARDATA rlPrintBar;
			if (prlLine->BkBars[i].Type == EQUIPMENT_BKBARDATA::FASTLINK)
				rlPrintBar.MarkerType = MARKFULL;
			else if (prlLine->BkBars[i].Type == EQUIPMENT_BKBARDATA::BLOCKED) 
				rlPrintBar.MarkerType = MARKHATCHED;
			else
				continue;
			rlPrintBar.Text = prlLine->BkBars[i].Text;
			rlPrintBar.StartTime = prlLine->BkBars[i].StartTime;
			rlPrintBar.EndTime = prlLine->BkBars[i].EndTime;
			rlPrintBar.FrameType = prlLine->BkBars[i].FrameType;
			rlPrintBar.IsShadowBar = FALSE;
			rlPrintBar.OverlapLevel = 0;
			rlPrintBar.IsBackGroundBar = TRUE;

			ropBkBars.NewAt(ropBkBars.GetSize(),rlPrintBar);
		}
	}

}


void EquipmentDiagramViewer::PrintEquipmentArea(CPtrArray &opPtrArray,int ipGroupNo)
{
	if( (pomPrint->imLineNo+ogBasicData.imFreeBottomLines) > pomPrint->imMaxLines )
	{
		if ( pomPrint->imPageNo > 0)
		{
			pomPrint->PrintGanttBottom();
			pomPrint->PrintFooter("","");
			pomPrint->omCdc.EndPage();
			igFirstGroupOnPage = TRUE;
		}
		PrintEquipmentDiagramHeader(ipGroupNo);
	}
	else
	{
		CString olAreaName = GetGroupText(ipGroupNo);
		pomPrint->PrintGanttHeader(400,2800,olAreaName);
	}

	CCSPtrArray<PRINTBARDATA> ropPrintLine;
	CCSPtrArray<PRINTBARDATA> ropBkBars;
	int ilLineCount = GetLineCount(ipGroupNo);
    for( int ilLineno = 0; ilLineno < ilLineCount; ilLineno++)
	{
		if( (pomPrint->imLineNo+ogBasicData.imFreeBottomLines) > pomPrint->imMaxLines )
		{
			if ( pomPrint->imPageNo > 0)
			{
				pomPrint->PrintGanttBottom();
				pomPrint->PrintFooter("","");
				pomPrint->omCdc.EndPage();
				igFirstGroupOnPage = TRUE;
			}
			PrintEquipmentDiagramHeader(ipGroupNo);
		}

		PrintPrepareLineData(ipGroupNo,ilLineno,ropPrintLine,ropBkBars);
		if (pomPrint->PrintGanttLine(GetLineText(ipGroupNo, ilLineno),ropPrintLine,ropBkBars) != TRUE)
		{
			// page full
			pomPrint->PrintGanttBottom();
			pomPrint->PrintFooter("","");
			pomPrint->omCdc.EndPage();
			igFirstGroupOnPage = TRUE;
			pomPrint->imLineNo = 0;
			PrintEquipmentDiagramHeader(ipGroupNo);
			// try it again, but don't continue with this line if it don't fit on page again!
			pomPrint->PrintGanttLine(GetLineText(ipGroupNo, ilLineno),ropPrintLine,ropBkBars);
		}

	}
	pomPrint->PrintGanttBottom();
	//pomPrint->imLineNo = 999;
}




void EquipmentDiagramViewer::PrintDiagram(CPtrArray &opPtrArray)
{
	igFirstGroupOnPage = TRUE;
	CString omTarget = CString("Anzeigebegin: ") + CString(omStartTime.Format("%d.%m.%Y"))
		+  "     Ansicht: " + ogCfgData.rmUserSetup.GACV;
	CString olPrintDate = ogBasicData.GetTime().Format("%d.%m.%Y");
	pomPrint = new CCSPrint(pomAttachWnd,PRINT_PORTRAIT,80,500,200,
		CString("EqupmentDiagramm"),olPrintDate,omTarget);
	if (pomPrint != NULL)
	{
		if (pomPrint->InitializePrinter(PRINT_LANDSCAPE,ogBasicData.pcmPrintForm,DMPAPER_A3)  == TRUE)
		{
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			DOCINFO	rlDocInfo;
			memset(&rlDocInfo, 0, sizeof(DOCINFO));
			rlDocInfo.cbSize = sizeof( DOCINFO );
			rlDocInfo.lpszDocName = "Equipment Diagram";	
			pomPrint->omCdc.StartDoc( &rlDocInfo );

			int ilGroupCount = GetGroupCount();
			for (int ilGroupno = 0; ilGroupno < ilGroupCount; ilGroupno++)
			{
				PrintEquipmentArea(opPtrArray,ilGroupno);
			}
			pomPrint->PrintFooter("","");
			pomPrint->omCdc.EndPage();
			pomPrint->omCdc.EndDoc();
		}
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	}
}

