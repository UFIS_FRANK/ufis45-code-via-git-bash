/////////////////////////////////////////////
// Modul: Implementation of Search
//
// Author: MWO
// Date:   20 September 1996

#include <stdafx.h>
#include <ccsglobl.h>
#include <CedaEmpData.h>
#include <CedaShiftData.h> 
#include <CedaJobData.h>
#include <CedaFlightData.h>
#include <FilterData.h>
#include <search.h>

Search::Search()
{
}

Search::~Search()
{
	omFlights.DeleteAll();
	omEmps.DeleteAll();
	omShifts.DeleteAll();
	omFlightUrnoMap.RemoveAll();
}

int Search::FilterEmployees()
{
	omFlights.DeleteAll();
	omEmps.DeleteAll();
	omShifts.DeleteAll();

	//Cases 
	// Name | Shift | Rank
	//   x  |   -   |  -
	//   x  |   x   |  -
	//   x  |   -   |  -
	//   -  |   x   |  x
	//   -  |   -   |  x
	//   -  |   x   |  x
	// Date is always filled

	//Name empty and shift filled ==> we have to search in ShiftData
	if( (strcmp(ogSearchFilter.rlEmpFilterData.Shift, "") != 0)  &&
		(strcmp(ogSearchFilter.rlEmpFilterData.Name, "") == 0))
	{
		int ilCount;
				  
		ilCount = ogShiftData.omData.GetSize();
		for(int i = 0; i < ilCount; i++)
		{
			SHIFTDATA rlShift = ogShiftData.omData[i];
			
			//Shiht id found
			char pclYear[5]="", pclMonth[3]="", pclDay[3]="";
			sscanf((const char *)rlShift.Sday, "%4s%2s%2s", pclYear, pclMonth, pclDay);
			pclYear[4] = '\0'; 
			pclMonth[2] = '\0'; 
			pclDay[2] = '\0';
			CTime olTmpDate = CTime(atoi(pclYear), atoi(pclMonth), atoi(pclDay), 0, 0, 0);
			if( (strcmp(ogSearchFilter.rlEmpFilterData.Shift, rlShift.Sfca) == 0) &&
				(ogSearchFilter.rlEmpFilterData.Date == olTmpDate))
			{
				//Search All Employees with that shift-code
				if(strcmp(ogSearchFilter.rlEmpFilterData.Rank, "") != 0)
				{  
					int ilEmpCount;
					ilEmpCount = ogEmpData.omData.GetSize();
					for(int j = 0; j < ilEmpCount; j++)
					{
						EMPDATA rlEmp = ogEmpData.omData[j];
						
						if(strcmp(rlShift.Peno, rlEmp.Peno) == 0)
						{
							if(strcmp(ogSearchFilter.rlEmpFilterData.Rank, rlShift.Efct) == 0)
							{
								EMPDATA *prlEmp = new EMPDATA;
								SHIFTDATA *prlShift = new SHIFTDATA;
								memcpy(prlShift, &rlShift, sizeof(SHIFTDATA));
								memcpy(prlEmp, &rlEmp, sizeof(EMPDATA));
								omEmps.Add(prlEmp);
								omShifts.Add(prlShift);
								j = ilEmpCount;
							}
						}
					}
				}
				else //Rank does not matter
				{
					int ilEmpCount;
					ilEmpCount = ogEmpData.omData.GetSize();
					for(int j = 0; j < ilEmpCount; j++)
					{
						EMPDATA rlEmp = ogEmpData.omData[j];
						
						if(strcmp(rlShift.Peno, rlEmp.Peno) == 0)
						{
							EMPDATA *prlEmp = new EMPDATA;
							SHIFTDATA *prlShift = new SHIFTDATA;
							memcpy(prlShift, &rlShift, sizeof(SHIFTDATA));
							memcpy(prlEmp, &rlEmp, sizeof(EMPDATA));
							omEmps.Add(prlEmp);
							omShifts.Add(prlShift);
							j = ilEmpCount;
						}
					}
				}//else Rank does not matter
			}//Shift found

		}//for all Shifts
	}//if name empty && shift filled
	//Shift empty ==> we have to perform the search per EmpData
	else if(strcmp(ogSearchFilter.rlEmpFilterData.Shift, "") == 0)
	{
		int ilEmpCount = ogEmpData.omData.GetSize();
		for(int i = 0; i < ilEmpCount; i++)
		{
			BOOL blRankAndName = FALSE;
			BOOL blNameOK = TRUE;
			BOOL blRankOK = FALSE;	
			EMPDATA rlEmp = ogEmpData.omData[i];
			if(strcmp(ogSearchFilter.rlEmpFilterData.Name, "") != 0)
			{
				blNameOK = CompareName(ogSearchFilter.rlEmpFilterData.Name,rlEmp.Lanm);
			}
			// if Name is not empty
			else
			{
				blNameOK = TRUE;
			}
			if(strcmp(ogSearchFilter.rlEmpFilterData.Rank, "") != 0)
			{
				/* Test only */
				SHIFTDATA *prlShift = ogShiftData.GetShiftByPeno(rlEmp.Peno);
				if (prlShift != NULL)
				{
					if(strcmp(ogSearchFilter.rlEmpFilterData.Rank, prlShift->Efct) == 0)
					{
						blRankOK = TRUE;
					}
				}
			} // Rank != ""
			else
			{
				blRankOK = TRUE;
			}
			blRankAndName = ((blNameOK == TRUE) && (blRankOK == TRUE));
			if(blRankAndName == TRUE)
			{
				// Now lets look for the date
				int ilShiftCount = ogShiftData.omData.GetSize();
				for(int ili = 0; ili < ilShiftCount; ili++)
				{
					SHIFTDATA rlShift = ogShiftData.omData[ili];
					if(strcmp(rlShift.Peno, rlEmp.Peno) == 0)
					{
						char pclYear[5]="", pclMonth[3]="", pclDay[3]="";
						sscanf((const char *)rlShift.Sday, "%4s%2s%2s", pclYear, pclMonth, pclDay);
						pclYear[4] = '\0'; 
						pclMonth[2] = '\0'; 
						pclDay[2] = '\0';
						CTime olTmpDate = CTime(atoi(pclYear), atoi(pclMonth), atoi(pclDay), 0, 0, 0);
						
						if(ogSearchFilter.rlEmpFilterData.Date == olTmpDate)
						{
							EMPDATA *prlEmp = new EMPDATA;
							SHIFTDATA *prlShift = new SHIFTDATA;
							memcpy(prlShift, &rlShift, sizeof(SHIFTDATA));		
							omShifts.Add(prlShift);
							memcpy(prlEmp, &rlEmp, sizeof(EMPDATA));
							omEmps.Add(prlEmp);
						}
					}
				}
			}

			blRankAndName = FALSE;
			blNameOK = TRUE;
			blRankOK = FALSE;	
		} //for all employees
	}
	// in this case we must take into consideration both
	else if((strcmp(ogSearchFilter.rlEmpFilterData.Shift, "") != 0)  &&
		    (strcmp(ogSearchFilter.rlEmpFilterData.Name, "") != 0))
	{
		int ilEmpCount = ogEmpData.omData.GetSize();
		for(int i = 0; i < ilEmpCount; i++)
		{
			BOOL blRankAndName = FALSE;
			BOOL blNameOK = TRUE;
			BOOL blRankOK = FALSE;	
			EMPDATA rlEmp = ogEmpData.omData[i];

			if(strcmp(ogSearchFilter.rlEmpFilterData.Name, "") != 0)
			{
				blNameOK = CompareName(ogSearchFilter.rlEmpFilterData.Name,rlEmp.Lanm);
				
			}
			// if Name is not empty
			else
			{
				blNameOK = TRUE;
			}

			if(strcmp(ogSearchFilter.rlEmpFilterData.Rank, "") != 0)
			{
				/* Test only */
				SHIFTDATA *prlShift = ogShiftData.GetShiftByPeno(rlEmp.Peno);
				if (prlShift != NULL)
				{
					if(strcmp(ogSearchFilter.rlEmpFilterData.Rank, prlShift->Efct) == 0)
					{
						blRankOK = TRUE;
					}
				}
			} // Rank != ""
			else
			{
				blRankOK = TRUE;
			}
			blRankAndName = ((blNameOK == TRUE) && (blRankOK == TRUE));
			if(blRankAndName == TRUE)
			{
				// Now lets look for the date
				int ilShiftCount = ogShiftData.omData.GetSize();
				for(int ili = 0; ili < ilShiftCount; ili++)
				{
					SHIFTDATA rlShift = ogShiftData.omData[ili];
					if((strcmp(rlShift.Peno, rlEmp.Peno) == 0) && 
					   (strcmp(ogSearchFilter.rlEmpFilterData.Shift, rlShift.Sfca) == 0))
					{
						char pclYear[5]="", pclMonth[3]="", pclDay[3]="";
						sscanf((const char *)rlShift.Sday, "%4s%2s%2s", pclYear, pclMonth, pclDay);
						pclYear[4] = '\0'; 
						pclMonth[2] = '\0'; 
						pclDay[2] = '\0';
						CTime olTmpDate = CTime(atoi(pclYear), atoi(pclMonth), atoi(pclDay), 0, 0, 0);
						
						if(ogSearchFilter.rlEmpFilterData.Date == olTmpDate)
						{
							SHIFTDATA *prlShift = new SHIFTDATA;
							EMPDATA *prlEmp = new EMPDATA;
							memcpy(prlShift, &rlShift, sizeof(SHIFTDATA));		
							omShifts.Add(prlShift);
							memcpy(prlEmp, &rlEmp, sizeof(EMPDATA));
							omEmps.Add(prlEmp);
						}
					}
				}
			}

			blRankAndName = FALSE;
			blNameOK = TRUE;
			blRankOK = FALSE;	
		} //for all employees
	}
	return 0;
}

int Search::FilterFlights()
{
	omFlights.DeleteAll();
	omFlightUrnoMap.RemoveAll();
	omEmps.DeleteAll();
	omShifts.DeleteAll();

	CTime olFlightDay;

	//char Airline[3];
	//char Flno[6];
	//CTime Date;

	if((strcmp(ogSearchFilter.rlFlightFilterData.Airline, "") != 0) &&
	   (strcmp(ogSearchFilter.rlFlightFilterData.Flno, "") == 0))
	{
		int ilFlightCount = ogFlightData.omData.GetSize();
		for(int i = 0; i < ilFlightCount; i++)
		{
			FLIGHTDATA rlFlight = ogFlightData.omData[i];

			if((olFlightDay = GetFlightDay(rlFlight)) != TIMENULL)
			{
				// 3 - Letter code used ?
				if (strlen(ogSearchFilter.rlFlightFilterData.Airline) == 3)
				{
					if((strcmp(rlFlight.Alc3, ogSearchFilter.rlFlightFilterData.Airline) == 0) &&
					   (ogSearchFilter.rlFlightFilterData.Date == olFlightDay))
					{
						AddFlight(&rlFlight);
					}
				}
				else	// 2 - Letter code used
				{
					if((strcmp(rlFlight.Alc2, ogSearchFilter.rlFlightFilterData.Airline) == 0) &&
					   (ogSearchFilter.rlFlightFilterData.Date == olFlightDay))
					{
						AddFlight(&rlFlight);
					}
				}
			}
		}
	}
	else if((strcmp(ogSearchFilter.rlFlightFilterData.Airline, "") == 0) &&
	   (strcmp(ogSearchFilter.rlFlightFilterData.Flno, "") != 0))
	{
		int ilFlightCount = ogFlightData.omData.GetSize();
		for(int i = 0; i < ilFlightCount; i++)
		{
			FLIGHTDATA rlFlight = ogFlightData.omData[i];

			if((olFlightDay = GetFlightDay(rlFlight)) != TIMENULL)
			{
				if((IsFlightOk(rlFlight.Fltn, ogSearchFilter.rlFlightFilterData.Flno) == TRUE) &&
					//(rlFlight.Flno == atol(ogSearchFilter.rlFlightFilterData.Flno)) &&
				   (ogSearchFilter.rlFlightFilterData.Date == olFlightDay))
				{
					AddFlight(&rlFlight);
				}
			}
		}
	} 
	else if((strcmp(ogSearchFilter.rlFlightFilterData.Airline, "") != 0) &&
	   (strcmp(ogSearchFilter.rlFlightFilterData.Flno, "") != 0))
	{
		int ilFlightCount = ogFlightData.omData.GetSize();
		for(int i = 0; i < ilFlightCount; i++)
		{
			FLIGHTDATA rlFlight = ogFlightData.omData[i];

			if((olFlightDay = GetFlightDay(rlFlight)) != TIMENULL)
			{
				// 3 - Letter code used ?
				if (strlen(ogSearchFilter.rlFlightFilterData.Airline) == 3)
				{
					if((strcmp(rlFlight.Alc3, ogSearchFilter.rlFlightFilterData.Airline) == 0) &&
						//(rlFlight.Flno == atol(ogSearchFilter.rlFlightFilterData.Flno))  &&
						(IsFlightOk(rlFlight.Fltn, ogSearchFilter.rlFlightFilterData.Flno) == TRUE) &&
					   (ogSearchFilter.rlFlightFilterData.Date == olFlightDay))
					{
						AddFlight(&rlFlight);
					}
				}
				else // 2 - Letter code used
				{
					if((strcmp(rlFlight.Alc2, ogSearchFilter.rlFlightFilterData.Airline) == 0) &&
						//(rlFlight.Flno == atol(ogSearchFilter.rlFlightFilterData.Flno))  &&
						(IsFlightOk(rlFlight.Fltn, ogSearchFilter.rlFlightFilterData.Flno) == TRUE) &&
					   (ogSearchFilter.rlFlightFilterData.Date == olFlightDay))
					{
						AddFlight(&rlFlight);
					}
				}
			}
		}
	} 
	return 0;
}

bool Search::AddFlight(FLIGHTDATA *prpFlight)
{
	bool blFlightAdded = false;
	void *pvlDummy;
	if(prpFlight != NULL && !omFlightUrnoMap.Lookup((void *)prpFlight->Urno, (void *&) pvlDummy))
	{
		omFlightUrnoMap.SetAt((void *)prpFlight->Urno,NULL);

		FLIGHTDATA *prlRotation = ogFlightData.GetRotationFlight(prpFlight);
		if(prlRotation != NULL)
		{
			// prevent turnaround flights being added twice
			omFlightUrnoMap.SetAt((void *)prlRotation->Urno,NULL);
		}
		blFlightAdded = true;

		FLIGHTDATA *prlFlight = new FLIGHTDATA;
		memcpy(prlFlight, prpFlight, sizeof(FLIGHTDATA));
		omFlights.Add(prlFlight);
	}

	return blFlightAdded;
}

CTime Search::GetFlightDay(FLIGHTDATA &ropFlight)
{
	CTime olFlightDay = TIMENULL;
/*
	if(strlen(ropFlight.Fdat) > 0)
	{
		char pclYear[10],pclMonth[10],pclDay[10];
		sscanf((const char *)ropFlight.Fdat, "%4s%2s%2s", pclYear, pclMonth, pclDay);
		pclYear[4] = '\0'; pclMonth[2] = '\0'; pclDay[2] = '\0';
		int ilYear = atoi(pclYear);
		int ilMonth = atoi(pclMonth);
		int ilDay = atoi(pclDay);
		if(ilYear > 0 && ilMonth > 0 && ilDay > 0)
		{
			olFlightDay = CTime(ilYear, ilMonth, ilDay, 0, 0, 0);
		}
	}
	else
	{*/
		if(ogFlightData.IsArrival(&ropFlight))
		{
			if ( ropFlight.Tifa != TIMENULL )
				olFlightDay = CTime(ropFlight.Tifa.GetYear(), ropFlight.Tifa.GetMonth(), ropFlight.Tifa.GetDay(), 0, 0, 0);
		}
		else
		{
			if ( ropFlight.Tifd != TIMENULL )
				olFlightDay = CTime(ropFlight.Tifd.GetYear(), ropFlight.Tifd.GetMonth(), ropFlight.Tifd.GetDay(), 0, 0, 0);
		}
	/*}*/

	return olFlightDay;
}

BOOL Search::IsFlightOk(const char *pcpFlight, char *pcpFilter)
{
	char pclFlight[512]="";
	char pclFilter[512]="";
	char pclTmp[100]="";
	int ilCount = 0;
	strcpy(pclFlight, pcpFlight);
	strcpy(pclFilter, pcpFilter);
	sprintf(pclTmp, "%05s", pclFlight);
	strcpy(pclFlight, pclTmp);
	ilCount = (int)strlen(pclFilter);
	for(int i = 0; i < ilCount; i++)
	{
		if(pclFilter[i] != '%')
		{
			//if(pclFilter[i] != pclFlight[i])
			if(pclFilter[i] != pclTmp[i])	//hag20000412
			{
				return FALSE;
			}
		}
	}
	return TRUE;
}

BOOL Search::CompareName(const char *pcpSearch,const char *pcpName)
{
	// ASSERTS
	if (!pcpSearch || !pcpName)
		return FALSE;

	//Now scanning for wildcards
	BOOL blNameOK = TRUE;

	char slSearch[100];
	strncpy(slSearch,pcpSearch,sizeof(slSearch));
	slSearch[sizeof(slSearch)-1] = '\0';

	if (slSearch[strlen(slSearch)] != '%')
		strcat(slSearch,"%");

	return StrFind(slSearch,pcpName);

}

BOOL Search::StrFind(const char *pcpMask,const char*pcpValue) const 
{
	char c;
	while(*pcpMask)
	{
		switch (c= *(pcpMask++))
		{
		case '_' :	// one character wildcard
			if (!*(pcpValue++))
				return FALSE;
		break;
		case '%' :	// multiple character wildcard
			while(*pcpValue)
			{
				if (StrFind(pcpMask,pcpValue++))
					return TRUE;
			}

			return !*pcpMask;
		break;
		default :
			if (tolower(c) != tolower(*(pcpValue++)))
				return FALSE;
		}
	 }

	 return !*pcpValue;
}