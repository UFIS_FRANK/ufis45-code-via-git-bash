// UndoManager.h - this module is the skeleton of the Undo/Redo feature
//
// Description:
// This undo manager is designed for Undo/Redo functionality in general.
// Before using this undo manager, you should know some basic concepts of
// the design of this undo manager.
//
// The heart of this manager is embedded in the class UndoManager.
// This class will record detail steps of each user action to the internal array.
// This manager will automatically generate the user action number internally.
// You can change user action number to a new one by calling the method NewAction().
// So, normally, you will call NewAction() followed by many times of NewStep(...).
// To ease the implementation, two dialogs are already included in this class for
// Undo/Redo. See methods DoModalUndoDialog() and DoModalRedoDialog().
//
// However, the undo manager cannot do anything by itself. It needs UndoObject.
// Since in each step the undo manager just keep a pointer to an UndoObject, so
// the real action of Undo/Redo has to be performed in UndoObject not in undo manager,
// in another word, this UndoManager just provide the skeleton of Undo/Redo features.
// To implement Undo/Redo features, we must write our own class derived from UndoObject.
//
// The UndoObject class is very simple. It is a pure-base class which consists of
// 4 virtual methods: destructor, undo text, undo implementation, and redo implementation.
// Since all of them are virtual, the undo manager will call these methods which will be
// corresponding to a specific method for each specific class which derived from
// the UndoObject.
//
// You have to create an UndoObject for passing to NewStep() with the "new" operator.
// The undo manager will automatically delete these objects as needed.
//
//
// Written by:
// Damkerng Thammathakerngkit	Sep 17, 1996

#ifndef _UNDOMANAGER_H_
#define _UNDOMANAGER_H_

#include <resource.h>
#include <CCSPtrArray.h>

// Constants
#define DEFAULT_UNDOSIZE	100

// Forward declarations
class CUndoDialog;

/////////////////////////////////////////////////////////////////////////////
// UndoObject

class UndoObject
{
public:
	virtual ~UndoObject();
	virtual CString UndoText() const;	// return text which will be shown in Undo dialog
	virtual BOOL Undo(CWnd *popParentWnd);	// do the necessary things for undoing
	virtual BOOL Redo(CWnd *popParentWnd);	// do the necessary things for redoing
};

/////////////////////////////////////////////////////////////////////////////
// UndoManager

class UndoManager
{
	friend CUndoDialog;

public:
	UndoManager(int ilUndoSize = DEFAULT_UNDOSIZE);
	~UndoManager();
	void RegisterDDX();
	BOOL SetUndoSize(int ipUndoSize);
	void NewAction();
	void NewStep(const CString &ropKey, UndoObject *popUndoObject);
	void DeleteAllUndoRedoSteps();
	void UndoOneAction(CWnd *popParentWnd);
	void RedoOneAction(CWnd *popParentWnd);
	void DoModalUndoDialog(CWnd *popParentWnd = NULL);
	void GetUndoList(CStringArray &ropUndoList);

// private data for handling Undo/Redo object
private:
	int imUndoSize;		// the number of steps that will be record in this instance
	int imActionno;		// the next user action number
	struct STEP
	{
		CString Key;	// used to identify which "What-if" the user create a job in
		int Actionno;	// for grouping many steps into a single user action
		UndoObject *p;	// pointer to an undo object instance
	};
	CCSPtrArray<STEP> omUndoStep;
	CCSPtrArray<STEP> omRedoStep;

};

/////////////////////////////////////////////////////////////////////////////
// CUndoDialog dialog

class CUndoDialog : public CDialog
{
// Construction
public:
	CUndoDialog(UndoManager *popUndoManager, CWnd* pParent = NULL);	// standard constructor
	void RefreshDialog();

// Dialog Data
	//{{AFX_DATA(CUndoDialog)
	enum { IDD = IDD_UNDODIALOG };
	CListBox	m_List;
	int		m_RadioButton;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CUndoDialog)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(CUndoDialog)
	virtual BOOL OnInitDialog();
	afx_msg void OnShowredolist();
	afx_msg void OnShowundolist();
	afx_msg void OnSelchangeList();
	afx_msg void OnDoit();
	virtual void OnCancel();
	afx_msg void OnMove(int x, int y); //PRF 8712
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

// Private data
private:
	UndoManager *pomUndoManager;
	CWnd *pomParentWnd;				// for MessageBox()
	BOOL bmIsUndoStepExist;
	BOOL bmIsRedoStepExist;
	CRect omWindowRect; //PRF 8712

// Helper routines
private:
	void UpdateShowRadioButtons();
	void LoadStepsToListBox();
	void UndoSelectedSteps();
	void RedoSelectedSteps();
};

#endif	// _UNDOMANAGER_H_
