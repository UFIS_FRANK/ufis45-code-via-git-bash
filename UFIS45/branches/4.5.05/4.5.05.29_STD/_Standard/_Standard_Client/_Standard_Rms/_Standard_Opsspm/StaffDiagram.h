// StaffDiagram.h : header file
//

#ifndef _STAFF_DIAGRAM_
#define _STAFF_DIAGRAM_

#include <clientwn.h>
#include <tscale.h>
#include <StaffViewer.h>
#include <TimePacket.h>
#include <BreakTable.h>
#include <CPrintPreviewView.h>
#define AUTOSCROLL_TIMER_EVENT 2
#define AUTOSCROLL_INITIAL_SPEED 10 // millisecs
#define AUTOSCROLL_SPEED 1000 // millisecs

/////////////////////////////////////////////////////////////////////////////
// StaffDiagram frame

class StaffDiagram : public CFrameWnd
{
    DECLARE_DYNCREATE(StaffDiagram)

public:
	StaffDiagram();   
    StaffDiagram(BOOL bpPrePlanMode,CTime opPrePlanTime);    
	void SetAllStaffAreaButtonsColor(void);
	void SetStaffAreaButtonColor(int ipGroupno);
	BOOL DestroyWindow();

// Attributes
public:
	BOOL bmNoUpdatesNow;

// Operations
public:
    void PositionChild();
    void SetTSStartTime(CTime opTSStartTime);
	void UpdateComboBox();
	void ChangeViewTo(const char *pcpViewName,BOOL RememberPositions = FALSE);
	void OnUpdatePrevNext(void);
	void PrePlanMode(BOOL bpToSet,CTime opPrePlanTime);
	void ToggleOnline(void);
    void OnFirstChart();
    void OnLastChart();
	void SetViewerDate(); //Singapore
	void SetDate(CTime opDate); //Singapore


// Internal used only
// Id 25-Sep-96 -- add keyboard handling to the diagram
private:
	CListBox *GetBottomMostGantt();
	void SetCaptionText(void);
	void SelectComboDate(CTime opDate); //Singapore

// Overrides
public:
	//{{AFX_DATA(StaffDiagram)
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(StaffDiagram)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
    virtual ~StaffDiagram();

    // Generated message map functions
    //{{AFX_MSG(StaffDiagram)
    afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnDestroy();
	afx_msg void OnClose();
    afx_msg void OnGetMinMaxInfo(MINMAXINFO FAR* lpMMI);
    afx_msg void OnPaint();
    afx_msg BOOL OnEraseBkgnd(CDC* pDC);
    afx_msg void OnSize(UINT nType, int cx, int cy);
    afx_msg void OnMove(int x, int y); //PRF 8712
    afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
    afx_msg void OnNextChart();
    afx_msg void OnPrevChart();
    afx_msg void OnArrival();
    afx_msg void OnDeparture();
    afx_msg void OnMabstab();
    afx_msg void OnTimer(UINT nIDEvent);
    afx_msg LONG OnPositionChild(WPARAM wParam, LPARAM lParam);
    afx_msg LONG OnUpdateDiagram(WPARAM wParam, LPARAM lParam);
    afx_msg LONG OnRememberListPositions(WPARAM wParam, LPARAM lParam);
    afx_msg LONG OnRestoreListPositions(WPARAM wParam, LPARAM lParam);
    afx_msg void OnAnsicht();
	afx_msg void OnViewSelChange();
	afx_msg void OnCloseupView();
	afx_msg void OnZeit();
	afx_msg void OnPrint();
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags);
    afx_msg LONG OnDragOver(UINT, LONG); 
    afx_msg void OnExpandTeam();
    afx_msg void OnCompressTeam();
    afx_msg void OnHideTeamDelegations();
	afx_msg void OnUpdateUIZeit(CCmdUI *pCmdUI);
	afx_msg void OnUpdateUIMabstab(CCmdUI *pCmdUI);
	afx_msg void OnUpdateUIPrint(CCmdUI *pCmdUI);
	afx_msg void OnUpdateUIAssign(CCmdUI *pCmdUI);
	afx_msg void OnUpdateUIAnsicht(CCmdUI *pCmdUI);
	afx_msg void OnUpdateUIView(CCmdUI *pCmdUI);
    afx_msg LONG OnSelectDiagram(WPARAM wParam, LPARAM lParam);
    afx_msg void OnBreakTable();
    afx_msg LONG OnBreakTableExit(UINT wParam, LONG lParam);
	afx_msg void OnSelchangeDate(); //Singapore
	afx_msg void OnPrintPreview(); //PRF 8704
	afx_msg void OnBeginPrinting(WPARAM wParam, LPARAM lParam); //PRF 8704
	afx_msg void PrintPreview(WPARAM wParam, LPARAM lParam); //PRF 8704
	afx_msg void OnEndPrinting(WPARAM wParam, LPARAM lParam); //PRF 8704
	afx_msg void OnPreviewPrint(WPARAM wParam, LPARAM lParam); //PRF 8704
	//}}AFX_MSG
    DECLARE_MESSAGE_MAP()

protected:
	CUIntArray omTopIndexList;
	void RememberListPositions();
	void RestoreListPositions();

    CDialogBar omDialogBar;
    C2StateButton omArrival;
    C2StateButton omDeparture;
    C3DStatic omTime;
    C3DStatic omDate;
    C3DStatic omTSDate;
    
    CTimeScale omTimeScale;
    CBitmapButton omBB1, omBB2;
    
    CClientWnd omClientWnd;
    CStatusBar omStatusBar;
    
    CTime omStartTime;
    CTimeSpan omDuration;
    CTime omTSStartTime;
    CTimeSpan omTSDuration;
    CTimeSpan omTSInterval;
    
    CPtrArray omPtrArray;
    int imFirstVisibleChart;
    int imStartTimeScalePos;
    
    CTime omPrePlanTime;
	BOOL omPrePlanMode;
	CString omCaptionText;
	BOOL bmIsViewOpen;

	BreakTable *m_wndBreakTable;

	long CalcTotalMinutes(void);

    StaffDiagramViewer omViewer;

// Redisplay all methods for DDX call back function
public:
	void RedisplayAll();

	bool bmScrolling;
	void AutoScroll(UINT ipInitialScrollSpeed = AUTOSCROLL_INITIAL_SPEED);
	void OnAutoScroll(void);
	CCSDragDropCtrl m_DragDropTarget;

// Time Band data, use the TIMENULL value for hiding the marker
public:
	void SetTimeBand(CTime opStartTime, CTime opEndTime);
	void UpdateTimeBand();
	void HandleGlobalDateUpdate(CTime opDate);
	void HandleUpdatePoolJobForTeam(JOBDATA *prpJob);
	CString omCurrentView;

private:
	CTime omTimeBandStartTime;
	CTime omTimeBandEndTime;
	CComboBox* pom_Date; //Singapore
	CRect omWindowRect; //PRF 8712
	CSingleDocTemplate* pomTemplate; //Singapore
	CPrintPreviewView* pomPrintPreviewView; //Singapore

private:
    static CPoint omMaxTrackSize;
    static CPoint omMinTrackSize;
    static COLORREF lmBkColor;
    static COLORREF lmTextColor;
    static COLORREF lmHilightColor;

};

/////////////////////////////////////////////////////////////////////////////


#endif // _STAFF_DIAGRAM_
