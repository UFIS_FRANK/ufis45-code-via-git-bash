// EquipmentDetailViewer.h : header file
//

#ifndef _EQUIPMENTDV_H_
#define _EQUIPMENTDV_H_

#include <CedaEquData.h>
#include <CedaJobData.h>
#include <CedaBlkData.h>
#include <CedaValData.h>
#include <CedaFlightData.h>

#define EQUIPMENTDATA	EMPDATA
#define EquipmentData	EQUIPMENTDATA


struct EQUIPMENTDETAIL_INDICATORDATA
{
	long DemandUrno;
	// standard data for indicators in general GanttBar
	CTime StartTime;
	CTime EndTime;
	COLORREF Color;
};

struct EQUIPMENTDETAIL_BKBARDATA
{
	enum	BKBARTYPE
	{
		NONE		= -1,
		BLOCKED		= 0,
		FASTLINK	= 1,
	};

	BKBARTYPE		Type;
	CString StatusText;
    CString Text;
	COLORREF TextColor;
    CTime StartTime;
    CTime EndTime;
    int FrameType;
    int MarkerType;
    CBrush *MarkerBrush;
    int OverlapLevel;       // zero for the top-most level, each level lowered by 4 pixels
	long JobUrno;
	bool OfflineBar;

	EQUIPMENTDETAIL_BKBARDATA(void)
	{
		TextColor		= 0L;
		FrameType		= -1;
		MarkerType		= -1;
		MarkerBrush		= NULL;
		OverlapLevel	= 0;
		OfflineBar		= false;
	}
};

struct EQUIPMENTDETAIL_BARDATA
{
	CString StatusText;
    CString Text;
    CTime StartTime;
    CTime EndTime;
    int FrameType;
    int MarkerType;
    CBrush *MarkerBrush;
    int OverlapLevel;       // zero for the top-most level, each level lowered by 4 pixels
	int FlightType;
	long JobUrno;
	long FlightUrno;
	bool OfflineBar;
	bool DifferentFunction;

	EQUIPMENTDETAIL_BARDATA(void)
	{
		FrameType		= -1;
		MarkerType		= -1;
		MarkerBrush		= NULL;
		OverlapLevel	= 0;
		JobUrno			= 0L;
		FlightUrno		= 0L;
		OfflineBar		= false;
		DifferentFunction = false;
	}
};


struct EQUIPMENTDETAIL_LINEDATA 
{
    CString Text;
    int MaxOverlapLevel;    // maximum number of overlapped level of bars in this line

    CCSPtrArray <EQUIPMENTDETAIL_BARDATA> Bars;
	CCSPtrArray <EQUIPMENTDETAIL_BKBARDATA> BkBars;

	EQUIPMENTDETAIL_LINEDATA()
	{
		MaxOverlapLevel = 0;
	}
};


/////////////////////////////////////////////////////////////////////////////
// EquipmentDetailViewer window

class EquipmentDetailViewer: public CViewer
{
// Constructions
public:
//	char  lmEmpUrno[24];
    EquipmentDetailViewer();
    ~EquipmentDetailViewer();
	void Init(EQUDATA *prpEqu);
	CCSPtrArray <JOBDATA> omJobList;
	EQUDATA *prmEqu;
	CBrush omBlockedBrush, omFastLinkBrush, omBkTextBrush;

	void RemoveAll();
	void Attach(CWnd *popAttachWnd);

	// Line
    int GetLineCount(void);
    EQUIPMENTDETAIL_LINEDATA *GetLine(int ipLineno);
    CString GetLineText(int ipLineno);

	// ForeGround Bar
    int CreateBar(int ipLineno, EQUIPMENTDETAIL_BARDATA *prpBar, BOOL bpFrontBar = TRUE);
    void DeleteBar(int ipLineno, int ipBarno);
    int GetBarCount(int ipLineno);
    EQUIPMENTDETAIL_BARDATA *GetBar(int ipLineno, int ipBarno);
    int GetBarnoFromTime(int ipLineno, CTime opTime1, CTime opTime2, int ipOverlapLevel1, int ipOverlapLevel2);
	CString BarText(JOBDATA *prpJob);
	CString GetBkBlkBarStatusText(int ipBkBlkBarno);
	CString GetBarStatusText(int ipLineno, int ipBarno);
	CString FlightStatus(FLIGHTDATA *prpFlight);

	// BackGround Bar
    int GetBkBarCount(int ipLineno);
    EQUIPMENTDETAIL_BKBARDATA *GetBkBar(int ipLineno, int ipBarno);
	int GetBkBarnoFromTime(int ipLineno, CTime opTime1, CTime opTime2, int ipOverlapLevel1, int ipOverlapLevel2);
	int GetBkBlkBarnoFromTime(CTime opTime1, CTime opTime2, int ipOverlapLevel1, int ipOverlapLevel2);
	
	// Indicator
    int CreateIndicator(int ipLineno, int ipBarno, EQUIPMENTDETAIL_INDICATORDATA *prpIndicator);
	int GetIndicatorCount(int ipLineno, int ipBarno);
	EQUIPMENTDETAIL_INDICATORDATA *GetIndicator(int ipLineno, int ipBarno, int ipIndicatorno);
	
	// Broadcast export function
	void ProcessRefresh(void);
	void ProcessBlkChange(BLKDATA *prpBlk);
	void ProcessJobChange(JOBDATA *prpJob);
	void ProcessValChange(VALDATA *prpVal);

public:
	long lmJobPrimaryKey;
//	BOOL bmIsFirstTime;
	CWnd *pomAttachWnd;
private:
	void GetOverlappedBarsFromTime(int ipLineno,CTime opTime1, CTime opTime2, CUIntArray &ropBarnoList);
///////////??????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????

public:
	CCSPtrArray <EQUIPMENTDETAIL_BKBARDATA> omEquipmentBlockedBkBars;
	void GetEquipmentBlockedBkBars(void);
	CCSPtrArray <EQUIPMENTDETAIL_LINEDATA> omEquipmentLines;
	EQUIPMENTDETAIL_LINEDATA *GetFreeLineForTime(CTime opBarStart, CTime opBarEnd);

	EQUIPMENTDETAIL_LINEDATA *MakeLine(void);
	EQUIPMENTDETAIL_BKBARDATA* MakeBlkBkBar(const CTime& ropFrom,const CTime& ropTo,const CString& ropText);
	EQUIPMENTDETAIL_BKBARDATA* MakeFastLinkBkBar(EQUIPMENTDETAIL_LINEDATA *prpLine, JOBDATA *prpJob);
	EQUIPMENTDETAIL_BARDATA* MakeBar(EQUIPMENTDETAIL_LINEDATA *prpLine, JOBDATA *prpJob);
};


/////////////////////////////////////////////////////////////////////////////

#endif
