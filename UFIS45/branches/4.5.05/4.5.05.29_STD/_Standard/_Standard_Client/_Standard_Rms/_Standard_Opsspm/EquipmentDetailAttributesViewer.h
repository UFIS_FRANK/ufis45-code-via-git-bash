///////////////////////////////////////////////////
//

#ifndef _EQDD_ATTRIBUTES_VIEWER_
#define _EQDD_ATTRIBUTES_VIEWER_

#include <CCSPtrArray.h>
#include <CedaEqaData.h>
#include <cviewer.h>
#include <CCSTable.h>


class EquipmentDetailAttributesViewer : public CViewer
{
public:
	EquipmentDetailAttributesViewer();
	~EquipmentDetailAttributesViewer();
    void Attach(CCSTable *popAttachWnd);
	EQADATA *GetEqaByLine(int ilLineno);

	void UpdateView(long lpEquUrno);
	void UpdateDisplay();
	void UpdateChangedValues(void);
	bool ResetChangedValues(int ipLine);

private:
    CCSTable *pomTable;
	CCSPtrArray <EQADATA> omEqaList;
	void MakeLineData(EQADATA *prpEqa, CCSPtrArray <TABLE_COLUMN> *popColList);

public:

};
#endif _EQDD_ATTRIBUTES_VIEWER_