// GateChart.h : header file
//

#ifndef _GATECHRT_
#define _GATECHRT_


#ifndef _CHART_STATE_
#define _CHART_STATE_

enum ChartState { Minimized, Normal, Maximized };

#endif // _CHART_STATE_

#include <CCSButtonCtrl.h>

/////////////////////////////////////////////////////////////////////////////
// GateChart frame

class GateDiagram;

class GateChart : public CFrameWnd
{
	friend GateDiagram;

    DECLARE_DYNCREATE(GateChart)
public:
    GateChart();           // protected constructor used by dynamic creation
    virtual ~GateChart();

// Operations
public:
    int GetHeight();
    ChartState GetState(void) { return imState; };
    void SetState(ChartState ipState) { imState = ipState; };
    
    CTimeScale *GetTimeScale(void) { return pomTimeScale; };
    void SetTimeScale(CTimeScale *popTimeScale)
    {
        pomTimeScale = popTimeScale;
    };

    CStatusBar *GetStatusBar(void) { return pomStatusBar; };
    void SetStatusBar(CStatusBar *popStatusBar)
    {
        pomStatusBar = popStatusBar;
    };
    
    GateDiagramViewer *GetViewer(void) { return pomViewer; };
    int GetGroupNo() { return imGroupNo; };
    void SetViewer(GateDiagramViewer *popViewer, int ipGroupNo)
    {
        pomViewer = popViewer;
        imGroupNo = ipGroupNo;
    };
    
    CTime GetStartTime(void) { return omStartTime; };
    void SetStartTime(CTime opStartTime) { omStartTime = opStartTime; };

    CTimeSpan GetInterval(void) { return omInterval; };
    void SetInterval(CTimeSpan opInterval) { omInterval = opInterval; };

    GateGantt *GetGanttPtr(void) { return &omGantt; };
    CCSButtonCtrl *GetChartButtonPtr(void) { return &omButton; };
    C3DStatic *GetTopScaleTextPtr(void) { return pomTopScaleText; };
    
// Overrides
public:

// Implementation
protected:
    // Generated message map functions
    //{{AFX_MSG(GateChart)
    afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
    afx_msg void OnSize(UINT nType, int cx, int cy);
    afx_msg BOOL OnEraseBkgnd(CDC* pDC);
    afx_msg void OnPaint();
    afx_msg void OnChartButton();
    afx_msg LONG OnDragOver(UINT, LONG); 
    afx_msg LONG OnDrop(UINT, LONG); 
	afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnDestroy();
	//}}AFX_MSG
    DECLARE_MESSAGE_MAP()
    
protected:
    ChartState imState;
    int imHeight;
    
    CCSButtonCtrl omButton;
    C3DStatic *pomTopScaleText;
    
    CTimeScale *pomTimeScale;
    CStatusBar *pomStatusBar;
    GateDiagramViewer *pomViewer;
    int imGroupNo;
    CTime omStartTime;
    CTimeSpan omInterval;
    GateGantt omGantt;

private:    
    static COLORREF lmBkColor;
    static COLORREF lmTextColor;
    static COLORREF lmHilightColor;
    static int imStartTopScaleTextPos;
    static int imStartVerticalScalePos;

// Drag-and-drop section
public:
    CCSDragDropCtrl m_ChartButtonDragDrop;
    CCSDragDropCtrl m_TopScaleTextDragDrop;
	LONG ProcessDropShifts(CCSDragDropCtrl *popDragDropCtrl, DROPEFFECT lpDropEffect);
};

/////////////////////////////////////////////////////////////////////////////

#endif // _GATECHRT_
