
#ifndef COVERAGEGRAPHIC_H
#define COVERAGEGRAPHIC_H

//#include "CCSGlobl.h"
#include <CCSPtrArray.h>


/////////////////////////////////////////////////////////////////////////////
// defines
#define FULL_HOUR					1
#define HALF_HOUR					2
#define QUARTER_HOUR				4
#define TEN_MINUTES				6
#define FIVE_MINUTES				12

//A square round the cursor (with this factor) is used to locate a value in the graphic
#define FUZZY_MOUSE_FACTOR		3


/////////////////////////////////////////////////////////////////////////////
// enum declaration

//@Man:
//@Memo: egCurveType
//@See: CCSCoverage
/*@Doc:
	The following values are defined:
	\begin{itemize}
    \item  LINE_CURVE
	 \item  BIT_LINE_CURVE
    \item  MOUNTAIN_CURVE
    \item  BAR_CURVE
	\end{itemize}
*/	
enum egCurveType {NO_CURVE,LINE_CURVE,BIT_LINE_CURVE,MOUNTAIN_CURVE,BAR_CURVE};

//@Man:
//@Memo: egBackGroundType
//@See: CCSCoverage
/*@Doc:
	The following values are defined:
	\begin{itemize}
    \item  OPAQUE_TYPE
    \item  TRANSPARENT_TYPE
    \item  SOLID_TYPE
	\end{itemize}
*/	
enum egBackNforeGroundType {OPAQUE_TYPE,TRANSPARENT_TYPE,SOLID_TYPE};

//@Man:
//@Memo: egFrameType
//@See: CCSCoverage
/*@Doc:
	The following values are defined:
	\begin{itemize}
    \item  NO_FRAME
    \item  BOXED
    \item  SHADOWED_IN
    \item  SHADOWED_OUT
	\end{itemize}
*/	
enum egFrameType {NO_FRAME,BOXED,SHADOWED_IN,SHADOWED_OUT};

//@Man:
//@Memo: egLineType
//@See: CCSCoverage
/*@Doc:
	The following values are defined:
	\begin{itemize}
    \item  SINGLE_LINE
    \item  DOUBLE_LINE
    \item  DASHED_LINE
    \item  DOTTED_LINE
    \item  DASH_DOT_LINE
	\end{itemize}
*/	
enum egLineType {SINGLE_LINE,DOUBLE_LINE,DASHED_LINE,DOTTED_LINE,DASH_DOT_LINE};

//@Man:
//@Memo: egCurveDesc
//@See: CCSCoverage
/*@Doc:
	The following values are defined:
	\begin{itemize}
    \item  NO_DESC
    \item  ONLY_CURVE
    \item  INTERVALL
    \item  VALUE
	\end{itemize}
*/	
enum egCurveDesc {NO_DESC,ONLY_CURVE,INTERVALL,VALUE};


/////////////////////////////////////////////////////////////////////////////
// Record structure declaration

//@Man: CURVESTYLE
//@See: CoverageGraphic
/*@Doc:
	The following struct-items are defined:
	\begin{itemize}
	\item  #int#		{\bf CurveType}
	\item  #CBrush# 	{\bf *Brush}						Zeiger auf verwendete Brush
	\item  #COLORREF#	{\bf Color}							Ist nur relevant, wenn keine Brush angegeben ist
	\item  #int#		{\bf ForeGroundType}				z.B.: OPAQUE_TYPE,TRANSPARENT_TYPE,SOLID_TYPE, u�.
	\item  #int#		{\bf FrameType}					z.B.: NO_FRAME, BOXED,SHADOWED_IN,
	\item  #int#		{\bf LineType}						Linienart, z.B.:SINGLE_LINE,DOUBLE_LINE,DASHED_LINE,DOTTED_LINE,DASH_DOT_LINE,...
	\item  #int#		{\bf FrameWidth}
	\item  #COLORREF#	{\bf FrameColor}
	\item  #int#		{\bf CurveDesc}					z.B.: NO_DESC,ONLY_CURVE,INTERVALL,VALUE,...
	\item  #COLORREF#	{\bf CurveDescColor}
	\item  #CFont#		{\bf *CurveDescFont}
	\item  #int#		{\bf CurveDescWidth}
	\item  #int#		{\bf CurveDescHeigth}
	\item  #int#		{\bf CurveDescFrame}				z.B.: NO_FRAME,BOXED,SHADOWED_IN,SHADOWED_OUT
	\item  #int#		{\bf CurveDescFrameWidth}
	\item  #COLORREF#	{\bf CurveDescFrameColor}
	\item  #COLORREF#	{\bf CurveDescBackGroundColor}
	\item  #int#		{\bf CurveDescBackGroundType}	z.B.: OPAQUE_TYPE,TRANSPARENT_TYPE,SOLID_TYPE,...
	\end{itemize}
	Kommentar: Nothin' up to now!
*/
struct CURVESTYLE
{
	CBrush 		*Brush;						// Zeiger auf verwendete Brush
	COLORREF		Color;						// Ist nur relevant, wenn keine Brush angegeben ist
													// Ist weder Brush noch Farbe angegeben, dann 
													// wird die Kurve als LINE behandelt und ausgege 
	int			ForeGroundType;			// z.B.: OPAQUE_TYPE,TRANSPARENT_TYPE,SOLID_TYPE, u�.
	int			FrameType;					// z.B.: NO_FRAME, BOXED,SHADOWED_IN,
													//	SHADOWED_OUT
	int			LineType;					// Linienart, z.B.:SINGLE_LINE,DOUBLE_LINE,
													//	DASHED_LINE,DOTTED_LINE, 
													//	DASH_DOT_LINE, u�.
	int			FrameWidth;
	COLORREF		FrameColor;
	int			CurveDescType;				// z.B.: NO_DESC,ONLY_CURVE,INTERVALL,VALUE, u�.
	char			CurveDescText[16];
	COLORREF		CurveDescColor;
	CFont			*CurveDescFont;
	int			CurveDescWidth;
	int			CurveDescHeigth;
	int			CurveDescFrame;			// z.B.: NO_FRAME,BOXED,SHADOWED_IN,
													//	SHADOWED_OUT
	int			CurveDescFrameWidth;
	COLORREF		CurveDescFrameColor;
	COLORREF		CurveDescBackGroundColor;
	int			CurveDescBackGroundType;// z.B.: OPAQUE_TYPE,TRANSPARENT_TYPE,SOLID_TYPE, u�.

	CURVESTYLE(void) 
	{ memset(this,'\0',sizeof(*this));
		ForeGroundType=SOLID_TYPE;FrameType=NO_FRAME;LineType=SINGLE_LINE;FrameWidth=1;
		FrameColor = RED;
		CurveDescFont = &ogSmallFonts_Regular_6;
		CurveDescColor = RED;
		CurveDescType=NO_DESC;CurveDescWidth=0;CurveDescHeigth=0;
		CurveDescFrame=NO_FRAME;CurveDescFrameWidth=0;CurveDescBackGroundType=SOLID_TYPE;
	}

};


//@Man: CURVE
//@See: CoverageGraphic
/*@Doc:
	The following struct-items are defined:
	\begin{itemize}
	\item  #int#							{\bf CurveType}
	\item  #int#							{\bf CurvePriv}
	\item  #CURVESTYLE#					{\bf CurveStyle}		Zeiger auf CURVESTYLE-Struct
	\item  #CCSPtrArray <int>#			{\bf CurveValues}		Liste mit Kurvenwerten
	\item  #CCSPtrArray <int>#			{\bf ScaledValues}	Liste mit skalierten Kurvenwerten
	\item  #CCSPtrArray <CString>#	{\bf TimeValues}		Liste mit Zeitwerten der x-Achse
	\end{itemize}
	Kommentar:
		Die Datenstrukturen zur Erfassung der Kurvendaten werden hier definiert, um 
		die Abstimmung mit den Funktionen dieser Klasse sicherzustellen. Sie m�ssen 
		in der Lage sein, s�mtliche Kurveninformationen verwalten zu k�nnen. Unter 
		Kurveninformationen werden in diesem Zusammenhang sowohl die Kurvenwerte wie 
		auch das Erscheinungsbild der Kurve verstanden. Da es m�glich sein wird, 
		verschiedene Kurventypen in einer Grafik zu verwenden, jeder Kurventyp sich 
		aber durch minimal unterschiedliche Parameter definiert, wird ein Satz von 
		Styles definiert, der die Kurven aller Kurventypen vollst�ndig und eindeutig 
		beschreiben kann. 
		Die Daten der Grafik f�r alle Kurventypen werden in dieser Struktur gehalten, 
		die als eindimensionales Array aufgebaut ist. F�r jede Kurve der Grafik 
		existiert somit ein eigenes Strukturelement. Jedes dieser Strukturelemente 
		setzt sich aus vier Hauptbestandteilen zusammen:
		� Einer Typbezeichnung der Kurve
		� Einer (optischen) Priorit�t
		� Einem Verweis auf eine Liste mit den Kurvenwerten dieser Kurve.
		� Einem Verweis auf ein weiteres Strukturelement, welches einen Satz von Styles enth�lt, 
		  die das optische Erscheinungsbild der Kurve definieren.
		Somit operieren alle Funktionen auf den f�r den jeweiligen Kurventyp relevanten
		Strukturelementen und Kurvendaten. Die Funktionen der Basisklasse wissen alle, 
		welche Kurventypen und welche Unterschiede bei deren Behandlung und Bearbeitung 
		es gibt. Durch die hier beschriebene Kapselung der kurvenrelevanten 
		Informationen in der Basisklasse wird sichergestellt, da� ein falsches Anwenden 
		der Funktionen der Basisklasse weitgehend ausgeschlossen werden kann. Eine 
		Funktion kann somit auf eine beliebige Kurve angewendet werden, die 
		Auswirkungen auf die Kurve verwaltet und realisiert allerdings ausschlie�lich 
		die Basisklasse.
*/
struct CURVE
{
	int							CurveType;
	int							CurvePriv;
	CURVESTYLE					CurveStyle;		// Zeiger auf CURVESTYLE-Struct
	CCSPtrArray <int>			CurveValues;	// Liste mit Kurvenwerten
	CCSPtrArray <int>			ScaledValues;	// Liste mit skalierten Kurvenwerten
};


//@Man: PREDEFINEDCOLORS
//@See: CoverageGraphic
/*@Doc:
	The following struct-items are defined:
	\begin{itemize}
	\item  #CString#			{\bf ColorName}
	\item  #COLORREF#			{\bf ColorValue}
	\item  #int#				{\bf RedValue}
	\item  #int#				{\bf GreenValue}
	\item  #int#				{\bf BlueValue}
	\end{itemize}
	Comment:
	This struct contains a couple of predefined colors. These Colors are defined in
	<CCSGlobl.h>. 
*/
struct PREDEFINEDCOLORS
{
	CString		ColorName;
	COLORREF		ColorValue;
	int			RedValue;
	int			GreenValue;
	int			BlueValue;
};


/////////////////////////////////////////////////////////////////////////////
// Class declaration

//@Man:
//@Memo: ccfgd data
//@See: CCSCedaData
/*@Doc:
	No comment on this class.
*/
class CCoverageGraphic : public CScrollView
{
// Operations
public:
    //@ManMemo: Default constructor
		CCoverageGraphic();
		CCoverageGraphic( CWnd *popMainDialog, CWnd *popCoverage,
							int ipCoverageSize, 
							int ipCoverageWidth,int ipCoverageHeight,
							int ipGraphicalWidth, int ipGraphicalHeight,
							int ipTreeWidth, int ipTreeHeight);
    //@ManMemo: Default Destructor
		~CCoverageGraphic();

    //@ManMemo: The last function to finish the viewer initialization.
		BOOL FixupScaling();

    //@ManMemo: UpdateDisplay
		BOOL UpdateDisplay();

    //@ManMemo: Set the data for a special curve. This is usually called by the Viewer.
		BOOL CurveData(char *popCurveName, CCSPtrArray<int> &ropValues);

    //@ManMemo: Set the data for a special curve. This is called by CurveData.
		BOOL DataForAcurve(CURVE *prpCurve, CCSPtrArray<int> &ropValues);

    //@ManMemo: Set the data for a special curve. This is called by DataForAcurve.
		BOOL SetValueDataForAcurve(CCSPtrArray<int> &destValues,
																	CCSPtrArray<int> &ropValues);

    //@ManMemo: Find the curve index according to the given name.
		int FindCurveNo(char *popCurveName);

	//@ManMemo: Init. the parameters for the null point of the diagram (in Coverage class)
		void SetCoverageNullPointParas(int ipCoverageOffsetX, int ipCoverageOffsetY);

	//@ManMemo: Draw the y-axis, based upon the values of the necessary global parameters
		BOOL DrawYaxis(CDC *dc);

	//@ManMemo: Draw the y-axis values
		BOOL DrawYAxisDescr(CDC *dc);

	//@ManMemo: Draws the description text of a curve
		BOOL DrawSingleCurveDescr(int ipCurveNo, CDC *dc);

	//@ManMemo: Configure a special Curves.
		BOOL ConfigureAcurve(int ipCurveIndex, CURVE *popCurve);

   //@ManMemo: Get back the length of the x-axis, which is in fact the maximal length of the graphic.
		int GetNewGraphicWidth();

   //@ManMemo: Set the dates shown in the caption.
		void SetDates(CTime opStartDate, CTime opEndDate);

	//@ManMemo: Set the preplanmode flag.
		void SetPreplanMode(BOOL bpIsPreplanMode);

	//Set a new intervall time
		BOOL GetIntervallTime(int &ipIntervallTime);
		BOOL SetIntervallTime(int ipIntervallTime);

	//Set a new time for the first value on the x-axis
		BOOL GetStartTime(int &ipStartTime);
		BOOL SetStartTime(CTime opStartTime);

    //@ManMemo: Pass the actual scaling factor for the curves.
		BOOL GetScalingFactor(float &fpScaleFactor);
		BOOL SetScalingFactor(float &fpScaleFactor);

    //@ManMemo: Pass the actual maxvalue per x-axis.
		BOOL GetValuesPerXaxis(int &ipValuesPerXaxis);
		BOOL SetValuesPerXaxis(int &ipValuesPerXaxis);

    //@ManMemo: Pass the actual maxvalue for marks per y-axis.
		BOOL GetMarksPerYaxis(int &ipMarksPerYaxis);
		BOOL SetMarksPerYaxis(int &ipMarksPerYaxis);

    //@ManMemo: Preconfigure graphic with standard values. These values are set as defines in <Coverage.h>.
		BOOL ConfigureCurveColor(int ipCurveNo, COLORREF lpCurveColor);
		BOOL ConfigureCurveDescrColor(int ipCurveNo, COLORREF lpAxisColor);
		BOOL ConfigureAxisColor(COLORREF lpAxisColor);
		BOOL ConfigureAxisStyle(int ipAxisWidth, int ipSimpleUnitLenght, int ipMainUnitLenght);
		BOOL ConfigureAxisValues(int ipMaxValuesPerXaxis,int ipMaxValuesPerYaxis,
															int ipMaxMarksPerXaxis,int ipMaxMarksPerYaxis);
		BOOL ConfigureGraphic(COLORREF lpBkColor, COLORREF lpAxisColor, 
															int imFrameTextHeight,
															int ipTopOffset, int ipBottomOffset, int ipLeftOffset,
															int ipRightOffset, int ipFrameLeftOffset, int ipFrameRightOffset,
															int ipFrameTopOffset, int ipFrameBottomOffset,
															int ipCancelButtonWidth, int ipCancelButtonRightOffset,
															int ipCurveDescrAxisOffset,
															int ipCurveDescrXoffset, int ipCurveDescrYoffset,
															int ipCurveDescrWidth, int ipCurveDescrHeight,
															int ipCurveTextWidth, int ipCurveTextHeight,
															int ipGraphicOffset);

protected:
	// Generated message map functions
	//{{AFX_MSG(CCoverageGraphic)
	afx_msg void OnDraw(CDC* pDC);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:
	//@ManMemo: Initialize predefined Colors from CCSGlobl.h
		BOOL InitPredefColor();

	//@ManMemo: Draw the x-axis, based upon the values of the necessary global parameters
		BOOL DrawXaxis(CDC *dc);
		BOOL DrawAxis(CDC *dc);

	//@ManMemo: Draw the x-axis values
		BOOL DrawAxisDescr(CDC *dc);
		BOOL DrawXAxisDescr(CDC *dc);

	//@ManMemo: Init. the parameters for the null point of the diagram
		void SetNullPointParas();

	//@ManMemo: Draw all description of the curves
		BOOL DrawCurveDescr(CDC *dc);

	//@ManMemo: Draws the description of a curve, called by DrawCurveDescr
		BOOL DrawSingleCurveDescrValues(int ipCurveNo, CDC *dc);

    //@ManMemo: Draw all the curves of the actual graphic
		BOOL DrawCurves(CDC *dc);

	//@ManMemo: Draws a curve, called by DrawCurves
		BOOL DrawCurve(int ipCurveNo, CDC *dc);

	//@ManMemo: Draw horizontal dashed lines.
		BOOL DrawHorizontalLines(CDC *dc);

	//@ManMemo: Draws a segment of a curve between two values, called by DrawCurve
		BOOL DrawCurveSegment();

	//@ManMemo: Draws the value field below the diagram
		BOOL DrawValueField();

	//@ManMemo: Draws one special row of the value field below the diagram
		BOOL DrawValueRow();

	//@ManMemo: Init. the parameters for the upper left corner of the graph. area
		BOOL AddCurveInternal(CURVE *prpCurve);

	//@ManMemo: How many values per curve can be displayed at one time on the screen
		int NewStepWidth(int ipValuesPerCurve, int ipAxisLength, int ipCurveDescrWidth);

	//@ManMemo: Get the highest value of all curves and find the needed scaling factor
		BOOL FindInternalScalingFactor();

	//@ManMemo: Reset the scaling factor
		BOOL NoScalingFactor();

		//@ManMemo: Set a new header 'cause something has changed on the x-axis
		BOOL SetNewWindowText();

	//@ManMemo: Use a new scaling factor
		BOOL ActivateNewScalingFactor();

	//@ManMemo: Set the intervalltime without building a new timevalue object
		BOOL SetInternalIntervallTime(int ipIntervallTime);

	//@ManMemo: 
		BOOL IsThisAfullUnit(int ipVerifyUnit);

	//@ManMemo: 
		BOOL TimeForAllCurveValues(int ipCurveNo);

	//@ManMemo: 
		int GetMaxNumValuesOfAllCurves();

	//@ManMemo: 
		int GetMaxRealValueOfAllCurves();
		int GetMaxScaledValueOfAllCurves();

	//@ManMemo: Compare actual mouse position with position of curve value (ipCurveNo, ipValueNo)
		BOOL ComparePositions(int ipCurveNo, int ipValueNo, 
															 int ipStartX, int ipStartY, CPoint opMousePoint);

	//@ManMemo: 
		BOOL SetNewYstepValue(int ipMaxValue);

	//@ManMemo: 
		int NewYmaxValue();



// Attributes
public:
    //@ManMemo: omData
    /*@Doc:
		Die Datenstrukturen zur Erfassung der Kurvendaten werden hier definiert, um 
		die Abstimmung mit den Funktionen dieser Klasse sicherzustellen. Sie m�ssen 
		in der Lage sein, s�mtliche Kurveninformationen verwalten zu k�nnen. Unter 
		Kurveninformationen werden in diesem Zusammenhang sowohl die Kurvenwerte wie 
		auch das Erscheinungsbild der Kurve verstanden. Da es m�glich sein wird, 
		verschiedene Kurventypen in einer Grafik zu verwenden, jeder Kurventyp sich 
		aber durch minimal unterschiedliche Parameter definiert, wird ein Satz von 
		Styles definiert, der die Kurven aller Kurventypen vollst�ndig und eindeutig 
		beschreiben kann. 
		Die Daten der Grafik f�r alle Kurventypen werden in dieser Struktur gehalten, 
		die als eindimensionales Array aufgebaut ist. F�r jede Kurve der Grafik 
		existiert somit ein eigenes Strukturelement. Jedes dieser Strukturelemente 
		setzt sich aus vier Hauptbestandteilen zusammen:
		� Einer Typbezeichnung der Kurve
		� Einer (optischen) Priorit�t
		� Einem Verweis auf eine Liste mit den Kurvenwerten dieser Kurve.
		� Einem Verweis auf ein weiteres Strukturelement, welches einen Satz von Styles enth�lt, die das optische Erscheinungsbild der Kurve definieren.
		Somit operieren alle Funktionen auf den f�r den jeweiligen Kurventyp relevanten
		Strukturelementen und Kurvendaten. Die Funktionen der Basisklasse wissen alle, 
		welche Kurventypen und welche Unterschiede bei deren Behandlung und Bearbeitung 
		es gibt. Durch die hier beschriebene Kapselung der kurvenrelevanten 
		Informationen in der Basisklasse wird sichergestellt, da� ein falsches Anwenden 
		der Funktionen der Basisklasse weitgehend ausgeschlossen werden kann. Eine 
		Funktion kann somit auf eine beliebige Kurve angewendet werden, die 
		Auswirkungen auf die Kurve verwaltet und realisiert allerdings ausschlie�lich 
		die Basisklasse.
	*/
    CCSPtrArray<CURVE>					omData;
	 CCSPtrArray<PREDEFINEDCOLORS>	omPredefColor;

protected:

private:
	CStringArray 	omTimeValues;		// Liste mit Zeitwerten der x-Achse

	CWnd				*pomCoverage;
	CWnd				*pomMainDialog;
	CDC				*omCdc;
	BOOL				imDoThePainting;
	int				imCoverageSize;
	int				imCoverageWidth;
	int				imCoverageHeight;
	int				imTreeWidth;
	int				imTreeHeight;
	int				imGraphicalWidth;
	int				imGraphicalHeight;
	int				imCoverageNullPointX;
	int				imCoverageNullPointY;
	int				imNullPointX;
	int				imNullPointY;
   CFont				*omFont;
	CFont				*fmCurveDescFont;
	COLORREF			lmAxisColor;
	COLORREF			lmBkColor;
   int				imFrameTextHeight;
	int				imTopOffset;
	int				imBottomOffset;
	int				imLeftOffset;
	int				imRightOffset;
	int				imFrameLeftOffset;
	int				imFrameRightOffset;
	int				imFrameTopOffset;
	int				imFrameBottomOffset;
	int				imCancelButtonWidth;
	int				imCancelButtonRightOffset;
	int				imCurveDescrAxisOffset;
	int				imCurveDescrXoffset;
	int				imCurveDescrYoffset;
	int				imCurveDescrWidth;
	int				imCurveDescrHeight;
	int				imCurveTextWidth;
	int				imCurveTextHeight;
	int				imGraphicOffset;
	int				imMaxValuesPerXaxis;
	int				imMaxValuesPerYaxis;
	int				imMaxMarksPerXaxis;
	int				imMaxMarksPerYaxis;
	int				imXstepWidth;
	int				imYstepWidth;
	int				imYvalueWidth;
	int				imXaxisLength;
	int				imYaxisLength;
	float				fmExternalScalingFactor;
	float				fmInternalScalingFactor;
	int				imAxisWidth;
	int				imSimpleUnitLenght;
	int				imMainUnitLenght;
	int				imIntervallTime;
	CTime				omStartTime;
	CTime				omStartDate;
	CTime				omEndDate;
	BOOL				bmIsPreplanMode;
	int				imSaveMaxMarksPerYaxis;
};

extern CCoverageGraphic ogCoverageGraphic;

#endif
