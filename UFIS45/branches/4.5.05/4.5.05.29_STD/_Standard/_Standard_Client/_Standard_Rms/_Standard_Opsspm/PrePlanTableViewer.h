// pviewer.h : header file
//

#ifndef _PPVIEWER_H_
#define _PPVIEWER_H_


// Assignment state of each line in PrePlanTable
enum { UNASSIGNED, PARTIAL_ASSIGNED, FULLY_ASSIGNED };

struct PREPLANT_ASSIGNMENT  // each assignment record
{
    CString Alid;   // assigned to gate or flight
	long Urno;      // gate or flight job Urno 
	BOOL IsSpecialJob; // Special flight or cci job
    CTime Acfr;     // assigned from
    CTime Acto;     // assigned until
};
struct PREPLANT_SHIFTDATA
{
    long ShiftUrno; // from SHFCKI.URNO
    CString Peno;   // staff ID of this record
    CString Name;   // staff name (both Lnam, Fnam)
    CString Rank;   // rank of this staff
    CString Tmid;   // the team that this staff is in
    CString Sfca;   // ShiftCode for this staff
    CTime Acfr;     // actual start period of time of this shift
    CTime Acto;     // actual ending period of time of this shift
	 BOOL  IsTimeChanged; // TRUE if Acfr/Acto is different from SHTCKI

    int AssignmentStatus;   // may be unassigned, partial assigned, or fully assigned
    CCSPtrArray<PREPLANT_ASSIGNMENT> Assignment;
};
struct PREPLANT_LINEDATA
{
    PREPLANT_SHIFTDATA Line;    // the staff who represent the line

    // fields which used only in Complete Team mode
    CCSPtrArray<PREPLANT_SHIFTDATA> TeamMember;
    int FmCount;    // number of flight managers in this team
    int EmpCount;   // number of normal (non-FM) employees in this team
};

/////////////////////////////////////////////////////////////////////////////
// PrePlanTableViewer

class PrePlanTableViewer: public CViewer
{
// Constructions
public:
    PrePlanTableViewer();
    ~PrePlanTableViewer();

    void Attach(CTable *popAttachWnd,BOOL bpIsFromSearch = FALSE);
	void Attach(CTable *popAttachWnd1,CTable *popAttachWnd2, BOOL bpIsFromSearch = FALSE); //Singapore
    CString ChangeViewTo(const char *pcpViewName, BOOL bpIsCompleteTeam,BOOL bpAbsent);
	CString ChangeView();
	
	CTime	StartTime() const;
	CTime	EndTime() const;
	int		Lines() const;
	PREPLANT_LINEDATA *GetLine(int ipLineNo);
	void	SetStartTime(CTime opStartTime);
	void	SetEndTime(CTime opEndTime);

// Internal data processing routines
private:
	static void PrePlanTableCf(void *popInstance, int ipDDXType,void *vpDataPointer, CString &ropInstanceName);
    void PrepareGrouping();
    void PrepareFilter();
    void PrepareSorter();
    BOOL IsPassFilter(const char *pcpRank, const char *pcpTeamId,
						const char *pcpShiftCode, CTime opAcfr, CTime opActo, 
						bool bpEmpIsAbsent,char *pcpDdat,CStringArray &ropPermits);
    int CompareShift(PREPLANT_SHIFTDATA *prpShift1, PREPLANT_SHIFTDATA *prpShift2);
    BOOL IsSameGroup(PREPLANT_SHIFTDATA *prpShift1, PREPLANT_SHIFTDATA *prpShift2);

    void MakeLines();
    void MakeAssignments();
	void MakeAssignment(JOBDATA *prpJob);
    void MakeLine(SHIFTDATA *prpShift);
	void SetLineColour(int ipLineno);

public:
    int RecalcAssignmentStatus(PREPLANT_SHIFTDATA *prpShift);
    void RecalcTeamAssignmentStatus(int ipLineno);
	CString GetGateAreaOrCCIAreaName(const char *pcpAlid);

// Operations
public:
	void GetTeams(CStringArray &ropTeams);
	void GetTeams(CStringList  &ropTeams); //Singapore
	BOOL FindTeam(SHIFTDATA *prpShift, int &rilLineno, CString opTeam);
    BOOL FindShift(long lpShiftUrno, int &rilLineno);
    BOOL FindTeamMember(long lpShiftUrno, int &rilLineno, int &rilTeamMemberno);
    void DeleteAll();
    int CreateLine(PREPLANT_SHIFTDATA *prpShift);
    void DeleteLine(int ipLineno);
    int CreateAssignment(int ipLineno, PREPLANT_ASSIGNMENT *prpAssignment);
	int DeleteAssignmentJob(long lpJobUrno);
    void DeleteAssignment(int ipLineno, int ipAssignmentno);
    int CreateTeamMember(int ipLineno, PREPLANT_SHIFTDATA *prpShift);
    void DeleteTeamMember(int ipLineno, int ipTeamMemberno);
    int CreateTeamMemberAssignment(int ipLineno, int ipTeamMemberno, PREPLANT_ASSIGNMENT *prpAssignment);
    void DeleteTeamMemberAssignment(int ipLineno, int ipTeamMemberno, int ipAssignmentno);
    CString Format(PREPLANT_LINEDATA *prpLine);
	void DeleteAllAssignments(int ipLineNo);
	void SetTerminal(const CString& ropTerminal){omTerminal = ropTerminal;} //Singapore
	inline CString GetTerminal() const {return omTerminal;} //Singapore

// Window refreshing routines
private:
    void UpdateDisplay();
	CString GetPoolName(const char *pcpPoolId);
	void ProcessJobDelete(JOBDATA *prpJob);
	void ProcessJobChange(JOBDATA *prpJob);
	void ProcessShiftChange(SHIFTDATA *prpShift);
	void ProcessShiftDelete(long lpUrno);
	void ProcessShiftSelect(SHIFTDATA *prpShift);
	BOOL FindItemByUrno(JOBDATA *prpJob,int& ripItem, int& ripAssignment);

    // Attributes used for filtering condition
private:
    int omGroupBy;              // enumerated value -- define in "ppviewer.cpp" (use first sorting)
    CWordArray omSortOrder;     // array of enumerated value -- defined in "ppviewer.cpp"
    CMapStringToPtr omCMapForRank;
    CMapStringToPtr omCMapForPermits;
    CMapStringToPtr omCMapForTeam;
    CMapStringToPtr omCMapForShiftCode;
    CString omDate;
    BOOL bmIsCompleteTeam;
    BOOL bmDisplayAbsentEmps;
	BOOL bmIsFromSearch;
	bool bmUseAllShiftCodes ;
	bool bmUseAllRanks;
	bool bmUseAllPermits;
	bool bmUseAllTeams;
	bool bmEmptyTeamSelected;

// Attributes
private:
    CTable *pomTable;
	CString omTerminal; //Singapore
	CTime omStartTime;
	CTime omEndTime;
	CString omSday;

// Methods which handle changes (from Data Distributor)
public:
    CCSPtrArray<PREPLANT_LINEDATA> omLines;
    void ProcessJobNew(JOBDATA *prpJob);
};

#endif  // __PPVIEWER_H_
