// TimeList.cpp : implementation file
//
//

#include <stdafx.h>
#include <TimeFrameList.h>
   
#ifdef _DEBUG
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#define new DEBUG_NEW
#endif

// General macros for testing a point against an interval
#define IsBetween(val, start, end)  ((start) <= (val) && (val) <= (end))
#define IsOverlapped(start1, end1, start2, end2)    ((start1) <= (end2) && (start2) <= (end1))
#define IsReallyOverlapped(start1, end1, start2, end2)	((start1) < (end2) && (start2) < (end1))
#define IsWithIn(start1, end1, start2, end2)	((start1) >= (start2) && (end1) <= (end2))

int TimeFrameList::ByStartTime(const TimeFrameList::TimeFrame **pppFrame1, const TimeFrameList::TimeFrame **pppFrame2)
{
	return (int)((**pppFrame1).omStart.GetTime() - (**pppFrame2).omStart.GetTime());
}

/////////////////////////////////////////////////////////////////////////////
// TimeFrameList

TimeFrameList::TimeFrameList()
{
}

TimeFrameList::TimeFrameList(const CTime& opStartTime,const CTime& opEndTime)
{
	ASSERT(opStartTime < opEndTime);
	omTimeList.New(TimeFrame(opStartTime,opEndTime));
}

TimeFrameList::~TimeFrameList()
{
	omTimeList.DeleteAll();
}

BOOL TimeFrameList::Union(const CTime& opStartTime,const CTime& opEndTime)
{
	ASSERT(opStartTime < opEndTime);

	for (int i = 0; i < omTimeList.GetSize(); i++)
	{
		TimeFrame& olTimeFrame = omTimeList[i];
		if (IsOverlapped(olTimeFrame.omStart,olTimeFrame.omEnd,opStartTime,opEndTime))
		{
			if (opStartTime >= olTimeFrame.omStart)
			{
				if (opEndTime >= olTimeFrame.omEnd)
				{
					olTimeFrame.omEnd = opEndTime;
					return Purge();
				}
				else
					return TRUE;
			}
			else
			{
				olTimeFrame.omStart = opStartTime;
				if (opEndTime >= olTimeFrame.omEnd)
					olTimeFrame.omEnd = opEndTime;
				return Purge();
			}
		}
	}

	omTimeList.New(TimeFrame(opStartTime,opEndTime));
	omTimeList.Sort(ByStartTime);	
	return TRUE;
}

BOOL TimeFrameList::Subtract(const CTime& opStartTime,const CTime& opEndTime)
{
	ASSERT(opStartTime < opEndTime);

	for (int i = 0; i < omTimeList.GetSize(); i++)
	{
		TimeFrame& olTimeFrame = omTimeList[i];
		if (olTimeFrame.emState == DELETED)
			continue;

		if (IsReallyOverlapped(olTimeFrame.omStart,olTimeFrame.omEnd,opStartTime,opEndTime))
		{
			if (opStartTime < olTimeFrame.omStart)
			{
				if (opEndTime >= olTimeFrame.omEnd)
				{
					olTimeFrame.emState = DELETED;
				}
				else
				{
					olTimeFrame.omStart = opEndTime;
					olTimeFrame.emState = CHANGED;
				}
			}
			else if (opStartTime == olTimeFrame.omStart)
			{
				if (opEndTime >= olTimeFrame.omEnd)
				{
					olTimeFrame.emState = DELETED;
				}
				else
				{
					olTimeFrame.omStart = opEndTime;
					olTimeFrame.emState = CHANGED;
				}
			}
			else	// opStartTime > olTimeFrame.omStart
			{
				if (opEndTime >= olTimeFrame.omEnd)
				{
					// the new bar overlaps the end of the timeframe
					// so set the end of the timeframe to the start of the new bar
					olTimeFrame.omEnd = opStartTime;
					olTimeFrame.emState = CHANGED;
				}
				else
				{
					// the new bar lies within the exsting timeframe
					// so split the timeframe in two

					// append new split time frame
					omTimeList.New(TimeFrame(opEndTime,olTimeFrame.omEnd));

					olTimeFrame.omEnd = opStartTime;
					olTimeFrame.emState = CHANGED;
				}
			}
		}
	}

	omTimeList.Sort(ByStartTime);	
	return Purge();
}


BOOL TimeFrameList::Purge()
{
	for (int i = omTimeList.GetSize() - 1; i >= 0; i--)
	{
		if (omTimeList[i].emState == DELETED)
		{
			omTimeList.DeleteAt(i);
		}
	}

	return TRUE;
}

int TimeFrameList::TimeFrames() const
{
	return omTimeList.GetSize();
}

BOOL TimeFrameList::GetTimeFrame(int ipInd,CTime& opStartTime,CTime& opEndTime)
{
	ASSERT(ipInd >= 0 && ipInd < omTimeList.GetSize());
	const TimeFrame& olFrame = omTimeList[ipInd];
	opStartTime = olFrame.omStart;
	opEndTime   = olFrame.omEnd;

	return TRUE;
}
