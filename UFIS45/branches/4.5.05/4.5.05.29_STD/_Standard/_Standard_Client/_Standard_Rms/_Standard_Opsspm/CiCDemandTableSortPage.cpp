// CicDemandTableSortPage.cpp : implementation file
//

#include <stdafx.h>
#include <OpssPm.h>
#include <CCSGlobl.h>
#include <CicDemandTableSortPage.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

// Change this key identifiers for name used in the registry database
#define NUMBER_OF_SORTKEYS	6
static CString ogSortKeys[NUMBER_OF_SORTKEYS] =
	{ "Dety", "Debe", "Deen", "Dedu", "DFnum","none" };
#define NOSORT	(NUMBER_OF_SORTKEYS - 1)	// must be "none"

/////////////////////////////////////////////////////////////////////////////
// CicDemandTableSortPage property page

IMPLEMENT_DYNCREATE(CicDemandTableSortPage, CPropertyPage)

CicDemandTableSortPage::CicDemandTableSortPage() : CPropertyPage(CicDemandTableSortPage::IDD)
{
	//{{AFX_DATA_INIT(CicDemandTableSortPage)
	m_SortOrder0 = -1;
	m_SortOrder1 = -1;
	m_SortOrder2 = -1;
	//}}AFX_DATA_INIT

	omTitle = GetString(IDS_STRING32900);
	m_psp.pszTitle = omTitle;
	m_psp.dwFlags |= PSP_USETITLE;
}

CicDemandTableSortPage::~CicDemandTableSortPage()
{
}

void CicDemandTableSortPage::DoDataExchange(CDataExchange* pDX)
{
	// Extended data exchange -- for member variables with Value type
	if (pDX->m_bSaveAndValidate == FALSE)
	{
		omSortOrders.SetSize(3);
		m_SortOrder0 = GetSortOrder(omSortOrders[0]);
		m_SortOrder1 = GetSortOrder(omSortOrders[1]);
		m_SortOrder2 = GetSortOrder(omSortOrders[2]);
	}

	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CicDemandTableSortPage)
	DDX_Radio(pDX, IDC_RADIO1, m_SortOrder0);
	DDX_Radio(pDX, IDC_RADIO2, m_SortOrder1);
	DDX_Radio(pDX, IDC_RADIO3, m_SortOrder2);
	//}}AFX_DATA_MAP

	// Extended data validation
	if (pDX->m_bSaveAndValidate == TRUE)
	{
		omSortOrders.RemoveAll();
		omSortOrders.Add(GetSortKey(m_SortOrder0));
		omSortOrders.Add(GetSortKey(m_SortOrder1));
		omSortOrders.Add(GetSortKey(m_SortOrder2));
	}
}


BEGIN_MESSAGE_MAP(CicDemandTableSortPage, CPropertyPage)
	//{{AFX_MSG_MAP(CicDemandTableSortPage)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CicDemandTableSortPage message handlers

BOOL CicDemandTableSortPage::OnCommand(WPARAM wParam, LPARAM lParam) 
{
	BOOL bResult = CPropertyPage::OnCommand(wParam, lParam);

	if (HIWORD(wParam) == BN_CLICKED)
	{
		UpdateData();	// caution: this assume there is always no error

		// Disallow using the same sorting order more than once
		if (m_SortOrder1 == m_SortOrder0)
			omSortOrders[1] = "none";
		if (m_SortOrder2 == m_SortOrder0 || m_SortOrder2 == m_SortOrder1)
			omSortOrders[2] = "none";
		UpdateData(FALSE);
	}
	return bResult;
}

/////////////////////////////////////////////////////////////////////////////
// CicDemandTableSortPage -- helper routines

int CicDemandTableSortPage::GetSortOrder(const char *pcpSortKey)
{
	for (int i = 0; i < NUMBER_OF_SORTKEYS; i++)
		if (ogSortKeys[i] == pcpSortKey)
			return i;

	// If there is no sorting order matched, assume "none" for no sorting
	return NOSORT;
}

CString CicDemandTableSortPage::GetSortKey(int ipSortOrder)
{
	if (0 <= ipSortOrder && ipSortOrder <= NUMBER_OF_SORTKEYS-1)
		return ogSortKeys[ipSortOrder];

	return "";	// invalid sort order
}

BOOL CicDemandTableSortPage::OnInitDialog() 
{
	CPropertyPage::OnInitDialog();
	
	CWnd *polWnd = GetDlgItem(IDC_TXT_SORTDETY); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61970));
	}
	polWnd = GetDlgItem(IDC_TXT_SORTDEBE); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61872));
	}
	polWnd = GetDlgItem(IDC_TXT_SORTDEEN); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61873));
	}
	polWnd = GetDlgItem(IDC_TXT_SORTDEDU); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61874));
	}
	polWnd = GetDlgItem(IDC_TXT_SORTDFNUM); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61882));
	}
	polWnd = GetDlgItem(IDC_TXT_SORTNONE); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61328));
	}
	// TODO: Add extra initialization here
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}
