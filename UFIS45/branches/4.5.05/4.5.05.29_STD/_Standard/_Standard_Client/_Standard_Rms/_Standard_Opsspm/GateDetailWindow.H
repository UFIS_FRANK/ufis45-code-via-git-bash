// GateDetailWindow.h : header file
//

#ifndef _GateDW_
#define _GateDW_

#include <cviewer.h>
#include <GateDetailViewer.h>


/////////////////////////////////////////////////////////////////////////////
// GateDetailWindow dialog

class GateDetailWindow : public CDialog
{
// Construction
public:
	GateDetailWindow(CWnd* pParent,const char *pcpAlid, BOOL bpArrival, BOOL bpDeparture,
		CTime opStartTime, CTime opEndTime);

// Dialog Data
	//{{AFX_DATA(GateDetailWindow)
	enum { IDD = IDD_GATE_DETAIL };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(GateDetailWindow)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void OnCancel();
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(GateDetailWindow)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnDestroy();
	afx_msg void OnSize(UINT nType, int cx, int cy);
    afx_msg LONG OnDragOver(UINT, LONG); 
    afx_msg LONG OnDrop(UINT, LONG); 
    afx_msg LONG OnTableLButtonDblclk(UINT wParam, LONG lParam);
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	static void GateDetailWindowCf(void *popInstance, int ipDDXType,void *vpDataPointer, CString &ropInstanceName);
protected:
	char cmAlid[255];
	char *pcmAlid;
	BOOL bmArrival;
	BOOL bmDeparture;
	CTime omStartTime;
	CTime omEndTime;

	int m_nDialogBarHeight;
	CTable omTable;
	GateDetailViewer omViewer;
	static GateDetailWindow *omCurrent;
// Drag-and-drop implementation
private:
	CCSDragDropCtrl m_DragDropTarget;
	long			imDropFlightUrno;
	long			imDropDemandUrno;
};

#endif // _GateDW_