#ifndef _CFLIGHTD_H_
#define _CFLIGHTD_H_


void ProcessFlightCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

#include <CCSCedaData.h>
#include <CCSPtrArray.h>
#include <CCSDefines.h>
#include <resource.h>

#define ID_NOT_RETURNFLIGHT 0
#define ID_ARR_RETURNFLIGHT 1
#define ID_DEP_RETURNFLIGHT 2

/////////////////////////////////////////////////////////////////////////////
// Record structure declaration

struct FlightDataStruct {

    long    Urno;           // Unique Record Number
	char    Fdat[10];       // Flight Date Format YYYYMMDD  19960715
	char    Flno[10];       // Complete Flight Number
    char    Alc2[3];        // Airline IATA-Code (2-letter code)
    char    Alc3[4];        // Airline 3-letter code
    char    Fltn[6];        // Flight Number
    char    Flns[2];        // Flight Suffix
    char    Adid[2];        // In/Out Flag (Arrival=A, Departure=D, Both/Rundflug=B)
	char	Org3[4];		// Origin
	char	Des3[4];		// Destination 3 letter
	char	Des4[5];		// Destination 4 letter
	char	Via3[4];		// Via
	int		Vian;			// Count of VIAs in Vial
	char	Vial[1025];		// List of Vias
	char	Gta1[6];		// Gate Arrival (1)
	char	Gta2[6];		// Gate Arrival (2)
	char	Gtd1[6];		// Gate Dep
	char	Gtd2[6];		// Double Gate Dep
    char    Regn[13];       // Registration
    char    Act3[4];        // Aircraft Type

	int     VEOF;           // Version Old First
	int     VEOC;           // Version Old Business
	int     VEOM;           // Version Old Economy
	int     VERF;           // Version First
	int     VERC;           // Version Business
	int     VERM;           // Version Economy
	int		Nose;			// Total number of seats
	char	Prmc[4];	    // PRM flag (Y/N)
	char	Prmu[34];	    // PRM status update user
	char	Prms[3];	    // PRM status (Y/N) Y for finalised, N for not finalised

    CTime   Stod;           // Scheduled Time of Departure (for Inbound ORIG/VIA)
    CTime   Stoa;           // Scheduled Time of Arrival (for Outbound DEST/VIA)
    CTime   Etod;           // Estimated Time of Departure (for Inbound ORIG/VIA)
    CTime   Etoa;           // Estimated Time of Arrival (for Outbound DEST/VIA)
    CTime   Tifd;           // Actual Time (best qualify field) of Departure (for Outbound DEST/VIA)
    CTime   Tifa;           // Actual Time (best qualify field) of Arrival (for Outbound DEST/VIA)
    CTime   Land;           // Touchdown (for Outbound DEST/VIA)
    CTime   Slot;           // Slot time
    CTime   Airb;           // Airborne (for Inbound at ORIG/VIA)
    CTime   Onbl;           // Onblock (for Outblock DEST/VIA)
    CTime   Onbe;           // Estimated Onblock
    CTime   Ofbl;           // Offblock (for Inbound ORIG/VIA)
	CTime   Tmoa;           // Ten Miles Out (for Outbound: Ten Minutes prior Departure)
    char    Tisd[2];        // Status of Actual Time
    char    Tisa[2];        // Status of Actual Time
    char    Styp[3];        // Service Type {M=Mail,C=Cargo,F=Ferry etc}
	char	Dcd1[3];		// Delay Code 1
	CTime	Dtd1;			// Delay Time 1
	char	Dcd2[3];		// Delay Code 2
	CTime	Dtd2;			// Delay Time 2
	CTime   Nxti;           // Next Info
	CTime   Iskd;           // Informative Scheduled Departure
	char	Ftyp[2];		// Type of flight {X=Cancelled,D=Delayed,N=NotOp ...}
	char	Psta[6];		// Position Arr
	char	Pstd[6];		// Position Dep
	int     Pax1;           // Pax checked in First
	int     Pax2;           // Pax checked in Economy
	int     Pax3;           // Pax checked in Business
    CTime   Lstu;           // Last Update timestamp
	long    Rkey;           // Urno of Corresponding Arrival/Departure
	char	Stev[2];		// keycode - user defined variable used for grouping flights
	long    Skey;           // Urno for seasonal flight plan
	char	Jfno[111];		// list of joint flight numbers eg "AAL6405  PR 5600" -> difference contemporaneous flight nums for a single aircraft
	int		Jcnt;			// number of jount flights in Jfno
	char	Act5[6];		// A/C-Type 5 letter code
	char	Ttyp[6];		// flight nature
	char	Hapx[6];		// handling agent passengers
	char	Hara[6];		// handling agent ramp
	int		Paxt;			// Pax total
	int		Cgot;			// Freight in tons
	int		Bagn;			// # of baggage	
	int		Mail;			// # of mail
	char	Blt1[6];		// baggage belt 1
	char	Baz1[4];		// ??
	int		Ddlf;			// deadload for flight in tons
	int		Bagw;			// total load -> added for Adr PRF 2573
	char	Flti[2];		//Flight ID (International/Schengen/Domestic)
	char    Csgn[9];        // Call Sign
    
	// Fields from table PAXCKI
    int     Bokf;           // Pax Booked First
    int     Bokc;           // Pax Booked Business
    int     Bokm;           // Pax Booked Economy
    CTime   Tifr;           // Actual Time (best qualify field) (FRA)
    char    Tist[2];        // Status of Actual Time (FRA)


    // Additional data not in the DB
    char	Fnum[15];		// Preformatted flight number
	int		ConflictType;	// Conflict Status
	int		ColorIndex;		// Index in global Color Table
	int		ConflictWeight;

	bool	GatIsPlanned[2];  // if true then flight bars will be half filled-in on the Gate Gantt
	bool	GatIsInFuture[2]; // if true then flights displayed on the gate gantt will be white
	bool	PstIsPlanned[2];  // if true then flight bars will be half filled-in on the pos Gantt
	bool	PstIsInFuture[2]; // if true then flights displayed on the pos gantt will be white

	bool	CicIsPlanned; // if true then flight bars will be half filled-in on the Cic Gantt
	bool	CicIsInFuture; // if true then flights displayed on the CIC gantt will be white
	bool	RegnIsPlanned; // if true then flight bars will be half filled-in on the regn Gantt
	bool	RegnIsInFuture; // if true then flights displayed on the regn gantt will be white

	int		IsDelayed;
	BOOL    IsTmo;
	BOOL	ConflictConfirmed;
	BOOL	IsDisplayed;
	BOOL	IsDisplayedInFlightSchedule;
	BOOL	IsShadoBarDisplayed;
	char	Line[10]; // counter - used only as temp value to sort flights, not updated with broadcasts
	int		ReturnFlightType; // this is only a temp value used for searching otherwise its value is not reliable

	char    Tga1[5]; //Arrival Terminal
	char    Tgd1[5]; //Departure Terminal

	FlightDataStruct(void) 
	{
		memset(this,0,sizeof(*this));
		Stod=TIMENULL;
		Stoa=TIMENULL;
		Etod=TIMENULL;
		Etoa=TIMENULL;
		Tifd=TIMENULL;
		Tifa=TIMENULL;
		Land=TIMENULL;
		Airb=TIMENULL;
		Onbl=TIMENULL;
		Onbe=TIMENULL;
		Ofbl=TIMENULL;
		Tmoa=TIMENULL;
		Dtd1=TIMENULL;
		Dtd2=TIMENULL;
		Nxti=TIMENULL;
		Iskd=TIMENULL;
		Lstu=TIMENULL;
		Tifr=TIMENULL;
		CicIsPlanned = false;
		GatIsPlanned[0] = false;
		GatIsPlanned[1] = false;
		PstIsPlanned[0] = false;
		PstIsPlanned[1] = false;
		CicIsInFuture = false;
		GatIsInFuture[0] = false;
		GatIsInFuture[1] = false;
		PstIsInFuture[0] = false;
		PstIsInFuture[1] = false;
		IsTmo=FALSE;
		ConflictConfirmed=FALSE;
		IsDisplayed=FALSE;
		IsDisplayedInFlightSchedule=FALSE;
		IsShadoBarDisplayed=FALSE;
	}

	FlightDataStruct& FlightDataStruct::operator=(const FlightDataStruct& rhs)
	{
		if (&rhs != this)
		{
			Urno = rhs.Urno;
			strcpy(Fdat, rhs.Fdat);
			strcpy(Flno, rhs.Flno);
			strcpy(Alc2, rhs.Alc2);
			strcpy(Alc3, rhs.Alc3);
			strcpy(Fltn, rhs.Fltn);
			strcpy(Flns, rhs.Flns);
			strcpy(Adid, rhs.Adid);
			strcpy(Org3, rhs.Org3);
			strcpy(Des3, rhs.Des3);
			strcpy(Des4, rhs.Des4);
			strcpy(Via3, rhs.Via3);
			Vian = rhs.Vian;
			strcpy(Vial, rhs.Vial);
			strcpy(Gta1, rhs.Gta1);
			strcpy(Gta2, rhs.Gta2);
			strcpy(Gtd1, rhs.Gtd1);
			strcpy(Gtd2, rhs.Gtd2);
			strcpy(Regn, rhs.Regn);
			strcpy(Act3, rhs.Act3);
			VEOF = rhs.VEOF;
			VEOC = rhs.VEOC;
			VEOM = rhs.VEOM;
			VERF = rhs.VERF;
			VERC = rhs.VERC;
			VERM = rhs.VERM;
			Nose = rhs.Nose;
			Stod = rhs.Stod;
			Stoa = rhs.Stoa;
			Etod = rhs.Etod;
			Etoa = rhs.Etoa;
			Tifd = rhs.Tifd;
			Tifa = rhs.Tifa;
			Land = rhs.Land;
			Slot = rhs.Slot;
			Airb = rhs.Airb;
			Onbl = rhs.Onbl;
			Onbe = rhs.Onbe;
			Ofbl = rhs.Ofbl;
			Tmoa = rhs.Tmoa;
			strcpy(Tisd, rhs.Tisd);
			strcpy(Tisa, rhs.Tisa);
			strcpy(Styp, rhs.Styp);
			strcpy(Dcd1, rhs.Dcd1);
			Dtd1 = rhs.Dtd1;
			strcpy(Dcd2, rhs.Dcd2);
			Dtd2 = rhs.Dtd2;
			Nxti = rhs.Nxti;
			Iskd = rhs.Iskd;
			strcpy(Ftyp, rhs.Ftyp);
			strcpy(Psta, rhs.Psta);
			strcpy(Pstd, rhs.Pstd);
			Pax1 = rhs.Pax1;
			Pax2 = rhs.Pax2;
			Pax3 = rhs.Pax3;
			Lstu = rhs.Lstu;
			Rkey = rhs.Rkey;
			strcpy(Stev, rhs.Stev);
			Skey = rhs.Skey;
			strcpy(Jfno, rhs.Jfno);
			Jcnt = rhs.Jcnt;
			strcpy(Act5, rhs.Act5);
			strcpy(Ttyp, rhs.Ttyp);
			strcpy(Hapx, rhs.Hapx);
			strcpy(Hara, rhs.Hara);
			Paxt = rhs.Paxt;
			Cgot = rhs.Cgot;
			Bagn = rhs.Bagn;
			Mail = rhs.Mail;
			strcpy(Blt1, rhs.Blt1);
			strcpy(Baz1, rhs.Baz1);
			Ddlf = rhs.Ddlf;
			Bagw = rhs.Bagw;
			strcpy(Flti, rhs.Flti);
			strcpy(Csgn, rhs.Csgn);			
			Bokf = rhs.Bokf;
			Bokc = rhs.Bokc;
			Bokm = rhs.Bokm;
			Tifr = rhs.Tifr;
			strcpy(Tist, rhs.Tist);
			strcpy(Fnum, rhs.Fnum);
			ConflictType = rhs.ConflictType;
			ColorIndex = rhs.ColorIndex;
			ConflictWeight = rhs.ConflictWeight;
			memcpy(GatIsPlanned, rhs.GatIsPlanned, 2);
			memcpy(GatIsInFuture, rhs.GatIsInFuture, 2);
			memcpy(GatIsInFuture, rhs.GatIsInFuture, 2);
			memcpy(GatIsInFuture, rhs.GatIsInFuture, 2);
			CicIsPlanned = rhs.CicIsPlanned;
			CicIsInFuture = rhs.CicIsInFuture;
			RegnIsPlanned = rhs.RegnIsPlanned;
			RegnIsInFuture = rhs.RegnIsInFuture;
			IsDelayed = rhs.IsDelayed;
			IsTmo = rhs.IsTmo;
			ConflictConfirmed = rhs.ConflictConfirmed;
			IsDisplayed = rhs.IsDisplayed;
			IsDisplayedInFlightSchedule = rhs.IsDisplayedInFlightSchedule;
			IsShadoBarDisplayed = rhs.IsShadoBarDisplayed;
			strcpy(Line, rhs.Line);
			strcpy(Tga1, rhs.Tga1);
			strcpy(Tgd1, rhs.Tgd1);
		}
		return *this;
	}
};
typedef struct FlightDataStruct FLIGHTDATA;
typedef struct FlightDataStruct *FLIGHTDATAPTR;

struct FlightUrnoStruct {

	long Urna;
	long Urnd;
	long Rkey;

	FlightUrnoStruct(void)
	{
		Urna = 0L;
		Urnd = 0L;
		Rkey = 0L;
	}
};



typedef struct FlightUrnoStruct FLIGHTURNO;
typedef struct FlightUrnoStruct *FLIGHTURNOPTR;




/////////////////////////////////////////////////////////////////////////////
// Class declaration

class CedaFlightData: public CCSCedaData
{
// Attributes
public:
    CMapPtrToPtr	omUrnoMap;
    CMapPtrToPtr	omRkeyMap;
	CMapStringToPtr omGateMap;
	CMapStringToPtr omPositionMap;
	CMapStringToPtr omAlioMap;
	CMapStringToPtr omRegnMap;
	CMapStringToPtr omJointFlightMap;
    CCSPtrArray<FLIGHTDATA> omData;
	CString GetTableName(void);
	CMapStringToPtr omPossibleKeycodes; // a select distinct stev from afttab
	int GetKeycodes(CStringArray &ropKeycodes);

private:
	char clFieldList[2048];

// Operations
public:
    CedaFlightData();
	~CedaFlightData();
	void ClearAll(void);

	void HandleMissingFlight(long lpFlightUrno);
	void ReadJobsAndDemandsForMissingFlight(long lpFlightUrno, long lpRotationUrno = 0L);
	bool ReadAllFlights(CTime opStartTime, CTime opEndTime, char *pcpSelection = NULL, int ipSendDdx = false);
	bool UpdateExistingFlight(FLIGHTDATA *prpFlight);
	bool UpdateExistingFlight(long lpFlur, char *pcpFieldList, char *pcpData);
	bool UpdateFlight(FLIGHTDATA *prpFlight, char *pcpFieldList, char *pcpData);
	bool AddFlight(FLIGHTDATA *prpFlight);
	void DeleteFlight(FLIGHTDATA *prpFlight, bool bpSendDdx =true);
	bool IsPassFtypFilter(const char *pcpFtyp);
	bool FlightInTimeframe(CTime opTifa, CTime opTifd);
    bool GetFlightsByGate(CCSPtrArray<FLIGHTDATA> &ropFlights, const char *pcpGate, bool bpReset = true);
    bool GetFlightsByPosition(CCSPtrArray<FLIGHTDATA> &ropFlights, const char *pcpPosition, bool bpReset = true);
	bool GetFlightsByAlio(CCSPtrArray<FLIGHTDATA> &ropFlights, const char *pcpAlio);
	bool GetFlightsByRegn(CCSPtrArray<FLIGHTDATA> &ropFlights, const char *pcpRegn);
	void GetFlightsByRegnAndTime(CCSPtrArray <FLIGHTDATA> &ropFlights,const char *pcpRegn,CTime opStartTime,CTime opEndTime);
	FLIGHTDATA  *CedaFlightData::GetFlightByAlcFltnFlns(const char *pcpAlc,const char *pcpFltn, 
		const char *pcpFlns,const char *pcpFdat, char cpAdid);
	bool GetFlightsByAlcFltnFlns(CCSPtrArray<FLIGHTDATA> &ropFlights, const char *pcpAlc,const char *pcpFltn, 
		const char *pcpFlns,const char *pcpFdat, char cpAdid, bool bpReset = true);
	bool GetFlightsWithoutGates(CCSPtrArray<FLIGHTDATA> &ropFlights);
	bool GetFlightsWithoutPositions(CCSPtrArray<FLIGHTDATA> &ropFlights);
	void GetThirdPartyFlights(CStringArray &ropThirdPartyFlights);

	bool GetFlightsByCallsign(CCSPtrArray<FLIGHTDATA> &ropFlights, const char *pcpCallsign ,const char *pcpFdat ,char cpAdid, bool bpReset = true);

	FLIGHTDATA  *GetFlightByUrno(long lpUrno);
	FLIGHTDATA  *GetRotationFlight(long lpFlightUrno, int ipReturnFlightType = ID_NOT_RETURNFLIGHT);
	FLIGHTDATA  *GetRotationFlight(FLIGHTDATA *prpFlight, int ipReturnFlightType = ID_NOT_RETURNFLIGHT);
	FLIGHTDATA  *GetRotationFlight(long lpUrno,long lpRkey);

	FLIGHTDATA  *GetFlightByFnum(const char *pcpFnum,const char *pcpFdat);

	bool FlightHasFullFieldList(FLIGHTDATA *prpFlight);

	void ProcessRacBroadcast(void *vpDataPointer);
	void ProcessInsertFlightBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
	void ProcessFlightBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
	void ProcessDeleteFlightBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
	void SaveGate(FLIGHTDATA  *prpFlight);
	void SavePosition(FLIGHTDATA  *prpFlight);

	CString MakeFnum(FLIGHTDATA *prpFlight);
	CString MakeAlc(FLIGHTDATA *prpFlight);
	BOOL	UseAlc3(FLIGHTDATA *prpFlight);

	void AddToJointFlightMap(FLIGHTDATA *prpFlight);
	bool IsJointFlight(FLIGHTDATA *prpFlight);

	void AddFlightToRkeyMap(FLIGHTDATA *prpFlight);
	void DeleteFlightFromRkeyMap(FLIGHTDATA *prpFlight);
	void UpdateMaps(FLIGHTDATA *prpOldFlight, FLIGHTDATA *prpNewFlight);
	void UpdateRkeyMap(FLIGHTDATA *prpOldFlight, FLIGHTDATA *prpNewFlight);
	bool AddFlightToGateMap(const char *pcpGate, FLIGHTDATA *prpFlight);
	bool DeleteFlightFromGateMap(const char *pcpGate, long lpFlightUrno);
	bool UpdateFlightForGate(const char *pcpOldGate, FLIGHTDATA *prpOldFlight,const char *pcpNewGate, FLIGHTDATA *prpNewFlight);
	bool AddFlightToPositionMap(const char *pcpPosition, FLIGHTDATA *prpFlight);
	bool DeleteFlightFromPositionMap(const char *pcpPosition, long lpFlightUrno);
	bool UpdateFlightForPosition(const char *pcpOldPosition, FLIGHTDATA *prpOldFlight,const char *pcpNewPosition, FLIGHTDATA *prpNewFlight);
	bool AddFlightToRegnMap(const char *pcpRegn, FLIGHTDATA *prpFlight);
	bool DeleteFlightFromRegnMap(const char *pcpRegn, long lpFlightUrno);
	bool UpdateFlightForRegn(const char *pcpOldRegn, FLIGHTDATA *prpOldFlight,const char *pcpNewRegn, FLIGHTDATA *prpNewFlight);
	bool IsTurnaround(FLIGHTDATA *prpFlight);
	bool IsDepartureOrBoth(FLIGHTDATA *prpFlight);
	bool IsDeparture(FLIGHTDATA *prpFlight);
	bool IsArrivalOrBoth(FLIGHTDATA *prpFlight);
	bool IsArrival(FLIGHTDATA *prpFlight);
	int GetRotationReturnFlightType(FLIGHTDATA *prpFlight1, FLIGHTDATA *prpFlight2, int ipReturnFlightType1 = ID_NOT_RETURNFLIGHT);
	bool IsReturnFlight(FLIGHTDATA *prpFlight);
	bool IsThirdPartyFlight(FLIGHTDATA *prpFlight);
	bool AdviceTimeSet(FLIGHTDATA *prpFlight);
	CString GetGate(FLIGHTDATA *prpFlight, int ipReturnFlightType = ID_NOT_RETURNFLIGHT);
	CString GetGate(long lpFlightUrno, int ipReturnFlightType = ID_NOT_RETURNFLIGHT);
	CString GetTerminal(FLIGHTDATA *prpFlight, int ipReturnFlightType = ID_NOT_RETURNFLIGHT);
	CString GetTerminal(long lpFlightUrno, int ipReturnFlightType = ID_NOT_RETURNFLIGHT);
	CString GetPosition(FLIGHTDATA *prpFlight, int ipReturnFlightType = ID_NOT_RETURNFLIGHT);
	CString GetPosition(long lpFlightUrno, int ipReturnFlightType = ID_NOT_RETURNFLIGHT);
	void ConvertDatesToUtc(FLIGHTDATA *prpFlight);
	void ConvertDatesToLocal(FLIGHTDATA *prpFlight);
	CString GetFlightNum(long lpUrno);
	CString GetFtypText(FLIGHTDATA *prpFlight);
	CString Dump(long lpUrno);
	CTime GetFlightTime(FLIGHTDATA *prpFlight);
	void GetFlightUrnos(CDWordArray &ropFlightUrnos);
	int GetAllFlightUrnos(CDWordArray &ropFlightUrnos);

	bool SavePrm(FLIGHTDATA *prpFlight);
	bool CreateDemands(long lpUrno);
	// Private methods
private:
    void PrepareFlightData(FLIGHTDATA *prpFlightData);

	BOOL IsViewLocked(); //igu
private:
	CStringArray	omUseAlc3;
};

#endif
