// DGateJob.h : header file
//
#ifndef _DGATEJOB_H_
#define _DGATEJOB_H_

/////////////////////////////////////////////////////////////////////////////
// DGateJob dialog

class DGateJob : public CDialog
{
// Construction
public:
	DGateJob(CWnd* pParent,CString opAloc,CString opAlid,CString opEmpName,CTime opStartTime);   // standard constructor

// Dialog Data
	//{{AFX_DATA(DGateJob)
	enum { IDD = IDD_DGATEJOB };
	int		    m_DurationButton;
	CString		m_Employee;
	CTime		m_Hour;
	CTime		m_Date;
	int			m_Min;
	int			m_Duration;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(DGateJob)
	public:
	virtual int DoModal(CTime& ropStartTime,int& ripDuration);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(DGateJob)
	virtual void OnOK();
	afx_msg void OnJgmin120();
	afx_msg void OnJgmin30();
	afx_msg void OnJgmin60();
	afx_msg void OnJgsonst();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	CString	omCaption;
};
#endif // _DGATEJOB_H_