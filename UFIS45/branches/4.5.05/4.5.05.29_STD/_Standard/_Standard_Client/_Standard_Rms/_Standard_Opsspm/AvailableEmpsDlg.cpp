// AvailableEmpsDlg.cpp : implementation file
//

#include <stdafx.h>
#include <opsspm.h>
#include <AvailableEmpsDlg.h>
#include <BasicData.h>
#include <Settings.h>
#include <CedaSprData.h>

#define IsTotallyInside(start1, end1, start2, end2)	((start1) >= (start2) && (end1) <= (end2))
#define IsOverlapped(Start1, end1, Start2, end2)    ((Start1) <= (end2) && (Start2) <= (end1))


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CAvailableEmpsDlg dialog
static int CompareDemandStartTime(const DEMANDDATA **pppDemand1, const DEMANDDATA **pppDemand2);

static int CompareDemandStartTime(const DEMANDDATA **pppDemand1, const DEMANDDATA **pppDemand2)
{
	int ilRc = (int)((**pppDemand1).Debe.GetTime() - (**pppDemand2).Debe.GetTime());
	if (ilRc == 0)
		ilRc = (int)((**pppDemand1).Urno - (**pppDemand2).Urno);
	return ilRc;
}

static void AvailableEmpsDlgCf(void *popInstance, int ipDDXType,void *vpDataPointer, CString &ropInstanceName);

static void AvailableEmpsDlgCf(void *popInstance, int ipDDXType,void *vpDataPointer, CString &ropInstanceName)
{
    CAvailableEmpsDlg *polDlg = (CAvailableEmpsDlg *)popInstance;

	if(ipDDXType == DEMANDGANTT_SELECT)
	{
		polDlg->ProcessSelectDemand((DEMANDDATA *)vpDataPointer);
	}
}

// See ogBasicData.GetAssignmentSuitabiltyWeighting() for a description of the weight
static int CompareWeight(const AVAILABLE_EMPS_LINEDATA **pppLine1, const AVAILABLE_EMPS_LINEDATA **pppLine2);
static int CompareWeight(const AVAILABLE_EMPS_LINEDATA **pppLine1, const AVAILABLE_EMPS_LINEDATA **pppLine2)
{
	return (int) (*pppLine1)->Weight - (*pppLine2)->Weight;
}

void CAvailableEmpsDlg::ProcessSelectDemand(DEMANDDATA *prpDemand)
{
	if(prpDemand != NULL && ogDemandData.GetDemandByUrno(prpDemand->Urno) != NULL && prmSelectedDemand->Urno != prpDemand->Urno)
	{
		AfxGetApp()->DoWaitCursor(1);
		if(SelectDemand(prpDemand))
		{
			RemoveShiftLines();
			//RemoveBreakJobLine(); // the break job was displayed for a different demand so remove it
			UpdateView();
		}
		AfxGetApp()->DoWaitCursor(-1);
	}
}


CAvailableEmpsDlg::CAvailableEmpsDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CAvailableEmpsDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CAvailableEmpsDlg)
	m_DisplayOnlyEmpsWithCorrectFunctionsAndPermits = FALSE;
	m_IgnoreBreaks = FALSE;
	m_IgnoreOverlap = FALSE;
	m_IgnoreOutsideShift = FALSE;
	//}}AFX_DATA_INIT

	pomEmpViewer = new AvailableEmpViewer(&omEmployees);
	pomEmpTable = new CTable;
	pomEmpTable->SetSelectMode(0);

	// display the same emps that are displayed on the StaffGantt
	pomEmpViewer->SetViewerKey("StaffDia");

	ogCCSDdx.Register(this, DEMANDGANTT_SELECT,CString("AVAILABLE_EMPS_DLG"), CString("Select Demand"), AvailableEmpsDlgCf);
	prmSelectedDemand = NULL;
}

CAvailableEmpsDlg::~CAvailableEmpsDlg()
{
	delete pomEmpTable;
	delete pomEmpViewer;
	omEmployees.DeleteAll();
	ogCCSDdx.UnRegister(this, NOTUSED);
}


void CAvailableEmpsDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAvailableEmpsDlg)
	DDX_Control(pDX, IDC_IGNOREBREAKS, m_IgnoreBreaksCtrl);
	DDX_Control(pDX, IDC_IGNOREOVERLAP, m_IgnoreOverlapCtrl);
	DDX_Control(pDX, IDC_IGNOREOUTSIDESHIFT, m_IgnoreOutsideShiftCtrl);
	DDX_Control(pDX, IDC_TEAM_ALLOC, m_TeamAllocCtrl);
	DDX_Control(pDX, IDC_FILTERFUNCPERMITS, m_DisplayOnlyEmpsWithCorrectFunctionsAndPermitsCtrl);
	DDX_Control(pDX, IDC_STATUS, m_StatusCtrl);
	DDX_Check(pDX, IDC_FILTERFUNCPERMITS, m_DisplayOnlyEmpsWithCorrectFunctionsAndPermits);
	DDX_Check(pDX, IDC_IGNOREBREAKS, m_IgnoreBreaks);
	DDX_Check(pDX, IDC_IGNOREOVERLAP, m_IgnoreOverlap);
	DDX_Check(pDX, IDC_IGNOREOUTSIDESHIFT, m_IgnoreOutsideShift);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CAvailableEmpsDlg, CDialog)
	//{{AFX_MSG_MAP(CAvailableEmpsDlg)
    ON_CBN_SELCHANGE(IDC_VIEW, UpdateView)
	ON_WM_CREATE()
	ON_BN_CLICKED(IDC_FILTERFUNCPERMITS, OnFilterfuncpermits)
    ON_MESSAGE(WM_TABLE_LBUTTONDBLCLK, OnTableLButtonDblclk)
    ON_MESSAGE(WM_TABLE_SELCHANGE, OnTableSelectionChanged)
	ON_BN_CLICKED(IDC_IGNOREBREAKS, OnIgnoreBreaksClicked)
	ON_BN_CLICKED(IDC_IGNOREOVERLAP, OnIgnoreOverlapClicked)
	ON_BN_CLICKED(IDC_IGNOREOUTSIDESHIFT, OnIgnoreOutsideShiftClicked)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CAvailableEmpsDlg message handlers

BOOL CAvailableEmpsDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	SetWindowPos(&wndTop, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE);	

	
	if(ogBasicData.bmShowMatchingEmpsByDefault)
	{
		m_DisplayOnlyEmpsWithCorrectFunctionsAndPermits = TRUE;
		UpdateData(FALSE);
	}

	if(ogBasicData.bmIgnorePausesByDefault)
	{
		m_IgnoreBreaks = TRUE;
		UpdateData(FALSE);
	}

	if(ogBasicData.bmIgnoreOverlapByDefault)
	{
		m_IgnoreOverlap = TRUE;
		UpdateData(FALSE);
	}

	if(ogBasicData.bmIgnoreOutsideShiftByDefault)
	{
		m_IgnoreOutsideShift = TRUE;
		UpdateData(FALSE);
	}

	// select the earliest demand by default
	if(omDemands.GetSize() > 0)
	{
		omDemands.Sort(CompareDemandStartTime);
		SelectDemand(&omDemands[0]);
	}

	m_DisplayOnlyEmpsWithCorrectFunctionsAndPermitsCtrl.SetWindowText(GetString(IDS_AVEMPS_FUNCSONLY));
	m_TeamAllocCtrl.SetWindowText(GetString(IDS_AVEMPS_TEAMALLOC));
	m_IgnoreBreaksCtrl.SetWindowText(GetString(IDS_AVEMPS_IGNOREBREAKS));
	m_IgnoreOutsideShiftCtrl.SetWindowText(GetString(IDS_IGNOREOUTSIDESHIFT));
	m_IgnoreOverlapCtrl.SetWindowText(GetString(IDS_IGNOREOVERLAP));

	// set the demands Gantt
	InitDemandsGantt();

	// set list of employees
	InitEmployeesTable();

	// set the list of views
	InitSelectViewComboBox();
	
	UpdateView(); // update the table of available emps

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CAvailableEmpsDlg::InitSelectViewComboBox(void)
{
	UpdateComboBox();
	ogBasicData.SetWindowStat("AVAILABLE_EMPS_DLG IDC_VIEW",GetDlgItem(IDC_VIEW));
}

void CAvailableEmpsDlg::InitEmployeesTable(void)
{
    CRect olDlgRect, olStatusBarRect, olNewRect;
    GetClientRect(&olDlgRect);
    olDlgRect.InflateRect(1, 1);
	olNewRect = olDlgRect;
	m_StatusCtrl.GetWindowRect(&olStatusBarRect);
	ScreenToClient(&olStatusBarRect);
	olNewRect.top = olStatusBarRect.bottom + 4;
	olNewRect.bottom += olStatusBarRect.bottom + 4;
    pomEmpTable->SetTableData(this, olNewRect.left, olNewRect.right, olNewRect.top, olNewRect.bottom);
	pomEmpViewer->Attach(pomEmpTable);
}

void CAvailableEmpsDlg::InitDemandsGantt(void)
{
	omDemandsViewer.prmSelectedDemand = prmSelectedDemand;

	CTime olStartTime = TIMENULL, olEndTime = TIMENULL;
	int ilCount = omDemands.GetSize(), ilLc;
	for(ilLc = 0; ilLc < ilCount; ilLc++)
	{
		if(olStartTime == TIMENULL || olStartTime > omDemands[ilLc].Debe)
		{
			olStartTime = omDemands[ilLc].Debe;
		}
		if(olEndTime == TIMENULL || olEndTime < omDemands[ilLc].Deen)
		{
			olEndTime = omDemands[ilLc].Deen;
		}
	}

	omTSStartTime = olStartTime - CTimeSpan(0, 2, 0, 0);
	omTSDuration = (olEndTime + CTimeSpan(0, 1, 0, 0)) - omTSStartTime;
    omTSInterval = CTimeSpan(0, 0, 10, 0);

	omDemandsViewer.RemoveAll();
	omDemandsViewer.SetDemandsPtr(&omDemands);
	omDemandsViewer.SetJobsPtr(&omJobs);
	omDemandsViewer.SetJodsPtr(&omJods);
	omDemandsViewer.SetNoDemandJobsPtr(&omNoDemandJobs);
	omDemandsViewer.CanSelectDemands(prmSelectedDemand);
	omDemandsViewer.Init();

	if (::IsWindow(omDemandsGantt.GetSafeHwnd()))
	{
	    for (int ilLineno = 0; ilLineno < omDemandsViewer.omFlightLines.GetSize(); ilLineno++)
		{
			omDemandsGantt.AddString("");
			omDemandsGantt.SetItemHeight(ilLineno, omDemandsGantt.GetLineHeight(ilLineno));
		}
		omDemandsGantt.InvalidateRect(NULL);
	}

    CRect olDlgRect, olStatusBarRect, olNewRect;
    GetClientRect(&olDlgRect);
    olDlgRect.InflateRect(1, 1);
	olNewRect = olDlgRect;
	m_StatusCtrl.GetWindowRect(&olStatusBarRect);
	ScreenToClient(&olStatusBarRect);
	olNewRect.bottom = olStatusBarRect.top - 4;
	olNewRect.top += 70;

    omTimeScale.EnableDisplayCurrentTime(FALSE);
    omTimeScale.Create(NULL, "TimeScale", WS_CHILD | WS_VISIBLE,
						CRect(olNewRect.left, olNewRect.top, olNewRect.right, olNewRect.top + 34),
						this, 0, NULL);
    omTimeScale.SetDisplayTimeFrame(omTSStartTime, omTSDuration, omTSInterval);

    omDemandsGantt.SetTimeScale(&omTimeScale);
    omDemandsGantt.SetViewer(&omDemandsViewer, 0 /* GroupNo */);
    omDemandsGantt.SetStatusBar(&m_StatusCtrl);
    omDemandsGantt.SetDisplayWindow(omTSStartTime, omTSStartTime + omTimeScale.GetDisplayDuration());
    omDemandsGantt.SetVerticalScaleWidth(-2);
    omDemandsGantt.SetFonts(&ogMSSansSerif_Regular_8, &ogSmallFonts_Regular_6);
	omDemandsGantt.SetGanttChartColors(NAVY, GRAY, YELLOW, GRAY);
    omDemandsGantt.Create(0, CRect(olNewRect.left, olNewRect.top + 34, olNewRect.right, olNewRect.bottom), this);
	omDemandsGantt.SetWindowPos(&wndTop, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOACTIVATE);

	omDemandsViewer.SetDisplayTimeframe(omTSStartTime, omTSStartTime + omTimeScale.GetDisplayDuration());

	ASSIGNCONFLICT_BARDATA *prlBkBar = omDemandsViewer.GetBkBarByUrno(prmSelectedDemand->Urno);
	if(prlBkBar != NULL)
	{
		omDemandsGantt.SetStatusText(prlBkBar->StatusText);
	}
	omDemandsViewer.Attach(this);
}

LONG CAvailableEmpsDlg::OnTableLButtonDblclk(UINT ipItem, LONG lpLParam)
{
	CListBox *prlListBox = pomEmpTable->GetCTableListBox();
	if(prlListBox != NULL)
	{
		int ilSelLine = prlListBox->GetCurSel ();
		if(ilSelLine >= 0)
		{
			OnOK();
		}
	}
	return 0L;
}

LONG CAvailableEmpsDlg::OnTableSelectionChanged(UINT ipItem, LONG lpLParam)
{
	AVAILABLE_EMPS_LINEDATA *prlLine = (AVAILABLE_EMPS_LINEDATA *)pomEmpTable->GetTextLineData(ipItem);
	if (prlLine != NULL)
	{
		//if(prlLine->OverlapsBreak)
		{
			//RemoveBreakJobLine();

			// get any break jobs that overlap with the selected demand
//			CCSPtrArray<JOBDATA> olJobs;
//			ogJobData.GetJobsByPoolJob(olJobs, prlLine->PoolJobUrno);
//			int ilJobCount = olJobs.GetSize();
//			for (int ilJobno = (ilJobCount-1); ilJobno >= 0; ilJobno--)
//			{
//				JOBDATA *prlJob = &olJobs[ilJobno];
//				if(strcmp(prlJob->Jtco,JOBBREAK) || !IsReallyOverlapped(prlJob->Acfr, prlJob->Acto, prmSelectedDemand->Debe, prmSelectedDemand->Deen))
//				{
//					olJobs.RemoveAt(ilJobno);
//				}
//			}
//			
//			int ilLine = omDemandsViewer.InsertBreakJobLine(prmSelectedDemand->Urno, olJobs);
//			if(ilLine!= -1)
//			{
//				omDemandsGantt.InsertString(ilLine,"");
//				omDemandsGantt.RepaintItemHeight(ilLine);
//			}
//			omDemandsGantt.InvalidateRect(NULL);

			RemoveShiftLines();
			JOBDATA *prlPJ = ogJobData.GetJobByUrno(prlLine->PoolJobUrno);
			if(prlPJ != NULL)
			{
				CUIntArray olNewLines;
				omDemandsViewer.InsertShiftLines(prmSelectedDemand->Urno, prlPJ->Shur, olNewLines);
				for(int ilL = 0; ilL < olNewLines.GetSize(); ilL++)
				{
					omDemandsGantt.InsertString(olNewLines[ilL],"");
					omDemandsGantt.RepaintItemHeight(olNewLines[ilL]);
				}
				omDemandsGantt.InvalidateRect(NULL);
			}
		}
//		else
//		{
//			RemoveBreakJobLine();
//		}
	}
	return 0L;
}

void CAvailableEmpsDlg::RemoveBreakJobLine()
{
	int ilLine = -1;
	if (::IsWindow(omDemandsGantt.GetSafeHwnd()))
	{
		if((ilLine = omDemandsViewer.RemoveBreakJobLine()) != -1)
		{
			int ilTopPos = omDemandsGantt.GetTopIndex();		

			if(omDemandsGantt.DeleteString(ilLine) != LB_ERR)
			{
				// prevent the listbox springing to the top
				if (ilTopPos < omDemandsGantt.GetCount())
					omDemandsGantt.SetTopIndex(ilTopPos);
			}
			omDemandsGantt.InvalidateRect(NULL);
		}
	}
}

void CAvailableEmpsDlg::RemoveShiftLines()
{
	int ilLine = -1;
	if (::IsWindow(omDemandsGantt.GetSafeHwnd()))
	{
		CUIntArray olLines;
		omDemandsViewer.RemoveShiftLines(olLines);
		int ilNumLines = olLines.GetSize();
		if(ilNumLines > 0)
		{
			int ilTopPos = omDemandsGantt.GetTopIndex();		

			for(int i = 0; i < ilNumLines; i++)
			{
				int ilLine = olLines[i];
				omDemandsGantt.DeleteString(ilLine);
			}

			// prevent the listbox springing to the top
			if (ilTopPos < omDemandsGantt.GetCount())
				omDemandsGantt.SetTopIndex(ilTopPos);
			omDemandsGantt.InvalidateRect(NULL);
		}
	}
}

void CAvailableEmpsDlg::OnOK() 
{
	CString olText;
	UpdateData(TRUE);
	omSelectedPoolJobUrnos.RemoveAll();

	if(prmSelectedDemand != NULL)
	{
		CListBox *prlListBox = pomEmpTable->GetCTableListBox();
		if(prlListBox != NULL)
		{
			int ilSelLine = prlListBox->GetCurSel ();
			if(ilSelLine >= 0)
			{
				AVAILABLE_EMPS_LINEDATA *prlLine = (AVAILABLE_EMPS_LINEDATA *) pomEmpTable->GetTextLineData(ilSelLine);
				if(prlLine != NULL)
				{
					omSelectedPoolJobUrnos.Add(prlLine->Urno);
					bmTeamAlloc = false;
					CDialog::OnOK();
				}
				else
				{
					MessageBox(GetString(IDS_AVAILEMPSSELLINEERR),GetString(IDS_AVAILEMPSCAPTION),MB_ICONERROR);
				}
			}
			else
			{
				MessageBox(GetString(IDS_AVAILEMPSSELLINEERR),GetString(IDS_AVAILEMPSCAPTION),MB_ICONERROR);
			}
		}
		else
		{
			MessageBox("The employee list box is NULL","Internal Error!",MB_ICONERROR);
		}
	}
	else
	{
		MessageBox(GetString(IDS_AVAILEMPSSELDEMERR),GetString(IDS_AVAILEMPSCAPTION),MB_ICONERROR);
	}
}

void CAvailableEmpsDlg::AddDemand(DEMANDDATA *prpDemand)
{
	if(prpDemand != NULL)
	{
		omDemands.Add(prpDemand);
	}
}

void CAvailableEmpsDlg::SetAllocUnit(const char *pcpAllocUnitType, const char *pcpAllocUnit)
{
	omAllocUnitType = pcpAllocUnitType;
	omAllocUnit = pcpAllocUnit;
}

// e.g. Registration name or flight name or single demand
void CAvailableEmpsDlg::SetCaptionObject(CString opCaptionObject)
{
	omCaptionObject = opCaptionObject;
}

bool CAvailableEmpsDlg::SelectDemand(DEMANDDATA *prpDemand)
{
	bool blChanged = false;
	if(prmSelectedDemand != prpDemand)
	{
		prmSelectedDemand = prpDemand;
		omDemandsViewer.prmSelectedDemand = prmSelectedDemand;
		GetAvailableEmployeesForDemand(prmSelectedDemand);
		blChanged = true;
	}

	return blChanged;
}

// if bpOnlyTestHowManyAvailable is true then returns how many emps are available but doesn't display them
int CAvailableEmpsDlg::GetAvailableEmployeesForDemand(DEMANDDATA *prpDemand, bool bpOnlyTestHowManyAvailable /* = false */)
{
	int ilNumEmpsAvailable = 0;
	omEmployees.DeleteAll();

	bool blIgnoreBreaks = bpOnlyTestHowManyAvailable ? true : (m_IgnoreBreaks ? TRUE : FALSE);
	bool blIgnoreOverlap = bpOnlyTestHowManyAvailable ? true : (m_IgnoreOverlap ? TRUE : FALSE);
	bool blIgnoreOutsideShift = bpOnlyTestHowManyAvailable ? true : (m_IgnoreOutsideShift ? TRUE : FALSE);

	if(prpDemand != NULL)
	{
		CTime olStartTime = prpDemand->Debe;
		CTime olEndTime = prpDemand->Deen;

		//CMapPrtToPtr olShiftsAlreadyAdded; // required if for some reason the emp has more than one pool job for a shift
		//void *pvlDummy;

		// select all pool jobs
		CCSPtrArray <JOBDATA> olPoolJobs;
		int ilPoolJobCount = ogJobData.GetJobsByType(JOBPOOL, olPoolJobs);

		for (int ilPJ = 0; ilPJ < ilPoolJobCount; ilPJ++)
		{
			// ignore pool jobs which is out of the range of time of the given bar
			JOBDATA *prlPoolJob = &olPoolJobs[ilPJ];

			// employee is marked as absent
			if(prlPoolJob->Infm)
				continue;

			// if the pool restrictions doesn't contain this alloc unit
			if(!omAllocUnit.IsEmpty() && !ogBasicData.IsInPool(omAllocUnitType,omAllocUnit,prlPoolJob->Alid))
				continue;

			bool blNotInsideShift = true;
			if(IsTotallyInside(olStartTime, olEndTime, prlPoolJob->Acfr, prlPoolJob->Acto) ||
				(blIgnoreOutsideShift && IsOverlapped(olStartTime, olEndTime, prlPoolJob->Acfr, prlPoolJob->Acto)))
				blNotInsideShift = false;

			if(blNotInsideShift)
				continue;

			// ignore invalid pool jobs (the link to other tables was losted)
			EMPDATA *prlEmp = ogEmpData.GetEmpByPeno((const char *)prlPoolJob->Peno);
			SHIFTDATA *prlShift = ogShiftData.GetShiftByUrno(prlPoolJob->Shur);
			//if (prlEmp == NULL || prlShift == NULL || olShiftsAlreadyAdded.Lookup((void *) prlShift->Urno, (void *&) pvlDummy))
			if (prlEmp == NULL || prlShift == NULL)
				continue;

			// if specified, clocked-out employees cannot be assigned new jobs
			if(ogSprData.bmNoChangesAllowedAfterClockout && ogSprData.ClockedOut(prlShift, olStartTime))
				continue;

			// check for a temporary absence
			CCSPtrArray<JOBDATA> olTAJobs;
			bool blTempAbsent = false;
			ogJobData.GetJobsByJour(olTAJobs,prlPoolJob->Urno);
			for(int ilTA = 0; ilTA < olTAJobs.GetSize(); ilTA++)
			{
				JOBDATA *prlTAJob = &olTAJobs[ilTA];
				if(!strcmp(prlTAJob->Jtco, JOBTEMPABSENCE) &&
					IsOverlapped(prlTAJob->Acfr, prlTAJob->Acto, olStartTime, olEndTime))
				{
					blTempAbsent = true;
					break;
				}
			}
			if(blTempAbsent)
				continue;

			//olShiftsAlreadyAdded.SetAt((void *) prlShift->Urno, (void *&) NULL);

			// we are sure that the user will not interest in any flight managers
			BOOL blIsFlightManager = ogBasicData.IsManager(prlShift->Efct);
			if (blIsFlightManager)
				continue;

			// Second step -- we have to find the largest period of time taht this guy
			// is available by extend the time from the given bar.
			bool blEmpIsAvailable = true;
			CTime olAvailableFrom = prlPoolJob->Acfr;
			CTime olAvailableTo = prlPoolJob->Acto;
			CTimeSpan  olBrkBuffer(0, 0, ogBasicData.imDefaultBreakBuffer, 0);
			CTime olLatestJobEnd;

			// We have to scan every jobs which associated with this pool job.
			CCSPtrArray<JOBDATA> olJobs;
			ogJobData.GetJobsByUstf(olJobs, prlPoolJob->Ustf);
			//ogJobData.GetJobsByPoolJob(olJobs, prlPoolJob->Urno);
			//ogJobData.GetJobsByPkno(olJobs,prlShift->Peno,"", JOBPOOL);
			int ilJobCount = olJobs.GetSize();
			for (int ilJobno = 0; ilJobno < ilJobCount; ilJobno++)
			{
				JOBDATA *prlJob = &olJobs[ilJobno];

				if(IgnoreJobType(prlJob->Jtco))
				{
					// pool jobs and delegations are not overlapping jobs
					continue;
				}

				if(!blIgnoreOverlap && (strcmp(prlJob->Jtco,JOBBREAK) || !blIgnoreBreaks)) // if this is not a break job or the ignoreBreak check box is not set...
				{
					//PRF5802: Job must be finished at least olBrkBuffer before break starts
					olLatestJobEnd = prlJob->Acfr - olBrkBuffer;
					if (IsReallyOverlapped(olLatestJobEnd, prlJob->Acto, olStartTime,olEndTime))
					{
						blEmpIsAvailable = false;
						break;
					}

					// otherwise, we are trying to continue our available time calculation
					// doing the left border test first
					if (IsTotallyInside(olLatestJobEnd, prlJob->Acto, olAvailableFrom,olStartTime))
						olAvailableFrom = prlJob->Acto;
					// now, doing the right border
					if (IsTotallyInside(olLatestJobEnd, prlJob->Acto,olEndTime, olAvailableTo))
						olAvailableTo = olLatestJobEnd;
				}
			}
			if (!blEmpIsAvailable)
				continue;	// he is not the staff we want

			if(!bpOnlyTestHowManyAvailable)
			{
				AddEmployee(prlPoolJob, prlEmp, prlShift);
			}
			ilNumEmpsAvailable++;
		}
	}


	//omEmployees.Sort(CompareWeight);

	return ilNumEmpsAvailable;
}

bool CAvailableEmpsDlg::IgnoreJobType(const char *pcpJobType)
{
	if(!strcmp(pcpJobType,JOBPOOL) || !strcmp(pcpJobType,JOBDETACH) || !strcmp(pcpJobType,JOBTEAMDELEGATION))
		return true;

	return false;
}

bool CAvailableEmpsDlg::HasOverlappingBreakJob(EMPDATA *prpEmp, DEMANDDATA *prpSelectedDemand)
{
	bool blHasOverlappingBreakJob = false;
	if(m_IgnoreBreaks)
	{
		CCSPtrArray<JOBDATA> olJobs;
		CTimeSpan  olBrkBuffer(0, 0, ogBasicData.imDefaultBreakBuffer, 0);
		ogJobData.GetJobsByUstf(olJobs, prpEmp->Urno);
		int ilNumJobs = olJobs.GetSize();
		for(int ilJ = 0; ilJ < ilNumJobs; ilJ++)
		{
			JOBDATA *prlJob = &olJobs[ilJ];
			//PRF5802: Check overlap -> Regard break buffer before break job 
			if(!strcmp(prlJob->Jtco,JOBBREAK) && IsReallyOverlapped(prlJob->Acfr-olBrkBuffer, prlJob->Acto, prpSelectedDemand->Debe, prpSelectedDemand->Deen))
			{
				blHasOverlappingBreakJob = true;
				break;
			}
		}
	}

	return blHasOverlappingBreakJob;
}

bool CAvailableEmpsDlg::HasOverlapConflict(EMPDATA *prpEmp, DEMANDDATA *prpSelectedDemand)
{
	bool blHasOverlapConflict = false;
	if(m_IgnoreOverlap)
	{
		CCSPtrArray<JOBDATA> olJobs;
		ogJobData.GetJobsByUstf(olJobs, prpEmp->Urno);
		int ilNumJobs = olJobs.GetSize();
		for(int ilJ = 0; ilJ < ilNumJobs; ilJ++)
		{
			JOBDATA *prlJob = &olJobs[ilJ];
			if(!IgnoreJobType(prlJob->Jtco) && strcmp(prlJob->Jtco,JOBBREAK) && 
				IsReallyOverlapped(prlJob->Acfr, prlJob->Acto, prpSelectedDemand->Debe, prpSelectedDemand->Deen))
			{
				blHasOverlapConflict = true;
				break;
			}
		}
	}

	return blHasOverlapConflict;
}

bool CAvailableEmpsDlg::HasOutsideShiftConflict(EMPDATA *prpEmp, DEMANDDATA *prpSelectedDemand)
{
	bool blHasOutsideShiftConflict = false;
	if(m_IgnoreOutsideShift)
	{
		CCSPtrArray<JOBDATA> olJobs;
		ogJobData.GetJobsByUstf(olJobs, prpEmp->Urno);
		int ilNumJobs = olJobs.GetSize();
		for(int ilJ = 0; ilJ < ilNumJobs; ilJ++)
		{
			JOBDATA *prlPoolJob = &olJobs[ilJ];
			if(!strcmp(prlPoolJob->Jtco,JOBPOOL))
			{
				if(IsTotallyInside(prpSelectedDemand->Debe, prpSelectedDemand->Deen, prlPoolJob->Acfr, prlPoolJob->Acto))
				{
					// pool job totally encompassing the demand has been found so no conflict
					blHasOutsideShiftConflict = false;
					break;
				}
				else if(IsOverlapped(prlPoolJob->Acfr, prlPoolJob->Acto, prpSelectedDemand->Debe, prpSelectedDemand->Deen))
				{
					// pool job overlapping the demand
					blHasOutsideShiftConflict = true;
				}
			}
		}
	}

	return blHasOutsideShiftConflict;
}

void CAvailableEmpsDlg::AddEmployee(JOBDATA *prpPoolJob, EMPDATA *prpEmp, SHIFTDATA *prpShift)
{
	// Third step -- if we reach this point we will add him to the array
	AVAILABLE_EMPS_LINEDATA rlEmpLine;

	rlEmpLine.Weight = ogBasicData.GetAssignmentSuitabiltyWeighting(prpPoolJob,prmSelectedDemand);
	if(!m_DisplayOnlyEmpsWithCorrectFunctionsAndPermits || rlEmpLine.Weight != EMP_NOT_MATCH_DEMAND)
	{
		rlEmpLine.Urno = prpPoolJob->Urno;
		rlEmpLine.OverlapsBreak = HasOverlappingBreakJob(prpEmp,prmSelectedDemand);
		rlEmpLine.OverlapConflict = HasOverlapConflict(prpEmp,prmSelectedDemand);
		rlEmpLine.OutsideShiftConflict = HasOutsideShiftConflict(prpEmp,prmSelectedDemand);
		strcpy(rlEmpLine.Tmid, prpShift->Egrp);
		strcpy(rlEmpLine.Sfca, prpShift->Sfca);
		rlEmpLine.PoolJobUrno = prpPoolJob->Urno;
		strcpy(rlEmpLine.Peno, prpEmp->Peno);
		CString olEmployeeName;
		olEmployeeName = ogEmpData.GetEmpName(prpEmp);
		strcpy(rlEmpLine.Lnam, olEmployeeName.Left(32));

		CString olNonEmpFunction = ogBasicData.GetNonEmployeeFunctionForPoolJob(prpPoolJob);
		strncpy(rlEmpLine.NonEmpFunction,olNonEmpFunction,MAXFUNCLEN);

		CString olEmpFunctions = ogBasicData.GetMultiFunctionText(prpEmp->Urno);
		strncpy(rlEmpLine.EmpFunctions,olEmpFunctions,MAXFUNCLEN);

		CStringArray olPermitsArray;
		CString olPermits;
		ogBasicData.GetPermitsForPoolJob(prpPoolJob, olPermitsArray);
		int ilNumPermits = olPermitsArray.GetSize();
		if(ilNumPermits > 0)
		{
			for(int ilPermit = 0; ilPermit < ilNumPermits; ilPermit++)
			{
				olPermits += olPermitsArray[ilPermit] + CString(",");
			}
			strncpy(rlEmpLine.EmpPermits,olPermits,MAXFUNCLEN);
		}

		rlEmpLine.Avfr = prpPoolJob->Acfr;
		rlEmpLine.Avto = prpPoolJob->Acto;

		strncpy(rlEmpLine.Pool,prpPoolJob->Alid,sizeof(rlEmpLine.Pool));
		rlEmpLine.Pool[sizeof(rlEmpLine.Pool) - 1] = '\0';

		omEmployees.New(rlEmpLine);
	}
}

// set the list of views
void CAvailableEmpsDlg::UpdateComboBox()
{
	// check if the view box is enabled and displayed depending on the status in BDPSSEC
	CComboBox *polCB = (CComboBox *) GetDlgItem(IDC_VIEW);
	if(polCB != NULL && !ogBasicData.IsHidden("AVAILABLE_EMPS_DLG IDC_VIEW"))
	{
		polCB->ResetContent();
		CStringArray olStrArr;
		pomEmpViewer->GetViews(olStrArr);
		for (int ilIndex = 0; ilIndex < olStrArr.GetSize(); ilIndex++)
		{
			polCB->AddString(olStrArr[ilIndex]);
		}
		CString olViewName = pomEmpViewer->GetViewName();
		if (olViewName.IsEmpty() == TRUE)
		{
			olViewName = ogCfgData.rmUserSetup.STCV;
		}

		ilIndex = polCB->FindString(-1,olViewName);
			
		if (ilIndex != CB_ERR)
		{
			polCB->SetCurSel(ilIndex);
		}
	}
}

// view selected (or initializing)
void CAvailableEmpsDlg::UpdateView() 
{
	char pclViewName[200];
	strcpy(pclViewName,GetString(IDS_STRINGDEFAULT));

	// check if the view box is enabled and displayed depending on the status in BDPSSEC
    CComboBox *polCB = (CComboBox *)GetDlgItem(IDC_VIEW);
	if(polCB != NULL && !ogBasicData.IsHidden("AVAILABLE_EMPS_DLG IDC_VIEW"))
	{
		polCB->GetLBText(polCB->GetCurSel(), pclViewName);
	}

	AfxGetApp()->DoWaitCursor(1);
	pomEmpViewer->UpdateView(pclViewName);

	CString olCaption;
	// "%s: %d Employees Available." 
	olCaption.Format("%s: %d %s",omCaptionObject,pomEmpViewer->GetLineCount(),GetString(IDS_AVAILEMPSCAPTION));
	SetWindowText(olCaption);
	AfxGetApp()->DoWaitCursor(-1);
}

int CAvailableEmpsDlg::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CDialog::OnCreate(lpCreateStruct) == -1)
		return -1;
	

	return 0;
}

void CAvailableEmpsDlg::OnFilterfuncpermits() 
{
	UpdateData(TRUE);
	ogBasicData.bmShowMatchingEmpsByDefault = (m_DisplayOnlyEmpsWithCorrectFunctionsAndPermits) ? true : false;
	AfxGetApp()->DoWaitCursor(1);
	GetAvailableEmployeesForDemand(prmSelectedDemand);
	UpdateView();
	AfxGetApp()->DoWaitCursor(-1);
}

void CAvailableEmpsDlg::OnIgnoreBreaksClicked() 
{
	UpdateData(TRUE);
	ogBasicData.bmIgnorePausesByDefault = (m_IgnoreBreaks) ? true : false;
	AfxGetApp()->DoWaitCursor(1);
//	RemoveBreakJobLine();
	RemoveShiftLines();
	GetAvailableEmployeesForDemand(prmSelectedDemand);
	UpdateView();
	AfxGetApp()->DoWaitCursor(-1);
}

void CAvailableEmpsDlg::OnIgnoreOverlapClicked() 
{
	UpdateData(TRUE);
	ogBasicData.bmIgnoreOverlapByDefault = (m_IgnoreOverlap) ? true : false;
	AfxGetApp()->DoWaitCursor(1);
//	RemoveBreakJobLine();
	RemoveShiftLines();
	GetAvailableEmployeesForDemand(prmSelectedDemand);
	UpdateView();
	AfxGetApp()->DoWaitCursor(-1);
}

void CAvailableEmpsDlg::OnIgnoreOutsideShiftClicked() 
{
	UpdateData(TRUE);
	ogBasicData.bmIgnoreOutsideShiftByDefault = (m_IgnoreOutsideShift) ? true : false;
	AfxGetApp()->DoWaitCursor(1);
//	RemoveBreakJobLine();
	RemoveShiftLines();
	GetAvailableEmployeesForDemand(prmSelectedDemand);
	UpdateView();
	AfxGetApp()->DoWaitCursor(-1);
}
