#if !defined(AFX_NAMECONFIG_H__E7292A74_804B_11D7_8237_00010215BFE5__INCLUDED_)
#define AFX_NAMECONFIG_H__E7292A74_804B_11D7_8237_00010215BFE5__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// NameConfig.h : header file
//
#include <NameConfigurationDlg.h>

/////////////////////////////////////////////////////////////////////////////
// CNameConfig view
#define NAMECONFIG_KEY "NameConfig"
#define NAMECONFIG_TEXT "Text"
#define NAMECONFIG_MAXNAMELEN 32

class CNameConfig
{
public:
	CNameConfig();
	virtual ~CNameConfig();

protected:
	NameConfigurationDlg *pomNameConfigurationDlg;
	CString omConfigString;
	bool bmInitialised;

public:
	bool Initialise();
	bool SetNameConfig();
	CString GetEmpName(char *pcpFinm, char *pcpLanm, char *pcpShnm, char *pcpPerc, char *pcpPeno);
};

extern CNameConfig ogNameConfig;

#endif // !defined(AFX_NAMECONFIG_H__E7292A74_804B_11D7_8237_00010215BFE5__INCLUDED_)
