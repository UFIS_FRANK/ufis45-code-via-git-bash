// GateTablePropertySheet.cpp : implementation file
//


// These following include files are project dependent
#include <stdafx.h>
#include <OpssPm.h>
#include <CCSGlobl.h>
#include <CCSCedaData.h>
#include <AllocData.h>
#include <CedaJobData.h>
#include <CedaDemandData.h>
#include <CedaJodData.h>
#include <CedaEmpData.h>
#include <CedaShiftData.h>
#include <CedaAltData.h>
#include <CedaGatData.h>

// These following include files are necessary
#include <cviewer.h>
#include <BasicData.h>
#include <FilterPage.h>
#include <GateTableSortPage.h>
//#include "ZeitPage.h"
#include <BasePropertySheet.h>
#include <GateTablePropertySheet.h>
#include <CedaFlightData.h>
#include <conflict.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


/////////////////////////////////////////////////////////////////////////////
// GateTablePropertySheet
//

//GateTablePropertySheet::GateTablePropertySheet(CWnd* pParentWnd,
//	CViewer *popViewer, UINT iSelectPage) : BasePropertySheet
//	(ID_SHEET_GATE_TABLE, pParentWnd, popViewer, iSelectPage)
GateTablePropertySheet::GateTablePropertySheet(CWnd* pParentWnd,
	CViewer *popViewer, UINT iSelectPage) : BasePropertySheet
	(GetString(IDS_STRING61591), pParentWnd, popViewer, iSelectPage)
{
	AddPage(&m_pageGate);
	AddPage(&m_pageAirline);
	AddPage(&m_pageSort);
//	AddPage(&m_pageZeit);

	// Change the caption in tab control of the PropertySheet
	//m_pageGate.SetCaption(ID_PAGE_FILTER_GATE);
	m_pageGate.SetCaption(GetString(IDS_STRING61596));

	//m_pageAirline.SetCaption(ID_PAGE_FILTER_AIRLINE);
	m_pageAirline.SetCaption(GetString(IDS_STRING61597));

	// Prepare possible values for each PropertyPage
//	ogBasicData.GetAllRanks(m_pageGate.omPossibleItems);
//	ogBasicData.GetAllShifts(m_pageAirline.omPossibleItems);
	
	/*
	CCSPtrArray <ALLOCUNIT> olGates;
	ogAllocData.GetUnitsByGroupType(ALLOCUNITTYPE_GATEGROUP,olGates);
	int ilNumGates = olGates.GetSize();
	for(int ilGate = 0; ilGate < ilNumGates; ilGate++)
	{
		ALLOCUNIT *prlGate = &olGates[ilGate];
		m_pageGate.omPossibleItems.Add(prlGate->Name);
	}
	*/

	//wny : 23/06/2011 
	//Get the Gate Name from basic data instead of static group
	ogGatData.GetAllValidGateNames(m_pageGate.omPossibleItems);

	// airline codes format ALC2/ALC3
	ogAltData.GetAlc2Alc3Strings(m_pageAirline.omPossibleItems);
}

void GateTablePropertySheet::LoadDataFromViewer()
{
	pomViewer->GetFilter("Gate", m_pageGate.omSelectedItems);
	pomViewer->GetFilter("Airline", m_pageAirline.omSelectedItems);
	pomViewer->GetSort(m_pageSort.omSortOrders);
	m_pageSort.m_Group = pomViewer->GetGroup()[0] - '0';
//	pomViewer->GetFilter("Zeit", m_pageZeit.omFilterValues);
}

void GateTablePropertySheet::SaveDataToViewer(CString opViewName,BOOL bpSaveToDb)
{
	pomViewer->SetFilter("Gate", m_pageGate.omSelectedItems);
	pomViewer->SetFilter("Airline", m_pageAirline.omSelectedItems);
	pomViewer->SetSort(m_pageSort.omSortOrders);
	pomViewer->SetGroup(CString(m_pageSort.m_Group + '0'));
//	pomViewer->SetFilter("Zeit", m_pageZeit.omFilterValues);
	if (bpSaveToDb == TRUE)
	{
		pomViewer->SafeDataToDB(opViewName);
	}
}

int GateTablePropertySheet::QueryForDiscardChanges()
{
	CStringArray olSelectedGates;
	CStringArray olSelectedAirlines;
	CStringArray olSortOrders;
	CString olGroupBy;
	CStringArray olSelectedTime;
	pomViewer->GetFilter("Gate", olSelectedGates);
	pomViewer->GetFilter("Airline", olSelectedAirlines);
	pomViewer->GetSort(olSortOrders);
	olGroupBy = pomViewer->GetGroup();
//	pomViewer->GetFilter("Zeit", olSelectedTime);

//	if (!IsIdentical(olSelectedGates, m_pageGate.omSelectedItems) ||
//		!IsIdentical(olSelectedAirlines, m_pageAirline.omSelectedItems) ||
//		!IsIdentical(olSortOrders, m_pageSort.omSortOrders) ||
//		olGroupBy != CString(m_pageSort.m_Group + '0') ||
//		!IsIdentical(olSelectedTime, m_pageZeit.omFilterValues))
	if (!IsIdentical(olSelectedGates, m_pageGate.omSelectedItems) ||
		!IsIdentical(olSelectedAirlines, m_pageAirline.omSelectedItems) ||
		!IsIdentical(olSortOrders, m_pageSort.omSortOrders) ||
		olGroupBy != CString(m_pageSort.m_Group + '0'))
	{
		// Discard changes ?
		return MessageBox(GetString(IDS_STRING61583), NULL, MB_ICONQUESTION | MB_OKCANCEL);
	}

	return IDOK;
}
