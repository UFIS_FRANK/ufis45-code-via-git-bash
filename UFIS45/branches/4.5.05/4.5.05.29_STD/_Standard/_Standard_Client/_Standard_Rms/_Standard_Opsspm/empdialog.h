// EmpDialog.h : header file
//
#ifndef  _EMPDIALOG_H
#define _EMPDIALOG_H

#include <CedaShiftTypeData.h>
#include <CedaOdaData.h>
#include <CedaSpfData.h>

class CEmpDialog : public CDialog
{
// Construction
public:
	CEmpDialog(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CEmpDialog)
	enum { IDD = IDD_EMPDIALOG };
	CEdit	m_CompleteCtrl;
	CEdit	m_AfterCtrl;
	CEdit	m_BeforeCtrl;
	CEdit	m_SfcaCtrl;
	CEdit	m_SbtoCtrl;
	CEdit	m_SbfrCtrl;
	CComboBox	m_RankCtrl;
	CComboBox	m_WorkGroupCtrl;
	CEdit	m_BreakDurationCtrl;
	CEdit	m_ActoTimeCtrl;
	CEdit	m_ActoDateCtrl;
	CEdit	m_AcfrTimeCtrl;
	CEdit	m_AcfrDateCtrl;
	CComboBox	m_Sub1Sub2Ctrl;
	CString	m_Rank;
	CString	m_WorkGroup;
	CString	m_Sub1Sub2;
	CString	m_Ddat;
	CString	m_Lnam;
	CString	m_Pkno;
	CString	m_Sxid;
	CString	m_Tel1;
	CString	m_Tel2;
	CString	m_Fnam;
	CString	m_Sfca;
	CTime	m_Acfr1;
	CTime	m_Acfr2;
	CTime	m_Acfr3;
	CTime	m_Acfr4;
	CTime	m_Acto1;
	CTime	m_Acto2;
	CTime	m_Acto3;
	CTime	m_Acto4;
	CString	m_Sfcs;
	CString m_Spco;
	CTime	m_BreakStart;
	CTime	m_BreakEnd;
	CTime	m_BreakDuration;
	CString m_Remark;
	CString m_Useu;
	CTime	m_Lstu;
	CString m_ClockOn;
	CString m_ClockOff;
	CString	m_Before;
	CString	m_After;
	CString	m_Complete;
	//}}AFX_DATA

	CString omOldAcfrDate,omOldAcfrTime,omOldActoDate,omOldActoTime,omOldBreakDuration,omOldSbfr,omOldSbto,omOldRank,omOldWorkGroup,omOldSfca, omOldBefore, omOldAfter, omOldComplete;
	bool bmChanged;


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CEmpDialog)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

private:
	void EnableWindow(UINT ipControlId, bool bpEnable);
	CTime omStartDate;
	void SetTitle(UINT ipControlId, UINT ipStringId);
	void SetFieldsForShift(void);
	void UpdateShiftForShiftCode(SHIFTTYPEDATA *prpShiftType);
	void UpdateShiftForAbsenceCode(ODADATA *prpOda);
	void SetSub1Sub2();
	void InitSub1Sub2ComboBox();
	void CheckOvertimeValue(UINT ipId);

public:
	SHIFTDATA omShift;
	void CEmpDialog_DDX_CCSTime( CDataExchange *pDX, int nIDC, CTime& tm );
	bool bmEmpIsAbsent;
	CCSPtrArray <SPFDATA> omSpfs;
	bool bmOnlyBaseFunctions;

// Implementation
protected:
	CWnd *pomParent;

	// Generated message map functions
	//{{AFX_MSG(CEmpDialog)
	virtual void OnCancel();
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg void OnSelectCodeButton();
	afx_msg void OnKillfocusAcfrdate2();
	afx_msg void OnKillfocusAcfrtime2();
	afx_msg void OnKillfocusActodate2();
	afx_msg void OnKillfocusActotime2();
	afx_msg void OnKillfocusBrdur();
	afx_msg void OnKillfocusSpfr();
	afx_msg void OnKillfocusSpto();
	afx_msg void OnSelchangeRank();
	afx_msg void OnSelchangeWorkGroup();
	afx_msg void OnChangeEZcode();
	afx_msg void OnChangeEZcode2();
	afx_msg void OnChangeEZcode3();
	afx_msg void OnKillfocusEZcode();
	afx_msg void OnKillfocusEZcode2();
	afx_msg void OnKillfocusEZcode3();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#endif

