// EquipmentViewer.h : header file
//

#ifndef __EQUIPMENTVIEWER__
#define __EQUIPMENTVIEWER__

#include <cviewer.h>
#include <ccsprint.h>
#include <CedaShiftData.h>
#include <CedaEmpData.h>
#include <CedaBlkData.h>
#include <CedaEquData.h>
#include <CedaValData.h>


struct EQUIPMENT_INDICATORDATA
{
	long DemandUrno;
	long JobUrno;
	CTime StartTime;
	CTime EndTime;
	COLORREF Color;
	BOOL IsArrival;

	EQUIPMENT_INDICATORDATA(void)
	{
		DemandUrno = -1;
		JobUrno	   = -1;
		IsArrival  = FALSE;
		Color	   = 0;
	}
};



struct EQUIPMENT_BARDATA
{
	CString StatusText;
    CString Text;
    CTime StartTime;
    CTime EndTime;
    int FrameType;
    int MarkerType;
    CBrush *MarkerBrush;
    int OverlapLevel;       // zero for the top-most level, each level lowered by 4 pixels
	CCSPtrArray<EQUIPMENT_INDICATORDATA> Indicators;
	bool OfflineBar;
	int FlightType;
	long JobUrno;
	long FlightUrno;
	bool DifferentFunction;

	EQUIPMENT_BARDATA(void)
	{
		FrameType		= -1;
		MarkerType		= -1;
		MarkerBrush		= NULL;
		OverlapLevel	= 0;
		JobUrno			= 0L;
		FlightUrno		= 0L;
		OfflineBar = true;
		DifferentFunction = false;
	}
};


struct EQUIPMENT_BKBARDATA
{
	enum	BKBARTYPE
	{
		NONE		= -1,
		BLOCKED		= 0,
		FASTLINK	= 1,
	};

	BKBARTYPE		Type;
	CString StatusText;
    CString Text;
	COLORREF TextColor;
    CTime StartTime;
    CTime EndTime;
    int FrameType;
    int MarkerType;
    CBrush *MarkerBrush;
    int OverlapLevel;       // zero for the top-most level, each level lowered by 4 pixels
	CCSPtrArray <EQUIPMENT_INDICATORDATA> Indicators;
	long JobUrno;
	CUIntArray JobUrnos; // Equ Fast Links can be made up of several jobs if they overlap
	bool OfflineBar;

	EQUIPMENT_BKBARDATA(void)
	{
		TextColor		= 0L;
		FrameType		= -1;
		MarkerType		= -1;
		MarkerBrush		= NULL;
		OverlapLevel	= 0;
		OfflineBar		= false;
	}

	EQUIPMENT_BKBARDATA& EQUIPMENT_BKBARDATA::operator=(const EQUIPMENT_BKBARDATA& rhs)
	{
		if (&rhs != this)
		{
			Type = rhs.Type;
			StatusText = rhs.StatusText;
			Text = rhs.Text;
			TextColor = rhs.TextColor;
			StartTime = rhs.StartTime;
			EndTime = rhs.EndTime;
			FrameType = rhs.FrameType;
			MarkerType = rhs.MarkerType;
			MarkerBrush = rhs.MarkerBrush;
			OverlapLevel = rhs.OverlapLevel ;
			this->Indicators.DeleteAll();
			for (int i = 0; i < rhs.Indicators.GetSize(); i++)
			{
				Indicators.NewAt(i,rhs.Indicators[i]);
			}
			JobUrno = rhs.JobUrno;

			for(i = 0; i < rhs.JobUrnos.GetSize(); i++)
			{
				JobUrnos.Add(rhs.JobUrnos[i]);
			}
		}		
		return *this;
	}

};

struct EQUIPMENT_LINEDATA 
{
	// standard data for lines in general Diagram's chart
    CString Text;
    CString StatusText;
    int MaxOverlapLevel;
	long EquUrno;
    CCSPtrArray<EQUIPMENT_BARDATA> Bars; // jobs
    CCSPtrArray<EQUIPMENT_BKBARDATA> BkBars; // fast links and not available time
    CCSPtrArray<int> TimeOrder;

	EQUIPMENT_LINEDATA(void)
	{
		MaxOverlapLevel = 0;
	}
};

struct EQUIPMENT_GROUPDATA 
{
	CString EquipmentAreaId;			// Equipment area's ID associated with this group
    CString Text;
    CCSPtrArray<EQUIPMENT_LINEDATA> Lines;
};

struct EQUIPMENT_SELECTION
{
	int imGroupno;
	int imLineno;
	int imBarno;

	EQUIPMENT_SELECTION(void)
	{
		imGroupno = -1;
		imLineno  = -1;
		imBarno	  = -1;
	}
};

enum enumEquipmentFlightType {EQUIPMENTBAR_ARRIVAL, EQUIPMENTBAR_DEPARTURE, EQUIPMENTBAR_TURNAROUND};

// Attention:
// Element "TimeOrder" in the struct "EQUIPMENT_LINEDATA".
//
// This array will maintain bars sorted by StartTime of each bar.
// You can see it the same way you see an array of index in the array EQUIPMENT_BARDATA[].
// For example, if there are five bars in the first overlapped group, and their
// overlap level are [3, 0, 4, 1, 2]. So the indexes to the EQUIPMENT_BARDATA[] sorted by
// the beginning of time should be [1, 3, 4, 0, 2]. You can see that the indexes
// in these two arrays are cross-linked together. Please notice that we define
// the overlap level to be the same number as the order of the bar when sorted
// by StartTime.
//
// However, I keep the relative offset in this array, not the absolute offset.
// The relative offset helps us separate each group of bar more efficiently.
// For example, using the same previous example, the relative offset of index
// to the array EQUIPMENT_BARDATA[] kept in this array would be [+1, +2, +3, -3, -2].
//
// For a given "i", the bar in the same overlap group will always have the
// same value of "i - Bars[i + TimeOrder[i]].OverlapLevel". This value will be the
// index of the first member in that group.
//
// Notes: This bar order array should be implemented with CCSArray <int>.


/////////////////////////////////////////////////////////////////////////////
// EquipmentDiagramViewer window

class EquipmentGantt;

class EquipmentDiagramViewer: public CViewer
{
	friend EquipmentGantt;

public:
    EquipmentDiagramViewer();
    ~EquipmentDiagramViewer();

	void Attach(CWnd *popAttachWnd);
	void ChangeViewTo(const char *pcpViewName, CTime opStartTime, CTime opEndTime);
	void ChangeViewTo(void);
	CString GetGroup();

private:
	static void EquipmentDiagramViewerCf(void *popInstance, int ipDDXType,void *vpDataPointer, CString &ropInstanceName);
	void PrepareGrouping();
	void PrepareFilter();
	BOOL IsPassFilter(const char *pcpEquipmentArea);
	BOOL IsPassFilter(long lpEquUrno, const CString& ropEquName = "");
	int CompareGroup(EQUIPMENT_GROUPDATA *prpGroup1, EQUIPMENT_GROUPDATA *prpGroup2);
	int CompareLine(EQUIPMENT_LINEDATA *prpLine1, EQUIPMENT_LINEDATA *prpLine2);

	void MakeGroupsAndLines();
	int MakeLine(int ipGroupno, EQUDATA *prpEqu);
	void MakeBars();
	void MakeBars(int ipGroupno, int ipLineno, long lpEquUrno);
	void MakeBar(int ipGroupno, int ipLineno, JOBDATA *prpJob);
	void MakeBackgroundBars();
	void MakeBkBars(int ipGroupno, int ipLineno);
	void MakeBlkBkBars(EQUIPMENT_LINEDATA *prpLine);
	EQUIPMENT_BKBARDATA* MakeBlkBkBar(EQUIPMENT_LINEDATA *prpLine,const CTime& ropFrom,const CTime& ropTo,const CString& ropText, long lpBlkUrno);
	void MakeFastLinkBkBars(EQUIPMENT_LINEDATA *prpLine);
	EQUIPMENT_BKBARDATA* MakeFastLinkBkBar(EQUIPMENT_LINEDATA *prpLine, CCSPtrArray <JOBDATA> &ropOverlappingJobs);
	CString FlightStatus(FLIGHTDATA *prpFlight);
	char GetInitial(const char *pcpName);

public:
	BOOL FindGroup(long lpEquUrno, int &ripGroupno);
	BOOL FindGroup(const char *pcpEquipmentAreaId, int &ripGroupno);
	BOOL FindGroupAndLine(long lpEquUrno, int &ripGroupno, int &ripLineno);
	BOOL FindBar(long lpUrno, int &ripGroupno, int &ripLineno, int &ripBarno);
	BOOL FindBkBar(long lpUrno, int &ripGroupno, int &ripLineno, int &ripBkBarno);
	BOOL FindLine(const char *pcpEquipmentId, int ipGroupno, int &ripLineno);

    int GetGroupCount();
    EQUIPMENT_GROUPDATA *GetGroup(int ipGroupno);
    CString GetGroupText(int ipGroupno);
    CString GetGroupTopScaleText(int ipGroupno);
    int GetLineCount(int ipGroupno);
    EQUIPMENT_LINEDATA *GetLine(int ipGroupno, int ipLineno);
    CString GetLineText(int ipGroupno, int ipLineno);
    CString GetLineStatusText(int ipGroupno, int ipLineno);
    int GetMaxOverlapLevel(int ipGroupno, int ipLineno);
    int GetVisualMaxOverlapLevel(int ipGroupno, int ipLineno);
    int GetBarCount(int ipGroupno, int ipLineno);
    int GetBkBarCount(int ipGroupno, int ipLineno);
    EQUIPMENT_BARDATA *GetBar(int ipGroupno, int ipLineno, int ipBarno);
    EQUIPMENT_BKBARDATA *GetBkBar(int ipGroupno, int ipLineno, int ipBarno);
    CString GetBarText(int ipGroupno, int ipLineno, int ipBarno);
    CString GetBarStatusText(int ipGroupno, int ipLineno, int ipBarno);
    CString GetBkBarText(int ipGroupno, int ipLineno, int ipBkBarno);
    CString GetBkBarStatusText(int ipGroupno, int ipLineno, int ipBkBarno);
	int GetIndicatorCount(int ipGroupno, int ipLineno, int ipBarno);
	EQUIPMENT_INDICATORDATA *GetIndicator(int ipGroupno, int ipLineno, int ipBarno,int ipIndicatorno);
//	CString GetTmo(int ipGroupno, int ipLineno);
	int GetGroupColorIndex(int ipGroupno,CTime opStart,CTime opEnd);
	void AllowUpdates(BOOL bpNoUpdatesNow);
	BOOL bmNoUpdatesNow;
    void DeleteAll();
    int CreateGroup(EQUIPMENT_GROUPDATA *prpGroup);
    void DeleteGroup(int ipGroupno);
    int CreateLine(int ipGroupno, EQUIPMENT_LINEDATA *prpLine);
    void DeleteLine(int ipGroupno, int ipLineno);
    int CreateBar(int ipGroupno, int ipLineno, EQUIPMENT_BARDATA *prpBar, BOOL bpFrontBar = TRUE);
    void DeleteBar(int ipGroupno, int ipLineno, int ipBarno);
    int CreateIndicator(int ipGroupno, int ipLineno, int ipBarno,
    	EQUIPMENT_INDICATORDATA *prpIndicator);
	void DeleteIndicator(int ipGroupno, int ipLineno, int ipBarno, int ipIndicator);

    int GetBarnoFromTime(int ipGroupno, int ipLineno, CTime opTime1, CTime opTime2, int ipOverlapLevel1, int ipOverlapLevel2);
	int GetBkBarnoFromTime(int ipGroupno, int ipLineno, CTime opTime1, CTime opTime2);

	CWnd *pomAttachWnd;

// Private helper routines
private:
	void GetOverlappedBarsFromTime(int ipGroupno, int ipLineno,
		CTime opTime1, CTime opTime2, CUIntArray &ropBarnoList);

// Attributes used for filtering condition
private:
	CBrush omBlockedBrush, omFastLinkBrush, omBkTextBrush;
	CMapStringToPtr omEquipmentGroupMap, omEquipmentTypeMap, omEquipmentNamesMap;
	CTime omStartTime;
	CTime omEndTime;
	CString omGroupBy;
	bool bmNotDisplayUnavailable; // if true, don't display equipment which is not available during the timeframe
	bool bmUseAllEquipmentTypes, bmUseAllEquipmentGroups, bmUseAllEquipmentNames;
// Attributes
private:
	CCSPtrArray<EQUIPMENT_GROUPDATA> omGroups;

// Methods which handle changes (from Data Distributor)
private:
	void UpdateEquLine(long lpEquUrno);
	void UpdateEquLine(EQUDATA *prpEqu);
	void DeleteEquLine(long lpEquUrno);
	void InsertEquLine(EQUDATA *prpEqu);

	void ProcessEquSelect(EQUDATA *prpEqu);
	void ProcessValChange(VALDATA *prpVal);
	void ProcessBlkChange(BLKDATA *prpBlk);
	void ProcessJobChange(JOBDATA * prpJob);
	void ProcessJobDelete(JOBDATA * prpJob);
	void DeleteBkBar(int ipGroupno, int ipLineno, int ipBkBarno);
	void DeleteBkBarsByJob(int ipGroupno, int ipLineno, long lpJobUrno);
	void ProcessEquInsert(EQUDATA *prpEqu);
	void ProcessEquUpdate(EQUDATA *prpEqu);
	void ProcessEquDelete(long lpEquUrno);

	// printing
public:
	void PrintDiagram(CPtrArray &opPtrArray);
	void GetDemandList(CUIntArray  &ropDemandList);

private:
	void PrintEquipmentDiagramHeader(int ipGroupno);
	CCSPrint *pomPrint;
	int	igFirstGroupOnPage;
	void PrintEquipmentArea(CPtrArray &opPtrArray,int ipGroupNo);
	void PrintPrepareLineData(int ipGroupNo,int ipLineno,CCSPtrArray<PRINTBARDATA> &ropPrintLine,
							  CCSPtrArray<PRINTBARDATA> &ropBkBars);
};

/////////////////////////////////////////////////////////////////////////////

#endif
