// SplitJobDlg.cpp : implementation file
//

#include <stdafx.h>
#include <opsspm.h>
#include <SplitJobDlg.h>
#include <CCSTime.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSplitJobDlg dialog


CSplitJobDlg::CSplitJobDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CSplitJobDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CSplitJobDlg)
	m_1FromDate = NULL;
	m_1FromTime = NULL;
	m_1ToDate = NULL;
	m_1ToTime = NULL;
	m_2FromDate = NULL;
	m_2FromTime = NULL;
	m_2ToDate = NULL;
	m_2ToTime = NULL;
	m_1FromTitle = _T("");
	m_1ToTitle = _T("");
	m_2FromTitle = _T("");
	m_2ToTitle = _T("");
	//}}AFX_DATA_INIT

	bmRc = false;

	om1From = TIMENULL;
	om1To = TIMENULL;
	om2From = TIMENULL;
	om2To = TIMENULL;
}


void CSplitJobDlg::DoDataExchange(CDataExchange* pDX)
{
	if(pDX->m_bSaveAndValidate == FALSE)
	{
		m_1FromDate = om1From;
		m_1FromTime = om1From;
		m_1ToDate = om1To;
		m_1ToTime = om1To;
		m_2FromDate = om2From;
		m_2FromTime = om2From;
		m_2ToDate = om2To;
		m_2ToTime = om2To;
	}

	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSplitJobDlg)
	DDX_Control(pDX, IDC_SECONDJOB, m_SecondJob);
	DDX_Control(pDX, IDC_FIRSTJOB, m_FirstJob);
	DDX_CCSddmmyy(pDX, IDC_1FROMDATE, m_1FromDate);
	DDX_CCSTime(pDX, IDC_1FROMTIME, m_1FromTime);
	DDX_CCSddmmyy(pDX, IDC_1TODATE, m_1ToDate);
	DDX_CCSTime(pDX, IDC_1TOTIME, m_1ToTime);
	DDX_CCSddmmyy(pDX, IDC_2FROMDATE, m_2FromDate);
	DDX_CCSTime(pDX, IDC_2FROMTIME, m_2FromTime);
	DDX_CCSddmmyy(pDX, IDC_2TODATE, m_2ToDate);
	DDX_CCSTime(pDX, IDC_2TOTIME, m_2ToTime);
	DDX_Text(pDX, IDC_1FROMTITLE, m_1FromTitle);
	DDX_Text(pDX, IDC_1TOTITLE, m_1ToTitle);
	DDX_Text(pDX, IDC_2FROMTITLE, m_2FromTitle);
	DDX_Text(pDX, IDC_2TOTITLE, m_2ToTitle);
	//}}AFX_DATA_MAP

	if(pDX->m_bSaveAndValidate == TRUE)
	{
		// Convert the date and time back
		om1From = CTime(m_1FromDate.GetYear(), m_1FromDate.GetMonth(), m_1FromDate.GetDay(), 
						m_1FromTime.GetHour(), m_1FromTime.GetMinute(),	m_1FromTime.GetSecond());
		om1To = CTime(m_1ToDate.GetYear(), m_1ToDate.GetMonth(),m_1ToDate.GetDay(),
						m_1ToTime.GetHour(), m_1ToTime.GetMinute(),	m_1ToTime.GetSecond());
		om2From = CTime(m_2FromDate.GetYear(), m_2FromDate.GetMonth(), m_2FromDate.GetDay(), 
						m_2FromTime.GetHour(), m_2FromTime.GetMinute(),	m_2FromTime.GetSecond());
		om2To = CTime(m_2ToDate.GetYear(), m_2ToDate.GetMonth(),m_2ToDate.GetDay(),
						m_2ToTime.GetHour(), m_2ToTime.GetMinute(),	m_2ToTime.GetSecond());
	}
}


BEGIN_MESSAGE_MAP(CSplitJobDlg, CDialog)
	//{{AFX_MSG_MAP(CSplitJobDlg)
	ON_EN_KILLFOCUS(IDC_1TODATE, OnKillfocus1todate)
	ON_EN_KILLFOCUS(IDC_1TOTIME, OnKillfocus1totime)
	ON_EN_KILLFOCUS(IDC_2FROMDATE, OnKillfocus2fromdate)
	ON_EN_KILLFOCUS(IDC_2FROMTIME, OnKillfocus2fromtime)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSplitJobDlg message handlers

BOOL CSplitJobDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	m_1FromTitle = GetString(IDS_SPLITDLG_FROM);
	m_1ToTitle = GetString(IDS_SPLITDLG_TO);
	m_2FromTitle = GetString(IDS_SPLITDLG_FROM);
	m_2ToTitle = GetString(IDS_SPLITDLG_TO);

	CString olTitle;
	olTitle = GetString(IDS_SPLITDLG_TITLE) + CString(" ") + omAdditionalText;
	SetWindowText(olTitle);
	m_FirstJob.SetWindowText(GetString(IDS_SPLITDLG_FIRSTJOB));
	m_SecondJob.SetWindowText(GetString(IDS_SPLITDLG_SECONDJOB));

	GetDlgItem(IDC_1FROMDATE)->EnableWindow(bmCanChangeStartTime);
	GetDlgItem(IDC_1FROMTIME)->EnableWindow(bmCanChangeStartTime);

	GetDlgItem(IDC_2TODATE)->EnableWindow(bmCanChangeEndTime);
	GetDlgItem(IDC_2TOTIME)->EnableWindow(bmCanChangeEndTime);

	UpdateData(FALSE);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CSplitJobDlg::OnOK() 
{
	if(UpdateData(TRUE))
	{
		if(om1From >= om1To)
		{
			MessageBox("The first job must start before it ends!","Split Job Error", MB_ICONERROR);
		}
		else if(om2From >= om2To)
		{
			MessageBox("The second job must start before it ends!","Split Job Error", MB_ICONERROR);
		}
		else if(om1To > om2From)
		{
			MessageBox("The second job must start after the first job ends!","Split Job Error", MB_ICONERROR);
		}
		else
		{
			bmRc = true;
			CDialog::OnOK();
		}
	}
}

void CSplitJobDlg::InitJobTimes(CString opAdditionalText, CTime opJobStart, CTime opJobEnd, 
								CTime opInitSplitTime /* = TIMENULL */, bool bpCanChangeStartTime /*=true*/, bool bpCanChangeEndTime /*=true*/)
{
	om1From = opJobStart;
	om2To = opJobEnd;
	if(opInitSplitTime != TIMENULL)
	{
//		om1To = opInitSplitTime - CTimeSpan(0,0,1,0);
		om1To = opInitSplitTime;
		om2From = opInitSplitTime;
	}
	else
	{
		om1To = opJobEnd;
		om2From = opJobStart;
	}

	omAdditionalText = opAdditionalText;
	bmCanChangeStartTime = bpCanChangeStartTime;
	bmCanChangeEndTime = bpCanChangeEndTime;
}

void CSplitJobDlg::GetJobTimes(CTime &ropJob1Start, CTime &ropJob1End, CTime &ropJob2Start, CTime &ropJob2End)
{
	ASSERT(bmRc);

	ropJob1Start = om1From;
	ropJob1End = om1To;
	ropJob2Start = om2From;
	ropJob2End = om2To;
}

void CSplitJobDlg::OnKillfocus1todate() 
{
	UpdateData(TRUE);
	//om2From = om1To + CTimeSpan(0,0,1,0);
	om2From = om1To;
	UpdateData(FALSE);
}

void CSplitJobDlg::OnKillfocus1totime() 
{
	UpdateData(TRUE);
	//om2From = om1To + CTimeSpan(0,0,1,0);
	om2From = om1To;
	UpdateData(FALSE);
}

void CSplitJobDlg::OnKillfocus2fromdate() 
{
	UpdateData(TRUE);
	//om1To = om2From - CTimeSpan(0,0,1,0);
	om1To = om2From;
	UpdateData(FALSE);
}

void CSplitJobDlg::OnKillfocus2fromtime() 
{
	UpdateData(TRUE);
	//om1To = om2From - CTimeSpan(0,0,1,0);
	om1To = om2From;
	UpdateData(FALSE);
}
