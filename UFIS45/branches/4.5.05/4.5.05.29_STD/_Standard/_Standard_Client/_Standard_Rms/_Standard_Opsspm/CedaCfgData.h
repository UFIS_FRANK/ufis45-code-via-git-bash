#ifndef _CCFGD_H_
#define _CCFGD_H_

#include <resource.h>
#include <CCSCedaData.h>
#include <CCSPtrArray.h>

/////////////////////////////////////////////////////////////////////////////
// Record structure declaration

struct RAW_VIEWDATA
{
	char Ckey[33];
	char Name[100];
	char Type[20];
	char Page[30];
	char Values[500];
	RAW_VIEWDATA()
	{ memset(this,'\0',sizeof(*this));}
};
struct VIEW_TEXTDATA
{
	CString Page;							// e.g. Rank, Pool, Shift
	CCSPtrArray <CString> omValues;					// e.g. Page=Shift: F1,F50,N1 etc.
};

struct VIEW_TYPEDATA
{
	CString Type;							// e.g. Filter, Group, Sort
	CCSPtrArray <VIEW_TEXTDATA> omTextData; // necessary only with filters
	CCSPtrArray<CString> omValues;					// values only relevant if Type != Filter
};
struct VIEW_VIEWNAMES
{
	CString ViewName;						// e.g. <Default>, Heute, Test etc.
	CCSPtrArray<VIEW_TYPEDATA> omTypeData;
};
struct VIEWDATA
{
	CString Ckey;							// e.g. Staffdia, CCI-Chart etc.
	CCSPtrArray<VIEW_VIEWNAMES> omNameData;
};
//struct CfgDataStruct {
struct CFGDATA {
    // Data fields from table CFGCKI for whatif-rows
    long    Urno;           // Unique Record Number of CFGCKI
    char    Appn[33];       // name of application
    char    Ctyp[33];       // Type of Row; in Whaif constant string "WHAT-IF"
	char    Ckey[33];		// Name of what-if row
    char	Vafr[15];       // Valid from
    char	Vato[15];       // Valid to
	char	Text[2001];		// Parameter string
	char	Pkno[33];		// Staff-/User-ID
    int	    IsChanged;		// Is changed flag

	CFGDATA(void) 
	{ memset(this,'\0',sizeof(*this));strcpy(Vafr, "19960101000000");
	  strcpy(Appn,pcgAppName);
	  strcpy(Vato, "20351231000000");IsChanged=DATA_UNCHANGED;}

};	

struct USERSETUPDATA
{
	enum CCAOPTIONS
	{
		NONE		= 0,
		READONLY	= 1,
		READWRITE	= 2,
	};

	CString MONS;	// Monitos:					1|2|3
	CString RESO;	// Resolution:				800x600|1024x768|1280y1024
	CString GACH;   // Gatechart monitor pos.:	L|M|R
	CString STCH;	// Staffchart monitor pos.:	L|M|R
	CString CCCH;	// CCI-Chart monitor pos.:	L|M|R
	CString RECH;	// REGN-Chart monitor pos.:	L|M|R
	CString FPTB;	// Flightplan monitor pos.:	L|M|R
	CString FJTB;	// Flight jobs table monitor pos.:	L|M|R
	CString STTB;	// Stafftable monitor pos.:	L|M|R
	CString GATB;   // Gatetable monitor pos.:	L|M|R
	CString CCTB;   // CCI-Table monitor pos.:	L|M|R
	CString RQTB;   // Requests monitor pos.:	L|M|R
	CString PKTB;	// Peaktable monitor pos.:	L|M|R
	CString CVTB;	// Coverage monitor pos.:	L|M|R
	CString VPTB;	// Prplantable monitor pos.:L|M|R
	CString CFTB;	// Conflicts monitor pos.:	L|M|R
	CString ATTB;	// Info monitor pos.:		L|M|R
	CString INFO;	// Info monitor pos.:		L|M|R
	CString LHHS;	// LH-Host monitor pos.:	L|M|R
	CString POCH;	// CCI-Chart monitor pos.:	L|M|R
	CString DETB;	// Demandtable monitor pos.:L|M|R
	CString CCDB;	// CCI-Demandtable mon.pos.:L|M|R
	CString FITB;	// Fl.ind. demand tbl pos.: L|M|R
	CString TACK;   // Turnaroundbars			J|N
	CString FBCK;	// Flightbars without demands J|N
	CString SBCK;	// Shadowbars				J|N
	CString STCV;   // View: Staffchart
	CString GACV;   // View: Gatechart
	CString EQCV;   // View: Equipmentchart
	CString PSCV;   // View: Pstchart
	CString RECV;   // View: Regnchart
	CString CCCV;   // View: CCI-Chart
	CString STLV;   // View: Stafftable
	CString FPLV;   // View: Flighttable
	CString GBLV;   // View: Gatetable
	CString FJTV;   // View: Flight Jobs Table
	CString CCBV;   // View: CCI-table
	CString RQSV;   // View: Requests
	CString PEAV;   // View: Peaktable
	CString VPLV;   // View: Preplantable
	CString CONV;   // View: Conflicts
	CString ATTV;   // View: "Achtung" (Attention)
	CString	DETV;	// View: Demandtable
	CString CCDV;	// View: Common checkin demand table
	CString PRMV;   // View: PRM Table
	CString FITV;	// View: Flight independent demand table
	CString BKPR;	// Automatic break print :	J
	CString BKPO;	// Automatic break print offset : nn
	CString EQCH;	// View: Equipment chart
	CString EQTB;	// View: Equipment Table

	struct 
	{
		CCAOPTIONS	CCA;
		BOOL		CIC_CHECK_LINK;
	}	OPTIONS;

	USERSETUPDATA(void)
	{
		MONS = "1";	RESO = "1024x768";
		GACH = "L"; EQTB = "L"; EQCH = "L";	STCH = "L";	CCCH = "L";	RECH = "L";	FPTB = "L";
		STTB = "L";	GATB = "L";	CCTB = "L";	RQTB = "L"; FJTB = "L";
		PKTB = "L";	CVTB = "L";	VPTB = "L";	CFTB = "L";
		ATTB = "L";	INFO = "L";	LHHS = "L";	TACK = "J";
		FBCK = "J";	SBCK = "J"; POCH = "L"; DETB = "L"; FITB = "L";
		CCDB = "L";
		STCV = "<Default>";	GACV = "<Default>";	EQCV = "<Default>"; RECV = "<Default>";
		CCCV = "<Default>";	STLV = "<Default>"; PSCV = "<Default>";
		FPLV = "<Default>";	GBLV = "<Default>";	FJTV = "<Default>";
		CCBV = "<Default>";	RQSV = "<Default>"; PRMV = "<Default>";
		PEAV = "<Default>";	VPLV = "<Default>";
		CONV = "<Default>";	ATTV = "<Default>"; DETV = "<Default>";
		FITV = "<Default>"; CCDV = "<Default>";
		BKPR = "N";
		BKPO = "30";

		OPTIONS.CCA	 = NONE;
		OPTIONS.CIC_CHECK_LINK = TRUE;
	}
};

typedef struct CFGDATA SETUPDATA;
//typedef struct CfgDataStruct CFGDATA;

// the broadcast CallBack function, has to be outside the CedaCfgData class
void ProcessCfgCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

/////////////////////////////////////////////////////////////////////////////
// Class declaration

class CedaCfgData: public CCSCedaData
{
// Types
public:

// Attributes
public:
    CCSPtrArray<CFGDATA>	omData;
    CCSPtrArray<CFGDATA>	omSetupData;
	CCSPtrArray<VIEWDATA>	omViews;
    CMapStringToPtr			omCkeyMap;
    CMapPtrToPtr			omUrnoMap;
    CMapPtrToPtr			omSetupUrnoMap;
	USERSETUPDATA			rmUserSetup;

	// this is the name of a dummy user for default values
	static const char* pomDefaultName;

// Operations
public:
    CedaCfgData();
	~CedaCfgData();

	CString GetTableName(void);
	CCSReturnCode CreateCfgRequest(const CFGDATA *prpCfgData);
	CCSReturnCode ReadCfgData();
	
	void GetDefaultConflictSetup(CStringArray &opLines);
	CCSReturnCode ChangeCfgData(CFGDATA *prpCfg);
	CCSReturnCode DeleteCfg(long lpUrno);
	CCSReturnCode AddCfg(CFGDATA *prpCfg);
	CCSReturnCode SaveCfg(CFGDATA *prpCfg);	
	void PrepareCfgData(CFGDATA *prpCfg);

	BOOL InterpretSetupString(CString popSetupString, USERSETUPDATA *prpSetupData);
    CCSReturnCode UpdateCfg(const CFGDATA *prpCfgData);    // used in PrePlanTable only
	void ProcessCfgBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
	CCSReturnCode InsertCfg(const CFGDATA *prpCfgData);

	CFGDATA  * GetCfgByUrno(long lpUrno);
	void SetCfgData();
	void MakeCurrentUser();
	void ClearAllViews();
	void PrepareViewData(CFGDATA *prpCfg);
	VIEWDATA * FindViewData(CFGDATA *prlCfg);
	VIEW_VIEWNAMES * FindViewNameData(RAW_VIEWDATA *prpRawData);
	VIEW_TYPEDATA * FindViewTypeData(RAW_VIEWDATA *prpRawData);
	VIEW_TEXTDATA * FindViewTextData(RAW_VIEWDATA *prpRawData);
	BOOL MakeRawViewData(CFGDATA *prpCfg, RAW_VIEWDATA * prpRawData);
	void MakeViewValues(char *pspValues, CCSPtrArray<CString> *popValues);
	CCSReturnCode UpdateViewForDiagram(CString opDiagram, VIEWDATA &prpView,CString opViewName);
	void DeleteViewFromDiagram(CString opDiagram, CString olView);

	// read conflict configuration data 
	CCSReturnCode ReadConflicts(const CString& opUserName,CStringArray &opLines);
	// save conflict configuration data
	CCSReturnCode SaveConflictData(const CString& opUserName,CFGDATA *prpCfg);
	// create cfg record from conflict string
	void		  CfgRecordFromConflict(const CString& popUsername,const CString& popRecord,CFGDATA& popCfgData);

	bool ResetToDefault(void);

	CFGDATA *GetCfgByCtypAndCkey(const char *pcpCtyp,  const char *pcpCkey);
	int GetCfgsByCtyp(const char *pcpCtyp, CCSPtrArray <CFGDATA> &ropCfgs);
	CString GetValueByCtypAndCkey(const char *pcpCtyp,  const char *pcpCkey, CString opDefaultValue = "");
	void DeleteByCtyp(const char *pcpCtyp);
	void DeleteByCtypAndCkey(const char *pcpCtyp, const char *pcpCkey);
	void CreateCfg(const char *pcpCtyp, const char *pcpCkey, const char *pcpText);

private:
    BOOL CfgExist(long lpUrno);
    CCSReturnCode InsertCfgRecord(const CFGDATA *prpCfgData);
    CCSReturnCode UpdateCfgRecord(const CFGDATA *prpCfgData);
};

extern CedaCfgData ogCfgData;
#endif
