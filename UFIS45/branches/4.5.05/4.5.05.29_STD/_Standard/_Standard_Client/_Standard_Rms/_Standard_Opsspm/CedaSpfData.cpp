//
// CedaSpfData.cpp - links emps (STFTAB) to their functions
//

#include <afxwin.h>
#include <ccsglobl.h>
#include <CCSPtrArray.h>
#include <CCSCedaData.h>
#include <CedaSpfData.h>
#include <BasicData.h>

static int CompareCode(const SPFDATA **pppSpf1, const SPFDATA **pppSpf2);
static int CompareCode(const SPFDATA **pppSpf1, const SPFDATA **pppSpf2)
{
	return (int)(strcmp((*pppSpf2)->Code,(*pppSpf1)->Code));
}

static int ComparePriority(const SPFDATA **pppSpf1, const SPFDATA **pppSpf2);
static int ComparePriority(const SPFDATA **pppSpf1, const SPFDATA **pppSpf2)
{
	return (int) (*pppSpf1)->Priority - (*pppSpf2)->Priority;
}

CedaSpfData::CedaSpfData()
{                  
    BEGIN_CEDARECINFO(SPFDATA, SpfDataRecInfo)
		FIELD_LONG(Urno,"URNO")
		FIELD_LONG(Surn,"SURN")
		FIELD_CHAR_TRIM(Code,"CODE")
		FIELD_DATE(Vpfr,"VPFR")
		FIELD_DATE(Vpto,"VPTO")
		FIELD_CHAR_TRIM(Prio,"PRIO")
    END_CEDARECINFO

    // Copy the record structure
    for (int i = 0; i < sizeof(SpfDataRecInfo)/sizeof(SpfDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&SpfDataRecInfo[i],sizeof(CEDARECINFO));
        omRecInfo.Add(prpCedaRecInfo);
	}

    // Initialize table names and field names
    strcpy(pcmTableName,"SPFTAB");
    pcmFieldList = "URNO,SURN,CODE,VPFR,VPTO,PRIO";
}
 
CedaSpfData::~CedaSpfData()
{
	TRACE("CedaSpfData::~CedaSpfData called\n");
	ClearAll();
}

void CedaSpfData::ClearAll()
{
	CMapPtrToPtr *polSingleMap;
	long llUrno;
	for(POSITION rlPos = omSurnMap.GetStartPosition(); rlPos != NULL; )
	{
		omSurnMap.GetNextAssoc(rlPos,(void *&) llUrno,(void *& )polSingleMap);
		polSingleMap->RemoveAll();
		delete polSingleMap;
	}
	omSurnMap.RemoveAll();
	omData.DeleteAll();
}

bool CedaSpfData::ReadSpfData()
{
	CCSReturnCode ilRc = RCSuccess;
	ClearAll();

	char pclCom[10] = "RT";
    char pclWhere[512] = "";
	CTime olDay = ogBasicData.GetTimeframeStart();

	sprintf ( pclWhere, "WHERE VPTO=' ' OR VPTO>'%s'", olDay.Format("%Y%m%d%H%M%S") );
    
	if ((ilRc = CedaAction2(pclCom, pclWhere)) != RCSuccess)
	{
		ogBasicData.LogCedaError("CedaSpfData Error",omLastErrorMessage,pclCom,pcmFieldList,pcmTableName,pclWhere);
	}
	else
	{
		SPFDATA rlSpfData;
		for (int ilLc = 0; ilRc == RCSuccess; ilLc++)
		{
			if ((ilRc = GetBufferRecord2(ilLc,&rlSpfData)) == RCSuccess)
			{
				AddSpfInternal(rlSpfData);
			}
		}
		ilRc = RCSuccess;
	}

	ogBasicData.Trace("CedaSpfData Read %d Records (Rec Size %d) %s",omData.GetSize(), sizeof(SPFDATA), pclWhere);
    return ilRc;
}

// fix - used to copy the contants of SPETAB instead of loading SPFTAB
// this is for Alitalia only !!!!!
// SPFTAB for Alitalia contains  CODE,ACT3,ACT5,ALC2,ALC3 which are combined to create
// functions specific to airplane types and airlines. Thes "formatted" function codes
// are then written to SPETAB so that SPETAB contains both functions (qualifiche traversali)
// and permits (qualifiche). This is not a problem when assigning because the function codes
// and permits are not the same.
void CedaSpfData::AddRecord(long lpUrno, long lpSurn, char *pcpCode, CTime opVpfr, CTime opVpto, char *pcpPrio)
{
	SPFDATA rlSpf;
	rlSpf.Urno = lpUrno;
	rlSpf.Surn = lpSurn;
	strcpy(rlSpf.Code,pcpCode);
	rlSpf.Vpfr = opVpfr;
	rlSpf.Vpto = opVpto;
	strcpy(rlSpf.Prio,pcpPrio);

	AddSpfInternal(rlSpf);
}


void CedaSpfData::AddSpfInternal(SPFDATA &rrpSpf)
{
	SPFDATA *prlSpf = new SPFDATA;
	*prlSpf = rrpSpf;

	prlSpf->Priority = atoi(prlSpf->Prio);
	if(prlSpf->Priority <= 0)
	{
		// an empty priority defaults to 2 so that it isn't selectd before a priority of 1
		prlSpf->Priority = 2;
	}

	omData.Add(prlSpf);
	CMapPtrToPtr *polSingleMap;
	if(omSurnMap.Lookup((void *) prlSpf->Surn, (void *&) polSingleMap))
	{
		polSingleMap->SetAt((void *)prlSpf->Urno,prlSpf);
	}
	else
	{
		polSingleMap = new CMapPtrToPtr;
		polSingleMap->SetAt((void *)prlSpf->Urno,prlSpf);
		omSurnMap.SetAt((void *)prlSpf->Surn,polSingleMap);
	}
}

CString CedaSpfData::GetFunctionBySurn(long lpSurn, CTime opTime /*=TIMENULL*/)
{
	CString olFunction = "";
	SPFDATA *prlHighestPrioSpfSoFar = NULL;

	CCSPtrArray <SPFDATA> olSpfs;
	GetSpfsBySurn(lpSurn, olSpfs, opTime);
	int ilNumSpfs = olSpfs.GetSize();
	for(int ilSpf = 0; ilSpf < ilNumSpfs; ilSpf++)
	{
		SPFDATA *prlSpf = &olSpfs[ilSpf];
		// for multi funtions return the function with the highest priority
		if(prlHighestPrioSpfSoFar == NULL || prlHighestPrioSpfSoFar->Priority > prlSpf->Priority)
		{
			olFunction = prlSpf->Code;
			prlHighestPrioSpfSoFar = prlSpf;
		}
	}
	olSpfs.RemoveAll();
	return olFunction;
}

int CedaSpfData::GetFunctionsBySurn(long lpSurn, CStringArray &ropFunctions, CTime opTime /*=TIMENULL*/)
{
	CString olFunction = "";
	CCSPtrArray <SPFDATA> olSpfs;
	GetSpfsBySurn(lpSurn, olSpfs, opTime);
	int ilNumSpfs = olSpfs.GetSize();
	for(int ilSpf = 0; ilSpf < ilNumSpfs; ilSpf++)
	{
		SPFDATA *prlSpf = &olSpfs[ilSpf];
		ropFunctions.Add(prlSpf->Code);
	}
	olSpfs.RemoveAll();
	return ropFunctions.GetSize();
}

void CedaSpfData::GetSpfsBySurn(long lpSurn, CCSPtrArray <SPFDATA> &ropSpfs, CTime opTime /*=TIMENULL*/, bool bpReset/*=true*/)
{
	if(bpReset)
	{
	    ropSpfs.RemoveAll();
	}

	CMapPtrToPtr *polSingleMap;
	long llUrno;
	SPFDATA *prlSpf;

	if(omSurnMap.Lookup((void *)lpSurn,(void *& )polSingleMap))
	{
		for(POSITION rlPos = polSingleMap->GetStartPosition(); rlPos != NULL; )
		{
			polSingleMap->GetNextAssoc(rlPos,(void *& ) llUrno,(void *& )prlSpf);
			if(opTime == TIMENULL || (prlSpf->Vpfr != TIMENULL && prlSpf->Vpfr <= opTime &&
				(prlSpf->Vpto == TIMENULL || prlSpf->Vpto > opTime)))
			{
				ropSpfs.Add(prlSpf);
			}
		}
	}

	ropSpfs.Sort(ComparePriority);
}


CString CedaSpfData::GetTableName(void)
{
	return CString(pcmTableName);
}