#if !defined(AFX_DEBUGCOUNTDLG_H__A4FFA583_B4D5_11D6_819C_00010215BFE5__INCLUDED_)
#define AFX_DEBUGCOUNTDLG_H__A4FFA583_B4D5_11D6_819C_00010215BFE5__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DebugCountDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CDebugCountDlg dialog

class CDebugCountDlg : public CDialog
{
// Construction
public:
	CDebugCountDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CDebugCountDlg)
	enum { IDD = IDD_DEBUGCOUNTDIALOG };
	CListBox	m_CountList;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDebugCountDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

public:
	void AddLine(CString opDate, CString opDrrCount, CString opJobCount, CString opDemCount, CString opAftCount);
private:
	CStringArray omLines;

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CDebugCountDlg)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DEBUGCOUNTDLG_H__A4FFA583_B4D5_11D6_819C_00010215BFE5__INCLUDED_)
