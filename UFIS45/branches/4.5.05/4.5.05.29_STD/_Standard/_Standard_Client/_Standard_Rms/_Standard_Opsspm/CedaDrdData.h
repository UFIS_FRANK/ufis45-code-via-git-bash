// CedaDrdData.cpp - Class for shift deviations - these can have their own funtions and permits
//
#ifndef _CEDADRDDATA_H_
#define _CEDADRDDATA_H_

#include <CCSCedaData.h>
#include <CCSPtrArray.h>

/////////////////////////////////////////////////////////////////////////////
// Record structure declaration

struct DrdDataStruct
{
	long	Urno;		// Unique Record Number
	char	Fctc[6];	// Function Code
	char	Prmc[91];	// Permits list format "XX|YY|ZZ|"
	long	Stfu;		// staff URNO - STFTAB.URNO
	char	Sday[9];	// shift day
	char	Drrn[2];	// record number for this sday
};

typedef struct DrdDataStruct DRDDATA;


/////////////////////////////////////////////////////////////////////////////
// Class declaration

class CedaDrdData: public CCSCedaData
{

// Attributes
public:
    CCSPtrArray <DRDDATA> omData;
	CMapPtrToPtr omUrnoMap;
	CMapPtrToPtr	omStfuMap;
	CString GetTableName(void);

// Operations
public:
	CedaDrdData();
	~CedaDrdData();

	bool ReadDrdData();
	CCSReturnCode DeleteDrdRecord(long lpUrno);
	DRDDATA* GetDrdByUrno(long lpUrno);
	CString GetFunctionsAndPermitsByUrno(long lpUrno, CStringArray &ropPermits);
	CString GetFunctionsByUrno(long lpUrno);
	void GetPermitsByUrno(long lpUrno, CStringArray &ropPermits);
	DRDDATA *GetDrdByShift(long lpStfu, char *pcpSday, char *pcpDrrn);
	void ProcessDrdBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

private:
	void AddDrdInternal(DRDDATA &rrpDrd);
	void DeleteDrdInternal(long lpUrno);
	void ClearAll();
};


extern CedaDrdData ogDrdData;
#endif _CEDADRDDATA_H_
