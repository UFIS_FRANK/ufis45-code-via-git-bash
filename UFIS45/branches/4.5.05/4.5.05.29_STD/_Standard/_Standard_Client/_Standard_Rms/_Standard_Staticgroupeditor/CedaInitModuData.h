// CedaInitModuData.h

#ifndef __CEDAINITMODUDATA__
#define __CEDAINITMODUDATA__
 
#include "stdafx.h"
#include "basicdata.h"

//--Class declaratino-------------------------------------------------------------------------------------------------------

class CedaInitModuData: public CCSCedaData
{
public:

// Operations
	CedaInitModuData();
	~CedaInitModuData();

	bool SendInitModu();
	CString GetInitModuTxt();
	CString GetInitModuTxtG();
	CString GetTemplatesString();

// Variaben
	char pcmInitModuFieldList[200];
	bool bmRegistered;
};

//---------------------------------------------------------------------------------------------------------

extern CedaInitModuData ogInitModuData;


#endif //__CEDAINITMODUDATA__
