// TreeDialog.cpp : implementation file
//

#include "stdafx.h"
#include "Grp.h"
#include "TreeDialog.h"

#include "CCSGlobl.h"
#include "CedaBasicData.h"
#include "InputBox.h"
#include "privlist.h"
#include "basicdata.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CTreeDialog dialog


CTreeDialog::CTreeDialog(CWnd* pParent /*=NULL*/)
	: CDialog(CTreeDialog::IDD, pParent)
{
	//{{AFX_DATA_INIT(CTreeDialog)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CTreeDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CTreeDialog)
	DDX_Control(pDX, IDC_TREE, m_Tree);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CTreeDialog, CDialog)
	//{{AFX_MSG_MAP(CTreeDialog)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CTreeDialog message handlers

BOOL CTreeDialog::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	CString olWhere;
	olWhere.Format("WHERE TABN='BSD%s' AND APPL='RMS'",pcgTableExt,pcgTableExt/*pcgHome*/);

	ogBCD.SetObject("GRN");
	ogBCD.Read("GRN",olWhere);
	ogBCD.SetSort("GRN","GRPN+",true);

	int ilDataCount = ogBCD.GetDataCount("GRN");
	CString clRow="";
	BOOL blRead;


	//*** ROOT ***
	TV_ITEM olTvItem;
	olTvItem.mask = TVIF_TEXT |/* TVIF_IMAGE |*/ TVIF_SELECTEDIMAGE | TVIF_PARAM;
	olTvItem.pszText = "Schichtgruppen";
	olTvItem.iImage = 0;

	TV_INSERTSTRUCT olInsert;
	olInsert.hParent =  TVI_ROOT;
	olInsert.hInsertAfter = TVI_FIRST;
	olInsert.item = olTvItem;

	HTREEITEM hRoot = m_Tree.InsertItem(&olInsert);
	HTREEITEM hChild;

	char cText[30];

	for(int i = 0; i < ilDataCount; i++)
	{
		RecordSet olRecord;
		blRead=ogBCD.GetRecord("GRN", i,  olRecord);
		clRow=olRecord.Values[ogBCD.GetFieldIndex("GRN","GRPN")];
		sprintf(cText,"%s",clRow);

		//*** CHILD 1 ***
		olTvItem.pszText = cText;
		//olTvItem.iImage = 1;
		olInsert.hParent = hRoot;
		olInsert.hInsertAfter = TVI_FIRST;
		olInsert.item = olTvItem;
		hChild = m_Tree.InsertItem(&olInsert);

		
		#ifdef _DEBUG	//  hag010605 
			afxDump << "clRow:" << clRow <<"\n";
		#endif
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}
