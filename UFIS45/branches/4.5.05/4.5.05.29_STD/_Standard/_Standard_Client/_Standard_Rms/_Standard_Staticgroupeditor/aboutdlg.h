#if !defined(AFX_ABOUTDLG_H__8613BBB1_16C7_11D5_9547_005004BCC8AF__INCLUDED_)
#define AFX_ABOUTDLG_H__8613BBB1_16C7_11D5_9547_005004BCC8AF__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// AboutDlg.h : header file
//

#define UFISVERSION "UFIS Version 4.4"

/////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog

class CAboutDlg : public CDialog
{
// Construction
public:
	CAboutDlg(CWnd* pParent = NULL);   // standard constructor
	
// Dialog Data
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	CStatic	m_ClassLibVersion;
	//}}AFX_DATA
	CString omGrpUrno;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAboutDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
public:
	int DoModal ( CString opActGrpUrno="" );

protected:

	// Generated message map functions
	//{{AFX_MSG(CAboutDlg)
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	afx_msg void OnAboutGroup();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////
// InfoDlg dialog

class InfoDlg : public CDialog
{
// Construction
public:
	InfoDlg(CString opRueUrno, CWnd* pParent = NULL);   // standard constructor
	~InfoDlg();

// Dialog Data
	//{{AFX_DATA(InfoDlg)
	enum { IDD = IDD_INFO_DLG };
	CStatic	m_CdatEdit;
	CStatic	m_LstuEdit;
	CStatic	m_UseuEdit;
	CStatic	m_UsecEdit;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(InfoDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	
	void Create();
	void SetStaticTexts();
	// Generated message map functions
	//{{AFX_MSG(InfoDlg)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

// Attributes
CWnd *pomParent;
CString omGrpUrno;

};

CString DBStringToDateString(CString opStr);

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ABOUTDLG_H__8613BBB1_16C7_11D5_9547_005004BCC8AF__INCLUDED_)
