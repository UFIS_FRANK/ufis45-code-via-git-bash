// Grp.cpp : Defines the class behaviors for the application.
//
#include "CCSGlobl.h"
#include "stdafx.h"

#include "Grp.h"
#include "ButtonListDlg.h"
#include "InitialLoadDlg.h"
#include "LoginDlg.h"
#include "registerdlg.h"
#include "BasicData.h"
#include "GroupsDialog.h"
#include "helperwnd.h"
#include "selectappl.h"
#include "privlist.h"
#include "CedaInitModuData.h"
#include "versioninfo.h"
#include <AatLogin.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif



/////////////////////////////////////////////////////////////////////////////
// CGrpApp

BEGIN_MESSAGE_MAP(CGrpApp, CWinApp)
	//{{AFX_MSG_MAP(CGrpApp)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG
	ON_COMMAND(ID_HELP, CWinApp::OnHelp)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CGrpApp construction

CGrpApp::CGrpApp() 
{
	// TODO: add construction code here,
	polHelperWindow = 0;
	// Place all significant initialization in InitInstance
}

CGrpApp::~CGrpApp()
{
	TRACE("CCSPrjApp::~CCSPrjApp\n");
	delete CGUILng::TheOne();
	ogGUILng = 0;
	DeleteBrushes();
}

/////////////////////////////////////////////////////////////////////////////
// The one and only CGrpApp object

CGrpApp theApp;

/////////////////////////////////////////////////////////////////////////////
// CGrpApp initialization

BOOL CGrpApp::InitInstance()
{
	// Initialize OLE libraries
	if (!AfxOleInit())
	{
		AfxMessageBox(IDP_OLE_INIT_FAILED);
		return FALSE;
	}

	AfxEnableControlContainer();

	#ifdef _AFXDLL
		Enable3dControls();			// Call this when using MFC in a shared DLL
	#else
		Enable3dControlsStatic();	// Call this when linking to MFC statically
	#endif

	CWnd *popPrevInstWnd = CWnd::FindWindow ( 0, pcgAppName );

	if ( popPrevInstWnd )
	{
		if (popPrevInstWnd->m_hWnd && IsWindow(popPrevInstWnd->m_hWnd) )
		{
			//ShowWindow(popPrevInstWnd->m_hWnd, SW_SHOWNORMAL );
			//SetForegroundWindow (popPrevInstWnd->m_hWnd);
			PostMessage (popPrevInstWnd->m_hWnd, WM_ACTIVATEAPP, TRUE, 0 );
		}
		return FALSE;
	}
	unsigned long flags;
	flags = WS_MINIMIZE|WS_DISABLED|WS_POPUP;
	BOOL ok ;

	//  Unsichtbares Hilfsfenster anlegen, um doppelten Programmstart zu
	//  vermeiden, solange MainFrame noch nicht zu sehen ist
	polHelperWindow = new CHelperWnd();
	ok = polHelperWindow->CreateEx ( 0, "Static", pcgAppName, flags, 0, 0, 1 ,1, 0, 0, 0 );

	/////////////////////////////////////////////////////////////////////////////
	// Standard CCS-Initialization
	/////////////////////////////////////////////////////////////////////////////

	//*** INIT STINGRAY GRID ***
	GXInit();

	// Standard CCS-Fonts and Brushes initialization
    InitFont();
	CreateBrushes();

	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	//--- initialize CEDA

	char pclConfigPath[142];
	char pclHomeAirport[256];

	if (getenv("CEDA") != NULL)
	{
		strcpy(pclConfigPath, getenv("CEDA"));
	}
	else
	{
		strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
	}
	

	char pclUseBcProxy[256];
	GetPrivateProfileString("GLOBAL", "BCEVENTTYPE", "BCSERV",pclUseBcProxy, sizeof pclUseBcProxy, pclConfigPath);

	//MWO/RRO 25.03.03
	if (stricmp(pclUseBcProxy,"BCPROXY") == 0) 
	{
		ogBcHandle.UseBcProxy();
		ogBcHandle.StopBc();
	}
	else if (stricmp(pclUseBcProxy,"BCCOM") == 0)
	{
		ogBcHandle.UseBcCom();
		ogBcHandle.StopBc();
	}

	//--- path to logfile for ccslog
	char pclTmp[128], pclDBParam[11];
	CString olErrorTxt;

	GetPrivateProfileString("GRP", "LOGFILE", CCSLog::GetUfisSystemPath("\\GRP.LOG"), pclTmp, sizeof(pclTmp), pclConfigPath);
	ogLog.Load(pclTmp);

	GetPrivateProfileString("GLOBAL", "TABLEEXTENSION", "TAB", pcgTableExt, sizeof(pcgTableExt), pclConfigPath);
	GetPrivateProfileString("GLOBAL", "HOMEAIRPORT", "TAB", pclHomeAirport, sizeof(pclHomeAirport), pclConfigPath);

	strcpy(CCSCedaData::pcmTableExt, pcgTableExt);
	strcpy(CCSCedaData::pcmHomeAirport, pclHomeAirport);
	strcpy(CCSCedaData::pcmApplName, ogAppName);

	VersionInfo olInfo;
	VersionInfo::GetVersionInfo(NULL,olInfo);
	CCSCedaData::bmVersCheck = true;
	strcpy(CCSCedaData::pcmVersion, olInfo.omFileVersion);
	strcpy(CCSCedaData::pcmInternalBuild, olInfo.omPrivateBuild);

	ogAppName = "GRP";
	ogCommHandler.SetAppName(ogAppName);
    if (ogCommHandler.Initialize() != true)  // connection error?
    {
        AfxMessageBox(CString("CedaCom:\n") + ogCommHandler.LastError());
        return FALSE;
    }

	strcpy(CCSCedaData::pcmReqId, ogCommHandler.pcmReqId);

	ogBCD.SetTableExtension(pcgTableExt);

	// *** Register your broadcasts here
	//ogBcHandle.AddTableCommand(CString("AFTHAJ"), CString("ISF"), BC_FLIGHT_CHANGE, true);

	GetPrivateProfileString("GLOBAL", "LANGUAGE", "", pclTmp, sizeof(pclTmp), 
							pclConfigPath);
   	GetPrivateProfileString(ogAppName, "DBParam", "0", pclDBParam, 
							sizeof pclDBParam, pclConfigPath);
	bool blLangInitOk = ogGUILng->MemberInit(&olErrorTxt, "XXX", "GRP", 
										pclHomeAirport, pclTmp, pclDBParam );

	//CButtonListDlg dlg(NULL);

	//*** CHECK THE COMMAND LINE ***
	//*** IF THE APPLICATION IS CALLED BY ANOTHER, KICK THE LOGIN DIALOG TO HELL ***

	CStringArray ogCmdLineArgsArray;
	ogCmdLineArgsArray.RemoveAll();
	CString olCmdLine =  LPCTSTR(m_lpCmdLine);
	CString olDmy;


	ogCallingAppName ="EMPTY";

	bIntern = FALSE;

#if	0

	if(olCmdLine.GetLength() != 0)
	{
		int ilParamCount = ExtractItemList(olCmdLine,&ogCmdLineArgsArray, ' ');

		//*** THE ORDER SHOULD BE APPNAME USER PASSWORD ***
		/*
		if ( ilParamCount >= 1 )		
			ogCallingAppName = ogCmdLineArgsArray.GetAt(0);

		if ( ilParamCount >= 2 )		
			sprintf(pcgUser,"%s",ogCmdLineArgsArray.GetAt(1));
		if ( ilParamCount >= 3 )		
			sprintf(pcgPasswd,"%s",ogCmdLineArgsArray.GetAt(2));

		if ( ilParamCount >= 4 )		
			dlg.omStartTpl = ogCmdLineArgsArray.GetAt(3);
		*/
		if ( __argc > 1 )
			ogCallingAppName = __targv[1];
		if ( __argc > 2 )
			sprintf(pcgUser,"%s", __targv[2] );
		if ( __argc > 3 )
			sprintf(pcgPasswd,"%s", __targv[3] );
		if ( __argc > 4 )
			dlg.omStartTpl = __targv[4];

		if (ogCallingAppName=="REGELWERK")
			ogCallingAppName ="RULE_AFT";
		/*	Testausgabe	
		CString olOutput;
	
		olOutput.Format ( "Appl.: %s\nUser: %s\nPassword: %s", 
						  ogCallingAppName,pcgUser, pcgPasswd );
		AfxMessageBox ( olOutput,MB_OK);
		*/
		BOOL blRc=FALSE;
		if ( ilParamCount >= 3 )
		{
			blRc = ogPrivList.Login(pcgTableExt,pcgUser,pcgPasswd,ogAppName);
			if (!blRc)
			{
				//*** CANCEL: CANNOT LOGIN ***
				AfxMessageBox(ogPrivList.omErrorMessage,MB_OK);
				//return FALSE;
			}
			else
				bIntern = TRUE; 
		}

	}
	if( !bIntern )
	{
		//*** LOGINDIALOG
		CLoginDlg olLoginDlg;
		if (olLoginDlg.DoModal() != IDOK)
			return FALSE;
	}
	//*** END OF LOGIN CHECKING ***

	strcpy(CCSCedaData::pcmUser, pcgUser);
	//afxDump << ogPrivList.GetStat("InitModu");

	ogBCD.SetTableExtension(pcgTableExt); 
	//  For registering all RegelWerk-Templates
	ogBCD.SetObject("TPL");
	ogBCD.Read("TPL","WHERE APPL='RULE_AFT' AND TPST in ('0','1')" );

	if(ogPrivList.GetStat("InitModu") == '1')
	{
		RegisterDlg olRegisterDlg;
		int ilStartApp = olRegisterDlg.DoModal();
	}
#else
	CDialog olDummyDlg;	
	olDummyDlg.Create(IDD_WAIT_DIALOG,NULL);
	CAatLogin olLoginCtrl;
	olLoginCtrl.Create(NULL,WS_CHILD|WS_DISABLED,CRect(0,0,100,100),&olDummyDlg,4711);
	olLoginCtrl.SetInfoButtonVisible(TRUE);
	olLoginCtrl.SetApplicationName("GRP");

	ogBCD.SetTableExtension(pcgTableExt); 
	//  For registering all RegelWerk-Templates
	ogBCD.SetObject("TPL");
	ogBCD.Read("TPL","WHERE APPL='RULE_AFT' AND TPST in ('0','1')" );

	olLoginCtrl.SetRegisterApplicationString(ogInitModuData.GetInitModuTxt());

	if(olCmdLine.GetLength() != 0)
	{
		int ilParamCount = ExtractItemList(olCmdLine,&ogCmdLineArgsArray, ' ');

		if ( __argc > 1 )
			ogCallingAppName = __targv[1];
		if ( __argc > 2 )
			sprintf(pcgUser,"%s", __targv[2] );
		if ( __argc > 3 )
			sprintf(pcgPasswd,"%s", __targv[3] );
		if ( __argc > 4 )
			dlg.omStartTpl = __targv[4];

		if (ogCallingAppName=="REGELWERK")
			ogCallingAppName ="RULE_AFT";
		BOOL blRc=FALSE;
		if ( ilParamCount >= 3 )
		{
			if(olLoginCtrl.DoLoginSilentMode(pcgUser,pcgPasswd) == "OK")
			{
				bIntern = TRUE; 
			}
		}
	}
	
	if( !bIntern )
	{

		if (olLoginCtrl.ShowLoginDialog() != "OK")
		{
			olDummyDlg.DestroyWindow();
			return FALSE;
		}
	}

	strcpy(pcgUser,olLoginCtrl.GetUserName_());
	strcpy(pcgPasswd,olLoginCtrl.GetUserPassword());

	ogCommHandler.SetUser(olLoginCtrl.GetUserName_());
	strcpy(CCSCedaData::pcmUser, olLoginCtrl.GetUserName_());
	strcpy(CCSCedaData::pcmReqId, ogCommHandler.pcmReqId);

	ogPrivList.Add(olLoginCtrl.GetPrivilegList());

	olDummyDlg.DestroyWindow();



#endif

	// *** InitialLoadDialog
    InitialLoad(NULL);

	if ( ogCallingAppName=="EMPTY" || ogCallingAppName.IsEmpty() )
	{
		CSelectAppl olSelApplDlg;
		
		olSelApplDlg.DoModal();
		if ( !olSelApplDlg.omSelectedAppl.IsEmpty() )
			ogCallingAppName = olSelApplDlg.omSelectedAppl;

	}

	// *** SHOW GROUP DIALOG 
	//CGroupsDialog dlg(NULL);

	m_pMainWnd = &dlg;
	int nResponse = dlg.DoModal();
	
	if (nResponse == IDOK) 
	{
		//  dismissed with OK
	}
	else if (nResponse == IDCANCEL)
	{
		//  dismissed with Cancel
	}

	return FALSE;
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void CGrpApp::InitialLoad(CWnd *pParent)
{
	if (!bIntern) pogInitialLoad = new CInitialLoadDlg(pParent);

	if (pogInitialLoad != NULL)
	{
		pogInitialLoad->SetMessage(GetString(IDS_START_TITLE));
	}
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));

	bgIsInitialized = false;


	//  ogBCD.SetTableExtension(pcgTableExt);  hag now before Registration

	if (pogInitialLoad != NULL) pogInitialLoad->SetProgress(50);
	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	// *** TABLE INFO
	ogBCD.SetObject("TAB");
	ogBCD.Read(CString("TAB"),"WHERE MODU='GRP'" );	//hag000510  ab sofort nicht mehr alle Eintr�ge
													// aus TABTAB, da sonst combobox zu voll
	//int x=ogBCD.GetFieldCount("TAB");
	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	if (pogInitialLoad != NULL) pogInitialLoad->SetProgress(100);

	if (pogInitialLoad != NULL)
	{
		pogInitialLoad->DestroyWindow(); 
		pogInitialLoad = NULL;
	} 
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
BOOL CGrpApp::ProcessMessageFilter(int code, LPMSG lpMsg)
{
   if (m_hwndDialog != NULL)              
   {
     if ((lpMsg->hwnd == m_hwndDialog) || ::IsChild(m_hwndDialog, lpMsg->hwnd))
        //*** GET ALL THE MESSAGES OF THE DIALOG AND HIS CHILDS
		{
			CWnd *cw;
			cw=(AfxGetMainWnd()->FromHandle(lpMsg->hwnd));

			if (lpMsg->message == WM_KEYDOWN)
			//*** CATCH ONLY THE KEY_DOWNS ****************
			{
				if ((lpMsg->wParam)==9) 
				//***CATCH ONLY THE TABS
				{
					//afxDump << "L:" << lpMsg->lParam <<"\n";

					//*** HARD CODED IDs TO SET FOCUS
					if ((cw->GetDlgCtrlID())==IDC_GRIDAUSWAHL)
						AfxGetMainWnd()->GetDlgItem(IDC_GRIDGROUP)->SetFocus();

					if ((cw->GetDlgCtrlID())==IDC_GRIDGROUP)
						AfxGetMainWnd()->GetDlgItem(IDC_SUCHTEXT)->SetFocus();
				}  

				if ((lpMsg->wParam)==13)
				//***CATCH ONLY THE RETURN
				{
					//*** HARD CODED IDs TO SET FOCUS
					if ((cw->GetDlgCtrlID())==IDC_GRIDAUSWAHL)
						dlg.AddGroupMember();

					if ((cw->GetDlgCtrlID())==IDC_GRIDGROUP)
						dlg.DeleteGroupMember();
				}

			}//***END WM_KEYDOWN********************************


			if (((cw->GetDlgCtrlID())==IDC_GRIDGROUP))
			//*** CATCH THE DOUBLECLICKS IN THE GRIDS
			{
				if  ((lpMsg->message)==WM_LBUTTONDBLCLK) 
					dlg.DeleteGroupMember();
			}
			if (((cw->GetDlgCtrlID())==IDC_GRIDAUSWAHL))
			{
				if  ((lpMsg->message)==WM_LBUTTONDBLCLK) 
					dlg.AddGroupMember();
			}

		}              
	} 

   // Default processing of the message.
   return CWinApp::ProcessMessageFilter(code, lpMsg);      
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
int CGrpApp::ExtractItemList(CString opSubString, CStringArray *popStrArray, char cpTrenner)
{
	//*** HELP FUNCTION TO PARSE COMMAND LINE  ***
	//*** EXTRACR COMMAND LINE TO STRING ARRAY ***

	CString olText;

	popStrArray->RemoveAll();

	BOOL blEnd = FALSE;
	
	if(!opSubString.IsEmpty())
	{
		int pos;
		int olPos = 0;
		while(blEnd == FALSE)
		{
			pos = opSubString.Find(cpTrenner);
			if(pos == -1)
			{
				blEnd = TRUE;
				olText = opSubString;
			}
			else
			{
				olText = opSubString.Mid(0, opSubString.Find(cpTrenner));
				opSubString = opSubString.Mid(opSubString.Find(cpTrenner)+1, opSubString.GetLength( )-opSubString.Find(cpTrenner)+1);
			}
			popStrArray->Add(olText);
		}
	}
	return	popStrArray->GetSize();
}

int CGrpApp::ExitInstance() 
{
	// TODO: Add your specialized code here and/or call the base class

	//  Hilfsfenster (s. InitInstance l�schen
	if ( polHelperWindow && polHelperWindow->m_hWnd )
	{
		polHelperWindow->DestroyWindow ();
		delete polHelperWindow;
	}

	// end CEDA communication
	ogCommHandler.CleanUpCom();
	AatHelp::ExitApp();
	
	return CWinApp::ExitInstance();
}
