// CedaInitModuData.cpp
 
#include "stdafx.h"
#include "CedaInitModuData.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

const igNoOfPackets = 10;
CedaInitModuData ogInitModuData;


//--CEDADATA-----------------------------------------------------------------------------------------------

CedaInitModuData::CedaInitModuData()
{
	strcpy(pcmInitModuFieldList,"APPL,SUBD,FUNC,FUAL,TYPE,STAT");
	bmRegistered = false;
}

//--~CEDADATA----------------------------------------------------------------------------------------------

CedaInitModuData::~CedaInitModuData(void)
{
}

//--SEND INIT MODULS-------------------------------------------------------------------------------------------

bool CedaInitModuData::SendInitModu()
{
	bool blRc = true;

	//-- exception handling
	CCS_TRY

	//INITMODU//////////////////////////////////////////////////////////////

	CString olInitModuData = this->GetInitModuTxt();

	//INITMODU END//////////////////////////////////////////////////////////


	int ilCount = olInitModuData.GetLength();

	char *pclInitModuData = new char[olInitModuData.GetLength()+3];
	strcpy(pclInitModuData,olInitModuData);

	int ilCount2 = strlen(pclInitModuData);

	char pclTableExt[65];
	strcpy(pclTableExt, pcgTableExt);
	strcat(pclTableExt, " ");


	blRc = CedaAction("SMI",pclTableExt,pcmInitModuFieldList,"","",pclInitModuData);
	if(blRc==false)
	{
		AfxMessageBox(CString("SendInitModu:") + omLastErrorMessage,MB_ICONEXCLAMATION);
	}
	else
		bmRegistered = true;

	delete pclInitModuData;
	
	//-- exception handling
	CCS_CATCH_ALL

	return blRc;
}


//--------------------------------------------------------------------------------------------------------------

CString CedaInitModuData::GetInitModuTxt() 
{
	CString olInitModuData = "GRP,InitModu,InitModu,Initialisieren (InitModu),B,-,";

	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	// ButtonList			
	olInitModuData += GetInitModuTxtG();
	olInitModuData += GetTemplatesString();

	return olInitModuData;
}
//--------------------------------------------------------------------------------------------------------------

CString CedaInitModuData::GetInitModuTxtG() 
{
	CString olStr="";

	CCS_TRY

	// Build register string for BDPS-SEC	
	//		Attention: You can't use '+=' too often with CString. When reaching a certain number
	//				   you get an error message. In that case you can do the following: 
	//				   Build several strings up to the max length before causing the error.	
	//				   Afterwards you can concatenate those strings (It'll work, I don't know why).
	CString olResStr;
	olResStr.LoadString(IDS_PRIV0000);
	olStr += "GRP,GROUPFUNCTIONS," + olResStr + ",Z,1,";
	olResStr.LoadString(IDS_PRIV0001);
	olStr += "GRP,IDNEU," + olResStr + ",B,1,";
	olResStr.LoadString(IDS_PRIV0002);
	olStr += "GRP,IDDELETE," + olResStr + ",B,1";
	
	CCS_CATCH_ALL

	return olStr;
}

CString CedaInitModuData::GetTemplatesString()
{
	int		i, ilCount;
	CString olInitModuData = ",Templates,*Universal,*Universal ,Z,1"; 
	CString olTnam;
	//-- exception handling
	CCS_TRY

	ilCount = ogBCD.GetDataCount("TPL");
	for ( i=0; i<ilCount; i++ )
	{
		olTnam = ogBCD.GetField("TPL", i, "TNAM" );
		if ( !olTnam.IsEmpty() )
		{
			CCSCedaData::MakeCedaString(olTnam);
			olInitModuData += ",Templates," + olTnam + CString(",") + olTnam + ",Z,1";
		}
	}

	//-- exception handling
	CCS_CATCH_ALL

	return olInitModuData;
}