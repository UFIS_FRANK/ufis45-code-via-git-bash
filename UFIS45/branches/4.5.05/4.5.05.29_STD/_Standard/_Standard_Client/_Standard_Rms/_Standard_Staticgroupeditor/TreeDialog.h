#if !defined(AFX_TREEDIALOG_H__C9C5C4B1_3B7A_11D3_97FC_00001C0411B3__INCLUDED_)
#define AFX_TREEDIALOG_H__C9C5C4B1_3B7A_11D3_97FC_00001C0411B3__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// TreeDialog.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CTreeDialog dialog

class CTreeDialog : public CDialog
{
// Construction
public:
	CTreeDialog(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CTreeDialog)
	enum { IDD = IDD_TREEDIALOG };
	CTreeCtrl	m_Tree;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTreeDialog)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CTreeDialog)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TREEDIALOG_H__C9C5C4B1_3B7A_11D3_97FC_00001C0411B3__INCLUDED_)
