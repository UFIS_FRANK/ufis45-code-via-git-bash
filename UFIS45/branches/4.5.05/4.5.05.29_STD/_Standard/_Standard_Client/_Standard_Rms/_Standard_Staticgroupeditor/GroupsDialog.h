#if !defined(AFX_GROUPSDIALOG_H__2CC4563F_1F15_11D3_97E1_00001C0411B3__INCLUDED_)
#define AFX_GROUPSDIALOG_H__2CC4563F_1F15_11D3_97E1_00001C0411B3__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "stgrid.h"
// GroupsDialog.h : header file
//

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// NUMBER OF DLGITEMS
#define NODLGITEMS 2
//
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


/////////////////////////////////////////////////////////////////////////////
// CGroupsDialog dialog

class CGroupsDialog : public CDialog
{
// Construction
public:
	int AddGroupMember();
	int DeleteGroupMember();
	CGroupsDialog(CWnd* pParent = NULL);   // standard constructor
	

// Dialog Data
	//{{AFX_DATA(CGroupsDialog)
	enum { IDD = IDD_GROUPS };
	CComboBox	omComboTypes;
	CComboBox	omAllocCB;
	CComboBox	omTplCB;
	CStatic	m_NoGroup;
	CStatic	m_NoTables;
	CComboBox	m_ComboGroups;
	CComboBox	m_ComboTables;
	//}}AFX_DATA
	BOOL bmAllocCBEnabled ;
	BOOL bmTplCBEnabled ;
	CString omStartTpl;


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CGroupsDialog)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void PostNcDestroy();
	//}}AFX_VIRTUAL

	CSTGrid *olGrdTable;
	CSTGrid *olGrdGroup;

// Implementation
protected:



	// Generated message map functions
	//{{AFX_MSG(CGroupsDialog)
	virtual BOOL OnInitDialog();
	afx_msg void OnCloseupCombotables();
	afx_msg void OnClose();
	afx_msg void OnCloseupCombogroups();
	afx_msg void OnNeu(); 
	afx_msg void OnBeenden();
	afx_msg void OnSuchen();
	afx_msg void OnCancelchange();
	afx_msg void OnSave();
	afx_msg void OnDelete();
	afx_msg void OnAddmember();
	afx_msg void OnDeletemember();
	afx_msg void OnTree();
	afx_msg void OnSelendokUgtyCb();
	afx_msg void OnSelendokUtplCb();
	afx_msg void OnKillfocusUgtyCb();
	afx_msg void OnAppAbout();
	afx_msg void OnCloseupCombotypes();
	//}}AFX_MSG
	afx_msg LONG OnBcAdd(UINT wParam, LONG lParam);
	DECLARE_MESSAGE_MAP()
private:
	int FillGridGroups();
	CString InputBox(CString cTitle, CString cText, CString cDefault, long iLen);
	int FillComboGroups(CString cTable);
//	CString LoadStg(UINT StrNo);   
	CGXFont mGrdFont; 
	BOOL IniAvailable(CString cTable); 
	CString ReadIni(CString cSection,CString cKey);
	int CreateTableInfo(CString cTable);
	void InitializeComboTypes();
	bool GetSelectedTypeUrno ( CString &ropSubtypeUrno );
	void FillComboTypes();


	CStringArray m_olLtna;
	CStringArray m_oHedr;
	CStringArray m_oFina;
	CPtrArray m_oGrpUrnos1;
	CArray<long,long> m_oGrpUrnos;
	CString m_cTable;//SHORT TABLENAME 'APT' 
	CString m_cTableLong;//SHORT TABLENAME 'APTHAJ' 
	CString omSubtypeUrno;
	long mActGrpUrno; 
	int FillGridTables(CString cTable);
	int FillComboTables();   
	void IniAlocCB ();
	void IniTplCB ();	
	CString GetActTplUrno ();
	void DecideControls ();
	bool SetCtrlState ( UINT ipCtrlId, char cpState );

	int m_ActFindCol;
	int m_ActFindRow;
  
	long m_iTableRows;
	long m_iGroupRows;
	
	long mListDlgItems[NODLGITEMS];
	
	
	//  CMap<long,long,long,long> omGrpUrno ;
	CMap<long,long,long,long> omGrpUrnoDel;  
	CMap<long,long,long,long> omGrpUrnoNew;

	CMap<long,long,long,long> omGrpUrnoMain;

	CMapStringToString omFinaToHedr;

	void OnOK();
	void OnCancel();
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_GROUPSDIALOG_H__2CC4563F_1F15_11D3_97E1_00001C0411B3__INCLUDED_)
