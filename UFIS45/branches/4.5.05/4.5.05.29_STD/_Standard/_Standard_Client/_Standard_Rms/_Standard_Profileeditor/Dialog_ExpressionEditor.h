#if !defined(AFX_DIALOG_EXPRESSIONEDITOR_H__CD31A931_CB00_11D2_AB09_00001C018CF3__INCLUDED_)
#define AFX_DIALOG_EXPRESSIONEDITOR_H__CD31A931_CB00_11D2_AB09_00001C018CF3__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Dialog_ExpressionEditor.h : header file
//
#include <resource.h>
#include <GridControl.h>

//////////////////////////////////////////////////////////////////////////////////
//
// Dialog_ExpressionEditor.h : header file
//
// Notes: The CDialog_ExpressionEditor class is derived from the standard CDialog class . 
//         
//        The class offers the possibilie to create an term and implements 
//        syntax checks on this term .
//
//        The finisched expression can be saved in the DGR Table and is then used to 
//        represent dynamic groups e.g.  alls aircraft types witha wingspan < 40 m
//
//
// Date : March 1999
//
// Author : EDE 
//
// Modification History:
//
//
//
///////////////////////////////////////////////////////////////////////////////////


enum STATUS { ANFANG , WERT , AND_OR , AUSWAHL , OPERATOR , KLAMMER_AUF , KLAMMER_ZU };


class CDialog_ExpressionEditor : public CDialog
{
// Construction
public:

	CDialog_ExpressionEditor(CString opTplUrno, CString opUrno, CString opRef, CString &ropTsrUrno, CWnd* pParent=NULL);   // standard constructor
	~CDialog_ExpressionEditor();

	//--- String Array mit Daten f�r Grid
	
	CString omDgrName;
	CString omRefFieldName;
	CString omDgrUrno;
	CString omTplUrno;
	CString omExpression;	// for writing to database
	CString omUserExpression;	// for user output
	CString omTsrUrno;
	
	CStringArray omInternUndoArr;
	CStringArray omUserUndoArr;
	
	CGridControl *pomSelectionGrid;

	int imCountOpenBrackets;
	int imCountCloseBrackets;

	int imDgrGnamIdx;
	int imDgrExprIdx;
	int imDgrUtplIdx;
	int imDgrReftIdx;
	int imDgrUrnoIdx;

// Dialog Data
	//{{AFX_DATA(CDialog_ExpressionEditor)
	enum { IDD = IDD_EXPRESSION_EDITOR_DLG };
	CComboBox	m_Combo_Name;
	CButton	m_Btn_New;
	CEdit	m_ExpressionEdit;
	CStatic	m_RefField;
	CString	m_Expression;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDialog_ExpressionEditor)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Eigene
	void SetStaticTexts();
	void SetExpression();
    void FillCombo();
    bool RetrieveDataFromDB(CString opUrno);

// Implementation
protected:
	
	//--- Grid
    

    //--- Aktueller  Status
	STATUS   imStatus;

	//--- Letzter Status
	STATUS   imLastStatus;
	
	void SetBdpsState();

	// Generated message map functions
	//{{AFX_MSG(CDialog_ExpressionEditor)
	virtual BOOL OnInitDialog();
	afx_msg void OnGridFeldClicked(WPARAM wParam, LPARAM lParam);
	afx_msg void OnBUTTONAnd();
	afx_msg void OnBUTTONAssign();
	afx_msg void OnBUTTONBigger();
	afx_msg void OnBUTTONEqual();
	afx_msg void OnBUTTONKlammerAuf();
	afx_msg void OnBUTTONKlammerZu();
	afx_msg void OnBUTTONNotEqual();
	afx_msg void OnBUTTONOr();
	afx_msg void OnBUTTONReset();
	afx_msg void OnBUTTONSmaller();
	afx_msg void OnKillfocusEditWert();
	afx_msg void OnSave();
	afx_msg void OnBUTTONCorrect();
	afx_msg void OnDelete();
	virtual void OnCancel();
	afx_msg void OnNew();
	afx_msg void OnSelendokName();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DIALOG_EXPRESSIONEDITOR_H__CD31A931_CB00_11D2_AB09_00001C018CF3__INCLUDED_)
