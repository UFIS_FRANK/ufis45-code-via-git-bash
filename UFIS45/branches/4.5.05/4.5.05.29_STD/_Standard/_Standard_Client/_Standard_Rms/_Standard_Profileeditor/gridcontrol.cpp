// GridControl.cpp: implementation of the CGridControl class.
//
//////////////////////////////////////////////////////////////////////

#include <stdafx.h>

#include <ccsglobl.h>
#include <utilities.h>
#include <GridControl.h>

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
//	Klasse f�r ein Grid innerhalb eines Dialogs, 
//  Baseclass:	BCGXGridWnd
//////////////////////////////////////////////////////////////////////


BEGIN_MESSAGE_MAP(CGridControl, CGXGridWnd)
	//{{AFX_MSG_MAP(CGridControl)
	ON_WM_LBUTTONUP()
//	ON_WM_KILLFOCUS()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


CGridControl::CGridControl()
{
	bmAutoGrow = true;
	bmSortEnabled = true;
	bmSortAscend = false;
	memset ( &smLButtonDblClick, 0, sizeof(smLButtonDblClick) );
	bmIsDirty = false;
	ilAutoGrowCol = 1;
}

CGridControl::CGridControl ( CWnd *popParent, UINT nID, ROWCOL nCols, ROWCOL nRows )
{
	umID = nID;
	pomWnd = popParent;
	SubclassDlgItem ( umID, pomWnd );
	Initialize();
	SetParent ( pomWnd );
	GetParam()->EnableUndo(FALSE);
	LockUpdate(TRUE);
	SetRowCount(nRows);
	SetColCount(nCols);
	LockUpdate(FALSE);
	GetParam()->EnableUndo(TRUE);
	bmAutoGrow = true;
	bmSortEnabled = true;
	bmSortAscend = false;
	GetParam()->EnableTrackRowHeight(FALSE);
	memset ( &smLButtonDblClick, 0, sizeof(smLButtonDblClick) );
	bmIsDirty = false;
	ilAutoGrowCol = 1;
}

CGridControl::~CGridControl()
{
	omSpecialFormats.RemoveAll ();
}

BOOL CGridControl::SetValue ( ROWCOL nRow, ROWCOL nCol, CString &cStr ) 
{
	return SetValueRange( CGXRange(nRow, nCol), cStr );
}


const CString& CGridControl::GetValue ( ROWCOL nRow, ROWCOL nCol ) 
{
	return GetValueRowCol ( nRow, nCol );
}

BOOL CGridControl::EnableGrid ( BOOL enable )
{
	BOOL blRet;

	int ilRowCount = GetRowCount();
	int ilColCount = GetColCount();

	if (ilRowCount >= 1 && ilColCount >= 1)
	{
		if ( enable )
			blRet = SetStyleRange(CGXRange(1, 1, ilRowCount, ilColCount), CGXStyle().SetInterior(ogColors[WHITE_IDX]).SetEnabled(TRUE));
		else
			blRet = SetStyleRange(CGXRange(1, 1, ilRowCount, ilColCount), CGXStyle().SetInterior(ogColors[SILVER_IDX]).SetEnabled(FALSE));
	}
	Redraw();
	return blRet;
}

void CGridControl::EnableAutoGrow ( bool enable /*=true*/ )
{
	bmAutoGrow = enable;
}

BOOL CGridControl::OnEndEditing( ROWCOL nRow, ROWCOL nCol) 
{
	BOOL blRet = CGXGridWnd::OnEndEditing( nRow, nCol) ;
	DoAutoGrow ( nRow, nCol) ;
	CGXControl* polControl;
	if ( polControl = GetControl( nRow, nCol) )
		bmIsDirty |= ( polControl->GetModify () == TRUE );

	GRIDNOTIFY rlNotify;
	rlNotify.idc = umID;
	rlNotify.row = nRow;
	rlNotify.col = nCol;
	rlNotify.headerrows = GetHeaderRows();
	rlNotify.headercols = GetHeaderCols();
	pomWnd->SendMessage ( WM_GRID_ENDEDITING, 0, (LPARAM)&rlNotify );
	DisplayInfo (nRow, nCol, true );
	SetStatusBarText ( CString("") );
	
	return blRet;
}

void CGridControl::DoAutoGrow ( ROWCOL nRow, ROWCOL nCol) 
{
	ROWCOL ilRowCount = GetRowCount ();
	if ( bmAutoGrow &&( nRow >= ilRowCount )  && (nCol==ilAutoGrowCol) )
	{	//  es wird bereits in letzter Zeile editiert, d.h. event. Grid verl�ngern
		if ( ! GetValueRowCol ( nRow, nCol).IsEmpty () )
			InsertBottomRow ();
	}
}

BOOL CGridControl::InsertBottomRow ()
{
	ROWCOL ilRowCount = GetRowCount ();
	//LockUpdate(TRUE);
	BOOL blRet = InsertRows ( ilRowCount+1, 1 );
	CopyStyleLastLineFromPrev ();
	//LockUpdate(FALSE);
	return blRet;
}

void CGridControl::CopyStyleOfLine ( ROWCOL ipSourceLine, ROWCOL ipDestLine,
									 bool bpResetValues/*=true*/ )
{
	ROWCOL ilColCount = GetColCount ();
	CGXStyle olStyle;

	for ( ROWCOL i=1; i<=ilColCount; i++ )
	{
		olStyle.SetDefault ();
		ComposeStyleRowCol( ipSourceLine, i, &olStyle );
		if ( bpResetValues )
		{
			olStyle.SetIncludeItemDataPtr ( FALSE );
			if ( i>0 )
				olStyle.SetInterior(WHITE);
		}
		SetStyleRange  ( CGXRange ( ipDestLine, i), olStyle );
		if ( bpResetValues )
		{
			SetValueRange ( CGXRange ( ipDestLine, i), "" );
		}
	}
}

void CGridControl::CopyStyleLastLineFromPrev ( bool bpResetValues/*=true*/ )
{
	ROWCOL ilRowCount = GetRowCount ();
	CopyStyleOfLine ( ilRowCount-1, ilRowCount, bpResetValues );
}

BOOL CGridControl::OnRButtonDblClkRowCol(ROWCOL nRow, ROWCOL nCol, UINT nFlags, CPoint pt)
{
	BOOL ok = CGXGridWnd::OnRButtonDblClkRowCol(nRow, nCol, nFlags, pt);
	
	int		*ilWidths;
	ROWCOL  ilRowAnz = GetColCount ();
	
	if ( ilWidths = new int[ilRowAnz+1] )
	{
		for ( ROWCOL i=0; i<=ilRowAnz; i++ )
			ilWidths[i] = GetColWidth(i);

 		delete ilWidths;
	}
	return ok;
}


void CGridControl::OnLButtonUp (UINT nFlags, CPoint point) 
{
	CGXGridWnd::OnLButtonUp(nFlags, point);

    ROWCOL  Row;
	ROWCOL  Col;

	if ( !bmSortEnabled )
		return;
	//--- Betroffene Zelle ermitten
    if ( HitTest ( point, &Row,  &Col, NULL ) != GX_HEADERHIT )
		return;

	//--- Check, ob ung�ltige Spaltennummer
	if ( ( Col > GetColCount( ) ) || ( Col== 0 ) )
		return;
	SortTable ( Row, Col );
}

/*
void CGridControl::OnKillFocus ( CWnd* pNewWnd )
{
	CGXGridWnd::OnKillFocus( pNewWnd );
	ROWCOL	lRow, lCol;
	CWnd  *polParentOfNew ;
	if (  pNewWnd )
	{
		polParentOfNew = pNewWnd->GetParent ();
		if ( ( polParentOfNew != this ) &&
	  		 GetCurrentCell( lRow, lCol ) )
			OnEndEditing ( lRow, lCol ) ;
	}
}
*/


bool CGridControl::SortTable ( ROWCOL ipRowClicked, ROWCOL ipColClicked )
{
	ROWCOL ilRowCount = GetRowCount ();
	ROWCOL ilColCount = GetColCount ();

	if ( ipRowClicked != 0 )
		return false;

	//--- Sortieren je nachdem welcher Header gedr�ckt wurde
	CGXSortInfoArray  sortInfo;
	sortInfo.SetSize( 1 );
		
	// toggle between sorting in ascending / descending order with each click
	bmSortAscend = !bmSortAscend;
	sortInfo[0].sortOrder = bmSortAscend ? CGXSortInfo::ascending : 
										   CGXSortInfo::descending;
	sortInfo[0].nRC = ipColClicked;                       
	sortInfo[0].sortType = CGXSortInfo::autodetect;  
	if ( bmAutoGrow && GetValueRowCol ( ilRowCount, 1 ).IsEmpty () )
		ilRowCount--;
	SortRows( CGXRange().SetRows(1, ilRowCount), sortInfo); 
	return true;
}

void CGridControl::EnableSorting ( bool enable/*=true*/ )
{
	bmSortEnabled = enable;
}

void CGridControl::SetLbDblClickAction ( UINT ipMsg, WPARAM ipWparam, bool bpOnlyInside/*=true*/ )
{
	smLButtonDblClick.bOnlyInside = bpOnlyInside;
	smLButtonDblClick.iMsg = ipMsg;
	smLButtonDblClick.iWparam = ipWparam;
}

BOOL CGridControl::OnLButtonDblClkRowCol(ROWCOL nRow, ROWCOL nCol, UINT nFlags, CPoint pt)
{
	if ( smLButtonDblClick.iMsg )
	{
		LPARAM  lParam = MAKELONG((WORD)nRow,(WORD)nCol);
		BOOL ok = SetCurrentCell ( nRow,nCol );
		pomWnd->PostMessage ( smLButtonDblClick.iMsg, smLButtonDblClick.iWparam, lParam );
		return TRUE;
	}
	else
	{
		GRIDNOTIFY rlNotify;
		rlNotify.idc = umID;
		rlNotify.row = nRow;
		rlNotify.col = nCol;
		rlNotify.headerrows = GetHeaderRows();
		rlNotify.headercols = GetHeaderCols();
		rlNotify.point = pt;
		pomWnd->SendMessage ( WM_GRID_LBUTTONDBLCLK, nFlags, (LPARAM)&rlNotify );
		return TRUE;
	}
}


void CGridControl::OnTextNotFound(LPCTSTR)
{
	ROWCOL		ilRow, ilCol, ilColAnz;
	BOOL		blFound = FALSE;
	ilColAnz = GetColCount();
	GX_FR_STATE	*pslState = GXGetLastFRState();

	if ( GetCurrentCell( &ilRow, &ilCol ) && pslState )
	{
		if ( pslState->bNext )		//  d.h Abw�rts suchen
		{
			while ( !blFound && ( ilCol < ilColAnz ) ) //  und noch nicht in letzter Spalte
			{
				ilCol ++;
				ilRow = GetHeaderRows ()/*+1*/;
				blFound = FindText( ilRow, ilCol, true ) ;
			}
		}
		else 
		{	//  d.h Aufw�rts suchen
			while ( !blFound &&  ( ilCol > GetHeaderCols () + 1 ) )	
			{
				ilCol --;
				ilRow = GetRowCount ()+1;
				blFound = FindText( ilRow, ilCol, true );
			}
		}
		if ( blFound )
			return;

	}
	MessageBeep(0);
}

void CGridControl::SetDirtyFlag ( bool dirty/*=true*/ )
{
	bmIsDirty = dirty;

}

bool CGridControl::IsGridDirty ()
{
	return bmIsDirty ;
}

BOOL CGridControl::OnDeleteCell(ROWCOL nRow, ROWCOL nCol)
{
	CGXControl	*polControl;
	CString		olWert;
	BOOL		ilRet = FALSE;
	ROWCOL		ilFrozenRows = GetFrozenRows();
	ROWCOL		ilFrozenCols = GetFrozenCols(); 

	bool blfrozen = ( nRow <= ilFrozenRows ) || ( nCol <= ilFrozenCols ) ;

	//  event. Selection auf Row- und Columnheadern entfernen
	SelectRange( CGXRange ().SetRows(0,ilFrozenRows), FALSE );
	SelectRange( CGXRange ().SetCols(0,ilFrozenCols), FALSE );

	//  nur f�r Zellen innerhalb des Grid, alten Wert merken
	if ( !blfrozen )
	{
		polControl = GetControl(nRow, nCol);
		if ( !polControl  || !polControl->GetValue(olWert) )
			olWert = "XXX";
		ilRet = CGXGridWnd::OnDeleteCell(nRow, nCol);
	}
	bmIsDirty |= ( olWert.IsEmpty()==FALSE) ;
	return ilRet&&!blfrozen;
}

BOOL CGridControl::OnLButtonClickedRowCol(ROWCOL nRow, ROWCOL nCol, UINT nFlags, CPoint pt)
{
	GRIDNOTIFY rlNotify;
	rlNotify.idc = umID;
	rlNotify.row = nRow;
	rlNotify.col = nCol;
	rlNotify.headerrows = GetHeaderRows();
	rlNotify.headercols = GetHeaderCols();
	rlNotify.point = pt;
	pomWnd->SendMessage ( WM_GRID_LBUTTONCLICK, nFlags, (LPARAM)&rlNotify );
	return TRUE;
}

//  DeleteEmptyRows:  
//	Checkt, ob �bergebene Zelle ohne Inhalt verlassen worden ist
//  In:  nRow, nCol:  Zelle, die modifiziert worden ist
//	return:  true, wenn Zeile gel�scht wurde
//			 false sonst
bool CGridControl::DeleteEmptyRows ( ROWCOL nRow, ROWCOL nCol ) 
{
	ROWCOL  ilRowCount = GetRowCount();
	ROWCOL  ilColCount = GetRowCount();
	ROWCOL  ilHeaderRows = GetHeaderRows();
	ROWCOL  ilHeaderCols = GetHeaderCols();
	if ( ( nCol <= ilHeaderCols ) || ( nCol > ilColCount ) )
		return false;
	if ( bmAutoGrow )
		ilRowCount	-= 1; //  Wenn autogrow, mu� eine leere Zeil �brig bleiben
	if ( ( nRow <= ilHeaderRows ) || ( nRow > ilRowCount ) )
		return false;
	if ( GetValueRowCol ( nRow, nCol ).IsEmpty() )
	{
		RemoveRows( nRow, nRow );
		return true;
	}
	else 
		return false;
}

void CGridControl::ResetValues ()
{
	ROWCOL ilColCount = GetColCount();
	ROWCOL ilRowCount = GetRowCount();
	ROWCOL  ilHeaderRows = GetHeaderRows();
	ROWCOL  ilHeaderCols = GetHeaderCols();

	if ( ( ilHeaderRows < ilRowCount ) && ( ilHeaderCols < ilColCount ) )
		SetValueRange ( CGXRange(ilHeaderRows+1,ilHeaderCols+1,
								 ilRowCount,ilColCount), "" );
	if ( bmAutoGrow && ( ilRowCount > ilHeaderRows+1 ) )
			RemoveRows( ilHeaderRows+2, ilRowCount );
	SetDirtyFlag ( false );
}

void CGridControl::RegisterGxCbsDropDown ( bool bpSort, bool bpTextFit )
{ 
	CGXComboBoxWnd *pWnd = new CGXComboBoxWnd(this);
	unsigned long flags = WS_VSCROLL | CBS_DROPDOWN | CBS_AUTOHSCROLL ;
	if ( bpSort )
		flags |= CBS_SORT;
	if ( bpTextFit )
		flags |= GXCOMBO_TEXTFIT;
	pWnd->Create ( flags, 0);
	pWnd->m_bFillWithChoiceList = TRUE;
	pWnd->m_bWantArrowKeys = TRUE;
	RegisterControl(GX_IDS_CTRL_CBS_DROPDOWN, pWnd);
}


void CGridControl::SetSpecialFormat ( ROWCOL ipRow, ROWCOL ipCol, CString opForm )
{
	char    key[41];

	sprintf( key, "%d %d", ipRow, ipCol );
	if ( opForm.IsEmpty () )
		omSpecialFormats.RemoveKey ( key );
	else
		omSpecialFormats.SetAt ( key, opForm );
}

bool CGridControl::FormatSpecial ( ROWCOL ipRow, ROWCOL ipCol, CString &ropValue )
{
	char    key[41];
	CString olFormat;
	CString olFormated="-------";
	bool	blRet=false;


	sprintf( key, "%d %d", ipRow, ipCol );
	if ( !omSpecialFormats.Lookup ( key, olFormat ) )
		return false;
	if ( olFormat == "WDAY" )
	{
		ropValue = ropValue.Left(7);
		for ( int i=0; i<7; i++ )
			if ( ropValue.Find ( '1'+i ) >= 0 )
				olFormated.SetAt ( i, '1'+i );
		ropValue = olFormated;
		blRet = true;
	}
	return blRet;
}

bool CGridControl::UnFormatSpecial ( ROWCOL ipRow, ROWCOL ipCol, CString &ropValue )
{
	char    key[41];
	CString olFormat;
	CString olUnFormated="";
	bool	blRet=false;


	sprintf( key, "%d %d", ipRow, ipCol );
	if ( !omSpecialFormats.Lookup ( key, olFormat ) )
		return false;
	if ( olFormat == "WDAY" )
	{
		ropValue = ropValue.Left(7);
		for ( int i=0; i<7; i++ )
			if ( ropValue.Find ( '1'+i ) >= 0 )
				olUnFormated += CString ( '1'+i );
		ropValue = olUnFormated;
		blRet = true;
	}
	return blRet;
}

//  Markiere Text ropValue als ung�ltig, wenn er nicht in Auswahlliste 
//	der Zelle (ipRow,ipCol) vorkommt
//	return:  true, wenn Wert als ung�ltig markiert worden ist
//			 false, wenn Wert g�ltig ist, oder Fehler aufgetreten ist
bool CGridControl::MarkInValid (  ROWCOL ipRow, ROWCOL ipCol, CString &ropValue )
{
	CGXControl	*polCB; 
	CGXStyle	olGXStyle;
	CString		olFound, olChoiceList;
	int			index=0;
	COLORREF	ilColor = ogColors[WHITE_IDX];
	
	if ( ( ipRow<=GetHeaderRows() ) || ( ipCol<=GetHeaderCols() ) )
		return false;

	if ( !ropValue.IsEmpty () && (ropValue[0]!=' ') &&
		 GetStyleRowCol ( ipRow, ipCol, olGXStyle ) &&
		 olGXStyle.GetIncludeChoiceList() && 
		 ( polCB = GetControl ( ipRow, ipCol ) )
	   )
	{
		olChoiceList = olGXStyle.GetChoiceList();
		index = polCB->FindStringInChoiceList( olFound, ropValue, 
											   olChoiceList, TRUE );
		if ( index < 0 )
			ilColor = ogColors[IDX_ORANGE];
	}
	SetStyleRange ( CGXRange(ipRow, ipCol), CGXStyle().SetInterior(ilColor) );

	return (index<0);
}

void CGridControl::ClearTable()
{
	  // empty grids
	CCS_TRY

	ROWCOL ilCount1 = GetRowCount();
	if(ilCount1 != 0)
		RemoveRows(1, ilCount1);

	omSpecialFormats.RemoveAll ();	
	//--- exception handling
	CCS_CATCH_ALL
}

void CGridControl::OnModifyCell (ROWCOL nRow, ROWCOL nCol)
{
	CGXGridWnd::OnModifyCell(nRow, nCol);

	ROWCOL ilRowCount, ilColCount;
	ilRowCount = GetRowCount();
	ilColCount = GetColCount();

	if ( (nRow<=0) || (nRow>ilRowCount) || (nCol<=0) || (nCol>ilColCount) )
		return;
	DisplayInfo (nRow, nCol);
}

void CGridControl::DisplayInfo ( ROWCOL nRow, ROWCOL nCol, bool bpToolTip/*=false*/ )
{
	SetStatusBarText ( CString("") );
}

BOOL CGridControl::OnStartEditing(ROWCOL nRow, ROWCOL nCol)
{
	//--- send message to parent dialog
	GRIDNOTIFY rlNotify;
	rlNotify.idc = umID;
	rlNotify.row = nRow;
	rlNotify.col = nCol;
	rlNotify.headerrows = GetHeaderRows();
	rlNotify.headercols = GetHeaderCols();
	
	pomWnd->SendMessage(WM_GRID_STARTEDITING, 0, (LPARAM)&rlNotify );
	DisplayInfo (nRow, nCol, true);
	SetStatusBarText(CString(""));

	return TRUE;
}

BOOL CGridControl::GetControlText (ROWCOL nRow, ROWCOL nCol, CString &ropText) 
{
	ROWCOL ilHeaderRows = GetHeaderRows();
	ROWCOL ilHeaderCols = GetHeaderCols();
	ROWCOL ilRows = GetRowCount();
	ROWCOL ilCols = GetColCount();
	if ( (nRow<ilHeaderRows) || (nRow>ilRows) ||
		 (nCol<ilHeaderCols) || (nCol>ilCols) )
		 return FALSE;
	
	CGXControl* polCtrl = GetControl(nRow, nCol );
	CGXStyle	olStyle;
	if ( polCtrl && GetStyleRowCol ( nRow, nCol, olStyle ) )
		return polCtrl->GetControlText(ropText, nRow, nCol, 0, olStyle );
	else
		return FALSE;
}

BOOL CGridControl::OnValidateCell(ROWCOL nRow, ROWCOL nCol)
{
	CGXStyle	olStyle;
	CGXControl	*polControl;
	CString		olText;
	unsigned	ulValFormat=777;

	if ( GetStyleRowCol(nRow,nCol,olStyle) && olStyle.GetIncludeFormat() )
		 ulValFormat = olStyle.GetFormat();
	if ( ( (ulValFormat==GX_FMT_FLOAT)||(ulValFormat==GX_FMT_FIXED) ) &&
		 (polControl = GetControl(nRow,nCol) ) &&
		 polControl->GetValue(olText) &&
		 (olText.Replace ( ',','.' )>0) 
	   )
	   polControl->SetControlText(nRow,nCol,olText);
	   //polControl->SetValue(olText);

	return CGXGridWnd::OnValidateCell(nRow, nCol);
}

bool CGridControl::SetItemDataPtr ( ROWCOL nRow, ROWCOL nCol, void *pv )
{
	if ( ( nRow > GetRowCount() ) || ( nCol > GetColCount() ) )
		return false;
	SetStyleRange ( CGXRange(nRow, nCol), CGXStyle().SetItemDataPtr(pv) );
	return true;
}

void* CGridControl::GetItemDataPtr ( ROWCOL nRow, ROWCOL nCol )
{
	void		*polRet=0;
	CGXStyle	olStyle;
	if ( ( nRow <= GetRowCount() ) && ( nCol <= GetColCount() ) &&
		 GetStyleRowCol(nRow, nCol, olStyle) && 
		 olStyle.GetIncludeItemDataPtr() )
		 polRet = olStyle.GetItemDataPtr() ;
	return polRet;
}

void CGridControl::ModifyChoiceListRowCol ( ROWCOL ipRow, ROWCOL ipCol, 
										    WORD ipCtrlType, CString opCode, 
											bool bpAdd )
{
	CGXStyle olStyle;
	CString	 olChoiceList="\n";
	CString  olItem = "\n";
	int ilLength ;

	//  Um mit CString::Find das richtige Vorkommen des Items in der Auswahl-
	//  liste sicherzustellen, wird in olChoiceList vorne und hinten ein Trenner 
	//  '\n' eingef�gt. 
	olItem += opCode;
	olItem += "\n";

	olStyle.SetDefault();	//  lokale Variable zur�cksetzen
	GetStyleRowCol(ipRow, ipCol, olStyle);
	BOOL blReadOnly = GetParam()->IsLockReadOnly();

	if (olStyle.GetIncludeChoiceList())	
	{
		olChoiceList += olStyle.GetChoiceList();
	}

	if ((olChoiceList.IsEmpty() == FALSE) && (olChoiceList.Right(1) != "\n"))
	{
		olChoiceList += "\n";
	}

	int ilPos = olChoiceList.Find(olItem);
	if (ilPos == -1)
	{
		if (bpAdd == false)	//  L�schen und nicht gefunden fertig
		{
			return;
		}

		//  Einf�gen
		olItem.Replace("\n", "");
		olChoiceList += olItem;
	}
	else		//  item ist in Auswahlliste
	{
		if ( bpAdd )  //  zuf�gen -> fertig
			return;
		//  l�schen
		olChoiceList.Replace ( olItem, "\n" );
		ilLength = olChoiceList.GetLength();
		if ( olChoiceList[ilLength-1]=='\n' )
			 olChoiceList = olChoiceList.Left(ilLength-1);
	}	
	//  vorderen '\n' wieder entfernen
	ilLength = olChoiceList.GetLength();
	if ( (ilLength>0) && ( olChoiceList[0]=='\n' ) )
		olChoiceList = olChoiceList.Right(ilLength-1);
	olStyle.SetControl(ipCtrlType).SetChoiceList(olChoiceList);
	if ( blReadOnly )
		GetParam( )->SetLockReadOnly(FALSE);
	BOOL blIsCurrentCell = IsCurrentCell(ipRow,ipCol) ;
	if ( blIsCurrentCell )
		ResetCurrentCell();

	SetStyleRange(CGXRange(ipRow, ipCol), olStyle);
	if ( blIsCurrentCell )
		SetCurrentCell(ipRow, ipCol);
	if ( blReadOnly )
		GetParam( )->SetLockReadOnly(TRUE);
}

		
void CGridControl::DisableRange ( CGXRange &ropRange )
{
	SetStyleRange( ropRange, CGXStyle().SetEnabled(FALSE).SetReadOnly(TRUE).SetInterior(ogColors[SILVER_IDX]) );
}

//////////////////////////////////////////////////////////////////////
//	Klasse f�r ein Grid mit �berschrift
//  Baseclass:	CGridControl
//////////////////////////////////////////////////////////////////////


CTitleGridControl::CTitleGridControl()
{
}


CTitleGridControl::CTitleGridControl(CWnd *popParent, UINT nID, ROWCOL nCols, ROWCOL nRows )
	:CGridControl ( popParent, nID, nCols, nRows+1 )
{
	
	// Assign a combo box to a cell
	BOOL ok ;
	CGXStyle style;
	
	SetStyleRange(CGXRange(0,0), 
				  CGXStyle().SetControl(GX_IDS_CTRL_HEADER)  );
	ok = SetFrozenRows ( 1,1 );
	ChangeRowHeaderStyle ( CGXStyle().SetValue("")  );
	ok = SetCoveredCellsRowCol( 0, 0, 0, nCols );
	SetRowHeadersText ();
}

CTitleGridControl::~CTitleGridControl()
{
}


void CTitleGridControl::SetTitle ( CString &opTitle )
{	
	omTitle = opTitle;
	SetValueRange( CGXRange(0,0), omTitle );
}

CString CTitleGridControl::GetTitle ()
{
	return omTitle ;
}

const CString &CTitleGridControl::GetValue ( ROWCOL nRow, ROWCOL nCol ) 
{
	return GetValue ( nRow+1, nCol );	
}

BOOL CTitleGridControl::SetValue ( ROWCOL nRow, ROWCOL nCol, CString &cStr ) 
{
	return SetValueRange( CGXRange(nRow+1, nCol), cStr );
}	


BOOL CTitleGridControl::EnableGrid ( BOOL enable )
{
	BOOL blRet;
	
	int ilRowCount = GetRowCount();
	int ilColCount = GetColCount();
	
	if (ilRowCount >= 2 && ilColCount >= 1)
	{
		if ( enable )
			blRet = SetStyleRange(CGXRange(2, 1, ilRowCount, ilColCount), CGXStyle().SetInterior(ogColors[WHITE_IDX]).SetEnabled(enable));
		else
		{
			//SetStyleRange(CGXRange().SetTable(), CGXStyle().SetEnabled(TRUE) );
			SetCurrentCell( 0, 0  ) ;
			blRet = SetStyleRange(CGXRange(2, 1, ilRowCount, ilColCount), CGXStyle().SetInterior(ogColors[SILVER_IDX]).SetEnabled(enable));
		}
		blRet &= SetStyleRange(CGXRange().SetTable(), CGXStyle().SetEnabled(enable));
			
	}
//	Redraw();
	return blRet;
}


BOOL CTitleGridControl::InsertBottomRow ()
{
	BOOL blRet = CGridControl::InsertBottomRow ();
	char pclString[11];
	ROWCOL ilRowCount = GetRowCount ();

	SetValue ( ilRowCount-1, 0, (CString)itoa(ilRowCount-1,pclString,10) ) ;

	return blRet;
}


bool CTitleGridControl::SortTable ( ROWCOL ipRowClicked, ROWCOL ipColClicked )
{
	ROWCOL ilRowCount = GetRowCount ();

	if ( ipRowClicked != 1 )
		return false;

	//--- Sortieren je nachdem welcher Header gedr�ckt wurde
	CGXSortInfoArray  sortInfo;
	sortInfo.SetSize( 1 );
		
	// toggle between sorting in ascending / descending order with each click
	bmSortAscend = !bmSortAscend;
	sortInfo[0].sortOrder = bmSortAscend ? CGXSortInfo::ascending : 
										   CGXSortInfo::descending;
	sortInfo[0].nRC = ipColClicked;                       
	sortInfo[0].sortType = CGXSortInfo::autodetect;  
	if ( bmAutoGrow && GetValueRowCol ( ilRowCount, 1 ).IsEmpty () )
		ilRowCount--;
	if ( ilRowCount > 2 )
		SortRows( CGXRange().SetRows(2, ilRowCount), sortInfo); 
	SetRowHeadersText ();
	return true;
}

//  Zeilek�pfe neu beschriften
void CTitleGridControl::SetRowHeadersText ()
{
	char pclString[11];
	for ( UINT i=1; i<GetRowCount(); i++ )
		SetValue ( i, 0, (CString)itoa(i,pclString,10) ) ;
}

void CTitleGridControl::RemoveOneRow ( ROWCOL nRow )
{
	RemoveRows( nRow, nRow );
	SetRowHeadersText ();
}

BOOL CTitleGridControl::OnStartEditing(ROWCOL nRow, ROWCOL nCol)
{
	if ( (nRow<2) || (nCol<1) )
	{
		TRACE("User tried to edit in Row=%u, Col=%u\n", nRow, nCol );
		return FALSE;
	}
	else
		return CGridControl::OnStartEditing(nRow, nCol);
}
	

