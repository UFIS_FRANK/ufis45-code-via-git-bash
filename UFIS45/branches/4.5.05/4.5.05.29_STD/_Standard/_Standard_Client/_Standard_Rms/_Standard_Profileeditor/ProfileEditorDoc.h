// ProfileEditorDoc.h : interface of the CProfileEditorDoc class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_PROFILEEDITORDOC_H__697CCAFC_B2F4_11D3_93B8_00001C033B5D__INCLUDED_)
#define AFX_PROFILEEDITORDOC_H__697CCAFC_B2F4_11D3_93B8_00001C033B5D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <ccsptrarray.h>
#include <validitydlg.h>

class CChartData;

#define HINT_CLASS_CREATED		1
#define HINT_CLASS_DELETED		2
#define HINT_CLASS_CHANGED		3
#define HINT_CONDITION_CHANGED	4
#define HINT_HEADER_CHANGED		5
#define HINT_PATTERN_CHANGED	6
#define HINT_CLASS_HEAD_CHANGED	7

#define NO_CHANGE	0
#define PRO_CHANGED 1
#define PXC_CHANGED 2
#define PXA_CHANGED 4
#define VTP_CHANGED 8
#define VAL_CHANGED 16

#define WM_PREPARE_CLOSING		WM_USER+400	

class CProfileEditorDoc : public CDocument
{
protected: // create from serialization only
	CProfileEditorDoc();
	DECLARE_DYNCREATE(CProfileEditorDoc)

// Attributes
public:
	CCSPtrArray<CChartData> omChartData;
	CString omDcop;
	bool    bmFdco;
	CString omDcup;
	bool    bmFdcu;
	CString omDgap;
	bool    bmFdga;
	CString omDsep;
	bool    bmFdse;
	CString omEvtd;
	CString omPrio;
	CString omPrst;
	CString omExcl;
	CString omRemark;
	CString omUrno;
	CString omUarc;
	VALDATA	rmValData;
	UINT	imModifications;

	int imProDcopIdx;
	int imProDcupIdx;
	int imProDgapIdx;
	int imProDsepIdx;
	int imProEvtdIdx;
	int imProFdcoIdx;
	int imProFdcuIdx;
	int imProFdgaIdx;
	int imProFdseIdx;
	int imProFisuIdx;
	int imProNameIdx;
	int imProPrioIdx;
	int imProPrstIdx;
	int imProRemaIdx;
	int imProUarcIdx;
	int imProUrnoIdx;
	int imProUtplIdx;
	int imProExclIdx;
	bool bmValid;
	bool bmDeletedLocal;
 
	// Operations
public:
	bool AddClass ( CView *popSender, long lpClassUrno, bool bpSendUpdate=true );

	bool DeleteClass ( CView *popSender, long lpClassUrno );
	int FindClass ( long lpClassUrno );
	bool SetOneDistribution ( CView *popSender, char *pcpDBField, 
							  bool bpValFlag, float bpVal );
	bool SetSingleDBField ( CView *popSender, CString opField, CString opValue );
	int GetDataDisplayIndex (CChartData *popData);
	bool ChangeDPAXandDBAG ( CView *popSender, long lpClassUrno, 
							 CString *popDPAX, CString *popDBAG );
	bool ChangeTransactionTimes ( CView *popSender, long lpClassUrno, 
		 					      CString *popTTWO, CString *popTTBG );
	void SendMessageToAllViews(UINT message, WPARAM wParam=0, LPARAM lParam=0);
	void ProcessPXCChange ( UINT ipBCType, RecordSet *popData );
	void ProcessPROChange ( UINT ipBCType, RecordSet *popData );
	void ProcessPXAChange ( UINT ipBCType, RecordSet *popData );
	virtual void OnCloseDocument();
	void PatternChanged ( CChartData *popData, UINT ipBits );	
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CProfileEditorDoc)
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
	virtual BOOL OnOpenDocument(LPCTSTR lpszPathName);
	protected:
	virtual BOOL SaveModified();
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CProfileEditorDoc();
	BOOL DoFileSave();
	void SetPathName( LPCTSTR lpszPathName, BOOL bAddToMRU = TRUE );
	bool DeleteFromDB ();
	void SetModified ( UINT ipBits );
	void UpdClassModifiedBits ();
	int CopyClasses ( CProfileEditorDoc *popSource );


#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
	bool SaveProRecord (bool bpUpdate);
	bool SaveClasses ();
	bool LoadProRecord (CString &ropProfileUrno);
	bool LoadClasses (CString &ropProfileUrno);
	bool LoadOneClass ( RecordSet &ropPXCRecord );


// Generated message map functions
protected:
	//{{AFX_MSG(CProfileEditorDoc)
	afx_msg void OnEditCopy();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

void ProcessBCInDoc ( void *popInstance, int ipDDXType,
					  void *vpDataPointer, CString &ropInstanceName);
/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PROFILEEDITORDOC_H__697CCAFC_B2F4_11D3_93B8_00001C033B5D__INCLUDED_)
