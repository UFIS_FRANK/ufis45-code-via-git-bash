// HistoColumn.cpp: implementation of the CHistoColumn class.
//
//////////////////////////////////////////////////////////////////////

#include <stdafx.h>

#include <ccsglobl.h>
#include <histogramm.h>
#include <HistoColumn.h>
#include <ChartData.h>

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

#define COLOR_REGULAR	0
#define COLOR_MARKED	1
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CHistoColumn::CHistoColumn(CHistogramm *popParent, int ipStart, float fpValue)
{
	imStart = ipStart;
	fmValue = fpValue;
	pomHistogramm = popParent;
	ilColor = COLOR_REGULAR;
}

CHistoColumn::CHistoColumn(CHistogramm *popParent, ValInPeriod &rrpValRec )
{
	imStart = rrpValRec.start;
	fmValue = rrpValRec.val;
	ilColor = rrpValRec.homogen ? COLOR_REGULAR : COLOR_MARKED;
	pomHistogramm = popParent;
}


CHistoColumn::~CHistoColumn()
{

}

bool CHistoColumn::CalcWorldRect()
{
	if ( !pomHistogramm )
		return false;
	int ilEnd = imStart + pomHistogramm->imColumnWidth;
	omWorldRect.bottom = 0;
	omWorldRect.top = (int)(fmValue*pomHistogramm->fmUnitsY);
	omWorldRect.left = (int)((float)imStart*pomHistogramm->fmUnitsX);
	omWorldRect.right = (int)((float)ilEnd*pomHistogramm->fmUnitsX);
	return true;
}

bool CHistoColumn::CalcDisplayRect()
{
	if ( !pomHistogramm )
		return false;
	omDisplayRect.bottom = omWorldRect.bottom - pomHistogramm->omRightBottom.y;
	omDisplayRect.top = omDisplayRect.bottom + omWorldRect.Height();
	omDisplayRect.left = omWorldRect.left - (int)((float)pomHistogramm->omRightBottom.x*pomHistogramm->fmUnitsX);
	omDisplayRect.right = omWorldRect.right - (int)((float)pomHistogramm->omRightBottom.x*pomHistogramm->fmUnitsX);
	omDisplayRect.OffsetRect( pomHistogramm->omViewPort.BottomRight() );
/*
	omDisplayRect.left = pomHistogramm->GetXClient ( imStart );
	omDisplayRect.right = pomHistogramm->GetXClient ( imStart + pomHistogramm->imColumnWidth );
*/
	//omDisplayRect.IntersectRect ( &omDisplayRect, &(pomHistogramm->omViewPort) );
	return true;
}

void CHistoColumn::Draw ( CDC *popCDC )
{
	if ( popCDC && (omDisplayRect.Width()>0) && (omDisplayRect.Height()>0) )
	{
		omDisplayRect.IntersectRect ( &omDisplayRect, &(pomHistogramm->omViewPort) );
		popCDC->FillSolidRect ( omDisplayRect, (ilColor==COLOR_MARKED) ?
//												pomHistogramm->ilMarkedColColor :
//												pomHistogramm->ilRegularColColor );   
												igMarkedColColor : igRegularColColor );   
	}
}	