// 
//  Must be add this to xxxxx.CLW in [General Info] section.
//  ExtraDDXCount=6
//  ExtraDDX1=E;;CCS_DDMMYY;CTime;;CCSddmmyy;This is a new type of DDMMYY.
//  ExtraDDX2=E;;CCS_DDMM;CTime;;CCSddmm;This is a new type of DDMM.
//  ExtraDDX3=E;;CCS_Date;CTime;;CCSDate;This is a new type of Date.
//  ExtraDDX4=E;;CCS_Month;CTime;;CCSMonth;This is a new type of Month. 
//  ExtraDDX5=E;;CCS_Year;CTime;;CCSYear;This is a new type of Year. 
//  ExtraDDX6=E;;CCS_Time;CTime;;CCSTime;This is a new type of Time.
//
//  
#ifndef __DDXDDV_H__
#define __DDXDDV_H__

#ifndef DAYINMONTH
#define DAYINMONTH
extern int g_dayInmonth[];
#endif 

BOOL CheckIfDateValid( CString str ); 
BOOL CheckIfMonthValid( CString str ); 
BOOL CheckIfYearValid( CString str );
BOOL CheckIfDDMMYYValid( CString &str ); 
BOOL CheckIfDDMMValid( CString &str ); 
BOOL CheckIfHHMMValid( CString &str );
/*
void WINAPI DDX_CCSDate( CDataExchange *pDX, int nIDC, CTime& tm ); 
void WINAPI DDX_CCSMonth( CDataExchange *pDX, int nIDC, CTime& tm ); 
void WINAPI DDX_CCSYear( CDataExchange *pDX, int nIDC, CTime& tm );
void WINAPI DDX_CCSddmmyy( CDataExchange *pDX, int nIDC, CTime& tm );
void WINAPI DDX_CCSTime( CDataExchange *pDX, int nIDC, CTime& tm );
void WINAPI DDX_SSSNumeric( CDataExchange *pDX, int nIDC, CString& str );
void WINAPI DDX_CCSddmm( CDataExchange *pDX, int nIDC, CTime& tm );
*/
BOOL DateStringToDate(CString &opString, CTime &opTime);
BOOL DateStringToDateExt(CString &opString, CTime &opTime);
CString DBStringToDateString(CString opStr);

#endif //__DDXDDV_H__
