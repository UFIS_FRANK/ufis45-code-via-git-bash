// ChartData.cpp: implementation of the CChartData class.
//
//////////////////////////////////////////////////////////////////////

#include <stdafx.h>

#include <cedabasicdata.h>

#include <ccsglobl.h>
#include <basicdata.h>
#include <ProfileEditor.h>
#include <ProfileEditorDoc.h>
#include <ChartData.h>
#include <utilities.h>
#include <ValuesGrid.h>
#include <NewNameDlg.h>

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CChartData::CChartData( CProfileEditorDoc *popDoc )
{
	pomDocument = popDoc;
	omUval = "";
	imMinStep = 5;
	imDspStep = 10;
	omDBAG = omDPAX = "";
	omTTWO = omTTBG = "";
	imVtpPberIdx = ogBCD.GetFieldIndex ("VTP", "PBER" );
	imVtpPenrIdx = ogBCD.GetFieldIndex ("VTP", "PENR" );
	imVtpValuIdx = ogBCD.GetFieldIndex ("VTP", "VALU" );
	imVtpUrnoIdx = ogBCD.GetFieldIndex ("VTP", "URNO" );

	bmPXCModified = bmPXAModified = bmVTPModified = false;
	flEpsHomogen = (float)0.005;		//  Schwelle wann Werte nicht mehr homogen sind
}

CChartData::~CChartData()
{
	omRawData.DeleteAll();
	omDspData.DeleteAll();
	omInputData.DeleteAll();
}

bool CChartData::ReadData (CValuesGrid *popGrid/*=0*/ )
{
	CString					olWert, olStart, olEnd;
	int						i=0;
	ValInPeriodExt			rlInputValues;
	CCSPtrArray<RecordSet>	olVtpRecords;

	ResetModified (PXA_CHANGED|VTP_CHANGED);
	omRawData.DeleteAll();
	omInputData.DeleteAll();
	if ( popGrid )
		popGrid->ResetValues ();

	if ( omUval.IsEmpty () )
		return false;
	omProfileName = GetProfileName ( omUval );
	if ( ogBCD.GetField ( "PXA", "URNO", omUval, "MSXM", olWert ) &&
		 ( sscanf(olWert,"%d", &i) >= 1 ) )
		imMinStep = i;
	imDspStep = (imDspStep/imMinStep)*imMinStep;
	imDspStep = max (imDspStep,imMinStep);
	
	ogBCD.GetRecords ( "VTP", "UVAL", omUval, &olVtpRecords );

	rlInputValues.status = rlInputValues.newstatus = DATA_UNCHANGED;
	for ( i=0; i<olVtpRecords.GetSize(); i++ )
	{
		olStart = olVtpRecords[i].Values[imVtpPberIdx];
		olEnd   = olVtpRecords[i].Values[imVtpPenrIdx];
		olWert  = olVtpRecords[i].Values[imVtpValuIdx];

		if ( sscanf ( olStart, "%d", &(rlInputValues.start) ) <= 0 )
			continue;
		if ( sscanf ( olEnd, "%d", &(rlInputValues.end) ) <= 0 )
			continue;
		if ( sscanf ( olWert, "%f", &(rlInputValues.val) ) <= 0 )
			continue;
		//  Werte in Datenbank in Sekunden, Eingabe u. Anzeige aber in Minuten
		rlInputValues.start /= 60;
		rlInputValues.end /= 60;
		rlInputValues.urno = olVtpRecords[i].Values[imVtpUrnoIdx];
		omInputData.New ( rlInputValues );
	}
	olVtpRecords.DeleteAll();
	CalcRawData ();
	return true;
}

bool CChartData::CalcRawData ()
{
	ValInPeriod		rlRawValues;
	ValInPeriodExt	rlInpValues;
	int				ilTimes;

	omRawData.DeleteAll();
	if ( omInputData.GetSize() <= 0)
		return true;	//  keine Daten da
	
//	omInputData.Sort ( CompareStartDescending );

//	rlValues.val = 0.0;
	rlRawValues.homogen = true;
	for ( int i=0; i<omInputData.GetSize(); i++ )
	{
		rlInpValues = omInputData[i];
		if ( rlInpValues.status == DATA_DELETED )
			continue;
		ilTimes = ( rlInpValues.end - rlInpValues.start ) / imMinStep;
		rlRawValues.val = rlInpValues.val / (float)ilTimes;
		for ( int j=0; j<ilTimes; j++ )
		{
			rlRawValues.start = rlInpValues.start + j*imMinStep;
			omRawData.New(rlRawValues);
		}
	}
	return true;
}

bool CChartData::SetDspStep	( int ipDspStep )
{
	if ( imDspStep == ipDspStep )
		return true;
	
	if ( ipDspStep <= 0 )
		return false;
	if ( ipDspStep % imMinStep != 0 )	//  Schrittweite f�r Darstellung mu�
		return false;					//  Vielfaches von im MinStep sein
	imDspStep = ipDspStep;
	SetModified (PXC_CHANGED);
	return CalcDspData();
}

bool CChartData::SetMinStep	( int ipMinStep )
{
	if ( ipMinStep == imMinStep )
		return true;
	if ( ipMinStep <= 0 )
		return false;
	for ( int i=0; i<omInputData.GetSize (); i++ )
	{
		int ilWidth = omInputData[i].end - omInputData[i].start;
		if ( ilWidth % ipMinStep )			//  jedes vorhandene Interval mu� 
			return false;					//  Vielfaches von ipMinStep sein
	}
	imMinStep = ipMinStep;
	SetModified (PXA_CHANGED);
	return CalcRawData ();
}


bool CChartData::CalcDspData ()
{
	ValInPeriod	rlValues, rlRawValues;
	int			ilDspStart;
	int			ilValsPerPeriod=0;		//  #Werte, die in ein Darstellungsinterval passen
	int			ilValsInActPeriod=0;	//  #Werte im aktuellen Darstellungsinterval 
	float       flFirstVal=0.0;
	
	omDspData.DeleteAll();
	if ( omRawData.GetSize() <= 0)
		return true;	//  keine Daten da
	
	omRawData.Sort ( CompareStartDescending );

	rlValues.val = 0.0;
	rlValues.homogen = true;
			
	ilValsPerPeriod = imDspStep / imMinStep;
	flFirstVal = omRawData[0].val;	//  ersten Wert im Interval merken
			
	for ( int i=0; i<omRawData.GetSize(); i++ )
	{
		rlRawValues = omRawData[i];
		if ( rlRawValues.start % imDspStep )
		{
			if ( rlRawValues.start >= 0 )
				ilDspStart = ( rlRawValues.start / imDspStep ) * imDspStep;
			else
				ilDspStart = ( rlRawValues.start / imDspStep - 1 ) * imDspStep;
		}
		else
			ilDspStart = rlRawValues.start;
		//  Wenn neues Interval -> bisheriges in omDspData einf�gen
		if ( ( i>0 ) && ( rlValues.start != ilDspStart ) )
		{
			rlValues.homogen &= (ilValsInActPeriod==ilValsPerPeriod);
			omDspData.New ( rlValues );		
			rlValues.val = 0.0;
			rlValues.homogen = true;
			flFirstVal = omRawData[i].val;	//  ersten Wert im Interval merken
			ilValsInActPeriod = 0;
		}
		else
		{
			//  wenn einer der �brigen Werte des Intervals mehr als
			//  flEpsHomogen vom ersten abweicht, ist das Interval heterogen
			rlValues.homogen = ( fabs(flFirstVal - omRawData[i].val ) < flEpsHomogen);
		}		
		rlValues.start = ilDspStart;
		rlValues.val += omRawData[i].val;
		ilValsInActPeriod ++;
	}
	rlValues.homogen &= (ilValsInActPeriod==ilValsPerPeriod);
	omDspData.New ( rlValues );
	return true;
}

void CChartData::Reset()
{
	omInputData.DeleteAll();
	omRawData.DeleteAll();
	omDspData.DeleteAll();
	omUval = omProfileName = "";
	bmVTPModified = bmPXAModified = false;
	SetModified (PXC_CHANGED);
}

bool CChartData::SaveModified ()
{
	bool blOk;

	blOk = SavePXA ();
	if ( blOk )
		blOk = SaveModifiedPXC ();
	return blOk;
}

bool CChartData::SavePXA ()
{
	bool blRet;
	if ( omProfileName.IsEmpty () )
	{
		//AfxMessageBox ( "Error: Name is empty.", MB_OK );
		LangMsgBox ( 0, IDS_NO_NAME );
		return false;
	}
	int ilNewPXAName = AskSaveUsedPXANewName (  );		//hag20010712
	if ( ilNewPXAName==IDCANCEL )
		return false;	// wurde nicht gespeichert

	bool	blUpdate = !omUval.IsEmpty() ;
	CString	olUrno;
	if ( ogBCD.GetField ( "PXA", "PXAN", omProfileName, "URNO", olUrno ) )
	{	//  Name bereits vergeben
		if ( olUrno != omUval )
		{/*
			CString olText;
			olText = "A check-in pattern with this name already exists. Please type in a different name.";
			AfxMessageBox ( olText, MB_OK );	*/
			LangMsgBox ( 0, IDS_DUPL_PATTERN_NAME );
			return false;
		}
	}
	blRet = SaveModifiedPXA ( blUpdate );
	if ( ilNewPXAName==IDYES )
	{
		//  update all views
		CDWordArray olHintObject;
		olHintObject.Add ( lmCounterClass );
		pomDocument->UpdateAllViews ( 0, HINT_CLASS_CHANGED, &olHintObject );
	}
	return blRet;
}

bool CChartData::SaveModifiedPXA ( bool bpUpdate )
{
	CString olOldName, olOldMinStep, olActMinStep ;
	bool	blPXAChanged=true, blOk=true;

	if ( !pomDocument )	//  evtl. Fehlerbehandlung
		return false;

	olActMinStep.Format ( "%d", imMinStep );
	if ( bpUpdate )
	{	
		olOldName = ogBCD.GetField ( "PXA", "URNO", omUval, "PXAN" ) ;
		olOldMinStep = ogBCD.GetField ( "PXA", "URNO", omUval, "MSXM" ) ;
		blPXAChanged = ( olOldName != omProfileName ) || 
					   ( olActMinStep != olOldMinStep );
	}
	else
		omUval.Format ( "%lu", ogBCD.GetNextUrno() );
	if ( !bmVTPModified && !blPXAChanged )
		return true;        // ok to continue

	pomDocument->BeginWaitCursor();
	DWORD	start, now, diff;
	UINT	ilModified=0, ilNew=0, ilDeleted=0, ilAll;

	if ( bmVTPModified )		//  Werte ge�ndert ?
	{
		RecordSet olVTPRec(ogBCD.GetFieldCount("VTP") );
		start = GetTickCount();
		int ilVtpUrnoIdx = ogBCD.GetFieldIndex ( "VTP", "URNO" );

		ilAll = omInputData.GetSize();

		for ( int i=omInputData.GetSize()-1; i>=0; i-- )
		{
			if ( omInputData[i].status == DATA_CHANGED )
			{
				if ( !ogBCD.GetRecord ( "VTP", "URNO", omInputData[i].urno, olVTPRec ) )
					omInputData[i].urno.Empty();
				if ( omInputData[i].urno.IsEmpty() )
				{
					TRACE ( "Warning: Update of value with empty Urno changed to Insert\n" );		
					omInputData[i].status = DATA_NEW;
				}
			}
			if ( ( omInputData[i].status == DATA_NEW ) && 
				 !omInputData[i].urno.IsEmpty() )
				TRACE ( "Warning: Insert new Value with Urno=%s\n", 
						omInputData[i].urno );
			olVTPRec[ogBCD.GetFieldIndex ( "VTP", "TABN" )] = "PXA"; 
			olVTPRec[ogBCD.GetFieldIndex ( "VTP", "UVAL" )] = omUval;
			switch ( omInputData[i].status )
			{
				case DATA_UNCHANGED:
					continue;
				case DATA_DELETED:
					if ( !omInputData[i].urno.IsEmpty() )
					{
						ilDeleted ++;
						blOk &= ogBCD.DeleteRecord ( "VTP", "URNO", omInputData[i].urno );
					}
					omInputData.DeleteAt ( i );
					break;
				case DATA_NEW:
				case DATA_CHANGED:
					//  Werte in Datenbank in Sekunden, Eingabe u. Anzeige aber in Minuten
					olVTPRec[imVtpPberIdx].Format("%d", omInputData[i].start*60); 
					olVTPRec[imVtpPenrIdx].Format("%d", omInputData[i].end*60);
					olVTPRec[imVtpValuIdx].Format("%.3f", omInputData[i].val);
					if ( omInputData[i].status ==  DATA_NEW )
					{
						omInputData[i].urno.Format("%lu", ogBCD.GetNextUrno());
						olVTPRec[imVtpUrnoIdx] = omInputData[i].urno;
						blOk &= ogBCD.InsertRecord ( "VTP", olVTPRec );
						ilNew++;
					}
					else
					{
						blOk &= ogBCD.SetRecord("VTP", "URNO", omInputData[i].urno, olVTPRec.Values );
						ilModified++;
					}
					break;
			}				
			
		}
		blOk &= ogBCD.Save ( "VTP" );
		if ( !blOk )
		{/*
			olOldName = "Error on saving changes for check-in pattern %s !" ;
			olOldName.Replace ( "%s", omProfileName );
			AfxMessageBox ( olOldName, MB_ICONSTOP|MB_OK );*/
			FileErrMsg ( 0, IDS_SAVE_PATTERN_ERR, pCHAR(omProfileName), 0, MB_ICONSTOP|MB_OK ); 
		}
		else
			for ( i=0; i<omInputData.GetSize(); i++ )
				omInputData[i].status = omInputData[i].newstatus = DATA_UNCHANGED;
	

		now = GetTickCount();
		diff = now-start;
		CString olMsg;

		DWORD ilMin, ilSec;
		ilMin = diff/60000;
		ilSec = diff%60000;
		ilSec/=1000;
		olMsg.Format ( "Datens�tze gesamt: %d\nGe�ndert: %u, Neu: %u, Gel�scht %u\nZeit: %d:%02d,%03d", 
					   ilAll, ilModified, ilNew, ilDeleted, ilMin, ilSec, diff%1000 );
		//AfxMessageBox ( olMsg, MB_ICONINFORMATION|MB_OK );		//  Testausgabe
	}
	
	//  �nderung:  Auch wenn blPXAChanged==false PXA-Satz speichern, damit 
	//			   �ber PXA-BroadCast auch neue VTP-Werte gelesen werden
	if ( /*blPXAChanged &&*/ blOk )		//  PXA-Satz neu oder ge�ndert
	{	//	PXA-Satz speichern
		RecordSet olPXARec(ogBCD.GetFieldCount("PXA"));
		olPXARec.Values[ogBCD.GetFieldIndex("PXA","URNO")] = omUval;
		olPXARec.Values[ogBCD.GetFieldIndex("PXA","MSXM")] = olActMinStep;
		olPXARec.Values[ogBCD.GetFieldIndex("PXA","PXAN")] = omProfileName;
		if ( bpUpdate )
		{
			blOk = ogBCD.SetRecord ( "PXA", "URNO", omUval, olPXARec.Values );
			blOk &= ogBCD.Save ( "PXA" );
		}
		else
			blOk = ogBCD.InsertRecord( "PXA", olPXARec, true );
	}	
	if ( blOk )
		//bmVTPModified = false;
		ResetModified (PXA_CHANGED|VTP_CHANGED);
	pomDocument->EndWaitCursor();
	return blOk;
}

bool CChartData::SaveModifiedPXC ()
{
	bool		blOk;
	CString		olCCCUrno;
	CString		olPXCUrno; 
	RecordSet	olPXCRec(ogBCD.GetFieldCount("PXC") ); 

	if ( !bmPXCModified )
		return true;

	if ( !pomDocument )	//  evtl. Fehlerbehandlung
		return false;

	olCCCUrno.Format ( "%lu", lmCounterClass );
	//  Wenn Datensatz in PXC mit gleichen Urnos f�r Profil und Counter-Class 
	//  existieren, Update !!!
	olPXCUrno = ogBCD.GetFieldExt("PXC", "UCCC","UPRO", olCCCUrno, 
								  pomDocument->omUrno, "URNO" );
	bool blUpdate = false;
	if ( !olPXCUrno.IsEmpty() && 
		 ogBCD.GetRecord ( "PXC", "URNO", olPXCUrno, olPXCRec ) )
		 blUpdate = true;
	else
	{
		olPXCRec.Values[ogBCD.GetFieldIndex("PXC", "URNO")].Format ("%lu", ogBCD.GetNextUrno() );
		olPXCRec.Values[ogBCD.GetFieldIndex("PXC", "UCCC")] = olCCCUrno;
		olPXCRec.Values[ogBCD.GetFieldIndex("PXC", "UPRO")] = pomDocument->omUrno;
	}
	olPXCRec.Values[ogBCD.GetFieldIndex("PXC", "UPXA")] = omUval;
	olPXCRec.Values[ogBCD.GetFieldIndex("PXC", "DBAG")] = omDBAG;
	int ilDspIndex = pomDocument->GetDataDisplayIndex (this); 
	if (ilDspIndex >= 0)
		olPXCRec.Values[ogBCD.GetFieldIndex("PXC", "DISP")].Format("%d", ilDspIndex ); 
	else
		olPXCRec.Values[ogBCD.GetFieldIndex("PXC", "DISP")] = "";
	olPXCRec.Values[ogBCD.GetFieldIndex("PXC", "DPAX")] = omDPAX;
	olPXCRec.Values[ogBCD.GetFieldIndex("PXC", "PXCN")] = 
									ogBCD.GetField("CCC", "URNO", olCCCUrno, "CICN");
	olPXCRec.Values[ogBCD.GetFieldIndex("PXC", "SCXM")].Format("%d", imDspStep);

	if ( igPxcTtbgIdx>=0 ) // New field "TTBG" already in DB
		olPXCRec.Values[igPxcTtbgIdx] = omTTBG;
	if ( igPxcTtwoIdx>=0 ) // New field "TTWO" already in DB
		olPXCRec.Values[igPxcTtwoIdx] = omTTWO;

	if ( blUpdate )
		blOk = ogBCD.SetRecord ( "PXC", "URNO", olPXCUrno, olPXCRec.Values, true );
	else
		blOk = ogBCD.InsertRecord( "PXC", olPXCRec, true );
	if ( blOk )
		//bmPXCModified = false;
		ResetModified (PXC_CHANGED);
	omUrno = olPXCRec.Values[ogBCD.GetFieldIndex("PXC", "URNO")];
	return blOk;
}


bool CChartData::AskForSaveChanges()
{
	int ilRet = IDNO;
	if ( bmVTPModified || bmPXAModified )
	{/*
		CString olText;
		olText = "Save changes of displayed check-in pattern %s?";
		olText.Replace ( "%s", omProfileName );
		ilRet = AfxMessageBox ( olText, MB_ICONQUESTION|MB_YESNO );*/
		ilRet = FileErrMsg ( 0, IDS_ASK_SAVE_PATTERN, pCHAR(omProfileName), 0,
							 MB_ICONQUESTION|MB_YESNO ); 
	}	
	if ( ilRet == IDYES )
		return SavePXA ();
	else
		return true;
}

void CChartData::SetModified (UINT ipBits)
{
	if  (ipBits&PXC_CHANGED)
		bmPXCModified = true;
	if  (ipBits&PXA_CHANGED)
		bmPXAModified = true;
	if  (ipBits&VTP_CHANGED)
		bmVTPModified = true;
	if ( pomDocument ) 
	{
		pomDocument->SetModified ( ipBits );
		pomDocument->PatternChanged ( this, ipBits );
	}
}


void CChartData::ResetModified (UINT ipBits)
{
	if  (ipBits&PXC_CHANGED)
		bmPXCModified = false;
	if  (ipBits&PXA_CHANGED)
		bmPXAModified = false;
	if  (ipBits&VTP_CHANGED)
		bmVTPModified = false;
	if ( pomDocument ) 
		pomDocument->UpdClassModifiedBits ();
}

bool CChartData::CheckInput ( bool bpDspError/*=true*/ )
{
	int i, ilValues = omInputData.GetSize();
	ValInPeriodExt *polVal1, *polVal2;
	CString olText;
	
	omInputData.Sort ( CompareInputAscending );

	for ( i=0; i<ilValues;i++ )
	{
		polVal1 = &(omInputData[i]);
		if ( polVal1->status == DATA_DELETED )
			continue;
		//  n�chsten nicht gel�schten Datensatz suchen
		//  polVal2 = (i+1<ilValues) ? &(omInputData[i+1]) : 0;
		polVal2 = 0;
		for ( int j = i+1; !polVal2&&(j<ilValues); j++ )
			if ( omInputData[j].status != DATA_DELETED )
				polVal2 = &(omInputData[j]);
		
		if ( ( polVal1->start % imMinStep ) || ( polVal1->end % imMinStep ) )
		{
			//olText = "At least one start or end time found, that doesn't match the minimum interval.";
			olText = GetString ( IDS_PATTERN_VAL_ERR1 );
			break;
		}
		if ( polVal1->start >= polVal1->end )
		{
			//olText = "At least one value found with start time >= end time.";
			olText = GetString ( IDS_PATTERN_VAL_ERR2 );
			break;
		}
		if ( polVal2 && (polVal1->end>polVal2->start) )
		{
			//olText = "At least two overlapping intervals found.";
			olText = GetString ( IDS_PATTERN_VAL_ERR3 );
			break;
		}
	}
	if ( bpDspError && !olText.IsEmpty() )
		AfxMessageBox ( olText, MB_ICONINFORMATION|MB_OK );
	return olText.IsEmpty() ? true : false;
}
	
void CChartData::operator =( const CChartData& srcData )
{
	pomDocument = srcData.pomDocument;
	omUval = srcData.omUval ;
	omProfileName = srcData.omProfileName;

	lmCounterClass = srcData.lmCounterClass;
	imMinStep = srcData.imMinStep ;
	imDspStep = srcData.imDspStep ;
	omDBAG = srcData.omDBAG;
	omDPAX = srcData.omDPAX;
	omTTWO = srcData.omTTWO;
	omTTBG = srcData.omTTBG;

	bmPXCModified = true;
	bmPXAModified = bmVTPModified = false;
	flEpsHomogen = srcData.flEpsHomogen ;

	//Werte kopieren
	CopyValues ( srcData );
	/*
	int i;
	for ( i=0; i<srcData.omRawData.GetSize(); i++ )
		omRawData.New ( srcData.omRawData[i] );
	for ( i=0; i<srcData.omDspData.GetSize(); i++ )
		omDspData.New ( srcData.omDspData[i] );
	for ( i=0; i<srcData.omInputData.GetSize(); i++ )
		omInputData.New ( srcData.omInputData[i] );
	*/
}


static int CompareStartAscending (const ValInPeriod **prpValRec1, const ValInPeriod **prpValRec2)
{
	if ( (*prpValRec1)->start < (*prpValRec2)->start )
		return -1;
	else
		if ( (*prpValRec1)->start > (*prpValRec2)->start )
			return 1;
		else
			return 0;
}

static int CompareStartDescending (const ValInPeriod **prpValRec1, const ValInPeriod **prpValRec2)
{
	int ilRet = CompareStartAscending (prpValRec1, prpValRec2);
	return ilRet*-1;
}

static int CompareInputAscending (const ValInPeriodExt **prpValRec1, const ValInPeriodExt **prpValRec2)
{
	if ( (*prpValRec1)->start < (*prpValRec2)->start )
		return -1;
	else
		if ( (*prpValRec1)->start > (*prpValRec2)->start )
			return 1;
		else
			return 0;
}


CString GetClassName ( long lpClassUrno )
{
	CString olUrno;

	olUrno.Format ( "%lu", lpClassUrno );
	return ogBCD.GetField("CCC", "URNO", olUrno, "CICN" );
}

long GetClassUrno ( CString opClassName )
{
	CString olUrno;
	long	llUrno , llRet=-1;

	olUrno = ogBCD.GetField("CCC", "CICN", opClassName, "URNO" );

	if ( sscanf ( olUrno, "%ld", &llUrno ) > 0 )
		llRet = llUrno;
	return llRet;
}

CString GetProfileName ( CString opProfileUrno )
{
	CString olName = ogBCD.GetField("PXA", "URNO", opProfileUrno, "PXAN" );
	return olName;
}

//	AskSaveUsedPXANewName:
//	Test, ob verwendetes Pattern (omUval) ge�ndert wurde und ob es auch in 
//	anderen Profilen verwendet wird. Wenn ja, Abfrage, ob das Pattern unter
//	neuem Namen gespeichert werden soll
//  return:	IDYES, wenn Pattern unter neuem Namen gespeichert werden soll,
//			IDNO, altes Pattern soll ver�ndert werden,
//			IDCANCEL: Fehler kein Speichern
int CChartData::AskSaveUsedPXANewName (  )
{
	CString olForm, olMsg, olText;

	if ( omUval.IsEmpty() )
		return IDNO;		//  neues Pattern kann noch nirgends verwendet sein

	if  ( !bmPXAModified && !bmVTPModified )
		return false;		//  nur anderes Pattern zugeordnet, kein bestehendes editiert
	CString olUsedByUrnos = ogBCD.GetValueList("PXC", "UPXA", omUval, "UPRO");
	int		i, ilRet;

	if ( !olUsedByUrnos.IsEmpty() )
	{
		CStringArray olProUrnos;
		CStringList  olProNames;
		CString		 olProName;
		POSITION	 pos;

		ExtractItemList(olUsedByUrnos, &olProUrnos, ',' );
		for ( i=0; i<olProUrnos.GetSize(); i++ )
		{
			if ( (olProUrnos[i] != pomDocument->omUrno) &&
				  ogBCD.GetField ( "PRO", "URNO", olProUrnos[i], 
								  "NAME", olProName ) && 
				 !olProNames.Find ( olProName ) )
				olProNames.AddTail ( olProName ) ;
		}
		if ( olProNames.IsEmpty() )
			return IDNO;			//  Pattern wird nicht von anderem Profil verwendet
		olForm = GetString ( IDS_SAVE_USED_PATTERN );
		pos = olProNames.GetHeadPosition ();
		while ( pos )
		{
			olProName = olProNames.GetNext ( pos );
			olText += olProName;
			olText += pos ? ", " : ".";
		}
		olMsg.Format ( olForm, omProfileName, olText );
		ilRet = AfxMessageBox ( olMsg, MB_ICONQUESTION|MB_YESNOCANCEL );
		if ( ilRet == IDYES )
		{
			CNewNameDlg dlg;
			bool blNameOk;
			do 
			{
				if ( dlg.DoModal ()==IDCANCEL )
					return IDCANCEL;			//  nicht speichern
				blNameOk  = !dlg.m_Name.IsEmpty() && 
							!ogBCD.GetField ( "PXA", "PXAN", dlg.m_Name, 
											  "URNO", olText ); 

			}while ( !blNameOk );
			omUval.Empty();
			omProfileName = dlg.m_Name;
			bmPXAModified = bmVTPModified = bmPXCModified = true;
			for ( i=0; i<omInputData.GetSize(); i++ )
			{
				omInputData[i].status = DATA_NEW;
				omInputData[i].urno.Empty();
			}
			return IDYES;	// unter neuem Namen speichern
		}
		if ( ilRet == IDCANCEL )
			return IDCANCEL;	//  nicht speichern
	}
	return IDNO;	//  nicht untern neuem Namen speichern
}

void CChartData::CopyValues ( const CChartData& srcData )
{
	int i;
	omRawData.DeleteAll();
	omDspData.DeleteAll();
	omInputData.DeleteAll();

	for ( i=0; i<srcData.omRawData.GetSize(); i++ )
		omRawData.New ( srcData.omRawData[i] );
	for ( i=0; i<srcData.omDspData.GetSize(); i++ )
		omDspData.New ( srcData.omDspData[i] );
	for ( i=0; i<srcData.omInputData.GetSize(); i++ )
		omInputData.New ( srcData.omInputData[i] );
}