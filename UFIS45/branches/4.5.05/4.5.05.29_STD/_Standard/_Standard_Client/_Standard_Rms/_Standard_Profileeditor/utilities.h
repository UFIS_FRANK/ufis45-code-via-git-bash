#if !defined(UTILITIES_H__INCLUDED_)
#define UTILITIES_H__INCLUDED_

#include <ccsddx.h>
#include <ccsglobl.h>

template <class>class CCSPtrArray;
class RecordSet;
struct CHOICE_LISTS;

bool SetDlgItemLangText ( CWnd *popWnd, UINT ipIDC, UINT ipIDS );
bool SetWindowLangText ( CWnd *popWnd, UINT ipIDS );
bool EnableDlgItem ( CWnd *popWnd, UINT ipIDC, BOOL bpEnable, BOOL bpShow );
BOOL MoveDlgItem ( CWnd *popWnd, UINT ipIDC, int x, int y );
BOOL ResizeDlgItem ( CWnd *popWnd, UINT ipIDC, int cx, int cy );

void GetMapForRecordList ( CCSPtrArray<RecordSet> &ropRecordList, 
						   CMapStringToPtr &ropRecordMap, int ipRefFieldIndex );
bool GetPrivProfInt( LPCTSTR pcpSection, LPCTSTR pcpEntry, int ipDefault, 
					 int &ripValue, LPCTSTR pcpFileName );
void WriPrivProfInt( LPCTSTR pcpSection, LPCTSTR pcpEntry, int ipValue, 
					 LPCTSTR pcpFileName );
bool GetPrivProfString ( LPCTSTR pcpSection, LPCTSTR pcpEntry, LPCTSTR pcpDefault, 
					     LPTSTR pcpValue, DWORD ipSize, LPCTSTR pcpFileName );
int FileErrMsg ( HWND hwnd, char *lpText, char *filna, char* lpCaption=0, 
				 UINT uType=MB_OK );
int FileErrMsg ( HWND hwnd, UINT idcText, char *filna, UINT idcCaption=0, 
				 UINT uType=MB_OK );
int LangMsgBox ( HWND hwnd, UINT idcText, UINT idcCaption=0, UINT uType=MB_OK );
void SetStatusBarText ( CString &ropText );
bool SaveWindowPosition ( CWnd *popWnd, char *pcpIniEntry );
bool IniWindowPosition ( CWnd *popWnd, char *pcpIniEntry );
int FindStringInside ( CString opWhole, CString opSub, 
					   BOOL bpCaseSensitiv/*=TRUE*/ );
void ClientToView( CScrollView *popView, LPRECT lpRect );
void ClientToView( CScrollView *popView, LPPOINT lpPoint );
CString GetToolTip ( CHOICE_LISTS *popChoice, CString &ropSelValue );
CString GetGrpToolTip ( CHOICE_LISTS *popChoice, CString &ropSelValue );
bool DeleteByWhere(CString opTable, CString opRefField, CString opRefValue, bool bpDbSave=true);
bool SetBCTypes ( CString opTable );
void RegisterBC ( void *vpInstance, CString opInstanceName, CString opTable, 
				  DDXCALLBACK pfpCallBack, UINT ipTypes=BC_ALL );
int CalcBCNumber ( CString &ropTable, UINT ipType );
bool GetBCType ( int ipDDXType, CString &ropTable, UINT &ripType );
bool InternalBC ( void *vpInstance, CString opTable, UINT ipType, 
				  void *vpDataPointer );

#endif // !defined(UTILITIES_H__INCLUDED_)
