// RecentRecordList.h: interface for the CRecentRecordList class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_RECENTRECORDLIST_H__2645E6E3_D2FB_11D3_93D0_00001C033B5D__INCLUDED_)
#define AFX_RECENTRECORDLIST_H__2645E6E3_D2FB_11D3_93D0_00001C033B5D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <afxadv.h>

class CRecentRecordList : public CRecentFileList  
{
public:
	CRecentRecordList( LPCTSTR lpszSection, LPCTSTR lpszEntryFormat, int nSize );
	virtual ~CRecentRecordList();
//  Operations
	void Add(LPCTSTR lpszPathName);
};

#endif // !defined(AFX_RECENTRECORDLIST_H__2645E6E3_D2FB_11D3_93D0_00001C033B5D__INCLUDED_)
