#include <stdafx.h>
#include <ctype.h>
#include <ddxddv.h>  
#include <BasicData.h>
  
#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// DDX/DDV of CCS type 
//
// Check function. 

BOOL CheckIfDateValid(CString str)
{          
    if(str.IsEmpty()) return FALSE;
    if(str.GetLength() > 2) return FALSE;
    if(!isdigit(str.GetAt(0)) || !isdigit(str.GetAt(1))) return FALSE;
    int i = atoi(str);
    if((i < 1) || (i > 31)) return FALSE;
    return TRUE;
}

BOOL CheckIfMonthValid(CString str)
{
    if(str.IsEmpty()) return FALSE;
    if(str.GetLength() > 2) return FALSE;
    if(!isdigit(str.GetAt(0)) || !isdigit(str.GetAt(1))) return FALSE;
    int i = atoi(str);
    if((i < 1) || (i > 12)) return FALSE;
    return TRUE;
}

BOOL CheckIfYearValid(CString str)
{       
    int i;
    
    if(str.IsEmpty()) return FALSE;
    if(str.GetLength() > 4) return FALSE;
    for(i = 0; i < str.GetLength(); i++) 
        if(!isdigit(str.GetAt(i))) return FALSE;
    i = atoi(str);        
    if(str.GetLength() == 2) 
	{
        i = (i < 70) ? 2000 + i : 1900 + i;                
    }
    if((i < 1970) || (i > 2070)) return FALSE;
    return TRUE;
}
    
BOOL CheckIfDDMMYYValid(CString &str)
{      
	//--- exception handling
	CCS_TRY

	//--- check if string empty or too long
	if(str.IsEmpty() || str.GetLength() > 10) 
		return FALSE;
	 
	//--- make sure that string consists only of numbers or dots
	for(int i = 0; i < str.GetLength(); i++) 
	{
		if((!isdigit(str.GetAt(i))) && (str.GetAt(i) != '.'))
		{
			return FALSE;
		}
	}

	
	int day = 0;
	int month = 0;
	int year  = 0;

	// get the day
	int ilSeparatorPos = str.Find('.');
	if (ilSeparatorPos == -1)
	{
		// no Separator, format is DDMMYY or DDMMYYYY
		day   = atoi(str.Left(2));
		month = atoi(str.Mid(2, 2));
		if (str.GetLength() == 8)
		{
			year  = atoi(str.Right(4));
		}
		else
		{
			if (str.GetLength() > 4)
			{
				year  = atoi(str.Right(2));
			}
			else
			{
				CTime olTime = CTime::GetCurrentTime();
				LocalToUtc(olTime);
				year = atoi(olTime.Format("%Y"));
			}
		}
	}
	else
	{
		// Separator, format is DD.MM.YY or DD.MM.YYYY
		int ilSeparatorPos2 = str.ReverseFind('.');
		if (ilSeparatorPos2 < 3)
			return FALSE;
		int ilMonthLen = ilSeparatorPos2-ilSeparatorPos-1;
		if (ilMonthLen < 1)
			return FALSE;
		day = atoi(str.Left(ilSeparatorPos));
		month = atoi(str.Mid(ilSeparatorPos+1,ilMonthLen));
		year = atoi(str.Right(str.GetLength()-(ilSeparatorPos2+1)));
		if (str.Right(2) == CString("00"))
		{
			year = 2000;
		}
		else
		{
			if (year == 0)
			{
				CTime olTime = CTime::GetCurrentTime();
				LocalToUtc(olTime);
				year = atoi(olTime.Format("%Y"));
			}
		}
	}

	if (year < 100)
		year = (year < 70) ? 2000 + year : 1900 + year;
	if(year < 1970) return FALSE;
    if((year % 4) == 0) g_dayInmonth[1] = 29;
	if(month < 1 || month > 12) return FALSE;
    if((day < 1) || (day > g_dayInmonth[month - 1 ])) return FALSE;
	str.Format("%02d.%02d.%04d",day,month,year);


//--- exception handling
	CCS_CATCH_ALL

    return TRUE;
}    


BOOL CheckIfDDMMValid(CString &str)
{               
    if(str.IsEmpty()) return FALSE;
    if(str.GetLength() > 5) return FALSE;
    for(int i = 0; i < str.GetLength(); i++) 
        if((!isdigit(str.GetAt(i))) && (str.GetAt(i) != '.')) return FALSE;
                                  
  //  if(!CheckIfDateValid(str.Left(2)))    return FALSE; 
  //  if(!CheckMonthValid(str.Right(2))) return FALSE; 
    
	int ilSeparatorPos = str.Find('.');
    int day = 0;
    int month = 0;
	if (ilSeparatorPos != -1)
	{
		if (ilSeparatorPos > 1)
		{
			day = atoi(str.Left(ilSeparatorPos-1));
		}
		month = atoi(str.Mid(ilSeparatorPos+1));
	}
	else
	{
		day   = atoi(str.Left(2));
		month = atoi(str.Right(2));
	}
	CTime olTime = CTime::GetCurrentTime();
	LocalToUtc(olTime);
	int year = atoi(olTime.Format("%Y"));
    year = (year < 70) ? 2000 + year : 1900 + year;
    if((year % 4) == 0) g_dayInmonth[1] = 29;
	if(month < 1 || month > 12) return FALSE;
    if((day < 1) || (day > g_dayInmonth[month - 1 ])) return FALSE;
	str.Format("%02d.%02d",day,month);
    return TRUE;
}    

BOOL CheckIfHHMMValid(CString &str)
{

    if(str.IsEmpty() || str.GetLength() > 5) return FALSE;
    for(int i = 0; i < str.GetLength(); i++) 
        if((!isdigit(str.GetAt(i))) && (str.GetAt(i) != ':')) return FALSE;
	int ilSeparatorPos = str.Find(':');
    int hour = 0;
    int min = 0;
	if (ilSeparatorPos != -1)
	{
		if (ilSeparatorPos > 0)
		{
			hour = atoi(str.Left(ilSeparatorPos));
		}
		min = atoi(str.Mid(ilSeparatorPos+1)); 
	}
	else
	{
		hour  = atoi(str.Left(2));
		min   = atoi(str.Right(2));
	}
    if((hour < 0) || (hour > 23)) return FALSE; 
    if((min < 0) || (min > 59))   return FALSE;
	char pclBuf[82];
	sprintf(pclBuf,"%02d:%02d",hour,min);
	str = pclBuf;
    return TRUE;
}
//--------------------------------------------------------------------------------------------------------------------

BOOL DateStringToDate(CString &opString, CTime &opTime)
{

	if(opString.IsEmpty())
	{
		opTime = -1;
		return TRUE;
	}

    
	if(opString.GetLength() < 10)
	{
		return FALSE;
	}
	if(opString[2] != '.' || opString[5] != '.')
	{
		return FALSE;
	}
	
	
	if (!CheckIfDDMMYYValid(opString))
    {
		return FALSE;
	}
	
    int year = atoi(opString.Right(4));
	if(year < 1900)
	{
		return FALSE;
	}
    int month = atoi(opString.Mid(3, 2));
    int day = atoi(opString.Left(2));
	
	if (year == 1970)
		day = max(day,2);

    opTime = CTime( year, month, day, 0, 0, 0);
	return TRUE;
}
//--------------------------------------------------------------------------------------------------------------------

BOOL DateStringToDateExt(CString &opString, CTime &opTime)
{

	if(opString.IsEmpty())
	{
		opTime = -1;
		return TRUE;
	}

    
	if(opString.GetLength() < 10)
	{
		if (opString.GetLength() < 8)
		{
			return FALSE;
		}
		else
		{
			CString olTmp;
			olTmp = opString.Left(opString.GetLength()  - 2);

			if (atoi(opString.Right(2)) < 70)
			{
				olTmp += CString("20");
			}
			else
			{
				olTmp += CString("19");
			}

			olTmp += opString.Right(2);
			DateStringToDateExt(olTmp, opTime);
		}
	}
	if(opString[2] != '.' || opString[5] != '.')
	{
		return FALSE;
	}
	
	
	if (!CheckIfDDMMYYValid(opString))
    {
		return FALSE;
	}
	
    int year = atoi(opString.Right(4));
	if(year < 1900)
	{
		return FALSE;
	}
    int month = atoi(opString.Mid(3, 2));
    int day = atoi(opString.Left(2));
	
	if (year == 1970)
		day = max(day,2);

    opTime = CTime( year, month, day, 0, 0, 0);
	return TRUE;
}
//--------------------------------------------------------------------------------------------------------------------

CString DBStringToDateString(CString opStr)
{
	//------------------------------------------------------------------
	// Converts a date string from CCS date format to german date format
	// e.g. "19990812134000"  ==> "12.08.1999 - 13:40:00"
	//------------------------------------------------------------------

	CString olReturn;	
	const CString olError = "";
	
	if (opStr.GetLength() == 14)
	{
		olReturn = opStr.Mid(6, 2) + "." + opStr.Mid(4,2) + "." + opStr.Left(4) + " - " + opStr.Mid(8,2) + ":" + 
						opStr.Mid(10,2) + ":" + opStr.Right(2);
	}
	else
	{
		return olError;
	}

	return olReturn;


}
    
/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
// DDX/DDV function.
  
/*void WINAPI DDX_CCSDate(CDataExchange *pDX, int nIDC, CTime& tm)
{
    CString val;

    pDX->PrepareEditCtrl(nIDC);    
    if (pDX->m_bSaveAndValidate)
    {
        pDX->m_pDlgWnd->GetDlgItem(nIDC)->GetWindowText(val);
        if(!CheckIfDateValid(val)) {
            pDX->m_pDlgWnd->MessageBox("Please enter a valid Date.\n01...31");
            pDX->Fail();
        }
        tm = CTime(tm.GetYear(), 
                    tm.GetMonth(), 
                    atoi(val), 
                    tm.GetHour(),
                    tm.GetMinute(), 
                    tm.GetSecond());
    }
    else
    {
        val = tm.Format("%d");
        pDX->m_pDlgWnd->GetDlgItem(nIDC)->SetWindowText(val);
    }
}  

void WINAPI DDX_CCSMonth(CDataExchange *pDX, int nIDC, CTime& tm)
{
    CString val;

    pDX->PrepareEditCtrl(nIDC);    
    if (pDX->m_bSaveAndValidate)
    {
        pDX->m_pDlgWnd->GetDlgItem(nIDC)->GetWindowText(val);
        if(!CheckMonthValid(val)) {
            pDX->m_pDlgWnd->MessageBox("Please enter a valid Month.\n01...12");
            pDX->Fail();
        }
        tm = CTime(tm.GetYear(), 
                    atoi(val), 
                    tm.GetDay(), 
                    tm.GetHour(),
                    tm.GetMinute(), 
                    tm.GetSecond());

        
    }
    else
    {
        val = tm.Format("%m");
        pDX->m_pDlgWnd->GetDlgItem(nIDC)->SetWindowText(val);
    }
}

void WINAPI DDX_CCSYear(CDataExchange *pDX, int nIDC, CTime& tm)
{
    CString val;

    pDX->PrepareEditCtrl(nIDC);    
    if (pDX->m_bSaveAndValidate)
    {
        pDX->m_pDlgWnd->GetDlgItem(nIDC)->GetWindowText(val);
        if(!CheckYearValid(val)) {
            pDX->m_pDlgWnd->MessageBox("Please enter a valid Year.");
            pDX->Fail();
        }
		int ilYearOfset = atoi(val) < 1900 ? 1900 : 0;
        tm = CTime(atoi(val) + ilYearOfset, 
                    tm.GetMonth(),
                    tm.GetDay(), 
                    tm.GetHour(),
                    tm.GetMinute(), 
                    tm.GetSecond());

    }
    else
    {
        val = tm.Format("%Y");
        pDX->m_pDlgWnd->GetDlgItem(nIDC)->SetWindowText(val);
    }
}

void WINAPI DDX_CCSddmmyy(CDataExchange *pDX, int nIDC, CTime& tm)
{ 
	CString val;

	pDX->PrepareEditCtrl(nIDC);
	if (pDX->m_bSaveAndValidate)
	{
		pDX->m_pDlgWnd->GetDlgItem(nIDC)->GetWindowText(val);
		if (!CheckIfDDMMYYValid(val))
		{
			if(! val.IsEmpty())
			{
				pDX->m_pDlgWnd->MessageBox("Please enter a valid value.\nDDMMYY (ex. 01.02.96)");
				pDX->Fail();
				tm = CTime(-1);
			}
			return;
		}

		int year = atoi(val.Right(4));
		int month = atoi(val.Mid(3, 2));
		int day = atoi(val.Left(2));
		
		if (year == 1970)
			day = max(day,2);
		if (year > 2037)
			year = 2037;

		tm = CTime( year, 
                    month, 
                    day, 0, 0, 0);
                    //tm.GetHour(),
                    //tm.GetMinute(), 
                    //tm.GetSecond());      
	}
	else
	{
		val = tm.Format("%d.%m.%Y");
		pDX->m_pDlgWnd->GetDlgItem(nIDC)->SetWindowText(val);
	}
}  

void WINAPI DDX_CCSddmm(CDataExchange *pDX, int nIDC, CTime& tm)
{ 
    CString val;

    pDX->PrepareEditCtrl(nIDC);
    if (pDX->m_bSaveAndValidate)
    {
        pDX->m_pDlgWnd->GetDlgItem(nIDC)->GetWindowText(val);
        if (!CheckIfDDMMValid(val))
        {
            pDX->m_pDlgWnd->MessageBox("Please enter a valid value.\nDDMM (ex. 21.04)");
            pDX->Fail();
        }
		
		CTime olTime = CTime::GetCurrentTime();
		int year = olTime.GetYear();
        int month = atoi(val.Right(2));
        int day = atoi(val.Left(2));
        tm = CTime(year,month, day, 0, 0, 0);
    }
    else
    {
        val = tm.Format("%d.%m");
        pDX->m_pDlgWnd->GetDlgItem(nIDC)->SetWindowText(val);
    }
}  

void WINAPI DDX_CCSTime(CDataExchange *pDX, int nIDC, CTime& tm)
{
	CString val;

	pDX->PrepareEditCtrl(nIDC); 
	if (pDX->m_bSaveAndValidate)
	{
		pDX->m_pDlgWnd->GetDlgItem(nIDC)->GetWindowText(val);
		if (!CheckIfHHMMValid(val))
		{
			if(! val.IsEmpty())
			{
            pDX->m_pDlgWnd->MessageBox("Please enter a valid Time.\nHHMM (ex. 23:59)");
			}
			tm = -1;
		}
		else
		{
			CTime olTime = CTime::GetCurrentTime();
			ogBasicData.LocalToUtc(olTime);
			int hour = atoi(val.Left(2));
			int minute = atoi(val.Right(2));

			tm = CTime(olTime.GetYear(), olTime.GetMonth(), olTime.GetDay(),
				       hour, minute, 0);
		}
	}
	else
	{
		val = tm.Format("%H:%M");
		pDX->m_pDlgWnd->GetDlgItem(nIDC)->SetWindowText(val);
	}
}*/


/*void FlightD::OnKillfocusStoadate() 
{
	CString olValue;

	m_StoaDateEdit.GetWindowText(olValue);
   if (CheckIfDDMMYYValid(olValue))
	{
		m_StoaDateEdit.SetWindowText(olValue);
		CString olTisa;
		m_ETISA.GetWindowText(olTisa);
		if (olTisa == "S")
		{
			m_TifaDateEdit.SetWindowText(olValue);
		}
	}
}

void FlightD::OnKillfocusStoatime() 
{
	CString olValue;

	m_StoaTimeEdit.GetWindowText(olValue);
   if (CheckIfHHMMValid(olValue))
	{
		m_StoaTimeEdit.SetWindowText(olValue);
		CString olTisa;
		m_ETISA.GetWindowText(olTisa);
		if (olTisa == "S")
		{
			m_TifaTimeEdit.SetWindowText(olValue);
		}
	}
}*/
