#if !defined(AFX_VALUESGRID_H__40A0C755_B393_11D3_93B9_00001C033B5D__INCLUDED_)
#define AFX_VALUESGRID_H__40A0C755_B393_11D3_93B9_00001C033B5D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ValuesGrid.h : header file
//

#include <GridControl.h>
class CProfileChart;
class CChartData;

/////////////////////////////////////////////////////////////////////////////
// CValuesGrid window

class CValuesGrid : public CGridControl
{
// Construction
public:
	CValuesGrid();

// Attributes
public:
protected:
	bool bmEditMode ;

// Operations
public:
	BOOL Open ( CProfileChart *popChart, UINT idc );
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CValuesGrid)
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CValuesGrid();
	void DisplayData ( CChartData *popData, bool bpEditMode );
	bool SaveData ( CChartData *popData );
	void DoAutoGrow ( ROWCOL nRow, ROWCOL nCol);

protected:
	void SetEditControls ();
	void SetDefaultValues ( ROWCOL nRow, ROWCOL nCol );
	
	BOOL OnEndEditing( ROWCOL nRow, ROWCOL nCol);

	// Generated message map functions
protected:
	//{{AFX_MSG(CValuesGrid)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////
float MyRound ( float fpVal, UINT ipPlaces );

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_VALUESGRID_H__40A0C755_B393_11D3_93B9_00001C033B5D__INCLUDED_)
