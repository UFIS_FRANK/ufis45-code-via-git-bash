// CedaCfgData.cpp - Class for handling CFG data
//

#include <stdafx.h>
#include <ccsglobl.h>
#include <ccsptrarray.h>
#include <CCScedacom.h>
#include <CCScedadata.h>
#include <ccsddx.h>
#include <CCSbchandle.h>
#include <ccsddx.h>
#include <basicdata.h>
#include <CedaCfgData.h>
#include <cviewer.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


#ifdef _DEBUG
#define INFO ::AfxTrace("%s(%i): ",__FILE__,__LINE__); ::AfxTrace
#else
#define INFO ((void)0)
#endif


CedaCfgData::CedaCfgData()
{
	//-- exception handling
	CCS_TRY

    // Create an array of CEDARECINFO for CFGDATA
    BEGIN_CEDARECINFO(CFGDATA, CfgDataRecInfo)
        FIELD_LONG(Urno,"URNO")
        FIELD_CHAR_TRIM(Appn,"APPN")
        FIELD_CHAR_TRIM(Ctyp,"CTYP")
        FIELD_CHAR_TRIM(Ckey,"CKEY")
        FIELD_DATE(Vafr,"VAFR")
        FIELD_DATE(Vato,"VATO")
        FIELD_CHAR_TRIM(Pkno,"PKNO")
        FIELD_CHAR_TRIM(Text,"TEXT")
    END_CEDARECINFO

    // Copy the record structure
    for (int i = 0; i < sizeof(CfgDataRecInfo)/sizeof(CfgDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&CfgDataRecInfo[i],sizeof(CEDARECINFO));
        omRecInfo.Add(prpCedaRecInfo);
	}
	
	//-- exception handling
	CCS_CATCH_ALL


	// To complete initialization the function Init() has to be called
	// Do that _AFTER_ setting the global variable pcgTableExt !!!
	// Otherwise pcgTableExt is set to "XXX" (in CCSGlobl.cpp)
}

CedaCfgData::~CedaCfgData()
{
	//-- exception handling
	CCS_TRY

	ogDdx.UnRegister(this,NOTUSED);
	omData.DeleteAll();
	omUrnoMap.RemoveAll();
	omCkeyMap.RemoveAll();
	omRecInfo.DeleteAll();
	ClearAllViews();

	//-- exception handling
	CCS_CATCH_ALL
}

void CedaCfgData::Init()
{
	// exception handling
	CCS_TRY

	// Initialize table names and field names
	strcpy(pcmTableName,"VCD");
	strcat(pcmTableName, pcgTableExt);
	strcpy(pcmListOfFields, "URNO,APPN,CTYP,CKEY,VAFR,VATO,PKNO,TEXT");
	pcmFieldList = pcmListOfFields;
    

	// exception handling
	CCS_CATCH_ALL
}


bool CedaCfgData::ReadCfgData()
{
	//-- exception handling
	CCS_TRY

    char pclWhere[512];
	bool ilRc = true;

// this section is responsible for the VIEWS in charts and lists
	ilRc = true;
    omCkeyMap.RemoveAll();
	omUrnoMap.RemoveAll();
    omData.DeleteAll();

    // Select data from the database
    sprintf(pclWhere, "WHERE APPN='%s' AND CTYP = 'VIEW-DATA' AND PKNO = '%s' ORDER BY URNO", 
			ogAppName, ogBasicData.omUserID);

	
	if (CedaAction("RT", pclWhere) == false)
        return false;

	//Initialize the datastructure of for the views
	ClearAllViews();
    for (int ilLc = 0; ilRc == true; ilLc++)
    {
		CFGDATA rlCfg;
		if ((ilRc = GetBufferRecord(ilLc,&rlCfg)) == true)
		{
			PrepareViewData(&rlCfg);
		}
	}
	
	//-- exception handling
	CCS_CATCH_ALL

    return true;
}


void CedaCfgData::DeleteViewFromDiagram(CString opDiagram, CString olView)
{
	//-- exception handling
	CCS_TRY

	bool olRc = true;
	char pclSelection[1024]="";
	char pclDiagram[100]="";
	char pclViewName[100]="";

	strcpy(pclDiagram, opDiagram);
	strcpy(pclViewName, olView);
	//MWO TO DO CCS_FPMS ==> APPL
	sprintf(pclSelection,"WHERE APPN='%s' AND (PKNO = '%s' AND CKEY = '%s' AND CTYP = 'VIEW-DATA' AND TEXT LIKE '%%VIEW=%s%%')", 
			ogAppName, ogBasicData.omUserID, pclDiagram, pclViewName);
	olRc = CedaAction("DRT",pclSelection);

	//-- exception handling
	CCS_CATCH_ALL 
}

/*
void CedaCfgData::UpdateViewForDiagram(CString opDiagram, VIEWDATA &prpView,CString opViewName)
{
	//-- exception handling
	CCS_TRY

	bool olRc = true;
	CString olListOfData;
	char pclSelection[2024]="";
	char pclData[2048];
	char pclDiagram[100]="";
	char pclText[2024]="";
	char pclTmp[2024]="";
	//

	strcpy(pclDiagram, opDiagram);

	

	// to make it easier we delete all rows of the user and his configuration for the
	// specified diagram
	sprintf(pclSelection,"WHERE APPN='CCS_%s' AND (PKNO = '%s' AND "
		"CKEY = '%s' AND CTYP = 'VIEW-DATA' AND TEXT LIKE 'VIEW=%s%%')", 
		ogAppName, ogBasicData.omUserID, pclDiagram,opViewName);
	
	olRc = CedaAction("DRT",pclSelection);

	// after removing we create a completely new configuration
	int ilC1 = prpView.omNameData.GetSize();

	for(int i = 0; i < ilC1; i++)
	{
		if (opViewName == prpView.omNameData[i].ViewName) 
		{

			int ilC2 = prpView.omNameData[i].omTypeData.GetSize();
			for(int j = 0; j < ilC2; j++)
			{
				if(prpView.omNameData[i].omTypeData[j].Type == "FILTER")
				{
					if(prpView.omNameData[i].ViewName != "<Default>")
					{
						CFGDATA rlCfg;
						strcpy(rlCfg.Ckey, pclDiagram);
						strcpy(rlCfg.Pkno, ogBasicData.omUserID);
						strcpy(rlCfg.Ctyp, "VIEW-DATA");
						int ilC3 = prpView.omNameData[i].omTypeData[j].omTextData.GetSize();
						for(int k = 0; k < ilC3; k++)
						{
							sprintf(pclText, "VIEW=%s#", prpView.omNameData[i].ViewName);
							sprintf(pclTmp, "TYPE=%s#", prpView.omNameData[i].omTypeData[j].Type );
							strcat(pclText, pclTmp);
							sprintf(pclTmp, "PAGE=%s#TEXT=", prpView.omNameData[i].omTypeData[j].omTextData[k].Page);
							strcat(pclText, pclTmp);
							pclTmp[0]='\0';
							int ilC4 = prpView.omNameData[i].omTypeData[j].omTextData[k].omValues.GetSize();
							for(int l = 0; l < ilC4; l++)
							{
								char pclS[1800]="";
								sprintf(pclS, "%s@", prpView.omNameData[i].omTypeData[j].omTextData[k].omValues[l]);
								strcat(pclTmp, pclS);
								if(strlen(pclTmp)>1800)
								{
									//we have to divide the row into two parts
									CFGDATA rlCfg2;
									rlCfg2.Urno = ogBasicData.GetNextUrno();
									strcpy(rlCfg2.Ckey, rlCfg.Ckey);
									strcpy(rlCfg2.Pkno, rlCfg.Pkno);
									strcpy(rlCfg2.Ctyp, rlCfg.Ctyp);
									strcpy(rlCfg2.Text, pclText);
									strcat(pclTmp, "#");
									strcat(rlCfg2.Text, pclTmp);
									pclTmp[0]='\0';
									
									//And save the record
									MakeCedaData(olListOfData,&rlCfg2);
									strcpy(pclData,olListOfData);
									olRc = CedaAction("IRT","","",pclData);
								}
							}
							rlCfg.Urno = ogBasicData.GetNextUrno();
							strcat(pclTmp, "#");
							strcat(pclText, pclTmp);
							strcpy(rlCfg.Text, pclText);
							rlCfg.Vafr = CTime::GetCurrentTime();
							rlCfg.Vato = rlCfg.Vafr + CTimeSpan(365*10, 0, 0, 0);

							//and save to database
							MakeCedaData(olListOfData,&rlCfg);
							strcpy(pclData,olListOfData);
							olRc = CedaAction("IRT","","",pclData);
							rlCfg.IsChanged = DATA_UNCHANGED;
							pclText[0]='\0';
						}
					}
				}
				else //not filter but ==> Group, Sort etc.
				{
					if(prpView.omNameData[i].ViewName != "<Default>")
					{
						CFGDATA rlCfg;
						rlCfg.Urno = ogBasicData.GetNextUrno();
						strcpy(rlCfg.Ckey, pclDiagram);
						strcpy(rlCfg.Pkno, ogBasicData.omUserID);
						strcpy(rlCfg.Ctyp, "VIEW-DATA");
						sprintf(pclText, "VIEW=%s#", prpView.omNameData[i].ViewName);
						sprintf(pclTmp, "TYPE=%s#TEXT=", prpView.omNameData[i].omTypeData[j].Type );
						strcat(pclText, pclTmp);
						pclTmp[0]='\0';
						int ilC3 = prpView.omNameData[i].omTypeData[j].omValues.GetSize();
						for(int k = 0; k < ilC3; k++)
						{
							char pclS[100]="";
							sprintf(pclS, "%s|", prpView.omNameData[i].omTypeData[j].omValues[k]);
							strcat(pclTmp, pclS);
						}
						strcat(pclTmp, "#");
						strcat(pclText, pclTmp);
						strcpy(rlCfg.Text, pclText);
						//and save to database
						MakeCedaData(olListOfData,&rlCfg);
						strcpy(pclData,olListOfData);
						olRc = CedaAction("IRT","","",pclData);
						rlCfg.IsChanged = DATA_UNCHANGED;
						pclText[0]='\0';
					}
				}
			}
		i = ilC1;
		}
	}

	//-- exception handling
	CCS_CATCH_ALL
}
*/

////////////////////////////////////////////////////////////////
// Interprets the raw data for one propertypage
BOOL CedaCfgData::MakeRawViewData(CFGDATA *prpCfg, RAW_VIEWDATA *prpRawData)
{
	//-- exception handling
	CCS_TRY

	strcpy(prpRawData->Ckey, prpCfg->Ckey);
	char *psp = NULL;
	char pclOriginal[2048]="";
	char pclVIEW[100]="";
	char pclTYPE[100]="";
	char pclPAGE[100]="";
	char pclTEXT[2048]="";

	strcpy(pclOriginal, prpCfg->Text);
	psp = strstr(pclOriginal, "#");
	while(psp != NULL)
	{
		char *pclTmp;
		char pclPart[2048];
		char pclRest[2048];
		strcpy(pclRest, psp + 1);
		*psp = '\0';
		strcpy(pclPart, pclOriginal);
		strcpy(pclOriginal, pclRest);
		if((pclTmp = strstr(pclPart, "VIEW=")) != NULL)
			strcpy(pclVIEW, pclPart);
		else if((pclTmp = strstr(pclPart, "TYPE=")) != NULL)
			strcpy(pclTYPE, pclPart);
		else if((pclTmp = strstr(pclPart, "PAGE=")) != NULL)
			strcpy(pclPAGE, pclPart);
		else if((pclTmp = strstr(pclPart, "TEXT=")) != NULL)
			strcpy(pclTEXT, pclPart);

		psp = strstr(pclOriginal, "#");
	}

	psp = strstr(pclTYPE, "=");
	if(psp == NULL)
	{
		return FALSE;
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal, psp);
		if(strlen(pclOriginal) == 0)
		{
			return FALSE;
		}
		else
		{
			strcpy(prpRawData->Type, pclOriginal);
		}
	}
	psp = NULL;
	psp = strstr(pclVIEW, "=");
	if(psp == NULL)
	{
		return FALSE;
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal, psp);
		if(strlen(pclOriginal) == 0)
		{
			return FALSE;
		}
		else
		{
			strcpy(prpRawData->Name, pclOriginal);
		}
	}
	psp = NULL;
	psp = strstr(pclPAGE, "=");
	if(psp == NULL)
	{
		if(!strcmp(prpRawData->Type, "FILTER"))
		{
			return FALSE;
		}
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal, psp);
		if(strlen(pclOriginal) == 0)
		{
			return FALSE;
		}
		else
		{
			strcpy(prpRawData->Page, pclOriginal);
		}
	}
	psp = NULL;
	psp = strstr(pclTEXT, "=");
	if(psp == NULL)
	{
		return FALSE;
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal, psp);
		if(strlen(pclOriginal) == 0)
		{
			return FALSE;
		}
		else
		{
			strcpy(prpRawData->Values, pclOriginal);
		}
	}
	psp = NULL;

	return TRUE;

	//-- exception handling
	CCS_CATCH_ALL
}

////////////////////////////////////////////////////////////////
//MWO: extracts the values which are comma-separated and copies them
//     into CStringArray of VIEW_TEXTDATA

// FILTER: pcpSepa = "@"
// SONST   pcpSepa = "|"
/*
void CedaCfgData::MakeViewValues(char *pspValues, CCSPtrArray<CString> *popValues, char *pcpSepa )
{
	//-- exception handling
	CCS_TRY

	char *psp = NULL;
	char pclOriginal[1024]="";
	char pclValue[100]="";

	strcpy(pclOriginal, pspValues);
	psp = strstr(pclOriginal, pcpSepa ); //"@ oder |"
	while(psp != NULL)
	{
		char pclPart[2048];
		char pclRest[2048];
		strcpy(pclRest, psp+1);
		*psp = '\0';
		strcpy(pclPart, pclOriginal);
		strcpy(pclOriginal, pclRest);
		if(strcmp(pclPart, "") != 0)
			 popValues->NewAt(popValues->GetSize(), pclPart);

		psp = strstr(pclOriginal, pcpSepa);
	}

	//-- exception handling
	CCS_CATCH_ALL
}
*/

////////////////////////////////////////////////////////////////
// MWO: prepares the nested arrays for the CFGDATA
void CedaCfgData::PrepareViewData(CFGDATA *prpCfg)
{
	//-- exception handling
	CCS_TRY

	VIEWDATA *prlFoundViewData;
	VIEW_VIEWNAMES *prlFoundViewName;
	VIEW_TYPEDATA *prlFoundTypeData;
	VIEW_TEXTDATA *prlFoundTextData;
	RAW_VIEWDATA rlRawData;

	if(MakeRawViewData(prpCfg, &rlRawData) != TRUE)
	{
		return; //then the row is corrupt
	}
	prlFoundViewData = FindViewData(prpCfg);
	if(prlFoundViewData == NULL) 
	{
		//if the key does not exist we have generate an new on one from the root
		VIEW_TEXTDATA rlTextData;
		VIEW_TYPEDATA rlTypeData;
		VIEW_VIEWNAMES rlNameData;
		VIEWDATA rlViewData;
		rlViewData.Ckey = CString(rlRawData.Ckey);
		rlNameData.ViewName = CString(rlRawData.Name);
		rlTypeData.Type = CString(rlRawData.Type);
		if(strcmp(rlRawData.Type, "FILTER") == 0)
		{
			rlTextData.Page = CString(rlRawData.Page);
			MakeViewValues(rlRawData.Values, &rlTextData.omValues, "@");
			rlTypeData.omTextData.NewAt(rlTypeData.omTextData.GetSize(), rlTextData);
		}
		else
		{
			MakeViewValues(rlRawData.Values, &rlTypeData.omValues, "|");
		}
		rlNameData.omTypeData.NewAt(rlNameData.omTypeData.GetSize(), rlTypeData);
		rlViewData.omNameData.NewAt(rlViewData.omNameData.GetSize(), rlNameData);
		omViews.NewAt(omViews.GetSize(), rlViewData);
	}
	else
	{

		//the key exists, we have to look for the rest of the tree and
		//evaluate, whether parts of it do exist and merge them together
		//Now first let's look for existing of the view-name
		prlFoundViewName = FindViewNameData(&rlRawData);
		if(prlFoundViewName == NULL)
		{
			//the view itself exists but there are no entries

			VIEW_TEXTDATA rlTextData;
			VIEW_TYPEDATA rlTypeData;
			VIEW_VIEWNAMES rlNameData;
			rlNameData.ViewName = CString(rlRawData.Name);
			rlTypeData.Type = CString(rlRawData.Type);
			if(strcmp(rlRawData.Type, "FILTER") == 0)
			{
				rlTextData.Page = CString(rlRawData.Page);
				MakeViewValues(rlRawData.Values, &rlTextData.omValues, "@");
				rlTypeData.omTextData.NewAt(rlTypeData.omTextData.GetSize(), rlTextData);
			}
			else
			{
				MakeViewValues(rlRawData.Values, &rlTypeData.omValues, "|");
			}
			rlNameData.omTypeData.NewAt(rlNameData.omTypeData.GetSize(), rlTypeData);
			prlFoundViewData->omNameData.NewAt(prlFoundViewData->omNameData.GetSize(), rlNameData);
		}
		else
		{
			//there are name entries => let's have a look at typs and their data
			prlFoundTypeData = FindViewTypeData(&rlRawData);
			if(prlFoundTypeData == NULL) //***** OK
			{
				//View + name exist but no Type entries
				VIEW_TYPEDATA rlTypeData;
				VIEW_TEXTDATA rlTextData;
				rlTypeData.Type = CString(rlRawData.Type);
				if(strcmp(rlRawData.Type, "FILTER") == 0)
				{
					rlTextData.Page = CString(rlRawData.Page);
					MakeViewValues(rlRawData.Values, &rlTextData.omValues, "@");
					rlTypeData.omTextData.NewAt(rlTypeData.omTextData.GetSize(), rlTextData);
				}
				else
				{
					MakeViewValues(rlRawData.Values, &rlTypeData.omValues, "|");
				}
				prlFoundViewName->omTypeData.NewAt(prlFoundViewName->omTypeData.GetSize(), rlTypeData);
			}
			else
			{
				if(rlRawData.Type == CString("FILTER"))//***
				{   
					prlFoundTextData = FindViewTextData(&rlRawData);
					if(prlFoundTextData == NULL) //*** OK
					{
						//View + Name + Type exist but no values for Filter
						VIEW_TEXTDATA rlTextData;
						rlTextData.Page = CString(rlRawData.Page);
						MakeViewValues(rlRawData.Values, &rlTextData.omValues, "@");
						prlFoundTypeData->omTextData.NewAt(prlFoundTypeData->omTextData.GetSize(), rlTextData);
					}
					else
					{ 
						MakeViewValues(rlRawData.Values, &prlFoundTextData->omValues, "@");
					}
				}
				else
				{

				}
			}
		}
	}

	//-- exception handling
	CCS_CATCH_ALL
}

/////////////////////////////////////////////////////////////////////
// MWO: Evaluates the existing of a view, e.g. Staffdia
VIEWDATA * CedaCfgData::FindViewData(CFGDATA *prlCfg)
{
	//-- exception handling
	CCS_TRY

	int ilCount = omViews.GetSize();

	for(int i = 0; i < ilCount; i++)
	{
		if(omViews[i].Ckey == prlCfg->Ckey)
		{
			return &omViews[i];
		}
	}

	//-- exception handling
	CCS_CATCH_ALL

	return NULL;
}

//////////////////////////////////////////////////////////////////////////////
//MWO: Evaluates the existing of a viewname, e.g. Heute, Morgen
VIEW_VIEWNAMES * CedaCfgData::FindViewNameData(RAW_VIEWDATA *prpRawData)
{
	//-- exception handling
	CCS_TRY

	int ilCount = omViews.GetSize();
	for(int i = 0; i < ilCount; i++)
	{
		if(omViews[i].Ckey == prpRawData->Ckey)
		{
			int ilC2 = omViews[i].omNameData.GetSize();
			for(int j = 0; j < ilC2; j++)
			{
				if(omViews[i].omNameData[j].ViewName == prpRawData->Name)
				{	
					return &omViews[i].omNameData[j];
				}
			}
		}
	}

	//-- exception handling
	CCS_CATCH_ALL

	return NULL;
}

/////////////////////////////////////////////////////////////////////////////////
// MWO: Evaluates the existing of a viewtyp, e.g. FILTER, GROUP
VIEW_TYPEDATA * CedaCfgData::FindViewTypeData(RAW_VIEWDATA *prpRawData)
{
	//-- exception handling
	CCS_TRY

	int ilCount = omViews.GetSize();
	for(int i = 0; i < ilCount; i++)
	{
		if(omViews[i].Ckey == prpRawData->Ckey)
		{
			int ilC2 = omViews[i].omNameData.GetSize();
			for(int j = 0; j < ilC2; j++)
			{
				if(omViews[i].omNameData[j].ViewName == CString(prpRawData->Name))
				{	
					int ilC3 = omViews[i].omNameData[j].omTypeData.GetSize();
					for(int k = 0; k < ilC3; k++)
					{
						if(omViews[i].omNameData[j].omTypeData[k].Type == CString(prpRawData->Type))
						{
							return  &omViews[i].omNameData[j].omTypeData[k];
						}
					}
				}
			}
		}
	}

	//-- exception handling
	CCS_CATCH_ALL

	return NULL;
}
////////////////////////////////////////////////////////////////////////////////////
// MWO: Evaluates the existing of values for FILTER
VIEW_TEXTDATA * CedaCfgData::FindViewTextData(RAW_VIEWDATA *prpRawData)
{
	//-- exception handling
	CCS_TRY

	int ilCount = omViews.GetSize();
	for(int i = 0; i < ilCount; i++)
	{
		if(omViews[i].Ckey == prpRawData->Ckey)
		{
			int ilC2 = omViews[i].omNameData.GetSize();
			for(int j = 0; j < ilC2; j++)
			{
				if(omViews[i].omNameData[j].ViewName == CString(prpRawData->Name))
				{	
					int ilC3 = omViews[i].omNameData[j].omTypeData.GetSize();
					for(int k = 0; k < ilC3; k++)
					{
						if(omViews[i].omNameData[j].omTypeData[k].Type == CString(prpRawData->Type))
						{
							int ilC4 = omViews[i].omNameData[j].omTypeData[k].omTextData.GetSize();
							for(int l = 0; l < ilC4; l++)
							{
								if(omViews[i].omNameData[j].omTypeData[k].omTextData[l].Page == CString(prpRawData->Page))
								{
									return &omViews[i].omNameData[j].omTypeData[k].omTextData[l];
								}
							}
						}
					}
				}
			}
		}
	}

	//-- exception handling
	CCS_CATCH_ALL

	return NULL;
}

/////////////////////////////////////////////////////////////////////////
//MWO: Clear all buffers allocated in omViews
void CedaCfgData::ClearAllViews()
{
	//-- exception handling
	CCS_TRY

	while(omViews.GetSize() > 0)
	{
		while(omViews[0].omNameData.GetSize() > 0)
		{
			while(omViews[0].omNameData[0].omTypeData.GetSize() > 0)
			{
				while(omViews[0].omNameData[0].omTypeData[0].omTextData.GetSize() > 0)
				{
					while(omViews[0].omNameData[0].omTypeData[0].omTextData[0].omValues.GetSize() > 0)
					{
						CString *prlText = &omViews[0].omNameData[0].omTypeData[0].omTextData[0].omValues[0];
						omViews[0].omNameData[0].omTypeData[0].omTextData[0].omValues.DeleteAt(0);
					}
					omViews[0].omNameData[0].omTypeData[0].omTextData.DeleteAt(0);
				}
			
				while(omViews[0].omNameData[0].omTypeData[0].omValues.GetSize() > 0)
				{
					omViews[0].omNameData[0].omTypeData[0].omValues.DeleteAt(0);
				}
			
				omViews[0].omNameData[0].omTypeData.DeleteAt(0);
			}
			omViews[0].omNameData.DeleteAt(0);
		}
		omViews.DeleteAt(0);
	}

	//-- exception handling
	CCS_CATCH_ALL
}



// Prepare some whatif data, not read from database
void CedaCfgData::PrepareCfgData(CFGDATA *prpCfg)
{
}


/////////////////////////////////////////////////////////////////////////////
// Update data methods (called from PrePlanTable class and StaffTable class)

bool CedaCfgData::InsertCfg(const CFGDATA *prpCfgData)
{
	bool blRet = false;

	//-- exception handling
	CCS_TRY

    if (CfgExist(prpCfgData->Urno))
	{
        blRet = UpdateCfgRecord(prpCfgData);
	}
    else
	{
        blRet = InsertCfgRecord(prpCfgData);
	}

	//-- exception handling
	CCS_CATCH_ALL

	return blRet;
}

bool CedaCfgData::UpdateCfg(const CFGDATA *prpCfgData)
{
    return(UpdateCfgRecord(prpCfgData));
}


BOOL CedaCfgData::CfgExist(long lpUrno)
{
	BOOL ilRet = FALSE;  

	//-- exception handling
	CCS_TRY

	// don't read from database anymore, just check internal array
	CFGDATA *prpData;

	ilRet = omUrnoMap.Lookup((void *) &lpUrno,(void *&)prpData); 

	//-- exception handling
	CCS_CATCH_ALL

	return ilRet;
}

bool CedaCfgData::InsertCfgRecord(const CFGDATA *prpCfgData)
{
	bool blRet = false;

	//-- exception handling
	CCS_TRY

		// insert new Cfg record into table "SHFCKI"
    char *CfgFields = "URNO,CTYP,CKEY,VAFR,VATO,TEXT";
    char CfgData[512]; // CFGCKI ~ 430 Byte
    sprintf(CfgData, "%ld,%s,%s,%s,%s,%s,%s,%s",
        prpCfgData->Urno, 
		prpCfgData->Appn,
		prpCfgData->Ctyp,
		prpCfgData->Ckey,
		prpCfgData->Vafr,
		prpCfgData->Vato,
		prpCfgData->Pkno,
		prpCfgData->Text);

   blRet = CedaAction("IRT", "CFGCKI", CfgFields, "", "", CfgData);

   //-- exception handling
	CCS_CATCH_ALL

	return blRet;
}


bool CedaCfgData::UpdateCfgRecord(const CFGDATA *prpCfgData)
{
	bool blRc = false;

	//-- exception handling
	CCS_TRY

    // update Cfgl data in table "SHFCKI"
    // insert new Cfg record into table "SHFCKI"
    char *CfgFields = "CTYP,CKEY,VAFR,VATO,TEXT";
    char CfgData[512]; // SHFCKI ~ 140 byte
	char pclSelection[64];
    sprintf(CfgData, "%s,%s,%s,%s,%s,%s,%s",
		prpCfgData->Appn,
		prpCfgData->Ctyp,
		prpCfgData->Ckey,
		prpCfgData->Vafr,
		prpCfgData->Vato,
		prpCfgData->Pkno,
		prpCfgData->Text);
	sprintf(pclSelection," where URNO = '%ld%'",prpCfgData->Urno);
	blRc = CedaAction("URT", "CFGCKI", CfgFields, pclSelection, "", CfgData);
	//ogDdx.DataChanged((void *)this,CFG_CHANGE,(void *)prpCfgData);

	//-- exception handling
	CCS_CATCH_ALL

	return blRc;
}



// the callback function for storing broadcasted fligthdata in FLIGHTDATA array

void  ProcessCfgCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	 ogCfgData.ProcessCfgBc(ipDDXType,vpDataPointer,ropInstanceName);
}


void  CedaCfgData::ProcessCfgBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	//-- exception handling
	CCS_TRY

	if ((ipDDXType == BC_CFG_CHANGE) || (ipDDXType == BC_CFG_INSERT))
	{
		CFGDATA *prpCfg;
		struct BcStruct *prlCfgData;

		prlCfgData = (struct BcStruct *) vpDataPointer;
		long llUrno = GetUrnoFromSelection(prlCfgData->Selection);

		if (omUrnoMap.Lookup((void *)llUrno,(void *& )prpCfg) == TRUE)
		{
			GetRecordFromItemList(prpCfg,
				prlCfgData->Fields,prlCfgData->Data);
			ogDdx.DataChanged((void *)this,CFG_CHANGE,(void *)prpCfg);
		}
		else
		{
			prpCfg = new CFGDATA;
			GetRecordFromItemList(prpCfg,
				prlCfgData->Fields,prlCfgData->Data);
			PrepareCfgData(prpCfg);
			omData.Add(prpCfg);
			omCkeyMap.SetAt(prpCfg->Ckey,prpCfg);
			omUrnoMap.SetAt((void *)prpCfg->Urno,prpCfg);
			ogDdx.DataChanged((void *)this,CFG_INSERT,(void *)prpCfg);
		}
	}

	//-- exception handling
	CCS_CATCH_ALL
}



CFGDATA  *CedaCfgData::GetCfgByUrno(long lpUrno)
{
	//-- exception handling
	CCS_TRY

	CFGDATA  *prlCfg;

	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlCfg) == TRUE)
	{
		return prlCfg;
	}

	//-- exception handling
	CCS_CATCH_ALL

	return NULL;
}

bool CedaCfgData::AddCfg(CFGDATA *prpCfg)
{
	//-- exception handling
	CCS_TRY

	CFGDATA *prlCfg = new CFGDATA;
	memcpy(prlCfg,prpCfg,sizeof(CFGDATA));
	prlCfg->IsChanged = DATA_NEW;

	omData.Add(prlCfg);
	omUrnoMap.SetAt((void *)prlCfg->Urno, prlCfg);
	omCkeyMap.SetAt(prlCfg->Ckey, prlCfg); 

	ogDdx.DataChanged((void *)this,CFG_INSERT,(void *)prlCfg);
	SaveCfg(prlCfg);

	//-- exception handling
	CCS_CATCH_ALL

	return true;
}

bool CedaCfgData::ChangeCfgData(CFGDATA *prpCfg)
{
	//-- exception handling
	CCS_TRY

	if (prpCfg->IsChanged == DATA_UNCHANGED)
	{
		prpCfg->IsChanged = DATA_CHANGED;
	}
	SaveCfg(prpCfg);

	//-- exception handling
	CCS_CATCH_ALL

	return true;
}

bool CedaCfgData::DeleteCfg(long lpUrno)
{
	//-- exception handling
	CCS_TRY

	CFGDATA *prpCfg = GetCfgByUrno(lpUrno);
	if (prpCfg != NULL)
	{
		prpCfg->IsChanged = DATA_DELETED;

		omUrnoMap.RemoveKey((void *)lpUrno);
		omCkeyMap.RemoveKey(prpCfg->Ckey);

		SaveCfg(prpCfg);
	}

	//-- exception handling
	CCS_CATCH_ALL

	return true;
}

bool CedaCfgData::SaveCfg(CFGDATA *prpCfg)
{
	//-- exception handling
	CCS_TRY

	bool blRc = true;
	CString olListOfData;
	char pclSelection[124];
	char pclData[824];

	if ((prpCfg->IsChanged == DATA_UNCHANGED) || (!bgOnline))
	{
		return true; // no change, nothing to do
	}
	switch(prpCfg->IsChanged)   // New job, insert into database
	{
	case DATA_NEW:
		MakeCedaData(olListOfData,prpCfg);
		strcpy(pclData,olListOfData);
		blRc = CedaAction("IRT","","",pclData);
		prpCfg->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_CHANGED:
		sprintf(pclSelection,"WHERE URNO = '%ld'",prpCfg->Urno);
		MakeCedaData(olListOfData,prpCfg);
		strcpy(pclData,olListOfData);
		blRc = CedaAction("URT",pclSelection,"",pclData);
		if (blRc != true)
		{
			prpCfg->IsChanged = DATA_NEW;
			SaveCfg(prpCfg);
		}
		else
		{
			prpCfg->IsChanged = DATA_UNCHANGED;
		}
		break;
	case DATA_DELETED:
		sprintf(pclSelection,"WHERE URNO = '%ld'",prpCfg->Urno);
		blRc = CedaAction("DRT",pclSelection);
		break;
	}
	
	//-- exception handling
	CCS_CATCH_ALL

	return true;
}


void CedaCfgData::SetCfgData(void)
{

}


//
// RRO 27.03.03
// correcting crash in FILTER-section
// now it is possible to save any view size, e.g. 1000 function-URNOs
//
void CedaCfgData::UpdateViewForDiagram(CString opDiagram, VIEWDATA &prpView,CString opViewName)
{
	bool olRc = true;
	CString olListOfData;
	char pclSelection[2024]="";
	char pclData[4096];
	char pclDiagram[100]="";
	char pclText[4096]="";
	char pclTmp[4096]="";

	strcpy(pclDiagram, opDiagram);

	// to make it easier we delete all rows of the user and his configuration for the specified diagram
	sprintf(pclSelection,"WHERE APPN='%s' AND (PKNO = '%s' AND "
		"CKEY = '%s' AND CTYP = 'VIEW-DATA' AND TEXT LIKE 'VIEW=%s%%')", 
		ogAppName, ogBasicData.omUserID, pclDiagram,opViewName);
	
	olRc = CedaAction("DRT",pclSelection);

	// after removing we create a completely new configuration
	int ilC1 = prpView.omNameData.GetSize();

	for (int i = 0; i < ilC1; i++)
	{
		if (opViewName == prpView.omNameData[i].ViewName) 
		{
			int ilC2 = prpView.omNameData[i].omTypeData.GetSize();
			for (int j = 0; j < ilC2; j++)
			{
				if (prpView.omNameData[i].omTypeData[j].Type == "FILTER")
				{
					if (prpView.omNameData[i].ViewName != "<Default>")
					{
						CFGDATA rlCfg;
						strcpy(rlCfg.Ckey, pclDiagram);
						strcpy(rlCfg.Pkno, ogBasicData.omUserID);
						strcpy(rlCfg.Ctyp, "VIEW-DATA");
						rlCfg.Vafr = CTime::GetCurrentTime();
						rlCfg.Vato = rlCfg.Vafr + CTimeSpan(365*10, 0, 0, 0);

						int ilC3 = prpView.omNameData[i].omTypeData[j].omTextData.GetSize();
						for(int k = 0; k < ilC3; k++)
						{
							// general view information
							sprintf(pclText, "VIEW=%s#", prpView.omNameData[i].ViewName);
							sprintf(pclTmp, "TYPE=%s#", prpView.omNameData[i].omTypeData[j].Type );
							strcat(pclText, pclTmp);
							sprintf(pclTmp, "PAGE=%s#TEXT=", prpView.omNameData[i].omTypeData[j].omTextData[k].Page);
							strcat(pclText, pclTmp);
							pclTmp[0]='\0';
							int ilC4 = prpView.omNameData[i].omTypeData[j].omTextData[k].omValues.GetSize();

							// getting values of the array and putting them together
							CString olTEXT = "";
							CString olTmp;
							for (int l = 0; l < ilC4; l++)
							{
								olTmp.Format("%s@", prpView.omNameData[i].omTypeData[j].omTextData[k].omValues[l]);
								olTEXT += olTmp;
							}

							// check how many packets we need (VCDTAB.TEXT is only VARCHAR(2000)!)
							int ilStringLength = olTEXT.GetLength();
							int ilTmp = 0;
							while (ilTmp < ilStringLength)
							{
								ilTmp += 1800;
								if (ilTmp < ilStringLength)
								{
									//int ilSep = olTEXT.Find("|",ilTmp);
									/*if (ilSep > ilTmp && ilSep < (ilTmp+50))
									{
										olTEXT.Delete(ilSep);
										olTEXT.Insert(ilSep, "\n");
									}
									else
									{*/
										olTEXT.Insert(ilTmp, "@\n@");
									//}
									ilStringLength++;
								}
							}

							// extract the packets into an array
							CStringArray olArr;
							//  int ilPackets = ExtractTextLineFast(olArr, olTEXT.GetBuffer(0), "\n");
							int ilPackets = ExtractItemList(olTEXT, &olArr, '\n' );

							// save every packet in the DB
							for (l = 0; l < ilPackets; l++)
							{
								rlCfg.Urno = ogBasicData.GetNextUrno();
								olTEXT = olArr[l] + "#";
								strcpy(rlCfg.Text, pclText);
								strcat(rlCfg.Text,olTEXT.GetBuffer(0));
								MakeCedaData(olListOfData,&rlCfg);
								strcpy(pclData,olListOfData);
								olRc = CedaAction("IRT","","",pclData);
							}
						}
					}
				}
				else //not filter but ==> Group, Sort etc.
				{
					if(prpView.omNameData[i].ViewName != "<Default>")
					{
						CFGDATA rlCfg;
						rlCfg.Urno = ogBasicData.GetNextUrno();
						strcpy(rlCfg.Ckey, pclDiagram);
						strcpy(rlCfg.Pkno, ogBasicData.omUserID);
						strcpy(rlCfg.Ctyp, "VIEW-DATA");
						sprintf(pclText, "VIEW=%s#", prpView.omNameData[i].ViewName);
						sprintf(pclTmp, "TYPE=%s#TEXT=", prpView.omNameData[i].omTypeData[j].Type );
						strcat(pclText, pclTmp);
						pclTmp[0]='\0';
						int ilC3 = prpView.omNameData[i].omTypeData[j].omValues.GetSize();
						for(int k = 0; k < ilC3; k++)
						{
							char pclS[100]="";
							sprintf(pclS, "%s|", prpView.omNameData[i].omTypeData[j].omValues[k]);
							strcat(pclTmp, pclS);
						}
						strcat(pclTmp, "#");
						strcat(pclText, pclTmp);
						strcpy(rlCfg.Text, pclText);
						//and save to database
						MakeCedaData(olListOfData,&rlCfg);
						strcpy(pclData,olListOfData);
						olRc = CedaAction("IRT","","",pclData);
						rlCfg.IsChanged = DATA_UNCHANGED;
						pclText[0]='\0';
					}
				}
			}
		i = ilC1;
		}
	}
}


// FILTER: pcpSepa = "@"
// SONST   pcpSepa = "|"
void CedaCfgData::MakeViewValues(char *pspValues, CCSPtrArray<CString> *popValues, char *pcpSepa )
{
	char *psp = NULL;
	char pclOriginal[2048]="";

	strcpy(pclOriginal, pspValues);
	psp = strstr(pclOriginal, pcpSepa/*"@ oder |"*/);
	if (psp == NULL)
	{
		if (popValues->GetSize())
		{
			(*popValues)[popValues->GetSize()-1] += pclOriginal;
		}
		else
		{
			popValues->New(pclOriginal);
		}
	}
	else
	{
		bool blFirst = true;
		while(psp != NULL)
		{
			char pclPart[2048];
			char pclRest[2048];
			strcpy(pclRest, psp+1);
			*psp = '\0';
			strcpy(pclPart, pclOriginal);
			strcpy(pclOriginal, pclRest);
			if(strcmp(pclPart, "") != 0)
			{
				if (popValues->GetSize() && blFirst == true)
				{
					(*popValues)[popValues->GetSize()-1] += pclPart;
					blFirst = false;
				}
				else
				{
					popValues->New(pclPart);
					blFirst = false;
				}
			}

			psp = strstr(pclOriginal, pcpSepa/*"@ oder |"*/);
		}
	}
}

