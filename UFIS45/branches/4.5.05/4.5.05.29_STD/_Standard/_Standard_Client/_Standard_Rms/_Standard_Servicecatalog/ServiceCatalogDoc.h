// ServiceCatalogDoc.h : interface of the CServiceCatalogDoc class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_SERVICECATALOGDOC_H__E523203D_3849_11D3_933A_00001C033B5D__INCLUDED_)
#define AFX_SERVICECATALOGDOC_H__E523203D_3849_11D3_933A_00001C033B5D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define RESORUCETABLEANZ  9
#define STARTINDLOCATIONS  3	//  0-based !!!
extern char* pgResourceTables[RESORUCETABLEANZ];
extern char* pgResourceCode[RESORUCETABLEANZ];
extern char* pgResourceDisplay[RESORUCETABLEANZ];
extern UINT pgResourceIDS[RESORUCETABLEANZ];


struct RESOURCETABLEINFO
{
	CString table;
	CString codefield;	
	CString dspfield;	
	CString resourcetyp;
	bool initialized;
	CString choicelist;		//  Auswahlliste f�r Combo-Boxen
	UINT	singlevalues;	//  Anzahl von einzelnen Werten
	UINT	all;			//  Anzahl von einzelnen Werten + Gruppen
};


class CServiceCatalogDoc : public CDocument
{
protected: // create from serialization only
	CServiceCatalogDoc();
	DECLARE_DYNCREATE(CServiceCatalogDoc)

// Attributes
public:
	RESOURCETABLEINFO *psmUsedResourceTables;
	UINT			  imResTableAnz;
	CMapStringToOb omResourceTableList;
	CMapStringToString	omEquChoiceLists;	
	//  needed for equipment's CVarServiceGrid::SetItemDataPtr
	CMapStringToString	omEquTableTypeStrings;
	CStringArray		omBCTables;
// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CServiceCatalogDoc)
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
	virtual void SetTitle(LPCTSTR lpszTitle);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CServiceCatalogDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
	bool LoadCedaTable ();
public:
	RESOURCETABLEINFO* GetResourceTableInfo ( char *tablename );
	void SetEquChoiceList ( CString &ropType );
	bool GetEquChoiceList ( CString &ropType, CString &ropChoiceList );
	bool UpdateChoiceLists ( CString opTable, int ipBCType, RecordSet* popData, CString &ropResTable );
	
protected:
	int FillResourceTableInfos ();
	void FillChoiceLists ();
	void SetResourceChoiceList ( int ipIdx );

// Generated message map functions
protected:
	//{{AFX_MSG(CServiceCatalogDoc)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SERVICECATALOGDOC_H__E523203D_3849_11D3_933A_00001C033B5D__INCLUDED_)
