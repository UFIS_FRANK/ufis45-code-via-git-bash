// LoginDlg.cpp : implementation file
//

#include <stdafx.h>
#include <CCSCedadata.h>
#include <CCSCedacom.h>
#include <LoginDlg.h>
#include <PrivList.h>
#include <BasicData.h>

#ifdef _DEBUG 
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif
  

//----------------------------------------------------------------------------------------------------------------------
//					construction
//----------------------------------------------------------------------------------------------------------------------
CLoginDialog::CLoginDialog(const char *pcpHomeAirport, const char *pcpAppl, CWnd* pParent /*=NULL*/)
    : CDialog(CLoginDialog::IDD, pParent)
{
    //{{AFX_DATA_INIT(CLoginDialog)
	//}}AFX_DATA_INIT

	strcpy(pcmHomeAirport,pcpHomeAirport);
	strcpy(pcmAppl,pcpAppl);
}



//----------------------------------------------------------------------------------------------------------------------
//					data exchange, message handling
//----------------------------------------------------------------------------------------------------------------------
void CLoginDialog::DoDataExchange(CDataExchange* pDX)
{
    CDialog::DoDataExchange(pDX);
    //{{AFX_DATA_MAP(CLoginDialog)
	DDX_Control(pDX, IDC_USER, m_UsernameCtrl);
	DDX_Control(pDX, IDC_PASSWORD, m_PasswordCtrl);
	DDX_Control(pDX, IDC_USER_TXT, m_UsidCaption);
	DDX_Control(pDX, IDC_PASSWORD_TXT, m_PassCaption);
	DDX_Control(pDX, IDOK, m_OkCaption);
	DDX_Control(pDX, IDCANCEL, m_CancelCaption);
	//}}AFX_DATA_MAP
}
//---------------------------------------------------------------------------------------------------------------------- 

BEGIN_MESSAGE_MAP(CLoginDialog, CDialog)
    //{{AFX_MSG_MAP(CLoginDialog)
//    ON_WM_PAINT()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()



//----------------------------------------------------------------------------------------------------------------------
//					message handling
//----------------------------------------------------------------------------------------------------------------------
/*
void CLoginDialog::OnPaint() 
{
	//-- exception handling
	CCS_TRY

    CPaintDC dc(this); // device context for painting
    
    // TODO: Add your message handler code here
    CBrush olBrush( RGB( 192, 192, 192 ) );
    CBrush *polOldBrush = dc.SelectObject( &olBrush );
    CRect olRect;
    GetClientRect( &olRect );
    dc.FillRect( &olRect, &olBrush );
    dc.SelectObject( polOldBrush );
    
    CDC olDC;
    olDC.CreateCompatibleDC( &dc );
    CBitmap olBitmap;
    olBitmap.LoadBitmap( IDB_LOGIN );
    olDC.SelectObject( &olBitmap );
    dc.BitBlt( 0, 140, 409, 52, &olDC, 0, 0, SRCCOPY );
    olDC.DeleteDC( );
    
    // Do not call CDialog::OnPaint() for painting messages

	//-- exception handling
	CCS_CATCH_ALL
}
*/

//----------------------------------------------------------------------------------------------------------------------

void CLoginDialog::OnOK() 
{
	//-- exception handling
	//CCS_TRY

	bool blRc = true;
	CString	olUsername, olPassword;

	CWnd *polFocusCtrl = &m_UsernameCtrl;

	// get the username
	m_UsernameCtrl.GetWindowText(olUsername);
	m_PasswordCtrl.GetWindowText(olPassword);


	// check the username + password
	AfxGetApp()->DoWaitCursor(1);

	strcpy(pcgUserName,olUsername.GetBuffer(0));
	strcpy(pcgPassWord,olPassword);
	ogBasicData.omUserID = olUsername.GetBuffer(0);

	ogCommHandler.SetUser(olUsername.GetBuffer(0));
	strcpy(CCSCedaData::pcmUser, olUsername.GetBuffer(0));
	strcpy(CCSCedaData::pcmReqId, ogCommHandler.pcmReqId);


	blRc = ogPrivList.Login(pcgTableExt,pCHAR(olUsername),pCHAR(olPassword), pcmAppl);

	AfxGetApp()->DoWaitCursor(-1);
	
	if( blRc )
	{
		CDialog::OnOK();
	}
	else
	{
		ErrorMessageBox ();	
		imLoginCount++;

		if( imLoginCount >= MAX_LOGIN )
			OnCancel();
		else
		{
			if( polFocusCtrl != NULL )
				polFocusCtrl->SetFocus(); // set the focus to the offending control
		}
	}

	//-- exception handling
	//CCS_CATCH_ALL
}
//----------------------------------------------------------------------------------------------------------------------

void CLoginDialog::OnCancel() 
{
	// TODO: Add extra cleanup here
	
	CDialog::OnCancel();
}
//----------------------------------------------------------------------------------------------------------------------

BOOL CLoginDialog::OnInitDialog() 
{
	//-- exception handling
	//CCS_TRY

	CDialog::OnInitDialog();
	
	imLoginCount = 0;
	m_UsidCaption.SetWindowText( GetString(IDS_USER_TXT) );
	m_PassCaption.SetWindowText( GetString(IDS_PASSWORT_TXT) );
	m_OkCaption.SetWindowText ( GetString(IDS_OK) );
	m_CancelCaption.SetWindowText( GetString(IDS_CANCEL) );

	m_UsernameCtrl.SetTypeToString("X(32)",32,1);
	m_UsernameCtrl.SetBKColor(YELLOW);
	m_UsernameCtrl.SetTextErrColor(RED);

	m_PasswordCtrl.SetTypeToString("X(32)",32,1);
	m_PasswordCtrl.SetBKColor(YELLOW);
	m_PasswordCtrl.SetTextErrColor(RED);


	m_UsernameCtrl.SetFocus();
//	CDialog::OnInitDialog();

	CString olCaption = GetString(IDS_LOGIN_TITEL);
	olCaption  += ogCommHandler.pcmRealHostName;
	olCaption  += " / ";
	olCaption  += ogCommHandler.pcmRealHostType;
	SetWindowText(olCaption  );

	int ilY = ::GetSystemMetrics(SM_CYSCREEN);
	CRect olRect, olNewRect;
	GetWindowRect(&olRect);
	int ilHeight = olRect.bottom - olRect.top;
	int ilWidth = olRect.right - olRect.left;
	olNewRect.top = (int)((int)(ilY/2) - (int)(ilHeight/2));
	olNewRect.left = 300;
	olNewRect.bottom = olNewRect.top + ilHeight;
	olNewRect.right = olNewRect.left + ilWidth;

	MoveWindow(&olNewRect);
	
	//-- exception handling
	//CCS_CATCH_ALL

	return FALSE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

//----------------------------------------------------------------------------------------------------------------------
bool CLoginDialog::Login(const char *pcpUsername, const char *pcpPassword)
{
	//-----------------------------------
	// This function will only be called from beyond this application (eg. when starting it from within another program)
	// A regular login (using the login dialog) will call OnOK() instead
	//-----------------------------------
	bool blRc = true;

	// check the username + password
	AfxGetApp()->DoWaitCursor(1);
	

	strcpy(pcgUserName,pcpUsername);
	strcpy(pcgPassWord,pcpPassword);

	ogBasicData.omUserID = pcpUsername;
	ogCommHandler.SetUser(pcpUsername);
	strcpy(CCSCedaData::pcmUser, pcpUsername );
	strcpy(CCSCedaData::pcmReqId, ogCommHandler.pcmReqId);

	blRc = ogPrivList.Login(pcgTableExt,pcpUsername,pcpPassword,pcmAppl);
	
	AfxGetApp()->DoWaitCursor(-1);

	if( ! blRc )
		ErrorMessageBox ();
	return blRc;
}
//----------------------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------


void CLoginDialog::ErrorMessageBox ()
{
	CString olErrTxt;

	if( !strcmp(ogPrivList.omErrorMessage,"LOGINERROR INVALID_USER") )  
		olErrTxt = GetString(IDS_WRONG_USER);
	else if( !strcmp(ogPrivList.omErrorMessage,"LOGINERROR INVALID_APPLICATION") ) 
			olErrTxt = GetString(IDS_WRONG_MODUL);
	else if( !strcmp(ogPrivList.omErrorMessage,"LOGINERROR INVALID_PASSWORD") )
			olErrTxt = GetString(IDS_WRONG_PASSWORD);
	else if( !strcmp(ogPrivList.omErrorMessage,"LOGINERROR EXPIRED_USER") ) 
			olErrTxt = GetString(IDS_USER_OUT_OF_DATE);
	else if( !strcmp(ogPrivList.omErrorMessage,"LOGINERROR EXPIRED_APPLICATION") ) 
			olErrTxt = GetString(IDS_MODUL_OUT_OF_DATE);
	else if( !strcmp(ogPrivList.omErrorMessage,"LOGINERROR EXPIRED_WORKSTATION") ) 
			olErrTxt = GetString(IDS_WKS_OUT_OF_DATE);
	else if( !strcmp(ogPrivList.omErrorMessage,"LOGINERROR DISABLED_USER") ) 
			olErrTxt = GetString(IDS_USER_NOT_ACTIV);
	else if( !strcmp(ogPrivList.omErrorMessage,"LOGINERROR DISABLED_APPLICATION") ) 
			olErrTxt = GetString(IDS_MODUL_NOT_ACTIV);
	else if( !strcmp(ogPrivList.omErrorMessage,"LOGINERROR DISABLED_WORKSTATION") ) 
			olErrTxt = GetString(IDS_WKS_NOT_ACTIV);
	else if( !strcmp(ogPrivList.omErrorMessage,"LOGINERROR UNDEFINED_PROFILE") ) 
			olErrTxt = GetString(IDS_USER_WITHOUT_PROFILE);
//		else 
//			olErrTxt = ogPrivList.omErrorMessage;
	if ( olErrTxt.IsEmpty() )
		MessageBox ( GetString(IDS_LOGIN_INCORRECT), pcgAppName, MB_ICONINFORMATION );
	else
		MessageBox ( olErrTxt, GetString(IDS_LOGIN_INCORRECT), MB_ICONINFORMATION );
		
}