// ServiceCatalog.cpp : Defines the class behaviors for the application.
//

#include <stdafx.h>
#include <TLHELP32.H>
#include <versioninfo.h>

#include <gxall.h>
#include <CCSGlobl.h>
#include <basicdata.h>
#include <privlist.h>
#include <logindlg.h>
#include <ServiceCatalog.h>

#include <MainFrm.h>
#include <ServiceCatalogDoc.h>
#include <utilities.h>
#include <cedabasicdata.h>
#include <ServiceInfoDlg.h>
#include <RegisterDlg.h>
#include <Cviewer.h>
#include <ServiceCatalogView.h>
#include <AatLogin.h>
#include <CedaInitModuData.h>
#include <LibGlobl.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

//  Hilfsfunktionen, unabhängig von ServiceCatalog zum Korrigieren und 
//  Dokumentieren der TXT-Tabelle
int ChangeWrongStrings();
void DokumentiereTXTTAB ();

/////////////////////////////////////////////////////////////////////////////
// CServiceCatalogApp

BEGIN_MESSAGE_MAP(CServiceCatalogApp, CWinApp)
	//{{AFX_MSG_MAP(CServiceCatalogApp)
	ON_COMMAND(ID_APP_ABOUT, OnAppAbout)
	//}}AFX_MSG_MAP
	// Standard file based document commands
	ON_COMMAND(ID_FILE_NEW, CWinApp::OnFileNew)
	ON_COMMAND(ID_FILE_OPEN, CWinApp::OnFileOpen)
	// Standard print setup command
	ON_COMMAND(ID_FILE_PRINT_SETUP, CWinApp::OnFilePrintSetup)
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// CServiceCatalogApp construction

CServiceCatalogApp::CServiceCatalogApp()
{
	// TODO: add construction code here,
	bmLoginByCommandLine = false;
	// Place all significant initialization in InitInstance
}

/////////////////////////////////////////////////////////////////////////////
// The one and only CServiceCatalogApp object

CServiceCatalogApp theApp;
CWnd ogHelperWindow ;
/////////////////////////////////////////////////////////////////////////////
// CServiceCatalogApp initialization

BOOL CServiceCatalogApp::InitInstance()
{
	// Standard initialization
	// If you are not using these features and wish to reduce the size
	//  of your final executable, you should remove from the following
	//  the specific initialization routines you do not need.

#ifdef _AFXDLL
	Enable3dControls();			// Call this when using MFC in a shared DLL
#else
	Enable3dControlsStatic();	// Call this when linking to MFC statically
#endif

	//SetDialogBkColor();        // Set dialog background color to gray

	if (!AfxSocketInit())
	{
		AfxMessageBox("IDP_SOCKETS_INIT_FAILED");
		return FALSE;
	}

	// Initialize OLE libraries
	if (!AfxOleInit())
	{
		AfxMessageBox("IDP_OLE_INIT_FAILED");
		return FALSE;
	}

	
	AfxEnableControlContainer();	

	CWnd *popPrevInstWnd = CWnd::FindWindow ( 0, (const char*)ogAppName );
	// Change the registry key under which our settings are stored.
	// TODO: You should modify this string to be something appropriate
	// such as the name of your company or organization.
	if ( popPrevInstWnd )
	{
		if (popPrevInstWnd->m_hWnd && IsWindow(popPrevInstWnd->m_hWnd) )
		{
			ShowWindow(popPrevInstWnd->m_hWnd, SW_SHOWNORMAL );
			SetForegroundWindow (popPrevInstWnd->m_hWnd);
		}
		return FALSE;
	}
	unsigned long flags;
	flags = WS_MINIMIZE|WS_DISABLED|WS_POPUP;
	BOOL ok ;

	//  Unsichtbares Hilfsfenster anlegen, um doppelten Programmstart zu
	//  vermeiden, solange MainFrame noch nicht zu sehen ist
	ok = ogHelperWindow.CreateEx ( 0, "Static", (const char*)ogAppName, flags, 0, 0, 1 ,1, 0, 0, 0 );


	SetRegistryKey(_T("Local AppWizard-Generated Applications"));

	LoadStdProfileSettings(0);  // Load standard INI file options (including MRU)

	//---	commandline parameters
	CStringArray olCmdLineArgsArray;
	CString olCmdLine =  LPCTSTR(m_lpCmdLine);
	if(olCmdLine.GetLength() == 0)	// set defaults 
	{
		//default ohne Parameterübergabe
		olCmdLineArgsArray.Add(ogAppName);
		olCmdLineArgsArray.Add("");
		olCmdLineArgsArray.Add("");
	}
	else
	{
		int ilParamCount = ExtractItemList(olCmdLine,&olCmdLineArgsArray, ' ');
		if(ilParamCount != 3)
		{
			MessageBox(NULL,GetResString(IDS_WRONG_PARAMETERS), ogAppName, MB_ICONERROR);
			return FALSE;
		}
	}
	if ( stricmp ( olCmdLineArgsArray[0], "REGELWERK" ) )
	{
		CString olMsg = GetResString ( IDS_INVALID_CALLING_PRG );
		olMsg.Replace ( "%s", ogAppName ) ;
		MessageBox(NULL,olMsg, ogAppName, MB_ICONERROR);
		return FALSE;
	}
	//*** INIT STINGRAY GRID ***
	GXInit();

	// Standard CCS-Fonts and Brushes initialization
    InitFont();
	CreateBrushes();

	//--- initialize CEDA

	char pclConfigPath[_MAX_PATH+1];
	char pclHomeAirport[_MAX_PATH+1];

	if (getenv("CEDA") != NULL)
	{
		strcpy(pclConfigPath, getenv("CEDA"));
	}
	else
	{
		strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
	}
	
	char pclUseBcProxy[256];
	GetPrivateProfileString("GLOBAL", "BCEVENTTYPE", "BCSERV",pclUseBcProxy, sizeof pclUseBcProxy, pclConfigPath);

	//MWO/RRO 25.03.03
	if (stricmp(pclUseBcProxy,"BCPROXY") == 0) 
	{
		ogBcHandle.UseBcProxy();
		ogBcHandle.StopBc();
	}
	else if (stricmp(pclUseBcProxy,"BCCOM") == 0)
	{
		ogBcHandle.UseBcCom();
		ogBcHandle.StopBc();
	}


	//--- path to logfile for ccslog
	char pclTmp[128], pclDBParam[11];
	CString olErrorTxt;
	GetPrivateProfileString("ServiceCatalog", "LOGFILE", CCSLog::GetUfisSystemPath("\\ServiceCatalog.LOG"), pclTmp, sizeof(pclTmp), pclConfigPath);
	ogLog.Load(pclTmp);
	GetPrivateProfileString("ServiceCatalog", "ISPRM", "NO", pclTmp, sizeof(pclTmp), pclConfigPath);
	if (!strcmp(pclTmp,"YES"))
	{
		bgIsPrm = true;
	}

	GetPrivateProfileString("GLOBAL", "TABLEEXTENSION", "TAB", pcgTableExt, sizeof(pcgTableExt), pclConfigPath);
	GetPrivateProfileString("GLOBAL", "HOMEAIRPORT", "TAB", pclHomeAirport, sizeof(pclHomeAirport), pclConfigPath);

	strcpy(CCSCedaData::pcmTableExt, pcgTableExt);
	strcpy(CCSCedaData::pcmHomeAirport, pclHomeAirport);
	strcpy(CCSCedaData::pcmApplName, ogAppName);

	VersionInfo olInfo;
	VersionInfo::GetVersionInfo(NULL,olInfo);
	CCSCedaData::bmVersCheck = true;
	strcpy(CCSCedaData::pcmVersion, olInfo.omFileVersion);
	strcpy(CCSCedaData::pcmInternalBuild, olInfo.omPrivateBuild);

	ogCommHandler.SetAppName(ogAppName);
    if (ogCommHandler.Initialize() != true)  // connection error?
    {
        AfxMessageBox(CString("CedaCom:\n") + ogCommHandler.LastError());
        return FALSE;
    }

	ogBCD.SetTableExtension(pcgTableExt);

	//  text-Tabelle Lesen
	GetPrivateProfileString("GLOBAL", "LANGUAGE", "", pclTmp, sizeof(pclTmp), pclConfigPath);
//	if ( strstr ( pclTmp, "DOKU" ) )
//		DokumentiereTXTTAB ();
   	GetPrivateProfileString(ogAppName, "DBParam", "0", pclDBParam, sizeof pclDBParam, pclConfigPath);
	bool blLangInitOk = ogGUILng->MemberInit(&olErrorTxt, "XXX", "SERVICE", pclHomeAirport, 
											 pclTmp, pclDBParam );

	//--- Login Dialog
	bool blAutomaticStartUp = false;
#if	0
	CLoginDialog olLoginDlg(pcgHome, ogAppName, NULL);

	if(olCmdLineArgsArray.GetAt(0) != ogAppName)
	{
		if(olLoginDlg.Login(olCmdLineArgsArray.GetAt(1),olCmdLineArgsArray.GetAt(2)) == true)
		{
			blAutomaticStartUp = bmLoginByCommandLine = true;
		}
	}
	
	if ( !blAutomaticStartUp && (olLoginDlg.DoModal() == IDCANCEL) )
		return FALSE;

	if(ogPrivList.GetStat("InitModu") == '1')
	{
		
		RegisterDlg olRegisterDlg;
		if (olRegisterDlg.DoModal() != IDOK)
		{
			return FALSE;
		}
		
	}
#else
	CDialog olDummyDlg;	
	olDummyDlg.Create(IDD_WAIT_DIALOG,NULL);
	CAatLogin olLoginCtrl;
	olLoginCtrl.Create(NULL,WS_CHILD|WS_DISABLED,CRect(0,0,100,100),&olDummyDlg,4711);
	olLoginCtrl.SetInfoButtonVisible(TRUE);
	olLoginCtrl.SetApplicationName(ogAppName);
	olLoginCtrl.SetRegisterApplicationString(ogInitModuData.GetInitModuTxt());

	if(olCmdLineArgsArray.GetAt(0) != ogAppName)
	{
		if(olLoginCtrl.DoLoginSilentMode(olCmdLineArgsArray.GetAt(1),olCmdLineArgsArray.GetAt(2)) == "OK")
		{
			blAutomaticStartUp = true;
			bmLoginByCommandLine = true;
		}
	}
	
	if (blAutomaticStartUp == false)
	{

		if (olLoginCtrl.ShowLoginDialog() != "OK")
		{
			olDummyDlg.DestroyWindow();
			return FALSE;
		}
	}

	strcpy(pcgUserName,olLoginCtrl.GetUserName_());
	ogBasicData.omUserID = pcgUserName;

	strcpy(pcgPassWord,olLoginCtrl.GetUserPassword());

	ogCommHandler.SetUser(olLoginCtrl.GetUserName_());
	strcpy(CCSCedaData::pcmUser, olLoginCtrl.GetUserName_());
	strcpy(CCSCedaData::pcmReqId, ogCommHandler.pcmReqId);

	ogPrivList.Add(olLoginCtrl.GetPrivilegList());

	olDummyDlg.DestroyWindow();

#endif

	ogCfgData.Init();
	ogCfgData.ReadCfgData();
	ogCfgData.SetCfgData();

	// Initialise DefaultView
	CViewer olViewer;
	CStringArray olPossibleFilters;
	
	olPossibleFilters.Add("SERLIST");
	olViewer.SetViewerKey("SERLIST");
	olViewer.CreateView("<Default>", olPossibleFilters);
	olViewer.SelectView("<Default>");

	// Register the application's document templates.  Document templates
	//  serve as the connection between documents, frame windows and views.

	CSingleDocTemplate* pDocTemplate;
	pDocTemplate = new CSingleDocTemplate(
		IDR_MAINFRAME,
		RUNTIME_CLASS(CServiceCatalogDoc),
		RUNTIME_CLASS(CMainFrame),       // main SDI frame window
		RUNTIME_CLASS(CServiceCatalogView));
	AddDocTemplate(pDocTemplate);

	//  Wir machen etwas eigenes
	// Parse command line for standard shell commands, DDE, file open
	CCommandLineInfo cmdInfo;
	//ParseCommandLine(cmdInfo);

	// Dispatch commands specified on the command line
	if (!ProcessShellCommand(cmdInfo))
		return FALSE;

	// The one and only window has been initialized, so show and update it.
	//m_pMainWnd->MoveWindow(0,0, 1024, 740);

	m_pMainWnd->MoveWindow(0,30, 1024, 670);
	m_pMainWnd->UpdateWindow();

	CServiceCatalogDoc*polDoc = GetOpenDocument ();

	if ( bmLoginByCommandLine )
		polDoc->LoadCedaTable ();
	CServiceCatalogView* polView;
	polView = (CServiceCatalogView*)((CMainFrame*)m_pMainWnd)->GetActiveView() ;
	if ( polView )
	{
		int ilDefLkbz = GetPrivateProfileInt("ServiceCatalog",
								"STANDARD_DEFAULTREFERENCE", -1, pclConfigPath);
		//  wennn ungültiger Wert in Ceda-Ini Eintrag erzeugen
		if ( (ilDefLkbz<0) || (ilDefLkbz>2) )
			WritePrivateProfileString ("ServiceCatalog", 
							"STANDARD_DEFAULTREFERENCE", "2", pclConfigPath);
		else
			polView->m_DefLkbz = ilDefLkbz;
		polView->OnCedaTablesLoaded ();
	}
	return TRUE;
}


/////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	CStatic	m_ClassLibVersion;
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAboutDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL
	RecordSet *pomAktService;

// Implementation
protected:
	//{{AFX_MSG(CAboutDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnAboutService();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT
	pomAktService = 0;
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	DDX_Control(pDX, IDC_CLASSLIBVERSION, m_ClassLibVersion);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
	ON_BN_CLICKED(IDC_ABOUT_SERVICE, OnAboutService)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

// App command to run the dialog
void CServiceCatalogApp::OnAppAbout()
{
	CAboutDlg aboutDlg;
	aboutDlg.DoModal();
}

/////////////////////////////////////////////////////////////////////////////
// CServiceCatalogApp message handlers


int CServiceCatalogApp::ExitInstance() 
{
	// TODO: Add your specialized code here and/or call the base class
	DeleteBrushes();
	delete CGUILng::TheOne();
	ogGUILng = 0;

	AatHelp::ExitApp();

	return CWinApp::ExitInstance();
}


CServiceCatalogDoc* GetOpenDocument ()
{
	CMainFrame * polFrame = (CMainFrame* )theApp.m_pMainWnd;
	CServiceCatalogView *polView = 0;

	if ( polFrame )
		polView = (CServiceCatalogView*)polFrame->GetActiveView( );
	return (polView) ? polView->GetDocument() : 0;
}

BOOL CAboutDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	CMainFrame * polFrame = (CMainFrame* )theApp.m_pMainWnd;
	CServiceCatalogView *polView = 0;
	
	SetWindowText ( GetString(IDS_ABOUT_TITLE) );
	SetDlgItemText ( IDC_ABOUT_SERVICE, GetString (IDS_SERVICE_INFO) );

	if ( polFrame )
		polView = (CServiceCatalogView*)polFrame->GetActiveView( );
	if ( polView )
		pomAktService = polView->pomDspSrv;
	EnableDlgItem ( this, IDC_ABOUT_SERVICE, pomAktService!=0 ); 

	CString olVersion, olText;
	VersionInfo olInfo;
	VersionInfo::GetVersionInfo(NULL,olInfo);

	olText.LoadString ( IDS_UFISVERSION );
	SetDlgItemText ( IDC_UFISVERSION, olText );
	olText.LoadString ( IDS_SERVCAT_VERSION );
	olVersion.Format ( olText, olInfo.omFileVersion);
	olVersion += "  " + CString(__DATE__);
	SetDlgItemText ( IDC_SERVICECATALOG_VERSION, olVersion );

	m_ClassLibVersion.SetWindowText(CString(pcgClassLibVersion));
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}


void CAboutDlg::OnAboutService() 
{
	// TODO: Add your control notification handler code here
	CServiceInfoDlg olDlg(this);

	olDlg.DoModal ( pomAktService );
}


int ChangeWrongStrings()
{
	CString olText, olUrno ;
	int ilIdx = ogBCD.GetFieldIndex ( "TXT", "STRG" );
	int ilUrnoIdx = ogBCD.GetFieldIndex ( "TXT", "URNO" );
	int	n, ilChanged=0;
	bool ok=true;
	int ilFields = ogBCD.GetFieldCount( "TXT" );
	RecordSet olRecord(ilFields);
	TCHAR char180[2], char166[2];
	
	ogBCD.Read("TXT");
	int ilCount = ogBCD.GetDataCount ("TXT");
		
	char180[0] = (TCHAR)180;
	char180[1] = '\0';
	char166[0] = (TCHAR)166;
	char166[1] = '\0';
	for ( int i=0; i<ilCount; i++ )
	{
		ok = ogBCD.GetRecord ( "TXT", i, olRecord );
		if ( !ok )
			continue;
		olText = olRecord.Values[ilIdx];
		olUrno = olRecord.Values[ilUrnoIdx];
		if ( olUrno=="1237" || olUrno=="1238" || olUrno=="1239" )
		{
			char text[81];
			n = olText.Find ('?');
			strcpy ( text, olText );
			olText = text ;
		}
		if ( n>=0 )
		{
			n = olText.Replace ('?', char(9) );
			ok &= ogBCD.SetField( "TXT", "URNO", olUrno, "STRG", olText );
			ilChanged++;
		}
	}
	if ( ok )
		ok = ogBCD.Save( "TXT" );
	return ok ? ilChanged : 0;
}

void DokumentiereTXTTAB ()
{
	char		*pclSprachen[3]={"DE", "US", "IT"}, pclWhere[21];
	RecordSet	olRecord;
	int			ilIdxAppl, ilIdxStid, ilIdxTxid, ilIdxStrg, ilAnz;
	CString		olAppl, olStid, olTxid, olStrg;
	CString		olPath="C:\\DOKU\\language\\txttab." , olFilna;
	FILE		*fp;

	ilIdxAppl = ogBCD.GetFieldIndex ( "TXT", "APPL" );
	ilIdxStid = ogBCD.GetFieldIndex ( "TXT", "STID" );
	ilIdxTxid = ogBCD.GetFieldIndex ( "TXT", "TXID" );
	ilIdxStrg = ogBCD.GetFieldIndex ( "TXT", "STRG" );

	for ( int i=0; i<3; i++ )
	{
		olFilna = olPath + pclSprachen[i];
		sprintf ( pclWhere, "WHERE COCO='%s'", pclSprachen[i] );
		ogBCD.Read("TXT",pclWhere);
		ogBCD.SetSort("TXT", "APPL+,STID+,TXID+", true );
		ilAnz = ogBCD.GetDataCount("TXT");
		fp = fopen ( olFilna, "w" );
		if ( fp )
		{
			fprintf ( fp, "%-8s %-32s %-10s %s\n", "APPL", "TXID", "STID", "STRG" );
			fprintf ( fp, "\n" );
			for ( int j=0; j<ilAnz; j++ )
			{
				if ( !ogBCD.GetRecord ( "TXT", j, olRecord ) )
					continue;
				olAppl = olRecord[ilIdxAppl];
				olStid = olRecord[ilIdxStid];
				olTxid = olRecord[ilIdxTxid];
				olStrg = olRecord[ilIdxStrg];
				olStrg.Replace ( "\n", "\\n" );
				olStrg.Replace ( "\r", "\\r" );
				olStrg.Replace ( "\t", "\\t" );
				fprintf ( fp, "%-8s %-32s %-10s %s\n", olAppl, olTxid, olStid, olStrg );
			}
			fclose ( fp );
		}
	}
}
