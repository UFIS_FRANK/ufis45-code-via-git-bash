// GridRecordXChange.h: interface for the CGridRecordXChange class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_GRIDRECORDXCHANGE_H__96E79383_3F33_11D3_9342_00001C033B5D__INCLUDED_)
#define AFX_GRIDRECORDXCHANGE_H__96E79383_3F33_11D3_9342_00001C033B5D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <CCSPtrArray.h>


class CServiceGrid;
class CFunctionsGrid;
class CQualificationsGrid;
class CPersonellData;
class RecordSet;

//
//	CGridRecordXChange: Klasse f�r den Datenaustausch zw. CServiceGrid und 
//						RecordSet f�r Funktionen, Qualifikationen, 
//						Ger�tegruppen, Ger�te und Locations
//
class CGridRecordXChange : public CObject  
{
public:
	CGridRecordXChange( char *pcpTableName, char *pcpAmountField, char *pcpResUrnoField, 
						char *ResCodeField, CServiceGrid *popGrid, char *pcpTypeField=0 );
	virtual ~CGridRecordXChange();
//  Data members
	CString omTableName;		//  SEF, SEL, SEG, SEE, SEQ
	CString omAmountField;
	CString omResourceUrnoField;
	CString omResourceCodeField;
	CString omTypeField;
	CServiceGrid *pomGrid;
	CCSPtrArray<RecordSet> omRecordList;
	RecordSet		*pomDisplayedService;
	int		imFieldsInRecord;
	//  Indices into RecordSet
	int		imResCodeInd;
	int		imResUrnoInd;
	int		imAmountInd;
	int		imPrioInd;
	int		imServiceUrnoInd;
	int		imGroupTabNameInd;
	int     imTypeInd; 
	//  Implementation
	int Grid2RecordList ();
	bool RecordList2Grid ();
	virtual CString	GetResCodeFieldEntry ( CString &opResTable, CString &opResCode, 
										   bool blStaticGroup ) ;
	bool SaveRecords ( char *pcpServiceUrno =0 );
	void ResetService ();
	virtual int SetService ( RecordSet *popService );
	void OnServiceUrnoChanged ();
};


//
//	CLocationGridRecordXChange: 
//  Baseclass:	CGridRecordXChange
//				Spezielle Klasse f�r Locations
//
class CLocationGridRecordXChange: public CGridRecordXChange
{
public:
	CLocationGridRecordXChange (char *pcpTableName, char *pcpAmountField, 
							    char *pcpResUrnoField, char *ResCodeField, 
							    CServiceGrid *popGrid );
	//  Implementation
public:
	int SetService ( RecordSet *popService );
	CString	GetResCodeFieldEntry ( CString &opResTable, CString &opResCode, 
								   bool blStaticGroup ) ;
	static bool ParseResCodeFieldEntry ( CString &entry, CString &opResTable, 
										 CString &opResCodeField ) ;
};


class CPersonalXChange : public CObject  
{
public:
	CPersonalXChange( CFunctionsGrid *popFuncGrid, 
					  CQualificationsGrid *popQualGrid );
	virtual ~CPersonalXChange();

//  Data members
	CFunctionsGrid *pomFuncGrid;
	CQualificationsGrid *pomQualGrid;
	CCSPtrArray<RecordSet> omFuncRecords;
	CCSPtrArray<RecordSet> omQualRecords;
	CCSPtrArray<RecordSet> omAssignRecords;
	RecordSet		*pomDisplayedService;
	int		imFieldsInPersRecord[2];
	int		imFieldsInAssignRecord;
	//  Indices into RecordSet
	int		imPersCodeInd[2];
	int		imPersResUrnoInd[2];
	int		imPersUrnoInd[2];
	int		imPersAmountInd[2];
	int		imPersPrioInd[2];
	int		imPersServiceUrnoInd[2];
	int		imPersGroupTabNameInd[2];
	int		imAssignUrnoInd;
	CString omTableName[2];		
	CString omResourceTable[2];
	CString omResourceCodeField[2];
	//  Implementation
	int Data2RecordList (CPersonellData *popData);
	int Data2PersRecords (CPersonellData *popData, UINT ipTyp );
	int Data2AssignRecords (CPersonellData *popData);
	bool SaveRecords ( char *pcpServiceUrno =0 );
	bool SaveRecordList ( CCSPtrArray<RecordSet> *popRecords, CString opTable,
						  char *pcpServiceUrno );

	void ResetService ();
	int SetService ( RecordSet *popService, CPersonellData *popData );
	int Records2PersData (CPersonellData *popData, UINT ipTyp );
	int Records2AssignData ( CPersonellData *popData );
	void OnServiceUrnoChanged ();

};

#endif // !defined(AFX_GRIDRECORDXCHANGE_H__96E79383_3F33_11D3_9342_00001C033B5D__INCLUDED_)
