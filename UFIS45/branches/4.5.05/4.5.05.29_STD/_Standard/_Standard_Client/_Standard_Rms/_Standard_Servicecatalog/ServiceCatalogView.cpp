// ServiceCatalogView.cpp : implementation of the CServiceCatalogView class
//

#include <stdafx.h>


#include <recordset.h>
#include <CedaBasicData.h>
#include <Ccsptrarray.h>

#include <resource.h>
#include <ServiceCatalog.h>
#include <basicdata.h>
#include <utilities.h>
#include <ServiceCatalogDoc.h>
#include <GridControl.h>
#include <ServiceGrid.h>
#include <SelectService.h>
#include <ServiceList.h>
#include <ControlGrid.h>
#include <GridRecordXChange.h>
#include <ValidityDlg.h>
#include <personalgrid.h>
#include <ServiceCatalogView.h>
#include <CheckReference.h>
#include <MainFrm.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

extern CWnd ogHelperWindow;

static void ProcessBC( void *popInstance, int ipDDXType, 
					   void *vpDataPointer, CString &ropInstanceName);

/////////////////////////////////////////////////////////////////////////////
// CServiceCatalogView

IMPLEMENT_DYNCREATE(CServiceCatalogView, CFormView)

BEGIN_MESSAGE_MAP(CServiceCatalogView, CFormView)
	//{{AFX_MSG_MAP(CServiceCatalogView)
	ON_WM_CREATE()
	ON_COMMAND(ID_FILE_OPEN, OnFileOpen)
	ON_COMMAND(ID_FILE_SAVE, OnSave)
	ON_COMMAND(ID_FILE_NEW, OnNew)
	ON_BN_CLICKED(IDC_CHECK_EQUIPMENT, OnCheckEquipment)
	ON_BN_CLICKED(IDC_CHECK_LOCATION, OnCheckLocation)
	ON_BN_CLICKED(IDC_CHECK_PERSONNEL, OnCheckPersonnel)
	ON_COMMAND(ID_EDIT_CUT, OnCut)
//	ON_COMMAND(ID_EDIT_PASTE, OnPaste)
	ON_COMMAND(ID_EDIT_COPY, OnCopy)
	ON_UPDATE_COMMAND_UI(ID_EDIT_CUT, OnUpdateCut)
	ON_UPDATE_COMMAND_UI(ID_EDIT_COPY, OnUpdateCopy)
//	ON_UPDATE_COMMAND_UI(ID_EDIT_PASTE, OnUpdatePaste)
//	ON_EN_CHANGE(IDC_LKCO, OnChangeEdit)
	ON_BN_CLICKED(IDC_FSET_DEFAULT, OnClickedButton)
	ON_COMMAND(ID_VALIDITY, OnValidity)
	ON_EN_CHANGE(IDC_LKAN, OnChangeEdit)
	ON_EN_CHANGE(IDC_LKAR, OnChangeEdit)
	ON_EN_CHANGE(IDC_ATRN, OnChangeEdit)
	ON_BN_CLICKED(IDC_LKBZ_N, OnClickedButton)
	ON_BN_CLICKED(IDC_LKBZ_F, OnClickedButton)
	ON_BN_CLICKED(IDC_LKBZ_T, OnClickedButton)
	ON_BN_CLICKED(IDC_FSET_FIXED, OnClickedButton)
	ON_BN_CLICKED(IDC_RTWT_FUSS, OnClickedButton)
	ON_BN_CLICKED(IDC_RTWT_FAHRT, OnClickedButton)
	ON_BN_CLICKED(IDC_RTWT_SCHLEPP, OnClickedButton)
	ON_BN_CLICKED(IDC_RTWF_FUSS, OnClickedButton)
	ON_BN_CLICKED(IDC_RTWF_FAHRT, OnClickedButton)
	ON_BN_CLICKED(IDC_RTWF_SCHLEPP, OnClickedButton)
	ON_BN_CLICKED(IDC_FDUT, OnClickedTimeFlag)
	ON_BN_CLICKED(IDC_FSUT, OnClickedTimeFlag)
	ON_BN_CLICKED(IDC_FSDT, OnClickedTimeFlag)
	ON_BN_CLICKED(IDC_FWTT, OnClickedTimeFlag)
	ON_BN_CLICKED(IDC_FWTF, OnClickedTimeFlag)
	ON_BN_CLICKED(IDC_FFIS, OnClickedTimeFlag)
	ON_UPDATE_COMMAND_UI(ID_FILE_OPEN, OnUpdateOpen)
	ON_UPDATE_COMMAND_UI(ID_FILE_SAVE, OnUpdateSave)
	ON_UPDATE_COMMAND_UI(ID_FILE_NEW, OnUpdateNew)
	ON_UPDATE_COMMAND_UI(ID_FILE_PRINT, OnUpdatePrint)
	ON_UPDATE_COMMAND_UI(ID_FILE_PRINT_PREVIEW, OnUpdatePrintPreview)
	ON_UPDATE_COMMAND_UI(ID_VALIDITY, OnUpdateValidity)
	//}}AFX_MSG_MAP
	// Standard printing commands
	ON_COMMAND(ID_FILE_PRINT, OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, OnFilePrintPreview)
//	ON_NOTIFY_RANGE(DTN_DATETIMECHANGE, IDC_VAFR_DATE, IDC_VATO_TIME, OnDateTimeChange )
	ON_MESSAGE(WM_BCADD,OnBcAdd)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CServiceCatalogView construction/destruction

CServiceCatalogView::CServiceCatalogView()
	: CFormView(CServiceCatalogView::IDD)
{
	//{{AFX_DATA_INIT(CServiceCatalogView)
	//}}AFX_DATA_INIT
	// TODO: add construction code here
	//  memset ( pomTables, 0, sizeof(pomTables) );
	pomPFCGrid = 0;
	pomPERGrid = 0;
	pomEQUGrid = 0;
	pomEQTGrid = pomLLGGrid = 0;
	pomLGSGrid = 0;
	pomSEEXChange = 0;
	//pomSEFXChange = 0;
	//pomSEQXChange = 0;
	pomPersXChange = 0;
	pomSELXChange = 0;
	pomDspSrv = 0;
	//pomClipBoard = 0;
	ResetValues ();
	pomGridToPrint = 0;
	pomHiddenList = 0;
	bmModified = false;
	bmFunctionsExpanded = false;
	m_DefLkbz = 2;
}

CServiceCatalogView::~CServiceCatalogView()
{
	//for ( int i=0; i<6; i++ )
	//	if ( pomTables[i] )
	//		delete pomTables[i];
	if ( pomPFCGrid )
		delete pomPFCGrid;
	if ( pomPERGrid )
		delete pomPERGrid;
	if ( pomEQTGrid )
		delete pomEQTGrid;
	if ( pomEQUGrid )
		delete pomEQUGrid;
	if ( pomLLGGrid )
		delete pomLLGGrid;
	if ( pomLGSGrid )
		delete pomLGSGrid;
	if ( pomSEEXChange )
		delete pomSEEXChange;
//	if ( pomSEFXChange )
//		delete pomSEFXChange;
//	if ( pomSEQXChange )
//		delete pomSEQXChange;
	if ( pomPersXChange )
		delete pomPersXChange;
	if ( pomSELXChange )
		delete pomSELXChange;
	if ( pomDspSrv )
		delete pomDspSrv;
//	if ( pomClipBoard )
//		delete pomClipBoard;
}

void CServiceCatalogView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CServiceCatalogView)
	DDX_Control(pDX, IDC_CHECK_PERSONNEL, omCheckBoxPersonell);
	DDX_Control(pDX, IDC_CHECK_LOCATION, omCheckBoxLocation);
	DDX_Control(pDX, IDC_CHECK_EQUIPMENT, omCheckBoxEquip);
	DDX_Radio(pDX, IDC_LKBZ_N, m_Lkbz);
	DDX_Text(pDX, IDC_LKNM, m_Lknm);
	DDX_Text(pDX, IDC_FRMT, m_Frmt);
	DDX_Text(pDX, IDC_PRMT, m_Prmt);
	DDX_Text(pDX, IDC_DTYP, m_Dtyp);
	DDV_MaxChars(pDX, m_Lknm, 40);
	DDV_MaxChars(pDX, m_Frmt, 1);
	DDV_MaxChars(pDX, m_Prmt, 10);
	DDV_MaxChars(pDX, m_Dtyp, 1);
	DDX_Text(pDX, IDC_LKCC, m_Lkcc);
	DDV_MaxChars(pDX, m_Lkcc, 8);
	DDX_Text(pDX, IDC_LKAN, m_Lkan);
	DDV_MaxChars(pDX, m_Lkan, 20);
	DDX_Text(pDX, IDC_LKAR, m_Lkar);
	DDV_MaxChars(pDX, m_Lkar, 3);
	DDX_Text(pDX, IDC_ATRN, m_Atrn);
	DDV_MaxChars(pDX, m_Atrn, 12);
	DDX_Text(pDX, IDC_LKHC, m_Lkhc);
	DDV_MaxChars(pDX, m_Lkhc, 10);
	DDX_Control(pDX, IDC_LKCC, m_LkccEdit);
	DDX_Control(pDX, IDC_LKNM, m_LknmEdit);
	DDX_Control(pDX, IDC_FRMT, m_FrmtEdit);
	DDX_Control(pDX, IDC_PRMT, m_PrmtEdit);
	DDX_Control(pDX, IDC_DTYP, m_DtypEdit);
	DDX_Control(pDX, IDC_LKHC, m_LkhcEdit);
	DDX_Control(pDX, IDC_FRMT_TXT, m_FrmtText);
	DDX_Control(pDX, IDC_PRMT_TXT, m_PrmtText);
	DDX_Control(pDX, IDC_DTYP_TXT, m_DtypText);
	DDX_Check(pDX, IDC_CHECK_EQUIPMENT, m_bEquipment);
	DDX_Check(pDX, IDC_CHECK_LOCATION, m_bLocation);
	DDX_Check(pDX, IDC_CHECK_PERSONNEL, m_bPersonnel);
	DDX_Radio(pDX, IDC_RTWF_FUSS, m_Rtwf);
	DDX_Radio(pDX, IDC_RTWT_FUSS, m_Rtwt);
	DDX_Radio(pDX, IDC_FSET_DEFAULT, m_Fset);
	DDX_Check(pDX, IDC_FDUT, m_Fdut);
	DDX_Check(pDX, IDC_FSDT, m_Fsdt);
	DDX_Check(pDX, IDC_FSUT, m_Fsut);
	DDX_Check(pDX, IDC_FWTF, m_Fwtf);
	DDX_Check(pDX, IDC_FWTT, m_Fwtt);
	DDX_Control(pDX, IDC_SDUT, m_SdutEdit);
	DDX_Text(pDX, IDC_SDUT, m_Sdut);
	DDV_MaxChars(pDX, m_Sdut, 3);
	DDX_Control(pDX, IDC_SSUT, m_SsutEdit);
	DDX_Text(pDX, IDC_SSUT, m_Ssut);
	DDV_MaxChars(pDX, m_Ssut, 3);
	DDX_Text(pDX, IDC_SSDT, m_Ssdt);
	DDV_MaxChars(pDX, m_Ssdt, 3);
	DDX_Control(pDX, IDC_SSDT, m_SsdtEdit);
	DDX_Text(pDX, IDC_SWTF, m_Swtf);
	DDV_MaxChars(pDX, m_Swtf, 3);
	DDX_Control(pDX, IDC_SWTF, m_SwtfEdit);
	DDX_Text(pDX, IDC_SWTT, m_Swtt);
	DDV_MaxChars(pDX, m_Swtt, 3);
	DDX_Control(pDX, IDC_SWTT, m_SwttEdit);
	DDX_Check(pDX, IDC_FFIS, m_Ffis);
	//}}AFX_DATA_MAP
	if (!pDX->m_bSaveAndValidate )
	{
		//m_LknmEdit.GetStatus();
	}
}

BOOL CServiceCatalogView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CFormView::PreCreateWindow(cs);
}

void CServiceCatalogView::OnInitialUpdate()
{
	CFormView::OnInitialUpdate();

	char pclConfigPath[_MAX_PATH+1];

	if (getenv("CEDA") != NULL)
	{
		strcpy(pclConfigPath, getenv("CEDA"));
	}
	else
	{
		strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
	}

	char pclUseBcProxy[256];
	GetPrivateProfileString("GLOBAL", "BCEVENTTYPE", "BCSERV",pclUseBcProxy, sizeof pclUseBcProxy, pclConfigPath);

	//MWO/RRO 25.03.03
	if (stricmp(pclUseBcProxy,"BCPROXY") == 0 || stricmp(pclUseBcProxy,"BCCOM") == 0)
	{
		//MWO/RRO
		//ogCommHandler.RegisterBcWindow(this);
		ogBcHandle.StartBc(); 
		//END MWO/RRO
	}
	else
	{
		ogCommHandler.RegisterBcWindow(this);
	}

	GetParentFrame()->RecalcLayout();
	ResizeParentToFit();

	IniStatics ();
//	InitializeGrids ();
//	InitializeGridXChanges ();
	SetBdpsState();
	EnableControls ();

	m_LkhcEdit.SetTypeToDouble(7,2,0,9999999.0);
	m_LkhcEdit.SetTextErrColor(RED);
	m_LkhcEdit.SetInitText(m_Lkhc);
	m_LkhcEdit.EnableWindow(FALSE);
	
	//  ! CodeFeld l�nger als in GHS
	m_LkccEdit.SetFormat("x|#x|#x|#x|#x|#x|#x|#x|#");		
	m_LkccEdit.SetTextLimit(1,8);
	m_LkccEdit.SetBKColor(YELLOW);  
	m_LkccEdit.SetTextErrColor(RED);
	m_LkccEdit.SetInitText(m_Lkcc);
	
	m_LknmEdit.SetTypeToString("X(40)",40,1);
	m_LknmEdit.SetBKColor(YELLOW);
	m_LknmEdit.SetTextErrColor(RED);
	m_LknmEdit.SetInitText(m_Lknm);

	if (bgIsPrm)
	{
		m_DtypEdit.SetInitText(m_Dtyp);
		m_FrmtEdit.SetInitText(m_Frmt);
		m_PrmtEdit.SetInitText(m_Prmt);
		
		m_DtypEdit.SetTypeToString("X(1)",1,1);
		m_FrmtEdit.SetTypeToString("X(1)",1,1);
		m_PrmtEdit.SetTypeToString("X(10)",10,1);
	}
	else
	{
		//m_DtypEdit.
		m_FrmtEdit.ShowWindow(SW_HIDE);
		m_PrmtEdit.ShowWindow(SW_HIDE);
		m_DtypEdit.ShowWindow(SW_HIDE);
		// lli: need to hide the label as well.
		m_FrmtText.ShowWindow(SW_HIDE);
		m_PrmtText.ShowWindow(SW_HIDE);
		m_DtypText.ShowWindow(SW_HIDE);
	}
	m_SdutEdit.SetTypeToInt(0,999);
	m_SsutEdit.SetTypeToInt(0,999);
	m_SsdtEdit.SetTypeToInt(0,999);
	m_SwtfEdit.SetTypeToInt(0,999);
	m_SwttEdit.SetTypeToInt(0,999);
	m_SdutEdit.SetInitText(m_Sdut);
	m_SsutEdit.SetInitText(m_Ssut);
	m_SsdtEdit.SetInitText(m_Ssdt);
	m_SwtfEdit.SetInitText(m_Swtf);
	m_SwttEdit.SetInitText(m_Swtt);
	m_SdutEdit.SetTextErrColor(RED);
	m_SsutEdit.SetTextErrColor(RED);
	m_SsdtEdit.SetTextErrColor(RED);
	m_SwtfEdit.SetTextErrColor(RED);
	m_SwttEdit.SetTextErrColor(RED);
	
	//  Hilfsfenster (s. CWinApp::InitInstance l�schen
	if ( ogHelperWindow.m_hWnd )
		ogHelperWindow.DestroyWindow ();

}

/////////////////////////////////////////////////////////////////////////////
// CServiceCatalogView printing

BOOL CServiceCatalogView::OnPreparePrinting(CPrintInfo* pInfo)
{
	BOOL ilRet;
	pInfo->SetMaxPage(0xffff);

	pInfo->m_pPD->m_pd.Flags &= ~PD_NOSELECTION;
	pInfo->m_pPD->m_pd.hInstance = AfxGetInstanceHandle();

	// default preparation

	ilRet = DoPreparePrinting(pInfo);
	if ( !ilRet )
	{	//  es wird nicht gedruckt
		if ( pomHiddenList )
		{	//  Wenn Drucken oder Vorschau nicht von Service-Auswahl aus aufgerufen
			//	worden ist, pomHiddenList wieder freigeben
			pomHiddenList->DestroyWindow();
			delete pomHiddenList;
		}
		pomHiddenList = 0;
		pomGridToPrint = 0;
	}
	return ilRet;
}

void CServiceCatalogView::OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo)
{
	// TODO: add extra initialization before printing
	DoPrintSettings ();
	if ( pomGridToPrint )
		pomGridToPrint->OnBeginPrinting ( pDC, pInfo ) ;
}

void CServiceCatalogView::OnEndPrinting(CDC* pDC, CPrintInfo* pInfo)
{
	// TODO: add cleanup after printing
	if ( pomGridToPrint )
	{		
		pomGridToPrint->OnEndPrinting( pDC, pInfo ) ;
		//  Wenn Drucken oder Vorschau nicht von Service-Auswahl aus aufgerufen
		//	worden ist, pomHiddenList wieder freigeben
		if ( pomHiddenList )
		{
			pomHiddenList->DestroyWindow();
			delete pomHiddenList;
		}
		pomHiddenList = 0;
		pomGridToPrint = 0;
	}
}

void CServiceCatalogView::OnPrint(CDC* pDC, CPrintInfo* pInfo )
{
	// TODO: add customized printing code here
	if ( pomGridToPrint )
	{
		pomGridToPrint->OnGridPrint ( pDC, pInfo ) ;
	}
}

void CServiceCatalogView::OnPrepareDC( CDC* pDC, CPrintInfo* pInfo /*= NULL*/ )
{
	if ( pomGridToPrint )
		pomGridToPrint->OnGridPrepareDC ( pDC, pInfo ) ;

}

void CServiceCatalogView::DoPrintSettings ()
{
	if ( pomGridToPrint )
	{
		int  top, left, bottom, right;
		//ROWCOL rowCount;
		CGXGridParam* popParam = pomGridToPrint->GetParam();
		CGXProperties* popProp = popParam->GetProperties();
		popProp->GetMargins( top, left, bottom, right) ;
		popProp->SetMargins( 72, 18, 60, 0 ) ;
		popProp->GetDistances( top, bottom);
		popProp->SetDistances( 18, 18 );
		
		CGXData& rolDataHeader = popProp->GetDataHeader();
		rolDataHeader.DeleteContents();
		rolDataHeader.StoreStyleRowCol(1, 1, CGXStyle().SetValue(GetString(IDS_KATALOG_TXT)).
										SetFont(CGXFont().SetSize(14).SetBold(TRUE)), gxCopy );
		rolDataHeader.StoreStyleRowCol(1, 3, CGXStyle().SetValue("$P/$N").
										SetFont(CGXFont().SetSize(14).SetBold(TRUE)), gxCopy );
	
		

		CGXData& rolDataFooter = popProp->GetDataFooter();
		rolDataFooter.DeleteContents();
		rolDataFooter.StoreStyleRowCol(1, 1, CGXStyle().SetValue(""), gxCopy );
		rolDataFooter.StoreStyleRowCol(1, 2, CGXStyle().SetValue("UFIS Airport Solutions GmbH,  $D{%d.%m.%Y %H:%M}").SetFont(CGXFont().SetSize(12)), gxCopy );
		rolDataFooter.StoreStyleRowCol(1, 3, CGXStyle().SetValue(""), gxCopy );
		
	}
}

/////////////////////////////////////////////////////////////////////////////
// CServiceCatalogView diagnostics

#ifdef _DEBUG
void CServiceCatalogView::AssertValid() const
{
	CFormView::AssertValid();
}

void CServiceCatalogView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}

CServiceCatalogDoc* CServiceCatalogView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CServiceCatalogDoc)));
	return (CServiceCatalogDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CServiceCatalogView message handlers

int CServiceCatalogView::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CFormView::OnCreate(lpCreateStruct) == -1)
		return -1;

	// TODO: Add your specialized creation code here
		
	return 0;
}

void CServiceCatalogView::InitializeGrids ()
{
//	pomPFCGrid = new CServiceGrid( this, IDC_FGSTAB, "PFC", GetDocument() );
	pomPERGrid = new CQualificationsGrid( this, IDC_PGSTAB, GetDocument() );
	pomPFCGrid = new CFunctionsGrid ( this, IDC_FGSTAB, GetDocument(),
									  pomPERGrid );
//	pomPERGrid = new CServiceGrid( this, IDC_PGSTAB, "PER", GetDocument() );
	pomEQTGrid = new CControlGrid( this, IDC_GEGTAB, GetString(IDS_EQT_HEADER),
								    EQUIPMENT );
	pomEQUGrid = new CVarServiceGrid( this, IDC_EQUTAB, GetString(IDS_EQU_HEADER)  );
	pomLLGGrid = new CControlGrid ( this, IDC_LLGTAB, GetString(IDS_LLG_HEADER),
									LOCATIONS );
	pomLGSGrid = new CVarServiceGrid( this, IDC_LGSTAB, GetString(IDS_LGS_HEADER) );
	EnableAllGrids ( FALSE );
}


void CServiceCatalogView::OnFileOpen() 
{
	// TODO: Add your command handler code here
	CSelectService	olDlg(this);
	RecordSet		olServiceToDisplay;
	int ilRet;

	SetFocus ();
	if ( !HandleModifiedService () )
		return;
	ilRet = olDlg.DoModal ( pcgTableName, &olServiceToDisplay );
	if ( ilRet == IDOK )
	{
		DisplayService ( olServiceToDisplay );
		OnInitialUpdate();  //hag010403
		SetDocTitle ();
	}
	
}

/*
void CServiceCatalogView::OnFileOpen() 
{
	// TODO: Add your command handler code here
	static CServiceList	olDlg(this);
	RecordSet		olServiceToDisplay;

	olDlg.DoModeless ( pcgTableName, &olServiceToDisplay );
	
}
*/

void CServiceCatalogView::DisplayService ( RecordSet &opService )
{
	ResetValues ();
	ResetGridValues ();
	ResetGridXChanges ();

	pomDspSrv = new RecordSet(opService);

	UINT	ilWert;
	CString olWert;
	
	/*
	olWert = pomDspSrv->Values[ogBCD.GetFieldIndex(pcgTableName,"CDAT")];
	if ( !olWert.IsEmpty () )
	{
		olTime = DBStringToDateTime(olWert);
		m_CdatDate = olTime.Format( "%d.%m.%Y" );
		m_CdatTime = olTime.Format( "%H:%M" );
	}
	olWert = pomDspSrv->Values[ogBCD.GetFieldIndex(pcgTableName,"LSTU")];
	if ( !olWert.IsEmpty () )
	{
		olTime = DBStringToDateTime(olWert);
		m_LstuDate = olTime.Format( "%d.%m.%Y" );
		m_LstuTime = olTime.Format( "%H:%M" );
	}
	m_Usec = pomDspSrv->Values[ogBCD.GetFieldIndex(pcgTableName,"USEC")];
	m_Useu = pomDspSrv->Values[ogBCD.GetFieldIndex(pcgTableName,"USEU")];
	*/
	olWert = pomDspSrv->Values[ogBCD.GetFieldIndex(pcgTableName,"FFIS")];
	//  flugunabh�ngig	??
	m_Ffis = ( !olWert.IsEmpty() && olWert[0] == '1' ) ? TRUE : FALSE;

	olWert = pomDspSrv->Values[ogBCD.GetFieldIndex(pcgTableName,"SEER")];
	if ( sscanf ( pCHAR(olWert), "%u", &ilWert ) && ( ilWert <= 2 ) )
		m_Lkbz = (ilWert+2) % 3;
	else 
		m_Lkbz = -1;
	/*
	if ( olWert[0]=='1' )
		m_Lkbz = 0;
	if ( olWert[0]=='2' )
		m_Lkbz = 1;*/

	m_Lknm = pomDspSrv->Values[ogBCD.GetFieldIndex(pcgTableName,"SNAM")];
	if (bgIsPrm)
	{
		m_Dtyp = pomDspSrv->Values[ogBCD.GetFieldIndex(pcgTableName,"DTYP")];
		m_Frmt = pomDspSrv->Values[ogBCD.GetFieldIndex(pcgTableName,"FRMT")];
		m_Prmt = pomDspSrv->Values[ogBCD.GetFieldIndex(pcgTableName,"PRMT")];
	}
	m_Lkcc = pomDspSrv->Values[ogBCD.GetFieldIndex(pcgTableName,"SECO")];
//	m_Lkco = pomDspSrv->Values[ogBCD.GetFieldIndex(pcgTableName,"SSNM")];
	m_Lkan = pomDspSrv->Values[ogBCD.GetFieldIndex(pcgTableName,"SEAX")];
	m_Lkar = pomDspSrv->Values[ogBCD.GetFieldIndex(pcgTableName,"SEIV")];
	m_Atrn = pomDspSrv->Values[ogBCD.GetFieldIndex(pcgTableName,"SSAP")];
	m_Lkhc = pomDspSrv->Values[ogBCD.GetFieldIndex(pcgTableName,"SEHC")];

	olWert = pomDspSrv->Values[ogBCD.GetFieldIndex(pcgTableName,"RTWT")];
	if ( (sscanf ( pCHAR(olWert), "%u", &ilWert )>0) && ( ilWert <= 2 ) )
		m_Rtwt = ilWert;
	else
		m_Rtwt = -1;
	olWert = pomDspSrv->Values[ogBCD.GetFieldIndex(pcgTableName,"RTWF")];
	if ( (sscanf ( pCHAR(olWert), "%u", &ilWert )>0) && ( ilWert <= 2 ) )
		m_Rtwf = ilWert;
	else
		m_Rtwf = -1;

	olWert = pomDspSrv->Values[ogBCD.GetFieldIndex(pcgTableName,"FSET")];
	if ( olWert[0] == '1')
		m_Fset = 1;
	//ableDlgItem ( this, IDC_FSET_FIXED, m_Fset );

	olWert = pomDspSrv->Values[ogBCD.GetFieldIndex(pcgTableName,"FDUT")];
	m_Fdut = ( olWert[0] == '1' ) ? TRUE : FALSE;
	olWert = pomDspSrv->Values[ogBCD.GetFieldIndex(pcgTableName,"FSDT")];
	m_Fsdt = ( olWert[0] == '1' ) ? TRUE : FALSE;
	olWert = pomDspSrv->Values[ogBCD.GetFieldIndex(pcgTableName,"FSUT")];
	m_Fsut = ( olWert[0] == '1' ) ? TRUE : FALSE;
	olWert = pomDspSrv->Values[ogBCD.GetFieldIndex(pcgTableName,"FWTF")];
	m_Fwtf = ( olWert[0] == '1' ) ? TRUE : FALSE;
	olWert = pomDspSrv->Values[ogBCD.GetFieldIndex(pcgTableName,"FWTT")];
	m_Fwtt = ( olWert[0] == '1' ) ? TRUE : FALSE;

	m_Sdut = pomDspSrv->Values[ogBCD.GetFieldIndex(pcgTableName,"SDUT")];
	m_Ssut = pomDspSrv->Values[ogBCD.GetFieldIndex(pcgTableName,"SSUT")];
	m_Ssdt = pomDspSrv->Values[ogBCD.GetFieldIndex(pcgTableName,"SSDT")];
	m_Swtf = pomDspSrv->Values[ogBCD.GetFieldIndex(pcgTableName,"SWTF")];
	m_Swtt = pomDspSrv->Values[ogBCD.GetFieldIndex(pcgTableName,"SWTT")];
	
	olWert = pomDspSrv->Values[ogBCD.GetFieldIndex(pcgTableName,"URNO")];
	LoadServiceValidity ( olWert, m_Vafr, m_Vato );

	DisplaySelectedResources ( *pomDspSrv );

	EnableEquipmentGrids ( m_bEquipment ); 
	EnableLocationsGrids ( m_bLocation ); 
	//EnablePersonnelGrids ( m_bPersonnel ); nun in CPersonalXChange::SetService
	EnableControls ();
	UpdateData(FALSE);
}

void CServiceCatalogView::OnSave() 
{
	// TODO: Add your command handler code here
	SetFocus ( );	//  Damit das event. aktive Grid ein OnEndEditing durchl�uft
	DoSave ();
}

bool CServiceCatalogView::DoSave ()
{
	bool blInputOk;
	UpdateData ();
	blInputOk = CheckInput ();
	if ( blInputOk )
	{
		if ( !m_Vato.IsEmpty() && ( m_Vafr.Compare(m_Vato)>0 ) )
			LangMsgBox ( m_hWnd, IDS_VALID_PERIOD_NOT_OK, IDS_ERROR,
						 MB_ICONEXCLAMATION|MB_OK );
		else
		{
			SaveAllRecords ();
			SetDocTitle ();
			//  Flags f�r "Modified Record" zur�cksetzen
			bmModified = false;	
			SetGridDirtyFlags ( false );
			SetCCSEditDirtyFlags( false );
			return true;
		}
	}
	return false;
}

void CServiceCatalogView::OnNew() 
{
	// TODO: Add your command handler code here
	SetFocus ( );	//  Damit das event. aktive Grid ein OnEndEditing durchl�uft
	if ( !HandleModifiedService () )
		return;
	ResetValues ();
	ResetGridValues ();
	EnableAllGrids ( FALSE );
	UpdateData(FALSE);
	SetDocTitle ();
	EnableControls ();
	omServicePersonal.Reset();
}

void CServiceCatalogView::ResetValues ()
{
	//m_CdatDate = _T("");
	//m_CdatTime = _T("");
	//m_LstuDate = _T("");
	//m_LstuTime = _T("");
	//m_Usec = _T("");
	//m_Useu = _T("");
	SetDefaultValitidy ();

	//m_Lkbz = 2;		//  default reference aus Ini-File f�r Alitalia
	m_Lkbz = m_DefLkbz;
	m_Lknm = _T("");
	// lli: it's better to separate the default value for PRM and None PRM for ease of use
	if(bgIsPrm)
	{
		m_Dtyp = _T("");
		m_Frmt = _T("");
		m_Prmt = _T("");
	}
	else
	{
		m_Dtyp = _T("");
		m_Frmt = _T("");
		m_Prmt = _T("");
	}
	m_Lkcc = _T("");
//	m_Lkco = _T("");
	m_Lkan = _T("");
	m_Lkar = _T("");
	m_Atrn = _T("");
	m_Lkhc = _T("");
	m_bEquipment = FALSE;
	m_bLocation = FALSE;
	m_bPersonnel = FALSE;
	m_Rtwf = 0;
	m_Rtwt = 0;
	m_Fset = 0;
	m_Fdut = FALSE;
	m_Fsdt = FALSE;
	m_Fsut = FALSE;
	m_Fwtf = TRUE;
	m_Fwtt = TRUE;
	m_Sdut = _T("");
	m_Ssut = _T("");
	m_Ssdt = _T("");
	m_Swtf = _T("5");
	m_Swtt = _T("5");
	m_Ffis = FALSE;

	if ( pomDspSrv )
		delete pomDspSrv;
	pomDspSrv = 0;
	bmModified = false;
}

bool CServiceCatalogView::CheckInput ()
{
	CString olErrorText = "";
	CString olKeineDaten= GetString(IDS_FIELD_WITHOUT_DATA);
	CString olNichtFormat = GetString(IDS_FORMAT_OF_DATA_NOT_OK);
	CString olResDoppelt = GetString(IDS_RESOURCE_DOPPELT);
	
	bool ilStatus = true;
	int n = sizeof(bool);
	int	ilResCnt=0;
	//  Merkt sich das erste Feld mit falschem Input
	CEdit *polCtrl = 0;

	if( m_LknmEdit.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += GetString(IDS_NAME) +  (m_Lknm.IsEmpty() ?  olKeineDaten : olNichtFormat );
		polCtrl = &m_LknmEdit;
	}
	if(m_LkccEdit.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += GetString(IDS_CODE) +  (m_Lkcc.IsEmpty() ?  olKeineDaten : olNichtFormat );
		if ( !polCtrl )
			polCtrl = &m_LkccEdit;
	}
	if(m_LkhcEdit.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += GetString(IDS_PREIS) + olNichtFormat;
		if ( !polCtrl )
			polCtrl = &m_LkhcEdit;
	}
	if ( m_Fdut && (m_SdutEdit.GetStatus() == false) )
	{
		ilStatus = false;
		olErrorText += GetString(IDS_DUTY_TIME) + olNichtFormat;
		if ( !polCtrl )
			polCtrl = &m_SdutEdit;
	}
	if( m_Fsut && ( m_SsutEdit.GetStatus() == false) )
	{
		ilStatus = false;
		olErrorText += GetString(IDS_SETUP_TIME) + olNichtFormat;
		if ( !polCtrl )
			polCtrl = &m_SsutEdit;
	}
	if( m_Fsdt && (m_SsdtEdit.GetStatus() == false) )
	{
		ilStatus = false;
		olErrorText += GetString(IDS_SETDOWN_TIME) + olNichtFormat;
		if ( !polCtrl )
			polCtrl = &m_SsdtEdit;
	}
	if( m_Fwtf && (m_SwtfEdit.GetStatus() == false) )
	{
		ilStatus = false;
		olErrorText += GetString(IDS_WAY_FROM_TIME) + olNichtFormat;
		if ( !polCtrl )
			polCtrl = &m_SwtfEdit;
	}
	if( m_Fwtt && (m_SwttEdit.GetStatus() == false) )
	{
		ilStatus = false;
		olErrorText += GetString(IDS_WAY_TO_TIME) + olNichtFormat;
		if ( !polCtrl )
			polCtrl = &m_SwttEdit;
	}
	ilResCnt = 0;
	//  Grids auf doppelt ausgew�hlte Resourcen �berpr�fen
	if ( m_bEquipment )
	{
		if ( pomEQUGrid->DoppelteResourcen () )
		{
			ilStatus = false;
			olErrorText += pomEQUGrid->GetTitle () + olResDoppelt;
		}
		ilResCnt += pomEQUGrid->GetSumOfAmounts ();
	}
	if ( m_bLocation )
	{
		if ( pomLGSGrid->DoppelteResourcen () )
		{
			ilStatus = false;
			olErrorText += pomLGSGrid->GetTitle () + olResDoppelt;
		}
		ilResCnt += pomLGSGrid->GetSumOfAmounts ();
	}
	if ( m_bPersonnel )
	{
		int  ilSumOfFunctions=0;
		int  ilMaxQualifications=0;
		
		if ( pomPFCGrid->DoppelteResourcen () )
		{
			ilStatus = false;
			olErrorText += pomPFCGrid->GetTitle () + olResDoppelt;
		}
		if ( pomPERGrid->DoppelteResourcen () )
		{
			ilStatus = false;
			olErrorText += pomPERGrid->GetTitle () + olResDoppelt;
		}
		
		//  pomPFCGrid enth�t Funktionen
		//ilSumOfFunctions = pomPFCGrid->GetSumOfAmounts ();
		//  pomPERGrid enth�t Qualifikationen	
		//ilMaxQualifications = pomPERGrid->GetMaxAmount ();
		ilSumOfFunctions = 
				omServicePersonal.GetSumOfPersonal(TYP_FUNC);
		ilMaxQualifications = 
				omServicePersonal.GetMaxAmountOfPersonal (TYP_QUALI);

		if ( ilMaxQualifications > ilSumOfFunctions )
		{
			ilStatus = false;
			olErrorText += GetString(IDS_NO_QUALI_TOO_BIG);
		}
		ilResCnt += ilSumOfFunctions;
	}
	
	///////////////////////////////////////////////////////////////////////////
	//Konsistenz-Check//
	if (ilStatus == true)
	{
		CString olUrnoInDB, olNeueUrno;

		olUrnoInDB = ogBCD.GetField ( pcgTableName, "SECO", m_Lkcc, "URNO" );
		if ( pomDspSrv )
			olNeueUrno = pomDspSrv->Values[ogBCD.GetFieldIndex(pcgTableName,"URNO")];
	
		if( !olUrnoInDB.IsEmpty () &&
			( olUrnoInDB!= olNeueUrno ) )
		{
			ilStatus = false;
 			olErrorText += GetString(IDS_CODE_ALREADY_IN_DB);
		}
	}
	
	///////////////////////////////////////////////////////////////////////////
	if ( !ilStatus )
	{
		Beep(440,70);
		MessageBox(olErrorText,GetString(IDS_ERROR),MB_ICONEXCLAMATION);
		if ( polCtrl )
		{
			polCtrl->SetFocus();
			polCtrl->SetSel(0,-1);
		}
	}
	else
	{
		//  Pr�fen ob Anzahl ausgew�hlter Resourcen > 0
		int ilRet;
		if ( ilResCnt < 1 )
		{
			ilRet = LangMsgBox ( m_hWnd, IDS_NORES_CHOSEN, AFX_IDS_APP_TITLE,
							 MB_YESNO | MB_ICONQUESTION | MB_DEFBUTTON2 );
			if ( ilRet != IDYES )
				ilStatus = 0;
		}
	}
	return ilStatus;
}

void CServiceCatalogView::OnCheckEquipment() 
{
	// TODO: Add your control notification handler code here
	UpdateData ();
	bmModified = true;
	EnableEquipmentGrids ( m_bEquipment ); 
}

void CServiceCatalogView::OnCheckLocation() 
{
	// TODO: Add your control notification handler code here
	UpdateData ();
	bmModified = true;
	EnableLocationsGrids ( m_bLocation ); 
}

void CServiceCatalogView::OnCheckPersonnel() 
{
	// TODO: Add your control notification handler code here
	SetFocus();
	UpdateData ();
	bmModified = true;
	EnablePersonnelGrids ( m_bPersonnel );
}


void CServiceCatalogView::DisplaySelectedResources ( RecordSet &opService )
{
	if ( pomSEEXChange->SetService ( &opService ) > 0 )
		m_bEquipment = TRUE;	

//	if ( pomSEFXChange->SetService ( &opService ) > 0 )
//		m_bPersonnel = TRUE;	
//	if ( pomSEQXChange->SetService ( &opService ) > 0 )
//		m_bPersonnel = TRUE;	
	if ( pomPersXChange->SetService ( &opService, &omServicePersonal ) > 0 )
		m_bPersonnel = TRUE;	

		
	if ( pomSELXChange->SetService ( &opService ) > 0 )
		m_bLocation = TRUE;	
}

void CServiceCatalogView::ResetGridValues ()
{
	if (pomPFCGrid)
		pomPFCGrid->ResetValues ();
	if (pomPERGrid)
		pomPERGrid->ResetValues ();
	if (pomEQTGrid)
		pomEQTGrid->ResetValues ();
	if (pomEQUGrid)
		pomEQUGrid->ResetValues ();
	if (pomLLGGrid)
		pomLLGGrid->ResetValues ();	
	if (pomLGSGrid)
		pomLGSGrid->ResetValues ();	
}


void CServiceCatalogView::EnablePersonnelGrids ( BOOL enable )
{
	pomPFCGrid->EnableGrid ( enable );
	pomPERGrid->EnableGrid ( enable );
}

void CServiceCatalogView::EnableEquipmentGrids ( BOOL enable )
{
	pomEQTGrid->EnableGrid ( enable );
	pomEQUGrid->EnableGrid ( enable );
}

void CServiceCatalogView::EnableLocationsGrids ( BOOL enable )
{
	pomLLGGrid->EnableGrid ( enable );
	pomLGSGrid->EnableGrid ( enable );
}

void CServiceCatalogView::EnableAllGrids ( BOOL enable )
{
	EnablePersonnelGrids ( enable );
	EnableEquipmentGrids ( enable );
	EnableLocationsGrids ( enable );
}

int CServiceCatalogView::SaveAllRecords ()
{
	CString		olUrno;
	char		buffer[21];
	bool		blOk=true;
	//RecordSet	olRecord(ogBCD.GetFieldCount(pcgTableName));
	
	theApp.BeginWaitCursor ();

	if ( !pomDspSrv )
		pomDspSrv = new RecordSet(ogBCD.GetFieldCount(pcgTableName));

	//  Lokale Variable in RecordSet �bernehmen
	pomDspSrv->Values[ogBCD.GetFieldIndex(pcgTableName,"FDUT")] = m_Fdut && (m_Lkbz!=2) ? "1" : "0";
	pomDspSrv->Values[ogBCD.GetFieldIndex(pcgTableName,"FSDT")] = m_Fsdt ? "1" : "0";
	pomDspSrv->Values[ogBCD.GetFieldIndex(pcgTableName,"FSET")] = m_Fset ? "1" : "0";
	pomDspSrv->Values[ogBCD.GetFieldIndex(pcgTableName,"FSUT")] = m_Fsut ? "1" : "0";
	pomDspSrv->Values[ogBCD.GetFieldIndex(pcgTableName,"FWTF")] = m_Fwtf ? "1" : "0";
	pomDspSrv->Values[ogBCD.GetFieldIndex(pcgTableName,"FWTT")] = m_Fwtt ? "1" : "0";
	pomDspSrv->Values[ogBCD.GetFieldIndex(pcgTableName,"RTWF")] = m_Fwtf && (m_Rtwf>=0) ? _itoa( m_Rtwf, buffer, 10 ) : "";
	pomDspSrv->Values[ogBCD.GetFieldIndex(pcgTableName,"RTWT")] = m_Fwtt && (m_Rtwt>=0) ? _itoa( m_Rtwt, buffer, 10 ) : "";
	pomDspSrv->Values[ogBCD.GetFieldIndex(pcgTableName,"SDUT")] = m_Fdut ? m_Sdut : "";
	pomDspSrv->Values[ogBCD.GetFieldIndex(pcgTableName,"SEAX")] = m_Lkan;
	pomDspSrv->Values[ogBCD.GetFieldIndex(pcgTableName,"SECO")] = m_Lkcc;
	pomDspSrv->Values[ogBCD.GetFieldIndex(pcgTableName,"SEER")] = (m_Lkbz>=0) ? _itoa( (m_Lkbz+1)%3, buffer, 10 ) : "";
	pomDspSrv->Values[ogBCD.GetFieldIndex(pcgTableName,"SEHC")] = m_Lkhc;
	pomDspSrv->Values[ogBCD.GetFieldIndex(pcgTableName,"SEIV")] = m_Lkar;
	sprintf ( buffer, "%d%d%d", m_bPersonnel, m_bEquipment, m_bLocation );
	pomDspSrv->Values[ogBCD.GetFieldIndex(pcgTableName,"SETY")] = buffer;
	pomDspSrv->Values[ogBCD.GetFieldIndex(pcgTableName,"SNAM")] = m_Lknm;
	if (bgIsPrm)
	{
		pomDspSrv->Values[ogBCD.GetFieldIndex(pcgTableName,"DTYP")] = m_Dtyp;
		pomDspSrv->Values[ogBCD.GetFieldIndex(pcgTableName,"FRMT")] = m_Frmt;
		pomDspSrv->Values[ogBCD.GetFieldIndex(pcgTableName,"PRMT")] = m_Prmt;
	}
	pomDspSrv->Values[ogBCD.GetFieldIndex(pcgTableName,"SSAP")] = m_Atrn;
	pomDspSrv->Values[ogBCD.GetFieldIndex(pcgTableName,"SSDT")] = m_Fsdt ? m_Ssdt : "";
//	pomDspSrv->Values[ogBCD.GetFieldIndex(pcgTableName,"SSNM")] = m_Lkco;
	pomDspSrv->Values[ogBCD.GetFieldIndex(pcgTableName,"SSUT")] = m_Fsut ? m_Ssut : "";
	pomDspSrv->Values[ogBCD.GetFieldIndex(pcgTableName,"SWTF")] = m_Fwtf ? m_Swtf : "";
	pomDspSrv->Values[ogBCD.GetFieldIndex(pcgTableName,"SWTT")] = m_Fwtt ? m_Swtt : "";
	/*
	pomDspSrv->Values[ogBCD.GetFieldIndex(pcgTableName,"VAFR")] = 
						CTwoOleDateTimeToDBString(m_VafrDate, m_VafrTime );
	pomDspSrv->Values[ogBCD.GetFieldIndex(pcgTableName,"VATO")] = 
						CTwoOleDateTimeToDBString(m_VatoDate, m_VatoTime );
	*/
	pomDspSrv->Values[ogBCD.GetFieldIndex(pcgTableName,"FFIS")] = m_Ffis ? "1" : "0";

	//  Wenn keine Urno eingetragen, handelt es sich um neuen Datensatz
	olUrno = pomDspSrv->Values[ogBCD.GetFieldIndex(pcgTableName,"URNO")];
	if ( olUrno.IsEmpty () )
	{	//  Wenn neuer Datensatz, dann FISU mit 0 vorbesetzen,
		//  ansonsten wird alter wert nicht ge�ndert
		pomDspSrv->Values[ogBCD.GetFieldIndex(pcgTableName,"FISU")] = "0";
		blOk = ogBCD.InsertRecord(pcgTableName,*pomDspSrv,true) ;
		if ( blOk )
		{ 
			pomSEEXChange->OnServiceUrnoChanged ();
			pomPersXChange->OnServiceUrnoChanged ();
			pomSELXChange->OnServiceUrnoChanged ();
		}
	}
	else
		blOk = ogBCD.SetRecord ( pcgTableName, "URNO", olUrno, pomDspSrv->Values, true );
	if ( !blOk )
	{
		CString olErrorTxt = GetString(IDS_WRITE_TABLE_ERR);
		FileErrMsg ( m_hWnd, pCHAR(olErrorTxt), pcgTableName, 
					 "<CServiceCatalogView::SaveAllRecords>", 
					 MB_ICONEXCLAMATION|MB_OK);

		return 1;
	}

	olUrno = pomDspSrv->Values[ogBCD.GetFieldIndex(pcgTableName,"URNO")];
	if ( blOk )
		SaveServiceValidity ( olUrno, m_Vafr, m_Vato );

	
	theApp.EndWaitCursor ();

	if ( !m_bPersonnel )
	{	//  Personal nicht angeklickt, Grids leeren
		if (pomPFCGrid)
			pomPFCGrid->ResetValues ();
		if (pomPERGrid)
			pomPERGrid->ResetValues ();
	}

	if ( !m_bEquipment )
	{	//  Equipment nicht angeklickt, Grids leeren
		if (pomEQTGrid)
			pomEQTGrid->ResetValues ();
		if (pomEQUGrid)
			pomEQUGrid->ResetValues ();
	}
	if ( !m_bLocation )
	{	//  Location nicht angeklickt, Grids leeren
		if (pomLLGGrid)
			pomLLGGrid->ResetValues ();	
		if (pomLGSGrid)
			pomLGSGrid->ResetValues ();	
	}

	pomSEEXChange->Grid2RecordList ();
	pomSEEXChange->SaveRecords ( pCHAR(olUrno) );

//	pomSEFXChange->Grid2RecordList ();
//	pomSEFXChange->SaveRecords ( pCHAR(olUrno) );
	
//	pomSEQXChange->Grid2RecordList ();
//	pomSEQXChange->SaveRecords ( pCHAR(olUrno) );

	pomPersXChange->Data2RecordList (&omServicePersonal);
	pomPersXChange->SaveRecords ( pCHAR(olUrno) );
	
	pomSELXChange->Grid2RecordList ();
	pomSELXChange->SaveRecords ( pCHAR(olUrno) );

	//  gespeicherten Datensatz wieder anzeigen, wg. Creation- und Update-Datum und -User
	if ( !olUrno.IsEmpty() )
	{	//  Datensatz neu einlesen
		//char pclWhere[31];
		RecordSet olRecord;

		//sprintf ( pclWhere, "WHERE URNO='%s'", pCHAR(olUrno) );
		if ( /*!ogBCD.Read ( pcgTableName, pclWhere ) && */
			 ogBCD.GetRecord( pcgTableName, "URNO", olUrno, olRecord ) )
			DisplayService ( olRecord );
	}
	return 0;
}

void CServiceCatalogView::InitializeGridXChanges ()
{
	pomSEEXChange = new CGridRecordXChange( "SEE", "EQNO", "UEQU", "EQCO",
											pomEQUGrid, "ETYP" );
//	pomSEFXChange = new CGridRecordXChange( "SEF", "FCNO", "UPFC", "FCCO",
//											pomPFCGrid );
//	pomSEQXChange = new CGridRecordXChange( "SEQ", "QUNO", "UPER", "QUCO",
//											pomPERGrid );
	pomPersXChange = new CPersonalXChange( pomPFCGrid, pomPERGrid );

	pomSELXChange = new CLocationGridRecordXChange( "SEL", "LONO", "RLOC", 
													"REFT", pomLGSGrid );

		
}

void CServiceCatalogView::ResetGridXChanges ()
{
	pomSEEXChange->ResetService ();
//	pomSEFXChange->ResetService ();
//	pomSEQXChange->ResetService ();
	pomPersXChange->ResetService (); 
	pomSELXChange->ResetService ();
}

void CServiceCatalogView::OnCut() 
{
	// TODO: Add your command handler code here
	SetFocus ( );	//  Damit das event. aktive Grid ein OnEndEditing durchl�uft
	if ( pomDspSrv )
	{
		CString olUrno;
		olUrno = pomDspSrv->Values[ogBCD.GetFieldIndex(pcgTableName,"URNO")];
		if (!olUrno.IsEmpty())
		{
			bool blChangeCode = false;
			CheckReference olCheckReference;
			CString olTxt;
			int ilCount = olCheckReference.Check(_SER,atol(olUrno),"");
			if (ilCount > 0)
			{
				blChangeCode = false;
				if (!olCheckReference.BackDoorEnabled())
				{	/* N entries reference to this record,\nit is not possible to delete this record ! */
					olTxt.Format("%d %s",ilCount, GetString(IDS_STRING311));
					MessageBox(olTxt,GetString(IDS_STRING312),MB_ICONERROR);
				}
				else
				{	/* N entries reference to this record,\ndo you really want to delete this record ? */
					olTxt.Format("%d %s",ilCount, GetString(IDS_STRING313));
					if (IDYES == MessageBox(olTxt,GetString(IDS_STRING312),(MB_ICONQUESTION | MB_YESNOCANCEL | MB_DEFBUTTON2)))
					{
						blChangeCode = true;
					}
				}
			}
			else
			{
				if (ilCount < 0)
				{
					olTxt.Format(GetString(IDS_USED_IN_ARCHIVED), -ilCount );
					if (IDYES == MessageBox( olTxt, GetString(IDS_STRING312),MB_YESNO ) )
					{
						blChangeCode = true;
					}
				}
				else
					if (IDYES == MessageBox(GetString(IDS_DELETE_REALLY),"",MB_YESNO))
					{
						blChangeCode = true;
					}

			}

			if (blChangeCode)
			{
				//  PRF4822: Delete also resources of service
				ResetGridValues ();
				omServicePersonal.Reset();
				pomPersXChange->Data2RecordList (&omServicePersonal);
				pomPersXChange->SaveRecords ( pCHAR(olUrno) );

				pomSEEXChange->Grid2RecordList ();
				pomSEEXChange->SaveRecords ( pCHAR(olUrno) );
	
				pomSELXChange->Grid2RecordList ();
				pomSELXChange->SaveRecords ( pCHAR(olUrno) );
				//  end of PRF4822

				ogBCD.DeleteRecord ( pcgTableName, "URNO", olUrno, true );
				DeleteServiceValidity ( olUrno );
				//  Flags f�r "Modified Record" zur�cksetzen
				bmModified = false;	
				SetGridDirtyFlags ( false );
				SetCCSEditDirtyFlags( false );
				OnNew ();
			}
		}
	}
}
/*
void CServiceCatalogView::OnPaste() 
{
	// TODO: Add your command handler code here
	SetFocus ( );	//  Damit das event. aktive Grid ein OnEndEditing durchl�uft
	if ( pomClipBoard )
	{
		if ( !HandleModifiedService () )
			return;
		DisplayService ( *pomClipBoard );
		SetDefaultValitidy ();		//  default-Werte f�r G�ltigkeit nach Einf�gen setzen
		bmModified = true;
		pomDspSrv->Values[ogBCD.GetFieldIndex(pcgTableName,"URNO")] = "";
	}
}
*/
void CServiceCatalogView::OnCopy() 
{
	// TODO: Add your command handler code here
	if ( !pomDspSrv )
		return;
/*	if ( !pomClipBoard )
		pomClipBoard = new RecordSet ( *pomDspSrv );
	else
		*pomClipBoard = *pomDspSrv;*/
	SetFocus ( );	//  Damit das event. aktive Grid ein OnEndEditing durchl�uft
	if ( !HandleModifiedService () )
		return;
	pomDspSrv->Values[ogBCD.GetFieldIndex(pcgTableName,"URNO")] = "";
	pomDspSrv->Values[ogBCD.GetFieldIndex(pcgTableName,"SECO")] = "";
	m_LkccEdit.SetWindowText("");
	SetDefaultValitidy ();		//  default-Werte f�r G�ltigkeit nach Einf�gen setzen
	bmModified = true;

}

void CServiceCatalogView::OnUpdateCut(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	if (pomDspSrv != NULL)
	{
		char clBdpsStat = '1';
		BOOL blEnable = TRUE;
		
		clBdpsStat = ogPrivList.GetStat("ID_EDIT_CUT") ;
		if ( clBdpsStat != '-' ) 
		{
			if ( /*( clBdpsStat == '-' ) || */( clBdpsStat == '0' ) )
				blEnable = FALSE;
			pCmdUI->Enable ( blEnable );		
		}	
		else
			pCmdUI->ContinueRouting();	// send to main frame !
	}
	else
	{
		pCmdUI->Enable (FALSE);
	}
}

void CServiceCatalogView::OnUpdateCopy(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	if (pomDspSrv != NULL)
	{
		char clBdpsStat = '1';
		BOOL blEnable = TRUE;
		
		clBdpsStat = ogPrivList.GetStat("ID_EDIT_COPY") ;
		if ( clBdpsStat != '-' ) 
		{
			if ( /*( clBdpsStat == '-' ) || */( clBdpsStat == '0' ) )
				blEnable = FALSE;
			pCmdUI->Enable ( blEnable );		
		}	
		else
			pCmdUI->ContinueRouting();	// send to main frame !
	}
	else
	{
		pCmdUI->Enable (FALSE);
	}
}

void CServiceCatalogView::OnUpdateOpen(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	char clBdpsStat = '1';
	BOOL blEnable = TRUE;
		
	clBdpsStat = ogPrivList.GetStat("ID_FILE_OPEN") ;
	if ( clBdpsStat != '-' ) 
	{
		if ( /*( clBdpsStat == '-' ) || */( clBdpsStat == '0' ) )
			blEnable = FALSE;
		pCmdUI->Enable ( blEnable );		
	}	
	else
		pCmdUI->ContinueRouting();	// send to main frame !
}

void CServiceCatalogView::OnUpdateSave(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	char clBdpsStat = '1';
	BOOL blEnable = TRUE;
		
	clBdpsStat = ogPrivList.GetStat("ID_FILE_SAVE") ;
	if ( clBdpsStat != '-' ) 
	{
		if ( /*( clBdpsStat == '-' ) || */( clBdpsStat == '0' ) )
			blEnable = FALSE;
		pCmdUI->Enable ( blEnable );		
	}	
	else
		pCmdUI->ContinueRouting();	// send to main frame !
}

void CServiceCatalogView::OnUpdateNew(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	char clBdpsStat = '1';
	BOOL blEnable = TRUE;
		
	clBdpsStat = ogPrivList.GetStat("ID_FILE_NEW") ;
	if ( clBdpsStat != '-' ) 
	{
		if ( /*( clBdpsStat == '-' ) || */( clBdpsStat == '0' ) )
			blEnable = FALSE;
		pCmdUI->Enable ( blEnable );		
	}	
	else
		pCmdUI->ContinueRouting();	// send to main frame !
}

void CServiceCatalogView::OnUpdateValidity(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	char clBdpsStat = '1';
	BOOL blEnable = TRUE;
		
	clBdpsStat = ogPrivList.GetStat("ID_VALIDITY") ;
	if ( clBdpsStat != '-' ) 
	{
		if ( /*( clBdpsStat == '-' ) || */( clBdpsStat == '0' ) )
			blEnable = FALSE;
		pCmdUI->Enable ( blEnable );		
	}	
	else
		pCmdUI->ContinueRouting();	// send to main frame !
}

void CServiceCatalogView::OnUpdatePrint(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	char clBdpsStat = '1';
	BOOL blEnable = TRUE;
		
	clBdpsStat = ogPrivList.GetStat("ID_FILE_PRINT") ;
	if ( clBdpsStat != '-' ) 
	{
		if ( /*( clBdpsStat == '-' ) || */( clBdpsStat == '0' ) )
			blEnable = FALSE;
		pCmdUI->Enable ( blEnable );		
	}	
	else
		pCmdUI->ContinueRouting();	// send to main frame !
}

void CServiceCatalogView::OnUpdatePrintPreview(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	char clBdpsStat = '1';
	BOOL blEnable = TRUE;
		
	clBdpsStat = ogPrivList.GetStat("ID_FILE_PRINT") ;
	if ( clBdpsStat != '-' ) 
	{
		if ( /*( clBdpsStat == '-' ) || */( clBdpsStat == '0' ) )
			blEnable = FALSE;
		pCmdUI->Enable ( blEnable );		
	}	
	else
		pCmdUI->ContinueRouting();	// send to main frame !
}

/*
void CServiceCatalogView::OnUpdatePaste(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	pCmdUI->Enable ( pomClipBoard!=0 )	;
}
*/
void CServiceCatalogView::SetDocTitle ()
{
	CString olTitle;

	
	if ( pomDspSrv )
		olTitle = pomDspSrv->Values[ogBCD.GetFieldIndex(pcgTableName,"SECO")];
	if ( olTitle.IsEmpty () )
		olTitle = GetString ( IDS_UNKNOWN );
	m_pDocument->SetTitle ( pCHAR(olTitle) );
}

void CServiceCatalogView::IniStatics ()
{
	SetDlgText ( IDC_LKBZ_N, IDS_INBOUND ) ;
	SetDlgText ( IDC_LKBZ_F, IDS_OUTBOUND ) ;
	SetDlgText ( IDC_LKBZ_T, IDS_TURNAROUND ) ;
	SetDlgText ( IDC_FSET_DEFAULT, IDS_DEFAULT_VALUES ) ;
	SetDlgText ( IDC_FSET_FIXED, IDS_FIXED_VALUES ) ;
	SetDlgText ( IDC_FDUT, IDS_DUTY_TIME ) ;
	SetDlgText ( IDC_FSUT, IDS_SETUP_TIME ) ;
	SetDlgText ( IDC_FSDT, IDS_SETDOWN_TIME ) ;
	SetDlgText ( IDC_FWTT, IDS_WAY_TO ) ;
	SetDlgText ( IDC_RTWT_FUSS, IDS_ON_FOOT ) ;
	SetDlgText ( IDC_RTWT_FAHRT, IDS_MOTORISED ) ;
	SetDlgText ( IDC_RTWT_SCHLEPP, IDS_TOW ) ;
	SetDlgText ( IDC_FWTF, IDS_WAY_FROM ) ;
	SetDlgText ( IDC_RTWF_FUSS, IDS_ON_FOOT ) ;
	SetDlgText ( IDC_RTWF_FAHRT, IDS_MOTORISED ) ;
	SetDlgText ( IDC_RTWF_SCHLEPP, IDS_TOW ) ;
	SetDlgText ( IDC_CHECK_PERSONNEL, IDS_PERSONAL ) ;
	SetDlgText ( IDC_CHECK_EQUIPMENT, IDS_EQUIPMENT ) ;
	SetDlgText ( IDC_CHECK_LOCATION, IDS_LGS_HEADER ) ;
	//SetDlgText ( IDC_CDAT_TXT, IDS_CREATED_ON ) ;
	//SetDlgText ( IDC_LSTU_TXT, IDS_CHANGED_ON ) ;
	SetDlgText ( IDC_SNAM_TXT, IDS_NAME ) ;
	SetDlgText ( IDC_SSNM, IDS_KURZNAME ) ;
	SetDlgText ( IDC_SEIV_TXT, IDS_INVOICING ) ;
	SetDlgText ( IDC_FRMT_TXT, IDS_FRMT ) ;
	SetDlgText ( IDC_PRMT_TXT, IDS_PRMT ) ;
	SetDlgText ( IDC_DTYP_TXT, IDS_DTYP ) ;
	//SetDlgText ( IDC_USEC_TXT, IDS_CREATED_BY ) ;
	//SetDlgText ( IDC_USEU_TXT, IDS_CHANGED_BY ) ;
	SetDlgText ( IDC_SSAP_TXT, IDS_ORDER_NO ) ;
	SetDlgText ( IDC_SEAN_TXT, IDS_ANNEX ) ;
	SetDlgText ( IDC_SEHC_TXT, IDS_PREIS ) ;
	SetDlgText ( IDC_SECO_TXT, IDS_CODE ) ;
	SetDlgText ( IDC_DAUER_TXT, IDS_DAUER ) ;
	SetDlgText ( IDC_SEER_GROUP, IDS_DEFAULT_REFERENCE ) ;
	SetDlgText ( IDC_ACCOUNTS_GROUP, IDS_ACCOUNTS ) ;
	//SetDlgText ( IDC_VAFR_TXT, IDS_VALID_FROM ) ;
	//SetDlgText ( IDC_VATO_TXT, IDS_VALID_TO	) ;
	SetDlgText ( IDC_FSET_TXT, IDS_FSET_REMARK ) ;
	SetDlgText ( IDC_WTYP_TXT, IDS_ROUTE_TYPE ) ;
	SetDlgText ( IDC_FFIS, IDS_FLIGHTINDIPENDENT );


}
	
void CServiceCatalogView::PrintGrid ( CGXGridWnd *popGridToPrint )
{
	pomGridToPrint = popGridToPrint;
	CFormView::OnFilePrint ();
	pomGridToPrint = 0;

}


void CServiceCatalogView::OnFilePrint()
{	//  Drucken der Serviceliste von au�erhalb der Service-Auswahl 
	if ( !pomHiddenList )
	{		
		pomHiddenList = new CServiceList ( this ) ;
		pomHiddenList->DoModeless ( pcgTableName, 0 ) ;
		pomHiddenList->ShowWindow ( SW_HIDE );
		pomGridToPrint = pomHiddenList->GetServiceList ();
	}
	CFormView::OnFilePrint ();
}


void CServiceCatalogView::OnFilePrintPreview()
{	//  Preview der Serviceliste von au�erhalb der Service-Auswahl 
	if ( ( pomHiddenList = new CServiceList ( this ) )&&
		 pomHiddenList->DoModeless ( pcgTableName, 0 ) )
	{		
		pomHiddenList->ShowWindow ( SW_HIDE );
		pomGridToPrint = pomHiddenList->GetServiceList ();
		CFormView::OnFilePrintPreview ();
	}
}

void CServiceCatalogView::SetGridDirtyFlags ( bool dirty/*=true*/ )
{
	if (pomPFCGrid)
		pomPFCGrid->SetDirtyFlag ( dirty ); 
	if (pomPERGrid)
		pomPERGrid->SetDirtyFlag ( dirty ); 
	if (pomEQUGrid)
		pomEQUGrid->SetDirtyFlag ( dirty ); 
	if (pomLGSGrid)
		pomLGSGrid->SetDirtyFlag ( dirty ); 
}

int CServiceCatalogView::GetGridDirtyFlags ()
{
	int ilDirtyCount = 0;
	if (pomPFCGrid && pomPFCGrid->IsGridDirty() )
		ilDirtyCount++;
	if (pomPERGrid && pomPERGrid->IsGridDirty() )
		ilDirtyCount++;
	if (pomEQUGrid && pomEQUGrid->IsGridDirty() )
		ilDirtyCount++;
	if (pomLGSGrid && pomLGSGrid->IsGridDirty() )
		ilDirtyCount++;
	return ilDirtyCount;
}

// return true:   Datensatz mu�te nicht gesichert werden oder ist gesichert worden
//		  false:  Datensatz konnte nicht gesichert werden		
bool CServiceCatalogView::HandleModifiedService ()
{
	int ilRet=IDNO;
	if ( !bmModified )
		bmModified = ( GetCCSEditDirtyFlags() > 0 );
	if ( !bmModified )
		bmModified = ( GetGridDirtyFlags() > 0 );
	if ( bmModified )
		ilRet = LangMsgBox ( m_hWnd, IDS_ASK_FOR_SAVE, AFX_IDS_APP_TITLE,
							 MB_YESNO | MB_ICONQUESTION | MB_DEFBUTTON1 );
	if ( ilRet == IDYES )
		return DoSave ();
	else
		return true;

}		
/*
void CServiceCatalogView::OnDateTimeChange( UINT id, NMHDR * pNotifyStruct, LRESULT * result )
{
	bmModified = true;
}
*/
void CServiceCatalogView::OnChangeEdit() 
{
	// TODO: Add your control notification handler code here
	bmModified = true;
}

void CServiceCatalogView::OnClickedButton() 
{
	// TODO: Add your control notification handler code here
	bmModified = true;
	UpdateData ();
	EnableControls ();
}

void CServiceCatalogView::OnClickedTimeFlag()
{
	OnClickedButton() ;
	//  move Focus to input control
	CWnd* pWnd = GetFocus( );
	if ( pWnd )
	{
		if ( ((CButton*)pWnd)->GetCheck () )
			SendMessage ( WM_NEXTDLGCTL, 0, 0L );
	}
}

int CServiceCatalogView::GetCCSEditDirtyFlags()
{
	int ilDirtyCount = 0;
	if ( m_LkccEdit.GetModify () )
		ilDirtyCount ++;
	if ( m_LknmEdit.GetModify () )
		ilDirtyCount ++;
	if ( m_LkhcEdit.GetModify () )
		ilDirtyCount ++;
	if ( m_SdutEdit.GetModify () )
		ilDirtyCount ++;
	if ( m_SsutEdit.GetModify () )
		ilDirtyCount ++;
	if ( m_SsdtEdit.GetModify () )
		ilDirtyCount ++;
	if ( m_SwtfEdit.GetModify () )
		ilDirtyCount ++;
	if ( m_SwttEdit.GetModify () )
		ilDirtyCount ++;
	return ilDirtyCount;
}

void CServiceCatalogView::SetCCSEditDirtyFlags ( bool dirty/*=true*/ )
{
	m_LkccEdit.SetModify( dirty );
	m_LknmEdit.SetModify( dirty );
	m_LkhcEdit.SetModify( dirty );
	m_SdutEdit.SetModify( dirty );
	m_SsutEdit.SetModify( dirty );
	m_SsdtEdit.SetModify( dirty );
	m_SwtfEdit.SetModify( dirty );
	m_SwttEdit.SetModify( dirty );
}


void CServiceCatalogView::EnableControls ()
{
	if ( m_Ffis )	//  Wenn flugunabh�ngig Typ des fluges uninteressant
	{
		m_Lkbz = -1;
		CheckDlgButton ( IDC_LKBZ_N, 0);
		CheckDlgButton ( IDC_LKBZ_F, 0);
		CheckDlgButton ( IDC_LKBZ_T, 0);
	}
	else
		if ( m_Lkbz <0 )
		{	//  keinen undefinierten Status �ber Typ des Services zulassen
			m_Lkbz = 2;
			CheckDlgButton ( IDC_LKBZ_T, 1);
		}
	EnableDlgItem ( this, IDC_LKBZ_N, !m_Ffis );
	EnableDlgItem ( this, IDC_LKBZ_F, !m_Ffis );
	EnableDlgItem ( this, IDC_LKBZ_T, !m_Ffis );
	//  Wenn Durchgangsleistung, Checkbox f�r Durchf�hrung ausschalten und disablen
	if ( m_Lkbz == 2 )
	{
		EnableDlgItem ( this, IDC_FDUT );
		CheckDlgButton( IDC_FDUT, 0 );
		m_Fdut = FALSE;
	}
	if ( !m_Fwtf )
	{
		CheckDlgButton ( IDC_RTWF_FUSS, 0);
		CheckDlgButton ( IDC_RTWF_FAHRT, 0);
		CheckDlgButton ( IDC_RTWF_SCHLEPP, 0);
		m_Rtwf = -1;
	}		
	if ( !m_Fwtt )
	{
		CheckDlgButton ( IDC_RTWT_FUSS, 0 );
		CheckDlgButton ( IDC_RTWT_FAHRT, 0);
		CheckDlgButton ( IDC_RTWT_SCHLEPP, 0);
		m_Rtwt = -1;
	}		
	EnableDlgItem ( this, IDC_FDUT, (m_Lkbz!= 2) );
	EnableDlgItem ( this, IDC_FSET_FIXED, m_Fset );
	m_SdutEdit.EnableWindow(m_Fdut);
	m_SsutEdit.EnableWindow(m_Fsut);
	m_SsdtEdit.EnableWindow(m_Fsdt);
	m_SwtfEdit.EnableWindow(m_Fwtf);
	m_SwttEdit.EnableWindow(m_Fwtt);

	if ( ogPrivList.GetStat("IDC_WTYP_TXT") == '1' ) 
	{
		EnableDlgItem ( this, IDC_RTWF_FUSS, m_Fwtf);
		EnableDlgItem ( this, IDC_RTWF_FAHRT, m_Fwtf);
		EnableDlgItem ( this, IDC_RTWF_SCHLEPP, m_Fwtf);
		EnableDlgItem ( this, IDC_RTWT_FUSS, m_Fwtt );
		EnableDlgItem ( this, IDC_RTWT_FAHRT, m_Fwtt );
		EnableDlgItem ( this, IDC_RTWT_SCHLEPP, m_Fwtt );
	}
}	




void CServiceCatalogView::OnValidity() 
{
	// TODO: Add your command handler code here
	CValidityDlg  olDlg;
	//olDlg.SetVaidity ( m_VafrDate, m_VafrTime, m_VatoDate, m_VatoTime );
	olDlg.SetValidity ( m_Vafr, m_Vato );
	if ( olDlg.DoModal () == IDOK )
	{
		//olDlg.GetVaidity ( m_VafrDate, m_VafrTime, m_VatoDate, m_VatoTime );
		olDlg.GetValidity ( m_Vafr, m_Vato );
		bmModified = true;
		UpdateData ( FALSE );
	}
}
 
void CServiceCatalogView::SetDefaultValitidy ()
{
	/*m_VafrDate = COleDateTime::GetCurrentTime();
	m_VafrTime = COleDateTime::GetCurrentTime();
	m_VatoDate = COleDateTime::GetCurrentTime();
	int ilYear = m_VatoDate.GetYear() ;
	int ilMonth = m_VatoDate.GetMonth() ;
	int ilDay = m_VatoDate.GetDay() ;         
	m_VatoDate.SetDate ( ilYear+10, ilMonth, ilDay );

	m_VatoTime = m_VatoDate;
	m_VatoTime.SetTime(23, 59, 59);*/
	COleDateTime olOleVafr;
	
	olOleVafr = COleDateTime::GetCurrentTime();
	m_Vafr = COneOleDateTimeToDBString( olOleVafr );
	m_Vato.Empty();;
}


void CServiceCatalogView::OnCedaTablesLoaded ()
{
	InitializeGrids ();
	InitializeGridXChanges ();

	CServiceCatalogDoc *polDoc = GetDocument();
	if ( polDoc )
	{ 
		CString	olBCString;

		for ( int i=0; i<polDoc->omBCTables.GetSize(); i++ )
		{	
			olBCString = polDoc->omBCTables[i] ;
			if ( i % 3 == 0 )
				olBCString += " New";
			else
				if ( i % 3 == URT_OFFSET )
					olBCString += " Change";
				else
					olBCString += " Delete";

			ogDdx.Register(this, i, CString("CServiceCatalogView"), olBCString, ProcessBC );
		}
	}

}

void CServiceCatalogView::SetBdpsState()
{
	char	clStat;
	CWnd	*pWnd;
	int ilAccountsIDs[] = {IDC_ACCOUNTS_GROUP, IDC_SEIV_TXT, IDC_LKAR, 
						   IDC_SSAP_TXT, IDC_ATRN, IDC_SEHC_TXT, IDC_LKHC };
	int ilRouteTypeIDs[] = {IDC_WTYP_TXT, IDC_RTWT_FUSS, IDC_RTWT_FAHRT, 
						    IDC_RTWT_SCHLEPP, IDC_RTWF_FUSS, IDC_RTWF_FAHRT, IDC_RTWF_SCHLEPP };
	clStat = ogPrivList.GetStat("IDC_CHECK_PERSONNEL");
	SetWndStatAll(clStat, omCheckBoxPersonell);
	clStat = ogPrivList.GetStat("IDC_CHECK_EQUIPMENT");
	// if table EQTTAB does not exist, disable Checkbox "Equipment"
	if ( (clStat=='1') && !DoesTableExist("EQT") )
		clStat = '0';
	SetWndStatAll(clStat, omCheckBoxEquip);
	clStat = ogPrivList.GetStat("IDC_CHECK_LOCATION");
	SetWndStatAll(clStat, omCheckBoxLocation);
	// Control Acconunt Group and Route types by BDPSSEC
	clStat = ogPrivList.GetStat("IDC_ACCOUNTS_GROUP");
	for ( int i=0; i<sizeof(ilAccountsIDs)/sizeof(int); i++ )
	{
		pWnd = GetDlgItem ( ilAccountsIDs[i] );
		if ( pWnd )
			SetWndStatAll(clStat, (*pWnd) );
	}
	clStat = ogPrivList.GetStat("IDC_WTYP_TXT");
	for ( i=0; i<sizeof(ilRouteTypeIDs)/sizeof(int); i++ )
	{
		pWnd = GetDlgItem ( ilRouteTypeIDs[i] );
		if ( pWnd )
			SetWndStatAll(clStat, (*pWnd) );
	}

}

LONG CServiceCatalogView::OnBcAdd(UINT wParam, LONG /*lParam*/)
{
	ogBcHandle.GetBc(wParam);
	return TRUE;
}

void CServiceCatalogView::UpdateChoiceLists ( CString opTable, int ipBCType, 
											 RecordSet* popData, CString opResTable ) 
{
	int		ilEqtIdx;
	CString	olEquType;

	if ( opResTable == "EQU" )
	{
		if ( opTable == "EQU" )
			ilEqtIdx = ogBCD.GetFieldIndex("EQU", "GKEY");
		if ( opTable == "SGR" )
			ilEqtIdx = ogBCD.GetFieldIndex("SGR", "STYP");
		if ( ilEqtIdx > 0 )
			olEquType = popData->Values[ilEqtIdx];
	}

	pomPFCGrid->ModifyChoiceLists ( opResTable, olEquType );
	pomPERGrid->ModifyChoiceLists ( opResTable, olEquType );

	pomEQUGrid->ModifyChoiceLists ( opResTable, olEquType );
	pomLGSGrid->ModifyChoiceLists ( opResTable, olEquType );
	
	if ( opTable=="EQT")
	{
		pomEQTGrid->IniResourcesList ( EQUIPMENT );
		pomEQTGrid->ModifyChoiceLists ();
	}
	
}

static void ProcessBC( void *popInstance, int ipDDXType, 
							  void *vpDataPointer, CString &ropInstanceName)
{
    CServiceCatalogView	*polView = (CServiceCatalogView *)popInstance;
	CServiceCatalogDoc	*polDoc=0;
	CString				olTable, olType="IRT", olResTable;
	int					ilType;

	if ( polView )
		polDoc = polView->GetDocument();

	if ( polDoc && ( ipDDXType < polDoc->omBCTables.GetSize() ) )
	{
		olTable = polDoc->omBCTables[ipDDXType];
		ilType = ipDDXType % 3;
		if ( ilType == DRT_OFFSET )
			olType = "DRT";
		if ( ilType == URT_OFFSET )
			olType = "URT";
		TRACE ( "BC received Table <%s> Command <%s>\n", olTable, olType );
		//  Update-BC only interesting if SGR and NESTED_GROUPS allowed
		if ( (ilType != URT_OFFSET) ||
			 ( (olTable == "SGR") && NESTED_GROUPS) )
		{
			ogBCD.Sort(olTable);
			if ( polDoc->UpdateChoiceLists ( olTable, ilType, (RecordSet*)vpDataPointer, olResTable ) )
				polView->UpdateChoiceLists ( olTable, ilType, (RecordSet*)vpDataPointer, olResTable );
		}
	}
	else
		TRACE ( "Received unregistered BC <%d>\n", ipDDXType );
}


