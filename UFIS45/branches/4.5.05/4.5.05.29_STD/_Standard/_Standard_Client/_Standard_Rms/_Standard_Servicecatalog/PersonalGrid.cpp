// PersonalGrid.cpp: implementation of the CPersonalGrid class.
//
//////////////////////////////////////////////////////////////////////

#include <stdafx.h>
#include <Resource.h>
#include <ServiceCatalogView.h>
#include <PersonalGrid.h>

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//  Spezielle Service-Grids, die nicht im Grid die Daten halten,
//  sondern alle �nderungen sofort in CPersonellData eintragen

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CPersonalGrid::CPersonalGrid ( CServiceCatalogView *popView, UINT nID, 
							   char * pcpTableName, CServiceCatalogDoc *popDoc )
	:CServiceGrid( popView, nID, pcpTableName, popDoc )
{
	pomData = &(popView->omServicePersonal);
	bmExpanded = false;
}

CPersonalGrid::~CPersonalGrid()
{

}

PERSONALRECORD*	CPersonalGrid::GetAssocPtr ( ROWCOL ipRow )
{
	PERSONALRECORD	*prlRet=0;
	if ( ipRow < 2 )
		return 0;
	CGXStyle olStyle;
	if ( GetStyleRowCol( ipRow, 1, olStyle ) && 
		 olStyle.GetIncludeItemDataPtr() )
		prlRet = (PERSONALRECORD*)olStyle.GetItemDataPtr();
	return prlRet;
}

void CPersonalGrid::OnChangedMode ()
{
//	SetStyleRange ( CGXRange(2,3,GetRowCount(), GetColCount() ), 
//					CGXStyle().SetEnabled (!bmExpanded).SetReadOnly (bmExpanded)  ); 
	int ilWidth1 = 90, ilWidth23 =50, ilWidthtotal;
	HideCols ( 2, 3, bmExpanded );
	
	ilWidth1 += ilWidth23; /* PRF3710 */
	ilWidthtotal = ilWidth1 + 2*ilWidth23;
	SetColWidth(1, 1, bmExpanded ? ilWidthtotal : ilWidth1 );
	SetColWidth(2, 3, bmExpanded ? 0 : ilWidth23 );
	HideCols ( 3,3);	/* PRF3710 */
	GetParam()->EnableTrackColWidth(!bmExpanded);
}

void CPersonalGrid::ResetValues ()
{
	CServiceGrid::ResetValues ();
	COLORREF ilBkColor;
	if ( GetAssocPtr ( 2 ) )
		SetStyleRange ( CGXRange(2,1), CGXStyle().SetItemDataPtr( 0 ) );
	if ( LookupStyleRowCol(2,1).GetEnabled() )
		ilBkColor = WHITE;
	else
		ilBkColor = SILVER;
	SetStyleRange ( CGXRange(2,1), 
					CGXStyle().SetIncludeItemDataPtr( FALSE ).
							   SetUserAttribute(GX_IDS_UA_TOOLTIPTEXT, "" ).
							   SetInterior(ilBkColor) );
}

BOOL CPersonalGrid::OnRButtonDblClkRowCol(ROWCOL nRow, ROWCOL nCol, UINT nFlags, CPoint pt)
{/*	zum Testen
	if ( nRow>=2 )
	{
		PERSONALRECORD *prlRet=0;
		CString cStr;
		prlRet = GetAssocPtr ( nRow );
		if ( prlRet )
		{
			cStr.Format ( "Data Pointer included: %s", prlRet->code ); 
			MessageBox ( cStr );
		}
		return TRUE;
	}
	else*/
		return CServiceGrid::OnRButtonDblClkRowCol(nRow, nCol, nFlags, pt);
}


//////////////////////////////////////////////////////////////////////
// CFunctionsGrid Class
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CFunctionsGrid::CFunctionsGrid( CServiceCatalogView *popView, UINT nID, 
							    CServiceCatalogDoc *popDoc, 
								CQualificationsGrid *popQualiGrid )
	:CPersonalGrid ( popView, nID, "PFC", popDoc )
{
	pomQualiGrid = popQualiGrid;
	imLastCurrentRow=0;
/*
	SetStyleRange(CGXRange(0,0), 
				  CGXStyle().SetControl(GX_IDS_CTRL_HEADER)  );
	ok = SetCoveredCellsRowCol( 0, 1, 0, nCols );


	SetStyleRange(CGXRange( 3, 2 ), CGXStyle( )   .SetControl( IDS_CTRL_BITMAP ));
*/
	SetCoveredCellsRowCol( 0, 0, 0, 0 );
	SetCoveredCellsRowCol( 0, 1, 0, GetColCount() );
	SetStyleRange ( CGXRange(0,0), 
					CGXStyle().SetValue ( bmExpanded ? "#BMP(138)" : "#BMP(137)" )
							  .SetVerticalAlignment(DT_VCENTER )
							  .SetHorizontalAlignment(DT_CENTER) );
	CString olTitle ;

	olTitle = GetString ( bmExpanded ? IDS_SINGLE_PFC_HEADER : IDS_PFC_HEADER );
	SetValueRange ( CGXRange(0,1), olTitle );

}

CFunctionsGrid::~CFunctionsGrid()
{

}

void CFunctionsGrid::UpdateDisplay ()
{
	PERSONALRECORD  *prlPersRec;
	int				ilDisplayed=0;
	int				ilCount;
	CString			olCount;
	CStringList		olAlreadyDisplayed;

	ResetValues ();
	if ( !pomData )
		return;
	for ( int i=0; i<pomData->omFuncs.GetSize(); i++ )
	{
		prlPersRec = &(pomData->omFuncs[i]);
		//  bei Sammeldarstellung interessiert nur der 1.Datensatz einer Funktion
		if ( !prlPersRec ||
			 ( !bmExpanded && ( olAlreadyDisplayed.Find(prlPersRec->code) >0 ) ) 
			)
			continue;
		if ( !bmExpanded )
			ilCount = pomData->GetPersonalCount ( TYP_FUNC, prlPersRec->code );
		else
			ilCount = 1;
		olCount.Format ("%d", ilCount );
		SetStyleRange ( CGXRange(ilDisplayed+2,1), 
							CGXStyle().SetItemDataPtr( (void*)prlPersRec ) );
		if ( SetLineValues ( ilDisplayed, (CString&)"", prlPersRec->code,
							 olCount, prlPersRec->prio ) )
		{
			ilDisplayed++;
			olAlreadyDisplayed.AddTail ( prlPersRec->code );
		}
		else
			SetStyleRange ( CGXRange(ilDisplayed+2,1), 
						    CGXStyle().SetIncludeItemDataPtr( FALSE ) );
		
	}
	SetCurrentCell (2,1);
	OnChangedMode ();
	UpdateQualiGrid ();
}


BOOL CFunctionsGrid::OnLButtonDblClkRowCol(ROWCOL nRow, ROWCOL nCol, 
										   UINT nFlags, CPoint pt)
{
	if ( nRow == 0 )
	{
		//CGXGridWnd::OnLButtonDblClkRowCol(nRow, nCol, nFlags, pt);
		bmExpanded = !bmExpanded;
		pomQualiGrid->SetExpanded ( bmExpanded );
		UpdateDisplay ();
		//SetCurrentCell (2,1);
		OnChangedMode ();
		//UpdateQualiGrid ();
		return TRUE;
	}
	else
		return CPersonalGrid::OnLButtonDblClkRowCol(nRow, nCol, nFlags, pt);

}


void CFunctionsGrid::OnInitCurrentCell(ROWCOL nRow, ROWCOL nCol)
{
	ROWCOL ilCurRow, ilCurCol;
	BOOL blOk = GetCurrentCell(ilCurRow, ilCurCol );
	
	if ( ( nCol == 0 ) && ( nRow >= 2 ) )
	{
		if ( LookupStyleRowCol(nRow,1).GetEnabled() )
			SetCurrentCell ( nRow,1 );
		return;
	}
	blOk = GetCurrentCell(ilCurRow, ilCurCol );

	CServiceGrid::OnInitCurrentCell ( nRow, nCol );
	blOk = GetCurrentCell(ilCurRow, ilCurCol );
	SelectRange( CGXRange ().SetCols(0), FALSE );
	if ( bmExpanded && (nRow>=2) )
		SelectRange( CGXRange(nRow,0) );
	blOk = GetCurrentCell(ilCurRow, ilCurCol );
	if ( nRow == imLastCurrentRow )
		return;		//  kein Wechsel der aktiven Zeile
	imLastCurrentRow = nRow;
	if ( !bmExpanded )
		return;
	UpdateQualiGrid ();
	blOk = GetCurrentCell(ilCurRow, ilCurCol );
}

void CFunctionsGrid::UpdateQualiGrid ()
{
	PERSONALRECORD *prpSingleFunc=0;
	
	CString olWert = GetValueRowCol ( imLastCurrentRow , 1 );
	if ( !olWert.IsEmpty() )
		prpSingleFunc = GetAssocPtr (imLastCurrentRow);
	pomQualiGrid->UpdateDisplay ( prpSingleFunc );

}

void CFunctionsGrid::OnChangedMode ()
{
	CPersonalGrid::OnChangedMode ();
	CString olTitle ;

	olTitle = GetString ( bmExpanded ? IDS_SINGLE_PFC_HEADER : IDS_PFC_HEADER );
	SetStyleRange ( CGXRange(0,0), 
					CGXStyle().SetValue ( bmExpanded ? "#BMP(138)" : "#BMP(137)" )
							  .SetVerticalAlignment(DT_VCENTER )
							  .SetHorizontalAlignment(DT_CENTER) );
	SetValueRange ( CGXRange(0,1), olTitle );
}

bool CFunctionsGrid::IsResourceAllReadySelected ( CString &ropResource, 
											    CString &ropTableName,
												ROWCOL ipDontTest, 
												bool bpErrMsg/*=true*/ )
{
	if ( bmExpanded )
		return false;
	else
		return CServiceGrid::IsResourceAllReadySelected ( ropResource, 
										ropTableName, ipDontTest, bpErrMsg );
}

BOOL CFunctionsGrid::OnEndEditing( ROWCOL nRow, ROWCOL nCol) 
{
	CString olOldCode, olOldPrio;
	UINT	ilOldAmount=0, ilNewAmount;
	bool	blOk=true, blRowDeleted =false;

	PERSONALRECORD*	prlDataRec = GetAssocPtr ( nRow );
	if ( prlDataRec )
	{
		olOldCode = prlDataRec->code;
		olOldPrio = prlDataRec->prio;
		ilOldAmount = pomData->GetPersonalCount ( TYP_FUNC, olOldCode );
	}

	BOOL ilRet = CTitleGridControl::OnEndEditing( nRow, nCol) ;

	CString olWert = GetValueRowCol ( nRow, nCol );
	if (nRow>=2 )
	{
		SetDefaultValues ( nRow );
		DisplayInfo ( nRow, nCol );
		if ( DeleteEmptyRows ( nRow, nCol ) )
			blRowDeleted = true;		//  Resource wurde gel�scht
	}	
	if ( nRow <2 )
		return ilRet;


	switch ( nCol )
	{
		case 1:
			if (olWert==olOldCode )
				break;	//  gleiche Resource wie vorher fertig
			if ( !olOldCode.IsEmpty() )
			{	//  ge�nderte Resource
				if ( bmExpanded )
					blOk = pomData->DeletePersonal ( TYP_FUNC, prlDataRec );
				else
					blOk = pomData->DeleteAllPersonal ( TYP_FUNC, olOldCode );
			}
			if ( !olWert.IsEmpty() )
			{
				ilNewAmount = 1;
				if ( !bmExpanded && !prlDataRec )		
				{	//  ggf. zuvor gesetzte Anzahl u. Prio auslesen
					CString olStr;
					olStr = GetValueRowCol ( nRow, 2 );
					if ( olStr.IsEmpty() ||
						 (sscanf(olStr, "%d", &ilNewAmount ) <= 0 ) )
						 ilNewAmount = 1;
					olStr = GetValueRowCol ( nRow, 3 );
					if ( !olStr.IsEmpty() )
						olOldPrio = olStr;
				}
				pomData->AddPersonal ( TYP_FUNC, olWert, CString(""), 
									   olOldPrio, ilNewAmount );
				prlDataRec = &(pomData->omFuncs[pomData->omFuncs.GetSize()-1]);
			}
			else
			{
				if ( blRowDeleted )
				{
					UpdateQualiGrid ();
					return ilRet&&blOk;
				}
				prlDataRec = 0;
			}
			SetStyleRange ( CGXRange(nRow,1), 
							CGXStyle().SetItemDataPtr( (void*)prlDataRec ) );
			UpdateQualiGrid ();
			break;
		case 2:
			if ( bmExpanded || !prlDataRec )
				break;
			if ( sscanf ( olWert, "%u", &ilNewAmount ) <= 0 )
				break;
			if ( ilOldAmount != ilNewAmount )
				blOk = pomData->ChangePersonalCount ( TYP_FUNC, olOldCode, 
													  olOldPrio, ilNewAmount );
			break;
		case 3:
			if ( !bmExpanded && prlDataRec && ( olWert!= olOldPrio ) )
				blOk = pomData->ChangePersonalPrio ( TYP_FUNC, olOldCode, 
													 olWert );
	}	
	return ilRet&&blOk;
}

bool CFunctionsGrid::SortTable ( ROWCOL ipRowClicked, ROWCOL ipColClicked )
{
	bool blRet = CPersonalGrid::SortTable( ipRowClicked, ipColClicked );
	if ( blRet )
		UpdateQualiGrid ();
	return blRet;
}

bool CFunctionsGrid::DoppelteResourcen ()
{
	//  suche doppelt vergebene Resourcen
	if ( !bmExpanded )
		return CPersonalGrid::DoppelteResourcen();
	else
		return false;
}

//////////////////////////////////////////////////////////////////////
// CQualificationsGrid Class
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CQualificationsGrid::CQualificationsGrid(CServiceCatalogView *popView, UINT nID, 
										 CServiceCatalogDoc *popDoc )
	:CPersonalGrid ( popView, nID, "PER", popDoc )
{
	 prmDisplayedFunc = 0;
}

CQualificationsGrid::~CQualificationsGrid()
{

}
void CQualificationsGrid::UpdateDisplay ( PERSONALRECORD *prpSingleFunc/*=0*/ )

{
	ASSIGNRECORD	*prlAssign;
	PERSONALRECORD  *prlQualRec;
	int				ilDisplayed=0;
	int				ilCount;
	CString			olCount;
	CStringList		olAlreadyDisplayed;

	ResetValues ();
	if ( !pomData )
		return;
	TRACE ("UpdateDisplay %s\n", prpSingleFunc ? "Einzelqualif." : "Qualifikation" );
	prmDisplayedFunc = bmExpanded ? prpSingleFunc : 0;
	if ( !bmExpanded )
		for ( int i=0; i<pomData->omQualis.GetSize(); i++ )
		{
			prlQualRec = &(pomData->omQualis[i]);
			//  bei Sammeldarstellung interessiert nur der 1.Datensatz einer Funktion
			if ( !prlQualRec || ( olAlreadyDisplayed.Find(prlQualRec->code) >0 ) ) 
				continue;
			ilCount = pomData->GetPersonalCount ( TYP_QUALI, prlQualRec->code );
			olCount.Format ("%d", ilCount );
			SetStyleRange ( CGXRange(ilDisplayed+2,1), 
								CGXStyle().SetItemDataPtr( (void*)prlQualRec ) );
			if ( SetLineValues ( ilDisplayed, (CString&)"", prlQualRec->code, 
								 olCount, prlQualRec->prio ) )
			{
				ilDisplayed++;
				olAlreadyDisplayed.AddTail(prlQualRec->code);
			}
			else
				SetStyleRange ( CGXRange(ilDisplayed+2,1), 
								CGXStyle().SetIncludeItemDataPtr( FALSE ) );
		}
	else
	{
		olCount = "1";
		if ( prpSingleFunc )
			for ( int i=0; i<pomData->omAssigns.GetSize(); i++ )
			{
				prlAssign = &(pomData->omAssigns[i]);
				if ( !prlAssign || !prlAssign->pQual  ||
					 (prlAssign->pFunc!=prpSingleFunc)  ) 
					continue;
				prlQualRec = prlAssign->pQual;
				SetStyleRange ( CGXRange(ilDisplayed+2,1), 
								CGXStyle().SetItemDataPtr( (void*)prlQualRec ) );
				if ( SetLineValues ( ilDisplayed, CString(""), 
									 prlQualRec->code, olCount, 
									 prlQualRec->prio ) )
				{
					ilDisplayed++;
				}
				else
					SetStyleRange ( CGXRange(ilDisplayed+2,1), 
									CGXStyle().SetIncludeItemDataPtr( FALSE ) );
			}
		//  au�erdem nicht zugeordnete (Gruppen-)Qualifikationen in gelb anzeigen
		for ( int i=0; i<pomData->omQualis.GetSize(); i++ )
		{
			prlQualRec = &(pomData->omQualis[i]);
			if ( pomData->IsPersonalAssigned ( TYP_QUALI, prlQualRec ) )
				continue;
			SetStyleRange ( CGXRange(ilDisplayed+2,1), 
							CGXStyle().SetItemDataPtr( (void*)prlQualRec ).
									   SetInterior(PYELLOW) );
			if ( SetLineValues ( ilDisplayed, (CString&)"", prlQualRec->code,
								 olCount, prlQualRec->prio ) )
			{
				ilDisplayed++;
			}
			else
				SetStyleRange ( CGXRange(ilDisplayed+2,1), 
						    CGXStyle().SetIncludeItemDataPtr( FALSE ) );
		}
	}
	OnChangedMode ();
}

void CQualificationsGrid::OnChangedMode ()
{
	CPersonalGrid::OnChangedMode ();
	CString olTitle ;
	olTitle = GetString ( bmExpanded ? IDS_SINGLE_PER_HEADER : IDS_PER_HEADER );
	SetTitle ( olTitle );
}

BOOL CQualificationsGrid::OnEndEditing( ROWCOL nRow, ROWCOL nCol) 
{
	PERSONALRECORD*	prlDataRec = GetAssocPtr ( nRow );
	CString olOldCode, olOldPrio;
	UINT	ilOldAmount=0, ilNewAmount;
	bool	blOk=true, blRowDeleted =false;

	if ( prlDataRec )
	{
		olOldCode = prlDataRec->code;
		olOldPrio = prlDataRec->prio;
		ilOldAmount = pomData->GetPersonalCount ( TYP_QUALI, olOldCode );
	}

	BOOL ilRet = CTitleGridControl::OnEndEditing( nRow, nCol) ;

	CString olWert = GetValueRowCol ( nRow, nCol );
	if (nRow>=2 )
	{
		SetDefaultValues ( nRow );
		DisplayInfo ( nRow, nCol );
		if ( DeleteEmptyRows ( nRow, nCol ) )
			blRowDeleted = true;		//  Resource wurde gel�scht
	}	
	if ( nRow <2 )
		return ilRet;

	switch ( nCol )
	{
		case 1:
			if (olWert==olOldCode )
				break;	//  gleiche Resource wie vorher fertig
			if ( !olOldCode.IsEmpty() )
			{	//  ge�nderte Resource
				if ( bmExpanded )
					//  nur Zuordnung l�schen
					//  pomData->OnDeletePersonal ( TYP_QUALI, prlDataRec );
					//  besser: Zuordnung und Quali l�schen
					pomData->DeletePersonal ( TYP_QUALI, prlDataRec );

				else
					blOk = pomData->DeleteAllPersonal ( TYP_QUALI, olOldCode );
			}
			if ( !olWert.IsEmpty() )
			{
				ilNewAmount = 1;
				if ( !bmExpanded && !prlDataRec )		
				{	//  ggf. zuvor gesetzte Anzahl u. Prio auslesen
					CString olStr;
					olStr = GetValueRowCol ( nRow, 2 );
					if ( olStr.IsEmpty() ||
						 (sscanf(olStr, "%d", &ilNewAmount ) <= 0 ) )
						 ilNewAmount = 1;
					olStr = GetValueRowCol ( nRow, 3 );
					if ( !olStr.IsEmpty() )
						olOldPrio = olStr;
				}

				if ( bmExpanded )
				{
					prlDataRec = pomData->GetUnAssignedQuali ( olWert );
					if ( prlDataRec && prmDisplayedFunc && 
						 IsQualificationAssigned ( nRow) )
						pomData->AddAssignment ( CString(""), prmDisplayedFunc, 
												 prlDataRec );
				}
				else
				{
					pomData->AddPersonal ( TYP_QUALI, olWert, CString(""), 
										   olOldPrio, ilNewAmount );
					prlDataRec = &(pomData->omQualis[pomData->omQualis.GetSize()-1]);
				}
			}
			else
			{
				if ( blRowDeleted )
					return ilRet&&blOk;
				prlDataRec = 0;
			}
			SetStyleRange ( CGXRange(nRow,1), 
							CGXStyle().SetItemDataPtr( (void*)prlDataRec ) );
			UpdateDisplay ( prmDisplayedFunc );
			break;
		case 2:
			if ( bmExpanded || !prlDataRec )
				break;
			if ( sscanf ( olWert, "%u", &ilNewAmount ) <= 0 )
				break;
			if ( ilOldAmount != ilNewAmount )
				blOk = pomData->ChangePersonalCount ( TYP_QUALI, olOldCode, 
													  olOldPrio, ilNewAmount );
			break;
		case 3:
			if ( bmExpanded || !prlDataRec )
				break;
			if ( olWert!= olOldPrio )
				blOk = pomData->ChangePersonalPrio ( TYP_QUALI, olOldCode,
													 olWert );
	}	
	return ilRet&&blOk;
}


bool CQualificationsGrid::IsResourceAllReadySelected ( CString &ropResource, 
													   CString &ropTableName,
													   ROWCOL ipDontTest, 
													   bool bpErrMsg/*=true*/ )
{
	CString olWert;
	ROWCOL	ilRowCount = GetRowCount();

	if ( bmExpanded )
	{
		for ( ROWCOL i=2; i<=ilRowCount; i ++ )
		{
			if ( i==ipDontTest )
				continue;
			olWert = GetValueRowCol ( i, 1 );
			if ( olWert == ropResource )
			{
				if ( !IsQualificationAssigned ( i ) )
					continue;	//  gefundene Qualifikation ist nicht zugeordnet

				if ( bpErrMsg )
					MessageBox ( GetString(IDS_RES_ALREADY_SEL) );
				return true;
			}

		}
		return false;
	}
	else
		return CServiceGrid::IsResourceAllReadySelected ( ropResource, 
										ropTableName, ipDontTest, bpErrMsg );
}

bool CQualificationsGrid::IsQualificationAssigned ( ROWCOL ipRow )
{
	CGXStyle olStyle;
	COLORREF ilBkColor, ilColor, ilVgl=PYELLOW;
	if ( GetStyleRowCol( ipRow, 1, olStyle ) )
	{
		ilBkColor = olStyle.GetInterior().GetBkColor(); 
		ilColor = olStyle.GetInterior().GetColor(); 
		return ( ilColor != PYELLOW );
		 //( olStyle.GetInterior().GetBkColor() == PYELLOW ) )
		//return false;
	}
	else
		return true;
}

BOOL CQualificationsGrid::OnRButtonClickedRowCol(ROWCOL nRow, ROWCOL nCol, UINT nFlags, CPoint pt)
{
	//  Check ob, Einzeldarstellung der Qualifikationen einer Funktion und ob
	//  die Zelle (nRow,nCol) mit einer Qualifikation verbunden
	if ( !bmExpanded || !prmDisplayedFunc || (nCol!=1) || (nRow<2) )
		return CPersonalGrid::OnRButtonClickedRowCol ( nRow, nCol, nFlags, pt );
	PERSONALRECORD	*prlQualRec = GetAssocPtr ( nRow );
	if ( !prlQualRec )
		return CPersonalGrid::OnRButtonClickedRowCol ( nRow, nCol, nFlags, pt );
	//  Toggle Status zugeordnet
	if ( IsQualificationAssigned ( nRow ) )
		//  Zuordnung l�schen
		pomData->OnDeletePersonal ( TYP_QUALI, prlQualRec );
	else
		if ( !IsResourceAllReadySelected ( prlQualRec->code, 
										   CString("PER"), nRow ) )
			pomData->AddAssignment ( CString(""), prmDisplayedFunc, prlQualRec );
		else
			return TRUE;
	UpdateDisplay ( prmDisplayedFunc );
	return TRUE;
}

bool CQualificationsGrid::DoppelteResourcen ()
{
	//  suche doppelt vergebene Resourcen
	if ( !bmExpanded )
		return CPersonalGrid::DoppelteResourcen();
	else
	{
		CString olWert1, olWert2;
		ROWCOL	i, j, ilRowCount = GetRowCount();
		for ( i=2; i<ilRowCount; i++ )
		{
			olWert1 = GetValueRowCol ( i, 1 );
			for ( j=i+1; j<=ilRowCount; j++ )
			{
				//  nicht zugeordnete d�rfen ruhig doppelt sein
				if ( !IsQualificationAssigned ( j )	)	
					continue;
				olWert2 = GetValueRowCol ( j, 1 );
				if ( !olWert1.IsEmpty () && ( olWert1==olWert2 ) )
					return true;
			}
		}
		return false;
	}
}
