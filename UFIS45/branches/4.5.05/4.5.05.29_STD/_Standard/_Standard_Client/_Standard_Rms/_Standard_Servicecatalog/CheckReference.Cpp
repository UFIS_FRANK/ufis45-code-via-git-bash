// CheckReference.cpp
 
#include <stdafx.h>
#include <resource.h>
#include <CheckReference.h>
#include <CCSGlobl.h>

#include <RecordSet.h>
#include <CedaBasicData.h>


//-------------------------------------------------------------------------------------


CheckReference::CheckReference()
{
	char pclTmpText[512];
	char pclConfigPath[512];
    if (getenv("CEDA") == NULL)
        strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
    else
        strcpy(pclConfigPath, getenv("CEDA"));

	bmUseBackDoor = false;
    GetPrivateProfileString(pcgAppName, "BACKDOORAAT","NO", pclTmpText,sizeof pclTmpText, pclConfigPath);
	if (stricmp(pclTmpText,"YES") == 0)
	{
		bmUseBackDoor = true;
	}

}

//-------------------------------------------------------------------------------------

CheckReference::~CheckReference()
{

}

//-------------------------------------------------------------------------------------

int CheckReference::Check(int ipTableID, long lpUrno, CString opCode)
{
	CWaitCursor olWait;

	int ilNoOfEntrys = 0;
	int ilCount = 0, ilCount2=0;
	DWORD dlStart1, dlEnd1, dlStart2, dlEnd2;

	CString olWhere;
	CString olTmpWhere;
	CString olObject;
	CString olFields = "URNO";
	CCSPtrArray<RecordSet> olBuffer;
	switch (ipTableID)
	{
		case _SER:
		{
			olObject = "RUD";
			olWhere.Format("WHERE UGHS='%ld'", lpUrno);
			dlStart1 = GetTickCount();
			ilCount = ogBCD.ReadSpecial(olObject, olFields, olWhere, olBuffer);
			dlEnd1 = GetTickCount();
			dlEnd1 -= dlStart1;
			if (ilCount > 0) 
			{
				olWhere.Format("WHERE UGHS='%ld' AND URUE IN (SELECT URNO FROM RUETAB WHERE RUST !='2')", lpUrno);
				olBuffer.DeleteAll();
				dlStart2 = GetTickCount();
				ilCount2 = ogBCD.ReadSpecial(olObject, olFields, olWhere, olBuffer);
				dlEnd2 = GetTickCount();
				dlEnd2 -= dlStart2;
				if ( (ilCount>0) && (ilCount2 <= 0 ) )
					ilNoOfEntrys = -ilCount; /* service only used in archived rule */
				else
					ilNoOfEntrys = ilCount;	/* service not only used in archived rule */

			}
			olBuffer.DeleteAll();

			break;
		}
		default:
		{
			ilNoOfEntrys = 0;
		}
	}
	return ilNoOfEntrys;
}

//-------------------------------------------------------------------------------------
bool CheckReference::BackDoorEnabled()
{
	return this->bmUseBackDoor;
}