// basicdat.h CCSBasicData class for providing general used methods

#ifndef _BASICDATA
#define _BASICDATA

#include <ccsglobl.h>
#include <ccsptrarray.h>
#include <ccscedaData.h>
#include <CcsLog.h>
#include <CedaCfgData.h>
#include <CCSTime.h>
#include <CCSDdx.h>
#include <CCSCedaCom.h>
#include <CCSBcHandle.h>
#include <CCSMessageBox.h>


int GetItemCount(CString olList, char cpTrenner  = ',' );
int ExtractItemList(CString opSubString, CStringArray *popStrArray, char cpTrenner = ',');
CString GetListItem(CString &opList, int ipPos, bool bpCut, char cpTrenner  = ',');
CString DeleteListItem(CString &opList, CString olItem);
//CString LoadStg(UINT nID);
CTime COleDateTimeToCTime(COleDateTime &opTime);
COleDateTime CTimeToCOleDateTime(CTime &opTime);
COleDateTime OneCOleDateTimeFromTwo(COleDateTime &opDate, COleDateTime &opTime);
CTime CTimeFromTwoCOleDateTime(COleDateTime &opDate, COleDateTime &opTime);
bool CTimeToTwoCOleDateTime( CTime &opDateTime, 
							 COleDateTime &opDate, COleDateTime &opTime );
void MakeUrnoListFromVC2 ( CString &opVc2, CStringList &list );
CString CTwoOleDateTimeToDBString(COleDateTime &opDate,COleDateTime &opTime);
CString COneOleDateTimeToDBString(COleDateTime &opDateTime);
bool DoesTableExist ( char *table );
bool SaveServiceValidity ( CString &ropServiceUrno, CString opVafr, 
						   CString opVato );
bool LoadServiceValidity ( CString &ropServiceUrno, CString &ropVafr,
						   CString &ropVato );
bool DeleteServiceValidity ( CString &ropServiceUrno );

/////////////////////////////////////////////////////////////////////////////
// class BasicData

class CBasicData : public CCSCedaData
{

public:

	CBasicData(void);
	~CBasicData(void);
	
	int imScreenResolutionX;
	int imScreenResolutionY;
	int imMonitorCount;
	long ConfirmTime;
	CString omUserID;

	long GetNextUrno(void);
	int imNextOrder;


	int GetNextOrderNo();

	char *GetCedaCommand(CString opCmdType);
	void GetDiagramStartTime(CTime &opStart, CTime &opEnd);
	void SetDiagramStartTime(CTime opDiagramStartTime, CTime opEndTime);
	void SetWorkstationName(CString opWsName);
	CString GetWorkstationName();


public:
	static void TrimLeft(char *s);
	static void TrimRight(char *s);

private:

	char pcmCedaCmd[12];
	CTime omDiaStartTime;
	CTime omDiaEndTime;

	CString omDefaultComands;
	CString omActualComands;
	char pcmCedaComand[24];
	CString omWorkstationName; 
};


#endif
