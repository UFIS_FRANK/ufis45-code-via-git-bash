// CCSGlobl.cpp: implementation of the CCSGlobl .
//
//////////////////////////////////////////////////////////////////////

#include <stdafx.h>
#include <resource.h>
#include <CCSGlobl.h>
#include <CCSCedaData.h>
#include <CCSCedaCom.h>
#include <CCSDdx.h>
#include <CCSLog.h>
#include <CCSBcHandle.h>
#include <CedaBasicData.h>
#include <CCSBasic.h>
#include <BasicData.h>
#include <CedaCfgData.h>
#include <ChangeInfo.h>

#include <stdio.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


//-------------------------------
//			global variables
//-------------------------------

//--- error handling
/*		PRF5468: no longer using CritErr.txt
ofstream of_catch;
*/
//--- field indices
// ALO (allocation units)
int igAloAlocIdx;
int igAloAlodIdx;
int igAloAlotIdx;
int igAloReftIdx;
int igAloUrnoIdx;
// EQT (equipment types)
int igEqtNameIdx = -1;
int igEqtUrnoIdx = -1;
// EQU (equipment numbers)
int igEquEnamIdx = -1;
int igEquEtypIdx = -1;
int igEquGcdeIdx = -1;
int igEquGkeyIdx = -1;
int igEquUrnoIdx = -1;
// DGR (dynamic groups)
int igDgrExprIdx;
int	igDgrGnamIdx;
int	igDgrReftIdx;
int	igDgrUrnoIdx;
int	igDgrUtplIdx;
// PFC (Funktionen)
int igPfcDptcIdx;
int igPfcFctcIdx;
int igPfcFctnIdx;
int igPfcPrflIdx;
int igPfcRemaIdx;
int igPfcUrnoIdx;
// RPF (rules personnel functions)
int igRpfFccoIdx;
int igRpfFcnoIdx;
int igRpfPrioIdx;
int igRpfUpfcIdx;
int igRpfUrnoIdx;
int igRpfUrudIdx;
int igRpfGtabIdx;	//  hag990826
// RPQ (rules personnel qualifications)
int igRpqPrioIdx;
int igRpqQucoIdx;
int igRpqQunoIdx;
int igRpqUdgrIdx;
int igRpqUperIdx;
int igRpqUrnoIdx;
int igRpqUrudIdx;
int igRpqGtabIdx;	//  hag990826
// REQ (rules personnel qualifications)
int igReqEqcoIdx;
int igReqEqnoIdx;
int igReqEtypIdx;	
int igReqPrioIdx;
int igReqUequIdx;
int igReqUrnoIdx;
int igReqUrudIdx;
int igReqGtabIdx;	//  hag990826
// RLO (rules personnel qualifications)
int igRloRlocIdx;
int igRloLonoIdx;
int igRloPrioIdx;
int igRloReftIdx;
int igRloUrnoIdx;
int igRloUrudIdx;
int igRloGtabIdx;	//  hag990826
// RUD (Rule Demands)
int igRudAlocIdx;
int igRudDbflIdx;
int igRudDeflIdx;
int igRudCoduIdx;
int igRudDbarIdx;
int igRudDearIdx;
int igRudDebeIdx;
int igRudDecoIdx;
int igRudDeduIdx;
int igRudDeenIdx;
int igRudDideIdx;
int igRudDindIdx;
int igRudDispIdx;
int igRudDrtyIdx;
int igRudEadbIdx;
int igRudEdemIdx;
int igRudFaddIdx;
int igRudFcndIdx;
int igRudFfpdIdx;
int igRudFncdIdx;
int igRudFondIdx;
int igRudFsadIdx;
int igRudLadeIdx;
int igRudLifrIdx;
int igRudLireIdx;
int igRudLitoIdx;
int igRudLodeIdx;
int igRudMaxcIdx;
int igRudMaxdIdx;
int igRudMincIdx;
int igRudMindIdx;
int igRudNresIdx;
int igRudRedeIdx;
int igRudRemaIdx; 
int igRudRendIdx;
int igRudRepdIdx;
int igRudRetyIdx;
int igRudRtdbIdx; 
int igRudRtdeIdx; 
int igRudSdtiIdx;
int igRudSutiIdx;
int igRudStdeIdx; 
int igRudTsdbIdx; 
int igRudTsdeIdx; 
int igRudTsndIdx; 
int igRudTspdIdx; 
int igRudTtgfIdx;
int igRudTtgtIdx;
int igRudUcruIdx; 
int igRudUdemIdx; 
int igRudUdgrIdx;
int igRudUghcIdx;
int igRudUghsIdx;
int igRudUlnkIdx;
int igRudUndeIdx; 
int igRudUpdeIdx;
int igRudUproIdx;  
int igRudUrnoIdx;
int igRudUrueIdx;
int igRudUsesIdx;
int igRudReftIdx;
int igRudVrefIdx;
int igRudUtplIdx;
// RUE (Rule Events)
int igRueApplIdx;
int igRueCdatIdx;
int igRueEvrmIdx;
int igRueEvtaIdx;
int igRueEvtdIdx;
int igRueEvttIdx;
int igRueEvtyIdx;
int igRueExclIdx;
int igRueFisuIdx;
int igRueFsplIdx;
int igRueLstuIdx;
int igRueMaxtIdx;
int igRueMistIdx;
int igRuePrioIdx;
int igRueRemaIdx;
int igRueRunaIdx;
int igRueRusnIdx;
int igRueRustIdx;
int igRueRutyIdx;
int igRueUarcIdx;
int igRueUrnoIdx;
int igRueUsecIdx;
int igRueUseuIdx;
int igRueUtplIdx;
// SEE (service catalogue personnel equipment)
int igSeeEqcoIdx;
int igSeeEqnoIdx;
int igSeeEtypIdx;
int igSeeGtabIdx;
int igSeePrioIdx;
int igSeeUequIdx;
int igSeeUpfcIdx;
int igSeeUrnoIdx;
int igSeeUsrvIdx;
// SEF (service catalogue personnel functions)
int igSefFccoIdx;
int igSefFcnoIdx;
int igSefGtabIdx;
int igSefPrioIdx;
int igSefUpfcIdx;
int igSefUrnoIdx;
int igSefUsrvIdx;
// SEL (service catalogue personnel locations)
int igSelGtabIdx;
int igSelLonoIdx;
int igSelPrioIdx;
int igSelReftIdx;
int igSelRlocIdx;
int igSelUrnoIdx;
int igSelUsrvIdx;
// SEQ (service catalogue personnel qualifications)
int igSeqGtabIdx;
int igSeqPrioIdx;
int igSeqQucoIdx;
int igSeqQunoIdx;
int igSeqUperIdx;
int igSeqUrnoIdx;
int igSeqUsrvIdx;
// SER (Static Groups)
int igSerFdutIdx;
int igSerFfisIdx;
int igSerFisuIdx;
int igSerFsdtIdx;
int igSerFsetIdx;
int igSerFsutIdx;
int igSerFwtfIdx;
int igSerFwttIdx;
int igSerRtwfIdx;
int igSerRtwtIdx;
int igSerSdutIdx;
int igSerSeaxIdx;
int igSerSecoIdx;
int igSerSeerIdx;
int igSerSehcIdx;
int igSerSeivIdx;
int igSerSetyIdx;
int igSerSnamIdx;
int igSerSsapIdx;
int igSerSsdtIdx;
//int igSerSsnmIdx;
int igSerSsutIdx;
int igSerSwtfIdx;
int igSerSwttIdx;
int igSerUrnoIdx;
int igSerVafrIdx;
int igSerVatoIdx;
// SGR (Static Groups)
int igSgrApplIdx;
int igSgrFldnIdx;
int igSgrGrdsIdx;
int igSgrGrpnIdx;
int igSgrGrsnIdx;
int igSgrPrflIdx;
int igSgrTabnIdx;
int igSgrUrnoIdx;
int igSgrUgtyIdx;
int igSgrUtplIdx;
// SGM (Group Members)
int igSgmPrflIdx;
int igSgmTabnIdx;
int igSgmUrnoIdx;
int igSgmUsgrIdx;
int igSgmUvalIdx;
int igSgmValuIdx;
// TPL (Templates)
int igTplApplIdx;
int igTplDaloIdx;
int igTplEtypIdx;
int igTplEvorIdx;
int igTplFisuIdx;
int igTplFldaIdx;
int igTplFlddIdx;
int igTplFldtIdx;
int igTplRelxIdx;
int igTplTnamIdx;
int igTplTpstIdx;
int igTplUrnoIdx;
int igTplFtyaIdx=-1;
int igTplFtydIdx=-1;
// TSR (template sources)
int igTsrBfldIdx;
int igTsrBtabIdx;
int igTsrExteIdx;
int igTsrFireIdx;
int igTsrFormIdx;
int igTsrNameIdx;
int igTsrOperIdx;
int igTsrRfldIdx;
int igTsrRtabIdx;
int igTsrTextIdx;
int igTsrTypeIdx;
int igTsrUrnoIdx;
// VAL (Validity)
int igValApplIdx;
int igValFreqIdx;
int igValTabnIdx;
int igValTimfIdx;
int igValTimtIdx;
int igValUrnoIdx;
int igValUvalIdx;
int igValVafrIdx;
int igValVatoIdx;
int igValVtypIdx;

//--- global choice lists for comboboxes (main window grids)
CCSPtrArray <CHOICE_LISTS> ogChoiceLists;

//--- global choice lists for comboboxes (DetailDlg and FlightIndepView)
CCSPtrArray <RES_CHOICELIST> ogResChoiceLists;

//--- global array of TSR-referenced tables and fields
CCSPtrArray <REFTABLE> ogRefTables;

//--- global array of items to update (from tables RUD, RPQ, RPF, RLO, REQ, ...)
//CCSPtrArray <TAB_ITEM> ogUpdItems;
CCSPtrArray <UPD_ITEM> ogRudItems;
CCSPtrArray <UPD_ITEM> ogRpfItems;
CCSPtrArray <UPD_ITEM> ogRpqItems;
CCSPtrArray <UPD_ITEM> ogRloItems;
CCSPtrArray <UPD_ITEM> ogReqItems;
CMapStringToPtr ogUpdItems;

//--- array with selected rud urnos 
// (a workaround, ogBCD's handling of logical fields is buggy)
CStringArray ogSelectedRuds; 

//--- array for given commandline parameters
CStringArray ogCmdLineArgsArray;

//--- string with list of time references (content of listboxes in detail window), separated by ";"
CCSPtrArray <REFTIME> ogInboundReferences;
CCSPtrArray <REFTIME> ogOutboundReferences;

//--- general global variables
CString ogAppName = "REGELWERK";	// This is the only string that is to be hardcoded here !!!
									// The Xs are only to recognize failures in SQLHDL.log
CString ogCallingApp = "";

char pcgUser[33] = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";
char pcgPasswd[33] = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";
char pcgConfigPath[142] = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";
char pcgHome[4] = "XXX";
char pcgTableExt[10] = "XXX";

char SOH = 1;
char STX = 2;
char ETX = 3;

//--- own classes' global objects
CCSLog			ogLog(NULL);
CCSDdx			ogDdx;
CCSCedaCom      ogCommHandler(&ogLog, NULL);  
CCSBcHandle     ogBcHandle(&ogDdx, &ogCommHandler, &ogLog);
CCSBasic		ogCCSBasic;		// lib
CBasicData		ogBasicData;	// local
CedaCfgData		ogCfgData;
PrivList		ogPrivList;
DataSet			ogDataSet;
CedaBasicData	ogBCD(pcgHome, &ogDdx, &ogBcHandle, &ogCommHandler, &ogCCSBasic);
ChangeInfo		ogChangeInfo;


//--- fixed fonts
CFont ogSmallFonts_Regular_6;
CFont ogSmallFonts_Regular_7;
CFont ogSmallFonts_Regular_8;
CFont ogSmallFonts_Bold_7;

CFont ogMSSansSerif_Regular_6;
CFont ogMSSansSerif_Regular_8;
CFont ogMSSansSerif_Regular_12;
CFont ogMSSansSerif_Regular_16;
CFont ogMSSansSerif_Bold_7;
CFont ogMSSansSerif_Bold_8;

CFont ogCourier_Regular_8;
CFont ogCourier_Regular_9;
CFont ogCourier_Regular_10;
CFont ogCourier_Bold_8;
CFont ogCourier_Bold_10;

CFont ogTimesNewRoman_9;
CFont ogTimesNewRoman_12;
CFont ogTimesNewRoman_16;
CFont ogTimesNewRoman_30;

CFont ogSetupFont;

//--- scalable fonts
CFont ogScalingFonts[30];
FONT_INDEXES ogStaffIndex;
FONT_INDEXES ogFlightIndex;
FONT_INDEXES ogHwIndex;
FONT_INDEXES ogRotIndex;

//--- font indices
int igFontIndex1;
int igFontIndex2;

//--- brush and color arrays
CBrush *ogBrushs[MAXCOLORS + 1];
CBrush ogHatchBrushTurn, ogHatchBrushIn, ogHatchBrushOut;
COLORREF ogColors[MAXCOLORS + 1];

//--- points
CPoint ogMaxTrackSize = CPoint(1024, 768);
CPoint ogMinTrackSize = CPoint(1024 / 4, 768 / 4);

//--- anything else
bool bgAdaptScale = false;
bool bgDebug = false;
bool bgUseValidityType = false;
bool bgOnline = true;
bool bgIsServicesOpen = false;
bool bgUseResourceStrings = true;
bool bgIsValSaved = false;
bool bgMaxGroundTimeIsMin = false;



CGUILng* ogGUILng = CGUILng::TheOne();
//const char pcgIniFile[_MAX_PATH+1] = "C:\\UFIS\\SYSTEM\\RmsRules.INI";
const CString ogIniFile = CCSLog::GetUfisSystemPath("\\RmsRules.INI");

//----------------------------------------------------------------------------------------------------------------------
//					global functions
//----------------------------------------------------------------------------------------------------------------------

CString GetResString(UINT ID)
{
	CString olRet;
	olRet.LoadString(ID);
	return olRet;
}
//----------------------------------------------------------------------------------------------------------------------

CString GetString(UINT ipIDS)
{
	/*CGUILng* */ogGUILng = CGUILng::TheOne();
	return ogGUILng->GetString(ipIDS);
}
//----------------------------------------------------------------------------------------------------------------------

void RudTmp_Dump()
{
	// MNE TEST 001110
	FILE *pFile = fopen(CCSLog::GetTmpPath("\\rud_dedu.log"), "at");
	if (pFile)
	{
		CString Separator("====================================\n");
		fwrite(Separator, Separator.GetLength(), 1, pFile);

		CString s;
		CCSPtrArray<RecordSet>olRuds;
		int ilRudCount = ogBCD.GetDataCount( "RUD_TMP" );

		for (int i = 0; i < ilRudCount; i++)
		{	
			s = ogBCD.GetField ( "RUD_TMP", i, "URNO" ) + "\t";
			s += ogBCD.GetField ( "RUD_TMP", i, "DEDU" ) + "\t";
			s += ogBCD.GetField ( "RUD_TMP", i, "UGHS" ) + "\n";
			fwrite(s, s.GetLength(), 1, pFile);
		}
		fclose(pFile);
		olRuds.DeleteAll();
	}
	else
	{
		AfxMessageBox("Dump of RUD_TMP duty time failed.");
	}
	// END TEST
}


/*
CString GetString(UINT ipIDS )
{
	
	//return GetResString ( ipIDS );
	CString olText="???", olUrno, olIDS;
	bool	blFound = false;
	if ( ogBCD.GetDataCount("TXT") > 0 )
	{
		olIDS.Format ("%ld", ipIDS );
		olUrno = ogBCD.GetFieldExt( "TXT", "STID", "APPL", olIDS, "RULE_AFT", "URNO" );
		if ( !olUrno.IsEmpty() )
			 blFound = ogBCD.GetField( "TXT", "URNO", olUrno, "STRG", olText ) ;
		if ( !blFound )
		{
			olUrno = ogBCD.GetFieldExt( "TXT", "STID", "APPL", olIDS, "GENERAL", "URNO" );
			if ( !olUrno.IsEmpty() )
				blFound = ogBCD.GetField( "TXT", "URNO", olUrno, "STRG", olText ) ;
		}
	}
	if ( !blFound && bgUseResourceStrings )
		olText = GetResString ( ipIDS );
	return olText;
}
*/

//----------------------------------------------------------------------------------------------------------------------
bool GetString (CString opRefField, CString opIDS, CString &ropText)
{
	CString olText="???", olUrno;
	bool	blFound = false;
	
	if ( ogBCD.GetDataCount("TXT") > 0 )
	{
		olUrno = ogBCD.GetFieldExt( "TXT", opRefField, "APPL", opIDS, "RULE_AFT", "URNO" );
		if ( !olUrno.IsEmpty() )
			 blFound = ogBCD.GetField( "TXT", "URNO", olUrno, "STRG", olText ) ;
		if ( !blFound )
		{
			olUrno = ogBCD.GetFieldExt( "TXT", opRefField, "APPL", opIDS, "GENERAL", "URNO" );
			if ( !olUrno.IsEmpty() )
				blFound = ogBCD.GetField( "TXT", "URNO", olUrno, "STRG", olText ) ;
		}
	}
	//if ( !blFound && bpUseResourceStrings )
	//	olText = GetResString ( ipIDS );
	ropText = olText;
	return blFound;
}
//----------------------------------------------------------------------------------------------------------------------

void CreateBrushes()
{
	 int ilLc;

	for( ilLc = 0; ilLc < FIRSTCONFLICTCOLOR; ilLc++)
	{
		ogColors[ilLc] = RED;
	}
	
	ogColors[RED_IDX] = RED;
	ogColors[LGREEN_IDX] = LGREEN;
	ogColors[GRAY_IDX] = GRAY;
	ogColors[GREEN_IDX] = GREEN;
	ogColors[BLUE_IDX] = BLUE;
	ogColors[SILVER_IDX] = SILVER;
	ogColors[MAROON_IDX] = MAROON;
	ogColors[OLIVE_IDX] = OLIVE;
	ogColors[NAVY_IDX] = NAVY;
	ogColors[PURPLE_IDX] = PURPLE;
	ogColors[TEAL_IDX] = TEAL;
	ogColors[LIME_IDX] = LIME;
	ogColors[YELLOW_IDX] = YELLOW;
	ogColors[FUCHSIA_IDX] = FUCHSIA;
	ogColors[AQUA_IDX] = AQUA;
	ogColors[WHITE_IDX] = WHITE;
	ogColors[BLACK_IDX] = BLACK;
	ogColors[ORANGE_IDX] = ORANGE;
	ogColors[PYELLOW_IDX] = PYELLOW;
	ogColors[PGREEN_IDX] = PGREEN;
	ogColors[PBLUE_IDX] = PBLUE;
	ogColors[ROSE_IDX] = ROSE;
	ogColors[DARKBLUE_IDX] = DARKBLUE;
	// don't use index 21 it's used for Break jobs already
	
	for(ilLc = 0; ilLc < MAXCOLORS; ilLc++)
	{
		ogBrushs[ilLc] = new CBrush();
		ogBrushs[ilLc]->CreateSolidBrush(ogColors[ilLc]);
	}
	CBitmap olBmpIn, olBmpOut, olBmpTurn;
	if ( olBmpIn.LoadBitmap ( IDB_PARTIAL_IN ) )
		ogHatchBrushIn.CreatePatternBrush ( &olBmpIn );
	else
		ogHatchBrushIn.CreateHatchBrush ( HS_BDIAGONAL, PGREEN_IDX );
	
	if ( olBmpOut.LoadBitmap ( IDB_PARTIAL_OUT ) )
		ogHatchBrushOut.CreatePatternBrush ( &olBmpOut );
	else
		ogHatchBrushOut.CreateHatchBrush ( HS_BDIAGONAL, PBLUE_IDX );

	if ( olBmpTurn.LoadBitmap ( IDB_PARTIAL_TURN ) )
		ogHatchBrushTurn.CreatePatternBrush ( &olBmpTurn );
	else
		ogHatchBrushTurn.CreateHatchBrush ( HS_BDIAGONAL, PYELLOW_IDX );
}

void DeleteBrushes()
{
	for( int ilLc = 0; ilLc < MAXCOLORS; ilLc++)
	{
		CBrush *olBrush = ogBrushs[ilLc];
		delete olBrush;
	}
}

void InitFont() 
{
    CDC dc;
    dc.CreateCompatibleDC(NULL);

    LOGFONT logFont;
    memset(&logFont, 0, sizeof(LOGFONT));

	for(int i = 0; i < 8; i++)
	{
		logFont.lfCharSet= DEFAULT_CHARSET;
		logFont.lfHeight = - MulDiv(i, dc.GetDeviceCaps(LOGPIXELSY), 72);
		logFont.lfWeight = FW_NORMAL;
		logFont.lfQuality = PROOF_QUALITY;
		logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_SWISS;
		lstrcpy(logFont.lfFaceName, "Small Fonts"); 
		ogScalingFonts[i].CreateFontIndirect(&logFont);
	}
	for( i = 8; i < 20; i++)
	{ 
		logFont.lfCharSet= DEFAULT_CHARSET;
		logFont.lfHeight = - MulDiv(i, dc.GetDeviceCaps(LOGPIXELSY), 72);
		logFont.lfWeight = FW_NORMAL;
		logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_SWISS;
		logFont.lfQuality = PROOF_QUALITY;

		lstrcpy(logFont.lfFaceName, "MS Sans Serif"); /*"MS Sans Serif""Arial"*/
		ogScalingFonts[i/*GANTT_S_FONT*/].CreateFontIndirect(&logFont);
	}

	logFont.lfCharSet= DEFAULT_CHARSET;
    logFont.lfHeight = - MulDiv(6, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_NORMAL;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_SWISS;
    lstrcpy(logFont.lfFaceName, "Small Fonts");
    ogSmallFonts_Regular_6.CreateFontIndirect(&logFont);
        
	logFont.lfCharSet= DEFAULT_CHARSET;
    logFont.lfHeight = - MulDiv(7, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_NORMAL;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_SWISS;
    lstrcpy(logFont.lfFaceName, "Small Fonts");
    ogSmallFonts_Regular_7.CreateFontIndirect(&logFont);

	logFont.lfCharSet= DEFAULT_CHARSET;
    logFont.lfHeight = - MulDiv(7, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_BOLD;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_SWISS;
    lstrcpy(logFont.lfFaceName, "Small Fonts");
    ogSmallFonts_Bold_7.CreateFontIndirect(&logFont);
        
	
	logFont.lfCharSet= DEFAULT_CHARSET;
	logFont.lfHeight = - MulDiv(8, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_NORMAL;
    logFont.lfPitchAndFamily = DEFAULT_PITCH | FF_SWISS;
    lstrcpy(logFont.lfFaceName, "MS Sans Serif");
    ogMSSansSerif_Regular_8.CreateFontIndirect(&logFont);

	logFont.lfCharSet= DEFAULT_CHARSET;
    logFont.lfHeight = - MulDiv(8, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_NORMAL;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_MODERN;
    lstrcpy(logFont.lfFaceName, "Courier New");
    ogCourier_Regular_8.CreateFontIndirect(&logFont);

	logFont.lfCharSet= DEFAULT_CHARSET;
	logFont.lfHeight = - MulDiv(9, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_NORMAL;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_MODERN;
    lstrcpy(logFont.lfFaceName, "Courier New");
    ogCourier_Regular_9.CreateFontIndirect(&logFont);
	
	logFont.lfCharSet= DEFAULT_CHARSET;
	logFont.lfHeight = - MulDiv(10, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_NORMAL;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_MODERN;
    lstrcpy(logFont.lfFaceName, "Courier New");
    ogCourier_Regular_10.CreateFontIndirect(&logFont);

	logFont.lfCharSet= DEFAULT_CHARSET;
	logFont.lfHeight = - MulDiv(10, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_BOLD;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_MODERN;
    lstrcpy(logFont.lfFaceName, "Courier New");
    ogCourier_Bold_10.CreateFontIndirect(&logFont);

	ogStaffIndex.VerticalScale = MS_SANS8;
	ogStaffIndex.Chart = MS_SANS6;
	ogFlightIndex.VerticalScale = MS_SANS8;
	ogFlightIndex.Chart = MS_SANS6;
	ogHwIndex.VerticalScale = MS_SANS8;
	ogHwIndex.Chart = MS_SANS6;

	logFont.lfCharSet= DEFAULT_CHARSET;
    logFont.lfHeight = - MulDiv(8, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_BOLD;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_SWISS;
    lstrcpy(logFont.lfFaceName, "MS Sans Serif");
    ogMSSansSerif_Bold_8.CreateFontIndirect(&logFont);

	logFont.lfCharSet= DEFAULT_CHARSET;
    logFont.lfHeight = - MulDiv(7, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_BOLD;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_SWISS;
    lstrcpy(logFont.lfFaceName, "Verdana");
    ogMSSansSerif_Bold_7.CreateFontIndirect(&logFont);

	logFont.lfCharSet= DEFAULT_CHARSET;
    logFont.lfHeight = - MulDiv(30, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_BOLD;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_ROMAN;
	logFont.lfQuality = PROOF_QUALITY;
    lstrcpy(logFont.lfFaceName, "Times New Roman");
    ogTimesNewRoman_30.CreateFontIndirect(&logFont);

	logFont.lfCharSet= DEFAULT_CHARSET;
    logFont.lfHeight = - MulDiv(16, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_NORMAL;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_ROMAN;
	logFont.lfQuality = PROOF_QUALITY;
    lstrcpy(logFont.lfFaceName, "Times New Roman");
    ogTimesNewRoman_16.CreateFontIndirect(&logFont);

	logFont.lfCharSet= DEFAULT_CHARSET;
    logFont.lfHeight = - MulDiv(12, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_NORMAL;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_ROMAN;
	logFont.lfQuality = PROOF_QUALITY;
    lstrcpy(logFont.lfFaceName, "Times New Roman");
    ogTimesNewRoman_12.CreateFontIndirect(&logFont);

	logFont.lfCharSet= DEFAULT_CHARSET;
    logFont.lfHeight = - MulDiv(9, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_NORMAL;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_ROMAN;
	logFont.lfQuality = PROOF_QUALITY;
    lstrcpy(logFont.lfFaceName, "Times New Roman");
    ogTimesNewRoman_9.CreateFontIndirect(&logFont);

	logFont.lfCharSet= DEFAULT_CHARSET;
	logFont.lfHeight = - MulDiv(9, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_NORMAL;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_MODERN;
    lstrcpy(logFont.lfFaceName, "Courier New");
    ogSetupFont.CreateFontIndirect(&logFont);

    dc.DeleteDC();
}



CString GetDBString (CString ID)
{
	// int test = ogBCD.GetDataCount("TXT");
	CString olID;
	CString olRetString;

	olRetString = ogBCD.GetField("TXT", "TXID", ID, "STRG");
	return (olRetString);
} // end of GetDBString


bool SetDlgItemLangText ( CWnd *popWnd, UINT ipIDC, UINT ipIDS ) 
{
	CString olText;
	CWnd	*polItem;
	
	if ( !popWnd )
		return false;
	polItem = popWnd->GetDlgItem(ipIDC);
	if ( !polItem )
		return false;
	olText = GetString ( ipIDS );
	polItem->SetWindowText(olText);
	return true;
}

bool SetWindowLangText ( CWnd *popWnd, UINT ipIDS )
{
	CString olText;
	
	if ( !popWnd )
		return false;
	olText = GetString ( ipIDS );
	popWnd->SetWindowText(olText);
	return true;
}


bool SetDlgItemLangText ( CWnd *popWnd, UINT ilIDC, CString opRefField, 
						  CString opIDS )
{
	CString olText;
	bool	blFound;
	CWnd	*polItem;
	
	if ( !popWnd )
		return false;
	polItem = popWnd->GetDlgItem(ilIDC);
	if ( !polItem )
		return false;
	blFound = GetString ( opRefField, opIDS, olText);
	if ( blFound || !bgUseResourceStrings )
		polItem->SetWindowText(olText);
	return true;
}

bool SetWindowLangText ( CWnd *popWnd, CString opRefField, CString opIDS )
{
	CString olText;
	bool	blFound;
	
	if ( !popWnd )
		return false;
	blFound = GetString ( opRefField, opIDS, olText);
	if ( blFound || !bgUseResourceStrings )
		popWnd->SetWindowText(olText);
	return true;
}

bool ClearDlgItem ( CWnd *popWnd, UINT ipIDC)
{
	CWnd *polCtrl = 0;
	if ( popWnd )
		polCtrl = popWnd->GetDlgItem ( ipIDC );
	if ( !polCtrl )
		return false;
	polCtrl->SetWindowText( "" );
	return true;
}


bool EnableDlgItem ( CWnd *popWnd, UINT ipIDC, BOOL bpEnable, BOOL bpShow )
{
	CWnd *polCtrl = 0;
	if ( popWnd )
		polCtrl = popWnd->GetDlgItem ( ipIDC );
	if ( !polCtrl )
		return false;
	polCtrl->EnableWindow ( bpEnable );
	polCtrl->ShowWindow ( bpShow ? SW_SHOW : SW_HIDE );
	return true;
}

BOOL MoveDlgItem ( CWnd *popWnd, UINT ipIDC, int x, int y )
{
	CWnd *polCtrl = 0;
	if ( popWnd )
		polCtrl = popWnd->GetDlgItem ( ipIDC );
	if ( !polCtrl )
		return FALSE;
	return polCtrl->SetWindowPos( 0, x, y, 0, 0, SWP_NOSIZE | SWP_NOZORDER  );
}

BOOL ResizeDlgItem ( CWnd *popWnd, UINT ipIDC, int cx, int cy )
{
	CWnd *polCtrl = 0;
	if ( popWnd )
		polCtrl = popWnd->GetDlgItem ( ipIDC );
	if ( !polCtrl )
		return FALSE;
	return polCtrl->SetWindowPos( 0, 0, 0, cx, cy, SWP_NOMOVE | SWP_NOZORDER  );
}


bool AddItemToUpdList(CString opUrno, CString opTable, int ipStatus)
{
	//  return at once, because it won't be evaluated anywhere
	return true;

	if ((ipStatus >= -1 && ipStatus <= 2) == FALSE)
	{
		return false;
	}
	
	CCSPtrArray <UPD_ITEM> *polItemArr;
	if (ogUpdItems.Lookup(opTable, (void *&) polItemArr) == TRUE)
	{
		UPD_ITEM rlItem;
		rlItem.Status = ipStatus;
		rlItem.Urno = atol(opUrno);
		
		polItemArr->New(rlItem);
	}

	return true;
}


bool SaveWindowPosition ( CWnd *popWnd, char *pcpIniEntry )
{
	char polEintrag[81];
	RECT rect;
	if ( !popWnd || !pcpIniEntry )
		return false;
	popWnd->GetWindowRect ( &rect );
	sprintf ( polEintrag, "%d %d %d %d", rect.left, rect.top, rect.right, 
										 rect.bottom );
	if ( WritePrivateProfileString ( "FENSTER", pcpIniEntry, polEintrag, 
									 ogIniFile ) )
		return true;
	else
		return false;
}


bool IniWindowPosition ( CWnd *popWnd, char *pcpIniEntry )
{
	char polEintrag[81];
	RECT rect;
	if ( !popWnd || !pcpIniEntry )
		return false;
	if ( !GetPrivProfString ( "FENSTER", pcpIniEntry, "", polEintrag, 80,
							  ogIniFile )  || 
		 ( sscanf ( polEintrag, "%d %d %d %d", &rect.left, &rect.top, &rect.right, 
										 &rect.bottom ) < 4 )
	   )
		return false;
	popWnd->MoveWindow ( &rect );
	return true;
}

bool GetPrivProfString ( LPCTSTR pcpSection, LPCTSTR pcpEntry, LPCTSTR pcpDefault, 
					     LPTSTR pcpValue, DWORD ipSize, LPCTSTR pcpFileName )
{
	char buffer1[11], buffer2[11];
	bool blRet = false;

	GetPrivateProfileString ( pcpSection, pcpEntry, "A", buffer1, 10, pcpFileName );
	GetPrivateProfileString ( pcpSection, pcpEntry, "B", buffer2, 10, pcpFileName );
	  
	blRet = strcmp ( buffer1, buffer2 ) ? false : true;
	GetPrivateProfileString ( pcpSection, pcpEntry, pcpDefault, pcpValue,
							  ipSize, pcpFileName );
	return blRet;


}
