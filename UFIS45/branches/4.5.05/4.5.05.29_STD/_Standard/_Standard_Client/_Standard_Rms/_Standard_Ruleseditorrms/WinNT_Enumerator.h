// WinNT_Enumerator.h: interface for the CWinNT_Enumerator class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_WINNT_ENUMERATOR_H__BEDCCE20_325F_11D3_B0F2_444553540000__INCLUDED_)
#define AFX_WINNT_ENUMERATOR_H__BEDCCE20_325F_11D3_B0F2_444553540000__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <windows.h>
#include <vdmdbg.h>
#include <psapi.h>

//Windows NT Functions
typedef BOOL (WINAPI *ENUMPROCESSES)(
  DWORD * lpidProcess,  // array to receive the process identifiers
  DWORD cb,             // size of the array
  DWORD * cbNeeded      // receives the number of bytes returned
);

typedef BOOL (WINAPI *ENUMPROCESSMODULES)(
  HANDLE hProcess,      // handle to the process
  HMODULE * lphModule,  // array to receive the module handles
  DWORD cb,             // size of the array
  LPDWORD lpcbNeeded    // receives the number of bytes returned
);

typedef DWORD (WINAPI *GETMODULEFILENAME)( 
  HANDLE hProcess,		// handle to the process
  HMODULE hModule,		// handle to the module
  LPTSTR lpstrFileName,	// array to receive filename
  DWORD nSize			// size of filename array.
);

typedef DWORD (WINAPI *GETMODULEBASENAME)( 
  HANDLE hProcess,		// handle to the process
  HMODULE hModule,		// handle to the module
  LPTSTR lpstrFileName,	// array to receive base name of module
  DWORD nSize			// size of module name array.
);

typedef INT (WINAPI *VDMENUMTASKWOWEX)(
  DWORD dwProcessId,	// ID of NTVDM process
  TASKENUMPROCEX fp,	// address of our callback function
  LPARAM lparam);		// anything we want to pass to the callback function.




class CWinNT_Enumerator  
{
	enum { max_num = 1024 };

    HANDLE psapi; 
	HANDLE vdmdbg;

    ENUMPROCESSES       EnumProcesses;
    GETMODULEFILENAME   GetModuleFileName;
    ENUMPROCESSMODULES  EnumProcessModules;  
	VDMENUMTASKWOWEX	VDMEnumTaskWOWEx;
	GETMODULEBASENAME	GetModuleBaseName;

public:
	BOOL Enumerate(CMapStringToPtr &MapProc);
	int InitEnumerate(CMapStringToPtr &MapProc);

	CWinNT_Enumerator();
	virtual ~CWinNT_Enumerator();

};

#endif // !defined(AFX_WINNT_ENUMERATOR_H__BEDCCE20_325F_11D3_B0F2_444553540000__INCLUDED_)
