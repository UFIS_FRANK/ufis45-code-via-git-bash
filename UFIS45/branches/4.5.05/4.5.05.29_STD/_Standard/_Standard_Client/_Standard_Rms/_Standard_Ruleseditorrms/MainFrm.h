// MainFrm.h : interface of the CMainFrame class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_MAINFRM_H__5C27D148_B08A_11D2_AAE6_00001C018CF3__INCLUDED_)
#define AFX_MAINFRM_H__5C27D148_B08A_11D2_AAE6_00001C018CF3__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000




#include <ComboBoxBar.h>
#include <ValidityDlg.h>



class CMainFrame : public CFrameWnd
{
public: // create from serialization only
	CMainFrame();
	~CMainFrame();

	DECLARE_DYNCREATE(CMainFrame)


		
//--- Implementation
	enum eView {FLIGHTRELATED = 1, INDEPENDENT = 2, COLLECTIVE = 3};
	
	bool SwitchToView(eView nView);
//	bool LoadValidity(CString opRueUrno);
	CString GetRueUrno();

	void GetMessageString(UINT nID, CString& rMessage) const;
	bool TranslateMenu ();

	void FillTemplateCombo();
	void UpdateViewArea();
	bool DoLoadRule ( CString &ropRuleUrno, CString &ropTplName );
	bool IsContinue();
	BOOL CanModifyActualTemplate ();

	void HideServicesListBtn(bool bpHide);
	void RestoreGridColWidths(CGridFenster *popGrid);
	void SaveGridColWidths(CGridFenster *popGrid);
	int GetViewType ( CView *popView =NULL);
	int FindTplInComboBox(CString &ropTplName );
	
	CCSPtrArray <DWORD> *GetListOfStartedProcesses();

//--- attributes
	// control bar embedded members
	CStatusBar     m_wndStatusBar;
	CComboBoxBar   m_wndToolBar;   //--- Eigene ToolBar erweiterung

	VALDATA rmValData;
	WIDTHS rmWidths; // this is an STL vector

	bool bmNewFlag;
	bool bmIsValChanged;
	bool bmIsSCRule;
	bool bmFirstTimeInit;
	bool bmIsRuleSavedOk;
	bool bmIsGridColWidthsSaved;

	int  ilEvty;

	HWND m_hwndFirstView;
private:
	bool bmMultiEditAllowed;
	bool bmRuleHistoryAllowed;
	DWORD ilLastExtStart;	//  Zeitpunkt zu dem zum letzten Mal ein
							//  externes Programm gestartet worden ist
	
//--- Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMainFrame)
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	//}}AFX_VIRTUAL

#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

// Generated message map functions
protected:
	//{{AFX_MSG(CMainFrame)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnContractBtn();
	afx_msg void OnValidityBtn();
	afx_msg void OnSaveRule();
	afx_msg void OnNewRule(bool bpShowNewDlg=true) ;
	afx_msg void OnGroupBtn();
	afx_msg void OnLeistungen();
	afx_msg void OnLoadRule();
	afx_msg void OnVorlagenEditor();
	afx_msg void OnSelEndOk();
	afx_msg void OnCopy();
	afx_msg void OnDelete();
	afx_msg void OnViewFlightrelated();
	afx_msg void OnViewIndep();
	afx_msg void OnViewCollective();
	afx_msg void OnServicesEditor();
	afx_msg BOOL OnToolTipText(UINT nID, NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnClose();
	afx_msg void OnRuleSavedOk(WPARAM wParam, LPARAM lParam);
	afx_msg void OnRuleModified(WPARAM wParam, LPARAM lParam);
	afx_msg void OnUpdateButtons(CCmdUI* pCmdUI);
	afx_msg void OnUpdateFileServices(CCmdUI* pCmdUI);
	afx_msg void OnUpdateOptStatgrp(CCmdUI* pCmdUI);
	afx_msg void OnMultiEdit();
	afx_msg void OnUpdateEditBatch(CCmdUI* pCmdUI);
	afx_msg void OnCheckArchive();
	afx_msg void OnFileHistory();
	afx_msg void OnUpdateFileHistory(CCmdUI* pCmdUI);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

BOOL CanModifyTemplate ( CString opTplName );
static void ProcessRueChange( void *popInstance, int ipDDXType, 
							  void *vpDataPointer, CString &ropInstanceName);

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MAINFRM_H__5C27D148_B08A_11D2_AAE6_00001C018CF3__INCLUDED_)
