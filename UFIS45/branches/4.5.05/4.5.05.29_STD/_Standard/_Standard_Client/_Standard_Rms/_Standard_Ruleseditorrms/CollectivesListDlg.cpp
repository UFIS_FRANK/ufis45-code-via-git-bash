// CollectivesListDlg.cpp : implementation file
//

#include <stdafx.h>
#include <regelwerk.h>
#include <ccsglobl.h>
#include <CedaBasicData.h>
#include <CollectivesListDlg.h>
#include <RulesGanttViewer.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif



//--------------------------------------------------------------------------------------------------------------------
//					construction / destruction
//--------------------------------------------------------------------------------------------------------------------
CollectivesListDlg::CollectivesListDlg(CString opUtpl, CWnd* pParent /*=NULL*/)
	: CDialog(CollectivesListDlg::IDD, pParent)
{
	omTplUrno = opUtpl;
	omRueUrno = "-1";
	omRudUrno = "-1";

	pomParent = pParent;
	pomGrid = new CGridFenster(this);
	pomRudGrid = new CGridFenster(this);


	//{{AFX_DATA_INIT(CollectivesListDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}
//--------------------------------------------------------------------------------------------------------------------

CollectivesListDlg::~CollectivesListDlg()
{
	delete pomGrid;
	pomGrid = NULL;
	delete pomRudGrid;
	pomRudGrid = NULL;
}



//--------------------------------------------------------------------------------------------------------------------
//					data exchange, message map
//--------------------------------------------------------------------------------------------------------------------
void CollectivesListDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CollectivesListDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CollectivesListDlg, CDialog)
	//{{AFX_MSG_MAP(CollectivesListDlg)
	ON_MESSAGE(GRID_MESSAGE_DOUBLECLICK, OnGridDoubleClick)
	ON_MESSAGE(GRID_MESSAGE_CURRENTCELLCHANGED, OnGridCurrentCellChanged)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()



//--------------------------------------------------------------------------------------------------------------------
//					message handlers
//--------------------------------------------------------------------------------------------------------------------
BOOL CollectivesListDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	SetStaticTexts();

	//CWnd *pWnd = GetDlgItem(IDC_RUE_GRID);
	int test = 0;
	BOOL blErr = pomGrid->SubclassDlgItem(IDC_RUE_GRID, this);
	pomGrid->Initialize();
	
	blErr &= pomRudGrid->SubclassDlgItem(IDC_RUD_GRID, this);
	pomRudGrid->Initialize();

	CCSPtrArray <RecordSet> olRecords;
	ogBCD.GetRecordsExt("RUE", "UTPL", "RUTY", omTplUrno, CString("1"), &olRecords);
	

	int ilRowCount = olRecords.GetSize();
	pomGrid->GetParam()->EnableUndo(FALSE);
	pomGrid->LockUpdate(TRUE);
	
	pomGrid->SetRowCount(ilRowCount);
	pomGrid->SetColCount(3);
	pomGrid->HideCols(3, 3);
	
	pomGrid->SetValueRange(CGXRange(0, 1), GetString(KURZNAME));
	pomGrid->SetValueRange(CGXRange(0, 2), GetString(217));
	
	pomGrid->GetParam()->EnableTrackRowHeight(GX_TRACK_NOTHEADER);

	CRect olRect;
	pomGrid->GetClientRect(&olRect);
	olRect.DeflateRect(8, 0);
	pomGrid->SetColWidth(0,0,20);	// header
	int ilWidthRest = olRect.Width()- 20;
	pomGrid->SetColWidth(1,1,ilWidthRest/3);	// shortname
	pomGrid->SetColWidth(2,2,ilWidthRest*2/3);	// name

	for (int ilRow = 0; ilRow < olRecords.GetSize(); ilRow++)
	{
		pomGrid->SetValueRange(CGXRange(ilRow + 1, 1), olRecords[ilRow].Values[igRueRusnIdx]);
		pomGrid->SetValueRange(CGXRange(ilRow + 1, 2), olRecords[ilRow].Values[igRueRunaIdx]);
		pomGrid->SetValueRange(CGXRange(ilRow + 1, 3), olRecords[ilRow].Values[igRueUrnoIdx]);
		if ( !omRueUrno.IsEmpty() && 
			 ( omRueUrno == olRecords[ilRow].Values[igRueUrnoIdx] ) )
		{
			pomGrid->SelectRowHeader(ilRow + 1 );
			//DisplayRuds ();
		}
	}

	pomGrid->LockUpdate(FALSE); 
	pomGrid->Redraw();
	pomGrid->SetStyleRange( CGXRange().SetTable(), 
							CGXStyle().SetReadOnly(TRUE));
	pomGrid->GetParam()->EnableUndo(TRUE);
	
	olRecords.DeleteAll();

	pomRudGrid->GetParam()->EnableUndo(FALSE);
	pomRudGrid->LockUpdate(TRUE);
	
	pomRudGrid->SetRowCount(0);
	pomRudGrid->SetColCount(4);
	pomRudGrid->HideCols(4, 4);
	
	pomRudGrid->SetValueRange(CGXRange(0, 1), GetString(IDS_SERVICE));
	pomRudGrid->SetValueRange(CGXRange(0, 2), GetString(IDS_RESOURCE));
	pomRudGrid->SetValueRange(CGXRange(0, 3), GetString(IDS_KLASSE));
	
	pomRudGrid->GetParam()->EnableTrackRowHeight(GX_TRACK_NOTHEADER);

	pomRudGrid->GetClientRect(&olRect);
	olRect.DeflateRect(8, 0);
	pomRudGrid->SetColWidth(0,0,20);	// header
	ilWidthRest = olRect.Width()- 20;
	pomRudGrid->SetColWidth(1,2,ilWidthRest*2/5);	// service + resource
	pomRudGrid->SetColWidth(3,3,ilWidthRest/5);		// counterclass

	pomRudGrid->LockUpdate(FALSE);
	pomRudGrid->Redraw();
	pomRudGrid->GetParam()->EnableUndo(TRUE);
	DisplayRuds ();
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}
//--------------------------------------------------------------------------------------------------------------------

void CollectivesListDlg::OnOK() 
{
	ROWCOL ilCol = 0;
	ROWCOL ilRow = 0;

	if ( pomGrid->GetCurrentCell(ilRow, ilCol) && (ilRow>0) )
	{
		omRueUrno = pomGrid->GetValueRowCol(ilRow, 3);
	}
	
	if ( omRueUrno.IsEmpty() == TRUE || omRueUrno == CString(" "))
	{
		omRueUrno = CString("-1");
	}

	if ( pomRudGrid->GetCurrentCell(ilRow, ilCol) && (ilRow>0) )
		omRudUrno = pomRudGrid->GetValueRowCol(ilRow, 4);
	
	if (omRudUrno.IsEmpty() || ( omRudUrno == " " ) )
		omRudUrno = CString("-1");
	CDialog::OnOK();
}
//--------------------------------------------------------------------------------------------------------------------

void CollectivesListDlg::OnCancel() 
{
	omRueUrno = omRudUrno = CString("-1");
	CDialog::OnCancel();
}
//--------------------------------------------------------------------------------------------------------------------

void CollectivesListDlg::OnGridDoubleClick(WPARAM wParam, LPARAM lParam) 
{
	CCS_TRY

	
	CELLPOS *prlPos = (CELLPOS*)lParam;
	ROWCOL ilCol = prlPos->Col;
	ROWCOL ilRow = prlPos->Row;
	CGridFenster	*polGrid = (CGridFenster*)wParam;
	
	/*omRueUrno = pomGrid->GetValueRowCol(ilRow, 3);
	
	if (omRueUrno.IsEmpty() == TRUE || omRueUrno == CString(" "))
	{
		omRueUrno = CString("-1");
	}
	EndDialog(IDOK);*/
	if ( polGrid && (ilRow>0 ) )
	{
		polGrid->SetCurrentCell ( ilRow, ilCol );
		OnOK();
	}
	
	CCS_CATCH_ALL
}
//--------------------------------------------------------------------------------------------------------------------

void CollectivesListDlg::SetStaticTexts()
{
	CCS_TRY
	
	CWnd *pWnd = NULL;

	// caption
	SetWindowText(GetString(1648));

	// buttons
	CString olText;
	//GetString("TXID", "OK", olText);
	olText = GetString(OK);
	pWnd = GetDlgItem(IDOK);
	if ( pWnd )
		pWnd->SetWindowText(olText);
	//GetString("TXID", "CANCEL", olText);
	olText = GetString(CANCEL);
	pWnd = GetDlgItem(IDCANCEL);
	if ( pWnd )
		pWnd->SetWindowText(olText);

	// statics
	pWnd = GetDlgItem(CONNECT_FRA_ORDER);
	//pWnd->SetWindowText();

	CCS_CATCH_ALL
}


void CollectivesListDlg::OnGridCurrentCellChanged(WPARAM wParam, LPARAM lParam) 
{
	CCS_TRY
	
	CELLPOS *prlPos = (CELLPOS*)lParam;
	ROWCOL ilCol = prlPos->Col;
	ROWCOL ilRow = prlPos->Row;
	
	if ( (CGridFenster*)wParam == pomGrid )
	{
		omRueUrno = pomGrid->GetValueRowCol(ilRow, 3);
		if (omRueUrno.IsEmpty() == TRUE || omRueUrno == CString(" "))
			omRueUrno = CString("-1");
		else
			omRudUrno = "-1";
		DisplayRuds ();
	}
	if ( ilRow && wParam )
		((CGridFenster*)wParam)->SelectRowHeader	( ilRow );
	CCS_CATCH_ALL
}

void CollectivesListDlg::DisplayRuds ()
{
	CCSPtrArray <RecordSet> olRecords;
	RulesGanttViewer *polViewer=0;
	CString olStr, olRudReft, olTable, olField;
	
	if ( !omRueUrno.IsEmpty() && (omRueUrno!="-1") )
	{
		ogBCD.GetRecordsExt("RUD", "URUE", "ALOC", omRueUrno, omRudAloc, &olRecords);
		long ilRueUrno = atol( omRueUrno );
		polViewer = new RulesGanttViewer(ilRueUrno); 
	}

	pomRudGrid->GetParam()->EnableUndo(FALSE);
	pomRudGrid->LockUpdate(TRUE);
	pomRudGrid->SetRowCount( olRecords.GetSize() );
	pomRudGrid->SelectRowHeader(-1);

	for ( int i=0; i<olRecords.GetSize(); i++ )
	{
		if ( !olRecords[i].Values[igRudUghsIdx].IsEmpty() &&
			 ogBCD.GetField ( "SER", "URNO", olRecords[i].Values[igRudUghsIdx], "SNAM", olStr ) )
			pomRudGrid->SetValueRange(CGXRange(i + 1, 1), olStr);
		if ( polViewer )
		{
			olStr = polViewer->BarTextAndValues( &(olRecords[i]), false );
			pomRudGrid->SetValueRange(CGXRange(i + 1, 2), olStr);
		}
		if ( (igRudReftIdx>=0) && (igRudVrefIdx>=0) &&
			 ParseReftFieldEntry ( olRecords[i].Values[igRudReftIdx], olTable, 
								   olField ) &&
			 ( olTable=="CCC" ) && 
			 ogBCD.GetField ( olTable, olField, 
							  olRecords[i].Values[igRudVrefIdx], 
							  "CICC", olStr ) 
		   )
		    pomRudGrid->SetValueRange(CGXRange(i + 1, 3), olStr);
		else
			pomRudGrid->SetValueRange(CGXRange(i + 1, 3), "");

		pomRudGrid->SetValueRange(CGXRange(i + 1, 4), olRecords[i].Values[igRudUrnoIdx]);
		if ( !omRudUrno.IsEmpty() && 
			 ( omRudUrno == olRecords[i].Values[igRudUrnoIdx] ) )
			 pomRudGrid->SelectRowHeader(i + 1 );

	}
	pomRudGrid->GetParam()->EnableUndo(TRUE);
	pomRudGrid->LockUpdate(FALSE);
	pomRudGrid->Redraw();

	olRecords.DeleteAll ();			
	if ( polViewer )
		delete polViewer;
}
//--------------------------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------------------------
//	GetResourceInCollective: Sucht f�r die RudURNO ropRudSiUrno einer Einzelregel, 
//							 die entsprechende Resource in der Sammelregel 
//							 ropRueCoUrno und legt diese ggf. an. 
//	IN: ropRudSiUrno: Urno der Einsatzvorgabe der EinzelRegel
//		ropRueCoUrno: Urno der Sammelregel
//		bpCreate:	  gibt an, ob Vollbedarf in Sammelregel angelegt werden soll, 
//					  falls diese Resource in der Sammelregel nicht gefunden worden ist.
//	return:	URNO der gesuchten Einsatzvorgabe in Sammelregel bzw. "", wenn nicht 
//			gefunden und bpCreate==false
CString GetResourceInCollective ( CString &ropRudSiUrno, CString &ropRueCoUrno, 
								  bool bpCreate/*=true*/ )
{
	
	RecordSet olRudSingle(ogBCD.GetFieldCount("RUD_TMP"));
	CCSPtrArray <RecordSet> olCollectiveRuds, olSingleRes;
	CString olRetySi, olAlocSi, olRet="", olResUrnoSi, olResUrnoCo ;
	CString	olReftSi, olReftCo, olDrtySi, olVrefSi, olUtpl;
	
	if ( !ogBCD.GetRecord( "RUD_TMP", "URNO", ropRudSiUrno, olRudSingle ) )
		return "";
	olRetySi = olRudSingle.Values[igRudRetyIdx];
	olAlocSi = olRudSingle.Values[igRudAlocIdx];
	olDrtySi = olRudSingle.Values[igRudDrtyIdx];

	if ( igRudReftIdx>=0 )
		olReftSi = olRudSingle.Values[igRudReftIdx];
	if ( igRudVrefIdx>=0 )
		olVrefSi = olRudSingle.Values[igRudVrefIdx];

	ogBCD.GetRecordsExt("RUD", "URUE", "ALOC", ropRueCoUrno, olAlocSi, 
						&olCollectiveRuds );

	for (int i=0; i<olCollectiveRuds.GetSize() && olRet.IsEmpty(); i++ )
	{
		//  Check resource-type
		if ( olCollectiveRuds[i].Values[igRudRetyIdx] != olRetySi )
			continue;
		//  check type of duty requirements
		if ( olCollectiveRuds[i].Values[igRudDrtyIdx] != olDrtySi )
			continue;
		//  demands must be relative 
		if ( ( olCollectiveRuds[i].Values[igRudDbarIdx] != "R" ) ||
			 ( olCollectiveRuds[i].Values[igRudDearIdx] != "R" ) )
			continue;
		//  demands must not be partial 
		if ( ( olCollectiveRuds[i].Values[igRudFfpdIdx] != "0" ) )
			continue;
		//  Check Counterclass of Resource
		if ( ( igRudReftIdx>=0 ) &&
			 ( olCollectiveRuds[i].Values[igRudReftIdx] != olReftSi ) )
			 continue;
		if ( ( igRudVrefIdx>=0 ) &&
			 ( olCollectiveRuds[i].Values[igRudVrefIdx] != olVrefSi ) )
			 continue;
	
		if ( olRetySi == "100" )
		{
			olResUrnoSi = ogBCD.GetField ( "RPF_TMP", "URUD", ropRudSiUrno, "UPFC" );
			olResUrnoCo = ogBCD.GetField ( "RPF", "URUD", 
								olCollectiveRuds[i].Values[igRudUrnoIdx], "UPFC" );
			if ( olResUrnoSi != olResUrnoCo )
				continue;
			//  Select all Qualifications of single rule
			ogBCD.GetRecords ("RPQ_TMP", "URUD", ropRudSiUrno, &olSingleRes );
			//  check whether found collective rule's RUD contains all these qualif.
			bool blOk=true;
			CString olUrno;
			for ( int j=0; (j<olSingleRes.GetSize())&&blOk; j++ ) 
			{
				olUrno = ogBCD.GetFieldExt("RPQ", "URUD", "UPER", 
									olCollectiveRuds[i].Values[igRudUrnoIdx], 
									olSingleRes[j].Values[igRpqUperIdx], "URNO" );
				blOk &= !olUrno.IsEmpty();
			}
			if ( blOk )						//passende Resource gefunden
				olRet = olCollectiveRuds[i].Values[igRudUrnoIdx];	
			olSingleRes.DeleteAll();
		}
		else if ( olRetySi == "001" )
		{
			olReftSi = ogBCD.GetField ( "RLO_TMP", "URUD", ropRudSiUrno, "REFT" );
			olReftCo = ogBCD.GetField ( "RLO", "URUD", 
							olCollectiveRuds[i].Values[igRudUrnoIdx], "REFT" );
			olResUrnoSi = ogBCD.GetField ( "RLO_TMP", "URUD", ropRudSiUrno, "RLOC" );
			olResUrnoCo = ogBCD.GetField ( "RLO", "URUD", 
								olCollectiveRuds[i].Values[igRudUrnoIdx], "RLOC" );
			if ( (olReftSi==olReftCo) && (olResUrnoSi==olResUrnoCo ) )
				olRet = olCollectiveRuds[i].Values[igRudUrnoIdx];	//passende Resource gefunden
		}
		
	}

	if ( olRet.IsEmpty() && bpCreate )
	{	//  erzeuge Einsatzvorgabe in Sammelregel
		RecordSet olRudCollective( olRudSingle.Values, 
								   ogBCD.GetFieldCount("RUD"));
		olRudCollective.Values[igRudUrueIdx] = ropRueCoUrno;
		if ( igRudUtplIdx >= 0 )
		{
			olUtpl = ogBCD.GetField( "RUE", "URNO", ropRueCoUrno, "UTPL" );
			if( !olUtpl.IsEmpty() )
				olRudCollective.Values[igRudUtplIdx] = olUtpl;
		}
		olRet.Format ( "%ld", ogBCD.GetNextUrno() );
		olRudCollective.Values[igRudUrnoIdx] = olRet;
		olRudCollective.Values[igRudDbarIdx] = 
			olRudCollective.Values[igRudDearIdx] = "R";
		olRudCollective.Values[igRudFfpdIdx] = "0";
		ogBCD.InsertRecord ( "RUD", olRudCollective, true );

		if ( olRetySi == "100" )
		{
			ogBCD.GetRecords ("RPF_TMP", "URUD", ropRudSiUrno, &olSingleRes );
			for ( i=0; i<olSingleRes.GetSize(); i++ ) 
			{
				olSingleRes[i].Values[igRpfUrnoIdx].Format("%ld", 
														ogBCD.GetNextUrno() );
				olSingleRes[i].Values[igRpfUrudIdx] = olRet;
				ogBCD.InsertRecord ( "RPF", olSingleRes[i], true );
			}
			olSingleRes.DeleteAll ();
			ogBCD.GetRecords ("RPQ_TMP", "URUD", ropRudSiUrno, &olSingleRes );
			for ( i=0; i<olSingleRes.GetSize(); i++ ) 
			{
				olSingleRes[i].Values[igRpqUrnoIdx].Format("%ld", 
														ogBCD.GetNextUrno() );
				olSingleRes[i].Values[igRpqUrudIdx] = olRet;
				olSingleRes[i].Values[igRpqUdgrIdx] = "";
				ogBCD.InsertRecord ( "RPQ", olSingleRes[i] );
			}
			if ( olSingleRes.GetSize() >0 )
				ogBCD.Save ( "RPQ" ) ;
		}
		else if ( olRetySi == "001" )
		{
			ogBCD.GetRecords ("RLO_TMP", "URUD", ropRudSiUrno, &olSingleRes );
			for ( i=0; i<olSingleRes.GetSize(); i++ ) 
			{
				olSingleRes[i].Values[igRloUrnoIdx].Format("%ld", 
														ogBCD.GetNextUrno() );
				olSingleRes[i].Values[igRloUrudIdx] = olRet;
				ogBCD.InsertRecord ( "RLO", olSingleRes[i], true );
			}
		}
		olSingleRes.DeleteAll ();
	}
	olCollectiveRuds.DeleteAll ();
	return olRet;
}
			
