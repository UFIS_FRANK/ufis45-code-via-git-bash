// GroupingDlg.cpp : implementation file
//

#include <stdafx.h>
#include <regelwerk.h>
#include <GroupingDlg.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif



//-----------------------------------------------------------------------------------------------------------------------
//								Construction / Destruction
//-----------------------------------------------------------------------------------------------------------------------
GroupingDlg::GroupingDlg(CWnd* pParent /*=NULL*/)
	: CDialog(GroupingDlg::IDD, pParent)
{
	pomSrcGrid = new CGridFenster;
	pomTabGrid = new CGridFenster;
	pomGrpGrid = new CGridFenster;

	//{{AFX_DATA_INIT(GroupingDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}
//-----------------------------------------------------------------------------------------------------------------------

GroupingDlg::~GroupingDlg()
{
	delete pomSrcGrid;
	delete pomTabGrid;
	delete pomGrpGrid;
}



//-----------------------------------------------------------------------------------------------------------------------
//								Data Exchange, Message Map
//-----------------------------------------------------------------------------------------------------------------------
void GroupingDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(GroupingDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}
//-----------------------------------------------------------------------------------------------------------------------

BEGIN_MESSAGE_MAP(GroupingDlg, CDialog)
	//{{AFX_MSG_MAP(GroupingDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()



//-----------------------------------------------------------------------------------------------------------------------
//								message handlers
//-----------------------------------------------------------------------------------------------------------------------
void GroupingDlg::OnCancel() 
{
	// TODO: Add extra cleanup here
	
	CDialog::OnCancel();
}
//-----------------------------------------------------------------------------------------------------------------------

void GroupingDlg::OnOK() 
{
	// TODO: Add extra validation here
	
	CDialog::OnOK();
}
//-----------------------------------------------------------------------------------------------------------------------

BOOL GroupingDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	//--- initializing grids
	BOOL blErr = pomTabGrid->SubclassDlgItem(GROUP_CUS_TABLE, this);
		 blErr = pomSrcGrid->SubclassDlgItem(GROUP_CUS_SOURCE, this);
	     blErr = pomGrpGrid->SubclassDlgItem(GROUP_CUS_GROUP, this);

	pomTabGrid->Initialize();
	pomSrcGrid->Initialize();
	pomGrpGrid->Initialize();

	pomTabGrid->SetColCount(2);
	pomTabGrid->SetRowCount(17);
	pomTabGrid->SetColWidth(0,0, 20);
	pomTabGrid->SetColWidth(1,1, 80);
	pomTabGrid->SetColWidth(2,2, 70);

	pomSrcGrid->SetColCount(3);
	pomSrcGrid->SetRowCount(17);
	pomSrcGrid->SetColWidth(0,0, 20);
	pomSrcGrid->SetColWidth(1,1, 70);
	pomSrcGrid->SetColWidth(2,2, 150);
	pomSrcGrid->SetColWidth(3,3, 70);

	pomGrpGrid->SetColCount(3);
	pomGrpGrid->SetRowCount(17);
	pomGrpGrid->SetColWidth(0,0, 20);
	pomGrpGrid->SetColWidth(1,1, 40);
	pomGrpGrid->SetColWidth(2,2, 60);
	pomGrpGrid->SetColWidth(3,3, 38);
	
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}
//-----------------------------------------------------------------------------------------------------------------------

