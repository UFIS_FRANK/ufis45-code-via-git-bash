#if !defined(AFX_OLLECTIVESLISTDLG_H__0DC1ABD3_18D5_11D3_A62C_0000C007916B__INCLUDED_)
#define AFX_OLLECTIVESLISTDLG_H__0DC1ABD3_18D5_11D3_A62C_0000C007916B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ollectivesListDlg.h : header file
//



#include <DialogGrid.h>

/////////////////////////////////////////////////////////////////////////////
// CollectivesListDlg dialog

class CollectivesListDlg : public CDialog
{
// Construction
public:
	CollectivesListDlg(CString opUtpl, CWnd* pParent = NULL);   // standard constructor
	~CollectivesListDlg();

	void SetStaticTexts();
// Dialog Data
	//{{AFX_DATA(CollectivesListDlg)
	enum { IDD = IDD_COLLECTIVES_LIST };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CollectivesListDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CollectivesListDlg)
	virtual void OnOK();
	virtual void OnCancel();
	virtual BOOL OnInitDialog();
	afx_msg void OnGridDoubleClick(WPARAM wParam, LPARAM lParam);
	afx_msg void OnGridCurrentCellChanged(WPARAM wParam, LPARAM lParam);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	void DisplayRuds ();

// attributes
public:
	CString omTplUrno;
	CString omRueUrno;
	CString omRudUrno;
	CString omRudAloc;

	CWnd *pomParent;
	CGridFenster *pomGrid;
	CGridFenster *pomRudGrid;

};

CString GetResourceInCollective ( CString &ropRudSiUrno, CString &ropRueCoUrno, 
								  bool bpCreate=true );

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_OLLECTIVESLISTDLG_H__0DC1ABD3_18D5_11D3_A62C_0000C007916B__INCLUDED_)
