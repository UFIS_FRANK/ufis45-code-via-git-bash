// DutyGrid.h: interface for the CDutyGrid class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_DUTYGRID_H__4F6E9453_9035_11D3_9393_00001C033B5D__INCLUDED_)
#define AFX_DUTYGRID_H__4F6E9453_9035_11D3_9393_00001C033B5D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#define MAXLINKCOLORS 11
#include <DialogGrid.h>

class CDutyGrid : public CGridFenster  
{
public:
	//  Construction, Destruction
	CDutyGrid(CWnd *pParent);
	virtual ~CDutyGrid();

	//  Implementation
	void SetExpanded(bool bpExpanded);
	bool IsExpanded() { return bmExpanded; };
	virtual void IniLayout();
	virtual void DisplayRudTmp (bool bpInitLinkColors = true);
	virtual bool DisplayDemandGroup ( CString &ropUdgr );
	void SetEnabledParam(BOOL bpState);
	BOOL ResetContent();
	bool GetSelectionInfo ( CString &ropUdgr, CString &ropUrud ) ;
	bool OnDeleteRudTmp ( RecordSet *popRecord );
	virtual bool OnNewRudTmp ( RecordSet *popRecord );
	BOOL SetCurrentCell(ROWCOL nRow, ROWCOL nCol, UINT flags = GX_SCROLLINVIEW | GX_UPDATENOW);
	int GetResTypeForSelection ();
	bool MarkLine ( CString opDgrUrno, CString opRudUrno="" );
	long CreateLinkBkColor(CString opUlnk);
	long GetLinkBkColor(CString opUlnk);
	void ResetLinkColorIndex();
	void OnDeleteLink(CString opUlnk);
	void GetSelectedUrnos(CStringArray &ropUrnoList);


protected:
	//void OnInitCurrentCell(ROWCOL nRow, ROWCOL nCol);
	virtual BOOL OnRButtonClickedRowCol(ROWCOL nRow, ROWCOL nCol, UINT nFlags, CPoint pt);
	virtual BOOL OnLButtonDblClkRowCol(ROWCOL nRow, ROWCOL nCol, UINT nFlags, CPoint pt);
	//  Data members
	bool bmExpanded ;

private:
	// background color for group links
	int imActiveLinkColorIdx;
	CMapStringToPtr	omUsedLinkColors;
	long rmLinkColors[MAXLINKCOLORS];


};

class CColDutyGrid : public CDutyGrid
{
public:
	CColDutyGrid(CWnd *pParent);
	void DisplayRudTmp(bool bpInitLinkColors = true );
	void IniLayout();
	bool DisplayDemandGroup ( CString &ropUdgr );
	bool GetSingleRules ( CString &ropUrud, CString &ropRuty0Codes );
	//bool OnNewRudTmp ( RecordSet *popRecord );

};

#endif // !defined(AFX_DUTYGRID_H__4F6E9453_9035_11D3_9393_00001C033B5D__INCLUDED_)
