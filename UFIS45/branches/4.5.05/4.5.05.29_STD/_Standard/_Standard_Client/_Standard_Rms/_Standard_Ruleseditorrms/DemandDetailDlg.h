// DemandDetailDlg.h : header file
#if !defined(AFX_DEMANDDETAILDLG_H__B5AF6ED4_2D56_11D3_A63D_0000C007916B__INCLUDED_)
#define AFX_DEMANDDETAILDLG_H__B5AF6ED4_2D56_11D3_A63D_0000C007916B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


#include <RecordSet.h>
#include <DialogGrid.h>
#include <BarWindow.h>
#include <RulesGanttViewer.h>
#include <MainFrm.h>

class DemandDetailDlg : public CDialog
{
// Construction
public:
	DemandDetailDlg(CString opRudUrno, RulesGanttViewer* pViewer, CWnd* pParent = NULL);   // standard constructor
	~DemandDetailDlg();

	void Create();
	 void Destroy();


// Implementation
public:
	void InitData(CString opRueUrno, CString opEvty, CString opRudUrno);
	
	void SetControls();
	void SetIOTRadioButtons();
	void EnableIOTRadioButtons(CString opEvty);
	void SetTimeValues();
	void SetStaticTexts();
	
	// bool DemandDetailDlg::AppendValue(RecordSet *popRud, CString opField, int ipDiff);
	// void MoveTargetBars(int ipWhatChanged, CString opCellText, RecordSet *popSource = NULL);

	void TablesFirstTimeInit();
	void SetTablesForStaffView();
	void SetTablesForEquipmentView();
	void SetTablesForLocationsView();
	void ClearRightTables();

	
	void MakeBarWindow();
	void UpdateListEntries(int ipIOT);
	void ProcessRudTmpChange(RecordSet *popRud);
	void ProcessRudTmpDelete(RecordSet *popRud);
	static void SelectAloc (CComboBox *popAlocCb, CString &ropRudUrno );
	static void IniAlocCB (CComboBox *popAlocCb, bool bpAddEmptyString=false );
	void IniVrefCB();

//	bool IsSingleResourceSelected ( ROWCOL ipRow, ROWCOL ipCol,
//									CGridFenster *popGrid,
//									CString opTable, CString &ropCode );
	CString GetCurrentRud();
	void ProcessSgrNew(RecordSet *popRecord);
	void ProcessSgrDelete(RecordSet *popRecord);


// attributes
public:
	CWnd *pomParent;
	BarWindow *pomBarWnd;
	RulesGanttViewer *pomGanttViewer;

	CString omRudUrno;
	CString omCurrRueUrno;
	CString omRefTab, omRefDsp, omRefVal;

	CGridFenster *pomTimesGrid;
	CGridFenster *pomLeftGrid;
	CGridFenster *pomRightGrid;

	CMainFrame *pomFrameWnd;
	CComboBox *pomTemplateCombo;
	
	int imRessourceType;	// (0 = Funct/Quali, 1 = Equipm., 2 = Locations)
	int imDiff;
	bool bmDestroying;

	//{{AFX_DATA(DemandDetailDlg)
	enum { IDD = IDD_DEMAND_DETAIL_DLG };
	CComboBox	m_Cob_Reference;
	CButton	m_Btn_Close;
	CComboBox	m_AlocCB;
	CEdit	m_Edt_ShortName;
	CEdit	m_Edt_Name;
	CEdit	m_Edt_Lifr;
	CEdit	m_Edt_Lito;
	CEdit	m_Edt_SplitMin;
	CEdit	m_Edt_SplitMax;
	CButton	m_Chb_Splittable;
	CButton	m_Chb_Partial;
	CButton	m_Chb_Operative;
	CButton	m_Chb_Calculated;
	CButton m_RBt_Inbound;
	CButton m_RBt_Outbound;
	CButton m_RBt_Turnaround;
	// CButton	m_Chb_Standard;
	// CButton	m_Chb_Activated;
	// CButton	m_Chb_Contractable;
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(DemandDetailDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void OnCancel();
	//}}AFX_VIRTUAL

// Generated message map functions
	//{{AFX_MSG(DemandDetailDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnEndEditing(WPARAM wParam, LPARAM lParam);
	afx_msg void OnInbound();
	afx_msg void OnOutbound();
	afx_msg void OnTurnaround();
	afx_msg void OnChbCalculated();
	afx_msg void OnChbContract();
	afx_msg void OnChbOperative();
	afx_msg void OnChbPartial();
	afx_msg void OnChbSplit();
	afx_msg void OnChbStandard();
	afx_msg void OnClose();
	afx_msg void OnChbActi();
	afx_msg void OnDestroy();
	afx_msg void OnSelendokCobAlod();
	afx_msg void OnBtnCollective();
	afx_msg void OnSelendokCobVref();
	afx_msg void OnChangeEdtSplitmax();
	afx_msg void OnChangeEdtSplitmin();
	//}}AFX_MSG
	afx_msg void OnGridRButton(WPARAM wParam, LPARAM lParam);
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DEMANDDETAILDLG_H__B5AF6ED4_2D56_11D3_A63D_0000C007916B__INCLUDED_)
