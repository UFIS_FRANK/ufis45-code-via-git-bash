// BarWindow.cpp : implementation file
//

#include <stdafx.h>
#include <BarWindow.h>
#include <CCSGlobl.h>
#include <Regelwerk.h>
#include <CCSEdit.h>
#include <BasicData.h>
#include <CedaBasicData.h>




#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


#define IDC_LEFT_EDIT	   47110813
#define IDC_RIGHT_EDIT	   47110814
#define	IDC_LEFT_COMBO     47110815
#define	IDC_RIGHT_COMBO    47110816




//-------------------------------------------------------------------------------------------------------------------------
//					construction / destruction
//-------------------------------------------------------------------------------------------------------------------------
BarWindow::BarWindow(CString opUrno, CRect opRect, CWnd *pParent /* =NULL */)
{
	CCS_TRY

	bmFirstTimeInit = true;

	pomParent = pParent;
	omRect = opRect;
	omRudUrno = opUrno;

	pomLeftList = new CComboBox;
	pomRightList = new CComboBox;
	
	pomLeftEdit = new CCSEdit;
	pomRightEdit = new CCSEdit;

	smLeftMinTxtPt	= CPoint(0,0);
	smRightMinTxtPt	= CPoint(0,0);

	imPrepX	= 0;
	imWaytX = 0;
	imDeduX = 0;
	imWayfX = 0;
	imFuptX = 0;
	imTextY = 0;
	imTsdb  = 0;
	imTsde  = 0;

	imPrep = 0;
	imWayt = 0;
	imDedu = 0;
	imWayf = 0;
	imFupt = 0;
	
	bmFirstTimeInit = true;

	imRudType = 0;	// 0 = turnaround, 1 = inbound, 2 = outbound
	imRueType = 0;  // 0 = turnaround, 1 = inbound, 2 = outbound

	CreateBarWindow();

	CCS_CATCH_ALL
}
//-------------------------------------------------------------------------------------------------------------------------

BarWindow::~BarWindow()
{
	delete pomLeftList;
	delete pomRightList;
	delete pomLeftEdit;
	delete pomRightEdit;
}



//-------------------------------------------------------------------------------------------------------------------------
//					creation
//-------------------------------------------------------------------------------------------------------------------------
void BarWindow::CreateBarWindow()
{
	Create("STATIC", "BarWindow", WS_CHILD | WS_VISIBLE/* | WS_CAPTION*/, omRect, pomParent, 4711);
}



//-------------------------------------------------------------------------------------------------------------------------
//					message map
//-------------------------------------------------------------------------------------------------------------------------
BEGIN_MESSAGE_MAP(BarWindow, CWnd)
	//{{AFX_MSG_MAP(BarWindow)
	ON_CBN_SELENDOK(IDC_LEFT_COMBO, OnSelChangeLeft)
	ON_CBN_SELENDOK(IDC_RIGHT_COMBO, OnSelChangeRight)
	ON_MESSAGE(WM_EDIT_KILLFOCUS, OnEditKillFocus)
	ON_MESSAGE(WM_ADAPTTIME, OnAdaptTime)
	ON_WM_ERASEBKGND()
	ON_WM_DESTROY()
	ON_WM_CREATE()
	ON_WM_PAINT()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()




//-------------------------------------------------------------------------------------------------------------------------
//					message handling
//-------------------------------------------------------------------------------------------------------------------------
int BarWindow::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	CCS_TRY

	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	CRect olRect(10,10,15,15);
	
	pomLeftList->Create(/*CBS_SORT | */CBS_DROPDOWNLIST | WS_VSCROLL, olRect, this, 47110815);
	pomRightList->Create(/*CBS_SORT | */CBS_DROPDOWNLIST | WS_VSCROLL, olRect, this, 47110816);	

	pomLeftEdit->CreateEx(WS_EX_CLIENTEDGE, "EDIT", "0", WS_VISIBLE | WS_CHILD | ES_RIGHT | ES_AUTOHSCROLL,
							olRect, this, 47110813, NULL);
	pomRightEdit->CreateEx(WS_EX_CLIENTEDGE, "EDIT", "0", WS_VISIBLE | WS_CHILD | ES_RIGHT | ES_AUTOHSCROLL,
							olRect, this, 47110814, NULL);

	// SetListBoxSel();
	
	CCS_CATCH_ALL

	return 0;
}
//-------------------------------------------------------------------------------------------------------------------------

void BarWindow::OnDestroy() 
{
	CWnd::OnDestroy();
	
	delete this;
}
//-------------------------------------------------------------------------------------------------------------------------

void BarWindow::OnPaint() 
{
	// **********	DO NOT REMOVE THIS FUNCTION		************
	// Without this, OnEraseBkgnd won't be called !

	CPaintDC dc(this); 
	
	// Do not call CWnd::OnPaint() for painting messages
}
//-------------------------------------------------------------------------------------------------------------------------

BOOL BarWindow::OnEraseBkgnd(CDC* pDC) 
{
	//--- draw white background rect
	CRect olRect;
	GetClientRect(olRect);
	pDC->FillRect(olRect, ogBrushs[WHITE_IDX]);
	pDC->FrameRect(olRect, ogBrushs[BLACK_IDX]);
	
	//--- calulate maximum bar rect
	CRect olBarRect;
	olBarRect.bottom = olRect.bottom - 35;
	olBarRect.left = olRect.left + 45;
	olBarRect.top = olRect.top + 35;
	olBarRect.right = olRect.right - 45;

	//--- calculate rect between lines
	CRect olInnerRect;
	olInnerRect.bottom = olBarRect.bottom + 10;
	olInnerRect.top = olBarRect.top - 10;
	int ilBarWidth = olBarRect.Width();
	olInnerRect.left = olBarRect.left + (ilBarWidth /5 );
	olInnerRect.right = olBarRect.right - (ilBarWidth / 5 );

	//  calculate setup,wayto etc.
	int ilMaxtime = max ( imPrep+imWayt, imWayf+imFupt);
	if ( ilMaxtime < 20 )
		ilMaxtime = 20;
	float flFactor = (float)(ilBarWidth/5)/(float)ilMaxtime ;
	olBarRect.left = olInnerRect.left - 
					(int)(flFactor * (float)(imPrep+imWayt) );
	olBarRect.right = olInnerRect.right + 
					(int)(flFactor * (float)(imWayf+imFupt) );
	int ilWayfromPos = olInnerRect.right + (int)(flFactor * (float)(imWayf) );
	int ilWaytoPos = olInnerRect.left - (int)(flFactor * (float)(imWayt) );

	//--- draw bar rect
	pDC->FillRect(olBarRect, ogBrushs[GREEN_IDX]);
	pDC->FrameRect(olBarRect, ogBrushs[BLACK_IDX]);

	if (bmFirstTimeInit == true)
	{
		bmFirstTimeInit = false;
		DrawListBoxes(olInnerRect);
		DrawEditControls(olInnerRect);
	}
	// get coordinates for TextOut (takes place in DrawBarTimes())
	imTextY = (olRect.bottom + olBarRect.bottom) / 2;
	imPrepX = ( olBarRect.left +  ilWaytoPos ) / 2;
	imWaytX = ( ilWaytoPos + olInnerRect.left ) / 2;
	imDeduX = olBarRect.left + ilBarWidth / 2;
	imWayfX = (ilWayfromPos + olInnerRect.right) / 2;
	imFuptX = (olBarRect.right + ilWayfromPos) / 2;
	
	//--- draw time lines
	CPen olThinPen(PS_SOLID, 1, BLACK);
	CPen *polOldPen = NULL;
	polOldPen = pDC->SelectObject(&olThinPen);
	
	//  int ilOffset = (olBarRect.Width() - olInnerRect.Width()) / 4; 
	pDC->MoveTo(ilWaytoPos, olBarRect.top);
	pDC->LineTo(ilWaytoPos, olBarRect.bottom);
	pDC->MoveTo(ilWayfromPos, olBarRect.top);
	pDC->LineTo(ilWayfromPos, olBarRect.bottom);
	
	DrawReferenceTimeLines(pDC, olInnerRect);
	pDC->SelectObject(polOldPen);

	//--- display time information
	DrawBarTimes(pDC, olBarRect);
	
	//---  display "min."
	CString olLeftStr, olRightStr;
	if (pomLeftEdit->IsWindowVisible() == TRUE)
	{
		olLeftStr = CString("min.");
	}
	else
	{
		olLeftStr = CString("     ");
	}
	
	if (pomRightEdit->IsWindowVisible() == TRUE)
	{
		olRightStr = CString("min.");
	}
	else
	{
		olRightStr = CString("     ");
	}
	
	if ( smLeftMinTxtPt.x > 0 )
	{
		COLORREF ilTxtCol = pDC->GetTextColor();
		pDC->SetTextColor(RGB(96,96,96));
		pDC->TextOut(smLeftMinTxtPt.x, smLeftMinTxtPt.y, olLeftStr);
		pDC->TextOut(smRightMinTxtPt.x, smRightMinTxtPt.y, olRightStr);
		pDC->SetTextColor(ilTxtCol);
	}
	return CWnd::OnEraseBkgnd(pDC);
}

//-------------------------------------------------------------------------------------------------------------------------

// SelChange MsgHandler for left list box
void BarWindow::OnSelChangeLeft() 
{
	RecordSet olRecord(ogBCD.GetFieldCount("RUD_TMP"));
	ogBCD.GetRecord("RUD_TMP", "URNO", omRudUrno, olRecord);
	int ilSel = pomLeftList->GetCurSel();

	if (olRecord.Values[igRudDrtyIdx] == CString("2"))
	{
		olRecord.Values[igRudRtdbIdx] = ogOutboundReferences[ilSel].Field;
	}
	else
	{
		olRecord.Values[igRudRtdbIdx] = ogInboundReferences[ilSel].Field;
	}
	
	CString olTest = olRecord.Values[igRudRtdbIdx];
	bool blTest = ogBCD.SetRecord("RUD_TMP", "URNO", omRudUrno, olRecord.Values);
}
//-------------------------------------------------------------------------------------------------------------------------

// SelChange MsgHandler for right list box
void BarWindow::OnSelChangeRight() 
{
	RecordSet olRecord(ogBCD.GetFieldCount("RUD_TMP"));
	ogBCD.GetRecord("RUD_TMP", "URNO", omRudUrno, olRecord);
	int ilSel = pomRightList->GetCurSel();

	if (olRecord.Values[igRudDrtyIdx] == CString("1"))
	{
		olRecord.Values[igRudRtdeIdx] = ogInboundReferences[ilSel].Field; 
	}
	else
	{
		olRecord.Values[igRudRtdeIdx] = ogOutboundReferences[ilSel].Field; 
	} 

	bool blTest = ogBCD.SetRecord("RUD_TMP", "URNO", omRudUrno, olRecord.Values); 
}
//-------------------------------------------------------------------------------------------------------------------------

// KillFocus MsgHandler for both, left and right, edit controls
void BarWindow::OnEditKillFocus(WPARAM wParam, LPARAM lParam)
{
	CCS_TRY

	CCSEDITNOTIFY *prlNotify;
	prlNotify = (CCSEDITNOTIFY*) lParam;

	UINT ilID = (UINT) wParam;
	
	RecordSet olRecord;
	ogBCD.GetRecord("RUD_TMP", "URNO", omRudUrno, olRecord);
	
	CString olValue;
	olValue = prlNotify->Text;
	int ilValue = atoi(olValue) * 60;
	int ilDiff = 0;
	
	// warn user if value too big (bar would move out of window)
	if (IfBigValueWarnUser(ilValue) == false)
	{
		return;
	}
	
	// handle changes
	if (::IsWindow(pomLeftEdit->GetSafeHwnd()))
	{
		if (prlNotify->SourceControl == pomLeftEdit)
		{
			if (pomLeftEdit->GetStatus() == true)
			{
				MustLimitOffsetTo24Hours(ilValue, pomLeftEdit);

				if (IsMovingOK(&olRecord) == true)
				{
					ilDiff = ilValue - imTsdb;
					imTsdb = ilValue;
					olRecord.Values[igRudTsdbIdx].Format("%d", ilValue);
					SetFollowingTimeIntervals(&olRecord, ilDiff);
				}
				else
				{
					int ilText = atoi(olRecord.Values[igRudTsdbIdx]);
					CString olText;
					olText.Format("%d", ilValue / 60);
					pomRightEdit->SetWindowText(olText);
				}
			}
		}
	}
	
	if (::IsWindow(pomRightEdit->GetSafeHwnd()))
	{
		if (prlNotify->SourceControl == pomRightEdit)
		{
			if (pomRightEdit->GetStatus() == true)
			{
				MustLimitOffsetTo24Hours(ilValue, pomRightEdit);

				if (IsMovingOK(&olRecord) == true)
				{
						ilDiff = ilValue - imTsde;
						imTsde = ilValue;
						olRecord.Values[igRudTsdeIdx].Format("%d", ilValue);
						SetFollowingTimeIntervals(&olRecord, ilDiff);
				}
				else
				{
					int ilText = atoi(olRecord.Values[igRudTsdeIdx]);
					CString olText;
					olText.Format("%d", ilValue / 60);
					pomRightEdit->SetWindowText(olText);
				}
			}
		}
	}

	ogBCD.SetRecord("RUD_TMP", "URNO", omRudUrno, olRecord.Values);
	ogDdx.DataChanged((void *)this,RUD_CON_CHANGE,(void *)&olRecord);

	CCS_CATCH_ALL
}
//-------------------------------------------------------------------------------------------------------------------------

void BarWindow::OnAdaptTime(WPARAM wParam, LPARAM lParam)
{
	CCS_TRY

	RecordSet *polRud = NULL;
	polRud = (RecordSet *) lParam;

	if (polRud != NULL)
	{
		// length of a turnaround demand requirement is undefined
		imRudType = atoi(polRud->Values[igRudDrtyIdx]);
		imDedu = (imRudType!=0) ? atoi(polRud->Values[igRudDeduIdx])/60 : 0;

		imWayt = atoi(polRud->Values[igRudTtgtIdx])/60;	// way there
		imPrep = atoi(polRud->Values[igRudSutiIdx])/60;  // preparation time
		imWayf = atoi(polRud->Values[igRudTtgfIdx])/60;  // way back
		imFupt = atoi(polRud->Values[igRudSdtiIdx])/60;  // follow-up treatment

		Invalidate(TRUE);
	}

	CCS_CATCH_ALL
}



//-------------------------------------------------------------------------------------------------------------------------
//					drawing methods
//-------------------------------------------------------------------------------------------------------------------------
void BarWindow::DrawReferenceTimeLines(CDC *pDC, CRect opRect)
{
	CCS_TRY

	CPen olThickPen(PS_SOLID, 2, BLACK);
	CPen olThinPen(PS_SOLID, 1, BLACK);
	CPen *polOldPen;

	polOldPen = pDC->SelectObject(&olThickPen);
	

	//--- get corners of rect between lines
	CPoint olTopLeft;
	CPoint olBottomRight;
	olTopLeft = opRect.TopLeft();
	olBottomRight = opRect.BottomRight();	

	//--- draw Onblock line
	if (imRudType != 2)
	{
		pDC->MoveTo(olTopLeft);
		pDC->LineTo(olTopLeft.x, olBottomRight.y);
	}
	else
	{
		pDC->SelectObject(&olThinPen);
		pDC->MoveTo(olTopLeft.x, olTopLeft.y + 10);
		pDC->LineTo(olTopLeft.x, olBottomRight.y - 11);
	}
	
	//--- draw OffBlock line
	if (imRudType != 1)
	{
		pDC->SelectObject(&olThickPen);
		pDC->MoveTo(olBottomRight);
		pDC->LineTo(olBottomRight.x, olTopLeft.y);
	}
	else
	{
		pDC->SelectObject(&olThinPen);
		pDC->MoveTo(olBottomRight.x, olBottomRight.y - 11);
		pDC->LineTo(olBottomRight.x, olTopLeft.y + 10);
	}
	

	pDC->SelectObject(polOldPen);

	CCS_CATCH_ALL
}
//-------------------------------------------------------------------------------------------------------------------------

void BarWindow::DrawBarTimes(CDC *pDC, CRect opRect)
{
	CFont *polOldFont = NULL;
	polOldFont = pDC->SelectObject(&ogCourier_Regular_10);
	COLORREF olOldColor = pDC->SetTextColor(BLACK);
	CString olTxt;
	if ( imPrep > 0 )
	{
		olTxt.Format ("%d", imPrep );
		pDC->TextOut(imPrepX - pDC->GetTextExtent(olTxt).cx / 2, 
					 imTextY - pDC->GetTextExtent(olTxt).cy / 2, olTxt, olTxt.GetLength());
	}
	if ( imWayt > 0 )
	{
		olTxt.Format ("%d", imWayt );
		pDC->TextOut(imWaytX - pDC->GetTextExtent(olTxt).cx / 2, 
					 imTextY - pDC->GetTextExtent(olTxt).cy / 2, olTxt, olTxt.GetLength());
	}
	if ( imDedu > 0 )
	{
		olTxt.Format ("%d", imDedu );
		pDC->TextOut(imDeduX - pDC->GetTextExtent(olTxt).cx / 2, 
					 imTextY - pDC->GetTextExtent(olTxt).cy / 2, olTxt, olTxt.GetLength());
	}
	if ( imWayf > 0 )
	{
		olTxt.Format ("%d", imWayf );
		pDC->TextOut(imWayfX - pDC->GetTextExtent(olTxt).cx / 2,  
					 imTextY - pDC->GetTextExtent(olTxt).cy / 2, olTxt, olTxt.GetLength());
	}
	if ( imFupt > 0 )
	{
		olTxt.Format ("%d", imFupt );
		pDC->TextOut(imFuptX - pDC->GetTextExtent(olTxt).cx / 2,  
					 imTextY - pDC->GetTextExtent(olTxt).cy / 2, olTxt, olTxt.GetLength());
	}
	pDC->SetTextColor(olOldColor);
	pDC->SelectObject(polOldFont);
}
//-------------------------------------------------------------------------------------------------------------------------

void BarWindow::DrawListBoxes(CRect opRect)
{
	//--- calculate rect for left listbox
	CRect olLeftRect;
	olLeftRect.right = opRect.left + 110;
	olLeftRect.top = opRect.top - 20;
	olLeftRect.bottom = opRect.top + 100;
	olLeftRect.left = opRect.left + 15;

	//--- calculate rect for right listbox
	CRect olRightRect;
	olRightRect.right = opRect.right + 110;
	olRightRect.top = opRect.top  - 20;
	olRightRect.bottom = opRect.top + 100;
	olRightRect.left = opRect.right + 15;

	pomLeftList->MoveWindow(olLeftRect);
	pomRightList->MoveWindow(olRightRect);
}
//-------------------------------------------------------------------------------------------------------------------------

void BarWindow::DrawEditControls(CRect opRect)
{
	//--- calculate rect for left edit control
	CRect olLeftRect;
	olLeftRect.right = opRect.left - 25;//-15
	olLeftRect.top = opRect.top - 20;
	olLeftRect.bottom = opRect.top + 3;
	olLeftRect.left = opRect.left - 70;//-50

	//--- calculate rect for right edit control
	CRect olRightRect;
	olRightRect.right = opRect.right - 25;//-15
	olRightRect.top = opRect.top - 20;
	olRightRect.bottom = opRect.top + 3;
	olRightRect.left = opRect.right - 70;//-50

	pomLeftEdit->MoveWindow(olLeftRect);
	pomRightEdit->MoveWindow(olRightRect);

	//--- calculate point for left "min."-display
	smLeftMinTxtPt.x = olLeftRect.right + 2;
	smLeftMinTxtPt.y = olLeftRect.top + 3;
	
	//--- calculate point for right "min."-display
	smRightMinTxtPt.x = olRightRect.right + 2;
	smRightMinTxtPt.y = olRightRect.top + 3;

}

//-------------------------------------------------------------------------------------------------------------------------
//					helper functions
//-------------------------------------------------------------------------------------------------------------------------

// bar can not be moved if it has a predecessor connected to it
bool BarWindow::IsMovingOK(RecordSet *popRud)
{
	if ((popRud->Values[igRudUpdeIdx].IsEmpty() == FALSE) && 
		(popRud->Values[igRudUpdeIdx] != CString(" ")))
	{
		Beep(800, 200);
		return false;
	}

	return true;
}



//-------------------------------------------------------------------------------------------------------------------------
//					set functions
//-------------------------------------------------------------------------------------------------------------------------
void BarWindow::SetControlsEnabled(drool ipLeftEdit, drool ipLeftList, drool ipRightEdit, drool ipRightList)
{
	if (ipLeftEdit != SKIP)
	{
	    if (ipLeftEdit == NO)
		{
			pomLeftEdit->ShowWindow(SW_HIDE);
		}
		else
		{
			pomLeftEdit->ShowWindow(SW_SHOW);
		}
	}
	
	if (ipLeftList != SKIP)
	{
		if (ipLeftList == NO)
		{
			pomLeftList->ShowWindow(SW_HIDE);
		}
		else
		{
			pomLeftList->ShowWindow(SW_SHOW);    
		}
	}

	if (ipRightEdit != SKIP)
	{
		if (ipRightEdit == NO)
		{
			pomRightEdit->ShowWindow(SW_HIDE);
		}
		else
		{
			pomRightEdit->ShowWindow(SW_SHOW);
		}
	}
	
	if (ipRightList != SKIP)
	{
		if (ipRightList == NO)
		{
			pomRightList->ShowWindow(SW_HIDE);
		}
		else
		{
			pomRightList->ShowWindow(SW_SHOW);
		}
	}

	Invalidate();
}
//-------------------------------------------------------------------------------------------------------------------------

void BarWindow::SetListBoxSel()
{
	RecordSet olRecord(ogBCD.GetFieldCount("RUD_TMP"));
	ogBCD.GetRecord("RUD_TMP", "URNO", omRudUrno, olRecord);

	CString olLeftRef;
	CString olRightRef;
	CString olDrty;
	CString olTsdb;
	CString olTsde;

	olLeftRef = olRecord.Values[igRudRtdbIdx];
	olRightRef = olRecord.Values[igRudRtdeIdx];
	olDrty =  olRecord.Values[igRudDrtyIdx];

	//  Sec (database) ==> Min (display)
	int ilValue = atoi(olRecord.Values[igRudTsdbIdx]) / 60;
	olTsdb.Format ( "%d", ilValue);
	ilValue = atoi(olRecord.Values[igRudTsdeIdx]) / 60;
	olTsde.Format ( "%d", ilValue);

	SetTimeIntervals(olTsdb, olTsde);

	
	if (olRecord.Values[igRudUpdeIdx].IsEmpty() == FALSE && olRecord.Values[igRudUpdeIdx] != CString(" "))
	{
		// bar can't be moved if it has a predecessor connected to it
		SetControlsEnabled(NO, NO, NO, NO);
	}
	else
	{
		bool blFound = false;
		if (olDrty != CString("2") && omEvty != CString("2"))
		{
			SetControlsEnabled(YES, YES, SKIP, SKIP);
			for (int i = 0; i < ogInboundReferences.GetSize(); i++)
			{
				if (ogInboundReferences[i].Field == olLeftRef)
				{
					if (pomLeftList->SelectString(-1, ogInboundReferences[i].Name) == CB_ERR)
					{
						pomLeftList->SetCurSel(0);
					}
					else
					{
						blFound = true;
					}
				}
			}
			
			if (blFound == false)
			{
				pomLeftList->SetCurSel(0);
			}
		}
		else
		{
			SetControlsEnabled(NO, NO, SKIP, SKIP);
		}
	

		if (olDrty != CString("1"))
		{
			blFound = false;
			SetControlsEnabled(SKIP, SKIP, YES, YES);
			for (int i = 0; i < ogOutboundReferences.GetSize(); i++)
			{
				if (ogOutboundReferences[i].Field == olRightRef)
				{
					if (pomRightList->SelectString(-1, ogOutboundReferences[i].Name) == CB_ERR)
					{
						pomRightList->SetCurSel(0);
					}
				}
				else
				{
					blFound = true;
				}
			}
			if (blFound == false)
			{
				pomRightList->SetCurSel(0);
			}
		}
		else
		{
			SetControlsEnabled(SKIP, SKIP, NO, NO);
		}
	}
}
//-------------------------------------------------------------------------------------------------------------------------

void BarWindow::SetTimeIntervals(CString opTsdb, CString opTsde)
{
	imTsdb = atoi(opTsdb) * 60;
	imTsde = atoi(opTsde) * 60;
	pomLeftEdit->SetWindowText(opTsdb);
	pomRightEdit->SetWindowText(opTsde);
}
//-------------------------------------------------------------------------------------------------------------------------

// If a bar is moved all following connected bars also must be moved
// DON'T FORGET to call DDX for update (see comment at the end of 
// this function !
void BarWindow::SetFollowingTimeIntervals(RecordSet *popSource, int ipDiff)
{
	CString olNextBarUrno;
	CString olConType;

	olNextBarUrno = popSource->Values[igRudUndeIdx];
	olConType = popSource->Values[igRudRendIdx];
		
	if (olNextBarUrno.IsEmpty() == FALSE && olNextBarUrno != CString(" "))	// any further connection ?
	{
		RecordSet olTarget;
		ogBCD.GetRecord("RUD_TMP", "URNO", olNextBarUrno, olTarget);

		int ilTargetBarType = 1;
		int ilDummy;
		if ( sscanf(olTarget.Values[igRudDrtyIdx], "%d", &ilDummy) >= 1 )
			ilTargetBarType = ilDummy;
		int ilTmp = 0;
		
		// turnaround bars can not be target of a connection, so we don't 
		// have to handle this case
		if (ilTargetBarType == 1)	// inbound
		{
			ilTmp = atoi(olTarget.Values[igRudTsdbIdx]);
			olTarget.Values[igRudTsdbIdx].Format("%d", ilTmp + ipDiff);
		}
		else if (ilTargetBarType == 2)	// outbound
		{
			ilTmp = atoi(olTarget.Values[igRudTsdeIdx]);
			olTarget.Values[igRudTsdeIdx].Format("%d", ilTmp + ipDiff);
		}
		
		ogBCD.SetRecord("RUD_TMP", "URNO", olTarget.Values[igRudUrnoIdx], olTarget.Values);

		// follow the chain recursively
		SetFollowingTimeIntervals(&olTarget, ipDiff);
	}

	// ATTENTION: In the calling function, you need to send a message like the one below.
	//            I didn't put it here, because it needn't be sent more
	//			  than once, which happens when this function is called recursively.
	//
	//			  ogDdx.DataChanged((void *)this,RUD_CON_CHANGE,(void *)&olRecord);
	
}
//-------------------------------------------------------------------------------------------------------------------------

void BarWindow::SetCurrentData(CString opRud, CString opEvty)
{
	RecordSet olRecord;
	omRudUrno = opRud;
	if ( ogBCD.GetRecord ("RUD_TMP", "URNO", omRudUrno, olRecord) )
	{	
		imRudType = atoi(olRecord.Values[igRudDrtyIdx]);
		imWayt = atoi(olRecord.Values[igRudTtgtIdx])/60;	// way there
		imPrep = atoi(olRecord.Values[igRudSutiIdx])/60;  // preparation time
		imWayf = atoi(olRecord.Values[igRudTtgfIdx])/60;  // way back
		imFupt = atoi(olRecord.Values[igRudSdtiIdx])/60;  // follow-up treatment
		imDedu = (imRudType!=0) ? atoi(olRecord.Values[igRudDeduIdx])/60 : 0;
	}
	omEvty = opEvty;

	// update controls
	SetListBoxSel();
}

//---------------------------------------------------
// displays warning message when user enters a number 
// in one of the offset edit controls that is so large
// that the bar would in consequence move out of the Gantt chart
// IN:  (by reference) value to check 
// OUT:	true  => continue (move bar)
//		false => undo user action
//---------------------------------------------------
bool BarWindow::IfBigValueWarnUser(int ipValue)
{
	bool blRet = true;

	// warn user that bar might irreversibly disappear
	if ((ipValue > 28800) || (ipValue < -28800))
	{
		if (MessageBox(GetString(1685),GetString(1684),MB_YESNO | MB_ICONEXCLAMATION | MB_ICONWARNING) == IDNO)
		{
			blRet = false;
		}
	}

	return blRet;
}

//---------------------------------------------------
// limit offset value to 24 hours at most
// IN:  int value
// OUT: value (by reference, changed only if > 86400)
//      bool: true if changed, false if unchanged
//---------------------------------------------------
void BarWindow::MustLimitOffsetTo24Hours(int &ripValue, CCSEdit *popEdit)
{
	CString olVal;

	if (ripValue > 86400)
	{
		ripValue = 86400;
		olVal.Format("%d", ripValue/60);
		popEdit->SetWindowText(olVal);
	}
	else if (ripValue < -86400)
	{
		ripValue = -86400;
		olVal.Format("%d", ripValue/60);
		popEdit->SetWindowText(olVal);					
	}
}
//-------------------------------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------------




