// NewDlg.cpp : implementation file
//

#include <stdafx.h>
#include <regelwerk.h>
#include <NewDlg.h>
#include <ccsglobl.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// NewDlg dialog


NewDlg::NewDlg(CWnd* pParent /*=NULL*/)
	: CDialog(NewDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(NewDlg)
	m_WhatNew = 0;
	//}}AFX_DATA_INIT
}


void NewDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(NewDlg)
	DDX_Control(pDX, IDC_SINGLE, m_Opt_SingleRule);
	DDX_Control(pDX, IDOK, m_OKBtn);
	DDX_Radio(pDX, IDC_SINGLE, m_WhatNew);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(NewDlg, CDialog)
	//{{AFX_MSG_MAP(NewDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()



BOOL NewDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	SetDlgItemLangText (this, IDC_SINGLE, 1401 );
	SetDlgItemLangText (this, IDC_COLLECTIVE, 1402 );
	SetDlgItemLangText (this, IDC_FLIGHTINDEP, 1403 );
	SetWindowLangText ( this, IDS_NEW_RULE_TIT );
	//m_Opt_SingleRule.SetCheck(CHECKED);
	m_OKBtn.SetFocus();
	
	return TRUE;  
}


void NewDlg::OnOK() 
{
	CDialog::OnOK();
}

void NewDlg::OnCancel() 
{
	CDialog::OnCancel();
}

