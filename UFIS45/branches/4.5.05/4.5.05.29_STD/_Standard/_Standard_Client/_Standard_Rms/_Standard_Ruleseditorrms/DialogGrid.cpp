// DialogGrid.cpp : implementation file
//

#include <stdafx.h>
#include <Regelwerk.h>
#include <RegelWerkView.h>
#include <DialogGrid.h>
#include <BasicData.h>
#include <Dialog_ExpressionEditor.h>
#include <Dialog_VorlagenEditor.h>
#include <RegexEdit.h>

#include <CedaBasicData.h>
#include <BasicData.h>
#include <CCSGlobl.h>
#include <gxttn.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


#ifdef _DEBUG
#define INFO ::AfxTrace("%s(%i): ",__FILE__,__LINE__); ::AfxTrace
#else
#define INFO ((void)0)
#endif


//---------------------------------------------------------------------------------------------------------------------
//					defines, global functions, etc.
//---------------------------------------------------------------------------------------------------------------------

#define MIN_COL_WIDTH		20		// minimum column width for all grid columns


int StringComp(const RecordSet** ppRecord1, const RecordSet** ppRecord2);
int CompareStrings (const class CString **p1 ,const class CString **p2 );



//---------------------------------------------------------------------------------------------------------------------
//					construction / destruction
//---------------------------------------------------------------------------------------------------------------------
CGridFenster::CGridFenster()
{
	pomParent = NULL;
	bmSortNumerical = false;
	bmSortAscend = true;
	bmIsSorting = true;
	imSortKey = 1;
	imCurrentRow = -1;
	bmAutoGrow = false;
	bmIsGridEnabled = true;
	bmCopyStyle = true;
	bmCheckUserFormat = false;
}

CGridFenster::CGridFenster(CWnd *pParent)
{
	pomParent = pParent;
	bmSortNumerical = false;
	bmSortAscend = true;
	bmIsSorting = true;
	imSortKey  = 1;
	imCurrentRow = -1;
	bmAutoGrow = false;
	bmIsGridEnabled = true;
	bmCopyStyle = true;
	bmCheckUserFormat = false;
}
//---------------------------------------------------------------------------------------------------------------------

CGridFenster::~CGridFenster()
{
	omSpecialFormats.RemoveAll ();
}

//---------------------------------------------------------------------------------------------------------------------
//					message map
//---------------------------------------------------------------------------------------------------------------------
BEGIN_MESSAGE_MAP(CGridFenster, CGXGridWnd)
	//{{AFX_MSG_MAP(CGridFenster)
	ON_WM_LBUTTONUP()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


//---------------------------------------------------------------------------------------------------------------------
//					message handlers
//---------------------------------------------------------------------------------------------------------------------
void CGridFenster::OnLButtonUp(UINT nFlags, CPoint point) 
{
	//--
	// checks sorting
	//-- 

	CGXGridWnd ::OnLButtonUp(nFlags, point);

    ROWCOL  Row;
	ROWCOL  Col;

	//--- Betroffene Zelle ermitten
    //HitTest(point, &Row,  &Col, NULL);
    if ( HitTest ( point, &Row,  &Col, NULL ) != GX_HEADERHIT )
		return;

	//--- Check f�r StingRay Fehler
	if(Col > GetColCount())
		return;


	//--- Sortieren je nachdem welcher Header gedr�ckt wurde
	if(Row == 0 && Col != 0 && bmIsSorting)
	{
		CGXSortInfoArray  sortInfo;
		sortInfo .SetSize(1);
		
		// switch between sorting in ascending / descending order with each click
		if (bmSortAscend == true)
		{
			sortInfo[0].sortOrder = CGXSortInfo::descending;
			bmSortAscend = false;
		}
		else
		{
			sortInfo[0].sortOrder = CGXSortInfo::ascending;
			bmSortAscend = true;
		}
		
		sortInfo[0].nRC = Col;                       
		sortInfo[0].sortType = CGXSortInfo::autodetect;  

		int ilRowCount = GetRowCount ();
		if ( bmAutoGrow && GetValueRowCol ( ilRowCount, 1 ).IsEmpty () )
			ilRowCount--;
		//SortRows( CGXRange().SetTable(), sortInfo); 
		SortRows( CGXRange().SetRows(1,ilRowCount), sortInfo); 

		//--- Merke welche Spalte f�r sorting verwendet wurde
		imSortKey  = Col;

		//--- Check ob numerisch sortiert wurde 
		CGXStyle  il_Cell_Style;
		GetStyleRowCol(1, Col, il_Cell_Style);


		if(il_Cell_Style.GetValueType() ==  GX_VT_NUMERIC )
		  bmSortNumerical = true;
		else
		  bmSortNumerical = false;
		SendMsgToParent(GRID_MESSAGE_RESORTED, Row, Col );
	}
}
//---------------------------------------------------------------------------------------------------------------------

BOOL  CGridFenster::OnSelDragDrop (ROWCOL nStartRow, ROWCOL nStartCol, ROWCOL nDragRow, ROWCOL nDragCol)
{
	//--
	// check dragging of rows
	//--

	 //---  Verschieben der Zeilen nur wenn numerisch sortiert
	 if( bmSortNumerical == true)
	 {
	      CGXGridCore::OnSelDragDrop (nStartRow, nStartCol, nDragRow, nDragCol);

		  //--- Neu durchnumerieren 
		  SetReadOnly(false);

	        int  il_ilSize = GetRowCount();

	        for(int  i = 1; i <= il_ilSize; i++) 
                 SetValueRange(CGXRange( i , 1), (WORD) i);  

		    Redraw();

		  SetReadOnly(true);

		  return true;
     }		   
	 else
	 {
	    // AfxMessageBox(GetString(IDS_STRING_NO_MOVE));
 	    return false;

	 }
}
//---------------------------------------------------------------------------------------------------------------------

BOOL CGridFenster::OnTrackColWidth(ROWCOL nCol)
{
	//------------------------------------
	// overwritten virtual of CGXGridCore
	//------------------------------------

	if (IsColHidden(nCol) == TRUE)
		return FALSE;
	else
		return TRUE;

}
//---------------------------------------------------------------------------------------------------------------------

int CGridFenster::GetColWidth(ROWCOL nCol)
{
	//----------------------------------
	// test if cols can be hidden or not
	//----------------------------------

	int nRet = CGXGridCore::GetColWidth(nCol);
	
	for (int i = 0; i < omVisibleCols.GetSize(); i++)
	{
		int ilColNo = (int) omVisibleCols.GetAt(i);
		if ((int)nCol == ilColNo)
		{
			if (nRet < MIN_COL_WIDTH)
			{
				nRet = MIN_COL_WIDTH;
			}
			else
			{
				nRet = 0;
			}
		}
	}

	//-- columns 6 to 8 stay hidden (width = 0)
	/*if (nCol <= 5)
	{
		//-- first 5 columns' width will be set to at least MIN_COL_WIDTH
		if (nRet < MIN_COL_WIDTH)
		{
			nRet = MIN_COL_WIDTH;
		}
	}
	else
	{
		nRet = 0;
	}*/

	return nRet;
}
//------------------------------------------------------------------------------------------------------------------------

void  CGridFenster::OnClickedButtonRowCol(ROWCOL nRow, ROWCOL nCol) 
{
	//--------------------------------------
	// Click onto a Pushbutton inside the grid
	// ==> Message to parent dialog
	//--------------------------------------

	SendMsgToParent(GRID_MESSAGE_BUTTONCLICK, nRow, nCol );
}
//------------------------------------------------------------------------------------------------------------------------

BOOL  CGridFenster::OnLButtonClickedRowCol(ROWCOL nRow, ROWCOL nCol, UINT nFlags, CPoint pt) 
{
	//---
	// Click into a cell of the grid
	// ==> Message to parent dialog
	//---

	return SendMsgToParent(GRID_MESSAGE_CELLCLICK, nRow, nCol );
}
//------------------------------------------------------------------------------------------------------------------------

BOOL CGridFenster::OnLButtonDblClkRowCol(ROWCOL nRow, ROWCOL nCol, UINT nFlags, CPoint pt)
{
	//---
	// DoubleClick into a cell of the grid
	// ==> Message to parent dialog
	//---

	return SendMsgToParent(GRID_MESSAGE_DOUBLECLICK, nRow, nCol );
}
//------------------------------------------------------------------------------------------------------------------------

BOOL  CGridFenster::OnStartEditing(ROWCOL nRow, ROWCOL nCol)
{
	CCS_TRY

	//--- send message to parent dialog
	BOOL blRet = SendMsgToParent(GRID_MESSAGE_STARTEDITING, nRow, nCol );

	DisplayInfo (nRow, nCol, true);
	theApp.SetStatusBarText(CString(""));


	CCS_CATCH_ALL

	return blRet;
}
//------------------------------------------------------------------------------------------------------------------------

BOOL  CGridFenster::OnEndEditing(ROWCOL nRow, ROWCOL nCol)
{

/*	hag 000126  damit gel�schte Auswahl aus Combobox erkannt wird 
	
	// prevent grid from sending message when destroying 
	// the surrounding window

	CGXControl *polCellControl;
	polCellControl = GetControl(nRow, nCol);

	if (polCellControl != NULL)
	{
		if (polCellControl->GetModify() == FALSE)
		{
			return TRUE;	
		}
	}
*/

	//--- automatically add one line at bottom of grid ?
	if (bmAutoGrow == true)
	{
		DoAutoGrow ( nRow, nCol) ;
	}

	//--- send message to parent dialog
	BOOL blRet = SendMsgToParent(GRID_MESSAGE_ENDEDITING, nRow, nCol );
	DisplayInfo (nRow, nCol, true );
	theApp.SetStatusBarText ( CString("") );


	return blRet;
}



//---------------------------------------------------------------------------------------------------------------------
//					helper functions
//---------------------------------------------------------------------------------------------------------------------
void CGridFenster::DoAutoGrow (ROWCOL nRow, ROWCOL nCol) 
{
	ROWCOL ilRowCount = GetRowCount();
	if (bmAutoGrow && (nRow >= ilRowCount))
	{	
		//  es wird bereits in letzter Zeile editiert, d.h. evt. Grid verl�ngern
		if (!GetValueRowCol(nRow, nCol).IsEmpty())
			InsertBottomRow ();
	}
}
//---------------------------------------------------------------------------------------------------------------------

BOOL CGridFenster::InsertBottomRow()
{
	ROWCOL ilRowCount = GetRowCount();
	
	BOOL blRet = InsertRows(ilRowCount + 1, 1);

	if (bmCopyStyle == true)
	{
		CopyStyleLastLineFromPrev();
	}
	
	return blRet;
}
//---------------------------------------------------------------------------------------------------------------------

void CGridFenster::CopyStyleLastLineFromPrev ( bool bpResetValues/*=true*/)
{
	ROWCOL ilRowCount = GetRowCount();
	CopyStyleOfLine(ilRowCount - 1, ilRowCount, bpResetValues);
}
//---------------------------------------------------------------------------------------------------------------------

void CGridFenster::CopyStyleOfLine(ROWCOL ipSourceLine, ROWCOL ipDestLine, bool bpResetValues/*=true*/)
{
	ROWCOL ilColCount = GetColCount ();
	CGXStyle olStyle;

	for (ROWCOL i = 0; i <= ilColCount; i++)
	{
		olStyle.Free();
		ComposeStyleRowCol(ipSourceLine, i, &olStyle);
		SetStyleRange(CGXRange(ipDestLine, i), olStyle);
		if (bpResetValues)
		{
			SetValueRange (CGXRange(ipDestLine, i), "");
		}
	}
}
//---------------------------------------------------------------------------------------------------------------------

BOOL CGridFenster::ResetContent()
{
	BOOL blRet = TRUE;
	
	int ilCount = GetRowCount();
	if (ilCount > 0)
	{
		blRet &= RemoveRows(1, ilCount);
	}
	SetSelection(NULL);
	Redraw();

	return blRet;
}

void CGridFenster::OnModifyCell (ROWCOL nRow, ROWCOL nCol)
{
	CGXGridWnd::OnModifyCell(nRow, nCol);

	ROWCOL ilRowCount, ilColCount;
	ilRowCount = GetRowCount();
	ilColCount = GetColCount();

	if ( (nRow<=0) || (nRow>ilRowCount) || (nCol<=0) || (nCol>ilColCount) )
		return;
	DisplayInfo (nRow, nCol);
}

void CGridFenster::DisplayInfo ( ROWCOL nRow, ROWCOL nCol, bool bpToolTip/*=false*/ )
{
   	CString olWert, olAnzeige, olValue;
	//CString	olRefTable, olRefCodeField;
	CHOICE_LISTS	*polChoice;
	CGXStyle		olStyle;
	CGXControl		*polControl=0;
	ROWCOL			ilColWithChoiceList;

	if ( (nRow > GetRowCount() ) || ( nCol > GetColCount() ) )
		return ;	//  ung�ltige Zelle

	//ilColWithChoiceList	= (nCol==5) ? 2 : nCol;
	ilColWithChoiceList	= (nCol==STATIC_GRP) ? SIMPLE_VAL : nCol;
	ComposeStyleRowCol( nRow, ilColWithChoiceList, &olStyle );
	if ( !olStyle.GetIncludeItemDataPtr () )
		return;
	if ( ( polControl = GetControl( nRow, nCol ) )  &&
		 polControl->GetValue(olWert) )
	{
		olValue = GetValueRowCol( nRow, nCol ) ;
		if ( olWert != olValue )
			olWert = olValue;
		polChoice = (CHOICE_LISTS*)olStyle.GetItemDataPtr();
		//if ( polChoice && !olWert.IsEmpty() && 
		//	 ParseReftFieldEntry ( polChoice->RefTabField, olRefTable, olRefCodeField ) )
		//	olAnzeige = ogBCD.GetField( olRefTable, olRefCodeField, olWert, 
		//								polChoice->ToolTipField );
		if ( polChoice && !olWert.IsEmpty() )
		{
			if ( nCol == ilColWithChoiceList )
				olAnzeige = GetToolTip ( polChoice, olWert );
			else
				olAnzeige = GetGrpToolTip ( polChoice, olWert );
		
		}

		//  Nur OnEndEditing den Tooltip setzen, w�hrend des Editierens wird er
		//	eh' nicht angezeigt und au�erdem wird das n�chste Zeichen als erstes
		//	interpretiert
		if ( bpToolTip )
		{
			SetStyleRange( CGXRange(nRow, nCol), 
						   CGXStyle().SetUserAttribute(GX_IDS_UA_TOOLTIPTEXT, olAnzeige ));
			if (m_bTtnIsEnabled && m_pTooltipProxy )
			{
				CGXToolTipCtrlProxy* polTooltipProxy;
				polTooltipProxy = (CGXToolTipCtrlProxy*)m_pTooltipProxy;
				polTooltipProxy->InitToolTip();
				if ( polTooltipProxy->m_wndTtnToolTip.m_hWnd )
				{
					polTooltipProxy->m_wndTtnToolTip.SetMaxTipWidth(800);
					RECT rect;
					polTooltipProxy->m_wndTtnToolTip.GetWindowRect (&rect );
				}
			}
		}
	}
	theApp.SetStatusBarText ( olAnzeige );
}

void CGridFenster::SetToolTipForValue ( ROWCOL nRow, ROWCOL nCol, CString &ropValue )
{
   	CString			olAnzeige;
	CHOICE_LISTS	*polChoice;
	CGXStyle		olStyle;
	ROWCOL			ilColWithChoiceList;

	ilColWithChoiceList	= (nCol==STATIC_GRP) ? SIMPLE_VAL : nCol;
	ComposeStyleRowCol( nRow, ilColWithChoiceList, &olStyle );
	if ( !olStyle.GetIncludeItemDataPtr () )
		return;
	polChoice = (CHOICE_LISTS*)olStyle.GetItemDataPtr();
	if ( polChoice && !ropValue.IsEmpty() )
	{
		if ( nCol == ilColWithChoiceList )
			olAnzeige = GetToolTip ( polChoice, ropValue );
		else
			olAnzeige = GetGrpToolTip ( polChoice, ropValue );
	}
	SetStyleRange( CGXRange(nRow, nCol), 
				   CGXStyle().SetUserAttribute(GX_IDS_UA_TOOLTIPTEXT, olAnzeige ));
}


//---------------------------------------------------------------------------------------------------------------------
//					set methods
//---------------------------------------------------------------------------------------------------------------------
void CGridFenster::SetNumSort(bool b)
{
	bmSortNumerical = b;
}
//---------------------------------------------------------------------------------------------------------------------

void CGridFenster::SetAutoGrow(bool b, bool bpCopyStyle)
{
	bmAutoGrow = b;
	bmCopyStyle = bpCopyStyle;
}
//---------------------------------------------------------------------------------------------------------------------

void CGridFenster::SetColsAlwaysVisible(CPtrArray opCols)
{
	omVisibleCols.RemoveAll();
	for (int i = 0; i < opCols.GetSize(); i++)
	{
		int	ilColNo = (int) opCols.GetAt(i);
		omVisibleCols.Add((void*)&ilColNo);
	}
}
//---------------------------------------------------------------------------------------------------------------------

BOOL CGridFenster::SetGridEnabled(bool bpEnable, bool bpResetContent /*=true*/)
{
	BOOL blRet = FALSE;

	int ilRowCount = GetRowCount();
	int ilColCount = GetColCount();

	if (ilRowCount >= 1 && ilColCount >= 1)
	{
		CGXRange olRange(1, 1, ilRowCount, ilColCount);
		GetParam()->SetLockReadOnly ( FALSE );
		SetStyleRange(olRange, CGXStyle().SetReadOnly(FALSE) );
		if (bpEnable == true)
		{
			blRet = SetStyleRange(olRange, CGXStyle().SetEnabled(TRUE)
													 .SetReadOnly(FALSE)
													 .SetInterior(WHITE));
			GetParam()->GetProperties()->SetColor(GX_COLOR_BACKGROUND, WHITE);
			bmIsGridEnabled = true;
		}
		else
		{
			blRet = SetStyleRange(olRange, CGXStyle().SetInterior(SILVER)
													 .SetEnabled(FALSE)
													 .SetReadOnly(TRUE));
			if (bpResetContent == true)
			{
				blRet = SetValueRange(olRange, CString(""));
			}
			GetParam()->GetProperties()->SetColor(GX_COLOR_BACKGROUND, SILVER);
			bmIsGridEnabled = false;
		}
		GetParam()->SetLockReadOnly ( TRUE );
	}
	Redraw();

	return blRet;
}
//-----------------------------------------------------------------------------------------------------------------------

void CGridFenster::SetSortingEnabled(bool bpEnable)
{
	if (bpEnable == true)
	{
		bmIsSorting = true;
	}
	else
	{
		bmIsSorting = false;
	}
}

void CGridFenster::RegisterGxCbsDropDown ( bool bpSort, bool bpTextFit )
{ 
	CGXComboBoxWnd *pWnd = new CGXComboBoxWnd(this);
	unsigned long flags = WS_VSCROLL | CBS_DROPDOWN | CBS_AUTOHSCROLL ;
	if ( bpSort )
		flags |= CBS_SORT;
	if ( bpTextFit )
		flags |= GXCOMBO_TEXTFIT;
	pWnd->Create ( flags, 0);
	pWnd->m_bFillWithChoiceList = TRUE;
	pWnd->m_bWantArrowKeys = TRUE;
	RegisterControl(GX_IDS_CTRL_CBS_DROPDOWN, pWnd);
}


//---------------------------------------------------------------------------------------------------------------------
//						get methods
//---------------------------------------------------------------------------------------------------------------------
bool CGridFenster::GetNumSort()
{
	return bmSortNumerical;
}
//---------------------------------------------------------------------------------------------------------------------

int CGridFenster::GetSortKey()
{
	return imSortKey;
}
//---------------------------------------------------------------------------------------------------------------------

int  CGridFenster::GetCurrentRow()
{
	return imCurrentRow;
}
//---------------------------------------------------------------------------------------------------------------------

int  CGridFenster::GetCurrentCol()
{
	return imCurrentCol;
}
//---------------------------------------------------------------------------------------------------------------------

void CGridFenster::SetSpecialFormat(ROWCOL ipRow, ROWCOL ipCol, CString opForm)
{
	char key[41];
	sprintf(key, "%d %d", ipRow, ipCol);

	if (opForm.IsEmpty())
	{
		omSpecialFormats.RemoveKey(key);
	}
	else
	{
		omSpecialFormats.SetAt(key, opForm);
	}
}
//---------------------------------------------------------------------------------------------------------------------

bool CGridFenster::FormatSpecial ( ROWCOL ipRow, ROWCOL ipCol, CString &ropValue )
{
	char    key[41];
	CString olFormat;
	CString olFormated="-------";
	bool	blRet=false;


	sprintf( key, "%d %d", ipRow, ipCol );
	if ( !omSpecialFormats.Lookup ( key, olFormat ) )
		return false;
	if ( olFormat == "WDAY" )
	{
		ropValue = ropValue.Left(7);
		for ( int i=0; i<7; i++ )
			if ( ropValue.Find ( '1'+i ) >= 0 )
				olFormated.SetAt ( i, '1'+i );
		ropValue = olFormated;
		blRet = true;
	}
	return blRet;
}
//---------------------------------------------------------------------------------------------------------------------

bool CGridFenster::UnFormatSpecial ( ROWCOL ipRow, ROWCOL ipCol, CString &ropValue )
{
	char    key[41];
	CString olFormat;
	CString olUnFormated="";
	bool	blRet=false;


	sprintf( key, "%d %d", ipRow, ipCol );
	if ( !omSpecialFormats.Lookup ( key, olFormat ) )
		return false;
	if ( olFormat == "WDAY" )
	{
		ropValue = ropValue.Left(7);
		for ( int i=0; i<7; i++ )
			if ( ropValue.Find ( '1'+i ) >= 0 )
				olUnFormated += CString ( '1'+i );
		ropValue = olUnFormated;
		blRet = true;
	}
	return blRet;
}

//  Markiere Text ropValue als ung�ltig, wenn er nicht in Auswahlliste 
//	der Zelle (ipRow,ipCol) vorkommt
//	return:  true, wenn Wert als ung�ltig markiert worden ist
//			 false, wenn Wert g�ltig ist, oder Fehler aufgetreten ist
bool CGridFenster::MarkInValid (  ROWCOL ipRow, ROWCOL ipCol, CString &ropValue )
{
	CGXControl	*polCB; 
	CGXStyle	olGXStyle;
	CString		olFound, olChoiceList;
	int			index=0;
	COLORREF	ilColor = ogColors[WHITE_IDX];
	
	if ( ( ipRow<=GetHeaderRows() ) || ( ipCol<=GetHeaderCols() ) )
		return false;

	if ( !ropValue.IsEmpty () && (ropValue[0]!=' ') &&
		 GetStyleRowCol ( ipRow, ipCol, olGXStyle ) &&
		 olGXStyle.GetIncludeChoiceList() && 
		 ( polCB = GetControl ( ipRow, ipCol ) )
	   )
	{
		olChoiceList = olGXStyle.GetChoiceList();
		index = polCB->FindStringInChoiceList( olFound, ropValue, 
											   olChoiceList, TRUE );
		if ( index < 0 )
			ilColor = ogColors[ORANGE_IDX];
	}
	SetStyleRange ( CGXRange(ipRow, ipCol), CGXStyle().SetInterior(ilColor) );

	return (index<0);
}

//  L�scht Zeile, wenn Zelle (ipRow,ipCol) leer ist
bool CGridFenster::DeleteEmptyRow ( ROWCOL ipRow, ROWCOL ipCol ) 
{
	ROWCOL  ilRowCount = GetRowCount();
	ROWCOL  ilLastRowToCheck = bmAutoGrow ? ilRowCount-1 : ilRowCount;

	if ((ipRow >= 1) && (ipRow <= ilLastRowToCheck) && GetValueRowCol(ipRow, ipCol).IsEmpty())
	{
		RemoveRows(ipRow, ipRow);
		return true;
	}
	else 
	{
		return false;
	}
}

//---------------------------------------------------------------------------------------------------------------------
//  Initialisiert ein Grid mit den Daten aus FLDA, FLDT oder FLDD 
//	aus Template-Tabelle TPL
void CGridFenster::IniGridFromTplString(CString &opTPLString, CString opTplUrno)
{
	//      Daten aus FLDA oder FLDT oder FLDD aufbereiten
    CStringArray   olStrArray;
    CString olRefTable;
	CString olRefFieldName;
	CString olRealName;
	CString olChoiceList;
	CString olTabName;
	CString olFieldName;
	CString olFieldNameStr;
	CString olRefTabFldStr;
	CString olTsrUrno;
	CString olFormat;

	RecordSet olTsrRecord(ogBCD.GetFieldCount("TSR"));
	
	
	// LockUpdate();
	ClearTable();

	int ilRecCount = ExtractItemList(opTPLString, &olStrArray, ';');
	if (ilRecCount > 0)
	{
		// go through string array
		InsertRows(1, ilRecCount);
		EnableGridToolTips(TRUE);		//hag990907

		for (int i = 0; i < ilRecCount; i++)
		{
			CString  olText;
			olText = olStrArray.GetAt(i); 
			if (olText.GetLength() >= 8)
			{
				int ilPos = olText.Find('.');
				olTabName = olText.Mid(ilPos + 1, 3);
				olFieldName = olText.Mid(ilPos + 1 + 4, 4);
			}
			else
			{
				ogLog.Trace("ERROR","Error in TPL: Invalid field definition (%s)", opTPLString);
			}

			olTsrUrno = ogBCD.GetFieldExt("TSR", "BTAB", "BFLD", olTabName, olFieldName, "URNO");
			if (olTsrUrno.IsEmpty() == TRUE)
			{
				ogLog.Trace("DEBUG","Record found in TSR where URNO empty.\n");
				continue;
			}
			
			ogBCD.GetRecord("TSR", "URNO", olTsrUrno, olTsrRecord);
			olFormat = olTsrRecord.Values[igTsrFormIdx];
			olRefTable = olTsrRecord.Values[igTsrRtabIdx];
			olRefFieldName = olTsrRecord.Values[igTsrRfldIdx];

			if (!GetNameOfTSRRecord(olTsrUrno, olRealName))
			{
				ogLog.Trace("DEBUG","Found no name for TSR-Set with Urno=%s !\n", olTsrUrno );
			}

				
		//--- Spalte 7 : Daten in versteckten Columns speichern
			olFieldNameStr = olTabName + '.' + olFieldName;	// base table, base field
			SetValueRange(CGXRange(i + 1, 7), olFieldNameStr);
		
		//--- Spalte 8 : Daten in versteckten Columns speichern
			olRefTabFldStr = olRefTable + '.' + olRefFieldName;	// reference table, reference field
			SetValueRange(CGXRange(i + 1, 8), olRefTabFldStr);
			
		//--- Spalte 1 : [RealName] 
			SetValueRange(CGXRange(i + 1, 1), olRealName);
			SetStyleRange(CGXRange(i + 1, 1), CGXStyle().SetEnabled(FALSE));
				
		//--- Spalte SIMPLE_VAL: Fill ComboBox if reference to another table
				// remark: SIMPLE_VAL is defined in ccsglobl.h, probably 2 right now, check there to be sure !!!
			/// if (olFormat != CString("NONE"))
			{
				if( (olRefTable.IsEmpty() == FALSE)  && (olFormat != CString("NONE")) )
				{
					int ilChoiceListSize = ogChoiceLists.GetSize();
					for (int j = 0; j < ilChoiceListSize; j++)
					{
						if (ogChoiceLists[j].TabFieldName == olFieldNameStr)  
						{
							olChoiceList = ogChoiceLists[j].ValueList;
							if(olChoiceList.IsEmpty() == FALSE)
							{
								CGXStyle olStyle;
								olStyle.SetControl(GX_IDS_CTRL_CBS_DROPDOWN).SetChoiceList(olChoiceList);
								//if (ogChoiceLists[j].ToolTipField.IsEmpty() == FALSE)
								//{
									olStyle.SetItemDataPtr(&(ogChoiceLists[j]));
								//}
								SetStyleRange(CGXRange(i + 1, SIMPLE_VAL), olStyle);
								j = ilChoiceListSize;	// break loop
							}
						}
					}
				}
				else
				{	//  edit-Feld event. mit speziellem Format
					 CString olValType = ogBCD.GetField("TSR", "URNO", olTsrUrno, "TYPE");
					 if (olValType == "WDAY")
					 {
						 SetSpecialFormat(i + 1, SIMPLE_VAL, olValType);
					 }
				}
				olChoiceList.Empty();

				if ( !olFormat.IsEmpty() && (olFormat != "NONE") )
				{
					SetStyleRange(CGXRange(i + 1, SIMPLE_VAL), CGXStyle().SetUserAttribute(IDS_GRID_FORMATSTR, olFormat).SetControl(GX_IDS_CTRL_REGEXEDIT) );
					bmCheckUserFormat = true;
				}
			}
			/*else
			{
				SetStyleRange(CGXRange(i + 1, SIMPLE_VAL), CGXStyle().SetEnabled(FALSE)
																	 .SetReadOnly(TRUE));
			}*/
			

		 //--- Spalte DYNAMIC_GRP: 
			if(olRefTable.IsEmpty() == FALSE) 
			{
			    //--- Gruppen suchen f�r die die RefTab eingetragen ist in TABN
				CString  olTabNameComp = olTabName + '.' + olFieldName;
				CCSPtrArray <RecordSet> olDgrArr;
				ogBCD.GetRecords("DGR", "REFT", olTabNameComp, &olDgrArr);
 				int ilCount = olDgrArr.GetSize ();

				CString olGroupName;
				for(int j = 0; j < ilCount; j++)
				{
					if (olDgrArr[j].Values[igDgrUtplIdx] == opTplUrno)
					{
						olGroupName = olDgrArr[j].Values[igDgrGnamIdx];
						olChoiceList += olGroupName + "\n";
					}
				}
				olDgrArr.DeleteAll();

				if(olChoiceList.IsEmpty() == FALSE)
				{
					// mne test
					//TRACE("[IniGridFromTplString] RowCount: %d, ColCount: %d, Row: %d, Col: %d", 
					//	      GetRowCount(), GetColCount(), i+1, DYNAMIC_GRP);
					// end test
					SetStyleRange(CGXRange(i + 1, DYNAMIC_GRP), CGXStyle().SetControl(GX_IDS_CTRL_CBS_DROPDOWN)
										                        .SetChoiceList(olChoiceList));
				}
				olChoiceList.Empty();
			}

		//---    Spalte DYNAMIC_GRP + 1 : PushButton setzen
			if (olRefTable.IsEmpty() == FALSE) 
			{
			    CString  olRefExtension;
				olRefExtension = ogBCD.GetField("TSR", "URNO", olTsrUrno, "EXTE");

			    //--- Wenn Exte Felder angegeben sind 
			    if(!olRefExtension.IsEmpty())
				{
					//--- Ref Felder  in versteckter Spalte 6 speichern
					SetValueRange(CGXRange(i + 1, 6), olTsrUrno);
					SetStyleRange(CGXRange(i + 1, DYNAMIC_GRP + 1),
								  CGXStyle().SetControl (GX_IDS_CTRL_PUSHBTN)
											.SetChoiceList("...\n"));
			   }

			}
			else 
			{
				SetStyleRange(CGXRange(i + 1, DYNAMIC_GRP + 1), CGXStyle().SetEnabled(FALSE));
				SetStyleRange(CGXRange(i + 1, DYNAMIC_GRP), CGXStyle().SetEnabled(FALSE));
			}


		//---  Spalte STATIC_GRP : Statische Gruppierungen f�r  REF - Tabelle

			// if TSR.FORM ="NONE" only dynamic groups are possible
			//if (olFormat != CString("NONE"))
			{
				if (olRefTable.IsEmpty() == FALSE) 
				{
					   //--- Gruppen suchen, f�r die in TABN die RefTab eingetragen ist
					CCSPtrArray <RecordSet> olRecordArray;
					CString olTabNameComp;

					olTabNameComp = olRefTable;
					ogBCD.GetRecords("SGR", "TABN", olRefTable, &olRecordArray);
 					int ilCount = olRecordArray.GetSize ();
					
					//  hag20011205: RFC for ADR, display only Universal groups 
					//	and groups for actual template
					CString ol2ndTplUrno = GetCorrespTplUrno ( opTplUrno );

					//--- Combo Box anlegen mit Daten 
					CString  olChoiceList, olGroupName;
					RecordSet rc(ogBCD.GetFieldCount("SGR"));
					for (int z = 0; z < ilCount; z++)
					{
						rc = olRecordArray[z];
  						olGroupName = rc.Values[igSgrGrpnIdx];
						if ( ( igSgrUtplIdx >=0 )  &&				//  db knows alread UTPL
							 !rc.Values[igSgrUtplIdx].IsEmpty() &&	//  no universal group
							 (rc.Values[igSgrUtplIdx]!=opTplUrno && rc.Values[igSgrUtplIdx]!=ol2ndTplUrno) 
						   )
						   continue;
						olChoiceList += olGroupName + "\n";
					}
					olRecordArray.DeleteAll();

					if(olChoiceList.IsEmpty() == FALSE)
					{
						SetStyleRange(CGXRange(i + 1, STATIC_GRP),CGXStyle().SetControl(GX_IDS_CTRL_CBS_DROPDOWN)
																			.SetChoiceList(olChoiceList));
					}
				}
				else
				{
					SetStyleRange(CGXRange(i + 1, STATIC_GRP), CGXStyle().SetEnabled(FALSE));
				}
			}
			/*else
			{
				// if TSR.FORM = "NONE" only dynamic groups can be used
				SetStyleRange(CGXRange(i + 1, STATIC_GRP),CGXStyle().SetEnabled(FALSE)
																	 .SetReadOnly(TRUE));
			}*/
		}
	}

	// LockUpdate(FALSE);
	// Redraw();
}
		
//---------------------------------------------------------------------------------------------------------------------



// Every grid initialization that has to be done only once goes inside this function
void CGridFenster::IniLayoutForConditions ()
{
	CCS_TRY


	//--- set number of columns
	SetColCount(8);

	//--- set column widths
	CRect olRect;
	GetClientRect(&olRect);
	olRect.DeflateRect(8, 0);
	int ColWidth0 = 25;
	int ColWidth1 = (int)((olRect.Width()- ColWidth0)* 0.25);
	int ColWidthSimpl = (int)((olRect.Width()- ColWidth0)* 0.25);
	int ColWidthDyn = (int)((olRect.Width()- ColWidth0)* (0.25 * 0.8));
	int ColWidthBtn = (int)((olRect.Width()- ColWidth0)* (0.25 * 0.2));
	int ColWidthStat = (int)((olRect.Width()- ColWidth0)* 0.25);

	//---  ComboBox inside grid
	RegisterGxCbsDropDown ( true, true );
	RegisterRegexEdit ();

	SetColWidth(0, 0, ColWidth0);	// header
	SetColWidth(1, 1, ColWidth1);	// field name
	SetColWidth(SIMPLE_VAL, SIMPLE_VAL, ColWidthSimpl);	// simple value
	SetColWidth(DYNAMIC_GRP, DYNAMIC_GRP, ColWidthDyn);	// dynamic group
	SetColWidth(DYNAMIC_GRP + 1, DYNAMIC_GRP + 1, ColWidthBtn);	// pushbutton
	SetColWidth(STATIC_GRP, STATIC_GRP, ColWidthStat);	// static groups
	// Hidden columns:
	// 6	-	reference extension:	(EXTE from TSR)
	// 7	-   base tables:			XXX.XXXX   (BTAB, BFLD from TSR)
	// 8	-   _not used_ (as of 990820, MNE)
//references: TAB.FLDN	(RTAB, RFLD from TSR)

	GetParam()->SetNumberedColHeaders(FALSE);                 
	GetParam()->SetNumberedRowHeaders(FALSE); 
	GetParam()->EnableTrackRowHeight(FALSE); 
	GetParam()->EnableTrackColWidth(FALSE);

	ChangeColHeaderStyle(CGXStyle().SetEnabled(FALSE));
	SetStyleRange(CGXRange(0,1), CGXStyle().SetEnabled(FALSE)
											.SetReadOnly(TRUE)
											.SetValue(GetString(1480)));
	SetValueRange(CGXRange(0, SIMPLE_VAL), CString(GetString(1521)));
	SetValueRange(CGXRange(0,DYNAMIC_GRP), CString(GetString(1519)));
	SetValueRange(CGXRange(0,STATIC_GRP), CString(GetString(1520)));

	SetSortingEnabled(false);	// disable sorting

	//--- these columns are only used to save data 
	HideCols(6, 8); 

	//--- row height can't be changed
	GetParam()->EnableMoveRows(false);


	CCS_CATCH_ALL
}
//---------------------------------------------------------------------------------------------------------------------

//  resets data in visible columns (except column 1)
void CGridFenster::ResetConditionsTable()
{
	CCS_TRY


	int ilRowCount = GetRowCount();
	if (ilRowCount > 0)
	{
		SetValueRange( CGXRange(1, 2, ilRowCount, 5),CString(""));
		SetStyleRange(CGXRange(1, 2, ilRowCount, 5), 
					   CGXStyle().SetUserAttribute(GX_IDS_UA_TOOLTIPTEXT, ""));
	}

	
	CCS_CATCH_ALL
}
//------------------------------------------------------------------------------------------------------------------------

void CGridFenster::ClearTable()
{
	CCS_TRY


	ROWCOL ilCount1 = GetRowCount();
	if(ilCount1 != 0)
	{
		RemoveRows(1, ilCount1);
	}

	omSpecialFormats.RemoveAll ();	

	
	CCS_CATCH_ALL
}
//------------------------------------------------------------------------------------------------------------------------

BOOL CGridFenster::EnableConditionsTable(bool bpEnable)
{
	BOOL blRet = FALSE;

	int ilRowCount = GetRowCount();
	int ilColCount = GetColCount();

	if (ilRowCount >= 1 && ilColCount >= 1)
	{
		CGXRange olRange(1, 1, ilRowCount, ilColCount);
		GetParam()->SetLockReadOnly(FALSE);
		SetStyleRange(olRange, CGXStyle().SetReadOnly(FALSE));

		if (bpEnable == true)
		{
			blRet = SetStyleRange(olRange, CGXStyle().SetEnabled(TRUE)
													 .SetReadOnly(FALSE)
													 .SetInterior(WHITE));
			GetParam()->GetProperties()->SetColor(GX_COLOR_BACKGROUND, WHITE);
			bmIsGridEnabled = true;
		}
		else
		{
			blRet = SetStyleRange(olRange, CGXStyle().SetInterior(SILVER)
													 .SetEnabled(FALSE)
													 .SetReadOnly(TRUE));
			GetParam()->GetProperties()->SetColor(GX_COLOR_BACKGROUND, SILVER);
			bmIsGridEnabled = false;
		}

		blRet &= SetStyleRange(CGXRange(0,1,ilRowCount,1), CGXStyle().SetEnabled(FALSE)
																	 .SetReadOnly(TRUE));
		GetParam()->SetLockReadOnly(TRUE);
	}

	Redraw();

	return blRet;
}

//------------------------------------------------------------------------------------------------------------------------

void CGridFenster::OnConditionsGridButton(CString &ropTplUrno, ROWCOL ilRow, ROWCOL ilCol)
{
	if (ilCol != DYNAMIC_GRP + 1)
	{
		return;
	}

	//--- Expression Editor anlegen
    CString  olAuswahl = GetValueRowCol(ilRow, DYNAMIC_GRP); 
	CString  olUrno;
			
	bool err = ogBCD.GetField("DGR", "GNAM", olAuswahl, "URNO", olUrno);
			
	CString  olTSRUrnoStr;
	
	olTSRUrnoStr = GetValueRowCol(ilRow, 6); 
    CString olRef;
	olRef = GetValueRowCol(ilRow, 7);
    CDialog_ExpressionEditor olDialog(ropTplUrno, olUrno, olRef, olTSRUrnoStr, this);
	olDialog.DoModal();

          
	//--- Text in Auswahl anzeigen 
	if(olDialog.omDgrName.IsEmpty() == FALSE)
	{
       SetValueRange(CGXRange(ilRow, DYNAMIC_GRP), olDialog.omDgrName);
	   SetValueRange(CGXRange(ilRow, STATIC_GRP), CString(""));	// MNE, 001008
	   SetValueRange(CGXRange(ilRow, SIMPLE_VAL), CString(""));	// MNE, 001008
	}
	
	//--- ComboBox neu aufbauen
 	CCSPtrArray <RecordSet> olRecordArray;
    CString  olRefFieldName;
	CString olChoiceList;
	CString olGroupName;
	RecordSet olRecord;

	olRefFieldName = GetValueRowCol(ilRow, 7);
	//  ogBCD.GetRecords("DGR", "REFT", olRefFieldName, &olRecordArray);
	ogBCD.GetRecordsExt("DGR", "REFT", "UTPL", olRefFieldName, ropTplUrno, &olRecordArray);
 	int ilSize = olRecordArray.GetSize ();
    olRecordArray.Sort(&StringComp);

	for(int ilLC = 0; ilLC < ilSize; ilLC++) 
	{
		olRecord = olRecordArray.GetAt(ilLC);
		olGroupName = olRecord[igDgrGnamIdx];
		olChoiceList += olGroupName + "\n";
	}
	olRecordArray.DeleteAll();

	//if(olChoiceList.IsEmpty() == FALSE)
	//{
		SetStyleRange ( CGXRange(ilRow, DYNAMIC_GRP), 
						CGXStyle().SetControl(GX_IDS_CTRL_COMBOBOX) 
							      .SetChoiceList(olChoiceList));
	//}
	olAuswahl = GetValueRowCol(ilRow, DYNAMIC_GRP); 
	MarkInValid(ilRow, DYNAMIC_GRP, olAuswahl);
}
//--------------------------------------------------------------------------------------------------------------------

bool CGridFenster::CheckIfLineEmpty( ROWCOL ipRow, ROWCOL ipMaxCol, 
									 ROWCOL ipMinCol)
{
	bool blReturn = false;

	for (ROWCOL ilCol = ipMinCol; ilCol <= ipMaxCol; ilCol++)
	{
		CString olContents = GetValueRowCol(ipRow, ilCol);
		if (olContents.IsEmpty() == FALSE &&  (olContents != " "))
		{
			blReturn = true;
			break;
		}
	}

	return blReturn;
}
//--------------------------------------------------------------------------------------------------------------------

CString CGridFenster::GetLinePrio(ROWCOL ipRow, ROWCOL ipMaxCol, ROWCOL ipMinCol)
{
	CString olRet = "0";

	CCS_TRY

	CString olContents;
	for (ROWCOL ilCol = ipMinCol; ilCol <= ipMaxCol; ilCol++)
	{
		olContents = GetValueRowCol(ipRow, ilCol);
		if (olContents.IsEmpty() == FALSE &&  (olContents != " "))
		{
			break;
		}
	}
	
	if (ilCol == SIMPLE_VAL)
	{
		olRet = "3";
	}
	else if (ilCol == STATIC_GRP)
	{
		olRet = "2";
	}
	else if (ilCol == DYNAMIC_GRP)
	{
		olRet = "1";
	}

	CCS_CATCH_ALL

	return olRet;
}
//--------------------------------------------------------------------------------------------------------------------

CString GetToolTip ( CHOICE_LISTS *popChoice, CString &ropSelValue )
{
	CString olRefTable, olRefCodeField;
	CString	olToolTip;
	if ( !popChoice->ToolTipField.IsEmpty() &&
		 ParseReftFieldEntry ( popChoice->RefTabField, olRefTable, 
							   olRefCodeField ) )
		olToolTip = ogBCD.GetField( olRefTable, olRefCodeField, ropSelValue, 
								    popChoice->ToolTipField );
	return olToolTip ;
}
//--------------------------------------------------------------------------------------------------------------------

CString GetGrpToolTip ( CHOICE_LISTS *popChoice, CString &ropSelValue )
{
	int						ilCount;
	CString					olToolTip, olSgrUrno, olMemberCode, olRefUrno;
	CCSPtrArray<RecordSet>	olGroupMembers;
	CCSPtrArray<CString>			olMembersMap;
	
	CString olRefTable, olRefCodeField;
	if ( !ParseReftFieldEntry ( popChoice->RefTabField, olRefTable, 
							    olRefCodeField ) )
		return olToolTip;
	olSgrUrno = ogBCD.GetFieldExt( "SGR", "TABN", "GRPN", olRefTable, 
								   ropSelValue, "URNO" );
	if ( !olSgrUrno.IsEmpty () )
		ogBCD.GetRecords( "SGM", "USGR", olSgrUrno, &olGroupMembers );

	ilCount = olGroupMembers.GetSize();
	
	for ( int i=0; (i<ilCount) && (igSgmUvalIdx>=0); i++ )
	{
		olRefUrno = olGroupMembers[i].Values[igSgmUvalIdx];
		olMemberCode = ogBCD.GetField ( olRefTable, "URNO", olRefUrno, 
										olRefCodeField );
		if ( olMemberCode.IsEmpty () && !popChoice->ToolTipField.IsEmpty() )
			olMemberCode = ogBCD.GetField ( olRefTable, "URNO", olRefUrno, 
											popChoice->ToolTipField );
		if ( !olMemberCode.IsEmpty() )
			olMembersMap.New( olMemberCode );
	}
	olMembersMap.Sort ( CompareStrings );
	for ( i=0; i<olMembersMap.GetSize(); i++ )
	{
		olToolTip += olMembersMap[i] + ", ";
	}
	int n = olToolTip.ReverseFind( ',' );
	if ( n>0 )
		olToolTip = olToolTip.Left ( n );
	if ( olToolTip.GetLength() > 3000 )
	{	//  too long tooltips will flicker on the screen
		olToolTip = olToolTip.Left ( 3000 );
		olToolTip += "...";
	}
	olGroupMembers.DeleteAll();
	olMembersMap.DeleteAll();
	return olToolTip;
}	
//------------------------------------------------------------------------------------------------------------------------

void CGridFenster::OnInitCurrentCell(ROWCOL nRow, ROWCOL nCol)
{
	CGXGridWnd::OnInitCurrentCell(nRow, nCol);

	SendMsgToParent(GRID_MESSAGE_CURRENTCELLCHANGED, nRow, nCol );
}
//------------------------------------------------------------------------------------------------------------------------

BOOL CGridFenster::OnRButtonClickedRowCol(ROWCOL nRow, ROWCOL nCol, UINT nFlags, CPoint pt)
{
	return SendMsgToParent(GRID_MESSAGE_RBUTTONCLICK, nRow, nCol );
}
//------------------------------------------------------------------------------------------------------------------------

void CGridFenster::SelectRowHeader(int ipRow, bool bpAdditional/*=false*/)
{
	ROWCOL ilRowCount = GetRowCount();
	if (bpAdditional == false)
	{
		SelectRange(CGXRange().SetCols(0), FALSE);
		omSelRowHeaders.RemoveAll();
	}

	if ((ipRow >= 0) && (ipRow <= (int)ilRowCount))
	{
		SelectRange(CGXRange(ipRow, 0));
		omSelRowHeaders.Add(ipRow);
	}
}
//------------------------------------------------------------------------------------------------------------------------

BOOL CGridFenster::DoCancelMode()
{
	BOOL blRet = CGXGridWnd::DoCancelMode ();
	for (int i = 0; i < omSelRowHeaders.GetSize(); i++)
	{
		SelectRange(CGXRange(omSelRowHeaders[i],0));
	}
	return blRet;
}
//------------------------------------------------------------------------------------------------------------------------
	
void CGridFenster::ModifyAllResChoiceLists (CString opCode, bool bpAdd, bool bpGroup)
{
	for (ROWCOL i = 1; i <= GetRowCount(); i++)
	{
		ModifyResChoiceList(opCode, bpAdd, bpGroup, i);
	}
}
//------------------------------------------------------------------------------------------------------------------------

void CGridFenster::ModifyResChoiceList (CString opCode, bool bpAdd, bool bpGroup, ROWCOL ipRow)
{
	CGXStyle olStyle;
	CString	 olChoiceList="\n";
	CString  olItem = bpGroup ? "\n*" : "\n";
	int ilLength ;

	//  Um mit CString::Find das richtige Vorkommen des Items in der Auswahl-
	//  liste sicherzustellen, wird in olChoiceList vorne und hinten ein Trenner 
	//  '\n' eingef�gt. 
	olItem += opCode;
	olItem += "\n";

	olStyle.SetDefault();	//  lokale Variable zur�cksetzen
	GetStyleRowCol(ipRow, 1, olStyle);
	BOOL blReadOnly = GetParam( )->IsLockReadOnly();

	if (olStyle.GetIncludeChoiceList())	
	{
		olChoiceList += olStyle.GetChoiceList();
	}

	if ((olChoiceList.IsEmpty() == FALSE) && (olChoiceList.Right(1) != "\n"))
	{
		olChoiceList += "\n";
	}

	int ilPos = olChoiceList.Find(olItem);
	if (ilPos == -1)
	{
		if (bpAdd == false)	//  L�schen und nicht gefunden fertig
		{
			return;
		}

		//  Einf�gen
		olItem.Replace("\n", "");
		olChoiceList += olItem;
	}
	else		//  item ist in Auswahlliste
	{
		if ( bpAdd )  //  zuf�gen -> fertig
			return;
		//  l�schen
		olChoiceList.Replace ( olItem, "\n" );
		ilLength = olChoiceList.GetLength();
		if ( olChoiceList[ilLength-1]=='\n' )
			 olChoiceList = olChoiceList.Left(ilLength-1);
	}	
	//  vorderen '\n' wieder entfernen
	ilLength = olChoiceList.GetLength();
	if ( (ilLength>0) && ( olChoiceList[0]=='\n' ) )
		olChoiceList = olChoiceList.Right(ilLength-1);
	olStyle.SetControl(GX_IDS_CTRL_CBS_DROPDOWN).SetChoiceList(olChoiceList);
	if ( blReadOnly )
		GetParam( )->SetLockReadOnly(FALSE);
	BOOL blIsCurrentCell = IsCurrentCell(ipRow,1) ;
	if ( blIsCurrentCell )
		ResetCurrentCell();

	SetStyleRange(CGXRange(ipRow, 1), olStyle);
	if ( blIsCurrentCell )
		SetCurrentCell(ipRow, 1);
	if ( blReadOnly )
		GetParam( )->SetLockReadOnly(TRUE);
}

void CGridFenster::SortTable(CString opCols, CString opDirs ) 
{
	//--
	// checks sorting
	//-- 
	int i, j=0, ilCol, ilColCnt;
	CGXSortInfoArray  sortInfo;
	CStringArray olColArr, olDirArr;
	int ilRecCount = ExtractItemList(opCols, &olColArr, ',');
	ExtractItemList(opDirs, &olDirArr, ',');

	sortInfo .SetSize(ilRecCount);
	ilColCnt = GetColCount();

	for ( i=0; i<ilRecCount; i++ )
	{
		if ( (sscanf( olColArr[i], "%d", &ilCol ) > 0) && (ilCol<=ilColCnt ) )
		{	//  i. column is valid 
			sortInfo[j].nRC = ilCol;                       
			sortInfo[j].sortType = CGXSortInfo::autodetect;  
			if ( (i<olDirArr.GetSize()) &&  (olDirArr[i]=="D") )
				sortInfo[j].sortOrder = CGXSortInfo::descending;
			else
				sortInfo[j].sortOrder = CGXSortInfo::ascending;
			j++;
		}
	}
	if ( j>0 )
	{
		SortRows( CGXRange().SetRows(1,GetRowCount()), sortInfo); 
		SendMsgToParent(GRID_MESSAGE_RESORTED, 0, sortInfo[0].nRC );
	}
}


//  Add or Remove a String (opCode) to/from the Coicelist of a GX_IDS_CTRL_CBS_DROPDOWN-control
void CGridFenster::ModifyChoiceList ( int ipRow, int ipCol, CString opCode, bool bpAdd)
{
	CGXStyle olStyle;
	CString	 olChoiceList="\n";
	CString  olItem = "\n";
	int ilLength ;

	//  Um mit CString::Find das richtige Vorkommen des Items in der Auswahl-
	//  liste sicherzustellen, wird in olChoiceList vorne und hinten ein Trenner 
	//  '\n' eingef�gt. 
	olItem += opCode;
	olItem += "\n";

	olStyle.SetDefault();	//  lokale Variable zur�cksetzen
	GetStyleRowCol(ipRow, ipCol, olStyle);
	BOOL blReadOnly = GetParam( )->IsLockReadOnly();

	if (olStyle.GetIncludeChoiceList())	
	{
		olChoiceList += olStyle.GetChoiceList();
	}

	if ((olChoiceList.IsEmpty() == FALSE) && (olChoiceList.Right(1) != "\n"))
	{
		olChoiceList += "\n";
	}

	int ilPos = olChoiceList.Find(olItem);
	if (ilPos == -1)
	{
		if (bpAdd == false)	//  L�schen und nicht gefunden fertig
		{
			return;
		}

		//  Einf�gen
		olItem.Replace("\n", "");
		olChoiceList += olItem;
	}
	else		//  item ist in Auswahlliste
	{
		if ( bpAdd )  //  zuf�gen -> fertig
			return;
		//  l�schen
		olChoiceList.Replace ( olItem, "\n" );
		ilLength = olChoiceList.GetLength();
		if ( olChoiceList[ilLength-1]=='\n' )
			 olChoiceList = olChoiceList.Left(ilLength-1);
	}	
	//  vorderen '\n' wieder entfernen
	ilLength = olChoiceList.GetLength();
	if ( (ilLength>0) && ( olChoiceList[0]=='\n' ) )
		olChoiceList = olChoiceList.Right(ilLength-1);
	olStyle.SetControl(GX_IDS_CTRL_CBS_DROPDOWN).SetChoiceList(olChoiceList);
	if ( blReadOnly )
		GetParam( )->SetLockReadOnly(FALSE);
	BOOL blIsCurrentCell = IsCurrentCell(ipRow,ipCol) ;
	if ( blIsCurrentCell )
		ResetCurrentCell();

	SetStyleRange(CGXRange(ipRow, ipCol), olStyle);
	if ( blIsCurrentCell )
		SetCurrentCell(ipRow, ipCol);
	if ( blReadOnly )
		GetParam( )->SetLockReadOnly(TRUE);
}

void CGridFenster::ChangeCondTableChoiceList ( CString opTable, int ipCol, 
											   CString opCode, bool bpAdd)
{
	CString		olGridRefTab;
	int			ilRotCount = GetRowCount();
	
	for (int ilRow = 1; ilRow <= ilRotCount; ilRow++)
	{
		olGridRefTab = GetValueRowCol(ilRow, 8);
		if (olGridRefTab.GetLength() == 8)
		{
			olGridRefTab = olGridRefTab.Left(3);
			if (olGridRefTab == opTable)
			{
				ModifyChoiceList ( ilRow, ipCol, opCode, bpAdd );
			} 
		}
	}

}

void CGridFenster::OnChangedSelection(const CGXRange* changedRect, BOOL bIsDragging, BOOL bKey)
{
	CGXGridWnd::OnChangedSelection(changedRect, bIsDragging, bKey );
	
	if ( !bIsDragging )
	{
		SendMsgToParent(GRID_MESSAGE_ENDSELECTION, 0, 0 );
	}
}

/*
BOOL CGridFenster::OnValidateCell(ROWCOL nRow, ROWCOL nCol)
{
	BOOL blRet = CGXGridWnd::OnValidateCell(nRow, nCol);

	if (bmCheckUserFormat)
	{
		CString olFormat, olValue;
		CGXStyle style;

		GetStyleRowCol(nRow, nCol, style);
		style.GetUserAttribute(IDS_GRID_FORMATSTR, olFormat);
		
		if ( !olFormat.IsEmpty() && (olFormat != "NONE") )
		{
			CGXControl* polControl = GetCurrentCellControl();
			if ( polControl )
			{	
				polControl->GetCurrentText(olValue);
				bool blCheck = CheckRegExMatch(olFormat, olValue);
				if (blCheck == false)
				{
					SetWarningText (GetString(IDS_STRING57346));
					blRet = FALSE;
				}
				TRACE ( "OnValidateCell: CheckRegExMatch Result <%d>\n", blRet );
			}
			else
				TRACE ( "OnValidateCell: GetCurrentCellControl Result <null>\n" );
		}
	}
	return blRet;
}
*/

void CGridFenster::RegisterRegexEdit ()
{ 
	CRegexEdit *pWnd = new CRegexEdit(this, GX_IDS_CTRL_REGEXEDIT);
	RegisterControl(GX_IDS_CTRL_REGEXEDIT, pWnd);
}


//--------------------------------------------------------------------------------------------------------------------
int CompareStrings (const class CString **p1 ,const class CString **p2 )
{
	if ( *p1 )
		return (*p1)->Compare ( **p2 );
	else
		return 0;
}
//--------------------------------------------------------------------------------------------------------------------

BOOL CGridFenster::SendMsgToParent(UINT ipMsg, ROWCOL nRow, ROWCOL nCol)
{
	CELLPOS rlPos;
	rlPos.Col = nCol;
	rlPos.Row = nRow;

	CDialog*  polDlg = NULL;
	
	if( !pomParent )
		polDlg = (CDialog*) GXGetParentWnd(this, RUNTIME_CLASS(CDialog), TRUE);
	else 
		polDlg = (CDialog*) pomParent;
	if ( polDlg )
	{
		polDlg->SendMessage(ipMsg, (WPARAM)this, (LPARAM)&rlPos);
		return TRUE;
	}
	else
		return FALSE;
}