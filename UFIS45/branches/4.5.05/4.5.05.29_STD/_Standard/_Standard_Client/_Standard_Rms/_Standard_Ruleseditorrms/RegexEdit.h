#if !defined(AFX_REGEXEDIT_H__41927145_F2F7_11D6_96E6_E4E099000000__INCLUDED_)
#define AFX_REGEXEDIT_H__41927145_F2F7_11D6_96E6_E4E099000000__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// RegexEdit.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CRegexEdit window

class CRegexEdit : public CGXEditControl
{
// Construction
public:
	CRegexEdit (CGXGridCore* pGrid, UINT nID );

// Attributes
public:

// Operations
public:
	BOOL ValidateString(const CString& sEdit);

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CRegexEdit)
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CRegexEdit();

	// Generated message map functions
protected:
	//{{AFX_MSG(CRegexEdit)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_REGEXEDIT_H__41927145_F2F7_11D6_96E6_E4E099000000__INCLUDED_)
