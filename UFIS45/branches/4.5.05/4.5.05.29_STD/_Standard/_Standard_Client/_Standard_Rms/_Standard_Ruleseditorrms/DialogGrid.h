#if !defined(AFX_DIALOGGRID_H__041C8F83_B1E0_11D2_AAEE_00001C018CF3__INCLUDED_)
#define AFX_DIALOGGRID_H__041C8F83_B1E0_11D2_AAEE_00001C018CF3__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

//////////////////////////////////////////////////////////////////////////////////
//
// GridFenster.h : header file
//
// Notes: The CGridFenster class is derived from the standard CGXGridWnd class . 
//         
//        The class offers the possibility to create Templates for the display in the main
//        FormView Window . The Dialog offers all possible fields from the TSR table in a gridform
//        display on the left hand side for the 3 areas (turnaround, arrival, departure).
//         
//        From these lists the apropriate fields can be choosen and will then appear in the corresponding 
//        grid on the right handside .
//
//        The selections can be sorted alphabeticly and lines in the grids can be moved in order to 
//        represent the priority by position .
//
//        The finiched selections are stored in the TPL table .
//
//
// Date : March 1999
//
// Author : EDE 
//
// Modification History:
//
//


#include <CCSGlobl.h>




typedef struct RowColPos
{
	int Row;
	int Col;

	RowColPos(void)
	{
		Row = -1;
		Col = -1;
	}

} CELLPOS;





class CGridFenster : public CGXGridWnd
{

	//--- Construction / Destruction
public:
	CGridFenster();	// default constructor
	CGridFenster(CWnd *pParent);
	~CGridFenster();

//--- Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CGridFenster)
	//}}AFX_VIRTUAL


//--- Implementation
	//--- �berschreiben von CGXGridCore virtual Funktionen
	BOOL OnSelDragDrop(ROWCOL nStartRow, ROWCOL nStartCol, ROWCOL nDragRow, ROWCOL nDragCol);
	BOOL OnEndEditing(ROWCOL nRow, ROWCOL nCol);
	BOOL OnStartEditing(ROWCOL nRow, ROWCOL nCol);
	BOOL OnTrackColWidth(ROWCOL nCol);

    BOOL OnLButtonClickedRowCol(ROWCOL nRow, ROWCOL nCol, UINT nFlags, CPoint pt);
	BOOL OnLButtonDblClkRowCol(ROWCOL nRow, ROWCOL nCol, UINT nFlags, CPoint pt);
	void OnClickedButtonRowCol(ROWCOL nRow, ROWCOL nCol);
	void OnModifyCell (ROWCOL nRow, ROWCOL nCol);
	void OnChangedSelection(const CGXRange* changedRect, BOOL bIsDragging, BOOL bKey);
	//virtual BOOL OnValidateCell(ROWCOL nRow, ROWCOL nCol);
	
	// helper functions
	virtual BOOL ResetContent();
	BOOL InsertBottomRow();
	void DoAutoGrow (ROWCOL nRow, ROWCOL nCol);
	void CopyStyleLastLineFromPrev(bool bpResetValues = true);
	void CopyStyleOfLine(ROWCOL ipSourceLine, ROWCOL ipDestLine, bool bpResetValues = true);
	void DisplayInfo ( ROWCOL nRow, ROWCOL nCol, bool bpToolTip=false );
	void SetToolTipForValue ( ROWCOL nRow, ROWCOL nCol, CString &ropValue );
	void RegisterGxCbsDropDown ( bool bpSort, bool bpTextFit );
	void RegisterRegexEdit ();
	void ClearTable();
	void SelectRowHeader ( int ipRow, bool bpAdditional=false );
	
	// Set
	void SetNumSort(bool b);
	void SetAutoGrow(bool b, bool bpCopyStyle = true);
	void SetSortingEnabled(bool bpEnable);
	BOOL SetGridEnabled(bool bpEnable, bool bpResetContent = true);
	void SetColsAlwaysVisible(CPtrArray opCols);
	

	// Get
	bool GetNumSort();
	int GetSortKey();
	int GetCurrentRow();
	int GetCurrentCol();
	int  GetColWidth(ROWCOL nCol);

	//  Funktionen f�r spezielle Anwendungen
	//  event. sp�ter Klasse von CGridFenster ableiten
	void IniGridFromTplString (CString &opTPLString, CString opTplUrno);
	void IniLayoutForConditions();
	void ResetConditionsTable();
	BOOL EnableConditionsTable(bool bpEnable);
	void OnConditionsGridButton (CString &ropTplUrno, ROWCOL ilRow, ROWCOL ilCol);
	bool CheckIfLineEmpty( ROWCOL ipRow, ROWCOL ipMaxCol, ROWCOL ipMinCol = 2);
	CString GetLinePrio(ROWCOL ipRow, ROWCOL ipMaxCol, ROWCOL ipMinCol = 2);
	void ModifyAllResChoiceLists (CString opCode, bool bpAdd, bool bpGroup);
	void ModifyResChoiceList (CString opCode, bool bpAdd, bool bpGroup, ROWCOL ipRow);
	void ModifyChoiceList ( int ipRow, int ipCol, CString opCode, bool bpAdd);
	void ChangeCondTableChoiceList ( CString opTable, int ipCol, 
	 							     CString opCode, bool bpAdd );


protected:
	void OnInitCurrentCell(ROWCOL nRow, ROWCOL nCol);
	BOOL OnRButtonClickedRowCol(ROWCOL nRow, ROWCOL nCol, UINT nFlags, CPoint pt);
	BOOL DoCancelMode();
	BOOL SendMsgToParent(UINT ipMsg, ROWCOL nRow, ROWCOL nCol);


//--- Generated message map functions
public:
	CMapStringToString omSpecialFormats;

	void SetSpecialFormat(ROWCOL ipRow, ROWCOL ipCol, CString opForm);
	bool FormatSpecial(ROWCOL ipRow, ROWCOL ipCol, CString &ropValue); 
	bool UnFormatSpecial(ROWCOL ipRow, ROWCOL ipCol, CString &ropValue); 
	bool MarkInValid( ROWCOL ipRow, ROWCOL ipCol, CString &ropValue);
	bool DeleteEmptyRow(ROWCOL ipRow, ROWCOL ipCol);
	void SortTable(CString opCols, CString opDirs ) ;

protected:
	//{{AFX_MSG(CGridFenster)
	afx_msg  void  OnLButtonUp(UINT nFlags, CPoint point);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

//--- Attributes
	CWnd *pomParent;
	CPtrArray omVisibleCols;
	bool bmSortNumerical; //--- Ist numerisch sortiert ? 
	bool bmSortAscend;    //--- sort asdcending or descending ? 
	bool bmIsSorting;	  //--- Is sorting enabled ?
	bool bmAutoGrow;	  //--- Is AutoGrow enabled ?
	bool bmCopyStyle;	  //--- Copy Style from previous row when auto-growing ?
	bool bmCheckUserFormat; //--- inserted text will be checked if passing a user defined format string

	int imSortKey; //--- Nach welchem Feld wird sortiert ?
	int imCurrentRow; 
	int imCurrentCol;	
	CUIntArray omSelRowHeaders;
public:
	bool bmIsGridEnabled; //--- Is grid activated or deactivated ?
};

CString GetToolTip ( CHOICE_LISTS *popChoice, CString &ropSelValue );
CString GetGrpToolTip ( CHOICE_LISTS *popChoice, CString &ropSelValue );

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DIALOGGRID_H__041C8F83_B1E0_11D2_AAEE_00001C018CF3__INCLUDED_)
