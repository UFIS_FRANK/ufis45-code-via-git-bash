// BasePropertySheet.h : header file
//

#ifndef __BASEPROPERTYSHEET_H__
#define __BASEPROPERTYSHEET_H__


#include <CViewer.h>


/////////////////////////////////////////////////////////////////////////////
// BasePropertySheet

class BasePropertySheet : public CPropertySheet
{
	DECLARE_DYNAMIC(BasePropertySheet)

// Construction
public:
	BasePropertySheet(LPCSTR pszCaption, CWnd* pParentWnd = NULL,
		CViewer *popViewer = NULL, UINT iSelectPage = 0);

// Attributes
protected:
	CWnd *pomParentWnd;	// require for PropertySheet protection logic
	CViewer *pomViewer;
	CStatic omSeparator;
	static int imCount;

// Operations
public:
	virtual void LoadDataFromViewer();
	virtual void SaveDataToViewer(CString opViewName,BOOL bpSaveToDb = TRUE);
	virtual int QueryForDiscardChanges();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(BasePropertySheet)
	public:
	virtual int DoModal();
	virtual BOOL OnInitDialog();
	virtual BOOL DestroyWindow();
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~BasePropertySheet();

	// Generated message map functions
protected:
	//{{AFX_MSG(BasePropertySheet)
	afx_msg void OnViewSelChange();
	afx_msg void OnSave();
	afx_msg void OnDelete();
	afx_msg void OnOK();
	afx_msg void OnApply();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

// Help routines
protected:
	void UpdateComboBox();
	CString GetComboBoxText();
	BOOL IsIdentical(CStringArray &ropArray1, CStringArray &ropArray2);
	BOOL IsInArray(CString &ropString, CStringArray &ropArray);
// the buttons
private:
	CComboBox	omView;
	CButton		omSave;
	CButton		omDelete;
	CButton		omApply;


};

/////////////////////////////////////////////////////////////////////////////
#endif //__BASEPROPERTYSHEET_H__