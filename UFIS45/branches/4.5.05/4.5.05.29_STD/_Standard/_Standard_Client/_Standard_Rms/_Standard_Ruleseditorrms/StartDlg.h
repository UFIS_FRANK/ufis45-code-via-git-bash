#if !defined(AFX_STARTDLG_H__C163AC53_1D93_11D3_A62E_0000C007916B__INCLUDED_)
#define AFX_STARTDLG_H__C163AC53_1D93_11D3_A62E_0000C007916B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// StartDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// StartDlg dialog

class StartDlg : public CDialog
{
// Construction
public:
	StartDlg(CWnd* pParent = NULL);   // standard constructor
	void Create();

// Dialog Data
	//{{AFX_DATA(StartDlg)
	enum { IDD = IDD_START_DLG };
	CListBox	omListBox;
	//}}AFX_DATA


bool AddString(CString opString);
bool InsertString(int ipIndex, CString opString);
void CheckVisibility();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(StartDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	
	CWnd *pomParent;

	// Generated message map functions
	//{{AFX_MSG(StartDlg)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_STARTDLG_H__C163AC53_1D93_11D3_A62E_0000C007916B__INCLUDED_)
