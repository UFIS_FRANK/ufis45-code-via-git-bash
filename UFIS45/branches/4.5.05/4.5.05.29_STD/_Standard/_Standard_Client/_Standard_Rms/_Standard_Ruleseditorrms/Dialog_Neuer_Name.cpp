// Dialog_Neuer_Name.cpp : implementation file
//

#include <stdafx.h>
#include <regelwerk.h>
#include <Dialog_Neuer_Name.h>

#include <CedaBasicData.h>
#include <CCSGlobl.h>
#include <BasicData.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDialog_Neuer_Name dialog


CDialog_Neuer_Name::CDialog_Neuer_Name(CWnd* pParent /*=NULL*/)
	: CDialog(CDialog_Neuer_Name::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDialog_Neuer_Name)
	m_Name = _T("");
	//}}AFX_DATA_INIT
}


void CDialog_Neuer_Name::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDialog_Neuer_Name)
	DDX_Text(pDX, IDC_Aktueller_Name, m_Name);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDialog_Neuer_Name, CDialog)
	//{{AFX_MSG_MAP(CDialog_Neuer_Name)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDialog_Neuer_Name message handlers


void CDialog_Neuer_Name::OnOK() 
{
	//--- 
	  this->UpdateData(TRUE) ;

	//--- Check ob Name schon vorhanden 
	  int ilRecordCount = ogBCD.GetDataCount("TPL");

      int i = 0;

	  while(i < ilRecordCount)
	  {
 	         CString olName;
			 olName = ogBCD.GetField("TPL", i, "TNAM");

			 if(olName == m_Name)
			 {
                 AfxMessageBox(GetString(159));
				 return;
			 }

			 if(m_Name.IsEmpty())
			 {
                 AfxMessageBox(GetString(160)) ;
				 return;
			 }

			 i++;
	  }

	CDialog::OnOK();
}




void CDialog_Neuer_Name::OnCancel() 
{
	
	CDialog::OnCancel();
}


//-------------------------------------
//   Focus ver�ndern
//-------------------------------------
BOOL CDialog_Neuer_Name::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	SetWindowText ( GetString ( NAME ) );
	SetDlgItemLangText ( this, IDC_NAME_TXT, IDS_TYPE_IN_NAME );
	SetDlgItemLangText ( this, IDOK, OK );
	SetDlgItemLangText ( this, IDCANCEL, CANCEL );
	
	CEdit* pEdit = (CEdit*) GetDlgItem(IDC_Aktueller_Name);

	       pEdit->SetFocus();


	return FALSE; //TRUE;  // return TRUE unless you set the focus to a control
	                        // EXCEPTION: OCX Property Pages should return FALSE
}
/////////////////////////////////////////////////////////////////////////////
// CPasswordDlg dialog


CPasswordDlg::CPasswordDlg(CWnd* pParent /*=NULL*/)
	: CDialog_Neuer_Name(pParent)
{
	//{{AFX_DATA_INIT(CPasswordDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CPasswordDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CPasswordDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CPasswordDlg, CDialog_Neuer_Name)
	//{{AFX_MSG_MAP(CPasswordDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CPasswordDlg message handlers

void CPasswordDlg::OnOK() 
{
	// TODO: Add extra validation here
	CString olCorrectPw, olInput ;
	olCorrectPw = "AATdarf";
	olCorrectPw += CTime::GetCurrentTime().Format("%d");
	olCorrectPw += pcgHome;
	GetDlgItemText ( IDC_Aktueller_Name, olInput );
	if ( olCorrectPw == olInput )
		CDialog::OnOK();
	else
		CDialog::OnCancel();
}

BOOL CPasswordDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	SetWindowText ( "" );
	SetDlgItemLangText ( this, IDC_NAME_TXT, IDS_AUTHORISATION );
	SetDlgItemLangText ( this, IDOK, OK );
	SetDlgItemLangText ( this, IDCANCEL, CANCEL );
	
	CEdit* pEdit = (CEdit*) GetDlgItem(IDC_Aktueller_Name);
	pEdit->SetPasswordChar ( '*' );
	pEdit->SetFocus();


	return FALSE; //TRUE;  // return TRUE unless you set the focus to a control
	                        // EXCEPTION: OCX Property Pages should return FALSE
}
