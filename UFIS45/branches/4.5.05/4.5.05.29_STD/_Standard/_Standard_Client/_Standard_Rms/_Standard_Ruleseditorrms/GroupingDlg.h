#if !defined(AFX_GROUPINGDLG_H__97742EF3_02D0_11D3_A61B_0000C007916B__INCLUDED_)
#define AFX_GROUPINGDLG_H__97742EF3_02D0_11D3_A61B_0000C007916B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// GroupingDlg.h : header file
//

#include <DialogGrid.h>

/////////////////////////////////////////////////////////////////////////////
// GroupingDlg dialog

class GroupingDlg : public CDialog
{
// Construction
public:
	GroupingDlg(CWnd* pParent = NULL);   // standard constructor
	~GroupingDlg();
// Dialog Data
	//{{AFX_DATA(GroupingDlg)
	enum { IDD = IDD_GROUPING };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(GroupingDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(GroupingDlg)
	virtual void OnCancel();
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	CGridFenster *pomTabGrid;
	CGridFenster *pomSrcGrid;
	CGridFenster *pomGrpGrid;

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_GROUPINGDLG_H__97742EF3_02D0_11D3_A61B_0000C007916B__INCLUDED_)
