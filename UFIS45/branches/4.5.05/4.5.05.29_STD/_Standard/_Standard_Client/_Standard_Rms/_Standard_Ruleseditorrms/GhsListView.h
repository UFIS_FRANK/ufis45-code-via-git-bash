#if !defined(AFX_RULEVIEW_H__86945561_BE3A_11D1_9912_0000B43C16D8__INCLUDED_)
#define AFX_RULEVIEW_H__86945561_BE3A_11D1_9912_0000B43C16D8__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// RuleView.h : header file
//


#include <BasicData.h>
#include <CedaBasicData.h>
// #include "GhsList.h"

/////////////////////////////////////////////////////////////////////////////
// GhsListView dialog

class GhsListView : public CPropertyPage
{
	DECLARE_DYNCREATE(GhsListView)

// Construction
public:
	GhsListView();
	~GhsListView();

	CStringArray omValues;
	CString omCalledFrom;
	CCSPtrArray <RecordSet> omSerData;

	void SetCalledFrom(CString opCalledFrom);
	void SetData(CStringArray &ropValues);
	BOOL GetData(CStringArray &ropValues);

	// Dialog Data
	//{{AFX_DATA_MAP(GhsListView)
	enum { IDD = IDD_GHSVIEW };
	CButton	m_Remove;
	CButton	m_Add;
	CListBox m_VrgcLst;
	CListBox m_VrgcFilter;
	//}}AFX_DATA

// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(GhsListView)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(GhsListView)
	virtual BOOL OnInitDialog();
	afx_msg void OnButtonAdd();
	afx_msg void OnButtonRemove();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()




};
//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_RULEVIEW_H__86945561_BE3A_11D1_9912_0000B43C16D8__INCLUDED_)
