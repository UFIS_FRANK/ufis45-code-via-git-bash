#ifndef __CicNoDemandTableViewer_H__

#include <stdafx.h>
#include <opsscm.h>
#include <CCSGlobl.h>
#include <CCSTable.h>
#include <CcaCedaFlightData.h>
#include <CViewer.h>

/////////////////////////////////////////////////////////////////////////////
// Record structure declaration

struct CICNODEMANDTABLE_LINEDATA
{
	long Urno;
	CString		Flno;
	CTime		Sto;
	CTime		Sto2;
	CString		Act3;
	CString		OrgDes;
	CString		Ftyp;
	CString		Remark;
	bool	    Error;	
	int			Freq;

	CICNODEMANDTABLE_LINEDATA(void)
	{ 
		Urno = 0;
		Sto = -1;
		Sto2= -1;
		Freq = 0;
		Error = false;
	}
};



/////////////////////////////////////////////////////////////////////////////
// CicNoDemandTableViewer

/////////////////////////////////////////////////////////////////////////////
// Class declaration of CicNoDemandTableViewer

//@Man:
//@Memo: CicNoDemandTableViewer
//@See:  STDAFX, CCSCedaData, CedaDIAFLIGHTDATA, CCSPTRARRAY,  CCSTable
/*@Doc:
  No comment on this up to now.
*/
class CicNoDemandTableViewer : public CViewer
{
// Constructions
public:
    //@ManMemo: Default constructor
    CicNoDemandTableViewer();
    //@ManMemo: Default destructor
    ~CicNoDemandTableViewer();

	void UnRegister();

	void ClearAll();

    //@ManMemo: Attach
	void Attach(CCSTable *popAttachWnd);
    //void Attach(CTable *popAttachWnd);
	/* void ChangeViewTo(const char *pcpViewName, CString opDate); */
    //@ManMemo: ChangeViewTo
    void ChangeViewTo(const char *pcpViewName);
    //@ManMemo: omLines
    CCSPtrArray<CICNODEMANDTABLE_LINEDATA> omLines;
// Internal data processing routines
private:
//MWO


	int  CompareFlight(CICNODEMANDTABLE_LINEDATA *prpFlight1, CICNODEMANDTABLE_LINEDATA *prpFlight2);
	
	int CompareLines(CICNODEMANDTABLE_LINEDATA *prpLine1, CICNODEMANDTABLE_LINEDATA *prpLine2);
	
	bool DeleteLine(long lpUrno);
//MWO END


	void MakeLines();
	
	bool MakeLineData(CICNODEMANDTABLE_LINEDATA *prpLineData, CCAFLIGHTDATA *prpFlight);
	bool MakeLineData(CICNODEMANDTABLE_LINEDATA *prpLineData, CCSPtrArray<CCAFLIGHTDATA> &opFlights);


	bool MakeLine(CCAFLIGHTDATA *prpFlight);
	bool MakeLine( CCSPtrArray<CCAFLIGHTDATA> &opFlights);

	
	bool MakeColList(CICNODEMANDTABLE_LINEDATA *prlLine, CCSPtrArray<TABLE_COLUMN> &olColList);
	
	int  CreateLine(CICNODEMANDTABLE_LINEDATA &rpLine);

	bool FindLine(long lpUrno, int &rilLineno);
	
	void SelectLine(long ilCurrUrno);

	void InsertDisplayLine( int ipLineNo);



// Operations
public:
    //@ManMemo: DeleteAll
	void DeleteAll();
    //@ManMemo: DeleteLine
	void DeleteLine(int ipLineno);
 
// Window refreshing routines
public:
	void UpdateDisplay();

	void DrawHeader();


	void ProcessCcaChange(long *plpCcaUrno);
	void ProcessDemandChange(long *plpCcaUrno);
	void ProcessKKeyChange(long *plpCcaUrno);
	void ProcessFlightChange(CCAFLIGHTDATA *plpCcaUrno, int ipDDXType);


	

	bool bmInit;


private:
	CCSTable *pomTable;


// Methods which handle changes (from Data Distributor)
public:


};

#endif //__CicNoDemandTableViewer_H__
