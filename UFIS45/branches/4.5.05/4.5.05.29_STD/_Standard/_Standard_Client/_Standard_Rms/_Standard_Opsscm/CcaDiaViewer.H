// stviewer.h : header file
//

#ifndef _CcaDIAVIEWER_H_
#define _CcaDIAVIEWER_H_

#include <cviewer.h>
#include <CCSPrint.h>
#include <CcaCedaFlightData.h>
#include <CCSGlobl.h>
#include <CedaBasicData.h>

struct CcaDIA_INDICATORDATA
{
	// standard data for indicators in general GanttBar
	CTime StartTime;
	CTime EndTime;
	COLORREF Color;
};

struct CcaDIA_BARDATA 
{
	long Urno; // CCA
	
	long DUrno;
	long Rkey;
    CString Text;
	CString Ctyp;
    CTime StartTime;
    CTime EndTime;

	//CCSPtrArray<TIMEFRAMEDATA> Times;

	long OrdDigit;

	bool IsSelected;
	//bool AlcKonf;
	CString StatusText;
    int FrameType;
    int MarkerType;
    CBrush *MarkerBrush;
	CBrush *TriangleLeft;
	CBrush *TriangleRight;
	COLORREF TextColor;
	COLORREF TriangelColorRight;
	COLORREF TriangelColorLeft;
    int OverlapLevel;       // zero for the top-most level, each level lowered by 4 pixels
	CCSPtrArray<CcaDIA_INDICATORDATA> Indicators;
	CcaDIA_BARDATA(void)
	{
		//AlcKonf = false;
		IsSelected = false;
		DUrno = 0;
		Rkey = 0;
		StartTime = TIMENULL;
		EndTime = TIMENULL;
		OrdDigit = 0;
	}

};


struct CcaDIA_BKBARDATA
{
	long AUrno;
	long DUrno;
	long ARkey;
	long DRkey;
    CString Text;
	CTime StartTime;
    CTime EndTime;
    CBrush *MarkerBrush;	// background bar will be only displayed in FRAMENONE, MARKFULL
	bool ThickLineSeparator;
	CcaDIA_BKBARDATA(void)
	{
		AUrno = 0;
		DUrno = 0;
		ARkey = 0;
		DRkey = 0;
		Text = "";
		StartTime = TIMENULL;
		EndTime  = TIMENULL;
		MarkerBrush = NULL;
		ThickLineSeparator = false;
	}
};
struct CcaDIA_LINEDATA 
{
	long Urno;
	CString Bnam;
	CString Term;
	CTime StartTime;
	CTime EndTime;
    CString Text;
	bool IsExpanded;
    int MaxOverlapLevel;    // maximum number of overlapped level of bars in this line
    CCSPtrArray<CcaDIA_BARDATA> Bars;		// in order of painting (leftmost is the bottommost)
    CCSPtrArray<CcaDIA_BKBARDATA> BkBars;	// background bar
    CCSPtrArray<int> TimeOrder;				// maintain overlapped bars, see below
	COLORREF TextColor;
	COLORREF TextColor2;
	COLORREF TextColor3;
	bool Show;
	CcaDIA_LINEDATA(void)
	{
		Show = true;
		IsExpanded = false;
	}
};


struct CcaDIA_GROUPDATA 
{
	int GroupNo;
	CString GroupName;			// Gruppen ID
	CString GroupType;			// 
	CCSPtrArray<CString> Codes; // For future Airlinecodes
	// standard data for groups in general Diagram
    CString Text;
    CCSPtrArray<CcaDIA_LINEDATA> Lines;
};

// Attention:
// Element "TimeOrder" in the struct "CcaDIA_LINEDATA".
//
// This array will maintain bars sorted by StartTime of each bar.
// You can see it the same way you see an array of index in the array CcaDIA_BARDATA[].
// For example, if there are five bars in the first overlapped group, and their
// overlap level are [3, 0, 4, 1, 2]. So the indexes to the CcaDIA_BARDATA[] sorted by
// the beginning of time should be [1, 3, 4, 0, 2]. You can see that the indexes
// in these two arrays are cross-linked together. Please notice that we define
// the overlap level to be the same number as the order of the bar when sorted
// by StartTime.
//
// However, I keep the relative offset in this array, not the absolute offset.
// The relative offset helps us separate each group of bar more efficiently.
// For example, using the same previous example, the relative offset of index
// to the array CcaDIA_BARDATA[] kept in this array would be [+1, +2, +3, -3, -2].
//
// For a given "i", the bar in the same overlap group will always have the
// same value of "i - Bars[i + TimeOrder[i]].OverlapLevel". This value will be the
// index of the first member in that group.
//
// Notes: This bar order array should be implemented with CCSArray <int>.


/////////////////////////////////////////////////////////////////////////////
// CcaDiagramViewer window

class CcaGantt;

class CcaDiagramViewer: public CViewer
{
	friend CcaGantt;

// Constructions
public:
    CcaDiagramViewer();
    ~CcaDiagramViewer();

	void Attach(CWnd *popAttachWnd);
	void ChangeViewTo(const char *pcpViewName, CTime opStartTime, CTime opEndTime);

	void SetDOO( int ipDOO) { imDOO = ipDOO;};

	int GetLastOverlappleverInTimeframe(int ipGroupno, int ipLineno,	CTime opTime1, CTime opTime2, int ipActualOverlaplevel);


	void UtcToLocal(CcaDIA_BARDATA *prlBar);
	void UtcToLocal(CcaDIA_BKBARDATA *prlBar);


// Internal data processing routines
private:
	void PrepareGrouping();
	void PrepareFilter();
	void PrepareSorter();
	bool IsPassFilter(RecordSet &ropRec);
	bool IsPassFilter(DIACCADATA *prpCca);


	int CompareGroup(CcaDIA_GROUPDATA *prpGroup1, CcaDIA_GROUPDATA *prpGroup2);
	int CompareLine(CcaDIA_LINEDATA *prpLine1, CcaDIA_LINEDATA *prpLine2);

	void MakeGroups();
	void MakeLines();
	void MakeBars();
	void MakeLine(RecordSet &ropRot, bool blThickSeparator = false);
	void MakeLineData(CcaDIA_LINEDATA *prlLine, RecordSet &ropRot, bool blShowLine = true);
	void CheckLines();

	
	void MakeTriangleColors(CCAFLIGHTDATA *prpFlightA, CCAFLIGHTDATA *prpFlightD, CcaDIA_BARDATA *prpBar);

	void MakeBarData(CcaDIA_BARDATA *prlBar, DIACCADATA *popCca);
	bool MakeBar(int ipGroupno, int ipLineno, DIACCADATA *popCca);

	bool MakeBarData(CcaDIA_BARDATA *prlBar, CCSPtrArray<DIACCADATA> &opCcas);
	bool MakeBar(int ipGroupno, int ipLineno, CCSPtrArray<DIACCADATA> &opCcas);


	long GetRkeyFromRotation(CcaCedaFlightData::RKEYLIST *popRotation);
	
	bool GetFlightsInRotation(CcaCedaFlightData::RKEYLIST *popRotation, CCAFLIGHTDATA *prpFlightA, CCAFLIGHTDATA *prpFlightD);
	CCAFLIGHTDATA * GetFlightAInRotation(CcaCedaFlightData::RKEYLIST *popRotation);
	CCAFLIGHTDATA * GetFlightDInRotation(CcaCedaFlightData::RKEYLIST *popRotation);

	CString GroupText(int ipGroupNo);
	CString LineText(CcaCedaFlightData::RKEYLIST *popRot);
	CString BarText(CcaCedaFlightData::RKEYLIST *popRot);
	CString BarTextAndValues(CcaDIA_BARDATA *prlBar);
	CString StatusText(CcaCedaFlightData::RKEYLIST *popRot);
	int FindGroup(CString opGroupName);
	bool FindLine(CString opBnam, CUIntArray &ropGroups, CUIntArray &ropLines);
	bool FindGroupsForType(CUIntArray &ropGroupNos, CString opType, CString opPfc);
	BOOL FindDutyBar(long lpUrno, int &ripGroupno, int &ripLineno);
	bool FindDutyBars(long lpUrno, CUIntArray &ropGroups, CUIntArray &ropLines);
	BOOL FindBar(long lpUrno, int &ripGroupno, int &ripLineno, int &ripBarno);
	bool FindBarsGlobal(long lpUrno, CUIntArray &ropGroups, 
						CUIntArray &ropLines, CUIntArray &ropBars);

// Operations
private:
	int omGeometryFontIndex;
	CTimeSpan omGeometryTimeSpan;
	bool bmGeometryTimeLines;
	bool bmGeometryMinimize;
	CTime omStartShowTime;
	int   imGeometrieHours;
public:

	void SelectBar(CcaDIA_BARDATA *prpBar);
	void DeSelectAll();
    int GetGroupCount();
    CcaDIA_GROUPDATA *GetGroup(int ipGroupno);
    CString GetGroupText(int ipGroupno);
    CString GetGroupTopScaleText(int ipGroupno);
	int GetGroupColorIndex(int ipGroupno,CTime opStart,CTime opEnd);
    int GetLineCount(int ipGroupno);
    CcaDIA_LINEDATA *GetLine(int ipGroupno, int ipLineno);
    CString GetLineText(int ipGroupno, int ipLineno);
    int GetMaxOverlapLevel(int ipGroupno, int ipLineno);
	bool IsExpandedLine(int ipGroupno, int ipLineno);
	void SetExpanded(int ipGroupno, int ipLineno);
	int GetVisualMaxOverlapLevel(int ipGroupno, int ipLineno);
    int GetBkBarCount(int ipGroupno, int ipLineno);
    CcaDIA_BKBARDATA *GetBkBar(int ipGroupno, int ipLineno, int ipBkBarno);
    CString GetBkBarText(int ipGroupno, int ipLineno, int ipBkBarno);
    int GetBarCount(int ipGroupno, int ipLineno);
    CcaDIA_BARDATA *GetBar(int ipGroupno, int ipLineno, int ipBarno);
    CString GetBarText(int ipGroupno, int ipLineno, int ipBarno);
    CString GetStatusBarText(int ipGroupno, int ipLineno, int ipBarno);
	int GetIndicatorCount(int ipGroupno, int ipLineno, int ipBarno);
	CcaDIA_INDICATORDATA *GetIndicator(int ipGroupno, int ipLineno, int ipBarno,
		int ipIndicatorno);

    void DeleteAll();
    int CreateGroup(CcaDIA_GROUPDATA *prpGroup);
    void DeleteGroup(int ipGroupno);
    int CreateLine(int ipGroupno, CcaDIA_LINEDATA *prpLine);
    void DeleteLine(int ipGroupno, int ipLineno);
    int CreateBkBar(int ipGroupno, int ipLineno, CcaDIA_BKBARDATA *prpBkBar);
    void DeleteBkBar(int ipGroupno, int ipLineno, int ipBkBarno);
    int CreateBar(int ipGroupno, int ipLineno, CcaDIA_BARDATA *prpBar, BOOL bpFrontBar = TRUE);
    void DeleteBar(int ipGroupno, int ipLineno, int ipBarno, bool bpSetOverlapColor = true);
    int CreateIndicator(int ipGroupno, int ipLineno, int ipBarno,
    	CcaDIA_INDICATORDATA *prpIndicator);
	void DeleteIndicator(int ipGroupno, int ipLineno, int ipBarno, int ipIndicator);

    int GetBarnoFromTime(int ipGroupno, int ipLineno, CTime opTime1, CTime opTime2,
        int ipOverlapLevel1, int ipOverlapLevel2);
    int GetBkBarnoFromTime(int ipGroupno, int ipLineno, CTime opTime1, CTime opTime2);
	void AllowUpdates(BOOL bpNoUpdatesNow);
	int GetGeometryFontIndex(){return omGeometryFontIndex;}
	CTimeSpan GetGeometryTimeSpan(){return omGeometryTimeSpan;}
	bool GetGeometryTimeLines(){return bmGeometryTimeLines;}
	bool GetGeometryMinimize(){return bmGeometryMinimize;}
	CTime GetGeometrieStartTime(){return omStartShowTime;}
	void MakeMasstab();


	void SetCnams( CString opCnams ) { omCnams = opCnams;};

	void SetLoadTimeFrame(CTime opStartDate, CTime opEndDate);

	bool GetDispoZeitraumFromTo(CTime &ropFrom, CTime &ropTo);
// Private helper routines
private:
	void GetOverlappedBarsFromTime(int ipGroupno, int ipLineno,
		CTime opTime1, CTime opTime2, CUIntArray &ropBarnoList);
	void GetGroupsFromViewer(CCSPtrArray<CcaDIA_GROUPDATA> &ropGroups);
	void PrintPrepareLineData(int ipGroupNo,int ipLineno,CCSPtrArray<PRINTBARDATA> &ropPrintLine);
	void CcaDiagramViewer::PrintDiaArea(CPtrArray &opPtrArray,int ipGroupNo);
	void CcaDiagramViewer::PrintDiagramHeader(int ipGroupno);
// Attributes used for filtering condition
public:
	int omGroupBy;			// enumerated value -- define in "stviewer.cpp"
	CStringArray omSortOrder;
	CTime omStartTime;
	CTime omEndTime;
	CTime omLoadStartTime;
	CTime omLoadEndTime;
// Attributes
private:
	CWnd *pomAttachWnd;
	CBrush omBkBrush;
	CBrush omBreakBrush;
	CBrush omBkBreakBrush;
	CBrush omWIFBkBrush;
	int	igFirstGroupOnPage;

	CString omCnams;
	int imDOO;

// Methods which handle changes (from Data Distributor)
public:
    CCSPtrArray<CcaDIA_GROUPDATA> omGroups;

	
/*	
	void ProcessFlightInsert(CUIntArray *popRkeys);
	void ProcessFlightChange(CCAFLIGHTDATA *prpFlight);
	void ProcessFlightDelete(CCAFLIGHTDATA *prpFlight);
*/

// MCU 22.09.98	void ProcessCcaChange(long *prpCcaUrno);
	void ProcessCcaChange(long *prpCcaUrno,int ipDDXType);
	void ProcessKKeyChange(long *prpCcaUrno,int ipDDXType);

	void ProcessBlkChange( RecordSet * popBlkRecord );
	void ProcessCicChange( RecordSet * popBlkRecord );

public:
	BOOL bmNoUpdatesNow;
	BOOL bmIsFirstGroupOnPage;

//Printing routines
	void PrintGantt(CPtrArray &opPtrArray);
	


///////////////////////////////////////////////////////////////////////////////////////////
// Printing routines
private:
	CCSPrint *pomPrint;

public:
};

/////////////////////////////////////////////////////////////////////////////

#endif
