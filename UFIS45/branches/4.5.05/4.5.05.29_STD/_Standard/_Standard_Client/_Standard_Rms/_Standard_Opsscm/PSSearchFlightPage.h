#if !defined(AFX_SEARCHFLIGHTPAGE_H__E58F1C91_50FA_11D1_9884_000001014864__INCLUDED_)
#define AFX_SEARCHFLIGHTPAGE_H__E58F1C91_50FA_11D1_9884_000001014864__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// searchflightpage.h : header file
//

#include <Ansicht.h>
//#include "CCSBasePSPage.h"
//#include "CCSString.h"
#include <CCSPtrArray.h>
#include <CCSEdit.h>
//#include "CCSPageBuffer.h"
/////////////////////////////////////////////////////////////////////////////
// CSearchFlightPage dialog

class CSearchFlightPage : public CPropertyPage
{
	DECLARE_DYNCREATE(CSearchFlightPage)

// Construction
public:
	CSearchFlightPage();
	~CSearchFlightPage();

	void SetCalledFrom(CString opCalledFrom);
	CString omCalledFrom;
// Dialog Data
	//{{AFX_DATA(CSearchFlightPage)
	enum { IDD = IDD_PSSEARCH_FLIGHT_PAGE };
	CButton	m_Check1;
	CButton	m_Check2;
	CButton	m_Check3;
	CButton	m_Check4;
	CButton	m_Check5;
	CButton	m_Check6;
	CButton	m_Check7;
	CButton	m_Check8;
	CButton	m_Check9;
	CButton	m_Check10;
	CButton	m_Check11;
	CButton	m_Check12;
	CButton	m_Rotation;
	CEdit	m_Edit1;   // TIFA From || TIFD From; Date
	CEdit	m_Edit2;   // TIFA From || TIFD From; Time
	CEdit	m_Edit3;   // TIFA To || TIFD To; Date
	CEdit	m_Edit4;   // TIFA To || TIFD To; Time
	CEdit	m_Edit5;   // SearchHours
	CEdit	m_Edit6;   // SearchDay
	CEdit	m_Edit7;   // DOOA DOOD
	CEdit	m_Edit8;   // RelHBefore
	CEdit	m_Edit9;   // RelHAfter
	CEdit	m_Edit10;  // LSTU; Date
	CEdit	m_Edit11;  // LSTU; Time
	CEdit	m_Edit12;  // FDAT; Date
	CEdit	m_Edit13;  // FDAT; Time
	CEdit	m_Edit14;  // ACL2
	CEdit	m_Edit15;  // FLTN
	CEdit	m_Edit16;  // FLNS
	CEdit	m_Edit17;  // REGN
	CEdit	m_Edit18;  // ACT3
	CEdit	m_Edit19;  // ORG3 || ORG4 || DES3 || DES4
	CEdit	m_Edit20;  // TIFA Flugzeit || TIFD Flugzeit; Date
	CEdit	m_Edit21;  // TIFA Flugzeit || TIFD Flugzeit; Time
	CEdit	m_Edit22;  // TTYP
	CEdit	m_Edit23;  // STEV
	//}}AFX_DATA

	//CCSPtrArray<CCSEdit> omEdits;
	//CCSPtrArray<CButton> omButtons;
	CStringArray omValues;

	void GetData();
	void GetData(CStringArray &ropValues);
	void SetData();
	void SetData(CStringArray &ropValues);
//	CCSItem omFieldList; //("TIFA From || TIFD From;Date,TIFA From || TIFD From; Time," 
		                 //"TIFA To || TIFD To; Date,TIFA To || TIFD To; Time," 
						 //"SearchHours, SearchDay,DOOA || DOOD," 
		                 //"RelHBefore,RelHAfter,LSTU; Date,LSTU; Time," 
						 //"FDAT; Date,FDAT; Time," 
						 //"ALC2,FLTN,FLNS,REGN,ACT3,ORG3 || ORG4 || DES3 || DES4,"
						 //"TIFA Flugzeit || TIFD Flugzeit; Date,"
						 //"TIFA Flugzeit || TIFD Flugzeit; Time,TTYP,STEV");
/*   struct DISPStrukt
   {
	   CString omFieldName;
	   CString omOperator;
	   CString omData;
	   CString omFormatIn;
	   CString omFormatOut;
	   DISPStrukt()
	   {
		   omFieldName = "";
		   omOperator="";
		   omData="";
		   omFormatIn="";
		   omFormatOut="";
		};
   };
   CCSPtrArray<DISPStrukt> omData;
*/
   
// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(CSearchFlightPage)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

//   void ResetBufferPtr(){CBasePropSheetPage::ResetBufferPtr(); pomPageBuffer = NULL;};
//   void *GetData();
//   void InitialScreen();
   CString GetPrivList();
protected:
//	CPageBuffer *pomPageBuffer;
//	int CreateFilter();
	void InitPage();
	// Generated message map functions
	//{{AFX_MSG(CSearchFlightPage)
		afx_msg void OnClickedAbflug();
		afx_msg void OnClickedAnkunft();
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SEARCHFLIGHTPAGE_H__E58F1C91_50FA_11D1_9884_000001014864__INCLUDED_)
