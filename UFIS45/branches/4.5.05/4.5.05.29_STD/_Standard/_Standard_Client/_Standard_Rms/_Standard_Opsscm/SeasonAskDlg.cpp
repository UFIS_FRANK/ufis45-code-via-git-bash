// SeasonAskDlg.cpp : implementation file
//

#include <stdafx.h>
//#include "fpms.h"
#include <SeasonAskDlg.h>
#include <SeasonCedaFlightData.h>
#include <CCSGlobl.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// SeasonAskDlg dialog


SeasonAskDlg::SeasonAskDlg(CWnd* pParent /*=NULL*/)
	: CDialog(SeasonAskDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(SeasonAskDlg)
	m_CC_ViewActiv = FALSE;
	//}}AFX_DATA_INIT
	Create(IDD,pParent);
	ShowWindow(SW_SHOWNORMAL);
}

SeasonAskDlg::~SeasonAskDlg(void)
{
}


void SeasonAskDlg::SetMessage(CString opMessage)
{
	m_MsgList.AddString(opMessage);
	if ((m_MsgList.GetCount()-6) > 0)
		m_MsgList.SetTopIndex(m_MsgList.GetCount()-6);
	pogSeasonAskDlg->UpdateWindow();

	SetWindowPos(&wndTop,0,0,0,0, SWP_NOSIZE | SWP_NOMOVE | SWP_SHOWWINDOW);
}


void SeasonAskDlg::RemoveAllMessage()
{
	m_MsgList.ResetContent();
	pogSeasonAskDlg->UpdateWindow();
}



BOOL SeasonAskDlg::DestroyWindow() 
{
	BOOL blRc = CDialog::DestroyWindow();
	pogSeasonAskDlg = NULL;
	delete this;
	return blRc;
}

void SeasonAskDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(SeasonAskDlg)
	DDX_Control(pDX, IDCANCEL, m_CB_Cancel);
	DDX_Control(pDX, IDC_DOWNLOAD, m_CB_DownLoad);
	DDX_Control(pDX, IDC_MSGLIST, m_MsgList);
	DDX_Check(pDX, IDC_VIEWACTIV, m_CC_ViewActiv);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(SeasonAskDlg, CDialog)
	//{{AFX_MSG_MAP(SeasonAskDlg)
	ON_BN_CLICKED(IDC_DOWNLOAD, OnDownload)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// SeasonAskDlg message handlers

void SeasonAskDlg::OnCancel() 
{
	//ogSeasonFlights.omISFQueue.DeleteAll();
	DestroyWindow();	
}

void SeasonAskDlg::OnDownload() 
{
	/*
	int ilRet = m_CC_ViewActiv.GetStatus();

	bool blViewActiv = false; 
	if(ilRet == 1)
		blViewActiv = true; 
*/

	m_CB_Cancel.EnableWindow(FALSE);
	m_CB_DownLoad.EnableWindow(FALSE);
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
	//ogSeasonFlights.ISFQueueLoad(false);
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	DestroyWindow();	
}
