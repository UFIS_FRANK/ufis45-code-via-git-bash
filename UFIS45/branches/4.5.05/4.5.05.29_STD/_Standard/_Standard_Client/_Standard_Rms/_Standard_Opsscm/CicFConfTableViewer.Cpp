// CicFConfTableViewer.cpp : implementation file
// 
// Modification History: 


#include <stdafx.h>
#include <CicFConfTableViewer.h>
#include <CcsGlobl.h>
#include <iomanip.h>
#include <iostream.h>
#include <fstream.h>
#include <BasicData.h>
#include <CedaBasicData.h>
#include <CcaCedaFlightConfData.h>
#include <CcaCedaFlightData.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif

// Local function prototype
static void FlightConflictTableCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName);




/////////////////////////////////////////////////////////////////////////////
// CicFConfTableViewer
//

CicFConfTableViewer::CicFConfTableViewer()
{
    pomTable = NULL;
	pomCflData = NULL;
	bmInit = false;
}


void CicFConfTableViewer::UnRegister()
{
	ogDdx.UnRegister(this, NOTUSED); 
}


CicFConfTableViewer::~CicFConfTableViewer()
{
	ogDdx.UnRegister(this, NOTUSED);
    DeleteAll();
}


// Connects the viewer with a table
void CicFConfTableViewer::Attach(CCSTable *popTable)
{
    pomTable = popTable;
}


// Clears the table of the viewer
void CicFConfTableViewer::ClearAll()
{
	bmInit = false;
	ogDdx.UnRegister(this, NOTUSED);
    DeleteAll();
    pomTable->ResetContent();
}


void CicFConfTableViewer::DeleteAll()
{
    omLines.DeleteAll();
}



void CicFConfTableViewer::DeleteLine(int ipLineno)
{
	omLines.DeleteAt(ipLineno);

	pomTable->DeleteTextLine(ipLineno);

}



bool CicFConfTableViewer::DeleteLine(long lpUrno)
{
	bool blRet = false;
    for (int i = omLines.GetSize() - 1; i >= 0; i--)
	{
	  if (omLines[i].Urno == lpUrno)
	  {
		omLines.DeleteAt(i);
		pomTable->DeleteTextLine(i);
		blRet = true;
	  }
	}
	return blRet;
}




// Change the viewed database 
void CicFConfTableViewer::ChangeViewTo(const CedaCflData *popCflData)
{
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
	
	ogDdx.UnRegister(this, NOTUSED);
	
	//ogDdx.Register(this, CFL_CHANGE, CString("CFL_CHANGE"), CString("CFL_CHANGE"), FlightConflictTableCf);
	//ogDdx.Register(this, CCA_KKEY_CHANGE,	CString("FCONFDIAVIEWER"), CString("Flight Changed"), FlightTableCf);
	
	pomCflData = popCflData;

	DrawHeader();
    pomTable->DisplayTable();

    pomTable->ResetContent();
    DeleteAll();    
	MakeLines();
   	UpdateDisplay();

	bmInit = true;
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));

}


// Transfer the data of the database to the table
void CicFConfTableViewer::MakeLines(void)
{
	POSITION pos;
	long llCflUrno;
	CFLDATA *prlFlightConflict;

	for( pos = pomCflData->omUrnoMap.GetStartPosition(); pos != NULL; )
	{
		pomCflData->omUrnoMap.GetNextAssoc( pos, (void *&)llCflUrno , (void *&)prlFlightConflict );
	
		MakeLine(prlFlightConflict);
	}

}


// Insert one line into the table
bool  CicFConfTableViewer::MakeLine(const CFLDATA *prpFlightConflict)
{
	CICFCONFTABLE_LINEDATA rlLineData;
	if(MakeLineData(&rlLineData, prpFlightConflict))
	{
		int ilLineNo = CreateLine(rlLineData);
		InsertDisplayLine(ilLineNo);
	}
	return true;
}




// Copy the data from the db-record to the table-record
bool CicFConfTableViewer::MakeLineData(CICFCONFTABLE_LINEDATA *prpLineData, const CFLDATA *prpFlightConflict) const
{

	prpLineData->Urno = prpFlightConflict->Urno;
	
	if (pomCflData == &ogCflData)
	{
		CCAFLIGHTCONFDATA *prlAftData = pomCflData->omFlightData.GetFlightByUrno(prpFlightConflict->Rurn);
		prpLineData->Flno = prlAftData->Flno;
		prpLineData->Stod = prlAftData->Stod;
		prpLineData->Des3 = prlAftData->Des3;
		prpLineData->Act3 = prlAftData->Act3;
		prpLineData->Akus = prpFlightConflict->Akus;
		prpLineData->LblOvalNval = ogBCD.GetField("LBL", "TKEY", prpFlightConflict->Meno, "TEXT") + " " +
			CString(prpFlightConflict->Oval) + " / " + CString(prpFlightConflict->Nval);

	}
	else
	{
		CCAFLIGHTDATA *prlAftData = ogCcaDiaFlightData.GetFlightByUrno(prpFlightConflict->Rurn);
		prpLineData->Flno = prlAftData->Flno;
		prpLineData->Stod = prlAftData->Stod;
		prpLineData->Des3 = prlAftData->Des3;
		prpLineData->Act3 = prlAftData->Act3;
		prpLineData->Akus = prpFlightConflict->Akus;
		prpLineData->LblOvalNval = ogBCD.GetField("LBL", "TKEY", prpFlightConflict->Meno, "TEXT") + " " +
			CString(prpFlightConflict->Oval) + " / " + CString(prpFlightConflict->Nval);
	}

	return true;	
	
}



int CicFConfTableViewer::CreateLine(const CICFCONFTABLE_LINEDATA &rpLine)
{
    for (int ilLineno = omLines.GetSize(); ilLineno > 0; ilLineno--)
	{
		if (CompareLines(&rpLine, &omLines[ilLineno-1]) >= 0)
		{
            break;  // should be inserted after Lines[ilLineno-1]
		}
	}

    omLines.NewAt(ilLineno, rpLine);
	return ilLineno;
}


// Get the table line number of the record with the given 'Urno'
bool CicFConfTableViewer::FindLine(long lpUrno, int &rilLineno) const
{
	rilLineno = -1;
    for (int i = omLines.GetSize() - 1; i >= 0; i--)
	{
	  if((omLines[i].Urno == lpUrno))
	  {
		rilLineno = i;
		return true;
	  }
	}
	return false;
}


// Get the 'urno' of the given table line number
long CicFConfTableViewer::GetUrno(int ipLineNo, long &lpUrno) const {

	if (ipLineNo >= omLines.GetSize() || ipLineNo < 0)
	{
		return FALSE;
	}

	lpUrno = omLines[ipLineNo].Urno;
	return TRUE;
}




/////////////////////////////////////////////////////////////////////////////
// CicFConfTableViewer - display drawing routine



// UpdateDisplay: Load data to the display by using "omTable"
void CicFConfTableViewer::UpdateDisplay()
{
	pomTable->ResetContent();

	DrawHeader();

	CICFCONFTABLE_LINEDATA *prlLine;
    
	for (int ilLc = 0; ilLc < omLines.GetSize(); ilLc++)
	{
		prlLine = &omLines[ilLc];
		CCSPtrArray<TABLE_COLUMN> olColList;
	
		MakeColList(prlLine, olColList);

		pomTable->AddTextLine(olColList, (void*)(prlLine));
		olColList.DeleteAll();
	}


    pomTable->DisplayTable();
}



void CicFConfTableViewer::InsertDisplayLine(int ipLineNo)
{
	if(!((ipLineNo >= 0) && (ipLineNo < omLines.GetSize())))
	{
		return;
	}
	CCSPtrArray<TABLE_COLUMN> olColList;
	MakeColList(&omLines[ipLineNo], olColList);
	pomTable->InsertTextLine(ipLineNo, olColList, &omLines[ipLineNo]);
	olColList.DeleteAll();
    pomTable->DisplayTable();
}






void CicFConfTableViewer::DrawHeader()
{
	CCSPtrArray<TABLE_HEADER_COLUMN> omHeaderDataArray;

	pomTable->SetShowSelection(true);
	pomTable->ResetContent();

	TABLE_HEADER_COLUMN rlHeader;
	rlHeader.Font = &ogCourier_Bold_10;

	// AFT-Data
	rlHeader.Length = 70; 
	rlHeader.Alignment = COLALIGN_CENTER;
	rlHeader.Text = GetString(IDS_STRING1691);//CString("Abflug")
	omHeaderDataArray.New(rlHeader);

	rlHeader.Length = 120; 
	rlHeader.Alignment = COLALIGN_CENTER;
	rlHeader.Text = GetString(IDS_STRING1692);//CString("STD")
	omHeaderDataArray.New(rlHeader);

	rlHeader.Length = 25; 
	rlHeader.Alignment = COLALIGN_CENTER;
	rlHeader.Text = GetString(IDS_STRING1693);//CString("Dest")
	omHeaderDataArray.New(rlHeader);

	rlHeader.Length = 25; 
	rlHeader.Alignment = COLALIGN_CENTER;
	rlHeader.Text = GetString(IDS_STRING1694);//CString("A/C")
	omHeaderDataArray.New(rlHeader);

	
	rlHeader.Length = 80; 
	rlHeader.Alignment = COLALIGN_CENTER;
	rlHeader.Text = GetString(IDS_STRING1659);//CString("Best�tigt")
	omHeaderDataArray.New(rlHeader);



	// CFL-Data
	rlHeader.Length = 500; 
	rlHeader.Alignment = COLALIGN_CENTER;
	rlHeader.Text = GetString(IDS_STRING1695);//CString("Konflikte: Alter Wert / Neuer Wert")
	omHeaderDataArray.New(rlHeader);

	
	pomTable->SetHeaderFields(omHeaderDataArray);
	omHeaderDataArray.DeleteAll();

	pomTable->SetDefaultSeparator();

}




// Fills one row of the table
bool CicFConfTableViewer::MakeColList(const CICFCONFTABLE_LINEDATA *prlLine, CCSPtrArray<TABLE_COLUMN> &olColList) const
{
	TABLE_COLUMN rlColumnData;

	rlColumnData.VerticalSeparator = SEPA_NONE;
	rlColumnData.SeparatorType = SEPA_NONE;
	rlColumnData.Font = &ogCourier_Regular_9;

	// AFT-Data
	rlColumnData.Alignment = COLALIGN_LEFT;
	rlColumnData.Text = prlLine->Flno;
	olColList.NewAt(olColList.GetSize(), rlColumnData);

	rlColumnData.Alignment = COLALIGN_LEFT;
	CTime olStod = prlLine->Stod;
	if (bgGatPosLocal) ogBasicData.UtcToLocal(olStod);
//	rlColumnData.Text =  prlLine->Stod.Format("%d.%m.%Y %H:%M");
	rlColumnData.Text =  olStod.Format("%d.%m.%Y %H:%M");
	olColList.NewAt(olColList.GetSize(), rlColumnData);

	rlColumnData.Alignment = COLALIGN_LEFT;
	rlColumnData.Text = prlLine->Des3;
	olColList.NewAt(olColList.GetSize(), rlColumnData);

	rlColumnData.Alignment = COLALIGN_LEFT;
	rlColumnData.Text = prlLine->Act3;
	olColList.NewAt(olColList.GetSize(), rlColumnData);

	rlColumnData.Alignment = COLALIGN_LEFT;
	rlColumnData.Text = prlLine->Akus;
	olColList.NewAt(olColList.GetSize(), rlColumnData);


	// CFL-Data
	rlColumnData.Alignment = COLALIGN_LEFT;
	rlColumnData.Text = prlLine->LblOvalNval;
	olColList.NewAt(olColList.GetSize(), rlColumnData);

	return true;
}


/*
static void FlightConflictTableCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName)
{
    CicFConfTableViewer *polViewer = (CicFConfTableViewer *)popInstance;

	if(ipDDXType == CFL_CHANGE)
		polViewer->ProcessCflChange((long *)vpDataPointer);

	if(ipDDXType == CCA_KKEY_CHANGE)
		polViewer->ProcessKKeyChange((long *)vpDataPointer);

}
*/



// Compares two lines of the table
int CicFConfTableViewer::CompareLines(const CICFCONFTABLE_LINEDATA *prpLine1, const CICFCONFTABLE_LINEDATA *prpLine2) const 
{

	return (prpLine1->Urno == prpLine2->Urno)? 0:	
		   (prpLine1->Urno > prpLine2->Urno)? 1: -1;
	
}





/*
void CicFConfTableViewer::ProcessCflChange(long *plpCflUrno)
{
	CFLDATA *prlCfl =ogCflData.GetCflByUrno(*plpCflUrno);
	if(prlCfl == NULL)
	{
		ChangeViewTo(ogCflData);
		
		return;
	}

	DeleteLine(prlCfl->Urno);
	MakeLine(prlCfl);
}
*/
/*
void CicFConfTableViewer::ProcessKKeyChange(long *prpCcaUrno)
{
	CCSPtrArray<DIACCADATA> olCcas;	
	CCAFLIGHTDATA *prlFlight;	
	CCSPtrArray<CCAFLIGHTDATA> rlFlights;	

	if(ogCcaDiaFlightData.omCcaData.GetCcasByKKey(olCcas, *prpCcaUrno))
	{
		if(olCcas.GetSize() > 0)
		{
			for(int i = olCcas.GetSize() -1 ; i >= 0; i--)
			{
				prlFlight = ogCcaDiaFlightData.GetFlightByUrno(olCcas[i].Flnu);	

				if(prlFlight != NULL)
					rlFlights.Add(prlFlight);

				DeleteLine(olCcas[i].Flnu);
			}
			if(rlFlights.GetSize() >0 )
			{
				MakeLine(rlFlights);
			}

		}
	}
	olCcas.RemoveAll();
}

*/
