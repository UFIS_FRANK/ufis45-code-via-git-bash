#ifndef __CicFConfTableViewer_H__

#include <stdafx.h>
#include <opsscm.h>
#include <CCSGlobl.h>
#include <CCSTable.h>
#include <CedaCflData.h>
#include <CViewer.h>

/////////////////////////////////////////////////////////////////////////////
// Record structure declaration

struct CICFCONFTABLE_LINEDATA
{
	long Urno;
	CString		Flno;
	CTime		Stod;
	CString		Des3;
	CString		Act3;
	CString		Akus;
	CString		LblOvalNval;

	CICFCONFTABLE_LINEDATA(void)
	{ 
		Urno = 0;
	}
};



/////////////////////////////////////////////////////////////////////////////
// CicFConfTableViewer

/////////////////////////////////////////////////////////////////////////////
// Class declaration of CicFConfTableViewer

//@Man:
//@Memo: CicFConfTableViewer
//@See:  STDAFX, CCSCedaData, CedaDIAFLIGHTDATA, CCSPTRARRAY,  CCSTable
/*@Doc:
  No comment on this up to now.
*/
class CicFConfTableViewer : public CViewer
{
// Constructions
public:
    //@ManMemo: Default constructor
    CicFConfTableViewer();
    //@ManMemo: Default destructor
    ~CicFConfTableViewer();

	void UnRegister();

	void ClearAll();

    //@ManMemo: Attach
	void Attach(CCSTable *popAttachWnd);
    //void Attach(CTable *popAttachWnd);
    //@ManMemo: ChangeViewTo
    void ChangeViewTo(const CedaCflData *popCflData);
    //@ManMemo: omLines
    CCSPtrArray<CICFCONFTABLE_LINEDATA> omLines;
// Internal data processing routines
private:
//MWO

	//bool IsPassFilter(CCSPtrArray<CFLDATA> &opCcas);
	int CompareLines(const CICFCONFTABLE_LINEDATA *prpLine1, const CICFCONFTABLE_LINEDATA *prpLine2) const;
	
	bool DeleteLine(long lpUrno);
//MWO END


	void MakeLines(void);
	
	bool MakeLineData(CICFCONFTABLE_LINEDATA *prpLineData, const CFLDATA *prpFlightConflict) const;

	bool MakeLine(const CFLDATA *prpFlightConflict);
	//bool MakeLine(CCSPtrArray<CFLDATA> &opFlightConflicts);

	
	bool MakeColList(const CICFCONFTABLE_LINEDATA *prlLine, CCSPtrArray<TABLE_COLUMN> &olColList) const;
	
	int  CreateLine(const CICFCONFTABLE_LINEDATA &rpLine);

	bool FindLine(long lpUrno, int &rilLineno) const;

	void SelectLine(long ilCurrUrno);

	void InsertDisplayLine(int ipLineNo);



// Operations
public:
    //@ManMemo: DeleteAll
	void DeleteAll();
    //@ManMemo: DeleteLine
	void DeleteLine(int ipLineno);
 
// Window refreshing routines
public:
	void UpdateDisplay();

	void DrawHeader();

	long GetUrno(int ipLineNo, long &lpUrno) const;

	//void ProcessCflChange(long *plpCflUrno);
	//void ProcessKKeyChange(long *plpCcaUrno);


	

	bool bmInit;


private:
	CCSTable *pomTable;
	const CedaCflData *pomCflData;


// Methods which handle changes (from Data Distributor)
public:


};

#endif //__CicFConfTableViewer_H__
