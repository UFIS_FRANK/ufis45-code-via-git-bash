#if !defined(AFX_REPORTSEASONTABLEVIEWER_H__657FF802_4EDA_11D2_8369_0000C03B916B__INCLUDED_)
#define AFX_REPORTSEASONTABLEVIEWER_H__657FF802_4EDA_11D2_8369_0000C03B916B__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
 
//#pragma warning(disable: 4786)

#include <stdafx.h>
//#include "CCSTable.h"
#include <CViewer.h>
#include <CCSPrint.h>
#include <CcaCedaFlightData.H>
#include <CcaDiaViewer.H>

#define REPORTSEASONTABLE_COLCOUNT 10

struct REPORTSEASONTABLE_LINEDATA
{
	long	CcaUrno; 	// Eindeutige Datensatz-Nr.
	long	Flnu;		// Urno des flights
	bool	BlkCom;		// Blocked oder common?

	CString	Alc ;	   // Airline	
	CString	Flno;  // Flightnumber
	CString	Act ;      // Aircraft-type
	CString	ViaOut ;   // via
	CString	Des ;	   // Destination
	CString Freq;      // Days in season
	CTime	Stod ;      // Departuretime
	

	int CAnz ;    // Counter count
 	CString Cicf;		// from counter 
	CString Cict;		// to counter
	CTime	Ckbs;  // Counter opening time
	CTime	Ckes;  // Counter closing time
 
 	
	REPORTSEASONTABLE_LINEDATA()
	{ 
		CcaUrno = 0;
		Flnu = 0;
		BlkCom = false;
		Alc.Empty() ;		
		Flno.Empty() ;
		Act.Empty() ;
		Des.Empty() ;
		Freq.Empty();
		ViaOut.Empty() ;

		CAnz = 0;
 		Cicf.Empty();
		Cict.Empty();
	}
};

 



/////////////////////////////////////////////////////////////////////////////
// Befehlsziel ReportSeasonTableViewer 


class ReportSeasonTableViewer : public CViewer
{
// Constructions
public:
    ReportSeasonTableViewer();
    ~ReportSeasonTableViewer();

	// Clears the table of the viewer
	void ClearAll();

	// Connects the viewer with a table
    void Attach(CGXGridWnd *popAttachWnd);
	// Change the viewed database 
    void ChangeViewTo(CcaDiagramViewer &ropViewer);

  	void DeleteLine(int ipLineno);
 
	// UpdateDisplay: Load data to the display by using "omTable"
 	void UpdateDisplay();
	// Print table to paper
	void PrintTable(void);
	// Print table in file
	bool PrintExcelFile(const char *popFilePath) const;
	
 	
private:

	// columns width of the table in char
	static int imTableColCharWidths[REPORTSEASONTABLE_COLCOUNT];
	CString omTableHeadlines[REPORTSEASONTABLE_COLCOUNT];
	int imTableColWidths[REPORTSEASONTABLE_COLCOUNT];
	int imPrintColWidths[REPORTSEASONTABLE_COLCOUNT];

	const int imOrientation;

	CCSPrint omPrint;

	// Table fonts and widths
	CFont &romTableHeaderFont; 
	CFont &romTableLinesFont;
	const float fmTableHeaderFontWidth; 
	const float fmTableLinesFontWidth;

	// Table fonts and widths for printing
	CFont &romPrintHeaderFont; 
	CFont &romPrintLinesFont;
	const float fmPrintHeaderFontWidth; 
	const float fmPrintLinesFontWidth;

 	int CompareLines(const REPORTSEASONTABLE_LINEDATA *prpLine1, const REPORTSEASONTABLE_LINEDATA *prpLine2) const;
	bool CompressCcaLines(void);


	void DrawHeader();
	// Transfer the data of the database to the intern data structures
	void MakeLines(CcaDiagramViewer &ropViewer);
	// Copy the data from the db-record to the table-record
	bool MakeLineData(REPORTSEASONTABLE_LINEDATA &rrpLineData, const CCSPtrArray<DIACCADATA> &ropCcas) const;
 	int  CreateLine(const REPORTSEASONTABLE_LINEDATA &rrpLine);
	// Fills one row of the table
 	bool InsertTextLine(int ipLineNo, const REPORTSEASONTABLE_LINEDATA &rrpLine) const;

	bool PrintTableHeader(CCSPrint &ropPrint);
 	bool PrintTableLine(CCSPrint &ropPrint, int ipLineNo);

 	// Extract the vias from the given flight
	bool GetVias(CString &ropDestStr, const CCAFLIGHTDATA &rrpFlight) const;
   	void DeleteAll();

	// Timespan
	CTime omMinDate;
	CTime omMaxDate;

 	// Grid
 	CGXGridWnd *pomGXGridWnd;

	// Table Data
	CCSPtrArray<REPORTSEASONTABLE_LINEDATA> omLines;
 
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_REPORTSEASONTABLEVIEWER_H__657FF802_4EDA_11D2_8369_0000C03B916B__INCLUDED_)
