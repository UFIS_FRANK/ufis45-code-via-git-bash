// ScrollToolBar.cpp: implementation of the CScrollToolBar class.
//
//////////////////////////////////////////////////////////////////////

#include <stdafx.h>
#include <resource.h>
#include <ScrollToolBar.h>
#include <CCSGlobl.h>

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

BEGIN_MESSAGE_MAP(CScrollToolBar, CToolBar)
	//{{AFX_MSG_MAP(CScrollToolBar)
	ON_COMMAND(LEFT, OnLeft)
	ON_COMMAND(RIGHT, OnRight)
	ON_WM_SIZE()
	ON_WM_WINDOWPOSCHANGING()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CScrollToolBar::CScrollToolBar()
{
	pomSizes = 0;	
	imButtonAnz = imFirstDisplayed = imLastDisplayed = 0;
	omTotalSize.cx = omTotalSize.cy = 0;
	pomParentRebar = 0;
	imID =0 ;
	bmPartiell=false;
	bmActOnWinPosChanging = true;
	imScrollBtnWidth=0;
	imLastCx = -1;
	bmArrows = false;
}


CScrollToolBar::~CScrollToolBar()
{
	TRACE("CScrollToolBar::~CScrollToolBar\n");
	if ( pomSizes )
		delete[] pomSizes;

}


BOOL CScrollToolBar::AddToRebar ( CScrollRebar *popRebar, UINT idToolBar)
{
	//unsigned long ulStyle1;
	unsigned long ulStyle2;
	
	if ( CreateEx(popRebar,TBSTYLE_FLAT|TBSTYLE_AUTOSIZE , WS_CHILD|WS_VISIBLE|CBRS_ALIGN_TOP,
					CRect(0, 0, 0, 0), idToolBar )
		 && LoadToolBar(idToolBar) )
	{

		//--- Activating buttontext
 		// Add the strings of the string-table with the button-ids to the buttons
		CToolBarCtrl&  bar = GetToolBarCtrl();
		
		bool blTextSet = false;
		int ilCmdID;
		for(int ilIndex = bar.GetButtonCount() - 1; ilIndex >= 0; ilIndex--)
		{
			//--- if separator
			if ((GetButtonStyle(ilIndex) & TBBS_SEPARATOR) == TBBS_SEPARATOR)   
				continue; 
			
			//--- if ID invalid
			if((ilCmdID=GetItemID(ilIndex)) == 0)
				continue;
	
			//--- create string
			CString  olCmdIDString;
			CString  olButtonText(_T(""));

			olCmdIDString = GetString(ilCmdID);
			
			if(!olCmdIDString.IsEmpty())
			{	
				olButtonText = olCmdIDString.Left(olCmdIDString.Find('\n'));
				if(!olButtonText.IsEmpty())
				{
					// set text	
					if (blTextSet == false)
					{
						bar.SetMaxTextRows(1);
						//bar.SetButtonWidth(75, 75);
						blTextSet = true;
					}
					bar.AddStrings(olButtonText);
					int blRet = SetButtonText(ilIndex , olButtonText);
					TRACE("New Button Text: %s %d  Ret: %d\n", olButtonText, olButtonText.GetLength(), blRet);
				
				}	
			}
		} // for(
 


		pomParentRebar = popRebar;
		imID = idToolBar;
		// Create left and right scroll arrow
		if ( omLeftBar.CreateEx(this,TBSTYLE_FLAT|TBSTYLE_TRANSPARENT,
								WS_CHILD|WS_VISIBLE,
								CRect(0, 0, 0, 0),(UINT)idToolBar+LEFT ) )
			omLeftBar.LoadToolBar(IDR_BLANK_BAR);
		if ( omRightBar.CreateEx(this,TBSTYLE_FLAT|TBSTYLE_TRANSPARENT,
								 WS_CHILD|WS_VISIBLE,
								 CRect(0, 0, 0, 0),(UINT)idToolBar+RIGHT ) )
			omRightBar.LoadToolBar(IDR_BLANK_BAR);
		//ulStyle1 = RBBS_FIXEDBMP|RBBS_GRIPPERALWAYS;
		ulStyle2 = RBBS_NOGRIPPER|RBBS_FIXEDBMP;
		UINT ilBands = pomParentRebar->GetReBarCtrl().GetBandCount( ) ;
		popRebar->AddBar ( &omLeftBar, (LPCTSTR)0, 0, ulStyle2 );
		popRebar->AddBar ( this, (LPCTSTR)0, 0, ulStyle2  );
		popRebar->AddBar ( &omRightBar, (LPCTSTR)0, 0, ulStyle2 );

		CalculateSizes ();
		imFirstDisplayed = 0;
		imLastDisplayed = imButtonAnz-1;

		REBARBANDINFO slInfo;
		slInfo.cbSize = sizeof ( REBARBANDINFO );
		slInfo.fMask = RBBIM_CHILDSIZE | RBBIM_STYLE | RBBIM_ID | RBBIM_IDEALSIZE | RBBIM_SIZE;
		slInfo.cx = omTotalSize.cx;
		slInfo.cxIdeal = omTotalSize.cx;
		slInfo.cxMinChild = 0;
		slInfo.cyMinChild = omTotalSize.cy;
		slInfo.wID = idToolBar;
		slInfo.fStyle = RBBS_NOGRIPPER;
		pomParentRebar->GetReBarCtrl().SetBandInfo ( ilBands+1, &slInfo );

		CRect olRect;
		omLeftBar.GetItemRect(0,&olRect);
		imScrollBtnWidth = olRect.Width();

		slInfo.fMask =  RBBIM_CHILDSIZE | RBBIM_IDEALSIZE | RBBIM_SIZE | RBBIM_ID | RBBIM_STYLE;
		pomParentRebar->GetReBarCtrl().GetBandInfo(ilBands,&slInfo);
		slInfo.cyMinChild = olRect.Height();
		slInfo.cxMinChild = slInfo.cx = slInfo.cxIdeal = olRect.Width();
		slInfo.fStyle = RBBS_NOGRIPPER; //|= RBBS_GRIPPERALWAYS;
		slInfo.wID = (UINT)idToolBar+LEFT;
		pomParentRebar->GetReBarCtrl().SetBandInfo(ilBands,&slInfo);
		
		//slInfo.fStyle = RBBS_NOGRIPPER; //| RBBS_FIXEDSIZE;
		slInfo.wID = (UINT)idToolBar+RIGHT;
		pomParentRebar->GetReBarCtrl().SetBandInfo(ilBands+2,&slInfo);

		//TRACE("imButtonAnz = %d\n", imButtonAnz);

		// Is scrolling necessary?
		if (imButtonAnz < 3)
		{
			// NO: Hide scroll arrows
			pomParentRebar->GetReBarCtrl().ShowBand(ilBands,FALSE);
			pomParentRebar->GetReBarCtrl().ShowBand(ilBands+2,FALSE);
			imScrollBtnWidth = 0;
			bmArrows = false;
		}
		else
		{
			bmArrows = true;
			// YES: Show scroll arrows
			pomParentRebar->GetReBarCtrl().ShowBand(ilBands,TRUE);
			pomParentRebar->GetReBarCtrl().ShowBand(ilBands+2,TRUE);

			omLeftBar.EnableWindow(FALSE);
			omRightBar.EnableWindow(FALSE);
		}

		int idx = pomParentRebar->GetReBarCtrl().IDToIndex( imID );
		pomParentRebar->GetReBarCtrl().GetRect(idx, &olRect );
	
		CalcShownButtons ( olRect.Width(), imFirstDisplayed, imLastDisplayed, bmPartiell );	
		ButtonText(false);
		Display();

	}
	return TRUE;
}


bool CScrollToolBar::DeleteButton(int nIndex) 
{

	BOOL blRet=GetToolBarCtrl().DeleteButton(nIndex);
	CalculateSizes();

	return blRet!=0;
}


// Calculate the size of each item in the toolbar and store them in 
// the 'pomSizes' array
void CScrollToolBar::CalculateSizes ()
{
	//TRACE("CScrollToolBar%d::CalculateSizes: x= ", imButtonAnz);
	imButtonAnz = GetToolBarCtrl().GetButtonCount( ) ;
	if ( pomSizes )
		delete[] pomSizes;
	pomSizes = new CSize[imButtonAnz];
	CRect	olRect;		
	for ( int i=0; i<imButtonAnz; i++ )
	{
		if (GetButtonStyle(i) == TBBS_SEPARATOR)
		{
			GetItemRect(i, &olRect );
			pomSizes[i] = olRect.Size();
		}
		else
		{
			pomSizes[i].cx = LOWORD(GetToolBarCtrl().GetButtonSize());
			pomSizes[i].cy = HIWORD(GetToolBarCtrl().GetButtonSize());
		}
		//TRACE("%d ", pomSizes[i].cx);
	}
	//TRACE("\n");

	// First display all Buttons
	for (i=0; i<imButtonAnz; i++ )
	{
		UINT ilID = GetItemID( i ) ;
		if ( ilID )
		{
			GetToolBarCtrl().HideButton( ilID, FALSE );
		}
	}

	// Then get the max. size of the toolbar
	GetToolBarCtrl().GetMaxSize( &omTotalSize ) ;
	omTotalSize.cx += 2*imScrollBtnWidth;

	if ( pomParentRebar )
		pomParentRebar->OnChildSizeChanged ( this, omTotalSize.cx, omTotalSize.cy );
}


void CScrollToolBar::OnSize(UINT nType, int cx, int cy)
{
	//TRACE("CScrollToolBar%d::OnSize cx=%d bmAct=%d\n", imButtonAnz, cx, bmActOnWinPosChanging);
	CToolBar::OnSize(nType, cx, cy);
	if ( bmActOnWinPosChanging )
		OnSizeChange (cx, cy);
}


void CScrollToolBar::OnLeft()
{
	imLastDisplayed = max ( 0, imLastDisplayed-1 );
	imFirstDisplayed = max ( 0, imFirstDisplayed-1 );
	
	// Is first button a separator?
	if (GetButtonStyle(imFirstDisplayed) & TBBS_SEPARATOR == TBBS_SEPARATOR)
	{
		if (imFirstDisplayed > 0) 
		{
			imFirstDisplayed--;
		}
	}
	// Is last button a separator?
	if (GetButtonStyle(imLastDisplayed) & TBBS_SEPARATOR == TBBS_SEPARATOR)
	{
		if (imLastDisplayed > imFirstDisplayed) 
		{
			imLastDisplayed--;
		}
	}
	
	//TRACE("OnLeft: imFirst= %d  imLast= %d\n", imFirstDisplayed, imLastDisplayed);
	Display();
}

void CScrollToolBar::OnRight()
{
	bmPartiell = bmPartiell && (imLastDisplayed<imButtonAnz-1);
	imLastDisplayed = min ( imButtonAnz-1,imLastDisplayed+1 );
	imFirstDisplayed = min ( imButtonAnz-1, imFirstDisplayed+1 );
	
	// Is last button a separator?
	if (GetButtonStyle(imLastDisplayed) & TBBS_SEPARATOR == TBBS_SEPARATOR)
	{
		if (imLastDisplayed < imButtonAnz-1) 
		{
			imLastDisplayed++;
		}
	}
	// Is first button a separator?
	if (GetButtonStyle(imFirstDisplayed) & TBBS_SEPARATOR == TBBS_SEPARATOR)
	{
		if (imFirstDisplayed < imLastDisplayed) 
		{
			imFirstDisplayed++;
		}
	}
	
	
	
	//TRACE("OnRight: imFirst= %d  imLast= %d\n", imFirstDisplayed, imLastDisplayed);

	Display();
	/*
	CRect olRect;
	int idx = pomParentRebar->GetReBarCtrl().IDToIndex( imID );
	pomParentRebar->GetReBarCtrl().GetRect(idx, &olRect );
	if ( CalcShownButtons ( olRect.Width(), imFirstDisplayed, imLastDisplayed,
							bmPartiell ) )
		Display();*/
}


void CScrollToolBar::OnWindowPosChanging( WINDOWPOS* lpwndpos )
{
	//TRACE("CScrollToolBar::OnWindowPosChanging\n");
	CToolBar::OnWindowPosChanging( lpwndpos );
	if ( ! (lpwndpos->flags & SWP_NOSIZE ) )
	{
		if ( bmActOnWinPosChanging )
		{
			OnSizeChange (lpwndpos->cx, lpwndpos->cy);
		}
	}

}

// Calculate the width of the shown buttons and separators
int CScrollToolBar::GetUsedWidth ( int ipStart, int ipLast ) 
{
	int ilRet=0;
	ipStart = max ( ipStart, 0 );
	ipLast = min ( ipLast, imButtonAnz-1 );

	/*
	if ( ipStart==0 && ipLast==imButtonAnz-1 )
	{
//		ilRet = omTotalSize.cx - 4;
		ilRet = omTotalSize.cx - pomSizes[0].cx/3-4;
		return ilRet;
	}
	*/
	for ( int i=ipStart; i <= ipLast; i++ )
		ilRet += pomSizes[i].cx;
	//if ( ilRet >0 )
	//	ilRet -= pomSizes[0].cx/3;
	//TRACE("CScrollToolBar::GetUsedWidth: %d\n", ilRet);
	return ilRet;
}


// Determine which buttons can be shown in the toolbar and stores it in 
// 'ipStart', 'ipEnd' and 'bpPartiell
bool CScrollToolBar::CalcShownButtons ( int ipWidth, int &ipStart, int &ipEnd, 
									    bool &bpPartiell )
{
	//TRACE("CScrollToolBar::CalcShownButtons: anzBut=%d first=%d last=%d\n", imButtonAnz, ipStart, ipEnd);
	int cx, cxNeu;
	int ilEndNeu;

	ipStart = max ( ipStart, 0 );
	ipEnd = min ( ipEnd, imButtonAnz-1 );

	bpPartiell = false;
	cx = GetUsedWidth ( ipStart, ipEnd );
	//TRACE("CScrollToolBar::CalcShownButtons: currWidth=%d\n", cx); 
	if ( cx == ipWidth )
		return true;	//  Buttons passen genau
	//  wir haben noch Platz �brig
	if ( cx < ipWidth )		
	{
		if ( ipEnd-ipStart>=imButtonAnz-1 )	//ben�tigte Gr��e kleiner vorhandener Platz,
			return true;					//aber alle Buttons bereits sichtbar
		if ( ipEnd<imButtonAnz-1 )
			ipEnd++;
		else
			ipStart--;
		cxNeu = GetUsedWidth ( ipStart, ipEnd );
		if ( cxNeu <= ipWidth )		//  ein Button mehr pa�t auch noch -> Rekursion
		{
			return CalcShownButtons ( ipWidth, ipStart, ipEnd, bpPartiell ); 
		}
		else
		{
			bpPartiell = ( ipWidth-cx < pomSizes[ipEnd].cx );
			return true;
		}
	}
	else
	{		//  ben�tigte Gr��e gr��er als vorhandener Platz
		if ( ipEnd>ipStart )	//  mehr als ein Button dargestellt
		{	
			ilEndNeu = ipEnd-1;
			cxNeu = GetUsedWidth ( ipStart, ilEndNeu );
			if ( cxNeu > ipWidth )		//  ein Button weiniger pa�t immer noch nicht -> Rekursion
				return CalcShownButtons ( ipWidth, ipStart, --ipEnd, bpPartiell );
			else //  ein Knopf weniger w�re zu klein, also letzten nur teilweise darstellen
			{
				//bpPartiell = ( cx-ipWidth < pomSizes[ipEnd].cx*2/3 );	//  rechter Button nicht zu 2/3 sichtbar
				bpPartiell = ( ipWidth-cxNeu < pomSizes[ipEnd].cx );
				return true;
			}
		}
		else
		{
			bpPartiell=true;
			return false;
		}
	}
	
}


// Display the toolbar: Hide or show the buttons, separator and arrow buttons
void CScrollToolBar::Display()
{
	//TRACE("CScrollToolBar::Display\n");
	bmActOnWinPosChanging = false;
	int ilFirst= imFirstDisplayed;
	int ilLast = imLastDisplayed;
	bool blLastPartiell = bmPartiell;
	for ( int i=0; i<imButtonAnz; i++ )
	{
		UINT ilID = GetItemID( i ) ;
		if ( ilID )
		{
			GetToolBarCtrl().HideButton( ilID, (i<ilFirst)||(i>ilLast) );
		}
	}
	
	// Is scrolling necessary?
	if (imButtonAnz >= 3)
	{
		if (ilFirst > 0)
		{
			// Show left button
			omLeftBar.EnableWindow(TRUE);
			omLeftBar.LoadToolBar(IDR_LEFT_BAR);
		}
		else
		{
			// Hide left button but show the blank button
			omLeftBar.EnableWindow(FALSE);
			omLeftBar.LoadToolBar(IDR_BLANK_BAR);		
		}

		if ((ilLast < imButtonAnz-1) || blLastPartiell)
		{
			// Show right button
			omRightBar.EnableWindow(TRUE);
			omRightBar.LoadToolBar(IDR_RIGHT_BAR);
		}	 
		else
		{
			// Hide right button but show the blank button
			omRightBar.EnableWindow(FALSE);
			omRightBar.LoadToolBar(IDR_BLANK_BAR);
		}
	}
	bmActOnWinPosChanging =true;
}

void CScrollToolBar::OnSizeChange (int cx, int cy)
{
	bool blOk;
	//static int ilLastCx = -1;

//	int idx = pomParentRebar->GetReBarCtrl().IDToIndex( imID );
//	if ( idx >= 0 )
//	{
//		CRect olRect;
//		pomParentRebar->GetReBarCtrl().GetRect(idx, &olRect );
//		cx = olRect.Width();
//	}

	cx -= imScrollBtnWidth;
	if ( ( cx <= 0) || !pomSizes )//|| (cx==imLastCx) ) 
		return;
	imLastCx = cx;
 
	blOk = CalcShownButtons ( cx, imFirstDisplayed, imLastDisplayed, bmPartiell ) ;
	if ( blOk )
		Display();
}

void CScrollToolBar::DeleteStringMap ()
{
	if(m_pStringMap)
	{
		delete m_pStringMap;
		m_pStringMap = NULL;
	}
}


CSize CScrollToolBar::CalcDynamicLayout( int nLength, DWORD dwMode )
{
	CSize olSize = CToolBar::CalcDynamicLayout( nLength, dwMode );
	CalculateSizes ();
	return olSize ;
}

CSize CScrollToolBar::CalcFixedLayout(BOOL bStretch, BOOL bHorz)
{
	CSize olSize = CToolBar::CalcFixedLayout( bStretch, bHorz );
	CalculateSizes ();
	return olSize ;
}


void CScrollToolBar::SetSizes(SIZE opButtonSize, SIZE opImageSize)
{
	CToolBar::SetSizes(opButtonSize, opImageSize);
	//CalcDynamicLayout(0, LM_HORZ | LM_HORZDOCK | LM_COMMIT);
	CalculateSizes ();
	pomParentRebar->UpdateWindow();
}


CSize CScrollToolBar::GetImageSize(void) const
{
	if (imButtonAnz < 1) return CSize(0,0);

	IMAGEINFO olImageInfo;
	int ilImageNo;
	UINT ilCmdId, ilStyle;
	CSize olImageSize;

	GetButtonInfo(0, ilCmdId, ilStyle, ilImageNo);
	GetToolBarCtrl().GetImageList()->GetImageInfo(ilImageNo ,&olImageInfo);

	olImageSize.cx = olImageInfo.rcImage.right - olImageInfo.rcImage.left;
	olImageSize.cy = olImageInfo.rcImage.bottom - olImageInfo.rcImage.top;

	return olImageSize;
}


bool CScrollToolBar::ButtonText(bool bpButtonText, int ipButtonWidth, int ipButtonHeight)
{

	if (imButtonAnz < 1) return true;

	//--- Resize  Buttons
	CSize olImageSize = GetImageSize();
	CSize olButtonSize;
	if (!bpButtonText)
	{
		olButtonSize.cx = olImageSize.cx + 7;
		olButtonSize.cy = olImageSize.cy + 6;
	}
	else
	{
		olButtonSize.cx = max(ipButtonWidth, olImageSize.cx + 7);
		olButtonSize.cy = max(ipButtonHeight, olImageSize.cy + 6);
	}	
	SetSizes(olButtonSize , olImageSize);
	
	//--- Tooltips needed?
	if (!bpButtonText)
	{
		// YES
		EnableToolTips(TRUE);
	}
	else
	{
		// NO
		EnableToolTips(FALSE);
	}

	
	return true;
}





CLRToolBar::CLRToolBar()
{
}

BEGIN_MESSAGE_MAP(CLRToolBar, CToolBar)
	ON_WM_LBUTTONDOWN()
	ON_WM_GETMINMAXINFO()
	ON_WM_WINDOWPOSCHANGING()
END_MESSAGE_MAP()

void CLRToolBar::OnLButtonDown( UINT nFlags, CPoint point )
{
	if (m_pDockBar != NULL && OnToolHitTest(point, NULL) == -1)
	{
		CToolBar::OnLButtonDown( nFlags, point );
	}
	else
	{
		CWnd::OnLButtonDown(nFlags, point);
	}
	
}


void CLRToolBar::OnUpdateCmdUI(CFrameWnd* pTarget, BOOL bDisableIfNoHndler)
{
	BOOL		blEnable;
	TBBUTTON	olButton;
	blEnable = m_hWnd && IsWindowVisible() ;
	if ( GetToolBarCtrl( ).GetButton ( 0, &olButton ) )
	{
		GetToolBarCtrl().EnableButton(olButton.idCommand);
	}
	
}

void CLRToolBar::OnWindowPosChanging( WINDOWPOS* lpwndpos )
{
	//TRACE("CLRToolBar::OnWindowPosChanging\n");
	RECT slRect;
	GetItemRect( 0, &slRect );
	if ( lpwndpos->cx > slRect.right - slRect.left )
		lpwndpos->flags|=SWP_NOSIZE;
	CToolBar::OnWindowPosChanging ( lpwndpos );
}

void CLRToolBar::OnGetMinMaxInfo(MINMAXINFO FAR* lpMMI)
{
	RECT slRect;
	CToolBar::OnGetMinMaxInfo(lpMMI);
	GetItemRect( 0, &slRect );
    lpMMI->ptMaxSize.x = 
    lpMMI->ptMinTrackSize.x =
    lpMMI->ptMaxTrackSize.x = slRect.right - slRect.left;
}
	

int CLRToolBar::GetWidth()
{
	CRect olRect;
	GetItemRect( 0, &olRect );
	return olRect.Width();
}



BEGIN_MESSAGE_MAP(CScrollRebar, CReBar)
	//{{AFX_MSG_MAP(CScrollToolBar)
	ON_WM_GETMINMAXINFO()
	ON_WM_SIZE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


CScrollRebar::CScrollRebar()
{

}

CScrollRebar::~CScrollRebar()
{
	TRACE("CScrollRebar::~CScrollRebar\n");
	CObject	*polObj;
	int i;
	for ( i=0; i<omItems.GetSize(); i++ )
		if ( polObj = omItems[i] )
			delete polObj;
	omItems.RemoveAll();

}


BOOL CScrollRebar::DestroyWindow(void)
{

	TRACE("CScrollRebar::DestroyWindow\n");
	CObject	*polObj;
	int i;
	for ( i=0; i<omItems.GetSize(); i++ )
		if ( polObj = omItems[i] )
			delete polObj;	
	omItems.RemoveAll();

	return CReBar::DestroyWindow();
}


void CScrollRebar::OnGetMinMaxInfo(MINMAXINFO FAR* lpMMI)
{
	CReBar::OnGetMinMaxInfo(lpMMI);

    lpMMI->ptMaxSize.y = 
    lpMMI->ptMaxTrackSize.y = GetReBarCtrl().GetRowHeight(0);
}

CScrollToolBar* CScrollRebar::AddScrollToolBar ( UINT idToolBar )
{
	if ( idToolBar >= LEFT )
	{
		TRACE ( "ID eines Scrollbaren Toolbars mu� < 16384 sein.\n" );
		return 0;
	}
	CScrollToolBar *polBar;
	polBar = new CScrollToolBar;

	if ( !polBar )
		return 0;

 	int ilOk = polBar->AddToRebar ( this, idToolBar);
	if (!ilOk) {
		delete polBar;
		return 0;
	}

	omItems.Add ( polBar );

	return polBar;
}
	

BOOL CScrollRebar::Create(CWnd* pParentWnd, DWORD dwCtrlStyle/* = RBS_FIXEDORDER*/,
						  DWORD dwStyle /*= WS_CHILD | WS_VISIBLE | WS_CLIPSIBLINGS | WS_CLIPCHILDREN | CBRS_TOP*/,
						  UINT nID /*= AFX_IDW_REBAR*/ )
{
	return CReBar::Create(pParentWnd, dwCtrlStyle, dwStyle, nID );
}
		
CButton* CScrollRebar::AddButton ( LPCTSTR lpszCaption, DWORD dwStyle, const RECT& rect, 
								   CWnd* pParentWnd, UINT nID  )
{
	/*
	if ( nID >= LEFT )
	{
		TRACE ( "ID eines Scrollbaren Toolbars mu� < 16384 sein.\n" );
		return 0;
	}*/
	CButton *polButton;
	polButton = new CButton;
	if ( polButton && polButton->Create ( lpszCaption, dwStyle, rect, pParentWnd, nID ))
	{
		if (AddBar ( polButton, 0, 0, RBBS_NOGRIPPER|RBBS_FIXEDBMP ) )
		{

			omItems.Add ( polButton );
			return polButton;
		}
	}
	if ( polButton )
		delete polButton;
	return 0;
}
	
CComboBox* CScrollRebar::AddComboBox ( DWORD dwStyle, const RECT& rect, 
									   CWnd* pParentWnd, UINT nID  )
{
	/*
	if ( nID >= LEFT )
	{
		TRACE ( "ID eines Scrollbaren Toolbars mu� < 16384 sein.\n" );
		return 0;
	}*/
	CComboBox *polCb;
	polCb = new CComboBox;
	if ( polCb && polCb->Create ( dwStyle, rect, pParentWnd, nID ) && 
		 AddBar ( polCb, 0, 0, RBBS_NOGRIPPER|RBBS_FIXEDBMP ) )
	{
		omItems.Add ( polCb );
		return polCb;
	}
	if ( polCb )
		delete polCb;
	return 0;
}
	
bool CScrollRebar::OnChildSizeChanged ( CWnd * pWnd, int cx, int cy )
{
	if ( !pWnd || !pWnd->m_hWnd )
		return false;
	bool blRet=false;
	REBARBANDINFO slInfo;

	memset ( &slInfo, 0, sizeof(slInfo) ) ;
	slInfo.cbSize = sizeof(slInfo);
	
	CReBarCtrl &rolCtrl = GetReBarCtrl();
	UINT ilBands = rolCtrl.GetBandCount();

	slInfo.fMask =  RBBIM_CHILDSIZE | RBBIM_CHILD | RBBIM_IDEALSIZE | RBBIM_SIZE;
	for ( UINT i=0; !blRet&&(i<ilBands); i++ )
	{
		if ( rolCtrl.GetBandInfo(i, &slInfo) && 
			(slInfo.hwndChild==pWnd->m_hWnd ) )
		{
			slInfo.cyMinChild = cy;
			slInfo.cx = cx;
			slInfo.cxIdeal = cx;
			if ( rolCtrl.SetBandInfo(i,&slInfo) )
				blRet = true;
			else
				break;
		}
	}
	CRect olRect;
	GetClientRect ( &olRect );
	rolCtrl.SizeToRect ( olRect );
	return blRet;
}


