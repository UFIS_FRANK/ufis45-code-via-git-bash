// MainFrm.cpp : implementation of the CMainFrame class
//

#include <stdafx.h>
#include <OPSSCM.h>
#include <process.h>

#include <MainFrm.h>

#include <CCSEdit.h>
#include <InitialLoadDlg.h>
#include <CcaDiaPropertySheet.h>
#include <CcaCedaFlightData.h>
#include <CViewer.h>
#include <CcaChart.h>
#include <CcaGantt.h>
#include <TimePacket.h>
#include <CicDemandTableDlg.h>
#include <CicNoDemandTableDlg.h>
#include <CicConfTableDlg.h>
#include <CicFConfTableDlg.h>
#include <CCAExpandDlg.h>
#include <DataSet.h>
#include <DelelteCounterDlg.h>
#include <AllocateCca.h>
#include <Allocate.h>
#include <AllocateCcaParameter.h>
#include <Splash.h>
#include <PrivList.h>
#include <CicCreateDemandsDlg.h>
#include <DailyCcaPrintDlg.h>
#include <ReportTableDlg.h>
#include <ReportSeasonTableDlg.h>
#include <CcaCommonDlg.h>
#include <SeasonDlg.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMainFrame

IMPLEMENT_DYNAMIC(CMainFrame, CFrameWnd)

BEGIN_MESSAGE_MAP(CMainFrame, CFrameWnd)
	//{{AFX_MSG_MAP(CMainFrame)
	ON_MESSAGE(WM_BCADD,OnBcAdd)
	ON_WM_CREATE()
	ON_WM_SETFOCUS()
	ON_WM_SIZE()
	ON_COMMAND(ID_VIEW, OnView)
	ON_COMMAND(ID_DELETE, OnDelete)
	ON_CBN_SELENDOK(IDC_VIEW_COMBO, OnViewSelChange)
	ON_COMMAND(ID_ALLOCATE, OnAllocate)
	ON_COMMAND(ID_CREATE_DEMANDS, OnCreateDemands)
	ON_UPDATE_COMMAND_UI(ID_CREATE_DEMANDS, OnUpdateCreateDemands)
	ON_MESSAGE(WM_REPAINT_ALL, RepaintAll)
	ON_COMMAND(ID_WO_DEMAND, OnWoDemand)
	ON_COMMAND(ID_DEMAND, OnDemand)
	ON_COMMAND(ID_CONFLICTS, OnConflicts)
	ON_COMMAND(ID_PRINT, OnPrint)
    ON_MESSAGE(WM_POSITIONCHILD, OnPositionChild)
    ON_WM_HSCROLL()
	ON_COMMAND(ID_NOW, OnNow)
	ON_COMMAND(ID_RULES, OnRules)
	ON_COMMAND(ID_BDPSUIF, OnBdpsuif)
	ON_WM_KEYDOWN()
	ON_WM_KEYUP()
	ON_WM_ERASEBKGND()
	ON_WM_PAINT()
    ON_MESSAGE(WM_UPDATEDIAGRAM, OnUpdateDiagram)
	ON_COMMAND(ID_APP_EXIT, OnAppExit)
	ON_CBN_SELCHANGE(IDC_SEASON_SEL, OnSeasonViewSelChange)
	ON_COMMAND(ID_FLIGHT_CONFLICTS, OnFlightConflicts)
	ON_COMMAND(ID_SYMBOLTEXT, OnSymbolText)
	ON_COMMAND(ID_REPORT, OnReport)
	ON_COMMAND(ID_REPORT_SEASON, OnReportSeason)
	ON_UPDATE_COMMAND_UI(ID_REPORT_SEASON, OnUpdateReportSeason)
	ON_UPDATE_COMMAND_UI(ID_SYMBOLTEXT, OnUpdateSymbolText)
	ON_UPDATE_COMMAND_UI(ID_FLIGHT_CONFLICTS, OnUpdateToolBarButtons)
	ON_UPDATE_COMMAND_UI(ID_VIEW, OnUpdateToolBarButtons)
	ON_WM_CLOSE()
	ON_UPDATE_COMMAND_UI(ID_APP_ABOUT, OnUpdateToolBarButtons)
	ON_UPDATE_COMMAND_UI(ID_BDPSUIF, OnUpdateToolBarButtons)
	ON_UPDATE_COMMAND_UI(ID_CONFLICTS, OnUpdateToolBarButtons)
	ON_UPDATE_COMMAND_UI(ID_DELETE, OnUpdateToolBarButtons)
	ON_UPDATE_COMMAND_UI(ID_DEMAND, OnUpdateToolBarButtons)
	ON_UPDATE_COMMAND_UI(ID_ALLOCATE, OnUpdateToolBarButtons)
	ON_UPDATE_COMMAND_UI(ID_NOW, OnUpdateToolBarButtons)
	ON_UPDATE_COMMAND_UI(ID_PRINT, OnUpdateToolBarButtons)
	ON_UPDATE_COMMAND_UI(ID_RULES, OnUpdateToolBarButtons)
	ON_UPDATE_COMMAND_UI(ID_WO_DEMAND, OnUpdateToolBarButtons)
	ON_WM_TIMER()
	ON_COMMAND(ID_CREATE_COMMON, OnCreateCommon)
	ON_UPDATE_COMMAND_UI(ID_CREATE_COMMON, OnUpdateCreateCommon)
	//}}AFX_MSG_MAP
	// Global help commands
	ON_COMMAND(ID_HELP_FINDER, CFrameWnd::OnHelpFinder)
	ON_COMMAND(ID_HELP, CFrameWnd::OnHelp)
	ON_COMMAND(ID_CONTEXT_HELP, CFrameWnd::OnContextHelp)
	ON_COMMAND(ID_DEFAULT_HELP, CFrameWnd::OnHelpFinder)
END_MESSAGE_MAP()

static UINT indicators[] =
{
	ID_SEPARATOR,           // status line indicator
	ID_INDICATOR_CAPS,
	ID_INDICATOR_NUM,
	ID_INDICATOR_SCRL,
};

static void CcaDiagramCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName);

extern int igAnsichtPageIndex;



/////////////////////////////////////////////////////////////////////////////
// CMainFrame construction/destruction

CMainFrame::CMainFrame()
{
	igAnsichtPageIndex = -1;
	omMaxTrackSize = CPoint(4000/*1024*/, 1500);
	omMinTrackSize = CPoint(1024 / 4, 768 / 4);
	omViewer.SetViewerKey("CCADIA");
	omViewer.SelectView("<Default>");
    imStartTimeScalePos = 104;
    imStartTimeScalePos++;              // plus one for left border of chart
    imFirstVisibleChart = 0;
	lmBkColor = lgBkColor;
	pogCcaDiagram = this;
	bmIsViewOpen = false;
	bgKlebefunktion = false;
	pogCicDemandTableDlg = NULL;//new CicDemandTableDlg(this);
	pogCommonCcaDlg = NULL;
	bmSymbolText = false;	
	pogSeasonDlg = NULL;
}

CMainFrame::~CMainFrame()
{
	TRACE("CMainFrame::~CMainFrame\n");
	if(pogCicDemandTableDlg != NULL)
	{
		delete pogCicDemandTableDlg;
	}
	if(pogCicNoDemandTableDlg != NULL)
	{
		delete pogCicNoDemandTableDlg;
	}
	if(pogCicConfTableDlg != NULL)
	{
		delete pogCicConfTableDlg;
	}
	if(pogCicFConfTableDlg != NULL)
	{
		delete pogCicFConfTableDlg;
	}
	if(pogCommonCcaDlg != NULL)
	{
		delete pogCommonCcaDlg;
		pogCommonCcaDlg = NULL;
	}
	if (pogSeasonDlg != NULL)
	{
		pogSeasonDlg->DestroyWindow();
		delete pogSeasonDlg;
		pogSeasonDlg = NULL;
	}
/*
	CcaChart *polChart;
	for (int i = 0; i < omPtrArray.GetSize(); i++)
	{
		if ((polChart = (CcaChart *) omPtrArray.GetAt(i)) != NULL)
			delete polChart; 
	}
 */
    omPtrArray.RemoveAll();
	pogCcaDiagram = NULL;

}

int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CFrameWnd::OnCreate(lpCreateStruct) == -1)
	{
		return -1;
	}
	// create a view to occupy the client area of the frame
	/*if (!m_wndView.Create(NULL, NULL, AFX_WS_DEFAULT_VIEW,
		CRect(0, 0, 0, 0), this, AFX_IDW_PANE_FIRST, NULL))
	{
		TRACE0("Failed to create view window\n");
		return -1;
	}
	*/	
	


	CreateToolBar();


	/*

	if (!m_wndStatusBar.Create(this) ||
		!m_wndStatusBar.SetIndicators(indicators,
		  sizeof(indicators)/sizeof(UINT)))
	{
		TRACE0("Failed to create status bar\n");
		return -1;      // fail to create
	}
	*/

	// TODO: Delete these three lines if you don't want the toolbar to
	//  be dockable
	//m_wndToolBar.EnableDocking(CBRS_ALIGN_ANY);
	//EnableDocking(CBRS_ALIGN_ANY);
	//DockControlBar(&m_wndToolBar);

	/////////////////////////////////////////////////////////////////////////////////////////
	char pclConfigPath[142];
    if (getenv("CEDA") == NULL)
        strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
    else
        strcpy(pclConfigPath, getenv("CEDA"));


	char pclUseBcProxy[256];
	GetPrivateProfileString("GLOBAL", "BCEVENTTYPE", "BCSERV",pclUseBcProxy, sizeof pclUseBcProxy, pclConfigPath);

	//MWO/RRO 25.03.03
	if (stricmp(pclUseBcProxy,"BCPROXY") == 0 || stricmp(pclUseBcProxy,"BCCOM") == 0)
	{
		//MWO/RRO
		//ogCommHandler.RegisterBcWindow(this);
		ogBcHandle.StartBc(); 
		//END MWO/RRO
	}
	else
	{
		ogCommHandler.RegisterBcWindow(this);
	}

	ogBcHandle.GetBc();


	pogCicDemandTableDlg = new CicDemandTableDlg(this);
	pogCicNoDemandTableDlg = new CicNoDemandTableDlg(this);
	pogCicConfTableDlg = new CicConfTableDlg(this);
	pogCicFConfTableDlg = new CicFConfTableDlg(this);
	pogCommonCcaDlg = new CCcaCommonDlg(this);
	pogSeasonDlg = new CSeasonDlg(this);


	CRect olRect;
	GetClientRect(olRect);

	/*
	pogCicNoDemandTableDlg->MoveWindow(CRect(olRect.right - 500, olRect.bottom - 300, olRect.right - 25, olRect.bottom - 25));

	pogCicDemandTableDlg->MoveWindow(CRect(olRect.right - 1024, olRect.top + pogButtonList->m_nDialogBarHeight + 25, olRect.right -25, olRect.top + 550));

	pogCicConfTableDlg->MoveWindow(CRect(olRect.right - 1024, olRect.bottom - 300, olRect.right - 25, olRect.bottom - 25));

	*/
	pogCicNoDemandTableDlg->ShowWindow(SW_HIDE);
	pogCicDemandTableDlg->ShowWindow(SW_HIDE);
	pogCicConfTableDlg->ShowWindow(SW_HIDE);
	pogCicFConfTableDlg->ShowWindow(SW_HIDE);
	pogCommonCcaDlg->ShowWindow(SW_HIDE);

    SetTimer(0, (UINT) 60 * 1000, NULL);
    SetTimer(1, (UINT) 60 * 1000 * 2, NULL);

	SetWindowPos(&wndTop, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE);


    omViewer.Attach(this);
 
	GetClientRect(&olRect);

	olRect.top += 20;

	CTime olCurrentTime;
	olCurrentTime = CTime::GetCurrentTime();

	if(!bgGatPosLocal)
	{
		ogBasicData.LocalToUtc(olCurrentTime);
	}

    omTSStartTime = olCurrentTime;// - CTimeSpan(0, 1, 0, 0);
    omStartTime = CTime(omTSStartTime.GetYear(), omTSStartTime.GetMonth(), omTSStartTime.GetDay(),
        0, 0, 0);// - CTimeSpan(1, 0, 0, 0);

    omDuration = CTimeSpan(3, 0, 0, 0);
    omTSDuration = CTimeSpan(0, 6, 0, 0);
    omTSInterval = CTimeSpan(0, 0, 10, 0);

	//Die wollen local
    CTime olUCT = CTime::GetCurrentTime();//olCurrentTime;
    CTime olRealUTC = olCurrentTime;
    char olBuf[64];
    sprintf(olBuf, "%02d%02d/%02d%02dz",
        olUCT.GetHour(), olUCT.GetMinute(),        
        olRealUTC.GetHour(), olRealUTC.GetMinute()
    );

    
	
	CTime olLocalTime = CTime::GetCurrentTime();
	CTime olUtcTime = CTime::GetCurrentTime();
	ogBasicData.LocalToUtc(olUtcTime);

	
	
	

    sprintf(olBuf, "%02d%02d%02d  %d",  omTSStartTime.GetDay(), omTSStartTime.GetMonth(), omTSStartTime.GetYear() % 100, GetDayOfWeek(omTSStartTime));
    int ilPos = (imStartTimeScalePos - olRect.left - 80) / 2;
    omTSDate.Create(olBuf, SS_CENTER | WS_CHILD | WS_VISIBLE,
        CRect(ilPos, 35, ilPos + 80, 52), this);

    // CBitmapButton
    omBB1.Create("PREV", BS_OWNERDRAW | WS_CHILD | WS_VISIBLE,
        CRect(olRect.right - 36, 21 + 5, olRect.right - 19, (21 + 5) + 25), this, IDC_PREV);
    omBB1.LoadBitmaps("PREVU", "PREVD", NULL, "NEXTX");

    omBB2.Create("NEXT", BS_OWNERDRAW | WS_CHILD | WS_VISIBLE,
        CRect(olRect.right - 19, 21 + 5, olRect.right - 2, (21 + 5) + 25), this, IDC_NEXT);
    omBB2.LoadBitmaps("NEXTU", "NEXTD", NULL, "NEXTX");

	omBB1.ShowWindow(SW_HIDE);
	omBB2.ShowWindow(SW_HIDE);

	omTimeScale.lmBkColor = lgBkColor;
    omTimeScale.Create(NULL, "TimeScale", WS_CHILD | WS_VISIBLE,
        CRect(imStartTimeScalePos, 28, olRect.right - (36 + 2), 62), this, 0, NULL);
    omTimeScale.SetDisplayTimeFrame(omTSStartTime, omTSDuration, omTSInterval);

	if(bgGatPosLocal)
	{
		omTimeScale.UpdateCurrentTimeLine(olLocalTime);
	}
	else
	{
		omTimeScale.UpdateCurrentTimeLine(olRealUTC);
    }

    // must be static
    static UINT ilIndicators[] = { ID_SEPARATOR };
    omStatusBar.Create(this, CBRS_BOTTOM | WS_CHILD | WS_VISIBLE,0);
    omStatusBar.SetIndicators(ilIndicators, sizeof(ilIndicators) / sizeof(UINT));

    UINT nID, nStyle; int cxWidth;
    omStatusBar.GetPaneInfo(0, nID, nStyle, cxWidth);
    omStatusBar.SetPaneInfo(0, nID, SBPS_STRETCH | SBPS_NORMAL, cxWidth);

    olRect.top += 64;                   // for Dialog Bar & TimeScale
    olRect.bottom -= 20;                // for Horizontal Scroll Bar
    omClientWnd.Create(NULL, "ClientWnd", WS_CHILD /*| WS_HSCROLL */ | WS_VISIBLE, olRect, this,
        0 /* IDD_CLIENTWND */, NULL);
        
	// This will fix the bug for the horizontal scroll bar in the preplan mode
    long llTSMin = (omTSStartTime - omStartTime).GetTotalMinutes();
    long llTotalMin = omDuration.GetTotalMinutes() - omTSDuration.GetTotalMinutes();
    SetScrollRange(SB_HORZ, 0, 1000, FALSE);
    SetScrollPos(SB_HORZ, (int) (1000 * llTSMin / llTotalMin), FALSE);

    CcaChart *polChart;
    CRect olPrevRect;
    omClientWnd.GetClientRect(&olRect);
    
    imFirstVisibleChart = 0;
	OnUpdatePrevNext();
    int ilLastY = 0;
    for (int ilI = 0; ilI < omViewer.GetGroupCount(); ilI++)
    {
        olRect.SetRect(olRect.left, ilLastY, olRect.right, ilLastY);
        
        polChart = new CcaChart;
        polChart->SetTimeScale(&omTimeScale);
        polChart->SetViewer(&omViewer, ilI);
        polChart->SetStatusBar(&omStatusBar);
        polChart->SetStartTime(omTSStartTime);
        polChart->SetInterval(omTimeScale.GetDisplayDuration());
        polChart->Create(NULL, "CcaChart", WS_OVERLAPPED | WS_BORDER | WS_CHILD | WS_VISIBLE,
            olRect, &omClientWnd,
            0 /* IDD_CHART */, NULL);
        
        omPtrArray.Add(polChart);

        ilLastY += polChart->GetHeight();
    }

    OnTimer(0);

	ogDdx.Register(this, REDISPLAY_ALL, CString("STAFFDIAGRAM"),
		CString("Redisplay all from What-If"), CcaDiagramCf);	// for what-if changes
	ogDdx.Register(this, STAFFDIAGRAM_UPDATETIMEBAND, CString("STAFFDIAGRAM"),
		CString("Update Time Band"), CcaDiagramCf);	// for updating the yellow lines

	 
	ogCcaDiaFlightData.Register();	 

	//SetWndPos();	

	// CG: The following line was added by the Splash Screen component.
	CSplashWnd::ShowSplashScreen(this);



	// Set the privileges
	SetPrivileges();

	// Set main-window-caption
	if (ogCcaDiaFlightData.omData.GetSize() > 0)
	{
		CString olFlightNumStr;
		olFlightNumStr.Format("%ld", ogCcaDiaFlightData.omData.GetSize());
		SetWindowText(ogAppName + CString(": ") + olFlightNumStr + CString(" ") + GetString(IDS_CAPTION));
	}
	else 
	{
		SetWindowText(ogAppName);
	}
	return 0;
}


 
bool CMainFrame::SetPrivileges(void) 
{

	//// Save privileges in local Map
	omPrivMap.SetAt((void *)ID_VIEW, ogPrivList.GetStat("DESKTOP_CB_Daten_Laden"));
	omPrivMap.SetAt((void *)ID_NOW, ogPrivList.GetStat("DESKTOP_CB_Jetzt"));
	omPrivMap.SetAt((void *)ID_DELETE, ogPrivList.GetStat("DESKTOP_CB_L�schen"));
	omPrivMap.SetAt((void *)ID_ALLOCATE, ogPrivList.GetStat("DESKTOP_CB_Einteilen"));
	omPrivMap.SetAt((void *)ID_WO_DEMAND, ogPrivList.GetStat("DESKTOP_CB_Fl�ge_ohne_Bedarfe"));
	omPrivMap.SetAt((void *)ID_DEMAND, ogPrivList.GetStat("DESKTOP_CB_Bedarfe"));
	omPrivMap.SetAt((void *)ID_FLIGHT_CONFLICTS, ogPrivList.GetStat("DESKTOP_CB_Flug_Konflikte"));
	omPrivMap.SetAt((void *)ID_CONFLICTS, ogPrivList.GetStat("DESKTOP_CB_Konflikte"));
	omPrivMap.SetAt((void *)ID_BDPSUIF, ogPrivList.GetStat("DESKTOP_CB_Stammdaten"));
	omPrivMap.SetAt((void *)ID_RULES, ogPrivList.GetStat("DESKTOP_CB_Regelwerk"));
	omPrivMap.SetAt((void *)ID_PRINT, ogPrivList.GetStat("DESKTOP_CB_Drucken"));
	omPrivMap.SetAt((void *)ID_APP_ABOUT, ogPrivList.GetStat("DESKTOP_CB_Info"));

	//// Hide buttons if necessary
	if (!pomToolBar1 || !pomToolBar2) return false;

	int ilButtonIndex;
	int ilCmdId;
	char clPriv; 
	POSITION pos;
	for( pos = omPrivMap.GetStartPosition(); pos != NULL; )
	{
		omPrivMap.GetNextAssoc( pos, (void *&)ilCmdId , (unsigned short &)clPriv );

		if (clPriv == '-')
		{	
			// Determine the toolbar of the button if one is present
			if ((ilButtonIndex = pomToolBar1->CommandToIndex(ilCmdId)) != -1)
			{
				pomToolBar1->DeleteButton(ilButtonIndex);
			}
			else if ((ilButtonIndex = pomToolBar2->CommandToIndex(ilCmdId)) != -1)
			{
				pomToolBar2->DeleteButton(ilButtonIndex);
			}
		}
	}


	/*
	SetPrivilege(ID_VIEW, "DESKTOP_CB_Daten_Laden");
	SetPrivilege(ID_NOW, "DESKTOP_CB_Jetzt");
	SetPrivilege(ID_DELETE, "DESKTOP_CB_L�schen");
	SetPrivilege(ID_ALLOCATE, "DESKTOP_CB_Einteilen");
	SetPrivilege(ID_WO_DEMAND, "DESKTOP_CB_Fl�ge_ohne_Bedarfe");
	SetPrivilege(ID_DEMAND, "DESKTOP_CB_Bedarfe");
	SetPrivilege(ID_FLIGHT_CONFLICTS, "DESKTOP_CB_Flug_Konflikte");
	SetPrivilege(ID_CONFLICTS, "DESKTOP_CB_Konflikte");
	SetPrivilege(ID_BDPSUIF, "DESKTOP_CB_Stammdaten");
	SetPrivilege(ID_RULES, "DESKTOP_CB_Regelwerk");
	SetPrivilege(ID_PRINT, "DESKTOP_CB_Drucken");
	SetPrivilege(ID_APP_ABOUT, "DESKTOP_CB_Info");
	*/

	return true;	
}



bool CMainFrame::CreateToolBar(void) {

	TRACE("CMainFrame::CreateToolBar\n");
	if (m_wndToolBar.Create(this, RBS_FIXEDORDER | RBS_AUTOSIZE, 
		WS_CHILD|WS_VISIBLE|WS_CLIPSIBLINGS|WS_CLIPCHILDREN|CBRS_TOP,
		AFX_IDW_REBAR )) //AFX_IDW_TOOLBAR ) )
	{
		CRect olRect;

		GetClientRect(olRect);

 		pomToolBar1 = m_wndToolBar.AddScrollToolBar(IDR_MAINFRAME1);	
		
		olRect.left += 150; //80;
		olRect.top	+= 10;
		olRect.right  = olRect.left + 110;
		olRect.bottom = olRect.top + 200;

		pomViewCombo = m_wndToolBar.AddComboBox(WS_CHILD | WS_VISIBLE | CBS_AUTOHSCROLL |  WS_VSCROLL | CBS_SORT | CBS_DROPDOWNLIST | CBS_HASSTRINGS, 
														 olRect, &m_wndToolBar, IDC_VIEW_COMBO  );


		//pomViewCombo->SetDroppedWidth(200);

		olRect.left += 110;
		olRect.right = olRect.left + 70;
		

		pomViewSeasonCombo = m_wndToolBar.AddComboBox(WS_CHILD | WS_VISIBLE | CBS_AUTOHSCROLL | WS_VSCROLL | CBS_DROPDOWNLIST | CBS_HASSTRINGS, 
														 olRect, &m_wndToolBar,IDC_SEASON_SEL); 
		

		pomToolBar2 = m_wndToolBar.AddScrollToolBar(IDR_MAINFRAME2);
 		UpdateComboBox();

		SetPrivileges();
		
		//m_wndToolBar.UpdateWindow();
		//pomToolBar1->UpdateWindow();
		//pomToolBar2->UpdateWindow();
		return true;
	}
	
	return false;
	
}


BOOL CMainFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	if( !CFrameWnd::PreCreateWindow(cs) )
	{
		return FALSE;
	}
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.lpszClass = AfxRegisterWndClass(0);
	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////
// CMainFrame diagnostics

#ifdef _DEBUG
void CMainFrame::AssertValid() const
{
	CFrameWnd::AssertValid();
}

void CMainFrame::Dump(CDumpContext& dc) const
{
	CFrameWnd::Dump(dc);
}

#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CMainFrame message handlers
void CMainFrame::OnSetFocus(CWnd* pOldWnd)
{
	// forward focus to the view window
	//m_wndView.SetFocus();
}

BOOL CMainFrame::OnCmdMsg(UINT nID, int nCode, void* pExtra, AFX_CMDHANDLERINFO* pHandlerInfo)
{
	// let the view have first crack at the command
	//if (m_wndView.OnCmdMsg(nID, nCode, pExtra, pHandlerInfo))
	//	return TRUE;

	// otherwise, do default handling
	return CFrameWnd::OnCmdMsg(nID, nCode, pExtra, pHandlerInfo);
}


void CMainFrame::OnSize(UINT nType, int cx, int cy) 
{

	//TRACE("CMainFrame::OnSize\n");
	if (cx < 300) return;
	
	CFrameWnd::OnSize(nType, cx, cy);

    CRect olRect; 
	
	GetClientRect(&olRect);

	olRect.top += m_wndToolBar.GetReBarCtrl().GetBarHeight() + 10;

    //CRect olBB1Rect(olRect.right - 36, 27 + 5, olRect.right - 19, (27 + 5) + 25);
    //omBB1.MoveWindow(&olBB1Rect, TRUE);
    
    //CRect olBB2Rect(olRect.right - 19, 27 + 5, olRect.right - 2, (27 + 5) + 25);
    //omBB2.MoveWindow(&olBB2Rect, TRUE);


	int ilPos = (imStartTimeScalePos - olRect.left - 80) / 2;
  
	olRect = CRect(ilPos, olRect.top, ilPos + 80, olRect.top + 18);

	//olRect = CRect(ilPos, 35, ilPos + 80, 52);

	omTSDate.MoveWindow(&olRect, TRUE);


	GetClientRect(&olRect);

	olRect.top += m_wndToolBar.GetReBarCtrl().GetBarHeight() + 10;


    CRect olTSRect(imStartTimeScalePos, olRect.top, olRect.right - (36 + 2), olRect.top + 30);
    
	omTimeScale.MoveWindow(&olTSRect, TRUE);

    olRect.top += 30;                   // for Dialog Bar & TimeScale
    olRect.bottom -= 20;                // for Horizontal Scroll Bar
    omClientWnd.MoveWindow(&olRect, TRUE);

	//PositionChild();
	ChangeViewTo(omViewer.SelectView());//ogCfgData.rmUserSetup.STCV);

	omTSDate.RedrawWindow();
 
}


static void CcaDiagramCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName)
{
	CMainFrame *polDiagram = (CMainFrame *)popInstance;

	switch (ipDDXType)
	{
	case REDISPLAY_ALL:
		polDiagram->RedisplayAll();
		break;
	case STAFFDIAGRAM_UPDATETIMEBAND:
		TIMEPACKET *polTimePacket = (TIMEPACKET *)vpDataPointer;
		polDiagram->SetTimeBand(polTimePacket->StartTime, polTimePacket->EndTime);
		polDiagram->UpdateTimeBand();
		break;
	}
}

/////////////////////////////////////////////////////////////////////////////
// CcaDiagram message handlers
void CMainFrame::PositionChild()
{
    CRect olRect;
    CRect olChartRect;
    CcaChart *polChart;
    
    int ilLastY = 0;
    omClientWnd.GetClientRect(&olRect);
    for (int ilIndex = 0; ilIndex < omPtrArray.GetSize(); ilIndex++)
    {
        polChart = (CcaChart *) omPtrArray.GetAt(ilIndex);
        
        if (ilIndex < imFirstVisibleChart)
        {
            polChart->ShowWindow(SW_HIDE);
            continue;
        }
        
        polChart->GetClientRect(&olChartRect);
        olChartRect.right = olRect.right;

        olChartRect.top = ilLastY;

        ilLastY += polChart->GetHeight();
        olChartRect.bottom = ilLastY;
        
        // check
        if ((polChart->GetState() != Minimized) &&
			(olChartRect.top < olRect.bottom) && (olChartRect.bottom > olRect.bottom))
        {
            olChartRect.bottom = olRect.bottom;
            polChart->SetState(Normal);
        }
        //
        
        polChart->MoveWindow(&olChartRect, FALSE);
		polChart->ShowWindow(SW_SHOW);
	}
    
	omClientWnd.Invalidate(TRUE);
	//SetAllStaffAreaButtonsColor();
	OnUpdatePrevNext();
	UpdateTimeBand();
}


void CMainFrame::SetTSStartTime(CTime opTSStartTime)
{
    omTSStartTime = opTSStartTime;
    if ((omTSStartTime < omStartTime) ||
		(omTSStartTime > omStartTime + omDuration))
    {
        omStartTime = CTime(omTSStartTime.GetYear(), omTSStartTime.GetMonth(), omTSStartTime.GetDay(),
            0, 0, 0) - CTimeSpan(1, 0, 0, 0);
    }
        
    char clBuf[64];
    sprintf(clBuf, "%02d%02d%02d  %d",
        omTSStartTime.GetDay(), omTSStartTime.GetMonth(), omTSStartTime.GetYear() % 100, GetDayOfWeek(omTSStartTime));
    omTSDate.SetWindowText(clBuf);
    omTSDate.Invalidate(FALSE);
}

CListBox *CMainFrame::GetBottomMostGantt()
{
	// Check the size of the area for displaying charts.
	// Pichet used "omClientWnd" not the diagram itself, so we will get the size of this window
	CRect olClientRect;
	omClientWnd.GetClientRect(olClientRect);
	omClientWnd.ClientToScreen(olClientRect);

	// Searching for the bottommost chart
	CcaChart *polChart;
	for (int ilLc = imFirstVisibleChart; ilLc < omPtrArray.GetSize(); ilLc++)
	{
		polChart = (CcaChart *)omPtrArray[ilLc];
		CRect olRect, olChartRect;
		polChart->GetClientRect(olChartRect);
		polChart->ClientToScreen(olChartRect);
		if (!olRect.IntersectRect(&olChartRect, &olClientRect)) 
		{
			break;
		}
	}

	// Check if the chart we have found is a valid one
	--ilLc;
	if (!(0 <= ilLc && ilLc <= omPtrArray.GetSize()-1))
	{
		return NULL;
	}

	return &((CcaChart *)omPtrArray[ilLc])->omGantt;
}

void CMainFrame::SetCaptionText(void)
{
	SetWindowText("");
}


void CMainFrame::SetMarkTime(CTime opStartTime, CTime opEndTime)
{
		CcaChart *polChart;
		for (int ilIndex = 0; ilIndex < omPtrArray.GetSize(); ilIndex++)
		{
			polChart = (CcaChart *) omPtrArray.GetAt(ilIndex);
			polChart->SetMarkTime(opStartTime, opEndTime);
		}
}


void CMainFrame::OnUpdatePrevNext(void)
{
	// Id 25-Sep-96
	// This will set the keyboard focus back to the bottom most list-box after this button was clicked.
	//MWO
	SetFocus();
	//END MWO

	if (imFirstVisibleChart == 0)
	{
		GetDlgItem(IDC_NEXT)->EnableWindow(FALSE);
	}
	else
	{
		GetDlgItem(IDC_NEXT)->EnableWindow(TRUE);
	}

	if (imFirstVisibleChart == omPtrArray.GetUpperBound())
	{
		GetDlgItem(IDC_PREV)->EnableWindow(FALSE);
	}
	else
	{
		GetDlgItem(IDC_PREV)->EnableWindow(TRUE);
	}
}


void CMainFrame::UpdateComboBox()
{
	pomViewCombo->ResetContent();
	CStringArray olStrArr;
	omViewer.GetViews(olStrArr);
	for (int ilIndex = 0; ilIndex < olStrArr.GetSize(); ilIndex++)
	{
		pomViewCombo->AddString(olStrArr[ilIndex]);
	}
	CString olViewName = omViewer.GetViewName();
	if (olViewName.IsEmpty() == TRUE)
	{

		olViewName = omViewer.SelectView();//ogCfgData.rmUserSetup.STCV;
	}

	ilIndex = pomViewCombo->FindString(-1,olViewName);
		
	if (ilIndex != CB_ERR)
	{
		pomViewCombo->SetCurSel(ilIndex);
	}
}



void CMainFrame::UpdateTimeBand()
{
	for (int ilLc = imFirstVisibleChart; ilLc < omPtrArray.GetSize(); ilLc++)
	{
		CcaChart *polChart = (CcaChart *)omPtrArray[ilLc];
		polChart->omGantt.SetMarkTime(omTimeBandStartTime, omTimeBandEndTime);
	}
}

void CMainFrame::SetTimeBand(CTime opStartTime, CTime opEndTime)
{
	omTimeBandStartTime = opStartTime;
	omTimeBandEndTime = opEndTime;
}

void CMainFrame::RedisplayAll()
{
	ChangeViewTo(omViewer.SelectView());//ogCfgData.rmUserSetup.STCV);
	SetCaptionText();
}

void CMainFrame::ChangeViewTo(const char *pcpViewName,bool RememberPositions)
{
	if (bgNoScroll == TRUE)
		return;

	int ilDwRef = m_dwRef;
	AfxGetApp()->DoWaitCursor(1);
	bmNoUpdatesNow = TRUE;
	omViewer.AllowUpdates(bmNoUpdatesNow);

	CCSPtrArray <int> olChartStates;
	olChartStates.RemoveAll();

	CRect olRect; omTimeScale.GetClientRect(&olRect);
    omViewer.ChangeViewTo(pcpViewName, omTSStartTime, omTimeScale.GetTimeFromX(olRect.Width() + 38));


    for (int ilIndex = 0; ilIndex < omPtrArray.GetSize(); ilIndex++)
    {
		if (RememberPositions == TRUE)
		{
			int ilState = ((CcaChart *)omPtrArray[ilIndex])->GetState();
			olChartStates.NewAt(ilIndex,ilState);
		}
		// Id 30-Sep-96
		// This will remove a lot of warning message when the user change view.
		// If we just delete a staff chart, MFC will produce two warning message.
		// First, Revoke not called before the destructor.
		// Second, calling DestroyWindow() in CWnd::~CWnd.
        //delete (CcaChart *)omPtrArray.GetAt(ilIndex);
		((CcaChart *)omPtrArray[ilIndex])->DestroyWindow();
    }
    omPtrArray.RemoveAll();

    CcaChart *polChart;
    CRect olPrevRect;
    omClientWnd.GetClientRect(&olRect);
    
	if (RememberPositions == FALSE)
	{
	    imFirstVisibleChart = 0;
	}
    int ilLastY = 0;
    for (int ilI = 0; ilI < omViewer.GetGroupCount(); ilI++)
    {
        olRect.SetRect(olRect.left, ilLastY, olRect.right, ilLastY);
        
        polChart = new CcaChart;
        polChart->SetTimeScale(&omTimeScale);
        polChart->SetViewer(&omViewer, ilI);
        polChart->SetStatusBar(&omStatusBar);
        polChart->SetStartTime(omTSStartTime);
        polChart->SetInterval(omTimeScale.GetDisplayDuration());
		if (ilI < olChartStates.GetSize())
		{
			polChart->SetState(olChartStates[ilI]);
		}
		else
		{
			polChart->SetState(Maximized);
		}

        polChart->Create(NULL, "CcaChart", WS_OVERLAPPED | WS_BORDER | WS_CHILD | WS_VISIBLE,
            olRect, &omClientWnd,
            0 /* IDD_CHART */, NULL);

		CTime olCurr = CTime::GetCurrentTime();
		if(!bgGatPosLocal)
		{
			ogBasicData.LocalToUtc(olCurr);
		}
		polChart->GetGanttPtr()->SetCurrentTime(olCurr);
        
        omPtrArray.Add(polChart);

        ilLastY += polChart->GetHeight();
    }

	PositionChild();
	AfxGetApp()->DoWaitCursor(-1);
	olChartStates.DeleteAll();
	bmNoUpdatesNow = FALSE;
	omViewer.AllowUpdates(bmNoUpdatesNow);

	// Id 25-Sep-96
	// This will set the keyboard focus back to the bottom most list-box after this button was clicked.
	//SetFocus();
}


void CMainFrame::OnView() 
{

	SetFocus();
	//END MWO

	CcaDiaPropertySheet olDlg("CCADIA", this, &omViewer, 0, GetString(IDS_STRING1646));
	bmIsViewOpen = true;

	if(olDlg.DoModal() != IDCANCEL)
	{
		UpdateWindow();
		omViewer.SelectView(omViewer.GetViewName());

		switch(igAnsichtPageIndex)
		{
		case CCA_ZEITRAUM_PAGE:
			LoadFlights();
			break;
		case CCA_FLUGSUCHEN_PAGE:
			//MessageBox("Flug Suchen", "", MB_OK);
			break;
		case CCA_GEOMETRIE_PAGE:
			//MessageBox("Geometrie", "", MB_OK);
			break;
		default:
			break;
		}
		omViewer.MakeMasstab();
		CTime olT = omViewer.GetGeometrieStartTime();

		CTime olFrom;
		CTime olTo;
		CString olFtyps;
		omViewer.GetTimeSpanAndFtyp(olFrom, olTo, olFtyps);


		CString olS = olT.Format("%d.%m.%Y-%H:%M");

		if(	bgCcaDiaSeason )
		{
			olT = ogCcaDiaDate;
		}

		if(bgGatPosLocal)
		{
//			ogBasicData.UtcToLocal(olT); //RSTL
		}

		SetTSStartTime(olT);
		omTSDuration = omViewer.GetGeometryTimeSpan();
        omTimeScale.SetDisplayTimeFrame(omTSStartTime, omTSDuration, omTSInterval);
        omTimeScale.Invalidate(TRUE);
		ChangeViewTo(omViewer.GetViewName(), false);
		long llTotalMin = omDuration.GetTotalMinutes() - omTSDuration.GetTotalMinutes();
		long nPos = (omTSStartTime - omStartTime).GetTotalMinutes();
		nPos = nPos * 1000L / llTotalMin;
		SetScrollPos(SB_HORZ, int(nPos), TRUE);
		UpdateComboBox();
		//ActivateTables();
	}
	bmIsViewOpen = false;

}


void CMainFrame::ActivateTables()
{
	pogCicDemandTableDlg->Reset();
	pogCicDemandTableDlg->Activate();

	pogCicNoDemandTableDlg->Reset();
	pogCicNoDemandTableDlg->Activate();
	
	pogCicConfTableDlg->Reset();
	pogCicConfTableDlg->Activate();

	pogCicFConfTableDlg->Reset();
	pogCicFConfTableDlg->Activate();
}


void CMainFrame::LoadFlights(char *pspView)
{
	CString olView;

	if(pspView != NULL)
	{
		omViewer.SelectView(pspView);
		olView = CString(pspView);
	}
	else
	{
		omViewer.SelectView(omViewer.GetViewName());
		olView = CString(omViewer.GetViewName());
	}
	
	CString olWhere;
	CTime olFrom;
	CTime olTo;
	CString olFtyps;
	int ilDOO;
	bool blRotation;



	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
	omStatusBar.SetPaneText(0, GetString(IDS_STRING1205));
	omStatusBar.UpdateWindow();
	
	ogDataSet.InitCcaConflictStrings();


	omViewer.GetTimeSpanAndFtyp(olFrom, olTo, olFtyps);

	CTimeSpan olTimeSpan = olTo - olFrom;
	if (olTimeSpan.GetTotalMinutes() <= 0)
	{
		::MessageBox(NULL, GetString(IDS_STRING_NO_TIMESPAN), GetString(IDS_WARNING), (MB_OK));
//		return; (don�t cancel the way, because all lists must be actual!)
	}

	ogCcaDiaFlightData.SetPreSelection(olFrom, olTo, olFtyps);


	ilDOO = omViewer.GetDOO();

	char pclSelection[7000] = "";
	ogCcaDiaDate = TIMENULL;


	TRACE("\n-----------------------------------------------");
	TRACE("\n-----------------------------------------------");
	TRACE("\n-----------------------------------------------");
	TRACE("\n-----------------------------------------------");
	TRACE("\n-----------------------------------------------");
	TRACE("\nBegin read:  %s", CTime::GetCurrentTime().Format("%H:%M:%S"));


	if( ilDOO >= 0 )
	{
		pomViewSeasonCombo->ShowWindow(SW_SHOW);
		pomViewSeasonCombo->ResetContent();
		pomViewSeasonCombo->AddString(CString(olView));
		pomViewSeasonCombo->SelectString(-1, CString(olView));

		ogCcaDiaSingleDate = TIMENULL;

		CString olText;
		ogCcaDiaFlightData.ClearAll();

		CTime olStart = olFrom;
		CTime olEnd;

		CString olFromTime	= olFrom.Format("%H%M");
		CString olToTime	= olTo.Format("%H%M");

		CTimeSpan olOneDay(1,0,0,0);

		TRACE("\nSTART %s------------------------------", CTime::GetCurrentTime().Format("%H:%M:%S"));

		while(olStart < olTo)
		{
			
			if(ilDOO == GetDayOfWeek(olStart))
			{
				olWhere = "";
				olStart = HourStringToDate(olFromTime, olStart);
				olEnd   = HourStringToDate(olToTime, olStart);


				if(ogCcaDiaDate == TIMENULL)
				{
					ogCcaDiaDate = olStart;
				}

				pomViewSeasonCombo->AddString(olStart.Format("%d.%m.%Y"));


				omViewer.GetFlightSearchWhereString(olWhere, blRotation, olStart, olEnd);

				strcpy(pclSelection, olWhere);
				TRACE("ReadFights-Where: %s\n",pclSelection);
				olText = CString("Fl�ge: ") + olStart.Format("%d.%m.%Y");
				omStatusBar.SetPaneText(0, olText);
				omStatusBar.UpdateWindow();
				ogCcaDiaFlightData.ReadFlights(pclSelection, false, true);

				olText = CString("Cca: ") + olStart.Format("%d.%m.%Y");
				omStatusBar.SetPaneText(0, olText);
				omStatusBar.UpdateWindow();
				ogCcaDiaFlightData.ReadCcas(olStart, olEnd);

				//TRACE("\n %s", olWhere);

			}
			olStart += olOneDay;
		}

		TRACE("\nEnd read:    %s", CTime::GetCurrentTime().Format("%H:%M:%S"));
		TRACE("\n-----------------------------------------------");
		TRACE("\n-----------------------------------------------");

		omViewer.SetLoadTimeFrame(ogCcaDiaDate, olEnd);
		

		bgCcaDiaSeason = true;


	}
	else
	{
		
		bgCcaDiaSeason = false;
		ogCcaDiaDate = TIMENULL;

		omViewer.GetZeitraumWhereString(olWhere, blRotation);

		//olWhere += CString(" AND FLNO > ' '");
	
		ogCcaDiaFlightData.ReadAllFlights(olWhere, blRotation);
		
		pomViewSeasonCombo->ShowWindow(SW_HIDE);

	}


	TRACE("\nBegin read BLK:  %s", CTime::GetCurrentTime().Format("%H:%M:%S"));


	CString olText = CString("Blk... ");
	omStatusBar.SetPaneText(0, olText);
	omStatusBar.UpdateWindow();

	char pclWhere[512]="";

	sprintf(pclWhere, " WHERE (NATO >= '%s' and NAFR <= '%s') OR (NAFR <= '%s' and NATO = ' ') OR  (NATO >= '%s'  and NAFR = ' ')",
												 olFrom.Format("%Y%m%d%H%M00"), olTo.Format("%Y%m%d%H%M59"),
												 olTo.Format("%Y%m%d%H%M59"),
												 olFrom.Format("%Y%m%d%H%M00"));
	ogBCD.Read(CString("BLK"), pclWhere);


	TRACE("\nEnd read BLK:    %s", CTime::GetCurrentTime().Format("%H:%M:%S"));

	CString olCnams = ogCcaDiaFlightData.omCcaData.MakeBlkData(TIMENULL, ilDOO);

	TRACE("\nEnd Make BLK:    %s", CTime::GetCurrentTime().Format("%H:%M:%S"));

	TRACE("\n-----------------------------------------------");
	TRACE("\n-----------------------------------------------");

	omViewer.SetCnams(olCnams);
	omViewer.SetDOO(ilDOO);


	//int ilC5 = ogBCD.GetDataCount("BLK");


	//int ilBnam = ogBCD.GetFieldIndex("CIC", "CNAM");
	//ogBCD.SetSort("CIC", "TERM+,CNAM+", true); //true ==> sort immediately



	

	if(bgCcaDiaSeason)
	{
		TRACE("\nBegin compress:  %s", CTime::GetCurrentTime().Format("%H:%M:%S"));

		olText = CString("Compress FLights/Cki... ");
		omStatusBar.SetPaneText(0, olText);
		omStatusBar.UpdateWindow();

		ogCcaDiaFlightData.Kompress();
		ogCcaDiaFlightData.omCcaData.KompressND();

		TRACE("\nEnd compress:    %s", CTime::GetCurrentTime().Format("%H:%M:%S"));
		TRACE("\n-----------------------------------------------");
		TRACE("\n-----------------------------------------------");
	
	}

	TRACE("\nBegin tables:  %s", CTime::GetCurrentTime().Format("%H:%M:%S"));

	ActivateTables();

	TRACE("\nEnd tables:    %s", CTime::GetCurrentTime().Format("%H:%M:%S"));
	TRACE("\n-----------------------------------------------");
	TRACE("\n-----------------------------------------------");




	TRACE("\n ANZ FLIGHT: %d\n", ogCcaDiaFlightData.omData.GetSize());
	TRACE("\n ANZ CCA   : %d\n", ogCcaDiaFlightData.omCcaData.omData.GetSize());
	TRACE("\n-----------------------------------------------");
	TRACE("\n-----------------------------------------------");

/*
	int ilBlk = 0;
	int ilFlight = 0;
	DIACCADATA *prlCca;

	for(int g = ogCcaDiaFlightData.omCcaData.omData.GetSize() -1 ; g >= 0; g-- )
	{
		prlCca = &ogCcaDiaFlightData.omCcaData.omData[g];

		if(CString(prlCca->Ctyp) == "")
			ilFlight++;

		if(CString(prlCca->Ctyp) == "N")
			ilBlk++;

	}

	TRACE("\n ANZ FLIGHTCCA: %d\n", ilFlight);
	TRACE("\n ANZ BLKCCA   : %d\n", ilBlk);
	TRACE("\n-----------------------------------------------");
	TRACE("\n-----------------------------------------------");
*/
	olText = CString("Check overlapping... ");
	omStatusBar.SetPaneText(0, olText);
	omStatusBar.UpdateWindow();
	
	TRACE("\nBegin Overlap:  %s", CTime::GetCurrentTime().Format("%H:%M:%S"));

	int ilCount = ogBCD.GetDataCount("CIC");

	for(int i = 0; i < ilCount; i++)
	{
		ogDataSet.CheckCCaForOverlapping(ogBCD.GetField("CIC", i, "CNAM"));
	}
	TRACE("\nEnd Overlap:    %s", CTime::GetCurrentTime().Format("%H:%M:%S"));
	TRACE("\n-----------------------------------------------");
	TRACE("\n-----------------------------------------------");


	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	omStatusBar.SetPaneText(0, "");
	omStatusBar.UpdateWindow();

	RecalcLayout();

	// Set 'all new conflicts' - radio button in the flight-conflict dialog
	/*
	if (pogCicFConfTableDlg->IsDlgButtonChecked(IDC_R_FLIGHTCONF))
	{
		pogCicFConfTableDlg->CheckRadioButton(IDC_R_ALLCONF, IDC_R_FLIGHTCONF, IDC_R_ALLCONF);
	}   // !!! IT DOESNT WORK !!!
	*/

	// Set main-window-caption
	if (ogCcaDiaFlightData.omData.GetSize() > 0)
	{
		CString olFlightNumStr;
		olFlightNumStr.Format("%ld", ogCcaDiaFlightData.omData.GetSize());
		SetWindowText(ogAppName + CString(": ") + olFlightNumStr + CString(" ") + GetString(IDS_CAPTION));
	}
	else 
	{
		SetWindowText(ogAppName);
	}


}


void CMainFrame::OnDelete() 
{

	DelelteCounterDlg *polDlg = new DelelteCounterDlg(this);
	if(polDlg->DoModal() == IDOK)
	{
		OnViewSelChange();
	}
	delete polDlg;

}

void CMainFrame::OnViewSelChange()
{

    char clText[64];
    pomViewCombo->GetLBText(pomViewCombo->GetCurSel(), clText);
	pomViewCombo->ShowDropDown(FALSE);

	if(strcmp(clText, "<Default>") == 0)
	{
		return;
	}
	LoadFlights(clText); 
	omViewer.SelectView(clText);
	omViewer.MakeMasstab();
	CTime olT = omViewer.GetGeometrieStartTime();

	if(	bgCcaDiaSeason ) 
	{
		olT = ogCcaDiaDate;
	}

	if(bgGatPosLocal)
	{
//		ogBasicData.UtcToLocal(olT); //RSTL
	}

	CTime olFrom;
	CTime olTo;
	CString olFtyps;
	omViewer.GetTimeSpanAndFtyp(olFrom, olTo, olFtyps);

	//if((olFrom > olT) || (olTo < olT))
	//	olT = olFrom;

	CString olS = olT.Format("%d.%m.%Y-%H:%M");
	SetTSStartTime(olT);
	omTSDuration = omViewer.GetGeometryTimeSpan();

    
	long llTotalMin = omDuration.GetTotalMinutes() - omTSDuration.GetTotalMinutes();
	long nPos = (omTSStartTime - omStartTime).GetTotalMinutes();
	nPos = nPos * 1000L / llTotalMin;
	SetScrollPos(SB_HORZ, int(nPos), TRUE);
	
	omTimeScale.SetDisplayTimeFrame(omTSStartTime, omTSDuration, omTSInterval);
    omTimeScale.Invalidate(TRUE);
	SetTSStartTime(omTSStartTime);
	ChangeViewTo(clText, false);

	//ActivateTables();


}


void CMainFrame::OnAllocate() 
{

	int ilRet = 0;
	AllocateCcaParameter *polDlg = new AllocateCcaParameter(this);
	ilRet = polDlg->DoModal();
	delete polDlg;
	
	if(ilRet == -1)
	{
		return;
	}
	/*
	Allocate olAlloc(ilRet);
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
	omStatusBar.SetPaneText(0, GetString(IDS_STRING1492)); // Einteilung l�uft---------------------
	omStatusBar.UpdateWindow();
	olAlloc.Run();
	omStatusBar.SetPaneText(0, GetString(IDS_STRING1493)); // Speichern des Plans---------------------
	omStatusBar.UpdateWindow();
	*/

	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));

	if(bgCcaDiaSeason)
	{

		TRACE("\n-----------------------------------------------");
		TRACE("\nBegin compress: %s", CTime::GetCurrentTime().Format("%H:%M:%S"));
	
		ogCcaDiaFlightData.Kompress();
		SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));

		ogCcaDiaFlightData.omCcaData.KompressND();


		TRACE("\nEnd compress:   %s", CTime::GetCurrentTime().Format("%H:%M:%S"));
		TRACE("\n-----------------------------------------------");
	
	}

	TRACE("\n-----------------------------------------------");
	TRACE("\nBegin repaint: %s", CTime::GetCurrentTime().Format("%H:%M:%S"));


	RepaintAll(0,0);

	TRACE("\nend repaint:   %s", CTime::GetCurrentTime().Format("%H:%M:%S"));
	TRACE("\n-----------------------------------------------");

	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));

	TRACE("\n-----------------------------------------------");
	TRACE("\nBegin tables: %s", CTime::GetCurrentTime().Format("%H:%M:%S"));

	
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
//	ActivateTables();
	pogCicDemandTableDlg->Reset();
	pogCicDemandTableDlg->Activate();

	pogCicNoDemandTableDlg->Reset();
	pogCicNoDemandTableDlg->Activate();
	
	pogCicConfTableDlg->Reset();
	pogCicConfTableDlg->Activate();
	
	TRACE("\nEnd tables:   %s", CTime::GetCurrentTime().Format("%H:%M:%S"));
	TRACE("\n-----------------------------------------------");
	
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	


}


void CMainFrame::OnCreateDemands() 
{
	int ilRet = 0;
	CicCreateDemandsDlg *polDlg = new CicCreateDemandsDlg(this);
	ilRet = polDlg->DoModal();

	if (ilRet == IDOK)
	{
		MessageBox(GetString(IDS_CREATE_DEMANDS_MESSAGE), GetString(IDS_WARNING), (MB_OK|MB_ICONINFORMATION));


		ogCcaDiaFlightData.CreateDemands(polDlg->omSearchText);
	}

	delete polDlg;
	
}


void CMainFrame::OnUpdateCreateDemands(CCmdUI* pCmdUI) 
{
	//pCmdUI->Enable(!bgCreateDemIsActive);
}


LONG CMainFrame::RepaintAll(WPARAM wParam, LPARAM lParam)
{
	ChangeViewTo(omViewer.GetViewName(), true);
	return 0L;
}

void CMainFrame::OnWoDemand() 
{

	pogCicNoDemandTableDlg->ShowWindow(SW_SHOW);
	pogCicNoDemandTableDlg->SetWindowPos(&wndTop, 0,0,0,0, SWP_SHOWWINDOW | SWP_NOSIZE | SWP_NOMOVE );

}

void CMainFrame::OnDemand() 
{
	pogCicDemandTableDlg->ShowWindow(SW_SHOW);
	pogCicDemandTableDlg->SetWindowPos(&wndTop, 0,0,0,0, SWP_SHOWWINDOW | SWP_NOSIZE | SWP_NOMOVE );
	
}

void CMainFrame::OnConflicts() 
{
	pogCicConfTableDlg->ShowWindow(SW_SHOW);
	pogCicConfTableDlg->SetWindowPos(&wndTop, 0,0,0,0, SWP_SHOWWINDOW | SWP_NOSIZE | SWP_NOMOVE );
	
}


void CMainFrame::OnFlightConflicts() 
{	
	pogCicFConfTableDlg->ShowWindow(SW_SHOW);
	pogCicFConfTableDlg->SetWindowPos(&wndTop, 0,0,0,0, SWP_SHOWWINDOW | SWP_NOSIZE | SWP_NOMOVE );

}


void CMainFrame::OnPrint() 
{
	omViewer.PrintGantt(omPtrArray);
}	


LONG CMainFrame::OnPositionChild(WPARAM wParam, LPARAM lParam)
{
    PositionChild();
    return 0L;
}


void CMainFrame::GetClientRect( LPRECT olRect)
{
	CFrameWnd::GetClientRect( olRect);

}


void CMainFrame::OnTimer(UINT nIDEvent)
{
	if(bmNoUpdatesNow == FALSE)
	{


		CTime olLocalTime = CTime::GetCurrentTime();
		CTime olUtcTime = CTime::GetCurrentTime();
		ogBasicData.LocalToUtc(olUtcTime);

		

		if(bgGatPosLocal)
		{
			omTimeScale.UpdateCurrentTimeLine(olLocalTime);
		}
		else
		{
			omTimeScale.UpdateCurrentTimeLine(olUtcTime);
		}

		CcaChart *polChart;
		for (int ilIndex = 0; ilIndex < omPtrArray.GetSize(); ilIndex++)
		{
			polChart = (CcaChart *) omPtrArray.GetAt(ilIndex);

			if(bgGatPosLocal)
			{
				polChart->GetGanttPtr()->SetCurrentTime(olLocalTime);
			}
			else
			{
				polChart->GetGanttPtr()->SetCurrentTime(olUtcTime);
			}
		}


		CFrameWnd::OnTimer(nIDEvent);
	}
}

void CMainFrame::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
{
    // TODO: Add your message handler code here and/or call default
    
    //CFrameWnd::OnHScroll(nSBCode, nPos, pScrollBar);


	CUIntArray olLines;

	int ilCurrLine;

	CcaChart *polChart;
	for (int ilLc = 0; ilLc < omPtrArray.GetSize(); ilLc++)
	{
		polChart = (CcaChart *)omPtrArray[ilLc];

		ilCurrLine = polChart->omGantt.GetTopIndex();
		olLines.Add(ilCurrLine);
	}


    long llTotalMin;
    int ilPos;
    
    switch (nSBCode)
    {
        case SB_LINEUP :
            //OutputDebugString("LineUp\n\r");
            
            llTotalMin = omDuration.GetTotalMinutes() - omTSDuration.GetTotalMinutes();            
            ilPos = GetScrollPos(SB_HORZ) - int (60 * 1000L / llTotalMin);
            if (ilPos <= 0)
            {
                ilPos = 0;
                SetTSStartTime(omStartTime);
            }
            else
			{
                SetTSStartTime(omTSStartTime - CTimeSpan(0, 0, 60, 0));
			}

            omTimeScale.SetDisplayStartTime(omTSStartTime);
            omTimeScale.Invalidate(TRUE);
			ChangeViewTo(omViewer.SelectView());//ogCfgData.rmUserSetup.STCV,TRUE);
            SetScrollPos(SB_HORZ, ilPos, TRUE);
////////////////////////////////////////////////////////////////////////
// Damkerng 06/27/96:
// This thing should be remove. The technic has been changed to use the
// method Invalidate() not only for the TimeScale, but for the entire
// omClientWnd also.
            omClientWnd.Invalidate(FALSE);
////////////////////////////////////////////////////////////////////////

        break;
        
        case SB_LINEDOWN :
            //OutputDebugString("LineDown\n\r");

            llTotalMin = omDuration.GetTotalMinutes() - omTSDuration.GetTotalMinutes();            
            ilPos = GetScrollPos(SB_HORZ) + int (60 * 1000L / llTotalMin);
            if (ilPos >= 1000)
            {
                ilPos = 1000;
                SetTSStartTime(omStartTime + CTimeSpan(0, 0, int (llTotalMin), 0));
            }
            else
			{
                SetTSStartTime(omTSStartTime + CTimeSpan(0, 0, 60, 0));
			}

            omTimeScale.SetDisplayStartTime(omTSStartTime);
            omTimeScale.Invalidate(TRUE);
			ChangeViewTo(omViewer.SelectView());//ogCfgData.rmUserSetup.STCV,TRUE);

			SetScrollPos(SB_HORZ, ilPos, TRUE);
////////////////////////////////////////////////////////////////////////
// Damkerng 06/27/96:
// This thing should be remove. The technic has been changed to use the
// method Invalidate() not only for the TimeScale, but for the entire
// omClientWnd also.
            omClientWnd.Invalidate(FALSE);
////////////////////////////////////////////////////////////////////////
        break;
        
        case SB_PAGEUP :
            //OutputDebugString("PageUp\n\r");
            
            llTotalMin = omDuration.GetTotalMinutes() - omTSDuration.GetTotalMinutes();            
            ilPos = GetScrollPos(SB_HORZ)
                - int ((omTSDuration.GetTotalMinutes() / 2) * 1000L / llTotalMin);
            if (ilPos <= 0)
            {
                ilPos = 0;
                SetTSStartTime(omStartTime);
            }
            else
			{
                SetTSStartTime(omTSStartTime - CTimeSpan(0, 0, int (omTSDuration.GetTotalMinutes() / 2), 0));
			}

            omTimeScale.SetDisplayStartTime(omTSStartTime);
            omTimeScale.Invalidate(TRUE);
			ChangeViewTo(omViewer.SelectView());//ogCfgData.rmUserSetup.STCV,TRUE);

            SetScrollPos(SB_HORZ, ilPos, TRUE);
////////////////////////////////////////////////////////////////////////
// Damkerng 06/27/96:
// This thing should be remove. The technic has been changed to use the
// method Invalidate() not only for the TimeScale, but for the entire
// omClientWnd also.
            omClientWnd.Invalidate(FALSE);
////////////////////////////////////////////////////////////////////////
        break;
        
        case SB_PAGEDOWN :
            //OutputDebugString("PageDown\n\r");
            
            llTotalMin = omDuration.GetTotalMinutes() - omTSDuration.GetTotalMinutes();            
            ilPos = GetScrollPos(SB_HORZ)
                + int ((omTSDuration.GetTotalMinutes() / 2) * 1000L / llTotalMin);
            if (ilPos >= 1000)
            {
                ilPos = 1000;
                SetTSStartTime(omStartTime + CTimeSpan(0, 0, int (llTotalMin), 0));
            }
            else
			{
                SetTSStartTime(omTSStartTime + CTimeSpan(0, 0, int (omTSDuration.GetTotalMinutes() / 2), 0));
			}

            omTimeScale.SetDisplayStartTime(omTSStartTime);
            omTimeScale.Invalidate(TRUE);
			ChangeViewTo(omViewer.SelectView());//ogCfgData.rmUserSetup.STCV,TRUE);

            SetScrollPos(SB_HORZ, ilPos, TRUE);
////////////////////////////////////////////////////////////////////////
// Damkerng 06/27/96:
// This thing should be remove. The technic has been changed to use the
// method Invalidate() not only for the TimeScale, but for the entire
// omClientWnd also.
            omClientWnd.Invalidate(FALSE);
////////////////////////////////////////////////////////////////////////
        break;
        
        case SB_THUMBTRACK /* pressed, any drag time */:
            //OutputDebugString("ThumbTrack\n\r");

            llTotalMin = omDuration.GetTotalMinutes() - omTSDuration.GetTotalMinutes();
            
            SetTSStartTime(omStartTime + CTimeSpan(0, 0, int (llTotalMin * nPos / 1000), 0));
			////ChangeViewTo(omViewer.SelectView());
            
            omTimeScale.SetDisplayStartTime(omTSStartTime);
            omTimeScale.Invalidate(TRUE);
            
            SetScrollPos(SB_HORZ, nPos, TRUE);

            //char clBuf[64];
            //wsprintf(clBuf, "Thumb Position : %d", nPos);
            //omStatusBar.SetWindowText(clBuf);
        break;

        //case SB_THUMBPOSITION /* released */:
            //OutputDebugString("ThumbPosition\n\r");
        //break;
////////////////////////////////////////////////////////////////////////
// Damkerng 06/27/96:
// This thing should be remove. The technic has been changed to use the
// method Invalidate() not only for the TimeScale, but for the entire
// omClientWnd also.
        case SB_THUMBPOSITION:	// the thumb was just released?
			ChangeViewTo(omViewer.SelectView());//ogCfgData.rmUserSetup.STCV,TRUE);
            omClientWnd.Invalidate(FALSE);

////////////////////////////////////////////////////////////////////////

        case SB_TOP :
        //break;
        case SB_BOTTOM :
            //OutputDebugString("TopBottom\n\r");
        break;
        
        case SB_ENDSCROLL :
            //OutputDebugString("EndScroll\n\r");
            //OutputDebugString("\n\r");
        break;
    }


	for (ilLc = 0; ilLc < omPtrArray.GetSize(); ilLc++)
	{
		polChart = (CcaChart *)omPtrArray[ilLc];
		polChart->omGantt.SetTopIndex(olLines[ilLc]);
	}


}

void CMainFrame::OnNow() 
{

	// Id 25-Sep-96
	// This will set the keyboard focus back to the bottom most list-box after this button was clicked.
	//MWO
	SetFocus();
	//END MWO

	CTime olTime = CTime::GetCurrentTime();

	if(!bgGatPosLocal)
	{
		ogBasicData.LocalToUtc(olTime);
	}

	//Die wollen local
	olTime -= CTimeSpan(0, 1, 0, 0);
	SetTSStartTime(olTime);
	//TRACE("Time: [%s]\n", olTime.Format("%H:%M"));

	omTimeScale.SetDisplayStartTime(omTSStartTime);
    omTimeScale.Invalidate(TRUE);
		
	// update scroll bar position
    long llTotalMin = omDuration.GetTotalMinutes() - omTSDuration.GetTotalMinutes();
	long nPos = (omTSStartTime - omStartTime).GetTotalMinutes();
	nPos = nPos * 1000L / llTotalMin;
    SetScrollPos(SB_HORZ, int(nPos), TRUE);

	ChangeViewTo(omViewer.SelectView());//ogCfgData.rmUserSetup.STCV);
    omClientWnd.Invalidate(FALSE);	

}


void CMainFrame::OnRules() 
{
	char pclConfigPath[256];
	char pclExcelPath[256];

	if (getenv("CEDA") == NULL)
		strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
	else
		strcpy(pclConfigPath, getenv("CEDA"));

	GetPrivateProfileString(ogAppName, "RULES", "DEFAULT",
		pclExcelPath, sizeof pclExcelPath, pclConfigPath);


	if(!strcmp(pclExcelPath, "DEFAULT"))
		MessageBox(GetString(IDS_STRING1478), GetString(ST_FEHLER), MB_ICONERROR);


	/*
	HWND hw=::FindWindow("#32770","Duty Preferences");
	//::ShowWindow(hw,SW_RESTORE   );
	::ShowWindow(hw,SW_MINIMIZE);
	::ShowWindow(hw,SW_SHOWNORMAL      );
	//::SetActiveWindow(hw);
	*/

	char *args[4];
	char slRunTxt[256] = "";
	args[0] = "child";
	sprintf(slRunTxt,"%s,%s",pcgUser,pcgPasswd);
//	args[1] = slRunTxt;
	args[1] = NULL;
	args[2] = NULL;
	args[3] = NULL;
	int ilReturn = _spawnv(_P_NOWAIT,pclExcelPath,args);

}


void CMainFrame::OnBdpsuif() 
{
	char pclConfigPath[256];
	char pclExcelPath[256];

    if (getenv("CEDA") == NULL)
        strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
    else
        strcpy(pclConfigPath, getenv("CEDA"));

    GetPrivateProfileString(ogAppName, "BDPS-UIF", "DEFAULT",
        pclExcelPath, sizeof pclExcelPath, pclConfigPath);


	if(!strcmp(pclExcelPath, "DEFAULT"))
		MessageBox(GetString(IDS_STRING981), GetString(ST_FEHLER), MB_ICONERROR);

	//CString	olTables = "ALTACTACRAPTRWYTWYPSTGATCICBLTEXTDENMVTNATHAGWROHTYSTYFID-STRSPHSEA";//Alle
	// Update der Liste von ARE 07.05.99
	//CString	olTables = "ALT ACT ACR APT RWY TWY PST GAT CIC BLT EXT DEN MVT NAT HAG WRO HTY STY FID STR SPH SEA GHS GEG PER PEF GRM GRN ORG PFC COT ASF BSS BSD ODA TEA STF HTC WAY PRC CHT TIP HOL WGP SWG";
	// Update der Liste von ARE 17.05.99
	CString	olTables = "ALT-ACT-ACR-APT-RWY-TWY-PST-GAT-CIC-BLT-EXT-DEN-MVT-NAT-HAG-WRO-HTY-STY-FID-STR-SPH-SEA-GHS-GEG-PER-PEF-GRM-GRN-ORG-PFC-COT-ASF-BSS-BSD-ODA-TEA-STF-HTC-WAY-PRC-CHT-TIP-BLK-HOL-WGP-SWG-PGP-AWI-CCC-VIP";


	char *args[4];
	char slRunTxt[256];
	args[0] = "child";
	sprintf(slRunTxt,"%s,%s,%s,%s",ogAppName,pcgUser,pcgPasswd,olTables);
	args[1] = slRunTxt;
	args[2] = NULL;
	args[3] = NULL;
	int ilReturn = _spawnv(_P_NOWAIT,pclExcelPath,args);
	
}


void CMainFrame::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	// check if the control key is pressed
    BOOL blIsControl = ::GetKeyState(VK_CONTROL) & 0x8080;
		// This statement has to be fixed for using both in Windows 3.11 and NT.
		// In Windows 3.11 the bit 0x80 will be turned on if control key is pressed.
		// In Windows NT, the bit 0x8000 will be turned on if control key is pressed.

	switch (nChar)
	{
	case VK_UP:		// move the bottom most gantt chart up/down one line
	case VK_DOWN:
		CListBox *polGantt;
		if ((polGantt = GetBottomMostGantt()) != NULL)
		{
			polGantt->SendMessage(WM_USERKEYDOWN, nChar);
		}
		break;
	case VK_PRIOR:
		//blIsControl? OnFirstChart(): OnPrevChart();
		break;
	case VK_NEXT:
		//blIsControl? OnLastChart(): OnNextChart();
		break;
	case VK_LEFT:
		OnHScroll(blIsControl? SB_PAGEUP: SB_LINEUP, 0, NULL);
		break;
	case VK_RIGHT:
		OnHScroll(blIsControl? SB_PAGEDOWN: SB_LINEDOWN, 0, NULL);
		break;
	case VK_HOME:
		SetScrollPos(SB_HORZ, 0, FALSE);
		OnHScroll(SB_LINEUP, 0, NULL);
		break;
	case VK_END:
		SetScrollPos(SB_HORZ, 1000, FALSE);
		OnHScroll(SB_LINEDOWN, 0, NULL);
		break;
	default:
		CFrameWnd::OnKeyDown(nChar, nRepCnt, nFlags);
		break;
	}
}

void CMainFrame::OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	CFrameWnd::OnKeyUp(nChar, nRepCnt, nFlags);
}

BOOL CMainFrame::OnEraseBkgnd(CDC* pDC) 
{
	// TODO: Add your message handler code here and/or call default
	
	return CFrameWnd::OnEraseBkgnd(pDC);
}

void CMainFrame::OnPaint() 
{
	CPaintDC dc(this); // device context for painting
	
    CBrush olBrush( RGB( 192, 192, 192 ) );
    CBrush *polOldBrush = dc.SelectObject( &olBrush );
    CRect olRect;
    GetClientRect( &olRect );
    dc.FillRect( &olRect, &olBrush );
    dc.SelectObject( polOldBrush );
	
}


LONG CMainFrame::OnUpdateDiagram(WPARAM wParam, LPARAM lParam)
{
    CString olStr;
    int ipGroupNo = HIWORD(lParam);
    int ipLineNo = LOWORD(lParam);
	CTime olTime = CTime((time_t) lParam);

	char clBuf[255];
	int ilCount;

    CcaChart *polCcaChart = (CcaChart *) omPtrArray.GetAt(ipGroupNo);
    CcaGantt *polCcaGantt = polCcaChart -> GetGanttPtr();
    switch (wParam)
    {
        // group message
        case UD_INSERTGROUP :
            // CreateChild(ipGroupNo);

            if (ipGroupNo <= imFirstVisibleChart)
			{
                imFirstVisibleChart++;
				OnUpdatePrevNext();
			}
            PositionChild();
        break;

        case UD_UPDATEGROUP :
            olStr = omViewer.GetGroupText(ipGroupNo);
            polCcaChart->GetChartButtonPtr()->SetWindowText(olStr);
            polCcaChart->GetChartButtonPtr()->Invalidate(FALSE);

            olStr = omViewer.GetGroupTopScaleText(ipGroupNo);
            polCcaChart->GetTopScaleTextPtr()->SetWindowText(olStr);
            polCcaChart->GetTopScaleTextPtr()->Invalidate(TRUE);
			//SetStaffAreaButtonColor(ipGroupNo);
        break;
        
        case UD_DELETEGROUP :
            delete omPtrArray.GetAt(ipGroupNo);
            //omPtrArray.GetAt(ipGroupNo) -> DestroyWindow();
            omPtrArray.RemoveAt(ipGroupNo);
            
            if (ipGroupNo <= imFirstVisibleChart)
			{
                imFirstVisibleChart--;
				OnUpdatePrevNext();
			}
            PositionChild();
        break;

        // line message
        case UD_INSERTLINE :
            polCcaGantt->InsertString(ipLineNo, "");
			polCcaGantt->RepaintItemHeight(ipLineNo);
			
			ilCount = omViewer.GetLineCount(ipGroupNo);
			sprintf(clBuf, "%d", ilCount);
			polCcaChart->GetCountTextPtr()->SetWindowText(clBuf);
			polCcaChart->GetCountTextPtr()->Invalidate(TRUE);

			//SetStaffAreaButtonColor(ipGroupNo);
            PositionChild();
        break;
        
        case UD_UPDATELINE :
            polCcaGantt->RepaintVerticalScale(ipLineNo);
            polCcaGantt->RepaintGanttChart(ipLineNo);
			//SetStaffAreaButtonColor(ipGroupNo);
        break;

        case UD_DELETELINE :
            polCcaGantt->DeleteString(ipLineNo);
            
			ilCount = omViewer.GetLineCount(ipGroupNo);
			sprintf(clBuf, "%d", ilCount);
			polCcaChart->GetCountTextPtr()->SetWindowText(clBuf);
			polCcaChart->GetCountTextPtr()->Invalidate(TRUE);

			//SetStaffAreaButtonColor(ipGroupNo);
			PositionChild();
        break;

        case UD_UPDATELINEHEIGHT :
            polCcaGantt->RepaintItemHeight(ipLineNo);
			//SetStaffAreaButtonColor(ipGroupNo);
            PositionChild();
		break;
    }

    return 0L;
}

void CMainFrame::OnAppExit() 
{
	if (AfxMessageBox(IDS_STRING1705, MB_OKCANCEL) == IDOK)
	{
		CFrameWnd::OnClose();
	}
}



void CMainFrame::OnSeasonViewSelChange()
{

	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
    
	CString olText;

    pomViewSeasonCombo->GetLBText(pomViewSeasonCombo->GetCurSel(), olText);
    pomViewSeasonCombo->ShowDropDown(FALSE);
	
	

	if(olText == CString(omViewer.GetViewName()))
	{
		ogCcaDiaSingleDate = TIMENULL;
		omTSStartTime = CTime(ogCcaDiaDate.GetYear(),ogCcaDiaDate.GetMonth(), ogCcaDiaDate.GetDay(), 0,0,0 );
		ogCcaDiaFlightData.Kompress();
		ogCcaDiaFlightData.omCcaData.KompressND();
	}
	else
	{
		ogCcaDiaSingleDate = DateStringToDate(olText);
		omTSStartTime = CTime(ogCcaDiaSingleDate.GetYear(),ogCcaDiaSingleDate.GetMonth(), ogCcaDiaSingleDate.GetDay(), 0,0,0 );
	}
	
	if(bgGatPosLocal)
//		ogBasicData.UtcToLocal(omTSStartTime); //RSTL

	SetTSStartTime(omTSStartTime);
	omTSDuration = omViewer.GetGeometryTimeSpan();

    
	long llTotalMin = omDuration.GetTotalMinutes() - omTSDuration.GetTotalMinutes();
	long nPos = (omTSStartTime - omStartTime).GetTotalMinutes();
	nPos = nPos * 1000L / llTotalMin;
	SetScrollPos(SB_HORZ, int(nPos), TRUE);
	
	omTimeScale.SetDisplayTimeFrame(omTSStartTime, omTSDuration, omTSInterval);
    omTimeScale.Invalidate(TRUE);
	SetTSStartTime(omTSStartTime);
	ChangeViewTo(omViewer.SelectView(), false);

	ActivateTables();

	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));

}



void CMainFrame::OnSymbolText() 
{
	bmSymbolText = !bmSymbolText;

	if (bmSymbolText)
	{
		pomToolBar1->ButtonText(bmSymbolText, 75, 50);
		pomToolBar2->ButtonText(bmSymbolText, 75, 50);
	}
	else
	{
		pomToolBar1->ButtonText(bmSymbolText);
		pomToolBar2->ButtonText(bmSymbolText);
	}
	
	CRect olClientRect;
	GetClientRect(olClientRect);
	OnSize(SIZE_RESTORED, olClientRect.Width(), olClientRect.Height());

 
	//TRACE("CMainFrame::OnSymbolText: TB1.ButtonText = %s\n", pomToolBar1->GetButtonText(0));


	return;
}


void CMainFrame::OnReport() 
{
	//TRACE("CMainFrame::OnReport\n");
	// get 'fromTime', 'toTime' and terminal selection
	CDailyCcaPrintDlg *polPrintDlg = new CDailyCcaPrintDlg(this);

	if (polPrintDlg->DoModal() == IDOK)
	{
		
 		// Are the flights of the specified day loaded?
		CCAFLIGHTDATA *prlFlight;
		bool blFlightsLoaded = false;
		for(int i = ogCcaDiaFlightData.omData.GetSize() - 1; i >= 0; i --)
		{
			prlFlight = &ogCcaDiaFlightData.omData[i];
			if (prlFlight->Tifd >= polPrintDlg->omMinDate &&
				prlFlight->Tifd <= polPrintDlg->omMaxDate)
			{
				blFlightsLoaded = true;
				break;
			}
		}
		if (!blFlightsLoaded)
		{
			MessageBox(GetString(IDS_NO_FLIGHT_DATA), GetString(IDS_WARNING));
			delete polPrintDlg;
			return;
		}
	

		//TRACE("Start CReportTableDlg\n");
 		CReportTableDlg olReportTableDlg(this, polPrintDlg->omMinDate, polPrintDlg->omMaxDate);
		olReportTableDlg.DoModal();
	}

	delete polPrintDlg;
}


void CMainFrame::OnReportSeason()
{
	
	//TRACE("CMainFrame::OnReport\n");	
 	CReportSeasonTableDlg olReportSeasonTableDlg(this, omViewer);
	olReportSeasonTableDlg.DoModal();
 
}

void CMainFrame::OnCreateCommon()
{
	DIACCADATA rlCCA;
	rlCCA.Ckbs = ogCcaDiaSingleDate;
	rlCCA.Ckes = ogCcaDiaSingleDate;

	if (pogCommonCcaDlg)
		pogCommonCcaDlg->NewData(&rlCCA);
 
}

void CMainFrame::OnUpdateCreateCommon(CCmdUI* pCmdUI) 
{
	if (bgCcaDiaSeason && ogCcaDiaSingleDate == TIMENULL)
		pCmdUI->Enable(FALSE);
	else
		pCmdUI->Enable(TRUE);
	
}

void CMainFrame::OnUpdateReportSeason(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable(bgCcaDiaSeason && ogCcaDiaSingleDate == TIMENULL);
	
}



void CMainFrame::OnUpdateSymbolText(CCmdUI* pCmdUI) 
{
	pCmdUI->SetCheck(bmSymbolText);
	
}



void CMainFrame::OnUpdateToolBarButtons(CCmdUI* pCmdUI) 
{
	unsigned short clButtonState;

	if (omPrivMap.Lookup((void *)pCmdUI->m_nID, clButtonState))
	{
		if (clButtonState == '0')
		{
			pCmdUI->Enable(FALSE);
		}
	}
}



void CMainFrame::OnClose() 
{
	
	OnAppExit();
}




LONG CMainFrame::OnBcAdd(UINT wParam, LONG /*lParam*/)
{
	ogBcHandle.GetBc(wParam); 
	
	return TRUE;
} 
