// DelelteCounterDlg.cpp : implementation file
//

#include <stdafx.h>
#include <opsscm.h>
#include <DelelteCounterDlg.h>
#include <CcaCedaFlightData.h>
#include <CCSGlobl.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// DelelteCounterDlg dialog


DelelteCounterDlg::DelelteCounterDlg(CWnd* pParent /*=NULL*/)
	: CDialog(DelelteCounterDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(DelelteCounterDlg)
	//}}AFX_DATA_INIT
}


void DelelteCounterDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(DelelteCounterDlg)
	DDX_Control(pDX, IDC_VK, m_VT);
	DDX_Control(pDX, IDC_DATETO, m_To);
	DDX_Control(pDX, IDC_DATEFROM, m_From);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(DelelteCounterDlg, CDialog)
	//{{AFX_MSG_MAP(DelelteCounterDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// DelelteCounterDlg message handlers

void DelelteCounterDlg::OnOK() 
{
	/* WARNING: DATA REPAIR TOOL --> ONLY USE BY RST

	CedaDiaCcaData olData;

	DIACCADATA *prlCca;
	char pclWhere[256];

	strcpy(pclWhere, "WHERE FLNO='US 893' AND CKBS LIKE '1970%'");

	olData.Read(pclWhere, true, false);


	for(int i = olData.omData.GetSize() -1; i>=0 ; i--)
	{
		prlCca = &olData.omData[i];


		prlCca->Ckbs = HourStringToDate( prlCca->Ckbs.Format("%H%M"), prlCca->Ckes);

		TRACE("\n %s %s %s %s",prlCca->Ckic ,prlCca->Flno , prlCca->Ckbs.Format("%d.%m.%Y  %M:%H"), prlCca->Ckes.Format("%d.%m.%Y  %M:%H"));

		prlCca->IsChanged = DATA_CHANGED;

		olData.Save(prlCca);

	}

	//olData.Release(&olData.omData, false, false);

	*/	



	BOOL blRet = FALSE;
	if(m_From.GetStatus() == true && m_To.GetStatus() == true)
	{
		CString olF, olT;
		m_From.GetWindowText(olF);
		m_To.GetWindowText(olT);

		CString olVT;
		m_VT.GetWindowText(olVT);
		olVT.TrimRight();

		if(olF.IsEmpty() || olT.IsEmpty() || ( !olVT.IsEmpty() && CString("1234567").Find(olVT) < 0 ))
		{
			MessageBox(GetString(IDS_NO_DATE), GetString(IDS_WARNING), MB_OK);
			return;
		}
		CTime olFrom = DateStringToDate(olF);
		CTime olTo   = DateStringToDate(olT);


//MWO StringTable
		char pclMsg[512]="";

	
		if(olVT.IsEmpty())
		{
			sprintf(pclMsg, GetString(IDS_STRING1515), olFrom.Format("%d.%m.%Y"), olTo.Format("%d.%m.%Y"));
		}
		else
		{
			sprintf(pclMsg, GetString(IDS_STRING1530), olVT, olFrom.Format("%d.%m.%Y"), olTo.Format("%d.%m.%Y"));
		}

		if(MessageBox(pclMsg, GetString(ST_FRAGE), MB_YESNO) == IDYES)
		{
			if(ogCcaDiaFlightData.omCcaData.DeleteOrUpdateTimeFrame(olFrom, olTo, olVT) == true)
			{
				blRet = TRUE;
			}
			else
			{
				blRet = FALSE;
			}
		}
	}

	// TODO: Add extra initialization here
	
	CDialog::OnOK();
}

BOOL DelelteCounterDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	m_From.SetTypeToDate(true);
	m_To.SetTypeToDate(true);

	CRect olR1 = CRect(0,0,100,100);
	CRect olR2 = CRect(0,0,0,0);
	CRect olTest;
	BOOL blRet = olTest.IntersectRect( olR1, olR2 );
//MWO Stringtable
	//SetWindowText("Counter-Zuordnung l�schen");
	if(blRet == TRUE)
	{
		int x =0;
	}

	return TRUE;              // EXCEPTION: OCX Property Pages should return FALSE
}
