// OPSSCM.h : main header file for the OPSSCM application
//

#if !defined(AFX_OPSSCM_H__1D5121C4_9846_11D3_97CB_00008630DDD6__INCLUDED_)
#define AFX_OPSSCM_H__1D5121C4_9846_11D3_97CB_00008630DDD6__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include <resource.h>       // main symbols

/////////////////////////////////////////////////////////////////////////////
// COPSSCMApp:
// See OPSSCM.cpp for the implementation of this class
//

class COPSSCMApp : public CWinApp
{
public:
	COPSSCMApp();
	~COPSSCMApp();

	void InitialLoad(CWnd *pParent);


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(COPSSCMApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation
private:
	bool SetMenuPrivilege(int ipCId, const CString &opPrivStr) const;


public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	//{{AFX_MSG(COPSSCMApp)
	afx_msg void OnAppAbout();
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_OPSSCM_H__1D5121C4_9846_11D3_97CB_00008630DDD6__INCLUDED_)
