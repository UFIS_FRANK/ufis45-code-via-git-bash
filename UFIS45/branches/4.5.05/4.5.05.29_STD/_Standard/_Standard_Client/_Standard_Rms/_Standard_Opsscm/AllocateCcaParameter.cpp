// AllocateCcaParameter.cpp: Implementierungsdatei
//

#include <StdAfx.h>
#include <opsscm.h>
#include <Timer.h>
#include <CedaBasicData.h>

#include <AllocateCcaParameter.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld AllocateCcaParameter 


AllocateCcaParameter::AllocateCcaParameter(CWnd* pParent /*=NULL*/)
	: CDialog(AllocateCcaParameter::IDD, pParent)
{
	//{{AFX_DATA_INIT(AllocateCcaParameter)
		// HINWEIS: Der Klassen-Assistent f�gt hier Elementinitialisierung ein
	//}}AFX_DATA_INIT
}


void AllocateCcaParameter::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(AllocateCcaParameter)
		// HINWEIS: Der Klassen-Assistent f�gt hier DDX- und DDV-Aufrufe ein
	//}}AFX_DATA_MAP
	DDX_Control(pDX, IDC_PROGRESS1, omProgressBar);
	DDX_Control(pDX, IDC_TIME, omTime);
}


BEGIN_MESSAGE_MAP(AllocateCcaParameter, CDialog)
	//{{AFX_MSG_MAP(AllocateCcaParameter)
	ON_BN_CLICKED(IDC_CANCEL, OnCancel)
	ON_BN_CLICKED(IDC_ALL, OnAll)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()



BOOL AllocateCcaParameter::OnInitDialog() 
{
	CDialog::OnInitDialog();

	omTime.SetWindowText("00:00");
	omProgressBar.SetPos(0);
	omProgressBar.SetStep(1);
	//omProgressBar.EnableWindow(false);

	return TRUE;
}


void AllocateCcaParameter::AllocateDemands( CCSPtrArray<DEMANDLIST> &ropDemandArrays, CCSPtrArray<CCAFLIGHTDATA> &ropFlightArray)
{
	
	int ilCountLegs = ropDemandArrays.GetSize();
	
	DEMANDDATA *prlDemand;
	CString olCcaList;
	CString olCnam;
	CStringArray olCcas;
	bool blTreffer = true;
	char buffer[64];
	int ilStartIndex;
	int ilCount; 

	CCSPtrArray<DIACCADATA> olNewCcas;
	CCSPtrArray<DEMANDLIST> rlDemandArrays;
	CCSPtrArray<DEMANDDATA> *prlArray;
	CCSPtrArray<DEMANDDATA> *prlDemands;


	CCAFLIGHTDATA *prlFlight;


	prlDemands = &ropDemandArrays[0];
	ogCcaDiaFlightData.omDemandData.GetDemandArrayByGroups( rlDemandArrays, *prlDemands);


	int ilGroupCount = rlDemandArrays.GetSize();




	for(int g = 0; g < ilGroupCount ; g++)
	{

		prlDemands = &ropDemandArrays[0];

		rlDemandArrays.RemoveAll();
		ogCcaDiaFlightData.omDemandData.GetDemandArrayByGroups( rlDemandArrays, *prlDemands);

		prlArray = &rlDemandArrays[g];

		ilCount = prlArray->GetSize();

		olCcas.RemoveAll();

		ogCcaDiaFlightData.omDemandData.GetCcas( &(*prlArray)[0], olCcas);

		blTreffer = true;

		ilStartIndex = 0;


		for(int j = 0; j <= olCcas.GetSize() - ilCount; j++)
		{


			for(int m = 0; m < ropDemandArrays.GetSize() ; m++)
			{

				prlDemands = &ropDemandArrays[m];

				rlDemandArrays.RemoveAll();
				ogCcaDiaFlightData.omDemandData.GetDemandArrayByGroups( rlDemandArrays, *prlDemands);


				prlArray = &rlDemandArrays[g];


				for(int k = 0; k < ilCount; k++)
				{
					olCnam = olCcas[j+k];

					if(olCnam.IsEmpty())
					{
						j = j+k+1;
						blTreffer = false;
						break;
					}

					prlDemand = &(*prlArray)[k];

					if(!ogCcaDiaFlightData.omCcaData.Check(olCnam, prlDemand->Debe, prlDemand->Deen))
					{
						blTreffer = false;
						break;
					}
				}
			}
			if(blTreffer)
			{

				for(int m = 0; m < ropDemandArrays.GetSize() ; m++)
				{
					prlDemands = &ropDemandArrays[m];
					rlDemandArrays.RemoveAll();
					
					ogCcaDiaFlightData.omDemandData.GetDemandArrayByGroups( rlDemandArrays, *prlDemands);

					prlArray = &rlDemandArrays[g];

					for(int k = 0; k < ilCount; k++)
					{
						olCnam = olCcas[j+k];

						prlDemand = &(*prlArray)[k];

						prlFlight = &ropFlightArray[m];


						DIACCADATA *prlCca = new DIACCADATA;

						prlCca->Cdat = CTime::GetCurrentTime();
						strcpy(prlCca->Usec, pcgUser);
						prlCca->Flnu = prlFlight->Urno;
						prlCca->Urno = ogBasicData.GetNextUrno();
						strcpy(prlCca->Ctyp, " ");
						strcpy(prlCca->Ckic, olCnam);
						prlCca->Ckbs = prlDemand->Debe;
						prlCca->Ckes = prlDemand->Deen;
						prlCca->Ghpu = prlDemand->Urno;
						prlCca->IsSelected = false;
						prlCca->IsChanged = DATA_NEW;
						prlCca->Stod = prlFlight->Stod;

						strcpy(prlCca->Act3, prlFlight->Act3);
						strcpy(prlCca->Flno, prlFlight->Flno);

						ltoa(prlDemand->Urud, buffer, 10);
						prlCca->Ghsu = atol(ogBCD.GetField("RUD", "URNO", CString(buffer),"URUE"));
						
						olNewCcas.Add(prlCca);
					}

					ogCcaDiaFlightData.omCcaData.Release(&olNewCcas, true, false);
			
					olNewCcas.RemoveAll();
				}
				break;
			}
		}
	}


	
	
	
/*	
	
	DEMANDDATA *prlDemand;
	DEMANDDATA *prlDemand2;
	CString olCcaList;
	CString olCnam;
	CStringArray olCcas;
	bool blTreffer = true;
	char buffer[64];

	CCSPtrArray<DIACCADATA> olNewCcas;
	CCSPtrArray<DIACCADATA> olTmpCcas;


	CCSPtrArray<DEMANDDATA> *polDemands;
	CCSPtrArray<DEMANDDATA> *polDemands2;


	polDemands = &rlDemandArrays[0];


	CCAFLIGHTDATA *prlFlight = &rlFlightArray[0];
	CCAFLIGHTDATA *prlFlight2;







	for(int i = polDemands->GetSize() - 1; i >= 0 && blTreffer; i--)
	{
		prlDemand = &(*polDemands)[i];

		ogCcaDiaFlightData.omDemandData.GetCcas(prlDemand, olCcas);

		blTreffer = true;

		for(int j = olCcas.GetSize() -1; j >= 0; j--)
		{
			olCnam = olCcas[j];

			if(olCcaList.Find(olCnam) < 0)
			{

				for(int k = 0; k < rlDemandArrays.GetSize(); k++)
				{
					polDemands2 = &rlDemandArrays[k];

					if(polDemands2->GetSize() > i)
					{
						prlDemand2 = &(*polDemands2)[i];

						if(!ogCcaDiaFlightData.omCcaData.Check(olCnam, prlDemand2->Debe, prlDemand2->Deen))
						{
							blTreffer = false;
							break;
						}
					}
				}


				if(blTreffer)
				{

					for(int k = 0; k < rlDemandArrays.GetSize(); k++)
					{
						polDemands2 = &rlDemandArrays[k];
						if(polDemands2->GetSize() > i)
						{
				
							prlDemand2 = &(*polDemands2)[i];

							prlFlight2 = &rlFlightArray[k];

							DIACCADATA *prlCca = new DIACCADATA;

							prlCca->Cdat = CTime::GetCurrentTime();
							strcpy(prlCca->Usec, pcgUser);
							prlCca->Flnu = prlFlight2->Urno;
							prlCca->Urno = ogBasicData.GetNextUrno();
							strcpy(prlCca->Ctyp, " ");
							strcpy(prlCca->Ckic, olCnam);
							prlCca->Ckbs = prlDemand2->Debe;
							prlCca->Ckes = prlDemand2->Deen;
							prlCca->Ghpu = prlDemand2->Urno;
							prlCca->IsSelected = false;
							prlCca->IsChanged = DATA_NEW;
							prlCca->Stod = prlFlight2->Stod;

							ltoa(prlDemand2->Urud, buffer, 10);
							prlCca->Ghsu = atol(ogBCD.GetField("RUD", "URNO", CString(buffer),"URUE"));

							strcpy(prlCca->Act3, prlFlight2->Act3);
							strcpy(prlCca->Flno, prlFlight2->Flno);

							olNewCcas.Add(prlCca);
						}
					}
					olCcaList += CString(",") + olCnam;
					break;
				}
			}
		}
	}

	if(!blTreffer)
	{
		olNewCcas.DeleteAll();
	}
	else
	{
		ogCcaDiaFlightData.omCcaData.Release(&olNewCcas, true, false);
	}
*/
}



void AllocateCcaParameter::AllocateDemands( CCSPtrArray<DEMANDDATA> &ropDemands, CCAFLIGHTDATA *prpFlight)
{
	DEMANDDATA *prlDemand;
	CString olCcaList;
	CString olCnam;
	CStringArray olCcas;
	bool blTreffer = true;
	char buffer[64];

	int ilStartIndex;

	CCSPtrArray<DIACCADATA> olNewCcas;
	CCSPtrArray<DEMANDLIST> rlDemandArrays;
	CCSPtrArray<DEMANDDATA> *prlArray;

	ogCcaDiaFlightData.omDemandData.GetDemandArrayByGroups( rlDemandArrays, ropDemands);

	int ilCount; 

	for(int i = rlDemandArrays.GetSize() - 1; i >= 0 ; i--)
	{
		prlArray = &rlDemandArrays[i];
	
		ilCount = prlArray->GetSize();

		olCcas.RemoveAll();

		ogCcaDiaFlightData.omDemandData.GetCcas( &(*prlArray)[0], olCcas);



		ilStartIndex = 0;

		for(int j = 0; j <= olCcas.GetSize() - ilCount; j++)
		{
			blTreffer = true;
			for(int k = 0; k < ilCount; k++)
			{
				olCnam = olCcas[j+k];

				if(olCnam.IsEmpty())
				{
					j = j+k+1;
					blTreffer = false;
					break;
				}

				prlDemand = &(*prlArray)[k];

				if(!ogCcaDiaFlightData.omCcaData.Check(olCnam, prlDemand->Debe, prlDemand->Deen))
				{
					blTreffer = false;
					break;
				}
			}

			if(blTreffer)
			{

				for(k = 0; k < ilCount; k++)
				{
					olCnam = olCcas[j+k];

					prlDemand = &(*prlArray)[k];

					DIACCADATA *prlCca = new DIACCADATA;

					prlCca->Cdat = CTime::GetCurrentTime();
					strcpy(prlCca->Usec, pcgUser);
					prlCca->Flnu = prpFlight->Urno;
					prlCca->Urno = ogBasicData.GetNextUrno();
					strcpy(prlCca->Ctyp, " ");
					strcpy(prlCca->Ckic, olCnam);
					prlCca->Ckbs = prlDemand->Debe;
					prlCca->Ckes = prlDemand->Deen;
					prlCca->Ghpu = prlDemand->Urno;
					prlCca->IsSelected = false;
					prlCca->IsChanged = DATA_NEW;

					prlCca->Stod = prpFlight->Stod;

					strcpy(prlCca->Act3, prpFlight->Act3);
					strcpy(prlCca->Flno, prpFlight->Flno);

					ltoa(prlDemand->Urud, buffer, 10);
					prlCca->Ghsu = atol(ogBCD.GetField("RUD", "URNO", CString(buffer),"URUE"));
					
					olNewCcas.Add(prlCca);
				}

				ogCcaDiaFlightData.omCcaData.Release(&olNewCcas, true, false);
		
				olNewCcas.RemoveAll();
				break;
			}
		}

	}
}
void AllocateCcaParameter::AllocateDailyCommonDemands( CCSPtrArray<DEMANDDATA> &ropDemands)
{
	DEMANDDATA *prlDemand;
	CString olCcaList;
	CString olCnam;
	CStringArray olCcas;
	bool blTreffer = true;
	char buffer[64];

	CCSPtrArray<DIACCADATA> olNewCcas;
	CCSPtrArray<DEMANDLIST> rlDemandArrays;
	CCSPtrArray<DEMANDDATA> *prlArray;

	ogCcaDiaFlightData.omDemandData.GetDemandArrayByGroups( rlDemandArrays, ropDemands);

	int ilCount = 0; 
	for(int i = rlDemandArrays.GetSize() - 1; i >= 0 ; i--)
	{
		prlArray = &rlDemandArrays[i];
		ilCount = prlArray->GetSize();
		olCcas.RemoveAll();
		ogCcaDiaFlightData.omDemandData.GetCcas( &(*prlArray)[0], olCcas);

		for(int l = 0; l < ilCount; l++)
		{
			for(int j = 0; j < olCcas.GetSize(); j++)
			{
				olCnam = olCcas[j];
				if(!olCnam.IsEmpty())
				{
					prlDemand = &(*prlArray)[l];
					if(ogCcaDiaFlightData.omCcaData.ExistCca(prlDemand->Urno))
						break;

					if(ogCcaDiaFlightData.omCcaData.Check(olCnam, prlDemand->Debe, prlDemand->Deen))
					{
						DIACCADATA *prlCca = new DIACCADATA;

						prlCca->Cdat = CTime::GetCurrentTime();
						strcpy(prlCca->Usec, pcgUser);
						prlCca->Flnu = 0;
						prlCca->Urno = ogBasicData.GetNextUrno();
						strcpy(prlCca->Ctyp, "C");
						strcpy(prlCca->Ckic, olCnam);
						prlCca->Ckbs = prlDemand->Debe;
						prlCca->Ckes = prlDemand->Deen;
						prlCca->Ghpu = prlDemand->Urno;
						prlCca->IsSelected = false;
						prlCca->IsChanged = DATA_NEW;

						ltoa(prlDemand->Urud, buffer, 10);
						prlCca->Ghsu = atol(ogBCD.GetField("RUD", "URNO", CString(buffer),"URUE"));
						
						olNewCcas.Add(prlCca);
						ogCcaDiaFlightData.omCcaData.Release(&olNewCcas, true, false);
				
						olNewCcas.RemoveAll();
						break;
					}
				}
			}

			omProgressBar.StepIt();
		}
	}
}

void AllocateCcaParameter::AllocateKompressCommonDemands( CCSPtrArray<DEMANDDATA> &ropDemands)
{
	DEMANDDATA *prlDemand;
	CString olCcaList;
	CString olCnam;
	CStringArray olCcas;
	bool blTreffer = true;
	char buffer[64];

	CCSPtrArray<DIACCADATA> olNewCcas;
	CCSPtrArray<DEMANDLIST> rlDemandArrays;
	CCSPtrArray<DEMANDDATA> *prlArray;

	ogCcaDiaFlightData.omDemandData.GetDemandArrayByGroups( rlDemandArrays, ropDemands);
	int ilCount; 

	for(int i = rlDemandArrays.GetSize() - 1; i >= 0 ; i--)
	{
		prlArray = &rlDemandArrays[i];
		ilCount = prlArray->GetSize();
		olCcas.RemoveAll();
		ogCcaDiaFlightData.omDemandData.GetCcas( &(*prlArray)[0], olCcas);
		CCSPtrArray<DEMANDDATA> prlTmpArray;

		while (prlArray->GetSize() > 0)
		{
			prlTmpArray.RemoveAll();
			int ilBestCcaIndex = -1;
			int ilLastAlloc = 0;
			for(int j = 0; j < olCcas.GetSize(); j++)
			{
				int ilActAlloc = 0;
				olCnam = olCcas[j];
				if(!olCnam.IsEmpty())
				{
					for(int k = prlArray->GetSize() -1; k >=0; k--)
					{
						prlDemand = &(*prlArray)[k];
						if(ogCcaDiaFlightData.omCcaData.Check(olCnam, prlDemand->Debe, prlDemand->Deen))
						{
							ilActAlloc++;
						}
					}
					if (ilActAlloc > ilLastAlloc)
					{
						ilLastAlloc = ilActAlloc;
						ilBestCcaIndex = j;
						if (ilLastAlloc == prlArray->GetSize())
							break;
					}
				}
			}
			if (ilBestCcaIndex >= 0 && ilBestCcaIndex < olCcas.GetSize())
			{
				olCnam = olCcas[ilBestCcaIndex];
				for(int k = prlArray->GetSize() -1; k >=0; k--)
				{
					prlDemand = &(*prlArray)[k];
					if(ogCcaDiaFlightData.omCcaData.Check(olCnam, prlDemand->Debe, prlDemand->Deen))
					{
						prlTmpArray.Add(prlDemand);
						prlArray->RemoveAt(k);
					}
				}
			}

			for(int l = 0; l < prlTmpArray.GetSize(); l++)
			{
				prlDemand = &prlTmpArray[l];

				DIACCADATA *prlCca = new DIACCADATA;

				prlCca->Cdat = CTime::GetCurrentTime();
				strcpy(prlCca->Usec, pcgUser);

				prlCca->Flnu = 0;

				prlCca->Urno = ogBasicData.GetNextUrno();

				strcpy(prlCca->Ctyp, "C");

				strcpy(prlCca->Ckic, olCnam);
				prlCca->Ckbs = prlDemand->Debe;
				prlCca->Ckes = prlDemand->Deen;
				prlCca->Ghpu = prlDemand->Urno;
				prlCca->IsSelected = false;
				prlCca->IsChanged = DATA_NEW;

				ltoa(prlDemand->Urud, buffer, 10);
				prlCca->Ghsu = atol(ogBCD.GetField("RUD", "URNO", CString(buffer),"URUE"));
				
				olNewCcas.Add(prlCca);
			}

			ogCcaDiaFlightData.omCcaData.Release(&olNewCcas, true, false);
	
			olNewCcas.RemoveAll();

			if (ilBestCcaIndex == -1)
				break;
		}
	}
}


void AllocateCcaParameter::Run(int ipMode)
{
	CCSPtrArray<DEMANDDATA> rlCommonDemands;
	CCSPtrArray<DEMANDDATA> rlDemands;
	CCSPtrArray<DEMANDDATA> *prlDemands;
	CCSPtrArray<DEMANDDATA> *prlDemandsTmp;
	CCSPtrArray<CCAFLIGHTDATA> rlFlights;
	CCAFLIGHTDATA *prlFlight;

	bool blHasCcas;
	bool blTimeSpan = false;

	

	CString olRestTimeStr;
	
	if(!bgCcaDiaSeason)
	{	

		rlCommonDemands.RemoveAll();
		int ilCount = ogCcaDiaFlightData.omData.GetSize() -1;
		if (ogCcaDiaFlightData.omDemandData.GetCommonDemands(rlCommonDemands))
			ilCount += rlCommonDemands.GetSize();


//		TRACE("ProgressBar (0,%ld)\n",ogCcaDiaFlightData.omData.GetSize() -1);
//		omProgressBar.SetRange32(0, ogCcaDiaFlightData.omData.GetSize() -1);
		TRACE("ProgressBar (0,%ld)\n",ilCount);
		omProgressBar.SetRange32(0, ilCount);

//		Timer olTimer(ogCcaDiaFlightData.omData.GetSize());
		Timer olTimer(ilCount);
		olTimer.Start();
		for( int i = ogCcaDiaFlightData.omData.GetSize() -1 ; i >= 0; i--)
		{
			prlFlight = &ogCcaDiaFlightData.omData[i];
			blHasCcas = false;
			if(strcmp(prlFlight->Adid, "D") == 0)
			{
				rlDemands.RemoveAll();
				ogCcaDiaFlightData.omDemandData.GetDemansByOuro( rlDemands, prlFlight->Urno);

				for(int j = rlDemands.GetSize()-1; j >= 0; j--)
				{
					if(ogCcaDiaFlightData.omCcaData.ExistCca(rlDemands[j].Urno))
					{
						blHasCcas = true;
						break;
					}
				}

				if(!blHasCcas)
				{
					AllocateDemands(rlDemands, prlFlight);
				}
			}
		
			olTimer.Step();

			// Update time display
			if (olTimer.Valid())
			{
				olRestTimeStr.Format("%02d:%02d", olTimer.GetMin(), olTimer.GetSec());
				omTime.SetWindowText(olRestTimeStr);
			}
			omProgressBar.StepIt();
		}

		//common demands
		AllocateDailyCommonDemands(rlCommonDemands);
/*			blHasCcas = false;
			for(int k = rlCommonDemands.GetSize()-1; k >= 0; k--)
			{
				if(ogCcaDiaFlightData.omCcaData.ExistCca(rlCommonDemands[k].Urno))
				{
					blHasCcas = true;
					break;
				}
			}

			if(!blHasCcas)
			{
				AllocateDailyCommonDemands(rlCommonDemands);
			}
*/


	}
	else
	{
		CCSPtrArray<CCAFLIGHTDATA> *prlArray;
		CCSPtrArray<DEMANDLIST> rlDemandArrays;
		POSITION pos;
		CString olTmp;

		bool blDiff = false;

		TRACE("ProgressBar (0,%ld)\n", ogCcaDiaFlightData.omKompMap.GetCount());
		omProgressBar.SetRange32(0, ogCcaDiaFlightData.omKompMap.GetCount());

		Timer olTimer(ogCcaDiaFlightData.omKompMap.GetCount());
		olTimer.Start();
		for( pos = ogCcaDiaFlightData.omKompMap.GetStartPosition(); pos != NULL; )
		{
			ogCcaDiaFlightData.omKompMap.GetNextAssoc( pos, olTmp , (void *&)prlArray );

			blHasCcas = false;
			for(int k = 0; k < prlArray->GetSize() && !blHasCcas; k++)
			{
				prlFlight = &(*prlArray)[k];

				
				TRACE("K=%d %s  %s", k, prlFlight->Flno, prlFlight->Stod.Format("%d.%m.%y %H:%M"));				
				
				prlDemands = new CCSPtrArray<DEMANDDATA> ;

				ogCcaDiaFlightData.omDemandData.GetDemansByOuro( *prlDemands, prlFlight->Urno);


				if(rlDemandArrays.GetSize() > 0)
				{
					blDiff = false;


						if( rlDemandArrays[0].GetSize() != prlDemands->GetSize() )
							blDiff = true;

						
						if(!blDiff)
						{
							if( prlDemands->GetSize() > 0 )
							{

								char buffer1[64];
								char buffer2[64];
								ltoa( (*prlDemands)[0].Urud, buffer1, 10);

								prlDemandsTmp = &rlDemandArrays[0];

								ltoa( (*prlDemandsTmp)[0].Urud, buffer1, 10);

								CString olRueUrno1 = ogBCD.GetField("RUD", "URNO", CString(buffer1),"URUE");
								CString olRueUrno2 = ogBCD.GetField("RUD", "URNO", CString(buffer2),"URUE");

								if(olRueUrno1 != olRueUrno2)
									blDiff = true;
							}
						
						}

						if(!blHasCcas && blDiff)
						{
							AllocateDemands(rlDemandArrays, rlFlights);
							rlDemandArrays.DeleteAll();
							rlFlights.RemoveAll();
							blHasCcas = false;
						}

				}


				for(int j = prlDemands->GetSize()-1; j >= 0; j--)
				{
					if(ogCcaDiaFlightData.omCcaData.ExistCca( (*prlDemands)[j].Urno))
					{
						blHasCcas = true;
						break;
					}
				}

				rlFlights.Add(prlFlight);			
				rlDemandArrays.Add(prlDemands);


			}

			if(!blHasCcas)
			{
				AllocateDemands(rlDemandArrays, rlFlights);
			}

			rlDemandArrays.DeleteAll();
			rlFlights.RemoveAll();

			olTimer.Step();

			// Update time display
			if (olTimer.Valid())
			{
				olRestTimeStr.Format("%02d:%02d", olTimer.GetMin(), olTimer.GetSec());
				omTime.SetWindowText(olRestTimeStr);
			}


			omProgressBar.StepIt();
			
		}

		//common demands

		DEMANDDATA *prlDemand;
		CCSPtrArray<DEMANDDATA> *prlDemandArray;

		int ilCount = ogCcaDiaFlightData.omDemandData.omKKeyMap.GetCount();
		TRACE("ProgressBar (0,%ld)\n", ilCount);
		omProgressBar.SetRange32(0, ilCount);
		Timer olTimerCommon(ilCount);
		olTimerCommon.Start();

		void  *pVoid;
		for( pos = ogCcaDiaFlightData.omDemandData.omKKeyMap.GetStartPosition(); pos != NULL; )
		{

			ogCcaDiaFlightData.omDemandData.omKKeyMap.GetNextAssoc( pos, pVoid, (void *&)prlDemandArray );
			prlDemand = &(*prlDemandArray)[0];
			blHasCcas = false;

			if(prlDemand->Dety[0] == CCI_DEMAND && prlDemand->Ouro == 0)
			{

				for(int j = prlDemandArray->GetSize()-1; j >= 0; j--)
				{
					if(ogCcaDiaFlightData.omCcaData.ExistCca( (*prlDemandArray)[j].Urno))
					{
						blHasCcas = true;
						break;
					}
				}

				if (!blHasCcas)
					AllocateKompressCommonDemands(*prlDemandArray);
			}

			olTimerCommon.Step();
			// Update time display
			if (olTimerCommon.Valid())
			{
				olRestTimeStr.Format("%02d:%02d", olTimerCommon.GetMin(), olTimerCommon.GetSec());
				omTime.SetWindowText(olRestTimeStr);
			}
			omProgressBar.StepIt();

		}




		/*

		CCSPtrArray<CCAFLIGHTDATA> *prlArray;
		CCSPtrArray<DEMANDLIST> rlDemandArrays;
		POSITION pos;
		CString olTmp;

		TRACE("ProgressBar (0,%ld)\n", ogCcaDiaFlightData.omKompMap.GetCount()-1);
		omProgressBar.SetRange32(0, ogCcaDiaFlightData.omKompMap.GetCount()-1);

		Timer olTimer(ogCcaDiaFlightData.omKompMap.GetCount());
		olTimer.Start();
		for( pos = ogCcaDiaFlightData.omKompMap.GetStartPosition(); pos != NULL; )
		{
			ogCcaDiaFlightData.omKompMap.GetNextAssoc( pos, olTmp , (void *&)prlArray );

			blHasCcas = false;
			for(int k = 0; k < prlArray->GetSize() && !blHasCcas; k++)
			{
				prlFlight = &(*prlArray)[k];

				prlDemands = new CCSPtrArray<DEMANDDATA> ;

				rlDemandArrays.Add(prlDemands);

				ogCcaDiaFlightData.omDemandData.GetDemansByOuro( *prlDemands, prlFlight->Urno);

				for(int j = prlDemands->GetSize()-1; j >= 0; j--)
				{
					if(ogCcaDiaFlightData.omCcaData.ExistCca( (*prlDemands)[j].Urno))
					{
						blHasCcas = true;
						break;
					}
				}
			}

			if(!blHasCcas)
			{
				AllocateDemands(rlDemandArrays, *prlArray);
			}

			rlDemandArrays.DeleteAll();


			olTimer.Step();

			// Update time display
			if (olTimer.Valid())
			{
				olRestTimeStr.Format("%02d:%02d", olTimer.GetMin(), olTimer.GetSec());
				omTime.SetWindowText(olRestTimeStr);
			}


			omProgressBar.StepIt();
			
		}

		*/



	}

}





/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten AllocateCcaParameter 

void AllocateCcaParameter::OnAll() 
{
	GetDlgItem(IDC_ALL)->EnableWindow(false);
	GetDlgItem(IDC_CANCEL)->EnableWindow(false);

	Run(0);
	EndDialog(1);	
}


void AllocateCcaParameter::OnCancel() 
{
	EndDialog(-1);	
}

