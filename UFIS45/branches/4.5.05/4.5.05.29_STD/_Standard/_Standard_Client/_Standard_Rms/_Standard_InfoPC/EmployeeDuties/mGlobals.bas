Attribute VB_Name = "mGlobals"
Public sAppl As String
Public sHopo As String
Public sTableExt As String
Public sServer As String
Public sConnectType As String

Public save_COLUMN_PAIRS As Integer
Public save_COLUMN_WIDTH As Integer
Public save_TAB_LINES As Long
Public save_LINEHEIGHT As Integer
Public save_TAB_FONT As String
Public save_TAB_FONT_SIZE As Integer
Public save_MINUS_X_HOURS As Integer
Public save_PLUS_X_HOURS As Integer
Public save_COLOR1 As Long
Public save_COLOR2 As Long
Public save_FUNCTIONS As String
Public save_BC_HIGHLIGHT_SECONDS As Integer
Public save_RELOAD_MINUTES As Long
Public save_COLUMN_PAIR_SEPARATOR As Long
Public save_LOGO As String

Public sgUTCOffsetHours As Single
