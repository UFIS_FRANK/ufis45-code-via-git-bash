// Table.h: interface for the Table class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_TABLE_H__F6245E63_0C5B_11D4_A713_00010204AA63__INCLUDED_)
#define AFX_TABLE_H__F6245E63_0C5B_11D4_A713_00010204AA63__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


//#include "ccsglobl.h"
#include <ccsobj.h>
#include <CCSDragDropCtrl.h>

// Constants for CTable::SetTextLineSeparator()
#define ST_NORMAL	0
#define ST_THICK	1

class Table; //Predecl.



class CTableListBox: public CListBox
{
public:
    Table *pomTableWindow;

	CRect omClipBox;

	int GetItemFromPoint(CPoint point);
	void DetectCurrentColumn(CPoint point);

protected:
    //{{AFX_MSG(CTableListBox)
    afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
    afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
    afx_msg LONG OnDragOver(UINT, LONG); 
    afx_msg LONG OnDrop(UINT, LONG); 
    afx_msg void OnPaint();
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	//}}AFX_MSG
    DECLARE_MESSAGE_MAP()
};




class Table: public CWnd
{
public:
	Table();
	//virtual ~Table();
    Table
	(
        CWnd *popParentWindow,
        int ipXStart, int ipXEnd, int ipYStart, int ipYEnd,
        COLORREF lpTextColor = GetSysColor(COLOR_WINDOWTEXT),
        COLORREF lpTextBkColor = GetSysColor(COLOR_WINDOW),
        COLORREF lpHighlightColor = GetSysColor(COLOR_HIGHLIGHTTEXT),
        COLORREF lpHighlightBkColor = GetSysColor(COLOR_HIGHLIGHT),
        CFont *popTextFont = NULL,
        CFont *popHeaderFont = NULL
    );

    ~Table();

	CCSDragDropCtrl m_DragDropSource;
	CCSDragDropCtrl m_DragDropTarget;

    CCSReturnCode SetTableData(
        CWnd *popParentWindow,
        int ipXStart, int ipXEnd, int ipYStart, int ipYEnd,
        COLORREF lpTextColor = GetSysColor(COLOR_WINDOWTEXT),
        COLORREF lpTextBkColor = GetSysColor(COLOR_WINDOW),
        COLORREF lpHighlightColor = GetSysColor(COLOR_HIGHLIGHTTEXT),
        COLORREF lpHighlightBkColor = GetSysColor(COLOR_HIGHLIGHT),
        CFont *popTextFont = NULL,
        CFont *popHeaderFont = NULL
    );
    
	void CreateTableWindow(CRect opRect, CWnd *popParent);

	/*@ManDoc:
      Displays the table. It is necessary to call this method after you have changed window
      parameters, moved/resized the window, or updated data in the table. Except for
      SetPosition(..), AddTextLine(..), ChangeTextLine(..) and DeleteTextLine(..)other 
	  provided public methods should call this method when update table is finished.
      The DisplayTable(..) will create the window if it is not already created.
      This method returns {\bf RCSuccess} on success, or {\bf RCFailure} if it cannot
      create windows for a {\bf CTable} instance.
    */
    CCSReturnCode DisplayTable();

	/*@ManDoc:
      Sets the position based data members to the passed parameters if the {\bf CTable} 
	  window is currently displayed. Also updates the display. This method always returns
      {\bf RCSuccess}.
    */
    CCSReturnCode SetPosition(int ipXStart, int ipXEnd, int ipYStart, int ipYEnd);

    //@ManMemo: Returns the associated listbox of this table.
    CListBox *GetCTableListBox();

	//@ManMemo: Returns the line where the point (logical related to listbox) relates to.
	int GetLinenoFromPoint(CPoint opPoint);

	//@ManMemo: Performs column detection and returns current column.
	int GetColumnnoFromPoint(CPoint opPoint);

	/*@ManDoc:
      Fill the {\bf omFieldNames} array with the text data passed in {\bf opFieldNames}.
      {\bf opFieldNames} passes the text of the field names in a comma-separated list.
      The first item in this list will be copied to {\bf omFieldNames[0]}. These field names
      will be used in {\bf GetFieldValue(..)}. This method always returns {\bf RCSuccess}.
    */
    CCSReturnCode SetFieldNames(CString opFieldNames);

	/*@ManDoc:
      Fill the {\bf omHeaderFields} array with the text data passed in {\bf opHeaderFields}.
      {\bf opHeaderFields} passes the text of the table header fields in a vertbar-separated
      list. The first item in this list will be copied to {\bf omHeaderFields[0]}. This
      method always returns {\bf RCSuccess}.
    */
    CCSReturnCode SetHeaderFields(CString opHeaderFields);

	/*@ManDoc:
      Fill the {\bf omFormatList} array with the text data passed in {\bf opFormatList}.
      {\bf opFormatList} passes the text of the format descriptors in a vertbar-separated list.
      The first item in this list will be copied to {\bf omFieldNames[0]}. These format
      descriptors will be used to format each column entry.

	  Examples:
      "20,40,6,40"      -- indicates 4 fields, the third field is 6 characters long

      Formatting a character field is easy, but there is no way to specify decimal points
      in a numeric field. 
    */
    CCSReturnCode SetFormatList(CString opFormatList);

	CCSReturnCode SetTypeList(CString opTypeList);

    //@ManMemo: Remove items from the table content.
    CCSReturnCode ResetContent();

    //@ManMemo: Returns zero-based index of the item that has the focus rectangle.
    int GetCurrentLine();

	/*@ManDoc:
      Formats the passed {\bf opText} and add it to the internal {\bf omData} member.
      Update the display if necessary. The parameter "pvpData" is a pointer to the
      corresponding of the formatted {\bf opText}. This is required for passing the
      {\bf pvpData} back to the parent window when user press a double-click and generate
      a WM_TABLETEXTSELECT message. Returns {\bf RCSuccess} or {\bf RCFailure}.

      {\bf ATTENTION!}
      The {\bf opText} changed to be a vertical bar separated list in stead of comma list.
    */
    CCSReturnCode AddTextLine(CString opText, void *pvpData = NULL);

	/*@Doc:
      Formats the passed {\bf opText} and insert it to the internal {\bf omData} member.
      Unlike the {\bf AddTextLine} member function, {\bf InsertTextLine} will insert the
      given {\bf opText} to be at the line number given in {\bf ipLineNo}. If {\bf ipLineNo}
      parameter is -1, the string is added to the end of the list. Update the display if
      necessary. The parameter "pvpData" is a pointer to the corresponding of the formatted
      {\bf opText}. This is required for passing the {\bf pvpData} back to the parent window
      when user press a double-click and generate a WM_TABLETEXTSELECT message. Returns
      {\bf RCSuccess} or {\bf RCFailure}.

      {\bf ATTENTION!}
      The {\bf opText} changed to be a vertical bar separated list in stead of comma list.
    */
    CCSReturnCode InsertTextLine(int ipLineNo, CString opText, void *pvpData = NULL);

	/*@ManDoc:
      If {\bf ipLineNo} contains a valid line number, formats the passed {\bf opText} and
      replace the line in {\bf omData} member. ({\bf ipLineNo} -1 indicates current line.)
      The parameter "pvpData" is a pointer to the corresponding of the formatted
      {\bf opText}. This is required for passing the {\bf pvpData} back to the parent window
      when user press a double-click and generate a WM_TABLETEXTSELECT message.
      Update display if necessary. Returns {\bf RCSuccess} or {\bf RCFailure}.

      {\bf ATTENTION!}
      The {\bf opText} changed to be a vertical bar separated list in stead of comma list.
    */
    CCSReturnCode ChangeTextLine(int ipLineNo, CString opText, void *pvpData = NULL);

	/*@ManDoc:
      If {\bf ipLineNo} contains a valid line number, delete the line from {\bf omData}
      member. ({\bf ipLineNo} -1 indicates current line.) Update display if necessary.
      Returns {\bf RCSuccess} or {\bf RCFailure}.
    */
    CCSReturnCode DeleteTextLine(int ipLineNo);

	/*@ManDoc:
      If {\bf ipLineNo} contains a valid line number, change color of the line.
      ({\bf ipLineNo} -1 indicates current line.) Update display if necessary.
      Returns {\bf RCSuccess} or {\bf RCFailure}.
    */
    CCSReturnCode SetTextLineColor(int ipLineNo, COLORREF lpTextColor, COLORREF lpTextBkColor);

	/*@ManDoc:
      If {\bf ipLineNo} contains a valid line number, change the line separator of the
	  line. The separator may be TLS_NORMAL or TLS_THICK. ({\bf ipLineNo} -1 indicates
	  current line.) Update display if necessary. Returns {\bf RCSuccess} or {\bf RCFailure}.
    */
	CCSReturnCode SetTextLineSeparator(int ipLineNo, int ipSeparatorType);

	/*@ManDoc:
      If {\bf ipLineNo} contains a valid line number, change the font of the line.
	  Update display if necessary. Returns {\bf RCSuccess} or {\bf RCFailure}.
    */
	CCSReturnCode SetTextLineFont(int ipLineNo, CFont *popFont);

	/*@ManDoc:
      If {\bf ipLineNo} contains a valid line number, change the current drag status of
      the given line ({\bf ipLineNo} -1 indicates current line.) The user can Only the lines with
	  dragging enabled will be dragg
      Returns {\bf RCSuccess} or {\bf RCFailure}.
    */
    CCSReturnCode SetTextLineDragEnable(int ipLineNo, BOOL bpEnable);

	/*@ManDoc:
      If {\bf ipLineNo} contains a valid line number, get the text from the line in
      {\bf omData} member ({\bf ipLineNo} -1 indicates current line), copy it to {\bf opText}.
      Returns {\bf RCSuccess} or {\bf RCFailure}.

      {\bf ATTENTION!}
      The specification expected this function formats line back to internal format
      (trim characer fields, format date and time fields to Ceda format). However, this
      feature was removed since it's not make sense since the destination is CString.
      Moreover, CTable does not know anything about the data record structure. But the
      caller do.
    */
    CCSReturnCode GetTextLineValue(int ipLineNo, CString &opText);

	/*@ManDoc:
      If {\bf ipLineNo} contains a valid line number, return a void pointer which previously
      stored together with text line when AddTextLine(), InsertTextLine(), or ChangeTextLine().
      Otherwise, return NULL if there is no current line or window created yet.
    */
    void *GetTextLineData(int ipLineNo);

	//@ManMemo: returns current drag enable status of the specified text line
	BOOL GetTextLineDragEnable(int ipLineNo);

	/*@ManDoc:
      If {\bf ipLineNo} contains a valid line number and {\bf ipFieldNo} a valid field
      number, get the text of the requested field from the line in {\bf omData} member
      ({\bf ipLineNo} -1 indicates current line), copy it to {\bf opText}. Returns
      {\bf RCSuccess} or {\bf RCFailure}.
      
      {\bf ATTENTION!}
      The specification expected this function formats line back to internal format
      (trim characer fields, format date and time fields to Ceda format). However, this
      feature was removed since it's not make sense since the destination is CString.
      Moreover, CTable does not know anything about the data record structure. But the
      caller do.
    */
    CCSReturnCode GetTextFieldValue(int ipLineNo, int ipFieldNo, CString &opText);

	/*@ManDoc:
      If {\bf ipLineNo} contains a valid line number and {\bf opFieldName} a valid field
      name, get the text of the requested field (calculated field number from
      {\bf omFieldNames}) from the line in {\bf omData} member. Returns {\bf RCSuccess}
      or {\bf RCFailure}.
    */
    CCSReturnCode GetTextFieldValue(int ipLineNo, CString opFieldName, CString &opText);

	/*@ManDoc:
      If {\bf ipFieldNo} contains a valid field number, search through the table for the
      first line containing {\bf opText}. Returns line number (base-0) containing search
      string ({\bf opText}), or -1 if not found. On success, remember {\bf imNextLineToSearch}
      and the text {\bf opText} in {\bf omLastSeachText}. On further calls to
      SearchTextLine(..) with the same text in {\bf opText} start at this next line.
    */
    int SearchTextLine(int ipFieldNo, CString opText);

	void SetSelectMode(int ipSelectMode);
// Protected members
protected:
    // This variable indicates that the member function SetTableData(..) has already
    // been called or not. It will be set to FALSE on the default constructor, and changed
    // to TRUE on success SetTableData(..). The member function DisplayTable(..) cannot
    // create the window until "bmInited" is set to TRUE.
    //
    BOOL bmInited;

    // these protected data members are corresponding to parameters in SetTableData()
    CWnd *pomParentWindow;
    int imXStart, imXEnd;
    int imYStart, imYEnd;
    COLORREF lmTextColor;
    COLORREF lmTextBkColor;
    COLORREF lmHighlightColor;
    COLORREF lmHighlightBkColor;
    CFont *pomTextFont;
    CFont *pomHeaderFont;

    // These three variables will be changed by SetHeaderFields(), SetFieldNames(), and
    // SetFormatList(). Actually, we may implement to keep the comma-separated strings
    // which passed into these member functions directly. However, for sake of some
    // efficiency, they are split into a form of array, which we could access any elements
    // directly and more efficiently.
    //
    CStringArray omHeaderFields;    // array of column headers
    CStringArray omFieldNames;      // array of column (field) names
    CStringArray omFormatList;      // array of column format specifiers
	CStringArray omTypeList;		// array of column type specifiers (text or color)
    // This array represents the whole data of the table.
    // Each record is kept in the vertbar-separated format, and will be extracted on the fly.
    //
    // ATTENTION!
    // Type of this member changed from the speicification (CPtrArray) which was defined
    // as an array of structs, because just handling string is simpler, use less resource,
    // and the specification say something unclear about the raw data struct with unknown
    // format or details of content.
    //
    CStringArray omData;
    CPtrArray omDataPtr;		// associated data pointer for each text line
	CDWordArray omColor;		// associated color for each text line
	CDWordArray omBkColor;		// associated background color for each text line
	CWordArray omDragEnabled;	// associated DragEnable boolean flag for each text line
	CWordArray omSeparatorType;	// associated separator type for each text line
	CPtrArray omFontPtr;		// associated font for each text line

    // internal members used for searching routine -- SearchTextLine(..)
    int imNextLineToSearch;
    CString omLastSearchText;

    // private data for pixel calculation -- mostly used in CalculateWidgets()
    int imTextFontHeight;
    int imHeaderFontHeight;
    CDWordArray omLBorder;       // array of column's left-border X position
    CDWordArray omRBorder;       // array of column's right-border X position
    int imTableContentOffset;
    int imHorizontalExtent;     // list box width (in pixel)
    CRect omHeaderBarRect;

    void CalculateWidgets();

    // The scrollable list box represents the table content:
   // class CTableListBox;
    friend CTableListBox;       // needed since CTableListBox calls method DrawItem()
public:
	CTableListBox *pomListBox;
	int imItemHeight;

    // helper drawing object -- created in constructor, destroyed in destructor.
    CPen FacePen, ShadowPen, WhitePen, BlackPen;
    CBrush FaceBrush, SeparatorBrush;

    // Message handling:
    // CTable is implemented as an owner-drawn listbox within a child window.
    // These member functions will handle essential messages to allow CTable work correctly.
    //
    // Please understand that, following to OOP concept and MFC style, we should
    // implement MeasureItem() and DrawItem() in the owner-draw list box itself instead of
    // in a CTable which is its parent. However, since MeasureItem() and DrawItem() need
    // to access too many of CTable protected members, I moved to use OnMeasureItem() and
    // OnDrawItem() of CTable instead. On the other hand, if we want to follow OOP concept
    // and MFC style, we should have a new class, CTableContent derived from CListBox, for
    // the table content and handling its own MeasureItem() and DrawItem(). Then, CTable
    // will have a CTableContent as a member, and only draw its header bar.
    //
    //{{AFX_MSG(Table)
    afx_msg void OnPaint();
    afx_msg void OnMeasureItem(int nIDCtl, LPMEASUREITEMSTRUCT lpMeasureItemStruct);
    afx_msg void OnDrawItem(int NIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct);
    afx_msg LONG OnDragOver(UINT, LONG); 
    afx_msg LONG OnDrop(UINT, LONG); 
	//}}AFX_MSG
    DECLARE_MESSAGE_MAP()

    // helper functions for drawing header bar and table
    void DrawHeaderBar();
    void DrawItem(CDC &dc, UINT itemID, const CRect &rectClip,
        BOOL bpIsToggleFocusOnly, BOOL bpIsSelected, BOOL bpIsFocus);

public:
	int tempFlag;    
	int imCurrentColumn;
	int imSelectMode;
};

#endif // !defined(AFX_TABLE_H__F6245E63_0C5B_11D4_A713_00010204AA63__INCLUDED_)
