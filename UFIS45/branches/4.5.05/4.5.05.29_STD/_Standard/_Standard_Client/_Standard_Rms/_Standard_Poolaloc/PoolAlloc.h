// PoolAlloc.h : main header file for the POOLALLOC application
//

#if !defined(AFX_POOLALLOC_H__2703BE55_A894_11D3_A6A9_0000C007916B__INCLUDED_)
#define AFX_POOLALLOC_H__2703BE55_A894_11D3_A6A9_0000C007916B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif




#include <resource.h>		// main symbols
#include <CCSPtrArray.h>
#include <RecordSet.h>


class PoolAllocApp : public CWinApp
{
public:
	PoolAllocApp();
	~PoolAllocApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(PoolAllocApp)
	public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();
	virtual void WinHelp(DWORD dwData, UINT nCmd = HELP_CONTEXT);
	//}}AFX_VIRTUAL


// Implementation
	void SetGlobalFieldIndices();
	void GetData(CString opObject, CCSPtrArray <RecordSet> &ropArray, CString opOrderBy);
	void MakeChoiceLists();
	void FillTmpDataObjects();	

	//{{AFX_MSG(PoolAllocApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

extern PoolAllocApp theApp;
/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_POOLALLOC_H__2703BE55_A894_11D3_A6A9_0000C007916B__INCLUDED_)
