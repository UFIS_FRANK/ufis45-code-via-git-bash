// CCSObj.cpp: implementation of the CCSObj class.
//
//////////////////////////////////////////////////////////////////////

#include <stdafx.h>
#include <poolalloc.h>
#include <CCSObj.h>

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif



#include <ostream.h>


//-----------------------------------------------------------------------------------------------------------------
//					Construction/Destruction
//-----------------------------------------------------------------------------------------------------------------
CCSObj::CCSObj()
{

}

CCSObj::~CCSObj()
{

}



//-----------------------------------------------------------------------------------------------------------------
//					Implementation
//-----------------------------------------------------------------------------------------------------------------
void CCSObj::Dump(ostream& ropOs, const char *pcpLabel) const
{
  ropOs << pcpLabel << ".CCSObject::omRc = " << RC() << endl;
}
//-----------------------------------------------------------------------------------------------------------------

const CCSReturnCode& CCSObj::RC() const
{
  return omRc;
}
//-----------------------------------------------------------------------------------------------------------------

// set return code; simply say
//
//   RC(RCSuccess);
//   return something;

//  In order to let us use the statement "return RC(RCSuccess);", 
//  return value is changed from this method to be CCSReturnCode.
//
CCSReturnCode CCSObj::RC(const CCSReturnCode& ropRc)
{
  omRc = ropRc;
  
  return omRc;
}
//-----------------------------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------------------------
