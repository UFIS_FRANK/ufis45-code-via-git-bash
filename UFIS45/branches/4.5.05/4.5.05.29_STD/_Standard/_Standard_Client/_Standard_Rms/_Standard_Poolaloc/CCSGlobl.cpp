// CCSGlobl.cpp: implementation of the CCSGlobl .
//
//////////////////////////////////////////////////////////////////////

#include <stdafx.h>
#include <resource.h>
#include <CCSGlobl.h>
#include <CCSCedaData.h>
#include <CCSCedaCom.h>
#include <CCSDdx.h>
#include <CCSLog.h>
#include <CCSBcHandle.h>
#include <CedaBasicData.h>
#include <CCSBasic.h>
#include <BasicData.h>
// #include "CedaCfgData.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


//-------------------------------
//			global variables
//-------------------------------

//--- error handling
ofstream of_catch;

//--- field indices
// WGP (workgroups)
int igWgpPgpuIdx;
int igWgpPrflIdx;
int igWgpRemaIdx;
int igWgpUrnoIdx;
int igWgpWgpcIdx;
int igWgpWgpnIdx;
// SWG (?)
/*int igSwgCodeIdx;
int igSwgSurnIdx;
int igSwgUrnoIdx;
int igSwgVpfrIdx;
int igSwgVpfoIdx;
// SPF (service functions)
int igSpfFccoIdx;
int igSpfFcnoIdx;
int igSpfGtabIdx;
int igSpfPrioIdx;
int igSpfUpfcIdx;
int igSpfUrnoIdx;
int igSpfUsrvIdx;
// SPE (service permits)
int igSpeCodeIdx;
int igSpeSurnIdx;
int igSpeUrnoIdx;
int igSpeVpfrIdx;
int igSpeVpfoIdx;
// SOR (?)
int igSorCodeIdx;
int igSorSurnIdx;
int igSorUrnoIdx;
int igSorVpfrIdx;
int igSorVpfoIdx;*/
// SGR (Static Groups)
int igSgrApplIdx;
int igSgrFldnIdx;
int igSgrGrdsIdx;
int igSgrGrpnIdx;
int igSgrGrsnIdx;
int igSgrPrflIdx;
int igSgrTabnIdx;
int igSgrUrnoIdx;
int igSgrUgtyIdx;
// SGM (Group Members)
int igSgmPrflIdx;
int igSgmTabnIdx;
int igSgmUgtyIdx;
int igSgmUrnoIdx;
int igSgmUsgrIdx;
int igSgmUvalIdx;
int igSgmValuIdx;
// SDA (staff default allocation)
/*int igSdaVptoIdx;
int igSdaVpfrIdx;
int igSdaUrnoIdx;
int igSdaUpolIdx;
int igSdaSurnIdx;*/
// SCO (?)
/*int igScoCodeIdx;
int igScoSurnIdx;
int igScoUrnoIdx;
int igScoVpfrIdx;
int igScoVpfoIdx;*/
// POL (pools)
int igPolUrnoIdx;
int igPolPoolIdx;
int igPolNameIdx;
// POA (pool assignment)
int igPoaUvalIdx;
int igPoaUrnoIdx;
int igPoaUpolIdx;
int igPoaReftIdx;
// PGP (planning groups)
int igPgpMaxmIdx;
int igPgpMinmIdx;
int igPgpPgpcIdx;
int igPgpPgpmIdx;
int igPgpPgpnIdx;
int igPgpPrflIdx;
int igPgpRemaIdx;
int igPgpUrnoIdx;
// PFC (personnel functions)
int igPfcDptcIdx;
int igPfcFctcIdx;
int igPfcFctnIdx;
int igPfcPrflIdx;
int igPfcRemaIdx;
int igPfcUrnoIdx;
// PER (permits)
int igPerPrflIdx;
int igPerPrioIdx;
int igPerPrmcIdx;
int igPerPrmnIdx;
int igPerRemaIdx;
int igPerUrnoIdx;
// ORG (organisation unit)
int igOrgDpt1Idx;
int igOrgDpt2Idx;
int igOrgDptnIdx;
int igOrgPrflIdx;
int igOrgRemaIdx;
int igOrgUrnoIdx;
// COT (contract types)
int igCotCtrcIdx;
int igCotCtrnIdx;
int igCotDptcIdx;
int igCotMislIdx;
int igCotMxslIdx;
int igCotNowdIdx;
int igCotPrflIdx;
int igCotRemaIdx;
int igCotSbpaIdx;
int igCotUrnoIdx;
int igCotWhpwIdx;
int igCotWrkdIdx;
// ALO (allocation)
int igAloAlocIdx;
int igAloAlodIdx;
int igAloAlotIdx;
int igAloReftIdx;
int igAloUrnoIdx;



// index for array sort function
int igIdx;

//--- global array of ALO-referenced tables and fields
CCSPtrArray <REFTABLE> ogRefTables;

//--- global choice lists for comboboxes
CCSPtrArray <CHOICE_LIST> ogChoiceLists;

//--- global array of choicelist entries and corresponding urnos
CCSPtrArray <REFTAB> ogRef;

//--- array for given commandline parameters
CStringArray ogCmdLineArgsArray;

//--- general global variables
CString ogAppName = "POOLALLOC";	// This is the only string that is to be hardcoded here !!!
									// The Xs are only to recognize failures in SQLHDL.log
CString ogCallingApp = "";

char pcgUser[33] = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";
char pcgPasswd[33] = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";
char pcgConfigPath[142] = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";
char pcgHome[4] = "XXX";
char pcgTableExt[10] = "XXX";

//--- own classes' global objects
CCSLog			ogLog(NULL);
CCSDdx			ogDdx;
CCSCedaCom      ogCommHandler(&ogLog, NULL);  
CCSBcHandle     ogBcHandle(&ogDdx, &ogCommHandler, &ogLog);
CCSBasic		ogCCSBasic;		// lib
CBasicData		ogBasicData;	// local
// CedaCfgData		ogCfgData;
PrivList		ogPrivList;
DataSet			ogDataSet;
CedaBasicData	ogBCD(pcgHome, &ogDdx, &ogBcHandle, &ogCommHandler, &ogCCSBasic);

CGUILng* pogGUILng = CGUILng::TheOne();

CMapStringToString ogRestrictions;

//--- fixed fonts
/*CFont ogSmallFonts_Regular_6;
CFont ogSmallFonts_Regular_7;
CFont ogSmallFonts_Regular_8;
CFont ogSmallFonts_Bold_7;

CFont ogMSSansSerif_Regular_6;
CFont ogMSSansSerif_Regular_8;
CFont ogMSSansSerif_Regular_12;
CFont ogMSSansSerif_Regular_16;
CFont ogMSSansSerif_Bold_7;
CFont ogMSSansSerif_Bold_8;

CFont ogCourier_Regular_8;
CFont ogCourier_Regular_9;
CFont ogCourier_Regular_10;
CFont ogCourier_Bold_8;
CFont ogCourier_Bold_10;

CFont ogTimesNewRoman_9;
CFont ogTimesNewRoman_12;
CFont ogTimesNewRoman_16;
CFont ogTimesNewRoman_30;

CFont ogSetupFont;*/


//--- brush and color arrays
CBrush *ogBrushs[MAXCOLORS + 1];
COLORREF ogColors[MAXCOLORS + 1];

//--- points
CPoint ogMaxTrackSize = CPoint(1024, 768);
CPoint ogMinTrackSize = CPoint(1024 / 4, 768 / 4);

//--- anything else
bool bgUseResourceStrings = true;
bool bgDebug = false;



//----------------------------------------------------------------------------------------------------------------------
//					global functions
//----------------------------------------------------------------------------------------------------------------------
CString GetResString(UINT ID)
{
	CString olRet;
	olRet.LoadString(ID);
	return olRet;
}
//----------------------------------------------------------------------------------------------------------------------


CString GetString(UINT nID)
{
	/*CGUILng* */pogGUILng = CGUILng::TheOne();
	return pogGUILng->GetString(nID);
}
//----------------------------------------------------------------------------------------------------------------------

/*CString GetString(UINT ipIDS )
{
	
	//return GetResString ( ipIDS );
	CString olText="???", olUrno, olIDS;
	bool	blFound = false;
	if ( ogBCD.GetDataCount("TXT") > 0 )
	{
		olIDS.Format ("%ld", ipIDS );
		olUrno = ogBCD.GetFieldExt( "TXT", "STID", "APPL", olIDS, "RULE_AFT", "URNO" );
		if ( !olUrno.IsEmpty() )
			 blFound = ogBCD.GetField( "TXT", "URNO", olUrno, "STRG", olText ) ;
		if ( !blFound )
		{
			olUrno = ogBCD.GetFieldExt( "TXT", "STID", "APPL", olIDS, "GENERAL", "URNO" );
			if ( !olUrno.IsEmpty() )
				blFound = ogBCD.GetField( "TXT", "URNO", olUrno, "STRG", olText ) ;
		}
	}
	if ( !blFound && bgUseResourceStrings )
		olText = GetResString ( ipIDS );
	return olText;
}*/

//----------------------------------------------------------------------------------------------------------------------
bool GetString (CString opRefField, CString opIDS, CString &ropText)
{
	CString olText="???", olUrno;
	bool	blFound = false;
	
	if ( ogBCD.GetDataCount("TXT") > 0 )
	{
		olUrno = ogBCD.GetFieldExt( "TXT", opRefField, "APPL", opIDS, "RULE_AFT", "URNO" );
		if ( !olUrno.IsEmpty() )
			 blFound = ogBCD.GetField( "TXT", "URNO", olUrno, "STRG", olText ) ;
		if ( !blFound )
		{
			olUrno = ogBCD.GetFieldExt( "TXT", opRefField, "APPL", opIDS, "GENERAL", "URNO" );
			if ( !olUrno.IsEmpty() )
				blFound = ogBCD.GetField( "TXT", "URNO", olUrno, "STRG", olText ) ;
		}
	}
	ropText = olText;
	return blFound;
}
//----------------------------------------------------------------------------------------------------------------------

void CreateBrushes()
{
	 int ilLc;

	for( ilLc = 0; ilLc < FIRSTCONFLICTCOLOR; ilLc++)
	{
		ogColors[ilLc] = RED;
	}
	
	ogColors[RED_IDX] = RED;
	ogColors[LGREEN_IDX] = LGREEN;
	ogColors[GRAY_IDX] = GRAY;
	ogColors[GREEN_IDX] = GREEN;
	ogColors[BLUE_IDX] = BLUE;
	ogColors[SILVER_IDX] = SILVER;
	ogColors[MAROON_IDX] = MAROON;
	ogColors[OLIVE_IDX] = OLIVE;
	ogColors[NAVY_IDX] = NAVY;
	ogColors[PURPLE_IDX] = PURPLE;
	ogColors[TEAL_IDX] = TEAL;
	ogColors[LIME_IDX] = LIME;
	ogColors[YELLOW_IDX] = YELLOW;
	ogColors[FUCHSIA_IDX] = FUCHSIA;
	ogColors[AQUA_IDX] = AQUA;
	ogColors[WHITE_IDX] = WHITE;
	ogColors[BLACK_IDX] = BLACK;
	ogColors[ORANGE_IDX] = ORANGE;
	ogColors[PYELLOW_IDX] = PYELLOW;
	ogColors[PGREEN_IDX] = PGREEN;
	ogColors[PBLUE_IDX] = PBLUE;
	ogColors[ROSE_IDX] = ROSE;
	ogColors[DARKBLUE_IDX] = DARKBLUE;
	// don't use index 21 it's used for Break jobs already
	
	for(ilLc = 0; ilLc < MAXCOLORS; ilLc++)
	{
		ogBrushs[ilLc] = new CBrush();
		ogBrushs[ilLc]->CreateSolidBrush(ogColors[ilLc]);
	}
}
//----------------------------------------------------------------------------------------------------------------------

void DeleteBrushes()
{
	for( int ilLc = 0; ilLc < MAXCOLORS; ilLc++)
	{
		CBrush *olBrush = ogBrushs[ilLc];
		delete olBrush;
	}
}
//----------------------------------------------------------------------------------------------------------------------

bool SetDlgItemLangText ( CWnd *popWnd, UINT ilIDC, CString opRefField, 
						  CString opIDS )
{
	CString olText;
	bool	blFound;
	CWnd	*polItem;
	
	if ( !popWnd )
		return false;
	polItem = popWnd->GetDlgItem(ilIDC);
	if ( !polItem )
		return false;
	blFound = GetString ( opRefField, opIDS, olText);
	if ( blFound || !bgUseResourceStrings )
		polItem->SetWindowText(olText);
	return true;
}
//----------------------------------------------------------------------------------------------------------------------

bool SetWindowLangText ( CWnd *popWnd, UINT ipIDS )
{
	CString olText;
	
	if ( !popWnd )
		return false;
	olText = GetString ( ipIDS );
	popWnd->SetWindowText(olText);
	return true;
}

//----------------------------------------------------------------------------------------------------------------------

bool EnableDlgItem ( CWnd *popWnd, UINT ipIDC, BOOL bpEnable, BOOL bpShow )
{
	CWnd *polCtrl = 0;
	if ( popWnd )
		polCtrl = popWnd->GetDlgItem ( ipIDC );
	if ( !polCtrl )
		return false;
	polCtrl->EnableWindow ( bpEnable );
	polCtrl->ShowWindow ( bpShow ? SW_SHOW : SW_HIDE );
	return true;
}
//----------------------------------------------------------------------------------------------------------------------

BOOL MoveDlgItem ( CWnd *popWnd, UINT ipIDC, int x, int y )
{
	CWnd *polCtrl = 0;
	if ( popWnd )
		polCtrl = popWnd->GetDlgItem ( ipIDC );
	if ( !polCtrl )
		return FALSE;
	return polCtrl->SetWindowPos( 0, x, y, 0, 0, SWP_NOSIZE | SWP_NOZORDER  );
}
//----------------------------------------------------------------------------------------------------------------------

BOOL ResizeDlgItem ( CWnd *popWnd, UINT ipIDC, int cx, int cy )
{
	CWnd *polCtrl = 0;
	if ( popWnd )
		polCtrl = popWnd->GetDlgItem ( ipIDC );
	if ( !polCtrl )
		return FALSE;
	return polCtrl->SetWindowPos( 0, 0, 0, cx, cy, SWP_NOMOVE | SWP_NOZORDER  );
}
//----------------------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------