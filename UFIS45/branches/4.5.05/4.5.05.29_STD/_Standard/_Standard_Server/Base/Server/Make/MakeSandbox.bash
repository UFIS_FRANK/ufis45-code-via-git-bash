# $Id: Ufis/_Standard/_Standard_Server/Base/Server/Make/MakeSandbox.bash 1.6 2007/03/28 22:59:10SGT jim Exp  $
#
### 20020528 JIM:  fit directory structure, for-loop for .exclude
### 20020611 JIM:  .exclude: using $f instead of $FNAME for wildcards 
### 20020618 JIM:  automatic change to _Standard_Server
### 20020618 JIM:  no creation of child base directory, must be created
###                by master of projects
### 20020619 JIM:  create links only if $prj is not _Standard_Server
### 20020625 JIM:  added directory Mkdep
### 20020702 JIM:  added directory FDI
### 20020711 JIM:  added $M/filestat
### 20020712 JIM:  prepared for _Standard_Server46_ (or Beta or so)
### 20020715 JIM:  replaced LD by DS (LD may be used by OS already)
### 20021008 JIM:  first test look '.include' , when not found look for 
###                '.exclude' (.include and .exclude can not cascade)
### 20030219 JIM: default GREP_RET=1 , because for-loop may be empty
### 20050307 JIM: LibDependencies.mak in allen Directories checken
### 20051213 JIM: MKS forgets to make 'filestat' executable

PARENT=_Standard45_Server
case $SHELL in
   *bash)   ECHO="echo -e"
           ;;
   *ksh)    ECHO="echo "
           ;;
esac

if [ ! -d "${SB}" ] ; then
   echo
   echo "Sorry, aber anscheinend ist kein SB (SandboxXXX) gesetzt"
   echo
   return
   exit
fi

CHILD=$1
if [ "${CHILD}" != "" ] ; then
   CHILD=$1
   echo MakeSandbox: Parent: ${PARENT}, Child: ${CHILD}
else
   cat <<EOF

    MakeSandbox: anlegen der Datei-Links fuer eine neue Sandbox mit
    '${PARENT}' als Vorlage
    Aufruf:
       "MakeSandbox.bash <child-name>"
    kein Child-Name angegeben
    
EOF
   return
   exit
fi

old_prj=${prj} 
if [ "${PARENT}" != "${prj}" ] ; then
   # we are in a derivate of ${PARENT}, so set Project to ${PARENT}
   . /sandbox/common/SCMSetPrj ${PARENT}
fi

DIREXCLUDE="`dirname ${SB}/${CHILD}/Ufis/*/*/Base/Server`/Server"
DIR_SOURCE="`dirname ${SB}/${PARENT}/Ufis/*/*/Base/Server`/Server"
EXCLUDE="${DIREXCLUDE}/.include"
if [ ! -f "${EXCLUDE}" ] ; then
   EXCLUDE="${DIREXCLUDE}/.exclude"
fi
if [ ! -f "${EXCLUDE}" ] ; then
   echo
   echo "Sorry, aber ohne \"${DIREXCLUDE}/.exclude\" "
   echo "bzw. ohne        \"${DIREXCLUDE}/.include\" "
   echo "werden keine Links erzeugt. (Die .exclude kann leer sein,"
   echo "die .include kann einen \"*\" enthalten)"
   echo
   if [ -d "${DIREXCLUDE}" ] ; then
      echo
      echo "Ein \"${DIREXCLUDE}/.exclude\" wird angelegt."
      echo
      cp ${DIR_SOURCE}/.exclude ${DIREXCLUDE}/.exclude
   fi
   . /sandbox/common/SCMSetPrj ${old_prj}
   return 2>/dev/null
   exit
else
   eval `grep "CLONE_OF=" ${EXCLUDE}` 
fi

if [ ! -z "${CLONE_OF}" ] ; then
   if [ "${PARENT}" != "${CLONE_OF}" ] ; then
      if [ -d ${SB}/${CLONE_OF} ] ; then
         PARENT="${CLONE_OF}"
         echo `basename ${EXCLUDE}`: Setze PARENT auf ${PARENT} !!
         DIR_SOURCE="`dirname ${SB}/${PARENT}/Ufis/*/*/Base/Server`/Server"
      else   
         echo 'Achtung: Verzeichnis "'${CLONE_OF}'" existiert nicht (siehe'
         echo '         "CLONE_OF=" in ${EXCLUDE})'
         echo 'Achtung: benutze weiterhin "'${PARENT}'" um die Links zu erzeugen.'
      fi
   fi
fi

if [ "${PARENT}" = "${CHILD}" ] ; then
   echo
   echo "Sorry, aber es macht keinen Sinn, Links von ${PARENT} nach "
   echo "${CHILD} zu erzeugen. "
   echo "(_Standard_Server hat keine Links und kann man deshalb nicht resyncen)"
   echo
   . /sandbox/common/SCMSetPrj ${old_prj}
   return 2>/dev/null
   exit
fi

# ============================ Begin INCLUDE-Zweig ============================
if [ "${EXCLUDE}" = "${DIREXCLUDE}/.include" ] ; then

# checken, ob StandardServer-Datei auch im aktuellen Projekt benutzt wird
# und somit ein Link erzeugt werden soll.
# in $f ist der komplette Dateiname der StandardServer-Datei
# $SB zeigt auf das SandboxXXX-Verzeichnis des Users
function create_link
{
  case $f in
     */Bin_*)    # do nothing
     ;;
     */Lib_*)    # do nothing
     ;;
     */tmp_*)    # do nothing
     ;;
     */.depends) # do nothing
     ;;
     *.o|*.a)    # do nothing
     ;;
     *)          # else: 
       if [ -f $f ] ; then # es koennte ein Link oder ein Dir sein
          DNAME=`dirname $f`
          FNAME="`basename ${DNAME}`/`basename $f`"
          NEW_LINK="${DIREXCLUDE}/$f"
          NEW_DIR=`dirname "${NEW_LINK}"`
          if [ ! -d ${NEW_DIR} ] ; then
             echo
             echo ${NEW_DIR} existiert nicht, wird angelegt
             mkdir -p ${NEW_DIR}
          fi
          if [ ! -h "${NEW_LINK}" ] ; then
             if [ ! -f "${NEW_LINK}" ] ; then
                # echo "ln -s ${DIR_SOURCE}/${f} ${NEW_LINK}"
                ${ECHO} ".\c"
                ln -s "${DIR_SOURCE}/${f}" "${NEW_LINK}"
             else
                echo
                echo "DATEI (!) ${NEW_LINK} existiert bereits"
             fi
          else
             #echo
             #echo "link ${NEW_LINK} existiert bereits"
             ${ECHO} "-\c"
          fi
       fi
     ;;
  esac
}

cd ${DIR_SOURCE} 
function Expand_List_Of_Includes
{
LIST_OF_REGS=`grep -v "^#" "${EXCLUDE}" | grep -v "^$" | grep -v "CLONE_OF"`
for s in ${LIST_OF_REGS} Make/* Tools/Mkdep/mkdep.c ; do
   if [ -d $s ] ; then
      for f in `find $s -type f` ; do
          create_link
      done   
   else
      f=$s ; create_link
   fi
done
}
Expand_List_Of_Includes

echo Wechsle wieder nach ${DIR_SOURCE}
cd ${DIR_SOURCE} 

# Laenge des Sandbox-Pfades bis einschliesslich '.../Base/Server'
LEN_CH=`echo ${DIREXCLUDE} | wc -c`
echo checke Makefiles in `pwd`

# Makefiles in allen Directories checken
for d in `find ${DIREXCLUDE} -type d` ; do
   sub="`echo $d | cut -c${LEN_CH}-`"
   if [ -r ${d}/Makefile ] ; then
      echo Datei .${sub}/Makefile existiert 
   else
#      echo Datei .${sub}/Makefile existiert nicht
      # wir pruefen mit .${sub}/, den wenn ${sub} leer ist, 
      # wollen wir nicht in / pruefen 
      if [ -r .${sub}/Makefile ] ; then
         # falls ${sub} leer ist 
         echo ergaenze Link  .${sub}/Makefile 
         # und hier nehmen wir das fuehrende ./ wieder weg
         f="`echo .${sub}/Makefile | cut -c3-`" ; echo $f ; create_link
#      else   
#         echo und Datei .${sub}/Makefile auch existiert nicht
#         ls .${sub}/Makefile
      fi
   fi
done

# LibDependencies.mak (Library dependencies) in allen Directories checken
for d in `find ${DIREXCLUDE} -type d` ; do
   sub="`echo $d | cut -c${LEN_CH}-`"
   if [ -r ${d}/LibDependencies.mak ] ; then
      echo Datei .${sub}/LibDependencies.mak existiert 
   else
#      echo Datei .${sub}/LibDependencies.mak existiert nicht
      # wir pruefen mit .${sub}/, den wenn ${sub} leer ist, 
      # wollen wir nicht in / pruefen 
      if [ -r .${sub}/LibDependencies.mak ] ; then
         # falls ${sub} leer ist 
         echo ergaenze Link  .${sub}/LibDependencies.mak 
         # und hier nehmen wir das fuehrende ./ wieder weg
         f="`echo .${sub}/LibDependencies.mak | cut -c3-`" ; echo $f ; create_link
#      else   
#         echo und Datei .${sub}/LibDependencies.mak auch existiert nicht
#         ls .${sub}/LibDependencies.mak
      fi
   fi
done

# ============================ Ende  INCLUDE-Zweig ============================
else
# ============================ Begin EXCLUDE-Zweig ============================

# Laenge des Sandbox-Pfades bis einschliesslich '.../Base/Server'
LEN_SB=`echo ${PB} | wc -c`

# checken, ob StandardServer-Datei auch im aktuellen Projekt benutzt wird
# und somit ein Link erzeugt werden soll.
# in $f ist der komplette Dateiname der StandardServer-Datei
# $SB zeigt auf das SandboxXXX-Verzeichnis des Users
function create_link
{
    if [ -f $f ] ; then # '*.y' wird z.B. nicht ueberall expandiert
       DNAME=`dirname $f`
       FNAME="`basename ${DNAME}`/`basename $f`"
       echo $FNAME ${EXCLUDE}
       GREP_RET=1 # 20030219 JIM: default, because for-loop may be empty
       for RegExp in `grep -v "^#" "${EXCLUDE}" | grep -v "^$" | grep -v "CLONE_OF"` ; do
          # echo checking "${FNAME}" for "${RegExp}" 
          # echo "${FNAME}" | grep "${RegExp}" 2>&1 >/dev/null
	        echo "${f}" | grep "${RegExp}" 2>&1 #>/dev/null
          GREP_RET=$?
          if [ ${GREP_RET} = 0 ] ; then
             # echo hit "${FNAME}" for "${RegExp}" 
	           break
	        fi
       done
       if [ ${GREP_RET} = 1 ] ; then
          NEW_LINK="${DIREXCLUDE}`echo $f | cut -c${LEN_SB}-`"
          NEW_DIR=`dirname "${NEW_LINK}"`
          if [ ! -d ${NEW_DIR} ] ; then
             echo
          	 echo ${NEW_DIR} existiert nicht, wird angelegt
          	 mkdir -p ${NEW_DIR}
          fi
          if [ ! -h "${NEW_LINK}" ] ; then
             if [ ! -f "${NEW_LINK}" ] ; then
                # echo "ln -s ${f} ${NEW_LINK}" 
                ${ECHO} ".\c"
                ln -s "${f}" "${NEW_LINK}"
             else
                echo
                echo "DATEI (!) ${NEW_LINK} existiert bereits"
             fi
          else
             #echo
             # echo "link ${NEW_LINK} existiert bereits" 
             ${ECHO} "-\c"
          fi
       else
          echo
          echo ${FNAME} wird excluded
       fi
    fi
}

# 1. Makefile:
f="${PB}/Makefile" ; create_link


# fuer folgende Directories Links der Dateien erzeugen:
for d in $S $TS $TS/Mkdep $RS $RS/Scbhdl $FS $IS $LS $DS ${PB}/Make $I ; do
   # fuer folgende Datei-Filter Links Erzeugen:
   echo
   echo Erzeuge Links fuer Verzeichnis $d
   for FILTER in \*.c \*.pc \*.l \*.y \*.h  Make\* GlobalDefines \*.def ; do
      for f in $d/${FILTER} ; do
         create_link
      done
   done
done
f=${PB}/Make/filestat ; create_link

# 20020702: JIM: added directory FDI
# fuer folgende Directories Links der Dateien erzeugen:
for d in $S/FDI ; do
   # fuer folgende Datei-Filter Links Erzeugen:
   echo
   echo Erzeuge Links fuer Verzeichnis $d
   for FILTER in \*.src ; do
      for f in $d/${FILTER} ; do
         create_link
      done
   done
done

# fuer folgende Scbhdl Directories Links der Dateien erzeugen:
for d in `find $RS/Scbhdl/ -type d` ; do
   # fuer folgende Datei-Filter Links Erzeugen:
   echo
   echo Erzeuge Links fuer Verzeichnis $d
   for FILTER in \*.c \*.pc \*.l \*.y \*.h  Makefile\* \*.pl \*.def ; do
      for f in $d/${FILTER} ; do
         create_link
      done
   done
done

# fuer die Configuration Links der Dateien erzeugen:
for d in `find ${CF} -type d` ; do
   # fuer folgende Datei-Filter Links Erzeugen:
   echo
   echo Erzeuge Links fuer Verzeichnis $d
   for FILTER in \*.cfg \*.dat \*.down \*.tab \*.msg ; do
      for f in $d/${FILTER} ; do
         create_link
      done
   done
done

# fuer die Scripte Links der Dateien erzeugen:
for d in ${TS}/Scripts ; do
   # fuer folgende Datei-Filter Links Erzeugen:
   echo
   echo Erzeuge Links fuer Verzeichnis $d
   for FILTER in \* ; do
      for f in $d/${FILTER} ; do
         create_link
      done
   done
done

# ============================ Ende  EXCLUDE-Zweig ============================
fi


echo
echo Und weiterhin viele Ostereier...
if [ ! -z "${old_prj}" ] ; then
   echo
   echo Setzt Projekt wieder auf ${old_prj}
   . /sandbox/common/SCMSetPrj ${old_prj}
fi
# 20051213 JIM: MKS forgets to make executable:
if [ -f ${M}/filestat -a ! -x ${M}/filestat ] ; then
   chmod ug+x ${M}/filestat
fi
