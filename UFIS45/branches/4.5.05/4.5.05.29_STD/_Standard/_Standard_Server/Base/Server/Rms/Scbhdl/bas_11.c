/*****************************/
/* 11: free days in the year */
/*****************************/
void bas_11()
{
	char	clIsAushilfe[2];
	char	clStaffUrno[12];
	char	clSday[16];
	double	dlAccValue[12];
	double	dlNewAccValue[12];
	double	dlResult;
	char	clYear[6];
	int	i;

	dbg(DEBUG, "============================= ACCOUNT 11 START ===========================");

	/* checking temporary staff */
	if (ReadTemporaryField("IsAushilfe",clIsAushilfe) == 1 && clIsAushilfe[0] == '1')
	{
		dbg(TRACE,"Account 11: Stopped, because of temporary staff");
		return;
	}

	/* checking the data */
	if (strlen(pcgData) == NULL)
	{
		dbg(TRACE,"Account 11: Stopped, because <pcgData> = undef!");
		return;
	}
	dbg(DEBUG,"Account 11: pcgData = <%s>",pcgData);

	/* checking the fields */
	if (strlen(pcgFields) == 0)
	{
		dbg(TRACE,"Account 11: Stopped, because <pcgFields> = undef!");
		return;
	}
	dbg(DEBUG, "Account 11: pcgFields = <%s>",pcgFields);

	/* checking the URNO of the employee */
	if (ReadTemporaryField("StfUrno",clStaffUrno) == 0)
	{
		dbg(TRACE,"Account 11: Stopped, because <clStaffUrno> = undef!");
		return;
	}

	/* checking the SDAY */
	if (ReadTemporaryField("SDAY",clSday) == 0)
	{
		dbg(TRACE,"Account 11: Stopped, because <clSday> = undef!");
		return;
	}

	/* get ACC-record of account 5 */
	/* account 11 is the sum of the 12 month-close-values of account 5 */
	strncpy(clYear,clSday,4);
	clYear[4] = '\0';
	GetMultiAccCloseValueHandle(clStaffUrno,"5",clYear,dlAccValue);

	dlResult = 0;
	for (i = 0; i < 12; i++)
	{
		dlResult += dlAccValue[i];
	}

	dbg(DEBUG, "Account 11: Sum  <%lf>",dlResult);
	dbg(DEBUG, "Account 11: Year <%s>",clYear);

	/* save result */
	for (i = 0; i < 12; i++)
	{
		dlNewAccValue[i] = dlResult;
	}

	UpdateMultiAccountHandle("11",clStaffUrno,clYear,"SCBHDL",dlNewAccValue);
}
