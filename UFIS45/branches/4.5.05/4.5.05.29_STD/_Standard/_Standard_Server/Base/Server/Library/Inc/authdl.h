#ifndef _DEF_mks_version_authdl_h
  #define _DEF_mks_version_authdl_h
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_authdl_h[]   MARK_UNUSED = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Library/Inc/authdl.h 1.2 2004/07/27 16:46:43SGT jim Exp  $";
#endif /* _DEF_mks_version */
/* ********************************************************************** */
/*  The Master include file.						  */
/*									  */
/*  Program	  :     						  */
/*  Revision date :							  */
/*  Author    	  :							  */
/*  									  */
/*  NOTE : This should be the only include file for your program          */
/* ********************************************************************** */

#include "router.h"

#ifndef __AUTHDL_INC
#define __AUTHDL_INC


#define PASSWD_LEN	16

typedef struct {
	char	usid[33];
	char	pass[PASSWD_LEN];
	char	vpfr[11];
	char	vpto[10];
	char	stat[2];
	char	init[2];
} SECTAB;


#endif

