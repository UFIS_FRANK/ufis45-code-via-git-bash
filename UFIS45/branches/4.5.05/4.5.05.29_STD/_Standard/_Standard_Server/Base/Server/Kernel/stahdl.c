#ifndef _DEF_mks_version
  #define _DEF_mks_version
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version[] MARK_UNUSED = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Kernel/stahdl.c 1.16 2005/04/13 23:57:08SGT hag Exp  $";
#endif /* _DEF_mks_version */
/******************************************************************************/
/*                                                                            */
/* ABB ACE/FC Program Skeleton                                                */
/*                                                                            */
/* Author	      : HAG                                                       */
/* Date           : 06.11.2003                                                */
/* Description    : Status Manager server process according to CS_Stahdl.doc  */
/*                                                                            */
/* Update history :                                                           */
/*                                                                            */
/******************************************************************************/
/*                                                                            */
/* be carefule with strftime or similar functions !!!                         */
/*                                                                            */
/******************************************************************************/
/* This program is a MIKE main program */


#define U_MAIN
#define UGCCS_PRG
#define STH_USE
#include <stdio.h>
#include <malloc.h>
#include <string.h>
#include <errno.h>
#include <signal.h>
#include "ugccsma.h"
#include "msgno.h"
#include "glbdef.h"
#include "quedef.h"
#include "uevent.h"
#include "sthdef.h"
#include "debugrec.h"
#include "hsbsub.h"
#include "fditools.h"
#include "libccstr.h"
#include "l_ccstime.h"
#include "rmschkrul.h"
#include "syslib.h"
#include "db_if.h"
#include "AATArray.h"
#include <time.h>
#include <cedatime.h>
#include "chkval.h"       

#ifdef _HPUX_SOURCE
	extern int daylight;
	extern long timezone;
	extern char *tzname[2];
#else
	extern int _daylight;
	extern long _timezone;
	extern char *_tzname[2];
#endif

#ifndef min
#define min(a,b) (a < b ? a : b)
#endif
#ifndef max
#define max(a,b) (a > b ? a : b)
#endif

extern char  cgSelStr[];

#define ARR_NAME_LEN    12
#define ARR_FLDLST_LEN  256
#define URNOS_TO_FETCH 100
#define ARR_MAX_IDX    6
#define MAX_UAFTINBC	500

/* flags for SetArrayInfo */
#define CEDAARRAY_ALL		0xFF
#define CEDAARRAY_FILL		0x01
#define CEDAARRAY_URNOIDX	0x02
#define CEDAARRAY_TRIGGER	0x04

#define COND_COUNT    4
#define SRH_COND_SINGLE	"ACTU,ALTU,APTU,NATU"
#define SRH_COND_GROUP  "ACTG,ALTG,APTG,NATG"
#define AFT_INB_FIELD	"ACT3,ALC3,ORG3,TTYP"
#define AFT_OUTB_FIELD	"ACT3,ALC3,DES3,TTYP"
#define BASIC_DATA_TAB	"ACT,ALT,APT,NAT"
#define BASIC_DATA_FLD	"ACT3,ALC3,APC3,TTYP"

#define AFT_FIELDS "URNO,RKEY,ADID,HOPO,ALC2,ALC3,TTYP,ORG3,DES3,ACT3,ACT5,STOA,ETAI,LAND,ONBE,ONBL,TIFA,STOD,ETDI,OFBL,AIRB,TIFD,GA1X,GD1X,GA1Y,GD1Y,B1BA,B1EA,FLNO,TMOA,GA1B,GD1B,GA1E,GD1E,B1BS,B1ES,FLTO,FLTC,BOAO,FCAL"
#define JOB_FIELDS "ACFR,ACTO,DETY,HOPO,STAT,UAFT,UJTY,URNO,UTPL,PLFR,PLTO"
#define CCA_FIELDS "CKBA,CKBS,CKEA,CKES,FLNO,FLNU,STOD,URNO"
#define SRH_FIELDS "ACTG,ACTU,ADID,ALTG,ALTU,APTG,APTU,NAME,NATG,NATU,UHSS,URNO,DELF,MAXT"
#define SGM_FIELDS "URNO,USGR,UVAL,HOPO"
#define OST_FIELDS "CDAT,CONA,CONS,COTY,RFLD,RTAB,RURN,SDAY,UAFT,UHSS,URNO,USDE,USEC,USRH,CONU"
#define OST_ADDFIELDS "RETY,TIFR,LEVL,UTPL,AFTF,FLT2"
#define SDE_FIELDS "AFTF,CCAF,FLDR,NAME,RETY,RTIM,SDEU,TIFR,URNO,USRH,UTPL,DELF"
#define EVAL_FIELDS "RKEY,OURI,OURO,UHSS,SRHI,SRHO"
#define AF1_FIELDS "URNO,FCA1,FCA2,FLNU"

#define ACT_FIELDS "GA1X,GD1X,GA1Y,GD1Y,B1BA,B1EA,ACFR,ACTO,CKBA,CKEA"
#define NOM_FIELDS "GA1B,GD1B,GA1E,GD1E,B1BS,B1ES,PLFR,PLTO,CKBS,CKES"

#define AFT_INBOUND_FIELDS "TIFA,STOA,ETAI,TMOA,LAND,ONBE,ONBL,GA1X,GA1Y,B1BA,B1EA,GA1B,GA1E,B1BS,B1ES"

#define INBOUNDTIMES "TIFA,STOA,ETAI,TMOA,LAND,ONBE,ONBL"
#define IN_TIME_CNT 7
#define OUTBOUNDTIMES "TIFD,STOD,ETDI,OFBL,AIRB"
#define OUT_TIME_CNT 5
#define RC_DONT_CARE	-777

#define EVT_IN		1
#define EVT_OUT		2
#define EVT_TURN	3

int debug_level = 0;
/******************************************************************************/
/* Global variables                                                           */
/******************************************************************************/
static ITEM  *prgItem      = NULL;                /* The queue item pointer  */
static EVENT *prgEvent     = NULL;                /* The event pointer       */
static int   igItemLen     = 0;                   /* length of incoming item */
static EVENT *prgOutEvent  = NULL;

static char cgProcessName[20] = "\0";
static char  cgConfigFile[512] = "\0";
static char  cgHopo[8] = "\0";                         /* default home airport    */
static char  cgTabEnd[8] ="\0";                       /* default table extension */
static char  cgTwEnd [33];

static long lgEvtCnt = 0;
static int	igStartUpMode = TRACE;
static int	igRuntimeMode = 0;

static time_t tgLoadStart;
static time_t tgLoadEnd;
static time_t tgCurrTime;
static time_t tgWorkTime;

static char cgLoadStart[20];
static char cgLoadEnd[20];
static BOOL bgSingleBC = FALSE;
static BOOL bgSbcUaft = FALSE;
static BOOL bgSbcSday = FALSE;
static int  igMinUaftInSbc = 1;
static int  igMaxUaftInSbc = 100;
static char cgMinAftTime[20]="", cgMaxAftTime[20]="";
static char cgChangedRkeys[101]="";

struct _arrayinfo
{
	HANDLE   rrArrayHandle;
	char     crArrayName[ARR_NAME_LEN+1];
	char	   crTableName[ARR_NAME_LEN+1];
	char     crArrayFieldList[ARR_FLDLST_LEN+1];
	long     lrArrayFieldCnt;
	char    *pcrArrayRowBuf;
	long     lrArrayRowLen;
	long    *plrArrayFieldOfs;
	long    *plrArrayFieldLen;
	HANDLE   rrIdxHandle[ARR_MAX_IDX];
	char     crIdxName	[ARR_MAX_IDX][ARR_NAME_LEN+1];
	char     crIdxFields[ARR_MAX_IDX][ARR_FLDLST_LEN+1];
	long     lrIdxLength[ARR_MAX_IDX];
	/*
	HANDLE   rrIdx01Handle;
	char     crIdx01Name[ARR_NAME_LEN+1];
	char     crIdx01FieldList[ARR_FLDLST_LEN+1];
	long     lrIdx01FieldCnt;
	char    *pcrIdx01RowBuf;
	long     lrIdx01RowLen;
	HANDLE   rrIdxUrnoHandle;
	char     crIdxUrnoName[ARR_NAME_LEN+1];*/
};
typedef struct _arrayinfo ARRAYINFO;

ARRAYINFO rgAftArray;
ARRAYINFO rgAf1Array;
ARRAYINFO rgReqAftArray;
ARRAYINFO rgJobArray;
ARRAYINFO rgCcaArray;
ARRAYINFO rgSrhArray;
ARRAYINFO rgSdeArray;
ARRAYINFO rgSgmArray;
ARRAYINFO rgEvalResults;
ARRAYINFO rgOstArray;
ARRAYINFO rgNewOstArr;

ARRAYINFO rgBasicData[COND_COUNT];
int rgCndFldIdxSgl[COND_COUNT];
int rgCndFldIdxGrp[COND_COUNT];
int rgAftInbFldIdx[COND_COUNT];
int rgAftOutFldIdx[COND_COUNT];
int rgInbTimeIdx[IN_TIME_CNT];
int rgOutbTimeIdx[OUT_TIME_CNT ];

static int   igReservedUrnoCnt = 0;
static long  lgActUrno =0 ;
static char  cgCurrentTime[21]="" ;
static char  cgFlagActive = '1';
static char  cgTdi1[24]="";
static char  cgTdi2[24]="";
static char  cgTich[24]="";

static char  cgBcData[MAX_UAFTINBC*11] = "";
static char  cgBcField[5] = "";

#define AFTFIELD(pcpBuf,ipFieldNo) (&pcpBuf[rgAftArray.plrArrayFieldOfs[ipFieldNo-1]])
#define AF1FIELD(pcpBuf,ipFieldNo) (&pcpBuf[rgAf1Array.plrArrayFieldOfs[ipFieldNo-1]])
#define JOBFIELD(pcpBuf,ipFieldNo) (&pcpBuf[rgJobArray.plrArrayFieldOfs[ipFieldNo-1]])
#define SRHFIELD(pcpBuf,ipFieldNo) (&pcpBuf[rgSrhArray.plrArrayFieldOfs[ipFieldNo-1]])
#define SDEFIELD(pcpBuf,ipFieldNo) (&pcpBuf[rgSdeArray.plrArrayFieldOfs[ipFieldNo-1]])
#define OSTFIELD(pcpBuf,ipFieldNo) (&pcpBuf[rgOstArray.plrArrayFieldOfs[ipFieldNo-1]])
#define CCAFIELD(pcpBuf,ipFieldNo) (&pcpBuf[rgCcaArray.plrArrayFieldOfs[ipFieldNo-1]])
#define EVALFIELD(pcpBuf,ipFieldNo) (&pcpBuf[rgEvalResults.plrArrayFieldOfs[ipFieldNo-1]])

static int igSrhAdidIdx;
static int igSrhMaxtIdx;
static int igSrhNameIdx;
static int igSrhUhssIdx;
static int igSrhUrnoIdx;

static int igSdeAdidIdx;
static int igSdeAftfIdx;
static int igSdeFldrIdx;
static int igSdeLevlIdx;
static int igSdeRetyIdx;
static int igSdeSdeuIdx;
static int igSdeTifrIdx;
static int igSdeUrnoIdx;
static int igSdeUsrhIdx;
static int igSdeUtplIdx;

static int igOstAftfIdx;
static int igOstConaIdx;
static int igOstConsIdx;
static int igOstConuIdx;
static int igOstCotyIdx;
static int igOstFlt2Idx;
static int igOstRetyIdx;
static int igOstRfldIdx;
static int igOstRtabIdx;
static int igOstRurnIdx;
static int igOstSdayIdx;
static int igOstTifrIdx;
static int igOstUrnoIdx;
static int igOstUaftIdx;
static int igOstUsdeIdx;
static int igOstUhssIdx;
static int igOstUsrhIdx;
static int igOstUtplIdx;
static int igOstLevlIdx;

static int igAftUrnoIdx;
static int igAftAdidIdx;
static int igAftStoaIdx;
static int igAftStodIdx;
static int igAftFlnoIdx;
static int igAftRkeyIdx;
static int igAftTifaIdx;
static int igAftTifdIdx;

static int igAf1Fca1Idx;
static int igAf1Fca2Idx;
static int igAf1FlnuIdx;

static int igCcaCkbaIdx;
static int igCcaCkeaIdx;
static int igCcaFlnuIdx;
static int igCcaCkbsIdx;
static int igCcaCkesIdx;

static int igJobAcfrIdx;
static int igJobActoIdx;
static int igJobStatIdx;
static int igJobUaftIdx;
static int igJobUtplIdx;
static int igJobPlfrIdx;
static int igJobPltoIdx;

static int igEvalOuriIdx; 
static int igEvalOuroIdx;
static int igEvalSrhiIdx;
static int igEvalSrhoIdx;
static int igEvalUhssIdx;


/******************************************************************************/
/* Function prototypes	                                                      */
/******************************************************************************/
static int  Init_Stahdl();
static int ReadConfigEntry(char *pcpSection,char *pcpKeyword,
			   char *pcpCfgBuffer);


static int	Reset(void);                       /* Reset program          */
static void	Terminate(int ipSleep);            /* Terminate program      */
static void	HandleSignal(int);                 /* Handles signals        */
static void	HandleErr(int);                    /* Handles general errors */
static void   HandleQueErr(int);                 /* Handles queuing errors */
static void HandleQueues(void);                /* Waiting for Sts.-switch*/


static int	HandleData(EVENT *prpEvent);       /* Handles event data     */
static int GetRowLength(char *pcpTana, char *pcpFieldList, long *plpLen);
static int GetFieldLength(char *pcpTana, char *pcpFina, long *plpLen);
static int SetArrayInfo(char *pcpArrayName, char *pcpTableName, 
						char *pcpArrayFieldList,char *pcpAddFields,
						long *pcpAddFieldLens,char *pcpSelection, 
						ARRAYINFO *prpArrayInfo, short spFlags );
static int CreateIdx ( ARRAYINFO *prpArrayInfo, char *pcpIdxName,
					   char *pcpFields, char *pcpOrdering, int ipIdx );
static int GetTotalRowLength( char *pcpFieldList, ARRAYINFO *prpArray, long *plpLen);
static int SaveIndexInfo (ARRAYINFO *prpArrayInfo, int ipMode, int ipLogFile );
static int StrToTime(char *pcpTime,time_t *plpTime);
static int TimeToStr(char *pcpTime,time_t lpTime);
static int EvaluateRules ( ARRAYINFO* prpFlightArray, char *pcpUhss );
static int EvaluateFlight ( char *pcpUrnoList );
static int IniPriority ( char * pcpUrno );
static int InitFieldIndices();
static BOOL CheckSingleValue ( ARRAYINFO* prpBasicData, char *pcpReqUrno,
							   char * pcpActValue );
static BOOL CheckGroupValue ( ARRAYINFO* prpBasicData, char *pcpReqGrpUrno,
							   char * pcpActValue );
static int SetAATArrayInfo ( char *pcpArrayName,char *pcpArrayFieldList, 
						     long *plpFieldLens, ARRAYINFO *prpArrayInfo );
static int CreateSkeleton ( ARRAYINFO* prpFlightArray );
static int FillOstRecord (ARRAYINFO* prpFlightArray, char *pcpBuf, int ipBufLen,
						  char *pcpSdePtr, char *pcpOuri, char *pcpOuro, char *pcpUhss);
void TrimRight(char *s);
static int GetNextUrno(char *pcpUrno);
static int UpdateOSTTAB ( ARRAYINFO* prpFlightArray );
static int PrepareOSTArray ( char *pcpUaft, char *pcpUhss/*, BOOL bpSave*/ );
static int SetCONS ( ARRAYINFO *prpFlightArray, char *pcpUaft, char *pcpUhss, 
					 BOOL bpSave );
static int GetStatusTime ( char *pcpUost, char *pcpTime );
static int GetFlightTime ( ARRAYINFO *prpFlightArray, char *pcpUaft, 
						   char *pcpFld, char *pcpTime );
static int SendAnswer(EVENT *prpEvent,int ipRc);
static int ProcessCFS ( char *pcpAftSelection );
static int ProcessSCC( char *pcpUaft, char *pcpUhss );
static int SaveOstTab ();
static int GetConaFlight ( char *pcpUaft, char *pcpRfld, char* pcpCona );
static int GetConaJob ( char *pcpUaft, char *pcpUtpl, char* pcpCona, char *pcpRety,
					    char *pcpUaft2, char *pcpConaTurn );
static int GetConaCca ( char *pcpUaft, char* pcpCona, char *pcpRety );
static int GetFieldByUrno ( ARRAYINFO *prpArray, int ipFldIdx, char *pcpUrno, char *pcpBuf );
static int HandleCCAUpdate ( char *pcpUaft );
static int HandleJobUpdate ( char *pcpUaft, char *pcpUtpl );
static int IsAftChangeImportant ( char *pcpFields, char *pcpData, BOOL *pbpDoEval, 
								  BOOL  *pbpSetCons, BOOL  *pbpSetCona );
static int SetInitialCONA ( char *pcpUaft, char *pcpUhss );
static int UtcToLocal(char *pcpTime);
static BOOL IsRuleValid(char *pcpUrno, char *pcpUTCDate );
static int InitTichTdi();
static int UpdateMinMax ( char *pcpTime );
static int GetNomField ( char *pcpActField, char *pcpNomField, char *pcpRety );
static int GetConsJob ( char *pcpUaft, char *pcpUaft2, char *pcpUtpl, char* pcpCons, char *pcpRety );
static int GetConsCca ( char *pcpUaft, char* pcpCons, char *pcpRety );
static int UpdateStatus ( long lpOldRow, char *pcpOstOld, char *pcpOstNew );
static int Find2ndFlight ( ARRAYINFO *prpFlightArr, char *pcpAftRow1, char **pcpAftRow2 );
static BOOL CheckOneRule ( char *pcpRule, char *pcpInbound, char *pcpOutbound, long lpActGroundTime );
static int CreateSkeleton4Rule ( ARRAYINFO *prpFlights, char *pcpRule, 
								 char *pcpOuri, char *pcpOuro, char *pcpUhss );
static int PrepareSdeArray ( char *pcpUsrh );
static int GetFlt2Value ( ARRAYINFO *prpFlightArr, char *pcpUaft1, 
						  char *pcpUsrh, char *pcpUaft2 );
static int Get2ndFlightUrno ( ARRAYINFO *prpFlightArr, char *pcpUaft1, char *pcpUaft2 );
static int HandleAf1Update ( char *pcpFlnu );



BOOL IS_EMPTY (char *pcpBuf) 
{
	return	(!pcpBuf || *pcpBuf==' ' || *pcpBuf=='\0') ? TRUE : FALSE;
}

/*	get actual date in UTC, if test date is configured, 
	act. date = day(test date) + time (now)	*/
static void GetActDate ( char *pcpNow )
{
	GetServerTimeStamp("UTC", 1, 0, pcpNow );
	if ( cgCurrentTime[0] )		/* if test date filled copy day of test date */
		strncpy ( pcpNow, cgCurrentTime, 8 );

}


/******************************************************************************/
/*                                                                            */
/* The MAIN program                                                           */
/*                                                                            */
/******************************************************************************/
MAIN
{
	int	ilRc = RC_SUCCESS;			/* Return code			*/
	int	ilCnt = 0;
	int ilOldDebugLevel = 0;


	INITIALIZE;			/* General initialization	*/



	debug_level = TRACE;

	strcpy(cgProcessName,argv[0]);

	dbg(TRACE,"MAIN: version <%s>",mks_version);

	/* Attach to the MIKE queues */
	do
	{
		ilRc = init_que();
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"MAIN: init_que() failed! waiting 6 sec ...");
			sleep(6);
			ilCnt++;
		}/* end of if */
	}while((ilCnt < 10) && (ilRc != RC_SUCCESS));

	if(ilRc != RC_SUCCESS)
	{
		dbg(TRACE,"MAIN: init_que() failed! waiting 60 sec ...");
		sleep(60);
		exit(1);
	}else{
		dbg(TRACE,"MAIN: init_que() OK! mod_id <%d>",mod_id);
	}/* end of if */

	do
	{
		ilRc = init_db();
		if (ilRc != RC_SUCCESS)
		{
			check_ret(ilRc);
			dbg(TRACE,"MAIN: init_db() failed! waiting 6 sec ...");
			sleep(6);
			ilCnt++;
		} /* end of if */
	} while((ilCnt < 10) && (ilRc != RC_SUCCESS));

	if(ilRc != RC_SUCCESS)
	{
		dbg(TRACE,"MAIN: init_db() failed! waiting 60 sec ...");
		sleep(60);
		exit(2);
	}else{
		dbg(TRACE,"MAIN: init_db() OK!");
	} /* end of if */

	/* logon to DB is ok, but do NOT use DB while ctrl_sta == HSB_COMING_UP !!! */

	sprintf(cgConfigFile,"%s/%s",getenv("BIN_PATH"),mod_name);
	dbg(TRACE,"ConfigFile <%s>",cgConfigFile);
	ilRc = TransferFile(cgConfigFile);
	if(ilRc != RC_SUCCESS)
	{
		dbg(TRACE,"MAIN: TransferFile(%s) failed!",cgConfigFile);
	} /* end of if */

	sprintf(cgConfigFile,"%s/%s.cfg",getenv("CFG_PATH"),mod_name);
	dbg(TRACE,"ConfigFile <%s>",cgConfigFile);
	ilRc = TransferFile(cgConfigFile);
	if(ilRc != RC_SUCCESS)
	{ 
		dbg(TRACE,"MAIN: TransferFile(%s) failed!",cgConfigFile);
	} /* end of if */

	ilRc = SendRemoteShutdown(mod_id);
	if(ilRc != RC_SUCCESS)
	{
		dbg(TRACE,"MAIN: SendRemoteShutdown(%d) failed!",mod_id);
	} /* end of if */

	if((ctrl_sta != HSB_STANDALONE) && (ctrl_sta != HSB_ACTIVE) && (ctrl_sta != HSB_ACT_TO_SBY))
	{
		dbg(DEBUG,"MAIN: waiting for status switch ...");
		HandleQueues();
	}/* end of if */

	if((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE) || (ctrl_sta == HSB_ACT_TO_SBY))
	{
		dbg(TRACE,"MAIN: initializing ...");
			ilRc = Init_Stahdl(); 
			if(ilRc != RC_SUCCESS)
			{
				dbg(TRACE,"Init_Stahdl: init failed!");
			} /* end of if */

	} else {
		Terminate(30);
	}/* end of if */



	dbg(TRACE,"=====================");
	dbg(TRACE,"MAIN: initializing OK");
	dbg(TRACE,"=====================");

	debug_level = igRuntimeMode;


	for(;;)
	{
		ilRc = que(QUE_GETBIG,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);
		/* depending on the size of the received item  */
		/* a realloc could be made by the que function */
		/* so do never forget to set event pointer !!! */
		prgEvent = (EVENT *) prgItem->text;
				
		if( ilRc == RC_SUCCESS )
		{
			/* Acknowledge the item */
			ilRc = que(QUE_ACK,0,mod_id,0,0,NULL);
			if( ilRc != RC_SUCCESS ) 
			{
				/* handle que_ack error */
				HandleQueErr(ilRc);
			} /* fi */

			
			lgEvtCnt++;

			switch( prgEvent->command )
			{
				case	HSB_STANDBY	:
					ctrl_sta = prgEvent->command;
					HandleQueues();
					break;	
				case	HSB_COMING_UP	:
					ctrl_sta = prgEvent->command;
					HandleQueues();
					break;	
				case	HSB_ACTIVE	:
					ctrl_sta = prgEvent->command;
					break;	
				case	HSB_ACT_TO_SBY	:
					ctrl_sta = prgEvent->command;
					/* CloseConnection(); */
					HandleQueues();
					break;	
				case	HSB_DOWN	:
					/* whole system shutdown - do not further use que(), send_message() or timsch() ! */
					ctrl_sta = prgEvent->command;
					Terminate(1);
					break;	
				case	HSB_STANDALONE	:
					ctrl_sta = prgEvent->command;
					ResetDBCounter();
					break;	
				case	REMOTE_DB :
					/* ctrl_sta is checked inside */
					HandleRemoteDB(prgEvent);
					break;
				case	SHUTDOWN	:
					/* process shutdown - maybe from uutil */
					Terminate(1);
					break;
						
				case	RESET		:
					ilRc = Reset();
					break;
						
				case	EVENT_DATA	:
					if((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE) || (ctrl_sta == HSB_ACT_TO_SBY))
					{
						ilRc = HandleData(prgEvent);
						/*
						if(ilRc != RC_SUCCESS)
						{
							HandleErr(ilRc);
						}*/
					}
					else
					{
						dbg(TRACE,"MAIN: wrong hsb status <%d>",ctrl_sta);
						DebugPrintItem(TRACE,prgItem);
						DebugPrintEvent(TRACE,prgEvent);
					}/* end of if */
					break;
					

				case	TRACE_ON :
					dbg_handle_debug(prgEvent->command);
					break;
				case	TRACE_OFF :
					dbg_handle_debug(prgEvent->command);
					break;
				case 	100:
					ilRc = RC_SUCCESS;
					for ( ilCnt=0; ilCnt<COND_COUNT; ilCnt++ )
						if ( rgBasicData[ilCnt].rrArrayHandle >= 0 )
							ilRc |= SaveIndexInfo ( &(rgBasicData[ilCnt]), 0, 0 );
					break;
				case	101:
					ilRc = SaveIndexInfo ( &rgAftArray, 0, 0 );
					ilRc = SaveIndexInfo ( &rgAf1Array, 0, 0 );
					break;
				case	102:
					ilRc = SaveIndexInfo ( &rgJobArray, 1, 0 );
					break;
				case	103:
					ilRc = SaveIndexInfo ( &rgSgmArray, 1, 0 );
					break;
				case	104:
					ilRc = SaveIndexInfo ( &rgSrhArray, 1, 0 );
					break;
				case	105:
					ilRc = SaveIndexInfo ( &rgSdeArray, 1, 0 );
					break;
				case	106:
					ilRc = SaveIndexInfo ( &rgEvalResults, 1, 0 );
					break;
				case	107:
					ilRc = SaveIndexInfo ( &rgOstArray, 1, 0 );
					break;
				case	108:
					ilRc = SaveIndexInfo ( &rgNewOstArr, 1, 0 );
					break;
				default			:
					dbg(TRACE,"MAIN: unknown event");
					DebugPrintItem(TRACE,prgItem);
					DebugPrintEvent(TRACE,prgEvent);
					break;
			} /* end switch */
		} 
		else 
		{
			/* Handle queuing errors */
			HandleQueErr(ilRc);
		} /* end else */
		
	} /* end for */
	
} /* end of MAIN */

/******************************************************************************/
/* The initialization routine                                                 */
/******************************************************************************/



static int ReadConfigEntry(char *pcpSection,char *pcpKeyword, char *pcpCfgBuffer)
{
    int ilRc = RC_SUCCESS;

    char clSection[124] = "\0";
    char clKeyword[124] = "\0";

    strcpy(clSection,pcpSection);
    strcpy(clKeyword,pcpKeyword);

    ilRc = iGetConfigEntry(cgConfigFile,clSection,clKeyword,
			   CFG_STRING,pcpCfgBuffer);
    if(ilRc != RC_SUCCESS)
    {
	dbg(TRACE,"Not found in %s: <%s> <%s>",cgConfigFile,clSection,clKeyword);
    } 
    else
    {
	dbg(DEBUG,"Config Entry <%s>,<%s>:<%s> found in %s",
	    clSection, clKeyword ,pcpCfgBuffer, cgConfigFile);
    }/* end of if */
    return ilRc;
}


static int Init_Stahdl()
{
	int  ilRc = RC_SUCCESS;			/* Return code */
	char clSection[64] = "\0";
	char clKeyword[64] = "\0";
    long pllAddFieldLens[10];
	char clFields[256] = "\0";
	char pclSqlBuf[2560] = "\0";
	char clSelection[1024] = "\0";
	char clValue[124],clCode[41];
	char clStartDay[9],clEndDay[9];
	char clBasicTables[101]="";
 	short slCursor = 0;
	short slSqlFunc = 0;
	time_t tlCurrTime;
	int	  ilLoadOffSetStart = -3600 * 12;
	int	  ilLoadOffSetEnd = 3600 * 24;
	int  ilOffSet = 0;
    int  ilCnt = 0, ilDummy;

	if(ilRc == RC_SUCCESS)
	{
		/* read HomeAirPort from SGS.TAB */
		ilRc = tool_search_exco_data("SYS", "HOMEAP", cgHopo);
		if (ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"Init_Stahdl: EXTAB,SYS,HOMEAP not found in SGS.TAB");
			Terminate(30);
		} else {
			dbg(TRACE,"Init_Stahdl: home airport <%s>",cgHopo);
		}
		InitTichTdi();
	}

	
	if(ilRc == RC_SUCCESS)
	{
		ilRc = tool_search_exco_data("ALL","TABEND", cgTabEnd);
		if (ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"Init_Stahdl: EXTAB,ALL,TABEND not found in SGS.TAB");
			Terminate(30);
		} else {
			dbg(TRACE,"Init_Stahdl: table extension <%s>",cgTabEnd);
		}
	}

	sprintf (cgTwEnd, "%s,%s,%s", cgHopo,cgTabEnd, cgProcessName);

	if(ilRc == RC_SUCCESS)
	{
		 GetDebugLevel("STARTUP_MODE", &igStartUpMode);
		 GetDebugLevel("RUNTIME_MODE", &igRuntimeMode);
	}
	debug_level = igStartUpMode;

	sprintf(clSection,"MAIN");
	sprintf(clKeyword,"CURRENT_TIME");
	if(ReadConfigEntry(clSection,clKeyword,clValue) != RC_SUCCESS)
	{
		GetServerTimeStamp("UTC", 1, 0, clValue);
	}
	else
	{
		strncpy ( cgCurrentTime, clValue, 14 );
		cgCurrentTime[14] = '\0';
	}
    StrToTime(clValue,&tlCurrTime);

	sprintf(clKeyword,"LOAD_OFFSET_START");
	if(ReadConfigEntry(clSection,clKeyword,clValue) == RC_SUCCESS)
	{
	    ilOffSet = atoi(clValue);
	    ilLoadOffSetStart = (int) ilOffSet*60*60;
	}

	sprintf(clKeyword,"LOAD_OFFSET_END");
	if(ReadConfigEntry(clSection,clKeyword,clValue) == RC_SUCCESS)
	{
	    ilOffSet = atoi(clValue);
	    ilLoadOffSetEnd = (int) ilOffSet*60*60;
	}
	tgLoadStart = tlCurrTime + ilLoadOffSetStart;
	tgLoadEnd = tlCurrTime + ilLoadOffSetEnd;
	TimeToStr(cgLoadStart,tgLoadStart);
	TimeToStr(cgLoadEnd,tgLoadEnd);

	/*  BC_TYPE possible values: SINGLE, SBCUAFT, SBCSDAY */
	sprintf(clKeyword,"BC_TYPE");
	if(ReadConfigEntry(clSection,clKeyword,clValue) == RC_SUCCESS)
	{
		if ( strstr ( clValue, "SINGLE" ) )
			bgSingleBC = TRUE;
		if ( strstr ( clValue, "SBCUAFT" ) )
			bgSbcUaft = TRUE;
		if ( strstr ( clValue, "SBCSDAY" ) )
			bgSbcSday = TRUE;
	}
	else
		bgSingleBC = bgSbcUaft = bgSbcSday = TRUE;
	dbg ( TRACE, "Init_Stahdl: BC-Types Single <%d> SBCUAFT <%d> SBCSDAY <%d>", 
		  bgSingleBC, bgSbcUaft, bgSbcSday );
    
	sprintf(clKeyword,"MAX_UAFTINBC");
	if(ReadConfigEntry(clSection,clKeyword,clValue) == RC_SUCCESS)
	{
		if ( ( sscanf ( clValue, "%d", &ilDummy ) > 0 ) && ( ilDummy > 0 ) )
			igMaxUaftInSbc = min( ilDummy, MAX_UAFTINBC ); 
	}
	sprintf(clKeyword,"MIN_UAFTINBC");
	if(ReadConfigEntry(clSection,clKeyword,clValue) == RC_SUCCESS)
	{
		if ( ( sscanf ( clValue, "%d", &ilDummy ) > 0 )  && ( ilDummy <= igMaxUaftInSbc ) )
			igMinUaftInSbc = ilDummy; 
	}
	dbg ( TRACE, "Init_Stahdl: MIN_UAFTINBC <%d> MAX_UAFTINBC <%d>", 
		  igMinUaftInSbc, igMaxUaftInSbc );

	dbg ( DEBUG, "Init_Stahdl: Going to Create Arrays ilRc <%d>", ilRc );
    if(ilRc == RC_SUCCESS)
    {
		ilRc = CEDAArrayInitialize(20,10);
		if(ilRc != RC_SUCCESS)
		    dbg(TRACE,"Init_Stahdl: CEDAArrayInitialize failed <%d>",ilRc);
    }/* end of if */

	/*  Initialise Basic Data Tables from BASIC_DATA_TAB */
	if(ilRc == RC_SUCCESS)
	{
		for ( ilCnt=0; (ilRc == RC_SUCCESS) && (ilCnt<COND_COUNT); ilCnt++ )
		{
			strcpy ( clFields, "URNO," );
			if ( get_real_item(clCode,BASIC_DATA_FLD,ilCnt+1 ) > 0 )
				strcat ( clFields, clCode);
			else
			{
				dbg ( TRACE, "Init_Stahdl: Unable to extract Basic Data Field no. <%d>", ilCnt+1 );
				continue;
			}
			if ( get_real_item(clValue,BASIC_DATA_TAB,ilCnt+1 ) <= 0 )
			{
				dbg ( TRACE, "Init_Stahdl: Unable to extract Basic Data Table no. <%d>", ilCnt+1 );
				continue;
			}			
			dbg ( DEBUG, "Init_Stahdl: SetArrayInfo Table <%s> Fields <%s>", 
				  clValue, clFields );
			ilRc = SetArrayInfo( clValue, clValue, clFields,0, 0, "",
								 &(rgBasicData[ilCnt]), CEDAARRAY_ALL );
			if ( ilRc != RC_SUCCESS )
			{
				dbg ( TRACE, "Init_Stahdl: SetArrayInfo Table <%s> Fields <%s> failed Rc <%d>", 
					  clValue, clFields, ilRc );
			}
			else
			{
				ilRc = CreateIdx ( &(rgBasicData[ilCnt]), "IDXCODE", clCode, "A", 1 );
				if (ilRc!=RC_SUCCESS)
					dbg(TRACE,"Init_discal: CreateIdx for <%s> failed <%d>",clValue, ilRc);		

				if ( clBasicTables[0] ) 
					strcat ( clBasicTables, "," );
				sprintf ( &(clBasicTables[strlen(clBasicTables)]), "'%s'", clValue );
			}
		}
	}
	/*  Initialise Flights AFTTAB */
	if(ilRc == RC_SUCCESS)
	{
		sprintf(clSelection,"WHERE (((TIFA BETWEEN '%s' AND '%s') AND DES3 = '%s') OR ((TIFD BETWEEN '%s' AND '%s') AND ORG3 = '%s'))",
				cgLoadStart,cgLoadEnd,cgHopo,cgLoadStart,cgLoadEnd,cgHopo);

		dbg ( DEBUG, "Init_Stahdl: Selection for CEDAArrayFill AFTTAB: <%s>", clSelection );
		ilRc = SetArrayInfo( "AFT", "AFT", AFT_FIELDS, 0, 0, clSelection, 
							 &rgAftArray, CEDAARRAY_ALL );
		if ( ilRc != RC_SUCCESS )
		{
			dbg ( TRACE, "Init_Stahdl: SetArrayInfo <AFT> failed Rc <%d>", ilRc );
		}
		sprintf(clSelection,
				"(((TIFA >= %s)  && (TIFA <= %s)  && (DES3 == %s)) || ((TIFD >= %s)  && (TIFD <= %s)  && (ORG3 == %s))) ",
				cgLoadStart,cgLoadEnd,cgHopo,cgLoadStart,cgLoadEnd,cgHopo);
		dbg(TRACE,"Init_Stahdl: Filter for AFTTAB <%s>", clSelection );
		ilRc = CEDAArraySetFilter(&rgAftArray.rrArrayHandle,
								  rgAftArray.crArrayName,clSelection );
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"Init_Stahdl: CEDAArraySetFilter AFTTAB failed <%d>",ilRc);
		}
	}
	if(ilRc == RC_SUCCESS)
	{
		ilRc = CreateIdx ( &rgAftArray, "IDXAFT1", "RKEY,ADID", "A,A", 1 );
		if (ilRc!=RC_SUCCESS)
			dbg(TRACE,"Init_discal: CreateIdx for AFT failed <%d>",ilRc);		
	}
	/*  Initialise Array for Flights to be evaluated */
	if(ilRc == RC_SUCCESS)
	{
		ilRc = SetArrayInfo( "REQAFT", "AFT", AFT_FIELDS, 0, 0, "", 
								&rgReqAftArray, CEDAARRAY_URNOIDX  );
		if ( ilRc != RC_SUCCESS )
		{
			dbg ( TRACE, "Init_Stahdl: SetArrayInfo <REQAFT> failed Rc <%d>", ilRc );
		}
	}
	if(ilRc == RC_SUCCESS)
	{
		ilRc = CreateIdx ( &rgReqAftArray, "IDXREQAFT1", "RKEY,ADID", "A,A", 1 );
		if (ilRc!=RC_SUCCESS)
			dbg(TRACE,"Init_discal: CreateIdx for REQAFT failed <%d>",ilRc);		
	}

	/*  AF1TAB, extension of AFTTAB */
	if(ilRc == RC_SUCCESS)
	{
		sprintf(clSelection,"WHERE flnu in (select urno from afttab where (TIFD BETWEEN '%s' AND '%s') AND ADID!='A' AND ORG3 = '%s')",
				cgLoadStart,cgLoadEnd,cgHopo);

		dbg ( DEBUG, "Init_Stahdl: Selection for CEDAArrayFill AF1TAB: <%s>", clSelection );

		ilRc = SetArrayInfo( "AF1", "AF1", AF1_FIELDS, 0, 0, clSelection, &rgAf1Array, CEDAARRAY_ALL );
		if ( ilRc != RC_SUCCESS )
		{
			dbg ( TRACE, "Init_Stahdl: SetArrayInfo <AF1> failed Rc <%d>", ilRc );
		}
		if(ilRc == RC_SUCCESS)
		{
			ilRc = CreateIdx ( &rgAf1Array, "IDXAF1FLNU", "FLNU", "A", 1 );
			if (ilRc!=RC_SUCCESS)
				dbg(TRACE,"Init_discal: CreateIdx for IDXAF1FLNU failed <%d>",ilRc);		
		}
		ilRc = RC_SUCCESS;		/* process shall also work without AF1TAB */
	}

	/*  Initialise Jobs on Flights JOBTAB */
	if(ilRc == RC_SUCCESS)
	{
		sprintf(clSelection,"WHERE UJTY = '2000' AND ACTO > '%s' AND ACFR < '%s' AND HOPO = '%s'",
			   cgLoadStart,cgLoadEnd,cgHopo); 
		dbg ( DEBUG, "Init_Stahdl: Selection for CEDAArrayFill JOBTAB: <%s>", clSelection );

		ilRc = SetArrayInfo( "JOB", "JOB", JOB_FIELDS, 0, 0, clSelection,
							 &rgJobArray, CEDAARRAY_ALL );
		if ( ilRc != RC_SUCCESS )
		{
			dbg ( TRACE, "Init_Stahdl: SetArrayInfo <JOB> failed Rc <%d>", ilRc );
		}
	}
	if(ilRc == RC_SUCCESS)
	{
		ilRc = CreateIdx ( &rgJobArray, "IDXJOB1", "UAFT,UTPL", "A,A", 1 );
		if (ilRc!=RC_SUCCESS)
			dbg(TRACE,"Init_discal: CreateIdx for JOB failed <%d>",ilRc);		
	}
	/*  Initialise Counter Allocations CCATAB */
	if(ilRc == RC_SUCCESS)
	{
		sprintf(clSelection,"WHERE CKES > '%s' AND CKBS < '%s' AND HOPO = '%s'",
			   cgLoadStart,cgLoadEnd,cgHopo); 
		dbg ( DEBUG, "Init_Stahdl: Selection for CEDAArrayFill CCATAB: <%s>", clSelection );

		ilRc = SetArrayInfo( "CCA", "CCA", CCA_FIELDS, 0, 0, clSelection,
							 &rgCcaArray, CEDAARRAY_ALL );
		if ( ilRc != RC_SUCCESS )
		{
			dbg ( TRACE, "Init_Stahdl: SetArrayInfo <CCA> failed Rc <%d>", ilRc );
		}
	}
	if(ilRc == RC_SUCCESS)
	{
		ilRc = CreateIdx ( &rgCcaArray, "IDXCCA1", "FLNU,CKBA", "A,A", 1 );
		if (ilRc!=RC_SUCCESS)
			dbg(TRACE,"Init_discal: CreateIdx for CCA failed <%d>",ilRc);		
	}
	if(ilRc == RC_SUCCESS)
	{
		ilRc = CreateIdx ( &rgCcaArray, "IDXCCA2", "FLNU,CKEA", "A,D", 2 );
		if (ilRc!=RC_SUCCESS)
			dbg(TRACE,"Init_discal: CreateIdx for CCA failed <%d>",ilRc);		
	}
	/*  Initialise Static Group Members SGMTAB */
	if(ilRc == RC_SUCCESS)
	{
		sprintf(clSelection,"WHERE TABN IN (%s) AND HOPO = '%s'",
			    clBasicTables,cgHopo); 
		dbg ( DEBUG, "Init_Stahdl: Selection for CEDAArrayFill SGMTAB: <%s>", clSelection );

		ilRc = SetArrayInfo( "SGM", "SGM", SGM_FIELDS, 0, 0, clSelection,
							 &rgSgmArray, CEDAARRAY_FILL | CEDAARRAY_TRIGGER );
		if ( ilRc != RC_SUCCESS )
		{
			dbg ( TRACE, "Init_Stahdl: SetArrayInfo <SGM> failed Rc <%d>", ilRc );
		}
	}
	if(ilRc == RC_SUCCESS)
	{
		ilRc = CreateIdx ( &rgSgmArray, "IDXSGM1", "USGR,UVAL", "A,A", 1 );
		if (ilRc!=RC_SUCCESS)
			dbg(TRACE,"Init_discal: CreateIdx for SGM failed <%d>",ilRc);		
	}

	/*  Initialise Validity Module in dblib */
   	if (ilRc == RC_SUCCESS)
	{
		strcpy ( cgSelStr, "WHERE appl = 'STATMGR'" );
  		ilRc = InitValidityModule (CHKVAL_ARRAY, "VALTAB") ;
  		if (ilRc != RC_SUCCESS)
  		{
   			dbg (TRACE,"Init_Stahdl: InitValidityModule failed <%d>",ilRc) ;
   			ilRc = RC_SUCCESS ;
  		}
  		else
  		{
   			dbg (DEBUG,"Init_Stahdl: InitValidityModule OK") ;  

    			ilRc = ConfigureAction ("VALTAB", 0, 0 ) ;
    			if (ilRc != RC_SUCCESS)
     				dbg(TRACE,"Init_Stahdl: ConfigureAction failed <%d>", ilRc) ;
 		} /* end of if */
 	} /* end of if */
	/*  Initialise Status Rule Headers SRHTAB */
	if(ilRc == RC_SUCCESS)
	{
		pllAddFieldLens[0] = COND_COUNT;
		pllAddFieldLens[1] = 0;

		ilRc = SetArrayInfo( "SRH", "SRH", SRH_FIELDS, "PRIO", pllAddFieldLens, 
						"", &rgSrhArray, CEDAARRAY_ALL );
		if ( ilRc != RC_SUCCESS )
		{
			dbg ( TRACE, "Init_Stahdl: SetArrayInfo <SRH> failed Rc <%d>", ilRc );
		}
	}
	InitFieldIndices();
	IniPriority ( "" );

	if(ilRc == RC_SUCCESS)
	{
		ilRc = CreateIdx ( &rgSrhArray, "IDXSRH1", "DELF,UHSS,ADID,PRIO,MAXT", "A,A,D,D,A", 1 );
		if (ilRc!=RC_SUCCESS)
			dbg(TRACE,"Init_Stahdl: CreateIdx for SRH failed <%d>",ilRc);		
	}
	/*  Initialise Status Definitions SDETAB */
	if(ilRc == RC_SUCCESS)
	{
		pllAddFieldLens[0] = 1;		/* ADID */
		pllAddFieldLens[1] = 2;		/* LEVL */
		pllAddFieldLens[2] = 0;
		ilRc = SetArrayInfo( "SDE", "SDE", SDE_FIELDS, "ADID,LEVL", 
							 pllAddFieldLens, "", &rgSdeArray, CEDAARRAY_ALL );
		if ( ilRc != RC_SUCCESS )
		{
			dbg ( TRACE, "Init_Stahdl: SetArrayInfo <SDE> failed Rc <%d>", ilRc );
		}
	}
	if(ilRc == RC_SUCCESS)
	{
		ilRc = CreateIdx ( &rgSdeArray, "IDXSDE1", "DELF,USRH,URNO", "A,A,A", 1 );
		if (ilRc!=RC_SUCCESS)
			dbg(TRACE,"Init_Stahdl: CreateIdx for SDE failed <%d>",ilRc);		
	}
	PrepareSdeArray ( "" );

	/*  Initialise logical Array for rule evaluation results */
	if(ilRc == RC_SUCCESS)
	{
		pllAddFieldLens[0] = pllAddFieldLens[1] = pllAddFieldLens[2] = 10;
		pllAddFieldLens[3] = pllAddFieldLens[4] = pllAddFieldLens[5] = 10;
		pllAddFieldLens[6] = 0;
		/* EVAL_FIELDS "RETY,OURI,OURO,UHSS,SRHI,SRHO" */
		ilRc = SetAATArrayInfo ( "RESULTS", EVAL_FIELDS, pllAddFieldLens,
						         &rgEvalResults );
		if (ilRc!=RC_SUCCESS)
			dbg(TRACE,"Init_Stahdl: SetAATArrayInfo for RESULTS failed <%d>",ilRc);		
	}
	if(ilRc == RC_SUCCESS)
	{
		ilRc = CreateIdx ( &rgEvalResults, "IDXRKEY", "RKEY", "A", 1 );
		if (ilRc!=RC_SUCCESS)
			dbg(TRACE,"Init_Stahdl: CreateIdx for RESULTS failed <%d>",ilRc);		
	}

	/*  Initialise Array for existing Online Status OSTTAB */
	if(ilRc == RC_SUCCESS)
	{
		strncpy ( clStartDay, cgLoadStart, 8 );
		strncpy ( clEndDay, cgLoadEnd, 8 );
		clStartDay[8] = clEndDay[8] = '\0';

		/* OST_ADDFIELDS: RETY,TIFR,LEVL,UTPL,AFTF,FLT2 */
		if ( GetFieldLength("SDETAB","RETY",&(pllAddFieldLens[0]))!=RC_SUCCESS )
			pllAddFieldLens[0] = 10;
		pllAddFieldLens[1] = 7;		/* TIFR */
		pllAddFieldLens[2] = 2;		/* LEVL */
		if ( GetFieldLength("SDETAB","UTPL",&(pllAddFieldLens[3]))!=RC_SUCCESS )
			pllAddFieldLens[3] = 10;
		if ( GetFieldLength("SDETAB","AFTF",&(pllAddFieldLens[4]))!=RC_SUCCESS )
			pllAddFieldLens[4] = 4;
		pllAddFieldLens[5] = 10;
		pllAddFieldLens[6] = 0;
		
		sprintf(clSelection,"WHERE SDAY BETWEEN '%s' AND '%s'", clStartDay,clEndDay );
		dbg ( DEBUG, "Init_Stahdl: Selection for CEDAArrayFill OSTTAB: <%s>", clSelection );
		ilRc = SetArrayInfo( "OST", "OST", OST_FIELDS, OST_ADDFIELDS, pllAddFieldLens, 
							 clSelection, &rgOstArray, CEDAARRAY_ALL );
		if ( ilRc != RC_SUCCESS )
		{
			dbg ( TRACE, "Init_Stahdl: SetArrayInfo <OST> failed Rc <%d>", ilRc );
		}
		sprintf(clSelection, "(SDAY >= %s)  && (SDAY <= %s) ", clStartDay,clEndDay );
		dbg(TRACE,"Init_Stahdl: Filter for OSTTAB <%s>", clSelection );
		ilRc = CEDAArraySetFilter(&rgOstArray.rrArrayHandle,
								  rgOstArray.crArrayName,clSelection );
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"Init_Stahdl: CEDAArraySetFilter OSTTAB failed <%d>",ilRc);
		}
	}
	if(ilRc == RC_SUCCESS)
	{
		ilRc = CreateIdx ( &rgOstArray, "IDXOST1", "UAFT,UHSS,LEVL", "A,A,A", 1 );
		if (ilRc!=RC_SUCCESS)
			dbg(TRACE,"Init_Stahdl: CreateIdx for OST failed <%d>",ilRc);		
	}
	if(ilRc == RC_SUCCESS)
	{
		ilRc = CreateIdx ( &rgOstArray, "IDXOST2", "UAFT,UHSS,USDE", "A,A,A", 2 );
		if (ilRc!=RC_SUCCESS)
			dbg(TRACE,"Init_Stahdl: CreateIdx for OST failed <%d>",ilRc);		
	}
	if(ilRc == RC_SUCCESS)
	{
		ilRc = CreateIdx ( &rgOstArray, "IDXOST3", "RURN,UHSS", "A,A", 3 );
		if (ilRc!=RC_SUCCESS)
			dbg(TRACE,"Init_Stahdl: CreateIdx for OST failed <%d>",ilRc);		
	}
	if(ilRc == RC_SUCCESS)
	{
		ilRc = CreateIdx ( &rgOstArray, "IDXOST4", "FLT2", "A", 4 );
		if (ilRc!=RC_SUCCESS)
			dbg(TRACE,"Init_Stahdl: CreateIdx for OST failed <%d>",ilRc);		
	}
	
	if( ( ilRc == RC_SUCCESS) && 
		(ReadConfigEntry(clSection,"SEND_TO_ACTION",clValue) == RC_SUCCESS) )
	{
		if ( strstr ( clValue, "YES" ) )
			CEDAArraySendChanges2ACTION(&rgOstArray.rrArrayHandle,
   										rgOstArray.crArrayName, TRUE);
	}

	if(ilRc == RC_SUCCESS)
	{
		ilRc = SetArrayInfo( "NEWOST", "OST", OST_FIELDS, OST_ADDFIELDS, 
							 pllAddFieldLens, "", &rgNewOstArr, 0 );
		if ( ilRc != RC_SUCCESS )
		{
			dbg ( TRACE, "Init_Stahdl: SetArrayInfo <NEWOST> failed Rc <%d>", ilRc );
		}
	}
	if(ilRc == RC_SUCCESS)
	{
		ilRc = CreateIdx ( &rgNewOstArr, "NEWOST1", "UAFT,USDE", "A,A", 1 );
		if (ilRc!=RC_SUCCESS)
			dbg(TRACE,"Init_Stahdl: CreateIdx for NEWOST failed <%d>",ilRc);		
	}

	PrepareOSTArray (0, 0 );

	if ( ilRc == RC_SUCCESS ) 
	{
		sprintf(clKeyword,"EVAL_AT_STARTUP");
		if( (ReadConfigEntry(clSection,clKeyword,clValue) == RC_SUCCESS) &&
			 !strcmp(clValue,"YES") )
		{
			ilRc = EvaluateRules ( &rgAftArray, 0 );
			if ( ilRc != RC_SUCCESS )
				dbg ( TRACE, "Init_Stahdl: EvaluateRules failed RC <%d>", ilRc );
			else
			{
				ilRc = CreateSkeleton ( &rgAftArray );
				if ( ilRc != RC_SUCCESS )
					dbg ( TRACE, "Init_Stahdl: CreateSkeleton failed RC <%d>", ilRc );
			}
			if ( ilRc == RC_SUCCESS )
			{
				ilRc = UpdateOSTTAB ( &rgAftArray );
				if ( ilRc != RC_SUCCESS )
					dbg ( TRACE, "Init_Stahdl: UpdateOSTTAB failed RC <%d>", ilRc );
				PrepareSBC ( 0, &rgAftArray );
			}
		}
	}
	SaveOstTab ();

	if (ilRc != RC_SUCCESS)
	{
	    dbg(TRACE,"Init_Stahdl failed");
	}

	return(ilRc);
	
} /* end of initialize */




/******************************************************************************/
/* The Reset routine                                                          */
/******************************************************************************/
static int Reset()
{
	int	ilRc = RC_SUCCESS;				/* Return code */
	
	dbg(TRACE,"Reset: now resetting");
	
	return ilRc;
	
} /* end of Reset */

/******************************************************************************/
/* The termination routine                                                    */
/******************************************************************************/
static void Terminate(int ipSleep)
{
	/* unset SIGCHLD ! DB-Child will terminate ! */

	dbg(TRACE,"Terminate: now DB logoff ...");

	signal(SIGCHLD,SIG_IGN);

	logoff();

	dbg(TRACE,"Terminate: now sleep(%d) ...",ipSleep);


	dbg(TRACE,"Terminate: now leaving ...");

	fclose(outp);

	sleep(ipSleep < 1 ? 1 : ipSleep);
	
	exit(0);
	
} /* end of Terminate */

/******************************************************************************/
/* The handle signals routine                                                 */
/******************************************************************************/

static void HandleSignal(int pipSig)
{
	dbg(TRACE,"HandleSignal: signal <%d> received",pipSig);

	switch(pipSig)
	{
	default	:
		Terminate(1);
		break;
	} /* end of switch */

	exit(1);
	
} /* end of HandleSignal */

/******************************************************************************/
/* The handle general error routine                                           */
/******************************************************************************/
static void HandleErr(int pipErr)
{
	dbg(TRACE,"HandleErr: <%d> : unknown ERROR",pipErr);

	return;
} /* end of HandleErr */

/******************************************************************************/
/* The handle queuing error routine                                           */
/******************************************************************************/
static void HandleQueErr(int pipErr)
{
	switch(pipErr) {
	case	QUE_E_FUNC	:	/* Unknown function */
		dbg(TRACE,"<%d> : unknown function",pipErr);
		break;
	case	QUE_E_MEMORY	:	/* Malloc reports no memory */
		dbg(TRACE,"<%d> : malloc failed",pipErr);
		break;
	case	QUE_E_SEND	:	/* Error using msgsnd */
		dbg(TRACE,"<%d> : msgsnd failed",pipErr);
		break;
	case	QUE_E_GET	:	/* Error using msgrcv */
		dbg(TRACE,"<%d> : msgrcv failed",pipErr);
		break;
	case	QUE_E_EXISTS	:
		dbg(TRACE,"<%d> : route/queue already exists ",pipErr);
		break;
	case	QUE_E_NOFIND	:
		dbg(TRACE,"<%d> : route not found ",pipErr);
		break;
	case	QUE_E_ACKUNEX	:
		dbg(TRACE,"<%d> : unexpected ack received ",pipErr);
		break;
	case	QUE_E_STATUS	:
		dbg(TRACE,"<%d> :  unknown queue status ",pipErr);
		break;
	case	QUE_E_INACTIVE	:
		dbg(TRACE,"<%d> : queue is inaktive ",pipErr);
		break;
	case	QUE_E_MISACK	:
		dbg(TRACE,"<%d> : missing ack ",pipErr);
		break;
	case	QUE_E_NOQUEUES	:
		dbg(TRACE,"<%d> : queue does not exist",pipErr);
		break;
	case	QUE_E_RESP	:	/* No response on CREATE */
		dbg(TRACE,"<%d> : no response on create",pipErr);
		break;
	case	QUE_E_FULL	:
		dbg(TRACE,"<%d> : too many route destinations",pipErr);
		break;
	case	QUE_E_NOMSG	:	/* No message on queue */
		dbg(TRACE,"<%d> : no messages on queue",pipErr);
		break;
	case	QUE_E_INVORG	:	/* Mod id by que call is 0 */
		dbg(TRACE,"<%d> : invalid originator=0",pipErr);
		break;
	case	QUE_E_NOINIT	:	/* Queues is not initialized*/
		dbg(TRACE,"<%d> : queues are not initialized",pipErr);
		break;
	case	QUE_E_ITOBIG	:
		dbg(TRACE,"<%d> : requestet itemsize to big ",pipErr);
		break;
	case	QUE_E_BUFSIZ	:
		dbg(TRACE,"<%d> : receive buffer to small ",pipErr);
		break;
	default			:	/* Unknown queue error */
		dbg(TRACE,"<%d> : unknown error",pipErr);
		break;
	} /* end switch */
         
	return;

} /* end of HandleQueErr */

/******************************************************************************/
/* The handle queues routine                                                  */
/******************************************************************************/
static void HandleQueues()
{
	int	ilRc = RC_SUCCESS;			/* Return code */
	int	ilBreakOut = FALSE;
	
	do
	{
		ilRc = que(QUE_GETBIG,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);
		/* depending on the size of the received item  */
		/* a realloc could be made by the que function */
		/* so do never forget to set event pointer !!! */
		prgEvent = (EVENT *) prgItem->text;	

		if( ilRc == RC_SUCCESS )
		{
			/* Acknowledge the item */
			ilRc = que(QUE_ACK,0,mod_id,0,0,NULL);
			if( ilRc != RC_SUCCESS ) 
			{
				/* handle que_ack error */
				HandleQueErr(ilRc);
			} /* fi */
		
			switch( prgEvent->command )
			{
			case	HSB_STANDBY	:
				ctrl_sta = prgEvent->command;
				break;	
	
			case	HSB_COMING_UP	:
				ctrl_sta = prgEvent->command;
				break;	
	
			case	HSB_ACTIVE	:
				ctrl_sta = prgEvent->command;
				ilBreakOut = TRUE;
				break;	

			case	HSB_ACT_TO_SBY	:
				ctrl_sta = prgEvent->command;
				break;	
	
			case	HSB_DOWN	:
				/* whole system shutdown - do not further use que(), send_message() or timsch() ! */
				ctrl_sta = prgEvent->command;
				Terminate(1);
				break;	
	
			case	HSB_STANDALONE	:
				ctrl_sta = prgEvent->command;
				ResetDBCounter();
				ilBreakOut = TRUE;
				break;	

			case	REMOTE_DB :
				/* ctrl_sta is checked inside */
				HandleRemoteDB(prgEvent);
				break;

			case	SHUTDOWN	:
				Terminate(1);
				break;
						
			case	RESET		:
				ilRc = Reset();
				break;
						
			case	EVENT_DATA	:
				dbg(TRACE,"HandleQueues: wrong hsb status <%d>",ctrl_sta);
				DebugPrintItem(TRACE,prgItem);
				DebugPrintEvent(TRACE,prgEvent);
				break;
					
			case	TRACE_ON :
				dbg_handle_debug(prgEvent->command);
				break;

			case	TRACE_OFF :
				dbg_handle_debug(prgEvent->command);
				break;

			default			:
				dbg(TRACE,"HandleQueues: unknown event");
				DebugPrintItem(TRACE,prgItem);
				DebugPrintEvent(TRACE,prgEvent);
				break;
			} /* end switch */
		} else {
			/* Handle queuing errors */
			HandleQueErr(ilRc);
		} /* end else */
	} while (ilBreakOut == FALSE);

			ilRc = Init_Stahdl();
			if(ilRc != RC_SUCCESS)
			{
				dbg(TRACE,"InitDemhdl: init failed!");
			} 
     

} /* end of HandleQueues */
	



/******************************************************************************/
/* The handle data routine                                                    */
/******************************************************************************/
static int HandleData(EVENT *prpEvent)
{
	int	   ilRc           = RC_SUCCESS;			/* Return code */
	int      ilRc1          = 0;

	BC_HEAD *prlBchead       = NULL;
	CMDBLK  *prlCmdblk       = NULL;
	char    *pclSelection    = NULL;
	char    *pclFields       = NULL;
	char    *pclData         = NULL;
	char    *pclRow          = NULL;
	char 	clUrnoList[2400];
	char 	clTable[34];
	char 	clUaft[21]="", clUtpl[21]="";
	char clDestName[21] = "\0";
	char clRecvName[21] = "\0";
	char *pclDel=0;
	BOOL bpDoEval = FALSE;
	BOOL bpSetCons = FALSE;
	BOOL bpSetCona = FALSE;

	prlBchead    = (BC_HEAD *) ((char *)prpEvent + sizeof(EVENT));
	prlCmdblk    = (CMDBLK *)  ((char *)prlBchead->data);
	pclSelection = prlCmdblk->data;
	pclFields    = pclSelection + strlen(pclSelection) + 1;
	pclData      = pclFields + strlen(pclFields) + 1;

	strcpy(clTable,prlCmdblk->obj_name);
    strcpy(clDestName,prlBchead->dest_name);
	strcpy(clRecvName,prlBchead->recv_name);

	dbg(TRACE,"========== START <%10.10d> ==========",lgEvtCnt);

	/****************************************/
	DebugPrintBchead(TRACE,prlBchead);
	DebugPrintCmdblk(TRACE,prlCmdblk);
	dbg(TRACE,"Command: <%s>",prlCmdblk->command);
	dbg(TRACE,"originator follows event = %p ",prpEvent);

	dbg(TRACE,"originator<%d>",prpEvent->originator);
	dbg(TRACE,"selection follows Selection = %p ",pclSelection);
	dbg(TRACE,"selection <%s>",pclSelection);
	dbg(TRACE,"fields    <%s>",pclFields);
	dbg(TRACE,"data      <%s>",pclData);
	/****************************************/

	if ( pclDel = strchr ( pclData, '\n' ) )
		*pclDel = '\0';

	cgBcField[0] = cgBcData[0] = '\0';
	cgMinAftTime[0] = cgMaxAftTime[0] = '\0';
	cgChangedRkeys[0] = '\0';

	if ( !strcmp(prlCmdblk->command,"IRT") || !strcmp(prlCmdblk->command,"URT") )
	{
		ilRc = CEDAArrayEventUpdate2(prgEvent,clUrnoList,clTable);
		ilRc1 = RC_DONT_CARE;
		if ( !strncmp ( clTable, "CCA", 3 ) )
		{	
			ilRc1 = GetFieldByUrno ( &rgCcaArray, igCcaFlnuIdx, clUrnoList, clUaft );
			if ( ilRc1 == RC_SUCCESS )
				ilRc1 = HandleCCAUpdate ( clUaft );
		}
		else if ( !strncmp ( clTable, "JOB", 3 ) )
		{	
			ilRc1 = GetFieldByUrno ( &rgJobArray, igJobUaftIdx, clUrnoList, clUaft );
			ilRc1 |= GetFieldByUrno ( &rgJobArray, igJobUtplIdx, clUrnoList, clUtpl );
			if ( ilRc1 == RC_SUCCESS )
			{
				TrimRight ( clUtpl );
				ilRc1 = HandleJobUpdate ( clUaft, clUtpl );
			}
		}
		else if ( !strncmp ( clTable, "SRH", 3 ) )
		{	
			ilRc = IniPriority ( clUrnoList );
		}
		else if ( !strncmp ( clTable, "SDE", 3 ) )
		{	
			char *pclRow;
			long llRow=ARR_FIRST ;
			if ( CEDAArrayFindRowPointer(&(rgSdeArray.rrArrayHandle),
										  rgSdeArray.crArrayName,
										  &(rgSdeArray.rrIdxHandle[0]),
										  rgSdeArray.crIdxName[0],clUrnoList,
										  &llRow, (void*)&pclRow ) == RC_SUCCESS )
				ilRc = PrepareSdeArray ( SDEFIELD(pclRow,igSdeUsrhIdx)  );
		}
		else if ( !strncmp ( clTable, "OST", 3 ) )
		{	
			ilRc1 = GetFieldByUrno ( &rgOstArray, igOstUaftIdx, clUrnoList, clUaft );
		}
		else if ( !strncmp ( clTable, "AF1", 3 ) )
		{	
			ilRc1 = GetFieldByUrno ( &rgAf1Array, igAf1FlnuIdx, clUrnoList, clUaft );
			if ( ilRc1 == RC_SUCCESS )
			{
				ilRc1 = HandleAf1Update ( clUaft );
				if ( ilRc1 != RC_SUCCESS )
					dbg ( TRACE, "HandleData: HandleAf1Update FLNU <%s> failed", clUaft );
			}
		}
		else
			if ( ilRc1 != RC_DONT_CARE )
			{
				dbg ( TRACE, "HandleData: Processing of Update failed UAFT <%s> RC <%d>", 
					  clUaft, ilRc1 );
				ilRc = RC_FAIL;
			}
		if ( clUaft[0] )
		{	/*  Update on CCA, JOB or OST which has been handled */
			if ( ilRc1 == RC_SUCCESS )
			{
				ilRc1 = SetCONS ( &rgAftArray, clUaft, 0, FALSE );
				if ( ilRc1 != RC_SUCCESS )
					dbg ( TRACE, "HandleData: SetCONS UAFT <%s> UHSS <null> failed", 
						  clUaft );
			}		
			ilRc = ProcessSCC( clUaft, 0 );
			if ( ilRc != RC_SUCCESS )
				dbg ( TRACE, "HandleData: ProcessSCC UAFT <%s> UHSS <null> failed", 
					 clUaft );
			PrepareSBC ( clUaft, 0 );
			SaveOstTab ();
		}
	}
	else if ( !strcmp(prlCmdblk->command,"DRT") )
	{
		ilRc1 = RC_DONT_CARE;
		if ( !strncmp ( clTable, "CCA", 3 ) )
			ilRc1 = GetFieldByUrno ( &rgCcaArray, igCcaFlnuIdx, clUrnoList, clUaft );
		else if ( !strncmp ( clTable, "JOB", 3 ) )
		{	
			ilRc1 = GetFieldByUrno ( &rgJobArray, igJobUaftIdx, clUrnoList, clUaft );
			ilRc1 |= GetFieldByUrno ( &rgJobArray, igJobUtplIdx, clUrnoList, clUtpl );
		}
		else if ( !strncmp ( clTable, "AF1", 3 ) )
		{	
			ilRc1 = GetFieldByUrno ( &rgAf1Array, igAf1FlnuIdx, clUrnoList, clUaft );
		}
		ilRc = CEDAArrayEventUpdate2(prgEvent,clUrnoList,clTable);
		if ( (ilRc==RC_SUCCESS) && (ilRc1==RC_SUCCESS) )
		{
			if ( !strncmp ( clTable, "CCA", 3 ) )
				ilRc1 = HandleCCAUpdate ( clUaft );
			else if ( !strncmp ( clTable, "JOB", 3 ) )
				ilRc1 = HandleJobUpdate ( clUaft, clUtpl );
			else if ( !strncmp ( clTable, "AF1", 3 ) )
				ilRc1 = HandleAf1Update ( clUaft );

			if ( ilRc1 != RC_SUCCESS )
			{
				dbg ( TRACE, "HandleData: Processing of Update failed UAFT <%s> RC <%d>", 
					  clUaft, ilRc1 );
			}
			else
			{
				ilRc = ProcessSCC( clUaft, 0 );
				if ( ilRc != RC_SUCCESS )
					dbg ( TRACE, "HandleData: ProcessSCC UAFT <%s> UHSS <null> failed", 
						 clUaft );
			}
			PrepareSBC ( clUaft, 0 );
			SaveOstTab ();
		}
		if ( (ilRc1 != RC_SUCCESS) && (ilRc1 != RC_DONT_CARE) )
			ilRc = RC_FAIL;
	}
	else if ( !strcmp(prlCmdblk->command,"UFR") )
	{
		strcpy (prlCmdblk->command, "URT" );
		
		IsAftChangeImportant ( pclFields, pclData, &bpDoEval, &bpSetCons, &bpSetCona );

		ilRc = CEDAArrayEventUpdate2(prgEvent,clUrnoList,clTable);
		if ( ilRc == RC_SUCCESS )
		{
			if ( !IS_EMPTY(cgChangedRkeys) )
			{
				dbg ( TRACE, "HandleData: Recognized changed RKEY, selection for recalculation <%s>",
					  cgChangedRkeys );
				ilRc = CEDAArrayRefill(&rgReqAftArray.rrArrayHandle, 
									   rgReqAftArray.crArrayName, cgChangedRkeys, "", ARR_FIRST );
				if ( ilRc != RC_SUCCESS )
					dbg ( TRACE, "HandleData: CEDAArrayRefill <AFT> failed, RC <%d>", ilRc );
				ilRc = EvaluateRules ( &rgReqAftArray, 0 );
				if ( ilRc == RC_SUCCESS )
				{
					ilRc = CreateSkeleton ( &rgReqAftArray );
					if ( ilRc != RC_SUCCESS )
						dbg ( TRACE, "HandleData: CreateSkeleton failed RC <%d>", ilRc );
					ilRc = UpdateOSTTAB ( &rgReqAftArray );
					if ( ilRc != RC_SUCCESS )
						dbg ( TRACE, "HandleData: UpdateOSTTAB failed RC <%d>", ilRc );
					PrepareSBC ( 0, &rgReqAftArray );
					SaveOstTab ();
				}
			}
			else
			{
				if ( bpDoEval )
				{
					ilRc = EvaluateFlight ( clUrnoList );
					if ( ilRc == RC_SUCCESS )
						ilRc = CreateSkeleton ( &rgReqAftArray );
					if ( ilRc != RC_SUCCESS )
						dbg ( TRACE, "HandleData: CreateSkeleton failed RC <%d>", ilRc );
					else
					{
						ilRc = UpdateOSTTAB (&rgReqAftArray );
						PrepareSBC ( 0, &rgReqAftArray );
						SaveOstTab ();
					}
				}
				else if ( bpSetCons || bpSetCona )
				{
					if ( bpSetCona )
					{
						ilRc = SetConaForFlight( clUrnoList, 0 );
						if ( ilRc != RC_SUCCESS )
							dbg ( TRACE, "HandleData: SetConaForFlight UAFT <%s> UHSS <null> failed", 
								  clUrnoList );
					}
					if ( bpSetCons )
					{
						ilRc = SetCONS ( &rgAftArray, clUrnoList, 0, FALSE );
						if ( ilRc != RC_SUCCESS )
							dbg ( TRACE, "HandleData: SetCONS UAFT <%s> UHSS <null> failed", 
								clUrnoList );
					}
					if ( ilRc == RC_SUCCESS )
					{
						ilRc = ProcessSCC( clUrnoList, 0 );
						if ( ilRc != RC_SUCCESS )
							dbg ( TRACE, "HandleData: ProcessSCC UAFT <%s> UHSS <null> failed", 
								  clUrnoList );
					}
					PrepareSBC ( clUrnoList, 0 );
					SaveOstTab ();
				}
			}
		}
		else
			dbg ( TRACE, "HandleData: CEDAArrayEventUpdate2 RC <%d>", ilRc );
	}
	else if ( !strcmp(prlCmdblk->command,"IFR") )
	{
		strcpy (prlCmdblk->command, "IRT" );
		ilRc = CEDAArrayEventUpdate2(prgEvent,clUrnoList,clTable);
		if ( ilRc == RC_SUCCESS )
		{
			ilRc = EvaluateFlight ( clUrnoList );
			if ( ilRc == RC_SUCCESS )
				ilRc = CreateSkeleton ( &rgReqAftArray );
			if ( ilRc != RC_SUCCESS )
				dbg ( TRACE, "HandleData: CreateSkeleton failed RC <%d>", ilRc );
			else
			{
				ilRc = UpdateOSTTAB ( &rgReqAftArray );
				PrepareSBC ( clUrnoList, 0 );
				SaveOstTab ();
			}
		}
		else
			dbg ( TRACE, "HandleData: CEDAArrayEventUpdate2 RC <%d>", ilRc );

	}
	else if ( !strcmp(prlCmdblk->command,"CFS") )
	{
		SendAnswer(prgEvent,RC_SUCCESS);
		ProcessCFS(pclSelection);	
		/*
		tools_send_info_flag(1900,mod_id, clDestName, "STAHDL", clRecvName,
							  "", "", clTwStart, clTwEnd,
							  prlCmdblk->command,clTable,pclSelection,"","",0);          
		*/
	}
	else if ( !strcmp(prlCmdblk->command,"SCC") )
	{	/* command 'SCC' = status conflict check */
		ProcessSCC( 0, 0 );	
		SaveOstTab ();
	}
	else if ( !strcmp(prlCmdblk->command,"UPS") || !strcmp(prlCmdblk->command,"UPJ") )
	{
		strcpy (prlCmdblk->command, "UFR" );
		ilRc = CEDAArrayEventUpdate2(prgEvent,clUrnoList,clTable);
	}

	dbg(TRACE,"==========  END  <%10.10d> ==========",lgEvtCnt);

	return(ilRc);
	
} /* end of HandleData */

static int GetDebugLevel(char *pcpMode, int *pipMode)
{
	int		ilRC = RC_SUCCESS;
	char	clCfgValue[64];

	ilRC = iGetConfigRow(cgConfigFile, "SYSTEM", pcpMode, CFG_STRING,
							clCfgValue);

	if(ilRC != RC_SUCCESS)
	{
		dbg(TRACE,"SYSTEM %s not found in <%s>", pcpMode, cgConfigFile);
	}
	else
	{
		dbg(TRACE, "SYSTEM %s <%s>", pcpMode, clCfgValue);
		if (!strcmp(clCfgValue, "DEBUG"))
			*pipMode = DEBUG;
		else if (!strcmp(clCfgValue, "TRACE"))
			*pipMode = TRACE;
		else
			*pipMode = 0;
	}

	return ilRC;
}

/******************************************************************************/
/*  SetArrayInfo:                                                             */
/*  following flags activate/disactivate features to improve performance	  */
/*	CEDAARRAY_FILL:    fill array from DB; rgNewSDIArr doesn't need to be filled  */
/*	CEDAARRAY_URNOIDX: create an index for URNO; if not used, we have one	  */
/*					   Index less, which has to be sorted !					  */
/*	CEDAARRAY_TRIGGER: UCDTAB, that is used only by DISCAL doesn't need to be */
/*					   triggered											  */
/******************************************************************************/

static int SetArrayInfo(char *pcpArrayName, char *pcpTableName, 
						char *pcpArrayFieldList,char *pcpAddFields,
						long *pcpAddFieldLens,char *pcpSelection,
						ARRAYINFO *prpArrayInfo, short spFlags )
{
	int	i, ilRealFields=0, ilAddFields=0, ilRc = RC_SUCCESS;

	if(prpArrayInfo == NULL)
	{
		dbg(TRACE,"SetArrayInfo: invalid last parameter : null pointer not valid");
		ilRc = RC_FAIL;
	}
	else
	{
		memset(prpArrayInfo,0x00,sizeof(prpArrayInfo));

		prpArrayInfo->rrArrayHandle = -1;
		for ( i=0; i<ARR_MAX_IDX; i++ )
			prpArrayInfo->rrIdxHandle[i] = -1;

		if(pcpArrayName != NULL)
		{
			if(strlen(pcpArrayName) <= ARR_NAME_LEN)
			{
				strcpy(prpArrayInfo->crArrayName,pcpArrayName);
			}/* end of if */
		}/* end of if */
	}/* end of if */

	if(ilRc == RC_SUCCESS)
	{
		ilRealFields = get_no_of_items(pcpArrayFieldList);
		prpArrayInfo->lrArrayFieldCnt = ilRealFields ;
		if (pcpAddFields != NULL)
		{
			ilAddFields = get_no_of_items(pcpAddFields);
			prpArrayInfo->lrArrayFieldCnt += ilAddFields ;
		}

		prpArrayInfo->plrArrayFieldOfs = (long*) calloc(prpArrayInfo->lrArrayFieldCnt,sizeof(long));
		if(prpArrayInfo->plrArrayFieldOfs == NULL)
		{
			dbg(TRACE,"SetArrayInfo: plrArrayFieldOfs calloc(%d,%d) failed",
				prpArrayInfo->lrArrayFieldCnt,sizeof(long));
			ilRc = RC_FAIL;
		}/* end of if */
	}/* end of if */

	if(ilRc == RC_SUCCESS)
	{
		prpArrayInfo->plrArrayFieldLen = (long*) calloc(prpArrayInfo->lrArrayFieldCnt,sizeof(long));
		if(prpArrayInfo->plrArrayFieldLen == NULL)
		{
			dbg(TRACE,"SetArrayInfo: plrArrayFieldLen calloc(%d,%d) failed",
				prpArrayInfo->lrArrayFieldCnt,sizeof(long));
			ilRc = RC_FAIL;
		}
		else
		{
			*(prpArrayInfo->plrArrayFieldLen) = -1;
		}/* end of if */
	}/* end of if */

	if(pcpTableName == NULL)
	{
		dbg(TRACE,"SetArrayInfo: invalid parameter TableName: null pointer not valid");
		ilRc = RC_FAIL;
	}
	else
	{
		strcpy ( prpArrayInfo->crTableName,pcpTableName );
		strcat ( prpArrayInfo->crTableName,cgTabEnd );
       	dbg(TRACE,"SetArrayInfo: crTableName=<%s>",prpArrayInfo->crTableName );
    }
   	if(ilRc == RC_SUCCESS)
   	{
       	ilRc = GetRowLength(prpArrayInfo->crTableName, pcpArrayFieldList,
							&(prpArrayInfo->lrArrayRowLen));
       	if(ilRc != RC_SUCCESS)
       	{
           	dbg(TRACE,"SetArrayInfo: GetRowLength failed <%s> <%s>",pcpArrayName,pcpArrayFieldList);
      	}/* end of if */
   	}/* end of if */

   	if(ilRc == RC_SUCCESS)
   	{
		prpArrayInfo->lrArrayRowLen+=100;
		prpArrayInfo->pcrArrayRowBuf = (char *) calloc(1,(prpArrayInfo->lrArrayRowLen+10));
   		if(prpArrayInfo->pcrArrayRowBuf == NULL)
   		{
      			ilRc = RC_FAIL;
      			dbg(TRACE,"SetArrayInfo: calloc failed");
   		}/* end of if */
   	}/* end of if */


	if(ilRc == RC_SUCCESS)
	{
		dbg(TRACE,"CEDAArrayCreate follows Array <%s> Table <%s>",pcpArrayName,
													prpArrayInfo->crTableName);
		ilRc = CEDAArrayCreate( &(prpArrayInfo->rrArrayHandle),
								pcpArrayName,prpArrayInfo->crTableName,
								pcpSelection,pcpAddFields, pcpAddFieldLens, 
								pcpArrayFieldList,
								&(prpArrayInfo->plrArrayFieldLen[0]),
								&(prpArrayInfo->plrArrayFieldOfs[0]));

		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"SetArrayInfo: CEDAArrayCreate failed <%d>",ilRc);
		}/* end of if */
	}/* end of if */

	if(pcpArrayFieldList != NULL)
	{
		if(strlen(pcpArrayFieldList) <= ARR_FLDLST_LEN)
		{
			strcpy(prpArrayInfo->crArrayFieldList,pcpArrayFieldList);
		}/* end of if */
	}/* end of if */
	if(pcpAddFields != NULL)
	{
		if(strlen(pcpAddFields) + 
			strlen(prpArrayInfo->crArrayFieldList) <= 
					(size_t)ARR_FLDLST_LEN)
		{
			strcat(prpArrayInfo->crArrayFieldList,",");
			strcat(prpArrayInfo->crArrayFieldList,pcpAddFields);
			dbg(TRACE,"<%s>",prpArrayInfo->crArrayFieldList);
		}/* end of if */
		for ( i=0; i<ilAddFields; i++ )
		{
			prpArrayInfo->plrArrayFieldLen[i+ilRealFields] = pcpAddFieldLens[i];
		}
	}/* end of if */
	if ( (ilRc == RC_SUCCESS) && (spFlags&CEDAARRAY_FILL) )
	{
		ilRc = CEDAArrayFill(&(prpArrayInfo->rrArrayHandle),pcpArrayName,NULL);
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"SetArrayInfo: CEDAArrayFill failed <%d>",ilRc);
		}
	}
	if ( (ilRc == RC_SUCCESS) && (spFlags&CEDAARRAY_URNOIDX) && 
		  strstr(pcpArrayFieldList,"URNO") )
	{
		strcpy ( prpArrayInfo->crIdxName[0], "URNOIDX" );
		strcpy ( prpArrayInfo->crIdxFields[0], "URNO" );
		prpArrayInfo->lrIdxLength[0] = 11;
		ilRc = CEDAArrayCreateSimpleIndexUp(&(prpArrayInfo->rrArrayHandle),
											  prpArrayInfo->crArrayName,
											  &(prpArrayInfo->rrIdxHandle[0]), 
											  prpArrayInfo->crIdxName[0], "URNO" );
		if(ilRc != RC_SUCCESS)                                             
		{
			dbg( TRACE,"SetArrayInfo: CEDAArrayCreateSimpleIndexDown for %s failed <%d>",
				 prpArrayInfo->crTableName, ilRc);		
		}
	}
	if ( (ilRc == RC_SUCCESS) && (spFlags&CEDAARRAY_TRIGGER) )
	{                                          /* send update msg. for active changes */ 
		/*ilRc = TriggerAction(prpArrayInfo->crTableName,pcpArrayFieldList) ;*/
		if ( strcmp ( pcpTableName, "AFT" ) == 0 )
			ilRc = ConfigureAction (prpArrayInfo->crTableName,pcpArrayFieldList, "IFR,UFR,DFR,UPS,UPJ" );
		else
			ilRc = ConfigureAction (prpArrayInfo->crTableName,pcpArrayFieldList, NULL );

		if (ilRc != RC_SUCCESS)
			dbg (TRACE, "SetArrayInfo: ConfigureAction table<%s> failed <%d>", 
				  prpArrayInfo->crTableName, ilRc) ;
		else
			dbg (TRACE, "SetArrayInfo: ConfigureAction table<%s> succeeded.", 
				  prpArrayInfo->crTableName ) ; 
	}/* end of if */


	{
		int ilLc;
		char clFina[11];

		for(ilLc = 0; ilLc < prpArrayInfo->lrArrayFieldCnt; ilLc++)
		{
			get_real_item(clFina,prpArrayInfo->crArrayFieldList,ilLc+1);
			dbg(TRACE,"Field <%s> Idx <%02d> Offs <%d> Length <%d>",clFina, 
						ilLc,prpArrayInfo->plrArrayFieldOfs[ilLc],
						prpArrayInfo->plrArrayFieldLen[ilLc]);
		}
	}
	return(ilRc);
}



/******************************************************************************/
/******************************************************************************/
static int GetRowLength(char *pcpTana, char *pcpFieldList, long *plpLen)
{
	int	 ilRc = RC_SUCCESS;			/* Return code */
	int  ilNoOfItems = 0;
	int  ilLoop = 0;
	long llFldLen = 0;
	long llRowLen = 0;
	char clFina[8];
	
	ilNoOfItems = get_no_of_items(pcpFieldList);

	ilLoop = 1;
	do
	{
		ilRc = get_real_item(&clFina[0],pcpFieldList,ilLoop);
		if(ilRc > 0)
		{
			ilRc = GetFieldLength(pcpTana,clFina,&llFldLen);
			if(ilRc == RC_SUCCESS)
			{
				llRowLen++;
				llRowLen += llFldLen;
			}/* end of if */
		}/* end of if */
		ilLoop++;
	}while((ilRc == RC_SUCCESS) && (ilLoop <= ilNoOfItems));

	if(ilRc == RC_SUCCESS)
	{
		*plpLen = llRowLen;
	}/* end of if */

	return(ilRc);
	
} /* end of GetRowLength */


/*  GetTotalRowLength: Get length of a row containing a specified fieldlist */
/*	IN:		pcpFieldList:	requested field list							*/
/*			prpArray:		array to be handled								*/
/*			pcpFields:		Field to be used for index						*/
/*	OUT:	plpLen:			requesed length including space for delimiters	*/
static int GetTotalRowLength( char *pcpFieldList, ARRAYINFO *prpArray, long *plpLen)
{
    int	 ilRc        = RC_SUCCESS;			/* Return code */
    int  ilNoOfItems = 0;
    int  ilItemNo = 0;
    int  ilLoop      = 0;
    long llRowLen    = 0;
    long llFldLen    = 0;
    char clFina[8];
	
    if (prpArray != NULL && pcpFieldList != NULL)
    {
		ilNoOfItems = get_no_of_items(pcpFieldList);
		dbg ( DEBUG, "GetTotalRowLength: Fields to calculate <%s>", pcpFieldList);
		dbg ( DEBUG, "GetTotalRowLength: Array-Fieldlist <%s>", prpArray->crArrayFieldList);
			
		ilLoop = 1;
		do
		{
			get_real_item(clFina,pcpFieldList,ilLoop);

			ilItemNo = get_item_no(prpArray->crArrayFieldList,clFina,strlen(clFina)+1 ); 
			/* if(GetItemNo(clFina,prpArray->crArrayFieldList,&ilItemNo) == RC_SUCCESS)  */
			if ( ilItemNo >= 0 )
			{
				llRowLen++;
				llRowLen += prpArray->plrArrayFieldLen[ilItemNo];
				dbg(DEBUG,"GetTotalRowLength: clFina <%s> Index <%d> Length <%ld>",
					clFina,ilItemNo, prpArray->plrArrayFieldLen[ilItemNo]);
			}
			else
				ilRc == RC_FAIL;
			ilLoop++;
		}while(ilLoop <= ilNoOfItems);
	}
	if(ilRc == RC_SUCCESS)
	{
		*plpLen = llRowLen;
	}/* end of if */

	dbg(DEBUG,"GetTotalRowLength:  FieldList <%s> Length <%ld>",pcpFieldList,*plpLen);
	return(ilRc);
	
} /* end of GetTotalRowLength */

/******************************************************************************/
/******************************************************************************/
static int GetFieldLength(char *pcpTana, char *pcpFina, long *plpLen)
{
    int  ilRc  = RC_SUCCESS;			/* Return code */
    int  ilCnt = 0;
    char clTaFi[32];
    char clFele[16];
    char clFldLst[16];

    ilCnt = 1;
    sprintf(&clTaFi[0],"%s,%s",pcpTana,pcpFina);
    sprintf(&clFldLst[0],"TANA,FINA"); /* wird von syslib zerst�rt - konstante nicht m�glich */

 
    ilRc = syslibSearchSystabData(&cgTabEnd[0],&clFldLst[0],&clTaFi[0],"FELE",&clFele[0],&ilCnt,"");
    switch (ilRc)
    {
	case RC_SUCCESS :
	    *plpLen = atoi(&clFele[0]);
	    /* dbg(DEBUG,"GetFieldLength: <%s,%s,%s> <%d>",pcpHopo,pcpTana,pcpFina,*plpLen);*/
	    break;

	case RC_NOT_FOUND :

	    dbg(TRACE,"GetFieldLength: pcpTana <%s> pcpFina<%s>",pcpTana,pcpFina);
	    dbg(TRACE,"GetFieldLength: syslibSSD returned <%d> RC_NOT_FOUND <%s>",ilRc,&clTaFi[0]);
	    break;

	case RC_FAIL :
	    dbg(TRACE,"GetFieldLength: syslibSSD returned <%d> RC_FAIL",ilRc);
	    break;

	default :
	    dbg(TRACE,"GetFieldLength: syslibSSD returned <%d> unexpected",ilRc);
	    break;
    }/* end of switch */

    return(ilRc);
	
} /* end of GetFieldLength */


/*  CreateIdx: create an index for an array									*/
/*	IN:	prpArrayInfo:	array to be handled									*/
/*		pcpIdxName:		Name of index										*/
/*		pcpFields:		Field to be used for index							*/
/*		pcpOrdering:	requesed ordering of fields, e. g. "A,D,A"			*/
/*		pcpOrdering:	number of index in the array of indices				*/
static int CreateIdx ( ARRAYINFO *prpArrayInfo, char *pcpIdxName,
					   char *pcpFields, char *pcpOrdering, int ipIdx )
{
	int		ilRC = RC_FAIL, ilLoop;
	char clOrderTxt[4];
	long	*pllOrdering=0, llFieldCnt;

	if ( !pcpFields )
		return ilRC;		/*  0-Pointer in parameter pcpFields */
	if ( ipIdx >= ARR_MAX_IDX )
		return ilRC;

	strcpy ( prpArrayInfo->crIdxName[ipIdx], pcpIdxName );
	strcpy ( prpArrayInfo->crIdxFields[ipIdx], pcpFields );
	
	ilRC = GetTotalRowLength( pcpFields, prpArrayInfo,
							  &(prpArrayInfo->lrIdxLength[ipIdx]) );

	if(ilRC != RC_SUCCESS)
        dbg(TRACE,"CreateIdx: GetTotalRowLength failed <%s> <%s>",
			prpArrayInfo->crTableName, pcpFields);

	if(ilRC == RC_SUCCESS)
	{
		llFieldCnt = get_no_of_items(pcpFields);
		pllOrdering = (long *) calloc(llFieldCnt,sizeof(long));
		if(!pllOrdering )
		{
			dbg(TRACE,"CreateIdx: pllOrdering calloc(%d,%d) failed", 
				llFieldCnt,sizeof(long));
			ilRC = RC_FAIL;
		}
		else
		{
			for(ilLoop=0; ilLoop < llFieldCnt; ilLoop++)
			{
				if ( !pcpOrdering ||
					 ( get_real_item(clOrderTxt,pcpOrdering,ilLoop+1)<=0 )
					)				 
					clOrderTxt[0] = 'D';
				if ( clOrderTxt[0] == 'A' )
					pllOrdering[ilLoop] = ARR_ASC;
				else
					pllOrdering[ilLoop] = ARR_DESC;
			}/* end of for */
		}/* end of else */
	}/* end of if */

	ilRC = CEDAArrayCreateMultiIndex( &(prpArrayInfo->rrArrayHandle), 
									  prpArrayInfo->crArrayName,
									  &(prpArrayInfo->rrIdxHandle[ipIdx]), 
									  prpArrayInfo->crIdxName[ipIdx],
									  prpArrayInfo->crIdxFields[ipIdx], 
									  pllOrdering );
	if ( pllOrdering )
		free (pllOrdering);
	pllOrdering = 0;
	return ilRC;									
}

/******************************************************************************/
static int SaveIndexInfo (ARRAYINFO *prpArrayInfo, int ipMode, int ipLogFile )
{
	int    ilRC       = RC_SUCCESS ;
	long   llRow      = 0 ;
	FILE  *prlFile    = NULL ;
	char   clDel      = ',' ;
	char   clFile[512] ;
	char  *pclTrimBuf = NULL ;
	long llRowCount;
	int ilCount = 0;
	char  *pclTestBuf = NULL ;
	char	*pclRowBuff=0;
	int     ilRowLen;

	ilRowLen = prpArrayInfo->lrIdxLength[ipMode];
	pclRowBuff = (char *) calloc(1, ilRowLen+1);
	dbg (TRACE,"SaveIndexInfo: ArrayName <%s>",prpArrayInfo->crArrayName) ;

	if ( !pclRowBuff )
	{
		dbg ( TRACE, "SaveIndexInfo: Calloc failed" );
		ilRC = RC_FAIL;
	}
		
	if (ilRC == RC_SUCCESS)
	{
		sprintf (&clFile[0],"%s/%s_%s.info",getenv("DBG_PATH"),mod_name,prpArrayInfo->crArrayName) ;

		errno = 0 ;
		prlFile = fopen (&clFile[0], "w") ;
		if (prlFile == NULL)
		{
			dbg (TRACE, "SaveIndexInfo: fopen <%s> failed <%d-%s>", clFile, 
				  errno, strerror(errno)) ;
			ilRC = RC_FAIL ;
		} /* end of if */
	} /* end of if */

	if (ilRC == RC_SUCCESS)
	{
		if ( ipLogFile )
		{
			dbg (DEBUG, "ArrayName      <%s>\n", prpArrayInfo->crArrayName) ;      
			dbg (DEBUG, "ArrayFieldList <%s>\n", prpArrayInfo->crArrayFieldList) ;  
			dbg (DEBUG, "IdxFields <%s>\n", prpArrayInfo->crIdxFields[ipMode]) ;  
		}
		fprintf (prlFile, "ArrayName      <%s>\n", prpArrayInfo->crArrayName) ; /*fflush(prlFile) ;*/
		fprintf (prlFile, "ArrayFieldList <%s>\n", prpArrayInfo->crArrayFieldList) ; /*fflush(prlFile) ;*/
		fprintf (prlFile, "IdxFields <%s>\n", prpArrayInfo->crIdxFields[ipMode]) ; 
		fflush(prlFile) ;

		if ( ipLogFile )
			dbg(DEBUG,"IndexData") ; 
		fprintf(prlFile,"IndexData\n"); fflush(prlFile);
		if ( prpArrayInfo->rrIdxHandle[ipMode] > -1) 
		{
			pclRowBuff[0] = '\0';
			llRow = ARR_FIRST ;
			do
			{
				ilRC = CEDAArrayFindKey ( &(prpArrayInfo->rrArrayHandle), 
										  prpArrayInfo->crArrayName, 
										  &(prpArrayInfo->rrIdxHandle[ipMode]), 
										  prpArrayInfo->crIdxName[ipMode], 
										  &llRow, ilRowLen, pclRowBuff );
				if (ilRC == RC_SUCCESS)
				{
					if ( ipLogFile )
						dbg(DEBUG, "SaveIndexInfo: Key <%ld> <%s>",llRow,pclRowBuff) ;
		
					if (strlen (pclRowBuff) == 0)
					{
						ilRC = CEDAArrayGetRow(&(prpArrayInfo->rrArrayHandle), &(prpArrayInfo->crArrayName[0]), llRow, clDel, prpArrayInfo->lrArrayRowLen, prpArrayInfo->pcrArrayRowBuf);
						if( ipLogFile && (ilRC == RC_SUCCESS) )
							dbg (DEBUG, "SaveIndexInfo: Row <%ld> <%s>", llRow,prpArrayInfo->pcrArrayRowBuf) ;
					} /* end of if */

					pclTrimBuf = strdup (pclRowBuff) ;
					if (pclTrimBuf != NULL)
					{
						ilRC = GetDataItem (pclTrimBuf,pclRowBuff, 1, clDel, "", "  ") ;
						if (ilRC > 0)
						{
						   ilRC = RC_SUCCESS ;
						   fprintf (prlFile,"Key            <%ld><%s>\n",llRow,pclTrimBuf) ; fflush(prlFile) ;
						} /* end of if */
						free (pclTrimBuf) ;
						pclTrimBuf = NULL ;
					} /* end of if */
					llRow = ARR_NEXT;
				}
				else
				{
					if ( ipLogFile )
						dbg (TRACE, "SaveIndexInfo: CEDAArrayFindKey failed <%d>", ilRC) ;
				}
			} while (ilRC == RC_SUCCESS) ;
		}
		if ( ipLogFile )
			dbg (DEBUG,"ArrayData") ;
	
		fprintf (prlFile,"ArrayData\n"); fflush(prlFile);
		llRow = ARR_FIRST ;

		CEDAArrayGetRowCount(&(prpArrayInfo->rrArrayHandle),prpArrayInfo->crArrayName,&llRowCount);

		llRow = ARR_FIRST;
		if ( ipLogFile )
			dbg(DEBUG,"Array  <%s> data follows Rows %ld", prpArrayInfo->crArrayName,llRowCount);
		do
		{
			ilRC = CEDAArrayGetRowPointer(&(prpArrayInfo->rrArrayHandle),
			&(prpArrayInfo->crArrayName[0]),llRow,(void *)&pclTestBuf);
			if (ilRC == RC_SUCCESS)
			{
				for( ilCount = 0; ilCount < prpArrayInfo->lrArrayFieldCnt; ilCount++)
				{
				
					fprintf (prlFile,"<%s>",&pclTestBuf[prpArrayInfo->plrArrayFieldOfs[ilCount]]) ; 
					fflush(prlFile) ;
					if ( ipLogFile )
						dbg (DEBUG, "SaveIndexInfo: ilCount <%ld> <%s>", ilCount,&pclTestBuf[prpArrayInfo->plrArrayFieldOfs[ilCount]]) ;
				}
				fprintf (prlFile,"\n") ; 
				if ( ipLogFile )
					dbg (DEBUG, "\n") ;
				fflush(prlFile) ;
				llRow = ARR_NEXT;
			}
			else
			{
				if ( ipLogFile )
					dbg(DEBUG,"GetRowPointer failed RC=%d",ilRC);
			}
		} while (ilRC == RC_SUCCESS);

		if (ilRC == RC_NOTFOUND)
			ilRC = RC_SUCCESS ;
	} /* end of if */

	if (prlFile != NULL)
	{
		fclose(prlFile) ;
		prlFile = NULL ;
		if ( ipLogFile )
			dbg (TRACE, "SaveIndexInfo: event saved - file <%s>", clFile) ;
	} /* end of if */
	if ( pclRowBuff )
		free ( pclRowBuff );
	return (ilRC) ;

}/* end of SaveIndexInfo */

/*correct timefunction HEB*/
static int StrToTime(char *pcpTime,time_t *plpTime)
{

    struct tm *prlTm;
    struct tm rlTm;
    int i = 0;
    time_t tlNow;
    int ilIsDst = 0;
    int ilYear,ilMon,ilDay,ilHour,ilMin,ilSec;
    char clTmpString[124];
    char clTime[124];


    tlNow = time(NULL);
    prlTm = (struct tm *)localtime_r((const time_t*)&tlNow,&rlTm);
    tlNow = mktime(&rlTm);
    for (i=1; i<=2; i++)
    {
	strcpy(clTime,pcpTime);

	strncpy(clTmpString,clTime,4);
	clTmpString[4] = '\0';
	ilYear = atoi(clTmpString)-1900;

	strncpy(clTmpString,&clTime[4],2);
	clTmpString[2] = '\0';
	ilMon = atoi(clTmpString)-1;

	strncpy(clTmpString,&clTime[6],2);
	clTmpString[2] = '\0';
	ilDay = atoi(clTmpString);

	strncpy(clTmpString,&clTime[6],2);
	clTmpString[2] = '\0';
	ilDay = atoi(clTmpString);

	strncpy(clTmpString,&clTime[8],2);
	clTmpString[2] = '\0';
	ilHour = atoi(clTmpString);

	strncpy(clTmpString,&clTime[10],2);
	clTmpString[2] = '\0';
	ilMin = atoi(clTmpString);

	strncpy(clTmpString,&clTime[12],2);
	clTmpString[2] = '\0';
	ilSec = atoi(clTmpString);

	rlTm.tm_year = ilYear;
	rlTm.tm_mon  = ilMon;
	rlTm.tm_mday = ilDay;
	rlTm.tm_hour = ilHour;
	rlTm.tm_min = ilMin;
	rlTm.tm_sec = ilSec;

	ilIsDst = rlTm.tm_isdst;
	tlNow = mktime(&rlTm);
	if ((i == 1) && (ilIsDst != rlTm.tm_isdst) && (ilHour == 2))
	{
	    rlTm.tm_isdst = 0;
	}
 
    } /* end of for*/


    /* dbg(DEBUG,"StrToTime: %02d.%02d.%04d %02d:%02d Now: %ld",rlTm.tm_mday,
	rlTm.tm_mon+1,rlTm.tm_year+1900,rlTm.tm_hour,rlTm.tm_min,tlNow); */
    if (tlNow != (time_t) -1)
    {
	dbg(DEBUG,"StrToTime: pcpTime <%s> plptime: <%d>", pcpTime, tlNow);
	*plpTime = tlNow;
	return RC_SUCCESS;
    }
    *plpTime = time(NULL);
    return RC_FAIL;
}

/*correct timefunction HEB*/
static int TimeToStr(char *pcpTime,time_t lpTime) 
{               
    struct tm *prlTm;
    struct tm rlTm;

    prlTm = (struct tm *)localtime_r(&lpTime,&rlTm);

    sprintf(pcpTime,"%4d%02d%02d%02d%02d%02d",
	    rlTm.tm_year+1900,rlTm.tm_mon+1,rlTm.tm_mday,rlTm.tm_hour,
	    rlTm.tm_min,rlTm.tm_sec);
    return (rlTm.tm_wday == 0 ? 7 : rlTm.tm_wday) + '0';
}                      


/*  EvaluateRules: evaluate rules for all flights in an AFT-array			*/
/*	IN:	prpFlightArray:	array of flights to be evaluated					*/
/*		pcpUhss:		URNO of HSS to be evaluated, can be NULL for all	*/
static int EvaluateRules ( ARRAYINFO* prpFlightArray, char *pcpUhss )
{
	char *pclFlight, *pclRule, clSrhUrno[21], *pcl2ndFlight;
	char *pclInbound, *pclOutbound, *pclRkey, *pclResult;
	long llAftRow, llSrhRow;
	char clKey[21], clActUhss[21], clRowBuff[71], clAdid[11], *pclSrhAdid; 
	int	 i, ilRc, il2ndFound;
	BOOL blGoon ;
	long llNext, llGroundTime;
	int	ilEvtType, ilRuleFound;
	char clOuri[21], clOuro[21], clSrhi[21], clSrho[21];
	time_t tlTifa, tlTifd;

	llAftRow = ARR_FIRST;

	CEDAArrayDelete ( &(rgEvalResults.rrArrayHandle), rgEvalResults.crArrayName );
	while ( CEDAArrayGetRowPointer(&(prpFlightArray->rrArrayHandle),
								   prpFlightArray->crArrayName,llAftRow,
								   (void *)&pclFlight) == RC_SUCCESS )
	{		
		llAftRow = ARR_NEXT;
		llNext = ARR_FIRST;
		pclRkey = AFTFIELD(pclFlight,igAftRkeyIdx);
		if ( CEDAArrayFindRowPointer ( &(rgEvalResults.rrArrayHandle), 
									   rgEvalResults.crArrayName,
									  &(rgEvalResults.rrIdxHandle[1]),
									  rgEvalResults.crIdxName[1],pclRkey,
									  &llNext, (void*)&pclResult ) == RC_SUCCESS )
		{
			dbg ( TRACE, "EvaluateRules: RKEY <%s> already evaluated UAFT <%s>",
				  pclRkey, AFTFIELD(pclFlight,igAftUrnoIdx) );
			continue;
		}
		strcpy ( clAdid, AFTFIELD(pclFlight,igAftAdidIdx) );
		pclInbound = pclOutbound = 0;
		llGroundTime = 0;
		il2ndFound = Find2ndFlight ( prpFlightArray, pclFlight, &pcl2ndFlight );
		if ( clAdid[0] == 'A') 
		{
			pclInbound = pclFlight;
			if ( il2ndFound == RC_SUCCESS )
			{
				pclOutbound = pcl2ndFlight;
				ilEvtType = EVT_TURN;
			}
			else
				ilEvtType = EVT_IN;
		}
		else
		{
			pclOutbound = pclFlight;
			if ( il2ndFound == RC_SUCCESS )
			{
				pclInbound = pcl2ndFlight;
				ilEvtType = EVT_TURN;
			}
			else
				ilEvtType = EVT_OUT;
		}
		if ( pclInbound )
		{
			strcpy ( clOuri, AFTFIELD(pclInbound,igAftUrnoIdx) );
			UpdateMinMax ( AFTFIELD(pclInbound,igAftTifaIdx) );
		}
		else
			strcpy ( clOuri, " " );
		if ( pclOutbound )
		{
			strcpy ( clOuro, AFTFIELD(pclOutbound,igAftUrnoIdx) );
			UpdateMinMax ( AFTFIELD(pclOutbound,igAftTifdIdx) );
		}
		else
			strcpy ( clOuro, " " );
			
		sprintf ( clKey, "%c", cgFlagActive ); 
		if ( pclInbound && pclOutbound )
		{	/* calculate actual ground time */
			StrToTime(AFTFIELD(pclInbound,igAftTifaIdx),&tlTifa);
			StrToTime(AFTFIELD(pclOutbound,igAftTifdIdx),&tlTifd);
			llGroundTime = tlTifd - tlTifa;
		}
		if ( pcpUhss )
		{
			strcat ( clKey, "," );
			strcat ( clKey, pcpUhss );
		}
		dbg ( TRACE, "EvaluateRules: Evaluate Flight(s) <%s> UAFT1 <%s> UAFT2 <%s> Evt-Type <%d> SRH-Key <%s>",
			  AFTFIELD(pclFlight,igAftFlnoIdx), clOuri,  clOuro, ilEvtType, clKey );
		
		llSrhRow = ARR_FIRST;
		clActUhss[0]='\0';
		ilRuleFound = 0;
		strcpy ( clSrhi, " " );
		strcpy ( clSrho, " " );

		while ( CEDAArrayFindRowPointer(&(rgSrhArray.rrArrayHandle),
									    rgSrhArray.crArrayName,
										&(rgSrhArray.rrIdxHandle[1]),
										rgSrhArray.crIdxName[1],clKey,&llSrhRow,
										(void*)&pclRule ) == RC_SUCCESS )
		{
			strcpy ( clSrhUrno, SRHFIELD(pclRule,igSrhUrnoIdx) );
			pclSrhAdid = SRHFIELD(pclRule,igSrhAdidIdx) ;
			blGoon = TRUE;
			if ( strcmp ( clActUhss, SRHFIELD(pclRule,igSrhUhssIdx) ) == 0 )	
			{	/* same section */
				if ( ilRuleFound == ilEvtType ) 
				{	/* rule(s) already found */
					blGoon = FALSE;
					dbg ( DEBUG, "EvaluateRules: Skip Rule <%s> UHSS <%s>", 
						  SRHFIELD(pclRule,igSrhNameIdx), SRHFIELD(pclRule,igSrhUhssIdx) );
				}
				else
					dbg ( DEBUG, "EvaluateRules: Test Rule <%s> same UHSS <%s>", 
						  SRHFIELD(pclRule,igSrhNameIdx), SRHFIELD(pclRule,igSrhUhssIdx) );
			}	
			else
			{	/* change of section */
				if ( clActUhss[0] ) 
				{	/* EVAL_FIELDS "RETY,OURI,OURO,UHSS,SRHI,SRHO" */
					memset ( clRowBuff, ' ', sizeof(clRowBuff) );
					sprintf ( clRowBuff, "%s,%s,%s,%s,%s,%s", AFTFIELD(pclFlight,igAftRkeyIdx), 
													clOuri, clOuro, clActUhss, clSrhi,clSrho );
					delton(clRowBuff);
					llNext = ARR_NEXT;
					ilRc = AATArrayAddRow(&(rgEvalResults.rrArrayHandle),rgEvalResults.crArrayName,
										  &llNext,(void *)clRowBuff);
				}
				dbg ( DEBUG, "EvaluateRules: Test Rule <%s> new UHSS <%s>", 
					  SRHFIELD(pclRule,igSrhNameIdx), SRHFIELD(pclRule,igSrhUhssIdx) );
				ilRuleFound = 0;	/* reset variable  */
				strcpy ( clSrhi, " " );
				strcpy ( clSrho, " " );
				strcpy ( clActUhss, SRHFIELD(pclRule,igSrhUhssIdx) );
			}
			if ( blGoon )
			{
				if ( *pclSrhAdid == 'A' &&  (ilRuleFound & EVT_IN) )
					blGoon = FALSE;
				if ( *pclSrhAdid == 'D' &&  (ilRuleFound & EVT_OUT) )
					blGoon = FALSE;
				if ( *pclSrhAdid == 'T' &&  (ilRuleFound != 0) )
					blGoon = FALSE;
			}
			if ( blGoon )
			{	/*  We need to find a rule of this type */
				if ( CheckOneRule ( pclRule, pclInbound, pclOutbound, llGroundTime ) )
				{	/* all conditions are true */
					/* EVAL_FIELDS "RETY,OURI,OURO,UHSS,SRHI,SRHO" */
					if ( (*pclSrhAdid == 'A') || (*pclSrhAdid == 'T') )
					{
						ilRuleFound |= EVT_IN;
						strcpy ( clSrhi, clSrhUrno );
					}
					if ( (*pclSrhAdid == 'D') || (*pclSrhAdid == 'T') )
					{
						ilRuleFound |= EVT_OUT;
						strcpy ( clSrho, clSrhUrno );
					}
					dbg ( TRACE, "EvaluateRules: Rule <%s> UHSS <%s> ALL CONDITIONS ARE TRUE!!!",
						  SRHFIELD(pclRule,igSrhNameIdx), SRHFIELD(pclRule,igSrhUhssIdx) );
					dbg ( TRACE, "EvaluateRules: Evt-Type <%d> UHSS <%s> SRHI <%s> SRHO <%s> ilRuleFound <%d>!!!",
						  ilEvtType, SRHFIELD(pclRule,igSrhUhssIdx), clSrhi, clSrho, ilRuleFound );
				}
			}
			llSrhRow = ARR_NEXT;
		}
		if ( clActUhss[0] ) 
		{		/* no rule in last section found */
			memset ( clRowBuff, ' ', sizeof(clRowBuff) );
			sprintf ( clRowBuff, "%s,%s,%s,%s,%s,%s", AFTFIELD(pclFlight,igAftRkeyIdx), 
								clOuri, clOuro, clActUhss, clSrhi,clSrho );
			delton(clRowBuff);
			llNext = ARR_NEXT;
			ilRc = AATArrayAddRow(&(rgEvalResults.rrArrayHandle),rgEvalResults.crArrayName,
								  &llNext,(void *)clRowBuff);
		}
	}

	return 0;
}

/*  EvaluateFlight: evaluate rules for one flight (list of flights later)   */
/*	IN:		pcpUrnoList:	URNO of flight, must not be NULL				*/
static int EvaluateFlight ( char *pcpUrnoList )
{
	int ilRC ;
	char *pclFlightRow = 0, *pcl2ndFlight=0;
	long llRow;

	dbg ( TRACE, "EvaluateFlight: URNO-List <%s>", pcpUrnoList );
	ilRC = CEDAArrayDelete(&(rgReqAftArray.rrArrayHandle), rgReqAftArray.crArrayName );
	if ( ilRC != RC_SUCCESS )
		dbg ( TRACE, "EvaluateFlight: CEDAArrayDelete failed RC <%d>", ilRC );
	else
	{	/* copy flight from rgAftArray to rgReqAftArray */
		llRow = ARR_FIRST;
		ilRC = CEDAArrayFindRowPointer(&(rgAftArray.rrArrayHandle),
									    rgAftArray.crArrayName,
										&(rgAftArray.rrIdxHandle[0]),
										rgAftArray.crIdxName[0], pcpUrnoList, 
										&llRow, (void *)&pclFlightRow ) ;
		if ( ilRC != RC_SUCCESS )
			dbg ( TRACE, "EvaluateFlight: Flight not found !!! RC <%d>", ilRC );
		else	
		{
			llRow = ARR_LAST;
			ilRC = CEDAArrayAddRow (&rgReqAftArray.rrArrayHandle, 
									 rgReqAftArray.crArrayName, &llRow, 
									 (void*)pclFlightRow );
			if ( ilRC != RC_SUCCESS )
				dbg ( TRACE, "EvaluateFlight: CEDAArrayAddRow failed RC <%d>", ilRC );
			if ( Find2ndFlight ( &rgAftArray, pclFlightRow, &pcl2ndFlight ) == RC_SUCCESS )
			{
				llRow = ARR_LAST;
				ilRC = CEDAArrayAddRow (&rgReqAftArray.rrArrayHandle, 
										 rgReqAftArray.crArrayName, &llRow, 
										 (void*)pcl2ndFlight );
				if ( ilRC != RC_SUCCESS )
					dbg ( TRACE, "EvaluateFlight: CEDAArrayAddRow 2n Flight failed RC <%d>", ilRC );
			}
		}
	}
	if ( ilRC == RC_SUCCESS )
	{
		if ( debug_level > TRACE )
		{
			CEDAArrayGetRowCount(&(rgReqAftArray.rrArrayHandle), 
								 rgReqAftArray.crArrayName, &llRow );
			dbg ( DEBUG, "EvaluateFlight: %d flights found for UAFT <%s>", llRow, pcpUrnoList );
		}
		ilRC = EvaluateRules ( &rgReqAftArray, 0 );
		dbg ( TRACE, "EvaluateFlight: EvaluateRules returned RC <%d>", ilRC );
	}
	return ilRC;
}

/*  IniPriority: initialise logical field PRIO of rules array				*/
/*	IN:		pcpUrno:	URNO of rule, can be NULL for all					*/
static int IniPriority ( char * pcpUrno )
{
	char *pclResult=0;
	long llRow;
	char clPrio[COND_COUNT];
	char clValue[21];
	long llFieldNo = -1;
	int  i, ilRC = RC_SUCCESS;

	dbg ( TRACE, "IniPriority: Start pcpUrno <%s>", pcpUrno );

	llRow = ARR_FIRST;
	while ( CEDAArrayFindRowPointer( &(rgSrhArray.rrArrayHandle),
									 rgSrhArray.crArrayName,
									 &(rgSrhArray.rrIdxHandle[0]),
									 rgSrhArray.crIdxName[0],
									 pcpUrno, &llRow, (void *) &pclResult) 
			== RC_SUCCESS )
	{
		memset ( clPrio, 0, sizeof(clPrio) );
		for ( i=0; i<COND_COUNT; i++ )
		{
			if ( rgCndFldIdxSgl[i] >= 0 )
			{		
				strcpy ( clValue, SRHFIELD(pclResult, rgCndFldIdxSgl[i]) );
				if ( clValue[0] && (clValue[0]!=' ') )
				{	
					clPrio[i] = '3';
					continue;
				}
			}
			if ( rgCndFldIdxGrp[i] >= 0 )
			{		
				strcpy ( clValue, SRHFIELD(pclResult, rgCndFldIdxGrp[i]) );
				if ( clValue[0] && (clValue[0]!=' ') )
				{	
					clPrio[i] = '2';
					continue;
				}
			}
			clPrio[i] = '0';
		}
		dbg ( DEBUG, "IniPriority: URNO <%s> PRIO <%s>",
			  SRHFIELD(pclResult,igSrhUrnoIdx), clPrio );
		ilRC = CEDAArrayPutField( &rgSrhArray.rrArrayHandle,
								rgSrhArray.crArrayName, &llFieldNo, "PRIO", 
								llRow, clPrio );	
		if ( ilRC != RC_SUCCESS )
		{
			dbg ( TRACE, "IniPriority: CEDAArrayPutField failed RC <%d>", ilRC );
			break;
		}
		llRow = ARR_NEXT;

	}
	ilRC = CEDAArrayWriteDB(&rgSrhArray.rrArrayHandle, rgSrhArray.crArrayName,
							NULL,NULL,NULL,NULL,ARR_COMMIT_ALL_OK);

	dbg ( TRACE, "IniPriority: End ilRC <%d>", ilRC );

	return ilRC;
}



static int InitFieldIndices()
{
	int ilRc = RC_SUCCESS;
	int ilCol,ilPos, i, ilRealFields ;
	char clFina1[11], clFina2[11];

	for ( i=0; i<COND_COUNT; i++ )
	{
		rgCndFldIdxSgl[i] = rgCndFldIdxGrp[i] = -1;
		get_real_item(clFina1,SRH_COND_SINGLE,i+1);
		FindItemInList( SRH_FIELDS, clFina1, ',', &(rgCndFldIdxSgl[i]), &ilCol,&ilPos);

		get_real_item(clFina2,SRH_COND_GROUP,i+1);
		FindItemInList( SRH_FIELDS, clFina2, ',', &(rgCndFldIdxGrp[i]), &ilCol,&ilPos);

		dbg ( DEBUG, "InitFieldIndices: SRH: <%s> <%d>  <%s> <%d>", clFina1, 
			  rgCndFldIdxSgl[i], clFina2, rgCndFldIdxGrp[i] );

		rgAftInbFldIdx[i] = rgAftOutFldIdx[i] = -1;
		get_real_item(clFina1,AFT_INB_FIELD,i+1);
		FindItemInList( AFT_FIELDS, clFina1, ',', &(rgAftInbFldIdx[i]), &ilCol,&ilPos);

		get_real_item(clFina2,AFT_OUTB_FIELD,i+1);
		FindItemInList( AFT_FIELDS, clFina2, ',', &(rgAftOutFldIdx[i]), &ilCol,&ilPos);

		dbg ( DEBUG, "InitFieldIndices: AFT: <%s> <%d>  <%s> <%d>", clFina1, 
			  rgAftInbFldIdx[i], clFina2, rgAftOutFldIdx[i] );
	
	}
	for ( i=0; i<IN_TIME_CNT; i++ )
	{
		get_real_item(clFina1,INBOUNDTIMES,i+1);
		FindItemInList( AFT_FIELDS, clFina1, ',', &(rgInbTimeIdx[i]), &ilCol,&ilPos);
		dbg ( DEBUG, "InitFieldIndices: INBOUND TIME: <%s> Idx <%d> ", clFina1, rgInbTimeIdx[i] );
	}
	for ( i=0; i<OUT_TIME_CNT; i++ )
	{
		get_real_item(clFina1,OUTBOUNDTIMES,i+1);
		FindItemInList( AFT_FIELDS, clFina1, ',', &(rgOutbTimeIdx[i]), &ilCol,&ilPos);
		dbg ( DEBUG, "InitFieldIndices: OUTBOUND TIME: <%s> Idx <%d> ", clFina1, rgOutbTimeIdx[i] );
	}

    FindItemInList(SRH_FIELDS,"ADID",',',&igSrhAdidIdx,&ilCol,&ilPos);
    FindItemInList(SRH_FIELDS,"MAXT",',',&igSrhMaxtIdx,&ilCol,&ilPos);
   	FindItemInList(SRH_FIELDS,"NAME",',',&igSrhNameIdx,&ilCol,&ilPos);
    FindItemInList(SRH_FIELDS,"UHSS",',',&igSrhUhssIdx,&ilCol,&ilPos);
    FindItemInList(SRH_FIELDS,"URNO",',',&igSrhUrnoIdx,&ilCol,&ilPos);

	FindItemInList(SDE_FIELDS,"AFTF",',',&igSdeAftfIdx,&ilCol,&ilPos);
	FindItemInList(SDE_FIELDS,"FLDR",',',&igSdeFldrIdx,&ilCol,&ilPos);
	FindItemInList(SDE_FIELDS,"RETY",',',&igSdeRetyIdx,&ilCol,&ilPos);
	FindItemInList(SDE_FIELDS,"SDEU",',',&igSdeSdeuIdx,&ilCol,&ilPos);
	FindItemInList(SDE_FIELDS,"TIFR",',',&igSdeTifrIdx,&ilCol,&ilPos);
	FindItemInList(SDE_FIELDS,"URNO",',',&igSdeUrnoIdx,&ilCol,&ilPos);
	FindItemInList(SDE_FIELDS,"USRH",',',&igSdeUsrhIdx,&ilCol,&ilPos);
	FindItemInList(SDE_FIELDS,"UTPL",',',&igSdeUtplIdx,&ilCol,&ilPos);
	ilRealFields = get_no_of_items(SDE_FIELDS);
	igSdeAdidIdx = ilRealFields+1 ;
	igSdeLevlIdx = igSdeAdidIdx+1 ;


	FindItemInList(OST_FIELDS,"CONA",',',&igOstConaIdx,&ilCol,&ilPos);
	FindItemInList(OST_FIELDS,"CONS",',',&igOstConsIdx,&ilCol,&ilPos);
	FindItemInList(OST_FIELDS,"CONU",',',&igOstConuIdx,&ilCol,&ilPos);
	FindItemInList(OST_FIELDS,"COTY",',',&igOstCotyIdx,&ilCol,&ilPos);
	FindItemInList(OST_FIELDS,"RETY",',',&igOstRetyIdx,&ilCol,&ilPos);
	FindItemInList(OST_FIELDS,"RFLD",',',&igOstRfldIdx,&ilCol,&ilPos);
	FindItemInList(OST_FIELDS,"RTAB",',',&igOstRtabIdx,&ilCol,&ilPos);
	FindItemInList(OST_FIELDS,"RURN",',',&igOstRurnIdx,&ilCol,&ilPos);
	FindItemInList(OST_FIELDS,"SDAY",',',&igOstSdayIdx,&ilCol,&ilPos);
	FindItemInList(OST_FIELDS,"UAFT",',',&igOstUaftIdx,&ilCol,&ilPos);
	FindItemInList(OST_FIELDS,"URNO",',',&igOstUrnoIdx,&ilCol,&ilPos);
	FindItemInList(OST_FIELDS,"USDE",',',&igOstUsdeIdx,&ilCol,&ilPos);
	FindItemInList(OST_FIELDS,"UHSS",',',&igOstUhssIdx,&ilCol,&ilPos);
	FindItemInList(OST_FIELDS,"USRH",',',&igOstUsrhIdx,&ilCol,&ilPos);

	ilRealFields = get_no_of_items(OST_FIELDS);
	FindItemInList(OST_ADDFIELDS,"LEVL",',',&igOstLevlIdx,&ilCol,&ilPos);
	igOstLevlIdx += ilRealFields;
	FindItemInList(OST_ADDFIELDS,"TIFR",',',&igOstTifrIdx,&ilCol,&ilPos);
	igOstTifrIdx += ilRealFields;
	FindItemInList(OST_ADDFIELDS,"UTPL",',',&igOstUtplIdx,&ilCol,&ilPos);
	igOstUtplIdx += ilRealFields;
	FindItemInList(OST_ADDFIELDS,"AFTF",',',&igOstAftfIdx,&ilCol,&ilPos);
	igOstAftfIdx += ilRealFields;
	FindItemInList(OST_ADDFIELDS,"RETY",',',&igOstRetyIdx,&ilCol,&ilPos);
	igOstRetyIdx += ilRealFields;
	FindItemInList(OST_ADDFIELDS,"FLT2",',',&igOstFlt2Idx,&ilCol,&ilPos);
	igOstFlt2Idx += ilRealFields;
	
	FindItemInList(AFT_FIELDS,"URNO",',',&igAftUrnoIdx,&ilCol,&ilPos);
	FindItemInList(AFT_FIELDS,"ADID",',',&igAftAdidIdx,&ilCol,&ilPos);
	FindItemInList(AFT_FIELDS,"STOA",',',&igAftStoaIdx,&ilCol,&ilPos);
	FindItemInList(AFT_FIELDS,"STOD",',',&igAftStodIdx,&ilCol,&ilPos);
	FindItemInList(AFT_FIELDS,"FLNO",',',&igAftFlnoIdx,&ilCol,&ilPos);
	FindItemInList(AFT_FIELDS,"RKEY",',',&igAftRkeyIdx,&ilCol,&ilPos);
	FindItemInList(AFT_FIELDS,"TIFA",',',&igAftTifaIdx,&ilCol,&ilPos);
	FindItemInList(AFT_FIELDS,"TIFD",',',&igAftTifdIdx,&ilCol,&ilPos);

	FindItemInList(AF1_FIELDS,"FCA1",',',&igAf1Fca1Idx,&ilCol,&ilPos);
	FindItemInList(AF1_FIELDS,"FCA2",',',&igAf1Fca2Idx,&ilCol,&ilPos);
	FindItemInList(AF1_FIELDS,"FLNU",',',&igAf1FlnuIdx,&ilCol,&ilPos);

	FindItemInList(CCA_FIELDS,"CKBA",',',&igCcaCkbaIdx,&ilCol,&ilPos);
	FindItemInList(CCA_FIELDS,"CKEA",',',&igCcaCkeaIdx,&ilCol,&ilPos);
	FindItemInList(CCA_FIELDS,"FLNU",',',&igCcaFlnuIdx,&ilCol,&ilPos);
	FindItemInList(CCA_FIELDS,"CKBS",',',&igCcaCkbsIdx,&ilCol,&ilPos);
	FindItemInList(CCA_FIELDS,"CKES",',',&igCcaCkesIdx,&ilCol,&ilPos);

	FindItemInList(JOB_FIELDS,"ACFR",',',&igJobAcfrIdx,&ilCol,&ilPos);
	FindItemInList(JOB_FIELDS,"ACTO",',',&igJobActoIdx,&ilCol,&ilPos);
	FindItemInList(JOB_FIELDS,"STAT",',',&igJobStatIdx,&ilCol,&ilPos);
	FindItemInList(JOB_FIELDS,"UAFT",',',&igJobUaftIdx,&ilCol,&ilPos);
	FindItemInList(JOB_FIELDS,"UTPL",',',&igJobUtplIdx,&ilCol,&ilPos);
	FindItemInList(JOB_FIELDS,"PLFR",',',&igJobPlfrIdx,&ilCol,&ilPos);
	FindItemInList(JOB_FIELDS,"PLTO",',',&igJobPltoIdx,&ilCol,&ilPos);

	FindItemInList(EVAL_FIELDS,"OURI",',',&igEvalOuriIdx,&ilCol,&ilPos);
	FindItemInList(EVAL_FIELDS,"OURO",',',&igEvalOuroIdx,&ilCol,&ilPos);
	FindItemInList(EVAL_FIELDS,"SRHI",',',&igEvalSrhiIdx,&ilCol,&ilPos);
	FindItemInList(EVAL_FIELDS,"SRHO",',',&igEvalSrhoIdx,&ilCol,&ilPos);
	FindItemInList(EVAL_FIELDS,"UHSS",',',&igEvalUhssIdx,&ilCol,&ilPos);
}


/*  CheckSingleValue: check if a value and an URNO belong to the same record*/
/*	IN:		prpBasicData:	Basic data array regarded						*/
/*			pcpReqUrno:		URNO of basic data record						*/
/*			pcpActValue:	value to be tested (code field)					*/
/*	return:	TRUE if code field of record identified by URNO is pcpActValue	*/
static BOOL CheckSingleValue ( ARRAYINFO* prpBasicData, char *pcpReqUrno,
							   char * pcpActValue )
{
	long llRow=ARR_FIRST;
	BOOL blFound = FALSE;
	char *pclResult;
	char clBasicVal[101];
	
	if ( !pcpReqUrno || !pcpReqUrno[0] || (pcpReqUrno[0]==' ') )
		return TRUE;		/*  condition empty */
	
	/* get code of requested URNO pcpReqUrno from prpBasicData */
	while ( !blFound &&
		    ( CEDAArrayFindRowPointer(&(prpBasicData->rrArrayHandle),
									  prpBasicData->crArrayName,
									  &(prpBasicData->rrIdxHandle[0]),
									  prpBasicData->crIdxName[0], pcpReqUrno,
									  &llRow, (void*)&pclResult)  == RC_SUCCESS ) )
	{
		strcpy ( clBasicVal, &(pclResult[prpBasicData->plrArrayFieldOfs[1]]) );
		/* compare acutal code and code requested */
		if ( !strcmp ( pcpActValue, clBasicVal ) )
		{
			blFound = TRUE;
			dbg ( DEBUG, "CheckSingleValue: Actual <%s> == Requested <%s>", 
				  pcpActValue, clBasicVal );
		}
		else
			dbg ( DEBUG, "CheckSingleValue: Actual <%s> != Requested <%s>", 
				  pcpActValue, clBasicVal );
		llRow = ARR_NEXT;
	}
	dbg ( DEBUG, "CheckSingleValue: Actual <%s> Req. URNO <%s> Result <%d>", 		
		  pcpActValue, pcpReqUrno, blFound );

	return blFound ;
}

/*  CheckGroupValue: check if a value is in a static group					*/
/*	IN:		prpBasicData:	Basic data array regarded						*/
/*			pcpReqGrpUrno:	URNO of static group							*/
/*			pcpActValue:	value to be tested (code field)					*/
/*	return:	TRUE if value is in group, else FALSE							*/
static BOOL CheckGroupValue ( ARRAYINFO* prpBasicData, char *pcpReqGrpUrno,
							   char * pcpActValue )
{
	long llRow=ARR_FIRST;
	long llSgmRow=ARR_FIRST;
	BOOL blFound = FALSE;
	char *pclResult, pclSgmRowPtr;
	char clActUrno[21],clKey[41];
	
	if ( !pcpReqGrpUrno || !pcpReqGrpUrno[0] || (pcpReqGrpUrno[0]==' ') )
		return TRUE;		/*  condition empty */
	
	/* get URNO of actual value clActUrno from prpBasicData */
	while ( !blFound &&
		    ( CEDAArrayFindRowPointer(&(prpBasicData->rrArrayHandle),
									  prpBasicData->crArrayName,
									  &(prpBasicData->rrIdxHandle[1]),
									  prpBasicData->crIdxName[1], pcpActValue,
									  &llRow, (void*)&pclResult)  == RC_SUCCESS ) )
	{
		strcpy ( clActUrno, &(pclResult[prpBasicData->plrArrayFieldOfs[0]]) ); 
		/* search in rgSgmArray for key pcpReqGrpUrno,clActUrno */
		sprintf ( clKey, "%s,%s", pcpReqGrpUrno, clActUrno );
		if ( CEDAArrayFindRowPointer(&(rgSgmArray.rrArrayHandle),
									  rgSgmArray.crArrayName,
									  &(rgSgmArray.rrIdxHandle[1]),
									  rgSgmArray.crIdxName[1], clKey,
									  &llSgmRow, (void*)&pclSgmRowPtr)  == RC_SUCCESS )
		{
			blFound = TRUE;
			dbg ( DEBUG, "CheckGroupValue: Found key <%s> in SGM-Array", clKey );
		}
		else
			dbg ( DEBUG, "CheckGroupValue: Key <%s> not found in SGM-Array", clKey );
		llRow = ARR_NEXT;
	}
	dbg ( DEBUG, "CheckGroupValue: Actual <%s> Req.group URNO <%s> Result <%d>", 		
		  pcpActValue, pcpReqGrpUrno, blFound );

	return blFound ;
}		
	

static int SetAATArrayInfo ( char *pcpArrayName,char *pcpArrayFieldList, 
						     long *plpFieldLens, ARRAYINFO *prpArrayInfo )
{
	int	ilRc   = RC_SUCCESS;				/* Return code */
	int ilLoop = 0;

	if(prpArrayInfo == NULL)
	{
		dbg(TRACE,"SetAATArrayInfo: invalid last parameter : null pointer not valid");
		return  RC_FAIL;
	}
	
	memset(prpArrayInfo,0x00,sizeof(prpArrayInfo));

	prpArrayInfo->rrArrayHandle = -1;
	for ( ilLoop=0; ilLoop<ARR_MAX_IDX; ilLoop++ )
		prpArrayInfo->rrIdxHandle[ilLoop] = -1;

	if( pcpArrayName && (strlen(pcpArrayName) <= ARR_NAME_LEN) )
		strcpy(prpArrayInfo->crArrayName,pcpArrayName);
	
	if(pcpArrayFieldList && (strlen(pcpArrayFieldList) <= ARR_FLDLST_LEN) )
		strcpy(prpArrayInfo->crArrayFieldList,pcpArrayFieldList);

	if(ilRc == RC_SUCCESS)
	{
		prpArrayInfo->lrArrayFieldCnt = get_no_of_items(pcpArrayFieldList);

		prpArrayInfo->plrArrayFieldLen = (long *) calloc(prpArrayInfo->lrArrayFieldCnt,sizeof(long));
		prpArrayInfo->plrArrayFieldOfs = (long *) calloc(prpArrayInfo->lrArrayFieldCnt,sizeof(long));

		if( prpArrayInfo->plrArrayFieldOfs == NULL )
		{
			dbg(TRACE,"SetAATArrayInfo: plrArrayFieldOfs calloc(%d,%d) failed",prpArrayInfo->lrArrayFieldCnt,sizeof(long));
			ilRc = RC_FAIL;
		}/* end of if */
		else
		{
			prpArrayInfo->plrArrayFieldOfs[0] = 0;
			for(ilLoop = 1 ; ilLoop < prpArrayInfo->lrArrayFieldCnt ; ilLoop++)
			{
				prpArrayInfo->plrArrayFieldOfs[ilLoop] = prpArrayInfo->plrArrayFieldOfs[ilLoop -1] + (plpFieldLens[ilLoop-1] +1);
			}/* end of for */
			for(ilLoop = 0 ; ilLoop < prpArrayInfo->lrArrayFieldCnt ; ilLoop++)
				prpArrayInfo->plrArrayFieldLen[ilLoop] = plpFieldLens[ilLoop];
		}
	}/* end of if */

	if(ilRc == RC_SUCCESS)
	{
		ilRc = AATArrayCreate( &(prpArrayInfo->rrArrayHandle), pcpArrayName,
							   TRUE, 100, prpArrayInfo->lrArrayFieldCnt,
							   plpFieldLens, pcpArrayFieldList, NULL );
		if(ilRc != RC_SUCCESS)
			dbg(TRACE,"SetAAtArrayInfo: CEDAArrayInitialize failed <%d>",ilRc);
	}/* end of if */

   	if(ilRc == RC_SUCCESS)
   	{
		ilRc = GetTotalRowLength( pcpArrayFieldList, prpArrayInfo, &(prpArrayInfo->lrArrayRowLen) );
       	if(ilRc != RC_SUCCESS)
           	dbg(TRACE,"SetAATArrayInfo: GetTotalRowLength failed <%s> <%s>",pcpArrayName,pcpArrayFieldList);
   	}/* end of if */

   	if(ilRc == RC_SUCCESS)
   	{
		prpArrayInfo->lrArrayRowLen++;
		prpArrayInfo->pcrArrayRowBuf = (char *) calloc(1,(prpArrayInfo->lrArrayRowLen+1));
   		if(prpArrayInfo->pcrArrayRowBuf == NULL)
   		{
      		ilRc = RC_FAIL;
      		dbg(TRACE,"SetAATArrayInfo: calloc failed");
   		}/* end of if */
   	}/* end of if */


	if ( (ilRc == RC_SUCCESS) && strstr(pcpArrayFieldList,"URNO") )
	{
		strcpy ( prpArrayInfo->crIdxName[0], "URNOIDX" );
		ilRc = CEDAArrayCreateSimpleIndexDown(&(prpArrayInfo->rrArrayHandle),
											  prpArrayInfo->crArrayName,
											  &(prpArrayInfo->rrIdxHandle[0]), 
											  prpArrayInfo->crIdxName[0], "URNO" );
		if(ilRc != RC_SUCCESS)                                             
		{
			dbg( TRACE,"SetAAtArrayInfo: CEDAArrayCreateSimpleIndexDown for %s failed <%d>",
				 pcpArrayName, ilRc);		
		}
	}

	return(ilRc);
	
} /* end of SetAAtArrayInfo */


/*  CreateSkeleton: create new status in NEWOST for results in rgEvalResults	*/
/*	IN:		prpFlightArray: array where flight data should be taken from		*/
static int CreateSkeleton ( ARRAYINFO* prpFlightArray )
{
	int		ilRc = RC_SUCCESS, ilRc1 = RC_SUCCESS;
	long	llEvalRow = ARR_FIRST;
	char	*pclResult;
	char	clOuri[21], clOuro[21], clUsrhI[21], clUsrhO[21], clUhss[21];

	CEDAArrayDelete ( &(rgNewOstArr.rrArrayHandle), rgNewOstArr.crArrayName );
	while ( CEDAArrayGetRowPointer( &(rgEvalResults.rrArrayHandle),
									rgEvalResults.crArrayName,
									llEvalRow, (void *)&pclResult) == RC_SUCCESS )
	{	/*  fields of pclResult "UAFT,UHSS,USRH",  SDE-Idx: "USRH,URNO" */
		strcpy ( clOuri, EVALFIELD(pclResult,igEvalOuriIdx) ); 
		strcpy ( clOuro, EVALFIELD(pclResult,igEvalOuroIdx) );
		strcpy ( clUsrhI, EVALFIELD(pclResult,igEvalSrhiIdx) );
		strcpy ( clUsrhO, EVALFIELD(pclResult,igEvalSrhoIdx) );
		strcpy ( clUhss, EVALFIELD(pclResult,igEvalUhssIdx) );
		
		if ( !IS_EMPTY (clOuri) && IS_EMPTY (clUsrhI) )
		{
			dbg ( DEBUG, "CreateSkeleton: No rule for inbound <%s> in section <%s>", 
				  clOuri, clUhss );	
		}
		if ( !IS_EMPTY (clOuro) && IS_EMPTY (clUsrhO) )
		{
			dbg ( DEBUG, "CreateSkeleton: No rule for outbound <%s> in section <%s>", 
				  clOuro, clUhss );	
		}

		if ( !IS_EMPTY (clUsrhI) || !IS_EMPTY (clUsrhO) )
		{	/* at least one rule found for rotation/flight */
			if ( !strcmp ( clUsrhI,clUsrhO )  )
			{
				dbg ( DEBUG, "CreateSkeleton: Going to apply Turnaround Rule <%s>", clUsrhI );
				ilRc1 = CreateSkeleton4Rule ( prpFlightArray, clUsrhI, clOuri, clOuro, clUhss );
				if ( ilRc1 != RC_SUCCESS )
				{
					dbg ( TRACE, "CreateSkeleton: CreateSkeleton4Rule TURN <%s><%s> USRH <%s> failed RC <%d>",
						 clOuri, clOuro, clUsrhI, ilRc1 );
					/* ilRc = RC_FAIL; */
				}
			}
			else
			{
				if ( !IS_EMPTY (clUsrhI) )
				{
					dbg ( DEBUG, "CreateSkeleton: Going to apply inbound Rule <%s>", clUsrhI );
					ilRc1 = CreateSkeleton4Rule ( prpFlightArray, clUsrhI, clOuri, "", clUhss );
					if ( ilRc1 != RC_SUCCESS )
					{
						dbg ( TRACE, "CreateSkeleton: CreateSkeleton4Rule INBOUND <%s> USRH <%s> failed RC <%d>",
							 clOuri, clUsrhI, ilRc1 );
						/* ilRc = RC_FAIL; */
					}
				}
				if ( !IS_EMPTY (clUsrhO) )
				{
					dbg ( DEBUG, "CreateSkeleton: Going to apply outbound Rule <%s>", clUsrhI );
					ilRc1 = CreateSkeleton4Rule ( prpFlightArray, clUsrhO, "", clOuro, clUhss );
					if ( ilRc1 != RC_SUCCESS )
					{
						dbg ( TRACE, "CreateSkeleton: CreateSkeleton4Rule OUTBOUND <%s> USRH <%s> failed RC <%d>",
							 clOuro, clUsrhO, ilRc1 );
						/* ilRc = RC_FAIL; */
					}
				}
			}
		}
		llEvalRow = ARR_NEXT;
	}

	return ilRc;
}

/********************************************************************/
static int GetNextUrno(char *pcpUrno)
{
	int ilRC = RC_SUCCESS ;
	char  pclDataArea[IDATA_AREA_SIZE] ;
	
	if (igReservedUrnoCnt <= 0)
	{                            /* hole Urno buffer -> get new Urnos */
		memset( pclDataArea, 0, IDATA_AREA_SIZE) ;
		if ((ilRC = GetNextValues(pclDataArea, URNOS_TO_FETCH)) != RC_SUCCESS)
		{
			dbg ( TRACE,"GetNextValues failed RC <%d>", ilRC );
		}
		else
		{
			strcpy ( pcpUrno, pclDataArea );
			igReservedUrnoCnt = URNOS_TO_FETCH - 1 ;
			lgActUrno = atol(pcpUrno);
			dbg ( DEBUG, "GetNextUrno: fetched %d Urnos. Next is <%s>", 
				  URNOS_TO_FETCH, pcpUrno ) ;
		}
	}
	else
	{
		igReservedUrnoCnt-- ;
		lgActUrno++ ;
		sprintf ( pcpUrno, "%ld", lgActUrno );      /* get next reserved Urno */
		dbg ( DEBUG, "GetNextUrno: Next Urno <%s>, %d Urnos remaining", 
			  pcpUrno, igReservedUrnoCnt ) ;
	}
	
	return (ilRC) ;
} /* getNextUrno () */

void TrimRight(char *s)
{
    int i = 0;    /* search for last non-space character */
    for (i = strlen(s) - 1; i >= 0 && isspace(s[i]); i--); /* trim off right spaces */    
	s[++i] = '\0';
}

/*  FillOstRecord: initialise new status record with data already known		*/
/*		   i.e. CDAT,RFLD,RTAB,RURN,SDAY,UAFT,UHSS,USDE,USEC,USRH			*/
/*	IN:	prpFlightArray: array where flight data should be taken from		*/
/*		pcpBuf:		result buffer											*/
/*		ipBufLen:	length of result buffer									*/
/*		pcpSdePtr:	row of used status definition, must not be NULL			*/
/*		pcpOuri:	inbound-URNO, must not be NULL, if "" single outbound	*/
/*		pcpUaft:	outbound-URNO, must not be NULL, if "" single inbound	*/
/*		pcpUhss:	URNO of handling status sect., must not be NULL			*/	
/*	OUT:pcpBuf:		result buffer filled									*/
static int FillOstRecord (ARRAYINFO* prpFlightArray, char *pcpBuf, int ipBufLen,
						  char *pcpSdePtr, char *pcpOuri, char *pcpOuro, char *pcpUhss)
{	/* "CDAT,CONA,CONS,COTY,RFLD,RTAB,RURN,SDAY,UAFT,UHSS,URNO,USDE,USEC,USRH"*/
	char	clNow[21], *pclFlight, clSday[21], clFldr[11]; 
	char	clRtab[4], clRurn[11], clUaft[11], *pclAftf, *pclRety; 
	long	llAftRow = ARR_FIRST;
	int		ilRc, ilIdx=-1, ilPos, ilCol;
	
	if ( !pcpOuri || !pcpOuro || !pcpUhss || !pcpBuf || !pcpSdePtr )
	{
		dbg ( TRACE, "FillOstRecord: At least one parameter is NULL" );
		return RC_INVALID;
	}
	strcpy ( clFldr, SDEFIELD(pcpSdePtr,igSdeFldrIdx) );
	pclAftf = SDEFIELD(pcpSdePtr,igSdeAftfIdx);
	pclRety = SDEFIELD(pcpSdePtr,igSdeRetyIdx);
	TrimRight ( clFldr );
	strcpy ( clRurn, " " );

	/*  set RTAB */
	strcpy ( clRtab, "AFT" );
	if ( strcmp ( clFldr, "PLAN" ) == 0 )
	{
		strncpy ( clRtab, pclRety, 3 );
		clRtab[3] = '\0';
		ilRc = GetNomField ( pclAftf, clFldr, pclRety );
		if ( ilRc != RC_SUCCESS )
			dbg ( TRACE, "FillOstRecord: GetNomField for AFTF <%s> RETY <%s> failed RC <%d>", 
						 pclAftf, pclRety, ilRc );

	}
	if ( strcmp ( clFldr, "CONA" ) == 0 )
	{
		strcpy ( clRtab, "OST" );
	}
	
	/*  SET UAFT to the flight responsible for actual value (CONA) */
	if ( strncmp ( pclRety, "AFT", 3 ) == 0 )
	{
		FindItemInList( AFT_INBOUND_FIELDS, pclAftf, ',', &ilIdx, &ilCol,&ilPos);
		if ( ilIdx > 0 )
			strcpy ( clUaft, pcpOuri );
		else
			strcpy ( clUaft, pcpOuro );
	}			
	else if ( strncmp ( pclRety, "JOB", 3 ) == 0 )
	{
		if ( *SDEFIELD(pcpSdePtr,igSdeAdidIdx) == 'A' )
			strcpy ( clUaft, pcpOuri );
		else
			strcpy ( clUaft, pcpOuro );

	}
	else if ( strncmp ( pclRety, "CCA", 3 ) == 0 )
	{
		strcpy ( clUaft, pcpOuro );
	}
	
	/*  SET RURN to the flight responsible for scheduled value (CONS) */
	/*  if FLDR='CONA' RURN is the URNO of predecessor OSTTAB-record -> filled later */
	if ( strcmp ( clFldr, "CONA" ) != 0 )
	{
		if ( strcmp ( clFldr, "PLAN" ) == 0 )
			strcpy ( clRurn, clUaft );
		else
		{
			FindItemInList( INBOUNDTIMES, clFldr, ',', &ilIdx, &ilCol,&ilPos);
			if ( ilIdx > 0 )
				strcpy ( clRurn, pcpOuri );
			else
				strcpy ( clRurn, pcpOuro );

		}
	}

	GetServerTimeStamp("UTC", 1, 0, clNow);

	memset ( pcpBuf, ' ', ipBufLen );

	if ( IS_EMPTY(clUaft) )
	{
		dbg ( TRACE, "FillOstRecord: Failure UAFT is empty, USDE <%s> OURI <%s>  OURO <%s>", 
			  SDEFIELD(pcpSdePtr,igSdeUrnoIdx), pcpOuri, pcpOuro );
		return RC_FAILURE;
	}
	ilRc = CEDAArrayFindRowPointer(	&(prpFlightArray->rrArrayHandle),
									prpFlightArray->crArrayName,
									&(prpFlightArray->rrIdxHandle[0]),
									prpFlightArray->crIdxName[0], clUaft, 
									&llAftRow, (void *)&pclFlight ) ;
	if ( ilRc != RC_SUCCESS )
	{
		dbg ( TRACE, "FillOstRecord: Flight <%s> not found", clUaft );
	}
	else
	{
		if ( *AFTFIELD(pclFlight,igAftAdidIdx) == 'D' )
			strcpy ( clSday, AFTFIELD(pclFlight,igAftTifdIdx) );
		else
			strcpy ( clSday, AFTFIELD(pclFlight,igAftTifaIdx) );
		clSday[8] = '\0';
									
		/* "CDAT,CONA,CONS,COTY,RFLD,RTAB,RURN,SDAY,UAFT,UHSS,URNO,USDE,USEC,USRH"*/
		sprintf ( pcpBuf, "%s, ,UNDEF, ,%s,%s,%s,%s,%s,%s, ,%s,STAHDL,%s",
				  clNow, clFldr, clRtab, clRurn, clSday, clUaft, pcpUhss, 
				  SDEFIELD(pcpSdePtr,igSdeUrnoIdx), SDEFIELD(pcpSdePtr,igSdeUsrhIdx) );
		strcat ( pcpBuf, ", , , , , , " );  /* empty logic fields */
		delton ( pcpBuf );
	}
	return ilRc;
}


/*  UpdateOSTTAB: update OSTTAB from NEWOST array after rule evaluation		*/
static int UpdateOSTTAB ( ARRAYINFO* prpFlightArray )
{
	int		ilRc = RC_SUCCESS, ilRc1 = RC_SUCCESS, ilUsed;
	long	llEvalRow = ARR_FIRST, llSdeRow, llOldRow, llNewRow;
	char	*pclResult, *pclOstOld, *pclOstNew, *pclSdePtr, clUhss[21];
	char	clKeyOld[31], clKeyNew[41],clUsde[21], clUrno[21];
	char	clOuri[21], clOuro[21];
 	char	*pclUaft, *pclUsrh, *pclAftTmp;
	long	llFieldNo = -1, llCount;
	int		ilAdded=0, ilDeleted=0, ilKept=0, i;

	while ( CEDAArrayGetRowPointer( &(rgEvalResults.rrArrayHandle),
									rgEvalResults.crArrayName,
									llEvalRow, (void *)&pclResult) == RC_SUCCESS )
	{	/*  fields of pclResult "UAFT,UHSS,USRH",  OST-Idx: "UAFT,UHSS" */
		strcpy ( clOuri, EVALFIELD(pclResult,igEvalOuriIdx) ); 
		strcpy ( clOuro, EVALFIELD(pclResult,igEvalOuroIdx) );
		strcpy ( clUhss, EVALFIELD(pclResult,igEvalUhssIdx) );
		
		for ( i=0; i<2; i++ )
		{
			if ( i==0 )
				pclUaft = clOuri;
			else
				pclUaft = clOuro;
			if ( !IS_EMPTY ( pclUaft )  )
			{
				sprintf ( clKeyOld, "%s,%s", pclUaft, clUhss ); 
				dbg ( DEBUG, "UpdateOSTTAB: Search old Status with key <%s>", clKeyOld );	
				llOldRow = ARR_LAST;
				while ( CEDAArrayFindRowPointer(&(rgOstArray.rrArrayHandle),
												rgOstArray.crArrayName,
												&(rgOstArray.rrIdxHandle[1]),
												rgOstArray.crIdxName[1],clKeyOld,
												&llOldRow, (void*)&pclOstOld ) == RC_SUCCESS )
				{
					strcpy ( clUsde, OSTFIELD(pclOstOld, igOstUsdeIdx) );
					dbg ( DEBUG, "UpdateOSTTAB: Found old status USDE <%s> Row <%ld>", clUsde );
					sprintf ( clKeyNew, "%s,%s", pclUaft, clUsde ); 
					dbg ( DEBUG, "UpdateOSTTAB: Search in new status with key <%s>", clKeyNew );
					
					llNewRow = ARR_FIRST;
					ilUsed = CEDAArrayFindRowPointer(&(rgNewOstArr.rrArrayHandle),
													 rgNewOstArr.crArrayName,
													 &(rgNewOstArr.rrIdxHandle[1]),
													 rgNewOstArr.crIdxName[1], clKeyNew,
													 &llNewRow, (void*)&pclOstNew );
					if ( ilUsed != RC_SUCCESS )
					{		
						ilRc1 = CEDAArrayDeleteRow ( &(rgOstArray.rrArrayHandle),  
													 rgOstArray.crArrayName, llOldRow );
						if ( ilRc1 != RC_SUCCESS )
						{
							dbg ( TRACE, "UpdateOSTTAB: CEDAArrayDeleteRow Row <%ld> of old status failed, RC <%d>",
								  llOldRow, ilRc1 );
							ilRc = RC_FAIL;
						}
						else
						{
							dbg ( TRACE, "UpdateOSTTAB: Old Status USDE <%s> is not used any longer", clUsde );
							ilDeleted++;
						}
					}
					else
					{
						/* TODO: possibly update old status row */
						ilRc1 = UpdateStatus ( llOldRow, pclOstOld, pclOstNew );
						if ( ilRc1 != RC_SUCCESS )
						{
							dbg ( TRACE, "UpdateOSTTAB: UpdateStatus Row <%ld> failed, RC <%d>", 
								  llOldRow, ilRc1 );
							ilRc = RC_FAIL;
						}
						ilRc1 = CEDAArrayDeleteRow ( &(rgNewOstArr.rrArrayHandle),  
													 rgNewOstArr.crArrayName, llNewRow );
						if ( ilRc1 != RC_SUCCESS )
						{
							dbg ( TRACE, "UpdateOSTTAB: CEDAArrayDeleteRow Row <%ld> of new status failed, RC <%d>",
								  llNewRow, ilRc1 );
							ilRc = RC_FAIL;
						}
						else
						{
							dbg ( TRACE, "UpdateOSTTAB: New Status USDE <%s> already in DB", clUsde );
							ilKept++;
						}
					}
					llOldRow = ARR_PREV;
				}
			}
		}
		llEvalRow = ARR_NEXT;
	}
	/*  if more than 100 new status have to be inserted, we better			*/
	/*	disactivate indices on OSTTAB										*/
	CEDAArrayGetRowCount(&(rgNewOstArr.rrArrayHandle),
						 rgNewOstArr.crArrayName,&llCount);
	if ( llCount > 100 )
	{
		dbg ( DEBUG, "UpdateOSTTAB: Going to disactivate indices on <OST>" );	
		for ( i=0; i<=2; i++ )
			if ( CEDAArrayDisactivateIndex (&(rgOstArray.rrArrayHandle),	
											&(rgOstArray.crArrayName[0]),
											&(rgOstArray.rrIdxHandle[i]),
											rgOstArray.crIdxName[i] ) != RC_SUCCESS )
				dbg ( TRACE, "UpdateOSTTAB: CEDAArrayDisactivateIndex Index <%d> failed", i );


	}
	/* copy all new status to rgOstArray and set URNO */
	llNewRow = ARR_FIRST;
	while ( CEDAArrayGetRowPointer( &(rgNewOstArr.rrArrayHandle),
									rgNewOstArr.crArrayName,
									llNewRow, (void *)&pclOstNew) == RC_SUCCESS )
	{	
		llOldRow = ARR_LAST;
		ilRc1 = CEDAArrayAddRow(&rgOstArray.rrArrayHandle, rgOstArray.crArrayName,
								&llOldRow, (void*)pclOstNew );
		if ( ilRc1 != RC_SUCCESS )
		{
			dbg ( TRACE, "UpdateOSTTAB: CEDAArrayAddRow for new status failed RC <%d>", ilRc1 );
		}
		else
		{
			ilRc1 = GetNextUrno ( clUrno );
			if ( ilRc1 != RC_SUCCESS )
				dbg ( TRACE, "UpdateOSTTAB: GetNextUrno failed RC <%d>", ilRc1 );
		}
		if ( ilRc1 == RC_SUCCESS )
		{
			ilRc1 = CEDAArrayPutField( &rgOstArray.rrArrayHandle,
									   rgOstArray.crArrayName, &llFieldNo, 
									   "URNO", llOldRow, clUrno );	
			if ( ilRc1 != RC_SUCCESS )
				dbg ( TRACE, "UpdateOSTTAB: CEDAArrayPutField URNO failed Row <%ld> RC <%d>", 
					  llOldRow, ilRc1 );
			else
				ilAdded++;
		}
		if ( ilRc1 != RC_SUCCESS )
			ilRc = RC_FAIL;
		llNewRow = ARR_NEXT;
	}
	
	dbg ( DEBUG, "UpdateOSTTAB: Going to activate indices on <OST> again" );	
	if ( llCount > 100 )
	{
		for ( i=0; i<=2; i++ )
			if ( CEDAArrayActivateIndex(&(rgOstArray.rrArrayHandle),	
										&(rgOstArray.crArrayName[0]),
										&(rgOstArray.rrIdxHandle[i]),
										rgOstArray.crIdxName[i] ) != RC_SUCCESS )
				dbg ( TRACE, "UpdateOSTTAB: CEDAArrayActivateIndex Index <%d> failed", i );
	}

	dbg ( TRACE, "UpdateOSTTAB: Going to set predecessors" );	
		
	/* TODO: Set RURN for all status with SDE.FLDR=="CONA" and set CONS */
	llFieldNo = -1;
	llEvalRow = ARR_FIRST;
	while ( CEDAArrayGetRowPointer( &(rgEvalResults.rrArrayHandle),
									rgEvalResults.crArrayName,
									llEvalRow, (void *)&pclResult) == RC_SUCCESS )
	{	/*  fields of pclResult "UAFT,UHSS,USRH",  OST-Idx: "UAFT,UHSS,RTAB" */
		strcpy ( clOuri, EVALFIELD(pclResult,igEvalOuriIdx) ); 
		strcpy ( clOuro, EVALFIELD(pclResult,igEvalOuroIdx) );
		strcpy ( clUhss, EVALFIELD(pclResult,igEvalUhssIdx) );
		
		for ( i=0; i<2; i++ )
		{
			if ( i==0 )
				pclUaft = clOuri;
			else
				pclUaft = clOuro;
			if ( !IS_EMPTY ( pclUaft )  )
			{
				sprintf ( clKeyOld, "%s,%s", pclUaft, clUhss );
											  
				dbg ( DEBUG, "UpdateOSTTAB: Search Status to update RURN with key <%s>", clKeyOld );	
				llOldRow = ARR_FIRST;
				/* Search all status which are related to a predecessor status */
				while ( CEDAArrayFindRowPointer(&(rgOstArray.rrArrayHandle),
												rgOstArray.crArrayName,
												&(rgOstArray.rrIdxHandle[1]),
												rgOstArray.crIdxName[1],clKeyOld,
												&llOldRow, (void*)&pclOstOld ) == RC_SUCCESS )
				{
					if ( strcmp ( OSTFIELD (pclOstOld, igOstRtabIdx), "OST" ) == 0 )
					{
						strcpy ( clUsde, OSTFIELD(pclOstOld, igOstUsdeIdx) );
						dbg ( DEBUG, "UpdateOSTTAB: Found status USDE <%s> Row <%ld>", clUsde );
						
						/* Search definition of this status */
						llSdeRow = ARR_FIRST;
						ilRc1 = CEDAArrayFindRowPointer(&(rgSdeArray.rrArrayHandle),
														 rgSdeArray.crArrayName,
														 &(rgSdeArray.rrIdxHandle[0]),
														 rgSdeArray.crIdxName[0], clUsde,
														 &llSdeRow, (void*)&pclSdePtr );
						if ( ilRc1 != RC_SUCCESS )
						{		
							dbg ( TRACE, "UpdateOSTTAB: SDE-record key <%s> not found RC <%d>", 
								  clUsde, ilRc1 ) ;
						}
						else
						{	/* IDXOST2 fields: "UAFT,USDE" */
							/* Search predecessor status in online status */
							llNewRow = ARR_FIRST;
							sprintf ( clKeyNew, "%s,%s,%s", pclUaft, OSTFIELD (pclOstOld, igOstUhssIdx),  
									  SDEFIELD(pclSdePtr,igSdeSdeuIdx) );
							dbg ( DEBUG, "UpdateOSTTAB: Search predecessor status using 1st key <%s>", clKeyNew );
							ilRc1 = CEDAArrayFindRowPointer(&(rgOstArray.rrArrayHandle),
															rgOstArray.crArrayName,
															&(rgOstArray.rrIdxHandle[2]),
															rgOstArray.crIdxName[2], clKeyNew,
															&llNewRow, (void*)&pclOstNew );
							if ( ilRc1 != RC_SUCCESS )
							{
								pclAftTmp = (i==0) ? clOuro : clOuri;
								if ( !IS_EMPTY(pclAftTmp ))
								{
									llNewRow = ARR_FIRST;
									sprintf ( clKeyNew, "%s,%s,%s", pclAftTmp, OSTFIELD (pclOstOld, igOstUhssIdx),  
																 SDEFIELD(pclSdePtr,igSdeSdeuIdx) );
									dbg ( DEBUG, "UpdateOSTTAB: Search predecessor status using 1st key <%s>", clKeyNew );
									ilRc1 = CEDAArrayFindRowPointer(&(rgOstArray.rrArrayHandle),
																	rgOstArray.crArrayName,
																	&(rgOstArray.rrIdxHandle[2]),
																	rgOstArray.crIdxName[2], clKeyNew,
																	&llNewRow, (void*)&pclOstNew );	
									dbg ( TRACE, "UpdateOSTTAB: predecessor status using key <%s> not found RC <%d>", 
												  clKeyNew, ilRc1 ) ;
								}
							}
						}
						if ( ilRc1 == RC_SUCCESS )
						{
							/*  Set URNO of predecessor as RURN */
							strcpy ( clUrno, OSTFIELD(pclOstNew, igOstUrnoIdx) ); 
							ilRc1 = CEDAArrayPutField( &rgOstArray.rrArrayHandle,
													   rgOstArray.crArrayName, &llFieldNo, 
													   "RURN", llOldRow, clUrno );	
							if ( ilRc1 != RC_SUCCESS )
								dbg ( TRACE, "UpdateOSTTAB: CEDAArrayPutField RURN <%s> failed Row <%ld> RC <%d>", 
									  clUrno, llOldRow, ilRc1 );
							else
								dbg ( DEBUG, "UpdateOSTTAB: CEDAArrayPutField RURN <%s> Row <%ld>  OK", 
									  clUrno, llOldRow );
						}
					}
					llOldRow = ARR_NEXT;
				}	/* endof while-loop rgOstArray */

				/*  Fill logical fields */
				ilRc1 = PrepareOSTArray ( pclUaft, clUhss );
				if ( ilRc1 != RC_SUCCESS )
					dbg ( TRACE, "UpdateOSTTAB: PrepareOSTArray UAFT <%s> UHSS <%s> failed", 
								  pclUaft, clUhss );
				ilRc1 = SetInitialCONA ( pclUaft, clUhss );
				if ( ilRc1 != RC_SUCCESS )
					dbg ( TRACE, "UpdateOSTTAB: SetInitialCONA UAFT <%s> UHSS <%s> failed", 
								  pclUaft, clUhss );
				ilRc1 = SetCONS ( prpFlightArray, pclUaft, clUhss, FALSE );
				if ( ilRc1 != RC_SUCCESS )
					dbg ( TRACE, "UpdateOSTTAB: SetCONS UAFT <%s> UHSS <%s> failed", 
								  pclUaft, clUhss );
				ilRc1 = ProcessSCC( pclUaft, clUhss );
				if ( ilRc1 != RC_SUCCESS )
					dbg ( TRACE, "UpdateOSTTAB: ProcessSCC UAFT <%s> UHSS <%s> failed", 
								  pclUaft, clUhss );
			}	
		}	/* end for loop over the two flights of rotation */
		llEvalRow = ARR_NEXT;
	} /* endof while-loop rgEvalResults */

	dbg ( TRACE, "UpdateOSTTAB: Finished with RC <%d>, Added <%d> kept <%d> deleted <%d> rows", 
		  ilRc, ilAdded, ilKept, ilDeleted );

	return ilRc;
}

/*  SetCONS: set logical fields of loaded online status						*/
/*	IN:	prpFlightArray:	flight array to be examined							*/	
/*		pcpUaft:		flight URNO, can be NULL for all flights			*/
/*		pcpUhss:		URNO of handling status sect., can be NULL for all	*/
/*		bpSave:			save OSTTAB if TRUE									*/
static int SetCONS ( ARRAYINFO *prpFlightArray, char *pcpUaft, char *pcpUhss, 
					 BOOL bpSave )
{
	char clOstKey[31]="", *pclOstPtr, clTime[21];
	char *pclRtab, *pclRurn, *pclUaft, *pclFlt2 ;
	long llOstRow;
	long llTifr, llFieldNo=-1;
	int ilRc1, ilRc = RC_SUCCESS;

	/*  SetCONS must set times with increasing LEVL to avoid undefined times of successors */

	if ( pcpUaft && pcpUaft[0] )
	{
		strcpy ( clOstKey, pcpUaft );
		if ( pcpUhss && pcpUhss[0] )
		{
			strcat ( clOstKey, "," );
			strcat ( clOstKey, pcpUhss );
		}
	}


	dbg ( TRACE, "SetCONS: key for Status to update <%s>", clOstKey );
	/*  look for all status with UAFT=pcpUaft */
	llOstRow = ARR_FIRST;
	while ( CEDAArrayFindRowPointer(&(rgOstArray.rrArrayHandle),
								    rgOstArray.crArrayName,
									&(rgOstArray.rrIdxHandle[1]),
									rgOstArray.crIdxName[1],clOstKey,
									&llOstRow, (void*)&pclOstPtr ) == RC_SUCCESS )
	{
		llTifr = atol ( OSTFIELD(pclOstPtr,igOstTifrIdx ) );
		pclRtab = OSTFIELD(pclOstPtr,igOstRtabIdx);
		pclRurn = OSTFIELD(pclOstPtr,igOstRurnIdx);
		pclUaft = OSTFIELD(pclOstPtr,igOstUaftIdx);
		pclFlt2 = OSTFIELD(pclOstPtr,igOstFlt2Idx);
		if ( strncmp ( pclRtab,"AFT", 3 ) == 0 )
			ilRc1 = GetFlightTime ( prpFlightArray, pclRurn, 
									OSTFIELD(pclOstPtr,igOstRfldIdx), clTime );
		else
			if ( strncmp ( pclRtab,"OST", 3 ) == 0 )
				ilRc1 = GetStatusTime ( pclRurn, clTime );
		else
			if ( strncmp ( pclRtab,"JOB", 3 ) == 0 )
				ilRc1 = GetConsJob ( pclUaft, pclFlt2, OSTFIELD(pclOstPtr,igOstUtplIdx), 
									 clTime, OSTFIELD(pclOstPtr,igOstRetyIdx) );
		else
			if ( strncmp ( pclRtab,"CCA", 3 ) == 0 )
				ilRc1 = GetConsCca ( pclUaft, clTime, OSTFIELD(pclOstPtr,igOstRetyIdx) );

		if ( ilRc1 != RC_SUCCESS )
		{
			dbg ( TRACE, "SetCONS: Evaluation of status time RTAB <%s> RURN <%s> failed RC <%d>", 
				  pclRtab, pclRurn, ilRc1 );
			strcpy ( clTime, "UNDEF" );
		}
		else
			AddSecondsToCEDATime(clTime,llTifr,1);
		ilRc1 = CEDAArrayPutField( &rgOstArray.rrArrayHandle,
								   rgOstArray.crArrayName, &llFieldNo, 
									"CONS", llOstRow, clTime );
		if ( ilRc1 != RC_SUCCESS )
		{
			dbg ( TRACE, "SetCONS: CEDAArrayPutField failed Row <%ld> RC <%d>", 
				llOstRow, ilRc1 );
		}
		if ( ilRc1 != RC_SUCCESS )
			ilRc = RC_FAIL;
		llOstRow = ARR_NEXT;
	}

	/*  look for all status with RURN=pcpUaft AND RURN!=UAFT */
	llOstRow = ARR_FIRST;
	while ( CEDAArrayFindRowPointer(&(rgOstArray.rrArrayHandle),
								    rgOstArray.crArrayName,
									&(rgOstArray.rrIdxHandle[3]),
									rgOstArray.crIdxName[3],clOstKey,
									&llOstRow, (void*)&pclOstPtr ) == RC_SUCCESS )
	{
		llTifr = atol ( OSTFIELD(pclOstPtr,igOstTifrIdx ) );
		pclRtab = OSTFIELD(pclOstPtr,igOstRtabIdx);
		pclRurn = OSTFIELD(pclOstPtr,igOstRurnIdx);
		pclUaft = OSTFIELD(pclOstPtr,igOstUaftIdx);
		pclFlt2 = OSTFIELD(pclOstPtr,igOstFlt2Idx);
		if ( strcmp(pclRurn,pclUaft) && !strncmp ( pclRtab,"AFT", 3 ) )
		{
			ilRc1 = GetFlightTime ( prpFlightArray, pclRurn, 
									OSTFIELD(pclOstPtr,igOstRfldIdx), clTime );
			if ( ilRc1 != RC_SUCCESS )
			{
				dbg ( TRACE, "SetCONS: Evaluation of status time RTAB <%s> RURN <%s> failed RC <%d>", 
					  pclRtab, pclRurn, ilRc1 );
				strcpy ( clTime, "UNDEF" );
			}
			else
				AddSecondsToCEDATime(clTime,llTifr,1);
			ilRc1 = CEDAArrayPutField( &rgOstArray.rrArrayHandle,
									   rgOstArray.crArrayName, &llFieldNo, 
										"CONS", llOstRow, clTime );
			if ( ilRc1 != RC_SUCCESS )
			{
				dbg ( TRACE, "SetCONS: CEDAArrayPutField failed Row <%ld> RC <%d>", 
					llOstRow, ilRc1 );
			}
			if ( ilRc1 != RC_SUCCESS )
				ilRc = RC_FAIL;
		}
		llOstRow = ARR_NEXT;
	}

	return ilRc;
}

/*  PrepareOSTArray: set logical fields of loaded online status				*/
/*	IN:		pcpUaft:	flight URNO, can be NULL for all flights			*/
/*			pcpUhss:	URNO of handling status sect., can be NULL for all  */
/*			bpSave:		save OSTTAB if TRUE									*/
static int PrepareOSTArray ( char *pcpUaft, char *pcpUhss/*, BOOL bpSave */)
{
	char clOstKey[31]="", clUsde[21], clUrno[21], clUaft2[11];
	char *pclOstPtr, *pclSdePtr, *pclPredPtr, clTifr[8];
	long llOstRow, llRowPred, llSdeRow, llTifr;
	long llFldLevlNo=-1, llFldRetyNo=-1, llFldTifrNo=-1, llFldUtplNo=-1;
	long llFldAftfNo=-1, llFldFlt2No=-1;
	int	 ilLevl, ilRc = RC_SUCCESS, ilRc1 ;
	BOOL blChangedSomething ;
	int ilSuccessors=0, ilIdx=3;

	if ( pcpUaft && pcpUaft[0] )
	{
		strcpy ( clOstKey, pcpUaft );
		/*
		if ( pcpUhss && pcpUhss[0] )
		{
			strcat ( clOstKey, "," );
			strcat ( clOstKey, pcpUhss );
		}
		*/
	}
	dbg ( TRACE, "PrepareOSTArray: key for Status to update <%s>", clOstKey );
	
	/*  first loop set RETY, TIFR UTPL, and level "00", if RTAB="AFT" */
	if ( !pcpUaft )
	{	/* discativate indices if all flights are going to be processed */
		ilRc1 = CEDAArrayDisactivateIndex ( &(rgOstArray.rrArrayHandle),	
											&(rgOstArray.crArrayName[0]),
											&(rgOstArray.rrIdxHandle[1]),
											rgOstArray.crIdxName[1] );
		ilRc1 |= CEDAArrayDisactivateIndex ( &(rgOstArray.rrArrayHandle),	
											&(rgOstArray.crArrayName[0]),
											&(rgOstArray.rrIdxHandle[4]),
											rgOstArray.crIdxName[4] );
		dbg ( DEBUG, "PrepareOSTArray: CEDAArrayDisactivateIndex returns <%d>", ilRc1 );	
	}

	llOstRow = ARR_FIRST; 
	while ( CEDAArrayFindRowPointer(&(rgOstArray.rrArrayHandle),
									rgOstArray.crArrayName, 
									&(rgOstArray.rrIdxHandle[2]),
									rgOstArray.crIdxName[2], clOstKey,
									&llOstRow, (void*)&pclOstPtr ) == RC_SUCCESS )
	{
		dbg ( DEBUG, "PrepareOSTArray: Found OST-Record URNO <%s> USDE <%s>", 
			  OSTFIELD(pclOstPtr, igOstUrnoIdx), OSTFIELD(pclOstPtr, igOstUsdeIdx) );

		if ( pcpUhss &&  pcpUhss[0] && 
			 strcmp (OSTFIELD(pclOstPtr, igOstUhssIdx), pcpUhss ) )
		{
			dbg ( DEBUG, "PrepareOSTArray: Found status with wrong UHSS <%s> != <%s>", 
				  OSTFIELD(pclOstPtr, igOstUhssIdx), pcpUhss ) ;	
		}
		else
		{
			llSdeRow = ARR_FIRST;
			strcpy ( clUsde, OSTFIELD(pclOstPtr, igOstUsdeIdx) );
			ilRc1 = CEDAArrayFindRowPointer(&(rgSdeArray.rrArrayHandle),
											rgSdeArray.crArrayName,
											&(rgSdeArray.rrIdxHandle[0]),
											rgSdeArray.crIdxName[0], clUsde,
											&llSdeRow, (void*)&pclSdePtr );
			if ( ilRc1 != RC_SUCCESS )
			{		
				dbg ( TRACE, "PrepareOSTArray: SDE-record URNO <%s> not found RC <%d>", 
					  clUsde, ilRc1 ) ;
			}
			else
			{
				ilRc1 = CEDAArrayPutField( &rgOstArray.rrArrayHandle,
										   rgOstArray.crArrayName, &llFldRetyNo, 
											"RETY", llOstRow, 
											SDEFIELD(pclSdePtr,igSdeRetyIdx) );
				ilRc1 |= CEDAArrayPutField( &rgOstArray.rrArrayHandle,
											rgOstArray.crArrayName, &llFldAftfNo, 
											"AFTF", llOstRow, 
											SDEFIELD(pclSdePtr,igSdeAftfIdx) );
				llTifr = atol ( SDEFIELD(pclSdePtr,igSdeTifrIdx) );
				llTifr *= 60;
				sprintf ( clTifr, "%ld", llTifr );
				ilRc1 |= CEDAArrayPutField( &rgOstArray.rrArrayHandle,
											rgOstArray.crArrayName, &llFldTifrNo, 
											"TIFR", llOstRow, clTifr );
				ilRc1 |= CEDAArrayPutField( &rgOstArray.rrArrayHandle,
											rgOstArray.crArrayName, &llFldUtplNo, 
											"UTPL", llOstRow, 
											SDEFIELD(pclSdePtr,igSdeUtplIdx) );
				/*
				if ( strncmp ( OSTFIELD(pclOstPtr,igOstRtabIdx),"OST", 3 ) == 0 )
					strcpy ( clLevl, "-1" );
				else
					strcpy ( clLevl, "00" );*/	
				ilRc1 |= CEDAArrayPutField( &rgOstArray.rrArrayHandle,
											rgOstArray.crArrayName, &llFldLevlNo, 
											"LEVL", llOstRow, SDEFIELD(pclSdePtr,igSdeLevlIdx)  );
				if ( GetFlt2Value ( &rgAftArray, OSTFIELD(pclOstPtr, igOstUaftIdx), 
									OSTFIELD(pclOstPtr, igOstUsrhIdx), clUaft2 ) == RC_SUCCESS )
					ilRc1 |= CEDAArrayPutField( &rgOstArray.rrArrayHandle,
												rgOstArray.crArrayName, &llFldFlt2No, 
												"FLT2", llOstRow, clUaft2 );
				if ( ilRc1 != RC_SUCCESS )
				{
					dbg ( TRACE, "PrepareOSTArray: CEDAArrayPutField failed Row <%ld> RC <%d>", 
						  llOstRow, ilRc1 );
					ilRc = RC_FAIL;
				}
			}
		}
		llOstRow = ARR_NEXT;
	}

	if ( !pcpUaft )
	{	/* discativate indices if all flights are going to be processed */
		ilRc1 = CEDAArrayActivateIndex( &(rgOstArray.rrArrayHandle),	
										&(rgOstArray.crArrayName[0]),
										&(rgOstArray.rrIdxHandle[1]),
										rgOstArray.crIdxName[1] );
		ilRc1 |= CEDAArrayActivateIndex ( &(rgOstArray.rrArrayHandle),	
										  &(rgOstArray.crArrayName[0]),
										  &(rgOstArray.rrIdxHandle[4]),
										  rgOstArray.crIdxName[4] );
		dbg ( DEBUG, "PrepareOSTArray: CEDAArrayActivateIndex returns <%d>", ilRc1 );		  		  
	}
	return ilRc;
}

/*  GetFlightTime:	Get content of time field if filled, otherwise best		*/
/*					time known, which is less precise than the requested 	*/
/*	IN:		prpFlightArray:	Array to search the flight in					*/
/*			pcpUaft:	flight URNO, must not be NULL						*/
/*			pcpFld:		requested time field,  must not be NULL				*/
/*	OUT:	pcpTime:	result buffer, must not be NULL						*/
static int GetFlightTime ( ARRAYINFO *prpFlightArray, char *pcpUaft, 
						   char *pcpFld, char *pcpTime )
{
	char *pclFlight, *pclVal;
	long llAftRow = ARR_FIRST;
	int  i, ilRc, *pilTimesIdx=0, ilStartIdx, ilPos, ilCol ;
	BOOL blNoArrivalDepartureTime = FALSE;			

	if ( !prpFlightArray || !pcpUaft || !pcpFld || !pcpTime )
	{
		dbg ( TRACE, "GetFlightTime: At least one parameter is null!" );
		return RC_INVALID ;
	}
	memset ( pcpTime, 0, 15 );

	ilRc = CEDAArrayFindRowPointer(&(prpFlightArray->rrArrayHandle),
									prpFlightArray->crArrayName,
									&(prpFlightArray->rrIdxHandle[0]),
									prpFlightArray->crIdxName[0],pcpUaft,
									&llAftRow, (void*)&pclFlight );
	if ( ilRc != RC_SUCCESS )
		dbg ( TRACE, "GetFlightTime: Flight <%s> not found, RC <%d>", pcpUaft, ilRc );
	else
	{
		ilStartIdx = get_item_no(INBOUNDTIMES,pcpFld,strlen(pcpFld)+1 ); 
		if ( ilStartIdx >= 0 )
		{	/* reference time is an inbound time */
			pilTimesIdx =rgInbTimeIdx;
		}
		else
		{
			ilStartIdx = get_item_no(OUTBOUNDTIMES,pcpFld,strlen(pcpFld)+1 ); 
			if ( ilStartIdx >= 0 )
			{	/* reference time is an inbound time */
				pilTimesIdx =rgOutbTimeIdx;
			}
		}		
		if ( !pilTimesIdx )
		{	/*  Maybe one of the new nominal fields is requested */
			FindItemInList( AFT_FIELDS, pcpFld, ',', &ilStartIdx, &ilCol,&ilPos);
			if ( ilStartIdx <= 0 )
			{
				dbg ( TRACE, "GetFlightTime: Requested field <%s> not found in used fieldlist", pcpFld );
				ilRc = RC_FAIL;
			}
			blNoArrivalDepartureTime = TRUE;			
		}
	}
	if ( ilRc == RC_SUCCESS )
	{
		if ( blNoArrivalDepartureTime )
		{
			pclVal = AFTFIELD ( pclFlight, ilStartIdx );
			if ( ( pclVal[0]!=' ' ) && ( strlen ( pclVal) >= 14 ) )
				strncpy ( pcpTime, pclVal, 14 );
		}
		else
			for ( i=ilStartIdx; !pcpTime[0] &&(i>=0); i-- )
			{
				pclVal = AFTFIELD ( pclFlight, pilTimesIdx[i] );
				if ( ( pclVal[0]!=' ' ) && ( strlen ( pclVal) >= 14 ) )
					strncpy ( pcpTime, pclVal, 14 );
			}
		if ( !pcpTime[0] )
		{
			dbg ( TRACE, "GetFlightTime: No Flight time filled, FLD <%s> IDX <%d> UAFT <%s>", 
				  pcpFld, ilStartIdx, pcpUaft );
			ilRc = RC_NOTFOUND;
		}
	}	
	dbg ( DEBUG, "GetFlightTime:  UAFT <%s> FLD <%s> TIME <%s>", pcpUaft, pcpFld, pcpTime );	
	return ilRc;
}

/*  GetStatusTime: Get the first status field filled from "CONU,CONA,CONS"  */
/*	IN:		pcpUost:	URNO of status, must not be NULL					*/
/*	OUT:	pcpTime:	result buffer,  must not be NULL					*/
static int GetStatusTime ( char *pcpUost, char *pcpTime )
{
	char *pclStatus, *pclVal;
	long llOstRow = ARR_FIRST;
	int ilRc, *pilTimesIdx=0, ilStartIdx ;

	if ( !pcpUost || !pcpTime )
	{
		dbg ( TRACE, "GetStatusTime: At least one parameter is null!" );
		return RC_INVALID ;
	}
	memset ( pcpTime, 0, 15 );

	ilRc = CEDAArrayFindRowPointer(&(rgOstArray.rrArrayHandle),
									rgOstArray.crArrayName,
									&(rgOstArray.rrIdxHandle[0]),
									rgOstArray.crIdxName[0],pcpUost,
									&llOstRow, (void*)&pclStatus );
	if ( ilRc != RC_SUCCESS )
		dbg ( TRACE, "GetStatusTime: Predecessor <%s> not found, RC <%d>", pcpUost, ilRc );
	else
	{
		pclVal = OSTFIELD ( pclStatus, igOstConuIdx );
		if ( ( pclVal[0]!=' ' ) && ( strlen ( pclVal) >= 14 ) )
			strncpy ( pcpTime, pclVal, 14 );
		else
		{
			pclVal = OSTFIELD ( pclStatus, igOstConaIdx );
			if ( ( pclVal[0]!=' ' ) && ( strlen ( pclVal) >= 14 ) )
				strncpy ( pcpTime, pclVal, 14 );
			else
			{
				pclVal = OSTFIELD ( pclStatus, igOstConsIdx );
				if ( ( pclVal[0]!=' ' ) && ( strlen ( pclVal) >= 14 ) )
					strncpy ( pcpTime, pclVal, 14 );
			}
		}
		if ( !pcpTime[0] )
		{
			dbg ( TRACE, "GetStatusTime: No Status time filled, UOST <%s>", pcpUost );
			ilRc = RC_NOTFOUND;
		}
	}	
	dbg ( DEBUG, "GetStatusTime:  UOST <%s> TIME <%s>", pcpUost, pcpTime );	
	return ilRc;
}

/*  SendAnswer: Send answer on incoming event to originator					*/
/*	IN:		prpEvent:	incoming event										*/
/*			ipRc:		return code to be set in outgoing event				*/
static int SendAnswer(EVENT *prpEvent,int ipRc)
{
	int	      ilRc           = RC_SUCCESS;			/* Return code */

	BC_HEAD *prlBchead       = NULL;
	CMDBLK  *prlCmdblk       = NULL;
	char    *pclSelection    = NULL;
	char    *pclFields       = NULL;
	char	*pclData         = NULL;
	int ilLen;
	
	prlBchead    = (BC_HEAD *) ((char *)prpEvent + sizeof(EVENT));
	prlCmdblk    = (CMDBLK *)  ((char *)prlBchead->data);
	pclSelection = prlCmdblk->data;
	pclFields    = pclSelection + strlen(pclSelection) + 1;
	pclData      = pclFields + strlen(pclFields) + 1;

	prlBchead->rc = ipRc;

	ilLen = sizeof(EVENT) + sizeof(BC_HEAD) + sizeof(CMDBLK) + 
			strlen(pclSelection) + strlen(pclFields) + strlen(pclData) + 20; 

	dbg(TRACE,"SendAnswer to: %d",prpEvent->originator);
	if ((ilRc = que(QUE_PUT, prpEvent->originator, mod_id, PRIORITY_3, 
					ilLen, (char*)prpEvent)) != RC_SUCCESS)
	{
		dbg(TRACE,"SendAnswer: QUE_PUT failed RC <%d>", ilRc );
	}

	return ilRc;
}


/*  ProcessCFS: process CFS-command, i. e. creation of skeleton				*/
/*	IN:		pcpAftSelection:	where-clause for flights to be evaluated	*/
static int ProcessCFS ( char *pcpAftSelection )
{
	int ilRc = RC_SUCCESS, ilLen;
	char *pclSel = 0, *ps;
	BOOL blReload = FALSE;
	char clTmp[101], clStart[9], clEnd[9];

	ilLen = strlen (pcpAftSelection)+50;
	pclSel = malloc ( ilLen * sizeof(char) );
	if ( !pclSel )
	{
		dbg ( TRACE, "ProcessCFS: Malloc of <%d> char failed", ilLen );
		ilRc = RC_NOMEM;
	}
	else
	{
		sprintf ( pclSel, "where rkey in (select rkey from afttab %s",  pcpAftSelection );
		/* strcpy ( pclSel, pcpAftSelection ); */
		if ( ps = strchr ( pclSel, '|' ) )
			*ps  = '\0';
		strcat ( pclSel, ")" );
		ilRc = CEDAArrayRefill(&rgReqAftArray.rrArrayHandle, 
							 rgReqAftArray.crArrayName, pclSel, "", ARR_FIRST );
		if ( ilRc != RC_SUCCESS )
			dbg ( TRACE, "ProcessCFS: CEDAArrayRefill <AFT> failed, RC <%d>", ilRc );
	}
	ps = strchr ( pcpAftSelection, '|' );
	if ( ps )
		ps++;
	ilRc = EvaluateRules ( &rgReqAftArray, ps );
	if ( ilRc == RC_SUCCESS )
	{
		ilRc = CreateSkeleton ( &rgReqAftArray );
		if ( ilRc != RC_SUCCESS )
			dbg ( TRACE, "ProcessCFS: CreateSkeleton failed RC <%d>", ilRc );
	}
	if ( ilRc == RC_SUCCESS )
	{
		/*  Reload OSTTAB, if requested flights are not completely inside	*/
		/*	loaded time frame												*/
		if ( cgMinAftTime[0] && ( strncmp(cgMinAftTime,cgLoadStart,8) < 0 ) )
			blReload = TRUE;
		if ( strncmp(cgLoadEnd,cgMaxAftTime,8) < 0 ) 
			blReload = TRUE;
		if ( blReload )
		{
			strncpy ( clStart, cgMinAftTime, 8 );
			strncpy ( clEnd, cgMaxAftTime, 8 );
			clStart[8] = clEnd[8] = '\0';
			sprintf(clTmp, "WHERE SDAY BETWEEN '%s' AND '%s'", clStart,clEnd );
			dbg ( TRACE, "CALCULATION OUTSIDE LOADED TIMEFRAME !!!" );
			dbg ( TRACE, "ProcessCFS: Reloading time frame, selection <%s>", clTmp );
			ilRc = CEDAArrayRefill(&rgOstArray.rrArrayHandle, 
								rgOstArray.crArrayName, clTmp, "", ARR_FIRST );
			if ( ilRc != RC_SUCCESS )
				dbg ( TRACE, "ProcessCFS: CEDAArrayRefill <OST> failed RC <%d>", ilRc );
		}

		ilRc = UpdateOSTTAB ( &rgReqAftArray );
		if ( ilRc != RC_SUCCESS )
			dbg ( TRACE, "ProcessCFS: UpdateOSTTAB failed RC <%d>", ilRc );
	}
	PrepareSBC ( 0, &rgReqAftArray );
	SaveOstTab ();

	if ( blReload )
	{
		strncpy ( clStart, cgLoadStart, 8 );
		strncpy ( clEnd, cgLoadEnd, 8 );
		sprintf(clTmp, "WHERE SDAY BETWEEN '%s' AND '%s'", clStart,clEnd );
		dbg ( TRACE, "ProcessCFS: Reloading time frame, selection <%s>", clTmp );
		ilRc = CEDAArrayRefill( &rgOstArray.rrArrayHandle, 
								rgOstArray.crArrayName, clTmp, "", ARR_FIRST );
		if ( ilRc != RC_SUCCESS )
			dbg ( TRACE, "ProcessCFS: CEDAArrayRefill <OST> failed RC <%d>", ilRc );
		ilRc = PrepareOSTArray (0, 0 );
		SaveOstTab ();
	}
		
	if ( pclSel )
		free ( pclSel );
	return ilRc;
}
		
/*  ProcessSCC: set conflict field OSTTAB.COTY for flight and HSS			*/
/*	IN:		pcpUaft:	flight URNO, can be NULL for all flights			*/
/*			pcpUhss:	URNO of handling status sect., can be NULL for all  */
static int ProcessSCC( char *pcpUaft, char *pcpUhss )
{
	long llRow = ARR_FIRST, llFieldNo=-1;
	char *pclStat;
	char *pclCoty, *pclCona, *pclCons;
	char clNow[20], clCotyNew[2]=" ";
	int ilRc = RC_SUCCESS, ilRc1;
	char clKey[31]="";

	GetActDate ( clNow );

	if ( pcpUaft && pcpUaft[0] )
	{
		strcpy ( clKey, pcpUaft );
		if ( pcpUhss && pcpUhss[0] )
		{
			strcat ( clKey, "," );
			strcat ( clKey, pcpUhss );
		}
	}

	dbg ( DEBUG, "ProcessSCC: actual date and time <%s>", clNow );
	while ( CEDAArrayFindRowPointer(&(rgOstArray.rrArrayHandle),
								   rgOstArray.crArrayName,
								   &(rgOstArray.rrIdxHandle[1]),
									rgOstArray.crIdxName[1], clKey, &llRow,
								   (void *)&pclStat) == RC_SUCCESS )
	{
		pclCoty = OSTFIELD(pclStat,igOstCotyIdx);
		pclCona = OSTFIELD(pclStat,igOstConuIdx);
		/*  confirmation time by User (CONU) has to be used, if filled,
			otherwise  confirmation time by system (CONA) has to be used */
		if ( IS_EMPTY(pclCona) )
			pclCona	= OSTFIELD(pclStat,igOstConaIdx);
		pclCons = OSTFIELD(pclStat,igOstConsIdx);
			
		clCotyNew[0] = *pclCoty;
		if ( IS_EMPTY(pclCons) || !strncmp( pclCons, "UNDEF", 5) )
		{	/*  scheduled time (CONS) is empty => no conflict */
			clCotyNew[0] = ' ';	
		}
		else if ( IS_EMPTY(pclCona) )
		{	/* actual confirmation (CONA) is empty */
			if ( strcmp ( clNow, pclCons ) > 0 ) 
			{
				if ( *pclCoty!='A' )
					clCotyNew[0] = 'N' ;
			}
			else
				clCotyNew[0] = ' ';
		}	
		else
		{	/*  actual confirmation (CONA) is filled */
			if ( strcmp ( pclCona, pclCons ) > 0 ) 
			{
				if ( *pclCoty!='A') 
					clCotyNew[0] = 'N';
			}
			else
			{
				clCotyNew[0] = 'C';
			}
		}
		if ( *pclCoty != *clCotyNew )
		{
			dbg ( TRACE, "ProcessSCC: Now <%s> CONA <%s> CONS <%s> COTY <%s> -> <%s>",
				  clNow, pclCona, pclCons, pclCoty, clCotyNew );
			ilRc1 = CEDAArrayPutField( &rgOstArray.rrArrayHandle,
									   rgOstArray.crArrayName, &llFieldNo, 
									   "COTY", llRow, clCotyNew );				
			if ( ilRc1 != RC_SUCCESS )
			{
				dbg ( TRACE, "ProcessSCC: Set COTY <%s> Row <%ld> failed, RC <%d>", 
							  clCotyNew, llRow, ilRc1 );
				ilRc = RC_FAIL;
			}			
			else
				dbg ( DEBUG, "ProcessSCC: Set COTY <%s> Row <%ld> successful", 
							 clCotyNew, llRow );
		}
		else
			dbg ( DEBUG, "ProcessSCC: Now <%s> CONA <%s> CONS <%s> COTY remains <%s>",
				  clNow, pclCona, pclCons, pclCoty );
		llRow = ARR_NEXT;
	}
	return ilRc;
}


static int SaveOstTab ()
{
	int ilRc;
	char	*pcpSelList=0;

	if ( cgBcField[0] && cgBcData[0] )
	{	/*  don't send single BC, we use SBC */
		CEDAArraySendChanges2BCHDL(&rgOstArray.rrArrayHandle, 
								   rgOstArray.crArrayName, FALSE );
	}
	else
		CEDAArraySendChanges2BCHDL(&rgOstArray.rrArrayHandle, 
								   rgOstArray.crArrayName, bgSingleBC );

	ilRc = CEDAArrayWriteDB(&rgOstArray.rrArrayHandle, rgOstArray.crArrayName,
							&pcpSelList,NULL,NULL,NULL,ARR_COMMIT_ALL_OK);
	if ( ilRc != RC_SUCCESS )
		dbg ( TRACE, "SaveOstTab: CEDAArrayWriteDB returned RC <%d>", ilRc );
	else
	{
		if ( pcpSelList && pcpSelList[0] && cgBcField[0] && cgBcData[0] )
		{
			/*  send SBC, if cgBcField and cgBcData have been initialized	*/
			/*  and if pcpSelList is filled									*/
			ilRc = tools_send_info_flag( 1900, 0, "BCHDL", "STAHDL", "STAHDL", "", 
										"", "", cgTwEnd, "SBC","OSTTAB","",
										cgBcField, cgBcData, 0);          
			dbg ( TRACE, "SaveOstTab: Send SBC: <%s> RC <%d>", cgBcData, ilRc );
		}
	}
	return ilRc;
}


/*  GetConaFlight: get actual status confirmation time for flight 			*/
/*	IN:		pcpUaft:	flight URNO, must not be NULL						*/
/*			pcpRfld:	reference field (AFT), which is used as conf. time  */
/*	OUT:	pcpCona:	time from pcpRfld, if flight is found, else ""		*/
static int GetConaFlight ( char *pcpUaft, char *pcpRfld, char* pcpCona )
{
	char *pclFlight, *pclAf1Rec;
	long llRow = ARR_FIRST;
	int  ilFldIdx, ilRc = RC_SUCCESS, ilCol, ilPos;
	
	if ( !pcpUaft || !pcpCona || !pcpRfld )
	{
		dbg ( TRACE, "GetConaFlight: At least one parameter is NULL" );
		return RC_INVALID;
	}
	pcpCona[0] = '\0';

	FindItemInList( AFT_FIELDS, pcpRfld, ',', &ilFldIdx, &ilCol,&ilPos);
	if ( ilFldIdx > 0 )
	{
		ilRc = CEDAArrayFindRowPointer(&(rgAftArray.rrArrayHandle),
										rgAftArray.crArrayName,
										&(rgAftArray.rrIdxHandle[0]),
										rgAftArray.crIdxName[0], pcpUaft, 
										&llRow, (void *)&pclFlight );
		if ( ilRc != RC_SUCCESS ) 
		{
			dbg ( TRACE, "GetConaFlight: Flight <%s> not found", pcpUaft );
		}
		else
		{
			if ( !IS_EMPTY ( AFTFIELD(pclFlight,ilFldIdx) ) )
				strcpy ( pcpCona, AFTFIELD(pclFlight,ilFldIdx) );
		}
	}
	else
	{
		FindItemInList( AF1_FIELDS, pcpRfld, ',', &ilFldIdx, &ilCol,&ilPos);
		if ( ilFldIdx > 0 )
		{
			llRow = ARR_FIRST;
			ilRc = CEDAArrayFindRowPointer(&(rgAf1Array.rrArrayHandle),
											rgAf1Array.crArrayName,
											&(rgAf1Array.rrIdxHandle[1]),
											rgAf1Array.crIdxName[1], pcpUaft, 
											&llRow, (void *)&pclAf1Rec );
			if ( ilRc != RC_SUCCESS ) 
			{
				dbg ( TRACE, "GetConaFlight: No AF1 record found for flight <%s>", pcpUaft );
			}
			else
			{
				if ( !IS_EMPTY ( AF1FIELD(pclAf1Rec,ilFldIdx) ) )
					strcpy ( pcpCona, AF1FIELD(pclAf1Rec,ilFldIdx) );
			}
		}
		else
		{
			dbg ( TRACE, "GetConaFlight: Reference field <%s> not found in used fieldlists (AFT,AF1)", pcpRfld );
			return RC_INVALID;
		}
	}

	dbg ( TRACE, "GetConaFlight: RFLD <%s> UAFT <%s> => CONA <%s> RC <%d>", 
		  pcpRfld, pcpUaft, pcpCona, ilRc  );
	return ilRc;
}

/*  GetConaJob: get actual status confirmation time for jobs	 			*/
/*	IN:		pcpUaft:	flight URNO, must not be NULL						*/
/*			pcpUtpl:	template  of jobs that are used for the status		*/
/*			pcpRety:	type of reference JOBI(informed), JOBC(confirmed),	*/
/*						JOBF(finished)										*/
/*	OUT:	pcpCona:	confirmation time, if existing						*/
static int GetConaJob ( char *pcpUaft, char *pcpUtpl, char* pcpCona, char *pcpRety,
					    char *pcpUaft2, char *pcpConaTurn )
{
	char *pclJob, *pclStat, *pclActo, *pclAcfr;
	long llRow = ARR_FIRST;
	int  ilType=0, ilJobs=0, ilSet=0, i;
	char clCona[20], clKey[31], clNow[21], clEndTime[21];
	char *pclFlights[2];
	
	if ( !pcpUaft || !pcpCona || !pcpRety || !pcpUtpl || IS_EMPTY(pcpUtpl) )
	{
		dbg ( TRACE, "GetConaJob: At least one parameter is NULL" );
		return RC_INVALID;
	}

	GetActDate ( clNow );
	clCona[0] = '\0';
	pcpCona[0] = pcpConaTurn[0] = '\0';

	if ( !strncmp(pcpRety, "JOBI", 4 ) )
		ilType = 1;
	else if ( !strncmp(pcpRety, "JOBC", 4 ) )
	{
		ilType = 2;
	}
	else if ( !strncmp(pcpRety, "JOBF", 4 ) )
	{
		ilType = 3;
	}
	else
	{
		dbg ( TRACE, "GetConaJob: Unknown status type <%s>", pcpRety );
		return RC_INVALID;
	}
	pclFlights[0] = pcpUaft;
	pclFlights[1] = pcpUaft2;

	for ( i=0; (i<2) && !IS_EMPTY(pclFlights[i]); i++ )
	{
		sprintf ( clKey, "%s,%s", pclFlights[i], pcpUtpl );
		dbg ( DEBUG, "GetConaJob: Jobtab-Key <%s> ilType <%d>", clKey, ilType );
		while ( CEDAArrayFindRowPointer(&(rgJobArray.rrArrayHandle),
										rgJobArray.crArrayName,
										&(rgJobArray.rrIdxHandle[1]),
										rgJobArray.crIdxName[1], clKey, &llRow,
										(void *)&pclJob ) == RC_SUCCESS )
		{
			ilJobs ++;
			pclStat = JOBFIELD(pclJob,igJobStatIdx);
			pclAcfr	= JOBFIELD(pclJob,igJobAcfrIdx);
			pclActo = JOBFIELD(pclJob,igJobActoIdx);
			dbg ( DEBUG, "GetConaJob: Found job STAT <%s> ACFR <%s> ACTO <%s>", 
				  pclStat, pclAcfr, pclActo );
			switch ( ilType )
			{
				case 1:		/*  Job informed, at least one must be informed */
					if ( ( (strlen(pclStat)>=5) && (pclStat[4]=='1') ) ||
						 (pclStat[0]=='C') || (pclStat[0]=='F')
					   )
					{
						ilSet++;
						if (!clCona[0] )
							strcpy ( clCona, clNow );
					}
					break;
				case 2:		/*  Job confirmed, at least one must be confirmed */
					if ( (pclStat[0]=='C') || (pclStat[0]=='F') )
					{
						ilSet++;
						if (!clCona[0] )
							strcpy ( clCona, clNow );
						if ( !IS_EMPTY(pclAcfr) && (strcmp(clCona,pclAcfr)> 0) )
							strcpy (clCona, pclAcfr ) ;
					}
					break;
				case 3:		/*  Job finished, all must be finished */
					if ( pclStat[0]=='F' )
					{
						ilSet++;
						/*  for actual job, endtime is min(now,Acto) */
						if ( !IS_EMPTY(pclActo) && (strcmp(clNow,pclActo)> 0) )
							strcpy (clEndTime, pclActo ) ;
						else
							strcpy ( clEndTime, clNow );
						/*  last endtime of all jobs is confirmation time */
						if ( strcmp(clEndTime,clCona)> 0 )
							strcpy (clCona, clEndTime ) ;
					}
					break;
			}
			llRow = ARR_NEXT;
		}
		/*
		if ( !ilJobs ) 
		{
			dbg ( TRACE, "GetConaJob: No Jobs found for UAFT <%s> UTPL <%s>", 
				  pclFlights[i], pcpUtpl );
			return RC_NOTFOUND;
		}*/
		if ( (ilType<=2) && (ilSet>0) )
		{	/* if at least one Job is informed resp. confirmed take first one */
			if ( i == 0)
				strcpy ( pcpCona, clCona );	
			else
				strcpy ( pcpConaTurn, clCona );	
		}
		else
			if ( ilSet>=ilJobs )
			{	/* all jobs are finished take latest one */
				if ( i == 0)
					strcpy ( pcpCona, clCona );	
				else
					strcpy ( pcpConaTurn, clCona );	
			}
	}
	dbg ( TRACE, "GetConaJob: RETY <%s> UAFT <%s> UTPL <%s> %d of %d jobs set => CONA <%s> CONA-Turn <%s>", 
		  pcpRety, pcpUaft, pcpUtpl, ilSet, ilJobs, pcpCona, pcpConaTurn );
	if ( ilJobs > 0 )
		return RC_SUCCESS;
	else
		return RC_NOTFOUND;
}


/*  GetConaCca: get actual status confirmation time for check-in counters	*/
/*	IN:		pcpUaft:	flight URNO, must not be NULL						*/
/*			pcpRety:	type of reference CCAB(open), CCAE(closed)			*/
/*	OUT:	pcpCona:	confirmation time, if existing						*/
static int GetConaCca ( char *pcpUaft, char* pcpCona, char *pcpRety )
{
	char *pclCcaRow, clCona[20] = "";
	long llRow = ARR_FIRST;
	int  ilFldIdx, ilKeyIdx=1;
	int  ilCounters = 0, ilFilled = 0;
	
	if ( !pcpUaft || !pcpCona || !pcpRety )
	{
		dbg ( TRACE, "GetConaCca: At least one parameter is NULL" );
		return RC_INVALID;
	}
	
	pcpCona[0] = '\0';

	if ( !strncmp(pcpRety, "CCAB", 4 ) )
		ilFldIdx = igCcaCkbaIdx;
	else if ( !strncmp(pcpRety, "CCAE", 4 ) )
	{
		ilFldIdx = igCcaCkeaIdx;
		ilKeyIdx=2;
	}
	else
	{
		dbg ( TRACE, "GetConaCca: Unknown status type <%s>", pcpRety );
		return RC_INVALID;
	}
	while ( CEDAArrayFindRowPointer(&(rgCcaArray.rrArrayHandle),
								    rgCcaArray.crArrayName,
								    &(rgCcaArray.rrIdxHandle[ilKeyIdx]),
									rgCcaArray.crIdxName[ilKeyIdx], pcpUaft, 
									&llRow,(void *)&pclCcaRow ) == RC_SUCCESS )
	{
		ilCounters++;
		if ( !IS_EMPTY ( CCAFIELD(pclCcaRow,ilFldIdx) ) )
		{
			ilFilled ++;			
			if ( !clCona[0] )
				strcpy ( clCona, CCAFIELD(pclCcaRow,ilFldIdx) );
		}
		llRow = ARR_NEXT;
	}
	if ( !ilCounters ) 
	{
		dbg ( TRACE, "GetConaCca: No CCA-record found for UAFT <%s>", pcpUaft );
		return RC_NOTFOUND;
	}
	if ( (ilFldIdx==igCcaCkbaIdx) && (ilFilled>0) )
	{	/* if at least one counter open time is filled take first one */
		strcpy ( pcpCona, clCona );	
	}
	if ( (ilFldIdx==igCcaCkeaIdx) && (ilFilled>=ilCounters) )
	{	/* all counter closed time are filled take latest one */
		strcpy ( pcpCona, clCona );	
	}
	dbg ( TRACE, "GetConaCca: RETY <%s> UAFT <%s> %d of %d times filled => CONA <%s>", 
		  pcpRety, pcpUaft, ilFilled, ilCounters, pcpCona  );
	return RC_SUCCESS;
}

/*  HandleCCAUpdate: set OST.CONA for all affected status					*/
/*	IN:		pcpUaft:	flight URNO, must not be NULL						*/
static int HandleCCAUpdate ( char *pcpUaft )
{
	char	clUrno[21], clConaBeg[21], clConaEnd[21], *pclOstPtr;
	long	llOstRow, llFieldNo=-1;
	int		ilRc, ilRc1=RC_SUCCESS;

	if ( !pcpUaft )
	{
		dbg ( TRACE, "HandleCCAUpdate: pcpUaft is NULL" );
		return RC_INVALID;
	}

	ilRc = GetConaCca ( pcpUaft, clConaBeg, "CCAB" );
	ilRc |= GetConaCca ( pcpUaft, clConaEnd, "CCAE" );

	if(ilRc != RC_SUCCESS)
	{
		dbg ( TRACE, "HandleCCAUpdate: GetConaCca failed for flight <%s>", pcpUaft );
	}
	else
	{	/*  search all OST-records for flight with RETY =='CCAB' or 'CCAE' */
		llOstRow = ARR_FIRST;
		while ( CEDAArrayFindRowPointer(&(rgOstArray.rrArrayHandle),
										rgOstArray.crArrayName,
										&(rgOstArray.rrIdxHandle[1]),
										rgOstArray.crIdxName[1],pcpUaft,
										&llOstRow, (void*)&pclOstPtr ) == RC_SUCCESS )
		{
			if ( !strncmp( OSTFIELD(pclOstPtr,igOstRetyIdx), "CCAB", 4 ) )
			{	
				 ilRc1 |= CEDAArrayPutField (&rgOstArray.rrArrayHandle,
											rgOstArray.crArrayName, &llFieldNo, 
											"CONA", llOstRow, clConaBeg );
			}
			if ( !strncmp( OSTFIELD(pclOstPtr,igOstRetyIdx), "CCAE", 4 ) )
			{	
				 ilRc1 |= CEDAArrayPutField (&rgOstArray.rrArrayHandle,
											rgOstArray.crArrayName, &llFieldNo, 
											"CONA", llOstRow, clConaEnd );
			}
			llOstRow = ARR_NEXT;
		}
	}/* end of if */
	if ( ilRc1 != RC_SUCCESS )
	{
		dbg ( TRACE, "HandleCCAUpdate: Update of Field <CONA> failed for flight <%s>", pcpUaft );
		ilRc = RC_FAIL;
	}/* end of if */
	
	return ilRc;
}

/*  GetFieldByUrno: Get content of field of a record from an array			*/
/*	IN:		prpArray:	CEDAArray that contains the record					*/
/*			ipFldIdx:	index of requested field							*/
/*			pcpUrno:	URNO of requested record							*/
/*	OUT:	pcpBuf:		result buffer, if record found and ipFldIdx valid	*/
static int GetFieldByUrno ( ARRAYINFO *prpArray, int ipFldIdx, char *pcpUrno, char *pcpBuf )
{
	long llRow=ARR_FIRST;
	char *pclResult;
	int ilRc;

	if ( !prpArray || !pcpUrno || !pcpBuf )
	{
		dbg ( TRACE, "GetFieldByUrno: At least one parameter is NULL" );
		return RC_INVALID;
	}
	if ( (ipFldIdx > prpArray->lrArrayFieldCnt) || (ipFldIdx<=0) )
	{
		dbg ( TRACE, "GetFieldByUrno: Invalid field index <%d> Array <%s has <%ld> fields",
			  ipFldIdx, prpArray->crArrayName, prpArray->lrArrayFieldCnt );
		return RC_INVALID;
	}
	if ( prpArray->rrIdxHandle[0] < 0 )
	{
		dbg ( TRACE, "GetFieldByUrno: URNO-index not initialised for array <%s>",
			  prpArray->crArrayName );
		return RC_NOTINITIALIZED;
	}
	pcpBuf[0] = '\0';
	ilRc = CEDAArrayFindRowPointer(&(prpArray->rrArrayHandle),
									prpArray->crArrayName,
									&(prpArray->rrIdxHandle[0]),
									prpArray->crIdxName[0],pcpUrno,
									&llRow, (void*)&pclResult );
	if ( ilRc != RC_SUCCESS )
		dbg ( TRACE, "GetFieldByUrno: URNO <%s> not found in array <%s>", 
			  pcpUrno, prpArray->crArrayName );
	else
		strcpy ( pcpBuf, &(pclResult[prpArray->plrArrayFieldOfs[ipFldIdx-1]]) );
	dbg ( DEBUG, "GetFieldByUrno: Array <%s> URNO <%s> FldIdx <%d> Data <%s> RC <%d>",
		  prpArray->crArrayName, pcpUrno, ipFldIdx, pcpBuf, ilRc );	
	return ilRc;
}

/*  HandleJobUpdate: set OST.CONA for all affected status					*/
/*	IN:		pcpUaft:	flight URNO, must not be NULL						*/
/*	   		pcpUtpl:	URNO of job's template, must not be NULL			*/
static int HandleJobUpdate ( char *pcpUaft, char *pcpUtpl )
{
	char	clUrno[21], clConaI[21], clConaC[21], clConaF[21], *pclOstPtr;
	char	clUaft2[11]="", clTurnConaI[21], clTurnConaC[21], clTurnConaF[21];
	long	llOstRow, llFieldNo=-1;
	int		ilRc, ilRc1=RC_SUCCESS;

	if ( !pcpUaft || !pcpUtpl )
	{
		dbg ( TRACE, "HandleJobUpdate: At least one parameter is NULL" );
		return RC_INVALID;
	}
	if ( IS_EMPTY(pcpUtpl) )
	{
		dbg ( TRACE, "HandleJobUpdate: URNO of Job not filled -> nothing to do" );
		return RC_SUCCESS;
	}
	Get2ndFlightUrno ( &rgAftArray, pcpUaft, clUaft2 );
	dbg ( TRACE, "HandleJobUpdate: UAFT <%s> UTPL <%s> UAFT2 <%s>", pcpUaft, pcpUtpl, clUaft2 );
	ilRc = GetConaJob ( pcpUaft, pcpUtpl, clConaI, "JOBI", clUaft2, clTurnConaI );
	ilRc |= GetConaJob ( pcpUaft, pcpUtpl, clConaC, "JOBC", clUaft2, clTurnConaC );
	ilRc |= GetConaJob ( pcpUaft, pcpUtpl, clConaF, "JOBF", clUaft2, clTurnConaF );

	if(ilRc != RC_SUCCESS)
	{
		dbg ( TRACE, "HandleJobUpdate: GetConaJob failed UAFT <%s> UTPL <%s>", 
			  pcpUaft, pcpUtpl );
	}
	else
	{	/*  search all OST-records for flight with RETY =='JOBI' or 'JOBC' or 'JOBF' */
		llOstRow = ARR_FIRST;
		while ( CEDAArrayFindRowPointer(&(rgOstArray.rrArrayHandle),
										rgOstArray.crArrayName,
										&(rgOstArray.rrIdxHandle[2]),
										rgOstArray.crIdxName[2],pcpUaft,
										&llOstRow, (void*)&pclOstPtr ) == RC_SUCCESS )
		{
			if ( !strncmp( OSTFIELD(pclOstPtr,igOstUtplIdx), pcpUtpl, strlen(pcpUtpl) ) )
			{	/*  Job's template fits to template of status */
				dbg ( DEBUG, "HandleJobUpdate: Found status with UTPL <%s> in Row <%ld>", 
					  OSTFIELD(pclOstPtr,igOstUtplIdx), llOstRow );	
				if ( !strncmp( OSTFIELD(pclOstPtr,igOstRetyIdx), "JOBI", 4 ) )
				{
					 ilRc1 = CEDAArrayPutField (&rgOstArray.rrArrayHandle,
												rgOstArray.crArrayName, &llFieldNo, 
												"CONA", llOstRow, clConaI );
					 dbg ( DEBUG, "HandleJobUpdate: Set CONA <%s> for RETY <%s> Row <%ld> RC <%d>",
						   clConaI, OSTFIELD(pclOstPtr,igOstRetyIdx), llOstRow, ilRc );
				}
				if ( !strncmp( OSTFIELD(pclOstPtr,igOstRetyIdx), "JOBC", 4 ) )
				{
					 ilRc1 = CEDAArrayPutField (&rgOstArray.rrArrayHandle,
												rgOstArray.crArrayName, &llFieldNo, 
												"CONA", llOstRow, clConaC );
					 dbg ( DEBUG, "HandleJobUpdate: Set CONA <%s> for RETY <%s> Row <%ld> RC <%d>",
						   clConaC, OSTFIELD(pclOstPtr,igOstRetyIdx), llOstRow, ilRc );
				}
				if ( !strncmp( OSTFIELD(pclOstPtr,igOstRetyIdx), "JOBF", 4 ) )
				{
					 ilRc1 = CEDAArrayPutField (&rgOstArray.rrArrayHandle,
												rgOstArray.crArrayName, &llFieldNo, 
												"CONA", llOstRow, clConaF );
					 dbg ( DEBUG, "HandleJobUpdate: Set CONA <%s> for RETY <%s> Row <%ld> RC <%d>",
						   clConaF, OSTFIELD(pclOstPtr,igOstRetyIdx), llOstRow, ilRc );
				}
				if ( ilRc1 != RC_SUCCESS )
					ilRc = RC_FAIL;
			} /* end of: if template ok */
			llOstRow = ARR_NEXT;
		} /* end of while */
		if ( ilRc != RC_SUCCESS )
			dbg ( TRACE, "HandleJobUpdate: Update of Field <CONA> failed UAFT <%s> UTPL <%s>", 
				  pcpUaft, pcpUtpl );
		/*  Update Status from Turnaround rules where pcpUaft is 2nd Flight */
		if ( !IS_EMPTY (clUaft2) )
		{
			llOstRow = ARR_FIRST;
			while ( CEDAArrayFindRowPointer(&(rgOstArray.rrArrayHandle),
											rgOstArray.crArrayName,
											&(rgOstArray.rrIdxHandle[4]),
											rgOstArray.crIdxName[4],pcpUaft,
											&llOstRow, (void*)&pclOstPtr ) == RC_SUCCESS )
			{
				dbg ( DEBUG, "HandleJobUpdate: 2nd loop found status URNO <%s> with UTPL <%s>", 
							 OSTFIELD(pclOstPtr,igOstUrnoIdx), OSTFIELD(pclOstPtr,igOstUtplIdx) );	
				if ( !strncmp( OSTFIELD(pclOstPtr,igOstUtplIdx), pcpUtpl, strlen(pcpUtpl) ) )
				{	/*  Job's template fits to template of status */
					if ( !strncmp( OSTFIELD(pclOstPtr,igOstRetyIdx), "JOBI", 4 ) )
					{
						 ilRc1 = CEDAArrayPutField (&rgOstArray.rrArrayHandle,
													rgOstArray.crArrayName, &llFieldNo, 
													"CONA", llOstRow, clTurnConaI );
						 dbg ( DEBUG, "HandleJobUpdate: Set CONA(TURN) <%s> for RETY <%s> Row <%ld> RC <%d>",
							   clTurnConaI, OSTFIELD(pclOstPtr,igOstRetyIdx), llOstRow, ilRc );
					}
					if ( !strncmp( OSTFIELD(pclOstPtr,igOstRetyIdx), "JOBC", 4 ) )
					{
						 ilRc1 = CEDAArrayPutField (&rgOstArray.rrArrayHandle,
													rgOstArray.crArrayName, &llFieldNo, 
													"CONA", llOstRow, clTurnConaC );
						 dbg ( DEBUG, "HandleJobUpdate: Set CONA(TURN) <%s> for RETY <%s> Row <%ld> RC <%d>",
							   clTurnConaC, OSTFIELD(pclOstPtr,igOstRetyIdx), llOstRow, ilRc );
					}
					if ( !strncmp( OSTFIELD(pclOstPtr,igOstRetyIdx), "JOBF", 4 ) )
					{
						 ilRc1 = CEDAArrayPutField (&rgOstArray.rrArrayHandle,
													rgOstArray.crArrayName, &llFieldNo, 
													"CONA", llOstRow, clTurnConaF );
						 dbg ( DEBUG, "HandleJobUpdate: Set CONA(TURN) <%s> for RETY <%s> Row <%ld> RC <%d>",
							   clTurnConaF, OSTFIELD(pclOstPtr,igOstRetyIdx), llOstRow, ilRc );
					}
					if ( ilRc1 != RC_SUCCESS )
						ilRc = RC_FAIL;
				} /* end of: if template ok */
				llOstRow = ARR_NEXT;
			} /* end of while */
		}
	}/* end of if */
	
	return ilRc;
}


/*  IsAftChangeImportant: Determine what has to be done on UFR-event		*/
/*	IN:		pcpFields:	field-list from event								*/
/*	   		pcpData:	data-list from event								*/
/*	OUT: 	pbpDoEval:	set to TRUE, if evaluation of rules is necessary	*/
/*	   		pbpSetCons:	set to TRUE, if status (OST.CONS) have to be moeved	*/
/*	   		pbpSetCona:	set to TRUE, if actual confirmation has to be set	*/
static int IsAftChangeImportant ( char *pcpFields, char *pcpData, BOOL *pbpDoEval, 
								  BOOL  *pbpSetCons, BOOL  *pbpSetCona )
{
	int		ilNoOfItems, ilIdx, ilLoop, ilCol,ilPos, ilRc; 
	char	clUaft[21], *pclRow=0;
	long	llFunc = ARR_FIRST;
	char	clFina[11], clNew[81], clOld[81];
	char    clEvalFields[]="ADID,ALC2,ALC3,TTYP,ORG3,DES3,ACT3,ACT5,TIFA,TIFD,RKEY";
	char    clConsFields[]="STOA,ETAI,LAND,ONBE,ONBL,TIFA,STOD,ETDI,OFBL,AIRB,TIFD,TMOA,GA1B,GD1B,GA1E,GD1E,B1BS,B1ES";
	char    clConaFields[]="GA1X,GD1X,GA1Y,GD1Y,B1BA,B1EA,AIRB,ONBL,OFBL,FLTO,FLTC,BOAO,FCAL";
	BOOL	blGoon ;
	
	*pbpDoEval = *pbpSetCons = *pbpSetCona = FALSE;

	ilRc = FindItemInList(pcpFields,"URNO",',',&ilIdx,&ilCol,&ilPos);
	if ( ilRc != RC_SUCCESS )
		dbg ( TRACE, "IsAftChangeImportant: URNO not in Fieldlist" );
	else
	{
		if ( get_real_item(clUaft,pcpData,ilIdx)<=0 )
		{
			ilRc = RC_FAIL;
			dbg ( TRACE, "IsAftChangeImportant: URNO not filled" );
		}
	}
	if ( ilRc == RC_SUCCESS )
	{
		ilRc = CEDAArrayFindRowPointer (&(rgAftArray.rrArrayHandle),
										rgAftArray.crArrayName,
										&(rgAftArray.rrIdxHandle[0]),
										rgAftArray.crIdxName[0],
										clUaft ,&llFunc, (void *) &pclRow );
		if ( ilRc != RC_SUCCESS )
		{
			dbg ( TRACE, "IsAftChangeImportant: Flight URNO <%s> not found", clUaft );
		}
	}
	if ( ilRc == RC_SUCCESS )
	{
		ilNoOfItems = get_no_of_items(pcpFields);
		ilLoop = 1;
		blGoon = TRUE; 
		while( blGoon && (ilLoop <= ilNoOfItems) )
		{
			get_real_item(clFina,pcpFields,ilLoop);
			get_real_item(clNew,pcpData,ilLoop);
			if ( FindItemInList(AFT_FIELDS,clFina,',',&ilIdx,&ilCol,&ilPos) == RC_SUCCESS )
			{
				strcpy ( clOld, AFTFIELD(pclRow,ilIdx) );
				TrimRight ( clOld );
				if ( !strcmp ( clOld," " ) )
					clOld[0] = '\0';
				TrimRight ( clNew );
				if ( !strcmp ( clNew," " ) )
					clNew[0] = '\0';
				if ( strcmp ( clNew, clOld ) )  /* significant change */
				{
					dbg ( TRACE, "IsAftChangeImportant: Field <%s> Old <%s> New <%s> are different",
						  clFina, clOld, clNew );
					if ( strstr ( clEvalFields, clFina ) )
						*pbpDoEval = TRUE;
					if ( strstr ( clConsFields, clFina ) )
						*pbpSetCons = TRUE;
					if ( strstr ( clConaFields, clFina ) )
						*pbpSetCona = TRUE;
					if ( strstr ( clFina, "RKEY" ) )
						sprintf ( cgChangedRkeys, "Where rkey in ('%s','%s')", clOld, clNew );
				}
				else
					dbg ( DEBUG, "IsAftChangeImportant: Field <%s> Old <%s> New <%s> are equal", 
						  clFina, clOld, clNew );
		
			}
			blGoon = (!*pbpDoEval || !*pbpSetCons || !*pbpSetCona );
			ilLoop++;
		}
	}
	dbg ( DEBUG, "IsAftChangeImportant: DoEval <%d>  SetCons <%d>  SetCona <%d>", 
				 *pbpDoEval, *pbpSetCons, *pbpSetCona );
	return ilRc;
}

/*  SetConaForFlight: Set actual confirmation for all status of type "AFT"	*/
/*	IN:		pcpUaft:	flight URNO, can be NULL for all flights			*/
/*			pcpUhss:	URNO of handling status sect., can be NULL for all  */
static int SetConaForFlight ( char *pcpUaft, char *pcpUhss )
{
	char clOstKey[31]="", *pclOstPtr, clCona[21];
	long llOstRow;
	long llTifr, llFieldNo=-1;
	int ilRc1, ilRc = RC_SUCCESS;
	char *pclUost, *pclRety, *pclUaft, *pclAftf;

	if ( pcpUaft && pcpUaft[0] )
	{
		strcpy ( clOstKey, pcpUaft );
		if ( pcpUhss && pcpUhss[0] )
		{
			strcat ( clOstKey, "," );
			strcat ( clOstKey, pcpUhss );
		}
	}

	dbg ( TRACE, "SetConaForFlight: key for Status to update <%s>", clOstKey );

	llOstRow = ARR_FIRST;
	while ( CEDAArrayFindRowPointer(&(rgOstArray.rrArrayHandle),
								    rgOstArray.crArrayName,
									&(rgOstArray.rrIdxHandle[2]),
									rgOstArray.crIdxName[2],clOstKey,
									&llOstRow, (void*)&pclOstPtr ) == RC_SUCCESS )
	{
		pclUost = OSTFIELD(pclOstPtr,igOstUrnoIdx);
		pclRety = OSTFIELD(pclOstPtr,igOstRetyIdx);
		pclUaft = OSTFIELD(pclOstPtr,igOstUaftIdx);
		pclAftf = OSTFIELD(pclOstPtr,igOstAftfIdx);
		if ( !strncmp( pclRety, "AFT", 3 ) )
		{	
			dbg ( TRACE, "SetConaForFlight: Processing UOST <%s> RETY <%s> RURN <%s> AFTF <%s>",
				  pclUost, pclRety, pclUaft, pclAftf );
			ilRc1 = GetConaFlight ( pclUaft, pclAftf, clCona );
			if ( ilRc1 == RC_SUCCESS )
				ilRc1 = CEDAArrayPutField (&rgOstArray.rrArrayHandle,
										   rgOstArray.crArrayName, &llFieldNo, 
										   "CONA", llOstRow, clCona );
			if ( ilRc1 |= RC_SUCCESS )
			{
				dbg ( TRACE, "SetConaForFlight: Update of Field <CONA> failed URNO <%s> UAFT <%s>", 
					   OSTFIELD(pclOstPtr,igOstUrnoIdx), OSTFIELD(pclOstPtr,igOstUaftIdx) );
				ilRc = RC_FAIL;
			}
		}
		else
			dbg ( DEBUG, "SetConaForFlight: Skipping UOST <%s> RETY <%s> RURN <%s> AFTF <%s>",
				  pclUost, pclRety, pclUaft, pclAftf );
		
		llOstRow = ARR_NEXT;
	}
	return ilRc;
}	

/*  SetInitialCONA: Set actual confirmation for all status (after creation)	*/
/*	IN:		pcpUaft:	flight URNO, can be NULL for all flights			*/
/*			pcpUhss:	URNO of handling status sect., can be NULL for all  */
static int SetInitialCONA ( char *pcpUaft, char *pcpUhss )
{
	char clOstKey[31]="", *pclOstPtr ;
	long llOstRow;
	long llTifr, llFieldNo=-1;
	int ilRc1, ilRc = RC_SUCCESS;
	char clUtplList[500]="", *pclUtpl, *pclOstUaft, *pclRety, *pclOstRurn ;
	BOOL blCcaStatDone = FALSE;
	BOOL blAftStatDone = FALSE;

	if ( pcpUaft && pcpUaft[0] )
	{
		strcpy ( clOstKey, pcpUaft );
		if ( pcpUhss && pcpUhss[0] )
		{
			strcat ( clOstKey, "," );
			strcat ( clOstKey, pcpUhss );
		}
	}

	dbg ( TRACE, "SetInitialCONA: key for Status to update <%s>", clOstKey );

	llOstRow = ARR_FIRST;
	while ( CEDAArrayFindRowPointer(&(rgOstArray.rrArrayHandle),
								    rgOstArray.crArrayName,
									&(rgOstArray.rrIdxHandle[1]),
									rgOstArray.crIdxName[1],clOstKey,
									&llOstRow, (void*)&pclOstPtr ) == RC_SUCCESS )
	{
		pclRety = OSTFIELD(pclOstPtr,igOstRetyIdx);
		pclOstUaft = OSTFIELD(pclOstPtr,igOstUaftIdx);
		pclOstRurn = OSTFIELD(pclOstPtr,igOstRurnIdx);

		if ( !blCcaStatDone && !strncmp( pclRety, "CCA", 3 ) )
		{	/*  we found the first status type CCAB or CCAE */
			ilRc1 = HandleCCAUpdate ( pclOstUaft );
			/* if only one flight is handled, HandleCCAUpdate won't be called again */
			if ( pcpUaft )  
				blCcaStatDone = TRUE;
		}
		else if ( !blAftStatDone && !strncmp( pclRety, "AFT", 3 ) )
		{	/*  we found the first status type CCAB or CCAE */
			ilRc1 = SetConaForFlight ( pclOstUaft, pcpUhss );
			/* if only one flight is handled, HandleCCAUpdate won't be called again */
			if ( pcpUaft )  
				blAftStatDone  = TRUE;
		}
		else if ( !strncmp( pclRety, "JOB", 3 ) )
		{
			pclUtpl = OSTFIELD(pclOstPtr,igOstUtplIdx) ; 
			if ( !IS_EMPTY(pclUtpl) && !strstr( clUtplList, pclUtpl ) )
			{
				ilRc1 = HandleJobUpdate ( pclOstUaft, pclUtpl );
				if ( pcpUaft )  
				{
					strcat ( clUtplList, pclUtpl );
					strcat ( clUtplList, "," );
				}
			}
		}
		if ( ilRc1 |= RC_SUCCESS )
		{
			dbg ( TRACE, "SetInitialCONA: Update of Field <CONA> failed URNO <%s> UAFT <%s>", 
				   OSTFIELD(pclOstPtr,igOstUrnoIdx), pclOstUaft );
			ilRc = RC_FAIL;
		}
		llOstRow = ARR_NEXT;
	}
	return ilRc;
}	


/******************************************************************************/
static BOOL IsRuleValid(char *pcpUrno, char *pcpUTCDate )
{
	int		ilRC = RC_SUCCESS, ilValid=0;			/* Return code */
	char    clDate[24]; 
	BOOL	blReturn = FALSE;

	strcpy ( clDate, pcpUTCDate );
	UtcToLocal( clDate );

	ilRC = CheckValidity("STATUS", pcpUrno, clDate, &ilValid);
	if (ilRC != RC_SUCCESS)
	{
		dbg (DEBUG, "IsRuleValid: CheckValidity failed <%d>", ilRC);
	}
	else
	{
		dbg(DEBUG, "IsRuleValid: <%s> is valid at <%s>", pcpUrno, pcpUTCDate);
		blReturn = TRUE;
	} /* end of if */

	return blReturn;
	
} /* end of IsRuleValid */

static int UtcToLocal(char *pcpTime)
{
	int ilRc = RC_SUCCESS;
	char *pclTdi;
	char clUtc[32];

	strcpy(clUtc,pcpTime);
	if ( cgTich[0] )
	{
		pclTdi = (strcmp(pcpTime,cgTich) < 0) ? cgTdi1 : cgTdi2;
		AddSecondsToCEDATime(pcpTime,(time_t)0+atol(pclTdi),1);
		dbg(DEBUG,"UtcToLocal: <%s> -> <%s> ", clUtc, pcpTime);
	}
	return ilRc;
}


static int InitTichTdi()
{
	/** read TICH,TDI1,TDI2 ***********/
	char pclDataArea[2256];
	char pclSqlBuf[256];
	short slCursor = 0;
	int   ilRc=RC_SUCCESS;

	sprintf(pclSqlBuf,
		"SELECT TICH,TDI1,TDI2 FROM APTTAB WHERE APC3 = '%s'",cgHopo);
	ilRc = sql_if(START,&slCursor,pclSqlBuf,pclDataArea);
	if (ilRc != DB_SUCCESS)
	{
		dbg(TRACE,"Home Airport <%s> not found in APTTAB %d",cgHopo,ilRc);
	} /* end while */
	else
	{
		strcpy(cgTdi1,"60");
		strcpy(cgTdi2,"60");
		get_fld(pclDataArea,0,STR,14,cgTich);
		get_fld(pclDataArea,1,STR,14,cgTdi1);
		get_fld(pclDataArea,2,STR,14,cgTdi2);
		TrimRight(cgTich);
		if (*cgTich != '\0')
		{
			TrimRight(cgTdi1);
			TrimRight(cgTdi2);
			sprintf(cgTdi1,"%d",atoi(cgTdi1)*60);
			sprintf(cgTdi2,"%d",atoi(cgTdi2)*60);
		}
		dbg(TRACE,"Tich: <%s> Tdi1: <%s> Tdi2 <%s>",
			cgTich,cgTdi1,cgTdi2);
	}
	close_my_cursor(&slCursor);
	slCursor = 0;
	return ilRc;
}

static int PrepareSBC ( char *pcpUaft, ARRAYINFO *prpAftArray )
{
	int ilRc = RC_SUCCESS;
	long llFlights=0, llRow;
	HANDLE slIdxHdl;
	char *pclRow, *pclRow2, clUrno[12];
	char clIdxNam[11]= "SDAYIDX";
	char clValue[124]="";

	cgBcField[0] = cgBcData[0] = '\0';

	if ( debug_level > TRACE )
	{		
		if(ReadConfigEntry("MAIN","BC_TYPE",clValue) == RC_SUCCESS)
		{
			bgSingleBC = ( strstr ( clValue, "SINGLE" ) ) ? TRUE : FALSE;
			bgSbcUaft = ( strstr ( clValue, "SBCUAFT" ) ) ? TRUE : FALSE;
			bgSbcSday = ( strstr ( clValue, "SBCSDAY" ) ) ? TRUE : FALSE;
		}
		else
			bgSingleBC = bgSbcUaft = bgSbcSday = TRUE;
	}
	if ( pcpUaft && (igMinUaftInSbc<=1) )
	{
		if ( bgSbcUaft )
		{
			strcpy ( cgBcData, pcpUaft );
			strcpy ( cgBcField, "UAFT" );
		}
		else
			ilRc = RC_IGNORE;
	}
	else if ( prpAftArray )
	{
		CEDAArrayGetRowCount(&(prpAftArray->rrArrayHandle),
							 prpAftArray->crArrayName, &llFlights );
		if ( llFlights <= 0 ) 
			return RC_NODATA;
		if ( llFlights < igMinUaftInSbc )
			ilRc = RC_IGNORE;
		else if ( llFlights <= igMaxUaftInSbc ) 
		{
			if ( bgSbcUaft )
			{
				strcpy ( cgBcField, "UAFT" );
				llRow = ARR_FIRST;
				while ( CEDAArrayGetRowPointer(&(prpAftArray->rrArrayHandle),
												prpAftArray->crArrayName,llRow,
												(void *)&pclRow) == RC_SUCCESS )

				{							
					strcpy ( clUrno, AFTFIELD(pclRow,igAftUrnoIdx) );
					TrimRight (  clUrno );
					if ( cgBcData[0] )
						strcat ( cgBcData, "," );
					strcat ( cgBcData, clUrno );
					llRow = ARR_NEXT;
				}
			}
			else
				ilRc = RC_IGNORE;
		}
		else
		{
			if ( bgSbcSday )
			{
				ilRc = CEDAArrayCreateSimpleIndexUp(&(rgNewOstArr.rrArrayHandle),
													rgNewOstArr.crArrayName,
													&slIdxHdl, clIdxNam, "SDAY" );
				if ( ilRc == RC_SUCCESS )
				{
					llRow = ARR_FIRST;
					ilRc = CEDAArrayFindRowPointer( &(rgNewOstArr.rrArrayHandle),
													 rgNewOstArr.crArrayName, 
													 &slIdxHdl, clIdxNam, "", 
													 &llRow, (void *)&pclRow);
					llRow = ARR_LAST;
					ilRc |= CEDAArrayFindRowPointer( &(rgNewOstArr.rrArrayHandle),
													 rgNewOstArr.crArrayName,
													 &slIdxHdl, clIdxNam, "", 
													 &llRow, (void *)&pclRow2);
					if ( ilRc == RC_SUCCESS )
					{
						strcpy ( cgBcField, "SDAY" );
						sprintf ( cgBcData, "%s-%s", OSTFIELD (pclRow, igOstSdayIdx ), 
								  OSTFIELD (pclRow2, igOstSdayIdx ) );
					}
					if ( CEDAArrayDestroyIndex( &(rgNewOstArr.rrArrayHandle),
												rgNewOstArr.crArrayName, &slIdxHdl,
												clIdxNam ) != RC_SUCCESS )
					{
						dbg ( TRACE, "PrepareSBC: CEDAArrayDestroyIndex failed IdxHdl <%d> IdxNam <%s>",
							  slIdxHdl, clIdxNam );
					}
				}
			}
			else
				ilRc = RC_IGNORE;
		}
	}
	if ( pcpUaft )
		dbg ( TRACE, "PrepareSBC: UAFT <%s> BC-Field <%s> BC-Data <%s> RC <%d>", 
			  pcpUaft, cgBcField, cgBcData, ilRc );		
	else
		dbg ( TRACE, "PrepareSBC: #Flights <%ld> BC-Field <%s> BC-Data <%s> RC <%d>", 
			  llFlights, cgBcField, cgBcData, ilRc );		
	return ilRc;
}

/*  Store earliest and last flight time for later use (reloading of OSTTAB */
static int UpdateMinMax ( char *pcpTime )
{
	if ( !pcpTime )
		return RC_INVALID;

	if ( !cgMinAftTime[0] || ( strcmp ( pcpTime, cgMinAftTime ) < 0 ) )
		strcpy ( cgMinAftTime, pcpTime );
	if ( strcmp(cgMaxAftTime, pcpTime ) < 0 )
		strcpy ( cgMaxAftTime, pcpTime );
	return RC_SUCCESS;
}

static int GetNomField ( char *pcpActField, char *pcpNomField, char *pcpRety )
{
	int ilIdx, ilCol, ilPos;
	int ilRc = RC_INVALID;
	
	if ( pcpActField && pcpNomField )
	{
		pcpNomField[0] = '\0';
		if ( !strncmp( pcpRety, "AFT", 3 ) )
		{
			ilRc = FindItemInList( ACT_FIELDS, pcpActField, ',', &ilIdx, &ilCol,&ilPos);
			if ( ilRc == RC_SUCCESS )
				get_real_item(pcpNomField,NOM_FIELDS,ilIdx);
		}
		else
		{
			if ( !strncmp ( pcpRety, "JOBC", 4 ) )
				strcpy ( pcpNomField, "PLFR" );
			else if ( !strncmp ( pcpRety, "JOBF", 4 ) )
					strcpy ( pcpNomField, "PLTO" );
			else if ( !strncmp ( pcpRety, "CCAB", 4 ) )
					strcpy ( pcpNomField, "CKBS" );
			else if ( !strncmp ( pcpRety, "CCAE", 4 ) )
					strcpy ( pcpNomField, "CKES" );
		}
		if ( pcpNomField[0] )
			ilRc = RC_SUCCESS ;
		dbg ( DEBUG, "GetNomField ACT <%s> NOM <%s> RC <%d>", pcpActField, pcpNomField, ilRc );
	}
	return ilRc;
}

/*  GetConsJob: get actual status confirmation time for jobs	 			*/
/*	IN:		pcpUaft:	flight URNO, must not be NULL						*/
/*			pcpUtpl:	template  of jobs that are used for the status		*/
/*			pcpRety:	type of reference JOBI(informed), JOBC(confirmed),	*/
/*						JOBF(finished)										*/
/*	OUT:	pcpCons:	scheduled confirmation time, if existing			*/
static int GetConsJob ( char *pcpUaft, char *pcpUaft2, char *pcpUtpl, char* pcpCons, char *pcpRety )
{
	char *pclJob, *pclPlto, *pclPlfr, *pclFlights[2];
	long llRow = ARR_FIRST;
	int  i, ilType=0, ilJobs=0, ilRc = RC_SUCCESS;
	char clKey[31] ;
	
	if ( !pcpUaft || !pcpCons || !pcpRety || !pcpUtpl || IS_EMPTY(pcpUtpl) )
	{
		dbg ( TRACE, "GetConsJob: At least one parameter is NULL" );
		return RC_INVALID;
	}

	pcpCons[0] = '\0';
	if ( !strncmp(pcpRety, "JOBC", 4 ) )
	{
		ilType = 2;
	}
	else if ( !strncmp(pcpRety, "JOBF", 4 ) )
	{
		ilType = 3;
	}
	else
	{
		dbg ( TRACE, "GetConsJob: Unhandled status type <%s>", pcpRety );
		return RC_INVALID;
	}
	pclFlights[0] = pcpUaft;
	pclFlights[1] = pcpUaft2;
	for ( i=0; (i<2) && !IS_EMPTY(pclFlights[i]); i++ )
	{
		llRow = ARR_FIRST;
		sprintf ( clKey, "%s,%s", pclFlights[i], pcpUtpl );
		dbg ( DEBUG, "GetConsJob: Jobtab-Key <%s> ilType <%d>", clKey, ilType );
		while ( CEDAArrayFindRowPointer(&(rgJobArray.rrArrayHandle),
										rgJobArray.crArrayName,
										&(rgJobArray.rrIdxHandle[1]),
										rgJobArray.crIdxName[1], clKey, &llRow,
										(void *)&pclJob ) == RC_SUCCESS )
		{
			ilJobs ++;
			pclPlfr	= JOBFIELD(pclJob,igJobPlfrIdx);
			pclPlto = JOBFIELD(pclJob,igJobPltoIdx);
			dbg ( DEBUG, "GetConsJob: Found job RETY <%s> PLFR <%s> PLTO <%s>", 
				  pcpRety, pclPlfr, pclPlto );
			switch ( ilType )
			{
				case 2:		/*  Job confirmed, at least one must be confirmed */
					if ( IS_EMPTY(pcpCons) || (strcmp(pcpCons,pclPlfr)> 0) )
						strcpy (pcpCons, pclPlfr ) ;
					break;
				case 3:		/*  Job finished, all must be finished */
					if ( IS_EMPTY(pcpCons) || (strcmp(pcpCons,pclPlto)< 0) )
						strcpy (pcpCons, pclPlto ) ;
					break;
			}
			llRow = ARR_NEXT;
		}
	}
	dbg ( TRACE, "GetConsJob: RETY <%s> UAFT <%s> FLT2 <%s> UTPL <%s> <%d> jobs found => CONS <%s>", 
		  pcpRety, pcpUaft, pcpUaft2, pcpUtpl, ilJobs, pcpCons  );
	if ( !ilJobs )
		ilRc = RC_NOTFOUND;
	return ilRc;
}


/*  GetConsCca: get scheduled status time for check-in counters				*/
/*	IN:		pcpUaft:	flight URNO, must not be NULL						*/
/*			pcpRety:	type of reference CCAB(open), CCAE(closed)			*/
/*	OUT:	pcpCons:	confirmation time, if existing						*/
static int GetConsCca ( char *pcpUaft, char* pcpCons, char *pcpRety )
{
	char *pclCcaRow, *pclVal;
	long llRow = ARR_FIRST;
	int  ilFldIdx, ilKeyIdx=1;
	int  ilCounters = 0;
	
	if ( !pcpUaft || !pcpCons || !pcpRety )
	{
		dbg ( TRACE, "GetConsCca: At least one parameter is NULL" );
		return RC_INVALID;
	}
	
	pcpCons[0] = '\0';

	if ( !strncmp(pcpRety, "CCAB", 4 ) )
		ilFldIdx = igCcaCkbsIdx;
	else if ( !strncmp(pcpRety, "CCAE", 4 ) )
	{
		ilFldIdx = igCcaCkesIdx;
		ilKeyIdx=2;
	}
	else
	{
		dbg ( TRACE, "GetConaCca: Unknown status type <%s>", pcpRety );
		return RC_INVALID;
	}
	while ( CEDAArrayFindRowPointer(&(rgCcaArray.rrArrayHandle),
								    rgCcaArray.crArrayName,
								    &(rgCcaArray.rrIdxHandle[ilKeyIdx]),
									rgCcaArray.crIdxName[ilKeyIdx], pcpUaft, 
									&llRow,(void *)&pclCcaRow ) == RC_SUCCESS )
	{
		ilCounters++;
		pclVal = CCAFIELD(pclCcaRow,ilFldIdx);
		if ( !IS_EMPTY ( pclVal ) )
		{
			switch ( ilKeyIdx )
			{
				case 1:
					if ( IS_EMPTY(pcpCons) || (strcmp(pcpCons,pclVal)> 0) )
						strcpy (pcpCons, pclVal ) ;
					break;
				case 2:
					if ( IS_EMPTY(pcpCons) || (strcmp(pcpCons,pclVal)< 0) )
						strcpy (pcpCons, pclVal ) ;
					break;
			}
		}
		llRow = ARR_NEXT;
	}
	if ( !ilCounters ) 
	{
		dbg ( TRACE, "GetConsCca: No CCA-record found for UAFT <%s>", pcpUaft );
		return RC_NOTFOUND;
	}
	dbg ( TRACE, "GetConsCca: RETY <%s> UAFT <%s> %d records found => CONS <%s>", 
		  pcpRety, pcpUaft, ilCounters, pcpCons  );
	return RC_SUCCESS;
}

static int UpdateStatus ( long lpOldRow, char *pcpOstOld, char *pcpOstNew )
{
	char *clCmpFlds[] = { "RTAB","RURN","RFLD",NULL };
	int i=0, ilIdx, ilPos, ilCol, ilRc = RC_SUCCESS, ilRc1;
	char *pclOld, *pclNew;
	long llFieldNo;
	char clBuffer[101];

	clBuffer[100] = '\0';

	while ( clCmpFlds[i] )
	{
		FindItemInList( OST_FIELDS, clCmpFlds[i], ',', &ilIdx, &ilCol,&ilPos);
		if ( ilIdx > 0 )
		{
			pclOld = OSTFIELD (pcpOstOld,ilIdx );
			pclNew = OSTFIELD (pcpOstNew,ilIdx );
			if ( strcmp ( pclOld, pclNew ) )
			{
				if ( debug_level > TRACE )
					strncpy ( clBuffer, pclOld, 100 ); /* save old value for dbg */
				llFieldNo = -1;
				ilRc1 = CEDAArrayPutField (&rgOstArray.rrArrayHandle,
										   rgOstArray.crArrayName, &llFieldNo, 
										   clCmpFlds[i], lpOldRow, pclNew );
				if ( ilRc1 != RC_SUCCESS )
				{
					ilRc = RC_FAIL;
					dbg ( TRACE, "UpdateStatus: CEDAArrayPutField FINA <%s> VALU <%s> Row <%ld> failed RC <%d>", 
						  clCmpFlds[i], pclNew, lpOldRow, ilRc1 );
				}		
				else
					dbg ( DEBUG, "UpdateStatus: updated field <%s> Old <%s> new <%s> successfully", 
						  clCmpFlds[i], clBuffer, pclNew );

			}
		}
		i++;
	}	
	return ilRc ;
}					
				

static int Find2ndFlight ( ARRAYINFO *prpFlightArr, char *pcpAftRow1, char **pcpAftRow2 )
{
	char clKey[21];
	long llRow=ARR_FIRST;
	char *pclAdid;
	int ilRc;

	pclAdid = AFTFIELD(pcpAftRow1,igAftAdidIdx );
	if ( *pclAdid == 'A' )
		sprintf ( clKey, "%s,D", AFTFIELD(pcpAftRow1,igAftRkeyIdx ) );
	else
		sprintf ( clKey, "%s,A", AFTFIELD(pcpAftRow1,igAftRkeyIdx ) );
	ilRc = CEDAArrayFindRowPointer (&(prpFlightArr->rrArrayHandle),
									prpFlightArr->crArrayName,
									&(prpFlightArr->rrIdxHandle[1]),
									prpFlightArr->crIdxName[1],
									clKey ,&llRow, (void *) pcpAftRow2 );
	if ( ilRc == RC_SUCCESS )
		dbg ( DEBUG, "Find2ndFlight: Found UAFT2 <%s> using key <%s>", 
			  AFTFIELD(*pcpAftRow2,igAftUrnoIdx), clKey );
	else
	{
		dbg ( DEBUG, "Find2ndFlight: Didn't find 2nd Flight using key <%s> RC <%d>", clKey, ilRc );
		*pcpAftRow2 = 0;
	}
	return ilRc;
}

static BOOL CheckOneRule ( char *pcpRule, char *pcpInbound, char *pcpOutbound, long lpActGroundTime )
{
	char *pclTime, *pclUsrh, *pclSrhAdid, *pclMaxt;
	BOOL blRet = TRUE;
	int i;
	long llMaxt;
	
	pclUsrh = SRHFIELD(pcpRule,igSrhUrnoIdx );
	pclSrhAdid = SRHFIELD(pcpRule,igSrhAdidIdx );
	pclMaxt = SRHFIELD(pcpRule,igSrhMaxtIdx );

	if ( !pcpInbound && (*pclSrhAdid != 'D') ) 
	{
		dbg ( DEBUG, "CheckOneRule: Event type mismatch, Single outbound vs. SRH.ADID <%s>", pclSrhAdid );
		return FALSE;
	}
	if ( !pcpOutbound && (*pclSrhAdid != 'A') ) 
	{
		dbg ( DEBUG, "CheckOneRule: Event type mismatch, Single inbound vs. SRH.ADID <%s>", pclSrhAdid );
		return FALSE;
	}
	if ( pcpInbound )
	{
		pclTime = AFTFIELD(pcpInbound,igAftTifaIdx);
		if ( !IsRuleValid( pclUsrh, pclTime ) ) 
		{
			dbg ( DEBUG, "CheckOneRule: Rule USRH <%s> is not valid for inbound TIFA <%s>", pclUsrh, pclTime );
			return FALSE;
		}
	}
	if ( pcpOutbound )
	{
		pclTime = AFTFIELD(pcpOutbound,igAftTifdIdx);
		if ( !IsRuleValid( pclUsrh, pclTime ) ) 
		{
			dbg ( DEBUG, "CheckOneRule: Rule USRH <%s> is not valid for outbound TIFD <%s>", pclUsrh, pclTime );
			return FALSE;
		}
	}
	/* Check MAXT */
	if (  (*pclSrhAdid == 'T') && pcpInbound && pcpOutbound )
	{
		llMaxt = atol(pclMaxt) * 60;
		if ( lpActGroundTime > llMaxt )
		{
			dbg ( DEBUG, "CheckOneRule: Act. Groundtime <%ld> > MAXT <%ld> [seconds]", lpActGroundTime, llMaxt );
			return FALSE;
		}
	}
	for ( i=0; blRet && (i<COND_COUNT); i++ )
	{
		if ( pcpInbound && (*pclSrhAdid != 'D') )
		{
			blRet = CheckSingleValue ( &(rgBasicData[i]), 
										SRHFIELD(pcpRule,rgCndFldIdxSgl[i]),
										AFTFIELD(pcpInbound,rgAftInbFldIdx[i]) );
			if  ( blRet )
				blRet = CheckGroupValue ( &(rgBasicData[i]), 
										   SRHFIELD(pcpRule,rgCndFldIdxGrp[i]),
										   AFTFIELD(pcpInbound,rgAftInbFldIdx[i]) );
		}	
		if ( pcpOutbound && (*pclSrhAdid != 'A') )
		{
			blRet = CheckSingleValue (&(rgBasicData[i]), 
									  SRHFIELD(pcpRule,rgCndFldIdxSgl[i]),
									  AFTFIELD(pcpOutbound,rgAftOutFldIdx[i]) );
			if  ( blRet )
				blRet = CheckGroupValue (&(rgBasicData[i]), 
									     SRHFIELD(pcpRule,rgCndFldIdxGrp[i]),
										 AFTFIELD(pcpOutbound,rgAftOutFldIdx[i]) );
		}	
	}	
	return blRet;
}


static int CreateSkeleton4Rule ( ARRAYINFO *prpFlights, char *pcpRule, 
								 char *pcpOuri, char *pcpOuro, char *pcpUhss )
{
	char clSdeKey[21], clOstBuf[251];
	char	*pclSdePtr;
	long llSdeRow, llOstRow; 
	int ilRc = RC_SUCCESS, ilRc1;
 
	sprintf ( clSdeKey, "%c,%s", cgFlagActive, pcpRule ); 

	dbg ( DEBUG, "CreateSkeleton4Rule: Create Status UAFTs <%s><%s> SDE-Key <%s>", 
				  pcpOuri, pcpOuro, clSdeKey );	
	llSdeRow = ARR_FIRST;
	while ( CEDAArrayFindRowPointer(&(rgSdeArray.rrArrayHandle),
									rgSdeArray.crArrayName,
									&(rgSdeArray.rrIdxHandle[1]),
									rgSdeArray.crIdxName[1],clSdeKey,
									&llSdeRow, (void*)&pclSdePtr ) == RC_SUCCESS )
	{
		ilRc1 = FillOstRecord( prpFlights, clOstBuf, sizeof(clOstBuf),
							   pclSdePtr, pcpOuri, pcpOuro, pcpUhss );
		if ( ilRc1 != RC_SUCCESS )
		{
			dbg ( TRACE, "CreateSkeleton4Rule: FillOstRecord failed UAFTs <%s><%s> RC <%d>",
				  pcpOuri, pcpOuro, ilRc1 );
			ilRc = RC_FAIL;
		}
		else
		{
			llOstRow = ARR_LAST;
			ilRc1 = CEDAArrayAddRow (&rgNewOstArr.rrArrayHandle, 
									rgNewOstArr.crArrayName, &llOstRow, 
									(void*)clOstBuf );
			if ( ilRc1 != RC_SUCCESS )
			{
				dbg ( TRACE, "CreateSkeleton4Rule: FillOstRecord failed UAFTs <%s><%s> RC <%d>",
					  pcpOuri, pcpOuro, ilRc1 );
				ilRc = RC_FAIL;
			}	
		}
		llSdeRow = ARR_NEXT;
	}
	return ilRc;
}

static int PrepareSdeArray ( char *pcpUsrh )
{
	char clKey[21]="1";		/* key on USRH */
	long llSdeRow, llSrhRow, llPredRow, llLevlIdx=-1, llAdidIdx=-1;
	char *pclSdePtr, *pclFldr, *pclAftf, *pclRety, *pclSrhPtr, *pclSrhAdid ;
	int ilPos, ilCol, ilIdx, ilRc = RC_SUCCESS;
	char clAdid[5], clLevl[5], clPredLevl[5];
	char *pclLevl, *pclAdid, *pclSdeu, *pclPredecessor, *pclUsrh;
	BOOL blAnythingChanged;

	dbg ( TRACE, "PrepareSdeArray: Start pcpUsrh <%s>", pcpUsrh );
	llSrhRow = ARR_FIRST;
	while ( CEDAArrayFindRowPointer(&(rgSrhArray.rrArrayHandle),
									rgSrhArray.crArrayName,
									&(rgSrhArray.rrIdxHandle[0]),
									rgSrhArray.crIdxName[0],pcpUsrh,
									&llSrhRow, (void*)&pclSrhPtr ) == RC_SUCCESS )
	{
		llSrhRow = ARR_NEXT;
		sprintf ( clKey, "1,%s", SRHFIELD(pclSrhPtr,igSrhUrnoIdx) );
		pclSrhAdid = SRHFIELD(pclSrhPtr,igSrhAdidIdx) ;
		dbg ( DEBUG, "PrepareSdeArray: Key for SDE-search <%s>", clKey );
		llSdeRow = ARR_FIRST;
		while ( CEDAArrayFindRowPointer(&(rgSdeArray.rrArrayHandle),
										rgSdeArray.crArrayName,
										&(rgSdeArray.rrIdxHandle[1]),
										rgSdeArray.crIdxName[1],clKey,
										&llSdeRow, (void*)&pclSdePtr ) == RC_SUCCESS )
		{
			pclFldr = SDEFIELD(pclSdePtr,igSdeFldrIdx);
			pclAftf = SDEFIELD(pclSdePtr,igSdeAftfIdx);
			pclRety = SDEFIELD(pclSdePtr,igSdeRetyIdx);
			strcpy ( clLevl, "00" );
			/* dbg ( DEBUG, "PrepareSdeArray: found SDE-URNO <%s> RETY <%s> FLDR <%s> AFTF <%s>", 
						  SDEFIELD(pclSdePtr,igSdeUrnoIdx),pclRety,pclFldr, pclAftf );*/
			if ( strncmp	( pclFldr, "PLAN", 4 ) == 0 )
			{	/* reference time is planned time of event */
				if ( strncmp ( pclRety, "CCA", 3 ) == 0 )
					strcpy ( clAdid, "D" );		
				else if ( strncmp ( pclRety, "AFT", 3 ) == 0 )
				{
					ilIdx = -1;
					FindItemInList( INBOUNDTIMES, pclAftf, ',', &ilIdx, &ilCol,&ilPos);
					if ( ilIdx > 0 )
						strcpy ( clAdid, "A" );
					else
						strcpy ( clAdid, "D" );
				}
				else
				{	/* referenced object is JOB */
					strcpy ( clAdid, pclSrhAdid );	
				}

			}
			else if ( strcmp	( pclFldr, "CONA" ) == 0 )
			{
				strcpy ( clAdid, "-" );
				strcpy ( clLevl, "-1" );
			}
			else
			{
				ilIdx = -1;
				FindItemInList( INBOUNDTIMES, pclFldr, ',', &ilIdx, &ilCol,&ilPos);
				if ( ilIdx > 0 )
					strcpy ( clAdid, "A" );
				else
					strcpy ( clAdid, "D" );
			}
			dbg ( DEBUG, "PrepareSdeArray: set LEVL <%s> ADID <%s> for SDE-URNO <%s>", 
						  clLevl, clAdid, SDEFIELD(pclSdePtr,igSdeUrnoIdx));

			CEDAArrayPutField( &rgSdeArray.rrArrayHandle,
								rgSdeArray.crArrayName, &llLevlIdx, "LEVL",
								llSdeRow, clLevl );	
			CEDAArrayPutField( &rgSdeArray.rrArrayHandle,
								rgSdeArray.crArrayName, &llAdidIdx, "ADID", 
								llSdeRow, clAdid );	
			llSdeRow = ARR_NEXT;
		}	
	}
	
	dbg ( TRACE, "PrepareSdeArray: First initialisation done" );
	do 
	{
		blAnythingChanged = FALSE;
		llSdeRow = ARR_FIRST;
		while ( CEDAArrayFindRowPointer(&(rgSdeArray.rrArrayHandle),
										rgSdeArray.crArrayName,
										&(rgSdeArray.rrIdxHandle[1]),
										rgSdeArray.crIdxName[1], "",
										&llSdeRow, (void*)&pclSdePtr ) == RC_SUCCESS )
		{
			pclLevl = SDEFIELD(pclSdePtr,igSdeLevlIdx);
			pclSdeu = SDEFIELD(pclSdePtr,igSdeSdeuIdx);
			pclUsrh = SDEFIELD(pclSdePtr,igSdeUrnoIdx);
			if ( *pclLevl == '-' )
			{
				llPredRow = ARR_FIRST;
				if ( CEDAArrayFindRowPointer(&(rgSdeArray.rrArrayHandle),
											 rgSdeArray.crArrayName,
											 &(rgSdeArray.rrIdxHandle[0]),
											 rgSdeArray.crIdxName[0],pclSdeu,
											 &llPredRow, (void*)&pclPredecessor ) == RC_SUCCESS )
				{
					strcpy ( clPredLevl, SDEFIELD(pclPredecessor,igSdeLevlIdx) );
					if ( *clPredLevl == '-' )
						dbg ( DEBUG, "PrepareSdeArray: Predecessor SDEU <%s> not yet initialised LEVL <%s>", 
							  pclSdeu, clPredLevl );
					else
					{
						pclAdid = SDEFIELD(pclPredecessor,igSdeAdidIdx);
						ilPos = atoi(clPredLevl);
						dbg ( DEBUG, "PrepareSdeArray: Found initialised predecessor SDEU <%s> LEVL <%s> ADID <%s>", 
							  pclSdeu, clPredLevl, pclAdid );
						sprintf ( clLevl, "%02d", ilPos + 1 );
						CEDAArrayPutField( &rgSdeArray.rrArrayHandle,
											rgSdeArray.crArrayName, &llLevlIdx, "LEVL",
											llSdeRow, clLevl );	
						CEDAArrayPutField( &rgSdeArray.rrArrayHandle,
											rgSdeArray.crArrayName, &llAdidIdx, "ADID", 
											llSdeRow, pclAdid );	
						blAnythingChanged = TRUE;
					}
				}
				else
				{
					dbg ( TRACE, "PrepareSdeArray: Didn't find predecessor SDEU <%s> USRH <%s>", 
						  pclSdeu, pclUsrh );
					ilRc = RC_NOTFOUND;
				}
			}
			llSdeRow = ARR_NEXT;
		}
	}while (blAnythingChanged);

	ilRc = CEDAArrayWriteDB(&rgSdeArray.rrArrayHandle, rgSdeArray.crArrayName,
							NULL,NULL,NULL,NULL,ARR_COMMIT_ALL_OK);

	dbg ( TRACE, "PrepareSdeArray: End ilRC <%d>", ilRc );
	return ilRc;
}

/*  if Status is based on a turnaround rule, get the 2nd flight URNO */
static int GetFlt2Value ( ARRAYINFO *prpFlightArr, char *pcpUaft1, 
						  char *pcpUsrh, char *pcpUaft2 )
{
	long llSrhRow = ARR_FIRST;
	char *pclSrhRes = 0;
	int ilRc;

	if ( !prpFlightArr || !pcpUaft1 || !pcpUsrh || !pcpUaft2 )
		return RC_INVALID;
	pcpUaft2[0] = '\0';
	dbg ( DEBUG, "GetFlt2Value: UAFT1 <%s>, USRH <%s>", pcpUaft1, pcpUsrh );
	ilRc = CEDAArrayFindRowPointer(&(rgSrhArray.rrArrayHandle),
								 rgSrhArray.crArrayName,
								 &(rgSrhArray.rrIdxHandle[0]),
								 rgSrhArray.crIdxName[0], pcpUsrh, 
								 &llSrhRow, (void*)&pclSrhRes ) ;
	if ( ilRc == RC_SUCCESS )
	{
		if ( *SRHFIELD(pclSrhRes,igSrhAdidIdx) != 'T' )
			ilRc = RC_DONT_CARE;
	}
	if ( ilRc == RC_SUCCESS )
	{	/* we found a turnaround rule responsible for the status */
		ilRc = Get2ndFlightUrno ( prpFlightArr, pcpUaft1, pcpUaft2 );
	}
	dbg ( DEBUG, "GetFlt2Value: UAFT1 <%s>, USRH <%s> UAFT2 <%s> RC <%d>", pcpUaft1, pcpUsrh, pcpUaft2, ilRc );
	return ilRc;
}		


static int Get2ndFlightUrno ( ARRAYINFO *prpFlightArr, char *pcpUaft1, char *pcpUaft2 )
{
	long llAftRow = ARR_FIRST;
	char *pclAftRes = 0, *pcl2ndFlightRow;
	int ilRc;

	if ( !pcpUaft1 || !pcpUaft2 || !prpFlightArr )
		return RC_INVALID;

	pcpUaft2[0] = '\0';
	ilRc = CEDAArrayFindRowPointer(&(prpFlightArr->rrArrayHandle),
								   prpFlightArr->crArrayName,
								   &(prpFlightArr->rrIdxHandle[0]),
								   prpFlightArr->crIdxName[0], pcpUaft1, 
								   &llAftRow, (void*)&pclAftRes ) ;
	if ( ilRc == RC_SUCCESS )
		ilRc = Find2ndFlight ( prpFlightArr, pclAftRes, &pcl2ndFlightRow );
	if (ilRc == RC_SUCCESS) 
	{
		if ( !pcl2ndFlightRow )
			ilRc = RC_INVALID;
		else
			strcpy ( pcpUaft2, AFTFIELD (pcl2ndFlightRow, igAftUrnoIdx) );
	}
	return ilRc;
}



static int HandleAf1Update ( char *pcpFlnu )
{
	char	clUrno[21], *pclFca1, *pclFca2, *pclOstPtr, *pclAf1Row;
	long	llOstRow, llFieldNo=-1, llAf1Row=ARR_FIRST;
	int		ilRc=RC_SUCCESS, ilRc1=RC_SUCCESS;

	if ( !pcpFlnu )
	{
		dbg ( TRACE, "HandleAf1Update: pcpFlnu is NULL" );
		return RC_INVALID;
	}
	if ( CEDAArrayFindRowPointer(&(rgAf1Array.rrArrayHandle),
								 rgAf1Array.crArrayName,
								 &(rgAf1Array.rrIdxHandle[1]),
								 rgAf1Array.crIdxName[1],pcpFlnu,
								 &llAf1Row, (void*)&pclAf1Row ) != RC_SUCCESS )
		return RC_NOTFOUND;

	pclFca1 = AF1FIELD(pclAf1Row, igAf1Fca1Idx);
	pclFca2 = AF1FIELD(pclAf1Row, igAf1Fca2Idx);

	/*  search all OST-records for flight with AFTF =='FCA1' or AFTF =='FCA2' */
	llOstRow = ARR_FIRST;
	while ( CEDAArrayFindRowPointer(&(rgOstArray.rrArrayHandle),
									rgOstArray.crArrayName,
									&(rgOstArray.rrIdxHandle[1]),
									rgOstArray.crIdxName[1],pcpFlnu,
									&llOstRow, (void*)&pclOstPtr ) == RC_SUCCESS )
	{
		if ( !strncmp( OSTFIELD(pclOstPtr,igOstAftfIdx), "FCA1", 4 ) )
		{	
			dbg ( TRACE, "HandleAf1Update: Setting <CONA> to <%s> UOST <%s> for flight <%s>", 
				 pclFca1, OSTFIELD(pclOstPtr,igOstUrnoIdx), pcpFlnu );
			ilRc1 |= CEDAArrayPutField (&rgOstArray.rrArrayHandle,
										rgOstArray.crArrayName, &llFieldNo, 
										"CONA", llOstRow, pclFca1 );
		}
		if ( !strncmp( OSTFIELD(pclOstPtr,igOstAftfIdx), "FCA2", 4 ) )
		{	
			dbg ( TRACE, "HandleAf1Update: Setting <CONA> to <%s> UOST <%s> for flight <%s>", 
	   					 pclFca2, OSTFIELD(pclOstPtr,igOstUrnoIdx), pcpFlnu );
			 ilRc1 |= CEDAArrayPutField (&rgOstArray.rrArrayHandle,
										rgOstArray.crArrayName, &llFieldNo, 
										"CONA", llOstRow, pclFca2 );
		}
		llOstRow = ARR_NEXT;
	}
	if ( ilRc1 != RC_SUCCESS )
	{
		dbg ( TRACE, "HandleAf1Update: Update of Field <CONA> failed for flight <%s>", pcpFlnu );
		ilRc = RC_FAIL;
	}/* end of if */
	
	return ilRc;
}

