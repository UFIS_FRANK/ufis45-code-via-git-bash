<?php

/*
Note: Since PHP 5.1.0 (when the date/time functions were rewritten), every call to a date/time function will generate a E_NOTICE if the timezone isn't valid, and/or a E_STRICT message if using the system settings or the TZ environment variable. 
*/

//date_default_timezone_set("UTC");

//----------------------- Connection Section ----------------------- 
//Define the Connection String
$DatabaseType="oci8";
$dbusername="prm";
$dbpassword="prm";
$database="PRM";
$servername="PRM";



$DatabaseTypeMysql="mysqlt";
$dbusernameMysql="root";
$dbpasswordMysql="";
$databaseMysql="cute_fids";
// Oracle Home variable
//putenv("ORACLE_HOME=/app/home/oracle_client/product/10.1.0.2");

//----------------------- Authorization   Section ----------------------- 
// AuthType Please uncoment what you want

//Authedication Type
//$AuthType="Mysql";
//$AuthType="Oracle";
//$AuthType="Variable"; // + File  , Set the Loginhopo
$AuthType="File";
//if AuthType== FILE || AuthType==Variable specifie the directory with the users,passwd file
$UserFilename="db/users.txt";

	
//THE HOPO  of the Airport
$hopo = "ATH";
$Loginhopo = "ATH"; // Must be provided from SITA


//----------------------- Cput   Section ----------------------- 
// The RSH server For the cput Command
// Leave blank ($RSHIP="";) if the cput command is in the local server

//$RSHIP="ufislh";
$RSHIP="prmappl.aia.gr";
// The comand tha we want to use in order to execute the remote script
//$RSCMD="/usr/bin/rexec -l username -p passwd ";
$RSCMD="/usr/bin/rsh -n ";

//$CputPath="/ceda/etc/cput45";
//The Cput File
$CputPath="/ceda/etc/kshCput";

// Page Title
$PageTitle="PRM INTERFACE INTERNATIONAL";

//----------------------- Messages ------------------------------

//$RedGoodByeMsg="PLEASE REMEMBER TO CLOSE THE CHECK-IN FLAP DOORS <br>THANK YOU !";
$RedGoodByeMsg="";

// Cput parameteres
//$wks="\'".$HostName."\'";
$wks="\'EXCO\'";
$cputuser="\'PRM\'";
$TW_START="\'0\'";
$TW_END="\'$hopo,TAB,PRM\'";

//----------------------- TemplatePath ------------------------------

//TemplatePath
$tmpl_path="templates/";

// Template Array
$tmpl_array=array();
// Main Template name
$maintmpl="";


//------ HG Groups --//

$swiss =   "'TTF','ABP','AAB','BVR','NRO','CJE','BES','GBJ','UCR','ADN','NRP','RTE','OVA','UAR','MMD','AMA','FIF','JTV','DLY',";
$swiss = $swiss ."'AJU','LZR','AMC','AOE','ACG','SFB','ATJ','TSC','FIX','JAR','UMB','LBC','PAJ','LVN','ELG','XPE','ADI','AUF','DEF',";
$swiss = $swiss ."'AJF','AIA','AUL','AXY','BBA','BHP','BLI','BAF','BXA','CKM','BLM','VOL','BPA','BMW','BOO','BAW','LZB','BGF','CBI',";
$swiss = $swiss ."'GLJ','OTL','CLS','EXS','RUS','SDR','FYN','CLA','COE','COA','CYP','DCS','DAF','DTR','DWT','DAL','AMB','UDN','DBK','DUK',";
$swiss = $swiss ."'EAB','EZE','EIR','LBR','EJD','UAE','EUP','GOJ','FPO','VIP','VMP','EJM','IFA','FAH','FLT','FTY','FYA','FYG','FLK','FXR',";
$swiss = $swiss ."'FGL','FUA','GMA','GZP','GAF','GWI','GES','GJA','GDA','GDK','GRE','GZA','HTG','GSJ','HLX','HFA','HEA','IMP',";
$swiss = $swiss ."'FHE','HMS','HER','ICJ','ILL','JII','ITN','IFT','IJM','ISF','TIH','JCB','JAG','JCX','JEI','JEK','JDI','JEF','JNV','JEP',";
$swiss = $swiss ."'KLM','LEA','WGT','LTU','MND','MHN','MPJ','TFG','MYO','SUM','MEA','MEM','ISS','MSA','MON','MSI','NKL','NJE','NTJ','NBL',";
$swiss = $swiss ."'NFA','NAX','OAD','PEA','BAT','PMU','PTI','PTG','PWF','PLM','QFA','QTR','MTL','RAQ','RYT','RDP','ROJ','RJA','NAF','RSJ',";
$swiss = $swiss ."'RYR','RLS','MOZ','SNM','SHE','SVW','SIW','SIO','SEH','SRK','KYB','DAT','OKT','SOX','ONG','KSA','SUS','MDT','SVF','SAZ',";
$swiss = $swiss ."'CRX','SWR','TGM','FPG','TAG','ROT','THA','TCX','THZ','AWC','TLY','TSI','TSY','TWJ','TYW','TJS','VEA','VIB','VIK','MOV',";
$swiss = $swiss ."'7W','PRX','VPA','VEX','VLE','JET','QGA','WNR','MJS','KLM','TUB','JAF','FIN'";


$oa = "'BJT','ADE','ADR','AEW','ITE','FAS','ACL','CCA','AEA','AEY','BIE','BER','UDC','AXN','AZA',";
$oa = $oa . "'AZE','AIZ','ASE','AEU','AMT','OGE','AUA','AVW','JOR','BAL','TOM','BMA','CFI','CLI','CST',";
$oa = $oa . "'CFG','CRL','CSA','DNV','EDW','FSD','MSR','ELY','ECA','EEZ','JLN','EAF','XLA','FCA',";
$oa = $oa . "'FTL','EXH','GNJ','TGZ','GJT','HHI','HLF','OAW','HOA','IBE','IWD','NSK','ISR','JAT',";
$oa = $oa . "'JXX','KNI','MVD','KIL','LLA','LVG','LTE','LGL','DAN','MPH','BSK','MYT','VKG','NOS',";
$oa = $oa . "'NLY','NRD','NDC','NVR','OLY','OAL','ORF','PEV','PVG','QAJ','REI','RMV','SBU','SOV','SNA',";
$oa = $oa . "'FFD','SUA','SIA','ESK','HSK','SLL','JKK','SEU','SNB','JTT','ERO','SYR','TMI','TCW','TDR',";
$oa = $oa . "'TRA','TVS','TVL','TFL','TAR','TJT','AUI','UZB','VDA','WLC','XLF',";
$oa = $oa . "'IHE','INJ','AZA','HEJ'";

$interjet = "'IHE','INJ'";

$ga = "'AEE','EIN','AFL','ALD','AEN','AFM','AAW','AAF','AHR','ARR','BTI','ABR','AFR','MHS','MLD',";
$ga = $ga ."'ADH','SCU','SVK','URG','WLR','DRU','ADB','RNV','KKK','CLU','VBC','BGH','BID','BLF','BLE',";
$ga = $ga ."'BRW','BPS','KRP','CAZ','CLW','CNB','CTN','DGX','DSO','BAG','DMO','DUB','EZY','EZS','EFD',";
$ga = $ga ."'NLK','ELL','JNL','ERJ','EIA','XJC','EXU','CLB','FWK','GSM','FHY','HCY','HUF','IBZ','IRA',";
$ga = $ga ."'IRL','ISD','PJS','JBR','KSJ','KAJ','KGL','KJC','LNX','LOT','DLH','MAH','NOY','EBF','MYW',";
$ga = $ga ."'VCX','OHY','ORE','PIA','POT','PLK','RBB','SDM','SVA','SAS','CGL','SWZ','SXP','IGA','GSW',";
$ga = $ga ."'SPW','SXS','SWT','TAP','TIS','TAY','TUL','THY','UVN','USA','VLG','PIX','WZZ',";
$ga = $ga ."'GFA','MYW','ADH'";

// for the  Ambulift RFC
//If the Passenger has PRM Type should be allowed to have Ambulift
$prmType_amb = array();
$prmType_amb[] = "WCHC";
$prmType_amb[] = "WCHR";
$prmType_amb[] = "WCHS";
$prmType_amb[] = "WCBD";
$prmType_amb[] = "WCBW";
$prmType_amb[] = "WCMP";

//If the Passenger is in a flight that is parked to the following stands
$remoteStand_amd = array();
$remoteStand_amd[] = "A40";
$remoteStand_amd[] = "A41";
$remoteStand_amd[] = "A42";
$remoteStand_amd[] = "A43";
$remoteStand_amd[] = "A44";
$remoteStand_amd[] = "A45";
$remoteStand_amd[] = "A46";
$remoteStand_amd[] = "A47";


$remoteStand_amd[] = "A49";
$remoteStand_amd[] = "A50";
$remoteStand_amd[] = "A51";
$remoteStand_amd[] = "A52";
$remoteStand_amd[] = "A53";
$remoteStand_amd[] = "A54";
$remoteStand_amd[] = "A55";
$remoteStand_amd[] = "A56";
$remoteStand_amd[] = "A57";
$remoteStand_amd[] = "A58";

$remoteStand_amd[] = "B17";
$remoteStand_amd[] = "B30";
$remoteStand_amd[] = "B31";
$remoteStand_amd[] = "B32";
$remoteStand_amd[] = "B33";
$remoteStand_amd[] = "B34";
$remoteStand_amd[] = "B35";
$remoteStand_amd[] = "B36";
$remoteStand_amd[] = "B37";
$remoteStand_amd[] = "B38";
$remoteStand_amd[] = "B39";
$remoteStand_amd[] = "B40";
$remoteStand_amd[] = "B41";
$remoteStand_amd[] = "B42";
$remoteStand_amd[] = "B43";
$remoteStand_amd[] = "B44";
$remoteStand_amd[] = "B45";

$remoteStand_amd[] = "B50";
$remoteStand_amd[] = "B51";
$remoteStand_amd[] = "B52";
$remoteStand_amd[] = "B53";
$remoteStand_amd[] = "B54";
$remoteStand_amd[] = "B55";
$remoteStand_amd[] = "B56";
$remoteStand_amd[] = "B57";
$remoteStand_amd[] = "B58";
$remoteStand_amd[] = "B59";
$remoteStand_amd[] = "B60";
$remoteStand_amd[] = "B61";
$remoteStand_amd[] = "B62";
$remoteStand_amd[] = "B63";
$remoteStand_amd[] = "B64";
$remoteStand_amd[] = "B65";
$remoteStand_amd[] = "B66";
$remoteStand_amd[] = "B67";

$remoteStand_amd[] = "C01";
$remoteStand_amd[] = "C02";
$remoteStand_amd[] = "C03";
$remoteStand_amd[] = "C04";
$remoteStand_amd[] = "C05";
$remoteStand_amd[] = "C06";
$remoteStand_amd[] = "C07";
$remoteStand_amd[] = "C08";
$remoteStand_amd[] = "C09";
$remoteStand_amd[] = "C10";
$remoteStand_amd[] = "C11";
$remoteStand_amd[] = "C12";

$remoteStand_amd[] = "F02D";
$remoteStand_amd[] = "F02E";
$remoteStand_amd[] = "F02F";
$remoteStand_amd[] = "F02G";
$remoteStand_amd[] = "F02H";

$remoteStand_amd[] = "F08A";
$remoteStand_amd[] = "F08B";
$remoteStand_amd[] = "F08G";
$remoteStand_amd[] = "F08H";
$remoteStand_amd[] = "F08E";
$remoteStand_amd[] = "F08D";
$remoteStand_amd[] = "F08F";
$remoteStand_amd[] = "F08C";
$remoteStand_amd[] = "F08I";


$remoteStand_amd[] = "G01";
$remoteStand_amd[] = "G02";
$remoteStand_amd[] = "G03";
$remoteStand_amd[] = "G04";
$remoteStand_amd[] = "G05";
$remoteStand_amd[] = "G06";
$remoteStand_amd[] = "G07";
$remoteStand_amd[] = "G08";
$remoteStand_amd[] = "G09";
$remoteStand_amd[] = "G10";
$remoteStand_amd[] = "G11";
$remoteStand_amd[] = "G12";




$mailfrom="webmaster";
$APPName="Web PRM";

$APPVersion="4.5.0.2";
$APPReleaseDate="29/04/2010";




?>
