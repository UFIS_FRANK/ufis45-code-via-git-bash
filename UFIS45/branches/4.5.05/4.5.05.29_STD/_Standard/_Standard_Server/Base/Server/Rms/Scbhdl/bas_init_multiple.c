/*********************************/
/* start initializing accounts   */
/* we get severeal init-accounts */
/*********************************/
static void bas_init_multiple()
{
	char *pclEmployees;
	char *cp;
	char clCommands[64];
	char clFrom[16];
	char clTo[16];
	char clYearFrom[6];
	char clYearTo[6];
	char clCurrentCmd[12];
	char clScriptName[MAX_SCRIPTNAME];
	int	ilAccHandle;
	int	ilIndex;
	int	ilCountEmpl;
	int	ilCountCmds;
	int	ilCmdCounter;

	dbg(DEBUG,"============================= INIT ACCOUNT INIT_MULT START =============================");

	/* check the data */
	if (strlen(pcgData) == 0)
	{
		dbg(TRACE,"bas_init_multiple(): Stopped, because <pcgData> = undef!");
		return;
	}
	dbg(DEBUG,"bas_init_multiple(): <pcgData> = <%s>",pcgData);

	/* check the fields */
	if (strlen(pcgFields) == 0)
	{
		dbg(TRACE,"bas_init_multiple(): Stopped, because <pcgFields> = undef!");
		return;
	}
	dbg(DEBUG, "bas_init_multiple(): <pcgFields> = <%s>",pcgFields);

	/* get the employee URNOs of the selection */
	if (strlen(pcgSelKey) == 0)
	{
		dbg(TRACE,"bas_init_multiple(): Stopped, because <pcgSelKey> = undef!");
		return;
	}
	dbg(DEBUG, "pcgSelKey = <%s>",pcgSelKey);
	pclEmployees = (char*)calloc(1,strlen(pcgSelKey)+2);
	strcpy(pclEmployees,pcgSelKey);
	dbg(DEBUG,"bas_init_multiple(): Employee URNOs <pclEmployees> = <%s>",pclEmployees);
	ilCountEmpl = CountElements(pclEmployees);
	if (ilCountEmpl < 0)
	{
		dbg(TRACE,"bas_init_multiple(): Stopped, because no employees to be initilized!");
		return;
	}

	/* get start and end date/time */
	GetFieldValueFromDataString("FROM",pcgFields,pcgData,clFrom);
	strncpy(clYearFrom,&clFrom[0],4);
	clYearFrom[4] = '\0';

	GetFieldValueFromDataString("TO",pcgFields,pcgData,clTo);
	sprintf(clYearTo,"%-4d",atoi(clYearFrom) + 1);

	/* get the accounts to initialize */
	GetFieldValueFromDataString("INIT",pcgFields,pcgData,clCommands);

	/* replace '|' with ',' and count commands */
	ilCountCmds = 0;
	cp = &clCommands[0];
	while (*cp != '\0')
	{
		if (*cp == '|')
		{
			*cp = ',';
			++ilCountCmds;
		}
		++cp;
	}

	/* add staff to prefetch array */
	HandlePrefetchArray(pclEmployees, clYearFrom, clYearTo, 1);

	/* all employee data are prefetched */
	/* loop per every INIT_XXX command */
	for (ilCmdCounter = 0; ilCmdCounter < ilCountCmds; ilCmdCounter++)
	{
		GetItem(clCommands,ilCmdCounter,",",clCurrentCmd);

		ilIndex = CheckCommandAndGetXBSCommandIndex(clCurrentCmd);

		if (ilIndex != -1)
		{
			/* execute now */ 
			dbg(DEBUG, "bas_init_multiple(): XBSCommand is:  <%s>",clCurrentCmd);
			dbg(DEBUG, "bas_init_multiple(): script list is: <%s>",sgXBSCommands[ilIndex].cScriptList);
			dbg(DEBUG, "bas_init_multiple(): path is:        <%s>",sgXBSCommands[ilIndex].cPath);

			/* execute next script in script list of action section */
			while (GetNextXBSScript(ilIndex,clScriptName))
			{
				dbg(DEBUG, "bas_init_multiple(): executing script '%s'",clScriptName);
				ExecuteScript(clScriptName,NULL);
			}

			/* reset next-script-to-execute-pointer */
			sgXBSCommands[ilIndex].pNextScript = sgXBSCommands[ilIndex].cScriptList;
		}
	}

	/* closing recordset, so all records will be written to DB */
	ilAccHandle = GetArrayIndex("ACCHDL");
	if (ilAccHandle >= 0)
	{
		/* disable send Account update to action */
		SetAccAction(ilAccHandle,FALSE);

		CloseRecordset(ilAccHandle,1);
		/* enable send Account update to action	*/
		SetAccAction(ilAccHandle,TRUE);

		/* send RELACC BC to action and bchdl	*/
		SendRelaccBc(pcgSelKey,pcgFields,pcgData);
	}

	/* send RECALC BC to InitAccount Client that initialization is finished */
	SendBc("XBS","SBCTAB",pcgSelKey,pcgFields,pcgData,pcgRecvName);

	/* remove staff from prefetch array */
	HandlePrefetchArray(pclEmployees, clYearFrom, clYearTo, 0);

	/* tidy up */
	if (pclEmployees)
		free (pclEmployees);

	/* stop the initialization - it is finished, all employees for all accounts are initialized  */
	BreakScriptListExecution();
}
