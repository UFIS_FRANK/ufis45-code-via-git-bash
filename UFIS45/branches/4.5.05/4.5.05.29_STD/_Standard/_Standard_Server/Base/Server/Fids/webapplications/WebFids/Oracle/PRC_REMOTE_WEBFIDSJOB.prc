CREATE OR REPLACE PROCEDURE PRC_REMOTE_WEBFIDSJOB (p_Table IN VARCHAR2)
	IS

	---------------------------------------------------------------------------------------
	---------------------------------------------------------------------------------------
	--	Procedure	        -	PRC_REMOTE_WEBFIDS_JOB
	--
	--	Version			    -	1.0.0
	--
	--	Written By	      	-	Jurgen Hammerschmid
	--
	--	Procedure Desc.		-	NULL
	--
	--  Input-Parameters
	--
	--  Errors              Errors will be written into the table CEDA_ERRORS
	--
	---------------------------------------------------------------------------------------
	---------------------------------------------------------------------------------------
	--	Change Log
	--
	--	Name           Date               Version    Change
	--	----           ----------         -------    ------
	--  Hammerschmid   14.05.2008         1.0        new
	---------------------------------------------------------------------------------------
	---------------------------------------------------------------------------------------

		jobno			NUMBER;
    	v_Job_check		NUMBER := 0;
	BEGIN
		BEGIN
			-- check if job is already running
			SELECT job
    		INTO v_Job_check
			FROM SYS.user_Jobs
			WHERE what LIKE '%PRC_REMOTE_WEBFIDSJOB%';
    	EXCEPTION
    		WHEN NO_DATA_FOUND THEN
        		v_Job_check := 0;
	    	WHEN OTHERS THEN
				PCK_CEDA_ERRORS.PRC_WRITE_ERROR('PRC_REMOTE_WEBFIDSJOB ', 'FIDS-80000',
                           'Error-Code: ' || TO_CHAR(SQLCODE) || ' Error-Message: ' ||
						   SUBSTR(SQLERRM, 1, 150), 9);
		END;
		IF v_Job_check <> 0 THEN
			--remove old job and create new
			dbms_job.remove(v_Job_check);
	    END IF;
		-- create job
	    DBMS_JOB.SUBMIT(jobno,
		'DECLARE
        v_SEQ_JOB   NUMBER(9);
        v_Count		NUMBER;
        v_TimeStart		DATE;
        v_TimeEnd		DATE;

		BEGIN
        v_TimeStart := SYSDATE;
        SELECT WEBFIDS_SEQ.Nextval
        INTO v_SEQ_JOB
        FROM dual;
        v_TimeEnd := SYSDATE;
        IF v_TimeEnd - v_TimeStart > 25/86400 THEN
			PCK_CEDA_ERRORS.PRC_WRITE_ERROR(''PRC_REMOTE_WEBFIDSJOB '', ''FIDS-80000'',
											''WEBFIDS JOB RUNS TO LONG'',1);
        END IF;
        END;',
		SYSDATE,
		'SYSDATE + 31/86400',
		FALSE);
		COMMIT;

	EXCEPTION
		WHEN OTHERS THEN
    		ROLLBACK;
			PCK_CEDA_ERRORS.PRC_WRITE_ERROR('PRC_REMOTE_WEBFIDSJOB ', 'FIDS-80000',
                  'Error-Code: ' || TO_CHAR(SQLCODE) || ' Error-Message: ' ||
				   SUBSTR(SQLERRM, 1, 150), 2);

	END PRC_REMOTE_WEBFIDSJOB;
/
