DATE=`date +%Y%m%d`
TIME="`date +%H%M`00"

if [ ! -z "$2" ] ; then
  if [ ${#1} -gt 2 ] ; then
    ALCF=ALC3
  else
    ALCF=ALC2
  fi
  ALCV=$1
  FLTN=$2
  if [ ! -z "$3" ] ; then
    FDAT=$3
  else
    FDAT=`date +%Y%m%d`
  fi
fi

if [ ! "$1" = "URNO=" ] ; then
  URNOS=`kshsql "select urno from afttab
            where ${ALCF}='${ALCV}' and fltn='${FLTN}'
            and (stoa like '${FDAT}%' or stod like '${FDAT}%');"`
  echo "Flug ${ALCV} ${FLTN} am ${FDAT} hat Urno(s): "${URNOS}
else
  URNOS=$2
fi

GetFlightUrno()
{
  kshsql "set lines 2200
          set head off
          select time||' '||substr(USEC,1,18)||chr(10)||flst||chr(10)||fval from l01tab where ORNO in 
           (${URNOS}) order by orno,time;"
}

MY_AWK()
{
 awk 'BEGIN {
  FS=sprintf("%c",30) 
  } 
  {
    print "====== "$0 " ======"
    getline
    for (ii=1; ii <= NF ; ii++ ) 
    {
      FF[ii]=$ii
    }
    getline
    for (ii=1; ii <= NF ; ii++ ) 
    {
      found=0
      for (jj=1; jj<= numvals ; jj++)
      {
        if (VV[jj] == FF[ii]"="$ii)
        {
           found=jj
           break
        }
      }
      if (found==0)
      {
        numvals++
        VV[numvals]=FF[ii]"="$ii
        print FF[ii]"="$ii 
      }
    }
  }'
}

if [ "$4" = "raw" ] ; then
   GetFlightUrno
else
   GetFlightUrno | MY_AWK
fi
# awk '{ for (ii = 1 ; 11 < 255 ; ii++ ) { cc=sprintf("%c",ii) ; if (substr($0,5,1)==cc) { print ii,xx ; break } } }'