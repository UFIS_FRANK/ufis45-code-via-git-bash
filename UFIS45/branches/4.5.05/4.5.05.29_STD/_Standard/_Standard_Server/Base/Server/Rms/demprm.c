#ifndef _DEF_mks_version
  #define _DEF_mks_version
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version[] = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Rms/demprm.c 1.5 2009/07/22 18:02:07SGT lli Exp  $";
#endif /* _DEF_mks_version */
/******************************************************************************/
/*                                                                            */
/* ABB ACE/FC Program Skeleton                                                */
/*                                                                            */
/* Author         :                                                           */
/* Date           :                                                           */
/* Description    :                                                           */
/*                                                                            */
/* Update history :                                                           */
/*                                                                            */
/******************************************************************************/
/*                                                                            */
/* source-code-control-system version string                                  */

/*static char sccs_version[] ="@(#) UFIS4.5 (c) ABB AAT/I demhdl.c 4.5.1.10 / 03/09/25 HAG";*/

/* be carefule with strftime or similar functions !!!                         */
/*                                                                            */
/******************************************************************************/
/* This program is a MIKE main program */


#define U_MAIN
#define UGCCS_PRG
#define STH_USE
#include <stdio.h>
#include <malloc.h>
#include <string.h>
#include <errno.h>
#include <signal.h>
#include "ugccsma.h"
#include "msgno.h"
#include "glbdef.h"
#include "quedef.h"
#include "uevent.h"
#include "sthdef.h"
#include "debugrec.h"
#include "hsbsub.h"
#include "fditools.h"
#include "libccstr.h"
#include "l_ccstime.h"
#include "rmschkrul.h"
#include "syslib.h"
#include "AATArray.h"
#include "db_if.h"
#include <time.h>

#ifndef min
#define min(a,b) (a < b ? a : b)
#endif
#ifndef max
#define max(a,b) (a > b ? a : b)
#endif

#define RUDFIELD(pcpBuf,ipFieldNo) (&pcpBuf[rgRudArray.plrArrayFieldOfs[ipFieldNo-1]])
#define NEWDEMFIELD(pcpBuf,ipFieldNo) (&pcpBuf[rgNewDemArray.plrArrayFieldOfs[ipFieldNo-1]])
#define OLDDEMFIELD(pcpBuf,ipFieldNo) (&pcpBuf[rgOldDemArray.plrArrayFieldOfs[ipFieldNo-1]])
/*#define AFTFIELD(pcpBuf,ipFieldNo) (&pcpBuf[rgAftArray.plrArrayFieldOfs[ipFieldNo-1]])*/

#define CCIDEMFIELD(pcpBuf,ipFieldNo) (&pcpBuf[rgCCIDemArray.plrArrayFieldOfs[ipFieldNo-1]])
#define CCIRUDFIELD(pcpBuf,ipFieldNo) (&pcpBuf[rgCCIRudArray.plrArrayFieldOfs[ipFieldNo-1]])
#define RUEFIELD(pcpBuf,ipFieldNo) (&pcpBuf[rgRueArray.plrArrayFieldOfs[ipFieldNo-1]])

#define RPQFIELD(pcpBuf,ipFieldNo) (&pcpBuf[rgRpqArray.plrArrayFieldOfs[ipFieldNo-1]])
#define RLOFIELD(pcpBuf,ipFieldNo) (&pcpBuf[rgRloArray.plrArrayFieldOfs[ipFieldNo-1]])
#define SGRFIELD(pcpBuf,ipFieldNo) (&pcpBuf[rgSgrArray.plrArrayFieldOfs[ipFieldNo-1]])
#define ALTFIELD(pcpBuf,ipFieldNo) (&pcpBuf[rgAltArray.plrArrayFieldOfs[ipFieldNo-1]])

BOOL IS_EMPTY (char *pcpBuf) 
{
	return	(!pcpBuf || *pcpBuf==' ' || *pcpBuf=='\0') ? TRUE : FALSE;
}

extern int get_no_of_items(char *s);
extern int get_item_no(char *, char *, short);
extern int	GetServerTimeStamp(char *, int, long, char *);
extern int	AddSecondsToCEDATime(char *, time_t, int);
extern	long	StrDiffTime(char *, char *);

/*default values*/
#define MINDEDUDEFAULT	"1800"

/* fieldlength */
#define DATELEN		14
#define HOPOLEN		3
#define URNOLEN		10
#define DURATIONLEN	10

/*flags for new CCI demands */
#define	CUT			-3
#define	DELETE		-2
#define	OPENDEL		-1
#define	NOOP		0
#define	OPENNEW		1
#define	NEWDEM		2
#define	ADDED		3


#define RUL_FLD_LEN	(4)
#define MAXRUES (2000)
#define MAXALOCREFS (12)
#define MAXALOCREFENTRIES (8)

#define EVT_TURNAROUND	(0)
#define EVT_INBOUND			(1)
#define EVT_OUTBOUND		(2)
#define EVT_UNKNOWN     (3)

#define CMD_IFR     (1)
#define CMD_UFR     (2)
#define CMD_FIR     (3)
#define CMD_CFL     (4)

#define CMD_IAZ     (5)
#define CMD_UAZ     (6)
#define CMD_DAZ     (7)
#define CMD_UDR     (8)

#define CMD_IFI     (9)
#define CMD_UFI     (10)
#define CMD_DFI     (11)
#define CMD_UCD     (12)

#define CMD_DFR     (13)
#define CMD_IDR     (14)
#define CMD_IRT     (15)
#define CMD_URT     (16)

#define CMD_DRT     (17)
#define CMD_ANSWER	(18)
#define CMD_DLF		(19)

#define CMD_PRM     (20)
#define CMD_PRD     (21)
#define IDX_INBOUND (0)
#define IDX_OUTBOUND (1)

static char *prgCmdTxt[] = {    "IFR","UFR", "FIR", "CFL", 
								"IAZ", "UAZ", "DAZ", "UDR", 
								"IFI", "UFI", "DFI", "UCD",
								"DFR", "IPRM", "IRT", "URT",
								"DRT", "ANSWER", "DLF", "PRM", "PRD", NULL };
static int rgCmdDef[] = { CMD_IFR, CMD_UFR, CMD_FIR, CMD_CFL,
							CMD_IAZ, CMD_UAZ, CMD_DAZ, CMD_UDR, 
							CMD_IFI, CMD_UFI, CMD_DFI, CMD_UCD,
							CMD_DFR, CMD_IDR, CMD_IRT, CMD_URT,
							CMD_DRT, CMD_ANSWER, CMD_DLF, CMD_PRM,CMD_PRD, 0 };


#define CDT_SIMPLE  (0)
#define CDT_DYNAMIC (1)
#define CDT_STATIC  (2)

#define CMP_FALSE   (1)
#define CMP_SMALLER (2)
#define CMP_EQUAL   (3)
#define CMP_GREATER (4)

#define TPL_ACTIVE  (1)

/******************************************************************************/
/***   Fields                                                             *****/
/******************************************************************************/

#define DEM_FIELDS "ALID,ALOC,CDAT,DBAR,DEAR,DEBE,DEDU,DEEN,DETY,EADB,HOPO,LADE,UCON,LSTU,OBTY,OURI,OURO,RTDB,RTDE,SDTI,SUTI,TSDB,TSDE,TTGF,TTGT,UDGR,URNO,URUD,USEC,USEU,RETY,UREF,FLGS" 
#define ADD_DEM_FIELDS "JFND,SAVE"
/*#define RUD_FIELDS "ALOC,CDAT,CODU,DBAR,DBFL,DEAR,DEBE,DEDU,DEEN,DEFL,DIDE,DIND,DRTY,EADB,FADD,FCND,FFPD,FOND,FSAD,HOPO,LADE,LODE,LSTU,MAXD,MIND,NRES,REDE,REND,REPD,RETY,RTDB,RTDE,SDTI,SUTI,STDE,TSDB,TSDE,TSND,TSPD,TTGF,TTGT,UCRU,UDEM,UDGR,UEDE,UGHC,UGHS,UNDE,UPDE,UPRO,URNO,URUE,USEC,USEU"*/
#define RUD_FIELDS "ALOC,DBAR,DBFL,DEAR,DEBE,DEDU,DEEN,DEFL,DIDE,DRTY,EADB,FADD,FFPD,HOPO,LADE,LSTU,MAXD,MIND,REPD,RETY,RTDB,RTDE,SDTI,SUTI,TSDB,TSDE,TSPD,TTGF,TTGT,UDGR,UGHS,UPDE,URNO,URUE"
#define ADD_RUD_FIELDS "FLAG"
/*#define AFT_FIELDS "URNO,RKEY,FLNO,TIFA,TIFD,ADID,PSTA,PSTD,REGN"*/
#define RUE_FIELDS "URNO,FISU"
#define RPF_FIELDS "URNO,FCCO,GTAB,UPFC,URUD"
#define RPQ_FIELDS "URNO,QUCO,GTAB,UPER,URUD"
#define REQ_FIELDS "URNO,EQCO,GTAB,UEQU,URUD"
#define RLO_FIELDS "URNO,RLOC,GTAB,REFT,URUD"
#define SGR_FIELDS "URNO,GRPN"
#define ALT_FIELDS "URNO,ALC3"

#define URNOS_TO_FETCH 500

#define NO_BC	0
#define TCP_SBC	1
#define UDP_SBC	2
#define SINGLE_TO_BCHDL		4
#define SINGLE_TO_ACTION	8

#define IDX_IRTD 4	/* Index of List "INSERT" in prgDemInf->DataList */
#define IDX_URTD 5  /* Index of List "UPDATE" in prgDemInf->DataList */
#define IDX_DRTU 6  /* Index of List "DELETE" in prgDemInf->DataList */
#define IDX_IRTU 7	/* Index of List "IRT_URNO" in prgDemInf->DataList */
#define IDX_URTU 8	/* Index of List "URT_URNO" in prgDemInf->DataList */
#define IDX_DRTD 9	/* Index of List "DRT_DATA" in prgDemInf->DataList */

static int rgUIdx[3] = { IDX_IRTU, IDX_URTU, IDX_DRTU };

/******************************************************************************/
/* External variables                                                         */
/******************************************************************************/
/* outp is defined in ugccsma.h! double definitions kill */
/* your process under HP-UX immediatly !!!!              */
/*FILE *outp       = NULL;*/
int  debug_level = 0;

/******************************************************************************/
/* Global variables                                                           */
/******************************************************************************/
static ITEM  *prgItem      = NULL;                /* The queue item pointer  */
static EVENT *prgEvent     = NULL;                /* The event pointer       */
static	EVENT	*prgOutEvent      = NULL ;
static	long    lgOutEventSize  = 0 ;
static int   igItemLen     = 0;                   /* length of incoming item */
static int igAftuItemNo = 0;
static int igAlc3ItemNo = 0;
static int igUprmItemNo = 0;
static int igRegnItemNo = 0;
static int   igInitOK      = FALSE;
static long  lgEvtCnt      = 0;
static char  cgConfigFile[512];
static char  cgDataArea[8192];
static char  cgHopo[8];                         /* default home airport    */
static char  cgTabEnd[8];                       /* default table extension */
static char  cgProcessName[80];                  /* name of executable */
static int   igRouter = 1200;
static char  cgRegn[26];
static char  cgMinDedu[DURATIONLEN+1];
static int   lgEventType = 0;
static char  *pcgFieldList = NULL;
static char  *pcgInbound = NULL;
static char  *pcgOutbound = NULL;
static char  *pcgTemplates = 0;
static int	 igLenofTemplates = 0;

static time_t tgBeginStamp = 0;
static time_t tgEndStamp = 0;
static time_t tgQueDiff = 0;
static time_t tgMaxStampDiff = 0;
static BOOL bgShowEvent = FALSE;
static BOOL bgUseFilt = FALSE;

/*static char *pcgUsedRudUrnos = NULL;
static long lgUsedRudUrnosLength = 0L;*/
static char pcgTmpBuf[8192];
static char pcgNewDemandBuf[8192];

static char cgCfgBuffer[1024];
static char cgRegnAloc[1024];
static	char	pcgCCIDemBuf[8192];
static	BOOL	bgOldDemandsEx = FALSE;
static	int		igNwD = 0;
static	int		igMinDemand = 1;
static	int		igMaxDemand = 0;
static	time_t	tgIvStart,tgIvEnd;
static	char	*pcgIvStart = NULL;
static	char	*pcgIvEnd = NULL;
static	char	*pcgRudUrno = NULL;
static	char	*pcgStep = NULL;
static	char  	cgTdi1[24];
static	char  	cgTdi2[24];
static	char  	cgTich[24];
static	int		igStartUpMode = TRACE;
static	int		igRuntimeMode = 0;
static	int		igTOIgnore = 0;	/* don't leave gaps smaller than igToIgnore, if possible */
static	BOOL	bgSendSBC = FALSE;
static	BOOL	bgSendUpdDem = FALSE;
static	BOOL	bgUseFIDSTimes = FALSE;
static char		cgMinDebe[15];
static char		cgMaxDeen[15];

static char pcgRudFields[401];
static char pcgRudAddFields[41];
static char pcgDemFields[401];
static long   lgActUrno =0 ;
static int   igReservedUrnoCnt = 0;
static int	 igActionId = 0;	
static char  cgIgnoreAloc [124]="";
static BOOL  bgDelInvalidDemands = FALSE;
static unsigned int  igLoadSel = 0;
static long lgBCDays = -1;
static char  cgPrmTemplates[512]  = "PRM";
static char  pcgHandlingTypeForPRM[44]  = "";
static char  cgLineBuf[2048] = "";
static char  cgFoundMatchingDemands[5000] = "";
static char  cgErrMsg[1024] = "";


struct _rgExDArray
{
	long	lrStartInSec;
	float	frSumOfDem;
	long	lrDiffOfDem;
};
typedef struct	_rgExDArray	EXDARRAY;

EXDARRAY	*prgExD = 0;

struct _rgNwDArray
{
	char	crStartDemand[DATELEN+1];
	char	crEndDemand[DATELEN+1];
	long	lrStartInSec;
	long	lrEndInSec;
	short	srState;
};
typedef	struct	_rgNwDArray	NWDARRAY;

NWDARRAY	*prgNwD = 0;

static struct RueUrnoStruct
{
	char Urno[11];
	char Fisu[2];
	char  IsUsed;
} prgRueUrnos[MAXRUES];

static struct AlocRefStruct
{
	char Name[24];
	char Table[MAXALOCREFENTRIES+1][14];
	char Field[MAXALOCREFENTRIES+1][12];
	int  IsUsed;
} prgAlocRefInbound[MAXALOCREFS+1],
		 prgAlocRefOutbound[MAXALOCREFS+1];

#define INBOUNDTIMES "TIFA,STOA,ETAI,ETOA,TMOA,LAND,ONBE,ONBL"
#define OUTBOUNDTIMES "AATI,SATI,TIFD,STOD,ETDI,ETOD,OFBL,AIRB"
#define PRMTIMES "SATI,AATI"
/*
#define INBOUNDTIMES_NOFIDS "TIFA,STOA,ETAI,TMOA,LAND,ONBE,ONBL"
#define OUTBOUNDTIMES_NOFIDS "TIFD,STOD,ETDI,OFBL,AIRB"

static char *pcgTimeFieldsIn = INBOUNDTIMES_NOFIDS;
static char *pcgTimeFieldsOut = OUTBOUNDTIMES_NOFIDS;
*/
static char   InboundTimes[12][16] = { 0 };
static char   OutboundTimes[12][16] = { 0 };
static char   cgSoh[2];
static char   cgStx[2];
static char   cgDc1[2];
static char   cgKomma[2];
static char   cgEtx[2];
static char   cgTplLoadExt[3][101] =  { " AND URUD IN (SELECT URNO FROM RUDTAB WHERE URUE IN (SELECT URNO FROM RUETAB WHERE UTPL IN (%s)))",	
									" AND URUD IN (SELECT URNO FROM RUDTAB WHERE UTPL IN (%s))", 
									" AND UTPL IN (%s)" };


/* static array informations */

#define ARR_NAME_LEN   (18)
#define ARR_FLDLST_LEN (512)

struct _arrayinfo {
	HANDLE   rrArrayHandle;
	char     crArrayName[ARR_NAME_LEN+1];
	char     crTableName[ARR_NAME_LEN+1];
	char     crArrayFieldList[ARR_FLDLST_LEN+1];
	long     lrArrayFieldCnt;
	char   *pcrArrayRowBuf;			/* no Buffer will be allocated, only used as row pointer */
	long     lrArrayRowLen;
	long   *plrArrayFieldOfs;
	long   *plrArrayFieldLen;
	HANDLE   rrIdx01Handle;
	char     crIdx01Name[ARR_NAME_LEN+1];
	char     crIdx01FieldList[ARR_FLDLST_LEN+1];
	long     lrIdx01FieldCnt;
	char   *pcrIdx01RowBuf;
	long     lrIdx01RowLen;
	long   *plrIdx01FieldPos;
	long   *plrIdx01FieldOrd;
	HANDLE	rrIdx02Handle;
	char	crIdx02Name[ARR_NAME_LEN+1];
	char	crIdx02FieldList[ARR_FLDLST_LEN+1];
	long	lrIdx02FieldCnt;
	char	*pcrIdx02RowBuf;
	long	lrIdx02RowLen;
	long	*plrIdx02FieldPos;
	long	*plrIdx02FieldOrd;
};
typedef struct _arrayinfo ARRAYINFO;

static ARRAYINFO rgRudArray;
static ARRAYINFO rgNewDemArray;
static ARRAYINFO rgOldDemArray;
/*static ARRAYINFO rgAftArray;*/
static ARRAYINFO	rgCCIDemArray;
static ARRAYINFO	rgCCIRudArray;
static ARRAYINFO rgRueArray;
static ARRAYINFO rgJodArray;
static ARRAYINFO rgJobArray;
static ARRAYINFO rgRpfArray ;
static ARRAYINFO rgRpqArray ;
static ARRAYINFO rgReqArray ;
static ARRAYINFO rgRloArray ;
static ARRAYINFO rgSgrArray ;
static ARRAYINFO rgAltArray ;


static int igDemALID;
static int igDemALOC;
static int igDemCDAT;
static int igDemDBAR;
static int igDemDEAR;
static int igDemDEBE;
static int igDemDEDU;
static int igDemDEEN;
static int igDemDETY;
static int igDemEADB;
static int igDemHOPO;
static int igDemLADE;
static int igDemUCON;
static int igDemLSTU;
static int igDemOBTY;
static int igDemOURI;
static int igDemOURO;
static int igDemRTDB;
static int igDemRTDE;
static int igDemSDTI;
static int igDemSUTI;
static int igDemTSDB;
static int igDemTSDE;
static int igDemTTGF;
static int igDemTTGT;
static int igDemUDGR;
static int igDemURNO;
static int igDemURUD;
static int igDemUSEC;
static int igDemUSEU;
static int igDemRETY;
static int igDemUREF;
static int igDemFLGS;
static int igDemUSES;
static int igDemUTPL;
static int igDemUPRM;
static int igDemFILT;

static int igDemJFND;
static int igDemSAVE;
static int igDemUGHS=-1;
static int igDemQUCO=-1;
static int igDemRECO=-1;

static int igRudALOC;
static int igRudDBAR;
static int igRudDBFL;
static int igRudDEFL;
static int igRudDEAR;
static int igRudDEBE;
static int igRudDEDU;
static int igRudDEEN;
static int igRudDIDE;
static int igRudDRTY;
static int igRudEADB;
static int igRudFADD;
static int igRudFFPD;
static int igRudHOPO;
static int igRudLADE;
static int igRudLSTU;
static int igRudMAXD;
static int igRudMIND;
static int igRudREPD;
static int igRudRTDB;
static int igRudRTDE;
static int igRudSDTI;
static int igRudSUTI;
static int igRudTSDB;
static int igRudTSDE;
static int igRudTSPD;
static int igRudTTGF;
static int igRudTTGT;
static int igRudUDGR;
static int igRudUGHS;
static int igRudUPDE;
static int igRudURNO;
static int igRudURUE;
static int igRudRETY;
static int igRudFLAG;
static int igRudUSES;
static int igRudUTPL=-1;
static int igRudQUCO=-1;
static int igRudRECO=-1;

static int igAftURNO;
static int igAftADID;
static int igAftFLNO;
static int igAftTIFA;
static int igAftTIFD;
static int igAftPSTA;
static int igAftPSTD;
static int igAftRKEY;
static int igAftREGN;

static int igRpfFCCO;
static int igRpfGTAB;
static int igRpfUPFC;

static int igRpqQUCO;
static int igRpqGTAB;
static int igRpqUPER;

static int igReqEQCO;
static int igReqGTAB;
static int igReqUEQU;

static int igRloRLOC;
static int igRloGTAB;
static int igRloREFT;
	
static int igSgrGRPN;


static AAT_PARAM *prgDemInf=0;
static BOOL bgUseJODTAB=TRUE;


/******************************************************************************/
/* Function prototypes	                                                      */
/******************************************************************************/
int check_ret1(int ipRc,int ipLine);
static int  InitDemhdl();
static int SetArrayInfo(char *pcpArrayName, char *pcpTableName, char *pcpArrayFieldList,char *pcpAddFields,long *pcpAddFieldLens,ARRAYINFO *prpArrayInfo);



static int	Reset(void);                       /* Reset program          */
static void	Terminate(int ipSleep);            /* Terminate program      */
/*static void	HandleSignal(int);*/                /* Handles signals        */
static void	HandleErr(int);                    /* Handles general errors */
static void	HandleQueErr(int);                 /* Handles queuing errors */
static void HandleQueues(void);                /* Waiting for Sts.-switch*/

static char *GetRef(char *pcpCfgBuffer,struct AlocRefStruct *pcpAlocBuffer);
static int  GetFieldLength(char *pcpHopo, char *pcpTana, char *pcpFina, long *pipLen);
static int  GetRowLength(char *pcpHopo, char *pcpTana, char *pcpFieldList, long *pipLen);

static int  GetEventType(long *plpEventType,char **pcpInbound, char **pcpOutbound,char *pcpData);
static int  GetPrmEventType(long *plpEventType,char **pcpInbound, char **pcpOutbound,char *pcpData);
static int SaveEventTimes(char *pcpInbound,char *pcpOutbound);
static int  GetCommand(char *pcpCommand, int *pipCmd);

static int GetRueUrnos(char *pcpRueUrnos);
static int ReadOldDemands(char *pcpOURI,char *pcpOURO, char *pcpTemplates);
static int ReadOldPrmDemands2(char *pcpOURI,char *pcpOURO, char *pcpTemplates);
static int ReadOtherOldPrmDemands(char *pcpAFTU, char *pcpUPRM, char *pcpTemplates,BOOL bpFirstRead);
static int ReadOldFid(char *pcpDate, char *pcpTemplates);
static int ReadRudTab();
static int CreateDemands();
static int CreatePrmDemands();
static int CreateFid(char *pcpDate);
static int CompareDemands();
static int UpdateDemand(ARRAYINFO *prpDest, long lpRowNum, ARRAYINFO *prpSource);
static int DeleteOldDemands();
static int SaveDemands( int ipBcMode );
static int SaveFisu();
static int	HandleData(EVENT *prpEvent);       /* Handles event data     */
static int TimeToStr(char *pcpTime,time_t lpTime); 

static int StrToTime(char *pcpTime,time_t *pclTime);
static void  SunCompStrAddTime(char *,char*,char *);
static int SendToSqlHdl(char *pcpObject,char *pcpSelection,
			char *pcpFields,char *pcpData);
static int SendToQue(int ipModID, int ipPrio, BC_HEAD *prpBCHead, 
							CMDBLK *prpCmdblk, char *pcpSelection, 
							char *pcpFields, char *pcpData);
static int SendAnswer(EVENT *prpEvent,int ipRc);
static int UpdateDemandRecord(char *pclFields,char *pclData);
static void TrimRight(char *pcpBuffer);
static int GetAlidFromAloc(char *pcpAlid,char *pcpAloc,char *pcpIOFlag);

static int	CCIDemands(char *);
static int	ReadCCIDemands(char *, char *);
static int	CompareCCIDemands(short,long,char *,long *);
static int	WritingNewCCIDemands(char *);
static int	BuildingNewCCIDemands(long,long);
static int	ExtendingOldCCIDemands(void);
static int	DeletingOldCCIDemands(void);
static int	CuttingOldCCIDemands(void);
static int	SplittingOldCCIDemands(void);
static int	InitializeCCIDemand(char *);
static int	JobExists(char *);
static int	IntegrateOthers(time_t, time_t, time_t);
static int	SaveIndexInfo(ARRAYINFO *);
static int	CreateIdx1(ARRAYINFO *,char *,char *,char *);       
static int	CreateIdx2(ARRAYINFO *,char *,char *,char *);       
static int	HandleIDR(char *, char *);
static int	GetDebeFromPrevDem(char *, char *, char *);
static int	LocalToUtc(char *);
static int	UtcToLocal(char *);
static int	SetFisu(char *);
static int	GetDebugLevel(char *, int *);
static int	ToDeleteOrNot(char *, long);
static int	RemoveDemands(char *);
static int JoinOthers(time_t *tpRealEnd, time_t tpMinEnd, time_t tpMaxEnd);
static int UpdateManualDemand ( char *pcpDemRow );
static int SplitManualDemand ( char *pcpDemRow, char *pcpOURI, char *pcpOURO );
static int GetDebeDeen ( char *pcpRudRow, char * pcpDety, char *pcpDebe, 
						 char *pcpDeen, char *pcpDedu );
static void InitDemFieldsFromRud ( char *pcpNewDemandBuf, char *pcpRudRow );
static int AddNewDemandRow ();
static int AddNewCCIDemandRow ();
static int RecalcDedu ( char *pcpDemandBuf );
static int AddRowToRudArray ( long lpRow );
static int GetNextUrno(char *pcpUrno);
static BOOL SetFisuIfNecessary(char *pcpRueUrno);
static int DoSendSBC ( char *pcpStart, char *pcpEnd, char *pcpTpl, 
					   char *pcpDety, char *pcpWks );
static int ReadJobtab ( ARRAYINFO *prpDemArray );
static int SearchJob (char *pcpDemUrno);
static void CheckPerformance (int ipStart, char *pcpRtTime);
static int HandleJobDelete ( char *pcpFields, char *pcpData );
static int GetTotalRowLength( char *pcpFieldList, ARRAYINFO *prpArray, long *plpLen);
static int ReadConfigEntries ();
static BOOL ToBeBroadcasted ( char *pcpDate );
static int CombineNwdArray ( short	spState );
static void DbgNwdArray ( int ipDbg, char *pcpText );
static void DbgCciDemArray ( int ipDbg, char *pcpText, char *pcpKey );
static int SetRudLogicalFields ( ARRAYINFO *prpRudArr, char *pcpRudUrno );
static int GetResourceName ( ARRAYINFO *prpArrayInfo, int ipGtabIdx, int ipUresIdx, 
							 int ipCodeIdx, char *pcpUrud, char *pcpName );
static int GetNameFromBasicData ( char *pcpReft, char *pcpUrno, char *pcpName );
static int GetQualifications ( char *pcpUrud, char *pcpQuco );
static int RebuildArrayIndexes ( ARRAYINFO *prpArr, BOOL bpIdx1, BOOL bpIdx2 );
static int AlignCCIToStep ( int ipStepSec );
static int GetRueUrnos(char *pcpRueUrnos);
static int GetUrnoFromSelection(char *pcpSelection,char *pcpUrno);
static int GetPrmRueUrnos(char *pcpRueUrnos);
static int FindHsna(char *pcpAlc3,char *pcpHsna);

/******************************************************************************/
/*                                                                            */
/* The MAIN program                                                           */
/*                                                                            */
/******************************************************************************/
MAIN
{
	int	ilRc = RC_SUCCESS;			/* Return code			*/
	int	ilCnt = 0;

	INITIALIZE;			/* General initialization	*/

	if (debug_level < TRACE)
		debug_level = TRACE;

	strcpy(cgProcessName,argv[0]);

	dbg(TRACE,"MAIN: version <%s>",mks_version);

	/* Attach to the MIKE queues */
	do
	{
		ilRc = init_que();
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"MAIN: init_que() failed! waiting 6 sec ...");
			sleep(6);
			ilCnt++;
		}/* end of if */
	}while((ilCnt < 10) && (ilRc != RC_SUCCESS));

	if(ilRc != RC_SUCCESS)
	{
		dbg(TRACE,"MAIN: init_que() failed! waiting 60 sec ...");
		sleep(60);
		exit(1);
	}
	else{
		dbg(TRACE,"MAIN: init_que() OK! mod_id <%d>",mod_id);
	}/* end of if */

	do
	{
		ilRc = init_db();
		if (ilRc != RC_SUCCESS)
		{
			check_ret(ilRc);
			dbg(TRACE,"MAIN: init_db() failed! waiting 6 sec ...");
			sleep(6);
			ilCnt++;
		} /* end of if */
	} while((ilCnt < 10) && (ilRc != RC_SUCCESS));

	if(ilRc != RC_SUCCESS)
	{
		dbg(TRACE,"MAIN: init_db() failed! waiting 60 sec ...");
		sleep(60);
		exit(2);
	}else{
		dbg(TRACE,"MAIN: init_db() OK!");
	} /* end of if */

	/*logon to DB is ok, but do NOT use DB while ctrl_sta == HSB_COMING_UP !!!*/

	sprintf(cgConfigFile,"%s/%s",getenv("BIN_PATH"),mod_name);
	dbg(TRACE,"ConfigFile <%s>",cgConfigFile);
	ilRc = TransferFile(cgConfigFile);
	if(ilRc != RC_SUCCESS)
	{
		dbg(TRACE,"MAIN: TransferFile(%s) failed!",cgConfigFile);
	} /* end of if */

	sprintf(cgConfigFile,"%s/%s.cfg",getenv("CFG_PATH"),mod_name);
	dbg(TRACE,"ConfigFile <%s>",cgConfigFile);
	ilRc = TransferFile(cgConfigFile);
	if(ilRc != RC_SUCCESS)
	{ 
		dbg(TRACE,"MAIN: TransferFile(%s) failed!",cgConfigFile);
	} /* end of if */

	ilRc = SendRemoteShutdown(mod_id);
	if(ilRc != RC_SUCCESS)
	{
		dbg(TRACE,"MAIN: SendRemoteShutdown(%d) failed!",mod_id);
	} /* end of if */

	if((ctrl_sta != HSB_STANDALONE) && (ctrl_sta != HSB_ACTIVE) && (ctrl_sta != HSB_ACT_TO_SBY))
	{
		dbg(DEBUG,"MAIN: waiting for status switch ...");
		HandleQueues();
	}/* end of if */

	if((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE) || (ctrl_sta == HSB_ACT_TO_SBY))
	{
		dbg(TRACE,"MAIN: initializing ...");
		if(igInitOK == FALSE)
		{
			ilRc = InitDemhdl();
			if(ilRc != RC_SUCCESS)
			{
				dbg(TRACE,"InitDemhdl: init failed!");
			} /* end of if */
		}/* end of if */
	} else {
		Terminate(30);
	}/* end of if */

	dbg(TRACE,"=====================");
	dbg(TRACE,"MAIN: initializing OK");
	dbg(TRACE,"=====================");
	
	debug_level = igRuntimeMode;

	for(;;)
	{
		ilRc = que(QUE_GETBIG,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);
		/* depending on the size of the received item  */
		/* a realloc could be made by the que function */
		/* so do never forget to set event pointer !!! */
		prgEvent = (EVENT *) prgItem->text;
				
		if( ilRc == RC_SUCCESS )
		{
			/* Acknowledge the item */
			ilRc = que(QUE_ACK,0,mod_id,0,0,NULL);
			if( ilRc != RC_SUCCESS ) 
			{
				/* handle que_ack error */
				HandleQueErr(ilRc);
			} /* fi */
			
			lgEvtCnt++;

			switch( prgEvent->command )
			{
			case	HSB_STANDBY	:
				ctrl_sta = prgEvent->command;
				HandleQueues();
				break;	
			case	HSB_COMING_UP	:
				ctrl_sta = prgEvent->command;
				HandleQueues();
				break;	
			case	HSB_ACTIVE	:
				ctrl_sta = prgEvent->command;
				break;	
			case	HSB_ACT_TO_SBY	:
				ctrl_sta = prgEvent->command;
				/* CloseConnection(); */
				HandleQueues();
				break;	
			case	HSB_DOWN	:
				/* whole system shutdown - do not further use que(), send_message() or timsch() ! */
				ctrl_sta = prgEvent->command;
				Terminate(1);
				break;	
			case	HSB_STANDALONE	:
				ctrl_sta = prgEvent->command;
				ResetDBCounter();
				break;	
			case	REMOTE_DB :
				/* ctrl_sta is checked inside */
				HandleRemoteDB(prgEvent);
				break;
			case	SHUTDOWN	:
				/* process shutdown - maybe from uutil */
				Terminate(1);
				break;
					
			case	RESET		:
				ilRc = Reset();
				break;
					
			case	EVENT_DATA	:
				if((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE) || (ctrl_sta == HSB_ACT_TO_SBY))
				{
					ilRc = HandleData(prgEvent);
					if(ilRc != RC_SUCCESS)
					{
						HandleErr(ilRc);
					}/* end of if */
				}else{
					dbg(TRACE,"MAIN: wrong hsb status <%d>",ctrl_sta);
					DebugPrintItem(TRACE,prgItem);
					DebugPrintEvent(TRACE,prgEvent);
				}/* end of if */
				break;
					
			case	TRACE_ON :
				dbg_handle_debug(prgEvent->command);
				break;
			case	TRACE_OFF :
				dbg_handle_debug(prgEvent->command);
				break;
			case 100:
				SaveIndexInfo( &rgCCIRudArray );
				break;
			case 101:
				SaveIndexInfo( &rgOldDemArray );
				SaveIndexInfo( &rgNewDemArray );
				break;
			case 102:
				SaveIndexInfo( &rgCCIDemArray );
				break;
			case 103:
				SaveIndexInfo( &rgRpfArray );
				SaveIndexInfo( &rgRpqArray );
				SaveIndexInfo( &rgReqArray );
				SaveIndexInfo( &rgRloArray );
				break;
			default			:
				dbg(TRACE,"MAIN: unknown event");
				DebugPrintItem(TRACE,prgItem);
				DebugPrintEvent(TRACE,prgEvent);
				break;
			} /* end switch */
		} else {
			/* Handle queuing errors */
			HandleQueErr(ilRc);
		} /* end else */
		
	} /* end for */
	
} /* end of MAIN */

/******************************************************************************/
/* The initialization routine                                                 */
/******************************************************************************/

static int InitFieldIndex()
{
	int ilRc = RC_SUCCESS;
	int ilCol,ilPos, ilDBFields;

	FindItemInList(pcgDemFields,"ALID",',',&igDemALID,&ilCol,&ilPos);
	FindItemInList(pcgDemFields,"ALOC",',',&igDemALOC,&ilCol,&ilPos);
	FindItemInList(pcgDemFields,"CDAT",',',&igDemCDAT,&ilCol,&ilPos);
	FindItemInList(pcgDemFields,"DBAR",',',&igDemDBAR,&ilCol,&ilPos);
	FindItemInList(pcgDemFields,"DEAR",',',&igDemDEAR,&ilCol,&ilPos);
	FindItemInList(pcgDemFields,"DEBE",',',&igDemDEBE,&ilCol,&ilPos);
	FindItemInList(pcgDemFields,"DEDU",',',&igDemDEDU,&ilCol,&ilPos);
	FindItemInList(pcgDemFields,"DEEN",',',&igDemDEEN,&ilCol,&ilPos);
	FindItemInList(pcgDemFields,"DETY",',',&igDemDETY,&ilCol,&ilPos);
	FindItemInList(pcgDemFields,"EADB",',',&igDemEADB,&ilCol,&ilPos);
	FindItemInList(pcgDemFields,"HOPO",',',&igDemHOPO,&ilCol,&ilPos);
	FindItemInList(pcgDemFields,"LADE",',',&igDemLADE,&ilCol,&ilPos);
	FindItemInList(pcgDemFields,"UCON",',',&igDemUCON,&ilCol,&ilPos);
	FindItemInList(pcgDemFields,"LSTU",',',&igDemLSTU,&ilCol,&ilPos);
	FindItemInList(pcgDemFields,"OBTY",',',&igDemOBTY,&ilCol,&ilPos);
	FindItemInList(pcgDemFields,"OURI",',',&igDemOURI,&ilCol,&ilPos);
	FindItemInList(pcgDemFields,"OURO",',',&igDemOURO,&ilCol,&ilPos);
	FindItemInList(pcgDemFields,"RTDB",',',&igDemRTDB,&ilCol,&ilPos);
	FindItemInList(pcgDemFields,"RTDE",',',&igDemRTDE,&ilCol,&ilPos);
	FindItemInList(pcgDemFields,"SDTI",',',&igDemSDTI,&ilCol,&ilPos);
	FindItemInList(pcgDemFields,"SUTI",',',&igDemSUTI,&ilCol,&ilPos);
	FindItemInList(pcgDemFields,"TSDB",',',&igDemTSDB,&ilCol,&ilPos);
	FindItemInList(pcgDemFields,"TSDE",',',&igDemTSDE,&ilCol,&ilPos);
	FindItemInList(pcgDemFields,"TTGF",',',&igDemTTGF,&ilCol,&ilPos);
	FindItemInList(pcgDemFields,"TTGT",',',&igDemTTGT,&ilCol,&ilPos);
	FindItemInList(pcgDemFields,"UDGR",',',&igDemUDGR,&ilCol,&ilPos);
	FindItemInList(pcgDemFields,"URNO",',',&igDemURNO,&ilCol,&ilPos);
	FindItemInList(pcgDemFields,"URUD",',',&igDemURUD,&ilCol,&ilPos);
	FindItemInList(pcgDemFields,"USEC",',',&igDemUSEC,&ilCol,&ilPos);
	FindItemInList(pcgDemFields,"USEU",',',&igDemUSEU,&ilCol,&ilPos);
	FindItemInList(pcgDemFields,"RETY",',',&igDemRETY,&ilCol,&ilPos);
	FindItemInList(pcgDemFields,"UREF",',',&igDemUREF,&ilCol,&ilPos);
	FindItemInList(pcgDemFields,"FLGS",',',&igDemFLGS,&ilCol,&ilPos);
	FindItemInList(pcgDemFields,"USES",',',&igDemUSES,&ilCol,&ilPos);
	FindItemInList(pcgDemFields,"UTPL",',',&igDemUTPL,&ilCol,&ilPos);
	FindItemInList(pcgDemFields,"UPRM",',',&igDemUPRM,&ilCol,&ilPos);
	if (bgUseFilt)
	{
		FindItemInList(pcgDemFields,"FILT",',',&igDemFILT,&ilCol,&ilPos);
	}

	ilDBFields = get_no_of_items(pcgDemFields);
	FindItemInList(ADD_DEM_FIELDS,"JFND",',',&igDemJFND,&ilCol,&ilPos);
	if ( igDemJFND > 0 )
		igDemJFND += ilDBFields;
	FindItemInList(ADD_DEM_FIELDS,"SAVE",',',&igDemSAVE,&ilCol,&ilPos);
	if ( igDemSAVE > 0 )
		igDemSAVE += ilDBFields;
	dbg ( DEBUG, "Indices of DEMTAB-fields ALID <%d> USES <%d>",
				 igDemALID, igDemUSES );
	dbg ( DEBUG, "Indixes of add. DEMTAB-fields JFND <%d> SAVE <%d>",
				 igDemJFND, igDemSAVE );

	FindItemInList(pcgDemFields,"UGHS",',',&igDemUGHS,&ilCol,&ilPos);
	FindItemInList(pcgDemFields,"QUCO",',',&igDemQUCO,&ilCol,&ilPos);
	FindItemInList(pcgDemFields,"RECO",',',&igDemRECO,&ilCol,&ilPos);

	FindItemInList(pcgRudFields,"ALOC",',',&igRudALOC,&ilCol,&ilPos);
	FindItemInList(pcgRudFields,"DBAR",',',&igRudDBAR,&ilCol,&ilPos);
	FindItemInList(pcgRudFields,"DBFL",',',&igRudDBFL,&ilCol,&ilPos);
	FindItemInList(pcgRudFields,"DEFL",',',&igRudDEFL,&ilCol,&ilPos);
	FindItemInList(pcgRudFields,"DEAR",',',&igRudDEAR,&ilCol,&ilPos);
	FindItemInList(pcgRudFields,"DEBE",',',&igRudDEBE,&ilCol,&ilPos);
	FindItemInList(pcgRudFields,"DEDU",',',&igRudDEDU,&ilCol,&ilPos);
	FindItemInList(pcgRudFields,"DEEN",',',&igRudDEEN,&ilCol,&ilPos);
	FindItemInList(pcgRudFields,"DIDE",',',&igRudDIDE,&ilCol,&ilPos);
	FindItemInList(pcgRudFields,"DRTY",',',&igRudDRTY,&ilCol,&ilPos);
	FindItemInList(pcgRudFields,"EADB",',',&igRudEADB,&ilCol,&ilPos);
	FindItemInList(pcgRudFields,"FADD",',',&igRudFADD,&ilCol,&ilPos);
	FindItemInList(pcgRudFields,"FFPD",',',&igRudFFPD,&ilCol,&ilPos);
	FindItemInList(pcgRudFields,"HOPO",',',&igRudHOPO,&ilCol,&ilPos);
	FindItemInList(pcgRudFields,"LADE",',',&igRudLADE,&ilCol,&ilPos);
	FindItemInList(pcgRudFields,"LSTU",',',&igRudLSTU,&ilCol,&ilPos);
	FindItemInList(pcgRudFields,"MAXD",',',&igRudMAXD,&ilCol,&ilPos);
	FindItemInList(pcgRudFields,"MIND",',',&igRudMIND,&ilCol,&ilPos);
	FindItemInList(pcgRudFields,"REPD",',',&igRudREPD,&ilCol,&ilPos);
	FindItemInList(pcgRudFields,"RTDB",',',&igRudRTDB,&ilCol,&ilPos);
	FindItemInList(pcgRudFields,"RTDE",',',&igRudRTDE,&ilCol,&ilPos);
	FindItemInList(pcgRudFields,"SDTI",',',&igRudSDTI,&ilCol,&ilPos);
	FindItemInList(pcgRudFields,"SUTI",',',&igRudSUTI,&ilCol,&ilPos);
	FindItemInList(pcgRudFields,"TSDB",',',&igRudTSDB,&ilCol,&ilPos);
	FindItemInList(pcgRudFields,"TSDE",',',&igRudTSDE,&ilCol,&ilPos);
	FindItemInList(pcgRudFields,"TSPD",',',&igRudTSPD,&ilCol,&ilPos);
	FindItemInList(pcgRudFields,"TTGF",',',&igRudTTGF,&ilCol,&ilPos);
	FindItemInList(pcgRudFields,"TTGT",',',&igRudTTGT,&ilCol,&ilPos);
	FindItemInList(pcgRudFields,"UDGR",',',&igRudUDGR,&ilCol,&ilPos);
	FindItemInList(pcgRudFields,"UGHS",',',&igRudUGHS,&ilCol,&ilPos);
	FindItemInList(pcgRudFields,"UPDE",',',&igRudUPDE,&ilCol,&ilPos);
	FindItemInList(pcgRudFields,"URNO",',',&igRudURNO,&ilCol,&ilPos);
	FindItemInList(pcgRudFields,"URUE",',',&igRudURUE,&ilCol,&ilPos);
	FindItemInList(pcgRudFields,"RETY",',',&igRudRETY,&ilCol,&ilPos);
	FindItemInList(pcgRudFields,"USES",',',&igRudUSES,&ilCol,&ilPos);
	FindItemInList(pcgRudFields,"UTPL",',',&igRudUTPL,&ilCol,&ilPos);

	ilDBFields = get_no_of_items(pcgRudFields);
	FindItemInList(pcgRudAddFields,"FLAG",',',&igRudFLAG,&ilCol,&ilPos);
	if ( igRudFLAG >= 0 )
		igRudFLAG += ilDBFields;
	if ( (igRudUTPL<=0) || (igDemUTPL<=0) )
	{
		dbg ( TRACE, "InitFieldIndex: igRudUTPL <%d> igDemUTPL <%d> => Reset load format!",
			  igRudUTPL, igDemUTPL );
	    igLoadSel = 0; 
	}
	FindItemInList(pcgRudAddFields,"QUCO",',',&igRudQUCO,&ilCol,&ilPos);
	if ( igRudQUCO >= 0 )
		igRudQUCO += ilDBFields;
	FindItemInList(pcgRudAddFields,"RECO",',',&igRudRECO,&ilCol,&ilPos);
	if ( igRudRECO >= 0 )
		igRudRECO += ilDBFields;

	FindItemInList(RPF_FIELDS,"FCCO",',',&igRpfFCCO,&ilCol,&ilPos);
	FindItemInList(RPF_FIELDS,"GTAB",',',&igRpfGTAB,&ilCol,&ilPos);
	FindItemInList(RPF_FIELDS,"UPFC",',',&igRpfUPFC,&ilCol,&ilPos);

	FindItemInList(RPQ_FIELDS,"QUCO",',',&igRpqQUCO,&ilCol,&ilPos);
	FindItemInList(RPQ_FIELDS,"GTAB",',',&igRpqGTAB,&ilCol,&ilPos);
	FindItemInList(RPQ_FIELDS,"UPER",',',&igRpqUPER,&ilCol,&ilPos);

	FindItemInList(REQ_FIELDS,"EQCO",',',&igReqEQCO,&ilCol,&ilPos);
	FindItemInList(REQ_FIELDS,"GTAB",',',&igReqGTAB,&ilCol,&ilPos);
	FindItemInList(REQ_FIELDS,"UEQU",',',&igReqUEQU,&ilCol,&ilPos);

	FindItemInList(RLO_FIELDS,"RLOC",',',&igRloRLOC,&ilCol,&ilPos);
	FindItemInList(RLO_FIELDS,"GTAB",',',&igRloGTAB,&ilCol,&ilPos);
	FindItemInList(RLO_FIELDS,"REFT",',',&igRloREFT,&ilCol,&ilPos);

	FindItemInList(SGR_FIELDS,"GRPN",',',&igSgrGRPN,&ilCol,&ilPos);
	
	return ilRc;
}


static int InitDemhdl()
{
	int  ilRc = RC_SUCCESS;			/* Return code */
	char clSection[128];
	char clKeyword[64];
	char *pclCfgBuffer;
	int ilIndex = 0, ilDummy;
	long pclAddFieldLens[12];
	long llRecoLen = -1, llQucoLen=-1;

	/* BST und SMI wollen das so */
	cgSoh[0] = 0x01;
	cgSoh[1] = 0x00;

	cgStx[0] = 0x02;
	cgStx[1] = 0x00;

 cgDc1[0] = 0x11 ;
 cgDc1[1] = 0x00 ;

 cgEtx[0] = 0x03 ;
 cgEtx[1] = 0x00 ;

	cgKomma[0] = ',';
	cgKomma[1] = 0x00;

	/* now reading from configfile or from database */

	if(ilRc == RC_SUCCESS)
	{
		/* read HomeAirPort from SGS.TAB */
		memset((void*)&cgHopo[0], 0x00, 8);

		sprintf(&clSection[0],"SYS");
		sprintf(&clKeyword[0],"HOMEAP");

		ilRc = tool_search_exco_data(&clSection[0], &clKeyword[0], &cgHopo[0]);
		if (ilRc != RC_SUCCESS)
		{
			dbg(TRACE, "<InitDemhdl> EXTAB,%s,%s not found in SGS.TAB",
				&clSection[0], &clKeyword[0]);
			Terminate(30);
		} else {
			dbg(TRACE,"<InitDemhdl> home airport    <%s>",&cgHopo[0]);
		}/* end of if */
	}/* end of if */

	if(ilRc == RC_SUCCESS)
	{
		sprintf(&clSection[0],"ALL");
		sprintf(&clKeyword[0],"TABEND");

		ilRc = tool_search_exco_data(&clSection[0], &clKeyword[0], &cgTabEnd[0]);
		if (ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"<InitDemhdl> EXTAB,%s,%s not found in SGS.TAB",&clSection[0], &clKeyword[0]);
			Terminate(30);
		} else {
			dbg(TRACE,"<InitDemhdl> table extension <%s>",&cgTabEnd[0]);
		}/* end of if */
	}/* end of if */

	if(ilRc == RC_SUCCESS)
	{
		 GetDebugLevel("STARTUP_MODE", &igStartUpMode);
		 GetDebugLevel("RUNTIME_MODE", &igRuntimeMode);
	}
	debug_level = igStartUpMode;

	memset(prgAlocRefInbound,0,sizeof(prgAlocRefInbound));
	memset(prgAlocRefOutbound,0,sizeof(prgAlocRefOutbound));

	if(ilRc == RC_SUCCESS)
	{
		sprintf(&clSection[0],"MAIN");
		sprintf(&clKeyword[0],"regnaloc");

		ilRc = iGetConfigEntry(cgConfigFile,clSection,clKeyword,CFG_STRING,cgCfgBuffer);
		ilRc = iGetConfigRow(cgConfigFile,clSection,clKeyword,CFG_STRING,cgCfgBuffer);
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"InitDemhdl: not found: <%s> <%s> in <%s>",&clSection[0],&clKeyword[0],cgConfigFile);
		}
		else
		{
			dbg(TRACE,"InitDemhdl: <%s> <%s> <%s>",&clSection[0],&clKeyword[0],cgCfgBuffer);
			strcpy(cgRegnAloc,cgCfgBuffer);
		}/* end of if */
	}/* end of if */


	if(ilRc == RC_SUCCESS)
	{
		sprintf(&clSection[0],"MAIN");
		sprintf(&clKeyword[0],"aloci");

		ilRc = iGetConfigEntry(cgConfigFile,clSection,clKeyword,CFG_STRING,cgCfgBuffer);
		ilRc = iGetConfigRow(cgConfigFile,clSection,clKeyword,CFG_STRING,cgCfgBuffer);
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"InitDemhdl: not found: <%s> <%s> in <%s>",&clSection[0],&clKeyword[0],cgConfigFile);
		}else
		{
			dbg(TRACE,"InitDemhdl: <%s> <%s> <%s>",&clSection[0],&clKeyword[0],cgCfgBuffer);
			ilIndex = 0;
			pclCfgBuffer = cgCfgBuffer;
			do
			{
				pclCfgBuffer = 
					GetRef(pclCfgBuffer,&prgAlocRefInbound[ilIndex++]);
			} while (*pclCfgBuffer != '\0');
			ilIndex = 0;
			while(*prgAlocRefInbound[ilIndex].Name != '\0')
			{
				int ilTableNo = 0;
				dbg(TRACE,"Name: <%s>",prgAlocRefInbound[ilIndex].Name);
				while (*prgAlocRefInbound[ilIndex].Table[ilTableNo] != '\0')
				{
					dbg(TRACE,"Table: <%s>, Field: <%s>",
						prgAlocRefInbound[ilIndex].Table[ilTableNo],
						prgAlocRefInbound[ilIndex].Field[ilTableNo]);
					ilTableNo++;
				}
				ilIndex++;
			}

		}/* end of if */
	}/* end of if */


	if(ilRc == RC_SUCCESS)
	{
		sprintf(&clSection[0],"MAIN");
		sprintf(&clKeyword[0],"aloco");
		ilIndex = 0;

		ilRc = iGetConfigRow(cgConfigFile,clSection,clKeyword,CFG_STRING,cgCfgBuffer);
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"InitDemhdl: not found: <%s> <%s> in <%s>",&clSection[0],&clKeyword[0],cgConfigFile);
		}else
		{
			dbg(TRACE,"InitDemhdl: <%s> <%s> <%s>",&clSection[0],&clKeyword[0],cgCfgBuffer);
			ilIndex = 0;
			pclCfgBuffer = cgCfgBuffer;
			do
			{
				pclCfgBuffer = 
					GetRef(pclCfgBuffer,&prgAlocRefOutbound[ilIndex++]);
			} while (*pclCfgBuffer != '\0');

			ilIndex = 0;
			while(*prgAlocRefOutbound[ilIndex].Name != '\0')
			{
				int ilTableNo = 0;
				dbg(TRACE,"Name: <%s>",prgAlocRefOutbound[ilIndex].Name);
				while (*prgAlocRefOutbound[ilIndex].Table[ilTableNo] != '\0')
				{
					dbg(TRACE,"Table: <%s>, Field: <%s>",
						prgAlocRefOutbound[ilIndex].Table[ilTableNo],
						prgAlocRefOutbound[ilIndex].Field[ilTableNo]);
					ilTableNo++;
				}
				ilIndex++;
			}

		}/* end of if */
	}

	if(ilRc == RC_SUCCESS)
	{
		sprintf(&clSection[0], "DEMANDLEN");
		sprintf(&clKeyword[0], "MIN_DURATION");

		ilRc = iGetConfigRow(cgConfigFile, clSection, clKeyword, CFG_STRING,
								cgCfgBuffer);
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"InitDemhdl: not found: <%s> <%s> in <%s>",
				&clSection[0], &clKeyword[0], cgConfigFile);
			strcpy(cgMinDedu, MINDEDUDEFAULT);
			ilRc = RC_SUCCESS;
		}
		else
		{
			dbg(TRACE, "InitDemhdl: <%s> <%s> <%s>", &clSection[0],
				&clKeyword[0], cgCfgBuffer);
			strcpy(cgMinDedu, cgCfgBuffer);
			if (!atoi(cgMinDedu))
				strcpy(cgMinDedu, MINDEDUDEFAULT);
		}
		dbg(TRACE,"Minimal length of demand: %s",cgMinDedu);

		ilRc = iGetConfigRow ( cgConfigFile, "CCI", "MIN_GAP_LENGTH", 
						  CFG_STRING, cgCfgBuffer );
		if ( ilRc == RC_SUCCESS )
			igTOIgnore = atoi ( cgCfgBuffer );
		else
		{
			dbg(TRACE,"InitDemhdl: not found: <CCI>, <MIN_GAP_LENGTH> in <%s>",
					   cgConfigFile);
			ilRc = RC_SUCCESS;
		}
		dbg(TRACE,"InitDemhdl: Gaps <= %d minutes will be ignored",igTOIgnore);

	}

	if(ilRc == RC_SUCCESS)
	{
		sprintf(&clSection[0],"MAIN");
		sprintf(&clKeyword[0],"SendSBC");

		ilRc = iGetConfigRow(cgConfigFile,clSection,clKeyword,CFG_STRING,cgCfgBuffer);
		if(ilRc != RC_SUCCESS)
		{	/* parameter missing -> take default and go on */
			dbg(TRACE,"InitDemhdl: not found: <%s> <%s> in <%s> take default",&clSection[0],&clKeyword[0],cgConfigFile);
			ilRc = RC_SUCCESS; 
		}
		else
		{
			if ( strcmp(cgCfgBuffer,"YES") == 0)
				bgSendSBC = TRUE;
		}/* end of if */
		dbg(TRACE,"InitDemhdl: SendSBC <%d>", bgSendSBC );
	}/* end of if */
	
	if(ilRc == RC_SUCCESS)
	{
		sprintf(&clSection[0],"MAIN");
		sprintf(&clKeyword[0],"SendUPDDEM");

		ilRc = iGetConfigRow(cgConfigFile,clSection,clKeyword,CFG_STRING,cgCfgBuffer);
		if(ilRc != RC_SUCCESS)
		{	/* parameter missing -> take default and go on */
			dbg(TRACE,"InitDemhdl: not found: <%s> <%s> in <%s> take default",clSection,clKeyword,cgConfigFile);
			ilRc = RC_SUCCESS; 
		}
		else
		{
			if ( strcmp(cgCfgBuffer,"YES") == 0)
				bgSendUpdDem = TRUE;
		}/* end of if */
		dbg(TRACE,"InitDemhdl: SendUPDDEM <%d>", bgSendUpdDem );
	}/* end of if */
	
	sprintf(&clSection[0],"MAIN");
	strcpy(clKeyword,"UseFidsTimes");

	ilRc = iGetConfigRow(cgConfigFile,clSection,clKeyword,CFG_STRING,cgCfgBuffer);
	if(ilRc != RC_SUCCESS)
	{	/* parameter missing -> take default and go on */
		dbg(TRACE,"InitDemhdl: not found: <%s> <%s> in <%s> take default",&clSection[0],&clKeyword[0],cgConfigFile);
		ilRc = RC_SUCCESS; 
	}
	else
	{
		if ( strcmp(cgCfgBuffer,"YES") == 0)
		{
			bgUseFIDSTimes = TRUE;
		}
	}/* end of if */
	dbg(TRACE,"InitDemhdl: UseFIDSTimes <%d>", bgUseFIDSTimes );

	ilRc = iGetConfigRow ( cgConfigFile, "MAIN", "DelInvalidDemands", CFG_INT, (char *)&ilDummy);
	if ( ilRc == RC_SUCCESS )
	{
		dbg ( TRACE,"InitDemhdl: Deletion of invalid demands is handled by MODID <%d>", ilDummy );
		if ( ilDummy == mod_id )
			bgDelInvalidDemands = TRUE;
	}
	dbg(TRACE,"InitDemhdl: bgDelInvalidDemands <%d>", bgDelInvalidDemands );


	bgUseFilt = FALSE;
	ilRc = iGetConfigRow(cgConfigFile,"MAIN","UseFilt", CFG_STRING,cgCfgBuffer);
	if( (ilRc == RC_SUCCESS) && (strcmp(cgCfgBuffer,"YES") == 0) )
	{
		bgUseFilt = TRUE;
		dbg(TRACE,"UseFilt set to TRUE");
	}
	else
	{
		bgUseFilt = FALSE;
		dbg(TRACE,"UseFilt not found, set to FALSE");
	}

	ilRc = iGetConfigRow ( cgConfigFile, "SYSTEM", "TIME_TRACING", CFG_STRING, cgCfgBuffer );
	if ( ilRc == RC_SUCCESS )
	{
		tgMaxStampDiff = atol ( cgCfgBuffer );
		dbg(TRACE,"InitDemhdl: Warning if processing of event lasts longer than %d seconds (since flight)",
			tgMaxStampDiff );
	}
	else
	{
		dbg(TRACE,"InitDemhdl: not found: <SYSTEM>, <TIME_TRACING> in <%s>", cgConfigFile);
		ilRc = RC_SUCCESS;
	}

	ilRc = ReadConfigEntries();

	if ( pcgTemplates = malloc ( 512 ) )
	{
		pcgTemplates[0] = '\0';
		igLenofTemplates = 512;
		dbg ( DEBUG, "InitDemhdl: Allocated 512 Bytes for Templatebuffer" );
	}
	else
	{
		dbg ( TRACE, "InitDemhdl: Unable to allocated 512 Bytes for Templatebuffer" );
		ilRc = RC_NOMEM;
	}
	if(ilRc == RC_SUCCESS)
	{
		ilRc = CEDAArrayInitialize(10,10);
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"InitDemhdl: CEDAArrayInitialize failed <%d>",ilRc);
		}/* end of if */
	}/* end of if */

	if(ilRc == RC_SUCCESS)
	{
	/** read TICH ***********/
		char pclDataArea[2256];
		char pclSqlBuf[256];
		short slCursor = 0;
		sprintf(pclSqlBuf,
			"SELECT TICH,TDI1,TDI2 FROM APTTAB WHERE APC3 = '%s'",cgHopo);
		ilRc = sql_if(START,&slCursor,pclSqlBuf,pclDataArea);
		if (ilRc != DB_SUCCESS)
		{
			dbg(TRACE,"Home Airport <%s> not found in APTTAB %d",cgHopo,ilRc);
			check_ret(ilRc);
		} /* end while */
		else
		{
			strcpy(cgTdi1,"60");
			strcpy(cgTdi2,"60");
			get_fld(pclDataArea,0,STR,14,cgTich);
			get_fld(pclDataArea,1,STR,14,cgTdi1);
			get_fld(pclDataArea,2,STR,14,cgTdi2);
			TrimRight(cgTich);
			if (*cgTich != '\0')
			{
				TrimRight(cgTdi1);
				TrimRight(cgTdi2);
				sprintf(cgTdi1,"%d",atoi(cgTdi1)*60);
				sprintf(cgTdi2,"%d",atoi(cgTdi2)*60);
			}
			dbg(TRACE,"Tich: <%s> Tdi1: <%s> Tdi2 <%s>",
				cgTich,cgTdi1,cgTdi2);
		}
		close_my_cursor(&slCursor);
		slCursor = 0;
	}

    igActionId = tool_get_q_id ("action");
    dbg (TRACE, "InitDemhdl: action mod_id <%d> found", igActionId );
    
	if(ilRc == RC_SUCCESS)
	{
		dbg(DEBUG,"InitDemhdl: SetArrayInfo new demand array");
		strcpy ( pcgDemFields, DEM_FIELDS );
		if ( GetFieldLength( &cgHopo[0], "DEMTAB", "USES", pclAddFieldLens ) 
			 == RC_SUCCESS )
		{
			strcat ( pcgDemFields, ",USES" );
		}
		else
		{
			dbg ( TRACE,"InitDemhdl: field DEMTAB.USES not in DB configuration" );
		}
		if ( GetFieldLength( &cgHopo[0], "DEMTAB", "UTPL", pclAddFieldLens ) 
			 == RC_SUCCESS )
		{
			strcat ( pcgDemFields, ",UTPL" );
		}
		else
		{
			dbg ( TRACE,"InitDemhdl: field DEMTAB.UTPL not in DB configuration" );
		}

		if ( GetFieldLength( &cgHopo[0], "DEMTAB", "UPRM", pclAddFieldLens ) 
			 == RC_SUCCESS )
		{
			strcat ( pcgDemFields, ",UPRM" );
		}
		else
		{
			dbg ( TRACE,"InitDemhdl: field DEMTAB.UPRM not in DB configuration" );
		}

		if ( GetFieldLength( &cgHopo[0], "DEMTAB", "FILT", pclAddFieldLens ) 
			 == RC_SUCCESS )
		{
			strcat ( pcgDemFields, ",FILT" );
		}
		else
		{
			dbg ( TRACE,"InitDemhdl: field DEMTAB.FILT not in DB configuration" );
		}


		if ( GetFieldLength( cgHopo, "DEMTAB", "RECO", &llRecoLen ) 
			 == RC_SUCCESS )
		{
			strcat ( pcgDemFields, ",RECO" );
		}
		else
		{
			dbg ( TRACE,"InitDemhdl: field DEMTAB.RECO not in DB configuration" );
			llRecoLen = -1;
		}
		if ( GetFieldLength( cgHopo, "DEMTAB", "QUCO", &llQucoLen ) 
			 == RC_SUCCESS )
		{
			strcat ( pcgDemFields, ",QUCO" );
		}
		else
		{
			dbg ( TRACE,"InitDemhdl: field DEMTAB.QUCO not in DB configuration" );
			llQucoLen = -1;
		}
		if ( GetFieldLength( cgHopo, "DEMTAB", "UGHS", pclAddFieldLens ) 
			 == RC_SUCCESS )
		{
			strcat ( pcgDemFields, ",UGHS" );
		}
		else
		{
			dbg ( TRACE,"InitDemhdl: field DEMTAB.UGHS not in DB configuration" );
		}

		pclAddFieldLens[0] = 1;
		pclAddFieldLens[1] = 1;
		pclAddFieldLens[2] = 0;
		pclAddFieldLens[3] = 0;
		pclAddFieldLens[4] = 0;
		ilRc = SetArrayInfo("NEWDEMANDS","DEMTAB",pcgDemFields,
							ADD_DEM_FIELDS,pclAddFieldLens,&rgNewDemArray);
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"InitDemhdl: SetArrayInfo NEWDEMANDS failed <%d>",ilRc);
		}
		else
		{
			ilRc = CreateIdx1(&rgNewDemArray, "IDXNDEM", "URUD", "A");
			if (ilRc != RC_SUCCESS)
 				dbg(TRACE, "Init_demhdl:CreateIdx1 for NEWDEMANDS failed <%d>",
					ilRc);
			dbg(TRACE, "InitDemhdl: SetArrayInfo NEWDEMANDS OK");
			dbg(TRACE, "<%s> %d", rgNewDemArray.crArrayName,
				rgNewDemArray.rrArrayHandle);
			CEDAArraySendChanges2ACTION(&rgNewDemArray.rrArrayHandle,
   										rgNewDemArray.crArrayName, TRUE);
			CEDAArraySendChanges2BCHDL(&rgNewDemArray.rrArrayHandle,
										rgNewDemArray.crArrayName, TRUE);
		}
	}

	if(ilRc == RC_SUCCESS)
	{
		dbg(DEBUG,"InitDemhdl: SetArrayInfo DEMTAB start");

		pclAddFieldLens[0] = 1;
		pclAddFieldLens[1] = 1;
		pclAddFieldLens[2] = 0;
		pclAddFieldLens[3] = 0;
		pclAddFieldLens[4] = 0;
		ilRc = SetArrayInfo("DEMANDS","DEMTAB",pcgDemFields,
							ADD_DEM_FIELDS, pclAddFieldLens,&rgOldDemArray);
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"InitDemhdl: SetArrayInfo DEMANDS failed <%d>",ilRc);
		}
		else
		{
			ilRc = CreateIdx1(&rgOldDemArray, "IDXODEM", "URUD,JFND", "A,D");
			if (ilRc != RC_SUCCESS)
 				dbg(TRACE, "Init_demhdl:CreateIdx1 for DEMANDS failed <%d>", ilRc);
			
			dbg(TRACE,"InitDemhdl: SetArrayInfo DEMANDS OK");
			CEDAArraySendChanges2ACTION(&rgOldDemArray.rrArrayHandle,
								        rgOldDemArray.crArrayName,TRUE);
			CEDAArraySendChanges2BCHDL( &rgOldDemArray.rrArrayHandle,
							            rgOldDemArray.crArrayName,TRUE);
		}/* end of if */
	}
	if(ilRc == RC_SUCCESS)
	{
		dbg(DEBUG,"InitDemhdl: SetArrayInfo CCIDEMs start");

		ilRc = SetArrayInfo("CCIDEM","DEMTAB",pcgDemFields,NULL,NULL,
							&rgCCIDemArray);
 		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"InitDemhdl: SetArrayInfo CCIDEM failed <%d>",ilRc);
		}else{
			ilRc = CreateIdx1(&rgCCIDemArray,"IDXDEM1","URUD,DEEN,DEBE",
								"A,A,D");
			if (ilRc!=RC_SUCCESS)
 				dbg(TRACE,"Init_demhdl:CreateIdx1 for CCIDEM failed <%d>",ilRc);
 			ilRc = CreateIdx2(&rgCCIDemArray,"IDXDEM2","URUD,DEBE,DEEN",
								"A,A,D" );
 			if (ilRc!=RC_SUCCESS)
				dbg(TRACE,"Init_demhdl:CreateIdx2 for CCIDEM failed <%d>",ilRc);
			CEDAArraySendChanges2ACTION(&rgCCIDemArray.rrArrayHandle,
								          rgCCIDemArray.crArrayName,TRUE);
			CEDAArraySendChanges2BCHDL(&rgCCIDemArray.rrArrayHandle,
								          rgCCIDemArray.crArrayName,!bgSendSBC);
			dbg(TRACE,"InitDemhdl: SetArrayInfo CCIDEM OK");
		}

	}

	if(ilRc == RC_SUCCESS)
	{
		dbg(DEBUG,"InitDemhdl: SetArrayInfo RUDTAB start");
		strcpy ( pcgRudFields, RUD_FIELDS );
		strcpy ( pcgRudAddFields, ADD_RUD_FIELDS );
		if ( GetFieldLength( &cgHopo[0], "RUDTAB", "USES", pclAddFieldLens ) 
			 == RC_SUCCESS )
		{
			strcat ( pcgRudFields, ",USES" );
		}
		else
		{
			dbg ( TRACE,"InitDemhdl: field RUDTAB.USES not in DB configuration" );
		}
		if ( GetFieldLength( &cgHopo[0], "RUDTAB", "UTPL", pclAddFieldLens ) 
			 == RC_SUCCESS )
		{
			strcat ( pcgRudFields, ",UTPL" );
		}
		else
		{
			dbg ( TRACE,"InitDemhdl: field RUDTAB.UTPL not in DB configuration" );
		}
		dbg ( DEBUG, "Actual RUDTAB-fields: %s", pcgRudFields );

		pclAddFieldLens[0] = 2;
		pclAddFieldLens[1] = pclAddFieldLens[2] = pclAddFieldLens[3] = 0;
		if ( llRecoLen > 0 )
		{
			pclAddFieldLens[1] = llRecoLen;
			strcat ( pcgRudAddFields, ",RECO" );
			if ( llQucoLen > 0 ) 
			{
				pclAddFieldLens[2] = llQucoLen;
				strcat ( pcgRudAddFields, ",QUCO" );
			}
		}
		dbg ( TRACE,"InitDemhdl: Addiditonal RUD-Fields <%s>", pcgRudAddFields );

		ilRc = SetArrayInfo("RUDTAB", "RUDTAB",	pcgRudFields, pcgRudAddFields,
							pclAddFieldLens, &rgRudArray);
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"InitDemhdl: SetArrayInfo RUDTAB failed <%d>",ilRc);
		}else
		{
			ilRc = CreateIdx1(&rgRudArray,"IDXRUD1","URNO","A");
			if ( ilRc != RC_SUCCESS )
				dbg(TRACE,"Init_demhdl: CreateIdx1 for RUD failed <%d>", ilRc);
			else
				dbg(TRACE,"InitDemhdl: SetArrayInfo RUDTAB OK");
		}
	}

	if(ilRc == RC_SUCCESS)
	{
		dbg(DEBUG,"InitDemhdl: SetArrayInfo CCIRUDTAB start");

		ilRc = SetArrayInfo("CCIRUD","RUDTAB",pcgRudFields,pcgRudAddFields,
							pclAddFieldLens, &rgCCIRudArray);
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"InitDemhdl: SetArrayInfo CCIRUDTAB failed <%d>",ilRc);
		}
		else
		{
			strcpy ( clSection, "WHERE URUE in (select URNO from RUETAB where rust!='2')" );
			ilRc = CEDAArrayRefill (&rgCCIRudArray.rrArrayHandle,
									rgCCIRudArray.crArrayName, clSection, NULL, ARR_FIRST );
			/* ilRc = CEDAArrayFill(&rgCCIRudArray.rrArrayHandle,
									rgCCIRudArray.crArrayName, NULL); */
			if(ilRc != RC_SUCCESS)
			{
				dbg(TRACE,"SetArrayInfo: CEDAArrayRefill failed <%d>",ilRc);
			}
			else
			{
				dbg(TRACE,"InitDemhdl: SetArrayInfo CCIRUDTAB OK");
				ilRc = CreateIdx1(&rgCCIRudArray,"IDXRUD1","URNO","A");
				if (ilRc!=RC_SUCCESS)
					dbg(TRACE,"Init_demhdl: CreateIdx1 for RUD failed <%d>",
						ilRc);
				else
				{
		 			ilRc = CreateIdx2 ( &rgCCIRudArray,"IDXRUD2","URUE,FADD,FFPD,UPDE",
										"A,A,A,A" );
					/*
					ilRc = CEDAArrayCreateSimpleIndexUp(&rgCCIRudArray.rrArrayHandle,
														rgCCIRudArray.crArrayName,
														&rgCCIRudArray.rrIdx02Handle,
														rgCCIRudArray.crIdx02Name,
														"URUE,FADD,FFPD,UPDE" );*/
					if (ilRc!=RC_SUCCESS)
						dbg(TRACE,"Init_demhdl: CreateIdx2 for RUD failed <%d>",
							ilRc);
				}
				ConfigureAction("RUDTAB", 0, 0 );
			}
		}
	}

	/*if(ilRc == RC_SUCCESS)
	{
		dbg(TRACE,"InitDemhdl: SetArrayInfo AFTTAB start");

		ilRc = SetArrayInfo("AFTTAB","AFTTAB",AFT_FIELDS,NULL,NULL,&rgAftArray);
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"InitDemhdl: SetArrayInfo AFTTAB failed <%d>",ilRc);
		}else{
			dbg(TRACE,"InitDemhdl: SetArrayInfo AFTTAB OK");
		}
	}*/

	if(ilRc == RC_SUCCESS)
	{
		dbg(DEBUG,"InitDemhdl: SetArrayInfo RUETAB start");

		ilRc = SetArrayInfo("RUETAB","RUETAB",RUE_FIELDS,NULL,NULL,&rgRueArray);
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"InitDemhdl: SetArrayInfo RUETAB failed <%d>",ilRc);
		}
		else
		{
			ConfigureAction("RUETAB", RUE_FIELDS, 0 );
			CEDAArrayRefill(&rgRueArray.rrArrayHandle,rgRueArray.crArrayName,
						"WHERE RUST!='2'", "", ARR_FIRST );
			ilRc = CreateIdx1(&rgRueArray,"IDXRUE1","URNO","A");
			if ( ilRc != RC_SUCCESS )
				dbg(TRACE,"Init_demhdl: CreateIdx1 for RUE failed <%d>", ilRc);
			else
				dbg(TRACE,"InitDemhdl: SetArrayInfo RUETAB OK");
			CEDAArraySendChanges2ACTION(&rgRueArray.rrArrayHandle,
   										rgRueArray.crArrayName, TRUE);
			CEDAArraySendChanges2BCHDL(&rgRueArray.rrArrayHandle,
										rgRueArray.crArrayName, TRUE);
		}
	}

	if ( CheckDBTable ( "JODTAB", cgConfigFile ) != RC_SUCCESS )
		bgUseJODTAB = FALSE;
	dbg(TRACE,"InitDemhdl: UseJODTAB <%d>",bgUseJODTAB);

	if(ilRc == RC_SUCCESS)
	{
		if ( bgUseJODTAB )
		{
			dbg(DEBUG,"InitDemhdl: SetArrayInfo JODTAB start");

			ilRc = SetArrayInfo("JODTAB","JODTAB","URNO,UDEM",NULL,NULL,&rgJodArray);
			if(ilRc != RC_SUCCESS)
			{
				dbg(TRACE,"InitDemhdl: SetArrayInfo JODTAB failed <%d>",ilRc);
			}
			else
			{
				ilRc = CreateIdx1(&rgJodArray,"IDXUDEM","UDEM","A");
				if ( ilRc != RC_SUCCESS )
					dbg(TRACE,"Init_demhdl: CreateIdx1 for JOD failed <%d>", ilRc);
				else
					dbg(TRACE,"InitDemhdl: SetArrayInfo JODTAB OK");
				if ( bgDelInvalidDemands )
					ConfigureAction("JODTAB", 0, "DRT" );
				else
					ChangeDynActionConfig ( "JODTAB", 0, "DRT", FALSE, TRUE );
			}
		}
		else
		{/*  jobs are linked to demands using JOBTAB.UDEM instead of JODTAB */
			dbg(DEBUG,"InitDemhdl: SetArrayInfo JOBTAB start");

			ilRc = SetArrayInfo("JOBTAB","JOBTAB","URNO,UDEM",NULL,NULL,&rgJobArray);
			if(ilRc != RC_SUCCESS)
			{
				dbg(TRACE,"InitDemhdl: SetArrayInfo JOBTAB failed <%d>",ilRc);
			}
			else
			{
				ilRc = CreateIdx1(&rgJobArray,"IDXUDEM","UDEM","A");
				if ( ilRc != RC_SUCCESS )
					dbg(TRACE,"Init_demhdl: CreateIdx1 for JOB failed <%d>", ilRc);
				else
					dbg(TRACE,"InitDemhdl: SetArrayInfo JOBTAB OK");
				if ( bgDelInvalidDemands )
					ConfigureAction("JOBTAB", 0, "DRT" );
				else
					ChangeDynActionConfig ( "JOBTAB", 0, "DRT", FALSE, TRUE );
				ChangeDynActionConfig ( "JODTAB", 0, "DRT", FALSE, TRUE );
			}
		}
	}

	if(ilRc == RC_SUCCESS)
	{
		dbg(DEBUG,"InitDemhdl: SetArrayInfo RPFTAB start");

		ilRc = SetArrayInfo("RPFTAB","RPFTAB",RPF_FIELDS,NULL,NULL,&rgRpfArray);
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"InitDemhdl: SetArrayInfo RPFTAB failed <%d>",ilRc);
		}
		else
		{
			ConfigureAction("RPFTAB", RPF_FIELDS, 0 );
			strcpy ( clSection, "WHERE URUD IN (SELECT URNO FROM RUDTAB WHERE URUE in (select URNO from RUETAB where rust!='2'))" );
			CEDAArrayRefill(&rgRpfArray.rrArrayHandle,rgRpfArray.crArrayName, clSection, "", ARR_FIRST );
			ilRc = CreateIdx1(&rgRpfArray,"IDXRPF1","URUD","A");
			if ( ilRc != RC_SUCCESS )
				dbg(TRACE,"Init_demhdl: CreateIdx1 for RPF failed <%d>", ilRc);
			else
				dbg(TRACE,"InitDemhdl: SetArrayInfo RPFTAB OK");
		}
	}

	if(ilRc == RC_SUCCESS)
	{
		dbg(DEBUG,"InitDemhdl: SetArrayInfo RPQTAB start");

		ilRc = SetArrayInfo("RPQTAB","RPQTAB",RPQ_FIELDS,NULL,NULL,&rgRpqArray);
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"InitDemhdl: SetArrayInfo RPQTAB failed <%d>",ilRc);
		}
		else
		{
			ConfigureAction("RPQTAB", RPQ_FIELDS, 0 );
			strcpy ( clSection, "WHERE URUD IN (SELECT URNO FROM RUDTAB WHERE URUE in (select URNO from RUETAB where rust!='2'))" );
			CEDAArrayRefill(&rgRpqArray.rrArrayHandle,rgRpqArray.crArrayName, clSection, "", ARR_FIRST );
			ilRc = CreateIdx1(&rgRpqArray,"IDXRPQ1","URUD","A");
			if ( ilRc != RC_SUCCESS )
				dbg(TRACE,"Init_demhdl: CreateIdx1 for RPQ failed <%d>", ilRc);
			else
				dbg(TRACE,"InitDemhdl: SetArrayInfo RPQTAB OK");
		}
	}

	if(ilRc == RC_SUCCESS)
	{
		dbg(DEBUG,"InitDemhdl: SetArrayInfo REQTAB start");

		ilRc = SetArrayInfo("REQTAB","REQTAB",REQ_FIELDS,NULL,NULL,&rgReqArray);
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"InitDemhdl: SetArrayInfo REQTAB failed <%d>",ilRc);
		}
		else
		{
			ConfigureAction("REQTAB", REQ_FIELDS, 0 );
			strcpy ( clSection, "WHERE URUD IN (SELECT URNO FROM RUDTAB WHERE URUE in (select URNO from RUETAB where rust!='2'))" );
			CEDAArrayRefill(&rgReqArray.rrArrayHandle,rgReqArray.crArrayName, clSection, "", ARR_FIRST );
			ilRc = CreateIdx1(&rgReqArray,"IDXREQ1","URUD","A");
			if ( ilRc != RC_SUCCESS )
				dbg(TRACE,"Init_demhdl: CreateIdx1 for REQ failed <%d>", ilRc);
			else
				dbg(TRACE,"InitDemhdl: SetArrayInfo REQTAB OK");
		}
	}

	if(ilRc == RC_SUCCESS)
	{
		dbg(DEBUG,"InitDemhdl: SetArrayInfo RLOTAB start");

		ilRc = SetArrayInfo("RLOTAB","RLOTAB",RLO_FIELDS,NULL,NULL,&rgRloArray);
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"InitDemhdl: SetArrayInfo RLOTAB failed <%d>",ilRc);
		}
		else
		{
			ConfigureAction("RLOTAB", RLO_FIELDS, 0 );
			strcpy ( clSection, "WHERE URUD IN (SELECT URNO FROM RUDTAB WHERE URUE in (select URNO from RUETAB where rust!='2'))" );
			CEDAArrayRefill(&rgRloArray.rrArrayHandle,rgRloArray.crArrayName, clSection, "", ARR_FIRST );
			ilRc = CreateIdx1(&rgRloArray,"IDXRLO1","URUD","A");
			if ( ilRc != RC_SUCCESS )
				dbg(TRACE,"Init_demhdl: CreateIdx1 for RLO failed <%d>", ilRc);
			else
				dbg(TRACE,"InitDemhdl: SetArrayInfo RLOTAB OK");
		}
	}
	 
	if(ilRc == RC_SUCCESS)
	{
		dbg(DEBUG,"InitDemhdl: SetArrayInfo SGRTAB start");

		ilRc = SetArrayInfo("SGRTAB","SGRTAB",SGR_FIELDS,NULL,NULL,&rgSgrArray);
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"InitDemhdl: SetArrayInfo RLOTAB failed <%d>",ilRc);
		}
		else
		{
			ConfigureAction("SGRTAB", SGR_FIELDS, 0 );
			strcpy ( clSection, "WHERE APPL='RULE_AFT'" );
			CEDAArrayRefill(&rgSgrArray.rrArrayHandle,rgSgrArray.crArrayName, clSection, "", ARR_FIRST );
			ilRc = CreateIdx1(&rgSgrArray,"IDXSGR1","URNO","A");
			if ( ilRc != RC_SUCCESS )
				dbg(TRACE,"Init_demhdl: CreateIdx1 for SGR failed <%d>", ilRc);
			else
				dbg(TRACE,"InitDemhdl: SetArrayInfo SGRTAB OK");
		}
	}

	if(ilRc == RC_SUCCESS)
	{
		dbg(DEBUG,"InitDemhdl: SetArrayInfo ALTTAB start");

		ilRc = SetArrayInfo("ALTTAB","ALTTAB",ALT_FIELDS,NULL,NULL,&rgAltArray);
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"InitDemhdl: SetArrayInfo ALTTAB failed <%d>",ilRc);
		}
		else
		{
			ConfigureAction("ALTTAB", ALT_FIELDS, 0 );
			strcpy ( clSection, "" );
			CEDAArrayRefill(&rgAltArray.rrArrayHandle,rgAltArray.crArrayName, clSection, "", ARR_FIRST );
			ilRc = CreateIdx1(&rgAltArray,"IDXALT1","ALC3","A");
			if ( ilRc != RC_SUCCESS )
			{
				dbg(TRACE,"Init_demhdl: CreateIdx1 for ALT failed <%d>", ilRc);
			}
			else
			{
				dbg(TRACE,"InitDemhdl: SetArrayInfo ALTTAB OK");
				ConfigureAction("ALTTAB", ALT_FIELDS, 0 );
			}
		}
	}


	if(ilRc == RC_SUCCESS)
	{
		ilRc = CEDAArrayReadAllHeader(outp);
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"InitDemhdl: CEDAArrayReadAllHeader failed <%d>",ilRc);
		}else{
			dbg(TRACE,"InitDemhdl: CEDAArrayReadAllHeader OK");
		}/* end of if */
	}/* end of if */


	if(ilRc == RC_SUCCESS)
	{
		ilRc = InitFieldIndex();
	}
	if( (ilRc == RC_SUCCESS) && (igRudQUCO>0)&& (llRecoLen>0) && (llQucoLen>0) )
	{
		dbg ( DEBUG, "initDemhdl: Length of field QUCU <%ld> index <%d>", 
			  rgCCIRudArray.plrArrayFieldLen[igRudQUCO-1], igRudQUCO );
		SetRudLogicalFields ( &rgCCIRudArray, "" );
	}
	if(ilRc == RC_SUCCESS)
	{
		igInitOK = TRUE;
	}/* end of if */

	{
		long llMaxDbgLines = 30000;
	  int ilMaxDbgFiles = 10;

		ilRc = iGetConfigRow (cgConfigFile, "SYSTEM", "MAXDBGLINES", 
						  CFG_STRING, cgCfgBuffer );
		if ( ilRc == RC_SUCCESS )
			llMaxDbgLines = atol ( cgCfgBuffer );
		else
		{
			dbg(TRACE,"InitDemhdl: not found: <SYSTEM>, <MAXDBGLINES> in <%s>",
					   cgConfigFile);
			ilRc = RC_SUCCESS;
		}
		ilRc = iGetConfigRow (cgConfigFile, "SYSTEM", "MAXDBGFILES", 
						  CFG_STRING, cgCfgBuffer );
		if ( ilRc == RC_SUCCESS )
			ilMaxDbgFiles = atoi ( cgCfgBuffer );
		else
		{
			dbg(TRACE,"InitDemhdl: not found: <SYSTEM>, <MAXDBGFILES> in <%s>",
					   cgConfigFile);
			ilRc = RC_SUCCESS;
		}
		SetDbgLimits(llMaxDbgLines,ilMaxDbgFiles);
	}
	
	/* Initialise AAT_PARAM structure for old and new demands */
	ilRc = AATArrayCreateArgDesc(rgOldDemArray.crArrayName, &prgDemInf );
	if ( ilRc == RC_SUCCESS )
	{
		prgDemInf->AppendData = TRUE;
		ToolsListSetInfo(&(prgDemInf->DataList[7]), "IRT_URNO", rgOldDemArray.crTableName, 
						 "IRT", "URNO", "","",",",4096);
		ToolsListSetInfo(&(prgDemInf->DataList[8]), "URT_URNO", rgOldDemArray.crTableName, 
						 "URT", "URNO", "","",",",4096);
		ToolsListSetInfo(&(prgDemInf->DataList[9]), "DRT_DATA", rgOldDemArray.crTableName, 
						 "DRT", rgOldDemArray.crArrayFieldList, "","",",",-1);
		ToolsListSetInfo(&(prgDemInf->DataList[10]),"CMD_LIST","","","","","","\n",-1);
		ToolsListSetInfo(&(prgDemInf->DataList[11]),"SEL_LIST","","","","","","\n",-1);
		ToolsListSetInfo(&(prgDemInf->DataList[12]),"FLD_LIST","","","","","","\n",-1);
		ToolsListSetInfo(&(prgDemInf->DataList[13]),"DAT_LIST","","","","","","\n",-1);
	}
	return(ilRc);
	
} /* end of initialize */



char *GetRef(char *pcpCfgBuffer,struct AlocRefStruct *pcpAlocBuffer)
{
	char *pclResult = NULL;
	char *pclTmp;
	char *pclNext;
	char *pclTable;
	char *pclField;
	char pclTmpBuf[124];
	int ilIndex = 0;


	pclTmp = strchr(pcpCfgBuffer,';');
	if(pclTmp != NULL)
	{
		*pclTmp = '\0';
		pclResult = pclTmp + 1;
	}
	strcpy(pclTmpBuf,pcpCfgBuffer);
	dbg(TRACE,"%05d:GetRef <%s>",__LINE__,pclTmpBuf);
	pclTmp = pclTmpBuf;
	{
		if((pclTable = strchr(pclTmp,',')) != NULL)
		{
			*pclTable = '\0';
			strcpy(pcpAlocBuffer->Name,pclTmp);
			pclTable++;
			pclNext = pclTable;
			do
			{
				pclTable = pclNext;
				pclNext = strchr(pclTable,',');
				if(pclNext != NULL)
				{
					pclNext++;
				}
				pclField = strchr(pclTable,'.');
				if (pclField != NULL)
				{
					*pclField = '\0';
					pclField++;
					strcpy(pcpAlocBuffer->Table[ilIndex],pclTable);
					pclTable = strchr(pclField,',');
					pclField[4] = '\0';
					if(pclTable != NULL)
					{
						*pclTable = '\0';
					}
					strcpy(pcpAlocBuffer->Field[ilIndex++],pclField);
				}
			} while(pclNext != NULL);
		}
	}
	return(pclResult);
}

/******************************************************************************/
/*                                                                            */
/******************************************************************************/
static int SetArrayInfo(char *pcpArrayName, char *pcpTableName, char *pcpArrayFieldList,char *pcpAddFields,long *pcpAddFieldLens,ARRAYINFO *prpArrayInfo)
{
	int	ilRc = RC_SUCCESS;				/* Return code */
	int ilStart, i;

	if(prpArrayInfo == NULL)
	{
		dbg(TRACE,"SetArrayInfo: invalid last parameter : null pointer not valid");
		ilRc = RC_FAIL;
	}else
	{
		memset(prpArrayInfo,0x00,sizeof(prpArrayInfo));

		prpArrayInfo->rrArrayHandle = -1;
		prpArrayInfo->rrIdx01Handle = -1;

		if(pcpArrayName != NULL)
		{
			if(strlen(pcpArrayName) <= ARR_NAME_LEN)
			{
				strcpy(prpArrayInfo->crArrayName,pcpArrayName);
			}/* end of if */
		}/* end of if */
	}/* end of if */

	if(ilRc == RC_SUCCESS)
	{
		prpArrayInfo->lrArrayFieldCnt = get_no_of_items(pcpArrayFieldList);
		if (pcpAddFields != NULL)
		{
			prpArrayInfo->lrArrayFieldCnt += get_no_of_items(pcpAddFields);
		}

		prpArrayInfo->plrArrayFieldOfs = (long *) calloc(prpArrayInfo->lrArrayFieldCnt,sizeof(long));
		if(prpArrayInfo->plrArrayFieldOfs == NULL)
		{
			dbg(TRACE,"SetArrayInfo: plrArrayFieldOfs calloc(%d,%d) failed",prpArrayInfo->lrArrayFieldCnt,sizeof(long));
			ilRc = RC_FAIL;
		}/* end of if */
	}/* end of if */

	if(ilRc == RC_SUCCESS)
	{
		prpArrayInfo->plrArrayFieldLen = (long *) calloc(prpArrayInfo->lrArrayFieldCnt,sizeof(long));
		if(prpArrayInfo->plrArrayFieldLen == NULL)
		{
			dbg(TRACE,"SetArrayInfo: plrArrayFieldLen calloc(%d,%d) failed",prpArrayInfo->lrArrayFieldCnt,sizeof(long));
			ilRc = RC_FAIL;
		}else{
			*(prpArrayInfo->plrArrayFieldLen) = -1;
		}/* end of if */
	}/* end of if */

	if(pcpTableName == NULL)
	{
		dbg(TRACE,"SetArrayInfo: invalid parameter TableName: null pointer not valid");
		ilRc = RC_FAIL;
	}
	else
	{
		strcpy ( prpArrayInfo->crTableName,pcpTableName );
       	dbg(DEBUG,"SetArrayInfo: crTableName=<%s>",prpArrayInfo->crTableName );
    }
   
	if(ilRc == RC_SUCCESS)
   	{
       	ilRc = GetRowLength(&cgHopo[0],pcpTableName,pcpArrayFieldList,&(prpArrayInfo->lrArrayRowLen));
       	if(ilRc != RC_SUCCESS)
       	{
           	dbg(TRACE,"SetArrayInfo: GetRowLength failed <%s> <%s>",pcpArrayName,pcpArrayFieldList);
      	}/* end of if */
   	}/* end of if */

   	if(ilRc == RC_SUCCESS)
   	{
		prpArrayInfo->lrArrayRowLen+=100;
		prpArrayInfo->pcrArrayRowBuf = 0;
		/*prpArrayInfo->pcrArrayRowBuf = (char *) calloc(1,(prpArrayInfo->lrArrayRowLen+10));
   		if(prpArrayInfo->pcrArrayRowBuf == NULL)
   		{
      			ilRc = RC_FAIL;
      			dbg(TRACE,"SetArrayInfo: calloc failed");
   		}
		*//* end of if */
   	}/* end of if */

   	if(ilRc == RC_SUCCESS)
   	{
		prpArrayInfo->pcrIdx01RowBuf = 0; /*PRF6158: memory will be allocated in CreateIdx1 */
		/*prpArrayInfo->pcrIdx01RowBuf = (char *) calloc(1,(prpArrayInfo->lrIdx01RowLen+1));
   		if(prpArrayInfo->pcrIdx01RowBuf == NULL)
   		{
      			ilRc = RC_FAIL;
      			dbg(TRACE,"SetArrayInfo: calloc failed");
   		} */
   	}

	if(ilRc == RC_SUCCESS)
	{
		dbg(TRACE,"CEDAArrayCreate follows Array <%s> Table <%s>",pcpArrayName,pcpTableName);
		ilRc = CEDAArrayCreate(&(prpArrayInfo->rrArrayHandle),
				pcpArrayName,pcpTableName,NULL,pcpAddFields,
				pcpAddFieldLens,pcpArrayFieldList,
				&(prpArrayInfo->plrArrayFieldLen[0]),
				&(prpArrayInfo->plrArrayFieldOfs[0]));

		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"SetArrayInfo: CEDAArrayInitialize failed <%d>",ilRc);
		}/* end of if */
		else
		{
			if (pcpAddFields != NULL)
			{
				ilStart = get_no_of_items(pcpArrayFieldList);
				for ( i=ilStart; i<prpArrayInfo->lrArrayFieldCnt; i++ )
				{
					prpArrayInfo->plrArrayFieldLen[i] = pcpAddFieldLens[i-ilStart];
				}
			}
		}
	}/* end of if */

	if(pcpArrayFieldList != NULL)
	{
		if(strlen(pcpArrayFieldList) <= ARR_FLDLST_LEN)
		{
			strcpy(prpArrayInfo->crArrayFieldList,pcpArrayFieldList);
		}/* end of if */
	}/* end of if */
	if(pcpAddFields != NULL)
	{
		if(strlen(pcpAddFields) + 
			strlen(prpArrayInfo->crArrayFieldList) <= 
					(size_t)ARR_FLDLST_LEN)
		{
			strcat(prpArrayInfo->crArrayFieldList,",");
			strcat(prpArrayInfo->crArrayFieldList,pcpAddFields);
			dbg(TRACE,"FieldList <%s>",prpArrayInfo->crArrayFieldList);
		}/* end of if */
	}/* end of if */

	return(ilRc);
}



/******************************************************************************/
/* The Reset routine                                                          */
/******************************************************************************/
static int Reset()
{
	int	ilRc = RC_SUCCESS;				/* Return code */
	
	dbg(TRACE,"Reset: now resetting");
	ReadConfigEntries ();
	return ilRc;
	
} /* end of Reset */

/******************************************************************************/
/* The termination routine                                                    */
/******************************************************************************/
static void Terminate(int ipSleep)
{
	/* unset SIGCHLD ! DB-Child will terminate ! */

	dbg(TRACE,"Terminate: now DB logoff ...");

	signal(SIGCHLD,SIG_IGN);

	logoff();

	dbg(TRACE,"Terminate: now sleep(%d) ...",ipSleep);


	dbg(TRACE,"Terminate: now leaving ...");

	fclose(outp);

	sleep(min(ipSleep,1));
	
	exit(0);
	
} /* end of Terminate */

/******************************************************************************/
/* The handle signals routine                                                 */
/******************************************************************************/
#if 0
static void HandleSignal(int pipSig)
{
	dbg(TRACE,"HandleSignal: signal <%d> received",pipSig);

	switch(pipSig)
	{
	default	:
		Terminate(1);
		break;
	} /* end of switch */

	exit(1);
	
} /* end of HandleSignal */
#endif

/******************************************************************************/
/* The handle general error routine                                           */
/******************************************************************************/
static void HandleErr(int pipErr)
{
	dbg(TRACE,"HandleErr: <%d> : unknown ERROR",pipErr);

	return;
} /* end of HandleErr */

/******************************************************************************/
/* The handle queuing error routine                                           */
/******************************************************************************/
static void HandleQueErr(int pipErr)
{
	switch(pipErr) {
	case	QUE_E_FUNC	:	/* Unknown function */
		dbg(TRACE,"<%d> : unknown function",pipErr);
		break;
	case	QUE_E_MEMORY	:	/* Malloc reports no memory */
		dbg(TRACE,"<%d> : malloc failed",pipErr);
		break;
	case	QUE_E_SEND	:	/* Error using msgsnd */
		dbg(TRACE,"<%d> : msgsnd failed",pipErr);
		break;
	case	QUE_E_GET	:	/* Error using msgrcv */
		dbg(TRACE,"<%d> : msgrcv failed",pipErr);
		break;
	case	QUE_E_EXISTS	:
		dbg(TRACE,"<%d> : route/queue already exists ",pipErr);
		break;
	case	QUE_E_NOFIND	:
		dbg(TRACE,"<%d> : route not found ",pipErr);
		break;
	case	QUE_E_ACKUNEX	:
		dbg(TRACE,"<%d> : unexpected ack received ",pipErr);
		break;
	case	QUE_E_STATUS	:
		dbg(TRACE,"<%d> :  unknown queue status ",pipErr);
		break;
	case	QUE_E_INACTIVE	:
		dbg(TRACE,"<%d> : queue is inaktive ",pipErr);
		break;
	case	QUE_E_MISACK	:
		dbg(TRACE,"<%d> : missing ack ",pipErr);
		break;
	case	QUE_E_NOQUEUES	:
		dbg(TRACE,"<%d> : queue does not exist",pipErr);
		break;
	case	QUE_E_RESP	:	/* No response on CREATE */
		dbg(TRACE,"<%d> : no response on create",pipErr);
		break;
	case	QUE_E_FULL	:
		dbg(TRACE,"<%d> : too many route destinations",pipErr);
		break;
	case	QUE_E_NOMSG	:	/* No message on queue */
		dbg(TRACE,"<%d> : no messages on queue",pipErr);
		break;
	case	QUE_E_INVORG	:	/* Mod id by que call is 0 */
		dbg(TRACE,"<%d> : invalid originator=0",pipErr);
		break;
	case	QUE_E_NOINIT	:	/* Queues is not initialized*/
		dbg(TRACE,"<%d> : queues are not initialized",pipErr);
		break;
	case	QUE_E_ITOBIG	:
		dbg(TRACE,"<%d> : requestet itemsize to big ",pipErr);
		break;
	case	QUE_E_BUFSIZ	:
		dbg(TRACE,"<%d> : receive buffer to small ",pipErr);
		break;
	default			:	/* Unknown queue error */
		dbg(TRACE,"<%d> : unknown error",pipErr);
		break;
	} /* end switch */
         
	return;

} /* end of HandleQueErr */

/******************************************************************************/
/* The handle queues routine                                                  */
/******************************************************************************/
static void HandleQueues()
{
	int	ilRc = RC_SUCCESS;			/* Return code */
	int	ilBreakOut = FALSE;
	
	do
	{
		ilRc = que(QUE_GETBIG,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);
		/* depending on the size of the received item  */
		/* a realloc could be made by the que function */
		/* so do never forget to set event pointer !!! */
		prgEvent = (EVENT *) prgItem->text;	

		if( ilRc == RC_SUCCESS )
		{
			/* Acknowledge the item */
			ilRc = que(QUE_ACK,0,mod_id,0,0,NULL);
			if( ilRc != RC_SUCCESS ) 
			{
				/* handle que_ack error */
				HandleQueErr(ilRc);
			} /* fi */
		
			switch( prgEvent->command )
			{
			case	HSB_STANDBY	:
				ctrl_sta = prgEvent->command;
				break;	
	
			case	HSB_COMING_UP	:
				ctrl_sta = prgEvent->command;
				break;	
	
			case	HSB_ACTIVE	:
				ctrl_sta = prgEvent->command;
				ilBreakOut = TRUE;
				break;	

			case	HSB_ACT_TO_SBY	:
				ctrl_sta = prgEvent->command;
				break;	
	
			case	HSB_DOWN	:
				/* whole system shutdown - do not further use que(), send_message() or timsch() ! */
				ctrl_sta = prgEvent->command;
				Terminate(1);
				break;	
	
			case	HSB_STANDALONE	:
				ctrl_sta = prgEvent->command;
				ResetDBCounter();
				ilBreakOut = TRUE;
				break;	

			case	REMOTE_DB :
				/* ctrl_sta is checked inside */
				HandleRemoteDB(prgEvent);
				break;

			case	SHUTDOWN	:
				Terminate(1);
				break;
						
			case	RESET		:
				ilRc = Reset();
				break;
						
			case	EVENT_DATA	:
				dbg(TRACE,"HandleQueues: wrong hsb status <%d>",ctrl_sta);
				DebugPrintItem(TRACE,prgItem);
				DebugPrintEvent(TRACE,prgEvent);
				break;
					
			case	TRACE_ON :
				dbg_handle_debug(prgEvent->command);
				break;

			case	TRACE_OFF :
				dbg_handle_debug(prgEvent->command);
				break;

			default			:
				dbg(TRACE,"HandleQueues: unknown event");
				DebugPrintItem(TRACE,prgItem);
				DebugPrintEvent(TRACE,prgEvent);
				break;
			} /* end switch */
		} else {
			/* Handle queuing errors */
			HandleQueErr(ilRc);
		} /* end else */
	} while (ilBreakOut == FALSE);

	if(igInitOK == FALSE)
	{
			ilRc = InitDemhdl();
			if(ilRc != RC_SUCCESS)
			{
				dbg(TRACE,"InitDemhdl: init failed!");
			} /* end of if */
	}/* end of if */

} /* end of HandleQueues */
	

/******************************************************************************/
/******************************************************************************/
static int GetRowLength(char *pcpHopo, char *pcpTana, char *pcpFieldList, long *plpLen)
{
	int	 ilRc = RC_SUCCESS;			/* Return code */
	int  ilNoOfItems = 0;
	int  ilLoop = 0;
	long llFldLen = 0;
	long llRowLen = 0;
	char clFina[8];
	
	ilNoOfItems = get_no_of_items(pcpFieldList);

	ilLoop = 1;
	do
	{
		ilRc = get_real_item(&clFina[0],pcpFieldList,ilLoop);
		if(ilRc > 0)
		{
			ilRc = GetFieldLength(pcpHopo,pcpTana,&clFina[0],&llFldLen);
			if(ilRc == RC_SUCCESS)
			{
				llRowLen++;
				llRowLen += llFldLen;
			}/* end of if */
		}/* end of if */
		ilLoop++;
	}while((ilRc == RC_SUCCESS) && (ilLoop <= ilNoOfItems));

	if(ilRc == RC_SUCCESS)
	{
		*plpLen = llRowLen;
	}/* end of if */

	return(ilRc);
	
} /* end of GetRowLength */


/******************************************************************************/
/******************************************************************************/
static int GetFieldLength(char *pcpHopo, char *pcpTana, char *pcpFina, long *plpLen)
{
	int  ilRc = RC_SUCCESS;			/* Return code */
	int  ilCnt = 0;
	char clTaFi[32];
	char clFele[16];
	char clFldLst[16];

	ilCnt = 1;
	sprintf(&clTaFi[0],"%s,%s",pcpTana,pcpFina);
	sprintf(&clFldLst[0],"TANA,FINA"); /* wird von syslib zerst�rt */

	ilRc = syslibSearchSystabData(&cgTabEnd[0],&clFldLst[0],&clTaFi[0],"FELE",&clFele[0],&ilCnt,"");
	switch (ilRc)
	{
	case RC_SUCCESS :
		*plpLen = atoi(&clFele[0]);
		 dbg(DEBUG,"GetFieldLength: <%s,%s,%s> <%d>",pcpHopo,pcpTana,pcpFina,*plpLen);
		break;

	case RC_NOT_FOUND :
		dbg(TRACE,"GetFieldLength: syslibSSD returned <%d> RC_NOT_FOUND <%s,%s>",
						ilRc,pcpTana,pcpFina);
		break;

	case RC_FAIL :
		dbg(TRACE,"GetFieldLength: syslibSSD returned <%d> RC_FAIL",ilRc);
		break;

	default :
		dbg(TRACE,"GetFieldLength: syslibSSD returned <%d> unexpected",ilRc);
		break;
	}/* end of switch */

	return(ilRc);
	
} /* end of GetFieldLength */


static int SaveEventTimes(char *pcpInbound,char *pcpOutbound)
{
	int	 ilRc = RC_SUCCESS;			/* Return code */
	int ilLen;
	int ilTimeNo = 1;
	int ilItemNo = 1;
	char *pclTimeStr;
	int ilCol,ilPos;

	memset(InboundTimes,0,sizeof(InboundTimes));
	memset(OutboundTimes,0,sizeof(InboundTimes));

	FindItemInList(pcgFieldList,"AFTU",',',&igAftuItemNo,&ilCol,&ilPos);
	FindItemInList(pcgFieldList,"ALC3",',',&igAlc3ItemNo,&ilCol,&ilPos);
	pclTimeStr = INBOUNDTIMES;

	
	dbg(DEBUG,"SaveEventTimes: UrnoItemNo %d \nInbound: <%s>\nOutbound: <%s>",
		igAftuItemNo,pcpInbound,pcpOutbound);
	do
	{
		ilLen = GetNextDataItem(pcgTmpBuf,&pclTimeStr,&cgKomma[0],
							"","\0\0");
		if (ilLen > 0)
		{
			if ( bgUseFIDSTimes || strcmp(pcgTmpBuf, "ETOA") )
			{
				ilRc = FindItemInList(pcgFieldList,pcgTmpBuf,',',&ilItemNo,&ilCol,&ilPos);
				if (ilRc == RC_SUCCESS)
				{
					GetDataItem(InboundTimes[ilTimeNo],pcpInbound,ilItemNo,',',""," \0");
				}
			}
			if (!*InboundTimes[ilTimeNo] && ilTimeNo > 1)
			{
				strcpy(InboundTimes[ilTimeNo],InboundTimes[ilTimeNo-1]);
			}
			dbg(DEBUG,"%s %d <%s>",pcgTmpBuf,ilTimeNo,InboundTimes[ilTimeNo]);
			ilTimeNo++;
		}
	} while (ilLen > 0);


	ilTimeNo = 1;
	pclTimeStr = OUTBOUNDTIMES;
	do
	{
		ilLen = GetNextDataItem(pcgTmpBuf,&pclTimeStr,&cgKomma[0],
							"","\0\0");
		if (ilLen > 0)
		{
			if ( bgUseFIDSTimes || strcmp(pcgTmpBuf, "ETOD") )
			{
				ilRc = FindItemInList(pcgFieldList,pcgTmpBuf,',',&ilItemNo,&ilCol,&ilPos);
				if (ilRc == RC_SUCCESS)
				{
					GetDataItem(OutboundTimes[ilTimeNo],pcpOutbound,ilItemNo,',',""," \0");
				}
			}
			if (!*OutboundTimes[ilTimeNo] && ilTimeNo > 1)
			{
				strcpy(OutboundTimes[ilTimeNo],OutboundTimes[ilTimeNo-1]);
			}
			dbg(DEBUG,"%s %d <%s>",pcgTmpBuf,ilTimeNo,OutboundTimes[ilTimeNo]);
			ilTimeNo++;
		}
	} while (ilLen > 0);
	/*** save registration, sometimes we need it later ***/
	*cgRegn = '\0';
	if (FindItemInList(pcgFieldList,"REGN",',',&igRegnItemNo,&ilCol,&ilPos)
				== RC_SUCCESS)
	{
		GetDataItem(cgRegn,pcpInbound,igRegnItemNo,',',""," \0");
		if (*cgRegn == '\0')
		{
			GetDataItem(cgRegn,pcpOutbound,igRegnItemNo,',',""," \0");
		}
		dbg(DEBUG,"REGN=<%s>",cgRegn);
	}
	else
	{
		dbg(TRACE,"REGN not found in Fieldlist");
	}
	
	return(ilRc);
}


/******************************************************************************/
static int GetPrmEventType(long *plpEventType,char **pcpInbound, char **pcpOutbound,char *pcpData)
{
	int	 ilRc = RC_SUCCESS;			/* Return code */
	char clDel = '\n';
	char *pclTmp1 = NULL;
	int ilCol,ilPos;
	int ilAdidItemNo  =  0;
	char clAdid[12];

#if 0
	pclTmp1 = strchr(pcpData,*cgEtx);
	if (pclTmp1 != NULL)
	{
		*pclTmp1 = '\0';
		*pcpInbound = strdup(pcpData);
		*pcpOutbound = strdup(pclTmp1+1);
		*pclTmp1 = *cgEtx;
	}

	*plpEventType = EVT_UNKNOWN;

	ilRc = FindItemInList(pcgFieldList,"ADID",',',&ilAdidItemNo,&ilCol,&ilPos);
	if (ilRc == RC_SUCCESS)
	{
			ilRc = GetDataItem(clAdid,pcpData,ilAdidItemNo,',',""," \0");
			dbg(DEBUG,"%05d: ADID=<%s>",__LINE__,clAdid);
			if  (ilRc > 0)
			{
					ilRc = RC_SUCCESS;
					if (*clAdid == 'A')
					{
						*plpEventType = EVT_INBOUND;
						*pcpInbound = strdup(pcpData);
						if(*pcpInbound == NULL)
						{
							dbg(TRACE,"GetEventType: strdup - calloc failed");
							ilRc = RC_FAIL;
						}
					} 
					else if (*clAdid == 'D' || *clAdid == 'B')
					{
						*plpEventType = EVT_OUTBOUND;
						*pcpOutbound = strdup(pcpData);
						if(*pcpOutbound == NULL)
						{
							dbg(TRACE,"GetEventType: strdup - calloc failed");
							ilRc = RC_FAIL;
						}
			}
		}
		}
		else
		{
			dbg(TRACE,"ADID not found in FieldList <%s>",pcgFieldList);
		}
#endif

	return ilRc;
	}
						
/******************************************************************************/
static int GetEventType(long *plpEventType,char **pcpInbound, char **pcpOutbound,char *pcpData)
{
	int	 ilRc = RC_SUCCESS;			/* Return code */
	char clDel = '\n';
	char *pclTmp1 = NULL;

	/* clDel for PRM is ETX */
	clDel = *cgEtx;

	if(ilRc == RC_SUCCESS)
	{
		pclTmp1 = strchr(pcpData,clDel);
		if(pclTmp1!= NULL)
		{
			*pclTmp1 = 0x00;
	
			*pcpInbound = strdup(pcpData);
			if(*pcpInbound == NULL)
			{
				dbg(TRACE,"GetEventType: strdup - calloc failed");
				ilRc = RC_FAIL;
			}/* end of if*/

			*pclTmp1 = clDel;
		}
		else
		{
			dbg(TRACE, "GetEventType: strchr <0x%x> not found in <%s>", clDel,
				pcpData);
			ilRc = RC_FAIL;
		}/* end of if*/
	}/* end of if*/

	
	if(ilRc == RC_SUCCESS)
	{
		pclTmp1++;
		*pcpOutbound = strdup(pclTmp1);
		if(*pcpOutbound == NULL)
		{
			dbg(TRACE,"GetEventType: strdup - calloc failed");
			ilRc = RC_FAIL;
		}/* end of if*/
	}/* end of if*/

	if (ilRc == RC_SUCCESS)
	{
		dbg(DEBUG,"GetEventType:  inbound <%x><%s>",pcpInbound,*pcpInbound);
		dbg(DEBUG,"GetEventType: outbound <%x><%s>",pcpOutbound,*pcpOutbound);

		*plpEventType = EVT_UNKNOWN;

		if(strlen(*pcpInbound) == 0)
		{
			if(strlen(*pcpOutbound) == 0)
			{
				dbg(TRACE,"GetEventType: inbound and outbound empty");
				*plpEventType = EVT_UNKNOWN;
				ilRc = RC_FAIL;
			}else{
				*plpEventType = EVT_OUTBOUND;
				ilRc = RC_SUCCESS;
			}/* end of if */
		}else{
			if(strlen(*pcpOutbound) == 0)
			{
				*plpEventType = EVT_INBOUND;
				ilRc = RC_SUCCESS;
			}else{
				*plpEventType = EVT_TURNAROUND;
				ilRc = RC_SUCCESS;
			}/* end of if */
		}/* end of if */
	}

	return(ilRc);
	
} /* end of GetEventType */


/******************************************************************************/
/******************************************************************************/
static int GetCommand(char *pcpCommand, int *pipCmd)
{
	int	ilRc = RC_SUCCESS;			/* Return code */
	int ilLoop = 0;
	char clCommand[48];
	
	memset(&clCommand[0],0x00,8);

	ilRc = get_real_item(&clCommand[0],pcpCommand,1);
	if(ilRc > 0)
	{
		ilRc = RC_FAIL;
	}/* end of if */

	while((ilRc != 0) && (prgCmdTxt[ilLoop] != NULL))
	{
		ilRc = strcmp(&clCommand[0],prgCmdTxt[ilLoop]);
		ilLoop++;
	}/* end of while */

	if(ilRc == 0)
	{
		ilLoop--;
		dbg(DEBUG,"GetCommand: <%s> <%d>",prgCmdTxt[ilLoop],rgCmdDef[ilLoop]);
		*pipCmd = rgCmdDef[ilLoop];
	}else{
		dbg(TRACE,"GetCommand: <%s> is not valid",&clCommand[0]);
		ilRc = RC_FAIL;
	}/* end of if */

	return(ilRc);
	
} /* end of GetCommand */

static int DeletePrmDemands(char *pcpAftUrno)
{
	int		ilRc = RC_FAILURE;
 	short	slCursor;
	short	slSqlFunc;
	char	clSqlBuf[128];
	char	pclDataArea[256];
  char  clTmpBuf[512];
	slSqlFunc = START;
	slCursor = 0;

#if 0
	/***
	if((pcpTemplates != NULL) && (atoi(pcpTemplates) != 0) ) 
	if (*cgPrmTemplates != '\0')
	{

		*clTmpBuf = '\0';
		ilNoOfItems = get_no_of_items(cgPrmTemplates);
		for ( i = 0; i < ilNoOfItems; i++ )
		{
			if (i == 0)
				strcat(clTmpBuf, "'");
			else
				strcat(clTmpBuf, ",'");
			get_real_item(clField, cgPrmTemplates, i+1);
			strcat(clTmpBuf, clField);
			strcat(clTmpBuf, "'");
		}
	}
	sprintf(clSqlBuf,"DELETE FROM DEMTAB WHERE (OURI='%s' OR OURO='%s' AND UTPL IN (%s)",);
	memset(cgDataArea,0,sizeof(cgDataArea));
	sprintf(clSqlBuf,"DELETE FROM DEMTAB WHERE (OURI='%s' OR OURO='%s'", pcpAftUrno,pcpAftUrno);
	ilRc = sql_if(slSqlFunc,&slCursor,clSqlBuf,cgDataArea);
	if(ilRc != RC_SUCCESS && ilRc != NOTFOUND)
	{
	   get_ora_err(ilRc, cgErrMsg);
	   dbg(TRACE,"DeletePrmDemands failed RC=%d <%s>",ilRc,cgErrMsg);
	   dbg(TRACE,"SQL: <%s>",clSqlBuf);
	 }
	else if (ilRc == NOTFOUND) 
	{
			dbg(TRACE,"No demands found for AFT-URNO <%s>",pcpAftUrno);
	}

	close_my_cursor(&slCursor);

	dbg(DEBUG,"select from jodtab with RC = %d",ilRc);
	***/



	llOldDemRowNum = ARR_FIRST;
	while(CEDAArrayGetRowPointer(&rgOldDemArray.rrArrayHandle,
								 rgOldDemArray.crArrayName,llOldDemRowNum,
								 (void *)&rgOldDemArray.pcrArrayRowBuf) == RC_SUCCESS)
	{

			ilRc = CEDAArrayDeleteRow(&rgOldDemArray.rrArrayHandle,
							rgOldDemArray.crArrayName,llRowNum);
					if (ilRc != RC_SUCCESSS)
					{
						dbg ( TRACE, "DeleteOldDemands: CEDAArrayDeleteRow failed RC <%d> URNO <%s> URUD <%s>",
						  ilRc, clUrno, clUrud );
					}



#endif									



	return ilRc;
}
/******************************************************************************/
/* The handle data routine                                                    */
/******************************************************************************/
static int HandleData(EVENT *prpEvent)
{
	int	      ilRc           = RC_SUCCESS;			/* Return code */
	int       ilCmd          = 0;

	BC_HEAD *prlBchead       = NULL;
	CMDBLK  *prlCmdblk       = NULL;
	char    *pclSelection    = NULL;
	char    *pclFields       = NULL;
	char    *pclData         = NULL;
	char    *pclRueUrnos     = NULL;
	char    *pclTmp = NULL;
	char	clSelection[512], clUrno[21];
	int		ilSBCMode = SINGLE_TO_BCHDL |SINGLE_TO_ACTION;
	int		ilLen, ilOldDebugLevel;
	BOOL    blDoBc = TRUE;
	char    *pclLine;
	char    clAftu[24];
	BOOL    blFirstRead = TRUE;

	prlBchead    = (BC_HEAD *) ((char *)prpEvent + sizeof(EVENT));
	prlCmdblk    = (CMDBLK *)  ((char *)prlBchead->data);
	pclSelection = prlCmdblk->data;
	pclFields    = pclSelection + strlen(pclSelection) + 1;
	pclData      = pclFields + strlen(pclFields) + 1;
	pclRueUrnos  = pclData + strlen(pclData) + 1;

	pcgFieldList = pclFields;

	dbg(TRACE,"========== START <%10.10d> ==========",lgEvtCnt);
	if ( prgItem )
		dbg (TRACE, "item->priority <%d>", prgItem->priority ) ;

	/****************************************/
	if ( debug_level < DEBUG )
		dbg (TRACE, "bchead->orig_name  (%s)", prlBchead->orig_name );
	else
		DebugPrintBchead(DEBUG,prlBchead);
	DebugPrintCmdblk(TRACE,prlCmdblk);
	dbg(TRACE,"selection <%s>",pclSelection);
	dbg(TRACE,"fields    <%s>",pclFields);
	dbg(TRACE,"data      <%s>",pclData);
	/****************************************/

	bgShowEvent = FALSE;
	CheckPerformance (TRUE, prlBchead->orig_name);

	ilRc = GetCommand(&prlCmdblk->command[0],&ilCmd);
	if(ilRc == RC_SUCCESS)
	{
		CEDAArraySendChanges2BCHDL( &rgNewDemArray.rrArrayHandle,
									rgNewDemArray.crArrayName, TRUE);
		CEDAArraySendChanges2BCHDL( &rgOldDemArray.rrArrayHandle,
									rgOldDemArray.crArrayName,TRUE);
		CEDAArraySendChanges2ACTION( &rgNewDemArray.rrArrayHandle,
									 rgNewDemArray.crArrayName, TRUE);
		CEDAArraySendChanges2ACTION( &rgOldDemArray.rrArrayHandle,
									 rgOldDemArray.crArrayName,TRUE);
		if (strstr(prlCmdblk->tw_start,".NBC.") != NULL)
		{
			dbg(DEBUG,"SILENT TRANSACTION (NO BROADCASTS)");
			ilSBCMode = NO_BC; /* no BC at all */
		}

		cgMinDebe[0] = '\0';
		cgMaxDeen[0] = '\0';
		pcgTemplates[0] = '\0';

	if (ilCmd == CMD_UFR)
	   ilCmd = CMD_PRM;

		switch (ilCmd)
		{
			case CMD_IFR :
			case CMD_UFR :

				if ( pclTmp = strchr(pclSelection,'|') )
				{	/* hag20020529: remove possible flag from rule diagnosis */
					*pclTmp = '\0';
				}

				ilRc = GetRueUrnos(pclRueUrnos);

				ilRc = GetEventType((long *)&lgEventType, &pcgInbound, &pcgOutbound, pclData);
				if(ilRc == RC_SUCCESS)
				{
					char pclOURI[24] = "";
					char pclOURO[24] = "";

					SaveEventTimes(pcgInbound,pcgOutbound);
					ReadRudTab();
					if ( pcgInbound )
						GetDataItem(pclOURI,pcgInbound,igAftuItemNo,',',""," \0");
					if (pcgOutbound)
							GetDataItem(pclOURO,pcgOutbound,igAftuItemNo,',',""," \0");
					if (!strcmp(prlBchead->dest_name, "FLICOL"))
					{
						ReadOldDemands(pclOURI,pclOURO,pclSelection);
						if ( bgSendSBC )
						{	/* Event from Coverage and CFG-Flag set -> one BC for all demands */
							if ( ilSBCMode != NO_BC )
								ilSBCMode = TCP_SBC|SINGLE_TO_ACTION;	/* old UDP-SBC (RELDEM) at the end of calc., TCP-SBC (UPDDEM) after each flight */
						}
					}
					else
					{
						ReadOldDemands(pclOURI,pclOURO,NULL);
						if ( pcgInbound )
							blDoBc = ToBeBroadcasted ( InboundTimes[1] );
						else
							blDoBc = ToBeBroadcasted ( OutboundTimes[1] );
							
						if ( !blDoBc )		
						{
							dbg ( TRACE, "HandleData: Flight outside the configured time frmame for broadcasts -> NO BC!" );
							ilSBCMode = NO_BC;
						}
						else if ( bgSendUpdDem )
						{	/* Event from Coverage and CFG-Flag set -> one BC for all demands */
							if ( ilSBCMode != NO_BC )
								ilSBCMode = TCP_SBC | UDP_SBC |SINGLE_TO_ACTION;	/* UDPDEM as UDP-SBC and TCP-SBC after each flight */
						}				
					}
					if(strstr(pclSelection,"RUTY=3") == NULL)
					{
						CreateDemands();
					}
					CompareDemands();
					DeleteOldDemands();
					SaveDemands(ilSBCMode);
					SaveFisu();
				}
				else
				{
					dbg(TRACE,"HandleData: GetEventType failed <%d>",ilRc);
				}/* end of if */
				
				break; /* CMD_UFR */

			case CMD_PRD:
		
				CEDAArrayDelete(&rgNewDemArray.rrArrayHandle,
					rgNewDemArray.crArrayName);
				dbg(DEBUG,"old new demand-array deleted ");
				dbg(DEBUG,"CMD_PRD Selection:<%s>",pclSelection);
				GetUrnoFromSelection(pclSelection,clAftu);
				ReadOldPrmDemands2(clAftu,clAftu,cgPrmTemplates);
				CompareDemands();
				DeleteOldDemands();
				SaveDemands(ilSBCMode);
				break;


			case CMD_PRM:
				if ( pclTmp = strchr(pclSelection,'|') )
				{	/* hag20020529: remove possible flag from rule diagnosis */
					*pclTmp = '\0';
				}
			
				CEDAArrayDelete(&rgNewDemArray.rrArrayHandle,
					rgNewDemArray.crArrayName);
				dbg(DEBUG,"old new demand-array deleted ");
				GetUrnoFromSelection(pclSelection,clAftu);
				dbg(TRACE,"DATA: <%s>",pclData);

				pclLine =  strtok(pclData,"\n");
				if (pclLine != NULL)
				{
				  ReadOldPrmDemands2(clAftu,clAftu,cgPrmTemplates);
					blFirstRead = FALSE;
					do
					{
						dbg(DEBUG,"PRM-Line: <%s>",pclLine);
						strcpy(cgLineBuf,pclLine);
						pclRueUrnos = strstr(cgLineBuf,cgDc1);
						if (pclRueUrnos != NULL)
						{
							*pclRueUrnos = '\0';
							pclRueUrnos++;
							dbg(DEBUG,"%05d:RueUrnos found: <%s>",__LINE__,pclRueUrnos);
							ilRc = GetPrmRueUrnos(pclRueUrnos);
						}
						dbg(DEBUG,"%05d:RueUrnos: <%s>",__LINE__,pclRueUrnos);
						/*ilRc = GetPrmEventType((long *)&lgEventType, &pcgInbound, &pcgOutbound, cgLineBuf);*/
						ilRc = GetEventType((long *)&lgEventType, &pcgInbound, &pcgOutbound, cgLineBuf);
						if(ilRc == RC_SUCCESS)
						{
							char pclOURI[24] = "";
							char pclOURO[24] = "";
							char pclUPRM[24] = "";
							int ilCol = 0;
							int ilPos = 0;

							SaveEventTimes(pcgInbound,pcgOutbound);
							/* for  PRM use FLNU instead of URNO */
							FindItemInList(pcgFieldList,"AFTU",',',&igAftuItemNo,&ilCol,&ilPos);
							/*FindItemInList(pcgFieldList,"AFTU",',',&igAftuItemNo,&ilCol,&ilPos);*/
							FindItemInList(pcgFieldList,"URNO",',',&igUprmItemNo,&ilCol,&ilPos);
							GetDataItem(pclUPRM,cgLineBuf,igUprmItemNo,',',""," \0");
							dbg(TRACE,"Fields: <%s>  igUprmItemNo %d clUPRM <%s>",
								pcgFieldList,igUprmItemNo,pclUPRM);
							ReadRudTab();
							if (lgEventType == EVT_INBOUND)
							{
								GetDataItem(pclOURI,pcgInbound,igAftuItemNo,',',""," \0");
								dbg(DEBUG,"INBOUND, OURI=<%s>",pclOURI);
							}
							if (lgEventType == EVT_OUTBOUND)
							{
								GetDataItem(pclOURO,pcgOutbound,igAftuItemNo,',',""," \0");
								dbg(DEBUG,"OUTBOUND, OURO=<%s>",pclOURO);
							}
						
							ReadOtherOldPrmDemands(clAftu,pclUPRM,cgPrmTemplates,blFirstRead);
							blFirstRead = FALSE;
							
							if(strstr(pclSelection,"RUTY=3") == NULL)
							{
								CreatePrmDemands(pclUPRM);
							}
							}

						pclLine = strtok(NULL,"\n");
					} while(pclLine != NULL);

							CompareDemands();
							{
								long llNewDemandCount;
								long llOldDemandCount;
	CEDAArrayGetRowCount(&rgNewDemArray.rrArrayHandle,rgNewDemArray.crArrayName,&llNewDemandCount);
	CEDAArrayGetRowCount(&rgOldDemArray.rrArrayHandle,rgOldDemArray.crArrayName,&llOldDemandCount);
		dbg(TRACE,"After CreateDemands: new demands <%ld>, old demands <%ld> ", llNewDemandCount,llOldDemandCount);
							}
							DeleteOldDemands();
							{
								long llNewDemandCount;
								long llOldDemandCount;
	CEDAArrayGetRowCount(&rgNewDemArray.rrArrayHandle,rgNewDemArray.crArrayName,&llNewDemandCount);
	CEDAArrayGetRowCount(&rgOldDemArray.rrArrayHandle,rgOldDemArray.crArrayName,&llOldDemandCount);
		dbg(TRACE,"After DeleteOldDemands: new demands <%ld>, old demands <%ld> ", llNewDemandCount,llOldDemandCount);
							}
							SaveDemands(ilSBCMode);
							SaveFisu();

				}
				
				break; /* CMD_PRM */

			case CMD_DFR :
				if ( pclTmp = strchr(pclSelection,'|') )
				{	/* hag20020529: remove possible flag from rule diagnosis */
					*pclTmp = '\0';
				}
				if ( bgSendUpdDem )
				{	/* Event from Coverage and CFG-Flag set -> one BC for all demands */
					if ( ilSBCMode != NO_BC )
						ilSBCMode = TCP_SBC | UDP_SBC |SINGLE_TO_ACTION;	/* UDPDEM as UDP-SBC and TCP-SBC after each flight */
				}				
				ReadOldDemands(pclSelection,pclSelection,NULL);
				CEDAArrayDelete(&rgNewDemArray.rrArrayHandle,
								rgNewDemArray.crArrayName);
				CompareDemands();
				DeleteOldDemands();
				blDoBc = ToBeBroadcasted ( cgMinDebe );
				if ( !blDoBc )		
				{
					dbg ( TRACE, "HandleData: Flight outside the configured time frmame for broadcasts -> NO BC!" );
					ilSBCMode = NO_BC;
				}
	
				SaveDemands(ilSBCMode);
				break; /* CMD_DFR */

			case CMD_FIR :
				dbg (DEBUG,"Create flight independent demands") ;
		       	/*dbg(TRACE, "%05d HandleData: Selection = <%s> ", __LINE__,
					pclSelection);*/
				if ( bgSendSBC )
				{	/* Event from Coverage and CFG-Flag set -> one BC for all demands */
					ilSBCMode = TCP_SBC | SINGLE_TO_ACTION;	
				}
				else
					ilSBCMode = SINGLE_TO_ACTION;	

				strcpy(pcgTmpBuf, pclSelection);
				pclTmp = strchr(pcgTmpBuf,'|');
				if(pclTmp != NULL)
				{
					ilLen = strlen ( pclTmp+1 ) +1;
					if ( ilLen > igLenofTemplates )
					{
						if ( pcgTemplates = realloc ( pcgTemplates, ilLen ) )
						{
							igLenofTemplates = ilLen;
							dbg ( DEBUG, "HandleData: Reallocated <%d> Bytes for Templatebuffer", ilLen );
							strcpy(pcgTemplates, pclTmp+1);
						}
						else
						{
							dbg ( TRACE, "HandleData: Unable to reallocate <%d> Bytes for Templatebuffer", ilLen );
							strcpy(pcgTemplates, "\0");
						}
					} 
					else
						strcpy(pcgTemplates, pclTmp+1);
					*pclTmp = '\0';
				}
				else
					strcpy(pcgTemplates, "\0");
				strcpy ( clSelection, pcgTmpBuf );
                if (*clSelection != '\0')
                {
           	    	GetRueUrnos(pclRueUrnos);
					ReadRudTab();
					ReadOldFid(clSelection, pcgTemplates);
					CreateFid(clSelection);
					CompareDemands();
					DeleteOldDemands();
					SaveDemands(ilSBCMode);
					SaveFisu();
				}
                else 
                { 
					/* save selected time frame and templates in clTemplates */
			        strcpy(prlCmdblk->command,"CFL");   
          	       	if (SendToQue(1900,PRIORITY_4,prlBchead, prlCmdblk,
						pclSelection,pclFields,pclData) != RC_SUCCESS)
			 		{
						dbg(TRACE,"%05d SendToQue (BCHDL) failed", __LINE__);
				 	}
					dbg ( DEBUG, "HandleData: CMD_FIR pclData <%s>, bgSendSBC <%d>", pclData, bgSendSBC );
					if ( bgSendSBC )
					{	/* demand creation finished and CFG-Flag set -> send one BC for all demands */
						char	*pclStart=0, *pclTpl=0, *pclEnd=0;

						pclStart = pclData;
						pclEnd = strstr ( pclData, "-" );
						if ( pclEnd  ) 
							pclEnd ++ ;
						pclTpl = strchr ( pclData, '|' );
						
						if ( pclStart && pclEnd && pclTpl )
							DoSendSBC ( pclStart, pclEnd, pclTpl, "7", prlBchead->recv_name );
					}
                }
				break;
			case CMD_CFL :
				if (SendToQue(1900,PRIORITY_4,prlBchead, prlCmdblk,
						pclSelection,pclFields,pclData) != RC_SUCCESS)
				{
					dbg(TRACE,"%05d SendToQue (BCHDL) failed", __LINE__);
				}
				if ( bgSendSBC )
				{	/* demand creation finished and CFG-Flag set -> send one BC for all demands */
					char	*pclStart=0, *pclTpl=0, *pclEnd=0;

					pclStart = strstr ( pclSelection, ">=" );
					if ( pclStart )
						pclStart = strchr ( pclStart, '\'' );
					pclEnd = strstr ( pclSelection, "<=" );
					if ( pclEnd  ) 
						pclEnd = strchr ( pclEnd, '\'' );
					pclTpl = strchr ( pclSelection, '|' );
					if ( pclStart && pclEnd && pclTpl )
						DoSendSBC ( pclStart+1, pclEnd+1, pclTpl, "0,1,2,6", prlBchead->recv_name );
				}
				break;
			case CMD_UDR :
				UpdateDemandRecord(pclFields,pclData);
				SendAnswer(prpEvent,RC_SUCCESS);
				break;
			case CMD_IDR :
				HandleIDR(pclFields, pclData);
				SendAnswer(prpEvent,RC_SUCCESS);
				break;
			case CMD_UCD:
				CCIDemands(pclData);
				break;
			case CMD_DRT:
				if(strcmp(prlCmdblk->obj_name,"JODTAB") == 0)
				{
					if ( bgDelInvalidDemands && bgUseJODTAB )
						HandleJobDelete ( pclFields, pclData );
					break;
				}
				if(strcmp(prlCmdblk->obj_name,"JOBTAB") == 0)
				{
					if ( bgDelInvalidDemands && !bgUseJODTAB )
						HandleJobDelete ( pclFields, pclData );
					break;
				}
			case CMD_IRT:
			case CMD_URT:
				ilRc = CEDAArrayEventUpdate( prgEvent ); 
				if( (ilRc==RC_SUCCESS) && (ilCmd!=CMD_DRT) )
				{	
					clUrno[0] = '\0';
					if ( strcmp(prlCmdblk->obj_name,"RUDTAB") == 0) 
						ilRc = tool_get_field_data (pclFields, pclData, "URNO", clUrno );
					else
						if ( !strcmp(prlCmdblk->obj_name,"RPFTAB") ||
							 !strcmp(prlCmdblk->obj_name,"RPQTAB") ||
							 !strcmp(prlCmdblk->obj_name,"REQTAB") ||
							 !strcmp(prlCmdblk->obj_name,"RLOTAB") )
							ilRc = tool_get_field_data (pclFields, pclData, "URUD", clUrno );
					if ( (ilRc == RC_SUCCESS) && !IS_EMPTY (clUrno) )
						ilRc = SetRudLogicalFields ( &rgCCIRudArray, clUrno );
					else
						dbg ( TRACE, "HandleData (IRT/URT): Could not find RUD-URNO in event" );
				}
				dbg(DEBUG, "command IRT, URT or DRT processed, table<%s> ilRC <%ld>", 
					prlCmdblk->obj_name, ilRc);
				break;
			case CMD_ANSWER:
				break;
			case CMD_DLF:
				/* Get Data Load Format */				
				cgCfgBuffer[0] = '\0';
				iGetConfigRow ( cgConfigFile, "MAIN", "LoadFormat", CFG_STRING, cgCfgBuffer );
          	    ilRc = SendToQue(prgEvent->originator,PRIORITY_4,prlBchead, 
								 prlCmdblk, "","",cgCfgBuffer) ;
			 	dbg(TRACE,"HandleData: CMD <DLF> SendToQue <%d> Data <%s> RC <%d>", prgEvent->originator,
					       cgCfgBuffer, ilRc );
				break;

			default:
				dbg(TRACE, "HandleData: unknown command <%s>",
					&prlCmdblk->command[0]);
				/*
				DebugPrintBchead(TRACE,prlBchead);
				DebugPrintCmdblk(TRACE,prlCmdblk);
				dbg(TRACE,"selection <%s>",pclSelection);
				dbg(TRACE,"fields    <%s>",pclFields);
				dbg(TRACE,"data      <%s>",pclData);*/
				SendAnswer(prpEvent,RC_FAIL);
				break; /* default */

		}/* end of switch */
	}else{
		dbg(TRACE,"HandleData: GetCommand failed");
		SendAnswer(prpEvent,RC_FAIL);
		/*
		DebugPrintBchead(TRACE,prlBchead);
		DebugPrintCmdblk(TRACE,prlCmdblk);
		dbg(TRACE,"selection <%s>",pclSelection);
		dbg(TRACE,"fields    <%s>",pclFields);
		dbg(TRACE,"data      <%s>",pclData);*/
	}/* end of if */

	if(pcgInbound != NULL)
	{
		free(pcgInbound);
		pcgInbound = NULL;
	}/* end of if */

	if(pcgOutbound != NULL)
	{
		free(pcgOutbound);
		pcgOutbound = NULL;
	}/* end of if */

	CheckPerformance (FALSE, "\0");
	ilOldDebugLevel = debug_level;
	if (bgShowEvent == TRUE)
	{
		debug_level = TRACE;
		dbg (TRACE, "item->priority <%d>", prgItem->priority ) ;
		dbg (TRACE, "bchead->orig_name  (%s)", prlBchead->orig_name );
		DebugPrintCmdblk(TRACE,prlCmdblk);
		dbg(TRACE,"selection <%s>",pclSelection);
		dbg(TRACE,"fields    <%s>",pclFields);
		dbg(TRACE,"data      <%s>",pclData);
	}                             /* end if */

	dbg(TRACE,"==========  END  <%10.10d> ==========",lgEvtCnt);
	debug_level = ilOldDebugLevel ;

	return(RC_SUCCESS);
	
} /* end of HandleData */

static int GetPrmRueUrnos(char *pcpRueUrnos)
{

	int ilRc = RC_SUCCESS;
	int       ilLC	         = 0; /* Loop Count for inner loop */
	int       ilTemplateNo   = 0;
	int ilRueLen;
	int ilIdx = 0;  /* actual index in prgRueUrnos */

	/* initialize RUE-Urnos */
	memset(prgRueUrnos,0,sizeof(prgRueUrnos));

	dbg(TRACE,"RueUrnos: <%s>",pcpRueUrnos);
	do
	{
		ilRueLen = GetNextDataItem(pcgTmpBuf,
					&pcpRueUrnos,&cgStx[0],"","\0\0");
		if (ilRueLen  > 0)
		{	
			char *pclTmpBuf = pcgTmpBuf;
			for (ilLC = IDX_INBOUND; ilLC <= IDX_OUTBOUND; ilLC++)
			{
				/* we've got RUE-Urnos for one template */
				char pclLocalBuf[128];
				int ilDL;
					
				{
					ilDL=GetNextDataItem(pclLocalBuf,&pclTmpBuf,
								&cgSoh[0],"","  \0\0");
					if (ilDL >= 1 )
					{
						prgRueUrnos[ilIdx].IsUsed = TRUE;
					
						strcpy(prgRueUrnos[ilIdx].Urno, pclLocalBuf);
						ilDL = GetNextDataItem(pclLocalBuf,
										&pclTmpBuf,&cgSoh[0],""," \0\0");
					
						strcpy(prgRueUrnos[ilIdx].Fisu,pclLocalBuf);
						ilIdx++;
					}
				}
			}
			ilTemplateNo++;
		}
	} while (ilRueLen > 0);

	for( ilTemplateNo = 0; ilTemplateNo < MAXRUES && 
							prgRueUrnos[ilTemplateNo].IsUsed ; ilTemplateNo++)
	{
		dbg(DEBUG,"%d. Rule URNO <%s> FISU <%s>", ilTemplateNo+1, 
			prgRueUrnos[ilTemplateNo].Urno, prgRueUrnos[ilTemplateNo].Fisu );
	}

return ilRc;
}


static int GetRueUrnos(char *pcpRueUrnos)
{

	int ilRc = RC_SUCCESS;
	int       ilLC	         = 0; /* Loop Count for inner loop */
	int       ilTemplateNo   = 0;
	int ilRueLen;
	int ilIdx = 0;  /* actual index in prgRueUrnos */

	/* initialize RUE-Urnos */
	memset(prgRueUrnos,0,sizeof(prgRueUrnos));

	dbg(TRACE,"RueUrnos: <%s>",pcpRueUrnos);
	do
	{
		ilRueLen = GetNextDataItem(pcgTmpBuf,
					&pcpRueUrnos,&cgStx[0],"","\0\0");
		if (ilRueLen  > 0)
		{	
			char *pclTmpBuf = pcgTmpBuf;
			for (ilLC = IDX_INBOUND; ilLC <= IDX_OUTBOUND; ilLC++)
			{
				/* we've got RUE-Urnos for one template */
				char pclLocalBuf[128];
				int ilDL;
					

				{
					ilDL=GetNextDataItem(pclLocalBuf,&pclTmpBuf,
								&cgSoh[0],"","  \0\0");
					if (ilDL >= 1 )
					{
						prgRueUrnos[ilIdx].IsUsed = TRUE;
					
						strcpy(prgRueUrnos[ilIdx].Urno, pclLocalBuf);
						ilDL = GetNextDataItem(pclLocalBuf,
										&pclTmpBuf,&cgSoh[0],""," \0\0");
					
						strcpy(prgRueUrnos[ilIdx].Fisu,pclLocalBuf);
						ilIdx++;
					}
				}
			}
			ilTemplateNo++;
		}
	} while (ilRueLen > 0);

	for( ilTemplateNo = 0; ilTemplateNo < MAXRUES && 
							prgRueUrnos[ilTemplateNo].IsUsed ; ilTemplateNo++)
	{
		dbg(DEBUG,"%d. Rule URNO <%s> FISU <%s>", ilTemplateNo+1, 
			prgRueUrnos[ilTemplateNo].Urno, prgRueUrnos[ilTemplateNo].Fisu );
	}

return ilRc;
}


static int ReadOldFid(char *pcpDate, char *pcpTemplates)
{
	int		ilRc = RC_SUCCESS;
	int		ilNoOfItems = 0;
	int		i;
	char	pclDate[24], clHint[41];
	char	pclAddData[256];
	char	clField[URNOLEN+1];
	long llAction = ARR_FIRST;

	strcpy(pclAddData,"");
	strcpy(pclDate,pcpDate);
	AddSecondsToCEDATime(pclDate, 86400, 1);
 
	if (pcpTemplates != NULL)
		dbg(DEBUG, "ReadOldFid Templates: <%s>", pcpTemplates);

	/*   *clRudBuf = '\0';*//*	 cgDataArea instead of clRudBuf */
	*cgDataArea = '\0';
	clHint[0] = '\0';

	if((pcpTemplates != NULL) && (*pcpTemplates != '\0'))
	{
		*pcgTmpBuf = '\0';	/* has been clTemplates before */
		ilNoOfItems = get_no_of_items(pcpTemplates);
		for ( i = 0; i < ilNoOfItems; i++ )
		{
			if (i == 0)
				strcat(pcgTmpBuf, "'");
			else
				strcat(pcgTmpBuf, ",'");
			get_real_item(clField, pcpTemplates, i+1);
			strcat(pcgTmpBuf, clField);
			strcat(pcgTmpBuf, "'");
		}
		if ( igLoadSel <= 2 ) 
			sprintf( cgDataArea, cgTplLoadExt[igLoadSel], pcgTmpBuf);
		if ( igLoadSel < 2 )
			strcpy ( clHint, "/*+ INDEX(RUDTAB RUDTAB_UTPL) */" );
	}

	*pcgTmpBuf = '\0';	/* pcgTmpBuf instead of pclSelection */

	sprintf(pcgTmpBuf,
			"WHERE DETY IN (' ','7') AND (DEBE BETWEEN '%s' AND '%s')", pcpDate,
			pclDate);

	if (*cgDataArea != '\0')
	{
		strcat(pcgTmpBuf,cgDataArea);
	}

	dbg(DEBUG, "ReadOldFid: Selection %s", pcgTmpBuf);
	CEDAArrayRefillHint (&rgOldDemArray.rrArrayHandle,rgOldDemArray.crArrayName,
					      pcgTmpBuf,pclAddData,llAction, clHint );

	dbg(DEBUG,"ReadOldFid RC=%d",ilRc);
	return ilRc;
}

static int ReadOtherOldPrmDemands(char *pcpAFTU, char *pcpUPRM, char *pcpTemplates,BOOL bpFirstRead)
{
	int ilRc = RC_SUCCESS;
	/* char pclSelection[1024]; buffers may be too small  */
	char pclTmpBuf[1024];
	/*  char pclRudBuf[512];  buffers may be too small  */
	char pclWhere[512];
	char pclAddData[256];
	long llAction = ARR_FIRST;
	int		ilNoOfItems = 0;
	char	clField[URNOLEN+1];
	/*  char	clTemplates[512]; buffers may be too small  */
	int		i;

	if (bpFirstRead)
	{
		llAction = ARR_FIRST;
	}
	else
	{
		llAction = ARR_NEXT;
	}
	if (pcpTemplates != NULL)
		dbg(DEBUG, "ReadOtherOldDemands Templates: <%s>, UPRM: <%s>, AFTU: <%s>", pcpTemplates, pcpUPRM, pcpAFTU);

	*cgDataArea = '\0';

	/* if((pcpTemplates != NULL) && (*pcpTemplates != '\0')) */
	if((pcpTemplates != NULL) && (atoi(pcpTemplates) != 0) ) 
	{
		*pcgTmpBuf = '\0';
		ilNoOfItems = get_no_of_items(pcpTemplates);
		for ( i = 0; i < ilNoOfItems; i++ )
		{
			if (i == 0)
				strcat(pcgTmpBuf, "'");
			else
				strcat(pcgTmpBuf, ",'");
			get_real_item(clField, pcpTemplates, i+1);
			strcat(pcgTmpBuf, clField);
			strcat(pcgTmpBuf, "'");
		}
		if ( igLoadSel <= 2 ) 
			sprintf(cgDataArea, cgTplLoadExt[igLoadSel], pcgTmpBuf);
	}

	dbg(DEBUG, "ReadOtherOldDemands DataArea: <%s>", cgDataArea);
	*pcgTmpBuf = '\0';

	strcpy(pclWhere,"WHERE (DETY = '0' AND (");

	if(*pcpAFTU != '\0')
	{
		sprintf(pcgTmpBuf,"%s OURI!='%s' AND UPRM = '%s' ",pclWhere,pcpAFTU,pcpUPRM);
		strcpy(pclWhere," AND ");
	}
	if(*pcpAFTU != '\0')
	{
		sprintf(pclTmpBuf,"%s OURO!='%s' AND UPRM = '%s' ",pclWhere,pcpAFTU,pcpUPRM);
		strcat(pcgTmpBuf,pclTmpBuf);
	}
	strcat( pcgTmpBuf, "))" );

	if (*cgDataArea != '\0')
	{
		strcat(pcgTmpBuf,cgDataArea);
	}

	dbg(DEBUG,"ReadOtherOldPRMDemands Selection: <%s>",pcgTmpBuf);

	if (*pcgTmpBuf)
	{
		CEDAArrayRefill(&rgOldDemArray.rrArrayHandle,
						rgOldDemArray.crArrayName, pcgTmpBuf, pclAddData,
						llAction);
		llAction = ARR_NEXT;
	}

	if(*pcpAFTU != '\0')
	{
		*pcgTmpBuf = '\0';

		strcpy(pclWhere,"WHERE (DETY = '1' AND");

		sprintf(pcgTmpBuf,"%s OURI!='%s' AND UPRM = '%s' ",pclWhere,pcpAFTU,pcpUPRM);
		strcat(pcgTmpBuf, ")");

		if (*cgDataArea != '\0')
		{
			strcat(pcgTmpBuf,cgDataArea);
		}

		dbg(DEBUG,"ReadOtherOldPRMDemands Selection: <%s>",pcgTmpBuf);

		if (*pcgTmpBuf)
		{
			CEDAArrayRefill(&rgOldDemArray.rrArrayHandle,
							rgOldDemArray.crArrayName,
							pcgTmpBuf,pclAddData,llAction);
			llAction = ARR_NEXT;
		}
	}
	if(*pcpAFTU != '\0')
	{
		*pcgTmpBuf = '\0';

		strcpy(pclWhere,"WHERE (DETY = '2' AND");

		sprintf(pcgTmpBuf,"%s OURO!='%s' AND UPRM = '%s' ",pclWhere,pcpAFTU,pcpUPRM);
		strcat(pcgTmpBuf, ")");

		if (*cgDataArea != '\0')
		{
			strcat(pcgTmpBuf,cgDataArea);
		}

		dbg(DEBUG,"ReadOtherOldPRMDemands Selection: <%s>",pcgTmpBuf);
	
		if (*pcgTmpBuf)
		{
			CEDAArrayRefill(&rgOldDemArray.rrArrayHandle,
							rgOldDemArray.crArrayName,
							pcgTmpBuf,pclAddData,llAction);
			llAction = ARR_NEXT;
		}
	}
	dbg(DEBUG,"ReadOtherOldPRMDemands RC=%d",ilRc);
	return ilRc;
}

static int ReadOldPrmDemands2(char *pcpOURI,char *pcpOURO, char *pcpTemplates)
{
	int ilRc = RC_SUCCESS;
	/* char pclSelection[1024]; buffers may be too small  */
	char pclTmpBuf[1024];
	/*  char pclRudBuf[512];  buffers may be too small  */
	char pclWhere[512];
	char pclAddData[256];
	long llAction = ARR_FIRST;
	int		ilNoOfItems = 0;
	char	clField[URNOLEN+1];
	/*  char	clTemplates[512]; buffers may be too small  */
	int		i;

	if (pcpTemplates != NULL)
		dbg(DEBUG, "ReadOldPrmDemands2 Templates: <%s>", pcpTemplates);

	*cgDataArea = '\0';

	/* if((pcpTemplates != NULL) && (*pcpTemplates != '\0')) */
	if((pcpTemplates != NULL) && (atoi(pcpTemplates) != 0) ) 
	{
		*pcgTmpBuf = '\0';
		ilNoOfItems = get_no_of_items(pcpTemplates);
		for ( i = 0; i < ilNoOfItems; i++ )
		{
			if (i == 0)
				strcat(pcgTmpBuf, "'");
			else
				strcat(pcgTmpBuf, ",'");
			get_real_item(clField, pcpTemplates, i+1);
			strcat(pcgTmpBuf, clField);
			strcat(pcgTmpBuf, "'");
		}
		if ( igLoadSel <= 2 ) 
			sprintf(cgDataArea, cgTplLoadExt[igLoadSel], pcgTmpBuf);
	}

	*pcgTmpBuf = '\0';

	strcpy(pclWhere,"WHERE (DETY = '0' AND (");

	if(*pcpOURI != '\0')
	{
		sprintf(pcgTmpBuf,"%s OURI='%s' AND UPRM != '0' ",pclWhere,pcpOURI);
		strcpy(pclWhere," OR ");
	}
	if(*pcpOURO != '\0')
	{
		sprintf(pclTmpBuf,"%s OURO='%s' AND UPRM != '0' ",pclWhere,pcpOURO);
		strcat(pcgTmpBuf,pclTmpBuf);
	}
	strcat( pcgTmpBuf, "))" );

	if (*cgDataArea != '\0')
	{
		strcat(pcgTmpBuf,cgDataArea);
	}

	dbg(DEBUG,"ReadOldPrmDemands2 Selection: <%s>",pcgTmpBuf);

	if (*pcgTmpBuf)
	{
		CEDAArrayRefill(&rgOldDemArray.rrArrayHandle,
						rgOldDemArray.crArrayName, pcgTmpBuf, pclAddData,
						llAction);
		llAction = ARR_NEXT;
	}

	if(*pcpOURI != '\0')
	{
		*pcgTmpBuf = '\0';

		strcpy(pclWhere,"WHERE (DETY = '1' AND");

		sprintf(pcgTmpBuf,"%s OURI='%s' AND UPRM != '0' ",pclWhere,pcpOURI);
		strcat(pcgTmpBuf, ")");

		if (*cgDataArea != '\0')
		{
			strcat(pcgTmpBuf,cgDataArea);
		}

		dbg(DEBUG,"ReadOldPrmDemands2 Selection: <%s>",pcgTmpBuf);

		if (*pcgTmpBuf)
		{
			CEDAArrayRefill(&rgOldDemArray.rrArrayHandle,
							rgOldDemArray.crArrayName,
							pcgTmpBuf,pclAddData,llAction);
			llAction = ARR_NEXT;
		}
	}
	if(*pcpOURO != '\0')
	{
		*pcgTmpBuf = '\0';

		strcpy(pclWhere,"WHERE (DETY = '2' AND");

		sprintf(pcgTmpBuf,"%s OURO='%s' AND UPRM != '0' ",pclWhere,pcpOURO);
		strcat(pcgTmpBuf, ")");

		if (*cgDataArea != '\0')
		{
			strcat(pcgTmpBuf,cgDataArea);
		}

		dbg(DEBUG,"ReadOldPrmDemands2 Selection: <%s>",pcgTmpBuf);
	
		if (*pcgTmpBuf)
		{
			CEDAArrayRefill(&rgOldDemArray.rrArrayHandle,
							rgOldDemArray.crArrayName,
							pcgTmpBuf,pclAddData,llAction);
			llAction = ARR_NEXT;
		}
	}
	dbg(DEBUG,"ReadOldPrmDemands2 RC=%d",ilRc);
	return ilRc;
}

static int ReadOldDemands(char *pcpOURI,char *pcpOURO, char *pcpTemplates)
{
	int ilRc = RC_SUCCESS;
	/* char pclSelection[1024]; buffers may be too small  */
	char pclTmpBuf[1024];
	/*  char pclRudBuf[512];  buffers may be too small  */
	char pclWhere[512];
	char pclAddData[256];
	long llAction = ARR_FIRST;
	int		ilNoOfItems = 0;
	char	clField[URNOLEN+1];
	/*  char	clTemplates[512]; buffers may be too small  */
	int		i;

	if (pcpTemplates != NULL)
		dbg(DEBUG, "ReadOldDemands Templates: <%s>", pcpTemplates);

	*cgDataArea = '\0';

	/* if((pcpTemplates != NULL) && (*pcpTemplates != '\0')) */
	if((pcpTemplates != NULL) && (atoi(pcpTemplates) != 0) ) 
	{
		*pcgTmpBuf = '\0';
		ilNoOfItems = get_no_of_items(pcpTemplates);
		for ( i = 0; i < ilNoOfItems; i++ )
		{
			if (i == 0)
				strcat(pcgTmpBuf, "'");
			else
				strcat(pcgTmpBuf, ",'");
			get_real_item(clField, pcpTemplates, i+1);
			strcat(pcgTmpBuf, clField);
			strcat(pcgTmpBuf, "'");
		}
		if ( igLoadSel <= 2 ) 
			sprintf(cgDataArea, cgTplLoadExt[igLoadSel], pcgTmpBuf);
	}

	*pcgTmpBuf = '\0';

	strcpy(pclWhere,"WHERE (DETY = '0' AND (");

	if(*pcpOURI != '\0')
	{
		sprintf(pcgTmpBuf,"%s OURI='%s' ",pclWhere,pcpOURI);
		strcpy(pclWhere," OR ");
	}
	if(*pcpOURO != '\0')
	{
		sprintf(pclTmpBuf,"%s OURO='%s' ",pclWhere,pcpOURO);
		strcat(pcgTmpBuf,pclTmpBuf);
	}
	strcat( pcgTmpBuf, "))" );

	if (*cgDataArea != '\0')
	{
		strcat(pcgTmpBuf,cgDataArea);
	}

	dbg(DEBUG,"ReadOldDemands Selection: <%s>",pcgTmpBuf);

	if (*pcgTmpBuf)
	{
		CEDAArrayRefill(&rgOldDemArray.rrArrayHandle,
						rgOldDemArray.crArrayName, pcgTmpBuf, pclAddData,
						llAction);
		llAction = ARR_NEXT;
	}

	if(*pcpOURI != '\0')
	{
		*pcgTmpBuf = '\0';

		strcpy(pclWhere,"WHERE (DETY = '1' AND");

		sprintf(pcgTmpBuf,"%s OURI='%s' ",pclWhere,pcpOURI);
		strcat(pcgTmpBuf, ")");

		if (*cgDataArea != '\0')
		{
			strcat(pcgTmpBuf,cgDataArea);
		}

		dbg(DEBUG,"ReadOldDemands Selection: <%s>",pcgTmpBuf);

		if (*pcgTmpBuf)
		{
			CEDAArrayRefill(&rgOldDemArray.rrArrayHandle,
							rgOldDemArray.crArrayName,
							pcgTmpBuf,pclAddData,llAction);
			llAction = ARR_NEXT;
		}
	}
	if(*pcpOURO != '\0')
	{
		*pcgTmpBuf = '\0';

		strcpy(pclWhere,"WHERE (DETY = '2' AND");

		sprintf(pcgTmpBuf,"%s OURO='%s' ",pclWhere,pcpOURO);
		strcat(pcgTmpBuf, ")");

		if (*cgDataArea != '\0')
		{
			strcat(pcgTmpBuf,cgDataArea);
		}

		dbg(DEBUG,"ReadOldDemands Selection: <%s>",pcgTmpBuf);
	
		if (*pcgTmpBuf)
		{
			CEDAArrayRefill(&rgOldDemArray.rrArrayHandle,
							rgOldDemArray.crArrayName,
							pcgTmpBuf,pclAddData,llAction);
			llAction = ARR_NEXT;
		}
	}
	dbg(DEBUG,"ReadOldDemands RC=%d",ilRc);
	return ilRc;
}

static int DeleteOldDemands()
{
	int ilRc = RC_SUCCESS, ilRc1;
	char *pclDemRow;
	/* long llRowNum = ARR_FIRST; */
	long llRowNum = ARR_LAST;
	char clFlgs[24], clUrno[12], clUrud[12];

	dbg(DEBUG,"DeleteOldDemands: Start" );
	while(CEDAArrayGetRowPointer(&rgOldDemArray.rrArrayHandle,
				rgOldDemArray.crArrayName,llRowNum,(void *)&pclDemRow) == RC_SUCCESS)
	{

		/*llRowNum = ARR_NEXT; */
		/*
		dbg(DEBUG,"%05d:%10s %-s %-14.14s %-14.14s %-14s %-2s <%-2s> %-2s",__LINE__,
					OLDDEMFIELD(pclDemRow,igDemURNO),
					OLDDEMFIELD(pclDemRow,igDemDBAR),
					OLDDEMFIELD(pclDemRow,igDemDEBE),
					OLDDEMFIELD(pclDemRow,igDemDEEN),
					OLDDEMFIELD(pclDemRow,igDemLSTU),
					OLDDEMFIELD(pclDemRow,igDemJFND),
					OLDDEMFIELD(pclDemRow,igDemFLGS),
					OLDDEMFIELD(pclDemRow,igDemSAVE));*/
		strcpy ( clFlgs, OLDDEMFIELD(pclDemRow,igDemFLGS) );
		strcpy ( clUrno, OLDDEMFIELD(pclDemRow,igDemURNO) );
		strcpy ( clUrud, OLDDEMFIELD(pclDemRow,igDemURUD) );

		/*  don't delete manually created demands. Instead update some fields */
		if ( (strlen(clFlgs)>=7) && clFlgs[6]=='1' )
		{
			ilRc = UpdateManualDemand ( pclDemRow );
			dbg ( DEBUG, "DeleteOldDemands: manual demand URNO <%s> URUD <%s> FLGS <%s> will be kept", 
				  clUrno, clUrud, clFlgs );
		}
		else
			if((strncmp(OLDDEMFIELD(pclDemRow,igDemSAVE),"1",1)) &&
				(strncmp(OLDDEMFIELD(pclDemRow,igDemJFND),"1",1)))
			{
				/*  Store earliest debe and last deen for later use in SaveDemands */
				if ( !cgMinDebe[0] || 
					 ( strcmp(OLDDEMFIELD(pclDemRow,igDemDEBE), cgMinDebe ) < 0 )
				   )
					strcpy ( cgMinDebe, OLDDEMFIELD(pclDemRow,igDemDEBE) );
				if ( strcmp(cgMaxDeen, OLDDEMFIELD(pclDemRow,igDemDEEN) ) < 0 )
					strcpy ( cgMaxDeen, OLDDEMFIELD(pclDemRow,igDemDEEN) );

				llRowNum = ARR_CURRENT;
				dbg(DEBUG,"Delete Old Demand Row follows SAVE: <%s> JFND <%s>",
					OLDDEMFIELD(pclDemRow,igDemSAVE),
					OLDDEMFIELD(pclDemRow,igDemJFND));
				ilRc1 = CEDAArrayDeleteRow(&rgOldDemArray.rrArrayHandle,
											rgOldDemArray.crArrayName,llRowNum);
				if ( ilRc1 != RC_SUCCESS )
				{
							int ilOldDbgLevel = debug_level;
					debug_level = TRACE;
					dbg ( TRACE, "DeleteOldDemands: CEDAArrayDeleteRow failed RC <%d> URNO <%s> URUD <%s>",
						  ilRc, clUrno, clUrud );
					debug_level = ilOldDbgLevel;
					ilRc = ilRc1;
				}
				else
					dbg ( TRACE, "DelOld: URNO <%s> URUD <%s> deleted", clUrno, clUrud );
			}
			else
			{
				dbg ( TRACE, "DelOld: Keep URNO <%s> URUD <%s> SAVE <%s> JFND <%s>", 
					  clUrno, clUrud, OLDDEMFIELD(pclDemRow,igDemSAVE), OLDDEMFIELD(pclDemRow,igDemJFND) );
			}

		llRowNum = ARR_PREV;
	}
/*	deleted outside 
	{
		char *pclUpdSelLst = NULL;
		char *pclUpdFldLst = NULL;
		char *pclUpdDatLst = NULL;
		char *pclUpdCmdLst = NULL;
		long llNumUrnos = NULL;
		long llRowCount = 0L;

		CEDAArrayGetRowCount(&rgOldDemArray.rrArrayHandle,
			rgOldDemArray.crArrayName,&llRowCount);

		{
			dbg(TRACE,"DeleteOldDemands: CEDAArrayWriteDBGetChanges follows" );
			ilRc=CEDAArrayWriteDBGetChanges(&rgOldDemArray.rrArrayHandle,
				rgOldDemArray.crArrayName,&pclUpdCmdLst,&pclUpdSelLst,&pclUpdFldLst,
				&pclUpdDatLst,&llNumUrnos,
				ARR_COMMIT_ALL_OK);
			dbg(TRACE,"DeleteOldDemands: commit_work follows" );
			commit_work();
			if (ilRc == RC_SUCCESS && llNumUrnos > 0)
			{
				dbg(DEBUG,"%05d:NumUrnos %d, UpdCmdLst <%s> SelLst <%s> FldLst <%s> DatLst <%s>",__LINE__,
						  llNumUrnos,pclUpdCmdLst,pclUpdSelLst,pclUpdFldLst,pclUpdDatLst);
			}
			else
			{
				dbg(TRACE,"%05d:CEDAArrayWriteDBGetChanges failed RC=%d",__LINE__,ilRc);
			}
		}
	}*/

	dbg(DEBUG,"DeleteOldDemands RC=%d",ilRc);
	return ilRc;
}


static int ReadRudTab()
{
	int ilRc = RC_SUCCESS;
	int ilTemplateNo = 0;
	char clKey[21];
	long llAction = ARR_FIRST;
	long llRow = ARR_FIRST;
	int  ilRefillCnt = 0;
	char *pclResult=0;
	long llWorkRows = 0;

	dbg ( DEBUG, "ReadRudTab: Start");

	CEDAArrayDelete(&rgRudArray.rrArrayHandle,
					rgRudArray.crArrayName);
	CEDAArrayDisactivateIndex ( &(rgRudArray.rrArrayHandle),	
								rgRudArray.crArrayName,
								&(rgRudArray.rrIdx01Handle),
								rgRudArray.crIdx01Name );
	dbg ( DEBUG, "ReadRudTab: CEDAArray cleared");
	for( ilTemplateNo = 0; ilTemplateNo < MAXRUES && 
							prgRueUrnos[ilTemplateNo].IsUsed; ilTemplateNo++)
	{	
		if ( *prgRueUrnos[ilTemplateNo].Urno &&
			 (atol(prgRueUrnos[ilTemplateNo].Urno)>0) )
		{
			if ( (ilTemplateNo == 0 ) ||  
				strcmp(prgRueUrnos[ilTemplateNo-1].Urno,
						   prgRueUrnos[ilTemplateNo].Urno ) )
			{	/* if same URNO has been inserted before (Turnaround rule) -> skip */
				sprintf ( clKey, "%s,1,0", prgRueUrnos[ilTemplateNo].Urno );
				llRow = ARR_FIRST;
				while ( CEDAArrayFindRowPointer(&rgCCIRudArray.rrArrayHandle,
					 							rgCCIRudArray.crArrayName,
												&rgCCIRudArray.rrIdx02Handle,
												rgCCIRudArray.crIdx02Name,clKey,
												&llRow,(void**)&pclResult) == RC_SUCCESS )
				{
					AddRowToRudArray ( llRow );
					llRow = ARR_NEXT;
				}
				dbg ( DEBUG, "ReadRudTab: %d. rule added <%s>", ilTemplateNo+1,
					  prgRueUrnos[ilTemplateNo].Urno  );
			}
		}
	}
	CEDAArrayActivateIndex (&(rgRudArray.rrArrayHandle),	
							rgRudArray.crArrayName,
							&(rgRudArray.rrIdx01Handle),
							rgRudArray.crIdx01Name );
	CEDAArrayGetRowCount(&rgRudArray.rrArrayHandle,
							rgRudArray.crArrayName,&llWorkRows);
	dbg ( TRACE, "ReadRudTab: End RudCount=%ld",llWorkRows);
	/*lgUsedRudUrnosLength = (llWorkRows*15)+100;
	pcgUsedRudUrnos = realloc(pcgUsedRudUrnos,lgUsedRudUrnosLength);
	memset(pcgUsedRudUrnos,0,lgUsedRudUrnosLength);*/
	return ilRc;
}

static int UpdateDemand(ARRAYINFO *prpDest,long lpRowNum, ARRAYINFO *prpSource)
{
	int ilRc = RC_SUCCESS;
	char pclNow[DATELEN+1], clAloc[21], clFlgs[24];
	long llFieldNo;
	BOOL blModified = FALSE;

	strcpy ( clAloc, NEWDEMFIELD(prpSource->pcrArrayRowBuf,igDemALOC) );

	if ( !strstr ( cgIgnoreAloc, clAloc ) )
	{
		if (strcmp(OLDDEMFIELD(prpDest->pcrArrayRowBuf,igDemALID),
			NEWDEMFIELD(prpSource->pcrArrayRowBuf,igDemALID)))
		{
			llFieldNo = -1;

			CEDAArrayPutField(&prpDest->rrArrayHandle,prpDest->crArrayName,
				&llFieldNo,"ALID",lpRowNum,
					NEWDEMFIELD(prpSource->pcrArrayRowBuf,igDemALID));
			blModified = TRUE;
			
		}
	}
	else
		dbg ( DEBUG, "UpdateDemand: No update of ALID for ALOC <%s>", clAloc );

	if (strcmp(OLDDEMFIELD(prpDest->pcrArrayRowBuf,igDemALOC),
		NEWDEMFIELD(prpSource->pcrArrayRowBuf,igDemALOC)))
	{
		llFieldNo = -1;

		CEDAArrayPutField(&prpDest->rrArrayHandle,prpDest->crArrayName,
			&llFieldNo,"ALOC",lpRowNum,
				NEWDEMFIELD(prpSource->pcrArrayRowBuf,igDemALOC));
		blModified = TRUE;
	}
	if (strcmp(OLDDEMFIELD(prpDest->pcrArrayRowBuf,igDemDBAR),
		NEWDEMFIELD(prpSource->pcrArrayRowBuf,igDemDBAR)))
	{
		llFieldNo = -1;

		CEDAArrayPutField(&prpDest->rrArrayHandle,prpDest->crArrayName,
			&llFieldNo,"DBAR",lpRowNum,
				NEWDEMFIELD(prpSource->pcrArrayRowBuf,igDemDBAR));
		blModified = TRUE;
	}
	if (strcmp(OLDDEMFIELD(prpDest->pcrArrayRowBuf,igDemDEAR),
		NEWDEMFIELD(prpSource->pcrArrayRowBuf,igDemDEAR)))
	{
		llFieldNo = -1;

		CEDAArrayPutField(&prpDest->rrArrayHandle,prpDest->crArrayName,
			&llFieldNo,"DEAR",lpRowNum,
				NEWDEMFIELD(prpSource->pcrArrayRowBuf,igDemDEAR));
		blModified = TRUE;
	}
	if (strcmp(OLDDEMFIELD(prpDest->pcrArrayRowBuf,igDemDEBE),
		NEWDEMFIELD(prpSource->pcrArrayRowBuf,igDemDEBE)))
	{
		llFieldNo = -1;

		CEDAArrayPutField(&prpDest->rrArrayHandle,prpDest->crArrayName,
			&llFieldNo,"DEBE",lpRowNum,
				NEWDEMFIELD(prpSource->pcrArrayRowBuf,igDemDEBE));
		blModified = TRUE;
	}
	if (strcmp(OLDDEMFIELD(prpDest->pcrArrayRowBuf,igDemDEDU),
		NEWDEMFIELD(prpSource->pcrArrayRowBuf,igDemDEDU)))
	{
		llFieldNo = -1;

		CEDAArrayPutField(&prpDest->rrArrayHandle,prpDest->crArrayName,
			&llFieldNo,"DEDU",lpRowNum,
				NEWDEMFIELD(prpSource->pcrArrayRowBuf,igDemDEDU));
		blModified = TRUE;
	}
	if (strcmp(OLDDEMFIELD(prpDest->pcrArrayRowBuf,igDemDEEN),
		NEWDEMFIELD(prpSource->pcrArrayRowBuf,igDemDEEN)))
	{
		llFieldNo = -1;

		CEDAArrayPutField(&prpDest->rrArrayHandle,prpDest->crArrayName,
			&llFieldNo,"DEEN",lpRowNum,
				NEWDEMFIELD(prpSource->pcrArrayRowBuf,igDemDEEN));
		blModified = TRUE;
	}
	if (strcmp(OLDDEMFIELD(prpDest->pcrArrayRowBuf,igDemDETY),
		NEWDEMFIELD(prpSource->pcrArrayRowBuf,igDemDETY)))
	{
		llFieldNo = -1;

		CEDAArrayPutField(&prpDest->rrArrayHandle,prpDest->crArrayName,
			&llFieldNo,"DETY",lpRowNum,
				NEWDEMFIELD(prpSource->pcrArrayRowBuf,igDemDETY));
		blModified = TRUE;
	}
	if (strcmp(OLDDEMFIELD(prpDest->pcrArrayRowBuf,igDemEADB),
		NEWDEMFIELD(prpSource->pcrArrayRowBuf,igDemEADB)))
	{
		llFieldNo = -1;

		CEDAArrayPutField(&prpDest->rrArrayHandle,prpDest->crArrayName,
			&llFieldNo,"EADB",lpRowNum,
				NEWDEMFIELD(prpSource->pcrArrayRowBuf,igDemEADB));
		blModified = TRUE;
	}
	if (strcmp(OLDDEMFIELD(prpDest->pcrArrayRowBuf,igDemLADE),
		NEWDEMFIELD(prpSource->pcrArrayRowBuf,igDemLADE)))
	{
		llFieldNo = -1;

		CEDAArrayPutField(&prpDest->rrArrayHandle,prpDest->crArrayName,
			&llFieldNo,"LADE",lpRowNum,
				NEWDEMFIELD(prpSource->pcrArrayRowBuf,igDemLADE));
		blModified = TRUE;
	}
	if (strcmp(OLDDEMFIELD(prpDest->pcrArrayRowBuf,igDemUCON),
		NEWDEMFIELD(prpSource->pcrArrayRowBuf,igDemUCON)))
	{
		llFieldNo = -1;

		CEDAArrayPutField(&prpDest->rrArrayHandle,prpDest->crArrayName,
			&llFieldNo,"UCON",lpRowNum,
				NEWDEMFIELD(prpSource->pcrArrayRowBuf,igDemUCON));
		blModified = TRUE;
	}
	
	if (strcmp(OLDDEMFIELD(prpDest->pcrArrayRowBuf,igDemOBTY),
		NEWDEMFIELD(prpSource->pcrArrayRowBuf,igDemOBTY)))
	{
		llFieldNo = -1;

		CEDAArrayPutField(&prpDest->rrArrayHandle,prpDest->crArrayName,
			&llFieldNo,"OBTY",lpRowNum,
				NEWDEMFIELD(prpSource->pcrArrayRowBuf,igDemOBTY));
		blModified = TRUE;
	}
	if (strcmp(OLDDEMFIELD(prpDest->pcrArrayRowBuf,igDemOURI),
		NEWDEMFIELD(prpSource->pcrArrayRowBuf,igDemOURI)))
	{
		llFieldNo = -1;

		CEDAArrayPutField(&prpDest->rrArrayHandle,prpDest->crArrayName,
			&llFieldNo,"OURI",lpRowNum,
				NEWDEMFIELD(prpSource->pcrArrayRowBuf,igDemOURI));
		blModified = TRUE;
	}
	if (strcmp(OLDDEMFIELD(prpDest->pcrArrayRowBuf,igDemOURO),
		NEWDEMFIELD(prpSource->pcrArrayRowBuf,igDemOURO)))
	{
		llFieldNo = -1;

		CEDAArrayPutField(&prpDest->rrArrayHandle,prpDest->crArrayName,
			&llFieldNo,"OURO",lpRowNum,
				NEWDEMFIELD(prpSource->pcrArrayRowBuf,igDemOURO));
		blModified = TRUE;
	}
	if (strcmp(OLDDEMFIELD(prpDest->pcrArrayRowBuf,igDemRTDB),
		NEWDEMFIELD(prpSource->pcrArrayRowBuf,igDemRTDB)))
	{
		llFieldNo = -1;

		CEDAArrayPutField(&prpDest->rrArrayHandle,prpDest->crArrayName,
			&llFieldNo,"RTDB",lpRowNum,
				NEWDEMFIELD(prpSource->pcrArrayRowBuf,igDemRTDB));
		blModified = TRUE;
	}
	if (strcmp(OLDDEMFIELD(prpDest->pcrArrayRowBuf,igDemRTDE),
		NEWDEMFIELD(prpSource->pcrArrayRowBuf,igDemRTDE)))
	{
		llFieldNo = -1;

		CEDAArrayPutField(&prpDest->rrArrayHandle,prpDest->crArrayName,
			&llFieldNo,"RTDE",lpRowNum,
				NEWDEMFIELD(prpSource->pcrArrayRowBuf,igDemRTDE));
		blModified = TRUE;
	}
	if (strcmp(OLDDEMFIELD(prpDest->pcrArrayRowBuf,igDemSDTI),
		NEWDEMFIELD(prpSource->pcrArrayRowBuf,igDemSDTI)))
	{
		llFieldNo = -1;

		CEDAArrayPutField(&prpDest->rrArrayHandle,prpDest->crArrayName,
			&llFieldNo,"SDTI",lpRowNum,
				NEWDEMFIELD(prpSource->pcrArrayRowBuf,igDemSDTI));
		blModified = TRUE;
	}
	if (strcmp(OLDDEMFIELD(prpDest->pcrArrayRowBuf,igDemSUTI),
		NEWDEMFIELD(prpSource->pcrArrayRowBuf,igDemSUTI)))
	{
		llFieldNo = -1;

		CEDAArrayPutField(&prpDest->rrArrayHandle,prpDest->crArrayName,
			&llFieldNo,"SUTI",lpRowNum,
				NEWDEMFIELD(prpSource->pcrArrayRowBuf,igDemSUTI));
		blModified = TRUE;
	}
	if (strcmp(OLDDEMFIELD(prpDest->pcrArrayRowBuf,igDemTSDB),
		NEWDEMFIELD(prpSource->pcrArrayRowBuf,igDemTSDB)))
	{
		llFieldNo = -1;

		CEDAArrayPutField(&prpDest->rrArrayHandle,prpDest->crArrayName,
			&llFieldNo,"TSDB",lpRowNum,
				NEWDEMFIELD(prpSource->pcrArrayRowBuf,igDemTSDB));
		blModified = TRUE;
	}
	if (strcmp(OLDDEMFIELD(prpDest->pcrArrayRowBuf,igDemTSDE),
		NEWDEMFIELD(prpSource->pcrArrayRowBuf,igDemTSDE)))
	{
		llFieldNo = -1;

		CEDAArrayPutField(&prpDest->rrArrayHandle,prpDest->crArrayName,
			&llFieldNo,"TSDE",lpRowNum,
				NEWDEMFIELD(prpSource->pcrArrayRowBuf,igDemTSDE));
		blModified = TRUE;
	}
	if (strcmp(OLDDEMFIELD(prpDest->pcrArrayRowBuf,igDemTTGF),
		NEWDEMFIELD(prpSource->pcrArrayRowBuf,igDemTTGF)))
	{
		llFieldNo = -1;

		CEDAArrayPutField(&prpDest->rrArrayHandle,prpDest->crArrayName,
			&llFieldNo,"TTGF",lpRowNum,
				NEWDEMFIELD(prpSource->pcrArrayRowBuf,igDemTTGF));
		blModified = TRUE;
	}
	if (strcmp(OLDDEMFIELD(prpDest->pcrArrayRowBuf,igDemTTGT),
		NEWDEMFIELD(prpSource->pcrArrayRowBuf,igDemTTGT)))
	{
		llFieldNo = -1;

		CEDAArrayPutField(&prpDest->rrArrayHandle,prpDest->crArrayName,
			&llFieldNo,"TTGT",lpRowNum,
				NEWDEMFIELD(prpSource->pcrArrayRowBuf,igDemTTGT));
		blModified = TRUE;
	}
	if (strcmp(OLDDEMFIELD(prpDest->pcrArrayRowBuf,igDemUDGR),
		NEWDEMFIELD(prpSource->pcrArrayRowBuf,igDemUDGR)))
	{
		llFieldNo = -1;

		CEDAArrayPutField(&prpDest->rrArrayHandle,prpDest->crArrayName,
			&llFieldNo,"UDGR",lpRowNum,
				NEWDEMFIELD(prpSource->pcrArrayRowBuf,igDemUDGR));
		blModified = TRUE;
	}
	/*
	if (strcmp(OLDDEMFIELD(prpDest->pcrArrayRowBuf,igDemURUD),
		NEWDEMFIELD(prpSource->pcrArrayRowBuf,igDemURUD)))
	{
		llFieldNo = -1;

		CEDAArrayPutField(&prpDest->rrArrayHandle,prpDest->crArrayName,
			&llFieldNo,"URUD",lpRowNum,
				NEWDEMFIELD(prpSource->pcrArrayRowBuf,igDemURUD));
		blModified = TRUE;
	}*/
	if (strcmp(OLDDEMFIELD(prpDest->pcrArrayRowBuf,igDemRETY),
		NEWDEMFIELD(prpSource->pcrArrayRowBuf,igDemRETY)))
	{
		llFieldNo = -1;

		CEDAArrayPutField(&prpDest->rrArrayHandle,prpDest->crArrayName,
			&llFieldNo,"RETY",lpRowNum,
				NEWDEMFIELD(prpSource->pcrArrayRowBuf,igDemRETY));
		blModified = TRUE;
	}
	if (strcmp(OLDDEMFIELD(prpDest->pcrArrayRowBuf,igDemUREF),
		NEWDEMFIELD(prpSource->pcrArrayRowBuf,igDemUREF)))
	{
		llFieldNo = -1;

		CEDAArrayPutField(&prpDest->rrArrayHandle,prpDest->crArrayName,
			&llFieldNo,"UREF",lpRowNum,
				NEWDEMFIELD(prpSource->pcrArrayRowBuf,igDemUREF));
		blModified = TRUE;
	}
	if (bgUseFilt)
	{
		if (strcmp(OLDDEMFIELD(prpDest->pcrArrayRowBuf,igDemFILT),
			NEWDEMFIELD(prpSource->pcrArrayRowBuf,igDemFILT)))
		{
			llFieldNo = -1;

			CEDAArrayPutField(&prpDest->rrArrayHandle,prpDest->crArrayName,
				&llFieldNo,"FILT",lpRowNum,
					NEWDEMFIELD(prpSource->pcrArrayRowBuf,igDemFILT));
			blModified = TRUE;
		}
	}
	if ( (igDemUSES>0) && 
		 strcmp(OLDDEMFIELD(prpDest->pcrArrayRowBuf,igDemUSES),
				NEWDEMFIELD(prpSource->pcrArrayRowBuf,igDemUSES)))
	{
		llFieldNo = -1;

		CEDAArrayPutField(&prpDest->rrArrayHandle,prpDest->crArrayName,
			&llFieldNo,"USES",lpRowNum,
				NEWDEMFIELD(prpSource->pcrArrayRowBuf,igDemUSES));
		blModified = TRUE;
	}
	if (strcmp(OLDDEMFIELD(prpDest->pcrArrayRowBuf,igDemFLGS),
		NEWDEMFIELD(prpSource->pcrArrayRowBuf,igDemFLGS)))
	{
		strcpy ( clFlgs, NEWDEMFIELD(prpSource->pcrArrayRowBuf,igDemFLGS) );
		strcat ( clFlgs, "          " );
		clFlgs[10] ='\0';
		if (!strncmp((OLDDEMFIELD(prpDest->pcrArrayRowBuf,igDemFLGS)+5),
						"1", 1))
			clFlgs[5] = '1';
		/*  take flag "disactivated" from original demand */
		if (!strncmp((OLDDEMFIELD(prpDest->pcrArrayRowBuf,igDemFLGS)+7),
						"1", 1))
			clFlgs[7] = '1';
		llFieldNo = -1;
		dbg ( DEBUG, "UpdateDemand: URUD <%s> FLGS Old <%s> New <%s>  ==> <%s>", 
			  OLDDEMFIELD(prpDest->pcrArrayRowBuf,igDemURUD),
			  OLDDEMFIELD(prpDest->pcrArrayRowBuf,igDemFLGS), 
			  NEWDEMFIELD(prpSource->pcrArrayRowBuf,igDemFLGS), clFlgs );

		CEDAArrayPutField(&prpDest->rrArrayHandle,prpDest->crArrayName,
						  &llFieldNo,"FLGS",lpRowNum, clFlgs );
		blModified = TRUE;
	}

	if ( blModified )
	{
		dbg ( DEBUG, "UpdateDemand: Going to update demand URUD  <%s>",
			  OLDDEMFIELD(prpDest->pcrArrayRowBuf,igDemURUD) );
		if (strcmp(OLDDEMFIELD(prpDest->pcrArrayRowBuf,igDemUSEU),
			NEWDEMFIELD(prpSource->pcrArrayRowBuf,igDemUSEU)))
		{
			llFieldNo = -1;
			GetServerTimeStamp("UTC", 1, 0, pclNow);
			CEDAArrayPutField(&prpDest->rrArrayHandle, prpDest->crArrayName,
							  &llFieldNo, "LSTU", lpRowNum, pclNow);
		
			llFieldNo = -1;
			CEDAArrayPutField(&prpDest->rrArrayHandle,prpDest->crArrayName,
				&llFieldNo,"USEU",lpRowNum,
					NEWDEMFIELD(prpSource->pcrArrayRowBuf,igDemUSEU));
		}
	}
	else 
		dbg ( DEBUG, "UpdateDemand: demand URUD <%s> unchanged ", 
			 OLDDEMFIELD(prpDest->pcrArrayRowBuf,igDemURUD) );
	return ilRc;
}

static int InitializeNewDemand(char *pcpNewDemandBuf)
{
		int ilRc = RC_SUCCESS;
		char pclUrno[32]="0";
		char pclNow[DATELEN+1];

		/* GetNextValues(pclUrno,1); */
		GetNextUrno(pclUrno);
		GetServerTimeStamp("UTC", 1, 0, pclNow);

		memset(pcpNewDemandBuf,0,rgNewDemArray.lrArrayRowLen);

		strcpy(NEWDEMFIELD(pcpNewDemandBuf,igDemALOC)," ");
		strcpy(NEWDEMFIELD(pcpNewDemandBuf,igDemCDAT),pclNow);
		strcpy(NEWDEMFIELD(pcpNewDemandBuf,igDemDBAR)," ");
		strcpy(NEWDEMFIELD(pcpNewDemandBuf,igDemDEAR)," ");
		strcpy(NEWDEMFIELD(pcpNewDemandBuf,igDemDEBE),"0");
		strcpy(NEWDEMFIELD(pcpNewDemandBuf,igDemDEDU),"0");
		strcpy(NEWDEMFIELD(pcpNewDemandBuf,igDemDEEN),"0");
		strcpy(NEWDEMFIELD(pcpNewDemandBuf,igDemDETY),"3");
		strcpy(NEWDEMFIELD(pcpNewDemandBuf,igDemEADB),"0");
		strcpy(NEWDEMFIELD(pcpNewDemandBuf,igDemHOPO),cgHopo);
		strcpy(NEWDEMFIELD(pcpNewDemandBuf,igDemLADE),"0");
		strcpy(NEWDEMFIELD(pcpNewDemandBuf,igDemUCON)," ");
		strcpy(NEWDEMFIELD(pcpNewDemandBuf,igDemLSTU)," ");
		strcpy(NEWDEMFIELD(pcpNewDemandBuf,igDemOBTY),"AFT");
		strcpy(NEWDEMFIELD(pcpNewDemandBuf,igDemOURI),"0");
		strcpy(NEWDEMFIELD(pcpNewDemandBuf,igDemOURO),"0");
		strcpy(NEWDEMFIELD(pcpNewDemandBuf,igDemRTDB),"0");
		strcpy(NEWDEMFIELD(pcpNewDemandBuf,igDemRTDE),"0");
		strcpy(NEWDEMFIELD(pcpNewDemandBuf,igDemSDTI),"0");
		strcpy(NEWDEMFIELD(pcpNewDemandBuf,igDemSUTI),"0");
		strcpy(NEWDEMFIELD(pcpNewDemandBuf,igDemTSDB),"0");
		strcpy(NEWDEMFIELD(pcpNewDemandBuf,igDemTSDE),"0");
		strcpy(NEWDEMFIELD(pcpNewDemandBuf,igDemTTGF),"0");
		strcpy(NEWDEMFIELD(pcpNewDemandBuf,igDemTTGT),"0");
		strcpy(NEWDEMFIELD(pcpNewDemandBuf,igDemUDGR),"0");
		strcpy(NEWDEMFIELD(pcpNewDemandBuf,igDemURNO),pclUrno);
		strcpy(NEWDEMFIELD(pcpNewDemandBuf,igDemURUD),"0");
		strcpy(NEWDEMFIELD(pcpNewDemandBuf,igDemUSEC),"DEMHDL");
		strcpy(NEWDEMFIELD(pcpNewDemandBuf,igDemUSEU),"DEMHDL");
		strcpy(NEWDEMFIELD(pcpNewDemandBuf,igDemRETY)," ");
		strcpy(NEWDEMFIELD(pcpNewDemandBuf,igDemUREF)," ");
		if (igDemUSES>0)
			strcpy(NEWDEMFIELD(pcpNewDemandBuf,igDemUSES)," ");
		if (igDemUTPL>0)
			strcpy(NEWDEMFIELD(pcpNewDemandBuf,igDemUTPL),"0");
		if (igDemUPRM>0)
			strcpy(NEWDEMFIELD(pcpNewDemandBuf,igDemUPRM),"0");
		return ilRc;
}

static int CreateFid(char *pcpDate)
{

	int ilRc = RC_SUCCESS;
	long llRowNum = ARR_FIRST;
	char *pclRudRow = NULL;
	char pclTime1[32];
	char pclTime2[32];
	char pclDate[32];
	char pclTmpStr[32];
	/* char *pclUsedRudUrnos = NULL; */

/*	int ilLc;
	long llDemandRowNum = ARR_CURRENT;
	char pclFieldName[32];
	char *pclTmpBuf;
*/

	/* first delete all old demands **/
	
	dbg(DEBUG,"CreateFid");

	CEDAArrayDelete(&rgNewDemArray.rrArrayHandle,
					rgNewDemArray.crArrayName);

	dbg(DEBUG,"old new demand-array deleted ");

	strcpy(pclDate,pcpDate);
	UtcToLocal(pclDate);
	pclDate[8] = '\0';

	/************ empty FISU list *******************/
	/*memset(pcgUsedRudUrnos,0,lgUsedRudUrnosLength); */


	dbg(DEBUG,"DB DEDU DEBE DEEN DRTY  TSDB TSDE SUTI SDTI TTGT TTGF");
	while(CEDAArrayGetRowPointer(&rgRudArray.rrArrayHandle,
				rgRudArray.crArrayName,llRowNum,(void *)&pclRudRow) == RC_SUCCESS)
	{
		llRowNum = ARR_NEXT;

		dbg(DEBUG,"%-s  %-6.6s %-14.14s %-14.14s <%-1.1s> %-5.5s%-5.5s%-5.5s%-5.5s%-5.5s%-5.5s%-5.5s%-5.5s",

					RUDFIELD(pclRudRow,igRudDBAR),
					RUDFIELD(pclRudRow,igRudDEDU),
					RUDFIELD(pclRudRow,igRudDEBE),
					RUDFIELD(pclRudRow,igRudDEEN),
					RUDFIELD(pclRudRow,igRudDRTY),
					RUDFIELD(pclRudRow,igRudTSDB),
					RUDFIELD(pclRudRow,igRudTSDE),
					RUDFIELD(pclRudRow,igRudSUTI),
					RUDFIELD(pclRudRow,igRudSDTI),
					RUDFIELD(pclRudRow,igRudRTDB),
					RUDFIELD(pclRudRow,igRudRTDE),
					RUDFIELD(pclRudRow,igRudTTGT),
					RUDFIELD(pclRudRow,igRudTTGF));

		InitializeNewDemand(pcgNewDemandBuf);
		InitDemFieldsFromRud ( pcgNewDemandBuf, pclRudRow );

		dbg(DEBUG,"New Urno: <%s>",NEWDEMFIELD(pcgNewDemandBuf,igDemURNO));

		strcpy(NEWDEMFIELD(pcgNewDemandBuf,igDemDETY), "7");
		strcpy(NEWDEMFIELD(pcgNewDemandBuf,igDemHOPO),
				RUDFIELD(pclRudRow,igRudHOPO));
	 	/*
		strcpy(NEWDEMFIELD(pcgNewDemandBuf,igDemDBAR),
				RUDFIELD(pclRudRow,igRudDBAR));
		strcpy(NEWDEMFIELD(pcgNewDemandBuf,igDemDEAR),
				RUDFIELD(pclRudRow,igRudDEAR));
		strcpy(NEWDEMFIELD(pcgNewDemandBuf,igDemDEDU),
				RUDFIELD(pclRudRow,igRudDEDU));
		strcpy(NEWDEMFIELD(pcgNewDemandBuf,igDemEADB),
				RUDFIELD(pclRudRow,igRudEADB));
		strcpy(NEWDEMFIELD(pcgNewDemandBuf,igDemLADE),
				RUDFIELD(pclRudRow,igRudLADE));
		*/
		strcpy(NEWDEMFIELD(pcgNewDemandBuf,igDemOBTY),"FID");
		if(strncmp(RUDFIELD(pclRudRow,igRudDBFL),"1",1) == 0)
		{
			/************ calculate demand begin ****/
			sprintf(pclTime1,"%s%s",pclDate,(char *)RUDFIELD(pclRudRow,igRudEADB)+8);
			SunCompStrAddTime(pclTime1,RUDFIELD(pclRudRow,igRudDEDU),pclTime2);
			/*********** + duty duration = duty end ******************/
			sprintf(pclTmpStr,"-%s",RUDFIELD(pclRudRow,igRudSUTI));
			SunCompStrAddTime(pclTime1,pclTmpStr,pclTime1);
			/*********** setup time substracted **********************/ 
			sprintf(pclTmpStr,"-%s",RUDFIELD(pclRudRow,igRudTTGT));
			SunCompStrAddTime(pclTime1,pclTmpStr,pclTime1);
			/*********** time to go to time substracted ****************/ 
			LocalToUtc(pclTime1);
			strcpy(NEWDEMFIELD(pcgNewDemandBuf,igDemDEBE),pclTime1);
			/********* now calculate duty end, saved in pclTime2 ******/
			SunCompStrAddTime(pclTime2,RUDFIELD(pclRudRow,igRudTTGF),pclTime2);
			/*********** time to go to time added *********************/ 
			SunCompStrAddTime(pclTime2,RUDFIELD(pclRudRow,igRudSDTI),pclTime2);
			/*********** setdown time added **********************/ 
			LocalToUtc(pclTime2);
			strcpy(NEWDEMFIELD(pcgNewDemandBuf,igDemDEEN),pclTime2);
		}
		else if(strncmp(RUDFIELD(pclRudRow,igRudDEFL),"1",1) == 0)
		{
			/************ calculate demand end ****/
			sprintf(pclTime1,"%s%s",pclDate,(char *)RUDFIELD(pclRudRow,igRudLADE)+8);
			SunCompStrAddTime(pclTime1,RUDFIELD(pclRudRow,igRudTTGF),pclTime1);
			/*********** time to go from time added *********************/ 
			sprintf(pclTmpStr,"-%s",RUDFIELD(pclRudRow,igRudDEDU));
			SunCompStrAddTime(pclTime1,pclTmpStr,pclTime1);
			/*********** - duty duration = duty begin ******************/
			sprintf(pclTmpStr,"-%s",RUDFIELD(pclRudRow,igRudSUTI));
			SunCompStrAddTime(pclTime1,pclTmpStr,pclTime1);
			/*********** setup time subtracted **********************/ 
			sprintf(pclTmpStr,"-%s",RUDFIELD(pclRudRow,igRudTTGT));
			SunCompStrAddTime(pclTime1,pclTmpStr,pclTime1);
			/*********** time to go to time subtracted ****************/ 
			LocalToUtc(pclTime1);
			strcpy(NEWDEMFIELD(pcgNewDemandBuf,igDemDEBE),pclTime1);
			/********* now calculate duty end, saved in pclTime2 ******/
			SunCompStrAddTime(pclTime2,RUDFIELD(pclRudRow,igRudSDTI),pclTime2);
			/*********** setdown time added **********************/ 
			LocalToUtc(pclTime2);
			strcpy(NEWDEMFIELD(pcgNewDemandBuf,igDemDEEN),pclTime2);
		}
		else
		{
			/************ calculate demand begin ****/
			sprintf(pclTime1,"%s%s",pclDate,(char *)RUDFIELD(pclRudRow,igRudDEBE)+8);
			SunCompStrAddTime(pclTime1,RUDFIELD(pclRudRow,igRudDEDU),pclTime2);
			/*********** + duty duration = duty end ******************/
			sprintf(pclTmpStr,"-%s",RUDFIELD(pclRudRow,igRudSUTI));
			SunCompStrAddTime(pclTime1,pclTmpStr,pclTime1);
			/*********** setup time substracted **********************/ 
			sprintf(pclTmpStr,"-%s",RUDFIELD(pclRudRow,igRudTTGT));
			SunCompStrAddTime(pclTime1,pclTmpStr,pclTime1);
			/*********** time to go to time substracted ****************/ 
			LocalToUtc(pclTime1);
			strcpy(NEWDEMFIELD(pcgNewDemandBuf,igDemDEBE),pclTime1);
			/********* now calculate duty end, saved in pclTime2 ******/
			SunCompStrAddTime(pclTime2,RUDFIELD(pclRudRow,igRudTTGF),pclTime2);
			/*********** time to go to time added *********************/ 
			SunCompStrAddTime(pclTime2,RUDFIELD(pclRudRow,igRudSDTI),pclTime2);
			/*********** setdown time added **********************/ 
			LocalToUtc(pclTime2);
			strcpy(NEWDEMFIELD(pcgNewDemandBuf,igDemDEEN),pclTime2);
		}
		/*
		strcpy(NEWDEMFIELD(pcgNewDemandBuf,igDemRTDB),
				RUDFIELD(pclRudRow,igRudRTDB));
		strcpy(NEWDEMFIELD(pcgNewDemandBuf,igDemRTDE),
				RUDFIELD(pclRudRow,igRudRTDE));
		strcpy(NEWDEMFIELD(pcgNewDemandBuf,igDemSDTI),
				RUDFIELD(pclRudRow,igRudSDTI));
		strcpy(NEWDEMFIELD(pcgNewDemandBuf,igDemSUTI),
				RUDFIELD(pclRudRow,igRudSUTI));
		strcpy(NEWDEMFIELD(pcgNewDemandBuf,igDemTSDB),
				RUDFIELD(pclRudRow,igRudTSDB));
		strcpy(NEWDEMFIELD(pcgNewDemandBuf,igDemTSDE),
				RUDFIELD(pclRudRow,igRudTSDE));
		strcpy(NEWDEMFIELD(pcgNewDemandBuf,igDemTTGF),
				RUDFIELD(pclRudRow,igRudTTGF));
		strcpy(NEWDEMFIELD(pcgNewDemandBuf,igDemTTGT),
				RUDFIELD(pclRudRow,igRudTTGT));
		strcpy(NEWDEMFIELD(pcgNewDemandBuf,igDemURUD),
				RUDFIELD(pclRudRow,igRudURNO));
		strcpy(NEWDEMFIELD(pcgNewDemandBuf,igDemRETY),
				RUDFIELD(pclRudRow,igRudRETY));
		if ( (igRudUSES>0) && (igDemUSES>0) )
			strcpy( NEWDEMFIELD(pcgNewDemandBuf,igDemUSES),
					RUDFIELD(pclRudRow,igRudUSES));*/
/*		if (pclUsedRudUrnos != NULL)
		{
			sprintf(pclUsedRudUrnos,",'%s'",RUDFIELD(pclRudRow,igRudURNO));
			pclUsedRudUrnos += strlen(pclUsedRudUrnos) + 1;
		}
		else
		{
			pclUsedRudUrnos = pcgUsedRudUrnos;
			sprintf(pclUsedRudUrnos,"WHERE URNO IN ('%s'",RUDFIELD(pclRudRow,igRudURNO));
		}*/
		dbg(DEBUG,"<%s>",rgNewDemArray.crArrayFieldList);

		dbg(DEBUG,"New Urno: <%s>",NEWDEMFIELD(pcgNewDemandBuf,igDemURNO));
		ilRc = AddNewDemandRow ();
	}
	return ilRc;
}

static int GetAlidFromAloc(char *pcpAlid,char *pcpAloc,char *pcpIOFlag)
{
	int ilRc = RC_SUCCESS;
	int ilIndex;
	int ilTableNo;
	BOOL blFound = FALSE, blIgnore = TRUE;
	char *pclFlight = NULL;
	struct AlocRefStruct *prlAlocRef;

	dbg(DEBUG,"GetAlidFromAloc <%s> <%s>",pcpAloc,pcpIOFlag);
	switch(*pcpIOFlag)
	{
		case '0' : 
		case '2' : 
			pclFlight = pcgOutbound;
			prlAlocRef = prgAlocRefOutbound;
			break;
		case '1' : 
			pclFlight = pcgInbound;
			prlAlocRef = prgAlocRefInbound;
			break;
		default: /* nothing to do, pclFlight is already NULL */
			;
	}
	if(pclFlight == NULL)
	{
		dbg(TRACE,"Invalid I/O Flag <%s>",pcpIOFlag);
		ilRc = RC_FAIL;
	}

	if(ilRc == RC_SUCCESS)
	{
		if ( strstr(cgIgnoreAloc, pcpAloc ) )
		{
			/* this is an allocation unit, that can be ignored, e. g. CIC */
			ilRc = RC_FAIL;
			dbg ( DEBUG, "GetAlidFromAloc: ALOC <%s> can be ignored cgIgnoreAloc <%s>",
					pcpAloc, cgIgnoreAloc );
		}	
	}
	if(ilRc == RC_SUCCESS)
	{
		for(ilIndex = 0; 
					ilIndex < MAXALOCREFS && *prlAlocRef[ilIndex].Name !='\0' &&
							blFound == FALSE; ilIndex++)
		{
			if(strncmp(pcpAloc,prlAlocRef[ilIndex].Name,
					strlen(prlAlocRef[ilIndex].Name)) == 0)
			{
				blIgnore = FALSE;
				dbg(DEBUG,"Index %d Name: <%s>",ilIndex,prlAlocRef[ilIndex].Name);
				for(ilTableNo = 0; ilTableNo < MAXALOCREFENTRIES && 
							! blFound && 
								*prlAlocRef[ilIndex].Table[ilTableNo] != '\0';ilTableNo++)
				{
					dbg(DEBUG,"Table: <%s> Field: <%s>",
							prlAlocRef[ilIndex].Table[ilTableNo],
							prlAlocRef[ilIndex].Field[ilTableNo]);
					if(strcmp(prlAlocRef[ilIndex].Table[ilTableNo],"AFT") == 0)
					{
						int ilCol,ilPos;
						int ilItemNo;

						ilRc = FindItemInList(pcgFieldList,
								prlAlocRef[ilIndex].Field[ilTableNo],',',&ilItemNo,
										&ilCol,&ilPos);
						if(ilRc == RC_SUCCESS)
						{
							char pclTmp[124];

							GetDataItem(pclTmp,pclFlight,ilItemNo,',',""," \0");
							if(*pclTmp != '\0')
							{
								blFound = TRUE;
								pclTmp[9] = '\0';
								strcpy(pcpAlid,pclTmp);

								dbg(DEBUG,"Found <%s>",pcpAlid);
							}
						}
						else
						{
							dbg(TRACE,"GetAlidFromAloc: pcpAloc <%s> Field not found", pcpAloc );
						}
					}
				}
			}
		}
		if ( blIgnore )
		{
			int ilLen1, ilLen2;
			ilLen1 = strlen ( cgIgnoreAloc );
			ilLen2 = strlen ( pcpAloc );
			if ( (ilLen2>0) && (ilLen1+ilLen2+1<124) )
			{
				if ( ilLen1 > 0 )
					strcat ( cgIgnoreAloc, "," );
				strcat ( cgIgnoreAloc, pcpAloc );
				dbg ( DEBUG, "GetAlidFromAloc: ALOC <%s> added to cgIgnoreAloc <%s>",
					pcpAloc,  cgIgnoreAloc );
			}
		}
	}
	return ilRc;
}

 
static int CreatePrmDemands(char *pclUPRM)
{

	int ilRc = RC_SUCCESS;
	int ilItemNo = 0;
	int ilCol,ilPos;
	long llRowNum = ARR_FIRST;
	char *pclRudRow = NULL;
	char pclOURI[32] = "";
	char pclOURO[32] = "";
	char pclAlc3I[32] = "";
	char pclAlc3O[32] = "";
	char pclHsnaI[32] = "";
	char pclHsnaO[32] = "";
	char pclTime1[32] = "";
	char pclTime2[32] = "";
	char pclTmpStr[32] = "";
/*	char	*pclUsedRudUrnos = NULL; */
	long	llWorkRows;
	long	llWorkRowsOld;
	long	llCurrent;
	long	llDiff;
	long	llMinDedu;
	time_t	tlDebe;
	time_t	tlDeen;
	char   clAlidsIn[201]="";
	char   clAlidsOut[201]="";
	char   clKey[21], *ps1, *ps2;
	long	llFieldNo = -1;

	/* first delete all old demands **/
	dbg(DEBUG,"%05d:Create PRM-Demands",__LINE__);


	/************ empty FISU list *******************/
/*	memset(pcgUsedRudUrnos,0,lgUsedRudUrnosLength); */

			GetDataItem(pclAlc3I,pcgInbound,igAlc3ItemNo,',',""," \0");
			GetDataItem(pclAlc3O,pcgOutbound,igAlc3ItemNo,',',""," \0");


	dbg(TRACE,"%05d: Inbound %s, Outbound %s Alc3I <%s> HsnaI <%s> Alc3O <%s> HsnaO <%s>",__LINE__,
		pcgInbound ? "TRUE" : "FALSE",
		pcgOutbound ? "TRUE" : "FALSE",
			pclAlc3I,pclHsnaI,pclAlc3O,pclHsnaO);


	if ( pcgInbound )
	{
		GetDataItem(pclOURI,pcgInbound,igAftuItemNo,',',""," \0");
		if (bgUseFilt)
		{
			GetDataItem(pclAlc3I,pcgInbound,igAlc3ItemNo,',',""," \0");
			FindHsna(pclAlc3I,pclHsnaI);
		}
	}
	if (pcgOutbound)
	{
		GetDataItem(pclOURO,pcgOutbound,igAftuItemNo,',',""," \0");
		if (bgUseFilt)
		{
			GetDataItem(pclAlc3O,pcgOutbound,igAlc3ItemNo,',',""," \0");
			FindHsna(pclAlc3O,pclHsnaO);
		}
	}

	CEDAArrayGetRowCount(&rgRudArray.rrArrayHandle,
							rgRudArray.crArrayName,&llWorkRows);
	llWorkRowsOld = llWorkRows + 1; /*that the condition is true at the begin*/

	dbg(DEBUG,"DB DEDU DRTY  TSDB TSDE SUTI SDTI RTDB RTDE TTGT TTGF");

	while ((llWorkRows < llWorkRowsOld) && (llWorkRows > 0))
	{
		llRowNum = ARR_FIRST;
		llWorkRowsOld = llWorkRows;

		/*if (debug_level > TRACE )
			ilRc = SaveIndexInfo( &rgRudArray );*/
		while(CEDAArrayGetRowPointer(&rgRudArray.rrArrayHandle,
										rgRudArray.crArrayName, llRowNum,
										(void *)&pclRudRow) == RC_SUCCESS)
		{
			llRowNum = ARR_NEXT;
			
			if (strncmp(RUDFIELD(pclRudRow,igRudFLAG),"1",1) != 0)
			{
				dbg(DEBUG,
					"%-s  %-6.6s <%-1.1s> %-5.5s%-5.5s%-5.5s%-5.5s%-5.5s%-5.5s%-5.5s%-5.5s",
					RUDFIELD(pclRudRow,igRudDBAR),
					RUDFIELD(pclRudRow,igRudDEDU),
					RUDFIELD(pclRudRow,igRudDRTY),
					RUDFIELD(pclRudRow,igRudTSDB),
					RUDFIELD(pclRudRow,igRudTSDE),
					RUDFIELD(pclRudRow,igRudSUTI),
					RUDFIELD(pclRudRow,igRudSDTI),
					RUDFIELD(pclRudRow,igRudRTDB),
					RUDFIELD(pclRudRow,igRudRTDE),
					RUDFIELD(pclRudRow,igRudTTGT),
					RUDFIELD(pclRudRow,igRudTTGF));

				dbg(DEBUG, "CreateDemands: Next->InitializeNewDemand");	
				InitializeNewDemand(pcgNewDemandBuf);
				dbg(DEBUG, "CreateDemands: Next->InitDemFieldsFromRud");	
				InitDemFieldsFromRud ( pcgNewDemandBuf, pclRudRow );

				strcpy(NEWDEMFIELD(pcgNewDemandBuf,igDemDETY),
						RUDFIELD(pclRudRow,igRudDRTY));

				if (bgUseFilt)
				{
						if (*pclHsnaI != '\0')
						{
							strcpy(NEWDEMFIELD(pcgNewDemandBuf,igDemFILT),pclHsnaI);
						}
						else if (*pclHsnaO != '\0')
						{
							strcpy(NEWDEMFIELD(pcgNewDemandBuf,igDemFILT),pclHsnaO);
						}
				}
		

				/************* get ALID from ALOC *********************/

				/* save inbound and outbound references for current flight in a string
					like "PST:P11,GAT:A11,CIC: ," to avoid multiple calls to GetAlidFromAloc */
				sprintf ( clKey, "%s:", RUDFIELD(pclRudRow,igRudALOC) );
				if ( ( ( *NEWDEMFIELD(pcgNewDemandBuf,igDemDETY) == '1' ) &&
					   (ps1=strstr(clAlidsIn, clKey) )
					 ) || 
					 ( ( *NEWDEMFIELD(pcgNewDemandBuf,igDemDETY) != '1' ) &&
					   (ps1=strstr(clAlidsOut, clKey) )
					 )
				   )
				{
					ps1 += strlen(clKey);
					ps2 = NEWDEMFIELD(pcgNewDemandBuf,igDemALID); 
					while (*ps1 != ',' )
					{
						*ps2 = *ps1;
						ps1 ++;
						ps2 ++;
					}
					*ps2 = '\0';
					dbg ( DEBUG, "CreateDemands: Already calculated  ref. <%s> for key <%s>",
						  NEWDEMFIELD(pcgNewDemandBuf,igDemALID), clKey );
				}
				else
				{
					GetAlidFromAloc(NEWDEMFIELD(pcgNewDemandBuf,igDemALID),
									RUDFIELD(pclRudRow,igRudALOC),
									NEWDEMFIELD(pcgNewDemandBuf,igDemDETY));
					strcat (clKey, NEWDEMFIELD(pcgNewDemandBuf,igDemALID) );
					strcat (clKey, "," );
					if ( *NEWDEMFIELD(pcgNewDemandBuf,igDemDETY) == '1' )
						 strcat (clAlidsIn, clKey ) ;
					else
						 strcat (clAlidsOut, clKey ) ;
				}
				/************* fill Demand times *********************/
				ilRc = GetDebeDeen ( pclRudRow, NEWDEMFIELD(pcgNewDemandBuf,igDemDETY),
									 pclTime1, pclTime2, RUDFIELD(pclRudRow,igRudDEDU) );
				if (ilRc != RC_SUCCESS)
				{
					dbg(TRACE, "CreateDemands: GetDebeDeen failed RC <%d>", ilRc);
					continue;
				}
				else
				{
					strcpy(NEWDEMFIELD(pcgNewDemandBuf,igDemDEBE),pclTime1);
					strcpy(NEWDEMFIELD(pcgNewDemandBuf,igDemDEEN),pclTime2);
					llWorkRows--;
					llCurrent = ARR_CURRENT;
					CEDAArrayPutField(&rgRudArray.rrArrayHandle,
										rgRudArray.crArrayName,&llFieldNo,
										"FLAG",llCurrent,"1");
				}
				strcpy(NEWDEMFIELD(pcgNewDemandBuf,igDemOURI),pclOURI);
				strcpy(NEWDEMFIELD(pcgNewDemandBuf,igDemOURO),pclOURO);
				dbg(DEBUG,"OURI <%s> OURO <%s>",
					NEWDEMFIELD(pcgNewDemandBuf,igDemOURI),
					NEWDEMFIELD(pcgNewDemandBuf,igDemOURO));

				if (bgUseFilt)
				{
						if (*pclHsnaI != '\0')
						{
							strcpy(NEWDEMFIELD(pcgNewDemandBuf,igDemFILT),pclHsnaI);
							dbg(DEBUG,"%05d: HSNA=<%s>",__LINE__,NEWDEMFIELD(pcgNewDemandBuf,igDemFILT));
						}
						else if (*pclHsnaO != '\0')
						{
							strcpy(NEWDEMFIELD(pcgNewDemandBuf,igDemFILT),pclHsnaO);
							dbg(DEBUG,"%05d: HSNA=<%s>",__LINE__,NEWDEMFIELD(pcgNewDemandBuf,igDemFILT));
						}
						else
						{
							dbg(DEBUG,"%05d: HSNA is empty <%s/%s>",__LINE__,pclHsnaI,pclHsnaO);
						}
				}
		
	/*			if (pclUsedRudUrnos != NULL)
				{
					sprintf(pclUsedRudUrnos, ",'%s'",
							RUDFIELD(pclRudRow,igRudURNO));
					pclUsedRudUrnos += strlen(pclUsedRudUrnos) + 1;
				}
				else
				{
					pclUsedRudUrnos = pcgUsedRudUrnos;
					sprintf(pclUsedRudUrnos, "WHERE URNO IN ('%s'",
							RUDFIELD(pclRudRow,igRudURNO));
				} */
				dbg(DEBUG, "New Urno: <%s>",
					NEWDEMFIELD(pcgNewDemandBuf,igDemURNO));
					/* set PRM Urno */
				strcpy(NEWDEMFIELD(pcgNewDemandBuf,igDemUPRM),pclUPRM);

				ilRc = AddNewDemandRow ();
			}
		}
	}

	CEDAArrayGetRowCount(&rgRudArray.rrArrayHandle,
						rgRudArray.crArrayName,&llWorkRowsOld);
	CEDAArrayGetRowCount(&rgNewDemArray.rrArrayHandle,
						rgNewDemArray.crArrayName,&llWorkRows);
	if ( llWorkRowsOld != llWorkRows ) /*that the condition is true at the begin*/
		dbg(TRACE,"CreateDemands: created <%ld> demands for <%ld> RUDs", llWorkRows, llWorkRowsOld );
	else
		dbg(DEBUG,"CreateDemands finished");
	return ilRc;
}


static int FindHsna(char *pcpAlc3,char *pcpHsna)
{
	int ilRc = RC_SUCCESS;
	short slCursor = 0;
	short slFkt = 0;
	char clFilt[24];
	char pclSqlBuf[512];
	char pclAltUrno[24];
	int ilCount;

	dbg(DEBUG,"FindHsna Alc3=<%s>, HandlingTypeForPRM=<%s>",pcpAlc3,pcgHandlingTypeForPRM);
 	if (strlen(pcgHandlingTypeForPRM) > 0 && strlen(pcpAlc3) > 0)
  {
     ilRc = syslibSearchDbData("ALTTAB","ALC3",pcpAlc3,"URNO",pclAltUrno,&ilCount,"\n");
     TrimRight(pclAltUrno);
     sprintf(pclSqlBuf,"SELECT HSNA FROM HAITAB WHERE TASK = '%s' AND ALTU = %s",
             pcgHandlingTypeForPRM,pclAltUrno);
     dbg(DEBUG,"%05d: <%s>",__LINE__,pclSqlBuf);
     slCursor = 0;
     slFkt = START;
     ilRc = sql_if(slFkt,&slCursor,pclSqlBuf,clFilt);
     close_my_cursor(&slCursor);
     if (ilRc == DB_SUCCESS)
		{
        TrimRight(clFilt);
				strcpy(pcpHsna,clFilt);
				dbg(DEBUG,"FindHsna found: <%s>",pcpHsna);
			}
  }
	else
	{
		dbg(TRACE,"invalid configuration: pcgHandlingTypeForPRM <%s>, pcpAlc3 <%s>",
			pcgHandlingTypeForPRM,pcpAlc3);
	}

	return ilRc;
}


static int CreateDemands()
{

	int ilRc = RC_SUCCESS;
	int ilItemNo = 0;
	int ilCol,ilPos;
	long llRowNum = ARR_FIRST;
	char *pclRudRow = NULL;
	char pclOURI[32] = "";
	char pclOURO[32] = "";
	char pclAlc3I[32] = "";
	char pclAlc3O[32] = "";
	char pclHsnaI[32] = "";
	char pclHsnaO[32] = "";
	char pclTime1[32] = "";
	char pclTime2[32] = "";
	char pclTmpStr[32] = "";
/*	char	*pclUsedRudUrnos = NULL; */
	long	llWorkRows;
	long	llWorkRowsOld;
	long	llCurrent;
	long	llDiff;
	long	llMinDedu;
	time_t	tlDebe;
	time_t	tlDeen;
	char   clAlidsIn[201]="";
	char   clAlidsOut[201]="";
	char   clKey[21], *ps1, *ps2;
	long	llFieldNo = -1;

/*	long llDemandRowNum = ARR_CURRENT;
	char *pclTmpBuf;
	char pclFieldName[32];
*/
	/* first delete all old demands **/
	
	dbg(DEBUG,"%05d:Create Demands",__LINE__);

	CEDAArrayDelete(&rgNewDemArray.rrArrayHandle,
					rgNewDemArray.crArrayName);

	dbg(DEBUG,"old new demand-array deleted ");

	/************ empty FISU list *******************/
/*	memset(pcgUsedRudUrnos,0,lgUsedRudUrnosLength); */

			GetDataItem(pclAlc3I,pcgInbound,igAlc3ItemNo,',',""," \0");
			GetDataItem(pclAlc3O,pcgOutbound,igAlc3ItemNo,',',""," \0");
	dbg(TRACE,"%05d: Inbound %s, Outbound %s Alc3I <%s> HsnaI <%s> Alc3O <%s> HsnaO <%s>",__LINE__,
		pcgInbound ? "TRUE" : "FALSE",
		pcgOutbound ? "TRUE" : "FALSE",
			pclAlc3I,pclHsnaI,pclAlc3O,pclHsnaO);


	if ( pcgInbound )
	{
		GetDataItem(pclOURI,pcgInbound,igAftuItemNo,',',""," \0");
		if (bgUseFilt)
		{
			GetDataItem(pclAlc3I,pcgInbound,igAlc3ItemNo,',',""," \0");
			FindHsna(pclAlc3I,pclHsnaI);
		}
	}
	if (pcgOutbound)
	{
		GetDataItem(pclOURO,pcgOutbound,igAftuItemNo,',',""," \0");
		if (bgUseFilt)
		{
			GetDataItem(pclAlc3O,pcgOutbound,igAlc3ItemNo,',',""," \0");
			FindHsna(pclAlc3O,pclHsnaO);
		}
	}

	CEDAArrayGetRowCount(&rgRudArray.rrArrayHandle,
							rgRudArray.crArrayName,&llWorkRows);
	llWorkRowsOld = llWorkRows + 1; /*that the condition is true at the begin*/

	dbg(DEBUG,"DB DEDU DRTY  TSDB TSDE SUTI SDTI RTDB RTDE TTGT TTGF");

	while ((llWorkRows < llWorkRowsOld) && (llWorkRows > 0))
	{
		llRowNum = ARR_FIRST;
		llWorkRowsOld = llWorkRows;

		/*if (debug_level > TRACE )
			ilRc = SaveIndexInfo( &rgRudArray );*/
		while(CEDAArrayGetRowPointer(&rgRudArray.rrArrayHandle,
										rgRudArray.crArrayName, llRowNum,
										(void *)&pclRudRow) == RC_SUCCESS)
		{
			llRowNum = ARR_NEXT;
			
			if (strncmp(RUDFIELD(pclRudRow,igRudFLAG),"1",1) != 0)
			{
				dbg(DEBUG,
					"%-s  %-6.6s <%-1.1s> %-5.5s%-5.5s%-5.5s%-5.5s%-5.5s%-5.5s%-5.5s%-5.5s",
					RUDFIELD(pclRudRow,igRudDBAR),
					RUDFIELD(pclRudRow,igRudDEDU),
					RUDFIELD(pclRudRow,igRudDRTY),
					RUDFIELD(pclRudRow,igRudTSDB),
					RUDFIELD(pclRudRow,igRudTSDE),
					RUDFIELD(pclRudRow,igRudSUTI),
					RUDFIELD(pclRudRow,igRudSDTI),
					RUDFIELD(pclRudRow,igRudRTDB),
					RUDFIELD(pclRudRow,igRudRTDE),
					RUDFIELD(pclRudRow,igRudTTGT),
					RUDFIELD(pclRudRow,igRudTTGF));

				dbg(DEBUG, "CreateDemands: Next->InitializeNewDemand");	
				InitializeNewDemand(pcgNewDemandBuf);
				dbg(DEBUG, "CreateDemands: Next->InitDemFieldsFromRud");	
				InitDemFieldsFromRud ( pcgNewDemandBuf, pclRudRow );

				strcpy(NEWDEMFIELD(pcgNewDemandBuf,igDemDETY),
						RUDFIELD(pclRudRow,igRudDRTY));

				/************* get ALID from ALOC *********************/

				/* save inbound and outbound references for current flight in a string
					like "PST:P11,GAT:A11,CIC: ," to avoid multiple calls to GetAlidFromAloc */
				sprintf ( clKey, "%s:", RUDFIELD(pclRudRow,igRudALOC) );
				if ( ( ( *NEWDEMFIELD(pcgNewDemandBuf,igDemDETY) == '1' ) &&
					   (ps1=strstr(clAlidsIn, clKey) )
					 ) || 
					 ( ( *NEWDEMFIELD(pcgNewDemandBuf,igDemDETY) != '1' ) &&
					   (ps1=strstr(clAlidsOut, clKey) )
					 )
				   )
				{
					ps1 += strlen(clKey);
					ps2 = NEWDEMFIELD(pcgNewDemandBuf,igDemALID); 
					while (*ps1 != ',' )
					{
						*ps2 = *ps1;
						ps1 ++;
						ps2 ++;
					}
					*ps2 = '\0';
					dbg ( DEBUG, "CreateDemands: Already calculated  ref. <%s> for key <%s>",
						  NEWDEMFIELD(pcgNewDemandBuf,igDemALID), clKey );
				}
				else
				{
					GetAlidFromAloc(NEWDEMFIELD(pcgNewDemandBuf,igDemALID),
									RUDFIELD(pclRudRow,igRudALOC),
									NEWDEMFIELD(pcgNewDemandBuf,igDemDETY));
					strcat (clKey, NEWDEMFIELD(pcgNewDemandBuf,igDemALID) );
					strcat (clKey, "," );
					if ( *NEWDEMFIELD(pcgNewDemandBuf,igDemDETY) == '1' )
						 strcat (clAlidsIn, clKey ) ;
					else
						 strcat (clAlidsOut, clKey ) ;
				}
				/************* fill Demand times *********************/
				ilRc = GetDebeDeen ( pclRudRow, NEWDEMFIELD(pcgNewDemandBuf,igDemDETY),
									 pclTime1, pclTime2, RUDFIELD(pclRudRow,igRudDEDU) );
				if (ilRc != RC_SUCCESS)
				{
					dbg(TRACE, "CreateDemands: GetDebeDeen failed RC <%d>", ilRc);
					continue;
				}
				else
				{
					strcpy(NEWDEMFIELD(pcgNewDemandBuf,igDemDEBE),pclTime1);
					strcpy(NEWDEMFIELD(pcgNewDemandBuf,igDemDEEN),pclTime2);
					llWorkRows--;
					llCurrent = ARR_CURRENT;
					CEDAArrayPutField(&rgRudArray.rrArrayHandle,
										rgRudArray.crArrayName,&llFieldNo,
										"FLAG",llCurrent,"1");
				}
				strcpy(NEWDEMFIELD(pcgNewDemandBuf,igDemOURI),pclOURI);
				strcpy(NEWDEMFIELD(pcgNewDemandBuf,igDemOURO),pclOURO);
				dbg(DEBUG,"OURI <%s> OURO <%s>",
					NEWDEMFIELD(pcgNewDemandBuf,igDemOURI),
					NEWDEMFIELD(pcgNewDemandBuf,igDemOURO));

				if (bgUseFilt)
				{
						if (*pclHsnaI != '\0')
						{
							strcpy(NEWDEMFIELD(pcgNewDemandBuf,igDemFILT),pclHsnaI);
							dbg(DEBUG,"%05d: HSNA=<%s>",__LINE__,NEWDEMFIELD(pcgNewDemandBuf,igDemFILT));
						}
						else if (*pclHsnaO != '\0')
						{
							strcpy(NEWDEMFIELD(pcgNewDemandBuf,igDemFILT),pclHsnaO);
							dbg(DEBUG,"%05d: HSNA=<%s>",__LINE__,NEWDEMFIELD(pcgNewDemandBuf,igDemFILT));
						}
						else
						{
							dbg(DEBUG,"%05d: HSNA is empty <%s/%s>",__LINE__,pclHsnaI,pclHsnaO);
						}
				}
		
	/*			if (pclUsedRudUrnos != NULL)
				{
					sprintf(pclUsedRudUrnos, ",'%s'",
							RUDFIELD(pclRudRow,igRudURNO));
					pclUsedRudUrnos += strlen(pclUsedRudUrnos) + 1;
				}
				else
				{
					pclUsedRudUrnos = pcgUsedRudUrnos;
					sprintf(pclUsedRudUrnos, "WHERE URNO IN ('%s'",
							RUDFIELD(pclRudRow,igRudURNO));
				} */
				dbg(DEBUG, "New Urno: <%s>",
					NEWDEMFIELD(pcgNewDemandBuf,igDemURNO));
				ilRc = AddNewDemandRow ();
			}
		}
	}

	CEDAArrayGetRowCount(&rgRudArray.rrArrayHandle,
						rgRudArray.crArrayName,&llWorkRowsOld);
	CEDAArrayGetRowCount(&rgNewDemArray.rrArrayHandle,
						rgNewDemArray.crArrayName,&llWorkRows);
	if ( llWorkRowsOld != llWorkRows ) /*that the condition is true at the begin*/
		dbg(TRACE,"CreateDemands: created <%ld> demands for <%ld> RUDs", llWorkRows, llWorkRowsOld );
	else
		dbg(DEBUG,"CreateDemands finished");
	return ilRc;
}


static int CompareDemands()
{

	int ilRc = RC_SUCCESS, ilRc1;
	long llNewDemRowNum;
	long llOldDemRowNum;
	char pclNewDemUrud[24];
	char pclOldDemUrud[24];
	char pclNewDemUprm[24];
	char pclOldDemUprm[24];
	char pclJfnd[24];
	char pclFlgs[24];
	long llRowCount1, llRowCount2;
	long llCurrent = ARR_CURRENT;
	long llFieldJFND = -1;
	long llFieldSAVE = -1;
	long llFieldFLGS = -1;
	BOOL blActualFound ;
	char tmpOldDemUrno[24];

	*cgFoundMatchingDemands = '\0';

	dbg(DEBUG,"CompareDemands");
	ilRc = ReadJobtab ( &rgOldDemArray );

	/* PRF6094: Workaround, since the problem in AATArrays is not solved	*/
	/* Destroy index before following loop and rebuild it after loop		*/
	ilRc1 = CEDAArrayDestroyIndex( &rgOldDemArray.rrArrayHandle,
								   rgOldDemArray.crArrayName,
								   &rgOldDemArray.rrIdx01Handle,
							       rgOldDemArray.crIdx01Name );
	dbg ( DEBUG, "CompareDemands: CEDAArrayDestroyIndex RC <%d>", ilRc1 );


	llOldDemRowNum = ARR_FIRST;
	while(CEDAArrayGetRowPointer(&rgOldDemArray.rrArrayHandle,
								 rgOldDemArray.crArrayName,llOldDemRowNum,
								 (void *)&rgOldDemArray.pcrArrayRowBuf) == RC_SUCCESS)
	{
		llOldDemRowNum = ARR_NEXT;
		/*
		ilRc = JobExists(OLDDEMFIELD(rgOldDemArray.pcrArrayRowBuf,igDemURNO) );
		*/

		ilRc = SearchJob ( OLDDEMFIELD(rgOldDemArray.pcrArrayRowBuf,igDemURNO) );

		if (ilRc != RC_SUCCESS)
		{
			/*strcpy(OLDDEMFIELD(rgOldDemArray.pcrArrayRowBuf,igDemJFND),"0");*/
			llCurrent = ARR_CURRENT;

			CEDAArrayPutField(&rgOldDemArray.rrArrayHandle,
							  rgOldDemArray.crArrayName,&llFieldJFND,"JFND",llCurrent,"0");
		} 
		else
		{
			char clTmpFLGS[24];


			llCurrent = ARR_CURRENT;
			CEDAArrayPutField(&rgOldDemArray.rrArrayHandle,
							  rgOldDemArray.crArrayName,&llFieldJFND,"JFND",llCurrent,"1");

			/* set FLGS[1] to 1 if old demand is not deleted because job exists */
			CEDAArrayGetField(&rgOldDemArray.rrArrayHandle,
							  rgOldDemArray.crArrayName,&llFieldFLGS,"FLGS",12,llCurrent,clTmpFLGS);
			strcat(clTmpFLGS,"          ");
			if ( clTmpFLGS[6] != '1' )
			{	/* don't mark manually created demands as invalid */
				clTmpFLGS[1] = '1';
				clTmpFLGS[10] = '\0';
				CEDAArrayPutField(&rgOldDemArray.rrArrayHandle,
								  rgOldDemArray.crArrayName,&llFieldFLGS,"FLGS",llCurrent,clTmpFLGS);
			}
			CEDAArrayGetRowPointer(&rgOldDemArray.rrArrayHandle,
								   rgOldDemArray.crArrayName,llCurrent,
								   (void *)&rgOldDemArray.pcrArrayRowBuf);
	
			dbg(DEBUG,"CmpDem:Job found Jfnd=<%s> Flgs=<%s> Urud=<%s> Urno=<%s>",
				OLDDEMFIELD(rgOldDemArray.pcrArrayRowBuf,igDemJFND),
				OLDDEMFIELD(rgOldDemArray.pcrArrayRowBuf,igDemFLGS),
				OLDDEMFIELD(rgOldDemArray.pcrArrayRowBuf,igDemURUD),
				OLDDEMFIELD(rgOldDemArray.pcrArrayRowBuf,igDemURNO) );
		} 
	}
	llNewDemRowNum = ARR_LAST;
	dbg(DEBUG,"All Jobs read");
	CEDAArrayGetRowCount(&rgOldDemArray.rrArrayHandle,rgOldDemArray.crArrayName,&llRowCount1);
	CEDAArrayGetRowCount(&rgNewDemArray.rrArrayHandle,rgNewDemArray.crArrayName,&llRowCount2);

	dbg(TRACE,"CompareDemands: %ld old demands, %ld new demands", llRowCount1, llRowCount2 );

	/* PRF6094: 2nd part of workaround	*/
	ilRc1 = CreateIdx1( &rgOldDemArray, "IDXODEM", "URUD,JFND", "A,D" );
	dbg ( DEBUG, "CompareDemands: CreateIdx1 RC <%d>", ilRc1 );

	while(CEDAArrayGetRowPointer(&rgNewDemArray.rrArrayHandle,
								 rgNewDemArray.crArrayName,llNewDemRowNum,
								 (void *)&rgNewDemArray.pcrArrayRowBuf) == RC_SUCCESS)

	{
		strcpy(pclNewDemUrud,NEWDEMFIELD(rgNewDemArray.pcrArrayRowBuf,igDemURUD));
		TrimRight ( pclNewDemUrud );		
		strcpy(pclNewDemUprm,NEWDEMFIELD(rgNewDemArray.pcrArrayRowBuf,igDemUPRM));
		TrimRight ( pclNewDemUprm );		
		dbg(DEBUG,"CmpDem: Outer Loop: URNO <%s> URUD <%s>  UPRM <%s>", 
				NEWDEMFIELD(rgNewDemArray.pcrArrayRowBuf,igDemURNO), pclNewDemUrud , pclNewDemUprm);

		llOldDemRowNum = ARR_FIRST;
		blActualFound  = FALSE;	

		while(CEDAArrayFindRowPointer(&rgOldDemArray.rrArrayHandle,
									  rgOldDemArray.crArrayName,
									  &rgOldDemArray.rrIdx01Handle,
									  rgOldDemArray.crIdx01Name, pclNewDemUrud,
									  &llOldDemRowNum, 
									  (void *)&rgOldDemArray.pcrArrayRowBuf) == RC_SUCCESS)
		{
			strcpy(pclOldDemUrud,OLDDEMFIELD(rgOldDemArray.pcrArrayRowBuf,igDemURUD));
			dbg(DEBUG,"%05d:CmpDem: Old: URNO <%s> URUD <%s> New: URUD <%s>",	__LINE__,
				OLDDEMFIELD(rgOldDemArray.pcrArrayRowBuf,igDemURNO), 
				pclOldDemUrud, pclNewDemUrud);
			strcpy(pclFlgs, OLDDEMFIELD(rgOldDemArray.pcrArrayRowBuf,igDemFLGS));
			
			/*  don't use manually created demand for comparison with new demands */
			if ( (strlen(pclFlgs)>=7) && pclFlgs[6]=='1' )
			{
				llOldDemRowNum = ARR_NEXT; 
				continue; 
				} 
				strcpy(pclOldDemUprm,OLDDEMFIELD(rgOldDemArray.pcrArrayRowBuf,igDemUPRM));
				TrimRight ( pclOldDemUprm );
				dbg(DEBUG,"CompareDemands: OldPRM <%s>,  NewPRM <%s>",pclOldDemUprm,pclNewDemUprm);
				if (strcmp(pclOldDemUprm,pclNewDemUprm) )
		{
			/* demand is for other PRM, does not match */
				llOldDemRowNum = ARR_NEXT;
				continue;
		}
			/* matching old demand found */
	
		   strcpy(tmpOldDemUrno,OLDDEMFIELD(rgOldDemArray.pcrArrayRowBuf,igDemURNO));
			strcpy(pclJfnd,OLDDEMFIELD(rgOldDemArray.pcrArrayRowBuf,igDemJFND));
			dbg(TRACE, "CmpDem: matching old demand found URNO<%s> Urud=<%s> Jfnd=<%s> Flgs=<%s>",
					tmpOldDemUrno,pclOldDemUrud,pclJfnd,pclFlgs);
	
			/*if ( !blActualFound )*/
			if (strstr(cgFoundMatchingDemands,tmpOldDemUrno) == NULL)
			{
				/* dbg(DEBUG,"UpdateDemand follows Rownum <%ld>", llOldDemRowNum ); */
				UpdateDemand(&rgOldDemArray, llOldDemRowNum, &rgNewDemArray);
				CEDAArrayGetRowPointer(&rgOldDemArray.rrArrayHandle,
						  rgOldDemArray.crArrayName,llOldDemRowNum,
						 (void *)&rgOldDemArray.pcrArrayRowBuf);
				/*strcpy(OLDDEMFIELD(rgOldDemArray.pcrArrayRowBuf,igDemSAVE),"1"); */
				CEDAArrayPutField(&rgOldDemArray.rrArrayHandle,
							  rgOldDemArray.crArrayName,&llFieldSAVE,"SAVE",llOldDemRowNum,"1");
				dbg(DEBUG,"Demand matches.  New  with URNO <%s> will be deleted old with URNO <%s> will be kept",
				    NEWDEMFIELD(rgNewDemArray.pcrArrayRowBuf,igDemURNO), OLDDEMFIELD(rgOldDemArray.pcrArrayRowBuf,igDemURNO));

				llNewDemRowNum = ARR_CURRENT;
				dbg(DEBUG,"Delete New Demand Row follows URNO: <%s> SAVE: <%s> JFND <%s>",
							NEWDEMFIELD(rgNewDemArray.pcrArrayRowBuf,igDemURNO),
							NEWDEMFIELD(rgNewDemArray.pcrArrayRowBuf,igDemSAVE),
							NEWDEMFIELD(rgNewDemArray.pcrArrayRowBuf,igDemJFND));
				ilRc1 = CEDAArrayDeleteRow(&rgNewDemArray.rrArrayHandle,
										  rgNewDemArray.crArrayName,llNewDemRowNum);
				if ( ilRc1 != RC_SUCCESS )
				{
					int ilOldDbgLevel = debug_level;
					debug_level = TRACE;
					dbg ( TRACE, "CompareDemands: CEDAArrayDeleteRow failed RC <%d> URUD <%s> OURI <%s> OURO <%s>",
						  ilRc, pclNewDemUrud, OLDDEMFIELD(rgOldDemArray.pcrArrayRowBuf,igDemOURI), 
						  OLDDEMFIELD(rgOldDemArray.pcrArrayRowBuf,igDemOURO) );
					debug_level = ilOldDbgLevel;
					ilRc = ilRc1;
				}
				/* blActualFound = TRUE;  commented  for TEST */
				strcat(cgFoundMatchingDemands,",");
				strcat(cgFoundMatchingDemands,tmpOldDemUrno);
			}
			else
				dbg(TRACE,"CmpDem: duplicate demand found URNO <%s> URUD <%s> UPRM  <%s> FLGS <%s> JFND <%s> CDAT<%s>", 
					OLDDEMFIELD(rgOldDemArray.pcrArrayRowBuf,igDemURNO), pclOldDemUrud, 
					OLDDEMFIELD(rgOldDemArray.pcrArrayRowBuf,igDemUPRM),
					pclFlgs, pclJfnd, 
					OLDDEMFIELD(rgOldDemArray.pcrArrayRowBuf,igDemCDAT) );
				
			/*break;*/
			llOldDemRowNum = ARR_NEXT;
		}
		llNewDemRowNum = ARR_PREV;
	}	
	dbg(DEBUG,"CompareDemands finished");
	return ilRc;
}


static int SaveFisu()
{

	int ilRc = RC_SUCCESS;
	/* char pclSelection[124]; */
	int ilTemplateNo = 0;
	BOOL blSet = FALSE;

	for( ilTemplateNo = 0; ilTemplateNo < MAXRUES && 
							prgRueUrnos[ilTemplateNo].IsUsed ; ilTemplateNo++)
	{
		if ((*prgRueUrnos[ilTemplateNo].Fisu != '1') &&
			(atol(prgRueUrnos[ilTemplateNo].Urno) != 0))
		{
			blSet |= SetFisuIfNecessary( prgRueUrnos[ilTemplateNo].Urno );
		}
	}
	if ( blSet )
	{
		ilRc = CEDAArrayWriteDB(&rgRueArray.rrArrayHandle, rgRueArray.crArrayName,
								NULL,NULL,NULL,NULL,ARR_COMMIT_ALL_OK);
		if ( ilRc == RC_SUCCESS )
			dbg(DEBUG,"SaveFisu: CEDAArrayWriteDB succeeded!" );
		else
			dbg(TRACE,"SaveFisu: CEDAArrayWriteDB failed RC<%d>", ilRc );
	}
	return ilRc;
}


static int StrToTime(char *pcpTime,time_t *plpTime)
{
	struct tm *_tm;
	time_t now;
	char   _tmpc[6];

	dbg(0,"StrToTime, <%s>",pcpTime);
	if (strlen(pcpTime) < 12 )
	{
		*plpTime = time(0L);
		return RC_FAIL;
	} /* end if */

	now = time(0L);
	_tm = (struct tm *)gmtime(&now);

	_tmpc[2] = '\0';
	_tm -> tm_sec = 0;
	
	strncpy(_tmpc,pcpTime+10,2);
	_tm -> tm_min = atoi(_tmpc);
	strncpy(_tmpc,pcpTime+8,2);
	_tm -> tm_hour = atoi(_tmpc);
	strncpy(_tmpc,pcpTime+6,2);
	_tm -> tm_mday = atoi(_tmpc);
	strncpy(_tmpc,pcpTime+4,2);
	_tm -> tm_mon = atoi(_tmpc)-1;
	strncpy(_tmpc,pcpTime,4);
	_tmpc[4] = '\0';
	_tm -> tm_year = atoi(_tmpc)-1900;
	_tm -> tm_wday = 0;
	_tm -> tm_yday = 0;
	_tm->tm_isdst = -1; /* no adjustment of daylight saving time */
	now = mktime(_tm);
	dbg(0,"StrToTime: %02d.%02d.%04d %02d:%02d Now: %ld",_tm->tm_mday,
_tm->tm_mon+1,_tm->tm_year+1900,_tm->tm_hour,_tm->tm_min,now);
	if (now != (time_t) -1)
	{
		/*now +=  + 3600;*/
		*plpTime = now;
		return RC_SUCCESS;
	}
	*plpTime = time(NULL);
	return RC_FAIL;
}
static int TimeToStr(char *pcpTime,time_t lpTime) 
{                                                                               
	struct tm *_tm;
	
	_tm = (struct tm *)localtime(&lpTime);
	sprintf(pcpTime,"%4d%02d%02d%02d%02d%02d",
			_tm->tm_year+1900,_tm->tm_mon+1,_tm->tm_mday,_tm->tm_hour,
			_tm->tm_min,_tm->tm_sec);
	return (_tm->tm_wday == 0 ? 7 : _tm->tm_wday) + '0';                          
}                                                                               


/*
 *     struct  tm {
 *             int     tm_sec;    seconds after the minute -- [0, 59]  
 *             int     tm_min;    minutes after the hour -- [0, 59] 
 *             int     tm_hour;   hour since midnight -- [0, 23] 
 *             int     tm_mday;   day of the month -- [1, 31] 
 *             int     tm_mon;    months since January -- [0, 11] 
 *             int     tm_year;   years since 1900 
 *             int     tm_wday;   days since Sunday -- [0, 6] 
 *             int     tm_yday;   days since January 1 -- [0, 365] 
 *             int     tm_isdst;  flag for daylight savings time 
 *             long    tm_tzadj;  seconds from GMT (east < 0) 
 *             char    tm_name[LTZNMAX];   name of timezone 
 *     };
 */


void	SunCompStrAddTime(char *pcpBase,char *pcpOffs,char *pcpDest)
{
	strcpy(pcpDest, pcpBase);
	AddSecondsToCEDATime(pcpDest,atoi(pcpOffs),1);
	return ;
}

/*void	StrAddTime(char *pcpBase,char *pcpOffs,char *pcpDest)
{

	time_t ilTime;

	StrToTime(pcpBase,&ilTime);
	ilTime += (time_t)(atoi(pcpOffs));
	TimeToStr(pcpDest,ilTime);

	return ;
}*/ /* end of StrAddTime */



static int SendToSqlHdl(char *pcpObject,char *pcpSelection,
			char *pcpFields,char *pcpData)
{
	int			ilRC;
	BC_HEAD		rlBCHead;
	CMDBLK		rlCmdblk;

	dbg(DEBUG,"<SCAFC> ----- START -----");

	/* set BC-Head members */
	memset((void*)&rlBCHead, 0x00, sizeof(BC_HEAD));
	rlBCHead.rc = RC_SUCCESS;
	strncpy(rlBCHead.dest_name, cgProcessName, 10);
	strncpy(rlBCHead.dest_name, "DEMHDL", 10);
	strncpy(rlBCHead.recv_name, "DEMHDL", 10);

	/* set CMDBLK members */
	memset((void*)&rlCmdblk, 0x00, sizeof(CMDBLK));
	strncpy(rlCmdblk.command, "URT", 6);
	strncpy(rlCmdblk.obj_name, pcpObject, 10);
	sprintf(rlCmdblk.tw_end, "%s,%s,%s", cgHopo,cgTabEnd, cgProcessName);

	/* write request to QUE */
	if ((ilRC = SendToQue(igRouter, PRIORITY_4, &rlBCHead, &rlCmdblk,
			pcpSelection,pcpFields,pcpData)) != RC_SUCCESS)
	{
		dbg(TRACE,"%05d SendToQue returns <%d>", __LINE__, ilRC);
		dbg(DEBUG," ----- END -----");
		return ilRC;
	}
		
	dbg(DEBUG,"<SCAFC> ----- END -----");

	/* bye bye */
	return RC_SUCCESS;
}

/******************************************************************************/
/* the SendToQue routine                                                      */
/******************************************************************************/
static int SendToQue(int ipModID, int ipPrio, BC_HEAD *prpBCHead, 
							CMDBLK *prpCmdblk, char *pcpSelection, 
							char *pcpFields, char *pcpData)
{
	int			ilRC;
	int			ilLen;
	EVENT			*prlOutEvent 	= NULL;
	BC_HEAD		*prlOutBCHead 	= NULL;
	CMDBLK		*prlOutCmdblk	= NULL;

	dbg(DEBUG,"<SendToQue> ----- START -----");

	/* calculate size of memory we need */
	ilLen = sizeof(EVENT) + sizeof(BC_HEAD) + sizeof(CMDBLK) + 
			 strlen(pcpSelection) + strlen(pcpFields) + strlen(pcpData) + 20; 

	/* get memory for out event */
	if ((prlOutEvent = (EVENT*)malloc((size_t)ilLen)) == NULL)
	{
		dbg(TRACE,"<SendToQue> %05d malloc failed, can't allocate %d bytes", __LINE__, ilLen);
		dbg(DEBUG,"<SendToQue> ----- END -----");
		return RC_FAIL;
	}

	/* clear buffer */
	memset((void*)prlOutEvent, 0x00, ilLen);

	/* set structure members */
	prlOutEvent->type 		 = SYS_EVENT;
	prlOutEvent->command 	 = EVENT_DATA;
	prlOutEvent->originator  = mod_id;
	prlOutEvent->retry_count = 0;
	prlOutEvent->data_offset = sizeof(EVENT);
	prlOutEvent->data_length = ilLen - sizeof(EVENT);

	/* BCHead members */
	prlOutBCHead = (BC_HEAD*)((char*)prlOutEvent+sizeof(EVENT));
	memcpy(prlOutBCHead, prpBCHead, sizeof(BC_HEAD));

	/* CMDBLK members */
	prlOutCmdblk = (CMDBLK*)((char*)prlOutBCHead->data);
	memcpy(prlOutCmdblk, prpCmdblk, sizeof(CMDBLK));

	/* Selection */
	strcpy(prlOutCmdblk->data, pcpSelection);

	/* fields */
	strcpy(prlOutCmdblk->data+strlen(pcpSelection)+1, pcpFields);

	/* data */
	strcpy(prlOutCmdblk->data+strlen(pcpSelection)+strlen(pcpFields)+2, pcpData);

	/* send this message */
	dbg(DEBUG,"<SendToQue> sending to Mod-ID: %d", ipModID);
	if ((ilRC = que(QUE_PUT, ipModID, mod_id, ipPrio, ilLen, (char*)prlOutEvent)) != RC_SUCCESS)
	{
		dbg(TRACE,"<SendToQue> %05d QUE_PUT returns: %d", __LINE__, ilRC);
		return RC_FAIL;
	}

	/* delete memory */
	free((void*)prlOutEvent);

	dbg(DEBUG,"<SendToQue> ----- END -----");

	/* bye bye */
	return RC_SUCCESS;
}


int check_ret1(int ipRc,int ipLine)
{
	int ilOldDebugLevel;

	ilOldDebugLevel = debug_level;

	debug_level = TRACE;

	dbg(TRACE,"DB-ERROR in Line: %d",ipLine);
	debug_level  = ilOldDebugLevel;
	return check_ret(ipRc);
}

static int SendAnswer(EVENT *prpEvent,int ipRc)
{
	int	      ilRc           = RC_SUCCESS;			/* Return code */

	BC_HEAD *prlBchead       = NULL;
	CMDBLK  *prlCmdblk       = NULL;
	char    *pclSelection    = NULL;
	char    *pclFields       = NULL;
	char    *pclData         = NULL;

	prlBchead    = (BC_HEAD *) ((char *)prpEvent + sizeof(EVENT));
	prlCmdblk    = (CMDBLK *)  ((char *)prlBchead->data);
	pclSelection = prlCmdblk->data;
	pclFields    = pclSelection + strlen(pclSelection) + 1;
	pclData      = pclFields + strlen(pclFields) + 1;

	prlBchead->rc = ipRc;

	dbg(DEBUG,"SendAnswer to: %d",prpEvent->originator);
	if ((ilRc=SendToQue(prpEvent->originator,
				PRIORITY_4, prlBchead, prlCmdblk,
						pclSelection,pclFields,pclData)) != RC_SUCCESS)
			{
				dbg(DEBUG,"%05d SendToQue returns <%d>", __LINE__, ilRc);
				dbg(DEBUG," ----- END -----");
			}
			else
			{
				dbg(DEBUG,"%05d SendToQue (%d) returns <%d>", __LINE__,
							prpEvent->originator,ilRc);
			}

	return ilRc;
}

static int UpdateDemandRecord(char *pcpFields,char *pcpData)
{
	int ilRc = RC_SUCCESS;
	int ilLc;
	int ilItemCount;
	int ilUrnoItem;
	char clFieldName[33];
	char clSqlBuf[1200];
	char clTmpSql[1200];
	char clDataBuf[4096];
	char clData[4096];
	char *pclNewline;
	char *pclLine = NULL;
	char clKomma = ' ';
	short slCursor;
	short slSqlFunc;
	int ilItemNo = 1;
	char clTwEnd[33];
	char clTana[33];
	char clSelection[101];
	

	ilItemCount = get_no_of_items(pcpFields);

	dbg(DEBUG,"UpdateDemandRecord itemCount = %d",ilItemCount);	
	if (ilItemCount <= 1)
	{
		ilRc = RC_FAIL;
	}

	if (ilRc == RC_SUCCESS)
	{
		ilRc = RC_FAIL;
		for(ilLc = 1; ilLc <= ilItemCount && ilRc != RC_SUCCESS; ilLc++)
		{
			get_real_item(clFieldName,pcpFields,ilLc);
			if(!strcmp(clFieldName,"URNO"))
			{
					ilUrnoItem = ilLc;
					ilRc = RC_SUCCESS;
			}
		}
	}

	if (ilRc == RC_SUCCESS)
	{
		sprintf(clSqlBuf,"UPDATE DEM%s SET ",cgTabEnd);

		ilItemNo = 1;
		for(ilLc = 1; ilLc <= ilItemCount && ilRc == RC_SUCCESS; ilLc++)
		{
			if (ilLc != ilUrnoItem)
			{
				get_real_item(clFieldName,pcpFields,ilLc);
				if(ilItemNo > 1)
				{
					clKomma = ',';
				}
				ilItemNo++;
				sprintf(clTmpSql,"%c%s=:V%s ",clKomma,clFieldName,clFieldName);
				strcat(clSqlBuf,clTmpSql);
			}
		}
		strcat(clSqlBuf," WHERE URNO = :VURN");

		dbg(DEBUG,"SqlBuf: <%s> RC=%d",clSqlBuf,ilRc);	

		pclLine = pcpData;
	
		slCursor = 0;
		while(pclLine != NULL && ilRc == RC_SUCCESS)
		{
			char clTmpData[1024];
			char clTmpDataBuf[1024];

			*clDataBuf = '\0';

			for(ilLc = 1; ilLc <= ilItemCount && ilRc == RC_SUCCESS; ilLc++)
			{
				if (ilLc != ilUrnoItem)
				{
					get_real_item(clTmpData,pclLine,ilLc);
					pclNewline = strchr(clTmpData,'\n');
					if (pclNewline != NULL)
					{
						*pclNewline = '\0';
					}
					sprintf(clTmpDataBuf,"%s,",clTmpData);
					strcat(clDataBuf,clTmpDataBuf);
				}
			}
			get_real_item(clTmpData,pclLine,ilUrnoItem);
			strcat(clDataBuf,clTmpData);
			dbg(DEBUG,"Data: <%s>",clDataBuf);

			slSqlFunc = START;
			strcpy(clData,clDataBuf);
			delton(clData);
			ilRc = sql_if(slSqlFunc,&slCursor,clSqlBuf,clData);
			if (ilRc == NOTFOUND)
			{
				dbg(TRACE,"%05d: ilRc=%d Record not found <%s>",__LINE__,
					ilRc,clTmpData);
				ilRc = RC_SUCCESS;
			}
			else if (ilRc != RC_SUCCESS)
			{
				dbg(TRACE,
					"%05d: ilRc=%d Error writing DEMTAB <%s> <%s>",__LINE__,
					ilRc, clSqlBuf, clTmpData);
				
			}
			else
			{
				sprintf (clTwEnd, "%s,%s,%s", cgHopo,cgTabEnd, cgProcessName);
				sprintf (clTana, "DEM%s", cgTabEnd );
				sprintf( clSelection, "WHERE URNO='%s'", clTmpData );

				ilRc =  tools_send_info_flag (1900, 0, "BCHDL", "DEMHDL", "DEMHDL", "", "", "", clTwEnd, "URT", clTana, clSelection, pcpFields, pcpData, 0 );
				if ( ilRc == RC_SUCCESS )
					dbg ( DEBUG, "UpdateDemandRecord: Send Broadcast ok!" );
				else
				{
					dbg ( TRACE, "UpdateDemandRecord: Send Broadcast failed, RC <%d>", ilRc );
					ilRc = RC_SUCCESS;	/* to go on even if broadcast was not sucessfull */
				}
				if ( igActionId > 0 )
				{
					ilRc =  tools_send_info_flag (igActionId, 0, "ACTION", "DEMHDL", "DEMHDL", "", "", "", clTwEnd, "URT", clTana, clSelection, pcpFields, pcpData, 0 );
					if ( ilRc == RC_SUCCESS )
						dbg ( DEBUG, "UpdateDemandRecord: Sending to action ok!" );
					else
					{
						dbg ( TRACE, "UpdateDemandRecord: Sending to action failed, RC <%d>", ilRc );
						ilRc = RC_SUCCESS;	/* to go on even if sending to action was not sucessfull */
					}
				}
			/*****************/
			}

			pclLine = strchr(pclLine,'\n');
			if (pclLine != NULL)
			{
				pclLine++;
				TrimRight(pclLine);
				if (!*pclLine)
				{
					pclLine = NULL;
				}
			}
		}
		commit_work();
		close_my_cursor(&slCursor);
	}
	return ilRc;
}



static void TrimRight(char *s)
{    /* search for last non-space character */
    int i = 0;
    for (i = strlen(s) - 1; i >= 0 && isspace(s[i]); i--);
	s[++i] = '\0'; /* trim off right spaces */    
}

static int CCIDemands(char *pcpData)
{
	char	*pclData = NULL;
	int		ilRc = RC_SUCCESS;
	short	slStep;
	long	llNoOfSteps;
	long	llMaxOfDem = 0;
	long	llRudRow = ARR_FIRST;
	char	*pclResult = NULL;
	char	*pclRudResult = NULL;
	char	clKeyRud[HOPOLEN+1+URNOLEN+1];
	char	clKeyDem[URNOLEN+1];
	long	llRow = ARR_FIRST;
	long	llRowCount = 0;
	time_t	tlHelpStart = 0,tlHelpEnd = 0;
	int		ilRc1, ilRc2;

	dbg(DEBUG,"Command UCD arrived");

	/*dbg(DEBUG,"Data %s",pcpData);*/
	pcgRudUrno = pcpData;
	pcgIvStart = strchr(pcgRudUrno,'\n');
	*pcgIvStart = '\0';
	pcgIvStart++;
	pcgIvEnd = strchr(pcgIvStart,'\n');
	*pcgIvEnd = '\0';
	pcgIvEnd++;
	pcgStep = strchr(pcgIvEnd,'\n');
	*pcgStep = '\0';
	pcgStep++;
	pclData = strchr(pcgStep,'\n');


	sprintf(clKeyRud,"%s",pcgRudUrno);
	dbg(DEBUG,"Key for RUD %s\tStart of interval %s\tEnd of interval %s",
		clKeyRud,pcgIvStart,pcgIvEnd);

	ilRc = CEDAArrayFindRowPointer(&rgCCIRudArray.rrArrayHandle,
									rgCCIRudArray.crArrayName,
									&rgCCIRudArray.rrIdx01Handle,
									rgCCIRudArray.crIdx01Name,clKeyRud,
									&llRudRow,(void**)&pclRudResult);
	if (ilRc != RC_SUCCESS)
	{
		dbg(TRACE,"CEDAArrayFindRowPointer failed with RC %d",ilRc);
		return ilRc;
	}

	if (strncmp(CCIRUDFIELD(pclRudResult, igRudDIDE), "1", 1) == 0)
	{
		igMinDemand = atoi(CCIRUDFIELD(pclRudResult,igRudMIND));
		igMaxDemand = atoi(CCIRUDFIELD(pclRudResult,igRudMAXD));
	}
	else
	{
		igMinDemand = 1;
		igMaxDemand = 0;
	}

	if (igMinDemand <= 0)
		igMinDemand = 1;
	if (igMaxDemand < 0)
		igMaxDemand = 0;

	dbg(DEBUG, "Dide: <%s>\tminimal demandlen: %d\tmaximal demandlen: %d",
		CCIRUDFIELD(pclRudResult, igRudDIDE), igMinDemand, igMaxDemand);

	StrToTime(pcgIvStart,&tgIvStart);
	StrToTime(pcgIvEnd,&tgIvEnd);

	ilRc = ReadCCIDemands(pcgRudUrno, CCIRUDFIELD(pclRudResult, igRudLSTU));

	slStep = atoi(pcgStep) * 60;

	AlignCCIToStep ( slStep );

	llNoOfSteps = ceil((float)(tgIvEnd - tgIvStart) / slStep);

	sprintf(clKeyDem,"%s",pcgRudUrno);
	dbg ( DEBUG, "CCIDemands: Key for searching old demands <%s>", clKeyDem );
	while( CEDAArrayFindRowPointer(&rgCCIDemArray.rrArrayHandle,
									rgCCIDemArray.crArrayName,
									&rgCCIDemArray.rrIdx01Handle,
									rgCCIDemArray.crIdx01Name,clKeyDem,
									&llRow,(void**)&pclResult) == RC_SUCCESS)
	{
		llRow = ARR_NEXT;
		llRowCount++;
	}

	if (llRowCount == 0)
	{
		dbg(DEBUG,"CCIDemands: No old demands for this interval");
		bgOldDemandsEx = FALSE;
	}
	else
	{
		dbg(DEBUG,"CCIDemands: %ld old demands for this interval", llRowCount );
		bgOldDemandsEx = TRUE;
	}

	CompareCCIDemands(slStep,llNoOfSteps,pclData,&llMaxOfDem);

	igNwD = 0;
	if ( llNoOfSteps * llMaxOfDem > 0 )
	{
		prgNwD = (NWDARRAY*)malloc( llNoOfSteps * llMaxOfDem * sizeof(NWDARRAY) );
		if ( !prgNwD )
		{
			dbg(TRACE,"CCIDemands: not enough memory for existing demands");
			return RC_FAIL;
		}
		else
		{
			memset( prgNwD, 0, llNoOfSteps * llMaxOfDem * sizeof(NWDARRAY) );
		}
	}
	if (llMaxOfDem)
	{
		BuildingNewCCIDemands(llNoOfSteps,llMaxOfDem);
		DbgCciDemArray ( DEBUG, "IST", clKeyRud );
		if (bgOldDemandsEx == TRUE)
		{
			ExtendingOldCCIDemands();
			do
			{
				ilRc1 = DeletingOldCCIDemands();
				ilRc2 = CuttingOldCCIDemands();
				dbg ( TRACE, "CCIDemands: Delete RC <%d> Cut RC <%d>", ilRc1, ilRc2 );
				ilRc1 = (ilRc1&&ilRc2);
				/* repeat while anything had been deleted or rests could be combined */
			} while ( ( CombineNwdArray ( DELETE ) > 0 ) || (ilRc1==RC_SUCCESS) ); 
			SplittingOldCCIDemands();
		}
	}
	/*  took call to WritingNewCCIDemands out of if-block, because 
		WritingNewCCIDemands could have been modified in CompareCCIDemands	*/
	WritingNewCCIDemands(pclRudResult);


	if ( prgNwD )
		free ( prgNwD );
	prgNwD = NULL;
	igNwD = 0;
	/*SetFisu(pclRudResult);*/

	return ilRc;
}

static int ReadCCIDemands(char *pcpRudUrno, char *pcpLstu)
{
	int ilRc = RC_SUCCESS;
	char pclSelection[512];
	char pclTmpBuf[124];
	char pclAddData[256];
	long llAction = ARR_FIRST;

	dbg(DEBUG,"Reading old demands");

	*pclSelection = '\0';

	sprintf(pclSelection,"WHERE URUD = '%s' AND ((DEBE BETWEEN '%s' AND '%s')",
			pcgRudUrno, pcgIvStart, pcgIvEnd);

	sprintf(pclTmpBuf,"OR (DEEN BETWEEN '%s' AND '%s')) ", pcgIvStart,
			pcgIvEnd);
	strcat(pclSelection,pclTmpBuf);

	sprintf(pclTmpBuf,"AND DETY = '6' AND SUBSTRB(FLGS, 2, 1) <> '1'");
	strcat(pclSelection,pclTmpBuf);

	dbg(DEBUG,"ReadCCIDemands Selection: <%s>",pclSelection);

	if (*pclSelection)
	{
		ilRc = CEDAArrayRefill(&rgCCIDemArray.rrArrayHandle,
								rgCCIDemArray.crArrayName,pclSelection,
								pclAddData,llAction);
		ReadJobtab ( &rgCCIDemArray );
	}
	
	if (strlen(pcpLstu) == DATELEN)
		RemoveDemands(pcpLstu);

	if (ilRc != RC_SUCCESS)
		dbg(DEBUG,"ReadCCIDemands: CEDAArrayRefill failed with RC=%d",ilRc);

	return ilRc;
}

static int CompareCCIDemands(short spStep, long lpNoOfSteps, char *pcpData,
							 long *plpMaxOfDem)
{
	time_t	tlDemStart,tlDemEnd;
	int		i;
	long	llRow = ARR_FIRST;
	char	*pclResult = 0;
	char	clNoOfDemStr[3+1];
	char	clKeyDem[URNOLEN+1];
	long	llNoOfDem;
	BOOL    blDeleteAll = TRUE;

	dbg(DEBUG,"Compare old and new demands within interval");

	prgExD = (EXDARRAY*)malloc( lpNoOfSteps * sizeof(EXDARRAY) ) ;	
	if ( !prgExD )
	{
		dbg( TRACE,"CompareCCIDemands: not enough memory for existing demands");
		return RC_FAIL;
	}
	else
	{
		memset( prgExD, 0, lpNoOfSteps * sizeof(EXDARRAY) );
	}

	for( i = 0; i < lpNoOfSteps; i++)
	{
		prgExD[i].lrStartInSec = tgIvStart + i * spStep;
		prgExD[i].frSumOfDem = 0;
	}

	sprintf(clKeyDem,"%s",pcgRudUrno);
	while( CEDAArrayFindRowPointer(&rgCCIDemArray.rrArrayHandle,
									rgCCIDemArray.crArrayName,
									&rgCCIDemArray.rrIdx01Handle,
									rgCCIDemArray.crIdx01Name,clKeyDem,
									&llRow,(void**)&pclResult) == RC_SUCCESS)
	{
		StrToTime(CCIDEMFIELD(pclResult,igDemDEBE),&tlDemStart);
		StrToTime(CCIDEMFIELD(pclResult,igDemDEEN),&tlDemEnd);

		for( i = 0; i < lpNoOfSteps; i++)
		{
			if ((tlDemStart < (prgExD[i].lrStartInSec + spStep)) &&
				(tlDemEnd > prgExD[i].lrStartInSec))
			{
				if (tlDemEnd < (prgExD[i].lrStartInSec + spStep))
				{
					if (tlDemStart >= prgExD[i].lrStartInSec)
						prgExD[i].frSumOfDem += (float)(tlDemEnd - tlDemStart)
																	 / spStep;
					else
						prgExD[i].frSumOfDem +=
									 (float)(tlDemEnd - prgExD[i].lrStartInSec)
																	 / spStep;
					dbg(DEBUG,"Summe im if %f fuer i = %d",
						prgExD[i].frSumOfDem,i);
				}
				else
				{
					if (tlDemStart >= prgExD[i].lrStartInSec)
						prgExD[i].frSumOfDem +=
						(float)(prgExD[i].lrStartInSec + spStep - tlDemStart)
																	 / spStep;
					else
						prgExD[i].frSumOfDem += 1;
				}
			}
		}
		llRow = ARR_NEXT;
	}

	for( i = 0; i < lpNoOfSteps; i++)
	{
		GetDataItem(clNoOfDemStr,pcpData,i+1,'|',"","\0 ");
		llNoOfDem = atol(clNoOfDemStr);
		if ( llNoOfDem > 0 )
			blDeleteAll = FALSE;
		prgExD[i].lrDiffOfDem = ceil(llNoOfDem - prgExD[i].frSumOfDem);
		if(fabs(prgExD[i].lrDiffOfDem) > *plpMaxOfDem)
			*plpMaxOfDem = fabs(prgExD[i].lrDiffOfDem);
	}

	if  ( blDeleteAll )
	{
		dbg ( TRACE, "CompareCCIDemands: All existing demands to be deleted -> igMinDemand = 1; ");
		igMinDemand = 1;
	}
	return RC_SUCCESS;
}

static int WritingNewCCIDemands(char *pcpResult)
{
	int		i = 0, ilTouched=0, ilModified;
	int		ilRc = RC_SUCCESS;
	long	llRow = ARR_FIRST;
	long	llFieldNo;
	time_t	tlStart,tlEnd, tlMaxEnd, tlMinEnd;
	long	llDuration, llBytes;
	char	clTwEnd[33], clLists[11];
	char	*pclData = 0;
	
	dbg(DEBUG,"Writing new demands");

	for( i = 0; i < igNwD; i++ )
	{
		dbg(DEBUG, "igNwD = %d", igNwD);
		if (prgNwD[i].srState == NEWDEM)
		{
			InitializeCCIDemand(pcgCCIDemBuf);

			/*StrToTime(prgNwD[i].crStartDemand,&tlStart);
			StrToTime(prgNwD[i].crEndDemand,&tlEnd);*/
			tlStart = prgNwD[i].lrStartInSec;
			tlEnd = prgNwD[i].lrEndInSec;

			llDuration = tlEnd - tlStart;
			if ((igMinDemand > 0) && (llDuration < igMinDemand))
			{/* the demand have to be extended, if it's too short */
				if ( igMinDemand < 10 )
				{
					dbg ( DEBUG, "WritingNewCCIDemands, extending demand from <%ld> to <%d>", llDuration, igMinDemand);
					dbg ( DEBUG, "WritingNewCCIDemands, extending original DEBE <%s> DEEN <%s>", prgNwD[i].crStartDemand, prgNwD[i].crEndDemand);
				}
				TimeToStr(prgNwD[i].crEndDemand,(tlStart + igMinDemand));
				prgNwD[i].lrEndInSec = tlStart + igMinDemand;
				llDuration = igMinDemand;
/* maybe some demands are coincided with the extension */
				/*
				if (igMaxDemand)
					tlMaxEnd = tlStart + igMaxDemand;
				else
					tlMaxEnd = 0;
				IntegrateOthers(tlEnd, tlStart+igMinDemand, tlMaxEnd);*/
			}
			
			if (igMaxDemand)
				tlMaxEnd = tlStart + igMaxDemand;
			else
				tlMaxEnd = 0;
			tlMinEnd = max ( tlStart+igMinDemand, tlStart+llDuration );
		
			dbg ( DEBUG, "Before JoinOthers: Start <%s> End <%s>", 
				  prgNwD[i].crStartDemand, prgNwD[i].crEndDemand );
			
			JoinOthers(&tlEnd, tlMinEnd, tlMaxEnd);
			tlEnd = max ( tlEnd, tlMinEnd );
			llDuration = tlEnd - tlStart;
			TimeToStr(prgNwD[i].crEndDemand, tlEnd );
			prgNwD[i].lrEndInSec = tlEnd;

			dbg ( DEBUG, "After JoinOthers:  Start <%s> End <%s>", 
				  prgNwD[i].crStartDemand, prgNwD[i].crEndDemand );
				
			if ((igMaxDemand > 0) && (llDuration > igMaxDemand))
			{/* the demand has to be splitted, if it's too long */
				strcpy(prgNwD[igNwD].crEndDemand,prgNwD[i].crEndDemand);
				prgNwD[igNwD].lrEndInSec = prgNwD[i].lrEndInSec;

				TimeToStr(prgNwD[igNwD].crStartDemand,(tlStart+igMaxDemand));
				prgNwD[igNwD].lrStartInSec = tlStart+igMaxDemand;

/*TODO : try to use the half length for both*/
				prgNwD[igNwD].srState = NEWDEM;
				if ( strcmp ( prgNwD[igNwD].crStartDemand, prgNwD[igNwD].crEndDemand ) >= 0 )
					dbg ( DEBUG, "WritingNewCCIDemands, splitting demand -> new neg. demand: Begin <%s> End <%s>", prgNwD[igNwD].crStartDemand, prgNwD[igNwD].crEndDemand  );

				igNwD++;
				llDuration = igMaxDemand;
				TimeToStr(prgNwD[i].crEndDemand,(tlStart+igMaxDemand));
				prgNwD[i].lrEndInSec = tlStart+igMaxDemand;
			}

			strcpy(CCIDEMFIELD(pcgCCIDemBuf,igDemDEBE),prgNwD[i].crStartDemand);
			strcpy(CCIDEMFIELD(pcgCCIDemBuf,igDemDEEN),prgNwD[i].crEndDemand);
			sprintf(CCIDEMFIELD(pcgCCIDemBuf,igDemDEDU),"%ld",llDuration);

			strcpy(CCIDEMFIELD(pcgCCIDemBuf,igDemRETY),
					CCIRUDFIELD(pcpResult,igRudRETY));
			strcpy(CCIDEMFIELD(pcgCCIDemBuf,igDemURUD),
					CCIRUDFIELD(pcpResult,igRudURNO));
			if ( (igRudUSES>0) && (igDemUSES>0) )
				strcpy( CCIDEMFIELD(pcgCCIDemBuf,igDemUSES),
						CCIRUDFIELD(pcpResult,igRudUSES));
			if ( (igRudUTPL>0) && (igDemUTPL>0) )
				strcpy( CCIDEMFIELD(pcgCCIDemBuf,igDemUTPL),
						CCIRUDFIELD(pcpResult,igRudUTPL));

			if ( (igRudRECO>0) && (igDemRECO>0) )
				strcpy( CCIDEMFIELD(pcgCCIDemBuf,igDemRECO),
						CCIRUDFIELD(pcpResult,igRudRECO));
			if ( (igRudQUCO>0) && (igDemQUCO>0) )
				strcpy( CCIDEMFIELD(pcgCCIDemBuf,igDemQUCO),
						CCIRUDFIELD(pcpResult,igRudQUCO));
			if ( (igRudUGHS>0) && (igDemUGHS>0) )
				strcpy( CCIDEMFIELD(pcgCCIDemBuf,igDemUGHS),
						CCIRUDFIELD(pcpResult,igRudUGHS));

			ilRc =	AddNewCCIDemandRow ();
			/*llRow = ARR_FIRST;
			ilRc =	CEDAArrayAddRow(&rgCCIDemArray.rrArrayHandle,
									rgCCIDemArray.crArrayName,
									&llRow,(void *)pcgCCIDemBuf);*/
			if ( ilRc != RC_SUCCESS)
				dbg(DEBUG, "WritingNewCCIDemands: AddNewCCIDemandRow failed RC <%d>", ilRc);
		}
	}
	if ( prgDemInf )
		prgDemInf->AppendData = FALSE;
	ilRc = AATArraySaveChanges(&rgCCIDemArray.rrArrayHandle, rgCCIDemArray.crArrayName, 
								&prgDemInf, ARR_COMMIT_ALL_OK);

	ilTouched = prgDemInf->DataList[IDX_IRTU].ValueCount + 
				prgDemInf->DataList[IDX_URTU].ValueCount +
				prgDemInf->DataList[IDX_DRTU].ValueCount; 
	dbg ( TRACE, "WritingNewCCIDemands: AATArraySaveChanges <CCIDEM>, <%d> records touched, RC <%d>", 
			ilTouched, ilRc );
	if ( bgSendSBC && (ilRc==RC_SUCCESS) && (ilTouched>0) )
	{	/*  Send UPDDEM only as TCP/IP-BC, UDP clients get RELDEM as usual */
		dbg(DEBUG,"WritingNewCCIDemands: Preparing SBC info, %d demands saved", ilTouched );
		sprintf (clTwEnd, "%s,%s,%s", cgHopo,cgTabEnd, cgProcessName);
		/* calculate min(DEBE) and max(DEEN) of all Demands */
		llBytes = prgDemInf->DataList[IDX_IRTU].UsedSize + 
				  prgDemInf->DataList[IDX_URTU].UsedSize +
				  prgDemInf->DataList[IDX_DRTU].UsedSize + 60;
		pclData = malloc ( llBytes );
		if ( pclData )
		{
			dbg(DEBUG,"WritingNewCCIDemands: Preparing new broadcast" );
			/*	Form: "Date1,Date2,DEL:n,URNO1,..,URNOn,UPD:m,URNO1,..,URNOm"	*/
			ilModified = prgDemInf->DataList[IDX_IRTU].ValueCount + 
						 prgDemInf->DataList[IDX_URTU].ValueCount;
			if ( prgDemInf->DataList[IDX_DRTU].ValueCount > 0 )
				sprintf ( pclData, "%s,%s,DEL:%d,%s,UPD:%d", pcgIvStart, pcgIvEnd, 
							  prgDemInf->DataList[IDX_DRTU].ValueCount, 
							  prgDemInf->DataList[IDX_DRTU].ValueList, ilModified );
			else
				sprintf ( pclData, "%s,%s,DEL:0,UPD:%d", pcgIvStart, pcgIvEnd, ilModified );
						  
			if ( prgDemInf->DataList[IDX_IRTU].ValueCount > 0 )
			{
				strcat ( pclData, "," );
				strcat ( pclData, prgDemInf->DataList[IDX_IRTU].ValueList );
			}
			if ( prgDemInf->DataList[IDX_URTU].ValueCount > 0 )
			{
				strcat ( pclData, "," );
				strcat ( pclData, prgDemInf->DataList[IDX_URTU].ValueList );
			}
			strcat ( pclData, "\n{=NOUDP=}" );
			ilRc = AATArraySetSbcInfo(rgCCIDemArray.crArrayName, prgDemInf, "1900", 
									  "3", "SBC", "UPDDEM", "CEDA", "DEMHDL", "", 
									  clTwEnd, "", "DEBE,DEEN,DEL,UPD", pclData );
		}
		else
		{
			dbg(TRACE,"WritingNewCCIDemands: malloc of <%ld> bytes for SBC failed", llBytes );
			ilRc = AATArraySetSbcInfo(rgCCIDemArray.crArrayName, prgDemInf, "1900", 
									  "3", "SBC", "UPDDEM", "CEDA", "DEMHDL", "", 
									  clTwEnd, "", "", "{=NOUDP=}" );
		}

		if ( ilRc != RC_SUCCESS )
			dbg ( TRACE, "WritingNewCCIDemands: AATArraySetSbcInfo failed RC <%d>", ilRc );
		else
		{
			sprintf ( clLists, "%d,%d,%d", IDX_IRTD, IDX_URTD, IDX_DRTU );
			ilRc = AATArrayCreateSbc(rgCCIDemArray.crArrayName, prgDemInf, 0, clLists, FALSE);
			if ( ilRc != RC_SUCCESS )
				dbg ( TRACE, "WritingNewCCIDemands: AATArrayCreateSbc failed RC <%d>", ilRc );
		}
	}
	if ( pclData )
		free ( pclData );
	dbg(DEBUG,"WritingNewCCIDemands: finished RC <%d>", ilRc );
	return ilRc;
}

static int BuildingNewCCIDemands(long lpNoOfSteps,long lpMaxOfDems)
{
	int		i,j;
	int		ilLoop;
	int		ilCurr = 0;
	int		ilLast = 0;

	dbg(DEBUG,"Building new demands");

	for ( i = 0; i < lpNoOfSteps; i++)
	{
		ilCurr = prgExD[i].lrDiffOfDem;
		if(ilCurr > ilLast)
		{
			for(ilLoop = ilLast+1; ilLoop <= ilCurr; ilLoop++)
			{
				if (ilLoop > 0)
				{
					TimeToStr(prgNwD[igNwD].crStartDemand,
								 prgExD[i].lrStartInSec);
					prgNwD[igNwD].lrStartInSec = prgExD[i].lrStartInSec;
					prgNwD[igNwD].srState = OPENNEW;
					igNwD++;
				}
				else
				{
					while ( prgNwD[j].srState != OPENDEL )
					{
						j++;
					}
					prgNwD[j].srState = DELETE;
					TimeToStr(prgNwD[j].crEndDemand, prgExD[i].lrStartInSec);
					prgNwD[j].lrEndInSec = prgExD[i].lrStartInSec;
				}
			}
		}
		if(ilCurr < ilLast)
		{
			j = 0;
			for (ilLoop = ilCurr + 1; ilLoop <= ilLast; ilLoop++)
			{
				if (ilLoop > 0)
				{
					while ( prgNwD[j].srState != OPENNEW )
					{
						j++;
					}
					prgNwD[j].srState = NEWDEM;
					TimeToStr(prgNwD[j].crEndDemand, prgExD[i].lrStartInSec);
					prgNwD[j].lrEndInSec = prgExD[i].lrStartInSec;

					if ( strcmp ( prgNwD[j].crStartDemand, prgNwD[j].crEndDemand ) >=0)
					{	
						dbg ( DEBUG, "BuildingNewCCIDemands: New demand with Begin <%s> > End <%s> required", 
							  prgNwD[j].crStartDemand, prgNwD[j].crEndDemand ); 	
						dbg ( DEBUG, "BuildingNewCCIDemands: ilCurr <%d>  ilLast <%d>, ilLoop <%d>", ilCurr, ilLast, ilLoop );
					}
				}
				else
				{
					TimeToStr(prgNwD[igNwD].crStartDemand,
								 prgExD[i].lrStartInSec);
					prgNwD[igNwD].lrStartInSec = prgExD[i].lrStartInSec;
					prgNwD[igNwD].srState = OPENDEL;
					igNwD++;
				}
			}
		}
		ilLast = ilCurr;
	}

	for( i = 0; i < igNwD; i++ )
	{
		if (strcmp(prgNwD[i].crEndDemand,"\0") == 0)
		{/* terminate the demands which are still open */
			TimeToStr(prgNwD[i].crEndDemand, tgIvEnd);
			prgNwD[i].lrEndInSec = tgIvEnd;

			if(prgNwD[i].srState == OPENNEW)
			{
				prgNwD[i].srState = NEWDEM;
			}
			else if(prgNwD[i].srState == OPENDEL)
				prgNwD[i].srState = DELETE;
		}

		dbg(DEBUG,"BuildCCI: <%d> Start %s  End %s  State %d", i, prgNwD[i].crStartDemand,
			prgNwD[i].crEndDemand, prgNwD[i].srState);
	}

	dbg(DEBUG, "igNwD = %d", igNwD);
	return RC_SUCCESS;
}

static int ExtendingOldCCIDemands(void)
{
	int		i;
	int		ilRc = RC_SUCCESS;
	char	clKey[URNOLEN+1 + DATELEN+1 + DATELEN+1];
	long	llRow;
	char	*pclResult = NULL;
	long	llBestDem;
	long	llDuration;
	time_t	tlStart,tlEnd;
	long	llFieldUSEU = -1;
	long	llFieldDEBE = -1;
	long	llFieldDEEN = -1;
	long	llFieldDEDU = -1;

	dbg(DEBUG,"Adding at old demands");

	for( i = 0; i < igNwD; i++ )
	{
		if (prgNwD[i].srState == NEWDEM)
		{
			llRow = ARR_FIRST;
			sprintf(clKey,"%s,%s",pcgRudUrno,prgNwD[i].crStartDemand);
			dbg(DEBUG,"Key for adding end %s",clKey);
			ilRc = CEDAArrayFindRowPointer(&rgCCIDemArray.rrArrayHandle,
											rgCCIDemArray.crArrayName,
											&rgCCIDemArray.rrIdx01Handle,
											rgCCIDemArray.crIdx01Name,clKey,
											&llRow,(void**)&pclResult);
			if (ilRc == RC_SUCCESS)
			{
				llBestDem = llRow;
				ilRc = RC_FAILURE;

/* the first demand found is the best, if there exists no job for this demand */
				while(SearchJob(CCIDEMFIELD(pclResult,igDemURNO)) == RC_SUCCESS)
				{
					llRow = ARR_NEXT;
					ilRc = CEDAArrayFindRowPointer(&rgCCIDemArray.rrArrayHandle,
													rgCCIDemArray.crArrayName,
												   &rgCCIDemArray.rrIdx01Handle,
													rgCCIDemArray.crIdx01Name,
													clKey,&llRow,
													(void**)&pclResult);
					if ( ilRc != RC_SUCCESS )
					{
						dbg ( DEBUG, "ExtendingOldCCIDemands: Break 1st loop, no suitable demand w/o job found" );
						break;
					}
				}
				if (ilRc == RC_SUCCESS)
					llBestDem = llRow;			

				StrToTime(CCIDEMFIELD(pclResult,igDemDEBE),&tlStart);
				/*StrToTime(prgNwD[i].crEndDemand,&tlEnd);*/
				tlEnd = prgNwD[i].lrEndInSec;
				llDuration = tlEnd - tlStart;

				if (!((igMaxDemand > 0) && (llDuration > igMaxDemand)))
				{
					ilRc = CEDAArrayPutField(&rgCCIDemArray.rrArrayHandle,
												rgCCIDemArray.crArrayName,
												&llFieldDEEN,"DEEN",llBestDem,
												prgNwD[i].crEndDemand);
					sprintf(CCIDEMFIELD(pcgCCIDemBuf,igDemDEDU),"%ld",
								llDuration);
					if ( llDuration < 10 )
						dbg ( DEBUG, "ExtendingOldCCIDemands: 1. setting demand length to <%ld>", llDuration);
					ilRc = CEDAArrayPutField(&rgCCIDemArray.rrArrayHandle,
												rgCCIDemArray.crArrayName,
												&llFieldDEDU,"DEDU",llBestDem,
												CCIDEMFIELD(pcgCCIDemBuf,
																igDemDEDU));
					strcpy(CCIDEMFIELD(pcgCCIDemBuf,igDemUSEU),"DEMHDL");
					ilRc = CEDAArrayPutField(&rgCCIDemArray.rrArrayHandle,
												rgCCIDemArray.crArrayName,
												&llFieldUSEU,"USEU",llRow,
												CCIDEMFIELD(pcgCCIDemBuf,
																igDemUSEU));
					prgNwD[i].srState = ADDED;
					RebuildArrayIndexes ( &rgCCIDemArray, TRUE, TRUE );
				}
			}
/*TODO: this else is not reached, if we didn't extend the found demand, because
the extended demand will be too long */
			else
			{
				llRow = ARR_LAST;
				sprintf(clKey,"%s,%s",pcgRudUrno,prgNwD[i].crEndDemand);
				dbg(DEBUG,"Key for adding begin %s",clKey);
				ilRc = CEDAArrayFindRowPointer(&rgCCIDemArray.rrArrayHandle,
												rgCCIDemArray.crArrayName,
												&rgCCIDemArray.rrIdx02Handle,
												rgCCIDemArray.crIdx02Name,clKey,
												&llRow,(void**)&pclResult);
				if (ilRc == RC_SUCCESS)
				{
					llBestDem = llRow;
					ilRc = RC_FAILURE;

/* the first demand found is the best, if there exists no job for this demand */
					while(SearchJob(CCIDEMFIELD(pclResult,igDemURNO))
																 == RC_SUCCESS)
					{
						llRow = ARR_PREV;
						ilRc = CEDAArrayFindRowPointer(
												&rgCCIDemArray.rrArrayHandle,
												rgCCIDemArray.crArrayName,
												&rgCCIDemArray.rrIdx02Handle,
												rgCCIDemArray.crIdx02Name,
												clKey,&llRow,
												(void**)&pclResult);
						if ( ilRc != RC_SUCCESS )
						{
							dbg ( DEBUG, "ExtendingOldCCIDemands: Break 2nd loop, no suitable demand w/o job found" );
							break;
						}
					}
					if (ilRc == RC_SUCCESS)
						llBestDem = llRow;			
	
					/*StrToTime(prgNwD[i].crStartDemand,&tlStart);*/
					tlStart = prgNwD[i].lrStartInSec;
					StrToTime(CCIDEMFIELD(pclResult,igDemDEEN),&tlEnd);
					llDuration = tlEnd - tlStart;

					if (!((igMaxDemand > 0) && (llDuration > igMaxDemand)))
					{
						ilRc = CEDAArrayPutField(&rgCCIDemArray.rrArrayHandle,
													rgCCIDemArray.crArrayName,
													&llFieldDEBE,"DEBE",llBestDem,
													prgNwD[i].crStartDemand);
						if ( llDuration < 10 )
							dbg ( DEBUG, "ExtendingOldCCIDemands: 2. setting demand length to <%ld>", llDuration);
						sprintf(CCIDEMFIELD(pcgCCIDemBuf,igDemDEDU),"%ld",
									llDuration);
						ilRc = CEDAArrayPutField(&rgCCIDemArray.rrArrayHandle,
													rgCCIDemArray.crArrayName,
													&llFieldDEDU,"DEDU",llBestDem,
													CCIDEMFIELD(pcgCCIDemBuf,
																igDemDEDU));
						strcpy(CCIDEMFIELD(pcgCCIDemBuf,igDemUSEU),"DEMHDL");
						ilRc = CEDAArrayPutField(&rgCCIDemArray.rrArrayHandle,
													rgCCIDemArray.crArrayName,
													&llFieldUSEU,"USEU",llRow,
													CCIDEMFIELD(pcgCCIDemBuf,
																igDemUSEU));
						prgNwD[i].srState = ADDED;
						RebuildArrayIndexes ( &rgCCIDemArray, TRUE, TRUE );
					}
				}
			}
		}
	}
	return RC_SUCCESS;
}

static int DeletingOldCCIDemands(void)
{
	int		i;
	int		ilRc = RC_SUCCESS, ilRet=RC_FAIL;
	long	llRow;
	long	llBestDem = 0, llPossibleDem=0;
	char	clKey[URNOLEN+1 + DATELEN+1 + DATELEN+1];
	char	clDemUrno[URNOLEN+1];
	char	clDebe[DATELEN+1], clDeen[DATELEN+1], *pclFlgs;
	char	*pclResult = NULL;
	BOOL	blAgain = FALSE;

	sprintf(clKey,"%s",pcgRudUrno ); 
	dbg(DEBUG,"Deleting old demands with key <%s>", clKey );

	/* delete any demands with negative length first */
	llRow = ARR_LAST;
	while ( CEDAArrayFindRowPointer(&rgCCIDemArray.rrArrayHandle,
									rgCCIDemArray.crArrayName,
									&rgCCIDemArray.rrIdx02Handle,
									rgCCIDemArray.crIdx02Name,clKey,
									&llRow,(void**)&pclResult) == RC_SUCCESS )
	{
		if ( strcmp ( CCIDEMFIELD(pclResult,igDemDEBE), CCIDEMFIELD(pclResult,igDemDEEN) ) >= 0 )
		{
			strcpy(clDemUrno, CCIDEMFIELD(pclResult,igDemURNO));
			dbg ( TRACE, "DeletingOldCCIDemands: Found invalid demand URUD <%s> UDEM <%s> DEBE <%s> DEEN <%s>",
						  CCIDEMFIELD(pclResult,igDemURUD), clDemUrno, 
						  CCIDEMFIELD(pclResult,igDemDEBE), CCIDEMFIELD(pclResult,igDemDEEN) );
			ToDeleteOrNot(clDemUrno, llRow);
			ilRet = RC_SUCCESS;		/* we could delete anything -> Success */
		}
		llRow = ARR_PREV;
	}

	for( i = 0; i < igNwD; i++ )
	{
		llBestDem = 0;
		llPossibleDem = 0;

		if (prgNwD[i].srState == DELETE)
		{
			dbg ( DEBUG, "DeletingOldCCIDemands: Trying prgNw[%d] <%s-%s> igNwD <%d>", 
						 i, prgNwD[i].crStartDemand, prgNwD[i].crEndDemand, igNwD );
			llRow = ARR_FIRST;
			/* sprintf(clKey,"%s,%s,%s",pcgRudUrno,prgNwD[i].crStartDemand,
					prgNwD[i].crEndDemand); */
			while ( ( llBestDem==0) &&
					( ( ilRc = CEDAArrayFindRowPointer(&rgCCIDemArray.rrArrayHandle,
													rgCCIDemArray.crArrayName,
													&rgCCIDemArray.rrIdx02Handle,
													rgCCIDemArray.crIdx02Name,clKey,
													&llRow,(void**)&pclResult) ) == RC_SUCCESS )
				  )
			{
				/*  for deletion of demands don't consider demands which are  
					not completely inside prgNwD[i] */ 
				if ( ( strcmp ( CCIDEMFIELD(pclResult,igDemDEBE), prgNwD[i].crEndDemand ) >= 0 ) )
				{
					/*dbg ( DEBUG, "DeletingOldCCIDemands: Demand too late, break loop DEBE <%s>",
						  CCIDEMFIELD(pclResult,igDemDEBE) );*/
					break;
				}
				pclFlgs = CCIDEMFIELD(pclResult,igDemFLGS);
				if ( ( strcmp ( CCIDEMFIELD(pclResult,igDemDEBE), prgNwD[i].crStartDemand ) < 0 ) ||
					 ( strcmp ( prgNwD[i].crEndDemand, CCIDEMFIELD(pclResult,igDemDEEN) ) < 0 ) ||
					 ( pclFlgs[1] == '1' ) )
				{	
					/*dbg ( DEBUG, "DeletingOldCCIDemands: Skip demand FLGS <%s> DEBE <%s> DEEN <%s>",
						  pclFlgs, CCIDEMFIELD(pclResult,igDemDEBE), CCIDEMFIELD(pclResult,igDemDEEN) );*/
					llRow = ARR_NEXT;
					continue;
				}
				if ( !llPossibleDem )
					llPossibleDem = llRow;	/* keep first demand found */
				if ( SearchJob(CCIDEMFIELD(pclResult,igDemURNO)) != RC_SUCCESS)
					llBestDem = llRow;	 /* no job -> keep demand as to be deleted */
				llRow = ARR_NEXT;
			}
			dbg ( DEBUG, "DeletingOldCCIDemands: End of outer loop llBestDem <%ld> llPossibleDem <%ld>",
						  llBestDem, llPossibleDem );
			if ( !llBestDem )
			{	/* only demands with jobs found */
				llBestDem = llPossibleDem;
			}
			if (llBestDem != 0 )
			{
				blAgain = FALSE;
				ilRc = CEDAArrayGetRowPointer(&rgCCIDemArray.rrArrayHandle,
												rgCCIDemArray.crArrayName,
												llBestDem,(void**)&pclResult);
				if (ilRc == RC_SUCCESS)
				{
					strcpy(clDemUrno, CCIDEMFIELD(pclResult,igDemURNO));
					strcpy(clDebe, CCIDEMFIELD(pclResult,igDemDEBE) );
					strcpy(clDeen, CCIDEMFIELD(pclResult,igDemDEEN) );
					ToDeleteOrNot(clDemUrno, llBestDem);
					ilRet = RC_SUCCESS;		/* we could delete anything -> Success */
					dbg ( DEBUG, "DeletingOldCCIDemands: prgNw[%d] delete DEM <%s> <%s-%s>",
						  i, clDemUrno, clDebe, clDeen );
					if ( strcmp ( prgNwD[i].crStartDemand, clDebe ) == 0 )
					{
						if ( strcmp ( prgNwD[i].crEndDemand, clDeen ) == 0 )
						{
							dbg ( DEBUG, "DeletingOldCCIDemands: Demand fits exactly" );
							prgNwD[i].srState = CUT;
						}
						else
						{	/* prgNwD[i].crEndDemand > clDeen */
							strcpy ( prgNwD[i].crStartDemand, clDeen );
							StrToTime(prgNwD[i].crStartDemand, &(prgNwD[i].lrStartInSec) );
							dbg ( DEBUG, "DeletingOldCCIDemands: Cut begin of prgNw[%d] to <%s>", 
										  i, prgNwD[i].crStartDemand );
							blAgain = TRUE;							
						}
					}
					else if ( strcmp ( prgNwD[i].crEndDemand, clDeen ) == 0 ) 
					{	/* prgNwD[i].crStartDemand < clDebe */
						strcpy ( prgNwD[i].crEndDemand, clDebe );
						StrToTime(prgNwD[i].crEndDemand, &(prgNwD[i].lrEndInSec) );
						dbg ( DEBUG, "DeletingOldCCIDemands: Cut end of prgNw[%d] to <%s>", 
									  i, prgNwD[i].crEndDemand );
						blAgain = TRUE;							
					}
					else
					{	/* demand completely inside  prgNwD[i] */
						/* insert a new entry in prgNwD */
						strcpy ( prgNwD[igNwD].crEndDemand, prgNwD[i].crEndDemand );
						prgNwD[igNwD].lrEndInSec = prgNwD[i].lrEndInSec;
						strcpy ( prgNwD[igNwD].crStartDemand, clDeen );
						StrToTime(prgNwD[igNwD].crStartDemand, &(prgNwD[igNwD].lrStartInSec) );
						prgNwD[igNwD].srState = DELETE;
						
						strcpy ( prgNwD[i].crEndDemand, clDebe );
						StrToTime(prgNwD[i].crEndDemand, &(prgNwD[i].lrEndInSec) );
						
						dbg ( DEBUG, "DeletingOldCCIDemands: Splitted prgNw[%d] to <%s-%s> and <%s-%s>", 
									  i, prgNwD[i].crStartDemand, prgNwD[i].crEndDemand, 
									  prgNwD[igNwD].crStartDemand, prgNwD[igNwD].crEndDemand );
						igNwD ++;
						blAgain = TRUE;							
					}
					if ( blAgain )
					{	
						if ( strcmp ( prgNwD[i].crStartDemand, prgNwD[i].crEndDemand ) >= 0 ) 
						{
							dbg ( DEBUG, "DeletingOldCCIDemands: Remaining prgNw[%d] <%s-%s> not valid", 
										  i, prgNwD[i].crStartDemand, prgNwD[i].crEndDemand );
							prgNwD[i].srState = CUT;
						}
						else
							i --; /* the shortend rest has to be investigated again */
					}
				}
			}
		}
	}

	return ilRet;
}

static int CuttingOldCCIDemands(void)
{
	int		i;
	int		ilRc = RC_SUCCESS, ilRet=RC_FAIL;
	char	clKey[URNOLEN+1 + DATELEN+1 + DATELEN+1];
	char	clDemUrno[URNOLEN+1];
	char	*pclResult = NULL;
	long	llRow;
	long	llBestDem;
	long	llFieldUSEU = -1;
	long	llFieldDEBE = -1;
	long	llFieldDEEN = -1;
	long	llFieldDEDU = -1;
	long	llDuration;
	time_t	tlStart,tlEnd;

	dbg(DEBUG,"Cutting old demands");

	for( i = 0; i < igNwD; i++ )
	{
		if (prgNwD[i].srState == DELETE)
		{
			llRow = ARR_FIRST;
			sprintf(clKey,"%s,%s",pcgRudUrno,prgNwD[i].crStartDemand);
			dbg(DEBUG,"Key for cutting begin %s",clKey);
			ilRc = CEDAArrayFindRowPointer(&rgCCIDemArray.rrArrayHandle,
											rgCCIDemArray.crArrayName,
											&rgCCIDemArray.rrIdx02Handle,
											rgCCIDemArray.crIdx02Name,clKey,
											&llRow,(void**)&pclResult);
			if (ilRc == RC_SUCCESS)
			{
				llBestDem = llRow;
				ilRc = RC_FAILURE;

				dbg( DEBUG,"CuttingOldCCIDemands: found demand URNO <%s> from <%s> till <%s>",
					 CCIDEMFIELD(pclResult,igDemURNO), CCIDEMFIELD(pclResult,igDemDEBE), 
					 CCIDEMFIELD(pclResult,igDemDEEN) );

				/* the first demand found is the best, if there exists no job for this demand */
				while(SearchJob(CCIDEMFIELD(pclResult,igDemURNO)) == RC_SUCCESS)
				{
					llRow = ARR_NEXT;
					ilRc = CEDAArrayFindRowPointer(&rgCCIDemArray.rrArrayHandle,
												rgCCIDemArray.crArrayName,
						  			 			&rgCCIDemArray.rrIdx02Handle,
												rgCCIDemArray.crIdx02Name,
												clKey,&llRow,
												(void**)&pclResult);
					if ( ilRc != RC_SUCCESS )
					{
						dbg ( DEBUG, "CuttingOldCCIDemands: Break 1st loop, no suitable demand w/o job found" );
						break;
					}
				}
				if (ilRc == RC_SUCCESS)
					llBestDem = llRow;			

				/*StrToTime(prgNwD[i].crEndDemand,&tlStart);*/
				tlStart = prgNwD[i].lrEndInSec;

				StrToTime(CCIDEMFIELD(pclResult,igDemDEEN),&tlEnd);
				llDuration = tlEnd - tlStart;

				/*dbg ( DEBUG, "CuttingOldCCIDemands: llDuration <%ld> igMinDemand <%d>", 
					  llDuration, igMinDemand );*/
				if (llDuration >= igMinDemand)
				{
					dbg( DEBUG,"CuttingOldCCIDemands: Now UDEM <%s> from <%s> till <%s>",
						 CCIDEMFIELD(pclResult,igDemURNO), prgNwD[i].crEndDemand, 
						 CCIDEMFIELD(pclResult,igDemDEEN) );

					ilRc = CEDAArrayPutField(&rgCCIDemArray.rrArrayHandle,
												rgCCIDemArray.crArrayName,
												&llFieldDEBE,"DEBE",llBestDem,
												prgNwD[i].crEndDemand);
					sprintf(CCIDEMFIELD(pcgCCIDemBuf,igDemDEDU),"%ld",
								llDuration);
					if ( llDuration < 10 )
						dbg ( DEBUG, "CuttingOldCCIDemands: 1. setting demand length to <%ld>", llDuration);

					ilRc = CEDAArrayPutField(&rgCCIDemArray.rrArrayHandle,
												rgCCIDemArray.crArrayName,
												&llFieldDEDU,"DEDU",llBestDem,
												CCIDEMFIELD(pcgCCIDemBuf,
															igDemDEDU));
					strcpy(CCIDEMFIELD(pcgCCIDemBuf,igDemUSEU),"DEMHDL");
					ilRc = CEDAArrayPutField(&rgCCIDemArray.rrArrayHandle,
												rgCCIDemArray.crArrayName,
												&llFieldUSEU,"USEU",llRow,
												CCIDEMFIELD(pcgCCIDemBuf,
															igDemUSEU));
					prgNwD[i].srState = CUT;
					RebuildArrayIndexes ( &rgCCIDemArray, TRUE, TRUE );
					ilRet = RC_SUCCESS;		/* we could delete anything -> Success */
				}
				else if (llDuration == 0)
				{
					strcpy(clDemUrno, CCIDEMFIELD(pclResult,igDemURNO));
					ToDeleteOrNot(clDemUrno, llRow);
					ilRet = RC_SUCCESS;		/* we could delete anything -> Success */
					prgNwD[i].srState = CUT;
					dbg( DEBUG,"CuttingOldCCIDemands: UDEM <%s> deleted",
						 CCIDEMFIELD(pclResult,igDemURNO) ); 
				}
			}
/*TODO: this else is not reached, if we didn't cut the found demand, because the
cut demand will be too short */
			else 
			{
				llRow = ARR_LAST;
				sprintf(clKey,"%s,%s",pcgRudUrno,prgNwD[i].crEndDemand);
				dbg(DEBUG,"Key for cutting end %s",clKey);
				ilRc = CEDAArrayFindRowPointer(&rgCCIDemArray.rrArrayHandle,
												rgCCIDemArray.crArrayName,
												&rgCCIDemArray.rrIdx01Handle,
												rgCCIDemArray.crIdx01Name,clKey,
												&llRow,(void**)&pclResult);
				if (ilRc == RC_SUCCESS)
				{
					llBestDem = llRow;
					ilRc = RC_FAILURE;
	
/* the first demand found is the best, if there exists no job for this demand */
					while(SearchJob(CCIDEMFIELD(pclResult,igDemURNO))
																 == RC_SUCCESS)
					{
						llRow = ARR_PREV;
						ilRc = CEDAArrayFindRowPointer(
										&rgCCIDemArray.rrArrayHandle,
										rgCCIDemArray.crArrayName,
										&rgCCIDemArray.rrIdx01Handle,
										rgCCIDemArray.crIdx01Name,
										clKey,&llRow, (void**)&pclResult);
						if ( ilRc != RC_SUCCESS )
						{
							dbg ( DEBUG, "CuttingOldCCIDemands: Break 2nd loop, no suitable demand w/o job found" );
							break;
						}
					}
					if (ilRc == RC_SUCCESS)
						llBestDem = llRow;			

					StrToTime(CCIDEMFIELD(pclResult,igDemDEBE),&tlStart);
					/*StrToTime(prgNwD[i].crStartDemand,&tlEnd);*/
					tlEnd = prgNwD[i].lrStartInSec;
					llDuration = tlEnd - tlStart;

					if (llDuration >= igMinDemand)
					{
						dbg( DEBUG,"CuttingOldCCIDemands: Now UDEM <%s> from <%s> till <%s>",
							 CCIDEMFIELD(pclResult,igDemURNO), CCIDEMFIELD(pclResult,igDemDEBE), 
							prgNwD[i].crStartDemand );

						ilRc = CEDAArrayPutField(&rgCCIDemArray.rrArrayHandle,
													rgCCIDemArray.crArrayName,
													&llFieldDEEN,"DEEN",llBestDem,
													prgNwD[i].crStartDemand);
						sprintf(CCIDEMFIELD(pcgCCIDemBuf,igDemDEDU),"%ld",
									llDuration);
						if ( llDuration < 10 )
							dbg ( DEBUG, "CuttingOldCCIDemands: 2. setting demand length to <%ld>", llDuration);

						ilRc = CEDAArrayPutField(&rgCCIDemArray.rrArrayHandle,
													rgCCIDemArray.crArrayName,
													&llFieldDEDU,"DEDU",llBestDem,
													CCIDEMFIELD(pcgCCIDemBuf,
																 igDemDEDU));
						strcpy(CCIDEMFIELD(pcgCCIDemBuf,igDemUSEU), "DEMHDL");
						ilRc = CEDAArrayPutField(&rgCCIDemArray.rrArrayHandle,
													rgCCIDemArray.crArrayName,
													&llFieldUSEU,"USEU",llRow,
													CCIDEMFIELD(pcgCCIDemBuf,
																igDemUSEU));
						prgNwD[i].srState = CUT;
						RebuildArrayIndexes ( &rgCCIDemArray, TRUE, TRUE );
						ilRet = RC_SUCCESS;		/* we could delete anything -> Success */
					}
					else if (llDuration == 0)
					{
					 	strcpy(clDemUrno, CCIDEMFIELD(pclResult,igDemURNO));
						ToDeleteOrNot(clDemUrno, llRow);
						prgNwD[i].srState = CUT;
						dbg( DEBUG,"CuttingOldCCIDemands: UDEM <%s> deleted",
							 CCIDEMFIELD(pclResult,igDemURNO) ); 
						ilRet = RC_SUCCESS;		/* we could delete anything -> Success */
					}
				}
			}
		}
	}
	return ilRet;
}

static int SplittingOldCCIDemands(void)
{
	int		i;
	int		ilRc = RC_SUCCESS, ilRet=RC_FAIL;
	time_t	tlOldStart,tlOldEnd,tlNewStart,tlNewEnd;
	long	llRow;
	char	*pclResult = NULL;
	char	clKeyDem[URNOLEN+1];
	long	llBestRow;
	long	llBestStartRow;
	long	llBestEndRow;
	long	llFieldDEBE = -1;
	long	llFieldDEEN = -1;
	long	llItem = 0;
	long	llDiffStart = 0;
	long	llDiffEnd = 0;
	long	llBestItem;
	long	llBestStart;
	long	llBestEnd;

	dbg(DEBUG,"Splitting old demands");

	for( i = 0; i < igNwD; i++ )
	{
		if (prgNwD[i].srState == DELETE)
		{
			llBestRow = 0;
			llBestStartRow = 0;
			llBestEndRow = 0;
			llBestItem = 0;
			llBestStart = 0;
			llBestEnd = 0;
			llRow = ARR_FIRST;

			/* StrToTime(prgNwD[i].crStartDemand,&tlNewStart); */
			/* StrToTime(prgNwD[i].crEndDemand,&tlNewEnd); */
			tlNewStart = prgNwD[i].lrStartInSec;
			tlNewEnd = prgNwD[i].lrEndInSec;
			sprintf(clKeyDem,"%s",pcgRudUrno);
			while( CEDAArrayFindRowPointer(&rgCCIDemArray.rrArrayHandle,
											rgCCIDemArray.crArrayName,
											&rgCCIDemArray.rrIdx02Handle,
											rgCCIDemArray.crIdx02Name,clKeyDem,
											&llRow,(void**)&pclResult)
																 == RC_SUCCESS)
			{/* for better performance looking for the best, the best start */
			/* and the best end is done in one loop */
				StrToTime(CCIDEMFIELD(pclResult,igDemDEBE),&tlOldStart);
				StrToTime(CCIDEMFIELD(pclResult,igDemDEEN),&tlOldEnd);
				if ((tlNewStart > (tlOldStart + igMinDemand)) &&
					(tlNewEnd < (tlOldEnd - igMinDemand)))
				{
					llDiffStart = tlNewStart - tlOldStart;
					llDiffEnd = tlOldEnd - tlNewEnd;
					llItem = min(llDiffStart,llDiffEnd);

					if (llItem > llBestItem)
					{
						llBestItem = llItem;
						llBestRow = llRow;
					}
				}
				/* if a best exists, that means cut out the whole demand is */
				/* possible, best start and best end don't mind */
				if (llBestRow == 0)
				{
					if ((tlNewStart > (tlOldStart + igMinDemand)) &&
						(tlNewStart < tlOldEnd))
					{
						dbg(DEBUG,"No old data for the whole demand");
						llDiffStart = tlNewStart - tlOldStart;
						if (llDiffStart > llBestStart)
						{
							llBestStart = llDiffStart;
							llBestStartRow = llRow;
						}
					}
					else if ((tlNewEnd < (tlOldEnd - igMinDemand)) &&
							(tlNewEnd > tlOldStart))
					{
						dbg(DEBUG,"No old data for the whole demand");
						llDiffEnd = tlOldEnd - tlNewEnd;
						if (llDiffEnd > llBestEnd)
						{
							llBestEnd = llDiffEnd;
							llBestEndRow = llRow;
						}
					}
				}
				llRow = ARR_NEXT;
			}
			if (llBestRow != 0 )
			{
				ilRc = CEDAArrayGetRowPointer(&rgCCIDemArray.rrArrayHandle,
												rgCCIDemArray.crArrayName,
												llBestRow,(void**)&pclResult);
				if (ilRc == RC_SUCCESS)
				{
					strcpy(prgNwD[igNwD].crEndDemand, CCIDEMFIELD(pclResult,igDemDEEN)); 
					StrToTime ( prgNwD[igNwD].crEndDemand, &(prgNwD[igNwD].lrEndInSec) );

					ilRc = CEDAArrayPutField(&rgCCIDemArray.rrArrayHandle,
												rgCCIDemArray.crArrayName,
												&llFieldDEEN,"DEEN",llBestRow,
												prgNwD[i].crStartDemand);
					strcpy(prgNwD[igNwD].crStartDemand,prgNwD[i].crEndDemand);
					prgNwD[igNwD].lrStartInSec = prgNwD[i].lrEndInSec;
					/*  strcpy(prgNwD[igNwD].crEndDemand,
							CCIDEMFIELD(pclResult,igDemDEEN)); maybe too late here, moved 2 lines up */
					prgNwD[igNwD].srState = NEWDEM;
					if ( strcmp ( prgNwD[igNwD].crStartDemand, prgNwD[igNwD].crEndDemand ) >= 0 )
					{	
						dbg ( DEBUG, "SplittingOldCCIDemands: New demand with Begin <%s> > End <%s> required", 
							  prgNwD[igNwD].crStartDemand, prgNwD[igNwD].crEndDemand ); 	
					}
					igNwD++;
					RebuildArrayIndexes ( &rgCCIDemArray, TRUE, TRUE );
					ilRet = RC_SUCCESS;		/* we could delete anything -> Success */
				}
			}
			else if (llBestStart != 0 || llBestEnd != 0)
			{
				if (llBestStart >= llBestEnd)
				{/* a piece as long as possible is cut from the end of the demand */
					llRow = llBestStartRow;
					ilRc = CEDAArrayGetRowPointer(&rgCCIDemArray.rrArrayHandle,
													rgCCIDemArray.crArrayName,
													llRow,(void**)&pclResult);
					if (ilRc == RC_SUCCESS)
					{
						ilRc = CEDAArrayPutField(&rgCCIDemArray.rrArrayHandle,
													rgCCIDemArray.crArrayName,
													&llFieldDEEN,"DEEN",llRow,
													prgNwD[i].crStartDemand);
						strcpy(prgNwD[i].crStartDemand,
								CCIDEMFIELD(pclResult,igDemDEEN));
						StrToTime(prgNwD[i].crStartDemand, &(prgNwD[i].lrStartInSec) );
						if (strcmp(prgNwD[i].crStartDemand,
									prgNwD[i].crEndDemand) < 0)
							i--;
					/*because the rest of the demand has to be treated again*/
						RebuildArrayIndexes ( &rgCCIDemArray, TRUE, TRUE );
						ilRet = RC_SUCCESS;		/* we could delete anything -> Success */
					}
				}
				else
				{/* a piece as long as possible is cut from the begin of the demand */
					llRow = llBestEndRow;
					ilRc = CEDAArrayGetRowPointer(&rgCCIDemArray.rrArrayHandle,
													rgCCIDemArray.crArrayName,
													llRow,(void**)&pclResult);
					if (ilRc == RC_SUCCESS)
					{
						ilRc = CEDAArrayPutField(&rgCCIDemArray.rrArrayHandle,
													rgCCIDemArray.crArrayName,
													&llFieldDEBE,"DEBE",llRow,
													prgNwD[i].crEndDemand);
						strcpy(prgNwD[i].crEndDemand,
								CCIDEMFIELD(pclResult,igDemDEBE));
						StrToTime ( prgNwD[i].crEndDemand, &(prgNwD[i].lrEndInSec) );

						if (strcmp(prgNwD[i].crStartDemand,
									prgNwD[i].crEndDemand) < 0)
							i--;
					/*because the rest of the demand has to be treated again*/
						RebuildArrayIndexes ( &rgCCIDemArray, TRUE, TRUE );
						ilRet = RC_SUCCESS;		/* we could delete anything -> Success */
					}
				}
			}
			else
				dbg(DEBUG,"No chance to delete demand from %s to %s",
					prgNwD[i].crStartDemand,prgNwD[i].crEndDemand);
		}
	}
	return ilRet;
}

static int InitializeCCIDemand(char *pcpCCIDemBuf)
{
	int ilRc = RC_SUCCESS;
	char pclUrno[URNOLEN+1]="0";
	char pclNow[DATELEN+1];

	/*GetNextValues(pclUrno,1);*/
	GetNextUrno(pclUrno);
		
	GetServerTimeStamp("UTC", 1, 0, pclNow);
 
	memset(pcpCCIDemBuf,0,rgCCIDemArray.lrArrayRowLen);

	strcpy(CCIDEMFIELD(pcpCCIDemBuf,igDemALID)," ");
	strcpy(CCIDEMFIELD(pcgCCIDemBuf,igDemALOC),"CIC");
	strcpy(CCIDEMFIELD(pcpCCIDemBuf,igDemCDAT),pclNow);
	strcpy(CCIDEMFIELD(pcpCCIDemBuf,igDemDBAR)," ");
	strcpy(CCIDEMFIELD(pcpCCIDemBuf,igDemDEAR)," ");
	strcpy(CCIDEMFIELD(pcpCCIDemBuf,igDemDEBE),"0");
	strcpy(CCIDEMFIELD(pcpCCIDemBuf,igDemDEDU),"0");
	strcpy(CCIDEMFIELD(pcpCCIDemBuf,igDemDEEN),"0");
	strcpy(CCIDEMFIELD(pcpCCIDemBuf,igDemDETY),"6");
	strcpy(CCIDEMFIELD(pcpCCIDemBuf,igDemEADB),"0");
	strcpy(CCIDEMFIELD(pcpCCIDemBuf,igDemFLGS)," ");
	strcpy(CCIDEMFIELD(pcpCCIDemBuf,igDemHOPO),cgHopo);
	strcpy(CCIDEMFIELD(pcpCCIDemBuf,igDemLADE),"0");
	strcpy(CCIDEMFIELD(pcpCCIDemBuf,igDemLSTU)," ");
	strcpy(CCIDEMFIELD(pcpCCIDemBuf,igDemOBTY),"AFT");
	strcpy(CCIDEMFIELD(pcpCCIDemBuf,igDemOURI),"0");
	strcpy(CCIDEMFIELD(pcpCCIDemBuf,igDemOURO),"0");
	strcpy(CCIDEMFIELD(pcpCCIDemBuf,igDemRETY)," ");
	strcpy(CCIDEMFIELD(pcpCCIDemBuf,igDemRTDB),"0");
	strcpy(CCIDEMFIELD(pcpCCIDemBuf,igDemRTDE),"0");
	strcpy(CCIDEMFIELD(pcpCCIDemBuf,igDemSDTI),"0");
	strcpy(CCIDEMFIELD(pcpCCIDemBuf,igDemSUTI),"0");
	strcpy(CCIDEMFIELD(pcpCCIDemBuf,igDemTSDB),"0");
	strcpy(CCIDEMFIELD(pcpCCIDemBuf,igDemTSDE),"0");
	strcpy(CCIDEMFIELD(pcpCCIDemBuf,igDemTTGF),"0");
	strcpy(CCIDEMFIELD(pcpCCIDemBuf,igDemTTGT),"0");
	strcpy(CCIDEMFIELD(pcpCCIDemBuf,igDemUCON)," ");
	strcpy(CCIDEMFIELD(pcpCCIDemBuf,igDemUDGR),"0");
	strcpy(CCIDEMFIELD(pcpCCIDemBuf,igDemUREF)," ");
	strcpy(CCIDEMFIELD(pcpCCIDemBuf,igDemURNO),pclUrno);
	strcpy(CCIDEMFIELD(pcpCCIDemBuf,igDemURUD),"0");
	strcpy(CCIDEMFIELD(pcpCCIDemBuf,igDemUSEC),"DEMHDL");
	strcpy(CCIDEMFIELD(pcpCCIDemBuf,igDemUSEU),"DEMHDL");
	if (igDemUSES>0)
		strcpy(CCIDEMFIELD(pcpCCIDemBuf,igDemUSES)," ");
	if (igDemUTPL>0)
		strcpy(CCIDEMFIELD(pcpCCIDemBuf,igDemUTPL)," ");

	return ilRc;
}

static int JobExists(char *pcpDemUrno)
{
	int		ilRc = RC_FAILURE;
 	short	slCursor;
	short	slSqlFunc;
	char	pclSqlBuf[128];
	char	pclDataArea[256];

	slSqlFunc = START;
	slCursor = 0;

	sprintf(pclSqlBuf,"SELECT * FROM JODTAB WHERE UDEM='%s'",pcpDemUrno);

	ilRc = sql_if(slSqlFunc,&slCursor,pclSqlBuf,pclDataArea);
	close_my_cursor(&slCursor);

	dbg(DEBUG,"select from jodtab with RC = %d",ilRc);
	return ilRc;
}

static int IntegrateOthers (time_t tpOldEnd, time_t tpNewEnd, time_t tpMaxEnd)
{
	int		ilRc = RC_SUCCESS;
	int		i,j;
	short	slStep;
	time_t	tlOtherStart,tlOtherEnd;

	slStep = atoi(pcgStep) * 60;
	for (i = tpOldEnd; i <= tpNewEnd; )
	{
		for (j = 0; j < igNwD; j++)
		{
			if (prgNwD[j].srState == NEWDEM)
			{
				/*StrToTime(prgNwD[j].crStartDemand,&tlOtherStart);
				StrToTime(prgNwD[j].crEndDemand,&tlOtherEnd);*/
				tlOtherStart = prgNwD[j].lrStartInSec;
				tlOtherEnd = prgNwD[j].lrEndInSec;
				if ( i == tlOtherStart &&
					((tpMaxEnd == 0) || (tlOtherEnd <= tpMaxEnd)))
				{ /* demand is coincided with the extension */
					prgNwD[j].srState = NOOP;
					i = tlOtherEnd - slStep;
					j = igNwD;
				}
			}
		}
		i += slStep;
	}
	return ilRc;
}

static int SaveIndexInfo (ARRAYINFO *prpArrayInfo)
{
	int		ilRC = RC_SUCCESS;
	long	llRow = 0;
	FILE	*prlFile = NULL;
	char	clDel = ',';
	char	clFile[512];
	char	*pclTrimBuf = NULL;
	long	llRowCount;
	int		ilCount = 0;
	char	*pclTestBuf = NULL;


	dbg(TRACE,"SaveIndexInfo: Array name <%s>",prpArrayInfo->crArrayName);

	if (ilRC == RC_SUCCESS)
	{
		sprintf(&clFile[0],"%s/%s_%s.info",getenv("DBG_PATH"),mod_name,
				prpArrayInfo->crArrayName);

		errno = 0;
		prlFile = fopen (&clFile[0], "w");
		if (prlFile == NULL)
		{
		    dbg(TRACE, "SaveIndexInfo: fopen <%s> failed <%d-%s>", &clFile[0],
				errno, strerror(errno));
			ilRC = RC_FAIL;
 		}
	}

	if (ilRC == RC_SUCCESS)
  	{
		dbg(DEBUG, "ArrayName      <%s>\n", prpArrayInfo->crArrayName);
		dbg(DEBUG, "ArrayFieldList <%s>\n", prpArrayInfo->crArrayFieldList);
		dbg(DEBUG, "Idx01FieldList <%s>\n", prpArrayInfo->crIdx01FieldList);

 		fprintf(prlFile,"ArrayName      <%s>\n",prpArrayInfo->crArrayName);
		fflush(prlFile);
		fprintf(prlFile,"ArrayFieldList <%s>\n",prpArrayInfo->crArrayFieldList);		fflush(prlFile);
		fprintf(prlFile,"Idx01FieldList <%s>\n",prpArrayInfo->crIdx01FieldList);
		fflush(prlFile);

		dbg(DEBUG,"IndexData"); 
		fprintf(prlFile,"IndexData\n"); fflush(prlFile);

		dbg(DEBUG,"FindKey <%s>",prpArrayInfo->pcrIdx01RowBuf);
		*prpArrayInfo->pcrIdx01RowBuf = '\0';
		if ( prpArrayInfo->rrIdx01Handle >= 0 )
		{
			llRow = ARR_FIRST;
			do
   			{
				ilRC = CEDAArrayFindKey(&(prpArrayInfo->rrArrayHandle),
										&(prpArrayInfo->crArrayName[0]),
										&(prpArrayInfo->rrIdx01Handle),
										prpArrayInfo->crIdx01Name, &llRow,
										prpArrayInfo->lrIdx01RowLen,
										prpArrayInfo->pcrIdx01RowBuf);
				if (ilRC == RC_SUCCESS)
				{
    
   					dbg(DEBUG, "SaveIndexInfo: Key <%ld> <%s>",llRow,
						prpArrayInfo->pcrIdx01RowBuf);
    
					/*
					if (strlen (prpArrayInfo->pcrIdx01RowBuf) == 0)
					{
						ilRC = CEDAArrayGetRow(&(prpArrayInfo->rrArrayHandle),
												&(prpArrayInfo->crArrayName[0]),
												llRow, clDel,
												prpArrayInfo->lrArrayRowLen,
												prpArrayInfo->pcrArrayRowBuf);
						if(ilRC == RC_SUCCESS)
		     			{
		       				dbg(DEBUG, "SaveIndexInfo: Row <%ld> <%s>", llRow,
								prpArrayInfo->pcrArrayRowBuf);
		   				}
					}*/

		    		pclTrimBuf = strdup (prpArrayInfo->pcrIdx01RowBuf);
		     		if (pclTrimBuf != NULL)
					{
						ilRC = GetDataItem(pclTrimBuf,prpArrayInfo->pcrIdx01RowBuf,
											1, clDel, "", "  ");
		      			if (ilRC > 0)
		      			{
							ilRC = RC_SUCCESS;
							fprintf(prlFile,"Key            <%ld><%s>\n",llRow,
									pclTrimBuf);
							fflush(prlFile);
		     			}

						free (pclTrimBuf);
		   				pclTrimBuf = NULL;
		    		}

 		   			llRow = ARR_NEXT;
				}
				else
    			{
   					dbg(TRACE, "SaveIndexInfo: CEDAArrayFindKey failed <%d>", ilRC);
				}
			} while (ilRC == RC_SUCCESS) ;
		}
		else
			dbg (DEBUG,"rrIdx01Handle not initialized");
		dbg (DEBUG,"ArrayData");
		fprintf (prlFile,"ArrayData\n"); fflush(prlFile);

  		llRow = ARR_FIRST;

		CEDAArrayGetRowCount(&(prpArrayInfo->rrArrayHandle),
								&(prpArrayInfo->crArrayName[0]),&llRowCount);

		llRow = ARR_FIRST;
		dbg(DEBUG,"Array  <%s> data follows Rows %ld",prpArrayInfo->crArrayName,
			llRowCount);
		do
		{
			ilRC = CEDAArrayGetRowPointer(&(prpArrayInfo->rrArrayHandle),
			&(prpArrayInfo->crArrayName[0]),llRow,(void *)&pclTestBuf);
			if (ilRC == RC_SUCCESS)
			{
				for( ilCount = 0; ilCount < prpArrayInfo->lrArrayFieldCnt;
					 ilCount++)
				{
					fprintf(prlFile,"<%s>",
						&pclTestBuf[prpArrayInfo->plrArrayFieldOfs[ilCount]]); 
					fflush(prlFile);
					/* dbg(DEBUG, "SaveIndexInfo: ilCount <%ld> <%s>", ilCount,
						&pclTestBuf[prpArrayInfo->plrArrayFieldOfs[ilCount]]);*/
				}
				fprintf(prlFile,"\n"); 
				dbg(DEBUG,"\n");
				fflush(prlFile);
				llRow = ARR_NEXT;
			}
			else
				dbg(DEBUG,"GetRowPointer failed RC=%d",ilRC);
		} while (ilRC == RC_SUCCESS);

		if (ilRC == RC_NOTFOUND)
			ilRC = RC_SUCCESS;
	}

	if (prlFile != NULL)
	{
		fclose(prlFile);
		prlFile = NULL;
		dbg(DEBUG, "SaveIndexInfo: event saved - file <%s>", &clFile[0]);
	}

	return ilRC;
}

static int CreateIdx1 ( ARRAYINFO *prpArrayInfo, char *pcpIdxName,
						char *pcpFields, char *pcpOrdering )
{
	int		ilRC = RC_FAIL, ilLoop;
	char clOrderTxt[4];
	long	*pllOrdering=0;

	if ( !pcpFields )
		return ilRC;		/*  0-Pointer in parameter pcpFields */

	strcpy ( prpArrayInfo->crIdx01Name, pcpIdxName );
	strcpy ( prpArrayInfo->crIdx01FieldList, pcpFields );

	ilRC = GetTotalRowLength( pcpFields, prpArrayInfo, &(prpArrayInfo->lrIdx01RowLen) )	;
    
	if(ilRC != RC_SUCCESS)
        dbg(TRACE,"CreateIdx1: GetTotalRowLength failed <%s> <%s>",
			prpArrayInfo->crTableName, pcpFields);
	else
    {
		prpArrayInfo->pcrIdx01RowBuf = (char *) calloc(1,
									   prpArrayInfo->lrIdx01RowLen+1);
   		if( prpArrayInfo->pcrIdx01RowBuf == NULL )
   		{
      		ilRC = RC_FAIL;
      		dbg(TRACE,"CreateIdx1: calloc failed");
   		}
   	}

	if(ilRC == RC_SUCCESS)
	{
		prpArrayInfo->lrIdx01FieldCnt = get_no_of_items(pcpFields);
		pllOrdering = (long *)calloc(prpArrayInfo->lrIdx01FieldCnt,
										sizeof(long));
		if(!pllOrdering )
		{
			dbg(TRACE,"SetArrayInfo: pllOrdering calloc(%d,%d) failed",
				prpArrayInfo->lrIdx01FieldCnt,sizeof(long));
			ilRC = RC_FAIL;
		}
		else
		{
			for(ilLoop=0; ilLoop < prpArrayInfo->lrIdx01FieldCnt; ilLoop++)
			{
				if (!pcpOrdering ||
					 ( get_real_item(clOrderTxt,pcpOrdering,ilLoop+1) <= 0 ) )
				clOrderTxt[0] = 'D';
				if ( clOrderTxt[0] == 'A' )
					pllOrdering[ilLoop] = ARR_ASC;
				else
					pllOrdering[ilLoop] = ARR_DESC;
			}
		}
	}

	ilRC = CEDAArrayCreateMultiIndex( &(prpArrayInfo->rrArrayHandle), 
									  prpArrayInfo->crArrayName,
									  &(prpArrayInfo->rrIdx01Handle), 
									  prpArrayInfo->crIdx01Name,
									  prpArrayInfo->crIdx01FieldList, 
									  pllOrdering );
	if ( pllOrdering )
		free (pllOrdering);
	pllOrdering = 0;
	return ilRC;									
}

static int CreateIdx2( ARRAYINFO *prpArrayInfo, char *pcpIdxName,
						char *pcpFields, char *pcpOrdering )
{
	int		ilRC = RC_FAIL, ilLoop;
	char	clOrderTxt[4];
	long	*pllOrdering=0;

	if ( !pcpFields )
		return ilRC;		/*  0-Pointer in parameter pcpFields */

	strcpy ( prpArrayInfo->crIdx02Name, pcpIdxName );
	strcpy ( prpArrayInfo->crIdx02FieldList, pcpFields );
	
	ilRC = GetTotalRowLength( pcpFields, prpArrayInfo, &(prpArrayInfo->lrIdx02RowLen) )	;
    /*ilRC = GetRowLength( &cgHopo[0], prpArrayInfo->crTableName, pcpFields, 
						 &(prpArrayInfo->lrIdx02RowLen));*/
    
	if(ilRC != RC_SUCCESS)
        dbg(TRACE,"CreateIdx2 GetRowLength failed <%s> <%s>",
			prpArrayInfo->crTableName, pcpFields);
	else
    {
		prpArrayInfo->pcrIdx02RowBuf = (char *) calloc(1,
									   prpArrayInfo->lrIdx02RowLen+1);
   		if( prpArrayInfo->pcrIdx02RowBuf == NULL )
   		{
      		ilRC = RC_FAIL;
      		dbg(TRACE,"CreateIdx2: calloc failed");
   		}
   	}

	if(ilRC == RC_SUCCESS)
	{
		prpArrayInfo->lrIdx02FieldCnt = get_no_of_items(pcpFields);
		pllOrdering = (long *)calloc(prpArrayInfo->lrIdx02FieldCnt,
										sizeof(long));
		if(!pllOrdering )
		{
			dbg(TRACE,"SetArrayInfo: pllOrdering calloc(%d,%d) failed",
				prpArrayInfo->lrIdx02FieldCnt,sizeof(long));
			ilRC = RC_FAIL;
		}
		else
		{
			for(ilLoop=0; ilLoop < prpArrayInfo->lrIdx02FieldCnt; ilLoop++)
			{
				if ( !pcpOrdering ||
					( get_real_item(clOrderTxt,pcpOrdering,ilLoop+1) <=0) )				 
					clOrderTxt[0] = 'D';
				if ( clOrderTxt[0] == 'A' )
					pllOrdering[ilLoop] = ARR_ASC;
				else
					pllOrdering[ilLoop] = ARR_DESC;
			}
		}
	}

	ilRC = CEDAArrayCreateMultiIndex( &(prpArrayInfo->rrArrayHandle), 
									  prpArrayInfo->crArrayName,
									  &(prpArrayInfo->rrIdx02Handle), 
									  prpArrayInfo->crIdx02Name,
									  prpArrayInfo->crIdx02FieldList, 
									  pllOrdering );
	if ( pllOrdering )
		free (pllOrdering);
	pllOrdering = 0;
	return ilRC;									
}

/*
static int	HandleIDR(char *pcpFields, char *pcpData)
{
	int		ilRc = RC_SUCCESS;
	int		i;
	int		ilPos = 0;
	int		ilNoOfItems = 0;
	char	clUrno[URNOLEN+1];
	char	clField[128];
	char	pclSqlBuf[2048],pclDataArea[2048],clData[2048];
	short	slLocalCursor,slFuncCode;

	memset(pclSqlBuf,0,sizeof(pclSqlBuf));
	memset(pclDataArea,0,sizeof(pclDataArea));

	slFuncCode = START;
	slLocalCursor = 0;
	ilPos = get_item_no(pcpFields, "URNO", 5);
	if (ilPos >= 0)
	{
		get_real_item(clUrno, pcpData, ilPos+1);
        sprintf(pclSqlBuf, "SELECT * FROM DEMTAB WHERE URNO = '%s'", clUrno);
		ilRc = sql_if(slFuncCode,&slLocalCursor,pclSqlBuf,pclDataArea);
		close_my_cursor(&slLocalCursor);
	}

	if (ilRc != RC_SUCCESS)
	{
		memset(pclSqlBuf,0,sizeof(pclSqlBuf));
		memset(pclDataArea,0,sizeof(pclDataArea));
		memset(clData,0,sizeof(clData));
		slFuncCode = START;
		slLocalCursor = 0;

		ilNoOfItems = get_no_of_items(pcpData);
		for ( i = 0; i < ilNoOfItems; i++ )
		{
			if ( i == 0 )
				strcat(clData,"'");
			else
				strcat(clData,",'");
			get_real_item(clField, pcpData, i+1);
			strcat(clData,clField);
			strcat(clData,"'");
		}

		if (strstr(pcpFields,"HOPO") == NULL)
	    {
   		    sprintf(pclSqlBuf, "INSERT INTO DEMTAB (%s,HOPO) VALUES (%s,'%s')",
					 pcpFields, clData, cgHopo);
   		}
   		else
   		{
   		    sprintf(pclSqlBuf, "INSERT INTO DEMTAB (%s) VALUES (%s)",
					 pcpFields, clData);
   		}

		ilRc = sql_if(slFuncCode,&slLocalCursor,pclSqlBuf,pclDataArea);
		if( ilRc != RC_SUCCESS )
		{
			dbg(TRACE, "sql_if: ilRc = %d", ilRc);
			ilRc = RC_FAIL;
		}
		commit_work();
		close_my_cursor(&slLocalCursor);
	}
	else
	{
		dbg(TRACE, "URNO %s already exists", clUrno);
		ilRc = RC_FAIL;
	}


	return ilRc;
}
*/

static	int	GetDebeFromPrevDem(char *pcpDebe, char *pcpDeen, char *pcpRudRow)
{
	int     ilRC = RC_SUCCESS;
	long	llPrevRow = ARR_FIRST;
	char	*pclPrevResult = NULL;
	char	clKey[URNOLEN+1];
	char	pclTmpStr[32];

	/* if (debug_level > TRACE)
		ilRC = SaveIndexInfo( &rgNewDemArray ); */
	sprintf(clKey, "%s", RUDFIELD(pcpRudRow, igRudUPDE));
	dbg(DEBUG, "Key for CEDAArrayFindRowPointer %s", clKey);

	ilRC = CEDAArrayFindRowPointer(&rgNewDemArray.rrArrayHandle,
									rgNewDemArray.crArrayName,
									&rgNewDemArray.rrIdx01Handle,
									rgNewDemArray.crIdx01Name, clKey,
									&llPrevRow, (void**)&pclPrevResult);
	if (ilRC != RC_SUCCESS)
	{
		dbg(TRACE, "CEDAArrayFindRowPointer failed with RC %d", ilRC);
		return ilRC;
	}
	
	if (strncmp((RUDFIELD(pcpRudRow,igRudREPD)+1), "S", 1) == 0)
	{
		if (strncmp(RUDFIELD(pcpRudRow,igRudREPD), "S", 1) == 0)
			strcpy(pcpDebe, NEWDEMFIELD(pclPrevResult, igDemDEBE));
		else
			strcpy(pcpDebe, NEWDEMFIELD(pclPrevResult, igDemDEEN));

		SunCompStrAddTime(pcpDebe,RUDFIELD(pcpRudRow,igRudTSPD),pcpDebe);
		SunCompStrAddTime(pcpDebe,RUDFIELD(pcpRudRow,igRudDEDU),pcpDeen);
		SunCompStrAddTime(pcpDeen,RUDFIELD(pcpRudRow,igRudSUTI),pcpDeen);
		SunCompStrAddTime(pcpDeen,RUDFIELD(pcpRudRow,igRudTTGT),pcpDeen);
		SunCompStrAddTime(pcpDeen,RUDFIELD(pcpRudRow,igRudTTGF),pcpDeen);
		SunCompStrAddTime(pcpDeen,RUDFIELD(pcpRudRow,igRudSDTI),pcpDeen);
	}
	else
	{
		if (strncmp(RUDFIELD(pcpRudRow,igRudREPD), "S", 1) == 0)
			strcpy(pcpDeen, NEWDEMFIELD(pclPrevResult, igDemDEBE));
		else
			strcpy(pcpDeen, NEWDEMFIELD(pclPrevResult, igDemDEEN));

		SunCompStrAddTime(pcpDeen,RUDFIELD(pcpRudRow,igRudTSPD),pcpDeen);
		sprintf(pclTmpStr,"-%s",RUDFIELD(pcpRudRow,igRudDEDU));
		SunCompStrAddTime(pcpDeen,pclTmpStr,pcpDebe);
		sprintf(pclTmpStr,"-%s",RUDFIELD(pcpRudRow,igRudSDTI));
		SunCompStrAddTime(pcpDebe,pclTmpStr,pcpDebe);
		sprintf(pclTmpStr,"-%s",RUDFIELD(pcpRudRow,igRudTTGF));
		SunCompStrAddTime(pcpDebe,pclTmpStr,pcpDebe);
		sprintf(pclTmpStr,"-%s",RUDFIELD(pcpRudRow,igRudTTGT));
		SunCompStrAddTime(pcpDebe,pclTmpStr,pcpDebe);
		sprintf(pclTmpStr,"-%s",RUDFIELD(pcpRudRow,igRudSUTI));
		SunCompStrAddTime(pcpDebe,pclTmpStr,pcpDebe);
	}

	return ilRC;
}

static int LocalToUtc(char *pcpTime)
{
	int ilRc = RC_SUCCESS;
	char *pclTdi;
	char clLocal[32];

	strcpy(clLocal,pcpTime);
	pclTdi = (strcmp(pcpTime,cgTich) < 0) ? cgTdi1 : cgTdi2;

	AddSecondsToCEDATime(pcpTime,(time_t)0-atol(pclTdi),1);
	dbg(DEBUG,"2UTC  : <%s> ",pcpTime);

	return ilRc;
}

static int UtcToLocal(char *pcpTime)
{
	int ilRc = RC_SUCCESS;
	char *pclTdi;
	char clUtc[32];

	strcpy(clUtc,pcpTime);
	pclTdi = (strcmp(pcpTime,cgTich) < 0) ? cgTdi1 : cgTdi2;

	AddSecondsToCEDATime(pcpTime,(time_t)0+atol(pclTdi),1);
	dbg(DEBUG,"2LOCAL  : <%s> ",pcpTime);

	return ilRc;
}

#if 0
static int SetFisu(char *pcpRudResult)
{/*not detailly tested*/
	int ilRc = RC_SUCCESS;
 	short	slCursor;
	short	slSqlFunc;
	char	pclSelection[124];
	char	pclSqlBuf[128];
	char	pclDataArea[256];

	slSqlFunc = START;
	slCursor = 0;

	sprintf(pclSqlBuf, "SELECT FISU FROM RUETAB WHERE URNO='%s'",
			RUDFIELD(pcpRudResult, igRudURUE));

	ilRc = sql_if(slSqlFunc, &slCursor, pclSqlBuf, pclDataArea);
	close_my_cursor(&slCursor);

	if ((ilRc == RC_SUCCESS) && (strncmp(pclDataArea, "1", 1) != 0))
	{
		sprintf(pclSelection, "WHERE URNO='%s'",
				RUDFIELD(pcpRudResult, igRudURUE));

		ilRc = SendToSqlHdl("RUETAB", pclSelection, "FISU", "1");
	}
	return ilRc;
}
#endif

static int GetDebugLevel(char *pcpMode, int *pipMode)
{
	int		ilRC = RC_SUCCESS;
	char	clCfgValue[64];

	ilRC = iGetConfigRow(cgConfigFile, "SYSTEM", pcpMode, CFG_STRING,
							clCfgValue);

	if(ilRC != RC_SUCCESS)
	{
		dbg(TRACE,"SYSTEM %s not found in <%s>", pcpMode, cgConfigFile);
	}
	else
	{
		dbg(TRACE, "SYSTEM %s <%s>", pcpMode, clCfgValue);
		if (!strcmp(clCfgValue, "DEBUG"))
			*pipMode = DEBUG;
		else if (!strcmp(clCfgValue, "TRACE"))
			*pipMode = TRACE;
		else
			*pipMode = 0;
	}

	return ilRC;
}

static int ToDeleteOrNot(char *plpDemUrno, long plRow)
{
	int		ilRC = RC_SUCCESS;
	long	llFieldNo;
	char	clTmpFLGS[24];

	if (SearchJob(plpDemUrno) == RC_SUCCESS)
	{
		llFieldNo = -1;

		/* set FLGS[1] to 1 if old demand is not deleted because job exists */
		CEDAArrayGetField(&rgCCIDemArray.rrArrayHandle,
							rgCCIDemArray.crArrayName, &llFieldNo, "FLGS", 12,
							plRow,clTmpFLGS);
		strcat(clTmpFLGS,"          ");
		clTmpFLGS[1] = '1';
		clTmpFLGS[10] = '\0';
		CEDAArrayPutField(&rgCCIDemArray.rrArrayHandle,
 							rgCCIDemArray.crArrayName, &llFieldNo, "FLGS",
							plRow, clTmpFLGS);
	}
	else
		CEDAArrayDeleteRow(&rgCCIDemArray.rrArrayHandle,
							rgCCIDemArray.crArrayName, plRow);

	return ilRC;
}

static int RemoveDemands(char *pcpLstu)
{
	int		ilRC = RC_SUCCESS;
	char	*pclResult = NULL;
	char	clKeyDem[URNOLEN+1] = "";
	char	pclTomorrow[DATELEN+1];
	char	pclLstuUtc[DATELEN+1];
	long	llRow = ARR_LAST;
	int		ilDel=0;

	GetServerTimeStamp("LOC", 1, 0, pclTomorrow);
	pclTomorrow[8] = '\0';
	strcat(pclTomorrow, "000000");
	AddSecondsToCEDATime(pclTomorrow, 86400, 1);
	LocalToUtc ( pclTomorrow );

	strcpy ( pclLstuUtc, pcpLstu );
	LocalToUtc ( pclLstuUtc );


	dbg ( DEBUG, "RemoveDemands: where CDAT < %s and DEBE>= %s", pclLstuUtc, pclTomorrow );
	while( CEDAArrayFindRowPointer(&rgCCIDemArray.rrArrayHandle,
									rgCCIDemArray.crArrayName,
									&rgCCIDemArray.rrIdx01Handle,
									rgCCIDemArray.crIdx01Name,clKeyDem,
									&llRow,(void**)&pclResult) == RC_SUCCESS)
	{
		if ((strcmp(CCIDEMFIELD(pclResult, igDemCDAT), pclLstuUtc) < 0) &&
			(strcmp(CCIDEMFIELD(pclResult, igDemDEBE), pclTomorrow) >= 0))
		{
			ToDeleteOrNot(CCIDEMFIELD(pclResult, igDemURNO), llRow);
			ilDel++;
		}
		llRow = ARR_PREV;
	}
	dbg ( DEBUG, "RemoveDemands: %d CCI demands deleted/marked as invalid", ilDel );
	return ilRC;
}

static int JoinOthers(time_t *tpRealEnd, time_t tpMinEnd, time_t tpMaxEnd)
{
	int		ilRc = RC_SUCCESS;
	int		i,j;
	short	slStep;
	time_t	tlOtherStart,tlOtherEnd, tlLoopEnd;

	slStep = atoi(pcgStep) * 60;
	tlLoopEnd = tpMinEnd+igTOIgnore*60;
	for (i = *tpRealEnd; i <= tlLoopEnd; )
	{
		for (j = 0; j < igNwD; j++)
		{
			if (prgNwD[j].srState == NEWDEM)
			{
				/*StrToTime(prgNwD[j].crStartDemand,&tlOtherStart);
				StrToTime(prgNwD[j].crEndDemand,&tlOtherEnd);*/
				tlOtherStart = prgNwD[j].lrStartInSec;
				tlOtherEnd = prgNwD[j].lrEndInSec;

				if ( i == tlOtherStart &&
					((tpMaxEnd == 0) || (tlOtherEnd <= tpMaxEnd)))
				{ /* demand is coincided with the extension */
					prgNwD[j].srState = NOOP;
					i = tlOtherEnd - slStep;
					if ( *tpRealEnd < tlOtherEnd )
						*tpRealEnd = tlOtherEnd;
					dbg ( DEBUG, "JoinOthers: Integrated START <%s> End <%s>", 
						  prgNwD[j].crStartDemand, prgNwD[j].crEndDemand );
					tlLoopEnd = max ( tlLoopEnd, tlOtherEnd+igTOIgnore*60 );
					j = igNwD;
				}
			}
		}
		i += slStep;
	}
	return ilRc;
}

static int UpdateManualDemand ( char *pcpDemRow )
{
	char pclOURI[21] = "", pclOURO[21] = "", pclUrud[21];
	char pclDety[11], pclSelection[41]; 
	char pclDebe[21], pclDeen[21], pclAlid[21], pclDedu[21];
	char pclNow[DATELEN+1];
	int	 ilRc = RC_SUCCESS;
	char *pclRudRow=0;
	long llRudRow, llDemRow=ARR_CURRENT, llFieldNo=-1;
	BOOL blNewRule=FALSE, blSplitDone = FALSE;
	char clOldOURI[21], clOldOURO[21];

					if ( pcgInbound )
	GetDataItem(pclOURI,pcgInbound,igAftuItemNo,',',""," \0");
	TrimRight (pclOURI);
					if (pcgOutbound)
	GetDataItem(pclOURO,pcgOutbound,igAftuItemNo,',',""," \0");
	TrimRight (pclOURO);

	strcpy ( pclDety, OLDDEMFIELD(pcpDemRow,igDemDETY) );
	strcpy ( pclUrud, OLDDEMFIELD(pcpDemRow,igDemURUD) );
	strcpy ( clOldOURI, OLDDEMFIELD(pcpDemRow,igDemOURI) );
	TrimRight (clOldOURI);
	strcpy ( clOldOURO, OLDDEMFIELD(pcpDemRow,igDemOURO) );
	TrimRight (clOldOURO);
	/*  Check for dety=0, whether rotation changed */
	if ( !strcmp (pclDety, "0") && ( strcmp (clOldOURI, pclOURI ) || 
									 strcmp (clOldOURO, pclOURO ) ) )
	{	/*  rotation changed -> Split demand into dety=1 and dety=2 */
		dbg ( DEBUG, "UpdateManualDemand: Going to split man. demand DETY <%s> OURI <%s> OURO <%s>", 
			  OLDDEMFIELD(pcpDemRow,igDemDETY), OLDDEMFIELD(pcpDemRow,igDemOURI), 
			  OLDDEMFIELD(pcpDemRow,igDemOURO) );
		dbg ( DEBUG, "UpdateManualDemand: Flight data Inbound <%s> Outbound <%s>", pclOURI, pclOURO );
		ilRc = SplitManualDemand ( pcpDemRow, pclOURI, pclOURO );
		if ( ilRc != RC_SUCCESS )
			dbg ( DEBUG, "UpdateManualDemand: SplitManualDemand failed RC <%d>", ilRc );
		else
		{
			blSplitDone = TRUE;
			CEDAArrayGetRowPointer (&rgOldDemArray.rrArrayHandle,
									rgOldDemArray.crArrayName, llDemRow,
									(void *)&pcpDemRow) ;
			dbg ( DEBUG, "SplitManualDemand ok: Old Demand: URNO <%s> DETY <%s>",
						  OLDDEMFIELD(pcpDemRow,igDemURNO), OLDDEMFIELD(pcpDemRow,igDemDETY) );

		}
		strcpy ( pclDety, OLDDEMFIELD(pcpDemRow,igDemDETY) );
	}
	strcpy ( pclDedu, OLDDEMFIELD(pcpDemRow,igDemDEDU) );
	dbg ( DEBUG, "UpdateManualDemand: Dedu <%s>", pclDedu );
	/* set save flag */
	strcpy( OLDDEMFIELD(pcpDemRow,igDemSAVE), "1" );

	if ( !strstr ( cgIgnoreAloc, OLDDEMFIELD(pcpDemRow,igDemALOC) ) )
	{
		/*  Update allocation  */
		pclAlid[0] = '\0';
		GetAlidFromAloc (pclAlid, OLDDEMFIELD(pcpDemRow,igDemALOC),
						 OLDDEMFIELD(pcpDemRow,igDemDETY));
		llDemRow = ARR_CURRENT;
		llFieldNo = -1;
		CEDAArrayPutField(&rgOldDemArray.rrArrayHandle, 
						  rgOldDemArray.crArrayName, &llFieldNo,"ALID",llDemRow,
						  pclAlid );
	}
	else
		 dbg ( DEBUG, "UpdateManualDemand: No update of ALID for ALOC <%s>", 
			   OLDDEMFIELD(pcpDemRow,igDemALOC) );

	llRudRow = ARR_FIRST;
	/* if RUD-record for manual demand not already in rgRudArray, load record */
	if ( CEDAArrayFindRowPointer(&rgRudArray.rrArrayHandle, 
								 rgRudArray.crArrayName,
								 &rgRudArray.rrIdx01Handle,
								 rgRudArray.crIdx01Name,
								 pclUrud, &llRudRow, (void**)&pclRudRow ) != RC_SUCCESS )
	{
		blNewRule = TRUE;
		sprintf(pclSelection, "WHERE URNO='%s'", pclUrud );
		CEDAArrayRefill(&rgRudArray.rrArrayHandle,rgRudArray.crArrayName,
						pclSelection,"", ARR_NEXT);
		/* try again to get RUD-row */
		llRudRow = ARR_FIRST;
		ilRc = CEDAArrayFindRowPointer (&rgRudArray.rrArrayHandle, 
										rgRudArray.crArrayName,
										&rgRudArray.rrIdx01Handle,
										rgRudArray.crIdx01Name, pclUrud,
										&llRudRow, (void**)&pclRudRow );
		if ( ilRc != RC_SUCCESS )
			dbg ( TRACE, "UpdateManualDemand: Failed to load RUD-record <%s>", pclUrud );

	}
	if ( !pclRudRow )
		ilRc == RC_FAIL;
	if ( ilRc == RC_SUCCESS )
	{	/*  recalculate demand times */
		ilRc = GetDebeDeen ( pclRudRow, pclDety, pclDebe, pclDeen, pclDedu );
		if ( ilRc != RC_SUCCESS )
			dbg ( TRACE, "UpdateManualDemand: GetDebeDeen failed for man. demand <%s>, RC <%d>",
				  OLDDEMFIELD(pcpDemRow,igDemURNO), ilRc );
	}
	llDemRow = ARR_CURRENT;
	llFieldNo = -1;
	GetServerTimeStamp("UTC", 1, 0, pclNow);
	CEDAArrayPutField(&rgOldDemArray.rrArrayHandle, rgOldDemArray.crArrayName,
						&llFieldNo, "LSTU", llDemRow, pclNow);
	if ( ilRc == RC_SUCCESS )
	{	/* update demand demand times in record */
		llDemRow = ARR_CURRENT;
		llFieldNo = -1;
		CEDAArrayPutField(&rgOldDemArray.rrArrayHandle, 
						  rgOldDemArray.crArrayName, &llFieldNo,"DEBE",
						  llDemRow, pclDebe );
		dbg ( DEBUG, "UpdateManualDemand: CEDAArrayPutField <URNO> <%s> <DEBE> <%s> RC <%d>", 
					  OLDDEMFIELD(pcpDemRow,igDemURNO), pclDebe, ilRc );
		llDemRow = ARR_CURRENT;
		llFieldNo = -1;
		ilRc = CEDAArrayPutField(&rgOldDemArray.rrArrayHandle, 
						         rgOldDemArray.crArrayName, &llFieldNo,"DEEN",
						         llDemRow, pclDeen );
		dbg ( DEBUG, "UpdateManualDemand: CEDAArrayPutField <URNO> <%s> <DEEN> <%s> RC <%d>", 
					  OLDDEMFIELD(pcpDemRow,igDemURNO), pclDeen, ilRc );
	}
	llDemRow = ARR_CURRENT;
	llFieldNo = -1;
	CEDAArrayPutField(&rgOldDemArray.rrArrayHandle,rgOldDemArray.crArrayName,
					  &llFieldNo,"USEU",llDemRow, "DEMHDL" );
	if ( !blSplitDone )
	{
		llDemRow = ARR_CURRENT;
		llFieldNo = -1;
		CEDAArrayPutField(&rgOldDemArray.rrArrayHandle, 
						  rgOldDemArray.crArrayName, &llFieldNo,"OURI",
						  llDemRow, pclOURI );
		llDemRow = ARR_CURRENT;
		llFieldNo = -1;
		CEDAArrayPutField(&rgOldDemArray.rrArrayHandle, 
						  rgOldDemArray.crArrayName, &llFieldNo,"OURO",
						  llDemRow, pclOURO );
	}
	return ilRc;
}

static int GetDebeDeen ( char *pcpRudRow, char * pcpDety, char *pcpDebe, 
						 char *pcpDeen, char *pcpDedu )
{
	int ilItemNo = 0;
	int ilCol,ilPos,ilLc;
	char pclTime1[32];	/* time at the begin of demand */
	char pclTime2[32];	/* time at the end of demand */
	char pclTmpStr[32];
	int  ilRc = RC_FAIL;
	long	llDiff;
	long	llMinDedu;
	time_t	tlDebe;
	time_t	tlDeen;
	
	if (atoi(RUDFIELD(pcpRudRow,igRudUPDE)) == 0)
	{
		/************ calculate demand begin ****/
		if ( (*pcpDety == '1') || (*pcpDety == '0') )
		{	/*** calculate times for INBOUND-related demand ***************/
			ilRc = FindItemInList(INBOUNDTIMES, RUDFIELD(pcpRudRow, igRudRTDB),
		 						  ',', &ilItemNo, &ilCol, &ilPos);
			if (ilRc == RC_SUCCESS)
			{
				/************** duty reference time ******************/
				strcpy(pclTime1, InboundTimes[ilItemNo]);
				/*********** + offset = real duty begin *****************/
				SunCompStrAddTime( pclTime1, RUDFIELD(pcpRudRow, igRudTSDB),
								   pclTime1);
			}
			else
				dbg ( TRACE, "GetDebeDeen: Inbound reference time <%s> not supported", 
							  RUDFIELD(pcpRudRow, igRudRTDB) );
		}
		/************ calculate demand end ****/
		if ( (*pcpDety == '2') || (*pcpDety == '0') )
		{	/*** calculate times for OUTBOUND-related demand ***************/
			ilRc = FindItemInList(OUTBOUNDTIMES, RUDFIELD(pcpRudRow, igRudRTDE),
		 						  ',', &ilItemNo, &ilCol, &ilPos);
			if (ilRc == RC_SUCCESS)
			{
				/************** duty reference time ******************/
				strcpy(pclTime2,OutboundTimes[ilItemNo]);
				/*********** + offset = real duty end *****************/
				SunCompStrAddTime(pclTime2,	RUDFIELD(pcpRudRow,igRudTSDE),
								  pclTime2);
			}
			else
				dbg ( TRACE, "GetDebeDeen: Outbound reference time <%s> not supported", 
							  RUDFIELD(pcpRudRow, igRudRTDB) );

		}
		if ( *pcpDety == '1' )
		{
			/********* real duty begin + duty duration = duty end *************/
			SunCompStrAddTime(pclTime1,	pcpDedu, pclTime2);
		}
		if ( *pcpDety == '2' )
		{
			/********* real duty end - duty duration = duty begin *************/
			sprintf(pclTmpStr, "-%s", pcpDedu );
			SunCompStrAddTime(pclTime2,pclTmpStr,pclTime1);
		}

		/*********** substract setup time **********************/ 
		sprintf(pclTmpStr, "-%s", RUDFIELD(pcpRudRow, igRudSUTI));
		SunCompStrAddTime(pclTime1, pclTmpStr, pclTime1);
		/*********** substract time to go to ***************/
		sprintf(pclTmpStr, "-%s", RUDFIELD(pcpRudRow, igRudTTGT));
		SunCompStrAddTime(pclTime1, pclTmpStr, pclTime1);
		
		/*********** substract setup time **********************/ 
		SunCompStrAddTime(pclTime2,	RUDFIELD(pcpRudRow, igRudSDTI),
						  pclTime2);
		/*********** substract time to go from ***************/
		SunCompStrAddTime(pclTime2, RUDFIELD(pcpRudRow,igRudTTGF),
						  pclTime2);

		/*  for turnaround demands check min. demand length */
		if (*pcpDety == '0')
		{
			llMinDedu = atol(cgMinDedu);
			StrToTime(pclTime1, &tlDebe);
			StrToTime(pclTime2, &tlDeen);
			llDiff = tlDeen - tlDebe;
			dbg(DEBUG, "GetDebeDeen: Demandlength %ld, minimal length %ld", llDiff, llMinDedu);
			if (llDiff < llMinDedu)
			{
				SunCompStrAddTime(pclTime1, cgMinDedu, pclTime2 );
				dbg(DEBUG, "GetDebeDeen: Enlarged turnaround demand to min. length" );
			}
		}
		dbg(DEBUG, "GetDebeDeen: URUD <%s> DEBE <%s> DEEN <%s>", 
					RUDFIELD(pcpRudRow,igRudURNO), pclTime1, pclTime2 );
	}
	else
	{
		ilRc = GetDebeFromPrevDem(pclTime1, pclTime2, pcpRudRow);
		if (ilRc != RC_SUCCESS)
			dbg(TRACE, "GetDebeFromPrevDem: no previous demand found");

	}
	if ( ilRc == RC_SUCCESS )
	{
		strcpy ( pcpDebe, pclTime1 );
		strcpy ( pcpDeen, pclTime2 );
	}
	return ilRc;
}

/*  Split a manual turnaround demand into one inbound and one outbound	*/
/*  demand, because rotation did change									*/
/*  modified demand for current rotation stays in rgOldDemArray			*/
/*  second demand for other rotation is put in rgNewDemArray			*/
static int SplitManualDemand ( char *pcpDemRow, char *pcpOURI, char *pcpOURO )
{
	int ilRc = RC_SUCCESS;
	BOOL blInboundFilled = TRUE;
	BOOL blOutboundFilled = TRUE;
	int  ilDBFields, i ;
	char	clField[11], clValue[128], clOldOURI[11], clOldOURO[11];
	char	clTime1[21], clTime2[21];
	time_t	tlDebe, tlDeen;
	long	llDedu, llOldDedu, llTmp, llDemRow, llFieldNo;

	if ( !pcpOURI || ( atoi(pcpOURI)==0 ) )
		blInboundFilled = FALSE;
	if ( !pcpOURO || ( atoi(pcpOURO)==0 ) )
		blOutboundFilled = FALSE;

	InitializeNewDemand(pcgNewDemandBuf);
	dbg ( DEBUG, "SplitManualDemand: New demand initialized" );
	ilDBFields = get_no_of_items(pcgDemFields);
	/* copy pcpDemRow values to pcgNewDemandBuf */
	for ( i = 1; i <= ilDBFields; i++ )
	{
		get_real_item(clField, pcgDemFields, i);
		if ( !strcmp ( clField, "URNO" ) )
			continue;	/* don't overwrite URNO */
		strcpy(NEWDEMFIELD(pcgNewDemandBuf, i), OLDDEMFIELD(pcpDemRow,i) );
	}
	dbg ( DEBUG, "SplitManualDemand: Original values copied" );
	/* calculate original demand lenght */
	strcpy ( clTime1, OLDDEMFIELD(pcpDemRow,igDemDEBE) );
	strcpy ( clTime2, OLDDEMFIELD(pcpDemRow,igDemDEEN) );
	StrToTime(clTime1, &tlDebe);
	StrToTime(clTime2, &tlDeen);
	llDedu = tlDeen - tlDebe;
	llOldDedu = llDedu;  /* keep original DEDU */
	llTmp = atoi ( OLDDEMFIELD(pcpDemRow,igDemTTGT) );
	llDedu -= llTmp;
	llTmp = atoi ( OLDDEMFIELD(pcpDemRow,igDemSUTI) );
	llDedu -= llTmp;
	llTmp = atoi ( OLDDEMFIELD(pcpDemRow,igDemSDTI) );
	llDedu -= llTmp;
	llTmp = atoi ( OLDDEMFIELD(pcpDemRow,igDemTTGF) );
	llDedu -= llTmp;
	llDedu = max ( 5*60, llDedu /2 );	/* no demands smaller than 5 min */
	sprintf ( clValue, "%ld", llDedu );
	/* set dedu for both demands */
	llDemRow = ARR_CURRENT;
	llFieldNo = -1;
	CEDAArrayPutField(&rgOldDemArray.rrArrayHandle, 
					  rgOldDemArray.crArrayName, &llFieldNo,"DEDU",llDemRow,
					  clValue );
	strcpy( NEWDEMFIELD(pcgNewDemandBuf,igDemDEDU), clValue );

	dbg ( DEBUG, "SplitManualDemand: New demand initialized" );
	/* table at the end of this file explains new OURI,OURO, DETY of splitted demands */
	strcpy ( clOldOURI, OLDDEMFIELD(pcpDemRow,igDemOURI) );
	strcpy ( clOldOURO, OLDDEMFIELD(pcpDemRow,igDemOURO) );
	TrimRight ( clOldOURI );
	TrimRight ( clOldOURO );
	dbg ( DEBUG, "SplitManualDemand: new OURI <%s> OURO <%s> old OURI <%s> OURO <%s>",
		  pcpOURI, pcpOURO, clOldOURI, clOldOURO );
	if ( blInboundFilled && !strcmp(pcpOURI, clOldOURI ) )
	{	/* same inbound, case 1 or 3 in table */
		dbg ( DEBUG, "SplitManualDemand: Inbound keeps the same" );
		llDemRow = ARR_CURRENT;
		llFieldNo = -1;
		CEDAArrayPutField(&rgOldDemArray.rrArrayHandle, 
						  rgOldDemArray.crArrayName, &llFieldNo,"DETY",llDemRow,
						  "1" );
		llDemRow = ARR_CURRENT;
		llFieldNo = -1;
		CEDAArrayPutField(&rgOldDemArray.rrArrayHandle, 
						  rgOldDemArray.crArrayName, &llFieldNo,"OURO",llDemRow,
						  pcpOURO );
		strcpy( NEWDEMFIELD(pcgNewDemandBuf,igDemOURO), clOldOURO );
		strcpy( NEWDEMFIELD(pcgNewDemandBuf,igDemDETY), "2" );
		/* calculate demand begin for new outbound demand */
		sprintf ( clValue, "%ld", llOldDedu-llDedu );
		SunCompStrAddTime(clTime1,clValue,clTime1);
		strcpy( NEWDEMFIELD(pcgNewDemandBuf,igDemDEBE), clTime1 );
	}
	else
	{	/* same outbound, case 2 or 4 in table */
		dbg ( DEBUG, "SplitManualDemand: Outbound keeps the same" );
		llDemRow = ARR_CURRENT;
		llFieldNo = -1;
		CEDAArrayPutField(&rgOldDemArray.rrArrayHandle, 
						  rgOldDemArray.crArrayName, &llFieldNo,"OURI",llDemRow,
						  pcpOURI );
		llDemRow = ARR_CURRENT;
		llFieldNo = -1;
		CEDAArrayPutField(&rgOldDemArray.rrArrayHandle, 
						  rgOldDemArray.crArrayName, &llFieldNo,"DETY",llDemRow,
						  "2" );
		strcpy( NEWDEMFIELD(pcgNewDemandBuf,igDemOURI), clOldOURI );
		strcpy( NEWDEMFIELD(pcgNewDemandBuf,igDemDETY), "1" );
		/* calculate demand end for new inbound demand */
		sprintf ( clValue, "-%ld", llOldDedu-llDedu );
		SunCompStrAddTime(clTime2,clValue,clTime2);
		strcpy( NEWDEMFIELD(pcgNewDemandBuf,igDemDEEN), clTime2 );
	}
	dbg ( DEBUG, "SplitManualDemand: New Demand: URNO <%s> DETY <%s>",
		  NEWDEMFIELD(pcgNewDemandBuf,igDemURNO), NEWDEMFIELD(pcgNewDemandBuf,igDemDETY) );
	ilRc = AddNewDemandRow ();
	return ilRc;
}

static void InitDemFieldsFromRud ( char *pcpNewDemandBuf, char *pcpRudRow )
{
	strcpy(NEWDEMFIELD(pcpNewDemandBuf,igDemDBAR),
			RUDFIELD(pcpRudRow,igRudDBAR));
	strcpy(NEWDEMFIELD(pcpNewDemandBuf,igDemDEAR),
			RUDFIELD(pcpRudRow,igRudDEAR));
	strcpy(NEWDEMFIELD(pcpNewDemandBuf,igDemDEDU),
			RUDFIELD(pcpRudRow,igRudDEDU));
	strcpy(NEWDEMFIELD(pcpNewDemandBuf,igDemEADB),
			RUDFIELD(pcpRudRow,igRudEADB));
	strcpy(NEWDEMFIELD(pcpNewDemandBuf,igDemHOPO),
			RUDFIELD(pcpRudRow,igRudHOPO));
	strcpy(NEWDEMFIELD(pcpNewDemandBuf,igDemLADE),
			RUDFIELD(pcpRudRow,igRudLADE));
	strcpy(NEWDEMFIELD(pcpNewDemandBuf,igDemRTDB),
			RUDFIELD(pcpRudRow,igRudRTDB));
	strcpy(NEWDEMFIELD(pcpNewDemandBuf,igDemRTDE),
			RUDFIELD(pcpRudRow,igRudRTDE));
	strcpy(NEWDEMFIELD(pcpNewDemandBuf,igDemSDTI),
			RUDFIELD(pcpRudRow,igRudSDTI));
	strcpy(NEWDEMFIELD(pcpNewDemandBuf,igDemSUTI),
			RUDFIELD(pcpRudRow,igRudSUTI));
	strcpy(NEWDEMFIELD(pcpNewDemandBuf,igDemTSDB),
			RUDFIELD(pcpRudRow,igRudTSDB));
	strcpy(NEWDEMFIELD(pcpNewDemandBuf,igDemTSDE),
			RUDFIELD(pcpRudRow,igRudTSDE));
	strcpy(NEWDEMFIELD(pcpNewDemandBuf,igDemTTGF),
			RUDFIELD(pcpRudRow,igRudTTGF));
	strcpy(NEWDEMFIELD(pcpNewDemandBuf,igDemTTGT),
			RUDFIELD(pcpRudRow,igRudTTGT));
	strcpy(NEWDEMFIELD(pcpNewDemandBuf,igDemRETY),
			RUDFIELD(pcpRudRow,igRudRETY));
	strcpy(NEWDEMFIELD(pcpNewDemandBuf,igDemUDGR),
			RUDFIELD(pcpRudRow,igRudUDGR));
	strcpy(NEWDEMFIELD(pcpNewDemandBuf,igDemALOC),
			RUDFIELD(pcpRudRow,igRudALOC));
	strcpy(NEWDEMFIELD(pcpNewDemandBuf,igDemURUD),
			RUDFIELD(pcpRudRow,igRudURNO));

	if ( (igRudUSES>0) && (igDemUSES>0) )
		strcpy( NEWDEMFIELD(pcpNewDemandBuf,igDemUSES),
				RUDFIELD(pcpRudRow,igRudUSES));
	if ( (igRudUTPL>0) && (igDemUTPL>0) )
		strcpy( NEWDEMFIELD(pcpNewDemandBuf,igDemUTPL),
				RUDFIELD(pcpRudRow,igRudUTPL));

	if ( (igRudRECO>0) && (igDemRECO>0) )
		strcpy( NEWDEMFIELD(pcpNewDemandBuf,igDemRECO),
				RUDFIELD(pcpRudRow,igRudRECO));
	if ( (igRudQUCO>0) && (igDemQUCO>0) )
		strcpy( NEWDEMFIELD(pcpNewDemandBuf,igDemQUCO),
				RUDFIELD(pcpRudRow,igRudQUCO));
	if ( (igRudUGHS>0) && (igDemUGHS>0) )
		strcpy( NEWDEMFIELD(pcpNewDemandBuf,igDemUGHS),
				RUDFIELD(pcpRudRow,igRudUGHS));


}

static int	HandleIDR(char *pcpFields, char *pcpData)
{
	int		ilRc = RC_SUCCESS;
	int		ilPos = 0, ilFieldIdx, ilCol, i;
	int		ilNoOfItems = 0;
	char	clUrno[URNOLEN+1], clUrud[URNOLEN+1];
	char	clField[11], clValue[128];
	char	pclSqlBuf[128],pclDataArea[2048];
	short	slLocalCursor,slFuncCode;
	char	*pclRudRow = NULL;
	long	llAction = ARR_FIRST, llRowCount;
	
	memset(pclSqlBuf,0,sizeof(pclSqlBuf));
	memset(pclDataArea,0,sizeof(pclDataArea));

	slFuncCode = START;
	slLocalCursor = 0;
	ilPos = get_item_no(pcpFields, "URNO", 5);
	if (ilPos >= 0)
	{
		get_real_item(clUrno, pcpData, ilPos+1);
        sprintf(pclSqlBuf, "SELECT * FROM DEMTAB WHERE URNO = '%s'", clUrno);
		ilRc = sql_if(slFuncCode,&slLocalCursor,pclSqlBuf,pclDataArea);
		close_my_cursor(&slLocalCursor);
	}

	if (ilRc != RC_SUCCESS)
	{
		dbg(DEBUG,"Create Demand");

		CEDAArrayDelete(&rgNewDemArray.rrArrayHandle,rgNewDemArray.crArrayName);
		CEDAArrayDelete(&rgOldDemArray.rrArrayHandle,rgOldDemArray.crArrayName);
		InitializeNewDemand(pcgNewDemandBuf);

		ilPos = get_item_no(pcpFields, "URUD", 5);
		if (ilPos >= 0)
		{	/* fill DEMTAB fields from RUDTAB */
			get_real_item(clUrud, pcpData, ilPos+1);
			TrimRight ( clUrud );
			if ( clUrud[0] && ( clUrud[0]!=' ' ) )
			{	/* URUD filled by calling process */
				CEDAArrayDelete(&rgRudArray.rrArrayHandle, rgRudArray.crArrayName);	
				
				memset(pclSqlBuf,0,sizeof(pclSqlBuf));
				sprintf(pclSqlBuf,"WHERE URNO='%s'", clUrud );
				CEDAArrayRefill(&rgRudArray.rrArrayHandle,rgRudArray.crArrayName,
								pclSqlBuf,"",llAction);

				CEDAArrayGetRowCount(&rgRudArray.rrArrayHandle,
									rgRudArray.crArrayName,&llRowCount);

				llAction = ARR_FIRST;
				if ( llRowCount >= 1 )
				{	
					SetRudLogicalFields ( &rgRudArray, clUrud );
					if ( CEDAArrayGetRowPointer(&rgRudArray.rrArrayHandle,
										        rgRudArray.crArrayName, llAction,
												 (void *)&pclRudRow) == RC_SUCCESS)
					{   
						InitDemFieldsFromRud ( pcgNewDemandBuf, pclRudRow );
					}
				}
			}
		}
		/*  fill DEMTAB fields with given values */
		ilNoOfItems = get_no_of_items(pcpData);
		for ( i = 0; i < ilNoOfItems; i++ )
		{
			get_real_item(clField, pcpFields, i+1);
			get_real_item(clValue, pcpData, i+1);
			FindItemInList(pcgDemFields,clField,',',&ilFieldIdx,&ilCol,&ilPos);
			if ( ilFieldIdx>0 )
			{
				strcpy(NEWDEMFIELD(pcgNewDemandBuf, ilFieldIdx), clValue );
			}
		}
		/*  set flag for manually created demand */
		strcpy(clValue, NEWDEMFIELD(pcgNewDemandBuf, igDemFLGS));
		strcat ( clValue, "          " );
		clValue[6] = '1';
		clValue[10] = '\0';
		strcpy(NEWDEMFIELD(pcgNewDemandBuf, igDemFLGS), clValue );

		RecalcDedu ( pcgNewDemandBuf );
		if (strstr(pcpFields,"HOPO") == NULL)
			strcpy(NEWDEMFIELD(pcgNewDemandBuf, igDemHOPO), cgHopo );
		ilRc = AddNewDemandRow ();
		/*
		*pcgTmpBuf = '\0';
		pclTmpBuf = pcgTmpBuf;
		for(i = 1; i <= rgNewDemArray.lrArrayFieldCnt; i++)
		{
			*clField = '\0';
			GetDataItem(clField,rgNewDemArray.crArrayFieldList,i,',',"","\0\0");
			strcpy(pclTmpBuf,NEWDEMFIELD(pcgNewDemandBuf,i)); 
			dbg(TRACE, "%s <%s> <%s>", clField,
				       NEWDEMFIELD(pcgNewDemandBuf,i),pclTmpBuf);
			pclTmpBuf += strlen(pclTmpBuf)+1;
		}
		llAction = ARR_FIRST;
		ilRc = CEDAArrayAddRow(&rgNewDemArray.rrArrayHandle,
										rgNewDemArray.crArrayName,
										&llAction, (void *)pcgTmpBuf);*/
		if ( ilRc != RC_SUCCESS )
			dbg ( TRACE, "HandleIDR: AddNewDemandRow failed, RC <%d>", ilRc );
		else
		{
			ilRc = SaveDemands (SINGLE_TO_BCHDL |SINGLE_TO_ACTION);
			if ( ilRc != RC_SUCCESS )
				dbg ( TRACE, "HandleIDR: SaveDemands failed, RC <%d>", ilRc );
		}
	}
	else
	{
		dbg(TRACE, "URNO %s already exists", clUrno);
		ilRc = RC_FAIL;
	}


	return ilRc;
}

/*  Adds a new demand from pcgNewDemandBuf to rgNewDemArray */
static int AddNewDemandRow ()
{
	int		i, ilRc;
	char	*pclTmpBuf;
	long	llAction = ARR_FIRST;
	char	clField[11];

	*pcgTmpBuf = '\0';
	pclTmpBuf = pcgTmpBuf;
	for(i = 1; i <= rgNewDemArray.lrArrayFieldCnt; i++)
	{
		*clField = '\0';
		GetDataItem(clField,rgNewDemArray.crArrayFieldList,i,',',"","\0\0");
		strcpy(pclTmpBuf,NEWDEMFIELD(pcgNewDemandBuf,i)); 
		/*dbg(DEBUG, "%s <%s> <%s>", clField,
				   NEWDEMFIELD(pcgNewDemandBuf,i),pclTmpBuf);*/
		pclTmpBuf += strlen(pclTmpBuf)+1;
	}
	ilRc = CEDAArrayAddRow(&rgNewDemArray.rrArrayHandle,
									rgNewDemArray.crArrayName,
									&llAction, (void *)pcgTmpBuf);
	if ( ilRc != RC_SUCCESS )
		dbg ( DEBUG, "AddNewDemandRow: RC <%ld>", ilRc );
	return ilRc;
}

/*  Adds a new demand from pcgCCIDemBuf to rgCCIDemArray */
static int AddNewCCIDemandRow ()
{
	int		i, ilRc;
	char	*pclTmpBuf;
	long	llAction = ARR_FIRST;
	char	clField[11];

	*pcgTmpBuf = '\0';
	pclTmpBuf = pcgTmpBuf;
	for(i = 1; i <= rgCCIDemArray.lrArrayFieldCnt; i++)
	{
		*clField = '\0';
		GetDataItem(clField,rgCCIDemArray.crArrayFieldList,i,',',"","\0\0");
		strcpy(pclTmpBuf,CCIDEMFIELD(pcgCCIDemBuf,i)); 
		/*dbg(DEBUG, "%s <%s> <%s>", clField,
				   NEWDEMFIELD(pcgCCIDemBuf,i),pclTmpBuf);*/
		pclTmpBuf += strlen(pclTmpBuf)+1;
	}
	ilRc = CEDAArrayAddRow( &rgCCIDemArray.rrArrayHandle,
							rgCCIDemArray.crArrayName,
							&llAction, (void *)pcgTmpBuf);
	if ( ilRc != RC_SUCCESS )
		dbg ( DEBUG, "AddNewCCIDemandRow: RC <%ld>", ilRc );
	return ilRc;
}

static int RecalcDedu ( char *pcpDemandBuf )
{
	char	clDedu[21], clTime1[21], clTime2[21], clDety[11];
	time_t	tlDebe, tlDeen;
	long	llDedu=0, llTmp;
	int		ilRc = RC_SUCCESS;


	strcpy ( clDety, NEWDEMFIELD(pcpDemandBuf,igDemDETY) );
	strcpy ( clDedu, NEWDEMFIELD(pcpDemandBuf,igDemDEDU) );
	if ( atoi (clDety) != 0 )
	{	
		strcpy ( clTime1, NEWDEMFIELD(pcpDemandBuf,igDemDEBE) );
		strcpy ( clTime2, NEWDEMFIELD(pcpDemandBuf,igDemDEEN) );
		ilRc = StrToTime(clTime1, &tlDebe);
		ilRc |= StrToTime(clTime2, &tlDeen);
		if ( ilRc != RC_SUCCESS )
		{
			dbg ( "RecalcDedu: StrToTime failed DEBE <%s>  DEEN <%s>", 
				  clTime1, clTime2 );
			return RC_FAIL;
		}
		llDedu = tlDeen - tlDebe;
		
		llTmp = atoi ( NEWDEMFIELD(pcpDemandBuf,igDemTTGT) );
		llDedu -= llTmp;
		llTmp = atoi ( NEWDEMFIELD(pcpDemandBuf,igDemSUTI) );
		llDedu -= llTmp;
		llTmp = atoi ( NEWDEMFIELD(pcpDemandBuf,igDemSDTI) );
		llDedu -= llTmp;
		llTmp = atoi ( NEWDEMFIELD(pcpDemandBuf,igDemTTGF) );
		llDedu -= llTmp;
	}
	llTmp = atoi ( clDedu );
	if ( llTmp != llDedu )
	{
		dbg ( DEBUG, "RecalcDedu: Change DEDU Old <%s> New <%ld> Dety <%s>", 
			  clDedu, llDedu, clDety );
		sprintf ( clDedu, "%ld", llDedu );
		strcpy( NEWDEMFIELD(pcpDemandBuf,igDemDEDU), clDedu );
	}
	return RC_SUCCESS;
}

/*
static int AddRowToRudArray ( long lpRow )
{
	long	*pllFields=0;
	int ilRC;
	long llRowWrite;

	dbg ( DEBUG, "AddRowToRudArray: Start Row <%ld>", lpRow );
	pllFields = (long*)calloc( rgCCIRudArray.lrArrayFieldCnt, sizeof(long) );
	if ( !pllFields )
	{
		dbg ( TRACE, "AddRowToRudArray: calloc failed" );
		return RC_NOMEM;
	}
	pllFields[0] = -1;
	memset ( cgDataArea, 0, sizeof(cgDataArea) );
	ilRC = CEDAArrayGetFields( &rgCCIRudArray.rrArrayHandle,
							   rgCCIRudArray.crArrayName, pllFields, 
				  			   rgCCIRudArray.crArrayFieldList, ',',
							   rgCCIRudArray.lrArrayRowLen, lpRow,
							   (void*)cgDataArea);
	if ( ilRC == RC_SUCCESS )
	{
		memset ( pcgTmpBuf, 0, sizeof(pcgTmpBuf) );
		llRowWrite = ARR_LAST;
		ilRC = CEDAArrayAddRow( &rgRudArray.rrArrayHandle,
								rgRudArray.crArrayName, 
								&llRowWrite, (void *)pcgTmpBuf);
		if ( ilRC != RC_SUCCESS )
			dbg ( TRACE, "AddRowToRudArray: CEDAArrayAddRow failed to add row" );
		else
		{
			ilRC = CEDAArrayPutFields( &rgRudArray.rrArrayHandle,
									   rgRudArray.crArrayName, 0, &llRowWrite, 
									   rgRudArray.crArrayFieldList,cgDataArea );
			if ( ilRC != RC_SUCCESS )
				dbg ( TRACE, "AddRowToRudArray: CEDAArrayPutFields failed to set row\n<%s>", 
					  cgDataArea );
		}
	}
	else
		dbg ( TRACE, "AddRowToRudArray: CEDAArrayGetFields failed for row <%d>", lpRow );

	if ( pllFields )
		free ( pllFields  );
	return ilRC;
}

*/
/********************************************************************/
static int GetNextUrno(char *pcpUrno)
{
	int ilRC = RC_SUCCESS ;
	char  pclDataArea[IDATA_AREA_SIZE] ;
	
	if (igReservedUrnoCnt <= 0)
	{                            /* hole Urno buffer -> get new Urnos */
		memset( pclDataArea, 0, IDATA_AREA_SIZE) ;
		if ((ilRC = GetNextValues(pclDataArea, URNOS_TO_FETCH)) != RC_SUCCESS)
		{
			dbg ( TRACE,"GetNextValues failed RC <%d>", ilRC );
		}
		else
		{
			strcpy ( pcpUrno, pclDataArea );
			igReservedUrnoCnt = URNOS_TO_FETCH - 1 ;
			lgActUrno = atol(pcpUrno);
			dbg ( DEBUG, "GetNextUrno: fetched %d Urnos. Next is <%s>", 
				  URNOS_TO_FETCH, pcpUrno ) ;
		}
	}
	else
	{
		igReservedUrnoCnt-- ;
		lgActUrno++ ;
		sprintf ( pcpUrno, "%ld", lgActUrno );      /* get next reserved Urno */
		dbg ( DEBUG, "GetNextUrno: Next Urno <%s>, %d Urnos remaining", 
			  pcpUrno, igReservedUrnoCnt ) ;
	}
	
	return (ilRC) ;
} /* getNextUrno () */

static BOOL SetFisuIfNecessary(char *pcpRueUrno)
{
	char	clRueUrno[24];
	int		ilFound, ilFisu = 0, ilRc = RC_SUCCESS;
	char	*pclResult = 0;
	long	llRow = ARR_FIRST;
	/* char	clSelection[124]; */
	static long	llFieldNo = -1;
	BOOL	blSet = FALSE;
		
	strcpy( clRueUrno, pcpRueUrno );
	TrimRight ( clRueUrno );
	ilFound = CEDAArrayFindRowPointer(&rgRueArray.rrArrayHandle, rgRueArray.crArrayName,
									  &rgRueArray.rrIdx01Handle, rgRueArray.crIdx01Name, 
									  clRueUrno ,&llRow, (void *)&pclResult );
	if ( ilFound == RC_SUCCESS )
	{
		ilFisu = atoi ( RUEFIELD ( pclResult, 2 ) );
	}
	if ( ilFisu == 1 )
		dbg ( TRACE, "SetFisuIfNecessary: Fisu already set for rule <%s>", clRueUrno );
	else
	{
		/*
		sprintf(clSelection,"WHERE URNO='%s'", clRueUrno );
		dbg(DEBUG,"SetFisuIfNecessary: <%s>",clSelection);
		ilRc = SendToSqlHdl("RUETAB",clSelection, "FISU","1");
		if ( ilRc != RC_SUCCESS )
			dbg ( TRACE, "SetFisuIfNecessary: SendToSqlHdl failed <%d>", ilRc );
		else
		{*/
			if ( ilFound != RC_SUCCESS )
			{
				llRow = ARR_LAST;
				ilRc = CEDAArrayAddRow( &rgRueArray.rrArrayHandle,
									    rgRueArray.crArrayName,
										&llRow, (void *)clRueUrno );
				dbg ( DEBUG, "SetFisuIfNecessary: CEDAArrayAddRow returned <%d>", ilRc );
			}
			if ( ilRc == RC_SUCCESS)
			{
				ilRc = CEDAArrayPutField( &rgRueArray.rrArrayHandle, 
										  rgRueArray.crArrayName,
										  &llFieldNo, "FISU", llRow, "1" );
				dbg ( DEBUG, "SetFisuIfNecessary: Setting FISU=1 for rule <%s> RC <%d>", 
					  clRueUrno, ilRc );
				blSet = ( ilRc == RC_SUCCESS );
			}
		/*}*/
	}
	return blSet;
}

static int DoSendSBC ( char *pcpStart, char *pcpEnd, char *pcpTpl, 
					   char *pcpDety, char *pcpWks )
{
	char	*pclData;
	int		ilRC = RC_SUCCESS, ilLen;
	char	clTwEnd[33];


	if ( !pcpStart || !pcpEnd || !pcpTpl || !pcpDety || !pcpWks )
		return RC_FAIL;

	ilLen = strlen(pcpStart) + strlen(pcpEnd) + strlen(pcpTpl) + 24;
	pclData = malloc( ilLen );

	if ( !pclData )
		ilRC = RC_FAIL;

	strncpy ( pclData, pcpStart, 14 );
	pclData[14]='\0';
	strcat ( pclData, "-" );
	ilLen = strlen ( pclData );
	strncpy ( &(pclData[ilLen]), pcpEnd, 14 );
	pclData[14+ilLen]='\0';
	strcat ( pclData, pcpTpl );
	strcat ( pclData, "\n{=NOTCP=}" );
	
	dbg ( TRACE, "DoSendSBC: Data <%s>", pclData );
	sprintf (clTwEnd, "%s,%s,%s", cgHopo,cgTabEnd, cgProcessName);
	ilRC = tools_send_info_flag( 1900,0, "BCHDL", "DEMHDL", pcpWks, "", 
								 "", "", clTwEnd, "SBC","RELDEM",pcpDety,
								 "DEBE,DEEN|UTPL",pclData,0);          
	if ( pclData )
		free ( pclData );

	dbg ( TRACE, "DoSendSBC: RC <%d>", ilRC );
	return ilRC;
}


static int AddRowToRudArray ( long lpRow )
{
	int ilRC;
	long llRowWrite = ARR_LAST;
	char *pclRudRow=0;

	dbg ( DEBUG, "AddRowToRudArray: Start Row <%ld>", lpRow );

	ilRC = CEDAArrayGetRowPointer(&rgCCIRudArray.rrArrayHandle, rgCCIRudArray.crArrayName, 
								  lpRow, (void*)&pclRudRow );
	if ( ilRC == RC_SUCCESS )
	{
		ilRC = CEDAArrayAddRow (&rgRudArray.rrArrayHandle, rgRudArray.crArrayName, 
								&llRowWrite, (void*)pclRudRow );
		if ( ilRC != RC_SUCCESS )
			dbg ( TRACE, "AddRowToRudArray: CEDAArrayAddRow failed" );
	}
	else
		dbg ( TRACE, "AddRowToRudArray: CEDAArrayGetRowPointer failed for row <%d>", lpRow );

	return ilRC;
}


static int ReadJobtab ( ARRAYINFO *prpDemArray )
{
	long llReadFunc = ARR_FIRST;
	long llFillFunc = ARR_FIRST;
	int	 ilUrnos, ilRc ;
	char *pclDemRow = NULL, *ps;
	long llRowCount;
	ARRAYINFO *prlArr;

	prlArr = bgUseJODTAB ? &rgJodArray : &rgJobArray;

	ilUrnos =0;
	strcpy ( cgDataArea, "WHERE UDEM IN (");
	ps = cgDataArea + strlen(cgDataArea);

	CEDAArrayGetRowCount(&(prpDemArray->rrArrayHandle),
						 prpDemArray->crArrayName,&llRowCount );
	while(CEDAArrayGetRowPointer(&(prpDemArray->rrArrayHandle),
								  prpDemArray->crArrayName,
								  llReadFunc,(void *)&pclDemRow) == RC_SUCCESS)
	{
		if	( ilUrnos % 620 != 0 )
		{
			*ps = ',';
			ps ++;
		}
		*ps = '\'';
		ps++;
		strcpy ( ps, OLDDEMFIELD(pclDemRow,igDemURNO) ) ;
		ps += strlen (ps);
		*ps = '\'';
		ps++;
		ilUrnos++;
		llReadFunc = ARR_NEXT;
		if ( ( ilUrnos % 620 == 0 )  || (llRowCount<=ilUrnos) )
		{
			*ps = ')';
			ps++;
			*ps = '\0';
			/*dbg ( DEBUG, "ReadJobtab: Read DB with selection: <%s>", cgDataArea );*/
			ilRc = CEDAArrayRefill (&(prlArr->rrArrayHandle), prlArr->crArrayName,
									cgDataArea, NULL, llFillFunc );
			strcpy ( cgDataArea, "WHERE UDEM IN (");
			ps = cgDataArea + strlen(cgDataArea);
			llFillFunc = ARR_NEXT;
		}
	}
	if ( ilUrnos == 0 )
		ilRc = CEDAArrayDelete ( &(prlArr->rrArrayHandle), prlArr->crArrayName );

	CEDAArrayGetRowCount(&(prlArr->rrArrayHandle), prlArr->crArrayName,&llRowCount);
	dbg ( DEBUG, "ReadJobtab: %ld records read", llRowCount );
	return ilRc;	
}

static int SearchJob (char *pcpDemUrno)
{
	long llRowNum = ARR_FIRST;
	int ilRc ;
	char *pclRow;
	ARRAYINFO *prlArr;

	prlArr = bgUseJODTAB ? &rgJodArray : &rgJobArray;
	ilRc = CEDAArrayFindRowPointer( &(prlArr->rrArrayHandle), prlArr->crArrayName,
									&prlArr->rrIdx01Handle,	prlArr->crIdx01Name, 
									pcpDemUrno, &llRowNum, (void *)&pclRow );

	/* dbg(DEBUG,"SearchJob: demand URNO <%s> ilRc=%d", pcpDemUrno, ilRc); */
	return ilRc;
}

/* *******************************************************/
static void CheckPerformance (int ipStart, char *pcpRtTime)
{
	time_t tlQueTime, tlStampDiff, tlNowTime;
	int ilOldDbgLevel = debug_level;

	if (tgMaxStampDiff > 0)
	{
		if (ipStart == TRUE)
		{
			tgBeginStamp = time (0L);
			tgQueDiff = 0;
			if (pcpRtTime && (pcpRtTime[0] >= '0') && (pcpRtTime[0] <= '9'))
			{
				tlNowTime = tgBeginStamp % 10000;
				tlQueTime = atol (pcpRtTime);
				tgQueDiff = tlNowTime - tlQueTime;
				if (tgQueDiff > tgMaxStampDiff)
				{
					if ( debug_level < TRACE )
					{
						debug_level = TRACE;
						bgShowEvent = TRUE;
					}
					dbg (TRACE, "===> PERF_CHECK: EVENT QUEUE_TIME (%d SEC) <===", tgQueDiff);
				}                       
			}                         
		}  
		else
		{
			tgEndStamp = time (0L);
			tlStampDiff = tgEndStamp - tgBeginStamp;
			if ( tlStampDiff > tgMaxStampDiff )
			{		
				if ( debug_level < TRACE )
				{
					debug_level = TRACE;
					bgShowEvent = TRUE;
				}
				dbg (TRACE, "===> PERF_CHECK: DEMHDL  EVENT PROCESSING (%d SEC) <===", tlStampDiff);
			}
			else
				dbg (TRACE, "----- DEMHDL  EVENT PROCESSING (%d SEC)", tlStampDiff);
			if ((tgQueDiff > 0) && (tlStampDiff > 0))
			{
				tlStampDiff += tgQueDiff;
			    if (tlStampDiff > tgMaxStampDiff)
			    {
					if ( debug_level < TRACE )
					{
						debug_level = TRACE;
						bgShowEvent = TRUE;
					}
					dbg (TRACE, "===> PERF_CHECK: TOTAL QUEUE_TIME (%d SEC) <===", tlStampDiff);
			    }
				else
					dbg (TRACE, "----- TOTAL QUEUE_TIME (%d SEC)", tlStampDiff);
			}    
		}      
		debug_level = ilOldDbgLevel;
	}        
	return;
}                               /* end CheckPerformance */


static int HandleJobDelete ( char *pcpFields, char *pcpData )
{
	/*  if job had been on a invalid demand ("invalid job/demand combination" conflict	*/
	/*  delete the demand, if there are no more jobs									*/
	int		ilRc = RC_SUCCESS;
	int		ilPos = 0 ;
	char	clUdem[URNOLEN+1]="";
	char    pclSelection[81], *pclDel;
	long	llRowCount=0, llAction = ARR_FIRST;
	ARRAYINFO *prlArr;

	prlArr = bgUseJODTAB ? &rgJodArray : &rgJobArray;

	ilPos = get_item_no(pcpFields, "UDEM", 5);
	if (ilPos < 0)
		dbg ( DEBUG, "HandleJobDelete: UDEM not found in <%s>", pcpFields );
	else
	{
		pclDel = strchr ( pcpData, '\n' );
		if ( !pclDel )
		{
			ilRc = RC_NODATA;
		}
		else
		{
			pclDel ++;
			get_real_item(clUdem, pclDel, ilPos+1);
			dbg ( DEBUG, "HandleJobDelete: Found UDEM <%s>", clUdem );
			TrimRight ( clUdem );
			sprintf ( pclSelection, "WHERE URNO='%s' and substr(FLGS,2,1)='1'", clUdem );
			dbg ( DEBUG, "HandleJobDelete: Found UDEM <%s>", clUdem );
			ilRc = CEDAArrayRefill(&rgOldDemArray.rrArrayHandle,rgOldDemArray.crArrayName,
									pclSelection,"",llAction);
			if ( ilRc != RC_SUCCESS )
				dbg ( TRACE, "HandleJobDelete: CEDAArrayRefill with selection <%s> failed RC <%d>",
							 pclSelection, ilRc );
			else
			{
				CEDAArrayGetRowCount(&rgOldDemArray.rrArrayHandle, rgOldDemArray.crArrayName,&llRowCount);
				dbg ( DEBUG, "HandleJobDelete: CEDAArrayRefill with <%s>: %ld records found",
							 pclSelection, llRowCount );
				if ( llRowCount > 0 )
				{
					ilRc = ReadJobtab ( &rgOldDemArray );				
					if ( ilRc != RC_SUCCESS )
						dbg ( TRACE, "HandleJobDelete: ReadJobtab failed RC <%d>", ilRc );
					else
					{
						CEDAArrayGetRowCount(&(prlArr->rrArrayHandle), prlArr->crArrayName,&llRowCount);
						if ( llRowCount == 0 )
						{	/*  no more jobs on this demand -> delete it */
							dbg ( TRACE, "HandleJobDelete: DEMAND <%s> can be deleted", clUdem );
							ilRc = CEDAArrayDeleteRow(&rgOldDemArray.rrArrayHandle,
													  rgOldDemArray.crArrayName,ARR_FIRST );
							if ( ilRc != RC_SUCCESS )
								dbg ( TRACE, "HandleJobDelete: CEDAArrayDeleteRow failed RC <%d>", ilRc );
							else
							{
								SaveDemands( SINGLE_TO_BCHDL |SINGLE_TO_ACTION );
							}
						}
					}
				}
			}
		}
	}
	dbg ( DEBUG, "HandleJobDelete: UDEM <%s> ilRc <%d>", clUdem, ilRc );
	return ilRc;
}

static int GetTotalRowLength( char *pcpFieldList, ARRAYINFO *prpArray, long *plpLen)
{
    int	 ilRc        = RC_SUCCESS;			/* Return code */
    int  ilNoOfItems = 0;
    int  ilItemNo = 0;
    int  ilLoop      = 0;
    long llRowLen    = 0;
    long llFldLen    = 0;
    char clFina[8];
	
    if (prpArray != NULL && pcpFieldList != NULL)
    {
		ilNoOfItems = get_no_of_items(pcpFieldList);
			
		ilLoop = 1;
		do
		{
			get_real_item(clFina,pcpFieldList,ilLoop);

			ilItemNo = get_item_no(prpArray->crArrayFieldList, clFina, 5);
			/*if(GetItemNo(clFina,prpArray->crArrayFieldList,&ilItemNo) == RC_SUCCESS) */
			if (ilItemNo >= 0)
			{
				llRowLen++;
				llRowLen += prpArray->plrArrayFieldLen[ilItemNo];
				dbg(DEBUG,"GetTotalRowLength: clFina <%s> Length <%ld>",clFina,prpArray->plrArrayFieldLen[ilItemNo]);
			}
			else
				ilRc == RC_FAIL;
			ilLoop++;
		}while(ilLoop <= ilNoOfItems);
	}
	if(ilRc == RC_SUCCESS)
	{
		*plpLen = llRowLen;
	}/* end of if */

	dbg(DEBUG,"GetTotalRowLength:  FieldList <%s> Length <%ld>",pcpFieldList,*plpLen);
	return(ilRc);
	
} /* end of GetTotalRowLength */

	
static int ReadConfigEntries ()
{
	int ilRc, ilValue;

	ilRc = iGetConfigRow ( cgConfigFile, "MAIN", "LoadFormat", CFG_STRING, cgCfgBuffer );
	if ( ilRc == RC_SUCCESS )
	{
		if ( strstr ( cgCfgBuffer, "RUD" ) )
			igLoadSel = 1;
		else if ( strstr ( cgCfgBuffer, "DEM" ) )
			igLoadSel = 2;
	}
	dbg(TRACE,"ReadConfigEntries: Using format Nr. %d to load old demands", igLoadSel );
	lgBCDays = -1;
	if ( ( iGetConfigRow ( cgConfigFile, "MAIN", "BroadCastDays", CFG_STRING, cgCfgBuffer ) == RC_SUCCESS ) &&
		  ( sscanf ( cgCfgBuffer, "%d", &ilValue ) > 0 ) )
	{
		dbg(TRACE,"ReadConfigEntries: Broadcasting within %d days", ilValue );
		lgBCDays = ilValue * 24 * 3600;
	}
	else
		dbg(TRACE,"ReadConfigEntries: No time frame for broadcasting defined !" );

	if ( iGetConfigRow ( cgConfigFile, "MAIN","HandlingTypeForPRM", CFG_STRING, cgCfgBuffer ) == RC_SUCCESS )
     {
        strcpy(pcgHandlingTypeForPRM,cgCfgBuffer);
     }
     dbg(TRACE,"Handling Type for PRM = <%s>",pcgHandlingTypeForPRM);
	
	return RC_SUCCESS;
}


static int SaveDemands( int ipBcMode )
{

	int ilRc = RC_SUCCESS;
	char *pclRow, *pclData = 0;
	int	 i, ilModified, ilTouched;
	long llRowNum ,llBytes;
	char clTwEnd[33], clLists[11];
	BOOL blActive;
								long llNewDemandCount;
								long llOldDemandCount;
	CEDAArrayGetRowCount(&rgNewDemArray.rrArrayHandle,rgNewDemArray.crArrayName,&llNewDemandCount);
	CEDAArrayGetRowCount(&rgOldDemArray.rrArrayHandle,rgOldDemArray.crArrayName,&llOldDemandCount);

	dbg(DEBUG,"SaveDemands: Start ipBcMode <%d>", ipBcMode );
	if ( ipBcMode < 0 )
		ipBcMode = NO_BC;

	blActive = (ipBcMode&SINGLE_TO_BCHDL) ? TRUE : FALSE;
	CEDAArraySendChanges2BCHDL( &rgNewDemArray.rrArrayHandle,
								rgNewDemArray.crArrayName, blActive );
	CEDAArraySendChanges2BCHDL( &rgOldDemArray.rrArrayHandle,
								rgOldDemArray.crArrayName, blActive );

	blActive = (ipBcMode&SINGLE_TO_ACTION) ? TRUE : FALSE;
	CEDAArraySendChanges2ACTION( &rgNewDemArray.rrArrayHandle,
								 rgNewDemArray.crArrayName, blActive );
	CEDAArraySendChanges2ACTION( &rgOldDemArray.rrArrayHandle,
								 rgOldDemArray.crArrayName, blActive );

	if ( prgDemInf )
		prgDemInf->AppendData = FALSE;
	dbg(DEBUG,"SaveDemands: OldDemArray follows:count=%d",llOldDemandCount);
	ilRc = AATArraySaveChanges(&rgOldDemArray.rrArrayHandle, rgOldDemArray.crArrayName, 
								&prgDemInf, ARR_COMMIT_ALL_OK);
	dbg ( TRACE, "SaveDemands: AATArraySaveChanges <DEMANDS> RC <%d>", ilRc );
	if ( prgDemInf )
		prgDemInf->AppendData = TRUE;
	dbg(DEBUG,"SaveDemands: NewDemArray follows:count=%d",llNewDemandCount);
	/****/
	ilRc = AATArraySaveChanges( &rgNewDemArray.rrArrayHandle, rgNewDemArray.crArrayName, 
								&prgDemInf, ARR_COMMIT_ALL_OK);
								/***/
	dbg ( TRACE, "SaveDemands: AATArraySaveChanges <NEWDEMANDS> RC <%d>", ilRc );
	ilTouched = prgDemInf->DataList[IDX_IRTU].ValueCount + 
				prgDemInf->DataList[IDX_URTU].ValueCount +
				prgDemInf->DataList[IDX_DRTU].ValueCount; 
	if ( ilTouched <= 0)
		dbg ( TRACE, "SaveDemands: No data changes !!!" );
	if ( ( (ipBcMode&TCP_SBC) || (ipBcMode&UDP_SBC) ) && prgDemInf && (ilTouched>0) )
	{
		dbg(DEBUG,"SaveDemands: Preparing SBC info, %d demands saved", ilTouched );
		/* calculate min(DEBE) and max(DEEN) of all Demands */
		llRowNum = ARR_FIRST;
		while( CEDAArrayFindRowPointer( &rgOldDemArray.rrArrayHandle,
										rgOldDemArray.crArrayName,
										&rgOldDemArray.rrIdx01Handle,
										rgOldDemArray.crIdx01Name,"",
										&llRowNum,(void**)&pclRow) == RC_SUCCESS )
		{
			llRowNum = ARR_NEXT;
			if ( !cgMinDebe[0] || 
				 ( strcmp(OLDDEMFIELD(pclRow,igDemDEBE), cgMinDebe ) < 0 )
			   )
				strcpy ( cgMinDebe, OLDDEMFIELD(pclRow,igDemDEBE) );
			if ( strcmp(cgMaxDeen, OLDDEMFIELD(pclRow,igDemDEEN) ) < 0 )
				strcpy ( cgMaxDeen, OLDDEMFIELD(pclRow,igDemDEEN) );

		}
		llRowNum = ARR_FIRST;
		while( CEDAArrayFindRowPointer( &rgNewDemArray.rrArrayHandle,
										rgNewDemArray.crArrayName,
										&rgNewDemArray.rrIdx01Handle,
										rgNewDemArray.crIdx01Name,"",
										&llRowNum,(void**)&pclRow) == RC_SUCCESS )
		{
			llRowNum = ARR_NEXT;
			if ( !cgMinDebe[0] || 
				 ( strcmp(NEWDEMFIELD(pclRow,igDemDEBE), cgMinDebe ) < 0 )
			   )
				strcpy ( cgMinDebe, NEWDEMFIELD(pclRow,igDemDEBE) );
			if ( strcmp(cgMaxDeen, NEWDEMFIELD(pclRow,igDemDEEN) ) < 0 )
				strcpy ( cgMaxDeen, NEWDEMFIELD(pclRow,igDemDEEN) );

		}
		llBytes = prgDemInf->DataList[IDX_IRTU].UsedSize + 
				  prgDemInf->DataList[IDX_URTU].UsedSize +
				  prgDemInf->DataList[IDX_DRTU].UsedSize + 60;
		pclData = malloc ( llBytes );
		if ( !pclData )
			dbg(TRACE,"SaveDemands: malloc of <%ld> bytes for SBC failed", llBytes );
	}
	if ( pclData )
	{
		dbg(DEBUG,"SaveDemands: Preparing new broadcast" );
		/*	Form: "Date1,Date2,DEL:n,URNO1,..,URNOn,UPD:m,URNO1,..,URNOm"	*/
		ilModified = prgDemInf->DataList[IDX_IRTU].ValueCount + 
					 prgDemInf->DataList[IDX_URTU].ValueCount;
		if ( prgDemInf->DataList[IDX_DRTU].ValueCount > 0 )
			sprintf ( pclData, "%s,%s,DEL:%d,%s,UPD:%d", cgMinDebe, cgMaxDeen, 
						  prgDemInf->DataList[IDX_DRTU].ValueCount, 
						  prgDemInf->DataList[IDX_DRTU].ValueList, ilModified );
		else
			sprintf ( pclData, "%s,%s,DEL:0,UPD:%d", cgMinDebe, cgMaxDeen, ilModified );
					  
		if ( prgDemInf->DataList[IDX_IRTU].ValueCount > 0 )
		{
			strcat ( pclData, "," );
			strcat ( pclData, prgDemInf->DataList[IDX_IRTU].ValueList );
		}
		if ( prgDemInf->DataList[IDX_URTU].ValueCount > 0 )
		{
			strcat ( pclData, "," );
			strcat ( pclData, prgDemInf->DataList[IDX_URTU].ValueList );
		}

		/* dbg(DEBUG,"SaveDemands: Broadcast Data <%s>", pclData ); */
	}
	if ( (ilTouched > 0) && ( (ipBcMode&TCP_SBC) || (ipBcMode&UDP_SBC) ) )
	{
		sprintf (clTwEnd, "%s,%s,%s", cgHopo,cgTabEnd, cgProcessName);
		if ( pclData )
		{
			if ( !(ipBcMode & UDP_SBC ) )
				strcat ( pclData, "\n{=NOUDP=}" );
			ilRc = AATArraySetSbcInfo(rgOldDemArray.crArrayName, prgDemInf, "1900", 
									  "3", "SBC", "UPDDEM", "CEDA", "DEMHDL", "", 
									  clTwEnd, "", "DEBE,DEEN,DEL,UPD", pclData );
		}
		else
			ilRc = AATArraySetSbcInfo(rgOldDemArray.crArrayName, prgDemInf, "1900", 
									  "3", "SBC", "UPDDEM", "CEDA", "DEMHDL", "", 
									  clTwEnd, "", "", "{=NOUDP=}" );
		if ( ilRc != RC_SUCCESS )
			dbg ( TRACE, "SaveDemands: AATArraySetSbcInfo failed RC <%d>", ilRc );
		else
		{
			sprintf ( clLists, "%d,%d,%d", IDX_IRTD, IDX_URTD, IDX_DRTU );
			ilRc = AATArrayCreateSbc(rgOldDemArray.crArrayName, prgDemInf, 0, clLists, FALSE);
			if ( ilRc != RC_SUCCESS )
				dbg ( TRACE, "AATArrayCreateSbc: failed RC <%d>", ilRc );
			dbg ( DEBUG, "List Infos: " );
			for ( i=0; i<3; i++ )
			{
				dbg(DEBUG,"-------------------------------------------------");
				dbg(DEBUG,"LIST %d: <%s> COUNT=%d",rgUIdx[i],prgDemInf->DataList[rgUIdx[i]].ListName,prgDemInf->DataList[rgUIdx[i]].ValueCount);
				if (prgDemInf->DataList[rgUIdx[i]].UsedSize > 0)
				{
					dbg(DEBUG,"LIST CONTENT (%d BYTES):\n%s>",prgDemInf->DataList[rgUIdx[i]].UsedSize,prgDemInf->DataList[rgUIdx[i]].ValueList);
				}
			}
			dbg(DEBUG,"-------------------------------------------------");
		}
	}

	if ( pclData )
		free ( pclData );

	dbg(DEBUG,"SaveDemands: finished RC <%d>", ilRc );
	return ilRc;
}

/******************************************************************************/
static int GetCedaTimestamp(char *pcpResultBuf,int ipResultLen, long lpTimeOffset)
{
	int         ilRC      = RC_SUCCESS ;      /* Return code */
	int         ilTimeLen = 0 ;
	time_t      llCurTime = 0 ;
	struct tm  *prlLocalTimeResult ;
	struct tm   rlTimeStruct ;
	char        clTimeStamp[16];

	if (ilRC == RC_SUCCESS)
	{
		if (pcpResultBuf == NULL)
		{
			dbg (TRACE, "GetCedaTimestamp: Result <%s>",clTimeStamp) ; 
			dbg (TRACE, "GetCedaTimestamp: invalid 1.st argument <0x%8.8x>",pcpResultBuf) ; 
			ilRC = RC_FAIL ;
		}  
	} 

	if (ilRC == RC_SUCCESS)
	{
		if (ipResultLen < 15)
		{
			dbg( TRACE, "GetCedaTimestamp: invalid 2.st argument <%d>",ipResultLen);
			dbg( TRACE,"GetCedaTimestamp: minimum of 15 characters required!");
			ilRC = RC_FAIL ;
		} /* end of if */
	} /* end of if */

	if (ilRC == RC_SUCCESS)
	{
		memset ((void*) &clTimeStamp[0], 0x00, 16) ;
		llCurTime = time(0L) ;
		llCurTime += lpTimeOffset ;
		prlLocalTimeResult = (struct tm *) localtime (&llCurTime) ;
		memcpy ((char *) &rlTimeStruct,(char *)prlLocalTimeResult,sizeof(struct tm)) ;
		ilTimeLen = strftime(clTimeStamp,15,"%" "Y%" "m%" "d%" "H%" "M%" "S",&rlTimeStruct) ;
		if (ilTimeLen <= ipResultLen)
		{
  			LocalToUtc ( clTimeStamp );
			strcpy (pcpResultBuf, clTimeStamp) ;
		} /* end of if */
	} /* end of if */

	return ilRC ;

} /* end of GetCedaTimestamp */


static BOOL ToBeBroadcasted ( char *pcpDate )
{
	BOOL blRet = TRUE;
	char clEndOfRange[16];

	if ( !pcpDate )
		dbg ( TRACE, "ToBeBroadcasted: Date is NULL, BCDays <%ld>", lgBCDays );
	else
		dbg ( DEBUG, "ToBeBroadcasted: Date <%s>, BCDays <%ld>", pcpDate, lgBCDays );
	if ( pcpDate && pcpDate[0] && ( lgBCDays >= 0 ) )
	{
		if ( GetCedaTimestamp( clEndOfRange, 16, lgBCDays ) == RC_SUCCESS )
		{
			if ( strcmp ( pcpDate, clEndOfRange ) > 0 )
			{
				dbg ( DEBUG, "ToBeBroadcasted: <%s> later than end of Range <%s>",
							 pcpDate, clEndOfRange ); 
				blRet = FALSE;
			}
			else
				dbg ( DEBUG, "ToBeBroadcasted: <%s> before end of Range <%s>",
							 pcpDate, clEndOfRange ); 
		}
	}
	return blRet;
}


static int SetRudLogicalFields ( ARRAYINFO *prpRudArr, char *pcpRudUrno )
{
	int ilRc = RC_SUCCESS, ilRet = RC_SUCCESS;
	long llRudRow = ARR_FIRST, llResRow, llFieldNo=-1, llQucoIdx=-1;
	char *pclRudResult=0, *pclResource=0, *pclRety, *pclRloc;
	char clReco[34], clQuco[211];
	char clResKey[12];

	if ( !pcpRudUrno )
		return RC_INVALID;

	while ( CEDAArrayFindRowPointer(&(prpRudArr->rrArrayHandle),
									prpRudArr->crArrayName,
									&(prpRudArr->rrIdx01Handle),
									prpRudArr->crIdx01Name,pcpRudUrno,
									&llRudRow, (void**)&pclRudResult) == RC_SUCCESS )
	{
		clReco[0] = clQuco[0] = '\0';
		strcpy ( clResKey, CCIRUDFIELD(pclRudResult,igRudURNO) );
		TrimRight( clResKey );
		pclRety = CCIRUDFIELD(pclRudResult,igRudRETY);
		/* dbg ( DEBUG, "SetRudLogicalFields: found URNO <%s> RETY <%s>", clResKey, pclRety ); */
		if ( strcmp( pclRety, "100") == 0 )
		{
			ilRc = GetResourceName ( &rgRpfArray, igRpfGTAB, igRpfUPFC, 
									 igRpfFCCO, clResKey, clReco );
			ilRc |= GetQualifications ( clResKey, clQuco );
		}
		else if ( strcmp( pclRety, "010") == 0 )
		{
			ilRc = GetResourceName ( &rgReqArray, igReqGTAB, igReqUEQU, 
									 igReqEQCO, clResKey, clReco );
		}
		else
		{
			llResRow = ARR_FIRST;
			if ( CEDAArrayFindRowPointer(&rgRloArray.rrArrayHandle,
										 rgRloArray.crArrayName,
										 &rgRloArray.rrIdx01Handle,
										 rgRloArray.crIdx01Name,clResKey,
										 &llResRow, (void**)&pclResource) == RC_SUCCESS )
			{
				pclRloc = RLOFIELD(pclResource,igRloRLOC);
				if ( *RLOFIELD(pclResource,igRloGTAB) == 'S' )
				{
					llResRow = ARR_FIRST;
					if ( CEDAArrayFindRowPointer(&rgSgrArray.rrArrayHandle,
										 rgRpfArray.crArrayName,
										 &rgRpfArray.rrIdx01Handle,
										 rgRpfArray.crIdx01Name,pclRloc,
										 &llResRow, (void**)&pclResource) == RC_SUCCESS )
					{
						clReco[0] = '*';
						strcpy ( &(clReco[1]), SGRFIELD(pclResource, igSgrGRPN) );
					}
				}
				else
				{
					ilRc = GetNameFromBasicData ( RLOFIELD(pclResource,igRloREFT),
												  pclRloc, clReco );
				}
			}

		}
		dbg ( DEBUG, "SetRudLogicalFields: URUD <%s> RECO <%s> QUCO <%s> RC <%d>",
			  clResKey, clReco, clQuco, ilRc );
		if ( ilRc == RC_SUCCESS )
			ilRc = CEDAArrayPutField( &(prpRudArr->rrArrayHandle), 
									  prpRudArr->crArrayName, &llFieldNo,
									  "RECO",llRudRow, clReco );
		if ( clQuco[0] )
			ilRc = CEDAArrayPutField( &(prpRudArr->rrArrayHandle), 
									  prpRudArr->crArrayName,
									  &llQucoIdx,"QUCO",llRudRow, clQuco );
		if ( ilRc != RC_SUCCESS )
			ilRet = ilRc;
		llRudRow = ARR_NEXT;	
	}/* endof while */
	return ilRet;
}

static int GetResourceName ( ARRAYINFO *prpArrayInfo, int ipGtabIdx, int ipUresIdx, 
							 int ipCodeIdx, char *pcpUrud, char *pcpName )
{
	long llRow = ARR_FIRST;
	char *pclResource=0, *pclSgrRow, *pclUres, *pclGtab, *pclCode;
	int ilRc = RC_NOTFOUND;

	if ( !pcpUrud || !pcpName )
		return RC_INVALID;

	/*dbg ( DEBUG, "GetResourceName: ARRAY <%s> URUD <%s> UresIdx <%d> Codeidx <%d> GtabIdx <%d>", 
		  prpArrayInfo->crArrayName, pcpUrud, ipUresIdx, ipCodeIdx, ipGtabIdx );*/
	pcpName[0]= '\0';
	if ( CEDAArrayFindRowPointer(&(prpArrayInfo->rrArrayHandle),
								 prpArrayInfo->crArrayName,
								 &(prpArrayInfo->rrIdx01Handle),
								 prpArrayInfo->crIdx01Name,pcpUrud,
								 &llRow, (void**)&pclResource) == RC_SUCCESS )
	{
		pclUres = pclResource + prpArrayInfo->plrArrayFieldOfs[ipUresIdx-1];
		pclGtab = pclResource + prpArrayInfo->plrArrayFieldOfs[ipGtabIdx-1];
		pclCode = pclResource + prpArrayInfo->plrArrayFieldOfs[ipCodeIdx-1];
		
		if ( *pclGtab == 'S' )
		{
			llRow = ARR_FIRST;
			if ( CEDAArrayFindRowPointer(&rgSgrArray.rrArrayHandle,
										 rgSgrArray.crArrayName,
										 &rgSgrArray.rrIdx01Handle,
										 rgSgrArray.crIdx01Name,pclUres,
										 &llRow, (void**)&pclSgrRow) == RC_SUCCESS )
			{
				pcpName[0]='*';
				strcpy ( &(pcpName[1]), SGRFIELD(pclSgrRow,igSgrGRPN) );
				ilRc = RC_SUCCESS;
			}
		}
		else
		{
			strcpy ( pcpName, pclCode );
			ilRc = RC_SUCCESS;
		}
	}
	if ( ilRc != RC_SUCCESS )
		dbg ( TRACE, "GetResourceName failed: URUD <%s> ilRc <%d>", pcpUrud, ilRc ); 
	/* else
		dbg ( DEBUG, "GetResourceName: URUD <%s> NAME <%s> ilRc <%d>", pcpUrud, pcpName, ilRc ); */

	return ilRc;
}

static int GetNameFromBasicData ( char *pcpReft, char *pcpUrno, char *pcpName )
{
	int ilRc, dbg_lvl=DEBUG;
	char *pclDot;
	char clSqlBuf[128], clDataArea[128]="";
	short slCursor=0;

	if ( !pcpReft || !pcpReft || !pcpName )
		return RC_INVALID;
	
	pclDot = strchr( pcpReft, '.' );
	if ( !pclDot )
		return RC_INVALID;
	*pclDot = '\0';
	pclDot++;

	sprintf(clSqlBuf, "SELECT %s FROM %sTAB WHERE URNO = '%s'", pclDot, pcpReft, pcpUrno);
	ilRc = sql_if(START,&slCursor,clSqlBuf, clDataArea);
	if (ilRc != DB_SUCCESS)
	{
		check_ret(ilRc);
		dbg_lvl = TRACE;
	}
	else
	{
		strncpy ( pcpName, clDataArea, 32 );
		pcpName[32] = '\0';
	}
	close_my_cursor(&slCursor);
	/* dbg(dbg_lvl,"GetNameFromBasicData: <%s> Data <%s> ilRc <%d>", clSqlBuf, clDataArea, ilRc); */
	return ilRc;
}

static int GetQualifications ( char *pcpUrud, char *pcpQuco )
{
	int ilRc =RC_SUCCESS, ilRc1;
	long llRow = ARR_FIRST, llSgrRow;
	char *pclQuali=0, *pclSgrRow, *pclGtab, *pclUper, *pclQuco;
	char clCode[40];

	if ( !pcpUrud || !pcpQuco )
		return RC_INVALID;

	pcpQuco[0] = '\0';
	while ( CEDAArrayFindRowPointer( &rgRpqArray.rrArrayHandle,
									 rgRpqArray.crArrayName,
									 &rgRpqArray.rrIdx01Handle,
									 rgRpqArray.crIdx01Name,pcpUrud,
									 &llRow, (void**)&pclQuali) == RC_SUCCESS )
	{
		pclUper = RPQFIELD(pclQuali,igRpqUPER);
		pclGtab = RPQFIELD(pclQuali,igRpqGTAB);
		pclQuco = RPQFIELD(pclQuali,igRpqQUCO);
		
		ilRc1 = RC_NOTFOUND;
		clCode[0] = '\0';
		if ( *pclGtab == 'S' )
		{
			llSgrRow = ARR_FIRST;
			if ( CEDAArrayFindRowPointer(&rgSgrArray.rrArrayHandle,
										 rgSgrArray.crArrayName,
										 &rgSgrArray.rrIdx01Handle,
										 rgSgrArray.crIdx01Name,pclUper,
										 &llSgrRow, (void**)&pclSgrRow) == RC_SUCCESS )
			{
				clCode[0]='*';
				strcpy ( &(clCode[1]), SGRFIELD(pclSgrRow,igSgrGRPN) );
				ilRc1 = RC_SUCCESS;
			}
		}
		else
		{
			strcpy ( clCode, pclQuco );
			ilRc1 = RC_SUCCESS;
		}
		if ( ilRc1 == RC_SUCCESS )
		{	
			TrimRight ( clCode );
			if ( strlen(pcpQuco) + strlen(clCode) +2 > rgCCIRudArray.plrArrayFieldLen[igRudQUCO-1] )
			{
				dbg ( TRACE, "GetQualifications: QUCU too small (%d bytes) for URUD <%s>", 
					  rgCCIRudArray.plrArrayFieldLen[igRudQUCO], pcpUrud );
				ilRc = RC_NOMEM;
			}
			else
			{
				if ( *pcpQuco ) 
					strcat ( pcpQuco, "|" );
				strcat ( pcpQuco, clCode );
			}
		}
		else 
		{
			dbg ( TRACE, "GetQualifications: Could not find resource URUD <%s>", pcpUrud );
			ilRc = ilRc1;
		}
		llRow = ARR_NEXT;
	}
	/* dbg ( DEBUG, "GetQualifications: URUD <%s> QUCO <%s> RC <%d>", pcpUrud, pcpQuco, ilRc ); */
	return ilRc;
}

static int CombineNwdArray ( short	spState )
{
	int i, j;
	int ilRet = 0;

	if ( ( iGetConfigRow ( cgConfigFile, "CCI", "COMBINE_RESTS", CFG_STRING, 
						   cgCfgBuffer ) != RC_SUCCESS ) || 
		   ( strcmp ( cgCfgBuffer, "YES" ) != 0 ) )
	{
		dbg ( TRACE, "CombineNwdArray not active" );
		return 0;
	}

	DbgNwdArray ( DEBUG, "Combine Start" );
	for( i = 0; i < igNwD; i++ )
	{
		if (prgNwD[i].srState == spState )
		{
			for ( j=0; j<igNwD; j++ )
			{
				if ( (i!=j) && (prgNwD[j].srState==spState) && 
					 (prgNwD[i].lrEndInSec == prgNwD[j].lrStartInSec) )
				{	/* found two entries of same state fitting together */
					dbg ( TRACE, "CombineNwdArray: <%s>-><%s> + <%s>-><%s>",  
						  prgNwD[i].crStartDemand, prgNwD[i].crEndDemand, 
						  prgNwD[j].crStartDemand, prgNwD[j].crEndDemand ); 
					strcpy ( prgNwD[i].crEndDemand, prgNwD[j].crEndDemand );
					prgNwD[i].lrEndInSec = prgNwD[j].lrEndInSec;
					prgNwD[j].srState = CUT;
					ilRet ++;
					j = -1; /* maybe an already investigated entry fits to new end */
				}
			}
		}
	}
	/*if ( ilRet > 0 )
		DbgNwdArray ( DEBUG, "Combine End  " );*/
	dbg ( TRACE, "CombineNwdArray: finished, <%d> entries became obsolete", ilRet );
	return ilRet;
}


static void DbgNwdArray ( int ipDbg, char *pcpText )
{
	int i;

	for( i = 0; i < igNwD; i++ )
		dbg(ipDbg, "%s: <%d> <%s>-<%s> (%ld-%ld) State <%d>",pcpText, i,
			prgNwD[i].crStartDemand, prgNwD[i].crEndDemand, prgNwD[i].lrStartInSec, 
			prgNwD[i].lrEndInSec, prgNwD[i].srState);
}

static void DbgCciDemArray ( int ipDbg, char *pcpText, char *pcpKey )
{
	long	llRow = ARR_FIRST;
	char	*pclResult = NULL;

	while( CEDAArrayFindRowPointer(&rgCCIDemArray.rrArrayHandle,
									rgCCIDemArray.crArrayName,
									&rgCCIDemArray.rrIdx01Handle,
									rgCCIDemArray.crIdx01Name,pcpKey,
									&llRow,(void**)&pclResult) == RC_SUCCESS)
	{
		llRow = ARR_NEXT;
		dbg(ipDbg, "%s: URNO <%s> DEBE <%s> DEEN <%s>",pcpText, CCIDEMFIELD(pclResult, igDemURNO),
			CCIDEMFIELD(pclResult, igDemDEBE), CCIDEMFIELD(pclResult, igDemDEEN));
	}
}

/* WORK-AROUND function */
static int RebuildArrayIndexes ( ARRAYINFO *prpArr, BOOL bpIdx1, BOOL bpIdx2 )
{
	int ilRc = RC_SUCCESS;

	if ( !prpArr )
		return RC_INVALID;
	dbg ( DEBUG, "RebuildArrayIndexes: Array <%s> bpIdx1 <%d> bpIdx2 <%d>", 
		  prpArr->crArrayName, bpIdx1, bpIdx2 );
	if ( bpIdx1 && (prpArr->rrIdx01Handle>=0) )
	{
		ilRc |= CEDAArrayDisactivateIndex ( &(prpArr->rrArrayHandle), prpArr->crArrayName, 
											&(prpArr->rrIdx01Handle), prpArr->crIdx01Name );
		ilRc |= CEDAArrayActivateIndex( &(prpArr->rrArrayHandle), prpArr->crArrayName, 
										&(prpArr->rrIdx01Handle), prpArr->crIdx01Name );
	}
	if ( bpIdx2 && (prpArr->rrIdx02Handle>=0) )
	{
		ilRc |= CEDAArrayDisactivateIndex ( &(prpArr->rrArrayHandle), prpArr->crArrayName, 
											&(prpArr->rrIdx02Handle), prpArr->crIdx02Name );
		ilRc |= CEDAArrayActivateIndex( &(prpArr->rrArrayHandle), prpArr->crArrayName, 
										&(prpArr->rrIdx02Handle), prpArr->crIdx02Name );
	}
	dbg ( DEBUG, "RebuildArrayIndexes: returning <%d>", ilRc );
	return ilRc;
}


static int AlignCCIToStep ( int ipStepSec )
{
	long	llRow=ARR_LAST;
	char	*pclResult;
	time_t	tlDebe, tlDeen, tlNewDebe, tlNewDeen;
	int     ilRound = ipStepSec / 2;
	char	clDebe[15], clDeen[15], *pclUrno, clDedu[11];
	BOOL	blSetDebe, blSetDeen;
	int     ilDeleted=0, ilChanged = 0;
	long	llFieldDEBE = -1;
	long	llFieldDEEN = -1;
	long	llFieldDEDU = -1;
	long    llRowCnt;
	int		ilRc = RC_SUCCESS, ilRc1;

	dbg ( TRACE, "AlignCCIToStep: Start, Step <%d>", ipStepSec );
	CEDAArrayGetRowCount( &rgCCIDemArray.rrArrayHandle,
						  rgCCIDemArray.crArrayName, &llRowCnt );

	while ( CEDAArrayGetRowPointer( &rgCCIDemArray.rrArrayHandle,
									rgCCIDemArray.crArrayName, llRow,
									(void**)&pclResult ) == RC_SUCCESS )
	{
		blSetDebe = FALSE;
		blSetDeen = FALSE;
		pclUrno = CCIDEMFIELD(pclResult,igDemURNO);

		StrToTime ( CCIDEMFIELD(pclResult,igDemDEBE), &tlDebe );
		if ( tlDebe % ipStepSec != 0 )
		{	/*	DEBE does not fit to grid of stepwidth -> adjust to grid
				e.g.  00 min., 15 min. 30 min, 45 min				*/
			tlNewDebe = (tlDebe+ilRound) / ipStepSec ;
			tlNewDebe *=ipStepSec;
			TimeToStr ( clDebe, tlNewDebe );
			dbg ( DEBUG, "AlignCCIToStep: URNO <%s> DEBE <%s> -> <%s>", 
				  pclUrno, CCIDEMFIELD(pclResult,igDemDEBE), clDebe );	
			blSetDebe = TRUE;
		}
		else
		{
			strcpy ( clDebe, CCIDEMFIELD(pclResult,igDemDEBE) );
			tlNewDebe = tlDebe; 	
		}
		
		StrToTime(CCIDEMFIELD(pclResult,igDemDEEN), &tlDeen );
		if ( tlDeen % ipStepSec != 0 )
		{	/*	DEEN does not fit to grid of stepwidth -> adjust to grid
				e.g.  00 min., 15 min. 30 min, 45 min				*/
			tlNewDeen = (tlDeen+ilRound) / ipStepSec ;
			tlNewDeen *=ipStepSec;
			TimeToStr ( clDeen, tlNewDeen );
			dbg ( DEBUG, "AlignCCIToStep: URNO <%s> DEEN <%s> -> <%s>", 
				  pclUrno, CCIDEMFIELD(pclResult,igDemDEEN), clDeen );	
			blSetDeen = TRUE;
		}
		else
		{
			strcpy ( clDeen, CCIDEMFIELD(pclResult,igDemDEEN) );
			tlNewDeen = tlDeen; 	
		}				
		if ( tlNewDebe >= tlNewDeen )	/* no positive demand length */
		{
			dbg ( DEBUG, "AlignCCIToStep: Deleting URNO <%s> DEBE <%s> DEEN <%s>",
				  pclUrno, clDebe, clDeen );
			ToDeleteOrNot(pclUrno, ARR_CURRENT);
			ilDeleted++;
		}
		else if ( blSetDeen || blSetDebe )
		{
			ilRc1 = RC_SUCCESS;
			sprintf ( clDedu,"%ld", tlNewDeen-tlNewDebe );
			if ( strcmp ( clDedu, CCIDEMFIELD(pclResult,igDemDEDU) ) != 0 )
			{
				dbg ( DEBUG, "AlignCCIToStep: URNO <%s> DEDU <%s> -> <%s>",
					  pclUrno, CCIDEMFIELD(pclResult,igDemDEDU), clDedu );
				ilRc1 = CEDAArrayPutField(&rgCCIDemArray.rrArrayHandle,
										  rgCCIDemArray.crArrayName,
										  &llFieldDEDU,"DEDU",ARR_CURRENT, clDedu );
			}
			if ( blSetDebe )
				ilRc1 |= CEDAArrayPutField(&rgCCIDemArray.rrArrayHandle,
										  rgCCIDemArray.crArrayName,
										  &llFieldDEBE,"DEBE",ARR_CURRENT, clDebe );

			if ( blSetDeen )
				ilRc1 |= CEDAArrayPutField(&rgCCIDemArray.rrArrayHandle,
										  rgCCIDemArray.crArrayName,
										  &llFieldDEEN,"DEEN",ARR_CURRENT, clDeen );
			if ( ilRc1 != RC_SUCCESS )
			{
				dbg ( TRACE, "AlignCCIToStep: CEDAArrayPutField failed, RC <%d>", ilRc1 );
				ilRc = ilRc1;
			}
			ilChanged++;
		}
		llRow=ARR_PREV;
	}
	if ( ilChanged > 0 )
		ilRc = RebuildArrayIndexes ( &rgCCIDemArray, TRUE, TRUE );
	dbg ( TRACE, "AlignCCIToStep: Rows <%ld> Changed <%d> Deleted <%d>  RC <%d>", 
		  llRowCnt, ilChanged, ilDeleted, ilRc );
	return ilRc;
}



/****************************************************************************
* Function: GetUrnoFromSelection
* Parameter:           
*			 
* Return:
* Description: gets the Urno from the SQL Statement "WHERE URNO = 'xxxxxxx'"
*			   
*****************************************************************************/
static int GetUrnoFromSelection(char *pcpSelection,char *pcpUrno)
{
	char *pclFirst;
	char *pclLast;
	char pclTmp[24];
	int ilLen;
	
	pclFirst = strchr(pcpSelection,'\'');
	if (pclFirst != NULL)
	{
		pclLast  = strrchr(pcpSelection,'\'');
		if (pclLast == NULL)
		{
			pclLast = pclFirst + strlen(pclFirst);
		}
		ilLen = pclLast-pclFirst < 22 ? pclLast - pclFirst : 22;
		strncpy(pclTmp,++pclFirst,ilLen);
		pclTmp[ilLen-1] = '\0';
		strcpy(pcpUrno,pclTmp);
	}
	else
		{
		 pclFirst = pcpSelection;
		 while (!isdigit(*pclFirst) && *pclFirst != '\0')
		 {
			pclFirst++;
			}
		strcpy(pcpUrno,pclFirst);
		}
	return RC_SUCCESS;
}


/******************************************************************************/
/* Space for Appendings (Source-Code Control)                                 */
/******************************************************************************/
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

/*	This table explains new OURI,OURO, DETY of splitted manual demands		  	
	(turnaround-> inbound + outbound) in function SplitManualDemand

	old Turnaround dem.  current Flight	->	old Demand          new Demand
		OURI  OURO			OURI  OURO		OURI  OURO DETY		OURI  OURO DETY
	----------------------------------------------------------------------------
	1.	1234  4567			1234  7890		1234  7890	1		empty 4567  2
	2.	1234  4567			7890  4567		7890  4567	2		1234  empty 1	
	3.	1234  4567			1234  empty		1234  empty	1		empty 4567  2		
	4.	1234  4567			empty 4567		empty 4567	2		1234  empty	1
*/
