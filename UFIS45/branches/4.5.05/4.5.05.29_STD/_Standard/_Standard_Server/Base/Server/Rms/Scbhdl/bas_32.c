/******************************************/
/* GetOverBalValue: delivers the OT-value */
/* ****************************************/
static double GetOverBalValue(const char *pcpBreakTime,const char *pcpStartTime,const char *pcpEndTime,const char *pcpBsdu,const char *pcpCode1,const char *pcpCode3,const char *pcpCode4,const char *pcpSday)
{
	double dlResult = 0;
	char   clBsdStartTime2[6];
	char   clBsdEndTime2[6];
	char   clBsdStartTime[16];
	char   clBsdEndTime[16];

	int ilBsdHandle = GetArrayIndex("BSDTAB");
	if (ilBsdHandle < 0)
	{
		dbg(TRACE,"GetOverBalValue: array 'BSDTAB' not found! Return 0!");
		return 0;
	}

	/* look if there is any OT in the shift */
	if (pcpCode1[0] != '1' && pcpCode3[0] != '1' && pcpCode4[0] != '1')
	{
		if (pcpCode1[0] != '2' && pcpCode3[0] != '2' && pcpCode4[0] != '2')
		{
			dbg(DEBUG,"GetOverBalValue(): No OT-flag in the DRS-fields! Return 0!");
			return 0;
		}
	}

	dbg(DEBUG,"GetOverBalValue(): varBreakTime: <%s>" , pcpBreakTime);
	dbg(DEBUG,"GetOverBalValue(): varStartTime: <%s>" , pcpStartTime);
	dbg(DEBUG,"GetOverBalValue(): varEndTime: <%s" , pcpEndTime);
	dbg(DEBUG,"GetOverBalValue(): varBsdu: <%s>" , pcpBsdu);
	dbg(DEBUG,"GetOverBalValue(): Code1: <%s>" , pcpCode1);
	dbg(DEBUG,"GetOverBalValue(): Code3: <%s>" , pcpCode3);
	dbg(DEBUG,"GetOverBalValue(): Code4: <%s>" , pcpCode4);

	if (pcpCode1[0] =='1' || pcpCode3[0] == '1' || pcpCode1[0] == '2' || pcpCode3[0] == '2')
	{
		/* only a part of the shift time is OT */
		/* we have to calculate it in comparison to the basic shift (a BSD-record) */
		strcpy(clBsdStartTime2,"ESBG");
		strcpy(clBsdEndTime2,"LSEN");
		if (FindFieldValueByIndex(ilBsdHandle,(char*)pcpBsdu,clBsdStartTime2) <= 0 || FindFieldValueByIndex(ilBsdHandle,(char*)pcpBsdu,clBsdEndTime2)   <= 0 )
		{
			dbg(DEBUG,"GetOverBalValue(): GetOverBalValue: BSD with URNO <%s> not found! Return 0!", pcpBsdu);
			return 0;
		}

		strcpy(clBsdStartTime,pcpSday);
		strcpy(&clBsdStartTime[8],clBsdStartTime2);
		strcat(clBsdStartTime,"00");

		strncpy(clBsdEndTime,pcpSday,8);
		strcpy(&clBsdEndTime[8],clBsdEndTime2);
		strcat(clBsdEndTime,"00");
		if (strcmp(clBsdEndTime,clBsdStartTime) < 0)
		{
			/* add one day */
			AddSecondsToCEDATime(clBsdEndTime,(time_t)86400,1);
		}

		dbg(DEBUG,"GetOverBalValue(): Start time of basic shift <clBsdStartTime>: <%s>", clBsdStartTime);
		dbg(DEBUG,"GetOverBalValue(): End time of basic shift <clBsdEndTime>    : <%s>", clBsdEndTime);
	}

	if (pcpCode4[0] == '1' || pcpCode4[0] == '2')
	{
		/* the complete shift is OT */
		double dlDrrMin;
		double dlBreakMin;

		dlDrrMin = GetDailyDRRMinutesByStaff(pcpBreakTime,pcpStartTime,pcpEndTime);
		dlBreakMin = GetDailyBreakMinutesByStaff(pcpBsdu,pcpBreakTime,pcpSday);

		dbg(DEBUG,"GetOverBalValue(): Shift minutes <dlDrrMin>  : %lf",dlDrrMin);
		dbg(DEBUG,"GetOverBalValue(): Break minutes <dlBreakMin>: %lf",dlBreakMin);

		dlResult = (dlDrrMin+dlBreakMin) / 60;

		/* if there is an absence and the duration is 0, take the agreed weekly workig hours */
		if (fabs(dlResult) < 1.0e-5 && IsBsd(pcpBsdu) == 0)
		{
			char clResult[128];
			if (ReadTemporaryField("GetOverBalValue(): VereinbarteWochenstunden",clResult) > 0)
			{
				dlResult = atof(clResult) / 60;
			}
		}
		return dlResult;
	}
	else
	{
		/* only a part of the shift is OT */

		/* the time after the BSD end time is OT */
		if (pcpCode3[0] == '1' || pcpCode3[0] == '2')
		{
			dlResult = GetEndTimeDiff(pcpEndTime,clBsdEndTime) / 60;
		}

		/* the time before the BSD start time is OT */
		if (pcpCode1[0] == '1' || pcpCode1[0] == '2')
		{
			dlResult = (GetStartTimeDiff(pcpStartTime,clBsdStartTime) / 60) + dlResult;
		}

		return dlResult;
	}
}


/* *************************************************/
/* ****** Account 32: calculating overtime of year */
/* *************************************************/
static void bas_32()
{
	char 	clLastCommand[4];
	char	clIsAushilfe[2];
	char	clStaffUrno[12];
	char	clSday[16];
	char	clNewDrrStatus[2];
	char	clYearIndex[6];
	char	clMonthIndex[4];
	char	clDrrBsduUrno[12];
	char	clDrrBreakTime[16];
	char	clDrrStartTime[16];
	char	clDrrEndTime[16];
	char	clDrrStatus[2];
	char	clNewCode1[2];
	char	clNewCode3[2];
	char	clNewCode4[2];
	char	clOldDrrBreakTime[16];
	char	clOldDrrStartTime[16];
	char	clOldDrrEndTime[16];
	char	clOldDrrStatus[2];
	char	clOldCode1[2];
	char	clOldCode3[2];
	char	clOldCode4[2];
	char	clOldDrrBsduUrno[16];

	double	dlAccValue[12];
	char	clCl12Acc33[12];

	double	dlOldValue;
	double	dlNewValue;
	double	dlResultValue;
	double	dlAccTmp;
	int	i;
		
	dbg(DEBUG, "============================= ACCOUNT 32 START ===========================");

	/* checking temporary staff */
	if (ReadTemporaryField("IsAushilfe",clIsAushilfe) == 1 && clIsAushilfe[0] == '1')
	{
		dbg(TRACE,"Account 32: Stopped, because of temporary staff");
		return;
	}

	/* get last command (IRT,URT,DRT) */
	strncpy(clLastCommand,GetLastCommand(),3);
	clLastCommand[3] = '\0';
	dbg(DEBUG,"Account 32: Command (%s)",clLastCommand);

	/* checking the data */
	if (strlen(pcgData) == NULL)
	{
		dbg(TRACE,"Account 32: Stopped, because <pcgData> = undef!");
		return;
	}
	dbg(DEBUG,"Account 32: pcgData = <%s>",pcgData);

	/* checking the fields */
	if (strlen(pcgFields) == 0)
	{
		dbg(TRACE,"Account 32: Stopped, because <pcgFields> = undef!");
		return;
	}
	dbg(DEBUG, "Account 32: pcgFields = <%s>",pcgFields);


	/* checking the URNO of the employee */
	if (ReadTemporaryField("StfUrno",clStaffUrno) == 0)
	{
		dbg(TRACE,"Account 32: Stopped, because <clStaffUrno> = undef!");
		return;
	}

	/* checking the SDAY */
	if (ReadTemporaryField("SDAY",clSday) == 0)
	{
		dbg(TRACE,"Account 32: Stopped, because <clSday> = undef!");
		return;
	}

	/* BSDU of new DRR-record */
	if (ReadTemporaryField("NewBsdu",clDrrBsduUrno) == 0)
	{
		dbg(TRACE,"Account 32: <clDrrBsduUrno> = undef!");
		strcpy(clDrrBsduUrno,"0");
	}

	/* Status of new record	*/
	if (ReadTemporaryField("NewRoss",clNewDrrStatus) == 0)
	{
		dbg(TRACE,"Account 32: Stopped, because <clNewDrrStatus> = undef!");
		return;
	}

	/* start time */
	if (ReadTemporaryField("NewAvfr",clDrrStartTime) <= 0)
	{
		dbg(TRACE, "Account 32: <clDrrStartTime> = undef!");
		strcpy(clDrrStartTime,"0");
	}

	/* end time */
	if (ReadTemporaryField("NewAvto",clDrrEndTime) <= 0)
	{
		dbg(TRACE, "Account 32: <clDrrEndTime> = undef!");
		strcpy(clDrrEndTime,"0");
	}

	/* break duration */
	if (ReadTemporaryField("NewSblu",clDrrBreakTime) <= 0)
	{
		dbg(TRACE, "Account 32: <clDrrBreakTime> = undef!");
		strcpy(clDrrBreakTime,"0");
	}

	/* DRS values */
	GetFieldValueFromMonoBlockedDataString("DRS1",pcgFields,pcgData,clNewCode1);
	GetFieldValueFromMonoBlockedDataString("DRS3",pcgFields,pcgData,clNewCode3);
	GetFieldValueFromMonoBlockedDataString("DRS4",pcgFields,pcgData,clNewCode4);

	/* check old data if it was an URT */
	if (strcmp(clLastCommand,"URT") == 0) 
	{
		/* checking old data */
		if (strlen(pcgOldData) == NULL)
		{
			dbg(TRACE,"Account 32: <pcgOldData> = undef!");
		}

		/* old start time */
		if (ReadTemporaryField("OldAvfr",clOldDrrStartTime) <= 0)
		{
			dbg(TRACE, "Account 32: <clOldDrrStartTime> = undef!");
			strcpy(clOldDrrStartTime,"0");
		}
	
		/* old end time */
		if (ReadTemporaryField("OldAvto",clOldDrrEndTime) <= 0)
		{
			dbg(TRACE, "Account 32: <clOldDrrEndTime> = undef!");
			strcpy(clOldDrrEndTime,"0");
		}
	
		/* old status */
		if (ReadTemporaryField("OldRoss",clOldDrrStatus) < 0)
		{
			dbg(TRACE, "Account 32: <clOldDrrStatus> = undef!");
			clOldDrrStatus[0] = '\0';
		}

		/* old break duration */
		if (ReadTemporaryField("OldSblu",clOldDrrBreakTime) <= 0)
		{
			strcpy(clOldDrrBreakTime,"0");
		}

		/* BSDU of old DRR-record */
		if (ReadTemporaryField("OldBsdu",clOldDrrBsduUrno) == 0)
		{
			dbg(TRACE,"Account 32: <clOldDrrBsduUrno> = undef!");
			strcpy(clOldDrrBsduUrno,"0");
		}

		/* old DRS values */
		GetFieldValueFromMonoBlockedDataString("DRS1",pcgFields,pcgOldData,clOldCode1);
		GetFieldValueFromMonoBlockedDataString("DRS3",pcgFields,pcgOldData,clOldCode3);
		GetFieldValueFromMonoBlockedDataString("DRS4",pcgFields,pcgOldData,clOldCode4);
	}

	/* calculate old value */
	dlOldValue = 0;
	if (strcmp(clLastCommand,"URT") == 0)
	{
		dlOldValue = GetOverBalValue(clOldDrrBreakTime,clOldDrrStartTime,clOldDrrEndTime,clOldDrrBsduUrno,clOldCode1,clOldCode3,clOldCode4,clSday);
		dbg(DEBUG,"Account 32: Old Value = <%lf>" , dlOldValue);
	}
	
	/* calculate new value */
	dlNewValue = 0;
	dlNewValue = GetOverBalValue(clDrrBreakTime,clDrrStartTime,clDrrEndTime,clDrrBsduUrno,clNewCode1,clNewCode3,clNewCode4,clSday);
	dbg(DEBUG,"Account 32: New Value) = <%lf>", dlNewValue);

	/* calculate difference */
	if (strcmp(clLastCommand,"DRT") != 0)
	{
		dlResultValue = dlNewValue - dlOldValue;
	}
	else
	{
		dlResultValue = - dlNewValue;
	}

	dbg(DEBUG,"Account 32: Result Value = <%lf>",dlResultValue);

	if  (dlResultValue == 0)
	{
		dbg(DEBUG,"Account 26: Value not changed.");
		return;
	}

	/* get ACC record and save result */
	strncpy(clYearIndex,clSday,4);
	clYearIndex[4] = '\0';
	GetMultiAccCloseValueHandle(clStaffUrno,"32",clYearIndex,dlAccValue);
	strncpy(clMonthIndex,&clSday[4],2);
	clMonthIndex[2] = '\0';
	i = atoi(clMonthIndex);

	dbg(DEBUG,"Account 32: Old ACC value / month [%s] : <%lf>",clMonthIndex, dlAccValue[atoi(clMonthIndex)-1]);
	dbg(DEBUG,"Account 32: Month index <clMonthIndex> = <%s>",clMonthIndex);

	while  (i <= 12)
	{
		dlAccTmp = dlAccValue[i-1] + dlResultValue;
		dlAccValue[i-1] = dlAccTmp;

		dbg(DEBUG,"Account 32: New ACC value / month [%d] : <%lf>",i,dlAccValue[i-1]);
		i = i + 1;
	}

	/* save result */
	UpdateMultiAccountHandle("32",clStaffUrno,clYearIndex,"SCBHDL",dlAccValue);
	if (GetAccValueHandle("33","12",clYearIndex,clStaffUrno,"CL",clCl12Acc33))
	{
		dbg(DEBUG,"Account 32: CL-value account 32 = <%lf>",dlAccValue[11]);
		dbg(DEBUG,"Account 32: CL-value account 33 = <%lf>",atof(clCl12Acc33));
		dbg(DEBUG,"Account 32: Calling UpdateNextYearsValuesHandleAdjust() ...");
		UpdateNextYearsValuesHandleAdjust(clYearIndex,clStaffUrno,dlAccValue[11],atof(clCl12Acc33),"32","33");
	} 
}
