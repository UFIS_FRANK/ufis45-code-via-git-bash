#ifndef _DEF_mks_version_fdiasg
  #define _DEF_mks_version_fdiasg
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_fdiasg[] = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Kernel/FDI/fdipsm.src 1.16 2012/02/01 16:00:13SGT akl Exp  $";
#endif /* _DEF_mks_version */
/******************************************************************************/
/*                                                                            */
/* ABB ACE/FC Program Skeleton                                                */
/*                                                                            */
/* Author         :  AKL                                                      */
/* Date           :                                                           */
/* Description    :  INCLUDE PART FROM FDIHDL                                 */
/*                   Request Information Message                              */
/*                                                                            */
/* Update history :                                                           */
/* 20090817 	AKL 1.15	Last checked in version in SVN.	              */
/* 20120201	DKA 1.16	UFIS-1354 - prevent duplicate DPX entries if  */
/*                              if a ABSR='CAL' was present if configured     */
/*                              It seems like the only correct operation of   */
/*                              a combination of PAL and PSM is for this to   */
/*                              be set in fdihdl.cfg:                         */
/*     *Send Message to Alerter for PAL/CAL or PSM Telexes: YES / NO          */
/*     SendMsgToAlerterForPAL_PSM = YES                                       */
/*                                                                            */
/******************************************************************************/
/*                                                                            */
/* FDIPSM handle functions                                                    */
/* -----------------------                                                    */
/* static int HandlePSM(char *pcpData)                                        */
/* static int SendPSMDPXRes()                                                 */

static int PSMInterpretation(char *pcpData);

static int HandlePSM(char *pcpData)
{
  int ilRC;
  int ilLine;
  char pclFunc[]="HandlePSM:";
  char *pclData;
  char pclResult[100];
  char pclResult2[100];
  char *pclTmpPtr;
  T_TLXINFOCMD *prlInfoCmd;
  T_TLXRESULT rlTlxResult;
  int ilI;
  int ilCount;
  char pclOrig[16];

  strcpy(pclOrig,"");
  prlInfoCmd = &prgInfoCmd[0][0];
  pclData = pcpData;
  dbg(DEBUG,"%s Extract Flight Line",pclFunc); 
  /* Go back one line */
  ilI = rgTlxInfo.TxtStart - 2;
  while (ilI > 0 && pclData[ilI] != '\n')
  {
     ilI--;
  }
  if (ilI > 0)
  {
     ilI++;
     CopyLine(pclResult,&pclData[ilI]);
     MakeTokenList(pclResult2,pclResult," ",',');
     ilCount = GetNoOfElements(pclResult2,',');
     if (ilCount > 1)
     {
        rgTlxInfo.TxtStart = ilI + 4;
        while (pclData[rgTlxInfo.TxtStart] == ' ')
        {
           rgTlxInfo.TxtStart++;
        }
     }
  }
  ilLine = 0;
  ilRC = 0;
  /* i < 4 => flight line has to be within the following 4 lines   */
  /* and char. length > 1 */
  while (ilRC  < 2 && ilLine< 4)
  {
     /* Search for Flight line  */
     ilRC = GetDataItem(pclResult,&(pclData[rgTlxInfo.TxtStart]),ilLine,'\n',"","  ");
     ilLine++;
  }
  if (ilRC > 0)
  {
     if (strcmp(pclResult,"=TEXT") == 0)
     {
        ilRC = GetDataItem(pclResult,&(pclData[rgTlxInfo.TxtStart]),ilLine,'\n',"","  ");
        ilLine++;
        if (strcmp(pclResult,"COR") == 0)
        {
           ilRC = GetDataItem(pclResult,&(pclData[rgTlxInfo.TxtStart]),ilLine,'\n',"","  ");
           ilLine++;
        }
        if (strcmp(pclResult,"QUOTE") == 0)
        {
           ilRC = GetDataItem(pclResult,&(pclData[rgTlxInfo.TxtStart]),ilLine,'\n',"","  ");
           ilLine++;
        }
        if (strcmp(pclResult,"PSM") == 0)
        {
           ilRC = GetDataItem(pclResult,&(pclData[rgTlxInfo.TxtStart]),ilLine,'\n',"","  ");
           ilLine++;
        }
     }
     dbg(DEBUG,"%s Found Flight Line <%s>",pclFunc,pclResult); 
     ConvertFlightLine(pclResult);
     GetFlightDate(pclResult,1,2);
     if (strlen(pcgFltDate) == 0)
        GetFlightDate(pclResult,2,1);
     MakeTokenList(pclResult2,pclResult," ",',');
     MakeTokenList(pclResult,pclResult2,"/",',');
     MakeTokenList(pclResult2,pclResult,".",',');

     pclTmpPtr = pclResult2;
     /* Get Airline Code */
     ilRC = GetAlc(&pclTmpPtr,prgMasterTlx);
     /* Get Flight Number */
     ilRC = GetFltn(&pclTmpPtr,prgMasterTlx);
     /* Get Destination */
     GetDataItem(pclResult,pclResult2,3,',',"","\0\0");
     if (strlen(pclResult) == 3)
     {
        dbg(DEBUG,"%s Found Origin <%s>",pclFunc,pclResult); 
        memset(&rlTlxResult,0x00,TLXRESULT);
        strcpy(rlTlxResult.DValue,pclResult);
        strcpy(rlTlxResult.FValue,pclResult);
        strcpy(rlTlxResult.DName,"Orig");
        CCSArrayAddUnsort(prgMasterTlx,&rlTlxResult,TLXRESULT);
        strcpy(pclOrig,pclResult);
     }
     else
     {
        GetDataItem(pclResult,pclResult2,4,',',"","\0\0");
        if (strlen(pclResult) == 3)
        {
           dbg(DEBUG,"%s Found Origin <%s>",pclFunc,pclResult); 
           memset(&rlTlxResult,0x00,TLXRESULT);
           strcpy(rlTlxResult.DValue,pclResult);
           strcpy(rlTlxResult.FValue,pclResult);
           strcpy(rlTlxResult.DName,"Orig");
           CCSArrayAddUnsort(prgMasterTlx,&rlTlxResult,TLXRESULT);
           strcpy(pclOrig,pclResult);
        }
     }
  } 

  if (igPSMInterpretation == TRUE)
  {
     if (igIgnoreDepPSMTelexesFromHomeAddr == TRUE)
     {
        if (strcmp(pclOrig,pcgHomeAP) != 0)
           strcpy(pclOrig,"");
        if (strlen(pclOrig) > 0 && strlen(pcgTlxOriginator) > 0 && strncmp(pclOrig,pcgTlxOriginator,3) == 0)
           dbg(DEBUG,"%s No Telex Interpretation, because Telex was sent from <%s>",
               pclFunc,pcgTlxOriginator); 
        else
           ilRC = PSMInterpretation(pcpData);
     }
     else
        ilRC = PSMInterpretation(pcpData);
  }

  ilRC = SendTlxRes(FDI_PSM);

  return ilRC;
} /* end of HandlePSM  */
 

static int PSMInterpretation(char *pcpData)
{
  int ilRC = RC_SUCCESS;
  char pclFunc[]="PSMInterpretation:";
  char pclSearch[128];
  char *pclTlxDat;
  int ilLine;
  char *pclTmpPtr;
  char *pclTmpPtr2;
  char pclLine[128];
  int ilNoPRMType;
  char pclPRMType[16];
  int ilI;
  int ilFound;
  int ilCount;
  char pclResult[128];
  char pclItem[128];
  char pclItem1[128];
  char pclItem2[128];
  char pclFlight[16];
  char pclFlightDate[16];
  char pclSaveFlightDate[16];
  T_TLXRESULT *prlTlxPtr = NULL;
  char clAdid;
  int ilTypeStored = FALSE;
  char pclConFlight[32];
  char pclConFlightClass[32];
  char pclConFlightDate[32];
  char pclConFlightDest[32];
  int ilCnt;
  char pclTmpBuf[1024];

  igCurPsmValues = 0;

  strcpy(pclItem,"XXX");
  prlTlxPtr = CCSArrayGetData(prgMasterTlx,"Orig");
  if (prlTlxPtr != NULL )
     strcpy(pclItem,prlTlxPtr->FValue);
  if (strcmp(pcgHomeAP,pclItem) != 0)
  {
     sprintf(pclSearch,"\n-%s",pcgHomeAP);
     pclTlxDat = strstr(pcpData,pclSearch);
     if (pclTlxDat == NULL)
     {
        dbg(TRACE,"%s Home Airport <%s> not found in telex message",pclFunc,pclSearch);
        return ilRC;
     }
     clAdid = 'A';
  }
  else
  {
     pclTlxDat = strstr(pcpData,"\n-");
     if (pclTlxDat == NULL)
        return ilRC;
     clAdid = 'D';
  }

  ilNoPRMType = GetNoOfElements(pcgPSMKeyWords,',');
  pclTlxDat += 6;
  ilLine = 1;
  pclTmpPtr = GetLine(pclTlxDat,ilLine);
  CopyLine(pclLine,pclTmpPtr);
  if (strncmp(pclLine,"NIL",3) == 0 ||
      (strstr(pclLine,"PAX") != NULL && strstr(pclLine,"/") != NULL && strstr(pclLine,"SSR") != NULL))
  {
     while (pclTmpPtr != NULL)
     {
        strcpy(&prgPsmValues[igCurPsmValues].pclPsmName[0]," ");
        strcpy(&prgPsmValues[igCurPsmValues].pclPsmType[0]," ");
        strcpy(&prgPsmValues[igCurPsmValues].pclPsmSeat[0]," ");
        strcpy(&prgPsmValues[igCurPsmValues].pclPsmConFlight[0]," ");
        strcpy(&prgPsmValues[igCurPsmValues].pclPsmConFlightDate[0]," ");
        strcpy(&prgPsmValues[igCurPsmValues].pclPsmClass[0]," ");
        strcpy(&prgPsmValues[igCurPsmValues].pclPsmConHdlAgt[0]," ");
        CopyLine(pclLine,pclTmpPtr);
        dbg(DEBUG,"%s Line = <%s>",pclFunc,pclLine);
        if (*pclLine == '1' && strncmp(pclLine,"1PAX",4) != 0)
        {
           ilTypeStored = FALSE;
           MakeTokenList(pclResult,pclLine," ",',');
           ilCount = GetNoOfElements(pclResult,',');
           GetDataItem(pclItem,pclResult,1,',',"","\0\0");
           strcpy(&prgPsmValues[igCurPsmValues].pclPsmName[0],&pclItem[1]);
           if (ilCount > 1)
           {
              GetDataItem(pclItem,pclResult,2,',',"","\0\0");
              strcpy(&prgPsmValues[igCurPsmValues].pclPsmSeat[0],pclItem);
           }
           ilLine++;
           pclTmpPtr = GetLine(pclTlxDat,ilLine);
           while (pclTmpPtr != NULL &&
                  (*pclTmpPtr == ' ' ||
                   (*pclTmpPtr != '1' && *pclTmpPtr != '-' && strncmp(pclTmpPtr,"END",3) != 0)))
           {
              CopyLine(pclLine,pclTmpPtr);
              dbg(DEBUG,"%s Line = <%s>",pclFunc,pclLine);
              MakeTokenList(pclResult,pclLine," ",',');
              ilCount = GetNoOfElements(pclResult,',');
              GetDataItem(pclItem,pclResult,1,',',"","\0\0");
              if (ilCount == 2 && (strlen(pclItem) == 2 || strlen(pclItem) == 3))
              {
                 dbg(DEBUG,"%s This could be an Airline Code <%s>",pclFunc,pclItem);
                 ilCnt = 1;
                 if (strlen(pclItem) == 2)
                    ilRC = syslibSearchDbData("ALTTAB","ALC2",pclItem,"URNO",pclTmpBuf,&ilCnt,"\n");
                 else
                    ilRC = syslibSearchDbData("ALTTAB","ALC3",pclItem,"URNO",pclTmpBuf,&ilCnt,"\n");
                 if (ilRC == RC_SUCCESS)
                 {
                    dbg(DEBUG,"%s It is",pclFunc);
                    ilCount = 1;
                    GetDataItem(pclTmpBuf,pclResult,2,',',"","\0\0");
                    strcat(pclItem,pclTmpBuf);
                 }
                 else
                    dbg(DEBUG,"%s It is not",pclFunc);
              }
              if (ilCount == 1 && strlen(pclItem) > 4)
              {
                 dbg(DEBUG,"%s This must be the connected Flight Number <%s>",pclFunc,pclItem);
                 strcpy(pclConFlight,pclItem);
                 ilI = 3;
                 while (isdigit(pclConFlight[ilI]) && ilI < strlen(pclConFlight))
                    ilI++;
                 if (!isdigit(pclConFlight[ilI+1]))
                    ilI++;
                 strncpy(pclConFlightClass,&pclConFlight[ilI],1);
                 pclConFlightClass[1] = '\0';
                 if (strlen(pclConFlight) >= ilI+1)
                    strcpy(pclConFlightDate,&pclConFlight[ilI+1]);
                 else
                    strcpy(pclConFlightDate,"");
                 pclConFlight[ilI] = '\0';
                 if (strlen(pclConFlightDate) >= 2)
                    strcpy(pclConFlightDest,&pclConFlightDate[2]);
                 else
                    strcpy(pclConFlightDest," ");
                 pclConFlightDest[3] = '\0';
                 pclConFlightDate[2] = '\0';
                 ConvertFlightNumber(pclConFlight,&prgPsmValues[igCurPsmValues].pclPsmConFlight[0]);
                 strcpy(pclSaveFlightDate,pcgFltDate);
                 GetFlightDate(pclConFlightDate,1,1);
                 strcpy(&prgPsmValues[igCurPsmValues].pclPsmConFlightDate[0],pcgFltDate);
                 strcpy(pcgFltDate,pclSaveFlightDate);
              }
              else
              {
                 ilFound = FALSE;
                 for (ilI = 0; ilI < ilNoPRMType && ilFound == FALSE && ilTypeStored == FALSE; ilI++)
                 {
                    GetDataItem(pclPRMType,pcgPSMKeyWords,ilI+1,',',"","\0\0");
                    if (strcmp(pclItem,pclPRMType) == 0)
                    {
                       ilFound = TRUE;
                       ilTypeStored = TRUE;
                       dbg(DEBUG,"%s This must be the PRM Type <%s>",pclFunc,pclItem);
                       strcpy(&prgPsmValues[igCurPsmValues].pclPsmType[0],pclItem);
                       igCurPsmValues++;
                    }
                 }
              }
              ilLine++;
              pclTmpPtr = GetLine(pclTlxDat,ilLine);
           }
           ilLine--;
        }
        ilLine++;
        pclTmpPtr = GetLine(pclTlxDat,ilLine);
        if (pclTmpPtr != NULL &&
            (strncmp(pclTmpPtr,"ENDPSM",6) == 0 || strncmp(pclTmpPtr,"ENDPART",7) == 0))
           pclTmpPtr = NULL;
        if (pclTmpPtr != NULL && clAdid == 'A' && *pclTmpPtr == '-')
           pclTmpPtr = NULL;
        if (pclTmpPtr != NULL && clAdid == 'D' && *pclTmpPtr == '-')
        {
           ilLine++;
           pclTmpPtr = GetLine(pclTlxDat,ilLine);
        }
     }
  }
  else
  {
     while (pclTmpPtr != NULL)
     {
        strcpy(&prgPsmValues[igCurPsmValues].pclPsmName[0]," ");
        strcpy(&prgPsmValues[igCurPsmValues].pclPsmType[0]," ");
        strcpy(&prgPsmValues[igCurPsmValues].pclPsmSeat[0]," ");
        strcpy(&prgPsmValues[igCurPsmValues].pclPsmConFlight[0]," ");
        strcpy(&prgPsmValues[igCurPsmValues].pclPsmConFlightDate[0]," ");
        strcpy(&prgPsmValues[igCurPsmValues].pclPsmClass[0]," ");
        strcpy(&prgPsmValues[igCurPsmValues].pclPsmConHdlAgt[0]," ");
        CopyLine(pclLine,pclTmpPtr);
        dbg(DEBUG,"%s Line = <%s>",pclFunc,pclLine);
        ilFound = FALSE;
        for (ilI = 0; ilI < ilNoPRMType && ilFound == FALSE; ilI++)
        {
           GetDataItem(pclPRMType,pcgPSMKeyWords,ilI+1,',',"","\0\0");
           if (strstr(pclLine,pclPRMType) != NULL)
              ilFound = TRUE;
        }
        if (ilFound == FALSE)
           dbg(DEBUG,"%s None of the Key Words <%s> found ==> Ignore Line",pclFunc,pcgPSMKeyWords);
        else
        {
           MakeTokenList(pclResult,pclLine," ",',');
           ilCount = GetNoOfElements(pclResult,',');
           GetDataItem(pclItem,pclResult,1,',',"","\0\0");
           if (strcmp(pclItem,pclPRMType) == 0)
           {
              dbg(DEBUG,"%s PRM Info split over several Lines",pclFunc);
           }
           else
           {
              GetDataItem(pclItem1,pclResult,2,',',"","\0\0");
              GetDataItem(pclItem2,pclResult,3,',',"","\0\0");
              if (strcmp(pclItem1,pclPRMType) == 0 || strcmp(pclItem2,pclPRMType) == 0)
              {
                 dbg(DEBUG,"%s PRM Info found in one Line",pclFunc);
                 strcpy(&prgPsmValues[igCurPsmValues].pclPsmType[0],pclPRMType);
                 GetDataItem(pclItem,pclResult,1,',',"","\0\0");
                 strcpy(&prgPsmValues[igCurPsmValues].pclPsmName[0],pclItem);
                 if (strcmp(pclItem1,pclPRMType) == 0 && ilCount > 2)
                 {
                    GetDataItem(pclItem,pclResult,3,',',"","\0\0");
                    if (strlen(pclItem) <= 4)
                       strcpy(&prgPsmValues[igCurPsmValues].pclPsmSeat[0],pclItem);
                 }
                 else
                 {
                    GetDataItem(pclItem,pclResult,2,',',"","\0\0");
                    if (strlen(pclItem) <= 4)
                       strcpy(&prgPsmValues[igCurPsmValues].pclPsmSeat[0],pclItem);
                 }
                 if (ilCount > 3)
                 {
                    GetDataItem(pclItem,pclResult,4,',',"","\0\0");
                    pclTmpPtr2 = strstr(pclItem,"/");
                    if (pclTmpPtr2 != NULL)
                    {
                       *pclTmpPtr2 = '\0';
                       pclTmpPtr2++;
                       strcpy(pclSaveFlightDate,pcgFltDate);
                       GetFlightDate(pclTmpPtr2,1,1);
                       strcpy(&prgPsmValues[igCurPsmValues].pclPsmConFlightDate[0],pcgFltDate);
                       strcpy(pcgFltDate,pclSaveFlightDate);
                    }
                    else
                       strcpy(&prgPsmValues[igCurPsmValues].pclPsmConFlightDate[0],pcgFltDate);
                    ConvertFlightNumber(pclItem,&prgPsmValues[igCurPsmValues].pclPsmConFlight[0]);
                 }
                 igCurPsmValues++;
              }
              else
                 dbg(DEBUG,"%s Unknown PRM Info Format",pclFunc);
           }
        }
        ilLine++;
        pclTmpPtr = GetLine(pclTlxDat,ilLine);
        if (pclTmpPtr != NULL &&
            (strncmp(pclTmpPtr,"ENDPSM",6) == 0 || strncmp(pclTmpPtr,"ENDPART",7) == 0))
           pclTmpPtr = NULL;
        if (pclTmpPtr != NULL && clAdid == 'A' && *pclTmpPtr == '-')
           pclTmpPtr = NULL;
        if (pclTmpPtr != NULL && clAdid == 'D' && *pclTmpPtr == '-')
        {
           ilLine++;
           pclTmpPtr = GetLine(pclTlxDat,ilLine);
        }
     }
  }

  return ilRC;
} /* end of PSMInterpretation  */


static int SendPSMDPXRes()
{
  int ilRC = RC_SUCCESS;
  char pclFunc[]="SendPSMDPXRes:";
  int ilRCdb = DB_SUCCESS;
  short slFkt;
  short slCursor;
  char pclSqlBuf[1024];
  char pclSelection[1024];
  char pclDataBuf[1024];
  int ilI;
  char pclFieldList[1024];
  char pclValueList[1024];
  char pclCurTime[16];
  char pclUrnoList[1024];
  int ilNoEle;
  char pclCurUrno[16];
  int ilCount;
  char pclAltUrno[16];
  char pclFilt[16];
  char pclAlc[16];
  int ilInsert;
  char pclName[128];
  char pclTime[16];
  char pclAlmFields[128];
  char pclAlmData[1024];
  char pclDpxUrno[16];
  char pclDpxSelection[1024];
  char pclDpxFields[128];
  char pclDpxData[1024];

  if (igPSMInterpretation == FALSE)
     return ilRC;
  if (strlen(pcgAftUrno) <= 1)
     return ilRC;
  if (igCurPsmValues == 0)
     return ilRC;

  strcpy(pclFilt," ");
  if (strlen(pcgHandlingTypeForPRM) > 0 && strlen(pcgAlc3ForPRM) > 0)
  {
     ilCount = 1;
     ilRC = syslibSearchDbData("ALTTAB","ALC3",pcgAlc3ForPRM,"URNO",pclAltUrno,&ilCount,"\n");
     TrimRight(pclAltUrno);
     sprintf(pclSqlBuf,"SELECT HSNA FROM HAITAB WHERE TASK = '%s' AND ALTU = %s",
             pcgHandlingTypeForPRM,pclAltUrno);
     dbg(DEBUG,"%s <%s>",pclFunc,pclSqlBuf);
     slCursor = 0;
     slFkt = START;
     ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclFilt);
     close_my_cursor(&slCursor);
     if (ilRCdb == DB_SUCCESS)
        TrimRight(pclFilt);
  }

  sprintf(pclSelection,"WHERE (FLNU = %s OR TFLU = %s) AND ABSR = 'PSM' AND USEC = 'FDIHDL'",
          pcgAftUrno,pcgAftUrno);
  strcpy(pclSqlBuf,"DELETE FROM DPXTAB ");
  strcat(pclSqlBuf,pclSelection);
  ilRC = SendCedaEvent(7150,0,pcgAftDestName,pcgAftRecvName,
                       pcgAftTwStart,pcgAftTwEnd,
                       "DRT","DPXTAB",pclSelection,
                       "","","",3,NETOUT_NO_ACK);
  dbg(DEBUG,"%s Selection <%s>",pclFunc,pclSelection);
  dbg(DEBUG,"%s Sent <DRT> for <DPXTAB>. Returned <%d>",pclFunc,ilRC);

  for (ilI = 0; ilI < igCurPsmValues; ilI++)
  {
     if (prgPsmValues[ilI].pclPsmConFlight[0] != ' ')
     {
        strcpy(&prgPsmValues[ilI].pclPsmConUrno[0]," ");
        sprintf(pclSqlBuf,"SELECT URNO,STOD FROM AFTTAB ");
        sprintf(pclSelection,"WHERE FLNO = '%s' AND STOD LIKE '%s%%' AND ORG3 = '%s'",
                &prgPsmValues[ilI].pclPsmConFlight[0],
                &prgPsmValues[ilI].pclPsmConFlightDate[0],
                pcgHomeAP);
        strcat(pclSqlBuf,pclSelection);
        dbg(DEBUG,"%s <%s>",pclFunc,pclSqlBuf);
        slCursor = 0;
        slFkt = START;
        ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
        close_my_cursor(&slCursor);
        if (ilRCdb == DB_SUCCESS)
        {
           BuildItemBuffer(pclDataBuf,"",2,",");
           get_real_item(&prgPsmValues[ilI].pclPsmConUrno[0],pclDataBuf,1);
           TrimRight(&prgPsmValues[ilI].pclPsmConUrno[0]);
           get_real_item(&prgPsmValues[ilI].pclPsmConStod[0],pclDataBuf,2);
           TrimRight(&prgPsmValues[ilI].pclPsmConStod[0]);
           dbg(DEBUG,"%s Found Flight with URNO,STOD = <%s>",pclFunc,pclDataBuf);
           strcpy(pclAlc,&prgPsmValues[ilI].pclPsmConFlight[0]);
           if (strlen(pcgHandlingTypeForPRM) > 0)
           {
              ilCount = 1;
              if (pclAlc[2] == ' ')
              {
                 pclAlc[2] = '\0';
                 ilRC = syslibSearchDbData("ALTTAB","ALC2",pclAlc,"URNO",pclAltUrno,&ilCount,"\n");
              }
              else
              {
                 pclAlc[3] = '\0';
                 ilRC = syslibSearchDbData("ALTTAB","ALC3",pclAlc,"URNO",pclAltUrno,&ilCount,"\n");
              }
              TrimRight(pclAltUrno);
              sprintf(pclSqlBuf,"SELECT HSNA FROM HAITAB WHERE TASK = '%s' AND ALTU = %s",
                      pcgHandlingTypeForPRM,pclAltUrno);
              dbg(DEBUG,"%s <%s>",pclFunc,pclSqlBuf);
              slCursor = 0;
              slFkt = START;
              ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,&prgPsmValues[ilI].pclPsmConHdlAgt[0]);
              close_my_cursor(&slCursor);
              if (ilRCdb == DB_SUCCESS)
                 TrimRight(&prgPsmValues[ilI].pclPsmConHdlAgt[0]);
           }
        }
        else
           strcpy(&prgPsmValues[ilI].pclPsmConUrno[0],"0");
     }
     else
        strcpy(&prgPsmValues[ilI].pclPsmConUrno[0],"0");
  }

  strcpy(pclUrnoList,"");
  TimeToStr(pclCurTime,time(NULL));
  if (strlen(pcgHandlingTypeForPRM) > 0)
     strcpy(pclFieldList,"NAME,PRMT,FLNU,TFLU,TLXU,SEAT,ABSR,FDAT,SATI,ABDT,FILT,CDAT,USEC");
  else
     strcpy(pclFieldList,"NAME,PRMT,FLNU,TFLU,TLXU,SEAT,ABSR,FDAT,SATI,ABDT,CDAT,USEC");
  for (ilI = 0; ilI < igCurPsmValues; ilI++)
  {
     if (strlen(pcgHandlingTypeForPRM) > 0)
        sprintf(pclValueList,"%s,%s,%s,%s,%s,%s,PSM,%s,%s,%s,%s,%s,FDIHDL",
                &prgPsmValues[ilI].pclPsmName[0],
                &prgPsmValues[ilI].pclPsmType[0],
                pcgAftUrno,
                &prgPsmValues[ilI].pclPsmConUrno[0],
                pcgNextTlxUrno,
                &prgPsmValues[ilI].pclPsmSeat[0],
                pcgFlightSchedTime,
                pcgFlightSchedTime,
                pclCurTime,
                pclFilt,
                pclCurTime);
     else
        sprintf(pclValueList,"%s,%s,%s,%s,%s,%s,PSM,%s,%s,%s,%s,FDIHDL",
                &prgPsmValues[ilI].pclPsmName[0],
                &prgPsmValues[ilI].pclPsmType[0],
                pcgAftUrno,
                &prgPsmValues[ilI].pclPsmConUrno[0],
                pcgNextTlxUrno,
                &prgPsmValues[ilI].pclPsmSeat[0],
                pcgFlightSchedTime,
                pcgFlightSchedTime,
                pclCurTime,
                pclCurTime);
     ilInsert = TRUE;
     if (igSendMsgToAlerterForPAL_PSM == TRUE)
     {
        strcpy(pclName,&prgPsmValues[ilI].pclPsmName[0]);
        if (strcmp(&pclName[strlen(pclName)-2],"MR") == 0)
           pclName[strlen(pclName)-2] = '\0';
        else if (strcmp(&pclName[strlen(pclName)-3],"MRS") == 0)
           pclName[strlen(pclName)-3] = '\0';
        sprintf(pclSqlBuf,"SELECT URNO FROM DPXTAB ");
        if (igPsmDelCAL == TRUE)
        {
          sprintf(pclSelection,
            "WHERE FLNU = %s AND NAME LIKE '%s%%' AND ABSR IN ('PAL','Confirm PSM','CAL')",
            pcgAftUrno,pclName);
        }
        else
        {
          sprintf(pclSelection,
            "WHERE FLNU = %s AND NAME LIKE '%s%%' AND ABSR IN ('PAL','Confirm PSM')",
            pcgAftUrno,pclName);
        }
        strcat(pclSqlBuf,pclSelection);
        dbg(DEBUG,"%s <%s>",pclFunc,pclSqlBuf);
        slCursor = 0;
        slFkt = START;
        ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDpxUrno);
        close_my_cursor(&slCursor);
        if (ilRCdb == DB_SUCCESS)
        {
           ilInsert = FALSE;
           TimeToStr(pclTime,time(NULL));
           strcpy(pclAlmFields,"TXTL,URAL,CDAT,USEC");
           sprintf(pclAlmData,pcgRalTextPsm,
                   &prgPsmValues[ilI].pclPsmName[0],pcgFlightNumber,pcgFlightSchedTime);
           strcat(pclAlmData,",");
           strcat(pclAlmData,pcgRalUrnoPsm);
           strcat(pclAlmData,",");
           strcat(pclAlmData,pclTime);
           strcat(pclAlmData,",FDIHDL");
           ilRC = FdiHandleSql("IBT","ALMTAB","",pclAlmFields,pclAlmData,
                               pcgTwStart,pcgTwEndNew,FALSE,TRUE);
           sprintf(pclDpxSelection,"WHERE URNO = %s",pclDpxUrno);
           strcpy(pclDpxFields,"ABSR");
           strcpy(pclDpxData,"Confirm PSM");
           ilRC = SendCedaEvent(7150,0,pcgAftDestName,pcgAftRecvName,
                                pcgAftTwStart,pcgAftTwEnd,
                                "URT","DPXTAB",pclDpxSelection,
                                pclDpxFields,pclDpxData,"",3,NETOUT_NO_ACK);
           dbg(DEBUG,"%s Selection <%s>",pclFunc,pclDpxSelection);
           dbg(DEBUG,"%s FieldList <%s>",pclFunc,pclDpxFields);
           dbg(DEBUG,"%s ValueList <%s>",pclFunc,pclDpxData);
           dbg(DEBUG,"%s Sent <URT> for <DPXTAB>. Returned <%d>",pclFunc,ilRC);

        }
     }
     if (ilInsert == TRUE)
     {
        ilRC = SendCedaEvent(7150,0,pcgAftDestName,pcgAftRecvName,
                             pcgAftTwStart,pcgAftTwEnd,
                             "IRT","DPXTAB","",
                             pclFieldList,pclValueList,"",3,NETOUT_NO_ACK);
        dbg(DEBUG,"%s FieldList <%s>",pclFunc,pclFieldList);
        dbg(DEBUG,"%s ValueList <%s>",pclFunc,pclValueList);
        dbg(DEBUG,"%s Sent <IRT> for <DPXTAB>. Returned <%d>",pclFunc,ilRC);
        if (strcmp(&prgPsmValues[ilI].pclPsmConUrno[0],"0") != 0)
        {
           if (igCreate2DPXRecords == TRUE)
           {
              if (strlen(pcgHandlingTypeForPRM) > 0)
                 sprintf(pclValueList,"%s,%s,%s,%s,%s, ,PSM,%s,%s,%s,%s,%s,FDIHDL",
                         &prgPsmValues[ilI].pclPsmName[0],
                         &prgPsmValues[ilI].pclPsmType[0],
                         &prgPsmValues[ilI].pclPsmConUrno[0],
                         pcgAftUrno,
                         pcgNextTlxUrno,
                         &prgPsmValues[ilI].pclPsmConStod[0],
                         &prgPsmValues[ilI].pclPsmConStod[0],
                         pclCurTime,
                         &prgPsmValues[ilI].pclPsmConHdlAgt[0],
                         pclCurTime);
              else
                 sprintf(pclValueList,"%s,%s,%s,%s,%s, ,PSM,%s,%s,%s,%s,FDIHDL",
                         &prgPsmValues[ilI].pclPsmName[0],
                         &prgPsmValues[ilI].pclPsmType[0],
                         &prgPsmValues[ilI].pclPsmConUrno[0],
                         pcgAftUrno,
                         pcgNextTlxUrno,
                         &prgPsmValues[ilI].pclPsmConStod[0],
                         &prgPsmValues[ilI].pclPsmConStod[0],
                         pclCurTime,
                         pclCurTime);
              ilRC = SendCedaEvent(7150,0,pcgAftDestName,pcgAftRecvName,
                                   pcgAftTwStart,pcgAftTwEnd,
                                   "IRT","DPXTAB","",
                                   pclFieldList,pclValueList,"",3,NETOUT_NO_ACK);
              dbg(DEBUG,"%s FieldList <%s>",pclFunc,pclFieldList);
              dbg(DEBUG,"%s ValueList <%s>",pclFunc,pclValueList);
              dbg(DEBUG,"%s Sent <IRT> for <DPXTAB>. Returned <%d>",pclFunc,ilRC);
           }
           if (strstr(pclUrnoList,&prgPsmValues[ilI].pclPsmConUrno[0]) == NULL)
           {
              strcat(pclUrnoList,",");
              strcat(pclUrnoList,&prgPsmValues[ilI].pclPsmConUrno[0]);
           }
        }
     }
  }

  sprintf(pclSelection,"WHERE URNO = %s",pcgAftUrno);
  ilRC = SendCedaEvent(igAftReceiver,0,pcgAftDestName,pcgAftRecvName,
                       pcgAftTwStart,pcgAftTwEnd,
                       "UFR","AFTTAB",pclSelection,
                       "PRMC","Y","",3,NETOUT_NO_ACK);
  dbg(DEBUG,"%s Selection <%s>",pclFunc,pclSelection);
  dbg(DEBUG,"%s FieldList <PRMC>",pclFunc);
  dbg(DEBUG,"%s ValueList <Y>",pclFunc);
  dbg(DEBUG,"%s Sent <UFR>. Returned <%d>",pclFunc,ilRC);
  if (strlen(pclUrnoList) > 0)
  {
     ilNoEle = GetNoOfElements(pclUrnoList,',');
     for (ilI = 2; ilI <= ilNoEle; ilI++)
     {
        get_real_item(pclCurUrno,pclUrnoList,ilI);
        sprintf(pclSelection,"WHERE URNO = %s",pclCurUrno);
        ilRC = SendCedaEvent(igAftReceiver,0,pcgAftDestName,pcgAftRecvName,
                             pcgAftTwStart,pcgAftTwEnd,
                             "UFR","AFTTAB",pclSelection,
                             "PRMC","Y","",3,NETOUT_NO_ACK);
        dbg(DEBUG,"%s Selection <%s>",pclFunc,pclSelection);
        dbg(DEBUG,"%s FieldList <PRMC>",pclFunc);
        dbg(DEBUG,"%s ValueList <Y>",pclFunc);
        dbg(DEBUG,"%s Sent <UFR>. Returned <%d>",pclFunc,ilRC);
     }
  }

  if (strlen(pclUrnoList) > 0)
     sprintf(pclSelection,"WHERE URNO IN (%s%s)",pcgAftUrno,pclUrnoList);
  else
     sprintf(pclSelection,"WHERE URNO IN (%s)",pcgAftUrno);
  ilRC = SendCedaEvent(7540,0,pcgAftDestName,pcgAftRecvName,
                       pcgAftTwStart,pcgAftTwEnd,
                       "SFC","",pclSelection,
                       "","","",3,NETOUT_NO_ACK);
  dbg(DEBUG,"%s Selection <%s>",pclFunc,pclSelection);
  dbg(DEBUG,"%s Sent <SFC> to flicol. Returned <%d>",pclFunc,ilRC);


/*
for (ilI = 0; ilI < igCurPsmValues; ilI++)
{
   dbg(TRACE,"AKL <%s><%s><%s><%s><%s><%s><%s><%s>",
       pcgAftUrno,pcgNextTlxUrno,
       &prgPsmValues[ilI].pclPsmName[0],
       &prgPsmValues[ilI].pclPsmType[0],
       &prgPsmValues[ilI].pclPsmSeat[0],
       &prgPsmValues[ilI].pclPsmConFlight[0],
       &prgPsmValues[ilI].pclPsmConFlightDate[0],
       &prgPsmValues[ilI].pclPsmConUrno[0]);
}
*/

  return ilRC;
} /* end of SendPSMDPXRes  */


