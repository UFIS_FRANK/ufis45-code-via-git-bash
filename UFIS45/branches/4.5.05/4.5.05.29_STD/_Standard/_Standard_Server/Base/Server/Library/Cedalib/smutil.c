#ifndef _DEF_mks_version_smutil_c
  #define _DEF_mks_version_smutil_c
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_smutil_c[]   MARK_UNUSED = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Library/Cedalib/smutil.c 1.2 2004/08/10 20:29:49SGT jim Exp  $";
#endif /* _DEF_mks_version */
/* ******************************************************************** */
/*									*/
/* CEDA Program 							*/
/*									*/
/* Author		: Jochen Beese modified by Berni		*/
/* Date			: 11/08/95					*/
/* Description		: Base Data Table Utility Functions   		*/
/*    			: copied from BDUTIL (Uses Shared Memory only)	*/
/* ******************************************************************** */


#define STH_USE
#define STH_IS_USED_IN_LIB

#include  <sys/types.h>
#include  <sys/times.h>
#include  <stdio.h>

#include <memory.h>
#include <errno.h>

#include  "glbdef.h"
#include  "sptdef.h"
#include  "initdef.h"

#include  "sthdef.h"


/* ******************************************************************** */
/* The bd_name_to_no routine						*/
/* Returns the Base table number relating to the given base table name	*/
/* Returns RC_FAIL, if the name is unknown				*/
/* ******************************************************************** */
int shm_bd_name_to_no (char *Pname)
{
  if 	  (strcmp (Pname, "SPTAB") == 0) return SPTAB;
  else if (strcmp (Pname, "COTAB") == 0) return COTAB;
  else if (strcmp (Pname, "TETAB") == 0) return TETAB;
  else if (strcmp (Pname, "FITAB") == 0) return FITAB;
  else if (strcmp (Pname, "ROTAB") == 0) return ROTAB;
  else if (strcmp (Pname, "CNTAB") == 0) return CNTAB;
  else if (strcmp (Pname, "CATAB") == 0) return CATAB;
  else if (strcmp (Pname, "ASTAB") == 0) return ASTAB;
  else if (strcmp (Pname, "FHTAB") == 0) return FHTAB;
  else if (strcmp (Pname, "PNTAB") == 0) return PNTAB;
  else if (strcmp (Pname, "ACTAB") == 0) return ACTAB;
  else if (strcmp (Pname, "APTAB") == 0) return APTAB;
  else if (strcmp (Pname, "CKTAB") == 0) return CKTAB;
  else if (strcmp (Pname, "BCTAB") == 0) return BCTAB;
  else if (strcmp (Pname, "BDTAB") == 0) return BDTAB;
  else if (strcmp (Pname, "ALTAB") == 0) return ALTAB;
  else if (strcmp (Pname, "DETAB") == 0) return DETAB;
  else if (strcmp (Pname, "ARTAB") == 0) return ARTAB;
  else if (strcmp (Pname, "GATAB") == 0) return GATAB;
  else if (strcmp (Pname, "NATAB") == 0) return NATAB;
  else if (strcmp (Pname, "PSTAB") == 0) return PSTAB;
  else if (strcmp (Pname, "HGTAB") == 0) return HGTAB;
  else if (strcmp (Pname, "SETAB") == 0) return SETAB;
  else return RC_FAIL; 
} /* shm_bd_name_to_no */

/* ******************************************************************** */
/* The bd_dbtab_to_no routine						*/
/* Returns the Base table number relating to the given DB table name	*/
/* Returns RC_FAIL, if the name is unknown				*/
/* ******************************************************************** */
int shm_bd_dbtab_to_no (char *Pname)
{
  if 	  (strcmp (Pname, "ACTTAB") == 0) return ACTAB;
  else if (strcmp (Pname, "APTTAB") == 0) return APTAB;
  else if (strcmp (Pname, "ALTTAB") == 0) return ALTAB;
  else if (strcmp (Pname, "DENTAB") == 0) return DETAB;
  else if (strcmp (Pname, "ACRTAB") == 0) return ARTAB;
  else if (strcmp (Pname, "GATTAB") == 0) return GATAB;
  else if (strcmp (Pname, "NATTAB") == 0) return NATAB;
  else if (strcmp (Pname, "PSTTAB") == 0) return PSTAB;
  else if (strcmp (Pname, "HAGTAB") == 0) return HGTAB;
  else if (strcmp (Pname, "SEATAB") == 0) return SETAB;
  else return RC_FAIL; 
}

/* ******************************************************************** */
/* The bd_search routine						*/
/* RETURN: RC_SUCCESS or RC_NOT_FOUND or RC_FAIL			*/
/* ******************************************************************** */
int shm_bd_search (char *Pdb_name, char *Pbd_name, int tab_no, int no_bytes,
	       int inc, char *Pattern, int *Pres_rec_no, char **PPresult)
{
  int rc = RC_SUCCESS;
  STHBCB bcb;
  SRCDES srcdes;		/*    allocate search buffer*/
  unsigned long ret = STNORMAL;	/* 	return code from 'STH' */
  int i = 0;
  int real_tab_no=0;

  if (no_bytes > 20)
  {
    dbg (DEBUG, "bd_search: no_bytes too high: %d", no_bytes); 
    
    return RC_FAIL;
  } /* if */

  if (tab_no > 0)
  {
    real_tab_no = tab_no;
  } /* if */
  else
  {
    if (Pbd_name != NULL)
    {
      real_tab_no = shm_bd_name_to_no (Pbd_name);
      if (real_tab_no == RC_FAIL)
      {
	dbg (DEBUG, "bd_search: Unknown BD-Name %s", Pbd_name); 
	
	rc = RC_FAIL;
      } /* if */
    }
    else
    {
      if (Pdb_name != NULL)
      {
	real_tab_no = shm_bd_dbtab_to_no (Pdb_name);
	if (real_tab_no == RC_FAIL)
	{
	  dbg (DEBUG, "bd_search: Unknown DB-Name %s", Pdb_name); 
	  
	  rc = RC_FAIL;
	} /* if */
      }
      else
      {
	dbg (DEBUG, "bd_search: No Tabno-Info given !"); 
	
	rc = RC_FAIL;
      } /* if */
    } /* if */
  } /* if */

  if (rc == RC_SUCCESS)
  {
    /* Args 4 sth call */
    bcb.function = (STRECORD|STSEARCH|STLOCATE);
    bcb.inputbuffer = (char *) &srcdes; 
    bcb.outputbuffer = (char *) PPresult;
    bcb.tabno = real_tab_no;	
    bcb.recno = 0;

    srcdes.nobytes = no_bytes;
    srcdes.increment = inc;
    for (i = 0; i < srcdes.nobytes; i++) 
    {
      srcdes.bitmask |= (BITVHO >> i); 
      srcdes.data[i] = Pattern[i];
    }

    ret = sth(&bcb, &gbladr);

    if (ret != STNOTFND && ret != STNORMAL)
    {
      rc = RC_FAIL;
      dbg (DEBUG, "Bd_search: Error ret = %d", ret);
    } /* fi */
    else if (ret == STNOTFND)
    {
      rc = RC_NOT_FOUND;
      dbg (DEBUG, "Bd_search: Value not found", ret); 
    } /* fi */
    else
    {
      *Pres_rec_no = bcb.recno;
      rc = RC_SUCCESS;
    } /* fi */
  } /* fi */

  return rc;
} /* shm_bd_search */

/* ******************************************************************** */
/* The bd_alc2_to_alc3 routine						*/
/* RETURN: RC_SUCCESS or RC_NOT_FOUND or RC_FAIL			*/
/* ******************************************************************** */
int shm_bd_alc2_to_alc3 (char *Palc2, char *Palc3)
{
  int rc = RC_FAIL;
  char *Ptmp=NULL;
  int result_no=-1;
  int argLen;
  
  argLen = strlen(Palc2);
  if (argLen <= 2)
  {
    rc = shm_bd_search (NULL, NULL, ALTAB, 2, 0, Palc2, &result_no, &Ptmp);
  } /* end if */
  if (rc == RC_SUCCESS)
  {
    memcpy (Palc3, Ptmp + 2, 3);
    Palc2[3] = 0x00;
  } /* fi */

  return rc;
} /* shm_bd_alc2_to_alc3 */

/* ******************************************************************** */
/* The bd_alc3_to_alc2 routine						*/
/* RETURN: RC_SUCCESS or RC_NOT_FOUND or RC_FAIL			*/
/* ******************************************************************** */
int shm_bd_alc3_to_alc2 (char *Palc3, char *Palc2)
{
  int rc = RC_SUCCESS;
  char *Ptmp=NULL;
  int result_no=-1;

  rc = shm_bd_search (NULL, NULL, ALTAB, 3, 2, Palc3, &result_no, &Ptmp);
  if (rc == RC_SUCCESS)
  {
    memcpy (Palc2, Ptmp , 2);
    Palc2[2] = 0x00;
  } /* fi */

  return rc;
} /* shm_bd_alc3_to_alc2 */


/* ******************************************************************** */
/* The bd_get_regi_dat routine						*/
/* RETURN: RC_SUCCESS or RC_NOT_FOUND or RC_FAIL			*/
/* ******************************************************************** */
int shm_bd_get_regi_dat (char *Pregi, char *Pact3, char *Pming, char *Pmaxg)
{
  int rc = RC_SUCCESS;
  char *Ptmp=NULL;
  int result_no=-1;

  rc = shm_bd_search (NULL, NULL, ARTAB, 5, 0, Pregi, &result_no, &Ptmp);
  if (rc == RC_SUCCESS)
  {
    memcpy (Pact3, Ptmp +  5, 3);
    memcpy (Pming, Ptmp +  8, 4);
    memcpy (Pmaxg, Ptmp + 12, 4);
    Pact3[3] = 0x00;
    Pming[4] = 0x00;
    Pmaxg[4] = 0x00;
  } /* fi */

  return rc;
} /* shm_bd_get_regi_dat */

/* ******************************************************************** */
/* The bd_get_pnam_dat routine						*/
/* RETURN: RC_SUCCESS or RC_NOT_FOUND or RC_FAIL			*/
/* ******************************************************************** */
int shm_bd_get_pnam_dat (char *Ppnam, char *Ppdpo, char *Ppcat, int *Precno)
{
  int rc = RC_SUCCESS;
  char *Ptmp=NULL;
  int result_no=-1;
  int arg_len;
  arg_len = strlen(Ppnam) + 1;
  if (arg_len > 4)
  {
    arg_len = 4;
  } /* end if */

  rc = shm_bd_search (NULL, NULL, PSTAB, arg_len, 0, Ppnam, &result_no, &Ptmp);
  if (rc == RC_SUCCESS)
  {
    memcpy (Ppdpo, Ptmp +  4, 4);
    memcpy (Ppcat, Ptmp +  8, 1);
    Ppdpo[4] = 0x00;
    Ppcat[1] = 0x00;
  } /* fi */

  (*Precno) = result_no;
  return rc;
} /* shm_bd_get_pnam_dat */

/* ******************************************************************** */
/* The bd_get_pnam_rec routine						*/
/* RETURN: RC_SUCCESS or RC_NOT_FOUND or RC_FAIL			*/
/* ******************************************************************** */
int shm_bd_get_pnam_rec (char *Ppnam, int *Precno)
{
  int rc = RC_SUCCESS;
  char *Ptmp=NULL;
  int result_no=-1;
  int arg_len;
  arg_len = strlen(Ppnam) + 1;
  if (arg_len > 4)
  {
    arg_len = 4;
  } /* end if */

  rc = shm_bd_search (NULL, NULL, PSTAB, arg_len, 0, Ppnam, &result_no, &Ptmp);
  (*Precno) = result_no;
  return rc;
} /* bd_get_pnam_rec */

/* ******************************************************************** */
/* Space for Appendings (Source-Code Control)				*/
/* ******************************************************************** */

/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
