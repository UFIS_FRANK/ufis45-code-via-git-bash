#ifndef _DEF_mks_version_oracle_c
  #define _DEF_mks_version_oracle_c
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_oracle_c[]   MARK_UNUSED = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Library/Dblib/oracle.c 1.3 2004/10/18 18:48:36SGT bst Exp  $";
#endif /* _DEF_mks_version */
/* ***********************************************************
 * Prototype of a read_rec for DEVSTFILDAT
 * ***********************************************************/
#include <stdio.h>
#include "db_if.h"


extern	FILE	*outp;


/****************************************************************************/

/*
 *
 * Known problemes and critical statements
 *
 * 1.) form_hex_buf  : 1.Parameter must be a char *
 * 2.) get_fld       : 1. Parameter  
 *
 */



/*
 * Following the global variables
 */

char	sql_buf[SQL_BUFSIZE];
char	data_area[MAX_FLD_SIZE];
char	*work_buf;
short	default_cursor=0;

static  short sgUrnoSelectCursor     = 0;
static  short sgUrnoUpdateCursor     = 0;



#ifdef OLDVERSION
/***********************************************************************
 * Following the INIT_DB function for ORACLE
 **********************************************************************/
int init_db()
{
	int	rc;


	rc=sql_if(IGNORE,NULL,"LOGON CEDA/CEDA",NULL);

	return check_ret(rc);
} /* end of init_db */
#endif /* OLD */



/***********************************************************************
 * Following the new INIT_DB function for ORACLE
 **********************************************************************/
int init_db()
{
	char	*envuser,*envpw,db_buf[50];
	int	rc = DB_ERROR;

	if ( (envuser = getenv("CEDADBUSER")) == NULL) {
fprintf(stderr,"Error Reading CEDA-User\n");fflush(stderr);
		return check_ret(rc);
	}
	else if ( (envpw = getenv("CEDADBPW")) == NULL) {
fprintf(stderr,"Error Reading CEDA-Password\n");fflush(stderr);
		return check_ret(rc);
	}
	else 
		sprintf(db_buf,"LOGON %s/%s",envuser,envpw);
	rc = sql_if(IGNORE,NULL,db_buf,NULL);

	return check_ret(rc);
} /* end of init_db */




/***********************************************************************
 * Following the begin transaction  function 
 **********************************************************************/
 int begin_work(void)
 {
	int	rc;
	short	local_curs=0;

	strcpy(sql_buf,"DBMS_TRANSACTION.BEGIN_DISCRETE_TRANSACTION");

	rc = sql_if(IGNORE|REL_CURSOR,&local_curs,sql_buf,NULL);

	return check_ret( rc);
 }


/***********************************************************************
 * Following the rollback function 
 **********************************************************************/
 int rollback(void)
 {
	int	rc;
	short	local_curs=0;

	rc = sql_if(IGNORE|REL_CURSOR,&local_curs,"ROLLBACK",NULL);

	return check_ret( rc);
 } /* end of rollback */



/***********************************************************************
 * Following the commit work function 
 **********************************************************************/
 int commit_work(void)
 {
	int	rc;
	short	local_curs=0;

	rc = sql_if(IGNORE|REL_CURSOR,&local_curs,"COMMIT WORK",NULL);

	return check_ret( rc);
 } /* end of commit work */



/***********************************************************************
 * Following the logoff function 
 **********************************************************************/
 int logoff(void)
 {
	int	rc;
	rc = sql_if(NOACTION,NULL,"LOGOFF",NULL);
	return check_ret(rc);
 } /* end of logoff */




/* *******************************************************************
 *  Following the utility functions
 *
 * - get_oper		to get the relevant operator for the
 *                      WHERE clause
 * - get_fld		to get a specific field out of the data_area
 * - calc_adr		to calculate the adress of one field in the
 *   			buffer
 * - hex_to_byte        to convert a two byte hexa string into one
 *                      byte char
 *
 *  *****************************************************************/
/* ******************************************************************
 * following the get_idx_cnt function
 *
 *
 * ******************************************************************/
get_idx_cnt( char *table )
{
	int	rc;
	short local_curs=0;

	/* the following statement has to be removed after the test */

	return 0;

	sprintf(sql_buf,"SELECT COUNT (*) from %s;",table);

	rc = sql_if(START|REL_CURSOR,&local_curs,sql_buf,data_area);
	check_ret(rc);

	if ( rc != DB_SUCCESS ) {
		return rc;
	} /* end if */

	get_fld(data_area,FIELD_1,LONG,IGNORE,(char *) &rc);

	return rc;

}/* end of get index count */


/* ******************************************************************
 * following the get_fld function
 *
 * get_fld reads one field out the buffer and converts it to
 * the specified data type
 * ATTENTION
 * -----------
 * The get_fld function will not work over data areas containing
 * LONG RAW Data type informations
 * ******************************************************************/
void get_fld(char *buf,short fld_no,short fld_type,int fld_len,char *dest)
{
	int	i;
	char	*offs;
	int	length;
	short	raw_length;
	short	short_val;
	int	int_val;
	long	long_val;
	char	asc_val[10];
	char	cur_word[10];
	char	msg[150];

	

	offs = calc_adr(buf,fld_no);

	if ( offs == (char *)NULL) {
		printf("Oracle.getfld : NULL OFFSET ERROR\n");
		memcpy(dest,"DBER",4);
		return;
	} /* end if */

	/* sprintf(msg,"ORACLE: get_fld : dest is %ld. Offs in data_area is %d.",
				dest,offs); */
	/* ora_debug(msg); */

	switch( fld_type ) {
		case	STR	:
			length	= strlen(offs);
			length  = ( length > fld_len) ? fld_len : length;
			memcpy((char *)dest,offs,length+1);
			/* fprintf(outp,"Oracle.getfld : String : %s\n",offs); */
			return;
			break;
		case	SHORT	:
			length = sizeof(short);
			strcpy(asc_val,offs);
			short_val = (short )atoi(asc_val);
			/* fprintf(outp,"Oracle.getfld : Short : %d\n",short_val);  */
			memcpy((short *)dest,&short_val,sizeof(short));
			break;
		case	INT	:
			length = sizeof(int);
			strcpy(asc_val,offs);
			int_val = (int )atoi(asc_val);
			memcpy((int *)dest,&int_val,sizeof(int));
			/* fprintf(outp,"Oracle.getfld : Int : %d\n",int_val);  */
			break;
		case	LONG	:
			length = sizeof(long);
			strcpy(asc_val,offs);
			long_val = (long )atol(asc_val);
			memcpy((long *)dest,&long_val,sizeof(long));
			/*  fprintf(outp,"Oracle.getfld : Long : %d\n",long_val);  */
			break;
		case	RAW	:
			length = fld_len;
			/* printf("Oracle.getfld : RAW over %d\n",fld_len);*/
			/* printf("RAW field is <%s> \n",offs);*/
			for ( i=0; i<length; i++ ) {
				memcpy(cur_word,(offs+(i*2)),2);
				*(dest+i) = hex_to_byte(cur_word);
			} /* end_for */
			break;
		case	LONGRAW	:
			offs++; /* jump over field delimiter */
			memcpy(&raw_length,offs,sizeof(short));
			/* fprintf(outp,"Oracle.getfld %d bytes long raw\n",raw_length); */
			offs+=2;
			memcpy(dest,offs,raw_length);
			break;	
		default		:
			printf("Oracle.getfld : INVALID FIELD TYPE\n");
			break;
	} /* end of switch_type */
	return;
} /* end of get_fld */



/* ******************************************************************
 * following the calc_adr function
 *
 * calc_adr returns the offset of a specified field
 * ATTENTION
 * -----------
 * The calc_adr function will not work over data areas containing
 * LONG RAW Data type informations
 * ******************************************************************/
 char *calc_adr(char *buf,short fldno)
{
	short	i;	/* loop variable */


	for ( i = 0 ; i < fldno ; i++ ) {
		buf += strlen(buf);
		if (*buf != 0x00 ) {  	/* buffer does not    */
			return NULL;    /* contain spec no of */
		} /* end if */          /* fields             */
		else {
			buf ++;         /* go further         */
		} /* end else */
	} /* end for */

	return buf;                     /* return fields adr  */
} /* end of calc_adr */



/* ******************************************************************
 * following the hex_to_byte function
 *
 * hex_to_byte converts a two byte string containing data in
 * hexa notation into one byte containing the value
 * ******************************************************************/
char hex_to_byte(char *hex_val)
{
	long	long_val;
	char	rc;
	char	xval[4];

	rc = 0x0;

	toupper(hex_val[0]);
	toupper(hex_val[1]);

	memset(xval,0x00,4);
	memcpy(xval,hex_val,2);

	long_val = strtol(xval,NULL,16);

	rc = (char)long_val;

	return rc;
} /* end of hex_to_byte */



/* ******************************************************************
 * following the form_hexa_buf function
 *
 * form_hexa_buf builds a character string in hexadezimal notation
 * Ex. If a char x contains the value 0xff form_hex_buf will return
 * a char string containing "0xff0x00"
 * ATTENTION :
 * -----------
 * The destination buffer must have a minumum size of
 * ((len *2) +1) bytes
 * ******************************************************************/
 void form_hex_buf(char *src,int len,char *dest)
{
	int	i;			/* loopi 	    */
	char	cur_byte[3];		/* the current byte */

	memset(dest,0x00,len *2 +1);		/* set null terminator */

	mtox(dest,src,len);

	return;

} /* end of form_hex_buffer */




void form_char_buf(char *src,int len,char *dest)
{
	int	i;

	for ( i = 0 ; i<len ; i++ ) {
		*(dest+i) = *(src+i)+128 ;
	} /* end for */
	*(dest+len) = 0x00;

	return;

} /* end of form_char_buf */



short delton(char *buffer)
{
	short	i;
	short	len;

	len = strlen(buffer);

	for ( i = 0 ; i < len; i++ ) {
		if ( *(buffer+i) == ',' ) {
			*(buffer+i) = 0x00;
		} /* end if */
	} /* end for */

	return i;
} /* end of NewLine TO Null */




short nlton(char *buffer)
{
	short	i;
	short	len;

	len = strlen(buffer);

	for ( i = 0 ; i < len; i++ ) {
		if ( *(buffer+i) == '\n' ) {
			*(buffer+i) = 0x00;
		} /* end if */
	} /* end for */

	return i;
} /* end of NewLine TO Null */



int close_my_cursor(short *cursor)
{
	commit_work();
	return sql_if(NOACTION,cursor,"CLOSE",data_area);
}



int sql_if(short cmd,short *curs,char *sqlcmd,char *data)
{
	int	rc;

	/* fprintf(outp,"SQL_IF: sql_buf : <%s>\n",sqlcmd); fflush(outp); */

	rc =   db_if(cmd,curs,sqlcmd,data);

	return rc;

}



int GetUrno(int *pipUrno)
{
	int     ilRC = RC_SUCCESS;
	char    pclUpdateBuf[1024];

	if(ilRC == DB_SUCCESS)
	{
		ilRC = sql_if(START,&sgUrnoSelectCursor,"SELECT actual_sequence FROM snotab FOR UPDATE",data_area);
		if(ilRC != DB_SUCCESS)
		{
			*pipUrno = -1;
			ilRC = check_ret(ilRC);
		}/* end of if */
	}/* end of if */

	if(ilRC == DB_SUCCESS)
	{
		get_fld(data_area,FIELD_1,INT,IGNORE,(char *)pipUrno);
		*pipUrno = *pipUrno + 1;

		sprintf(pclUpdateBuf,"UPDATE snotab SET actual_sequence=actual_sequence+1");

		ilRC = sql_if(START,&sgUrnoUpdateCursor,pclUpdateBuf,data_area);
		if(ilRC != DB_SUCCESS)
		{
			*pipUrno = -1;
			ilRC = check_ret(ilRC);
		} /* end if */
	} /* end if */

	commit_work();

	return(ilRC);

}/* end of GetUrno */

