#ifndef _DEF_mks_version_cdilib_h
  #define _DEF_mks_version_cdilib_h
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_cdilib_h[]   MARK_UNUSED = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Library/Inc/cdilib.h 1.2 2004/07/27 16:46:54SGT jim Exp  $";
#endif /* _DEF_mks_version */
/*  The Master include file.						  */
/*									  */
/*  Program	  :     						  */
/*  Rev							  */
/*  									  */
/*  NOTE : This should be the only include file for your program          */
/* ********************************************************************** */
#ifdef __cplusplus
extern "C" {  // only need to export C interface if
              // used by C++ source code
#endif


#include<stdio.h>
#include<stdlib.h>
#include <malloc.h>
#include <string.h>
#ifndef WIN32
#include<unistd.h>
#endif
#include<errno.h>
#include<memory.h>
#include<signal.h>
#include<ctype.h>
#include<math.h>
#include <sys/timeb.h>
#include <sys/types.h>
#if defined (_DECUNIX) || defined(_IBMAIX)
#include <netinet/in.h> 
#include <sys/time.h> 
#endif
#ifdef WIN32
#include <process.h>
#include <winsock.h>
#include <time.h>
//#include <types.h>
#else
#include <sys/socket.h>
#include <netdb.h> 
#include <stdarg.h>
#ifdef ODT5
#include <netinet/in.h> 
#endif

/*
#include <sys/times.h>
#include <sys/signal.h>
#include <sys/wait.h>
*/

#if defined(_HPUX_SOURCE) || defined(_SNI) || defined(_SOLARIS) || defined(_LINUX) || defined(_DECUNIX)
#include <time.h> 
#else
#include <sys/itimer.h>
#endif

#if !defined(_HPUX_SOURCE) && !defined(_SNI) && !defined(_LINUX) && !defined(_DECUNIX)

#include <sys/select.h>
#endif

#ifdef _LINUX
#include <sys/time.h>
#include <netinet/in.h>
#endif

#ifndef _WINNT
#include <time.h>
#endif

#include <sys/timeb.h>

#if !defined (_HPUX_SOURCE) && !defined(_WINNT) && !defined(_SOLARIS) && !defined(_SNI) && !defined(_LINUX) && !defined(_DECUNIX) && !defined(_IBMAIX)
#include <prototypes.h>
#endif

#endif   /* end of #ifdef WIN32 */

/* default values for configuration */

#define MAX_CONNECT_RETRIES 10       
#define MAX_NO_OF_ERRORS     5 
#define HSB_SYSTEM          "N"
#define TCP_TIMEOUT	    10.0
#define CCO_INTERVAL	    10.0
#define CCO_RESPONSE	    10.0

	

#define		EOS '\0'
#define		MAXLINE 80
#define		LINE_CHR 16
#define		PCHR '$'
#define		XCHR '#'
#define		CCHR '*'
#define		CFG_STRING	1
#define		CFG_INT		2
#define		CFG_FLOAT	3
#ifndef	CDI_FALSE
#define		CDI_FALSE		-1
#endif
#ifndef CDI_TRUE	
#define 	CDI_TRUE		1
#endif
#ifndef	OK
#define		OK		0
#endif
#ifndef FREE	
#define	FREE		2
#endif
#ifndef RESERVED	
#define	RESERVED		3
#endif
#ifndef USED	
#define	USED		4
#endif

#define		E_FOPEN		-1
#define		E_SECTION	-2
#define		E_PARAM		-3
#define		E_ERROR		-4
#define		E_EMPTY		-5
#define 	CDI_MIN_DATA_LEN 3
#define 	CDI_MAX_DATA_LEN 9999

#ifdef U_MAIN
#define MAIN		int main(int argc,char *argv[]) 

/* #include "catchal.h" */

#endif

/*	UGCCS error codes */
#ifndef SUCCESS
#define SUCCESS		0	/* return from subroutine successful */
#endif

#ifndef NONFATAL
#define NONFATAL	1	/* Non-fatal errors are pos. numbers */
#endif

#ifndef FATAL
#define FATAL	    -1	/* Fatal errors are negative numbers */
#endif

/* More different error codes */

#ifndef CDI_SUCCESS
#define CDI_SUCCESS 0 
#endif

#ifndef CDI_FAIL
#define CDI_FAIL    		-1  /* Describes serious errors */
#endif


#ifndef CDI_ERROR
#define CDI_ERROR    		-2  /* Describes soft errors, e.g. false
															user input, that can be corrected */	
#endif

#ifndef CDI_SERR
#define CDI_SERR    		-3  /* Socket read error */	
#endif

#ifndef CDI_TOUT
#define CDI_TOUT    		-4  /* Time out error */	
#endif

#ifndef CDI_NACT
#define CDI_NACT	-6 /* No activity at socket */
#endif

#ifndef CDI_NOT_FOUND
#define CDI_NOT_FOUND	-7
#endif

#ifndef CDI_ECONN
#define CDI_ECONN	-8 /* Connect error */
#endif

#define NUL		0	/* Makes the source code more readable */


#define	OFF			  0x0000
#define	TRACE		  0x1000
#define	DEBUG		  0x1001
#define DEFAULT_DEBUG 0x0000 /* OFF */	

/*#define		NETREAD_SIZE		(35000)
#define		NET_DATA		(0x2)
#define		PACKET_START		(-200)
*/
#define		NET_SHUTDOWN		(0x1)
#define		PACKET_END		(-300)
#define		PACKET_DATA		(-100)
#define		PACKET_LEN              (1024)
#define		iMAX_CMDS  256 
#define		iMAX_CLASSES  256 
#define		MAX_TGRAM_SIZE  4096  /* Max. telegram size */
#define		MAX_FIELD_LEN  128 
#define		iMAX_READ_BUF  2048 
#define   iMIN 32
#define   iMAX 160
#define   iTIME_SIZE 16 


typedef struct {
	char cmd[5];
	int cmd_type;
	char args[MAX_FIELD_LEN+1];
	char table[8];
	char fields[MAX_FIELD_LEN+1];
	char separator[2];
} _CMD;

typedef struct {
	char class[5];
	int max_val;
	int min_val;
} _TGRAM_CLASS;

typedef struct {
	int status;
	int secure_system;
	int max_no_of_errors;
	int max_connect_retries;
	int connect_num;
	int socket;
	double cco_interval;
	double cco_response;
	double tcp_timeout;
	char dest_host[2][iMIN+1];
	char dest_port[2][iMIN+1];
	char valid_cmds[iMAX_CMDS+1];
	char valid_dat_cmds[iMAX_CMDS+1];
	char valid_del_cmds[iMAX_CMDS+1];
	_CMD *cmd_defn;
	char tgram_classes[iMAX_CLASSES+1];
	_TGRAM_CLASS *tgram_class_defn;
	char server_sys[4];
	char sender[11];
	char receiver[11];
	char version[4];
	char hsb_system[2];
} _CLIENT;


typedef	struct {
		char server_sys[3];
		char sender[10];
		char receiver[10];
		char version[3];
		char time_stamp[14];
		char  tgram_no[5];
		char tgram_class[3];
		char len[4];
		char data[1];
} _TGRAM_HDR;



/* Define some global variables */
/* extern char *mod_name=NULL;
extern int	debug_level=TRACE;
extern FILE 	*outp=NULL; */

/*Exported  Prototypes */

/* **************************** */
/*                              */
/* CDI Client Library functions */
/*                              */
/* **************************** */
extern int CDIInitClient(char *, char *);
extern int CDIOpenConnection(int);
extern int CDIResetConnection(int);
extern int CDIGetClient(int,_CLIENT *);
extern int CDIBuildHeader(int,_TGRAM_HDR *,int ,char *);
extern int CDIAnswerCCO(int,_TGRAM_HDR *);
extern int CDISendCCO(int,int ,_TGRAM_HDR *);
extern int CDISendErr(int,int ,_TGRAM_HDR *,char *);
extern int CDISendData(int,_TGRAM_HDR *,char *,int);
extern int CDICloseConnection(int,int,_TGRAM_HDR *,char *);
extern int CDIReceiveData(int,_TGRAM_HDR **,int *);
extern int CDITcpReceiveData(int,_TGRAM_HDR **,int *);
extern int CDICheckTgramHdr(int,_TGRAM_HDR *,char *);
extern int CDIBuildData(int,_TGRAM_HDR *,int TgramNo,char *pcpCmd,char *pcpTgramClass, char *pcpSrc,char *pcpType,int ipKey,char *pcpFldLst,char *pcpDataLst,char cpFldSep);
extern int CDIBuildFreeData(int,_TGRAM_HDR *,int,char *,...);
extern int CDITcpWaitTimeOut(int,double t);
extern int *CDITcpMultiWaitTimeOut();
extern void CDIPrintClient(int );
extern void CDIPrintTgramHdr(_TGRAM_HDR *prpTgramHdr,int ipDataLen);
extern int CDICheckCCOInterval(int,time_t *);
extern void CDISetRealTimer(int,int,int,int);
extern void CDISetVirtualTimer(int,int,int,int);

extern int snap(char *pmem,long lim,FILE *outp);
extern int field_count(char *);
extern void dbg(int,char *,...);
/* extern int ftime(struct timeb *); */
char mod_name[100];
/* int debug_level = 0; */


#ifdef __cplusplus
}
#endif

