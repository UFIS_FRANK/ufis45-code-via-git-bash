#ifndef _DEF_mks_version_tcpudp_c
  #define _DEF_mks_version_tcpudp_c
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_tcpudp_c[]   MARK_UNUSED = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Library/Cedalib/tcpudp.c 1.4 2010/05/18 23:00:17SGT jha Exp  $";
#endif /* _DEF_mks_version */
/* *************************************************************** */
/*                                                                 */
/*                                                                 */
/*                                                                 */
/* 20060123 JIM: added flag for AIX                                */
/* *************************************************************** */


  
 
#include "tcpudp.h"

#define TCP_SHUTDOWN    0x14046701
#define TCP_DATA        0x14046702
#define TCP_KEEPALIVE   0x14046703
#define TCP_ACKNOWLEDGE 0x14046704

typedef struct {
        long command;
        long length;
}TCP_INFOHEADER;
#define TCP_INFOHEADER_SIZE sizeof(TCP_INFOHEADER)

static TCP_INFOHEADER InfoHeader;

#define HEADER_COMMAND_SIZE 20
#define HEADER_MSG_LENGTH_SIZE 10
static char cgHeaderCommand[HEADER_COMMAND_SIZE] = "\0";
static char cgHeaderMsgLength[HEADER_MSG_LENGTH_SIZE] = "\0";
static long lgMsgLength = 0;

static int igAcceptSocket = 0;

static int TcpCreateSocket(int *pipSocket, char *pcpService, char *pcpHost, struct sockaddr_in *prpSocketNamei, int ipBindFlag);
static int TcpRecvBuf(int ipSocket, char *pcpBuf, int ipBufLen);
static int TcpSendBuf(int ipSocket, char *pcpBuf, int ipBufLen);
static int TcpCloseSocket(int ipSocket);

/* extern FILE *outp; only for snap */

/* *************************************************************** */
/*                                                                 */
/*                                                                 */
/*                                                                 */
/* *************************************************************** */
int TcpAccept(int *pipSocket, char *pcpService, char *pcpHost)
{
	int ilRC = RC_SUCCESS;
#if defined(_AIX)
  /* 20060123 JIM: added flag for AIX */
	socklen_t ilRecLen = 0;
#else
	int ilRecLen = 0;
#endif
	struct sockaddr_in rlSocketName;
	struct sockaddr_in rlClientName;
	struct in_addr *prlInAddr = NULL;

	/***** Parameter check *****/

	if(pipSocket == NULL)
	{
		ilRC = RC_FAIL;
		dbg(TRACE,"TcpAccept: invalid 1st Parameter");
	}/* end of if */

	if((pcpService == NULL) || (strlen(pcpService) == 0))
	{
		ilRC = RC_FAIL;
		dbg(TRACE,"TcpAccept: invalid 2nd Parameter");
	}/* end of if */
	
	if((pcpHost == NULL) || (strlen(pcpHost) == 0))
	{
		ilRC = RC_FAIL;
		dbg(TRACE,"TcpAccept: invalid 3rd Parameter");
	}/* end of if */
	

	/***** functionality *****/

	if(ilRC == RC_SUCCESS)
	{
		ilRC = TcpCreateSocket(&igAcceptSocket, pcpService, pcpHost, &rlSocketName, TRUE);
	}/* end of if */

	if(ilRC == RC_SUCCESS)
	{
		prlInAddr = (struct in_addr *) &rlSocketName.sin_addr.s_addr;

		dbg(DEBUG,"TcpAccept: Socket created <%d> <%s> <%s>",igAcceptSocket, pcpService, pcpHost, &rlSocketName);
		dbg(DEBUG,"TcpAccept: Socket family <%d>",rlSocketName.sin_family);
		dbg(DEBUG,"TcpAccept: Socket port   <%d>",ntohs(rlSocketName.sin_port));
		/* dbg(DEBUG,"TcpAccept: Socket addr   <%s>",inet_ntoa((struct in_addr)rlSocketName.sin_addr.s_addr)); */
		dbg(DEBUG,"TcpAccept: Socket addr   <%s>",inet_ntoa(*prlInAddr));

		errno = 0;
		ilRC = listen(igAcceptSocket,5);
		if( ilRC < 0 )
		{
			dbg(TRACE,"TcpAccept: listen <%s>",strerror(errno));
			TcpCloseSocket(igAcceptSocket);
			igAcceptSocket = 0;
			ilRC = RC_FAIL;
		} else {
			dbg(DEBUG,"TcpAccept: listen returned <%d>",ilRC);
			ilRC = RC_SUCCESS;
		}/* end of if */
	}/* end of if */
	
	if(ilRC == RC_SUCCESS)
	{
		ilRecLen = sizeof(struct sockaddr_in);

		errno = 0;
		*pipSocket = accept(igAcceptSocket,(struct sockaddr *) &rlClientName, &ilRecLen);
		if( *pipSocket < 0 )
		{
			if(errno != EINTR)
			{
				dbg(TRACE,"TcpAccept: accept <%s>",strerror(errno));
			} /* end if */
			ilRC = RC_FAIL;
		}else{
			/* dbg(DEBUG,"TcpAccept: Socket addr   <%s>",inet_ntoa(rlClientName.sin_addr.s_addr)); */
			prlInAddr = (struct in_addr *) &rlClientName.sin_addr.s_addr;
			dbg(DEBUG,"TcpAccept: Socket addr   <%s>",inet_ntoa(*prlInAddr));
		} /* end if */

		TcpCloseSocket(igAcceptSocket);
		igAcceptSocket = 0;
	}/* end of if */

	return(ilRC);

}/* end of TcpAccept */


/* *************************************************************** */
/*                                                                 */
/*                                                                 */
/*                                                                 */
/* *************************************************************** */
int TcpConnect(int *pipSocket, char *pcpService, char *pcpHost)
{
	int ilRC = RC_SUCCESS;
	struct sockaddr_in rlSocketName;
	struct in_addr *prlInAddr = NULL;


	/***** Parameter check *****/

	if(pipSocket == NULL)
	{
		ilRC = RC_FAIL;
		dbg(TRACE,"TcpConnect: invalid 1st Parameter");
	}/* end of if */

	if((pcpService == NULL) || (strlen(pcpService) == 0))
	{
		ilRC = RC_FAIL;
		dbg(TRACE,"TcpConnect: invalid 2nd Parameter");
	}/* end of if */
	
	if((pcpHost == NULL) || (strlen(pcpHost) == 0))
	{
		ilRC = RC_FAIL;
		dbg(TRACE,"TcpConnect: invalid 3rd Parameter");
	}/* end of if */
	

	/***** functionality *****/

	if(ilRC == RC_SUCCESS)
	{
		ilRC = TcpCreateSocket(pipSocket,pcpService,pcpHost,&rlSocketName,FALSE);
		if(ilRC == RC_SUCCESS)
		{
			dbg(DEBUG,"TcpConnect: Socket created <%d> <%s> <%s>",*pipSocket, pcpService, pcpHost, pcpService);
			dbg(DEBUG,"TcpConnect: Socket family <%d>",rlSocketName.sin_family);
			dbg(DEBUG,"TcpConnect: Socket port   <%d>",ntohs(rlSocketName.sin_port));
			/* dbg(DEBUG,"TcpConnect: Socket addr   <%s>",inet_ntoa(rlSocketName.sin_addr.s_addr)); */
			
			prlInAddr = (struct in_addr *) &rlSocketName.sin_addr.s_addr;
			dbg(DEBUG,"TcpConnect: Socket addr   <%s>",inet_ntoa(*prlInAddr));

			errno = 0;
         #if defined(_SOLARIS)
			if (connect(*pipSocket,(struct sockaddr *) &rlSocketName, sizeof(rlSocketName)) < 0) 
			#else
			if (connect(*pipSocket,(const struct sockaddr *) &rlSocketName, sizeof(rlSocketName)) < 0) 
			#endif
			{
				dbg(DEBUG,"TcpConnect: connect <%s>",strerror(errno));
				TcpCloseSocket(*pipSocket);
				ilRC = RC_FAIL;
			}/* end of if */
		}/* end of if */
	}/* end of if */

	return(ilRC);

}/* end of TcpConnect */


/* *************************************************************** */
/*                                                                 */
/*                                                                 */
/*                                                                 */
/* *************************************************************** */
static int TcpCreateSocket(int *pipSocket, char *pcpService, char *pcpHost, struct sockaddr_in *prpSocketName, int ipBindFlag)
{
	int ilRC = RC_SUCCESS;
#if defined(_AIX)
  /* 20060123 JIM: added flag for AIX */
	socklen_t ilRecLen = 0;
#else
	int ilRecLen = 0;
#endif
	int ilReuse = 0;
	int ilKeepalive = 0;
	struct linger rlLinger;
	struct servent *prlServiceEntry = NULL;
	struct hostent *prlHostEntry = NULL;
	struct in_addr *prlInAddr = NULL;


	/***** Parameter check *****/

	/* FLW: internal static functions always get correct parameters !!! */



	/***** functionality *****/

	if(ilRC == RC_SUCCESS)
	{
		errno = 0;
		if ((*pipSocket = socket(AF_INET, SOCK_STREAM, 0)) < 0) 
		{
			dbg(TRACE,"TcpCreateSocket: socket <%s>",strerror(errno));
			TcpCloseSocket(*pipSocket);
			ilRC = RC_FAIL;
		} else {
			dbg(DEBUG,"TcpCreateSocket: Socket <%d> created <AF_INET> <SOCK_STREAM>",*pipSocket);
		}/* end of if */
	}/* end of if */

	if(ilRC == RC_SUCCESS)
	{
		errno = 0;
		if (!(prlServiceEntry = getservbyname(pcpService,NULL))) 
		{
			dbg(TRACE,"TcpCreateSocket: getservbyname(%s) <%s>",pcpService,strerror(errno));
			TcpCloseSocket(*pipSocket);
			ilRC = RC_FAIL;
		} else {
			dbg(DEBUG,"TcpCreateSocket: got Service <%s>",pcpService);

			dbg(DEBUG,"TcpCreateSocket: Service name     <%s>",prlServiceEntry->s_name);
			dbg(DEBUG,"TcpCreateSocket: Service port     <%d>",ntohs(prlServiceEntry->s_port));
			dbg(DEBUG,"TcpCreateSocket: Service proto    <%s>",prlServiceEntry->s_proto);
		}/* end of if */
	}/* end of if */

	if(ilRC == RC_SUCCESS)
	{
		errno = 0;
		if (!(prlHostEntry = gethostbyname(pcpHost))) 
		{
			dbg(TRACE,"TcpCreateSocket: gethostbyname(%s) <%s>",pcpHost,strerror(errno));
			TcpCloseSocket(*pipSocket);
			ilRC = RC_FAIL;
		} else {
			dbg(DEBUG,"TcpCreateSocket: got Host <%s>",pcpHost);

			dbg(DEBUG,"TcpCreateSocket: Host name     <%s>",prlHostEntry->h_name);
			/* dbg(DEBUG,"TcpCreateSocket: Host addr     <%s>",inet_ntoa(*prlHostEntry->h_addr)); */

			prlInAddr = (struct in_addr *) &(*prlHostEntry->h_addr);
			dbg(DEBUG,"TcpCreateSocket: Host addr     <%s>",inet_ntoa(*prlInAddr));
		}/* end of if */
	}/* end of if */

	if(ilRC == RC_SUCCESS)
	{
		memset(prpSocketName, 0x00, sizeof(prpSocketName));

		prpSocketName->sin_family = AF_INET;
		prpSocketName->sin_port   = prlServiceEntry->s_port;

		if(ipBindFlag == TRUE)
		{
			prpSocketName->sin_addr.s_addr = INADDR_ANY;
		} else {
			memcpy(&(prpSocketName->sin_addr), prlHostEntry->h_addr, prlHostEntry->h_length);
		}/* end of if */

	}/* end of if */

	if(ilRC == RC_SUCCESS)
	{
		ilKeepalive = 0;
		#if defined(_UNIXWARE) || defined(_SOLARIS) || defined(_LINUX)
			ilRecLen = sizeof(int);
		#endif
		ilRC = getsockopt(*pipSocket, SOL_SOCKET, SO_KEEPALIVE, (char *) &ilKeepalive, &ilRecLen);
		if (ilRC == -1 )
		{	
			ilRC = RC_FAIL;
			dbg(TRACE,"TcpCreateSocket: getsockopt <%s>", strerror(errno));
		}else{
			if(ilKeepalive == 1)
			{
				dbg(DEBUG,"TcpCreateSocket: SO_KEEPALIVE already set");
			}else{
				ilKeepalive = 1;
				ilRC = setsockopt(*pipSocket, SOL_SOCKET, SO_KEEPALIVE, (char *) &ilKeepalive, sizeof(ilKeepalive));
				if (ilRC == -1 )
				{	
					ilRC = RC_FAIL;
					dbg(TRACE,"TcpCreateSocket: setsockopt <%s>", strerror(errno));
				} else {
					dbg(DEBUG,"TcpCreateSocket: SO_KEEPALIVE set");
				}/* end of if */
			}/* end of if */
		}/* end of if */
	}/* end of if */

	if(ilRC == RC_SUCCESS)
	{
		ilReuse = 0;
		#if defined(_UNIXWARE) || defined(_SOLARIS) || defined(_LINUX)
			ilRecLen = sizeof(int);
		#endif
		ilRC = getsockopt(*pipSocket, SOL_SOCKET, SO_REUSEADDR, (char *) &ilReuse, &ilRecLen);
		if (ilRC == -1 )
		{	
			ilRC = RC_FAIL;
			dbg(TRACE,"TcpCreateSocket: getsockopt <%s>", strerror(errno));
		}else{
			if(ilReuse == 1)
			{
				dbg(DEBUG,"TcpCreateSocket: SO_REUSEADDR already set");
			}else{
				ilReuse = 1;
				ilRC = setsockopt(*pipSocket, SOL_SOCKET, SO_REUSEADDR, (char *) &ilReuse, sizeof(ilReuse));
				if (ilRC == -1 )
				{	
					ilRC = RC_FAIL;
					dbg(TRACE,"TcpCreateSocket: setsockopt <%s>", strerror(errno));
				} else {
					dbg(DEBUG,"TcpCreateSocket: SO_REUSEADDR set");
				}/* end of if */
			}/* end of if */
		}/* end of if */
	}/* end of if */

	if (ilRC == RC_SUCCESS)
	{
		rlLinger.l_onoff = 0;
		rlLinger.l_linger = 0;
		ilRC = setsockopt(*pipSocket, SOL_SOCKET, SO_LINGER, (char *) &rlLinger, sizeof(rlLinger));
		if (ilRC == -1 )
		{	
			ilRC = RC_FAIL;
			dbg(TRACE,"TcpCreateSocket: setsockopt <%s>", strerror(errno));
		} else {
			dbg(DEBUG,"TcpCreateSocket: SO_LINGER set");
		}/* end of if */
	}/* end of if */

	if((ilRC == RC_SUCCESS) && (ipBindFlag == TRUE))
	{
		ilRecLen = sizeof(struct sockaddr_in);

		errno = 0;
		ilRC = bind(*pipSocket,(const struct sockaddr *) prpSocketName,ilRecLen);
		if( ilRC < 0 )
		{
			ilRC = RC_FAIL;
			dbg(TRACE,"TcpCreateSocket: bind returned <%d><%d:%s>",ilRC,errno,strerror(errno));
			TcpCloseSocket(*pipSocket);
		}/* end of if */
	}/* end of if */

	return(ilRC);

}/* end of TcpCreateSocket */


/* *************************************************************** */
/*                                                                 */
/*                                                                 */
/*                                                                 */
/* *************************************************************** */
int TcpRecv(int ipSocket, char **pppBuf, int *pipBufLen)
{
	int ilRC = RC_SUCCESS;
	int ilLoop = FALSE;
	int ilRecvLen = 0;
        long llTheCommand = 0;


	/***** Parameter check *****/

	if(ipSocket < 0)
	{
		ilRC = RC_FAIL;
		dbg(TRACE,"TcpRecv: invalid 1st Parameter");
	}/* end of if */

	if(pppBuf == NULL)
	{
		ilRC = RC_FAIL;
		dbg(TRACE,"TcpRecv: invalid 2nd Parameter");
	}/* end of if */

	if(pipBufLen == NULL)
	{
		ilRC = RC_FAIL;
		dbg(TRACE,"TcpRecv: invalid 3rd Parameter");
	}/* end of if */


	/***** functionality *****/

	if(ilRC == RC_SUCCESS)
	{
		do
		{
			ilLoop = FALSE;
			ilRecvLen = TcpRecvBuf(ipSocket,(char *) &InfoHeader, TCP_INFOHEADER_SIZE);
			if(ilRecvLen != TCP_INFOHEADER_SIZE)
			{
				if(ilRecvLen != RC_TIMEOUT)
				{
					dbg(TRACE,"TcpRecv: recv InfoHeader failed <%d> <%d>",ilRecvLen,TCP_INFOHEADER_SIZE);
					ilRC = RC_FAIL;
				}else{
					ilRC = ilRecvLen;
					ilLoop = FALSE;
				}/* end of if */
			}else{
				if(ntohl(InfoHeader.command) == TCP_KEEPALIVE)
				{
					dbg(DEBUG,"TcpRecv: TCP_KEEPALIVE received");
					ilLoop = TRUE;
				}/* end of if */
			}/* end of if */
		}while((ilLoop == TRUE) && (ilRC == RC_SUCCESS));
	}/* end of if */

	if(ilRC == RC_SUCCESS)
	{
		switch(ntohl(InfoHeader.command))
		{
		case TCP_DATA :
			ilRecvLen = ntohl(InfoHeader.length);
			if(ilRecvLen > *pipBufLen)
			{
				errno = 0;
				*pppBuf = realloc(*pppBuf,ilRecvLen);
				if(*pppBuf == NULL)
				{
					ilRC = RC_FAIL;
					dbg(TRACE,"TcpRecv: realloc(%d) failed <%s>",ilRecvLen,strerror(errno));
				}else{
					*pipBufLen = ilRecvLen;
					ilRC = RC_SUCCESS;
					dbg(DEBUG,"TcpRecv: new buffer size <%d>",ilRecvLen);
				}/* end of if */
			}/* end of if */
			if(ilRC == RC_SUCCESS)
			{
				memset(*pppBuf,0x00,ilRecvLen);
				ilRC = TcpRecvBuf(ipSocket,*pppBuf,ilRecvLen);
			}/* end of if */
			break;
		case TCP_SHUTDOWN :
			TcpCloseSocket(ipSocket);
			ilRC = RC_SHUTDOWN;
			dbg(DEBUG,"TcpRecv: TCP_SHUTDOWN received");
			break;
		default :
			ilRC = RC_FAIL;
			dbg(TRACE,"TcpRecv: unknown command");
			break;
		}/* end of switch */
	}/* end of if */

	return(ilRC);

}/* end of TcpRecv*/


/* *************************************************************** */
/* Same as TcpRecv but using a 30Byte FixedLenghHeader (FLH)       */
/* The Header is a string containing 20 Bytes for the command      */
/* and 10 Bytes for the msg-length                                 */
/* *************************************************************** */
int TcpRecvFLH(int ipSocket, char **pppBuf, int *pipBufLen)
{
	int ilRC = RC_SUCCESS;
	int ilLoop = FALSE;
	int ilRecvLen = 0;


	/***** Parameter check *****/

	if(ipSocket < 0)
	{
		ilRC = RC_FAIL;
		dbg(TRACE,"TcpRecvFLH: invalid 1st Parameter");
	}/* end of if */

	if(pppBuf == NULL)
	{
		ilRC = RC_FAIL;
		dbg(TRACE,"TcpRecvFLH: invalid 2nd Parameter");
	}/* end of if */

	if(pipBufLen == NULL)
	{
		ilRC = RC_FAIL;
		dbg(TRACE,"TcpRecvFLH: invalid 3rd Parameter");
	}/* end of if */


	/***** functionality *****/

        /* first receive HEADER_COMMAND_SIZE Byte for the Header-Command */
	if(ilRC == RC_SUCCESS)
	{

			ilLoop = FALSE;
                        memset(cgHeaderCommand, 0x00, HEADER_COMMAND_SIZE);
			ilRecvLen = TcpRecvBuf(ipSocket,(char *) &cgHeaderCommand, HEADER_COMMAND_SIZE );
                        dbg(DEBUG,"Header length <%d>", strlen(cgHeaderCommand));
			str_trm_rgt (&cgHeaderCommand, " ", TRUE);
                        dbg(DEBUG,"TcpRecvFLH: received HeaderCommand <%s>", cgHeaderCommand);
			if(ilRecvLen != HEADER_COMMAND_SIZE)
			{
				if(ilRecvLen != RC_TIMEOUT)
				{
					dbg(TRACE,"TcpRecvFLH: recv InfoHeader-command failed <%d> <%d>",ilRecvLen,HEADER_COMMAND_SIZE);
					ilRC = RC_FAIL;
				}else{
					ilRC = ilRecvLen;
					ilLoop = FALSE;
				}/* end of if */
			}else{
                            if(strcmp(cgHeaderCommand,"TCP_KEEPALIVE") == 0)
				{
					dbg(DEBUG,"TcpRecvFLH: TCP_KEEPALIVE received");
                                        InfoHeader.command = htonl(TCP_KEEPALIVE);
					ilLoop = TRUE;
				}/* end of if */
                            if(strcmp(cgHeaderCommand,"TCP_SHUTDOWN") == 0)
				{
					dbg(DEBUG,"TcpRecvFLH: TCP_SHUTDOWN received");
                                        InfoHeader.command = htonl(TCP_SHUTDOWN);
					ilLoop = TRUE;
				}/* end of if */
                            if(strcmp(cgHeaderCommand,"TCP_DATA") == 0)
				{
					dbg(DEBUG,"TcpRecvFLH: TCP_DATA received");
                                        InfoHeader.command = htonl(TCP_DATA);
					ilLoop = TRUE;
				}/* end of if */
                            if(strcmp(cgHeaderCommand,"TCP_ACKNOWLEDGE") == 0)
				{
					dbg(DEBUG,"TcpRecvFLH: TCP_ACKNOWLEDGE received");
                                        InfoHeader.command = htonl(TCP_ACKNOWLEDGE);
					ilLoop = TRUE;
				}/* end of if */
			}/* end of if */

	}/* end of if */

        /*now receive HEADER_MSG_LENGTH_SIZE Bytes for the HeaderMsgLength */
        if(ilRC == RC_SUCCESS)
	{

			ilLoop = FALSE;
                        memset(cgHeaderMsgLength, 0x00, HEADER_MSG_LENGTH_SIZE);
			ilRecvLen = TcpRecvBuf(ipSocket,(char *) &cgHeaderMsgLength, HEADER_MSG_LENGTH_SIZE);
                        dbg(DEBUG,"Headerlength-length <%d>", strlen(cgHeaderMsgLength));
			str_trm_rgt (&cgHeaderMsgLength, " ", TRUE);
                        dbg(DEBUG,"TcpRecvFLH: received HeaderLength String: <%s>", cgHeaderMsgLength);
                        dbg(DEBUG,"TcpRecvFLH: received HeaderLength int:    <%d>", atol(cgHeaderMsgLength));
                        dbg(DEBUG,"ilRecvLen: <%d>", ilRecvLen);
			if(ilRecvLen != HEADER_MSG_LENGTH_SIZE)
			{
				if(ilRecvLen != RC_TIMEOUT)
				{
					dbg(TRACE,"TcpRecvFLH: recv InfoHeader messageLength failed <%d> <%d>",ilRecvLen,HEADER_MSG_LENGTH_SIZE);
					ilRC = RC_FAIL;
				}else{
					ilRC = ilRecvLen;
					ilLoop = FALSE;
				}/* end of if */
			}else{

				lgMsgLength = atol(cgHeaderMsgLength);
                                InfoHeader.length  = htonl(lgMsgLength);
			}/* end of if */

	}/* end of if */

	if(ilRC == RC_SUCCESS)
	{
		switch(ntohl(InfoHeader.command))
		{
		case TCP_DATA :
			ilRecvLen = ntohl(InfoHeader.length);
			if(ilRecvLen > *pipBufLen)
			{
				errno = 0;
				*pppBuf = realloc(*pppBuf,ilRecvLen);
				if(*pppBuf == NULL)
				{
					ilRC = RC_FAIL;
					dbg(TRACE,"TcpRecv: realloc(%d) failed <%s>",ilRecvLen,strerror(errno));
				}else{
					*pipBufLen = ilRecvLen;
					ilRC = RC_SUCCESS;
					dbg(DEBUG,"TcpRecv: new buffer size <%d>",ilRecvLen);
				}/* end of if */
			}/* end of if */
			if(ilRC == RC_SUCCESS)
			{
				memset(*pppBuf,0x00,ilRecvLen+1);
				ilRC = TcpRecvBuf(ipSocket,*pppBuf,ilRecvLen+1);

			}/* end of if */
			break;
		case TCP_SHUTDOWN :
			TcpCloseSocket(ipSocket);
			ilRC = RC_SHUTDOWN;
			dbg(DEBUG,"TcpRecv: TCP_SHUTDOWN received");
			break;
		default :
			ilRC = RC_FAIL;
			dbg(TRACE,"TcpRecv: unknown command");
			break;
		}/* end of switch */
	}/* end of if */

	return(ilRC);

}/* end of TcpRecvFLH*/

/* *************************************************************** */
/*                                                                 */
/*                                                                 */
/*                                                                 */
/* *************************************************************** */
static int TcpRecvBuf(int ipSocket, char *pcpBuf, int ipBufLen)
{
	int ilRC = RC_SUCCESS;
	int ilRecvLen = 0;
	int ilRemainder = 0;
	int ilFlag = 0;
	char *pclBufOfs = NULL;
	

	/***** Parameter check *****/

	/* FLW: internal static functions always get correct parameters !!! */



	/***** functionality *****/
        dbg(DEBUG,"tcpRecvBuf: byte to be read: <%d>", ipBufLen);
        dbg(DEBUG,"bufferlength: <%d>", strlen(pcpBuf));
        dbg(DEBUG,"buffer:<%s>", pcpBuf);
	if(ilRC == RC_SUCCESS)
	{
		pclBufOfs   = pcpBuf;
		ilRemainder = ipBufLen;
	
		while(ilRemainder != 0)
		{
                    dbg(DEBUG,"ilRecvLen <%d>, ilRemainder <%d>", ilRecvLen, ilRemainder);
			errno = 0;
			ilRecvLen = read(ipSocket,pclBufOfs,ilRemainder);
			/* ilRecvLen = recv(ipSocket,pclBufOfs,ilRemainder,ilFlag); */
			if(ilRecvLen > 0)
			{
				if(ilRecvLen == ilRemainder)
				{
                                    dbg(DEBUG,"dbg1");
					ilRC         = ipBufLen;
					ilRemainder  = 0;
                                        /*pcpBuf[ipBufLen] = '\0';*/
				}else{
                                    dbg(DEBUG,"bytes received so far <%d> content: <%s>", ilRecvLen, pcpBuf);
					ilRemainder -= ilRecvLen;
					pclBufOfs   += ilRecvLen;
				}/* end of if */
			}else{
				switch(errno)
				{
				case EINTR :
					dbg(DEBUG,"TcpRecvBuf: errno EINTR <%d> ilRecvLen <%d>",errno,ilRecvLen);
					if (ilRemainder == TCP_INFOHEADER_SIZE)
					{
						ilRC = RC_TIMEOUT;
						ilRemainder = 0;
					}/* end of if */
					break;

				case 0 :
					dbg(DEBUG,"TcpRecvBuf: errno <%d> ilRecvLen <%d>",errno,ilRecvLen);
					TcpCloseSocket(ipSocket);
					ilRC = RC_FAIL;
					ilRemainder = 0;
					break;

				default :
					dbg(TRACE,"TcpRecvBuf: <%d> bytes <%s>",ilRecvLen,strerror(errno));
					TcpCloseSocket(ipSocket);
					ilRC = RC_FAIL;
					ilRemainder = 0;
					break;
				}/* end of switch */
			}/* end of if */
		}/* end of while */
	}/* end of if */

	return(ilRC);

}/* end of TcpRecvBuf */


/* *************************************************************** */
/*                                                                 */
/*                                                                 */
/*                                                                 */
/* *************************************************************** */
int TcpSend(int ipSocket, char *pcpBuf, int ipBufLen)
{
	int ilRC = RC_SUCCESS;
	int ilSendLen = 0;


	/***** Parameter check *****/

	if(ipSocket < 1)
	{
		ilRC = RC_FAIL;
		dbg(TRACE,"TcpSend: invalid 1st Parameter");
	}/* end of if */

	if(pcpBuf == NULL)
	{
		ilRC = RC_FAIL;
		dbg(TRACE,"TcpSend: invalid 2nd Parameter");
	}/* end of if */

	if(ipBufLen < 1)
	{
		ilRC = RC_FAIL;
		dbg(TRACE,"TcpSend: invalid 3rd Parameter");
	}/* end of if */


	/***** functionality *****/

	if(ilRC == RC_SUCCESS)
	{
		InfoHeader.command = htonl(TCP_DATA);
		InfoHeader.length  = htonl(ipBufLen);

		ilSendLen = TcpSendBuf(ipSocket,(char *) &InfoHeader,sizeof(TCP_INFOHEADER));
		if(ilSendLen != sizeof(TCP_INFOHEADER))
		{
			ilRC = RC_FAIL;
		}/* end of if */
	}/* end of if */

	if(ilRC == RC_SUCCESS)
	{
		ilRC = TcpSendBuf(ipSocket,pcpBuf,ipBufLen);
	}/* end of if */

	return(ilRC);

}/* end of TcpSend */


/* *************************************************************** */
/*                                                                 */
/*                                                                 */
/*                                                                 */
/* *************************************************************** */
static int TcpSendBuf(int ipSocket, char *pcpBuf, int ipBufLen)
{
	int ilRC = RC_SUCCESS;
	int ilSendLen = 0;
	int ilRemainder = 0;
	int ilFlag = 0;
	char *pclBufOfs = NULL;


	/***** Parameter check *****/

	/* FLW: internal static functions always get correct parameters !!! */



	/***** functionality *****/

	pclBufOfs   = pcpBuf;
	ilRemainder = ipBufLen;

	while(ilRemainder != 0)
	{
		errno = 0;
		ilSendLen = write(ipSocket,pclBufOfs,ilRemainder);
		/* ilSendLen = send(ipSocket,pclBufOfs,ilRemainder,ilFlag); */
		if(ilSendLen > 0)
		{
			if(ilSendLen == ilRemainder)
			{
				ilRC         = ipBufLen;
				ilRemainder  = 0;
			}else{
				ilRemainder -= ilSendLen;
				pclBufOfs   += ilSendLen;
			}/* end of if */
		}else{
			switch(errno)
			{
			case EINTR :
				dbg(DEBUG,"TcpSendBuf: errno EINTR <%d> ilRecvLen <%d>",errno,ilSendLen);
				break;

			case 0 :
				dbg(DEBUG,"TcpSendBuf: errno <%d> ilRecvLen <%d>",errno,ilSendLen);
				TcpCloseSocket(ipSocket);
				ilRC = RC_FAIL;
				ilRemainder = 0;
				break;

			default :
				dbg(TRACE,"TcpSendBuf: <%d> bytes <%s>",ilSendLen,strerror(errno));
				TcpCloseSocket(ipSocket);
				ilRC = RC_FAIL;
				ilRemainder = 0;
				break;
			}/* end of switch */
		}/* end of if */
	}/* end of while */

	return(ilRC);

}/* end of TcpSendBuf */


/* *************************************************************** */
/*                                                                 */
/*                                                                 */
/*                                                                 */
/* *************************************************************** */
int TcpSendKeepalive(int ipSocket)
{
	int	ilRC = RC_SUCCESS;
	int ilSendLen = 0;

	/***** Parameter check *****/

	if(ipSocket < 1)
	{
		ilRC = RC_FAIL;
		dbg(TRACE,"TcpSendKeepalive: invalid 1st Parameter");
	}/* end of if */

	/***** functionality *****/

	if(ilRC == RC_SUCCESS)
	{
		InfoHeader.command = htonl(TCP_KEEPALIVE);
		InfoHeader.length  = htonl(0);

		ilSendLen = TcpSendBuf(ipSocket,(char *) &InfoHeader,sizeof(TCP_INFOHEADER));
		if(ilSendLen != sizeof(TCP_INFOHEADER))
		{
			ilRC = RC_FAIL;
		}/* end of if */
	}/* end of if */

	return(ilRC);

}/* end of TcpSendKeepalive */


/* *************************************************************** */
/*                                                                 */
/*                                                                 */
/*                                                                 */
/* *************************************************************** */
int TcpSendAck(int ipSocket)
{
	int	ilRC = RC_SUCCESS;
	int ilSendLen = 0;

	/***** Parameter check *****/

	if(ipSocket < 1)
	{
		ilRC = RC_FAIL;
		dbg(TRACE,"TcpSendAck: invalid 1st Parameter");
	}/* end of if */

	return(ilRC);

	/***** functionality *****/

	if(ilRC == RC_SUCCESS)
	{
		InfoHeader.command = htonl(TCP_ACKNOWLEDGE);
		InfoHeader.length  = htonl(0);

		ilSendLen = TcpSendBuf(ipSocket,(char *) &InfoHeader,sizeof(TCP_INFOHEADER));
		if(ilSendLen != sizeof(TCP_INFOHEADER))
		{
			ilRC = RC_FAIL;
		}/* end of if */
	}/* end of if */

	return(ilRC);

}/* end of TcpSendAck */

/* *************************************************************** */
/*                                                                 */
/*                                                                 */
/*                                                                 */
/* *************************************************************** */
int TcpRecvAck(int ipSocket, int ipTimeout)
{
	int	ilRC = RC_SUCCESS;
	int ilRecvLen = 0;
	fd_set rlReadFileDescriptor;
	struct timeval rlTimeout;

	/***** Parameter check *****/

	if(ipSocket < 1)
	{
		ilRC = RC_FAIL;
		dbg(TRACE,"TcpRecvAck: invalid 1st Parameter");
	}/* end of if */

	if(ipTimeout < 0)
	{
		ilRC = RC_FAIL;
		dbg(TRACE,"TcpRecvAck: invalid 2nd Parameter");
	}/* end of if */

	return(ilRC);

	/***** functionality *****/

    FD_ZERO(&rlReadFileDescriptor);
    FD_SET(ipSocket, &rlReadFileDescriptor);

    rlTimeout.tv_sec = ipTimeout;
    rlTimeout.tv_usec = 0;

	errno = 0;

	ilRC = select((ipSocket+1), &rlReadFileDescriptor, NULL, NULL, &rlTimeout);
	switch(ilRC)
	{
	case -1 :
		dbg(TRACE,"TcpRecvAck: select failed  errno<%d:%s>",errno,strerror(errno));
		ilRC = RC_FAIL;
		break;

	case 0 :
		dbg(DEBUG,"TcpRecvAck: timeout  select returned 0");
		ilRC = RC_TIMEOUT;
		break;

	default :
		ilRecvLen = TcpRecvBuf(ipSocket,(char *) &InfoHeader, TCP_INFOHEADER_SIZE);
		if(ilRecvLen != TCP_INFOHEADER_SIZE)
		{
			dbg(TRACE,"TcpRecvAck: recv InfoHeader failed <%d> <%d>",ilRecvLen,TCP_INFOHEADER_SIZE);
			ilRC = RC_FAIL;
		}else{
			if(ntohl(InfoHeader.command) == TCP_ACKNOWLEDGE)
			{
				ilRC = RC_SUCCESS;
			}else{
				dbg(TRACE,"TcpRecvAck: unexpected data received");
				ilRC = RC_FAIL;
			}/* end of if */
		}/* end of if */
		break;
	}/* end of switch */
 
	return(ilRC);

}/* end of TcpReadAck */


/* *************************************************************** */
/*                                                                 */
/*                                                                 */
/*                                                                 */
/* *************************************************************** */
int TcpClose(int ipSocket)
{
	int ilRC = RC_SUCCESS;
	int ilSendLen = 0;


	/***** Parameter check *****/

	if(ipSocket < 0)
	{
		ilRC = RC_FAIL;
		dbg(TRACE,"TcpClose: invalid 1st Parameter");
	}/* end of if */


	/***** functionality *****/

	if(ilRC == RC_SUCCESS)
	{
		if(ipSocket > 0)
		{
			InfoHeader.command = htonl(TCP_SHUTDOWN);
			InfoHeader.length  = htonl(0); /* haha */
	
			TcpSendBuf(ipSocket,(char *) &InfoHeader,sizeof(TCP_INFOHEADER));

			TcpCloseSocket(ipSocket);
		}/* end of if */

		if(igAcceptSocket > 0)
		{
			TcpCloseSocket(igAcceptSocket);
		}/* end of if */
	}/* end of if */

	return(ilRC);

}/* end of TcpClose */


/* *************************************************************** */
/*                                                                 */
/*                                                                 */
/*                                                                 */
/* *************************************************************** */
static int TcpCloseSocket(int ipSocket)
{
	int ilRC = RC_SUCCESS;


	/***** Parameter check *****/

	/* FLW: internal static functions always get correct parameters !!! */



	/***** functionality *****/

	if(ilRC == RC_SUCCESS)
	{
		errno = 0;
		close(ipSocket);
		switch(errno)
		{
		case 0 :
			break;
		default :
			/*dbg(TRACE,"TcpCloseSocket: close <%s>",strerror(errno));*/
			break;
		}/* end of switch */

		errno = 0;
		shutdown(ipSocket,2);
		switch(errno)
		{
		case 0 :
			break;
		default :
			/*dbg(TRACE,"TcpCloseSocket: shutdown <%s>",strerror(errno));*/
			break;
		}/* end of switch */

	}/* end of if */

	return(ilRC);

}/* end of TcpCloseSocket */

