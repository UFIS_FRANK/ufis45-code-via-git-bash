extern int GetDayOfWeek(const char* pcpDate);
extern char *strupr(char *s);

/* Get the day of week out of a date string (0=Mon to 6=Sun, <0 -> error)*/
int GetDayOfWeek(const char* pcpDate)
{
	char    clActStr[36] ;
	int     ilCmpChr;
	int     ilyear, ilmonth, ilday ;   
	long    Julians19DayIs ;  
	int     alMonthDayArr [] = { 0,31,59,90,120,151,181,212,243,273,304,334 } ;  

	/* min. length has to be 8 (JJJJMMDD) */
	if (strlen(pcpDate) < 8) return -1;
	/* copy date */
	strncpy (clActStr, pcpDate, 8 ) ; /* only date */ 

	ilday = atoi (&clActStr[6]) ;
	clActStr[6] = '\0' ;    
	ilmonth = atoi (&clActStr[4])  ; 
	clActStr[4] = '\0' ; 
	ilyear = atoi(clActStr) - 1900 ;
	if ((ilmonth > 12) || (ilmonth < 1)) 
	{
		return (-2) ;
	} 
	if ((ilday < 1) || (ilday > 31)) 
	{
		return (-3) ;
	} 

	Julians19DayIs = (365 * ilyear ) + (ilyear >> 2) ; 
	Julians19DayIs += alMonthDayArr[ilmonth-1] + ilday ; 

	if ((ilyear % 4) == 0) 
	{
		if ( ilmonth < 3 ) Julians19DayIs-- ;
	}
	ilCmpChr = (Julians19DayIs % 7) - 1 ;

	if (ilCmpChr < 0) ilCmpChr = 6 ;       /* shift sunday */

	/* 0 = MONDAY ... 6 = SUNDAY */
	return (ilCmpChr);
}


/*-----------------------------------------------------------------------------
 * Converts string to uppercase
 *---------------------------------------------------------------------------
**/
char *strupr(char *s)
{
char *p;

  p=s;
  while(*p)
  {
    *p=(char)toupper(*p); 
    p++;
  }
  return (s);
}


/*	original code starts here	*/
static char*	rtrim(char *pcpString)
{
	int ilCount;

	for (ilCount = strlen(pcpString) - 1; ilCount >= 0; ilCount--)
	{
		if (pcpString[ilCount] == ' ')
			pcpString[ilCount] = '\0';
	}

	return pcpString;
}

static char*	ltrim(char *pcpString)
{
	int ilCount;
	int ilLen = strlen(pcpString);
	for (ilCount = 0; ilCount < ilLen; ilCount++)
	{
		if (pcpString[ilCount] != ' ')
		{
			if (ilCount > 0)
			{
				strcpy(pcpString,&pcpString[ilCount]);
			}

			break;
		}
	}

	return pcpString;
}

static char* trim(char *pcpString)
{
	ltrim(pcpString);
	return rtrim(pcpString);
}

/******************************************************/
/* delivers the value of a parameter stored in PARTAB */
/******************************************************/
BOOL GetPartabValue(const char *pcpPAID,char *pcpValue)
{
	static BOOL blFirst = TRUE;
	static int  ilParHandle=-1;
	static const char *pcsParFieldList = "PAID,VALU"; 
	char clRecord[1024];

	if (blFirst)
	{
		blFirst = FALSE;
		ilParHandle = GetArrayIndex("PARTAB");
	}

	if (ilParHandle < 0)
	{
		return FALSE;
	}

	strcpy(clRecord,pcpPAID);
	if (FindFirstRecordByIndex(ilParHandle,clRecord) > 0)
	{
		if (GetFieldValueFromMonoBlockedDataString("VALU",(char *)pcsParFieldList,clRecord,pcpValue) > 0)
		{
			return TRUE;
		}
	}

	return FALSE;
}

/************************************/
/* checks if a URNO exits in BSDTAB */
/************************************/
static BOOL IsBsd(const char *pcpBSDU)
{
	static BOOL blFirst = TRUE;
	static int ilBsdHandle;
	char clFieldValue[100];

	/* get handle of BSDTAB-array */
	if (blFirst)
	{
		blFirst = FALSE;
		ilBsdHandle = GetArrayIndex("BSDTAB");
	}
	if (ilBsdHandle < 0)
	{
		dbg(TRACE,"IsBsd(): ERROR - handle of Recordsets 'BSDTAB' invalid (value: %d)!",ilBsdHandle);
		return FALSE;
	}

	dbg(DEBUG,"IsBsd(): Check Urno <%s>",pcpBSDU);
	strcpy(clFieldValue,"URNO");
	return (FindFieldValueByIndex(ilBsdHandle,(char *)pcpBSDU,clFieldValue) > 0 && strcmp(trim(clFieldValue),pcpBSDU) == 0);
}

/* **************************************************************************/
/* IsCodeRegFree(pUrno)														*/
/* Rueckgabe: 0 -> Kein regulaer arbeitsfrei								*/
/* 		  1 -> regulaer arbeitsfrei											*/
/* **************************************************************************/
/* **************************************************************************/
static BOOL IsCodeRegFree(const char *pcpUrno)
{
	static BOOL blFirst = TRUE;
	static int ilOdaHandle;
	char clFieldValue[100];

	/* get handle of ODATAB-array */
	if (blFirst)
	{
		blFirst = FALSE;
		ilOdaHandle = GetArrayIndex("ODATAB");
	}
	if (ilOdaHandle < 0)
	{
		dbg(TRACE,"IsCodeRegFree(): ERROR - handle of Recordsets 'ODATAB' invalid (value: %d)!",ilOdaHandle);
		return FALSE;
	}

	strcpy(clFieldValue,"FREE");
	if (FindFieldValueByIndex(ilOdaHandle,(char *)pcpUrno,clFieldValue) > 0)
	{
		dbg(DEBUG,"IsRegFree Urno:   <%s>",pcpUrno);
		dbg(DEBUG,"IsRegFree Result: <%s>",clFieldValue);

		if (strcmp(clFieldValue,"x") == 0)
		{
			return TRUE;
		}
	}

	return FALSE;
}

/********************************************************/
/* GetODATypes(...)	- delivers all types of the absence */
/* return: 0 -> Keine Abwesenheit						*/
/* 		  	  1 -> Abwesenheit							*/
/********************************************************/
static BOOL GetODATypes(const char *pcpUrno,BOOL *pbpIsRegFree,BOOL *pbpIsShiftChangeoverDay,BOOL *pbpIsTraining,BOOL *pbpIsCompensation,BOOL *pbpIsCompensationNight,BOOL *pbpIsHoliday,BOOL *pbpIsIllness)
{
	static BOOL blFirst = TRUE;
	static int ilOdaHandle;
	char *pclOdaFieldList;
	int ilLen;
	char clOdaRecord[100];
	char clFieldValue[100];

	/* get handle of ODATAB-array */
	if (blFirst)
	{
		blFirst = FALSE;
		ilOdaHandle = GetArrayIndex("ODATAB");
	}

	if (ilOdaHandle < 0)
	{
		dbg(TRACE,"GetODATypes(): ERROR - handle of Recordsets 'ODATAB' invalid (value: %d)!",ilOdaHandle);
		return FALSE;
	}
	pclOdaFieldList = sgRecordsets[ilOdaHandle].cFieldList; /*"URNO,FREE,TBSD,SDAC,WORK,SDAS,TYPE,UPLN"*/

	/* get first record that matches condition */
	strcpy(clOdaRecord,pcpUrno);
	ilLen = FindFirstRecordByIndex(ilOdaHandle,clOdaRecord);
	if (ilLen > 0)
	{
		*pbpIsRegFree = FALSE;
		*pbpIsShiftChangeoverDay = FALSE;
		*pbpIsTraining = FALSE;
		*pbpIsCompensation = FALSE;
		*pbpIsCompensationNight = FALSE;
		*pbpIsHoliday = FALSE;
		*pbpIsIllness = FALSE;

		/* get regular free	(ODA.FREE) */
		if (GetFieldValueFromDataString("FREE",pclOdaFieldList,clOdaRecord,clFieldValue) > 0)
		{
			rtrim(clFieldValue);
			if (strcmp(clFieldValue,"x") == 0)
				*pbpIsRegFree = TRUE;
		}

		/* get type	(ODA.TYPE) */
		if (GetFieldValueFromDataString("TYPE",pclOdaFieldList,clOdaRecord,clFieldValue) > 0)
		{
			rtrim(clFieldValue);
			dbg(DEBUG,"GetODATypes Type		: <%s>",clFieldValue);

			if (strcmp(clFieldValue,"T") == 0)
				*pbpIsTraining = TRUE;
			else if (strcmp(clFieldValue,"S") == 0)
				*pbpIsShiftChangeoverDay = TRUE;
			else if (strcmp(clFieldValue,"C") == 0)
				*pbpIsCompensation = TRUE;
			else if (strcmp(clFieldValue,"N") == 0)
				*pbpIsCompensationNight = TRUE;
			else if (strcmp(clFieldValue,"I") == 0)
				*pbpIsIllness = TRUE;
		}

		/* get holiday (ODA.UPLN) */
		if (GetFieldValueFromDataString("UPLN",pclOdaFieldList,clOdaRecord,clFieldValue) > 0)
		{
			rtrim(clFieldValue);
			if (strcmp(clFieldValue,"Y") == 0)
			{
				*pbpIsHoliday = TRUE;
			}
		}

		dbg(DEBUG,"GetODATypes Urno	   				: <%s>",pcpUrno);
		dbg(DEBUG,"GetODATypes Reg Free				: <%d>",*pbpIsRegFree);
		dbg(DEBUG,"GetODATypes Training				: <%d>",*pbpIsTraining);
		dbg(DEBUG,"GetODATypes Changeover day 		: <%d>",*pbpIsShiftChangeoverDay);
		dbg(DEBUG,"GetODATypes Compensation			: <%d>",*pbpIsCompensation);
		dbg(DEBUG,"GetODATypes CompensationNight	: <%d>",*pbpIsCompensationNight);
		dbg(DEBUG,"GetODATypes Holiday				: <%d>",*pbpIsHoliday);
		dbg(DEBUG,"GetODATypes Illness				: <%d>",*pbpIsIllness);
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}

/* **************************************************************************/
/* **********************************************************************/
/* OpenACCRecordset: liest die Datensaetze der Tabelle ACCTAB in einen	*/
/* Action Recordset, der ge�ffnet bleibt.				*/
/* R�ckgabe: 0 oder Fehlercode						*/
/* **********************************************************************/
static int OpenACCRecordset(const char *pcpStfu,const char *pcpYear)
{
	static const char* pcsAccFields = "YELA,YECU,PENO,FUMO,LSTU,CDAT,USEC,USEU,URNO,YEAR,STFU,TYPE,HOPO,OP01,OP02,OP03,OP04,OP05,OP06,OP07,OP08,OP09,OP10,OP11,OP12,CO01,CO02,CO03,CO04,CO05,CO06,CO07,CO08,CO09,CO10,CO11,CO12,CL01,CL02,CL03,CL04,CL05,CL06,CL07,CL08,CL09,CL10,CL11,CL12";
	static const char* pcsAccKeyFields = "YEAR,STFU,TYPE";

	int  ilAccHandle;
	char clAccWhere[10*1024];
	char clNextYear[20];

	if (pcpStfu == NULL || pcpYear == NULL)
	{
		return -1;
	}

	sprintf(clNextYear,"%d",atoi(pcpYear)+1);
	sprintf(clAccWhere,"WHERE STFU='%s' AND (YEAR='%s' OR YEAR='%s')",pcpStfu,pcpYear,clNextYear);

	dbg(DEBUG,"OpenACCRecordset(): Opening ACCHandle.");
	dbg(DEBUG,"OpenACCRecordset(): Where statement <clAccWhere> = <%s>!",clAccWhere);

	ilAccHandle = OpenRecordset("ACCHDL","ACCTAB",clAccWhere,pcsAccFields,pcsAccKeyFields,0,0);

	if (ilAccHandle < 0)
	{
		dbg(TRACE,"OpenACCRecordset(): Error opening action array <%s> containing <%s>!!!","ACCHDL","ACCTAB");
		return ilAccHandle;
	}
	else
	{
		dbg(DEBUG,"OpenACCRecordset(): Succeeded in opening array <%s>.","ACCTAB");
		return 0;
	}
}


/* **************************************************************************/
/* **************************************************************************/
/* OpenACCRecordsetEx: liest die Datensaetze der Tabelle ACCTAB in einen	*/
/* Prefetch Recordset, der ge�ffnet bleibt.									*/
/* R�ckgabe: ACCTAB record handle oder Fehlercode							*/
/* **************************************************************************/
static int OpenACCRecordsetEx(const char *pcpAccWhere)
{
	static const char* pcsAccFields = "YELA,YECU,PENO,FUMO,LSTU,CDAT,USEC,USEU,URNO,YEAR,STFU,TYPE,HOPO,OP01,OP02,OP03,OP04,OP05,OP06,OP07,OP08,OP09,OP10,OP11,OP12,CO01,CO02,CO03,CO04,CO05,CO06,CO07,CO08,CO09,CO10,CO11,CO12,CL01,CL02,CL03,CL04,CL05,CL06,CL07,CL08,CL09,CL10,CL11,CL12,YEDE";
	static const char* pcsAccKeyFields = "YEAR,STFU,TYPE";

	int  ilAccHandle;

	if (pcpAccWhere == NULL)
	{
		return -1;
	}

	dbg(DEBUG,"opening ACCHandle !");


	dbg(DEBUG,"accWhere <%s>!",pcpAccWhere);

	ilAccHandle = OpenRecordset("ACCHDL","ACCTAB",pcpAccWhere,pcsAccFields,pcsAccKeyFields,0,0);

	if (ilAccHandle <= -1)
	{
		dbg(TRACE,"Fehler beim Oeffnen des action array <%s>: <%s>!!!","ACCHDL","ACCTAB");
	}
	else
	{
		/* don't delete array contents automatically	*/
		sgRecordsets[ilAccHandle].iAutoDelete = 0;
	}

	return ilAccHandle;
}

/***************************************************/
/* GetStaffData: Mitarbeiterdaten werden ermittelt */
/* STFHandle mu� ge�ffnet sein                     */
/***************************************************/
typedef	struct
{
	char	cmMaName[42];
	char	cmGeburtstag[10];
	char	cmPersonalnummer[22];
	char	cmAustrittsdatum[16];
	char	cmEintrittsdatum[16];
}	STAFFDATA;

static BOOL GetStaffData(const char *pcpStfu,STAFFDATA *popStaffData)
{
	static BOOL blFirst = TRUE;
	static int ilStfHandle;

	char	clRecord[MAX_DATA_SIZE];
	char	clField[1024];
	char*	pclStfFields;

	/*** Siehe init_scbhdl und init_xbshdl ***/
	popStaffData->cmMaName[0] = '\0';
	popStaffData->cmGeburtstag[0] = '\0';
	popStaffData->cmPersonalnummer[0] = '\0';
	popStaffData->cmAustrittsdatum[0] = '\0';
	popStaffData->cmEintrittsdatum[0] = '\0';

	/****** Handle fuer Mitarbeiter ermitteln	***/
	if (blFirst)
	{
		blFirst = FALSE;
		ilStfHandle = GetArrayIndex("STFTAB");
	}

	if (ilStfHandle < 0)
	{
		dbg(TRACE,"GetStaffData(): ERROR - handle of Recordsets 'STFTAB' invalid (value: %d)!",ilStfHandle);
		return FALSE;
	}

	pclStfFields = sgRecordsets[ilStfHandle].cFieldList;/*"URNO,PDGL,PDKL,PMAK,PMAG,LANM,GEBU,PENO,DODM,DOEM";*/

	/*** Record finden ***/
	strcpy(clRecord,pcpStfu);
	if (FindFirstRecordByIndex(ilStfHandle,clRecord) > 0)
	{
		/*** Record auswerten ***/
		if (GetFieldValueFromMonoBlockedDataString("LANM",pclStfFields,clRecord,clField) > 0)
		{
			strcpy(popStaffData->cmMaName,clField);
		}

		if (GetFieldValueFromMonoBlockedDataString("GEBU",pclStfFields,clRecord,clField) > 0)
		{
			strcpy(popStaffData->cmGeburtstag,clField);
		}

		if (GetFieldValueFromMonoBlockedDataString("PENO",pclStfFields,clRecord,clField) > 0)
		{
			strcpy(popStaffData->cmPersonalnummer,clField);
		}

		if (GetFieldValueFromMonoBlockedDataString("DODM",pclStfFields,clRecord,clField) > 0)
		{
			strcpy(popStaffData->cmAustrittsdatum,clField);
			trim(popStaffData->cmAustrittsdatum);
		}

		if (GetFieldValueFromMonoBlockedDataString("DOEM",pclStfFields,clRecord,clField) > 0)
		{
			strcpy(popStaffData->cmEintrittsdatum,clField);
			trim(popStaffData->cmEintrittsdatum);
		}

		return TRUE;
	}
	else
	{
		dbg(TRACE,"GetStaffData(): ERROR - Employee record of employee with URNO <%s> not found!",pcpStfu);
		return FALSE;
	}
}

/* **********************************************************************/
/* **********************************************************************/
/* APO 28.05.2001:							*/
/* GetScoData:								*/
/* Read contract data of employee, new version. changes:		*/
/*  1.) use open array, filled when handler starts up			*/
/*  2.) use index and loop over array					*/
/* Return: 1 success, 0 error						*/
/* **********************************************************************/
/* **********************************************************************/
typedef struct
{
	double	dmVereinbarteWochenstunden;
	char	cmScoCode[6];
}	SCODATA;

static BOOL GetScoData(const char *pcpStfu,const char *pcpSday,SCODATA *popScoData)
{
	char *pclScoFieldList;
	static BOOL blFirst = TRUE;
	static int ilScoHandleLocal;

	char	clScoDate[16];
	char	clScoRecord[510];
	char	clScoField[40];
	char	clVpfr[16];
	char	clVpto[16];
	char	clWorkDaysPerWeek[66];
	int	ilLen;

	/* initialize and then get handle of array */
	if (blFirst)
	{
		blFirst = FALSE;
		ilScoHandleLocal = GetArrayIndex("SCOTAB");
	}
	if (ilScoHandleLocal < 0)
	{
		dbg(TRACE,"GetScoData(): ERROR - handle of Recordsets 'SCOTAB' not found (value: %d)!",ilScoHandleLocal);
		return FALSE;
	}
	popScoData->dmVereinbarteWochenstunden = 0;
	popScoData->cmScoCode[0] = '\0';

	/* get the fieldlist */
	pclScoFieldList = sgRecordsets[ilScoHandleLocal].cFieldList;

	/* ASSERTS */
	if (pcpStfu == NULL || pcpSday == NULL || popScoData == NULL)
	{
		dbg(TRACE,"GetScoData(): ASSERT failed - wrong parameter!");
		return FALSE;
	}

	if (!GetPartabValue("ID_NUM_WOR_DAYS",clWorkDaysPerWeek))
	{
		/* default is 5 days per week if nothing is set	*/
		strcpy(clWorkDaysPerWeek,"5");
	}

	/* change date format to the same which is used in examined fields of table */
	strcpy(clScoDate,pcpSday);
	rtrim(clScoDate);
	strcat(clScoDate,"000000");

	/* get first record that matches condition */
	strcpy(clScoRecord,pcpStfu);
	ilLen = FindFirstRecordByIndex(ilScoHandleLocal,clScoRecord);

	while (ilLen > 0)
	{
		/* get check data */
		if (GetFieldValueFromMonoBlockedDataString("VPFR",pclScoFieldList,clScoRecord,clVpfr) > 0 && strcmp(clVpfr,clScoDate) <= 0)
		{
			if (GetFieldValueFromMonoBlockedDataString("VPTO",pclScoFieldList,clScoRecord,clVpto) <= 0 || strlen(rtrim(clVpto)) == 0 || strcmp(clVpto,clScoDate) >= 0)
			{
				dbg(DEBUG,"GetScoData Vpfr=%s,Vpto=%s",clVpfr,clVpto);

				/* print urno for checking result of new function */
				if (GetFieldValueFromMonoBlockedDataString("CODE",pclScoFieldList,clScoRecord,clScoField) > 0)
				{
					strcpy(popScoData->cmScoCode,trim(clScoField));

					dbg(TRACE,"GetScoData(): using CODE <%s>",popScoData->cmScoCode);

					if (GetFieldValueFromMonoBlockedDataString("CWEH",pclScoFieldList,clScoRecord,clScoField) > 0)
					{
						char clTmpHour[3];
						char clTmpMin[3];

						trim(clScoField);
						dbg(DEBUG,"GetScoData(): Field <CWEH> = <%s>",clScoField);
						if (strlen(clScoField) > 0)
						{
							char *cp = strchr(clScoField,'.');
							if (cp)
							{
								*cp = '\0';
								strcpy(clTmpHour,clScoField);
								++cp;
								strcpy(clTmpMin,cp);

								popScoData->dmVereinbarteWochenstunden = (((double)atoi(clTmpHour) * 60) + atoi(clTmpMin)) / atoi(clWorkDaysPerWeek);
							}
							else
							{
								dbg(TRACE,"GetScoData():  ERROR - Field <CWEH> not valid, '.' is missing as decimal separator!");
							}
						}
					}
					else
					{
						dbg(TRACE,"GetScoData(): ERROR - Field <CWEH> not found!");
					}

					dbg(DEBUG,"GetScoData(): Work days per week    = <%s>",clWorkDaysPerWeek);
					dbg(DEBUG,"GetScoData(): Daily working minutes = <%lf>",popScoData->dmVereinbarteWochenstunden);
					dbg(DEBUG,"GetScoData(): Contract code         = <%s>",popScoData->cmScoCode);

					return TRUE;
				}
			}
		}

		strcpy(clScoRecord,pcpStfu);
		ilLen = FindNextRecordByIndex(ilScoHandleLocal,clScoRecord);
	}

	/* if we get here, no matching record was found */
	dbg(TRACE,"GetScoData(): WARNING - no contract data found for SURN = <%s> and SDAY = <%s>!",pcpStfu,pcpSday);
	return FALSE;
}

typedef struct
{
	double	dmVereinbarteWochenstunden;
	char	cmINBU[2];
}	COTDATA;

static BOOL GetCotDataByCtrc(const char *pcpCtrc,COTDATA *popCotData)
{
	static const char *pcsCotFieldList = "CTRC,WHPW,INBU,URNO";
	int ilCotHandle;
	int ilFoundRow;
	char clCotRecord[50];
	char clCotField[50];
	char clWorkDaysPerWeek[128];	
	
	popCotData->dmVereinbarteWochenstunden = 0;
	popCotData->cmINBU[0] = '\0';

	if (!GetPartabValue("ID_NUM_WOR_DAYS",clWorkDaysPerWeek))
	{
		/* default is 5 days per week if nothing is set	*/
		strcpy(clWorkDaysPerWeek,"5");
	}

	/* initialize and then get handle of array */
	ilCotHandle = GetArrayIndex("COTTAB");

	if (ilCotHandle < 0)
	{
		dbg(TRACE,"ERROR: Handle of the recordsets 'COTTAB' could not be detected (value: %d)!",ilCotHandle);
		return FALSE;
	}

	strcpy(clCotRecord,pcpCtrc);
	ilFoundRow = FindFirstRecordByIndex(ilCotHandle,clCotRecord);

	if (ilFoundRow > 0)
	{
		/* there can be only one record! */
		if (GetFieldValueFromMonoBlockedDataString("INBU",(char *)pcsCotFieldList,clCotRecord,clCotField) > 0)
		{
			strcpy(popCotData->cmINBU,trim(clCotField));
		}

		if (GetFieldValueFromMonoBlockedDataString("WHPW",(char *)pcsCotFieldList,clCotRecord,clCotField) > 0)
		{
			char clTmpHour[3];
			char clTmpMin[3];

			ltrim(clCotField);
			clTmpHour[0] = clCotField[0];
			clTmpHour[1] = clCotField[1];
			clTmpHour[2] = '\0';

			clTmpMin[0] = clCotField[2];
			clTmpMin[1] = clCotField[3];
			clTmpMin[2] = '\0';

			popCotData->dmVereinbarteWochenstunden = (((double)atoi(clTmpHour) * 60) + atoi(clTmpMin)) /  atoi(clWorkDaysPerWeek);
		}

		return TRUE;
	}

	return FALSE;
}

/********************************************************************************/
/* GetOdaWorkData(varBsdu,varWork)						*/
/* Search for record with URNO=<varBsdu> in ODATAB and store value of field	*/ 
/* WORK in <varWork> if record is found. New function, reads from static	*/
/* recordset opened in init-script and search record by index.			*/
/* Return: 	0 -> failure							*/
/* 		1 -> success							*/
/********************************************************************************/
static BOOL	GetOdaWorkData(const char *pcpBsdu,char *pcpWork)
{
	static BOOL blFirst = TRUE;
	static int ilOdaHandle;

	char clFieldValue[1024];

	/****** Handle fuer Abwesenheiten ermitteln	*****/
	if (blFirst)
	{
		blFirst = FALSE;
		ilOdaHandle = GetArrayIndex("ODATAB");
	}

	if (ilOdaHandle < 0)
	{
		dbg(TRACE,"Fehler: Handle des Recordsets 'ODATAB' konnte nicht ermittelt werden (Wert: %d)!!!",ilOdaHandle);
		return FALSE;
	}

	/*** initialize results with failure values	****/
	pcpWork[0] = '\0';

	/*** search record with URNO=<varBsdu>	****/
	strcpy(clFieldValue,"WORK");
	if (FindFieldValueByIndex(ilOdaHandle,(char *)pcpBsdu,clFieldValue) > 0)
	{
		strcpy(pcpWork,clFieldValue);
		dbg(TRACE,"GetOdaWorkData:varWork = <%s>",pcpWork);
		return TRUE;
	}

	return FALSE;
}

/************************************************************************************************************/
/************************************************************************************************************/
/*	Ermittelt fuer einen Mitarbeiter <varStaffUrno> fuer den Monat in <varMonth> den						*/
/*	Kontowert fuer das Konto <varAccountType>.																*/
/************************************************************************************************************/
/************************************************************************************************************/

BOOL GetAccValueHandle(const char *pcpAccountType,const char *pcpLocalMonth,const char *pcpLocalYear,const char *pcpStaffUrno,const char *pcpField,char *pcpResult)
{
	int		ilAccHandleLocal;
	char	clAccKeyValue[256];

	/*** initialize data ****/
	strcpy(clAccKeyValue,pcpLocalYear);
	strcat(clAccKeyValue,",");
	strcat(clAccKeyValue,pcpStaffUrno);
	strcat(clAccKeyValue,",");
	strcat(clAccKeyValue,pcpAccountType);

	dbg(TRACE,"GetAccValueHandle: varAccKeyValue <%s>",clAccKeyValue);

	/*** get handle of array	*/
	ilAccHandleLocal = GetArrayIndex("ACCHDL");
	if (ilAccHandleLocal >= 0)
	{
		/***** get field values from first record that matches condition	****/

		strcpy(pcpResult,pcpField);
		strcat(pcpResult,pcpLocalMonth);
		if (FindFieldValueByIndex(ilAccHandleLocal,clAccKeyValue,pcpResult) <= 0)
		{
			/*** avoid undef values ***/
			strcpy(pcpResult,"0");
		}
	}
	else
	{
		/*** array not found	***/
		dbg(TRACE,"GetAccCloseValueHandle: array 'ACCHDL' not found!!!");
		return FALSE;
	}

	return TRUE;
}

/****************************************************************************/
/****************************************************************************/
/*	MakeAccUpdateString														*/
/*	Erstellt UpdateString f�r ein ACC Record								*/
/****************************************************************************/
/****************************************************************************/
static void	MakeAccUpdateString(const char *pcpMonth,const char *pcpField,const char *pcpMonth2,const char *pcpField2,char *pcpString,const char *pcpValue)
{
	int		i;
	int		n;
	static const char*	clFieldIndex[3] = {	"OP","CO","CL" };

	pcpString[0] = '\0';

	dbg(DEBUG,"varMonth <%s>" , pcpMonth);
	dbg(DEBUG,"varField <%s>" , pcpField);
	dbg(DEBUG,"varMonth2<%s>" , pcpMonth2);
	dbg(DEBUG,"varField2<%s>" , pcpField2);
	dbg(DEBUG,"varValue <%s>" , pcpValue);

	/*** Alle Typen (CL,CO,OP) und alle Monate durchgehen ***/
	for (i=1; i <= 3; i++)
	{
		for (n = 1; n <= 12; n++)
		{
			if (atoi(pcpMonth) == n && strcmp(pcpField,clFieldIndex[i-1]) == 0)
			{
				strcat(pcpString,pcpValue);
				strcat(pcpString,",");
			}
			else 
			{
				if (atoi(pcpMonth2) == n && strcmp(pcpField2,clFieldIndex[i-1]) == 0)
				{
					strcat(pcpString,pcpValue);
					strcat(pcpString,",");
				}
				else
				{
					strcat(pcpString," ,");
				}
			}
		}
	}
}

/********************************************************************************************************/
/********************************************************************************************************/
/*	UpdateAccountHandle: aktualisiert oder erzeugt das Konto fuer den Ma. CL und OP Feld wird gesetzt	*/
/*	Rueckgabe: 0 -> Erfolg																				*/
/*  <0 -> Fehler																						*/
/********************************************************************************************************/

static BOOL UpdateAccountHandle(double dpAccAccountValue,const char *pcpAccAccountType,const char *pcpMonth,const char *pcpYear,const char *pcpStfu,const char *pcpUseu)
{
	char	clPeno[128];
	char	clAccAccountValue[128];

	char	clRecord[2048];
	char	clTimeNowString[16];
	Date	olDate;
	char	clAccNextMonth[6];
	char	clMonth1[6];
	char	clMonth2[6];
	BOOL	blIsDezember;
	char	clUpdFields[128];
	char	clUpdValues[256];
	char	clUrno[12];
	char	clString[1024];
	char	clAccKeyValue[256];
	int		ilAccHandleLocal;
	int		ilLen;
	char	clAccRecord[2048];

	dbg(TRACE,"*** Start: accAccountValue <%lf>",dpAccAccountValue);

	/*** Personalnummer wird in init_drr.bas oder �hnlichem initialisiert ***/
	if (ReadTemporaryField("Personalnummer",clPeno) <= 0)
	{
		strcpy(clPeno," ");
	}

	/*** Runden und 2 Stellen hinter dem Komma ***/
	sprintf(clAccAccountValue,"%-8.2lf",dpAccAccountValue);

	/***** Darf nicht l�nger als 8 Zeichen sein ***/
	clAccAccountValue[8] = '\0';

	/*** Aktuelle Zeit als String auslesen ***/
	olDate = GetCurrentDate();

	DateToString(clTimeNowString,16,"%Y%m%d%H%M%S",&olDate);

	/********************** Oeffnungkonto fuer naechsten Monat ****************/
	sprintf(clAccNextMonth,"%02d",atoi(pcpMonth)+1);

	if (strcmp(pcpMonth,"12") == 0)
	{
		blIsDezember = TRUE;
	}
	else
	{
		char clTmp[6];
		strcpy(clTmp,clAccNextMonth);
		blIsDezember = FALSE;
		sprintf(clAccNextMonth,"OP%s",clTmp);
	}

	dbg(TRACE,"UpdateAccount(): varIsDezember<%d>" , blIsDezember);

	/*** initialize data ****/
	strcpy(clAccKeyValue,pcpYear);
	strcat(clAccKeyValue,",");
	strcat(clAccKeyValue,pcpStfu);
	strcat(clAccKeyValue,",");
	strcat(clAccKeyValue,pcpAccAccountType);

	dbg(TRACE, "varAccKeyValue <%s>",clAccKeyValue);
	dbg(TRACE, "accAccountValue<%s>",clAccAccountValue);

	/*** get handle of array	****/
	ilAccHandleLocal = GetArrayIndex("ACCHDL");
	if (ilAccHandleLocal >= 0)
	{
		/***** get first record that matches condition	*/
		strcpy(clAccRecord,clAccKeyValue);
		ilLen = FindFirstRecordByIndex(ilAccHandleLocal,clAccRecord);
		if (ilLen > 0)
		{
			/**** Key wurde gefunden, Update wird eingeleitet ***/
			/***** Feldnamen fuer Korrekturkonto dieses Monats generieren	****/

			if (!blIsDezember)
			{
				/*** OP Feld naechster Monat setzen ***/
				strcpy(clMonth1,"CL");
				strcat(clMonth1,pcpMonth);

				strcpy(clMonth2,clAccNextMonth);

				/***** Feldliste erzeugen *****/
				strcpy(clUpdFields,clMonth1);
				strcat(clUpdFields,",");
				strcat(clUpdFields,clMonth2);
				strcat(clUpdFields,",USEU,LSTU");

				/***** Wertliste erzeugen *****/
				strcpy(clUpdValues,clAccAccountValue);
				strcat(clUpdValues,",");
				strcat(clUpdValues,clAccAccountValue);
				strcat(clUpdValues,",");
				strcat(clUpdValues,pcpUseu);
				strcat(clUpdValues,",");
				strcat(clUpdValues,clTimeNowString);
			}
			else
			{
				/*** Nur CL Feld setzen ***/

				strcpy(clMonth1,"CL");
				strcat(clMonth1,pcpMonth);

				/***** Feldliste erzeugen ****/
				strcpy(clUpdFields,clMonth1);
				strcat(clUpdFields,",USEU,LSTU");

				/***** Wertliste erzeugen ****/
				strcpy(clUpdValues,clAccAccountValue);
				strcat(clUpdValues,",");
				strcat(clUpdValues,pcpUseu);
				strcat(clUpdValues,",");
				strcat(clUpdValues,clTimeNowString);
			}

			/***************** USEU und COxy setzen *******************************/
			dbg(TRACE,"UpdateAccountHandle(): Veraendere Datensatz: Felder <%s> - Werte <%s>",clUpdFields,clUpdValues);

			if (UpdateRecord(ilAccHandleLocal,clUpdFields,clUpdValues) != 0)
			{
				dbg(TRACE,"UpdateAccountHandle(): Fehler beim Aktualisieren des Datensatzes!!!");
				return FALSE;
			}
			else
			{
				return TRUE;
			}
		}

		/***  if we get here, no matching record was found, inserting new record	****/
		/****** Datensatz existiert NICHT -> anlegen	****/
		/****** get urno *****/
		if (GetNextUrno(clUrno) <= 0)
		{
			dbg(TRACE,"UpdateAccountHandle(): neue URNO konnte nicht erhalten werden!!!");
			return FALSE;
		}

		strcpy(clRecord,"0,");				/*	YELA	*/
		strcat(clRecord,"0,");				/*	YECU	*/
		strcat(clRecord,clPeno);
		strcat(clRecord,",000000000000");	/*	FUMO	*/ 
		strcat(clRecord,", ,");
		strcat(clRecord,clTimeNowString);
		strcat(clRecord,",");
		strcat(clRecord,pcpUseu);
		strcat(clRecord,", ,");
		strcat(clRecord,clUrno);
		strcat(clRecord,",");
		strcat(clRecord,pcpYear);
		strcat(clRecord,",");
		strcat(clRecord,pcpStfu);
		strcat(clRecord,",");
		strcat(clRecord,pcpAccAccountType);
		strcat(clRecord,",");
		strcat(clRecord,pcgHopo);
		strcat(clRecord,",");

		if (!blIsDezember)
		{
			sprintf(clAccNextMonth,"%02d",atoi(pcpMonth)+1);
			MakeAccUpdateString(pcpMonth,"CL",clAccNextMonth,"OP",clString,clAccAccountValue);
		}
		else
		{
			MakeAccUpdateString(pcpMonth,"CL","","",clString,clAccAccountValue);
		}

		/*** Zusammenbauen ***/
		strcat(clRecord,clString);

		dbg(TRACE,"UpdateAccountHandle(): Erzeuge neuen Datensatz: <%s>" , clRecord);

		if (InsertRecord(ilAccHandleLocal,clRecord) != 0)
		{
			dbg(TRACE,"UpdateAccountHandle(): Fehler beim Erzeugen eines neuen Datensatzes!!!");
		}
	}
	else
	{
		/*** array not found	****/
		dbg(TRACE,"UpdateAccountHandle(): array 'ACCHDL' not found!!!");
		return FALSE;
	}

	return TRUE;
}

/****************************************************************************/
/****************************************************************************/
/*	UpdateAccountXY: aktualisiert oder erzeugt den Wert in varField fuer	*/
/*	das Konto <varAccountType> fuer den Ma <varStaffUrno>.					*/
/*	Rueckgabe: keine														*/
/****************************************************************************/
/****************************************************************************/
static BOOL UpdateAccountXYHandle(const char *pcpStaffUrno,double dpAccountValue,const char *pcpAccountType,const char *pcpDate,const char *pcpUseu,const char *pcpField)
{
	char	clPeno[128];
	char	clAccountValue[128];
	Date	olDate;
	char	clTimeNowString[16];
	int		ilHandle;
	char	clAccKeyValue[512];
	char	clString[512];
	char	clRecord[2048];
	char	clYear[6];
	char	clMonth[4];
	char	clUpdFields[128];
	char	clUpdValues[256];
	char	clUrno[12];
	char	clSourceMonth[4];

	/*** Personalnummer wird in init_drr.bas oder �hnlichem initialisiert ***/ 
	if (ReadTemporaryField("Personalnummer",clPeno) <= 0)
	{
		strcpy(clPeno," ");
	}

	/*** Runden und 2 Stellen hinter dem Komma ***/
	sprintf(clAccountValue,"%-8.2lf",dpAccountValue);

	/***** Darf nicht l�nger als 8 Zeichen sein ***/
	clAccountValue[8] = '\0';

	/*** Aktuelle Zeit als String auslesen ***/
	olDate = GetCurrentDate();

	DateToString(clTimeNowString,16,"%Y%m%d%H%M%S",&olDate);

	clSourceMonth[0] = pcpDate[4];
	clSourceMonth[1] = pcpDate[5];
	clSourceMonth[2] = '\0';

	strncpy(clYear,pcpDate,4);
	clYear[4] = '\0';

	strcpy(clAccKeyValue,clYear);
	strcat(clAccKeyValue,",");
	strcat(clAccKeyValue,pcpStaffUrno);
	strcat(clAccKeyValue,",");
	strcat(clAccKeyValue,pcpAccountType);

	/****** Recordset oeffnen	****/
	ilHandle = GetArrayIndex("ACCHDL");
	if (ilHandle >= 0)
	{
		strcpy(clRecord,clAccKeyValue);
		if (FindFirstRecordByIndex(ilHandle,clRecord) > 0)
		{

			/***** Feldnamen fuer Korrekturkonto dieses Monats generieren	****/
			strcpy(clMonth,pcpField);
			strcat(clMonth,clSourceMonth);

			/***** Feldliste erzeugen	*****/
			strcpy(clUpdFields,clMonth);
			strcat(clUpdFields,",USEU,LSTU");

			/***** Wertliste erzeugen	*****/
			strcpy(clUpdValues,clAccountValue);
			strcat(clUpdValues,",");
			strcat(clUpdValues,pcpUseu);
			strcat(clUpdValues,",");
			strcat(clUpdValues,clTimeNowString);

			/***************** USEU und COxy setzen *******************************/
			dbg(TRACE,"UpdateAccountXY(): Veraendere Datensatz: Felder <%s> - Werte <%s>",clUpdFields,clUpdValues);

			if (UpdateRecord(ilHandle,clUpdFields,clUpdValues) != 0)
			{
				dbg(TRACE,"UpdateAccountXY(): Fehler beim Aktualisieren des Datensatzes!!!");
				return FALSE;
			}

			return TRUE;
		}

		/****** Datensatz existiert NICHT -> anlegen	****/
		/****** get urno ******/
		if (GetNextUrno(clUrno) <= 0)
		{
			dbg(TRACE,"UpdateAccountXYHandle(): neue URNO konnte nicht erhalten werden!!!");
			return FALSE;
		}

		strcpy(clRecord,"0,");				/*	YELA	*/
		strcat(clRecord,"0,");				/*	YECU	*/
		strcat(clRecord,clPeno);
		strcat(clRecord,",000000000000");	/*	FUMO	*/
		strcat(clRecord,", ,");
		strcat(clRecord,clTimeNowString);
		strcat(clRecord,",");
		strcat(clRecord,pcpUseu);
		strcat(clRecord,", ,");
		strcat(clRecord,clUrno);
		strcat(clRecord,",");
		strcat(clRecord,clYear);
		strcat(clRecord,",");
		strcat(clRecord,pcpStaffUrno);
		strcat(clRecord,",");
		strcat(clRecord,pcpAccountType);
		strcat(clRecord,",");
		strcat(clRecord,pcgHopo);
		strcat(clRecord,",");

		MakeAccUpdateString(clSourceMonth,pcpField,"","",clString,clAccountValue);

		/* Zusammenbauen	*/
		strcat(clRecord,clString);

		dbg(TRACE,"UpdateAccountXY(): Erzeuge neuen Datensatz: <%s>" ,clRecord);
		if (InsertRecord(ilHandle,clRecord) != 0)
		{
			dbg(TRACE,"UpdateAccountXY(): Fehler beim Erzeugen eines neuen Datensatzes!!!");
			return FALSE;
		}
	}
	else
	{
		dbg(TRACE,"UpdateAccountXY(): Recordset '%s' konnte nicht geoeffnet werden!!!","ACCHDL");
		return FALSE;
	}

	return TRUE;
}

/************************************************************************************************************/
/************************************************************************************************************/
/*	Auslesen von allen Kontowerten (alle Monate) auf Basis des Handle										*/
/************************************************************************************************************/
/************************************************************************************************************/

static BOOL GetMultiAccCloseValueHandle(const char *pcpStfu,const char *pcpAccountType,const char *pcpYear,double dpAccValue[12])
{
	int		ilAccHandleLocal;
	char	clAccKeyValue[256];
	char	clAccValue[256];
	int		i;

	/**** get acc.-data from ACCTAB ****/
	memset(dpAccValue,0x00,sizeof(dpAccValue));

	/*** initialize data */
	strcpy(clAccKeyValue,pcpYear);
	strcat(clAccKeyValue,",");
	strcat(clAccKeyValue,pcpStfu);
	strcat(clAccKeyValue,",");
	strcat(clAccKeyValue,pcpAccountType);

	dbg(DEBUG, "varAccKeyValue <%s>",clAccKeyValue);

	/*** get handle of array	*****/
	ilAccHandleLocal = GetArrayIndex("ACCHDL");
	if (ilAccHandleLocal >= 0)
	{
		/***** get field values from first record that matches condition	*****/
		for (i = 0; i < 12; i++)
		{
			sprintf(clAccValue,"CL%02d",i+1);
			if (FindFieldValueByIndex(ilAccHandleLocal,clAccKeyValue,clAccValue) > 0)
			{
				dpAccValue[i] = atof(clAccValue);
			}
			else
			{
				dpAccValue[i] = 0;
			}
		}
	}
	else
	{
		dbg(TRACE,"GetMultiAccCloseValueHandle: array 'ACCHDL' not found!!!");
		return FALSE;
	}

	return TRUE;
}

/**************************************************************************/
/**************************************************************************/
/*	UpdateMultiAccountHandle: aktualisiert oder erzeugt das Konto fuer den Ma.f�r alle Monate	*/
/*	Rueckgabe:	1 -> Erfolg																		*/
/*				0 -> Fehler																		*/
/**************************************************************************/
/**************************************************************************/
BOOL UpdateMultiAccountHandle(const char *pcpAccountType,const char *pcpStfu,const char *pcpYear,const char *pcpUser,double dpNewAccValue[12])
{
	static const char *pcsUpdateFieldList = "CL01,CL02,CL03,CL04,CL05,CL06,CL07,CL08,CL09,CL10,CL11,CL12,OP02,OP03,OP04,OP05,OP06,OP07,OP08,OP09,OP10,OP11,OP12,USEU,LSTU";

	char	clPeno[128];
	char	clNewAccValue[12][20];
	Date	olDate;
	char	clTimeNowString[16];
	int		ilAccHandleLocal;
	char	clAccKeyValue[512];
	char	clString[512];
	char	clRecord[2048];
	char	clYear[6];
	char	clMonth[4];
	char	clUpdateValueList[2048];
	char	clUrno[12];
	int		i;

	/*** Personalnummer wird in init_drr.bas oder �hnlichem initialisiert ***/ 
	if (ReadTemporaryField("Personalnummer",clPeno) <= 0)
	{
		strcpy(clPeno," ");
	}

	/*** Runden und 2 Stellen hinter dem Komma ***/
	for (i = 0; i < 12; i++)
	{
		sprintf(clNewAccValue[i],"%-8.2lf",dpNewAccValue[i]);
		/***** Darf nicht l�nger als 8 Zeichen sein ***/
		clNewAccValue[i][8] = '\0';
	}

	/*** Aktuelle Zeit als String auslesen ***/
	olDate = GetCurrentDate();

	DateToString(clTimeNowString,16,"%Y%m%d%H%M%S",&olDate);

	/*** initialize data *****/
	strcpy(clAccKeyValue,pcpYear);
	strcat(clAccKeyValue,",");
	strcat(clAccKeyValue,pcpStfu);
	strcat(clAccKeyValue,",");
	strcat(clAccKeyValue,pcpAccountType);

	dbg(DEBUG,"varAccKeyValue <%s>",clAccKeyValue);

	/*** get handle of array	****/
	ilAccHandleLocal = GetArrayIndex("ACCHDL");
	if (ilAccHandleLocal >= 0)
	{
		strcpy(clRecord,clAccKeyValue);
		if (FindFirstRecordByIndex(ilAccHandleLocal,clRecord) > 0)
		{
			/**** Key wurde gefunden, Update wird eingeleitet ***/
			/***** Feldnamen fuer Korrekturkonto dieses Monats generieren	***/
			clUpdateValueList[0] = '\0';
			for (i = 0; i < 12; i++)
			{
				strcat(clUpdateValueList,clNewAccValue[i]);
				strcat(clUpdateValueList,",");
			}

			for (i = 0; i < 11; i++)
			{
				strcat(clUpdateValueList,clNewAccValue[i]);
				strcat(clUpdateValueList,",");
			}

			strcat(clUpdateValueList,pcpUser);
			strcat(clUpdateValueList,",");
			strcat(clUpdateValueList,clTimeNowString);

			/***************** USEU und COxy setzen *******************************/
			dbg(TRACE,"UpdateMultiAccountHandle(): Veraendere Datensatz: Felder <%s> - Werte <%s>",pcsUpdateFieldList,clUpdateValueList);

			if (UpdateRecord(ilAccHandleLocal,(char *)pcsUpdateFieldList,clUpdateValueList) != 0)
			{
				dbg(TRACE,"UpdateMultiAccountHandle(): Fehler beim Aktualisieren des Datensatzes!!!");
				return FALSE;
			}
			else
			{
				return TRUE;
			}
		};

		/****** Datensatz existiert NICHT -> anlegen	****/
		/****** get urno ******/
		if (GetNextUrno(clUrno) <= 0)
		{
			dbg(DEBUG,"UpdateAccountXY(): neue URNO konnte nicht erhalten werden!!!");
			return FALSE;
		}

		strcpy(clRecord,"0,");
		strcat(clRecord,"0,");
		strcat(clRecord,clPeno);
		strcat(clRecord,",000000000000");
		strcat(clRecord,", ,");
		strcat(clRecord,clTimeNowString);
		strcat(clRecord,",");
		strcat(clRecord,pcpUser);
		strcat(clRecord,", ,");
		strcat(clRecord,clUrno);
		strcat(clRecord,",");
		strcat(clRecord,pcpYear);
		strcat(clRecord,",");
		strcat(clRecord,pcpStfu);
		strcat(clRecord,",");
		strcat(clRecord,pcpAccountType);
		strcat(clRecord,",");
		strcat(clRecord,pcgHopo);
		strcat(clRecord,",");

		strcat(clRecord,"0,");
		for (i = 0; i < 11; i++)
		{
			strcat(clRecord,clNewAccValue[i]);
			strcat(clRecord,",");
		}

		strcat(clRecord,"0,0,0,0,0,0,0,0,0,0,0,0,");

		for (i = 0; i < 12; i++)
		{
			strcat(clRecord,clNewAccValue[i]);
			strcat(clRecord,",");
		}

		dbg(TRACE,"UpdateMultiAccountHandle(): Erzeuge neuen Datensatz: <%s>" ,clRecord);

		if (InsertRecord(ilAccHandleLocal,clRecord) != 0)
		{
			dbg(TRACE,"UpdateAccountXY(): Fehler beim Erzeugen eines neuen Datensatzes!!!");
			return FALSE;
		}
	}
	else
	{
		/*** array not found	****/
		dbg(TRACE,"UpdateAccountHandle(): array 'ACCHDL' not found!!!");
		return FALSE;
	}

	return TRUE;
}

/****************************************************************************/
/****************************************************************************/
/*	GetHolType: gibt den Typ des Feiertags f�r diesen Tag zur�ck			*/
/*	Rueckgabe: 0 -> kein Feiertag											*/
/****************************************************************************/
/****************************************************************************/
static BOOL GetHolType(const char *pcpSday,char *pcpHolType)
{
	char	clStrKey[18];

	dbg(DEBUG,"include/GetHolType(): search holiday SDAY = <%s>",pcpSday);

	/*	get holiday info from cached table HOLTAB	*/
	strcpy(clStrKey,pcpSday);
	strcat(clStrKey,"000000");

	return GetCachedDbValue("HOLTAB","HDAY",clStrKey,"TYPE",pcpHolType) > 0;
}

/* **************************************************************************/
/* **************************************************************************/
/* GetStartTimeDiff: Zeitunterschied an den Endpunkten						*/
/* **************************************************************************/
/* **************************************************************************/

static double GetStartTimeDiff(const char *pcpDrrStartTime,const char *pcpBsdStartTime)
{
	double	dlDiffTime;
	time_t	llDiffTime;

	/* ***  Laenge der Schicht inklusive Pause ***/
	if (GetRealTimeDiff(pcpDrrStartTime,pcpBsdStartTime,&llDiffTime) != RC_SUCCESS)
	{
		dbg(TRACE,"GetStartTimeDiff: GetRealTimeDiff failed");
		return 0;
	}

	/*** Laenge der Schicht in Minuten ***/
	dlDiffTime = llDiffTime;
	dlDiffTime /= 60;

	dbg(TRACE,"Differenz DRR-BSD Startzeit<%lf>",dlDiffTime);

	return dlDiffTime;
}

/* **************************************************************************/
/* **************************************************************************/
/* GetEndTimeDiff: Zeitunterschied an den Endpunkten						*/
/* **************************************************************************/
/* **************************************************************************/

static double GetEndTimeDiff(const char *pcpDrrEndTime,const char *pcpBsdEndTime)
{
	time_t	llDiffTime;
	double	dlDiffTime;
	char	clBsdEndTime[16];

	strcpy(clBsdEndTime,pcpBsdEndTime);
	if (strcmp(pcpDrrEndTime,pcpBsdEndTime) < 0)
	{
		/*** Einen Tag abziehen ****/
		AddSecondsToCEDATime(clBsdEndTime,(time_t)0-86400,1);	/* a day in seconds = 24 * 60 * 60 */
		dbg(TRACE,"Differenz DRR-BSD Endzeit geht ueber 24 Stunden hinaus!");
	}

	/***  Laenge der Schicht inklusive Pause ***/
	if (GetRealTimeDiff(clBsdEndTime,pcpDrrEndTime,&llDiffTime) != RC_SUCCESS)
	{
		dbg(TRACE,"GetEndTimeDiff: GetRealTimeDiff failed");
		return 0;
	}

	/*** Laenge der Schicht in Minuten ***/
	dlDiffTime = llDiffTime;
	dlDiffTime /= 60;

	dbg(TRACE, "Differenz DRR-BSD Endzeit<%lf>",dlDiffTime);

	return dlDiffTime;
}

/**********************************************************************/
/* GetDailyDRRMinutesByStaff: ermittelt die taegliche Planarbeitszeit */
/**********************************************************************/
static double GetDailyDRRMinutesByStaff(const char *pcpDrrBreakTime,const char *pcpDrrStartTime,const char *pcpDrrEndTime)
{
	time_t		llDiffTime;
	double		dlDiffTime;

	dbg(DEBUG, "GetDailyDRRMinutesByStaff: <%s>" , pcpDrrBreakTime);
	dbg(DEBUG, "GetDailyDRRMinutesByStaff: <%s>" , pcpDrrStartTime);
	dbg(DEBUG, "GetDailyDRRMinutesByStaff: <%s>" , pcpDrrEndTime);

	/* Length of shift/absence incl. break */
	if (GetRealTimeDiff(pcpDrrStartTime,pcpDrrEndTime,&llDiffTime) != RC_SUCCESS)
	{
		dbg(TRACE,"GetDailyDRRMinutesByStaff: ERROR - GetRealTimeDiff failed!");
		return 0;
	}

	/* Length of shift/absence incl. break in minutes */
	dlDiffTime = llDiffTime;
	dlDiffTime /= 60;
	dbg(DEBUG, "Length of shift/absence incl. break: <%lf> minutes",dlDiffTime);

	/* reduce break time */
	dlDiffTime = dlDiffTime - atof(pcpDrrBreakTime);
	if (dlDiffTime < 0)
		dlDiffTime = 0;
	dbg(DEBUG, "Length of shift/absence without break: <%lf> minutes",dlDiffTime);

	return dlDiffTime;
}

/* **************************************************************************/
/* **************************************************************************/
/* GetDailyBreakMinutesByStaff: ermittelt die taegliche Planarbeitszeit laut DRR	*/
/* **************************************************************************/
/* **************************************************************************/

static double GetDailyBreakMinutesByStaff(const char *pcpLocalCheckBsdu,const char *pcpLocalDrrBreakTime,const char *pcpSday)
{
	return 0;
}

/* **************************************************************************/
/* **************************************************************************/
/* GetDrsValue																*/
/* Rueckgabe: Anzahl der TZPlus und �berzeit Stunden						*/
/* **** ACHTUNG: R�ckgabe in Stunden ****									*/
/* **************************************************************************/
/* **************************************************************************/
static double GetDrsValue(const char *pcpBreakTime,const char *pcpStartTime,const char *pcpEndTime,const char *pcpBsdu,const char *pcpCode1,const char *pcpCode3,const char *pcpCode4,const char* pcpSDAY, const char *pcpIsTraining)
{
	double	dlDrrMin;
	double	dlResult;
	char	clResult[128];
	char	clBsdStartTime[16];
	char	clBsdEndTime[16];
	char	clBsdStartTime2[6];
	char	clBsdEndTime2[6];

	int ilBsdHandle = GetArrayIndex("BSDTAB");
	if (ilBsdHandle < 0)
	{
		dbg(TRACE,"GetDrsValue(): array 'BSDTAB' not found!");
		return 0;
	}

	dbg(DEBUG, "GetDrsValue(): varBreakTime: <%s>" , pcpBreakTime);
	dbg(DEBUG, "GetDrsValue(): varStartTime: <%s>" , pcpStartTime);
	dbg(DEBUG, "GetDrsValue(): varEndTime:   <%s>" , pcpEndTime);
	dbg(DEBUG, "GetDrsValue(): varBsdu:      <%s>" , pcpBsdu);
	dbg(DEBUG, "GetDrsValue(): Code1:        <%s>" , pcpCode1);
	dbg(DEBUG, "GetDrsValue(): Code3:        <%s>" , pcpCode3);
	dbg(DEBUG, "GetDrsValue(): Code4:        <%s>" , pcpCode4);

	/* **** Schichtdatensatz finden ****/
	if (strcmp(pcpCode1,"8") == 0 ||
		strcmp(pcpCode3,"8") == 0 ||
		strcmp(pcpCode1,"1") == 0 ||
		strcmp(pcpCode3,"1") == 0 ||
		strcmp(pcpCode1,"2") == 0 ||
		strcmp(pcpCode3,"2"))
	{
		strcpy(clBsdStartTime2,"ESBG");
		strcpy(clBsdEndTime2,"LSEN");
		if (FindFieldValueByIndex(ilBsdHandle,(char *)pcpBsdu,clBsdStartTime2) <= 0 ||
		    FindFieldValueByIndex(ilBsdHandle,(char *)pcpBsdu,clBsdEndTime2) <= 0)
		{
			dbg(TRACE, "GetDrsValue(): BSD-record not found!");
			return 0;
		}

		strcpy(clBsdStartTime,pcpSDAY);
		strcpy(&clBsdStartTime[8],clBsdStartTime2);
		strcat(clBsdStartTime,"00");

		strncpy(clBsdEndTime,pcpSDAY,8);
		strcpy(&clBsdEndTime[8],clBsdEndTime2);
		strcat(clBsdEndTime,"00");
		if (strcmp(clBsdEndTime,clBsdStartTime) < 0)
		{
			/* add one day */
			AddSecondsToCEDATime(clBsdEndTime,(time_t)86400,1);
		}

		dbg(DEBUG, "GetDrsValue(): varBsdStartTime: <%s>" , clBsdStartTime);
		dbg(DEBUG, "GetDrsValue(): varBsdEndTime:   <%s>" , clBsdEndTime);
	}

	/* ************ Pr�fung *********/
	if (strcmp(pcpCode1,"8") != 0 &&
		strcmp(pcpCode3,"8") != 0 &&
		strcmp(pcpCode4,"8") != 0 &&
		strcmp(pcpCode1,"1") != 0 &&
		strcmp(pcpCode3,"1") != 0 &&
		strcmp(pcpCode4,"1") != 0 &&
		strcmp(pcpCode1,"2") != 0 &&
		strcmp(pcpCode3,"2") != 0 &&
		strcmp(pcpCode4,"2") != 0)
	{
		dbg(TRACE, "GetDrsValue(): No OT-Code found.");
		return 0;
	}

	/* **** Fall 1: varCode4 hat Wert 8 oder 1 oder 2, dann ist die gesamte Schicht  ****/
	if (strcmp(pcpCode4,"1") == 0 || strcmp(pcpCode4,"2") == 0 || strcmp(pcpCode4,"8") == 0)
	{
		dbg(DEBUG, "GetDrsValue(): varBreakTime: <%s>" , pcpBreakTime);
		dbg(DEBUG, "GetDrsValue(): varStartTime: <%s>" , pcpStartTime);
		dbg(DEBUG, "GetDrsValue(): varEndTime:   <%s>" , pcpEndTime);

		/* *********** taegliche, geplante Soll-Arbeitszeit ermitteln ***************/
		dlDrrMin = GetDailyDRRMinutesByStaff(pcpBreakTime,pcpStartTime,pcpEndTime);

		dlResult = (dlDrrMin + atof(pcpBreakTime)) / 60;

		dbg(DEBUG, "GetDrsValue(): varResult: <%lf>" , dlResult);

		/* *** Achtung, falls es eine Abwesenheit ist und die L�nge NULL ist, wird die Wochenarbeitszeit eingesetzt ***/
		if (IsBsd(pcpBsdu) == 0  && fabs(dlResult) < 1.0e-5)
		{
			dbg(DEBUG, "GetDrsValue(): Absence with duration NULL found.");

			/* **** Pr�fe auf Schulung ****/
			if (pcpIsTraining[1] == '1')
			{
				dbg(DEBUG, "GetDrsValue(): Training found.");
				ReadTemporaryField("KursLaenge",clResult);
				dlResult = atof(clResult);
			}
			else
			{
				dbg(DEBUG, "GetDrsValue(): General absence found.");
				ReadTemporaryField("VereinbarteWochenstunden",clResult);
				dlResult = atof(clResult) / 60;
			}
		}
		return dlResult;
	}

	/* **** Fall 2: varCode3 hat Wert 8 oder 1 oder 2, dann nur der hintere Teil der Schicht ****/
	if (strcmp(pcpCode3,"1") == 0 || strcmp(pcpCode3,"2") == 0 || strcmp(pcpCode3,"8") == 0)
	{
		dlResult = GetEndTimeDiff(pcpEndTime,clBsdEndTime) / 60;
	}

	/* **** Fall 3: varCode1 hat Wert 8 oder 1 oder 2, dann nur der vordere Teil der Schicht ****/
	if (strcmp(pcpCode1,"1") == 0 || strcmp(pcpCode1,"2") == 0 || strcmp(pcpCode1,"8") == 0)
	{
		dlResult = (GetStartTimeDiff(pcpStartTime,clBsdStartTime) / 60) + dlResult;
	}
	dbg(DEBUG, "GetDrsValue(): <dlResult> = <%lf>" , dlResult);
	return dlResult;
}

/**************************************************************************************/
/*	Konto 26                                                                      */
/*	GetValue26: Berechnung der Arbeitszeit fuer einen Tag und einen  Mitarbeiter  */
/**************************************************************************************/
BOOL GetValue26(const char *pcpCheckBsdu,const char *pcpLocalDrrBreakTime,const char *pcpLocalDrrStartTime,const char *pcpLocalDrrEndTime,const char *pcpLocalDrrShiftCode,const char *pcpSday,const char *pcpVereinbarteWochenstunden,const char *pcpScoCode,double *pdpDrrMin,const char *pcpRegFree, const char *pcpSleepDay, const char *pcpIsTraining, const char *pcpIsBsd)
{
	char	clKursLaenge[128];
	char	clHolType[2];

	*pdpDrrMin = 0;

	/* check variables */
	if (strcmp(pcpCheckBsdu,"0") == 0 || strlen(pcpCheckBsdu) == 0 || 
		strlen(pcpLocalDrrBreakTime) == 0 || 
		strlen(pcpLocalDrrStartTime) == 0 ||
		strlen(pcpLocalDrrEndTime) == 0	  ||
		strlen(pcpLocalDrrShiftCode) == 0)
	{
		dbg(TRACE,"GetValue26(): Wrong parameters! (maybe deleted)");
		return FALSE;
	}

	/* is it regular free or sleep day? */
	if (pcpRegFree[0] == '1' || pcpSleepDay[0] == '1')
	{
		dbg(TRACE, "GetValue26(): Regular free or sleep day found.");
		return TRUE;
	}

	/* is it a shift ? */
	if (pcpIsBsd[0] == '1')
	{
		dbg(DEBUG,"GetValue26(): Shift found.");

		/* calculate shift duration */
		*pdpDrrMin = GetDailyDRRMinutesByStaff(pcpLocalDrrBreakTime,pcpLocalDrrStartTime,pcpLocalDrrEndTime);

		dbg(DEBUG,"GetValue26() : Shift minutes <pdpDrrMin> = <%lf>",*pdpDrrMin);

		return TRUE;
	}
	else
	{
		/* it is an absence */
		dbg(TRACE,"GetValue26(): Absence found.");

		/* calculate shift duration */
		*pdpDrrMin = GetDailyDRRMinutesByStaff(pcpLocalDrrBreakTime,pcpLocalDrrStartTime,pcpLocalDrrEndTime);

		dbg(DEBUG,"GetValue26(): VarDrrMin %lf",*pdpDrrMin);

		/* check if shift duration is valid */
		if (fabs(*pdpDrrMin) <= 1.0e-5)
		{
			if (pcpIsTraining[0] == '1')
			{
				ReadTemporaryField("KursLaenge",clKursLaenge);
				*pdpDrrMin = atof(clKursLaenge) * 60;

				dbg(DEBUG, "GetValue26(): It is a training!");
				dbg(DEBUG, "GetValue26(): Training found, duration: %lf",*pdpDrrMin);
			}
			else
			{
				*pdpDrrMin = atof(pcpVereinbarteWochenstunden); 
				GetHolType(pcpSday,clHolType);

				dbg(DEBUG, "GetValue26(): absence, holiday type: <%s>",clHolType,">");
				dbg(DEBUG, "GetValue26(): Full duration: %lf",*pdpDrrMin);

				if (strcmp(clHolType,"3") == 0)
				{
					/* take only the half time */
					dbg(DEBUG,"GetValue26(): 'Half'-holiday, so we take only the half agreed workingtime!");
					*pdpDrrMin = atof(pcpVereinbarteWochenstunden) / 2;
				}
			}
		}
	}

	dbg(DEBUG,"GetValue26(): Result value %lf",*pdpDrrMin);
	return TRUE;
}

/****************************************************************************/
/****************************************************************************/
/*	initialize add staff to account prefetch								*/
/****************************************************************************/
/****************************************************************************/
void bas_init_addstf()
{
	char clLocalYear[6];

	dbg(DEBUG,"============================= INIT ADD STAFF SCRIPT START =======================");

	if (!StfToAccPrefetch(pcgData,pcgSelKey,TRUE))
	{
		dbg(TRACE,"bas_init_addstf:StfToAccPrefetch failed");
	}
	else
	{
		dbg(TRACE,"Added <%s> to prefetch account list of year <%s>",pcgData,pcgSelKey);
	}


	sprintf(clLocalYear,"%d",atoi(pcgSelKey)+1);

	if (!StfToAccPrefetch(pcgData,clLocalYear,TRUE))
	{
		dbg(TRACE,"bas_init_addstf:StfToAccPrefetch failed");
	}
	else
	{
		dbg(TRACE,"Added <%s> to prefetch account list of year <%s>",pcgData,clLocalYear);
	}

}

/****************************************************************************/
/****************************************************************************/
/*	initialize delete staff from account prefetch							*/
/****************************************************************************/
/****************************************************************************/
void bas_init_delstf()
{
	char clLocalYear[128];

	dbg(DEBUG,"============================= INIT DEL STAFF SCRIPT START =======================");

	if (!StfToAccPrefetch(pcgData,pcgSelKey,FALSE))
	{
		dbg(TRACE,"bas_init_delstf:StfToAccPrefetch failed");
	}
	else
	{
		dbg(TRACE,"Removed <%s> from prefetch account list of year <%s>",pcgData,pcgSelKey);
	}

	sprintf(clLocalYear,"%d",atoi(pcgSelKey)+1);

	if (!StfToAccPrefetch(pcgData,clLocalYear,FALSE))
	{
		dbg(TRACE,"bas_init_delstf:StfToAccPrefetch failed");
	}
	else
	{
		dbg(TRACE,"Removed <%s> from prefetch account list of year <%s>",pcgData,clLocalYear);
	}
}

/***************************************************************/
/* check if the account should be calculated for that contract */
/***************************************************************/
static int CheckCalcAccount(const char *pcpAccount,const char *pcpScoCode,const char *pcpSday)
{
	static 	BOOL blFirst = TRUE;
	static 	int  ilAciHandle;
	char*	pclAciFields;
	char 	clAciRecord[100];
	char	clAciDate[16];
	char	clVpfr[16];
	char	clVpto[16];
	int	ilLen;

	/* get handle of ACITAB */
	if (blFirst)
	{
		blFirst = FALSE;
		ilAciHandle = GetArrayIndex("ACITAB");
	}
	if (ilAciHandle < 0)
	{
		dbg(TRACE,"CheckCalcAccount(): ERROR - handle of Recordsets 'ACITAB' invalid (value: %d)!",ilAciHandle);
		return FALSE;
	}

	/* do initializations */
	pclAciFields = sgRecordsets[ilAciHandle].cFieldList;/*"CTRC,TYPE,VPFR,VPTO";*/

	strcpy(clAciDate,pcpSday);
	strcat(clAciDate,"000000");

	strcpy(clAciRecord,pcpScoCode);
	strcat(clAciRecord,",");
	strcat(clAciRecord,pcpAccount);

	ilLen = FindFirstRecordByIndex(ilAciHandle,clAciRecord);
	while(ilLen > 0)
	{
		/* check times of record */
		if (GetFieldValueFromMonoBlockedDataString("VPFR",pclAciFields,clAciRecord,clVpfr) > 0 && strcmp(clVpfr,clAciDate) <= 0)
		{
			if (GetFieldValueFromMonoBlockedDataString("VPTO",pclAciFields,clAciRecord,clVpto) <= 0 || strlen(rtrim(clVpto)) == 0 || strcmp(clVpto,clAciDate) >= 0)
			{
				dbg(DEBUG,"CheckCalcAccount(): Calculate account for account = <%s> and contract = <%s> at day = <%s>",pcpAccount,pcpScoCode,pcpSday);
				return TRUE;
			}
		}

		strcpy(clAciRecord,pcpScoCode);
		strcat(clAciRecord,",");
		strcat(clAciRecord,pcpAccount);
		ilLen = FindNextRecordByIndex(ilAciHandle,clAciRecord);
	};

	dbg(TRACE,"CheckCalcAccount(): No valid record found for account = <%s> and contract = <%s> at day = <%s>",pcpAccount,pcpScoCode,pcpSday);
	return FALSE;
}

/**************************************************************************
 **************************************************************************
 CountElements
 Zaehlt die Anzahl der Eintraege in einer Kommagetrennten Wertliste
 R�ckgabe Anzahl der Eintraege
 **************************************************************************
 **************************************************************************/
static int CountElements(char *pcpValueList)
{
	/***** lokale Variablen	****/
	int	ilStop	 = 0;
	int ilAnzahl = 1;
	int ilPosition;

	/***** abschliessende Leerzeichen loeschen	***/
	pcpValueList = rtrim(pcpValueList);

	dbg(DEBUG,"CountElements: count entries in <%s>" ,pcpValueList);

	/***** gibt es was zu tun (Liste leer oder nur ein Komma)?	***/
	if (strlen(pcpValueList) == 0)
	{
		dbg(DEBUG,"CountElements: <pcpValueList> empty!");
		return 0;
	}

	ilStop = strlen(pcpValueList);
	for (ilPosition = 0; ilPosition < ilStop; ilPosition++)
	{
		/***** aktuelles Zeichen = Komma?	***/
		if (pcpValueList[ilPosition] == ',')
		{
			/***** gibt es Zeichen hinter dem Komma?	***/
			if (ilPosition < ilStop - 1)
			{
				/***** weiteres Element gefunden		***/
				++ilAnzahl;
			}
		}
	}

	return ilAnzahl;
}

/**************************************************************************
 **************************************************************************
 GetElement:
 liefert den Eintrag Nr. <varNummer> aus einer Komma-getrennten Wertliste
 <varListe>. Der erste Eintrag hat die Nummer 0!!!!!
 R�ckgabe der Eintrag oder undef, wenn nicht enthalten
 **************************************************************************
 **************************************************************************/
static int GetElement(char *pcpVarList,int ipNumber,char *pcpElement)
{
	/***** lokale Variablen	*****/
	int ilPosition 	= 0;
	int	ilStop 		= -1;
	int	ilAnzahl 	= 1;
	int	ilVon		= -1;
	int	ilBis		= -1;

	/***** abschliessende Leerzeichen loeschen	***/
	pcpVarList = rtrim(pcpVarList);

	/***** gibt es was zu tun (Liste leer oder nur ein Komma)?	***/
	if (strlen(pcpVarList) == 0)
	{
		dbg(DEBUG,"GetElement: Liste ist leer!!!");
		return 0;
	}

	/***** Nummer des letzten Zeichens	**/
	ilStop = strlen(pcpVarList);

	/***** Startposition ermitteln		****/
	if (ipNumber == 0)
	{ 
		ilVon = 0;
	}
	else
	{
		for(ilPosition = 0; ilPosition < ilStop; ilPosition++)
		{
			/***** aktuelles Zeichen = Komma?		****/
			if(pcpVarList[ilPosition] == ',')
			{
				/***** gibt es Zeichen hinter dem Komma?	****/
				if (ilPosition < ilStop - 1)
				{
					/***** beginnt hier Element Nr. X?	****/
					if (ilAnzahl == ipNumber)
					{
						/***** ja -> suche Ende	***/
						ilVon = ilPosition+1;
						break;
					}
					/***** weiteres Element gefunden	***/
					++ilAnzahl;
				}
			}
		}
	}

	/***** Entrag nicht gefunden?	****/
	if (ilVon == -1)
	{
		return -1;
	}

	ilBis = ilStop - 1;

	for(ilPosition = ilVon; ilPosition < ilStop; ilPosition++)
	{
		/***** aktuelles Zeichen = Komma?	***/
		if(pcpVarList[ilPosition] == ',')
		{
			/***** hier endet Element Nr. X	***/
			{
				ilBis = ilPosition;
				break;
			}
		}
	}

	strncpy(pcpElement,&pcpVarList[ilVon],ilBis-ilVon);
	pcpElement[ilBis-ilVon] = '\0';
	return strlen(pcpElement);
}

/************************************************************************************************************/
/************************************************************************************************************/
/*	Auslesen von allen Kontowerten (alle Monate) auf Basis des Handle										*/
/************************************************************************************************************/
/************************************************************************************************************/

static BOOL GetMultiAccCorValueHandle(const char *pcpStfu,const char *pcpAccountType,const char *pcpYear,double dpAccValue[12])
{
	int		ilAccHandleLocal;
	char	clAccKeyValue[256];
	char	clAccValue[256];
	int		i;

	/**** get acc.-data from ACCTAB ****/
	memset(dpAccValue,0x00,sizeof(dpAccValue));

	/*** initialize data */
	strcpy(clAccKeyValue,pcpYear);
	strcat(clAccKeyValue,",");
	strcat(clAccKeyValue,pcpStfu);
	strcat(clAccKeyValue,",");
	strcat(clAccKeyValue,pcpAccountType);

	dbg(DEBUG, "varAccKeyValue <%s>",clAccKeyValue);

	/*** get handle of array	*****/
	ilAccHandleLocal = GetArrayIndex("ACCHDL");
	if (ilAccHandleLocal >= 0)
	{
		/***** get field values from first record that matches condition	*****/
		for (i = 0; i < 12; i++)
		{
			sprintf(clAccValue,"CO%02d",i+1);
			if (FindFieldValueByIndex(ilAccHandleLocal,clAccKeyValue,clAccValue) > 0)
			{
				dpAccValue[i] = atof(clAccValue);
			}
			else
			{
				dpAccValue[i] = 0;
			}
		}
	}
	else
	{
		dbg(TRACE,"GetMultiAccCloseValueHandle: array 'ACCHDL' not found!!!");
		return FALSE;
	}

	return TRUE;
}

/********************************************************************************************************/
/********************************************************************************************************/
/*	UpdateAccountWithoutOpenHandle: aktualisiert oder erzeugt das Konto fuer den Ma.					*/
/*	setzt nicht den Open Wert f�r den n�chsten Monat													*/
/*	Rueckgabe: 0 -> Erfolg																				*/
/*  <0 -> Fehler																						*/
/********************************************************************************************************/

static BOOL UpdateAccountWithoutOpenHandle(double dpAccAccountValue,const char *pcpAccAccountType,const char *pcpMonth,const char *pcpYear,const char *pcpStfu,const char *pcpUseu)
{
	char	clPeno[128];
	char	clAccAccountValue[128];

	char	clRecord[2048];
	char	clTimeNowString[16];
	Date	olDate;
	char	clMonth1[6];
	char	clUpdFields[128];
	char	clUpdValues[256];
	char	clUrno[12];
	char	clString[1024];
	char	clAccKeyValue[256];
	int		ilAccHandleLocal;
	int		ilLen;
	char	clAccRecord[2048];

	dbg(TRACE,"*** Start: accAccountValue <%lf>",dpAccAccountValue);

	/*** Personalnummer wird in init_drr.bas oder �hnlichem initialisiert ***/
	if (ReadTemporaryField("Personalnummer",clPeno) <= 0)
	{
		strcpy(clPeno," ");
	}

	/*** Runden und 2 Stellen hinter dem Komma ***/
	sprintf(clAccAccountValue,"%-8.2lf",dpAccAccountValue);

	/***** Darf nicht l�nger als 8 Zeichen sein ***/
	clAccAccountValue[8] = '\0';

	/*** Aktuelle Zeit als String auslesen ***/
	olDate = GetCurrentDate();

	DateToString(clTimeNowString,16,"%Y%m%d%H%M%S",&olDate);

	/*** initialize data ****/
	strcpy(clAccKeyValue,pcpYear);
	strcat(clAccKeyValue,",");
	strcat(clAccKeyValue,pcpStfu);
	strcat(clAccKeyValue,",");
	strcat(clAccKeyValue,pcpAccAccountType);

	dbg(TRACE, "varAccKeyValue <%s>",clAccKeyValue);
	dbg(TRACE, "accAccountValue<%s>",clAccAccountValue);

	/*** get handle of array	****/
	ilAccHandleLocal = GetArrayIndex("ACCHDL");
	if (ilAccHandleLocal >= 0)
	{
		/***** get first record that matches condition	*/
		strcpy(clAccRecord,clAccKeyValue);
		ilLen = FindFirstRecordByIndex(ilAccHandleLocal,clAccRecord);
		if (ilLen > 0)
		{
			/**** Key wurde gefunden, Update wird eingeleitet ***/
			/***** Feldnamen fuer Korrekturkonto dieses Monats generieren	****/

			/*** Nur CL Feld setzen ***/

			strcpy(clMonth1,"CL");
			strcat(clMonth1,pcpMonth);

			/***** Feldliste erzeugen ****/
			strcpy(clUpdFields,clMonth1);
			strcat(clUpdFields,",USEU,LSTU");

			/***** Wertliste erzeugen ****/
			strcpy(clUpdValues,clAccAccountValue);
			strcat(clUpdValues,",");
			strcat(clUpdValues,pcpUseu);
			strcat(clUpdValues,",");
			strcat(clUpdValues,clTimeNowString);

			/***************** USEU und COxy setzen *******************************/
			dbg(TRACE,"UpdateAccountWithoutOpenHandle(): Veraendere Datensatz: Felder <%s> - Werte <%s>",clUpdFields,clUpdValues);

			if (UpdateRecord(ilAccHandleLocal,clUpdFields,clUpdValues) != 0)
			{
				dbg(TRACE,"UpdateAccountWithoutOpenHandle(): Fehler beim Aktualisieren des Datensatzes!!!");
				return FALSE;
			}
			else
			{
				return TRUE;
			}
		}

		/***  if we get here, no matching record was found, inserting new record	****/
		/****** Datensatz existiert NICHT -> anlegen	****/
		/****** get urno *****/
		if (GetNextUrno(clUrno) <= 0)
		{
			dbg(TRACE,"UpdateAccountWithoutOpenHandle(): neue URNO konnte nicht erhalten werden!!!");
			return FALSE;
		}

		strcpy(clRecord,"0,");				/*	YELA	*/
		strcat(clRecord,"0,");				/*	YECU	*/
		strcat(clRecord,clPeno);
		strcat(clRecord,",000000000000");	/*	FUMO	*/ 
		strcat(clRecord,", ,");
		strcat(clRecord,clTimeNowString);
		strcat(clRecord,",");
		strcat(clRecord,pcpUseu);
		strcat(clRecord,", ,");
		strcat(clRecord,clUrno);
		strcat(clRecord,",");
		strcat(clRecord,pcpYear);
		strcat(clRecord,",");
		strcat(clRecord,pcpStfu);
		strcat(clRecord,",");
		strcat(clRecord,pcpAccAccountType);
		strcat(clRecord,",");

		MakeAccUpdateString(pcpMonth,"CL","","",clString,clAccAccountValue);

		/*** Zusammenbauen ***/
		strcat(clRecord,clString);

		dbg(TRACE,"UpdateAccountWithoutOpenHandle(): Erzeuge neuen Datensatz: <%s>" , clRecord);

		if (InsertRecord(ilAccHandleLocal,clRecord) != 0)
		{
			dbg(TRACE,"UpdateAccountWithoutOpenHandle(): Fehler beim Erzeugen eines neuen Datensatzes!!!");
		}
	}
	else
	{
		/*** array not found	****/
		dbg(TRACE,"UpdateAccountWithoutOpenHandle(): array 'ACCHDL' not found!!!");
		return FALSE;
	}

	return TRUE;
}

/**************************************************************************
 **************************************************************************
 GetDaysInMonth(varDate)
 Rueckgabe: Anzahl der Tage im Monat
 **************************************************************************
 **************************************************************************/
static int GetDaysInMonth(const char *pcpDate)
{
	static int ilDaysOfMonth[12] = {31,28,31,30,31,30,31,31,30,31,30,31};
	char clLocalYear[6];
	char clLocalMonth[4];

	int	ilLocalYear;
	int	ilLocalMonth;


	strncpy(clLocalYear,&pcpDate[0],4);
	clLocalYear[4] = '\0';

	strncpy(clLocalMonth,&pcpDate[4],2);
	clLocalMonth[2] = '\0';

	ilLocalYear = atoi(clLocalYear);
	ilLocalMonth = atoi(clLocalMonth);

	if (ilLocalMonth == 2)
	{
		if (LeapYear(ilLocalYear))
			return 29;
		else
			return 28;
	}
	else
	{
		return ilDaysOfMonth[ilLocalMonth-1];
	}

}

/**************************************************************************
 **************************************************************************
 GetDaysInYear(varYear)
 Rueckgabe: Anzahl der Tage im Jahr
 **************************************************************************
 **************************************************************************/
static int GetDaysInYear(int ipYear)
{
	int		ilDayCount = 0;
	char	clDate[16];
	int		ilMonthDays;
	int		i;

	dbg(DEBUG,"GetDaysInYear(varYear) %d",ipYear); 

	for (i = 1; i <= 12; i++)
	{
		/*** Tage f�r diesen Monat berechnen ***/

		sprintf(clDate,"%4d%02d01",ipYear,i);

		ilMonthDays = GetDaysInMonth(clDate);
		ilDayCount += ilMonthDays;

		dbg(DEBUG,"Monat:<%s> Tage: <%d>",clDate,ilMonthDays);
	}

	return ilDayCount;
}

/**************************************************************************
 **************************************************************************
 UpdateInitAccount: aktualisiert oder erzeugt das Konto fuer den Ma.
 Rueckgabe: 1 -> Erfolg
 		    0 -> Fehler
 **************************************************************************
 **************************************************************************/
 
static BOOL UpdateInitAccount(double dpAccAccountValue1,double dpAccAccountValue2,const char *pcpAccAccountType,const char *pcpYear,const char *pcpStfu,const char *pcpLastUser)
{
	char  clAccFieldlist[128];
	char  clAccRecord[2048];
	char  clAccUrno[12];
	int	  ilAccHandleLocal;

	char clPeno[12];
	char clAccAccountValue1[128];
	char clAccAccountValue2[128];
	Date olDate;
	char clTimeNowString[16];
	char clAccKeyValue[256];
	int	 ilLen;
	char clUpdateFieldList[128];
	char clUpdateValueList[256];

	/*** Personalnummer wird in init_drr.bas oder �hnlichem initialisiert ***/
	if (ReadTemporaryField("Personalnummer",clPeno) <= 0)
	{
		strcpy(clPeno," ");
	}

	/*** Runden und 2 Stellen hinter dem Komma ***/
	sprintf(clAccAccountValue1,"%-8.2lf",dpAccAccountValue1);
	sprintf(clAccAccountValue2,"%-8.2lf",dpAccAccountValue2);

	/***** Darf nicht l�nger als 8 Zeichen sein ***/
	clAccAccountValue1[8] = '\0';
	clAccAccountValue2[8] = '\0';

	/*** Aktuelle Zeit als String auslesen ***/
	olDate = GetCurrentDate();

	DateToString(clTimeNowString,16,"%Y%m%d%H%M%S",&olDate);

	strcpy(clAccKeyValue,pcpYear);
	strcat(clAccKeyValue,",");
	strcat(clAccKeyValue,pcpStfu);
	strcat(clAccKeyValue,",");
	strcat(clAccKeyValue,pcpAccAccountType);

	dbg(TRACE, "varAccKeyValue  <%s>",clAccKeyValue);
	dbg(TRACE, "accAccountValue1<%s>",clAccAccountValue1);
	dbg(TRACE, "accAccountValue2<%s>",clAccAccountValue2);

	/*** get handle of array	****/
	ilAccHandleLocal = GetArrayIndex("ACCHDL");
	if (ilAccHandleLocal >= 0)
	{
		/***** get first record that matches condition	*/
		strcpy(clAccRecord,clAccKeyValue);
		ilLen = FindFirstRecordByIndex(ilAccHandleLocal,clAccRecord);
		if (ilLen > 0)
		{
			/****** Datensatz existiert -> aendern	****/
			dbg(DEBUG,"UpdateInitAccount(): Aktualisiere Datensatz (alte Werte): <%s>" ,clAccRecord);

			/***************** Datensatz veraendern *******************************/

			strcpy(clUpdateFieldList,"OP01,YECU,USEU,LSTU");
			strcpy(clUpdateValueList,clAccAccountValue1);
			strcat(clUpdateValueList,",");
			strcat(clUpdateValueList,clAccAccountValue2);
			strcat(clUpdateValueList,",");
			strcat(clUpdateValueList,pcpLastUser);
			strcat(clUpdateValueList,",");
			strcat(clUpdateValueList,clTimeNowString);

			dbg(DEBUG,"UpdateInitAccount(): Veraendere Datensatz: <%s> Werte : <%s>",clUpdateFieldList ,clUpdateValueList);
			if (UpdateRecord(ilAccHandleLocal,clUpdateFieldList,clUpdateValueList) != 0)
			{
				dbg(DEBUG,"UpdateInitAccount(): Fehler beim Aktualisieren des Datensatzes!!!");
				return FALSE;
			}
		}
		else
		{
			/****** Datensatz existiert NICHT -> anlegen	****/
			/****** get urno 	****/
			if (GetNextUrno(clAccUrno) <= 0)
			{
				dbg(TRACE,"UpdateAccount(): neue URNO konnte nicht erhalten werden!!!");
				return FALSE;
			}

/*
			static const char* pcsAccFields = "YELA,YECU,PENO,FUMO,LSTU,CDAT,USEC,USEU,URNO,YEAR,STFU,TYPE,HOPO,OP01,OP02,OP03,OP04,OP05,OP06,OP07,OP08,OP09,OP10,OP11,OP12,CO01,CO02,CO03,CO04,CO05,CO06,CO07,CO08,CO09,CO10,CO11,CO12,CL01,CL02,CL03,CL04,CL05,CL06,CL07,CL08,CL09,CL10,CL11,CL12,YEDE";
*/

			strcpy(clAccRecord,"0,");				/*	YELA	*/
			strcat(clAccRecord,clAccAccountValue2);	/*	YECU	*/
			strcat(clAccRecord,",");
			strcat(clAccRecord,clPeno);
			strcat(clAccRecord,",000000000000");	/*	FUMO	*/ 
			strcat(clAccRecord,", ,");
			strcat(clAccRecord,clTimeNowString);
			strcat(clAccRecord,",");
			strcat(clAccRecord,pcpLastUser);
			strcat(clAccRecord,", ,");
			strcat(clAccRecord,clAccUrno);
			strcat(clAccRecord,",");
			strcat(clAccRecord,pcpYear);
			strcat(clAccRecord,",");
			strcat(clAccRecord,pcpStfu);
			strcat(clAccRecord,",");
			strcat(clAccRecord,pcpAccAccountType);
			strcat(clAccRecord,",");
			strcat(clAccRecord,pcgHopo);
			strcat(clAccRecord,",");
			strcat(clAccRecord,clAccAccountValue1);

			strcat(clAccRecord,"0,0,0,0,0,0,0,0,0,0,0,");
			strcat(clAccRecord,"0,0,0,0,0,0,0,0,0,0,0,0,");
			strcat(clAccRecord,"0,0,0,0,0,0,0,0,0,0,0,0,");

			dbg(DEBUG,"UpdateInitAccount(): Erzeuge neuen Datensatz: <%s>" ,clAccRecord);

			if (InsertRecord(ilAccHandleLocal,clAccRecord) != 0)
			{
				dbg(DEBUG,"UpdateInitAccount(): Fehler beim Erzeugen eines neuen Datensatzes!!!");
				return FALSE;
			}

		}
	}
	else
	{
		dbg(TRACE,"UpdateInitAccount(): Recordset 'ACCHDL' konnte nicht geoeffnet werden!!!");
		return FALSE;
	}

	return TRUE;
}


/**************************************************************************
 **************************************************************************
 NewUpdateMultiAccount: aktualisiert oder erzeugt das Konto fuer den Ma.f�r alle Monate
 Rueckgabe: 1 -> Erfolg
 		    0 -> Fehler
 **************************************************************************
 **************************************************************************/
 
static BOOL NewUpdateMultiAccount(const char *pcpAccAccountType,const char *pcpStaffUrno,const char *pcpYear,double dpNewAccValue[12])
{
	char	clNewAccValue[12][128];
	char	clPeno[12];
	Date	olDate;
	char	clTimeNowString[16];
	int		i;
	char	clAccKeyValue[256];
	int		ilAccHandleLocal;
	char	clAccRecord[2048];
	int		ilLen;
	char	clUpdateFieldList[256];
	char	clUpdateValueList1[256];
	char	clUpdateValueList2[256];
	char	clUpdateValueList[512];
	char	clAccUrno[12];
	char	clOpenString[256];
	char	clCloseString[256];

	/*** Runden und 2 Stellen hinter dem Komma ***/
	for (i = 0; i < 12; i++)
	{
		sprintf(clNewAccValue[i],"%-8.2lf",dpNewAccValue[i]);
		clNewAccValue[i][8] = '\0';
	}

	/*** Personalnummer wird in init_drr.bas oder �hnlichem initialisiert ***/
	if (ReadTemporaryField("Personalnummer",clPeno) <= 0)
	{
		strcpy(clPeno," ");
	}

	/*** Aktuelle Zeit als String auslesen ***/
	olDate = GetCurrentDate();

	DateToString(clTimeNowString,16,"%Y%m%d%H%M%S",&olDate);

	strcpy(clAccKeyValue,pcpYear);
	strcat(clAccKeyValue,",");
	strcat(clAccKeyValue,pcpStaffUrno);
	strcat(clAccKeyValue,",");
	strcat(clAccKeyValue,pcpAccAccountType);

	dbg(TRACE, "varAccKeyValue  <%s>",clAccKeyValue);
	for (i = 0; i < 12;i++)
	{
		dbg(TRACE, "accAccountValue1<%s>",clNewAccValue[i]);
	}

	/*** get handle of array	****/
	ilAccHandleLocal = GetArrayIndex("ACCHDL");
	if (ilAccHandleLocal >= 0)
	{
		/***** get first record that matches condition	*/
		strcpy(clAccRecord,clAccKeyValue);
		ilLen = FindFirstRecordByIndex(ilAccHandleLocal,clAccRecord);
		if (ilLen > 0)
		{
			/****** Datensatz existiert -> aendern	*****/

			strcpy(clUpdateFieldList,"CL01,CL02,CL03,CL04,CL05,CL06,CL07,CL08,CL09,CL10,CL11,CL12,OP02,OP03,OP04,OP05,OP06,OP07,OP08,OP09,OP10,OP11,OP12,USEU,LSTU");

			strcpy(clUpdateValueList1,clNewAccValue[0]);
			for (i = 1; i < 12; i++)
			{
				strcat(clUpdateValueList1,",");
				strcat(clUpdateValueList1,clNewAccValue[i]);
			}

			strcpy(clUpdateValueList2,clNewAccValue[0]);
			for (i = 1; i < 11; i++)
			{
				strcat(clUpdateValueList2,",");
				strcat(clUpdateValueList2,clNewAccValue[i]);
			}

			strcpy(clUpdateValueList,clUpdateValueList1);
			strcat(clUpdateValueList,",");
			strcat(clUpdateValueList,clUpdateValueList2);
			strcat(clUpdateValueList,",");
			strcat(clUpdateValueList,"SCBHDL,");
			strcat(clUpdateValueList,clTimeNowString);

			if (UpdateRecord(ilAccHandleLocal,clUpdateFieldList,clUpdateValueList) != 0)
			{
				dbg(DEBUG,"NewUpdateMultiAccount(): Fehler beim Aktualisieren des Datensatzes!!!");
				return FALSE;
			}
		}
		else
		{
			/****** Datensatz existiert NICHT -> anlegen	****/
			/****** get urno 	****/
			if (GetNextUrno(clAccUrno) <= 0)
			{
				dbg(TRACE,"UpdateAccount(): neue URNO konnte nicht erhalten werden!!!");
				return FALSE;
			}

			strcpy(clCloseString,clNewAccValue[0]);
			for (i = 1; i < 12; i++)
			{
				strcat(clCloseString,",");
				strcat(clCloseString,clNewAccValue[i]);
			}

			strcpy(clOpenString,clNewAccValue[0]);
			for (i = 1; i < 11; i++)
			{
				strcat(clOpenString,",");
				strcat(clOpenString,clNewAccValue[i]);
			}

			strcpy(clAccRecord,clPeno);
			strcat(clAccRecord,", ,");
			strcat(clAccRecord,clTimeNowString);
			strcat(clAccRecord,",SCBHDL, ,");
			strcat(clAccRecord,clAccUrno);
			strcat(clAccRecord,",");
			strcat(clAccRecord,pcpYear);
			strcat(clAccRecord,",");
			strcat(clAccRecord,pcpStaffUrno);
			strcat(clAccRecord,",");
			strcat(clAccRecord,pcpAccAccountType);
			strcat(clAccRecord,",");
			strcat(clAccRecord,pcgHopo);
			strcat(clAccRecord,",");
			strcat(clAccRecord,clCloseString);
			strcat(clAccRecord,",");
			strcat(clAccRecord,clOpenString);

			dbg(DEBUG,"NewUpdateMultiAccount(): Erzeuge neuen Datensatz: <%s>" , clAccRecord);
			if (InsertRecord(ilAccHandleLocal,clAccRecord) != 0)
			{
				dbg(DEBUG,"NewUpdateMultiAccount(): Fehler beim Erzeugen eines neuen Datensatzes!!!");
				return FALSE;
			}
		}
	}
	else
	{
		dbg(DEBUG,"NewUpdateMultiAccount(): Recordset 'ACCHDL' konnte nicht geoeffnet werden!!!");
		return FALSE;
	}

	return TRUE;
}



/***********************************************************************************************************
 ***********************************************************************************************************
 Auslesen von Kontowerten (YELA Wert)
 ***********************************************************************************************************
 ***********************************************************************************************************/
static double GetAccYelaValueHandle(const char *pcpAccountType,const char *pcpYear,const char *pcpStfu)
{
	int 	ilAccHandleLocal;
	char	clAccKeyValue[256];
	char	clAccValue[1024];

/*	vargTimerGetAccYelaValueHandle = initprofileentry()	*/

	/*** initialize data ****/
	strcpy(clAccKeyValue,pcpYear);
	strcat(clAccKeyValue,",");
	strcat(clAccKeyValue,pcpStfu);
	strcat(clAccKeyValue,",");
	strcat(clAccKeyValue,pcpAccountType);

	dbg(DEBUG,"varAccKeyValue <%s>",clAccKeyValue);

	/*** get handle of array	****/
	ilAccHandleLocal = GetArrayIndex("ACCHDL");
	if (ilAccHandleLocal >= 0)
	{
		/***** get field values from first record that matches condition	***/
		strcpy(clAccValue,"YELA");
		if (FindFieldValueByIndex(ilAccHandleLocal,clAccKeyValue,clAccValue) > 0)
		{
			return atof(clAccValue);
		}
	}
	else
	{
		/*** array not found	****/
		dbg(TRACE,"GetAccYelaValueHandle: array 'ACCHDL' not found!!!");
	}

	/*** R�ckgabe ***/
/*	dummy = addprofileentry("include.bas","GetAccYelaValueHandle",GetAccYelaValueHandle)*/
	return 0.0e0;
}


/***********************************************************************************************************
 ***********************************************************************************************************
 Auslesen von Kontowerten (YECU Wert) Initialwert im Jahr
 Liest ebenso YEDE Wert aus. Wert steht dann in varYedeValue
 ***********************************************************************************************************
 ***********************************************************************************************************/
static double GetAccYecuValueHandle(const char *pcpAccountType,const char *pcpYear,const char *pcpStfu,double *dpYedeValue)
{
	int 	ilAccHandleLocal;
	char	clAccKeyValue[256];
	char	clAccValue[1024];

/*	vargTimerGetAccYelaValueHandle = initprofileentry()	*/

	/*** initialize data ****/
	strcpy(clAccKeyValue,pcpYear);
	strcat(clAccKeyValue,",");
	strcat(clAccKeyValue,pcpStfu);
	strcat(clAccKeyValue,",");
	strcat(clAccKeyValue,pcpAccountType);

	dbg(DEBUG,"varAccKeyValue <%s>",clAccKeyValue);

	/*** get handle of array	****/
	ilAccHandleLocal = GetArrayIndex("ACCHDL");
	if (ilAccHandleLocal >= 0)
	{
		/***** get field values from first record that matches condition	***/
		strcpy(clAccValue,"YEDE");
		if (FindFieldValueByIndex(ilAccHandleLocal,clAccKeyValue,clAccValue) > 0)
		{
			*dpYedeValue = atof(clAccValue);
		}
		else
		{
			*dpYedeValue = 0.0e0;
		}

		strcpy(clAccValue,"YECU");
		if (FindFieldValueByIndex(ilAccHandleLocal,clAccKeyValue,clAccValue) > 0)
		{
			return atof(clAccValue);
		}
	}
	else
	{
		/*** array not found	****/
		dbg(TRACE,"GetAccYecuValueHandle: array 'ACCHDL' not found!!!");
	}

	/*** R�ckgabe ***/
/*	dummy = addprofileentry("include.bas","GetAccYelaValueHandle",GetAccYelaValueHandle)*/
	return 0.0e0;
}

/***********************************************************************************************************
 ***********************************************************************************************************
 Ver�ndert oder Erzeugt ein Initialkonto mit allen relevanten Werten
 Setzt Startwerte. Eine Re-initialisierung ist ebenfalls m�glich.
 vorhandene Monatswerte werden ber�cksichtigt
 ***********************************************************************************************************
 ***********************************************************************************************************/

static BOOL UpdateInitialAcc(const char *pcpAccountType,double dpYecuValue,const char *pcpYear,const char *pcpStfu,double *dpYelaValue)
{
	char	clValue[128];
	char	clOldOP01[128];
	double	dlOldYecuValue;
	double	dlAccValue[12];
	double	dlOP01;
	int		i;
	double	dlDeltaValue;
	double	dlNewAccValue[12];
	double	dlYede;
	double	dlOldYela;

	/*** Runden und 2 Stellen hinter dem Komma ***/
	sprintf(clValue,"%-8.2lf",dpYecuValue);
	clValue[8] = '\0';
	dpYecuValue = atof(clValue);

	/**** Endwert des Vorjahres berechnen ****/
	*dpYelaValue = GetAccYelaValueHandle(pcpAccountType,pcpYear,pcpStfu);

	dbg(DEBUG,"Vorjahreswert f�r Konto = %8.2lf",*dpYelaValue);

	/*** Bisherigen Initialwert retten ***/
	dlOldYecuValue = GetAccYecuValueHandle(pcpAccountType,pcpYear,pcpStfu,&dlYede);

	dbg(DEBUG,"-------------------------------------------------------");
	dbg(DEBUG,"Jahreswert <%s>",clValue);
	dbg(DEBUG,"bisheriger Yecu Wert:<%lf>",dlOldYecuValue);


	/*** Die Alten Monats Werte auslesen ***/
	GetMultiAccCloseValueHandle(pcpStfu,pcpAccountType,pcpYear,dlAccValue);

	/*** Alten YELA Wert ermitteln	***/
	if (GetAccValueHandle(pcpAccountType,"01",pcpYear,pcpStfu,"OP",clOldOP01))
		dlOldYela = atof(clOldOP01) - dlOldYecuValue;
	else
		dlOldYela = 0;

	/*** Setzt YECU und OP01 Wert ***/
	dlOP01 = dpYecuValue+(*dpYelaValue);

	UpdateInitAccount(dlOP01,dpYecuValue,pcpAccountType,pcpYear,pcpStfu,"XBSHDL");

	/*** Unterschied bisheriger Startwert und aktueller Startwert ***/
	dlDeltaValue = (dpYecuValue - dlOldYecuValue) + (*dpYelaValue - dlOldYela);

	dbg(DEBUG,"Delta Value <%lf>",dlDeltaValue);

	/*** Endwert des letzten Jahres und die vorherigen Eingaben und Delta ber�cksichtigen ***/
	/*** Delta ist wichtig falls es schon einen Startwert gab und das Konto re-initialisiert wird ***/

	for (i = 0;i < 12; i++)
	{
		dlNewAccValue[i] = dlAccValue[i] + dlDeltaValue;
	}

	dbg(DEBUG,"Montatswert <%lf>",dpYecuValue + (*dpYelaValue));

	NewUpdateMultiAccount(pcpAccountType,pcpStfu,pcpYear,dlNewAccValue);
	return TRUE;
}

/**************************************************************************
 **************************************************************************
 UpdateYelaValueHandle: setzt Yela Wert des neuen Jahres. Konto wird erzeugt falls noch nicht
 vorhanden. (YELA -> vorjahresendwert)
 Rueckgabe: 0 -> Erfolg
 		  <0 -> Fehler
 **************************************************************************
 **************************************************************************/
static BOOL UpdateYelaValueHandle(const char *pcpAccountType,const char *pcpYear,const char *pcpStaffUrno,const char *pcpUseu,double dpAccountValue)
{
	static BOOL blFirst = TRUE;
	static int ilAccHandle = -1;

	char	clAccountValue[128];
	Date	olDate;
	char	clTimeNowString[16];
	int		i;
	char	clAccKeyValue[256];
	char	clAccRecord[2048];
	int		ilLen;
	char	clUpdFields[256];
	char	clUpdValues[256];
	char	clAccUrno[12];
	char	clYear[6];
	char	clMonth[4];
	char	clRecord[2048];
	char	clString[1024];

	if (blFirst)
	{
		blFirst = FALSE;
		ilAccHandle = GetArrayIndex("ACCHDL");
	}

	if (ilAccHandle >= 0)
	{
		/*** Runden und 2 Stellen hinter dem Komma ***/
		sprintf(clAccountValue,"%-8.2lf",dpAccountValue);
		clAccountValue[8] = '\0';

		/*** Aktuelle Zeit als String auslesen ***/
		olDate = GetCurrentDate();
		DateToString(clTimeNowString,16,"%Y%m%d%H%M%S",&olDate);

		/*** initialize data 	****/
		strcpy(clAccKeyValue,pcpYear);
		strcat(clAccKeyValue,",");
		strcat(clAccKeyValue,pcpStaffUrno);
		strcat(clAccKeyValue,",");
		strcat(clAccKeyValue,pcpAccountType);

		dbg(DEBUG, "UpdateYelaValueHandle(): <clAccKeyValue> = <%s>",clAccKeyValue);

		/***** get first record that matches condition	*/
		strcpy(clAccRecord,clAccKeyValue);
		ilLen = FindFirstRecordByIndex(ilAccHandle,clAccRecord);
		if (ilLen > 0)
		{
			/**** Key wurde gefunden, Update wird eingeleitet ***/
			double 	dlYECU;
			double	dlYEDE;
			double	dlOP01;

			char	clYECU[128];
			char	clOP01[128];
			/***** Feldliste erzeugen ****/
			strcpy(clUpdFields,"YELA,USEU,LSTU,OP01");

			dlYECU = GetAccYecuValueHandle(pcpAccountType,pcpYear,pcpStaffUrno,&dlYEDE);
			sprintf(clYECU,"%-8.2lf",dlYECU);
			clYECU[8] = '\0';

			dlOP01 = atof(clYECU) + atof(clAccountValue);
			sprintf(clOP01,"%-8.2lf",dlOP01);
			clOP01[8] = '\0';

			/***** Wertliste erzeugen	****/
			strcpy(clUpdValues,clAccountValue);
			strcat(clUpdValues,",");
			strcat(clUpdValues,pcpUseu);
			strcat(clUpdValues,",");
			strcat(clUpdValues,clTimeNowString);
			strcat(clUpdValues,",");
			strcat(clUpdValues,clOP01);

			/***************** USEU und COxy setzen *******************************/
			dbg(TRACE,"UpdateYelaValueHandle(): Veraendere Datensatz: Felder <%s> - Werte <%s>",clUpdFields,clUpdValues);

			if (UpdateRecord(ilAccHandle,clUpdFields,clUpdValues) != 0)
			{
				dbg(DEBUG,"UpdateYelaValueHandle(): ERROR! Not possible to update record!");
				return FALSE;
			}
			else
			{
				return TRUE;
			}
		}
		else
		{
			dbg(TRACE,"UpdateYelaValueHandle(): ERROR! Not possible to create record!");
			return FALSE;
		}
	}
	else
	{
		/* array not found */
		dbg(TRACE,"UpdateYelaValueHandle:() array 'ACCHDL' not found!!!");
		return FALSE;
	}
}


/***********************************************************************************************************
 ***********************************************************************************************************
 Sollte sich der Endwert eines Konto ver�ndert haben, so m�ssen die Werte im n�chsten Jahr angepasst werden
 ***********************************************************************************************************
 ***********************************************************************************************************/

static BOOL UpdateNextYearsValuesHandle(const char *pcpAccountType,const char *pcpYear,const char *pcpStfu,double dpNewEndValue)
{
	char 	clNewEndValue[128];
	double 	dlLocalNewEndValue;
	char	clNewYear[16];
	double	dlOldYela;
	double	dlDelta;
	double	dlNewAccValue[12];
	double	dlAccValue[12];
	int	i;

	/*** Runden und 2 Stellen hinter dem Komma ***/
	sprintf(clNewEndValue,"%-8.2lf",dpNewEndValue);
	clNewEndValue[8] = '\0';

	dlLocalNewEndValue = atof(clNewEndValue);

	/*** das n�chste Jahr ermitteln ****/
	sprintf(clNewYear,"%-4d",atoi(pcpYear) + 1);

	dbg(DEBUG,"Anpassung f�r:");
	dbg(DEBUG,"Jahr :<%s>",clNewYear);
	dbg(DEBUG,"Mitarbeiter :<%s>",pcpStfu);
	dbg(DEBUG,"Endwert letztes Jahr :<%lf>",dlLocalNewEndValue);

	/*** Alten Yela Wert retten ***/
	dlOldYela = GetAccYelaValueHandle(pcpAccountType,clNewYear,pcpStfu);

	dbg(DEBUG,"Alter Yela Wert :<%lf>",dlOldYela);

	/*** Abweichung berechnen ***/
	dlDelta = dlLocalNewEndValue - dlOldYela;

	dbg(DEBUG,"Abweichung :<%-8.2lf>",dlDelta);

	/*** Das Konto im n�chsten Jahr um diesen Wert ver�ndern (jeden Monat) ***/
	GetMultiAccCloseValueHandle(pcpStfu,pcpAccountType,clNewYear,dlNewAccValue);

	/*** Differenz anwenden ***/
	for (i = 0; i < 12; i++)
	{
		dlAccValue[i] = dlNewAccValue[i];
		dlNewAccValue[i] += dlDelta;
		dbg(DEBUG,"Monat :<%d> <%-8.2lf> <%8.2lf>",i,dlAccValue[i],dlNewAccValue[i]);
	}

	/*** Abspeichern ***/
	UpdateMultiAccountHandle(pcpAccountType,pcpStfu,clNewYear,"SCBHDL",dlNewAccValue);

	dbg(DEBUG,"Setzte YELA auf:<%-8.2lf>",dlLocalNewEndValue);

	UpdateYelaValueHandle(pcpAccountType,clNewYear,pcpStfu,"SCBHDL",dlLocalNewEndValue);

	return TRUE;
}

static BOOL UpdateNextYearsValuesHandleAdjust(const char *pcpYear,const char *pcpStfu,double dpNewEndValue1,double dpNewEndValue2,const char *pcpAcc1,const char *pcpAcc2)
{
	char	clNewYear[6];
	char	clNewValue1[128];
	char	clNewValue2[128];
	double	dlDiff;
	double 	dlOldYela1;
	double 	dlOldYela2;
	double 	dlDelta1;
	double 	dlDelta2;

	double	dlAccValue[12];
	double	dlNewAccValue[12];

	int i;

	dlDiff = dpNewEndValue1 - dpNewEndValue2;

	/*** gegeneinander verrechnen	***/
	if (dlDiff > 0)
	{
		sprintf(clNewValue1,"%-8.2lf",dlDiff);
		clNewValue1[8] = '\0';
		strcpy(clNewValue2,"0");
	}
	else if (dlDiff < 0)
	{
		dlDiff *= -1;
		strcpy(clNewValue1,"0");
		sprintf(clNewValue2,"%-8.2lf",dlDiff);
		clNewValue2[8] = '\0';
	}
	else
	{
		strcpy(clNewValue1,"0");
		strcpy(clNewValue2,"0");
	}

	/*** das n�chste Jahr ermitteln ****/
	sprintf(clNewYear,"%-4d",atoi(pcpYear) + 1);

	dbg(DEBUG, "UpdateNextYearsValuesHandleAdjust(): Adaption of...");
	dbg(DEBUG, "UpdateNextYearsValuesHandleAdjust(): Year = <%s>",clNewYear);
	dbg(DEBUG, "UpdateNextYearsValuesHandleAdjust(): EmployeeUrno = <%s>",pcpStfu);
	dbg(DEBUG, "UpdateNextYearsValuesHandleAdjust(): dpNewEndValue1 = <%lf>",dpNewEndValue1);
	dbg(DEBUG, "UpdateNextYearsValuesHandleAdjust(): dpNewEndValue2 = <%lf>",dpNewEndValue2);
	dbg(DEBUG, "UpdateNextYearsValuesHandleAdjust(): clNewValue1    = <%s>",clNewValue1);
	dbg(DEBUG, "UpdateNextYearsValuesHandleAdjust(): clNewValue2    = <%s>",clNewValue2);

	/*** Das Konto varAcc1 im n�chsten Jahr updaten (jeden Monat) ***/
	/*** Alten Yela Wert retten ***/
	dlOldYela1 = GetAccYelaValueHandle(pcpAcc1,clNewYear,pcpStfu);

	/*** Abweichung berechnen ***/
	dlDelta1 = atof(clNewValue1) - dlOldYela1;

	/*** CL-Werte holen	****/
	GetMultiAccCloseValueHandle(pcpStfu,pcpAcc1,clNewYear,dlAccValue);

	/*** Differenz anwenden ***/
	for (i = 0; i < 12; i++)
	{
		dlNewAccValue[i] = dlAccValue[i] + dlDelta1;
		dbg(DEBUG,"UpdateNextYearsValuesHandleAdjust(): Month <%d>, Old <%lf>, New <%lf>",i+1,dlAccValue[i],dlNewAccValue[i]);
	}

	UpdateMultiAccountHandle(pcpAcc1,pcpStfu,clNewYear,"SCBHDL",dlNewAccValue);
	UpdateYelaValueHandle(pcpAcc1,clNewYear,pcpStfu,"SCBHDL",atof(clNewValue1));

	/*** Das Konto varAcc2 im n�chsten Jahr updaten (jeden Monat) ***/
	/*** Alten Yela Wert retten ***/
	dlOldYela2 = GetAccYelaValueHandle(pcpAcc2,clNewYear,pcpStfu);

	/*** Abweichung berechnen ***/
	dlDelta2 = atof(clNewValue2) - dlOldYela2;

	/*** CL-Werte holen	***/
	GetMultiAccCloseValueHandle(pcpStfu,pcpAcc2,clNewYear,dlAccValue);

	/*** Differenz anwenden ***/
	for (i = 0; i < 12; i++)
	{
		dlNewAccValue[i] = dlAccValue[i] + dlDelta2;
		dbg(DEBUG,"UpdateNextYearsValuesHandleAdjust(): Month <%d>, Old <%lf>, New <%lf>",i+1,dlAccValue[i],dlNewAccValue[i]);
	}

	UpdateMultiAccountHandle(pcpAcc2,pcpStfu,clNewYear,"SCBHDL",dlNewAccValue);
	UpdateYelaValueHandle(pcpAcc2,clNewYear,pcpStfu,"SCBHDL",atof(clNewValue2));

	return TRUE;
}

/*****************************************/
/* CalcNightWorkTime			 */
/* Rueckgabe: time in night shift window */
/*****************************************/
double CalcNightWorkTime(const char *pcpStartTime,const char *pcpEndTime,const char* pcpIsBsd,const char* pcpIsLikeWork)
{
	char	clStartTime[16];
	char	clEndTime[16];
	char	clStartHour[4];
	char	clStartMin[4];
	char	clEndHour[4];
	char	clEndMin[4];

	char	clSdoStart[66];
	char	clSdoEnd[66];
	char	clSdoStartHour[4];
	char	clSdoStartMin[4];
	char	clSdoEndHour[4];
	char	clSdoEndMin[4];

	double	dlStartTimeInMin = 0;
	double	dlEndTimeInMin = 0;
	double	dlSdoStart = 0;
	double	dlSdoEnd = 0;

	double dlDiffTime = 0;
	double dlTmpTime = 0;
	if (strcmp(pcpIsBsd,"1") != 0 && strcmp(pcpIsLikeWork,"1") != NULL)
	{
		dbg(DEBUG,"CalcNightWorkTime(): No shift or absence is not handled like a shift! Return 0!");
		return 0;
	}

	strcpy(clStartTime,pcpStartTime);
	strcpy(clEndTime,pcpEndTime);

	dbg(DEBUG,"CalcNightWorkTime(): <pcpStartTime> = <%s>",pcpStartTime);
	dbg(DEBUG,"CalcNightWorkTime(): <pcpEndTime> = <%s>",pcpEndTime);

	if (strcmp(pcpStartTime,pcpEndTime) == 0)
	{
		dbg(DEBUG,"CalcNightWorkTime(): Start- and endtime are the same. Duration is 0.");
		return 0;
	}

	/* get start time values */
	strncpy(clStartHour,&clStartTime[8],2);
	clStartHour[2] = '\0';
	strncpy(clStartMin,&clStartTime[10],2);
	clStartMin[2] = '\0';

	/* get end time values */
	strncpy(clEndHour,&clEndTime[8],2);
	clEndHour[2] = '\0';
	strncpy(clEndMin,&clEndTime[10],2);
	clEndMin[2] = '\0';

	dbg(DEBUG,"CalcNightWorkTime(): Start time: <%s:%s>",clStartHour,clStartMin);
	dbg(DEBUG,"CalcNightWorkTime(): End time:   <%s:%s>",clEndHour,clEndMin);

	/* times in minutes */
	dlStartTimeInMin = atof(clStartHour) * 60 + atof(clStartMin);
	dlEndTimeInMin = atof(clEndHour) * 60 + atof(clEndMin);
	if (dlEndTimeInMin < 0)
	{
		dlEndTimeInMin = 1440;
	}

	dbg(DEBUG,"CalcNightWorkTime(): Start time minutes from 00:00h <dlStartTimeInMin> = <%lf>",dlStartTimeInMin);
	dbg(DEBUG,"CalcNightWorkTime(): End time minutes from 00:00h <dlEndTimeInMin> = <%lf>",dlEndTimeInMin);

	/* get parameter for night working time end */
	if (GetPartabValue("ID_NIGHT_START",clSdoStart) == FALSE)
	{
		dlSdoStart = 1200; /* 20:00 */
	}
	else
	{
		strncpy(clSdoStartHour,clSdoStart,2);
		clSdoStartHour[2] = '\0';
		strncpy(clSdoStartMin,&clSdoStart[3],2);
		clSdoStartMin[2] = '\0';
		dlSdoStart = atoi(clSdoStartHour) * 60 + atoi(clSdoStartMin);
	}

	/* get parameter for night working time start */
	if (GetPartabValue("ID_NIGHT_END",clSdoEnd) == FALSE)
	{
		dlSdoEnd = 360; /* 06:00 */
	}
	else
	{
		strncpy(clSdoEndHour,clSdoEnd,2);
		clSdoEndHour[2] = '\0';
		strncpy(clSdoEndMin,&clSdoEnd[3],2);
		clSdoEndMin[2] = '\0';
		dlSdoEnd = atoi(clSdoEndHour) * 60 + atoi(clSdoEndMin);
	}

	dbg(DEBUG,"CalcNightWorkTime(): Parameter night working time start: <%lf>",dlSdoStart);
	dbg(DEBUG,"CalcNightWorkTime(): Parameter night working time end: <%lf>" ,dlSdoEnd);

	if (dlStartTimeInMin < dlEndTimeInMin)
	{
		dbg(DEBUG,"CalcNightWorkTime(): Start time and end time are on the same day.");
		if (dlStartTimeInMin < dlSdoEnd)
		{
			if (dlEndTimeInMin <= dlSdoEnd)
			{
				dlTmpTime = dlEndTimeInMin - dlStartTimeInMin;
				dbg(DEBUG,"CalcNightWorkTime(): Complete time between <00:00> and <%s:%s>",clSdoEndHour,clSdoEndMin);
				dbg(DEBUG,"CalcNightWorkTime(): Result night minutes: <%lf>",dlTmpTime);
				return dlTmpTime / 60;
			}
			else
			{
				dlTmpTime = dlSdoEnd - dlStartTimeInMin;
				dlDiffTime = dlDiffTime + dlTmpTime;
				dbg(DEBUG,"CalcNightWorkTime(): Night minutes in the morning: <%lf>",dlTmpTime);
			}
		}
		else if (dlStartTimeInMin >= dlSdoStart)
		{
			dlTmpTime = dlEndTimeInMin - dlStartTimeInMin;
			dbg(DEBUG,"CalcNightWorkTime(): Complete time between <%s:%s> and <24:00>",clSdoStartHour,clSdoStartMin);
			dbg(DEBUG,"CalcNightWorkTime(): Result night minutes: <%lf>",dlTmpTime);
			return dlTmpTime / 60;
		}

		if (dlEndTimeInMin > dlSdoStart)
		{
			dlTmpTime = dlEndTimeInMin - dlSdoStart;
			dlDiffTime = dlDiffTime + dlTmpTime;
			dbg(DEBUG,"CalcNightWorkTime(): Night minutes in the evening: <%lf>",dlTmpTime);
		}
	}
	else
	{
		dbg(DEBUG,"CalcNightWorkTime(): Start time and end time are on different days.");
		if (dlStartTimeInMin < dlSdoEnd)
		{
			dlTmpTime = dlSdoEnd - dlStartTimeInMin;
			dlDiffTime = dlDiffTime + dlTmpTime;
			dbg(DEBUG,"CalcNightWorkTime(): Night minutes in the morning of the first day: <%lf>",dlTmpTime);
		}

		if (dlStartTimeInMin <= dlSdoStart)
		{
			dlTmpTime = 1440 - dlSdoStart;
		}
		else
		{
			dlTmpTime = 1440 - dlStartTimeInMin;
		}
		dlDiffTime = dlDiffTime + dlTmpTime;
		dbg(DEBUG,"CalcNightWorkTime(): Night minutes in the evening of the first day: <%lf>",dlTmpTime);

		if (dlEndTimeInMin < dlSdoEnd)
		{
			dlTmpTime = dlEndTimeInMin;
		}
		else
		{
			dlTmpTime = dlSdoEnd;
		}
		dlDiffTime = dlDiffTime + dlTmpTime;
		dbg(DEBUG,"CalcNightWorkTime(): Night minutes in the morning of the second day: <%lf>",dlTmpTime);
	}

	dbg(DEBUG,"CalcNightWorkTime(): Result night minutes: <%lf>",dlDiffTime);
	return dlDiffTime / 60;
}

/********************************************************/
/* CalcBreakNightWorkTime				*/
/* Rueckgabe: Time of the break within night timeframe  */
/* ******************************************************/
double CalcBreakNightWorkTime(const char *pcpBreakStartTime,const char *pcpBreakDuration,const char *pcpSday)
{
	double	dlValue = 0;
	char	clBreakEndTime[16];
	char	clTmp[2];

	/* check break start time */
	if (strlen(pcpBreakStartTime) == 0)
	{
		dbg(DEBUG,"CalcBreakNightWorkTime(): No valid break start time found! Return 0.");
		return 0;
	}

	dbg(DEBUG,"CalcBreakNightWorkTime(): Break start: <%s>",pcpBreakStartTime);
	dbg(DEBUG,"CalcBreakNightWorkTime(): Break duration: <%s>",pcpBreakDuration);

	/* calculate break end time */
	strncpy(clBreakEndTime,pcpBreakStartTime,12);
	clBreakEndTime[12] = '\0';
	strcat(clBreakEndTime,"00");
	AddSecondsToCEDATime2(clBreakEndTime,atoi(pcpBreakDuration) * 60,"LOC",1);
	dbg(DEBUG,"CalcBreakNightWorkTime(): Break end: <%s>",clBreakEndTime);

	/* get the minutes of the break within night timeframe */
	strcpy(clTmp,"1");
	dlValue = CalcNightWorkTime(pcpBreakStartTime,clBreakEndTime,clTmp,clTmp);
	dbg(DEBUG,"CalcBreakNightWorkTime(): Night minutes of break: <%lf>",dlValue);

	return dlValue;
}

static void HandlePrefetchArray(char* pcpEmployees, char* pclYearFrom, char* pclYearTo, int ipAddRemove)
{
	int i;
	int ilCountEmpl;
	int	ilStfuAdd = 0;
	int ilMaxUrnoSize;
	char* pclEmployees;
	char* pclStfuList;
	char clCurrentEmpl[12];
	char clMaxUrnoSize[128];

	if (ReadTemporaryField("MaxUrnoSize",clMaxUrnoSize) < 0)
	{
		ilMaxUrnoSize = 75;
	}
	else
	{
		ilMaxUrnoSize = atoi(clMaxUrnoSize);
	}

	pclEmployees = pcpEmployees;
	ilCountEmpl = CountElements(pclEmployees);
	pclStfuList = (char*)calloc(1,strlen(pclEmployees)+2);

	/* add staff to prefetch array */
	if (ilCountEmpl <= ilMaxUrnoSize)
	{
		ilStfuAdd  = ilCountEmpl;
		strcpy(pclStfuList,pclEmployees);
	}
	else
	{
		ilStfuAdd = 0;
		/* step through all employees */
		for (i = 0; i < ilCountEmpl; i++)
		{
			GetItem(pclEmployees,i,",",clCurrentEmpl);
			if (strlen(clCurrentEmpl) == 0)
			{
				dbg(TRACE,"HandlePrefetchArray()(): Stopped, bacause <clCurrentEmpl> = undef!");
				return;
			}

			if (ilStfuAdd == 0)
			{
				strcpy(pclStfuList,clCurrentEmpl);
			}
			else
			{
				strcat(pclStfuList,",");
				strcat(pclStfuList,clCurrentEmpl);
			}

			ilStfuAdd++;

			if (ilStfuAdd == ilMaxUrnoSize)
			{
				/* add employees to prefetch array */
				StfToAccPrefetch(pclStfuList,pclYearFrom,ipAddRemove);
				StfToAccPrefetch(pclStfuList,pclYearTo,ipAddRemove);
				ilStfuAdd = 0;
			}
		}
	}
	if (ilStfuAdd > 0)
	{
		/* add (rest) employees to prefetch array */
		StfToAccPrefetch(pclStfuList,pclYearFrom,ipAddRemove);
		StfToAccPrefetch(pclStfuList,pclYearTo,ipAddRemove);
	}

	if (pclStfuList)
		free (pclStfuList);
}
