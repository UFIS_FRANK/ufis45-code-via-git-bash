#ifndef _DEF_mks_version
  #define _DEF_mks_version
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version[] = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Rms/rdphdl.c 1.23 2010/10/19 15:29:31SGT ble Exp  $";
#endif /* _DEF_mks_version */
/******************************************************************************/
/*                                                                            */
/* ABB ACE/FC Program Skeleton                                                */
/*                                                                            */
/* Author         :  MCU                                                      */
/* Date           :  11.01.2002                                               */
/* Description    :  RDPHDL release staff data                                */
/*                                                                            */
/* Update history :                                                           */
/* MCU 20020327   : PlanFlag other than 'D' is still done by RELDPL           */
/* MCU 20020429   : Corrections concerning releas of level 1 to level 2       */
/* RRO 20020910   : 1) Correction concerning the weekday-configuration        */
/*                  2) correcting DELRELTABOFFSET-calculation (* 86400)       */ 
/* RRO/MCU 020920 : release only staff which is really employed for the       */
/*                  shift day (checked by DODM/DOEM from STFTAB               */
/* RRO 20020930   : imrpoving DB-access performance                           */
/* MEI 20101015   : Bug in v1.22, REL_SECTIONS was not initialized            */
/*                                                                            */
/*                                                                            */
/******************************************************************************/

/* be carefule with strftime or similar functions !!!                         */
/*                                                                            */
/******************************************************************************/
/* This program is a MIKE main program */


#define U_MAIN
#define UGCCS_PRG
#define STH_USE
#include <stdio.h>
#include <malloc.h>
#include <string.h>
#include <errno.h>
#include <signal.h>
#include "ugccsma.h"
#include "msgno.h"
#include "glbdef.h"
#include "quedef.h"
#include "uevent.h"
#include "sthdef.h"
#include "debugrec.h"
#include "hsbsub.h"
#include "fditools.h"
#include "libccstr.h"
#include "l_ccstime.h"
/*#include "rmschkrul.h"*/
#include "syslib.h"
#include "AATArray.h"
#include "db_if.h"
#include <time.h>
#include <cedatime.h>

#define RELFIELD(pcpBuf,ipFieldNo) (&pcpBuf[rgRelArray.plrArrayFieldOfs[ipFieldNo]])
#define DRRFIELD(pcpBuf,ipFieldNo) (&pcpBuf[rgDrrArray.plrArrayFieldOfs[ipFieldNo]])
#define NEWDRRFIELD(pcpBuf,ipFieldNo) (&pcpBuf[rgNewDrrArray.plrArrayFieldOfs[ipFieldNo]])
#define FIELDVAL(rrArray,pcpBuf,ipFieldNo) (&pcpBuf[rrArray.plrArrayFieldOfs[ipFieldNo]])
#define PFIELDVAL(prArray,pcpBuf,ipFieldNo) (&pcpBuf[prArray->plrArrayFieldOfs[ipFieldNo]])
#define STFFIELD(pcpBuf,ipFieldNo) (&pcpBuf[rgStfArray.plrArrayFieldOfs[ipFieldNo]])


#define CMD_RDL     (0)
#define CMD_RDN     (1)
#define CMD_RDS     (2)
#define CMD_RDT     (3)
#define CMD_RDD     (4)
#define CMD_IRT     (5)
#define CMD_URT     (6)
#define CMD_DRT     (7)

#define IDX_IRTD 4  /* Index of List "INSERT" in prgDemInf->DataList */
#define IDX_URTD 5  /* Index of List "UPDATE" in prgDemInf->DataList */
#define IDX_DRTU 6  /* Index of List "DELETE" in prgDemInf->DataList */
#define IDX_IRTU 7  /* Index of List "IRT_URNO" in prgDemInf->DataList */
#define IDX_URTU 8  /* Index of List "URT_URNO" in prgDemInf->DataList */
#define IDX_DRTD 9  /* Index of List "DRT_DATA" in prgDemInf->DataList */

static int rgUIdx[3] = { IDX_IRTU, IDX_URTU, IDX_DRTU };


static char *prgCmdTxt[] = {"RDL","RDN","RDS","RDT","RDD","IRT","URT","DRT",NULL };
static int    rgCmdDef[] = {CMD_RDL,CMD_RDN,CMD_RDS,CMD_RDT,CMD_RDD,CMD_IRT,CMD_URT,CMD_DRT,0 };

static char *cgWeekDays[8] = {
    "SO","MO","DI","MI","DO","FR","SA","SO" };
static char *cgWeekDaysEng[8] = {
    "SU","MO","TU","WE","TH","FR","SA","SU" };
static int igTestOutput = 9;

/******************************************************************************/
/***   Fields                                                             *****/
/******************************************************************************/


#define REL_FIELDS "CDAT,DORS,HOPO,LSTU,RLAT,RLFR,RLTO,ROSL,SJOB,SPLU,STFU,URNO,USEC,USEU"

/***************
#define DRR_FIELDS "AVFR,AVTO,BKDP,BSDU,BUFU,CDAT,DRRN,DRS1,DRS2,DRS3,DRS4,"\
           "DRSF,EXPF,FCTC,HOPO,LSTU,REMA,ROSL,ROSS,SBFR,SBLP,SBLU,"\
           "SBTO,SCOD,SCOO,SDAY,STFU,URNO,USEC,USEU"
*****************/

#define DRR_FIELDS "AVFR,AVTO,BKDP,BSDU,BUFU,CDAT,DRRN,DRS1,DRS2,DRS3,DRS4,"\
           "DRSF,EXPF,FCTC,HOPO,REMA,ROSL,ROSS,SBFR,SBLP,SBLU,"\
           "SBTO,SCOD,SCOO,SDAY,STFU,URNO,USEC,USEU"
static char cgDrrFields[256] = "";
static char cgDrrReloadHint[256];

#define ADD_DRR_FIELDS "USED,DUMY"

#define ODA_FIELDS "URNO,SDAC"
#define STF_FIELDS "URNO,DOEM,DODM"

static char cgStfFields[256] = STF_FIELDS;

static AAT_PARAM *prgRdpInf=0;

/******************************************************************************/
/* External variables                                                         */
/******************************************************************************/
/* outp is defined in ugccsma.h! double definitions kill */
/* your process under HP-UX immediatly !!!!              */
/*FILE *outp       = NULL;*/
int  debug_level = TRACE;

/******************************************************************************/
/* Global variables                                                           */
/******************************************************************************/
static ITEM  *prgItem      = NULL;                /* The queue item pointer  */
static EVENT *prgEvent     = NULL;                /* The event pointer       */
static int   igItemLen     = 0;                   /* length of incoming item */
static EVENT *prgOutEvent  = NULL;
static int   igInitOK      = FALSE;
static long  lgEvtCnt      = 0;
static char  cgConfigFile[512];
static char  cgHopo[8];                         /* default home airport    */
static char  cgTabEnd[8];                       /* default table extension */
static  int  igStartUpMode = TRACE;
static  int  igRuntimeMode = 0;
static char  cgProcessName[80];                  /* name of executable */

static char cgCmdBuf[240];
static char cgSelection[50*1024];
static char cgFldBuf[8196];
static char cgSqlBuf[5*1024];
static char cgTwStart[128];
static char cgTwEnd[128];
static char cgRecvName[128];
static char cgDestName[128];
static int igQueOut;

static int igReltabOffset;

static char cgRelBuf[1024];
static char cgDrrBuf[2048];

static char cgErrMsg[1024];

static char cgCfgBuffer[256];

static char cgRelSequence[124];
static char cgRelSections[256];
static int  igSections;
static char cgReltabStfu[50*1024];
static char cgDataArea[5*1024];

static BOOL bgBcActiv = TRUE;
static BOOL bgBreak = FALSE;
static BOOL bgSendBroadcast = TRUE;
static BOOL bgSend2Action = TRUE;
static int  igBchdlModid = 1900;
static int  igActionModid = 7400; 
static BOOL bgUseFilt = FALSE;

static BOOL bgBROS = FALSE;
static BOOL bgOABS = FALSE;

static char cgUser[48] = "";

/* static array informations */

#define ARR_NAME_LEN   (18)
#define ARR_FLDLST_LEN (256)

struct _configSecData
{
    char  cPeriod[26];
    char  cSequence[256+1];
    char  cLevel[3+1];
    char  cDuration[3+1];
    char  cType[23+1];
    char  cOffset[3+1];
        char  cSectionName[124];
};
typedef struct _configSecData CFGDATA;
static CFGDATA rgCfgData[256] ; 
 

struct _RelData
{
    char cUsec[32];
    char cSectionName[32];
    char cPlanFlag[4];
    char cHopo[8];
    char cPlanStufe[6];
    char cDateFrTo[18];
    char pcUrnoList[50*1024];
    char pcUrnoList2[50*1024];

};

typedef struct _RelData RELDATA;
static RELDATA rgRelData;

static char cgPlanStufeOld[4];
static char cgPlanStufeNew[4];

struct _arrayinfo {
    HANDLE   rrArrayHandle;
    char     crArrayName[ARR_NAME_LEN+1];
    char     crArrayFieldList[ARR_FLDLST_LEN+1];
    long     lrArrayFieldCnt;
    char   *pcrArrayRowBuf;
    long     lrArrayRowLen;
    long   *plrArrayFieldOfs;
    long   *plrArrayFieldLen;
    HANDLE   rrIdx01Handle;
    char     crIdx01Name[ARR_NAME_LEN+1];
    char     crIdx01FieldList[ARR_FLDLST_LEN+1];
    long     lrIdx01FieldCnt;
    char   *pcrIdx01RowBuf;
    long     lrIdx01RowLen;
    long   *plrIdx01FieldPos;
    long   *plrIdx01FieldOrd;

/*****************/
    HANDLE   rrIdx02Handle;
    char     crIdx02Name[ARR_NAME_LEN+1];
    char     crIdx02FieldList[ARR_FLDLST_LEN+1];
    long     lrIdx02FieldCnt;
    char   *pcrIdx02RowBuf;
    long     lrIdx02RowLen;
    long   *plrIdx02FieldPos;
    long   *plrIdx02FieldOrd;
    /****************************/
    HANDLE   rrIdx03Handle;
    char     crIdx03Name[ARR_NAME_LEN+1];
    char     crIdx03FieldList[ARR_FLDLST_LEN+1];
    long     lrIdx03FieldCnt;
    char   *pcrIdx03RowBuf;
    long     lrIdx03RowLen;
    long   *plrIdx03FieldPos;
    long   *plrIdx03FieldOrd;
};
typedef struct _arrayinfo ARRAYINFO;

static ARRAYINFO rgRelArray;
static ARRAYINFO rgDrrArray;
static ARRAYINFO rgNewDrrArray;
static ARRAYINFO rgOdaArray;
static ARRAYINFO rgStfArray;


static char *pcgDrsArrayName;
static char *pcgDrsFields;
HANDLE      sgDrsInfo = 0;
char        pcgDrsTab[10];

#define REL_MAXFIELDS   100
long        lgDrsFldLen[REL_MAXFIELDS];



static int *pigRelKey = NULL;
static int *pigDrrKey = NULL;

static int igRelCDAT;
static int igRelDORS;
static int igRelHOPO;
static int igRelLSTU;
static int igRelRLAT;
static int igRelRLFR;
static int igRelRLTO;
static int igRelROSL;
static int igRelSJOB;
static int igRelSPLU;
static int igRelSTFU;
static int igRelURNO;
static int igRelUSEC;
static int igRelUSEU;

static int igDrrAVFR;
static int igDrrAVTO;
static int igDrrBKDP;
static int igDrrBSDU;
static int igDrrBUFU;
static int igDrrCDAT;
static int igDrrDRRN;
static int igDrrDRS1;
static int igDrrDRS2;
static int igDrrDRS3;
static int igDrrDRS4;
static int igDrrDRSF;
static int igDrrEXPF;
static int igDrrFCTC;
static int igDrrHOPO;
/*static int igDrrLSTU;*/
static int igDrrREMA;
static int igDrrROSL;
static int igDrrROSS;
static int igDrrSBFR;
static int igDrrSBLU;
static int igDrrSBLP;
static int igDrrSBTO;
static int igDrrSCOD;
static int igDrrSCOO;
static int igDrrSDAY;
static int igDrrSTFU;
static int igDrrURNO;
static int igDrrUSEC;
static int igDrrUSEU;
static int igDrrUSED;
static int igDrrFILT;
static int igStfURNO;
static int igStfDODM;
static int igStfDOEM;


/* used only when field BROS is in DRRTAB */
static int igDrrBROS;
/* used only when field OABS is in DRRTAB */
static int igDrrOABS;



/******************************************************************************/
/* Function prototypes                                                        */
/******************************************************************************/
int check_ret1(int ipRc,int ipLine);
static int  InitRdphdl();

static long GetLengtOfMonth(int ipMonth,int ipYear);

static int HandleDrrDataNow(char *pclData,char *pcpNextRosl,char *pcpJob);

static int ArrayFindRow(HANDLE * pspHandle, char *pcpArrayName, char *pcgFieldName, char *pcPattern, long * Row);

static int SetArrayInfo(char *pcpArrayName, char *pcpTableName, 
        char *pcpArrayFieldList,char *pcpAddFields,long *pcpAddFieldLens,
            ARRAYINFO *prpArrayInfo,char *pcpSelection,BOOL bpFill);
static int ReadConfigEntry(char *pcpSection,char *pcpKeyword,
    char *pcpCfgBuffer);

/***
static int ExtractItem(int ilItemNo, char* pcpSource, 
                                     char* pcpTarget, char cpDelim,int ilMaxLen);
***/

static int GetItemNbr(char *Item, char *Data, char Delim, long Nbr);
static int ItemCnt(char *pcgData, char cpDelim);

static int SendAnswer(int ipDest, char *pcpAnswer);

extern int get_no_of_items(char *s);
extern int itemCount(char *);

static int  Reset(void);                       /* Reset program          */
static void Terminate(int ipSleep);            /* Terminate program      */
/*static void   HandleSignal(int); */                /* Handles signals        */
static void HandleErr(int);                    /* Handles general errors */
static void HandleQueErr(int);                 /* Handles queuing errors */
static void HandleQueues(void);                /* Waiting for Sts.-switch*/

static void TrimRight(char *pcpBuffer);
static int  GetFieldLength(char *pcpHopo, char *pcpTana, char *pcpFina,
                            long *pipLen);
static int  GetRowLength(char *pcpHopo, char *pcpTana, char *pcpFieldList,
                            long *pipLen);


static int  GetCommand(char *pcpCommand, int *pipCmd);

static int HandleData(EVENT *prpEvent);       /* Handles event data     */
static int SaveIndexInfo (ARRAYINFO *prpArrayInfo);
static int MySaveIndexInfo (ARRAYINFO *prpArrayInfo);

extern int  AddSecondsToCEDATime(char *, time_t, int);                   

static int StrToTime(char *pcpTime,time_t *pclTime);
static void  StrAddTime(char *,char*,char *);
static int LocalToUtc(char *pcpTime);
static int MyNewToolsSendSql(int ipRouteID,
        BC_HEAD *prpBchd,
        CMDBLK *prpCmdblk,
        char *pcpSelection,
        char *pcpFields,
        char *pcpData);
static int  TriggerAction(char *pcpTableName);
static int  WaitAndCheckQueue(int ipTimeout, int ipModId, ITEM **prpItem);
static int  CheckQueue(int ipModId, ITEM **prpItem);
static int FindItemInArrayList(char *pcpInput, char *pcpPattern,char cpDel,int *ipLine,int *ipCol,int *ipPos);
static int GetUrnoFromSelection(char *pcpSelection,char *pcpUrno);

static int  GetDebugLevel(char *, int *);
static int GetRealTimeDiff ( char *pcpLocal1, char *pcpLocal2, time_t *plpDiff );
static int DumpArray(ARRAYINFO *prpArray,char *pcpFields);

static int GetNextRoslFromSequence(char*,char,char*,char); 
static int UpdateRELTABAfterRelease(char *pcpJob,char *pcpRlat);

static int CopyDRS_DRRU(char *, char *);
static int InitDRSArray();

static int TimeToStr(char *pcpTime,time_t lpTime)
{
  struct tm *_tm;
 
  _tm = (struct tm *)localtime(&lpTime);
  sprintf(pcpTime,"%4d%02d%02d%02d%02d%02d",
      _tm->tm_year+1900,_tm->tm_mon+1,_tm->tm_mday,_tm->tm_hour,
      _tm->tm_min,_tm->tm_sec);
  return (_tm->tm_wday == 0 ? 7 : _tm->tm_wday) + '0';
}                                                                                

/******************************************************************************/
/*                                                                            */
/* The MAIN program                                                           */
/*                                                                            */
/******************************************************************************/
MAIN
{
    int ilRc = RC_SUCCESS;          /* Return code          */
    int ilCnt = 0;
    int ilOldDebugLevel;

    /************************
    FILE *prlErrFile = NULL;
    char clErrFile[512];
    ************************/

    INITIALIZE;         /* General initialization   */

    /****************************************************************************
    sprintf(&clErrFile[0],"%s/%s%5.5d.err",getenv("DBG_PATH"),mod_name,getpid());
    prlErrFile = freopen(&clErrFile[0],"w",stderr);
    fprintf(stderr,"HoHoHo\n");fflush(stderr);
    dbg(TRACE,"MAIN: error file <%s>",&clErrFile[0]);
    ****************************************************************************/


    strcpy(cgProcessName,argv[0]);

    dbg(TRACE,"MAIN: version <%s>",mks_version);

    /* Attach to the MIKE queues */
    do
    {
        ilRc = init_que();
        if(ilRc != RC_SUCCESS)
        {
            dbg(TRACE,"MAIN: init_que() failed! waiting 6 sec ...");
            sleep(6);
            ilCnt++;
        }/* end of if */
    }while((ilCnt < 10) && (ilRc != RC_SUCCESS));

    if(ilRc != RC_SUCCESS)
    {
        dbg(TRACE,"MAIN: init_que() failed! waiting 60 sec ...");
        sleep(60);
        exit(1);
    }else{
        dbg(TRACE,"MAIN: init_que() OK! mod_id <%d>",mod_id);
    }/* end of if */

    do
    {
        ilRc = init_db();
        if (ilRc != RC_SUCCESS)
        {
            check_ret(ilRc);
            dbg(TRACE,"MAIN: init_db() failed! waiting 6 sec ...");
            sleep(6);
            ilCnt++;
        } /* end of if */
    } while((ilCnt < 10) && (ilRc != RC_SUCCESS));

    if(ilRc != RC_SUCCESS)
    {
        dbg(TRACE,"MAIN: init_db() failed! waiting 60 sec ...");
        sleep(60);
        exit(2);
    }else{
        dbg(TRACE,"MAIN: init_db() OK!");
    } /* end of if */

    /* logon to DB is ok, but do NOT use DB while ctrl_sta == HSB_COMING_UP !!! */

    sprintf(cgConfigFile,"%s/%s",getenv("BIN_PATH"),mod_name);
    dbg(TRACE,"ConfigFile <%s>",cgConfigFile);
    ilRc = TransferFile(cgConfigFile);
    if(ilRc != RC_SUCCESS)
    {
        dbg(TRACE,"MAIN: TransferFile(%s) failed!",cgConfigFile);
    } /* end of if */

    sprintf(cgConfigFile,"%s/%s.cfg",getenv("CFG_PATH"),mod_name);
    dbg(TRACE,"ConfigFile <%s>",cgConfigFile);
    ilRc = TransferFile(cgConfigFile);
    if(ilRc != RC_SUCCESS)
    { 
        dbg(TRACE,"MAIN: TransferFile(%s) failed!",cgConfigFile);
    } /* end of if */

    ilRc = SendRemoteShutdown(mod_id);
    if(ilRc != RC_SUCCESS)
    {
        dbg(TRACE,"MAIN: SendRemoteShutdown(%d) failed!",mod_id);
    } /* end of if */

    if((ctrl_sta != HSB_STANDALONE) && (ctrl_sta != HSB_ACTIVE) && (ctrl_sta != HSB_ACT_TO_SBY))
    {
        dbg(DEBUG,"MAIN: waiting for status switch ...");
        HandleQueues();
    }/* end of if */

    if((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE) || (ctrl_sta == HSB_ACT_TO_SBY))
    {
        dbg(TRACE,"MAIN: initializing ...");
        if(igInitOK == FALSE)
        {
            ilRc = InitRdphdl();
            if(ilRc != RC_SUCCESS)
            {
                dbg(TRACE,"InitRdphdl: init failed!");
            } /* end of if */
        }/* end of if */
    } else {
        Terminate(30);
    }/* end of if */

/***
    dbg(TRACE,"MAIN: initializing OK sleep(60) ...");
    sleep(60);
***/

    dbg(TRACE,"=====================");
    dbg(TRACE,"MAIN: initializing OK");
    dbg(TRACE,"=====================");
    
    debug_level = igRuntimeMode;

    for(;;)
    {
        ilRc = que(QUE_GETBIG,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);
        /* depending on the size of the received item  */
        /* a realloc could be made by the que function */
        /* so do never forget to set event pointer !!! */
        prgEvent = (EVENT *) prgItem->text;
                
        if( ilRc == RC_SUCCESS )
        {
            /* Acknowledge the item */
            ilRc = que(QUE_ACK,0,mod_id,0,0,NULL);
            if( ilRc != RC_SUCCESS ) 
            {
                /* handle que_ack error */
                HandleQueErr(ilRc);
            } /* fi */
            
            lgEvtCnt++;

            switch( prgEvent->command )
            {
            case    HSB_STANDBY :
                ctrl_sta = prgEvent->command;
                HandleQueues();
                break;  
            case    HSB_COMING_UP   :
                ctrl_sta = prgEvent->command;
                HandleQueues();
                break;  
            case    HSB_ACTIVE  :
                ctrl_sta = prgEvent->command;
                break;  
            case    HSB_ACT_TO_SBY  :
                ctrl_sta = prgEvent->command;
                /* CloseConnection(); */
                HandleQueues();
                break;  
            case    HSB_DOWN    :
                /* whole system shutdown - do not further use que(), send_message() or timsch() ! */
                ctrl_sta = prgEvent->command;
                Terminate(1);
                break;  
            case    HSB_STANDALONE  :
                ctrl_sta = prgEvent->command;
                ResetDBCounter();
                break;  
            case    REMOTE_DB :
                /* ctrl_sta is checked inside */
                HandleRemoteDB(prgEvent);
                break;
            case    SHUTDOWN    :
                /* process shutdown - maybe from uutil */
                Terminate(1);
                break;
                    
            case    RESET       :
                ilRc = Reset();
                break;
                    
            case    EVENT_DATA  :
                if((ctrl_sta == HSB_STANDALONE) || 
                    (ctrl_sta == HSB_ACTIVE) || 
                    (ctrl_sta == HSB_ACT_TO_SBY))
                {
                    ilRc = HandleData(prgEvent);
                    if(ilRc != RC_SUCCESS)
                    {
                        HandleErr(ilRc);
                    }/* end of if */
                }else{
                    dbg(TRACE,"MAIN: wrong hsb status <%d>",ctrl_sta);
                    DebugPrintItem(TRACE,prgItem);
                    DebugPrintEvent(TRACE,prgEvent);
                }/* end of if */
                break;
                
            case    TRACE_ON :
                dbg_handle_debug(prgEvent->command);
                break;
            case    TRACE_OFF :
                dbg_handle_debug(prgEvent->command);
                break;
            default         :
                dbg(TRACE,"MAIN: unknown event");
                DebugPrintItem(TRACE,prgItem);
                DebugPrintEvent(TRACE,prgEvent);
                break;
            } /* end switch */
        } else {
            /* Handle queuing errors */
            HandleQueErr(ilRc);
        } /* end else */
        
    } /* end for */
    
} /* end of MAIN */

/******************************************************************************/
/* The initialization routine                                                 */
/******************************************************************************/

static int FindItemInArrayList(char *pcpInput, char *pcpPattern,char cpDel,int *ipLine,int *ipCol,int *ipPos)
{
    int ilRc = RC_SUCCESS;

    ilRc = FindItemInList(pcpInput,pcpPattern,cpDel,ipLine,ipCol,ipPos);
    *ipLine = (*ipLine) -1;
    
    return ilRc;
}



static int InitFieldIndex()
{
    int ilRc = RC_SUCCESS;
    int ilCol,ilPos;
    int ilDBFields;

    /*FindItemInArrayList(DEM_FIELDS,"ALID",',',&igDemALID,&ilCol,&ilPos);*/



    FindItemInArrayList(REL_FIELDS,"CDAT",',',&igRelCDAT,&ilCol,&ilPos);
    FindItemInArrayList(REL_FIELDS,"DORS",',',&igRelDORS,&ilCol,&ilPos);
    FindItemInArrayList(REL_FIELDS,"HOPO",',',&igRelHOPO,&ilCol,&ilPos);
    FindItemInArrayList(REL_FIELDS,"LSTU",',',&igRelLSTU,&ilCol,&ilPos);
    FindItemInArrayList(REL_FIELDS,"RLAT",',',&igRelRLAT,&ilCol,&ilPos);
    FindItemInArrayList(REL_FIELDS,"RLFR",',',&igRelRLFR,&ilCol,&ilPos);
    FindItemInArrayList(REL_FIELDS,"RLTO",',',&igRelRLTO,&ilCol,&ilPos);
    FindItemInArrayList(REL_FIELDS,"ROSL",',',&igRelROSL,&ilCol,&ilPos);
    FindItemInArrayList(REL_FIELDS,"SJOB",',',&igRelSJOB,&ilCol,&ilPos);
    FindItemInArrayList(REL_FIELDS,"SPLU",',',&igRelSPLU,&ilCol,&ilPos);
    FindItemInArrayList(REL_FIELDS,"STFU",',',&igRelSTFU,&ilCol,&ilPos);
    FindItemInArrayList(REL_FIELDS,"URNO",',',&igRelURNO,&ilCol,&ilPos);
    FindItemInArrayList(REL_FIELDS,"USEC",',',&igRelUSEC,&ilCol,&ilPos);
    FindItemInArrayList(REL_FIELDS,"USEU",',',&igRelUSEU,&ilCol,&ilPos);

    FindItemInArrayList(cgDrrFields,"AVFR",',',&igDrrAVFR,&ilCol,&ilPos);
    FindItemInArrayList(cgDrrFields,"AVTO",',',&igDrrAVTO,&ilCol,&ilPos);
    FindItemInArrayList(cgDrrFields,"BKDP",',',&igDrrBKDP,&ilCol,&ilPos);
    FindItemInArrayList(cgDrrFields,"BSDU",',',&igDrrBSDU,&ilCol,&ilPos);
    FindItemInArrayList(cgDrrFields,"BUFU",',',&igDrrBUFU,&ilCol,&ilPos);
    FindItemInArrayList(cgDrrFields,"CDAT",',',&igDrrCDAT,&ilCol,&ilPos);
    FindItemInArrayList(cgDrrFields,"DRRN",',',&igDrrDRRN,&ilCol,&ilPos);
    FindItemInArrayList(cgDrrFields,"DRS1",',',&igDrrDRS1,&ilCol,&ilPos);
    FindItemInArrayList(cgDrrFields,"DRS2",',',&igDrrDRS2,&ilCol,&ilPos);
    FindItemInArrayList(cgDrrFields,"DRS3",',',&igDrrDRS3,&ilCol,&ilPos);
    FindItemInArrayList(cgDrrFields,"DRS4",',',&igDrrDRS4,&ilCol,&ilPos);
    FindItemInArrayList(cgDrrFields,"DRSF",',',&igDrrDRSF,&ilCol,&ilPos);
    FindItemInArrayList(cgDrrFields,"EXPF",',',&igDrrEXPF,&ilCol,&ilPos);
    FindItemInArrayList(cgDrrFields,"FCTC",',',&igDrrFCTC,&ilCol,&ilPos);
    FindItemInArrayList(cgDrrFields,"HOPO",',',&igDrrHOPO,&ilCol,&ilPos);
    /*FindItemInArrayList(cgDrrFields,"LSTU",',',&igDrrLSTU,&ilCol,&ilPos);*/
    FindItemInArrayList(cgDrrFields,"REMA",',',&igDrrREMA,&ilCol,&ilPos);
    FindItemInArrayList(cgDrrFields,"ROSL",',',&igDrrROSL,&ilCol,&ilPos);
    FindItemInArrayList(cgDrrFields,"ROSS",',',&igDrrROSS,&ilCol,&ilPos);
    FindItemInArrayList(cgDrrFields,"SBFR",',',&igDrrSBFR,&ilCol,&ilPos);
    FindItemInArrayList(cgDrrFields,"SBLP",',',&igDrrSBLP,&ilCol,&ilPos);
    FindItemInArrayList(cgDrrFields,"SBLU",',',&igDrrSBLU,&ilCol,&ilPos);
    FindItemInArrayList(cgDrrFields,"SBTO",',',&igDrrSBTO,&ilCol,&ilPos);
    FindItemInArrayList(cgDrrFields,"SCOD",',',&igDrrSCOD,&ilCol,&ilPos);
    FindItemInArrayList(cgDrrFields,"SCOO",',',&igDrrSCOO,&ilCol,&ilPos);
    FindItemInArrayList(cgDrrFields,"SDAY",',',&igDrrSDAY,&ilCol,&ilPos);
    FindItemInArrayList(cgDrrFields,"STFU",',',&igDrrSTFU,&ilCol,&ilPos);
    FindItemInArrayList(cgDrrFields,"URNO",',',&igDrrURNO,&ilCol,&ilPos);
    FindItemInArrayList(cgDrrFields,"USEC",',',&igDrrUSEC,&ilCol,&ilPos);
    FindItemInArrayList(cgDrrFields,"USEU",',',&igDrrUSEU,&ilCol,&ilPos);
    if (bgUseFilt)
    {
        FindItemInArrayList(cgDrrFields,"FILT",',',&igDrrFILT,&ilCol,&ilPos);
    }


    FindItemInArrayList(cgStfFields,"URNO",',',&igStfURNO,&ilCol,&ilPos);
    FindItemInArrayList(cgStfFields,"DODM",',',&igStfDODM,&ilCol,&ilPos);
    FindItemInArrayList(cgStfFields,"DOEM",',',&igStfDOEM,&ilCol,&ilPos);

    if (bgBROS == TRUE)
    {
    FindItemInArrayList(cgDrrFields,"BROS",',',&igDrrBROS,&ilCol,&ilPos);
    }
    if (bgOABS == TRUE)
    {
    FindItemInArrayList(cgDrrFields,"OABS",',',&igDrrOABS,&ilCol,&ilPos);
    }

    dbg ( TRACE, "Index of DRRTAB-fields BROS <%d>", igDrrBROS );

  ilDBFields = get_no_of_items(cgDrrFields);
  FindItemInList(ADD_DRR_FIELDS,"USED",',',&igDrrUSED,&ilCol,&ilPos);
  if ( igDrrUSED >= 0 )
    igDrrUSED += ilDBFields; /* -1 */

  dbg(TRACE,"USED=%d FieldCount=%d",igDrrUSED,ilDBFields);
    return ilRc;

}


static int ReadConfigEntry(char *pcpSection,char *pcpKeyword,
    char *pcpCfgBuffer)
{
    int ilRc = RC_SUCCESS;

    char clSection[124];
    char clKeyword[124];

    strcpy(clSection,pcpSection);
    strcpy(clKeyword,pcpKeyword);

    ilRc = iGetConfigEntry(cgConfigFile,clSection,clKeyword,
            CFG_STRING,pcpCfgBuffer);
    if(ilRc != RC_SUCCESS)
    {
        dbg(TRACE,"Not found in %s: <%s> <%s>",cgConfigFile,clSection,clKeyword);
    } 
    else
    {
            dbg(DEBUG,"Config Entry <%s>,<%s>:<%s> found in %s",
                    &clSection[0],&clKeyword[0],pcpCfgBuffer,cgConfigFile);
    }/* end of if */
    return ilRc;
}


static int InitRdphdl()
{
    int  ilRc = RC_SUCCESS;         /* Return code */
  char clSection[64];
  char clKeyword[64];
  char clNow[64];
    time_t tlNow;
    int ilOldDebugLevel;
    long pclAddFieldLens[12];
    char pclAddFields[256];
    char pclSqlBuf[2560];
    char pclSelection[1024];
    short slCursor;
    short slSqlFunc;
    char  clBreak[24];
    int ilLc;

    tlNow = time(NULL);
    tlNow -= tlNow % 86400;

    TimeToStr(clNow,tlNow);

    dbg(TRACE,"Now = <%s>",clNow);

    /* now reading from configfile or from database */


    if(ilRc == RC_SUCCESS)
    {
        /* read HomeAirPort from SGS.TAB */
        memset((void*)&cgHopo[0], 0x00, 8);

        sprintf(&clSection[0],"SYS");
        sprintf(&clKeyword[0],"HOMEAP");

        ilRc = tool_search_exco_data(&clSection[0], &clKeyword[0], &cgHopo[0]);
        if (ilRc != RC_SUCCESS)
        {
            dbg(TRACE,"<InitRdphdl> EXTAB,%s,%s not found in SGS.TAB",&clSection[0], &clKeyword[0]);
            Terminate(30);
        } else {
            dbg(TRACE,"<InitRdphdl> home airport    <%s>",&cgHopo[0]);
        }/* end of if */
    }/* end of if */

    
    if(ilRc == RC_SUCCESS)
    {
        sprintf(&clSection[0],"ALL");
        sprintf(&clKeyword[0],"TABEND");

        ilRc = tool_search_exco_data(&clSection[0], &clKeyword[0], &cgTabEnd[0]);
        if (ilRc != RC_SUCCESS)
        {
            dbg(TRACE,"<InitRdphdl> EXTAB,%s,%s not found in SGS.TAB",&clSection[0], &clKeyword[0]);
            Terminate(30);
        } else {
            dbg(TRACE,"<InitRdphdl> table extension <%s>",&cgTabEnd[0]);
        }/* end of if */
    }/* end of if */

    if (ilRc == RC_SUCCESS)
    {
        if((ilRc = tool_get_q_id("bchdl")) != RC_FAIL)
        {
            igBchdlModid = ilRc;
        }
        if((ilRc = tool_get_q_id("action")) != RC_FAIL)
        {
            igActionModid = ilRc;
        }

        ilRc = RC_SUCCESS;
    }

    if(ilRc == RC_SUCCESS)
    {
         GetDebugLevel("STARTUP_MODE", &igStartUpMode);
         GetDebugLevel("RUNTIME_MODE", &igRuntimeMode);
    }
    debug_level = igStartUpMode;


    if(ilRc == RC_SUCCESS)
    {
        ilRc = ReadConfigEntry("MAIN","REL_SEQUENCE",cgRelSequence);
        if(ilRc == RC_SUCCESS)
        {
            dbg(TRACE,"REL_SEQUENCE set to <%s>",cgRelSequence);
        }
        else
        {
            dbg(TRACE,"REL_SEQUENCE not found, set to <%s>",cgRelSequence);
            ilRc = RC_SUCCESS;
        }
    }

    if(ilRc == RC_SUCCESS)
    {
        char clTmpString[124];

        ilRc = ReadConfigEntry("MAIN","DELRELTABOFFSET",clTmpString);
        if(ilRc == RC_SUCCESS)
        {
            igReltabOffset = atoi(clTmpString);
            dbg(TRACE,"DELRELTABOFFSET set to <%d>",igReltabOffset);
            igReltabOffset *= 86400;
        }
        else
        {
            igReltabOffset = -5;
            dbg(TRACE,"DELRELTABOFFSET not found, set to <%d>",igReltabOffset);
            igReltabOffset *= 86400;
            ilRc = RC_SUCCESS;
        }
    }

    if(ilRc == RC_SUCCESS)
    {
        char clTmpString[124];

        bgUseFilt = FALSE;
        ilRc = iGetConfigRow(cgConfigFile,"MAIN","USEFILT", CFG_STRING,clTmpString);
        if( (ilRc == RC_SUCCESS) && (strcmp(clTmpString,"YES") == 0) )
        {
            bgUseFilt = TRUE;
        }
        ilRc = RC_SUCCESS;
    }

    if(ilRc == RC_SUCCESS)
    {
        ilRc = ReadConfigEntry("MAIN","REL_SECTIONS",cgRelSections);
        if(ilRc == RC_SUCCESS)
        {
            dbg(TRACE,"REL_SECTIONS set to <%s>",cgRelSections);
            igSections = itemCount(cgRelSections);
        }
        else
        {
            dbg(TRACE,"REL_SECTIONS not found, set to <%s>",cgRelSections);
            igSections = 0;
            ilRc = RC_SUCCESS;
        }
    }

  ilRc = iGetConfigRow(cgConfigFile,"MAIN","DrrReloadHint", CFG_STRING,cgDrrReloadHint);
   if ( ilRc != RC_SUCCESS )
      strcpy ( cgDrrReloadHint, "/*+ INDEX(DRRTAB DRRTAB_SDAY_ROSL) */");
   dbg(TRACE,"InitPolhdl: Hint for reloading DRRTAB <%s>", cgDrrReloadHint );

    
    /* read all sections */
    for(ilLc = 0; ilLc < igSections; ilLc++)
    {
      char clTmpSection[24];

        ilRc = get_real_item(clTmpSection,cgRelSections,ilLc+1);
        if (ilRc > 0)
        {
            strcpy(rgCfgData[ilLc].cSectionName,clTmpSection);
          ilRc = ReadConfigEntry(clTmpSection,"PERIOD",rgCfgData[ilLc].cPeriod);  
            if (ilRc != RC_SUCCESS)
            {
                strcpy (rgCfgData[ilLc].cPeriod," ");  
                dbg(TRACE,"InitRdphdl: Period in <%s> not found, Period set to <%s>",
                    clTmpSection,rgCfgData[ilLc].cPeriod);
            }
          ilRc = ReadConfigEntry(clTmpSection,"REL_SEQUENCE",rgCfgData[ilLc].cSequence);  
            if (ilRc != RC_SUCCESS)
            {
                strcpy (rgCfgData[ilLc].cSequence,"");  
                dbg(TRACE,"InitRdphdl: Sequence in <%s> not found, Sequence set to <%s>",
                    clTmpSection,rgCfgData[ilLc].cSequence);
            }
          ilRc = ReadConfigEntry(clTmpSection,"LEVEL",rgCfgData[ilLc].cLevel);  
            if (ilRc != RC_SUCCESS)
            {
                strcpy (rgCfgData[ilLc].cLevel," ");  
                dbg(TRACE,"InitRdphdl: Level in <%s> not found, Level set to <%s>",
                    clTmpSection,rgCfgData[ilLc].cLevel);
            }
          ilRc = ReadConfigEntry(clTmpSection,"DURATION",rgCfgData[ilLc].cDuration);  
            if (ilRc != RC_SUCCESS)
            {
                strcpy (rgCfgData[ilLc].cDuration," ");  
                dbg(TRACE,"InitRdphdl: Duration in <%s> not found, Duration set to <%s>",
                    clTmpSection,rgCfgData[ilLc].cDuration);
            }
          ilRc = ReadConfigEntry(clTmpSection,"TYPE",rgCfgData[ilLc].cType);  
            if (ilRc != RC_SUCCESS)
            {
                strcpy (rgCfgData[ilLc].cType," ");  
                dbg(TRACE,"InitRdphdl: Type in <%s> not found, Type set to <%s>",
                    clTmpSection,rgCfgData[ilLc].cType);
            }
          ilRc = ReadConfigEntry(clTmpSection,"OFFSET",rgCfgData[ilLc].cOffset);  
            if (ilRc != RC_SUCCESS)
            {
                strcpy (rgCfgData[ilLc].cOffset," ");  
                dbg(TRACE,"InitRdphdl: Offset in <%s> not found, Offset set to <%s>",
                    clTmpSection,rgCfgData[ilLc].cOffset);
            }
        }
    }
    
    /* read user tables to detect if field BROS exist in DRRTAB */
    slCursor = 0;
    ilRc = sql_if(START,&slCursor,"SELECT COLUMN_NAME FROM USER_TAB_COLUMNS WHERE TABLE_NAME = 'DRRTAB' AND COLUMN_NAME = 'BROS'",cgDataArea);
    if(ilRc == RC_SUCCESS)
    {
        bgBROS = TRUE; /* we found field BROS */
        sprintf(cgDrrFields,"%s,BROS",DRR_FIELDS);
        dbg(TRACE,"found field BROS in DRRTAB");
  }
    else if(ilRc == RC_NOTFOUND)
    {
         bgBROS = FALSE; /* field BROS does not exist */   
        strcpy(cgDrrFields,DRR_FIELDS);
        dbg(TRACE,"field BROS does not exist in DRRTAB");
    } 
    else
    { 
        /* there must be a database error */
        get_ora_err (ilRc, cgErrMsg);
        dbg(TRACE,"Reading USER_TABLES for field BROS failed %d<%s>",ilRc,cgErrMsg);
    }
    close_my_cursor(&slCursor);
    slCursor = 0;

    if (bgUseFilt)
    {
        strcat(cgDrrFields,",FILT");
    }
    
    /* read user tables to detect if field OABS exist in DRRTAB */
    slCursor = 0;
    ilRc = sql_if(START,&slCursor,"SELECT COLUMN_NAME FROM USER_TAB_COLUMNS WHERE TABLE_NAME = 'DRRTAB' AND COLUMN_NAME = 'OABS'",cgDataArea);
    if(ilRc == RC_SUCCESS)
    {
        bgOABS = TRUE; /* we found field OABS */
        strcat(cgDrrFields,",OABS");
        dbg(TRACE,"found field OABS in DRRTAB");
  }
    else if(ilRc == RC_NOTFOUND)
    {
         bgOABS = FALSE; /* field OABS does not exist */   
         dbg(TRACE,"field OABS does not exist in DRRTAB");
    } 
    else
    { 
        /* there must be a database error */
        get_ora_err (ilRc, cgErrMsg);
        dbg(TRACE,"Reading USER_TABLES for field OABS failed %d<%s>",ilRc,cgErrMsg);
    }
    close_my_cursor(&slCursor);
    slCursor = 0;

    ilRc = RC_SUCCESS; /** read config file is always successful **/

    
    if(ilRc == RC_SUCCESS)
    {
        ilRc = CEDAArrayInitialize(20,20);
        if(ilRc != RC_SUCCESS)
        {
            dbg(TRACE,"InitRdphdl: CEDAArrayInitialize failed <%d>",ilRc);
        }/* end of if */
    }/* end of if */



    if(ilRc == RC_SUCCESS)
    {
        long pclAddFieldLens[12];  


         pclAddFieldLens[0] = 2;  
         pclAddFieldLens[1] = 2;  
         pclAddFieldLens[1] = 0;  

        sprintf(pclSelection,"WHERE HOPO = '%s'",cgHopo);
        dbg(TRACE,"InitRdphdl: SetArrayInfo DRRNEW array \n<%s>",pclSelection);
        ilRc = SetArrayInfo("DRRNEW","DRRTAB",cgDrrFields,
                ADD_DRR_FIELDS,pclAddFieldLens,&rgNewDrrArray,pclSelection,FALSE);
        if(ilRc != RC_SUCCESS)
        {
            dbg(TRACE,"InitRdphdl: SetArrayInfo DRRNEW failed <%d>",ilRc);
        }
        else
        {
            dbg(DEBUG,"InitRdphdl: SetArrayInfo DRRNEW OK");
            dbg(DEBUG,"<%s> %d",rgNewDrrArray.crArrayName,
                rgNewDrrArray.rrArrayHandle);
/************
            CEDAArraySendChanges2BCHDL(&rgNewDrrArray.rrArrayHandle,
                      rgNewDrrArray.crArrayName,TRUE);
            CEDAArraySendChanges2ACTION(&rgNewDrrArray.rrArrayHandle,
                      rgNewDrrArray.crArrayName,TRUE);
**********************/

                strcpy(rgNewDrrArray.crIdx01Name,"DRR_STFU");
                ilRc = CEDAArrayCreateSimpleIndexUp(&rgNewDrrArray.rrArrayHandle,
                    rgNewDrrArray.crArrayName,
                    &rgNewDrrArray.rrIdx01Handle,
                    rgNewDrrArray.crIdx01Name,"STFU,SDAY,DRRN");
                    strcpy(rgNewDrrArray.crIdx01FieldList,"STFU,SDAY,DRRN");
        }
    }
    if(ilRc == RC_SUCCESS)
    {

        sprintf(pclSelection,"WHERE HOPO = '%s'",cgHopo);
        dbg(TRACE,"InitRdphdl: SetArrayInfo DRRTAB array \n<%s>",pclSelection);
        ilRc = SetArrayInfo("DRRTAB","DRRTAB",cgDrrFields,
                NULL,NULL,&rgDrrArray,pclSelection,FALSE);
        if(ilRc != RC_SUCCESS)
        {
            dbg(TRACE,"InitRdphdl: SetArrayInfo DRRTAB failed <%d>",ilRc);
        }
        else
        {
            dbg(DEBUG,"InitRdphdl: SetArrayInfo DRRTAB OK");
            dbg(DEBUG,"<%s> %d",rgDrrArray.crArrayName,rgDrrArray.rrArrayHandle);
        }
    }

    if(ilRc == RC_SUCCESS)
    {

        sprintf(pclSelection,"WHERE HOPO = '%s'",cgHopo);
        dbg(TRACE,"InitRdphdl: SetArrayInfo ODATAB array \n<%s>",pclSelection);
        ilRc = SetArrayInfo("ODATAB","ODATAB",ODA_FIELDS,NULL,NULL,&rgOdaArray,pclSelection,TRUE);
        if(ilRc != RC_SUCCESS)
        {
            dbg(TRACE,"InitRdphdl: SetArrayInfo ODATAB failed <%d>",ilRc);
        }
        else
        {
            dbg(DEBUG,"InitRdphdl: SetArrayInfo ODATAB OK");
            dbg(DEBUG,"<%s> %d",rgOdaArray.crArrayName,rgOdaArray.rrArrayHandle);
            strcpy(rgOdaArray.crIdx01Name,"ODA_URNO");
            ilRc = CEDAArrayCreateSimpleIndexUp(&rgOdaArray.rrArrayHandle,
                rgOdaArray.crArrayName,
                &rgOdaArray.rrIdx01Handle,
                rgOdaArray.crIdx01Name,"URNO");
                strcpy(rgOdaArray.crIdx01FieldList,"URNO");

            strcpy(rgOdaArray.crIdx02Name,"ODA_SDAC");
            ilRc = CEDAArrayCreateSimpleIndexUp(&rgOdaArray.rrArrayHandle,
                rgOdaArray.crArrayName,
                &rgOdaArray.rrIdx02Handle,
                rgOdaArray.crIdx02Name,"SDAC");
                strcpy(rgOdaArray.crIdx02FieldList,"SDAC");
                ilRc = TriggerAction("ODATAB");
                if(ilRc != RC_SUCCESS)
                {
                    dbg(TRACE,"InitRdphdl: TriggerAction ODATAB failed <%d>",ilRc);
                }
        }

        sprintf(pclSelection,"WHERE HOPO = '%s'",cgHopo);
        dbg(TRACE,"InitRdphdl: SetArrayInfo STFTAB array \n<%s>",pclSelection);
        ilRc = SetArrayInfo("STFTAB","STFTAB",STF_FIELDS,NULL,NULL,&rgStfArray,pclSelection,TRUE);
        if(ilRc != RC_SUCCESS)
        {
            dbg(TRACE,"InitRdphdl: SetArrayInfo STFTAB failed <%d>",ilRc);
        }
        else
        {
            dbg(DEBUG,"InitRdphdl: SetArrayInfo STFTAB OK");
            dbg(DEBUG,"<%s> %d",rgStfArray.crArrayName,rgStfArray.rrArrayHandle);
            strcpy(rgStfArray.crIdx03Name,"STF_URNO");
            ilRc = CEDAArrayCreateSimpleIndexUp(&rgStfArray.rrArrayHandle,
                rgStfArray.crArrayName,
                &rgStfArray.rrIdx03Handle,
                rgStfArray.crIdx03Name,"URNO");
                strcpy(rgStfArray.crIdx03FieldList,"URNO");

                ilRc = TriggerAction("STFTAB");
                if(ilRc != RC_SUCCESS)
                {
                    dbg(TRACE,"InitRdphdl: TriggerAction failed <%d>",ilRc);
                }

        }
    }


    if(ilRc == RC_SUCCESS)
    {
        sprintf(pclSelection,"WHERE HOPO = '%s'",cgHopo);
        dbg(TRACE,"InitRdphdl: SetArrayInfo RELTAB array");
        ilRc = SetArrayInfo("RELTAB","RELTAB",REL_FIELDS,
                NULL,NULL,&rgRelArray,pclSelection,FALSE);
        if(ilRc != RC_SUCCESS)
        {
            dbg(TRACE,"InitRdphdl: SetArrayInfo RELTAB failed <%d>",ilRc);
        }
        else
        {
            dbg(TRACE,"InitRdphdl: SetArrayInfo RELTAB OK");
            dbg(TRACE,"<%s> %d",rgRelArray.crArrayName,
                rgRelArray.rrArrayHandle);
        }
    }   

    if(ilRc == RC_SUCCESS)
    {
        ilRc = CEDAArrayReadAllHeader(outp);
        if(ilRc != RC_SUCCESS)
        {
            dbg(TRACE,"InitRdphdl: CEDAArrayReadAllHeader failed <%d>",ilRc);
        }else{
            dbg(TRACE,"InitRdphdl: CEDAArrayReadAllHeader OK");
        }/* end of if */
    }/* end of if */



    if(ilRc == RC_SUCCESS)
    {
    InitDRSArray();
    }

    if(ilRc == RC_SUCCESS)
    {
        ilRc = InitFieldIndex();
    }
    if(ilRc == RC_SUCCESS)
    {
        igInitOK = TRUE;
        dbg(TRACE,"InitRdphdl: InitFieldIndex OK");
    }/* end of if */
        else
        {
        dbg(TRACE,"InitRdphdl: InitFieldIndex failed");
        }

    dbg(TRACE,"Debug Level is %d",debug_level);
    return (ilRc);  
} /* end of initialize */




/******************************************************************************/
/*                                                                            */
/******************************************************************************/
static int SetArrayInfo(char *pcpArrayName, char *pcpTableName, char *pcpArrayFieldList,char *pcpAddFields,long *pcpAddFieldLens,ARRAYINFO *prpArrayInfo,
    char *pcpSelection,BOOL bpFill)
{
    int ilRc = RC_SUCCESS;              /* Return code */
  int ilLc;

    if(prpArrayInfo == NULL)
    {
        dbg(TRACE,"SetArrayInfo: invalid last parameter : null pointer not valid");
        ilRc = RC_FAIL;
    }else
    {
        memset(prpArrayInfo,0x00,sizeof(prpArrayInfo));

        prpArrayInfo->rrArrayHandle = -1;
        prpArrayInfo->rrIdx01Handle = -1;

        if(pcpArrayName != NULL)
        {
            if(strlen(pcpArrayName) <= ARR_NAME_LEN)
            {
                strcpy(prpArrayInfo->crArrayName,pcpArrayName);
            }/* end of if */
        }/* end of if */
    }/* end of if */

    if(ilRc == RC_SUCCESS)
    {
        prpArrayInfo->lrArrayFieldCnt = get_no_of_items(pcpArrayFieldList);
        if (pcpAddFields != NULL)
        {
            prpArrayInfo->lrArrayFieldCnt += get_no_of_items(pcpAddFields);
        }

        prpArrayInfo->plrArrayFieldOfs = (long *) calloc(prpArrayInfo->lrArrayFieldCnt,sizeof(long));
        if(prpArrayInfo->plrArrayFieldOfs == NULL)
        {
            dbg(TRACE,"SetArrayInfo: plrArrayFieldOfs calloc(%d,%d) failed",prpArrayInfo->lrArrayFieldCnt,sizeof(long));
            ilRc = RC_FAIL;
        }/* end of if */
    }/* end of if */

    if(ilRc == RC_SUCCESS)
    {
        prpArrayInfo->plrArrayFieldLen = (long *) calloc(prpArrayInfo->lrArrayFieldCnt,sizeof(long));
        if(prpArrayInfo->plrArrayFieldLen == NULL)
        {
            dbg(TRACE,"SetArrayInfo: plrArrayFieldLen calloc(%d,%d) failed",prpArrayInfo->lrArrayFieldCnt,sizeof(long));
            ilRc = RC_FAIL;
        }else{
            *(prpArrayInfo->plrArrayFieldLen) = -1;
        }/* end of if */
    }/* end of if */

    if(ilRc == RC_SUCCESS)
    {
        ilRc = GetRowLength(&cgHopo[0],pcpTableName,pcpArrayFieldList,
                            &(prpArrayInfo->lrArrayRowLen));
        if(ilRc != RC_SUCCESS)
        {
            dbg(TRACE,"SetArrayInfo: GetRowLength failed <%s> <%s>",pcpArrayName,pcpArrayFieldList);
        }/* end of if */
    }/* end of if */

    if(ilRc == RC_SUCCESS)
    {
        prpArrayInfo->lrArrayRowLen+=100;
        prpArrayInfo->pcrArrayRowBuf = (char *) calloc(1,(prpArrayInfo->lrArrayRowLen+10));
        if(prpArrayInfo->pcrArrayRowBuf == NULL)
        {
                ilRc = RC_FAIL;
                dbg(TRACE,"SetArrayInfo: calloc failed");
        }/* end of if */
    }/* end of if */

    if(ilRc == RC_SUCCESS)
    {
        prpArrayInfo->pcrIdx01RowBuf = (char *) calloc(1,(prpArrayInfo->lrIdx01RowLen+1));
        if(prpArrayInfo->pcrIdx01RowBuf == NULL)
        {
                ilRc = RC_FAIL;
                dbg(TRACE,"SetArrayInfo: calloc failed");
        }/* end of if */
    }/* end of if */

    if(ilRc == RC_SUCCESS)
    {
        dbg(TRACE,"CEDAArrayCreate follows Array <%s> Table <%s>",pcpArrayName,pcpTableName);
        ilRc = CEDAArrayCreate(&(prpArrayInfo->rrArrayHandle),
                pcpArrayName,pcpTableName,pcpSelection,pcpAddFields,
                pcpAddFieldLens,pcpArrayFieldList,
                &(prpArrayInfo->plrArrayFieldLen[0]),
                &(prpArrayInfo->plrArrayFieldOfs[0]));

        if(ilRc != RC_SUCCESS)
        {
            dbg(TRACE,"SetArrayInfo: CEDAArrayInitialize failed <%d>",ilRc);
        }/* end of if */
    }/* end of if */

        if(pcpArrayFieldList != NULL)
        {
            if(strlen(pcpArrayFieldList) <= ARR_FLDLST_LEN)
            {
                strcpy(prpArrayInfo->crArrayFieldList,pcpArrayFieldList);
            }/* end of if */
        }/* end of if */
        if(pcpAddFields != NULL)
        {
            if(strlen(pcpAddFields) + 
                strlen(prpArrayInfo->crArrayFieldList) <= (size_t)ARR_FLDLST_LEN)
            {
                strcat(prpArrayInfo->crArrayFieldList,",");
                strcat(prpArrayInfo->crArrayFieldList,pcpAddFields);
                dbg(TRACE,"<%s>",prpArrayInfo->crArrayFieldList);
            }/* end of if */
        }/* end of if */


    if(ilRc == RC_SUCCESS && bpFill)
    {
        ilRc = CEDAArrayFill(&(prpArrayInfo->rrArrayHandle),pcpArrayName,NULL);
        if(ilRc != RC_SUCCESS)
        {
            dbg(TRACE,"SetArrayInfo: CEDAArrayFill failed <%d>",ilRc);
        }/* end of if */
    }/* end of if */

   for(ilLc = 0; ilLc < prpArrayInfo->lrArrayFieldCnt; ilLc++)
   {
            char    pclFieldName[25];

            *pclFieldName  = '\0';

            GetDataItem(pclFieldName,prpArrayInfo->crArrayFieldList,
                        ilLc+1,',',"","\0\0");
     dbg(TRACE,"%02d %-10s Offset: %d",
                ilLc,pclFieldName,prpArrayInfo->plrArrayFieldOfs[ilLc]);
   }   

    return(ilRc);
}



/******************************************************************************/
/* The Reset routine                                                          */
/******************************************************************************/
static int Reset()
{
    int ilRc = RC_SUCCESS;              /* Return code */
    
    dbg(TRACE,"Reset: now resetting");
    
    return ilRc;
    
} /* end of Reset */

/******************************************************************************/
/* The termination routine                                                    */
/******************************************************************************/
static void Terminate(int ipSleep)
{
    /* unset SIGCHLD ! DB-Child will terminate ! */

    dbg(TRACE,"Terminate: now DB logoff ...");

    signal(SIGCHLD,SIG_IGN);

    logoff();

    dbg(TRACE,"Terminate: now sleep(%d) ...",ipSleep);


    dbg(TRACE,"Terminate: now leaving ...");

    fclose(outp);

    sleep(ipSleep < 1 ? 1 : ipSleep);
    
    exit(0);
    
} /* end of Terminate */

/******************************************************************************/
/* The handle signals routine                                                 */
/******************************************************************************/
#if 0
static void HandleSignal(int pipSig)
{
    dbg(TRACE,"HandleSignal: signal <%d> received",pipSig);

    switch(pipSig)
    {
    default :
        Terminate(1);
        break;
    } /* end of switch */

    exit(1);
    
} /* end of HandleSignal */
#endif
/******************************************************************************/
/* The handle general error routine                                           */
/******************************************************************************/
static void HandleErr(int pipErr)
{
    dbg(TRACE,"HandleErr: <%d> : unknown ERROR",pipErr);

    return;
} /* end of HandleErr */

/******************************************************************************/
/* The handle queuing error routine                                           */
/******************************************************************************/
static void HandleQueErr(int pipErr)
{
    switch(pipErr) {
    case    QUE_E_FUNC  :   /* Unknown function */
        dbg(TRACE,"<%d> : unknown function",pipErr);
        break;
    case    QUE_E_MEMORY    :   /* Malloc reports no memory */
        dbg(TRACE,"<%d> : malloc failed",pipErr);
        break;
    case    QUE_E_SEND  :   /* Error using msgsnd */
        dbg(TRACE,"<%d> : msgsnd failed",pipErr);
        break;
    case    QUE_E_GET   :   /* Error using msgrcv */
        dbg(TRACE,"<%d> : msgrcv failed",pipErr);
        break;
    case    QUE_E_EXISTS    :
        dbg(TRACE,"<%d> : route/queue already exists ",pipErr);
        break;
    case    QUE_E_NOFIND    :
        dbg(TRACE,"<%d> : route not found ",pipErr);
        break;
    case    QUE_E_ACKUNEX   :
        dbg(TRACE,"<%d> : unexpected ack received ",pipErr);
        break;
    case    QUE_E_STATUS    :
        dbg(TRACE,"<%d> :  unknown queue status ",pipErr);
        break;
    case    QUE_E_INACTIVE  :
        dbg(TRACE,"<%d> : queue is inaktive ",pipErr);
        break;
    case    QUE_E_MISACK    :
        dbg(TRACE,"<%d> : missing ack ",pipErr);
        break;
    case    QUE_E_NOQUEUES  :
        dbg(TRACE,"<%d> : queue does not exist",pipErr);
        break;
    case    QUE_E_RESP  :   /* No response on CREATE */
        dbg(TRACE,"<%d> : no response on create",pipErr);
        break;
    case    QUE_E_FULL  :
        dbg(TRACE,"<%d> : too many route destinations",pipErr);
        break;
    case    QUE_E_NOMSG :   /* No message on queue */
        dbg(TRACE,"<%d> : no messages on queue",pipErr);
        break;
    case    QUE_E_INVORG    :   /* Mod id by que call is 0 */
        dbg(TRACE,"<%d> : invalid originator=0",pipErr);
        break;
    case    QUE_E_NOINIT    :   /* Queues is not initialized*/
        dbg(TRACE,"<%d> : queues are not initialized",pipErr);
        break;
    case    QUE_E_ITOBIG    :
        dbg(TRACE,"<%d> : requestet itemsize to big ",pipErr);
        break;
    case    QUE_E_BUFSIZ    :
        dbg(TRACE,"<%d> : receive buffer to small ",pipErr);
        break;
    default         :   /* Unknown queue error */
        dbg(TRACE,"<%d> : unknown error",pipErr);
        break;
    } /* end switch */
         
    return;

} /* end of HandleQueErr */

/******************************************************************************/
/* The handle queues routine                                                  */
/******************************************************************************/
static void HandleQueues()
{
    int ilRc = RC_SUCCESS;          /* Return code */
    int ilBreakOut = FALSE;
    
    do
    {
        ilRc = que(QUE_GETBIG,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);
        /* depending on the size of the received item  */
        /* a realloc could be made by the que function */
        /* so do never forget to set event pointer !!! */
        prgEvent = (EVENT *) prgItem->text; 

        if( ilRc == RC_SUCCESS )
        {
            /* Acknowledge the item */
            ilRc = que(QUE_ACK,0,mod_id,0,0,NULL);
            if( ilRc != RC_SUCCESS ) 
            {
                /* handle que_ack error */
                HandleQueErr(ilRc);
            } /* fi */
        
            switch( prgEvent->command )
            {
            case    HSB_STANDBY :
                ctrl_sta = prgEvent->command;
                break;  
    
            case    HSB_COMING_UP   :
                ctrl_sta = prgEvent->command;
                break;  
    
            case    HSB_ACTIVE  :
                ctrl_sta = prgEvent->command;
                ilBreakOut = TRUE;
                break;  

            case    HSB_ACT_TO_SBY  :
                ctrl_sta = prgEvent->command;
                break;  
    
            case    HSB_DOWN    :
                /* whole system shutdown - do not further use que(), send_message() or timsch() ! */
                ctrl_sta = prgEvent->command;
                Terminate(1);
                break;  
    
            case    HSB_STANDALONE  :
                ctrl_sta = prgEvent->command;
                ResetDBCounter();
                ilBreakOut = TRUE;
                break;  

            case    REMOTE_DB :
                /* ctrl_sta is checked inside */
                HandleRemoteDB(prgEvent);
                break;

            case    SHUTDOWN    :
                Terminate(1);
                break;
                        
            case    RESET       :
                ilRc = Reset();
                break;
                        
            case    EVENT_DATA  :
                dbg(TRACE,"HandleQueues: wrong hsb status <%d>",ctrl_sta);
                DebugPrintItem(TRACE,prgItem);
                DebugPrintEvent(TRACE,prgEvent);
                break;
                    
            case    TRACE_ON :
                dbg_handle_debug(prgEvent->command);
                break;

            case    TRACE_OFF :
                dbg_handle_debug(prgEvent->command);
                break;

            default         :
                dbg(TRACE,"HandleQueues: unknown event");
                DebugPrintItem(TRACE,prgItem);
                DebugPrintEvent(TRACE,prgEvent);
                break;
            } /* end switch */
        } else {
            /* Handle queuing errors */
            HandleQueErr(ilRc);
        } /* end else */
    } while (ilBreakOut == FALSE);

    if(igInitOK == FALSE)
    {
            ilRc = InitRdphdl();
            if(ilRc != RC_SUCCESS)
            {
                dbg(TRACE,"InitDemhdl: init failed!");
            } /* end of if */
    }/* end of if */

} /* end of HandleQueues */
    

/******************************************************************************/
/******************************************************************************/
static int GetRowLength(char *pcpHopo, char *pcpTana, char *pcpFieldList,
                        long *plpLen)
{
    int  ilRc = RC_SUCCESS;         /* Return code */
    int  ilNoOfItems = 0;
    int  ilLoop = 0;
    long llFldLen = 0;
    long llRowLen = 0;
    char clFina[8];
    
    ilNoOfItems = get_no_of_items(pcpFieldList);

    ilLoop = 1;
    do
    {
        ilRc = get_real_item(&clFina[0],pcpFieldList,ilLoop);
        if(ilRc > 0)
        {
            ilRc = GetFieldLength(pcpHopo,pcpTana,&clFina[0],&llFldLen);
            if(ilRc == RC_SUCCESS)
            {
                llRowLen++;
                llRowLen += llFldLen;
            }/* end of if */
        }/* end of if */
        ilLoop++;
    }while((ilRc == RC_SUCCESS) && (ilLoop <= ilNoOfItems));

    if(ilRc == RC_SUCCESS)
    {
        *plpLen = llRowLen;
    }/* end of if */

    return(ilRc);
    
} /* end of GetRowLength */


/******************************************************************************/
/******************************************************************************/
static int GetFieldLength(char *pcpHopo, char *pcpTana, char *pcpFina,
                            long *plpLen)
{
    int  ilRc = RC_SUCCESS;         /* Return code */
    int  ilCnt = 0;
    char clTaFi[32];
    char clFele[16];
    char clFldLst[16];

    ilCnt = 1;
    sprintf(&clTaFi[0],"%s,%s",pcpTana,pcpFina);
    sprintf(&clFldLst[0],"TANA,FINA"); /* wird von syslib zerst�rt */

    /*dbg(TRACE,"GetFieldLength Tana <%s> Fina <%s>",pcpTana,pcpFina);*/

    ilRc = syslibSearchSystabData(&cgTabEnd[0],&clFldLst[0],&clTaFi[0],"FELE",&clFele[0],&ilCnt,"");
    switch (ilRc)
    {
    case RC_SUCCESS :
        *plpLen = atoi(&clFele[0]);
        /* dbg(DEBUG,"GetFieldLength: <%s,%s,%s> <%d>",pcpHopo,pcpTana,pcpFina,*plpLen);*/
        break;

    case RC_NOT_FOUND :
        dbg(TRACE,"GetFieldLength: syslibSSD returned <%d> RC_NOT_FOUND <%s>",ilRc,&clTaFi[0]);
        break;

    case RC_FAIL :
        dbg(TRACE,"GetFieldLength: syslibSSD returned <%d> RC_FAIL",ilRc);
        break;

    default :
        dbg(TRACE,"GetFieldLength: syslibSSD returned <%d> unexpected",ilRc);
        break;
    }/* end of switch */

    return(ilRc);
    
} /* end of GetFieldLength */


/******************************************************************************/
/******************************************************************************/
static int GetCommand(char *pcpCommand, int *pipCmd)
{
    int ilRc = RC_SUCCESS;          /* Return code */
    int ilLoop = 0;
    char clCommand[48];
    
    memset(&clCommand[0],0x00,8);

    ilRc = get_real_item(&clCommand[0],pcpCommand,1);
    if(ilRc > 0)
    {
        ilRc = RC_FAIL;
    }/* end of if */

    while((ilRc != 0) && (prgCmdTxt[ilLoop] != NULL))
    {
        ilRc = strcmp(&clCommand[0],prgCmdTxt[ilLoop]);
        ilLoop++;
    }/* end of while */

    if(ilRc == 0)
    {
        ilLoop--;
        dbg(TRACE,"GetCommand: <%s> <%d>",prgCmdTxt[ilLoop],rgCmdDef[ilLoop]);
        *pipCmd = rgCmdDef[ilLoop];
    }else{
        dbg(TRACE,"GetCommand: <%s> is not valid",&clCommand[0]);
        ilRc = RC_FAIL;
    }/* end of if */

    return(ilRc);
    
} /* end of GetCommand */


static int GetDrrUrnoFromDrs(char *pcpFields,char *pcpData,char *pcpUrno)
{
    int ilRc = RC_SUCCESS;
    char      clDel          = ',';
    int         ilItemNo;
    int     ilCol;
    int         ilPos;
    char        clDrru[24];



    dbg(TRACE,"GetDrrUrnoFromDrs: <%s> <%s>",pcpFields,pcpData);

    ilRc = FindItemInList(pcpFields,"DRRU",clDel,&ilItemNo,&ilCol,&ilPos);
    if (ilRc == RC_SUCCESS)
    {
        GetDataItem(clDrru,pcpData,ilItemNo,clDel,"","\0\0");
        dbg(TRACE,"DRRU found: <%s>",clDrru);
    }
    if (ilRc == RC_SUCCESS)
    {
        strncpy(pcpUrno,clDrru,10);
    }
    else
    {
        dbg(TRACE,"DRR not found ");
    }
    return(ilRc);

}


static int ExtractItem(int ilItemNo, char* pcpSource, 
                                     char* pcpTarget, char cpDelim,int ilMaxLen)
{
    /* delimiter (= item) counter */
    int ilCountDelim = 1;
    /* length of item */
    int ilLength = 0;
    /* char counter */
    int ilPos;

    for (ilPos = 0; ilPos < strlen(pcpSource); ilPos += 1)
    {
        /* this char = delimiter ? */
        if (*(pcpSource+ilPos) == cpDelim)
        {
            /* start of next item in string */
            ilCountDelim += 1;
        }
        /* in scope of item? */
        else if (ilCountDelim == ilItemNo)
        {
            /* max. length reached? */
            /* MCU 20050524: should be > instead of >= */
            if (ilLength > ilMaxLen)
            {
                /* yes -> can't copy byte, print error and terminate */
                dbg(DEBUG,"ExtractItem: items exceeds max. length of <%d>!!!",ilMaxLen);
                return -1;
            }
            /* copy next byte from source */
            *(pcpTarget+ilLength) = *(pcpSource+ilPos);
            ilLength += 1;
            /* zero-terminate target buffer */
            *(pcpTarget+ilLength) = '\0';
        }
        else if (ilCountDelim > ilItemNo)
        {
            /* next item reached -> break loop */
            break;
        }
    }

    /* if we get here and the item number is greater than the counter, 
       there are fewer items in the string than expected */
    if (ilCountDelim < ilItemNo)
    {
        /* dump error info and terminate with error */
        dbg(DEBUG,"ExtractItem: fewer items in string than expected (<%d> items found in '%s')",ilCountDelim,pcpSource);
        return -2;
    }

    /* everything is ok -> return size of item */
    return ilLength;
}


/******************************************************************************/
/* Fill RelData Struct                                                        */
/******************************************************************************/
static int FillRelData(char *pcpData)
{
    int ilRc            = RC_SUCCESS;       /* Return code */
    char clDel          = '|';              /* item delimiter in data string */
    long llDataLen      = 0;                /* length of urno list */
    int ilItemLen       = 0;                /* length of single item (like USEC) in data string */
    int ilTotalItemLen  = 0;                /* length of all params in data string without urno list */
    
    /****Trace pcpData and fill Struct****/
    dbg(DEBUG,"FillRelData: START-->");

    /* extract release parameters from data string and store them in <rgRelData> */
    /* format of data string is supposed to be '<USEC>|<DORS>|<HOPO>|<ROSL>|<RLFR-RLTO>|<... STFU-list>' */
    /* get USEC, max. size = 10 */
    if ((ilItemLen = ExtractItem(1,pcpData,rgRelData.cUsec,clDel,10)) < 0)
    {
        /* error extracting USEC from data string -> terminate function */
        return RC_FAILURE;
    }
    ilTotalItemLen += ilItemLen + 1;
    
    /* get DORS, max. size = 1 */
    if ((ilItemLen = ExtractItem(1,pcpData+ilTotalItemLen,rgRelData.cPlanFlag,clDel,1)) < 0)
    {
        /* error extracting DORS from data string -> terminate function */
        return RC_FAILURE;
    }
    ilTotalItemLen += ilItemLen + 1;

    /* get HOPO, max. size = 3 */
    if ((ilItemLen = ExtractItem(1,pcpData+ilTotalItemLen,rgRelData.cHopo,clDel,3)) < 0)
    {
        /* error extracting HOPO from data string -> terminate function */
        return RC_FAILURE;
    }
    ilTotalItemLen += ilItemLen + 1;

    /* get ROSL, max. size = 1 */
    if ((ilItemLen = ExtractItem(1,pcpData+ilTotalItemLen,rgRelData.cPlanStufe,clDel,1)) < 0)
    {
        /* error extracting ROSL from data string -> terminate function */
        return RC_FAILURE;
    }
    ilTotalItemLen += ilItemLen + 1;

    /* get RLFR-RLTO, max. size = 17 */
    if ((ilItemLen = ExtractItem(1,pcpData+ilTotalItemLen,rgRelData.cDateFrTo,clDel,17)) < 0)
    {
        /* error extracting RLFR-RLTO from data string -> terminate function */
        return RC_FAILURE;
    }
    ilTotalItemLen += ilItemLen + 1;

#if 0
    /* allocate memory for <rgRelData.pcUrnoList> */
    llDataLen = strlen(pcpData) - ilTotalItemLen;
    dbg(DEBUG,"FillRelData: allocating <%d> bytes of memory...",llDataLen+100);
    if (rgRelData.pcUrnoList != NULL)
    {
        free(rgRelData.pcUrnoList);
    }
    if ((rgRelData.pcUrnoList = (char*) malloc(llDataLen+100)) == NULL)
    {
        /* error allocating memory -> terminate function */
        dbg(DEBUG,"FillRelData: error allocating <%d> bytes of memory for <rgRelData.pcUrnoList>....",llDataLen+1);
        return RC_NOMEM;
    }
#endif
    /* copy urno list */
    llDataLen = strlen(pcpData) - ilTotalItemLen;
    strncpy(rgRelData.pcUrnoList,pcpData+ilTotalItemLen,llDataLen);
    rgRelData.pcUrnoList[llDataLen] = '\0';

    dbg(DEBUG,"FillRelData: <-- ENDE ");

    return (ilRc);
}


/******************************************************************************/
/* print RelData Struct                                                       */
/******************************************************************************/
static int PrintRelData(int ipDebugLevel)
{
    dbg(ipDebugLevel,"Print RelData Struct: > > >");
    dbg(ipDebugLevel,"Duty or shift flag = %s",rgRelData.cPlanFlag);
    dbg(ipDebugLevel,"HOPO               = %s",rgRelData.cHopo);
    dbg(ipDebugLevel,"Plan.Step          = %s",rgRelData.cPlanStufe);
    dbg(ipDebugLevel,"DateFrTo           = %s",rgRelData.cDateFrTo);
    dbg(ipDebugLevel,"UrnoList           = %s",rgRelData.pcUrnoList);
    dbg(ipDebugLevel,"< < < Print RelData Struct!");
    return 0;
}


static int UpdateRELTABAfterRelease(char *pcpJob,char *pcpRlat)
{
    int ilRc = RC_SUCCESS;
    char clSelection[512];
    char clStfu[512];
    char clTmpJob[12];
    char clDateTo[24];
    char clDateFrom[24];
    short slCursor;
    short slFunction;
    char clTmpStfu[24];
    int  ilCount = 0;
    int  ilItems = 0;
    int  ilLc = 0;
    BOOL blIsShiftPlan = FALSE;
    long llRowNum;

  char clUrno[32];

  if ( strcmp("N",pcpJob) == 0)
    {
        strcpy(clTmpJob,pcpJob);
    }
    else
    {
        strcpy(clTmpJob,"L");
    }

    if ( strcmp("S",rgRelData.cPlanFlag) == 0)
    {
        *clDateFrom = '\0';
        *clDateTo = '\0';
        blIsShiftPlan = TRUE;
    }
    else
    {
        GetItemNbr(clDateFrom,rgRelData.cDateFrTo,'-',1);
        GetItemNbr(clDateTo,rgRelData.cDateFrTo,'-',2);
    }

    sprintf(cgSqlBuf,"UPDATE RELTAB SET RLAT='%s' WHERE RLFR >= '%s' AND "
    " RLTO <= '%s' AND RLAT = 'x' AND SJOB = '%s' AND DORS = '%s' AND ROSL = '%s'", 
                            pcpRlat,clDateFrom,clDateTo,clTmpJob,
                            rgRelData.cPlanFlag,rgRelData.cPlanStufe);

    dbg(TRACE,"Updating Reltab: SqlBuf <%s>",cgSqlBuf);
                                                                                               
    slCursor = 0;
    slFunction = START;
    ilRc = sql_if(slFunction,&slCursor,cgSqlBuf,cgDataArea);
    if (ilRc != DB_SUCCESS)
    {
        get_ora_err (ilRc, cgErrMsg);
        dbg(TRACE,"Updateing Reltab failed %d<%s>",ilRc,cgErrMsg);
        dbg(TRACE,"Updateing Reltab failed <%s>",cgSqlBuf);
    }
    /*commit_work();*/
    close_my_cursor(&slCursor);

    return ilRc;
}


static int ReadSTFUfromRELTAB(char *pcpJob)
{
    int ilRc = RC_SUCCESS;
    char clSelection[512];
    char clStfu[512];
    char clTmpJob[12];
    char clDateTo[24];
    char clDateFrom[24];
    short slCursor;
    short slFunction;
    char clTmpStfu[24];
    int  ilCount = 0;
    int  ilItems = 0;
    int  ilLc = 0;
    BOOL blIsShiftPlan = FALSE;
    long llRowNum;

  char clUrno[32];

    /***
  if ( strcmp("N",pcpJob) == 0)
    {
        strcpy(clTmpJob,pcpJob);
    }
    else
    {
        strcpy(clTmpJob,"L");
    }
    *******/
    /* MCU 20040330 ReadSTFUfromRELTAB is used only for night jobs now */
    strcpy(clTmpJob,"L");

    if ( strcmp("S",rgRelData.cPlanFlag) == 0)
    {
        *clDateFrom = '\0';
        *clDateTo = '\0';
        blIsShiftPlan = TRUE;
    }
    else
    {
        GetItemNbr(clDateFrom,rgRelData.cDateFrTo,'-',1);
        GetItemNbr(clDateTo,rgRelData.cDateFrTo,'-',2);
    }

    sprintf(cgSqlBuf,"SELECT DISTINCT RLFR,RLTO FROM RELTAB WHERE"
    " RLAT = 'x' AND SJOB = 'L' AND DORS = 'D' AND ROSL = '%s'",rgRelData.cPlanStufe); 
    dbg(TRACE,"%05d: <%s>",__LINE__,cgSqlBuf);
    slCursor = 0;
    slFunction = START;
    *cgReltabStfu = '\0';
    ilRc = sql_if(slFunction,&slCursor,cgSqlBuf,cgDataArea);
    close_my_cursor(&slCursor);
    if (ilRc == RC_SUCCESS)
    {
        char *pclData;

        pclData = cgDataArea;

        strcpy(clDateFrom,pclData);
        pclData += strlen(pclData) + 1;
        strcpy(clDateTo,pclData);
        strcpy(rgRelData.cPlanFlag,"D");

        sprintf(rgRelData.cDateFrTo,"%s-%s",clDateFrom,clDateTo);
        dbg(TRACE,"from <%s> to <%s>",clDateFrom,clDateTo);

    sprintf(cgSqlBuf,"SELECT STFU FROM RELTAB WHERE RLFR >= '%s' AND "
    " RLTO <= '%s' AND RLAT = 'x' AND SJOB = '%s' AND DORS = '%s' AND ROSL = '%s'", 
                            clDateFrom,clDateTo,"L","D",rgRelData.cPlanStufe);

    dbg(TRACE,"Reading Reltab: SqlBuf <%s>",cgSqlBuf);
                                                                                               
    slCursor = 0;
    slFunction = START;
    *cgReltabStfu = '\0';
    while((ilRc = sql_if(slFunction,&slCursor,cgSqlBuf,cgDataArea)) == DB_SUCCESS)
    {
        slFunction = NEXT;
        sprintf(clTmpStfu,"'%s',",cgDataArea);
        strcat(cgReltabStfu,clTmpStfu);
        ilCount++;
    }
    close_my_cursor(&slCursor);
    if (ilRc != RC_NOTFOUND)
    {
        get_ora_err (ilRc, cgErrMsg);
        dbg(TRACE,"Reading Reltab failed %d<%s>",ilRc,cgErrMsg);
        dbg(TRACE,"Reading Reltab failed <%s>",cgSqlBuf);
    }
    else
    {
        dbg(TRACE,"%d STFUs from RELTAB read",ilCount);
        ilRc = RC_SUCCESS;
    }

if (cgReltabStfu[strlen(cgReltabStfu)-1] == ',')
{
    cgReltabStfu[strlen(cgReltabStfu)-1] = '\0';
}
    }
    else
    {
        dbg(TRACE,"No STFUs read %d",ilRc);
        ilRc = RC_FAIL;
    }
dbg(DEBUG,"STFUs: <%s>",cgReltabStfu);
    if (*cgReltabStfu == '\0')
    {
        return RC_FAIL;
    }
    return ilRc;
}

/******************************************************************************/
/* fill Array RELTAB with data                                                */
/******************************************************************************/
static int InitRELArray(char *pcpJob)
{
    int ilRc = RC_SUCCESS;
    char clSelection[512];
    char clStfu[512];
    char clTmpJob[12];
    char clDateTo[24];
    char clDateFrom[24];
    short slCursor;
    short slFunction;
    char clTmpStfu[24];
    int  ilCount = 0;
    int  ilItems = 0;
    int  ilLc = 0;
    BOOL blIsShiftPlan = FALSE;
    long llRowNum;

  char clUrno[32];
    char clNow[32]; 

  if ( strcmp("N",pcpJob) == 0)
    {
        strcpy(clTmpJob,pcpJob);
    }
    else
    {
        strcpy(clTmpJob,"L");
    }

    if ( strcmp("S",rgRelData.cPlanFlag) == 0)
    {
        *clDateFrom = '\0';
        *clDateTo = '\0';
        blIsShiftPlan = TRUE;
    }
    else
    {
        GetItemNbr(clDateFrom,rgRelData.cDateFrTo,'-',1);
        GetItemNbr(clDateTo,rgRelData.cDateFrTo,'-',2);
    }


    sprintf(cgSqlBuf,"SELECT STFU FROM RELTAB WHERE RLFR = '%s' AND "
    " RLTO = '%s' AND RLAT = 'x' AND SJOB = '%s' AND DORS = '%s' AND ROSL = '%s'", 
                            clDateFrom,clDateTo,clTmpJob,
                            rgRelData.cPlanFlag,rgRelData.cPlanStufe);

    dbg(TRACE,"Reading Reltab: SqlBuf <%s>",cgSqlBuf);
                                                                                               
    slCursor = 0;
    slFunction = START;
    *cgReltabStfu = '\0';
    while((ilRc = sql_if(slFunction,&slCursor,cgSqlBuf,cgDataArea)) == DB_SUCCESS)
    {
        slFunction = NEXT;
        ilCount++;

        if (*cgReltabStfu == '\0')
        {
            sprintf(clTmpStfu,"'%s'",cgDataArea);
        }
        else
        {
            sprintf(clTmpStfu,",'%s'",cgDataArea);
        }
        strcat(cgReltabStfu,clTmpStfu);
    }
    close_my_cursor(&slCursor);
    if (ilRc != RC_NOTFOUND)
    {
        get_ora_err (ilRc, cgErrMsg);
        dbg(TRACE,"Reading Reltab failed %d<%s>",ilRc,cgErrMsg);
        dbg(TRACE,"Reading Reltab failed <%s>",cgSqlBuf);
    }
    else
    {
        dbg(TRACE,"%d STFUs from RELTAB read",ilCount);
        ilRc = RC_SUCCESS;
    }

dbg(DEBUG,"STFUs: <%s>",cgReltabStfu);

/*define REL_FIELDS "CDAT,DORS,HOPO,LSTU,RLAT,RLFR,RLTO,ROSL,SJOB,SPLU,STFU,URNO,USEC,USEU"*/
    /** initialize RELTAB record **/
    memset(cgRelBuf,0,rgRelArray.lrArrayRowLen);

    GetServerTimeStamp("UTC", 1, 0, clNow);
                      
    strcpy(RELFIELD(cgRelBuf,igRelCDAT),clNow);
    strcpy(RELFIELD(cgRelBuf,igRelDORS),rgRelData.cPlanFlag);
    strcpy(RELFIELD(cgRelBuf,igRelHOPO),rgRelData.cHopo);
    strcpy(RELFIELD(cgRelBuf,igRelLSTU),"              ");
    strcpy(RELFIELD(cgRelBuf,igRelRLAT),"x             ");
    strcpy(RELFIELD(cgRelBuf,igRelRLFR),clDateFrom);
    strcpy(RELFIELD(cgRelBuf,igRelRLTO),clDateTo);
    strcpy(RELFIELD(cgRelBuf,igRelROSL),rgRelData.cPlanStufe);
    strcpy(RELFIELD(cgRelBuf,igRelSJOB),pcpJob);
    strcpy(RELFIELD(cgRelBuf,igRelSPLU),"0         ");
    strcpy(RELFIELD(cgRelBuf,igRelSTFU),"0         ");
    strcpy(RELFIELD(cgRelBuf,igRelURNO),"0         ");
    /*strcpy(RELFIELD(cgRelBuf,igRelUSEC),rgRelData.cUsec);*/
    sprintf(RELFIELD(cgRelBuf,igRelUSEC),"%-33s",rgRelData.cUsec);
    sprintf(RELFIELD(cgRelBuf,igRelUSEU),"%-33s",rgRelData.cUsec);

    ilItems = ItemCnt(rgRelData.pcUrnoList,'-');
    dbg(DEBUG,"Urnolist: <%s> %d Items",rgRelData.pcUrnoList,ilItems);
    for(ilLc = 0; ilLc < ilItems; ilLc++)
    {
        if (blIsShiftPlan == FALSE)
        {
            GetItemNbr(clStfu,rgRelData.pcUrnoList,'-',ilLc+1);
            TrimRight(clStfu);
            dbg(DEBUG,"Processing %2d STFU <%s>",ilLc,clStfu);
            if (strstr(cgReltabStfu,clStfu) == NULL)
            {
                char clTmpUrno[24];

            GetNextValues(clTmpUrno,1);
            sprintf(clUrno,"%-10s",clTmpUrno);
                dbg(DEBUG,"no RELTAB entry for STFU=<%s>, create new one with URNO=<%s>",
                        clStfu,clUrno);

                if (*clTmpJob != 'N')
                {
                    /** command is not RDN, store STFU in RELTAB for later use **/
                    /** else add clStfu to cgReltabStfu **/
                sprintf(RELFIELD(cgRelBuf,igRelURNO),"%-10s",clUrno);
                sprintf(RELFIELD(cgRelBuf,igRelSTFU),"%-10s",clStfu);
                llRowNum = ARR_FIRST;
                ilRc = CEDAArrayAddRow(&rgRelArray.rrArrayHandle,rgRelArray.crArrayName,
                    &llRowNum,(void *)cgRelBuf);
                if (ilRc != RC_SUCCESS)
                {
                    dbg(TRACE,"Add new RELTAB row failed RC=%d",ilRc);
                }
                else
                {
                    char *pclRelRow;

                    dbg(DEBUG,"AddRow successful RowNum = %ld",llRowNum);
                    if (debug_level == DEBUG)
                    {
                    if(CEDAArrayGetRowPointer(&rgRelArray.rrArrayHandle,
                                    rgRelArray.crArrayName,llRowNum,(void *)&pclRelRow) == RC_SUCCESS)       
                        {
                            int ilFieldNo;

                            for (ilFieldNo = 1; ilFieldNo < rgRelArray.lrArrayFieldCnt; ilFieldNo++)
                            {
                                    char pclFieldName[24];

                                    *pclFieldName = '\0';
                                    GetDataItem(pclFieldName,rgRelArray.crArrayFieldList,
                                                                                    ilFieldNo,',',"","\0\0");
                                    dbg(DEBUG,"%s: <%s>",pclFieldName,RELFIELD(cgRelBuf,ilFieldNo-1));
                                    dbg(DEBUG,"%s: <%s>",pclFieldName,RELFIELD(pclRelRow,ilFieldNo-1));
                            }
                        }
                        }
                }
                } /**  if (*clTmpJob != 'N')   **/
                else
                {
                    /** command is RDN, store STFU in cgReltabStfu for later use **/

                    if (*cgReltabStfu == '\0')
                    {
                        sprintf(clTmpStfu,"'%s'",clStfu);
                    }
                    else
                    {
                        sprintf(clTmpStfu,",'%s'",clStfu);
                    }
                    strcat(cgReltabStfu,clTmpStfu);
                }
            }
        }
    }
                if (*clTmpJob != 'N')
                {
                    /** command is not RDN, write RELTAB for later use **/
            CEDAArrayWriteDB(&rgRelArray.rrArrayHandle,
                        rgRelArray.crArrayName, NULL, NULL, NULL, NULL,ARR_COMMIT_ALL_OK);
            }
    

    return ilRc;

}
/******************************************************************************/
/* The handle (later) routine                                                 */
/******************************************************************************/
static int HandleDataArray(char *pclData, char *pcpJob)
{
    int ilRc = RC_SUCCESS;              /* Return code */
    
    dbg(TRACE,"HandleDataArray: ");
    ilRc = FillRelData(pclData);           /* fills only relstruct*/
    PrintRelData(TRACE);

    if (*rgRelData.cPlanFlag == 'D')
    {
        /*fill RELTAB*/
        ilRc = InitRELArray(pcpJob);      /*fill RELTAB Array with data*/
    
        dbg(TRACE,"HandleDataArray: finished ");
    }
    else
    {
        dbg(TRACE,"Plan Flag is <%s> work is done by RELDPL",*rgRelData.cPlanFlag);
        ilRc = FALSE;
    }

    return (ilRc);
}                   

static int UpdateOldDrr()
{

    int ilRc = RC_SUCCESS;              /* Return code */

  long llRowNum = ARR_CURRENT;
    long llRoss = -1;
  long llUseu = -1;
  long llLstu = -1;
    char clNow[32]; 
    char clStfu[32];

    GetServerTimeStamp("UTC", 1, 0, clNow);

    CEDAArrayPutField(&rgDrrArray.rrArrayHandle,
        rgDrrArray.crArrayName,&llRoss,"ROSS",llRowNum,"L");

  CEDAArrayGetField(&rgDrrArray.rrArrayHandle,rgDrrArray.crArrayName,
         NULL , "STFU",12, llRowNum,clStfu);
    if (strstr(rgRelData.pcUrnoList2,clStfu) == NULL)
    {
        char clUrno[32];

        if (*rgRelData.pcUrnoList2 != '\0')
        {
            sprintf(clUrno,"-%s",clStfu);
            strcat(rgRelData.pcUrnoList2,clUrno);
        }
        else
        {
            strcat(rgRelData.pcUrnoList2,clStfu);
        }
    }
    /************
    CEDAArrayPutField(&rgDrrArray.rrArrayHandle,
        rgDrrArray.crArrayName,&llUseu,"USEU",llRowNum,"RELDPL");

    CEDAArrayPutField(&rgDrrArray.rrArrayHandle,
        rgDrrArray.crArrayName,&llLstu,"LSTU",llRowNum,clNow);

        ************/

    return ilRc;
}

static int CheckUsageToday(int ipSection)
{
    int ilRc = RC_FAIL; 
    struct tm *prlTmToday;

    if (strcmp(rgCfgData[ipSection].cPeriod,"DAILY") == 0)
    {
        dbg(TRACE,"We do this section today because it should be done daily");
        ilRc=RC_SUCCESS;  /** daily usage, we should do if today **/
    }
    else if (strcmp(rgCfgData[ipSection].cPeriod,"DAY_OF_WEEK") == 0)
    {
        time_t tlNow;
      int ilWeekDay;

        
        /** we check the day of week **/
        dbg(DEBUG,"we check day of week");
        tlNow = time(NULL);
        prlTmToday = gmtime(&tlNow);
        ilWeekDay = prlTmToday->tm_wday;
        if ((strstr(rgCfgData[ipSection].cSequence,cgWeekDays[ilWeekDay]) != NULL) ||
            (strstr(rgCfgData[ipSection].cSequence,cgWeekDaysEng[ilWeekDay]) != NULL))
        {
                dbg(TRACE,"We do this section because the day of week <%s> is in REL_SEQUENCE",
                        cgWeekDaysEng[ilWeekDay]);
                ilRc = RC_SUCCESS; 
        }
        else
        {
                dbg(TRACE,"We don't do this section because the day of week <%s> is not in REL_SEQUENCE",
                        cgWeekDaysEng[ilWeekDay]);
                dbg(TRACE,"REL_SEQUENCE is <%s>",rgCfgData[ipSection].cSequence);
        }
    }
    else if (strcmp(rgCfgData[ipSection].cPeriod,"ABSOLUTE_DATE") == 0)  
    {
        time_t tlNow;
        char clToday[16];
        
        dbg(DEBUG,"we check absolute date");
        tlNow = time(NULL);
        prlTmToday = gmtime(&tlNow);
        sprintf(clToday,"%02d/%02d",prlTmToday->tm_mon+1,prlTmToday->tm_mday);
        if (strstr(rgCfgData[ipSection].cSequence,clToday) != NULL)
        {
                dbg(TRACE,"We do this section because today <%s> is in REL_SEQUENCE",
                        clToday);
                ilRc = RC_SUCCESS; 
        }
        else
        {
                dbg(TRACE,"We don't do this section because today <%s> is not in REL_SEQUENCE",
                        clToday);
                dbg(TRACE,"REL_SEQUENCE is <%s>",rgCfgData[ipSection].cSequence);
        }
    }

    return ilRc;
}




static int GetDateFrToForDays(int ipSection,char *cpDateFrTo)
{
    int ilRc = RC_SUCCESS;             
    char clNow[24];
    char clDate1[24];
    char clDate2[24];
    time_t tlNow;
    int ilOffset;
    int ilDuration;

    ilDuration = (atoi(rgCfgData[ipSection].cDuration)-1) * 86400;
    ilOffset = atoi(rgCfgData[ipSection].cOffset) * 86400;

    tlNow = time(NULL);
    TimeToStr(clDate1,tlNow);
  AddSecondsToCEDATime(clDate1,ilOffset,1);
    strcpy(clDate2,clDate1);
  AddSecondsToCEDATime(clDate2,ilDuration,1);
    clDate1[8] = '\0';
    clDate2[8] = '\0';

    if(strcmp(clDate1,clDate2) < 0) 
    {
        sprintf(cpDateFrTo,"%s-%s",clDate1,clDate2);
    }
    else 
    {
        sprintf(cpDateFrTo,"%s-%s",clDate2,clDate1);
    }
    dbg(TRACE,"Release for %d days <%s>",
            atoi(rgCfgData[ipSection].cDuration),cpDateFrTo);
    return ilRc;
}


static int GetDateFrToForMonths(int ipSection,char *cpDateFrTo) 
{
    int ilRc = RC_SUCCESS;             
    char clNow[24];
    char clDate1[24];
    char clDate2[24];
    char clDateFrTo[24];
    char clMonth1[24];
    char clMonth2[24];
    time_t tlNow;
    int ilOffset;
    int ilDuration;
    char clYear[6];
    int ilYear1;
    int ilYear2;
    int ilDays;
    int ilMonth;
    int ilMonth1;
    int ilMonth2;

    ilDuration = atoi(rgCfgData[ipSection].cDuration) - 1;
    ilOffset = atoi(rgCfgData[ipSection].cOffset);

    tlNow = time(NULL);
    TimeToStr(clNow,tlNow);
    strncpy(clMonth1,&clNow[4],2);
    clMonth1[2] = '\0';

    ilMonth = atoi(clMonth1);


    /*** calculate first month **/
    strncpy(clYear,clNow,4);
    ilYear1 = atoi(clYear);
    ilMonth1 = ilMonth + ilOffset;
    if (ilMonth1 < 1)
    {
        ilYear1--;
    }
    if (ilMonth1 > 12)
    {
        ilYear1++;
    }
    sprintf(clDate1,"%04d%02d",ilYear1,ilMonth1);

    /*** calculate second month **/
    ilYear2 = ilYear1;
    ilMonth2 = ilMonth1;
  ilMonth2 += ilDuration;
    if (ilMonth2 < 1)
    {
        ilYear2--;
    }
    if (ilMonth2 > 12)
    {
        ilYear2++;
    }
    sprintf(clDate2,"%04d%02d",ilYear2,ilMonth2);

    if(strcmp(clDate1,clDate2) < 0) 
    {
        ilDays = GetLengtOfMonth(ilMonth2,ilYear2);
        sprintf(cpDateFrTo,"%s01-%s%02d",clDate1,clDate2,ilDays);
    }
    else 
    {
        ilDays = GetLengtOfMonth(ilMonth1,ilYear1);
        sprintf(cpDateFrTo,"%s01-%s%02d",clDate2,clDate1,ilDays);
    }

    dbg(TRACE,"Release for %d months <%s>",atoi(rgCfgData[ipSection].cDuration),cpDateFrTo);
    
    return ilRc;
}




static int FillRelDataFromConfig(int ipSection,char *pcpNextRosl)
{
    int ilRc = RC_SUCCESS;              /* Return code */

    strcpy(rgRelData.cUsec,"RELDPL");
    strcpy(rgRelData.cPlanFlag,"D");
    strcpy(rgRelData.cHopo,cgHopo);

    ilRc = CheckUsageToday(ipSection);
    
  dbg(TRACE,"FillRelDataFromConfig RC from CheckUsageToday=%d",ilRc);   
    if (ilRc == RC_SUCCESS)
    {

        if (strcmp(rgCfgData[ipSection].cType,"DAY") == 0)
        {
            ilRc=GetDateFrToForDays(ipSection,rgRelData.cDateFrTo);
        }
        else if (strcmp(rgCfgData[ipSection].cType,"MONTH") == 0)
        {
            ilRc=GetDateFrToForMonths(ipSection,rgRelData.cDateFrTo);   
        }
        else
        {
            ilRc = RC_FAIL; /** invalid configuration **/
            dbg(TRACE,"Invalid TYPE or PERIOD in Section: <%s>",
                            rgCfgData[ipSection].cSectionName);
        }

    }
    return ilRc;
}


static int ReadSTFUfromStfTabAndRelease(char *pcpNextRosl)
{
    int ilRc = RC_SUCCESS;              /* Return code */
    short slCursor = 0;
    int slFunction = START;
    char clTmpStfu[24];
    int ilCount = 0;
    char clData[124];
    
    strcpy(cgSqlBuf,"SELECT URNO FROM STFTAB WHERE RELS = '1'");
    *cgReltabStfu = '\0';
    while((ilRc = sql_if(slFunction,&slCursor,cgSqlBuf,cgDataArea)) == DB_SUCCESS)
    {
        slFunction = NEXT;
      if (*cgReltabStfu == '\0')
        {
            sprintf(clTmpStfu,"'%s'",cgDataArea);
        }
        else
        {
            sprintf(clTmpStfu,",'%s'",cgDataArea);
        }
        strcat(cgReltabStfu,clTmpStfu);
        ilCount++;
        if (ilCount > 500)
        {
            HandleDrrDataNow(clData,pcpNextRosl,"N");
            *cgReltabStfu = '\0';
            ilCount = 0;
        }
    }
    close_my_cursor(&slCursor);
    if (ilCount > 0)
    {
        HandleDrrDataNow(clData,pcpNextRosl,"N");
        *cgReltabStfu = '\0';
        ilCount = 0;
    }

    return ilRc;
}

static int DeleteOldReltabEntries()
{
    int ilRc = RC_SUCCESS;
    short slCursor;
    char clTmpSqlString[1024];
    char clTmpDate[24];
    time_t tlNow;

    tlNow = time(NULL);
    
    TimeToStr(clTmpDate,tlNow);
    AddSecondsToCEDATime(clTmpDate,igReltabOffset,1);
    clTmpDate[8] = '\0';
    sprintf(cgSqlBuf,"DELETE FROM RELTAB WHERE RLTO < '%s'",clTmpDate);

    dbg(TRACE,"%05d: <%s>",__LINE__,cgSqlBuf);

    slCursor = 0;
    ilRc = sql_if(START,&slCursor,cgSqlBuf,cgDataArea);
    if (ilRc != DB_SUCCESS)
    {
        get_ora_err (ilRc, cgErrMsg);
        dbg(TRACE,"Updateing Reltab failed %d<%s>",ilRc,cgErrMsg);
        dbg(TRACE,"Updateing Reltab failed <%s>",cgSqlBuf);
    }
    commit_work();
    close_my_cursor(&slCursor);

    return ilRc;
}


static int ReleaseFromConfig(char *pcpRosl,char *pcpNextRosl)
{
    int ilRc = RC_SUCCESS;              /* Return code */
    int ilLc;

  if (ilRc == RC_SUCCESS)
    {
        for(ilLc = 0; ilLc < igSections; ilLc++)
        {
            if (strcmp(rgCfgData[ilLc].cLevel,pcpRosl) == 0)
            {
                dbg(TRACE,"Section <%s>, Level %s",
                            rgCfgData[ilLc].cSectionName,rgCfgData[ilLc].cLevel);
                ilRc = FillRelDataFromConfig(ilLc,pcpNextRosl);
                dbg(TRACE,"RC from FillRelDataFromConfig = %d",ilRc);
                if (ilRc == RC_SUCCESS)
                {
                    ilRc = ReadSTFUfromStfTabAndRelease(pcpNextRosl);
                }
                break; /** only one level for each section **/
            }
        }
    }
    return ilRc;
}


static int HandleDrrDataNow(char *pclData,char *pcpNextRosl,char *pcpJob)
{
    int ilRc = RC_SUCCESS;              /* Return code */
    char clDateTo[24];
    char clDateFrom[24];
    long llRoss = -1,llRosl = -1,llUsec = -1,llUseu = -1,llCdat = -1,llLstu = -1;
    long llRowNum;
    char *pclRowBuf = NULL;
    char *pclStfRowBuf = NULL;
    char clDrsFlag[24];
    BOOL blIs1To2 = FALSE;
    char clRoss[24] = "";
    char clScod[24] = "";
/*  char clDontChange[] =  "URNO,ROSS,ROSL,LSTU";*/
     char clDontChange[] = "URNO,ROSS,ROSL"; /* don't set LSTU, copy from old DRR */
    char clNow[32]; 
    char clAddData[32]; 
    char clTest[24];

    GetServerTimeStamp("UTC", 1, 0, clNow);

    if (*pcpJob != 'N')
    {
        /*** read RELTAB entries to release **/
        ilRc = ReadSTFUfromRELTAB("N");
        if (ilRc != RC_SUCCESS)
        {
            return(ilRc);
        }
    }
    /*** else use already prepared cgReltabStfu **/

    dbg(TRACE,"Release job <%s> from/to <%s> Level from/to <%s/%s>",
                pcpJob,rgRelData.cDateFrTo,rgRelData.cPlanStufe,pcpNextRosl);

    *rgRelData.pcUrnoList2 = '\0';  

    if (*cgReltabStfu == '\0')
    {
        dbg(TRACE,"no STFUs to release");
        return RC_SUCCESS;
    }

    GetItemNbr(clDateFrom,rgRelData.cDateFrTo,'-',1);
    GetItemNbr(clDateTo,rgRelData.cDateFrTo,'-',2);

    if (*rgRelData.cPlanStufe == '1' && *pcpNextRosl == '2')
    {
        dbg(TRACE,"Release from level 1 to level 2, special handling required");
        blIs1To2 = TRUE;;
        *clRoss = '\0';
        sprintf(cgSelection,"WHERE STFU IN (%s) AND SDAY BETWEEN '%s' AND '%s' AND ROSL = '%s'",
            cgReltabStfu,clDateFrom,clDateTo,pcpNextRosl);
        dbg(DEBUG, "HandleDrrDataNow, read New Drr: Selection %s", cgSelection);
        strcpy(clAddData,"Y,O");
        ilRc = CEDAArrayRefillHint(&rgNewDrrArray.rrArrayHandle,
                    rgNewDrrArray.crArrayName,
                    cgSelection,clAddData,ARR_FIRST,cgDrrReloadHint);
        {
        llRowNum = ARR_FIRST;
    while(CEDAArrayGetRowPointer(&rgNewDrrArray.rrArrayHandle,
        rgNewDrrArray.crArrayName,llRowNum,(void *)&pclRowBuf) == RC_SUCCESS)       
    {
    /*** set the USED field to avoid unwanted deletions **/
            long llFieldNo = -1;
        llRowNum = ARR_CURRENT;
          CEDAArrayPutField(&rgNewDrrArray.rrArrayHandle,
            rgNewDrrArray.crArrayName,
            &llFieldNo,"USED",llRowNum,"Y");
        llRowNum = ARR_NEXT;
    }
    }
    }
    else
    {
        strcpy(clRoss,"AND ROSS = 'A'");
    }

    sprintf(cgSelection,"WHERE STFU IN (%s) AND SDAY BETWEEN '%s' AND '%s' AND ROSL = '%s' %s",
        cgReltabStfu,clDateFrom,clDateTo,rgRelData.cPlanStufe,clRoss);
    dbg(DEBUG, "HandleDrrDataNow: Selection %s", cgSelection);
    ilRc = CEDAArrayRefillHint(&rgDrrArray.rrArrayHandle,
                rgDrrArray.crArrayName,
                cgSelection,"",ARR_FIRST,cgDrrReloadHint);

    llRowNum = ARR_FIRST;

    while(CEDAArrayGetRowPointer(&rgDrrArray.rrArrayHandle,rgDrrArray.crArrayName,llRowNum,(void *)&pclRowBuf) == RC_SUCCESS)       
    {

        long llStfRowNum;
        long llSDAY;
        /*Eintrittsdatum*/
        long llDOEM = 0;
        /*Austrittsdatum*/
        long llDODM = 0;
        char clDOEM[20];
        char clDODM[20];
        char clSDAY[20];

        long llNewRowNum = ARR_FIRST;
        char clTmpUrno[24];
        char *pclNewDrrRow;
        char clKey[124];

        /*check, if employee is employed at this day*/
        sprintf(clKey, "%s", NEWDRRFIELD(pclRowBuf,igDrrSTFU));
        dbg(DEBUG,"%05d: Looking for employee with the STFU  = <%s>",__LINE__,clKey);     
        llStfRowNum = ARR_FIRST;
        ilRc = CEDAArrayFindRowPointer( &(rgStfArray.rrArrayHandle),
                                      &(rgStfArray.crArrayName[0]),
                                      &(rgStfArray.rrIdx03Handle),
                                      &(rgStfArray.crIdx03Name[0]),
                                      clKey,&llStfRowNum,
                                      (void *) &pclStfRowBuf);
                        
        if (ilRc == RC_SUCCESS)
        {
            strcpy(clDOEM,STFFIELD(pclStfRowBuf,igStfDOEM));
            strcpy(clDODM,STFFIELD(pclStfRowBuf,igStfDODM));
            strcpy (clSDAY,NEWDRRFIELD(pclRowBuf,igDrrSDAY));
            dbg(DEBUG,"Entry date of the employee = <%s>",clDOEM);
            dbg(DEBUG,"Exit date of the employee = <%s>",clDODM);

            if (strlen(clDOEM) > 8)
            {
                clDOEM[8] = '\0';
                llDOEM = atol(clDOEM);
            }

            if (strlen(clDODM) > 8)
            {
                clDODM[8] = '\0';
                llDODM = atol(clDODM);
            }

            if (llDODM == 0)
            {
                llDODM = 99999999;
            }

            dbg(DEBUG,"Compare with shiftday = <%s> DOEM = <%s> DODM = <%s>",clSDAY,clDOEM,clDODM);
            llSDAY = atol(clSDAY);

            if (llDOEM <= llSDAY && llSDAY <= llDODM)
            {
                /*** check if new DRR record already exist ***/
                sprintf(clKey,"%s,%s,%s",
                        NEWDRRFIELD(pclRowBuf,igDrrSTFU),
                        NEWDRRFIELD(pclRowBuf,igDrrSDAY),
                        NEWDRRFIELD(pclRowBuf,igDrrDRRN));

                dbg(DEBUG,"%05d:Reading already existing DRR Key=<%s>",__LINE__,clKey);     
                
                
                llNewRowNum = ARR_FIRST;
                ilRc = CEDAArrayFindRowPointer( &(rgNewDrrArray.rrArrayHandle),
                                              &(rgNewDrrArray.crArrayName[0]),
                                                                                    /*********
                                              &(rgNewDrrArray.rrIdx03Handle),
                                              &(rgNewDrrArray.crIdx03Name[0]),
                                                                                    **********/
                                              &(rgNewDrrArray.rrIdx01Handle),
                                              &(rgNewDrrArray.crIdx01Name[0]),
                                              clKey,&llNewRowNum,
                                              (void *) &pclNewDrrRow);
    
                    dbg(DEBUG,"%05d: RC from FindRowPointer is %d found: <%s>",
                        __LINE__,ilRc,ilRc == RC_SUCCESS ?  
                                            NEWDRRFIELD(pclNewDrrRow,igDrrSTFU) : "nothing" );
                if (ilRc == RC_SUCCESS)
                {
                        long llFieldNo;
                    char clRoss[24];
                    char clRosl[24];
                    char *pclOdaRow;
                    long llOdaRowNum = ARR_FIRST;
                    int ilLc;
                    char *pclTmpDrr;
                    char *pclTmpNew;
        
                    strcpy(clRoss,NEWDRRFIELD(pclNewDrrRow,igDrrROSS)); 
                    strcpy(clRosl,NEWDRRFIELD(pclNewDrrRow,igDrrROSL)); 
                    if (*clRoss != 'A' || *clRosl == '1')
                    {
                        dbg(DEBUG,"DRR exist already but is not active ROSS=<%s> or is Level 1 ROSL=<%s>",clRoss,clRosl);
                        /** set USED, so we don't delete it later **/
                        /***
                        llFieldNo = -1;
                                CEDAArrayPutField(&rgNewDrrArray.rrArrayHandle,
                                    rgNewDrrArray.crArrayName,
                                    &llFieldNo,"USED",llNewRowNum,"Y");
                        *****/
                        llRowNum = ARR_NEXT;
                        continue;
                    }
                    /* check if ist is not an absence */
                    strcpy(clScod,NEWDRRFIELD(pclNewDrrRow,igDrrSCOD)); 
                    llOdaRowNum = ARR_FIRST;
                    ilRc = CEDAArrayFindRowPointer( &(rgOdaArray.rrArrayHandle),
                                              &(rgOdaArray.crArrayName[0]),
                                              &(rgOdaArray.rrIdx02Handle),
                                              &(rgOdaArray.crIdx02Name[0]),
                                              clScod,&llOdaRowNum,
                                              (void *) &pclOdaRow);                             
                    if (ilRc == RC_SUCCESS)
                    {
                        dbg(DEBUG,"DRR exist already but is an absence code <%s>",clScod);
                        /** set USED, so we don't delete it later **/
                        /***
                        llFieldNo = -1;
                                CEDAArrayPutField(&rgNewDrrArray.rrArrayHandle,
                                    rgNewDrrArray.crArrayName,
                                    &llFieldNo,"USED",llNewRowNum,"Y");
                        **************/
                        llRowNum = ARR_NEXT;
                        continue;
                    }
                    /*** record may be deleted **/
                    llFieldNo = -1;
              CEDAArrayPutField(&rgNewDrrArray.rrArrayHandle,
                rgNewDrrArray.crArrayName,
                 &llFieldNo,"USED",llNewRowNum,"N");
        
                    dbg(DEBUG,"existing DRR with level <%s> is active <%s> and not an absence code <%s>",
                        NEWDRRFIELD(pclNewDrrRow,igDrrROSL),clRoss,clScod);
                    dbg(DEBUG,"we will overwrite it");
                    UpdateOldDrr();
        
                    /* copy DRR record from old level to new level */
                    pclTmpDrr = pclRowBuf;
                    pclTmpNew = pclNewDrrRow;
              
              for(ilLc = 0; ilLc < rgDrrArray.lrArrayFieldCnt; ilLc++)
              {
                        char clFieldName[24];
        
                *clFieldName = '\0';
                GetDataItem(clFieldName,rgDrrArray.crArrayFieldList,ilLc+1,',',"","\0\0");
                if(strstr(clDontChange, clFieldName) == NULL)
                {
                    dbg(DEBUG,"%05d:%s <%s> overwrites <%s>",__LINE__,clFieldName,pclTmpDrr,pclTmpNew);
                  llFieldNo = ilLc;
                  CEDAArrayPutField(&rgNewDrrArray.rrArrayHandle,
                    rgNewDrrArray.crArrayName,
                    &llFieldNo,clFieldName,llNewRowNum,
                    pclTmpDrr);
                }
                pclTmpDrr += strlen(pclTmpDrr)+1;
                pclTmpNew += strlen(pclTmpNew)+1;
              }
                    /*** set new ROSS,USEU**/
                    llFieldNo = -1;
                        CEDAArrayPutField(&rgNewDrrArray.rrArrayHandle,
                                rgNewDrrArray.crArrayName,
                                &llFieldNo,"ROSS",llNewRowNum,"A");
        
                    llFieldNo = -1;
                    /************
                        CEDAArrayPutField(&rgNewDrrArray.rrArrayHandle,
                                rgNewDrrArray.crArrayName,
                                &llFieldNo,"USEU",llNewRowNum,"RELDPL");
        
                    llFieldNo = -1;
                        CEDAArrayPutField(&rgNewDrrArray.rrArrayHandle,
                                rgNewDrrArray.crArrayName,
                                &llFieldNo,"LSTU",llNewRowNum,clNow);
        
                                                **********/

                    /** set USED, so we don't delete it later **/
                    llFieldNo = -1;
                        CEDAArrayPutField(&rgNewDrrArray.rrArrayHandle,
                                rgNewDrrArray.crArrayName,
                                &llFieldNo,"USED",llNewRowNum,"Y ");
                }
                else
                {
        
                    /*** copy DRR record ****/
                    memcpy(cgDrrBuf,pclRowBuf,rgDrrArray.lrArrayRowLen);
        
                    llRowNum = ARR_CURRENT;
                    /*** update old DRR record **/
                    UpdateOldDrr();
        
                    /*** change data in new DRR Record ***/
                    strcpy(NEWDRRFIELD(cgDrrBuf,igDrrROSS),"A");
                    strcpy(NEWDRRFIELD(cgDrrBuf,igDrrROSL),pcpNextRosl);
                    /***************
                    sprintf(NEWDRRFIELD(cgDrrBuf,igDrrUSEU),"%-32s","RELDPL");
                    sprintf(NEWDRRFIELD(cgDrrBuf,igDrrUSEC),"%-32s","RELDPL");
### USEC-USEU ###
                    ***********************/
                    sprintf(NEWDRRFIELD(cgDrrBuf,igDrrUSEU),"%-32s"," ");
                    sprintf(NEWDRRFIELD(cgDrrBuf,igDrrUSEC),"%-32s",
                        DRRFIELD(pclRowBuf,igDrrUSEU));
                    if (*cgUser != '\0')
                    {
                        sprintf(NEWDRRFIELD(cgDrrBuf,igDrrUSEC),"%-32s",cgUser);
                    }
                    strcpy(NEWDRRFIELD(cgDrrBuf,igDrrCDAT),clNow);
                    /*strcpy(NEWDRRFIELD(cgDrrBuf,igDrrLSTU),clNow);*/
        
                    GetNextValues(clTmpUrno,1);
                    sprintf(NEWDRRFIELD(cgDrrBuf,igDrrURNO),"%-10s",clTmpUrno);
        
                    llNewRowNum = ARR_FIRST;
        
                    CEDAArrayAddRow(&rgNewDrrArray.rrArrayHandle,
                        rgNewDrrArray.crArrayName,
                        &llNewRowNum,(void *)cgDrrBuf);
        
                        CEDAArrayPutField(&rgNewDrrArray.rrArrayHandle,
                            rgNewDrrArray.crArrayName,
                            NULL , "USED",llNewRowNum,"Y");
        
                        CEDAArrayGetField(&rgNewDrrArray.rrArrayHandle,
                            rgNewDrrArray.crArrayName,
                            NULL , "USED",3, llNewRowNum,clTest);
        
                    /*** check for DRS copy **/
                    strcpy(clDrsFlag,DRRFIELD(pclRowBuf,igDrrDRSF));
                    if (*clDrsFlag == '1')
                    {
                        dbg(TRACE,"DRSF is '1', DRS Record will be copied");
                        CopyDRS_DRRU(DRRFIELD(pclRowBuf,igDrrURNO),NEWDRRFIELD(cgDrrBuf,igDrrURNO));
                    }
                }
            }
            else
            {
                dbg(TRACE,"not released because emp is not employed at <%s>",clSDAY);
            }
        }
        llRowNum = ARR_NEXT;
    }

    /*** step through the NEW Array and delete not used records **/
    llRowNum = ARR_LAST;
    while(CEDAArrayGetRowPointer(&rgNewDrrArray.rrArrayHandle,
        rgNewDrrArray.crArrayName,llRowNum,(void *)&pclRowBuf) == RC_SUCCESS)       
    {
        char clUrno[24];
        char clSday[24];
        char clStfu[24];

        llRowNum = ARR_CURRENT;
            CEDAArrayGetField(&rgNewDrrArray.rrArrayHandle,rgNewDrrArray.crArrayName,
                NULL , "USED",3, llRowNum,clTest);
            CEDAArrayGetField(&rgNewDrrArray.rrArrayHandle,rgNewDrrArray.crArrayName,
                NULL , "URNO",12, llRowNum,clUrno);
            CEDAArrayGetField(&rgNewDrrArray.rrArrayHandle,rgNewDrrArray.crArrayName,
                NULL , "SDAY",12, llRowNum,clSday);
            CEDAArrayGetField(&rgNewDrrArray.rrArrayHandle,rgNewDrrArray.crArrayName,
                NULL , "STFU",12, llRowNum,clStfu);

        dbg(DEBUG,"%05d: USED=<%s> URNO <%s> SDAY <%s> STFU <%s>",__LINE__,clTest,clUrno,clSday,clStfu);

        if (*clTest != 'Y')
        {
            /*** this DRR is not used anymore ***/
            dbg(TRACE,"DRR will be deleted");
            llRowNum = ARR_CURRENT;
            CEDAArrayDeleteRow(&rgNewDrrArray.rrArrayHandle,
                        rgNewDrrArray.crArrayName,llRowNum);   
        }
        llRowNum = ARR_PREV;
    }

/* replaced by the following AATArraySaveChanges 
dbg(TRACE,"CEDAArrayWriteDB follows");
    CEDAArrayWriteDB(&rgDrrArray.rrArrayHandle,
        rgDrrArray.crArrayName, NULL, NULL, NULL, NULL,ARR_COMMIT_ALL_OK);
dbg(TRACE,"DrrArray written");
    CEDAArrayWriteDB(&rgNewDrrArray.rrArrayHandle,
        rgNewDrrArray.crArrayName, NULL, NULL, NULL, NULL,ARR_COMMIT_ALL_OK);
dbg(TRACE,"New DrrArray written");
   **/
    
        /* Initialise AAT_PARAM structure for old and new demands */
    ilRc = AATArrayCreateArgDesc(rgDrrArray.crArrayName, &prgRdpInf );
    if ( ilRc == RC_SUCCESS )
    {
        prgRdpInf->AppendData = FALSE;
        ToolsListSetInfo(&(prgRdpInf->DataList[7]), "IRT_URNO", "DRRTAB", 
                         "IRT", rgDrrArray.crArrayFieldList, "","",",",4096);
        ToolsListSetInfo(&(prgRdpInf->DataList[8]), "URT_URNO", "DRRTAB", 
                         "URT",rgDrrArray.crArrayFieldList, "","",",",4096);
        ToolsListSetInfo(&(prgRdpInf->DataList[9]), "DRT_DATA", "DRRTAB", 
                         "DRT", rgDrrArray.crArrayFieldList, "","",",",-1);
        ToolsListSetInfo(&(prgRdpInf->DataList[10]),"CMD_LIST","","","","","","\n",-1);
        ToolsListSetInfo(&(prgRdpInf->DataList[11]),"SEL_LIST","","","","","","\n",-1);
        ToolsListSetInfo(&(prgRdpInf->DataList[12]),"FLD_LIST","","","","","","\n",-1);
        ToolsListSetInfo(&(prgRdpInf->DataList[13]),"DAT_LIST","","","","","","\n",-1);
    }
    
    ToolsSetSpoolerInfo(cgDestName, cgRecvName, cgTwStart, cgTwEnd );
    
    AATArraySaveChanges(&rgDrrArray.rrArrayHandle,rgDrrArray.crArrayName, 
        &prgRdpInf, ARR_COMMIT_ALL_OK);
        prgRdpInf->AppendData = TRUE;
    AATArraySaveChanges(&rgNewDrrArray.rrArrayHandle,rgNewDrrArray.crArrayName, 
        &prgRdpInf, ARR_COMMIT_ALL_OK);
    

    if (*pcpJob != 'N')
    {
        UpdateRELTABAfterRelease("L",clNow);
    }
    commit_work();

    if ((bgSendBroadcast == TRUE) ||
            (bgSend2Action == TRUE))
    {
        char *pclData = NULL;
    char *pclTrenner;

    /* Late Answer */
    dbg(TRACE,"LATE ANSWER SENT TO %d",igQueOut);

        dbg(DEBUG,"vorher: <%s>",rgRelData.pcUrnoList2);
        do
        {
            pclTrenner = strchr(rgRelData.pcUrnoList2,'-');
            if (pclTrenner != NULL)
            {
                *pclTrenner = 178;
            }
        } while (pclTrenner != NULL);

        dbg(DEBUG,"nachher: <%s>",rgRelData.pcUrnoList2);
        pclData = malloc(strlen(rgRelData.cDateFrTo) + 
            strlen(rgRelData.pcUrnoList2) +100 );
        sprintf(pclData,"%s-%s-%s%c%s",rgRelData.cDateFrTo,
            rgRelData.pcUrnoList2,rgRelData.cPlanStufe,
                   178, cgPlanStufeNew);

        dbg(TRACE,"%05d:Release from %s to %s",__LINE__,rgRelData.cPlanStufe,cgPlanStufeNew);

        if (bgSendBroadcast == TRUE)
        {
            dbg(TRACE,"Send to bchdl follows");
                    /***************
                (void) tools_send_info_flag(igBchdlModid,0, cgDestName, "", cgRecvName,
                  "", "", cgTwStart, cgTwEnd,
                                    "SBC","RELDRR","OK","EOT,RM",pclData,0);
                                                                         ****************/
    ilRc = AATArraySetSbcInfo(rgDrrArray.crArrayName, prgRdpInf, "1900", 
                                  "3", "SBC", "RELDRR", cgRecvName, cgDestName, cgTwStart, 
                                  cgTwEnd, "", "", pclData );
                if (ilRc == RC_SUCCESS)
                {
                char clLists[24];

                sprintf ( clLists, "%d,%d,%d", IDX_IRTD, IDX_URTD, IDX_DRTU );
                ilRc = AATArrayCreateSbc(rgDrrArray.crArrayName, prgRdpInf, 0, clLists, FALSE);
                }
        }
        if (bgSend2Action == TRUE)
        {
            dbg(TRACE,"Send to action follows"); 
                (void) tools_send_info_flag(igActionModid,0, cgDestName, "", cgRecvName,
                                     "", "", cgTwStart, cgTwEnd,
                                     "SBC","RELDRR","OK","EOT,RM",pclData,0);          
        }
        if (pclData != NULL)
        {
            free(pclData);
        }
    }


    /*** clean up **/
    CEDAArrayDelete(&rgDrrArray.rrArrayHandle,rgDrrArray.crArrayName);
    CEDAArrayDelete(&rgNewDrrArray.rrArrayHandle,rgNewDrrArray.crArrayName);

    /*return (ilRc); due to changes for night release return always RC_SUCCESS*/
    return (RC_SUCCESS);
}                   




/******************************************************************************/
/* The handle data routine                                                    */
/******************************************************************************/
static int HandleData(EVENT *prpEvent)
{
    int    ilRc           = RC_SUCCESS;         /* Return code */
    int      ilCmd          = 0;

    BC_HEAD *prlBchd       = NULL;
    CMDBLK  *prlCmdblk       = NULL;
    char    *pclSelection    = NULL;
    char    *pclFields       = NULL;
    char    *pclData         = NULL;
    char    *pclRow          = NULL;
    char        clUrnoList[2400];
    char        clTmpData[312];
    char        clTable[34];
    char    clTmpJob[12];

    prlBchd    = (BC_HEAD *) ((char *)prpEvent + sizeof(EVENT));
    prlCmdblk    = (CMDBLK *)  ((char *)prlBchd->data);
    pclSelection = prlCmdblk->data;
    pclFields    = pclSelection + strlen(pclSelection) + 1;
    pclData      = pclFields + strlen(pclFields) + 1;

    strcpy(clTable,prlCmdblk->obj_name);

    dbg(TRACE,"========== START <%10.10d> ==========",lgEvtCnt);

  igQueOut = prgEvent->originator;

  /* Save Global Elements of CMDBLK */
  strcpy(cgTwStart,prlCmdblk->tw_start);
  strcpy(cgTwEnd,prlCmdblk->tw_end);
  /* Save Global Elements of BC_HEAD */
  strcpy(cgRecvName,prlBchd->recv_name);
  strcpy(cgDestName,prlBchd->dest_name);
  *cgUser = '\0';

    /****************************************/
    DebugPrintBchead(TRACE,prlBchd);
    DebugPrintCmdblk(TRACE,prlCmdblk);
  dbg(TRACE,"TwStart   <%s>",cgTwStart);
  dbg(TRACE,"TwEnd     <%s>",cgTwEnd);
    dbg(TRACE,"originator<%d>",prpEvent->originator);
    dbg(TRACE,"selection <%s>",pclSelection);
    dbg(TRACE,"fields    <%s>",pclFields);
    if (debug_level == DEBUG)
    {
        dbg(TRACE,"data      <%s>",pclData);
    }
    else if (debug_level == TRACE)
    {
        strncpy(clTmpData,pclData,300);
        clTmpData[299] = '\0';
        dbg(TRACE,"data      <%s>",clTmpData);
    }
    /****************************************/
    { /* delete old data from pclData */
        char *pclNewline = NULL;

        if ((pclNewline = strchr(pclData,'\n')) != NULL)
        {
            *pclNewline = '\0';
        }
    }

/*************
            CEDAArraySendChanges2BCHDL(&rgDrrArray.rrArrayHandle,
                      rgDrrArray.crArrayName,TRUE);
            CEDAArraySendChanges2ACTION(&rgDrrArray.rrArrayHandle,
                      rgDrrArray.crArrayName,TRUE);
*************/
  if (strstr(pclSelection,"NOACTION") == NULL)
  {
      dbg(TRACE,"Changes send to Action"); 
            CEDAArraySendChanges2ACTION(&rgDrrArray.rrArrayHandle,
                      rgDrrArray.crArrayName,TRUE);
            CEDAArraySendChanges2ACTION(&rgNewDrrArray.rrArrayHandle,
                      rgNewDrrArray.crArrayName,TRUE);
            bgSend2Action = FALSE;
  }
    else
    {
      dbg(TRACE,"don't send changes to Action"); 
            CEDAArraySendChanges2ACTION(&rgDrrArray.rrArrayHandle,
                      rgDrrArray.crArrayName,FALSE);
            CEDAArraySendChanges2ACTION(&rgNewDrrArray.rrArrayHandle,
                      rgNewDrrArray.crArrayName,FALSE);
            bgSend2Action = TRUE;
  } 
/*****************
  if (strstr(pclSelection,"NOLOG") == NULL)
  {
        sprintf(pcgOutRoute,"%d,",igToLogHdl);
  }
**********************/
  if (strstr(pclSelection,"NOBC") == NULL)
  {
      dbg(TRACE,"Changes send to BcHdl"); 
            CEDAArraySendChanges2BCHDL(&rgDrrArray.rrArrayHandle,
                      rgDrrArray.crArrayName,TRUE);
            CEDAArraySendChanges2BCHDL(&rgNewDrrArray.rrArrayHandle,
                      rgNewDrrArray.crArrayName,TRUE);
            bgSendBroadcast = FALSE;
  }
  else
  {
        dbg(TRACE,"don't send changes to BcHdl"); 
                CEDAArraySendChanges2BCHDL(&rgDrrArray.rrArrayHandle,
                      rgDrrArray.crArrayName,FALSE);
                CEDAArraySendChanges2BCHDL(&rgNewDrrArray.rrArrayHandle,
                      rgNewDrrArray.crArrayName,FALSE);
              bgSendBroadcast = TRUE;
  }
  
    ilRc = GetCommand(&prlCmdblk->command[0],&ilCmd);
    if(ilRc == RC_SUCCESS)
    {
        switch (ilCmd)
        {
    case CMD_RDL :
        case CMD_RDN :
        dbg(TRACE,"%05d:HandleData: %s",__LINE__,prgCmdTxt[ilCmd]);
            SendAnswer(1900, ""); 
            if (ilCmd == CMD_RDL) 
            {
                strcpy(clTmpJob,"L"); 
            }
            else
            {
                strcpy(clTmpJob,"N"); 
            }
            ilRc = HandleDataArray(pclData, clTmpJob); /*Store REL-Data*/    
            strcpy(cgUser,rgRelData.cUsec);
            if (ilCmd != CMD_RDN)
            {
                break ; 
            }
            if (ilRc == RC_SUCCESS && ilCmd == CMD_RDN)
            {
                char clNextRosl[24];

            if (GetNextRoslFromSequence(cgRelSequence,rgRelData.cPlanStufe[0],
                        clNextRosl,',') > 0)
                {
                    strcpy(cgPlanStufeNew,clNextRosl);
                  /*** if there is no next ROSL do nothing **/
                    ilRc = HandleDrrDataNow(pclData,clNextRosl,clTmpJob); /*Handle REL-Data*/
                }
            }
            break;
        case CMD_RDT :
        case CMD_RDS :
                /*** automatic release, do it for all possible dates **/

            strcpy(rgRelData.cDateFrTo,"20000100-20991231");
                {
                    dbg(TRACE,"Automatic release, don't send changes to Action"); 
                    CEDAArraySendChanges2ACTION(&rgDrrArray.rrArrayHandle,
                                        rgDrrArray.crArrayName,FALSE);
                    CEDAArraySendChanges2ACTION(&rgNewDrrArray.rrArrayHandle,
                                        rgNewDrrArray.crArrayName,FALSE);
                    bgSend2Action = TRUE;
                    dbg(TRACE,"Automatic release, don't send changes to BcHdl"); 
                    CEDAArraySendChanges2BCHDL(&rgDrrArray.rrArrayHandle,
                        rgDrrArray.crArrayName,FALSE);
                    CEDAArraySendChanges2BCHDL(&rgNewDrrArray.rrArrayHandle,
                        rgNewDrrArray.crArrayName,FALSE);
                    bgSendBroadcast = TRUE;
                }

            {
                    int ilRosl = 1;
                  char clNextRosl[6];
                            
                    strcpy(clNextRosl,"1");
                    strcpy(rgRelData.cPlanStufe,"1");
                    while (GetNextRoslFromSequence(cgRelSequence,rgRelData.cPlanStufe[0],
                            clNextRosl,',') > 0)
                    {
                        strcpy(cgPlanStufeNew,clNextRosl);
                        /*** if there is no next ROSL do nothing **/
                        /*ilRc = HandleDrrDataNow(pclData,clNextRosl,"S");*/
                        while(HandleDrrDataNow(pclData,clNextRosl,"S") == RC_SUCCESS);
                        strcpy(rgRelData.cPlanStufe,clNextRosl);
                    }
            }
            if (ilCmd == CMD_RDS)
            {
                    dbg(TRACE,"Command is RDS, break here!");
                    break;
            }

            /************ now do the jobs from config file ****/
            {
                int ilRosl = 1;
                char clNextRosl[6];
                            
                strcpy(clNextRosl,"1");
                strcpy(rgRelData.cPlanStufe,"1");
                while (GetNextRoslFromSequence(cgRelSequence,rgRelData.cPlanStufe[0], clNextRosl,',') > 0)
                {
                    dbg(TRACE,"RDT release from level %s to %s",&rgRelData.cPlanStufe[0],clNextRosl);
                    /*** if there is no next ROSL do nothing **/
                    strcpy(cgPlanStufeNew,clNextRosl);
                    ilRc = ReleaseFromConfig(rgRelData.cPlanStufe,clNextRosl);
                    strcpy(rgRelData.cPlanStufe,clNextRosl);
                }

                /************ delete old stuff from RELTAB ****/
                DeleteOldReltabEntries();
            }
            break ; 
        case CMD_RDD :
        case CMD_IRT:
      case CMD_URT:
      case CMD_DRT:
                      ilRc = CEDAArrayEventUpdate(prgEvent);    
                break;
            default: ;
            }

        /* end of switch */
    }else{
        dbg(TRACE,"HandleData: GetCommand failed");
        DebugPrintBchead(TRACE,prlBchd);
        DebugPrintCmdblk(TRACE,prlCmdblk);
        dbg(TRACE,"selection <%s>",pclSelection);
        dbg(TRACE,"fields    <%s>",pclFields);
        dbg(TRACE,"data      <%s>",pclData);
    }/* end of if */

    dbg(TRACE,"==========  END  <%10.10d> ==========",lgEvtCnt);

    return(RC_SUCCESS);
    
} /* end of HandleData */


static int StrToTime(char *pcpTime,time_t *plpTime)
{
    struct tm *_tm;
    time_t now;
    char   _tmpc[6];
    long llUtcDiff;

    dbg(0,"StrToTime, <%s>",pcpTime);
    if (strlen(pcpTime) < 12 )
    {
        *plpTime = time(0L);
        return RC_FAIL;
    } /* end if */




    now = time(0L);
    _tm = (struct tm *)gmtime(&now);

    _tmpc[2] = '\0';
/*
    strncpy(_tmpc,pcpTime+12,2);
    _tm -> tm_sec = atoi(_tmpc);
*/
    _tm -> tm_sec = 0;
    
    strncpy(_tmpc,pcpTime+10,2);
    _tm -> tm_min = atoi(_tmpc);
    strncpy(_tmpc,pcpTime+8,2);
    _tm -> tm_hour = atoi(_tmpc);
    strncpy(_tmpc,pcpTime+6,2);
    _tm -> tm_mday = atoi(_tmpc);
    strncpy(_tmpc,pcpTime+4,2);
    _tm -> tm_mon = atoi(_tmpc)-1;
    strncpy(_tmpc,pcpTime,4);
    _tmpc[4] = '\0';
    _tm ->tm_year = atoi(_tmpc)-1900;
    _tm ->tm_wday = 0;
    _tm ->tm_yday = 0;
    _tm->tm_isdst = -1; /* no adjustment of daylight saving time */
    now = mktime(_tm);
    dbg(0,"StrToTime: %02d.%02d.%04d %02d:%02d Now: %ld",_tm->tm_mday,
_tm->tm_mon+1,_tm->tm_year+1900,_tm->tm_hour,_tm->tm_min,now);
    if (now != (time_t) -1)
    {
    {
        time_t utc,local;

    _tm = (struct tm *)gmtime(&now);
    utc = mktime(_tm);
    _tm = (struct tm *)localtime(&now);
    local = mktime(_tm);
    dbg(TRACE,"utc %ld, local %ld, utc-diff %ld",
        utc,local,local-utc);
        llUtcDiff = local-utc;
    }
        now +=  + llUtcDiff;
        *plpTime = now;
        return RC_SUCCESS;
    }
    *plpTime = time(NULL);
    return RC_FAIL;
}


/**
dbg(TRACE,"%05d:",__LINE__);
***/



/*
 *     struct  tm {
 *             int     tm_sec;    seconds after the minute -- [0, 59]  
 *             int     tm_min;    minutes after the hour -- [0, 59] 
 *             int     tm_hour;   hour since midnight -- [0, 23] 
 *             int     tm_mday;   day of the month -- [1, 31] 
 *             int     tm_mon;    months since January -- [0, 11] 
 *             int     tm_year;   years since 1900 
 *             int     tm_wday;   days since Sunday -- [0, 6] 
 *             int     tm_yday;   days since January 1 -- [0, 365] 
 *             int     tm_isdst;  flag for daylight savings time 
 *             long    tm_tzadj;  seconds from GMT (east < 0) 
 *             char    tm_name[LTZNMAX];   name of timezone 
 *     };
 */




void    StrAddTime(char *pcpBase,char *pcpOffs,char *pcpDest)
{

    if (pcpBase != pcpDest)
    {
        strcpy(pcpDest,pcpBase);
    }
    AddSecondsToCEDATime(pcpDest,(time_t)atol(pcpOffs),1);

} /* end of StrAddTime */



int check_ret1(int ipRc,int ipLine)
{
    int ilOldDebugLevel;

    ilOldDebugLevel = debug_level;

    debug_level = TRACE;

    dbg(TRACE,"DB-ERROR in Line: %d",ipLine);
    debug_level  = ilOldDebugLevel;
    return check_ret(ipRc);
}

static void TrimRight(char *pcpBuffer)
{
    char *pclBlank = &pcpBuffer[strlen(pcpBuffer)-1];

    if (*pclBlank != '\0')
    {
        while(isspace(*pclBlank) && pclBlank != pcpBuffer)
        {
            *pclBlank = '\0';
            pclBlank--;
        }
    }
}


static int TriggerAction(char *pcpTableName)
{
  int           ilRc          = RC_SUCCESS ;    /* Return code */
  EVENT        *prlEvent      = NULL ;
  BC_HEAD      *prlBchead     = NULL ;
  CMDBLK       *prlCmdblk     = NULL ;
  /*ACTIONConfig *prlAction     = NULL ;*/
    ACTIONConfig rlAction;
    
  char         *pclSelection  = NULL ;
  char         *pclFields     = NULL ;
  char         *pclData       = NULL ;
  long          llActSize     = 0 ;
  int           ilAnswerQueue = 0 ;
  char          clQueueName[16] ;
    
  if(ilRc == RC_SUCCESS)
  {

  }/* end of if */
    

  if (ilRc == RC_SUCCESS)
  {
   sprintf (&clQueueName[0],"%s2", mod_name) ;

   ilRc = GetDynamicQueue(&ilAnswerQueue,&clQueueName[0]);
   if (ilRc != RC_SUCCESS)
   {
    dbg(TRACE,"TriggerAction: GetDynamicQueue failed <%d>",ilRc);
   }
   else
   {
    dbg(DEBUG,"TriggerAction: got <%d> for <%s>",ilAnswerQueue, &clQueueName[0]);
   }/* end of if */
  }/* end of if */
    
  if (ilRc == RC_SUCCESS)
  {
   llActSize = sizeof(EVENT) + sizeof(BC_HEAD) + sizeof(CMDBLK) + sizeof(ACTIONConfig) + 666;
   prgOutEvent = realloc(prgOutEvent,llActSize);
   if(prgOutEvent == NULL)
   {
    dbg(TRACE,"TriggerAction: realloc out event <%d> bytes failed",llActSize);
    ilRc = RC_FAIL ;
   } /* end of if */
  } /* end of if */

  if(ilRc == RC_SUCCESS)
  {
   prlBchead     = (BC_HEAD *) ((char *) prgOutEvent + sizeof(EVENT)) ;
   prlCmdblk     = (CMDBLK *)  ((char *) prlBchead->data) ;
   pclSelection  = prlCmdblk->data ;
   pclFields     = pclSelection + strlen (pclSelection) + 1 ;
   pclData       = pclFields + strlen (pclFields) + 1 ;
   strcpy (pclData, "DYN") ;
    
        memset(&rlAction,0,sizeof(ACTIONConfig));
        
    prlBchead->rc = RC_SUCCESS;
    
   prgOutEvent->type        = USR_EVENT ;
   prgOutEvent->command     = EVENT_DATA ;
   prgOutEvent->originator  = ilAnswerQueue ;
   prgOutEvent->retry_count = 0 ;
   prgOutEvent->data_offset = sizeof(EVENT) ;
   prgOutEvent->data_length = llActSize-sizeof(EVENT) ;

   rlAction.iEmptyFieldHandling = iDELETE_ALL_BLANKS ;
   rlAction.iIgnoreEmptyFields = 0 ;
   strcpy(rlAction.pcSndCmd, "") ;
   sprintf(rlAction.pcSectionName,"%s_%s", mod_name,pcpTableName) ;
   strcpy(rlAction.pcTableName, pcpTableName) ;
   strcpy(rlAction.pcFields, "-") ;
     /*********************** MCU TEST ***************/
   strcpy(rlAction.pcFields, "") ;
     /*********************** MCU TEST ***************/
     /************/
     if (!strcmp(pcpTableName,"SPRTAB"))
        {
    strcpy(rlAction.pcFields, "PKNO,USTF,ACTI,FCOL") ;
     }
     /************/
   strcpy(rlAction.pcSectionCommands, "IRT,URT,DRT") ;
   rlAction.iModID = mod_id ;

   /****************************************
     DebugPrintEvent(DEBUG,prgOutEvent) ;
     DebugPrintBchead(DEBUG,prlBchead) ;
     DebugPrintCmdblk(DEBUG,prlCmdblk) ;
     dbg (DEBUG,"TriggerAction: selection <%s>",pclSelection) ;
     dbg (DEBUG,"TriggerAction: fields    <%s>",pclFields) ;
     dbg (DEBUG,"TriggerAction: data      <%s>",pclData) ;
     DebugPrintACTIONConfig(DEBUG,&rlAction) ;
   ****************************************/

   if(ilRc == RC_SUCCESS)
   {
    rlAction.iADFlag = iDELETE_SECTION ;
             memcpy(pclData + strlen(pclData) + 1,&rlAction,sizeof(ACTIONConfig));

    ilRc = que(QUE_PUT, 7400, ilAnswerQueue, PRIORITY_3, llActSize, (char *) prgOutEvent) ; 
    if(ilRc != RC_SUCCESS)
    {
     dbg(TRACE,"TriggerAction: QUE_PUT failed <%d>",ilRc) ;
    }
    else
    {
     ilRc = WaitAndCheckQueue(10, ilAnswerQueue,&prgItem) ;
     if(ilRc != RC_SUCCESS)
     {
      dbg(TRACE,"TriggerAction: WaitAndCheckQueue failed <%d>",ilRc) ;
     }
     else
     {
      /* return value for delete does not matter !!! */
        
      /****************************************
      prlEvent     = (EVENT *)   ((char *)prgItem->text) ;
      prlBchead    = (BC_HEAD *) ((char *)prlEvent + sizeof(EVENT)) ;
      prlCmdblk    = (CMDBLK *)  ((char *)prlBchead->data) ;
      pclSelection = (char *)    prlCmdblk->data ;
      pclFields    = (char *)    pclSelection + strlen(pclSelection) + 1;
      pclData      = (char *)    pclFields + strlen(pclFields) + 1;
      DebugPrintItem(DEBUG, prgItem) ;
      DebugPrintEvent(DEBUG, prlEvent) ;
      DebugPrintBchead(DEBUG, prlBchead) ;
      DebugPrintCmdblk(DEBUG, prlCmdblk) ;
      dbg(DEBUG,"TriggerAction: selection <%s>",pclSelection) ;
      dbg(DEBUG,"TriggerAction: fields    <%s>",pclFields) ;
      dbg(DEBUG,"TriggerAction: data      <%s>",pclData) ;
      ****************************************/
    } /* end of if */
   }/* end of if */
  }/* end of if */

  if(ilRc == RC_SUCCESS)
  {
    rlAction.iADFlag = iADD_SECTION ;
      memcpy(pclData + strlen(pclData) + 1,&rlAction,sizeof(ACTIONConfig));
         
   ilRc = que(QUE_PUT, 7400, ilAnswerQueue, PRIORITY_3, llActSize,
                                              (char *) prgOutEvent ) ;
   if(ilRc != RC_SUCCESS)
   {
    dbg(TRACE,"TriggerAction: QUE_PUT failed <%d>",ilRc) ;
   }
   else
   {
    ilRc = WaitAndCheckQueue(10,ilAnswerQueue,&prgItem) ;
    if(ilRc != RC_SUCCESS)
    {
     dbg(TRACE,"TriggerAction: WaitAndCheckQueue failed <%d>",ilRc) ;
    }
    else
    {
     prlEvent     = (EVENT *)   ((char *)prgItem->text) ;
     prlBchead    = (BC_HEAD *) ((char *)prlEvent + sizeof(EVENT)) ;
     prlCmdblk    = (CMDBLK *)  ((char *)prlBchead->data) ;
     pclSelection = (char *)    prlCmdblk->data ;
     pclFields    = (char *)    pclSelection + strlen(pclSelection) + 1 ;
     pclData      = (char *)    pclFields + strlen(pclFields) + 1 ;

     if(strcmp(pclData,"SUCCESS") != 0)
     {
      dbg(TRACE,"TriggerAction: add dynamic action config failed") ;
      DebugPrintItem(DEBUG,prgItem) ;
      DebugPrintEvent(DEBUG,prlEvent) ;
      DebugPrintBchead(DEBUG,prlBchead) ;
      DebugPrintCmdblk(DEBUG,prlCmdblk) ;
      dbg (DEBUG,"TriggerAction: selection <%s>",pclSelection) ;
      dbg (DEBUG,"TriggerAction: fields    <%s>",pclFields) ;
      dbg(DEBUG,"TriggerAction: data      <%s>",pclData) ;
      ilRc = RC_FAIL ;
     }
     else
     {
      dbg(DEBUG,"TriggerAction: add dynamic action config OK") ;
     }/* end of if */
    }/* end of if */
   }/* end of if */
  }/* end of if */

  ilRc = que(QUE_DELETE,ilAnswerQueue,ilAnswerQueue,0,0,0) ;
  if(ilRc != RC_SUCCESS)
  {
   dbg(TRACE,"que QUE_DELETE <%d> failed <%d>",ilAnswerQueue, ilRc) ;
  }
  else
  {
   dbg(DEBUG,"TriggerAction: queue <%d> <%s> deleted", ilAnswerQueue, &clQueueName[0]) ;
  }/* end of if */
 }/* end of if */

 return (ilRc) ;
    
}
static int WaitAndCheckQueue(int ipTimeout, int ipModId, ITEM **prpItem)
{
    int ilRc          = RC_SUCCESS;                      /* Return code */
    int ilWaitCounter = 0;

    do
    {
        sleep(1);
        ilWaitCounter += 1;

        ilRc = CheckQueue(ipModId,prpItem);
        if(ilRc != RC_SUCCESS)
        {
            if(ilRc != QUE_E_NOMSG)
            {
                dbg(TRACE,"WaitAndCheckQueue: CheckQueue <%d> failed <%d>",mod_id,ilRc);
            }/* end of if */
        }/* end of if */
    }while((ilRc == QUE_E_NOMSG) && (ilWaitCounter < ipTimeout));

    if(ilWaitCounter >= ipTimeout)
    {
        dbg(TRACE,"WaitAndCheckQueue: timeout reached <%d>",ipTimeout);
        ilRc = RC_FAIL;
    }/* end of if */

    return ilRc;
}


static int CheckQueue(int ipModId, ITEM **prpItem)
{
    int     ilRc       = RC_SUCCESS;                      /* Return code */
    int     ilItemSize = 0;
    EVENT *prlEvent    = NULL;
static  ITEM  *prlItem      = NULL;
    
    ilItemSize = I_SIZE;
    
    ilRc = que(QUE_GETBIGNW,ipModId,ipModId,PRIORITY_3,ilItemSize,(char *) &prlItem);
    if(ilRc == RC_SUCCESS)
    {
        dbg(TRACE,"DebugPrintItem with prlItem follows");
        DebugPrintItem(TRACE,prlItem);
        *prpItem = prlItem;
        dbg(TRACE,"DebugPrintItem with prpItem follows");
        DebugPrintItem(TRACE,*prpItem);
        prlEvent = (EVENT*) ((prlItem)->text);

        switch( prlEvent->command )
        {
            case    HSB_STANDBY :
                dbg(TRACE,"CheckQueue: HSB_STANDBY");
                HandleQueues();
                ctrl_sta = prlEvent->command;
                break;
    
            case    HSB_COMING_UP   :
                dbg(TRACE,"CheckQueue: HSB_COMING_UP");
                ctrl_sta = prlEvent->command;
                break;
    
            case    HSB_ACTIVE  :
                dbg(TRACE,"CheckQueue: HSB_ACTIVE");
                ctrl_sta = prlEvent->command;
                break;
    
            case    HSB_ACT_TO_SBY  :
                dbg(TRACE,"CheckQueue: HSB_ACT_TO_SBY");
                HandleQueues();
                ctrl_sta = prlEvent->command;
                break;
    
            case    HSB_DOWN    :
                dbg(TRACE,"CheckQueue: HSB_DOWN");
                ctrl_sta = prlEvent->command;
                /* Acknowledge the item 
                ilRc = que(QUE_ACK,0,ipModId,0,0,NULL);
                if( ilRc != RC_SUCCESS )
                {
                    HandleQueErr(ilRc);
                }  */
                Terminate(30);
                break;
        
        case    HSB_STANDALONE  :
                dbg(TRACE,"CheckQueue: HSB_STANDALONE");
                ctrl_sta = prlEvent->command;
                break;
    
            case    SHUTDOWN    :
                dbg(TRACE,"CheckQueue: SHUTDOWN");
                /* Acknowledge the item */
                ilRc = que(QUE_ACK,0,ipModId,0,0,NULL);
                if( ilRc != RC_SUCCESS )
                {
                    /* handle que_ack error */
                    HandleQueErr(ilRc);
                } /* fi */
                Terminate(30);
                break;
    
            case    RESET       :
                ilRc = Reset();
                break;
    
            case    EVENT_DATA  :
                /* DebugPrintItem(DEBUG,prgItem); */
                /* DebugPrintEvent(DEBUG,prlEvent); */
                break;
    
            case    TRACE_ON :
                dbg_handle_debug(prlEvent->command);
                break;
    
            case    TRACE_OFF :
                dbg_handle_debug(prlEvent->command);
                break;


            default         :
                dbg(TRACE,"CheckQueue: unknown event");
                DebugPrintItem(TRACE,prlItem);
                DebugPrintEvent(TRACE,prlEvent);
                break;
    
        } /* end switch */
    
        /* Acknowledge the item */
        ilRc = que(QUE_ACK,0,ipModId,0,0,NULL);
        if( ilRc != RC_SUCCESS )
        {
            /* handle que_ack error */
            HandleQueErr(ilRc);
        } /* fi */
    }else{
        if(ilRc != QUE_E_NOMSG)
        {
            dbg(TRACE,"CheckQueue: que(QUE_GETBIG,%d,%d,...) failed <%d>",ipModId,ipModId,ilRc);
            HandleQueErr(ilRc);
        }/* end of if */
    }/* end of if */
       
    return ilRc;
}/* end of CheckQueue */



/****************************************************************************
* Function: GetUrnoFromSelection
* Parameter:           
*            
* Return:
* Description: gets the Urno from the SQL Statement "WHERE URNO = 'xxxxxxx'"
*              
*****************************************************************************/
static int GetUrnoFromSelection(char *pcpSelection,char *pcpUrno)
{
    char *pclFirst;
    char *pclLast;
    char pclTmp[24];
    int ilLen;
    
    pclFirst = strchr(pcpSelection,'\'');
    if (pclFirst != NULL)
    {
        pclLast  = strrchr(pcpSelection,'\'');
        if (pclLast == NULL)
        {
            pclLast = pclFirst + strlen(pclFirst);
        }
        ilLen = pclLast-pclFirst < 22 ? pclLast - pclFirst : 22;
        strncpy(pclTmp,++pclFirst,ilLen);
        pclTmp[ilLen-1] = '\0';
        strcpy(pcpUrno,pclTmp);
    }
    else
        {
         pclFirst = pcpSelection;
         while (!isdigit(*pclFirst) && *pclFirst != '\0')
         {
            pclFirst++;
            }
        strcpy(pcpUrno,pclFirst);
        }
    return RC_SUCCESS;
}


static int SaveIndexInfo (ARRAYINFO *prpArrayInfo)
{
  int    ilRc       = RC_SUCCESS ;
  long   llRow      = 0 ;
  FILE  *prlFile    = NULL ;
  char   clDel      = ',' ;
  char   clFile[512] ;
  char  *pclTrimBuf = NULL ;

  dbg (TRACE,"SaveIndexInfo: SetArrayInfo <%s>",prpArrayInfo->crArrayName) ;

  if (ilRc == RC_SUCCESS)
  {
   sprintf (&clFile[0],"%s/%s_%s.info",getenv("DBG_PATH"),mod_name,prpArrayInfo->crArrayName) ;

   errno = 0 ;
   prlFile = fopen (&clFile[0], "w") ;
   if (prlFile == NULL)
   {
    dbg (TRACE, "SaveIndexInfo: fopen <%s> failed <%d-%s>", &clFile[0], errno, strerror(errno)) ;
    ilRc = RC_FAIL ;
   } /* end of if */
  } /* end of if */

  if (ilRc == RC_SUCCESS)
  {
   if (igTestOutput > 2) 
   {
    dbg (DEBUG, "ArrayName      <%s>\n", prpArrayInfo->crArrayName) ;      
    dbg (DEBUG, "ArrayFieldList <%s>\n", prpArrayInfo->crArrayFieldList) ;  
    dbg (DEBUG, "Idx01FieldList <%s>\n", prpArrayInfo->crIdx01FieldList) ;  
   }

   fprintf (prlFile, "ArrayName      <%s>\n", prpArrayInfo->crArrayName) ; fflush(prlFile) ;
   fprintf (prlFile, "ArrayFieldList <%s>\n", prpArrayInfo->crArrayFieldList) ; fflush(prlFile) ;
   fprintf (prlFile, "Idx01FieldList <%s>\n", prpArrayInfo->crIdx01FieldList) ; fflush(prlFile) ;

   if (igTestOutput > 1) 
   {
    dbg(DEBUG,"IndexData") ; 
   }
   fprintf(prlFile,"IndexData\n"); fflush(prlFile);

   llRow = ARR_FIRST ;
   do
   {
    ilRc = CEDAArrayFindKey (&(prpArrayInfo->rrArrayHandle), &(prpArrayInfo->crArrayName[0]), &(prpArrayInfo->rrIdx01Handle), prpArrayInfo->crIdx01Name, &llRow, prpArrayInfo->lrIdx01RowLen, prpArrayInfo->pcrIdx01RowBuf);
    if (ilRc == RC_SUCCESS)
    {
     if (igTestOutput > 1) 
     {
      dbg(DEBUG, "SaveIndexInfo: Key <%ld> <%s>",llRow,prpArrayInfo->pcrIdx01RowBuf) ;
     }

     if (strlen (prpArrayInfo->pcrIdx01RowBuf) == 0)
     {
      ilRc = CEDAArrayGetRow(&(prpArrayInfo->rrArrayHandle), &(prpArrayInfo->crArrayName[0]), llRow, clDel, prpArrayInfo->lrArrayRowLen, prpArrayInfo->pcrArrayRowBuf);
      if(ilRc == RC_SUCCESS)
      {
       if (igTestOutput > 1) 
       {
        dbg (DEBUG, "SaveIndexInfo: Row <%ld> <%s>", llRow,prpArrayInfo->pcrArrayRowBuf) ;
       }
      } /* end of if */
     } /* end of if */

     pclTrimBuf = strdup (prpArrayInfo->pcrIdx01RowBuf) ;
     if (pclTrimBuf != NULL)
     {
      ilRc = GetDataItem (pclTrimBuf,prpArrayInfo->pcrIdx01RowBuf, 1, clDel, "", "  ") ;
      if (ilRc > 0)
      {
       ilRc = RC_SUCCESS ;
       fprintf (prlFile,"Key            <%ld><%s>\n",llRow,pclTrimBuf) ; fflush(prlFile) ;
      } /* end of if */

      free (pclTrimBuf) ;
      pclTrimBuf = NULL ;
     } /* end of if */

     llRow = ARR_NEXT;
    }
    else
    {
     dbg (TRACE, "SaveIndexInfo: CEDAArrayFindKey failed <%d>", ilRc) ;
    } /* end of if */
   } while (ilRc == RC_SUCCESS) ;

   if (igTestOutput > 1) 
   {
    dbg (DEBUG,"ArrayData") ;
   }
   fprintf (prlFile,"ArrayData\n"); fflush(prlFile);

   llRow = ARR_FIRST ;
   do
   {
    ilRc = CEDAArrayFindKey (&(prpArrayInfo->rrArrayHandle), &(prpArrayInfo->crArrayName[0]), &(prpArrayInfo->rrIdx01Handle), prpArrayInfo->crIdx01Name, &llRow, prpArrayInfo->lrIdx01RowLen, prpArrayInfo->pcrIdx01RowBuf);
    if (ilRc == RC_SUCCESS)
    {
     ilRc = CEDAArrayGetRow(&(prpArrayInfo->rrArrayHandle), &(prpArrayInfo->crArrayName[0]), llRow, clDel, prpArrayInfo->lrArrayRowLen, prpArrayInfo->pcrArrayRowBuf);
     if (ilRc == RC_SUCCESS)
     {
      if (igTestOutput > 1) 
      {
       dbg(DEBUG,"SaveIndexInfo: Row <%ld> <%s>",llRow,prpArrayInfo->pcrArrayRowBuf) ;
      }
      fprintf (prlFile,"Row            <%ld><%s>\n",llRow,prpArrayInfo->pcrArrayRowBuf) ; fflush(prlFile) ;
     }
     else
     {
      dbg (TRACE, "SaveIndexInfo: CEDAArrayGetRow failed <%d>", ilRc) ;
     } /* end of if */

     llRow = ARR_NEXT ;
    } 
    else
    {
     dbg (TRACE, "SaveIndexInfo: CEDAArrayFindKey failed <%d>", ilRc) ;
    } /* end of if */
   } while (ilRc == RC_SUCCESS) ;

   if (ilRc == RC_NOTFOUND)
   {
    ilRc = RC_SUCCESS ;
   } /* end of if */
  } /* end of if */

  if (prlFile != NULL)
  {
   fclose(prlFile) ;
   prlFile = NULL ;
   if (igTestOutput > 1) 
   {
    dbg (TRACE, "SaveIndexInfo: event saved - file <%s>", &clFile[0]) ;
   }
  } /* end of if */

  return (ilRc) ;

}

        
static int SendAnswer(int ipDest, char *pcpAnswer)
{
    int         ilRC;
    int         ilLen;
    EVENT           *prlOutEvent    = NULL;
    BC_HEAD     *prlOutBCHead   = NULL;
    CMDBLK      *prlOutCmdblk   = NULL;

    dbg(DEBUG,"SendAnswer: START-->");

    /* calculate size of memory we need */
    ilLen = sizeof(EVENT) + sizeof(BC_HEAD) + sizeof(CMDBLK) + strlen(pcpAnswer) + 32 ;

    /* get memory for out event */
    if ((prlOutEvent = (EVENT*)malloc((size_t)ilLen)) == NULL)
    {
        dbg(TRACE,"<SendAnswer> %05d malloc failed, can't allocate %d bytes", __LINE__, ilLen) ;
        dbg(DEBUG,"<SendAnswer> ----- END -----") ;
        return RC_FAIL ;
    }

    /* clear buffer */
    memset((void*)prlOutEvent, 0x00, ilLen) ;

    /* set structure members */
    prlOutEvent->type      = SYS_EVENT ;
    prlOutEvent->command       = EVENT_DATA ;
    prlOutEvent->originator  = ipDest ;            /* mod_id ;   */ 
    prlOutEvent->retry_count = 0 ;
    prlOutEvent->data_offset = sizeof(EVENT) ;
    prlOutEvent->data_length = ilLen - sizeof(EVENT) ;

    /* BCHead members */
    prlOutBCHead = (BC_HEAD*)((char*)prlOutEvent+sizeof(EVENT)) ;
    prlOutBCHead->rc = NETOUT_NO_ACK ;
    strncpy(prlOutBCHead->dest_name, "ACTION", 10) ;

    /* CMDBLK members */
    prlOutCmdblk = (CMDBLK*)((char*)prlOutBCHead->data) ;
    strcpy(prlOutCmdblk->command, "FIR") ;
    strcpy(prlOutCmdblk->tw_end, "ANS,WER,ACTION") ;

    /* data */
    strcpy(prlOutCmdblk->data+2, pcpAnswer) ;

    /* send this message */
    dbg(DEBUG,"<SendAnswer> sending to Mod-ID: %d", prgEvent->originator) ;
    if ((ilRC = que(QUE_PUT, prgEvent->originator, mod_id, PRIORITY_3, ilLen, (char*)prlOutEvent)) != RC_SUCCESS)
    {
        /* delete memory */
        free((void*)prlOutEvent) ;
        dbg(TRACE,"<SendAnswer> %05d QUE_PUT returns: %d", __LINE__, ilRC) ;
        dbg(DEBUG,"<SendAnswer> ----- END -----") ;
        return RC_FAIL ;
    }

/*  
    DebugPrintBchead (DEBUG, prlOutBCHead) ;
    DebugPrintCmdblk (DEBUG, prlOutCmdblk) ;
    DebugPrintEvent  (DEBUG, prlOutEvent) ;               
*/
    /* release memory */
    free((void*)prlOutEvent);
    
    dbg(DEBUG,"SendAnswer: <-- ENDE ");
    
    /* bye bye */
    return RC_SUCCESS;
}

static int GetDebugLevel(char *pcpMode, int *pipMode)
{
    int     ilRC = RC_SUCCESS;
    char    clCfgValue[64];

    ilRC = iGetConfigRow(cgConfigFile, "SYSTEM", pcpMode, CFG_STRING,
                            clCfgValue);

    if(ilRC != RC_SUCCESS)
    {
        dbg(TRACE,"SYSTEM %s not found in <%s>", pcpMode, cgConfigFile);
    }
    else
    {
        dbg(TRACE, "SYSTEM %s <%s>", pcpMode, clCfgValue);
        if (!strcmp(clCfgValue, "DEBUG"))
            *pipMode = DEBUG;
        else if (!strcmp(clCfgValue, "TRACE"))
            *pipMode = TRACE;
        else
            *pipMode = 0;
    }

    return ilRC;
}

static int ItemCnt(char *pcgData, char cpDelim)
{
    int ilItemCount=0;

    if (pcgData && strlen(pcgData)) 
    {
        ilItemCount = 1;
        while (*pcgData) 
        {
            if (*pcgData++ == cpDelim) 
            {
                ilItemCount++;
            } /* end if */
        } /* end while */
    } /* end if */
    return ilItemCount;
}

/******************************************************************************/
/* Get Item Nbr n from DATA with Delim '|' ...                                */
/******************************************************************************/
static int GetItemNbr(char *Item, char *Data, char Delim, long Nbr)
{
    int ilRC = RC_SUCCESS;              /* Return code */
    long llItemCnt = -1;
    int dmmy;
    int ilLoop;
    int ilFirstItemlngth = -1;
    char  *pclData   = NULL;
    char  *pclDelim   = NULL;
    
/*    dbg(DEBUG,"GetItemNbr: START-->");
*/
    pclData  = Data ;
    pclDelim = &Delim ;

    /*dbg(DEBUG,"GetItemNbr: counting items in '%s', delim is '%c'",Data,Delim);*/

    llItemCnt = ItemCnt(Data,Delim); 
    if (Nbr > llItemCnt)
    {
        dbg(DEBUG,"GetItemNbr: less than <%d> items in string (<%d>)!!!",Nbr,llItemCnt);
        ilRC = RC_FAIL;
    }
    else
    {
        for(ilLoop = 0 ; ilLoop < Nbr ; ilLoop++)
        {
            ilFirstItemlngth = GetNextDataItem(Item,&pclData,pclDelim,"","\0\0");
        }
    }
   
/*  dbg(DEBUG,"GetItemNbr: <-- ENDE ");
*/
    return (ilRC);
}

static int DumpArray(ARRAYINFO *prpArray,char *pcpFields)
{
    int ilRc;
    int ilRecord = 0;
    long llRowNum = ARR_FIRST;
    char *pclRowBuf;

    dbg(TRACE,"Arraydump: %s", prpArray->crArrayName);
    if (pcpFields != NULL)
    {
        dbg(TRACE,"Arraydump: Fields = %s", pcpFields);
    }
    else
    {
        dbg(TRACE,"Arraydump: Fields = %s",prpArray->crArrayFieldList);
    }

    while(CEDAArrayGetRowPointer(&prpArray->rrArrayHandle,
        prpArray->crArrayName,llRowNum,(void *)&pclRowBuf) == RC_SUCCESS)       
    {
            int ilFieldNo;
        
            ilRecord++;
            llRowNum = ARR_NEXT;

            dbg(TRACE,"Record No: %d",ilRecord);
            for (ilFieldNo = 1; ilFieldNo < prpArray->lrArrayFieldCnt; ilFieldNo++)
            {
                char pclFieldName[24];

                *pclFieldName = '\0';
                GetDataItem(pclFieldName,prpArray->crArrayFieldList,
                    ilFieldNo,',',"","\0\0");
                if (pcpFields != NULL)      
                {
                    if(strstr(pcpFields,pclFieldName) == NULL)
                    {
                        continue;
                    }
                }
                dbg(TRACE,"%s: <%s>",
                        pclFieldName,PFIELDVAL(prpArray,pclRowBuf,ilFieldNo-1));
            }
    }

    return ilRc;

}



/***********************************************************************************/
/* APO 02.05.2001                                                             */
/* GetAbsoluteElementPosition: locate <pcpRosl> in <pcpSequence> and return its   */
/*  position. Additionally the relative position (Xnd element in lsit) is stored in*/
/*  <pipRelativePosition>.                                                         */
/* Return: >=0 -> success, absolute position in string                             */
/*         < 0 -> element not found                                               */
/***********************************************************************************/
int GetAbsoluteElementPosition(char* pcpSequence, char cpRosl, 
                               int *pipRelativePosition, char cpDelim)
{
    int ilCount = 0;
    
    /* reset relative position */
    *pipRelativePosition = 0;

    /* search string for char */
    for (;ilCount<strlen(pcpSequence);ilCount+=1)
    {
        /* check current char */
        if (*(pcpSequence+ilCount) == cpDelim)
        {
            *pipRelativePosition += 1;
        }
        else if (*(pcpSequence+ilCount) == cpRosl)
        {
            /* element found -> return absolute position */
            return ilCount;
        }
    }
    
    /* when we get here the element has not been located -> terminate with error code */
    return -1;
}


/******************************************************************************/
/* APO 02.05.2001                                                             */
/* GetNextRoslFromSequence: search for the given <pcpRosl> in <pcpSequence>   */ 
/* and look the next valid ROSL which will be stored in <pcpNextRosl>.        */
/* <pcpNextRosl> must be an unmistakable stage as release target, e.g. if     */
/* <pcpRosl>='1' in <pcpSequence>= '1,LU,2,3,4' <pcpNextRosl> will be '2'     */
/* because 'LU' is ambigous (release to 'L' or 'U'?). <cpDelim> is the        */
/* delimiter used in <pcpSequence>.                                           */
/* Return: < 0 -> <pcpRosl> could not be found in <pcpSequence>               */
/*         ==0 -> <pcpRosl> has no <pcpNextRosl> because its the last element */
/*            or there are ambigous elements left only.                       */
/*         > 0 -> success, the position of <pcpNextRosl> in <pcpSequence>     */ 
/******************************************************************************/
int GetNextRoslFromSequence(char* pcpSequence, char cpRosl,
                            char* pcpNextRosl, char cpDelim)
{
    int ilCount = 0;
    int ilElementPosition = 0, ilNextElementPosition = 0, ilNextElementRelativePosition;
    
    /* first locate element in sequence */
    if ((ilElementPosition = GetAbsoluteElementPosition(pcpSequence,cpRosl,&ilNextElementRelativePosition,cpDelim)) < 0)
    {
        /* not found -> terminate */
        return -1;
    }

    /* start searching after absolute position in sequence */
    for (ilCount = ilElementPosition+1; ilCount<strlen(pcpSequence); ilCount++)
    {
        /* examine current char */
        if (*(pcpSequence+ilCount) == '\0')
        {
            /* end of sequence, next rosl not found -> terminate, return will be 0 */
            break;
        }
        else if (*(pcpSequence+ilCount) == cpDelim)
        {
            /* delimiter found -> next element begins */
            ilNextElementRelativePosition += 1;
            /* check if next char is valid (neither end of sequence nor delimiter nor vacancy stage) 
               and second next char is delim, end of sequence or vacancy stage (='U') */
            if ((*(pcpSequence+ilCount+1) != '\0') && 
                (*(pcpSequence+ilCount+1) != cpDelim) &&
                (*(pcpSequence+ilCount+1) != 'U') &&
                ((*(pcpSequence+ilCount+2) == '\0') ||
                 (*(pcpSequence+ilCount+2) == cpDelim) ||
                 (*(pcpSequence+ilCount+2) == 'U')))
            {
                /* next element found -> copy to <pcpNextRosl> */
                *pcpNextRosl = *(pcpSequence+ilCount+1);
                *(pcpNextRosl+1) = '\0';
                return ilNextElementRelativePosition;
            }
        }
    }
    return ilNextElementPosition;
}


/******************************************************************************/
/* CopyDRS_DRRU(): copy DRS with DRRU=<pcpOldDRRU> and set it to <pcpNewDRRU>.*/
/* Return: 0    -> operation successful, DRS updated                          */
/*         != 0 -> record not found or CEDA error                             */
/******************************************************************************/
static int CopyDRS_DRRU(char *pcpOldDRRU, char *pcpNewDRRU)
{
    long llRowNum = -1;
    int ilRC = RC_SUCCESS;
    char clDrsSel[32];
    char clTimeStamp[32];

#define M_BUFF 1024

    /* select DRS with DRRU=<pcpOldDRRU> */ 
    sprintf(clDrsSel,"WHERE DRRU='%s'",pcpOldDRRU); 
    /* refill opened array */
    if ((ilRC = CEDAArrayRefill(&sgDrsInfo,pcgDrsArrayName,clDrsSel,NULL, ARR_FIRST)) != RC_SUCCESS)
    {
        dbg(TRACE,"CopyDRS_DRRU: CEDAArrayFill for <%s> failed!",pcgDrsArrayName);
    }               

    /* get row */
    if ((ilRC =  ArrayFindRow(&sgDrsInfo, pcgDrsArrayName, "DRRU", pcpOldDRRU, &llRowNum)) == RC_SUCCESS)
    {
        /* new DRR record must exist    */
        if ((pcpNewDRRU == NULL) || (*pcpNewDRRU == '\0'))
        {
            /* an error occurred */
            dbg(DEBUG,"CopyDRS_DRRU: error from CEDAArray '%s' : no target DRR record exists",pcgDrsArrayName);
            return -1;
        }
        else
        {
            char pclData[M_BUFF];
            char clNewURNO[12];

            memset(pclData,0x00,M_BUFF);

            /* copy record */
            dbg(DEBUG,"CopyDRS_DRRU: copying DRS with DRRU=<%s>",pcpOldDRRU);

            /* read old data record */
            ilRC = CEDAArrayGetRow(&sgDrsInfo,pcgDrsArrayName,llRowNum,'\0',M_BUFF,pclData);
            if (ilRC != RC_SUCCESS)
            {
                /* error */
                dbg(TRACE,"CopyDRS_DRRU: CEDAArrayGetRow for <%s> failed!",pcgDrsArrayName);
                return -2;
            }

            /* add new data record  */
            ilRC = CEDAArrayAddRow(&sgDrsInfo,pcgDrsArrayName,&llRowNum,pclData);
            if (ilRC != RC_SUCCESS)
            {
                /* error */
                dbg(TRACE,"CopyDRS_DRRU: CEDAArrayAddRow for <%s> failed!",pcgDrsArrayName );
                return -3;
            }


                GetNextValues(clNewURNO,1);

            /* change urno  */
            if ((ilRC = CEDAArrayPutField(&sgDrsInfo,pcgDrsArrayName,NULL,"URNO",llRowNum,clNewURNO)) != RC_SUCCESS)
            {
                /* error */
                dbg(TRACE,"CopyDRS_DRRU: CEDAArrayPutField for <%s> failed",pcgDrsArrayName );
                return -4;
            }

            GetServerTimeStamp("UTC",1,0,clTimeStamp);
            /* change creation date */
            if ((ilRC = CEDAArrayPutField(&sgDrsInfo,pcgDrsArrayName,NULL,"CDAT",llRowNum,clTimeStamp)) != RC_SUCCESS)
            {
                /* error */
                dbg(TRACE,"CopyDRS_DRRU: CEDAArrayPutField for <%s> failed!",pcgDrsArrayName );
                return -4;
            }

            /* change last update date */
            /* change creator */
            /*************
            if ((ilRC = CEDAArrayPutField(&sgDrsInfo,pcgDrsArrayName,NULL,"LSTU",llRowNum,clTimeStamp)) != RC_SUCCESS)
            {
                dbg(TRACE,"CopyDRS_DRRU: CEDAArrayPutField for <%s> failed!",pcgDrsArrayName );
                return -4;
            }
            *************/
/*      ### USEC-USEU ###   */
            /* change creator */
            /*************
            if ((ilRC = CEDAArrayPutField(&sgDrsInfo,pcgDrsArrayName,NULL,"USEC",llRowNum,"RELDPL")) != RC_SUCCESS)
            {
                dbg(TRACE,"CopyDRS_DRRU: CEDAArrayPutField for <%s> failed!",pcgDrsArrayName );
                return -4;
            }
            *************/

            /* change last updater */
            /*************
            if ((ilRC = CEDAArrayPutField(&sgDrsInfo,pcgDrsArrayName,NULL,"USEU",llRowNum,"RELDPL")) != RC_SUCCESS)
            {
                dbg(TRACE,"CopyDRS_DRRU: CEDAArrayPutField for <%s> failed!",pcgDrsArrayName);
                return -4;
            }
            *************/
            
            /* change urno of refering DRR in DRS: DRRU is now = URNO of new DRR */
            if ((ilRC = CEDAArrayPutField(&sgDrsInfo,pcgDrsArrayName,NULL,"DRRU",llRowNum,pcpNewDRRU)) != RC_SUCCESS)
            {
                /* error */
                dbg(TRACE,"CopyDRS_DRRU: CEDAArrayPutField for <%s> failed  with !",pcgDrsArrayName);
                return -4;
            }
        }
    }
    else
    {
        /* DRSF flag is set, but no DRSF found -> print warning to log-file */
        /* (someone has to check the DRSTAB and DRRTAB manually...) */
        dbg(TRACE,"CopyDRS_DRRU: Warning: DRS with DRRU=<%s> NOT FOUND!!! Check DRRTAB / DRSTAB!", pcpOldDRRU);
        return -5;
    }

    /* save changes on DRSTAB */
    ilRC = CEDAArrayWriteDB(&sgDrsInfo,pcgDrsArrayName,NULL,NULL,NULL,NULL,ARR_COMMIT_ALL_OK);
    if (ilRC != RC_SUCCESS)
    {
        /* error */
        dbg(TRACE,"CopyDRS_DRRU: CEDAArrayWriteDB for <%s> failed  !",pcgDrsArrayName);
        return -6;
    }

    return 0;
}


/******************************************************************************/
/* Init DRS Array                                                           */
/******************************************************************************/
static int InitDRSArray()
{
    int ilRC   = RC_SUCCESS;              /* Return code */
    long llRow = -1;

    dbg(DEBUG,"InitDRSArray: START-->");
    /**********/
    /* DRSTAB */
    /**********/
    pcgDrsArrayName = "drstab";
    pcgDrsFields = "URNO,DRRU,SDAY,STFU,HOPO,ATS1,DRS1,DRS2,STAT,DRS3,USES,CDAT,DRS4,LSTU,USEC,USEU";
    lgDrsFldLen[0] = 32;    /* USEU */
    lgDrsFldLen[1] = 32;    /* USEC */
    lgDrsFldLen[2] = 14;    /* LSTU */
    lgDrsFldLen[3] = 1;     /* DRS4 */
    lgDrsFldLen[4] = 14;    /* CDAT */
    lgDrsFldLen[5] = 32;    /* USES */
    lgDrsFldLen[6] = 1;     /* DRS3 */
    lgDrsFldLen[7] = 1;     /* STAT */
    lgDrsFldLen[8] = 8;     /* DRS2 */
    lgDrsFldLen[9] = 1;     /* DRS1 */
    lgDrsFldLen[10] = 10;   /* ATS1 */
    lgDrsFldLen[11] = 3;    /* HOPO */
    lgDrsFldLen[12] = 10;   /* STFU */
    lgDrsFldLen[13] = 8;    /* SDAY */
    lgDrsFldLen[14] = 10;   /* DRRU */
    lgDrsFldLen[15] = 10;   /* URNO */

    memset(pcgDrsTab,0x00,10);
    sprintf(pcgDrsTab,"DRSTAB");

    sprintf(cgHopo,"");
    
    ilRC = CEDAArrayCreate (&sgDrsInfo,pcgDrsArrayName,pcgDrsTab,NULL, NULL, NULL, pcgDrsFields, lgDrsFldLen,0);
 
    if (ilRC != RC_SUCCESS)
    {
        dbg(DEBUG,"InitDRSArray: CEDAArrayCreate for <%s> failed  with <%d>!",
        pcgDrsArrayName, ilRC);
    }
    else
    {
                CEDAArraySendChanges2BCHDL(&sgDrsInfo, pcgDrsArrayName, TRUE);
    }

    dbg(DEBUG,"InitDRSArray finished  ");

    return (ilRC);
}


/******************************************************************************/
/* Returns Row (found by pattern)                                             */
/* if returnvalue == RC_FAIL, fkt found no valid row                          */
/******************************************************************************/
static int ArrayFindRow(HANDLE * pspHandle, char *pcpArrayName, char *pcgFieldName, char *pcPattern, long * Row)
{
    int   ilRC      = RC_FAIL;              /* Return code */
    int   ilCmpStr  = -1;
    long  llRow     = -1;
    long  llLoop    = -1;    
    int   ilItemNo  = 0;
    char  clDel     = '|';
    char *pclFields = NULL;
    char  cpTmp[256];
    char  pclData[M_BUFF];
    
    dbg(DEBUG,"ArrayFindRow: START-->");

    memset(pclData,0x00,M_BUFF);
    
    /*get count */
    CEDAArrayGetRowCount (pspHandle,pcpArrayName,&llRow);

    for(llLoop = 0 ; llLoop < llRow ; llLoop++)
    {
        if (llLoop == 0)
        {
            CEDAArrayGetField(pspHandle,pcpArrayName,
                      NULL , pcgFieldName,
                      M_BUFF, ARR_FIRST,
                      pclData);
        }
        else
        {
            CEDAArrayGetField(pspHandle,pcpArrayName,
                      NULL , pcgFieldName,
                      M_BUFF, ARR_NEXT,
                      pclData);
        }
        
        ilCmpStr = strncmp(pcPattern,pclData,strlen(pcPattern));
        if (0 == ilCmpStr)
        {
            *Row = ARR_CURRENT;
            dbg(DEBUG,"ArrayFindRow: pattern <%s> == data <%s> (cmp returns %d) in Row <%ld> ",pcPattern,pclData,ilCmpStr,Row);
            llLoop = llRow;
            ilRC = RC_SUCCESS;
        }
        {
            dbg(DEBUG,"ArrayFindRow: pattern <%s> != data <%s> (cmp returns %d)",pcPattern,pclData,ilCmpStr);
            
        }
    }
    if(ilRC != RC_SUCCESS)
    {
        dbg(DEBUG,"ArrayFindRow: pattern <%s> not found",pcPattern);
    }

    dbg(DEBUG,"ArrayFindRow: <-- ENDE ");

    return (ilRC);
}


 

static long GetLengtOfMonth(int ipMonth,int ipYear)
{
    long lllength = -1;
    
    switch (ipMonth)
    {
        case 1:     
            lllength = 31;
            break ;  
        case 2:     
            lllength = ipYear % 4 == 0 ? 29 : 28;
            break ;  
        case 3:     
            lllength = 31;
            break ;  
        case 4:     
            lllength = 30;
            break ;  
        case 5:     
            lllength = 31;
            break ;  
        case 6:     
            lllength = 30;
            break ;  
        case 7:     
            lllength = 31;
            break ;  
        case 8:     
            lllength = 31;
            break ;  
        case 9:     
            lllength = 30;
            break ;  
        case 10:     
            lllength = 31;
            break ;  
        case 11:     
            lllength = 30;
            break ;  
        case 12:     
            lllength = 31;
            break ;  
        default:    
            lllength = -1;
            dbg(TRACE,"GetLengtOfMonth: invalid month <%d>",ipMonth);
            break ;  
    }
    return lllength;
}


