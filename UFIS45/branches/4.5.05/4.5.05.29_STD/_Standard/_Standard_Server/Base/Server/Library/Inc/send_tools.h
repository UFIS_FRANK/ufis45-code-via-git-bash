#ifndef _DEF_mks_version_send_tools_h
  #define _DEF_mks_version_send_tools_h
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_send_tools_h[]   MARK_UNUSED = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Library/Inc/send_tools.h 1.2 2004/07/27 16:48:24SGT jim Exp  $";
#endif /* _DEF_mks_version */
/* ********************************************************************** */
/*  The Master include file.						  									  */
/*									  															  */
/*  Program	  :     						  												  */
/*  Revision date :							  											  */
/*  Author    	  :							  											  */
/*  									  														  */
/*  NOTE : This should be the only include file for your program          */
/* ********************************************************************** */
#ifndef 	_SEND_TOOLS_H
#define 	_SEND_TOOLS_H


#undef 	__PROZESS__				/* this is not a prozess... */
#define 	U_MAIN


#include <stdlib.h>
#include "glbdef.h"
#include "router.h"
#include "ugccsma.h"
#include "nmghdl.h"
#include "helpful.h"
#include "tools.h"
#include "ntisch.h"

#define	iINSERT_SECTION	0
#define	iDELETE_SECTION	1

extern char *mod_name;
extern int	mod_id;

int send_message(int, int, int, int, char *);
int ScheduleAction(short, short, char *, char *, char *, char *, char*);
int Timsch(int, int, int, int, PERIOD *, int, char *);
int HandleActionSection(int, char *, char *, int, int);
int GetUniqueNumber(char *, char *, char *, int, long *, long *, long *);

#endif /*_SEND_TOOLS_H */


