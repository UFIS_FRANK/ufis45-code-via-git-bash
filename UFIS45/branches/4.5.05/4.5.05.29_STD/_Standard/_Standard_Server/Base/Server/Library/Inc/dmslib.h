#ifndef _DEF_mks_version_dmslib_h
  #define _DEF_mks_version_dmslib_h
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_dmslib_h[]   MARK_UNUSED = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Library/Inc/dmslib.h 1.1 2011/08/09 16:26:44SGT bst Exp bst(2011/08/09 16:42:24SGT) $";
#endif /* _DEF_mks_version */
#ifndef __DMSLIB_H__
#define __DMSLIB_H__

extern int DMS_InitDmisEnvironment(void);

#endif
