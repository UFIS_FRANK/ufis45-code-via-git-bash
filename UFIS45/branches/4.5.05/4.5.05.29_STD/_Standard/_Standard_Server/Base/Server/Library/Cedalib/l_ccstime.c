#ifndef _DEF_mks_version_l_ccstime_c
  #define _DEF_mks_version_l_ccstime_c
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_l_ccstime_c[]   MARK_UNUSED = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Library/Cedalib/l_ccstime.c 1.4 2006/01/21 00:33:38SGT jim Exp  $";
#endif /* _DEF_mks_version */
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/* #define		UGCCS_FKT */

/* ******************************************************************** */
/*	$LS/l_ccstime.c							*/
/*	Library fuer 'time'-Funktionen					*/
/*	Autor: ipr	21.02.95					*/
/*									*/
/*	Update:								*/
/*			16.06.97 Timebuffer static (RBI) */
/*      17.03.03 JIM: added UtcTimeToLocalFixTZ and LocalTimeToUtcFixTZ */
/*                    Note: getenv("TZ") after putenv("TZ=...") may core*/
/*                    on SOLARIS!                                       */
/*      17.03.03 JIM: renamed UtcTimeToLocalFixTZ to UtcToLocalTimeFixTZ*/
/*                    "dbg(DEBUG," in U2L and L2U                       */
/* 20060117 JIM: OS AIX: altzone is not defined                         */
/* ********************************************************************	*/


#include <time.h>
#include "glbdef.h"
#include "l_ccstime.h"

extern FILE *outp;

#define CEDA_DATELEN 14

static int DebugPrintTm(int ipDebugFlag, struct tm *prpTimeStruct);
static int GetMinuteOfSwitch(char *pcpTimeString, struct tm *prpTimeStruct);
static int DaylightIsActiveAtDate(char *pcpDate);

/**************************  Function prototype header **************** */
/*  exportierten Funktionenprototypen	in 'l_ccstime.h'		*/

/* ******************************************************************** */
/* External variables							*/
/* ******************************************************************** */
/* ******************************************************************     
   Funktionname	: static char * ymdhm_str_time_stamp(void)
                  static char * ddmmyy_strtimestamp(void)
		  static char * hhmmss_strtimestamp(void)
   Input	: 
   Returnwert	: Pointer Char. auf String 'timestamp' 
                  z.B. "9202201545"
                  Pointer Char. auf String 'timestamp'
		  z.B. "20/02/95"
		  Pointer Char. auf String 'timestamp'
		  z.B. "15:45:59"
   ACHTUNG	:
   der Nutzer dieser Funktion muss selbst dafuer sorgen, dass 
   in dem String, wohin der Returnwert kopiert werden wird, 
   genuegend Platz vorhanden ist.
   Beispiel Anwendung:
   char yymmdd_str[8+1];
   strcpy(yymmdd_str, yymmdd_strtimestamp());

******************************************************************* */
char * yymmddHHMM_strtimestamp(void)
{
  static char timebuf[11];

  time_t	_CurTime;
  struct tm	*CurTime;
  timebuf[0] = '\0';
  _CurTime = time(0L);
  CurTime = (struct tm *) gmtime(&_CurTime);
  strftime(timebuf,11,"%" "y%" "m%" "d%" "H%" "M",CurTime);
  return(timebuf);
}
char *ddmmyy_strtimestamp(void)
{
  static char timebuf[9];

  time_t	_CurTime;
  struct tm	*CurTime;
  timebuf[0] = '\0';
  _CurTime = time(0L);
  CurTime = (struct tm *) gmtime(&_CurTime);
  strftime(timebuf,9,"%" "d/%" "m/%" "y",CurTime);
  return(timebuf);
}
char * hhmmss_strtimestamp(void)
{
  static char timebuf[9];

  time_t	_CurTime;
  struct tm	*CurTime;
  timebuf[0] = '\0';
  _CurTime = time(0L);
  CurTime = (struct tm *) gmtime(&_CurTime);
  strftime(timebuf,9,"%" "H:%" "M:%" "S",CurTime);
  return(timebuf);
}

#ifndef _WINNT
#ifndef _HPUX_SOURCE
/****************************************************************************/
/* Get the difference between UTC-time and Localtime                        */
/****************************************************************************/
int TimeTest(void)
{
	int ilRC = RC_SUCCESS;
	time_t rlTime = 0;
	time_t rlLoTime = 0;
	time_t rlGmTime = 0;
	struct tm *prlLoTimeStruct = NULL;
	struct tm *prlGmTimeStruct = NULL;
	char *pclTimeResult = NULL;
	char clTimeResultBuf[64];
	char clLocTime[16];
	char clUtcTime[16];

	dbg(DEBUG,"TimeTest: TZ <%s>",getenv("TZ"));

  	/* extern time_t timezone, altzone;		*/
  	/* extern int daylight;					*/
  	/* extern char *tzname[2];				*/

	dbg(DEBUG,"TimeTest: timezone  <%ld>",timezone);
#if !defined(_LINUX) && !defined(_AIX)
	dbg(DEBUG,"TimeTest: altzone   <%ld>",altzone);
#endif
	dbg(DEBUG,"TimeTest: daylight  <%ld>",daylight);

	dbg(DEBUG,"TimeTest: tzname[0] <%s>",tzname[0]);
	dbg(DEBUG,"TimeTest: tzname[1] <%s>",tzname[1]);

	rlTime = time(0);
	dbg(DEBUG,"TimeTest: time      <%ld>",rlTime);

	rlGmTime = time(0);
	dbg(DEBUG,"TimeTest: rlGmTime  <%ld>",rlTime);
	prlGmTimeStruct = gmtime(&rlGmTime);
	pclTimeResult = asctime(prlGmTimeStruct);
	memset(clTimeResultBuf,0x00,64);
	memcpy(&clTimeResultBuf[0],pclTimeResult,strlen(pclTimeResult)-1);
	dbg(DEBUG,"TimeTest: gmtime    <%s>",clTimeResultBuf);
	memset(clUtcTime,0x00,16);
	memset(clUtcTime,0x20,14);
	prlGmTimeStruct = gmtime(&rlGmTime);
	ilRC = TimeStructToCedaDate(clUtcTime,prlGmTimeStruct);
	dbg(DEBUG,"TimeTest: gmtime <%s>",clUtcTime);
	memset(clLocTime,0x00,16);
	memset(clLocTime,0x20,14);
	ilRC = UtcToLocaltime(clLocTime,clUtcTime);
	dbg(DEBUG,"TimeTest: gmtime <%s>",clUtcTime);
	dbg(DEBUG,"TimeTest: localtime <%s>",clLocTime);

	rlLoTime = time(0);
	dbg(DEBUG,"TimeTest: rlLoTime  <%ld>",rlTime);
	prlLoTimeStruct = (struct tm *) localtime(&rlLoTime);
	pclTimeResult = asctime(prlLoTimeStruct);
	memset(clTimeResultBuf,0x00,64);
	memcpy(&clTimeResultBuf[0],pclTimeResult,strlen(pclTimeResult)-1);
	dbg(DEBUG,"TimeTest: localtime <%s>",clTimeResultBuf);
	memset(clLocTime,0x00,16);
	memset(clLocTime,0x20,14);
	prlLoTimeStruct = (struct tm *) localtime(&rlLoTime);
	ilRC = TimeStructToCedaDate(clLocTime,prlLoTimeStruct);
	dbg(DEBUG,"TimeTest: localtime <%s>",clLocTime);
	memset(clUtcTime,0x00,16);
	memset(clUtcTime,0x20,14);
	ilRC = LocaltimeToUtc(clUtcTime,clLocTime);
	dbg(DEBUG,"TimeTest: localtime <%s>",clLocTime);
	dbg(DEBUG,"TimeTest: gmtime <%s>",clUtcTime);

	return(ilRC);

}/* end of TimeTest */



/****************************************************************************/
/* Get the difference between UTC-time and Localtime                        */
/*                                                                          */
/* if pcpDate is NULL, actual date is used !                                */
/****************************************************************************/
int GetTimeDiffToUtc(char *pcpDate, int *pipDiff)
{
	int ilRC = RC_SUCCESS;

	/***** Parameter check *****/

	if(pipDiff == NULL)
	{
		dbg(TRACE,"GetTimeDiffToUtc: invalid 2. parameter");
		ilRC = RC_FAIL;
	}/* end of if */

	if(pcpDate == NULL)
	{
		dbg(DEBUG,"GetTimeDiffToUtc: using actual date");
	} else {
	}/* end of if */



	/***** functionality *****/

	if(ilRC == RC_SUCCESS)
	{
		switch(daylight)
		{
		case 0 :
			dbg(DEBUG,"GetTimeDiffToUtc: daylight is not set");
			*pipDiff = timezone;
			break;
		case 1 :
#if !defined(_LINUX) && !defined(_AIX)
			dbg(DEBUG,"GetTimeDiffToUtc: daylight is set");
			*pipDiff = altzone;
#endif
			break;
		default :
			dbg(DEBUG,"GetTimeDiffToUtc: unknown value of daylight <%d>",daylight);
		}/* end of switch */
	}/* end of if */

	return(ilRC);

}/* end of GetTimeDiffToUtc */
#endif
#endif

/****************************************************************************/
/*                                                                          */
/****************************************************************************/
int GetDateOfSwitch(char *pcpWinterToSummer,char *pcpSummerToWinter,char *pcpYear)
{
	int ilRC = RC_SUCCESS;
	int ilYear = 0;
	int ilYearLength = 0;
	int ilDayOfYear = 0;
	int ilDstFlag = 0;
	int ilSwitchCounter = 0;
    time_t rlTime = 0;
    struct tm *prlTimeStruct = NULL;
    struct tm *prlLocalTimeResult = NULL;


	/***** Parameter check *****/

	if(pcpWinterToSummer == NULL)
	{
		dbg(TRACE,"GetDateOfSwitch: invalid 1. parameter");
		ilRC = RC_FAIL;
	} else {
		if(strlen(pcpWinterToSummer) < CEDA_DATELEN)
		{
			dbg(TRACE,"GetDateOfSwitch: 1. parameter too short : minimum length = %d",CEDA_DATELEN);
			ilRC = RC_FAIL;
		}/* end of if */
	}/* end of if */

	if(pcpWinterToSummer == NULL)
	{
		dbg(TRACE,"GetDateOfSwitch: invalid 2. parameter");
		ilRC = RC_FAIL;
	} else {
		if(strlen(pcpSummerToWinter) < CEDA_DATELEN)
		{
			dbg(TRACE,"GetDateOfSwitch: 2. parameter too short : minimum length = %d",CEDA_DATELEN);
			ilRC = RC_FAIL;
		}/* end of if */
	}/* end of if */

	if(pcpYear == NULL)
	{
		dbg(TRACE,"GetDateOfSwitch: invalid 3. parameter");
		ilRC = RC_FAIL;
	} else {
		ilYearLength = strlen(pcpYear);
		if (ilYearLength == 4 )
		{
			ilYear = atoi(&pcpYear[0]) - 1900;
			if(ilYear < 0)
			{
				dbg(TRACE,"GetDateOfSwitch: invalid 3. parameter : sould  be > 0 <%s>",pcpYear);
				ilRC = RC_FAIL;
			}/* end of if */
		}else{
			dbg(TRACE,"GetDateOfSwitch: invalid length 3. parameter : length sould be 4 <%s>",pcpYear);
			ilRC = RC_FAIL;
		}/* end of if */
	}/* end of if */


	/***** functionality *****/

	if(ilRC == RC_SUCCESS)
	{
		if(daylight == 0)
		{
        	dbg(TRACE,"GetDateOfSwitch: no alternate timezone : daylight <%d> TZ <%s>",daylight,getenv("TZ"));
			ilRC = RC_FAIL;
		} else {
    		prlTimeStruct = (struct tm *) calloc(1,sizeof(struct tm));
    		if(prlTimeStruct == NULL)
    		{
        		dbg(TRACE,"GetDateOfSwitch: calloc failed <%d:%s>",errno,strerror(errno));
        		ilRC = RC_FAIL;
    		} else {
				prlLocalTimeResult = NULL;
				rlTime = time(0);

				/* prlLocalTimeResult = (struct tm *)localtime_r(&rlTime,(struct tm *)prlTimeStruct); */

				prlLocalTimeResult = localtime(&rlTime);
				if(prlLocalTimeResult != NULL)
				{
					memcpy((char *)prlTimeStruct,(char *)prlLocalTimeResult,sizeof(struct tm));

					prlTimeStruct->tm_year = ilYear;
					prlTimeStruct->tm_mon = 11;
					prlTimeStruct->tm_mday = 31;

					ilRC = TimeStructToTimeType(&rlTime,prlTimeStruct,NULL);
					if(ilRC == RC_SUCCESS)
					{
						ilSwitchCounter = 0;
						ilDayOfYear = 0;

						memset(prlTimeStruct,0x00,sizeof(struct tm));
						prlLocalTimeResult = NULL;

						/* prlLocalTimeResult = (struct tm *)localtime_r(&rlTime,(struct tm *)prlTimeStruct); */

						prlLocalTimeResult = (struct tm *)localtime(&rlTime);
						if(prlLocalTimeResult != NULL)
						{
							memcpy((char *)prlTimeStruct,(char *)prlLocalTimeResult,sizeof(struct tm));

							do
							{
								ilDstFlag = prlTimeStruct->tm_isdst;
								memset(prlTimeStruct,0x00,sizeof(struct tm));
								prlLocalTimeResult = NULL;
	
								/* prlLocalTimeResult = (struct tm *)localtime_r(&rlTime,(struct tm *)prlTimeStruct); */

								prlLocalTimeResult = (struct tm *)localtime(&rlTime);
								if(prlLocalTimeResult != NULL)
								{
									memcpy((char *)prlTimeStruct,(char *)prlLocalTimeResult,sizeof(struct tm));

									if(ilDstFlag != prlTimeStruct->tm_isdst)
									{
										ilSwitchCounter++;

										switch(ilSwitchCounter)
										{
										case 1 :
											ilRC = GetMinuteOfSwitch(pcpWinterToSummer,prlTimeStruct);
											if(ilRC != RC_SUCCESS)
											{
												dbg(TRACE,"GetDateOfSwitch: GetMinuteOfSwitch failed");
												DebugPrintTm(TRACE,prlTimeStruct);
										}/* end of if */
										break;

										case 2 :
											ilRC = GetMinuteOfSwitch(pcpSummerToWinter,prlTimeStruct);
											if(ilRC != RC_SUCCESS)
											{
												dbg(TRACE,"GetDateOfSwitch: GetMinuteOfSwitch failed");
												DebugPrintTm(TRACE,prlTimeStruct);
											}/* end of if */
											break;
	
										default :
											dbg(DEBUG,"GetDateOfSwitch: day of <%d>. switch",ilSwitchCounter);
											DebugPrintTm(TRACE,prlTimeStruct);
											ilRC = RC_FAIL;
										}/* end of switch */
									}/* end of if */

									ilDayOfYear++;
									rlTime += 86400; /* next day */
								} else {
									dbg(TRACE,"GetDateOfSwitch: localtime failed");
									ilRC = RC_FAIL;
								}/* end of if */
							} while((ilDayOfYear < 367) && (ilRC == RC_SUCCESS));
				
							if(ilSwitchCounter > 2)
							{
								dbg(TRACE,"GetDateOfSwitch: too many dates found <%d>",ilSwitchCounter);
								ilRC = RC_FAIL;
							}/* end of if */
						} else {
							dbg(TRACE,"GetDateOfSwitch: localtime failed");
							ilRC = RC_FAIL;
						}/* end of if */
					} else {
						dbg(TRACE,"GetDateOfSwitch: TimeStructToTimeType failed");
					}/* end of if */
				} else {
					dbg(TRACE,"GetDateOfSwitch: localtime failed");
					ilRC = RC_FAIL;
    			}/* end of if */

				free(prlTimeStruct);
    		}/* end of if */

		}/* end of if */
	}/* end of if */

	return(ilRC);

}/* end of GetDateOfSwitch */


/****************************************************************************/
/*                                                                          */
/****************************************************************************/
static int GetMinuteOfSwitch(char *pcpTimeString, struct tm *prpTimeStruct)
{
	int ilRC = RC_SUCCESS;
	int ilFound = FALSE;
	int ilMinuteOfDay = 0;
	int ilDstFlag = 0;
    time_t rlTime = 0;
    struct tm *prlTimeStruct = NULL;
    struct tm *prlLocalTimeResult = NULL;


	/***** Parameter check *****/

	if((pcpTimeString) == NULL)
	{
		dbg(TRACE,"GetMinuteOfSwitch: invalid 1. parameter");
		ilRC = RC_FAIL;
	} else {
		if(strlen(pcpTimeString) < CEDA_DATELEN)
		{
			dbg(TRACE,"GetMinuteOfSwitch: 1. parameter too short : minimum length = %d",CEDA_DATELEN);
			ilRC = RC_FAIL;
		}/* end of if */
	}/* end of if */

	if((prpTimeStruct) == NULL)
	{
		dbg(TRACE,"GetMinuteOfSwitch: invalid 2. parameter");
		ilRC = RC_FAIL;
	}/* end of if */


	/***** functionality *****/

	if(ilRC == RC_SUCCESS)
	{
		prlTimeStruct = (struct tm *) calloc(1,sizeof(struct tm));
		if(prlTimeStruct == NULL)
		{
			dbg(TRACE,"GetMinuteOfSwitch: calloc failed <%d:%s>",errno,strerror(errno));
			ilRC = RC_FAIL;
		} else {
			memcpy((char *)prlTimeStruct,(char *)prpTimeStruct,sizeof(struct tm));

			ilRC = TimeStructToTimeType(&rlTime,prlTimeStruct,NULL);
			if(ilRC == RC_SUCCESS)
			{
				rlTime -= 86400; /* prev day */
				rlTime -= 60;

				ilMinuteOfDay = 0;
				ilFound = FALSE;

				memset(prlTimeStruct,0x00,sizeof(struct tm));
				prlLocalTimeResult = NULL;

				/* prlLocalTimeResult = (struct tm *)localtime_r(&rlTime,(struct tm *)prlTimeStruct); */

				prlLocalTimeResult = (struct tm *)localtime(&rlTime);
				if(prlLocalTimeResult != NULL)
				{
					memcpy((char *)prlTimeStruct,(char *)prlLocalTimeResult,sizeof(struct tm));

					do
					{
						ilDstFlag = prlTimeStruct->tm_isdst;
						memset(prlTimeStruct,0x00,sizeof(struct tm));
						prlLocalTimeResult = NULL;

						/* prlLocalTimeResult = (struct tm *)localtime_r(&rlTime,(struct tm *)prlTimeStruct); */

						prlLocalTimeResult = (struct tm *)localtime(&rlTime);
						if(prlLocalTimeResult != NULL)
						{
							memcpy((char *)prlTimeStruct,(char *)prlLocalTimeResult,sizeof(struct tm));

							if(ilDstFlag != prlTimeStruct->tm_isdst)
							{
								prlTimeStruct->tm_sec = 0;
								ilRC = TimeStructToCedaDate(pcpTimeString,prlTimeStruct);
								if(ilRC != RC_SUCCESS)
								{
									dbg(TRACE,"GetMinuteOfSwitch: TimeStructToCedaDate failed");
									ilRC = RC_FAIL;
								} else {
									ilFound = TRUE;
								}/* end of if */
							}/* end of if */
			
							ilMinuteOfDay++;
							rlTime += 60; /* next minute */
						} else {
							dbg(TRACE,"GetMinuteOfSwitch: localtime failed");
							ilRC = RC_FAIL;
						}/* end of if */
					} while((ilMinuteOfDay < 1442) && (ilRC == RC_SUCCESS) && (ilFound == FALSE));
				} else {
					dbg(TRACE,"GetMinuteOfSwitch: localtime failed");
					ilRC = RC_FAIL;
				}/* end of if */
			} else {
				dbg(TRACE,"GetMinuteOfSwitch: TimeStructToTimeType failed");
			}/* end of if */
			free(prlTimeStruct);
		}/* end of if */
	}/* end of if */
				
	return(ilRC);

}/* end of GetMinuteOfSwitch */



/****************************************************************************/
/* convert UTC-time to Localtime                                            */
/****************************************************************************/
static int DaylightIsActiveAtDate(char *pcpDate)
{
	int	ilRC = RC_SUCCESS;
	char pclWinterToSummer[16];
	char pclSummerToWinter[16];
	char pclYear[8];

	memset(pclYear,0x00,8);
	memset(pclWinterToSummer,0x00,16);
	memset(pclSummerToWinter,0x00,16);



    /***** Parameter check *****/

    if(pcpDate == NULL)
    {
        dbg(TRACE,"DaylightIsActiveAtDate: invalid 1. parameter");
        ilRC = RC_FAIL;
    } else {
        if(strlen(pcpDate) < CEDA_DATELEN)
        {
            dbg(TRACE,"DaylightIsActiveAtDate: 1. parameter too short : minimum length = %d",CEDA_DATELEN);
            ilRC = RC_FAIL;
        }/* end of if */
    }/* end of if */



	/***** functionality *****/

	if(ilRC == RC_SUCCESS)
	{
		sprintf(pclWinterToSummer,"19670414000000");
		sprintf(pclSummerToWinter,"19670414000000");

		strncpy(pclYear,pcpDate,4);

		dbg(DEBUG,"DaylightIsActiveAtDate: checking                          <%s>",pcpDate);

		ilRC = GetDateOfSwitch(pclWinterToSummer,pclSummerToWinter,pclYear);
		if(ilRC == RC_SUCCESS)
		{
			dbg(DEBUG,"DaylightIsActiveAtDate: winter to summer                  <%s>",pclWinterToSummer);
			dbg(DEBUG,"DaylightIsActiveAtDate: summer to winter                  <%s>",pclSummerToWinter);
	
			ilRC = strcmp(pcpDate,pclWinterToSummer);
			if(ilRC <= 0)
			{
				dbg(DEBUG,"DaylightIsActiveAtDate:  daylight saving is NOT active at <%s>",pcpDate);
				ilRC = RC_FAIL;
			} else {
				ilRC = strcmp(pcpDate,pclSummerToWinter);
				if(ilRC <= 0)
				{
					dbg(DEBUG,"DaylightIsActiveAtDate:  daylight saving is active at     <%s>",pcpDate);
					ilRC = RC_SUCCESS;
				} else {
					dbg(DEBUG,"DaylightIsActiveAtDate:  daylight saving is NOT active at <%s>",pcpDate);
					ilRC = RC_FAIL;
				}/* end of if */
			}/* end of if */
		} else {
			dbg(TRACE,"DaylightIsActiveAtDate: GetDateOfSwitch failed");
		}/* end of if */
	}/* end of if */
	
	return(ilRC);

}/* end of DaylightIsActiveAtDate */



/****************************************************************************/
/* convert UTC-time to Localtime                                            */
/****************************************************************************/
int UtcToLocaltime(char *pcpLocalDate, char *pcpUtcDate)
{
	int ilRC = RC_SUCCESS;
	int ilDateLength = 0;
    time_t rlTime2 = 0;
    struct tm *prlLocalTimeResult = NULL;
    struct tm *prlTimeStruct = NULL;


	/***** Parameter check *****/

	if(pcpLocalDate == NULL)
	{
		dbg(TRACE,"UtcToLocaltime: invalid 1. parameter");
		ilRC = RC_FAIL;
	} else {
		ilDateLength = strlen(pcpLocalDate);
		if(ilDateLength < CEDA_DATELEN)
		{
			dbg(TRACE,"UtcToLocaltime: <%s> is too short : length should be %d",pcpLocalDate,CEDA_DATELEN);
			ilRC = RC_FAIL;
		} else {
			if(ilDateLength > CEDA_DATELEN)
			{
				dbg(TRACE,"UtcToLocaltime: <%s> is too long : length should be %d",pcpLocalDate,CEDA_DATELEN);
				ilRC = RC_FAIL;
			}/* end of if */
		}/* end of if */
	}/* end of if */

	if(pcpUtcDate == NULL)
	{
		dbg(TRACE,"UtcToLocaltime: invalid 2. parameter");
		ilRC = RC_FAIL;
	} else {
		ilDateLength = strlen(pcpUtcDate);
		if(ilDateLength < CEDA_DATELEN)
		{
			dbg(TRACE,"UtcToLocaltime: <%s> is too short : length should be %d",pcpUtcDate,CEDA_DATELEN);
			ilRC = RC_FAIL;
		} else {
			if(ilDateLength > CEDA_DATELEN)
			{
				dbg(TRACE,"UtcToLocaltime: <%s> is too long : length should be %d",pcpUtcDate,CEDA_DATELEN);
				ilRC = RC_FAIL;
			}/* end of if */
		}/* end of if */
	}/* end of if */


	/***** functionality *****/

	if(ilRC == RC_SUCCESS)
	{
		prlTimeStruct = (struct tm *) calloc(1,sizeof(struct tm));
		if(prlTimeStruct == NULL)
		{
			dbg(TRACE,"UtcToLocaltime: calloc failed <%d:%s>",errno,strerror(errno));
			ilRC = RC_FAIL;
		} else {
			ilRC = CedaDateToTimeStruct(prlTimeStruct,pcpUtcDate);
			if(ilRC == RC_SUCCESS)
			{
				ilRC = TimeStructToTimeType(&rlTime2,prlTimeStruct,NULL);
				if(ilRC == RC_SUCCESS)
				{
					/* XXX prlLocalTimeResult = (struct tm *)localtime_r(&rlTime2,(struct tm *)prlTimeStruct); */

					prlLocalTimeResult = (struct tm *) localtime(&rlTime2);
					if(prlLocalTimeResult != NULL)
					{
						memcpy((char *)prlTimeStruct,(char *)prlLocalTimeResult,sizeof(struct tm));
						/* dbg(TRACE,"UtcToLocaltime: rlTime2 <%ld>",rlTime2); */
					
						ilRC = TimeStructToCedaDate(pcpLocalDate,prlTimeStruct);
						if(ilRC != RC_SUCCESS)
						{
							dbg(TRACE,"UtcToLocaltime: TimeStructToCedaDate failed");
							DebugPrintTm(TRACE,prlTimeStruct);
						}/* end of if */
					} else {
						dbg(TRACE,"UtcToLocaltime: localtime failed");
						ilRC = RC_FAIL;
					}/* end of if */
				} else {
					dbg(TRACE,"UtcToLocaltime: TimeStructToTimeType failed");
					ilRC = RC_FAIL;
				}/* end of if */
			} else {
				dbg(TRACE,"UtcToLocaltime: CedaDateToTimeStruct failed");
			}/* end of if */
			free(prlTimeStruct);
		}/* end of if */
	}/* end of if */

	return(ilRC);

}/* end of UtcToLocaltime */


/****************************************************************************/
/* convert Localtime to UTC-time                                            */
/****************************************************************************/
int LocaltimeToUtc(char *pcpUtcDate, char *pcpLocalDate)
{
	int ilRC = RC_SUCCESS;
	int ilDateLength = 0;
    time_t rlTime1 = 0;
    struct tm *prlTimeStruct = NULL;
    struct tm *prlLocalTimeResult = NULL;


	/***** Parameter check *****/

	if(pcpUtcDate == NULL)
	{
		dbg(TRACE,"LocaltimeToUtc: invalid 1. parameter");
		ilRC = RC_FAIL;
	} else {
		ilDateLength = strlen(pcpUtcDate);
		if(ilDateLength < CEDA_DATELEN)
		{
			dbg(TRACE,"LocaltimeToUtc: <%s> is too short : length should be %d",pcpUtcDate,CEDA_DATELEN);
			ilRC = RC_FAIL;
		} else {
			if(ilDateLength > CEDA_DATELEN)
			{
				dbg(TRACE,"LocaltimeToUtc: <%s> is too long : length should be %d",pcpUtcDate,CEDA_DATELEN);
				ilRC = RC_FAIL;
			}/* end of if */
		}/* end of if */
	}/* end of if */

	if(pcpLocalDate == NULL)
	{
		dbg(TRACE,"LocaltimeToUtc: invalid 2. parameter");
		ilRC = RC_FAIL;
	} else {
		ilDateLength = strlen(pcpLocalDate);
		if(ilDateLength < CEDA_DATELEN)
		{
			dbg(TRACE,"LocaltimeToUtc: <%s> is too short : length should be %d",pcpLocalDate,CEDA_DATELEN);
			ilRC = RC_FAIL;
		} else {
			if(ilDateLength > CEDA_DATELEN)
			{
				dbg(TRACE,"LocaltimeToUtc: <%s> is too long : length should be %d",pcpLocalDate,CEDA_DATELEN);
				ilRC = RC_FAIL;
			}/* end of if */
		}/* end of if */
	}/* end of if */


	/***** functionality *****/

	if(ilRC == RC_SUCCESS)
	{
		prlTimeStruct = (struct tm *) calloc(1,sizeof(struct tm));
		if(prlTimeStruct == NULL)
		{
			dbg(TRACE,"LocaltimeToUtc: calloc failed <%d:%s>",errno,strerror(errno));
			ilRC = RC_FAIL;
		} else {
			ilRC = CedaDateToTimeStruct(prlTimeStruct,pcpLocalDate);
			if(ilRC == RC_SUCCESS)
			{
				ilRC = DaylightIsActiveAtDate(pcpLocalDate);
				if(ilRC == RC_SUCCESS)
				{
					prlTimeStruct->tm_isdst = 1;
				} else {
					prlTimeStruct->tm_isdst = 0;
					ilRC = RC_SUCCESS;
				}/* end of if */
		
				DebugPrintTm(DEBUG,prlTimeStruct);
	
				ilRC = TimeStructToTimeType(&rlTime1,prlTimeStruct,NULL);
				if(ilRC == RC_SUCCESS)
				{
					/* YYY prlLocalTimeResult = (struct tm *)gmtime_r(&rlTime1,(struct tm *)prlTimeStruct); */
	
					prlLocalTimeResult = (struct tm *) gmtime(&rlTime1);
					if(prlLocalTimeResult != NULL)
					{
						memcpy((char *)prlTimeStruct,(char *)prlLocalTimeResult,sizeof(struct tm));
	
						DebugPrintTm(DEBUG,prlTimeStruct);
	
						ilRC = TimeStructToCedaDate(pcpUtcDate,prlTimeStruct);
						if(ilRC != RC_SUCCESS)
						{
							dbg(TRACE,"LocaltimeToUtc: TimeStructToCedaDate failed");
							DebugPrintTm(TRACE,prlTimeStruct);
						}/* end of if */

						dbg(DEBUG,"LocaltimeToUtc: UtcDate <%s>",pcpUtcDate);
					} else {
						dbg(TRACE,"LocaltimeToUtc: gmtime failed");
						ilRC = RC_FAIL;
					}/* end of if */
				} else {
					dbg(TRACE,"LocaltimeToUtc: TimeStructToTimeType failed");
					ilRC = RC_FAIL;
				}/* end of if */
			} else {
				dbg(TRACE,"LocaltimeToUtc: CedaDateToTimeStruct failed");
				ilRC = RC_FAIL;
			}/* end of if */
			free(prlTimeStruct);
		}/* end of if */
	}/* end of if */

	return(ilRC);

}/* end of LocaltimeToUtc */


/****************************************************************************/
/* converting a struct tm into a time_t using UTC not localtime             */
/****************************************************************************/
int TimeStructToTimeType(time_t *prpTimeType,  struct tm *prpTimeStruct, char *pcpTimeZone)
{
	int  ilRC = RC_SUCCESS;
	char clTimezoneBuf[1024];


	/***** Parameter check *****/

	if(prpTimeType == NULL)
	{
		dbg(TRACE,"TimeStructToTimeType: invalid 1. parameter");
		ilRC = RC_FAIL;
	}/* end of if */

	if(prpTimeStruct == NULL)
	{
		dbg(TRACE,"TimeStructToTimeType: invalid 2. parameter");
		ilRC = RC_FAIL;
	}/* end of if */

	if(pcpTimeZone == NULL)
	{
		/* using actual timezone */
	} else {
		dbg(DEBUG,"TimeStructToTimeType: using TimeZone <%s>",pcpTimeZone);
	}/* end of if */


	/***** functionality *****/

	if(ilRC == RC_SUCCESS)
	{
		if(prpTimeStruct->tm_isdst > 0)
		{
			dbg(DEBUG,"TimeStructToTimeType: daylightsaving is active <%d>",prpTimeStruct->tm_isdst);
		}/* end of if */

		if(pcpTimeZone != NULL)
		{
			sprintf(clTimezoneBuf,"TZ=%s",getenv("TZ"));
			putenv(pcpTimeZone);
			tzset();
		}/* end of if */

		*prpTimeType = mktime(prpTimeStruct);

		if(pcpTimeZone != NULL)
		{
			putenv(clTimezoneBuf);
			tzset();
		}/* end of if */

		if(*prpTimeType == -1)
		{
			dbg(TRACE,"TimeStructToTimeType: mktime failed <%d:%s>",errno,strerror(errno));
			ilRC = RC_FAIL;
		} else {
			/* fprintf(outp,"time_t <%ld>\n",*prpTimeType);fflush(outp);*/
		}/* end of if */
	}/* end of if */

	return(ilRC);

}/* end of TimeStructToTimeType */

/****************************************************************************/
/* */
/****************************************************************************/
int CedaDateToTimeStruct(struct tm *prpTimeStruct, char *pcpCedaDate)
{
	int  ilRC = RC_SUCCESS;
	int  ilDateLength = 0;
	char *pclBuf = NULL;
	char clBuf[8];


	/***** Parameter check *****/

	if(prpTimeStruct == NULL)
	{
		dbg(TRACE,"CedaDateToTimeStruct: invalid 1. parameter");
		ilRC = RC_FAIL;
	}/* end of if */

	if(pcpCedaDate == NULL)
	{
		dbg(TRACE,"CedaDateToTimeStruct: invalid 2. parameter");
		ilRC = RC_FAIL;
	} else {
		ilDateLength = strlen(pcpCedaDate);
		if(ilDateLength < CEDA_DATELEN)
		{
			dbg(TRACE,"CedaDateToTimeStruct: <%s> is too short : length should be %d",pcpCedaDate,CEDA_DATELEN);
			ilRC = RC_FAIL;
		} else {
			if(ilDateLength > CEDA_DATELEN)
			{
				dbg(TRACE,"CedaDateToTimeStruct: <%s> is too long : length should be %d",pcpCedaDate,CEDA_DATELEN);
				ilRC = RC_FAIL;
			}/* end of if */
		}/* end of if */
	}/* end of if */


	/***** functionality *****/

	if(ilRC == RC_SUCCESS)
	{
		memset((char *)prpTimeStruct,0x00,sizeof(struct tm));

		memset(clBuf,0x00,8);
		memcpy(clBuf,&pcpCedaDate[0],4);
		prpTimeStruct->tm_year = (atoi(clBuf) - 1900);

		memset(clBuf,0x00,8);
		memcpy(clBuf,&pcpCedaDate[4],2);
		prpTimeStruct->tm_mon  = atoi(clBuf) - 1;

		memset(clBuf,0x00,8);
		memcpy(clBuf,&pcpCedaDate[6],2);
		prpTimeStruct->tm_mday = atoi(clBuf);

		memset(clBuf,0x00,8);
		memcpy(clBuf,&pcpCedaDate[8],2);
		prpTimeStruct->tm_hour = atoi(clBuf);

		memset(clBuf,0x00,8);
		memcpy(clBuf,&pcpCedaDate[10],2);
		prpTimeStruct->tm_min  = atoi(clBuf);

		memset(clBuf,0x00,8);
		memcpy(clBuf,&pcpCedaDate[12],2);
		prpTimeStruct->tm_sec  = atoi(clBuf);

	}/* end of if */

	return(ilRC);

}/* end of CedaDateToTimeStruct */



/****************************************************************************/
/* */
/****************************************************************************/
int TimeStructToCedaDate(char *pcpCedaDate, struct tm *prpTimeStruct)
{
	int  ilRC = RC_SUCCESS;
	int  ilDateLength = 0;


	/***** Parameter check *****/

	if(pcpCedaDate == NULL)
	{
		dbg(TRACE,"TimeStructToCedaDate: invalid 1. parameter");
		ilRC = RC_FAIL;
	} else {
		ilDateLength = strlen(pcpCedaDate);
		if(ilDateLength < CEDA_DATELEN)
		{
			dbg(TRACE,"TimeStructToCedaDate: <%s> is too short : length should be %d",pcpCedaDate,CEDA_DATELEN);
			ilRC = RC_FAIL;
		} else {
			if(ilDateLength > CEDA_DATELEN)
			{
				dbg(TRACE,"TimeStructToCedaDate: <%s> is too long : length should be %d",pcpCedaDate,CEDA_DATELEN);
				ilRC = RC_FAIL;
			}/* end of if */
		}/* end of if */
	}/* end of if */

	if(prpTimeStruct == NULL)
	{
		dbg(TRACE,"TimeStructToCedaDate: invalid 2. parameter");
		ilRC = RC_FAIL;
	}/* end of if */


	/***** functionality *****/

	if(ilRC == RC_SUCCESS)
	{
		errno = 0;
		ilRC = strftime(pcpCedaDate,(CEDA_DATELEN+1),"%" "Y%" "m%" "d%" "H%" "M%" "S",prpTimeStruct);
		if(ilRC == CEDA_DATELEN)
		{
			ilRC = RC_SUCCESS;
		} else {
			dbg(DEBUG,"TimeStructToCedaDate: strftime failed <%d> <%d:%s>",ilRC,errno,strerror(errno));
			ilRC = RC_FAIL;
		}/* end of if */
	}/* end of if */

	return(ilRC);

}/* end of TimeStructToCedaDate*/



/****************************************************************************/
/* */
/****************************************************************************/
static int DebugPrintTm(int ipDebugFlag, struct tm *prpTimeStruct)
{
	int ilRC = RC_SUCCESS;

	if(prpTimeStruct == NULL)
	{
		dbg(TRACE,"DebugPrintTm: invalid 1. parameter");
		ilRC = RC_FAIL;
	} else {
		dbg(ipDebugFlag,"DebugPrintTm: Time->tm_sec   <%d>",prpTimeStruct->tm_sec);
		dbg(ipDebugFlag,"DebugPrintTm: Time->tm_min   <%d>",prpTimeStruct->tm_min);
		dbg(ipDebugFlag,"DebugPrintTm: Time->tm_hour  <%d>",prpTimeStruct->tm_hour);
		dbg(ipDebugFlag,"DebugPrintTm: Time->tm_mday  <%d>",prpTimeStruct->tm_mday);
		dbg(ipDebugFlag,"DebugPrintTm: Time->tm_mon   <%d>",prpTimeStruct->tm_mon);
		dbg(ipDebugFlag,"DebugPrintTm: Time->tm_year  <%d>",prpTimeStruct->tm_year);
		dbg(ipDebugFlag,"DebugPrintTm: Time->tm_wday  <%d>",prpTimeStruct->tm_wday);
		dbg(ipDebugFlag,"DebugPrintTm: Time->tm_yday  <%d>",prpTimeStruct->tm_yday);
		dbg(ipDebugFlag,"DebugPrintTm: Time->tm_isdst <%d>",prpTimeStruct->tm_isdst);

/*****
#if (! defined(_SOLARIS)) && (! defined(_SNI))

		dbg(ipDebugFlag,"DebugPrintTm: Time->tm_tzadj <%ld>",prpTimeStruct->tm_tzadj);
		dbg(ipDebugFlag,"DebugPrintTm: Time->tm_name  <%s>",prpTimeStruct->tm_name);
#endif
*****/
	}/* end of if */

	return(ilRC);

}/* end of DebugPrintTm */

/****************************************************************************'*/
/* function LocalTimeToUtcFixTZ: use current TZ setting to calculate UTC time */
/*                               from local time                              */
/* Parameter: pcpTime as IN and OUT. pcpTime must be greater than 14 chars!   */
/******************************************************************************/
void LocalTimeToUtcFixTZ(char *pcpTime)
{

  struct tm *prlTm;
  struct tm rlTm;
  int i = 0;
  time_t tlNow, tlNow2;
  size_t timesize;
  char clTmpString[124];
  char clTime[124];
  int ilIsDst = 0;
  int ilYear,ilMon,ilDay,ilHour,ilMin,ilSec;
  
  
  if ((pcpTime != NULL) && (pcpTime[0] != 0) && (pcpTime[0] != ' '))
  {

    tlNow2 = time(NULL);
    tlNow = tlNow2;
    prlTm = (struct tm *) localtime_r(&tlNow,&rlTm);

    tlNow = mktime(&rlTm);

    for (i=1; i<=2; i++)
    {
      strcpy(clTime,pcpTime);

      strncpy(clTmpString,clTime,4);
      clTmpString[4] = '\0';
      ilYear = atoi(clTmpString)-1900;

      strncpy(clTmpString,&clTime[4],2);
      clTmpString[2] = '\0';
      ilMon = atoi(clTmpString)-1;

      strncpy(clTmpString,&clTime[6],2);
      clTmpString[2] = '\0';
      ilDay = atoi(clTmpString);

      strncpy(clTmpString,&clTime[6],2);
      clTmpString[2] = '\0';
      ilDay = atoi(clTmpString);

      strncpy(clTmpString,&clTime[8],2);
      clTmpString[2] = '\0';
      ilHour = atoi(clTmpString);

      strncpy(clTmpString,&clTime[10],2);
      clTmpString[2] = '\0';
      ilMin = atoi(clTmpString);

      strncpy(clTmpString,&clTime[12],2);
      clTmpString[2] = '\0';
      ilSec = atoi(clTmpString);

/*      dbg(TRACE,"LocalTimeToUtcFixTZ: LocalTime: %2d.%2d.%d ,%2d:%2d:%2d)",
		       ilDay,ilMon+1,ilYear+1900,ilHour,ilMin,ilSec);
*/
      rlTm.tm_year = ilYear;
      rlTm.tm_mon  = ilMon;
      rlTm.tm_mday = ilDay;
      rlTm.tm_hour = ilHour;
      rlTm.tm_min = ilMin;
      rlTm.tm_sec = ilSec;

      ilIsDst = rlTm.tm_isdst;
      tlNow = mktime(&rlTm);
     
      if ((i == 1) && (ilIsDst != rlTm.tm_isdst) && (ilHour == 2))
      {
         rlTm.tm_isdst = 0;
      }
   
    } /* end of for*/


    prlTm = (struct tm *) localtime_r(&tlNow,&rlTm);
    /* dbg(TRACE,"LocalTimeToUtcFixTZ: LocalTime: <%s>",pcpTime); */

    prlTm = (struct tm *) gmtime_r(&tlNow,&rlTm);

    timesize = strftime(pcpTime, 256, "%Y%m%d%H%M%S", &rlTm);
    /* dbg(TRACE,"LocalTimeToUtcFixTZ: CEDA time: <%s>",pcpTime); */
    dbg(DEBUG,"L2U: <%s> ==> <%s>",clTime,pcpTime);
  }
}

/****************************************************************************'*/
/* function UtcToLocalTimeFixTZ: use current TZ setting to calculate local    */
/*                               time from UTC time                           */
/* Parameter: pcpTime as IN and OUT. pcpTime must be greater than 14 chars!   */
/******************************************************************************/
void UtcToLocalTimeFixTZ(char *pcpTime)
{

  struct tm *prlTm;
  struct tm rlTm;
  int i = 0;
  time_t tlNow, tlNow2;
  size_t timesize;
  char clTmpString[124];
  char clTime[124];
  int ilIsDst = 0;
  int ilYear,ilMon,ilDay,ilHour,ilMin,ilSec;
  
  if ((pcpTime != NULL) && (pcpTime[0] != 0) && (pcpTime[0] != ' '))
  {
    tlNow2 = time(NULL);
    tlNow = tlNow2;
    prlTm = (struct tm *) gmtime_r(&tlNow, &rlTm);

    strcpy(clTime,pcpTime);

    strncpy(clTmpString,clTime,4);
    clTmpString[4] = '\0';
    ilYear = atoi(clTmpString)-1900;

    strncpy(clTmpString,&clTime[4],2);
    clTmpString[2] = '\0';
    ilMon = atoi(clTmpString)-1;

    strncpy(clTmpString,&clTime[6],2);
    clTmpString[2] = '\0';
    ilDay = atoi(clTmpString);

    strncpy(clTmpString,&clTime[6],2);
    clTmpString[2] = '\0';
    ilDay = atoi(clTmpString);

    strncpy(clTmpString,&clTime[8],2);
    clTmpString[2] = '\0';
    ilHour = atoi(clTmpString);

    strncpy(clTmpString,&clTime[10],2);
    clTmpString[2] = '\0';
    ilMin = atoi(clTmpString);

    strncpy(clTmpString,&clTime[12],2);
    clTmpString[2] = '\0';
    ilSec = atoi(clTmpString);
    rlTm.tm_year = ilYear;
    rlTm.tm_mon  = ilMon;
    rlTm.tm_mday = ilDay;
    rlTm.tm_hour = ilHour;
    rlTm.tm_min = ilMin;
    rlTm.tm_sec = ilSec;

/*    printf("Eingabe: %2d.%2d.%d ,%2d:%2d:%2d    ----->    ",
	    ilDay,ilMon+1,ilYear+1900,ilHour,ilMin,ilSec);
*/
/*  dbg(TRACE,"UtcTimeToLocalFixTZ: CEDA time: <%s>",pcpTime); */

     tlNow = mktime(&rlTm);
     tlNow -= timezone;
     prlTm = (struct tm *) localtime_r(&tlNow, &rlTm);


    prlTm = (struct tm *) localtime_r(&tlNow,&rlTm);
    timesize = strftime(pcpTime, 256, "%Y%m%d%H%M%S", &rlTm);
/*  dbg(TRACE,"UtcTimeToLocalFixTZ: LocalTime: <%s>",pcpTime); */
    dbg(DEBUG,"U2L: <%s> ==> <%s>",clTime,pcpTime);
  }
}

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
