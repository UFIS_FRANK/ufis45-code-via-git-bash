
host clear 
host echo "----------------------------------------------------------------" 
host echo "Grant create/drop any view to ceda"
host echo "----------------------------------------------------------------" 
grant create any view to ceda;
grant drop any view to ceda;

host echo "----------------------------------------------------------------" 
host echo "Creating user pe/pe with select privileges for pe-views"
host echo "----------------------------------------------------------------" 
drop user pe cascade;
create user pe identified by pe profile default;
alter user pe default tablespace temp temporary tablespace temp;
revoke select any table from pe;
grant create session to pe;
commit;

host echo "----------------------------------------------------------------" 
host echo "Creating \"create view\" procedure"
host echo "----------------------------------------------------------------" 
CREATE OR REPLACE  PROCEDURE "CEDA"."PE_CREATE_VIEW" (group_name 
    IN char)
as pragma AUTONOMOUS_TRANSACTION;
v_string varchar(32000) := ' ';
v_tablecheck number := 0;
begin
  v_tablecheck := pe_check_name(rtrim(group_name,' '));
  --dbms_output.put_line('PRO: NAME-CHECK: <'||v_tablecheck||'>');
  if v_tablecheck = 0 then
    --dbms_output.put_line('PRO:<'||group_name||'>');
    v_string := pe_get_create_view_string(rtrim(group_name,' '));
    --dbms_output.put_line('PRO:<'||rtrim(v_string,' ')||'>');
    execute immediate 'create or replace view '||rtrim(group_name,' ')||'
    as select '||v_string||' from pedtab';
    execute immediate 'grant select on '||group_name||' to pe';
    COMMIT;
  else
    --dbms_output.put_line('PRO-ERR: '||rtrim(group_name,' ')||' exists in CEDA-schema! Cant create Pageeditor-view!');
    ROLLBACK;
  end if;
EXCEPTION
 WHEN others THEN
 ROLLBACK;
END pe_create_view;
/

host echo "----------------------------------------------------------------" 
host echo "Creating \"delete view\" procedure"
host echo "----------------------------------------------------------------" 
CREATE OR REPLACE  PROCEDURE "CEDA"."PE_DEL_VIEW"             (
    old_gnam in char, new_gnam in char)
as pragma AUTONOMOUS_TRANSACTION;
v_count NUMBER;
begin
     --dbms_output.put_line('PRO:<'||rtrim(old_gnam,' ')||'>');
     select count(*) into v_count from pedtab where pedtab.gnam = old_gnam;
     --dbms_output.put_line('PRO:<Count:'||v_count||'>');
     if v_count <= 1 and old_gnam <> new_gnam then
           execute immediate 'drop view '||rtrim(old_gnam,' ');
           COMMIT;
     end if;
EXCEPTION
  WHEN others THEN
  ROLLBACK;
end pe_del_view;
/

host echo "----------------------------------------------------------------" 
host echo "Creating \"get special string\" function for Pageeditor"
host echo "----------------------------------------------------------------" 
CREATE OR REPLACE  FUNCTION "CEDA"."PE_GET_CREATE_VIEW_STRING"                             (
    v_gnam in pedtab.gnam%TYPE)
RETURN char as v_result varchar2(32000);
-- Variable declaration
v_count NUMBER;
v_snam char(40) := ' ';
i NUMBER := 0;
CURSOR pedtab_curs (p_snam in pedtab.snam%TYPE) IS SELECT snam
  FROM pedtab WHERE pedtab.gnam = v_gnam order by pedtab.snam;

BEGIN
  --dbms_output.put_line('FUN:'||v_gnam);
  open pedtab_curs (v_gnam);
  select count(*) into v_count from pedtab where pedtab.gnam = v_gnam;
  --dbms_output.put_line('FUN: Count group <'||v_gnam||'>='||v_count);
  i := 1;
  while i <= v_count
    loop
      fetch pedtab_curs into v_snam;
      --dbms_output.put_line('Symbolic name='||v_snam);
      IF pedtab_curs%NOTFOUND THEN
			   v_snam := ' ';
  	  END IF;
	    EXIT WHEN pedtab_curs%NOTFOUND;
	    if i < v_count then
  	     v_result := ' '||RTRIM(v_result,' ')||' '||'fake '||RTRIM(v_snam,' ')||',';
    	else
  	     v_result := ' '||RTRIM(v_result,' ')||' '||'fake '||RTRIM(v_snam,' ');
    	end if;
  	  i := i + 1;
  	end loop;
	close pedtab_curs;
--dbms_output.put_line('FUN: result=<'||v_result||'>');
  v_result := RTRIM(v_result,' ');
  v_result := LTRIM(v_result,' ');
  RETURN (v_result);
exception
  when others then
	return ' ';
END;
/

host echo "----------------------------------------------------------------" 
host echo "Creating \"check group-name validity \" function for Pageeditor"
host echo "----------------------------------------------------------------" 
CREATE OR REPLACE  FUNCTION "CEDA"."PE_CHECK_NAME"         (
    v_gnam in pedtab.gnam%TYPE) 
RETURN number as v_nr number;

-- Variable declaration
v_count NUMBER := -1;
CURSOR nr_curs (p_count in user_tables.table_name%TYPE) IS SELECT count(*) 
  FROM user_tables WHERE user_tables.table_name = v_gnam;
BEGIN
  v_nr := -1;
  --dbms_output.put_line('FUN: Check Table: <'||v_gnam||'>');
  if v_gnam <> 'FLVTAB' and v_gnam <> 'FDVTAB' then
    open nr_curs (v_gnam);
    select count(*) into v_count from user_tables where user_tables.table_name = v_gnam;
    --dbms_output.put_line('FUN: Count from user_tables: <'||v_gnam||'>='||v_count);
    if v_count <= 0 then
      --dbms_output.put_line('OK!');
      v_nr := 0;
    --else
     -- dbms_output.put_line('NOT OK!');
    end if;
    close nr_curs;
   end if;
   v_nr := v_count;
  RETURN (v_nr);
exception
  when others then
  rollback;
	return -1;
END;
/

host echo "----------------------------------------------------------------" 
host echo "Creating \"create view\" trigger"
host echo "----------------------------------------------------------------" 
CREATE OR REPLACE TRIGGER "CEDA"."PE_CALL_CREATE_VIEW" AFTER 
    DELETE OR 
    UPDATE OF "FAKE", "GNAM" 
    ON "PEDTAB" 
    FOR EACH ROW BEGIN 
    PE_CREATE_VIEW(:old.gnam);
END;
/

host echo "----------------------------------------------------------------" 
host echo "Creating \"delete view\" trigger"
host echo "----------------------------------------------------------------" 
CREATE OR REPLACE TRIGGER "CEDA"."PE_CALL_DEL_VIEWS" AFTER 
    DELETE OR 
    UPDATE OF "GNAM" 
    ON "PEDTAB" 
    FOR EACH ROW BEGIN 
    --dbms_output.put_line('TRI:<'||rtrim(:old.gnam,' ')||'>');
   PE_DEL_VIEW(:old.gnam,:new.gnam);
END;
/

host echo "----------------------------------------------------------------" 
host echo "Creating all Pageeditor-views according to PEDTAB!"
host echo "----------------------------------------------------------------" 
update pedtab set fake = ' ';
commit;
exit;
