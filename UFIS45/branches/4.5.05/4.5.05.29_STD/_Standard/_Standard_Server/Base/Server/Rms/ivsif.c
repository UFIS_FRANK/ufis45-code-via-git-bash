#ifndef _DEF_mks_version
  #define _DEF_mks_version
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version[] = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Rms/ivsif.c 1.1 2003/03/19 16:55:27SGT fei Exp  $";
#endif /* _DEF_mks_version */
/******************************************************************************/
/*                                                                            */
/* ABB ACE/FC Program Skeleton                                                */
/*                                                                            */
/* Author         : Eric Theessen eth                                         */
/* Date           : 12.02.2001                                                */
/* Description    : Interaktives Voice System Interface                       */
/*                                                                            */
/* Update history : eth 12.01.2001 start creation                             */
/*                  eth 19.04.2001 changed behavior in child function         */
/*                                 added config entry retry Init_ivsif        */
/*                                 added igRetry for Event timeout            */
/*                                                                            */
/*                  eth 29.05.2001 changed child behavior                     */
/*                                 always fork and die fast                   */
/*                                 no communication between parent and child  */
/*                                                                            */
/*                                                                            */
/******************************************************************************/
/*                                                                            */
/* source-code-control-system version string                                  */
static char sccs_version[] ="%Z% UFIS44 (c) ABB AAT/I %M% %I% / %E% %U% / VBL";
/* be carefule with strftime or similar functions !!!                         */
/*                                                                            */
/******************************************************************************/
/* This program is a MIKE main program */
#define U_MAIN
#define UGCCS_PRG
#define STH_USE
#include <stdio.h>
#include <malloc.h>
#include <errno.h>
#include <signal.h>
#include <sys/socket.h> /* by eth */
#include <sys/select.h> /* by eth */
#include <sys/types.h>  /* by eth */
#include <sys/wait.h>   /* by eth */
#include <sys/time.h>   /* by eth */
#include <unistd.h>     /* by eth */
#include "ugccsma.h"
#include "msgno.h"
#include "glbdef.h"
#include "quedef.h"
#include "uevent.h"
#include "sthdef.h"
#include "debugrec.h"
#include "hsbsub.h"

#include "ivsif.h"
/******************************************************************************/
/* External variables                                                         */
/******************************************************************************/
/* outp is defined in ugccsma.h! double definitions kill */
/* your process under HP-UX immediatly !!!!              */
/*FILE *outp       = NULL;*/
int  debug_level = DEBUG;

/******************************************************************************/
/* Global variables                                                           */
/******************************************************************************/
static ITEM  *prgItem      = NULL;              /* The queue item pointer     */
static EVENT *prgEvent     = NULL;              /* The event pointer          */
static EVENT *prgOutEvent  = NULL;              /* The Output Event for child */
static int   igItemLen     = 0;                 /* length of incoming item    */
static int   igInitOK      = FALSE;
static char  cgConfigFile[512];

static int igQueId;

static int    igRetry;  /* count of retrys to get event from ivshdl */

static char   pcgService[8]; /* the VRU service socket from config-file*/
static char   pcgHomePort[8]; /* the Home Airport from config-file*/

static int   listenfd = 0; /* the listen-filedescriptor */

/******************************************************************************/
/* Function prototypes	                                                      */
/******************************************************************************/
static int	Init_ivsif();			/* Initialize the program */
static int	Reset(void);			/* Reset program          */
static void	Terminate(void);		/* Terminate program      */
static void	HandleSignal(int);		/* Handles signals        */
static void	HandleErr(int);			/* Handles general errors */
static void	HandleQueErr(int);		/* Handles queuing errors */
static int	HandleData(void);		/* Handles event data     */
static void	HandleQueues(void);		/* Waiting for Sts.-switch*/

static int listen_to_port(int listenfd);
static int child(int listenfd);

static int WaitAndCheckQueue(int ipTimeout, int ipModId, ITEM **prpItem);
static int CheckQueue(int ipModId, ITEM **prpItem);



/******************************************************************************/
/*                                                                            */
/* The MAIN program                                                           */
/*                                                                            */
/******************************************************************************/
MAIN
{
  int  ilRC = RC_SUCCESS;  /* Return code */
  int  ilCnt = 0; /* counter */
  int  ilCount = 0; /* counter */

  int maxfdp1; /* max count of file-descriptors plus one */
  fd_set rset; /* the read set of descriptors */
  struct timeval connection; /* timeout structure for select */

	INITIALIZE;  /* General initialization */

	/* all Signals */
	(void)SetSignals(HandleSignal);


	dbg(TRACE,"MAIN: version <%s>",sccs_version);
	/* Attach to the MIKE queues */
	do
	{
		ilRC = init_que();
		if(ilRC != RC_SUCCESS)
		{
			dbg(TRACE,"MAIN: init_que() failed! waiting 6 sec ...");
			sleep(6);
			ilCnt++;
		}/* end of if */
	}while((ilCnt < 10) && (ilRC != RC_SUCCESS));

	if(ilRC != RC_SUCCESS)
	{
		dbg(TRACE,"MAIN: init_que() failed! waiting 60 sec ...");
		sleep(60);
		exit(1);
	}
        else
        {
	  dbg(TRACE,"MAIN: init_que() OK! mod_id <%d>",mod_id);
	}/* end of if */

	sprintf(cgConfigFile,"%s/ivsif",getenv("BIN_PATH"));
	ilRC = TransferFile(cgConfigFile);
	if(ilRC != RC_SUCCESS)
	{
	  dbg(TRACE,"MAIN: TransferFile(%s) failed!",cgConfigFile);
	} /* end of if */

	/* uncomment if necessary */
	/* sprintf(cgConfigFile,"%s/ivsif.cfg",getenv("CFG_PATH")); */
	/* ilRC = TransferFile(cgConfigFile); */
	/* if(ilRC != RC_SUCCESS) */
	/* { */
	/*   dbg(TRACE,"MAIN: TransferFile(%s) failed!",cgConfigFile); */
	/* } */ /* end of if */

	ilRC = SendRemoteShutdown(mod_id);
	if(ilRC != RC_SUCCESS)
	{
	  dbg(TRACE,"MAIN: SendRemoteShutdown(%d) failed!",mod_id);
	} /* end of if */

	if((ctrl_sta != HSB_STANDALONE) &&
           (ctrl_sta != HSB_ACTIVE) &&
           (ctrl_sta != HSB_ACT_TO_SBY))
	{
		dbg(DEBUG,"MAIN: waiting for status switch ...");
		dbg(DEBUG,"MAIN: calling HandleQueues ...");
		HandleQueues();
	}/* end of if */

	if((ctrl_sta == HSB_STANDALONE) ||
           (ctrl_sta == HSB_ACTIVE) ||
           (ctrl_sta == HSB_ACT_TO_SBY))
	{
		dbg(TRACE,"MAIN: initializing ...");
		if(igInitOK == FALSE)
		{
			ilRC = Init_ivsif();
			if(ilRC != RC_SUCCESS)
			{
				dbg(TRACE,"Init_ivsif: init failed!");
			} /* end of if */
		}/* end of if */
	}
    else
    {
	  Terminate();
	}/* end of if */


    /* den listenfd erzeugen */
    dbg(DEBUG,"MAIN: calling tcp_create_socket");
    listenfd = tcp_create_socket( SOCK_STREAM, &pcgService);
    if (listenfd != RC_FAIL)
    {
      ilRC = listen( listenfd, 5); /* maximal 5 zusaetzliche Anfrage */
      if(ilRC != 0)
      {
        /* ERROR */
        dbg(TRACE,"MAIN: problem with listen to listenfd");
      }
      else
      {
        /* alles klar */
        dbg(DEBUG,"MAIN: listen to listenfd OK");
      }
    }
    else
    {
      dbg(TRACE,"MAIN: can't get listenfd ... terminating");
      Terminate();
    }

	dbg(TRACE,"MAIN: initializing OK");


	for(;;)
	{
        FD_ZERO(&rset);
        FD_SET(listenfd, &rset);
        maxfdp1 = 0;
        maxfdp1 = listenfd + 1;

        connection.tv_sec = 2;
        connection.tv_usec = 0;

        /* hier kommt der select auf listenfd */
        ilRC = select (maxfdp1, &rset, NULL, NULL, &connection);
        if ((ilRC > 0) && (FD_ISSET(listenfd, &rset)))
        {
	      dbg(DEBUG,"MAIN: calling listen_to_port");
		  ilRC = listen_to_port(listenfd);
          if( ilRC != RC_SUCCESS)
          {
             dbg(DEBUG,"MAIN: ERROR no more free childs ... terminate");
            Terminate();
          }
        }
        else
        {
	      dbg(DEBUG,"MAIN: someting wrong with the socket connection");
        }

		ilRC = que(QUE_GETBIGNW,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);
		/* depending on the size of the received item  */
		/* a realloc could be made by the que function */
		/* so do never forget to set event pointer !!! */
		prgEvent = (EVENT *) prgItem->text;
				
		if( ilRC == RC_SUCCESS )
		{
			/* Acknowledge the item */
			ilRC = que(QUE_ACK,0,mod_id,0,0,NULL);
			if( ilRC != RC_SUCCESS ) 
			{
			  /* handle que_ack error */
			  HandleQueErr(ilRC);
			}
			
			switch( prgEvent->command )
			{
			case	HSB_STANDBY	:
				ctrl_sta = prgEvent->command;
				HandleQueues();
				break;	
			case	HSB_COMING_UP	:
				ctrl_sta = prgEvent->command;
				HandleQueues();
				break;	
			case	HSB_ACTIVE	:
				ctrl_sta = prgEvent->command;
				break;	
			case	HSB_ACT_TO_SBY	:
				ctrl_sta = prgEvent->command;
				/* CloseConnection(); */
				HandleQueues();
				break;	
			case	HSB_DOWN	:
				/* whole system shutdown - */
                /*   do not further use que(), send_message() or timsch() ! */
				ctrl_sta = prgEvent->command;
				Terminate();
				break;	
			case	HSB_STANDALONE	:
				ctrl_sta = prgEvent->command;

				break;	

			case	SHUTDOWN	:
				/* process shutdown - maybe from uutil */
				Terminate();
				break;
					
			case	RESET		:
				ilRC = Reset();
				break;
					
			case	EVENT_DATA	:
				if((ctrl_sta == HSB_STANDALONE) ||
                   (ctrl_sta == HSB_ACTIVE) ||
                   (ctrl_sta == HSB_ACT_TO_SBY))
				{
				  ilRC = HandleData();
				  if(ilRC != RC_SUCCESS)
				  {
				    HandleErr(ilRC);
				  }/* end of if */
				}
                else
                {
				  dbg(TRACE,"MAIN: wrong hsb status <%d>",ctrl_sta);
				  DebugPrintItem(TRACE,prgItem);
				  DebugPrintEvent(TRACE,prgEvent);
				}
				break;
					
			case	TRACE_ON :
				dbg_handle_debug(prgEvent->command);
				break;
			case	TRACE_OFF :
				dbg_handle_debug(prgEvent->command);
				break;

			default:
				dbg(TRACE,"MAIN: unknown event");
				DebugPrintItem(TRACE,prgItem);
				DebugPrintEvent(TRACE,prgEvent);
				break;
			} /* end switch */

		} /* que returns RC_SUCCESS */

        else if(ilRC == QUE_E_NOMSG)
             {
              /* because QUE_GETBIGNW ... nowait */
              /* dbg(DEBUG,"MAIN: no message on queue\n"); */
             }
             else
             {
		       /* Handle queuing errors */
		       HandleQueErr(ilRC);
		     } /* end else */
		
	} /* end for-ever */

	/* never reached */
	/* exit(0); */
	
} /* end of MAIN */



/******************************************************************************/
/* The initialization routine                                                 */
/******************************************************************************/
static int Init_ivsif()
{
  int ilRC = RC_SUCCESS;  /* Return code */
  int ilCount = 0;  /* counter */
  int ilRetry = 0;  /* retry counter */

  char  *pclCfgPath = NULL;
  char  pclCfgFile[iMIN_BUF_SIZE];
  char  pclTmpBuf[iMAX_BUF_SIZE];

  /* now reading from configfile or from database */

  dbg(DEBUG,"<Init_ivsif> ---- START INIT ----");
 
  /* to inform the status-handler about startup time */
  if ((ilRC = SendStatus("FILEIO", "ivsif", "START", 1, "", "", ""))
            != RC_SUCCESS)
  {
    dbg(TRACE,"<Init_ivsif> %05d SendStatus returns: %d", __LINE__, ilRC);
    dbg(DEBUG,"<Init_ivsif> ----- END -----");
    return RC_FAIL;
  }
 
  if ((pclCfgPath = getenv("CFG_PATH")) == NULL)
  {
    dbg(TRACE,"<Init_ivsif> ERROR: missing environment CFG_PATH...");
    dbg(DEBUG,"<Init_ivsif> ---- END ----");
    return RC_FAIL;
  }
 
  strcpy(pclCfgFile, pclCfgPath);
 
  if (pclCfgFile[strlen(pclCfgFile)-1] != '/')
    strcat(pclCfgFile, "/");
 
  strcat(pclCfgFile, mod_name);
  strcat(pclCfgFile, ".cfg");
  dbg(DEBUG,"<Init_ivsif> CFG-File: <%s>", pclCfgFile);

/* merken */

/* debug_level -------------------------------------------------- */
  pclTmpBuf[0] = 0x00;
  if ((ilRC = iGetConfigEntry(pclCfgFile,
                              "MAIN",
                              "debug_level",
                              CFG_STRING,
                              pclTmpBuf))
            != RC_SUCCESS)
  {
    /* default */
    dbg(DEBUG,"<Init_ivsif> no debug_level in section <MAIN>");
    dbg(DEBUG,"<Init_ivsif> setting debug_level to TRACE");
    strcpy(pclTmpBuf, "TRACE");
  }

  /* which debug_level is set in the Configfile ? */
  StringUPR((UCHAR*)pclTmpBuf);
  if (!strcmp(pclTmpBuf,"DEBUG"))
  {
    debug_level = DEBUG;
    dbg(TRACE,"<Init_ivsif> debug_level set to DEBUG");
  }
  else if (!strcmp(pclTmpBuf,"TRACE"))
       {
         debug_level = TRACE;
         dbg(TRACE,"<Init_ivsif> debug_level set to TRACE");
       }
       else if (!strcmp(pclTmpBuf,"OFF"))
            {
              debug_level = 0;
              dbg(TRACE,"<Init_ivsif> debug_level set to OFF");
            }
            else
            {
              debug_level = 0;
              dbg(TRACE,"<Init_ivsif> unknown debug_level set to default OFF");
            }


/* Home Airport ------------------------------------------------- */
  pclTmpBuf[0] = 0x00;
  if ((ilRC = iGetConfigEntry(pclCfgFile,
                              "MAIN",
                              "homeport",
                              CFG_STRING,
                              pclTmpBuf))
            != RC_SUCCESS)
  {
    /* default */
    dbg(TRACE,"<Init_ivsif> no homeport in section <MAIN>");
    dbg(TRACE,"<Init_ivsif> setting homeport to ZRH");
    strcpy(pclTmpBuf, "ZRH");
    sprintf(pcgHomePort, "%s", pclTmpBuf);
  }
  else
  {
    sprintf(pcgHomePort, "%s", pclTmpBuf);
    dbg(TRACE,"<Init_ivsif> homeport set to %s", pcgHomePort);
  }


/* VRU service socket ------------------------------------------- */
  pclTmpBuf[0] = 0x00;
  if ((ilRC = iGetConfigEntry(pclCfgFile,
                              "MAIN",
                              "service",
                              CFG_STRING,
                              pclTmpBuf))
            != RC_SUCCESS)
  {
    /* default */
    dbg(TRACE,"<Init_ivsif> no service in section <MAIN>");
    dbg(TRACE,"<Init_ivsif> setting service to UFIS_IVS");
    strcpy(pclTmpBuf, "UFIS_IVS");
    sprintf(pcgService, "%s", pclTmpBuf);
  }
  else
  {
    sprintf(pcgService, "%s", pclTmpBuf);
    dbg(TRACE,"<Init_ivsif> service set to %s", pcgService);
  }


/* send to ------------------------------------------------------ */
  pclTmpBuf[0] = 0x00;
  if ((ilRC = iGetConfigEntry(pclCfgFile,
                              "MAIN",
                              "sendto",
                              CFG_STRING,
                              pclTmpBuf))
            != RC_SUCCESS)
  {
    /* default */
    dbg(TRACE,"<Init_ivsif> no sendto in section <MAIN>");
    dbg(TRACE,"<Init_ivsif> setting sendto to ivshdl");
    igQueId = tool_get_q_id("ivshdl");
  }
  else
  {
    dbg(TRACE,"<Init_ivsif> sendto set to %s", pclTmpBuf);
    igQueId = tool_get_q_id(pclTmpBuf);
  }

  dbg(DEBUG,"<Init_ivsif> Queue ID = %d", igQueId);


/* count of retrys ---------------------------------------------- */
  if ((ilRC = iGetConfigEntry(pclCfgFile,
                              "MAIN",
                              "retry",
                              CFG_INT,
                              (char*)&ilRetry))
            != RC_SUCCESS)
  {
    dbg(TRACE,"<Init_ivsif> missing retry in section <MAIN>");
    dbg(TRACE,"<Init_ivsif> set to default 30");
    igRetry = 30;
  }
  else
  {
    igRetry = ilRetry;
    dbg(TRACE,"<Init_ivsif> retry = %d",igRetry);
  }


  igInitOK = TRUE;

  return(RC_SUCCESS);

} /* end of initialize */



/******************************************************************************/
/* The Reset routine                                                          */
/******************************************************************************/
static int Reset()
{
  int  ilRC = RC_SUCCESS;  /* Return code */

  dbg(DEBUG,"Reset: calling Init_ivsif");
	
  ilRC = Init_ivsif();
  if(ilRC != RC_SUCCESS)
  {
    dbg(TRACE,"Reset: Init_ivsif failed!");
  }

  return ilRC;
	
} /* end of Reset */



/******************************************************************************/
/* The termination routine                                                    */
/******************************************************************************/
static void Terminate()
{
  int ilRC = RC_SUCCESS; /* return value ... doch wohin return-en??? */
  int ilCount = 0; /* counter */

  dbg(DEBUG,"\n Terminate was called ...");

  /* dbg(DEBUG,"Terminate: wait 30 sec to finish childs work"); */

  /* sleep(30); */

  sleep(5);

  dbg(TRACE,"Terminate: now leaving ...");

  exit(0);
	
} /* end of Terminate */



/******************************************************************************/
/* The handle signals routine                                                 */
/******************************************************************************/
static void HandleSignal(int pipSig)
{
  int  ilRC = RC_SUCCESS;  /* Return code */
  int  ilCount = 0;        /* counter */
  int  ilCpid;             /* the child pid */

  dbg(DEBUG,"HandleSignal: signal <%d> received",pipSig);

  /* see: /usr/include/sys/signal.h */

  switch(pipSig)
  {
    case SIGCHLD:

      dbg(DEBUG,"HandleSignal: reveived SIGCLD ...");
      ilCpid = wait(0);
      dbg(DEBUG,"HandleSignal: child %05d is dead now",ilCpid);
    break;

    case SIGPIPE:
      dbg(TRACE,"HandleSignal: reveived SIGPIPE ...");
      dbg(TRACE,"HandleSignal: write on a pipe with no one to read it\n");
      /* #define SIGPIPE 13     **  write on a pipe with no one to read it */
    break;

    default:
      Terminate();
    break;

  } /* end of switch */

} /* end of HandleSignal */



/******************************************************************************/
/* The handle general error routine                                           */
/******************************************************************************/
static void HandleErr(int pipErr)
{
	int	ilRC = RC_SUCCESS;
	return;
} /* end of HandleErr */



/******************************************************************************/
/* The handle queuing error routine                                           */
/******************************************************************************/
static void HandleQueErr(int pipErr)
{
	int	ilRC = RC_SUCCESS;
	
	switch(pipErr) {
	case	QUE_E_FUNC	:	/* Unknown function */
		dbg(TRACE,"<%d> : unknown function",pipErr);
		break;
	case	QUE_E_MEMORY	:	/* Malloc reports no memory */
		dbg(TRACE,"<%d> : malloc failed",pipErr);
		break;
	case	QUE_E_SEND	:	/* Error using msgsnd */
		dbg(TRACE,"<%d> : msgsnd failed",pipErr);
		break;
	case	QUE_E_GET	:	/* Error using msgrcv */
		dbg(TRACE,"<%d> : msgrcv failed",pipErr);
		break;
	case	QUE_E_EXISTS	:
		dbg(TRACE,"<%d> : route/queue already exists ",pipErr);
		break;
	case	QUE_E_NOFIND	:
		dbg(TRACE,"<%d> : route not found ",pipErr);
		break;
	case	QUE_E_ACKUNEX	:
		dbg(TRACE,"<%d> : unexpected ack received ",pipErr);
		break;
	case	QUE_E_STATUS	:
		dbg(TRACE,"<%d> :  unknown queue status ",pipErr);
		break;
	case	QUE_E_INACTIVE	:
		dbg(TRACE,"<%d> : queue is inaktive ",pipErr);
		break;
	case	QUE_E_MISACK	:
		dbg(TRACE,"<%d> : missing ack ",pipErr);
		break;
	case	QUE_E_NOQUEUES	:
		dbg(TRACE,"<%d> : queue does not exist",pipErr);
		break;
	case	QUE_E_RESP	:	/* No response on CREATE */
		dbg(TRACE,"<%d> : no response on create",pipErr);
		break;
	case	QUE_E_FULL	:
		dbg(TRACE,"<%d> : too many route destinations",pipErr);
		break;
	case	QUE_E_NOMSG	:	/* No message on queue */
		dbg(TRACE,"<%d> : no messages on queue",pipErr);
		break;
	case	QUE_E_INVORG	:	/* Mod id by que call is 0 */
		dbg(TRACE,"<%d> : invalid originator=0",pipErr);
		break;
	case	QUE_E_NOINIT	:	/* Queues is not initialized*/
		dbg(TRACE,"<%d> : queues are not initialized",pipErr);
		break;
	case	QUE_E_ITOBIG	:
		dbg(TRACE,"<%d> : requestet itemsize to big ",pipErr);
		break;
	case	QUE_E_BUFSIZ	:
		dbg(TRACE,"<%d> : receive buffer to small ",pipErr);
		break;
	default			:	/* Unknown queue error */
		dbg(TRACE,"<%d> : unknown error",pipErr);
		break;
	} /* end switch */
         
	return;
} /* end of HandleQueErr */



/******************************************************************************/
/* The handle queues routine                                                  */
/******************************************************************************/
static void HandleQueues()
{
	int	ilRC = RC_SUCCESS;			/* Return code */
	int	ilBreakOut = FALSE;
	
	do
	{
		ilRC = que(QUE_GETBIG,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);
		/* depending on the size of the received item  */
		/* a realloc could be made by the que function */
		/* so do never forget to set event pointer !!! */
		prgEvent = (EVENT *) prgItem->text;	

		if( ilRC == RC_SUCCESS )
		{
			/* Acknowledge the item */
			ilRC = que(QUE_ACK,0,mod_id,0,0,NULL);
			if( ilRC != RC_SUCCESS ) 
			{
				/* handle que_ack error */
				HandleQueErr(ilRC);
			}
		
			switch( prgEvent->command )
			{
			case	HSB_STANDBY	:
				ctrl_sta = prgEvent->command;
				break;	
			case	HSB_COMING_UP	:
				ctrl_sta = prgEvent->command;
				break;	
			case	HSB_ACTIVE	:
				ctrl_sta = prgEvent->command;
				ilBreakOut = TRUE;
				break;	
			case	HSB_ACT_TO_SBY	:
				ctrl_sta = prgEvent->command;
				break;	
			case	HSB_DOWN	:
				/* whole system shutdown - */
                                /* do not further use que(), send_message() or timsch() ! */
				ctrl_sta = prgEvent->command;
				Terminate();
				break;	
			case	HSB_STANDALONE	:
				ctrl_sta = prgEvent->command;
				ilBreakOut = TRUE;
				break;	
			case	SHUTDOWN	:
				Terminate();
				break;
			case	RESET		:
				ilRC = Reset();
				break;
			case	EVENT_DATA	:
				dbg(TRACE,"HandleQueues: wrong hsb status <%d>",ctrl_sta);
				DebugPrintItem(TRACE,prgItem);
				DebugPrintEvent(TRACE,prgEvent);
				break;
			case	TRACE_ON :
				dbg_handle_debug(prgEvent->command);
				break;
			case	TRACE_OFF :
				dbg_handle_debug(prgEvent->command);
				break;
			default			:
				dbg(TRACE,"HandleQueues: unknown event");
				DebugPrintItem(TRACE,prgItem);
				DebugPrintEvent(TRACE,prgEvent);
				break;
			} /* end switch */
		}
        	else
        	{
          	  /* Handle queuing errors */
          	  HandleQueErr(ilRC);
		} /* end else */

	} while (ilBreakOut == FALSE);

	if(igInitOK == FALSE)
	{
		ilRC = Init_ivsif();
		if(ilRC != RC_SUCCESS)
		{
			dbg(TRACE,"HandleQueues: init failed!");
		} /* end of if */
	}/* end of if */

} /* end of HandleQueues */



/******************************************************************************/
/* The handle data routine                                                    */
/******************************************************************************/
static int HandleData()
{
  int    ilRC  = RC_SUCCESS;  /* Return code */

  return ilRC;
	
} /* end of HandleData */



/******************************************************************************/
/* listen_to_port */
/* by eth 12.02.2001                                                          */
/******************************************************************************/
static int listen_to_port(int listenfd)
{
  char *pclFct = "listen_to_port";
  int    ilRC  = RC_SUCCESS;
  pid_t    childpid;


  dbg(DEBUG,"Now in %s: starting new child",pclFct);

  /* neues child erzeugen ... fork */
  if ((childpid = fork()) == 0)
  {
    /* ############ child process ############ */
    dbg(TRACE,"Child: new Child has been started");

    /* now call the child function */
    /* dbg(DEBUG,"Child: calling child function"); */
    ilRC = child(listenfd);
 
    /* dbg(DEBUG,"Child: Child is back ... exiting", pclFct); */
    exit(0); /* end child */
  }


  /* ############ parent process ############ */

  dbg(DEBUG,"%s: new childpid = %d",pclFct,childpid);

  return ilRC; /* wichtig fuer Fehlererkennung */

} /* end of listen_to_port */



/******************************************************************************/
/* The child routine */
/******************************************************************************/
static int child(int listenfd)
{
  char *pclFct = "child";    /* the function name */
  int    ilRC = RC_SUCCESS;  /* Return code */
  int    ilCpid;             /* the child pid */

/* from polhdl.c TriggerAction */
  EVENT        *prlEvent      = NULL;
  BC_HEAD      *prlBchead     = NULL;
  CMDBLK       *prlCmdblk     = NULL;
  char         *pclSelection  = NULL;
  char         *pclFields     = NULL;
  char         *pclData       = NULL;
  long          llActSize     = 0;
  int           ilAnswerQueue = 0;

  char pclQueueName[MSGSTRLEN]; /* the queue name */
  char pclTempName[MSGSTRLEN];  /* for the queue name */
  char pclDummy[MSGSTRLEN];     /* for hopo name */

  char pclRvrumsg[VRUMSGLEN]; /* message string from VRU */
  char pclWvrumsg[VRUMSGLEN]; /* message string to VRU */

  int connfd; /* connection filedescriptor */


  ilCpid = getpid();
  dbg(DEBUG," %s %05d is now running", pclFct, ilCpid);

  /* nur die ersten drei buchstaben */
  strcpy(pclTempName, mod_name);
  pclTempName[3] = '\0';

/* temp Queue einrichten */
  sprintf (&pclQueueName[0],"%s%05d", pclTempName, ilCpid) ;
 
  ilRC = GetDynamicQueue( &ilAnswerQueue, &pclQueueName[0]);
  if (ilRC != RC_SUCCESS)
  {
    dbg(TRACE,"%s %05d: GetDynamicQueue failed <%d>", pclFct, ilCpid, ilRC);
    return(RC_FAIL);
  }
  else /* got a new temp queue */
  {
    dbg(DEBUG,"%s %05d: got queue <%d> for child <%s>",
         pclFct,ilCpid,ilAnswerQueue,&pclQueueName[0]);

    dbg(TRACE,"%s %05d: start job\n\n", pclFct, ilCpid);

    /* accept the request from VRU */
    connfd = accept(listenfd, NULL, NULL);
    dbg(DEBUG,"%s %05d: accept listenfd ... connfd = %d", pclFct, ilCpid, connfd);

    /* clear pclRvrumsg */
    memset(pclRvrumsg, 0x00, VRUMSGLEN);

    dbg(DEBUG,"%s %05d: now reading socket", pclFct, ilCpid);
    ilRC = read(connfd, pclRvrumsg, sizeof(pclRvrumsg));
/* Baustelle ... Fehlerpruefung */
/*  
          if (ilRC < 0)  dbg(DEBUG,"%s %05d: read returns ERROR", pclFct, ilCpid);
          if (ilRC == 0) dbg(DEBUG,"%s %05d: read returns EOF", pclFct, ilCpid);
          if (ilRC > 0)  dbg(DEBUG,"%s %05d: read returns %d bytes", pclFct, ilCpid, ilRC);
*/ 
    dbg(DEBUG,"%s %05d: received  %s  from VRU", pclFct, ilCpid, pclRvrumsg);

    sprintf(pclDummy, "TAB,%s,ivsif", pcgHomePort);

    dbg(DEBUG,"%s %05d: now calling SendCedaEvent ...", pclFct, ilCpid);

    ilRC = SendCedaEvent ( igQueId,       /* adress to send the answer   */
                           ilAnswerQueue, /* Set to mod_id if < 1        */
                           "ivshdl",      /* BC_HEAD.dest_name           */
                           "ivsif\0",     /* BC_HEAD.recv_name           */
                           " ",           /* CMDBLK.tw_start             */
                           pclDummy,      /* CMDBLK.tw_end               */
                           "IVS",         /* CMDBLK.command              */
                           "",            /* CMDBLK.obj_name             */
                           "",            /* Selection Block             */
                           "",            /* Field Block                 */
                           pclRvrumsg,    /* Data Block                  */
                           "",            /* Error description           */
                           3,             /* 0 or Priority               */
                           RC_SUCCESS);   /* BC_HEAD.rc: RC_SUCCESS or   */
                                          /*             RC_FAIL or      */
                                          /*             NETOUT_NO_ACK   */

    /* warten auf Antwort vom ivshdl */
    ilRC = WaitAndCheckQueue( igRetry, ilAnswerQueue, &prgItem);
    if(ilRC != RC_SUCCESS)
    {
      dbg(TRACE,"%s %05d: WaitAndCheckQueue failed <%d>", pclFct, ilCpid, ilRC);

      /* close connection to VRU */
      dbg(DEBUG,"%s %05d: close connection to VRU", pclFct, ilCpid);
      close(connfd);

      dbg(TRACE,"\n\n%s %05d: kill myself\n\n", pclFct, ilCpid);
    }
    else
    {
      prlEvent     = (EVENT *)   ((char *) prgItem->text) ;
      prlBchead    = (BC_HEAD *) ((char *) prlEvent + sizeof(EVENT)) ;
      prlCmdblk    = (CMDBLK *)  ((char *) prlBchead->data) ;
      pclSelection = (char *)    prlCmdblk->data ;
      pclFields    = (char *)    pclSelection + strlen(pclSelection) + 1 ;
      pclData      = (char *)    pclFields + strlen(pclFields) + 1 ;
 
      /* Event anzeigen */
      /* DebugPrintItem(DEBUG,prgItem); */
      /* DebugPrintEvent(DEBUG,prlEvent); */
      /* DebugPrintBchead(DEBUG,prlBchead); */
      /* DebugPrintCmdblk(DEBUG,prlCmdblk); */

      /* dbg(DEBUG,"%s %05d: selection <%s>", pclFct, ilCpid, pclSelection); */
      /* dbg(DEBUG,"%s %05d: fields    <%s>", pclFct, ilCpid, pclFields); */
      /* dbg(DEBUG,"%s %05d: data      <%s>", pclFct, ilCpid, pclData); */
      dbg(DEBUG,"%s %05d: received -%s-", pclFct, ilCpid, pclData);

      /* "Antwort" Event umwandeln in vrumsg */
      strcpy(pclWvrumsg,pclData);

      /* vrumsg in den socket zur VRU schreiben */
      /* send message to VRU */
      sprintf(pclWvrumsg, "%s\n\r", pclData);
      dbg(DEBUG,"%s %05d: send %s to VRU", pclFct, ilCpid, pclWvrumsg);
      ilRC = write(connfd, pclWvrumsg, strlen(pclWvrumsg)+0);
   /* Baustelle ... Fehlerpruefung */

      /* close connection to VRU */
      dbg(DEBUG,"%s %05d: close connection to VRU", pclFct, ilCpid);
      close(connfd); /* bleibt erst mal so ... kein shutdown */

      dbg(TRACE,"\n\n%s %05d: finished job\n\n", pclFct, ilCpid);

    } /* end of Wait for answer */


    /* temp Queue loeschen */
    ilRC = que( QUE_DELETE, ilAnswerQueue, ilAnswerQueue, 0, 0, 0) ;
    if(ilRC != RC_SUCCESS)
    {
      dbg(TRACE,"%s %05d: que QUE_DELETE <%d> failed <%d>",
                     pclFct, ilCpid, ilAnswerQueue, ilRC) ;
    }
    else
    {
      dbg(DEBUG,"%s %05d: queue <%d> <%s> deleted",
        pclFct, ilCpid, ilAnswerQueue, &pclQueueName[0]) ;
    }

  } /* end of ... got queue */

  return RC_SUCCESS;

} /* end of child */



/******************************************************************************/
/* WaitAndCheckQueue **********************************************************/
/******************************************************************************/
static int WaitAndCheckQueue(int ipTimeout, int ipModId, ITEM **prpItem)
{
  int ilRC          = RC_SUCCESS; /* Return code */
  int ilWaitCounter = 0;
 
  do
  {
    ilRC = CheckQueue( ipModId, prpItem);
    if(ilRC != RC_SUCCESS)
    {
      if(ilRC != QUE_E_NOMSG)
      {
        dbg(TRACE,"WaitAndCheckQueue: CheckQueue <%d> failed <%d>",mod_id,ilRC);
      }
    }

    napms(100); /* nap 100 ms */
    ilWaitCounter += 1;
 
  }while((ilRC == QUE_E_NOMSG) && (ilWaitCounter < ipTimeout));
 
  if(ilWaitCounter >= ipTimeout)
  {
    dbg(TRACE,"WaitAndCheckQueue: timeout reached <%d>",ipTimeout);
    ilRC = RC_FAIL;
  }
 
  return ilRC;

} /* end of WaitAndCheckQueue */



/******************************************************************************/
/* CheckQueue *****************************************************************/
/******************************************************************************/
static int CheckQueue(int ipModId, ITEM **prpItem)
{
  int     ilRC       = RC_SUCCESS; /* Return code */
  int     ilItemSize = 0;
  EVENT *prlEvent    = NULL;
 
  ilItemSize = I_SIZE;
 
  ilRC = que(QUE_GETBIGNW,ipModId,ipModId,PRIORITY_3,ilItemSize,(char *) prpItem);
  if(ilRC == RC_SUCCESS)
  {
    prlEvent = (EVENT*) ((*prpItem)->text);
 
    switch( prlEvent->command )
    {
      case    HSB_STANDBY :
              dbg(TRACE,"CheckQueue: HSB_STANDBY");
              HandleQueues();
              ctrl_sta = prlEvent->command;
              break;
 
      case    HSB_COMING_UP   :
              dbg(TRACE,"CheckQueue: HSB_COMING_UP");
              ctrl_sta = prlEvent->command;
              break;
 
      case    HSB_ACTIVE  :
              dbg(TRACE,"CheckQueue: HSB_ACTIVE");
              ctrl_sta = prlEvent->command;
              break;
 
      case    HSB_ACT_TO_SBY  :
              dbg(TRACE,"CheckQueue: HSB_ACT_TO_SBY");
              HandleQueues();
              ctrl_sta = prlEvent->command;
              break;

      case    HSB_DOWN    :
              dbg(TRACE,"CheckQueue: HSB_DOWN");
              ctrl_sta = prlEvent->command;
              /* Acknowledge the item */
              ilRC = que(QUE_ACK,0,ipModId,0,0,NULL);
              if( ilRC != RC_SUCCESS )
              {
                /* handle que_ack error */
                HandleQueErr(ilRC);
              }
              Terminate();
              break;
 
      case    HSB_STANDALONE  :
              dbg(TRACE,"CheckQueue: HSB_STANDALONE");
              ctrl_sta = prlEvent->command;
              break;
 
      case    SHUTDOWN    :
              dbg(TRACE,"CheckQueue: SHUTDOWN");
              /* Acknowledge the item */
              ilRC = que(QUE_ACK,0,ipModId,0,0,NULL);
              if( ilRC != RC_SUCCESS )
              {
                /* handle que_ack error */
                HandleQueErr(ilRC);
              }
              Terminate();
              break;
 
      case    RESET       :
              ilRC = Reset();
              break;
 
      case    EVENT_DATA  :
              /* DebugPrintItem(DEBUG,prgItem); */
              /* DebugPrintEvent(DEBUG,prlEvent); */
              break;
 
      case    TRACE_ON :
              dbg_handle_debug(prlEvent->command);
              break;
 
      case    TRACE_OFF :
              dbg_handle_debug(prlEvent->command);
              break;

      default:
              dbg(TRACE,"CheckQueue: unknown event");
              DebugPrintItem(TRACE,*prpItem);
              DebugPrintEvent(TRACE,prlEvent);
              break;
 
    } /* end switch */
 
    /* Acknowledge the item */
    ilRC = que( QUE_ACK, 0, ipModId, 0, 0, NULL);
    if( ilRC != RC_SUCCESS )
    {
      /* handle que_ack error */
      HandleQueErr(ilRC);
    }
  }
  else
  {
    if(ilRC != QUE_E_NOMSG)
    {
      dbg(TRACE,"CheckQueue: que(QUE_GETBIGNW,%d,%d,...) failed <%d>",ipModId,ipModId,ilRC);
      HandleQueErr(ilRC);
    }
  }
 
  return ilRC;

} /* end of CheckQueue */



/******************************************************************************/
/* The End ********************************************************************/
/******************************************************************************/

