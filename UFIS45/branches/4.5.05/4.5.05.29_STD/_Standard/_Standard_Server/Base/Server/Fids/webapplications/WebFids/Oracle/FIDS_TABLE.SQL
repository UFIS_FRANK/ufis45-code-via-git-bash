CREATE OR REPLACE VIEW FIDS_TABLE AS
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
--      Function              - FIDS_TABLE VIEW
--
--      Version               - 1.0.0
--
--      Written By            - Juergen Hammerschmid
--
--      Return                -
--
--      Errors                - 
--
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
--      Change Log
--
--      Name           Date               Version    Change
--      ----           ----------         -------    ------
--      Hammerschmid   30.05.2008         1.0        new
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
SELECT a.FDDURNO,
	a.AFTURNO,
	a.FTYP,
	a.ADVERTISMENT_ID,
    a.IP_ADDRESS,
	a.IP_PORT,
	a.DEVICE_NAME,
	a.DEVICE_AREA,
	a.ALARM_PAGE_NUMBER,
	a.DEFAULT_PAGE_NUMBER,
	a.FIRST_PAGE_NUMER,
	a.NUMBER_OF_PAGES,
	a.PAGE_NUMBER,
	a.INSTALLATION_CONFIG_FILE,
	a.TERMINAL_NAME,
	a.GROUP_NAME,
	a.DISPLAY_ID,
	a.PAGE_ID,
	a.CONFIG_FILE,
	a.DISPLAY_SEQUENCE,
	a.NUMBER_OF_FLIGHTS,
	a.REFERENCED_FIELD,
	a.PAGE_CAROUSEL_TIME,
	a.TIME_DELETE_1,
	a.TIME_DELETE_2,
	a.TIME_CANCELLED,
	a.TIME_FRAME_BEGIN,
	a.TIME_FRAME_END,
	a.HOME_AIRPORT,
	a.AIRLINECODE,
	a.AIRLINECODE2,
	a.FLIGHTNUMBER,
	a.FLIGHT_TYPE,
	a.CODE_SHARE,
	a.FLIGHT_NATURE,
	a.ESTIMATED_TIME_OF_FLIGHT,
	a.SCHEDULED_TIME,
	a.TIFF,
	a.STANDARD_TIME_OF_DEPARTURE,
	a.ESTIMATED_TIME_OF_DEPARTURE,
	a.STANDARD_TIME_OF_ARRIVAL,
	a.TIF_ARRIVAL,
	a.TIF_DEPARTURE,
	a.NEW_TIME,
	a.ETA_INTERN_BEST,
	a.ETD_INTERN_BEST,
	a.BOARDING_OPEN,
	a.ACTUAL_TIME,
	a.OFFBLOCK_TIME,
	a.ONBLOCK_TIME,
	a.AIRBORNE_TIME,
	a.TIME_TO_CURRENT_REMARK,
	a.LANDING_TIME,
	a.BELT1,
	a.BELT2,
	a.CHECK_IN_FROM,
	a.CHECK_IN_TO,
	a.CHECKIN_REMARK,
	a.CHECKIN_TYPE,
	a.BELT_REMARK,
	a.RESOURCE_NAME,
	a.RESOURCE_TYPE,
	a.FLIGHT_LOGO,
	a.FULL_SCREEN_LOGO,
	a.TEXT1,
	a.TEXT2,
	a.GATE_DEPARTURE_1,
	a.GATE_DEPARTURE_2,
	a.GATE_ARRIVAL_1,
	a.GATE_ARRIVAL_2,
	a.POSITION_ARRIVAL,
	a.POSITION_DEPARTURE,
	a.REGISTRATION,
	a.CAROUSEL,
	a.STATUS,
	a.SEQ_ID,
	a.DATAREFRESHTIME,
	a.AIRPORTCODE,
	(select apfn from apttab where apc3 = a.AIRPORTCODE) AIRPORT_DESCRIPTION,
	(select apn2 from apttab where apc3 = a.AIRPORTCODE) AIRPORT_SHORT_NAME_2,
	(select apn3 from apttab where apc3 = a.AIRPORTCODE) AIRPORT_SHORT_NAME_3,
	(select apn4 from apttab where apc3 = a.AIRPORTCODE) AIRPORT_SHORT_NAME_4,
	(select apsn from apttab where apc3 = a.AIRPORTCODE) AIRPORT_SHORT_NAME,
    a.AIRPORT_VIA,
    a.VIA_AIRPORT,
    (select apfn from apttab where apc3 = a.VIA_AIRPORT) VIA_DESCRICPTION,
    (select apn2 from apttab where apc3 = a.VIA_AIRPORT) VIA_SHORT_NAME_2,
    (select apn3 from apttab where apc3 = a.VIA_AIRPORT) VIA_SHORT_NAME_3,
    (select apn4 from apttab where apc3 = a.VIA_AIRPORT) VIA_SHORT_NAME_4,
    (select apsn from apttab where apc3 = a.VIA_AIRPORT) VIA_SHORT_NAME,
	a.AIRPORT_ORIGIN,
    (select apfn from apttab where apc3 = a.AIRPORT_ORIGIN) ORIGIN_DESCRICPTION,
    (select apn2 from apttab where apc3 = a.AIRPORT_ORIGIN) ORIGIN_SHORT_NAME_2,
    (select apn3 from apttab where apc3 = a.AIRPORT_ORIGIN) ORIGIN_SHORT_NAME_3,
    (select apn4 from apttab where apc3 = a.AIRPORT_ORIGIN) ORIGIN_SHORT_NAME_4,
    (select apsn from apttab where apc3 = a.AIRPORT_ORIGIN) ORIGIN_SHORT_NAME,
	a.AIRPORT_DESTINATION,
    (select apfn from apttab where apc3 = a.AIRPORT_DESTINATION) DESTINATION_DESCRICPTION,
    (select apn2 from apttab where apc3 = a.AIRPORT_DESTINATION) DESTINATION_SHORT_NAME_2,
    (select apn3 from apttab where apc3 = a.AIRPORT_DESTINATION) DESTINATION_SHORT_NAME_3,
    (select apn4 from apttab where apc3 = a.AIRPORT_DESTINATION) DESTINATION_SHORT_NAME_4,
    (select apsn from apttab where apc3 = a.AIRPORT_DESTINATION) DESTINATION_SHORT_NAME,
    a.CHECKIN_SUM_REMARK_STATUS,
    (select beme from fidtab where trim(code) = trim(a.CHECKIN_SUM_REMARK_STATUS)) CHECKIN_SUM_REMARK_DESCRIPTION,
    (select bemd from fidtab where trim(code) = trim(a.CHECKIN_SUM_REMARK_STATUS)) CHECKIN_SUM_REMARK_LOCAL,
    (select bet3 from fidtab where trim(code) = trim(a.CHECKIN_SUM_REMARK_STATUS)) CHECKIN_SUM_REMARK_3,
    (select bet4 from fidtab where trim(code) = trim(a.CHECKIN_SUM_REMARK_STATUS)) CHECKIN_SUM_REMARK_4,
    a.FLIGHT_STATUS,
    (select beme from fidtab where trim(code) = trim(a.FLIGHT_STATUS)) FLIGHT_STATUS_DESCRIPTION,
    (select bemd from fidtab where trim(code) = trim(a.FLIGHT_STATUS)) FLIGHT_STATUS_LOCAL,
    (select bet3 from fidtab where trim(code) = trim(a.FLIGHT_STATUS)) FLIGHT_STATUS_3,
    (select bet4 from fidtab where trim(code) = trim(a.FLIGHT_STATUS)) FLIGHT_STATUS_4
FROM WEBFIDS_TABLE a
WITH READ ONLY;
