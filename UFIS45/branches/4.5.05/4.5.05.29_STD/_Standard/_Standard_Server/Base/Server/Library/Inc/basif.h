#ifndef _DEF_mks_version_basif_h
  #define _DEF_mks_version_basif_h
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_basif_h[]   MARK_UNUSED = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Library/Inc/basif.h 1.3 2004/07/27 17:25:34SGT jim Exp  $";
#endif /* _DEF_mks_version */
/* **************************************************************** */
/*  The Master include file.                                        */
/*                                                                  */
/*  Program       : basif                                           */
/*                                                                  */
/*  Revision date :                                                 */
/*                                                                  */
/*  Author        : RKL                                             */
/*                                                                  */
/*  History  :16.06.00 RKL  Written                                 */
/*                                                                  */
/* **************************************************************** */
/* **************************************************************** */
/* **************************************************************** */
/* **************************************************************** */
/* source-code-control-system version string                        */
/* **************************************************************** */

/* Defines of I/O - Register Structures of the COM-Server */

#ifndef _BASIF_H_
#define _BASIF_H_

/* ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! */
/* !   WORD is 16bit_Integer  it depending from the Machine  ! */
/* ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! */
#ifdef _SOLARIS
typedef short WORD; 	
#else
typedef int WORD;
#endif

#ifndef BOOL
#define BOOL int
#endif /* BOOL */

#define READ_TIMEOUT 2
#define WRITE_TIMEOUT 2


#define MAX_REGISTER 1024

/* structure Types */

#define REG_READ	0x0001	/* Read Register */
#define REG_WRITE	0x0008	/* Write Register */
#define SET_BIT		0x0009	/* Set Bit */
#define SND_MODE	0x0010	/* Send Mode    */
#define REG_REQUEST	0x0021	/* Register Request */
#define REG_STATE	0x0031	/* Register State   */



typedef struct _HEAD
{
	WORD	sendseq;    /* UDP: Transmit - Packetcounter or 0 , TCP : 0 */
	WORD	recvseq;    /* UDP: Receive  - Packetcounter or 0 , TCP : 0 */
	WORD	type;       /* Strukture Typ from name */
	WORD	length;     /* Packetlength in Bytes ( 8 + sizeof(name ) ) */
} HEAD;


typedef struct _REQUEST
{
	HEAD	head;

} REQUEST;

typedef struct _WRITE_REG   
{
	HEAD	head;
	WORD	reg_anz;  /* number of register */
	WORD	reg[1024]; /* Maximum number of Register to be send */

} WRITE_REG;

typedef struct _SET_OUT
{
	HEAD	head;
	WORD	set_bits;
	WORD	value;

} SET_OUT;

typedef struct _SEND_MODE
{
	HEAD	head;
	WORD	IO_bits;
	WORD	interval;

} SEND_MODE;

typedef struct _STATE_REG
{
	HEAD	head;
	WORD	driver_id;
	WORD	input_reg;
	WORD	output_reg;
} STATE_REG;

/************************************************************/


#define TYPE_CHECK  0x0001
#define TYPE_SET    0x0002


/* stuctures */

typedef struct _BASTcpInfo
{
	char	cHostname[64];
	char	cServicename[32];
	int	iRecvTimeout;
	int	iConnect;
	int	iSocket;
	int	iInitOK;
} BASTcpInfo;

typedef struct _BASCedaEvent {
	char	cCommand[8];
	char	cTablename[8];
	char	cSelection[1024];
	char	cNull[32];
	char	cNotNull[32];
} BASCedaEvent;

typedef struct _BASCmds {
	int     iValid;
	char	cCommand[32];
	char	cLogicalName[32];
	int		iType;         /* TYP_CHECK=1 || TYPE_SET=2 */ 	
	char	cOutputNr[32];
	char	cOutputValue[32];
	WORD	iOutRegisterBit;
	WORD	iOutRegisterValue;        
	int	iInputNr;
	WORD	iInRegisterBit;
	WORD	iInRegisterValue;
	char	cCommandSend[32];
	int 	iModId;
	BASCedaEvent  *prCedaEvent;
} BASCmds;

typedef struct _BASMain {
	int 	iNoOfCmds;	/* number of commands */
	BASTcpInfo rInfo;	/* TCPInfo structure for BAS */
	BASCmds	*prCmds;
} BASMain;

/************************************************************/





#endif  /* _BASIF_H_ */



