#ifndef _DEF_mks_version_ugccsma_h
  #define _DEF_mks_version_ugccsma_h
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_ugccsma_h[]   MARK_UNUSED = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Library/Inc/ugccsma.h 1.3 2004/07/27 17:04:47SGT jim Exp  $";
#endif /* _DEF_mks_version */
/* ******************************************************************** */
/*									*/
/* This file contains the global macros for the ugccs environment.	*/
/*									*/
/* 20020822 JIM: if argv[0] contains path (i.e. started by ddd), skip path */
/* 20020822 JIM: initialize_no_dbg: avoid (empty) putque log files      */
/* ******************************************************************** */
#ifndef UGCCSMA_HEADER
#define UGCCSMA_HEADER 


#include <stdio.h>

extern void initialize(int argc,char *argv[]);
extern void initialize_no_dbg(int argc,char *argv[]);

int	mod_id;
int     ctrl_sta;
char	*mod_name;
char	__CedaLogFile[128];
FILE *outp;

#define MAIN		    char  *mod_version = mks_version; \
                    main(int argc,char *argv[],char *envp[])

/*** #ifdef STH_USE vbl ***/
#include "initdef.h"
/*** #endif vbl ***/

#define INITIALIZE	initialize(argc,argv);

#define CLEANUP  fclose(outp);

/* New prot file for child */
#define REINITIALIZE	fclose(outp);\
       		sprintf(__CedaLogFile,"%s/%s%.5ld.log",getenv("DBG_PATH"),mod_name,getpid());\
			outp = fopen(__CedaLogFile,"w");


#define CLEANUP  fclose(outp);

#ifndef NEW
#define NEW(dat)	(dat *) malloc(sizeof(dat))
#endif

#endif

/****************************************************************************/
/****************************************************************************/
