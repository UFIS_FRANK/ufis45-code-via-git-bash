#ifndef _DEF_mks_version_fdipsc
  #define _DEF_mks_version_fdipsc
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_fdipsc[] = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Kernel/FDI/fdipsc.src 1.6 2012/02/14 15:44:43SGT dka Exp  $";
#endif /* _DEF_mks_version */
/******************************************************************************/
/*                                                                            */
/* ABB ACE/FC Program Skeleton                                                */
/*                                                                            */
/* Author         :  AKL                                                      */
/* Date           :                                                           */
/* Description    :  INCLUDE PART FROM FDIHDL                                 */
/*                   Passenger Service Message                                */
/*                                                                            */
/* Update history :                                                           */
/* 20110608 1.4 Corrected mks_version string name.            */
/*          Add a second test for "=TEXT" if a dot is not found   */
/*          at first column anywhere in telex.            */
/* 20120210 1.5 UFIS-1341 - Stop processing PSC if AA line is not matching    */
/*              configuration. Set status to Error.                           */
/*              UFIS-1385 - flag when values are saved to LOATAB so fdihdl.c  */
/*              SendToLoatab() can evaluate whether to send to LOAHDL. Will   */
/*              use cgTlxStatus as the flag. 'U' means that values would be   */
/*              saved to LOATAB.                                              */
/* 20120214 GFO: Added DB_LoadUrnoList UFIS-1484                              */
/******************************************************************************/
/*                                                                            */
/* FDIPSC handle functions                                                    */
/* -----------------------                                                    */
/* static int HandlePSC(char *pcpData, int ipIdx)                             */
/* static int ReadPSCConfig()                                                 */
/* static int CheckTelexInputForPSCData(char *pcpData)                        */
/* static int ConvertDateToCEDA(char *pcpDate, char *pcpFormat)               */
static int GetPSCValueLine(char *pcpData, char *pcpKey, char *pcpValueLine);
static int GetPSCValues(char *pcpValueLine, int ipFclW, int ipBclW, int ipEclW,
                        char *pcpFcl, char *pcpBcl, char *pcpEcl);
static void GetDigits(char *pcpValue);
static int StorePSCData(char *pcpFieldList, char *pcpType, char *pcpValue, char *pcpRecCont);

static int HandlePSC(char *pcpData, int ipIdx)
{
  int ilRC;
  char pclFunc[]="HandlePSC:";
  char pclValueLine[128];
  typedef struct
  {
     char pclFcl[128];
     char pclBcl[128];
     char pclEcl[128];
  } PSC_VALUES;
  PSC_VALUES prlPscValues[10];
  char pclSqlBuf[1024];
  char pclSelection[1024];
  char pclFieldList[1024];
  char pclValueList[1024];
  char pclSum[16];

  if (igCheckPscTelex == FALSE)
  {
     ilRC = CheckTelexInputForPSCData(pcpData);
     return ilRC;
  }

  InitRecordDescriptor(&rgRecDesc,1024,0,FALSE);
  memset(pcgLoaBuf,0x00,1000*512);
  igArrayPtr = 0;

  memset(prlPscValues,0x00,sizeof(prlPscValues));

  ilRC = GetPSCValueLine(pcpData,&prgPscConfigData[ipIdx].pclAAKey[0],pclValueLine);
  dbg(DEBUG,"%s Found AA Line: <%s>",pclFunc,pclValueLine);
  if (strlen(pclValueLine) > 0)
  {
     ilRC = GetPSCValues(pclValueLine,prgPscConfigData[ipIdx].ilFirstValue,
                         prgPscConfigData[ipIdx].ilBusValue,prgPscConfigData[ipIdx].ilEcoValue,
                         prlPscValues[0].pclFcl,prlPscValues[0].pclBcl,prlPscValues[0].pclEcl);
     dbg(DEBUG,"%s Found AA Values: <%s> <%s> <%s>",pclFunc,
         prlPscValues[0].pclFcl,prlPscValues[0].pclBcl,prlPscValues[0].pclEcl);
  }
  else /* strlen() never returns less than zero */
  {
    dbg(DEBUG,"%s AA line does not match configuration. Stop processing",pclFunc);
    cgTlxStatus = FDI_STAT_ERROR;
    return ilRC;
  }

  ilRC = GetPSCValueLine(pcpData,&prgPscConfigData[ipIdx].pclBBKey[0],pclValueLine);
  dbg(DEBUG,"%s Found BB Line: <%s>",pclFunc,pclValueLine);
  if (strlen(pclValueLine) > 0)
  {
     ilRC = GetPSCValues(pclValueLine,prgPscConfigData[ipIdx].ilFirstValue,
                         prgPscConfigData[ipIdx].ilBusValue,prgPscConfigData[ipIdx].ilEcoValue,
                         prlPscValues[1].pclFcl,prlPscValues[1].pclBcl,prlPscValues[1].pclEcl);
     dbg(DEBUG,"%s Found BB Values: <%s> <%s> <%s>",
         pclFunc,prlPscValues[1].pclFcl,prlPscValues[1].pclBcl,prlPscValues[1].pclEcl);
  }
  ilRC = GetPSCValueLine(pcpData,&prgPscConfigData[ipIdx].pclCCKey[0],pclValueLine);
  dbg(DEBUG,"%s Found CC Line: <%s>",pclFunc,pclValueLine);
  if (strlen(pclValueLine) > 0)
  {
     ilRC = GetPSCValues(pclValueLine,prgPscConfigData[ipIdx].ilFirstValue,
                         prgPscConfigData[ipIdx].ilBusValue,prgPscConfigData[ipIdx].ilEcoValue,
                         prlPscValues[2].pclFcl,prlPscValues[2].pclBcl,prlPscValues[2].pclEcl);
     dbg(DEBUG,"%s Found CC Values: <%s> <%s> <%s>",
         pclFunc,prlPscValues[2].pclFcl,prlPscValues[2].pclBcl,prlPscValues[2].pclEcl);
  }
  ilRC = GetPSCValueLine(pcpData,&prgPscConfigData[ipIdx].pclDDKey[0],pclValueLine);
  dbg(DEBUG,"%s Found DD Line: <%s>",pclFunc,pclValueLine);
  if (strlen(pclValueLine) > 0)
  {
     ilRC = GetPSCValues(pclValueLine,prgPscConfigData[ipIdx].ilFirstValue,
                         prgPscConfigData[ipIdx].ilBusValue,prgPscConfigData[ipIdx].ilEcoValue,
                         prlPscValues[3].pclFcl,prlPscValues[3].pclBcl,prlPscValues[3].pclEcl);
     dbg(DEBUG,"%s Found DD Values: <%s> <%s> <%s>",
         pclFunc,prlPscValues[3].pclFcl,prlPscValues[3].pclBcl,prlPscValues[3].pclEcl);
  }
  ilRC = GetPSCValueLine(pcpData,&prgPscConfigData[ipIdx].pclEEKey[0],pclValueLine);
  dbg(DEBUG,"%s Found EE Line: <%s>",pclFunc,pclValueLine);
  if (strlen(pclValueLine) > 0)
  {
     ilRC = GetPSCValues(pclValueLine,prgPscConfigData[ipIdx].ilFirstValue,
                         prgPscConfigData[ipIdx].ilBusValue,prgPscConfigData[ipIdx].ilEcoValue,
                         prlPscValues[4].pclFcl,prlPscValues[4].pclBcl,prlPscValues[4].pclEcl);
     dbg(DEBUG,"%s Found EE Values: <%s> <%s> <%s>",
         pclFunc,prlPscValues[4].pclFcl,prlPscValues[4].pclBcl,prlPscValues[4].pclEcl);
  }
  ilRC = GetPSCValueLine(pcpData,&prgPscConfigData[ipIdx].pclFFKey[0],pclValueLine);
  dbg(DEBUG,"%s Found FF Line: <%s>",pclFunc,pclValueLine);
  if (strlen(pclValueLine) > 0)
  {
     ilRC = GetPSCValues(pclValueLine,prgPscConfigData[ipIdx].ilFirstValue,
                         prgPscConfigData[ipIdx].ilBusValue,prgPscConfigData[ipIdx].ilEcoValue,
                         prlPscValues[5].pclFcl,prlPscValues[5].pclBcl,prlPscValues[5].pclEcl);
     dbg(DEBUG,"%s Found FF Values: <%s> <%s> <%s>",
         pclFunc,prlPscValues[5].pclFcl,prlPscValues[5].pclBcl,prlPscValues[5].pclEcl);
  }
  ilRC = GetPSCValueLine(pcpData,&prgPscConfigData[ipIdx].pclINFKey[0],pclValueLine);
  dbg(DEBUG,"%s Found INF Line: <%s>",pclFunc,pclValueLine);
  if (strlen(pclValueLine) > 0)
  {
     ilRC = GetPSCValues(pclValueLine,prgPscConfigData[ipIdx].ilFirstValue,0,0,
                         prlPscValues[6].pclFcl,prlPscValues[6].pclBcl,prlPscValues[6].pclEcl);
     dbg(DEBUG,"%s Found INF Value: <%s>",pclFunc,prlPscValues[6].pclFcl);
  }

  sprintf(pclSelection,"WHERE FLNU = %s AND DSSN = 'PSC'",pcgAftUrno);
  strcpy(pclSqlBuf,"DELETE FROM LOATAB ");
  strcat(pclSqlBuf,pclSelection);
  ilRC = FdiSendSql(pclSqlBuf);
  strcpy(pclFieldList,"TIME,IDNT,TYPE,STYP,SSTP,SSST,APC3,VALU,DSSN,FLNU,RURN");
  if (igNewLoatab == TRUE)
  {
     strcat(pclFieldList,",KEYC,LEVL,SORT,REMA");
     if (igNewLoatabUkey == TRUE)
         strcat(pclFieldList,",UKEY");
  }
  ilRC = StorePSCData(pclFieldList,"PSC,F, , , ",prlPscValues[0].pclFcl,pcgPscFclJoiPax);
  ilRC = StorePSCData(pclFieldList,"PSC,B, , , ",prlPscValues[0].pclBcl,pcgPscBclJoiPax);
  ilRC = StorePSCData(pclFieldList,"PSC,E, , , ",prlPscValues[0].pclEcl,pcgPscEclJoiPax);
  sprintf(pclSum,"%d",atoi(prlPscValues[0].pclFcl) +
                      atoi(prlPscValues[0].pclBcl) +
                      atoi(prlPscValues[0].pclEcl));
  ilRC = StorePSCData(pclFieldList,"PSC,S, , , ",pclSum,pcgPscJoiPax);
  ilRC = StorePSCData(pclFieldList,"PSC,F,T,1, ",prlPscValues[3].pclFcl,pcgPscFclJoiTt1);
  ilRC = StorePSCData(pclFieldList,"PSC,B,T,1, ",prlPscValues[3].pclBcl,pcgPscBclJoiTt1);
  ilRC = StorePSCData(pclFieldList,"PSC,E,T,1, ",prlPscValues[3].pclEcl,pcgPscEclJoiTt1);
  sprintf(pclSum,"%d",atoi(prlPscValues[3].pclFcl) +
                      atoi(prlPscValues[3].pclBcl) +
                      atoi(prlPscValues[3].pclEcl));
  ilRC = StorePSCData(pclFieldList,"PSC,S,T,1, ",pclSum,pcgPscJoiTt1);
  ilRC = StorePSCData(pclFieldList,"PSC,F,T,2, ",prlPscValues[1].pclFcl,pcgPscFclJoiTt2);
  ilRC = StorePSCData(pclFieldList,"PSC,B,T,2, ",prlPscValues[1].pclBcl,pcgPscBclJoiTt2);
  ilRC = StorePSCData(pclFieldList,"PSC,E,T,2, ",prlPscValues[1].pclEcl,pcgPscEclJoiTt2);
  sprintf(pclSum,"%d",atoi(prlPscValues[1].pclFcl) +
                      atoi(prlPscValues[1].pclBcl) +
                      atoi(prlPscValues[1].pclEcl));
  ilRC = StorePSCData(pclFieldList,"PSC,S,T,2, ",pclSum,pcgPscJoiTt2);
  ilRC = StorePSCData(pclFieldList,"PSC,S,I,2, ",prlPscValues[6].pclFcl,pcgPscTotInf);
  if (strlen(pcgLoaBuf) > 0 && igArrayInsert == TRUE)
  {
     igArrayPtr--;
     pcgLoaBuf[igArrayPtr] = '\0';
     strcpy(pclFieldList,"URNO,TIME,IDNT,TYPE,STYP,SSTP,SSST,APC3,VALU,DSSN,FLNU,RURN");
     strcpy(pclValueList,":VURNO,:VTIME,:VIDNT,:VTYPE,:VSTYP,:VSSTP,:VSSST,:VAPC3,:VVALU,:VDSSN,:VFLNU,:VRURN");
     if (igNewLoatab == TRUE)
     {
        strcat(pclFieldList,",KEYC,LEVL,SORT,REMA");
        strcat(pclValueList,",:VKEYC,:VLEVL,:VSORT,:VREMA");
        if (igNewLoatabUkey == TRUE)
        {
           strcat(pclFieldList,",UKEY");
           strcat(pclValueList,",:VUKEY");
        }
     }
     ilRC = SqlArrayInsert(pclFieldList,pclValueList,pcgLoaBuf);
  }

  return ilRC;
} /* End of HandlePSC */


static int StorePSCData(char *pcpFieldList, char *pcpType, char *pcpValue, char *pcpRecCont)
{
  int ilRC = RC_SUCCESS;
  char pclValueList[2000];

  sprintf(pclValueList,"%s, ,%s,%s,PSC,%s,%s",
          pcgFlightSchedTime,pcpType,pcpValue,pcgAftUrno,pcgNextTlxUrno);
  if (igNewLoatab == TRUE)
  {
     strcat(pclValueList,",");
     strcat(pclValueList,pcpRecCont);
     if (igNewLoatabUkey == TRUE)
        strcat(pclValueList,",0");
  }
  if (igArrayInsert == TRUE)
  {
     ilRC = FillArray(pclValueList);
  }
  else
  {
     ilRC = FdiHandleSql("IBT","LOATAB","",pcpFieldList,pclValueList,
                         pcgTwStart,pcgTwEndNew,FALSE,FALSE);
  }

  return RC_SUCCESS;
} /* End of StorePSCData */


static int GetPSCValues(char *pcpValueLine, int ipFclW, int ipBclW, int ipEclW,
                        char *pcpFcl, char *pcpBcl, char *pcpEcl)
{
  int ilRC = RC_SUCCESS;
  char pclFunc[]="GetPSCValues:";
  char pclLine[128];

  strcpy(pcpFcl,"");
  strcpy(pcpBcl,"");
  strcpy(pcpEcl,"");
  ReplaceChar(pcpValueLine,'.',' ');
  ReplaceChar(pcpValueLine,':',' ');
  ReplaceChar(pcpValueLine,'=',' ');
  ReplaceChar(pcpValueLine,'-',' ');
  ReplaceChar(pcpValueLine,'/',' ');
  MakeTokenList(pclLine,pcpValueLine," ",',');
  if (ipFclW != 0)
  {
     get_real_item(pcpFcl,pclLine,ipFclW);
     GetDigits(pcpFcl);
  }
  if (ipBclW != 0)
  {
     get_real_item(pcpBcl,pclLine,ipBclW);
     GetDigits(pcpBcl);
  }
  if (ipEclW != 0)
  {
     get_real_item(pcpEcl,pclLine,ipEclW);
     GetDigits(pcpEcl);
  }

  return ilRC;
} /* End of GetPSCValues */


static void GetDigits(char *pcpValue)
{
  int ilRC;
  char pclTmpBuf[128];
  int ilI, ilJ;

  memset(pclTmpBuf,0x00,128);
  ilJ = 0;
  for (ilI = 0; ilI < strlen(pcpValue) && ilI < 128; ilI++)
  {
     if (pcpValue[ilI] >= '0' && pcpValue[ilI] <= '9')
     {
        pclTmpBuf[ilJ] = pcpValue[ilI];
        ilJ++;
     }
  }
  if (*pclTmpBuf == '\0')
     strcpy(pclTmpBuf,"0");
  if (strlen(pclTmpBuf) == 3)
  {
     if (pclTmpBuf[0] == '0')
     {
        if (pclTmpBuf[1] == '0')
           strcpy(pcpValue,&pclTmpBuf[2]);
        else
           strcpy(pcpValue,&pclTmpBuf[1]);
     }
     else
        strcpy(pcpValue,pclTmpBuf);
  }
  else if (strlen(pclTmpBuf) == 2)
  {
     if (pclTmpBuf[0] == '0')
        strcpy(pcpValue,&pclTmpBuf[1]);
     else
        strcpy(pcpValue,&pclTmpBuf[0]);
  }
  else
     strcpy(pcpValue,pclTmpBuf);

} /* End of GetDigits */


static int GetPSCValueLine(char *pcpData, char *pcpKey, char *pcpValueLine)
{
  int ilRC = RC_SUCCESS;
  char pclFunc[]="GetPSCValueLine:";
  char pclSearch[128];
  char *pclKeyPtr;
  char *pclTmpPtr;

  strcpy(pcpValueLine,"");
  if (strcmp(pcpKey,"NIL") != 0)
  {
     if (strncmp(pcpKey,"\134n",2) == 0)
        sprintf(pclSearch,"\n%s",&pcpKey[2]);
     else
        strcpy(pclSearch,pcpKey);
     pclKeyPtr = strstr(pcpData,pclSearch);
     if (pclKeyPtr != NULL)
     {
        pclKeyPtr += strlen(pclSearch);
        if (*pclKeyPtr == 'S' && strstr(pclSearch,"INFANT") != NULL)
           pclKeyPtr++;
        pclTmpPtr = GetLine(pclKeyPtr,1);
        CopyLine(pcpValueLine,pclTmpPtr);
     }
  }

  return ilRC;
} /* End of GetPSCValueLine */


static int ReadPSCConfig()
{
  int ilRC = RC_SUCCESS;
  char pclFunc[]="ReadPSCConfig:";
  int ilI;
  char pclParam[16];
  char pclLine[2048];
  char pclResult[1024];
  char pclItem[1024];

  sprintf(pclParam,"Config%3.3d",igNoPscConfigData+1);
  ilRC = iGetConfigRow(pcgCfgFile,"PSC",pclParam,CFG_STRING,pclLine);
  while (ilRC == RC_SUCCESS && igNoPscConfigData < 200)
  {
     get_real_item(pclResult,pclLine,1);
     ReplaceChar(pclResult,'_',' ');
     strcpy(&prgPscConfigData[igNoPscConfigData].pclGeneralKey[0],pclResult);
     get_real_item(pclResult,pclLine,2);
     ReplaceChar(pclResult,'_',' ');
     ReplaceChar(pclResult,'\134',',');
     get_real_item(pclItem,pclResult,1);
     strcpy(&prgPscConfigData[igNoPscConfigData].pclFlightKey[0],pclItem);
     get_real_item(pclItem,pclResult,2);
     prgPscConfigData[igNoPscConfigData].ilNoCharAlc = atoi(pclItem);
     get_real_item(pclResult,pclLine,3);
     ReplaceChar(pclResult,'_',' ');
     ReplaceChar(pclResult,'\134',',');
     get_real_item(pclItem,pclResult,1);
     strcpy(&prgPscConfigData[igNoPscConfigData].pclDateKey[0],pclItem);
     get_real_item(pclItem,pclResult,2);
     strcpy(&prgPscConfigData[igNoPscConfigData].pclDateFormat[0],pclItem);
     get_real_item(pclResult,pclLine,4);
     ReplaceChar(pclResult,'_',' ');
     strcpy(&prgPscConfigData[igNoPscConfigData].pclAAKey[0],pclResult);
     get_real_item(pclResult,pclLine,5);
     ReplaceChar(pclResult,'_',' ');
     strcpy(&prgPscConfigData[igNoPscConfigData].pclBBKey[0],pclResult);
     get_real_item(pclResult,pclLine,6);
     ReplaceChar(pclResult,'_',' ');
     strcpy(&prgPscConfigData[igNoPscConfigData].pclCCKey[0],pclResult);
     get_real_item(pclResult,pclLine,7);
     ReplaceChar(pclResult,'_',' ');
     strcpy(&prgPscConfigData[igNoPscConfigData].pclDDKey[0],pclResult);
     get_real_item(pclResult,pclLine,8);
     ReplaceChar(pclResult,'_',' ');
     strcpy(&prgPscConfigData[igNoPscConfigData].pclEEKey[0],pclResult);
     get_real_item(pclResult,pclLine,9);
     ReplaceChar(pclResult,'_',' ');
     strcpy(&prgPscConfigData[igNoPscConfigData].pclFFKey[0],pclResult);
     get_real_item(pclResult,pclLine,10);
     ReplaceChar(pclResult,'\134',',');
     get_real_item(pclItem,pclResult,1);
     prgPscConfigData[igNoPscConfigData].ilFirstValue = atoi(pclItem);
     get_real_item(pclItem,pclResult,2);
     prgPscConfigData[igNoPscConfigData].ilBusValue = atoi(pclItem);
     get_real_item(pclItem,pclResult,3);
     prgPscConfigData[igNoPscConfigData].ilEcoValue = atoi(pclItem);
     get_real_item(pclResult,pclLine,11);
     ReplaceChar(pclResult,'_',' ');
     strcpy(&prgPscConfigData[igNoPscConfigData].pclINFKey[0],pclResult);
     get_real_item(pclResult,pclLine,12);
     prgPscConfigData[igNoPscConfigData].ilInfValue = atoi(pclResult);

     igNoPscConfigData++;
     sprintf(pclParam,"Config%3.3d",igNoPscConfigData+1);
     ilRC = iGetConfigRow(pcgCfgFile,"PSC",pclParam,CFG_STRING,pclLine);
  }

  for (ilI = 0; ilI < igNoPscConfigData; ilI++)
  {
     sprintf(pclParam,"Config%3.3d",ilI+1);
     dbg(DEBUG,"%s Parameter Values of %s:",pclFunc,pclParam);
     dbg(DEBUG,"%s General Key      : <%s>",pclFunc,&prgPscConfigData[ilI].pclGeneralKey[0]);
     dbg(DEBUG,"%s Flight Key       : <%s>",pclFunc,&prgPscConfigData[ilI].pclFlightKey[0]);
     dbg(DEBUG,"%s No Chars for ALC : <%d>",pclFunc,prgPscConfigData[ilI].ilNoCharAlc);
     dbg(DEBUG,"%s Date Key         : <%s>",pclFunc,&prgPscConfigData[ilI].pclDateKey[0]);
     dbg(DEBUG,"%s Date Format      : <%s>",pclFunc,&prgPscConfigData[ilI].pclDateFormat[0]);
     dbg(DEBUG,"%s AA KEY           : <%s>",pclFunc,&prgPscConfigData[ilI].pclAAKey[0]);
     dbg(DEBUG,"%s BB KEY           : <%s>",pclFunc,&prgPscConfigData[ilI].pclBBKey[0]);
     dbg(DEBUG,"%s CC KEY           : <%s>",pclFunc,&prgPscConfigData[ilI].pclCCKey[0]);
     dbg(DEBUG,"%s DD KEY           : <%s>",pclFunc,&prgPscConfigData[ilI].pclDDKey[0]);
     dbg(DEBUG,"%s EE KEY           : <%s>",pclFunc,&prgPscConfigData[ilI].pclEEKey[0]);
     dbg(DEBUG,"%s FF KEY           : <%s>",pclFunc,&prgPscConfigData[ilI].pclFFKey[0]);
     dbg(DEBUG,"%s Pos of FCL Value : <%d>",pclFunc,prgPscConfigData[ilI].ilFirstValue);
     dbg(DEBUG,"%s Pos of BCL Value : <%d>",pclFunc,prgPscConfigData[ilI].ilBusValue);
     dbg(DEBUG,"%s Pos of FCL Value : <%d>",pclFunc,prgPscConfigData[ilI].ilEcoValue);
     dbg(DEBUG,"%s INF KEY          : <%s>",pclFunc,&prgPscConfigData[ilI].pclINFKey[0]);
     dbg(DEBUG,"%s Pos of INF Value : <%d>\n",pclFunc,prgPscConfigData[ilI].ilInfValue);
  }

  return ilRC;
} /* end of ReadPSCConfig */


static int CheckTelexInputForPSCData(char *pcpData)
{
  int ilRC = RC_SUCCESS;
  char pclFunc[]="CheckTelexInputForPSCData:";
  int ilRCdb = DB_SUCCESS;
  short slFkt;
  short slCursor;
  char pclSqlBuf[1024];
  char pclSelectBuf[1024];
  char pclDataBuf[1024];
  int ilI;
  char pclTmpBuf[1024];
  char *pclTlxPtr;
  char *pclGenKeyPtr;
  char *pclFltKeyPtr;
  char *pclDatKeyPtr;
  char *pclTmpPtr;
  char pclFltNo[128];
  char pclDate[128];
  char pclResult[128];
  T_TLXRESULT rlTlxResult;
  T_TLXRESULT *prlTlxPtr;
  int ilDebugLevel;

  igCheckPscTelex = TRUE;
  ilRC = RC_FAIL;
  pclTlxPtr = strstr(pcpData,"\n.");
  /*
      v.1.4 - cater for formats where there is no dot at the start of any line
      in the telex.
  */

  if (pclTlxPtr == '\0')
  {
    pclTlxPtr = strstr(pcpData,"=TEXT\n");
    if( pclTlxPtr != NULL )
        pclTlxPtr += 6;
  }

  if (pclTlxPtr != NULL)
  {
     for (ilI = 0; ilI < igNoPscConfigData && ilRC == RC_FAIL; ilI++)
     {
        if (strcmp(&prgPscConfigData[ilI].pclGeneralKey[0],"NIL") != 0)
           pclGenKeyPtr = strstr(pclTlxPtr,&prgPscConfigData[ilI].pclGeneralKey[0]);
        else
           pclGenKeyPtr = pclTlxPtr;
        if (strcmp(&prgPscConfigData[ilI].pclFlightKey[0],"NIL") != 0)
           pclFltKeyPtr = strstr(pclTlxPtr,&prgPscConfigData[ilI].pclFlightKey[0]);
        else
           pclFltKeyPtr = pclTlxPtr;
        if (strcmp(&prgPscConfigData[ilI].pclDateKey[0],"NIL") != 0)
           pclDatKeyPtr = strstr(pclTlxPtr,&prgPscConfigData[ilI].pclDateKey[0]);
        else
           pclDatKeyPtr = pclTlxPtr;
        if (pclGenKeyPtr != NULL && pclFltKeyPtr != NULL && pclDatKeyPtr != NULL)
        {
           pclFltKeyPtr += strlen(&prgPscConfigData[ilI].pclFlightKey[0]);
           pclFltKeyPtr -= prgPscConfigData[ilI].ilNoCharAlc;
           pclTmpPtr = GetLine(pclFltKeyPtr,1);
           CopyLine(pclFltNo,pclTmpPtr);
           MakeTokenList(pclResult,pclFltNo," ",',');
           ReplaceChar(pclResult,'/',',');
           get_real_item(pclFltNo,pclResult,1);
           strcpy(pclTmpBuf,pclFltNo);
           CheckFlightNo(pclFltNo);
           dbg(DEBUG,"%s Found FlightNo <%s>, converted to <%s>",pclFunc,pclTmpBuf,pclFltNo);
           pclDatKeyPtr += strlen(&prgPscConfigData[ilI].pclDateKey[0]);
           pclTmpPtr = GetLine(pclDatKeyPtr,1);
           CopyLine(pclDate,pclTmpPtr);
           MakeTokenList(pclResult,pclDate," ",',');
           ReplaceChar(pclResult,'/',',');
           get_real_item(pclDate,pclResult,1);
           strcpy(pclTmpBuf,pclDate);
           ConvertDateToCEDA(pclDate,&prgPscConfigData[ilI].pclDateFormat[0]);
           dbg(DEBUG,"%s Found Date <%s>, converted to <%s>",pclFunc,pclTmpBuf,pclDate);
           memset(&rlTlxResult,0x00,TLXRESULT);
           pclTmpPtr = pclFltNo;
           ilRC = GetAlc(&pclTmpPtr,prgMasterTlx);
           ilRC = GetFltn(&pclTmpPtr,prgMasterTlx);
           memset(&rlTlxResult,0x00,TLXRESULT);
           strcpy(rlTlxResult.DValue,pcgHomeAP);
           strcpy(rlTlxResult.FValue,pcgHomeAP);
           strcpy(rlTlxResult.DName,"Orig");
           CCSArrayAddUnsort(prgMasterTlx,&rlTlxResult,TLXRESULT);
           rgTlxInfo.TlxCmd = FDI_PSC;
           memset(&rlTlxResult,0x00,TLXRESULT);
           strcpy(rlTlxResult.DValue,"PSC");
           strcpy(rlTlxResult.FValue,"PSC");
           strcpy(rlTlxResult.DName,"TlxTyp");
           CCSArrayAddUnsort(prgMasterTlx,&rlTlxResult,TLXRESULT);
           strcpy(pcgFlightSchedTime,pclDate);
           strcpy(pclSqlBuf,"SELECT URNO,STOD FROM AFTTAB");
           sprintf(pclSelectBuf," WHERE FLNO = '%s' AND TIFD LIKE '%s%%'",pclFltNo,pclDate);
           strcat(pclSelectBuf," AND ADID = 'D' AND FTYP NOT IN ('T','G')");
           strcat(pclSqlBuf,pclSelectBuf);
           if (igIgnoreOldTelexes == TRUE && igTlxTooOld == TRUE)
           {
              cgTlxStatus = FDI_STAT_TLX_TOO_OLD;
              dbg(DEBUG,"%s Telex is too old",pclFunc);
           }
           else
           {
              slCursor = 0;
              slFkt = START;
              dbg(DEBUG,"%s <%s>",pclFunc,pclSqlBuf);
              ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
              close_my_cursor(&slCursor);
              if (ilRCdb == DB_SUCCESS)
              {
                 BuildItemBuffer(pclDataBuf,"",24,",");
                 get_real_item(pcgAftUrno,pclDataBuf,1);
                 get_real_item(pcgFlightSchedTime,pclDataBuf,2);
                 dbg(DEBUG,"%s Found Flight with URNO <%s>",pclFunc,pcgAftUrno);
                 strcpy(pcgAftAdid,"D");
                 strcpy(pcgFlightNumber,pclFltNo);
                 cgTlxStatus = FDI_STAT_FLT_UPDATED;
                 if (igCurTlxUrnoIdx < igMaxTlxUrnoIdx)
                 {
                    strcpy(pcgNextTlxUrno,&pcgTlxUrnoList[igCurTlxUrnoIdx][0]);
                 }
                 else
                 {
                    ilDebugLevel = debug_level;
                    debug_level = 0;
                    /*
                    GetNextValues(pcgNextTlxUrno,1);
                    UFIS-1485
                    */

                    DB_LoadUrnoList(1,pcgNextTlxUrno,pcgTableKey);
                    debug_level = ilDebugLevel;
                 }

                 ilRC = HandlePSC(pclTlxPtr,ilI);
              }
              else
              {
                 cgTlxStatus = FDI_STAT_FLT_NOT_FOUND;
                 dbg(DEBUG,"%s No Flight Found",pclFunc);
              }
           }
           ilRC = RC_SUCCESS;
        }
     }
  }

  if (ilRC == RC_FAIL)
    dbg(DEBUG,"%s : No matching configured PSC format was found", pclFunc);

  return ilRC;
} /* end of CheckTelexInputForPSCData */


static int ConvertDateToCEDA(char *pcpDate, char *pcpFormat)
{
  int ilRC = RC_SUCCESS;
  char *pclTmpPtr;
  char pclTmpBuf[16];
  char pclYear[16];
  char pclMonth[16];
  char pclDay[16];
  char pclCurTim[16];
  char pclCurYear[16];
  char pclCurMonth[16];
  char pclCurDay[16];

  memset(pclTmpBuf,0x00,16);
  memset(pclYear,0x00,16);
  memset(pclMonth,0x00,16);
  memset(pclDay,0x00,16);
  pclTmpPtr = strstr(pcpFormat,"YYYY");
  if (pclTmpPtr != NULL)
     strncpy(pclYear,&pcpDate[pclTmpPtr-pcpFormat],4);
  else
  {
     pclTmpPtr = strstr(pcpFormat,"YY");
     if (pclTmpPtr != NULL)
     {
        strcpy(pclYear,"20");
        strncat(pclYear,&pcpDate[pclTmpPtr-pcpFormat],2);
     }
  }
  pclTmpPtr = strstr(pcpFormat,"MMM");
  if (pclTmpPtr != NULL)
  {
     strncpy(pclTmpBuf,&pcpDate[pclTmpPtr-pcpFormat],3);
     if (strcmp(pclTmpBuf,"JAN") == 0)
        strcpy(pclMonth,"01");
     else if (strcmp(pclTmpBuf,"FEB") == 0)
        strcpy(pclMonth,"02");
     else if (strcmp(pclTmpBuf,"MAR") == 0)
        strcpy(pclMonth,"03");
     else if (strcmp(pclTmpBuf,"APR") == 0)
        strcpy(pclMonth,"04");
     else if (strcmp(pclTmpBuf,"MAY") == 0)
        strcpy(pclMonth,"05");
     else if (strcmp(pclTmpBuf,"JUN") == 0)
        strcpy(pclMonth,"06");
     else if (strcmp(pclTmpBuf,"JUL") == 0)
        strcpy(pclMonth,"07");
     else if (strcmp(pclTmpBuf,"AUG") == 0)
        strcpy(pclMonth,"08");
     else if (strcmp(pclTmpBuf,"SEP") == 0)
        strcpy(pclMonth,"09");
     else if (strcmp(pclTmpBuf,"OCT") == 0)
        strcpy(pclMonth,"10");
     else if (strcmp(pclTmpBuf,"NOV") == 0)
        strcpy(pclMonth,"11");
     else if (strcmp(pclTmpBuf,"DEC") == 0)
        strcpy(pclMonth,"12");
  }
  else
  {
     pclTmpPtr = strstr(pcpFormat,"MM");
     if (pclTmpPtr != NULL)
        strncpy(pclMonth,&pcpDate[pclTmpPtr-pcpFormat],2);
  }
  pclTmpPtr = strstr(pcpFormat,"DD");
  if (pclTmpPtr != NULL)
     strncpy(pclDay,&pcpDate[pclTmpPtr-pcpFormat],2);
  strcpy(pclCurTim,GetTimeStamp());
  strncpy(pclCurDay,&pclCurTim[6],2);
  pclCurDay[2] = '\0';
  strncpy(pclCurMonth,&pclCurTim[4],2);
  pclCurMonth[2] = '\0';
  strncpy(pclCurYear,&pclCurTim[0],4);
  pclCurYear[4] = '\0';
  if (strlen(pclDay) > 0 && strlen(pclMonth) > 0 && strlen(pclYear) > 0)
  {
     sprintf(pcpDate,"%s%s%s",pclYear,pclMonth,pclDay);
  }
  else if (strlen(pclDay) > 0 && strlen(pclMonth) > 0)
  {
     if (strcmp(pclMonth,pclCurMonth) > 0)
        sprintf(pclYear,"%d",atoi(pclCurYear)-1);
     else
        strcpy(pclYear,pclCurYear);
     sprintf(pcpDate,"%s%s%s",pclYear,pclMonth,pclDay);
  }
  else if (strlen(pclDay) > 0)
  {
     if (strcmp(pclDay,pclCurDay) > 0)
        sprintf(pclMonth,"%2.2d",atoi(pclCurMonth)-1);
     else
        strcpy(pclMonth,pclCurMonth);
     if (strcmp(pclMonth,"00") == 0)
        strcpy(pclMonth,"12");
     if (strcmp(pclMonth,pclCurMonth) > 0)
        sprintf(pclYear,"%d",atoi(pclCurYear)-1);
     else
        strcpy(pclYear,pclCurYear);
     sprintf(pcpDate,"%s%s%s",pclYear,pclMonth,pclDay);
  }

  return ilRC;
} /* end of ConvertDateToCEDA */


