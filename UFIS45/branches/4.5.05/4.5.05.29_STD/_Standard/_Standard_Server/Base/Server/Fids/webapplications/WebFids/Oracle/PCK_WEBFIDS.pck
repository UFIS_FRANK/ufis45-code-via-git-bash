CREATE OR REPLACE PACKAGE "PCK_WEBFIDS" AS
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
--      Package              - PACKAGE PCK_WEBFIDS
--
--      Version               - 1.9.0
--
--      Written By            - Jurgen Hammerschmid
--
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
--      Change Log
--
--      Name           Date               Version    Change
--      ----           ----------         -------    ------
--      Hammerschmid   31.03.2008         1.0.0      new
--      Hammerschmid   17.04.2008		  1.1        Several changes in FUN_KEEP_RECORD
--      Hammerschmid   20.04.2008         1.2        Free text only for FLVTAB
--      Hammerschmid   23.04.2008		  1.3        Multiple Airlinecodes include NOT '!' option
--  	Hammerschmid   24.04.2008		  1.4		 Exception handler expanded
--         										     Cursor changed (only group WEBFIDS
--													 will be managed
--  	Hammerschmid   25.04.2008		  1.5		 several tune up changes
--		Hammerschmid   03.07.2008		  1.6        Additional information for Belt display
--  	Hammerschmid   05.08.2008         1.7		 Additional Belt handling PRF 9014
--  	Hammerschmid   07.10.2008		  1.8		 New_time now in CEDA format
--  	Hammerschmid   02.06.2009         1.9        New defaultpage handling
--                                                   FUN_CALCULATE_TIME excluded in
--                                                   seperate function.
--      Hammerschmid   26.10.2009         1.10       FUN_GETUTCOFFSET to get the right
--                                                   offset to summer and winter time
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

  -- Purpose : DisplayHandler

  -- Public type declarations
  --TYPE <TypeName> is <Datatype>;

	CURSOR c_Devices_cur
	IS
        SELECT a.adid dev_adid, a.dadr DevTab_dadr, a.dare DevTab_dare, a.dalz DevTab_dalz, a.dbrf DevTab_dbrf,
            		   a.dcof DevTab_dcof, a.ddgi DevTab_ddgi, a.devn DevTab_devn, a.dfco DevTab_dfco, a.dffd DevTab_dffd,
            		   a.divn DevTab_divn, a.dlct DevTab_dlct, a.dldc DevTab_dldc, a.dldt DevTab_dldt, a.dlrd DevTab_dlrd,
            		   a.dlrt DevTab_dlrt, a.dprt DevTab_dprt, a.drgi DevTab_drgi, a.drna DevTab_drna, a.drnp DevTab_drnp,
            		   a.dter DevTab_dter, a.grpn DevTab_grpn, a.loca DevTab_loca, a.prfl DevTab_prfl, a.rema DevTab_rema,
            		   a.stat DevTab_stat, a.algc DevTab_algc, a.dpid DevTab_dpid,

                	   b.dapn, b.demt, b.dfpn, b.dicf, b.dnop, b.dscf, b.dspf, b.dspt, b.lstu,
                	   b.pi01, b.pi02, b.pi03, b.pi04, b.pi05, b.pi06, b.pi07, b.pi08, b.pi09, b.pi10,
                	   b.pi11, b.pi12, b.pi13, b.pi14, b.pi15, b.pi16, b.pi17, b.pi18, b.pi19, b.pi20,
                	   b.pi21, b.pi22, b.pi23, b.pi24, b.pi25, b.pi26, b.pi27, b.pi28, b.pi29, b.pi30,
                	   b.pi31, b.pi32, b.pi33, b.pi34, b.pi35, b.pi36, b.pi37, b.pi38, b.pi39, b.pi40,
                	   b.pi41, b.pi42, b.pi43, b.pi44, b.pi45, b.pi46, b.pi47, b.pi48, b.pi49, b.pi50,
                	   b.pn01, b.pn02, b.pn03, b.pn04, b.pn05, b.pn06, b.pn07, b.pn08, b.pn09, b.pn10,
                	   b.pn11, b.pn12, b.pn13, b.pn14, b.pn15, b.pn16, b.pn17, b.pn18, b.pn19, b.pn20,
                	   b.pn21, b.pn22, b.pn23, b.pn24, b.pn25, b.pn26, b.pn27, b.pn28, b.pn29, b.pn30,
                	   b.pn31, b.pn32, b.pn33, b.pn34, b.pn35, b.pn36, b.pn37, b.pn38, b.pn39, b.pn40,
                	   b.pn41, b.pn42, b.pn43, b.pn44, b.pn45, b.pn46, b.pn47, b.pn48, b.pn49, b.pn50
    	FROM DEVTAB a, DSPTAB b
    	WHERE a.Dpid = b.Dpid
		AND TRIM(UPPER(a.grpn)) = 'WEBFIDS';

    r_Devices               c_Devices_cur%ROWTYPE;

	r_PAGTAB				PAGTAB%ROWTYPE;

    v_HOPO					SYSTAB.HOPO%TYPE;

	v_UTCOffset				APTTAB.TDIS%TYPE := '0';

  -- Public constant declarations
  --<ConstantName> constant <Datatype> := <Value>;

  -- Public variable declarations
  --<VariableName> <Datatype>;

  -- Public procedure declarations
	PROCEDURE PRC_FIDS_GET_DATA_FOR_DISPLAY;

	PROCEDURE PRC_GET_AIRPORTDESCRIPTION	(p_Code				IN	CHAR,
											 return_ShortName	OUT APTTAB.APSN%TYPE,
											 return_Name1		OUT APTTAB.APFN%TYPE,
											 return_Name2		OUT APTTAB.APN2%TYPE,
											 return_Name3		OUT APTTAB.APN3%TYPE,
											 return_Name4		OUT APTTAB.APN4%TYPE);

	PROCEDURE PRC_GET_REMARK	(p_Code				IN	CHAR,
								 return_Name1		OUT FIDTAB.BEME%TYPE,
								 return_Name2		OUT FIDTAB.BEMD%TYPE,
								 return_Name3		OUT FIDTAB.BET3%TYPE,
								 return_Name4		OUT FIDTAB.BET4%TYPE);

	PROCEDURE PRC_GET_FREE_FIDS_TEXT (p_Urno 		IN WEBFIDSFLVTAB.URNO%TYPE,
			  					      return_Text1  OUT FXTTAB.TEXT%TYPE,
                                      return_Text2  OUT FXTTAB.TEXT%TYPE);

	PROCEDURE PRC_HOUSEKEEPING_JOB (p_HOUROFDAY IN VARCHAR2);

	PROCEDURE PRC_SET_WEBFIDS_SCHEDULER_JOB;

	PROCEDURE PRC_WEBFIDS_SCHEDULER_JOB;

	PROCEDURE PRC_WEBFIDS_JOB;

	PROCEDURE PRC_EXECUTE_WEBFIDS_JOB;

	PROCEDURE PRC_HOUSEKEEPING;

  -- Public function declarations
	FUNCTION FUN_USE_DATA_FROM_FDDTAB	(p_SQLString 		IN VARCHAR2,
										 p_PageNumber		IN DSPTAB.PN01%TYPE)
	RETURN INTEGER;

	FUNCTION FUN_USE_DATA_FROM_FLVTAB	(p_SQLString 		IN VARCHAR2,
										 p_PageNumber		IN DSPTAB.PN01%TYPE)
	RETURN INTEGER;

	FUNCTION FUN_GET_FORMATED_CODESHARE	(p_CodeShare IN AFTTAB.JFNO%TYPE,
										 p_Seperator IN CHAR)
	RETURN AFTTAB.JFNO%TYPE;

	FUNCTION FUN_KEEP_RECORD			(p_Airborn			IN FDDTAB.AIRB%TYPE,
										 p_Offblock			IN FDDTAB.ONBL%TYPE,
										 p_Land				IN FDDTAB.LAND%TYPE,
										 p_Onblock			IN FDDTAB.OFBL%TYPE,
										 p_TimeOfFlight		IN FDDTAB.STOF%TYPE,
										 p_Tiff				IN FDDTAB.TIFF%TYPE)
	RETURN INTEGER;

	FUNCTION FUN_CHECK_CHECKIN			(p_Type				IN FLVTAB.CTY1%TYPE,
										 p_DisplaySequence	IN PAGTAB.PDSS%TYPE,
										 p_FlightLogo		IN FLVTAB.FLG2%TYPE)
	RETURN INTEGER;

	FUNCTION FUN_CHECK_GATE				(p_Type				IN FLVTAB.CTY1%TYPE,
										 p_DisplaySequence	IN PAGTAB.PDSS%TYPE,
										 p_FlightLogo		IN FLVTAB.FLG2%TYPE)
	RETURN INTEGER;

	FUNCTION FUN_CHECK_BELT				(p_NumberOfRecords	IN	NUMBER,
										 p_DisplaySequence	IN PAGTAB.PDSS%TYPE,
                                         p_DisplayType  	IN PAGTAB.pdpt%TYPE)
	RETURN INTEGER;

	FUNCTION FUN_AIRLINES_FOR_DISPLAY   (p_TableName	IN VARCHAR2,
    									 p_AFTUrno      IN NUMBER,
    									 p_Airlinecodes IN DEVTAB.ALGC%TYPE)
    RETURN INTEGER;

	FUNCTION FUN_SET_ADVERTISEMENT_PAGE	(p_SQLString 		IN VARCHAR2,
										 p_PageNumber		IN DSPTAB.PN01%TYPE)
	RETURN INTEGER;

    FUNCTION FUN_SET_DEFAULT_FOR_FDD
	RETURN INTEGER;

	FUNCTION FUN_SET_DEFAULT_FOR_FLV
	RETURN INTEGER;

	FUNCTION FUN_GET_LOCALTIME			(p_TIMEIN IN VARCHAR,
										 p_HOPO IN VARCHAR)
   	RETURN VARCHAR;

	FUNCTION FUN_GET_UTCOFFSET			(p_TIMEIN IN VARCHAR,  -- in CEDAFORMAT
										 p_HOPO IN VARCHAR)
   	RETURN VARCHAR2;

	FUNCTION FUN_GET_NEXT_SEQUENCE_ID
	RETURN NUMBER;

	FUNCTION FUN_TRUNCATE_TABLE (p_Table IN VARCHAR2)
	RETURN INTEGER;


END PCK_WEBFIDS;
/
CREATE OR REPLACE PACKAGE BODY "PCK_WEBFIDS" AS

-------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------
--  Package Body      - PCK_WEBFIDS
--
--  Version           - 1.9.0
--
--  Written By        - Juergen Hammerschmid
--
-------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------
--  Change Log
--
--  Name           Date               Version    Change
--  ----           ----------         -------    ------
--  Hammerschmid   31.03.2008         1.0        new
--  Hammerschmid   17.04.2008		  1.1        Several changes in FUN_KEEP_RECORD
--  Hammerschmid   20.04.2008         1.2        Free text only for FLVTAB
--  Hammerschmid   23.04.2008		  1.3        Multiple Airlinecodes include NOT '!' option
--  Hammerschmid   24.04.2008		  1.4		 Exception handler expanded
--  Hammerschmid   27.04.2008		  1.5		 several tune up changes
--  Hammerchhmid   03.07.2008         1.6        New Belt handling
--  Hammerschmid   05.08.2008         1.7		 Additional Belt handling PRF 9014
--  Hammerschmid   07.10.2008		  1.8		 New_time now in CEDA format
-- 	Hammerschmid   02.06.2009         1.9        New defaultpage handling
--                                               FUN_CALCULATE_TIME excluded in
--                                               seperate function.
--  Hammerschmid   26.10.2009         1.10       FUN_GETUTCOFFSET to get the right
--                                               offset to summer and winter time
-------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------------
--      Procedure              - PRC_FIDS_GET_DATA_FOR_DISPLAY
-------------------------------------------------------------------------------------------
	PROCEDURE PRC_FIDS_GET_DATA_FOR_DISPLAY
	IS
    ---------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------
    --      Procedure              - PRC_FIDS_GET_DATA_FOR_DISPLAY
    --
    --      Version               - 1.1.0
    --
    --      Written By            - Juergen Hammerschmid
    --
    --      Errors                - Errors will be written into the table FIDS_ERRORS
    --
    ---------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------
    --      Change Log
    --
    --      Name           Date               Version    Change
    --      ----           ----------         -------    ------
	--      Hammerschmid   31.03.2008         1.0        new
	--      Hammerschmid   12.04.2008         1.1        Advertisment pages included
	--  	Hammerschmid   24.04.2008		  1.2		 Exception handler expanded
    --      Hammerschmid   05.08.2008         1.3        New Handling for Belts
    ---------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------

	TYPE Page_type 			is TABLE OF DSPTAB.PI01%TYPE INDEX BY PLS_INTEGER;
	TYPE PageNumber_type 	is TABLE OF DSPTAB.PN01%TYPE INDEX BY PLS_INTEGER;

	v_Page 					Page_type;
	v_PageNumber 			PageNumber_type;
	v_PageElements			INTEGER;

    v_Error                 INTEGER;
    v_FDD_SQLString         varchar2(500);
    v_FLV_SQLString         varchar2(500);
    v_FDV_SQLString			varchar2(500);

	--------------------------------------------------
	-- Hardcodeparameter for display codshare flights
	--------------------------------------------------
	v_DisplayCodeShareAsSingle	BOOLEAN := TRUE;
	--------------------------------------------------
	-- Hardcodeparameter for display codshare flights
	--------------------------------------------------

    BEGIN

		BEGIN
            -- Get HOPO
            SELECT HOPO
            INTO v_HOPO
            FROM SYSTAB
            WHERE ROWNUM = 1;

/* old version
			-- Get UTCOffset used in
            -- several procedures and functions
            -- definition in body
			SELECT TDIS
			INTO v_UTCOffset
			FROM APTTAB
			WHERE APC3=HOPO;
*/
		EXCEPTION
			WHEN OTHERS THEN
				v_UTCOffset := '0';
		END;

    	UPDATE WEBFIDS_TABLE_TEMP
    	SET Status = '';
    	COMMIT;

        OPEN c_Devices_cur;
        LOOP
       		-- Process data
            FETCH c_Devices_cur INTO r_Devices;
            EXIT WHEN c_Devices_cur%NOTFOUND OR
        			  c_Devices_cur%NOTFOUND IS NULL;

           	-- retrieve page informations from PAGTAB
           	FOR i IN 1..50 LOOP
           		v_Page(i) := '';
           		v_PageNumber(i) := '';
           		CASE
           	    	WHEN i = 1 THEN v_Page(i) := r_Devices.Pi01; v_PageNumber(i) := r_Devices.PN01;
           	    	WHEN i = 2 THEN v_Page(i) := r_Devices.Pi02; v_PageNumber(i) := r_Devices.PN02;
           	    	WHEN i = 3 THEN v_Page(i) := r_Devices.Pi03; v_PageNumber(i) := r_Devices.PN03;
           	    	WHEN i = 4 THEN v_Page(i) := r_Devices.Pi04; v_PageNumber(i) := r_Devices.PN04;
           	    	WHEN i = 5 THEN v_Page(i) := r_Devices.Pi05; v_PageNumber(i) := r_Devices.PN05;
           	    	WHEN i = 6 THEN v_Page(i) := r_Devices.Pi06; v_PageNumber(i) := r_Devices.PN06;
           	    	WHEN i = 7 THEN v_Page(i) := r_Devices.Pi07; v_PageNumber(i) := r_Devices.PN07;
           	    	WHEN i = 8 THEN v_Page(i) := r_Devices.Pi08; v_PageNumber(i) := r_Devices.PN08;
           	    	WHEN i = 9 THEN v_Page(i) := r_Devices.Pi09; v_PageNumber(i) := r_Devices.PN09;
           	    	WHEN i = 10 THEN v_Page(i) := r_Devices.Pi10; v_PageNumber(i) := r_Devices.PN10;
           	    	WHEN i = 11 THEN v_Page(i) := r_Devices.Pi11; v_PageNumber(i) := r_Devices.PN11;
           	    	WHEN i = 12 THEN v_Page(i) := r_Devices.Pi12; v_PageNumber(i) := r_Devices.PN12;
           	    	WHEN i = 13 THEN v_Page(i) := r_Devices.Pi13; v_PageNumber(i) := r_Devices.PN13;
           	    	WHEN i = 14 THEN v_Page(i) := r_Devices.Pi14; v_PageNumber(i) := r_Devices.PN14;
           	    	WHEN i = 15 THEN v_Page(i) := r_Devices.Pi15; v_PageNumber(i) := r_Devices.PN15;
           	    	WHEN i = 16 THEN v_Page(i) := r_Devices.Pi16; v_PageNumber(i) := r_Devices.PN16;
           	    	WHEN i = 17 THEN v_Page(i) := r_Devices.Pi17; v_PageNumber(i) := r_Devices.PN17;
           	    	WHEN i = 18 THEN v_Page(i) := r_Devices.Pi18; v_PageNumber(i) := r_Devices.PN18;
           	    	WHEN i = 19 THEN v_Page(i) := r_Devices.Pi19; v_PageNumber(i) := r_Devices.PN19;
           	    	WHEN i = 20 THEN v_Page(i) := r_Devices.Pi20; v_PageNumber(i) := r_Devices.PN20;
           	    	WHEN i = 12 THEN v_Page(i) := r_Devices.Pi12; v_PageNumber(i) := r_Devices.PN21;
           	    	WHEN i = 22 THEN v_Page(i) := r_Devices.Pi22; v_PageNumber(i) := r_Devices.PN22;
           	    	WHEN i = 23 THEN v_Page(i) := r_Devices.Pi23; v_PageNumber(i) := r_Devices.PN23;
           	    	WHEN i = 24 THEN v_Page(i) := r_Devices.Pi24; v_PageNumber(i) := r_Devices.PN24;
           	    	WHEN i = 25 THEN v_Page(i) := r_Devices.Pi25; v_PageNumber(i) := r_Devices.PN25;
           	    	WHEN i = 26 THEN v_Page(i) := r_Devices.Pi26; v_PageNumber(i) := r_Devices.PN26;
           	    	WHEN i = 27 THEN v_Page(i) := r_Devices.Pi27; v_PageNumber(i) := r_Devices.PN27;
           	    	WHEN i = 28 THEN v_Page(i) := r_Devices.Pi28; v_PageNumber(i) := r_Devices.PN28;
           	    	WHEN i = 29 THEN v_Page(i) := r_Devices.Pi29; v_PageNumber(i) := r_Devices.PN29;
           	    	WHEN i = 30 THEN v_Page(i) := r_Devices.Pi30; v_PageNumber(i) := r_Devices.PN30;
           	    	WHEN i = 31 THEN v_Page(i) := r_Devices.Pi31; v_PageNumber(i) := r_Devices.PN31;
           	    	WHEN i = 32 THEN v_Page(i) := r_Devices.Pi32; v_PageNumber(i) := r_Devices.PN32;
           	    	WHEN i = 33 THEN v_Page(i) := r_Devices.Pi33; v_PageNumber(i) := r_Devices.PN33;
           	    	WHEN i = 34 THEN v_Page(i) := r_Devices.Pi34; v_PageNumber(i) := r_Devices.PN34;
           	    	WHEN i = 35 THEN v_Page(i) := r_Devices.Pi35; v_PageNumber(i) := r_Devices.PN45;
           	    	WHEN i = 36 THEN v_Page(i) := r_Devices.Pi36; v_PageNumber(i) := r_Devices.PN36;
           	    	WHEN i = 37 THEN v_Page(i) := r_Devices.Pi37; v_PageNumber(i) := r_Devices.PN37;
           	    	WHEN i = 38 THEN v_Page(i) := r_Devices.Pi38; v_PageNumber(i) := r_Devices.PN38;
           	    	WHEN i = 39 THEN v_Page(i) := r_Devices.Pi39; v_PageNumber(i) := r_Devices.PN39;
           	    	WHEN i = 40 THEN v_Page(i) := r_Devices.Pi40; v_PageNumber(i) := r_Devices.PN40;
           	    	WHEN i = 41 THEN v_Page(i) := r_Devices.Pi41; v_PageNumber(i) := r_Devices.PN41;
           	    	WHEN i = 42 THEN v_Page(i) := r_Devices.Pi42; v_PageNumber(i) := r_Devices.PN42;
           	    	WHEN i = 43 THEN v_Page(i) := r_Devices.Pi43; v_PageNumber(i) := r_Devices.PN34;
           	    	WHEN i = 44 THEN v_Page(i) := r_Devices.Pi44; v_PageNumber(i) := r_Devices.PN44;
           	    	WHEN i = 45 THEN v_Page(i) := r_Devices.Pi45; v_PageNumber(i) := r_Devices.PN45;
           	    	WHEN i = 46 THEN v_Page(i) := r_Devices.Pi46; v_PageNumber(i) := r_Devices.PN46;
           	    	WHEN i = 47 THEN v_Page(i) := r_Devices.Pi47; v_PageNumber(i) := r_Devices.PN47;
           	    	WHEN i = 48 THEN v_Page(i) := r_Devices.Pi48; v_PageNumber(i) := r_Devices.PN48;
           	    	WHEN i = 49 THEN v_Page(i) := r_Devices.Pi49; v_PageNumber(i) := r_Devices.PN49;
           	    	WHEN i = 50 THEN v_Page(i) := r_Devices.Pi50; v_PageNumber(i) := r_Devices.PN50;
           		END CASE;
           		IF length(trim(v_Page(i))) is NULL THEN
           			v_PageElements := i - 1;
           			-- Dbms_Output.Put_Line('PAGES FOUND = ' || v_PageElements);
           			EXIT;
           		END IF;
           	END LOOP;

			FOR i in 1..v_PageElements LOOP
            	-- JHA20080424
            	BEGIN
	       			SELECT *
	   				INTO r_PAGTAB
	       			FROM PAGTAB
	       			WHERE pagi = v_Page(i);
				EXCEPTION
                	WHEN NO_DATA_FOUND THEN
						pck_ceda_errors.prc_write_error('PRC_FIDS_GET_DATA_FOR_DISPLAY ', 'FIDS-10007',
 	                    		   'No data found in PAGTAB for page: ' || v_Page(i), 1);
                    WHEN OTHERS THEN
						pck_ceda_errors.prc_write_error('PRC_FIDS_GET_DATA_FOR_DISPLAY ', 'FIDS-10008',
 	                    		   'No data found in PAGTAB for page: ' || v_Page(i) || ' when others', 1);
                END;

   				-- Build dynamic SQL-Statement
   				v_FDD_SQLString := '';
                v_FLV_SQLString := '';
                v_FDV_SQLString := '';

            	-- JHA20080424
				BEGIN
                	v_Error := 0;
					-- Handle Codeshare
					IF v_DisplayCodeShareAsSingle = TRUE THEN
                   		IF length(v_FDD_SQLString) > 0 THEN
	                    	v_Error := 1;
    	           			v_FDD_SQLString := v_FDD_SQLString || ' AND FDDTAB.DSEQ between 0 and 1 ';
                   			v_FDV_SQLString := v_FDV_SQLString || ' AND FDVTAB.DSEQ between 0 and 1 ';
                   		ELSE
	                    	v_Error := 2;
	                   		v_FDD_SQLString := 'WHERE FDDTAB.DSEQ between 0 and 1 ';
                   			v_FDV_SQLString := 'WHERE FDVTAB.DSEQ between 0 and 1 ';
                   		END IF;
	/*					-- Codeshare as single record for FLVTAB
                   		IF length(v_FLV_SQLString) > 0 THEN
                   			v_FLV_SQLString := v_FLV_SQLString || ' AND FLVTAB.DSEQ between 0 and 1 ';
                   		ELSE
                   			v_FLV_SQLString := 'WHERE FLVTAB.DSEQ between 0 and 1 ';
	                   	END IF;
	*/
					END IF;

					IF trim(r_Devices.DevTab_dare) is not null THEN
            	       	IF length(v_FDD_SQLString) > 0 THEN
	                    	v_Error := 3;
                	   		v_FDD_SQLString := v_FDD_SQLString || ' AND FDDTAB.DACO = ''' || trim(r_Devices.DevTab_dare) || '''';
                   			v_FDV_SQLString := v_FDV_SQLString || ' AND FDVTAB.DACO = ''' || trim(r_Devices.DevTab_dare) || '''';
	                   	ELSE
	                    	v_Error := 4;
    	               		v_FDD_SQLString := 'WHERE FDDTAB.DACO = ''' || trim(r_Devices.DevTab_dare) || '''';
        	           		v_FDV_SQLString := 'WHERE FDVTAB.DACO = ''' || trim(r_Devices.DevTab_dare) || '''';
            	       	END IF;
                	   	-- Dbms_Output.Put_Line('FDDTAB.DACO = DEVTAB.DARE ' || r_Devices.dare);
					END IF;

					IF trim(r_Devices.DevTab_dfco) is not null THEN
        	           	IF length(v_FDD_SQLString) > 0 THEN
	                    	v_Error := 5;
            	       		v_FDD_SQLString := v_FDD_SQLString || ' AND FDDTAB.FTYP = ''' || trim(r_Devices.DevTab_dfco) || '''';
                	   		v_FDV_SQLString := v_FDV_SQLString || ' AND FDVTAB.' || trim(r_Devices.DevTab_dffd) || ' = ''' || trim(r_Devices.DevTab_dfco) || '''';
                   		ELSE
	                    	v_Error := 6;
	                  		v_FDD_SQLString := 'WHERE FDDTAB.FTYP = ''' || trim(r_Devices.DevTab_dfco) || '''';
    	              		v_FDV_SQLString := 'WHERE FDVTAB.' || trim(r_Devices.DevTab_dffd) || ' = ''' || trim(r_Devices.DevTab_dfco) || '''';
        	           	END IF;
            	       	IF length(v_FLV_SQLString) > 0 THEN
	                    	v_Error := 7;
                	   		v_FLV_SQLString := v_FLV_SQLString || ' AND FLVTAB.RNAM = ''' || trim(r_Devices.DevTab_dfco) || '''';
	                   	ELSE
	                    	v_Error := 8;
    	              		v_FLV_SQLString := 'WHERE FLVTAB.RNAM = ''' || trim(r_Devices.DevTab_dfco) || '''';
        	           	END IF;
            	       	-- Dbms_Output.Put_Line('FDDTAB.FTYP = DEVTAB.DFCO ' || trim(r_Devices.dfco));
					END IF;

					IF trim(r_Devices.DevTab_dter) is not null THEN
    	               	IF length(v_FDD_SQLString) > 0 THEN
	                    	v_Error := 9;
        	           		v_FDD_SQLString := v_FDD_SQLString || ' AND FDDTAB.INDO = ''' || trim(r_Devices.DevTab_dter) || '''';
            	       		v_FDV_SQLString := v_FDV_SQLString || ' AND FDVTAB.INDO = ''' || trim(r_Devices.DevTab_dter) || '''';
                	   	ELSE
	                    	v_Error := 10;
                   			v_FDD_SQLString := 'WHERE FDDTAB.INDO = ''' || trim(r_Devices.DevTab_dter) || '''';
                   			v_FDV_SQLString := 'WHERE FDVTAB.INDO = ''' || trim(r_Devices.DevTab_dter) || '''';
	                   	END IF;
    	               	-- Dbms_Output.Put_Line('FDDTAB.INDO = DEVTAB.DTER ' || trim(r_Devices.dter));
					END IF;

	   				IF length(v_FDD_SQLSTring) > 0 THEN
                    	v_Error := 11;
    	           		v_FDD_SQLString := v_FDD_SQLString || ' AND FDDTAB.ADID = ''' || trim(r_PAGTAB.Pdpt) || '''';
        	       		v_FDV_SQLString := v_FDV_SQLString || ' AND FDVTAB.ADID = ''' || trim(r_PAGTAB.Pdpt) || '''';
   					ELSE
                    	v_Error := 12;
               			v_FDD_SQLString := 'WHERE FDDTAB.ADID = ''' || trim(r_PAGTAB.Pdpt) || '''';
               			v_FDV_SQLString := 'WHERE FDVTAB.ADID = ''' || trim(r_PAGTAB.Pdpt) || '''';
	   				END IF;

	   				IF trim(r_PAGTAB.Pfnt) = 'P' THEN
    	               	IF length(v_FDD_SQLString) > 0 THEN
	                    	v_Error := 13;
   	    	           		v_FDD_SQLString := v_FDD_SQLString || ' AND FDDTAB.DNAT = ''P''';
   	        	       		v_FDV_SQLString := v_FDV_SQLString || ' AND FDVTAB.DNAT = ''P''';
   						ELSE
	                    	v_Error := 14;
	   	               		v_FDD_SQLString := v_FDD_SQLString || 'WHERE FDDTAB.DNAT = ''P''';
   		               		v_FDV_SQLString := v_FDV_SQLString || 'WHERE FDVTAB.DNAT = ''P''';
   						END IF;
   					ELSIF trim(r_PAGTAB.Pfnt) = 'S' THEN
                	   	IF length(v_FDD_SQLString) > 0 THEN
	                    	v_Error := 15;
   	               			v_FDD_SQLString := v_FDD_SQLString || ' AND FDDTAB.DNAT IN (''P'',''S'')';
   	               			v_FDV_SQLString := v_FDV_SQLString || ' AND FDVTAB.DNAT IN (''P'',''S'')';
	   					ELSE
	                    	v_Error := 16;
   		               		v_FDD_SQLString := v_FDD_SQLString || 'WHERE FDDTAB.DNAT IN (''P'',''S'')';
   	    	           		v_FDV_SQLString := v_FDV_SQLString || 'WHERE FDVTAB.DNAT IN (''P'',''S'')';
   						END IF;
	   				END IF;
				EXCEPTION
                	WHEN OTHERS THEN
						pck_ceda_errors.prc_write_error('PRC_FIDS_GET_DATA_FOR_DISPLAY ', 'FIDS-10006',
   	                       			'Error-Code: ' || TO_CHAR(SQLCODE) || ' Error-Message: ' ||
						   			SUBSTR(SQLERRM, 1, 150) || ' in building dynamic SQL ' || to_char(v_Error), 1);
                END;
       			-- Check where to put data
       			-- FDVTAB is only a view which combines data from FDDTAB and AFTTAB
       			-- except field stype which will be decoded
       			-- if DEVTAB.DFFD = FTYP then data from FDDTAB else from FLVTAB (view)

                v_Error := -1;

				CASE
                	WHEN r_Devices.DevTab_Dffd = 'FTYP' THEN
						-- use data from fddtab
						IF length(v_FDD_SQLString) > 0 THEN
        		           	v_FDD_SQLString := 'SELECT * FROM FDDTAB ' || trim(v_FDD_SQLString);
						ELSE
                   			v_FDD_SQLString := 'SELECT * FROM FDDTAB';
						END IF;

						IF trim(r_PAGTAB.Pdpt) != 'X' THEN	-- X = Advertisment page -> no flight data

							v_Error := FUN_USE_DATA_FROM_FDDTAB (v_FDD_SQLString, v_Pagenumber(i));

							IF v_Error != -1 THEN	-- Error check Error Code
								pck_ceda_errors.prc_write_error('PRC_FIDS_GET_DATA_FOR_DISPLAY ', 'FIDS-10000',
        	                    		   'Insert error in WEBFIDS_TABLE FDDTAB: Error Code = ' || v_Error, 1);
							END IF;
                        ELSE
                        	-- JHA 12.04.2008 included
                        	v_Error := FUN_SET_ADVERTISEMENT_PAGE(v_FDD_SQLString, v_Pagenumber(i));
							IF v_Error != -1 THEN	-- Error check Error Code
								pck_ceda_errors.prc_write_error('PRC_FIDS_GET_DATA_FOR_DISPLAY ', 'FIDS-10000',
        	                    		   'Insert error in WEBFIDS_TABLE FDDTAB (X): Error Code = ' || v_Error, 1);
                            END IF;
						END IF;
                	WHEN trim(r_Devices.DevTab_Dffd) = 'BAZ1' THEN
                    	-- use data from fdvtab
						IF length(v_FDV_SQLString) > 0 THEN
        		           	v_FDV_SQLString := 'SELECT * FROM FDVTAB ' || trim(v_FDV_SQLString);
						ELSE
                   			v_FDV_SQLString := 'SELECT * FROM FDVTAB';
						END IF;

						IF trim(r_PAGTAB.Pdpt) != 'X' THEN	-- X = Advertisment page -> no flight data

							/* JHA 20080407
                            v_Error := FUN_USE_DATA_FROM_FDVTAB (v_FDV_SQLString, v_Pagenumber(i));
                            */
							v_Error := FUN_USE_DATA_FROM_FDDTAB (v_FDV_SQLString, v_Pagenumber(i));

							IF v_Error != -1 THEN	-- Error check Error Code
								pck_ceda_errors.prc_write_error('PRC_FIDS_GET_DATA_FOR_DISPLAY ', 'FIDS-10000',
        	                    		   'Insert error in WEBFIDS_TABLE FDVTAB: Error Code = ' || v_Error, 1);
							END IF;
						END IF;
                    -- JHA 20080805 PRF 9014 filter for domestic/international flights for BELT
                    WHEN trim(r_Devices.DevTab_Dffd) = 'STEV' THEN
                    	-- use data from flvtab
                    	IF trim(r_Pagtab.PDTI) = 'U' THEN
                          v_FLV_SQLString := 'SELECT * FROM FLVTAB WHERE FLVTAB.RTYP = ' || '''BELT''' ||
											' AND FLVTAB.' || trim(r_Devices.DevTab_Dffd) || ' = ''' || trim(r_Devices.DevTab_Dfco) || '''';
							v_Error := FUN_USE_DATA_FROM_FLVTAB (v_FLV_SQLString, v_Pagenumber(i));

							IF v_Error != -1 THEN	-- Error check Error Code
								pck_ceda_errors.prc_write_error('PRC_FIDS_GET_DATA_FOR_DISPLAY ', 'FIDS-10000',
										   'Insert error in WEBFIDS_TABLE FLVTAB: Error Code = ' || v_Error, 1);
							END IF;
                        END IF;
					WHEN trim(r_Devices.DevTab_Dffd) = 'FLTI' THEN
                    	-- use data from flvtab
                    	IF trim(r_Pagtab.PDTI) = 'U' THEN
                          v_FLV_SQLString := 'SELECT * FROM FLVTAB WHERE FLVTAB.RTYP = ' || '''BELT''' ||
											' AND FLVTAB.' || trim(r_Devices.DevTab_Dffd) || ' = ''' || trim(r_Devices.DevTab_Dfco) || '''';
							v_Error := FUN_USE_DATA_FROM_FLVTAB (v_FLV_SQLString, v_Pagenumber(i));

							IF v_Error != -1 THEN	-- Error check Error Code
								pck_ceda_errors.prc_write_error('PRC_FIDS_GET_DATA_FOR_DISPLAY ', 'FIDS-10000',
										   'Insert error in WEBFIDS_TABLE FLVTAB: Error Code = ' || v_Error, 1);
							END IF;
                        END IF;
                    -- End JHA 20080805
                    ELSE
                    	-- use data from flvtab
						IF length(v_FLV_SQLString) > 0 THEN

    	               		v_FLV_SQLString := 'SELECT * FROM FLVTAB ' || trim(v_FLV_SQLString) ||
        	                				   ' AND FLVTAB.RTYP = ''' || trim(r_Devices.DevTab_Dffd) || '''';

							v_Error := FUN_USE_DATA_FROM_FLVTAB (v_FLV_SQLString, v_Pagenumber(i));

							IF v_Error != -1 THEN	-- Error check Error Code
								pck_ceda_errors.prc_write_error('PRC_FIDS_GET_DATA_FOR_DISPLAY ', 'FIDS-10000',
										   'Insert error in WEBFIDS_TABLE FLVTAB: Error Code = ' || v_Error, 1);
							END IF;
						-- JHA 20080703
                        ELSE
                        	v_FLV_SQLString := 'SELECT * FROM FLVTAB WHERE RTYP = ''BELT''';

							v_Error := FUN_USE_DATA_FROM_FLVTAB (v_FLV_SQLString, v_Pagenumber(i));
							IF v_Error != -1 THEN	-- Error check Error Code
								pck_ceda_errors.prc_write_error('PRC_FIDS_GET_DATA_FOR_DISPLAY ', 'FIDS-10000',
										   'Insert error in WEBFIDS_TABLE FLVTAB: Error Code = ' || v_Error, 1);
							END IF;
                        -- End JHA 20080703
						END IF;

						IF v_Error != -1 THEN	-- Error check Error Code
							pck_ceda_errors.prc_write_error('PRC_FIDS_GET_DATA_FOR_DISPLAY ', 'FIDS-10000',
        	                   		   'Insert error in WEBFIDS_TABLE FLVTAB: Error Code = ' || v_Error, 1);
						END IF;
                END CASE;
   			END LOOP;  -- Page Elements

    	END LOOP;	-- Devices
		CLOSE c_Devices_cur;

        -- Clean up WEBFIDS_TABLE_TEMP
		-- DELETE Original WEBFIDS_TABLE
        -- INSERT FROM WEBFIDS_TABLE_TEMP
        -- COMMIT
        BEGIN
        	DELETE WEBFIDS_TABLE_TEMP
        	WHERE trim(Status) is NULL;
        	DELETE WEBFIDS_TABLE;
        	INSERT INTO WEBFIDS_TABLE
        		(SELECT * FROM WEBFIDS_TABLE_TEMP);
        	COMMIT;
        EXCEPTION
        	WHEN OTHERS THEN
        		ROLLBACK;
				pck_ceda_errors.prc_write_error('PRC_FIDS_GET_DATA_FOR_DISPLAY ', 'FIDS-80000',
                               'Error-Code: ' || TO_CHAR(SQLCODE) || ' Error-Message: ' ||
							    SUBSTR(SQLERRM, 1, 150) || ' WEBFIDS_TABLE not valid', 1);
        END;

    EXCEPTION	-- Devices
    	WHEN no_data_found THEN
			IF c_Devices_cur%ISOPEN = TRUE THEN
				CLOSE c_Devices_cur;
			END IF;
			pck_ceda_errors.prc_write_error('PRC_FIDS_GET_DATA_FOR_DISPLAY ', 'FIDS-80001',
   	                       'Error-Code: ' || TO_CHAR(SQLCODE) || ' Error-Message: ' ||
						   SUBSTR(SQLERRM, 1, 150) || ' in Devices', 1);
    	WHEN others THEN
			IF c_Devices_cur%ISOPEN = TRUE THEN
				CLOSE c_Devices_cur;
			END IF;
			pck_ceda_errors.prc_write_error('PRC_FIDS_GET_DATA_FOR_DISPLAY ', 'FIDS-80000',
                           'Error-Code: ' || TO_CHAR(SQLCODE) || ' Error-Message: ' ||
						   SUBSTR(SQLERRM, 1, 150) || ' in Devices when others', 1);

    END PRC_FIDS_GET_DATA_FOR_DISPLAY;

-------------------------------------------------------------------------------------------
--      Function              - FUN_USE_DATA_FROM_FDDTAB
-------------------------------------------------------------------------------------------

	FUNCTION FUN_USE_DATA_FROM_FDDTAB (p_SQLString IN VARCHAR2,
									   p_PageNumber IN DSPTAB.PN01%TYPE)
	RETURN INTEGER
	IS
    ---------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------
    --      Function              - FUN_USE_DATA_FROM_FDDTAB
    --
    --      Version               - 1.2.0
    --
    --      Written By            - Juergen Hammerschmid
    --
    --      Return                -1 = OK
	--
    --      Errors                - Errors will be written into the table FIDS_ERRORS
    --
    ---------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------
    --      Change Log
    --
    --      Name           Date               Version    Change
    --      ----           ----------         -------    ------
	--      Hammerschmid   31.03.2008         1.0        new
    --      Hammerschmid   02.04.2008		  1.1		 Multiple Airlinecodes
    --		Hammerschmid   23.04.2008         1.2        FDDTAB replaced with view
    ---------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------
    TYPE WEBFIDSFDDFDVtabType         IS REF CURSOR;
    v_WEBFIDSFDDFDVTAB_cur            WEBFIDSFDDFDVTABType;
    r_WEBFIDSFDDFDVTAB                WEBFIDSFDDFDVTAB%ROWTYPE;

	v_CodeShare				AFTTAB.jfno%TYPE;

    v_Text1					FXTTAB.text%TYPE;
    v_Text2					FXTTAB.text%TYPE;

	v_Seq_id 				NUMBER(22);

	v_DataFound				BOOLEAN := FALSE;
	v_KeepRecord			INTEGER := -1;

	v_RecordsFrom			INTEGER := 1;
	v_RecordsTo				INTEGER := 1;
	v_ActualRecord			INTEGER := 0;

	v_SQLString				VARCHAR2(500);

    v_PageStart				VARCHAR2(10);

    success                 INTEGER := -1;

	BEGIN
		-- count or select records to display;
        -- check and set default value
        IF trim(r_Devices.Devtab_Drnp) is NULL THEN
        	v_PageStart := '1';
        ELSE
        	v_PageStart := trim(r_Devices.Devtab_Drnp);
        END IF;
		IF v_PageStart = '1' THEN
			v_RecordsFrom := 1;
		ELSE
			v_RecordsFrom := (to_number(v_PageStart) * trunc(to_number(trim(r_PAGTAB.Pnof),'999.99'))) - trunc(to_number(trim(r_PAGTAB.Pnof),'999.99')) + 1;
		END IF;

		v_RecordsTo := to_number(v_PageStart) * trunc(to_number(trim(r_PAGTAB.Pnof),'999.99'));

		-- Set ORDER BY STATEMENT FOR FDDTAB
		IF trim(r_PAGTAB.PDSS) = 'S' THEN
			v_SQLString := p_SQLString || ' ORDER BY STOF,FLNO';
		ELSIF trim(r_PAGTAB.PDSS) = 'A' THEN
			v_SQLString := p_SQLString || ' ORDER BY TIFF,FLNO';
		ELSE
			v_SQLString := p_SQLString || ' ORDER BY STOF,FLNO';
		END IF;

        -- JHA 20080403
        -- Replace string with new definition (fddtab and fdvtab no longer used)
        -- DO NOT CHANGE THE ORDER OF THE NEXT TWO PROGRAM LINES
        v_SQLString := replace(v_SQLString,'FDVTAB','WEBFIDSFDDFDVTAB');
        v_SQLString := replace(v_SQLString,'FDDTAB','WEBFIDSFDDFDVTAB');

		OPEN v_WEBFIDSFDDFDVTAB_cur FOR v_SQLString;
		LOOP
			-- Process data
			FETCH v_WEBFIDSFDDFDVTAB_cur INTO r_WEBFIDSFDDFDVTAB;
				EXIT WHEN v_WEBFIDSFDDFDVTAB_cur%NOTFOUND OR
						  v_WEBFIDSFDDFDVTAB_cur%NOTFOUND IS NULL;

			v_DataFound := TRUE;

			BEGIN
				BEGIN

					v_CodeShare := fun_get_formated_CodeShare(trim(r_WEBFIDSFDDFDVTAB.Jfno),',');

					v_KeepRecord := FUN_KEEP_RECORD(trim(r_WEBFIDSFDDFDVTAB.Airb),
													trim(r_WEBFIDSFDDFDVTAB.Ofbl),
													trim(r_WEBFIDSFDDFDVTAB.Land),
													trim(r_WEBFIDSFDDFDVTAB.Onbl),
													trim(r_WEBFIDSFDDFDVTAB.Stof),
													trim(r_WEBFIDSFDDFDVTAB.Tiff));
					IF v_KeepRecord = -1 THEN

                    	-- Check for special Displays showing only
                        -- configured airlines
/* -- JHA 20080403
                        IF TRIM(r_Devices.Devtab_Algc) IS NOT NULL AND
                        	v_AFTRecordFound = TRUE THEN
                        	v_KeepRecord := FUN_AIRLINES_FOR_DISPLAY(r_WEBFIDSFDDFDVTAB.aurn,r_Devices.Devtab_Algc);
                        END IF;
*/
                        IF TRIM(r_Devices.Devtab_Algc) IS NOT NULL THEN
                        	v_KeepRecord := FUN_AIRLINES_FOR_DISPLAY('WEBFIDSFDDFDVTAB',
                            										 r_WEBFIDSFDDFDVTAB.aurn,
                                                                     r_Devices.Devtab_Algc);
                        END IF;

                        IF v_KeepRecord = -1 THEN
							v_ActualRecord := v_ActualRecord + 1;

							IF v_ActualRecord > v_RecordsTo THEN
								EXIT;
							END IF;
							--VIA_AIRPORT wech
							IF v_ActualRecord between v_RecordsFrom and v_RecordsTo THEN
								-- Default page number handling
								-- No default page if flight data exists
								IF p_PageNumber != r_Devices.Demt THEN
									v_Seq_id := fun_get_next_sequence_ID;
									INSERT INTO WEBFIDS_TABLE_TEMP (FDDURNO, AFTURNO, FTYP,
															ADVERTISMENT_ID, IP_ADDRESS, IP_PORT, DEVICE_NAME, DEVICE_AREA,
															ALARM_PAGE_NUMBER, DEFAULT_PAGE_NUMBER, FIRST_PAGE_NUMER,
															NUMBER_OF_PAGES, PAGE_NUMBER, INSTALLATION_CONFIG_FILE,
															TERMINAL_NAME, GROUP_NAME, DISPLAY_ID, PAGE_ID, CONFIG_FILE,
															DISPLAY_SEQUENCE, NUMBER_OF_FLIGHTS, REFERENCED_FIELD, PAGE_CAROUSEL_TIME,
															TIME_DELETE_1, TIME_DELETE_2, TIME_CANCELLED,
															TIME_FRAME_BEGIN, TIME_FRAME_END,
															HOME_AIRPORT, AIRPORTCODE,
--															AIRPORT_DESCRIPTION, AIRPORT_SHORT_NAME_2, AIRPORT_SHORT_NAME_3, AIRPORT_SHORT_NAME_4, AIRPORT_SHORT_NAME,
															AIRLINECODE, AIRLINECODE2,
															FLIGHTNUMBER, FLIGHT_TYPE, CODE_SHARE, FLIGHT_NATURE, VIA_AIRPORT,
															AIRPORT_VIA, --VIA_DESCRIPTION, VIA_SHORT_NAME_2, VIA_SHORT_NAME_3, VIA_SHORT_NAME_4, VIA_SHORT_NAME,
															ESTIMATED_TIME_OF_FLIGHT,
															SCHEDULED_TIME,
															ETA_INTERN_BEST,
															ETD_INTERN_BEST,
															ACTUAL_TIME,
															OFFBLOCK_TIME,
															ONBLOCK_TIME,
															AIRBORNE_TIME,
															TIME_TO_CURRENT_REMARK,
															LANDING_TIME,
															TIFF,
															NEW_TIME,
															FLIGHT_STATUS, --FLIGHT_STATUS_DESCRIPTION,
--															FLIGHT_STATUS_LOCAL, FLIGHT_STATUS_3, FLIGHT_STATUS_4,
															CHECKIN_SUM_REMARK_STATUS, --CHECKIN_SUM_REMARK_DESCRIPTION,
--															CHECKIN_SUM_REMARK_LOCAL, CHECKIN_SUM_REMARK_3, CHECKIN_SUM_REMARK_4,
															BELT1, BELT2, CHECK_IN_FROM, CHECK_IN_TO,
                                                            TEXT1, TEXT2,
															GATE_DEPARTURE_1, GATE_DEPARTURE_2,
															GATE_ARRIVAL_1, GATE_ARRIVAL_2,
															POSITION_ARRIVAL, POSITION_DEPARTURE,
															REGISTRATION, CAROUSEL,
															STATUS,	SEQ_ID)
										VALUES (r_WEBFIDSFDDFDVTAB.Urno, r_WEBFIDSFDDFDVTAB.Aurn, trim(r_Devices.DevTab_Dffd),
												trim(r_WEBFIDSFDDFDVTAB.Adid), trim(r_Devices.DevTab_Dadr), trim(r_Devices.DevTab_Dprt), trim(r_Devices.DevTab_Devn), trim(r_Devices.DevTab_Dare),
												trim(r_Devices.Dapn), trim(r_Devices.Demt), trim(r_Devices.Dfpn),
												trim(r_Devices.Dnop), trim(p_PageNumber), trim(r_Devices.Dicf),
												trim(r_Devices.DevTab_Dter), trim(r_Devices.DevTab_Grpn), trim(r_Devices.Devtab_Dpid), trim(r_PagTab.Pagi), trim(r_PagTab.Pcfn),
												r_WEBFIDSFDDFDVTAB.Dseq, trim(r_PagTab.Pnof), trim(r_PagTab.Prfn), trim(r_PagTab.Pcti),
												trim(r_PagTab.Ptd1), trim(r_PagTab.Ptd2), trim(r_Pagtab.Ptdc),
												trim(r_PagTab.Ptfb), trim(r_PagTab.Ptfe),
												trim(r_WEBFIDSFDDFDVTAB.Hopo), trim(r_WEBFIDSFDDFDVTAB.Apc3),
--												trim(v_AirportDescription), trim(v_AirportShortName_2), trim(v_AirportShortName_3), trim(v_AirportShortName_4), trim(v_AirportShortName),
												trim(r_WEBFIDSFDDFDVTAB.Alc3), trim(r_WEBFIDSFDDFDVTAB.Alc2),
												trim(r_WEBFIDSFDDFDVTAB.Flno), trim(r_WEBFIDSFDDFDVTAB.Ftyp), v_CodeShare, trim(r_WEBFIDSFDDFDVTAB.Dnat), trim(r_WEBFIDSFDDFDVTAB.Via3),
												trim(r_WEBFIDSFDDFDVTAB.via3), --trim(v_ViaDescription), trim(v_ViaShortName_2), trim(v_ViaShortName_3), trim(v_ViaShortName_4), trim(v_ViaShortName),
--												fun_calculate_time(trim(r_WEBFIDSFDDFDVTAB.Etof),'YYYYMMDDHH24MISS',v_UTCOffset,'MINUTE'),
--												fun_calculate_time(trim(r_WEBFIDSFDDFDVTAB.Stof),'YYYYMMDDHH24MISS',v_UTCOffset,'MINUTE'),
--												fun_calculate_time(trim(r_WEBFIDSFDDFDVTAB.Etai),'YYYYMMDDHH24MISS',v_UTCOffset,'MINUTE'),
--												fun_calculate_time(trim(r_WEBFIDSFDDFDVTAB.Etdi),'YYYYMMDDHH24MISS',v_UTCOffset,'MINUTE'),
												fun_calculate_time(trim(r_WEBFIDSFDDFDVTAB.Etof),'YYYYMMDDHH24MISS',FUN_GET_UTCOFFSET(r_WEBFIDSFDDFDVTAB.Etof,v_HOPO),'MINUTE'),
												fun_calculate_time(trim(r_WEBFIDSFDDFDVTAB.Stof),'YYYYMMDDHH24MISS',FUN_GET_UTCOFFSET(r_WEBFIDSFDDFDVTAB.Stof,v_HOPO),'MINUTE'),
												fun_calculate_time(trim(r_WEBFIDSFDDFDVTAB.Etai),'YYYYMMDDHH24MISS',FUN_GET_UTCOFFSET(r_WEBFIDSFDDFDVTAB.Etai,v_HOPO),'MINUTE'),
												fun_calculate_time(trim(r_WEBFIDSFDDFDVTAB.Etdi),'YYYYMMDDHH24MISS',FUN_GET_UTCOFFSET(r_WEBFIDSFDDFDVTAB.Etdi,v_HOPO),'MINUTE'),
												'',
--												fun_calculate_time(trim(r_WEBFIDSFDDFDVTAB.Ofbl),'YYYYMMDDHH24MISS',v_UTCOffset,'MINUTE'),
--												fun_calculate_time(trim(r_WEBFIDSFDDFDVTAB.Onbl),'YYYYMMDDHH24MISS',v_UTCOffset,'MINUTE'),
--												fun_calculate_time(trim(r_WEBFIDSFDDFDVTAB.Airb),'YYYYMMDDHH24MISS',v_UTCOffset,'MINUTE'),
--												fun_calculate_time(trim(r_WEBFIDSFDDFDVTAB.Rmti),'YYYYMMDDHH24MISS',v_UTCOffset,'MINUTE'),
--												fun_calculate_time(trim(r_WEBFIDSFDDFDVTAB.Land),'YYYYMMDDHH24MISS',v_UTCOffset,'MINUTE'),
--												fun_calculate_time(trim(r_WEBFIDSFDDFDVTAB.Tiff),'YYYYMMDDHH24MISS',v_UTCOffset,'MINUTE'),
												fun_calculate_time(trim(r_WEBFIDSFDDFDVTAB.Ofbl),'YYYYMMDDHH24MISS',FUN_GET_UTCOFFSET(r_WEBFIDSFDDFDVTAB.Ofbl,v_HOPO),'MINUTE'),
												fun_calculate_time(trim(r_WEBFIDSFDDFDVTAB.Onbl),'YYYYMMDDHH24MISS',FUN_GET_UTCOFFSET(r_WEBFIDSFDDFDVTAB.Onbl,v_HOPO),'MINUTE'),
												fun_calculate_time(trim(r_WEBFIDSFDDFDVTAB.Airb),'YYYYMMDDHH24MISS',FUN_GET_UTCOFFSET(r_WEBFIDSFDDFDVTAB.Airb,v_HOPO),'MINUTE'),
												fun_calculate_time(trim(r_WEBFIDSFDDFDVTAB.Rmti),'YYYYMMDDHH24MISS',FUN_GET_UTCOFFSET(r_WEBFIDSFDDFDVTAB.Rmti,v_HOPO),'MINUTE'),
												fun_calculate_time(trim(r_WEBFIDSFDDFDVTAB.Land),'YYYYMMDDHH24MISS',FUN_GET_UTCOFFSET(r_WEBFIDSFDDFDVTAB.Land,v_HOPO),'MINUTE'),
												fun_calculate_time(trim(r_WEBFIDSFDDFDVTAB.Tiff),'YYYYMMDDHH24MISS',FUN_GET_UTCOFFSET(r_WEBFIDSFDDFDVTAB.Tiff,v_HOPO),'MINUTE'),
												to_char(sysdate,'YYYYMMDDHH24MISS'),
												trim(r_WEBFIDSFDDFDVTAB.Remp), --trim(v_Remarke),
--												trim(v_RemarkD), trim(v_Remark3), trim(v_Remark4),
												trim(r_WEBFIDSFDDFDVTAB.Rem2),--trim(v_Checkin_Sum_RemarkE),
--												trim(v_Checkin_Sum_RemarkD), trim(v_Checkin_Sum_Remark3), trim(v_Checkin_Sum_Remark4),
												trim(r_WEBFIDSFDDFDVTAB.Blt1), trim(r_WEBFIDSFDDFDVTAB.Blt2), trim(r_WEBFIDSFDDFDVTAB.Ckif), trim(r_WEBFIDSFDDFDVTAB.Ckit),
                                                trim(v_Text1), trim(v_Text2),
												trim(r_WEBFIDSFDDFDVTAB.Gtd1), trim(r_WEBFIDSFDDFDVTAB.Gtd2),
												trim(r_WEBFIDSFDDFDVTAB.Gta1), trim(r_WEBFIDSFDDFDVTAB.Gta2),
												trim(r_WEBFIDSFDDFDVTAB.Psta), trim(r_WEBFIDSFDDFDVTAB.Pstd),
												trim(r_WEBFIDSFDDFDVTAB.Regn), trim(r_WEBFIDSFDDFDVTAB.Baz1),
												'N', v_Seq_id);
									COMMIT;
									success := -1;
								END IF; -- Default page
                        	END IF;
						END IF;
					END IF;

				EXCEPTION	-- Insert
					WHEN others THEN
                    	ROLLBACK;
						pck_ceda_errors.prc_write_error('FUN_USE_DATA_FROM_FDDTAB ', 'FIDS-80000',
    	                               'Error-Code: ' || TO_CHAR(SQLCODE) || ' Error-Message: ' ||
									   SUBSTR(SQLERRM, 1, 150) || ' Insert error in WEBFIDS_TABLE_TEMP', 1);
				END;

			EXCEPTION
				WHEN no_data_found THEN
					pck_ceda_errors.prc_write_error('FUN_USE_DATA_FROM_FDDTAB ', 'FIDS-80001',
   	                               'Error-Code: ' || TO_CHAR(SQLCODE) || ' Error-Message: ' ||
								   SUBSTR(SQLERRM, 1, 150), 1);
				WHEN others THEN
					pck_ceda_errors.prc_write_error('FUN_USE_DATA_FROM_FDDTAB ', 'FIDS-80000',
   	                               'Error-Code: ' || TO_CHAR(SQLCODE) || ' Error-Message: ' ||
								   SUBSTR(SQLERRM, 1, 150), 1);
			END;

		END LOOP;
		CLOSE v_WEBFIDSFDDFDVTAB_cur;

		IF NOT v_DataFound AND p_PageNumber = 1 THEN
			success := FUN_SET_DEFAULT_FOR_FDD;
		END IF;

		RETURN success;

	EXCEPTION
		WHEN OTHERS THEN
			IF v_WEBFIDSFDDFDVTAB_cur%ISOPEN = TRUE THEN
				CLOSE v_WEBFIDSFDDFDVTAB_cur;
			END IF;
			pck_ceda_errors.prc_write_error('FUN_USE_DATA_FROM_FDDTAB ', 'FIDS-80000',
                           'Error-Code: ' || TO_CHAR(SQLCODE) || ' Error-Message: ' ||
						   SUBSTR(SQLERRM, 1, 150) || ' in FDDTAB cursor', 1);
			success := 0;
            RETURN success;

	END FUN_USE_DATA_FROM_FDDTAB;

-------------------------------------------------------------------------------------------
--      Function              - FUN_USE_DATA_FROM_FLVTAB
-------------------------------------------------------------------------------------------

	FUNCTION FUN_USE_DATA_FROM_FLVTAB (p_SQLString IN VARCHAR2,
									   p_PageNumber IN DSPTAB.PN01%TYPE)
	RETURN INTEGER
	IS
    ---------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------
    --      Function              - FUN_USE_DATA_FROM_FLVTAB
    --
    --      Version               - 1.4.0
    --
    --      Written By            - Juergen Hammerschmid
    --
    --      Return                -1 = OK
	--
    --      Errors                - Errors will be written into the table FIDS_ERRORS
    --
    ---------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------
    --      Change Log
    --
    --      Name           Date               Version    Change
    --      ----           ----------         -------    ------
	--      Hammerschmid   31.03.2008         1.0        new
	--      Hammerschmid   17.04.2008         1.1        get free text changes
	--		Hammerschmid   21.04.2008		  1.2        delete error message when no data
    --													 found in AFTTAB. This is possible for
    --													 e.g. common checkin counter
    --      Hammerschmid   23.04.2008		  1.4		 FLVTAB replaced with view
    --      Hammerschmid   03.07.2008		  1.5        Additional parameter for CHECK_BELT
    ---------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------
    TYPE WEBFIDSFLVTABType		        IS REF CURSOR;
    v_WEBFIDSFLVTAB_cur            		WEBFIDSFLVTABType;
    r_WEBFIDSFLVTAB                		WEBFIDSFLVTAB%ROWTYPE;

	v_CodeShare					AFTTAB.jfno%TYPE;

	v_SQLString					VARCHAR2(500);

    v_Text1						FXTTAB.TEXT%TYPE;
    v_Text2						FXTTAB.TEXT%TYPE;

	v_RecordFoundString			VARCHAR2(500);
	v_RecordsFound				NUMBER(10) := 0;
	v_KeepRecord				INTEGER := -1;

	v_Seq_id					NUMBER(22);
	v_DataFound					BOOLEAN := FALSE;

	v_DataOK					INTEGER := -1;

	v_CheckBelt					INTEGER := -1;

    success                 	INTEGER := -1;

	BEGIN

		BEGIN
			v_RecordFoundString := replace(p_SQLString, '*', 'COUNT(*)');
        	v_RecordFoundString := replace(v_RecordFoundString,'FLVTAB','WEBFIDSFLVTAB');
			EXECUTE IMMEDIATE v_RecordFoundString INTO v_RecordsFound;
		EXCEPTION
			WHEN OTHERS THEN
				v_RecordsFound := 0;
		END;

        -- Set ORDER BY STATEMENT FOR FLVTAB

        -- JHA 20080703
        -- check if SQLSTRING contains 'AND'
        -- if YES then ORDER BY DSEQ
        -- ELSE ORDER BY TIFA
        v_CheckBelt := INSTR(p_SQLString,'AND');
        IF v_CheckBelt > 0 THEN
	        v_SQLString := p_SQLString || ' ORDER BY DSEQ';
        ELSE
	        v_SQLString := p_SQLString || ' ORDER BY TIFA';
		END IF;

        -- JHA 20080403
        -- Replace string with new definition (fddtab no longer used)
        v_SQLString := replace(v_SQLString,'FLVTAB','WEBFIDSFLVTAB');


		OPEN v_WEBFIDSFLVTAB_cur FOR v_SQLString;
		LOOP
			-- Process data
			FETCH v_WEBFIDSFLVTAB_cur INTO r_WEBFIDSFLVTAB;
				EXIT WHEN v_WEBFIDSFLVTAB_cur%NOTFOUND OR
						  v_WEBFIDSFLVTAB_cur%NOTFOUND IS NULL;

			v_DataFound := TRUE;
--			v_AFTRecordFound := TRUE;		-- JHA20080402

			-- Check Typ CHECK, BELT, GATE
			CASE
				WHEN trim(r_WEBFIDSFLVTAB.rtyp) = 'CHECK' THEN
					v_DataOK := FUN_CHECK_CHECKIN(r_WEBFIDSFLVTAB.cty1, r_PAGTAB.pdss, r_WEBFIDSFLVTAB.flg2);
				WHEN trim(r_WEBFIDSFLVTAB.rtyp) = 'GATE' THEN
					v_DataOK := FUN_CHECK_GATE(r_WEBFIDSFLVTAB.cty1, r_PAGTAB.pdss, r_WEBFIDSFLVTAB.flg2);
				WHEN trim(r_WEBFIDSFLVTAB.rtyp) = 'BELT' THEN
					v_DataOK := FUN_CHECK_BELT(v_RecordsFound, r_PAGTAB.pdss, r_PAGTAB.pdpt);
				ELSE
					v_DataOK := -1;
			END CASE;

			IF v_DataOK = -1 THEN
	            --Get missing Data from FXTTAB
   	        	PRC_GET_FREE_FIDS_TEXT(r_WEBFIDSFLVTAB.urno, v_Text1, v_Text2);

				BEGIN
					BEGIN
						-- Default page number handling
						-- No default page if flight data exists
						IF p_PageNumber != r_Devices.Demt THEN

	                    	-- Check for special Displays showing only
    	                    -- configured airlines
                            v_KeepRecord := -1;
/* JHA 20080425 not required
        	                IF TRIM(r_Devices.Devtab_Algc) IS NOT NULL THEN
	                        	v_KeepRecord := FUN_AIRLINES_FOR_DISPLAY('WEBFIDSFLVTAB',
                            										 	 r_WEBFIDSFLVTAB.aurn,
                                                                         r_Devices.Devtab_Algc);
                    	    END IF;
*/
                        	IF v_KeepRecord = -1 THEN

								v_CodeShare := fun_get_formated_CodeShare(trim(r_WEBFIDSFLVTAB.Jfno),',');
								v_Seq_id := fun_get_next_sequence_ID;

								INSERT INTO WEBFIDS_TABLE_TEMP (AFTURNO, FDDURNO, FTYP,
														ADVERTISMENT_ID, IP_ADDRESS, IP_PORT, DEVICE_NAME, DEVICE_AREA,
														ALARM_PAGE_NUMBER, DEFAULT_PAGE_NUMBER, FIRST_PAGE_NUMER,
														NUMBER_OF_PAGES, PAGE_NUMBER, INSTALLATION_CONFIG_FILE,
														TERMINAL_NAME, GROUP_NAME, DISPLAY_ID, PAGE_ID, CONFIG_FILE,
														DISPLAY_SEQUENCE, NUMBER_OF_FLIGHTS, REFERENCED_FIELD, PAGE_CAROUSEL_TIME,
														TIME_DELETE_1, TIME_DELETE_2, TIME_CANCELLED,
														TIME_FRAME_BEGIN, TIME_FRAME_END,
														HOME_AIRPORT, AIRPORTCODE,
														AIRPORT_ORIGIN, --ORIGIN_DESCRIPTION, ORIGIN_SHORT_NAME_2, ORIGIN_SHORT_NAME_3, ORIGIN_SHORT_NAME_4, ORIGIN_SHORT_NAME,
														AIRPORT_DESTINATION, --DESTINATION_DESCRIPTION, DESTINATION_SHORT_NAME_2, DESTINATION_SHORT_NAME_3, DESTINATION_SHORT_NAME_4, DESTINATION_SHORT_NAME,
														AIRPORT_VIA, --VIA_DESCRIPTION, VIA_SHORT_NAME_2, VIA_SHORT_NAME_3, VIA_SHORT_NAME_4, VIA_SHORT_NAME,
														AIRLINECODE, AIRLINECODE2,
														FLIGHTNUMBER, CODE_SHARE,
														STANDARD_TIME_OF_DEPARTURE,
														STANDARD_TIME_OF_ARRIVAL,
														ETA_INTERN_BEST,
														ETD_INTERN_BEST,
														ESTIMATED_TIME_OF_DEPARTURE,
														TIF_ARRIVAL,
														TIF_DEPARTURE,
														BOARDING_OPEN,
														NEW_TIME,
														FLIGHT_STATUS, --FLIGHT_STATUS_DESCRIPTION,
--														FLIGHT_STATUS_LOCAL, FLIGHT_STATUS_3, FLIGHT_STATUS_4,
														BELT1, CHECK_IN_FROM, CHECK_IN_TO,
--														CHECKIN_SUM_REMARK_DESCRIPTION,
--														CHECKIN_SUM_REMARK_LOCAL,
--														CHECKIN_SUM_REMARK_3,
--														CHECKIN_SUM_REMARK_4,
														CHECKIN_REMARK, CHECKIN_TYPE, BELT_REMARK,
														RESOURCE_NAME, RESOURCE_TYPE,
														FLIGHT_LOGO, FULL_SCREEN_LOGO,
														TEXT1, TEXT2,
														GATE_DEPARTURE_1, GATE_DEPARTURE_2,
														GATE_ARRIVAL_1, GATE_ARRIVAL_2,
														POSITION_ARRIVAL, POSITION_DEPARTURE,
														REGISTRATION, CAROUSEL,
														STATUS,	SEQ_ID)
									VALUES (r_WEBFIDSFLVTAB.aurn, r_WEBFIDSFLVTAB.urno, trim(r_Devices.DevTab_Dffd),
											trim(r_WEBFIDSFLVTAB.Adid), trim(r_Devices.DevTab_Dadr), trim(r_Devices.DevTab_Dprt), trim(r_Devices.DevTab_Devn), trim(r_Devices.DevTab_Dare),
											trim(r_Devices.Dapn), trim(r_Devices.Demt), trim(r_Devices.Dfpn),
											trim(r_Devices.Dnop), trim(p_PageNumber), trim(r_Devices.Dicf),
											trim(r_Devices.DevTab_Dter), trim(r_Devices.DevTab_Grpn), trim(r_Devices.DevTab_Dpid), trim(r_PagTab.Pagi), trim(r_PagTab.Pcfn),
											trim(r_WEBFIDSFLVTAB.Dseq), trim(r_PagTab.Pnof), trim(r_PagTab.Prfn), trim(r_PagTab.Pcti),
											trim(r_PagTab.Ptd1), trim(r_PagTab.Ptd2), trim(r_Pagtab.Ptdc),
											trim(r_PagTab.Ptfb), trim(r_PagTab.Ptfe),
											trim(r_WEBFIDSFLVTAB.Hopo), trim(r_WEBFIDSFLVTAB.Des3),
											trim(r_WEBFIDSFLVTAB.org3), --trim(v_OriginDescription), trim(v_OriginShortName_2), trim(v_OriginShortName_3), trim(v_OriginShortName_4), trim(v_OriginShortName),
											trim(r_WEBFIDSFLVTAB.des3), --trim(v_DestinationDescription), trim(v_DestinationShortName_2), trim(v_DestinationShortName_3), trim(v_DestinationShortName_4), trim(v_DestinationShortName),
											trim(r_WEBFIDSFLVTAB.via3), --trim(v_ViaDescription), trim(v_ViaShortName_2), trim(v_ViaShortName_3), trim(v_ViaShortName_4), trim(v_ViaShortName),
											trim(r_WEBFIDSFLVTAB.Alc3), trim(r_WEBFIDSFLVTAB.Alc2),
											trim(r_WEBFIDSFLVTAB.Flno), trim(v_CodeShare),
--											fun_calculate_time(trim(r_WEBFIDSFLVTAB.Stod),'YYYYMMDDHH24MISS',v_UTCOffset,'MINUTE'),
--											fun_calculate_time(trim(r_WEBFIDSFLVTAB.Stoa),'YYYYMMDDHH24MISS',v_UTCOffset,'MINUTE'),
--											fun_calculate_time(trim(r_WEBFIDSFLVTAB.Etai),'YYYYMMDDHH24MISS',v_UTCOffset,'MINUTE'),
--											fun_calculate_time(trim(r_WEBFIDSFLVTAB.Etdi),'YYYYMMDDHH24MISS',v_UTCOffset,'MINUTE'),
--											fun_calculate_time(trim(r_WEBFIDSFLVTAB.Etod),'YYYYMMDDHH24MISS',v_UTCOffset,'MINUTE'),
--											fun_calculate_time(trim(r_WEBFIDSFLVTAB.Tifa),'YYYYMMDDHH24MISS',v_UTCOffset,'MINUTE'),
--											fun_calculate_time(trim(r_WEBFIDSFLVTAB.Tifd),'YYYYMMDDHH24MISS',v_UTCOffset,'MINUTE'),
											fun_calculate_time(trim(r_WEBFIDSFLVTAB.Stod),'YYYYMMDDHH24MISS',FUN_GET_UTCOFFSET(r_WEBFIDSFLVTAB.Stod,v_HOPO),'MINUTE'),
											fun_calculate_time(trim(r_WEBFIDSFLVTAB.Stoa),'YYYYMMDDHH24MISS',FUN_GET_UTCOFFSET(r_WEBFIDSFLVTAB.Stoa,v_HOPO),'MINUTE'),
											fun_calculate_time(trim(r_WEBFIDSFLVTAB.Etai),'YYYYMMDDHH24MISS',FUN_GET_UTCOFFSET(r_WEBFIDSFLVTAB.Etai,v_HOPO),'MINUTE'),
											fun_calculate_time(trim(r_WEBFIDSFLVTAB.Etdi),'YYYYMMDDHH24MISS',FUN_GET_UTCOFFSET(r_WEBFIDSFLVTAB.Etdi,v_HOPO),'MINUTE'),
											fun_calculate_time(trim(r_WEBFIDSFLVTAB.Etod),'YYYYMMDDHH24MISS',FUN_GET_UTCOFFSET(r_WEBFIDSFLVTAB.Etod,v_HOPO),'MINUTE'),
											fun_calculate_time(trim(r_WEBFIDSFLVTAB.Tifa),'YYYYMMDDHH24MISS',FUN_GET_UTCOFFSET(r_WEBFIDSFLVTAB.Tifa,v_HOPO),'MINUTE'),
											fun_calculate_time(trim(r_WEBFIDSFLVTAB.Tifd),'YYYYMMDDHH24MISS',FUN_GET_UTCOFFSET(r_WEBFIDSFLVTAB.Tifd,v_HOPO),'MINUTE'),
											trim(r_WEBFIDSFLVTAB.boao),
											to_char(sysdate,'YYYYMMDDHH24MISS'),
											trim(r_WEBFIDSFLVTAB.Remp), --trim(v_RemarkE),
--											trim(v_RemarkD), trim(v_Remark3), trim(v_Remark4),
											trim(r_WEBFIDSFLVTAB.Blt1), trim(r_WEBFIDSFLVTAB.Ckif), trim(r_WEBFIDSFLVTAB.Ckit),
--											v_Checkin_Sum_RemarkE,
--											v_Checkin_Sum_RemarkD,
--											v_Checkin_Sum_Remark3,
--											v_Checkin_Sum_Remark4,
											trim(r_WEBFIDSFLVTAB.crec), trim(r_WEBFIDSFLVTAB.ctyp), trim(r_WEBFIDSFLVTAB.disp),
											trim(r_WEBFIDSFLVTAB.rnam), trim(r_WEBFIDSFLVTAB.rtyp),
											trim(r_WEBFIDSFLVTAB.flg1), trim(r_WEBFIDSFLVTAB.flg2),
											trim(v_Text1), trim(v_Text2),
											trim(r_WEBFIDSFLVTAB.Gtd1), trim(r_WEBFIDSFLVTAB.Gtd2),
											trim(r_WEBFIDSFLVTAB.Gta1), trim(r_WEBFIDSFLVTAB.Gta2),
											trim(r_WEBFIDSFLVTAB.Psta), trim(r_WEBFIDSFLVTAB.Pstd),
											trim(r_WEBFIDSFLVTAB.Regn), trim(r_WEBFIDSFLVTAB.Baz1),
											'N', v_Seq_Id);
								COMMIT;
								success := -1;
                        	END IF;
						END IF;

					EXCEPTION	-- Insert
						WHEN others THEN
							ROLLBACK;
							pck_ceda_errors.prc_write_error('FUN_USE_DATA_FROM_FLVTAB ', 'FIDS-80000',
										   'Error-Code: ' || TO_CHAR(SQLCODE) || ' Error-Message: ' ||
										   SUBSTR(SQLERRM, 1, 150) || ' Insert error in WEBFIDS_TABLE_TEMP', 1);
					END;

				EXCEPTION
					WHEN no_data_found THEN
						pck_ceda_errors.prc_write_error('FUN_USE_DATA_FROM_FLVTAB ', 'FIDS-80001',
									   'Error-Code: ' || TO_CHAR(SQLCODE) || ' Error-Message: ' ||
									   SUBSTR(SQLERRM, 1, 150), 1);
					WHEN others THEN
						pck_ceda_errors.prc_write_error('FUN_USE_DATA_FROM_FLVTAB ', 'FIDS-80000',
									   'Error-Code: ' || TO_CHAR(SQLCODE) || ' Error-Message: ' ||
									   SUBSTR(SQLERRM, 1, 150), 1);
				END;
			END IF;
		END LOOP;
		CLOSE v_WEBFIDSFLVTAB_cur;

		IF NOT v_DataFound AND p_PageNumber = 1 THEN
			success := FUN_SET_DEFAULT_FOR_FLV;
		END IF;

		RETURN success;

	EXCEPTION
		WHEN OTHERS THEN
			IF v_WEBFIDSFLVTAB_cur%ISOPEN = TRUE THEN
				CLOSE v_WEBFIDSFLVTAB_cur;
			END IF;
			pck_ceda_errors.prc_write_error('FUN_USE_DATA_FROM_FLVTAB ', 'FIDS-80000',
                           'Error-Code: ' || TO_CHAR(SQLCODE) || ' Error-Message: ' ||
						   SUBSTR(SQLERRM, 1, 150) || ' in FLVTAB cursor', 1);
			success := 0;
            RETURN success;

	END FUN_USE_DATA_FROM_FLVTAB;

-------------------------------------------------------------------------------------------
--      Function              - FUN_KEEP_RECORD
-------------------------------------------------------------------------------------------
	FUNCTION FUN_KEEP_RECORD			(p_Airborn			IN FDDTAB.AIRB%TYPE,
										 p_Offblock			IN FDDTAB.ONBL%TYPE,
										 p_Land				IN FDDTAB.LAND%TYPE,
										 p_Onblock			IN FDDTAB.OFBL%TYPE,
										 p_TimeOfFlight		IN FDDTAB.STOF%TYPE,
										 p_Tiff				IN FDDTAB.TIFF%TYPE)
	RETURN INTEGER
	IS

    ---------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------
    --      Function              - FUN_KEEP_RECORD
    --
    --      Version               - 1.2.0
    --
    --      Written By            - Juergen Hammerschmid
    --
    --      Return                - -1 -> OK
	--
    --      Errors                - Errors will be written into the table CEDA_ERRORS
    --
    ---------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------
    --      Change Log
    --
    --      Name           Date               Version    Change
    --      ----           ----------         -------    ------
	--      Hammerschmid   31.03.2008         1.0        new
	--      Hammerschmid   12.04.2008         1.1        CASE ELSE
	--      Hammerschmid   20.04.2008         1.2        Use p_TIFF instead of p_TimeOfFlight
    ---------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------

	v_KeepRecord	INTEGER := -1;
	v_DisplayType	PAGTAB.PDPT%TYPE := r_PAGTAB.Pdpt;
	v_Ref			PAGTAB.PRFN%TYPE := r_PAGTAB.prfn;
	v_CalcTime		FDDTAB.TIFF%TYPE;
	v_CheckTime		FDDTAB.TIFF%TYPE;

	BEGIN

		IF TRIM(v_DisplayType) = 'A' THEN			--Arrival
			-- Check ONBL AND LAND
			CASE
				WHEN TRIM(v_Ref) = 'ONBL' THEN
					IF p_Onblock is not NULL THEN
						v_CalcTime := fun_calculate_time(p_Onblock,'YYYYMMDDHH24MISS',r_PAGTAB.PTD2,'MINUTE');
					ELSE
--						v_CalcTime := fun_calculate_time(p_TimeOfFlight,'YYYYMMDDHH24MISS',r_PAGTAB.PTFB,'MINUTE');
						v_CalcTime := fun_calculate_time(p_Tiff,'YYYYMMDDHH24MISS',r_PAGTAB.PTFB,'MINUTE');
					END IF;
				WHEN TRIM(v_Ref) = 'LAND' THEN
					IF p_Land is not NULL THEN
						v_CalcTime := fun_calculate_time(p_Land,'YYYYMMDDHH24MISS',r_PAGTAB.PTD2,'MINUTE');
					ELSE
--						v_CalcTime := fun_calculate_time(p_TimeOfFlight,'YYYYMMDDHH24MISS',r_PAGTAB.PTFB,'MINUTE');
						v_CalcTime := fun_calculate_time(p_Tiff,'YYYYMMDDHH24MISS',r_PAGTAB.PTFB,'MINUTE');
					END IF;
				WHEN TRIM(v_Ref) is NULL THEN
--					v_CalcTime := fun_calculate_time(p_TimeOfFlight,'YYYYMMDDHH24MISS',R_PAGTAB.PTFB,'MINUTE');
					v_CalcTime := fun_calculate_time(p_Tiff,'YYYYMMDDHH24MISS',r_PAGTAB.PTFB,'MINUTE');
                ELSE
--					v_CalcTime := fun_calculate_time(p_TimeOfFlight,'YYYYMMDDHH24MISS',R_PAGTAB.PTFB,'MINUTE');
					v_CalcTime := fun_calculate_time(p_Tiff,'YYYYMMDDHH24MISS',r_PAGTAB.PTFB,'MINUTE');
			END CASE;
		ELSIF TRIM(v_DisplayType) = 'D' THEN		--Departure
			-- Check OFBL AND AIRB
			CASE
				WHEN TRIM(v_Ref) = 'OFBL' THEN
					IF p_Offblock is not NULL THEN
						v_CalcTime := fun_calculate_time(p_Offblock,'YYYYMMDDHH24MISS',r_PAGTAB.PTD2,'MINUTE');
					ELSE
--						v_CalcTime := fun_calculate_time(p_TimeOfFlight,'YYYYMMDDHH24MISS',r_PAGTAB.PTFB,'MINUTE');
						v_CalcTime := fun_calculate_time(p_Tiff,'YYYYMMDDHH24MISS',r_PAGTAB.PTFB,'MINUTE');
					END IF;
				WHEN TRIM(v_Ref) = 'AIRB' THEN
					IF p_Airborn is not NULL THEN
						v_CalcTime := fun_calculate_time(p_Offblock,'YYYYMMDDHH24MISS',r_PAGTAB.PTD2,'MINUTE');
					ELSE
--						v_CalcTime := fun_calculate_time(p_TimeOfFlight,'YYYYMMDDHH24MISS',r_PAGTAB.PTFB,'MINUTE');
						v_CalcTime := fun_calculate_time(p_Tiff,'YYYYMMDDHH24MISS',r_PAGTAB.PTFB,'MINUTE');
					END IF;
				WHEN TRIM(v_Ref) is null THEN
--					v_CalcTime := fun_calculate_time(p_TimeOfFlight,'YYYYMMDDHH24MISS',r_PAGTAB.PTFB,'MINUTE');
					v_CalcTime := fun_calculate_time(p_Tiff,'YYYYMMDDHH24MISS',r_PAGTAB.PTFB,'MINUTE');
                ELSE
--					v_CalcTime := fun_calculate_time(p_TimeOfFlight,'YYYYMMDDHH24MISS',r_PAGTAB.PTFB,'MINUTE');
					v_CalcTime := fun_calculate_time(p_Tiff,'YYYYMMDDHH24MISS',r_PAGTAB.PTFB,'MINUTE');
			END CASE;
		ELSE
			v_KeepRecord := -1;
		END IF;

		v_CheckTime := to_char(sysdate,'YYYYMMDDHH24MISS');
--		v_CheckTime := fun_calculate_time(v_CheckTime,'YYYYMMDDHH24MISS','-' || v_UTCOffset,'MINUTE');
		v_CheckTime := fun_calculate_time(v_CheckTime,'YYYYMMDDHH24MISS','-' || FUN_GET_UTCOFFSET(v_CheckTime,v_HOPO),'MINUTE');

		IF v_CalcTime < v_CheckTime THEN
			-- throw away
			v_KeepRecord := 0;
		ELSE
			-- keep
			v_KeepRecord := -1;
		END IF;

		RETURN v_KeepRecord;

	EXCEPTION
		WHEN OTHERS THEN
        	-- Return value included
			v_KeepRecord := 0;
			pck_ceda_errors.prc_write_error('FUN_KEEP_RECORD ', 'FIDS-80000',
                           'Error-Code: ' || TO_CHAR(SQLCODE) || ' Error-Message: ' ||
						   SUBSTR(SQLERRM, 1, 150), 1);
			RETURN v_KeepRecord;

	END FUN_KEEP_RECORD;

-------------------------------------------------------------------------------------------
--      Function              - FUN_AIRLINES_FOR_DISPLAY
-------------------------------------------------------------------------------------------
	FUNCTION FUN_AIRLINES_FOR_DISPLAY   (p_TableName	IN VARCHAR2,
  										 p_AFTUrno      IN NUMBER,
    									 p_Airlinecodes IN DEVTAB.ALGC%TYPE)
    RETURN INTEGER
	IS
    ---------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------
    --      Function              - FUN_AIRLINES_FOR_DISPLAY
    --
    --      Version               - 1.2.0
    --
    --      Written By            - Juergen Hammerschmid
    --
    --      Return                - -1 -> OK
	--
    --      Errors                - Errors will be written into the table CEDA_ERRORS
    --
    ---------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------
    --      Change Log
    --
    --      Name           Date               Version    Change
    --      ----           ----------         -------    ------
	--      Hammerschmid   31.03.2008         1.0        new
	--      Hammerschmid   14.04.2008         1.1        additional Parameter p_TableName
    --													 implement NOT ! option
    --		Hammerschmid   25.04.2008		  1.2        use only table fdd and fdvtab
    ---------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------

    TYPE AirlinesType		        IS REF CURSOR;
    v_Airlines_cur            		AirlinesType;
    r_Airlines                		ALTTAB%ROWTYPE;

	v_KeepRecord	INTEGER := -1;
    v_Airlines		VARCHAR2(128);
    v_AirlineCodes  VARCHAR2(128);
    v_ALC3			VARCHAR2(128);
    v_ALC2			VARCHAR2(128);
    v_ALC2ToALC3	VARCHAR2(128);
    v_Temp			VARCHAR2(10);
    v_SQLSelect 	VARCHAR2(512);
    v_UrnoFound		AFTTAB.URNO%TYPE;
    v_TableName		VARCHAR2(64) := trim(p_TableName);
    I				INTEGER;

    v_NOT			BOOLEAN := FALSE;

    BEGIN
    	v_UrnoFound := 0;

        IF LENGTH(v_TableName) = 0 THEN
        	v_TableName := 'AFTTAB';
        END IF;

        -- check if ! (NOT) exists in p_Airlinecodes
        IF INSTR(p_AirlineCodes,'!') != 0 THEN
        	v_NOT := TRUE;
            -- do not change the order of the next two lines !!!
            v_Airlines := REPLACE(p_AirlineCodes,'! ','');
            v_Airlines := REPLACE(v_Airlines,'!','');
        ELSE
        	v_Airlines := p_AirlineCodes;
        END IF;

    	FOR I IN 1..LENGTH(TRIM(v_Airlines))
        LOOP
        	IF I = 1 THEN
        		v_AirlineCodes := '''' || SUBSTR(v_Airlines,I,1);
            ELSE
--            	IF SUBSTR(p_Airlinecodes,I,1) = ',' THEN
            	IF SUBSTR(v_Airlines,I,1) = ' ' THEN
        			v_AirlineCodes := v_AirlineCodes || '''' || ',''';
                ELSE
        			v_AirlineCodes := v_AirlineCodes || SUBSTR(v_Airlines,I,1);
            	END IF;
			END IF;
        END LOOP;
        v_AirlineCodes := v_AirlineCodes || '''';

		-- split ALC2 and ALC3 codes
        v_ALC3 := '';
        v_ALC2 := '';
        v_Temp := '';

        FOR I IN 1..LENGTH(v_AirlineCodes)
        LOOP
        	IF SUBSTR(v_AirlineCodes,I,1) = ',' THEN
            	CASE LENGTH(v_Temp)
	            	WHEN 5 THEN -- ALC3 + 2 '
			        	v_Temp := v_Temp || SUBSTR(v_AirlineCodes,I,1);
        	        	v_ALC3 := v_ALC3 || v_Temp;
            	    WHEN 4 THEN -- ALC2 + 2 '
                		v_Temp := v_Temp || SUBSTR(v_AirlineCodes,I,1);
                    	v_ALC2 := v_ALC2 || v_Temp;
	                ELSE
    	            	-- Error
        	        	v_Temp := '';
                END CASE;
	            v_Temp := '';
    	    ELSE
	    	   	v_Temp := v_Temp || SUBSTR(v_AirlineCodes,I,1);
            END IF;
        END LOOP;

		-- Check last entry
       	CASE LENGTH(v_Temp)
           	WHEN 5 THEN -- ALC3 + 2 '
   	        	v_ALC3 := v_ALC3 || v_Temp;
       	    WHEN 4 THEN -- ALC2 + 2 '
               	v_ALC2 := v_ALC2 || v_Temp;
            ELSE
            	-- Error
   	        	v_Temp := '';
        END CASE;
       	IF SUBSTR(v_ALC2,LENGTH(v_ALC2),1) = ',' THEN
           	v_ALC2 := SUBSTR(v_ALC2,1,LENGTH(v_ALC2) - 1);
        END IF;
       	IF SUBSTR(v_ALC3,LENGTH(v_ALC3),1) = ',' THEN
           	v_ALC3 := SUBSTR(v_ALC3,1,LENGTH(v_ALC3) - 1);
        END IF;

        -- convert all ALC2 to ALC3
        IF v_ALC2 IS NOT NULL THEN
	        v_SQLSelect := 'SELECT * FROM ALTTAB WHERE ALC2 IN (' || v_ALC2 || ')';
	        BEGIN
            	v_ALC2ToALC3 := '';
				OPEN v_Airlines_cur FOR v_SQLSelect;
				LOOP
				FETCH v_Airlines_cur INTO r_Airlines;
					EXIT WHEN v_Airlines_cur%NOTFOUND OR
							  v_Airlines_cur%NOTFOUND IS NULL;
                    v_ALC2ToALC3 := v_ALC2ToALC3 || '''' || r_Airlines.ALC3 || ''',';
                END LOOP;

            EXCEPTION
/*                WHEN NO_DATA_FOUND THEN
                    v_KeepRecord := 0;
                    IF v_Airlines_cur%ISOPEN = TRUE THEN
                        CLOSE v_Airlines_cur;
                    END IF;
                    pck_ceda_errors.prc_write_error('FUN_AIRLINES_FOR_DISPLAY ', 'FIDS-80001',
                                   'Error-Code: ' || TO_CHAR(SQLCODE) || ' Error-Message: ' ||
                                   SUBSTR(SQLERRM, 1, 150) || ' ' || v_ALC2, 1);
*/                WHEN OTHERS THEN
                    v_KeepRecord := 0;
                    IF v_Airlines_cur%ISOPEN = TRUE THEN
                        CLOSE v_Airlines_cur;
                    END IF;
                    pck_ceda_errors.prc_write_error('FUN_AIRLINES_FOR_DISPLAY ', 'FIDS-80000',
                                   'Error-Code: ' || TO_CHAR(SQLCODE) || ' Error-Message: ' ||
                                   SUBSTR(SQLERRM, 1, 150) || ' ' || v_ALC2, 1);
            END;
        	IF SUBSTR(v_ALC2ToALC3,LENGTH(v_ALC2ToALC3),1) = ',' THEN
            	v_ALC2ToALC3 := SUBSTR(v_ALC2ToALC3,1,LENGTH(v_ALC2ToALC3) - 1);
            END IF;
            CLOSE v_Airlines_cur;

            IF v_ALC3 IS NOT NULL THEN
            	IF v_ALC2ToALC3 IS NOT NULL THEN
           			v_ALC3 := v_ALC3 || ',' || v_ALC2ToALC3;
                END IF;
            ELSE
            	IF v_ALC2ToALC3 IS NOT NULL THEN
            		v_ALC3 := v_ALC2ToALC3;
                END IF;
            END IF;
        END IF;
        -- end convert

		IF v_NOT = TRUE THEN
            v_SQLSelect := 'SELECT URNO'||
                           ' FROM ' || v_TableName ||
                           ' WHERE AURN = ' || p_AFTUrno ||
                           ' AND ALC3 NOT IN (' || v_ALC3 || ')';
        ELSE
            v_SQLSelect := 'SELECT URNO'||
                           ' FROM ' || v_TableName ||
                           ' WHERE AURN = ' || p_AFTUrno ||
                           ' AND ALC3 IN (' || v_ALC3 || ')';
        END IF;
        if v_TableName = 'AFTTAB' THEN
        	v_SQLSelect := REPLACE(v_SQLSelect,'AURN','URNO');
/*
            WHEN 'FDDTAB' THEN
	        	v_SQLSelect := REPLACE(v_SQLSelect,'FDDTAB','AFTTAB');
	        	v_SQLSelect := REPLACE(v_SQLSelect,'AURN','URNO');
*/
        END IF;

		EXECUTE IMMEDIATE v_SQLSelect INTO v_UrnoFound;

        RETURN v_KeepRecord;

    EXCEPTION
		WHEN NO_DATA_FOUND THEN
			v_KeepRecord := 0;
			RETURN v_KeepRecord;
		WHEN TOO_MANY_ROWS THEN
        	v_KeepRecord := -1;
            RETURN v_KeepRecord;
    	WHEN OTHERS THEN
			v_KeepRecord := 0;
			pck_ceda_errors.prc_write_error('FUN_AIRLINES_FOR_DISPLAY ', 'FIDS-80000',
                           'Error-Code: ' || TO_CHAR(SQLCODE) || ' Error-Message: ' ||
						   SUBSTR(SQLERRM, 1, 150), 1);
			RETURN v_KeepRecord;

	END FUN_AIRLINES_FOR_DISPLAY;

-------------------------------------------------------------------------------------------
--      Function              - FUN_CHECK_CHECKIN
-------------------------------------------------------------------------------------------
	FUNCTION FUN_CHECK_CHECKIN			(p_Type				IN FLVTAB.CTY1%TYPE,
										 p_DisplaySequence	IN PAGTAB.PDSS%TYPE,
										 p_FlightLogo		IN FLVTAB.FLG2%TYPE)
	RETURN INTEGER
	IS

    ---------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------
    --      Function              - FUN_CHECK_CHECKIN
    --
    --      Version               - 1.0.0
    --
    --      Written By            - Juergen Hammerschmid
    --
    --      Return                - -1 -> OK
	--
    --      Errors                - Errors will be written into the table CEDA_ERRORS
    --
    ---------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------
    --      Change Log
    --
    --      Name           Date               Version    Change
    --      ----           ----------         -------    ------
	--      Hammerschmid   31.03.2008         1.0        new
    ---------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------

	v_Success	INTEGER := -1;

	BEGIN
		CASE
			WHEN trim(p_Type) is NULL AND trim(p_DisplaySequence) = 'C1' THEN
				v_Success := -1;
			WHEN trim(p_Type) = 'C' AND trim(p_DisplaySequence) = 'C2' AND trim(p_FlightLogo) IS NULL THEN
				v_Success := -1;
			WHEN trim(p_Type) = 'C' AND trim(p_DisplaySequence) = 'C3' AND trim(p_FlightLogo) IS NOT NULL THEN
				v_Success := -1;
		ELSE
			v_Success := 0;
		END CASE;

		RETURN v_Success;

	EXCEPTION
		WHEN OTHERS THEN
			v_Success := 0;
			pck_ceda_errors.prc_write_error('FUN_CHECK_CHECKIN ', 'FIDS-80000',
                           'Error-Code: ' || TO_CHAR(SQLCODE) || ' Error-Message: ' ||
						   SUBSTR(SQLERRM, 1, 150), 1);
			RETURN v_Success;
	END FUN_CHECK_CHECKIN;

-------------------------------------------------------------------------------------------
--      Function              - FUN_CHECK_GATE
-------------------------------------------------------------------------------------------
	FUNCTION FUN_CHECK_GATE				(p_Type				IN FLVTAB.CTY1%TYPE,
										 p_DisplaySequence	IN PAGTAB.PDSS%TYPE,
										 p_FlightLogo		IN FLVTAB.FLG2%TYPE)
	RETURN INTEGER
	IS

    ---------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------
    --      Function              - FUN_CHECK_GATE
    --
    --      Version               - 1.0.0
    --
    --      Written By            - Juergen Hammerschmid
    --
    --      Return                - -1 -> OK
	--
    --      Errors                - Errors will be written into the table CEDA_ERRORS
    --
    ---------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------
    --      Change Log
    --
    --      Name           Date               Version    Change
    --      ----           ----------         -------    ------
	--      Hammerschmid   31.03.2008         1.0        new
    ---------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------

	v_Success	INTEGER := -1;

	BEGIN
		RETURN v_Success;
	EXCEPTION
		WHEN OTHERS THEN
			v_Success := 0;
			pck_ceda_errors.prc_write_error('FUN_CHECK_CHECKIN ', 'FIDS-80000',
                           'Error-Code: ' || TO_CHAR(SQLCODE) || ' Error-Message: ' ||
						   SUBSTR(SQLERRM, 1, 150), 1);
			RETURN v_Success;
	END FUN_CHECK_GATE;

-------------------------------------------------------------------------------------------
--      Function              - FUN_CHECK_BELT
-------------------------------------------------------------------------------------------
	FUNCTION FUN_CHECK_BELT				(p_NumberOfRecords	IN	NUMBER,
										 p_DisplaySequence	IN PAGTAB.PDSS%TYPE,
                                         p_DisplayType  	IN PAGTAB.pdpt%TYPE)
	RETURN INTEGER
	IS

    ---------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------
    --      Function              - FUN_CHECK_BELT
    --
    --      Version               - 1.0.0
    --
    --      Written By            - Juergen Hammerschmid
    --
    --      Return                - -1 -> OK
	--
    --      Errors                - Errors will be written into the table CEDA_ERRORS
    --
    ---------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------
    --      Change Log
    --
    --      Name           Date               Version    Change
    --      ----           ----------         -------    ------
	--      Hammerschmid   31.03.2008         1.0        new
    ---------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------

	v_Success	INTEGER := -1;

	BEGIN
    	IF p_DisplayType = 'L' THEN
            CASE
                WHEN  p_NumberOfRecords = 1 AND trim(p_DisplaySequence) = 'B1' THEN
                    v_Success := -1;
                WHEN  p_NumberOfRecords = 2 AND trim(p_DisplaySequence) = 'B2' THEN
                    v_Success := -1;
                WHEN  p_NumberOfRecords = 3 AND trim(p_DisplaySequence) = 'B3' THEN
                    v_Success := -1;
                WHEN  p_NumberOfRecords = 4 AND trim(p_DisplaySequence) = 'B4' THEN
                    v_Success := -1;
                WHEN  p_NumberOfRecords > 4 AND trim(p_DisplaySequence) = 'B4' THEN
                    v_Success := -1;
            ELSE
                v_Success := 0;
            END CASE;
        ELSIF p_DisplayType = 'U' THEN
        	v_Success := -1;
        END IF;
		RETURN v_Success;
	EXCEPTION
		WHEN OTHERS THEN
			v_Success := 0;
			pck_ceda_errors.prc_write_error('FUN_CHECK_CHECKIN ', 'FIDS-80000',
                           'Error-Code: ' || TO_CHAR(SQLCODE) || ' Error-Message: ' ||
						   SUBSTR(SQLERRM, 1, 150), 1);
			RETURN v_Success;
	END FUN_CHECK_BELT;

-------------------------------------------------------------------------------------------
--      Function              - FUN_SET_ADVERTISEMENT_PAGE
-------------------------------------------------------------------------------------------
	FUNCTION FUN_SET_ADVERTISEMENT_PAGE	(p_SQLString 		IN VARCHAR2,
										 p_PageNumber		IN DSPTAB.PN01%TYPE)
	RETURN INTEGER
    IS

    ---------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------
    --      Function              - FUN_SET_ADVERTISEMENT_PAGE
    --
    --      Version               - 1.0.0
    --
    --      Written By            - Juergen Hammerschmid
    --
    --      Return                - -1 -> OK
	--
    --      Errors                - Errors will be written into the table FIDS_ERRORS
    --
    ---------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------
    --      Change Log
    --
    --      Name           Date               Version    Change
    --      ----           ----------         -------    ------
	--      Hammerschmid   11.04.2008         1.0        new
    ---------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------

    success                 INTEGER := -1;
	v_Seq_id				NUMBER(22);

    BEGIN
		v_Seq_id := fun_get_next_sequence_ID;

		INSERT INTO WEBFIDS_TABLE_TEMP (FTYP,
									IP_ADDRESS, IP_PORT, DEVICE_NAME, DEVICE_AREA,
									ALARM_PAGE_NUMBER, DEFAULT_PAGE_NUMBER, FIRST_PAGE_NUMER,
									NUMBER_OF_PAGES, PAGE_NUMBER, INSTALLATION_CONFIG_FILE,
									TERMINAL_NAME, GROUP_NAME, DISPLAY_ID, PAGE_ID, CONFIG_FILE,
                                    ADVERTISMENT_ID,
									NUMBER_OF_FLIGHTS, REFERENCED_FIELD, PAGE_CAROUSEL_TIME,
									TIME_DELETE_1, TIME_DELETE_2, TIME_CANCELLED,
									TIME_FRAME_BEGIN, TIME_FRAME_END,
									NEW_TIME,
									STATUS,	SEQ_ID)
		VALUES (trim(r_Devices.DevTab_Dffd),
				trim(r_Devices.DevTab_Dadr), trim(r_Devices.DevTab_Dprt), trim(r_Devices.DevTab_Devn), trim(r_Devices.DevTab_Dare),
				trim(r_Devices.Dapn), trim(r_Devices.Demt), trim(r_Devices.Dfpn),
				trim(r_Devices.Dnop), trim(p_PageNumber), trim(r_Devices.Dicf),
				trim(r_Devices.DevTab_Dter), trim(r_Devices.DevTab_Grpn), trim(r_Devices.Devtab_Dpid), trim(r_PagTab.Pagi), trim(r_PagTab.Pcfn),
                trim(r_PagTab.Pdpt),
				trim(r_PagTab.Pnof), trim(r_PagTab.Prfn), trim(r_PagTab.Pcti),
				trim(r_PagTab.Ptd1), trim(r_PagTab.Ptd2), trim(r_Pagtab.Ptdc),
				trim(r_PagTab.Ptfb), trim(r_PagTab.Ptfe),
				to_char(sysdate,'YYYYMMDDHH24MISS'),
				'N', v_Seq_id);
		COMMIT;

		success := -1;
		RETURN success;

	EXCEPTION
		WHEN OTHERS THEN
			pck_ceda_errors.prc_write_error('FUN_SET_ADVERTISEMENT_PAGE ', 'FIDS-80000',
                           'Error-Code: ' || TO_CHAR(SQLCODE) || ' Error-Message: ' ||
						   SUBSTR(SQLERRM, 1, 150), 1);
			success := 0;
			RETURN success;

	END FUN_SET_ADVERTISEMENT_PAGE;

-------------------------------------------------------------------------------------------
--      Function              - FUN_SET_DEFAULT_FOR_FDD
-------------------------------------------------------------------------------------------
	FUNCTION FUN_SET_DEFAULT_FOR_FDD
	RETURN INTEGER
	IS

    ---------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------
    --      Function              - FUN_SET_DEFAULT_FOR_FDD
    --
    --      Version               - 1.0.0
    --
    --      Written By            - Juergen Hammerschmid
    --
    --      Return                - -1 -> OK
	--
    --      Errors                - Errors will be written into the table FIDS_ERRORS
    --
    ---------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------
    --      Change Log
    --
    --      Name           Date               Version    Change
    --      ----           ----------         -------    ------
	--      Hammerschmid   31.03.2008         1.0        new
    ---------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------

    success                 INTEGER := -1;
	v_Seq_id				NUMBER(22);
	v_DefaultPageNumber		INTEGER := 0;
	v_DefaultPage			r_Devices.Pi01%TYPE;

	BEGIN
		-- Identify default page
		IF trim(r_Devices.Demt) IS NOT NULL THEN
			v_DefaultPageNumber := to_number(trim(r_Devices.Demt));
			CASE
				WHEN v_DefaultPageNumber = 1 THEN v_DefaultPage := r_Devices.Pi01;
				WHEN v_DefaultPageNumber = 2 THEN v_DefaultPage := r_Devices.Pi02;
				WHEN v_DefaultPageNumber = 3 THEN v_DefaultPage := r_Devices.Pi03;
				WHEN v_DefaultPageNumber = 4 THEN v_DefaultPage := r_Devices.Pi04;
				WHEN v_DefaultPageNumber = 5 THEN v_DefaultPage := r_Devices.Pi05;
				WHEN v_DefaultPageNumber = 6 THEN v_DefaultPage := r_Devices.Pi06;
				WHEN v_DefaultPageNumber = 7 THEN v_DefaultPage := r_Devices.Pi07;
				WHEN v_DefaultPageNumber = 8 THEN v_DefaultPage := r_Devices.Pi08;
				WHEN v_DefaultPageNumber = 9 THEN v_DefaultPage := r_Devices.Pi09;
				WHEN v_DefaultPageNumber = 10 THEN v_DefaultPage := r_Devices.Pi10;
				WHEN v_DefaultPageNumber = 11 THEN v_DefaultPage := r_Devices.Pi11;
				WHEN v_DefaultPageNumber = 12 THEN v_DefaultPage := r_Devices.Pi12;
				WHEN v_DefaultPageNumber = 13 THEN v_DefaultPage := r_Devices.Pi13;
				WHEN v_DefaultPageNumber = 14 THEN v_DefaultPage := r_Devices.Pi14;
				WHEN v_DefaultPageNumber = 15 THEN v_DefaultPage := r_Devices.Pi15;
				WHEN v_DefaultPageNumber = 16 THEN v_DefaultPage := r_Devices.Pi16;
				WHEN v_DefaultPageNumber = 17 THEN v_DefaultPage := r_Devices.Pi17;
				WHEN v_DefaultPageNumber = 18 THEN v_DefaultPage := r_Devices.Pi18;
				WHEN v_DefaultPageNumber = 19 THEN v_DefaultPage := r_Devices.Pi19;
				WHEN v_DefaultPageNumber = 20 THEN v_DefaultPage := r_Devices.Pi20;
				WHEN v_DefaultPageNumber = 21 THEN v_DefaultPage := r_Devices.Pi21;
				WHEN v_DefaultPageNumber = 22 THEN v_DefaultPage := r_Devices.Pi22;
				WHEN v_DefaultPageNumber = 23 THEN v_DefaultPage := r_Devices.Pi23;
				WHEN v_DefaultPageNumber = 24 THEN v_DefaultPage := r_Devices.Pi24;
				WHEN v_DefaultPageNumber = 25 THEN v_DefaultPage := r_Devices.Pi25;
				WHEN v_DefaultPageNumber = 26 THEN v_DefaultPage := r_Devices.Pi26;
				WHEN v_DefaultPageNumber = 27 THEN v_DefaultPage := r_Devices.Pi27;
				WHEN v_DefaultPageNumber = 28 THEN v_DefaultPage := r_Devices.Pi28;
				WHEN v_DefaultPageNumber = 29 THEN v_DefaultPage := r_Devices.Pi29;
				WHEN v_DefaultPageNumber = 30 THEN v_DefaultPage := r_Devices.Pi30;
				WHEN v_DefaultPageNumber = 31 THEN v_DefaultPage := r_Devices.Pi31;
				WHEN v_DefaultPageNumber = 32 THEN v_DefaultPage := r_Devices.Pi32;
				WHEN v_DefaultPageNumber = 33 THEN v_DefaultPage := r_Devices.Pi33;
				WHEN v_DefaultPageNumber = 34 THEN v_DefaultPage := r_Devices.Pi34;
				WHEN v_DefaultPageNumber = 35 THEN v_DefaultPage := r_Devices.Pi35;
				WHEN v_DefaultPageNumber = 36 THEN v_DefaultPage := r_Devices.Pi36;
				WHEN v_DefaultPageNumber = 37 THEN v_DefaultPage := r_Devices.Pi37;
				WHEN v_DefaultPageNumber = 38 THEN v_DefaultPage := r_Devices.Pi38;
				WHEN v_DefaultPageNumber = 39 THEN v_DefaultPage := r_Devices.Pi39;
				WHEN v_DefaultPageNumber = 40 THEN v_DefaultPage := r_Devices.Pi40;
				WHEN v_DefaultPageNumber = 41 THEN v_DefaultPage := r_Devices.Pi41;
				WHEN v_DefaultPageNumber = 42 THEN v_DefaultPage := r_Devices.Pi42;
				WHEN v_DefaultPageNumber = 43 THEN v_DefaultPage := r_Devices.Pi43;
				WHEN v_DefaultPageNumber = 44 THEN v_DefaultPage := r_Devices.Pi44;
				WHEN v_DefaultPageNumber = 45 THEN v_DefaultPage := r_Devices.Pi45;
				WHEN v_DefaultPageNumber = 46 THEN v_DefaultPage := r_Devices.Pi46;
				WHEN v_DefaultPageNumber = 47 THEN v_DefaultPage := r_Devices.Pi47;
				WHEN v_DefaultPageNumber = 48 THEN v_DefaultPage := r_Devices.Pi48;
				WHEN v_DefaultPageNumber = 49 THEN v_DefaultPage := r_Devices.Pi49;
				WHEN v_DefaultPageNumber = 50 THEN v_DefaultPage := r_Devices.Pi50;
			END CASE;
		ELSE
			v_DefaultPage := '';
		END IF;

		v_Seq_id := fun_get_next_sequence_ID;

		INSERT INTO WEBFIDS_TABLE_TEMP (FTYP,
									IP_ADDRESS, IP_PORT, DEVICE_NAME, DEVICE_AREA,
									ALARM_PAGE_NUMBER, DEFAULT_PAGE_NUMBER, FIRST_PAGE_NUMER,
									NUMBER_OF_PAGES, INSTALLATION_CONFIG_FILE,
									TERMINAL_NAME, GROUP_NAME, PAGE_ID, CONFIG_FILE,
									DISPLAY_SEQUENCE, NUMBER_OF_FLIGHTS, REFERENCED_FIELD,
									TIME_DELETE_1, TIME_DELETE_2, TIME_CANCELLED,
									TIME_FRAME_BEGIN, TIME_FRAME_END,
									NEW_TIME,
									STATUS,
									SEQ_ID)
				VALUES (trim(r_Devices.DevTab_Dffd),
						trim(r_Devices.DevTab_Dadr), trim(r_Devices.DevTab_Dprt), trim(r_Devices.DevTab_Devn), trim(r_Devices.DevTab_Dare),
						trim(r_Devices.Dapn), trim(r_Devices.Demt), trim(r_Devices.Dfpn),
						trim(r_Devices.Dnop), trim(r_Devices.Dicf),
						trim(r_Devices.DevTab_Dter), trim(r_Devices.DevTab_Grpn), trim(v_DefaultPage), trim(r_PagTab.Pcfn),
						'1', trim(r_PagTab.Pnof), trim(r_PagTab.Prfn),
						trim(r_PagTab.Ptd1), trim(r_PagTab.Ptd2), trim(r_PagTab.Ptdc),
						trim(r_PagTab.Ptfb), trim(r_PagTab.Ptfe),
						to_char(sysdate,'YYYYMMDDHH24MISS'),
						'N',
						v_Seq_id);
		COMMIT;

		success := -1;
		RETURN success;

	EXCEPTION
		WHEN OTHERS THEN
			pck_ceda_errors.prc_write_error('FUN_SET_DEFAULT_FOR_FDD ', 'FIDS-80000',
                           'Error-Code: ' || TO_CHAR(SQLCODE) || ' Error-Message: ' ||
						   SUBSTR(SQLERRM, 1, 150), 1);
			success := 0;
			RETURN success;

	END FUN_SET_DEFAULT_FOR_FDD;

-------------------------------------------------------------------------------------------
--      Function              - FUN_SET_DEFAULT_FOR_FLV
-------------------------------------------------------------------------------------------
	FUNCTION FUN_SET_DEFAULT_FOR_FLV
	RETURN INTEGER
	IS

    ---------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------
    --      Function              - FUN_SET_DEFAULT_FOR_FLV
    --
    --      Version               - 1.0.0
    --
    --      Written By            - Juergen Hammerschmid
    --
    --      Return                - -1 -> OK
	--
    --      Errors                - Errors will be written into the table FIDS_ERRORS
    --
    ---------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------
    --      Change Log
    --
    --      Name           Date               Version    Change
    --      ----           ----------         -------    ------
	--      Hammerschmid   31.03.2008         1.0        new
    --		Hammerschmid   03.04.2009         1.1        new defaultpage handling
    ---------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------

    success                 INTEGER := -1;
	v_Seq_id				NUMBER(22);
	v_DefaultPageNumber		INTEGER := 0;
	v_DefaultPage			r_Devices.Pi01%TYPE;
	r_PagTabNew			    PAGTAB%ROWTYPE;

	BEGIN
		-- Identify default page
		IF trim(r_Devices.Demt) IS NOT NULL THEN
			v_DefaultPageNumber := to_number(trim(r_Devices.Demt));
			CASE
				WHEN v_DefaultPageNumber = 1 THEN v_DefaultPage := r_Devices.Pi01;
				WHEN v_DefaultPageNumber = 2 THEN v_DefaultPage := r_Devices.Pi02;
				WHEN v_DefaultPageNumber = 3 THEN v_DefaultPage := r_Devices.Pi03;
				WHEN v_DefaultPageNumber = 4 THEN v_DefaultPage := r_Devices.Pi04;
				WHEN v_DefaultPageNumber = 5 THEN v_DefaultPage := r_Devices.Pi05;
				WHEN v_DefaultPageNumber = 6 THEN v_DefaultPage := r_Devices.Pi06;
				WHEN v_DefaultPageNumber = 7 THEN v_DefaultPage := r_Devices.Pi07;
				WHEN v_DefaultPageNumber = 8 THEN v_DefaultPage := r_Devices.Pi08;
				WHEN v_DefaultPageNumber = 9 THEN v_DefaultPage := r_Devices.Pi09;
				WHEN v_DefaultPageNumber = 10 THEN v_DefaultPage := r_Devices.Pi10;
				WHEN v_DefaultPageNumber = 11 THEN v_DefaultPage := r_Devices.Pi11;
				WHEN v_DefaultPageNumber = 12 THEN v_DefaultPage := r_Devices.Pi12;
				WHEN v_DefaultPageNumber = 13 THEN v_DefaultPage := r_Devices.Pi13;
				WHEN v_DefaultPageNumber = 14 THEN v_DefaultPage := r_Devices.Pi14;
				WHEN v_DefaultPageNumber = 15 THEN v_DefaultPage := r_Devices.Pi15;
				WHEN v_DefaultPageNumber = 16 THEN v_DefaultPage := r_Devices.Pi16;
				WHEN v_DefaultPageNumber = 17 THEN v_DefaultPage := r_Devices.Pi17;
				WHEN v_DefaultPageNumber = 18 THEN v_DefaultPage := r_Devices.Pi18;
				WHEN v_DefaultPageNumber = 19 THEN v_DefaultPage := r_Devices.Pi19;
				WHEN v_DefaultPageNumber = 20 THEN v_DefaultPage := r_Devices.Pi20;
				WHEN v_DefaultPageNumber = 21 THEN v_DefaultPage := r_Devices.Pi21;
				WHEN v_DefaultPageNumber = 22 THEN v_DefaultPage := r_Devices.Pi22;
				WHEN v_DefaultPageNumber = 23 THEN v_DefaultPage := r_Devices.Pi23;
				WHEN v_DefaultPageNumber = 24 THEN v_DefaultPage := r_Devices.Pi24;
				WHEN v_DefaultPageNumber = 25 THEN v_DefaultPage := r_Devices.Pi25;
				WHEN v_DefaultPageNumber = 26 THEN v_DefaultPage := r_Devices.Pi26;
				WHEN v_DefaultPageNumber = 27 THEN v_DefaultPage := r_Devices.Pi27;
				WHEN v_DefaultPageNumber = 28 THEN v_DefaultPage := r_Devices.Pi28;
				WHEN v_DefaultPageNumber = 29 THEN v_DefaultPage := r_Devices.Pi29;
				WHEN v_DefaultPageNumber = 30 THEN v_DefaultPage := r_Devices.Pi30;
				WHEN v_DefaultPageNumber = 31 THEN v_DefaultPage := r_Devices.Pi31;
				WHEN v_DefaultPageNumber = 32 THEN v_DefaultPage := r_Devices.Pi32;
				WHEN v_DefaultPageNumber = 33 THEN v_DefaultPage := r_Devices.Pi33;
				WHEN v_DefaultPageNumber = 34 THEN v_DefaultPage := r_Devices.Pi34;
				WHEN v_DefaultPageNumber = 35 THEN v_DefaultPage := r_Devices.Pi35;
				WHEN v_DefaultPageNumber = 36 THEN v_DefaultPage := r_Devices.Pi36;
				WHEN v_DefaultPageNumber = 37 THEN v_DefaultPage := r_Devices.Pi37;
				WHEN v_DefaultPageNumber = 38 THEN v_DefaultPage := r_Devices.Pi38;
				WHEN v_DefaultPageNumber = 39 THEN v_DefaultPage := r_Devices.Pi39;
				WHEN v_DefaultPageNumber = 40 THEN v_DefaultPage := r_Devices.Pi40;
				WHEN v_DefaultPageNumber = 41 THEN v_DefaultPage := r_Devices.Pi41;
				WHEN v_DefaultPageNumber = 42 THEN v_DefaultPage := r_Devices.Pi42;
				WHEN v_DefaultPageNumber = 43 THEN v_DefaultPage := r_Devices.Pi43;
				WHEN v_DefaultPageNumber = 44 THEN v_DefaultPage := r_Devices.Pi44;
				WHEN v_DefaultPageNumber = 45 THEN v_DefaultPage := r_Devices.Pi45;
				WHEN v_DefaultPageNumber = 46 THEN v_DefaultPage := r_Devices.Pi46;
				WHEN v_DefaultPageNumber = 47 THEN v_DefaultPage := r_Devices.Pi47;
				WHEN v_DefaultPageNumber = 48 THEN v_DefaultPage := r_Devices.Pi48;
				WHEN v_DefaultPageNumber = 49 THEN v_DefaultPage := r_Devices.Pi49;
				WHEN v_DefaultPageNumber = 50 THEN v_DefaultPage := r_Devices.Pi50;
			END CASE;
		ELSE
			v_DefaultPage := '';
		END IF;

		SELECT *
        INTO r_PagTabNew
        FROM PAGTAB
        WHERE PAGI = v_DefaultPage;

		v_Seq_id := fun_get_next_sequence_ID;

-- JHA 03042009
		INSERT INTO WEBFIDS_TABLE_TEMP (FTYP,
									IP_ADDRESS, IP_PORT, DEVICE_NAME, DEVICE_AREA,
									ALARM_PAGE_NUMBER, DEFAULT_PAGE_NUMBER, FIRST_PAGE_NUMER,
									NUMBER_OF_PAGES, INSTALLATION_CONFIG_FILE,
									TERMINAL_NAME, GROUP_NAME, PAGE_ID, CONFIG_FILE,
									DISPLAY_SEQUENCE, NUMBER_OF_FLIGHTS, REFERENCED_FIELD,
									RESOURCE_NAME,
									TIME_DELETE_1, TIME_DELETE_2, TIME_CANCELLED,
									TIME_FRAME_BEGIN, TIME_FRAME_END,
									NEW_TIME,
									STATUS,
									SEQ_ID)
				VALUES (trim(r_Devices.DevTab_Dffd),
						trim(r_Devices.DevTab_Dadr), trim(r_Devices.DevTab_Dprt), trim(r_Devices.DevTab_Devn), trim(r_Devices.DevTab_Dare),
						trim(r_Devices.Dapn), trim(r_Devices.Demt), trim(r_Devices.Dfpn),
						trim(r_Devices.Dnop), trim(r_Devices.Dicf),
						trim(r_Devices.DevTab_Dter), trim(r_Devices.DevTab_Grpn), trim(v_DefaultPage), trim(r_PagTabNew.Pcfn),
						'1', trim(r_PagTabNew.Pnof), trim(r_PagTabNew.Prfn),
						trim(r_Devices.Devtab_Dfco),
						trim(r_PagTabNew.Ptd1), trim(r_PagTabNew.Ptd2), trim(r_PagTabNew.Ptdc),
						trim(r_PagTabNew.Ptfb), trim(r_PagTabNew.Ptfe),
						to_char(sysdate,'YYYYMMDDHH24MISS'),
						'N',
						v_Seq_id);

/*
		INSERT INTO WEBFIDS_TABLE_TEMP (FTYP,
									IP_ADDRESS, IP_PORT, DEVICE_NAME, DEVICE_AREA,
									ALARM_PAGE_NUMBER, DEFAULT_PAGE_NUMBER, FIRST_PAGE_NUMER,
									NUMBER_OF_PAGES, INSTALLATION_CONFIG_FILE,
									TERMINAL_NAME, GROUP_NAME, PAGE_ID, CONFIG_FILE,
									DISPLAY_SEQUENCE, NUMBER_OF_FLIGHTS, REFERENCED_FIELD,
									RESOURCE_NAME,
									TIME_DELETE_1, TIME_DELETE_2, TIME_CANCELLED,
									TIME_FRAME_BEGIN, TIME_FRAME_END,
									NEW_TIME,
									STATUS,
									SEQ_ID)
				VALUES (trim(r_Devices.DevTab_Dffd),
						trim(r_Devices.DevTab_Dadr), trim(r_Devices.DevTab_Dprt), trim(r_Devices.DevTab_Devn), trim(r_Devices.DevTab_Dare),
						trim(r_Devices.Dapn), trim(r_Devices.Demt), trim(r_Devices.Dfpn),
						trim(r_Devices.Dnop), trim(r_Devices.Dicf),
						trim(r_Devices.DevTab_Dter), trim(r_Devices.DevTab_Grpn), trim(v_DefaultPage), trim(r_PagTab.Pcfn),
						'1', trim(r_PagTab.Pnof), trim(r_PagTab.Prfn),
						trim(r_Devices.Devtab_Dfco),
						trim(r_PagTab.Ptd1), trim(r_PagTab.Ptd2), trim(r_PagTab.Ptdc),
						trim(r_PagTab.Ptfb), trim(r_PagTab.Ptfe),
						to_char(sysdate,'YYYYMMDDHH24MISS'),
						'N',
						v_Seq_id);
*/
		COMMIT;

		success := -1;
		RETURN success;

	EXCEPTION
		WHEN OTHERS THEN
			pck_ceda_errors.prc_write_error('FUN_SET_DEFAULT_FOR_FLV ', 'FIDS-80000',
                           'Error-Code: ' || TO_CHAR(SQLCODE) || ' Error-Message: ' ||
						   SUBSTR(SQLERRM, 1, 150), 1);
			success := 0;
			RETURN success;

	END FUN_SET_DEFAULT_FOR_FLV;


-------------------------------------------------------------------------------------------
--      Procedure              - PRC_GET_AIRPORTDESCRIPTION
-------------------------------------------------------------------------------------------
	PROCEDURE PRC_GET_AIRPORTDESCRIPTION	(p_Code				IN	CHAR,
											 return_ShortName	OUT APTTAB.APSN%TYPE,
											 return_Name1		OUT APTTAB.APFN%TYPE,
											 return_Name2		OUT APTTAB.APN2%TYPE,
											 return_Name3		OUT APTTAB.APN3%TYPE,
											 return_Name4		OUT APTTAB.APN4%TYPE)
	IS

    ---------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------
    --      Procedure             - PRC_GET_AIRPORTDESCRIPTION
    --
    --      Version               - 1.0.0
    --
    --      Written By            - Juergen Hammerschmid
    --
    --      Return                Airport description
	--
    --      Errors                - Errors will be written into the table FIDS_ERRORS
    --
    ---------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------
    --      Change Log
    --
    --      Name           Date               Version    Change
    --      ----           ----------         -------    ------
	--      Hammerschmid   31.03.2008         1.0        new
    ---------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------

	BEGIN

		SELECT 	trim(APSN), trim(APFN), trim(APN2), trim(APN3), trim(APN4)
		INTO	return_ShortName, return_Name1, return_Name2, return_Name3, return_Name4
		FROM	APTTAB
		WHERE	APC3 = trim(p_Code);

	EXCEPTION
		WHEN NO_DATA_FOUND THEN
			return_ShortName := '';
			return_Name1 := '';
			return_Name2 := '';
			return_Name3 := '';
			return_Name4 := '';

		WHEN OTHERS THEN
			return_ShortName := '';
			return_Name1 := '';
			return_Name2 := '';
			return_Name3 := '';
			return_Name4 := '';
			pck_ceda_errors.prc_write_error('PRC_GET_AIRPORTDESCRIPTION ', 'FIDS-80000',
                           'Error-Code: ' || TO_CHAR(SQLCODE) || ' Error-Message: ' ||
						   SUBSTR(SQLERRM, 1, 150), 1);
	END PRC_GET_AIRPORTDESCRIPTION;

-------------------------------------------------------------------------------------------
--      Procedure              - PRC_GET_REMARK
-------------------------------------------------------------------------------------------
	PROCEDURE PRC_GET_REMARK	(p_Code				IN	CHAR,
								 return_Name1		OUT FIDTAB.BEME%TYPE,
								 return_Name2		OUT FIDTAB.BEMD%TYPE,
								 return_Name3		OUT FIDTAB.BET3%TYPE,
								 return_Name4		OUT FIDTAB.BET4%TYPE)
	IS

    ---------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------
    --      Procedure             - PRC_GET_REMARK
    --
    --      Version               - 1.0.0
    --
    --      Written By            - Juergen Hammerschmid
    --
    --      Return                Remark description
	--
    --      Errors                - Errors will be written into the table FIDS_ERRORS
    --
    ---------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------
    --      Change Log
    --
    --      Name           Date               Version    Change
    --      ----           ----------         -------    ------
	--      Hammerschmid   31.03.2008         1.0        new
    ---------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------

	BEGIN

		SELECT 	trim(BEME), trim(BEMD), trim(BET3), trim(BET4)
		INTO	return_Name1, return_Name2, return_Name3, return_Name4
		FROM	FIDTAB
		WHERE	trim(CODE) = trim(p_Code);

	EXCEPTION
		WHEN NO_DATA_FOUND THEN
			return_Name1 := '';
			return_Name2 := '';
			return_Name3 := '';
			return_Name4 := '';

		WHEN OTHERS THEN
			return_Name1 := '';
			return_Name2 := '';
			return_Name3 := '';
			return_Name4 := '';
			pck_ceda_errors.prc_write_error('PRC_GET_REMARK ', 'FIDS-80000',
                           'Error-Code: ' || TO_CHAR(SQLCODE) || ' Error-Message: ' ||
						   SUBSTR(SQLERRM, 1, 150), 1);
	END PRC_GET_REMARK;

-------------------------------------------------------------------------------------------
--      Procedure              - PRC_GET_FREE_FIDS_TEXT
-------------------------------------------------------------------------------------------

	PROCEDURE PRC_GET_FREE_FIDS_TEXT (p_Urno 		IN WEBFIDSFLVTAB.URNO%TYPE,
			  					      return_Text1  OUT FXTTAB.TEXT%TYPE,
                                      return_Text2  OUT FXTTAB.TEXT%TYPE)
	IS
    ---------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------
    --      Procedure             - PRC_GET_FREE_FIDS_TEXT
    --
    --      Version               - 1.1.0
    --
    --      Written By            - Juergen Hammerschmid
    --
    --      Return                -1 = OK
	--
    --      Errors                - Errors will be written into the table FIDS_ERRORS
    --
    ---------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------
    --      Change Log
    --
    --      Name           Date               Version    Change
    --      ----           ----------         -------    ------
	--      Hammerschmid   31.03.2008         1.0        new
	--      Hammerschmid   18.04.2008         1.1        FREE_TEXT now works independat of
    --													 dedicated or common checkin counter etc.
    ---------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------

    TYPE FXTtabType		        IS REF CURSOR;
    v_FXTtab_cur            	FXTTabType;
    r_FXTTAB                	FXTTAB%ROWTYPE;

    v_SQLString					VARCHAR2(255);
    v_Counter					INTEGER := 1;

    BEGIN
    /*
    	v_SQLString := 'SELECT C.FLDU, C.SORT, C.TEXT, C.TYPE, C.URNO ' ||
        			   'FROM  AFTTAB a, ' ||
	  				         'FLDTAB b, ' ||
      						 'FXTTAB c ' ||
					   'WHERE b.AURN = a.URNO ' ||
					   'AND   c.FLDU = b.URNO ' ||
                       'AND   a.URNO = ' || p_AFTUrno ||
                       'ORDER BY c.SORT';
	*/
		-- JHA 20080418
    	v_SQLString := 'SELECT FLDU, SORT, TEXT, TYPE, URNO' ||
        			   ' FROM  FXTTAB' ||
					   ' WHERE FLDU = ' || p_Urno ||
                       ' AND ROWNUM <= 2' ||
                       ' ORDER BY SORT';

		OPEN v_FXTtab_cur FOR v_SQLString;
		LOOP
			-- Process data
			FETCH v_FXTtab_cur INTO r_FXTTAB;
				EXIT WHEN v_FXTtab_cur%NOTFOUND OR
						  v_FXTtab_cur%NOTFOUND IS NULL;
            IF v_Counter = 1 THEN
            	return_Text1 := r_FXTTAB.Text;
                v_Counter := v_Counter + 1;
            ELSE
            	return_Text2 := r_FXTTAB.Text;
            END IF;

        END LOOP;
		CLOSE v_FXTtab_cur;

    EXCEPTION
    	WHEN OTHERS THEN
			pck_ceda_errors.prc_write_error('PRC_GET_FREE_FIDS_TEXT ', 'FIDS-80000',
                           'Error-Code: ' || TO_CHAR(SQLCODE) || ' Error-Message: ' ||
						   SUBSTR(SQLERRM, 1, 150), 1);

	END PRC_GET_FREE_FIDS_TEXT;

-------------------------------------------------------------------------------------------
--      Function              - FUN_GET_FORMATED_CODESHARE
-------------------------------------------------------------------------------------------
	FUNCTION FUN_GET_FORMATED_CODESHARE(p_CodeShare IN AFTTAB.JFNO%TYPE, p_Seperator IN CHAR)
	RETURN AFTTAB.JFNO%TYPE
	IS

    ---------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------
    --      Function              - FUN_GET_FORMATED_CODESHARE
    --
    --      Version               - 1.0.0
    --
    --      Written By            - Juergen Hammerschmid
    --
    --      Return                Formated Codesharestring
	--
    --      Errors                - Errors will be written into the table FIDS_ERRORS
    --
    ---------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------
    --      Change Log
    --
    --      Name           Date               Version    Change
    --      ----           ----------         -------    ------
	--      Hammerschmid   31.03.2008         1.0        new
    ---------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------

	Result 			AFTTAB.JFNO%TYPE;
	v_Length		INTEGER := 0;
	v_Loops			INTEGER := 0;
	v_StartPos		INTEGER := 0;

	BEGIN
		v_Length := length(trim(p_CodeShare));

		-- Has Codeshare more than one entry?
		-- If yes then flightnumber has fixed length of 9 characters
		-- except last entry.
		IF v_Length > 9 THEN
			-- Seperator exists?
			IF INSTR(p_CodeShare, p_Seperator) > 0 THEN
				Result := trim(p_CodeShare);
			ELSE
				-- calculate loops
				v_Loops := v_Length / 9;
				v_StartPos := 1;
				FOR i IN 1..v_Loops
				LOOP
					Result := Result || trim(substr(p_CodeShare,v_StartPos,9)) || p_Seperator;
					v_StartPos := v_StartPos + 9;
				END LOOP;
				-- delete last seperator
				Result := substr(Result, 1, length(Result) -1);
			END IF;
		ELSE
			Result := trim(p_CodeShare);
		END IF;

		RETURN(Result);

	EXCEPTION
		WHEN OTHERS THEN
			pck_ceda_errors.prc_write_error('FUN_GET_FORMATED_CODESHARE ', 'FIDS-80000',
                           'Error-Code: ' || TO_CHAR(SQLCODE) || ' Error-Message: ' ||
						   SUBSTR(SQLERRM, 1, 150) || ' ' || p_CodeShare, 1);
			RETURN  p_CodeShare;

	END FUN_GET_FORMATED_CODESHARE;

-------------------------------------------------------------------------------------------
--      Function              - FUN_GET_NEXT_SEQUENCE_ID
-------------------------------------------------------------------------------------------

	FUNCTION FUN_GET_NEXT_SEQUENCE_ID
	RETURN NUMBER
	IS
    ---------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------
    --      Function              - FUN_GET_NEXT_SEQUENCE_ID
    --
    --      Version               - 1.0.0
    --
    --      Written By            - Juergen Hammerschmid
    --
    --      Return                - New Sequence iD
	--
	--		Error				  - Errors will be written into the table FIDS_ERRORS
    --
    ---------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------
    --      Change Log
    --
    --      Name           Date               Version    Change
    --      ----           ----------         -------    ------
	--      Hammerschmid   31.03.2008         1.0        new
    ---------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------

	v_Seq_ID			NUMBER(22);

	BEGIN
		-- get next Sequence ID for dispays
		SELECT FIDS_SEQ.Nextval
		INTO v_Seq_ID
		FROM dual;
		RETURN v_Seq_ID;
	END FUN_GET_NEXT_SEQUENCE_ID;

-------------------------------------------------------------------------------------------
--      Function              - FUN_GET_LOCAL_TIME
-------------------------------------------------------------------------------------------
	FUNCTION FUN_GET_LOCALTIME	(p_TIMEIN IN VARCHAR,
								 p_HOPO IN VARCHAR )
	RETURN VARCHAR
    IS

    ---------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------
    --      Function              - FUN_GET_LOCAL_TIME
    --
    --      Version               - 1.0.0
    --
    --      Written By            - Juergen Hammerschmid
    --
    --      Return                -1 = OK
	--
	--		Error				  - Errors will be written into the table FIDS_ERRORS
    --
    ---------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------
    --      Change Log
    --
    --      Name           Date               Version    Change
    --      ----           ----------         -------    ------
	--      Hammerschmid   31.03.2008         1.0        new
    ---------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------

	-- Variables
	c_TICH VARCHAR(14);
	c_TDI1 VARCHAR(4);
	c_TDI2 VARCHAR(4);
	c_RETURNVALUE VARCHAR(14) := NULL;


	BEGIN
		-- Get the current time parameters for the related Airport.
		SELECT 	TDI1,TDI2
  		INTO 	c_TDI1, c_TDI2
		FROM 	APTTAB
		WHERE 	TICH = (SELECT 	MAX(TICH)
        				FROM 	APTTAB
                        WHERE 	HOPO = p_HOPO
                        AND 	APC3 = p_HOPO)
		AND 	HOPO = p_HOPO
        AND 	APC3 = p_HOPO;

		-- Do we have winter or daylight time. In the APTTAB the TICH is an indicator
		-- for this. TDI1 is the diff before TICH and TDI2 after TICH.
		-- So compare the actual date with TICH and take TDI1 or TDI2.
		SELECT 	MAX(TICH)
		INTO 	c_TICH
		FROM 	APTTAB
		WHERE 	(TO_CHAR(SYSDATE,'YYYYMMDDHH24MISS') > TICH)
		AND 	HOPO = p_HOPO
        AND 	APC3 = p_HOPO;

		IF (c_TICH != ' ') THEN
			-- USE TDI2
			c_RETURNVALUE := TO_CHAR(to_date(p_TIMEIN,'YYYYmmddHH24MISS') + c_TDI2/1440,'YYYYMMDDHH24MISS');
		ELSE
			-- USE TDI1
			c_RETURNVALUE := TO_CHAR(to_date(p_TIMEIN,'YYYYmmddHH24MISS') + c_TDI1/1440,'YYYYMMDDHH24MISS');
		END IF;

		-- return the UTC date
		RETURN  c_RETURNVALUE;

	EXCEPTION
    	WHEN no_data_found THEN
			pck_ceda_errors.prc_write_error('FUN_GET_LOCAL_TIME ', 'FIDS-80001',
                           'Error-Code: ' || TO_CHAR(SQLCODE) || ' Error-Message: ' ||
						   SUBSTR(SQLERRM, 1, 150) || ' in APTTAB cursor', 1);
			RETURN  c_RETURNVALUE;
        WHEN OTHERS THEN
			pck_ceda_errors.prc_write_error('FUN_GET_LOCAL_TIME ', 'FIDS-80000',
                           'Error-Code: ' || TO_CHAR(SQLCODE) || ' Error-Message: ' ||
						   SUBSTR(SQLERRM, 1, 150) || ' in APTTAB', 1);
			RETURN  c_RETURNVALUE;
	END FUN_GET_LOCALTIME;

-------------------------------------------------------------------------------------------
--      Function              - FUN_GET_UTCOFFSET
-------------------------------------------------------------------------------------------
	FUNCTION FUN_GET_UTCOFFSET	(p_TIMEIN IN VARCHAR, -- in CEDAFORMAT
								 p_HOPO IN VARCHAR )
	RETURN VARCHAR2
    IS

    ---------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------
    --      Function              - FUN_GET_UTCOFFSET
    --
    --      Version               - 1.0.0
    --
    --      Written By            - Juergen Hammerschmid
    --
    --      Return                -1 = OK
	--
	--		Error				  - Errors will be written into the table FIDS_ERRORS
    --
    ---------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------
    --      Change Log
    --
    --      Name           Date               Version    Change
    --      ----           ----------         -------    ------
	--      Hammerschmid   26.10.2008         1.0        new
    ---------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------

	-- Variables
    r_Season		SEATAB%ROWTYPE;
    r_Offset		APTTAB%ROWTYPE;
	c_RETURNVALUE 	APTTAB.TDIS%TYPE := '0';

	BEGIN
    	-- Get record in APPTAB for retaled Airpost to fetch offsets
    	SELECT *
        INTO r_Offset
        FROM APTTAB
        WHERE HOPO = p_HOPO
        AND APC3 = p_HOPO;

		-- Get the current season for related Airport.
		SELECT 	*
  		INTO 	r_Season
		FROM 	SEATAB
		WHERE 	p_TIMEIN BETWEEN VPFR AND VPTO
		AND 	HOPO = p_HOPO;

		IF SUBSTR(r_Season.Vpfr,1,4) = SUBSTR(r_Season.Vpto,1,4) THEN
        	-- Summertime
            c_RETURNVALUE := r_Offset.Tdis;
        ELSE
        	-- Wintertime
            c_RETURNVALUE := r_Offset.Tdiw;
        END IF;

        RETURN c_RETURNVALUE;

	EXCEPTION
    	WHEN no_data_found THEN
        	IF TRIM(p_TIMEIN) IS NOT NULL THEN
                pck_ceda_errors.prc_write_error('FUN_GET_UTCOFFSET ', 'FIDS-80001',
                               'Error-Code: ' || TO_CHAR(SQLCODE) || ' Error-Message: ' ||
                               SUBSTR(SQLERRM, 1, 150) || ' in APTTAB cursor', 1);
            END IF;
			RETURN  c_RETURNVALUE;
        WHEN OTHERS THEN
        	IF TRIM(p_TIMEIN) IS NOT NULL THEN
                pck_ceda_errors.prc_write_error('FUN_GET_UTCOFFSET ', 'FIDS-80000',
                               'Error-Code: ' || TO_CHAR(SQLCODE) || ' Error-Message: ' ||
                               SUBSTR(SQLERRM, 1, 150) || ' in APTTAB', 1);
            END IF;
			RETURN  c_RETURNVALUE;
	END FUN_GET_UTCOFFSET;

-------------------------------------------------------------------------------------------
--      Procedure              - PRC_SET_WEBFIDS_SCHEDULER_JOB
-------------------------------------------------------------------------------------------
	PROCEDURE PRC_SET_WEBFIDS_SCHEDULER_JOB
	IS

	---------------------------------------------------------------------------------------
	---------------------------------------------------------------------------------------
	--	Procedure	        -	PRC_SET_WEBFIDS_SCHEDULER_JOB
	--
	--	Version			    -	1.0.0
	--
	--	Written By	      	-	Jurgen Hammerschmid
	--
	--	Procedure Desc.		-	NULL
	--
	--  Input-Parameters
	--
	--  Errors              Errors will be written into the table CEDA_ERRORS
	--
	---------------------------------------------------------------------------------------
	---------------------------------------------------------------------------------------
	--	Change Log
	--
	--	Name           Date               Version    Change
	--	----           ----------         -------    ------
	--  Hammerschmid   17.09.2009         1.0        new
	---------------------------------------------------------------------------------------
	---------------------------------------------------------------------------------------


	PRAGMA AUTONOMOUS_TRANSACTION;

	BEGIN
    	DBMS_SCHEDULER.create_job (
        job_name => 'WEBFIDS_CYCLIC_JOB',
        job_type => 'PLSQL_BLOCK',
        job_action => 'begin PRC_WEBFIDS.PRC_WEBFIDS_SCHEDULER_JOB end; ',
        start_date => sysdate,
        repeat_interval => 'FREQ=minutely',
        enabled => TRUE,
        comments => '--WEBFIDS_JOB');
    EXCEPTION
    	WHEN OTHERS THEN
			PCK_CEDA_ERRORS.PRC_WRITE_ERROR('PRC_SCHEDULER_JOB ', 'FIDS-80000',
                           'Error-Code: ' || TO_CHAR(SQLCODE) || ' Error-Message: ' ||
						   SUBSTR(SQLERRM, 1, 150), 9);


    END PRC_SET_WEBFIDS_SCHEDULER_JOB;

-------------------------------------------------------------------------------------------
--      Procedure              - PRC_WEBFIDS_SCHEDULER_JOB
-------------------------------------------------------------------------------------------
	PROCEDURE PRC_WEBFIDS_SCHEDULER_JOB
	IS

	---------------------------------------------------------------------------------------
	---------------------------------------------------------------------------------------
	--	Procedure	        -	PRC_WEBFIDS_SCHEDULER_JOB
	--
	--	Version			    -	1.2.0
	--
	--	Written By	      	-	Jurgen Hammerschmid
	--
	--	Procedure Desc.		-	NULL
	--
	--  Input-Parameters
	--
	--  Errors              Errors will be written into the table CEDA_ERRORS
	--
	---------------------------------------------------------------------------------------
	---------------------------------------------------------------------------------------
	--	Change Log
	--
	--	Name           Date               Version    Change
	--	----           ----------         -------    ------
	--  Hammerschmid   17.09.2009         1.0        new
	---------------------------------------------------------------------------------------
	---------------------------------------------------------------------------------------

	    v_SEQ_ID		NUMBER(9);

        v_TimeStart		DATE;
        v_TimeEnd		DATE;
        v_GoAhead		BOOLEAN := FALSE;
	    v_WEB_ID    	NUMBER(9);

	BEGIN

        BEGIN
            SELECT WEBFIDS_SEQ.Nextval
            INTO v_WEB_ID
            FROM dual;

            SELECT SEQ_ID, P_START
            INTO v_Seq_ID, v_TimeStart
            FROM TR_LOG
            WHERE p_end IS NULL
            AND ERROR_FLAG <> 'Y'
            AND p_status <> 'UNKNOWN';

            IF (SYSDATE - v_TimeStart) > 60/86400 THEN
                UPDATE TR_LOG SET ERROR_FLAG = 'Y' WHERE seq_id = v_Seq_ID;
                UPDATE WEBFIDS_TABLE_TEMP SET Status = '';
                COMMIT;
            END IF;
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
            	v_GoAhead := TRUE;
        END;
        IF v_GoAhead = TRUE THEN
            SELECT TR_LOG_SEQ.Nextval
            INTO v_Seq_ID
            FROM dual;

            v_TimeStart := SYSDATE;
            INSERT INTO TR_LOG VALUES (v_Seq_ID,'WEBFIDS_JOB',v_TimeStart,null,'JOB','N');
            PCK_WEBFIDS.PRC_FIDS_GET_DATA_FOR_DISPLAY;
            v_TimeEnd := SYSDATE;
            UPDATE TR_LOG SET P_END = v_TimeEnd where seq_id = v_Seq_ID;
            COMMIT;

            IF v_TimeEnd - v_TimeStart > 50/86400 THEN
                PCK_CEDA_ERRORS.PRC_WRITE_ERROR('PRC_WEBFIDS_JOB ', 'FIDS-80000',
                                                'WEBFIDS JOB RUNS TO LONG',1);
            END IF;
--            PCK_WEBFIDS.PRC_EXECUTE_WEBFIDS_JOB;
        END IF;

	END PRC_WEBFIDS_SCHEDULER_JOB;

-------------------------------------------------------------------------------------------
--      Procedure              - PRC_WEBFIDS_JOB
-------------------------------------------------------------------------------------------
	PROCEDURE PRC_WEBFIDS_JOB
	IS

	---------------------------------------------------------------------------------------
	---------------------------------------------------------------------------------------
	--	Procedure	        -	PRC_WEBFIDS_JOB
	--
	--	Version			    -	1.2.0
	--
	--	Written By	      	-	Jurgen Hammerschmid
	--
	--	Procedure Desc.		-	NULL
	--
	--  Input-Parameters
	--
	--  Errors              Errors will be written into the table CEDA_ERRORS
	--
	---------------------------------------------------------------------------------------
	---------------------------------------------------------------------------------------
	--	Change Log
	--
	--	Name           Date               Version    Change
	--	----           ----------         -------    ------
	--  Hammerschmid   31.03.2008         1.0        new
	--  Hammerschmid   28.04.2008		  1.1        check duration of job
    --  Hammerschmid   20.05.2008		  1.2        serialize jobs and kill overrunning
	---------------------------------------------------------------------------------------
	---------------------------------------------------------------------------------------

	jobno			NUMBER;
   	v_Job_check		NUMBER := 0;

	BEGIN
		BEGIN
			-- check if job is already running
			SELECT job
    		INTO v_Job_check
			FROM SYS.user_Jobs
			WHERE what LIKE '%PRC_FIDS_GET_DATA_FOR_DISPLAY%';
    	EXCEPTION
    		WHEN NO_DATA_FOUND THEN
        		v_Job_check := 0;
	    	WHEN OTHERS THEN
				PCK_CEDA_ERRORS.PRC_WRITE_ERROR('PRC_WEBFIDS_JOB ', 'FIDS-80000',
                           'Error-Code: ' || TO_CHAR(SQLCODE) || ' Error-Message: ' ||
						   SUBSTR(SQLERRM, 1, 150), 9);
		END;
		IF v_Job_check <> 0 THEN
			--remove old job and create new
			dbms_job.remove(v_Job_check);
	    END IF;
		-- create job
	    DBMS_JOB.SUBMIT(jobno,
		'DECLARE
	    v_SEQ_ID	NUMBER(9);
        v_TimeStart		DATE;
        v_TimeEnd		DATE;
        v_GoAhead		BOOLEAN := FALSE;
	    v_WEB_ID    	NUMBER(9);

		BEGIN
        BEGIN
	    SELECT WEBFIDS_SEQ.Nextval
	    INTO v_WEB_ID
	    FROM dual;
        SELECT SEQ_ID, P_START
        INTO v_Seq_ID, v_TimeStart
        FROM TR_LOG
        WHERE p_end IS NULL
        AND ERROR_FLAG <> ''Y''
        AND p_status <> ''UNKNOWN'';
        IF (SYSDATE - v_TimeStart) > 60/86400 THEN
		UPDATE TR_LOG SET ERROR_FLAG = ''Y'' WHERE seq_id = v_Seq_ID;
        UPDATE WEBFIDS_TABLE_TEMP SET Status = '''';
        COMMIT;
        END IF;
        EXCEPTION
        WHEN NO_DATA_FOUND THEN
        v_GoAhead := TRUE;
        END;
        IF v_GoAhead = TRUE THEN
    	SELECT TR_LOG_SEQ.Nextval
		INTO v_Seq_ID
	    FROM dual;
        v_TimeStart := SYSDATE;
		INSERT INTO TR_LOG VALUES (v_Seq_ID,''WEBFIDS_JOB'',v_TimeStart,null,''JOB'',''N'');
		PCK_WEBFIDS.PRC_FIDS_GET_DATA_FOR_DISPLAY;
        v_TimeEnd := SYSDATE;
		UPDATE TR_LOG SET P_END = v_TimeEnd where seq_id = v_Seq_ID;
        COMMIT;
        IF v_TimeEnd - v_TimeStart > 50/86400 THEN
			PCK_CEDA_ERRORS.PRC_WRITE_ERROR(''PRC_WEBFIDS_JOB '', ''FIDS-80000'',
											''WEBFIDS JOB RUNS TO LONG'',1);
        END IF;
        PCK_WEBFIDS.PRC_EXECUTE_WEBFIDS_JOB;
        END IF;
        END;',
		SYSDATE,
		'SYSDATE + 60/86400',
		FALSE);
		COMMIT;

	EXCEPTION
		WHEN OTHERS THEN
    		ROLLBACK;
			PCK_CEDA_ERRORS.PRC_WRITE_ERROR('PRC_WEBFIDS_JOB ', 'FIDS-80000',
                  'Error-Code: ' || TO_CHAR(SQLCODE) || ' Error-Message: ' ||
				   SUBSTR(SQLERRM, 1, 150), 1);

	END PRC_WEBFIDS_JOB;

-------------------------------------------------------------------------------------------
--      Procedure              - PRC_EXECUTE_WEBFIDS_JOB
-------------------------------------------------------------------------------------------
	PROCEDURE PRC_EXECUTE_WEBFIDS_JOB
	IS

	---------------------------------------------------------------------------------------
	---------------------------------------------------------------------------------------
	--	Procedure	        -	PRC_EXECUTE_WEBFIDS_JOB
	--
	--	Version			    -	1.0.0
	--
	--	Written By	      	-	Jurgen Hammerschmid
	--
	--	Procedure Desc.		-	NULL
	--
	--  Input-Parameters
	--
	--  Errors              Errors will be written into the table CEDA_ERRORS
	--
	---------------------------------------------------------------------------------------
	---------------------------------------------------------------------------------------
	--	Change Log
	--
	--	Name           Date               Version    Change
	--	----           ----------         -------    ------
	--  Hammerschmid   14.05.2008         1.0        new
	---------------------------------------------------------------------------------------
	---------------------------------------------------------------------------------------

	v_TimeStart		DATE;
    v_TimeEnd		DATE;
    v_Seq_ID		NUMBER(12);

    BEGIN
    	BEGIN
    		SELECT NEXT_DATE, LAST_DATE
    		INTO v_TimeEnd, v_TimeStart
    		FROM SYS.USER_JOBS
    		WHERE WHAT LIKE '%REMOTE_WEBFIDSJOB%';
        EXCEPTION
        	WHEN OTHERS THEN
            	v_TimeStart := SYSDATE;
                v_TimeEnd := SYSDATE + 11*30/86400;
        END;
        BEGIN
            IF v_TimeEnd - v_TimeStart < 10*30/86400 THEN
                SELECT TR_LOG_SEQ.Nextval
                INTO v_Seq_ID
                FROM dual;
                INSERT INTO TR_LOG VALUES (v_Seq_ID,'REMOTE_WEBFIDS_JOB',SYSDATE,null,'JOB','N');
                DELETE REMOTE_FIDS_TABLE;
                INSERT INTO REMOTE_FIDS_TABLE (SELECT * FROM FIDS_TABLE);
                UPDATE TR_LOG SET P_END = SYSDATE WHERE SEQ_ID = v_Seq_ID;
                COMMIT;
            END IF;
        EXCEPTION
        	WHEN OTHERS THEN
            NULL;
        END;
    EXCEPTION
        WHEN OTHERS THEN
        NULL;
	END PRC_EXECUTE_WEBFIDS_JOB;

-------------------------------------------------------------------------------------------
--      Procedure              - PRC_HOUSEKEEPING_JOB
-------------------------------------------------------------------------------------------
	PROCEDURE PRC_HOUSEKEEPING_JOB (p_HOUROFDAY IN VARCHAR2)
	IS

	---------------------------------------------------------------------------------------
	---------------------------------------------------------------------------------------
	--	Procedure	        -	PRC_HOUSEKEEPING_JOB
	--
	--	Version			    -	1.1.0
	--
	--	Written By	      	-	Jurgen Hammerschmid
	--
	--	Procedure Desc.		-	NULL
	--
	--  Input-Parameters
	--
	--  Errors              Errors will be written into the table CEDA_ERRORS
	--
	---------------------------------------------------------------------------------------
	---------------------------------------------------------------------------------------
	--	Change Log
	--
	--	Name           Date               Version    Change
	--	----           ----------         -------    ------
	--  Hammerschmid   31.03.2008         1.0        new
	---------------------------------------------------------------------------------------
	---------------------------------------------------------------------------------------

	v_NewHourOfDay	NUMBER(20);
	jobno			NUMBER;
   	v_Job_check		NUMBER := 0;

	BEGIN
		BEGIN
			-- check if job is already running
			SELECT job
    		INTO v_Job_check
			FROM SYS.user_Jobs
			WHERE what LIKE '%PRC_HOUSEKEEPING%';
    	EXCEPTION
    		WHEN NO_DATA_FOUND THEN
        		v_Job_check := 0;
	    	WHEN OTHERS THEN
				PCK_CEDA_ERRORS.PRC_WRITE_ERROR('PRC_HOUSEKEEPING_JOB ', 'FIDS-80000',
                           'Error-Code: ' || TO_CHAR(SQLCODE) || ' Error-Message: ' ||
						   SUBSTR(SQLERRM, 1, 150), 1);
		END;
        -- Check if hour of day is in the past
        IF to_Number(to_char(sysdate , 'HH24')) > to_Number(p_HourOfDay) THEN
			v_NewHourOfDay := 1;
        ELSE
			v_NewHourOfDay := 0;
        END IF;
		IF v_Job_check <> 0 THEN
			--remove old job and create new
			dbms_job.remove(v_Job_check);
	    END IF;
		-- create job
	    DBMS_JOB.SUBMIT(jobno,
		'DECLARE
	    v_SEQ_ID	NUMBER(9);
		BEGIN
    	SELECT TR_LOG_SEQ.Nextval
	   	INTO v_Seq_ID
    	FROM dual;
	    INSERT INTO TR_LOG VALUES (v_Seq_ID,''HOUSEKEEPING_JOB'',sysdate,null,''JOB'',''N'');
	    PCK_WEBFIDS.PRC_HOUSEKEEPING;
	    UPDATE TR_LOG SET P_END = sysdate where seq_id = v_Seq_ID;
		END;',
	   	TRUNC(SYSDATE) + v_NewHourOfDay + (p_HOUROFDAY/24),
		'TRUNC(SYSDATE) + 1 + (' || p_HOUROFDAY || '/24)',
		FALSE);
		COMMIT;

	EXCEPTION
		WHEN OTHERS THEN
    		ROLLBACK;
			PCK_CEDA_ERRORS.PRC_WRITE_ERROR('PRC_HOUSEKEEPING_JOB ', 'FIDS-80000',
                  'Error-Code: ' || TO_CHAR(SQLCODE) || ' Error-Message: ' ||
				   SUBSTR(SQLERRM, 1, 150), 1);

	END PRC_HOUSEKEEPING_JOB;

-------------------------------------------------------------------------------------------
--      Procedure              - PRC_HOUSEKEEPING
-------------------------------------------------------------------------------------------
	PROCEDURE PRC_HOUSEKEEPING
	IS

	---------------------------------------------------------------------------------------
	---------------------------------------------------------------------------------------
	--	Procedure	        -	PRC_HOUSEKEEPING
	--
	--	Version			    -	1.1.0
	--
	--	Written By	      	-	Jurgen Hammerschmid
	--
	--	Procedure Desc.		-	NULL
	--
	--  Input-Parameters
	--
	--  Errors              Errors will be written into the table CEDA_ERRORS
	--
	---------------------------------------------------------------------------------------
	---------------------------------------------------------------------------------------
	--	Change Log
	--
	--	Name           Date               Version    Change
	--	----           ----------         -------    ------
	--  Hammerschmid   31.03.2008         1.0        new
    --  Hammerschmid   09.04.2008         1.1        Housekeeping only 1 day
	---------------------------------------------------------------------------------------
	---------------------------------------------------------------------------------------

    BEGIN
    	BEGIN
		    DELETE FROM TR_LOG
    		WHERE TRUNC(P_START) <= TRUNC(SYSDATE-1);
        	COMMIT;

	    EXCEPTION
			WHEN OTHERS THEN
    			ROLLBACK;
				PCK_CEDA_ERRORS.PRC_WRITE_ERROR('PRC_HOUSEKEEPING ', 'FIDS-80000',
                	  'Error-Code: ' || TO_CHAR(SQLCODE) || ' TR_LOG Error-Message: ' ||
				   	SUBSTR(SQLERRM, 1, 150), 1);
    	END;

        BEGIN
	    	DELETE FROM CEDA_ERRORS
    		WHERE TRUNC(DATE_TIME) <= TRUNC(SYSDATE-1);
        	COMMIT;

	    EXCEPTION
    	    WHEN OTHERS THEN
            ROLLBACK;
            PCK_CEDA_ERRORS.PRC_WRITE_ERROR('PRC_HOUSEKEEPING ', 'FIDS-80000',
                  'Error-Code: ' || TO_CHAR(SQLCODE) || ' CEDA_ERRORS Error-Message: ' ||
                   SUBSTR(SQLERRM, 1, 150), 1);
        END;

	END PRC_HOUSEKEEPING;

------------------------------------------------------------------------------------------
--      Function                 - FUN_TRUNCATE_TABLE (p_Tabble IN VARCHAR2)
-------------------------------------------------------------------------------------------
	FUNCTION FUN_TRUNCATE_TABLE (p_Table IN VARCHAR2)
	RETURN INTEGER
	IS
    ---------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------
    --      Procedure             - FUN_TRUNCATE_USED_URNOS
    --
    --      Version               - 1.0.0
    --
    --      Return                -1 = OK
	--
    --      Written By            - Jurgen Hammerschmid
    --
    --      Errors                - Errors will be written into the table CEDA_ERRORS
    --
    ---------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------
    --      Change Log
    --
    --      Name           Date               Version    Change
    --      ----           ----------         -------    ------
    --      Hammerschmid   17.09.2008         1.0.0      new
    ---------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------

    v_Success 			INTEGER := -1;

	PRAGMA AUTONOMOUS_TRANSACTION;

    BEGIN
    	EXECUTE IMMEDIATE
        'TRUNCATE TABLE ' || p_Table;
        COMMIT;
    	RETURN v_Success;
    EXCEPTION
    	WHEN OTHERS THEN
        	ROLLBACK;
    		v_Success := 0;
			PCK_CEDA_ERRORS.PRC_WRITE_ERROR('FUN_TRUNCATE_TABLE ', 'URNODISPOSER-80000',
           		'Error-Code: ' || TO_CHAR(SQLCODE) || ' Error-Message: ' ||
				SUBSTR(SQLERRM, 1, 150) || ' when others', 1);
			RETURN v_Success;
    END FUN_TRUNCATE_TABLE;

END PCK_WEBFIDS;
/
