#ifndef _DEF_mks_version
  #define _DEF_mks_version
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version[] = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Library/Inc/cdi2.h 1.5 2010/05/04 19:32:54SGT jha Exp  $";
#endif /* _DEF_mks_version */
/* ********************************************************************** */
/*  The Master include file.                                              */
/*                                                                        */
/*  Program       :
 *                               */
/*                                                                        */
/*  Revision date :                                                       */
/*                                                                        */
/*  Author    	  :                                                       */
/*                                                                        */
/*                                                                        */
/* ********************************************************************** */

#ifndef 	__CDI2_INC
#define 	__CDI2_INC
/* -------------------------------------------------------------------------- */
/* -------------------------------------------------------------------------- */
/* -------------------------------------------------------------------------- */
#define	cCOMMA							','
#define	iSECONDS 						1
#define	iMICRO_SECONDS					0
#define	iMAX_DATA						10*iMAX_BUF_SIZE /* 10 kB */
#define	s14_BLANKS						"              "
#define	MORE_DEBUG						1000
#define	OFF								0

/* classes */
#define	iCTL_CLASS						0
#define	iDAT_CLASS						1
#define	iINS_CLASS						4
#define	iDEL_CLASS						2
#define	iCCO_CLASS						3 /* internal use only */

/* command types */
#define	iINVENTORY_CMD					0
#define	iUPDATE_CMD						1
#define	iFREE_QUERY_CMD				2
#define	iINCOMING_DATA_CMD			3
#define	iINTERNAL_CMD					4

/* for action section */
#define	iACTIVE							1
#define	iNOT_ACTIVE						0

/* CCO- or DBL-Check	*/
#define	iCCO_CHECK						0
#define	iDBL_CHECK						1

/* for rule-handling */
#define 	iRULE_START_POS				0
#define	iRULE_LENGTH					1

/* diverses */
#define 	iCDI_RESET						-150
#define	iCDI_TERMINATE					-151
#define 	iONE_2_TWO						-152
#define	iTWO_2_ONE						-153
#define	iNOT_CHANGED					RC_SUCCESS
#define	iFOUND							1
#define	iNOT_FOUND						0
#define	iPULL_TCP_ALLOWED				0
#define	iPULL_TCP_NOT_ALLOWED		1
#define	iOFFSET_IS_UNKNOWN			0
#define	iOFFSET_IS_KNOWN				1

/* defines for send_message */
#define	iERROR_FROM_DEST				513
#define	iCREATE_CONNECTION			514
#define	iDELETE_CONNECTION			515
#define	iWERR_TO_CLIENT				516

/* errorcodes for ERR-Command */
#define	sWRONG_SERVERSYSTEM			"WY"
#define	sUNKNOWN_SENDER				"US"
#define	sUNKNOWN_RECEIVER				"UR"
#define	sTIMEOUT							"TO"
#define	sWRONG_COMMAND					"WC"
#define	sUNKNOWN_CLASS					"UC"
#define	sWRONG_TIMESTAMP				"WT"
#define	sWRONG_LENGTH					"WL"
#define	sWRONG_NUMBER					"WN"
#define	sCCO_TNUMBER					"CN"
#define  sAC_ERROR						"AC"
#define	sNO_DATA							"ND"
#define	sWRONG_SUBCOMMAND				"WS"
#define	sSEPARATOR_MISSING			"SM"
#define	sINVALID_CMD					"IC"
#define	sWRONG_TABLE_LENGTH			"TL"
#define	sWRONG_FIELD_LENGTH			"FL"
#define	sTABLE_MISSING					"TM"
#define	sFIELDS_MISSING				"FM"
#define	sUNDEFINED_TABLE				"UT"
#define	sUNDEFINED_FIELDS				"UF"

/* for change header... */
#define	iSERVER_SYSTEM					0
#define	iSENDER							1
#define	iRECEIVER						2
#define	iVERSION							3
#define	iTIME_STAMP						4	
#define	iTELE_NUMBER					5
#define	iTELE_CLASS						6
#define	iLENGTH							7

/* defines for HandleFileRecovery */
#define  iCLEAR_ALL						0
#define 	iADD_CMD							1
#define	iALL_TELENO_AT_TIME			2
#define	iALL_TELENO_AT_NO				3
#define	iGET_TELENO_CMD				4
#define	iANZ_FAIL						-20

/* define for string handling functions */
#define	iDELETE_ALL_BLANKS			0
#define	iONE_BLANK_REMAINING			1
#define	iKEEP_DATA_UNTOUCHED			2

/* macros */
#define	VERSION(a,b,c) ((a) >= 200 ? (b) : (c))
#define	HEADER_LENGTH(a) ((a) >= 200 ? iCDI2_HEADER_LEN : iCDI1_HEADER_LEN)
#define 	END_LENGTH(a) ((a) >= 200 ? iCDI2_END_LEN : iCDI1_END_LEN)	
#define 	ERR_LENGTH(a) ((a) >= 200 ? iCDI2_ERR_LEN : iCDI1_ERR_LEN)	
#define	CCO_LENGTH(a) ((a) >= 200 ? iCDI2_CCO_LEN : iCDI1_CCO_LEN)
#define	EOD_LENGTH(a) ((a) >= 200 ? iCDI2_EOD_LEN : iCDI1_EOD_LEN)
#define	DBL_LENGTH(a) ((a) >= 200 ? iCDI2_DBL_LEN : iCDI1_DBL_LEN)
#define  UNIQUE(a,b,c) (((a)%((b)+1)) < (c) ? (c) : ((a)%((b)+1)))

/* debug macros */
#define 	CMD_TYPE(a) ((a) == iINVENTORY_CMD ? "Inventory Command(s)" : ((a) == iUPDATE_CMD ? "Update Command(s)" : ((a) == iFREE_QUERY_CMD ? "Free Query Command(s)" :((a) == iINTERNAL_CMD ? "Internal Command(s)": ((a) == iINCOMING_DATA_CMD ? "Incoming Data Command(s)" : "Unknown Command(s)")))))

#define	CTRL_STA(a) ((a) == HSB_STANDBY ? "STANDBY" : ((a) == HSB_COMING_UP ? "COMING UP" : ((a) == HSB_ACTIVE ? "ACTIVE" : ((a) == HSB_STANDALONE ? "STANDALONE" : ((a) == HSB_ACT_TO_SBY ? "ACTIVE TO STANDBY" : "OTHER STATUS")))))

#define	RETURN(a) ((a) == iCDI_RESET ? "RESET" : ((a) == iCDI_TERMINATE ? "TERMINATE" : ((a) == iONE_2_TWO ? "VERSION CHANGED 2 TO 1" : "VERSION CHANGED 1 TO 2")))

#define	FKT(a) ((a) == iINSERT_SECTION ? "INSERT" : "DELETE")

/* -------------------------------------------------------------------------- */
/* -------------------------------------------------------------------------- */
/* -------------------------------------------------------------------------- */
typedef struct _CDIEvent
{
	int 		iLength;
	char		*pcEvent;
} CDIEvent;

typedef struct _CDITcpPackage
{
	int 		iLength;
	char		*pcPackage;
} CDITcpPackage;

typedef struct _CDIRecover
{
	int		iLength;	/* length of recover entry */
	char		pcStartTimeStamp[15]; /* timestamp of telegram */
	char		pcEndTimeStamp[15]; /* optionally, end od update service */
	char		pcStartTeleNo[6];	/* number of telegram */
	char		pcEndTeleNo[6]; /* end telegram number */
	char		pcData[10*iMAX_BUF_SIZE]; /* data of this telegram (10 kB) */
	char		*pcAllValidNo; /* all valid telegram numbers */
} CDIRecover;

typedef struct _CDISave
{
	int					iItemIsStored; /* only a flag */
	int					iCmdNo; /* cmd-number of this item */
	int					iTabNo; /* table-number of this item */
	int					iCmdType; /* cmd-type of this item */
	char					*pcSelection; /* item selection */
	char					*pcFields; /* item fields */
	char					*pcData; /* item data */
	struct _CDITable 	*prCDITable; /* pointer to table-definition */
} CDISave;

typedef struct _CDITable
{
	int		iNoCurExternalFieldList; /* specially for free query cmd */
	char		pcCurExternalFieldList[2*iMAX_BUF_SIZE]; /* spec. for free query */
	char		pcFieldTableAssignment[iMAX_DATA]; /* field assignment */
        char            pcExternAssign[iMAX_DATA/2];
        char            pcInternAssign[iMAX_DATA/2];
	int		iFieldTableAssignment; /* corresponding field assignm. counter */
	int		iEmptyFieldHandling; /* handle the empty fields... */
	int		iIgnoreEmptyFields; /* ignore all empty fields... */
	int		iTelegramType; /* Data- or Delete-Telegram */
	char		pcTelegramKey[iMIN]; /* key for GetUniqueNumber... */
	int		iModID; /* mod-id we send request to, default is router-id */
	int		iNoExternalFields; /* external field counter */
	char		pcExternalFieldList[2*iMAX_BUF_SIZE]; /* 2k for fields... */
	int		iNoInternalFields; /* internal field counter */
	char		pcInternalFieldList[2*iMAX_BUF_SIZE]; /* field list for request */
	int		iVia34; /* 3 or 4-letter-code for vias */
	char		pcSndCmd[10]; /* command we send */
	char		pcDataType[3]; /* data type in header structure */
	char		pcTableName[10]; /* the table name */
	char		pcSelection[iMAX_BUF_SIZE]; /* selection could be long */
	char		pcCurrentSelection[iMAX_BUF_SIZE]; /* last selection */
	int		iUseReceivedSelection;/* should i use selection comming via TCPIP */
	int		iTimeMapping; /* map from UTC to LOCAL, LOCAL to UTC */
        int             iTimeMappingLocal2Utc; /* map Local to UTC time while building DAT */
	char		pcTimeMappingFields[iMAX_BUF_SIZE]; /* field list for mapping */
	int		iNoTimeFrameFields; /* timeframe field counter */
	char		pcTimeFrameFields[iMAX_BUF_SIZE]; /* field list -> timeframe test */
	int		iNoCombineRules; /* must we combine data from many fields...? */
	char		pcCombineFields[iMIN][iMIN_BUF_SIZE]; /* only neccessary fields */
	int		piNoOfRuleFields[iMIN]; /* how many fields for each rule? */
	int		piPositions[iMIN][2*iMIN][2]; /* start and length position */
	char		pcCombineResultFields[iMIN][iMIN_BUF_SIZE]; /* result field... */
	char		*pcCombineResultData[iMIN]; /* result data */
        char            pcTableNameForFlag[8];
        char            pcFieldNameForFlag[8];
        char            pcFieldContentsForFlag[32];
        char            pcWaitForAckFlag[8];
} CDITable;

typedef struct _CDICmd
{
	char		pcFileCreateCmd[iMIN]; /* command is send after creating a file */
	int			iFileCreateModID; /* the to send the command to */
	char		pcSendFileCmd[iMIN]; /* the command (see below) */
	int			iSendFileToQue; /* only a mod-id */	
	char		pcHomeAirport[iMIN]; /* the HomeAirport */
	char		pcTableExtension[iMIN]; /* the table externsion */
	int			iSendEODAfterUpdate; /* send EOD (end of data) after update */
	FILE		*pFh; /* the file pointer */
	int			iUseFtp; /* should i use ftp */
	int			(*pTelegramHandler)(struct _CDICmd *);
	char		pcLocalFilePath[iMIN_BUF_SIZE]; /* internal use only */
	char		pcFileName[iMIN_BUF_SIZE]; /* local ftp-filename... */
	char		pcRemoteFilePath[iMIN_BUF_SIZE]; /* path at remote machine */
	char		pcRFileName[iMIN_BUF_SIZE]; /* remote ftp-filename... */
	char		*pcCmd; /* current command */
	int			iNextTableNo; /* the next table number */
	int			iNoReceivedTables; /* internal counter */
	CDISave	rCDISave[iMIN_BUF_SIZE]; /* max 128 tables */	
	int			iNoTable; /* how many db-tables */
	CDITable	*prTabDef; /* everything for table-handling */
} CDICmd;

/* initialization structures */
typedef struct _CDIMain
{
	/* global entries for all following sections */
	char		pcSeparator[2]; /* global data separator, used for all telegrams */
	int		iTimeUnit;		/* seconds or microseconds */
	int		iCheckConnection; /* send a CCOC every n seconds */
	int		iCheckResponse; /* wait x seconds for response */
        int             iMake_Connect; /* make connection TRUE or FALSE  */
        int             iAddLFatEnd; /* every line ends with a newline*/
        char            pcRemote_Host[100]; /* connect server to remote host */
	char		pcRecoverFile[iMIN_BUF_SIZE]; /* recover filename and path */
	int		iRecoverCounter; /* how many records should we save in rec area */
	char		pcCCOKey[iMIN]; /* key for unique number */
	char		pcDATKey[iMIN]; /* key for unique number */
	char		pcCTLKey[iMIN]; /* key for unique number */
	char		pcDELKey[iMIN]; /* key for unique number */
	char		pcServerName[iMIN]; /* ServerName */
	char		pcVersion[4]; /* current version */
	int		iVersion; /* current version */
	int		iSecureOption; /* decode the tcp-ip messages */
	int		iWriteFINATelegram; /* Write the FINA telgram to client 0=no (default) 1=yes */
	char  cFINADataType[iMIN];	 /* Expected Datatype for the telegram */
	int		iKey_adjustment; /* right or left justified key */
	int		iKey_default; /* zero or space */

	/* for file transfer protocol... */
	char		pcShell[iMIN]; /* the shell we use... */

	/* if we got connection, send command to following mod-id? */ 
	char		pcSendOpenConnectCmd[iMIN]; /* the open command we send */
	char		pcSendCloseConnectCmd[iMIN]; /* the close command we send */
	int		iSendConnectModID; /* the mod-id */

	/* following ftp-specific entries */
	char		pcFTPUser[iMIN_BUF_SIZE]; /* user for ftp-Filetransfer */
	char		pcFTPPass[iMIN_BUF_SIZE]; /* password for ftp-user */
	char		pcFTPCtrlFile[iMIN_BUF_SIZE]; /* ctrl-file for ftp-filetransfer */
	int 		iUseFtphdl; 							/* should ftphdl be used or not */
	int 		iftphdl_modid; 						/* maybe there's another ftphdl than 8750 what i can use */
	char 		pcMachine[iMIN_BUF_SIZE]; /* remote machine */
	int 		iReverseIp; 							/* invert ip or not */
	char 		pcMachineType[iMIN_BUF_SIZE]; /* remote machine type*/

	/* next all protocol entries, TCP/IP-Level */
	int		iUseRProt;	/* sould i use send protocol */
	int		iRProtFile; 
	char		pcRProtFile[iMIN_BUF_SIZE]; /* name + path */
	int		iUseSProt;
	int		iSProtFile; /* file-pointer */
	char		pcSProtFile[iMIN_BUF_SIZE];

	/* decoded data logging */
	int		iUseRCProt;	/* sould i use send protocol */
	int		iRCProtFile; 
	char		pcRCProtFile[iMIN_BUF_SIZE]; /* name + path */
	int		iUseSCProt;
	int		iSCProtFile; /* file-pointer */
	char		pcSCProtFile[iMIN_BUF_SIZE];

	/* TCP/IP-specific entries */
	long		lTCPTimeout; /* it depends on TimeUnit, see above */
	char		pcServiceName[iMIN_BUF_SIZE];
	int		iTCPStartSocket;
	int		iTCPWorkSocket;

	/* Server-System, Receiver and Sender values */
	char		pcServerSystem[4];
	char		pcSender[11];
	char		pcReceiver[11];

	/* for nap */
	long		lNapTime;	

	/* Telegram-Class-Handler */
	int		iTCNumber;
	int	   (*pTCHandler[6])(void);

	/* CTL-Class Handler */
	int		(*pCTLHandler[6])(int);

	/* Data Handler for writing data to client */
	int		(*pDataHandler[6][2])(char *, char *, CDITable *);

	/* following all stucturs (Command, Tables, ...) */
	int		iNoInternalCmd; /* how many command do we handle */
	CDICmd	*prInternalCmd; /* all commands and tables and ... */

	int		iNoInventoryCmd; /* how many command do we handle */
	CDICmd	*prInventoryCmd; /* all commands and tables and ... */

	int		iNoUpdateCmd; /* how many command do we handle */
	CDICmd	*prUpdateCmd; /* all commands and tables and ... */

	int		iNoFreeQueryCmd; /* how many command do we handle */
	CDICmd	*prFreeQueryCmd; /* all commands and tables and ... */

	int		iNoIncomingDataCmd; /* how many command do we handle */
	CDICmd	*prIncomingDataCmd; /* all commands and tables and ... */
} CDIMain;

/* -------------------------------------------------------------------------- */
/* -------------------------------------------------------------------------- */
/* -------------------------------------------------------------------------- */
/* communication structures */
typedef struct 
{
	char 	pcServerSystem[3];
	char	pcSender[10];
	char	pcReceiver[10];
	char	pcTimeStamp[14];
	char	pcTeleNo[5];
	char	pcTeleClass[3];
	char	pcLength[4];
} CDI1Header;

typedef struct 
{
	char 	pcServerSystem[3];
	char	pcSender[10];
	char	pcReceiver[10];
	char	pcVersion[3];
	char	pcTimeStamp[14];
	char	pcTeleNo[5];
	char	pcTeleClass[3];
	char	pcLength[4];
} CDI2Header;

/* this is for DBL-Command... (database locked) */
typedef struct
{
	char			pcCommand[3];
} CDIDBLocked;

/* next structure is for most internal commands (CCO,END,EOD,...) */
typedef struct
{
	char			pcCommand[3];
	char			pcData[2]; /* data depends on command */
} CDICheck, CDIError, CDIEnd, CDIEod, CDIInternal;

typedef struct 
{
	char			pcTimeStamp[14];
	char			pcSource[3];
	char			pcDataType[2];
	char			pcKey[25];
	char			pcSeparator[1];
	char			pcData[1];
} CDIDataTelegram;

typedef struct
{
	char			pcCommand[3];
	char			pcTimeStamp[14];
	char			pcSource[3];
	char			pcDataType[2];
	char			pcKey[25];
	char			pcSeparator[1];
	char			pcData[1];
} CDIIncomingData, CDIDelTelegram;

typedef struct
{
	char			pcCommand[3];
	char			pcFromTime[14];
	char			pcToTime[14];
} CDIInventory;

typedef struct
{
	char			pcCommand[3];
	char			pcSubCommand[20];
	char			pcBegin[14];
	char			pcEnd[14];
} CDIUpdate;

typedef struct
{
	char	pcCommand[3];
	char	pcSeparator[1];
	char	pcData[1];
} CDIFreeQuery;

/* -------------------------------------------------------------------------- */
/* -------------------------------------------------------------------------- */
/* -------------------------------------------------------------------------- */
/* some internal structures */
#define iUNIQUE_STEP_SIZE	1000
typedef struct
{
	long	lCurrentNo;
	long	lLastNo;
	long	lAvailableNo;
	long	lMax;
	long	lMin;
} CDIUnique;

/* length of structure */
#define iCDI2_HEADER_LEN		sizeof(CDI2Header)
#define iCDI2_CCO_LEN			sizeof(CDICheck) + iCDI2_HEADER_LEN - 1
#define iCDI2_ERR_LEN			sizeof(CDIError) + iCDI2_HEADER_LEN
#define iCDI2_END_LEN			sizeof(CDIEnd) + iCDI2_HEADER_LEN
#define iCDI2_EOD_LEN			sizeof(CDIEod) + iCDI2_HEADER_LEN - 2
#define iCDI2_DBL_LEN			sizeof(CDIDBLocked) + iCDI2_HEADER_LEN
#define iCDI2_DATA_LEN			sizeof(CDIDataTelegam) + iCDI2_HEADER_LEN
#define iCDI2_DEL_LEN			sizeof(CDIDelTelegam) + iCDI2_HEADER_LEN
#define iCDI2_INVENTORY_LEN	sizeof(CDIInventory) + iCDI2_HEADER_LEN
#define iCDI2_UPDATE_LEN		sizeof(CDIUpdate) + iCDI2_HEADER_LEN
#define iCDI2_FREE_QUERY_LEN	sizeof(CDIFreeQuery) + iCDI2_HEADER_LEN
#define iCDI2_INCOMING_DATA_LEN	sizeof(CDIIncomingData) + iCDI2_HEADER_LEN

#define iCDI1_HEADER_LEN		sizeof(CDI1Header)
#define iCDI1_CCO_LEN			sizeof(CDICheck) + iCDI1_HEADER_LEN - 1
#define iCDI1_ERR_LEN			sizeof(CDIError) + iCDI1_HEADER_LEN
#define iCDI1_END_LEN			sizeof(CDIEnd) + iCDI1_HEADER_LEN
#define iCDI1_EOD_LEN			sizeof(CDIEod) + iCDI1_HEADER_LEN - 2
#define iCDI1_DBL_LEN			sizeof(CDIDBLocked) + iCDI1_HEADER_LEN
#define iCDI1_DATA_LEN			sizeof(CDIDataTelegam) + iCDI1_HEADER_LEN
#define iCDI1_DEL_LEN			sizeof(CDIDelTelegam) + iCDI1_HEADER_LEN
#define iCDI1_INVENTORY_LEN	sizeof(CDIInventory) + iCDI1_HEADER_LEN
#define iCDI1_UPDATE_LEN		sizeof(CDIUpdate) + iCDI1_HEADER_LEN
#define iCDI1_FREE_QUERY_LEN	sizeof(CDIFreeQuery) + iCDI1_HEADER_LEN
#define iCDI1_INCOMING_DATA_LEN	sizeof(CDIIncomingData) + iCDI1_HEADER_LEN

#endif /* __CDI2_H */


