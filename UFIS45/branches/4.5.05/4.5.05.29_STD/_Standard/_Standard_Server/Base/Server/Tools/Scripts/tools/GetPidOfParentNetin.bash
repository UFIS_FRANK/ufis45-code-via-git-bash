SYSMONPID=`ps -e | grep " sysmon" | awk '{print $1}'`
if [ -z "${SYSMONPID}" ] ; then
	echo "Fatal: no SYSMON pid found, aborting ....."
else
	NETINPID=`ps -ef | grep " netin "  | grep " ${SYSMONPID} " | awk '{print $2}'`
	if [ -z "${NETINPID}" ] ; then
		echo "Fatal: no NETIN pid found, aborting ....."
	else
		echo
		echo "PID of parent NETIN is ${NETINPID}"
		echo "In case FIPS login fails, use 'kill -9 ${NETINPID}' to kill parent NETIN..."
		echo "DO NOT USE 'kill -TERM ....', this may disconnect all clients!"
		echo
	fi
fi


