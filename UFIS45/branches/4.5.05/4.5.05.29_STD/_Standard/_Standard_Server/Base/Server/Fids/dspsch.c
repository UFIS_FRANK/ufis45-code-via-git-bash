#ifndef _DEF_mks_version
  #define _DEF_mks_version
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version[] = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Fids/dspsch.c 1.2 2008/07/28 15:35:59SGT akl Exp  $";
#endif /* _DEF_mks_version */

/*                                                                            */
/* UFIS AS DSPSCH.C                                                           */
/*                                                                            */
/* Author         : Andreas Kloidt                                            */
/* Date           : July 2008                                                 */
/* Description    : Process to change display layouts                         */
/*                                                                            */
/* Update history :                                                           */
/*                                                                            */
/*                                                                            */
/******************************************************************************/
/*                                                                            */
/* source-code-control-system version string                                  */
static char sccs_eqihdl[]="%Z% UFIS 4.4 (c) ABB AAT/I %M% %I% / %E% %U% / AKL";
/* be carefule with strftime or similar functions !!!                         */
/*                                                                            */
/******************************************************************************/
/* This program is a MIKE main program */
#define U_MAIN
#define UGCCS_PRG
#define STH_USE

#include <stdio.h>
#include <malloc.h>
#include <errno.h>
#include <signal.h>
#include <netdb.h>
#include <time.h>
#include "debugrec.h"
#include "hsbsub.h"
#include "db_if.h" 
#include "tools.h"
#include "helpful.h"
#include "timdef.h"
 

#define XS_BUFF  128
#define S_BUFF   512
#define M_BUFF   1024
#define L_BUFF   2048
#define XL_BUFF  4096
#define XXL_BUFF 8192 



/******************************************************************************/
/* External variables                                                         */
/******************************************************************************/
FILE *outp       = NULL;
int  debug_level = TRACE;
/******************************************************************************/
/* External functions                                                         */
/******************************************************************************/
extern int SetSignals(void (*HandleSignal)(int));
extern int DebugPrintItem(int,ITEM *);
extern int DebugPrintEvent(int,EVENT *);
extern int init_db(void);
extern int  ResetDBCounter(void);
extern void HandleRemoteDB(EVENT*);
extern int  sql_if(short ,short* ,char* ,char* ); 
extern int close_my_cursor(short *cursor); 
extern void snap(char*,int,FILE*);
extern int GetDataItem(char *pcpResult, char *pcpInput, int ipNum, char cpDel,char *pcpDef,char *pcpTrim);
extern int BuildItemBuffer(char *pcpData, char *pcpFieldList, int ipNoOfFields, char *pcpSepChr);
extern int get_item_no(char *s, char *f, short elem_len);
extern void  GetServerTimeStamp(char*,int,long,char*);
extern int AddSecondsToCEDATime(char *,long,int);
extern int get_real_item(char *, char *, int);
extern int GetNoOfElements(char *s, char c);
extern long nap(long);
extern int SendCedaEvent(int,int,char*,char*,char*,char*,char*,char*,char*,char*,char*,char*,int,int);
extern int GetFullDay(char *, int *, int *, int *);
/******************************************************************************/
/* Global variables                                                           */
/******************************************************************************/
static ITEM  *prgItem      = NULL;        /* The queue item pointer  */
static EVENT *prgEvent     = NULL;        /* The event pointer       */
static int   igItemLen     = 0;           /* length of incoming item */
static int   igInitOK      = FALSE;      /* Flag for init */

/*   static CFG   *prgCfg; */                     /* structure of the cfg-file */
static char  pcgHomeAp[XS_BUFF];      /* buffer for home airport */
static char  pcgTabEnd[XS_BUFF];      /* buffer for TABEND */
static char  pcgTwStart[XS_BUFF] = "";
static char  pcgTwEnd[XS_BUFF];
static char  pcgConfFile[S_BUFF];      /* buffer for config-file name */
static int   igUseHopo = FALSE;          /* flag for use of HOPO-field */
static int   igModID_Router  = 1200;      /* MOD-ID of Router  */

static char  pcgHostName[XS_BUFF];
static char  pcgHostIp[XS_BUFF];

static int   igQueCounter=0;
static int   que_out=0;

/*entry's from configfile*/
static char pcgConfigFile[512];
static char pcgCfgBuffer[512];
static EVENT *prgOutEvent = NULL;
static char pcgCurrentTime[32];
static int igSendResponse;
static int igDiffUtcToLocal;

static int    Init_dspsch();
static int    Reset(void);                        /* Reset program          */
static void   Terminate(int);                     /* Terminate program      */
static void   HandleSignal(int);                  /* Handles signals        */
static void   HandleErr(int);                     /* Handles general errors */
static void   HandleQueErr(int);                  /* Handles queuing errors */
static int    HandleInternalData(void);           /* Handles event data     */
static void   HandleQueues(void);                 /* Waiting for Sts.-switch*/
/******************************************************************************/
/* Function prototypes by AKL                                                 */
/******************************************************************************/
/* Init-functions  */

static int GetQueues();
static int TimeToStr(char *pcpTime,time_t lpTime);
static int GetConfig();
static int CheckSchedule();
/******************************************************************************/
/*                                                                            */
/* The MAIN program                                                           */
/*                                                                            */
/******************************************************************************/
MAIN
{
  int    ilRc = RC_SUCCESS;            /* Return code            */
  int    ilCnt = 0;
  int   ilItemFlag=TRUE; 
  time_t now = 0;
  INITIALIZE;            /* General initialization    */

  /* signal handling of SIGPIPE,SIGCHLD,SIGALRM,SIGTERM */
  SetSignals(HandleSignal);

  dbg(TRACE,"------------------------------------------");
  dbg(TRACE,"MAIN: version <%s>",sccs_eqihdl);

  /* Attach to the MIKE queues */
  do{
    ilRc = init_que();
    if(ilRc != RC_SUCCESS)
      {
	dbg(TRACE,"MAIN: init_que() failed! waiting 6 sec ...");
	sleep(6);
	ilCnt++;
      }/* end of if */
  }while((ilCnt < 10) && (ilRc != RC_SUCCESS));
  if(ilRc != RC_SUCCESS)
    {
      dbg(TRACE,"MAIN: init_que() failed! waiting 60 sec ...");
      sleep(60);
      exit(1);
    }else{
      dbg(TRACE,"MAIN: init_que() OK!");
      dbg(TRACE,"MAIN: mod_id   <%d>",mod_id);
      dbg(TRACE,"MAIN: mod_name <%s>",mod_name);
    }/* end of if */
  do
    {
      ilRc = init_db();
      if (ilRc != RC_SUCCESS)
	{
	  dbg(TRACE,"MAIN: init_db() failed! waiting 6 sec ...");
	  sleep(6);
	  ilCnt++;
	} /* end of if */
    } while((ilCnt < 10) && (ilRc != RC_SUCCESS));
  if(ilRc != RC_SUCCESS)
    {
      dbg(TRACE,"MAIN: init_db() failed! waiting 60 sec ...");
      sleep(60);
      exit(2);
    }else{
      dbg(TRACE,"MAIN: init_db()  OK!");
    } /* end of if */

  /* logon to DB is ok, but do NOT use DB while ctrl_sta == HSB_COMING_UP !!! */
  *pcgConfFile = 0x00;
  sprintf(pcgConfFile,"%s/%s",getenv("BIN_PATH"),mod_name);
  ilRc = TransferFile(pcgConfFile);
  if(ilRc != RC_SUCCESS)
    {
      dbg(TRACE,"MAIN: TransferFile(%s) failed!",pcgConfFile);
    } /* end of if */
  dbg(TRACE,"MAIN: Binary-file = <%s>",pcgConfFile);
  ilRc = SendRemoteShutdown(mod_id);
  if(ilRc != RC_SUCCESS)
    {
      dbg(TRACE,"MAIN: SendRemoteShutdown(%d) failed!",mod_id);
    }
  if((ctrl_sta != HSB_STANDALONE) && (ctrl_sta != HSB_ACTIVE) && (ctrl_sta != HSB_ACT_TO_SBY))
    {
      dbg(TRACE,"MAIN: waiting for status switch ...");
      HandleQueues();
      dbg(TRACE,"MAIN: now running ...");
    }/* end of if */
  if((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE) || (ctrl_sta == HSB_ACT_TO_SBY))
    {
      dbg(TRACE,"MAIN: initializing ...");
      dbg(TRACE,"------------------------------------------");
      if(igInitOK == FALSE)
	{
	  ilRc = Init_dspsch();
	  if(ilRc == RC_SUCCESS)
	    {
	      dbg(TRACE,"");
	      dbg(TRACE,"------------------------------------------");
	      dbg(TRACE,"MAIN: initializing OK");
	      igInitOK = TRUE;
	    } 
	}
    }else{
      Terminate(1);
    }
  dbg(TRACE,"------------------------------------------");
    
  if (igInitOK == TRUE)
    {
      now = time(NULL);
      while(TRUE)
	{
	  memset(prgItem,0x00,igItemLen);
	  ilRc = que(QUE_GETBIG,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);
	  dbg(DEBUG,"QUE Counter %d",++igQueCounter);
	  /* depending on the size of the received item  */
	  /* a realloc could be made by the que function */
	  /* so do never forget to set event pointer !!! */
	  prgEvent = (EVENT *) prgItem->text;
	  if( ilRc == RC_SUCCESS )
	    {
	      /* Acknowledge the item */
	      ilRc = que(QUE_ACK,0,mod_id,0,0,NULL);
	      if( ilRc != RC_SUCCESS ) 
		{
		  /* handle que_ack error */
		  HandleQueErr(ilRc);
		} /* fi */
	      switch( prgEvent->command )
		{
		case    HSB_STANDBY    :
		  ctrl_sta = prgEvent->command;
		  HandleQueues();
		  break;    
		case    HSB_COMING_UP    :
		  ctrl_sta = prgEvent->command;
		  HandleQueues();
		  break;    
		case    HSB_ACTIVE    :
		  ctrl_sta = prgEvent->command;
		  break;    
		case    HSB_ACT_TO_SBY    :
		  ctrl_sta = prgEvent->command;
		  /* CloseConnection(); */
		  HandleQueues();
		  break;    
		case    HSB_DOWN    :
		  /* whole system shutdown - do not further use que(), send_message() or timsch() ! */
		  ctrl_sta = prgEvent->command;
		  Terminate(1);
		  break;    
		case    HSB_STANDALONE    :
		  ctrl_sta = prgEvent->command;
		  ResetDBCounter();
		  break;    
		case    REMOTE_DB :
		  /* ctrl_sta is checked inside */
		  HandleRemoteDB(prgEvent);
		  break;
		case    SHUTDOWN    :
		  /* process shutdown - maybe from uutil */
		  Terminate(1);
		  break;
		case    RESET        :
		  ilRc = Reset();
		  break;
		case    EVENT_DATA    :
		  if((ctrl_sta == HSB_STANDALONE) ||
		     (ctrl_sta == HSB_ACTIVE) ||
		     (ctrl_sta == HSB_ACT_TO_SBY))
		    {
		      ilItemFlag=TRUE;
		      ilRc = HandleInternalData();
		      if(ilRc != RC_SUCCESS)
			{
			  HandleErr(ilRc);
			}/* end of if */
		    }
		  else
		    {
		      dbg(TRACE,"MAIN: wrong HSB-status <%d>",ctrl_sta);
		      DebugPrintItem(TRACE,prgItem);
		      DebugPrintEvent(TRACE,prgEvent);
		    }/* end of if */
		  break; 
		case    TRACE_ON :
		  dbg_handle_debug(prgEvent->command);
		  break;
		case    TRACE_OFF :
		  dbg_handle_debug(prgEvent->command);
		  break;
                case  111 :
                  break;
		default            :
		  dbg(TRACE,"MAIN: unknown event");
		  DebugPrintItem(TRACE,prgItem);
		  DebugPrintEvent(TRACE,prgEvent);
		  break;
		} /* end switch */
	    }else{
	      /* Handle queuing errors */
	      HandleQueErr(ilRc);
	    } /* end else */



	  /**************************************************************/
	  /* time parameter for cyclic actions                          */
	  /**************************************************************/
    
	  now = time(NULL);

	} /* end while */
    }else{
      dbg(TRACE,"MAIN: Init_dspsch() failed with <%d> Sleeping 30 sec.! Then terminating ...",ilRc);
      sleep(30);
    }
  exit(0);
  return 0;
} /* end of MAIN */

/******************************************************************************/
/* The initialization routine                                                 */
/******************************************************************************/
static int Init_dspsch()
{
  int    ilRc = RC_SUCCESS;            /* Return code */

  GetQueues();
  /* reading default home-airport from sgs.tab */
  memset(pcgHomeAp,0x00,sizeof(pcgHomeAp));
  ilRc = tool_search_exco_data("SYS","HOMEAP",pcgHomeAp);
  if (ilRc != RC_SUCCESS)
    {
      dbg(TRACE,"Init_dspsch : No HOMEAP entry in sgs.tab: EXTAB! Please add!");
      return RC_FAIL;
    }
  else
    {
      dbg(TRACE,"Init_dspsch : HOMEAP = <%s>",pcgHomeAp);
    }
  /* reading default table-extension from sgs.tab */
  memset(pcgTabEnd,0x00,sizeof(pcgTabEnd));
  ilRc = tool_search_exco_data("ALL","TABEND",pcgTabEnd);
  if (ilRc != RC_SUCCESS)
    {
      dbg(TRACE,"Init_dspsch : No TABEND entry in sgs.tab: EXTAB! Please add!");
      return RC_FAIL;
    }
  else
    {
      dbg(TRACE,"Init_dspsch : TABEND = <%s>",pcgTabEnd);
      memset(pcgTwEnd,0x00,XS_BUFF);
      sprintf(pcgTwEnd,"%s,%s,%s",pcgHomeAp,pcgTabEnd,mod_name);
      dbg(TRACE,"Init_dspsch : TW_END = <%s>",pcgTwEnd);
      
      if (strcmp(pcgTabEnd,"TAB") == 0)
	{
	  igUseHopo = TRUE;
	  dbg(TRACE,"Init_dspsch: use HOPO-field!");
	}
    }

  ilRc = GetConfig();

  ilRc = TimeToStr(pcgCurrentTime,time(NULL));
  
  return(ilRc);
} /* end of initialize */
/*********************************************************************
Function : GetQueues()
Paramter :
Return Code: RC_SUCCESS,RC_FAIL
Result:
Description: Gets all necessary queue-ID's for CEDA-internal
             communication!
*********************************************************************/
static int GetQueues()
{
  int ilRc = RC_FAIL;

  /* get mod-id of router */
  if ((igModID_Router = tool_get_q_id("router")) == RC_NOT_FOUND ||
      igModID_Router == RC_FAIL || igModID_Router == 0)
    {
      dbg(TRACE,"GetQueues   : tool_get_q_id(router) returns: <%d>",igModID_Router);
      ilRc = RC_FAIL;
    }else{
      dbg(TRACE,"GetQueues   : <router> mod_id <%d>",igModID_Router);
      ilRc = RC_SUCCESS;
    } 
  return ilRc;
}

/******************************************************************************/
/* The Reset routine                                                          */
/******************************************************************************/
static int Reset()
{

  int    ilRc = RC_SUCCESS;    /* Return code */
    
  dbg(TRACE,"Reset: now reseting ...");

  return ilRc;
    
} /* end of Reset */
/******************************************************************************/
/* The termination routine                                                    */
/******************************************************************************/
static void Terminate(int ipSleep)
{
  dbg(TRACE,"Terminate: now leaving ...");
 
  sleep(ipSleep);
  
  exit(0);
    
} /* end of Terminate */
/******************************************************************************/
/* The handle signals routine                                                 */
/******************************************************************************/
static void HandleSignal(int pipSig)
{
  switch(pipSig)
    {
    case SIGALRM:
      break;
    case SIGPIPE:
      break;
    case SIGCHLD:
      break;
    case SIGTERM:
      Terminate(1);
      break;
    default    :
      Terminate(10);
      break;
    } /* end of switch */
} /* end of HandleSignal */
/******************************************************************************/
/* The handle general error routine                                           */
/******************************************************************************/
static void HandleErr(int pipErr)
{
  /*    int    ilRc = RC_SUCCESS; */
    return;
} /* end of HandleErr */
/******************************************************************************/
/* The handle queuing error routine                                           */
/******************************************************************************/
static void HandleQueErr(int pipErr)
{
    int    ilRc = RC_SUCCESS;
    
    switch(pipErr) {
    case    QUE_E_FUNC    :    /* Unknown function */
        dbg(TRACE,"<%d> : unknown function",pipErr);
        break;
    case    QUE_E_MEMORY    :    /* Malloc reports no memory */
        dbg(TRACE,"<%d> : malloc failed",pipErr);
        break;
    case    QUE_E_SEND    :    /* Error using msgsnd */
            dbg(TRACE,"<%d> : msgsnd failed",pipErr);
            break;
    case    QUE_E_GET    :    /* Error using msgrcv */
            if(pipErr != 4)        
             dbg(DEBUG,"<%d> : msgrcv failed",pipErr);
        break;
    case    QUE_E_EXISTS    :
        dbg(TRACE,"<%d> : route/queue already exists ",pipErr);
        break;
    case    QUE_E_NOFIND    :
        dbg(TRACE,"<%d> : route not found ",pipErr);
        break;
    case    QUE_E_ACKUNEX    : 
        dbg(TRACE,"<%d> : unexpected ack received ",pipErr);
        break;
    case    QUE_E_STATUS    :
        dbg(TRACE,"<%d> :   unknown queue status ",pipErr);
        break;
    case    QUE_E_INACTIVE    :
        dbg(TRACE,"<%d> : queue is inaktive ",pipErr);
        break;
    case    QUE_E_MISACK    :
        dbg(TRACE,"<%d> : missing ack ",pipErr);
        break;
    case    QUE_E_NOQUEUES    :
        dbg(TRACE,"<%d> : queue does not exist",pipErr);
        break;
    case    QUE_E_RESP    :    /* No response on CREATE */
        dbg(TRACE,"<%d> : no response on create",pipErr);
        break;
    case    QUE_E_FULL    :
        dbg(TRACE,"<%d> : too many route destinations",pipErr);
        break;
    case    QUE_E_NOMSG    :    /* No message on queue */
        /*dbg(TRACE,"<%d> : no messages on queue",pipErr);*/
        break;
    case    QUE_E_INVORG    :    /* Mod id by que call is 0 */
        dbg(TRACE,"<%d> : invalid originator=0",pipErr);
        break;
    case    QUE_E_NOINIT    :    /* Queues is not initialized*/
        dbg(TRACE,"<%d> : queues are not initialized",pipErr);
        break;
    case    QUE_E_ITOBIG    :
        dbg(TRACE,"<%d> : requestet itemsize to big ",pipErr);
        break;
    case    QUE_E_BUFSIZ    :
        dbg(TRACE,"<%d> : receive buffer to small ",pipErr);
        break;
    case    QUE_E_PRIORITY    :
        dbg(TRACE,"<%d> : wrong priority was send ",pipErr);
        break;
    default            :    /* Unknown queue error */
        dbg(TRACE,"<%d> : unknown error",pipErr);
        break;
    } /* end switch */
         
    return;
} /* end of HandleQueErr */
/******************************************************************************/
/* The handle queues routine                                                  */
/******************************************************************************/
static void HandleQueues()
{
  int    ilRc = RC_SUCCESS;            /* Return code */
  int    ilBreakOut = FALSE;
    
  do{
    memset(prgItem,0x00,igItemLen);
    ilRc = que(QUE_GETBIG,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);
    /* depending on the size of the received item  */
    /* a realloc could be made by the que function */
    /* so do never forget to set event pointer !!! */
    prgEvent = (EVENT *) prgItem->text;    
    if( ilRc == RC_SUCCESS )
      {
    /* Acknowledge the item */
    ilRc = que(QUE_ACK,0,mod_id,0,0,NULL);
    if( ilRc != RC_SUCCESS ) 
      {
        /* handle que_ack error */
        HandleQueErr(ilRc);
      } /* fi */
        
    switch( prgEvent->command )
      {
      case    HSB_STANDBY    :
        ctrl_sta = prgEvent->command;
        break;    
      case    HSB_COMING_UP    :
        ctrl_sta = prgEvent->command;
        break;    
      case    HSB_ACTIVE    :
        ctrl_sta = prgEvent->command;
        ilBreakOut = TRUE;
        break;    
      case    HSB_ACT_TO_SBY    :
        ctrl_sta = prgEvent->command;
        break;    
      case    HSB_DOWN    :
        /* whole system shutdown - do not further use que(), send_message() or timsch() ! */
        ctrl_sta = prgEvent->command;
        Terminate(10);
        break;    
      case    HSB_STANDALONE    :
        ctrl_sta = prgEvent->command;
        ResetDBCounter();
        ilBreakOut = TRUE;
        break;    
      case    REMOTE_DB :
        /* ctrl_sta is checked inside */
        HandleRemoteDB(prgEvent);
        break;
      case    SHUTDOWN    :
        Terminate(1);
        break;
      case    RESET        :
        ilRc = Reset();
        break;
      case    EVENT_DATA    :
        dbg(TRACE,"HandleQueues: wrong hsb status <%d>",ctrl_sta);
        DebugPrintItem(TRACE,prgItem);
        DebugPrintEvent(TRACE,prgEvent);
        break;
      case    TRACE_ON :
        dbg_handle_debug(prgEvent->command);
        break;
      case    TRACE_OFF :
        dbg_handle_debug(prgEvent->command);
        break;
      default            :
        dbg(TRACE,"HandleQueues: unknown event");
        DebugPrintItem(TRACE,prgItem);

        DebugPrintEvent(TRACE,prgEvent);

        break;
      } /* end switch */
      }else{
    /* Handle queuing errors */
    HandleQueErr(ilRc);
      } /* end else */
  } while (ilBreakOut == FALSE);
  if(igInitOK == FALSE)
    {
      ilRc = Init_dspsch();
      if(ilRc == RC_SUCCESS)
    {
      dbg(TRACE,"HandleQueues: Init_dspsch() OK!");
      igInitOK = TRUE;
    }else{ /* end of if */
      dbg(TRACE,"HandleQueues: Init_dspsch() failed!");
      igInitOK = FALSE;
    } /* end of if */
    }/* end of if */
  /* OpenConnection(); */
} /* end of HandleQueues */
/******************************************************************************/
/* The handle data routine                                                    */
/******************************************************************************/
static int HandleInternalData()
{
  int  ilRC = RC_SUCCESS;      /* Return code */
  char *pclSelection = NULL;
  char *pclFields = NULL;
  char *pclData = NULL;
  BC_HEAD *bchd = NULL;          /* Broadcast header*/
  CMDBLK  *cmdblk = NULL; 

  que_out = prgEvent->originator;
  bchd  = (BC_HEAD *) ((char *)prgEvent + sizeof(EVENT));
  cmdblk= (CMDBLK  *) ((char *)bchd->data);
  if (bchd->rc == NETOUT_NO_ACK)
     igSendResponse = FALSE;
  else
     igSendResponse = TRUE;

  strcpy(pcgTwStart,cmdblk->tw_start);
  strcpy(pcgTwEnd,cmdblk->tw_end);

  /***********************************/
  /*    DebugPrintItem(DEBUG,prgItem);   */
  /*    DebugPrintEvent(DEBUG,prgEvent); */
  /***********************************/
  pclSelection = cmdblk->data;
  pclFields = (char *)pclSelection + strlen(pclSelection) + 1;
  pclData = (char *)pclFields + strlen(pclFields) + 1;

  ilRC = TimeToStr(pcgCurrentTime,time(NULL));

  if (strcmp(cmdblk->command,"SCHD") == 0)
  {
     dbg(DEBUG,"Command:   <%s>",cmdblk->command);
     dbg(DEBUG,"Selection: <%s>",pclSelection);
     dbg(DEBUG,"Fields:    <%s>",pclFields);
     dbg(DEBUG,"Data:      <%s>",pclData);
     dbg(DEBUG,"TwStart:   <%s>",pcgTwStart);
     dbg(DEBUG,"TwEnd:     <%s>",pcgTwEnd);
     ilRC = CheckSchedule();
  }

  dbg(DEBUG,"========================= START / END =========================");
  return ilRC;
} /* end of HandleInternalData */
/*********************************************************************

/*
	Get Config Entries
*/
static int GetConfig()
{
  int ilRC = RC_SUCCESS;
  char pclFunc[] = "GetConfig:";
  char pclDebugLevel[32];

  sprintf(pcgConfigFile,"%s/%s.cfg",getenv("CFG_PATH"),mod_name);
  dbg(TRACE,"%s Config File is <%s>",pclFunc,pcgConfigFile);

  ilRC = iGetConfigEntry(pcgConfigFile,"DSPSCH","DEBUG_LEVEL",CFG_STRING,pclDebugLevel);
  if (strcmp(pclDebugLevel,"DEBUG") == 0)
  {
     debug_level = DEBUG;
  }
  else
  {
     if (strcmp(pclDebugLevel,"TRACE") == 0)
     {
        debug_level = TRACE;
     }
     else
     {
        if (strcmp(pclDebugLevel,"NULL") == 0)
        {
           debug_level = 0;
        }
        else
        {
           debug_level = TRACE;
        }
     }
  }

  dbg(TRACE,"%s DEBUG_LEVEL = %s",pclFunc,pclDebugLevel);

  return RC_SUCCESS;
} /* Enf of GetConfig */

/******************************************************************************/
/* The TimeToStr routine                                                      */
/******************************************************************************/
static int TimeToStr(char *pcpTime,time_t lpTime) 
{                                                                              
  struct tm *_tm;
    
  _tm = (struct tm *)gmtime(&lpTime);

  sprintf(pcpTime,"%4d%02d%02d%02d%02d%02d",
          _tm->tm_year+1900,_tm->tm_mon+1,_tm->tm_mday,_tm->tm_hour,
          _tm->tm_min,_tm->tm_sec);

  return RC_SUCCESS;  
                        
}     /* end of TimeToStr */


static int CheckSchedule()
{
  int ilRC = RC_SUCCESS;
  char pclFunc[] = "CheckSchedule:";
  int ilRCdbDsc = DB_SUCCESS;
  short slFktDsc;
  short slCursorDsc;
  char pclSelectionDsc[1024];
  char pclSqlBufDsc[1024];
  char pclDataBufDsc[2048];
  int ilRCdbDpt = DB_SUCCESS;
  short slFktDpt;
  short slCursorDpt;
  char pclSelectionDpt[1024];
  char pclSqlBufDpt[1024];
  char pclDataBufDpt[2048];
  char pclFieldListDpt[2048];
  int ilRCdbDev = DB_SUCCESS;
  short slFktDev;
  short slCursorDev;
  char pclSelectionDev[1024];
  char pclSqlBufDev[1024];
  char pclDataBufDev[2048];
  char pclFieldListDev[2048];
  int ilRCdbUpd = DB_SUCCESS;
  short slFktUpd;
  short slCursorUpd;
  char pclSelectionUpd[1024];
  char pclSqlBufUpd[1024];
  char pclDataBufUpd[2048];
  char pclCurDat[16];
  char pclCurTim[16];
  char pclUrnoDsc[16];
  char pclDevnDsc[128];
  char pclDoopDpt[16];
  char pclDbgnDpt[16];
  char pclDendDpt[16];
  char pclDpidDpt[128];
  char pclDffdDpt[16];
  char pclDfcoDpt[256];
  char pclDterDpt[16];
  char pclDareDpt[16];
  char pclAlgcDpt[128];
  char pclUrnoDev[16];
  char pclGrpnDev[16];
  char pclDpidDev[128];
  char pclDffdDev[16];
  char pclDfcoDev[256];
  char pclDterDev[16];
  char pclDareDev[16];
  char pclAlgcDev[128];
  int ilDays;
  int ilMins;
  int ilWeekDay;
  char pclWeekDay[8];
  int ilFound;
  char pclTmpBuf[1024];
  char pclUpdList[2048];
  char pclGrpnList[1024];
  int ilI;
  int ilNoEle;
  char pclGrpn[64];
  char pclCommand[16];
  char pclProcId[16];

  strcpy(pclGrpnList,"");
  strcpy(pclCurDat,pcgCurrentTime);
  pclCurDat[8] = '\0';
  strcpy(pclCurTim,&pcgCurrentTime[8]);
  pclCurTim[4] = '\0';
  sprintf(pclSelectionDsc,"WHERE VAFR <= '%s' AND VATO >= '%s'",pclCurDat,pclCurDat);
  sprintf(pclSqlBufDsc,"SELECT URNO,DEVN FROM DSCTAB %s ORDER BY DEVN",pclSelectionDsc);
  slCursorDsc = 0;
  slFktDsc = START;
  dbg(DEBUG,"%s SQL = <%s>",pclFunc,pclSqlBufDsc);
  ilRCdbDsc = sql_if(slFktDsc,&slCursorDsc,pclSqlBufDsc,pclDataBufDsc);
  while (ilRCdbDsc == DB_SUCCESS)
  {
     BuildItemBuffer(pclDataBufDsc,"",2,",");
     get_real_item(pclUrnoDsc,pclDataBufDsc,1);
     get_real_item(pclDevnDsc,pclDataBufDsc,2);
     ilFound = FALSE;
     strcpy(pclFieldListDpt,"DOOP,DBGN,DEND,DPID,DFFD,DFCO,DTER,DARE,ALGC");
     sprintf(pclSelectionDpt,"WHERE RURN = %s AND DEFL = ' '",pclUrnoDsc);
     sprintf(pclSqlBufDpt,"SELECT %s FROM DPTTAB %s ORDER BY DBGN",pclFieldListDpt,pclSelectionDpt);
     slCursorDpt = 0;
     slFktDpt = START;
     dbg(DEBUG,"%s SQL = <%s>",pclFunc,pclSqlBufDpt);
     ilRCdbDpt = sql_if(slFktDpt,&slCursorDpt,pclSqlBufDpt,pclDataBufDpt);
     while (ilRCdbDpt == DB_SUCCESS && ilFound == FALSE)
     {
        BuildItemBuffer(pclDataBufDpt,"",9,",");
        get_real_item(pclDoopDpt,pclDataBufDpt,1);
        get_real_item(pclDbgnDpt,pclDataBufDpt,2);
        get_real_item(pclDendDpt,pclDataBufDpt,3);
        get_real_item(pclDpidDpt,pclDataBufDpt,4);
        get_real_item(pclDffdDpt,pclDataBufDpt,5);
        get_real_item(pclDfcoDpt,pclDataBufDpt,6);
        get_real_item(pclDterDpt,pclDataBufDpt,7);
        get_real_item(pclDareDpt,pclDataBufDpt,8);
        get_real_item(pclAlgcDpt,pclDataBufDpt,9);
        if (strcmp(pclDbgnDpt,pclCurTim) <= 0 && strcmp(pclDendDpt,pclCurTim) >= 0)
        {
           GetFullDay(pclCurDat,&ilDays,&ilMins,&ilWeekDay);
           sprintf(pclWeekDay,"%d",ilWeekDay);
           if (strstr(pclDoopDpt,pclWeekDay) != NULL)
           {
              ilFound = TRUE;
           }
        }
        slFktDpt = NEXT;
        ilRCdbDpt = sql_if(slFktDpt,&slCursorDpt,pclSqlBufDpt,pclDataBufDpt);
     }
     close_my_cursor(&slCursorDpt);
     if (ilFound == FALSE)
     {
        strcpy(pclFieldListDpt,"DPID,DFFD,DFCO,DTER,DARE,ALGC");
        sprintf(pclSelectionDpt,"WHERE RURN = %s AND DEFL <> ' '",pclUrnoDsc);
        sprintf(pclSqlBufDpt,"SELECT %s FROM DPTTAB %s ORDER BY DBGN",pclFieldListDpt,pclSelectionDpt);
        slCursorDpt = 0;
        slFktDpt = START;
        dbg(DEBUG,"%s SQL = <%s>",pclFunc,pclSqlBufDpt);
        ilRCdbDpt = sql_if(slFktDpt,&slCursorDpt,pclSqlBufDpt,pclDataBufDpt);
        close_my_cursor(&slCursorDpt);
        if (ilRCdbDpt == DB_SUCCESS)
        {
           BuildItemBuffer(pclDataBufDpt,"",6,",");
           get_real_item(pclDpidDpt,pclDataBufDpt,1);
           get_real_item(pclDffdDpt,pclDataBufDpt,2);
           get_real_item(pclDfcoDpt,pclDataBufDpt,3);
           get_real_item(pclDterDpt,pclDataBufDpt,4);
           get_real_item(pclDareDpt,pclDataBufDpt,5);
           get_real_item(pclAlgcDpt,pclDataBufDpt,6);
           ilFound = TRUE;
        }
     }
     if (ilFound == TRUE)
     {
        strcpy(pclFieldListDev,"URNO,GRPN,DPID,DFFD,DFCO,DTER,DARE,ALGC");
        sprintf(pclSelectionDev,"WHERE DEVN = '%s'",pclDevnDsc);
        sprintf(pclSqlBufDev,"SELECT %s FROM DEVTAB %s",pclFieldListDev,pclSelectionDev);
        slCursorDev = 0;
        slFktDev = START;
        dbg(DEBUG,"%s SQL = <%s>",pclFunc,pclSqlBufDev);
        ilRCdbDev = sql_if(slFktDev,&slCursorDev,pclSqlBufDev,pclDataBufDev);
        close_my_cursor(&slCursorDev);
        if (ilRCdbDev == DB_SUCCESS)
        {
           BuildItemBuffer(pclDataBufDev,"",8,",");
           get_real_item(pclUrnoDev,pclDataBufDev,1);
           get_real_item(pclGrpnDev,pclDataBufDev,2);
           get_real_item(pclDpidDev,pclDataBufDev,3);
           get_real_item(pclDffdDev,pclDataBufDev,4);
           get_real_item(pclDfcoDev,pclDataBufDev,5);
           get_real_item(pclDterDev,pclDataBufDev,6);
           get_real_item(pclDareDev,pclDataBufDev,7);
           get_real_item(pclAlgcDev,pclDataBufDev,8);
           ilFound = TRUE;
           strcpy(pclUpdList,"");
           if (strcmp(pclDpidDpt,pclDpidDev) != 0)
           {
              sprintf(pclTmpBuf,"DPID='%s',",pclDpidDpt);
              strcat(pclUpdList,pclTmpBuf);
           }
           if (strcmp(pclDffdDpt,pclDffdDev) != 0)
           {
              sprintf(pclTmpBuf,"DFFD='%s',",pclDffdDpt);
              strcat(pclUpdList,pclTmpBuf);
           }
           if (strcmp(pclDfcoDpt,pclDfcoDev) != 0)
           {
              sprintf(pclTmpBuf,"DFCO='%s',",pclDfcoDpt);
              strcat(pclUpdList,pclTmpBuf);
           }
           if (strcmp(pclDterDpt,pclDterDev) != 0)
           {
              sprintf(pclTmpBuf,"DTER='%s',",pclDterDpt);
              strcat(pclUpdList,pclTmpBuf);
           }
           if (strcmp(pclDareDpt,pclDareDev) != 0)
           {
              sprintf(pclTmpBuf,"DARE='%s',",pclDareDpt);
              strcat(pclUpdList,pclTmpBuf);
           }
           if (strcmp(pclAlgcDpt,pclAlgcDev) != 0)
           {
              sprintf(pclTmpBuf,"ALGC='%s',",pclAlgcDpt);
              strcat(pclUpdList,pclTmpBuf);
           }
           if (strlen(pclUpdList) > 0)
           {
              pclUpdList[strlen(pclUpdList)-1] = '\0';
              dbg(TRACE,"-----------------------------------------------------------------------------");
              dbg(TRACE,"%s Configuration Change for Device: %s/%s",pclFunc,pclDevnDsc,pclUrnoDev);
              dbg(TRACE,"%s <%s>",pclFunc,pclUpdList);
              sprintf(pclSelectionUpd,"WHERE URNO = %s",pclUrnoDev);
              sprintf(pclSqlBufUpd,"UPDATE DEVTAB SET %s %s",pclUpdList,pclSelectionUpd);
              slCursorUpd = 0;
              slFktUpd = START;
              dbg(DEBUG,"%s SQL = <%s>",pclFunc,pclSqlBufUpd);
              ilRCdbUpd = sql_if(slFktUpd,&slCursorUpd,pclSqlBufUpd,pclDataBufUpd);
              if (ilRCdbUpd == DB_SUCCESS)
                 commit_work();
              close_my_cursor(&slCursorUpd);
              dbg(TRACE,"-----------------------------------------------------------------------------");
              if (strstr(pclGrpnList,pclGrpnDev) == NULL)
              {
                 strcat(pclGrpnList,pclGrpnDev);
                 strcat(pclGrpnList,",");
              }
           }
        }
     }
     slFktDsc = NEXT;
     ilRCdbDsc = sql_if(slFktDsc,&slCursorDsc,pclSqlBufDsc,pclDataBufDsc);
  }
  close_my_cursor(&slCursorDsc);
  if (strlen(pclGrpnList) > 0)
  {
     pclGrpnList[strlen(pclGrpnList)-1] = '\0';
     ilNoEle = GetNoOfElements(pclGrpnList,',');
     for (ilI = 1; ilI <= ilNoEle; ilI++)
     {
        get_real_item(pclGrpn,pclGrpnList,ilI);
        ilRC = iGetConfigEntry(pcgConfigFile,"COMMAND",pclGrpn,CFG_STRING,pclTmpBuf);
        if (ilRC == RC_SUCCESS)
        {
           get_real_item(pclProcId,pclTmpBuf,1);
           get_real_item(pclCommand,pclTmpBuf,2);
           dbg(TRACE,"%s Send Command <%s> to Process <%s>",pclFunc,pclCommand,pclProcId);
           ilRC = SendCedaEvent(atoi(pclProcId),0,mod_name,"CEDA",pcgTwStart,pcgTwEnd,
                                pclCommand,"","","","","",3,NETOUT_NO_ACK);
        }
     }
  }

  return ilRC;
} /* End of CheckSchedule */

