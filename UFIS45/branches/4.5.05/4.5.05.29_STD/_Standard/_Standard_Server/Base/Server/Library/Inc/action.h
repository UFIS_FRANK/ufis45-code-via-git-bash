#ifndef _DEF_mks_version_action_h
  #define _DEF_mks_version_action_h
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_action_h[]   MARK_UNUSED = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Library/Inc/action.h 1.9 2006/10/10 21:41:08SGT akl Exp  $";
#endif /* _DEF_mks_version */
/* ********************************************************************** */
/*  The Master include file.						  */
/*									  */
/*  Program	  :     						  */
/*  Revision date :							  */
/*  Author    	  :							  */
/*  									  */
/*  NOTE : This should be the only include file for your program          */
/* ********************************************************************** */
#ifndef _ACTION_H
#define _ACTION_H


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>
#include <errno.h>
#include <signal.h>

/* #include "ugccsma.h" */
/* #include "msgno.h" */
/* #include "glbdef.h" */
/* #include "quedef.h" */
/* #include "uevent.h" */
/* #include "tools.h" */
/* #include "helpful.h" */

/* #include "new_catch.h" */
/* #include "hsbsub.h" */

#define	iINTEGER			0
#define	iSTRING			1
#define	iTIME				2
#define	iDYNAMIC_TIME	3
#define	iAND				0
#define	iOR				1
#define 	iVALID			0
#define	iNOT_VALID		1

#define	sVALID_FILE_NAME		"./action.dat"
#define	iUSE_ALL_RECEIVED_FIELDS	-150

/* define for string handling functions */
#define	iDELETE_ALL_BLANKS			0
#define	iONE_BLANK_REMAINING			1
#define	iKEEP_DATA_UNTOUCHED			2
#define	iORG_EVENT_PRIO			6

/* defines for dynamic section handling */
#define	iADD_SECTION					0
#define	iDELETE_SECTION				1
#define	iSTATIC_SECTION				0
#define	iDYNAMIC_SECTION				1
#define	iCREATE_DYNAMIC_FILE			0
#define	iDONT_CREATE_DYNAMIC_FILE	1

/*defines for CEI usage */
#define CEISRV	10
#define CEICLI	20
#define CEIUNK	-1
#define iMAX_CEI_TABLES 1000

extern char *strdup(const char *);

typedef struct _CEITable
{
	int	iCount;
	char	pcTableName[10];
} CEITable;

typedef struct _ACTIONConfig
{
	int			iADFlag; /* add or delete this section */
	int			iEmptyFieldHandling; /* what happen with empty fields */
	int			iIgnoreEmptyFields;	/* must we ignore empty fields? */
	char		pcSndCmd[iMIN_BUF_SIZE];	/* send command */
	char		pcSectionName[iMIN]; /* name of this section */
	char		pcTableName[iMIN]; /* name of db-table */
	char		pcFields[iMAX_BUF_SIZE];	/* the fields */
	char		pcSectionCommands[iMIN_BUF_SIZE]; /* section specific commands */
	int			iModID;  	/* mod_id we send to */
	int     iSuspendOwn; /*dont echo to me*/
	char    pcSuspendOriginator[iMAX_BUF_SIZE];/*nothing from them*/
} ACTIONConfig;
typedef struct _tsection
{


  int			iSectionType; /* static or dynamic */
	int			iEmptyFieldHandling; /* what happen with empty fields */
	int			iNoOfIgnoreDataFields; /* counter for fields below */
	char		pcIgnoreDataFields[iMAX_BUF_SIZE]; /* to ignore data */
  int     iSendOldData;
	int			iIgnoreEmptyFields;	/* must we ignore empty fields? */
	int			iUseSndCmd; /* should i use send_command? */ 
 	char		pcKeepOrgTwstart[10];	/* shall action write tw_start or not default=no*/
 	char		pcClearTwstart[10];	/* shall action clear tw_start or not default=no*/
	int			iKeepOrgTwstart; /* */ 
	int			iClearTwstart; /* */ 
	char		pcSndCmd[iMIN_BUF_SIZE];	/* send command */
	char		pcSectionName[iMIN_BUF_SIZE]; /* name of this section */
	char		pcTableName[iMIN_BUF_SIZE]; /* name of db-table */
	int			iNoOfKeys; /* number of keys */
	char		**pcKeys; /* the keys */
	char		**pcKeyConditions; /* conditions for keys */
	UINT		*piKeyConditionTypes;  /* type of condition (INT; STRING;) */
	int			iNoOfFields; /* number of fields */
	char		**pcFields;	/* the fields */
  char    *pcFieldResponseType;/*always, only when updated*/  
  char    *pcFieldResponseFlag;/*should it be send*/       
  char		**pcFieldConditions; /* condition for the fields */
	UINT		*piFieldConditionTypes;		/* integer or string */
	UINT		iNoOfSectionCommands; /* number of next */
	char		**pcSectionCommands; /* section specific commands */
	UINT		iModID;	/* mod_id we send to */
	char		pcPrio[10];	/* prio we use */
	UINT		iPrio;	/* prio we use */
	UINT		iCommandType;	/* command type EVENT->command */
	int			iValidQueueId;		/* is this ModID valid? */
	int			iValidSection;		/* is this section valid? */
	int			iCmdNo;	/* command number */
	int			iTabNo;	/* table number */
	char DataFilter[2048];
	char DataMapping[2048];
	int  iSuspendOwn; /*dont echo to me*/
	char pcSuspendOriginator[iMAX_BUF_SIZE];/*nothing from them*/

	char 	cei_pcInternalType[iMIN_BUF_SIZE]; /* is this a usual section or a CEI-type section (forwarding according to table-names only) */
	int 	cei_iInternalType; /* is this a usual section or a CEI-type section (forwarding according to table-names only) */
	char	cei_pcTask[32]; /* CEI-section task; act as server or as client ? */
	char	cei_pcId[iMIN_BUF_SIZE]; /* CEI-Identifier */
	int 	cei_iTask; /* is this a usual section or a CEI-type section (forwarding according to table-names only) */
	char 	cei_pcRcvModId[iMIN_BUF_SIZE]; /* is this a usual section or a CEI-type section (forwarding according to table-names only) */
	int		cei_iRcvModId;	/* accept CEI comands only from that modid */
	char 	cei_pcSndModId[iMIN_BUF_SIZE]; /* is this a usual section or a CEI-type section (forwarding according to table-names only) */
	int		cei_iSndModId;  /* send cei-event to that mod-id */
	char 	cei_pcEventOriginator[iMIN_BUF_SIZE]; 
	int		cei_iEventOriginator;  
	CEITable	cei_rPublTab[iMAX_CEI_TABLES]; /* tablenames and it's number of times being registered */
	char	cei_pcDenyTab[iMAX_BUF_SIZE]; /* simple, static list of tables to exclude from that exchange mechanism */
} TSection;

typedef struct _tmain
{
	UINT			iNoOfGlobalCommands; /* number of global command s */
	char			**pcGlobalCommands; /* the global commands */
	UINT			iNoOfSections; /* number of sections */
	TSection		*prSection; /* the sections */
} TMain;
#endif /* _ACTION_H */
