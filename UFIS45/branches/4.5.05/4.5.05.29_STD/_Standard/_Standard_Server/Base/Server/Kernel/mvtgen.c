#ifndef _DEF_mks_version
  #define _DEF_mks_version
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version[] MARK_UNUSED = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Kernel/mvtgen.c 1.5 2011/01/14 12:16:54SGT bst Exp  $";
#endif /* _DEF_mks_version */
/******************************************************************************/
/*                                                                            */
/* ABB ACE/FC Program Skeleton                                                */
/*                                                                            */
/* Author         :                                                           */
/* Date           :                                                           */
/* Description    :                                                           */
/*                                                                            */
/* Update history :                                                           */
/* 20040810 JIM removed sccs_ version string                                  */
/* 20091204 MEI: to handle special request from SATs to auto send MVT telex   */
/*               base on flight event timing                                  */
/******************************************************************************/
/*                                                                            */


/******************************************************************************/
/* This program is a MIKE main program */
#define U_MAIN
#define UGCCS_PRG
#define STH_USE

#define DATABLK_SIZE     (256*1024)
#define C_MAX_SP_FIELDS        (10)
#define C_MAXFIELDS           (100)
#define C_MAXFIELDNAMELEN       (4)
#define C_MAXFIELDVALLEN     (2048)

/* selection keyword inps */
#define C_IMPORT           (1)
#define C_PREVIEW          (2)

/* for field struct definition */
#define C_AKTIV            (1)
#define C_DEAKTIV          (0)
#define C_NOTSET           (-1)
#define C_SET              (1)
#define C_DISABLE          (0)
#define C_ENABLE           (1)
#define C_FOUND            (0)
#define C_NOTFOUND         (1)
#define C_SEARCH           (1)
#define C_INSERT           (2)
#define C_UPDATE           (3)
#define C_SELECT           (4)
#define C_FLDCMP           (5)

#define C_FIRST            (1)
#define C_NEXT             (2)
#define C_LAST             (3)
#define C_PREV             (4)

/* FUNCTION ERROR CODES */
#define C_NO_ERROR           (0)
#define C_UNKNOWN_COMMAND    (1)
#define C_NO_ACTIVE_FIELDS   (2)
#define C_SET_ON_FIRST_FIELD (3)
#define C_SET_ON_LAST_FIELD  (4)


/* DEFINITION FOR LOADING URNOS */
#define C_RESERVED_X_URNOS   (100)

#define CEDA_DATE_LEN   (14)

#include <stdio.h>
#include <malloc.h>
#include <errno.h>
#include <signal.h>
#include "ugccsma.h"
#include "msgno.h"
#include "glbdef.h"
#include "quedef.h"
#include "uevent.h"
#include "sthdef.h"
#include "debugrec.h"
#include "hsbsub.h"
#include "db_if.h"
#include "tlxgen.h"
#include "send.h"
#include "ct.h"

#define SUCCESS RC_SUCCESS
#define FAIL    RC_FAIL


/******************************************************************************/
/* External variables                                                         */
/******************************************************************************/
/* outp is defined in ugccsma.h! double definitions kill */
/* your process under HP-UX immediatly !!!!              */
/*FILE *outp       = NULL;*/
int  debug_level = TRACE;
int MyDebugSwitch = FALSE;

/*************************************************************************/
/* STRUCTS                                                               */
/*************************************************************************/
typedef struct
{
 int  Status      [C_MAXFIELDS];
 char Fieldname   [C_MAXFIELDS][C_MAXFIELDNAMELEN+1];
 char FieldVal    [C_MAXFIELDS][C_MAXFIELDVALLEN];
 int  StrItem     [C_MAXFIELDS];
 long StrBeginPos [C_MAXFIELDS];
 long ValLength   [C_MAXFIELDS];
 long Item        [C_MAXFIELDS];
 int  ReadFromEv  [C_MAXFIELDS];
 int  LastAttach  [C_MAXFIELDS];
 int  DoFldCmp    [C_MAXFIELDS];
 int  DoUpdate    [C_MAXFIELDS];
 int  DoSelect    [C_MAXFIELDS];
 int  DoInsert    [C_MAXFIELDS];
 int  DoSearch    [C_MAXFIELDS];

} ST_EVT_DATA_HDL;


typedef struct 
{
 char    cDateStampStr [32] ;
 int     iYear ;
 int     iMonth ;
 int     iDate ;
 int     iHour ;
 int     iMins ;
 int     iSecs ;
 int iStDateInterpret ;
 int iStTimeInterpret ;
} DATEINFO ;

typedef struct 
{
    char pcDelayDura[6];
    char pcDelayReas[4001];
    char pcDelayCode[5];
} DELAY_STRUCT;

typedef struct 
{
    int iPaxInfo;
    int iInfant;
} PAX_STRUCT;

typedef struct 
{
    char pcTana[4];
    char pcFina[5];
    char pcPrefix[3];
} MCGTAB_STRUCT;

/******************************************************************************/
/* Global variables                                                           */
/******************************************************************************/
static ITEM  *prgItem      = NULL;                /* The queue item pointer  */
static EVENT *prgEvent     = NULL;                /* The event pointer       */
static int   igItemLen     = 0;                   /* length of incoming item */
static int   igInitOK      = FALSE;
static char  cgConfigFile[512];
static EVENT * prgOutEvent = NULL ;
static ST_EVT_DATA_HDL    sBRS_ARR ;
static ST_EVT_DATA_HDL    sMVT_ARR ;
static ST_EVT_DATA_HDL    sMV1_ARR ;
static ST_EVT_DATA_HDL    sTLX_ARR ;
static ST_EVT_DATA_HDL    sBRS_PRN ;
static ST_EVT_DATA_HDL    sMVT_PRN ;
static ST_EVT_DATA_HDL    sMTPREC;

/** MEI 08-DEC-2009 **/
MCGTAB_STRUCT *rgMCG;       /* only re-loaded when process startup */
DELAY_STRUCT rgDELAY[2];
PAX_STRUCT rgPAX;
int igNumMCG;
int igPendingSelect;
int igMaxAddrPerLine;
int igMaxAddrPerTelex;
int igBCHDL;
int igTELEX;
int igSQLHDL;
char pcgNewline[4];
char pcgSenderTelexAddr[128];
char pcgSenderEmailAddr[128];
char pcgSenderEmailSmtp[128];
char pcgSenderEmailPath[128];

int igEmailIsReadyToUse = FALSE;

static int   igMVT_Flag ; 
static int   igBRS_Flag ; 
static int   que_out = 0;
/* static int   igToTlxQue = 7298;  */
static int   igBrsToTlxQue = 7208;
static int   igToTlxQue = 7208;
static int   igToInfQue = 7206; 
static char  cgConfigFile[512] ;
static char  pcgTblNam[12] ;
static char  pcgTblExt[12] ;
static char  pcgChkWks[128] ;
static char  pcgChkUsr[128] ;
static char  pcgSqlBuf[12*1024] ;
static char  pcgDataArea[4096] ;
static char  pcgTmpDataArea[4096] ;
static char  pcgHomeHopo[64] ; 
static char  cgDateFrom[32] ; 
static char  pcgTimeInfo[64] ; 
static char  pcgDateOffset[64] ; 
static char  cur_time[16];  /* buffer for timestamp */

static int   igRealImportFlag ;  /* send 2 flight if TRUE */
static int   igActUrno ;
static int   igReservedUrnoCnt ;

static char  pcgRecvName[64] ;  /* BC-Head used as WKS-Name */
static char  pcgDestName[64] ;  /* BC-Head used as USR-Name */
static char  pcgTwStart[64] ;   /* BC-Head used as Stamp    */
static char  pcgTwEnd[64] ;     /* BC-Head used as Stamp    */

/* BST 17-12-09 */

static char pcgCurTplType[8];

static char pcgActTrgFldNames[1024];
static char pcgActTrgNewValues[1024];
static char pcgActTrgOldValues[1024];

static char pcgAftSqlFldNames[1024];
static char pcgAftSqlFldValues[4096];

static char pcgAftRecFldNames[1024];
static char pcgTplRecFldNames[1024];
static char pcgTplRecFldTypes[1024];
static char pcgTplFldDatTypes[1024];
static char pcgTplFldDatProps[1024];
static char pcgTplRecFldValues[4096];
static char pcgTplRecFldValuesSve[4096];

static char pcgMtpRecFldNames[1024];
static char pcgMtpRecFldValues[4096];
static char pcgMtpTplFldValues[4096];

static STR_DESC rgMyDataString;
static char pcgTlxTmplText[1024];
static char pcgTlxTmplPatched[1024];

static int igSaveAsPending = FALSE;
static char pcgSendTlxTime[32] = "";

static int igFlightIsArrival = FALSE;
static int igFlightIsDeparture = FALSE;
static int igFlightIsDelayed = FALSE;
static int igDelayIsAccepted = FALSE;
static int igDelayCodesRequired = FALSE;
static int igDelayTimesRequired = FALSE;
static int igTotalDelayTime = 0;

/* Buffers for Telex Addresses and EMail Adresses */
static char pcgMadSitaAddr[4096*2];
static char pcgTplSitaAddr[4096*2];
static char pcgAllSitaAddr[4096*2];
static char pcgMadMailAddr[4096*2];
static char pcgTplMailAddr[4096*2];
static char pcgAllMailAddr[4096*2];
static int igMadSitaCount = 0;
static int igTplSitaCount = 0;
static int igAllSitaCount = 0;
static int igMadMailCount = 0;
static int igTplMailCount = 0;
static int igAllMailCount = 0;

static int igTlxTxtSize = 4000;

/***************************************************************************/
/* Flags for testing                                                       */
/***************************************************************************/
/* if one of the parameters is set to = 1 --> test is active ELSE no test  */

static int gl_updateTest ;
static int gl_noChangeTest ;
static int igTestFktFlag ;     /* if set to TRUE >> additional fct. output */

/***************************************************************************/
/* External functions                                                      */
/***************************************************************************/

extern int get_item (int, char *, char *, int, char *, char *, char *) ;
extern int get_real_item (char *, char *, int) ;
extern int get_item_no (char *, char *, int) ;
extern int FullDayDate (char * clDateStr, int ilDay ) ;
extern int BuildItemBuffer(char *, char *, int, char *) ;

/*****************************************************************************/
/* Function prototypes                                                       */
/*****************************************************************************/
static int      Init_cedadb() ;
static int      Reset(void) ;                    /* Reset program            */
static void     Terminate(void) ;                /* Terminate program        */
static void     HandleSignal(int) ;              /* Handles signals          */
static void     HandleErr(int) ;                 /* Handles general errors   */
static void     HandleQueErr(int) ;              /* Handles queuing errors   */
static int      HandleData(void) ;               /* Handles event data       */
static void     HandleQueues(void) ;             /* Waiting for Sts.-switch  */
static int      handleBRS_Call(char*, char *) ;  /* load pricipial data      */
static int      getNextBRSFilename() ;           /* chk., if something to do */
static int      getItemCnt  (char *cpFldStr, int *ipCnt) ;
static int      prepSqlValueStr (int iItems, char *cpValStr) ;
static int      GetNextUrno (int *pipUrno) ;
static int      GetNextUrnoItem (int *pipUrno) ;
static int      GetLocalTimeStamp (char *cpTimeStamp) ;
static int      readDataItemFromStr( char *cpFldData, char *cpFldName,
                                     char *cpFldLst, char *cpDataArea) ;
static int      getDataFromDbStrs (int *iItemNo, char *cFld,
                                   char *pcgSearchFldLst, /* fld list select */
                                   char *pcgDataArea,    /* data list select */
                                   char *pcgFoundData
                                  ) ;
static int      setTlxTableDat  (ST_EVT_DATA_HDL *sTLX_ARR, 
                                 ST_EVT_DATA_HDL *sSND_ARR ) ; 
static int      getTextField (ST_EVT_DATA_HDL sSND_ARR, char *ipTxtStr ) ;
static int      createValStr4Tlx (char *cpValStr, ST_EVT_DATA_HDL sTLX_ARR ) ;  
static int      SQLInsertTlxTab (ST_EVT_DATA_HDL sTlxTab) ; 
static int      getVialShortTxt (char *cpVialStr, char *cpVialShortStr) ; 
static int      getBRSHeaderInfo (ST_EVT_DATA_HDL sBRS_ARR, 
                                  ST_EVT_DATA_HDL *sBRS_PRN ) ;  
static int      getBRSTlxTextData (void) ; 
static int      createMVTTelex (void) ;  
static int      TriggerAction(char *pcpTableName, char *pcpCmdRec, 
                                                    char *pcpFieldName) ; 
static int      WaitAndCheckQueue(int ipTimeout, int ipModId, ITEM **prpItem);
static int      CheckQueue(int ipModId, ITEM **prpItem);
static int      handleMVT_Call (char *cpUrno, char *pcpField, char *pcpData, char *cpAction ) ; 
static int      createBRSTelex (void) ;  


/* field struct handle functions */
static int    ResetFldStruct (ST_EVT_DATA_HDL *sFldArr) ;
static int    fillFldStruct (char *cpFldsStr, ST_EVT_DATA_HDL *sFldArr) ;
static int    fillDataInFldStruct (char *cpDataStr, ST_EVT_DATA_HDL *sFldArr) ;
static int    AddFld2Struct (ST_EVT_DATA_HDL *sFdlArr, char *cpFldName,
                             char *cpValStr ) ;
static int    InsOrReplFld2Struct (ST_EVT_DATA_HDL *sFldArr, char *cpFldName,
                                   char *cpValStr ) ;
static int    ReplaceFldDataInStruct (char *cpDatStr, char *pclFld,
                                      ST_EVT_DATA_HDL *sFldArr) ;
static int    getFldDataFromStruct (char *cpRetStr, char *cpFld,
                                    ST_EVT_DATA_HDL sFldArr) ;
static int    DoesFldExistInStruct (char *cpFld, ST_EVT_DATA_HDL sFldArr) ;
static int    getFldStruct (char *pclFldsStr, char *pclDataStr, int iAction,
                            ST_EVT_DATA_HDL *sFldArr) ;
static int    setNextFldPosInStruct (int *ipActionResult, int iAction,
                                     ST_EVT_DATA_HDL *sFldArr) ;
static int    setChkFldinStruct4Fld (char *pclFld, int iDoWhat,
                                     ST_EVT_DATA_HDL *sFldArr) ;
static int    setStatusInStruct (char *pclFld, int iDoWhat, int iKindOfFlg,
                                 ST_EVT_DATA_HDL *sFldArr) ;
static int    createFldAndDataLst (char *cpFldLst, char *cpDataLst,
                                int iKindOf, ST_EVT_DATA_HDL sFldArr) ;
static int    ifRecordChanged (int *iChgFlg, char *pcgSearchFldLst,
                               char *pcgDataArea, ST_EVT_DATA_HDL sFldArr) ;

static int    GetItemsFromCfgFile(char *cpConfigFile) ; 
static int    getDateDiff (DATEINFO *st_beginDate, DATEINFO *st_lastDate,
                           int *retDateCnt ) ;
static int    interpretDateInfo (char *charDate, DATEINFO *st_Date ) ;
static int    incDateAtOne (char *retStrDate, DATEINFO *st_Date,
                            int *ipStatus ) ;

static int    startWhileTimeReached (ST_EVT_DATA_HDL *stAct, char *cpDateFrom, char *cpDateTo) ; 
static int    readMVTTab (void) ;  
static int    ReadTelexAndSend (char *pcpFields, char *pcpData ) ; 
static int    generateMTVTelex (char *clFldLst, char *clData) ; 

static void PatchTextServerToClient(char *pcpText, int ipUseOld);

static void ForwardTelexToFdihdl(char *pcpSelKey, char *pcpData);

static int CreateInftabXML(char *pcpSelKey, char *pcpFields, char *pcpData);


static void TestReplace(void);
static void SetStringValue(STR_DESC* prpStringBuffer, char *pcpNewString);
static void ReplacePatternInString(STR_DESC* prpString, char *pcpSearch, char *pcpReplace);


/** MEI 08-DEC-2009 **/
static int RunSQL( char *pcpSelection, char *pcpData );
static int TimeDiff( char *pcpTime1, char *pcpTime2, int *ipTimeDiff );
static int MVTTelexSending( ST_EVT_DATA_HDL pAFT_RCVNEW, char cpStatus );
static int GetDelayInfo( char *pcpUAFT );
static int GetPaxInfo( char *pcpUAFT );
static int InterpretMVTTemp( char *pcpResult, char *pcpTemplate );
static int GetVarValue( char *pcpVarValue, char *pcpVarName, char *pcpFormat );
static int AutoSendMVTTelex( char *pcpTelex, char cpStatus );
static int GetTelexAddr( int *ipNumTelexAddr, int *ipNumEmailAddr, char *pcpTelexAddr, char *pcpEmailAddr, int ipIncludeHome );


/** BST 17-DEC-2009 **/
static int HandleSqlSelectRows(char *pcpTable, char *pcpSqlKey, char *pcpSqlFld, char *pcpSqlDat, int ipLines);
static int get_any_item(char *dest, char *src, int ItemNbr, char *sep);
static int GetFieldValue(char *pcpFldVal, char *pcpFldLst, char *pcpFldNam, char *pcpDatLst);
static int GetFieldValues(char *pcpFldVal, char *pcpFldLst, char *pcpFldNam, char *pcpDatLst);
static int GetCfg (char *, char *, char *, short, char *, char *);

static int InitAutoMvtConfig(void);
static void SendTestEvents(int ipWhat);
static int HandleAutoSendTrigger(char *pcpFld, char *pcpDat, int ipCaller, char *pcpGetType);
static int GetAftDataSnapshot(char *pcpAftUrno);
static int CreateTlxTplRecord(void);
static int ReplaceStringInString(char *pcpString, char *pcpSearch, char *pcpReplace);
static int PatchTemplateText(void);
static int CompleteTplDataSet(void);
static int GetTlxTextFields(char *pcpTypeCode, int ipAddrItmNo, char *pcpTxt1, char *pcpTxt2);
static int CreateTlxSnapshotRec(char *pcpTypeCode, char *pcpStatCode);
static int GetMailAddresses(void);
static int GetSitaAddresses(void);
static int GetTemplateConfig(char *pcpIgnore, char *pcpWanted, int ipCaller);
static void CheckTelexTimeConditions(int ipCaller);
static void CheckTelexWaitStatus(int ipCaller);
static void CheckFlightDelayStatus(void);
static int EvaluateDelayInfo(char *pcpAftDCD1,char *pcpAftDCD2,char *pcpAftDTD1,char *pcpAftDTD2,char *pcpAftDELD);
static int CheckFieldRequired(char *pcpAftName, char *pcpTplName);
static int GetNextStationCode(char *pcpTplNstn);
static int GetShortStationList(char *pcpTplVial,char *pcpPreFix,char *pcpSuffix,char *pcpValSep);
static int EvaluatePaxInfo(char *pcpAftPAX1,char *pcpAftPAX2,char *pcpAftPAX3,char *pcpAftPXSI);
static int GetNextStationETA(char *pcpTplFldVal);
static int GetSitaAddrBlock(int ipFirstItmNo, char *pcpAddrTo, char *pcpAddrCc);
static int CheckTelexAndSend(char *pcpFields, char *pcpData);
static int SendConfigToClient(int ipQueOut, char *pcpSelKey, char *pcpFields, char *pcpData);
static int GetMinutesFromTime(char *pcpTime);
static int FormatMinutesToTime(int ipMinutes, char *pcpTime, int ipCmpVal);
static int FetchPaxValueFromDB(char *pcpAftUrno, char *pcpDataType, char *pcpGotValue);
static int CheckTelexTemplateConditions(int ipCaller, int ipAskUser);
static int CleanDoubleBytes(char *pcpText, char *pcpFind, char *pcpSet);

/******************************************************************************/
/*                                                                            */
/* The MAIN program                                                           */
/*                                                                            */
/******************************************************************************/
MAIN
{
 int     ilRC = RC_SUCCESS ;            /* Return code            */
 int     ilCnt = 0 ;

    char dateTo[64] ; 
    char dateFrom_i[64] ; 
    char dateFrom[64] ;
    char helpStr[64] ; 
    int  i ; 


 /*--------------------------------------------------------------------*/
 /* the normal sequenz of this process is the simulation mode. This    */
 /* means, that the flag igRealImportFlag is set to FALSE. This        */
 /* incident can be solved by the event 998. The 998 calling causes a  */
 /* toggle to the igRealImportFlag.                                    */
 /*--------------------------------------------------------------------*/

 debug_level = TRACE ;     /* also debug level is on */

 INITIALIZE ;              /* General initialization       */
 dbg (TRACE, "MAIN: version <%s>", mks_version) ;
 igRealImportFlag = TRUE ;

 /* setting Testfalgs */
 /* all telex modes are set to disable here. For use a telex */
 /* format, you should turn on with the telex.cfg inputs     */
 /* the CONFIG names, which intressting for this, can you    */
 /* find after the disable statements in comment             */ 

 igMVT_Flag = C_DISABLE ;       /* [MVT]  MVT_TELEXE_ON */ 
 igBRS_Flag = C_DISABLE ;       /* [BRS]  BRS_TELEXE_ON */ 


 igTestFktFlag = TRUE ;
 gl_noChangeTest = 0 ;
 igActUrno = 0 ;          /* URNO VALUE - not set */
 igReservedUrnoCnt = 0 ;  /* Urno Buffer - no reserved Urnos */
 ilRC = GetLocalTimeStamp(cgDateFrom) ;

 /* Attach to the MIKE queues */
 do
 {
  ilRC = init_que() ;
  if (ilRC != RC_SUCCESS)
  {
   dbg (TRACE, "MAIN: init_que() failed! waiting 6 sec ...") ;
   sleep(6) ;
   ilCnt++ ;
  } /* if */
 } while ((ilCnt < 10) && (ilRC != RC_SUCCESS)) ;

 if (ilRC != RC_SUCCESS)
 {
  dbg (TRACE,"MAIN: init_que() failed! waiting 60 sec ...") ;
  sleep(60) ;
  exit(1) ;
 }
 else
 {
  dbg (TRACE, "MAIN: init_que() OK! mod_id <%d>",mod_id) ;
 } /* if */

 do
 {
  ilRC = init_db();
  if (ilRC != RC_SUCCESS)
  {
   check_ret (ilRC) ;
   dbg (TRACE, "MAIN: init_db() failed! waiting 6 sec ...") ;
   sleep(6) ;
   ilCnt++ ;
  } /* if */
 } while ((ilCnt < 10) && (ilRC != RC_SUCCESS)) ;


 if (ilRC != RC_SUCCESS)
 {
  dbg (TRACE,"MAIN: init_db() failed! waiting 60 sec ...") ;
  sleep(60) ;
  exit(2) ;
 }
 else
 {
  dbg (TRACE, "MAIN: init_db() OK!") ;
 } /* if */

 /* logon to DB is ok, but do NOT use DB while ctrl_sta == HSB_COMING_UP !!! */

 sprintf(cgConfigFile,"%s/tlxgen",getenv("BIN_PATH")) ;
 ilRC = TransferFile(cgConfigFile) ;
 if (ilRC != RC_SUCCESS)
 {
  dbg(TRACE, "MAIN: TransferFile(%s) failed!", cgConfigFile) ;
 } /* if */

 sprintf (cgConfigFile, "%s/tlxgen.cfg", getenv ("CFG_PATH")) ;
 ilRC = TransferFile(cgConfigFile) ;
 ilRC = GetItemsFromCfgFile (cgConfigFile) ; 
 if(ilRC != RC_SUCCESS)
 {
  dbg(TRACE,"MAIN:CONFIG FILE READING ERROR -> CANT WORK (%s)!", cgConfigFile) ;
 } /* if */


 ilRC = SendRemoteShutdown (mod_id) ;
 if (ilRC != RC_SUCCESS)
 {
  dbg (TRACE,"MAIN: SendRemoteShutdown(%d) failed!", mod_id) ;
 } /* if */

 if ((ctrl_sta != HSB_STANDALONE) && (ctrl_sta != HSB_ACTIVE)
    && (ctrl_sta != HSB_ACT_TO_SBY))
 {
  dbg (DEBUG,"MAIN: waiting for status switch ...") ;
  HandleQueues() ;
 } /* if */

 if ((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE) || (ctrl_sta == HSB_ACT_TO_SBY))
 {
  dbg (TRACE, "MAIN: initializing ...") ;
  if (igInitOK == FALSE)
  {
   ilRC = Init_cedadb() ;
   if (ilRC != RC_SUCCESS)
   {
    dbg (TRACE, "Init_cedadb: init failed!") ;
   } /* if */
  } /* if */
 }
 else
 {
  Terminate() ;
 } /* if */

/*  if (igMVT_Flag != C_ENABLE)
 {
  ilRC = TriggerAction("AFTTAB", "MV1", "NXTI") ; 
  ilRC = TriggerAction("AFTTAB", "MV2", "AIRB") ; 
  ilRC = TriggerAction("AFTTAB", "MV3", "EDTI") ; 
  ilRC = TriggerAction("AFTTAB", "MV4", "FLDA") ; 
  ilRC = TriggerAction("AFTTAB", "MV5", "VIAL") ; 
  ilRC = TriggerAction("AFTTAB", "MV6", "ONBL") ; 
 }
*/
 dbg (TRACE,"MAIN: initializing OK");
 for (;;)
 {
  dbg( TRACE, "============================= START/END =========================================" );
  ilRC = que (QUE_GETBIG, 0, mod_id, PRIORITY_3, igItemLen,
             (char *) &prgItem) ;
  /* depending on the size of the received item  */
  /* a realloc could be made by the que function */
  /* so do never forget to set event pointer !!! */
  prgEvent = (EVENT *) prgItem->text ;

  if( ilRC == RC_SUCCESS )
  {                            /* Acknowledge the item */

   ilRC = que (QUE_ACK, 0, mod_id, 0, 0, NULL) ;
   if ( ilRC != RC_SUCCESS )
   {                         /* handle que_ack error */
    HandleQueErr(ilRC);
  } /* fi */

  switch( prgEvent->command )
  {
   case    HSB_STANDBY     :
           ctrl_sta = prgEvent->command ;
           HandleQueues() ;
          break ;

   case    HSB_COMING_UP   :
           ctrl_sta = prgEvent->command ;
           HandleQueues() ;
          break ;

   case    HSB_ACTIVE      :
           ctrl_sta = prgEvent->command ;
          break ;

   case    HSB_ACT_TO_SBY  :
           ctrl_sta = prgEvent->command ;
           /* CloseConnection() ; */
           HandleQueues() ;
          break ;

   case    HSB_DOWN        :
           /* whole system shutdown - do not further use que(), send_message() or timsch() ! */
           ctrl_sta = prgEvent->command ;
           Terminate() ;
          break ;

   case    HSB_STANDALONE  :
           ctrl_sta = prgEvent->command ;
           ResetDBCounter() ;
          break ;

   case    REMOTE_DB :
           /* ctrl_sta is checked inside */
           HandleRemoteDB(prgEvent) ;
          break ;

   case    SHUTDOWN        :
           /* process shutdown - maybe from uutil */
           Terminate();
          break ;

   case    RESET           :
           ilRC = Reset() ;
          break ;

   case    EVENT_DATA      :
           if((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE) || (ctrl_sta == HSB_ACT_TO_SBY))
           {
            ilRC = HandleData() ;
            if (ilRC != RC_SUCCESS)
            {
             HandleErr(ilRC) ;
            } /* if */
           }
           else
           {
            dbg (TRACE, "MAIN: wrong hsb status <%d>", ctrl_sta) ;
            DebugPrintItem (TRACE, prgItem) ;
            DebugPrintEvent (TRACE, prgEvent) ;
           } /* if */
          break ;

   case    TRACE_ON :
           dbg_handle_debug (prgEvent->command) ;
          break ;

   case    TRACE_OFF :
           dbg_handle_debug (prgEvent->command) ;
          break ;


   case 100:     /* data testmode */

           dbg (DEBUG, "------------------------------------------------") ;
           dbg (DEBUG, "I  Using BRS test datas                        I") ;
           dbg (DEBUG, "------------------------------------------------") ;
                     if (igBRS_Flag == C_ENABLE)
                     {
            handleBRS_Call ("20010317000000", "20010317201000" ) ;
           } 
                     else 
                     {
                        dbg (DEBUG, "BRS handle switch is off >> change config!") ; 
                     }
          /*  SendCedaEvent (9200,0,"ufis","Y","0","ATH,TAB,BDPS-UIF","BRS",
                           "","BBRS","FLD","DAT","X",4, RC_SUCCESS) ;    */
          break ;


   case 101:     /* data testmode */

           dbg (DEBUG, "------------------------------------------------") ;
           dbg (DEBUG, "I  Using MVT test datas                        I") ;
           dbg (DEBUG, "------------------------------------------------") ;

                     if (igMVT_Flag == C_ENABLE)
                     {
                handleMVT_Call ("0030883440", NULL, NULL, "AB") ;
           }
                     else 
                     {
                        dbg (DEBUG, "MVT handle switch is off >> change config!") ; 
                     }

                /*  SendCedaEvent (9200,0,"ufis","Y","0","ATH,TAB,BDPS-UIF","BRS",
                           "","BBRS","FLD","DAT","X",4, RC_SUCCESS) ;    */
          break ;

       case 102:     /* data testmode */

           dbg (DEBUG, "------------------------------------------------") ;
           dbg (DEBUG, "I  Test the date / time differenz function     I") ;
           dbg (DEBUG, "------------------------------------------------") ;

                     strcpy(dateFrom_i, "200103xx000000") ; 
                     for (i=1; i<25; i++)  
                     {
                        sprintf(helpStr, "%#02d", i) ;
                        dateFrom_i[6] = helpStr[0] ; 
                        dateFrom_i[7] = helpStr[1] ; 
                ReplaceFldDataInStruct (dateFrom_i, "#SDT", &sBRS_ARR) ; 
                      ilRC = startWhileTimeReached (&sBRS_ARR, dateFrom, dateTo) ; 

           }
    break;                      


  case 700:
    dbg (TRACE, "------------------------------------------------") ;
    SendTestEvents(1);
    dbg (TRACE, "------------------------------------------------") ;
    break;                      
  case 701:
    dbg (TRACE, "------------------------------------------------") ;
    SendTestEvents(2);
    dbg (TRACE, "------------------------------------------------") ;
    break;                      
  case 777:
    dbg (TRACE, "------------------------------------------------") ;
    SendConfigToClient(0, "", "", "");
    /* TestReplace(); */
    dbg (TRACE, "------------------------------------------------") ;
    break;                      


       case 997:

            if (igTestFktFlag == FALSE)
            {
             igTestFktFlag = TRUE ;
             debug_level = DEBUG ;     /* also debug level is on */
            }
            else
            {
             igTestFktFlag = FALSE ;
            }
          break ;


       case 998:

        /*--------------------------------------------------------------------*/
        /* the normal sequenz of this process is the simulation mode. This    */
        /* means, that the flag igRealImportFlag is set to FALSE. This        */
        /* incident can be solved by the event 998. The 998 calling causes a  */
        /* toggle to the igRealImportFlag.                                    */
        /*--------------------------------------------------------------------*/

         dbg (DEBUG, "---------------------------------") ;
         if (igRealImportFlag == FALSE)
         {
          dbg (DEBUG, "MAIN: real import is switched on!") ;
          debug_level = DEBUG ;     /* also debug level is on */
          igRealImportFlag = TRUE ;
         }
         else
         {
          dbg (DEBUG, "MAIN: real import is switched off!") ;
          igRealImportFlag = FALSE ;
         }
         dbg (DEBUG, "---------------------------------") ;
              break ;

   default   :
           dbg (TRACE, "MAIN: unknown event") ;
           DebugPrintItem (TRACE, prgItem) ;
           DebugPrintEvent (TRACE, prgEvent) ;
           /* break ;  statement not reached */
   } /* end switch */
  }
  else
  {                       /* Handle queuing errors */
   HandleQueErr (ilRC) ;
  } /* end else */

 } /* end for */

 /* exit(0) ;      statement not reached */

} /* MAIN */




/*****************************************************************************/
/* read all important Items from the config file !                           */
/* this function was written for a dynamicly read between the runtime!       */
/*****************************************************************************/
static int GetItemsFromCfgFile(char *cpConfigFile)
{
 int   ilRC = RC_SUCCESS ; 
 int   ilZ ; 
 int   ilStat ; 
 int   il ;
 int   il4Chr ; 
 int   ilLen ;
 long int ilFldLen ;
 char  clSelection[64] ; 
 char  cpDestStr[1024] ; 
 char  cpFoundStr[1024] ;
 char  clCfgItem[100] ; 
 char  clStrItem[100] ;
 char  cpHlpStr[100] ; 
 char pclTmpBuf[128];

 ilStat = RC_SUCCESS ; 
 /* test, if this function is used between the runtime or at first time */
 if (DoesFldExistInStruct ("IERR", sBRS_ARR) == RC_SUCCESS)
 {
    /* Reset here all other telex structs */
  ilRC = ResetFldStruct (&sBRS_ARR) ;
  ilRC = ResetFldStruct (&sTLX_ARR) ;
  ilRC = ResetFldStruct (&sMVT_ARR) ;
  ilRC = ResetFldStruct (&sMV1_ARR) ;
  ilRC = ResetFldStruct (&sMVT_PRN) ;
  ilRC = ResetFldStruct (&sBRS_PRN) ;

    dbg (DEBUG, "------------------------------") ; 
    dbg (DEBUG, ">>> Interpret CONFIG FILE new!") ; 
    dbg (DEBUG, "------------------------------") ; 
 }
 else 
 {
    dbg (DEBUG, "----------------------------------------") ; 
    dbg (DEBUG, ">>> Interpret CONFIG FILE at first time!") ; 
    dbg (DEBUG, "----------------------------------------") ; 
 }

  /* set PRINTER FIELDS FOR BRS TELEXE */
  ilRC = AddFld2Struct (&sBRS_PRN, "H001", "" ) ; 
  ilRC = AddFld2Struct (&sBRS_PRN, "H002", "" ) ; 
  ilRC = AddFld2Struct (&sBRS_PRN, "H003", "" ) ; 
  ilRC = AddFld2Struct (&sBRS_PRN, "H004", "" ) ; 
  ilRC = AddFld2Struct (&sBRS_PRN, "H005", "" ) ; 
  ilRC = AddFld2Struct (&sBRS_PRN, "H006", "" ) ; 
  ilRC = AddFld2Struct (&sBRS_PRN, "H007", "" ) ; 
  ilRC = AddFld2Struct (&sBRS_PRN, "H008", "" ) ; 
  ilRC = AddFld2Struct (&sBRS_PRN, "H009", "" ) ; 
  ilRC = AddFld2Struct (&sBRS_PRN, "H010", "" ) ; 
  ilRC = AddFld2Struct (&sBRS_PRN, "H011", "" ) ; 
  ilRC = AddFld2Struct (&sBRS_PRN, "H012", "" ) ; 
  ilRC = AddFld2Struct (&sBRS_PRN, "H013", "" ) ; 
  ilRC = AddFld2Struct (&sBRS_PRN, "H014", "" ) ; 
  ilRC = AddFld2Struct (&sBRS_PRN, "H015", "" ) ; 
  sprintf (cpHlpStr, ".AAD%c%c",0x0d, 0x0a) ;  
    ilRC = AddFld2Struct (&sBRS_PRN, "BEGI", cpHlpStr ) ; 
  ilRC = AddFld2Struct (&sBRS_PRN, "FLNO", ".F/" ) ; 
  ilRC = AddFld2Struct (&sBRS_PRN, "STOD", ".S/" ) ; 
  ilRC = AddFld2Struct (&sBRS_PRN, "FTYP", ".C/" ) ; 
  ilRC = AddFld2Struct (&sBRS_PRN, "TIFD", ".D/" ) ; 
  ilRC = AddFld2Struct (&sBRS_PRN, "TIFT", ".T/" ) ; 
  ilRC = AddFld2Struct (&sBRS_PRN, "ADID", ".H/" ) ; 
  ilRC = AddFld2Struct (&sBRS_PRN, "VIAL", ".R/" ) ; 

    /* fields for read the MVTTAB */
    ilRC = AddFld2Struct (&sMV1_ARR, "URNO", "" ) ; 
  ilRC = setStatusInStruct ("URNO", C_ENABLE, C_SELECT, &sMV1_ARR) ;  
    ilRC = AddFld2Struct (&sMV1_ARR, "ALC3", "" ) ; 
  ilRC = setStatusInStruct ("ALC3", C_ENABLE, C_SELECT, &sMV1_ARR) ;  
    ilRC = AddFld2Struct (&sMV1_ARR, "APC3", "" ) ; 
  ilRC = setStatusInStruct ("APC3", C_ENABLE, C_SELECT, &sMV1_ARR) ;  
    ilRC = AddFld2Struct (&sMV1_ARR, "TEAD", "" ) ; 
  ilRC = setStatusInStruct ("TEAD", C_ENABLE, C_SELECT, &sMV1_ARR) ;  
    ilRC = AddFld2Struct (&sMV1_ARR, "TEMA", "" ) ; 
  ilRC = setStatusInStruct ("TEMA", C_ENABLE, C_SELECT, &sMV1_ARR) ;  
    ilRC = AddFld2Struct (&sMV1_ARR, "SMVT", "" ) ; 
  ilRC = setStatusInStruct ("SMVT", C_ENABLE, C_SELECT, &sMV1_ARR) ;  
    ilRC = AddFld2Struct (&sMV1_ARR, "VAFR", "" ) ; 
  ilRC = setStatusInStruct ("VAFR", C_ENABLE, C_SELECT, &sMV1_ARR) ;  
    ilRC = AddFld2Struct (&sMV1_ARR, "BEME", "" ) ; 
  ilRC = setStatusInStruct ("BEME", C_ENABLE, C_SELECT, &sMV1_ARR) ;  
    ilRC = AddFld2Struct (&sMV1_ARR, "PAYE", "" ) ; 
  ilRC = setStatusInStruct ("PAYE", C_ENABLE, C_SELECT, &sMV1_ARR) ;  

 ilRC = AddFld2Struct (&sBRS_ARR, "IERR", "" ) ; /* Test Fld. BRS */
 ilRC = setStatusInStruct ("IERR", C_DISABLE, 0, &sBRS_ARR) ;

 /* read all globals into globals fields    - GLOBAL -  */
 sprintf (clSelection, "GLOBAL") ; 

 /* read home airport */
 ilRC = iGetConfigEntry(cpConfigFile, clSelection, "HOMEAIRPORT", CFG_STRING,pcgHomeHopo);
 if (ilRC != RC_SUCCESS)
 {
  dbg (DEBUG, "ERR! - cant read <HOMEAIRPORT> from <%s>", cpConfigFile) ;
  pcgHomeHopo[0] = 0x00 ;
    ilStat = RC_FAIL ;     /* input needed */
 }
 
 /* GET TLX Destination */
 ilRC = iGetConfigEntry(cpConfigFile, clSelection, "BRS_TELEX_MOD_ID", CFG_STRING, pclTmpBuf) ; 
 if (ilRC == RC_SUCCESS)
 {
   igBrsToTlxQue = atoi(pclTmpBuf);
 }
 else
 {
 }
 dbg(TRACE,"SENDING BRS TELEXES TO TELEX_MOD_ID %d",igToTlxQue);
 

 /* read next date offset */
 ilRC = iGetConfigEntry(cpConfigFile, clSelection, "DATE_OFFSET", CFG_STRING,
                                                pcgDateOffset) ; 
 if (ilRC != RC_SUCCESS)
 {
  dbg (DEBUG, "ERR! - cant read <DATE_OFFSET> from <%s>", cpConfigFile) ;
  pcgDateOffset[0] = 0x00 ;
    ilStat = RC_FAIL ;     /* input needed */
 }
 else
 {
  dbg(DEBUG,
            "read <DATE_OFFSET> from <%s> with <%s>",cpConfigFile,pcgDateOffset); 
 }

 /* read send time info */
 ilRC = iGetConfigEntry(cpConfigFile, clSelection, "INFO_TIME", CFG_STRING,
                                                pcgTimeInfo) ; 
 if (ilRC != RC_SUCCESS)
 {
  dbg (DEBUG, "ERR! - cant read <INFO_TIME> from <%s>", cpConfigFile) ;
  pcgTimeInfo[0] = 0x00 ;
    ilStat = RC_FAIL ;     /* input needed */
 } 
 else
 {
  dbg(DEBUG, "read <INFO_TIME> from <%s> with <%s>",cpConfigFile,pcgTimeInfo) ;
 }

 /* -------- read TLXTAB fields -------------- */
 ilRC = iGetConfigEntry(cpConfigFile, clSelection, "SQL_TLXTAB_FLDS", 
                                                CFG_STRING, cpDestStr) ; 
 if (ilRC != RC_SUCCESS) 
 {
  dbg(DEBUG, "ERR! - cant read GLOBAL/SQL_TLXTAB_FLDS from <%s>",cpConfigFile) ;
  ilStat = RC_FAIL ; 
 }
 else    /* bring fields into the struct */  
 { 
    /*  store the comlete field list in field #TLX */
    ilRC = AddFld2Struct (&sTLX_ARR, "#TLX", cpDestStr) ; /* get SQL Fld. list */
  ilRC = setStatusInStruct ("#TLX", C_DISABLE, 0, &sTLX_ARR) ;
  il = 1 ; 
    do 
    {
   ilLen = get_real_item (cpFoundStr, cpDestStr, il) ;
     if (ilLen > 0)
     {
        ilRC = AddFld2Struct (&sTLX_ARR, cpFoundStr, "") ; /* get SQL Fld */
    ilRC = setStatusInStruct (cpFoundStr, C_DISABLE, 0, &sTLX_ARR) ;
    ilRC = setStatusInStruct (cpFoundStr, C_ENABLE, C_INSERT, &sTLX_ARR) ;  
    ilRC = setStatusInStruct (cpFoundStr, C_ENABLE, C_SELECT, &sTLX_ARR) ;  
     }
     else il = -1 ; 
     il++ ; 
    } while (il > 0) ; 
 }

 
 /* -------- read all information for BRS fields  - BRS TELEXE -  */
 sprintf (clSelection, "BRS") ; 
 ilRC = AddFld2Struct (&sBRS_ARR, "#_ID", "BRS" ) ; /* get own id */
 ilRC = setStatusInStruct ("#_ID", C_DISABLE, 0, &sBRS_ARR) ;

 ilRC = iGetConfigEntry(cpConfigFile, clSelection, "SQL_SELECT_FLDS", 
                                                CFG_STRING, cpDestStr) ; 
 if (ilRC != RC_SUCCESS) 
 {
  dbg(DEBUG, "ERR! - cant read BRS/SQL_SELECT_FLDS from <%s>", cpConfigFile) ;
  ReplaceFldDataInStruct
                        ("BRS CANT FIND SQL_SELECT_FLDS in CFG", "IERR", &sBRS_ARR) ; 
  ilStat = RC_FAIL ; 
 }
 else    /* bring fields into the struct */  
 {
    /*  store the comlete field list in field #BRS */
    ilRC = AddFld2Struct (&sBRS_ARR, "#TLX", cpDestStr) ; /* get SQL Fld. list */
  ilRC = setStatusInStruct ("#TLX", C_DISABLE, 0, &sBRS_ARR) ;
  il = 1 ; 
    do 
    {
   ilLen = get_real_item (cpFoundStr, cpDestStr, il) ;
     if (ilLen > 0)     
     {
        ilRC = AddFld2Struct (&sBRS_ARR, cpFoundStr, "") ; /* get SQL Fld */
    ilRC = setStatusInStruct (cpFoundStr, C_DISABLE, 0, &sBRS_ARR) ;
    ilRC = setStatusInStruct (cpFoundStr, C_ENABLE, C_SELECT, &sBRS_ARR) ;  
     }
     else il = -1 ; 
     il++ ; 
    } while (il > 0) ; 
 }

 /* read default text fields */
 /* The default text must start with the string DEFAULT_TXT_001 and */
 /* counts up like DEFAULT_TXT_002,....003 and so on                */
 il = 1 ;      /* start with DEFAULT_TXT_001 */
 while (il > 0) 
 {
    sprintf ( clCfgItem, "BRSDEF_TXT_%#03d", il) ; 
    sprintf ( clStrItem, "@%#03d", il) ;       /* in struct like @001... */ 
  dbg(DEBUG,"Generate Item <%s> for struct item <%s>", clCfgItem, clStrItem) ;
  ilRC = iGetConfigEntry(cpConfigFile, clSelection, clCfgItem, 
                                     CFG_STRING, cpDestStr) ; 
  if (ilRC == RC_SUCCESS)
    {
   /* change the @ char to blanks ! */
   ilZ = strlen (cpDestStr) ;
     il4Chr = 0 ;  
   while (il4Chr < ilZ) 
     {
    if (cpDestStr[il4Chr] == '@')
        { 
         cpDestStr[il4Chr] = ' ' ;
    } 
        il4Chr++ ; 
     }
   /* insert the text item into the struct */
   ilRC = AddFld2Struct (&sBRS_ARR, clStrItem, cpDestStr ) ; 
   ilRC = setStatusInStruct (clStrItem, C_DISABLE, 0, &sBRS_ARR) ;
   ilRC = setStatusInStruct (clStrItem, C_ENABLE, C_SEARCH, &sBRS_ARR) ;  
     il++ ;                /* read next item */
    }
    else il = 0 ;          /* mark the end */
 } /* while not all DEFAULT_TXT_ITEMS read */
 getBRSHeaderInfo (sBRS_ARR, &sBRS_PRN ) ;  


 /* ------------- read BRS MAX FLIGHTS ----------- */
 ilRC = iGetConfigEntry(cpConfigFile, clSelection, "BRS_MAX_FLIGHTS", 
                                                CFG_STRING, cpDestStr) ; 
 if (ilRC != RC_SUCCESS) 
 {
  strcpy (cpDestStr, "1") ; 
 }
 ilRC = AddFld2Struct (&sBRS_ARR, "#DAT", cpDestStr ) ; /* get date */
 ilRC = setStatusInStruct ("#DAT", C_DISABLE, 0, &sBRS_ARR) ;


 /* ------------- read DATE INTERPRET -------- */
 ilRC = iGetConfigEntry(cpConfigFile, clSelection, "DATE_INTERPRET", 
                                                CFG_STRING, cpDestStr) ; 
 if (ilRC != RC_SUCCESS) 
 {
  sprintf (cpDestStr, "JAN,FEB,MAR,APR,MAI,JUN,JUL,AUG,SEP,OCT,NOV,DEC") ; 
 }
 AddFld2Struct (&sBRS_ARR, "#MON", cpDestStr ) ; /* get month short names  */
 setStatusInStruct ("#MON", C_DISABLE, 0, &sBRS_ARR) ;

 /* ------------- read BRS DATE OFFSET ----------- */
 ilRC = iGetConfigEntry(cpConfigFile, clSelection, "BRS_DATE_OFFSET", 
                                                CFG_STRING, cpDestStr) ; 
 if (ilRC != RC_SUCCESS) 
 {
  dbg(DEBUG, "ERR! - cant read BRS/BRS_DATE_OFFSET from <%s>", cpConfigFile) ;
  /* ilStat = RC_FAIL ;       can work without this field */  
 }
 else    /* bring fields into the struct */  
 {
  ilRC = AddFld2Struct (&sBRS_ARR, "#DAT", cpDestStr ) ; /* get date */
  ilRC = setStatusInStruct ("#DAT", C_DISABLE, 0, &sBRS_ARR) ;
 }

 /* ------------- read BRS TIME OFFSET ------------ */
 ilRC = iGetConfigEntry(cpConfigFile, clSelection, "BRS_INFO_TIME", 
                                                CFG_STRING, cpDestStr) ; 
 if (ilRC != RC_SUCCESS) 
 {
  dbg(DEBUG, "ERR! - cant read BRS/BRS_INFO_TIME from <%s>", cpConfigFile) ;
  ilStat = RC_FAIL /* can work without this field */ ;  
 }
 else    /* bring fields into the struct */  
 {
  ilRC = AddFld2Struct (&sBRS_ARR, "#TIM", cpDestStr ) ; /* get date */
  ilRC = setStatusInStruct ("#TIM", C_DISABLE, 0, &sBRS_ARR) ;
 }

 /* ------------- read BRS START DATE TIME ------------ */
 ilRC = iGetConfigEntry(cpConfigFile, clSelection, "BRS_START_DATI", 
                                                CFG_STRING, cpDestStr) ; 
 if (ilRC != RC_SUCCESS) 
 {
    if (cgDateFrom[0] != 0x00) 
    {
   ilRC = AddFld2Struct (&sBRS_ARR, "#SDT", cgDateFrom ) ; /* get date */
  } 
    else 
    {
   ilRC = AddFld2Struct (&sBRS_ARR, "#SDT", "" ) ; /* get no date */
    }
  ilRC = setStatusInStruct ("#SDT", C_DISABLE, 0, &sBRS_ARR) ;
 }
 else    /* bring fields into the struct */  
 {
  ilRC = AddFld2Struct (&sBRS_ARR, "#SDT", cpDestStr ) ; /* get date */
  ilRC = setStatusInStruct ("#SDT", C_DISABLE, 0, &sBRS_ARR) ;
 }


 /* ------------- read BRS PRIORITY ------------ */
 ilRC = iGetConfigEntry(cpConfigFile, clSelection, "BRS_PRIORITY", 
                                                CFG_STRING, cpDestStr) ; 
 if (ilRC != RC_SUCCESS) 
 {
  dbg(DEBUG, "ERR! - cant read BRS/BRS_TIME_OFFSET from <%s>", cpConfigFile) ;
  /* ilStat = RC_FAIL ;    can work without this field */  
 }
 else    /* bring fields into the struct */  
 {
  ilRC = AddFld2Struct (&sBRS_ARR, "#PRI", cpDestStr ) ; /* get date */
  ilRC = setStatusInStruct ("#PRI", C_DISABLE, 0, &sBRS_ARR) ;
 }

 /* ------------- read BRS TELEX SWITCH ON ------------ */
 ilRC = iGetConfigEntry(cpConfigFile, clSelection, "BRS_TELEXE_ON", 
                                                CFG_STRING, cpDestStr) ; 
 igBRS_Flag = C_DISABLE ; 
 if (ilRC != RC_SUCCESS) 
 {
  dbg(DEBUG, "WARNING! - cant read BRS/BRS_TELEXE_ON from <%s>", cpConfigFile) ;
  dbg(DEBUG, "SET BRS TELEX SENDING TO DISABLED") ; 
 }
 else    /* bring fields into the struct */  
 {
  ilRC = AddFld2Struct (&sBRS_ARR, "#SON", cpDestStr ) ; /* get date */
  ilRC = setStatusInStruct ("#SON", C_DISABLE, 0, &sBRS_ARR) ;
  if (cpDestStr[0] == '1')
    {
     igBRS_Flag = C_ENABLE ;  
   dbg(DEBUG, "SET BRS TELEX SENDING TO ENABLED") ; 
  }
    else 
    {
   dbg(DEBUG, "SET BRS TELEX SENDING TO DISABLED") ; 
    }
 }

 /* ------------- read BRS SEND FILEPATH --------- */
 ilRC = iGetConfigEntry(cpConfigFile, clSelection, "BRS_SEND_TLX_PATH", 
                                                CFG_STRING, cpDestStr) ; 
 if (ilRC != RC_SUCCESS) 
 {
  dbg(DEBUG, "ERR! - cant read BRS/BRS_SEND_TLX_PATH from <%s>", cpConfigFile) ;
  ReplaceFldDataInStruct
                        ("BRS CANT FIND BRS_SEND_TLX_PATH in CFG", "IERR", &sBRS_ARR) ; 
  ilStat = RC_FAIL ;    /* need this field */ 
 }
 else    /* bring fields into the struct */  
 {
  ilRC = AddFld2Struct (&sBRS_ARR, "#FIL", cpDestStr ) ; /* get date */
  ilRC = setStatusInStruct ("#FIL", C_DISABLE, 0, &sBRS_ARR) ;
 }


 /* ------------- read BRS ADRESS LIST --------- */
 ilRC = iGetConfigEntry(cpConfigFile, clSelection, "BRS_ADRESS_LIST", 
                                                CFG_STRING, cpDestStr) ; 
 if (ilRC != RC_SUCCESS) 
 {
  dbg(DEBUG, "ERR! - cant read BRS/BRS_ADRESS_LIST from <%s>", cpConfigFile) ;
  ReplaceFldDataInStruct
                        ("BRS CANT FIND BRS_ADRESS_LIST in CFG", "IERR", &sBRS_ARR) ; 
  ilStat = RC_FAIL ;    /* need this field */ 
 }
 else    /* bring fields into the struct */  
 {
  ilRC = AddFld2Struct (&sBRS_ARR, "#ADR", cpDestStr ) ; /* get date */
  ilRC = setStatusInStruct ("#FIL", C_DISABLE, 0, &sBRS_ARR) ;
 }



 /* MVT Information */
 /* -------- read all information for MVT fields  - MVT TELEXE -  */
 sprintf (clSelection, "MVT") ; 

 /* ------------- read MVT TELEX SWITCH ON ------------ */
 ilRC = iGetConfigEntry(cpConfigFile, clSelection, "MVT_TELEXE_ON", 
                                                CFG_STRING, cpDestStr) ; 
 igMVT_Flag = C_DISABLE ; 
 if (ilRC != RC_SUCCESS) 
 {
  dbg(DEBUG, "WARNING! - cant read MVT/MVT_TELEXE_ON from <%s>", cpConfigFile) ;
  dbg(DEBUG, "SET MVT TELEX SENDING TO DISABLED") ;   
 }
 else    /* bring fields into the struct */  
 {
  ilRC = AddFld2Struct (&sMVT_ARR, "#SON", cpDestStr ) ; /* get date */
  ilRC = setStatusInStruct ("#SON", C_DISABLE, 0, &sMVT_ARR) ;
    if (cpDestStr[0] == '1')
    {
   dbg(DEBUG, "SET MVT TELEX SENDING TO ENABLED") ;  
   igMVT_Flag = C_ENABLE ; 
    }
    else 
    {
   dbg(DEBUG, "SET MVT TELEX SENDING TO DISABLED") ;   
    }
 }

 /* ------------ get my own id ------------------------ */
 ilRC = AddFld2Struct (&sMVT_ARR, "#_ID", "MVT" ) ; /* get own id */
 ilRC = setStatusInStruct ("#_ID", C_DISABLE, 0, &sMVT_ARR) ;

 ilRC = iGetConfigEntry(cpConfigFile, clSelection, "SQL_SELECT_FLDS", 
                                                CFG_STRING, cpDestStr) ; 
 if (ilRC != RC_SUCCESS) 
 {
  dbg(DEBUG, "ERR! - cant read MVT/SQL_SELECT_FLDS from <%s>", cpConfigFile) ;
  ReplaceFldDataInStruct
                        ("MVT CANT FIND SQL_SELECT_FLDS in CFG", "IERR", &sMVT_ARR) ; 
  ilStat = RC_FAIL ; 
 }
 else    /* bring fields into the struct */  
 {
    /*  store the comlete field list in field #MVT */
    ilRC = AddFld2Struct (&sMVT_ARR, "#TLX", cpDestStr) ; /* get SQL Fld. list */
  ilRC = setStatusInStruct ("#TLX", C_DISABLE, 0, &sMVT_ARR) ;
  il = 1 ; 
    do 
    {
   ilLen = get_real_item (cpFoundStr, cpDestStr, il) ;
     if (ilLen > 0)     
     {
        ilRC = AddFld2Struct (&sMVT_ARR, cpFoundStr, "") ; /* get SQL Fld */
    ilRC = setStatusInStruct (cpFoundStr, C_DISABLE, 0, &sMVT_ARR) ;
    ilRC = setStatusInStruct (cpFoundStr, C_ENABLE, C_SELECT, &sMVT_ARR) ;  
     }
     else il = -1 ; 
     il++ ; 
    } while (il > 0) ; 
 }


 /* ------------- read MVT SEND FILEPATH --------- */
 ilRC = iGetConfigEntry(cpConfigFile, clSelection, "MVT_SEND_TLX_PATH", 
                                                CFG_STRING, cpDestStr) ; 
 if (ilRC != RC_SUCCESS) 
 {
  dbg(DEBUG, "ERR! - cant read MVT/MVT_SEND_TLX_PATH from <%s>", cpConfigFile) ;
  ReplaceFldDataInStruct
                        ("MVT CANT FIND MVT_SEND_TLX_PATH in CFG", "IERR", &sMVT_ARR) ; 
  ilStat = RC_FAIL ;    /* need this field */ 
 }
 else    /* bring fields into the struct */  
 {
  ilRC = AddFld2Struct (&sMVT_ARR, "#FIL", cpDestStr ) ; /* get date */
  ilRC = setStatusInStruct ("#FIL", C_DISABLE, 0, &sMVT_ARR) ;
 }


 /* The default text must start with the string CL2SRV_CHRxx  */
 /* where x a raising sequenz from 01 TO 99 be should         */
 il = 1 ;      /* start with CL2SRV_CHR01 */
 while (il > 0) 
 {
    sprintf ( clCfgItem, "CL2SRV_CHR%#02d", il) ; 
    sprintf ( clStrItem, "@C%#02d", il) ;       /* in struct like @001... */ 
  dbg(DEBUG,"Generate Item <%s> for struct item <%s>", clCfgItem, clStrItem) ;
  ilRC = iGetConfigEntry(cpConfigFile, clSelection, clCfgItem, 
                                     CFG_STRING, cpDestStr) ; 
  if (ilRC == RC_SUCCESS)
    {
   /* insert the text item into the struct */
   ilRC = AddFld2Struct (&sMVT_ARR, clStrItem, cpDestStr ) ; 
   ilRC = setStatusInStruct (clStrItem, C_DISABLE, 0, &sMVT_ARR) ;
     il++ ;                /* read next item */
    }
    else il = 0 ;          /* mark the end */
 } /* while not all CL2SRV_SRV ITEMS read */



 ilRC = RC_SUCCESS ;  

 return (ilRC) ; 

} /* GetItemsFromCfgFile() */


 /* read default text fields */





/******************************************************************************/
/* The initialization routine                                                 */
/******************************************************************************/
static int Init_cedadb()
{
    int ilRC = RC_SUCCESS; /* Return code */
    int ili;
    short slSqlCursor;
    short slSqlFunc;
    char pclSqlBuf[512];
    char pclSqlData[128];
    char pclString[512];
    char *pclFunc = "Init_cedadb";

    /* now reading from configfile or from database */
    sprintf( pclSqlBuf, "%s", "SELECT COUNT(*) FROM MCGTAB" );
    ilRC = RunSQL( pclSqlBuf, pclSqlData );
    if( ilRC == DB_SUCCESS )
    {
        igNumMCG = atoi(pclSqlData);
        dbg( TRACE, "%s: #Rec = %d in MCGTAB", pclFunc, igNumMCG );
        rgMCG = (MCGTAB_STRUCT *) malloc( sizeof(MCGTAB_STRUCT) * igNumMCG );
        if( rgMCG == NULL )
        {
            dbg( TRACE,"%s: ERROR IN MALLOC FOR MCGTAB!!!!!!!", pclFunc );
            return RC_FAIL;
        }
        sprintf( pclSqlBuf, "%s", "SELECT TRIM(TANA),TRIM(FINA),TRIM(PREF) FROM MCGTAB" );
        dbg(TRACE,"%s",pclSqlBuf);
        slSqlCursor = 0;
        slSqlFunc = START;
        ilRC = DB_SUCCESS;
        ili = 0;
        while( ilRC == DB_SUCCESS )
        {
            ilRC = sql_if(slSqlFunc, &slSqlCursor, pclSqlBuf, pclSqlData);
            if( ilRC != DB_SUCCESS )
                break;
            slSqlFunc = NEXT;
            get_fld( pclSqlData, FIELD_1, STR, 20, rgMCG[ili].pcTana );
            get_fld( pclSqlData, FIELD_2, STR, 20, rgMCG[ili].pcFina );
            get_fld( pclSqlData, FIELD_3, STR, 4, rgMCG[ili].pcPrefix );

            dbg( TRACE, "%s: MCG[%d] TANA <%s> FINA <%s> PREFIX <%s>", 
                        pclFunc, ili, rgMCG[ili].pcTana, rgMCG[ili].pcFina, rgMCG[ili].pcPrefix );
            ili++;
        }
        close_my_cursor (&slSqlCursor) ;

    }

    igBCHDL = tool_get_q_id( "bchdl" );
    if( igBCHDL <= 0 )
        igBCHDL = 1900;
    dbg( TRACE, "BCHDL modid = %d", igBCHDL );
    igTELEX = tool_get_q_id( "telex" );
    if( igTELEX <= 0 )
        igTELEX = 7298;
    dbg( TRACE, "TELEX modid = %d", igTELEX );
    igSQLHDL = 506;
    dbg( TRACE, "SQLHDL modid = %d", igSQLHDL );

    sprintf( pcgNewline, "%c%c", 0x1D,0x1C );

    ilRC = iGetConfigEntry(cgConfigFile, "MAIN", "DEBUG_LEVEL", CFG_STRING, pclString) ; 
    debug_level = TRACE;
    if( ilRC == RC_SUCCESS )
    {
        if( !strcmp( pclString, "DEBUG" ) )
            debug_level = DEBUG;
        else if( !strcmp( pclString, "OFF" ) )
            debug_level = 0;
    }
    dbg( TRACE, "DEBUG_LEVEL = %d", debug_level );


    (void) InitAutoMvtConfig();

    igInitOK = TRUE ;
    return (ilRC) ;

} /* end of initialize */





/*****************************************************************************/
/* The Reset routine                                                         */
/*****************************************************************************/
static int Reset()
{
 int     ilRC = RC_SUCCESS ;           /* Return code */

 dbg(TRACE,"Reset: now resetting") ;
 return ilRC ;

} /* end of Reset */



/*****************************************************************************/
/* The termination routine                                                   */
/*****************************************************************************/
static void Terminate()
{
 /* unset SIGCHLD ! DB-Child will terminate ! */
 logoff() ;
 dbg (TRACE, "Terminate: now leaving ...") ;

 exit(0) ;

} /* end of Terminate */






/*****************************************************************************/
/* The handle signals routine                                                */
/*****************************************************************************/
static void HandleSignal(int pipSig)
{
 int  ilRC = RC_SUCCESS ;                      /* Return code */

 dbg(TRACE, "HandleSignal: signal <%d> received", pipSig) ;
 switch(pipSig)
 {
  default :
           Terminate();
          break;
 } /* switch */
 exit(0) ;

} /* end of HandleSignal */







/******************************************************************************/
/* The handle general error routine                                           */
/******************************************************************************/
static void HandleErr (int pipErr)
{
 int     ilRC = RC_SUCCESS ;
 return ;
} /* end of HandleErr */







/******************************************************************************/
/* The handle queuing error routine                                           */
/******************************************************************************/
static void HandleQueErr (int pipErr)
{
 int     ilRC = RC_SUCCESS;

 switch (pipErr)
 {
  case    QUE_E_FUNC      :       /* Unknown function */
          dbg (TRACE, "<%d> : unknown function", pipErr) ;
         break ;
  case    QUE_E_MEMORY    :       /* Malloc reports no memory */
          dbg (TRACE, "<%d> : malloc failed", pipErr) ;
         break ;
  case    QUE_E_SEND      :       /* Error using msgsnd */
          dbg (TRACE, "<%d> : msgsnd failed", pipErr) ;
         break ;
  case    QUE_E_GET       :       /* Error using msgrcv */
          dbg (TRACE, "<%d> : msgrcv failed", pipErr) ;
         break ;
  case    QUE_E_EXISTS    :
          dbg (TRACE, "<%d> : route/queue already exists ", pipErr) ;
         break ;
  case    QUE_E_NOFIND    :
          dbg (TRACE, "<%d> : route not found ", pipErr) ;
         break ;
  case    QUE_E_ACKUNEX   :
          dbg (TRACE, "<%d> : unexpected ack received ", pipErr) ;
         break;
  case    QUE_E_STATUS    :
          dbg (TRACE, "<%d> :  unknown queue status ", pipErr) ;
         break;
  case    QUE_E_INACTIVE  :
          dbg (TRACE, "<%d> : queue is inaktive ", pipErr) ;
         break ;
  case    QUE_E_MISACK    :
          dbg (TRACE, "<%d> : missing ack ", pipErr) ;
         break ;
  case    QUE_E_NOQUEUES  :
          dbg (TRACE, "<%d> : queue does not exist", pipErr) ;
         break ;
  case    QUE_E_RESP      :       /* No response on CREATE */
          dbg(TRACE, "<%d> : no response on create", pipErr) ;
         break ;
  case    QUE_E_FULL      :
          dbg (TRACE, "<%d> : too many route destinations", pipErr) ;
         break ;
  case    QUE_E_NOMSG     :       /* No message on queue */
          dbg (TRACE, "<%d> : no messages on queue", pipErr) ;
         break ;
  case    QUE_E_INVORG    :       /* Mod id by que call is 0 */
          dbg (TRACE, "<%d> : invalid originator=0",pipErr);
         break ;
  case    QUE_E_NOINIT    :       /* Queues is not initialized*/
          dbg (TRACE, "<%d> : queues are not initialized", pipErr) ;
         break ;
  case    QUE_E_ITOBIG    :
          dbg (TRACE, "<%d> : requestet itemsize to big ", pipErr) ;
         break ;
  case    QUE_E_BUFSIZ    :
          dbg (TRACE, "<%d> : receive buffer to small ", pipErr) ;
         break ;
 default                 :       /* Unknown queue error */
         dbg (TRACE,"<%d> : unknown error", pipErr) ;
         break;
 } /* end switch */

 return ;
} /* end of HandleQueErr */


/******************************************************************************/
/* The handle queues routine                                                  */
/******************************************************************************/
static void HandleQueues()
{
 int  ilRC = RC_SUCCESS ;                      /* Return code */
 int  ilBreakOut = FALSE ;

 do
 {
  ilRC = que (QUE_GETBIG, 0, mod_id, PRIORITY_3, igItemLen, (char *)&prgItem) ;
  /* depending on the size of the received item  */
  /* a realloc could be made by the que function */
  /* so do never forget to set event pointer !!! */
  prgEvent = (EVENT *) prgItem->text ;
  if( ilRC == RC_SUCCESS )
  {
   /* Acknowledge the item */
   ilRC = que(QUE_ACK,0,mod_id,0,0,NULL) ;
   if( ilRC != RC_SUCCESS )
   {
    /* handle que_ack error */
    HandleQueErr(ilRC);
   } /* fi */

   switch ( prgEvent->command )
   {
    case    HSB_STANDBY     :
            ctrl_sta = prgEvent->command ;
           break ;

    case    HSB_COMING_UP   :
            ctrl_sta = prgEvent->command ;
           break ;

    case    HSB_ACTIVE      :
            ctrl_sta = prgEvent->command ;
            ilBreakOut = TRUE;
           break ;

    case    HSB_ACT_TO_SBY  :
            ctrl_sta = prgEvent->command ;
           break ;

    case    HSB_DOWN        :
            /* whole system shutdown - do not further use que(), send_message() or timsch() ! */
            ctrl_sta = prgEvent->command ;
            Terminate() ;
           break ;

    case    HSB_STANDALONE  :
            ctrl_sta = prgEvent->command;
            ResetDBCounter() ;
            ilBreakOut = TRUE ;
           break ;

    case    REMOTE_DB :
            /* ctrl_sta is checked inside */
            HandleRemoteDB(prgEvent) ;
           break ;

    case    SHUTDOWN        :
            Terminate() ;
           break ;

    case    RESET           :
            ilRC = Reset() ;
           break ;

    case    EVENT_DATA      :
            dbg(TRACE, "HandleQueues: wrong hsb status <%d>", ctrl_sta) ;
            DebugPrintItem (TRACE, prgItem) ;
            DebugPrintEvent (TRACE, prgEvent) ;
           break ;

    case    TRACE_ON :
            dbg_handle_debug(prgEvent->command) ;
           break ;

    case    TRACE_OFF :
            dbg_handle_debug(prgEvent->command) ;
           break ;
    default                 :
            dbg(TRACE, "HandleQueues: unknown event") ;
            DebugPrintItem (TRACE, prgItem) ;
            DebugPrintEvent (TRACE, prgEvent) ;
           break ;
   } /* end switch */
  }
  else
  {                           /* Handle queuing errors */
   HandleQueErr (ilRC) ;
  } /* else */
 } while (ilBreakOut == FALSE) ;

 if(igInitOK == FALSE)
 {
  ilRC = Init_cedadb() ;
  if(ilRC != RC_SUCCESS)
  {
   dbg (TRACE,"Init_cedadb: init failed!") ;
  } /* if */
 }/* if */
                              /* OpenConnection() ; */
} /* end of HandleQueues */



/******************************************************************************/
/* The handle data routine                                                    */
/******************************************************************************/
static int HandleData()
{
    int       ilRC = RC_SUCCESS;          /* Return code */
    BC_HEAD   *prlBchd;
    CMDBLK    *prlCmdblk;
    char      pclActCmd[16];
    char      *pclSelKey;
    char      *pclFields;
    char      *pclData;
    char      cpDateFrom[128] ; 
    char      cpDateTo[128] ; 
    char      clActDate[128] ; 
    char      cpUrno[100];
    char      clFldItem1[1024] ; 
    char      clFldItem2[1024] ;
    int       ilLen ; 
    int       ilStat ; 


    que_out = prgEvent->originator;
    prlBchd    = (BC_HEAD *) ((char *) prgEvent + sizeof(EVENT)) ;
    prlCmdblk  = (CMDBLK *) ((char *)prlBchd->data) ;
    pclSelKey  = (char *) prlCmdblk->data ;
    pclFields  = (char *) pclSelKey + strlen(pclSelKey) + 1 ;
    pclData    = (char *) pclFields + strlen(pclFields) + 1 ;

    /* Save global elements of BC_HEAD */
    /* workstation */
    memset (pcgRecvName, '\0', (sizeof(prlBchd->recv_name) + 1)) ;
    strncpy (pcgRecvName, prlBchd->recv_name, sizeof(prlBchd->recv_name)) ;

    /* User  */
    memset (pcgDestName, '\0', sizeof(prlBchd->recv_name)) ;
    strncpy (pcgDestName, prlBchd->dest_name, sizeof(prlBchd->recv_name)) ;
    sprintf (pcgChkWks, "%s,", pcgRecvName) ;
    sprintf (pcgChkUsr, "%s,", pcgDestName) ;

    /* Save Global info of CMDBLK */
    strcpy (pcgTwStart, prlCmdblk->tw_start) ;
    strcpy (pcgTwEnd, prlCmdblk->tw_end) ;
    strcpy (pclActCmd, prlCmdblk->command) ;

    dbg (TRACE, "CMD <%s> FROM (%d) WKS <%s> USR <%s>", pclActCmd, que_out, pcgRecvName, pcgDestName) ;
    dbg (TRACE, "SPECIAL INFO TWS <%s> TWE <%s>", pcgTwStart, pcgTwEnd ) ;
    dbg (TRACE, "FIELDS <%s>", pclFields) ;
    dbg (TRACE, "DATA   <%s>", pclData) ;
    dbg (TRACE, "SELECT <%s>", pclSelKey) ;

    if (prlBchd->rc == RC_FAIL)
    {
        return (RC_SUCCESS) ;
    }

    ilRC = RC_SUCCESS ; 
    if (strcmp(pclActCmd, "BRS") == 0)
    {
        dbg (DEBUG, "-------------------------------") ;
        dbg (DEBUG, "HandeData: get <BRS> Command   ") ;
        dbg (DEBUG, "-------------------------------") ;
        if (igBRS_Flag == C_ENABLE) 
        {
            dbg (DEBUG, "BRS TELEX MODE IS SWITCHED OFF !!!") ; 
        }
        else 
        {
            if (startWhileTimeReached (&sBRS_ARR, cpDateFrom, cpDateTo) == RC_SUCCESS) 
            {
                ilRC = handleBRS_Call (cpDateFrom, cpDateTo) ;
                if (ilRC == RC_SUCCESS)
                {                     /* set new locate time into BRS_ARR -> Fld. #SDT */
                    ilRC = GetLocalTimeStamp(clActDate) ;
                    ReplaceFldDataInStruct (clActDate, "#SDT", &sBRS_ARR) ; 
                }
            }
        } /* BRS TELEXE WORK... */
    }
    else    /* other commands */ 
    {
        if (strcmp(pclActCmd, "SEND") == 0)
        {
            dbg (DEBUG, "-------------------------------") ;
            dbg (DEBUG, "HandeData: get <MVT> Command   ") ;
            dbg (DEBUG, "-------------------------------") ;
            sprintf(pcgTwEnd,"%s,TAB,TLXGEN",pcgHomeHopo);
            get_real_item (cpUrno, pclData, 1) ;
            ReadTelexAndSend(pclFields, pclData);
        }
        else if (strcmp(pclActCmd, "TLX") == 0)
        {
            if (strcmp(pclSelKey,"CONFIG") == 0)
            {
                /* que_out = prgEvent->originator; */
                SendConfigToClient(que_out, pclSelKey, pclFields, pclData);
            }
            else
            {
                ForwardTelexToFdihdl(pclSelKey, pclData);
            }
        }
        else if (strcmp(pclActCmd, "INF") == 0)
        {
            ilRC = CreateInftabXML(pclSelKey, pclFields, pclData);
        }
        else if (strcmp(pclActCmd, "AUTO") == 0)
        {
          sprintf(pcgTwEnd,"%s,TAB,TLXGEN",pcgHomeHopo);
          ilRC = HandleAutoSendTrigger(pclFields, pclData, 1, "");
        }
        else if (strcmp(pclActCmd, "CHECK") == 0)
        {
            sprintf(pcgTwEnd,"%s,TAB,TLXGEN",pcgHomeHopo);
            CheckTelexAndSend(pclFields, pclData);
        }
        else
        {
            dbg (TRACE, "=== DATA ELEMENTS FROM ORIGINATOR =======");
            dbg (TRACE, "... CMD    <%s>", pclActCmd) ;
            dbg (TRACE, "... FIELDS <%s>", pclFields) ;
            dbg (TRACE, "... DATA   <%s>", pclData) ;
            dbg (TRACE, "... SELECT <%s>", pclSelKey) ;
            dbg (TRACE, "=========================================");

            if( strncmp( pclActCmd, "MVT", 3 ) )  /* Not MVT msg */
            {
                dbg (TRACE, "****UNKNOWN COMMAND RECEIVED****");
                return ilRC;
            }
            if( strlen(pclActCmd) < 5 )
            {
                dbg (DEBUG, "Action gets unknown ") ;
                dbg (TRACE, "****CANT INTERPRET CMD****");
                return ilRC;
            }

            dbg (DEBUG, "---------------------------------") ;
            dbg (DEBUG, "HandeData: get <MVTxx> Command   ") ;
            dbg (DEBUG, "---------------------------------") ;
            /* read the data from the data string */
            ilStat = get_item_no (pclFields, "URNO", 5);  
            if( ilStat < 0 )
            {
                dbg (DEBUG, "URNO NOT FOUND") ; 
                return ilRC;
            }
            get_real_item (cpUrno, pclData, ilStat+1) ;
            dbg(TRACE,"OLD FUNCTION <handleMVT_Call> DISABLED");
            ilRC = handleMVT_Call (cpUrno, pclFields, pclData, pclActCmd) ;
        }
    }
    return ilRC ;
} /* end of HandleData */



static int CreateInftabXML(char *pcpSelKey, char *pcpFields, char *pcpData)
{
  int ilRC = RC_SUCCESS;
  int ilGetRc = RC_SUCCESS;
  char pclGocTbl[32];
  char pclGocCmd[32];
  char pclGocKey[32];
  char pclGocFld[32];
  char pclGocDat[32];
  char pclInfTabFields[128];
  char pclOutMsg[4096*2];
  char pclSqlBuf[128];
  char pclSqlData[4096];
  char pclTmpData[4096];
  char pclTmpItem[4096];
  int ilOutPos = 0;
  int ilLen = 0;
  short sql_cursor = 0;
  short sql_fkt = 0;
  dbg(TRACE,"SELKEY <%s>",pcpSelKey);
  dbg(TRACE,"FIELDS <%s>",pcpFields);
  dbg(TRACE,"DATA   <%s>",pcpData);
  strcpy(pclInfTabFields,"APC3,APPL,FUNK,AREA,LSTU,USEU,TEXT");
  sprintf(pclSqlBuf,"SELECT %s FROM INFTAB WHERE URNO='%s'",pclInfTabFields,pcpData);
  pclSqlData[0] = 0x00;
  sql_cursor = 0 ;
  sql_fkt = START ;
  ilGetRc = sql_if(sql_fkt, &sql_cursor, pclSqlBuf, pclSqlData);
  close_my_cursor (&sql_cursor) ;
  if (ilGetRc == DB_SUCCESS)
  {
    ilRC = BuildItemBuffer(pclSqlData,pclInfTabFields,0,",");
    dbg(TRACE,"%s",pclSqlData);
    ilOutPos = 0;
    ilLen = get_real_item(pclTmpItem, pclSqlData, 1) ;
    sprintf(pclTmpData,"<INFO>\n  <PATH>\n    <ROOT>%s</ROOT>\n",pclTmpItem);
    StrgPutStrg(pclOutMsg, &ilOutPos, pclTmpData, 0, -1, "\0");
    ilLen = get_real_item(pclTmpItem, pclSqlData, 2) ;
    sprintf(pclTmpData,"    <CATEGORY>%s</CATEGORY>\n",pclTmpItem);
    StrgPutStrg(pclOutMsg, &ilOutPos, pclTmpData, 0, -1, "\0");
    ilLen = get_real_item(pclTmpItem, pclSqlData, 3) ;
    sprintf(pclTmpData,"    <PARTITION>%s</PARTITION>\n",pclTmpItem);
    StrgPutStrg(pclOutMsg, &ilOutPos, pclTmpData, 0, -1, "\0");
    ilLen = get_real_item(pclTmpItem, pclSqlData, 4) ;
    sprintf(pclTmpData,"    <SECTION>%s</SECTION>\n",pclTmpItem);
    StrgPutStrg(pclOutMsg, &ilOutPos, pclTmpData, 0, -1, "\0");
    sprintf(pclTmpData,"  </PATH>\n  <MESSAGE>\n");
    StrgPutStrg(pclOutMsg, &ilOutPos, pclTmpData, 0, -1, "\0");
    ilLen = get_real_item(pclTmpItem, pclSqlData, 5) ;
    sprintf(pclTmpData,"    <PUBLISHED>%s</PUBLISHED>\n",pclTmpItem);
    StrgPutStrg(pclOutMsg, &ilOutPos, pclTmpData, 0, -1, "\0");
    ilLen = get_real_item(pclTmpItem, pclSqlData, 6) ;
    sprintf(pclTmpData,"    <PUBLISHER>%s</PUBLISHER>\n",pclTmpItem);
    StrgPutStrg(pclOutMsg, &ilOutPos, pclTmpData, 0, -1, "\0");
    ilLen = get_real_item(pclTmpItem, pclSqlData, 7) ;
    PatchTextServerToClient(pclTmpItem,TRUE);
    if ((strstr(pclTmpItem,"<") != NULL) || (strstr(pclTmpItem,"<") != NULL) ||
        (strstr(pclTmpItem,"&") != NULL))
    {
      sprintf(pclTmpData,"    <FULLTEXT><![CDATA[%s]]></FULLTEXT>\n",pclTmpItem);
    }
    else
    {
      sprintf(pclTmpData,"    <FULLTEXT>%s</FULLTEXT>\n",pclTmpItem);
    }
    StrgPutStrg(pclOutMsg, &ilOutPos, pclTmpData, 0, -1, "\0");
    sprintf(pclTmpData,"  </MESSAGE>\n</INFO>\n");
    StrgPutStrg(pclOutMsg, &ilOutPos, pclTmpData, 0, -1, "\0");
    ilOutPos--;
    pclOutMsg[ilOutPos] = 0x00;
    dbg(TRACE,"\n%s",pclOutMsg);

    strcpy(pclGocTbl,"TBL");
    strcpy(pclGocCmd,"XMLO");
    strcpy(pclGocFld,"FLD");
    strcpy(pclGocFld,"DAT");
    strcpy(pclGocKey,"KEY");
    (void) tools_send_info_flag (igToInfQue, 0, pcgDestName, "", pcgRecvName, "", "",
                                 pcgTwStart, pcgTwEnd, pclGocCmd,
                                 pclGocTbl, pclGocKey, pclGocFld, pclOutMsg, 3);



  }
  return ilRC ;
}


static int handleMVT_Call (char *cpUrno, char *pcpFields, char *pcpData, char *cpAction )
{
    ST_EVT_DATA_HDL  sAFT_RCVOLD;
    ST_EVT_DATA_HDL  sAFT_RCVNEW;
    int   ilNew = 0;
    int   ilOld = 1;
    int   ilRC = RC_SUCCESS;
    int   ili;
    int   ilLen; 
    int   ilCnt;
    int   ilMinDiff;
    int   ilTimeLapse;
    short slSqlCursor;
    short slSqlFunc;
    char  pclTimeBase[16];
    char  pclSqlBuf[2048];
    char  pclSqlWhere[128];
    char  pclSqlData[4096]; 
    char  pclSqlFields[2048]; 
    char  pclDataItem[2][1024]; 
    char  pclFldItem[1024]; 
    char  pclRcvFields[1024]; 
    char  pclRcvData[2][1024];
    char  pclALC[8];
    char  pclFSND[8];
    char  pclFLDD[12];
    char  clADID;
    char  pclTmpStr[512];
    char  pclSubType[4];
    char  *pclP;
    char  *pclFunc = "handleMVT_Call";


    dbg( TRACE, "%s: ======================= START =====================", pclFunc );
    
    memset( pclRcvData, 0, sizeof(pclRcvData) );
    memset( pclRcvFields, 0, sizeof(pclRcvFields) );
    memset( pclDataItem, 0, sizeof(pclDataItem) );
    ResetFldStruct (&sAFT_RCVOLD);
    ResetFldStruct (&sAFT_RCVNEW);

    strcpy( pclRcvData[ilNew], pcpData );
    pclP = (char *)strstr( pclRcvData[ilNew], "\n" );
    if( pclP != NULL )
    {
        *pclP = '\0';
        pclP++;
        strcpy( pclRcvData[ilOld], pclP );
    }

    strcpy( pclRcvFields, pcpFields );

    /* Extract ALC3 */
    ili = get_item_no( pclRcvFields, "FLNO", 5 );
    if( ili < 0 )
    {
        dbg( TRACE, "%s: FLNO not found", pclFunc );
        return RC_FAIL;
    }
    get_real_item ( pclTmpStr, pclRcvData[ilNew], ili+1 );
    strncpy( pclALC, pclTmpStr, 3 );
    pclALC[3] = '\0';
    dbg( DEBUG, "%s: ALC code <%s>", pclFunc, pclALC );

    /* Extract ADID */
    ili = get_item_no( pclRcvFields, "ADID", 5 );
    if( ili < 0 )
    {
        dbg( TRACE, "%s: ADID not found", pclFunc );
        return RC_FAIL;
    }
    get_real_item ( pclTmpStr, pclRcvData[ilNew], ili+1 );
    clADID = pclTmpStr[0];
    dbg( DEBUG, "%s: ADID <%c>", pclFunc, clADID );
    if( clADID != 'A' && clADID != 'D' )
        return RC_FAIL;


    /* Is the airline configured for MVT telex sending */
    sprintf( pclSqlWhere, "ALC3='%s' AND ADID='%c' AND STAT='A'", pclALC, clADID );
    if( pclALC[2] == ' ' )
        sprintf( pclSqlWhere, "ALC2='%2.2s' AND ADID='%c' AND STAT='A'", pclALC, clADID );
    sprintf( pclSqlBuf, "SELECT URNO FROM MTPTAB WHERE %s", pclSqlWhere );
    dbg( TRACE, "%s: MPT SqlBuf <%s>", pclFunc, pclSqlBuf );

    ilRC = RunSQL( pclSqlBuf, pclTmpStr );
    if( ilRC != DB_SUCCESS )
    {
        dbg( TRACE, "%s: Airline <%s> not configured for auto-MVT", pclFunc, pclALC );
        return RC_SUCCESS;
    }

    

    /* Sort out Received fields/data */
    ili = 1;
    ilCnt = get_no_of_items(pclRcvFields);
    for( ili = 1; ili <= ilCnt; ili++ )
    { 
        get_real_item( pclFldItem, pclRcvFields, ili );
        get_real_item( pclDataItem[ilNew], pclRcvData[ilNew], ili );
        AddFld2Struct( &sAFT_RCVNEW, pclFldItem, pclDataItem[ilNew] );
        get_real_item( pclDataItem[ilOld], pclRcvData[ilOld], ili );
        AddFld2Struct( &sAFT_RCVOLD, pclFldItem, pclDataItem[ilOld] );
    }
/* MEI GET_TEMPLATE CONFIG */
    sprintf( pclSqlFields, "%s", "URNO,TYPE,AUTO,MASI,MAPX,FSND,"
                                 "TSND,FLDD,TMED,TMPL,DELA,ALLS,"
                                 "HMST,HMTE,HMEM" );
    sprintf( pclSqlBuf, "SELECT %s FROM MTPTAB WHERE %s", pclSqlFields, pclSqlWhere );
    dbg( TRACE, "%s: SelMPT SqlBuf <%s>", pclFunc, pclSqlBuf );

    slSqlCursor = 0 ;
    slSqlFunc = START ;
    ilRC = DB_SUCCESS;

    /* Search indiviual config item */
    while( (sql_if(slSqlFunc, &slSqlCursor, pclSqlBuf, pclSqlData)) == DB_SUCCESS )
    {
        slSqlFunc = NEXT;

        memset( &sMTPREC, 0, sizeof(ST_EVT_DATA_HDL) );
        ilRC = ResetFldStruct (&sMTPREC) ;

        BuildItemBuffer( pclSqlData, pclSqlFields, 0, "," );
        ilCnt = get_no_of_items( pclSqlFields );
        /* Store DB data */
        for( ili = 1; ili <= ilCnt; ili++ )
        { 
            get_real_item( pclFldItem, pclSqlFields, ili );
            get_real_item( pclDataItem[ilNew], pclSqlData, ili );
            AddFld2Struct( &sMTPREC, pclFldItem, pclDataItem[ilNew] );
        }

        /* Check on FSND field, for field to base on to send telex i.e. AA and AD */
        ilTimeLapse = 0;
        ilRC = getFldDataFromStruct (pclFSND, "FSND", sMTPREC); 
        if( ilRC != RC_SUCCESS || (ilRC == RC_SUCCESS && strlen(pclFSND) <= 0) )
            dbg( TRACE, "%s: FSND is not applicable", pclFunc );
        else
        {
            dbg( TRACE, "%s: FSND <%s>", pclFunc, pclFSND );
            getFldDataFromStruct (pclTmpStr, "TSND", sMTPREC);
            ilTimeLapse = atoi(pclTmpStr);
            dbg( TRACE, "%s: TSND <%d>", pclFunc, ilTimeLapse );

            ilRC = getFldDataFromStruct (pclDataItem[ilNew], pclFSND, sAFT_RCVNEW);
            if( ilRC == RC_SUCCESS )
            {
                getFldDataFromStruct (pclDataItem[ilOld], pclFSND, sAFT_RCVOLD); 
                dbg( TRACE, "%s: Field <%s> Value OLD<%s>-NEW<%s>", pclFunc, pclFSND, pclDataItem[ilOld], pclDataItem[ilNew] );

                if( strcmp( pclDataItem[ilNew], pclDataItem[ilOld] ) )  /* Field values differ */
                {
                    GetServerTimeStamp( "UTC", 1, 0, pclTimeBase );
                    ilRC = TimeDiff( pclDataItem[ilNew], pclTimeBase, &ilMinDiff );
                    if( ilRC == RC_SUCCESS )
                    {
                        if( ilMinDiff >= ilTimeLapse )
                        {
                            dbg( TRACE, "%s: Send telex base on field <%s> TimeDiff<%d> ElapseTime<%d>", 
                                        pclFunc, pclFSND, ilMinDiff, ilTimeLapse );
                            MVTTelexSending( sAFT_RCVNEW, 'S' );
                        }
                        else
                        {
                            dbg( TRACE, "%s: Queue telex base on field <%s> TimeDiff<%d> ElapseTime<%d>", 
                                        pclFunc, pclFSND, ilMinDiff, ilTimeLapse );
                            MVTTelexSending( sAFT_RCVNEW, 'P' );
                        }
                        continue;
                    }
                }
            }
        }

        /* Check on FLDD field, for fields to base on to calculate the difference in timing to send telex i.e. EA and ED */
        ilTimeLapse = 0;
        getFldDataFromStruct (pclFLDD, "FLDD", sMTPREC) ; 
        if( strlen(pclFLDD) != 8 )
        {
            dbg( TRACE, "%s: FLDD is not applicable", pclFunc );
            continue;
        }
        dbg( TRACE, "%s: FLDD <%s>", pclFunc, pclFLDD );
        getFldDataFromStruct (pclTmpStr, "TMED", sMTPREC);
        ilTimeLapse = atoi(pclTmpStr);
        dbg( TRACE, "%s: TMED <%d>", pclFunc, ilTimeLapse );

        memcpy( pclFldItem, pclFLDD, 4 );
        dbg( TRACE, "%s: FIELD 1 <%s>", pclFunc, pclFldItem );
        
        ilRC = getFldDataFromStruct (pclDataItem[ilNew], pclFldItem, sAFT_RCVNEW); 
        if( ilRC != RC_SUCCESS )
            continue;
        
        ilRC = getFldDataFromStruct (pclDataItem[ilOld], pclFldItem, sAFT_RCVOLD);
        if( ilRC != RC_SUCCESS )
            continue;
        
        dbg( TRACE, "%s: Field <%s> Value OLD<%s>-NEW<%s>", pclFunc, pclFldItem, pclDataItem[ilOld], pclDataItem[ilNew] );
        strcpy( pclTimeBase, pclDataItem[ilNew] );
        memcpy( pclFldItem, &pclFLDD[4], 4 );
        dbg( TRACE, "%s: FIELD 2 <%s>", pclFunc, pclFldItem );
        if( !strcmp( pclDataItem[ilNew], pclDataItem[ilOld] ) )  /* Field values same */
        {
            ilRC = getFldDataFromStruct (pclDataItem[ilNew], pclFldItem, sAFT_RCVNEW) ; 
            if( ilRC != RC_SUCCESS )
                continue;
            ilRC = getFldDataFromStruct (pclDataItem[ilOld], pclFldItem, sAFT_RCVOLD) ; 
            if( ilRC != RC_SUCCESS )
                continue;
            if( !strcmp( pclDataItem[ilNew], pclDataItem[ilOld] ) )  /* Field values same again */
                continue;
        }   
        else
        {
            ilRC = getFldDataFromStruct (pclDataItem[ilNew], pclFldItem, sAFT_RCVNEW) ; 
            if( ilRC != RC_SUCCESS )
                continue;
        }

        /* One of the values change */
        ilRC = TimeDiff( pclDataItem[ilNew], pclTimeBase, &ilMinDiff );
        if( ilRC == RC_SUCCESS )
            dbg( TRACE, "%s: Time diff of <%d>[limit=<%d>] for <%s>", 
                        pclFunc, ilMinDiff, ilTimeLapse, pclFLDD );
        if( ilRC == RC_SUCCESS && abs(ilMinDiff) >= ilTimeLapse )
            MVTTelexSending( sAFT_RCVNEW, 'S' );

    } /* while loop */
    close_my_cursor (&slSqlCursor) ;
    dbg( TRACE, "%s: ======================= END =====================", pclFunc );
    return (ilRC) ;

} /* handleMVT_Call () */



static int ReadTelexAndSend(char *pcpFields, char *pcpData)
{
    int   ilRC = RC_SUCCESS;
    int   ilLen; 
    int   ilGetRc;
    int   ilToPid;
    int   ilChkMVT;
    long  llLen = 0;
    short slSqlCursor;
    short slSqlFunc;
    char  pclUrno[12];
    char  pclSere[12];
    char  pclStat[12];
    char  pclStartTime[20];
    char  pclEndTime[20];
    char  pclSqlFld[2048];
    char  pclSqlBuf[2048];
    char  pclSqlData[8096]; 
    char  pclTlxText[8096]; 
    char  pclDataItem[4096]; 
    char  pclSelection[512];
    char  pclTlxMail[8096]; 
    char pclCleanLook[4] = "";
    char pclCleanPaste[4] = "";
    char pclFunc[] = {"ReadTelexAndSend"};
    char  pclNowTime[20] = "";
    char  pclTlxSubject[128] = "";
    char  pclMailFile[128] = "";
    char  pclFullName[128] = "";
    char  pclSmtpScript[1024] = "";
    char  pclSystemCall[1024] = "";
    char  pclQuote[4] = "X";
    char *pclPtr = NULL;
    FILE  *tmpFile ;
    pclQuote[0] = 34;

    ilToPid = igTELEX;
    ilChkMVT = FALSE;
    dbg(TRACE,"FIELDS <%s>",pcpFields);
    dbg(TRACE,"DATA   <%s>",pcpData);
    ilGetRc = GetFieldValue(pclUrno, pcpFields, "URNO", pcpData);
    ilGetRc = GetFieldValue(pclSere, pcpFields, "SERE", pcpData);
    ilGetRc = GetFieldValue(pclStat, pcpFields, "STAT", pcpData);
    if ((strcmp(pclSere,"S") == 0) && (strcmp(pclStat,"S") == 0))
    {
      strcpy(pclSqlFld,"TXT1,TXT2,URNO");  
      sprintf(pclSqlBuf,"WHERE URNO=%s", pclUrno);  
      ilGetRc = HandleSqlSelectRows("TLXTAB", pclSqlBuf, pclSqlFld, pclSqlData, 1);
      dbg(TRACE,"SQL FIELDS <%s>", pclSqlFld);
      dbg(TRACE,"SQL DATA   <%s>", pclSqlData);
      /* TXT1 */
      ilLen = get_real_item (pclDataItem, pclSqlData, 1);
      strcpy(pclTlxText, pclDataItem);
      /* TXT2 */
      ilLen = get_real_item (pclDataItem, pclSqlData, 2);
      strcat(pclTlxText, pclDataItem);

      PatchTextServerToClient(pclTlxText,FALSE);
      sprintf(pclCleanLook, "%c%c", 0x0D, 0x0D);
      sprintf(pclCleanPaste, "%c", 0x0D);
      (void) CleanDoubleBytes(pclTlxText, pclCleanLook, pclCleanPaste);
      dbg(TRACE,"TELEX TEXT CLEAN\n<%s>", pclTlxText);

      /* Here now we'll strip off the eMail addresses */
      llLen = -1;
      pclPtr = strstr(pclTlxText,"<EMAIL_ADDR>");
      if (pclPtr != NULL)
      {
        dbg(TRACE,"TELEX CONTAINS ATTACHED EMAIL ADDRESSES");
        (void) CedaGetKeyItem(pclTlxMail, &llLen, pclTlxText, "<EMAIL_ADDR>", "</EMAIL_ADDR>", TRUE);
        if (pclTlxText[2] == 0x01)
        {
          /* SATS TPF NODE FORMAT */
          /*  TelexText = Chr(13) & Chr(10) & Chr(1) & Trim(txtPreview(idx).Text) & Chr(13) & Chr(10) & Chr(3) */
          pclPtr[0] = 0x03;
          pclPtr[1] = 0x00;
        }
        else
        {
          pclPtr[0] = 0x00;
        }
        dbg(TRACE,"TELEX TEXT:\n<%s>",pclTlxText);
        dbg(TRACE,"EMAIL ADDR:\n<%s>",pclTlxMail);
      }
   
      if (igToTlxQue > 1000)
      { 
        sprintf(pcgTwEnd,"%s,TAB,TLXGEN",pcgHomeHopo);
        ilRC = SendCedaEvent(igToTlxQue,0,mod_name,"",pclUrno,pcgTwEnd,
                      "TLX", "TLXTAB", pclUrno,"",pclTlxText, "", 4, RC_SUCCESS); 
        dbg(TRACE,"TELEX PUT ONTO  QUEUE %d",igToTlxQue);
        if (ilRC != RC_SUCCESS)
        {
          dbg(TRACE,"TELEX NOT PUT ONTO FAILING QUEUE %d",igToTlxQue);
          igToTlxQue = -igToTlxQue;
        }
      }
      else
      {
        dbg(TRACE,"TELEX NOT PUT ONTO INVALID QUEUE %d",igToTlxQue);
      }
      sprintf( pclSelection, "WHERE URNO=%s", pclUrno );
      ilRC = SendCedaEvent(1200,0,mod_name,"",pclUrno,pcgTwEnd,
                               "URT", "TLXTAB", pclSelection, "STAT", "Q", "", 3, NETOUT_NO_ACK); 
      dbg(TRACE,"TELEX STATUS UPDATED TO <Q>");

      if (pclPtr != NULL)
      {
        /* Here now we can invoke the Telex Email function           */
        /* Addresses in: pclTlxMail as list separated by CRLF (\r\n) */
        /* Telex Text in: pclTlxText as plain ASCII text             */
        /* Originator in: pcgSenderEmailAddr                         */
        dbg(TRACE,"GONNA SEND THE EMAIL NOW ...");
        if (igEmailIsReadyToUse == TRUE)
        {
          /* ... call the SEND_EMAIL function ... */
          if (pclTlxText[2] == 0x01)
          {
            /* SATS TPF NODE FORMAT */
            /*  TelexText = Chr(13) & Chr(10) & Chr(1) & Trim(txtPreview(idx).Text) & Chr(13) & Chr(10) & Chr(3) */
            pclPtr[0] = 0x00;
            pclPtr = &pclTlxText[2];
            pclPtr++;
          }
          else
          {
            pclPtr = &pclTlxText[2];
          }
          /* JIRA.ISSUE.UFIS-95:
          AutoMVT: Sending Telex using Email
          There is a shell script named: send_telex_email.sh for sending emails.
          Pre-requirement: SMTP setup working correctly on the server system.
          Script requires minimum of 4 parameters as described below
          (does not care about more than 4):
          send_telex_email.sh <reply address> <subject> <addressees> <file with mail body>
          Reply Address: Reply address is in the form of xxxxxx@xxxxxxxx.com.
          Subject: A free text as decided by the user.
          (NOTE: No special characters have been tested in the subject)
          Addressees: List of addresses in the comma separated format.
          File with mail body: The body of the message have to be saved 
          in a plain text file first before sending 
          (Yet to be decided what to do with the file once the message have been sent,
          could be deleted if requested). It is also assumed that the file can be found 
          in the directory from which the script execution takes place; 
          however full path of the file have to be specified if the location of the file 
          is not as specified before.
          */

          /* 1.) Create comma separated address list */
          /* Replace CRLF by Comma */
          ReplaceStringInString(pclTlxMail, "\r\n", ",");
          /* Remove all spaces */
          ReplaceStringInString(pclTlxMail, " ", "");
          ReplaceStringInString(pclTlxMail, ",,", ",");
          while (pclTlxMail[0] == ',')
          {
            pclTlxMail[0] = ' ';
            ReplaceStringInString(pclTlxMail, " ", "");
          }
          ilLen = strlen(pclTlxMail);
          while (ilLen > 0) 
          {
            ilLen--;
            if (pclTlxMail[ilLen] == ',')
            {
              pclTlxMail[ilLen] = '\0';
            }
            else
            {
              ilLen = -1;
            }
          }

          strcpy(pclTlxSubject,"TELEX");

          dbg(TRACE,"EMAIL ORIG: <%s>",pcgSenderEmailAddr);
          dbg(TRACE,"EMAIL ADDR:\n<%s>",pclTlxMail);
          ReplaceStringInString(pclPtr, "\r", "");
          dbg(TRACE,"EMAIL TEXT:\n<%s>",pclPtr);

          /* mksingh82@gmail.com */

          /* 2.) Create eMail Text File */
          /* We'll use a TimeStamp plus Tlxtab.Urno as name */
          GetServerTimeStamp("UTC", 1, 0, pclNowTime);
          sprintf(pclMailFile,"MAIL%sTLX%s",pclNowTime,pclUrno);
          sprintf(pclFullName,"%s/%s",pcgSenderEmailPath,pclMailFile);
          tmpFile = fopen (pclFullName, "w"); 
          if (tmpFile != NULL) 
          {
            fprintf(tmpFile, pclPtr); 
            fflush(tmpFile); 
            fclose(tmpFile);

            /* Note: Subject must be one word (no spaces) */
            sprintf(pclTlxSubject,"MAIL:%s-TLX:%s",pclNowTime,pclUrno);
            /* send_telex_email.sh <reply address> <subject> <addressees> <file with mail body> */
            strcpy(pclSmtpScript,"UfisEmail");

            sprintf(pclSystemCall, "%s %s %s %s %s",
                                    pclSmtpScript,
                                    pcgSenderEmailAddr,
                                    pclTlxSubject,
                                    pclTlxMail,
                                    pclFullName); 

            dbg(TRACE, "SYSTEM CALL <%s>", pclSystemCall); 
            system(pclSystemCall);
          }
          else 
          {
            dbg(TRACE, "CREATE EMAIL FILE ERROR !!!") ; 
            dbg(TRACE, "CANNOT CREATE <%s>", pclFullName) ; 
          }
        }
        else
        {
          dbg(TRACE,"EMAIL FUNCTION NOT ACTIVATED");
        }
      }
      else
      {
        dbg(TRACE,"OOOPS: THE POINTER IS SUDDENLY NULL");
      }
    }
    else
    {
      dbg(TRACE,"COMBINATION OF SERE(%s) AND STAT(%s) NOT TO BE SENT",pclSere,pclStat);
    }
  return ilRC;
} /* ReadTelexAndSend ( ) */

static int CheckTelexAndSend(char *pcpFields, char *pcpData)
{
  int ilRc = RC_SUCCESS;
  int ilGetRc = DB_SUCCESS;
  int ilGetRow = 0;
  char pclBgnTime[32] = "";
  char pclEndTime[32] = "";
  char pclSqlBuf[256] = "";
  char pclSqlFld[128] = "URNO,FLNU,STYP,TKEY";
  char pclSqlDat[128] = "";
  char pclTlxType[16] = "";
  char pclGetType[16] = "";
  char pclTlxTkey[32] = "";
  char pclAftUrno[16] = "";
  char pclAftFld[256] = "URNO,FTYP,ADID,FLNO,ALC2,ALC3,OFBL,AIRB,ETAI,ETDI,STOA,STOD,LAND,ONBL";
  char pclAftKey[256] = "";
  char pclTlxUrno[16] = "";
  char pclTlxKey[256] = "";
  char pclAftRec[1024] = "";
  short slSqlCursor = 0;
  short slSqlFunc = 0;
  /* Note: pcpFields and pcpData will be empty */
  /* because the trigger comes from ntisch.    */

  dbg(TRACE,"===================================================");
  dbg(TRACE,">>> CHECK PENDING MVT TELEXES === MAIN FUNCTION ===");
  dbg(TRACE,"---------------------------------------------------");
  GetServerTimeStamp("UTC", 1, 0, pclEndTime);
  GetServerTimeStamp("UTC", 1, -(igPendingSelect*60), pclBgnTime);
  sprintf(pclSqlBuf,"SELECT %s FROM TLXTAB WHERE (TKEY BETWEEN '%s' AND '%s') AND SERE='P' AND STAT='C'",
                     pclSqlFld, pclBgnTime, pclEndTime);
  dbg(TRACE,"\n%s",pclSqlBuf);
  slSqlCursor = 0;
  slSqlFunc = START;
  ilGetRc = DB_SUCCESS;
  while( ilGetRc == DB_SUCCESS )
  {
    pclSqlDat[0] = 0x00;
    ilGetRc = sql_if(slSqlFunc, &slSqlCursor, pclSqlBuf, pclSqlDat);
    if(ilGetRc == DB_SUCCESS)
    {
      BuildItemBuffer(pclSqlDat,pclSqlFld,0,",") ;
      dbg(TRACE,"FOUND A PENDING TELEX: <%s>",pclSqlDat);
      dbg(TRACE,"---------------------------------------------------");
      get_real_item (pclTlxUrno, pclSqlDat, 1);
      get_real_item (pclAftUrno, pclSqlDat, 2);
      get_real_item (pclTlxType, pclSqlDat, 3);
      get_real_item (pclTlxTkey, pclSqlDat, 4);
      sprintf(pclTlxKey, "WHERE URNO=%s", pclTlxUrno);
      sprintf(pcgTwEnd,"%s,TAB,TLXGEN",pcgHomeHopo);
      ilRc = SendCedaEvent(1200,0,mod_name,"",pclTlxUrno,pcgTwEnd,
                               "DRT", "TLXTAB", pclTlxKey, "URNO", pclTlxUrno, "", 3, NETOUT_NO_ACK); 
      dbg(TRACE,"PENDING TELEX RECORD DELETED (SENT 'DRT' TO ROUTER)");
      dbg(TRACE,"---------------------------------------------------");
      sprintf(pclAftKey,"WHERE URNO=%s",pclAftUrno);
      ilGetRow = HandleSqlSelectRows("AFTTAB", pclAftKey, pclAftFld, pclAftRec, 1);
      if (ilGetRow > 0)
      {
        sprintf(pclGetType,"'%s'",pclTlxType);
        HandleAutoSendTrigger(pclAftFld, pclAftRec, 2, pclGetType);
        dbg(TRACE,"---------------------------------------------------");
        dbg(TRACE,"SEARCH NEXT PENDING TELEX");
      }
      slSqlFunc = NEXT;
    }
  }
  close_my_cursor (&slSqlCursor) ;
  dbg(TRACE,"---------------------------------------------------");
  ilRc = RC_SUCCESS;
  return ilRc;
}

static void PatchTextServerToClient(char *pcpText, int ipUseOld)
{
    char pclOldChr[] = "-80,-79,178,-77,180,155,156,149,150,-65";
    char pclSvrChr[] = "23,24,25,28,29,40,41";
    char pclCliChr[] = "34,39,44,10,13,40,41,91,93,58";
    char pclVal[4];
    int ili = 0;
    int ilLen = 0;
    int ilItm = 0;
    int ilMax = 0;
    unsigned int ilSvr = 0;
    int ilCli = 0;

    ilLen = strlen(pcpText);
    if (ipUseOld == FALSE)
    {
        ilMax = field_count(pclSvrChr);
    }
    else
    {
        ilMax = field_count(pclOldChr);
    }  
    for (ilItm=1; ilItm<=ilMax; ilItm++)
    {
        if (ipUseOld == FALSE)
        {
            get_real_item (pclVal, pclSvrChr, ilItm);
        }
        else
        {
            get_real_item (pclVal, pclOldChr, ilItm);
        }
        ilSvr = atoi(pclVal);
        get_real_item (pclVal, pclCliChr, ilItm);
        ilCli = atoi(pclVal);
        for (ili=0; ili<ilLen;ili++)
        {
            /* dbg(TRACE,"CHAR<%c> ASC(%d) SVR(%d)",pcpText[ili],pcpText[ili],ilSvr); */
            if (pcpText[ili] == ilSvr)
            {
                pcpText[ili] = ilCli;
            }
        }
    }
    return;
}


static void PatchTextClientToServer(char *pcpText)
{
  char pclSvrChr[] = "23,24,25,28,29";
  char pclCliChr[] = "34,39,44,10,13";
  char pclVal[4];
  int ili = 0;
  int ilLen = 0;
  int ilItm = 0;
  int ilMax = 0;
  int ilSvr = 0;
  int ilCli = 0;
  ilLen = strlen(pcpText);
  ilMax = field_count(pclSvrChr);
  for (ilItm=1; ilItm<=ilMax; ilItm++)
  {
    get_real_item (pclVal, pclSvrChr, ilItm);
    ilSvr = atoi(pclVal);
    get_real_item (pclVal, pclCliChr, ilItm);
    ilCli = atoi(pclVal);
    for (ili=0; ili<ilLen;ili++)
    {
      if (pcpText[ili] == ilCli)
      {
        pcpText[ili] = ilSvr;
      }
    }
  }
  return;
}
static int generateMTVTelex (char *clFldLst, char *clData)
{
  int   ilRC = RC_SUCCESS ; 
  char  cpWorkFilename[256] ; 
  char  cpFilename[128] ; 
  char  cpPath[128] ; 
  char  cpTmpFilename[128] ;
    FILE  *tmpFile ;
  char  clDataStr[2096] ;
  char  clMonthStr[256] ; 
  char  clHlpStr[256] ;
    char  clFoundField[64] ; 
  int   ilPtr ; 
  char  cpField[64] ; 
  int   isLen ; 
  int   iChgChrPtr ;
  char  clFldStr[20] ;
    char  clChrStr[50] ; 
  char  cClientChr ;  
  char  cSrvChr ;  
  int   ili ; 
  int   ilLen ; 
  char  cpSystemCall[1024] ; 

    dbg (DEBUG, "generateMTVTelex >>") ; 
    ilRC = getNextBRSFilename (cpFilename ) ; /* also used for MVT !!! */ 
  ilRC = getFldDataFromStruct (cpPath, "#FIL", sMVT_ARR) ;  
  sprintf (cpWorkFilename, "%s%s.snd", cpPath, cpFilename) ; 
    sprintf (cpTmpFilename, "%s%s.tmp", cpPath, cpFilename) ; 
  dbg (DEBUG, "createMVTTelex: filename new is (%s)", cpWorkFilename ) ; 

  tmpFile = fopen (cpTmpFilename, "w") ; 
  isLen = strlen (clData) ; 
  iChgChrPtr = 1 ;  /* change all defined chars in created list */

  do 
    {
   cClientChr = 0 ;  
   cSrvChr = 0 ;  
   sprintf (clFldStr, "@C%#02d", iChgChrPtr) ;  
   ilRC = getFldDataFromStruct (clChrStr, clFldStr, sMVT_ARR) ;  
   if (ilRC == RC_SUCCESS)
     {
    /* get the new chars */
    ilLen = get_real_item (clDataStr, clChrStr, 1) ;
    cClientChr = atoi(clDataStr) ;  
    ilLen = get_real_item (clDataStr, clChrStr, 2) ;
    cSrvChr = atoi(clDataStr) ;  
    /*
    if ((cSrvChr > 0) && (cSrvChr <= 254) && (cSrvChr != 26) &&
                (cClientChr > 0) && (cClientChr <= 254) && (cClientChr != 26))
    */
    if ((cSrvChr != 26) && (cClientChr != 26))
        {
      ili = 0 ; 
            while (ili < isLen) 
      { 
             if (clData[ili] == cClientChr) clData[ili] = cSrvChr ; 
       ili++ ; 
      }
        }
     }
   iChgChrPtr++ ; 
    } while ((cClientChr == 0x00) && (cSrvChr == 0x00)) ; 
  ilRC = RC_SUCCESS ; 

  /* write this to the file and close file */

  if (tmpFile != NULL) 
    {
   dbg (DEBUG, "createMVTTelex FILE writing <%s>",cpTmpFilename) ; 
   /* HEADER INFO for the BRS TELEX */
   fprintf (tmpFile, clData) ; 
     fflush (tmpFile) ; 
     fclose (tmpFile) ;
     sprintf (cpSystemCall, "mv %s %s", cpTmpFilename, cpWorkFilename ) ; 
     system (cpSystemCall) ;
     dbg (DEBUG, "system call = <%s>", cpSystemCall) ; 
  }
  else 
    {
   dbg (DEBUG, "createMVTTelex FILE writing ERROR !!!") ; 
   dbg (DEBUG, "Cant create MVT filename <%s>", cpTmpFilename) ; 
     ilRC = RC_FAIL ; 
  } 

  return ilRC ; } /* generateMTVTelex () */




/*-----------------------------------------------------------------------*/
/* function :  readMVTTab ()                                             */
/*             read all importamnt information (send adress,....)        */
/*             from the MVT tab                                          */
/* pars.: none (gets information over MVTARR (act AFT record) and MV1TAB */
/*                                                                  GHe  */
/*-----------------------------------------------------------------------*/
static int readMVTTab (void) 
{
 int   ilRC = RC_SUCCESS ;
 int   il ;
 int   ilLen ; 
 int   ilGetRc ;
 int   ilMoreRecs ; 
 short sql_cursor ;
 short sql_fkt ;
 char  clSqlStatement[2048] ;
 char  clCondFld[2048] ; 
 char  clCondData[2048] ; 
 char  clSelFlds[2048] ; 
 char  clData [4096] ; 
 char  clDataItem [1024] ; 
 char  clFldItem[1024] ;
 char  clVialStr[2096] ; 
 char  clAlc3Str[1024] ; 
 char  clOrg3Str[1024] ;
 char  clDes3Str[1024] ; 
 char  clAdressPayd[2096] ; 
 char  clDelimiter[2] ; 

 clDelimiter[0] = 0x019 ;    /* delimiter for clAdressPayd field */ 
 clDelimiter[1] = 0x000 ; 

 /* test if the date and time correct, for sending telexs */
 clAdressPayd[0] = 0x00 ; 
 clSelFlds[0]  = 0x00 ;    /* create sql field list */ 
 clCondData[0] = 0x00 ;
 ilRC=createFldAndDataLst(clSelFlds, clCondData, C_SELECT, sMV1_ARR) ;

 /* from AFTTAB we need the information for Dest, OR3, VIAL ACT3 */
 ilRC = getFldDataFromStruct (clAlc3Str, "ALC3", sMVT_ARR) ; 
 ilRC = getFldDataFromStruct (clVialStr, "VIAL", sMVT_ARR) ; 
 ilRC = getFldDataFromStruct (clOrg3Str, "ORG3", sMVT_ARR) ; 
 ilRC = getFldDataFromStruct (clDes3Str, "DES3", sMVT_ARR) ; 

 if (clAlc3Str[0] == 0x00) strcpy (clAlc3Str, "=--") ; /* this never exist */ 
 if (clVialStr[0] == 0x00) strcpy (clVialStr, "===") ; /* this never exist */ 
 if (clOrg3Str[0] == 0x00) strcpy (clOrg3Str, "==-") ; /* -.- */ 
 if (clDes3Str[0] == 0x00) strcpy (clDes3Str, "-=-") ; /* -.- */ 
 sprintf (clSqlStatement,
 "SELECT %s FROM MVTTAB WHERE ALC3 = '%s' AND (APC3 = '%s' OR APC3 = '%s' OR APC3 = '%s')" ,
                clSelFlds, clAlc3Str, clVialStr, clOrg3Str, clDes3Str) ; 
 dbg (DEBUG, "MVT TAB: SQL SELECT STATEMENT = <%s>", clSqlStatement) ;
 sprintf (clData, clCondData) ;        /* prepare data pool */
 sql_cursor = 0 ;
 sql_fkt = START ;
 do 
 {
    ilGetRc = sql_if (sql_fkt, &sql_cursor, clSqlStatement, clData) ;
  if (ilGetRc == DB_SUCCESS)
  {
   ilRC = BuildItemBuffer(clData,clSelFlds,0,",") ;
     ilMoreRecs++ ; 
   /* telex record */ 
   dbg (DEBUG, "SQLSelectRecord: find rec=<%ld> data = <%s>",
                            ilMoreRecs, clData) ;
   il = 1 ; 
   do                               /* add to the adress list */ 
     {
    ilLen = get_real_item (clFldItem, clSelFlds, il) ;
      if (ilLen > 0)     
      {
     ilLen = get_real_item (clDataItem, clData, il) ;
       ilRC = ReplaceFldDataInStruct (clDataItem, clFldItem, &sMV1_ARR) ;
      }
      else il = -1 ; 
      il++ ; 
   } while (il > 0) ; 
   /* add the adress and paid field in the list  */ 
   /* like in form of ...,TEAD,PAYE,TEAD,PAYE,.. */
   if(clAdressPayd[0] != 0x00) strcat (clAdressPayd, clDelimiter) ; 
   ilRC = getFldDataFromStruct (clDataItem, "TEAD", sMV1_ARR) ; 
   strcat (clAdressPayd, clDataItem) ; 
   if(clAdressPayd[0] != 0x00) strcat (clAdressPayd, clDelimiter) ; 
   ilRC = getFldDataFromStruct (clDataItem, "PAYE", sMV1_ARR) ; 
   strcat (clAdressPayd, clDataItem) ; 
  }
  else
  {
   dbg (DEBUG, "SQLSelectRecord: All records for the time range read") ; 
     ilRC = ReplaceFldDataInStruct (clAdressPayd, "TEAD", &sMV1_ARR) ;
   ilRC = RC_FAIL ; 
    }
  sql_fkt = NEXT ;
 } while (ilGetRc == DB_SUCCESS) ; 

 close_my_cursor (&sql_cursor) ;
 if (clAdressPayd[0] != 0x00) ilRC = RC_SUCCESS ; 

 return (ilRC) ;

} /* readMVTTab () */







/*-----------------------------------------------------------------------*/
/* function :  setTlxTableDat ()                                         */
/*             use the AFT Fields to get the information for the TLXTAB  */
/*             fields, which must be set                                 */
/*  pars.:                                                               */
/*          ST_EVT_DATA_HDL sTLX_ARR : the array with telex datas        */
/*          ST_EVT_DATA_HDL sSND_ARR : the array with act telex generated*/
/*                                     datas from AFTTAB                 */
/*                                                                  GHe  */
/*-----------------------------------------------------------------------*/
static int setTlxTableDat (ST_EVT_DATA_HDL *sTLX_ARR, 
                           ST_EVT_DATA_HDL *sSND_ARR ) 
{
    int ilNextUrno; 
    int  ilRC = RC_SUCCESS;
    char clFoundData[4096]; 


    /* check if the field ALC3 exists in TLX */
    if (DoesFldExistInStruct ("ALC3", *sTLX_ARR) == RC_SUCCESS) 
    {
        clFoundData[0] = 0x00 ; 
        getFldDataFromStruct (clFoundData, "ALC3", *sSND_ARR) ; /* read from AFT */ 
        ReplaceFldDataInStruct (clFoundData, "ALC3", sTLX_ARR) ;  
    }

    /* BEME Bemerkungen (in the moment no information) */
     clFoundData[0] = 0x00 ; 

    /* check if the field CDAT exists in TLX */
  if (DoesFldExistInStruct ("CDAT", *sTLX_ARR) == RC_SUCCESS) 
    {
   ilRC = GetLocalTimeStamp(clFoundData) ;
   ReplaceFldDataInStruct (clFoundData, "CDAT", sTLX_ARR) ;  
    }

    /* check if the field FKEY exists in TLX */
  if (DoesFldExistInStruct ("FKEY", *sTLX_ARR) == RC_SUCCESS) 
    {
     clFoundData[0] = 0x00 ; 
   getFldDataFromStruct (clFoundData, "FKEY", *sSND_ARR) ; /* read from AFT */ 
   ReplaceFldDataInStruct (clFoundData, "FKEY", sTLX_ARR) ;  
    }


    /* check if the field FLNS exists in TLX */
  if (DoesFldExistInStruct ("FLNS", *sTLX_ARR) == RC_SUCCESS) 
    {
     clFoundData[0] = 0x00 ; 
   getFldDataFromStruct (clFoundData, "FLNS", *sSND_ARR) ; /* read from AFT */ 
   ReplaceFldDataInStruct (clFoundData, "FLNS", sTLX_ARR) ;  
    }

    /* check if the field FLNU exists in TLX */
  if (DoesFldExistInStruct ("FLNU", *sTLX_ARR) == RC_SUCCESS) 
    {
     /* this field gets the URNO from AFT TAB */
     clFoundData[0] = 0x00 ; 
   getFldDataFromStruct (clFoundData, "URNO", *sSND_ARR) ; /* read from AFT */ 
   ReplaceFldDataInStruct (clFoundData, "FLNU", sTLX_ARR) ;  
    }

    /* check if the field FLTN exists in TLX */
  if (DoesFldExistInStruct ("FLTN", *sTLX_ARR) == RC_SUCCESS) 
    {
     clFoundData[0] = 0x00 ; 
   getFldDataFromStruct (clFoundData, "FLTN", *sSND_ARR) ; /* read from AFT */ 
   ReplaceFldDataInStruct (clFoundData, "FLTN", sTLX_ARR) ;  
    }

  if (DoesFldExistInStruct ("HOPO", *sTLX_ARR) == RC_SUCCESS) 
    {
   /* read from SYS */ 
   ReplaceFldDataInStruct (pcgHomeHopo, "HOPO", sTLX_ARR) ;  
    }

    /* the fields LSTU, MORE, MSNO, MSTX, PRFL are stored on other pos. */

    /* check if the field SERE exists in TLX */
    /* send (S) and receive (R) and create (C) marker */
  if (DoesFldExistInStruct ("SERE", *sTLX_ARR) == RC_SUCCESS) 
    {
     strcpy (clFoundData, "C") ;    
   ReplaceFldDataInStruct (clFoundData, "SERE", sTLX_ARR) ;  
    }

    /* check if the field STAT exists in TLX */
    /* send (S) and receive (R) and create (C) marker */
  if (DoesFldExistInStruct ("STAT", *sTLX_ARR) == RC_SUCCESS) 
    {
     strcpy (clFoundData, "C") ;    
   ReplaceFldDataInStruct (clFoundData, "STAT", sTLX_ARR) ;  
    }


    /* check if the field TIME exists in TLX */
  if (DoesFldExistInStruct ("TIME", *sTLX_ARR) == RC_SUCCESS) 
    {         /* copy from CDAT */
     clFoundData[0] = 0x00 ; 
   getFldDataFromStruct (clFoundData, "CDAT", *sTLX_ARR) ; /* read from AFT */ 
   ReplaceFldDataInStruct (clFoundData, "TIME", sTLX_ARR) ;  
    }

  /* TKEY ???   */

    /* TTYP  >> AAD/MVT status      */


    /* TXNO ???  */


    /* TXT1 is the complete information of the AFT tab in the */
    /* field sequenz from BRS,MVT,...                         */
  if (DoesFldExistInStruct ("TXT1", *sTLX_ARR) == RC_SUCCESS) 
    {      
     clFoundData[0] = 0x00 ; 
   getTextField (*sSND_ARR, clFoundData) ; 
   ReplaceFldDataInStruct (clFoundData, "TXT1", sTLX_ARR) ;  
    }
 
    /* check if the field TXT2 exists in TLX */
  if (DoesFldExistInStruct ("TXT2", *sTLX_ARR) == RC_SUCCESS) 
    {
     clFoundData[0] = 0x00 ; 
   getFldDataFromStruct (clFoundData, "#ADR", *sSND_ARR) ;  
   ReplaceFldDataInStruct (clFoundData, "TXT2", sTLX_ARR) ;  
    }

  /* get URNO */
  ilRC = GetNextUrnoItem (&ilNextUrno) ;     /* get next Urno */
  if (ilRC == RC_SUCCESS)
  {
     sprintf (clFoundData, "%#010ld", ilNextUrno) ; 
   ReplaceFldDataInStruct (clFoundData, "URNO", sTLX_ARR) ;  
     ilRC = RC_SUCCESS ;
  }
  else
    {  
     dbg (DEBUG, "cant read a URNO for TLXTAB") ; 
     ilRC = RC_FAIL ;
  }

  /* write in USER = USEC and USEU  = user (last change) */
    /* check if the field USEC exists in TLX */
  if (DoesFldExistInStruct ("USEC", *sTLX_ARR) == RC_SUCCESS) 
    {
   ReplaceFldDataInStruct (pcgRecvName, "USEC", sTLX_ARR) ;  
    }
    /* check if the field USEU exists in TLX */
  if (DoesFldExistInStruct ("USEU", *sTLX_ARR) == RC_SUCCESS) 
    {
   ReplaceFldDataInStruct (pcgRecvName, "USEU", sTLX_ARR) ;  
    }

  return (RC_SUCCESS) ; 

} /* setTlxTableDat () */ 




/*-----------------------------------------------------------------------*/
/* function :  getVIALShortTxt ()                                        */
/*             Returns a string with all VIAL HOPOs like xxxYYYsssMMM... */
/*  pars.:                                                               */
/*    char * cpVialStr : coma sep value list                             */
/*    char * cpVialShortStr : the new short string from vial             */
/*                                                                  GHe  */
/*-----------------------------------------------------------------------*/
static int getVialShortTxt (char *cpVialStr, char *cpVialShortStr) 
{
 int  ilRC = RC_SUCCESS ; 
 char ilPtr ; 
 char clVial [10] ; 

 cpVialShortStr[0] = 0x00 ;
 ilPtr = 0 ; 
 while (strlen(&cpVialStr[ilPtr]) > 3)
 {
  memcpy (clVial, &cpVialStr[ilPtr+1], 3) ;
  clVial[3] = 0x00 ;
    strcat (cpVialShortStr, clVial) ; 
    ilPtr += 120 ; 
 }
 return ilRC ; 

} /* getVialShortTxt () */ 






/*-----------------------------------------------------------------------*/
/* function :  createValStr4Tlx()                                        */
/*             creates the data string for Telex                         */
/*  pars.:                                                               */
/*    char         * cpValStr : coma sep value list                      */
/*    ST_EVT_DATA_HDL sSNDARR : includes all information about the AFTTAB*/
/*                                                                  GHe  */
/*-----------------------------------------------------------------------*/
static int createValStr4Tlx (char *cpValStr, ST_EVT_DATA_HDL sTLX_ARR ) 
{
 int  ilRC = RC_SUCCESS ; 
 char clCompleteFldList[1024] ; 
 int  actItemInFldLst ;
 int  ilFldLen ; 
 char clFoundData[4096] ;
 char clFoundField[256] ;

 ilRC = getFldDataFromStruct (clCompleteFldList, "#TLX", sTLX_ARR) ;  

 cpValStr[0] = 0x00 ; 
 actItemInFldLst = 1 ; 
 if (ilRC == RC_SUCCESS) 
 {
  do 
    {
   ilFldLen = get_real_item (clFoundField, clCompleteFldList, actItemInFldLst) ;
   if (ilFldLen > 0)
     {
    getFldDataFromStruct (clFoundData, clFoundField, sTLX_ARR) ; 
        if (strlen(cpValStr) > 0) strcat (cpValStr, ",") ; 
    strcat (cpValStr, clFoundData) ; 
     } /* if field exist */
   actItemInFldLst++ ; 
    } while (ilFldLen > 0) ; 
 }
 return ilRC ;

} /* createValStr4Tlx () */ 


/*****************************************************************************/
static int SQLInsertTlxTab (ST_EVT_DATA_HDL sTLX_ARR)
{
 int   ilRC = RC_SUCCESS ;
 int   ilDBRc ;
 short sql_cursor ;
 short sql_fkt ;
 char  cpUsedFlds[1024] ;
 char  pcSqlStatement[1024] ;
 char  cplValStr[1024] ;
 int   ilItems ;

 getFldDataFromStruct (cpUsedFlds, "#TLX", sTLX_ARR) ; 
 ilRC = getItemCnt (cpUsedFlds, &ilItems) ;
 ilRC = prepSqlValueStr (ilItems, cplValStr) ;

 /* put in information for SQL statement */
 sprintf (pcSqlStatement, "INSERT INTO TLXTAB (%s) %s", cpUsedFlds, cplValStr) ;
 sql_cursor = 0 ;
 sql_fkt = START ;

 createValStr4Tlx (cplValStr, sTLX_ARR) ;

 dbg (DEBUG, "INSERT INTO TLXTAB USED FLDS = <%s>", cpUsedFlds) ; 
 dbg (DEBUG, "INSERT INTO TLXTAB VAL STR   = <%s>", cplValStr) ; 
 delton (cplValStr) ;
 ilDBRc = sql_if (sql_fkt, &sql_cursor, pcSqlStatement, cplValStr) ;
 if (ilDBRc != DB_SUCCESS)
 {
  dbg (TRACE, "-------------------------------------------------") ;
  dbg (TRACE, "TlxTab : CANT INSERT RECORD stat.: <%s>", pcSqlStatement) ;
  dbg (TRACE, "-------------------------------------------------") ;
  ilRC = RC_FAIL ;
 }
 else
 { 
  ilRC = RC_SUCCESS ; 
  commit_work() ;
 }
 close_my_cursor (&sql_cursor) ;

 return ilRC ; 
} /* SQLInsertTlxTab() */ 


/*-----------------------------------------------------------------------*/
/* function :  interpretDateInfo ()                                      */
/*             interprets a date/Time-string and writes the information  */
/*             to the DATEINFO struct                                    */
/*             On success the return is RC_SUCCESS else RC_FAIL          */
/*  pars.:                                                               */
/*          charDate       : the interpret date/time as string           */
/*          st_Date        : act. date in DATEINFO struct format         */
/*                                                                  GHe  */
/*-----------------------------------------------------------------------*/
static int interpretDateInfo (char *charDate, DATEINFO *st_Date )
{
 char cHelpDate[64] ;
 int  ilRC = RC_SUCCESS ;
 int  iLen = 0 ;

 dbg (DEBUG, "interpretDateInfo : START") ;

 iLen = strlen (charDate) ;
 if (iLen > 32)
 {
  memcpy (st_Date->cDateStampStr, charDate, 14) ;
  st_Date->cDateStampStr[14] = '\0' ;
 }
 else
 {
  strcpy (st_Date->cDateStampStr, charDate) ;
 }

 dbg (DEBUG, "interpretDateInfo : input str = <%s>", charDate) ;

 st_Date->iStDateInterpret = 0 ;     /* status for date */
 st_Date->iStTimeInterpret = 0 ;     /* status for time */
 st_Date->iYear  = 0 ;               /* reset all dates */
 st_Date->iMonth = 0 ;
 st_Date->iDate  = 0 ;
 st_Date->iHour  = 0 ;
 st_Date->iMins  = 0 ;
 st_Date->iSecs  = 0 ;

 if (iLen >= 8)
 {                                       /* get Date info */
  dbg (DEBUG, "interpretDateInfo : get date info") ;

  strcpy (cHelpDate, charDate) ;
  cHelpDate[4] = '\0' ;
  st_Date->iYear = atoi (cHelpDate) ;    /* year */

  strcpy (cHelpDate, &charDate[4]) ;
  cHelpDate[2] = '\0' ;
  st_Date->iMonth = atoi (cHelpDate) ;   /* month */

  strcpy (cHelpDate, &charDate[6]) ;
  cHelpDate[2] = '\0' ;
  st_Date->iDate = atoi (cHelpDate) ;    /* day */
  st_Date->iStDateInterpret = 1 ;        /* status : 1 -> read a date seq. */

 }
 else
 {                                       /* cant interpret date */
  dbg (DEBUG,"<interpretDateInfo> cant interpret date (len) => <%s>", charDate) ;
  ilRC = RC_FAIL ;
 }

 if (ilRC == RC_SUCCESS)                 /* interpret time string */
 {
  dbg (DEBUG, "interpretDateInfo : get time info") ;

  strcpy (cHelpDate, &charDate[8]) ;     /* start of time */
  if (strlen(cHelpDate) < 6)
  {
   dbg (DEBUG,"<interpretDateInfo> time not interpreted (len) !") ;
   ilRC = RC_FAIL ;
  }
  else
  {
   strcpy (cHelpDate, charDate) ;
   cHelpDate[2] = '\0' ;
   st_Date->iHour = atoi (cHelpDate) ;    /* hours  */

   strcpy (cHelpDate, &charDate[10]) ;
   cHelpDate[2] = '\0' ;
   st_Date->iMins = atoi (cHelpDate) ;    /* minutes */

   strcpy (cHelpDate, &charDate[12]) ;
   cHelpDate[2] = '\0' ;
   st_Date->iSecs = atoi (cHelpDate) ;    /* seconds */
   st_Date->iStTimeInterpret = 1 ;        /* status : 1 -> read a time seq. */
  }
 }

 dbg (DEBUG, "interpretDateInfo : check sequenz") ;

 if ((ilRC == RC_SUCCESS) && (st_Date->iStDateInterpret == 1) ) /*SYNCHK 4DATE*/
 {
  if ((st_Date->iMonth > 12) || (st_Date->iMonth < 1))
  {
   st_Date->iStDateInterpret = 11 ;  /* status:11 -> wrong month in date seq. */
   return (RC_FAIL) ;
  }

  if ((st_Date->iDate < 1) || (st_Date->iDate > 31))
  {
   st_Date->iStDateInterpret = 12 ;  /* status : 12 -> wrong day in date seq. */
   return (RC_FAIL) ;
  }

  switch (st_Date->iMonth)
  {
   case 2 : if ((((st_Date->iYear - 1900) % 4 ) != 0) && (st_Date->iDate > 28) )
            {
             st_Date->iStDateInterpret = 12 ;  /*st:12-> wrong day date seq. */
             return (RC_FAIL) ;
            }
            else
            {
             if (( ((st_Date->iYear - 1900) % 4 ) == 0) && (st_Date->iDate > 29) )
             {
              st_Date->iStDateInterpret = 12 ; /*st: 12->wrong day date seq. */
              return (RC_FAIL) ;
             }
            }
           break ;

   case  4 : /* months with 30 days */
   case  6 :
   case  9 :
   case 11 : if (st_Date->iDate > 30)
             {
              st_Date->iStDateInterpret = 12 ; /*st:12 -> wrong daydate seq. */
              return (RC_FAIL) ;
             }
           break ;

   default : /* months with 31 days ! check is done above */

           break ;

  }  /* switch */

  st_Date->iStTimeInterpret = 2 ;        /* status : 2 -> date seq. ok */
 }    /* if rc == RC_SUCCESS */


 if ((ilRC == RC_SUCCESS)  && (st_Date->iStTimeInterpret == 1) )  /* SYN CHK FOR TIME */
 {
  if ((st_Date->iHour > 23) || (st_Date->iHour <= 0))
  {
   st_Date->iStTimeInterpret = 12 ; /* st: 11 -> wrong hour range date seq. */
   return (RC_FAIL) ;
  }

  if ((st_Date->iMins > 60) || (st_Date->iMins <= 0))
  {
   st_Date->iStTimeInterpret = 13 ; /* st:11 -> wrong minute range date seq. */
   return (RC_FAIL) ;
  }

  if ((st_Date->iSecs > 60) || (st_Date->iSecs <= 0))
  {
   st_Date->iStTimeInterpret = 14 ; /* st:11 -> wrong secs range date seq. */
   return (RC_FAIL) ;
  }

  st_Date->iStTimeInterpret = 1 ;    /* status : 2 -> correct time seq. */

 }

 return ( ilRC ) ;

} /* end of interpretDateInfo () */





/*-----------------------------------------------------------------------*/
/* function :  incDateAtOne()                                            */
/*             adds 1 day, if the date in st_Date less than  st_maxDate  */
/*             if end date reached (no day added!) ipStatus returns 1    */
/*             and function return status is RC_FAIL (retStrDate is "",  */
/*             else the ipStatus is 0, function status is RC_SUCCESS     */
/*             and the string retStrDate contains the act. date in form  */
/*             yyyymmddhhmmss. The time part (hhmmss) is always zero, if */
/*             the act. date not the max date                            */
/*  pars.:                                                               */
/*          retStrDate     : returns the date in str. form after INC     */
/*          st_Date        : act. date previous the INC                  */
/*          st_MaxDate     : the max. useful whised date                 */
/*          ipStatus       : if inc done -> = 0, else  = 1 (no inc)      */
/*          incDateAtOne() : if inc done -> = RC_SUCCESS, else = RC_FAIL */
/*                           (no inc)                               GHe  */
/*-----------------------------------------------------------------------*/
static int incDateAtOne (char *retStrDate, DATEINFO *st_Date, int *ipStatus )
{
 char helpStr[64] ;
 int  ilRC ;
 int  iDateSet ;

 *ipStatus = 0 ;            /* init */
 retStrDate[0] = '\0' ;     /* reset a date */
 ilRC = RC_SUCCESS ;

 dbg (DEBUG, "incDateAtOne: START") ;

 switch (st_Date->iMonth)  /* in the switch, are all critical incs computed */
 {
  case 2 : if ((((st_Date->iYear - 1900) % 4 ) != 0) && (st_Date->iDate == 28) )
           {
            st_Date->iDate = 1 ;               /* NEXT MONTH */
            st_Date->iMonth = 3 ;
            iDateSet = 1 ;
           }
           else
           {
            if((((st_Date->iYear - 1900) % 4 ) == 0) && (st_Date->iDate == 29) )
            {
             st_Date->iDate = 1 ;               /* NEXT MONTH */
             st_Date->iMonth = 3 ;
             iDateSet = 1 ;
            }
           }
          break ;

  case  4 : /* months with 30 days */
  case  6 :
  case  9 :
  case 11 : if (st_Date->iDate == 30)
            {
             st_Date->iDate = 1 ;               /* NEXT MONTH */
             st_Date->iMonth = st_Date->iMonth + 1 ;
             iDateSet = 1 ;
            }
           break ;

  default : /* months with 31 days ! check is done above */
            if (st_Date->iDate == 31)
            {
             st_Date->iDate = 1 ;               /* NEXT MONTH */
             st_Date->iMonth = st_Date->iMonth + 1 ;
             if (st_Date->iMonth > 12)
             {
              st_Date->iMonth = 1 ;
              st_Date->iYear = st_Date->iYear + 1 ;
             }
             iDateSet = 1 ;
            }
           break ;

  }  /* switch */

  if (iDateSet == 0)
  {
   st_Date->iDate = st_Date->iDate + 1 ;        /* next Day  */
  }

 if (ilRC == RC_SUCCESS)
 {
  /* bring struct to string form = yyyymmddhhmmss*/
  sprintf (retStrDate, "%4d%#02d%#02d", st_Date->iYear,
                     st_Date->iMonth, st_Date->iDate) ;

  strcpy (helpStr, "000000") ;    /* no time */
  /* if the last date reached, add the time */
  strcat (retStrDate, helpStr) ;
 } /* else get datestr */

 return ( ilRC ) ;

} /* end of interpretDateInfo () */








/*-----------------------------------------------------------------------*/
/* function :  getDateDiff()                            SHORTNAME : gDD  */
/*             gets the day differenz between a StartDate and a Enddate. */
/*             The function works with the DAEINFO struct                */
/*  pars.:                                                               */
/*          st_beginDate   : act. date previous the INC                  */
/*          st_lastDate    : the max. useful whised date                 */
/*          retDateCnt     : the date differenz (on ERR -> -1)           */
/*          getDateDiff()  : RC_SUCCESS OR RC_FAIL                       */
/*                                                                  GHe  */
/*-----------------------------------------------------------------------*/
static int getDateDiff (DATEINFO *st_beginDate, DATEINFO *st_lastDate,
                                                int *retDateCnt )
{
 int  ilRC ;
 int  internDayCounter ;
 int  startYear ;
 int  endYear ;
 int  startMonth ;
 int  endMonth ;
 int  startDay ;
 int  endDay ;

 dbg (DEBUG, "gDD: -->") ;

 /*--- init ---*/
 internDayCounter = 0 ;
 *retDateCnt = 0 ;                    /* on error return here -1 */
 ilRC = RC_SUCCESS ;
 startYear  = st_beginDate->iYear ;
 endYear    = st_lastDate->iYear ;
 startMonth = st_beginDate->iMonth ;
 endMonth   = st_lastDate->iMonth ;
 startDay   = st_beginDate->iDate ;
 endDay     = st_lastDate->iDate ;


 /*--- check for right pars ---*/

 /* if date correct ? */
 if (startYear < endYear)
 {
  *retDateCnt = -1 ;
  dbg (DEBUG, "gDD: !!! START_YEAR is smaller as END_YEAR") ;
  return (RC_FAIL) ;
 }
 if ((startMonth > endMonth) && (startYear == endYear ))
 {
  dbg (DEBUG, "gDD: !!! START_YEAR/MONTH is bigger as END_YEAR/MONTH") ;
  *retDateCnt = -1 ;
  return (RC_FAIL) ;
 }
 if ((startDay > endDay) && (startMonth == endMonth) && (startYear == endYear))
 {
  dbg (DEBUG, "gDD: !!! START_YEAR/MONTH/DAY is bigger as END_YEAR/MONTH/DAY") ;
  *retDateCnt = -1 ;
  return (RC_FAIL) ;
 }

 /* interpret the daydiff 2 next month, if startmonth not equal endmonth */
 if (startMonth != endMonth)
 {
  switch (startMonth)
  {
   case 2 : if ( ( (startYear - 1900) % 4 ) != 0)
            {
             internDayCounter = 28 - startDay ;
            }
            else
            {
             internDayCounter = 29 - startDay ;
            }
           break ;

   case  4 : /* months with 30 days */
   case  6 :
   case  9 :
   case 11 : internDayCounter = 30 - startDay ;
           break ;

   default :              /* months with 31 days ! check is done above */
             internDayCounter = 31 - startDay ;
           break ;

  }  /* switch */

  startDay = 1 ;
  startMonth += 1 ;
  if (startMonth == 13)     /* jump into next year */
  {
   startMonth = 1 ;
   startYear += 1 ;
  }
 } /* if add diff. days from 1.st month */


 while (    (startYear < endYear)
        || ((startYear == endYear) && (startMonth < endMonth)) )
 {
  switch (startMonth)  /* in switch, are all critical incs computed */
  {
   case 2 : if ( ( (startYear - 1900) % 4 ) != 0)
            {
             internDayCounter += 28 ;
            }
            else
            {
             internDayCounter += 29 ;
            }
           break ;

   case  4 : /* months with 30 days */
   case  6 :
   case  9 :
   case 11 : internDayCounter += 30 ;
           break ;

   default :              /* months with 31 days ! check is done above */
             internDayCounter += 31 ;
           break ;

  }  /* switch */

  startMonth += 1 ;
  if (startMonth == 13)     /* jump into next year */
  {
   startMonth = 1 ;
   startYear += 1 ;
  }
 } /* while startyear... */

 /* add last diff days */
 internDayCounter += (endDay - startDay) ;

 dbg (DEBUG, "gDD: Daydiffernz between <%#02d.%#02d.%4d> and <%#02d.%#02d.%4d> is : <%d> days",
             st_beginDate->iDate, st_beginDate->iMonth,
                         st_beginDate->iYear, st_lastDate->iDate,
                         st_lastDate->iMonth, st_lastDate->iYear, internDayCounter
     ) ;

 *retDateCnt = internDayCounter ;

 dbg (DEBUG, "gDD: <--") ;
 return ( ilRC ) ;

} /* end of getDateDiff () */




/*==============================================*/
/* FUNCTION get_data_item                       */
/*==============================================*/
static int get_data_item(char *dest, char *src, int nbr)
{
 int rc ; /* logical Length of Item */
 rc = get_real_item(dest, src, nbr) ;
 dbg(DEBUG,"------------------------------------------------------") ;
 dbg(DEBUG,"ITEM (%d) RC=(%d)", nbr, rc) ;
 dbg(DEBUG,"DATA <%s>", src) ;
 dbg(DEBUG,"TEXT <%s>", dest) ;
 dbg(DEBUG,"------------------------------------------------------") ;
 if (rc < 1)
 {
  dest[0] = 0x20 ;
  dest[1] = 0x00 ;
 } /* end if */

 return rc ;
} /* end of get_data_item */

/*==============================================*/
/* FUNCTION timestamp                           */
/*                                              */
/*                                              */
/*==============================================*/
static int timestamp(void)
{
  time_t        _CurTime;
  struct tm     *CurTime;

  cur_time[0] = '\0';
  _CurTime = time(0L);
  CurTime = (struct tm *) localtime(&_CurTime);
  strftime (cur_time, 11, "%" "y%" "m%" "d%" "H%" "M",CurTime);
  return (0) ; 
} /* end of timestamp */



/**********************************************************************/
static int readDataItemFromStr( char *cpFldData, char *cpFldName,
                                char *cpFldLst, char *cpDataArea)
{
 int ilRC ;
 int ilItem ;

 ilRC = RC_SUCCESS ;
 ilItem = 0 ;
 ilRC = getDataFromDbStrs(&ilItem, cpFldName, cpFldLst, cpDataArea, cpFldData) ;
 return (ilRC) ;

} /* function readDataItemFromStr () */



/******************************************************************************/
static int getDataFromDbStrs (int *iItemNo, char *cFld,
                              char *pcgSearchFldLst, /* field list select */
                              char *pcgDataArea,     /* data list select */
                              char *pcgFoundData
                             )
{
 /* if ilItemNo (Input != 0) : search the item (1..n) and return the  */
 /* fieldname and data. If not found return inItemNo = 0              */
 /* if cFld (Input != "") : search the fieldname and return the       */
 /* ilItemNo (1..n) and data. If not found return inItemNo = 0        */

 int  ilRC = RC_SUCCESS ;
 int  ilLen ;
 int  ilFoundItem ;
 int  ilStatus ;

 ilStatus = C_NOTFOUND ;
 strcpy (pcgFoundData, "") ;
 if(*iItemNo == 0)
 {                                   /* search with field */
  ilFoundItem = get_item_no (pcgSearchFldLst, cFld, 5) + 1 ;
  if (ilFoundItem > 0)
  {                     /* found the field -> read data  */
   ilLen = get_real_item ( pcgFoundData, pcgDataArea, ilFoundItem ) ;
   if (ilLen > 0)
   {
    ilStatus = C_FOUND ;
    dbg (DEBUG, "getDataFromDbStrs: found fielddata <%s> from field <%s>",
                pcgFoundData, cFld) ;
   }
   else
   {
    ilStatus = C_FOUND ;
    dbg (DEBUG, "getDataFromDbStrs: fielddata length == 0 for field <%s>", cFld);
   }
  }
  else      /* fld name not found */
  {
   dbg(DEBUG,"getDataFromDbStrs: field <%s> not found", cFld);
  }
 }
 else   /* search with item */
 {                                                        /* get Field */
  ilFoundItem = *iItemNo ;
  ilLen = get_real_item ( cFld, pcgSearchFldLst, ilFoundItem ) ;
  if (ilLen > 0)
  {
   dbg (DEBUG, "getDataFromDbStrs: found field <%s> for item <%d>",
                cFld, ilFoundItem) ;
  }
  else
  {
   dbg(DEBUG,"getDataFromDbStrs: field length == 0 for field <%s>", cFld);
  }

  /* get Data */
  ilLen = get_real_item ( pcgFoundData, pcgDataArea, ilFoundItem ) ;
  if (ilLen > 0)
  {
   ilStatus = C_FOUND ;
   dbg (DEBUG, "getDataFromDbStrs: found fielddata <%s> from field <%s>",
               pcgFoundData, cFld) ;
  }
  else
  {
   ilStatus = C_FOUND ;
   dbg (DEBUG, "getDataFromDbStrs: fielddata length == 0 for field <%s>", cFld);
  }

 } /* search with item */

 if (ilStatus == C_NOTFOUND)
 {
  ilRC = RC_FAIL ;
 }
 else
 {
  ilRC = RC_SUCCESS ;
  *iItemNo = ilFoundItem ;
 }
 return (ilRC) ;
} /* getDataFromDbStrs () */




/******************************************************************************/

/******************************************************************************/
static int getItemCnt (char *cpFldStr, int *ipCnt)
{
 int   ilRC = RC_SUCCESS ;
 int   ilZ ;
 int   ilCnt ;
 int   ilReady ;
 char  clSubStr1[1024] ;
 char  clSubStr[1024] ;
 char  *cpChrPos ;

 ilZ = 0 ;
 ilCnt = 1 ;
 ilReady = RC_SUCCESS ;
 strcpy (clSubStr, cpFldStr) ;
 strcpy (clSubStr1, cpFldStr) ;
 if (strlen (clSubStr) < 3) ilCnt = 0 ;
 do
 {
  strcpy (clSubStr1, clSubStr) ;
  ilZ = 0 ;
  cpChrPos = strstr (clSubStr, ",") ;
  if (cpChrPos != NULL)                 /* more selections */
  {
   ilCnt++ ;
   *cpChrPos = 0x00 ;
   ilZ = strlen(clSubStr)+1 ;
   strcpy (clSubStr, &clSubStr1[ilZ]) ;
  }
  else
  {
   ilReady = RC_FAIL ;
  }
 } while (ilReady != RC_FAIL ) ;
 *ipCnt = ilCnt ;
 return (ilRC) ;

} /* getItemCnt () */





/*****************************************************************************/
/*******************************************************************/
static int prepSqlValueStr (int iItems, char *cpValStr)
{
 int   ilRC = RC_SUCCESS ;
 int   ilGetRc ;
 int   ilZ ;
 int   ilFirst ;
 char  cpDivStr[2] ;
 char  cpVal[20] ;

 strcpy (cpDivStr, ",") ;
 ilFirst = TRUE ;
 strcpy (cpValStr, "VALUES (") ;
 if (iItems == 0 ) return (RC_FAIL) ;
 for (ilZ = 0; ilZ < iItems; ilZ++)
 {
  if (ilFirst == FALSE) strcat (cpValStr, cpDivStr) ;
  ilFirst = FALSE ;
  sprintf (cpVal, ":val%d",ilZ);
  strcat (cpValStr, cpVal) ;
 }
 strcat (cpValStr, ")") ;
 if (igTestFktFlag)
 {
  dbg (DEBUG, "prepSqlValueStr: CREATE PREPARE SQL VAL STR = <%s>", cpValStr) ;
 }
 return (ilRC) ;
} /* prepSqlValueStr() */




/********************************************************************/
static int GetNextUrno(int *pipUrno)
{
  int   ilRC ;
  int   ilOldDbgLvl ;
  char  pclDataArea[IDATA_AREA_SIZE] ;

  if (igTestFktFlag)
  {
   dbg (DEBUG, "GetNextUrno >>") ;
  }
  /* clear buffer */
  memset((void*)pclDataArea, 0x00, IDATA_AREA_SIZE) ;

  /* get the next... */
  ilOldDbgLvl = debug_level;
  debug_level = TRACE;
  if ((ilRC = GetNextValues(pclDataArea, C_RESERVED_X_URNOS)) != RC_SUCCESS)
  {
   dbg (TRACE,"GetNextValues returns: %d", ilRC) ;
   debug_level = ilOldDbgLvl ;
   ilRC = RC_FAIL ;
  }
  else
  {
   debug_level = ilOldDbgLvl ;
  }

  /* convert to integer */
  *pipUrno = atoi(pclDataArea) ;
  if (igTestFktFlag)
  {
   dbg (DEBUG, "GetNextUrno <<") ;
  }
  return RC_SUCCESS ;

} /* getNextUrno () */




/********************************************************************/
static int GetNextUrnoItem(int *pipUrno)
{
 int  ilRC ;

 if (igTestFktFlag)
 {
  dbg (DEBUG, "GetNextUrnoItem >>") ;
 }
 igReservedUrnoCnt-- ;
 ilRC = RC_SUCCESS ;
 if (igReservedUrnoCnt <= 0)
 {                            /* hole Urno buffer -> get new Urnos */
  ilRC = GetNextUrno (pipUrno) ;
  igReservedUrnoCnt = C_RESERVED_X_URNOS - 1 ;
  igActUrno = *pipUrno ;
 }
 else
 {
  igActUrno++ ;
  *pipUrno = igActUrno ;      /* get next reserved Urno */
 }

 if (igTestFktFlag)
 {
  dbg (DEBUG, "GetNextUrnoItem: Next Urno is : <%#010d>", *pipUrno) ;
  dbg (DEBUG, "GetNextUrnoItem <<") ;
 }
 return (ilRC) ;

} /* getNextUrnoItem () */



/********************************************************************/
static int GetLocalTimeStamp(char *cpTimeStamp)
{
 int  ilRC ;
 static char  pclTime[iTIME_SIZE] ;
 time_t       _llCurTime ;
 struct tm   *_tm ;
 struct tm    prlCurTime ;

 if (igTestFktFlag)
 {
  dbg (DEBUG, "GetLocalTimeStamp >>") ;
 }
 ilRC = RC_SUCCESS ;
 memset((void*)pclTime, 0x00, iTIME_SIZE) ;
 _llCurTime = time(0L) ;
 _tm = (struct tm *) localtime(&_llCurTime) ;
 if (_tm != NULL)
 {
  prlCurTime = *_tm ;
  if (_llCurTime > 0)
  {
   strftime(pclTime,15,"%" "Y%" "m%" "d%" "H%" "M%" "S", &prlCurTime) ;
  }
  else
  {
   dbg(TRACE,"GetLocalTimeStamp: _llCurTime is %ld", _llCurTime) ;
  }
 }
 else
 {
  dbg(TRACE,"GetLocalTimeStamp: localtime %ld failed", _llCurTime) ;
  ilRC = RC_FAIL ;
 }
 strcpy (cpTimeStamp, pclTime) ;
 if (igTestFktFlag)
 {
  dbg (DEBUG, "GetLocalTimeStamp <<") ;
 }
 return (ilRC) ;
} /* GetLocalTimeStamp() */


/*-------------------------------------------------------------------*/
/*              FUNTIONS for FIELD STRUCT HANDLING                   */
/*-------------------------------------------------------------------*/

/* RESET_FLD_STRUCT */
/*********************************************************************/
static int ResetFldStruct (ST_EVT_DATA_HDL *sFldArr)
{
 int ilRC ;
 int ilZ ;

 ilRC = RC_SUCCESS ;
 for (ilZ = 0; ilZ < C_MAXFIELDS; ilZ++)
 {
  sFldArr->Status[ilZ] = C_DEAKTIV ;    /* reset field */
 }
 return (ilRC) ;
} /* ResetFldStruct () */


/* INSERT_FLD_STRUCT */
/*******************************************************************/
/* ATTENTION: USE THIS FUNCTION ONLY FOR HANDLE EVENT DATA         */
/*******************************************************************/
static int fillFldStruct (char *cpFldsStr, ST_EVT_DATA_HDL *sFldArr)
{
 char delim[2] ;
 int  fldLen ;
 int  fldPos ;
 long ilZ ;
 int  ilRC ;

 if (igTestFktFlag)
 {
  dbg (DEBUG, "fillFldStruct: work with string <%s>", cpFldsStr) ;
 }

 ilRC = RC_SUCCESS ;
 fldPos = 1 ;
 fldLen = 4 ;
 ilZ = 0 ;
 strcpy (delim, ",") ;                        /* used delimiter */
 while(strlen (&cpFldsStr[fldPos-1]) >= fldLen )
 {
  if (fldPos < fldLen) fldPos-- ;
  strncpy (sFldArr->Fieldname[ilZ], &cpFldsStr[fldPos], fldLen) ;
  sFldArr->Fieldname[ilZ][fldLen] = 0x00 ;
  sFldArr->Status[ilZ] = C_AKTIV ;           /* set field active */
  sFldArr->FieldVal[ilZ][0] = 0x000 ;

  sFldArr->Item[ilZ] = get_item_no(cpFldsStr,sFldArr->Fieldname[ilZ],5)+1;
  sFldArr->StrItem[ilZ] = C_NOTSET ;
  sFldArr->StrBeginPos[ilZ] = (ilZ + (ilZ * fldLen)) - 1 ;
  sFldArr->ValLength[ilZ] = 0 ;
  sFldArr->ReadFromEv[ilZ] = C_ENABLE ;
  sFldArr->DoFldCmp[ilZ] = C_ENABLE ;
  sFldArr->DoUpdate[ilZ] = C_DISABLE ;
  sFldArr->DoSelect[ilZ] = C_DISABLE ;
  sFldArr->DoInsert[ilZ] = C_DISABLE ;
  sFldArr->DoSearch[ilZ] = C_DISABLE ;
  if (igTestFktFlag)
  {
   dbg(DEBUG,"fillFldStruct: interpret Fld=<%s>",sFldArr->Fieldname[ilZ]);
  }
  ilZ++ ;
  fldPos = ilZ + (ilZ * fldLen) ;
 }
 return (ilRC) ;
} /* end of fillFldStruct */





/*******************************************************************/
/* ATTENTION: USE THIS FUNCTION ONLY FOR READ EVENT DATA           */
/*******************************************************************/
static int fillDataInFldStruct (char *cpDataStr, ST_EVT_DATA_HDL *sFldArr)
{
 int ilRC = RC_SUCCESS ;
 int ilZ ;
 int ilLen ;

 ilZ = 0 ;
 while (sFldArr->Status[ilZ] == C_AKTIV)
 {
  if (sFldArr->Item[ilZ] > C_NOTSET)
  {
   if (sFldArr->ReadFromEv[ilZ] == C_ENABLE)
   {
    ilLen=get_real_item(sFldArr->FieldVal[ilZ], cpDataStr, sFldArr->Item[ilZ]);
    /* str_trm_all(sFldArr->FieldVal[ilZ], " ", TRUE) ; */
    if (igTestFktFlag)
    {
     dbg (DEBUG, "fillDataInFieldStruct: Fld=<%s> Data=<%s>",
                 sFldArr->Fieldname[ilZ], sFldArr->FieldVal[ilZ]) ;
    }
    sFldArr->ValLength[ilZ] = strlen (sFldArr->FieldVal[ilZ]) ;
   }
  }
  else
  {
   ilRC = RC_FAIL ;
   dbg (DEBUG, "fillDataInFieldStruct: ERR: sFldArr ITEM > 0 !!! ") ;
  }

  ilZ++ ;
 }
 return (ilRC) ;

} /* fillDataInFldStruct () */





/*******************************************************************/
static int AddFld2Struct (ST_EVT_DATA_HDL *sFldArr, char *cpFldName,
                          char *cpValStr )
{
 long ilZ ;
 int  ilRC = RC_SUCCESS ;

 ilZ = 0 ;

 while (sFldArr->Status[ilZ] == C_AKTIV) ilZ++ ;
 if (ilZ > C_MAXFIELDS)
 {
  dbg (TRACE, "AddFldStruct: BUFFER OVERFLOW") ;
  ilRC = RC_FAIL ;
 }
 if (ilRC == RC_SUCCESS)
 {
  strcpy (sFldArr->Fieldname[ilZ], cpFldName) ;
  sFldArr->Status[ilZ] = C_AKTIV ;           /* set field active */
  strcpy (sFldArr->FieldVal[ilZ], cpValStr) ;
  sFldArr->StrItem[ilZ] = C_NOTSET ;
  sFldArr->StrBeginPos[ilZ] = 0 ;
  sFldArr->Item[ilZ] = 0 ;
  sFldArr->ValLength[ilZ] = strlen (cpValStr) ;
  sFldArr->LastAttach[ilZ] = C_NOTSET ;
  sFldArr->ReadFromEv[ilZ] = C_DISABLE ;
  sFldArr->DoFldCmp[ilZ] = C_DISABLE ;
  sFldArr->DoUpdate[ilZ] = C_DISABLE ;
  sFldArr->DoSelect[ilZ] = C_DISABLE ;
  sFldArr->DoInsert[ilZ] = C_DISABLE ;
  sFldArr->DoSearch[ilZ] = C_DISABLE ;
  if (igTestFktFlag)
  {
   dbg (DEBUG, "AddFld2Struct: INSERT REC=FLD<%s>,STAT=AKTIV,DATA=<%s>",
               sFldArr->Fieldname[ilZ],
               sFldArr->FieldVal[ilZ]) ;
  }
 }
 return (ilRC) ;
} /* end of AddFld2Struct */






/*******************************************************************/
static int InsOrReplFld2Struct (ST_EVT_DATA_HDL *sFldArr, char *cpFldName,
                                char *cpValStr )
{
 int  ilRC = RC_SUCCESS ;

 ilRC = DoesFldExistInStruct (cpFldName, *sFldArr) ;
 if (ilRC == RC_FAIL)
 {
  /* field does not exist */
  ilRC = AddFld2Struct (sFldArr, cpFldName, cpValStr ) ;
 }
 else
 {
  /* field exist -> update the field */
  dbg(DEBUG, "InsOrReplFld2Struct: REPLACE NOT ACTIVE!!!") ;
  /* ilRC = ReplaceFldDataInStruct (cpValStr, cpFldName, sFldArr) ; */
 }
 return (ilRC) ;
} /* end of InsOrReplFld2Struct */






/*******************************************************************/
static int ReplaceFldDataInStruct (char *cpDatStr, char *pclFld,
                                   ST_EVT_DATA_HDL *sFldArr)
{
 int ilRC = RC_SUCCESS ;
 int ilZ ;
 int ilStatus ;

 ilZ = 0 ;
 ilStatus = C_NOTFOUND ;
 while ((sFldArr->Status[ilZ] == C_AKTIV) && (ilStatus == C_NOTFOUND))
 {
  if (strcmp (pclFld, sFldArr->Fieldname[ilZ]) == 0)
  {
   /* found the field */
   strcpy (sFldArr->FieldVal[ilZ], cpDatStr) ;
   ilStatus = C_FOUND ;
   if (igTestFktFlag)
   {
    dbg (DEBUG, "ReplaceFldDataInStruct: Fld=<%s> Data=<%s>", pclFld,
                cpDatStr) ;
   }
  }
  ilZ++ ;
 }
 if (ilStatus == C_NOTFOUND)
 {
  ilRC = RC_FAIL ;
  dbg (TRACE, "ReplaceFldDataInStruct: ERR: Field=<%s> NOT found", pclFld) ;
 }
 return (ilRC) ;

} /* ReplaceDataInStruct () */




/*******************************************************************/
static int getFldDataFromStruct (char *cpRetStr, char *cpFld,
                                 ST_EVT_DATA_HDL sFldArr)
{
 int ilRC = RC_SUCCESS ;
 int ilZ ;
 int ilStatus ;

 ilZ = 0 ;
 ilStatus = C_NOTFOUND ;
 while ((sFldArr.Status[ilZ] == C_AKTIV) && (ilStatus == C_NOTFOUND))
 {
  if (strcmp (cpFld, sFldArr.Fieldname[ilZ]) == 0)
  {
   /* found the field */
   strcpy (cpRetStr, sFldArr.FieldVal[ilZ]);
   ilStatus = C_FOUND ;
   if (igTestFktFlag)
   {
    dbg (DEBUG, "getFldDataFromStruct: DATA=<%s> FLD=<%s>",
                cpRetStr, cpFld) ;
   }
  }
  ilZ++ ;
 }
 if (ilStatus == C_NOTFOUND)
 {
  ilRC = RC_FAIL ;
    cpRetStr[0] = 0x00 ; 
  dbg (TRACE, "getFldDataFromStruct: CANT FIND FIELD: <%s>", cpFld) ;
 }
 return (ilRC) ;

} /* getFldDataFromStruct () */





/*******************************************************************/
static int DoesFldExistInStruct (char *cpFld, ST_EVT_DATA_HDL sFldArr)
{
 int ilRC = RC_SUCCESS ;
 int ilZ ;
 int ilStatus ;

 ilZ = 0 ;
 ilStatus = C_NOTFOUND ;
 while ((sFldArr.Status[ilZ] == C_AKTIV) && (ilStatus == C_NOTFOUND))
 {
  if (strcmp (cpFld, sFldArr.Fieldname[ilZ]) == 0)
  {
   /* found the field */
   if (igTestFktFlag)
   {
    dbg (DEBUG, "DoesFldExistInStruct: Fld=<%s> exist", cpFld) ;
   }
   ilStatus = C_FOUND ;
  }
  ilZ++ ;
 }
 if (ilStatus == C_NOTFOUND)
 {
  ilRC = RC_FAIL ;
  if (igTestFktFlag)
  {
   dbg (DEBUG, "DoesFldExistInStruct: Fld=<%s> does not exist", cpFld) ;
  }
 }
 return (ilRC) ;

} /* DoesFldExistInStruct () */







/*******************************************************************/
static int getFldStruct (char *pclFldsStr, char *pclDataStr, int iAction,
                         ST_EVT_DATA_HDL *sFldArr)
{
 int  ilZ ;
 int  ilRC = RC_SUCCESS ;
 int  ilCanPosAttach ;
 int  ilDone ;

 pclFldsStr[0] = 0x00 ;
 pclDataStr[0] = 0x00 ;
 ilRC = setNextFldPosInStruct (&ilCanPosAttach, iAction, sFldArr) ;
 if (ilRC == RC_SUCCESS)    /* found another field */
 {
  /* get the active attach field */
  ilDone = FALSE ;
  ilZ = 0 ;
  do
  {
   if (sFldArr->LastAttach[ilZ] == C_AKTIV)
   {
    strcpy (pclFldsStr, sFldArr->Fieldname[ilZ]) ;
    strcpy (pclDataStr, sFldArr->FieldVal[ilZ]) ;
    ilDone = TRUE ;
    if (igTestFktFlag)
    {
     dbg (DEBUG, "getFldStruct: READ DATA=<%s>, FLD=<%s>", pclDataStr,
                  pclFldsStr) ;
    }
   } /* if found field & data */
   else
   {
    if (ilZ >= C_MAXFIELDS-1)
    {
     ilDone = TRUE ;
     ilRC = RC_FAIL ;
     if (igTestFktFlag)
     {
      dbg (DEBUG, "getFldStruct: NO FIELDPOS FOUND!") ;
     }
    }
   }
   ilZ++ ;
  } while (ilDone == FALSE) ;
 }

 return (ilRC) ;

} /* getFldStruct() */





/*******************************************************************/
static int setNextFldPosInStruct (int *ipActionResult, int iAction,
                                  ST_EVT_DATA_HDL *sFldArr)
{
 long ilZ ;
 int  ilRC ;
 int  ilDone ;
 int  ilActionCode ;

 ilRC = RC_SUCCESS ;
 ilActionCode = C_NO_ERROR ;
 ilDone = FALSE ;
 switch (iAction)
 {
  case  C_FIRST:
                 if (igTestFktFlag)
                 {
                   dbg (DEBUG, "setNextFldPosInStruct: POS ON BEGINING!") ;
                 }
                 for (ilZ = 0; ilZ < C_MAXFIELDS; ilZ++)
                 {                    /* reset all attach fields */
                  sFldArr->LastAttach[ilZ] = C_DEAKTIV ;
                 }
                 if (sFldArr->Status[0] == C_AKTIV)
                 {
                  sFldArr->LastAttach[0] = C_AKTIV ;
                 }
                 else
                 {
                  ilActionCode = C_NO_ACTIVE_FIELDS ;
                 }
                break ;

  case C_NEXT:
                 if (igTestFktFlag)
                 {
                  dbg (DEBUG, "setNextFldPosInStruct: POS ON NEXT FIELD!") ;
                 }
                 ilZ = 0 ;
                 do
                 {
                  if (sFldArr->Status[ilZ] == C_AKTIV)
                  {
                   if (sFldArr->LastAttach[ilZ] == 1)
                   {
                    if (ilZ >= C_MAXFIELDS)
                    {
                     ilDone = TRUE ;
                     ilActionCode = C_SET_ON_LAST_FIELD ;
                    }
                    else
                    {
                     if (sFldArr->Status[ilZ+1] == C_AKTIV)
                     {
                      sFldArr->LastAttach[ilZ] = C_DEAKTIV ;   /* reset old */
                      sFldArr->LastAttach[ilZ+1] = C_AKTIV ;   /* set new */
                      ilDone = TRUE ;
                     }
                     else
                     {
                      ilDone = TRUE ;
                      ilActionCode = C_SET_ON_LAST_FIELD ;
                     } /* if next fldStr == AKTIV */
                    } /* if not all fields */
                   } /* if last attach == AKTIV */
                  } /* if status == AKTIV */
                  else
                  {
                   ilDone = TRUE ;
                   ilActionCode = C_NO_ACTIVE_FIELDS ;
                  }
                  ilZ++ ;
                 } while (ilDone == FALSE) ;
                break ;

  case C_PREV:
                 if (igTestFktFlag)
                 {
                  dbg (DEBUG, "setNextFldPosInStruct: POS ON PREVIOUS FIELD!") ;
                 }
                 ilZ = 0 ;
                 do
                 {
                  if (sFldArr->Status[ilZ] == C_AKTIV)
                  {
                   if (sFldArr->LastAttach[ilZ] == C_AKTIV)
                   {
                    if (ilZ <= 0)      /* set on the first position */
                    {
                     ilDone = TRUE ;
                     ilActionCode = C_SET_ON_FIRST_FIELD ;
                    }
                    else
                    {
                     sFldArr->LastAttach[ilZ] = C_DEAKTIV ; /* reset old */
                     sFldArr->LastAttach[ilZ-1] = C_AKTIV ; /* set new */
                     ilDone = TRUE ;
                    } /* if not all fields */
                   } /* if last attach == AKTIV */
                  } /* if status == AKTIV */
                  else
                  {
                   ilDone = TRUE ;
                   ilActionCode = C_NO_ACTIVE_FIELDS ;
                  }
                  ilZ++ ;
                 } while (ilDone == FALSE) ;
                break ;

  case C_LAST:
                 if (igTestFktFlag)
                 {
                  dbg (DEBUG, "setNextFldPosInStruct: POS ON LAST FIELD!") ;
                 }
                 for (ilZ = 0; ilZ < C_MAXFIELDS; ilZ++)
                 {                         /* reset all attach fields */
                  sFldArr->LastAttach[ilZ] = C_DEAKTIV ;
                 }
                 ilZ = 0 ;      /* set on start */
                 while (sFldArr->Status[ilZ] == C_AKTIV)
                 {
                  ilZ++ ;
                 }  /* search the last field */
                 if (ilZ == 0)
                 {
                  ilActionCode = C_NO_ACTIVE_FIELDS ;
                 }
                 else
                 {
                  sFldArr->LastAttach[ilZ-1] = C_AKTIV ;
                 }
                break ;

  default :

                 dbg (DEBUG, "setNextFldPosInStruct: UNKNOWN Cmd = <%d>", iAction) ;
                 ilActionCode = C_UNKNOWN_COMMAND ;
                break ;

  } /* switch */

  if (ilActionCode != C_NO_ERROR)
  {
   if (igTestFktFlag)
   {
    dbg (DEBUG, "setNextFldPosInStruct: CONDITION REACHED -> CANT POS!") ;
   }
   ilRC = RC_FAIL ;
  }
  *ipActionResult = ilActionCode ;

  return ( ilRC) ;

} /* function setNextFldPosInStruct() */






/*******************************************************************/
static int setChkFldinStruct4Fld (char *pclFld, int iDoWhat,
                                  ST_EVT_DATA_HDL *sFldArr)
{
 int ilRC = RC_SUCCESS ;
 int ilZ ;
 int ilStatus ;

 ilZ = 0 ;
 ilStatus = C_NOTFOUND ;
 while ((sFldArr->Status[ilZ] == C_AKTIV) && (ilStatus == C_NOTFOUND))
 {
  if (strcmp (pclFld, sFldArr->Fieldname[ilZ]) == 0)
  {
   /* found the field */
   sFldArr->DoFldCmp[ilZ] = iDoWhat ;
   ilStatus = C_FOUND ;
   if (igTestFktFlag)
   {
    dbg (DEBUG, "setChkFldinStruct4Fld: Set fldCmp to=<%d> in FLD=<%s>",
                iDoWhat, pclFld) ;
   }
  }
  ilZ++ ;
 }
 if (ilStatus == C_NOTFOUND)
 {
  ilRC = RC_FAIL ;
  dbg (TRACE, "setChkFldinStruct4Fld: CANT FIND FIELD: <%s>", pclFld) ;
 }

 return (ilRC) ;
} /* setChkFldinStruct4Fld () */







/*******************************************************************/
/* if pclFld == "" -> do set Flag iDoWhat for all fields           */
/* if iKindOfFlg == 0 -> Update all Falgs with iDoWhat             */
/*******************************************************************/
static int setStatusInStruct (char *pclFld, int iDoWhat, int iKindOfFlg,
                              ST_EVT_DATA_HDL *sFldArr)
{
 int  ilRC = RC_SUCCESS ;
 int  ilZ ;
 int  ilStatus ;
 char cActionKind[30] ;

 ilZ = 0 ;
 ilStatus = C_NOTFOUND ;
 if (pclFld[0] == 0x00)
 {
  while (sFldArr->Status[ilZ] == C_AKTIV)
  {
   switch (iKindOfFlg)
   {
    case 0        : sFldArr->DoFldCmp[ilZ] = iDoWhat ;
                    sFldArr->DoUpdate[ilZ] = iDoWhat ;
                    sFldArr->DoSelect[ilZ] = iDoWhat ;
                    sFldArr->DoInsert[ilZ] = iDoWhat ;
                    sFldArr->DoSearch[ilZ] = iDoWhat ;
                    strcpy (cActionKind, "ALL") ;
                  break ;

    case C_SEARCH : sFldArr->DoSearch[ilZ] = iDoWhat ;
                    strcpy (cActionKind, "SEARCH") ;
                  break ;

    case C_INSERT : sFldArr->DoInsert[ilZ] = iDoWhat ;
                    strcpy (cActionKind, "INSERT") ;
                  break ;

    case C_UPDATE : sFldArr->DoUpdate[ilZ] = iDoWhat ;
                    strcpy (cActionKind, "UPDATE") ;
                  break ;

    case C_SELECT : sFldArr->DoSelect[ilZ] = iDoWhat ;
                    strcpy (cActionKind, "SELECT") ;
                  break ;

    case C_FLDCMP : sFldArr->DoFldCmp[ilZ] = iDoWhat ;
                    strcpy (cActionKind, "FLDCMP") ;
                  break ;

    default       : dbg (TRACE, "SetStatusInStruct: UNKNOWN KIND OF FIELD") ;
                    strcpy (cActionKind, "UNKNOWN") ;
                  break ;

   } /* switch() */
   ilZ++ ;
   ilStatus = C_FOUND ;
  } /* while */

  if (igTestFktFlag)
  {
   switch (iDoWhat)
   {
    case C_ENABLE :

          dbg(DEBUG, "setStatusInStruct: ENAB <%s> STAT FOR ALL FLDS",
                     cActionKind   ) ;
         break ;

    case C_DISABLE :

          dbg(DEBUG,"setStatusInStruct: DISAB <%s> STAT FOR ALL FLDS",
                     cActionKind   ) ;
         break ;

    default :

          dbg(DEBUG,"setStatusInStruct: SET <%s> STAT = <%d> FOR ALL FLDS",
                     cActionKind, iDoWhat) ;
         break ;
   } /* switch */
  } /* if TestFlag */
 } /* if all */
 else
 {           /* search for the field */
  while ((sFldArr->Status[ilZ] == C_AKTIV) && (ilStatus == C_NOTFOUND))
  {
   if (strcmp (pclFld, sFldArr->Fieldname[ilZ]) == 0)
   {
    /* found the field */
    switch (iKindOfFlg)
    {
     case 0        : sFldArr->DoFldCmp[ilZ] = iDoWhat ;
                     sFldArr->DoUpdate[ilZ] = iDoWhat ;
                     sFldArr->DoSelect[ilZ] = iDoWhat ;
                     sFldArr->DoInsert[ilZ] = iDoWhat ;
                     sFldArr->DoSearch[ilZ] = iDoWhat ;
                     strcpy (cActionKind, "ALL") ;
                   break ;

     case C_SEARCH : sFldArr->DoSearch[ilZ] = iDoWhat ;
                     strcpy (cActionKind, "SEARCH") ;
                   break ;

     case C_INSERT : sFldArr->DoInsert[ilZ] = iDoWhat ;
                     strcpy (cActionKind, "INSERT") ;
                   break ;

     case C_UPDATE : sFldArr->DoUpdate[ilZ] = iDoWhat ;
                     strcpy (cActionKind, "UPDATE") ;
                   break ;

     case C_SELECT : sFldArr->DoSelect[ilZ] = iDoWhat ;
                     strcpy (cActionKind, "SELECT") ;
                   break ;

     case C_FLDCMP : sFldArr->DoFldCmp[ilZ] = iDoWhat ;
                     strcpy (cActionKind, "FLDCMP") ;
                   break ;

     default       : dbg (TRACE, "SetStatusInStruct: KIND OF FIELD") ;
                     strcpy (cActionKind, "UNKNOWN") ;
                   break ;

    } /* switch() */
    ilStatus = C_FOUND ;

    if (igTestFktFlag)
    {
     switch (iDoWhat)
     {
      case C_ENABLE :

           dbg (DEBUG, "setStatusInStruct: ENAB <%s> STAT > FLD = <%s>",
                       cActionKind, pclFld);
          break ;

      case C_DISABLE :

           dbg (DEBUG, "setStatusInStruct: DISAB <%s> STAT > FLD = <%s>",
                       cActionKind, pclFld);
          break ;

       default :

           dbg (DEBUG, "setStatusInStruct: SET <%s> STAT=<%d> > FLD = <%s>",
                       cActionKind, iDoWhat, pclFld) ;
          break ;
     } /* switch */
    } /* if testFlag */
   } /* if strcmp == fld */
   ilZ++ ;
  } /* while field not found */
 } /* else */

 if (ilStatus == C_NOTFOUND)
 {
  ilRC = RC_FAIL ;
  dbg (TRACE, "setStatusInStruct: CANT FIND FIELD: <%s>", pclFld) ;
 }
 return (ilRC) ;
} /* setStatusInStruct () */




/*******************************************************************/
static int createFldAndDataLst (char *cpFldLst, char *cpDataLst,
                                int iKindOf, ST_EVT_DATA_HDL sFldArr)
{
 int ilRC = RC_SUCCESS ;
 int ilZ ;
 int ilFirstItem ;
 int iAddFldData ;
 char cpDivStr[2] ;

 strcpy (cpDivStr, ",") ;
 ilZ = 0 ;
 ilFirstItem = TRUE ;
 while (sFldArr.Status[ilZ] == C_AKTIV)
 {
  iAddFldData = FALSE ;
  switch (iKindOf)
  {
   case C_SEARCH : if (sFldArr.DoSearch[ilZ] == C_ENABLE)
                   {
                    iAddFldData = TRUE ;
                   }
                  break ;

   case C_INSERT : if (sFldArr.DoInsert[ilZ] == C_ENABLE)
                   {
                    iAddFldData = TRUE ;
                   }
                  break ;

   case C_SELECT : if (sFldArr.DoSelect[ilZ] == C_ENABLE)
                   {
                    iAddFldData = TRUE ;
                   }
                  break ;

   case C_UPDATE : if (sFldArr.DoUpdate[ilZ] == C_ENABLE)
                   {
                    iAddFldData = TRUE ;
                   }
                  break ;

   case C_FLDCMP : if (sFldArr.DoFldCmp[ilZ] == C_ENABLE)
                   {
                    iAddFldData = TRUE ;
                   }
                  break ;

    } /* switch */

    if (iAddFldData == TRUE)
    {
     /* add item to field and data */
     if (ilFirstItem == FALSE)
     {
      strcat (cpFldLst, cpDivStr) ;
      strcat (cpDataLst, cpDivStr) ;
     }
     ilFirstItem = FALSE ;
     strcat (cpFldLst, sFldArr.Fieldname[ilZ]) ;
     strcat (cpDataLst, sFldArr.FieldVal[ilZ]) ;
    }
    ilZ++ ;
   }  /* while not all flds */

   if (igTestFktFlag)
   {
    dbg (DEBUG, "createFldAndDataLst: generated Fieldlist = <%s>", cpFldLst) ;
    dbg (DEBUG, "createFldAndDataLst: generated Datalist = <%s>", cpDataLst) ;
   }
   return (ilRC) ;

} /* createFldAndDataLst () */







/*****************************************************************************/
/* send a event message to other processes, that this process gets an        */
/* update of active database changes                                         */
/*****************************************************************************/
static int TriggerAction(char *pcpTableName, char *pcpCmdRec, 
                                                 char *pcpFieldName)
{
 int           ilRC          = RC_SUCCESS ;    /* Return code */
 EVENT        *prlEvent      = NULL ;
 BC_HEAD      *prlBchead     = NULL ;
 CMDBLK       *prlCmdblk     = NULL ;
 ACTIONConfig *prlAction     = NULL ;
 char         *pclSelection  = NULL ;
 char         *pclFields     = NULL ;
 char         *pclData       = NULL ;
 long          llSize        = 0 ;
 int           ilAnswerQueue = 0 ;
 char          clQueueName[16] ;
 int           ili = 0 ; 

 /* get memory for event */
 llSize= sizeof(EVENT)+sizeof(BC_HEAD)+sizeof(CMDBLK)+sizeof(ACTIONConfig)
                                            +strlen(pcpTableName)+strlen(pcpFieldName)+
                                            +strlen(pcpCmdRec) + 666; 
 prgOutEvent = (EVENT *)malloc((size_t)llSize) ; 
 if(prgOutEvent == NULL)
 {
  dbg(TRACE,"TriggerAction: malloc out event <%d> bytes failed", llSize) ;
  return (RC_FAIL) ;
 } /* end of if */
 memset ((void*)prgOutEvent, 0x00, llSize) ;

 /* create trigger queue */
 sprintf (&clQueueName[0],"%s2", mod_name) ;
 dbg (DEBUG, "Used Trigger queue name is <%s>", clQueueName) ; 
 ilRC = GetDynamicQueue(&ilAnswerQueue, &clQueueName[0]) ;
 if (ilRC != RC_SUCCESS)
 {
  dbg (TRACE, "TriggerAction: GetDynamicQueue failed <%d>", ilRC) ;
  return (ilRC) ; 
 }
 else
 {
  dbg(DEBUG,"TriggerAction: got <%d> for <%s>",ilAnswerQueue, &clQueueName[0]) ;
 } /* end of if */

 prlBchead     = (BC_HEAD *) ((char *) prgOutEvent + sizeof(EVENT)) ; 
 prlCmdblk     = (CMDBLK *)  ((char *) prlBchead->data) ;
 pclSelection  = prlCmdblk->data ;
 pclFields     = pclSelection + strlen (pclSelection) + 1 ;
 pclData       = pclFields + strlen (pclFields) + 1 ;
 strcpy (pclData, "DYN") ;   /* STA */
 prlAction     = (ACTIONConfig *) (pclData + strlen(pclData) + 1) ;

 prgOutEvent->type        = USR_EVENT ;
 prgOutEvent->command     = EVENT_DATA ;
 prgOutEvent->originator  = ilAnswerQueue ;
 prgOutEvent->retry_count = 0 ;
 prgOutEvent->data_offset = sizeof(EVENT) ;
 prgOutEvent->data_length = llSize-sizeof(EVENT) ;

 prlAction->iEmptyFieldHandling = iDELETE_ALL_BLANKS ;
 prlAction->iIgnoreEmptyFields = 0 ;
 /* from Action return defs. */
 strcpy(prlAction->pcSndCmd, "") ;       /* pcpCmdRec) ; */
 sprintf(prlAction->pcSectionName,"%s_%s", mod_name, pcpTableName) ;
 strcpy(prlAction->pcTableName, pcpTableName) ;
 sprintf(prlAction->pcFields, "%s%s", pcpFieldName, ",URNO") ; /* VIAL,FLDA */
 strcpy(prlAction->pcSectionCommands, "URT,IRT") ;
 prlAction->iModID = mod_id ;

 /* send to action handler send first a delete, for action knows this already */
 if(ilRC == RC_SUCCESS)
 {
  prlAction->iADFlag = iDELETE_SECTION ;
  ilRC=
     que(QUE_PUT, 7400, ilAnswerQueue, PRIORITY_3, llSize, (char *)prgOutEvent);
  if (ilRC != RC_SUCCESS)
  {
   dbg(TRACE,"TriggerAction: QUE_PUT failed <%d>",ilRC) ;
  }
  else
  {
   ilRC = WaitAndCheckQueue(10, ilAnswerQueue,&prgItem) ;
   if(ilRC != RC_SUCCESS)
   {
    dbg(TRACE,"TriggerAction: WaitAndCheckQueue failed <%d>",ilRC) ;
   }
  } /* end of if */
 } /* end of if */


 if(ilRC == RC_SUCCESS)
 {
  prlAction->iADFlag = iADD_SECTION ;

  ilRC = que(QUE_PUT,7400,ilAnswerQueue,PRIORITY_3,llSize,(char *)prgOutEvent);
  if(ilRC != RC_SUCCESS)
  {
   dbg(TRACE,"TriggerAction: QUE_PUT failed <%d>",ilRC) ;
  }
  else
  {
   ilRC = WaitAndCheckQueue(10,ilAnswerQueue,&prgItem) ;
   if(ilRC != RC_SUCCESS)
   {
    dbg(TRACE,"TriggerAction: WaitAndCheckQueue failed <%d>",ilRC) ;
   }
   else
   {
    prlEvent     = (EVENT *)   ((char *)prgItem->text) ;
    prlBchead    = (BC_HEAD *) ((char *)prlEvent + sizeof(EVENT)) ;
    prlCmdblk    = (CMDBLK *)  ((char *)prlBchead->data) ;
    pclSelection = (char *)    prlCmdblk->data ;
    pclFields    = (char *)    pclSelection + strlen(pclSelection) + 1 ;
    pclData      = (char *)    pclFields + strlen(pclFields) + 1 ;

    if (strcmp (pclData, "SUCCESS") != 0)
    {
     ilRC = RC_FAIL ;
    }
    else
    {
     dbg(DEBUG,"TriggerAction: add dynamic action config OK") ;
    }/* end of if */
   }/* end of if */
  }/* end of if */
 }/* end of if */

 /* delete the queue */
 ilRC = que(QUE_DELETE,ilAnswerQueue,ilAnswerQueue,0,0,0) ;
 if(ilRC != RC_SUCCESS)
 {
  dbg(TRACE,"que QUE_DELETE <%d> failed <%d>", ilAnswerQueue, ilRC) ;
 }
 else
 {
  dbg(DEBUG,"TriAct: queue <%d> <%s> deleted",ilAnswerQueue,&clQueueName[0]) ;
 }/* end of if */

 return (ilRC) ;
} /* end of TriggerAction */





/*****************************************************************************/
/*****************************************************************************/
static int WaitAndCheckQueue(int ipTimeout, int ipModId, ITEM **prpItem)
{
 int ilRC          = RC_SUCCESS ;      /* Return code */
 int ilWaitCounter = 0 ;

 do
 {
  sleep(1) ;
  ilWaitCounter++ ;    

  ilRC = CheckQueue (ipModId, prpItem) ;
  if (ilRC != RC_SUCCESS)
  {
   if (ilRC != QUE_E_NOMSG)
   {
    dbg(TRACE, "WaitAndCheckQueue: CheckQueue <%d> failed <%d>", mod_id, ilRC) ;
   } /* end of if */
  } /* end of if */
 } while ((ilRC == QUE_E_NOMSG) && (ilWaitCounter < ipTimeout)) ;

 if (ilWaitCounter >= ipTimeout)
 {
  dbg (TRACE, "WaitAndCheckQueue: timeout reached <%d>", ipTimeout) ;
  ilRC = RC_FAIL ;
 } /* end of if */

 return (ilRC) ;

} /* end of WaitAndCheckQueue */




/*****************************************************************************/
static int CheckQueue(int ipModId, ITEM **prpItem)
{
 int     ilRC       = RC_SUCCESS;                      /* Return code */
 int     ilItemSize = 0 ;
 EVENT  *prlEvent   = NULL ;

 ilItemSize = I_SIZE ;

 ilRC =que(QUE_GETBIGNW,ipModId,ipModId,PRIORITY_3,ilItemSize,(char *) prpItem);
 if (ilRC == RC_SUCCESS)
 {
  prlEvent = (EVENT*) ((*prpItem)->text) ;

  switch ( prlEvent->command )
  {
   case    HSB_STANDBY     : dbg(TRACE, "CheckQueue: HSB_STANDBY") ;
                             HandleQueues() ;
                             ctrl_sta = prlEvent->command ;
                            break ;

   case    HSB_COMING_UP   : dbg(TRACE, "CheckQueue: HSB_COMING_UP") ;
                             ctrl_sta = prlEvent->command ;
                            break ;

   case    HSB_ACTIVE      : dbg(TRACE, "CheckQueue: HSB_ACTIVE") ;
                             ctrl_sta = prlEvent->command ;
                            break ;

   case    HSB_ACT_TO_SBY  : dbg(TRACE, "CheckQueue: HSB_ACT_TO_SBY") ;
                             HandleQueues() ;
                             ctrl_sta = prlEvent->command ;
                            break ;

   case    HSB_DOWN        : dbg (TRACE, "CheckQueue: HSB_DOWN") ;
                             ctrl_sta = prlEvent->command ;
                             ilRC = que ( QUE_ACK, 0, ipModId, 0, 0, NULL) ;
                             if ( ilRC != RC_SUCCESS )
                             {        /* handle que_ack error */
                              HandleQueErr(ilRC) ;
                             } /* fi */
                             Terminate() ;
                            break ;

   case    HSB_STANDALONE  : dbg (TRACE, "CheckQueue: HSB_STANDALONE") ;
                             ctrl_sta = prlEvent->command ;
                            break ;

   case    SHUTDOWN        : dbg (TRACE, "CheckQueue: SHUTDOWN") ;   /* Ack */
                             ilRC = que (QUE_ACK, 0, ipModId, 0, 0, NULL) ;
                             if ( ilRC != RC_SUCCESS )
                             {   /* handle que_ack error */
                              HandleQueErr(ilRC) ;
                             } /* fi */
                             Terminate() ;
                            break ;

   case    RESET           : ilRC = Reset() ;
                            break ;

   case    EVENT_DATA      : break ;

   case    TRACE_ON        : dbg_handle_debug(prlEvent->command) ;
                            break ;

   case    TRACE_OFF       : dbg_handle_debug(prlEvent->command) ;
                            break ;

   default                 : dbg (TRACE, "CheckQueue: unknown event") ;
                            break ;

  } /* end switch */

  /* Acknowledge the item */
  ilRC = que (QUE_ACK, 0, ipModId, 0, 0, NULL) ;
  if ( ilRC != RC_SUCCESS )
  {                                    /* handle que_ack error */
   HandleQueErr (ilRC) ;
  } /* fi */
 }
 else
 {
  if (ilRC != QUE_E_NOMSG)
  {
   dbg (TRACE, "CheckQueue: que (QUE_GETBIG,%d,%d,...) failed <%d>", ipModId, ipModId, ilRC) ;
   HandleQueErr (ilRC) ;
  } /* end of if */
 } /* end of if */

 return (ilRC) ;

} /* end of CheckQueue */




/*-----------------------------------------------------------------------*/
/* function :  getTextField ()                                           */
/*             creates a complete information string of the AFT Tab      */
/*             The field VIAL is compressed only AIRPORT shortnames      */
/*             in sequenz                                                */
/*  pars.:                                                               */
/*    ST_EVT_DATA_HDL sSNDARR : includes all information about the AFTTAB*/
/*                                                                  GHe  */
/*-----------------------------------------------------------------------*/
static int getTextField (ST_EVT_DATA_HDL sSND_ARR, char *pcpTxtStr ) 
{
    int  ilRC = RC_SUCCESS ; 
    int  ilactItemInFldLst ;
    int  ilFldLen ; 
    int  ili ; 
    char pclCompleteFldList[2096]; 
    char pclFoundData[2096];
    char pclFoundField[512];
    char pclHelpStr[256]; 
    char pclVial[12]; 
    char pclMCGData[1024];
    char cpDeli[2];
    char cpDeliFldDiv[2];
    char *pclFunc = "getTextField";

    cpDeli[0] = 0x19;            /* char 25 */
    cpDeli[1] = 0x00;
    cpDeliFldDiv[0] = 0x1E;      /* char 30 */
    cpDeliFldDiv[1] = 0x00;      

    /* Fields of flight */
    getFldDataFromStruct (pclCompleteFldList, "#TLX", sSND_ARR) ;  
    strcpy(pcpTxtStr, pclCompleteFldList) ; 

    /* Replace all commas with 0x19 */
    ilFldLen = strlen (pcpTxtStr) ; 
    ili = 0 ; 
    while (ili < ilFldLen) 
    {
        if (pcpTxtStr[ili] == ',') pcpTxtStr[ili] = cpDeli[0]; 
        ili++ ;
    }
    strcat( pcpTxtStr, cpDeli );

    /* Fields of MCGTAB */
/*    memset( pclMCGData, 0, sizeof(pclMCGData) );
    for( ili = 0; ili < igNumMCG; ili++ )
    {
        if( !strcmp( rgMCG[ili].pcTana, "AFT" ) )
            continue;
        strcat( pcpTxtStr, rgMCG[ili].pcFina );
        strcat( pcpTxtStr, cpDeli );

        GetVarValue( pclFoundField, rgMCG[ili].pcFina, NULL );
        strcat( pclMCGData, pclFoundField );
        strcat( pclMCGData, cpDeli );
    }
*/
    /* Divide fields from data */ 
    ili = strlen(pcpTxtStr);
    strcat( pcpTxtStr, cpDeliFldDiv );
    /*dbg (DEBUG, "Fieldlist for TXT field is = <%s>", pcpTxtStr) ; */

    /* Data of flight */
    ilactItemInFldLst = 1 ; 
    do 
    {
        ilFldLen = get_real_item (pclFoundField, pclCompleteFldList, ilactItemInFldLst) ;
        /*dbg (DEBUG, "TXT1 CREAETE : ACT ITEM IS <%ld>", ilactItemInFldLst) ; 
        dbg (DEBUG, "TXT1 CREATE  : ACT FLD IS <%s>", pclFoundField) ; */
        if (ilFldLen > 0)
        {
            getFldDataFromStruct (pclFoundData, pclFoundField, sSND_ARR) ; 
            /*dbg (DEBUG, "TXT1 CREAETE : FOUND DATA <%s>", pclFoundData);*/ 
            if (ilactItemInFldLst > 1) strcat (pcpTxtStr, cpDeli) ; 
            if (strcmp(pclFoundField, "VIAL") == 0) 
            {
                getVialShortTxt (pclFoundData, pclHelpStr) ;  
                strcpy (pclFoundData, pclHelpStr) ;
                dbg (DEBUG, "VIAL SHORT TEXT IS <%s>", pclFoundData) ; 
            } 
            ilRC = RC_SUCCESS ; 
            strcat (pcpTxtStr, pclFoundData) ; 
        } 
        ilactItemInFldLst++ ; 
    } while (ilFldLen > 0) ; 

    ili = strlen(pcpTxtStr);
    strcat( pcpTxtStr, cpDeli );

    /* Data of MCGTAB */
    /*strcat( pcpTxtStr, pclMCGData );*/

    ili = strlen(pcpTxtStr);
    if( ili > 4000 ) 
    {
        /* string is to long for database field */
        dbg (DEBUG, "STRING TOO LONG : <%s>", pcpTxtStr) ;  
        dbg (DEBUG, "LOST THE FOLLOWING INFORMATION: : <%s>", &pcpTxtStr[4000]) ; 
        pcpTxtStr[4000] = 0x00 ; 
    } 
    dbg( DEBUG, "%s: TXT1 <%s>", pclFunc, pcpTxtStr );

    return ilRC ;
} /* getTextField () */



/*----------------------------------------------------------------------*/
/* Functions to generate BRS telexe                                     */
/*----------------------------------------------------------------------*/


static int getBRSTlxTextData (void) 
{
  int   ilRC = RC_SUCCESS ; 
    char  cpCRLF[3] ; 
    char  cpErrStr[255] ; 
  char  clDataStr[2096] ;
  char  clDayStr[256] ; 
  char  clMonthStr[256] ; 
  char  clHlpStr[256] ;
    char  clMonth[64] ;
    char  clFoundField[64] ;
    char  clCompleteStr[64] ; 
  char  clHelpStr[1024] ; 

    cpCRLF[0] = 0x0D ;
    cpCRLF[1] = 0x0A ;
    cpCRLF[2] = 0x0 ;
  cpErrStr[0] = 0x00 ; 

     /* set flight no  like <OA 944> FLD AFT = FLNO */
   clDataStr[0] = 0x00 ; 
     ilRC = getFldDataFromStruct (clDataStr, "FLNO", sBRS_ARR) ; 
   if (ilRC == RC_SUCCESS) 
     {
        sprintf (clCompleteStr, ".F/%s%s", clDataStr, cpCRLF) ; 
    ReplaceFldDataInStruct (clCompleteStr, "FLNO", &sBRS_PRN) ;  
     }
   else 
     {
        if (strlen (cpErrStr) > 0) strcat (cpErrStr, ",") ; 
        strcat (cpErrStr, "FLNO") ; 
   }

   /* set STOD/TIFD date/time */
   /* we need the day and the first 3 letters from the month */
     ilRC = getFldDataFromStruct (clMonthStr, "#MON", sBRS_ARR) ; 
     ilRC = getFldDataFromStruct (clDataStr, "STOD", sBRS_ARR) ;
   strcpy (clDayStr, &clDataStr[6]) ;    
   clDayStr[2] = 0x00 ; 
   /* get month for this (use this as item for the month field) */
   strcpy ( clMonth, &clDataStr[4] ) ; 
   clMonth[2] = 0x00 ; 
   get_real_item (clFoundField, clMonthStr, atoi(clMonth)) ;
   sprintf (clHelpStr, "%s%s", clDayStr, clFoundField) ;
     dbg (DEBUG, "Created STOD is <%s>", clHelpStr) ; 
     if (strlen (clHelpStr) == 5) 
   {
    sprintf (clCompleteStr, ".S/%s%s", clHelpStr, cpCRLF) ; 
    ReplaceFldDataInStruct (clCompleteStr, "STOD", &sBRS_PRN) ;  
   }
     else 
     {
        if (strlen (cpErrStr) > 0) strcat (cpErrStr, ",") ; 
        strcat (cpErrStr, "STOD") ; 
   }
   
     /* set FTYP or TISD ./C */
   clDataStr[0] = 0x00 ; 
     ilRC = getFldDataFromStruct (clDataStr, "FTYP", sBRS_ARR) ; 
   if (clDataStr[0] != 'X') 
     {
    clDataStr[0] = 0x00 ; 
      ilRC = getFldDataFromStruct (clDataStr, "TISD", sBRS_ARR) ; 
   }
     if (ilRC == RC_SUCCESS)  
   {
    sprintf (clCompleteStr, ".C/%s%s", clDataStr, cpCRLF) ; 
    ReplaceFldDataInStruct (clCompleteStr, "FTYP", &sBRS_PRN) ;  
   }
   else 
     {
      if (strlen (cpErrStr) > 0) strcat (cpErrStr, ",") ; 
      strcat (cpErrStr, "FTYP//TISD") ; 
   }

   /* get TIFD additional time is used use moth str from STOA  */
     ilRC = getFldDataFromStruct (clDataStr, "TIFD", sBRS_ARR) ;
   strcpy ( clDayStr, &clDataStr[6]) ;   
   clDayStr[2] = 0x00 ; 
   /* get month for this (use this as item for the month field) */
   strcpy ( clMonth, &clDataStr[4] ) ; 
   clMonth[2] = 0x00 ; 
   get_real_item (clFoundField, clMonthStr, atoi(clMonth)) ;
   sprintf (clHelpStr, "%s%s", clDayStr, clFoundField) ; 
     if (strlen (clHelpStr) == 5)  
   {
    sprintf (clCompleteStr, ".D/%s%s", clHelpStr, cpCRLF) ; 
    ReplaceFldDataInStruct (clCompleteStr, "TIFD", &sBRS_PRN) ;  
   }
   else 
     {
        if (strlen (cpErrStr) > 0) strcat (cpErrStr, ",") ; 
        strcat (cpErrStr, "TIFD") ; 
   }
   
   strcpy ( clHelpStr, &clDataStr[8]) ;      
   clHelpStr[4] = 0x00 ; 
     if (strlen (clHelpStr) == 4)  
   {
    sprintf (clCompleteStr, ".T/%s%s", clHelpStr, cpCRLF) ; 
    ReplaceFldDataInStruct (clCompleteStr, "TIFT", &sBRS_PRN) ;  
   }
   else 
     {
        if (strlen (cpErrStr) > 0) strcat (cpErrStr, ",") ; 
        strcat (cpErrStr, "TIFT") ; 
   }

     /* set help -> field ADID .H/  */
   clDataStr[0] = 0x00 ; 
     ilRC = getFldDataFromStruct (clDataStr, "ADID", sBRS_ARR) ; 
   if (ilRC == RC_SUCCESS)  
   {
    sprintf (clCompleteStr, ".F/%s%s", clDataStr, cpCRLF) ; 
    ReplaceFldDataInStruct (clCompleteStr, "ADID", &sBRS_PRN) ;  
   }
   else 
     {
        if (strlen (cpErrStr) > 0) strcat (cpErrStr, ",") ; 
        strcat (cpErrStr, "ADID") ; 
   }

     /* VIAL in short list */
   clDataStr[0] = 0x00 ; 
     ilRC = getFldDataFromStruct (clDataStr, "VIAL", sBRS_ARR) ; 
   if (ilRC == RC_SUCCESS)
     { 
    getVialShortTxt (clDataStr, clHelpStr) ;
    sprintf (clCompleteStr, ".R/%s%s", clHelpStr, cpCRLF) ; 
    ReplaceFldDataInStruct (clCompleteStr, "VIAL", &sBRS_PRN) ;  
   } 
   else 
     {
        if (strlen (cpErrStr) > 0) strcat (cpErrStr, ",") ; 
        strcat (cpErrStr, "ADID") ; 
   }

   ilRC = RC_SUCCESS ; 
     if (strlen (cpErrStr) > 0)
     {
        ilRC = RC_FAIL ;
        dbg (DEBUG, "------------------------------------------------") ; 
        dbg (DEBUG, "TELEX CREATOR --- BRS --------------------------") ;  
        dbg (DEBUG, "ERRS DETECTED in FIELDS: -----------------------") ; 
        dbg (DEBUG, "FIELDS = <%s> ", cpErrStr ) ;  
        dbg (DEBUG, "------------------------------------------------") ; 
   }

  return ilRC ; 
} /* end getBRSTlxTextData () */ 









/*-----------------------------------------------------------------------*/
/* function :  getBRSHeaderInfo ()                                       */
/*             interprets from the config file the header information    */
/*             into the struct sBRS_PRN                                  */
/*  pars.:                                                               */
/*          ST_EVT_DATA_HDL sBRS_ARR : the struct with telex datas       */
/*          ST_EVT_DATA_HDL sBRS_PRN : the struct with header info       */
/*                                                                  GHe  */
/*-----------------------------------------------------------------------*/
static int getBRSHeaderInfo (ST_EVT_DATA_HDL sBRS_ARR, 
                             ST_EVT_DATA_HDL *sBRS_PRN ) 
{
  int      ilRC = RC_SUCCESS ;
  char     clFoundData [2096] ; 
  char     cpCRLF[3];
  char     cpFieldBRS[64] ;
  char     cpFieldPrnNo[64] ;
    char     cpHelpStr[128] ; 
    int      ilEnd ; 
    int      ilActPrnPos ;  
    int      ilActFieldPos ;  

    cpCRLF[0] = 0x0D ;
    cpCRLF[1] = 0x0A ;
    cpCRLF[2] = 0x0 ;
  ilActPrnPos = 1 ; 
    ilActFieldPos = 1 ;
    ilEnd = 0 ; 
    do 
    {
   sprintf (cpFieldBRS, "@%#03d", ilActFieldPos) ;  
   ilRC = getFldDataFromStruct (clFoundData, cpFieldBRS, sBRS_ARR) ;  
   ilActFieldPos++ ;
   if ((strcmp(clFoundData, "BLOCK_BEGIN") == 0) || 
             (strcmp (clFoundData, "BLOCK_END") == 0 ) || (ilRC != RC_SUCCESS) ) 
     {
    if (strcmp(clFoundData, "BLOCK_END") == 0) ilEnd = 1 ;
    if (ilRC != RC_SUCCESS) ilEnd = 1 ;
   }
     else 
   {       /* write this line to the header */
    sprintf (cpFieldPrnNo, "H%#03d", ilActPrnPos) ;
        sprintf (cpHelpStr, "%s%s", clFoundData, cpCRLF) ; 
    ReplaceFldDataInStruct (cpHelpStr, cpFieldPrnNo, sBRS_PRN) ;  
    ilActPrnPos++ ;
     } 
   if (ilActFieldPos == 15) ilEnd = 1 ; 
    } while (ilEnd == 0) ;

    ilRC = RC_SUCCESS ; 
  return (ilRC) ;

}  /* getBRSHeaderInfo() */






static int getNextBRSFilename (char *cpNxtFilename )
{
 int      ilRC = RC_SUCCESS ; 
 short    sql_cursor ;
 short    sql_fkt ;
 char     clSqlStatement[2048] ;
 char     clData [4096] ; 
 long     int ilActNo ; 
 long     int ilMaxNo ;
 long     int ilMinNo ; 
 char     clDataItem[128] ; 
 int      ilItems ; 
 char     clUsedFlds[1024] ;
 char     clValStr[4096] ;
 int      ilDBRc ; 

 /* select from the database the last number */
 /* NUMTAB : ACNU = actualy number */
 /*          FLAG = ???            */
 /*          HOP  = Flughafen      */
 /*          KEYS = Actionsname    */
 /*          MAXN = max number     */
 /*          MINN = min number     */

 dbg (DEBUG, "getNextBRSFilename >>") ; 
 if (ilRC == RC_SUCCESS)    /* then sending telex */
 {
  dbg (DEBUG, "getNextBRSFilename: SQL SELECT PART NUMTAB (TELEXE)") ; 
  sprintf (clSqlStatement,
                     "SELECT ACNU,MAXN,MINN FROM NUMTAB WHERE KEYS = 'TELEXE'" ) ; 
  sql_cursor = 0 ;
  sql_fkt = START ;
  ilDBRc = sql_if (sql_fkt, &sql_cursor, clSqlStatement, clData) ;
  close_my_cursor (&sql_cursor) ;     /* close select cursor */
  if (ilDBRc != DB_SUCCESS)
  {
   /* SPECIAL CASE USE TELEXE AT 1.ST TIME */
   /* -> insert telexe into the database   */
   dbg(DEBUG, "getNextBRSFilename: SQL INSERT PART NUMTAB (TELEXE)") ; 
     dbg(DEBUG, "getNextBRSFileName: create new record KEYS=TELEXE for NumTab");
   strcpy (clUsedFlds, "ACNU,FLAG,HOPO,KEYS,MAXN,MINN") ;
   ilRC = getItemCnt (clUsedFlds, &ilItems) ;
   ilRC = prepSqlValueStr (ilItems, clValStr) ;

   /* put in information for SQL statement */
   sprintf (clSqlStatement, "INSERT INTO NUMTAB (%s) %s",clUsedFlds, clValStr);
   sql_cursor = 0 ;
   sql_fkt = START ;

   /* flds : ACNU,FLAG,HOPO,KEYS,MAXN,MINN   */
   /* len  :  10   20    3   10   10   10    */
   sprintf (clValStr, "%#010ld,%s,%s,%s,%#010ld,%#010ld",
                      1,                       /* start number is 1 */
                                        "",                      /* 20 Blanks */
                      pcgHomeHopo,             /*  3 chars like ATH */ 
                      "TELEXE",                /* 10 chars Home Airport */ 
                      999999999,               /* 10 chars max number */
                                          1                        /* 10 chars min number */
                   ) ;
   dbg (DEBUG, "SELECT SQL VALUE STRING IS: <%s>", clValStr) ;
   delton (clValStr) ;
   ilDBRc = sql_if (sql_fkt, &sql_cursor, clSqlStatement, clValStr) ;
   if (ilDBRc = DB_SUCCESS) 
   {
    dbg (TRACE, "-------------------------------------------------") ;
    dbg (TRACE, "SQLInsertRecord : CANT INSERT RECORD stat.: <%s>",
                                                     clSqlStatement) ;
    dbg (TRACE, "-------------------------------------------------") ;
    ilRC = RC_FAIL ;
    if (gl_updateTest == 1) ilRC = RC_SUCCESS ;
   }
   else
   {
    commit_work() ;
    ilRC = RC_SUCCESS ;
        strcpy (cpNxtFilename, "0000000001") ; 
   }
   close_my_cursor (&sql_cursor) ; /* close insert cursor */
  } /* special case set data at first time */
  else      /* found a telex number */
  {
   dbg(DEBUG, "getNextBRSFilename: SQL UPDATE PART NUMTAB (TELEXE)") ; 
   ilRC = BuildItemBuffer(clData, "ACNU,MAXN,MINN",0,",") ;
   get_real_item (clDataItem, clData, 1) ;
   ilActNo = atol (clDataItem) ; 
   get_real_item (clDataItem, clData, 2) ;
   ilMaxNo = atol (clDataItem) ; 
   get_real_item (clDataItem, clData, 3) ;
     ilMinNo = atol (clDataItem) ; 
   ilActNo++ ; 
     if (ilActNo > ilMaxNo) 
     {
      ilActNo = ilMinNo ; 
   } ;
   sprintf (cpNxtFilename, "%#010ld", ilActNo) ;  
   dbg(DEBUG,"read act=<%ld>, max=<%ld>, min=<%ld>",ilActNo,ilMaxNo,ilMinNo) ; 
     /* update the new actNo into the database */
   strcpy (clUsedFlds, "ACNU") ;

   /* put in information for SQL statement */
   sprintf (clSqlStatement,
                     "UPDATE NUMTAB SET ACNU =:VFLD1 WHERE KEYS='TELEXE'" ) ;
   sql_cursor = 0 ;
   sql_fkt = START ;

   /* flds : ACNU,FLAG,HOPO,KEYS,MAXN,MINN   */
   /* len  :  10   20    3   10   10   10    */
   sprintf (clValStr, "%#010ld", ilActNo) ; 
   dbg (DEBUG, "UPDATE SQL VALUE STRING IS: <%s>", clValStr) ;
   delton (clValStr) ;
   ilDBRc = sql_if (sql_fkt, &sql_cursor, clSqlStatement, clValStr) ;
   if (ilDBRc != DB_SUCCESS) 
   {
    dbg (TRACE, "-------------------------------------------------") ;
    dbg (TRACE, "SQL UPDATE Record : CANT UPDATE RECORD stat.: <%s>",
                                                    clSqlStatement) ;
    dbg (TRACE, "-------------------------------------------------") ;
    ilRC = RC_FAIL ;
   }
   else
   {
    commit_work() ;
    ilRC = RC_SUCCESS ;
     } 
   close_my_cursor (&sql_cursor) ;    /* close update cursor */
  } /* else if must send telex */
 } /* time check done */
 return (ilRC) ;

} /* end getNextBRSFilename()  */





static int createBRSTelex (void) 
{
  int   ilRC = RC_SUCCESS ; 
  char  cpWorkFilename[256] ; 
  char  cpFilename[128] ; 
  char  cpPath[128] ; 
    char  cpTmpFilename[128] ; 
    FILE  *tmpFile ;
  char  clDataStr[2096] ;
  char  clMonthStr[256] ; 
  char  clHlpStr[256] ;
    char  clFoundField[64] ; 
  int   ilPtr ; 
  char  cpField[64] ; 
  char  cpSystemCall[1024] ; 

  dbg (DEBUG, "createBRSTelex >>") ; 
    
    ilRC = getNextBRSFilename (cpFilename ) ; 
  ilRC = getFldDataFromStruct (cpPath, "#FIL", sBRS_ARR) ;  
  sprintf (cpWorkFilename, "%s%s.snd", cpPath, cpFilename) ; 
    sprintf (cpTmpFilename, "%s%s.tmp", cpPath, cpFilename) ; 
  dbg (DEBUG, "createBRSTelex: filename new is (%s)", cpWorkFilename ) ; 

  ilRC = getBRSTlxTextData () ;   /* now the BRS info in sBRS_PRN */ 
  if (ilRC == RC_SUCCESS) 
    {
   tmpFile = fopen (cpTmpFilename, "w") ; 
     if (tmpFile != NULL) 
     {
    dbg (DEBUG, "createBRSTelex FILE writing <%s>",cpTmpFilename) ; 
    /* HEADER INFO for the BRS TELEX */
    ilPtr = 1 ;
      do 
      {
       sprintf (cpField, "H%#03d", ilPtr) ;  
       ilRC = getFldDataFromStruct (clDataStr, cpField, sBRS_PRN) ; 
     if (clDataStr[0] != 0x00) fprintf (tmpFile, clDataStr) ; 
     ilPtr++ ; 
    } while ((ilPtr < 15) && (clDataStr[0] != 0x00)) ; 

        ilRC = getFldDataFromStruct (clDataStr, "BEGI", sBRS_PRN) ; 
    fprintf (tmpFile, clDataStr) ; 
      ilRC = getFldDataFromStruct (clDataStr, "FLNO", sBRS_PRN) ; 
    fprintf (tmpFile, clDataStr) ; 
      ilRC = getFldDataFromStruct (clDataStr, "STOD", sBRS_PRN) ; 
    fprintf (tmpFile, clDataStr) ; 
      ilRC = getFldDataFromStruct (clDataStr, "FTYP", sBRS_PRN) ; 
    fprintf (tmpFile, clDataStr) ; 
      ilRC = getFldDataFromStruct (clDataStr, "TIFD", sBRS_PRN) ; 
    fprintf (tmpFile, clDataStr) ; 
      ilRC = getFldDataFromStruct (clDataStr, "TIFT", sBRS_PRN) ; 
    fprintf (tmpFile, clDataStr) ; 
      ilRC = getFldDataFromStruct (clDataStr, "ADID", sBRS_PRN) ; 
    fprintf (tmpFile, clDataStr) ; 
      ilRC = getFldDataFromStruct (clDataStr, "VIAL", sBRS_PRN) ; 
    fprintf (tmpFile, clDataStr) ; 

      fflush (tmpFile) ; 
      fclose (tmpFile) ;
    setTlxTableDat (&sTLX_ARR, &sBRS_ARR ) ; 
      /* TODO set telex status to S */
      SQLInsertTlxTab (sTLX_ARR) ; 
      sprintf (cpSystemCall, "mv %s %s", cpTmpFilename, cpWorkFilename) ; 
      system (cpSystemCall) ; 
    dbg (DEBUG, "system call = <%s>", cpSystemCall) ; 
   }
   else 
     {
    dbg (DEBUG, "createBRSTelex FILE writing ERROR !!!") ; 
    dbg (DEBUG, "Cant create BRS filename <%s>", cpTmpFilename) ; 
      ilRC = RC_FAIL ; 
   } 
  } /* if RC_SUCCESS */ 

  return ilRC ; 
} /* end createBRSTelex */



static int handleBRS_Call (char *cpDateFrom, char *cpDateTo )
{
 
 int   ilRC = RC_SUCCESS ;
 int   il ;
 int   ilLen ; 
 int   ilGetRc ;
 int   ilMoreRecs ; 
 short sql_cursor ;
 short sql_fkt ;
 char  clSqlStatement[2048] ;
 char  clCondFld[2048] ; 
 char  clCondData[2048] ; 
 char  clSelFlds[2048] ; 
 char  clData [4096] ; 
 char  clDataItem [1024] ; 
 char  clFldItem[1024] ; 

 /* test if the date and time correct, for sending telexs */
 clSelFlds[0]  = 0x00 ;    /* create sql field list */ 
 clCondData[0] = 0x00 ; 
 ilRC=createFldAndDataLst(clSelFlds, clCondData, C_SELECT, sBRS_ARR) ;
 sprintf (clSqlStatement,
                "SELECT %s FROM AFTTAB WHERE TIFD BETWEEN '%s' AND '%s'",
                clSelFlds, cpDateFrom, cpDateTo) ;  
 dbg (DEBUG, "handlBRSCall: SQL SELECT STATEMENT = <%s>", clSqlStatement) ;
 sprintf (clData, clCondData) ;        /* prepare data pool */
 sql_cursor = 0 ;
 sql_fkt = START ;
 ilMoreRecs = 0 ;  
 do 
 {
  ilGetRc = sql_if (sql_fkt, &sql_cursor, clSqlStatement, clData) ;
  if (ilGetRc == DB_SUCCESS)
  {
     ilMoreRecs++ ; 
   ilRC = BuildItemBuffer(clData,clSelFlds,0,",") ;
   ilRC = RC_SUCCESS ;
   /* telex record */ 
   dbg (DEBUG, "SQLSelectRecord: find rec=<%ld> data = <%s>",
                                ilMoreRecs, clData) ;
   il = 1 ; 
   do 
     {
    ilLen = get_real_item (clFldItem, clSelFlds, il) ;
      if (ilLen > 0)     
      {
     ilLen = get_real_item (clDataItem, clData, il) ;
       ilRC = ReplaceFldDataInStruct (clDataItem, clFldItem, &sBRS_ARR) ;
      }
      else il = -1 ; 
      il++ ; 
   } while (il > 0) ; 
   
   ilRC = createBRSTelex () ; 
     /* call generate telex */
  }  /* if found next record DB_SUCCESS */
  else
  {
   dbg (DEBUG, "handleBRSCall: All records for the time range read") ; 
   ilMoreRecs = 0 ;   /* all telex records read */
  }
  sql_fkt = NEXT ; 
 } while (ilMoreRecs != 0) ; 

 close_my_cursor (&sql_cursor) ;
 return (ilRC) ;

} /* handleBRSCall () */








static int startWhileTimeReached (ST_EVT_DATA_HDL *stAct, char *cpDateFrom,
                                                                char *cpDateTo)
{
 int       ilRC = RC_SUCCESS ; 
 char      clDateFrom [64] ; 
 char      clDateTo   [64] ;
 char      clActionDate[64] ; 
 DATEINFO  stlDateFrom ; 
 DATEINFO  stlDateTo; 
 int       ilDayOffset ; 
 char      cpHStr[64] ;
 long int  ilActionDay ; 
 long int  ilActionTime ; 
 long int  ilToDay ; 
 long int  ilToTime ; 
 int       ilTimeInt ;  
 int       ilStatus ;  


 ilRC = getFldDataFromStruct (cpHStr, "#DAT", *stAct) ; 
 ilDayOffset = atoi(cpHStr) ; 
 ilRC = getFldDataFromStruct (cpHStr, "#TIM", *stAct) ; 
 ilRC = getFldDataFromStruct (clDateFrom, "#SDT", *stAct) ;  
 ilRC = interpretDateInfo (clDateFrom, &stlDateFrom) ;
 ilRC = GetLocalTimeStamp(clDateTo) ;
 ilRC = interpretDateInfo (clDateTo, &stlDateTo) ;
 while (ilDayOffset > 0) 
 {
  ilRC = incDateAtOne (clDateFrom, &stlDateFrom, &ilStatus ) ;
  ilDayOffset-- ; 
 }
 if (cpHStr[0] != 0x00) clDateFrom[8] = cpHStr[0] ; 
 if (cpHStr[1] != 0x00) clDateFrom[9] = cpHStr[1] ;
 if (cpHStr[2] != 0x00) 
 {
  if (cpHStr[3] != 0x00) clDateFrom[10] = cpHStr[3] ; 
  if (cpHStr[4] != 0x00) clDateFrom[11] = cpHStr[4] ; 
 } 
 
 /* get new offset date */
 /* compute the dates as long */ 
 ilActionTime = atol (&clDateFrom[8]) ; 
 clDateFrom[8] = 0x00 ; 
 ilActionDay = atol (clDateFrom)  ; 
 ilToTime = atol (&clDateTo[8]) ; 
 clDateTo[8] = 0x00 ; 
 ilToDay = atol (clDateTo)  ; 

 ilTimeInt = 0 ; 
 if (ilActionDay > ilToDay) ilTimeInt = 1 ; 
 if ((ilActionDay == ilToDay) && (ilActionTime >= ilToTime)) ilTimeInt = 1 ;  
 
 
 dbg (DEBUG, "The actualy time with offset is : <%8ld> <%6ld>", ilActionDay,
                                                                                                                            ilActionTime) ;
 dbg (DEBUG, "The server time is <%8ld> <%6ld>", ilToDay, ilToTime) ;
 if (ilTimeInt == 1)
 {
    dbg (DEBUG, "The Telex sending is started!") ;
 }
 else
 {
    dbg (DEBUG, "No Telex sending is started!") ;
 }

 if (ilTimeInt == 1) return RC_SUCCESS ;  /* start telex sending */
 return RC_FAIL ;  /* time not reached */
} /* startWhileTimeReached () */



static void ForwardTelexToFdihdl(char *pcpSelKey, char *pcpData)
{
  SendCedaEvent (8450,0,pcgDestName,pcgRecvName,pcgTwStart,pcgTwEnd,
                 "FDI", "TLXTAB", pcpSelKey,"",pcpData, "", 4, RC_SUCCESS) ; 
  SendCedaEvent (que_out,0,"","","0",pcgTwEnd,
                 "TLX", "TLXTAB", "TELEXE","","", "", 4, RC_SUCCESS) ; 
  return;
} 


/******************************************************************************/
/******************************************************************************/
static void TestReplace(void)
{
  char pclText[1024] = "";
  char pclPatt[1024] = "";
  char pclData[1024] = "";
  long llTmpLen = 0;
  int ilRetVal = 0;
  int ilMinute = 0;

  dbg(TRACE,"====================================================");
  dbg(TRACE,"TEST: REPLACE (0)");
  dbg(TRACE,"=================");
  strcpy(pclText,"SQ 123");
  strcpy(pclPatt," ");
  strcpy(pclData,"");
  dbg(TRACE,"TEST: SEARCH  STRING = <%s>",pclPatt);
  dbg(TRACE,"TEST: REPLACE STRING = <%s>",pclData);
  SetStringValue(&rgMyDataString, pclText);
  ReplacePatternInString(&rgMyDataString, pclPatt, pclData);
  dbg(TRACE,"TEST: RESULT  STRING = \n----%s\n----",rgMyDataString.Value);

  sprintf(pclText,"\n\nMVT\n"
                  "[FLNO]/[AIRB|DD]\n"
                  "AD[OFBL|HHMM]/[AIRB|HHMM]\n"
                  "AO[OFBL|HHMM]/[AIRB|HHMM]\n"
                  "AE[OFBL|DDHHMM]/[AIRB|DDHHMM]\n"
                  "SEAS [OFBL]/[AIRB]\n"
                  "SI [FLNO]/[AIRB|DD]\n");

  SetStringValue(&rgMyDataString, pclText);
  dbg(TRACE,"TEST: BASIC TEMPLATE = %s",rgMyDataString.Value);

  dbg(TRACE,"====================================================");
  dbg(TRACE,"TEST: REPLACE (1)");
  dbg(TRACE,"=================");
  strcpy(pclPatt,"[FLNO]");
  strcpy(pclData,"SQ123");
  dbg(TRACE,"TEST: SEARCH  STRING = <%s>",pclPatt);
  dbg(TRACE,"TEST: REPLACE STRING = <%s>",pclData);
  ReplacePatternInString(&rgMyDataString, pclPatt, pclData);
  dbg(TRACE,"TEST: RESULT  STRING = \n----%s\n----",rgMyDataString.Value);

  dbg(TRACE,"====================================================");
  dbg(TRACE,"TEST: REPLACE (2)");
  dbg(TRACE,"=================");
  strcpy(pclPatt,"[AIRB|DD]");
  strcpy(pclData,"17");
  dbg(TRACE,"TEST: SEARCH  STRING = <%s>",pclPatt);
  dbg(TRACE,"TEST: REPLACE STRING = <%s>",pclData);
  ReplacePatternInString(&rgMyDataString, pclPatt, pclData);
  dbg(TRACE,"TEST: RESULT  STRING = \n----%s\n----",rgMyDataString.Value);
  
  dbg(TRACE,"====================================================");
  dbg(TRACE,"TEST: REPLACE (3)");
  dbg(TRACE,"=================");
  strcpy(pclPatt,"[AIRB|HHMM]");
  strcpy(pclData,"1245");
  dbg(TRACE,"TEST: SEARCH  STRING = <%s>",pclPatt);
  dbg(TRACE,"TEST: REPLACE STRING = <%s>",pclData);
  ReplacePatternInString(&rgMyDataString, pclPatt, pclData);
  dbg(TRACE,"TEST: RESULT  STRING = \n----%s\n----",rgMyDataString.Value);
  
  dbg(TRACE,"====================================================");
  dbg(TRACE,"TEST: REPLACE (4)");
  dbg(TRACE,"=================");
  strcpy(pclPatt,"[OFBL|HHMM]");
  strcpy(pclData,"1235");
  dbg(TRACE,"TEST: SEARCH  STRING = <%s>",pclPatt);
  dbg(TRACE,"TEST: REPLACE STRING = <%s>",pclData);
  ReplacePatternInString(&rgMyDataString, pclPatt, pclData);
  dbg(TRACE,"TEST: RESULT  STRING = \n----%s\n----",rgMyDataString.Value);
  
  dbg(TRACE,"====================================================");
  dbg(TRACE,"TEST: CHECK TEMPLATE ");
  dbg(TRACE,"====================");

  (void) CedaGetKeyItem(pclData, &llTmpLen, rgMyDataString.Value, "[", "]", TRUE);
  if (llTmpLen > 0)
  {
    dbg(TRACE,"TEST: FOUND UNRESOLVED [%s] IN TELEX TEXT",pclData);
  }
  else
  {
    dbg(TRACE,"TEST: RESULTING TELEX TEXT IS CLEAN");
  }
  dbg(TRACE,"====================================================");
  dbg(TRACE,"");
  dbg(TRACE,"====================================================");
  dbg(TRACE,"TEST: TIME DIFFERENCE ");
  dbg(TRACE,"=====================");
  strcpy(pclPatt,"20091217124553");
  strcpy(pclData,"20091217123500");
  dbg(TRACE,"TEST: CEDA DATE/TIME STRG 1 = <%s>",pclPatt);
  dbg(TRACE,"TEST: CEDA DATE/TIME STRG 2 = <%s>",pclData);
  ilRetVal = DateDiffToMin(pclPatt,pclData,&ilMinute);
  dbg(TRACE,"TEST: CEDA DATE/TIME RESULT = %d",ilMinute);
  dbg(TRACE,"====================================================");

  return;
}

static void SetStringValue(STR_DESC* prpStringBuffer, char *pcpNewString)
{
  int ilLen = 0;
  ilLen = strlen(pcpNewString);

  if (prpStringBuffer->AllocatedSize <= ilLen)
  {
    prpStringBuffer->Value = realloc(prpStringBuffer->Value, ilLen+1024);
    prpStringBuffer->AllocatedSize = ilLen+1024;
  }

  strcpy(prpStringBuffer->Value, pcpNewString);
  prpStringBuffer->UsedLen = ilLen;
  
  return;
}

static void ReplacePatternInString(STR_DESC* prpString, char *pcpSearch, char *pcpReplace)
{
  int ilSearchLen = 0;
  int ilReplaceLen = 0;
  int ilNewLen = 0;
  int ilLeftPos = 0;
  int ilRightPos = 0;
  int ilCurOffs = 0;
  char *pclPos = NULL;
  char *pclPtr1 = NULL;
  char *pclPtr2 = NULL;

  if (strcmp(pcpSearch,pcpReplace) == 0)
  {
    return;
  }

  ilSearchLen = strlen(pcpSearch);
  ilReplaceLen = strlen(pcpReplace);

  pclPos = prpString->Value;
  while (pclPos != NULL)
  {
    pclPos = strstr(pclPos,pcpSearch);
    if (pclPos != NULL)
    {
      if (ilReplaceLen == ilSearchLen)
      {
        strncpy(pclPos,pcpReplace,ilReplaceLen);
        pclPos += ilReplaceLen;
      }
      else if (ilReplaceLen < ilSearchLen)
      {
        strncpy(pclPos,pcpReplace,ilReplaceLen);
        ilLeftPos = pclPos - prpString->Value + ilReplaceLen;
        ilRightPos = pclPos - prpString->Value + ilSearchLen;
        StrgShiftLeft(ilLeftPos,ilRightPos,prpString->Value);
        prpString->UsedLen = prpString->UsedLen - ilSearchLen + ilReplaceLen;
        pclPos += ilReplaceLen;
      }
      else
      {
        ilNewLen = prpString->UsedLen - ilSearchLen + ilReplaceLen;
        if (ilNewLen >= prpString->AllocatedSize)
        {
          dbg(TRACE,"MUST REALLOC MY STRING LENGTH");
          pclPtr1 = prpString->Value;
          prpString->Value = realloc(prpString->Value, ilNewLen+1024);
          prpString->AllocatedSize = ilNewLen+1024;
          pclPtr2 = prpString->Value;
          if (pclPtr2 != pclPtr1)
          {
            dbg(TRACE,"MUST RECOVER MY POS POINTER");
            ilCurOffs = pclPos - pclPtr1;
            pclPos = pclPtr2;
            pclPos += ilCurOffs;
          }
        }
        ilLeftPos = pclPos - prpString->Value + ilSearchLen;
        ilRightPos = pclPos - prpString->Value + ilReplaceLen;
        StrgShiftRight(ilLeftPos,ilRightPos,prpString->Value);
        strncpy(pclPos,pcpReplace,ilReplaceLen);
        prpString->UsedLen = prpString->UsedLen - ilSearchLen + ilReplaceLen;
        pclPos += ilReplaceLen;
      }
    }
  }
  return;
}


/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* MEI: 04-DEC-2009
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
static int RunSQL( char *pcpSelection, char *pcpData )
{
    int ilRc = DB_SUCCESS;
    short slSqlFunc = 0;
    short slSqlCursor = 0;
    char pclErrBuff[128];
    char *pclFunc = "RunSQL";

    ilRc = sql_if ( START, &slSqlCursor, pcpSelection, pcpData );
    close_my_cursor ( &slSqlCursor );

    if( ilRc == DB_ERROR )
    {
        get_ora_err( ilRc, &pclErrBuff[0] );
        dbg( TRACE, "<%s> Oracle Error <%s>", pclFunc, &pclErrBuff[0] );
    }
    return ilRc;
}


/*************************************************************************/
/* Time2 - Time1 [must be UTC timing] (return number of minutes)         */
/*************************************************************************/
static int TimeDiff( char *pcpTime1, char *pcpTime2, int *ipTimeDiff )
{
    struct tm rlTm;
    time_t tlNow;
    time_t tlTime1;
    time_t tlTime2;
    char pclTime1[16];
    char pclTime2[16];
    char pclTmpStr[16];
    char *pclFunc = "TimeDiff";

    strncpy( pclTime1, pcpTime1, CEDA_DATE_LEN );
    strncpy( pclTime2, pcpTime2, CEDA_DATE_LEN );
    str_trm_all( pclTime1, " ", TRUE );
    str_trm_all( pclTime2, " ", TRUE );

    if( strlen(pclTime1) != CEDA_DATE_LEN || strlen(pclTime2) != CEDA_DATE_LEN )
        return RC_FAIL;

    tlNow = time(0);
    gmtime_r( &tlNow, &rlTm );

    sprintf( pclTmpStr, "%4.4s", pclTime1 );     rlTm.tm_year = atoi(pclTmpStr) - 1900;
    sprintf( pclTmpStr, "%2.2s", &pclTime1[4] ); rlTm.tm_mon  = atoi(pclTmpStr) - 1;
    sprintf( pclTmpStr, "%2.2s", &pclTime1[6] ); rlTm.tm_mday = atoi(pclTmpStr);
    sprintf( pclTmpStr, "%2.2s", &pclTime1[8] ); rlTm.tm_hour = atoi(pclTmpStr);
    sprintf( pclTmpStr, "%2.2s", &pclTime1[10] );rlTm.tm_min  = atoi(pclTmpStr);
    sprintf( pclTmpStr, "%2.2s", &pclTime1[12] );rlTm.tm_sec  = atoi(pclTmpStr);
    tlTime1 = mktime( (struct tm *)&rlTm );

    memset( &rlTm, 0, sizeof(struct tm) );
    gmtime_r( &tlNow, &rlTm );

    sprintf( pclTmpStr, "%4.4s", pclTime2 );     rlTm.tm_year = atoi(pclTmpStr) - 1900;
    sprintf( pclTmpStr, "%2.2s", &pclTime2[4] ); rlTm.tm_mon  = atoi(pclTmpStr) - 1;
    sprintf( pclTmpStr, "%2.2s", &pclTime2[6] ); rlTm.tm_mday = atoi(pclTmpStr);
    sprintf( pclTmpStr, "%2.2s", &pclTime2[8] ); rlTm.tm_hour = atoi(pclTmpStr);
    sprintf( pclTmpStr, "%2.2s", &pclTime2[10] );rlTm.tm_min  = atoi(pclTmpStr);
    sprintf( pclTmpStr, "%2.2s", &pclTime2[12] );rlTm.tm_sec  = atoi(pclTmpStr);
    tlTime2 = mktime( (struct tm *)&rlTm );

    *ipTimeDiff = (tlTime2 - tlTime1) /SECONDS_PER_MINUTE;
    return RC_SUCCESS;
} /* TimeDiff */

static int MVTTelexSending( ST_EVT_DATA_HDL pAFT_RCVNEW, char cpStatus )
{
    int ilAuto = TRUE;
    int ilDelay = FALSE;
    int ilManSI = FALSE;
    int ilManPX = FALSE;
    int ilRC;
    int ilTimeLapse;
    int ilMinDiff;
    int ili;
    int ilCnt;
    int ilLen;
    int ilCustomTemplate;
    int ilNumTAddr;
    int ilNumEAddr;
    char clADID;
    char pclSqlBuf[1024];
    char pclSqlFields[1024];
    char pclSqlData[8008];
    char pclTimeBase[16];
    char pclDataItem[1024];
    char pclFldItem[56];
    char pclUAFT[12];
    char pclTemplate[1024];
    char pclSubType[4];
    char pclTelex[4004];
    char pclTelexAddr[1024];
    char pclEmailAddr[2048];
    char pclTmpStr[4096];
    char pclCurrentTime[20];
    char *pclFunc = "MVTTelexSending";


    /* Check whether user wants it to be AUTO */
    getFldDataFromStruct (pclDataItem, "AUTO", sMTPREC); 
    if( pclDataItem[0] != 'Y' )
    {
        dbg( DEBUG, "%s: User set it to Manual", pclFunc );
        ilAuto = FALSE;
    }

    /* Check whether SI is mandatory */
    getFldDataFromStruct (pclDataItem, "MASI", sMTPREC); 
    if( pclDataItem[0] == 'Y' )
    {
        dbg( DEBUG, "%s: Mandatory SI. Send MVT manually", pclFunc );
        ilAuto = FALSE;
        ilManSI = TRUE;
    }

    ilRC = getFldDataFromStruct (pclUAFT, "URNO", pAFT_RCVNEW); 
    if( ilRC != RC_SUCCESS )
    {
        dbg( TRACE, "%s: Invalid UAFT received.", pclFunc );
        return RC_FAIL;
    }
    dbg( TRACE, "%s: UAFT <%s>", pclFunc, pclUAFT );

    getFldDataFromStruct(pclTmpStr, "ADID", pAFT_RCVNEW);
    clADID = pclTmpStr[0];

    /* Was the dep flight delayed? */
    if( clADID == 'D' )
    {
        ilDelay = FALSE;
        ilTimeLapse = 0;

        getFldDataFromStruct (pclDataItem, "DELA", sMTPREC); 
        ilTimeLapse = atoi(pclDataItem);
        dbg( DEBUG, "%s: Delay Tolerance <%s>-<%d>", pclFunc, pclDataItem, ilTimeLapse );
        
        ilRC = getFldDataFromStruct (pclTimeBase, "AIRB", pAFT_RCVNEW); 
        if( ilRC == RC_SUCCESS )
        {
            dbg( DEBUG, "%s: AIRB <%s>", pclFunc, pclTimeBase );
            ilRC = getFldDataFromStruct (pclDataItem, "STOD", pAFT_RCVNEW); 
            if( ilRC == RC_SUCCESS )
            {
                dbg( DEBUG, "%s: STOD <%s>", pclFunc, pclDataItem );
                ilRC = TimeDiff( pclDataItem, pclTimeBase, &ilMinDiff );
                dbg( DEBUG, "%s: AIRB-STOD = <%d>", pclFunc, ilMinDiff );
                if( ilRC == RC_SUCCESS && ilMinDiff >= ilTimeLapse )
                {
                    dbg( DEBUG, "%s: Delayed flight.", pclFunc );
                    ilDelay = TRUE;
                }
            }
        }
        if( ilDelay == TRUE )
        {
            GetDelayInfo( pclUAFT );
            ilAuto = FALSE;
        }
    }

    /* Check whether Pax is mandatory */
    getFldDataFromStruct (pclDataItem, "MAPX", sMTPREC); 
    if( pclDataItem[0] == 'Y' )
    {
        ilManPX = TRUE;
        dbg( DEBUG, "%s: Mandatory PX.", pclFunc );
    }

    /* Get flight record from AFTTAB */
    memset( pclSqlFields, 0, sizeof(pclSqlFields) );
    memset( pclSqlData, 0, sizeof(pclSqlData) );

    ilRC=createFldAndDataLst(pclSqlFields, pclSqlData, C_SELECT, sMVT_ARR) ;
    if( ilRC != RC_SUCCESS )
    {
        dbg( TRACE, "%s: Error in creating AFTTAB field list", pclFunc );
        return RC_FAIL;
    }
    sprintf( pclSqlBuf, "SELECT %s FROM AFTTAB WHERE URNO = %s", pclSqlFields, pclUAFT );  
    dbg( TRACE, "%s: SelAFT <%s>", pclFunc, pclSqlBuf );

    ilRC = RunSQL( pclSqlBuf, pclSqlData );
    if( ilRC != DB_SUCCESS )
    {
        dbg( TRACE, "%s: Error in selecting AFTTAB record", pclFunc );
        return RC_FAIL;
    }
    BuildItemBuffer( pclSqlData, pclSqlFields, 0, "," );

    ilCnt = get_no_of_items( pclSqlFields );
    for( ili = 1; ili <= ilCnt; ili++ )
    {
        get_real_item( pclFldItem, pclSqlFields, ili );
        get_real_item( pclDataItem, pclSqlData, ili );
        ReplaceFldDataInStruct( pclDataItem, pclFldItem, &sMVT_ARR);
    }

    ilRC = GetPaxInfo( pclUAFT );
    /* Get PAX Information */
    if( ilManPX == TRUE )
    {
        if( ilRC != RC_FAIL )
        {
            dbg( TRACE, "%s: Error in getting PaxInfo. Send manually", pclFunc );
            ilAuto = FALSE;
        }
    }

    ilCustomTemplate = FALSE;
    getFldDataFromStruct (pclSubType, "TYPE", sMTPREC); 
    dbg( TRACE, "%s: MVT Telex subtype <%s>", pclFunc, pclSubType );
    if( ilAuto == TRUE )
    {
        /* Get Template Info */
        getFldDataFromStruct (pclTemplate, "TMPL", sMTPREC); 
        if( strlen(pclTemplate) <= 0 )
        {
            sprintf( pclSqlBuf, "SELECT TMPL FROM MTPTAB WHERE ALC3 = '#%s'", pclSubType );
            dbg( TRACE, "%s: SelStdTemplate <%s>", pclFunc, pclSqlBuf );
            ilRC = RunSQL( pclSqlBuf, pclTemplate );
            ilCustomTemplate = TRUE;
        }
        if( strlen(pclTemplate) <= 0 )
        {
            dbg( TRACE, "%s: Error. No template available. Switch to manual", pclFunc );
            ilAuto = FALSE;
        }
        else
        {
            if( ilCustomTemplate == TRUE )
            {
                if( ilManPX == TRUE )
                {
                    strcat( pclTemplate, "[PAXINNF]" );
                    strcat( pclTemplate, pcgNewline );
                }
            }

            memset( pclTelex, 0, sizeof(pclTelex) );
            InterpretMVTTemp( pclTelex, pclTemplate );
            AutoSendMVTTelex( pclTelex, cpStatus );
        }
    } /* auto sending */

    /* Manually send */
    if( ilAuto == FALSE )
    {
        dbg( TRACE, "%s: SENDING MANUAL TELEX", pclFunc );
        /* write this information into the tlxtab */
        setTlxTableDat (&sTLX_ARR, &sMVT_ARR );  
        ReplaceFldDataInStruct ("TLXGEN", "USEC", &sTLX_ARR);
        ReplaceFldDataInStruct ("MVT", "TTYP", &sTLX_ARR);
        ReplaceFldDataInStruct (pclSubType, "STYP", &sTLX_ARR);
        ReplaceFldDataInStruct ("C", "STAT", &sTLX_ARR);
        ReplaceFldDataInStruct ("C", "SERE", &sTLX_ARR);
        ReplaceFldDataInStruct (" ", "WSTA", &sTLX_ARR);
        ReplaceFldDataInStruct ("TLXGEN", "USEC", &sTLX_ARR);
        GetServerTimeStamp( "UTC", 1, 0, pclCurrentTime );
        ReplaceFldDataInStruct (pclCurrentTime, "CDAT", &sTLX_ARR);
        if( clADID == 'A' )
            getFldDataFromStruct(pclTmpStr, "STOA", sMVT_ARR);
        else
            getFldDataFromStruct(pclTmpStr, "STOD", sMVT_ARR);
        ReplaceFldDataInStruct (pclTmpStr, "TIME", &sTLX_ARR);
  
        ilRC = GetTelexAddr( &ilNumTAddr, &ilNumEAddr, pclTelexAddr, pclEmailAddr, FALSE );
        if( ilNumTAddr <= 0 && ilNumEAddr <= 0 )
        {
            dbg( TRACE, "%s: No telex/email addr.", pclFunc );
            return RC_FAIL;
        }

        ilLen = strlen(pclTelexAddr);
        for( ili=0; ili < ilLen; ili++ )
        {
            if( pclTelexAddr[ili] == ' ' )
                pclTelexAddr[ili] = 0x19;
        }
        pclTelexAddr[ili++] = 0x1E;
        pclTelexAddr[ili] = '\0';

        ilLen = strlen(pclEmailAddr);
        for( ili=0; ili < ilLen; ili++ )
        {
            if( pclEmailAddr[ili] == ' ' )
                pclEmailAddr[ili] = 0x19;
        }
        pclEmailAddr[ili++] = 0x1E;
        pclEmailAddr[ili] = '\0';

        /* URNO of MTPTAB */
        getFldDataFromStruct( pclSqlData, "URNO", sMTPREC );
        sprintf( pclTmpStr, "%s%c", pclSqlData, 0x1E );

        strcpy( pclSqlData, pclTmpStr );
        strcat( pclSqlData, pclTelexAddr );
        strcat( pclSqlData, pclEmailAddr );
        dbg( DEBUG, "%s: Manual Telex.TXT2 <%s>", pclFunc, pclSqlData );

        ReplaceFldDataInStruct (pclSqlData, "TXT2", &sTLX_ARR);


        getFldDataFromStruct (pclSqlFields, "#TLX", sTLX_ARR) ; 
        createValStr4Tlx (pclSqlData, sTLX_ARR) ;

        /* create Telex */
        dbg(TRACE,"TELEX RECORD NOT INSERTED INTO TLXTAB");
        /*
        ilRC = SQLInsertTlxTab (sTLX_ARR) ;
        */

        /* send the info to the braodcast handler 1900 */
        /* first set field and data list for Tlx */ 
        dbg(TRACE,"TELEX RECORD NOT BROADCASTED TO CLIENTS");
        /*
        ilRC = SendCedaEvent (igBCHDL,0,"","","0","SIN,TAB,TLXGEN",
                              "MVT", "TLXTAB", "TELEXE", pclSqlFields, pclSqlData,
                              "", 4, RC_SUCCESS) ;
        dbg (TRACE, "SendCedaEvent BCHDL ilRC=%d", ilRC) ; 
        */
    } 
    return ilRC;
} /* MVTTelexSending */


static int GetDelayInfo( char *pcpUAFT )
{
    short slSqlCursor;
    short slSqlFunc;
    int ilRC;
    int il;
    char pclSqlBuf[512];
    char pclSqlBuf2[512];
    char pclSqlData[4096];
    char pclDURN[8];
    char *pclFunc = "GetDelayInfo";

    slSqlCursor = 0;
    slSqlFunc = START;
    sprintf( pclSqlBuf, "SELECT TRIM(TEXT) FROM DLYTAB WHERE FLNU = %s", pcpUAFT );
    dbg( TRACE, "%s: SelDLY <%s>", pclFunc, pclSqlBuf );

    ilRC = DB_SUCCESS;
    il = 0;
    while( ilRC == DB_SUCCESS )
    {
        ilRC = sql_if( slSqlFunc, &slSqlCursor, pclSqlBuf, rgDELAY[il].pcDelayReas );
        if( ilRC != DB_SUCCESS )
            break;
        slSqlFunc = NEXT;
        dbg( DEBUG, "%s: DELAY REASON %d <%s>", pclFunc, il, rgDELAY[il].pcDelayReas );
        il++;
    }
    close_my_cursor( &slSqlCursor );

    slSqlCursor = 0;
    slSqlFunc = START;
    sprintf( pclSqlBuf, "SELECT DURN,TRIM(DURA) FROM DCFTAB WHERE FURN = %s", pcpUAFT );
    dbg( TRACE, "%s: SelDCF <%s>", pclFunc, pclSqlBuf );

    ilRC = DB_SUCCESS;
    il = 0;
    while( ilRC == DB_SUCCESS )
    {
        ilRC = sql_if( slSqlFunc, &slSqlCursor, pclSqlBuf, pclSqlData );
        if( ilRC != DB_SUCCESS )
            break;
        slSqlFunc = NEXT;
        get_fld( pclSqlData, FIELD_1, STR, 10, pclDURN );
        get_fld( pclSqlData, FIELD_2, STR, 4, rgDELAY[il].pcDelayDura );
        dbg( DEBUG, "%s: DELAY DURATION %d <%s>", pclFunc, il, rgDELAY[il].pcDelayDura );

        sprintf( pclSqlBuf2, "SELECT TRIM(DECA), TRIM(DECN) FROM DENTAB WHERE URNO = %s", pclDURN );
        dbg( TRACE, "%s: SelDEN <%s>", pclFunc, pclSqlBuf2 );
        ilRC = RunSQL( pclSqlBuf2, pclSqlData );
        if( ilRC == DB_SUCCESS )
        {
            get_fld( pclSqlData, FIELD_1, STR, 2, rgDELAY[il].pcDelayCode );
            if( strlen(rgDELAY[il].pcDelayCode) <= 0 )
                get_fld( pclSqlData, FIELD_2, STR, 2, rgDELAY[il].pcDelayCode );
            dbg( DEBUG, "%s: DELAY CODE %d <%s>", pclFunc, il, rgDELAY[il].pcDelayCode );
        }
        il++;
    }
    close_my_cursor( &slSqlCursor );

    return RC_SUCCESS;
} /* GetDelayIfno */


static int GetPaxInfo( char *pcpUAFT )
{
    int ilRC;
    char pclSqlBuf[512];
    char pclSqlData[512];
    char *pclFunc = "GetPaxInfo";

    sprintf( pclSqlBuf, "SELECT SUM(VALU) FROM LOATAB WHERE FLNU = %s AND DSSN = 'USR' "
                        "AND TYPE = 'PAX' AND STYP IN ( '2','J','F','E','B' )", pcpUAFT );
    dbg( TRACE, "%s: SelLOAPax <%s>", pclFunc, pclSqlBuf );

    ilRC = RunSQL( pclSqlBuf, pclSqlData );
    rgPAX.iPaxInfo = -1;
    if( ilRC == DB_SUCCESS )
        rgPAX.iPaxInfo = atoi(pclSqlData);
    dbg( TRACE, "%s: Pax Info <%d>", pclFunc, rgPAX.iPaxInfo  );

    sprintf( pclSqlBuf, "SELECT TRIM(VALU) FROM LOATAB WHERE FLNU = %s AND DSSN = 'USR' "
                        "AND TYPE = 'PAX' AND SSST = 'I'", pcpUAFT );
    dbg( TRACE, "%s: SelLOAInfant <%s>", pclFunc, pclSqlBuf );
    ilRC = RunSQL( pclSqlBuf, pclSqlData );
    rgPAX.iInfant  = 0;
    if( ilRC == DB_SUCCESS )
        rgPAX.iInfant = atoi(pclSqlData);
    dbg( TRACE, "%s: Infant <%d>", pclFunc, rgPAX.iInfant );

    if( rgPAX.iPaxInfo < 0 || rgPAX.iInfant < 0 )
        return RC_FAIL;
    return RC_SUCCESS;
} /* GetPaxInfo */

static int InterpretMVTTemp( char *pcpResult, char *pcpTemplate )
{
    int ilRC;
    int ili, ilr, ilv, ilf;
    int ilLen;
    int ilVar;
    int ilFormat;
    short slSqlCursor;
    short slSqlFunc;
    char pclSqlBuf[1024];
    char pclSqlData[512];
    char pclVarName[32];
    char pclVarValue[4004];
    char pclFormat[12];
    char clChr;
    char *pclFunc = "InterpretMVTTemp";

    dbg( TRACE, "%s:  Telex Template ===> \n<\n%s\n>", pclFunc, pcpTemplate );
    ilLen = strlen(pcpTemplate);
    ilf = ilr = ilv = 0;
    ilVar = ilFormat = FALSE;
    memset( pclFormat, 0, sizeof(pclFormat) );
    memset( pclVarValue, 0, sizeof(pclVarValue) ); 
    memset( pclVarName, 0, sizeof(pclVarName) ); 
    for( ili = 0; ili < ilLen; ili++ )
    {
        clChr = pcpTemplate[ili];
        if( clChr == '[' )
        {
            ilVar = TRUE;
            ilFormat = FALSE;
            memset( pclVarName, 0, sizeof(pclVarName) ); 
            ilv = 0;
            continue;
        }
        if( clChr == '|' )
        {
            ilFormat = TRUE;
            memset( pclFormat, 0, sizeof(pclFormat) ); 
            ilf = 0;
            continue;
        }

        if( clChr == ']' )
        {   
            pclVarName[ilv] = '\0';
            if( ilFormat == TRUE )
                pclFormat[ilf] = '\0';
            GetVarValue( pclVarValue, pclVarName, pclFormat );
            dbg( TRACE, "%s: VALUE <%s>", pclFunc, pclVarValue );
            pcpResult[ilr] = '\0';  /* end properly to allow proper strcat */
            strcat( pcpResult, pclVarValue );
            ilr += strlen(pclVarValue);
            ilVar = FALSE;
            ilFormat = FALSE;
            memset( pclFormat, 0, sizeof(pclFormat) );
            memset( pclVarValue, 0, sizeof(pclVarValue) ); 
            continue;
        }
        if( ilVar == TRUE && ilFormat == FALSE )
        {
            pclVarName[ilv++] = clChr ;
            continue;
        }
        if( ilFormat == TRUE )
        {
            pclFormat[ilf++] = clChr;
            continue;
        }
        pcpResult[ilr++] = clChr;
    }
    pcpResult[ilr] = '\0';

    dbg( TRACE, "%s: MVT Telex ===>\n<\n%s\n>", pclFunc, pcpResult );
    return RC_SUCCESS;

} /* InterpretMVTTemp */

static int GetVarValue( char *pcpVarValue, char *pcpVarName, char *pcpFormat )
{
    int ilRC;
    int ili, ilj;
    int ilf;
    int ilFound;
    int ilLen;
    char clADID;
    char pclVarName[32];
    char pclData[2056];
    char pclTmpStr[1024];
    char *pclFunc = "GetVarValue";

    strcpy( pclVarName, pcpVarName );
    pcpVarValue[0] = '\0';
    memset( pclData, 0, sizeof(pclData) );
    if( strlen(pclVarName) <= 0 )
        return RC_FAIL;
    dbg( TRACE, "%s: VARNAME <%s> FORMAT <%s>", pclFunc, pclVarName, pcpFormat );

    ilFound = FALSE;
    for( ili = 0; ili < igNumMCG; ili++ )
    {
        if( !strcmp( rgMCG[ili].pcFina, pclVarName ) )
        {
            ilFound = TRUE;
            break;
        }
    }
    if( ilFound == FALSE )
    {
        dbg( TRACE, "%s: Did not find any config. Field unchanged", pclFunc );
        strcpy( pcpVarValue, pcpVarName );
        return RC_SUCCESS;
    }
    /* Flight fields */
    if( !strcmp( rgMCG[ili].pcTana, "AFT" ) )
    {
        ilRC = getFldDataFromStruct(pclData, pclVarName, sMVT_ARR) ;  
        if( ilRC != RC_SUCCESS )
        {
            dbg( TRACE, "%s: Cannot find field <%s> in array", pclFunc, pclVarName );
            strcpy( pcpVarValue, pcpVarName );
            return RC_SUCCESS;
        }
        if( strlen(pclData) <= 0 )
            return RC_SUCCESS;
        if( pcpFormat != NULL )
        {
            ilLen = strlen(pcpFormat);
            if( ilLen > 0 )
            {
                for( ilf = 0; ilf < ilLen; ilf+=2 )
                {
                    memcpy( pclTmpStr, &pcpFormat[ilf], 2 );
                    if( !strcmp( pclTmpStr, "DD" ) )
                        strncat( pcpVarValue, &pclData[6], 2 );
                    else if( !strcmp( pclTmpStr, "HH" ) )
                        strncat( pcpVarValue, &pclData[8], 2 );
                    else if( !strcmp( pclTmpStr, "MM" ) )
                        strncat( pcpVarValue, &pclData[10], 2 );
                }
                return RC_SUCCESS;
            }
        }
        if( !strcmp( rgMCG[ili].pcFina, "FLNO" ) )
        {
            ilLen = strlen(pclData);
            ilf = 0;
            if( ilLen > 9 ) ilLen = 9;
            for( ilj = 0; ilj < ilLen; ilj++ )
            {
                if( pclData[ilj] != ' ' )  /* trim all blanks */
                    pcpVarValue[ilf++] = pclData[ilj];
            }
            pcpVarValue[ilf] = '\0';
            return RC_SUCCESS;
        }
        strcpy( pcpVarValue, pclData );
        return RC_SUCCESS;
    }

    /* Table which does not exist */
    if( !strcmp( rgMCG[ili].pcTana, "VIA" ) )
    {
        if( !strcmp( pclVarName, "NHOP" ) )
        {
            ilRC = getFldDataFromStruct(pclData, "VIAL", sMVT_ARR) ;  
            if( ilRC != RC_SUCCESS )
            {
                dbg( TRACE, "%s: Cannot find field <%s> in array", pclFunc, "VIAL" );
                strcpy( pcpVarValue, pcpVarName );
                return RC_SUCCESS;
            }
            if( strlen(pclData) >= 4 )
            {
                sprintf( pclTmpStr, "%3.3s", &pclData[1] );
                strcpy( pcpVarValue, pclTmpStr );
            }
            else
            {
                ilRC = getFldDataFromStruct(pclTmpStr, "ADID", sMVT_ARR) ;  
                clADID = pclTmpStr[0];
                if( clADID == 'A' )
                    ilRC = getFldDataFromStruct(pcpVarValue, "ORG3", sMVT_ARR) ;  
                else
                    ilRC = getFldDataFromStruct(pcpVarValue, "DES3", sMVT_ARR) ;  
            }
            return RC_SUCCESS;
        }
    }

    /* Pax Information */
    if( !strcmp( rgMCG[ili].pcTana, "LOA" ) )
    {
        if( !strcmp( rgMCG[ili].pcFina, "PXIN" ) )
            sprintf( pcpVarValue, "%2.2s%d", rgMCG[ili].pcPrefix, (rgPAX.iPaxInfo+rgPAX.iInfant) );
        else if( !strcmp( rgMCG[ili].pcFina, "PXIF" ) )
            sprintf( pcpVarValue, "%2.2s%d INCLDG %d INF", rgMCG[ili].pcPrefix, (rgPAX.iPaxInfo+rgPAX.iInfant), rgPAX.iInfant );
        else if( !strcmp( rgMCG[ili].pcFina, "PXEF" ) )
            sprintf( pcpVarValue, "%2.2s%d PLUS %d INF", rgMCG[ili].pcPrefix, rgPAX.iPaxInfo, rgPAX.iInfant );
        return RC_SUCCESS;
    }

    /* Delay Information */
    if( !strcmp( rgMCG[ili].pcTana, "DCF" ) || !strcmp( rgMCG[ili].pcTana, "DEN" ) || 
        !strcmp( rgMCG[ili].pcTana, "DLY" ) )
    {
        if( !strcmp( rgMCG[ili].pcFina, "DUR1" ) )
            sprintf( pcpVarValue, "%2.2s%s", rgMCG[ili].pcPrefix, rgDELAY[0].pcDelayDura );
        else if( !strcmp( rgMCG[ili].pcFina, "DUR2" ) )
            sprintf( pcpVarValue, "%2.2s%s", rgMCG[ili].pcPrefix, rgDELAY[1].pcDelayDura );
        else if( !strcmp( rgMCG[ili].pcFina, "DCO1" ) )
            sprintf( pcpVarValue, "%2.2s%s", rgMCG[ili].pcPrefix, rgDELAY[0].pcDelayCode );
        else if( !strcmp( rgMCG[ili].pcFina, "DCO2" ) )
            sprintf( pcpVarValue, "%2.2s%s", rgMCG[ili].pcPrefix, rgDELAY[1].pcDelayCode );
        else if( !strcmp( rgMCG[ili].pcFina, "DRE1" ) )
            sprintf( pcpVarValue, "%2.2s%s", rgMCG[ili].pcPrefix, rgDELAY[0].pcDelayReas );
        else if( !strcmp( rgMCG[ili].pcFina, "DRE2" ) )
            sprintf( pcpVarValue, "%2.2s%s", rgMCG[ili].pcPrefix, rgDELAY[1].pcDelayReas );
        return RC_SUCCESS;
    }
    strcpy( pcpVarValue, pclData );
    return RC_SUCCESS;
} /* GetVarValue */

static int AutoSendMVTTelex( char *pcpTelex, char cpStatus )
{
    int ilRC;
    int ilLen;
    int ilPerLine, ilPerTelex;
    int ilPerCCLine;
    int ili, ilj;
    int ilPrintStart, ilPrintEnd;
    int ilNumTAddr;
    int ilNumEAddr;
    int ilccList;
    char pclDataItem[2048];
    char pclSenderAddr[8] = "SQ12345";
    char pclTelexAddr[1024];
    char pclEmailAddr[2048];
    char pclFullTelex[8008];
    char pclCurrentTime[16];
    char pclFldLst[1024];
    char pclDatLst[4096];
    char pclTelexHeading[20];
    char pclTelexTail[20];
    char pclTmpStr[4096];
    char clADID;
    char *pclP;
    char *pclFunc = "AutoSendMVTTelex";

/* BERNI */
    sprintf( pclTelexHeading, "%c%c%cQU ", 0x0D, 0x0A, 0x01 );
    sprintf( pclTelexTail, "%c%c%c ", 0x0D, 0x0A, 0x03 );
    ilRC = GetTelexAddr( &ilNumTAddr, &ilNumEAddr, pclTelexAddr, pclEmailAddr, TRUE );
    if( ilNumTAddr <= 0 && ilNumEAddr <= 0 )
    {
        dbg( TRACE, "%s: Telex NOT send. No telex addr configured", pclFunc );
        return RC_SUCCESS;
    }
    ilccList = FALSE;
    if( ilNumTAddr > 31 )
        ilccList = TRUE;

    /* Formatting the actual telex */
    GetServerTimeStamp( "UTC", 1, 0, pclCurrentTime );

    setTlxTableDat( &sTLX_ARR, &sMVT_ARR ) ;
    ReplaceFldDataInStruct ("MVT", "TTYP", &sTLX_ARR) ;
    getFldDataFromStruct(pclDataItem, "TYPE", sMTPREC) ;
    ReplaceFldDataInStruct (pclDataItem, "STYP", &sTLX_ARR) ;
    ReplaceFldDataInStruct ("S", "WSTA", &sTLX_ARR) ;
    ReplaceFldDataInStruct ("TLXGEN", "USEC", &sTLX_ARR);
    ReplaceFldDataInStruct (pclCurrentTime, "CDAT", &sTLX_ARR);
    getFldDataFromStruct(pclTmpStr, "ADID", sMVT_ARR) ;
    clADID = pclTmpStr[0];
    if( clADID == 'A' )
        getFldDataFromStruct(pclTmpStr, "STOA", sMVT_ARR);
    else
        getFldDataFromStruct(pclTmpStr, "STOD", sMVT_ARR);
    ReplaceFldDataInStruct (pclTmpStr, "TIME", &sTLX_ARR);


    if( cpStatus == 'P' ) /* Dun send first */
    {
        dbg( TRACE, "%s: SENDING PENDING TELEX", pclFunc );

        getFldDataFromStruct(pclTmpStr, "TSND", sMTPREC) ;
        ili = atoi(pclTmpStr);
        getFldDataFromStruct(pclTmpStr, "FSND", sMTPREC) ;
        getFldDataFromStruct(pclCurrentTime, pclTmpStr, sMVT_ARR);
        AddSecondsToCEDATime( pclCurrentTime, ili * 60, 1 );
        dbg( TRACE, "%s: Pending Sent <%d> min after <%s>. At <%s>", pclFunc, ili, pclTmpStr, pclCurrentTime );
        ReplaceFldDataInStruct (pclCurrentTime, "TKEY", &sTLX_ARR) ;
        ReplaceFldDataInStruct ("P", "SERE", &sTLX_ARR) ;
    }
    else
    {
        dbg( TRACE, "%s: SENDING AUTO TELEX", pclFunc );

        ReplaceFldDataInStruct ("S", "SERE", &sTLX_ARR) ;
        ReplaceFldDataInStruct ("A", "STAT", &sTLX_ARR) ;
    }

    /* Format Telex base on telex addr, assuming only max 60 address */
    pclFullTelex[0] = '\0';
    ilPerLine = 0;
    ilPerTelex = 0;
    for( ili = 1; ili <= ilNumTAddr; ili++ )
    {
        if( ilPerTelex == 0 )
        {
            strcpy( pclFullTelex, pclTelexHeading );    /* QU */
            ilPrintStart = ili;
        }

        strncat( pclFullTelex, &pclTelexAddr[(ili-1)*8], 8 );  /* Include blank */
        ilPerTelex++;
        ilPerLine++;

        if( ilPerLine == igMaxAddrPerLine )
        {
            strcat( pclFullTelex, pcgNewline );
            ilPerLine = 0;
        }

        if( ilPerTelex == igMaxAddrPerTelex || ((ili+1) > ilNumTAddr) )
        {
            ilPerTelex = 0;            
            ilPerLine = 0;
            strcat( pclFullTelex, pcgNewline );
            strcat( pclFullTelex, ". ");
            strcat( pclFullTelex, pcgSenderTelexAddr );
            strcat( pclFullTelex, " ");
            strncat( pclFullTelex, &pclCurrentTime[6], 6 );
            strcat( pclFullTelex, pcgNewline );
            strcat( pclFullTelex, pcpTelex );

            /* Prepare the cc list */
            if( ilccList == TRUE )
            {
                ilPerCCLine = 0;
                ilPrintEnd = ili;
                for( ilj = 1; ilj <= ilNumTAddr; ilj++ )
                {
                    if( ilj >= ilPrintStart && ilj <= ilPrintEnd )
                        continue;
                    if( ilPerCCLine == 0 )
                    {
                        strcat( pclFullTelex, pcgNewline );
                        strcat( pclFullTelex, "SI " );
                    }
                    strncat( pclFullTelex, &pclTelexAddr[(ilj-1)*8], 8 );
                    ilPerCCLine++;
                    if( ilPerCCLine == igMaxAddrPerLine )
                        ilPerCCLine = 0;
                }
            }
            strcat( pclFullTelex, pcgNewline );
            strcat( pclFullTelex, pclTelexTail );

            memcpy( pclTmpStr, pclFullTelex, 4000 );
            ReplaceFldDataInStruct (pclTmpStr, "TXT1", &sTLX_ARR) ;
            if( strlen(pclFullTelex) > 4000 )
            {
                memcpy( pclTmpStr, &pclFullTelex[4000], 4000 );
                ReplaceFldDataInStruct (pclTmpStr, "TXT2", &sTLX_ARR) ;
            }
            dbg(TRACE,"TELEX (AUTO SEND) NOT INSERTED INTO TLXTAB");
            /*
            ilRC = SQLInsertTlxTab (sTLX_ARR) ;
            */
     
            if( cpStatus == 'S' )
            {
                getFldDataFromStruct (pclFldLst, "#TLX", sTLX_ARR) ;
                createValStr4Tlx (pclDatLst, sTLX_ARR );

                PatchTextServerToClient( pclFullTelex, FALSE );
                dbg(TRACE,"TELEX (AUTO SEND) NOT BROADCASTED TO CLIENTS");
                /*
                ilRC = SendCedaEvent ( igTELEX,0,"","","0","SIN,TAB,TLXGEN",
                                       "TLX", "", "TELEXE", "", pclFullTelex,
                                       "", 4, RC_SUCCESS) ;
                dbg( TRACE, "%s: Send to TELEX ilRC = %d", pclFunc, ilRC );
                ilRC = SendCedaEvent (igBCHDL,0,"","","0","SIN,TAB,TLXGEN",
                                      "MVT", "TLXTAB", "TELEXE", pclFldLst, pclDatLst,
                                       "", 4, RC_SUCCESS) ;
                */
            }
            /*
            dbg( TRACE, "%s: Send to BCHDL ilRC = %d", pclFunc, ilRC );
            strcpy( pclFullTelex, pclTelexHeading );
            */
        }
    }
    return RC_SUCCESS;
} /* AutoSendMVTTelex */

static int GetTelexAddr( int *ipNumTelexAddr, int *ipNumEmailAddr, char *pcpTelexAddr, char *pcpEmailAddr, int ipIncludeHome )
{
    int ilRC;
    int ilAllStations;
    int ilAPC3;
    int ilHops3;
    int ilHops4;
    int ilNumTAddr;
    int ilNumEAddr;
    int ilVialLen;
    int ili;
    short slSqlCursor;
    short slSqlFunc;
    char clADID;
    char pclALC[8];
    char pclSqlBuf[1024];
    char pclSqlData[1024];
    char pclAPC[8];
    char pclVial[1028];
    char pclTmpStr[2048];
    char pclOneHop[20];
    char pclTelexAddr[1024];
    char pclEmailAddr[2048];
    char pclHops3[1024];
    char pclHops4[1024];
    char pclSqlGeneral[1024];
    char *pclP;
    char *pclFunc = "GetTelexAddr";

    memset( pclVial, 0, sizeof(pclVial) );
    ilRC = getFldDataFromStruct( pclVial, "VIAL", sMVT_ARR ); 
    dbg( TRACE, "%s: VIAL <%s>", pclFunc, pclVial );
    ilVialLen = strlen(pclVial);  /* Each hop is 120 char */

    ilAllStations = FALSE;
    getFldDataFromStruct(pclTmpStr, "ALLS", sMTPREC); 
    if( pclTmpStr[0] == 'Y' )
        ilAllStations = TRUE;
    dbg( TRACE, "%s: All Stations <%d>", pclFunc, ilAllStations );


    getFldDataFromStruct(pclTmpStr, "ADID", sMVT_ARR); 
    clADID = pclTmpStr[0];

    memset( pclHops3, 0, sizeof(pclHops3) );
    memset( pclHops4, 0, sizeof(pclHops4) );
    if( ilAllStations == TRUE || ilVialLen <= 0 )
    {
        if( clADID == 'A' )
            getFldDataFromStruct(pclAPC, "ORG3", sMVT_ARR); 
        else
            getFldDataFromStruct(pclAPC, "DES3", sMVT_ARR); 
        if( strlen(pclAPC) <= 0 )
        {
            if( clADID == 'A' )
                getFldDataFromStruct(pclAPC, "ORG4", sMVT_ARR); 
            else
                getFldDataFromStruct(pclAPC, "DES4", sMVT_ARR); 
            sprintf( pclTmpStr, "'%s'", pclAPC );
            strcpy( pclHops4, pclTmpStr );
        }
        else
        {
            sprintf( pclTmpStr, "'%s'", pclAPC );
            strcpy( pclHops3, pclTmpStr );
        }
    }

     if( ilVialLen >= 4 )
     {
        for( ili = 0; ili < ilVialLen; ili+=120 )
        {
            ilAPC3 = TRUE;
            memcpy( pclOneHop, &pclVial[ili+1], 3 );
            if( !strncmp( pclOneHop, "   ", 3 ) )   /* No APC3 */
            {
                memcpy( pclOneHop, &pclVial[ili+1+3], 4 );
                ilAPC3 = FALSE;
            }

            if( ilAllStations == TRUE )
            {
                if( ilAPC3 == TRUE )
                {
                    sprintf( pclTmpStr, "'%s'", pclOneHop );        
                    strcat( pclHops3, "," );
                    strcat( pclHops3, pclTmpStr );
                }
                else
                {
                    sprintf( pclTmpStr, "'%s'", pclOneHop );
                    strcat( pclHops4, "," );
                    strcat( pclHops4, pclTmpStr );
                }
                continue;
            }
            if( (ili == 0 && clADID == 'D')  || 
                ((ili+120) > ilVialLen && clADID == 'A') )
            {
                if( ilAPC3 == TRUE )
                {
                    sprintf( pclTmpStr, "'%s'", pclOneHop );
                    if( ilAllStations == TRUE )
                        strcat( pclHops3, "," );
                    strcat( pclHops3, pclTmpStr );
                }
                else
                {
                    sprintf( pclTmpStr, "'%s'", pclOneHop );
                    if( ilAllStations == TRUE )
                        strcat( pclHops4, "," );
                    strcat( pclHops4, pclTmpStr );
                }               
            }
        } /* for */
        dbg( TRACE, "%s: HOP3 <%s> HOP4 <%s>", pclFunc, pclHops3, pclHops4 );
    }
    else
        dbg( TRACE, "%s: No VIAs for this flight", pclFunc );

    memset( pclTelexAddr, 0, sizeof(pclTelexAddr) );
    memset( pclEmailAddr, 0, sizeof(pclEmailAddr) );
    ilNumTAddr = 0;
    ilNumEAddr = 0;
    if( ipIncludeHome == TRUE )
    {
        getFldDataFromStruct(pclTmpStr, "HMST", sMTPREC); 
        if( pclTmpStr[0] == 'Y' )
        {
            getFldDataFromStruct(pclTmpStr, "HMTE", sMTPREC); 
            if( strlen(pclTmpStr) < 7 )
                dbg( TRACE, "%s: Home telex addr NOT configured", pclFunc );
            else
            {
                strcpy( pclTelexAddr, pclTmpStr );
				strcat( pclTelexAddr, " " );
                ilNumTAddr = (strlen(pclTelexAddr)/8) + 1;  /* Last addr does not append with space */
            }

            getFldDataFromStruct(pclTmpStr, "HMEM", sMTPREC); 
            if( strlen(pclTmpStr) <= 0 )
                dbg( TRACE, "%s: Home email addr NOT configured", pclFunc );
            else
            {
                strcpy( pclEmailAddr, pclTmpStr );
				strcat( pclEmailAddr, " " );
                ilNumEAddr = (strlen(pclEmailAddr)/8) + 1;  /* Last addr does not append with space */
            }
        }
        dbg( TRACE, "%s: #HomeTAddr <%d> #HomeEAddr <%d>", pclFunc, ilNumTAddr, ilNumEAddr );
        dbg( DEBUG, "%s: Home Telex Addr <%s>", pclFunc, pclTelexAddr );
        dbg( DEBUG, "%s: Home Email Addr <%s>", pclFunc, pclEmailAddr );
    }

    getFldDataFromStruct(pclALC, "ALC2", sMVT_ARR); 
    if( strncmp( pclALC, "  ", 2 ) )
        getFldDataFromStruct(pclALC, "ALC3", sMVT_ARR); 

    sprintf( pclSqlGeneral, "%s", "SELECT DISTINCT(ADDR) FROM MADTAB WHERE " );
    sprintf( pclTmpStr, "'%s'", pclALC );
    if( strlen(pclALC) == 2 )
        strcat( pclSqlGeneral, "ALC2 = " );
    else
        strcat( pclSqlGeneral, "ALC3 = " );
    strcat( pclSqlGeneral, pclTmpStr );
    strcat( pclSqlGeneral, " AND " );

    ilHops3 = FALSE;
    if( strlen(pclHops3) > 0 )
        ilHops3 = TRUE;
    ilHops4 = FALSE;
    if( strlen(pclHops4) > 0 )
        ilHops4 = TRUE;
    if( ilHops3 == TRUE || ilHops4 == TRUE )
    {
        if( ilHops3 == TRUE && ilHops4 == TRUE )
            sprintf( pclTmpStr, "(APC3 IN ( %s ) OR APC4 IN ( %s ) )", pclHops3, pclHops4 ); 
        else if( ilHops3 == TRUE )  
            sprintf( pclTmpStr, "APC3 IN ( %s )", pclHops3 ); 
        else if( ilHops4 == TRUE )
            sprintf( pclTmpStr, "APC4 IN ( %s )", pclHops4 ); 
        strcat( pclSqlGeneral, pclTmpStr );
        strcat( pclSqlGeneral, " AND " );
    }
    strcat( pclSqlGeneral, "STAT = 'A'" );

    /* Get Telex Address */
    slSqlCursor = 0;
    slSqlFunc = START;
    sprintf( pclSqlBuf, "%s AND TYPE ='T'", pclSqlGeneral );
    dbg( TRACE, "%s: SelMAD-telex <%s>", pclFunc, pclSqlBuf );
    ilRC = DB_SUCCESS;
    while( ilRC == DB_SUCCESS )
    {
        ilRC = sql_if(slSqlFunc, &slSqlCursor, pclSqlBuf, pclSqlData);
        if( ilRC != DB_SUCCESS )
            break;
        slSqlFunc = NEXT;

        get_fld( pclSqlData, FIELD_1, STR, 100, pclTmpStr );
        if( ipIncludeHome == TRUE )
        {
            pclP = strstr( pclTelexAddr, pclTmpStr );
            if( pclP != NULL )
            {
                dbg( DEBUG, "%s: TelexAddr <%s> exist in Home addr", pclFunc, pclTmpStr );
                continue;
            }
        }
        strncat( pclTelexAddr, pclTmpStr, 7 );
        strcat( pclTelexAddr, " " );
        ilNumTAddr++;
        if( ilNumTAddr > 60 )
        {
            ilNumTAddr = 60;
            dbg( TRACE, "%s: Too many TelexAddr. Cap at <%d>", pclFunc, ilNumTAddr );
            break;
        }
    }
    close_my_cursor( &slSqlCursor );
    dbg( TRACE, "%s: #TelexAddr <%d>", pclFunc, ilNumTAddr );
    dbg( DEBUG, "%s: TelexAddr <%s>", pclFunc, pclTelexAddr );

    strcpy( pcpTelexAddr, pclTelexAddr );
    *ipNumTelexAddr = ilNumTAddr;

    /* Get Email Address */
    slSqlCursor = 0;
    slSqlFunc = START;
    sprintf( pclSqlBuf, "%s AND TYPE ='E'", pclSqlGeneral );
    dbg( TRACE, "%s: SelMAD-email <%s>", pclFunc, pclSqlBuf );
    ilRC = DB_SUCCESS;
    while( ilRC == DB_SUCCESS )
    {
        ilRC = sql_if(slSqlFunc, &slSqlCursor, pclSqlBuf, pclSqlData);
        if( ilRC != DB_SUCCESS )
            break;
        slSqlFunc = NEXT;

        get_fld( pclSqlData, FIELD_1, STR, 100, pclTmpStr );
        if( ipIncludeHome == TRUE )
        {
            pclP = strstr( pclEmailAddr, pclTmpStr );
            if( pclP != NULL )
            {
                dbg( DEBUG, "%s: EmailAddr <%s> exist in Home addr", pclFunc, pclTmpStr );
                continue;
            }
        }
        strcat( pclEmailAddr, pclTmpStr  );
        strcat( pclEmailAddr, " " );
        ilNumEAddr++;
        if( strlen(pclEmailAddr) > sizeof(pclEmailAddr) )
        {
            dbg( TRACE, "%s: Too many EmailAddr. Cap at <%d>", pclFunc, ilNumEAddr );
            break;
        }
    }
    close_my_cursor( &slSqlCursor );
    dbg( TRACE, "%s: #EmailAddr <%d>", pclFunc, ilNumEAddr );
    dbg( DEBUG, "%s: EmailAddr <%s>", pclFunc, pclEmailAddr );

    strcpy( pcpEmailAddr, pclEmailAddr );
    *ipNumEmailAddr = ilNumEAddr;

    return RC_SUCCESS;
}

/******************************************************************************/
/* Space for Appendings (Source-Code Control)                                 */
/******************************************************************************/
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

static int ReplaceStringInString(char *pcpString, char *pcpSearch, char *pcpReplace)
{
  int ilRc = RC_SUCCESS;
  int ilGetRc = RC_SUCCESS;
  if (MyDebugSwitch == TRUE)
  {
    dbg(TRACE,"TEST: PATCH   STRING = <%s>",pcpString);
    dbg(TRACE,"TEST: SEARCH  STRING = <%s>",pcpSearch);
    dbg(TRACE,"TEST: REPLACE STRING = <%s>",pcpReplace);
  }
  if (MyDebugSwitch == TRUE)
  {
    dbg(TRACE,"BEFORE SET STRING");
  }
  SetStringValue(&rgMyDataString, pcpString);
  if (strcmp(pcpSearch,pcpReplace) != 0)
  {
    if (MyDebugSwitch == TRUE)
    {
      dbg(TRACE,"CALLING REPLACE PATTERN");
    }
    ReplacePatternInString(&rgMyDataString, pcpSearch, pcpReplace);
    strcpy(pcpString,rgMyDataString.Value);
    if (MyDebugSwitch == TRUE)
    {
      dbg(TRACE,"BACK FROM REPLACE PATTERN");
    }
  }
  if (MyDebugSwitch == TRUE)
  {
    dbg(TRACE,"TEST: RESULT  STRING = <%s>",pcpString);
  }
  return ilRc;
}

/* FROM GOCHDL */

/* ******************************************************************** */
/* ******************************************************************** */
static int HandleSqlSelectRows(char *pcpTable, char *pcpSqlKey, char *pcpSqlFld, char *pcpSqlDat, int ipLines)
{
  int ilRc = RC_SUCCESS;
  int ilGetRc = DB_SUCCESS; 
  char pclSqlBuf[4096*2];
  char pclDatBuf[4096*2];
  int ilPos = 0;
  int ilCnt = 0;
  sprintf(pclSqlBuf,"SELECT %s FROM %s %s", pcpSqlFld, pcpTable, pcpSqlKey);
  dbg(TRACE,"<%s>",pclSqlBuf);
  ilPos = 0;
  ilCnt = 0;
  short slCursor = 0;
  short slFkt = START;
  ilGetRc = DB_SUCCESS; 
  while (ilGetRc == DB_SUCCESS)
  {
    pclDatBuf[0] = 0x00;
    ilGetRc = sql_if(slFkt,&slCursor,pclSqlBuf,pclDatBuf);
    if (ilGetRc == DB_SUCCESS)
    {
      ilCnt++;
      /* dbg(TRACE,"DATA <%s>",pclDatBuf); */
      BuildItemBuffer(pclDatBuf,pcpSqlFld,-1,",");
      StrgPutStrg(pcpSqlDat, &ilPos, pclDatBuf, 0, -1, "\n");
      slFkt = NEXT;
    }
  }
  close_my_cursor(&slCursor);
  if (ilPos > 0)
  {
    ilPos--;
  }
  pcpSqlDat[ilPos] = 0x00;
  dbg(TRACE,"SELECT ROWS: GOT %d RECORDS",ilCnt);
  ilRc = ilGetRc;
  if ((ilGetRc == ORA_NOT_FOUND) && (ilPos > 0))
  {
    ilRc = DB_SUCCESS;
  }
  else
  {
    dbg(TRACE,"SELECT ROWS: NO DATA FOUND");
  }
  dbg(TRACE,"--------------------------");
  return ilCnt;
}

/* ******************************************************************** */
/* ******************************************************************** */
static int  get_any_item(char *dest, char *src, int ItemNbr, char *sep)
{
        int rc = -1;
        int gt_rc;
        int b_len = 0;
        int trim = TRUE;
        if (ItemNbr < 0){
                trim = FALSE;
                ItemNbr = - ItemNbr;
        } /* end if */
        gt_rc = get_item(ItemNbr, src, dest, b_len, sep, "\0", "\0");
        if (gt_rc == TRUE){
                if (trim == TRUE){
                        str_trm_rgt(dest, " ", TRUE);
                } /* end if */
                rc = strlen(dest);
        }/* end if */
        return rc;
}/* end of get_any_item */

/* ******************************************************************** */
/* ******************************************************************** */
static int GetFieldValues(char *pcpFldVal, char *pcpFldLst, char *pcpGetFldLst, char *pcpDatLst)
{
  int ilGetRc = 0;
  char pclFldName[8] = "";
  char pclFldData[4096] = "";
  int ilPos = 0;
  int ilFld = 0;
  int ilMax = 0;

  ilPos = 0;
  ilMax = field_count(pcpGetFldLst);
  for (ilFld=1;ilFld<=ilMax;ilFld++)
  {
    ilGetRc = get_real_item(pclFldName,pcpGetFldLst, ilFld);
    ilGetRc = GetFieldValue(pclFldData, pcpFldLst, pclFldName, pcpDatLst);
    StrgPutStrg (pcpFldVal, &ilPos, pclFldData, 0, -1, ",");
  }
  ilPos--;
  pcpFldVal[ilPos] = 0x00;
  return ilPos;
}

/* ******************************************************************** */
/* ******************************************************************** */
static int GetFieldValue(char *pcpFldVal, char *pcpFldLst, char *pcpFldNam, char *pcpDatLst)
{
  int ilLen = -1;
  int ilFldItm = 0;
  ilFldItm = get_item_no (pcpFldLst, pcpFldNam, 5) + 1;
  if (ilFldItm > 0)
  {
    ilLen = get_real_item (pcpFldVal, pcpDatLst, ilFldItm);
  }
  if (ilLen <= 0)
  {
    strcpy (pcpFldVal, " ");
  }
  return ilLen;
}

/* FROM FLIGHT (Modified) */
/*******************************************************/
/*******************************************************/
static int GetCfgLine(char *pcpFile, char *pcpGroup, char *pcpLine, short spNoOfLines, char *pcpDestBuff, char *pcpDefault)
{
  int ilRC = RC_SUCCESS;

  pcpDestBuff[0] = 0x00;
  ilRC = iGetConfigRow (pcpFile, pcpGroup, pcpLine, spNoOfLines, pcpDestBuff);
  if (ilRC != RC_SUCCESS)
  {
    /* use default entry */
    strcpy (pcpDestBuff, pcpDefault);
  }                             /* end if */
  return ilRC;
}


/* ======================================================================================= */
/*******************************************************/
/*******************************************************/
static int InitAutoMvtConfig(void)
{
  int ilRc = RC_SUCCESS;
  int ilGetRc = RC_SUCCESS;
  char pclString[512] = "";
  char pclDefault[1024] = "";
  CT_InitStringDescriptor(&rgMyDataString);

  dbg(TRACE,"========== AUTO MVT SECTION ==================");
  strcpy(pcgSenderTelexAddr,"OOOOOPS");
  iGetConfigEntry(cgConfigFile, "AUTO_MVT", "SENDER_TELEX_ADDR", CFG_STRING, pcgSenderTelexAddr) ; 
  dbg( TRACE, "TELEX SENDER ADDR = <%s>", pcgSenderTelexAddr );

  strcpy(pcgSenderEmailAddr,"OOOOOPS@OOOPS.OPS");
  iGetConfigEntry(cgConfigFile, "AUTO_MVT", "SENDER_EMAIL_ADDR", CFG_STRING, pcgSenderEmailAddr) ; 
  dbg( TRACE, "EMAIL SENDER ADDR = <%s>", pcgSenderEmailAddr );

  sprintf(pclString,"%d",igToTlxQue);
  iGetConfigEntry(cgConfigFile, "AUTO_MVT", "TELEX_MOD_ID", CFG_STRING, pclString) ; 
  igToTlxQue = atoi(pclString);
  dbg(TRACE,"SENDING TELEXES TO TELEX_MOD_ID %d",igToTlxQue);

  strcpy(pcgSenderEmailSmtp,"NO");
  iGetConfigEntry(cgConfigFile, "AUTO_MVT", "EMAIL_READY_TO_USE", CFG_STRING, pcgSenderEmailSmtp) ; 
  if (strcmp(pcgSenderEmailSmtp,"YES") == 0)
  {
    igEmailIsReadyToUse = TRUE;
  }
  else
  {
    igEmailIsReadyToUse = FALSE;
  }
  dbg(TRACE,"SENDING TELEXES PER EMAIL = %s",pcgSenderEmailSmtp);

  strcpy(pcgSenderEmailPath,"/ceda/debug");
  iGetConfigEntry(cgConfigFile, "AUTO_MVT", "SENDER_EMAIL_PATH", CFG_STRING, pcgSenderEmailPath) ; 
  dbg(TRACE,"SENDER_EMAIL_PATH IS <%s>",pcgSenderEmailPath);

  strcpy(pclString,"5");
  iGetConfigEntry(cgConfigFile, "AUTO_MVT", "PENDING_SELECT_MIN", CFG_STRING, pclString) ; 
  igPendingSelect = atoi(pclString);
  if( igPendingSelect <= 0 )
        igPendingSelect = 5;   /* default to 5 min chk */
  dbg( TRACE, "PENDING SELECT <%d> MINUTES", igPendingSelect );
    
  strcpy(pclString,"8");
  iGetConfigEntry(cgConfigFile, "AUTO_MVT", "MAX_ADDR_PER_LINE", CFG_STRING, pclString) ; 
  igMaxAddrPerLine = atoi(pclString);
  if( igMaxAddrPerLine <= 0 )
        igMaxAddrPerLine = 8;   /* default to 8 addr */
  dbg( TRACE, "MAX ADDR PER LINE <%d>", igMaxAddrPerLine );

  strcpy(pclString,"31");
  iGetConfigEntry(cgConfigFile, "AUTO_MVT", "MAX_ADDR_PER_TELEX", CFG_STRING, pclString) ; 
  igMaxAddrPerTelex = atoi(pclString);
  if( igMaxAddrPerTelex <= 0 )
      igMaxAddrPerTelex = 31; /* default to 31 addr */
  dbg( TRACE, "MAX ADDR PER TELEX <%d>", igMaxAddrPerTelex );

  memset( rgDELAY, 0, sizeof(DELAY_STRUCT)*2 );
  memset( &rgPAX, 0, sizeof(PAX_STRUCT) );

  strcpy(pclDefault,"ADID");
  ilGetRc = GetCfgLine(cgConfigFile,"AUTO_MVT","AFT_SQL_FLD_NAMES",CFG_STRING,pcgAftSqlFldNames,pclDefault);
  dbg(TRACE,"AFT_SQL_FLD_NAMES: \n<%s>",pcgAftSqlFldNames);

  strcpy(pclDefault,"ADID");
  ilGetRc = GetCfgLine(cgConfigFile,"AUTO_MVT","AFT_REC_FLD_NAMES",CFG_STRING,pcgAftRecFldNames,pclDefault);
  dbg(TRACE,"AFT_REC_FLD_NAMES: \n<%s>",pcgAftRecFldNames);

  strcpy(pclDefault,"ADID");
  ilGetRc = GetCfgLine(cgConfigFile,"AUTO_MVT","TPL_REC_FLD_NAMES",CFG_STRING,pcgTplRecFldNames,pclDefault);
  dbg(TRACE,"TPL_REC_FLD_NAMES: \n<%s>",pcgTplRecFldNames);

  strcpy(pclDefault,"ADID");
  ilGetRc = GetCfgLine(cgConfigFile,"AUTO_MVT","TPL_REC_FLD_TYPES",CFG_STRING,pcgTplRecFldTypes,pclDefault);
  dbg(TRACE,"TPL_REC_FLD_TYPES: \n<%s>",pcgTplRecFldTypes);

  strcpy(pclDefault,"ADID");
  ilGetRc = GetCfgLine(cgConfigFile,"AUTO_MVT","TPL_FLD_DAT_TYPES",CFG_STRING,pcgTplFldDatTypes,pclDefault);
  dbg(TRACE,"TPL_FLD_DAT_TYPES: \n<%s>",pcgTplFldDatTypes);

  strcpy(pclDefault,"MAND");
  ilGetRc = GetCfgLine(cgConfigFile,"AUTO_MVT","TPL_FLD_DAT_PROPS",CFG_STRING,pcgTplFldDatProps,pclDefault);
  dbg(TRACE,"TPL_FLD_DAT_PROPS: \n<%s>",pcgTplFldDatProps);

  strcpy(pclDefault,"URNO,TYPE,STAT,ALC2,ALC3,ADID,AUTO,MASI,MAPX,FSND,TSND,FLDD,TMED,TMPL,DELA,ALLS,HMST,HMTE,HMEM,RFLD");
  ilGetRc = GetCfgLine(cgConfigFile,"AUTO_MVT","MTP_REC_FLD_NAMES",CFG_STRING,pcgMtpRecFldNames,pclDefault);
  dbg(TRACE,"MTP_REC_FLD_NAMES: \n<%s>",pcgMtpRecFldNames);

  return ilRc;
}


/*******************************************************/
/*******************************************************/
static void SendTestEvents(int ipWhat)
{
  char pclCmd[64];
  char pclTbl[64];
  char pclSel[64];
  char pclFld[128];
  char pclDat[4096];
  if (ipWhat == 1)
  {
    dbg(TRACE,"SENDING TEST UPDATES TO FLIGHT");
    strcpy(pclCmd,"UFR");
    strcpy(pclTbl,"AFTTAB");
    strcpy(pclSel,"WHERE URNO=1253322236");
    /*
    strcpy(pclFld,"AIRA,REGN,VIAL");
    strcpy(pclDat,"20091217042500,9VSBA, BLRVOBG                                                                                                                 SHJOMSJ              ");
    */
    strcpy(pclFld,"OFBS,AIRA,REGN");
    strcpy(pclDat,"20091217050000,20091217051500,9VSBA");
    sprintf(pcgTwEnd,"%s,TAB,TLXGEN",pcgHomeHopo);
    (void) tools_send_info_flag (7800, 0, "ELSE", "", "ELSE", "", "", "TEST", pcgTwEnd, 
                                 pclCmd, pclTbl, pclSel, pclFld, pclDat, NETOUT_NO_ACK);

    /*
    strcpy(pclFld,"AIRA,REGN,VIAL");
    strcpy(pclDat,"20091217051000,9VSBA, BLRVOBG                                                                                                                 SHJOMSJ              ");
    */
    strcpy(pclFld,"AIRA,REGN");
    strcpy(pclDat,"20091217051000,9VSBA");
    (void) tools_send_info_flag (7800, 0, "ELSE", "", "ELSE", "", "", "TEST", pcgTwEnd, 
                                 pclCmd, pclTbl, pclSel, pclFld, pclDat, NETOUT_NO_ACK);
  }
  if (ipWhat == 2)
  {
    dbg(TRACE,"SENDING TEST UPDATES TO FLIGHT");
    strcpy(pclCmd,"UFR");
    strcpy(pclTbl,"AFTTAB");
    strcpy(pclSel,"WHERE URNO=1253322236");
    strcpy(pclFld,"BAAA");
    strcpy(pclDat,"150");
    (void) tools_send_info_flag (7800, 0, "ELSE", "", "ELSE", "", "", "TEST", pcgTwEnd, 
                                 pclCmd, pclTbl, pclSel, pclFld, pclDat, NETOUT_NO_ACK);
  }
  return;
}

/*******************************************************/
/*******************************************************/
static int HandleAutoSendTrigger(char *pcpFld, char *pcpDat, int ipCaller, char *pcpGetType)
{
  int ilRc = RC_SUCCESS;
  int ilGetRc = RC_SUCCESS;
  int ilGetSitaRc = RC_SUCCESS;
  int ilGetMailRc = RC_SUCCESS;
  char pclAftUrno[16] = "";
  char pclAftFtyp[16] = "";
  char pclIgnore[64] = "";
  char pclWanted[64] = "";
  int ilLoopCnt = 0;
  int ilGotAftData = FALSE;
  int ilGotTlxAddr = FALSE;
  int ilAskUser = FALSE;
  dbg(TRACE,"=================================================");
  dbg(TRACE,">>> HANDLE AUTO MVT TRIGGER === MAIN FUNCTION ===");
  dbg(TRACE,"-------------------------------------------------");
  ilRc = RC_SUCCESS;
  strcpy(pcgActTrgFldNames,pcpFld);
  (void) get_any_item(pcgActTrgNewValues, pcpDat, 1, "\n");
  (void) get_any_item(pcgActTrgOldValues, pcpDat, 2, "\n");
  dbg(TRACE,"FLD: <%s>",pcgActTrgFldNames);
  dbg(TRACE,"OLD: <%s>",pcgActTrgOldValues);
  dbg(TRACE,"NEW: <%s>",pcgActTrgNewValues);
  ilGetRc = GetFieldValue(pclAftUrno, pcgActTrgFldNames, "URNO", pcgActTrgNewValues);
  dbg(TRACE,"URNO <%s>",pclAftUrno);
  if (ilGetRc <= 0)
  {
    dbg(TRACE,"MISSING AFT.URNO in ACTION TRIGGER");
    ilRc = RC_FAIL;
  }
  ilGetRc = GetFieldValue(pclAftFtyp, pcgActTrgFldNames, "FTYP", pcgActTrgNewValues);
  dbg(TRACE,"FTYP <%s>",pclAftFtyp);
  if (ilGetRc > 0)
  {
    switch(pclAftFtyp[0])
    {
      case 'T':
        dbg(TRACE,"IGNORE TELEX CREATION FOR TOWINGS");
        ilRc = RC_IGNORE;
      break;
      case 'G':
        dbg(TRACE,"IGNORE TELEX CREATION FOR GROUND MOVEMENTS");
        ilRc = RC_IGNORE;
      break;
      default:
      break;
    }
  }
  else
  {
    /* currently tolerated */
    dbg(TRACE,"MISSING AFT.FTYP in ACTION TRIGGER");
  }

  strcpy(pclIgnore,"");
  strcpy(pclWanted,pcpGetType);
  ilGotAftData = FALSE;
  ilGotTlxAddr = FALSE;
  ilLoopCnt = 0;

  while (ilRc == RC_SUCCESS)
  {
    ilLoopCnt++;
    ilAskUser = FALSE;
    igSaveAsPending = FALSE;
    ilGetRc = RC_SUCCESS;
    ilRc = GetTemplateConfig(pclIgnore, pclWanted, ipCaller);
    if (ilRc == RC_SUCCESS)
    {
      if (ilGotAftData == FALSE)
      {
        ilRc = GetAftDataSnapshot(pclAftUrno);
        ilGetRc = CreateTlxTplRecord();
        ilGotAftData = TRUE;
      }

      ilGetRc = RC_SUCCESS;
      if (ilGotTlxAddr == FALSE)
      {
        ilGetMailRc = GetMailAddresses();
        ilGetSitaRc = GetSitaAddresses();
        ilGotTlxAddr = TRUE;
      }

      if ((ilGetSitaRc != RC_SUCCESS) && (ilGetMailRc != RC_SUCCESS))
      {
        ilAskUser = TRUE;
        dbg(TRACE,"MISSING SITA AND EMAIL ADDRESSES: MUST PROMPT THE USER");
      }

      ilAskUser = CheckTelexTemplateConditions(ipCaller, ilAskUser);

      CheckTelexTimeConditions(ipCaller);
   
      ilGetRc = CompleteTplDataSet();
      if (ilGetRc != RC_SUCCESS)
      {
        ilAskUser = TRUE;
        dbg(TRACE,"MISSING SOME DATA: MUST PROMPT THE USER");
      }

      if (igSaveAsPending == TRUE)
      {
        dbg(TRACE,"CREATING PENDING TELEX SNAPSHOT RECORD");
        CreateTlxSnapshotRec("P","C");
      }
      else
      {
        ilGetRc = RC_FAIL;
        if (ilAskUser == FALSE)
        {
          ilGetRc = PatchTemplateText();
        }
        if (ilGetRc == RC_SUCCESS)
        {
          dbg(TRACE,"CREATING PLAIN TEXT TELEX");
          CreateTlxSnapshotRec("S","S");
        }
        else
        {
          dbg(TRACE,"=======================================================");
          dbg(TRACE,"TEMPLATE EVALUATION RESULT: CREATE DATA SNAPSHOT RECORD");
          dbg(TRACE,"-------------------------------------------------------");
          CreateTlxSnapshotRec("C","C");
        }
      }
    }
    else
    {
      if (ilLoopCnt == 1)
      {
        dbg(TRACE,"AIRLINE NOT HANDLED (NO TEMPLATE FOUND)");
      }
      else
      {
        ilLoopCnt--;
        dbg(TRACE,"HANDLED %d TELEX TEMPLATES",ilLoopCnt);
      }
    }
    /* STOP LOOP */
    /* ilRc = RC_FAIL; */
  }
  return ilRc;
}
/*******************************************************/
/*******************************************************/
static int CheckTelexTemplateConditions(int ipCaller, int ipAskUser)
{
  int ilRc = TRUE;
  char pclMtpFlag[8] = "";
  ilRc = ipAskUser;
  dbg(TRACE,"=====================================================");
  dbg(TRACE,">>> HANDLE AUTO %s: CHECK TEMPLATE CONDITIONS     ===",pcgCurTplType);
  dbg(TRACE,"-----------------------------------------------------");

  GetFieldValue(pclMtpFlag, pcgMtpRecFldNames, "AUTO", pcgMtpRecFldValues);
  dbg(TRACE,"MPTTAB.AUTO FIELD VALUE = <%s>",pclMtpFlag);
  if (pclMtpFlag[0] == 'N') ilRc = TRUE;

  GetFieldValue(pclMtpFlag, pcgMtpRecFldNames, "MASI", pcgMtpRecFldValues);
  dbg(TRACE,"MPTTAB.AUTO FIELD VALUE = <%s>",pclMtpFlag);
  if (pclMtpFlag[0] == 'Y') ilRc = TRUE;

  GetFieldValue(pclMtpFlag, pcgMtpRecFldNames, "MAPX", pcgMtpRecFldValues);
  dbg(TRACE,"MPTTAB.AUTO FIELD VALUE = <%s>",pclMtpFlag);
  if (pclMtpFlag[0] == 'Y') ilRc = TRUE;

  if (ilRc == TRUE)
  {
    dbg(TRACE,"THE USER WILL BE PROMPTED");
  }
  dbg(TRACE,"-----------------------------------------------------");
  return ilRc;
}

/*******************************************************/
/*******************************************************/
static int CreateTlxSnapshotRec(char *pcpTypeCode, char *pcpStatCode)
{
  int ilRc = RC_SUCCESS;
  char pclAftUrno[16] = "";
  char pclAftAdid[4] = "";
  char pclTplAddr[16] = "";
  char pclOutRcv[32] = "";
  char pclOutDst[32] = "";
  char pclOutTws[32] = "";
  char pclOutTwe[32] = "";
 
  char pclOutCmd[8] = "";
  char pclOutTbl[8] = "";
  char pclOutSel[32] = "";
  char pclOutFld[256] = "";
  char pclAftFldLst[256] = "";
  char pclTlxFldLst[256] = "";
  char pclOutDat[4096*3] = "";
  char pclFldName[256] = "";
  char pclFldData[4096] = "";
  /* TXT1 must be big enough */
  /* to keep the whole text! */
  char pclTlxTxt1[4096*2] = "";
  char pclTlxTxt2[4096] = "";

  int ilItm = 0;
  int ilLen = 0;
  int ilNextItmNo = 0;
  int ilOutFldPos = 0;
  int ilOutDatPos = 0;
  int ilOutTxtFldPos = 0;
  int ilOutTxtDatPos = 0;
  int ilFldPos = 0;
  int ilDatPos = 0;
  int ilCurAddr = 0;
  int ilAddrLen = 0;

  dbg(TRACE,"=====================================================");
  dbg(TRACE,">>> HANDLE AUTO %s: CREATE TELEX SNAPSHOT RECORD ===",pcgCurTplType);
  dbg(TRACE,"-----------------------------------------------------");
  if (pcpTypeCode[0] == 'C')
  {
    dbg(TRACE,">>> CREATE TELEX SNAPSHOT RECORD FOR USER ACTION  ===");
    dbg(TRACE,"-----------------------------------------------------");
  }
  else if (pcpTypeCode[0] == 'P')
  {
    dbg(TRACE,">>> CREATE TELEX SNAPSHOT RECORD AS PENDING TELEX ===");
    dbg(TRACE,"-----------------------------------------------------");
  }
  else if (pcpTypeCode[0] == 'S')
  {
    dbg(TRACE,">>> CREATE PLAIN TEXT TELEX TO BE SENT RIGHT NOW  ===");
    dbg(TRACE,"-----------------------------------------------------");
  }
  strcpy(pclOutRcv,"TlxGen");
  strcpy(pclOutDst,"CEDA");
  strcpy(pclOutTws,"--");
  strcpy(pclOutTwe,"XXX,TAB,TlxGen");
  strcpy(pclOutCmd,"IBT");
  strcpy(pclOutTbl,"TLXTAB");
  strcpy(pclOutSel,"          ");
  sprintf(pclOutTwe,"%s,TAB,TLXGEN",pcgHomeHopo);

  GetFieldValue(pclAftAdid, pcgAftSqlFldNames, "ADID", pcgAftSqlFldValues);
  if (pclAftAdid[0] == 'A')
  {
    strcpy(pclAftFldLst,"URNO,ALC3,FLTN,FLNS,FKEY,FLDA");
    strcpy(pclTlxFldLst,"FLNU,ALC3,FLTN,FLNS,FKEY,FLDA");
  }
  else
  {
    strcpy(pclAftFldLst,"URNO,ALC3,FLTN,FLNS,FKEY,FLDA");
    strcpy(pclTlxFldLst,"FLNU,ALC3,FLTN,FLNS,FKEY,FLDA");
  }

  ilOutFldPos = 0;
  ilOutDatPos = 0;
  StrgPutStrg (pclOutFld, &ilOutFldPos, "SERE", 0, -1, ",");
  StrgPutStrg (pclOutDat, &ilOutDatPos, pcpTypeCode, 0, -1, ",");

  StrgPutStrg (pclOutFld, &ilOutFldPos, "STAT", 0, -1, ",");
  StrgPutStrg (pclOutDat, &ilOutDatPos, pcpStatCode, 0, -1, ",");

  StrgPutStrg (pclOutFld, &ilOutFldPos, "TTYP,WSTA,USEC,PRFL", 0, -1, ",");
  StrgPutStrg (pclOutDat, &ilOutDatPos, "MVT, ,TlxGen, ", 0, -1, ",");

  StrgPutStrg (pclOutFld, &ilOutFldPos, "STYP", 0, -1, ",");
  GetFieldValue(pclFldData, pcgMtpRecFldNames, "TYPE", pcgMtpRecFldValues);
  StrgPutStrg (pclOutDat, &ilOutDatPos, pclFldData, 0, -1, ",");

  StrgPutStrg (pclOutFld, &ilOutFldPos, pclTlxFldLst, 0, -1, ",");
  GetFieldValues(pclFldData, pcgAftSqlFldNames, pclAftFldLst, pcgAftSqlFldValues);
  StrgPutStrg (pclOutDat, &ilOutDatPos, pclFldData, 0, -1, ",");

  StrgPutStrg (pclOutFld, &ilOutFldPos, "TIME", 0, -1, ",");
  GetServerTimeStamp( "UTC", 1, 0, pclFldData );
  StrgPutStrg (pclOutDat, &ilOutDatPos, pclFldData, 0, -1, ",");

  if (pcpTypeCode[0] == 'P')
  {
    StrgPutStrg (pclOutFld, &ilOutFldPos, "TKEY", 0, -1, ",");
    StrgPutStrg (pclOutDat, &ilOutDatPos, pcgSendTlxTime, 0, -1, ",");
  }
  else
  {
    StrgPutStrg (pclOutFld, &ilOutFldPos, "TKEY", 0, -1, ",");
    StrgPutStrg (pclOutDat, &ilOutDatPos, " ", 0, -1, ",");
  }

  StrgPutStrg (pclOutFld, &ilOutFldPos, "MSTX,BEME,MSNO,TXNO", 0, -1, ",");
  StrgPutStrg (pclOutDat, &ilOutDatPos, " , , , ", 0, -1, ",");

  StrgPutStrg (pclOutFld, &ilOutFldPos, "MORE", 0, -1, ",");
  StrgPutStrg (pclOutDat, &ilOutDatPos, "0", 0, -1, ",");

  StrgPutStrg (pclOutFld, &ilOutFldPos, "CDAT", 0, -1, ",");
  GetServerTimeStamp( "UTC", 1, 0, pclFldData );
  StrgPutStrg (pclOutDat, &ilOutDatPos, pclFldData, 0, -1, ",");

  StrgPutStrg (pclOutFld, &ilOutFldPos, "HOPO", 0, -1, ",");
  strcpy(pclFldData,pcgHomeHopo);
  StrgPutStrg (pclOutDat, &ilOutDatPos, pclFldData, 0, -1, ",");

  pclOutFld[ilOutFldPos] = 0x00;
  pclOutDat[ilOutDatPos] = 0x00;
  ilOutTxtFldPos = ilOutFldPos;
  ilOutTxtDatPos = ilOutDatPos;

  if (pcpTypeCode[0] == 'S')
  {
    /* Prepare Plain Text Telex for Sending */
    strcpy(pcgAllSitaAddr,pcgMadSitaAddr);
    igAllSitaCount = igMadSitaCount;
    str_trm_all(pcgAllSitaAddr, " ", TRUE);
    ilAddrLen = strlen(pcgAllSitaAddr);
    if (ilAddrLen <= 0)
    {
      igAllSitaCount = 0;
    }
    pcgTplSitaAddr[0] = 0x00;
    GetFieldValue(pcgTplSitaAddr, pcgMtpRecFldNames, "HMTE", pcgMtpRecFldValues);
    str_trm_all(pcgTplSitaAddr, " ", TRUE);
    ReplaceStringInString(pcgTplSitaAddr, " ", ",");
    igTplSitaCount = field_count(pcgTplSitaAddr);
    ilAddrLen = strlen(pcgTplSitaAddr);
    if (ilAddrLen <= 0)
    {
      igTplSitaCount = 0;
    }
    if (igTplSitaCount > 0)
    {
      dbg(TRACE,"APPENDING %d HOME TELEX ADDRESSES",igTplSitaCount);
      dbg(TRACE,"\n<%s>",pcgTplSitaAddr);
      /*
      if (igAllSitaCount > 0)
      {
      */
        for (ilItm=1;ilItm<=igTplSitaCount;ilItm++)
        {
          ilLen = get_real_item(pclTplAddr, pcgTplSitaAddr, ilItm) ;
          if (ilLen > 0)
          {
            if (strstr(pcgAllSitaAddr, pclTplAddr) == NULL)
            {
              if (igAllSitaCount > 0)
              {
                strcat(pcgAllSitaAddr,",");
              }
              strcat(pcgAllSitaAddr,pclTplAddr);
              igAllSitaCount++;
            }
          }
        }
      /*
      }
      */
    }
    dbg(TRACE,"HAVING %d SITA ADDRESSES",igAllSitaCount);
    dbg(TRACE,"\n<%s>",pcgAllSitaAddr);
    if (igEmailIsReadyToUse == TRUE)
    {
      /* MAIL Addresses MADTAB are separated by LF (\n) */
      strcpy(pcgAllMailAddr,pcgMadMailAddr);
      igAllMailCount = igMadMailCount;
      str_trm_all(pcgAllMailAddr, " ", TRUE);
      ilAddrLen = strlen(pcgAllMailAddr);
      if (ilAddrLen <= 0)
      {
        igAllMailCount = 0;
      }
      pcgTplMailAddr[0] = 0x00;
      GetFieldValue(pcgTplMailAddr, pcgMtpRecFldNames, "HMEM", pcgMtpRecFldValues);
      str_trm_all(pcgTplMailAddr, " ", TRUE);
      /* MAIL Addresses MTPTAB are separated by Space */
      /* but we need a comma for the item function */
      ReplaceStringInString(pcgTplMailAddr, " ", ",");
      igTplMailCount = field_count(pcgTplMailAddr);
      ilAddrLen = strlen(pcgTplMailAddr);
      if (ilAddrLen <= 0)
      {
        igTplMailCount = 0;
      }
      if (igTplMailCount > 0)
      {
        dbg(TRACE,"APPENDING %d HOME MAIL ADDRESSES",igTplMailCount);
        dbg(TRACE,"\n%s",pcgTplMailAddr);
        /*
        if (igAllMailCount > 0)
        {
        */
          for (ilItm=1;ilItm<=igTplMailCount;ilItm++)
          {
            ilLen = get_real_item(pclTplAddr, pcgTplMailAddr, ilItm) ;
            if (ilLen > 0)
            {
              if (strstr(pcgAllMailAddr, pclTplAddr) == NULL)
              {
                if (igAllMailCount > 0)
                {
                  strcat(pcgAllMailAddr,"\n");
                }
                strcat(pcgAllMailAddr,pclTplAddr);
                igAllMailCount++;
              }
            }
          }
        /*
        }
        */
      }
      dbg(TRACE,"HAVING %d MAIL ADDRESSES",igAllMailCount);
      dbg(TRACE,"\n%s",pcgAllMailAddr);
    }
  }
  dbg(TRACE,"NOW CREATING THE TELEX TEXT FIELDS");
  ilNextItmNo = 1;
  while (ilNextItmNo > 0)
  {
    ilOutFldPos = ilOutTxtFldPos;
    ilOutDatPos = ilOutTxtDatPos;
    StrgPutStrg (pclOutFld, &ilOutFldPos, "TXT1,TXT2", 0, -1, ",");
    ilNextItmNo = GetTlxTextFields (pcpTypeCode, ilNextItmNo, pclTlxTxt1, pclTlxTxt2);
    StrgPutStrg (pclOutDat, &ilOutDatPos, pclTlxTxt1, 0, -1, ",");
    StrgPutStrg (pclOutDat, &ilOutDatPos, pclTlxTxt2, 0, -1, ",");
    ilOutFldPos--;
    ilOutDatPos--;
    pclOutFld[ilOutFldPos] = 0x00;
    pclOutDat[ilOutDatPos] = 0x00;
    dbg(TRACE,"TELEX CREATED AND SENT TO ROUTER\nTLX FLD LIST <%s>\nTLX DAT LIST <%s>", pclOutFld,pclOutDat);
    SendCedaEvent(1200,0,pclOutDst,pclOutRcv,pclOutTws,pclOutTwe,
                         pclOutCmd, pclOutTbl, pclOutSel, pclOutFld, pclOutDat,
                         "", 4, NETOUT_NO_ACK);
  }
  dbg(TRACE,"-----------------------------------------------------");

  return ilRc;
}

/*******************************************************/
/*******************************************************/
static int GetTlxTextFields(char *pcpTypeCode, int ipFirstItmNo, char *pcpTxt1, char *pcpTxt2)
{
  int ilRc = RC_SUCCESS;
  int ilGetRc = RC_SUCCESS;
  char pclBuff[4096*2] = "";
  char pclAddrTo[4096] = "";
  char pclAddrCc[4096] = "";
  char pclGrpSep[4] = " ";
  char pclSep2[4] = "";
  char pclTelexHeading[64] = "";
  char pclTelexTail[16] = "";
  char pclNewLine[4] = "";
  char pclCleanLook[4] = "";
  char pclCleanPaste[4] = "";
  int ilNextItmNo = 0;
  int ilPos1 = 0;
  int ilPos2 = 0;
  int ilLen = 0;
  pclGrpSep[0] = 30;
  sprintf(pclNewLine, "%c%c", 0x0D, 0x0A);
  /* sprintf(pclNewLine,"\r\n"); */
  if ((pcpTypeCode[0] == 'C') || (pcpTypeCode[0] == 'P'))
  {
    /* Data SnapShot for Client App */
    /* Data SnapShot for Pending Telex */
    strcpy(pclBuff,pcgTplRecFldNames);
    dbg(TRACE,"TPL FLD <%s>",pcgTplRecFldNames);
    PatchTextClientToServer(pclBuff);
    StrgPutStrg(pcpTxt1, &ilPos1, pclBuff, 0, -1, pclGrpSep);

    strcpy(pclBuff,pcgTplRecFldValues);
    dbg(TRACE,"TPL DAT <%s>",pcgTplRecFldValues);
    dbg(TRACE,"-----------------------------------------------------");
    PatchTextClientToServer(pclBuff);
    StrgPutStrg(pcpTxt1, &ilPos1, pclBuff, 0, -1, pclGrpSep);

    ilPos1--;
    pcpTxt1[ilPos1] = 0x00;
    ilNextItmNo = 0;
  }
  if (pcpTypeCode[0] == 'C')
  {
    /* Addresses SnapShot for Client App */
    ilPos2 = 0;
    GetFieldValue(pclBuff, pcgMtpRecFldNames, "URNO", pcgMtpRecFldValues);
    dbg(TRACE,"MTP REC URNO <%s>",pclBuff);
    StrgPutStrg(pcpTxt2, &ilPos2, pclBuff, 0, -1, pclGrpSep);

    strcpy(pclBuff,pcgMadSitaAddr);
    PatchTextClientToServer(pclBuff);
    StrgPutStrg(pcpTxt2, &ilPos2, pclBuff, 0, -1, pclGrpSep);
    strcpy(pclBuff,pcgMadMailAddr);
    PatchTextClientToServer(pclBuff);
    StrgPutStrg(pcpTxt2, &ilPos2, pclBuff, 0, -1, pclGrpSep);

    ilPos2--;
    pcpTxt2[ilPos2] = 0x00;
    ilNextItmNo = 0;
  }
  if (pcpTypeCode[0] == 'S')
  {
    /* BERNI */
    /* Plain Text Telex to be Sent */
    ilNextItmNo = ipFirstItmNo;
    sprintf(pclTelexHeading, "%c%c%cQU", 0x0D, 0x0A, 0x01);
    ilPos1 = 0;
    StrgPutStrg(pcpTxt1, &ilPos1, pclTelexHeading, 0, -1, " ");
    ilNextItmNo = GetSitaAddrBlock(ilNextItmNo,pclAddrTo,pclAddrCc);
    StrgPutStrg(pcpTxt1, &ilPos1, pclAddrTo, 0, -1, pclNewLine);
    GetServerTimeStamp("UTC", 1, 0, pclBuff);
    pclBuff[12] = 0x00;
    sprintf(pclTelexHeading,".%s %s",pcgSenderTelexAddr,&pclBuff[6]);
    StrgPutStrg(pcpTxt1, &ilPos1, pclTelexHeading, 0, -1, pclNewLine);
    StrgPutStrg(pcpTxt1, &ilPos1, pcgTlxTmplPatched, 0, -1, pclNewLine);
    ilLen = strlen(pclAddrCc);
    if (ilLen > 0)
    {
      StrgPutStrg(pcpTxt1, &ilPos1, "SI CC", 0, -1, pclNewLine);
      StrgPutStrg(pcpTxt1, &ilPos1, pclAddrCc, 0, -1, pclNewLine);
    }
    if (igEmailIsReadyToUse == TRUE)
    {
      /* Attach eMail address list */
      /* only in the first telex.  */
      if (ipFirstItmNo == 1)
      {
        ilLen = strlen(pcgAllMailAddr);
        if (ilLen > 0)
        {
          /* All MAIL Addresses are separated by LF (\n) but */
          /* we need CRLF for proper function of TelexPool. */
          MyDebugSwitch = TRUE;
          MyDebugSwitch = FALSE;
          ReplaceStringInString(pcgAllMailAddr, "\n", pclNewLine);
          MyDebugSwitch = FALSE;

          StrgPutStrg(pcpTxt1, &ilPos1, "<EMAIL_ADDR>", 0, -1, pclNewLine);
          StrgPutStrg(pcpTxt1, &ilPos1, pcgAllMailAddr, 0, -1, pclNewLine);
          StrgPutStrg(pcpTxt1, &ilPos1, "</EMAIL_ADDR>", 0, -1, pclNewLine);
        }
      }
    }
    sprintf(pclTelexTail, "%c%c%c ", 0x0D, 0x0A, 0x03);
    StrgPutStrg(pcpTxt1, &ilPos1, pclTelexTail, 0, -1, " ");
    ilPos1--;
    pcpTxt1[ilPos1] = 0x00;
    sprintf(pclCleanLook, "%c%c", 0x0D, 0x0D);
    sprintf(pclCleanPaste, "%c", 0x0D);
    (void) CleanDoubleBytes(pcpTxt1, pclCleanLook, pclCleanPaste);

    PatchTextClientToServer(pcpTxt1);
    ilLen = strlen(pcpTxt1);
    if (ilLen > igTlxTxtSize)
    {
      ilPos1 = igTlxTxtSize - 1;
      while (pcpTxt1[ilPos1] == ' ')
      {
        ilPos1--;
      }
      ilPos1++;
      ilLen -= ilPos1;
      if (ilLen > igTlxTxtSize)
      {
        ilPos2 = ilPos1 + igTlxTxtSize - 1;
        pcpTxt1[ilPos2] = '\0';
      }
      strcpy(pcpTxt2,&(pcpTxt1[ilPos1]));
      pcpTxt1[ilPos1] = '\0';
    }
    else
    {
      ilPos2 = 0;
      pcpTxt2[ilPos2] = 0x00;
    }
  }
  return ilNextItmNo;
}

/*******************************************************/
/*******************************************************/
static int CleanDoubleBytes(char *pcpText, char *pcpFind, char *pcpSet)
{
  int ilLen1 = 0;
  int ilLen2 = 0;
  ilLen1 = strlen(pcpText);
  ReplaceStringInString(pcpText, pcpFind, pcpSet);
  ilLen2 = strlen(pcpText);
  if (ilLen1 != ilLen2)
  {
    dbg(TRACE,"CLEAN DOUBLE 0D0D: LEN1=%d LEN2=%d",ilLen1,ilLen2);
  }
  return ilLen2;
}

/*******************************************************/
/*******************************************************/
static int GetSitaAddrBlock(int ipFirstItmNo, char *pcpAddrTo, char *pcpAddrCc)
{
  int ilNextItmNo = 0;
  char pclAddr[16] = "";
  char pclAddrSep[4] = "";
  int ilToPos = 0;
  int ilCcPos = 0;
  int ilToCnt = 0;
  int ilCcCnt = 0;
  int ilMaxPerLine = 8;
  int ilMaxPerBlock = 31;
  int ilBgnItm = 0;
  int ilEndItm = 0;
  int ilCurItm = 0;
  int ilBufLen = 0;
  /* BERNI */
  /* ReplaceStringInString(pcgMadSitaAddr, "\n", ","); */
  /* dbg(TRACE,"SITA: %d ADDRESSES\n%s",igMadSitaCount,pcgMadSitaAddr); */
  ilToPos = 0;
  ilBgnItm = ipFirstItmNo;
  ilEndItm = ilBgnItm + ilMaxPerBlock - 1;
  if (igAllSitaCount > 0)
  {
    if (ilEndItm > igAllSitaCount) ilEndItm = igAllSitaCount;
    ilToPos = 0;
    ilToCnt = 0;
    for (ilCurItm=ilBgnItm;ilCurItm<=ilEndItm;ilCurItm++)
    {
      ilToCnt++;
      get_real_item(pclAddr, pcgAllSitaAddr, ilCurItm) ;
      if (ilToCnt < ilMaxPerLine)
      {
        StrgPutStrg(pcpAddrTo, &ilToPos, pclAddr, 0, -1, " ");
      }
      else
      {
        StrgPutStrg(pcpAddrTo, &ilToPos, pclAddr, 0, -1, "\r\n");
        ilToCnt = 0;
      }
    }
  }
  StrgPutStrg(pcpAddrTo, &ilToPos, pcgSenderTelexAddr, 0, -1, "\r\n");
  if (ilToPos > 1) 
  {
    ilToPos--;
    ilToPos--;
  }
  pcpAddrTo[ilToPos] = 0x00;
  ilNextItmNo = ilEndItm + 1;
  if (ilNextItmNo >= igAllSitaCount)  ilNextItmNo = 0;

  ilCcPos = 0;
  ilCcCnt = 0;
  if (igAllSitaCount > 0)
  {
    for (ilCurItm=1;ilCurItm<ilBgnItm;ilCurItm++)
    {
      ilCcCnt++;
      get_real_item(pclAddr, pcgAllSitaAddr, ilCurItm) ;
      if (ilCcCnt < ilMaxPerLine)
      {
        StrgPutStrg(pcpAddrCc, &ilCcPos, pclAddr, 0, -1, " ");
      }
      else
      {
        StrgPutStrg(pcpAddrCc, &ilCcPos, pclAddr, 0, -1, "\r\n");
        ilCcCnt = 0;
      }
    }
    for (ilCurItm=ilEndItm+1;ilCurItm<=igAllSitaCount;ilCurItm++)
    {
      ilCcCnt++;
      get_real_item(pclAddr, pcgAllSitaAddr, ilCurItm) ;
      if (ilCcCnt < ilMaxPerLine)
      {
        StrgPutStrg(pcpAddrCc, &ilCcPos, pclAddr, 0, -1, " ");
      }
      else
      {
        StrgPutStrg(pcpAddrCc, &ilCcPos, pclAddr, 0, -1, "\r\n");
        ilCcCnt = 0;
      }
    }
  }
  if (ilCcPos > 1)
  {
    ilCcPos--;
    ilCcPos--;
  }
  pcpAddrCc[ilCcPos] = 0x00;
  return ilNextItmNo;
}

/*******************************************************/
/*******************************************************/
static int CompleteTplDataSet(void)
{
  int ilRc = RC_SUCCESS;
  int ilGetRc = RC_SUCCESS;
  char pclAftDcd1[16] = "";
  char pclAftDcd2[16] = "";
  char pclAftDtd1[16] = "";
  char pclAftDtd2[16] = "";
  char pclAftDeld[256] = "";
  char pclAftPax1[16] = "";
  char pclAftPax2[16] = "";
  char pclAftPax3[16] = "";
  char pclAftPxsi[16] = "";
  dbg(TRACE,"==========================================");
  dbg(TRACE,">>> HANDLE AUTO %s: COMPLETE DATA SET ===",pcgCurTplType);
  dbg(TRACE,"------------------------------------------");

  strcpy(pcgTplRecFldValues,pcgTplRecFldValuesSve);

  GetFieldValue(pclAftDcd1, pcgAftSqlFldNames, "DCD1", pcgAftSqlFldValues);
  GetFieldValue(pclAftDcd2, pcgAftSqlFldNames, "DCD2", pcgAftSqlFldValues);
  GetFieldValue(pclAftDtd1, pcgAftSqlFldNames, "DTD1", pcgAftSqlFldValues);
  GetFieldValue(pclAftDtd2, pcgAftSqlFldNames, "DTD2", pcgAftSqlFldValues);
  GetFieldValue(pclAftDeld, pcgAftSqlFldNames, "DELD", pcgAftSqlFldValues);
  ilGetRc = EvaluateDelayInfo(pclAftDcd1,pclAftDcd2,pclAftDtd1,pclAftDtd2,pclAftDeld);
  if (ilGetRc != RC_SUCCESS) ilRc = RC_FAIL;
  ReplaceStringInString(pcgTplRecFldValues, "{DCD1}", pclAftDcd1);
  ReplaceStringInString(pcgTplRecFldValues, "{DCD2}", pclAftDcd2);
  ReplaceStringInString(pcgTplRecFldValues, "{DTD1}", pclAftDtd1);
  ReplaceStringInString(pcgTplRecFldValues, "{DTD2}", pclAftDtd2);
  ReplaceStringInString(pcgTplRecFldValues, "{DELD}", pclAftDeld);

  GetFieldValue(pclAftPax1, pcgAftSqlFldNames, "PAX1", pcgAftSqlFldValues);
  GetFieldValue(pclAftPax2, pcgAftSqlFldNames, "PAX2", pcgAftSqlFldValues);
  GetFieldValue(pclAftPax3, pcgAftSqlFldNames, "PAX3", pcgAftSqlFldValues);
  GetFieldValue(pclAftPxsi, pcgAftSqlFldNames, "PXSI", pcgAftSqlFldValues);
  ilGetRc = EvaluatePaxInfo(pclAftPax1,pclAftPax2,pclAftPax3,pclAftPxsi);
  if (ilGetRc != RC_SUCCESS) ilRc = RC_FAIL;
  ReplaceStringInString(pcgTplRecFldValues, "{PAX1}", pclAftPax1);
  ReplaceStringInString(pcgTplRecFldValues, "{PAX2}", pclAftPax2);
  ReplaceStringInString(pcgTplRecFldValues, "{PAX3}", pclAftPax3);
  ReplaceStringInString(pcgTplRecFldValues, "{PXSI}", pclAftPxsi);

  dbg(TRACE,"TEMPLATE DATA SET COMPLETED");
  dbg(TRACE,"TPL REC FLD LIST <%s>",pcgTplRecFldNames);
  dbg(TRACE,"TPL REC DAT LIST <%s>",pcgTplRecFldValues);
  return ilRc;
}


/*******************************************************/
/*******************************************************/
static int GetAftDataSnapshot(char *pcpAftUrno)
{
  int ilRc = RC_SUCCESS;
  int ilGetRc = RC_SUCCESS;
  char pclSqlKey[64] = "";
  dbg(TRACE,"=================================================");
  dbg(TRACE,">>> HANDLE AUTO MVT: GET AFTTAB DATA SNAPSHOT ===");
  dbg(TRACE,"-------------------------------------------------");
  sprintf(pclSqlKey,"WHERE URNO=%s",pcpAftUrno);
  ilGetRc = HandleSqlSelectRows("AFTTAB", pclSqlKey, pcgAftSqlFldNames, pcgAftSqlFldValues, 1);
  dbg(TRACE,"AFT FLD <%s>",pcgAftSqlFldNames);
  dbg(TRACE,"AFT DAT <%s>",pcgAftSqlFldValues);
  return ilRc;
}


/*******************************************************/
/*******************************************************/
static int CreateTlxTplRecord(void)
{
  int ilRc = RC_SUCCESS;
  int ilGetRc = RC_SUCCESS;
  char pclTplFldName[16];
  char pclAftFldName[16];
  char pclTplFldType[16];
  char pclAftFldValue[4096];
  char pclTplFldValue[4096];
  int ilPos = 0;
  int ilMax = 0;
  int ilFld = 0;
  dbg(TRACE,"=============================================");
  dbg(TRACE,">>> HANDLE AUTO MVT: CREATE TEMPLATE DATA ===");
  dbg(TRACE,"---------------------------------------------");
  ilMax = field_count(pcgTplRecFldNames);
  for (ilFld=1;ilFld<=ilMax;ilFld++)
  {
    (void) get_any_item(pclTplFldName, pcgTplRecFldNames, ilFld, ",");
    (void) get_any_item(pclAftFldName, pcgAftRecFldNames, ilFld, ",");
    (void) get_any_item(pclTplFldType, pcgTplRecFldTypes, ilFld, ",");
    (void) GetFieldValue(pclAftFldValue, pcgAftSqlFldNames, pclAftFldName, pcgAftSqlFldValues);
    if (strcmp(pclTplFldType,"TEXT")==0)
    {
      dbg(DEBUG,"TYPE OF FIELD <%s> IS <%s>",pclTplFldName,pclTplFldType);
      strcpy(pclTplFldValue,pclAftFldValue);
    } 
    else if (strcmp(pclTplFldType,"TIME")==0)
    {
      dbg(DEBUG,"TYPE OF FIELD <%s> IS <%s>",pclTplFldName,pclTplFldType);
      strcpy(pclTplFldValue,pclAftFldValue);
    }
    else if (strcmp(pclTplFldType,"FLNO")==0)
    {
      dbg(DEBUG,"TYPE OF FIELD <%s> IS <%s>",pclTplFldName,pclTplFldType);
      strcpy(pclTplFldValue,pclAftFldValue);
    }
    else if (strcmp(pclTplFldType,"NSTN")==0)
    {
      dbg(DEBUG,"TYPE OF FIELD <%s> IS <%s>",pclTplFldName,pclTplFldType);
      GetNextStationCode(pclTplFldValue);
    }
    else if (strcmp(pclTplFldType,"EANS")==0)
    {
      dbg(TRACE,"TYPE OF FIELD <%s> IS <%s>",pclTplFldName,pclTplFldType);
      GetNextStationETA(pclTplFldValue);
    }
    else if (strcmp(pclTplFldType,"LEGS")==0)
    {
      dbg(DEBUG,"TYPE OF FIELD <%s> IS <%s>",pclTplFldName,pclTplFldType);
      GetShortStationList(pclTplFldValue,"","","/");
    }
    else if (strcmp(pclTplFldType,"DLYC")==0)
    {
      dbg(DEBUG,"TYPE OF FIELD <%s> IS <%s>",pclTplFldName,pclTplFldType);
      dbg(DEBUG,"SET DATA VARIABLE FOR EVALUTION RESULT <DELAY CODE>");
      sprintf(pclTplFldValue,"{%s}",pclAftFldName);
    }
    else if (strcmp(pclTplFldType,"DLYT")==0)
    {
      dbg(DEBUG,"TYPE OF FIELD <%s> IS <%s>",pclTplFldName,pclTplFldType);
      dbg(DEBUG,"SET DATA VARIABLE FOR EVALUTION RESULT <DELAY TIME>");
      sprintf(pclTplFldValue,"{%s}",pclAftFldName);
    }
    else if (strcmp(pclTplFldType,"PXNO")==0)
    {
      dbg(TRACE,"TYPE OF FIELD <%s> IS <%s>",pclTplFldName,pclTplFldType);
      dbg(TRACE,"SET DATA VARIABLE FOR EVALUTION RESULT <PAX SUMMARY>");
      sprintf(pclTplFldValue,"{%s}",pclAftFldName);
    }
    else
    {
      dbg(TRACE,"UNDEFINED TYPE OF FIELD <%s> <%s>",pclTplFldName,pclTplFldType);
      strcpy(pclTplFldValue,pclAftFldValue);
    }

    dbg(DEBUG,"AFT FIELD NAME <%s> VALUE <%s>",pclAftFldName,pclAftFldValue);
    dbg(DEBUG,"TPL FIELD NAME <%s> VALUE <%s>",pclTplFldName,pclTplFldValue);
    dbg(DEBUG," ");
    StrgPutStrg(pcgTplRecFldValues, &ilPos, pclTplFldValue, 0, -1, ",");

  }

  ilPos--;
  pcgTplRecFldValues[ilPos] = 0x00;
  strcpy(pcgTplRecFldValuesSve,pcgTplRecFldValues);

  dbg(TRACE,"FLD LIST <%s>",pcgTplRecFldNames);
  dbg(TRACE,"DAT LIST <%s>",pcgTplRecFldValues);
  return ilRc;
}

/*******************************************************/
/*******************************************************/
static int PatchTemplateText(void)
{
  int ilRc = RC_SUCCESS;
  int ilGetRc = RC_SUCCESS;
  char pclText[4096];
  char pclSearch[32];
  char pclReplace[256];
  char pclTplFldName[16];
  char pclTplFldType[16];
  char pclTplFldValue[256];
  char pclTplRplValue[256];
  
  long llTmpLen = 0;
  int ilTxtLen = 0;
  int ilPos = 0;
  int ilMax = 0;
  int ilFld = 0;
  int ilPatchIt = TRUE;

  dbg(TRACE,"============================================");
  dbg(TRACE,">>> HANDLE AUTO MVT: PATCH TEMPLATE TEXT ===");
  dbg(TRACE,"--------------------------------------------");
  strcpy(pclText,pcgTlxTmplText);
  /* Test ONLY */
  /*
  sprintf(pclText,"\n\nMVT\n"
                  "[FLNO]/[AIRB|DD]\n"
                  "AD[OFBL|HHMM]/[AIRB|HHMM]\n"
                  "AO[OFBL|HHMM]/[AIRB|HHMM]\n"
                  "AE[OFBL|DDHHMM]/[AIRB|DDHHMM]\n"
                  "SEAS [OFBL]/[AIRB]\n"
                  "SI [FLNO]/[AIRB|DD]\n");
  */

  dbg(TRACE,"CURRENT TEMPLATE \n%s",pclText);
  ilMax = field_count(pcgTplRecFldNames);
  for (ilFld=1;ilFld<=ilMax;ilFld++)
  {
    ilPatchIt = TRUE;
    (void) get_any_item(pclTplFldName, pcgTplRecFldNames, ilFld, ",");
    (void) get_any_item(pclTplFldType, pcgTplFldDatTypes, ilFld, ",");
    ilTxtLen = get_any_item(pclTplFldValue, pcgTplRecFldValues, ilFld, ",");
    if (ilTxtLen <= 0)
    {
      /* To do: Check if mandatory */
      ilPatchIt = FALSE;
      dbg(TRACE,"NOT PATCHING EMPTY VALUE OF <%s> <%s>",pclTplFldName,pclTplFldType);
    }
    if (ilPatchIt == TRUE)
    {
      if (strcmp(pclTplFldType,"TEXT")==0)
      {
        sprintf(pclSearch,"[%s]",pclTplFldName);
        dbg(DEBUG,"DATA TYPE OF TPL FIELD <%s> IS <%s> = PATCH %s =",pclTplFldName,pclTplFldType,pclSearch);
        strcpy(pclReplace,pclTplFldValue);
        ilGetRc = ReplaceStringInString(pclText, pclSearch, pclReplace);
      } 
      else if (strcmp(pclTplFldType,"TIME")==0)
      {
        sprintf(pclSearch,"[%s]",pclTplFldName);
        dbg(DEBUG,"DATA TYPE OF TPL FIELD <%s> IS <%s> = PATCH %s =",pclTplFldName,pclTplFldType,pclSearch);
        strcpy(pclReplace,pclTplFldValue);
        ilGetRc = ReplaceStringInString(pclText, pclSearch, pclReplace);

        sprintf(pclSearch,"[%s|DD]",pclTplFldName);
        dbg(DEBUG,"DATA TYPE OF TPL FIELD <%s> IS <%s> = PATCH %s =",pclTplFldName,pclTplFldType,pclSearch);
        strncpy(pclReplace,&pclTplFldValue[6],2);
        pclReplace[2] = 0x00;
        ilGetRc = ReplaceStringInString(pclText, pclSearch, pclReplace);

        sprintf(pclSearch,"[%s|HHMM]",pclTplFldName);
        dbg(DEBUG,"DATA TYPE OF TPL FIELD <%s> IS <%s> = PATCH %s =",pclTplFldName,pclTplFldType,pclSearch);
        strncpy(pclReplace,&pclTplFldValue[8],4);
        pclReplace[4] = 0x00;
        ilGetRc = ReplaceStringInString(pclText, pclSearch, pclReplace);

        sprintf(pclSearch,"[%s|DDHHMM]",pclTplFldName);
        dbg(DEBUG,"DATA TYPE OF TPL FIELD <%s> IS <%s> = PATCH %s =",pclTplFldName,pclTplFldType,pclSearch);
        strncpy(pclReplace,&pclTplFldValue[6],6);
        pclReplace[6] = 0x00;
        ilGetRc = ReplaceStringInString(pclText, pclSearch, pclReplace);
      }
      else if (strcmp(pclTplFldType,"FLNO")==0)
      {
        sprintf(pclSearch,"[%s]",pclTplFldName);
        dbg(DEBUG,"DATA TYPE OF TPL FIELD <%s> IS <%s> = PATCH %s =",pclTplFldName,pclTplFldType,pclSearch);
        dbg(DEBUG,"TYPE OF TPL <%s> <%s> RPL <%s>",pclTplFldName,pclTplFldType,pclSearch);
        strcpy(pclReplace,pclTplFldValue);
        ilGetRc = ReplaceStringInString(pclReplace, " ", "");
        ilGetRc = ReplaceStringInString(pclText, pclSearch, pclReplace);
      }
      else
      {
        dbg(DEBUG,"DEFAULT DATA TYPE OF TPL FIELD <%s> <%s>",pclTplFldName,pclTplFldType);
        sprintf(pclSearch,"[%s]",pclTplFldName);
        dbg(TRACE,"DATA TYPE OF TPL FIELD <%s> IS <%s> = PATCH %s =",pclTplFldName,pclTplFldType,pclSearch);
        strcpy(pclReplace,pclTplFldValue);
        ilGetRc = ReplaceStringInString(pclText, pclSearch, pclReplace);
      }
    }
  }

  dbg(TRACE,"PATCHED TELEX TEXT \n%s",pclText);
  (void) CedaGetKeyItem(pclTplRplValue, &llTmpLen, pclText, "[", "]", TRUE);
  if (llTmpLen > 0)
  {
    dbg(TRACE,"FOUND UNRESOLVED [%s] IN TELEX TEXT",pclTplRplValue);
    ilRc = RC_FAIL;
  }
  else
  {
    dbg(TRACE,"THE RESULTING TELEX TEXT IS CLEAN");
    ilRc = RC_SUCCESS;
  }

  strcpy(pcgTlxTmplPatched,pclText);
  return ilRc;
}

/*******************************************************/
/*******************************************************/
static int GetMailAddresses(void)
{
  int ilRc = RC_SUCCESS;
  int ilGetRc = RC_SUCCESS;
  int ilLen = 0;
  char pclMtpAlls[8] = "";
  char pclAlc2[8] = "";
  char pclAlc3[8] = "";
  char pclOrg3[8] = "";
  char pclDes3[8] = "";
  char pclVia3[8] = "";
  char pclAftVial[1024] = "";
  char pclMadVial[64] = "";
  char pclSqlFld[256] = "";
  char pclSqlKey[256] = "";
  dbg(TRACE,"===========================================");
  dbg(TRACE,">>> HANDLE AUTO MVT: GET MAIL ADDRESSES ===");
  dbg(TRACE,"-------------------------------------------");
  strcpy(pclSqlFld,"DISTINCT(ADDR)");
  ilLen = GetFieldValue(pclAlc2, pcgAftSqlFldNames, "ALC2", pcgAftSqlFldValues);
  if (ilLen <= 0) strcpy(pclAlc2,"--");
  ilLen = GetFieldValue(pclAlc3, pcgAftSqlFldNames, "ALC3", pcgAftSqlFldValues);
  if (ilLen <= 0) strcpy(pclAlc3,"---");
  ilLen = GetFieldValue(pclOrg3, pcgAftSqlFldNames, "ORG3", pcgAftSqlFldValues);
  if (ilLen <= 0) strcpy(pclOrg3,"---");
  ilLen = GetFieldValue(pclVia3, pcgAftSqlFldNames, "VIA3", pcgAftSqlFldValues);
  if (ilLen <= 0) strcpy(pclVia3,"---");
  ilLen = GetFieldValue(pclDes3, pcgAftSqlFldNames, "DES3", pcgAftSqlFldValues);
  if (ilLen <= 0) strcpy(pclDes3,"---");
  ilLen = GetShortStationList(pclMadVial, "'", "'", ",");
  if (ilLen < 3) strcpy(pclMadVial,"'---'");
  ilGetRc = GetFieldValue(pclMtpAlls, pcgMtpRecFldNames, "ALLS", pcgMtpRecFldValues);
  if (strcmp(pclMtpAlls,"N") == 0)
  {
    strcpy(pclDes3,"===");
    strcpy(pclMadVial,"'==='");
  }
  sprintf(pclSqlKey,"WHERE (ALC2='%s' OR ALC3='%s') "
                    "AND (APC3='%s' OR APC3='%s' OR APC3='%s' OR APC3 IN (%s)) "
                    "AND TYPE='E' AND STAT='A' AND ADDR<>' '",
                    pclAlc2,pclAlc3,pclOrg3,pclVia3,pclDes3,pclMadVial);
  ilGetRc = HandleSqlSelectRows("MADTAB", pclSqlKey, pclSqlFld, pcgMadMailAddr, -1);
  if (ilGetRc <= 0) ilRc = RC_FAIL;
  /* MAIL Addresses are separated by LF (\n) */
  igMadMailCount = ilGetRc;
  dbg(TRACE,"MAIL: %d ADDRESSES\n%s",igMadMailCount,pcgMadMailAddr);
  strcpy(pcgAllMailAddr,pcgMadMailAddr);
  igAllMailCount = igMadMailCount;
  return ilRc;
}

/*******************************************************/
/*******************************************************/
static int GetSitaAddresses(void)
{
  int ilRc = RC_SUCCESS;
  int ilGetRc = RC_SUCCESS;
  int ilLen = 0;
  char pclMtpAlls[8] = "";
  char pclAlc2[8] = "";
  char pclAlc3[8] = "";
  char pclOrg3[8] = "";
  char pclDes3[8] = "";
  char pclVia3[8] = "";
  char pclMadVial[128] = "";
  char pclSqlFld[256] = "";
  char pclSqlKey[256] = "";
  dbg(TRACE,"===========================================");
  dbg(TRACE,">>> HANDLE AUTO MVT: GET SITA ADDRESSES ===");
  dbg(TRACE,"-------------------------------------------");
  strcpy(pclSqlFld,"DISTINCT(ADDR)");
  ilLen = GetFieldValue(pclAlc2, pcgAftSqlFldNames, "ALC2", pcgAftSqlFldValues);
  if (ilLen <= 0) strcpy(pclAlc2,"--");
  ilLen = GetFieldValue(pclAlc3, pcgAftSqlFldNames, "ALC3", pcgAftSqlFldValues);
  if (ilLen <= 0) strcpy(pclAlc3,"---");
  ilLen = GetFieldValue(pclOrg3, pcgAftSqlFldNames, "ORG3", pcgAftSqlFldValues);
  if (ilLen <= 0) strcpy(pclOrg3,"---");
  ilLen = GetFieldValue(pclVia3, pcgAftSqlFldNames, "VIA3", pcgAftSqlFldValues);
  if (ilLen <= 0) strcpy(pclVia3,"---");
  ilLen = GetFieldValue(pclDes3, pcgAftSqlFldNames, "DES3", pcgAftSqlFldValues);
  if (ilLen <= 0) strcpy(pclDes3,"---");
  ilLen = GetShortStationList(pclMadVial, "'", "'", ",");
  if (ilLen < 3) strcpy(pclMadVial,"'---'");
  ilGetRc = GetFieldValue(pclMtpAlls, pcgMtpRecFldNames, "ALLS", pcgMtpRecFldValues);
  if (strcmp(pclMtpAlls,"N") == 0)
  {
    strcpy(pclDes3,"===");
    strcpy(pclMadVial,"'==='");
  }
  sprintf(pclSqlKey,"WHERE (ALC2='%s' OR ALC3='%s') "
                    "AND (APC3='%s' OR APC3='%s' OR APC3='%s' OR APC3 IN (%s)) "
                    "AND TYPE='T' AND STAT='A' AND ADDR<>' '",
                    pclAlc2,pclAlc3,pclOrg3,pclVia3,pclDes3,pclMadVial);
  ilGetRc = HandleSqlSelectRows("MADTAB", pclSqlKey, pclSqlFld, pcgMadSitaAddr, -1);
  if (ilGetRc <= 0) ilRc = RC_FAIL;
  ReplaceStringInString(pcgMadSitaAddr, "\n", ",");
  igMadSitaCount = field_count(pcgMadSitaAddr);
  dbg(TRACE,"SITA: %d ADDRESSES\n%s",igMadSitaCount,pcgMadSitaAddr);
  strcpy(pcgAllSitaAddr,pcgMadSitaAddr);
  igAllSitaCount = igMadSitaCount;
  return ilRc;
}

/*******************************************************/
/*******************************************************/
static int GetTemplateConfig(char *pcpIgnore, char *pcpWanted, int ipCaller)
{
  int ilRc = RC_SUCCESS;
  int ilGetRc = RC_SUCCESS;
  int ilLen = 0;
  int ilGetLen = 0;
  int ilTplLen = 0;
  int ilIgnLen = 0;
  int ilRecCnt = 0;
  int ilLen1 = 0;
  int ilLen2 = 0;
  int ilFldDiff = 0;
  int ilMinDiff = 0;
  int ilGotTpl = FALSE;
  char pclAdid[8] = "";
  char pclAlc2[8] = "";
  char pclAlc3[8] = "";
  char pclCode[8] = "";
  char pclType[8] = "";
  char pclFsnd[8] = "";
  char pclFldd[16] = "";
  char pclFld1[8] = "";
  char pclFld2[8] = "";
  char pclFld1Val[32] = "";
  char pclFld2Val[32] = "";
  char pclTmed[8] = "";
  char pclAftNewVal[32] = "";
  char pclAftOldVal[32] = "";
  char pclSqlFld[256] = "";
  char pclSqlKey[256] = "";
  dbg(TRACE,"============================================");
  dbg(TRACE,">>> HANDLE AUTO MVT: GET TEMPLATE CONFIG ===");
  dbg(TRACE,"--------------------------------------------");
  
  strcpy(pcgTlxTmplText,"");
  strcpy(pcgMtpRecFldValues,"");
  strcpy(pcgMtpTplFldValues,"");
  ilLen = GetFieldValue(pclAdid, pcgActTrgFldNames, "ADID", pcgActTrgNewValues);
  ilLen = GetFieldValue(pclAlc2, pcgActTrgFldNames, "ALC2", pcgActTrgNewValues);
  ilLen = GetFieldValue(pclAlc3, pcgActTrgFldNames, "ALC3", pcgActTrgNewValues);

  if (pclAdid[0] == 'A')
  {
    igFlightIsArrival = TRUE;
    igFlightIsDeparture = FALSE;
  }
  else if (pclAdid[0] == 'D')
  {
    igFlightIsArrival = FALSE;
    igFlightIsDeparture = TRUE;
  }
  else
  {
    igFlightIsArrival = FALSE;
    igFlightIsDeparture = FALSE;
  }

  ilRecCnt = 1;  
  ilGotTpl = FALSE;
  while((ilGotTpl == FALSE) && (ilRecCnt > 0))
  {
    strcpy(pcgTlxTmplText,"");
    strcpy(pcgMtpRecFldValues,"");
    strcpy(pcgMtpTplFldValues,"");
    dbg(TRACE,"MVT TYPES WANTED <%s> NOT WANTED <%s>",pcpWanted,pcpIgnore);
    dbg(TRACE,"--------------------------------------------");
    ilGetLen = strlen(pcpWanted);
    ilIgnLen = strlen(pcpIgnore);
    if ((ilGetLen > 0) && (ilIgnLen > 0))
    {
      sprintf(pclSqlKey,"WHERE ALC2='%s' AND ADID='%s' AND TYPE IN (%s) AND TYPE NOT IN (%s) AND STAT='A'",
                               pclAlc2,pclAdid,pcpWanted,pcpIgnore);
    }
    else if (ilGetLen > 0) 
    {
      sprintf(pclSqlKey,"WHERE ALC2='%s' AND ADID='%s' AND TYPE IN (%s) AND STAT='A'",pclAlc2,pclAdid,pcpWanted);
    }
    else if (ilIgnLen > 0) 
    {
      sprintf(pclSqlKey,"WHERE ALC2='%s' AND ADID='%s' AND TYPE NOT IN (%s) AND STAT='A'",pclAlc2,pclAdid,pcpIgnore);
    }
    else 
    {
      sprintf(pclSqlKey,"WHERE ALC2='%s' AND ADID='%s' AND STAT='A'",pclAlc2,pclAdid);
    }
  
    ilRecCnt = HandleSqlSelectRows("MTPTAB", pclSqlKey, pcgMtpRecFldNames, pcgMtpRecFldValues, -1);
    dbg(TRACE,"MTPTAB TEMPLATE FOUND %d RECORDS",ilRecCnt);
    if (ilRecCnt > 0)
    {
      dbg(TRACE,"MTPTAB TEMPLATE REC FIELDS\n%s",pcgMtpRecFldNames);
      dbg(TRACE,"MTPTAB TEMPLATE REC VALUES\n%s",pcgMtpRecFldValues);
      strcpy(pcgMtpTplFldValues,pcgMtpRecFldValues);
      ilGetRc = GetFieldValue(pclType, pcgMtpRecFldNames, "TYPE", pcgMtpRecFldValues);
      sprintf(pclCode,"'%s'",pclType);
      dbg(TRACE,"TEMPLATE TYPE <%s>",pclCode);
      strcpy(pcgCurTplType,pclCode);
      if (ilIgnLen > 0) strcat(pcpIgnore,",");
      strcat(pcpIgnore,pclCode);
      ilGetRc = GetFieldValue(pcgTlxTmplText, pcgMtpRecFldNames, "TMPL", pcgMtpRecFldValues);
      if (ilGetRc <= 0)
      {
        dbg(TRACE,"MTPTAB <%s> TEMPLATE NOT CUSTOMIZED FOR <%s>",pclType,pclAlc2);
        dbg(TRACE,"MTPTAB <%s> TEMPLATE USING IATA DEF FOR <%s>",pclType,pclAlc2);
        sprintf(pclCode,"#%s",pclType);
        /* sprintf(pclSqlKey,"WHERE ALC3='%s' AND ADID='%s' AND STAT='A'",pclCode,pclAdid); */
        sprintf(pclSqlKey,"WHERE ALC3='%s' AND ADID='%s'",pclCode,pclAdid);
        ilGetRc = HandleSqlSelectRows("MTPTAB", pclSqlKey, pcgMtpRecFldNames, pcgMtpTplFldValues, -1);
        dbg(TRACE,"MTPTAB TEMPLATE TPL FIELDS\n%s",pcgMtpRecFldNames);
        dbg(TRACE,"MTPTAB TEMPLATE TPL VALUES\n%s",pcgMtpTplFldValues);
        ilGetRc = GetFieldValue(pcgTlxTmplText, pcgMtpRecFldNames, "TMPL", pcgMtpTplFldValues);
      }
    }
    ilTplLen = strlen(pcgTlxTmplText);
    if (ilTplLen > 1)
    {
      ilGotTpl = TRUE;
      dbg(TRACE,"CHECK TEMPLATE TRIGGER FIELD CONDITION");
      ilGetRc = GetFieldValue(pclFsnd, pcgMtpRecFldNames, "FSND", pcgMtpRecFldValues);
      if (ilGetRc <= 0)
      {
        ilGetRc = GetFieldValue(pclFsnd, pcgMtpRecFldNames, "FSND", pcgMtpTplFldValues);
      }
      if (ilGetRc > 0)
      {
        ilGetRc = GetFieldValue(pclAftNewVal, pcgActTrgFldNames, pclFsnd, pcgActTrgNewValues);
        if (ilGetRc > 0)
        {
          ilGetRc = GetFieldValue(pclAftOldVal, pcgActTrgFldNames, pclFsnd, pcgActTrgOldValues);
          dbg(TRACE,"AFTTAB FIELD <%s> NEW <%s> OLD <%s>",pclFsnd,pclAftNewVal,pclAftOldVal);
          if (strcmp(pclAftOldVal,pclAftNewVal) == 0)
          {
            dbg(TRACE,"NO CHANGE OF TRIGGER FIELD VALUE");
            ilGotTpl = FALSE;
          }
          else
          {
            dbg(TRACE,"GOT MTP TEMPLATE TEXT\n%s",pcgTlxTmplText);
          }
        }
        else
        {
          dbg(TRACE,"TRIGGER FIELD <%s> (NEW) VALUE IS EMPTY",pclFsnd);
          ilGotTpl = FALSE;
        }
        if (ilGotTpl == FALSE)
        {
          if (ipCaller != 1)
          {
            dbg(TRACE,"TEMPLATE ACCEPTED DUE TO CALLER %d",ipCaller);
            ilGotTpl = TRUE;
          }
        }
      }
      else
      {
        ilGotTpl = TRUE;
        dbg(TRACE,"TEMPLATE TRIGGER FIELD (FSND) NOT CONFIGURED");
        dbg(TRACE,"NOW SWITCHING TO USE OF FIELD FLDD");
        dbg(TRACE,"CHECK TEMPLATE FLDD TRIGGER FIELD CONDITION");
        ilGetRc = GetFieldValue(pclFldd, pcgMtpRecFldNames, "FLDD", pcgMtpRecFldValues);
        if (ilGetRc <= 0)
        {
          ilGetRc = GetFieldValue(pclFldd, pcgMtpRecFldNames, "FLDD", pcgMtpTplFldValues);
        }
        if (ilGetRc > 0)
        {
          ilGotTpl = FALSE;
          strncpy(pclFld1,pclFldd,4);
          strncpy(pclFld2,&pclFldd[4],4);
          pclFld1[4] = 0x00;
          pclFld2[4] = 0x00;
          dbg(TRACE,"FOUND FLDD TRIGGER FIELDS <%s> <%s>",pclFld1,pclFld2);
          ilLen1 = GetFieldValue(pclFld1Val, pcgActTrgFldNames, pclFld1, pcgActTrgNewValues);
          ilLen2 = GetFieldValue(pclFld2Val, pcgActTrgFldNames, pclFld2, pcgActTrgNewValues);
          dbg(TRACE,"FOUND FLDD TRIGGER VALUES <%s> <%s>",pclFld1Val,pclFld2Val);
          if ((ilLen1 > 0) && (ilLen2 > 0))
          {
            GetFieldValue(pclTmed, pcgMtpRecFldNames, "TMED", pcgMtpRecFldValues);
            ilMinDiff = atoi(pclTmed);
            ilGetRc = DateDiffToMin(pclFld2Val,pclFld1Val,&ilFldDiff);
            dbg(TRACE,"FOUND FLDD TRIGGER MINIMUM TIME DIFF = %d",ilMinDiff);
            dbg(TRACE,"FOUND FLDD TRIGGER FIELDS  TIME DIFF = %d",ilFldDiff);
            if (ilFldDiff >= ilMinDiff)
            {
              dbg(TRACE,"THE TIME CONDITION MATCHES");
              ilGotTpl = TRUE;
            }
            else
            {
              dbg(TRACE,"THE TIME CONDITION DOES NOT MATCH");
              ilGotTpl = FALSE;
            }
/* BERNI */
          }
          else
          {
            dbg(TRACE,"AT LEAST ONE OF FLDD FIELDS IS EMPTY");
            dbg(TRACE,"THE TIME CONDITION DOES NOT MATCH");
            ilGotTpl = FALSE;
          }
        }
        else
        {
          dbg(TRACE,"TEMPLATE TRIGGER FIELDS (FLDD) NOT CONFIGURED");
          ilGotTpl = FALSE;
        }
      }
    }
    else
    {
      dbg(TRACE,"MTPTAB TEMPLATE NOT FOUND FOR <%s>",pclAlc2);
      ilGotTpl = FALSE;
      ilRecCnt = 0;
      ilRc = RC_FAIL;
    }
  }
  return ilRc;
}

/*******************************************************/
/*******************************************************/
static void CheckTelexTimeConditions(int ipCaller)
{
  int ilRc = RC_SUCCESS;
  int ilGetRc = RC_SUCCESS;
  dbg(TRACE,"==============================================");
  dbg(TRACE,">>> HANDLE AUTO %s: CHECK TIME CONDITIONS ===",pcgCurTplType);
  dbg(TRACE,"----------------------------------------------");
  CheckTelexWaitStatus(ipCaller);
  CheckFlightDelayStatus();
  return;
}

/*******************************************************/
/*******************************************************/
static void CheckTelexWaitStatus(int ipCaller)
{
  int ilRc = RC_SUCCESS;
  int ilGetRc = RC_SUCCESS;
  char pclTsnd[16] = "";
  char pclFsnd[16] = "";
  char pclAftFldVal[32] = "";
  char pclTimeStamp[32] = "";
  int ilWaitTime = 0;
  int ilTimeDiff = 0;
  int ilSendTime = 0;
  int ilWkd = 0;
  int ilDay = 0;
  dbg(TRACE,">>> HANDLE AUTO %s: CHECK WAIT CONDITIONS ===",pcgCurTplType);
  dbg(TRACE,"----------------------------------------------");
  igSaveAsPending = FALSE;
  ilGetRc = GetFieldValue(pclTsnd, pcgMtpRecFldNames, "TSND", pcgMtpRecFldValues);
  if (ilGetRc > 0)
  {
    ilWaitTime = atoi(pclTsnd);
    if (ilWaitTime > 0)
    {
      ilGetRc = GetFieldValue(pclFsnd, pcgMtpRecFldNames, "FSND", pcgMtpRecFldValues);
      if (ilGetRc <= 0)
      {
        ilGetRc = GetFieldValue(pclFsnd, pcgMtpRecFldNames, "FSND", pcgMtpTplFldValues);
      }
      if (ilGetRc > 0)
      {
        dbg(TRACE,"TEMPLATE FSND <%s> TSND (%d)",pclFsnd,ilWaitTime);
        ilGetRc = GetFieldValue(pclAftFldVal, pcgAftSqlFldNames, pclFsnd, pcgAftSqlFldValues);
        if (ilGetRc > 0)
        {
          GetServerTimeStamp("UTC", 1, 0, pclTimeStamp);
          dbg(TRACE,"CEDA DATE/TIME STRG = <%s>",pclTimeStamp);
          dbg(TRACE,"%s DATE/TIME STRG = <%s>",pclFsnd,pclAftFldVal);
          ilGetRc = DateDiffToMin(pclTimeStamp,pclAftFldVal,&ilTimeDiff);
          dbg(TRACE,"CEDA DATE/TIME DIFF = %d",ilTimeDiff);
          if (ilTimeDiff >= ilWaitTime)
          {
            dbg(TRACE,"NO NEED TO WAIT: SEND TELEX NOW");
            igSaveAsPending = FALSE;
          }
          else
          {
            ilWaitTime = ilWaitTime - ilTimeDiff;
            (void) GetFullDay (pclTimeStamp, &ilDay, &ilSendTime, &ilWkd);
            ilSendTime += ilWaitTime;
            (void) FullTimeString (pcgSendTlxTime, ilSendTime, "00");
            dbg(TRACE,"MUST STILL WAIT %d MINUTES: SEND TELEX AT <%s> UTC",ilWaitTime,pcgSendTlxTime);
            igSaveAsPending = TRUE;
            if (ipCaller != 1)
            {
              dbg(TRACE,"NO NEED TO WAIT: CALLER=%d",ipCaller);
              igSaveAsPending = FALSE;
            }
          }
        }
      }
    }
    else
    {
      dbg(TRACE,"NO VALUE FOR FIELD <TSND> CONFIGURED (IS ZERO)");
    }
  }
  else
  {
    dbg(TRACE,"NO VALUE FOR FIELD <TSND> CONFIGURED");
  }
  return;
}

/*******************************************************/
/*******************************************************/
static void CheckFlightDelayStatus(void)
{
  int ilRc = RC_SUCCESS;
  int ilGetRc = RC_SUCCESS;
  char pclTplFldName[16] = "";
  char pclStod[16] = "";
  char pclOfbl[16] = "";
  char pclDela[16] = "";
  int ilLenOfbl = 0;
  int ilLenStod = 0;
  int ilTimeDiff = 0;
  int ilLimit = 0;
  int ilWkd = 0;
  int ilDay = 0;
  dbg(TRACE,"--------------------------------------------------");
  dbg(TRACE,">>> HANDLE AUTO %s: CHECK FLIGHT DELAY STATUS ===",pcgCurTplType);
  dbg(TRACE,"--------------------------------------------------");
  igFlightIsDelayed = FALSE;
  igDelayIsAccepted = TRUE;
  igTotalDelayTime = 0;
  if (igFlightIsDeparture == TRUE)
  {
    ilLenStod = GetFieldValue(pclStod, pcgAftSqlFldNames, "STOD", pcgAftSqlFldValues);
    ilLenOfbl = GetFieldValue(pclOfbl, pcgAftSqlFldNames, "OFBL", pcgAftSqlFldValues);
    if ((ilLenStod > 0) && (ilLenOfbl > 0))
    {
      dbg(TRACE,"%s DATE/TIME STRG = <%s>","OFBL",pclOfbl);
      dbg(TRACE,"%s DATE/TIME STRG = <%s>","STOD",pclStod);
      ilGetRc = DateDiffToMin(pclOfbl,pclStod,&ilTimeDiff);
      dbg(TRACE,"CEDA DATE/TIME DIFF = %d",ilTimeDiff);
      igTotalDelayTime = ilTimeDiff;
      if (ilTimeDiff > 0)
      {
        dbg(TRACE,"FLIGHT IS DELAYED");
        igFlightIsDelayed = TRUE;
        ilGetRc = GetFieldValue(pclDela, pcgMtpRecFldNames, "DELA", pcgMtpRecFldValues);
        if (ilGetRc > 0)
        {
          ilLimit = atoi(pclDela);
          dbg(TRACE,"FLIGHT DELAY TOLERANCE LIMIT = <%s> (%d)",pclDela,ilLimit);
          if (ilLimit >= ilTimeDiff)
          {
            dbg(TRACE,"DELAY IS TOLERATED");
            igDelayIsAccepted = TRUE;
          }
          else
          {
            dbg(TRACE,"DELAY IS OUT OF LIMIT");
            igDelayIsAccepted = FALSE;
          }
        }
      }
      else if (ilTimeDiff == 0)
      {
        dbg(TRACE,"FLIGHT IS ON TIME");
        igFlightIsDelayed = FALSE;
        igDelayIsAccepted = TRUE;
      }
      else if (ilTimeDiff < 0)
      {
        dbg(TRACE,"FLIGHT IS TOO EARLY");
        igFlightIsDelayed = FALSE;
        igDelayIsAccepted = TRUE;
      }
    }
    else
    {
      if (ilLenStod <= 0)
      {
        dbg(TRACE,"MISSING SCHEDULED DEPARTURE TIME");
      }
      if (ilLenOfbl <= 0)
      {
        dbg(TRACE,"FLIGHT IS NOT YET OFF BLOCK");
      }
      igFlightIsDelayed = FALSE;
      igDelayIsAccepted = TRUE;
    }
    if (igDelayIsAccepted == FALSE)
    {
      igDelayCodesRequired = CheckFieldRequired("DCD1",pclTplFldName);
      if (igDelayCodesRequired == TRUE)
      {
        dbg(TRACE,"DELAY CODES REQUIRED IN TEMPLATE TEXT");
      }
      else
      {
        dbg(TRACE,"NO DELAY CODES REQUIRED IN TEMPLATE TEXT");
      }
      igDelayTimesRequired = CheckFieldRequired("DTD1",pclTplFldName);
      if (igDelayTimesRequired == TRUE)
      {
        dbg(TRACE,"DELAY TIMES REQUIRED IN TEMPLATE TEXT");
      }
      else
      {
        dbg(TRACE,"NO DELAY TIMES REQUIRED IN TEMPLATE TEXT");
      }
    }
  }
  else
  {
    dbg(TRACE,"FLIGHT IS NOT A DEPARTURE");
  }
  return;
}

/*******************************************************/
/*******************************************************/
static int CheckFieldRequired(char *pcpAftName, char *pcpTplName)
{
  int ilRc = TRUE;
  int ilGetRc = RC_SUCCESS;
  char pclAftFldName[16] = "";
  char pclTplFldName[16] = "";
  char pclCheckName[16] = "";
  strcpy(pclAftFldName,pcpAftName);
  strcpy(pcpTplName,"----");
  ilGetRc = GetFieldValue(pclTplFldName, pcgAftRecFldNames, pclAftFldName, pcgTplRecFldNames);
  if (ilGetRc > 1)
  {
    dbg(TRACE,"AFT FIELD <%s> FOUND AS <%s> IN TPL REC RELATION",pclAftFldName,pclTplFldName);
    strcpy(pcpTplName,pclTplFldName);
    sprintf(pclCheckName,"[%s",pclTplFldName);
    if (strstr(pcgTlxTmplText,pclCheckName) != NULL)
    {
      dbg(TRACE,"TPL FIELD <%s> IS REQUIRED IN TEMPLATE TEXT",pclTplFldName);
      ilRc = TRUE;
    }
    else
    {
      dbg(TRACE,"TPL FIELD <%s> IS NOT REQUIRED IN TEMPLATE TEXT",pclTplFldName);
      ilRc = FALSE;
    }
  }
  else
  {
    dbg(TRACE,"AFT FIELD <%s> NOT FOUND IN TPL REC RELATION",pclAftFldName);
    ilRc = FALSE;
  }
  return ilRc;
}


/*******************************************************/
/*******************************************************/
static int EvaluateDelayInfo(char *pcpAftDCD1,char *pcpAftDCD2,char *pcpAftDTD1,char *pcpAftDTD2,char *pcpAftDELD)
{
  int ilRc = RC_SUCCESS;
  int ilGetRc = RC_SUCCESS;
  char pclTplFldName[16] = "";
  int ilAftTotal = 0;
  int ilTplTotal = 0;
  int ilTplDiff = 0;
  int ilValDTD1 = 0;
  int ilValDTD2 = 0;
  int ilLenDCD1 = 0;
  int ilLenDCD2 = 0;
  int ilLenDTD1 = 0;
  int ilLenDTD2 = 0;
  int ilSetDCD1 = TRUE;
  int ilSetDCD2 = TRUE;
  int ilSetDTD1 = TRUE;
  int ilSetDTD2 = TRUE;
  int ilSetCheck = FALSE;
  dbg(TRACE,"--------------------------------------------");
  dbg(TRACE,">>> HANDLE AUTO %s: EVALUATE DELAY INFO ===",pcgCurTplType);
  dbg(TRACE,"--------------------------------------------");
  str_trm_all(pcpAftDCD1, " ", TRUE);
  str_trm_all(pcpAftDCD2, " ", TRUE);
  str_trm_all(pcpAftDTD1, " ", TRUE);
  str_trm_all(pcpAftDTD2, " ", TRUE);
  dbg(TRACE,"FETCHED AFTTAB [DCD1]=[%s] [DCD2]=[%s] [DTD1]=[%s] [DTD2]=[%s]",pcpAftDCD1,pcpAftDCD2,pcpAftDTD1,pcpAftDTD2);
  ilLenDCD1 = strlen(pcpAftDCD1);
  ilLenDCD2 = strlen(pcpAftDCD2);
  ilLenDTD1 = strlen(pcpAftDTD1);
  ilLenDTD2 = strlen(pcpAftDTD2);
  ilAftTotal = igTotalDelayTime;
  /*
  ilValDTD1 = atoi(pcpAftDTD1);
  ilValDTD2 = atoi(pcpAftDTD2);
  */
  ilValDTD1 = GetMinutesFromTime(pcpAftDTD1);
  ilValDTD2 = GetMinutesFromTime(pcpAftDTD2);
  ilTplTotal = ilValDTD1 + ilValDTD2;
  ilTplDiff = ilAftTotal - ilTplTotal;
  if ((igFlightIsDelayed == TRUE) && (igDelayIsAccepted == FALSE))
  {
    dbg(TRACE,"FLIGHT IS DELAYED AND OUT OF LIMIT");
    ilSetDCD1 = CheckFieldRequired("DCD1",pclTplFldName);
    ilSetDCD2 = CheckFieldRequired("DCD2",pclTplFldName);
    ilSetDTD1 = CheckFieldRequired("DTD1",pclTplFldName);
    ilSetDTD2 = CheckFieldRequired("DTD2",pclTplFldName);
    if ((igDelayTimesRequired == TRUE) && (igDelayCodesRequired == TRUE))
    {
      if ((ilSetDCD1 == TRUE) && (ilSetDCD2 == TRUE) && (ilSetDTD1 == TRUE) && (ilSetDTD2 == TRUE))
      {
        dbg(TRACE,"REQUIRED LIST: [DCD1]/[DCD2]/[DTD1]/[DTD2]");
        dbg(TRACE,"EXISTING DATA: [-%s-]/[-%s-]/[%s]/[%s]",pcpAftDCD1,pcpAftDCD2,pcpAftDTD1,pcpAftDTD2);
		if ((ilLenDCD1 == 2) && (ilLenDCD2 == 2) && (ilValDTD1 > 0) && (ilValDTD2 > 0))
		{
		  dbg(TRACE,"ALL REQUIRED VALUES ARE AVAILABLE");
		  if (ilTplDiff != 0)
		  {
		    dbg(TRACE,"DIFFERENT TOTALS OF DELAY TIME: AFT=%d TPL=%d",ilAftTotal,ilTplTotal);
		  }
		}
		else
		{
		  dbg(TRACE,"SOME VALUES ARE NOT AVAILABLE");
		  if (ilTplTotal == 0)
		  {
		    dbg(TRACE,"BOTH TIME FIELDS ARE EMPTY");
		    if ((ilLenDCD1 == 2) && (ilLenDCD2 == 0))
			{
              ilValDTD2 = ilAftTotal;
			  sprintf(pcpAftDTD2,"%04d",ilValDTD2);
			  ilLenDTD2 = 4;
			  ilTplDiff = 0;
			} 
		  }
		  else
		  {
		    dbg(TRACE,"EITHER AT LEAST ONE TIME FIELD IS EMPTY");
		    dbg(TRACE,"OR     AT LEAST ONE DELAY CODE IS EMPTY");
		    if ((ilLenDCD2 == 2) && (ilValDTD2 == 0) && (ilTplDiff > 0))
			{
              ilValDTD2 = ilTplDiff;
			  sprintf(pcpAftDTD2,"%04d",ilValDTD2);
			  ilLenDTD2 = 4;
			  ilTplDiff = 0;
			} 
		    if ((ilLenDCD1 == 2) && (ilValDTD1 == 0) && (ilTplDiff > 0))
			{
              ilValDTD1 = ilTplDiff;
			  sprintf(pcpAftDTD1,"%04d",ilValDTD1);
			  ilLenDTD1 = 4;
			  ilTplDiff = 0;
			}
		  }
		}
        dbg(TRACE,"REQUIRED LIST: [DCD1]/[DCD2]/[DTD1]/[DTD2]");
        dbg(TRACE,"TEMPLATE DATA: [-%s-]/[-%s-]/[%s]/[%s]",pcpAftDCD1,pcpAftDCD2,pcpAftDTD1,pcpAftDTD2);
      }
      else if ((ilSetDCD1 == TRUE) && (ilSetDTD1 == TRUE))
      {
        dbg(TRACE,"REQUIRED LIST: [DCD1]/[DTD1]");
        dbg(TRACE,"EXISTING DATA: [-%s-]/[%s]",pcpAftDCD1,pcpAftDTD1);
        strcpy(pcpAftDCD2,"");
        strcpy(pcpAftDTD2,"");
        if ((ilLenDCD1 == 0) && (ilTplDiff > 0))
        {
          strcpy(pcpAftDCD1,"??");
          ilLenDCD1 = 2;
        }
        if ((ilLenDCD1 == 2) && (ilValDTD1 == 0) && (ilTplDiff > 0))
        {
          ilValDTD1 = ilTplDiff;
          sprintf(pcpAftDTD1,"%04d",ilValDTD1);
          ilLenDTD1 = 4;
          ilTplDiff = 0;
        }
        dbg(TRACE,"REQUIRED LIST: [DCD1]/[DTD1]");
        dbg(TRACE,"TEMPLATE DATA: [-%s-]/[%s]",pcpAftDCD1,pcpAftDTD1);
      }
      else
      {
        dbg(TRACE,"NON-STANDARD COMBINATION OF FIELDS IN TEMPLATE:");
        if (ilSetDCD1 == TRUE)
		{
          dbg(TRACE,"TEMPLATE FIELD: [DCD1] VALUE [-%s-]",pcpAftDCD1);
		}
        if (ilSetDCD2 == TRUE)
		{
          dbg(TRACE,"TEMPLATE FIELD: [DCD2] VALUE [-%s-]",pcpAftDCD2);
		}
        if (ilSetDTD1 == TRUE)
		{
          dbg(TRACE,"TEMPLATE FIELD: [DTD1] VALUE [%s]",pcpAftDTD1);
		}
        if (ilSetDTD2 == TRUE)
		{
          dbg(TRACE,"TEMPLATE FIELD: [DTD2] VALUE [%s]",pcpAftDTD2);
		}
      }
      if (ilTplDiff > 0)
      {
        dbg(TRACE,"UNEXPECTED COMBINATION OF FIELD VALUES");
      }
      ilSetCheck = FALSE;
      if ((ilLenDCD1 == 0) && (ilValDTD1 > 0))
      {
        strcpy(pcpAftDCD1,"??");
        ilLenDCD1 = 2;
        ilSetCheck = TRUE;
      }
      if ((ilLenDCD2 == 0) && (ilValDTD2 > 0))
      {
        strcpy(pcpAftDCD2,"??");
        ilLenDCD2 = 2;
        ilSetCheck = TRUE;
      }
      if (ilLenDCD1 == 0) 
      {
        strcpy(pcpAftDCD1,"??");
        ilLenDCD1 = 2;
        ilSetCheck = TRUE;
      }
    }
  }
  else
  {
	strcpy(pcpAftDCD1,"");
	strcpy(pcpAftDCD2,"");
  	strcpy(pcpAftDTD1,"");
  	strcpy(pcpAftDTD2,"");
  	strcpy(pcpAftDELD,"");
	ilAftTotal = 0;
	ilValDTD1 = 0;
	ilValDTD2 = 0;
  }
  FormatMinutesToTime(ilValDTD1, pcpAftDTD1, 0);
  FormatMinutesToTime(ilValDTD2, pcpAftDTD2, 0);
  dbg(TRACE,"TEMPLATE VALUE [DCD1]=[%s] [DCD2]=[%s] [DTD1]=[%s] [DTD2]=[%s]",pcpAftDCD1,pcpAftDCD2,pcpAftDTD1,pcpAftDTD2);
  ilTplTotal = ilValDTD1 + ilValDTD2;
  if (ilTplTotal != ilAftTotal) ilRc = RC_FAIL;
  if (strcmp(pcpAftDCD1,"??") == 0) ilRc = RC_FAIL;
  if (strcmp(pcpAftDCD2,"??") == 0) ilRc = RC_FAIL;
  return ilRc;
}

/*******************************************************/
/*******************************************************/
static int GetMinutesFromTime(char *pcpTime)
{
  char pclHH[4] = "";
  char pclMM[4] = "";
  int ilHH = 0;
  int ilMM = 0;
  int ilMinutes = 0;
  if (strlen(pcpTime) == 4)
  {
    strncpy(pclHH,&pcpTime[0],2);
    pclHH[2] = 0x00;
    strncpy(pclMM,&pcpTime[2],2);
    pclMM[2] = 0x00;
    ilMinutes = atoi(pclHH) * 60;
    ilMinutes += atoi(pclMM);
  }
  return ilMinutes;
}

/*******************************************************/
/*******************************************************/
static int FormatMinutesToTime(int ipMinutes, char *pcpTime, int ipCmpValue)
{
  int ilHH = 0;
  int ilMM = 0;
  pcpTime[0] = 0x00;
  if (ipMinutes > 0)
  {
    ilHH = ipMinutes / 60;
    if (ilHH > 99) ilHH = 99;
    ilMM = ipMinutes % 60;
  }
  if (ipMinutes > ipCmpValue) sprintf(pcpTime,"%02d%02d",ilHH,ilMM);
}

/*******************************************************/
/*******************************************************/
static int GetNextStationCode(char *pcpTplNstn)
{
  int ilRc = RC_SUCCESS;
  int ilGetRc = RC_SUCCESS;
  ilGetRc = GetFieldValue(pcpTplNstn, pcgAftSqlFldNames, "VIA3", pcgAftSqlFldValues);
  if (ilGetRc < 3)
  {
    ilGetRc = GetFieldValue(pcpTplNstn, pcgAftSqlFldNames, "DES3", pcgAftSqlFldValues);
  }
  return ilRc;
}

/*******************************************************/
/*******************************************************/
static int GetShortStationList(char *pcpTplVial,char *pcpPreFix,char *pcpSuffix,char *pcpValSep)
{
  int ilRc = RC_SUCCESS;
  int ilGetRc = RC_SUCCESS;
  char pclAftVial[2048] = "";
  char pclNxtApc3[8] = "";
  char pclDatItem[16] = "";
  int ilVialLen = 0;
  int ilNxtPos = 0;
  strcpy(pcpTplVial,"");
  ilVialLen = GetFieldValue(pclAftVial, pcgAftSqlFldNames, "VIAL", pcgAftSqlFldValues);
  if (ilVialLen > 3)
  {
    strcpy(pclNxtApc3,"---");
    ilNxtPos = 1;
    while (ilNxtPos < ilVialLen)
    {
      strncpy(pclNxtApc3,&pclAftVial[ilNxtPos],3);
      sprintf(pclDatItem,"%s%s%s",pcpPreFix,pclNxtApc3,pcpSuffix);
      strcat(pcpTplVial,pclDatItem);
      strcat(pcpTplVial,pcpValSep);
      ilNxtPos += 120;
    }
  }
  ilVialLen = strlen(pcpTplVial);
  if (ilVialLen > 0) 
  {
    ilVialLen -= strlen(pcpValSep);
  }
  pcpTplVial[ilVialLen] = 0x00;
  return ilVialLen;
}

/*******************************************************/
/*******************************************************/
static int EvaluatePaxInfo(char *pcpAftPAX1,char *pcpAftPAX2,char *pcpAftPAX3,char *pcpAftPXSI)
{
  int ilRc = RC_SUCCESS;
  int ilGetRc = RC_SUCCESS;
  char pclAftUrno[16] = "";
  char pclFldDatType[16] = "";
  char pclTplFldName[16] = "";
  char pclTplFldValue[16] = "";
  int ilPaxValue = 0;
  int ilLenPAX1 = 0;
  int ilLenPAX2 = 0;
  int ilLenPAX3 = 0;
  int ilSetPAX1 = TRUE;
  int ilSetPAX2 = TRUE;
  int ilSetPAX3 = TRUE;
  dbg(TRACE,"--------------------------------------------");
  dbg(TRACE,">>> HANDLE AUTO %s: EVALUATE PAX INFO ===",pcgCurTplType);
  dbg(TRACE,"--------------------------------------------");
  str_trm_all(pcpAftPAX1, " ", TRUE);
  str_trm_all(pcpAftPAX2, " ", TRUE);
  str_trm_all(pcpAftPAX3, " ", TRUE);
  str_trm_all(pcpAftPXSI, " ", TRUE);
  dbg(TRACE,"FETCHED FROM AFTTAB [PAX1]=[%s] [PAX2]=[%s] [PAX3]=[%s] [PXSI]=[%s]",pcpAftPAX1,pcpAftPAX2,pcpAftPAX3,pcpAftPXSI);
  ilGetRc = GetFieldValue(pclAftUrno, pcgAftSqlFldNames, "URNO", pcgAftSqlFldValues);

  ilSetPAX1 = CheckFieldRequired("PAX1",pclTplFldName);
  ilLenPAX1 = strlen(pcpAftPAX1);
  if (ilSetPAX1 == TRUE)
  {
    if (ilLenPAX1 == 0)
    {
      ilGetRc = GetFieldValue(pclFldDatType, pcgTplRecFldNames,pclTplFldName , pcgTplFldDatTypes);
      dbg(TRACE,"NEED REQUIRED PAX1 DATA FOR TYPE <%s>",pclFldDatType);
      ilPaxValue = FetchPaxValueFromDB(pclAftUrno, pclFldDatType, pclTplFldValue);
      if (ilPaxValue <= 0)
      {
        strcpy(pclTplFldValue,"??");
        ilRc = RC_FAIL;
      }
      strcpy(pcpAftPAX1,pclTplFldValue);
    }
  }

  ilSetPAX2 = CheckFieldRequired("PAX2",pclTplFldName);
  ilLenPAX2 = strlen(pcpAftPAX2);
  if (ilSetPAX2 == TRUE)
  {
    if (ilLenPAX2 == 0)
    {
      ilGetRc = GetFieldValue(pclFldDatType, pcgTplRecFldNames,pclTplFldName , pcgTplFldDatTypes);
      dbg(TRACE,"NEED REQUIRED PAX2 DATA FOR TYPE <%s>",pclFldDatType);
      ilPaxValue = FetchPaxValueFromDB(pclAftUrno, pclFldDatType, pclTplFldValue);
      if (ilPaxValue <= 0)
      {
        strcpy(pclTplFldValue,"??");
        ilRc = RC_FAIL;
      }
      strcpy(pcpAftPAX2,pclTplFldValue);
    }
  }

  ilSetPAX3 = CheckFieldRequired("PAX3",pclTplFldName);
  ilLenPAX3 = strlen(pcpAftPAX3);
  if (ilSetPAX3 == TRUE)
  {
    if (ilLenPAX3 == 0)
    {
      ilGetRc = GetFieldValue(pclFldDatType, pcgTplRecFldNames,pclTplFldName , pcgTplFldDatTypes);
      dbg(TRACE,"NEED REQUIRED PAX3 DATA FOR TYPE <%s>",pclFldDatType);
      ilPaxValue = FetchPaxValueFromDB(pclAftUrno, pclFldDatType, pclTplFldValue);
      if (ilPaxValue <= 0)
      {
        strcpy(pclTplFldValue,"??");
        ilRc = RC_FAIL;
      }
      strcpy(pcpAftPAX3,pclTplFldValue);
    }
  }

  dbg(TRACE,"EVALUATED TMPL [PAX1]=[%s] [PAX2]=[%s] [PAX3]=[%s] [PXSI]=[%s]",pcpAftPAX1,pcpAftPAX2,pcpAftPAX3,pcpAftPXSI);

  dbg(TRACE,"--------------------------------------------");
  return ilRc;
}

/*******************************************************/
/*******************************************************/
static int FetchPaxValueFromDB(char *pcpAftUrno, char *pcpDataType, char *pcpPaxValue)
{
  int ilRc = RC_SUCCESS;
  int ilGetRc = DB_SUCCESS;
  int ilGetRow = 0;
  int ilPaxValue = 0;
  char pclSqlKey[256] = "";
  char pclSqlTbl[16] = "";
  char pclSqlFld[64] = "";
  char pclSqlDat[64] = "";
  dbg(TRACE,"FETCHING PAX INFO FROM DB");

  strcpy(pclSqlTbl,"LOATAB");
  if (strcmp(pcpDataType,"PXIF") == 0)
  {
    /* TOTAL INCL. INFANTS */
    strcpy(pclSqlFld,"VALU");
    sprintf(pclSqlKey,"WHERE FLNU=%s AND FLNO=' ' AND TYPE='PAX' AND STYP='T' AND SSTP=' ' AND SSST=' ' AND APC3=' ' AND DSSN='USR'",pcpAftUrno);
    ilGetRow = HandleSqlSelectRows(pclSqlTbl, pclSqlKey, pclSqlFld, pclSqlDat, 1);
  }
  else if (strcmp(pcpDataType,"PXEF") == 0)
  {
    /* (TOTAL EXCL. INFANTS) */
    strcpy(pclSqlFld,"VALU");
    sprintf(pclSqlKey,"WHERE FLNU=%s AND FLNO=' ' AND TYPE='PAX' AND STYP='T' AND SSTP=' ' AND SSST=' ' AND APC3=' ' AND DSSN='USR'",pcpAftUrno);
    ilGetRow = HandleSqlSelectRows(pclSqlTbl, pclSqlKey, pclSqlFld, pclSqlDat, 1);
    ilPaxValue = atoi(pclSqlDat);
    strcpy(pclSqlFld,"VALU");
    sprintf(pclSqlKey,"WHERE FLNU=%s AND FLNO=' ' AND TYPE='PAX' AND STYP=' ' AND SSTP=' ' AND SSST='I' AND APC3=' ' AND DSSN='USR'",pcpAftUrno);
    ilGetRow = HandleSqlSelectRows(pclSqlTbl, pclSqlKey, pclSqlFld, pclSqlDat, 1);
    ilPaxValue -= atoi(pclSqlDat);
    sprintf(pclSqlDat,"%d",ilPaxValue);
  }
  else if (strcmp(pcpDataType,"PXIN") == 0)
  {
    /* TOTAL INFANTS */
    strcpy(pclSqlFld,"VALU");
    sprintf(pclSqlKey,"WHERE FLNU=%s AND FLNO=' ' AND TYPE='PAX' AND STYP=' ' AND SSTP=' ' AND SSST='I' AND APC3=' ' AND DSSN='USR'",pcpAftUrno);
    ilGetRow = HandleSqlSelectRows(pclSqlTbl, pclSqlKey, pclSqlFld, pclSqlDat, 1);
  }
  else
  {
    strcpy(pclSqlDat,"0");
    ilRc = RC_FAIL;
  }
  ilPaxValue = atoi(pclSqlDat);
  sprintf(pclSqlDat,"%d",ilPaxValue);
  strcpy(pcpPaxValue,pclSqlDat);
  return ilPaxValue;
}

/*******************************************************/
/*******************************************************/
static int GetNextStationETA(char *pcpTplFldVal)
{
  int ilRc = RC_SUCCESS;
  int ilGetRc = RC_SUCCESS;
  char pclAftVial[2048] = "";
  int ilVialLen = 0;
  int ilNxtPos = 0;
  strcpy(pcpTplFldVal,"");
  ilVialLen = GetFieldValue(pclAftVial, pcgAftSqlFldNames, "VIAL", pcgAftSqlFldValues);
  if (ilVialLen > 0)
  {
    ilNxtPos = 22;
    if (ilVialLen > ilNxtPos)
    {
      strncpy(pcpTplFldVal,&pclAftVial[ilNxtPos],14);
      pcpTplFldVal[14] = 0x00;
    }
  }
  else
  {
    ilGetRc = GetFieldValue(pcpTplFldVal, pcgAftSqlFldNames, "ETDI", pcgAftSqlFldValues);
  }
  return ilRc;
}

/*******************************************************/
/*******************************************************/
static int SendConfigToClient(int ipQueOut, char *pcpSelKey, char *pcpFields, char *pcpData)
{
  int ilRc = RC_SUCCESS;
  char pclOutBuff[4096] = "";
  char pclCfgName[32] = "";
  char pclBgnTag[32] = "";
  char pclEndTag[32] = "";
  int ilOutPos = 0;
  strcpy(pclCfgName,"AFT_REC_FLD_NAMES");
  sprintf(pclBgnTag,"<%s>",pclCfgName);
  sprintf(pclEndTag,"</%s>",pclCfgName);
  StrgPutStrg(pclOutBuff, &ilOutPos, pclBgnTag, 0, -1, pcgAftRecFldNames);
  StrgPutStrg(pclOutBuff, &ilOutPos, pclEndTag, 0, -1, "\n");
  strcpy(pclCfgName,"TPL_REC_FLD_NAMES");
  sprintf(pclBgnTag,"<%s>",pclCfgName);
  sprintf(pclEndTag,"</%s>",pclCfgName);
  StrgPutStrg(pclOutBuff, &ilOutPos, pclBgnTag, 0, -1, pcgTplRecFldNames);
  StrgPutStrg(pclOutBuff, &ilOutPos, pclEndTag, 0, -1, "\n");
  strcpy(pclCfgName,"TPL_REC_FLD_TYPES");
  sprintf(pclBgnTag,"<%s>",pclCfgName);
  sprintf(pclEndTag,"</%s>",pclCfgName);
  StrgPutStrg(pclOutBuff, &ilOutPos, pclBgnTag, 0, -1, pcgTplRecFldTypes);
  StrgPutStrg(pclOutBuff, &ilOutPos, pclEndTag, 0, -1, "\n");
  strcpy(pclCfgName,"TPL_FLD_DAT_TYPES");
  sprintf(pclBgnTag,"<%s>",pclCfgName);
  sprintf(pclEndTag,"</%s>",pclCfgName);
  StrgPutStrg(pclOutBuff, &ilOutPos, pclBgnTag, 0, -1, pcgTplFldDatTypes);
  StrgPutStrg(pclOutBuff, &ilOutPos, pclEndTag, 0, -1, "\n");
  strcpy(pclCfgName,"TPL_FLD_DAT_PROPS");
  sprintf(pclBgnTag,"<%s>",pclCfgName);
  sprintf(pclEndTag,"</%s>",pclCfgName);
  StrgPutStrg(pclOutBuff, &ilOutPos, pclBgnTag, 0, -1, pcgTplFldDatProps);
  StrgPutStrg(pclOutBuff, &ilOutPos, pclEndTag, 0, -1, "\n");
  strcpy(pclCfgName,"SENDER_TELEX_ADDR");
  sprintf(pclBgnTag,"<%s>",pclCfgName);
  sprintf(pclEndTag,"</%s>",pclCfgName);
  StrgPutStrg(pclOutBuff, &ilOutPos, pclBgnTag, 0, -1, pcgSenderTelexAddr);
  StrgPutStrg(pclOutBuff, &ilOutPos, pclEndTag, 0, -1, "\n");
  strcpy(pclCfgName,"SENDER_EMAIL_ADDR");
  sprintf(pclBgnTag,"<%s>",pclCfgName);
  sprintf(pclEndTag,"</%s>",pclCfgName);
  StrgPutStrg(pclOutBuff, &ilOutPos, pclBgnTag, 0, -1, pcgSenderEmailAddr);
  StrgPutStrg(pclOutBuff, &ilOutPos, pclEndTag, 0, -1, "\n");
  strcpy(pclCfgName,"SENDER_EMAIL_SMTP");
  sprintf(pclBgnTag,"<%s>",pclCfgName);
  sprintf(pclEndTag,"</%s>",pclCfgName);
  StrgPutStrg(pclOutBuff, &ilOutPos, pclBgnTag, 0, -1, pcgSenderEmailSmtp);
  StrgPutStrg(pclOutBuff, &ilOutPos, pclEndTag, 0, -1, "\n");
  dbg(TRACE,"CONFIGURATION DETAILS:\n%s",pclOutBuff);
  ilOutPos--;
  pclOutBuff[ilOutPos] = 0x00;
  if (ipQueOut > 0)
  {
    ilRc = SendCedaEvent(ipQueOut,0,mod_name,"","CONFIG",pcgTwEnd,
                "TLX", "TLXGEN", "CONFIG","FIELDS",pclOutBuff, "", 3, RC_SUCCESS); 
  }
  return ilRc;
}













