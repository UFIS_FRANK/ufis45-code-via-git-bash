#ifndef _DEF_mks_version
  #define _DEF_mks_version
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version[] = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Kernel/eqihdl.c 1.5 2009/01/21 21:56:13SGT akl Exp  $";
#endif /* _DEF_mks_version */

/*                                                                            */
/* UFIS AS EQIHDL.C                                                           */
/*                                                                            */
/* Author         : Andreas Kloidt                                            */
/* Date           : March 2006                                                */
/* Description    : Process to handle GPU, PCA and PLB Usage Data             */
/*                                                                            */
/* Update history :                                                           */
/*                                                                            */
/*                                                                            */
/******************************************************************************/
/*                                                                            */
/* source-code-control-system version string                                  */
static char sccs_eqihdl[]="%Z% UFIS 4.4 (c) ABB AAT/I %M% %I% / %E% %U% / AKL";
/* be carefule with strftime or similar functions !!!                         */
/*                                                                            */
/******************************************************************************/
/* This program is a MIKE main program */
#define U_MAIN
#define UGCCS_PRG
#define STH_USE

#include <stdio.h>
#include <malloc.h>
#include <errno.h>
#include <signal.h>
#include <netdb.h>
#include <time.h>
#include "debugrec.h"
#include "hsbsub.h"
#include "db_if.h" 
#include "tools.h"
#include "helpful.h"
#include "timdef.h"
 

#define XS_BUFF  128
#define S_BUFF   512
#define M_BUFF   1024
#define L_BUFF   2048
#define XL_BUFF  4096
#define XXL_BUFF 8192 



/******************************************************************************/
/* External variables                                                         */
/******************************************************************************/
FILE *outp       = NULL;
int  debug_level = TRACE;
/******************************************************************************/
/* External functions                                                         */
/******************************************************************************/
extern int SetSignals(void (*HandleSignal)(int));
extern int DebugPrintItem(int,ITEM *);
extern int DebugPrintEvent(int,EVENT *);
extern int init_db(void);
extern int  ResetDBCounter(void);
extern void HandleRemoteDB(EVENT*);
extern int  sql_if(short ,short* ,char* ,char* ); 
extern int close_my_cursor(short *cursor); 
extern void snap(char*,int,FILE*);
extern int GetDataItem(char *pcpResult, char *pcpInput, int ipNum, char cpDel,char *pcpDef,char *pcpTrim);
extern int BuildItemBuffer(char *pcpData, char *pcpFieldList, int ipNoOfFields, char *pcpSepChr);
extern int get_item_no(char *s, char *f, short elem_len);
extern void  GetServerTimeStamp(char*,int,long,char*);
extern int AddSecondsToCEDATime(char *,long,int);
extern int get_real_item(char *, char *, int);
extern int GetNoOfElements(char *s, char c);
extern long nap(long);
extern int SendCedaEvent(int,int,char*,char*,char*,char*,char*,char*,char*,char*,char*,char*,int,int);
/******************************************************************************/
/* Global variables                                                           */
/******************************************************************************/
static ITEM  *prgItem      = NULL;        /* The queue item pointer  */
static EVENT *prgEvent     = NULL;        /* The event pointer       */
static int   igItemLen     = 0;           /* length of incoming item */
static int   igInitOK      = FALSE;      /* Flag for init */

/*   static CFG   *prgCfg; */                     /* structure of the cfg-file */
static char  pcgHomeAp[XS_BUFF];      /* buffer for home airport */
static char  pcgTabEnd[XS_BUFF];      /* buffer for TABEND */
static char  pcgTwStart[XS_BUFF] = "";
static char  pcgTwEnd[XS_BUFF];
static char  pcgConfFile[S_BUFF];      /* buffer for config-file name */
static int   igUseHopo = FALSE;          /* flag for use of HOPO-field */
static int   igModID_Router  = 1200;      /* MOD-ID of Router  */

static char  pcgHostName[XS_BUFF];
static char  pcgHostIp[XS_BUFF];

static int   igQueCounter=0;
static int   que_out=0;

/*entry's from configfile*/
static char pcgConfigFile[512];
static char pcgCfgBuffer[512];
static EVENT *prgOutEvent = NULL;
static char pcgCurrentTime[32];
static int igDiffUtcToLocal;
static char pcgServerChars[110];
static char pcgClientChars[110];
static char pcgUfisConfigFile[512];
static int igTolerance;
static char cgSeperator;
static char pcgLogFileName[128];
static char pcgLogBuf[2048];
static FILE *pgLogFile = NULL;

static int igLastUrno = 0;
static int igLastUrnoIdx = -1;
static char pcgNewUrnos[1000*16];

#define MAX_POSITIONS 10000
typedef struct
{
  char pclPosNam[50];
  char pclGpuNam[50];
  char pclPcaNam[50];
  char pclPlbNam[50];
} _EQUI_ID;
_EQUI_ID rgEquipmentId[MAX_POSITIONS];
static int igNoPos;
static int igEquiHighNo;

static char pcgDataSource[16];
static char pcgEquipId[8];
static char pcgTimeStamp[8];
static char pcgTimeId[8];
static char pcgStatus[8];
static char pcgStart[8];
static char pcgStop[8];
static char pcgLastTimeStamp[16];

static int igTimeWindowForEqi;
static int igTimeWindowForAftArr;
static int igTimeWindowForAftDep;

static int igCount;
static int igERRCount;
static int igNOKCount;
static int igOKCount;

#define MAX_FIELDS_LOA 1000
typedef struct
{
  char pclFldNam[8];
  char pclLoaDssn[8];
  char pclLoaType[8];
  char pclLoaStyp[8];
  char pclLoaSstp[8];
  char pclLoaSsst[8];
  char pclLoaValu[8];
} _FLDS_LOA;
_FLDS_LOA rgFieldsLoa[MAX_FIELDS_LOA];
static int igNoFieldsLoa;
static char pcgLoaSelect[2048];
#define MAX_FIELDS_EQI 100
#define MAX_EQI_TIMES 10
typedef struct
{
  char pclFldNam[8];
  char pclTabNam[8];
  long llMinTime;
  char pclEqiValu[8];
  char pclBgn[MAX_EQI_TIMES][16];
  char pclEnd[MAX_EQI_TIMES][16];
} _FLDS_EQI;
_FLDS_EQI rgFieldsEqi[MAX_FIELDS_EQI];
static int igNoFieldsEqi;

static int igSendResponse = FALSE;

static int    Init_eqihdl();
static int    Reset(void);                        /* Reset program          */
static void   Terminate(int);                     /* Terminate program      */
static void   HandleSignal(int);                  /* Handles signals        */
static void   HandleErr(int);                     /* Handles general errors */
static void   HandleQueErr(int);                  /* Handles queuing errors */
static int    HandleInternalData(void);           /* Handles event data     */
static void   HandleQueues(void);                 /* Waiting for Sts.-switch*/
/******************************************************************************/
/* Function prototypes by AKL                                                 */
/******************************************************************************/
/* Init-functions  */

static int GetQueues();
static int SendEvent(char *pcpCmd,int ipModID,int ipPriority,char *pcpTable,
                     char *pcpTwStart,char* pcpTwEnd,
		     char *pcpSelection,char *pcpFields,char *pcpData,
                     char *pcpAddStruct,int ipAddstructSize); 
static int GetConfig();
static int TimeToStr(char *pcpTime,time_t lpTime);
static int TimeToStrLocal(char *pcpTime,time_t lpTime);
static int TriggerBchdlAction(char *pcpCmd, char *pcpTable, char *pcpSelection,
                              char *pcpFields, char *pcpData, int ipBchdl, int ipAction);
static long GetSecondsFromCEDATime(char *pcpDateTime);
static void TrimRight(char *pcpBuffer);
static void TrimLeftRight(char *pcpBuffer);
static int MyAddSecondsToCEDATime(char *pcpTime, long lpValue, int ipP);
static char *GetLine(char *pcpLine, int ipLineNo);
static void CopyLine(char *pcpDest, char *pcpSource);
static int GetNextUrno();

static int AssignUsage();
static int AssignToFlight(char *pcpUrno, char *pcpEquipment, char *pcpTimeStamp, char *pcpTimeId, char *pcpResult);
static int SearchArrTowInAft(char *pcpTimeStamp, char *pcpPosNam, char *pcpAftUrno);
static int SearchInFog(char *pcpTimeStamp, char *pcpPosNam, char *pcpAftUrno);
static int SearchDepInAft(char *pcpTimeStamp, char *pcpPosNam, char *pcpAftUrno);

static int GetLoaAndEqiData(char *pcpUrno, char *pcpFields);
static int GetEqiData(char *pcpUrno, char *pcpTable, char *pcpEqip, char *pcpValue);
/******************************************************************************/
/*                                                                            */
/* The MAIN program                                                           */
/*                                                                            */
/******************************************************************************/
MAIN
{
  int    ilRc = RC_SUCCESS;            /* Return code            */
  int    ilCnt = 0;
  int   ilItemFlag=TRUE; 
  time_t now = 0;
  INITIALIZE;            /* General initialization    */

  /* signal handling of SIGPIPE,SIGCHLD,SIGALRM,SIGTERM */
  SetSignals(HandleSignal);

  dbg(TRACE,"------------------------------------------");
  dbg(TRACE,"MAIN: version <%s>",sccs_eqihdl);

  /* Attach to the MIKE queues */
  do{
    ilRc = init_que();
    if(ilRc != RC_SUCCESS)
      {
	dbg(TRACE,"MAIN: init_que() failed! waiting 6 sec ...");
	sleep(6);
	ilCnt++;
      }/* end of if */
  }while((ilCnt < 10) && (ilRc != RC_SUCCESS));
  if(ilRc != RC_SUCCESS)
    {
      dbg(TRACE,"MAIN: init_que() failed! waiting 60 sec ...");
      sleep(60);
      exit(1);
    }else{
      dbg(TRACE,"MAIN: init_que() OK!");
      dbg(TRACE,"MAIN: mod_id   <%d>",mod_id);
      dbg(TRACE,"MAIN: mod_name <%s>",mod_name);
    }/* end of if */
  do
    {
      ilRc = init_db();
      if (ilRc != RC_SUCCESS)
	{
	  dbg(TRACE,"MAIN: init_db() failed! waiting 6 sec ...");
	  sleep(6);
	  ilCnt++;
	} /* end of if */
    } while((ilCnt < 10) && (ilRc != RC_SUCCESS));
  if(ilRc != RC_SUCCESS)
    {
      dbg(TRACE,"MAIN: init_db() failed! waiting 60 sec ...");
      sleep(60);
      exit(2);
    }else{
      dbg(TRACE,"MAIN: init_db()  OK!");
    } /* end of if */

  /* logon to DB is ok, but do NOT use DB while ctrl_sta == HSB_COMING_UP !!! */
  *pcgConfFile = 0x00;
  sprintf(pcgConfFile,"%s/%s",getenv("BIN_PATH"),mod_name);
  ilRc = TransferFile(pcgConfFile);
  if(ilRc != RC_SUCCESS)
    {
      dbg(TRACE,"MAIN: TransferFile(%s) failed!",pcgConfFile);
    } /* end of if */
  dbg(TRACE,"MAIN: Binary-file = <%s>",pcgConfFile);
  ilRc = SendRemoteShutdown(mod_id);
  if(ilRc != RC_SUCCESS)
    {
      dbg(TRACE,"MAIN: SendRemoteShutdown(%d) failed!",mod_id);
    }
  if((ctrl_sta != HSB_STANDALONE) && (ctrl_sta != HSB_ACTIVE) && (ctrl_sta != HSB_ACT_TO_SBY))
    {
      dbg(TRACE,"MAIN: waiting for status switch ...");
      HandleQueues();
      dbg(TRACE,"MAIN: now running ...");
    }/* end of if */
  if((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE) || (ctrl_sta == HSB_ACT_TO_SBY))
    {
      dbg(TRACE,"MAIN: initializing ...");
      dbg(TRACE,"------------------------------------------");
      if(igInitOK == FALSE)
	{
	  ilRc = Init_eqihdl();
	  if(ilRc == RC_SUCCESS)
	    {
	      dbg(TRACE,"");
	      dbg(TRACE,"------------------------------------------");
	      dbg(TRACE,"MAIN: initializing OK");
	      igInitOK = TRUE;
	    } 
	}
    }else{
      Terminate(1);
    }
  dbg(TRACE,"------------------------------------------");
    
  if (igInitOK == TRUE)
    {
      now = time(NULL);
      while(TRUE)
	{
	  memset(prgItem,0x00,igItemLen);
	  ilRc = que(QUE_GETBIG,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);
	  dbg(DEBUG,"QUE Counter %d",++igQueCounter);
	  /* depending on the size of the received item  */
	  /* a realloc could be made by the que function */
	  /* so do never forget to set event pointer !!! */
	  prgEvent = (EVENT *) prgItem->text;
	  if( ilRc == RC_SUCCESS )
	    {
	      /* Acknowledge the item */
	      ilRc = que(QUE_ACK,0,mod_id,0,0,NULL);
	      if( ilRc != RC_SUCCESS ) 
		{
		  /* handle que_ack error */
		  HandleQueErr(ilRc);
		} /* fi */
	      switch( prgEvent->command )
		{
		case    HSB_STANDBY    :
		  ctrl_sta = prgEvent->command;
		  HandleQueues();
		  break;    
		case    HSB_COMING_UP    :
		  ctrl_sta = prgEvent->command;
		  HandleQueues();
		  break;    
		case    HSB_ACTIVE    :
		  ctrl_sta = prgEvent->command;
		  break;    
		case    HSB_ACT_TO_SBY    :
		  ctrl_sta = prgEvent->command;
		  /* CloseConnection(); */
		  HandleQueues();
		  break;    
		case    HSB_DOWN    :
		  /* whole system shutdown - do not further use que(), send_message() or timsch() ! */
		  ctrl_sta = prgEvent->command;
		  Terminate(1);
		  break;    
		case    HSB_STANDALONE    :
		  ctrl_sta = prgEvent->command;
		  ResetDBCounter();
		  break;    
		case    REMOTE_DB :
		  /* ctrl_sta is checked inside */
		  HandleRemoteDB(prgEvent);
		  break;
		case    SHUTDOWN    :
		  /* process shutdown - maybe from uutil */
		  Terminate(1);
		  break;
		case    RESET        :
		  ilRc = Reset();
		  break;
		case    EVENT_DATA    :
		  if((ctrl_sta == HSB_STANDALONE) ||
		     (ctrl_sta == HSB_ACTIVE) ||
		     (ctrl_sta == HSB_ACT_TO_SBY))
		    {
		      ilItemFlag=TRUE;
		      ilRc = HandleInternalData();
		      if(ilRc != RC_SUCCESS)
			{
			  HandleErr(ilRc);
			}/* end of if */
		    }
		  else
		    {
		      dbg(TRACE,"MAIN: wrong HSB-status <%d>",ctrl_sta);
		      DebugPrintItem(TRACE,prgItem);
		      DebugPrintEvent(TRACE,prgEvent);
		    }/* end of if */
		  break; 
		case    TRACE_ON :
		  dbg_handle_debug(prgEvent->command);
		  break;
		case    TRACE_OFF :
		  dbg_handle_debug(prgEvent->command);
		  break;
                case  111 :
                  break;
		default            :
		  dbg(TRACE,"MAIN: unknown event");
		  DebugPrintItem(TRACE,prgItem);
		  DebugPrintEvent(TRACE,prgEvent);
		  break;
		} /* end switch */
	    }else{
	      /* Handle queuing errors */
	      HandleQueErr(ilRc);
	    } /* end else */



	  /**************************************************************/
	  /* time parameter for cyclic actions                          */
	  /**************************************************************/
    
	  now = time(NULL);

	} /* end while */
    }else{
      dbg(TRACE,"MAIN: Init_eqihdl() failed with <%d> Sleeping 30 sec.! Then terminating ...",ilRc);
      sleep(30);
    }
  exit(0);
  return 0;
} /* end of MAIN */

/******************************************************************************/
/* The initialization routine                                                 */
/******************************************************************************/
static int Init_eqihdl()
{
  int    ilRc = RC_SUCCESS;            /* Return code */

  GetQueues();
  /* reading default home-airport from sgs.tab */
  memset(pcgHomeAp,0x00,sizeof(pcgHomeAp));
  ilRc = tool_search_exco_data("SYS","HOMEAP",pcgHomeAp);
  if (ilRc != RC_SUCCESS)
    {
      dbg(TRACE,"Init_eqihdl : No HOMEAP entry in sgs.tab: EXTAB! Please add!");
      return RC_FAIL;
    }
  else
    {
      dbg(TRACE,"Init_eqihdl : HOMEAP = <%s>",pcgHomeAp);
    }
  /* reading default table-extension from sgs.tab */
  memset(pcgTabEnd,0x00,sizeof(pcgTabEnd));
  ilRc = tool_search_exco_data("ALL","TABEND",pcgTabEnd);
  if (ilRc != RC_SUCCESS)
    {
      dbg(TRACE,"Init_eqihdl : No TABEND entry in sgs.tab: EXTAB! Please add!");
      return RC_FAIL;
    }
  else
    {
      dbg(TRACE,"Init_eqihdl : TABEND = <%s>",pcgTabEnd);
      memset(pcgTwEnd,0x00,XS_BUFF);
      sprintf(pcgTwEnd,"%s,%s,%s",pcgHomeAp,pcgTabEnd,mod_name);
      dbg(TRACE,"Init_eqihdl : TW_END = <%s>",pcgTwEnd);
      
      if (strcmp(pcgTabEnd,"TAB") == 0)
	{
	  igUseHopo = TRUE;
	  dbg(TRACE,"Init_eqihdl: use HOPO-field!");
	}
    }

  ilRc = GetConfig();

  ilRc = TimeToStr(pcgCurrentTime,time(NULL));
  
  strcpy(pcgLastTimeStamp,"");

  return(ilRc);
} /* end of initialize */
/*********************************************************************
Function : GetQueues()
Paramter :
Return Code: RC_SUCCESS,RC_FAIL
Result:
Description: Gets all necessary queue-ID's for CEDA-internal
             communication!
*********************************************************************/
static int GetQueues()
{
  int ilRc = RC_FAIL;

  /* get mod-id of router */
  if ((igModID_Router = tool_get_q_id("router")) == RC_NOT_FOUND ||
      igModID_Router == RC_FAIL || igModID_Router == 0)
    {
      dbg(TRACE,"GetQueues   : tool_get_q_id(router) returns: <%d>",igModID_Router);
      ilRc = RC_FAIL;
    }else{
      dbg(TRACE,"GetQueues   : <router> mod_id <%d>",igModID_Router);
      ilRc = RC_SUCCESS;
    } 
  return ilRc;
}

static int CheckCfg(void)
{
  int ilRc = RC_SUCCESS;
  return ilRc;
}
/******************************************************************************/
/* The Reset routine                                                          */
/******************************************************************************/
static int Reset()
{

  int    ilRc = RC_SUCCESS;    /* Return code */
    
  dbg(TRACE,"Reset: now reseting ...");

  return ilRc;
    
} /* end of Reset */
/******************************************************************************/
/* The termination routine                                                    */
/******************************************************************************/
static void Terminate(int ipSleep)
{
  dbg(TRACE,"Terminate: now leaving ...");
 
  sleep(ipSleep);
  
  exit(0);
    
} /* end of Terminate */
/******************************************************************************/
/* The handle signals routine                                                 */
/******************************************************************************/
static void HandleSignal(int pipSig)
{
  switch(pipSig)
    {
    case SIGALRM:
      break;
    case SIGPIPE:
      break;
    case SIGCHLD:
      break;
    case SIGTERM:
      Terminate(1);
      break;
    default    :
      Terminate(10);
      break;
    } /* end of switch */
} /* end of HandleSignal */
/******************************************************************************/
/* The handle general error routine                                           */
/******************************************************************************/
static void HandleErr(int pipErr)
{
  /*    int    ilRc = RC_SUCCESS; */
    return;
} /* end of HandleErr */
/******************************************************************************/
/* The handle queuing error routine                                           */
/******************************************************************************/
static void HandleQueErr(int pipErr)
{
    int    ilRc = RC_SUCCESS;
    
    switch(pipErr) {
    case    QUE_E_FUNC    :    /* Unknown function */
        dbg(TRACE,"<%d> : unknown function",pipErr);
        break;
    case    QUE_E_MEMORY    :    /* Malloc reports no memory */
        dbg(TRACE,"<%d> : malloc failed",pipErr);
        break;
    case    QUE_E_SEND    :    /* Error using msgsnd */
            dbg(TRACE,"<%d> : msgsnd failed",pipErr);
            break;
    case    QUE_E_GET    :    /* Error using msgrcv */
            if(pipErr != 4)        
             dbg(DEBUG,"<%d> : msgrcv failed",pipErr);
        break;
    case    QUE_E_EXISTS    :
        dbg(TRACE,"<%d> : route/queue already exists ",pipErr);
        break;
    case    QUE_E_NOFIND    :
        dbg(TRACE,"<%d> : route not found ",pipErr);
        break;
    case    QUE_E_ACKUNEX    : 
        dbg(TRACE,"<%d> : unexpected ack received ",pipErr);
        break;
    case    QUE_E_STATUS    :
        dbg(TRACE,"<%d> :   unknown queue status ",pipErr);
        break;
    case    QUE_E_INACTIVE    :
        dbg(TRACE,"<%d> : queue is inaktive ",pipErr);
        break;
    case    QUE_E_MISACK    :
        dbg(TRACE,"<%d> : missing ack ",pipErr);
        break;
    case    QUE_E_NOQUEUES    :
        dbg(TRACE,"<%d> : queue does not exist",pipErr);
        break;
    case    QUE_E_RESP    :    /* No response on CREATE */
        dbg(TRACE,"<%d> : no response on create",pipErr);
        break;
    case    QUE_E_FULL    :
        dbg(TRACE,"<%d> : too many route destinations",pipErr);
        break;
    case    QUE_E_NOMSG    :    /* No message on queue */
        /*dbg(TRACE,"<%d> : no messages on queue",pipErr);*/
        break;
    case    QUE_E_INVORG    :    /* Mod id by que call is 0 */
        dbg(TRACE,"<%d> : invalid originator=0",pipErr);
        break;
    case    QUE_E_NOINIT    :    /* Queues is not initialized*/
        dbg(TRACE,"<%d> : queues are not initialized",pipErr);
        break;
    case    QUE_E_ITOBIG    :
        dbg(TRACE,"<%d> : requestet itemsize to big ",pipErr);
        break;
    case    QUE_E_BUFSIZ    :
        dbg(TRACE,"<%d> : receive buffer to small ",pipErr);
        break;
    case    QUE_E_PRIORITY    :
        dbg(TRACE,"<%d> : wrong priority was send ",pipErr);
        break;
    default            :    /* Unknown queue error */
        dbg(TRACE,"<%d> : unknown error",pipErr);
        break;
    } /* end switch */
         
    return;
} /* end of HandleQueErr */
/******************************************************************************/
/* The handle queues routine                                                  */
/******************************************************************************/
static void HandleQueues()
{
  int    ilRc = RC_SUCCESS;            /* Return code */
  int    ilBreakOut = FALSE;
    
  do{
    memset(prgItem,0x00,igItemLen);
    ilRc = que(QUE_GETBIG,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);
    /* depending on the size of the received item  */
    /* a realloc could be made by the que function */
    /* so do never forget to set event pointer !!! */
    prgEvent = (EVENT *) prgItem->text;    
    if( ilRc == RC_SUCCESS )
      {
    /* Acknowledge the item */
    ilRc = que(QUE_ACK,0,mod_id,0,0,NULL);
    if( ilRc != RC_SUCCESS ) 
      {
        /* handle que_ack error */
        HandleQueErr(ilRc);
      } /* fi */
        
    switch( prgEvent->command )
      {
      case    HSB_STANDBY    :
        ctrl_sta = prgEvent->command;
        break;    
      case    HSB_COMING_UP    :
        ctrl_sta = prgEvent->command;
        break;    
      case    HSB_ACTIVE    :
        ctrl_sta = prgEvent->command;
        ilBreakOut = TRUE;
        break;    
      case    HSB_ACT_TO_SBY    :
        ctrl_sta = prgEvent->command;
        break;    
      case    HSB_DOWN    :
        /* whole system shutdown - do not further use que(), send_message() or timsch() ! */
        ctrl_sta = prgEvent->command;
        Terminate(10);
        break;    
      case    HSB_STANDALONE    :
        ctrl_sta = prgEvent->command;
        ResetDBCounter();
        ilBreakOut = TRUE;
        break;    
      case    REMOTE_DB :
        /* ctrl_sta is checked inside */
        HandleRemoteDB(prgEvent);
        break;
      case    SHUTDOWN    :
        Terminate(1);
        break;
      case    RESET        :
        ilRc = Reset();
        break;
      case    EVENT_DATA    :
        dbg(TRACE,"HandleQueues: wrong hsb status <%d>",ctrl_sta);
        DebugPrintItem(TRACE,prgItem);
        DebugPrintEvent(TRACE,prgEvent);
        break;
      case    TRACE_ON :
        dbg_handle_debug(prgEvent->command);
        break;
      case    TRACE_OFF :
        dbg_handle_debug(prgEvent->command);
        break;
      default            :
        dbg(TRACE,"HandleQueues: unknown event");
        DebugPrintItem(TRACE,prgItem);

        DebugPrintEvent(TRACE,prgEvent);

        break;
      } /* end switch */
      }else{
    /* Handle queuing errors */
    HandleQueErr(ilRc);
      } /* end else */
  } while (ilBreakOut == FALSE);
  if(igInitOK == FALSE)
    {
      ilRc = Init_eqihdl();
      if(ilRc == RC_SUCCESS)
    {
      dbg(TRACE,"HandleQueues: Init_eqihdl() OK!");
      igInitOK = TRUE;
    }else{ /* end of if */
      dbg(TRACE,"HandleQueues: Init_eqihdl() failed!");
      igInitOK = FALSE;
    } /* end of if */
    }/* end of if */
  /* OpenConnection(); */
} /* end of HandleQueues */
/******************************************************************************/
/* The handle data routine                                                    */
/******************************************************************************/
static int HandleInternalData()
{
  int  ilRC = RC_SUCCESS;      /* Return code */
  char *pclSelection = NULL;
  char *pclFields = NULL;
  char *pclData = NULL;
  char pclNewData[512000];
  char pclOldData[512000];
  char pclUrno[50];
  char *pclTmpPtr=NULL;
  BC_HEAD *bchd = NULL;          /* Broadcast header*/
  CMDBLK  *cmdblk = NULL; 
  char pclTmpInitFog[32];
  char pclRegn[32];
  char pclStartTime[32];
  int ilLen;

  que_out = prgEvent->originator;
  bchd  = (BC_HEAD *) ((char *)prgEvent + sizeof(EVENT));
  cmdblk= (CMDBLK  *) ((char *)bchd->data);
  if (bchd->rc == NETOUT_NO_ACK)
     igSendResponse = FALSE;
  else
     igSendResponse = TRUE;

  strcpy(pcgTwStart,cmdblk->tw_start);
  strcpy(pcgTwEnd,cmdblk->tw_end);

  /***********************************/
  /*    DebugPrintItem(DEBUG,prgItem);   */
  /*    DebugPrintEvent(DEBUG,prgEvent); */
  /***********************************/
  memset(pclOldData,0x00,L_BUFF);
  pclSelection = cmdblk->data;
  pclFields = (char *)pclSelection + strlen(pclSelection) + 1;
  pclData = (char *)pclFields + strlen(pclFields) + 1;

  strcpy(pclNewData,pclData);
  *pclUrno = '\0';
  pclTmpPtr = strstr(pclSelection,"\n");
  if (pclTmpPtr != NULL)
  {
     *pclTmpPtr = '\0';
     pclTmpPtr++;
     strcpy(pclUrno,pclTmpPtr);
  }

  ilRC = TimeToStr(pcgCurrentTime,time(NULL));

  if (strcmp(cmdblk->command,"ASGU") == 0)
  {
     dbg(DEBUG,"Command:   <%s>",cmdblk->command);
     dbg(DEBUG,"Selection: <%s><%s>",pclSelection,pclUrno);
     dbg(DEBUG,"Fields:    <%s>",pclFields);
     dbg(DEBUG,"Data:      <%s>",pclNewData);
     dbg(DEBUG,"TwStart:   <%s>",pcgTwStart);
     dbg(DEBUG,"TwEnd:     <%s>",pcgTwEnd);
     ilRC = AssignUsage();
  }
  else if (strcmp(cmdblk->command,"GLED") == 0)
  {
     dbg(DEBUG,"Command:   <%s>",cmdblk->command);
     dbg(DEBUG,"Selection: <%s><%s>",pclSelection,pclUrno);
     dbg(DEBUG,"Fields:    <%s>",pclFields);
     dbg(DEBUG,"Data:      <%s>",pclNewData);
     dbg(DEBUG,"TwStart:   <%s>",pcgTwStart);
     dbg(DEBUG,"TwEnd:     <%s>",pcgTwEnd);
     ilRC = GetLoaAndEqiData(pclSelection,pclFields);
  }

  dbg(DEBUG,"========================= START / END =========================");
  return ilRC;
} /* end of HandleInternalData */
/*********************************************************************
Function : SendEvent()
Paramter : IN: pcpCmd = command for cmdblk->command
           IN: ipModID = process-ID where the event is send to
       IN: ipPriority = priority for sending ( 1- 5, usuallay 3)
       IN: pcpTable = Name (3 letters) of cmdblk->obj_name (if
           necessary), will be expanded with "pcgTabEnd".
       IN: pcpTwStart = cmdblk->twstart
       IN: pcpTwEnd = cmdblk->twend (always HOMEAP,TABEND,processname)
       IN: pcpSelection = selection for event (cmdblk->data)
       IN: pcpFields = fieldlist (corresponding to pcpdata)
       IN: pcpData = datalist (comma separated, corresponding to 
                                   pcpFields)
       IN: pcpAddStruct = additional structure to be transmitted
       IN: ipAddStructSize = size of the additional structure
Return Code: RC_SUCCESS, RC_FAIL
Result:
Description: Sends an event to another CEDA-process using que(QUE_PUT).
             Sends the event in standard CEDA-format (BCHEAD,CMDBLK,
         selection,fieldlist,datalist) or sends a different
         data structure (special) at CMDBLK->data. !! Sends always
         only one type, standard OR special, with one event !!
*********************************************************************/
static int SendEvent(char *pcpCmd,int ipModID,int ipPriority,char *pcpTable,
             char *pcpTwStart, char *pcpTwEnd,
             char *pcpSelection,char *pcpFields,char *pcpData,
             char *pcpAddStruct,int ipAddStructSize)
{
  int     ilRc             = RC_FAIL;
  int     ilLen            = 0;
  EVENT   *prlOutEvent  = NULL;
  BC_HEAD *prlOutBCHead = NULL;
  CMDBLK  *prlOutCmdblk = NULL;

  if (pcpAddStruct == NULL)
    ipAddStructSize = 0;
  if (ipAddStructSize == 0)
    pcpAddStruct = NULL;

  /* size-calculation for prlOutEvent */
  ilLen = sizeof(EVENT) + sizeof(BC_HEAD) + sizeof(CMDBLK) + 
    strlen(pcpSelection) + strlen(pcpFields) + strlen(pcpData) + 
    ipAddStructSize + 128;

  /* memory for prlOutEvent */
  if ((prlOutEvent = (EVENT*)malloc((size_t)ilLen)) == NULL)
    {
      dbg(TRACE,"SendEvent: cannot malloc <%d>-bytes for outgoing event!",ilLen);
      prlOutEvent = NULL;
    }else{
      /* clear whole outgoing event */
      memset((void*)prlOutEvent, 0x00, ilLen);

      /* set event structure... */
      prlOutEvent->type         = SYS_EVENT;
      prlOutEvent->command    = EVENT_DATA;
      prlOutEvent->originator   = (short)mod_id;
      prlOutEvent->retry_count  = 0;
      prlOutEvent->data_offset  = sizeof(EVENT);
      prlOutEvent->data_length  = ilLen - sizeof(EVENT); 

      /* BC_HEAD-Structure... */
      prlOutBCHead = (BC_HEAD*)((char*)prlOutEvent+sizeof(EVENT));
      /* prlOutBCHead->rc = (short)RC_SUCCESS;*/
      prlOutBCHead->rc = (short)NETOUT_NO_ACK;/*spaeter nur bei disconnect*/
      strncpy(prlOutBCHead->dest_name,mod_name,10);
      strncpy(prlOutBCHead->recv_name, "EXCO",10);
 
      /* Cmdblk-Structure... */
      prlOutCmdblk = (CMDBLK*)((char*)prlOutBCHead->data);
      strcpy(prlOutCmdblk->command,pcpCmd);
      if (pcpTable != NULL)
    {
      strcpy(prlOutCmdblk->obj_name,pcpTable);
      strcat(prlOutCmdblk->obj_name,pcgTabEnd);
    }
        
      /* setting tw_x entries */
      strncpy(prlOutCmdblk->tw_start,pcpTwStart,32);
      strncpy(prlOutCmdblk->tw_end,pcpTwEnd,32);
        
      /* means that no additional structure is used */
      /* STANDARD CEDA-ipcs between CEDA-processes */
      if (pcpAddStruct == NULL)
    {
      /* setting selection inside event */
      strcpy(prlOutCmdblk->data,pcpSelection);
      /* setting field-list inside event */
      strcpy(prlOutCmdblk->data+strlen(pcpSelection)+1,pcpFields);
      /* setting data-list inside event */
      strcpy((prlOutCmdblk->data + (strlen(pcpSelection)+1) + (strlen(pcpFields)+1)),pcpData);
    }else{
      /*an additional structure is used and will be copied to */
      /*cmdblk + sizeof(CMDBLK).!!! No STANDARD CEDA-ipcs is used !!!! */
      memcpy(prlOutCmdblk->data,(char*)pcpAddStruct,ipAddStructSize);    
    }

      /*DebugPrintEvent(DEBUG,prlOutEvent);*/ 
      /*snapit((char*)prlOutEvent,ilLen,outp);*/
      dbg(DEBUG,"SendEvent: sending event to mod_id <%d>",ipModID);

      if (ipModID != 0)
    {
      if ((ilRc = que(QUE_PUT,ipModID,mod_id,ipPriority,ilLen,(char*)prlOutEvent))
          != RC_SUCCESS)
        {
          dbg(TRACE,"SendEvent: QUE_PUT returns: <%d>", ilRc);
          Terminate(1);
        }
    }else{
      dbg(TRACE,"SendEvent: mod_id = <%d>! Can't send!",ipModID);
    }
      /* free memory */
      free((void*)prlOutEvent); 
    }
  return ilRc;
}

/*
	Get Config Entries
*/
static int GetConfig()
{
  int ilRC = RC_SUCCESS;
  char pclFunc[] = "GetConfig:";
  int ilI;
  int ilJ;
  char pclTmpBuf[16000];
  char pclResult[512];
  char pclDebugLevel[128];
  char pclFldNam[10];
  char pclFldDat[16];
  char pclItem[256];
  char pclSelDssn[256];
  char pclSelType[256];
  char pclSelStyp[256];
  char pclSelSstp[256];
  char pclSelSsst[256];
  char pclBuffer[10];
  char pclClieBuffer[100];
  char pclServBuffer[100];

  sprintf(pcgConfigFile,"%s/%s.cfg",getenv("CFG_PATH"),mod_name);
  dbg(TRACE,"%s Config File is <%s>",pclFunc,pcgConfigFile);

  ilRC = iGetConfigEntry(pcgConfigFile,"EQUI-NAME","EQUI_HIGH_NO",CFG_STRING,pclTmpBuf);
  if (ilRC == RC_SUCCESS)
     igEquiHighNo = atoi(pclTmpBuf);
  else
     igEquiHighNo = MAX_POSITIONS;
  igNoPos = 0;
  for (ilI = 0; ilI < igEquiHighNo; ilI++)
  {
     if (ilI < 10)
        sprintf(pclTmpBuf,"EQUI000%d",ilI);
     else if (ilI < 100)
        sprintf(pclTmpBuf,"EQUI00%d",ilI);
     else if (ilI < 1000)
        sprintf(pclTmpBuf,"EQUI0%d",ilI);
     else
        sprintf(pclTmpBuf,"EQUI%d",ilI);
     ilRC = iGetConfigEntry(pcgConfigFile,"EQUI-NAME",pclTmpBuf,CFG_STRING,pclResult);
     if (ilRC == RC_SUCCESS)
     {
        get_real_item(&rgEquipmentId[igNoPos].pclPosNam[0],pclResult,1);
        get_real_item(&rgEquipmentId[igNoPos].pclGpuNam[0],pclResult,2);
        get_real_item(&rgEquipmentId[igNoPos].pclPcaNam[0],pclResult,3);
        get_real_item(&rgEquipmentId[igNoPos].pclPlbNam[0],pclResult,4);
        igNoPos++;
     }
  }

  ilRC = iGetConfigEntry(pcgConfigFile,"EQIHDL","DATA_SOURCE",CFG_STRING,pcgDataSource);
  ilRC = iGetConfigEntry(pcgConfigFile,"EQIHDL","EQUIPMENT_ID",CFG_STRING,pcgEquipId);
  ilRC = iGetConfigEntry(pcgConfigFile,"EQIHDL","TIME_STAMP",CFG_STRING,pcgTimeStamp);
  ilRC = iGetConfigEntry(pcgConfigFile,"EQIHDL","TIME_ID",CFG_STRING,pclResult);
  get_real_item(pcgTimeId,pclResult,1);
  get_real_item(pcgStart,pclResult,2);
  get_real_item(pcgStop,pclResult,3);
  ilRC = iGetConfigEntry(pcgConfigFile,"EQIHDL","STATUS",CFG_STRING,pcgStatus);

  ilRC = iGetConfigEntry(pcgConfigFile,"EQIHDL","TIME_WINDOW_FOR_EQI",CFG_STRING,pclResult);
  if (ilRC == RC_SUCCESS)
     igTimeWindowForEqi = atoi(pclResult);
  else
     igTimeWindowForEqi = 24;

  ilRC = iGetConfigEntry(pcgConfigFile,"EQIHDL","TIME_WINDOW_FOR_AFT_ARR",CFG_STRING,pclResult);
  if (ilRC == RC_SUCCESS)
     igTimeWindowForAftArr = atoi(pclResult);
  else
     igTimeWindowForAftArr = 12;
  ilRC = iGetConfigEntry(pcgConfigFile,"EQIHDL","TIME_WINDOW_FOR_AFT_DEP",CFG_STRING,pclResult);
  if (ilRC == RC_SUCCESS)
     igTimeWindowForAftDep = atoi(pclResult);
  else
     igTimeWindowForAftDep = 30;

  ilRC = iGetConfigEntry(pcgConfigFile,"EQIHDL","TOLERANCE",CFG_STRING,pclResult);
  if (ilRC == RC_SUCCESS)
     igTolerance = atoi(pclResult);
  else
     igTolerance = 0;

  ilRC = iGetConfigEntry(pcgConfigFile,"EQIHDL","LOG_FILE",CFG_STRING,pclResult);
  if (ilRC == RC_SUCCESS)
     strcpy(pcgLogFileName,pclResult);
  else
     strcpy(pcgLogFileName,"/ceda/debug/eqihdl_log.log");
  ilRC = iGetConfigEntry(pcgConfigFile,"EQIHDL","EXCEL_SEPERATOR",CFG_STRING,pclResult);
  if (ilRC == RC_SUCCESS)
     cgSeperator = pclResult[0];
  else
     cgSeperator = ',';

  ilRC = iGetConfigEntry(pcgConfigFile,"EQIHDL","DEBUG_LEVEL",CFG_STRING,pclDebugLevel);
  if (strcmp(pclDebugLevel,"DEBUG") == 0)
  {
     debug_level = DEBUG;
  }
  else
  {
     if (strcmp(pclDebugLevel,"TRACE") == 0)
     {
        debug_level = TRACE;
     }
     else
     {
        if (strcmp(pclDebugLevel,"NULL") == 0)
        {
           debug_level = 0;
        }
        else
        {
           debug_level = TRACE;
        }
     }
  }

  strcpy(pclSelDssn,"");
  strcpy(pclSelType,"");
  strcpy(pclSelStyp,"");
  strcpy(pclSelSstp,"");
  strcpy(pclSelSsst,"");
  ilRC = iGetConfigEntry(pcgConfigFile,"FLDDESCRLOA","FIELDS",CFG_STRING,pclTmpBuf);
  igNoFieldsLoa = GetNoOfElements(pclTmpBuf,',');
  for (ilI = 0; ilI < igNoFieldsLoa; ilI++)
  {
     get_real_item(pclFldNam,pclTmpBuf,ilI+1);
     ilRC = iGetConfigEntry(pcgConfigFile,"FLDDESCRLOA",pclFldNam,CFG_STRING,pclResult);
     if (ilRC == RC_SUCCESS)
     {
        strcpy(&rgFieldsLoa[ilI].pclFldNam[0],pclFldNam);
        get_real_item(pclFldDat,pclResult,1);
        if (strlen(pclFldDat) == 0)
           strcpy(pclFldDat," ");
        strcpy(&rgFieldsLoa[ilI].pclLoaDssn[0],pclFldDat);
        sprintf(pclItem,"'%s'",pclFldDat);
        if (strlen(pclSelDssn) == 0)
           sprintf(pclSelDssn,"(%s",pclItem);
        else if (strstr(pclSelDssn,pclItem) == NULL)
        {
           strcat(pclSelDssn,",");
           strcat(pclSelDssn,pclItem);
        }
        get_real_item(pclFldDat,pclResult,2);
        if (strlen(pclFldDat) == 0)
           strcpy(pclFldDat," ");
        strcpy(&rgFieldsLoa[ilI].pclLoaType[0],pclFldDat);
        sprintf(pclItem,"'%s'",pclFldDat);
        if (strlen(pclSelType) == 0)
           sprintf(pclSelType,"(%s",pclItem);
        else if (strstr(pclSelType,pclItem) == NULL)
        {
           strcat(pclSelType,",");
           strcat(pclSelType,pclItem);
        }
        get_real_item(pclFldDat,pclResult,3);
        if (strlen(pclFldDat) == 0)
           strcpy(pclFldDat," ");
        strcpy(&rgFieldsLoa[ilI].pclLoaStyp[0],pclFldDat);
        sprintf(pclItem,"'%s'",pclFldDat);
        if (strlen(pclSelStyp) == 0)
           sprintf(pclSelStyp,"(%s",pclItem);
        else if (strstr(pclSelStyp,pclItem) == NULL)
        {
           strcat(pclSelStyp,",");
           strcat(pclSelStyp,pclItem);
        }
        get_real_item(pclFldDat,pclResult,4);
        if (strlen(pclFldDat) == 0)
           strcpy(pclFldDat," ");
        strcpy(&rgFieldsLoa[ilI].pclLoaSstp[0],pclFldDat);
        sprintf(pclItem,"'%s'",pclFldDat);
        if (strlen(pclSelSstp) == 0)
           sprintf(pclSelSstp,"(%s",pclItem);
        else if (strstr(pclSelSstp,pclItem) == NULL)
        {
           strcat(pclSelSstp,",");
           strcat(pclSelSstp,pclItem);
        }
        get_real_item(pclFldDat,pclResult,5);
        if (strlen(pclFldDat) == 0)
           strcpy(pclFldDat," ");
        strcpy(&rgFieldsLoa[ilI].pclLoaSsst[0],pclFldDat);
        sprintf(pclItem,"'%s'",pclFldDat);
        if (strlen(pclSelSsst) == 0)
           sprintf(pclSelSsst,"(%s",pclItem);
        else if (strstr(pclSelSsst,pclItem) == NULL)
        {
           strcat(pclSelSsst,",");
           strcat(pclSelSsst,pclItem);
        }
        strcpy(&rgFieldsLoa[ilI].pclLoaValu[0],"");
     }
  }
  if (strlen(pclSelDssn) > 0)
     strcat(pclSelDssn,")");
  if (strlen(pclSelType) > 0)
     strcat(pclSelType,")");
  if (strlen(pclSelStyp) > 0)
     strcat(pclSelStyp,")");
  if (strlen(pclSelSstp) > 0)
     strcat(pclSelSstp,")");
  if (strlen(pclSelSsst) > 0)
     strcat(pclSelSsst,")");
  strcpy (pcgLoaSelect,"");
  if (strlen(pclSelDssn) > 0)
  {
     sprintf(pclItem,"DSSN IN %s AND ",pclSelDssn);
     strcat(pcgLoaSelect,pclItem);
  }
  if (strlen(pclSelType) > 0)
  {
     sprintf(pclItem,"TYPE IN %s AND ",pclSelType);
     strcat(pcgLoaSelect,pclItem);
  }
  if (strlen(pclSelStyp) > 0)
  {
     sprintf(pclItem,"STYP IN %s AND ",pclSelStyp);
     strcat(pcgLoaSelect,pclItem);
  }
  if (strlen(pclSelSstp) > 0)
  {
     sprintf(pclItem,"SSTP IN %s AND ",pclSelSstp);
     strcat(pcgLoaSelect,pclItem);
  }
  if (strlen(pclSelSsst) > 0)
  {
     sprintf(pclItem,"SSST IN %s AND ",pclSelSsst);
     strcat(pcgLoaSelect,pclItem);
  }
  if (strlen(pcgLoaSelect) >= 5)
     pcgLoaSelect[strlen(pcgLoaSelect)-5] = '\0';

  ilRC = iGetConfigEntry(pcgConfigFile,"FLDDESCREQI","FIELDS",CFG_STRING,pclTmpBuf);
  igNoFieldsEqi = GetNoOfElements(pclTmpBuf,',');
  for (ilI = 0; ilI < igNoFieldsEqi; ilI++)
  {
     get_real_item(pclFldNam,pclTmpBuf,ilI+1);
     ilRC = iGetConfigEntry(pcgConfigFile,"FLDDESCREQI",pclFldNam,CFG_STRING,pclResult);
     if (ilRC == RC_SUCCESS)
     {
        strcpy(&rgFieldsEqi[ilI].pclFldNam[0],pclFldNam);
        get_real_item(&rgFieldsEqi[ilI].pclTabNam[0],pclResult,1);
        get_real_item(pclItem,pclResult,2);
        rgFieldsEqi[ilI].llMinTime = atol(pclItem);
        strcpy(&rgFieldsEqi[ilI].pclEqiValu[0],"");
     }
  }

  /* Get Client and Server chars */
  sprintf(pcgUfisConfigFile,"%s/ufis_ceda.cfg",getenv("CFG_PATH")); 
  ilRC = iGetConfigEntry(pcgUfisConfigFile,"CHAR_PATCH","CLIENT_CHARS",
                         CFG_STRING, pclClieBuffer);
  if (ilRC == RC_SUCCESS)
  {
     ilRC = iGetConfigEntry(pcgUfisConfigFile,"CHAR_PATCH","SERVER_CHARS",
                            CFG_STRING, pclServBuffer);
     if (ilRC == RC_SUCCESS)
     {
        ilI = GetNoOfElements(pclClieBuffer,',');
        if (ilI == GetNoOfElements(pclServBuffer,','))	
        {
           memset(pcgServerChars,0x00,100*sizeof(char));
           memset(pcgClientChars,0x00,100*sizeof(char));
           for (ilJ=0; ilJ < ilI; ilJ++) 
           {
              GetDataItem(pclBuffer,pclServBuffer,ilJ+1,',',"","\0\0");
              pcgServerChars[ilJ*2] = atoi(pclBuffer);	
              pcgServerChars[ilJ*2+1] = ',';
           }
           pcgServerChars[(ilJ-1)*2+1] = 0x00;
           for (ilJ=0; ilJ < ilI; ilJ++) 
           {
              GetDataItem(pclBuffer,pclClieBuffer,ilJ+1,',',"","\0\0");
              pcgClientChars[ilJ*2] = atoi(pclBuffer);	
              pcgClientChars[ilJ*2+1] = ',';
           }
           pcgClientChars[(ilJ-1)*2+1] = 0x00;
           dbg(DEBUG,"%s New Clientchars <%s> dec <%s>",pclFunc,pcgClientChars,pclClieBuffer);
           dbg(DEBUG,"%s New Serverchars <%s> dec <%s>",pclFunc,pcgServerChars,pclServBuffer);
        }
     }
     else
     {
        ilRC = RC_SUCCESS;
        dbg(DEBUG,"%s Use standard (old) serverchars",pclFunc);	
     }
  }
  else
  {
     dbg(DEBUG,"%s Use standard (old) serverchars",pclFunc);	
     ilRC = RC_SUCCESS;
  }
  if (ilRC != RC_SUCCESS)
  {
     strcpy(pcgClientChars,"\042\047\054\012\015");
     strcpy(pcgServerChars,"\260\261\262\263\263");
     ilRC = RC_SUCCESS;
  }

  dbg(TRACE,"%s DEBUG_LEVEL = %s",pclFunc,pclDebugLevel);
  dbg(TRACE,"%s Highest Equipment Number = %d",pclFunc,igEquiHighNo);
  for (ilI = 0; ilI < igNoPos; ilI++)
  {
     dbg(TRACE,"%s (%d/%d) Equipment Name for Position %s = <%s><%s><%s>",pclFunc,
         ilI+1,igNoPos,
         &rgEquipmentId[ilI].pclPosNam[0],
         &rgEquipmentId[ilI].pclGpuNam[0],
         &rgEquipmentId[ilI].pclPcaNam[0],
         &rgEquipmentId[ilI].pclPlbNam[0]);
  }
  dbg(TRACE,"%s Table for Data Source is            <%s>",pclFunc,pcgDataSource);
  dbg(TRACE,"%s Field Name for Equipment Id is      <%s>",pclFunc,pcgEquipId);
  dbg(TRACE,"%s Field Name for Time Stamp is        <%s>",pclFunc,pcgTimeStamp);
  dbg(TRACE,"%s Field Name for Time Id is           <%s>",pclFunc,pcgTimeId);
  dbg(TRACE,"%s Field Name for Processing Status is <%s>",pclFunc,pcgStatus);
  dbg(TRACE,"%s Keyword for Start Time is           <%s>",pclFunc,pcgStart);
  dbg(TRACE,"%s Keyword for Stop Time is            <%s>",pclFunc,pcgStop);
  dbg(TRACE,"%s Time Window for %s = %d Hours back",pclFunc,pcgDataSource,igTimeWindowForEqi);
  dbg(TRACE,"%s Time Window for AFTTAB (Arrival / Towing) = %d Hours back",pclFunc,igTimeWindowForAftArr);
  dbg(TRACE,"%s Time Window for AFTTAB (Departure) = %d Mins foreward",pclFunc,igTimeWindowForAftDep);
  dbg(TRACE,"%s Tolerance Value for On/OffBlock = %d Mins",pclFunc,igTolerance);
  dbg(TRACE,"%s Log File Name = <%s> with Seperator = <%c>",pclFunc,pcgLogFileName,cgSeperator);
  for (ilI = 0; ilI < igNoFieldsLoa; ilI++)
  {
     dbg(TRACE,"%s Defines for Field %s in LOATAB = <%s><%s><%s><%s><%s>",pclFunc,
         &rgFieldsLoa[ilI].pclFldNam[0],
         &rgFieldsLoa[ilI].pclLoaDssn[0],
         &rgFieldsLoa[ilI].pclLoaType[0],
         &rgFieldsLoa[ilI].pclLoaStyp[0],
         &rgFieldsLoa[ilI].pclLoaSstp[0],
         &rgFieldsLoa[ilI].pclLoaSsst[0]);
  }
  dbg(TRACE,"%s Select for Field DSSN in LOATAB = <%s>",pclFunc,pclSelDssn);
  dbg(TRACE,"%s Select for Field TYPE in LOATAB = <%s>",pclFunc,pclSelType);
  dbg(TRACE,"%s Select for Field STYP in LOATAB = <%s>",pclFunc,pclSelStyp);
  dbg(TRACE,"%s Select for Field SSTP in LOATAB = <%s>",pclFunc,pclSelSstp);
  dbg(TRACE,"%s Select for Field SSST in LOATAB = <%s>",pclFunc,pclSelSsst);
  dbg(TRACE,"%s Select for LOATAB = <%s>",pclFunc,pcgLoaSelect);
  for (ilI = 0; ilI < igNoFieldsEqi; ilI++)
  {
     dbg(TRACE,"%s Table Name for Equipment Field %s is %s with min Duration of %ld minutes",pclFunc,
         &rgFieldsEqi[ilI].pclFldNam[0],&rgFieldsEqi[ilI].pclTabNam[0],
         rgFieldsEqi[ilI].llMinTime);
  }

  return RC_SUCCESS;
} /* Enf of GetConfig */

/******************************************************************************/
/* The TimeToStr routine                                                      */
/******************************************************************************/
static int TimeToStr(char *pcpTime,time_t lpTime) 
{                                                                              
  struct tm *_tm;
    
  _tm = (struct tm *)gmtime(&lpTime);

  sprintf(pcpTime,"%4d%02d%02d%02d%02d%02d",
          _tm->tm_year+1900,_tm->tm_mon+1,_tm->tm_mday,_tm->tm_hour,
          _tm->tm_min,_tm->tm_sec);

  return RC_SUCCESS;  
                        
}     /* end of TimeToStr */

/******************************************************************************/
/* The TimeToStrLocal routine                                                 */
/******************************************************************************/
static int TimeToStrLocal(char *pcpTime,time_t lpTime) 
{                                                                              
  struct tm *_tm;
    
  _tm = (struct tm *)localtime(&lpTime);

  sprintf(pcpTime,"%4d%02d%02d%02d%02d%02d",
          _tm->tm_year+1900,_tm->tm_mon+1,_tm->tm_mday,_tm->tm_hour,
          _tm->tm_min,_tm->tm_sec);

  return RC_SUCCESS;  
                        
}     /* end of TimeToStrLocal */


static int TriggerBchdlAction(char *pcpCmd, char *pcpTable, char *pcpSelection,
                              char *pcpFields, char *pcpData, int ipBchdl, int ipAction)
{
  int ilRC = RC_SUCCESS;

  if (ipBchdl == TRUE)
  {
     (void) tools_send_info_flag(1900,0,"eqihdl","","CEDA","","",pcgTwStart,pcgTwEnd,
                                 pcpCmd,pcpTable,pcpSelection,pcpFields,pcpData,0);
     dbg(DEBUG,"Send Broadcast: <%s><%s><%s><%s><%s>",
         pcpCmd,pcpTable,pcpSelection,pcpFields,pcpData);
  }
  if (ipAction == TRUE)
  {
     (void) tools_send_info_flag(7400,0,"eqihdl","","CEDA","","",pcgTwStart,pcgTwEnd,
                                 pcpCmd,pcpTable,pcpSelection,pcpFields,pcpData,0);
     dbg(DEBUG,"Send to ACTION: <%s><%s><%s><%s><%s>",
         pcpCmd,pcpTable,pcpSelection,pcpFields,pcpData);
  }

  return ilRC;
} /* End of TriggerBchdlAction */


/*******************************************++++++++++*************************/
/*   GetSecondsFromCEDATime                                                   */
/*                                                                            */
/*   Serviceroutine, rechnet einen Datumseintrag im Cedaformat in Seconds um  */
/*   Input:  char *  Zeiger auf CEDA-Zeitpuffer                               */
/*   Output: long    Zeitwert                                                 */
/******************************************************************************/

static long GetSecondsFromCEDATime(char *pcpDateTime)
{
  long rc = 0;
  int year;
  char ch_help[5];
  struct tm *tstr1 = NULL, t1;
  struct tm *CurTime, t2;
  time_t llTime;
  time_t llUtcTime;
  time_t llLocalTime;

  CurTime = &t2;
  llTime = time(NULL);
  CurTime = (struct tm *)gmtime(&llTime);
  CurTime->tm_isdst = 0;
  llUtcTime = mktime(CurTime);
  CurTime = (struct tm *)localtime(&llTime);
  CurTime->tm_isdst = 0;
  llLocalTime = mktime(CurTime);
  igDiffUtcToLocal = llLocalTime - llUtcTime;
  /* dbg(TRACE,"UTC: <%ld> , Local: <%ld> , Diff: <%ld>",llUtcTime,llLocalTime,igDiffUtcToLocal); */

  memset(&t1,0x00,sizeof(struct tm));

  tstr1 = &t1;

  memset(ch_help,0x00,5);
  strncpy(ch_help,pcpDateTime,4);
  year = atoi(ch_help);
  tstr1->tm_year = year - 1900;

  memset(ch_help,0x00,5);
  strncpy(ch_help,&pcpDateTime[4],2);
  tstr1->tm_mon = atoi(ch_help) -1;

  memset(ch_help,0x00,5);
  strncpy(ch_help,&pcpDateTime[6],2);
  tstr1->tm_mday = atoi(ch_help);

  memset(ch_help,0x00,5);
  strncpy(ch_help,&pcpDateTime[8],2);
  tstr1->tm_hour = atoi(ch_help);

  memset(ch_help,0x00,5);
  strncpy(ch_help,&pcpDateTime[10],2);
  tstr1->tm_min = atoi(ch_help);

  memset(ch_help,0x00,5);
  strncpy(ch_help,&pcpDateTime[12],2);
  tstr1->tm_sec = atoi(ch_help);

  tstr1->tm_wday = 0;
  tstr1->tm_yday = 0;
  tstr1->tm_isdst = 0;
  rc = mktime(tstr1);

  rc = rc + igDiffUtcToLocal; /* add difference between local and utc */

  return(rc);
} /* end of GetSecondsFromCEDATime() */


static void TrimRight(char *pcpBuffer)
{
  char *pclBlank = &pcpBuffer[strlen(pcpBuffer)-1];

  if (strlen(pcpBuffer) == 0)
  {
     strcpy(pcpBuffer, " ");
  }
  else
  {
     while (isspace(*pclBlank) && pclBlank != pcpBuffer)
     {
        *pclBlank = '\0';
        pclBlank--;
     }
  }
} /* End of TrimRight */


static void TrimLeftRight(char *pcpBuffer)
{
  char *pclBlank = &pcpBuffer[0];

  if (strlen(pcpBuffer) == 0)
  {
     strcpy(pcpBuffer, " ");
  }
  else
  {
     while (isspace(*pclBlank) && *pclBlank != '\0')
     {
        pclBlank++;
     }
     strcpy(pcpBuffer,pclBlank);
     TrimRight(pcpBuffer);
  }
} /* End of TrimRight */


static int MyAddSecondsToCEDATime(char *pcpTime, long lpValue, int ipP)
{
  int ilRC = RC_SUCCESS;

  if (strlen(pcpTime) == 12)
     strcat(pcpTime,"00");
  ilRC = AddSecondsToCEDATime(pcpTime,lpValue,ipP);
  return ilRC;
} /* End of MyAddSecondsToCEDATime */


static char *GetLine(char *pcpLine, int ipLineNo)
{
  char *pclResult = NULL;
  char *pclP = pcpLine;
  int ilCurLine;

  ilCurLine = 1;
  while (*pclP != '\0' && ilCurLine < ipLineNo)
  {
     if (*pclP == '\n')
        ilCurLine++;
     pclP++;
  }
  if (*pclP != '\0')
     pclResult = pclP;

  return pclResult;
} /* End of GetLine */


static void CopyLine(char *pcpDest, char *pcpSource)
{
  char *pclP1 = pcpDest;
  char *pclP2 = pcpSource;

  while (*pclP2 != '\n' && *pclP2 != '\0')
  {
     *pclP1 = *pclP2;
     pclP1++;
     pclP2++;
  }
  *pclP1 = '\0';
  TrimRight(pcpDest);
} /* End of CopyLine */


static int GetNextUrno()
{
  int ilRC = RC_SUCCESS;
  char pclFunc[] = "GetNextUrno:";
  int ilDebugLevel;

  if (igLastUrnoIdx == 1000 || igLastUrnoIdx < 0)
  {
     ilDebugLevel = debug_level;
     debug_level = 0;
     GetNextValues(pcgNewUrnos,1010);
     debug_level = ilDebugLevel;
     igLastUrnoIdx = 0;
     igLastUrno = atoi(pcgNewUrnos);
  }
  else
  {
     igLastUrnoIdx++;
     igLastUrno++;
  }

  return ilRC;
} /* End of GetNextUrno */


static int AssignUsage()
{
  int ilRC = RC_SUCCESS;
  char pclFunc[] = "AssignUsage:";
  int ilRCdb = DB_SUCCESS;
  short slFkt;
  short slCursor;
  char pclSelection[1024];
  char pclSqlBuf[1024];
  char pclDataBuf[2048];
  int ilRCdbUpd = DB_SUCCESS;
  short slFktUpd;
  short slCursorUpd;
  char pclSelectionUpd[1024];
  char pclSqlBufUpd[1024];
  char pclDataBufUpd[2048];
  char pclFieldListUpd[1024];
  char pclTmpTime[64];
  char pclFieldList[1024];
  char pclUrno[64];
  char pclEquipment[64];
  char pclTimeStamp[64];
  char pclTimeId[64];
  char pclStatus[64];
  char pclResult[64];
  char pclLogFileName[128];

  TimeToStrLocal(pclTimeStamp,time(NULL));
  sprintf(pclLogFileName,"%s_%s.csv",pcgLogFileName,pclTimeStamp);
  pgLogFile = fopen(pclLogFileName,"w");
  if (!pgLogFile)
     dbg(TRACE,"%s Log File <%s> could not be opened. fopen() returns <%s>",pclFunc,pcgLogFileName,strerror(errno));
  if (pgLogFile)
  {
     memset(pclTmpTime,0x00,64);
     strncpy(pclTmpTime,&pclTimeStamp[6],2);
     strcat(pclTmpTime,".");
     strncat(pclTmpTime,&pclTimeStamp[4],2);
     strcat(pclTmpTime,".");
     strncat(pclTmpTime,&pclTimeStamp[0],4);
     strcat(pclTmpTime," ");
     strncat(pclTmpTime,&pclTimeStamp[8],2);
     strcat(pclTmpTime,":");
     strncat(pclTmpTime,&pclTimeStamp[10],2);
     strcat(pclTmpTime,":");
     strncat(pclTmpTime,&pclTimeStamp[12],4);
     sprintf(pcgLogBuf,"START: %s\n\n",pclTmpTime);
     fwrite(pcgLogBuf,sizeof(char),strlen(pcgLogBuf),pgLogFile);
     sprintf(pcgLogBuf,"EQUIPMENT%cTIME%cTYPE%cADID%cFLIGHT%cSTAD%cREMARK\n",
             cgSeperator,cgSeperator,cgSeperator,cgSeperator,cgSeperator,cgSeperator);
     fwrite(pcgLogBuf,sizeof(char),strlen(pcgLogBuf),pgLogFile);
     fflush(pgLogFile);
  }
  igERRCount = 0;
  igNOKCount = 0;
  igOKCount = 0;
  if (strlen(pcgLastTimeStamp) == 0)
  {
     strcpy(pclTmpTime,pcgCurrentTime);
     ilRC = MyAddSecondsToCEDATime(pclTmpTime,igTimeWindowForEqi*60*60*(-1),1);
     sprintf(pclSelection,"WHERE %s > '%s' AND %s = ' '",
             pcgTimeStamp,pclTmpTime,pcgStatus);
     sprintf(pclSqlBuf,"SELECT MIN(%s) FROM %s %s",pcgTimeStamp,pcgDataSource,pclSelection);
     slCursor = 0;
     slFkt = START;
     dbg(DEBUG,"%s SQL = <%s>",pclFunc,pclSqlBuf);
     ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
     close_my_cursor(&slCursor);
     if (ilRCdb == DB_SUCCESS)
     {
        if (strlen(pclDataBuf) == 0)
           strcpy(pcgLastTimeStamp,pclTmpTime);
        else
           strcpy(pcgLastTimeStamp,pclDataBuf);
     }
     else
        strcpy(pcgLastTimeStamp,pclTmpTime);
  }
  else
     MyAddSecondsToCEDATime(pcgLastTimeStamp,48*60*60*(-1),1);
  sprintf(pclFieldList,"URNO,%s,%s,%s",pcgEquipId,pcgTimeStamp,pcgTimeId);
  sprintf(pclSelection,"WHERE %s >= '%s' AND %s = ' ' ORDER BY %s",
          pcgTimeStamp,pcgLastTimeStamp,pcgStatus,pcgTimeStamp);
  sprintf(pclSqlBuf,"SELECT %s FROM %s %s",pclFieldList,pcgDataSource,pclSelection);
  slCursor = 0;
  slFkt = START;
  dbg(DEBUG,"%s SQL = <%s>",pclFunc,pclSqlBuf);
  ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
  while (ilRCdb == DB_SUCCESS)
  {
     BuildItemBuffer(pclDataBuf,"",4,",");
     get_real_item(pclUrno,pclDataBuf,1);
     get_real_item(pclEquipment,pclDataBuf,2);
     get_real_item(pclTimeStamp,pclDataBuf,3);
     get_real_item(pclTimeId,pclDataBuf,4);
     if (pgLogFile)
     {
        sprintf(pcgLogBuf,"%s%c%s%c%s%c",pclEquipment,cgSeperator,pclTimeStamp,cgSeperator,pclTimeId,cgSeperator);
     }
     ilRC = AssignToFlight(pclUrno,pclEquipment,pclTimeStamp,pclTimeId,pclResult);
     if (ilRC == RC_SUCCESS)
     {
        sprintf(pclFieldListUpd,"%s='OK'",pcgStatus);
        igOKCount++;
     }
     else
        sprintf(pclFieldListUpd,"%s='%s'",pcgStatus,pclResult);
     sprintf(pclSelectionUpd,"WHERE %s = '%s' AND %s = '%s' AND %s = '%s'",
             pcgEquipId,pclEquipment,
             pcgTimeStamp,pclTimeStamp,
             pcgTimeId,pclTimeId);
     sprintf(pclSqlBufUpd,"UPDATE %s SET %s %s",pcgDataSource,pclFieldListUpd,pclSelectionUpd);
     slCursorUpd = 0;
     slFktUpd = START;
     dbg(DEBUG,"%s SQL = <%s>",pclFunc,pclSqlBufUpd);
     ilRCdbUpd = sql_if(slFktUpd,&slCursorUpd,pclSqlBufUpd,pclDataBufUpd);
     if (ilRCdbUpd == DB_SUCCESS)
        commit_work();
     else
        dbg(TRACE,"%s Error updating %s",pclFunc,pcgDataSource);
     close_my_cursor(&slCursorUpd);
     strcpy(pcgLastTimeStamp,pclTimeStamp);
     slFkt = NEXT;
     ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
  }
  close_my_cursor(&slCursor);
  dbg(DEBUG,"%s Error Records:     %d",pclFunc,igERRCount);
  dbg(DEBUG,"%s Not Found Records: %d",pclFunc,igNOKCount);
  dbg(DEBUG,"%s Assigned Records:  %d",pclFunc,igOKCount);
  dbg(DEBUG,"%s Total Records:     %d",pclFunc,igERRCount+igNOKCount+igOKCount);
  if (pgLogFile)
  {
     strcpy(pcgLogBuf,"\nSummary:\n");
     fwrite(pcgLogBuf,sizeof(char),strlen(pcgLogBuf),pgLogFile);
     sprintf(pcgLogBuf,"   Error Records:     %d\n",igERRCount);
     fwrite(pcgLogBuf,sizeof(char),strlen(pcgLogBuf),pgLogFile);
     sprintf(pcgLogBuf,"   Not Found Records: %d\n",igNOKCount);
     fwrite(pcgLogBuf,sizeof(char),strlen(pcgLogBuf),pgLogFile);
     sprintf(pcgLogBuf,"   Assigned Records:  %d\n",igOKCount);
     fwrite(pcgLogBuf,sizeof(char),strlen(pcgLogBuf),pgLogFile);
     sprintf(pcgLogBuf,"   Total Records:     %d\n",igERRCount+igNOKCount+igOKCount);
     fwrite(pcgLogBuf,sizeof(char),strlen(pcgLogBuf),pgLogFile);
     TimeToStrLocal(pclTimeStamp,time(NULL));
     memset(pclTmpTime,0x00,64);
     strncpy(pclTmpTime,&pclTimeStamp[6],2);
     strcat(pclTmpTime,".");
     strncat(pclTmpTime,&pclTimeStamp[4],2);
     strcat(pclTmpTime,".");
     strncat(pclTmpTime,&pclTimeStamp[0],4);
     strcat(pclTmpTime," ");
     strncat(pclTmpTime,&pclTimeStamp[8],2);
     strcat(pclTmpTime,":");
     strncat(pclTmpTime,&pclTimeStamp[10],2);
     strcat(pclTmpTime,":");
     strncat(pclTmpTime,&pclTimeStamp[12],4);
     sprintf(pcgLogBuf,"\nEND: %s\n",pclTmpTime);
     fwrite(pcgLogBuf,sizeof(char),strlen(pcgLogBuf),pgLogFile);
     fflush(pgLogFile);
     fclose(pgLogFile);
  }

  return ilRC;
} /* End of AssignUsage */


static int AssignToFlight(char *pcpUrno, char *pcpEquipment, char *pcpTimeStamp, char *pcpTimeId, char *pcpResult)
{
  int ilRC = RC_SUCCESS;
  char pclFunc[] = "AssignToFlight:";
  int ilRCdb = DB_SUCCESS;
  short slFkt;
  short slCursor;
  char pclSelection[1024];
  char pclSqlBuf[1024];
  char pclDataBuf[2048];
  char pclFieldList[1024];
  char pclDataList[1024];
  int ilI;
  int ilFound;
  char pclPosNam[64];
  char pclAftUrno[16];
  char pclEquipmentType[64];
  char pclTableName[16];
  typedef struct
  {
    char pclUrno[16];
    char pclFlnu[16];
    char pclGaab[16];
    char pclGaae[16];
    char pclAseq[8];
    char pclHopo[8];
    char pclUsec[36];
    char pclCdat[16];
    char pclUseu[36];
    char pclLstu[16];
  } _EQUI_TAB;
  _EQUI_TAB rlEquipmentTab[100];
  int ilEqiTabCnt;
  int ilUpdate;
  char pclAftFlno[16];
  char pclAftStoa[16];
  char pclAftStod[16];
  char pclAftAdid[16];
  char pclTmpTime[64];
  char pclTmpBuf[1024];
  char pclFndUrno[16];
  char pclFndFlno[16];
  char pclFndAdid[16];
  char pclFndFtyp[16];
  char pclFndPsta[16];
  char pclFndPstd[16];

  strcpy(pclEquipmentType,"");
  strcpy(pclPosNam,"(");
  ilFound = FALSE;
  for (ilI = 0; ilI < igNoPos && ilFound == FALSE; ilI++)
  {
     if (strcmp(&rgEquipmentId[ilI].pclGpuNam[0],pcpEquipment) == 0)
     {
        strcpy(pclEquipmentType,"GPU");
        strcpy(pclTableName,"GPATAB");
        ilFound = TRUE;
     }
     else if (strcmp(&rgEquipmentId[ilI].pclPcaNam[0],pcpEquipment) == 0)
     {
        strcpy(pclEquipmentType,"PCA");
        strcpy(pclTableName,"PCATAB");
        ilFound = TRUE;
     }
     else if (strcmp(&rgEquipmentId[ilI].pclPlbNam[0],pcpEquipment) == 0)
     {
        strcpy(pclEquipmentType,"PLB");
        strcpy(pclTableName,"PLBTAB");
        ilFound = TRUE;
     }
     if (ilFound == TRUE)
     {
        strcat(pclPosNam,"'");
        strcat(pclPosNam,&rgEquipmentId[ilI].pclPosNam[0]);
        strcat(pclPosNam,"',");
        ilFound = FALSE;
     }
  }
  dbg(TRACE,"%s",pclFunc);
  dbg(TRACE,"%s",pclFunc);
  if (strlen(pclPosNam) == 1)
  {
     if (strcmp(pcpTimeId,pcgStart) == 0)
        dbg(TRACE,"%s %s Equipment <%s> not configured. Start Time = <%s>",
            pclFunc,pclEquipmentType,pcpEquipment,pcpTimeStamp);
     else
        dbg(TRACE,"%s %s Equipment <%s> not configured. Stop Time = <%s>",
            pclFunc,pclEquipmentType,pcpEquipment,pcpTimeStamp);
     strcpy(pcpResult,"ERR");
     igERRCount++;
     if (pgLogFile)
     {
        sprintf(pclTmpBuf,"%c%c%cInvalid equipment name\n",cgSeperator,cgSeperator,cgSeperator);
        strcat(pcgLogBuf,pclTmpBuf);
        fwrite(pcgLogBuf,sizeof(char),strlen(pcgLogBuf),pgLogFile);
        fflush(pgLogFile);
     }
     return RC_FAIL;
  }
  pclPosNam[strlen(pclPosNam)-1] = '\0';
  strcat(pclPosNam,")");
  strcpy(pcpResult,"NOK");
  if (strcmp(pcpTimeId,pcgStart) == 0)
     dbg(TRACE,"%s Usage of %s Equipment <%s> at Position(s) <%s> started at <%s>",
         pclFunc,pclEquipmentType,pcpEquipment,pclPosNam,pcpTimeStamp);
  else
     dbg(TRACE,"%s Usage of %s Equipment <%s> at Position(s) <%s> stopped at <%s>",
         pclFunc,pclEquipmentType,pcpEquipment,pclPosNam,pcpTimeStamp);
  igCount = 0;
  strcpy(pclAftUrno,"");
  ilRC = SearchArrTowInAft(pcpTimeStamp,pclPosNam,pclAftUrno);
  if (ilRC == RC_SUCCESS)
  {
     if (strlen(pclAftUrno) == 0)
     {
        dbg(TRACE,"%s No Flight found for Usage of Equipment",pclFunc);
        dbg(TRACE,"%s %d Record(s) found for Investigation.",pclFunc,igCount);
        igNOKCount++;
        if (pgLogFile)
        {
           sprintf(pclTmpBuf,"%c%c%cNo flight found\n",cgSeperator,cgSeperator,cgSeperator);
           strcat(pcgLogBuf,pclTmpBuf);
           fwrite(pcgLogBuf,sizeof(char),strlen(pcgLogBuf),pgLogFile);
           fflush(pgLogFile);
        }
        return RC_FAIL;
     }
  }
  if (strlen(pclAftUrno) == 0)
  {
     ilRC = SearchInFog(pcpTimeStamp,pclPosNam,pclAftUrno);
     if (ilRC == RC_SUCCESS)
     {
        if (strlen(pclAftUrno) == 0)
        {
           dbg(TRACE,"%s No Flight found for Usage of Equipment",pclFunc);
           dbg(TRACE,"%s %d Record(s) found for Investigation.",pclFunc,igCount);
           igNOKCount++;
           if (pgLogFile)
           {
              sprintf(pclTmpBuf,"%c%c%cNo flight found\n",cgSeperator,cgSeperator,cgSeperator);
              strcat(pcgLogBuf,pclTmpBuf);
              fwrite(pcgLogBuf,sizeof(char),strlen(pcgLogBuf),pgLogFile);
              fflush(pgLogFile);
           }
           return RC_FAIL;
        }
     }
  }
  if (strlen(pclAftUrno) == 0)
  {
     ilRC = SearchDepInAft(pcpTimeStamp,pclPosNam,pclAftUrno);
     if (ilRC == RC_SUCCESS)
     {
        if (strlen(pclAftUrno) == 0)
        {
           dbg(TRACE,"%s No Flight found for Usage of Equipment",pclFunc);
           dbg(TRACE,"%s %d Record(s) found for Investigation.",pclFunc,igCount);
           igNOKCount++;
           if (pgLogFile)
           {
              sprintf(pclTmpBuf,"%c%c%cNo flight found\n",cgSeperator,cgSeperator,cgSeperator);
              strcat(pcgLogBuf,pclTmpBuf);
              fwrite(pcgLogBuf,sizeof(char),strlen(pcgLogBuf),pgLogFile);
              fflush(pgLogFile);
           }
           return RC_FAIL;
        }
     }
  }
  if (strlen(pclAftUrno) == 0)
  {
     dbg(TRACE,"%s No Flight found for Usage of Equipment",pclFunc);
     dbg(TRACE,"%s %d Record(s) found for Investigation.",pclFunc,igCount);
     igNOKCount++;
     if (pgLogFile)
     {
        sprintf(pclTmpBuf,"%c%c%cNo flight found\n",cgSeperator,cgSeperator,cgSeperator);
        strcat(pcgLogBuf,pclTmpBuf);
        fwrite(pcgLogBuf,sizeof(char),strlen(pcgLogBuf),pgLogFile);
        fflush(pgLogFile);
     }
     return RC_FAIL;
  }

  strcpy(pclFieldList,"URNO,FLNO,ADID,FTYP,PSTA,PSTD");
  sprintf(pclSelection,"WHERE RKEY = %s ORDER BY ADID,STOA",pclAftUrno);
  sprintf(pclSqlBuf,"SELECT %s FROM AFTTAB %s",pclFieldList,pclSelection);
  slCursor = 0;
  slFkt = START;
  dbg(DEBUG,"%s SQL = <%s>",pclFunc,pclSqlBuf);
  ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
  while (ilRCdb == DB_SUCCESS)
  {
     BuildItemBuffer(pclDataBuf,"",6,",");
     get_real_item(pclFndUrno,pclDataBuf,1);
     get_real_item(pclFndFlno,pclDataBuf,2);
     get_real_item(pclFndAdid,pclDataBuf,3);
     get_real_item(pclFndFtyp,pclDataBuf,4);
     get_real_item(pclFndPsta,pclDataBuf,5);
     get_real_item(pclFndPstd,pclDataBuf,6);
     if (*pclFndAdid == 'D' && strstr(pclPosNam,pclFndPstd) != NULL)
     {
        strcpy(pclAftUrno,pclFndUrno);
        dbg(TRACE,"%s Assign Usage of Equipment to Departure Flight",pclFunc);
     }
     slFkt = NEXT;
     ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
  }
  close_my_cursor(&slCursor);

  dbg(TRACE,"%s Usage of %s Equipment will be assigned to Flight with URNO = <%s>",
      pclFunc,pclEquipmentType,pclAftUrno);
  dbg(TRACE,"%s %d Record(s) found for Investigation.",pclFunc,igCount);
  strcpy(pclFieldList,"URNO,FLNU,GAAB,GAAE,ASEQ,HOPO,USEC,CDAT,USEU,LSTU");
  sprintf(pclSelection,"WHERE FLNU = '%s' ORDER BY ASEQ",pclAftUrno);
  sprintf(pclSqlBuf,"SELECT %s FROM %s %s",pclFieldList,pclTableName,pclSelection);
  ilEqiTabCnt = 0;
  slCursor = 0;
  slFkt = START;
  dbg(DEBUG,"%s SQL = <%s>",pclFunc,pclSqlBuf);
  ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
  while (ilRCdb == DB_SUCCESS)
  {
     BuildItemBuffer(pclDataBuf,"",10,",");
     get_real_item(&rlEquipmentTab[ilEqiTabCnt].pclUrno[0],pclDataBuf,1);
     get_real_item(&rlEquipmentTab[ilEqiTabCnt].pclFlnu[0],pclDataBuf,2);
     get_real_item(&rlEquipmentTab[ilEqiTabCnt].pclGaab[0],pclDataBuf,3);
     get_real_item(&rlEquipmentTab[ilEqiTabCnt].pclGaae[0],pclDataBuf,4);
     get_real_item(&rlEquipmentTab[ilEqiTabCnt].pclAseq[0],pclDataBuf,5);
     get_real_item(&rlEquipmentTab[ilEqiTabCnt].pclHopo[0],pclDataBuf,6);
     get_real_item(&rlEquipmentTab[ilEqiTabCnt].pclUsec[0],pclDataBuf,7);
     get_real_item(&rlEquipmentTab[ilEqiTabCnt].pclCdat[0],pclDataBuf,8);
     get_real_item(&rlEquipmentTab[ilEqiTabCnt].pclUseu[0],pclDataBuf,9);
     get_real_item(&rlEquipmentTab[ilEqiTabCnt].pclLstu[0],pclDataBuf,10);
     ilEqiTabCnt++;
     slFkt = NEXT;
     ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
  }
  close_my_cursor(&slCursor);
/*
for (ilI = 0; ilI < ilEqiTabCnt; ilI++)
{
   dbg(TRACE,"AKL %d <%s><%s><%s><%s><%s><%s><%s><%s><%s><%s>",ilI+1,
       &rlEquipmentTab[ilI].pclUrno[0],
       &rlEquipmentTab[ilI].pclFlnu[0],
       &rlEquipmentTab[ilI].pclGaab[0],
       &rlEquipmentTab[ilI].pclGaae[0],
       &rlEquipmentTab[ilI].pclAseq[0],
       &rlEquipmentTab[ilI].pclHopo[0],
       &rlEquipmentTab[ilI].pclUsec[0],
       &rlEquipmentTab[ilI].pclCdat[0],
       &rlEquipmentTab[ilI].pclUseu[0],
       &rlEquipmentTab[ilI].pclLstu[0]);
}
*/
  ilUpdate = -1;
  for (ilI = 0; ilI < ilEqiTabCnt && ilUpdate == -1; ilI++)
  {
     if (strcmp(pcpTimeId,pcgStart) == 0)
     {
        if (strlen(&rlEquipmentTab[ilI].pclGaab[0]) == 0 &&
            strcmp(pcpTimeStamp,&rlEquipmentTab[ilI].pclGaae[0]) <= 0)
        {
           strcpy(&rlEquipmentTab[ilI].pclGaab[0],pcpTimeStamp);
           ilUpdate = ilI;
        }
     }
     else
     {
        if (strlen(&rlEquipmentTab[ilI].pclGaae[0]) == 0 &&
            strcmp(&rlEquipmentTab[ilI].pclGaab[0],pcpTimeStamp) <= 0)
        {
           strcpy(&rlEquipmentTab[ilI].pclGaae[0],pcpTimeStamp);
           ilUpdate = ilI;
        }
     }
     if (ilUpdate >= 0)
     {
        strcpy(&rlEquipmentTab[ilUpdate].pclUseu[0],mod_name);
        strcpy(&rlEquipmentTab[ilUpdate].pclLstu[0],pcgCurrentTime);
     }
  }
  ilRC = RC_SUCCESS;
  if (ilUpdate == -1)
  {
     ilRC = GetNextUrno();
     if (strcmp(pcpTimeId,pcgStart) == 0)
     {
        strcpy(pclFieldList,"URNO,FLNU,GAAB,ASEQ,HOPO,USEC,CDAT");
        sprintf(pclDataList,"'%d','%s','%s','%d','%s','%s','%s'",
                igLastUrno,pclAftUrno,pcpTimeStamp,ilEqiTabCnt+1,pcgHomeAp,mod_name,pcgCurrentTime);
     }
     else
     {
        strcpy(pclFieldList,"URNO,FLNU,GAAE,ASEQ,HOPO,USEC,CDAT");
        sprintf(pclDataList,"'%d','%s','%s','%d','%s','%s','%s'",
                igLastUrno,pclAftUrno,pcpTimeStamp,ilEqiTabCnt+1,pcgHomeAp,mod_name,pcgCurrentTime);
     }
     sprintf(pclSqlBuf,"INSERT INTO %s FIELDS(%s) VALUES(%s)",pclTableName,pclFieldList,pclDataList);
     slCursor = 0;
     slFkt = START;
     dbg(DEBUG,"%s SQL = <%s>",pclFunc,pclSqlBuf);
     ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
     if (ilRCdb == DB_SUCCESS)
        commit_work();
     else
     {
        dbg(TRACE,"%s Error inserting into %s",pclFunc,pclTableName);
        ilRC = RC_FAIL;
     }
     close_my_cursor(&slCursor);
  }
  else
  {
     sprintf(pclFieldList,"GAAB='%s',GAAE='%s',USEU='%s',LSTU='%s'",
             &rlEquipmentTab[ilUpdate].pclGaab[0],&rlEquipmentTab[ilUpdate].pclGaae[0],
             &rlEquipmentTab[ilUpdate].pclUseu[0],&rlEquipmentTab[ilUpdate].pclLstu[0]);
     sprintf(pclSelection,"WHERE URNO = '%s'",&rlEquipmentTab[ilUpdate].pclUrno[0]);
     sprintf(pclSqlBuf,"UPDATE %s SET %s %s",pclTableName,pclFieldList,pclSelection);
     slCursor = 0;
     slFkt = START;
     dbg(DEBUG,"%s SQL = <%s>",pclFunc,pclSqlBuf);
     ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
     if (ilRCdb == DB_SUCCESS)
        commit_work();
     else
     {
        dbg(TRACE,"%s Error updating %s",pclFunc,pclTableName);
        ilRC = RC_FAIL;
     }
     close_my_cursor(&slCursor);
  }
  if (pgLogFile)
  {
     strcpy(pclFieldList,"FLNO,STOA,STOD,ADID");
     sprintf(pclSelection,"WHERE URNO = %s",pclAftUrno);
     sprintf(pclSqlBuf,"SELECT %s FROM AFTTAB %s",pclFieldList,pclSelection);
     slCursor = 0;
     slFkt = START;
     dbg(DEBUG,"%s SQL = <%s>",pclFunc,pclSqlBuf);
     ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
     if (ilRCdb == DB_SUCCESS)
     {
        BuildItemBuffer(pclDataBuf,"",4,",");
        get_real_item(pclAftFlno,pclDataBuf,1);
        get_real_item(pclAftStoa,pclDataBuf,2);
        get_real_item(pclAftStod,pclDataBuf,3);
        get_real_item(pclAftAdid,pclDataBuf,4);
        if (*pclAftAdid == 'D')
           strcpy(pclAftStoa,pclAftStod);
        memset(pclTmpTime,0x00,64);
        strncpy(pclTmpTime,&pclAftStoa[6],2);
        strcat(pclTmpTime,".");
        strncat(pclTmpTime,&pclAftStoa[4],2);
        strcat(pclTmpTime,".");
        strncat(pclTmpTime,&pclAftStoa[0],4);
        strcat(pclTmpTime," ");
        strncat(pclTmpTime,&pclAftStoa[8],2);
        strcat(pclTmpTime,":");
        strncat(pclTmpTime,&pclAftStoa[10],2);
        strcat(pclTmpTime,":");
        strncat(pclTmpTime,&pclAftStoa[12],4);
        sprintf(pclTmpBuf,"%s%c%s%c%s%c\n",pclAftAdid,cgSeperator,pclAftFlno,cgSeperator,pclTmpTime,cgSeperator);
        strcat(pcgLogBuf,pclTmpBuf);
        fwrite(pcgLogBuf,sizeof(char),strlen(pcgLogBuf),pgLogFile);
        fflush(pgLogFile);
     }
     close_my_cursor(&slCursor);
  }

  return ilRC;
} /* End of AssignToFlight */


static int SearchArrTowInAft(char *pcpTimeStamp, char *pcpPosNam, char *pcpAftUrno)
{
  int ilRC = RC_SUCCESS;
  char pclFunc[] = "SearchArrTowInAft:";
  int ilRCdb = DB_SUCCESS;
  short slFkt;
  short slCursor;
  char pclSelection[1024];
  char pclSqlBuf[1024];
  char pclDataBuf[2048];
  char pclFieldList[1024];
  int ilRCdbDep = DB_SUCCESS;
  short slFktDep;
  short slCursorDep;
  char pclSqlBufDep[1024];
  char pclDataBufDep[2048];
  char pclTmpTimeBgn[16];
  char pclTmpTimeEnd[16];
  char pclUrno[16];
  char pclRkey[16];
  char pclFlno[16];
  char pclAdid[8];
  char pclFtyp[8];
  char pclRtyp[8];
  char pclTifa[16];
  char pclOnbl[16];
  char pclAurn[16];
  char pclOfbl[16];

  strcpy(pclTmpTimeBgn,pcpTimeStamp);
  ilRC = MyAddSecondsToCEDATime(pclTmpTimeBgn,igTimeWindowForAftArr*60*60*(-1),1);
  ilRC = MyAddSecondsToCEDATime(pclTmpTimeBgn,15*60*(-1),1);
  strcpy(pclTmpTimeEnd,pcpTimeStamp);
  ilRC = MyAddSecondsToCEDATime(pclTmpTimeEnd,15*60,1);
  strcpy(pclFieldList,"URNO,RKEY,FLNO,ADID,FTYP,RTYP,TIFA,ONBL,AURN");
  sprintf(pclSelection,"WHERE TIFA BETWEEN '%s' AND '%s' AND ADID IN ('A','B') AND PSTA IN %s",
          pclTmpTimeBgn,pclTmpTimeEnd,pcpPosNam);
  sprintf(pclSqlBuf,"SELECT %s FROM AFTTAB %s ORDER BY TIFA DESC",pclFieldList,pclSelection);
  ilRC = RC_FAIL;
  slCursor = 0;
  slFkt = START;
  dbg(DEBUG,"%s SQL = <%s>",pclFunc,pclSqlBuf);
  ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
  while (ilRCdb == DB_SUCCESS && ilRC == RC_FAIL)
  {
     igCount++;
     BuildItemBuffer(pclDataBuf,"",9,",");
     get_real_item(pclUrno,pclDataBuf,1);
     get_real_item(pclRkey,pclDataBuf,2);
     get_real_item(pclFlno,pclDataBuf,3);
     get_real_item(pclAdid,pclDataBuf,4);
     get_real_item(pclFtyp,pclDataBuf,5);
     get_real_item(pclRtyp,pclDataBuf,6);
     get_real_item(pclTifa,pclDataBuf,7);
     get_real_item(pclOnbl,pclDataBuf,8);
     get_real_item(pclAurn,pclDataBuf,9);
     if (*pclFtyp == 'T' || *pclFtyp == 'G')
        dbg(DEBUG,"%s Found Towing: URNO,RKEY,FLNO,ADID,TIFA,ONBL,OFBL,AURN",pclFunc);
     else
        dbg(DEBUG,"%s Found Flight: URNO,RKEY,FLNO,ADID,TIFA,ONBL,OFBL,AURN",pclFunc);
     if (*pclRtyp == 'J' && strlen(pclOnbl) > 0)
     {
        sprintf(pclSqlBufDep,"SELECT OFBL FROM AFTTAB WHERE RKEY = %s AND TIFD > '%s' ORDER BY TIFD",
                pclRkey,pclOnbl);
        slCursorDep = 0;
        slFktDep = START;
        /*dbg(DEBUG,"%s SQL = <%s>",pclFunc,pclSqlBufDep);*/
        ilRCdbDep = sql_if(slFktDep,&slCursorDep,pclSqlBufDep,pclDataBufDep);
        close_my_cursor(&slCursorDep);
        igCount++;
        if (ilRCdbDep != DB_SUCCESS)
           strcpy(pclOfbl,"");
        else
           get_real_item(pclOfbl,pclDataBufDep,1);
     }
     else
        strcpy(pclOfbl,"");
     if (igTolerance > 0)
     {
        MyAddSecondsToCEDATime(pclOnbl,igTolerance*60*(-1),1);
        MyAddSecondsToCEDATime(pclOfbl,igTolerance*60,1);
     }
     dbg(DEBUG,"%s <%s><%s><%s><%s><%s><%s><%s><%s>",
         pclFunc,pclUrno,pclRkey,pclFlno,pclAdid,pclTifa,pclOnbl,pclOfbl,pclAurn);
     if (strlen(pclOnbl) == 0)
        dbg(DEBUG,"%s Rejected, because it is not OnBlock",pclFunc);
     else
     {
        if (strcmp(pcpTimeStamp,pclOnbl) < 0)
           dbg(DEBUG,"%s Rejected, because OnBlock is after Time of Event",pclFunc);
        else
        {
           if (strlen(pclOfbl) > 0 && strcmp(pcpTimeStamp,pclOfbl) > 0)
              dbg(DEBUG,"%s Rejected, because OffBlock is before Time of Event, even if OnBlock fits",pclFunc);
           else
           {
              ilRC = RC_SUCCESS;
              dbg(DEBUG,"%s Accepted.",pclFunc);
              if (*pclFtyp == 'T' || *pclFtyp == 'G')
                 strcpy(pcpAftUrno,pclAurn);
              else
                 strcpy(pcpAftUrno,pclUrno);
           }
        }
     }
     slFkt = NEXT;
     ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
  }
  close_my_cursor(&slCursor);

  return ilRC;
} /* End of SearchArrTowInAft */


static int SearchInFog(char *pcpTimeStamp, char *pcpPosNam, char *pcpAftUrno)
{
  int ilRC = RC_SUCCESS;
  char pclFunc[] = "SearchInFog:";
  int ilRCdb = DB_SUCCESS;
  short slFkt;
  short slCursor;
  char pclSelection[1024];
  char pclSqlBuf[1024];
  char pclDataBuf[2048];
  char pclFieldList[1024];
  int ilRCdbAft = DB_SUCCESS;
  short slFktAft;
  short slCursorAft;
  char pclSelectionAft[1024];
  char pclSqlBufAft[1024];
  char pclDataBufAft[2048];
  char pclFieldListAft[1024];
  int ilRCdbDep = DB_SUCCESS;
  short slFktDep;
  short slCursorDep;
  char pclSelectionDep[1024];
  char pclSqlBufDep[1024];
  char pclDataBufDep[2048];
  char pclFlnu[16];
  char pclOgbs[16];
  char pclOges[16];
  char pclFtyp[8];
  char pclStat[8];
  char pclPosn[16];
  char pclRkey[16];
  char pclRtyp[8];
  char pclUrno[16];
  char pclFlno[16];
  char pclAdid[16];
  char pclTifa[16];
  char pclOnbl[16];
  char pclAurn[16];
  char pclOfbl[16];
  char pclTime1[16];
  char pclTime2[16];

  strcpy(pclTime1,pcpTimeStamp);
  strcpy(pclTime2,pcpTimeStamp);
  if (igTolerance > 0)
  {
     MyAddSecondsToCEDATime(pclTime1,igTolerance*60,1);
     MyAddSecondsToCEDATime(pclTime2,igTolerance*60*(-1),1);
  }
  strcpy(pclFieldList,"FLNU,OGBS,OGES,FTYP,STAT,POSN,RKEY,RTYP");
  sprintf(pclSelection,"WHERE OGBS <= '%s' AND OGES >= '%s' AND STAT IN ('O','C') AND (RTYP = 'J' OR (RTYP = 'S' AND POSN IN %s))",
          pclTime1,pclTime2,pcpPosNam);
  sprintf(pclSqlBuf,"SELECT %s FROM FOVTAB %s ORDER BY RTYP DESC",pclFieldList,pclSelection);
  ilRC = RC_FAIL;
  slCursor = 0;
  slFkt = START;
  dbg(DEBUG,"%s SQL = <%s>",pclFunc,pclSqlBuf);
  ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
  while (ilRCdb == DB_SUCCESS && ilRC == RC_FAIL)
  {
     igCount++;
     BuildItemBuffer(pclDataBuf,"",8,",");
     get_real_item(pclFlnu,pclDataBuf,1);
     get_real_item(pclOgbs,pclDataBuf,2);
     get_real_item(pclOges,pclDataBuf,3);
     get_real_item(pclFtyp,pclDataBuf,4);
     get_real_item(pclStat,pclDataBuf,5);
     get_real_item(pclPosn,pclDataBuf,6);
     get_real_item(pclRkey,pclDataBuf,7);
     get_real_item(pclRtyp,pclDataBuf,8);
     dbg(DEBUG,"%s Found FOG Record: FLNU,OGBS,OGES,FTYP,STAT,POSN,RKEY,RTYP",pclFunc);
     dbg(DEBUG,"%s <%s><%s><%s><%s><%s><%s><%s><%s>",
         pclFunc,pclFlnu,pclOgbs,pclOges,pclFtyp,pclStat,pclPosn,pclRkey,pclRtyp);
     if (*pclRtyp == 'S')
     {
        ilRC = RC_SUCCESS;
        dbg(DEBUG,"%s Accepted.",pclFunc);
        strcpy(pcpAftUrno,pclFlnu);
     }
     else
     {
        strcpy(pclTime1,pcpTimeStamp);
        if (igTolerance > 0)
           MyAddSecondsToCEDATime(pclTime1,igTolerance*60*(-1),1);
        strcpy(pclFieldListAft,"URNO,FLNO,ADID,FTYP,TIFA,ONBL,AURN");
        sprintf(pclSelectionAft,"WHERE RKEY = %s AND PSTA IN %s AND ONBL <> ' ' AND ONBL <= '%s'",
                pclRkey,pcpPosNam,pclTime1);
        sprintf(pclSqlBufAft,"SELECT %s FROM AFTTAB %s ORDER BY TIFA DESC",pclFieldListAft,pclSelectionAft);
        slCursorAft = 0;
        slFktAft = START;
        dbg(DEBUG,"%s SQL = <%s>",pclFunc,pclSqlBufAft);
        ilRCdbAft = sql_if(slFktAft,&slCursorAft,pclSqlBufAft,pclDataBufAft);
        while (ilRCdbAft == DB_SUCCESS && ilRC == RC_FAIL)
        {
           igCount++;
           BuildItemBuffer(pclDataBufAft,"",7,",");
           get_real_item(pclUrno,pclDataBufAft,1);
           get_real_item(pclFlno,pclDataBufAft,2);
           get_real_item(pclAdid,pclDataBufAft,3);
           get_real_item(pclFtyp,pclDataBufAft,4);
           get_real_item(pclTifa,pclDataBufAft,5);
           get_real_item(pclOnbl,pclDataBufAft,6);
           get_real_item(pclAurn,pclDataBufAft,7);
           sprintf(pclSqlBufDep,"SELECT OFBL FROM AFTTAB WHERE RKEY = %s AND TIFD > '%s' ORDER BY TIFD",
                   pclRkey,pclOnbl);
           slCursorDep = 0;
           slFktDep = START;
           /*dbg(DEBUG,"%s SQL = <%s>",pclFunc,pclSqlBufDep);*/
           ilRCdbDep = sql_if(slFktDep,&slCursorDep,pclSqlBufDep,pclDataBufDep);
           close_my_cursor(&slCursorDep);
           igCount++;
           if (ilRCdbDep != DB_SUCCESS)
              strcpy(pclOfbl,"");
           else
              get_real_item(pclOfbl,pclDataBufDep,1);
           if (igTolerance > 0)
              MyAddSecondsToCEDATime(pclOfbl,igTolerance*60,1);
           dbg(DEBUG,"%s Found Flight: URNO,FLNO,ADID,TIFA,ONBL,OFBL,AURN",pclFunc);
           dbg(DEBUG,"%s <%s><%s><%s><%s><%s><%s><%s><%s>",
               pclFunc,pclUrno,pclFlno,pclAdid,pclTifa,pclOnbl,pclOfbl,pclAurn);
           if (strlen(pclOfbl) > 0 && strcmp(pcpTimeStamp,pclOfbl) > 0)
              dbg(DEBUG,"%s Rejected, because OffBlock is before Time of Event, even if OnBlock fits",pclFunc);
           else
           {
              ilRC = RC_SUCCESS;
              dbg(DEBUG,"%s Accepted.",pclFunc);
              if (*pclFtyp == 'T' || *pclFtyp == 'G')
                 strcpy(pcpAftUrno,pclAurn);
              else
                 strcpy(pcpAftUrno,pclUrno);
           }
           slFktAft = NEXT;
           ilRCdbAft = sql_if(slFktAft,&slCursorAft,pclSqlBufAft,pclDataBufAft);
        }
        close_my_cursor(&slCursorAft);
     }
     slFkt = NEXT;
     ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
  }
  close_my_cursor(&slCursor);

  return ilRC;
} /* End of SearchInFog */


static int SearchDepInAft(char *pcpTimeStamp, char *pcpPosNam, char *pcpAftUrno)
{
  int ilRC = RC_SUCCESS;
  char pclFunc[] = "SearchDepInAft:";
  int ilRCdb = DB_SUCCESS;
  short slFkt;
  short slCursor;
  char pclSelection[1024];
  char pclSqlBuf[1024];
  char pclDataBuf[2048];
  char pclFieldList[1024];
  char pclTmpTime[16];
  char pclUrno[16];
  char pclRkey[16];
  char pclFlno[16];
  char pclAdid[8];
  char pclFtyp[8];
  char pclRtyp[8];
  char pclTifd[16];
  char pclOfbl[16];
  char pclAurn[16];
  char pclTime1[16];

  strcpy(pclTime1,pcpTimeStamp);
  if (igTolerance > 0)
     MyAddSecondsToCEDATime(pclTime1,igTolerance*60*(-1),1);
  strcpy(pclTmpTime,pcpTimeStamp);
  ilRC = MyAddSecondsToCEDATime(pclTmpTime,igTimeWindowForAftDep*60,1);
  strcpy(pclFieldList,"URNO,RKEY,FLNO,ADID,FTYP,RTYP,TIFD,OFBL,AURN");
  sprintf(pclSelection,"WHERE TIFD BETWEEN '%s' AND '%s' AND ADID = 'D' AND PSTD IN %s",
          pclTime1,pclTmpTime,pcpPosNam);
  sprintf(pclSqlBuf,"SELECT %s FROM AFTTAB %s ORDER BY TIFD",pclFieldList,pclSelection);
  ilRC = RC_FAIL;
  slCursor = 0;
  slFkt = START;
  dbg(DEBUG,"%s SQL = <%s>",pclFunc,pclSqlBuf);
  ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
  if (ilRCdb == DB_SUCCESS)
  {
     igCount++;
     BuildItemBuffer(pclDataBuf,"",9,",");
     get_real_item(pclUrno,pclDataBuf,1);
     get_real_item(pclRkey,pclDataBuf,2);
     get_real_item(pclFlno,pclDataBuf,3);
     get_real_item(pclAdid,pclDataBuf,4);
     get_real_item(pclFtyp,pclDataBuf,5);
     get_real_item(pclRtyp,pclDataBuf,6);
     get_real_item(pclTifd,pclDataBuf,7);
     get_real_item(pclOfbl,pclDataBuf,8);
     get_real_item(pclAurn,pclDataBuf,9);
     dbg(DEBUG,"%s Found Flight: URNO,RKEY,FLNO,ADID,TIFD,OFBL,AURN",pclFunc);
     dbg(DEBUG,"%s <%s><%s><%s><%s><%s><%s><%s>",
         pclFunc,pclUrno,pclRkey,pclFlno,pclAdid,pclTifd,pclOfbl,pclAurn);
     ilRC = RC_SUCCESS;
     dbg(DEBUG,"%s Accepted.",pclFunc);
     strcpy(pcpAftUrno,pclUrno);
  }
  close_my_cursor(&slCursor);

  return ilRC;
} /* End of SearchDepInAft */


static int GetLoaAndEqiData(char *pcpUrno, char *pcpFields)
{
  int ilRC = RC_SUCCESS;
  char pclFunc[] = "GetLoaAndEqiData:";
  int ilRCdb = DB_SUCCESS;
  short slFkt;
  short slCursor;
  char pclSelection[1024];
  char pclSqlBuf[1024];
  char pclDataBuf[2048];
  char pclFieldList[1024];
  char pclUrno[16];
  char pclDssn[16];
  char pclType[16];
  char pclStyp[16];
  char pclSstp[16];
  char pclSsst[16];
  char pclValu[16];
  int ilIdx;
  int ilI;
  char pclAftUrno[128];
  char pclFields[2048];
  char pclData[4096];
  int ilNoEle;
  int ilJ;
  int ilFound;
  char pclFldNam[16];
  int ilK;

  if (strlen(pcpUrno) == 0)
     ilRC = iGetConfigEntry(pcgConfigFile,"FLDDESCRLOA","FLNU",CFG_STRING,pclAftUrno);
  else
     strcpy(pclAftUrno,pcpUrno);
  strcpy(pclFieldList,"URNO,DSSN,TYPE,STYP,SSTP,SSST,VALU");
  sprintf(pclSelection,"WHERE FLNU = %s AND %s",pclAftUrno,pcgLoaSelect);
  sprintf(pclSqlBuf,"SELECT %s FROM LOATAB %s",pclFieldList,pclSelection);
  slCursor = 0;
  slFkt = START;
  dbg(DEBUG,"%s SQL = <%s>",pclFunc,pclSqlBuf);
  ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
  while (ilRCdb == DB_SUCCESS)
  {
     BuildItemBuffer(pclDataBuf,"",7,",");
     get_real_item(pclUrno,pclDataBuf,1);
     get_real_item(pclDssn,pclDataBuf,2);
     get_real_item(pclType,pclDataBuf,3);
     get_real_item(pclStyp,pclDataBuf,4);
     get_real_item(pclSstp,pclDataBuf,5);
     get_real_item(pclSsst,pclDataBuf,6);
     get_real_item(pclValu,pclDataBuf,7);
     if (*pclType == '\0')
        strcpy(pclType," ");
     if (*pclStyp == '\0')
        strcpy(pclStyp," ");
     if (*pclSstp == '\0')
        strcpy(pclSstp," ");
     if (*pclSsst == '\0')
        strcpy(pclSsst," ");
     ilIdx = -1;
     for (ilI = 0; ilI < igNoFieldsLoa && ilIdx < 0; ilI++)
     {
        if (strcmp(&rgFieldsLoa[ilI].pclLoaDssn[0],pclDssn) == 0 &&
            strcmp(&rgFieldsLoa[ilI].pclLoaType[0],pclType) == 0 &&
            strcmp(&rgFieldsLoa[ilI].pclLoaStyp[0],pclStyp) == 0 &&
            strcmp(&rgFieldsLoa[ilI].pclLoaSstp[0],pclSstp) == 0 &&
            strcmp(&rgFieldsLoa[ilI].pclLoaSsst[0],pclSsst) == 0)
           ilIdx = ilI;
     }
     if (ilIdx >= 0)
     {
        strcpy(&rgFieldsLoa[ilIdx].pclLoaValu[0],pclValu);
        dbg(TRACE,"%s Found <%s><%s><%s><%s><%s><%s><%s> for Field <%s=%s>",
            pclFunc,pclUrno,pclDssn,pclType,pclStyp,pclSstp,pclSsst,pclValu,
            &rgFieldsLoa[ilIdx].pclFldNam[0],&rgFieldsLoa[ilIdx].pclLoaValu[0]);
     }
     else
     {
        dbg(TRACE,"%s No Field Found for <%s><%s><%s><%s><%s><%s><%s>",
            pclFunc,pclUrno,pclDssn,pclType,pclStyp,pclSstp,pclSsst,pclValu);
     }
     slFkt = NEXT;
     ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
  }
  close_my_cursor(&slCursor);

  ilRC = GetEqiData(pclAftUrno,"GPATAB","GPU",pclValu);
  ilRC = GetEqiData(pclAftUrno,"PLBTAB","PLB",pclValu);
  ilRC = GetEqiData(pclAftUrno,"PCATAB","PCA",pclValu);

  if (strlen(pcpFields) == 0)
  {
     ilRC = iGetConfigEntry(pcgConfigFile,"FLDDESCRLOA","FIELDS",CFG_STRING,pclFields);
     strcat(pclFields,",");
     ilRC = iGetConfigEntry(pcgConfigFile,"FLDDESCREQI","FIELDS",CFG_STRING,pclData);
     strcat(pclFields,pclData);
  }
  else
     strcpy(pclFields,pcpFields);
  strcpy(pclData,"");
  dbg(DEBUG,"%s Flds = <%s>",pclFunc,pclFields);
  ilNoEle = GetNoOfElements(pclFields,',');
  for (ilI = 1; ilI <= ilNoEle; ilI++)
  {
     get_real_item(pclFldNam,pclFields,ilI);
     ilFound = FALSE;
     for (ilJ = 0; ilJ < igNoFieldsLoa && ilFound == FALSE; ilJ++)
     {
        if (strcmp(&rgFieldsLoa[ilJ].pclFldNam[0],pclFldNam) == 0)
        {
           strcat(pclData,&rgFieldsLoa[ilJ].pclLoaValu[0]);
           strcat(pclData,",");
           ilFound = TRUE;
        }
     }
     for (ilJ = 0; ilJ < igNoFieldsEqi && ilFound == FALSE; ilJ++)
     {
        if (strcmp(&rgFieldsEqi[ilJ].pclFldNam[0],pclFldNam) == 0)
        {
           strcat(pclData,&rgFieldsEqi[ilJ].pclEqiValu[0]);
           for (ilK = 0; ilK < 3; ilK++)
           {
              strcat(pclData,&rgFieldsEqi[ilJ].pclBgn[ilK][0]);
              strcat(pclData,&rgFieldsEqi[ilJ].pclEnd[ilK][0]);
           }
           if (rgFieldsEqi[ilJ].pclEqiValu[0] == '0')
              TrimRight(pclData);
           strcat(pclData,",");
           ilFound = TRUE;
        }
     }
     if (ilFound == FALSE)
        strcat(pclData,"0,");
  }
  if (strlen(pclData) > 0)
  {
     pclData[strlen(pclData)-1] = '\0';
     for (ilI = 0; ilI < strlen(pclData); ilI++)
     {
        if (pclData[ilI] == ' ')
           pclData[ilI] = '@';
     }
  }
  dbg(DEBUG,"%s Data = <%s>",pclFunc,pclData);
  if (igSendResponse == TRUE)
  {
     dbg(DEBUG,"%s Send Answer to ModId = %d",pclFunc,que_out);
     ilRC = SendCedaEvent(que_out,0,mod_name,"CEDA",pcgTwStart,pcgTwEnd,
                          "GLED","",pclAftUrno,pclFields,pclData,"",3,NETOUT_NO_ACK);
  }

  for (ilI = 0; ilI < igNoFieldsLoa; ilI++)
     strcpy(&rgFieldsLoa[ilI].pclLoaValu[0],"");
  for (ilI = 0; ilI < igNoFieldsEqi; ilI++)
     strcpy(&rgFieldsEqi[ilI].pclEqiValu[0],"");

  return ilRC;
} /* End of GetLoaAndEqiData */


static int GetEqiData(char *pcpUrno, char *pcpTable, char *pcpEqip, char *pcpValue)
{
  int ilRC = RC_SUCCESS;
  char pclFunc[] = "GetEqiData:";
  int ilRCdb = DB_SUCCESS;
  short slFkt;
  short slCursor;
  char pclSelection[1024];
  char pclSqlBuf[1024];
  char pclDataBuf[2048];
  char pclFieldList[1024];
  char pclGaab[16];
  char pclGaae[16];
  char pclAseq[16];
  long llTimeFrom;
  long llTimeTo;
  long llTimeDiff;
  long llDuration;
  int ilI;
  int ilIdx;

  ilIdx = -1;
  for (ilI = 0; ilI < igNoFieldsEqi && ilIdx < 0; ilI++)
  {
     if (strcmp(&rgFieldsEqi[ilI].pclTabNam[0],pcpTable) == 0)
        ilIdx = ilI;
  }
  if (ilIdx < 0)
  {
     dbg(TRACE,"%s No %s Usage defined",pclFunc,pcpEqip);
     return ilRC;
  }
  
  strcpy(&rgFieldsEqi[ilIdx].pclEqiValu[0],"   ");
  for (ilI = 0; ilI < MAX_EQI_TIMES; ilI++)
  {
     strcpy(&rgFieldsEqi[ilIdx].pclBgn[ilI][0],"              ");
     strcpy(&rgFieldsEqi[ilIdx].pclEnd[ilI][0],"              ");
  }

  llDuration = 0; 
  ilI = 0;
  strcpy(pclFieldList,"GAAB,GAAE,ASEQ");
  sprintf(pclSelection,"WHERE FLNU = %s",pcpUrno);
  sprintf(pclSqlBuf,"SELECT %s FROM %s %s ORDER BY GAAB",pclFieldList,pcpTable,pclSelection);
  slCursor = 0;
  slFkt = START;
  dbg(DEBUG,"%s SQL = <%s>",pclFunc,pclSqlBuf);
  ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
  while (ilRCdb == DB_SUCCESS)
  {
     BuildItemBuffer(pclDataBuf,"",3,",");
     get_real_item(pclGaab,pclDataBuf,1);
     get_real_item(pclGaae,pclDataBuf,2);
     get_real_item(pclAseq,pclDataBuf,3);
     if (strlen(pclGaab) == 0 || strlen(pclGaae) == 0 || strcmp(pclGaab,pclGaae) > 0)
     {
        dbg(TRACE,"%s Invalid or incomplete Times <%s><%s> discarded",pclFunc,pclGaab,pclGaae);
     }
     else
     {
        llTimeFrom = GetSecondsFromCEDATime(pclGaab);
        llTimeTo = GetSecondsFromCEDATime(pclGaae);
        llTimeDiff = llTimeTo - llTimeFrom;
        if (llTimeDiff/60 >= rgFieldsEqi[ilIdx].llMinTime)
        {
           ilI = atoi(pclAseq) - 1;
           llDuration += llTimeDiff;
           strcpy(&rgFieldsEqi[ilIdx].pclBgn[ilI][0],pclGaab);
           strcpy(&rgFieldsEqi[ilIdx].pclEnd[ilI][0],pclGaae);
           ilI++;
        }
        else
        {
           dbg(TRACE,"%s Actual Usage is %ld mins.",pclFunc,llTimeDiff/60);
           dbg(TRACE,"%s Minimum Usage is %ld mins.",pclFunc,rgFieldsEqi[ilIdx].llMinTime);
           dbg(TRACE,"%s Usage is therefore discarded.",pclFunc);
        }
     }
     slFkt = NEXT;
     ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
  }
  close_my_cursor(&slCursor);
  if (llDuration/60 < 10)
     sprintf(pcpValue,"%ld  ",llDuration/60);
  else if (llDuration/60 < 100)
     sprintf(pcpValue,"%ld ",llDuration/60);
  else
     sprintf(pcpValue,"%ld",llDuration/60);
  strcpy(&rgFieldsEqi[ilIdx].pclEqiValu[0],pcpValue);
  dbg(TRACE,"%s Found %s Usage for Field <%s=%s>",
      pclFunc,pcpEqip,&rgFieldsEqi[ilIdx].pclFldNam[0],&rgFieldsEqi[ilIdx].pclEqiValu[0]);

  return ilRC;
} /* End of GetEqiData */


