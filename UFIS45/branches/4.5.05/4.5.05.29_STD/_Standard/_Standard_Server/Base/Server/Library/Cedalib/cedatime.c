#ifndef _DEF_mks_version_cedatime_c
  #define _DEF_mks_version_cedatime_c
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_cedatime_c[]   MARK_UNUSED = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Library/Cedalib/cedatime.c 1.4 2005/11/08 21:53:51SGT akl Exp  $";
#endif /* _DEF_mks_version */
#include "cedatime.h"
#include "libccstr.h"


extern char *strptime(const char *, const char *, struct tm *);
extern long int timezone;
extern int  daylight;
extern char *tzname[];

static void BigGermanMonthToEnglishConvert(char *pcpBuffer); 

#define CHNG_CENTURY 70   /* years > 70 are considered 19yy
                             other years are 20yy              */

/*-----------------------------------------------------------------------------
 * Converts string to uppercase
 *---------------------------------------------------------------------------
**/
static char *strupr(char *s)
{
char *p;

  p=s;
  while(*p)
  {
    *p=(char)toupper(*p); 
    p++;
  }
  return (s);
}

/*-----------------------------------------------------------------------------
 * Converts string to lowercase
 *---------------------------------------------------------------------------
**/
static char *strlwr(char *s)
{
char *p;

  p=s;
  while(*p)
  {
   *p=(char)tolower(*p); 
   p++;
  }
  return (s);
}


/*********************************************************************
Function : GetServerTimeStamp() 
Paramter : IN: pcpType = type of timestamp (local time = "LOC", GMT-time
                         = "UTC")
					 IN: ipFormat = number of desired format.
					 IN: lpTimeDiff = possible time difference (+/-) which should be
					                  added or substracted from the timestamp.
					 OUT: pcpTimeStamp = result buffer for the timestamp.
Return Code: none
Result:
Description: returns a timestamp with the desired format in a string buffer
             Current formats are:
						 1 = CEDA-datetimestamp (YYYYMMDDHHMISS)
						 2 = CEDA-datestamp (YYYYMMDD)
						 3 = CEDA-timestamp (HHMISS)
*********************************************************************/
void GetServerTimeStamp(char *pcpType, int ipFormat, long lpTimeDiff,
			char *pcpTimeStamp)
{
  char pclTime[64];
  int ilPos = 0;
  struct tm *_tm = NULL;
  struct tm rlTm;
  time_t tlStamp = 0;

  if (ipFormat <= 100)
  {
    pclTime[0] = 0x00;
    pcpTimeStamp[0] = 0x00;
    tlStamp = (time_t)(time(NULL)) + (time_t)lpTimeDiff;
    if (strcmp(pcpType,"UTC")==0)
    {
      _tm = (struct tm *) gmtime(&tlStamp);
    }
    else
    {
      if (strcmp(pcpType,"LOC")==0)
      {
        _tm = (struct tm *) localtime(&tlStamp);
      }
      else
      {
        dbg(TRACE,"GetServerTimeStamp: Time-type <%s> not supported!",pcpType);
      }
    }

    if (_tm != NULL)
    {
      rlTm = *_tm;
      if (tlStamp >= 0)
      {
        switch(ipFormat)
        {
          case 1: /* CEDA-format date+time */
            strftime(pcpTimeStamp,15,"%" "Y%" "m%" "d%" "H%" "M%" "S",&rlTm);
          break;
          case 2: /* CEDA-format date */
            strftime(pcpTimeStamp,9,"%" "Y%" "m%" "d",&rlTm);
          break;
          case 3: /* CEDA-format time */
            strftime(pcpTimeStamp,7,"%" "H%" "M%" "S",&rlTm);
          break;
          default:
            dbg(TRACE,"GetServerTimeStamp: unknown format <%d>!",ipFormat);
          break;
        }
      }
    }
  } /* end if */
  else if (ipFormat <= 200)
  {
    switch(ipFormat)
    {
      case 101: /* CEDA-format to German date/time */
        strcpy(pclTime, pcpTimeStamp);
        strcpy(pcpTimeStamp," ");
	if (strlen(pclTime) >= 12)
	{
	  StrgPutStrg(pcpTimeStamp,&ilPos,pclTime,6,7,".");
	  StrgPutStrg(pcpTimeStamp,&ilPos,pclTime,4,5,".");
	  StrgPutStrg(pcpTimeStamp,&ilPos,pclTime,0,3,"/");
	  StrgPutStrg(pcpTimeStamp,&ilPos,pclTime,8,9,":");
	  StrgPutStrg(pcpTimeStamp,&ilPos,pclTime,10,11,"\0");
	  pcpTimeStamp[ilPos] = 0x00;
        } /* end if */
      break;
      case 102: /* German date/time to CEDA-format */
        strcpy(pclTime, pcpTimeStamp);
        strcpy(pcpTimeStamp," ");
	if (strlen(pclTime) >= 16)
	{
	  StrgPutStrg(pcpTimeStamp,&ilPos,pclTime,6,9,"\0");
	  StrgPutStrg(pcpTimeStamp,&ilPos,pclTime,3,4,"\0");
	  StrgPutStrg(pcpTimeStamp,&ilPos,pclTime,0,1,"\0");
	  StrgPutStrg(pcpTimeStamp,&ilPos,pclTime,11,12,"\0");
	  StrgPutStrg(pcpTimeStamp,&ilPos,pclTime,14,15,"00");
	  pcpTimeStamp[ilPos] = 0x00;
        } /* end if */
      break;
      default:
        dbg(TRACE,"GetServerTimeStamp: unknown format <%d>!",ipFormat);
      break;
    } /* end switch */
  } /* end else if */
  else
  {
    dbg(TRACE,"GetServerTimeStamp: unknown format <%d>!",ipFormat);
  } /* end else  */
}


/* *******************************************************/      
/* BST */
/* *******************************************************/      
int CheckTimeOut(int ipType, char *pcpTimVal)
{
  int ilRC = RC_SUCCESS;
  static time_t tlActSeconds = 0;
  static time_t tlMaxSeconds = 0;
  static int ilQueWait = 0;
  int ilBgnSecStamp = 0;
  int ilRcvSecStamp = 0;
  int ilMaxSecStamp = 0;
  int ilRcvSecTimOut = 0;
  int ilMaxSecTimOut = 0;
  char pclRcvSecs[16];
  char pclRcvTimOut[16];

  tlActSeconds = time(0L);
  switch (ipType)
  {
    case TIMO_INIT:
      ilBgnSecStamp = tlActSeconds % 10000;
      if ((pcpTimVal[0] < '0') || (pcpTimVal[0] > '9'))
      {
        sprintf(pcpTimVal,"%d,0",ilBgnSecStamp);
      } /* end if */
      /* dbg(DEBUG,"SENDER'S QUE TIME AND WAIT <%s>",pcpTimVal); */

      (void) get_real_item(pclRcvSecs,pcpTimVal,1);
      (void) get_real_item(pclRcvTimOut,pcpTimVal,2);

      ilRcvSecStamp = atoi(pclRcvSecs);
      ilRcvSecTimOut = atoi(pclRcvTimOut);
      if (ilRcvSecStamp > ilBgnSecStamp)
      {
        ilRcvSecStamp -= 10000;
        sprintf(pcpTimVal,"%d,%d",ilRcvSecStamp,ilRcvSecTimOut);
      } /* end if */
      if (ilRcvSecTimOut <= 0)
      {
        ilRcvSecTimOut = 100000;
      } /* end if */
      ilQueWait = ilBgnSecStamp - ilRcvSecStamp;
      ilMaxSecStamp = ilRcvSecStamp + ilRcvSecTimOut;
      ilMaxSecTimOut = ilMaxSecStamp - ilBgnSecStamp;
      /* dbg(DEBUG,"REMAINING TIME %d SEC",ilMaxSecTimOut); */
      tlMaxSeconds = tlActSeconds + ilMaxSecTimOut;
      break;
    case TIMO_CHECK:
      if (tlActSeconds > tlMaxSeconds)
      {
        dbg(TRACE,"WARNING: SENDER'S TIMEOUT REACHED");
        ilRC = RC_FAIL;
      } /* end if */
      break;
    case TIMO_VALUE:
      ilRC = tlMaxSeconds - tlActSeconds;
      break;
    case TIMO_QUEWAIT:
      ilRC = ilQueWait;
      break;
    default:
      break;
  } /* end switch */
  return ilRC;
} /* end CheckTimeOut */

/********************************************************/                      
/********************************************************/                      
int CheckServerTime(void)
{
  int ilUtcTimeDiff = 0;
  char pclLocTime[16];
  char pclUtcTime[16];
  int ilLocMin = 0;
  int ilUtcMin = 0;
  int ilDummy = 0;

  GetServerTimeStamp("LOC",1,0,pclLocTime);
  GetServerTimeStamp("UTC",1,0,pclUtcTime);
  dbg(TRACE,"SYSTEM: LOCAL <%s> UTC <%s> ",pclLocTime,pclUtcTime);
  (void) GetFullDay(pclLocTime, &ilDummy,&ilLocMin,&ilDummy);
  (void) GetFullDay(pclUtcTime, &ilDummy,&ilUtcMin,&ilDummy);
  ilUtcTimeDiff = ilLocMin - ilUtcMin;
  dbg(TRACE,"SYSTEM: LOCAL - UTC = %d MIN",ilUtcTimeDiff);
  return ilUtcTimeDiff;
} /* end CheckServerTime */


#if 0
/* TWE */
/*********************************************************************
Function   : AddSecondsToCEDATime 
Paramter   : IN/OUT: pcpCEDATime  = String in CEDATimeformat where to add
             IN    : tpSeconds    = Number of seconds (+/-)
					   IN    : ipFormat     = Number of desired format
                                    (1 = CEDATimeformat)
ReturnCode : RC_SUCCESS, RC_FAIL 
Description: returns a timestamp with the desired format in a string buffer
             Current formats are:
						 1 = CEDA-timestamp (YYYYMMDDHHMISS)
*********************************************************************/
int AddSecondsToCEDATime (char *pcpCEDATime, time_t tpSeconds, int ipFormat)
{
int        ilRC          = RC_SUCCESS;
int        ilSummerFlag  = 0;
int        ilNumHundred  = 0;
long       llCnt         = 0;
long       llLen         = 0;
time_t     llSecVal      = 0;
char       pclBuf[8];
char       clNum;
struct tm  rlTimeStruct;
struct tm  rlTimeStruct2;
struct tm  *prlTimeStruct;

  /**** CHECK pcpCEDATime  ****/
  if (pcpCEDATime == NULL)                               /* Check if not NULL */
  {
     dbg(TRACE, "AddSecondsToCEDATime: No Time to add to (NULL)");
     ilRC = RC_FAIL;
  }
  else
  {
     llLen = strlen(pcpCEDATime);
     if (llLen != 14)                         /* Check for CEDA-Format Length */
     {
        dbg(TRACE, "AddSecondsToCEDATime: <%s> is no valid CEDATIME (14 Signs)",
                    pcpCEDATime);
        ilRC = RC_FAIL;
     }
     else
     {
        for (llCnt = 0; (llCnt < llLen) && (ilRC == RC_SUCCESS); llCnt++)
        {
           clNum = pcpCEDATime[llCnt];
           if (!(isdigit(clNum)))              /* Check if only numbers exist */
           {
              ilRC = RC_FAIL;
           }
        }
     }
  }

  /***  Action part ***/
  if (ilRC == RC_SUCCESS)
  {
     /* fill timestruct from CEDATIME */
     prlTimeStruct = &rlTimeStruct;

     memset(pclBuf,0x00,8);
/* year 2000 problem */
     memcpy(pclBuf,&pcpCEDATime[0],2);
     ilNumHundred = atoi(pclBuf) - 19;
     memset(pclBuf,0x00,8);
     memcpy(pclBuf,&pcpCEDATime[2],2);
     prlTimeStruct->tm_year = atoi(pclBuf) + 100*ilNumHundred;
/******/
     memset(pclBuf,0x00,8);
     memcpy(pclBuf,&pcpCEDATime[4],2);
     prlTimeStruct->tm_mon  = atoi(pclBuf) - 1;
     memset(pclBuf,0x00,8);
     memcpy(pclBuf,&pcpCEDATime[6],2);
     prlTimeStruct->tm_mday = atoi(pclBuf);
     memset(pclBuf,0x00,8);   
     memset(pclBuf,0x00,8);
     memcpy(pclBuf,&pcpCEDATime[8],2);
     prlTimeStruct->tm_hour = atoi(pclBuf);
     memset(pclBuf,0x00,8);
     memcpy(pclBuf,&pcpCEDATime[10],2);
     prlTimeStruct->tm_min  = atoi(pclBuf);
     memset(pclBuf,0x00,8);
     memcpy(pclBuf,&pcpCEDATime[12],2);
     prlTimeStruct->tm_sec  = atoi(pclBuf);
     prlTimeStruct->tm_isdst = 0;
     prlTimeStruct->tm_yday  = 0;
     prlTimeStruct->tm_wday  = 0;

     /* get seconds from CEDATime */
     llSecVal = mktime (prlTimeStruct);
     if (llSecVal == -1)
     {
        dbg(TRACE,"AddSecondsToCEDATime: mktime of pcpCEDATime failed");
        ilRC = RC_FAIL;
        strcpy(pcpCEDATime, " ");
     }
     else
     {
        if (prlTimeStruct->tm_isdst == 1)   /* summertime not inserted before */
        {
           /* 1 hour back because struct was changed by mktime) */
           llSecVal -= 3600;
        }
        llSecVal += tpSeconds;                               /* add seconds */
     }
  }
   
  if (ilRC == RC_SUCCESS)
  {
     /* init timestruct */
     prlTimeStruct->tm_year = 0;
     prlTimeStruct->tm_mon  = 0;
     prlTimeStruct->tm_mday = 0;
     prlTimeStruct->tm_hour = 0;
     prlTimeStruct->tm_min  = 0;
     prlTimeStruct->tm_sec  = 0;
     prlTimeStruct->tm_isdst = 0;
     prlTimeStruct->tm_yday  = 0;
     prlTimeStruct->tm_wday  = 0;
     strcpy(pcpCEDATime, " ");

     /* get struct with new time */
     prlTimeStruct = localtime(&llSecVal);  
     rlTimeStruct2 = *prlTimeStruct;

     /* get CEDATIME from timestruct */
     if (ilRC == RC_SUCCESS)
     {
			  switch(ipFormat)
			  {
				  case 1: /* CEDA-format */
					   strftime(pcpCEDATime,15,
                      "%" "Y%" "m%" "d%" "H%" "M%" "S", &rlTimeStruct2);
					   break;
				  default:
					   dbg(TRACE,"AddSecondsToCEDATime: unknown format <%d>!",ipFormat);
        }
     }
  }

  return ilRC;

} /* end of AddSecondsToCEDATime */
#endif


/* TWE */
/*********************************************************************
Function   : AddSecondsToCEDATime 
Paramter   : IN/OUT: pcpCEDATime  = String in CEDATimeformat where to add
             IN    : tpSeconds    = Number of seconds (+/-)
             IN    : ipFormat     = Number of desired format
                                    (1 = CEDATimeformat)
ReturnCode : RC_SUCCESS, RC_FAIL 
Description: returns a timestamp with the desired format in a string buffer
             Current formats are:
             1 = CEDA-timestamp (YYYYMMDDHHMISS)
*********************************************************************/
int AddSecondsToCEDATime (char *pcpCEDATime, time_t tpSeconds, int ipFormat)
{
int        ilRC          = RC_SUCCESS;
int        ilSummerFlag  = 0;
int        ilNumHundred  = 0;
long       llCnt         = 0;
long       llLen         = 0;
time_t     llSecVal      = 0;
char       pclBuf[8];
char       clNum;
struct tm  rlTimeStruct;
struct tm  rlTimeStruct2;
struct tm  *prlTimeStruct;
int        ilIsDst1;
int        ilIsDst2;

  /**** CHECK pcpCEDATime  ****/
  if (pcpCEDATime == NULL) /* Check if not NULL */
  {
     dbg(TRACE, "AddSecondsToCEDATime: No Time to add to (NULL)");
     ilRC = RC_FAIL;
  }
  else
  {
     llLen = strlen(pcpCEDATime);
     if (llLen != 14) /* Check for CEDA-Format Length */
     {
        dbg(TRACE,"AddSecondsToCEDATime: <%s> is no valid CEDATIME (14 Signs)",pcpCEDATime);
        ilRC = RC_FAIL;
     }
     else
     {
        for (llCnt = 0; (llCnt < llLen) && (ilRC == RC_SUCCESS); llCnt++)
        {
           clNum = pcpCEDATime[llCnt];
           if (!(isdigit(clNum))) /* Check if only numbers exist */
           {
              ilRC = RC_FAIL;
           }
        }
     }
  }

  /***  Action part ***/
  if (ilRC == RC_SUCCESS)
  {
     /* fill timestruct from CEDATIME */
     prlTimeStruct = &rlTimeStruct;
     memset(pclBuf,0x00,8);
     /* year 2000 problem */
     memcpy(pclBuf,&pcpCEDATime[0],2);
     ilNumHundred = atoi(pclBuf) - 19;
     memset(pclBuf,0x00,8);
     memcpy(pclBuf,&pcpCEDATime[2],2);
     prlTimeStruct->tm_year = atoi(pclBuf) + 100*ilNumHundred;
     memset(pclBuf,0x00,8);
     memcpy(pclBuf,&pcpCEDATime[4],2);
     prlTimeStruct->tm_mon  = atoi(pclBuf) - 1;
     memset(pclBuf,0x00,8);
     memcpy(pclBuf,&pcpCEDATime[6],2);
     prlTimeStruct->tm_mday = atoi(pclBuf);
     memset(pclBuf,0x00,8);   
     memset(pclBuf,0x00,8);
     memcpy(pclBuf,&pcpCEDATime[8],2);
     prlTimeStruct->tm_hour = atoi(pclBuf);
     memset(pclBuf,0x00,8);
     memcpy(pclBuf,&pcpCEDATime[10],2);
     prlTimeStruct->tm_min  = atoi(pclBuf);
     memset(pclBuf,0x00,8);
     memcpy(pclBuf,&pcpCEDATime[12],2);
     prlTimeStruct->tm_sec  = atoi(pclBuf);
     prlTimeStruct->tm_isdst = 0;
     prlTimeStruct->tm_yday  = 0;
     prlTimeStruct->tm_wday  = 0;

     /* get seconds from CEDATime */
     llSecVal = mktime (prlTimeStruct);
     if (llSecVal == -1)
     {
        dbg(TRACE,"AddSecondsToCEDATime: mktime of pcpCEDATime failed");
        ilRC = RC_FAIL;
        strcpy(pcpCEDATime, " ");
     }
     else
     {
        ilIsDst1 = prlTimeStruct->tm_isdst;
        if (prlTimeStruct->tm_isdst == 1) /* summertime not inserted before */
        {
           /* 1 hour back because struct was changed by mktime) */
           llSecVal -= 3600;
        }
        llSecVal += tpSeconds; /* add seconds */
     }
  }
   
  if (ilRC == RC_SUCCESS)
  {
     /* init timestruct */
     prlTimeStruct->tm_year = 0;
     prlTimeStruct->tm_mon  = 0;
     prlTimeStruct->tm_mday = 0;
     prlTimeStruct->tm_hour = 0;
     prlTimeStruct->tm_min  = 0;
     prlTimeStruct->tm_sec  = 0;
     prlTimeStruct->tm_isdst = 0;
     prlTimeStruct->tm_yday  = 0;
     prlTimeStruct->tm_wday  = 0;
     strcpy(pcpCEDATime," ");
     /* get struct with new time */
     prlTimeStruct = localtime(&llSecVal);  

     ilIsDst2 = prlTimeStruct->tm_isdst;
     if (ilIsDst1 != ilIsDst2)
     {  /* summer / winter time adjustment */
        if (ilIsDst1 == 1)
           prlTimeStruct->tm_hour += 1;
        else
           prlTimeStruct->tm_hour -= 1;
        if (prlTimeStruct->tm_hour == 24)
        {
           prlTimeStruct->tm_hour = 0;
           prlTimeStruct->tm_mday += 1;
        }
        else if (prlTimeStruct->tm_hour < 0)
        {
           prlTimeStruct->tm_hour = 23;
           prlTimeStruct->tm_mday -= 1;
        }
        if (prlTimeStruct->tm_mday == 32)
        {
           prlTimeStruct->tm_mday = 1;
           prlTimeStruct->tm_mon += 1;
        }
        else if (prlTimeStruct->tm_mday <= 0)
        {
           prlTimeStruct->tm_mday = 31;
           prlTimeStruct->tm_mon -= 1;
        }
     }

     /* get CEDATIME from timestruct */
     rlTimeStruct2 = *prlTimeStruct;
     switch (ipFormat)
     {
        case 1: /* CEDA-format */
           strftime(pcpCEDATime,15,
                    "%" "Y%" "m%" "d%" "H%" "M%" "S", &rlTimeStruct2);
           break;
        default:
           dbg(TRACE,"AddSecondsToCEDATime: unknown format <%d>!",ipFormat);
     }
  }

  return ilRC;

} /* end of AddSecondsToCEDATime */



/* TWE */
/*********************************************************************
Function   : AddSecondsToCEDATime2 
Paramter   : IN/OUT: pcpCEDATime  = String in CEDATimeformat where to add
             IN    : tpSeconds    = Number of seconds (+/-)
             IN    : pcpTimForm   = "UTC" or "LOC"
					   IN    : ipFormat     = Number of desired format
                                    (1 = CEDATimeformat)
ReturnCode : RC_SUCCESS, RC_FAIL 
Description: returns a timestamp with the desired format in a string buffer
             Current formats are:
						 1 = CEDA-timestamp (YYYYMMDDHHMISS)
*********************************************************************/
int AddSecondsToCEDATime2 (char *pcpCEDATime, time_t tpSeconds, 
                           char *pcpTimForm, int ipFormat)
{
int        ilRC          = RC_SUCCESS;
int        ilSummerFlag  = 0;
int        ilNumHundred  = 0;
long       llCnt         = 0;
long       llLen         = 0;
time_t     llSecVal      = 0;
char       pclBuf[8];
char       clNum;
struct tm  rlTimeStruct;
struct tm  rlTimeStruct2;
struct tm  *prlTimeStruct;

  /**** CHECK pcpCEDATime  ****/
  if (pcpCEDATime == NULL)                               /* Check if not NULL */
  {
     dbg(TRACE, "AddSecondsToCEDATime2: No Time to add to (NULL)");
     ilRC = RC_FAIL;
  }
  else
  {
     llLen = strlen(pcpCEDATime);
     if (llLen != 14)                         /* Check for CEDA-Format Length */
     {
        dbg(TRACE, "AddSecondsToCEDATime2: <%s> is no valid CEDATIME (14 Signs)",
                    pcpCEDATime);
        ilRC = RC_FAIL;
     }
     else
     {
        for (llCnt = 0; (llCnt < llLen) && (ilRC == RC_SUCCESS); llCnt++)
        {
           clNum = pcpCEDATime[llCnt];
           if (!(isdigit(clNum)))              /* Check if only numbers exist */
           {
              ilRC = RC_FAIL;
           }
        }
     }
  }

  /***  Action part ***/
  if (ilRC == RC_SUCCESS)
  {
     /* fill timestruct from CEDATIME */
     prlTimeStruct = &rlTimeStruct;

     memset(pclBuf,0x00,8);
/* year 2000 problem */
     memcpy(pclBuf,&pcpCEDATime[0],2);
     ilNumHundred = atoi(pclBuf) - 19;
     memset(pclBuf,0x00,8);
     memcpy(pclBuf,&pcpCEDATime[2],2);
     prlTimeStruct->tm_year = atoi(pclBuf) + 100*ilNumHundred;
/******/
     memset(pclBuf,0x00,8);
     memcpy(pclBuf,&pcpCEDATime[4],2);
     prlTimeStruct->tm_mon  = atoi(pclBuf) - 1;
     memset(pclBuf,0x00,8);
     memcpy(pclBuf,&pcpCEDATime[6],2);
     prlTimeStruct->tm_mday = atoi(pclBuf);
     memset(pclBuf,0x00,8);   
     memset(pclBuf,0x00,8);
     memcpy(pclBuf,&pcpCEDATime[8],2);
     prlTimeStruct->tm_hour = atoi(pclBuf);
     memset(pclBuf,0x00,8);
     memcpy(pclBuf,&pcpCEDATime[10],2);
     prlTimeStruct->tm_min  = atoi(pclBuf);
     memset(pclBuf,0x00,8);
     memcpy(pclBuf,&pcpCEDATime[12],2);
     prlTimeStruct->tm_sec  = atoi(pclBuf);
     prlTimeStruct->tm_isdst = 0;
     prlTimeStruct->tm_yday  = 0;
     prlTimeStruct->tm_wday  = 0;

     /* get seconds from CEDATime */
     llSecVal = mktime (prlTimeStruct);
     if (llSecVal == -1)
     {
        dbg(TRACE,"AddSecondsToCEDATime2: mktime of pcpCEDATime failed");
        ilRC = RC_FAIL;
        strcpy(pcpCEDATime, " ");
     }
     else
     {
        if (prlTimeStruct->tm_isdst == 1)   /* summertime not inserted before */
        {
           /* 1 hour back because struct was changed by mktime) */
           llSecVal -= 3600;
        }
        llSecVal += tpSeconds;                               /* add seconds */
     }
  }
   
  if (ilRC == RC_SUCCESS)
  {
     /* init timestruct */
     prlTimeStruct->tm_year = 0;
     prlTimeStruct->tm_mon  = 0;
     prlTimeStruct->tm_mday = 0;
     prlTimeStruct->tm_hour = 0;
     prlTimeStruct->tm_min  = 0;
     prlTimeStruct->tm_sec  = 0;
     prlTimeStruct->tm_isdst = 0;
     prlTimeStruct->tm_yday  = 0;
     prlTimeStruct->tm_wday  = 0;
     strcpy(pcpCEDATime, " ");

     /* get struct with new time */
     if (!strncmp(pcpTimForm, "LOC", 3))
     {
        prlTimeStruct = localtime(&llSecVal);
     }
     else
     {
        prlTimeStruct = gmtime(&llSecVal); 
     }
     rlTimeStruct2 = *prlTimeStruct;

     /* get CEDATIME from timestruct */
     if (ilRC == RC_SUCCESS)
     {
			  switch(ipFormat)
			  {
				  case 1: /* CEDA-format */
					   strftime(pcpCEDATime,15,
                      "%" "Y%" "m%" "d%" "H%" "M%" "S", &rlTimeStruct2);
					   break;
				  default:
					   dbg(TRACE,"AddSecondsToCEDATime2: unknown format <%d>!",ipFormat);
        }
     }
  }

  return ilRC;

} /* end of AddSecondsToCEDATime2 */


/* TWE */
/*********************************************************************
Function   : AddSecondsToTime 
Paramter   : IN    : pcpInTime    = Input Timestring
             IN    : pcpFormat    = Format of Input Timestring
             IN    : tpSeconds    = (+/-) seconds to Input Timestring
					   IN    : spMaxSize    = max len of Output Timestring 
             OUT   : pcpOutTime   = Output Timestring (same format as Input)
ReturnCode : RC_SUCCESS, RC_FAIL 
Description: Adds /subbs number of seconds (one day for example) from a Timestr
*********************************************************************/
int AddSecondsToTime (char *pcpInTime, char *pcpFormat, time_t tpSeconds,
                      short spMaxSize, char *pcpOutTime)
{
int   ilRC = RC_SUCCESS;
Date  date;

  ilRC = StringToDate(pcpInTime, pcpFormat, &date);
  if (ilRC == RC_SUCCESS)
  {
     date += tpSeconds;
     ilRC = DateToString (pcpOutTime, spMaxSize, pcpFormat, &date);
  }

  return ilRC;

} /* end of AddSecondsToTime */


/* TWE */
/*********************************************************************
Function   : DateToString 
Paramter   : OUT   : buffer       = resulting string (date)
             IN    : size         = maximum len of resulting string 
             IN    : format       = wished Format of resulting string
					   IN    : date         = points on Seconds till 01011970 to convert 
ReturnCode : RC_SUCCESS, RC_FAIL 
Description: returns a timestamp with the desired format in a string buffer
             Formats is described in format parameter. The value is in 
             Seconds till 1970.
Example    : DateToString(pclDate,30,"%A %e.%h %y %H:%M:%S",&(GetCurrentDate));
             pclDate = Wednesday 16.Feb 00 17:08:13
*********************************************************************/
int DateToString(char *buffer, const short size,
                 const char *format, const Date *date)
{
int         ilRC = RC_SUCCESS;
int         ilGermanMonth = 0;
int         ilLen;
struct tm   *_tm;
struct tm   prlTime; 
char        *text;
char        *pclPtr;
char        *pclPtrBeg;
char        month[10];
char        pclformat[100];
size_t      length;
size_t      retCode = 0;

         
  if ((buffer == NULL) || (format == NULL) || (date == NULL) || (size <= 0))
  {
     ilRC = RC_FAIL;
     dbg(TRACE,"DateToString: wrong parameters");
     return ilRC;
  }
  
  strcpy(pclformat, format);
  pclPtrBeg = pclformat;
  ilGermanMonth = 0;
  if ((pclPtr = strstr(pclformat, "g")) != NULL)
  {
     ilGermanMonth = 1;
     ilLen = (long)(pclPtr-pclPtrBeg);
     pclformat[ilLen] = 'h';
  }
  if ((pclPtr =strstr(pclformat, "G")) != NULL)
  {
     ilGermanMonth = 2;
     ilLen = (long)(pclPtr-pclPtrBeg);
     pclformat[ilLen] = 'h';
  }             

  text = (char *)buffer;
  length = (size_t) size;
  _tm = (struct tm *) localtime(date);
  if (_tm != NULL)
  {
     prlTime = *_tm;
     retCode = strftime(text,length,pclformat,&prlTime);
     if (ilGermanMonth > 0)
     {
        strftime(month,sizeof(month),"%h",&prlTime);/* get month in return str*/
        if ((pclPtr = strstr(text, month)) != NULL) /* points to month in str */
        {
           /* change to GERMAN */
           if (!strcmp(month, "May"))
           {
              strcpy(month, "Mai");
           }    
           else if (!strcmp(month, "Oct"))
           {
              strcpy(month, "Okt");
           }   
           else if (!strcmp(month, "Dec"))
           {
              strcpy(month, "Dez");
           }   

           /* change to LARGE */
           if (ilGermanMonth == 2)
           {
              strupr(month);
           }
         
           /* write to return date string */
           memcpy(pclPtr, month, 3);
        }
     }
     if (retCode == 0)
     {
        dbg(TRACE,"DateToString: strftime failed for %ld", *date);
        ilRC = RC_FAIL;
        return ilRC;
     } 
  }
  else
  {
     ilRC = RC_FAIL;
     dbg(TRACE,"DateToString: localtime from %ld failed", *date);
     return ilRC;
  }

  return ilRC;
}

/* TWE */
/*********************************************************************
Function   : StringToDate 
Paramter   : IN    : buffer       = string to convert 
             IN    : format       = Format of string to convert 
					   OUT   : date         = Seconds till 01011970 
ReturnCode : RC_SUCCESS, RC_FAIL 
Description: returns Seconds till 01011970000000 from a datestring.
Example    : StringToDate("16.02.2000 17:12:43", "%d%m%Y %H:%M:%S", &date);
             date = 
*********************************************************************/
int StringToDate(const char *buffer, const char *format, Date *date)
{
int       ilRC = RC_SUCCESS;
int       ilLen = 0;
struct tm helptime;
time_t    x;
char      *pclPtr;
char      *pclPtrBeg;
char      pclBuffer[100];
char      pclformat[100];

  if ((buffer == NULL) || (format == NULL) || (date == NULL))
  {
     ilRC = RC_FAIL;
     dbg(TRACE,"StringToDate: wrong parameters");
     return ilRC;
  }
 
  strcpy(pclBuffer, buffer); 
  strcpy(pclformat, format);
  pclPtrBeg = pclformat;
  if ((pclPtr = strstr(pclformat, "g")) != NULL)
  {
     ilLen = (long)(pclPtr-pclPtrBeg);
     pclformat[ilLen] = 'h';
     if ((pclPtr = strstr(pclBuffer, "Mai")) != NULL)
     {
        strncpy(pclPtr, "May", 3);      
     }
     else if ((pclPtr = strstr(pclBuffer, "Okt")) != NULL)
     {
        strncpy(pclPtr, "Oct", 3);      
     }
     else if ((pclPtr = strstr(pclBuffer, "Dez")) != NULL)
     {
        strncpy(pclPtr, "Dec", 3);      
     }
  }
  if ((pclPtr = strstr(pclformat, "G")) != NULL)
  {
     ilLen = (long)(pclPtr-pclPtrBeg);
     pclformat[ilLen] = 'h';
     BigGermanMonthToEnglishConvert(pclBuffer);
  }             

  /*
  ** initialize struct helptime (all to 0)
  */
  memset(&helptime,0,sizeof(helptime));
  strptime("010170000000","%d%m%y09/26/00M%S",&helptime);

  /* get time struct */
dbg(TRACE,"strptime with <%s>, <%s>", pclBuffer,pclformat);
  if (strptime(pclBuffer,pclformat,&helptime) == NULL)
  {
      
     ilRC = RC_FAIL;
     dbg(TRACE,"StringToDate: strptime from %s failed", pclBuffer);
     dbg(TRACE,"StringToDate:   <%d:%s>", errno,strerror(errno));
     return ilRC;
  }

  if (helptime.tm_year < CHNG_CENTURY)
  {
     helptime.tm_year += 100;                  /* year 2000 */
  }
       
  x  = mktime(&helptime);                  /* sets summertime automatically */
  if (x == -1)
  {
     /* mktime fails */
     *date = (Date )x; 
     ilRC = RC_FAIL;
     dbg(TRACE,"StringToDate: mktime from %s failed", pclBuffer);
     return ilRC;
  }
  else
  {
     if (helptime.tm_isdst == 1)  /* summertime not set before by strptime */
     {
        x -= 3600;                                /* minus 1 Hour */
     }
     *date = (Date )x;
  }

  return ilRC; /* seems to be ok */

}

/* TWE */
/*********************************************************************
Function   : DateStringToDateString 
Paramter   : IN    : buffer       = string to convert 
             IN    : format       = Format of string to convert 
					   OUT   : date         = Seconds till 01011970 
ReturnCode : RC_SUCCESS, RC_FAIL 
Description: returns Seconds till 01011970000000 from a datestring.
Example    : StringToDate("16.02.2000 17:12:43", "%d%m%Y %H:%M:%S", &date);
             date = 
*********************************************************************/
int DateStringToDateString(char *target_string, 
                           const char * target_format,
                           short target_size,
                           const char *source_format,
                           char *source_string)
{
int   ilRC = RC_SUCCESS; 
Date  _date;

  ilRC = StringToDate(source_string,source_format,&_date);
  
  if (ilRC != RC_SUCCESS) return ilRC;
  
  ilRC = DateToString(target_string, target_size, target_format, &_date);

  return ilRC;

}

/* TWE */
/*********************************************************************
Function   : DayOfWeek
Paramter   : IN    : date       = Seconds till 01011970 00:00:00
ReturnCode : 1-7 or -20000
Description: returns weekday 1 till 7
*********************************************************************/
short DayOfWeek(const Date *date)
{
struct tm   *_tm;
struct tm   prlTime;
char        text[2];
short       day;

  if (date == NULL) return(-20000);
  _tm = (struct tm *)localtime(date);
  if (_tm != NULL)
  {
     prlTime = *_tm;
     strftime(text,sizeof(text),"%w",&prlTime);
     day = (short) atoi(text);
     /* correct day Sunday = 7, not 0 */
     if (day == 0) day = 7;
     return(day);
  }
  else
  {
     return (-20000);
  }
}


/* TWE */
/*********************************************************************
Function   : DayOfMonth
Paramter   : IN    : date       = Seconds till 01011970 00:00:00
ReturnCode : 1 - 31 or -20000
Description: returns DayOfMonth 1 - 31 
*********************************************************************/
short DayOfMonth(const Date * date)
{
struct tm   *_tm;
struct tm   prlTime;
char        text[3];

  if (date == NULL) return(-20000);
  _tm = (struct tm *)localtime(date);
  if (_tm != NULL)
  {
     prlTime = *_tm;
     strftime(text,sizeof(text),"%e",&prlTime);
     return((short) atoi(text));
  }
  else
  {
     return (-20000);
  }
}

/* TWE */
/*********************************************************************
Function   : DayOfYear
Paramter   : IN    : date       = Seconds till 01011970 00:00:00
ReturnCode : 1 - 366 or -20000
Description: returns DayOfYear 1 - 366 
*********************************************************************/
short DayOfYear(const Date * date)
{
struct tm   *_tm;
struct tm   prlTime;
char        text[4];

  if (date == NULL) return(-20000);
  _tm = (struct tm *)localtime(date);
  if (_tm != NULL)
  {
     prlTime = *_tm;
     strftime(text,sizeof(text),"%j",&prlTime);
     return((short) atoi(text));
  }
  else
  {
     return (-20000);
  }                                           
}

/* TWE */
/*********************************************************************
Function   : Year
Paramter   : IN    : date       = Seconds till 01011970 00:00:00
ReturnCode : Year or -20000 (4Letter)
Description: returns Year 1970 - 2030
*********************************************************************/
short Year(const Date * date)
{
struct tm   *_tm;
struct tm   prlTime;
char        text[5];

  if (date == NULL) return(-20000);
  _tm = (struct tm *)localtime(date);
  if (_tm != NULL)
  {
     prlTime = *_tm;
     strftime(text,sizeof(text),"%Y",&prlTime);
     return((short) atoi(text));
  }
  else
  {
     return (-20000);
  }                                    
}

/* TWE */
/*********************************************************************
Function   : MonthOfYear
Paramter   : IN    : date       = Seconds till 01011970 00:00:00
ReturnCode : Month 1 - 12 or -20000 (4Letter)
Description: returns month 1 - 12 
*********************************************************************/
short MonthOfYear(const Date * date)
{
struct tm   *_tm;
struct tm   prlTime; 
char        text[3];

  if (date == NULL) return(-20000);
  _tm = (struct tm *)localtime(date);
  if (_tm != NULL)
  {
     prlTime = *_tm;
     strftime(text,sizeof(text),"%m",&prlTime);
     return((short) atoi(text));
  }
  else
  {
     return (-20000);
  }                                   
}

/* TWE */
/*********************************************************************
Function   : GetCurrentDate
Paramter   : NONE
ReturnCode : Seconds from 01011970 00:00:00 till now 
Description: returns Seconds from 01011970 00:00:00 till now 
*********************************************************************/
Date GetCurrentDate()
{
time_t timenow = 0;
struct tm *timePtr;

  time(&timenow);
  timePtr = localtime(&timenow);

  return(mktime(timePtr));

}  

static void BigGermanMonthToEnglishConvert(char *pcpBuffer)
{
char *pclPtr;

  if ((pclPtr = strstr(pcpBuffer, "JAN")) != NULL)
  {
     strncpy(pclPtr, "Jan", 3);
  }
  else if ((pclPtr = strstr(pcpBuffer, "FEB")) != NULL)
  {
     strncpy(pclPtr, "Feb", 3);
  }
  else if ((pclPtr = strstr(pcpBuffer, "MAR")) != NULL)
  {
     strncpy(pclPtr, "Mar", 3);
  }
  else if ((pclPtr = strstr(pcpBuffer, "APR")) != NULL)
  {
     strncpy(pclPtr, "Apr", 3);
  }
  else if ((pclPtr = strstr(pcpBuffer, "MAI")) != NULL)
  {
     strncpy(pclPtr, "May", 3);
  }
  else if ((pclPtr = strstr(pcpBuffer, "JUN")) != NULL)
  {
     strncpy(pclPtr, "Jun", 3);
  }
  else if ((pclPtr = strstr(pcpBuffer, "JUL")) != NULL)
  {
     strncpy(pclPtr, "Jul", 3);
  }
  else if ((pclPtr = strstr(pcpBuffer, "AUG")) != NULL)
  {
     strncpy(pclPtr, "Aug", 3);
  }
  else if ((pclPtr = strstr(pcpBuffer, "SEP")) != NULL)
  {
     strncpy(pclPtr, "Sep", 3);
  }
  else if ((pclPtr = strstr(pcpBuffer, "OKT")) != NULL)
  {
     strncpy(pclPtr, "Oct", 3);
  }
  else if ((pclPtr = strstr(pcpBuffer, "NOV")) != NULL)
  {
     strncpy(pclPtr, "Nov", 3);
  }
  else if ((pclPtr = strstr(pcpBuffer, "DEZ")) != NULL)
  {
     strncpy(pclPtr, "Dec", 3);
  }

} /* end of BigGermanMonthToEnglishConvert */
