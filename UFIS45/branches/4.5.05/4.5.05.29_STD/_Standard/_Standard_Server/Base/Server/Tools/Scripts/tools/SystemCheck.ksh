# count file descriptors used by process
cd /proc
if [ "${LOGNAME}" = "root" -o "${LOGNAME}" = "" ] then
  # works on AIX
  ps -eo user,pid,comm | while read dummy pid proc ; do echo $pid $proc `find $pid/fd  2>/dev/null | wc -l` ; done | sort -nk 3
else
  # works on AIX
  ps -eo user,pid,comm | grep "${LOGNAME}" | while read dummy pid proc ; do echo $pid $proc `find $pid/fd  2>/dev/null | wc -l` ; done | sort -nk 3
fi

# sort processes by used memory:
ps -efl | sort -nk 10 

# sort CEDA processes by used memory, filter oracle shadow procs:
ps -efl | sort -nk 10 | grep ceda | grep -v oracle

# check for long running SQL selects (after 120 seconds client gives up)
grep "READY .... SEC" flight*.log* sqlhdl*.log*

# check for long running SQL selects from today (after 120 seconds client gives up)
grep "`date +%Y%m%d`.*READY .... SEC" flight*.log* sqlhdl*.log*

# check for long running SQL selects (2 digit check for performance leaks)
grep "READY ... SEC" flight*.log* sqlhdl*.log*

# check for KILL in running NETIN log files:
pfind netin | grep -v grep | while read d p r ; do grep -c KILL netin$p.log* ; done


SQL> select count(*) from drrtab where urno>0;

  COUNT(*)
----------
    910446

SQL> select count(*) from tlxtab  where urno>0;

  COUNT(*)
----------
    820796

SQL> select min sday from drrtab where sday<>' ';
select min sday from drrtab where sday<>' '
       *
ERROR at line 1:
ORA-00904: "MIN": invalid identifier


SQL> select (sday^C

SQL> select min(sday) from drrtab where sday<>' ';

MIN(SDAY
--------
20040426

SQL> select min(TIME) from tlxtab;

MIN(TIME)
--------------
20040215055000

grep  " ...TAB" daily.log | sort -nk 6
. . exporting table                         CFLTAB     135460 rows exported
. . exporting table                         AFTTAB     163233 rows exported
. . exporting table                         PRVTAB     185713 rows exported
. . exporting table                         CCATAB     208407 rows exported
. . exporting table                         APXTAB     266142 rows exported
. . exporting table                         GHDTAB     319652 rows exported
. . exporting table                         LOGTAB     332177 rows exported
. . exporting table                         L00TAB     380531 rows exported
. . exporting table                         JODTAB     387670 rows exported
. . exporting table                         SLGTAB     533655 rows exported
. . exporting table                         JOBTAB     633753 rows exported
. . exporting table                         RELTAB     645533 rows exported
. . exporting table                         DEMTAB     684206 rows exported
. . exporting table                         SPRTAB     793002 rows exported
. . exporting table                         TLXTAB     820083 rows exported
. . exporting table                         DRRTAB     910446 rows exported
. . exporting table                         LOATAB    1799029 rows exported
. . exporting table                         L01TAB    2443123 rows exported
