#ifndef _DEF_mks_version_oracle_h
  #define _DEF_mks_version_oracle_h
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_oracle_h[]   MARK_UNUSED = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Library/Inc/oracle.h 1.2 2004/07/27 16:48:14SGT jim Exp  $";
#endif /* _DEF_mks_version */
/*******************************************************************/
/*******************************************************************/
/*******************************************************************/
/*	<orafund.h>	Definitions and structures for ORACLEfunc  */

#ifndef _ORACLE_H
#define _ORACLE_H


#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>

#include "glbdef.h"
#include "flddef.h"

#ifdef CCSDB_ORACLE

/* following the configuration defines 			*/
/* Max field size in ORACLE */
#define		MAX_FLD_SIZE		32000	 /* usaed for data_area[] */
#define		SQL_BUFSIZE		(8*1024) /* used for sql_buf[]    */
#define		NO_SYSTEM		"_N_O_S_Y_S_T_E_M_"  

/* following the functional defines from frfunc		*/


#ifdef PREV 
#undef PREV
#endif

#ifdef LAST 
#undef LAST
#endif

#ifdef LESS 
#undef LESS
#endif

#ifdef EQUAL 
#undef EQUAL
#endif

/* following the functional defines for db_if		*/
#define		IGNORE			NOACTION
#define		PREV			6
#define		LAST			8
#define		LESS		       16
#define		EQUAL		       32

/* following the internal data type defines		*/
#define		BYTE			1
#define		CHAR			2
#define		SHORT			3
#define		INT				4
#define		LONG			5
#define		RAW				6
#define		STR             7
#define		LONGRAW			8

#define		BYTE_SIZE		1
#define		CHAR_SIZE		sizeof(char)
#define		SHORT_SIZE		sizeof(short)
#define		INT_SIZE		sizeof(int)
#define		LONG_SIZE		sizeof(long)
#define		RAW_SIZE		BYTE_SIZE
#define		STRING_SIZE		BYTE_SIZE

/* following the return + error code defines			*/
#define		DB_SUCCESS		RC_SUCCESS   /*rkl991209 SUCCESS -> RC_SUCCESS */
#define		DB_ERROR		RC_FAIL      /*rkl001209 FAILURE -> RC_FAIL    */
#define		NOTFOUND		1
#define		SQL_NOTFOUND		NOTFOUND

#ifndef END
#define		END			-9999
#endif
#define		INVALID_RC		-9998

int init_db(void);
int commit_work(void);
int create(int);
void get_fld(char *buf,short fld_no,short fld_type,int fld_len,char *dest);
char *calc_adr(char *,short );
void form_hex_buf();
void form_char_buf();
char hex_to_byte();
int sql_if(short ,short* ,char* ,char* );
int rollback(void);
int logoff(void);
int get_idx_cnt( char *table );
short delton(char *buffer);
short nlton(char *buffer);
int close_my_cursor(short *cursor);

extern mtox();

#endif

#endif  /* _ORACLE_H */
