<?php 
/*
Copyright � 2004 by INFOMAP S.A.
This computer program is protected by copyright law
of Greece and International treaties. Unauthorized
reproduction of this program, or any portion of it,
may result in severe civil and criminal penalties, and
will be prosecuted to the maximum extent possible under
the law.
*/

	include("include/manager.php");

	include("include/class.LoginInit.php");
	/* Login Check */
	/* Set Login Instance*/
	$login = new  LoginInit;
	$logintype="Login_"	.$AuthType;					
	if (isset($Session["userid"])) { 
		// The user has already been Logged

	} else {
		$Query=	$login->$logintype($username,$password);

	}
	
	/* Start Debug  if requested */
	if ($debug=="true") {
		$tmpdebug="?debug=true";
		$db->debug=$debug;
		$dbMysql->debug=$debug;
	}else {
		$debug="false";
	}			 
	if ($Session["Type"]=="GATE") {
		$main_file="mainGate.php";		
	} else {
		$main_file="main.php";
	}
	$CounterStatus = Status();
	
	
?>

<html>
<title><?php echo $PageTitle ?></title>
 <META NAME="description" CONTENT="">
  <META NAME="author" CONTENT="Infomap - Team@Work - T. Dimitropoulos/e-mail:thimios@infomap.gr">
  <META NAME="dbauthor" CONTENT="Infomap - Team@Work - A.Papachrysanthou/e-mail:Anthony@infomap.gr">  
  <META NAME="author" CONTENT="Infomap - Team@Work - G. Fourlanos/e-mail:fou@infomap.gr">
  <META NAME="Art designer" CONTENT="Infomap - Team@Work - S. Rallis/e-mail:stratos@infomap.gr">  
  <META NAME="Art designer" CONTENT="Infomap - Team@Work - K. Xenou/e-mail:batigol@infomap.gr">  
  <META HTTP-EQUIV="Reply-to" CONTENT="webmaster@infomap.gr">
  <META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=windows-1253">
  <META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=iso-8859-7">
  <META HTTP-EQUIV="Expires" CONTENT="<?php echo $Cexpires ?>">
  <link rel="STYLESHEET" type="text/css" href="style/style.css">
</head>

<script language="JavaScript1.2">
	<?php
	
	
		
	
	echo "parent.Worksheet.location.href=\"$main_file?RButton=".$RButton."\";\n";
	
	if ($logoSelection==false) { 
		//echo "window.setTimeout('parent.Logo.location.href=\"logo.php\"',240);\n";
	}
	
	
	if ($Session["Type"]=="CHECK-IN") {
		if ($DisplayBoth_Logo_Remark==false) {
		if ($Session["CheckinPredefinedSelection"] == true) {
			$logo_frame="blank.html";
			$remarks_file="remarks.php";
		} elseif ($Session["logoSelection"]== true || $Session["logoSelectionUser"] == true) {
			$logo_frame="logo.php";
			$remarks_file="blank.html";
		} else {
			$remarks_file="blank.html";
			$logo_frame="blank.html";
		}
		}else {
			$remarks_file = "remarks.php";
			$logo_frame="logo.php";
			}
	  echo "parent.Logo.location.href=\"".$logo_frame."\";\n";
	  echo "parent.Remarks.location.href=\"".$remarks_file."\";\n";
	}
	
	
?>
	//alert(parent.Logo.location.href);
</script>

<body background="pics/blueband.jpg" onload="">
	<center>
	<table border=0 width=100%>
		<tr>
			<td align="center"><br>
				<font size=5 font face=Arial font color=white>-<?php echo $Session["Type"] ?>&nbsp;<?php echo $Session["Number"] ?>&nbsp;<?php echo $CounterStatus?>-</font>
			</td>
		</tr>
	</table>
	</center>
</body>
</html>
