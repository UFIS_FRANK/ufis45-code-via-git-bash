#ifndef _DEF_mks_version_ivsif_h
  #define _DEF_mks_version_ivsif_h
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_ivsif_h[]   MARK_UNUSED = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Library/Inc/ivsif.h 1.2 2004/07/27 16:47:56SGT jim Exp  $";
#endif /* _DEF_mks_version */
/* ********************************************************************** */
/*  The Master include file.                                              */
/*                                                                        */
/*  Program       : ivsif  Interaktive Voice System Interface             */
/*                                                                        */
/*  Revision date : 13.02.2001                                            */
/*                                                                        */
/*  Author    	  : eth                                                   */
/*                                                                        */
/*  History       : eth 30.05.2001 deleted all parent-child               */
/*                  communication staff (child die fast now)              */
/*                                                                        */
/*                                                                        */
/*                                                                        */
/* ********************************************************************** */
/* ********************************************************************** */

/* length of queue name */
#define MSGSTRLEN 16

/* length of VRU message strings */
#define VRUMSGLEN 256


/* ********************************************************************** */
/* ********************************************************************** */
