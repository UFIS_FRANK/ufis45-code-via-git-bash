<?php
include("GlobalSettings.php");

function CallCput($Dbg_level,$Dest,$Prio,$Command,$Table,$Fields,$Data,$Selection,$Timeout,$SubCmd)
{
	$Func="CallCput";

	//////////////////////////////////////////////////////////////////////
	// if the len is <=0 we must not have some "" around the those values.
	// this would mix up the cput-programm
	//////////////////////////////////////////////////////////////////////
	if (strlen($Fields) >= 0)
	{
		str_pad($Fields,strlen($Fields)+2,"\"",STR_PAD_BOTH);
	}
	if (strlen($Data) >= 0)
	{
		str_pad($Data,strlen($Data)+2,"\"",STR_PAD_BOTH);
	}
	if (strlen($Selection) >= 0)
	{
		str_pad($Selection,strlen($Selection)+2,"\"",STR_PAD_BOTH);
		$Selection = stripslashes($Selection);
	}

	///////////////////////////////////////////////////////
	// now we sho some info about the event if DEBUG is set
	///////////////////////////////////////////////////////
	if ($Dbg_level==DEBUG)
	{
		// print $Func."--DEST:".$Dest." "."PRIO:".$Prio." "."COMMAND:".$Command." "."TABLE:".$Table." "."FIELDS:".$Fields." "."DATA:".$Data." "."SELECT.:".$Selection." "."TIMEOUT:".$Timeout;

		print "<br>-----------------------------------------";
		print "<br>".$Func."-Dest   :".$Dest;
		print "<br>".$Func."-Prio   :".$Prio;
		print "<br>".$Func."-Command:".$Command;
		print "<br>".$Func."-Table  :".$Table;
		print "<br>".$Func."-Fields :".$Fields;
		print "<br>".$Func."-Data   :".$Data;
		print "<br>".$Func."-Select.:".$Selection;
		print "<br>".$Func."-Timeout:".$Timeout;
		print "<br>-----------------------------------------<br>";
	}

	////////////////////////////////////////////// 
	// now we call cput with the passed parameters 
	////////////////////////////////////////////// 
	//$tmp_result=exec("/ceda/etc/cput 2 "
	//		.escapeshellarg($Dest)." "
	//		.escapeshellarg($Prio)." "
	//		.escapeshellarg($Command)." "
	//		.escapeshellarg($Table)." "
	//		.escapeshellarg($Fields)." "
	//		.escapeshellarg($Data)." "
	//		.escapeshellarg($Selection)." "
	//		.escapeshellarg($Timeout)
	//		,$output);
	$cmd='/ceda/etc/cput 0 '
			.$Dest.' '
			.$Prio.' '
			.$Command.' '
			.$Table.' '
			.$Fields.' "'
			.$Data.'" "'
			.$Selection.'" '
			.$Timeout.' ';
	//echo "<pre>CMD:$cmd, SubCmd:$SubCmd</pre>";
	if (!empty($SubCmd))
		$result=`$cmd|$SubCmd`;
	else
		$result=`$cmd`;
	//echo "<pre>RES:$result</pre>";
	return $result;
}
?>
